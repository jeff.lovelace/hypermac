import os
import re
ignore=['ccp4','hdf5','zlib','szip','build','git','helpers','cmake','examples','source','scratch','notused','finished']
b=os.walk("./")
functionlist=[]
for root,dirs,files in b:
  lroot=root.lower()
  gooddir=not any(x in lroot for x in ignore)
  if gooddir:
    print "Root: "+str(root.lower())
    for afile in files:
      if ".f" in afile.lower():
        inf = open(root+'/'+afile)
        try:
          cl=""
          cc=False
          for line in inf:
            tmp=re.sub('[\s+]','',line.lower())
            if tmp[-1:]=='&':
              if cc:
                cl+=tmp
              else:
                cl=""
                cl+=tmp
                cc=True
              continue
            else:
              if cc:
                cl+=tmp
                cc=False
                cl=re.sub('&','',cl)
              else:
                cl=tmp
            if cl.find('function')==0:
              start=len('function')
              end=tmp.find('(')
              if end is not -1:
                functionlist.append(cl[start:end])
              else:
                print "    > ERROR"
        finally:
          inf.close()

functionlist=list(set(functionlist))
functionlist.sort(key=len,reverse=True)
functionnames=functionlist
functionlist=list()
for item in functionnames:
  functionlist.append(item+"(")
b=os.walk("./")
for root,dirs,files in b:
  lroot=root.lower()
  gooddir=not any(x in lroot for x in ignore)
  if gooddir:
    print "Root: "+str(root.lower())
    for afile in files:
      if ".f" in afile.lower():
        peek_next_line=False
        inf = open(root+'/'+afile)
        if afile.lower()[-4:]==".for":
          peek_next_line=True
        print "Processing: "+root+"/"+afile
        try:
          fend=os.fstat(inf.fileno()).st_size
          cfp=inf.tell()
          cntchar=""
          cl=""
          rl=""
          crl=""
          raw=""
          cc=False
          ias=False
          cs=""
          cscf=set()
          cscs=set()
          cfcf=set()
          cfcs=set()
          cf=""
          iaf=False
          iap=False
          cp=""
          cpcf=set()
          cpcs=set()
          while fend!=cfp:
            line=inf.readline()
            cfp=inf.tell()
            line=line.rstrip()
            raw=line
            line=line.lstrip()
            line=line.lstrip('0123456789.- ')
            tmp=re.sub('[\s]','',line.lower())
            if tmp[-1:]=='&':
              if cc:
                cl+=tmp
                rl+=raw.lstrip()
              else:
                cl=""
                cl+=tmp
                rl=""
                rl+=raw.rstrip()
                rl+=rl.lstrip()
                cc=True
              continue
            else:
              if cc:
                if peek_next_line:
                  cl+=tmp[6:]
                  rl+=raw[6:].lstrip().rstrip()
                  tmploc=inf.tell()
                  tline=inf.readline()
                  inf.seek(tmploc)
                  contchar=""
                  try:
                    contchar=tline[5]
                  except IndexError:
                    contchar=" "
                  if contchar!=" ":
                    cc=True
                    continue
                  else:
                    cc=False
                else:
                  cl+=tmp
                  rl+=raw
                  cc=False
                  cl=re.sub('&','',cl)
                  rl=re.sub('&','',rl)
              else:
                cl=tmp
                rl=raw.lstrip().rstrip()
                if peek_next_line:
                  tmploc=inf.tell()
                  tline=inf.readline()
                  inf.seek(tmploc)
                  contchar=""
                  try:
                    contchar=tline[5]
                  except IndexError:
                    contchar=" "
                  if contchar!=" ":
                    cc=True
                    continue
            if ias or iaf:
              if (cl.find('endd') is not -1) or (cl.find('endi') is not -1):
                pass
              elif cl.find('end')==0:
                if ias:
                  print " Sub: "+cs
                  print "  "+crl
                  a=list(cscf)
                  a.sort()
                  for item in a:
                    print "  >func: "+item[:-1]
                  a=list(cscs)
                  a.sort()
                  for item in a:
                    print "  >sub: "+item

                if iaf:
                  print " Func: "+cf
                  print "  "+crl
                  a=list(cfcf)
                  a.sort()
                  for item in a:
                    print "  >func: "+item[:-1]
                  a=list(cfcs)
                  a.sort()
                  for item in a:
                    print "  >sub: "+item

                ias=False
                cs=""
                cscf=set()
                cscs=set()
                cfcf=set()
                cfcs=set()
                cf=""
                iaf=False
                continue
            if (cl == 'end' or cl.find('endp') is not -1) and iap:
              print " Program: "+cp
              a=list(cpcf)
              a.sort()
              for item in a:
                print "  >Func: "+item[:-1]
              a=list(cpcs)
              a.sort()
              for item in a:
                print "  >Sub: "+item
              iap=False
              cp=""
              cpcs=set()
              cpcf=set()
              continue

            if iap:
              matches = [x for x in functionlist if x in cl]
              matches = set(matches)
              subs=[]
              count=0
              for x in matches:
                for y in matches:
                  if x in y:
                    count=count+1
                if count>1:
                  subs.append(x)
                count=0
              for item in subs:
                matches.discard(item)
              matches=list(matches)
              for item in matches:
                cpcf.add(item)
              if cl.find('callt')==0:
                pass
              elif cl.find('call')==0:
                start=len('call')
                end=cl.find('(')
                cpcs.add(cl[start:end])
            elif ias:
              matches = [x for x in functionlist if x in cl]
              matches = set(matches)
              subs=[]
              count=0
              for x in matches:
                for y in matches:
                  if x in y:
                    count=count+1
                if count>1:
                  subs.append(x)
                count=0
              for item in subs:
                matches.discard(item)
              matches=list(matches)
              for item in matches:
                cscf.add(item)
              if cl.find('callt')==0:
                pass
              elif cl.find('call')==0:
                start=len('call')
                end=cl.find('(')
                cscs.add(cl[start:end])
            elif iaf:
              matches = [x for x in functionlist if x in cl]
              matches = set(matches)
              subs=[]
              count=0
              for x in matches:
                for y in matches:
                  if x in y:
                    count=count+1
                if count>1:
                  subs.append(x)
                count=0
              for item in subs:
                matches.discard(item)
              matches=list(matches)
              for item in matches:
                cfcf.add(item)
              if cl.find('callt')==0:
                pass
              elif cl.find('call')==0:
                start=len('call')
                end=cl.find('(')
                cfcs.add(cl[start:end]) 
            elif cl.find('subroutine')==0:
              start=len('subroutine')
              ias=True
              crl=rl
              end=cl.find('(')
              if end is not -1:
                cs=cl[start:end]
              else:
                cs=cl[start:]
            elif cl.find('function')==0:
              start=len('function')
              end=tmp.find('(')
              crl=rl
              iaf=True
              if end is not -1:
                cf=cl[start:end]
              else:
                print "    > ERROR"
            elif cl.find('program')==0:
               start=len('program')
               cp=cl[start:]
               iap=True
        finally:
          inf.close()
