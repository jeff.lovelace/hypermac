from os import listdir,getcwd,path,mkdir,chdir,system
from os.path import isfile, join
from distutils import file_util
from subprocess import call
from filecmp import cmp

newfilelist=[]
deletedfilelist=[]
skippedlibraries=[]

scratch='../scratch'
cmn_dir='../cmns'
lib_dir='../libs'
mod_dir='../mods'
exe_dir='../exes'
prog386split='../build/release/bin/386split'

def processLibrary(modulename,moduleext,moduldestdir):
  print ' -Processing Module: '+modulename
  moduledir=path.join(scratch,modulename)
  if path.exists(moduledir):
    if path.isdir(moduledir):
      print '  -Directory Exists'
    else:
      mkdir(moduledir)
      print '  -Created Directory'
  else:
    mkdir(moduledir)
    print '  -Created Directory'
  print '  -Copying file to src extension'
  modulefilename=modulename.lower()+'.src'
  file_util.copy_file(modulename+moduleext,path.join(moduledir,modulefilename))
  print '  -Changing working directory to: '+moduledir
  chdir(moduledir)
  print '  -Executing 386split'
  system(prog386split+' -Q -C  '+modulefilename+" \*")
  print '  -Updating Files'

  newfilesset=set(listdir(moduledir))
  oldfilesset=set(listdir(path.join(moduldestdir,modulename)))

  tnewfilelist=list(newfilesset-oldfilesset)
  tdeletedfilelist=list(oldfilesset-newfilesset)

  ignored_files={'CMakeLists.txt','ktere.lbc','preklad.bat'}

  tnewfilelist = [x for x in tnewfilelist if (x not in ignored_files) and (x!=modulefilename)]

  tdeletedfilelist = [x for x in tdeletedfilelist if x not in ignored_files]

  for index, item in enumerate(tnewfilelist):
    tnewfilelist[index]=path.join(moduldestdir,modulename,item)

  for index, item in enumerate(tdeletedfilelist):
    tdeletedfilelist[index]=path.join(moduldestdir,modulename,item)

  newfilelist.extend(tnewfilelist)
  deletedfilelist.extend(tdeletedfilelist)

  for anItem in listdir(moduledir):
    filename,fileext=path.splitext(anItem)
    fileext=fileext.strip().lower()
    if fileext=='.for':
      newfile=path.join(moduledir,anItem)
      oldfile=path.join(moduldestdir,modulename,anItem)
      if path.exists(oldfile):
        if cmp(newfile,oldfile,shallow=False):
          print '   -File Unchanged: '+anItem
        else:
          file_util.copy_file(newfile,oldfile,update=1)
          print '   -Updating: '+ anItem
      else:
        file_util.copy_file(newfile,oldfile,update=1)
        print '   -New File Discovered: '+anItem
        print '   -Updating: '+ anItem
    elif fileext=='.cmn':
      print '  -Got commnon block: '+filename+fileext
      afile=path.join(cmn_dir,anItem)
      updatefileifnewer(anItem,afile)
    else:
      print 'Unknown file type: '+anItem

  print '  -Changing working back to source.'
  chdir(startingdir)

def updatefileifnewer(srcfile,destfile):
  if path.exists(destfile):
    if cmp(srcfile,destfile,shallow=False):
      print '   -File Unchanged: '+anItem
    else:
      file_util.copy_file(srcfile,destfile,update=1)
      print '   -Updating: '+ anItem
  else:
    file_util.copy_file(srcfile,destfile,update=1)
    print '   -New File Discovered: '+anItem
    print '   -Updating: '+ anItem
    newfilelist.append(destfile)

startingdir=getcwd()
rootdiridx=startingdir.rfind('/')
rootdir=startingdir[0:rootdiridx]
scratch=rootdir+'/scratch'
cmn_dir=rootdir+'/cmns'
lib_dir=rootdir+'/libs'
mod_dir=rootdir+'/mods'
exe_dir=rootdir+'/exes'
prog386split=rootdir+'/build/release/bin/386split'

for anItem in listdir(startingdir):
  filename,fileext=path.splitext(anItem)
  fileext=fileext.strip().lower()
  if fileext=='.f95':
    if filename.lower()=='janamod':
      print 'Got a Jana Module'
      processLibrary(filename,fileext,mod_dir)
    elif filename.lower()=='wkscattf':
      print 'Got WKScatt Module'
      afile=path.join(mod_dir,filename,filename+'.for')
      updatefileifnewer(anItem,afile)
    elif filename.lower()=='386split':
      print 'Got 386Split Executable'
      afile=path.join(exe_dir,filename+'.f95')
      updatefileifnewer(anItem,afile)
    elif filename.lower()=='refinebatch':
      print 'Got Jana2006 Executable'
      afile=path.join(exe_dir,filename+'.f95')
      updatefileifnewer(anItem,afile)
    else:
      print 'Got a library: '+filename+fileext
      if path.exists(path.join(lib_dir,filename)):
        processLibrary(filename,fileext,lib_dir)
      else:
        skippedlibraries.append(path.join(lib_dir,filename))
        print ' -Library is not used by Hypermac'
  elif fileext=='.cmn':
    print 'Got commnon block: '+filename+fileext
    afile=path.join(cmn_dir,anItem)
    updatefileifnewer(anItem,afile)
  else:
    print 'Got unknown file type: '+filename+fileext

skippedlibraries

f = open(path.join(scratch,'skipped.txt'), "w")
f.write("\n".join(str(x) for x in skippedlibraries))
f.close()

print ' '
print 'Skipped Libraries:'
print '-----------------------'

for anItem in skippedlibraries:
  print ' '+anItem



f = open(path.join(scratch,'new.txt'), "w")
f.write("\n".join(str(x) for x in newfilelist))
f.close()

print ' ' 
print 'New Files:'
print '-----------------------'

for anItem in newfilelist:
  print ' '+anItem

f = open(path.join(scratch,'deleted.txt'), "w")
f.write("\n".join(str(x) for x in deletedfilelist))
f.close()

print ' '
print 'Deleted Files:'
print '-----------------------'

for anItem in deletedfilelist:
  print ' '+anItem

