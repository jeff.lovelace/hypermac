module hm_nl
  implicit none
  character(len=:),allocatable,save :: nl
  character,parameter :: lf=char(10)
  contains
    subroutine hm_nl_initialize()
      implicit none
      allocate(character(len=len(new_line("A"))) :: nl)
      nl=new_line("A")
    end subroutine hm_nl_initialize

end module hm_nl
