      module EDZones_mod
      character*8 :: IdM42(20) =
     1        (/'nzones  ','ormat   ','omega   ','gmax    ','sgmax   ',
     2          'sgmaxr  ','csgmaxr ','intsteps','calcdyn ','commands',
     3          'usewks  ','absflag ','end     ','Refblock',
     4          'threads ','tiltcorr','iedt    ','scalefc ',
     5          'r#19    ','r#20    '/)
      integer :: NMaxEDZone=0,EDNPar=6,NRefED,ActionED=1,AbsFlagED=1,
     1           NRefBlockED=0,ActionDatBlock
      character*256 :: EDCommands=' '
      logical :: CalcDyn=.false.,UseWKS=.true.,RescaleToFCalc=.true.
      real E0ED,UisoED,GammaED
      integer, allocatable :: NEDZone(:),EDIntSteps(:),EDThreads(:),
     1                        EDTiltcorr(:),EDGeometryIEDT(:)
      real, allocatable :: OrMatEDZone(:,:,:),GMaxEDZone(:),
     1                     SGMaxMEDZone(:),SGMaxREDZone(:),
     2                     CSGMaxREDZone(:),OmEDZone(:)
      integer, allocatable :: KiED(:,:,:),IHED(:,:),FlagED(:),
     1                        NThickEDZone(:,:)
      real, allocatable :: HEDZone(:,:,:),AlphaEDZone(:,:),
     1  BetaEDZone(:,:),PrAngEDZone(:,:),ScEDZone(:,:),ScEDZoneS(:,:),
     2  RFacEDZone(:,:),ThickEDZone(:,:),ThickEDZoneS(:,:),
     3  XNormEDZone(:,:),XNormEDZoneS(:,:),
     4  YNormEDZone(:,:),YNormEDZoneS(:,:),
     5  PhiEDZone(:,:),PhiEDZoneS(:,:),
     6  ThetaEDZone(:,:),ThetaEDZoneS(:,:),
     7  ADerED(:,:),BDerED(:,:),IDerED(:,:),ACalcED(:),BCalcED(:),
     8  SinThLED(:),IObsED(:),SIObsED(:),FCalcED(:),ExcitErrED(:)
      logical, allocatable :: UseEDZone(:,:)
      end
