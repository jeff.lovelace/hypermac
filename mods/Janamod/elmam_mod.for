      module ELMAM_mod
      character*80, allocatable :: DBText(:),DBWild(:)
      character*80 AtLocSyst(2),AtNeigh(4)
      character*8, allocatable :: DBLabel(:),DBLocSyst(:),
     1                            DBLocSystAtType(:,:)
      integer nSbwSelect,nLblLocSystem,nDB,iDB,iDBOld,NNeigh,NAtOffSet,
     1        NAtSel,iswELMAM,NAtActive,AtNeighN(4),nLblAtLocSyst(2)
      integer, allocatable :: DBLAsMax(:)
      real, allocatable :: DBPopVal(:),DBKappa(:,:),DBPopAs(:,:)
      end
