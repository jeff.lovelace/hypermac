      module DRIndex_mod
      character*256 FileIndex
      character*6 :: IndexWinfLabel(6) =
     1             (/'a    ','b    ','c    ',
     2               'alpha','beta ','gamma'/)
      integer, allocatable :: FlagS(:),FlagI(:),FlagK(:),FlagU(:),
     1         ProjMX(:),ProjMY(:),iha(:,:)
      integer :: GrIndexNAll,GrIndexColor,ProjNX,ProjNY,ProjNMaxX,
     1           ProjNMaxY,NCell,NCellMax,BasVec(4,25),OBasVec(25),
     2           MBasVec(25),NBasVec,IndexedSel,IndexedAll,
     3           IndexedSelSat,IndexedAllSat,NDimIndex,NTripl,MTripl,
     4           PorTripl(20),NIndSelTripl(20),GrIndexNSel,
     5           NRefForIndex=3000,NRefForTripl=20,
     6           MMin(3)=(/-1,-1,-1/),MMax(3)=(/1,1,1/),
     7           IndexCoorSystem=1,CrSystemRest=1,
     8           IH2d(3,2)=reshape((/0,0,0,0,0,0/),shape(IH2d)),
     9           GrIndexSuper(3)=(/1,1,1/)
      real, allocatable :: ProjX(:),ProjY(:),Int(:),Slozky(:,:),ha(:,:),
     1                     Uhel(:,:),Euler(:,:),Delka(:),SlozkyO(:,:),
     2                     IntO(:)
      real :: GrIndexDMax,GrIndexSMin,GrIndexSMax,GrIndexIMin,
     1        GrIndexIMax,GrIndexRad,CellDir(3,3),CellVolume,
     2        CellDirI(3,3),QPer(3),QBas(3),CellFor2d(6),Cell2d(3),
     3        XDirection(3),UBTripl(3,3,20),UBITripl(3,3,20),VTripl(20),
     4        ViewDirection(3),ViewMatrix(3,3),ViewMatrixI(3,3),
     5        CellTripl(6,20),VolMax=5000.,VolAMin=.1,DiffMax=0.01,
     6        CellParRest(6) = (/-1.,-1.,-1.,-1.,-1.,-1./)
      logical GrIndexUseSLim,GrIndexUseILim,LatticeOn,BasVecFlag(25),
     1        CellProjection,GrIndexSkipIndexed,Index2d
      logical :: AllowScalCell = .false.
      end
