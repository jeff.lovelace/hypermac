      module Dist_mod
      real, allocatable :: oi(:),oj(:),ok(:),xdst(:,:),xdi(:,:),
     1          xdj(:,:),xdk(:,:),sxdi(:,:),sxdj(:,:),sxdk(:,:),
     2          BetaDst(:,:),dxm(:),um(:,:),dum(:),dam(:),dums(:),
     3          dams(:),dumm(:),dhm(:,:),dhms(:,:),TypicalDistUse(:,:),
     4          DirRef(:,:)
      logical, allocatable :: BratPrvni(:),BratDruhyATreti(:)
      integer :: iselPrvni,iselDruhyATreti,NDirRef=0,NDirRefMax=0
      character*8, allocatable :: aselPrvni(:),aselDruhyATreti(:)
      logical :: CalcHBonds = (.false.)
      end
