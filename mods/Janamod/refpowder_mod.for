      module RefPowder_mod
      dimension ihPwdArr(:,:),KPhPwdArr(:),IQPwdArr(:),FCalcPwdArr(:),
     1          ypeaka(:,:),peaka(:,:),pcota(:,:),rdega(:,:,:),
     2          shifta(:,:),fnorma(:,:),tntsima(:,:),ntsima(:,:),
     3          sqsga(:,:),fwhma(:,:),sigpa(:,:),etaPwda(:,:),
     4          dedffga(:,:),dedffla(:,:),dfwdga(:,:),dfwdla(:,:),
     5          coef(:,:,:),coefp(:,:,:),coefq(:,:,:),Prof0(:),
     6          AxDivProfA(:,:,:),DerAxDivProfA(:,:,:,:),FDSProf(:,:,:),
     7          cotg2tha(:,:),cotgtha(:,:),cos2tha(:,:),cos2thqa(:,:),
     8          Alpha12a(:,:),Beta12a(:,:),IBroadHKLa(:)
      integer :: MxRefPwd
      real MultPwdArr(:)
      allocatable ihPwdArr,KPhPwdArr,IQPwdArr,MultPwdArr,FCalcPwdArr,
     1            ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,ntsima,
     2            sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,dfwdga,
     3            dfwdla,coef,coefp,coefq,Prof0,AxDivProfA,
     4            DerAxDivProfA,FDSProf,cotg2tha,cotgtha,cos2tha,
     5            cos2thqa,Alpha12a,Beta12a,IBroadHKLa
      end
