      module Contour_mod
      real :: CellParCon(6),CPCrit=(.0001),CPMaxStep=(.05),
     1        CPRhoMin=(0.),CPDmin= (0.), CPDmax=(2.),
     2        CPDGeom=(2.),teplota,toev,tpdfp,tpdfk,ntpdf,
     3        CutOffDist=(4.),fmzn,fmzp,cutpos,cutneg
      real, allocatable :: ActualMap(:),DenCore(:,:),DenVal(:,:),
     1                     DenCoreDer(:,:,:),DenValDer(:,:,:),
     2                     XfChd(:,:),XoChd(:,:),popasChd(:,:),Toll(:),
     3                     RCapture(:),rcla(:,:),xcpa(:,:),CPRho(:),
     4                     CPGrad(:),CPDiag(:,:),xpdf(:,:),
     5                     popaspdf(:,:),dpdf(:),fpdf(:),ypdf(:,:),
     6                     ContourArray(:)
      integer :: NActualMap,DrawPos,DrawNeg,NaChd,IPlaneOld=0,
     1           CPNIter = (40),ncp,npdf,MaxPDF,ContourType,
     2           ContourTypeErr,ContourNumber,ContourAIMOrder=0,
     3           IVarka,NZeroNeighAll,NumberOfPlanes,NumberOfPlanesMax
      integer, allocatable :: FlagMap(:),IaChd(:),IsChd(:),ireach(:),
     1                        iapdf(:),ispdf(:),ipor(:),idpdf(:)
      character*6, allocatable :: CPType(:)
      character*20, allocatable ::  LabelPlane(:),LabelPlaneO(:)
      character*20 LabelPlaneNew
      character*80, allocatable :: scpdf(:)
      logical :: OldNorm,NewAtoms = .false., NewPoints = .false.,
     1           ModifiedAtoms = .false.
      logical, allocatable :: atbrat(:),selpdf(:),BratAnharmPDF(:,:),
     1                        AtBratAnharm(:,:)
      character*30 :: MenDensity(7) = (/'total density            ',
     1                                  'valence density          ',
     2                                  'deformation density      ',
     3                                  'Laplacian-total density  ',
     4                                  'Laplacian-valence density',
     5                                  'inversed gradient vector ',
     6                                  'gradient vector          '/)
      end
