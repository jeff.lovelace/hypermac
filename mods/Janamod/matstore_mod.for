      module MatStore_mod
      integer :: NSymmStore(:),NSymmStoreO(:),
     1           NLattVecStore(:),NLattVecStoreO(:),
     2           NDimStore(:),NDimStoreO(:),
     3           NExtRefCondStore(:),NExtRefCondStoreO(:),
     4           NStore=0,NDimStoreMax=0,NLattVecStoreMax=0,
     5           NSymmStoreMax=0,NExtRefCondStoreMax=0
      real RM6Store(:,:,:),RM6StoreO(:,:,:),
     1      RMStore(:,:,:), RMStoreO(:,:,:),
     2     S6Store(:,:,:),S6StoreO(:,:,:),
     3     VT6Store(:,:,:),VT6StoreO(:,:,:),
     4     ProjStore(:,:,:),ProjStoreO(:,:,:),
     5     ExtRefGroupStore(:,:,:),ExtRefGroupStoreO(:,:,:),
     6     ExtRefCondStore(:,:,:),ExtRefCondStoreO(:,:,:),
     7     rmp(36),rms(36),xp(6),xq(6),xr(6)
      allocatable RM6Store,RM6StoreO,
     1             RMStore, RMStoreO,
     2            S6Store,S6StoreO,
     3            ProjStore,ProjStoreO,
     4            VT6Store,VT6StoreO,
     5            ExtRefGroupStore,ExtRefGroupStoreO,
     6            ExtRefCondStore,ExtRefCondStoreO,
     7            NSymmStore,NSymmStoreO,
     8            NLattVecStore,NLattVecStoreO,
     9            NDimStore,NDimStoreO,
     a            NExtRefCondStore,NExtRefCondStoreO
      end
