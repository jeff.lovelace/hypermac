      module Molec_mod
      character*27, allocatable :: LocMolSystSt(:,:,:),LocPGSystSt(:,:),
     1             StRefPoint(:)
      character*8, allocatable :: MolName(:),AtTrans(:),SmbPGMol(:)
      character*2, allocatable :: LocMolSystAx(:,:),LocPGSystAx(:)
      integer mxm,mxp,mxpm,mxdm,PrvniKiMol,PosledniKiMol,NMolec,KTLSMax,
     1        KModMMax(3)
      integer, allocatable :: iswmol(:),kswmol(:),ktls(:),RefPoint(:),
     2        KModM(:,:),KFM(:,:),kimol(:,:),
     3        LocMolSystType(:),PrvniKiMolekuly(:),DelkaKiMolekuly(:),
     4        RotSign(:),iam(:),mam(:),
     5        NPoint(:),KPoint(:),IPoint(:,:),TypeModFunMol(:)
      logical, allocatable :: UsePGSyst(:)
      real, allocatable :: aimol(:),xm(:,:),a0m(:),axm(:,:),aym(:,:),
     1     trans(:,:),utx(:,:,:),uty(:,:,:),
     2     euler(:,:),urx(:,:,:),ury(:,:,:),
     3     tt(:,:),ttx(:,:,:),tty(:,:,:),
     4     tl(:,:),tlx(:,:,:),tly(:,:,:),
     5     ts(:,:),tsx(:,:,:),tsy(:,:,:),
     6     phfm(:),
     7     saimol(:),sa0m(:),saxm(:,:),saym(:,:),
     8     strans(:,:),sutx(:,:,:),suty(:,:,:),
     9     seuler(:,:),surx(:,:,:),sury(:,:,:),
     a     stt(:,:),sttx(:,:,:),stty(:,:,:),
     1     stl(:,:),stlx(:,:,:),stly(:,:,:),
     2     sts(:,:),stsx(:,:,:),stsy(:,:,:),
     3     sphfm(:),
     4     RotMol(:,:),RotiMol(:,:),TrMol(:,:),
     5     TriMol(:,:),drotf(:,:),drotc(:,:),
     6     drotp(:,:),rotb(:,:),drotbf(:,:),
     7     drotbc(:,:),drotbp(:,:),durdx(:,:,:),
     8     rpoint(:,:,:),spoint(:,:,:),tpoint(:,:,:),TrPG(:,:),
     9     TriPG(:,:),LocPGSystX(:,:,:),LocMolSystX(:,:,:,:),
     a     OrthoX40Mol(:),OrthoDeltaMol(:),OrthoEpsMol(:)

! ---------------------------------------------------------------------

      integer :: NMolSplitMax=0,NMolSplit,MMolSplitMax=0
      integer, allocatable :: MMolSplit(:),IMolSplit(:,:)
      character*80, allocatable :: MolSplit(:,:)

      end
