      module Atoms_mod
      integer lasmaxm,ifrmax,itfmax,MagParMax,
     1        PrvniKiAtInd,PosledniKiAtInd,
     2        PrvniKiAtPos,PosledniKiAtPos,
     3        PrvniKiAtMol,PosledniKiAtMol,
     4        PrvniKiAtXYZMode,PosledniKiAtXYZMode,
     5        PrvniKiAtMagMode,PosledniKiAtMagMode,
     6        PrvniKi,
     7        NPolar,KModAMax(7),NAtAllocMod,
     8        NRenameAt,NRenameItems
      integer, allocatable :: isf(:),itf(:),lasmax(:),ifr(:),
     1        kmol(:),iswa(:),kswa(:),KFA(:,:),KModA(:,:),kmodao(:,:),
     2        MagPar(:),TypeModFun(:),KUsePolar(:),
     3        isa(:,:),isam(:,:),WyckoffMult(:),NPGAt(:),
     4        PrvniKiAtomu(:),DelkaKiAtomu(:),KiA(:,:)
      character*27, allocatable :: LocAtSystSt(:,:)
      character*8, allocatable ::  Atom(:),SmbPGAt(:),NamePolar(:),
     1             AtomRenameOld(:),
     1             AtomRenameNew(:,:)
      character*2, allocatable :: LocAtSystAx(:)
      character*1, allocatable :: LocAtSense(:),WyckoffSmb(:)
      real, allocatable :: ai(:),sai(:),qcnt(:,:),AtSiteMult(:),
     1     TrAt(:,:),TriAt(:,:),TroAt(:,:),TroiAt(:,:),
     2     a0(:),ax(:,:),ay(:,:),sa0(:),sax(:,:),say(:,:),
     3     x(:,:),ux(:,:,:),uy(:,:,:),sx(:,:),sux(:,:,:),suy(:,:,:),
     4     beta(:,:),bx(:,:,:),by(:,:,:),sbeta(:,:),sbx(:,:,:),
     5                                              sby(:,:,:),
     6     c3(:,:),c3x(:,:,:),c3y(:,:,:),sc3(:,:),sc3x(:,:,:),
     7                                            sc3y(:,:,:),
     8     c4(:,:),c4x(:,:,:),c4y(:,:,:),sc4(:,:),sc4x(:,:,:),
     9                                            sc4y(:,:,:),
     a     c5(:,:),c5x(:,:,:),c5y(:,:,:),sc5(:,:),sc5x(:,:,:),
     1                                            sc5y(:,:,:),
     2     c6(:,:),c6x(:,:,:),c6y(:,:,:),sc6(:,:),sc6x(:,:,:),
     3                                            sc6y(:,:,:),
     4     phf(:),xfr(:),sphf(:),sxfr(:),
     5     sm0(:,:),smx(:,:,:),smy(:,:,:),ssm0(:,:),ssmx(:,:,:),
     6     ssmy(:,:,:),RPGAt(:,:,:),
     7     kapa1(:),kapa2(:),skapa1(:),skapa2(:),
     8     popc(:),spopc(:),popv(:),spopv(:),popas(:,:),spopas(:,:),
     9     LocAtSystX(:,:,:)

! ---------------------------------------------------------------------

      real, allocatable :: OrthoX40(:),OrthoDelta(:),OrthoEps(:),
     1     OrthoMat(:,:),OrthoMatI(:,:)
      integer OrthoOrd
      integer, allocatable ::  OrthoSel(:,:)

! ---------------------------------------------------------------------

      real, allocatable :: tztl(:,:),tzts(:,:),durdr(:,:)

! ---------------------------------------------------------------------

      integer nor,mxo,mxda
      real,allocatable :: orx40(:),ordel(:),oreps(:)
      character*12, allocatable :: ora(:)
      logical SwitchedToHarm

! ---------------------------------------------------------------------

      logical XYZModeInAtoms,MagModeInAtoms
      character*8, allocatable :: LXYZAMode(:),LAtXYZMode(:,:),
     1            LMagAMode(:),LAtMagMode(:,:)
      integer NXYZAMode,NMagAMode,NAtXYZMode,NAtMagMode
      integer, allocatable :: MXYZAMode(:),MMagAMode(:),
     1        MAtXYZMode(:),NMAtXYZMode(:),IAtXYZMode(:,:),
     2        KiAtXYZMode(:,:),KAtXYZMode(:,:),JAtXYZMode(:),
     3        MAtMagMode(:),NMAtMagMode(:),IAtMagMode(:,:),
     4        KiAtMagMode(:,:),KAtMagMode(:,:),JAtMagMode(:)
      integer :: NXYZAModeMax = (0), MXYZAModeMax = (0),
     1           NAtXYZModeMax = (0),
     2           NMagAModeMax = (0), MMagAModeMax = (0),
     1           NAtMagModeMax = (0)
      real, allocatable ::  XYZAMode(:,:),MagAMode(:,:),
     1     AtXYZMode(:,:),SAtXYZMode(:,:),FAtXYZMode(:,:),
     2     AtMagMode(:,:),SAtMagMode(:,:),FAtMagMode(:,:)

! ---------------------------------------------------------------------

      integer :: NAtSplitMax=0,NAtSplit,MAtSplitMax=0
      integer, allocatable :: MAtSplit(:),IAtSplit(:,:)
      character*80, allocatable :: AtSplit(:,:)


      end
