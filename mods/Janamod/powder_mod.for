      module Powder_mod
      integer, allocatable :: PeakXPosI(:),PeakPor(:),YfPwd(:),
     1                        KPhArr(:),TMapTOF(:,:,:),ClckWdtTOF(:),
     2                        NPointsTimeMaps(:),KlicArr(:),indArr(:,:),
     3                        PhaseGr(:)
      integer :: KPhaseSolve = 0,ipointA = 0,iorderA = 0,
     1                           ipointI = 0,iorderI = 0, NIdM41 = 42,
     2           NTimeMaps,NPointsTimeMapsMax,BraggLabel = 0
      logical :: IncSpectNorm = .false.
      real :: TwoThetaLimits(3)=(/3.,177.,0.01/), XPwdMin(10),
     1        XPwdMax(10)
      real, allocatable :: XManBackg(:,:),YManBackg(:,:),MultArr(:),
     1                     LeBailPrfArr(:),ICalcArr(:),CalcPArr(:),
     2                     CalcPPArr(:),BkgArr(:),DifArr(:),BraggArr(:),
     3                     ShiftArr(:),FWHMArr(:),ypeakArr(:),
     4                     PeakXPos(:),PeakInt(:),PeakFWHM(:),
     5                     PeakBckg(:,:),PeakEta(:),RDeg1Arr(:),
     6                     RDeg2Arr(:),CutCoef1Arr(:),CutCoef2Arr(:),
     7                     XPwd(:),YoPwd(:),YcPwd(:),YbPwd(:),YsPwd(:),
     8                     YiPwd(:),YcPwdInd(:,:)
      character*10 :: IdM41(50) =
     1        (/'bckgtype ','bckgnum  ','manbckg  ','wtlebail ',
     2          'absor    ','mir      ','skipfrto ','phase    ',
     3          'proffun  ','asymm    ','partbroad','strain   ',
     4          'prefor   ','prefsum  ','cutoff   ','dirpref  ',
     5          'dirbroad ','maxsat   ','satfrmod ','skipfrdl ',
     6          'rough    ','datblock ','reflam   ','useinvx  ',
     7          'radprim  ','radsec   ','usersw   ','usefdsa  ',
     8          'usepsoll ','usessoll ','lamfile  ','cutoffmn ',
     9          'cutoffmx ','usetofab ','useds    ','illum    ',
     a          'focusBB  ','uklebail ','tofjason ','tofkey   ',
     1          'usehs    ','broadHKL ','#r43     ','#r44     ',
     2          '#r45     ','#r46     ','#r47     ','#r48     ',
     3          '#r49     ','#r50     '/)
      end
