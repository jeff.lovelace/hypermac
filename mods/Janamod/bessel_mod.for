      module Bessel_mod
      dimension besp(:,:),dbfdu1(:,:),dbfdu2(:,:),
     1          dchidu1(:),dchidu2(:),dsdu1(:),dsdu2(:),chi(:),
     2          dadu1(:),dadu2(:),dbdu1(:),dbdu2(:),dsdchi(:),
     3          sb(:),dsbdax(:),dsbday(:),ksi(:),dksidax(:),
     4          dksiday(:),dadax(:),daday(:),dbdax(:),dbday(:),
     5          besb(:,:),dbfdb1(:,:),dbfdb2(:,:),
     3          detadb1(:),detadb2(:),dsdb1(:),dsdb2(:),eta(:),
     4          dadb1(:),dadb2(:),dbdb1(:),dbdb2(:),dsdeta(:),
     5          dsdb3(:),dsdu3(:),dbfdb3(:,:),dbfdu3(:,:),
     6          daxddlt(:),dayddlt(:),daxdx40(:,:),daydx40(:,:),
     7          daxdfct(:,:),daydfct(:,:),mx(:),mxs(:),mxkk(:),
     8          mb(:),mbs(:),mbk(:),mxdkw1(:),mxdkw2(:),mxdkw3(:),
     9          mbdkw1(:),mbdkw2(:),mbdkw3(:),accur(:)
      real ksi
      allocatable besp,dbfdu1,dbfdu2,dchidu1,dchidu2,dsdu1,dsdu2,chi,
     1          dadu1,dadu2,dbdu1,dbdu2,dsdchi,sb,dsbdax,dsbday,ksi,
     2          dksidax,dksiday,dadax,daday,dbdax,dbday,besb,dbfdb1,
     3          dbfdb2,detadb1,detadb2,dsdb1,dsdb2,eta,dadb1,dadb2,
     4          dbdb1,dbdb2,dsdeta,dsdb3,dsdu3,dbfdb3,dbfdu3,daxddlt,
     5          dayddlt,daxdx40,daydx40,daxdfct,daydfct,mx,mxs,mxkk,
     6          mb,mbs,mbk,mxdkw1,mxdkw2,mxdkw3,mbdkw1,mbdkw2,mbdkw3,
     7          accur
      integer :: mxb = 50
      logical kolaps
      end
