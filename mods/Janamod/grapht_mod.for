      module Grapht_mod
      real, allocatable :: DrawT(:),DrawValue(:,:),DrawOcc(:,:),
     1                     DrawOccP(:,:),XMod(:)
      logical :: GrtDrawX4 = .false.,GrtShowCrenel = .false.,
     1           GrtPlaneDir = .false., GrtESD = .true.
      character*256 :: GrtPlane(2) = (/' ',' '/)
      character*256, allocatable :: DrawAtom(:,:)
      character*80, allocatable :: DrawSymSt(:),DrawSymStU(:),
     1                             AtomArr(:)
      character*8 :: SmbU(7) =
     1  (/'U11    ','U22    ','U33    ','Ueq    ',
     2    'U(max) ','U(mean)','U(min) '/)
      integer :: PocetParType=12,NAtPlane=0,NAtCenter=0,DrawN,
     1           DrawNMax=0,NAtomArr=0
      integer, allocatable  :: DrawColor(:),DrawParam(:)
      character*20  :: MenuParType(12) =
     1                 (/'Occupancy           ',
     2                   'Position            ',
     3                   'Displacement        ',
     4                   'Angles              ',
     5                   'Torsion angle       ',
     6                   'Plane               ',
     7                   'ADP parameter       ',
     8                   'Distances           ',
     9                   'Individual distances',
     a                   'Individual angles   ',
     1                   'Bond valence sum    ',
     2                   'Magnetic moment     '/)
      end
