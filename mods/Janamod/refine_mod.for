      module Refine_mod

C  ---------------------------------------------------------------------

      integer ICykl,ICyklMod6,mic,NCyklNew,nPowder,nSingle,ngrid(3),
     1        nxxn,nxxp(20),nFlowChart,NUseDatBlockActual,NRestDistAng,
     2        LSMethodNew
      logical, allocatable :: UseDatBlockActual(:)
      logical DelejStatistiku,CalcDer,Okraj,ContrExceedMessage,
     1        VisiMsgSc,VisiMsgRf,MakeItPermanent,RefSeriousWarnings
      real XdQuestRef,MarqLamNew,TlumNew
      integer :: ksimul = 1
      logical :: AnnouncedTwinProblem = .false.,
     1           FinalInver = .false., ExistMagnetic = .false.,
     2           RefKeyBoardInput=.false.
      character*256 :: FileSimul = ' '
      real, allocatable :: ScRes(:,:)


C  ---------------------------------------------------------------------

      double precision cosij,sinij,SumaYoPwd,SumaYcPwd
      real, allocatable :: yct(:,:),ycq(:),act(:,:),bct(:,:),afour(:,:),
     1                     bfour(:,:),snt(:),yctm(:,:),ycqm(:),
     2                     yctmp(:,:),ycqmp(:),F000(:,:),F000N(:,:)
      real fta,expij,ftacos,ftasin,cosijc,sinijc,Fobs,Fcalc,FcalcNucl,
     1     dy,dyp,wyoq,wdyq,a,b,affree,bffree,cosijm(3),sinijm(3),
     2     derme(12),afst,bfst,affreest,bffreest,sinthl,sinthlq,wt,yctw,
     3     yctwm,yctwmp,FCalcMag,FCalcMagPol,AMag(3),BMag(3),AMagR(3),
     4     BMagR(3),HMag(3,3),HMagR(3,3),RefDirCos(3,2),CReal,CImag,
     5     PhiAng,hj(3),HCoefj(84),phfp(3),XPwdDerTest,BroadHKL,
     6     UBRef(3,3),UBRefI(3,3)
      integer itw,itwr,iq,ihread(6),mj,nj,pj,mmabs(3),mmabsm,
     1        Maxmmabsm,ihref(6,3),HDerTest(6),DatBlockDerTest,
     2        IBroadHKL
      logical Nulova,HKLF5File,nicova,nic,Imag,MinusReal,MinusImag,
     1        atomic,nemod,okr(3),CorrScPwd

C  ---------------------------------------------------------------------

      character*137 RefHeader,RefHeaderMagPol
      character*128 ivenr(4)
      character*35 format3,format3p

C  ---------------------------------------------------------------------

      integer nfixpopv,npfixpopv

C  ---------------------------------------------------------------------

      real rxs,rxm,rxa(11)
      integer kxa(11),InvDel

C  ---------------------------------------------------------------------

      integer ihov(6),nchkr
      real difh(6),thdif(2),omdif(2),chidif(2)

C  ---------------------------------------------------------------------

      integer RefRandomSeed
      logical RefRandomize
      real    RefRandomDiff


C  ---------------------------------------------------------------------

      integer iab,ExtTensor(:),ExtType(:),ExtDistr(:)
      real aa(10),ba(10),dadmi(19),efpip(7),RefLam,extkor,extkorm,
     1     ExtRadius(:)
      allocatable ExtTensor,ExtType,ExtDistr,ExtRadius

C  ---------------------------------------------------------------------

      dimension ParSup(:),snw(:,:),csw(:,:),skfx1(:),skfx2(:),skfx3(:),
     1          x4low(:,:),x4length(:,:),XGauss(:,:,:),WGauss(:,:,:),
     2          snsinps(:), cssinps(:), sncosps(:),cscosps(:),
     3          snsinpsc(:),cssinpsc(:),sncospsc(:),cscospsc(:),
     4          rh(:,:),rk(:,:),rl(:,:),ath(:,:),atk(:,:),atl(:,:),t(:),
     5          imnp(:,:,:),HCoef(:,:,:),trezm(:,:),
     6          sngc(:,:,:),csgc(:,:,:),ScSup(:,:)
      integer PrvniParSup(:),DelkaParSup(:),NSuper(:,:,:)
      allocatable ParSup,snw,csw,skfx1,skfx2,skfx3,PrvniParSup,
     1            DelkaParSup,x4low,x4length,XGauss,WGauss,
     2            snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     3            sncospsc,cscospsc,ScSup,NSuper,sngc,csgc,
     4            rh,rk,rl,ath,atk,atl,t,imnp,HCoef,trezm


C  ---------------------------------------------------------------------

      dimension KFixOrigin(:)
      character*12 AtFixOrigin(:),AtFixX4(:),PromAtFixX4(:)
      character*5 ::  cfix(10) = (/'all  ','xyz  ','u    ','beta ',
     1                             'mod  ','chden','pol  ','x4   ',
     2                             'ind  ','value'/)
      integer NFixOrigin,NFixX4,KFixX4(:)
      logical RizikoX4
      allocatable KFixOrigin,AtFixOrigin,AtFixX4,PromAtFixX4,KFixX4

C  ---------------------------------------------------------------------

      real, allocatable :: par(:),ader(:),bder(:),aderm(:,:),bderm(:,:),
     1                     der(:),dc(:),derm(:,:),dcm(:,:),dertw(:),
     2                     dertwm(:),dcp(:),derpol(:,:),LSMat(:),
     3                     LSRS(:),LSSol(:)
      integer, allocatable :: NSingArr(:),ki(:)

C  ---------------------------------------------------------------------

      dimension dori(:,:)
      allocatable dori

C  ---------------------------------------------------------------------

      dimension isaRef(:,:),ismRef(:),CentrRef(:),NSymmRef(:),
     1          NCSymmRef(:),ZCSymmRef(:)
      allocatable isaRef,ismRef,CentrRef,NSymmRef,NCSymmRef,ZCSymmRef

C  ---------------------------------------------------------------------

      integer nvai
      integer :: nvaiMax = 0
      integer, allocatable :: nai(:,:),naixb(:),nails(:),mai(:)
      real, allocatable :: sumai(:)
      character*12, allocatable :: atvai(:,:),RestType(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: ihm(:,:),ihma(:,:),ihsn(:,:),ieqn(:),
     1                        imdn(:),ihsv(:,:),ieqv(:),imdv(:)
      integer :: NSkrtMax=0,NSkrt
      character*20, allocatable :: Skupina(:),Nevzit(:),Vzit(:)
      logical, allocatable ::  DontUse(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: iqs(:),ihms(:,:),ihmas(:,:),
     1        ihssn(:,:),ieqsn(:),imdsn(:),
     2        ihssv(:,:),ieqsv(:),imdsv(:)
      integer :: NScalesMax=0,NScales
      character*20, allocatable :: scsk(:),scvzit(:),scnevzit(:)

C  ---------------------------------------------------------------------

      integer, allocatable  :: RFactMatrix(:,:),RFactVector(:,:),
     1                         CondRFactIncl(:,:),AbsRFactIncl(:),
     2                         ModRFactIncl(:),CondRFactExcl(:,:),
     3                         AbsRFactExcl(:),ModRFactExcl(:),
     4                         nRPartAll(:),nRPartObs(:)
      integer :: NRFactorsMax = 0,NRFactors,nRPartAllAll,nRPartAllObs
      double precision, allocatable ::  RDenPartAll(:), RNumPartAll(:),
     1                                  RDenPartObs(:), RNumPartObs(:),
     2                                 wRDenPartAll(:),wRNumPartAll(:),
     3                                 wRDenPartObs(:),wRNumPartObs(:)
      double precision  RDenPartAllAll, RNumPartAllAll,
     1                  RDenPartAllObs, RNumPartAllObs,
     2                 wRDenPartAllAll,wRNumPartAllAll,
     3                 wRDenPartAllObs,wRNumPartAllObs
      character*20, allocatable :: StRFact(:),StRFactIncl(:),
     1                             StRFactExcl(:)

C  ---------------------------------------------------------------------

      integer, allocatable  :: nRZoneAll(:,:),nRZoneObs(:,:)
      double precision, allocatable ::
     1   RDenZoneAll(:,:), RNumZoneAll(:,:),
     2   RDenZoneObs(:,:), RNumZoneObs(:,:),
     3  wRDenZoneAll(:,:),wRNumZoneAll(:,:),
     4  wRDenZoneObs(:,:),wRNumZoneObs(:,:)

C  ---------------------------------------------------------------------

      character*80 WhatHasMaxChange(6)
      character*9 :: StRFac(6)=(/'R(obs)=  ','wR(obs)= ','wR2(obs)=',
     1                           'R(all)=  ','wR(all)= ','wR2(all)='/)
      double precision RNumObs(:), RNumAll(:), RDenObs(:), RDenAll(:),
     1                wRNumObs(:),wRNumAll(:),wRDenObs(:),wRDenAll(:),
     2                RINumObs, RINumAll, RIDenObs, RIDenAll,
     3               wRINumObs,wRINumAll,wRIDenObs,wRIDenAll,
     4                RNumProf,  RDenProf,wRNumProf,wRDenProf,
     5               cRDenProf,wcRDenProf,
     6                RNumOverall,RNumOverallObs,
     7                RDenOverall,RDenOverallObs,
     8                RFacOverall,RFacOverallObs,
     9                wRNumOverall,wRNumOverallObs,
     a                wRDenOverall,wRDenOverallObs,
     1                cRDenOverall,wcRDenOverall,
     2                 SumaDyQAll,SumaDyQQAll,SumaSigQAll,
     3                 SumaDyQObs,SumaDyQQObs,SumaSigQObs
      real  RFacObs(:,:,:,:),  RFacAll(:,:,:,:),
     1     wRFacObs(:,:,:,:), wRFacAll(:,:,:,:),
     2     RIFacObs(:,:,:),   RIFacAll(:,:,:),
     3     wRIFacObs(:,:,:),  wRIFacAll(:,:,:),
     4      RFacProf(:,:),     wRFacProf(:,:),
     5     cRFacProf(:,:),    wcRFacProf(:,:),
     6     eRFacProf(:),
     7     GOFProf(:,:),GOFObs(:,:),GOFAll(:,:),
     8     wRFacOverall,wRFacOverallObs,cRFacOverall,wcRFacOverall,
     9     eRFacOverall,GOFOverall,GOFOverAllObs,DampFac(6),
     a     ChngSUAve(6),ChngSUMax(6),wdyOld,wdyqOld,RLast(8),TlumOrigin
      integer nRObs(:),nRAll(:), nRFacObs(:,:,:,:), nRFacAll(:,:,:,:),
     1                          nRIFacObs(:,:,:),  nRIFacAll(:,:,:),
     2        nRFacProf(:),NSkipRef(:,:),NBadOverRef(:,:),
     3        KDatBlockUsed,NDynRed,nRLast,LstRef,LstSing,
     4        nRFacOverall,nRFacOverallObs,IStRFac(6)
      allocatable RNumObs, RNumAll, RDenObs, RDenAll,
     1            wRNumObs,wRNumAll,wRDenObs,wRDenAll,
     2            RFacObs,RFacAll,wRFacObs,wRFacAll,
     3            RIFacObs,RIFacAll,wRIFacObs,wRIFacAll,
     4            RFacProf,wRFacProf,cRFacProf,wcRFacProf,
     5           eRFacProf,GOFProf,GOFObs,GOFAll,nRObs,nRAll,nRFacObs,
     6           nRFacAll,nRIFacObs,nRIFacAll,nRFacProf,NSkipRef,
     7           NBadOverRef

C  ---------------------------------------------------------------------

      integer nLSRS,npsPwd,nLSMat,NSing,NPisSing,NParRef,nzz,noread,
     1        MaxDerAtom,LastNp,NConstrain,NRestrain,
     2        NParStr(:),NParData(:),NParPwd(:,:),NParPwdAll(:),
     3        NParStrAll(:),NDerNemodFirst,NDerNemodLast,
     4        NDerModFirst,NDerModLast,
     5        NDerMagFirst,NDerMagLast,NDerAll
      real    BerarS,BerarCorr
      logical JenSc
      allocatable NParStr,NParData,NParPwd,NParPwdAll,NParStrAll

C  ---------------------------------------------------------------------

      dimension snls(:,:,:),csls(:,:,:)
      allocatable snls,csls

C  ---------------------------------------------------------------------

      integer AtDisableNOld,AtDisableNNew
      real AtDisableFact(:),fyr(:)
      logical AtDisable(:)
      allocatable AtDisable,AtDisableFact,fyr

C  ---------------------------------------------------------------------

      dimension RMagDer(:,:,:)
      allocatable RMagDer

C  ---------------------------------------------------------------------

      character*8 ::  CKeepType(3) = (/'hydro','geom ','ADP  '/)
      character*8 ::  CKeepGeom(3) = (/'plane   ','rigid   ',
     1                                 'rigidmod'/)
      character*8 ::  CKeepHydro(3) =
     1                (/'tetrahed','triang  ','apical  '/)
      character*8 ::  CKeepADP(2) = (/'riding','      '/)
      character*80, allocatable :: KeepAt(:,:),KeepAtCentr(:),
     1                             KeepAtNeigh(:,:),KeepAtH(:,:),
     2                             KeepAtAnchor(:)
      integer :: NAtMax,NKeep,NKeepAt,lnda,NKeepMax=0,NKeepAtMax=0,
     1           NKeepRigidAll=0
      integer, allocatable :: KeepType(:),KeepN(:),KeepNAt(:,:),
     1                        KeepKiX(:,:),KeepNAtCentr(:),
     2                        KeepKiXCentr(:),KeepNNeigh(:),
     3                        KeepNAtNeigh(:,:),KeepKiXNeigh(:,:),
     4                        KeepNH(:),KeepNAtH(:,:),KeepKiXH(:,:),
     5                        KeepNAtAnchor(:),KeepKiXAnchor(:),
     6                        NKeepRigid(:)
      logical UseAnchor
      real :: KeepDistHDefault(3)=(/-1.,-1.,-1./)
      real, allocatable :: KeepDistH(:),KeepAngleH(:),KeepADPExtFac(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: MagneticRFac(:)

C  ---------------------------------------------------------------------

      character*12 :: lk1 = 'kappa',lk2 = 'kappa''',lTOFAbsPwd='TOFAbs',
     1                lPrefPwd = 'pref', lRoughPwd = 'rough',
     2                lBackgPwd = 'bckg',lLamPwd = 'WLen',lStPwd='St',
     3                lZetaPwd='Zeta',lAsymPwd='asym',
     4                lBroadHKL='BroadHKL'
      character*12 :: lBasicPar(12) = (/'ai    ','x     ','y     ',
     1                                  'z     ','phi   ','chi   ',
     2                                  'psi   ','Uiso  ','RhoIso',
     3                                  'GIso  ','Phason','o     '/)
      character*12 :: lcell(6) = (/'a    ','b    ','c    ','alpha',
     1                             'beta ','gamma'/)
      character*12 :: lShiftPwd(3) = (/'shift','sycos','sysin'/)
      character*12 :: lTOF1Pwd(3) = (/'zero','difc','difa'/)
      character*12 :: lTOF2Pwd(8) = (/'ZeroT ','CT    ','AT    ',
     1                                'ZeroE ','CE    ',
     2                                'Wcross','Tcross','nic   '/)
      character*12 :: lGaussPwd(4) = (/'GU','GV','GW','GP'/)
      character*12 :: lGaussPwdF(4) = (/'CSizeG  ','CSizeGA ',
     1                                 'StrainG ','StrainGA'/)
      character*12 :: lGaussPwdTOF(3) = (/'Sig0','Sig1','Sig2'/)
      character*12 :: lLorentzPwd(5) = (/'LX ','LXe','LY ','LYe','   '/)
      character*12 :: lLorentzPwdF(5) = (/'CSizeL  ','CSizeLA ',
     1                                   'StrainL ','StrainLA',
     2                                   '        '/)
      character*12 :: lLorentzPwdTOF(5) = (/'Gam0 ','Gam1 ','Gam2 ',
     4                                      'Gam1e','Gam2e'/)
      character*12 :: lAsymPwdD(4) = (/'H/L  ','S/L  ','HpS/L','HmS/L'/)
      character*12 :: lAsymPwdF(8) = (/'RSW  ','FDSA ','XLen ','SLen ',
     1                                'RLen ','PSoll','SSoll','VDSL '/)
      character*12 :: lAsymTOF1Pwd(4)= (/'alpha0','alpha1','beta0 ',
     1                                   'beta1 '/)
      character*12 :: lAsymTOF2Pwd(8)= (/'alpha0e','alpha1e','beta0e ',
     1                                   'beta1e ',
     2                                   'alpha0t','alpha1t','beta0t ',
     3                                   'beta1t '/)
      character*12 :: lAsymPwdDI(6) = (/'RelBWidth ',
     1                                  'PixSize   ',
     2                                  'BeamHeight',
     3                                  'CapDiam   ',
     4                                  'BeamDiv   ',
     5                                  'RadS2I    '/)
      character*12 :: lEDVar(6) = (/'EDScale',
     1                              'EDThick',
     2                              'EDXNorm',
     3                              'EDYNorm',
     4                              'EDPhi  ',
     5                              'EDTheta'/)
      end
