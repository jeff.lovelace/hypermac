      module RepAnal_mod
      integer, allocatable :: NSymmEpi(:),NEqvEpi(:),NBasFun(:),
     1                        NDimIrrep(:),MultIrrep(:),IGen(:),KGen(:),
     2                        IoGen(:,:),KSymmLG(:),TakeEpi(:),
     3                        NCondKer(:),PerTab(:,:),NCondEpi(:),
     4                        NCondEpiAll(:),KIrrepEpi(:),NFamEpi(:),
     5                        FamEpi(:,:),EpiFam(:),NSymmFam(:),
     6                        OrdFam(:),OrdEpi(:),PerTabOrg(:,:),
     7                        SymmOrdOrg(:),SymmPorOrg(:),NGrpEpi(:),
     8                        NGrpKer(:),BratSub(:,:),NBratSub(:),
     9                        IPorSub(:),GenSub(:,:),NGenSub(:),
     a                        TakeSub(:),NEqAEpi(:),NEqPEpi(:),
     1                        NEqEpi(:),IGenLGConj(:,:),IGenLG(:),
     2                        SymmMagMult(:),NSymmEpiComm(:),
     3                        NLattVecEpiComm(:),
     4                        NGrpEpiO(:),KIrrepEpiO(:),NEqEpiO(:),
     5                        NSymmEpiO(:),NEqAEpiO(:),NEqPEpiO(:),
     6                        TakeEpiO(:),NCondEpiO(:),NCondEpiAllO(:)
      integer MaxBasFun,NIrrep,TriN,NSymmBlock,NSymmLG,NSymmS,NGrupaS,
     1        NGen,invcOrg,NEpi,NEpiMax,NPackArr(10),
     2        NFam,nSbwEpikernels,SelFamEpi,NTwinS,EpiSelFr,EpiSelTo,
     3        FamSel,CrSystemS,NSub,NSubMax,nSbwGroup,nSbwAtoms,
     4        NGrpMenuLast,nLblAtom,NAtomMenuLast,NSymmEpiCommMax,
     5        EpiSel,NLattVecEpiCommMax
      real TrMatSuperCell(3,3)
      real, allocatable :: EqAEpi(:,:),EqPEpi(:,:),EqPPEpi(:,:),
     1                     xyz(:,:),rm6r(:,:),rmr(:,:),s6r(:,:),
     2                     ZMagR(:),s6Ker(:,:,:),ZMagKer(:,:),
     3                     s6Epi(:,:,:),ZMagEpi(:,:),TrMatKer(:,:,:),
     4                     TrMatEpi(:,:,:),ShSgKer(:,:),ShSgEpi(:,:),
     5                     ZMagGen(:,:),s6Gen(:,:,:),PhaseX4(:),
     6                     rmEpiComm(:,:,:),s6EpiComm(:,:,:),
     7                     ZMagEpiComm(:,:),vt6EpiComm(:,:,:),
     8                     s6EpiO(:,:,:),ZMagEpiO(:,:),TrMatEpiO(:,:,:),
     9                     ShSgEpiO(:,:),EqAEpiO(:,:),EqPEpiO(:,:),
     a                     EqPPEpiO(:,:)
      complex, allocatable :: Rep(:,:,:,:),RIrrep(:,:,:),RIrrepP(:,:,:),
     1                        TraceIrrep(:,:),BasFun(:,:,:),EqEpi(:,:),
     2                        BasFunOrg(:,:,:),CondKer(:,:,:),
     3                        CondEpi(:,:,:),CondEpiAll(:,:,:),
     4                        GenLGConjDiff(:,:),EigenVecIrrep(:,:,:),
     5                        EigenValIrrep(:,:,:),EqEpiO(:,:),
     6                        CondEpiO(:,:,:),CondEpiAllO(:,:,:)
      character*80, allocatable :: GrpKer(:),GrpEpi(:),GrpMenu(:),
     1                             GrpEpiO(:)
      character*80 GrupaS
      character*20, allocatable :: StGen(:),SmbIrrep(:),SymmCodes(:)
      logical, allocatable :: BratSymmKer(:,:),BratSymmEpi(:,:),
     1                        AddSymm(:),AddSymmOrg(:),BratSymmEpiO(:,:)
      logical QPul,FromRepAnalysis,FromGoToSub
      end
