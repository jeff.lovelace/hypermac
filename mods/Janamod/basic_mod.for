      module Basic_mod


!  ---------------------------------------------------------------------

      dimension rmgc(:,:,:,:),rm6gc(:,:,:,:),s6gc(:,:,:,:),
     1          KwSymGC(:,:,:,:),rtgc(:,:,:,:),rc3gc(:,:,:,:),
     2          rc4gc(:,:,:,:),rc5gc(:,:,:,:),rc6gc(:,:,:,:),
     3          iswGC(:,:,:),RMagGC(:,:,:,:),ZMagGC(:,:,:)
      integer GammaIntGC(:,:,:,:)
      logical SwitchedToComm
      allocatable rmgc,rm6gc,s6gc,KwSymGC,GammaIntGC,rtgc,rc3gc,rc4gc,
     1            rc5gc,rc6gc,iswGC,RMagGC,ZMagGC
!  ---------------------------------------------------------------------

      integer, allocatable :: KwSym(:,:,:,:)
      integer :: NSatGroups=0,HSatGroups(1:3,0:19)

!  ---------------------------------------------------------------------

      real, allocatable :: rm6(:,:,:,:),rm(:,:,:,:),s6(:,:,:,:),
     1                     vt6(:,:,:,:),rmag(:,:,:,:),zmag(:,:,:)
      integer, allocatable :: ISwSymm(:,:,:),InvMag(:),ISymmMolec(:),
     1                        CenterMag(:)
      integer ISymmBasic, ISymmMolecMax
      character*80, allocatable :: StSymmCard(:)
      character*30, allocatable :: symmc(:,:,:,:)
      logical, allocatable :: BratSymm(:,:)

!  ---------------------------------------------------------------------

      dimension rm6l(:,:,:,:),rml(:,:,:,:),s6l(:,:,:,:),ISwSymmL(:,:,:),
     1          KwSymL(:,:,:,:),rtl(:,:,:,:),rc3l(:,:,:,:),
     2          rc4l(:,:,:,:),rc5l(:,:,:,:),rc6l(:,:,:,:),
     3          XYZMode(:,:,:)
      character*8 LXYZMode(:,:)
      integer GammaIntL(:,:,:,:),NXYZMode(:),MXYZMode(:)
      allocatable rm6l,s6l,rml,ISwSymmL,KwSymL,rtl,rc3l,rc4l,rc5l,rc6l,
     1            GammaIntL,XYZMode,LXYZMode,NXYZMode,MXYZMode

!  ---------------------------------------------------------------------

      real, allocatable :: AtWeight(:,:),AtRadius(:,:),AtNum(:,:),
     1                     AtMult(:,:),
     2                     FFBasic(:,:,:),FFCore(:,:,:),FFVal(:,:,:),
     3                     FFCoreD(:,:,:),FFValD(:,:,:),
     4                     FFra(:,:,:),FFia(:,:,:),FFrRef(:,:,:),
     5                     FFiRef(:,:,:),sFFrRef(:,:,:),sFFiRef(:,:,:),
     6                     FFn(:,:),FFni(:,:),FFMag(:,:,:),FFEl(:,:,:),
     7                     FFa(:,:,:,:),FFae(:,:,:,:),fx(:),fm(:),
     8                     ZSlater(:,:,:),HZSlater(:,:),
     9                     TypicalDist(:,:,:),DTypicalDist(:,:),
     a                     ZSTOA(:,:,:,:,:),CSTOA(:,:,:,:,:)
      integer :: NTypicalDistMax = (0)
      integer, allocatable :: NTypicalDist(:),PopCore(:,:,:),
     1                        PopVal(:,:,:),HNSlater(:,:),TypeCore(:,:),
     2                        TypeVal(:,:),AtVal(:,:),AtColor(:,:),
     3                        NSlater(:,:,:),NSTOA(:,:,:,:,:),
     4                        NCoefSTOA(:,:,:,:),TypeFFMag(:,:)
      character*256, allocatable :: CoreValSource(:,:)
      character*7, allocatable :: AtTypeFull(:,:),AtTypeMag(:,:),
     1                            AtTypeMenu(:),AtTypeMagJ(:,:)
      character*2, allocatable :: AtType(:,:),AtTypicalDist(:,:,:)

!  ---------------------------------------------------------------------

      parameter (nCIFAtX=14,nCIFAtT=8,nCIFAtSM=4,nCIFAtXM=5,nCIFAtTM=5,
     1           nCIFAtCM=5,nCIFPwdProf=5,nCIFPwdTOF=5,nCIFSingleRfl=4,
     2           nCIFElectronRfl=5,
     3           nCIFPwdFit=15,nCIFSingleFit=18,nCIFHBonds=11,
     4           nCIFRestDist=6,nCIFRestAngle=8,nCIFRestTorsion=10,
     5           nCIFRestEqDist=6,nCIFRestEqAngle=8,
     6           nCIFRestEqTorsion=10)
      integer :: CIFAtX(nCIFAtX) =
     1           (/33,46,30,31,32,1,48,106,41,21,42,28,29,52/)
      integer :: CIFAtT(nCIFAtT) = (/8,10,11,14,16,12,13,15/)
      integer :: CIFAtSM(nCIFAtSM) = (/68,70,71,75/)
      integer :: CIFAtXM(nCIFAtXM) = (/54,55,57,58,62/)
      integer :: CIFAtTM(nCIFAtTM) = (/88,90,91,92,96/)
      integer :: CIFAtCM(nCIFAtCM) = (/164,166,167,168,172/)
      integer :: CIFPwdProf(nCIFPwdProf) = (/180,114,130,148,147/)
      integer :: CIFPwdTOF(nCIFPwdProf) = (/180,118,130,148,147/)
      integer :: CIFSingleRfl(nCIFSingleRfl) = (/9,10,11,20/)
      integer :: CIFElectronRfl(nCIFElectronRfl) = (/16,17,18,24,20/)
      integer :: CIFPwdFit(nCIFPwdFit) =
     1           (/23,38,22,37,12,19,36,35,32,33,1,2,11,18,21/)
      integer :: CIFSingleFit(nCIFSingleFit) =
     1           (/23,38,22,40,15,13,20,19,36,35,32,33,1,2,11,18,21,34/)
      integer :: CIFHBonds(nCIFHBonds) =
     1           (/23,24,25,30,31,32,26,27,28,22,29/)
      integer :: CIFRestDist(nCIFRestDist) =
     1           (/12,16,13,17,18,19/)
      integer :: CIFRestAngle(nCIFRestAngle) =
     1           (/2,7,3,8,4,9,10,11/)
      integer :: CIFRestTorsion(nCIFRestTorsion) =
     1           (/104,110,105,111,106,112,107,113,114,115/)
      integer :: CIFRestEqDist(nCIFRestEqDist) =
     1           (/47,51,48,52,57,58/)
      integer :: CIFRestEqAngle(nCIFRestEqAngle) =
     1           (/33,38,34,39,35,40,45,46/)
      integer :: CIFRestEqTorsion(nCIFRestEqTorsion) =
     1           (/59,65,60,66,61,67,62,68,73,74/)
      integer CIFFlag
      character*80  CIFKey(:,:)
      character*128 CIFArray(:)
      character*256 CIFLastReadRecord,CIFArrayUpdate(:)
      integer CIFKeyFlag(:,:),nCIFArray,nCIFUsed,nCIFArrayUpdate
      logical :: CIFNewFourierWaves = .true.
      allocatable CIFKey,CIFKeyFlag,CIFArray,CIFArrayUpdate

!  ---------------------------------------------------------------------

      real RMatCalc(36,0:6)
      integer NRowCalc(0:6),NColCalc(0:6)

!  ---------------------------------------------------------------------

      integer      neq,neqs,mxe,mxep,lnp(:),lnpo(:),pnp(:,:),npa(:)
      real         pko(:,:),pab(:)
      character*20 lat(:),pat(:,:)
      character*12 lpa(:),ppa(:,:)
      logical      eqhar(:)
      allocatable lnp,lnpo,pnp,npa,pko,pab,lat,pat,lpa,ppa,eqhar

!  ---------------------------------------------------------------------

      character*12 :: IdM50(72) =
     1   (/'title       ','cell        ','ndim        ','ncomp       ',
     2     'qi          ','qr          ','wmatrix     ','commen      ',
     3     'tzero       ','centro      ','symmetry    ','lattice     ',
     4     'lattvec     ','spgroup     ','unitsnumb   ','sgshift     ',
     5     'formtab     ','esdcell     ','densities   ','atom        ',
     6     'atmag       ','localsymm   ','twin        ','refofanom   ',
     7     'chemform    ','roundmethod ','wavefile    ','phase       ',
     8     'phasetwin   ','magnetic    ','lambda      ','radtype     ',
     9     'lpfactor    ','nalpha      ','monangle    ','kalpha1     ',
     a     'kalpha2     ','kalphar     ','perfmono    ','slimits     ',
     1     'flimits     ','powder      ','noofref     ','datcolltemp ',
     2     'qmag        ','parentstr   ','typdist     ','xyzmode     ',
     3     'atmagj      ','#r50        ',
     4     'atradius    ','fneutron    ','fneutronim  ','FFBasic     ',
     5     'FFCore      ','FFVal       ','nslater     ','DzSlater    ',
     6     'ZSlater     ','color       ','f''          ','f"          ',
     7     'FFMag       ','atweight    ','formula     ','fffree      ',
     8     'dmax        ','formtab     ','alphagmono  ','betagmono   ',
     9     'wffile      ','stofunction '/)
      character*80  :: MenuWF(7) = (/'wavefj.dat          ',
     1                               'wavefn.dat          ',
     2                               'wavefc.dat          ',
     3                               'xd.bnk_RHF_CR       ',
     4                               'xd.bnk_RHF_BBB      ',
     5                               'xd.bnk_RDF_SCM      ',
     6                               'xd.bnk_PBE-QZ4P-ZORA'/)

C  ---------------------------------------------------------------------

      dimension XSavedPoint(:,:)
      character*80 StSavedPoint(:)
      integer :: NSavedPoints = 0, MaxSavedPoints = 0
      allocatable XSavedPoint,StSavedPoint

C  ---------------------------------------------------------------------

      dimension ihmin(6),ihmax(6)

C  ---------------------------------------------------------------------

      character*35  :: SpecMatrixName(14) =
     1             (/'from#F#to#P              ',
     2               'from#R obverse#to#P#(1)  ',
     3               'from#R obverse#to#P#(2)  ',
     4               'from#R obverse#to#P#(3)  ',
     5               'from#R reverse#to#P#(1)  ',
     6               'from#R reverse#to#P#(2)  ',
     7               'from#R reverse#to#P#(3)  ',
     8               'from#P#to#F              ',
     9               'from#P#to#R obverse#(1)  ',
     a               'from#P#to#R obverse#(2)  ',
     1               'from#P#to#R obverse#(3)  ',
     2               'from#P#to#R reverse#(1)  ',
     3               'from#P#to#R reverse#(2)  ',
     4               'from#P#to#R reverse#(3)  '/)
      real :: SpecMatrix(9,14) = reshape
     1    ((/0.,.5,.5,.5, 0.,.5,.5,.5,0.,
     2        .666667, .333333, .333333,
     2       -.333333, .333333, .333333,
     2       -.333333,-.666667, .333333,
     3       -.333333,-.666667, .333333,
     3        .666667, .333333, .333333,
     3       -.333333, .333333, .333333,
     4       -.333333, .333333, .333333,
     4       -.333333,-.666667, .333333,
     4        .666667, .333333, .333333,
     5       -.666667,-.333333, .333333,
     5        .333333,-.333333, .333333,
     5        .333333, .666667, .333333,
     6        .333333, .666667, .333333,
     6       -.666667,-.333333, .333333,
     6        .333333,-.333333, .333333,
     7        .333333,-.333333, .333333,
     7        .333333, .666667, .333333,
     7       -.666667,-.333333, .333333,
     8       -1., 1., 1., 1.,-1., 1., 1., 1., 1.,
     9        1.,-1., 0., 0., 1.,-1., 1., 1., 1.,
     a        0., 1.,-1.,-1., 0., 1., 1., 1., 1.,
     1       -1., 0., 1., 1.,-1., 0., 1., 1., 1.,
     2       -1., 1., 0., 0.,-1., 1., 1., 1., 1.,
     3        0.,-1., 1., 1., 0.,-1., 1., 1., 1.,
     4        1., 0.,-1.,-1., 1., 0., 1., 1., 1./),
     5         shape(SpecMatrix))

C  ---------------------------------------------------------------------

      character*44 :: MapType(10) = (/
     1             'F(obs)**2 - Patterson                      ',
     2             'F(calc)**2 - checking Patterson            ',
     3             'F(obs)**2-F(calc)**2 - difference Patterson',
     4             'F(obs) - Fourier                           ',
     5             'F(calc) - checking Fourier                 ',
     6             'F(obs)-F(calc) - difference Fourier        ',
     7             'dynamic multipole deformation map          ',
     8             'static multipole deformation map           ',
     9             '0/1 - shape function                       ',
     a             'Difference between two Fourier maps        '/)

C  ---------------------------------------------------------------------

      parameter (NMon=4)
      character*10 :: MonName(NMon) =
     1                (/'Si      ',
     2                  'Ge      ',
     3                  'Graphite',
     4                  'Other   '/)
      real :: MonCell(6,NMon) = reshape
     1        ((/5.431,5.431,5.431,90.,90.,90.,
     2           5.657,5.657,5.657,90.,90.,90.,
     3           2.464,2.464,6.7079,90.,90.,120.,
     4           5.43,5.43,5.43,90.,90.,90./),
     5           shape(MonCell))
      integer :: MonH(3,NMon) = reshape
     1        ((/1,1,1,1,1,1,0,0,2,1,0,0/),shape(MonH)),
     2        MonSel=3

C  ---------------------------------------------------------------------

      character*256 :: CallSIR(6) = (/' ',' ',' ',' ',' ',' '/),
     1                 DirSIR(6)  = (/' ',' ',' ',' ',' ',' '/)
      character*4   :: SIRName(6) = (/'97  ','2002','2004','2008',
     1                                '2011','2014'/)
      integer       :: SIRDefined(6) = (/0,0,0,0,0,0/)


      character*256 :: CallExpo(5) = (/' ',' ',' ',' ',' '/),
     1                 DirExpo(5)  = (/' ',' ',' ',' ',' '/)
      character*4   :: ExpoName(5) = (/'----','2004','2009','2013',
     1                                 '2014'/)
      integer       :: ExpoDefined(5) = (/0,0,0,0,0/)

      character*256 :: DCREDFile=' ',CallGeminography=' ',
     1                 CallShelxt=' ',CommandLineShelxt=' ',XDBnkDir=' '

C  ---------------------------------------------------------------------

      real :: AvogadroNumber=6.02214129E23

C  ---------------------------------------------------------------------

      character*256, allocatable :: CyclicRefFile(:)
      character*256 :: CyclicRefMasterFile=' '
      character*8 CyclicRefTypeUnits
      integer :: ICyclicRefFile=1,NCyclicRefFile,CyclicRefType,
     1           CyclicRefTypeTemp=1,CyclicRefTypePress=2,
     2           CyclicRefTypeTime=2
      integer, allocatable :: CyclicRefStart(:),CyclicRefUse(:),
     1                        CyclicRefUpDown(:),CyclicRefOrder(:)
      logical :: CyclicRefMode=.false.,CyclicBack=.false.,
     1           CyclicWizardHotovo=.false.
      real, allocatable :: CyclicRefParam(:)
      end
