module hm_sl
  use hm_nl
  implicit none
  private

  type :: hm_node
    character(len=1),dimension(:),pointer :: text
    type(hm_node), pointer :: next
  end type hm_node

  type :: hm_stringlist 
    type(hm_node),pointer :: head
    type(hm_node),pointer :: current
    type(hm_node),pointer :: last
    integer :: index
    integer :: count
    integer :: max
  end type hm_stringlist

  public hm_stringlist, hm_sl_initialize, hm_sl_add, &
         hm_sl_string_at, hm_sl_free_memory

  contains

    function hm_sl_a2s(a)  result (s)    ! copy char array to string
      character,intent(in) :: a(:)
      character(size(a)) :: s
      integer :: i
      do i = 1,size(a)
        s(i:i) = a(i)
      end do
    end function hm_sl_a2s

    subroutine hm_sl_initialize(sl_pointer)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      allocate(sl_pointer)
      nullify(sl_pointer%head)
      nullify(sl_pointer%current)
      nullify(sl_pointer%last)
      sl_pointer%count=0
      sl_pointer%index=0
      sl_pointer%max=0
    end subroutine hm_sl_initialize

    subroutine hm_sl_add(sl_pointer,astring)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      character(*),intent(in) :: astring
      character(len=:),allocatable :: temp
      integer :: i
      type(hm_node),pointer :: anode
      allocate(anode)
      nullify(anode%next)
      if (associated(sl_pointer%head)) then
        sl_pointer%last=>anode
        sl_pointer%current%next=>anode
        sl_pointer%current=>anode
        allocate(anode%text(len(astring)))
        do i=1, len(astring)
          anode%text(i)=astring(i:i)
        end do
        sl_pointer%count=sl_pointer%count+1
        sl_pointer%index=sl_pointer%count
        if (sl_pointer%max.lt.len(astring)) then
          sl_pointer%max=len(astring)
        end if
      else
        sl_pointer%last=>anode
        sl_pointer%current=>anode
        sl_pointer%head=>anode
        allocate(anode%text(len(astring)))
        do i=1, len(astring)
          anode%text(i)=astring(i:i)
        end do
        sl_pointer%max=len(astring)
        sl_pointer%count=1
        sl_pointer%index=1
      end if
      allocate(character(len=size(sl_pointer%current%text))::temp)
      temp=hm_sl_a2s(sl_pointer%current%text)
      nullify(anode)
    end subroutine hm_sl_add

    function hm_sl_get_current(sl_pointer) result(astring)
      implicit none
      type(hm_stringlist),pointer,intent(in) :: sl_pointer
      character(len=:),allocatable :: astring
      if (associated(sl_pointer%current)) then
        if (associated(sl_pointer%current%text)) then
          allocate(character(len=size(sl_pointer%current%text))::astring)
          astring=hm_sl_a2s(sl_pointer%current%text)
        else
          allocate(character(len=0)::astring)
          astring=""
        endif
      else
        allocate(character(len=0)::astring)
        astring=""
      endif
    end function hm_sl_get_current

    subroutine hm_sl_reset(sl_pointer)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      sl_pointer%index=1
      if (associated(sl_pointer%head)) then
         sl_pointer%current=>sl_pointer%head
         sl_pointer%index=1
      else
         nullify(sl_pointer%current)
         sl_pointer%index=0
      end if
    end subroutine hm_sl_reset

    subroutine hm_sl_offset(sl_pointer,anoffset)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      integer,intent(in) :: anoffset
      integer :: offset
      offset=anoffset
      if (sl_pointer%count.eq.0) then
        write(*,*) "Error: The StringList is empty and can not be&
                   & offset to: ",anoffset
        stop
      end if
      if (anoffset.lt.1) then
        write(*,*) "Error: StringList min offset is 1 not: ",anoffset
        stop
      end if
      if (anoffset.gt.sl_pointer%count) then
        write(*,*) "Error: StringList max offset is ",sl_pointer%count, &
                   " and offest ",anoffset," is too much."
      end if

      if (offset.ne.sl_pointer%index) then
        call hm_sl_reset(sl_pointer)
        do while (offset.ne.sl_pointer%index)
          call hm_sl_next(sl_pointer)
        end do
      end if
    end subroutine hm_sl_offset

    subroutine hm_sl_next(sl_pointer)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      sl_pointer%current=>sl_pointer%current%next
      sl_pointer%index=sl_pointer%index+1
    end subroutine hm_sl_next

    function hm_sl_string_at(sl_pointer,anindex) result(astring)
      implicit none
      integer,intent(in) :: anindex
      character(len=:),allocatable :: astring
      character(len=:),allocatable :: temp
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      call hm_sl_offset(sl_pointer,anindex)
      allocate(character(len=sl_pointer%max) :: temp)
      temp = hm_sl_get_current(sl_pointer)
      allocate(character(len=len(temp)) :: astring)
      astring=trim(temp)
      if (len(astring).eq.0) then
        astring=" "
      endif
    end function hm_sl_string_at

    subroutine hm_sl_free_memory(sl_pointer)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      type(hm_node),pointer :: tmp1
      type(hm_node),pointer :: tmp2
      INTEGER :: DeAllocateStatus
      nullify(tmp1)
      nullify(tmp2)
      tmp1=>sl_pointer%head
      if (associated(tmp1)) then
        if (associated(tmp1%next)) then
          tmp2=>tmp1%next
        end if
      end if 
      nullify(sl_pointer%head)
      nullify(sl_pointer%current)
      nullify(sl_pointer%last)
      do while (associated(tmp1))
        if (associated(tmp1%text)) then
          deallocate(tmp1%text,STAT=DeAllocateStatus)
          nullify(tmp1%text)
        endif
        deallocate(tmp1,STAT=DeAllocateStatus)
        nullify(tmp1)
        if(associated(tmp2)) then
          tmp1=>tmp2
          if (associated(tmp1%next)) then
            tmp2=>tmp1%next
          else 
            nullify(tmp2)
          end if
        endif
      enddo
      deallocate(sl_pointer,STAT=DeAllocateStatus)
      nullify(sl_pointer)
    end subroutine hm_sl_free_memory
    
end module hm_sl
