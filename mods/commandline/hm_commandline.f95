module hm_commandline
  use hm_nl
  use hm_sl
  use hm_stream
  use hm_io
  implicit none
  type, private :: hm_cl_option
    character(len=32) :: name
    logical :: enabled
    logical :: default
    character(len=32) :: tui_name
    type(hm_stringlist),pointer :: description
    character(len=4096) :: option_string
    integer :: option_int
    real :: option_real
  end type hm_cl_option

  logical,private,save :: hm_cl_initialized=.false.

  type(hm_stringlist),pointer,private,save :: hm_description

  type(hm_cl_option),save :: commandfile
  
  type(hm_cl_option),private,save :: verboselevel

  type(hm_cl_option),private,save :: logfile

  type(hm_cl_option),private,save :: enablelogfile

  type(hm_cl_option),private,save :: commandlinehelp

  type(hm_cl_option),save :: hdf5file

  character(len=*),parameter :: hm_cl_exe_str=&
    &"hypermac [-h] [-v <level>] [-l] [-lf <filename>] [-c <commandfilename>]&
    & [-hdf5 <hdf5filename>]"
  private :: hm_cl_initialize

  contains
    subroutine hm_cl_initialize()
      implicit none
      logical :: isopen
      if (hm_cl_initialized) then
        ! Do nothting
      else
        allocate(hm_description)
        call hm_sl_initialize(hm_description)
        call hm_sl_add(hm_description,&
&"Description: HyperMac is designed to provide 3+1 dimensional")
        call hm_sl_add(hm_description,&
&"  calculations to extend the capabilities of existing protein")
        call hm_sl_add(hm_description,&
&"  refinement programs.  A large portion of the code is based on")
        call hm_sl_add(hm_description,&
&"  Jana2006. Items shown in < > are manditory and those in [ ]")
        call hm_sl_add(hm_description,&
&"  optional.")

        allocate(commandfile%description)
        call hm_sl_initialize(commandfile%description)
        commandfile%name="-c"
        commandfile%enabled=.false.
        commandfile%default=.false.
        commandfile%tui_name="CommandFile"
        commandfile%option_string=""
        commandfile%option_int=0
        commandfile%option_real=0.00
        call hm_sl_add(commandfile%description,&
&"-c <commandfilename>")
        call hm_sl_add(commandfile%description,&
&"  The option allows for automating calls to the commandline")
        call hm_sl_add(commandfile%description,&
&"  interpreter. <commandfilename> is a text file containing a")
        call hm_sl_add(commandfile%description,&
&"  list of commands to be executed.  Once all of the commands")
        call hm_sl_add(commandfile%description,&
&"  have been executed the program will exit.")
        call hm_sl_add(commandfile%description,&
&" ")
        call hm_sl_add(commandfile%description,&
&"  Default: Disabled")
        call hm_sl_add(commandfile%description,&
&"  CommandFileName: (empty)")

        allocate(logfile%description)
        call hm_sl_initialize(logfile%description)
        logfile%name="-lf"
        logfile%enabled=.false.
        logfile%default=.false.
        logfile%tui_name="LogFile"
        logfile%option_string="log.txt"
        logfile%option_int=0
        logfile%option_real=0.00
        call hm_sl_add(logfile%description,&
&"-lf <logfilename>")
        call hm_sl_add(logfile%description,&
&"  The option allows for changing the logfile name from the")
        call hm_sl_add(logfile%description,&
&"  default. The new name will be <logfilename>.")
        call hm_sl_add(logfile%description,&
&" ")
        call hm_sl_add(logfile%description,&
&"  Default LogFileName: ./log.txt")

        allocate(enablelogfile%description)
        call hm_sl_initialize(enablelogfile%description)
        enablelogfile%name="-l"
        enablelogfile%enabled=.false.
        enablelogfile%default=.false.
        enablelogfile%tui_name="EnableLogFile"
        enablelogfile%option_string=""
        enablelogfile%option_int=0
        enablelogfile%option_real=0.00
        call hm_sl_add(enablelogfile%description,&
&"-l")
        call hm_sl_add(enablelogfile%description,&
&"  This option enables/disables the use of the logfile.  It will")
        call hm_sl_add(enablelogfile%description,&
&"  toggle the setting opposite of the default.")
call hm_sl_add(enablelogfile%description,&
&" ")
        call hm_sl_add(enablelogfile%description,&
&"  Default: Disabled")

        allocate(verboselevel%description)
        call hm_sl_initialize(verboselevel%description)
        verboselevel%name="-v"
        verboselevel%enabled=.true.
        verboselevel%default=.true.
        verboselevel%tui_name="VerboseLevel"
        verboselevel%option_string="5"
        verboselevel%option_int=5
        verboselevel%option_real=5.00
        call hm_sl_add(verboselevel%description,&
&"-v <level>")
        call hm_sl_add(verboselevel%description,&
&"  This option sets how much information is displayed in the")
        call hm_sl_add(verboselevel%description,&
&"  interpreter as well as in the log file.  The higher the")
        call hm_sl_add(verboselevel%description,&
&"  <level> the more information will be displayed/written.")
        call hm_sl_add(verboselevel%description,&
&" ")
        call hm_sl_add(verboselevel%description,&
&"  Default: Enabled")
        call hm_sl_add(verboselevel%description,&
&"  Level: 5")
        allocate(commandlinehelp%description)
        call hm_sl_initialize(commandlinehelp%description) 
        commandlinehelp%name="-h"
        commandlinehelp%enabled=.false.
        commandlinehelp%default=.false.
        commandlinehelp%tui_name="CommandLineHelp"
        commandlinehelp%option_string=""
        commandlinehelp%option_int=0
        commandlinehelp%option_real=0.00
        call hm_sl_add(commandlinehelp%description,&
&"-h")
        call hm_sl_add(commandlinehelp%description,&
&"  This option will display command line options.  If this")
        call hm_sl_add(commandlinehelp%description,&
&"  option is included on the commandline this text will be")
        call hm_sl_add(commandlinehelp%description,&
&"  displayed and the program will exit independent of other.")
        call hm_sl_add(commandlinehelp%description,&
&"  options that may have been provided.")
        call hm_sl_add(commandlinehelp%description,&
&" ")
        call hm_sl_add(commandlinehelp%description,&
&"  Default: Disabled")

        allocate(hdf5file%description)
        call hm_sl_initialize(hdf5file%description)
        hdf5file%name="-h"
        hdf5file%enabled=.false.
        hdf5file%default=.false.
        hdf5file%tui_name="HDF5FileName"
        hdf5file%option_string=""
        hdf5file%option_int=0
        hdf5file%option_real=0.00
        call hm_sl_add(hdf5file%description,&
&"-hdf5  <hdf5filename>")
        call hm_sl_add(hdf5file%description,&
&"  This option will load an external HDF5 file.  If the HDF5")
        call hm_sl_add(hdf5file%description,&
&"  file contains a command section those commands will be")
        call hm_sl_add(hdf5file%description,&
&"  executed and the program will exit.  If there is no")
        call hm_sl_add(hdf5file%description,&
&"  command section all of the data in the HDF5 will be")
        call hm_sl_add(hdf5file%description,&
&"  loaded into memory prior to commands in an externally")
        call hm_sl_add(hdf5file%description,&
&"  supplied command file using the -c option.  If an")
        call hm_sl_add(hdf5file%description,&
&"  command file has been supplied, the command section")
        call hm_sl_add(hdf5file%description,&
&"  of the HDF5 will be ignored.")
        call hm_sl_add(hdf5file%description,&
&" ")
        call hm_sl_add(hdf5file%description,&
&"  Default: Disabled")
        call hm_sl_add(hdf5file%description,&
&"  HDF5FileName: (empty)")


        inquire(unit=hm_log,opened=isopen)
        if (isopen) then
          close(hm_log)
        end if
        hm_cl_initialized=.true.
      end if

    end subroutine hm_cl_initialize

    subroutine hm_cl_process()
      implicit none
      character(4096) :: line=""
      integer :: i
      integer(kind=2) :: lss
      logical :: call_print_help=.false.
      logical :: call_print_invalid_option=.false.
      logical :: call_verbose_flag_error=.false. 
      logical :: ex
      type(hm_stringlist),pointer :: invalid_option
      type(hm_stringlist),pointer :: cl_summary
      integer :: length,status,value
      character(4096) :: cl_option=""
      integer::ve
      ve=0

      nullify(invalid_option)
      nullify(cl_summary)
      call hm_sl_initialize(cl_summary)
      call  hm_cl_initialize()
      !
      ! Process the command line arguments
      !
      i=0
      do
        call get_command_argument(i, line)
        !
        ! If there are no more arguments to process exit
        !
        if (len_trim(line) == 0) exit
        if (i==0) then
          if (trim(line)=="hypermac") then
            ! this is what we should get
          else
            ! the program name may have been changed
          end if
        else if (trim(line)=="-h") then
          call_print_help = .true.
        else if (trim(line)=="-v") then
          call get_command_argument(i+1,cl_option,length,status)
          if (status/=0) then
            if (associated(invalid_option)) then
              call hm_sl_add(invalid_option," ")
            else
              allocate(invalid_option)
              call hm_sl_initialize(invalid_option)
              call hm_sl_add(invalid_option," ")
              call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
              call hm_sl_add(invalid_option," ")
              call hm_sl_add(invalid_option,hm_cl_exe_str)
              call hm_sl_add(invalid_option," ")
            end if
            call_verbose_flag_error=.true.
            call hm_sl_add(invalid_option,"Error: option -v&
                                        & requires an integer")
            call hm_sl_add(invalid_option,"  No value found after -v")
            ve=1
          else
            read(cl_option,*,iostat=status) value
            if (status/=0) then
              if (associated(invalid_option)) then
                call hm_sl_add(invalid_option," ")
              else
                allocate(invalid_option)
                call hm_sl_initialize(invalid_option)
                call hm_sl_add(invalid_option," ")
                call hm_sl_add(invalid_option,&
                     &"HyperMac Command Line Prototype:")
                call hm_sl_add(invalid_option," ")
                call hm_sl_add(invalid_option,hm_cl_exe_str)
                call hm_sl_add(invalid_option," ")
              end if
              call_verbose_flag_error=.true.
              call hm_sl_add(invalid_option,"Error: option -v&
                                            & requires an integer")
              call hm_sl_add(invalid_option,"  Value( "//&
                   &trim(cl_option)// ") not an integer.")
              ve=1
            else
              call set_verbose_level(value)
              i = i+1
              call hm_sl_add(cl_summary,"  Found: -v "//trim(cl_option))
              call hm_sl_add(cl_summary,"    Verbose Level set to:"&
                                       &//trim(cl_option))
              call hm_sl_add(cl_summary," ")
            end if
          end if
        else if (trim(line)=="-l") then
          enablelogfile%enabled=.not.enablelogfile%enabled
          call hm_sl_add(cl_summary,"  Found: -l")
          if (logfile%enabled) then
              enablelogfile%enabled=.true.
          end if
          if (enablelogfile%enabled) then
            call hm_sl_add(cl_summary,"    Log File Enabled")
          else
            call hm_sl_add(cl_summary,"    Log File Disabled")
          end if
          call hm_sl_add(cl_summary," ")
        else if (trim(line)=="-lf") then
          call get_command_argument(i+1,cl_option,length,status)
          if (status/=0) then
            if (associated(invalid_option)) then
              call hm_sl_add(invalid_option," ")
            else
              allocate(invalid_option)
              call hm_sl_initialize(invalid_option)
              call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
              call hm_sl_add(invalid_option," ")
              call hm_sl_add(invalid_option,hm_cl_exe_str)
              call hm_sl_add(invalid_option," ")
            end if
            call_verbose_flag_error=.true.
            call hm_sl_add(invalid_option,"Error: option -lf&
                                            & requires a filename")
          else
            call hm_sl_add(cl_summary,"  Found: -lf "//trim(cl_option))
            open(UNIT=99,FILE=trim(cl_option))
            close(99)
            INQUIRE(FILE=trim(cl_option),EXIST=ex)
            if (ex) then
              logfile%enabled=.true.
              logfile%option_string=trim(cl_option)
              call hm_sl_add(cl_summary,"    Logging to <"&
                             &//trim(cl_option)//">.")
              if (enablelogfile%enabled) then
                call hm_sl_add(cl_summary,"    Logging already &
                               &enabled].")                
              else
                call hm_sl_add(cl_summary,"    implied -l flag enabled.")
                enablelogfile%enabled=.true.
              endif              

              call hm_sl_add(cl_summary," ")
            else
              if (associated(invalid_option)) then
                call hm_sl_add(invalid_option," ")
              else
                allocate(invalid_option)
                call hm_sl_initialize(invalid_option)
                call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
                call hm_sl_add(invalid_option," ")
                call hm_sl_add(invalid_option,hm_cl_exe_str)
                call hm_sl_add(invalid_option," ")
              end if
              call_verbose_flag_error=.true.
              call hm_sl_add(invalid_option,&
                   & "Error: <filename> "//trim(cl_option)//" does not exist.") 
            end if
          i=i+1
          end if
        else if (trim(line)=="-c") then
          call get_command_argument(i+1,cl_option,length,status)
          if (status/=0) then
            if (associated(invalid_option)) then
              call hm_sl_add(invalid_option," ")
            else
              allocate(invalid_option)
              call hm_sl_initialize(invalid_option)
              call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
              call hm_sl_add(invalid_option," ")
              call hm_sl_add(invalid_option,hm_cl_exe_str)
              call hm_sl_add(invalid_option," ")
            end if
            call_verbose_flag_error=.true.
            call hm_sl_add(invalid_option,"Error: option -c&
                                            & requires a filename")
          else
            call hm_sl_add(cl_summary,"  Found: -c "//trim(cl_option))
            INQUIRE(FILE=trim(cl_option),EXIST=ex)
            if (ex) then
              commandfile%enabled=.true.
              commandfile%option_string=cl_option
              commandfile%option_int=0
              commandfile%option_real=0.00
              call hm_sl_add(cl_summary,"    Reading input from <"&
                             &//trim(cl_option)//">.")
              call hm_sl_add(cl_summary," ")
            else
              if (associated(invalid_option)) then
                call hm_sl_add(invalid_option," ")
              else
                allocate(invalid_option)
                call hm_sl_initialize(invalid_option)
                call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
                call hm_sl_add(invalid_option," ")
                call hm_sl_add(invalid_option,hm_cl_exe_str)
                call hm_sl_add(invalid_option," ")
              end if
              call_verbose_flag_error=.true.
              call hm_sl_add(invalid_option,&
                   & "Error: <filename> "//trim(cl_option)//" does not exist.") 
            end if
          i=i+1
          end if
        else if (trim(line)=="-hdf5") then
          call get_command_argument(i+1,cl_option,length,status)
          if (status/=0) then
            if (associated(invalid_option)) then
              call hm_sl_add(invalid_option," ")
            else
              allocate(invalid_option)
              call hm_sl_initialize(invalid_option)
              call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
              call hm_sl_add(invalid_option," ")
              call hm_sl_add(invalid_option,hm_cl_exe_str)
              call hm_sl_add(invalid_option," ")
            end if
            call_verbose_flag_error=.true.
            call hm_sl_add(invalid_option,"Error: option -hdf5&
                                            & requires a filename")
          else
            call hm_sl_add(cl_summary,"  Found: -hdf5 "//trim(cl_option))
            INQUIRE(FILE=trim(cl_option),EXIST=ex)
            if (ex) then
              hdf5file%enabled=.true.
              hdf5file%option_string=cl_option
              hdf5file%option_int=0
              hdf5file%option_real=0.00
              call hm_sl_add(cl_summary,"    Reading input from <"&
                             &//trim(cl_option)//">.")
              call hm_sl_add(cl_summary," ")
            else
              if (associated(invalid_option)) then
                call hm_sl_add(invalid_option," ")
              else
                allocate(invalid_option)
                call hm_sl_initialize(invalid_option)
                call hm_sl_add(invalid_option,&
                   &"HyperMac Command Line Prototype:")
                call hm_sl_add(invalid_option," ")
                call hm_sl_add(invalid_option,hm_cl_exe_str)
                call hm_sl_add(invalid_option," ")
              end if
              call_verbose_flag_error=.true.
              call hm_sl_add(invalid_option,&
                   & "Error: <filename> "//trim(cl_option)//" does not exist.") 
            end if
          i=i+1
          end if
        else
          if (associated(invalid_option)) then
            call hm_sl_add(invalid_option," ")
          else
            allocate(invalid_option)
            call hm_sl_initialize(invalid_option)
            call hm_sl_add(invalid_option," ")
            call hm_sl_add(invalid_option,&
                 &"HyperMac Command Line Prototype:")
            call hm_sl_add(invalid_option," ")
            call hm_sl_add(invalid_option,hm_cl_exe_str)
            call hm_sl_add(invalid_option," ")
          end if
          call_print_invalid_option=.true.
          write (hm_buffer,'(a,a,a)') "Error: invalid option '",trim(line),"'"
          call hm_sl_add(invalid_option,trim(hm_buffer))
        end if
        i = i+1
      end do
      if (enablelogfile%enabled) then
        open (unit=hm_log,file=trim(logfile%option_string),access="stream", &
              status="replace")
        lss=hm_s_get_ss()
        lss=ior(flfu,lss)
        call hm_s_set_ss(lss)        
      end if
      call hm_s_write_header()
      if (call_print_invalid_option) then
        call hm_sl_add(invalid_option," ")
        call hm_sl_add(invalid_option,&
                         &"Try 'hypermac -h' for more information.")
        call hm_s_write_stringlist(invalid_option)
        call hm_s_write_bl()
        stop
      end if
      if (call_verbose_flag_error) then
        call hm_s_write_stringlist(invalid_option)
        call hm_s_write_bl()
        stop
      end if
      if (call_print_help) then
        call hm_cl_usage_help()
        stop
      end if
      if (cl_summary%count.ne.0) call hm_cl_print_summary(cl_summary)
      call hm_sl_free_memory(cl_summary)
    end subroutine hm_cl_process

    subroutine hm_cl_usage_help()
      implicit none
      if (.not. hm_cl_initialized) call hm_cl_initialize()
      call hm_s_write_bl()
      call hm_s_write(hm_cl_exe_str)
      call hm_s_write_bl()
      call hm_s_write_stringlist(hm_description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(commandlinehelp%description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(verboselevel%description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(enablelogfile%description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(logfile%description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(commandfile%description)
      call hm_s_write_bl()
      call hm_s_write_stringlist(hdf5file%description)
      call hm_s_write_bl()

    end subroutine hm_cl_usage_help

    subroutine set_verbose_level(i)
      implicit none
      integer,intent(in) :: i
      integer :: length,status,value
      character(4096) :: line=""
      character(10) :: tmp=""
      write(tmp,'(I6)') i
      tmp=adjustl(tmp)
      verboselevel%option_string=trim(tmp)
      verboselevel%option_int=i
      verboselevel%enabled=.true.
      verboselevel%default=.false.
    end subroutine set_verbose_level

    function get_verbose_level() result(level)
      integer :: level
      level = verboselevel%option_int
    end function get_verbose_level

    subroutine display_verbose_status()
      call hm_s_write_bl()
      call hm_s_write("VerboseLevel")
      write (hm_buffer,'(a,L)') "  Enabled: ",verboselevel%enabled
      call hm_s_write(trim(hm_buffer))
      write (hm_buffer,'(a,L)') "  Default: ",verboselevel%default
      call hm_s_write(trim(hm_buffer))
      call hm_s_write("  Value: "//trim(verboselevel%option_string))
      write (hm_buffer,'(a,I3,a)') "  Int: ",verboselevel%option_int,nl
      call hm_s_write(trim(hm_buffer))
    end subroutine display_verbose_status

    subroutine display_commandfile_status()
      call hm_s_write_bl()
      call hm_s_write("CommandFile")
      write (hm_buffer,'(a,L)') "  Enabled: ",commandfile%enabled
      call hm_s_write(trim(hm_buffer))
      write (hm_buffer,'(a,L)') "  Default: ",commandfile%default
      call hm_s_write(trim(hm_buffer))
      call hm_s_write("  File: "//trim(commandfile%option_string))
      write (hm_buffer,'(a,I3,a)') "  Int: ",commandfile%option_int,nl
      call hm_s_write(trim(hm_buffer))
    end subroutine display_commandfile_status

    subroutine display_enablelogfile_status()
      call hm_s_write_bl()
      call hm_s_write("EnableLogFile")
      write (hm_buffer,'(a,L)') "  Enabled: ",enablelogfile%enabled
      call hm_s_write(trim(hm_buffer))
      write (hm_buffer,'(a,L)') "  Default: ",enablelogfile%default
      call hm_s_write(trim(hm_buffer))
      call hm_s_write("  File: "//trim(enablelogfile%option_string))
      write (hm_buffer,'(a,I3,a)') "  Int: ",enablelogfile%option_int,nl
      call hm_s_write(trim(hm_buffer))
    end subroutine display_enablelogfile_status

    subroutine display_logfile_status()
      call hm_s_write_bl()
      call hm_s_write("LogFile")
      write (hm_buffer,'(a,L)') "  Enabled: ",logfile%enabled
      call hm_s_write(trim(hm_buffer))
      write (hm_buffer,'(a,L)') "  Default: ",logfile%default
      call hm_s_write(trim(hm_buffer))
      call hm_s_write("  File: "//trim(logfile%option_string))
      write (hm_buffer,'(a,I3,a)') "  Int: ",logfile%option_int,nl
      call hm_s_write(trim(hm_buffer))
    end subroutine display_logfile_status

    subroutine display_hdf5file_status()
      call hm_s_write_bl()
      call hm_s_write("HDF5File")
      write (hm_buffer,'(a,L)') "  Enabled: ",hdf5file%enabled
      call hm_s_write(trim(hm_buffer))
      write (hm_buffer,'(a,L)') "  Default: ",hdf5file%default
      call hm_s_write(trim(hm_buffer))
      call hm_s_write("  File: "//trim(hdf5file%option_string))
      write (hm_buffer,'(a,I3,a)') "  Int: ",hdf5file%option_int,nl
      call hm_s_write(trim(hm_buffer))
    end subroutine display_hdf5file_status

    subroutine hm_cl_print_summary(sl_pointer)
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: sl_pointer
      call hm_s_divider()
      call hm_s_write_bl()
      call hm_s_write("Command Line Options")
      call hm_s_write_bl()
      call hm_s_write_stringlist(sl_pointer)
    end subroutine hm_cl_print_summary

end module hm_commandline
