module hm_stream
  use hm_sl
  use hm_nl
  use hm_io
  implicit none
  private

  real,save :: pb_target_value
  real,save :: pb_current_value
  integer,save :: pb_current_step
  integer,save :: pb_steps
  character(len=:),allocatable,save :: pb_header
  character(len=:),allocatable,save :: pb_status
  character,save :: divider_character="*"
  integer,save :: divider_length=80
  
  integer,save :: lfu !Log File Unit
  integer,save :: cfu !Console File Unit
  integer(kind=2),save :: ss !Current Stream State
  integer(kind=2),parameter :: fV1=        b'0000000000000001'
  integer(kind=2),parameter :: fV2=        b'0000000000000010'
  integer(kind=2),parameter :: fV3=        b'0000000000000100'
  integer(kind=2),parameter :: flfu=       b'0000000000001000'
  integer(kind=2),parameter :: fcfu=       b'0000000000010000'
  integer(kind=2),parameter :: fProgBar=   b'0000000000100000'
  integer(kind=2),parameter :: fUnknown=   b'0000000001000000'
  integer(kind=2),parameter :: fEchoInput= b'0000000010000000'
  
  integer(kind=2),parameter :: vmask=      b'0000000000000111'
  integer(kind=2),parameter :: amask=      b'0111111111111111'  

  public hm_s_write_stringlist,hm_s_write,hm_s_initialize,hm_s_write_bl, &
       & fV1,fV2,fV3,flfu,fcfu,fProgBar,fUnknown,fEchoInput, &
       & vmask,amask,hm_s_set_ss,hm_s_get_ss,pb_initialize,pb_update, &
       & pb_close,hm_s_divider,hm_s_write_header,hm_s_write_footer, &
       & hm_s_pathsummary,hm_s_na_write,hm_s_write_logfile

  contains

    subroutine hm_s_write_header()
      call hm_s_divider()
      call hm_s_write_bl()
      call hm_s_write("HyperMac ("//hm_version//") 2016")
      call hm_s_write_bl()
      call hm_s_divider()
      call hm_s_write("Description:")
      call hm_s_write_bl()
      call hm_s_write("  Helper program to provide 3+1 support to protein")
      call hm_s_write("  crystallographic programs.")
      call hm_s_write_bl()
      call hm_s_write("  The code is based on JANA2006.")
      call hm_s_write_bl()
      call hm_s_write("  Please reference: Journal Here")
      call hm_s_write_bl()
      call hm_s_divider()
      call hm_s_write_bl()
      call hm_s_write("Execution begins at:")
      call hm_s_write("  "//nicedate(hm_start_date)//" "//nicetime(hm_start_time))
      call hm_s_write_bl()
    end subroutine hm_s_write_header

    subroutine hm_s_write_footer()
      call update_current_time()
      call hm_s_divider()
      call hm_s_write_bl()
      call hm_s_write("Execution ends at:")
      call hm_s_write("  "//nicedate(hm_curr_date)//" "//nicetime(hm_curr_time))
      call hm_s_write_bl()
      write(hm_buffer,'(a,f12.3,a)') "Elapsed time: ",hm_elapsed," seconds."
      call hm_s_write(trim(hm_buffer))
      call hm_s_write_bl()
      call hm_s_divider()
    end subroutine hm_s_write_footer

    subroutine hm_s_set_ss(newss)
      integer(kind=2),intent(in) :: newss
      ss=newss
    end subroutine hm_s_set_ss

    function hm_s_get_ss() result(currentss)
      integer(kind=2) :: currentss
      currentss=ss
    end function hm_s_get_ss

    subroutine hm_s_write_stringlist(psl,VerboseLevel,Flags)
      use hm_sl
      implicit none
      type(hm_stringlist),pointer,intent(inout) :: psl
      integer,optional,intent(in) :: VerboseLevel,Flags
      integer :: VL,LS,MaxV
      integer :: i
      MaxV=iand(vmask,ss)
      if (present(VerboseLevel)) then
         VL=VerboseLevel
      else
         VL=0
      endif
      if (present(Flags)) then
        LS=Flags
      else
        LS=ss
      endif
      if (VL.le.MaxV) then
        if (iand(LS,flfu).eq.flfu) then
          do i=1,psl%count
            call hm_s_write_logfile(hm_sl_string_at(psl,i))
          end do
        endif
        if (iand(LS,fcfu).eq.fcfu) then
          do i=1,psl%count
            call hm_s_write_console(hm_sl_string_at(psl,i))
          end do
        endif
      endif
    end subroutine hm_s_write_stringlist

    subroutine hm_s_na_write(AString,nobackspace,VerboseLevel,Flags)
      implicit none
      character(len=*),intent(in) :: AString
      logical,optional,intent(in) :: nobackspace
      integer,optional,intent(in) :: VerboseLevel,Flags
      integer :: VL,LS,MaxV
      logical :: nbs
      MaxV=iand(vmask,ss)
      if (present(VerboseLevel)) then
         VL=VerboseLevel
      else
         VL=0
      endif
      if (present(Flags)) then
        LS=Flags
      else
        LS=ss
      endif
      if (present(nobackspace)) then
        nbs=nobackspace
      else
        nbs=.false.
      endif
      if (VL.le.MaxV) then
        if (iand(LS,flfu).eq.flfu) then
          call hm_s_na_write_logfile(AString,nbs)
        endif
        if (iand(LS,fcfu).eq.fcfu) then
          call hm_s_na_write_console(AString,nbs)
        endif
      endif
    end subroutine hm_s_na_write

    subroutine hm_s_na_write_logfile(AString,nobackspace)
      implicit none
      character(len=*),intent(in) :: AString
      logical,intent(in) :: nobackspace
      integer :: curpos
      inquire(unit=hm_log,pos=curpos)
      if (nobackspace) then
        write(hm_log),AString
      else
        write(unit=hm_log,POS=curpos-len(AString))
        write(hm_log),AString
      end if 
    end subroutine hm_s_na_write_logfile

    subroutine hm_s_na_write_console(AString,nobackspace)
      implicit none
      character(len=*),intent(in) :: AString
      logical,intent(in) :: nobackspace
      if (nobackspace) then
        write(hm_cout,fmt="(a)",advance="no") AString
      else
        write(hm_cout,fmt="(a1,a)",advance="no")char(13),AString
      endif
    end subroutine hm_s_na_write_console

    subroutine hm_s_write(AString,VerboseLevel,Flags)
      implicit none
      character(len=*),intent(in) :: AString
      integer,optional,intent(in) :: VerboseLevel,Flags
      integer :: VL,LS,MaxV
      MaxV=iand(vmask,ss)
      if (present(VerboseLevel)) then
         VL=VerboseLevel
      else
         VL=0
      endif
      if (present(Flags)) then
        LS=Flags
      else
        LS=ss
      endif
      if (iand(ss,fProgBar)==fProgBar) then
        call pb_close(pb_current_value)
      endif
      if (VL.le.MaxV) then
        if (iand(LS,flfu).eq.flfu) then
          call hm_s_write_logfile(AString)
        endif
        if (iand(LS,fcfu).eq.fcfu) then
          call hm_s_write_console(AString)
        endif
      endif
    end subroutine hm_s_write

    subroutine hm_s_initialize()
      implicit none
      ss=5
      ss=ior(ss,fcfu)
      ss=ior(ss,fEchoInput)
    end subroutine hm_s_initialize

    subroutine hm_s_open_logfile()
      implicit none
    end subroutine hm_s_open_logfile

    subroutine hm_s_write_logfile(AString)
      implicit none
      character(len=*),intent(in) :: AString
      logical :: ex
      INQUIRE(UNIT=hm_log,OPENED=ex)
      if (ex) then
        write(hm_log),AString
        write(hm_log),lf
      endif
    end subroutine hm_s_write_logfile

    subroutine hm_s_write_console(AString)
      implicit none
      character(len=*),intent(in) :: AString
      write(hm_cout,"(a)"),AString
    end subroutine hm_s_write_console

    subroutine hm_s_write_bl(VerboseLevel,Flags)
      integer,intent(in),optional :: VerboseLevel,Flags
      integer :: calltype
      character(len=1) :: bl

      calltype=1
      bl=""

      if (present(VerboseLevel)) then
        calltype=2
      endif

      if (present(Flags)) then
        calltype=3
      endif
      if (calltype==1) then
        call hm_s_write(bl)
      else if (calltype==2) then
        call hm_s_write(bl,VerboseLevel)
      else if (calltype==3) then
        call hm_s_write(bl,VerboseLevel,Flags)
      endif
    end subroutine hm_s_write_bl      

    subroutine pb_update(avalue,verboselevel,Flags)
      implicit none
      real,intent(in) :: avalue
      integer,intent(in),optional :: VerboseLevel,Flags
      integer :: calltype
      real :: pcmplt
      character(6) :: pcmpltstr
      integer :: i,k,newstep,offset

      if (iand(ss,fProgBar)==fProgBar) then

      calltype=1
      if (present(VerboseLevel)) then
        calltype=2
      endif
      if (present(Flags)) then
        calltype=3
      endif

      pcmplt=avalue/pb_target_value
      newstep=int(pcmplt*pb_steps)
      write(unit=pcmpltstr,fmt="(f6.2)") pcmplt*100.0
      i=len(pb_status)
      if (pcmpltstr==pb_status(i-7:i-2)) then
        ! Do Nothing
      else
        pb_status(i-7:i-2)=pcmpltstr
        if (newstep/=pb_current_step) then
          offset=0
          do k=1,newstep
            pb_status(1+k:1+k)="="
          enddo
          if ((newstep.lt.pb_steps).and.(avalue.ne.0)) then
            pb_status((newstep+2):(newstep+2))=">"
            offset=1
          endif
          do k=offset+2+newstep,pb_steps
            pb_status(k:k)=" "
          enddo
          if (calltype==1) then
            call hm_s_na_write(pb_status)
          else if (calltype==2) then
            call hm_s_na_write(pb_status,.false.,VerboseLevel)
          else if (calltype==3) then
            call hm_s_na_write(pb_status,.false.,VerboseLevel,Flags)
          endif   
        else
          if (calltype==1) then
            call hm_s_na_write(pb_status)
          else if (calltype==2) then
            call hm_s_na_write(pb_status,.false.,VerboseLevel)
          else if (calltype==3) then
            call hm_s_na_write(pb_status,.false.,VerboseLevel,Flags)
          endif
        endif
      endif
    else
      write(*,*) "Error: Progress Bar updated but no Progress Bar to update"
      stop
    end if
    end subroutine pb_update

    subroutine pb_initialize(steps,targetvalue,initialvalue,verboselevel,Flags)
      implicit none
      integer,intent(in),optional :: steps,verboselevel,Flags
      real,intent(in),optional ::targetvalue
      real,intent(in),optional ::initialvalue
      integer :: calltype,i
      calltype=1
      if (present(verboselevel)) then
        calltype=2
      endif
      if (present(Flags)) then
        calltype=3
      endif

      if (iand(ss,fProgBar).eq.fProgBar) then
         call pb_close()
      endif

      if (present(steps)) then
        pb_steps=steps
      else
        pb_steps=10
      endif

      if (present(targetvalue)) then
        pb_target_value=targetvalue
      else
        pb_target_value=100.0
      endif

      if (present(initialvalue)) then
        pb_current_value=initialvalue
      else
        pb_target_value=0.0
      endif

      if (allocated(pb_header)) then
        deallocate(pb_header)
      endif
      allocate(character(len=steps+5) :: pb_header)
 
      pb_header(1:2)="0%"

      do i=3, steps+1
        pb_header(i:i)=" "
      enddo
      
      pb_header(steps+2:steps+5)="100%"

      if (calltype==1) then
        call hm_s_write(pb_header)
      else if (calltype==2) then
        call hm_s_write(pb_header,verboselevel)
      else if (calltype==3) then
        call hm_s_write(pb_header,verboselevel,Flags)
      endif

      if (allocated(pb_status)) then
        deallocate(pb_status)
      endif
      allocate(character(len=steps+11) :: pb_status)

      pb_status (1:1)="|"

      do i=2, steps+1
        pb_status(i:i)=" "
      enddo

      pb_status(pb_steps+2:pb_steps+11)="|(  0.00%)"

      ss=ior(ss,fProgBar)

      if (calltype==1) then
        call hm_s_na_write(pb_status,.true.)
      else if (calltype==2) then
        call hm_s_na_write(pb_status,.true.,verboselevel)
      else if (calltype==3) then
        call hm_s_na_write(pb_status,.true.,verboselevel,Flags)
      endif      

    end subroutine pb_initialize

    subroutine pb_close(finalvalue,verboselevel,Flags)
      real,intent(in),optional :: finalvalue
      integer,intent(in),optional :: verboselevel,Flags
      integer :: calltype,i
      calltype=1
      if (present(verboselevel)) then
        calltype=2
      endif
      if (present(Flags)) then
        calltype=3
      endif
      if (calltype==1) then
        call pb_update(finalvalue)
      else if (calltype==2) then
        call pb_update(finalvalue,verboselevel)
      else if (calltype==3) then
        call pb_update(finalvalue,verboselevel,Flags)
      endif
      ss=iand(ss,not(fProgBar))
      if (calltype==1) then
        call hm_s_write("")
        call hm_s_write_bl()
      else if (calltype==2) then
        call hm_s_write("",verboselevel)
        call hm_s_write_bl(verboselevel)
      else if (calltype==3) then
        call hm_s_write("",verboselevel,Flags)
        call hm_s_write_bl(verboselevel,Flags)
      endif
    deallocate(pb_header)
    deallocate(pb_status)
    pb_target_value=100
    pb_current_value=0
    pb_current_step=0
    pb_steps=10 
    end subroutine pb_close
   
    subroutine hm_s_divider(dlength,dchar,verboselevel,Flags)
      integer,intent(in),optional :: dlength,verboselevel,Flags
      character,intent(in),optional :: dchar
      integer :: length,i,calltype
      character :: char
      character(len=:),allocatable :: divider

      calltype=1

      if (present(dlength)) then
        length=dlength
      else      
        length=divider_length
      endif
 
      if (present(dchar)) then
        char=dchar
      else
        char=divider_character
      endif

      if (present(verboselevel)) then
        calltype=2
      endif

      if (present(Flags)) then
        calltype=3
      endif

      if (allocated(divider)) then
        deallocate(divider)
      endif

      allocate(character(len=length) :: divider)

      do i=1,length
        divider(i:i)=char
      enddo

      if (calltype==1) then
        call hm_s_write(divider)
      else if (calltype==2) then
        call hm_s_write(divider,verboselevel)
      else if (calltype==3) then
        call hm_s_write(divider,verboselevel,Flags)
      endif

    end subroutine hm_s_divider

    subroutine hm_s_pathsummary()
      call hm_s_divider()
      call hm_s_write_bl()
      call hm_s_write("Path and Enviornment Variable Settings")
      call hm_s_write_bl()
      call hm_s_write("  Hypermac Executable: "//hypermac_exe)
      call hm_s_write("  Hypermac Exe Path: "//hypermac_exe_path)
      call hm_s_write("  Hypermac Root Path: "//hypermac_root_path)
      call hm_s_write("  Hypermac Working Directory: "//hypermac_wd)
      call hm_s_write("  JANA2006_DIR: "//hypermac_wd)
      call hm_s_write_bl()

    end subroutine hm_s_pathsummary 
   
end module hm_stream
