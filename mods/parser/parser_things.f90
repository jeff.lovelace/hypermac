module parser_things
  !
  !---Things for parser
  integer :: stack_depth = 100
  integer :: recurse_limit = 0
  type stack_item
    integer :: fid
    character(len=4096) :: fname
    logical :: active
  end type stack_item

  type process_stack
    integer :: max_items
    integer :: active_item
    type(stack_item), dimension(stack_depth)::stack_items
  end type process_stack

  type(process_stack)::stack

  character :: line_parse*4096
  logical :: lprint,lend
  real :: fvalue(500)
  integer :: ntok
  integer :: ibeg(500),iend(500),ityp(500),idec(500)
  character :: key*4
  character :: cvalue(500)*4
end module parser_things
