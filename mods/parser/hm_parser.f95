module hm_parser
  use hm_sl
  use hm_nl
  use hm_io
  use hm_commandline
  implicit none
  private

  public hm_p_execute, hm_p_get_input, hm_p_parse_input, hm_p_digest_input

  contains

    subroutine hm_p_execute()
      character :: lt
      stack%active_item=0
      if (commandfile%enabled) then
        stack%active_item=1
        stack%items(1)%fid=101
        stack%items(1)%active=.true.
        stack%items(1)%fname=commandfile%option_string
        stack%items(1)%ftype='c'
        open(UNIT=stack%items(1)%fid,FILE=stack%items(1)%fname)
      else if (hdf5file%enabled) then
        stack%active_item=1
        stack%items(1)%fid=101
        stack%items(1)%active=.true.
        stack%items(1)%fname=hdf5file%option_string
        stack%items(1)%ftype='h'
        open(UNIT=stack%items(1)%fid,FILE=stack%items(1)%fname)
      else
        stack%active_item=1
        stack%items(1)%fid=hm_cin
        stack%items(1)%active=.true.
        stack%items(1)%fname=""
        stack%items(1)%ftype='s'
      endif
      process_input=.true.
      lt=stack%items(stack%active_item)%ftype
      if ((lt.eq.'c').or.(lt.eq.'m').or.(lt.eq.'h')) then
         call hm_s_write("Hypermac> Processing: "//&
                     &trim(stack%items(stack%active_item)%fname))
         call hm_s_write_bl()
      end if
      do while (process_input)
        call hm_p_get_input()
        if (len_trim(hm_buffer).gt.0) then
          call hm_p_parse_input()
          call hm_p_digest_input()
        endif
      end do
    end subroutine hm_p_execute
  
    subroutine hm_p_get_input()
      implicit none
      character(len=4096) :: tmp,tmp2
      integer :: fid,ios,len,clen
      logical :: cntflag
      character :: lt
      lt = stack%items(stack%active_item)%ftype
      cntflag=.true.
      fid = stack%items(stack%active_item)%fid
      hm_buffer=""
      clen=0
      call hm_s_na_write("Hypermac> ",.true.)
      do while (cntflag)
        read(fid,'(a)',IOSTAT=ios) tmp
        if (.not.(ios.eq.0)) then
          tmp=""
        end if
        len=len_trim(tmp)
        if (len.gt.0) then
          if ((lt.eq.'c').or.(lt.eq.'m').or.(lt.eq.'h')) then
            call hm_s_write(trim(tmp))
          end if
          if (tmp(len:len).eq.'&') then
            tmp(len:len)=' '
            cntflag=.true.
          else if (tmp(len:len).eq.'\') then
            tmp(len:len)=' '
            cntflag=.true.
          else if (tmp(len:len).eq.'-') then
            tmp(len:len)=' '
            cntflag=.true.
          else 
            cntflag=.false.
          endif
          if (cntflag) then
            if (clen.eq.0) then
              hm_buffer = trim(tmp)
              clen=len
            else
              tmp2=hm_buffer(1:clen)
              hm_buffer = tmp2 // trim(tmp)
            end if
          else
            if (clen.eq.0) then
              hm_buffer = trim(tmp)
              clen=len
            else
              tmp2=hm_buffer(1:clen)
              hm_buffer = tmp2(1:clen) // trim(tmp)
            end if 
          end if
        else
          cntflag=.false.
        endif
        if (.not.(ios.eq.0)) then
          call hm_s_write("Finished Processing: "//&
              &trim(stack%items(stack%active_item)%fname))
          close(stack%items(stack%active_item)%fid)
          stack%items(stack%active_item)%active=.false.
          stack%items(stack%active_item)%fname=""
          stack%items(stack%active_item)%ftype=''
          stack%active_item=stack%active_item-1
          if (stack%active_item.eq.0) then
            process_input=.false.
          end if
          cntflag=.false.
          hm_buffer=""
        endif
        if (lt.eq.'s') call hm_s_write_logfile(trim(hm_buffer))
        if (cntflag) then
          call hm_s_na_write("Hypermac>+",.true.)
        endif
        
      end do
    end subroutine hm_p_get_input
 
    subroutine hm_p_parse_input()
      if (len_trim(hm_buffer).gt.0) then
        call parser(key,hm_buffer,ibeg,iend,ityp,fvalue,cvalue,idec,   &
              ntok,lend,lprint)
      end if
    end subroutine hm_p_parse_input

    subroutine hm_p_digest_input()
      implicit none
      integer :: i
      if (cvalue(1).eq.'x') then
        call hm_s_write_bl()
        process_input=.false.
      else if (cvalue(1)=="dvs") then
        call display_verbose_status()
      else if (cvalue(1)=="dcs") then
        call display_commandfile_status()
      else if (cvalue(1)=="dls") then
        call display_enablelogfile_status()
      else if (cvalue(1)=="dlfs") then
        call display_logfile_status()
      else if (cvalue(1)=="d5s") then
        call display_hdf5file_status()
      else if (cvalue(1)=="tpb") then
        call pb_initialize(5,10*1.0,0*1.0)
        do i= 1, 10
          call pb_update(i*1.0)
          call sleep(1)
        end do
        call pb_close(10*1.0)
      else if (cvalue(1)=="td") then
        call hm_s_divider()
      else if (cvalue(1)=="tw") then
        call get_environment_variable("COLUMNS",hm_buffer)
        call hm_s_write(trim(hm_buffer))
      else
        call hm_s_write_bl()
        call hm_s_write("  Unknown command: "//trim(hm_buffer))
        call hm_s_write_bl()
      end if
    end subroutine hm_p_digest_input
end module hm_parser

