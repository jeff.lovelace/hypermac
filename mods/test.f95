program hello
implicit none
character(len=512), external :: GetHomeDir

   character(len=15) :: surname, firstname 
   character(len=6) :: title 
   character(len=40):: name
   character(len=25)::greetings
   character(len=1024) :: HomeDir
   title = 'Mr.' 
   firstname = 'Rowan' 
   surname = 'Atkinson'
   
   name = trim(title)//' '//trim(firstname)//' '//trim(surname)
   greetings = 'A big hello from Mr. Beans'
   
   print *, 'Here is ', name
   print *, greetings
   
   print *, GetHomeDir()

end program hello
