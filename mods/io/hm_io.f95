module hm_io
  use iso_fortran_env
  implicit none
  type :: process_stack_item
    integer :: fid
    character(len=4096) :: fname
    logical :: active
    character :: ftype
  end type process_stack_item
  type :: process_stack
    integer :: active_item
    type(process_stack_item),dimension(100)::items
  end type process_stack
  type(process_stack) :: stack

  integer,parameter :: hm_cin = input_unit
  integer,parameter :: hm_cout = output_unit
  integer,parameter :: hm_err = error_unit
  character(len=4096),save :: hm_buffer
  character(len=4096),save :: hm_tmp
  character(len=4096),save :: hm_inputline
  integer,save :: hm_log=20
  real(kind=16),save :: hm_start,hm_curr,hm_elapsed,hm_lap
  character(8),save  :: hm_start_date,hm_curr_date
  character(10),save :: hm_start_time,hm_curr_time
  character(5),save  :: hm_start_zone,hm_curr_zone
  integer,dimension(8),save :: hm_start_values,hm_curr_values
  character(len=:), allocatable,save :: hypermac_exe
  character(len=:), allocatable,save :: hypermac_exe_path
  character(len=:), allocatable,save :: hypermac_root_path
  character(len=:), allocatable,save :: hypermac_wd
  character(len=:), allocatable,save :: JANA2006_DIR
  character(len=*), parameter ::hm_version="00.00.00"
  logical,save :: process_input

  character line_parse*512
  logical   lprint,lend
  real      fvalue(500)
  integer   ntok
  integer   ibeg(500),iend(500),ityp(500),idec(500)
  character key*4
  character cvalue(500)*4

  contains

    subroutine update_start_time()
      call date_and_time(hm_start_date,hm_start_time,hm_start_zone,&
           & hm_start_values)
      hm_start=reducedate(hm_start_values,1)
      hm_curr=hm_start
      hm_elapsed=0.00
      hm_lap=0.00
      call update_current_time()
    end subroutine update_start_time
    
    subroutine update_current_time()
      real(kind=16) temp
      call date_and_time(hm_curr_date,hm_curr_time,hm_curr_zone,&
           & hm_curr_values)
      temp=hm_curr
      hm_curr=reducedate(hm_curr_values,1)
      hm_lap=hm_curr-temp
      hm_elapsed=hm_curr-hm_start
      end subroutine update_current_time

    function nicedate(date) result(value) 
      character(8) :: date
      character(10) :: value
      value=date(1:4)//"/"//date(5:6)//"/"//date(7:8)
    end function nicedate

    function nicetime(time) result(value)
      character(10) :: time
      character(12) :: value
      value=time(1:2)//":"//time(3:4)//" "//time(5:10)
    end function nicetime

    function reducedate(values,typeid) result(value)
      ! reducedata
      ! return the total number of typeid time units where
      ! typeid =
      ! 
      ! 0 - miliseconds
      ! 1 - seconds
      ! 2 - minutes
      ! 3 - hours
      ! 4 - days
      ! 5 - months
      ! 6 - years
      ! 
      implicit none

      integer, intent(in) :: typeid
      integer,dimension(8),intent(in) :: values
      real(kind=16) :: value
      value=(12.0*30.44*24.0*60.0*60.0*1000.0*values(1))
      value=(30.44*24.0*60.0*60.0*1000.0*values(2))+value
      value=(24.0*60.0*60.0*1000.0*values(3))+value
      value=(60.0*60.0*1000.0*values(5))+value
      value=(60.0*1000.0*values(6))+value
      value=(1000.0*values(7))+value
      value=values(8)+value
      if (typeid == 1) then
        value=value/1000.0
      else if (typeid == 2) then
        value=value/(60.0*1000.0)
      else if (typeid == 3) then
        value=value/(60.0*60.0*1000.0)
      else if (typeid == 4) then
        value=value/(24.0*60.0*60.0*1000.0)
      else if (typeid == 5) then
        value=value/(30.44*24.0*60.0*60.0*1000.0)
      else if (typeid == 6) then
        value=value/(12.0*30.44*24.0*60.0*60.0*1000.0)
      endif
    end function reducedate
 
    subroutine determine_env_paths()
      use ISO_C_BINDING
          implicit none
        interface
          function readlink(path, buf, bufsize) bind(C, NAME = 'readlink')
            import
            integer(C_SIZE_T) :: readlink
            character(KIND = C_CHAR), intent(IN) :: path(*)
            character(KIND = C_CHAR) :: buf(*)
            integer(C_SIZE_T), value :: bufsize
          end function
        end interface
      integer :: pid, i, idx
      integer(C_SIZE_T) :: szret
      character(4096) :: path
      character(KIND = C_CHAR) :: cbuf(4096)
      pid = GETPID()
      write (path, '(i0)') pid
      path='/proc/'//TRIM(path)//'/exe'
      szret = readlink(TRIM(path)//C_NULL_CHAR, cbuf, SIZE(cbuf,KIND = C_SIZE_T))
      if (szret == -1) stop 'Error: Could not find the exe path!'
      hm_buffer=''
      do i = 1, SIZE(cbuf)
        if (cbuf(i) == C_NULL_CHAR) exit
        hm_buffer(i:i) = cbuf(i)
      enddo
      idx=len_trim(hm_buffer)
      allocate(character(len=idx) :: hypermac_exe)
      hypermac_exe=trim(hm_buffer)
      idx = INDEX(hypermac_exe, '/', BACK = .TRUE.)
      idx=len_trim(hypermac_exe(:idx - 1))
      allocate(character(len=idx) :: hypermac_exe_path)
      hypermac_exe_path=hypermac_exe(:idx)
      idx = INDEX(hypermac_exe_path, '/', BACK = .TRUE.)
      idx=len_trim(hypermac_exe_path(:idx - 1))
      allocate(character(len=idx) :: hypermac_root_path)
      hypermac_root_path=hypermac_exe_path(:idx)
      CALL getcwd(hm_buffer)
      idx=len_trim(hm_buffer)
      allocate(character(len=idx) :: hypermac_wd)
      hypermac_wd=trim(hm_buffer)
      call get_environment_variable("JANA2006_DIR", hm_buffer)
      idx=len_trim(hm_buffer)
      if (idx .EQ. 0) then
        idx=len_trim(hypermac_root_path)
        allocate(character(len=idx) :: JANA2006_DIR)
        JANA2006_DIR=hypermac_root_path
      else
        allocate(character(len=idx) :: JANA2006_DIR)
        JANA2006_DIR=trim(hm_buffer)
      end if
    end subroutine determine_env_paths
end module hm_io
