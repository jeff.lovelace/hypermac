program test_cmdpath
    use ISO_C_BINDING
!    use IFPORT
         
    implicit none
    interface
        function readlink(path, buf, bufsize) bind(C, NAME = 'readlink')
            import
            integer(C_SIZE_T) :: readlink
            character(KIND = C_CHAR), intent(IN) :: path(*)
            character(KIND = C_CHAR) :: buf(*)
            integer(C_SIZE_T), value :: bufsize
        end function
    end interface
         
    integer :: pid, i, idx
    integer(C_SIZE_T) :: szret
    character(256) :: path
    character(KIND = C_CHAR) :: cbuf(256)
 
    pid = GETPID()
         
    write (path, '(i0)') pid
    path = '/proc/'//TRIM(path)//'/exe'
         
    szret = readlink(TRIM(path)//C_NULL_CHAR, cbuf, SIZE(cbuf,KIND = C_SIZE_T))
    if (szret == -1) stop 'Error reading link'
    path = ''
    do i = 1, SIZE(cbuf)
        if (cbuf(i) == C_NULL_CHAR) exit
            path(i:i) = cbuf(i)
    enddo
         
    print '("Command full path: ", A)', TRIM(path)
         
    idx = INDEX(path, '/', BACK = .TRUE.)
        
    print '("Command directory: ", A)', TRIM(path(:idx - 1))
         
end program test_cmdpath
