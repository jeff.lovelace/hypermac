program test
  use hm_nl
  implicit none
  integer(kind=4)::i
  character(16) :: header="0%         100%"
  interface
    subroutine progress(a,skipspace)
      integer(kind=4),intent(in) :: a
      integer(kind=4),intent(in),optional :: skipspace
    end subroutine progress
  end interface
  open (unit=6)
  open (unit=20,file="test.txt",access="stream")
  write(6,'(a)') header//nl
  write(20) header//lf

  call progress(0,1)
  do i=1,101,5
    ! Just spend some cpu time to calculate pi.
    ! This could be replaced by non-standard subroutine sleep(n).
    call sleep(1)
    call progress(i) ! generate the progress bar.
  enddo
  write(6,'(a)') ""
  write(20) lf
  close(20)
end

subroutine progress(a,skipbackspace)
  implicit none
  integer(kind=4)::z,j,k,curpos,skip
  integer(kind=4),intent(in) :: a
  integer,optional :: skipbackspace
  character(21)::bar
  bar="|          |(###.##%)"
  z=a
  if (present(skipbackspace)) then
    skip = skipbackspace
  else
    skip = 0
  endif
  if (z.lt.0) then
    z=0.00
  endif
  if (z.gt.100) then
    z=100.0
  endif
  j=int(real(z)/101.00*10.0)
  write(unit=bar(14:19),fmt="(f6.2)") z*1.0
  do k=1, j
    bar(1+k:1+k)="="
  enddo
  if ((j.lt.10).and.(z.ne.0)) then 
    bar((j+2):(j+2))=">"
  endif
  ! print the progress bar.
  write(unit=6,fmt="(a1,a)",advance="no") char(13), bar
  if (skip.eq.1) then
    write(unit=20) bar
  else
    inquire(unit=20,pos=curpos)
    write(unit=20,POS=curpos-len(bar))
    write(unit=20) bar
    flush(20)
  endif
  return
end subroutine progress
