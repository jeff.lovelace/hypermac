      program RefineBatch
      include 'fepc.cmn'
      include 'basic.cmn'
      logical RefineEnd,ExistFile,EqIgCase
      character*256 s256,p256,t256
      Console=.true.
      BatchMode=.false.
      RunningProgram='refine'
      call SetBasicConstants
      call SetFePCConstants

#ifdef __gfortran__
      OpSystem=1
      DirectoryDelimitor='/'
      HomeDir=GetHomeDir()
#else
      call System('set > set.txt')
      ln=NextLogicNumber()
      call OpenFile(ln,'set.txt','formatted','unknown')
500   read(ln,FormA,end=600) s256
      write(*,*) "S256: ",s256
      if(LocateSubstring(s256,'JANA2006DIR',.false.,.false.).eq.1)
     1  then
        i=index(s256,'=')
        if(i.gt.0) then
          HomeDir=s256(i+1:)
        else
          go to 500
        endif
      else
        go to 500
      endif
600   call CloseIfOpened(ln)
      call DeleteFile('set.txt')
#endif

      VasekDebug=0
      HKLUpdate=.false.
      k=0

#ifdef __gfortran__
      call getcl(CommandLine)
#else
      call get_command(CommandLine)
      call kusap(CommandLine,k,p256)
#endif

      n=0
1000  kp=k
      call kusap(CommandLine,k,p256)
      if(EqIgCase(p256,'-debug')) then
        VasekDebug=1
        n=n+100
      else if(EqIgCase(p256,'-HomeDir')) then
        call kusap(CommandLine,k,HomeDir)
        n=n+10
      else
        if(p256(1:1).ne.'"'.and.k.lt.256) then
1500      i=k
          call kusap(CommandLine,k,s256)
          if(s256(1:1).ne.'@'.and..not.EqIgCase(s256,'-debug').and.
     1       .not.EqIgCase(s256,'-HomeDir')) then
            if(k.lt.256) then
              go to 1500
            else
              i=k
            endif
          endif
          p256=CommandLine(kp+1:i)
          k=i
        endif
        ifln=idel(p256)
        if(ifln.le.0) then
          fln=' '
        else
          call ExtractDirectory(p256,t256)
          call ExtractFileName(p256,s256)
          call GetPureFileName(s256,fln)
          fln=t256(:idel(t256))//fln(:idel(fln))
          ifln=idel(fln)
          n=n+1
        endif
      endif
      if(n.ne.111.and.k.lt.256) go to 1000
      if(ifln.gt.0) then
        do i=1,9
          ExistMFile(i)=ExistFile(fln(:ifln)//ExtMFile(i))
        enddo
      else
        ExistMFile=.false.
      endif
      ShowInfoOnScreen=.true.
      SilentRun=.false.
      AniNaListing=.false.
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9000
      call iom90(0,fln(:ifln)//'.m90')
      if(ErrFlag.ne.0) go to 9000
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.ne.0) go to 9000
      call iom90(0,fln(:ifln)//'.m90')
      if(ErrFlag.ne.0) go to 9000
      if(NDatBlock.gt.1) then
        do i=1,NDatBlock
          MenuDatBlock(i)=DatBlockName(i)
          if(DataType(i).eq.1) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->single crystal'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->powder'
          endif
          if(Radiation(i).eq.NeutronRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/neutrons'
          else if(Radiation(i).eq.XRayRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/X-ray'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/electron'
          endif
        enddo
      endif
      KPhase=1
      call NewPg(1)
      write(6,FormA)
      call Refine(0,RefineEnd)
9000  pause 'Hotovo'
      stop
      end
      include 'datablockfepc.cmn'
      include 'datablockjana.cmn'
