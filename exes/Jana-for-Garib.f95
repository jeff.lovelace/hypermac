      module Garib_mod

      real, parameter :: EPiSq=78.95683521,tpisq=19.7392088,torad=0.017453293,&
                         pi=3.141592654,pi2=6.283185308
      integer, parameter :: MaxAt=1000,MaxDerAtom=100,MaxSymm=96
      integer, parameter :: MaxAtType=10, mxb=50, mxw=8

      integer :: TRank(0:6)=(/1,3,6,10,15,21,28/),&
                 TRankCumul(0:6)=(/1,4,10,20,35,56,84/)

      real DifBess

      integer NDim,NDimI,NDimQ
      real CellPar(6),PrCP(6),Qu(3),QuI(3),QuR(3),cp(6),rcp(6),urcp(6)
      integer kw(3,mxw)

      integer NSymm,imnp(MaxSymm)
      real RM4(4,4,MaxSymm),S4(4,MaxSymm),RM3(3,3,MaxSymm),CentrRef

      integer NAtType
      real FormFactor(9,MaxAtType),fx(MaxAtType),ffia(MaxAtType),ffra(MaxAtType)
      character*2 AtType(MaxAtType)

      character*8 Atom(MaxAt)
      integer NAtoms,isf(MaxAt),itf(MaxAt),KModA(3,MaxAt),KFA(3,MaxAt),&
              PrvniKiAtomu(MaxAt),DelkaKiAtomu(MaxAt),MaxUsedKw,ndoff
      real ai(MaxAt),a0(MaxAt),ax(mxw,MaxAt),ay(mxw,MaxAt),X(3,MaxAt),&
           Ux(1:3,mxw,MaxAt),Uy(1:3,mxw,MaxAt),Beta(6,MaxAt),fyr(MaxAt),&
           qcnt(3,MaxAt),ScaleFactor
      logical nemod


      real ath(MaxSymm),atk(MaxSymm),atl(MaxSymm),rh(MaxSymm),rk(MaxSymm),&
           rl(MaxSymm),tsymm(MaxSymm),HCoef(10,MaxSymm),sinthl,sinthlq

      real ader(MaxDerAtom),bder(MaxDerAtom),hj(3),HCoefj(84),par(MaxDerAtom),&
           expij,PhiAng,sinij,cosij,FCalc,der(MaxDerAtom*MaxAt+1),&
           dc(MaxDerAtom*MaxAt+1)
      integer NDerModFirst,NDerModLast,mj,nj,pj

      real, allocatable :: besp(:,:),dbfdu1(:,:),dbfdu2(:,:),&
                           dchidu1(:),dchidu2(:),dsdu1(:),dsdu2(:),chi(:),&
                           dadu1(:),dadu2(:),dbdu1(:),dbdu2(:),dsdchi(:),&
                           sb(:),dsbdax(:),dsbday(:),ksi(:),dksidax(:),&
                           dksiday(:),dadax(:),daday(:),dbdax(:),dbday(:),&
                           besb(:,:),dbfdb1(:,:),dbfdb2(:,:),&
                           detadb1(:),detadb2(:),dsdb1(:),dsdb2(:),eta(:),&
                           dadb1(:),dadb2(:),dbdb1(:),dbdb2(:),dsdeta(:),&
                           dsdb3(:),dsdu3(:),dbfdb3(:,:),dbfdu3(:,:),&
                           daxddlt(:),dayddlt(:),daxdx40(:,:),daydx40(:,:),&
                           daxdfct(:,:),daydfct(:,:),accur(:)
      integer, allocatable :: mx(:),mxs(:),mxkk(:),&
                              mb(:),mbs(:),mbk(:),mxdkw1(:),mxdkw2(:),&
                              mxdkw3(:),mbdkw1(:),mbdkw2(:),mbdkw3(:)
      logical kolaps

      end module


      program Jana_for_Garib
      use garib_mod
      character*256 Fln,Veta
      character*80  LabelA
      character*20 :: Label(7) = (/'cell      ',&
                                   'qi        ',&
                                   'qr        ',&
                                   'symmetry  ',&
                                   'lattice   ',&
                                   'atom      ',&
                                   'end       '/)
      character*8 :: SmbC='PABCIRFX'
      character*4 :: Centering
      integer ErrFlag,HRead(1:4),ihkl(4)
      logical EqIgCase,EqIV0
      real Intensity,SIntensity
      real hhp(6),hp,kp,lp
      Fln='/home/iplt/hypermac/src/hypermac/examples/garib/Garib-1'
      iFln=idel(Fln)
      open(50,file=Fln(:iFln)//'.m50g')
      kw=0
      do i=1,mxw
        kw(1,i)=i
      enddo
      MaxUsedKw=1
      DifBess=.00001
      if(allocated(besp))&
        deallocate(besp,dbfdu1,dbfdu2,dchidu1,dchidu2,dsdu1,dsdu2,chi,&
                   dadu1,dadu2,dbdu1,dbdu2,dsdchi,sb,dsbdax,dsbday,&
                   ksi,dksidax,dksiday,dadax,daday,dbdax,dbday,besb,&
                   dbfdb1,dbfdb2,detadb1,detadb2,dsdb1,dsdb2,eta,&
                   dadb1,dadb2,dbdb1,dbdb2,dsdeta,dsdb3,dsdu3,dbfdb3,&
                   dbfdu3,daxddlt,dayddlt,daxdx40,daydx40,daxdfct,&
                   daydfct,mx,mxs,mxkk,mb,mbs,mbk,mxdkw1,mxdkw2,&
                   mxdkw3,mbdkw1,mbdkw2,mbdkw3,accur)
      n=max(MaxUsedKw,1)
      allocate(besp(2*mxb+1,n),dbfdu1(2*mxb+1,n),&
            dbfdu2(2*mxb+1,n),dchidu1(n),dchidu2(n),dsdu1(n),&
            dsdu2(n),chi(n),dadu1(n),dadu2(n),dbdu1(n),dbdu2(n),&
            dsdchi(n),sb(2*mxb+1),dsbdax(n),dsbday(n),ksi(n),&
            dksidax(n),dksiday(n),dadax(n),daday(n),dbdax(n),&
            dbday(n),besb(mxb+1,n),dbfdb1(mxb+1,n),dbfdb2(mxb+1,n),&
            detadb1(n),detadb2(n),dsdb1(n),dsdb2(n),eta(n),dadb1(n),&
            dadb2(n),dbdb1(n),dbdb2(n),dsdeta(n),dsdb3(n),dsdu3(n),&
            dbfdb3(2*mxb+1,n),dbfdu3(2*mxb+1,n),daxddlt(n),&
            dayddlt(n),daxdx40(n,n),daydx40(n,n),daxdfct(n,n),&
            daydfct(n,n),mx(n),mxs(n),mxkk(n),mb(n),mbs(n),mbk(n),&
            mxdkw1(n),mxdkw2(n),mxdkw3(n),mbdkw1(n),mbdkw2(n),&
            mbdkw3(n),accur(mxb))
      pom=1.
      do i=1,mxb
        pom=pom*float(i)*.125
        accur(i)=16.*(DifBess*pom)**(1./float(i))
      enddo
      QuR=0.
      NSymm=0
      NAtType=0
      NDim=4
      NDimI=1
      NDimQ=16
      CentrRef=1.
1100  read(50,100,end=1500) Veta
      k=0
      call Kus(Veta,k,LabelA)
      do i=1,7
        if(EqIgCase(LabelA,Label(i))) then
          if(i.eq.1) then
            read(Veta(k+1:),*) CellPar
          else if(i.eq.2) then
            read(Veta(k+1:),*) QuI
          else if(i.eq.3) then
            read(Veta(k+1:),*) QuR
          else if(i.eq.4) then
            NSymm=NSymm+1
            call ReadSymm(Veta(k:),rm4(1,1,NSymm),s4(1,NSymm),ErrFlag)
          else if(i.eq.5) then
            call kus(Veta,k,Centering)
            call Velka(Centering)
            j=index(SmbC,Centering(1:1))
            if(j.eq.1) then
              NLattVec=1
            else if(j.le.5) then
              NLattVec=2
            else if(j.eq.6) then
              NLattVec=3
            else if(j.eq.7) then
              NLattVec=4
            else
              NLattVec=0
            endif
            if(NLattVec.gt.0) CentrRef=NLattVec
          else if(i.eq.6) then
            NAtType=NAtType+1
            call Kus(Veta,k,AtType(NAtType))
            read(50,105) FormFactor(1:9,NAtType)
          else if(i.eq.7) then
            go to 1500
          endif
        endif
      enddo
      go to 1100
1500  Qu=QuI+QuR
      cp(1:3)=CellPar(1:3)
      cosa=cos(torad*CellPar(4))
      cosb=cos(torad*CellPar(5))
      cosg=cos(torad*CellPar(6))
      if(abs(cosa).le..000001) cosa=0.
      if(abs(cosb).le..000001) cosb=0.
      if(abs(cosg).le..000001) cosg=0.
      cp(4)=cosa
      cp(5)=cosb
      cp(6)=cosg
      call recip(cp,rcp,CellVol)
      do j=1,3
        urcp(j)=tpisq*rcp(j)**2
      enddo
      urcp(4)=tpisq*rcp(1)*rcp(2)
      urcp(5)=tpisq*rcp(1)*rcp(3)
      urcp(6)=tpisq*rcp(2)*rcp(3)
      do j=1,3
        prcp(j)=.25*rcp(j)**2
      enddo
      prcp(4)=.25*rcp(1)*rcp(2)*rcp(6)
      prcp(5)=.25*rcp(1)*rcp(3)*rcp(5)
      prcp(6)=.25*rcp(2)*rcp(3)*rcp(4)
      close(50)
      open(40,file=Fln(:iFln)//'.m40')
      read(40,101) NAtoms
      read(40,102) ScaleFactor
      do i=1,3
        read(40,100)
      enddo
      ndoff=1
      m=1
      do i=1,NAtoms
        PrvniKiAtomu(i)=m
        DelkaKiAtomu(i)=16
        read(40,103) Atom(i),isf(i),itf(i),ai(i),X(1:3,i),Beta(1:6,i)
        a0(i)=1.
        if(itf(i).ge.2) then
          do j=1,6
            pom=urcp(j)
            Beta(j,i)=Beta(j,i)*pom
          enddo
        else
          pom=EPiSq
          Beta(1,i)=Beta(1,i)*pom
        endif
        read(40,104) Ux(1:3,1,i),Uy(1:3,1,i)
        read(40,100)
        KModA(1,i)=0
        KModA(2,i)=1
        KModA(3,i)=0
        qcnt(1,i)=scalmul(qu,X(1,i))
        qcnt(2,i)=0.
        qcnt(3,i)=0.
        KFA(1:3,i)=0
        m=m+16
      enddo
1600  close(50)
      open(90,file=Fln(:iFln)//'.m90g')
      open(60,file=Fln(:iFln)//'.lst')
2000  read(90,106,end=5000) HRead(1:4),Intensity,SIntensity
      if(EqIV0(HRead,NDim)) go to 5000
      call FromIndSinthl(HRead)
      call SetFormF
      do j=1,NSymm
        call IndTr(HRead,rm4(1,1,j),ihkl,NDim)
        do k=1,3
           hhp(k)=float(ihkl(k))+float(ihkl(4))*qu(k)
        enddo
        ath(j)=pi2*float(ihkl(1))
        atk(j)=pi2*float(ihkl(2))
        atl(j)=pi2*float(ihkl(3))
        imnp(j)=ihkl(4)
        hp=hhp(1)
        kp=hhp(2)
        lp=hhp(3)
        rh(j)=hp*pi2
        rk(j)=kp*pi2
        rl(j)=lp*pi2
        pom=0.
        do m=1,NDim
          pom=pom+s4(m,j)*float(HRead(m))
        enddo
        tsymm(j)=pom*pi2
        HCoef( 5,j)=hp**2
        HCoef( 6,j)=kp**2
        HCoef( 7,j)=lp**2
        HCoef( 8,j)=2.*hp*kp
        HCoef( 9,j)=2.*hp*lp
        HCoef(10,j)=2.*kp*lp
      enddo
      call Calc
      write(60,'('' Reflection:'',4i4,f12.4)') HRead(1:4),FCalc
      m=PrvniKiAtomu(1)
      do i=1,NAtoms
        write(60,'('' Atom: '',a8)') Atom(i)
        write(60,'(5e15.5)') der(m:m+DelkaKiAtomu(i)-1)
        m=m+DelkaKiAtomu(i)
      enddo
      go to 2000
5000  close(90)
      close(60)
      stop
100   format(a)
101   format(5i5)
102   format(f9.6)
103   format(a8,2i3,4x,4f9.6/6f9.6)
104   format(6f9.6)
105   format(7f11.6)
106   format(4i4,2f9.2)
      end
      subroutine ReadSymm(Veta,rm6p,s6p,ErrFlag)
      use garib_mod
      dimension rm6p(*),s6p(*)
      character*256 ErrString
      character*80 t80
      character*15 Cislo
      character*2 :: smbx6(6) = (/'x1','x2','x3','x4','x5','x6'/)
      character*(*) Veta
      integer ErrFlag
      logical EqIgCase
      call mala(Veta)
      ErrFlag=0
      rm6p(1:NDimQ)=0.
      k=0
      ij=0
      do 5000 i=1,NDim
        ij=i-NDim
        call kus(veta,k,t80)
        idl=idel(t80)
        if(idl.le.0) go to 9000
        do j=1,idl
          if(t80(j:j).eq.'*') t80(j:j)=' '
        enddo
        call zhusti(t80)
        s6p(i)=0.
        do 2000 j=1,NDim
          ij=ij+NDim
          kk=index(t80,smbx6(j))
          if(kk.eq.0) then
            rm6p(ij)=0.
            go to 2000
          endif
          if(kk.gt.1) then
            kp=kk-1
1100        if(t80(kp:kp).ne.'+'.and.t80(kp:kp).ne.'-') then
              if(kp.eq.1) go to 1150
              kp=kp-1
              go to 1100
            endif
          else
            kp=kk
          endif
1150      if(kp.ne.kk) then
            cislo=t80(kp:kk-1)
            if(cislo.eq.'+') then
              l=1
            else if(cislo.eq.'-') then
              l=-1
            else
              call posun(cislo,0)
              read(cislo,'(i15)',err=9000) l
            endif
          else
            l=1
          endif
          rm6p(ij)=l
          kk=kk+1
          do l=kp,kk
            t80(l:l)=' '
          enddo
          call zhusti(t80)
          if(idel(t80).le.0) go to 5000
2000    continue
        s6p(i)=fract(t80,ich)
        if(index(t80,'.').gt.0) then
          call ToFract(s6p(i),Cislo,24)
          s6p(i)=fract(Cislo,ich)
        endif
        if(ich.ne.0) go to 9000
5000  continue
      go to 9999
9000  ErrString=' A syntactic error in the following symmetry operator: '//Veta
      write(6,'(a)') ErrString(:idel(ErrString))
      ErrFlag=1
9999  return
      end
      subroutine FromIndSinthl(ih)
      use garib_mod
      dimension ih(*),h(3)
      do i=1,3
        h(i)=ih(i)
        do j=1,NDim-3
          h(i)=h(i)+qu(i)*float(ih(j+3))
        enddo
      enddo
      sinthlq=h(1)**2*prcp(1)+h(2)**2*prcp(2)+h(3)**2*prcp(3)+&
              2.*(h(1)*h(2)*prcp(4)+h(1)*h(3)*prcp(5)+h(2)*h(3)*prcp(6))
      sinthl=sqrt(sinthlq)
      return
      end
      function idel(l)
      character*(*) l
      idel=len(l)
1000  if(idel.ne.0) then
        if(l(idel:idel).eq.' ') then
          idel=idel-1
          go to 1000
        endif
      endif
      return
      end
      logical function EqIgCase(St1,St2)
      character*(*) St1,St2
      character*1   CharMal
      id1=idel(St1)
      id2=idel(St2)
      EqIgCase=.false.
      if(id1.ne.id2) go to 9999
      do i=1,id1
        if(CharMal(St1(i:i)).ne.CharMal(St2(i:i))) go to 9999
      enddo
      EqIgCase=.true.
9999  return
      end
      subroutine kus(radka,k,slovo)
      character*(*) radka,slovo
      mxs=len(slovo)
      mxr=len(radka)
      slovo=' '
      l=0
      if(k.ge.mxr) go to 9999
      k=k+1
1000  if(radka(k:k).eq.' '.or.ichar(radka(k:k)).eq.9) then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 1000
      endif
2000  if(radka(k:k).ne.' '.and.ichar(radka(k:k)).ne.9) then
        if(l.lt.mxs) then
          l=l+1
          slovo(l:l)=radka(k:k)
        endif
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 2000
      endif
3000  if(radka(k:k).eq.' '.or.ichar(radka(k:k)).eq.9) then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 3000
      endif
      k=k-1
9999  return
      end
      function CharMal(St)
      character*1 charmal,St
      CharMal=St
      call mala(CharMal)
      return
      end
      subroutine Mala(Veta)
      character*(*) Veta
      character*80 Pismena(2)
      data Pismena&
           /'abcdefghijklmnopqrstuvwxyz',&
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      IndIn=2
      IndOut=1
      go to 1000
      entry Velka(Veta)
      IndIn=1
      IndOut=2
1000  idl=44
      do i=1,idel(Veta)
        j=index(Pismena(IndIn)(:idl),Veta(i:i))
        if(j.gt.0) Veta(i:i)=Pismena(IndOut)(j:j)
      enddo
      return
      end
      subroutine zhusti(s)
      character*(*) s
      idl=idel(s)
      k=0
      do i=1,idl
        if(s(i:i).eq.' ') cycle
        k=k+1
        if(k.ne.i) then
          s(k:k)=s(i:i)
          s(i:i)=' '
        endif
      enddo
      return
      end
      subroutine posun(cislo,tecka)
      character*(*) cislo
      integer tecka
      l=idel(cislo)
      m=len(cislo)
      if(tecka.eq.1.and.index(cislo,'.').le.0.and.l.lt.m) then
        l=l+1
        cislo(l:l)='.'
      endif
      if(l.lt.m) then
        do i=l,1,-1
          cislo(m-l+i:m-l+i)=cislo(i:i)
          cislo(i:i)=' '
        enddo
      endif
      return
      end
      function fract(veta,ich)
      character*(*) veta
      character*15 pomc
      character*5 format
      ich=0
      pomc=veta
      idl=idel(pomc)
      i=index(pomc,'/')
      if(i.le.0) then
        call posun(pomc,1)
        read(pomc,'(f15.0)',err=9000) fract
      else
        format='(i  )'
        if(i.eq.1.or.i.eq.idel(pomc)) then
          fract=0.
          go to 9999
        endif
        write(format(3:4),100) i-1
        read(pomc(1:i-1),format,err=9000) i1
        write(format(3:4),100) idl-i
        read(pomc(i+1:idl),format,err=9000) i2
        fract=float(i1)/float(i2)
      endif
      go to 9999
9000  ich=1
9999  return
100   format(i2)
      end
      subroutine ToFract(x,st,n)
      character*(*) st
      character*10  t10
      do k=1,n
        fk=float(k)
        p=x*fk
        l=nint(p)
        if(abs(p-float(l)).lt..001) go to 1100
      enddo
      go to 9000
1100  if(l.gt.99) go to 9000
      write(t10,'(i3,''/'',i2)') l,k
      if(k.eq.1) t10(4:)='  '
      call zhusti(t10)
      st=t10
      go to 9999
9000  st='?/?'
9999  return
      end
      subroutine recip(cp,rcp,vr)
      dimension cp(6),rcp(6)
      ar=cp(1)
      br=cp(2)
      cr=cp(3)
      cosar=cp(4)
      cosbr=cp(5)
      cosgr=cp(6)
      vr=1.+2.*cosar*cosbr*cosgr-cosar**2-cosbr**2-cosgr**2
      if(vr.le.0) return
      vr=ar*br*cr*sqrt(vr)
      sinar=sqrt(1.-cosar**2)
      sinbr=sqrt(1.-cosbr**2)
      singr=sqrt(1.-cosgr**2)
      a=br*cr*sinar/vr
      b=cr*ar*sinbr/vr
      c=ar*br*singr/vr
      cosa=(cosbr*cosgr-cosar)/(sinbr*singr)
      cosb=(cosgr*cosar-cosbr)/(singr*sinar)
      cosc=(cosar*cosbr-cosgr)/(sinar*sinbr)
      rcp(1)=a
      rcp(2)=b
      rcp(3)=c
      rcp(4)=cosa
      rcp(5)=cosb
      rcp(6)=cosc
      return
      end
      logical function eqiv0(a,n)
      integer a(n)
      eqiv0=.false.
      do i=1,n
        if(a(i).ne.0) go to 2000
      enddo
      eqiv0=.true.
2000  return
      end
      subroutine Calc
      use garib_mod
      dimension hp(3),h(3),SpH(64),smp(3)
      character*80 ch80,t80
      logical izo,main
      NDerAll=NAtoms*16+1
      a=0.
      b=0.
      af=0.
      bf=0.
      aanom=0.
      banom=0.
      m=1
      NDerModFirst=11
      NDerModLast=16
      do i=1,NAtoms
        aii=ai(i)
        if(NDim.gt.3) call calcm1(i,0)
        itfi=itf(i)
        izo=itfi.eq.1
        l=0
        do n=0,max(itfi,2)
          if(n.eq.0) then
            nrank=1
          else if(n.eq.1) then
            nrank=3
          else
            nrank=6
          endif
          do j=1,nrank
            l=l+1
            if(n.eq.0) then
              par(l)=aii
            else if(n.eq.1) then
              par(l)=X(j,i)
            else if(n.eq.2) then
              if(itf(i).le.1.and.j.ge.2) then
                par(l)=0.
              else
                par(l)=beta(j,i)
              endif
            endif
          enddo
        enddo
        if(NDim.gt.3) then
          mm=0
          lx=l
          do n=2,2
            nrank=3
            kmod=1
            if(kmod.le.0) cycle
            mm=mm+1
            do k=1,kmod
              do j=1,nrank
                lx=lx+1
                ly=lx+nrank
                par(lx)=ux(j,k,i)
                par(ly)=uy(j,k,i)
              enddo
              lx=ly
            enddo
          enddo
        endif
        n=NDerModLast
        call SetRealArrayTo(ader,n,0.)
        call SetRealArrayTo(bder,n,0.)
        ader1=0.
        bder1=0.
        ader10=0.
        bder10=0.
        aderst=0.
        bderst=0.
        aderfreest=0.
        bderfreest=0.
        scosijb=0.
        ssinijb=0.
        expij=1.
        isfi=isf(i)
        fxi=fx(isfi)
        fyri=ffia(isfi)
        fxrfree=fxi
        fxr0=fxi
        fxr=fxr0+ffra(isfi)
        fxrfree=fxrfree+ffra(isfi)
        js=1
        do j=1,NSymm
          call CopyVek(HCoef(5,j),HCoefJ(5),6)
          hj(1)=rh(j)
          hj(2)=rk(j)
          hj(3)=rl(j)
          if(NDim.gt.3) then
            mj=imnp(j)
            main=mj.eq.0
            athj=ath(j)
            atkj=atk(j)
            atlj=atl(j)
          else
            athj=hj(1)
            atkj=hj(2)
            atlj=hj(3)
          endif
          HCoefj(2)=athj
          HCoefj(3)=atkj
          HCoefj(4)=atlj
          PhiAng=tsymm(j)
          do l=2,4
            PhiAng=PhiAng+HCoefj(l)*par(l)
          enddo
          PhiAng=PhiAng-anint(PhiAng/pi2)*pi2
          if(.not.izo) then
            pom=0.
            do l=5,10
              pom=pom-HCoefj(l)*par(l)
            enddo
            expij=ExpJana(pom,ich)
            if(expij.gt.1000..or.ich.ne.0) cycle
          endif
          if(NDim.le.3) then
            sinijst=sin(PhiAng)
            cosijst=cos(PhiAng)
            sinij=sinijst*expij
            cosij=cosijst*expij
          else
            sinijst=0.
            cosijst=0.
            call calcm1(i,1)
          endif
          sinija=sinij
          cosija=cosij
          sinij0=sinij
          cosij0=cosij
          sinijb=sinij
          cosijb=cosij
          sinijast=sinijst
          cosijast=cosijst
          cosijp=cosij
          sinijp=sinij
          cosij=cosijp*fxr
          sinij=sinijp*fxr
          cosij0=cosijp*fxr0
          sinij0=sinijp*fxr0
          cosijst=cosijst*fxr
          sinijst=sinijst*fxr
          ader(1)=ader(1)+cosij
          ader10=ader10+cosij0
          scosijb=scosijb+cosijb
          bder(1)=bder(1)+sinij
          bder10=bder10+sinij0
          ssinijb=ssinijb+sinijb
          do l=2,4
            pom=HCoefj(l)
            ader(l)=ader(l)-pom*sinij
            bder(l)=bder(l)+pom*cosij
          enddo
          if(.not.izo) then
            do l=5,10
              pom=-HCoefj(l)
              ader(l)=ader(l)+pom*cosij
              bder(l)=bder(l)+pom*sinij
            enddo
          else
            l=10
          endif
          call calcm1(i,2)
        enddo
        fta=aii
        ftast=fta
        if(izo) then
          tfi=ExpJana(-sinthlq*par(5),ich)
          if(tfi.gt.1000..or.ich.ne.0) cycle
          fta=fta*tfi
        endif
        ftacos =fta*ader(1)
        ftacos0=fta*ader10
        ftasin =fta*bder(1)
        ftasin0=fta*bder10
        a =a+ ftacos
        af=af+ftacos0
        aanom=aanom+fyri*fta*scosijb
        b =b +ftasin
        bf=bf+ftasin0
        banom=banom+fyri*fta*ssinijb
        if(abs(fxr).gt..001) then
          fyr(i)=fyri/fxr
        else
          fyr(i)=0.
        endif
        if(izo) then
          ader(5)=-sinthlq*ader(1)
          bder(5)=-sinthlq*bder(1)
        endif
        if(NDim.gt.3) call calcm1(i,3)
        ader(1)=ader(1)/aii
        bder(1)=bder(1)/aii
        l=m
        ftap=fta
        do n=1,NDerModLast
          if(n.ge.NDerModFirst.and.n.le.NDerModLast) then
            fta=ftap*fxr
          else
            fta=ftap
          endif
          der(l)=ader(n)*fta
          dc(l)=bder(n)*fta
          l=l+1
        enddo
        m=m+16
      enddo
      a=a*CentrRef
      b=b*CentrRef
      af=af*CentrRef
      bf=bf*CentrRef
      a=a-banom*CentrRef
      b=b+aanom*CentrRef
      sqrtab=sqrt(a**2+b**2)
      IZdvih=0
      der(1+IZdvih)=sqrtab
      sciq=ScaleFactor
      FCalc=sqrtab*sciq
      ACalc=a*sciq
      BCalc=b*sciq
      PhiPom=1.
6100  if(sqrtab.ne.0.) sqrtab=sciq/sqrtab
      c1=a*sqrtab*CentrRef*PhiPom
      c2=b*sqrtab*CentrRef*PhiPom
      m=1
      do i=1,NAtoms
        ca=c1+fyr(i)*c2
        cb=c2-fyr(i)*c1
        j=PrvniKiAtomu(i)
        do m=j,j+DelkaKiAtomu(i)-1
          der(m)=ca*der(m)+cb*dc(m)
        enddo
      enddo
9999  return
      end
      subroutine Calcm1(i,klic)
      use garib_mod
      save qcnt1,qcnt2,qcnt3,sumdlt,rstart,mss,msk,dadu3,dadu4,dbdu3,&
           dbdu4,kmodsim,dada0,dbda0,itfi,kfsi,kfxi,kfbi,kmodsi,kmodxi,&
           kmodbi,kmezx,kmezb
      if(klic.eq.0) then
        qcnt1=qcnt(1,i)
        qcnt2=qcnt(2,i)
        qcnt3=qcnt(3,i)
        itfi=itf(i)
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
          kfsi=KFA(1,i)
        else
          kfsi=0
        endif
        if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
          kfxi=KFA(2,i)
        else
          kfxi=0
        endif
        kfbi=KFA(3,i)
        if(kfxi.eq.0) then
          kmezx=kmodxi
        else if(kfxi.eq.1.or.kfxi.eq.4) then
          kmezx=kmodxi-1
        else if(kfxi.eq.2.or.kfxi.eq.5) then
          kmezx=kmodxi-2
        else if(kfxi.eq.3.or.kfxi.eq.6) then
          kmezx=kmodxi-3
        endif
        if(kfbi.eq.0) then
          kmezb=kmodbi
        else if(kfbi.eq.1) then
          kmezb=kmodbi-1
        endif
        if(kfsi.eq.0) then
          kmodsim=kmodsi
        else
          kmodsim=MaxUsedKw
        endif
        if(kmodsim.gt.0) then
          dada0=0.
          dbda0=0.
          do l=1,kmodsim
            dadax(l)=0.
            daday(l)=0.
            dbdax(l)=0.
            dbday(l)=0.
          enddo
        endif
        nemod=kmodxi.le.0.and.kmodsim.le.0.and.kmodbi.le.0
        if(kfsi.eq.0) then
          sb(1)=a0(i)
        else
          sumdlt=1.
          do j=1,kmodsim
            daxddlt(j)=0.
            dayddlt(j)=0.
          enddo
          delta=a0(i)
          sb(1)=delta*sumdlt
        endif
        rstart=abs(sb(1))
        do j=1,kmodsim
          if(kfsi.eq.0) then
            axp=ax(j,i)
            ayp=ay(j,i)
          else
            axp=0.
            ayp=0.
            do k=1,kmodsi
              call smod(u,axp,ayp,daxddlt(j),dayddlt(j),daxdfct(k,j),&
                        daydfct(k,j),daxdx40(k,j),daydx40(k,j),delta,&
                        ax(1,i),ay(1,i),kw(1,j),kfsi)
            enddo
          endif
          uqr=axp**2+ayp**2
          u=sqrt(uqr)
          if(uqr.gt.0) then
            ksi(j)=atan2(-axp,ayp)
            uqr=1./uqr
            pom=u*.5
            sb(j+1)=pom
            dsbdax(j)=axp/(u*pom)*.5
            dsbday(j)=ayp/(u*pom)*.5
            dksidax(j)=-ayp*uqr
            dksiday(j)= axp*uqr
          else
            sb(j+1)=0.
            ksi(j)=0.
            dsbdax(j)=0.
            dsbday(j)=0.
            dksidax(j)=0.
            dksiday(j)=0.
          endif
          rstart=max(abs(sb(j+1)),rstart)
        enddo
        n=max(MaxUsedKw,1)
        call SetIntArrayTo(mxs,n,0)
        call SetIntArrayTo(mxkk,n,0)
        call SetIntArrayTo(mbs,n,0)
        call SetIntArrayTo(mbk,n,0)
        mss=-kmodsim
        msk= kmodsim
      else if(klic.eq.1) then
        chi(1)=0.
        if(.not.nemod) redukce=rstart
        kk=NDerModFirst-1
        if(kmodsi.gt.0) kk=kk+1+2*kmodsi
        do k=1,kmodxi
          u1=0.
          do l=1,3
            kk=kk+1
            u1=u1+par(kk)*hj(l)
          enddo
          if(k.le.kmezx) then
            u2=0.
            do l=1,3
              kk=kk+1
              u2=u2+par(kk)*hj(l)
            enddo
            uqr=u1**2+u2**2
            if(uqr.le..00001) then
              mxs(k)=0
              mxkk(k)=0
              do l=0,mxkk(k)
                if(l.eq.0) then
                  besp(l+mxb+1,k)=1.
                  dbfdu1(l+mxb+1,k)=0.
                  dbfdu2(l+mxb+1,k)=0.
                else
                  if(l.eq.1) then
                    besp( l+mxb+1,k)=0.
                    besp(-l+mxb+1,k)=0.
                    dbfdu1( l+mxb+1,k)= .5
                    dbfdu1(-l+mxb+1,k)=-.5
                    dbfdu2( l+mxb+1,k)= .5
                    dbfdu2(-l+mxb+1,k)=-.5
                  else
                    besp( l+mxb+1,k)=0.
                    besp(-l+mxb+1,k)=0.
                    dbfdu1( l+mxb+1,k)=0.
                    dbfdu1(-l+mxb+1,k)=0.
                    dbfdu2( l+mxb+1,k)=0.
                    dbfdu2(-l+5,k)=0.
                  endif
                endif
              enddo
              chi(k)=0.
              dchidu1(k)=0.
              dchidu2(k)=0.
            else
              u=sqrt(uqr)
              do l=1,mxb
                if(u.lt.accur(l)/redukce) go to 2011
              enddo
              l=mxb
              kolaps=.true.
2011          mxs(k)=0
              mxkk(k)=l
              ru=1./u
              uqr=1./uqr
              chi(k)=atan2(u2,u1)
              call bessj(u,mxkk(k),besp(mxb+1,k),pomb)
              pomc=abs(besp(mxb+1,k))
              do l=1,mxkk(k)
                pom=besp(mxb+1+l,k)
                pomc=max(pomc,abs(pom))
                if(mod(l,2).eq.1) pom=-pom
                besp(-l+mxb+1,k)=pom
              enddo
              redukce=redukce*pomc
              pomb=pomb*ru
              do l=mxkk(k),0,-1
                pomc=besp(l+mxb+1,k)
                poma=pomc*ru
                if(pomc.ne.0.) pomc=1./pomc
                pom=-pomb
                if(l.ne.0) then
                  pom=pom+float(l)*ru*poma
                  pomu1=pom*u1*pomc
                  pomu2=pom*u2*pomc
                  dbfdu1(l+mxb+1,k)=pomu1
                  dbfdu2(l+mxb+1,k)=pomu2
                  dbfdu1(-l+mxb+1,k)=pomu1
                  dbfdu2(-l+mxb+1,k)=pomu2
                else
                  dbfdu1(mxb+1,k)=pom*u1*pomc
                  dbfdu2(mxb+1,k)=pom*u2*pomc
                endif
                pomb=poma
              enddo
              dchidu1(k)=-uqr*u2
              dchidu2(k)= uqr*u1
            endif
          else
            kk=kk+1
            chi(k)=pi2*par(kk)
            kk=kk+1
            delta=par(kk)
            kk=kk+1
            if(kmodxi.eq.1.and.kmodsim.eq.0.and.kmodbi.eq.0) then
              lp=iabs(mj)
              lk=lp
            else
              lp=0
              lk=mxb
            endif
            isum3=0
            do l=lp,lk
              call xmod(besp(l+mxb+1,k),dbfdu1(l+mxb+1,k),&
                        dbfdu2(l+mxb+1,k),dbfdu3(l+mxb+1,k),&
                        u1,delta,-l,kfxi)
              call xmod(besp(-l+mxb+1,k),dbfdu1(-l+mxb+1,k),&
                       dbfdu2(-l+mxb+1,k),dbfdu3(-l+mxb+1,k),&
                       u1,delta,l,kfxi)
              if(abs(besp(-l+mxb+1,k)).lt.DifBess.and.&
                 abs(besp( l+mxb+1,k)).lt.DifBess) then
               isum3=isum3+1
              else
                isum3=0
              endif
              if(isum3.eq.3) go to 2041
            enddo
            mxs(k)=0
            mxs(k)=-lk
            mxkk(k)= lk
            go to 2042
2041        mxs(k)=0
            mxkk(k)= l-3
2042        dchidu1(k)=pi2
          endif
          mxdkw1(k)=kw(1,k)*(mxkk(k)-mxs(k))
          if(NDimI.gt.1) mxdkw2(k)=kw(2,k)*(mxkk(k)-mxs(k))
          if(NDimI.gt.2) mxdkw3(k)=kw(3,k)*(mxkk(k)-mxs(k))
        enddo
        do k=1,kmodbi
          bb1=0.
          do l=5,10
            kk=kk+1
            bb1=bb1-par(kk)*HCoefj(l)
          enddo
          if(k.le.kmezb) then
            bb2=0.
            do l=5,10
              kk=kk+1
              bb2=bb2-par(kk)*HCoefj(l)
            enddo
            uqr=bb1**2+bb2**2
            if(uqr.le.0.) then
              mbs(k)=0
              mbk(k)=0
              do l=0,mbk(k)
                eta(k)=0.
                detadb1(k)=0.
                detadb2(k)=0.
                if(l.eq.0) then
                  besb(1,k)=1.
                  dbfdb1(1,k)=0.
                  dbfdb2(1,k)=0.
                else
                  if(l.eq.1) then
                    besb(2,k)=0.
                    dbfdb1(2,k)=.5
                    dbfdb2(2,k)=.5
                  else
                    besb(l+1,k)=0.
                    dbfdb1(l+1,k)=0.
                    dbfdb2(l+1,k)=0.
                  endif
                endif
              enddo
            else
              u=sqrt(uqr)
              do l=1,mxb
                if(u.lt.accur(l)/redukce) go to 2071
              enddo
              l=mxb
              kolaps=.true.
2071          mbs(k)=0
              mbk(k)= l
              ru=1./u
              uqr=1./uqr
              eta(k)=atan2(bb2,bb1)
              call bessi(u,mbk(k),besb(1,k),pomb)
              pomc=0.
              do l=0,mbk(k)
                pomc=max(pomc,abs(besb(l+1,k)))
              enddo
              redukce=redukce*pomc
              pomb=pomb*ru
              do l=mbk(k),0,-1
                pomc=besb(l+1,k)
                poma=pomc*ru
                if(pomc.ne.0.) pomc=1./pomc
                pom=pomb
                if(l.ne.0) pom=pom+float(l)*ru*poma
                dbfdb1(l+1,k)=pom*bb1*pomc
                dbfdb2(l+1,k)=pom*bb2*pomc
                pomb=poma
              enddo
              detadb1(k)=-uqr*bb2
              detadb2(k)= uqr*bb1
            endif
          else
            kk=kk+1
            eta(k)=pi2*par(kk)
            kk=kk+1
            delta=par(kk)
            kk=kk+4
            if(kmodbi.eq.1.and.kmodsim.eq.0.and.kmodxi.eq.0) then
              lp=mj
              lk=mj
            else
              lp=0
              lk=mxb
            endif
            isum3=0
            do l=lp,lk
              call bmod(besb(l+1,k),dbfdb1(l+1,k),dbfdb2(l+1,k),&
                        dbfdb3(l+1,k),bb1,delta,delta,l,kfbi)
              if(abs(besb(l+1,k)).lt.DifBess) then
                isum3=isum3+1
              else
                isum3=0
              endif
              if(isum3.eq.3) go to 2091
            enddo
            mbs(k)=0
            mbk(k)= lk
            go to 2092
2091        mbs(k)=0
            mbk(k)= l-3
2092        detadb1(k)=pi2
          endif
          mbdkw1(k)=kw(1,k)*(mbk(k)-mbs(k))
          if(NDimI.gt.1) mbdkw2(k)=kw(2,k)*(mbk(k)-mbs(k))
          if(NDimI.gt.2) mbdkw3(k)=kw(3,k)*(mbk(k)-mbs(k))
        enddo
        fip=PhiAng
        cosij=0.
        sinij=0.
        do k=1,kmodxi
          dadu1(k)=0.
          dadu2(k)=0.
          dbdu1(k)=0.
          dbdu2(k)=0.
        enddo
        dadu3=0.
        dadu4=0.
        dbdu3=0.
        dbdu4=0.
        do k=1,kmodbi
          dadb1(k)=0.
          dadb2(k)=0.
          dbdb1(k)=0.
          dbdb2(k)=0.
        enddo
        dadb3=0.
        dbdb3=0.
        dadb4=0.
        dbdb4=0.
        do 2500 ms=mss,msk
          if(ms.eq.0) then
            kw171=0
            kw172=0
            kw173=0
          else
            k=iabs(ms)
            l=isign(1,ms)
            kw171=kw(1,k)*l
            if(NDimI.gt.1) kw172=kw(2,k)*l
            if(NDimI.gt.2) kw173=kw(3,k)*l
          endif
          do k=NDim-2,kmodxi
            mx(k)=mxs(k)
          enddo
          do k=1,kmodbi
            mb(k)=mbs(k)
          enddo
          mf=mj-kw171
          if(NDimI.gt.1) mg=nj-kw172
          if(NDimI.gt.2) mh=pj-kw173
          do k=NDim-2,kmodxi
            mf=mf-mx(k)*kw(1,k)
            if(NDimI.gt.1) mg=mg-mx(k)*kw(2,k)
            if(NDimI.gt.2) mh=mh-mx(k)*kw(3,k)
          enddo
          do k=1,kmodbi
            mf=mf-mb(k)*kw(1,k)
            if(NDimI.gt.1) mg=mg-mb(k)*kw(2,k)
            if(NDimI.gt.2) mh=mh-mb(k)*kw(3,k)
          enddo
2300      chis=0.
          s=expij
          if(kmodsim.gt.0) then
            mp=iabs(ms)
            if(mp.ne.0) then
              fmp=-sign(1.,float(ms))
              chis=chis+ksi(mp)*fmp
            endif
            poms=sb(mp+1)
            s=s*poms
            if(abs(s).lt.DifBess) go to 2500
            dsdksi=fmp
          endif
          do k=kmodbi,1,-1
            fmp=-mb(k)
            mp=iabs(mb(k))+1
            if(k.gt.kmezb) fmp=-fmp
            s=s*besb(mp,k)
            if(abs(s).lt.DifBess) then
              do l=NDim-2,kmodxi
                mxdif=mxkk(l)-mx(l)
                mf=mf-kw(1,l)*mxdif
                if(NDimI.gt.1) mg=mg-kw(2,l)*mxdif
                if(NDimI.gt.2) mh=mh-kw(3,l)*mxdif
                mx(l)=mxkk(l)
              enddo
              do l=1,k-1
                mbdif=mbk(l)-mb(l)
                mf=mf-kw(1,l)*mbdif
                if(NDimI.gt.1) mg=mg-kw(2,l)*mbdif
                if(NDimI.gt.2) mh=mh-kw(3,l)*mbdif
                mb(l)=mbk(l)
              enddo
              go to 2450
            endif
            chis=chis+fmp*eta(k)
            if(k.le.kmezb) chis=chis-.25*fmp*pi2
            dsdeta(k)=fmp
          enddo
          do k=kmodxi,NDim-2,-1
            fmp=-mx(k)
            mp=-mx(k)+mxb+1
            if(k.gt.kmezx) fmp=-fmp
            s=s*besp(mp,k)
            if(abs(s).lt.DifBess) then
              do l=NDim-2,k-1
                mxdif=mxkk(l)-mx(l)
                mf=mf-kw(1,l)*mxdif
                if(NDimI.gt.1) mg=mg-kw(2,l)*mxdif
                if(NDimI.gt.2) mh=mh-kw(3,l)*mxdif
                mx(l)=mxkk(l)
              enddo
              go to 2450
            endif
            chis=chis+fmp*chi(k)
            dsdchi(k)=fmp
          enddo
          chiss=chis
          ss=s
          mfs=mf
          if(NDimI.gt.1) then
            mgs=mg
            if(NDimI.gt.2) then
              mhs=mh
            endif
          endif
2315      s=ss
          chis=chiss
          mx(1)=mfs
          if(kfxi.eq.1) mx(1)=mx(1)+(kw(1,kmodxi)-1)*mx(kmodxi)
          if(NDimI.gt.1) mx(2)=mgs
          if(NDimI.gt.2) mx(3)=mhs
          if(mx(1).lt.-mxkk(1).or.mx(1).gt.mxkk(1)) go to 2440
          if(NDimI.gt.1) then
            if(mx(2).lt.-mxkk(2).or.mx(2).gt.mxkk(2)) go to 2440
            if(NDimI.gt.2) then
              if(mx(3).lt.-mxkk(3).or.mx(3).gt.mxkk(3)) go to 2440
            endif
          endif
          fmp=-mx(1)
          if(kmodxi.gt.0) then
            mp=-mx(1)+mxb+1
            if(kmezx.lt.1) fmp=-fmp
            s=s*besp(mp,1)
            if(abs(s).lt.DifBess) go to 2440
          endif
          chis=chis+fmp*chi(1)
          dsdchi(1)=fmp
          if(NDimI.gt.1) then
            fmp=-mx(2)
            if(kmodxi.gt.0) then
              mp=-mx(2)+mxb+1
              s=s*besp(mp,2)
              if(abs(s).lt.DifBess) go to 2440
            endif
            chis=chis+fmp*chi(2)
            dsdchi(2)=fmp
            if(NDimI.gt.2) then
              fmp=-mx(3)
              if(kmodxi.gt.0) then
                mp=-mx(3)+mxb+1
                s=s*besp(mp,3)
                if(abs(s).lt.DifBess) go to 2440
              endif
              chis=chis+fmp*chi(3)
              dsdchi(3)=fmp
            endif
          endif
          do k=1,kmodxi
            mp=-mx(k)+mxb+1
            dsdu1(k)=s*dbfdu1(mp,k)
            dsdu2(k)=s*dbfdu2(mp,k)
            if(k.gt.kmezx) dsdu3(k)=s*dbfdu3(mp,k)
          enddo
          do k=1,kmodbi
            mp=iabs(mb(k))+1
            dsdb1(k)=s*dbfdb1(mp,k)
            dsdb2(k)=s*dbfdb2(mp,k)
            if(k.gt.kmezb) dsdb3(k)=s*dbfdb3(mp,k)
          enddo
          if(kmodsim.gt.0) then
            mp=iabs(ms)
            if(mp.eq.0) then
              dsda0=s/poms
            else
              dsdax=s*dsbdax(mp)
              dsday=s*dsbday(mp)
            endif
          endif
          pom=fip+chis
          cosp=cos(pom)
          sinp=sin(pom)
          cosps=cosp*s
          sinps=sinp*s
          cosij=cosij+cosps
          sinij=sinij+sinps
          do k=1,kmodxi
            poma=-dsdchi(k)*sinps
            pomb= dsdchi(k)*cosps
            if(k.le.kmezx) then
              dadu1(k)=dadu1(k)+poma*dchidu1(k)+cosp*dsdu1(k)
              dadu2(k)=dadu2(k)+poma*dchidu2(k)+cosp*dsdu2(k)
            else
              dadu1(k)=dadu1(k)+cosp*dsdu1(k)
              dadu2(k)=dadu2(k)+cosp*dsdu2(k)
              dadu3    =dadu3    +cosp*dsdu3(k)
              dadu4    =dadu4    +poma*dchidu1(k)
            endif
            if(k.le.kmezx) then
              dbdu1(k)=dbdu1(k)+pomb*dchidu1(k)+sinp*dsdu1(k)
              dbdu2(k)=dbdu2(k)+pomb*dchidu2(k)+sinp*dsdu2(k)
            else
              dbdu1(k)=dbdu1(k)+sinp*dsdu1(k)
              dbdu2(k)=dbdu2(k)+sinp*dsdu2(k)
              dbdu3    =dbdu3    +sinp*dsdu3(k)
              dbdu4    =dbdu4    +pomb*dchidu1(k)
            endif
          enddo
          do k=1,kmodbi
            poma=-dsdeta(k)*sinps
            pomb= dsdeta(k)*cosps
            if(k.le.kmezb) then
              dadb1(k)=dadb1(k)+poma*detadb1(k)+cosp*dsdb1(k)
              dadb2(k)=dadb2(k)+poma*detadb2(k)+cosp*dsdb2(k)
            else
              dadb1(k)=dadb1(k)+cosp*dsdb1(k)
              dadb2(k)=dadb2(k)+cosp*dsdb2(k)
              dadb3    =dadb3    +cosp*dsdb3(k)
              dadb4    =dadb4    +poma*detadb1(k)
            endif
            if(k.le.kmezb) then
              dbdb1(k)=dbdb1(k)+pomb*detadb1(k)+sinp*dsdb1(k)
              dbdb2(k)=dbdb2(k)+pomb*detadb2(k)+sinp*dsdb2(k)
            else
              dbdb1(k)=dbdb1(k)+sinp*dsdb1(k)
              dbdb2(k)=dbdb2(k)+sinp*dsdb2(k)
              dbdb3    =dbdb3    +sinp*dsdb3(k)
              dbdb4    =dbdb4    +pomb*detadb1(k)
            endif
          enddo
          if(kmodsi.gt.0) then
            if(mp.ne.0) then
              poma=-dsdksi*sinps
              pomb= dsdksi*cosps
              dadax(mp)=dadax(mp)+poma*dksidax(mp)+cosp*dsdax
              daday(mp)=daday(mp)+poma*dksiday(mp)+cosp*dsday
              dbdax(mp)=dbdax(mp)+pomb*dksidax(mp)+sinp*dsdax
              dbday(mp)=dbday(mp)+pomb*dksiday(mp)+sinp*dsday
            else
              dada0=dada0+dsda0*cosp
              dbda0=dbda0+dsda0*sinp
            endif
          endif
2440      do k=NDim-2,kmodxi
            if(k.gt.kmezx) cycle
            fmp=-mx(k)
            mx(k)=-mx(k)
            if(mod(iabs(mx(k)),2).eq.1) ss=-ss
            chiss=chiss-2.*fmp*chi(k)
            dsdchi(k)=-dsdchi(k)
            mfs=mfs-2*mx(k)*kw(1,k)
            if(NDimI.gt.1) then
              mgs=mgs-2*mx(k)*kw(2,k)
              if(NDimI.gt.2) then
                 mhs=mhs-2*mx(k)*kw(3,k)
               endif
             endif
            if(mx(k).lt.0) go to 2315
          enddo
          do k=1,kmodbi
            fmp=-mb(k)
            mb(k)=-mb(k)
            chiss=chiss-2.*fmp*eta(k)+pi*fmp
            dsdeta(k)=-dsdeta(k)
            mfs=mfs-2*mb(k)*kw(1,k)
            if(NDimI.gt.1) then
              mgs=mgs-2*mb(k)*kw(2,k)
              if(NDimI.gt.2) then
                mhs=mhs-2*mb(k)*kw(3,k)
              endif
            endif
            if(mb(k).lt.0) go to 2315
          enddo
          if(mfs.ne.mf) then
          endif
2450      do k=NDim-2,kmodxi
            if(mx(k).lt.mxkk(k)) then
              mx(k)=mx(k)+1
              mf=mf-kw(1,k)
              if(NDimI.gt.1) mg=mg-kw(2,k)
              if(NDimI.gt.2) mh=mh-kw(3,k)
              go to 2300
            else
              mx(k)=mxs(k)
              mf=mf+mxdkw1(k)
              if(NDimI.gt.1) mg=mg+mxdkw2(k)
              if(NDimI.gt.2) mh=mh+mxdkw3(k)
            endif
          enddo
          do k=1,kmodbi
            if(mb(k).lt.mbk(k)) then
              mb(k)=mb(k)+1
              mf=mf-kw(1,k)
              if(NDimI.gt.1) mg=mg-kw(2,k)
              if(NDimI.gt.2) mh=mh-kw(3,k)
              go to 2300
            else
              mb(k)=mbs(k)
              mf=mf+mbdkw1(k)
              if(NDimI.gt.1) mg=mg+mbdkw2(k)
              if(NDimI.gt.2) mh=mh+mbdkw3(k)
            endif
          enddo
2500    continue
      else if(klic.eq.2) then
        if(.not.nemod) then
          lx=NDerModFirst-1
          if(kmodsi.gt.0) lx=lx+1+2*kmodsi
          do k=1,kmodxi
            apom1=dadu1(k)
            apom2=dadu2(k)
            bpom1=dbdu1(k)
            bpom2=dbdu2(k)
            do l=1,3
              lx=lx+1
              ly=lx+3
              hjl=hj(l)
              ader(lx)=ader(lx)+hjl*apom1
              bder(lx)=bder(lx)+hjl*bpom1
              if(k.le.kmezx.or.l.eq.2) then
                ader(ly)=ader(ly)+hjl*apom2
                bder(ly)=bder(ly)+hjl*bpom2
              else
                if(l.eq.1) then
                  ader(ly)=ader(ly)+dadu4
                  bder(ly)=bder(ly)+dbdu4
                else
                  ader(ly)=ader(ly)+dadu3
                  bder(ly)=bder(ly)+dbdu3
                endif
              endif
            enddo
            lx=ly
          enddo
          do k=1,kmodbi
            apom1=dadb1(k)
            apom2=dadb2(k)
            bpom1=dbdb1(k)
            bpom2=dbdb2(k)
            do l=5,10
              lx=lx+1
              ly=lx+6
              HCoefjl=HCoefj(l)
              ader(lx)=ader(lx)-HCoefjl*apom1
              bder(lx)=bder(lx)-HCoefjl*bpom1
              if(k.le.kmezb.or.l.eq.2) then
                ader(ly)=ader(ly)-HCoefjl*apom2
                bder(ly)=bder(ly)-HCoefjl*bpom2
              else
                if(l.eq.1) then
                  ader(ly)=ader(ly)+dadb4
                  bder(ly)=bder(ly)+dbdu4
                else if(l.eq.3) then
                  ader(ly)=ader(ly)+dadb3
                  bder(ly)=bder(ly)+dbdb3
                endif
              endif
            enddo
            lx=ly
          enddo
        endif
      else if(klic.eq.3) then
        if(kmodsi.gt.0) then
          l=max(TRankCumul(itfi),10)+1
          if(kfsi.eq.0) then
            ader(l)=dada0
            bder(l)=dbda0
            do k=1,kmodsim
              l=l+1
              ader(l)=dadax(k)
              bder(l)=dbdax(k)
              l=l+1
              ader(l)=daday(k)
              bder(l)=dbday(k)
            enddo
          else
            apom=dada0*sumdlt
            bpom=dbda0*sumdlt
            do k=1,MaxUsedKw
              xpom=daxddlt(k)
              ypom=dayddlt(k)
              apom=apom+dadax(k)*xpom+daday(k)*ypom
              bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
            enddo
            ader(l)=apom
            bder(l)=bpom
            l=l+1
            do j=1,kmodsi
              apom=0.
              bpom=0.
              do k=1,MaxUsedKw
                xpom=daxdx40(j,k)
                ypom=daydx40(j,k)
                apom=apom+dadax(k)*xpom+daday(k)*ypom
                bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
              enddo
              ader(l)=apom
              bder(l)=bpom
              l=l+1
              apom=dada0*delta
              bpom=dbda0*delta
              do k=1,MaxUsedKw
                xpom=daxdfct(j,k)
                ypom=daydfct(j,k)
                apom=apom+dadax(k)*xpom+daday(k)*ypom
                bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
              enddo
              ader(l)=apom
              bder(l)=bpom
              l=l+1
            enddo
          endif
        endif
      endif
      return
      end
      function ExpJana(x,ich)
      if(abs(x).lt.88.) then
        ExpJana=exp(x)
        ich=0
      else if(x.gt.0.) then
        ExpJana=3.40e+38
        ich=1
      else
        ExpJana=1.18e-38
        ich=2
      endif
      return
      end
      subroutine CopyVek(a,b,n)
      dimension a(n),b(n)
      do i=1,n
        b(i)=a(i)
      enddo
      return
      end
      subroutine SetRealArrayTo(Array,n,Value)
      dimension Array(*)
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine indtr(ih,trm,ihh,n)
      dimension ih(n),ihh(n),trm(n,n)
      ihh=0
      do i=1,n
        pom=0.
        do j=1,n
          pom=pom+float(ih(j))*trm(j,i)
        enddo
        ihh(i)=nint(pom)
        if(abs(pom-float(ihh(i))).gt..01) then
          ihh(1)=999
          go to 9999
        endif
      enddo
9999  return
      end
      subroutine SetFormF
      use garib_mod
      call SetRealArrayTo(fx,NAtType,0.)
      do i=1,NAtType
        fx(i)=FormFactor(9,i)
        ffia=0.
        ffra=0.
        do k=1,7,2
          arg=-sinthlq*FormFactor(k+1,i)
          if(arg.lt.-60.) cycle
          fx(i)=fx(i)+FormFactor(k,i)*exp(arg)
        enddo
      enddo
      return
      end
      function scalmul(u,vg)
      dimension u(*),vg(*)
      scalmul=0.
      do i=1,3
        scalmul=scalmul+u(i)*vg(i)
      enddo
      return
      end
      subroutine SetIntArrayTo(Array,n,Value)
      integer Array(*),Value
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine smod(u,ax,ay,daxddlt,dayddlt,daxdfct,daydfct,daxdx40,&
                      daydx40,dlt,x40,fct,m,ityp)
      real, parameter :: pi2=6.283185308
      real ksi
      if(ityp.eq.1) then
        arg2=pi2*float(m)
        arg=arg2*.5
        alfa=arg*dlt
        ksi= arg2*x40
        csa=2.*cos(alfa)
        sna=2.*sin(alfa)
        u=sna/arg
        csk=cos(ksi)
        snk=sin(ksi)
        ax=ax+u*snk
        ay=ay+u*csk
        daxdfct= csa*snk*dlt
        daydfct= csa*csk*dlt
        daxddlt=daxddlt+csa*snk
        dayddlt=dayddlt+csa*csk
        daxdx40= u*csk*arg2
        daydx40=-u*snk*arg2
      endif
      return
      end
      subroutine bmod(f,df1,df2,df3,db,d1,d2,m,ityp)
      use garib_mod
      if(ityp.eq.1) then
        pom=exp(db)
        if(m.eq.0) then
          f=d1*pom+d2*(pom-1.)/db-d1-d2+1.
          if(f.ne.0.) then
            df1=(d1*pom+d2*(pom*db-pom+1.)/db**2)/f
            df2=(pom-1.)/f
            df3=((pom-1.)/db-1.)/f
          else
            df1=0.
            df2=0.
            df3=0.
          endif
        else
          arg=pi2*float(m)*.5
          alfa1=arg*d1
          cs1=cos(alfa1)
          sn1=sin(alfa1)
          if(d2.ne.0.) then
            alfa2=arg*d2
            aq=alfa2**2
            cs12=cos(alfa1+alfa2)
            sn12=sin(alfa1+alfa2)
            bq=db**2
            ab=alfa2*db
            fcit=ab*pom*cs1-ab*cs12+bq*pom*sn1-bq*sn12
            fjmen=alfa2*(aq+bq)
            fjq=fjmen**2
            z=fcit/fjmen
            f=d2*z
            if(f.ne.0) then
              df1=d2*((pom*cs1*(ab+alfa2)-alfa2*cs12&
                  +2.*db*(pom*sn1-sn12))*fjmen-2.*ab*fcit)/(fjq*f)
              df2=(-ab*alfa2*pom*sn1+ab*alfa2*sn12+bq*pom*alfa2*cs1&
                  -bq*alfa2*cs12)/(fjmen*f)
              df3=z+((ab*pom*cs1-ab*cs12+alfa2*ab*sn12&
                   -bq*alfa2*cs12)*fjmen-(3.*aq+bq)*alfa2*fcit)/(fjq*f)
            else
              df1=0.
              df2=0.
              df3=0.
            endif
          else
            fcit=sn1/arg
            f=(pom-1.)*fcit
            if(f.ne.0) then
              df1=pom*fcit/f
              df2=(pom-1.)*cs1/f
            else
              df1=0.
              df2=0.
              df3=0.
            endif
          endif
        endif
      else if(ityp.eq.2) then
        if(db.ne.0.) then
          d=sqrt(abs(db))
          pom=sin(d)/d
          pomd=-(cos(d)*d-sin(d))/d**3*.5
        else
          pom=1.
          pomd=0.
        endif
        if(m.eq.0) then
          f=d1*(pom-1.)+1.
          df1=d1*pomd
          df2=pom-1.
        else
          arg=pi2*float(m)*.5
          alfa1=arg*d1
          cs1=cos(alfa1)
          sn1=sin(alfa1)
          fcit=sn1/arg
          f=(pom-1.)*fcit
          df1=pomd*fcit
          df2=(pom-1.)*cs1
          df3=0.
        endif
      endif
      return
      end
      function bessi0(x)
      double precision y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,&
                       q9
      data p1,p2,p3,p4,p5,p6,p7/1.d0,3.5156229d0,3.0899424d0,&
           1.2067492d0,.2659732d0,0.360768d-1,0.45813d-2/
      data q1,q2,q3,q4,q5,q6,q7,q8,q9/0.39894228d0,0.1328592d-1,&
           0.225319d-2,-0.157565d-2,0.916281d-2,-0.2057706d-1,&
           0.2635537d-1,-0.1647633d-1,0.392377d-2/
      if(abs(x).lt.3.75) then
         y=(x/3.75)**2
         bessi0=p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))
      else
          ax=abs(x)
          y=3.75/ax
          bessi0=(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4&
                 +y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
      endif
      return
      end
      function bessi1(x)
      double precision y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,&
                       q9
      data p1,p2,p3,p4,p5,p6,p7/0.5d0,0.87890594d0,0.51498869d0,&
           0.15084934d0,0.2658733d-1,0.301532d-2,0.32411d-3/
      data q1,q2,q3,q4,q5,q6,q7,q8,q9/0.39894228d0,-0.3988024d-1,&
           -0.362018d-2,0.163801d-2,-0.1031555d-1,0.2282967d-1,&
           -0.2895312d-1,0.1787654d-1,-0.420059d-2/
      if(abs(x).lt.3.75) then
         y=(x/3.75)**2
         bessi1=x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
      else
          ax=abs(x)
          y=3.75/ax
          bessi1=(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4&
                 +y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
          if (x.lt.0.)bessi1=-bessi1
      endif
      return
      end
      subroutine bessi(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      dimension besp(*)
      double precision bi,bim,bip
      if(n.lt.0) go to 9999
      if(abs(x).lt.bigni) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        besp(1)=bessi0(x)
        bi=bessi1(x)
        if(n.ge.1) then
          besp(2)=bi
        else
          besp1=bi
          go to 9999
        endif
        tox=2./abs(x)
        m=2*(n+1+int(sqrt(float(iacc*(n+1)))))
        do i=3,n+1
          besp(i)=0.
        enddo
        besp1=0.
        bip=0.
        bi=1.
        do j=m,1,-1
          bim=bip+j*tox*bi
          bip=bi
          bi=bim
          if(abs(bi).gt.bigno) then
            bi=bi*bigni
            bip=bip*bigni
            do i=3,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
          endif
          if(j.le.n.and.j.ge.2) then
            besp(j+1)=bip
          else if(j.eq.n+1) then
            besp1=bip
          endif
        enddo
        bi=besp(1)/bi
        do i=3,n+1
          besp(i)=besp(i)*bi
        enddo
        besp1=besp1*bi
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
      function bessj0(x)
      double precision y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6&
            ,s1,s2,s3,s4,s5,s6
      data p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,&
           -.2073370639d-5,.2093887211d-6/,&
           q1,q2,q3,q4,q5/-.1562499995d-1,.1430488765d-3,&
           -.6911147651d-5,.7621095161d-6,-.934945152d-7/
      data r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,&
           651619640.7d0,-11214424.18d0,77392.33017d0,-184.9052456d0/,&
           s1,s2,s3,s4,s5,s6/57568490411.d0,1029532985.d0,&
           9494680.718d0,59272.64853d0,267.8532712d0,1.d0/
      if(abs(x).lt.8.) then
          y=x**2
          bessj0=(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))&
                 /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
      else
          ax=abs(x)
          z=8./ax
          y=z**2
          xx=ax-.785398164
          bessj0=sqrt(.636619772/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4&
                 +y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
      endif
      return
      end
      function bessj1(x)
      double precision y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6&
            ,s1,s2,s3,s4,s5,s6
      data r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,&
           242396853.1d0,-2972611.439d0,15704.48260d0,&
           -30.16036606d0/,&
           s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,&
           18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
      data p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,&
           .2457520174d-5,-.240337019d-6/,&
           q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,&
           .8449199096d-5,-.88228987d-6,.105787412d-6/
      if (abs(x).lt.8.)then
           y=x**2
           bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))&
                  /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
      else
          ax=abs(x)
          z=8./ax
          y=z**2
          xx=ax-2.356194491
          bessj1=sqrt(.636619772/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4&
                 +y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))&
                 *sign(1.,x)
      endif
      return
      end
      subroutine bessj(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      double precision bj,bjm,bjp
      dimension besp(*)
      if(n.lt.0) go to 9999
      if(x.eq.0.) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        bjm=bessj0(x)
        bj =bessj1(x)
        besp(1)=bjm
        if(n.gt.0) then
          besp(2)=bj
        else
          besp1=bj
          go to 9999
        endif
        ax=abs(x)
        tox=2./ax
        if(x.lt.0.) bj=-bj
        mez=min(ifix(ax),n+1)
        if(mez.lt.2) go to 2000
        do j=1,mez-1
          bjp=j*tox*bj-bjm
          bjm=bj
          bj=bjp
          if(j.lt.n) then
            besp(j+2)=bj
          else
            besp1=bj
          endif
        enddo
        if(mez.ge.n+1) go to 3000
2000     m=2*((n+1+int(sqrt(float(iacc*(n+1)))))/2)
        mez=max(mez,1)
        do i=mez+2,n+1
          besp(i)=0.
        enddo
        besp1=0.
        jsum=0
        sum=0.
        bjp=0.
        bj=1.
        do j=m,1,-1
          bjm=j*tox*bj-bjp
          bjp=bj
          bj=bjm
          if(abs(bj).gt.bigno) then
            bj=bj*bigni
            bjp=bjp*bigni
            do i=mez+2,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
            sum=sum*bigni
          endif
          if(jsum.ne.0) sum=sum+bj
          jsum=1-jsum
          if(j.le.n.and.j.ge.mez+1) then
            besp(j+1)=bjp
          else if(j.eq.n+1) then
            besp1=bjp
          endif
        enddo
        sum=2.*sum-bj
        sum=1./sum
        do i=mez+2,n+1
          besp(i)=besp(i)*sum
        enddo
        besp1=besp1*sum
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
      subroutine xmod(f,df1,df2,df3,u,delta,m,ityp)
      use garib_mod
      if(ityp.le.3) then
        pmd=pi*float(m)*delta
        arg=pmd+u
        if(arg.ne.0.) then
          sn=sin(arg)
          cs=cos(arg)
          v=sn/arg
          f=delta*v
          rf=1./f
          dv=(cs-v)/arg
          df1=delta*dv*rf
          df2=(v+dv*pmd)*rf
        else
          f=delta
          df1=0.
          df2=1./delta
        endif
        df3=0.
      else if(ityp.le.6) then
        pm=pi*float(m)
        pmd=pm*delta
        arg=pmd+u
        if(arg.ne.0.) then
          sn=sin(arg)
          cs=cos(arg)
          den=1./(arg*(arg-pm))
          v=sn*den
          f=v*u
          if(f.gt.0.) then
            rf=1./f
            dv=den*(cs-v*(2.*arg-pm))
            dvu=dv*u
            df1=(v+dvu)*rf
            df2=dvu*pm*rf
          else
            df1=0.
            df2=1.
          endif
        else
          f=1.
          df1=0.
          df2=1.
        endif
      endif
      return
      end
