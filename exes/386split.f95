!     Last change:  VP   28 Dec 2005    1:14 pm
      program split
      dimension ids(9)
      integer ArgCount,i,offset
      character*256 Radka,Radkap,Knihovna,ModulHledany,Modul,string(9),
     1             Filter,JmenoKnihovny,CurrArg
      logical Nalezeno,WriteLeadingCs,ShowDetails
      data string/'subroutine','function','module','integerfunction',
     1            'logicalfunction','characterfunction','realfunction',
     2            'complexfunction','doubleprecisionfunction'/
      do 500i=1,9
        ids(i)=idel(String(i))
500   continue
      WriteLeadingCs = .True.
      ShowDetails = .True.
      call getcl(Radka)
      if (index(Radka,'-Q').ne.0) then
C     Q option is speacial stops all output
        ShowDetails = .False.
      endif
      if (ShowDetails) then
        write(*,'('' '',A)') "Command Line: "//trim(Radka)
      endif
      ArgCount=0
      ArgCount=command_argument_count()
      if (ShowDetails) then
        write(*,'('' '',A,I3)') "Number of Arguments: ",ArgCount
      endif
      if (ArgCount.lt.2) then
        call PrintHelpMessage()
        stop
      else if (ArgCount.ge.3) then
C       Check for Command Line Options
        i=1
        call get_command_argument(i, CurrArg)
        if (ShowDetails) then
          write(*,'('' '',A)') "Processing Argument: "//trim(CurrArg)
        endif
510     if (CurrArg(1:1).eq.'-') then
C         We found an option
          if (CurrArg(2:2).eq.'C') then
            WriteLeadingCs=.False.
            if (ShowDetails) then
              write(*,'('' '',A)') "Surpressing leading Cs from file."
            endif
          else if (CurrArg(2:2).eq.'Q') then
C           Quiet Mode No Output
            ShowDetails=.False.
          else
            write(*,'('' '',A)') "Error - Unknown "//
     1                     "option: "//trim(CurrArg)
            call PrintHelpMessage()
            stop
          endif
          i=i+1
          if (ArgCount<i) then
            write(*,'('' '',A)') "Error - No src file"
            call PrintHelpMessage()
            stop
          else
            call get_command_argument(i, CurrArg)
            if (ShowDetails) then
              write(*,'('' '',A)') "Processing Argument: "//
     1                              trim(CurrArg)
            endif
            goto 510
          endif
        else
C         Start of src file description
          if (ShowDetails) then
            write(*,'('' '',A)') "Found src file: "//trim(CurrArg)
          endif
          offset=index(Radka,trim(CurrArg))
          Radka=Radka(offset:)
        endif
      endif
C      while 
      if(idel(Radka).ne.0) then
        i=0
        call kus(Radka,i,Radkap)
        call ExtractFileName(Radkap,Knihovna)
        call kus(Radka,i,ModulHledany)
        if(i.lt.80) then
          call kus(Radka,i,Filter)
        else
          Filter=' '
        endif
        call mala(Filter)
        idfl=idel(Filter)
      else
        write(6,'(''Error - no parameter specified'')')
        call PrintHelpMessage()
        stop
      endif
      call mala(Knihovna)
      if(index(Knihovna,'.').le.0) Knihovna=Knihovna(1:idel(Knihovna))//
     1                                      '.src'
      open(40,file=Knihovna)
      open(42,file='ktere.lbc')
      open(43,file='preklad.bat')
      JmenoKnihovny=Knihovna(:index(Knihovna,'.')-1)
      idjk=idel(JmenoKnihovny)
      radka='..\'//JmenoKnihovny(:idjk)//'/pa:64'
      write(42,101)(radka(i:i),i=1,idel(radka))
      call mala(ModulHledany)
      if(idel(ModulHledany).le.0) then
        write(6,'(''Error - no parameter specified'')')
        call PrintHelpMessage()
        stop
      endif
      Linka=0
1000  read(40,100,end=9900) Radka
      Linka=Linka+1
      Radkap=Radka
      Radka=' '
      j=0
      do 1010i=1,idel(Radkap)
        j=j+1
        if(ichar(Radkap(i:i)).eq.9) then
          j=8*(j/8+1)
        else
          Radka(j:j)=Radkap(i:i)
        endif
1010  continue
      Radkap=Radka
      call mala(Radka)
      ip=index(Radka,'*')
      if(ip.gt.0) then
        Radka(ip:ip)=' '
        do 1050i=ip+1,idel(Radka)
          k=ichar(radka(i:i))
          if(k.ge.48.and.k.le.57) then
            radka(i:i)=' '
          else
            go to 1100
          endif
1050    continue
      endif
1100  call zhusti(Radka)
      do 2000i=1,9
        idsi=ids(i)
        if(index(Radka,String(i)(:idsi)).eq.1) then
          ip=idsi+1
          go to 3000
        endif
2000  continue
      go to 1000
3000  ik=index(Radka,'(')
      if(ik.le.0) then
        ik=idel(Radka)
      else
        ik=ik-1
      endif
      Modul=Radka(ip:ik)
      if(ModulHledany.ne.'*'.and.Modul.ne.ModulHledany) go to 1000
      id=idel(Modul)
c      if(id.gt.9) then
c	id=9
c	Modul=Modul(:9)
c      endif
      open(41,file=Modul(:id)//'.for')
      if (WriteLeadingCs) then
        do 3100i=1,Linka-1
          write(41,'(''C'')')
3100    continue
      endif
      write(41,101)(Radkap(i:i),i=1,idel(Radkap))
      if(Filter.eq.' ') then
        Nalezeno=.true.
        if (ShowDetails) then
          write(6,'('' Module found : '',80a1)')(Modul(i:i),i=1,id)
        endif
      else
        Nalezeno=.false.
      endif
4100  read(40,100) Radka
      Linka=Linka+1
      Radkap=Radka
      Radka=' '
      j=0
      do 4110i=1,idel(Radkap)
        j=j+1
        if(ichar(Radkap(i:i)).eq.9) then
          j=8*(j/8+1)
        else
          Radka(j:j)=Radkap(i:i)
        endif
4110  continue
      Radkap=Radka
      call mala(Radka)
      if(Filter.ne.' '.and..not.Nalezeno) then
        Nalezeno=Nalezeno.or.index(Radka,filter(:idfl)).gt.0
        if (ShowDetails) then
          if(Nalezeno) write(6,'('' Module found : '',80a1)')
     1                                   (Modul(i:i),i=1,id)
        endif
      endif
      call zhusti(Radka)
      write(41,101)(Radkap(i:i),i=1,idel(Radkap))
      if(radka.ne.'end') go to 4100
      if(Nalezeno) then
        Radka=Modul(:id)//'.for'
        write(43,101)(Radka(i:i),i=1,idel(Radka))
        Radkap='-+'//Modul(:id)//'&'
        write(42,101)(Radkap(i:i),i=1,idel(Radkap))
      endif
      if(ModulHledany.eq.'*') then
        backspace 40
        Linka=Linka-1
        if(Nalezeno) then
          close(41)
        else
          close(41,status='delete')
        endif
        go to 1000
      endif
9900  radka=','//JmenoKnihovny(:idjk)//'.lsl,'//JmenoKnihovny(:idjk)//
     1      ';'
      write(42,101)(Radka(i:i),i=1,idel(Radka))
      write(43,'(''-c'')')
      close(43)
      if (ShowDetails) then
        write(6,'('' End of split program'')')
      endif
      stop
100   format(80a)
101   format(80a1)
      end
      subroutine mala(veta)
      character*(*) veta
      character*1 malchar(26),velchar(26),v
      logical first,souvisle
      save na,nz,ndif
      data first,souvisle/2*.true./
      data malchar/'a','b','c','d','e','f','g','h','i','j','k','l','m',
     1             'n','o','p','q','r','s','t','u','v','w','x','y','z'/
      data velchar/'A','B','C','D','E','F','G','H','I','J','K','L','M',
     1             'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'/
      if(first) then
        do 1000i=1,25
          if(ichar(malchar(i+1))-ichar(malchar(i)).ne.1) go to 1100
          if(ichar(velchar(i+1))-ichar(velchar(i)).ne.1) go to 1100
1000    continue
        na=ichar('A')
        nz=ichar('Z')
        ndif=ichar('a')-na
        go to 1200
1100    souvisle=.false.
1200    first=.false.
      endif
      if(souvisle) then
        do 2000i=1,idel(veta)
        n=ichar(veta(i:i))
        if(n.ge.na.and.n.le.nz) veta(i:i)=char(n+ndif)
2000    continue
      else
        do 4000i=1,idel(veta)
          v=veta(i:i)
          do 3000j=1,26
            if(v.eq.velchar(j)) go to 3500
3000      continue
          go to 4000
3500      veta(i:i)=malchar(j)
4000    continue
      endif
      return
      end
      subroutine kus(radka,k,slovo)
      character*(*) radka,slovo
      mxs=len(slovo)
      mxr=len(radka)
      slovo=' '
      l=0
      if(k.ge.mxr) go to 9999
      k=k+1
1000  if(radka(k:k).eq.' ') then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 1000
      endif
2000  if(radka(k:k).ne.' ') then
        if(l.lt.mxs) then
          l=l+1
          slovo(l:l)=radka(k:k)
        endif
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 2000
      endif
3000  if(radka(k:k).eq.' ') then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 3000
      endif
      k=k-1
9999  return
      end
      function idel(l)
      character*(*) l
      idel=len(l)
1000  if(idel.ne.0) then
        if(l(idel:idel).eq.' ') then
          idel=idel-1
          go to 1000
        endif
      endif
      return
      end
      subroutine zhusti(s)
      character*(*) s
      idl=idel(s)
      k=0
      do 1000i=1,idl
        if(s(i:i).eq.' ') go to 1000
        k=k+1
        if(k.ne.i) then
          s(k:k)=s(i:i)
          s(i:i)=' '
        endif
1000  continue
      return
      end
      subroutine ExtractFileName(FullName,FileName)
      character*(*) FullName,FileName
      j=0
1000  i=index(FullName(j+1:),'\')
      if(i.gt.0) then
        j=j+i
        go to 1000
      endif
      FileName=FullName(j+1:)
      return
      end
      subroutine PrintHelpMessage()
      write(*,*) " "
      write(*,'(A)') "386split - Written By Vaclav Petricek"
      write(*,*) " "
      write(*,'(A)') "Usage: 386split [options] sourcefile (\* or "//
     1           " func1) [func2] [funcN]"
      write(*,*) " "
      write(*,'(A)') "Description:"
      write(*,*) " "
      write(*,'(A)') "386split breaks a fortran file down into "//
     1           "individual functions.  The functions are "//
     1           "written to functionname.for files.  By default "//
     1           "they will be prepended with comment lines so "//
     1           "so that line numbering will remain consistent "//
     1           "with the original source file for debugging "//
     1           "purposes."
      write(*,*) " "
      write(*,'(A)') "Options:"
      write(*,*) " "
      write(*,'(A)') " -Q - Quiet surpress output"//
     1               " option to surpress all output)"
      write(*,'(A)') " -C - Remove leading comments"
      write(*,*) " "
      write(*,'(A)') "Required Arguments"
      write(*,*) " "
      write(*,'(A)') "sourcefile - libraryname.src (contains fortran "//
     1           "source that will be split)"
      write(*,*) " "
      write(*,'(A)') "Requires \* or func1 at a minimum"
      write(*,*) " "
      write(*,'(A)') "\* - Extract all functions, modules, commons, "//
     1               "etc."
      write(*,'(A)') "func1 - Extract only this function"
      write(*,'(A)') "funcN - Extract only the listed function names"//
     1           " (1,2,..,N)"
      write(*,*) " "
      write(*,*) " "
      return
      end

