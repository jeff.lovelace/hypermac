program hypermac
!
! Main Program for the TUI for the JANA2006 RefMac integration
! 
! Author: JJL
!
use hm_nl
use hm_commandline
use hm_sl
use hm_stream
use hm_parser
use hm_io
implicit none
call update_start_time()
call hm_nl_initialize()
call hm_s_initialize()
call determine_env_paths()
call hm_cl_process()
call hm_s_pathsummary()
call hm_s_divider()
call hm_s_write_bl()
call hm_p_execute()
call hm_s_write_footer()
end program hypermac
