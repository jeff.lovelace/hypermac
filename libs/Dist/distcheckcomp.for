      subroutine DistCheckComp(isw,y1,du,dmez,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension y1(*),yp(6),ypp(6),tmina(3),Right(3)
      ich=0
      call qbyx(y1,y1(4),isw)
      call multm(WPr,y1,yp,NDim(KPhase),NDim(KPhase),1)
      do l=1,3
        u(l)=x1(l)-yp(l)
      enddo
      call multm(GZd,u,Right,NDimI(KPhase),3,1)
      call multm(ZdGZdI,Right,tmina,NDimI(KPhase),NDimI(KPhase),1)
      call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
      u(1)=-scalmul(u,ug)
      pom=-u(1)
      call multm(Right,ZdGZdI,ug,1,NDimI(KPhase),NDimI(KPhase))
      call cultm(ug,Right,u,1,NDimI(KPhase),1)
      uu=max(-u(1),0.)
      du=sqrt(uu)
      if(uu.gt.dmez) go to 5000
      if(NDimI(KPhase).eq.1) then
        TMin=tmina(1)*TNaT(1)
        if(TMin.lt.TFirstI(1).or.TMin.gt.TLastI(1)) then
          call CopyVek(y1,yp,NDim(KPhase))
          yp(4)=yp(4)+TFirstI(1)*tnati(1)
          k=0
1500      call multm(WPr,yp,ypp,NDim(KPhase),NDim(KPhase),1)
          do l=1,3
            u(l)=ypp(l)-x1(l)
          enddo
          call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
          pom=scalmul(u,ug)
          if(pom.lt.dmez) go to 9999
          if(k.gt.0) go to 5000
          k=1
          yp(4)=yp(4)+(TLastI(1)-TFirstI(1))*tnati(1)
          go to 1500
        endif
      endif
      go to 9999
5000  ich=1
9999  return
      end
