      subroutine pisat
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(3),xs(3)
      logical eqwild,lpomPrvni,lpomDruhyATreti,Zprava
      character*80 FormOut,Veta
      character*8 at
      character*1 znak1,znak2
      data
     1  FormOut/'(i3,2x,a8,a1,1x,a1,6f10.6,3f9.4,f9.6,''('',i5,'')'')'/
      lpomPrvni=iselPrvni.le.0
      lpomDruhyATreti=iselDruhyATreti.le.0
      do j=1,NAtCalc
        BratPrvni(j)=lpomPrvni
        BratDruhyATreti(j)=lpomDruhyATreti
      enddo
      call inclat
      Zprava=.false.
      if(iselPrvni.gt.0) then
        fullcoo=1
        do i=1,iselPrvni
          if(index(aselPrvni(i),'*').ne.0) then
            do j=1,NAtCalc
              lpomPrvni=eqwild(atom(j),aselPrvni(i),.false.)
              BratPrvni(j)=(BratPrvni(j).or.lpomPrvni).and.
     1                      kswa(j).eq.KPhase
            enddo
          else
            j=ktat(atom,NAtCalc,aselPrvni(i))
            if(j.le.0) then
              at=aselPrvni(i)
              k=min(idel(at),7)
              at=at(:k)//''''
              j=ktat(atom,NAtCalc,at)
            endif
            if(j.gt.0) then
              lpomPrvni=j.gt.0
              BratPrvni(j)=(BratPrvni(j).or.lpomPrvni).and.
     1                      kswa(j).eq.KPhase
            else if(.not.Zprava) then
              Veta='Atom "'//aselPrvni(i)(:idel(aselPrvni(i)))//
     1             '" selected for calculation not present in the '//
     2             'structure'
              call FeChybne(-1.,-1.,Veta,'It may exist more spurious '//
     1          'atoms but only first one is announced.',Warning)
              Zprava=.true.
            endif
          endif
        enddo
      endif
      if(iselDruhyATreti.gt.0) then
        do i=1,iselDruhyATreti
          if(index(aselDruhyATreti(i),'*').ne.0) then
            do j=1,NAtCalc
              lpomDruhyATreti=eqwild(atom(j),aselDruhyATreti(i),.false.)
              BratDruhyATreti(j)=
     1          (BratDruhyATreti(j).or.lpomDruhyATreti).and.
     2           kswa(j).eq.KPhase
            enddo
          else
            j=ktat(atom,NAtCalc,aselDruhyATreti(i))
            if(j.le.0) then
              at=aselDruhyATreti(i)
              k=min(idel(at),7)
              at=at(:k)//''''
              j=ktat(atom,NAtCalc,at)
            endif
            if(j.gt.0) then
              lpomDruhyATreti=j.gt.0
              BratDruhyATreti(j)=
     1          (BratDruhyATreti(j).or.lpomDruhyATreti).and.
     2           kswa(j).eq.KPhase
            else if(.not.Zprava) then
              Veta='Atom "'//
     1             aselDruhyATreti(i)(:idel(aselDruhyATreti(i)))//
     2             '" selected for calculation not present in the '//
     3             'structure'
              call FeChybne(-1.,-1.,Veta,'It may exist more spurious '//
     1          'atoms but only first one is announced.',Warning)
              Zprava=.true.
            endif
          endif
        enddo
      endif
      if(ngc(KPhase).gt.0) then
        n=NAtCalcBasic
        do j=1,ngc(KPhase)
          do i=1,NAtCalcBasic
            if(kswa(i).ne.KPhase) cycle
            n=n+1
            BratPrvni(n)=BratPrvni(i)
            BratDruhyATreti(n)=BratDruhyATreti(i)
          enddo
        enddo
      endif
      call deleq
      KPhase=KPhaseBasic
      call newln(3)
      write(lst,'(/38x,''fractional'',36x,''cartesian'')')
      if(lite(KPhase).eq.1) then
        write(lst,'('' no'',2x,''atom'',13x,''xf'',8x,''yf'',8x,''zf'',
     1              5x,''sigxf'',5x,''sigyf'',5x,''sigzf'',8x,''x'',8x,
     2              ''y'',8x,''z'',6x,''Biso'')')
        FormOut(34:36)='6.2'
        fact=100.
      else
        write(lst,'('' no'',2x,''atom'',13x,''xf'',8x,''yf'',8x,''zf'',
     1              5x,''sigxf'',5x,''sigyf'',5x,''sigzf'',8x,''x'',8x,
     2              ''y'',8x,''z'',6x,''Uiso'')')
        FormOut(34:36)='9.6'
        fact=1000000.
      endif
      do i=1,NAtCalc
        m=isf(i)
        if(m.eq.0.or.kswa(i).ne.KPhase) cycle
        do j=1,3
          call round(xdst(j,i),sx(j,i),irnd)
        enddo
        if(itf(i).eq.1) then
          if(lite(KPhase).eq.0) then
            bizo=beta(1,i)/episq
            pom=sbeta(1,i)/episq
          else
            bizo=beta(1,i)
            pom=sbeta(1,i)
          endif
        else
          call boueq(betadst(1,i),sbeta(1,i),lite(KPhase),bizo,pom,1)
        endif
        call MultM(TrToOrtho(1,1,KPhase),xdst(1,i),xp,3,3,1)
        if(BratPrvni(i)) then
          znak1='+'
        else
          znak1='-'
        endif
        if(BratDruhyATreti(i)) then
          znak2='+'
        else
          znak2='-'
        endif
        call newln(1)
        do j=1,3
          xs(j)=max(sx(j,i),0.)
        enddo
        write(lst,FormOut) i,atom(i),znak1,znak2,(xdst(j,i),j=1,3),
     1                     xs,xp,bizo,nint(pom*fact)
      enddo
      return
      end
