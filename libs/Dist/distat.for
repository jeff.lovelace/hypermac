      subroutine DistAt
      use Atoms_mod
      use Basic_mod
      use EditM40_mod
      use Dist_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      real :: BI(6),BJ(6),sBI(6),sBJ(6),TrMat(36),rmpt(36),tlp(6),
     1        tpisq=19.7392088
      dimension y0(3),y1(3),y2(3),uz(3),shj(3),umg(3),sx0(6),shic(6),
     1          shjc(3),ish(3),jsh(3),
     2          uxj(3,mxw),uyj(3,mxw),y0p(6),y1p(3),y2p(3),uzp(3),up(3),
     3          MxCell(3),MxDCell(3),y340(3),yp(6),ypp(6),
     4          idtab(30),iptab(30),nutab(30),utab(30,30),tztnat(3),
     5          dttnat(3),GammaIntInv(9),tsp(3),xp(6),t(3),tj(3),nd(3),
     6          suxj(3,mxw),suyj(3,mxw),sgxm(6),dstab(30),sutab(30,30),
     7          dmod(6),y3(6),y3p(6),smp(36),rmp(9),rm6p(36),s6p(6),
     8          vt6p(:,:),ddg(3),JCell(3)
      character*128 ltab(500),Veta
      character*80 t80,s80
      character*15 t15
      character*10 t101,t102,t103
      character*8 at1,at2,at3,attab(30),atutab(30,30),sp8
      character*2 nty,sp2,Label,Lab2,Lab3
      allocatable vt6p
      logical iuhl0,Prvne,Konec,Nacitame,JeToUhel,eqrv,EqRvLT0
      equivalence (dmod(1),dumav),(dmod(2),sdumav),
     1            (dmod(3),dummn),(dmod(4),sdummn),
     2            (dmod(5),dummx),(dmod(6),sdummx)
      data sp2,sp8/2*' '/
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)),
     1         TypicalDistUse(NAtFormula(KPhase),NAtFormula(KPhase)))
      if(VolaToDist.and..not.CalcHBonds) then
        lnh=NextLogicNumber()
        call OpenFile(lnh,fln(:ifln)//'.l66','formatted','unknown')
      else
        lnh=0
      endif
      iswlast=0
      if(CalcHBonds) then
        if(VolaToDist) then
          if(isfh(KPhase).le.0) go to 9999
          call newpg(0)
          call TitulekVRamecku('List of possible hydrogen bonds')
          call newln(3)
          write(lst,'(/''Maximal donor-hydrogen bond :'',f6.3,5x,
     1                 ''Maximal hydrogen-acceptor bond:'',f6.3/)')
     2         DHMax,HAMax
          iuhl0=.false.
          LstType=99
        endif
      else
        call SetRealArrayTo(dmd,NAtFormula(KPhase),dmdk)
        if(dmhk.ge.0.) then
          call SetRealArrayTo(dmh,NAtFormula(KPhase),dmhk*.5)
        else
          call CopyVek(AtRadius(1,KPhase),dmh,NAtFormula(KPhase))
        endif
        pom=0.
        do i=1,NAtFormula(KPhase)
          if(2.*dmh(i).lt.dmd(i)) dmh(i)=0.
          pom=pom+dmh(i)
        enddo
        iuhl0=iuhl.eq.0
        if(dmhk.ge.0.) then
          call SetRealArrayTo(TypicalDistUse,NAtFormula(KPhase)**2,
     1                        dmhk)
        else
          do i=1,NAtFormula(KPhase)
            do j=i,NAtFormula(KPhase)
              if(TypicalDist(i,j,KPhase).gt.0.) then
                TypicalDistUse(i,j)=TypicalDist(i,j,KPhase)
              else
                TypicalDistUse(i,j)=AtRadius(i,KPhase)+
     1                              AtRadius(j,KPhase)
              endif
              TypicalDistUse(j,i)=TypicalDistUse(i,j)
            enddo
          enddo
        endif
        if(VolaToDist) then
          t80='List of distances'
          if(.not.iuhl0) t80=t80(:idel(t80))//' and angles'
          call NewPg(0)
          call TitulekVRamecku(t80)
          if(pom.le.0.) then
            call newln(2)
            write(lst,'(/''Distances and angles will not be '',
     1                   ''calculated'')')
            go to 9999
          endif
          if(LstType.eq.0) call SetStringArrayTo(iven,mxl4,' ')
          call NewLn(3)
          if(fullcoo.eq.1) then
            write(lst,'(/''Full coordination of atoms will be printed'')
     1                  ')
          else
            write(lst,'(/''Reduced coordination of atoms will be '',
     1                   ''printed'')')
          endif
          if(LstType.eq.0) then
            write(lst,'(''The results will be printed in columns '',
     1                  ''without symmetry codes'')')
          else if(LstType.eq.1) then
            write(lst,'(''The results will be printed in one column '',
     1                  ''with symmetry codes'')')
          endif
          if(iuhl0) then
            Veta='Distances will be calculated'
          else
            Veta='Distances and angles will be calculated'
          endif
          Veta=Veta(:idel(Veta))//' from d(min)='
          write(Veta(idel(Veta)+1:),105) dmdk
          if(dmhk.ge.0.) then
            Veta=Veta(:idel(Veta))//' to d(max)='
            write(Veta(idel(Veta)+1:),105) dmhk
          else
            Veta=Veta(:idel(Veta))//' to specific maximal distances '//
     1           'as listed below'
            if(ExpDMax.gt.0.) then
              write(Cislo,'(f5.2)') ExpDMax
              call ZdrcniCisla(Cislo,1)
              Veta=Veta(:idel(Veta))//' expanded by '//
     1             Cislo(:idel(Cislo))//'%'
            endif
            Veta=Veta(:idel(Veta))//':'
          endif
          call newln(3)
          write(lst,FormA)
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          if(dmhk.lt.0.) then
            do i=1,NAtFormula(KPhase)
              idl=1
              do j=i,NAtFormula(KPhase)
                write(t80,'(a2,''-'',a2)') AtType(i,KPhase),
     1                                     AtType(j,KPhase)
                call Zhusti(t80)
                if(TypicalDist(i,j,KPhase).gt.0.) then
                  Label=' T'
                else
                  Label=' R'
                endif
                write(Cislo,'(f6.3,a2)') TypicalDistUse(i,j),Label
                Veta(idl:)=t80(:5)//Cislo
                idl=idl+18
                if(idl.gt.120) then
                  call NewLn(1)
                  write(lst,FormA) Veta(:idel(Veta))
                  idl=1
                endif
              enddo
              if(idl.gt.1) then
                call NewLn(1)
                write(lst,FormA) Veta(:idel(Veta))
              endif
            enddo
            call NewLn(4)
            write(lst,'(/''T ... typical distances taken either from '',
     1                   ''the file distributed with Jana program or '',
     2                   ''defined by user'')')
            write(lst,'( ''R ... distance as derived from atomic '',
     1                   ''radii:''/)')
            call newln(NAtFormula(KPhase)+2)
            write(lst,'(45x,''Atom type    Atom radius'')')
            do i=1,NAtFormula(KPhase)
              write(lst,'(47x,a2,10x,f8.3)')
     1          AtType(i,KPhase),AtRadius(i,KPhase)
            enddo
            write(lst,FormA)
          endif
        else

        endif
      endif
      if(NDimI(KPhase).gt.0.and.nt(1,1).gt.0.and.VolaToDist) then
        call NewLn(NDimI(KPhase)+2)
        Veta='The calculation will run'
        idl=idel(Veta)+2
        do i=1,NDimI(KPhase)
          write(t80,'(''<'',f9.3,'','',f9.3,''>'')')
     1      tfirst(i,1),tlast(i,1)
          call zhusti(t80)
          write(Veta(idl:),'(''for '',a1,'' from the interval '',a,
     1                '' at '',i3,'' equidistant steps'')')
     2      smbt(i),t80(:idel(t80)),nt(i,1)
          write(lst,FormA) Veta(:idel(Veta))
          idl=1
        enddo
        if((LstType.eq.0.or.LstType.eq.99).and.ieach.ne.0) then
          if(ieach.eq.1) then
            write(lst,'(''Each step will be printed''/)')
          else
            write(lst,'(''Each'',i3,a2,'' step will be printed''/)')
     1        ieach,nty(ieach)
          endif
        else
          write(lst,'(''Print of steps supressed''/)')
        endif
      endif
      if(VolaToDist) then
        if(line.gt.40) then
          call newpg(0)
          if(LstType.eq.0) mxl=mxl4
        else
          if(LstType.eq.0) mxl=(mxline-line)*4
        endif
        if(LstType.eq.0) line=0
      endif
      if(.not.CalcHBonds) then
        call OpenFile(78,fln(:ifln)//'.l61','unformatted','unknown')
        if(ErrFlag.ne.0) go to 9999
      endif
      NaCelkem=0
      if(VolaToDist) then
        do i=1,NAtCalc
          if(.not.BratPrvni(i).or.isf(i).le.0.or.kswa(i).ne.KPhase.or.
     1       (CalcHBonds.and.isf(i).ne.isfh(KPhase))) cycle
          if(fullcoo.eq.1) then
            jp=1
          else
            jp=i
          endif
          do j=jp,NAtCalc
            if(.not.BratDruhyATreti(j).or.isf(j).le.0.or.
     1         kswa(j).ne.KPhase.or.
     2       (CalcHBonds.and.isf(j).eq.isfh(KPhase))) cycle
            NaCelkem=NaCelkem+1
          enddo
        enddo
        if(NaCelkem.gt.1) then
          if(CalcHBonds) then
            t80='Search for hydrogen bonds'
          else
            t80='Distance calculation'
          endif
          call FeFlowChartOpen(-1.,-1.,1,NaCelkem,t80,' ',' ')
        else
          if(NaCelkem.le.0) go to 9999
        endif
      endif
      nfloat=0
      if(CalcHBonds) then
        IHBonds=0
      else
        n61=0
      endif
      nmo=0
      iswo=0
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or..not.BratPrvni(i).or.isf(i).le.0.or.
     1     (CalcHBonds.and.isf(i).ne.isfh(KPhase))) cycle
        if(CalcHBonds) then
          dmdi=0.
        else
          dmdi=dmd(isf(i))
c          dmhi=dmh(isf(i))
        endif
        iswi=iswa(i)
        if(VolaToDist) then
          t80='x,y,z#'
          SymmCIF(1)='.'
          SymmLabel(1)=' '
          if(itf(i).eq.2) then
            call CopyVek( Beta(1,i), BI,6)
            call CopyVek(sBeta(1,i),sBI,6)
            do j=1,6
               BI(j)= BI(j)/tpisq
              sBI(j)=sBI(j)/tpisq
            enddo
          endif
        else
          if(PrvniSymmString.ne.' ') then
            NTrans=1
            if(allocated(TransMat)) call DeallocateTrans
            if(NAtCalc.ge.MxAtAll) call ReallocateAtoms(5)
            call AllocateTrans
            call AtCopy(i,NAtCalc+1)
            call CopyVek(xdst(1,i),xdst(1,NAtCalc+1),3)
            call AtomSymCode(PrvniSymmString,ia,isym,icenter,shic,ish,
     1                       ich)
            iswi=ISwSymm(isym,iswa(i),KPhase)
            if(iswi.eq.iswa(i)) then
              call CopyMat(rm (1,isym,iswi,KPhase),rmp ,3)
              call CopyMat(rm6(1,isym,iswi,KPhase),rm6p,NDim(KPhase))
              call CopyVek(shic,s6p,NDim(KPhase))
            else
              call multm(zv(1,iswi,KPhase),zvi(1,iswa(i),KPhase),smp,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call multm(smp,rm6(1,isym,iswa(i),KPhase),rm6p,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call MatBlock3(rm6p,rmp,NDim(KPhase))
              call multm(smp,shic,s6p,NDim(KPhase),NDim(KPhase),1)
            endif
            call CopyMat(rm6p,TransMat(1,1,iswi),NDim(KPhase))
            call CopyVek(shic,TransVec(1,1,iswi),NDim(KPhase))
            call Multm(rmp,xdst(1,i),x1p,3,3,1)
            call AddVek(x1p,shic,xdst(1,i),3)
            call SCode(isym,icenter,shic,ish,iswi,t80,SymmLabel(1))
            t80=t80(:idel(t80))//'#'
            l=idel(SymmLabel(1))
            if(l.gt.0) t80=t80(:idel(t80))//SymmLabel(1)(:l)
            call zhusti(t80)
            call EM40SetTr(0)
            call EM40TransAtFromTo(i,i,ich)
            call qbyx(x(1,i),qcnt(1,i),iswi)
            call ScodeCIF(isym,icenter,ish,iswi,SymmCIF(1))
          else
            t80='x,y,z#'
            SymmCIF(1)='.'
          endif
        endif
        call SetRealArrayTo(xp,3,0.)
        k=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,i).ne.0) then
            xp(1)=ax(KModA(1,i),i)
            k=1
          else if(KFA(2,i).ne.0) then
            xp(1)=uy(1,KModA(2,i),i)
            k=1
          endif
        endif
        call SpecPos(x(1,i),xp,k,iswi,.01,n)
        aii=ai(i)*float(n)
        ah=rcp(1,iswi,KPhase)
        bh=rcp(2,iswi,KPhase)
        ch=rcp(3,iswi,KPhase)
        call CopyVek(tfirst(1,iswi),tfirsti,NDimI(KPhase))
        call CopyVek (dt(1,iswi),dti,NDimI(KPhase))
        call CopyVekI(nt(1,iswi),nti,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          TLastI(j)=TFirstI(j)+dti(j)*float(nti(j)-1)
        enddo
        call bunka(xdst(1,i),x1,0)
        call CopyVek(x(1,i),x1p,3)
        call qbyx(x1,x1(4),iswi)
        if(.not.CalcHBonds) then
          if(VolaToDist) then
            call DistModifyAtName(i,At1)
          else
            At1=Atom(i)
          endif
          write(78) '#1',At1,isf(i),iswi,aii,(0.,m=1,6),t80,
     1               SymmCIF(1),1
          n61=n61+1
          if(VolaToDist) then
            if(LstType.eq.0) then
              call fflush
              line=line+1
              write(iven(line),102)
              call fflush
              line=line+1
              write(iven(line),103) atom(i)
              call fflush
              line=line+1
              write(iven(line),102)
              call fflush
            else if(LstType.eq.1) then
              call newln(2)
              if(iuhl0) then
                write(lst,'(''Distances concerning atom '',a8/
     1                      34(''=''))') atom(i)
              else
                write(lst,'(''Distances and angles concerning atom '',
     1                      a8/45(''=''))') atom(i)
              endif
            endif
          endif
        endif
        call CopyVek(sx(1,i),sgx(1),3)
        if(ntall(iswi).gt.0.and.NDimI(KPhase).gt.0) then
          call UnitMat(GammaIntInv,NDimI(KPhase))
          if(NonModulated(KPhase)) then
            call SetRealArrayTo(oi,ntall(iswi),1.)
            call SetRealArrayTo(xdi,3*ntall(iswi),0.)
            call SetRealArrayTo(sxdi,3*ntall(iswi),0.)
          else
            if(TypeModFun(i).le.1.or.KModA(1,i).le.1) then
              call MakeOccMod(oi,qcnt(1,i),tfirsti,nti,dti,
     1                        a0(i),ax(1,i),ay(1,i),KModA(1,i),
     2                        KFA(1,i),GammaIntInv)
              if(KFA(2,i).gt.0.and.KModA(2,i).gt.0)
     1          call MakeOccModSawTooth(oi,qcnt(1,i),tfirsti,nti,dti,
     2            uy(1,KModA(2,i),i),uy(2,KModA(2,i),i),KFA(2,i),
     3            GammaIntInv)
            else
              call MakeOccModPol(oi,qcnt(1,i),tfirsti,nti,dti,
     1          ax(1,i),ay(1,i),KModA(1,i)-1,
     2          ax(KModA(1,i),i),a0(i)*.5,GammaIntInv,TypeModFun(i))
            endif
            if(TypeModFun(i).le.1) then
              call MakePosMod(xdi,qcnt(1,i),tfirsti,nti,dti,ux(1,1,i),
     1          uy(1,1,i),KModA(2,i),KFA(2,i),GammaIntInv)
            else
              call MakePosModPol(xdi,qcnt(1,i),tfirsti,nti,dti,
     1          ux(1,1,i),uy(1,1,i),KModA(2,i),ax(1,i),a0(i)*.5,
     2          GammaIntInv,TypeModFun(i))
            endif
            call MakePosModS(sxdi,qcnt(1,i),tfirsti,nti,dti,ux(1,1,i),
     1                       uy(1,1,i),sux(1,1,i),suy(1,1,i),KModA(2,i),
     2                       KFA(2,i),GammaIntInv)
          endif
        endif
        if(fullcoo.eq.1.or.CalcHBonds) then
          jp=1
        else
          jp=i
        endif
        if(.not.VolaToDist) then
          if(PrvniSymmString.ne.' ') then
            call AtCopy(NAtCalc+1,i)
            call CopyVek(xdst(1,NAtCalc+1),xdst(1,i),3)
            call qbyx(x(1,i),qcnt(1,i),iswi)
          endif
        endif
        do j=jp,NAtCalc
          if(.not.BratDruhyATreti(j).or.kswa(j).ne.KPhase.or.
     1       isf(j).eq.0.or.(CalcHBonds.and.isf(j).eq.isfh(KPhase)))
     2      cycle
          if(SkipSplit.gt.0) then
            if(kmol(i).ne.kmol(j)) then
              do ii=1,NMolSplit
                k=0
                do jj=1,MMolSplit(ii)
                  if(IMolSplit(jj,ii).eq.kmol(i)) then
                    k=k+1
                  else if(IMolSplit(jj,ii).eq.kmol(j)) then
                    k=k+1
                  endif
                enddo
                if(k.ge.2) go to 4000
              enddo
            endif
            do ii=1,NAtSplit
              k=0
              do jj=1,MAtSplit(ii)
                if(IAtSplit(jj,ii).eq.i) then
                  k=k+1
                else if(IAtSplit(jj,ii).eq.j) then
                  k=k+1
                endif
              enddo
              if(k.ge.2) go to 4000
            enddo
          endif
          call SetRealArrayTo(xp,3,0.)
          k=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,j).ne.0) then
              xp(1)=ax(KModA(1,j),j)
              k=1
            else if(KFA(2,j).ne.0) then
              xp(1)=uy(1,KModA(2,j),j)
              k=1
            endif
          endif
          call SpecPos(x(1,j),xp,k,iswa(j),.01,n)
          aij=ai(j)*float(n)
          if(VolaToDist.and.NaCelkem.gt.1) then
            call FeFlowChartEvent(nfloat,ie)
            if(ie.ne.0) then
              call DistBudeBreak
              if(ErrFlag.ne.0) go to 9900
            endif
          endif
          if(CalcHBonds) then
            dmhij =DHMax+dxm(i)+dxm(j)
            dmhijm=DHMax
          else
            if(dmhk.ge.0) then
              pom=1.
            else
              pom=1.+.01*ExpDMax
            endif
            dmhijm=TypicalDistUse(isf(i),isf(j))*pom
            dmhij =dmhijm+dxm(i)+dxm(j)
          endif
          dmhija=dmhij*ah
          dmhijb=dmhij*bh
          dmhijc=dmhij*ch
          dmhijq=dmhij**2
          dmdijq=dmdi**2
          do l=1,3
            MxCell(l)=dmhij*rcp(l,iswa(j),KPhase)+1
          enddo
          MxDCell(3)=1
          do l=2,1,-1
            MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
          enddo
          MxNCell=MxDCell(1)*(2*MxCell(1)+1)
          do l=1,3
            y0(l)=xdst(l,j)
            y0p(l)=x(l,j)
            sx0(l)=sx(l,j)
          enddo
          js=0
          jst=0
          do jsym=1,NSymmN(KPhase)
            js=js+1
            if(isa(js,j).le.0) cycle
            iswj=ISwSymm(jsym,iswa(j),KPhase)
            iswjin=iswi*10+iswj
            if(iswj.ne.iswi) then
              if(iswjin.ne.iswlast) call DistSetComp(iswi,iswj)
            else
              call UnitMat(TNaT ,NDimI(KPhase))
              call UnitMat(TNaTi,NDimI(KPhase))
            endif
            iswlast=iswjin
            call multm(TNaTi,tfirsti,TzTNaT,NDimI(KPhase),NDimI(KPhase),
     1                 1)
            call multm(TNaTi,dti,DtTNaT,NDimI(KPhase),NDimI(KPhase),1)
            if(iswj.eq.iswa(j)) then
              call CopyMat(rm (1,jsym,iswj,KPhase),rmp ,3)
              call CopyMat(rm6(1,jsym,iswj,KPhase),rm6p,NDim(KPhase))
              call CopyVek( s6(1,jsym,iswj,KPhase), s6p,NDim(KPhase))
              do l=1,NLattVec(KPhase)
                call CopyVek(vt6(1,l,iswj,KPhase),vt6p(1,l),
     1                       NDim(KPhase))
              enddo
            else
              call multm(zv(1,iswj,KPhase),zvi(1,iswa(j),KPhase),smp,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call multm(smp,rm6(1,jsym,iswa(j),KPhase),rm6p,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call MatBlock3(rm6p,rmp,NDim(KPhase))
              call multm(smp,s6(1,jsym,iswa(j),KPhase),s6p,NDim(KPhase),
     1                   NDim(KPhase),1)
              do l=1,NLattVec(KPhase)
                call multm(smp,vt6(1,l,iswa(j),KPhase),vt6p(1,l),
     1                     NDim(KPhase),NDim(KPhase),1)
              enddo
            endif
            call multm(rmp,y0,y1,3,3,1)
            call multm(rmp,y0p,y1p,3,3,1)
            if(itf(j).eq.2) then
              call srotb(rmp,rmp,rmpt)
              call MultM(rmpt,  Beta(1,j), BJ,6,6,1)
              call MultMQ(rmpt,sBeta(1,j),sBJ,6,6,1)
              do l=1,6
                 BJ(l)= BJ(l)/tpisq
                sBJ(l)=sBJ(l)/tpisq
              enddo
            endif
            if(EqRvLT0(sx0,3)) then
              call SetRealArrayTo(sgx(4),3,-.1)
            else
              call multmq(rmp,sx0,sgx(4),3,3,1)
            endif
            call GetGammaIntInv(rm6p,GammaIntInv)
            do l=1,3
              y1(l)=y1(l)+s6p(l)
              y1p(l)=y1p(l)+s6p(l)
            enddo
            do 3100jcenter=1,NLattVec(KPhase)
              do l=1,jcenter-1
                if(eqrv(vt6p(1,jcenter),vt6p(1,l),3,.001)) go to 3100
              enddo
              do l=4,NDim(KPhase)
                tsp(l-3)=-s6p(l)
     1                   -vt6p(l,jcenter)
              enddo
              do l=1,3
                y2(l)=y1(l)+vt6p(l,jcenter)
                y2p(l)=y1p(l)+vt6p(l,jcenter)
              enddo
              if(iswj.eq.iswi) then
                call bunka(y2,shj,1)
                do l=1,3
                  uzp(l)=y2p(l)-x1p(l)+shj(l)
                  uz(l)=y2(l)-x1(l)
                enddo
                if(NComp(KPhase).gt.1) then
                  do l=1,3
                    MxCell(l)=dmhij*rcp(l,iswa(j),KPhase)+1
                  enddo
                  MxDCell(3)=1
                  do l=2,1,-1
                    MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                  enddo
                  MxNCell=MxDCell(1)*(2*MxCell(1)+1)
                endif
              else
                call multm(WPri,x1,yp,NDim(KPhase),NDim(KPhase),1)
                do l=1,3
                  shj(l)=yp(l)-y2(l)
                enddo
                do l=1,3
                  shj(l)=ifix(shj(l))
                  y2(l)=y2(l)+shj(l)
                  y2p(l)=y2p(l)+shj(l)
                enddo
                do l=1,NDim(KPhase)
                  xp(l)=x1(l)
                  if(l.gt.3) xp(l)=xp(l)+tfirsti(l-3)
                enddo
                do l=1,3
                  pom=abs(yp(l))
                  if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
                enddo
                do l=4,NDim(KPhase)
                  xp(l)=xp(l)+dti(l-3)*float(nti(l-3)-1)
                enddo
                call multm(WPri,xp,yp,NDim(KPhase),NDim(KPhase),1)
                do l=1,3
                  pom=abs(yp(l))
                  if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
                enddo
                MxDCell(3)=1
                do l=2,1,-1
                  MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                enddo
                MxNCell=MxDCell(1)*(2*MxCell(1)+1)
              endif
              do JCellI=0,MxNCell-1
                call UnPackHKL(JCellI,JCell,MxCell,MxDCell,3)
                do l=1,3
                  pom=JCell(l)
                  if(iswj.eq.iswi) then
                    u(l)=uz(l)+pom
                    up(l)=uzp(l)+pom
                  endif
                  y3(l)=y2(l)+pom
                  y3p(l)=y2p(l)+pom
                  jsh(l)=nint(shj(l))+pom
                  shjc(l)=shj(l)+pom+s6p(l)+vt6p(l,jcenter)
                enddo
                jst=jst+1
                if(iswj.eq.iswi) then
                  call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
                  uu=scalmul(u,ug)
                  if(uu.gt.dmhijq.or.uu.lt.dmdijq.or.
     1               (uu.eq.0..and.i.eq.j)) cycle
                  du=sqrt(uu)
                  do l=1,3
                    if(du.ne.0.) then
                      ddx(l+3)=ug(l)/du
                      ddg(l)=u(l)**2
                    else
                      ddx(l+3)=0.
                      ddg(l)=0.
                    endif
                    ddx(l)=-ddx(l+3)
                  enddo
                else
                  call DistCheckComp(iswj,y3,du,dmhijq,ich)
                  if(ich.ne.0) cycle
                endif
2520            call qbyx(y3,y3(4),iswj)
                call qbyx(y3p,y3p(4),iswj)
                if(iswi.eq.iswj) then
                  sdu=sigma(sgx,ddx,6)
                  gdu=0.
                  m=0
                  do l=1,3
                    do k=1,3
                      m=m+1
                      gdu=gdu+MetTensS(m,iswi,KPhase)*ddg(l)*ddg(k)
                    enddo
                  enddo
                  if(du.gt.0.) then
                    gdu=sqrt(gdu)/du
                  else
                    gdu=0.
                  endif
                  sdu=sqrt(sdu**2+gdu**2)
                  if(sdu.lt..00001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3)) sdu=0.
                  if(itf(i).eq.2.and.itf(j).eq.2.and.lnh.gt.0.and.
     1              du.gt.0.) then
                    pom=1./du
                    do l=1,3
                      xp(l)=ug(l)*pom
                    enddo
                    pom=abs((BI(1)-BJ(1))*xp(1)**2+
     1                      (BI(2)-BJ(2))*xp(2)**2+
     2                      (BI(3)-BJ(3))*xp(3)**2+
     3                      (BI(4)-BJ(4))*2.*xp(1)*xp(2)+
     4                      (BI(5)-BJ(5))*2.*xp(1)*xp(3)+
     5                      (BI(6)-BJ(6))*2.*xp(2)*xp(3))
                    spom=sqrt((sBI(1)*xp(1)**2)**2+
     1                       (sBJ(1)*xp(1)**2)**2+
     2                       (sBI(2)*xp(2)**2)**2+
     3                       (sBJ(2)*xp(2)**2)**2+
     4                       (sBI(3)*xp(3)**2)**2+
     5                       (sBJ(3)*xp(3)**2)**2+
     6                       (sBI(4)*2.*xp(1)*xp(2))**2+
     7                       (sBJ(4)*2.*xp(1)*xp(2))**2+
     8                       (sBI(5)*2.*xp(1)*xp(3))**2+
     9                       (sBJ(5)*2.*xp(1)*xp(3))**2+
     a                       (sBI(6)*2.*xp(2)*xp(3))**2+
     1                       (sBJ(6)*2.*xp(2)*xp(3))**2)
                    write(lnh,107) atom(i),atom(j),du,sdu,pom,spom
                  endif
                  if(kmol(i).eq.kmol(j).and.kmol(i).gt.0.and.
     1               itf(i).eq.2.and.itf(j).eq.2.and..not.CalcHBonds)
     2              then
                    nm=kmol(i)
                    if(KTLS((nm-1)/mxp+1).gt.0) then
                      if(nm.ne.nmo.or.iswo.ne.iswi) then
                        call srotb(TrToOrtho(1,iswi,KPhase),
     1                             TrToOrtho(1,iswi,KPhase),TrMat)
                        call MultM(TrMat,tl(1,nm),tlp,6,6,1)
                        do l=1,6
                           tlp(l)=tlp(l)/tpisq
                        enddo
                        iswo=iswi
                        nmo=nm
                      endif
                      call MultM(TrToOrtho(1,iswi,KPhase),u,xp,3,3,1)
                      dif=tlp(1)*(xp(2)**2+xp(3)**2)+
     1                    tlp(2)*(xp(1)**2+xp(3)**2)+
     2                    tlp(3)*(xp(1)**2+xp(2)**2)-
     3                    2.*(tlp(4)*xp(1)*xp(2)+
     4                        tlp(5)*xp(1)*xp(3)+
     5                        tlp(6)*xp(2)*xp(3))
                      if(du.gt.0.) then
                        dif=.5*dif/du
                        dutls=du+dif
                      else
                        dif=0.
                        dutls=0.
                      endif
                    else
                      dutls=-1.
                    endif
                  else
                    dutls=-1.
                  endif
                else
                  sdu=0.
                endif
                call scode(jsym,jcenter,shjc,jsh,iswj,t80,SymmLabel(2))
                call ScodeCIF(jsym,jcenter,jsh,iswj,SymmCIF(2))
                t80=t80(:idel(t80))//'#'
                l=idel(SymmLabel(2))
                if(l.gt.0) t80=t80(:idel(t80))//SymmLabel(2)(:l)
                call zhusti(t80)
                if((NDimI(KPhase).eq.0.or.MaxUsedKw(KPhase).eq.0).and.
     1             VolaToDist.and.LstType.eq.1)
     2            call DistM61DistHeader(atom(i),atom(j),du,sdu,t80)
                if(j.lt.i.and.BratPrvni(j).and.BratDruhyaTreti(i)) then
                  iflg1=0
                else
                  iflg1=1
                endif
                if(.not.CalcHBonds) then
                  if(VolaToDist) then
                    call DistModifyAtName(j,At2)
                  else
                    At2=Atom(j)
                  endif
                  write(78) '#2',At2,isf(j),iswj,aij,du,sdu,
     1                      (0.,m=1,4),t80,SymmCIF(2),iflg1
                  n61=n61+1
                  if(VolaToDist.and.LstType.eq.0) then
                    call fflush
                    line=line+1
                    call DistRoundESD(t15,du,sdu,4)
                    iven(line)=atom(j)//'.....'//
     1                         t15(:idel(t15))
                    do k=1,8
                      if(iven(line)(k:k).eq.' ')
     1                  iven(line)(k:k)='.'
                    enddo
                    if(dutls.gt.0.) then
                      call fflush
                      line=line+1
                      call DistRoundESD(t15,dutls,sdu,4)
                      iven(line)='    TLS      '//
     1                           t15(:idel(t15))
!                      do k=1,8
!                        if(iven(line)(k:k).eq.' ')
!     1                    iven(line)(k:k)='.'
!                      enddo
                    endif
                  endif
                endif
                if(ntall(iswi).le.0.or.NDimI(KPhase).le.0) go to 3000
                do m=1,KModA(2,j)
                  call multm(rmp,ux(1,m,j),uxj(1,m),3,3,1)
                  call multmq(rmp,sux(1,m,j),suxj(1,m),3,3,1)
                  if(KFA(2,j).eq.0.or.m.ne.KModA(2,j)) then
                    call multm(rmp,uy(1,m,j),uyj(1,m),3,3,1)
                    call multmq(rmp,suy(1,m,j),suyj(1,m),3,3,1)
                  else
                    call CopyVek( uy(1,m,j), uyj(1,m),3)
                    call CopyVek(suy(1,m,j),suyj(1,m),3)
                  endif
                enddo
                call qbyx(shjc,qx,iswj)
                call AddVek(qx,tsp,xp,NDimI(KPhase))
                call AddVek(xp,tztnat,qx,NDimI(KPhase))
                call multm(GammaIntInv,qx,xp,NDimI(KPhase),
     1                     NDimI(KPhase),1)
                if(NonModulated(KPhase)) then
                  call SetRealArrayTo(oj,ntall(iswj),1.)
                  call SetRealArrayTo(xdj,3*ntall(iswj),0.)
                  call SetRealArrayTo(sxdj,3*ntall(iswj),0.)
                else
                  if(TypeModFun(j).le.1.or.KModA(1,j).le.1) then
                    call MakeOccMod(oj,qcnt(1,j),xp,nti,dttnat,a0(j),
     1                ax(1,j),ay(1,j),KModA(1,j),KFA(1,j),GammaIntInv)
                    if(KFA(2,j).gt.0.and.KModA(2,j).gt.0)
     1                call MakeOccModSawTooth(oj,qcnt(1,j),xp,nti,
     2                  dttnat,uyj(1,KModA(2,j)),uyj(2,KModA(2,j)),
     3                  KFA(2,j),GammaIntInv)
                  else
                    call MakeOccModPol(oj,qcnt(1,j),xp,nti,dttnat,
     1                ax(1,j),ay(1,j),KModA(1,j)-1,
     2                ax(KModA(1,j),j),a0(j)*.5,GammaIntInv,
     3                TypeModFun(j))
                  endif
                  if(TypeModFun(j).le.1) then
                    call MakePosMod(xdj,qcnt(1,j),xp,nti,dttnat,uxj,
     1                uyj,KModA(2,j),KFA(2,j),GammaIntInv)
                  else
                    call MakePosModPol(xdj,qcnt(1,j),xp,nti,dttnat,
     1                uxj,uyj,KModA(2,j),ax(1,j),a0(j)*.5,GammaIntInv,
     2                TypeModFun(j))
                  endif
                  call MakePosModS(sxdj,qcnt(1,j),xp,nti,dttnat,
     1              uxj,uyj,suxj,suyj,KModA(2,j),KFA(2,j),GammaIntInv)
                endif
                dummx=-9999.
                sdummp=-9999.
                dummn= 9999.
                dumav=0.
                sdumav=0.
                call CopyVek(y3p(4),y340,NDimI(KPhase))
                npom=0
                do kt=1,ntall(iswi)
                  call RecUnpack(kt,nd,nti,NDimI(KPhase))
                  do k=1,NDimI(KPhase)
                    t(k)=tfirsti(k)+(nd(k)-1)*dti(k)
                  enddo
                  if(oi(kt).le.ocut.or.oj(kt).le.ocut) then
                     dumkt=0.
                    sdumkt=0.
                    cycle
                  endif
                  npom=npom+1
                  if(iswj.eq.iswi) then
                    do l=1,3
                      um(l,kt)=up(l)+xdj(l,kt)-xdi(l,kt)
                    enddo
                    dumm(kt)=sqrt(uu)
                  else
                    call multm(TNaTI,t,tj,NDimI(KPhase),
     1                         NDimI(KPhase),1)
                    do l=4,NDim(KPhase)
                      y3p(l)=y340(l-3)+tj(l-3)
                    enddo
                    call multm(WPr,y3p,yp,NDim(KPhase),
     1                         NDim(KPhase),1)
                    do l=1,3
                      yp(l)=yp(l)-x1p(l)
                    enddo
                    call multm(MetTens(1,iswi,KPhase),yp,ypp,3,3,
     1                         1)
                    dumm(kt)=sqrt(scalmul(yp,ypp))
                    do l=1,3
                      yp(l)=yp(l)-xdi(l,kt)
                      ypp(l)=xdj(l,kt)
                    enddo
                    call qbyx(ypp,ypp(4),iswj)
                    call cultm(WPr,ypp,yp,NDim(KPhase),NDim(KPhase),
     1                         1)
                    call CopyVek(yp,um(1,kt),3)
                  endif
                  call multm(MetTens(1,iswi,KPhase),um(1,kt),umg,3,
     1                       3,1)
                  dumkt=sqrt(scalmul(um(1,kt),umg))
                  dum(kt)=dumkt
                  do l=1,3
                    if(dumkt.ne.0.) then
                      ddx(l+3)=umg(l)/dumkt
                      ddg(l)=um(1,kt)**2
                    else
                      ddx(l+3)=0.
                      ddg(l)=0.
                    endif
                    ddx(l)=-ddx(l+3)
                  enddo
                  if(EqRvLT0(sgx,3)) then
                    call CopyVek(sxdi(1,kt),sgxm,3)
                  else
                    call AddVekQ(sgx,sxdi(1,kt),sgxm,3)
                  endif
                  if(EqRvLT0(sgx(4),3)) then
                    call CopyVek(sxdj(1,kt),sgxm(4),3)
                  else
                    call AddVekQ(sgx(4),sxdj(1,kt),sgxm(4),3)
                  endif
                  sdumkt=sigma(sgxm,ddx,6)
                  gdu=0.
                  m=0
                  do l=1,3
                    do k=1,3
                      m=m+1
                      gdu=gdu+MetTensS(m,iswi,KPhase)*ddg(l)*ddg(k)
                    enddo
                  enddo
                  if(dumkt.gt.0.) then
                    gdu=sqrt(gdu)/dumkt
                  else
                    gdu=0.
                  endif
                  sdumkt=sqrt(sdumkt**2+gdu**2)
                  dums(kt)=sdumkt
                  sdummp=max(sdummp,sdumkt)
                  if(dumkt.gt.dummx) then
                     dummx= dumkt
                    sdummx=sdumkt
                  endif
                  if(dumkt.lt.dummn) then
                     dummn= dumkt
                    sdummn=sdumkt
                    ktmin=kt
                  endif
                   dumav= dumav+ dumkt
                  sdumav=sdumav+sdumkt
                  if(.not.CalcHBonds) then
                    if(VolaToDist) then
                      call DistModifyAtName(j,At2)
                    else
                      At2=Atom(j)
                    endif
                    write(78) sp2,At2,kt,kt,dumm(kt),dumkt,
     1                  sdumkt,oi(kt),oj(kt),0.,0.,t80,SymmCIF(2),iflg1
                    n61=n61+1
                  endif
                enddo
!                if(sdummx.le.0.) sdummx=sdummp
!                if(sdummn.le.0.) sdummn=sdummp
                sdummx=sdummp
                sdummn=sdummp
                if(npom.le.0.or.dummn.gt.dmhijm) then
                  if(VolaToDist.and.LstType.eq.0) line=max(line-1,0)
                  cycle
                endif
                if(VolaToDist) then
                  if(LstType.eq.0) then
                    call fflush
                  else if(LstType.eq.1) then
                    call DistM61DistHeader(atom(i),atom(j),du,sdu,t80)
                  endif
                endif
                if(iswi.eq.iswj) then
                  dumkt=1./float(npom)
                   dumav= dumav*dumkt
                  sdumav=sdumav*dumkt
                  if(VolaToDist) then
                    if(LstType.eq.0) then
                      call fflush
                      line=line+1
                    endif
                    call DistRoundESD(t15,dumav,sdumav,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' ave         '//t15(:l)
                      call fflush
                      line=line+1
                    else if(LstType.eq.1) then
                      s80='  ave : '//t15(:l)
                    endif
                    call DistRoundESD(t15,dummn,sdummn,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' min         '//t15(:l)
                      call fflush
                      line=line+1
                    else if(LstType.eq.1) then
                      s80=s80(:idel(s80))//'  min : '//t15(:l)
                    endif
                    call DistRoundESD(t15,dummx,sdummx,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' max         '//t15(:l)
                      call fflush
                    else if(LstType.eq.1) then
                      s80=s80(:idel(s80))//'  max : '//t15(:l)
                    endif
                  endif
                else if(VolaToDist) then
                  if(LstType.eq.0) then
                    call fflush
                    line=line+1
                  endif
                  call DistRoundESD(t15,dummn,sdummn,4)
                  l=idel(t15)
                  if(LstType.eq.0) then
                    iven(line)=' min         '//t15(:l)
                    if(ktmin.eq.1.or.ktmin.eq.nti(1))
     1                    iven(line)(9:9)='<'
                    call fflush
                  else if(LstType.eq.1) then
                    s80='  min : '//t15(:l)
                  endif
                endif
                if(.not.CalcHBonds) then
                  if(VolaToDist) then
                    call DistModifyAtName(j,At2)
                  else
                    At2=Atom(j)
                  endif
                  write(78) '#4',At2,isf(j),iswj,aij,dmod,t80,
     1                       SymmCIF(2),iflg1
                endif
                if(.not.VolaToDist) go to 3000
                if(LstType.eq.0) then
                  call fflush
                else if(LstType.eq.1) then
                  call newln(1)
                  write(lst,FormA) s80(:idel(s80))
                endif
                if(ieach.ne.0) then
                  l=0
                  do kt=1,ntall(iswi),ieach
                    call RecUnpack(kt,nd,nti,NDimI(KPhase))
                    do m=1,NDimI(KPhase)
                      t(m)=tfirsti(m)+(nd(m)-1)*dti(m)
                    enddo
                    l=l+1
                    if(oi(kt).gt.ocut.and.oj(kt).gt.ocut) then
                       dumkt=dum (kt)
                      sdumkt=dums(kt)
                      if(LstType.eq.0) then
                        call fflush
                        line=line+1
                        write(iven(line),'(2x,f5.3)') t(1)
                      endif
                      call DistRoundESD(t15,dumkt,sdumkt,4)
                      if(LstType.eq.0) then
                        iven(line)=iven(line)(:13)//
     1                    t15(:idel(t15))
                        if(oi(kt).le.oind.or.oj(kt).le.oind)
     1                     iven(line)(1:1)='*'
                        call fflush
                      else if(LstType.eq.1) then
                      endif
                    endif
                  enddo
                endif
                if(LstType.eq.0) then
                  call fflush
                  line=line+1
                  write(iven(line),'(26(''-''))')
                  if(LstType.eq.0) call fflush
                endif
3000            if(.not.iuhl0) then
                  call uhelat(i,j)
                  if(ErrFlag.ne.0) go to 9900
                  if(LstType.eq.0) call fflush
                endif
              enddo
3100        continue
          enddo
          cycle
4000      if(VolaToDist.and.NaCelkem.gt.1) then
            call FeFlowChartEvent(nfloat,ie)
            if(ie.ne.0) then
              call DistBudeBreak
              if(ErrFlag.ne.0) go to 9900
            endif
          endif
        enddo
        if(VolaToDist) then
          if(LstType.eq.0) then
            call fflush
            line=line+1
            iven(line)='*'
            call fflush
          else if(LstType.eq.1) then
            call newln(1)
            write(lst,120)
          endif
        endif
      enddo
      if(.not.VolaToDist) go to 9999
      if(NaCelkem.gt.1) call FeFlowChartRemove
      if(line.gt.0.and.LstType.eq.0) then
        call OutSt
        line=mxline
      endif
      if(nBondVal.ne.0.and..not.CalcHBonds) then
        call newpg(0)
        call TitulekVRamecku('List of bond valence sums')
        nfloat=0
        if(NDimI(KPhase).gt.0) then
          call FeFlowChartOpen(-1.,-1.,1000,n61,
     1                         'Summation of bond valences sums',' ',
     2                         ' ')
        endif
        Nacitame=.false.
        Konec=.false.
        Prvne=.true.
        n=0
        rewind 78
5000    read(78,err=5010,end=5010) Label,At2,j,iswip,pom,du,sdu,occi,
     1                             occj
        if(NDimI(KPhase).gt.0) then
          call FeFlowChartEvent(nfloat,ie)
          if(ie.ne.0) then
            call DistBudeBreak
            if(ErrFlag.ne.0) go to 9900
          endif
        endif
        go to 5020
5010    Konec=.true.
        Label='#1'
5020    if(Label.eq.'#1') then
          if(n.gt.0) then
            if(Prvne) then
              call Newln(1)
              write(lst,FormA)
              Prvne=.false.
            endif
            if(NDimI(KPhase).eq.0) then
              call Newln(1)
              sdumav=sqrt(sdumav)
              call DistRoundESD(t15,dumav,sdumav,3)
              t80='Bond valence sum for : '//at1//t15
              write(lst,FormA) t80(:idel(t80))
            else
              dummx=-9999.
              dummn= 9999.
              dumav=0.
              sdumav=0.
              sdummp=-9999.
              npom=0
              do kt=1,ntall(iswi)
                 pom=dum (kt)
                spom=dums(kt)
                sdummp=max(sdummp,sdumkt)
                if(pom.gt.0.) then
                  if(pom.gt.dummx) then
                     dummx= pom
                    sdummx=spom
                  endif
                  if(pom.lt.dummn) then
                     dummn= pom
                    sdummn=spom
                  endif
                   dumav=dumav+pom
                  sdumav=sdumav+spom
                  npom=npom+1
                endif
              enddo
              sdummx=sdummp
              sdummn=sdummp
               dumav= dumav/float(npom)
              sdumav=sdumav/float(npom)
              t80='Bond valence sum for : '//at1//' average : '
              call DistRoundESD(t15,dumav,sdumav,3)
              s80=t15
              call DistRoundESD(t15,dummn,sdummn,3)
              s80=s80(:12)//' minumum : '//t15
              call DistRoundESD(t15,dummx,sdummx,3)
              s80=s80(:33)//'  maximum : '//t15
              call Newln(2)
              write(lst,FormA)
              write(lst,FormA) t80(:idel(t80))//' '//s80(:idel(s80))
            endif
            if(ieach.ne.0.and.npom.gt.0) then
              Veta='Individual values    :'
              l=24
              do kt=1,ntall(iswi),ieach
                call RecUnpack(kt,nd,nt(1,iswi),NDimI(KPhase))
                do i=1,NDimI(KPhase)
                  t(i)=tfirsti(i)+(nd(i)-1)*dti(i)
                enddo
                call DistRoundESD(t15,dum(kt),dums(kt),2)
                write(Veta(l:),'(f6.3,1x,a)') t(1),t15(:idel(t15))
                l=l+23
                if(l.gt.111) then
                  call newln(1)
                  write(lst,FormA) Veta(:idel(Veta))
                  Veta=' '
                  l=24
                endif
              enddo
              if(l.gt.1) then
                call newln(1)
                write(lst,FormA) Veta(:idel(Veta))
              endif
            endif
          endif
          if(NDimI(KPhase).gt.0) then
            call SetRealArrayTo(dum ,ntall(iswi),0.)
            call SetRealArrayTo(dums,ntall(iswi),0.)
          endif
          if(Konec) go to 5100
          At1=At2
          isfi=j
          aii=pom
          n=0
           dumav=0.
          sdumav=0.
          Nacitame=.false.
          iswi=iswip
        else if(Label.eq.'#2') then
          isfj=j
          aij=pom
          if(NDimI(KPhase).le.0) then
            if(iBondVal(isfi,isfj).ne.0) then
              n=n+1
              pom=exp((dBondVal(isfi,isfj)-du)/cBondVal(isfi,isfj))*aij
               dumav= dumav+pom
              sdumav=sdumav+(pom*sdu/cBondVal(isfi,isfj))**2
            endif
          else
            Nacitame=.true.
          endif
        else if(Label.eq.' '.and.Nacitame) then
          kt=j
          if(iBondVal(isfi,isfj).ne.0.and.
     1       occi.gt.ocut.and.occj.gt.ocut) then
            n=n+1
            ppp=exp((dBondVal(isfi,isfj)-du)/cBondVal(isfi,isfj))*aij*
     1          occj
            dum (kt)=dum (kt)+ppp
            dums(kt)=dums(kt)+(ppp*sdu/cBondVal(isfi,isfj))**2
          endif
        else
          Nacitame=.false.
        endif
        go to 5000
5100    call FeFlowChartRemove
      endif
      if(VolaToDist.and..not.CalcHBonds) then
          call newpg(0)
          call TitulekVRamecku('Hirshfeld test')
          call CloseIfOpened(lnh)
          call OpenFile(lnh,fln(:ifln)//'.l66','formatted','unknown')
          n=0
5200      read(lnh,107,err=5250,end=5250) at1,at2,du,sdu,pom,spom
          if(pom.gt.HirshLev*spom) then
            if(n.le.0) then
              Veta='Bond             Distance          Uij projection'
              call newln(1)
              write(lst,FormA) Veta(:idel(Veta))
              call newln(1)
              write(lst,FormA)
            endif
            n=n+1
            Veta=At1(:idel(At1))//'-'//At2(:idel(At2))
            call DistRoundESD(t15,du,sdu,4)
            Veta=Veta(:17)//t15
            call DistRoundESD(t15,pom,spom,6)
            Veta=Veta(:35)//t15
            call newln(1)
            write(lst,FormA) Veta(:idel(Veta))
          endif
          go to 5200
5250      if(n.le.0) then
            Veta='There were no bonds violating Hirshfeld condition'
            call newln(1)
            write(lst,FormA) Veta(:idel(Veta))
          endif
          close(lnh,status='delete')
      endif
      if(VolaToDist.and..not.CalcHBonds) then
        rewind 78
        if(NDimI(KPhase).le.0) then
          Lab2='#2'
          Lab3='#3'
        else
          Lab2='#4'
          Lab3='#5'
        endif
6000    read(78,end=6500) Label,At3,isfj,iswj,pom,dmod,t80,t103,iflg3
        if(Label.eq.'#1') then
          At1=At3
          t101=t103
          iflg1=iflg3
        else if(Label.eq.Lab2) then
          At2=At3
          t102=t103
          iflg2=iflg3
          if(iflg1.eq.1.and.iflg2.eq.1) then
            if(IDist.le.0) then
              write(LnDist,'(''loop_'')')
              do i=10,15
                if(i.eq.12.or.i.eq.13) cycle
                j=i
                if(i.ge.14.and.i.le.15.and.NDimI(KPhase).gt.0) j=j+39
                write(LnDist,FormCIF) ' '//CifKey(j,12)
              enddo
              if(NDimI(KPhase).le.0) then
                write(LnDist,FormCIF) ' '//CifKey(12,12)
              else
                do i=52,50,-1
                  write(LnDist,FormCIF) ' '//CifKey(i,12)
                enddo
              endif
              write(LnDist,FormCIF) ' '//CifKey(13,12)
            endif
            IDist=IDist+1
            t80=At1(:idel(At1))//' '//At2(:idel(At2))//' . '//
     1          t102(:idel(t102))
            if(NDimI(KPhase).le.0) then
              n=1
            else
              n=3
            endif
            do i=2,2*n,2
              call RoundESD(t15,dmod(i-1),dmod(i),4,0,0)
              t80=t80(:idel(t80))//' '//t15(:idel(t15))
            enddo
            write(LnDist,FormA) '  '//t80(:idel(t80))//' ?'
          endif
        else if(Label.eq.Lab3) then
          if(iflg1.eq.1.and.iflg3.eq.1) then
            if(IAngle.le.0) then
              write(LnAngle,'(''loop_'')')
              do i=3,9
                if(i.eq.6) cycle
                j=i
                if(i.ge.7.and.i.le.9.and.NDimI(KPhase).gt.0) j=j+40
                write(LnAngle,FormCIF) ' '//CifKey(j,12)
              enddo
              if(NDimI(KPhase).le.0) then
                write(LnAngle,FormCIF) ' '//CifKey(2,12)
              else
                do i=46,44,-1
                  write(LnAngle,FormCIF) ' '//CifKey(i,12)
                enddo
              endif
              write(LnAngle,FormCIF) ' '//CifKey(6,12)
            endif
            IAngle=IAngle+1
            t80=At2(:idel(At2))//' '//At1(:idel(At1))//' '//
     1          At3(:idel(At3))//' '//t102(:idel(t102))//' . '//
     2          t103(:idel(t103))
            if(NDimI(KPhase).le.0) then
              n=1
            else
              n=3
            endif
            do i=2,2*n,2
              call RoundESD(t15,dmod(i-1),dmod(i),2,0,0)
              t80=t80(:idel(t80))//' '//t15(:idel(t15))
            enddo
            write(LnAngle,FormA) '  '//t80(:idel(t80))//' ?'
          endif
        endif
        go to 6000
      endif
6500  if(ittab.eq.0.or.CalcHBonds) go to 9900
      call newln(2)
      write(lst,FormA)
      write(lst,FormA)
      do i=1,NAtCalc
        if(isf(i).le.0.or..not.BratPrvni(i).or.kswa(i).ne.KPhase) cycle
        iswi=iswa(i)
        call newln(3)
        write(lst,101)
        write(lst,'(44x,''** Coordination for atom : '',a8,'' **'')')
     1        atom(i)
        write(lst,101)
        call CopyVek (dt(1,iswi),dti,NDimI(KPhase))
        call CopyVekI(nt(1,iswi),nti,NDimI(KPhase))
        call CopyVek(tfirst(1,iswi),tfirsti,NDimI(KPhase))
        t(1)=tfirsti(1)
        l=0
        ip=333
        do kt=1,ntall(iswi),ieach
          l=l+1
          ntb=0
          call SetIntArrayTo(nutab,30,0)
          rewind 78
8200      read(78,end=8340) Label,At1,isfi
          if(Label.ne.'#1'.or.At1.ne.Atom(i)) go to 8200
8300      read(78,end=8340) Label,At2,isfj
8310      if(Label.ne.'#2'.and.Label.ne.'#3') then
            if(Label.eq.'#1') then
              go to 8340
            else
              go to 8300
            endif
          else
            if(dmhk.ge.0) then
              pom=1.
            else
              pom=1.+.01*ExpDMax
            endif
            dmhijm=TypicalDistUse(isfi,isfj)*pom
            JeToUhel=Label.eq.'#3'
          endif
8320      read(78,end=8340) Label,At1,k,k,pom,pom,spom,occi,occk
          if(Label.ne.sp2) then
            At2=At1
            go to 8310
          else
            if(k.lt.kt) then
              go to 8320
            else if(k.gt.kt) then
              go to 8300
            endif
          endif
          if(JeToUhel) then
            if(occi.gt.ocut.and.occj.gt.ocut.and.occk.gt.ocut.and.
     1         pomd.le.dmhijm) then
              if(nutab(ntb).le.30) then
                nutb=nutab(ntb)+1
                nutab(ntb)=nutb
                atutab(nutb,ntb)=at2
                utab(nutb,ntb)=pom
                sutab(nutb,ntb)=spom
              else
                call FeChybne(-1.,-1.,'the maximum number of 30 '//
     1                        'neighbours exceeded.',' ',SeriousError)
                ErrFlag=1
                go to 9900
              endif
            endif
          else
            occj=occk
            pomd=pom
            if(occi.gt.ocut.and.occj.gt.ocut.and.pomd.le.dmhijm) then
              if(ntb.lt.30) then
                ntb=ntb+1
                attab(ntb)=at2
                idtab(ntb)=nint(pom*10000.)
                dstab(ntb)=spom
              else
                call FeChybne(-1.,-1.,'the maximum number of 30 '//
     1                        'neighbours exceeded.',' ',SeriousError)
                ErrFlag=1
                go to 9900
              endif
            endif
          endif
          go to 8300
8340      if(ip.gt.111) then
            if(ip.ne.333) then
              do n=1,lmax
                if(n.eq.1) then
                  call newln(3)
                  write(lst,120)
                  write(lst,FormA) ltab(n)(:idel(ltab(n)))
                  write(lst,120)
                else
                  call newln(1)
                  write(lst,FormA) ltab(n)(:idel(ltab(n)))
                endif
              enddo
            endif
            lmax=0
            do n=1,444
              ltab(n)=' '
              do ip=1,128,25
                ltab(n)(ip:ip)='|'
              enddo
            enddo
            ip=3
          endif
          n=0
          n=n+1
          write(ltab(n)(ip:ip+22),'(6x,'' t='',f6.3)') t(1)
          if(ntb.gt.0) then
            call indexx(ntb,idtab,iptab)
            do j=1,ntb
              k=iptab(j)
              n=n+1
              call DistRoundESD(t15,float(idtab(k))*.0001,dstab(k),4)
              jj=max(idel(t15),index(t15,'.')+8)
              ltab(n)(ip:ip+7)=attab(k)
              ltab(n)(ip+22-jj:ip+21)=t15(:jj)
              do m=0,10
                if(ltab(n)(ip+m:ip+m).eq.' ') ltab(n)(ip+m:ip+m)='-'
              enddo
              do m=1,nutab(k)
                n=n+1
                call DistRoundESD(t15,utab(m,k),sutab(m,k),2)
                jj=idel(t15)
                ltab(n)(ip+1:ip+8)=atutab(m,k)
                jj=max(idel(t15),index(t15,'.')+6)
                ltab(n)(ip+23-jj:ip+22)=t15(:jj)
                do mm=1,10
                  if(ltab(n)(ip+mm:ip+mm).eq.' ')
     1                                    ltab(n)(ip+mm:ip+mm)='.'
                enddo
              enddo
            enddo
          endif
8500      t(1)=t(1)+dti(1)*float(ieach)
          lmax=max(n,lmax)
          ip=ip+25
        enddo
        if(ip.gt.3) then
          do l=1,lmax
            if(l.eq.1) then
              call newln(3)
              write(lst,120)
              write(lst,FormA) ltab(l)(:idel(ltab(l)))
              write(lst,120)
            else
              call newln(1)
              write(lst,FormA) ltab(l)(:idel(ltab(l)))
            endif
          enddo
        endif
        call newln(1)
        write(lst,120)
      enddo
9900  call CloseIfOpened(78)
9999  if(.not.VolaToDist) then
        if(PrvniSymmString.ne.' ') call iom40(0,0,fln(:ifln)//'.m40')
      endif
      if(ICDDBatch)
     1  call CopyFile(fln(:ifln)//'.l61',fln(:ifln)//'.m61')
      if(allocated(TransMat)) call DeallocateTrans
      if(allocated(vt6p)) deallocate(vt6p,TypicalDistUse)
      return
100   format(a1,a8,i6,3f10.6)
101   format(44x,38('*'))
102   format(26('*'),6x)
103   format('* atom ',a8,10x,'*',6x)
104   format(a1,i6,f9.4)
105   format(f8.3)
106   format(1x,a3,9x,f7.4,12x)
107   format(2a8,6f10.6)
116   format('distance: ',2(a8,1x),2f9.4,' .',7x,a7)
117   format('angle: ',3(a8,1x),2f9.4,1x,a7,' .',7x,a7)
118   format('distance: ',2(a8,1x),6f9.4,' .',7x,a7)
119   format('angle: ',3(a8,1x),6f9.4,1x,a7,' .',7x,a7)
120   format(126('-'))
      end
