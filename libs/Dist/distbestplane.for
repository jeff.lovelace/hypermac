      subroutine DistBestPlane(AtPlane,n1,n2,iesd,isw,nr,fn,sfn)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension fn(4,*),sfn(4,*),XPlane0(:,:),SXPlane0(:,:),XPlane(:,:),
     1          SXPlane(:,:),x4dif(3),tdif(:,:),nx4(3),dx4(3),oall(:,:),
     2          uxi(:,:,:),uyi(:,:,:),suxi(:,:,:),suyi(:,:,:),
     3          GammaIntInv(:,:),
     4          isig(4),t(3),xp(3),xpp(3),iarr(:),dx(:),idx(:),fnp(4),
     5          sfnp(4),nd(4),ish(3)
      character*(*) AtPlane(*)
      character*128 Ven1,Ven2,Ven3
      logical iesd,AllAtomsOccupied
      allocatable oall,XPlane0,SXPlane0,XPlane,SXPlane,tdif,uxi,uyi,
     1            suxi,suyi,GammaIntInv,iarr,dx,idx
      call CopyVek (dt(1,isw),dti,NDimI(KPhase))
      n=NAtAll
      allocate(XPlane0(3,n),SXPlane0(3,n),XPlane(3,n),SXPlane(3,n),
     1         tdif(3,n),uxi(3,mxw,n),uyi(3,mxw,n),suxi(3,mxw,n),
     2         suyi(3,mxw,n),GammaIntInv(9,n),iarr(n),dx(n),idx(n))
      do i=1,n2
        call AtSymSpec(AtPlane(i),ia,XPlane0(1,i),x4dif,tdif(1,i),isym,
     1                 icenter,ish,ich)
        iarr(i)=ia
        if(NDim(KPhase).le.3) go to 1400
        call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv(1,i))
        do l=1,KModA(2,ia)
          call multm (rm(1,isym,isw,KPhase), ux(1,l,ia), uxi(1,l,i),3,3,
     1                1)
          call multmq(rm(1,isym,isw,KPhase),sux(1,l,ia),suxi(1,l,i),3,3,
     1                1)
          if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
             call multm (rm(1,isym,isw,KPhase), uy(1,l,ia), uyi(1,l,i),
     1                   3,3,1)
             call multmq(rm(1,isym,isw,KPhase),suy(1,l,ia),suyi(1,l,i),
     1                   3,3,1)
          else
             call CopyVek( uy(1,l,ia), uyi(1,l,i),3)
             call CopyVek(suy(1,l,ia),suyi(1,l,i),3)
          endif
          if(isym.lt.0) then
            do j=1,3
              uxi(j,l,i)=-uxi(j,l,i)
              if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0)
     1          uyi(j,l,i)=-uyi(j,l,i)
            enddo
          endif
        enddo
1400    call multmq(rm(1,isym,isw,KPhase),sx(1,ia),SXPlane0(1,i),3,3,1)
      enddo
      nr=nr+1
      if(NDimI(KPhase).gt.0) then
        allocate(oall(1,n2))
        if(VolaToDist) write(79,'(''plane'',i2)') nr
      endif
      if(VolaToDist) then
        Ven1='Equation of best plane through atoms :'
        idlp=idel(Ven1)+1
        idl =idlp
        do j=1,n1
          idla=idel(AtPlane(j))
          if(idla+idl.gt.127) then
            call newln(1)
            write(lst,FormA) Ven1(:idel(Ven1))
            Ven1=' '
            idl=idlp
          endif
          Ven1=Ven1(:idl)//AtPlane(j)(:idla)
          idl=idl+idla+1
        enddo
        if(idl.gt.idlp) then
          call newln(1)
          write(lst,FormA)
          call newln(1)
          write(lst,FormA) Ven1(:idel(Ven1))
        endif
        call newln(1)
        write(lst,FormA)
        call DistGetOneBestPlane(XPlane0,SXPlane0,n1,n2,iesd,isw,
     1                           fn(1,nr),sfn(1,nr),isig,chiq,dx,idx)
        Ven2=' '
        do i=1,4
          write(Cislo,'(f8.4)') fn(i,nr)
          call Zhusti(Cislo)
          if(i.eq.1) then
            Ven1=Cislo
          else
            if(Cislo(1:1).ne.'-') Cislo='+'//Cislo(:idel(Cislo))
            Ven1=Ven1(:idel(Ven1))//Cislo(:idel(Cislo))
          endif
          idl=idel(Ven1)
          if(i.le.3) then
            Ven1=Ven1(:idl)//'*'//smbx(i)
          else
            Ven1=Ven1(:idl)//' = 0'
          endif
          write(Cislo,FormI15) isig(i)
          call Zhusti(Cislo)
          Cislo='('//Cislo(:idel(Cislo))//')'
          j=idel(Cislo)
          Ven2(idl-j+2:)=Cislo(:j)
        enddo
        write(Cislo,'(f15.3)') chiq
        call Zhusti(Cislo)
        write(Ven1(idel(Ven1)+5:),'(''ChiQ = '',a)') Cislo(:idel(Cislo))
        call newln(2)
        write(lst,FormA) Ven1(:idel(Ven1)),Ven2(:idel(Ven2))
        call newln(1)
        write(lst,FormA)
        Ven1='Atom name :'
        Ven2='Distance from plane :'
        Ven3=' '
        l=22
        do i=1,n2
          Ven1=Ven1(1:l+5)//(AtPlane(i)(1:8))
          if(idel(AtPlane(i)).gt.8) Ven3=Ven3(1:l+4)//(AtPlane(i)(9:16))
          write(Ven2(l+1:),'(f7.4)') dx(i)
          write(Cislo,FormI15) idx(i)
          call Zhusti(Cislo)
          Cislo='('//Cislo(:idel(Cislo))//')'
          Ven2=Ven2(:idel(Ven2))//Cislo(:idel(Cislo))
          l=l+12
          if(l.gt.117) then
            idl=idel(Ven3)
            if(idl.gt.0) then
              j=4
            else
              j=3
            endif
            call NewLn(j)
            write(lst,FormA)
            write(lst,FormA) Ven1(:idel(Ven1))
            if(idl.gt.0) write(lst,FormA) Ven3(:idel(Ven3))
            write(lst,FormA) Ven2(:idel(Ven2))
            l=22
            Ven1(23:)=' '
            Ven2(23:)=' '
            Ven3(23:)=' '
          endif
        enddo
        if(l.gt.22) then
          idl=idel(Ven3)
          if(idl.gt.0) then
            j=4
          else
            j=3
          endif
          call NewLn(j)
          write(lst,FormA)
          write(lst,FormA) Ven1(:idel(Ven1))
          if(idl.gt.0) write(lst,FormA) Ven3(:idel(Ven3))
          write(lst,FormA) Ven2(:idel(Ven2))
        endif
        if(NDimI(KPhase).le.0) go to 9999
        Ven1=' '
        j=5
        call NewLn(1)
        write(lst,FormA)
        do i=1,NDimI(KPhase)
          Ven1(j:j)=smbt(i)
          j=j+8
        enddo
        j=NDimI(KPhase)*8+8
        do i=0,3
          Ven1(j:j)=char(ichar('A')+i)
          j=j+9
        enddo
        Ven1(j+25:)='Distances'
        call NewLn(2)
        write(lst,FormA) Ven1(:idel(Ven1))
        write(lst,FormA)
      else
        call OpenFile(78,fln(:ifln)//'.scr','unformatted','unknown')
      endif
      call SetIntArrayTo (nx4,NDimI(KPhase),1)
      call SetRealArrayTo(dx4,NDimI(KPhase),0.)
      call SetRealArrayTo(t,3,0.)
      do 2000kt=1,ntall(isw)
        call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
        do k=1,NDimI(KPhase)
          t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
        enddo
        AllAtomsOccupied=.true.
        do i=1,n2
          ia=iarr(i)
          call AddVek(t,tdif(1,i),xpp,NDimI(KPhase))
          call multm(GammaIntInv(1,i),xpp,xp,NDimI(KPhase),
     1               NDimI(KPhase),1)
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(oall(1,i),qcnt(1,ia),xp,nx4,dx4,a0(ia),
     1                      ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                      GammaIntInv(1,i))
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1        call MakeOccModSawTooth(oall(1,i),qcnt(1,ia),xp,nx4,dx4,
     2          uyi(1,KModA(2,ia),i),uyi(2,KModA(2,ia),i),KFA(2,ia),
     3          GammaIntInv(1,i))
          else
            call MakeOccModPol(oall(1,i),qcnt(1,ia),xp,nx4,dx4,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv(1,i),TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakePosMod(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i),uyi(1,1,i),KModA(2,ia),KFA(2,ia),
     2                      GammaIntInv(1,i))
          else
            call MakePosModPol(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                         uxi(1,1,i),uyi(1,1,i),KModA(2,ia),
     2                         ax(1,ia),a0(ia)*.5,GammaIntInv(1,i),
     3                         TypeModFun(ia))
          endif
          call AddVek(XPlane0(1,i),xpp,XPlane(1,i),3)
          call MakePosModS(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i), uyi(1,1,i),
     2                     suxi(1,1,i),suyi(1,1,i),KModA(2,ia),
     3                     KFA(2,ia),GammaIntInv(1,i))
          call AddVekQ(SXPlane0(1,i),xpp,SXPlane(1,i),3)
          if(i.le.n1.and.oall(1,i).le.ocut) then
            if(VolaToDist) then
              write(79,100)(0.,j=1,7)
            else
              write(78) kt,(0.,j=1,4)
            endif
            go to 2000
          endif
        enddo
        call SetRealArrayTo(sfnp,4,0.)
        call DistGetOneBestPlane(XPlane,SXPlane,n1,n2,iesd,isw,fnp,sfnp,
     1                           isig,chiq,dx,idx)
        if(VolaToDist) then
          write(79,100) 1.,(fnp(i),sfnp(i),i=1,3)
          if(ieach.le.0) cycle
          if(mod(kt,ieach).eq.1.or.ieach.eq.1) then
            j=1
            Ven1=' '
            do i=1,NDimI(KPhase)
              write(Ven1(j:),'(f8.3)') t(i)
              j=j+8
            enddo
            j=j+3
            do i=1,4
              if(AllAtomsOccupied) then
                write(Ven1(j:),'(f9.4)') fnp(i)
              else
                write(Ven1(j:),'(4x,5(''-''))')
              endif
              j=j+9
            enddo
            j=j+3
            jp=j
            do i=1,n2
              write(Ven1(j:),'(f7.3)') dx(i)
              j=j+7
              if(j.gt.120) then
                call NewLn(1)
                write(lst,FormA) Ven1(:idel(Ven1))
                Ven1=' '
                j=jp
              endif
            enddo
            if(j.gt.jp) then
              call NewLn(1)
              write(lst,FormA) Ven1(:idel(Ven1))
            endif
          endif
        else
          write(78) kt,(fnp(i),i=1,3),1.
        endif
2000  continue
9999  if(NDimI(KPhase).gt.0) deallocate(oall)
      deallocate(XPlane0,SXPlane0,XPlane,SXPlane,tdif,uxi,uyi,suxi,
     1           suyi,GammaIntInv,iarr,dx,idx)
      return
100   format(f8.3,6f15.6)
      end
