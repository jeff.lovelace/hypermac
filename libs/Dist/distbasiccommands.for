      subroutine DistBasicCommands
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*80 Veta
      integer CrwStateQuest
      logical CrwLogicQuest
      save nCrwRound,nCrwAngles,nCrwFullCoor,nEdwDmin,nEdwDmax,
     1     nCrwAtRad,nButtTorsion,nButtPlane,nCrwLstType,nEdwDHAMin,
     2     nCrwFromFourier,nButtBondVal,nEdwDHMax,nEdwHAMax,
     3     nEdwExpand,nLblExpand,nButtDirRef,nEdwHirshLev,nButtCenter
      entry DistBasicCommandsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      il=1
      tpom=5.
      Veta='Ro%und input coordinates'
      xpom1=tpom+FeTxLength(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom1,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwRound=CrwLastMade
      tpom=xpom1+70.
      Veta='Calculate:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      tpom=tpom+FeTxLength(Veta)+15.
      Veta='An%gles'
      xpom=tpom+FeTxLength(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwAngles=CrwLastMade
      xpom=xpom+40.
      Veta='%Torsion angles'
      dpom=FeTxLengthUnder(Veta)+40.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtTorsion=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='%Planes'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPlane=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='%Centers'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCenter=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Reference directi%on'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDirRef=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=5.
      Veta='List %full coordination'
      call FeQuestCrwMake(id,tpom,il,xpom1,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwFullCoor=CrwLastMade
      do i=1,3
        if(i.eq.1) then
          j=nCmdirnd
          nCrw=nCrwRound
        else if(i.eq.2) then
          j=nCmdiuhl
          nCrw=nCrwAngles
        else
          j=nCmdfullcoo
          nCrw=nCrwFullCoor
        endif
        call FeQuestCrwOpen(nCrw,NacetlInt(j).eq.1)
      enddo
      tpom=xqd*.5+50.
      dpom=60.
      do i=1,2
        if(i.eq.1) then
          Veta='d(mi%n)'
        else
          Veta='d(ma%x)'
        endif
        il=il+1
        if(i.eq.1) then
          ilp=il
        else
          ilp=-10*il-5
        endif
        if(i.eq.1) xpom=tpom+FeTxLengthUnder(Veta)+12.
        call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwDmin=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmddmdk),
     1                            .false.,.false.)
        else
          nEdwDmax=EdwLastMade
          tpomp=25.
          Veta='d(max) derived from atomic %radii'
          xpomp=tpomp+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpomp,il,xpomp,ilp,Veta,'L',CrwXd,
     1                        CrwYd,1,0)
          nCrwAtRad=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,NacetlReal(nCmddmhk).lt.0.)
          Veta='e%xpanded by'
          tpom=xpom-FeTxLengthUnder(Veta)-5.
          call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,
     1                        0)
          nEdwExpand=EdwLastMade
          tpom=xpom+dpom+5.
          call FeQuestLblMake(id,tpom,ilp,'%','L','N')
          nLblExpand=LblLastMade
          call FeQuestLblOff(nLblExpand)
          ilp=-10*il-7
          Veta='and typical distances'
          call FeQuestLblMake(id,tpomp,ilp,Veta,'L','N')
        endif
      enddo
      xpom=10.
      Veta='Define coefficients for bond %valences'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il-1,dpom,ButYd,Veta)
      nButtBondVal=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      ilp=-10*il-7
      Veta='Report violations of the Hirshfeld''s condition if U(proj)>'
      tpom=5.
      call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
      xpom=tpom+FeTxLength(Veta)+5.
      Veta='*s.u.'
      dpom=60.
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdHirshLev),
     1                        .false.,.false.)
      nEdwHirshLev=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,-10*il-4)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Listing form','C','B')
      dpom=xqd*.5
      xpom=dpom*.5
      il=-10*il-7
      do i=1,2
        if(i.eq.1) then
          Veta='Without %symmetry code'
        else if(i.eq.2) then
          Veta='With s%ymmetry code'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il-7,Veta,'C',CrwgXd,
     1                      CrwgYd,0,1)
        if(i.eq.1) nCrwLstType=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdLstType).eq.i-1)
        xpom=xpom+dpom
      enddo
      il=il-14
      call FeQuestLinkaMake(id,il)
      il=il-7
      call FeQuestLblMake(id,xqd*.5,il,'Include peaks from Fourier '//
     1                    'calculation','C','B')
      dpom=xqd*.25
      xpom=dpom*.5
      il=il-7
      do i=1,4
        if(i.eq.1) then
          Veta='non%e'
        else if(i.eq.2) then
          Veta='m%axima'
        else if(i.eq.3) then
          Veta='m%inima'
        else
          Veta='%both'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il-7,Veta,'C',CrwgXd,
     1                      CrwgYd,0,2)
        if(i.eq.1) nCrwFromFourier=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdincl).eq.i-1)
        xpom=xpom+dpom
      enddo
      if(NAtH(KPhase).gt.0) then
        il=il-14
        call FeQuestLinkaMake(id,il)
        il=il-7
        call FeQuestLblMake(id,xqd*.5,il,'H-bonds limits','C','B')
        il=il-10
        tpom=5.
        Veta='Maximal distance %D-H:'
        pom=FeTxLengthUnder(Veta)+25.
        xpom=tpom+pom
        dpom=60.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDHMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdDHMax),
     1                          .false.,.false.)
        tpom=xpom+dpom+25.
        xpom=tpom+pom
        Veta='Maximal distance %H...A:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwHAMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdHAMax),
     1                          .false.,.false.)
        Veta='%Minimal angle D-H...A:'
        il=il-10
        tpom=5.
        xpom=tpom+pom
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDHAMin=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdDHAMin),
     1                          .false.,.false.)
      else
        nEdwDHMax=0
        nEdwHAMax=0
        nEdwDHAMin=0
      endif
      go to 2000
      entry DistBasicCommandsCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAtRad) then
        if(CrwLogicQuest(nCrwAtRad)) then
          NacetlReal(nCmddmhk)=-1.
          call FeQuestEdwClose(nEdwDmax)
          call FeQuestRealEdwOpen(nEdwExpand,ExpDMax,.false.,.false.)
          call FeQuestLblOn(nLblExpand)
        else
          NacetlReal(nCmddmhk)=2.5
          EventType=EventEdw
          EventNumber=nEdwDmax
          call FeQuestEdwClose(nEdwExpand)
          call FeQuestLblOff(nLblExpand)
          go to 2000
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAngles) then
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTorsion)
     1  then
        call DistTorsion
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPlane)
     1  then
        call DistPlane
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCenter)
     1  then
        call DistCenterDefine
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBondVal)
     1  then
        call DistBondVal
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDirRef)
     1  then
        call DistDirRef
      endif
      go to 9999
2000  if(CrwLogicQuest(nCrwAtRad)) then
        call FeQuestRealEdwOpen(nEdwExpand,ExpDMax,.false.,.false.)
        call FeQuestLblOn(nLblExpand)
      else
        call FeQuestRealEdwOpen(nEdwDmax,NacetlReal(nCmddmhk),.false.,
     1                          .false.)
      endif
      if(CrwLogicQuest(nCrwAngles)) then
        call FeQuestCrwClose(nCrwFullCoor)
        NacetlInt(nCmdfullcoo)=1
      else
        if(CrwStateQuest(nCrwFullCoor).eq.CrwClosed)
     1    call FeQuestCrwOpen(nCrwFullCoor,NacetlInt(nCmdfullcoo).eq.1)
      endif
      go to 9999
      entry DistBasicCommandsUpdate
      do i=1,3
        if(i.eq.1) then
          j=nCmdirnd
          nCrw=nCrwRound
        else if(i.eq.2) then
          j=nCmdiuhl
          nCrw=nCrwAngles
        else
          j=nCmdfullcoo
          nCrw=nCrwFullCoor
        endif
        if(CrwStateQuest(nCrw).ne.CrwClosed) then
          if(CrwLogicQuest(nCrw)) then
            nacetlInt(j)=1
          else
            nacetlInt(j)=0
          endif
        endif
      enddo
      nCrw=nCrwLstType
      do i=1,2
        if(CrwLogicQuest(nCrw)) then
          NacetlInt(nCmdLstType)=i-1
          go to 3150
        endif
        nCrw=nCrw+1
      enddo
3150  nCrw=nCrwFromFourier
      do i=1,4
        if(CrwLogicQuest(nCrw)) then
          NacetlInt(nCmdincl)=i-1
          go to 3300
        endif
        nCrw=nCrw+1
      enddo
3300  call FeQuestRealFromEdw(nEdwDMin,NacetlReal(nCmddmdk))
      if(CrwLogicQuest(nCrwAtRad)) then
        call FeQuestRealFromEdw(nEdwExpand,NacetlReal(nCmdExpDMax))
      else
        call FeQuestRealFromEdw(nEdwDMax,NacetlReal(nCmddmhk))
      endif
      call FeQuestRealFromEdw(nEdwHirshLev,NacetlReal(nCmdHirshLev))
      if(nEdwDHMax.gt.0) then
        call FeQuestRealFromEdw(nEdwDHMax,NacetlReal(nCmdDHMax))
        call FeQuestRealFromEdw(nEdwHAMax,NacetlReal(nCmdHAMax))
        call FeQuestRealFromEdw(nEdwDHAMin,NacetlReal(nCmdDHAMin))
      endif
9999  return
      end
