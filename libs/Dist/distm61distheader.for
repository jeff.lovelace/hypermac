      subroutine DistM61DistHeader(Atom1,Atom2,Value,ESD,Symmetry)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*15  t15
      character*(*) Atom1,Atom2,Atom3,Symmetry
      Klic=0
      i=4
      go to 1000
      entry DistM61AngleHeader(Atom1,Atom2,Atom3,Value,ESD,Symmetry)
      Klic=1
      i=3
1000  call RoundESD(t15,Value,ESD,i,0,0)
      call zhusti(t15)
      t256=Atom1(:idel(Atom1))//'-'//Atom2(:idel(Atom2))
      if(Klic.eq.1) t256='     '//t256(:idel(t256))//'-'//
     1                            Atom3(:idel(Atom3))
      if(Klic.eq.0) then
        i=19
      else
        i=32
      endif
      t256(i:)=t15(:idel(t15))
      if(Klic.eq.0) then
        i=i+28
        t256(i:)=' 2nd: '//Atom2(:idel(Atom2))
      else
        i=i+15
        t256(i:)=' 3rd: '//Atom3(:idel(Atom3))
      endif
      j=index(Symmetry,'#')
      k=idel(Symmetry)
      if(k.gt.j) t256=t256(:idel(t256))//'#'//
     1                Symmetry(j+1:idel(Symmetry))
      i=i+30
      t256(i:)=Symmetry(:j-1)
      call newln(1)
      write(lst,FormA) t256(:idel(t256))
      return
      end
