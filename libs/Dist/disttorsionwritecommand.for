      subroutine DistTorsionWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*(*) Command
      character*256 EdwStringQuest
      character*80 AtNames,ErrString
      save /TorsionQuest/
      ich=0
      Command='torsion'
      nEdw=nEdwFirst
      do i=1,4
        AtNames(i)=EdwStringQuest(nEdw)
        if(AtNames(i).eq.' ') go to 9100
        call TestAtomString(AtNames(i),IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nEdw=nEdw+1
      enddo
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
