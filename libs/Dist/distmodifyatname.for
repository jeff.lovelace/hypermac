      subroutine DistModifyAtName(ia,At)
      use Atoms_mod
      use Molec_mod
      character*(*) At
      logical EqIgCase
      At=Atom(ia)
      if(kmol(ia).gt.0) then
        idl=idel(At)
        if(mam(kmol(ia)).eq.1.and.EqIgCase(At(idl:idl),'a'))
     1    At(idl:idl)=' '
      endif
      return
      end
