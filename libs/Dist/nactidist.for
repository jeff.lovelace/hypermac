      subroutine NactiDist
      use Basic_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension ip(2),xp(1)
      character*80 t80,p80
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      ln=NextLogicNumber()
      call OpenFile(ln, fln(:ifln)//'.l55','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1000  read(m50,FormA,end=1090) t80
      write(ln,FormA) t80(:idel(t80))
      call mala(t80)
      k=0
      call kus(t80,k,Cislo)
      if(Cislo.ne.'dist') go to 1000
1010  read(m50,FormA,err=9999) t80
      k=0
      call kus(t80,k,Cislo)
      if(Cislo.eq.'plane') then
        call StToInt(t80,k,ip,2,.false.,ich)
        if(ich.ne.0) then
          go to 1050
        else
          n1=ip(1)
          n2=ip(2)
          p80='plane'
          n=0
          idp=6
1020      read(m50,FormA,end=9999) t80
          k=0
1030      call kus(t80,k,Cislo)
          idc=idel(Cislo)
          if(n.eq.n1) then
            if(idp+2.le.79) then
              p80=p80(:idp)//'|'
              idp=idp+2
            else
              p80=p80(:idp)//'&'
              write(ln,FormA) p80(:idp+1)
              p80=' |'
              idp=3
            endif
          endif
          if(idp+idc.le.79) then
            p80=p80(:idp)//Cislo(:idc)
            idp=idp+idc+1
            n=n+1
          else
            p80=p80(:idp)//'&'
            write(ln,FormA) p80(:idp+1)
            p80=Cislo
            idp=idc+1
          endif
          if(n.ge.n2) then
            t80=p80
            Cislo=' '
            go to 1050
          else
            if(k.lt.80) then
              go to 1030
            else
              go to 1020
            endif
          endif
        endif
      endif
1050  write(ln,FormA) t80(:idel(t80))
      if(Cislo.eq.'end') then
        go to 1000
      else
        go to 1010
      endif
1090  close(m50)
      close(ln)
      call MoveFile(fln(:ifln)//'.l55',fln(:ifln)//'.l51')
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call OpenFile(55,fln(:ifln)//'_torpl.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('dist',i)
      if(i.ne.1) go to 9999
      i=NactiInt+NactiReal+2
      call SetIntArrayTo(NactiRepeat,i,0)
      call SetIntArrayTo(NactiRepeat(i+1),NactiKeys-i,-1)
      PreskocVykricnik=.true.
      izpet=0
      nr=0
1100  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0) go to 9999
1110  if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntDist ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealDist,NactiReal)
        go to 9999
      else if(izpet.eq.nCmdnooft) then
        do i=1,NDimI(KPhase)
          call kus(NactiVeta,PosledniPozice,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15) nt(i,1)
        enddo
      else if(izpet.eq.nCmdtFirst.or.izpet.eq.nCmdtzero) then
        call StToReal(NactiVeta,PosledniPozice,tfirst,NDimI(KPhase),
     1                .false.,ich)
        if(ich.ne.0) go to 8000
      else if(izpet.eq.nCmdtlast) then
        call StToReal(NactiVeta,PosledniPozice,tlast,NDimI(KPhase),
     1                .false.,ich)
        if(ich.ne.0) go to 8000
      else if(izpet.eq.nCmdselect.or.izpet.eq.nCmdSelfirst) then
2100    if(iselPrvni.lt.NAtCalc) iselPrvni=iselPrvni+1
        call kus(NactiVeta,PosledniPozice,aselPrvni(iselPrvni))
        call uprat(aselPrvni(iselPrvni))
        if(PosledniPozice.ne.len(NactiVeta)) go to 2100
      else if(izpet.eq.nCmdSelsecnd) then
2200    if(iselDruhyATreti.lt.NAtCalc) iselDruhyATreti=iselDruhyATreti+1
        call kus(NactiVeta,PosledniPozice,
     1           aselDruhyATreti(iselDruhyATreti))
        call uprat(aselDruhyATreti(iselDruhyATreti))
        if(PosledniPozice.ne.len(NactiVeta)) go to 2200
      else if(izpet.eq.nCmdtorsion.or.izpet.eq.nCmdplane.or.
     1        izpet.eq.nCmdCenter) then
        nr=nr+1
3000    i=idel(NactiVeta)
        write(55,FormA) NactiVeta(:i)
        if(NactiVeta(i:i).eq.'&') then
          read(m50,FormA,end=1100) NactiVeta
          go to 3000
        else
          go to 1100
        endif
      else if(izpet.eq.nCmdbondval) then
        n=nBondVal+1
        k=PosledniPozice
        call kus(NactiVeta,k,t80)
        if(index(Cifry,t80(1:1)).gt.0) then
          call StToInt(NactiVeta,PosledniPozice,ip,2,.false.,ich)
          if(ich.ne.0) go to 8000
        else
          call uprat(t80)
          ip(1)=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(ip(1).lt.0.or.ip(1).gt.NAtFormula(KPhase)) go to 8000
          call kus(NactiVeta,k,t80)
          call uprat(t80)
          ip(2)=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(ip(2).lt.0.or.ip(2).gt.NAtFormula(KPhase)) go to 8000
          PosledniPozice=k
        endif
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.ne.0) go to 8000
        dBondVal(ip(1),ip(2))=xp(1)
        dBondVal(ip(2),ip(1))=xp(1)
        iBondVal(ip(1),ip(2))=1
        iBondVal(ip(2),ip(1))=1
        nBondVal=n
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.eq.0) then
          cBondVal(ip(1),ip(2))=xp(1)
          cBondVal(ip(2),ip(1))=xp(1)
        else
          cBondVal(ip(1),ip(2))=.37
          cBondVal(ip(2),ip(1))=.37
        endif
        go to 1100
      else if(izpet.eq.nCmdDirRef) then
        if(NDirRef.ge.NDirRefMax) call ReallocDirDef(5)
        NDirRef=NDirRef+1
        call StToReal(NactiVeta,PosledniPozice,DirRef(1,NDirRef),3,
     1                .false.,ich)
        if(ich.ne.0) go to 8000
        go to 1100
      endif
      if(izpet.ne.0) go to 1100
8000  PosledniPozice=len(NactiVeta)
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      if(nr.le.0) close(55,status='delete')
      if(kcommen(KPhase).gt.0.and.ieach.gt.1) ieach=0
      return
102   format(f15.5)
      end
