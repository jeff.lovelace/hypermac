      subroutine DistTorsion
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*80 AtNames
      character*4  t4
      character*2  nty
      integer WhatHappened
      external DistTorsionReadCommand,DistTorsionWriteCommand,FeVoid
      save /TorsionQuest/
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_torsion.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      dpom=80.
      tpom=xpom+dpom*.5
      do i=1,4
        write(t4,'(''%'',i1,a2)') i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il+1,t4//' atom','C',
     1                      dpom,EdwYd,0)
        nEdw=EdwLastMade
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        endif
        xpom=xpom+dpom+20.
        tpom=tpom+dpom+20.
      enddo
1300  call SetStringArrayTo(AtNames,4,' ')
1350  nEdw=nEdwFirst
      do i=1,4
        call FeQuestStringEdwOpen(nEdw,AtNames(i))
        nEdw=nEdw+1
      enddo
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistTorsionReadCommand,DistTorsionWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
