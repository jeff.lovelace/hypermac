      subroutine DistBondVal
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      dimension ip(2),xp(2)
      character*256 t256
      character*80  t80,BVFromFile(10)
      character*2   nty,AtNames,At(2)
      integer WhatHappened,RolMenuSelectedQuest,SbwItemPointerQuest
      logical EqIgCase
      external DistBondvalReadCommand,DistBondvalWriteCommand,FeVoid
      save /BondvalQuest/
      equivalence (t80,t256)
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_bondval.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      dpom=40.
      spom=80.
      tpom=30.
      do i=1,4
        if(i.le.2) then
          write(t80,'(''%'',i1,a2,'' atom type'')') i,nty(i)
        else if(i.eq.3) then
          t80='BV-R%ho'
          dpom=60.
          spom=80.
          tpom=15.
        else if(i.eq.4) then
          t80='B%V-B'
        endif
        if(i.le.2) then
          call FeQuestRolMenuMake(id,xpom+tpom,il,xpom,il+1,t80,'C',
     1                            dpom+EdwYd,EdwYd,0)
          if(i.eq.1) nRolMenuFirst=RolMenuLastMade
        else
          call FeQuestEdwMake(id,xpom+tpom,il,xpom,il+1,t80,'C',dpom,
     1                        EdwYd,0)
        endif
        if(i.eq.3) then
          nEdwRho=EdwLastMade
          nEdwRepeatCheck=0
        else if(i.eq.4) then
          nEdwB=EdwLastMade
        endif
        xpom=xpom+spom
      enddo
      il=il+1
      dpom=60.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'F%rom file')
      nButtReadIn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1300  nRolMenu=nRolMenuFirst
      do i=1,2
        call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        nRolMenu=nRolMenu+1
      enddo
      BVRho=2.
      BVB=.37
1350  call FeQuestRealEdwOpen(nEdwRho,BVRho,.false.,.false.)
      call FeQuestRealEdwOpen(nEdwB,BVB,.false.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistBondvalReadCommand,DistBondvalWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        nRolMenu=nRolMenuFirst
        do i=1,2
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtNames(i),AtType(j,KPhase))) then
              call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                                NAtFormula(KPhase),j)
              go to 2025
            endif
          enddo
2025      nRolMenu=nRolMenu+1
        enddo
        go to 1350
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReadIn)
     1  then
        nRolMenu=nRolMenuFirst
        do i=1,2
          AtNames(i)=AtType(RolMenuSelectedQuest(nRolMenu),KPhase)
          if(AtNames(i).eq.' ') go to 2000
          call Uprat(AtNames(i))
          nRolMenu=nRolMenu+1
        enddo
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                'bvparm.cif'
        else
          t256=HomeDir(:idel(HomeDir))//'bondval/bvparm.cif'
        endif
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.ne.0) go to 2200
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_bvfile.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 2200
2110    read(ln,FormA,end=2200) t80
        call Mala(t80)
        if(index(t80,'_valence_param_details').le.0) go to 2110
        n=0
2120    read(ln,FormA,end=2130) t80
        if(t80.eq.' ') go to 2120
        if(t80(1:1).eq.'#') go to 2120
        k=0
        do i=1,2
          call Kus(t80,k,At(i))
          call StToInt(t80,k,ip(i),1,.false.,ich)
        enddo
        if( (EqIgCase(At(1),AtNames(1)).and.EqIgCase(At(2),AtNames(2)))
     1  .or.(EqIgCase(At(1),AtNames(2)).and.EqIgCase(At(2),AtNames(1))))
     2    then
          call StToReal(t80,k,xp(1),2,.false.,ich)
          n=n+1
          do i=1,2
            if(i.eq.1) then
              BVFromFile(n)=At(i)
            else
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//At(i)
            endif
            if(ip(i).ne.9) then
              write(Cislo,'(i2)') iabs(ip(i))
              call ZdrcniCisla(Cislo,1)
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//
     1                      Cislo(:idel(Cislo))
              if(ip(i).gt.0) then
                BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'+'
              else
                BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'-'
              endif
            else
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'..'
            endif
          enddo
          write(Cislo,'(f15.4)') xp(1)
          call ZdrcniCisla(Cislo,1)
          BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//
     1                  ' coefficients: '//Cislo(:idel(Cislo))
          write(Cislo,'(f15.4)') xp(2)
          call ZdrcniCisla(Cislo,1)
          BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//
     1                  Cislo(:idel(Cislo))
          write(lno,FormA) BVFromFile(n)(:idel(BVFromFile(n)))
        endif
        if(n.lt.10) go to 2120
2130    call CloseIfOpened(ln)
        call CloseIfOpened(lno)
        if(n.eq.0) then
          t80='the combination of atomic types is not on the file'
          go to 2150
        else
          idp=NextQuestId()
          xqd=300.
          call FeQuestCreate(idp,-1.,-1.,xqd,n,'Select the proper '//
     1                       'pair of ions',0,LightGray,0,0)
          xpom=5.
          dpom=xqd-10.-SbwPruhXd
          if(n.eq.1) then
            i=0
          else
            i=SbwVertical
          endif
          call FeQuestSbwMake(idp,xpom,n,dpom,n,1,CutTextFromRight,i)
          nSbw=SbwLastMade
          call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_bvfile.tmp')
2135      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
             call NebylOsetren
             go to 2135
          endif
          if(ich.eq.0) then
            i=SbwItemPointerQuest(nSbw)
            k=0
            call kus(BVFromFile(i),k,Cislo)
            call kus(BVFromFile(i),k,Cislo)
            call kus(BVFromFile(i),k,Cislo)
            call StToReal(BVFromFile(i),k,xp,2,.false.,ich)
          endif
2145      call FeQuestRemove(idp)
          call DeleteFile(fln(:ifln)//'_bvfile.tmp')
          if(ich.ne.0) go to 2200
        endif
        BVRho=xp(1)
        BVB=xp(2)
        go to 2200
2150    call FeChybne(-1.,-1.,t80,' ',SeriousError)
2200    call FeQuestButtonOff(nButtReadIn)
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
