      subroutine DistRoundESD(String,Value,SValue,DecimalPoints)
      include 'fepc.cmn'
      character*(*) String
      integer DecimalPoints
      character*8 :: FormPom = ('(f__.__)')
      call RoundESD(String,Value,SValue,6,0,0)
      if(index(String,'(').le.0) then
        if(String.ne.'0') then
          write(FormPom(3:4),'(i2)') DecimalPoints+index(String,'.')
        else
          write(FormPom(3:4),'(i2)') DecimalPoints+3
        endif
        write(FormPom(6:7),'(i2)') DecimalPoints
        write(String,FormPom) Value
        call Zhusti(String)
      endif
      return
      end
