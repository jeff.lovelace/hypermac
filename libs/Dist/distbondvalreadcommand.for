      subroutine DistBondvalReadCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      dimension ip(2),xp(2)
      character*80 t80
      character*2  AtNames
      character*(*) Command
      equivalence (ip,xp)
      save /BondvalQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'bondval'.and.t80.ne.'!bondval') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        if(index(Cifry,t80(1:1)).gt.0) then
          kk=0
          call StToInt(t80,kk,ip,1,.false.,ich)
          j=ip(1)
          if(ich.ne.0) go to 8020
          if(j.lt.1.or.j.gt.NAtFormula(KPhase)) then
            t80='the atom type number is out of range'
            go to 8100
          endif
        else
          j=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(j.le.0) then
            t80='the atom type "'//t80(:idel(t80))//'" doesn''t exist'
            go to 8100
          endif
        endif
        AtNames(i)=AtType(j,KPhase)
      enddo
      if(k.ge.lenc) go to 8000
      kk=k
      call StToReal(Command,k,xp,2,.false.,ich)
      if(ich.eq.0) then
        BVRho=xp(1)
        BVB=xp(2)
      else
        call StToReal(Command,kk,xp,1,.false.,ich)
        if(ich.ne.0) go to 8030
        BVRho=xp(1)
        BVB=.37
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8020  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command',
     1              SeriousError)
9999  return
      end
