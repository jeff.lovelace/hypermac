      subroutine deleq
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      logical IdenticalAtoms
      call newln(1)
      write(lst,FormA)
      do j=1,NAtCalc
        BratDruhyATreti(j)=BratDruhyATreti(j).and.ai(j).gt.0.
        BratPrvni(j)=BratPrvni(j).and.ai(j).gt.0.
      enddo
      if(RejectIdentical.eq.1) then
        do i=1,NAtCalc-1
          do j=i+1,NAtCalc
            if(IdenticalAtoms(i,j,.001).and.BratPrvni(i)) then
              call newln(1)
              write(lst,'(''The atom '',a8,'' deleted from the '',
     1                    ''distance calculation as coincides with : '',
     2                    a8)') atom(j),atom(i)
              BratPrvni(j)=.false.
              BratDruhyATreti(j)=.false.
            endif
          enddo
        enddo
      endif
      return
      end
