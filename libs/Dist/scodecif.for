      subroutine ScodeCIF(jsym,jcenter,jsh,isw,t10)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension jsh(3),jshp(3)
      character*(*) t10
      do i=1,3
        jshp(i)=jsh(i)+5
        pom=vt6(i,jcenter,isw,KPhase)+s6(i,jsym,isw,KPhase)
1000    if(pom.gt..999) then
          jshp(i)=jshp(i)+1
          pom=pom-1.
          go to 1000
        endif
1100    if(pom.lt.-.999) then
          jshp(i)=jshp(i)+1
          pom=pom+1.
          go to 1100
        endif
      enddo
      write(t10,'(i3,''_'',6i1)') (jcenter-1)*ncvt+jsym,jshp
      do i=1,NDimI(KPhase)
        t10=t10(:idel(t10))//'5'
      enddo
      call zhusti(t10)
      if(t10(1:5).eq.'1_555') t10='.'
      return
      end
