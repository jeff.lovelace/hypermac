      subroutine DistTorsionReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*80 t80,AtNames
      character*(*) Command
      save /TorsionQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'torsion'.and.t80.ne.'!torsion') go to 8010
      do i=1,4
        if(k.ge.lenc) go to 8000
        call kus(Command,k,AtNames(i))
        call TestAtomString(AtNames(i),IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,t80)
        if(t80.ne.' ') go to 8100
      enddo
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
