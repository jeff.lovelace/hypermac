      subroutine DistSetCommandsWatch
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      save /DistSelAtoms/
      if(KartId.ne.KartIdSelect) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
9999  return
      end
