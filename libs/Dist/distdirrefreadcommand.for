      subroutine DistDirRefReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      dimension xp(3)
      character*256 StDirRef
      character*80  t80
      character*(*) Command
      save /DirRefQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'dirref'.and.t80.ne.'!dirref') go to 8010
      if(k.ge.lenc) go to 8000
      StDirRef=Command(k+1:)
      call StToReal(Command,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
