      subroutine DistModulationCommands
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(3)
      character*80 Veta
      logical CrwLogicQuest
      save nEdwTFirst,nEdwTLast,nEdwTStep,nEdwTPrint,nCrwTPrint,
     1     nCrwTCoor,nEdwOind,nEdwOcut
      entry DistModulationCommandsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      if(NDimI(KPhase).le.0) then
        Veta='Not applicable, the structure is not modulated.'
        call FeQuestLblMake(id,xqd*.5,7,Veta,'C','B')
        go to 9999
      endif
      il=1
      nEdwTFirst=0
      nEdwTLast=0
      nEdwNoOfT=0
      nCrwTPrint=0
      dpom=50.
      if(kcommen(KPhase).le.0) then
        tpom=5.
        Veta='Calculate distances %from t='
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        xpomp=xpom
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwTFirst=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,TFirst,NDimI(KPhase),
     1                           .false.,.false.)
        tpom=xpom+dpom+10.
        Veta='t%ill t='
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwTLast=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,TLast,NDimI(KPhase),
     1                           .false.,.false.)
        tpom=xpom+dpom+10.
        Veta='with %step'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        do i=1,NDimI(KPhase)
          if(nt(i,1).le.0) then
            xp(i)=.01
          else
            xp(i)=(tlast(i,1)-tfirst(i,1))/float(nt(i,1))
          endif
        enddo
        call FeQuestRealAEdwOpen(EdwLastMade,xp,NDimI(KPhase),.false.,
     1                           .false.)
        nEdwTStep=EdwLastMade
        il=il+1
        Veta='%Print each'
        tpom=5.
        call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
        tpom=xpomp+dpom+10.
        Veta='th value'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nEdwTPrint=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdieach),.false.)
      else
        tpom=5.
        Veta='Print %individual distances'
        xpom=tpom+FeTxLengthUnder(Veta)+25.
        xpomp=xpom
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdieach).gt.0)
        nCrwTPrint=CrwLastMade
      endif
      il=il+1
      tpom=5.
      Veta='Make coordination t-%tables'
      call FeQuestCrwMake(id,tpom,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwTCoor=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdittab).eq.1)
      il=il+1
      tpom=5.
      Veta='%Calculate if occupancies of all relevant atoms are '//
     1     'larger then:'
      xpom=tpom+FeTxLengthUnder(Veta)+50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOcut=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdocut),.false.,
     1                        .false.)
      il=il+1
      Veta='Indicate by "%*" if occupancy of one of relevant atoms '//
     1     'is smaller then:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOind=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdoind),.false.,
     1                        .false.)
      go to 9999
      entry DistModulationCommandsCheck
      go to 9999
      entry DistModulationCommandsUpdate
      if(NDimI(KPhase).le.0) go to 9999
      if(CrwLogicQuest(nCrwTCoor)) then
        NacetlInt(nCmdittab)=1
      else
        NacetlInt(nCmdittab)=0
      endif
      if(kcommen(KPhase).le.0) then
        call FeQuestRealAFromEdw(nEdwTFirst,tfirst)
        call FeQuestRealAFromEdw(nEdwTLast ,tlast)
        call FeQuestRealAFromEdw(nEdwTStep ,xp)
        do i=1,NDimI(KPhase)
          nt(i,1)=nint((TLast(i,1)-TFirst(i,1))/xp(i))
        enddo
        call FeQuestIntFromEdw(nEdwTPrint,NacetlInt(nCmdieach))
      else
        if(CrwLogicQuest(nCrwTPrint)) then
          NacetlInt(nCmdieach)=1
        else
          NacetlInt(nCmdieach)=0
        endif
      endif
      call FeQuestRealFromEdw(nEdwOcut,NacetlReal(nCmdocut))
      call FeQuestRealFromEdw(nEdwOind,NacetlReal(nCmdoind))
9999  return
      end
