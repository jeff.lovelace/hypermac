      subroutine DistTorsionAngle(AtTor,Radka)
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xtor(3,4),xtorm(3,4),x4dif(3),tdif(3),nd(3),uxi(3,mxw),
     1          uyi(3,mxw),suxi(3,mxw),suyi(3,mxw),GammaIntInv(9),xp(3),
     2          xpp(3),t(3),sgx0(12),ish(3),oall(:,:),xdall(:,:,:),
     3          sxdall(:,:,:)
      character*(*) attor(4),Radka
      character*8 At(4)
      character*10 t10(4)
      character*15 t15
      character*80 t80
      character*128 ven
      allocatable oall,xdall,sxdall
      if(NDimI(KPhase).gt.0) then
        n=0
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        allocate(oall(n,4),xdall(3,n,4),sxdall(3,n,4))
      endif
      k1=1
      do i=1,4
        call AtSymSpec(attor(i),ia,xtor(1,i),x4dif,tdif,isym,icenter,
     1                 ish,ich)
        j=index(attor(i),'#')
        if(VolaToDist) call DistModifyAtName(ia,attor(i))
        if(ia.le.0) then
          t80='atom "'//attor(i)(:idel(attor(i)))//
     1        '" isn''t of the file M40'
          go to 9900
        else
          if(i.eq.1) then
            isw=iswa(ia)
          else
            if(isw.ne.iswa(ia)) then
              t80='atoms belong to different composite parts'
              go to 9900
            endif
          endif
        endif
        call CopyVek (dt(1,isw),dti,NDimI(KPhase))
        if(ich.eq.1) then
          t80='string contains non-numerical character'
          go to 9900
        else if(ich.eq.3) then
          t80='atom "'//attor(i)(:idel(attor(i)))//
     1        '" has incorrect symmetry part'
          go to 9900
        endif
        if(isym.gt.0) then
          icntrsm=1
        else
          icntrsm=2
        endif
        call ScodeCIF(isym,icenter,ish,isw,t10(i))
        if(NDim(KPhase).le.3) go to 1400
        call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv)
        do l=1,KModA(2,ia)
          call multm (rm(1,isym,isw,KPhase), ux(1,l,ia), uxi(1,l),3,3,1)
          call multmq(rm(1,isym,isw,KPhase),sux(1,l,ia),suxi(1,l),3,3,1)
          if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
             call multm (rm(1,isym,isw,KPhase), uy(1,l,ia), uyi(1,l),3,
     1                   3,1)
             call multmq(rm(1,isym,isw,KPhase),suy(1,l,ia),suyi(1,l),3,
     1                   3,1)
          else
             call CopyVek( uy(1,l,ia), uyi(1,l),3)
             call CopyVek(suy(1,l,ia),suyi(1,l),3)
          endif
          if(isym.lt.0) then
            do j=1,3
              uxi(j,l)=-uxi(j,l)
              if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) uyi(j,l)=-uyi(j,l)
            enddo
          endif
        enddo
        call AddVek(tfirst(1,isw),tdif,xpp,3)
        call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
        if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
          call MakeOccMod(oall(1,i),qcnt(1,ia),xp,nt,dt,a0(ia),
     1                    ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                    GammaIntInv)
          if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1      call MakeOccModSawTooth(oall(1,i),qcnt(1,ia),xp,nt,dt,
     2        uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     3        GammaIntInv)
        else
          call MakeOccModPol(oall(1,i),qcnt(1,ia),xp,nt,dt,
     1                       ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                       ax(KModA(1,ia),ia),a0(ia)*.5,
     3                       GammaIntInv,TypeModFun(ia))
        endif
        if(TypeModFun(ia).le.1) then
          call MakePosMod(xdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,
     1                    KModA(2,ia),KFA(2,ia),GammaIntInv)
        else
          call MakePosModPol(xdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,
     1                       KModA(2,ia),ax(1,ia),a0(ia)*.5,GammaIntInv,
     2                       TypeModFun(ia))
        endif
        call MakePosModS(sxdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,suxi,
     1                   suyi,KModA(2,ia),KFA(2,ia),GammaIntInv)
1400    call multmq(rm(1,isym,isw,KPhase),sx(1,ia),sgx(k1),3,3,1)
        k1=k1+3
      enddo
      call CopyVek(sgx,sgx0,12)
      call TorsionAngle(xtor,isw,Angle,SAngle)
      if(VolaToDist) then
        j=1
        do i=1,4
          write(ven(j:),FormA) attor(i)(:idel(attor(i)))
          j=idel(ven)+1
          if(i.ne.4) then
            ven(j:j)='-'
          else
            ven(j:j+2)=' : '
            j=j+2
          endif
          j=j+1
        enddo
        call DistRoundESD(t15,Angle,SAngle,2)
        i=idel(t15)
        ven(j:)=t15(:i)
        call newln(1)
        write(lst,FormA) Ven(:idel(Ven))
        if(ITors.le.0) then
          write(LnTors,'(''loop_'')')
          do i=34,42
            if(i.eq.38) cycle
            j=i
            if(i.ge.39.and.i.le.42.and.NDimI(KPhase).gt.0) j=j+24
            write(LnTors,FormCIF) ' '//CifKey(j,12)
          enddo
          if(NDimI(KPhase).le.0) then
            write(LnTors,FormCIF) ' '//CifKey(33,12)
            write(LnTors,FormCIF) ' '//CifKey(38,12)
          else
            do i=62,60,-1
              write(LnTors,FormCIF) ' '//CifKey(i,12)
            enddo
          endif
        endif
        ITors=ITors+1
        t80=At(1)
        do i=2,4
          t80=t80(:idel(t80))//' '//At(i)(:idel(At(i)))
        enddo
        do i=1,4
          t80=t80(:idel(t80))//' '//t10(i)(:idel(t10(i)))
        enddo
        if(NDimI(KPhase).le.0) then
          t80=t80(:idel(t80))//' '//t15(:idel(t15))//' ?'
          write(LnTors,FormA) '  '//t80(:idel(t80))
          go to 9999
        endif
      else
        call OpenFile(78,fln(:ifln)//'.scr','unformatted','unknown')
      endif
      dummx=-9999.
      dummn= 9999.
      dumav=0.
      sdumav=0.
      npom=0
      do kt=1,ntall(isw)
        call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
        k1=1
        do i=1,4
          if(oAll(kt,i).le.ocut) then
            oAll(kt,1)=0.
             dumkt=333.
            sdumkt=0.
            go to 2500
          endif
          call AddVek (xtor(1,i),xdall(1,kt,i),xtorm(1,i),3)
          call AddVekQ(sgx0(k1),sxdall(1,kt,i),sgx(k1),3)
        enddo
        call TorsionAngle(xtorm,isw,dumkt,sdumkt)
2500    if(VolaToDist) then
          if(dumkt.lt.300.) then
            dum (kt)= dumkt
            dums(kt)=sdumkt
            if(dumkt.gt.dummx) then
               dummx= dumkt
              sdummx=sdumkt
            endif
            if(dumkt.lt.dummn) then
               dummn= dumkt
              sdummn=sdumkt
            endif
             dumav= dumav+ dumkt
            sdumav=sdumav+sdumkt
            npom=npom+1
          endif
        else
          write(78) kt,dumkt,OAll(kt,1)
        endif
      enddo
      if(VolaToDist) then
        ven=' '
        l=1
        if(ieach.ne.0.and.npom.gt.0) then
          call newln(2)
          write(lst,'(/''Individual values:'')')
          do kt=1,ntall(isw),ieach
            call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
            do i=1,NDimI(KPhase)
              t(i)=tfirsti(i)+(nd(i)-1)*dti(i)
            enddo
            if(oi(kt).le.ocut) cycle
            call DistRoundESD(t15,dum(kt),dums(kt),2)
            write(ven(l:),'(f6.3,1x,a)') t(1),t15(:idel(t15))
            l=l+17
            if(l.gt.111) then
              call newln(1)
              write(lst,FormA) Ven(:idel(Ven))
              l=1
            endif
          enddo
          if(l.gt.1) then
            call newln(1)
            write(lst,FormA) Ven(:idel(Ven))
          endif
        endif
        if(npom.gt.0) then
          pom=1./float(npom)
           dumav= dumav*pom
          sdumav=sdumav*pom
          ven='Average : '
          call DistRoundESD(t15,dumav,sdumav,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
          ven=ven(:idel(ven))//', Minimum : '
          call DistRoundESD(t15,dummn,sdummn,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
          ven=ven(:idel(ven))//', Maximum : '
          call DistRoundESD(t15,dummx,sdummx,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
        else
          ven='The selected atoms are not coexisting'
        endif
        call newln(1)
        write(lst,FormA) Ven(:idel(Ven))
        call newln(1)
        write(lst,FormA)
      endif
      call RoundESD(t15,dumav,sdumav,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      call RoundESD(t15,dummn,sdummn,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      call RoundESD(t15,dummx,sdummx,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      write(LnTors,FormA) '  '//t80(:idel(t80))//' ?'
      go to 9999
9900  call FeChybne(-1.,-1.,Radka,t80,SeriousError)
9999  if(NDimI(KPhase).gt.0) deallocate(oall,xdall,sxdall)
      return
116   format('torsion: ',4(a8,1x),2f9.3/9x,4(a7,2x))
      end
