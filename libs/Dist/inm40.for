      subroutine inm40(iesd)
      use Basic_mod
      use Dist_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(6),xpp(6),GammaIntInv(9)
      integer Tisk
      logical iesd
      call CopyVek(x,xdst,3*NAtCalc)
      call CopyVek(beta,betadst,6*NAtCalc)
      if(NDimI(KPhase).gt.0) then
        call TrOrthoS(0)
        if(kcommen(KPhase).gt.0) then
          if(VolaToDist) then
            Tisk=1
          else
            Tisk=0
          endif
          n=NAtCalcBasic
          do j=1,ngc(KPhase)
            do i=1,NAtCalcBasic
              if(kswa(i).ne.KPhase) cycle
              isw=iswa(i)
              n=n+1
              call qbyx(x(1,n),qcnt(1,n),isw)
              call CopyVek(xdst(1,i),xp,3)
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              call multm(rm6gc(1,j,isw,KPhase),xp,xpp,NDim(KPhase),
     1                   NDim(KPhase),1)
              call multmq(rmgc(1,j,isw,KPhase),sx(1,i),sx(1,n),3,3,1)
              do k=1,3
                xdst(k,n)=xpp(k)+s6gc(k,j,isw,KPhase)
              enddo
              call multm(rtgc(1,j,isw,KPhase),betadst(1,i),betadst(1,n),
     1                   6,6,1)
              call multmq(rtgc(1,j,isw,KPhase),sbeta(1,i),sbeta(1,n),
     1                    6,6,1)
              do k=1,KModA(2,i)
                if(KFA(2,i).ne.1.or.k.ne.KModA(2,i)) then
                  call multmq(rmgc(1,j,isw,KPhase),sux(1,k,i),sux(1,k,n)
     1                        ,3,3,1)
                  call multmq(rmgc(1,j,isw,KPhase),sux(1,k,i),sux(1,k,n)
     1                        ,3,3,1)
                else
                  call CopyVek(sux(1,k,i),sux(1,k,n),3)
                  call CopyVek(suy(1,k,i),suy(1,k,n),3)
                endif
              enddo
            enddo
          enddo
        endif
        call UnitMat(GammaIntInv,NDimI(KPhase))
        n=0
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                               sxdk)
        allocate(oi(n),oj(n),ok(n),xdi(3,n),xdj(3,n),xdk(3,n),sxdi(3,n),
     1           sxdj(3,n),sxdk(3,n))
        if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
        allocate(um(3,n),dum(n),dam(n),dums(n),dams(n),dumm(n),dhm(4,n),
     1           dhms(4,n))
      endif
      iesd=.false.
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        dxm(i)=0.
        if(NDimI(KPhase).gt.0..and..not.NonModulated(KPhase)) then
          isw=iswa(i)
          if(TypeModFun(i).le.1.or.KModA(1,i).le.1) then
            call MakeOccMod(oi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1                      dt(1,isw),a0(i),ax(1,i),ay(1,i),KModA(1,i),
     2                      KFA(1,i),GammaIntInv)
            if(KFA(2,i).gt.0.and.KModA(2,i).gt.0)
     1        call MakeOccModSawTooth(oi,qcnt(1,i),tfirst(1,isw),
     2          nt(1,isw),dt(1,isw),uy(1,KModA(2,i),i),
     3          uy(2,KModA(2,i),i),KFA(2,i),GammaIntInv)
          else
            call MakeOccModPol(oi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ax(1,i),ay(1,i),KModA(1,i)-1,
     2        ax(KModA(1,i),i),a0(i)*.5,GammaIntInv,TypeModFun(i))
          endif
          if(TypeModFun(i).le.1) then
            call MakePosMod(xdi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ux(1,1,i),uy(1,1,i),KModA(2,i),KFA(2,i),
     2        GammaIntInv)
          else
            call MakePosModPol(xdi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ux(1,1,i),uy(1,1,i),KModA(2,i),ax(1,i),
     2        a0(i)*.5,GammaIntInv,TypeModFun(i))
          endif
          do j=1,nt(1,isw)
            if(oi(j).le.ocut) cycle
            do k=1,3
              xdi(k,j)=xdi(k,j)+x(k,i)-xdst(k,i)
            enddo
            call multm(MetTens(1,isw,KPhase),xdi(1,j),xp,3,3,1)
            dxm(i)=max(dxm(i),sqrt(scalmul(xdi(1,j),xp)))
          enddo
        endif
        if(.not.iesd) then
          do j=1,3
            if(sx(j,i).gt.0.) then
              iesd=.true.
              exit
            endif
          enddo
        endif
      enddo
9999  return
      end
