      subroutine DistCenterWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/CenterQuest/ nEdwCenter1,nEdwCenter2,AtCenter1,AtCenter2
      character*(*) Command
      character*256 EdwStringQuest,AtCenter1,AtCenter2
      character*80 ErrString
      save /CenterQuest/
      ich=0
      Command='center'
      AtCenter1=EdwStringQuest(nEdwCenter1)
      call TestAtomString(AtCenter1,IdWildNo,IdAtMolYes,IdMolNo,
     1                    IdSymmYes,IdAtMolMixNo,ErrString)
      if(ErrString.ne.' ') go to 9000
      if(AtCenter1.eq.' ') then
        ErrString='no atoms defined'
        go to 9000
      endif
      Command=Command(:idel(Command)+1)//AtCenter1(:idel(AtCenter1))
      AtCenter2=EdwStringQuest(nEdwCenter2)
      if(AtCenter2.ne.' ') then
        Command=Command(:idel(Command))//' | '//
     1          AtCenter2(:idel(AtCenter2))
        call TestAtomString(AtCenter2,IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
      endif
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      ich=1
9999  return
      end
