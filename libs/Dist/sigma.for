      function sigma(sx,dx,n)
      include 'fepc.cmn'
      dimension sx(n),dx(n)
      s=0.
      do i=1,n
        s=s+(sx(i)*dx(i))**2
      enddo
      sigma=sqrt(s)
      return
      end
