      subroutine DistPlaneReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*256 AtPlane,AtMore
      character*80  t80
      character*(*) Command
      save /PlaneQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'plane'.and.t80.ne.'!plane') go to 8010
      if(k.ge.lenc) go to 8000
      i=index(Command,'|')
      if(i.le.0) then
        AtPlane=Command(k+1:)
        AtMore=' '
      else
        AtPlane=Command(k+1:i-1)
        AtMore=Command(i+1:)
      endif
      call TestAtomString(AtPlane,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      call TestAtomString(AtMore ,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
