      subroutine DistGetOneCenterAngle(XCenter,SXCenter,n1,n2,n3,isw,
     1                               XCenterP,SXCenterP,CAngle,SCAngle)
      include 'fepc.cmn'
      include 'basic.cmn'
      real XCenter(3,*),SXCenter(3,*),XCenterP(3,3),SXCenterP(3,3),
     1    u(3),ug(3),v(3),vg(3),sgx(9),ddx(6),ddg(3)
       XCenterP=0.
      SXCenterP=0.
      do i=1,3
        if(i.eq.1) then
          j1=1
          j2=n1
        else if(i.eq.2) then
          j1=n1+1
          j2=n2
        else
          j1=n2+1
          j2=n3
        endif
        do j=j1,j2
           XCenterP(1:3,i)= XCenterP(1:3,i)+ XCenter(1:3,j)
          SXCenterP(1:3,i)=SXCenterP(1:3,i)+SXCenter(1:3,j)**2
        enddo
         XCenterP(1:3,i)=XCenterP(1:3,i)/float(j2-j1+1)
        SXCenterP(1:3,i)=sqrt(SXCenterP(1:3,i))/float(j2-j1+1)
      enddo
      sgx(1:3)=SXCenterP(1:3,1)
      sgx(4:6)=SXCenterP(1:3,2)
      sgx(7:9)=SXCenterP(1:3,3)
      u=XCenterP(1:3,1)-XCenterP(1:3,2)
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      uu=scalmul(u,ug)
      v=XCenterP(1:3,3)-XCenterP(1:3,2)
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      vv=scalmul(v,vg)
      call DistAng(u,ug,v,vg,uu,vv,sgx,
     1             MetTensS(1,isw,KPhase),CAngle,SCAngle)
      return
      end
