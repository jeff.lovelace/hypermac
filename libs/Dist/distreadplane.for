      subroutine DistReadPlane(n,o,xd,sxd,ich)
      use Dist_mod
      include 'fepc.cmn'
      character*256 Radka
      dimension o(*),xd(3,*),sxd(3,*)
      logical EqIgCase
      ich=0
      rewind 79
1100  read(79,FormA,end=9000) Radka
      k=0
      call Kus(Radka,k,Cislo)
      if(EqIgCase(Cislo,'plane')) then
        call Kus(Radka,k,Cislo)
        call posun(Cislo,0)
        read(Cislo,FormI15,err=9000) i
        if(i.eq.n) go to 1200
      endif
      go to 1100
1200  i=0
1300  read(79,FormA,end=9999) Radka
      if(Radka.eq.' ') go to 1300
      k=0
      call Kus(Radka,k,Cislo)
      if(EqIgCase(Cislo,'plane')) go to 9999
      i=i+1
      read(Radka,100,err=9000,end=9000) o(i),(xd(j,i),sxd(j,i),j=1,3)
      go to 1300
9000  ich=1
9999  return
100   format(f8.3,6f15.6)
      end
