      subroutine dist
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*256 Radka,t256
      character*128 Hlavicka
      character*80, allocatable ::  AtPlane(:)
      character*80  t80,AtTor(4)
      character*15  t15
      character*11  FormD
      character*8 :: Nazev,Text(3)=(/'torsion','plane  ','center '/)
      dimension fmp(4,20),sfmp(4,20),ip(2),xs(3),xp(6),nx4(3),dx4(3),
     1          t(3),nd(4)
      logical iesd,ExistFile,PrvniTorzniUhel,OldPlane,PrvniRovina,
     1        PrvniCenter
      logical CIFAllocated
      real, allocatable :: sngc(:,:,:),csgc(:,:,:)
      Uloha='Calculating of distances, angles, torsion angles and '//
     1      'best planes'
      t80=fln(:ifln)//'_dist.tmp'
      call DeleteFile(t80)
      call OpenFile(lst,fln(:ifln)//'.dis','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      LnDist=NextLogicNumber()
      call OpenFile(LnDist,fln(:ifln)//'_dist.txt','formatted',
     1              'unknown')
      LnAngle=NextLogicNumber()
      call OpenFile(LnAngle,fln(:ifln)//'_angle.txt','formatted',
     1              'unknown')
      LnTors=NextLogicNumber()
      call OpenFile(LnTors,fln(:ifln)//'_tors.txt','formatted',
     1              'unknown')
      LnHBonds=NextLogicNumber()
      call OpenFile(LnHBonds,fln(:ifln)//'_hbonds.txt','formatted',
     1              'unknown')
      IDist=0
      IAngle=0
      ITors=0
      LstOpened=.true.
      VolaToDist=.true.
      call iom50(0,1,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9910
      call ComSym(0,1,ich)
      if(ngc(KPhase).gt.0) then
        allocate(sngc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc),
     1           csgc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc))
        call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),NAtCalc)
        if(ErrFlag.ne.0) go to 9999
      endif
      call specat
      n=NAtCalc+5
      allocate(xdst(3,n),AtPlane(n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call DefaultDist
      call NactiDist
      if(ErrFlag.ne.0) go to 9999
      ncvt=NSymmN(KPhase)
      if(iuhl.eq.1) FullCoo=1
      write(FormTD(2:2),'(i1)') NDimI(KPhase)
      write(FormTA(2:2),'(i1)') NDimI(KPhase)
      if(MaxUsedKw(KPhase).le.0.and.NComp(KPhase).le.1)
     1  call SetIntArrayTo(nt,9,0)
      if(nt(1,1).ne.0) then
        if(kcommen(KPhase).le.0) then
          do i=1,NDimI(KPhase)
            nt(i,1)=nt(i,1)+1
            dt(i,1)=(tlast(i,1)-tfirst(i,1))/float(nt(i,1)-1)
            do j=2,NComp(KPhase)
              nt(i,j)=nt(i,1)
              tfirst(i,j)=tfirst(i,1)
              tlast(i,j)=tlast(i,1)
              dt(i,j)=(tlast(i,j)-tfirst(i,j))/float(nt(i,j)-1)
            enddo
          enddo
        else
          do isw=1,NComp(KPhase)
            call CopyVek(trez(1,isw,KPhase),tfirst(1,isw),NDimI(KPhase))
            call CopyVekI(NCommQ(1,isw,KPhase),nt(1,isw),NDimI(KPhase))
            do i=1,NDimI(KPhase)
              nt(i,isw)=nt(i,isw)
              dt(i,isw)=1./float(nt(i,isw))
              tlast(i,isw)=tfirst(i,isw)+float(nt(i,isw)-1)*dt(i,isw)
            enddo
          enddo
        endif
      endif
      do i=1,NComp(KPhase)
        ntall(i)=1
        do j=1,NDimI(KPhase)
          ntall(i)=ntall(i)*nt(j,i)
        enddo
      enddo
      if(ntall(1).le.1.or.NDimI(KPhase).ne.1) ittab=0
      if(ittab.ne.0.and.ieach.eq.0) ieach=max(ntall(1)/10,1)
      pom=0.
      call inm40(iesd)
      if(ErrFlag.ne.0) go to 9900
      if(iesd) then
        call newln(2)
        write(lst,'(/''Input e.s.d of fraction coordinates of atoms '',
     1               ''included'')')
      else
        irnd=0
      endif
      call pisat
      call distat
      if(ErrFlag.ne.0) go to 9900
      if(NAtH(KPhase).gt.0.and..not.ICDDBatch) then
        CalcHBonds=.true.
        call DistAt
        CalcHBonds=.false.
      endif
4000  nr=0
      if(.not.ExistFile(fln(:ifln)//'_torpl.tmp')) go to 9000
      rewind 55
      PrvniTorzniUhel=.true.
      PrvniRovina=.true.
      PrvniCenter=.true.
4100  read(55,FormA,err=8000,end=8000) Radka
      if(Radka(1:1).eq.'*') go to 4100
      kk=0
      call mala(Radka)
      call kus(Radka,kk,nazev)
      if(nazev.eq.'end') go to 8000
      i=islovo(nazev,text,3)
      if(i.le.0) go to 4100
      go to (5000,6000,7000),i
5000  if(PrvniTorzniUhel) then
        call NewPg(0)
        call TitulekVRamecku('List of torsion angles')
        PrvniTorzniUhel=.false.
      endif
      do i=1,4
        call kus(Radka,kk,attor(i))
        call uprat(attor(i))
      enddo
      call DistTorsionAngle(AtTor,Radka)
      go to 4100
6000  if(PrvniRovina) then
        call NewPg(0)
        call TitulekVRamecku('List of best planes')
        if(NDimI(KPhase).gt.0)
     1    call OpenFile(79,fln(:ifln)//'_plane.txt','formatted',
     2                  'unknown')
        PrvniRovina=.false.
      endif
      kkp=kk
6010  call StToInt(Radka,kk,ip,2,.false.,ich)
      if(ich.eq.0) then
        n1=ip(1)
        n2=ip(2)
        if(n1.lt.3) then
          call FeChybne(-1.,-1.,Radka,'number of atoms defining plane'//
     1                  ' smaller than 3.',SeriousError)
          go to 4100
        endif
        OldPlane=.true.
        kk=len(Radka)
        nn=n2
      else
        kk=kkp
        nn=NAtAll
        OldPlane=.false.
        n1=0
6012    read(55,FormA,end=6015) t256
        call SkrtniPrvniMezery(t256)
        if(t256(1:1).eq.'&') then
          Radka=Radka(:idel(Radka))//t256(:idel(t256))
          go to 6012
        endif
6015    backspace 55
      endif
      do i=1,nn
        if(kk.eq.len(Radka)) then
          if(OldPlane) then
            read(55,FormA) Radka
            call mala(Radka)
            kk=0
          else
            n2=i-1
            if(n1.le.0) n1=n2
            go to 6050
          endif
        endif
6020    call kus(Radka,kk,t80)
        if(.not.OldPlane) then
          if(t80.eq.'|') then
            n1=i-1
            go to 6020
          endif
        endif
        AtPlane(i)=t80
        call uprat(AtPlane(i))
        call atsym(AtPlane(i),n,xs,xp,xp(4),jsym,ich)
        if(n.le.0) then
          t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' doesn''t '//
     1        'exist.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        else
          if(i.eq.1) then
            isw=iswa(n)
          else
            if(isw.ne.iswa(n)) then
              t80='atoms belong to different composite parts.'
              call FeChybne(-1.,-1.,Radka,t80,SeriousError)
              go to 4100
            endif
          endif
        endif
        if(ich.eq.1) then
          t80='string contains non-numerical character.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        else if(ich.eq.3) then
          t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' has incorrect '
     1        //'symmetry part.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        endif
      enddo
6050  call DistBestPlane(AtPlane,n1,n2,iesd,isw,nr,fmp,sfmp)
      go to 4100
7000  if(PrvniCenter) then
        call NewPg(0)
        call TitulekVRamecku('List of distances from atomic center')
        PrvniCenter=.false.
      endif
7012  read(55,FormA,end=7015) t256
      call SkrtniPrvniMezery(t256)
      if(t256(1:1).eq.'&') then
        Radka=Radka(:idel(Radka))//t256(:idel(t256))
        go to 7012
      endif
7015  backspace 55
      i=0
7020  if(kk.eq.len(Radka)) then
        n2=i
        if(n1.le.0) n1=n2
        go to 7050
      endif
7030  call kus(Radka,kk,t80)
      if(t80.eq.'|') then
        n1=i
        go to 7030
      endif
      i=i+1
      AtPlane(i)=t80
      call uprat(AtPlane(i))
      call atsym(AtPlane(i),n,xs,xp,xp(4),jsym,ich)
      if(n.le.0) then
        t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' doesn''t '//
     1      'exist.'
        call FeChybne(-1.,-1.,Radka,t80,SeriousError)
        go to 4100
      else
        if(i.eq.1) then
          isw=iswa(n)
        else
          if(isw.ne.iswa(n)) then
            t80='atoms belong to different composite parts.'
            call FeChybne(-1.,-1.,Radka,t80,SeriousError)
            go to 4100
          endif
        endif
      endif
      if(ich.eq.1) then
        t80='string contains non-numerical character.'
        call FeChybne(-1.,-1.,Radka,t80,SeriousError)
        go to 4100
      else if(ich.eq.3) then
        t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' has incorrect '
     1      //'symmetry part.'
        call FeChybne(-1.,-1.,Radka,t80,SeriousError)
        go to 4100
      endif
      go to 7020
7050  call DistCenter(AtPlane,n1,n2,isw)
      go to 4100
8000  close(55,status='delete')
      if(nr.lt.1.or.(nr.lt.2.and.NDirRef.le.0)) go to 9000
      if(NDimI(KPhase).gt.0) then
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                               sxdk)
        allocate(oi(n),oj(n),ok(n),xdi(3,n),xdj(3,n),xdk(3,n),sxdi(3,n),
     1           sxdj(3,n),sxdk(3,n))
        call SetIntArrayTo (nx4,NDimI(KPhase),1)
        call SetRealArrayTo(dx4,NDimI(KPhase),0.)
        call SetRealArrayTo(t,3,0.)
        FormD='( f8.3,a11)'
        if(NDimI(KPhase).eq.1) then
          Hlavicka='    t       Angle'
          NFormD=19
        else
          Hlavicka='    t(1)    t(2)'
          NFormD=27
          if(NDimI(KPhase).eq.3) then
            Hlavicka(17:)='    t(3)'
            NFormD=NFormD+8
          endif
          Hlavicka=Hlavicka(:idel(Hlavicka))//'    Angle'
          write(FormD(2:2),'(i1)') NDimI(KPhase)
        endif
        m=128/NFormD
        do i=1,m-1
          Hlavicka(NFormD*i+1:)=Hlavicka(:NFormD)
        enddo
      endif
      do id=1,2
        if(id.eq.1) then
          if(nr.le.2) cycle
          Radka='List dihedral angles'
        else
          if(NDirRef.le.0) cycle
          Radka='List of angles betwen calculated planes and '//
     1          'reference directions'
        endif
        call NewPg(0)
        call TitulekVRamecku(Radka)
        ichi=0
        ichj=0
        do i=1,nr
          if(i.eq.nr.and.id.eq.1) cycle
          if(NDimI(KPhase).gt.0) call DistReadPlane(i,oi,xdi,sxdi,ichi)
          write(Nazev,'(i5)') i
          call Zhusti(Nazev)
          if(id.eq.1) then
            jp=i+1
            jk=nr
          else
            jp=1
            jk=NDirRef
          endif
          do j=jp,jk
            if(id.eq.2) then
              call CopyVek(DirRef(1,j),xp,3)
              call multm(MetTensI(1,isw,KPhase),xp,xs,3,3,1)
              call VecNor(xp,xs)
            endif
            if(NDimI(KPhase).gt.0.and.ichi.eq.0) then
              if(id.eq.1) then
                call DistReadPlane(j,oj,xdj,sxdj,ichj)
              else
                do kt=1,ntall(isw)
                  oj(kt)=1.
                  xdj(1,kt)=xp(1)
                  xdj(2,kt)=xp(2)
                  xdj(3,kt)=xp(3)
                  sxdj(1,kt)=0.
                  sxdj(2,kt)=0.
                  sxdj(3,kt)=0.
                enddo
                ichj=0
              endif
               dumav=0.
              sdumav=0.
               dummx=0.
              sdummx=0.
              dummn=9999.
              sdummn=0.
              n=0
              if(ichj.eq.0) then
                do kt=1,ntall(isw)
                  ok(kt)=0.
                  call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
                  do k=1,NDimI(KPhase)
                    t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
                  enddo
                  if(oi(kt).le.0..or.oj(kt).le.0.) cycle
                  ok(kt)=1.
                  call DistDihedralAngle(xdi(1,kt),xdj(1,kt),sxdi(1,kt),
     1                                   sxdj(1,kt),isw,xdk(1,kt),
     2                                   sxdk(1,kt))
                  if(xdk(1,kt).lt.dummn) then
                     dummn= xdk(1,kt)
                    sdummn=sxdk(1,kt)
                  endif
                  if(xdk(1,kt).gt.dummx) then
                     dummx= xdk(1,kt)
                    sdummx=sxdk(1,kt)
                  endif
                   dumav= dumav+ xdk(1,kt)
                  sdumav=sdumav+sxdk(1,kt)
                  n=n+1
                enddo
                if(n.gt.0) then
                   dumav= dumav/float(n)
                  sdumav=sdumav/float(n)
                endif
              endif
            endif
            if(id.eq.1) then
              write(Cislo,FormI15) j
              call Zhusti(Cislo)
              Radka='Dihedral angle between plane#'//
     1              Nazev(:idel(Nazev))//
     2              ' and plane#'//Cislo(:idel(Cislo))
            else
              write(t80,'(3f10.3)')(DirRef(k,j),k=1,3)
              call ZdrcniCisla(t80,3)
              t80='('//t80(:idel(t80))//')'
              do k=1,idel(t80)
                if(t80(k:k).eq.' ') t80(k:k)=','
              enddo
              Radka='Angle between plane#'//Nazev(:idel(Nazev))//
     2              ' and direction '//t80(:idel(t80))
            endif
            if(NDimI(KPhase).gt.0) then
              Radka(idel(Radka)+1:)=' - Average : '
              call DistRoundESD(Cislo,dumav,sdumav,2)
              Radka=Radka(:idel(Radka)+1)//Cislo(:idel(Cislo))
              Radka=Radka(:idel(Radka))//', Minimum : '
              call DistRoundESD(Cislo,dummn,sdummn,2)
              Radka=Radka(:idel(Radka)+1)//Cislo(:idel(Cislo))
              Radka=Radka(:idel(Radka))//', Maximum : '
              call DistRoundESD(Cislo,dummx,sdummx,2)
              Radka=Radka(:idel(Radka)+1)//Cislo(:idel(Cislo))
            else
              if(id.eq.1) then
                k=j
              else
                k=nr+1
                call CopyVek(xp,fmp(1,k),3)
                sfmp(1,k)=0.
                sfmp(2,k)=0.
                sfmp(3,k)=0.
              endif
              call DistDihedralAngle(fmp(1,i),fmp(1,k),sfmp(1,i),
     1                               sfmp(1,k),isw,du,sdu)
              call DistRoundESD(Cislo,du,sdu,2)
              Radka=Radka(:idel(Radka))//': '//Cislo(:idel(Cislo))
            endif
            call NewLn(1)
            write(lst,FormA)
            call NewLn(1)
            write(lst,FormA) Radka(:idel(Radka))
            if(NDimI(KPhase).gt.0.and.ieach.gt.0) then
              call NewLn(3)
              write(lst,FormA)
              write(lst,FormA) Hlavicka(:idel(Hlavicka))
              write(lst,FormA)
              Radka=' '
              n=1
              do kt=1,ntall(isw),ieach
                if(ok(kt).le.0.) cycle
                call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
                do k=1,NDimI(KPhase)
                  t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
                enddo
                if(n.gt.128-NFormD) then
                  callNewLn(1)
                  write(lst,FormA) Radka(:idel(Radka))
                  n=1
                  Radka=' '
                endif
                call DistRoundESD(Cislo,xdk(1,kt),sxdk(1,kt),2)
                write(Radka(n:),FormD)(t(k),k=1,NDimI(KPhase)),
     1                                 Cislo(:idel(Cislo))
                n=n+NFormD
              enddo
              if(Radka.ne.' ') then
                callNewLn(1)
                write(lst,FormA) Radka(:idel(Radka))
              endif
            endif
          enddo
        enddo
      enddo
9000  call CloseListing
      close(79,status='delete')
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_Dist.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# Dist'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      if(IDist.gt.0) then
        rewind LnDist
9010    read(LnDist,FormA,end=9020) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9010
      endif
9020  if(IAngle.gt.0) then
        rewind LnAngle
        write(LnSum,FormA)
9030    read(LnAngle,FormA,end=9040) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9030
      endif
9040  if(ITors.gt.0) then
        rewind LnTors
        write(LnSum,FormA)
9050    read(LnTors,FormA,end=9060) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9050
      endif
9060  if(IHBonds.gt.0) then
        rewind LnHBonds
        write(LnSum,FormA)
9070    read(LnHBonds,FormA,end=9080) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9070
      endif
9080  call CloseIfOpened(LnSum)
      call CloseIfOpened(m50)
      if(.not.BatchMode) then
        NInfo=0
        call FeShowListing(-1.,-1.,'DIST program',fln(:ifln)//'.dis',
     1                     10000)
      endif
9900  call DeleteFile(PreviousM40)
      close(LnDist,Status='delete')
      close(LnAngle,Status='delete')
      close(LnTors,Status='delete')
      close(LnHBonds,Status='delete')
9910  if(ngcMax.ne.0) call DeleteFile(PreviousM50)
9999  if(allocated(xdst)) deallocate(xdst,AtPlane,BetaDst,dxm,BratPrvni,
     1  BratDruhyATreti,aselPrvni,aselDruhyATreti)
      if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,sxdk)
      if(.not.CIFAllocated) deallocate(CifKey,CifKeyFlag)
      if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
      if(allocated(DirRef)) then
        deallocate(DirRef)
        NDirRefMax=0
      endif
      if(allocated(sngc)) deallocate(sngc,csgc)
      if(NDimI(KPhase).gt.0) call TrOrthoS(1)
      call iom50(0,0,fln(:ifln)//'.m50')
      return
      end
