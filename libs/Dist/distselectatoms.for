      subroutine DistSelectAtoms
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*256 EdwStringQuest
      character*80 Veta
      integer SbwLnQuest,SbwItemPointerQuest,SbwItemSelQuest,
     1        RolMenuSelectedQuest
      logical CrwLogicQuest,lpom,PrvniSelected,PrvniSelectedOld,EqWild
      save nCrwSelCenter,nCrwSelNeighbour,nSbwSelect,nButtAll,
     1     nButtRefresh,nCrwSkipSplit,nButtAtMolSplit
      save /DistSelAtoms/
      entry DistSelectAtomsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      il=0
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Select %central atom(s)'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
        if(i.eq.1) then
          nCrwSelCenter=CrwLastMade
          Veta='Select neighbour atom(s)'
        else
          nCrwSelNeighbour=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      xpom=5.
      dpom=xqd-10.
      il=11
      ild=9
      call FeQuestSbwMake(id,xpom,il,dpom,ild,6,CutTextFromRight,
     1                    SbwHorizontal)
      nSbwSelect=SbwLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_lista.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NAtCalcBasic
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
      enddo
      call CloseIfOpened(ln)
      il=il+1
      dpom=80.
      xpom=xqd*.5-dpom-10.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,-10*il-5,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+10.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,-10*il-5,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=5.
      dpom=80.
      tpom=xpom+dpom+10.
      il=il+1
      ilp=-10*(il+1)-5.
      call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,'=>','L',dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      xpom=tpom+30.
      dpom=100.
      Veta='%Include atoms'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtInclAtoms=ButtonLastMade
          Veta='%Exclude atoms'
        else
          nButtExclAtoms=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
      enddo
      il=il-2
      xpom=xqd*.5+5.
      dpom=80.
      tpom=xpom+dpom+10.
      ilp=-10*(il+1)-5
      call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,'=>','L',dpom,EdwYd,
     1                        0)
      nRolMenuTypes=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                        NAtFormula(KPhase),0)
      xpom=tpom+30.
      dpom=100.
      Veta='Include atom %types'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtInclTypes=ButtonLastMade
          Veta='Exclude atom t%ypes'
        else
          nButtExclTypes=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Skip split atoms/molecules'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdSkipSplit).ne.0)
      nCrwSkipSplit=CrwLastMade
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      Veta='%Define/Edit split atoms/molecules'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAtMolSplit=ButtonLastMade
      go to 2500
      entry DistSelectAtomsCheck
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwSelCenter.or.
     2    CheckNumber.eq.nCrwSelNeighbour)) then
        PrvniSelectedOld=.not.CrwLogicQuest(nCrwSelCenter)
        j=SbwItemPointerQuest(nSbwSelect)
        do i=1,NAtCalcBasic
          lpom=SbwItemSelQuest(i,nSbwSelect).eq.1
          if(lpom) j=0
          if(PrvniSelectedOld) then
            BratPrvni(i)=lpom
          else
            BratDruhyATreti(i)=lpom
          endif
        enddo
        if(j.ne.0) then
          if(PrvniSelectedOld) then
            BratPrvni(j)=.true.
          else
            BratDruhyATreti(j)=.true.
          endif
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        if(CrwLogicQuest(nCrwSelCenter)) then
          call SetLogicalArrayTo(BratPrvni,NAtCalcBasic,lpom)
        else
          call SetLogicalArrayTo(BratDruhyATreti,NAtCalcBasic,lpom)
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclAtoms.or.
     2         CheckNumber.eq.nButtExclAtoms)) then
        lpom=CheckNumber.eq.nButtInclAtoms
        Veta=EdwStringQuest(nEdwAtoms)
        if(Veta.eq.' ') go to 9999
        call mala(Veta)
        do i=1,NAtCalcBasic
          if(EqWild(Atom(i),Veta,.false.)) then
            if(CrwLogicQuest(nCrwSelCenter)) then
              BratPrvni(i)=lpom
            else
              BratDruhyATreti(i)=lpom
            endif
          endif
        enddo
        call FeQuestStringEdwOpen(nEdwAtoms,' ')
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclTypes.or.
     2         CheckNumber.eq.nButtExclTypes)) then
        lpom=CheckNumber.eq.nButtInclTypes
        isfp=RolMenuSelectedQuest(nRolMenuTypes)
        do i=1,NAtCalcBasic
          if(isf(i).eq.isfp) then
            if(CrwLogicQuest(nCrwSelCenter)) then
              BratPrvni(i)=lpom
            else
              BratDruhyATreti(i)=lpom
            endif
          endif
        enddo
        call FeQuestRolMenuOpen(nRolMenuTypes,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSkipSplit)
     1  then
        if(CrwLogicQuest(nCrwSkipSplit)) then
          NacetlInt(nCmdSkipSplit)=1
        else
          NacetlInt(nCmdSkipSplit)=0
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtAtMolSplit) then
        call EditAtMolSplit
      endif
      go to 9999
2500  call CloseIfOpened(SbwLnQuest(nSbwSelect))
      PrvniSelected=CrwLogicQuest(nCrwSelCenter)
      do i=1,NAtCalcBasic
        if(PrvniSelected) then
          lpom=BratPrvni(i)
        else
          lpom=BratDruhyATreti(i)
        endif
        if(lpom) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSelect,j)
      enddo
      call FeQuestSbwSelectOpen(nSbwSelect,fln(:ifln)//'_lista.tmp')
      go to 9999
      entry DistSelectAtomsUpdate
      PrvniSelected=CrwLogicQuest(nCrwSelCenter)
      j=SbwItemPointerQuest(nSbwSelect)
      do i=1,NAtCalcBasic
        lpom=SbwItemSelQuest(i,nSbwSelect).eq.1
        if(lpom) j=0
        if(PrvniSelected) then
          BratPrvni(i)=lpom
        else
          BratDruhyATreti(i)=lpom
        endif
      enddo
      if(j.ne.0) then
        if(PrvniSelected) then
          BratPrvni(j)=.true.
        else
          BratDruhyATreti(j)=.true.
        endif
      endif
9999  return
      end
