      subroutine AngleCenter(AtCenter,n1,n2,n3,isw)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*(*) AtCenter(*)
      character*128 Ven
      integer, allocatable :: IAtoms(:)
      integer ish(3),nd(4),nx4(3)
      real, allocatable :: XCenter0(:,:),SXCenter0(:,:),
     1                     XCenter(:,:) ,SXCenter(:,:),
     2                     uxi(:,:,:),suxi(:,:,:),uyi(:,:,:),
     3                     suyi(:,:,:),GammaIntInv(:,:),tdif(:,:),
     4                     oall(:),CAngleAll(:),SCAngleAll(:)
      real x4dif(3),XCenterP(3,3),SXCenterP(3,3),t(3),dx4(3),xp(3),
     1     xpp(3),oind(1)
      if(allocated(IAtoms)) deallocate(IAtoms,XCenter0,SXCenter0,
     1                                 XCenter,SXCenter,uxi,suxi,uyi,
     2                                 suyi,GammaIntInv,tdif)
      allocate(IAtoms(n3),XCenter0(3,n3),SXCenter0(3,n3),
     1         XCenter(3,n3),SXCenter(3,n3),
     2         uxi(3,mxw,n3),suxi(3,mxw,n3),uyi(3,mxw,n3),
     3         suyi(3,mxw,n3),GammaIntInv(9,n3),tdif(3,n3))
      call CopyVek (dt(1,isw),dti,NDimI(KPhase))
      if(NDimI(KPhase).gt.0) then
        n=0
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        if(allocated(oall)) deallocate(oall,CAngleAll,SCAngleAll)
        allocate(oall(n),CAngleAll(n),SCAngleAll(n))
      endif
      do j=1,n3
        call AtSymSpec(AtCenter(j),ia,XCenter0(1,j),x4dif,tdif(1,j),
     1                 isym,icenter,ish,ich)
        IAtoms(j)=ia
        if(NDimI(KPhase).gt.0) then
          call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv(1,j))
          do l=1,KModA(2,ia)
            call multm (rm(1,isym,isw,KPhase), ux(1,l,ia), uxi(1,l,j),
     1                 3,3,1)
            call multmq(rm(1,isym,isw,KPhase),sux(1,l,ia),suxi(1,l,j),
     1                 3,3,1)
            if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
              call multm (rm(1,isym,isw,KPhase), uy(1,l,ia), uyi(1,l,j),
     1                    3,3,1)
              call multmq(rm(1,isym,isw,KPhase),suy(1,l,ia),suyi(1,l,j),
     1                    3,3,1)
            else
               call CopyVek( uy(1,l,ia), uyi(1,l,j),3)
               call CopyVek(suy(1,l,ia),suyi(1,l,j),3)
            endif
            if(isym.lt.0) then
              do k=1,3
                uxi(k,l,j)=-uxi(k,l,j)
                if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0)
     1            uyi(k,l,j)=-uyi(k,l,j)
              enddo
            endif
          enddo
        endif
        call multmq(rm(1,isym,isw,KPhase),sx(1,ia),SXCenter0(1,j),
     1              3,3,1)
      enddo
!      if(VolaToDist) then
!        Ven='Between the atomic center of :'
!        idlp=idel(Ven)+1
!        idl =idlp
!        do j=1,n1
!          idla=idel(AtCenter(j))
!          if(idla+idl.gt.127) then
!            call newln(1)
!            write(lst,FormA) Ven(:idel(Ven))
!            Ven=' '
!            idl=idlp
!          endif
!          Ven=Ven(:idl)//AtCenter(j)(:idla)
!          idl=idl+idla+1
!        enddo
!        if(idl.gt.idlp) then
!          call newln(1)
!          write(lst,FormA) Ven(:idel(Ven))
!        endif
!        if(n2-n1.gt.1) then
!          Ven='           to the atomic center of :'
!        else
!          Ven='                       to the atom :'
!        endif
!        idlp=idel(Ven)+1
!        idl =idlp
!        do j=n1+1,n2
!          idla=idel(AtCenter(j))
!          if(idla+idl.gt.127) then
!            call newln(1)
!            write(lst,FormA) Ven(:idel(Ven))
!            Ven=' '
!            idl=idlp
!          endif
!          Ven=Ven(:idl)//AtCenter(j)(:idla)
!          idl=idl+idla+1
!        enddo
!        if(idl.gt.idlp) then
!          call newln(1)
!          write(lst,FormA) Ven(:idel(Ven))
!        endif
!        call DistGetOneCenterAngle(XCenter0,SXCenter0,n1,n2,isw,
!     1                             XCenterP,SXCenterP,CDist,SCDist)
!        Ven=' '
!        call DistRoundESD(Cislo,CDist,SCDist,4)
!        write(Ven(idlp-1:),'(''= '',a)') Cislo(:idel(Cislo))
!        call newln(1)
!        write(lst,FormA) Ven(:idel(Ven))
!      else
        call OpenFile(78,fln(:ifln)//'.scr','unformatted','unknown')
!      endif
      if(NDimI(KPhase).le.0) then
        call newln(1)
        write(lst,FormA)
        go to 5000
      endif
      nx4(1:NDimI(KPhase))=1
      dx4(1:NDimI(KPhase))=0.
      t=0.
      CAngleMax=-9999.
      CAngleMin= 9999.
       CAngleAve=0.
      SCAngleAve=0.
      npom=0
      do kt=1,ntall(isw)
        call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
        do k=1,NDimI(KPhase)
          t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
        enddo
        do i=1,n3
          ia=IAtoms(i)
          call AddVek(t,tdif(1,i),xpp,NDimI(KPhase))
          call multm(GammaIntInv(1,i),xpp,xp,NDimI(KPhase),
     1               NDimI(KPhase),1)
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(oind,qcnt(1,ia),xp,nx4,dx4,a0(ia),
     1                      ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                      GammaIntInv(1,i))
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1        call MakeOccModSawTooth(oind,qcnt(1,ia),xp,nx4,dx4,
     2          uyi(1,KModA(2,ia),i),uyi(2,KModA(2,ia),i),KFA(2,ia),
     3          GammaIntInv(1,i))
          else
            call MakeOccModPol(oind,qcnt(1,ia),xp,nx4,dx4,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv(1,i),TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakePosMod(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i),uyi(1,1,i),KModA(2,ia),KFA(2,ia),
     2                      GammaIntInv(1,i))
          else
            call MakePosModPol(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                         uxi(1,1,i),uyi(1,1,i),KModA(2,ia),
     2                         ax(1,ia),a0(ia)*.5,GammaIntInv(1,i),
     3                         TypeModFun(ia))
          endif
          call AddVek(XCenter0(1,i),xpp,XCenter(1,i),3)
          call MakePosModS(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i), uyi(1,1,i),
     2                     suxi(1,1,i),suyi(1,1,i),KModA(2,ia),
     3                     KFA(2,ia),GammaIntInv(1,i))
          call AddVekQ(SXCenter0(1,i),xpp,SXCenter(1,i),3)
          if(oind(1).le.ocut) then
            oAll(kt)=0.
             CAngleAll(kt)=333.
            SCAngleAll(kt)=0.
            go to 2000
          endif
        enddo
        oAll(kt)=1.
        call DistGetOneCenterAngle(XCenter,SXCenter,n1,n2,n3,isw,
     1                             XCenterP,SXCenterP,CAngleAll(kt),
     2                             SCAngleAll(kt))
        if(CAngleAll(kt).gt.CAngleMax) then
           CAngleMax= CAngleAll(kt)
          SCAngleMax=SCAngleAll(kt)
        endif
        if(CAngleAll(kt).lt.CAngleMin) then
           CAngleMin= CAngleAll(kt)
          SCAngleMin=SCAngleAll(kt)
        endif
         CAngleAve= CAngleAve+ CAngleAll(kt)
        SCAngleAve=SCAngleAve+SCAngleAll(kt)
        npom=npom+1
2000  enddo
!      if(VolaToDist) then
!        Ven=' '
!        l=1
!        if(ieach.ne.0.and.npom.gt.0) then
!          call newln(1)
!          write(lst,'(''Individual values as a function of t:'')')
!          do kt=1,ntall(isw),ieach
!            call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
!            do i=1,NDimI(KPhase)
!              t(i)=tfirsti(i)+(nd(i)-1)*dti(i)
!            enddo
!            if(oAll(kt).le.0.) cycle
!            call DistRoundESD(Cislo,CDistAll(kt),SCDistAll(kt),4)
!            write(ven(l:),'(f6.3,1x,a)') t(1),Cislo(:idel(Cislo))
!            l=l+17
!            if(l.gt.111) then
!              call newln(1)
!              write(lst,FormA) Ven(:idel(Ven))
!              l=1
!            endif
!          enddo
!          if(l.gt.1) then
!            call newln(1)
!            write(lst,FormA) Ven(:idel(Ven))
!          endif
!        endif
!        if(npom.gt.0) then
!          pom=1./float(npom)
!           CDistAve= CDistAve*pom
!          SCDistAve=SCDistAve*pom
!          ven='Average : '
!          call DistRoundESD(Cislo,CDistAve,SCDistAve,4)
!          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
!          ven=ven(:idel(ven))//', Minimum : '
!          call DistRoundESD(Cislo,CDistMin,SCDistMin,4)
!          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
!          ven=ven(:idel(ven))//', Maximum : '
!          call DistRoundESD(Cislo,CDistMax,SCDistMax,4)
!          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
!        endif
!        call newln(1)
!        write(lst,FormA) Ven(:idel(Ven))
!        call newln(1)
!        write(lst,FormA)
!      else
        do kt=1,ntall(isw)
          write(78) kt,CAngleAll(kt),oAll(kt)
        enddo
!      endif
5000  if(allocated(IAtoms)) deallocate(IAtoms,XCenter0,SXCenter0,
     1                                 XCenter,SXCenter,uxi,suxi,uyi,
     2                                 suyi,GammaIntInv,tdif)
      if(allocated(oall)) deallocate(oall,CAngleAll,SCAngleAll)
      return
      end
