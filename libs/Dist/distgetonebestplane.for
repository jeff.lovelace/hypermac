      subroutine DistGetOneBestPlane(XPlane,SXPlane,n1,n2,iesd,isw,
     1                               abcd,sabcd,isabcd,chiq,dx,idx)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension XPlane(3,*),SXPlane(3,*),dx(*),idx(*),w(3),wg(3),
     1          abcd(4),sabcd(4),isabcd(4),wt(n1)
      logical iesd
      do i=1,3
        u(i)=xplane(i,1)-xplane(i,2)
        v(i)=xplane(i,3)-xplane(i,2)
      enddo
      call VecMul(u,v,wg)
      call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
      call VecNor(w,wg)
      do i=1,10
        i3=1
        if(abs(w(2)).gt.abs(w(1))) i3=2
        if(abs(w(3)).gt.abs(w(2)).and.abs(w(3)).gt.abs(w(1))) i3=3
        i1=mod(i3,3)+1
        i2=6-i1-i3
        avx=0.
        avy=0.
        avz=0.
        pom=0.
        do j=1,n1
          if(iesd) then
            pom1=0.
            do k=1,3
              pom1=pom1+(wg(k)*sxplane(k,j))**2
            enddo
            if(pom1.gt.0.) pom1=1./pom1
          else
            pom1=1.
          endif
          avx=avx+xplane(i1,j)*pom1
          avy=avy+xplane(i2,j)*pom1
          avz=avz+xplane(i3,j)*pom1
          pom=pom+pom1
          wt(j)=pom1
        enddo
        avx=avx/pom
        avy=avy/pom
        avz=avz/pom
        pom3=w(i1)/w(i3)
        pom4=w(i2)/w(i3)
        cx=0.
        cy=0.
        cz=0.
        p1=0.
        p2=0.
        do j=1,n1
          xxx=xplane(i1,j)-avx
          yyy=xplane(i2,j)-avy
          zzz=xplane(i3,j)-avz
          uu=xxx*wg(i1)+yyy*wg(i2)+zzz*wg(i3)
          pom=wt(j)
          pom1=xxx-pom3*zzz
          pom2=yyy-pom4*zzz
          cx=cx+pom1**2  *pom
          cy=cy+pom1*pom2*pom
          cz=cz+pom2**2  *pom
          p1=p1+pom1*uu  *pom
          p2=p2+pom2*uu  *pom
        enddo
        pom=1./(cx*cz-cy**2)
        cz= cz*pom
        cy=-cy*pom
        cx= cx*pom
        w(i1)=cz*p1+cy*p2
        w(i2)=cy*p1+cx*p2
        w(i3)=-pom3*w(i1)-pom4*w(i2)
        pom1=0.
        pom2=0.
        do j=1,3
          wg(j)=wg(j)-w(j)
          pom1=pom1+abs(w(j))
          pom2=pom2+abs(wg(j))
        enddo
        call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
        call vecnor(w,wg)
        if(pom1/pom2.le..00001) go to 1600
      enddo
1600  if(wg(1).lt.0.) call RealVectorToOpposite(wg,wg,3)
      call CopyVek(wg,abcd,3)
      call SetRealArrayTo(sabcd,3,0.)
      call SetIntArrayTo(isabcd,4,0)
      if(iesd) then
        isabcd(i1)=nint(sqrt(cz)*10000.)
        isabcd(i2)=nint(sqrt(cx)*10000.)
        pom3=w(i1)/w(i3)
        pom4=w(i2)/w(i3)
        pom=pom3**2*cz+pom4**2*cx+2.*pom3*pom4*cy
        isabcd(i3)=nint(sqrt(pom)*10000.)
        if(i1.eq.1) then
          sabcd(1)=cz
          sabcd(2)=cx
          sabcd(3)=cy*2.
        else if(i1.eq.2) then
          sabcd(1)=pom
          sabcd(2)=cz
          sabcd(3)=-2.*(pom3*cz+pom4*cy)
        else if(i1.eq.3) then
          sabcd(1)=cx
          sabcd(2)=pom
          sabcd(3)=-2.*(pom3*cy+pom4*cx)
        endif
      endif
      abcd(4)=-wg(i1)*avx-wg(i2)*avy-wg(i3)*avz
      chiq=0.
      do i=1,n2
        xxx=xplane(i1,i)-avx
        yyy=xplane(i2,i)-avy
        zzz=xplane(i3,i)-avz
        vv=wg(i1)*xxx+wg(i2)*yyy+wg(i3)*zzz
        dx(i)=vv
        idx(i)=0
        if(iesd) then
          pom1=xxx-pom3*zzz
          pom2=yyy-pom4*zzz
          pom=pom1**2*cz+pom2**2*cx+2.*pom1*pom2*cy
          do j=1,3
            pom=pom+(wg(j)*sxplane(j,i))**2
          enddo
          idx(i)=nint(10000.*sqrt(pom))
          if(i.le.n1) chiq=chiq+vv**2*wt(i)
        endif
      enddo
      if(iesd) then
        pom1=avx-pom3*avz
        pom2=avy-pom4*avz
        isabcd(4)=nint(sqrt(pom1**2*cz+pom2**2*cx+
     1                      2.*pom1*pom2*cy)*10000.)
      endif
      return
      end
