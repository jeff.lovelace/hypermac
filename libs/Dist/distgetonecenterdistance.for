      subroutine DistGetOneCenterDistance(XCenter,SXCenter,n1,n2,isw,
     1                               XCenterP,SXCenterP,CDist,SCDist)
      include 'fepc.cmn'
      include 'basic.cmn'
      real XCenter(3,*),SXCenter(3,*),XCenterP(3,2),SXCenterP(3,2),
     1    u(3),ug(3),sgx(6),ddx(6),ddg(3)
       XCenterP=0.
      SXCenterP=0.
      do i=1,2
        if(i.eq.1) then
          j1=1
          j2=n1
        else
          j1=n1+1
          j2=n2
        endif
        do j=j1,j2
           XCenterP(1:3,i)= XCenterP(1:3,i)+ XCenter(1:3,j)
          SXCenterP(1:3,i)=SXCenterP(1:3,i)+SXCenter(1:3,j)**2
        enddo
         XCenterP(1:3,i)=XCenterP(1:3,i)/float(j2-j1+1)
        SXCenterP(1:3,i)=sqrt(SXCenterP(1:3,i))/float(j2-j1+1)
      enddo
      sgx(1:3)=SXCenterP(1:3,1)
      sgx(4:6)=SXCenterP(1:3,2)
      u=XCenterP(1:3,2)-XCenterP(1:3,1)
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      CDist=sqrt(scalmul(u,ug))
      do l=1,3
        if(CDist.ne.0.) then
          ddx(l+3)=ug(l)/CDist
          ddg(l)=u(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      SCDist=sigma(sgx,ddx,6)
      gdu=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdu=gdu+MetTensS(m,isw,KPhase)*ddg(l)*ddg(k)
        enddo
      enddo
      if(CDist.gt.0.) then
        gdu=sqrt(gdu)/CDist
      else
        gdu=0.
      endif
      SCDist=sqrt(SCDist**2+gdu**2)
      return
      end
