      subroutine DistPlane
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*256 AtPlane,AtMore
      integer WhatHappened
      external DistPlaneReadCommand,DistPlaneWriteCommand,FeVoid
      save /PlaneQuest/
      xqd=400.
      il=4
      call RepeatCommandsProlog(id,fln(:ifln)//'_plane.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-2.*xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Atoms defining the '//
     1                    'best %plane','C',dpom,EdwYd,0)
      nEdwPlane=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      il=il+2
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'%Additional atoms '//
     1                    'for calculating distanes from the plane',
     2                    'C',dpom,EdwYd,0)
      nEdwMore=EdwLastMade
1300  AtPlane=' '
      AtMore =' '
1350  call FeQuestStringEdwOpen(nEdwPlane,AtPlane)
      call FeQuestStringEdwOpen(nEdwMore ,AtMore )
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistPlaneReadCommand,DistPlaneWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
