      subroutine DistOpenCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) return
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultDist
      NacetlReal(nCmddmhk)=-1.
      call NactiDist
      call DeleteFile(fln(:ifln)//'_torpl.tmp')
      do i=nCmdTorsion,nCmdCenter
        call MakeKeyWordFile(NactiKeywords(i)(:idel(NactiKeywords(i))),
     1                       'dist',NactiActive(i),NactiPassive(i))
      enddo
9999  return
      end
