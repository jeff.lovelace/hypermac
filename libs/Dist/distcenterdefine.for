      subroutine DistCenterDefine
      include 'fepc.cmn'
      include 'basic.cmn'
      common/CenterQuest/ nEdwCenter1,nEdwCenter2,AtCenter1,AtCenter2
      character*256 AtCenter1,AtCenter2
      integer WhatHappened
      external DistCenterReadCommand,DistCenterWriteCommand,FeVoid
      save /CenterQuest/
      xqd=400.
      il=4
      call RepeatCommandsProlog(id,fln(:ifln)//'_center.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-2.*xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Atoms defining the '//
     1                    'center#%1','C',dpom,EdwYd,0)
      nEdwCenter1=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      il=il+2
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Atoms defining the '//
     1                    'center#%2','C',dpom,EdwYd,0)
      nEdwCenter2=EdwLastMade
1300  AtCenter1=' '
      AtCenter2=' '
1350  call FeQuestStringEdwOpen(nEdwCenter1,AtCenter1)
      call FeQuestStringEdwOpen(nEdwCenter2,AtCenter2)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistCenterReadCommand,DistCenterWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
