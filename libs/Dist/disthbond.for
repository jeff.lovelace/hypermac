      subroutine DistHBond(u,ug,v,vg,sgx,MetTensP,du,sdu,
     1                     dv,sdv,dw,sdw,angle,sangle)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension u(3),ug(3),v(3),vg(3),sgx(9),ag(3,3),ddx(9),ddg(6),
     1          sgxp(6),w(3),wg(3)
      logical EqRvLT0
      real MetTensP(9)
      uu=scalmul(u,ug)
      du=sqrt(uu)
      do l=1,3
        if(du.ne.0.) then
          ddx(l+3)=ug(l)/du
          ddg(l)=u(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      sdu=sigma(sgx,ddx,6)
      if(sdu.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3)) then
        sdu=0.
        go to 1100
      endif
      gdu=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdu=gdu+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(du.gt.0.) then
        gdu=sqrt(gdu)/du
      else
        gdu=0.
      endif
      sdu=sqrt(sdu**2+gdu**2)
1100  vv=scalmul(v,vg)
      dv=sqrt(vv)
      do l=1,3
        if(dv.ne.0.) then
          ddx(l+3)=vg(l)/dv
          ddg(l)=v(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      call CopyVek(sgx,sgxp,3)
      call CopyVek(sgx(7),sgxp(4),3)
      sdv=sigma(sgxp,ddx,6)
      if(sdv.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(7),3)) then
        sdv=0.
        go to 1200
      endif
      gdv=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdv=gdv+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(dv.gt.0.) then
        gdv=sqrt(gdv)/dv
      else
        gdv=0.
      endif
      sdv=sqrt(sdv**2+gdv**2)
1200  do i=1,3
        w(i)=v(i)-u(i)
        wg(i)=vg(i)-ug(i)
      enddo
      ww=scalmul(w,wg)
      dw=sqrt(ww)
      do l=1,3
        if(dw.ne.0.) then
          ddx(l+3)=wg(l)/dw
          ddg(l)=w(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      call CopyVek(sgx(4),sgxp,3)
      call CopyVek(sgx(7),sgxp(4),3)
      sdw=sigma(sgxp,ddx,6)
      if(sdw.lt..00001.or.
     1   EqRvLT0(sgx(4),3).or.EqRvLT0(sgx(7),3)) then
       sdw=0.
       go to 1300
      endif
      gdw=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdw=gdw+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(dw.gt.0.) then
        gdw=sqrt(gdw)/dw
      else
        gdw=0.
      endif
      sdw=sqrt(sdw**2+gdw**2)
1300  angle=0.
      sangle=0.
      uv=scalmul(u,vg)
      denq=uu*vv
      den=sqrt(denq)
      if(abs(den).lt..0001) go to 9999
      arg=uv/den
      if(abs(arg).gt..999999) then
        angle=90.-sign(90.,arg)
        go to 9999
      endif
      if(abs(arg).lt..0001) then
        angle=90.
        go to 9999
      endif
      denp=1./(sqrt(1.-arg**2)*torad)
      pom=1./(den*(sqrt(1.-arg**2)*torad))
      angle=acos(arg)/torad
      do l=1,3
        dtul=pom*(vg(l)-ug(l)*uv/uu)
        dtvl=pom*(ug(l)-vg(l)*uv/vv)
        ddx(l)=-dtul-dtvl
        ddx(l+3)=dtul
        ddx(l+6)=dtvl
      enddo
      sangle=sigma(sgx,ddx,9)
      if(sangle.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.EqRvLT0(sgx(4),3)) then
        sangle=0.
        go to 9999
      endif
      do i=1,3
        do j=1,3
          ag(i,j)=1./denq*(u(i)*v(j)
     1                     -.5*uv/denq*(u(i)*u(j)*vv+v(i)*v(j)*uu))**2
        enddo
      enddo
      gu=0.
      m=0
      do i=1,3
        do j=1,3
          m=m+1
          gu=gu+ag(i,j)*MetTensP(m)
        enddo
      enddo
      gu=denp*sqrt(gu)
      sangle=sqrt(sangle**2+gu**2)
9999  return
      end
