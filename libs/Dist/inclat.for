      subroutine inclat
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xdsto(:,:),dxmo(:),BetaDstO(:,:)
      character*80 t80
      logical BratPrvniO(:),BratDruhyATretiO(:)
      allocatable xdsto,BratPrvniO,BratDruhyATretiO,dxmo,BetaDstO
      Nxdst=ubound(xdst,2)
      if(incl.eq.0) go to 9999
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      inclp=0
1000  if(inclp.ge.11) go to 1100
      read(m40,FormA,end=5000) t80
      if(t80(1:10).ne.'----------'.or.
     1   (LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0
     2     .and.
     3    LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0))
     4  go to 1000
      read(m40,'(i5)',end=5000) isw
      read(m40,FormA,end=5000) t80
      call mala(t80)
      if(index(t80,'max').gt.0.and.mod(inclp,10).eq.0) then
        inclp=inclp+1
        go to 1000
      else if(index(t80,'min').gt.0.and.inclp/10.eq.0) then
        inclp=inclp+10
        go to 1000
      endif
1100  if(inclp.eq.1) then
        incl=min(incl,1)
      else if(inclp.eq.10) then
        if(incl.eq.1) incl=0
      endif
      if(incl.eq.0) then
        close(m40)
        go to 9999
      endif
      if(incl.gt.0) then
        call newln(1)
        if(incl.eq.1) then
          write(lst,'(''Positive peaks from the previous Fourier '',
     1                ''calculation will be included'')')
        else if(incl.eq.2) then
          write(lst,'(''Negative peaks from the previous Fourier '',
     1                ''calculation will be included'')')
        else
          write(lst,'(''Both positive and negative peaks from the '',
     1                ''previous Fourier calculation will be included'')
     2                 ')
        endif
      else
        go to 9999
      endif
      l1=1
      l2=1
      if(incl.ge.2) then
        l2=2
        if(incl.eq.2) l1=2
      endif
      do 1500l=l1,l2
        rewind m40
        rewind m40
1300    read(m40,FormA,end=5000) t80
        if(t80(1:10).ne.'----------') go to 1300
        if(l.eq.1) then
          if(LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0)
     1      go to 1300
        else
          if(LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0)
     1      go to 1300
        endif
        read(m40,'(2i5)',end=1500) isw,KPh
        KPh=max(KPh,1)
        if(KPh.ne.KPhase) go to 1300
1400    if(NAtCalc.ge.MxAtAll) then
          NAtAll=max(NAtCalc,NAtAll)
          call ReallocateAtoms(NAtCalc)
        endif
        if(NAtCalc.ge.Nxdst) then
          allocate(xdsto(3,NAtCalc),BratPrvniO(NAtCalc),
     1             BratDruhyATretiO(NAtCalc),dxmo(NAtCalc),
     2             BetaDstO(6,NAtCalc))
          do i=1,NAtCalc
            BratPrvniO(i)=BratPrvni(i)
            BratDruhyATretiO(i)=BratDruhyATreti(i)
            dxmo(i)=dxm(i)
            call CopyVek(xdst(1,i),xdsto(1,i),3)
            call CopyVek(BetaDst(1,i),BetaDstO(1,i),6)
          enddo
          deallocate(xdst,BratPrvni,BratDruhyATreti,dxm,BetaDst)
          Nxdst=Nxdst+NAtCalc
          allocate(xdst(3,Nxdst),BetaDst(6,Nxdst),dxm(Nxdst),
     1             BratPrvni(Nxdst),BratDruhyATreti(Nxdst))
          do i=1,NAtCalc
            BratPrvni(i)=BratPrvniO(i)
            BratDruhyATreti(i)=BratDruhyATretiO(i)
            dxm(i)=dxmo(i)
            call CopyVek(xdsto(1,i),xdst(1,i),3)
            call CopyVek(BetaDstO(1,i),BetaDst(1,i),6)
          enddo
          deallocate(xdstO,BratPrvniO,BratDruhyATretiO,dxmO,BetaDstO)
        endif
        NAtCalc=NAtCalc+1
        read(m40,FormA,end=1460) t80
        if(index(t80,'--------').gt.0) go to 1460
        call SetBasicKeysForAtom(NAtCalc)
        read(t80,'(a8,2i3,4x,4f9.6,6x,3i1,3i3)')
     1    atom(NAtCalc),isf(NAtCalc),itf(NAtCalc),ai(NAtCalc),
     2    (xdst(k,NAtCalc),k=1,3),j,j,j,j,KModA(2,NAtCalc)
        read(m40,FormA,end=1460) t80
        if(index(t80,'---------').gt.0) go to 1460
        read(t80,101) pom
        iswa(NAtCalc)=isw
        kswa(NAtCalc)=KPh
        call CopyVek(xdst(1,NAtCalc),x(1,NAtCalc),3)
        if(MaxNDimI.gt.0) call qbyx(x(1,NAtCalc),qcnt(1,NAtCalc),isw)
        call SetRealArrayTo(beta(1,NAtCalc),6,0.)
        call SetRealArrayTo(BetaDst(1,NAtCalc),6,0.)
        BratPrvni(NAtCalc)=.true.
        BratDruhyATreti(NAtCalc)=.true.
        dxm(NAtCalc)=0.
        if(KModA(2,NAtCalc).ne.0) then
          do j=1,KModA(2,NAtCalc)
            read(m40,FormA,end=1460) t80
            if(index(t80,'--------').gt.0) go to 1460
            read(t80,101)(ux(k,j,NAtCalc),k=1,3),(uy(k,j,NAtCalc),k=1,3)
          enddo
          read(m40,FormA,end=1460) t80
          if(index(t80,'---------').gt.0) go to 1460
          read(t80,101) pom
        endif
        go to 1400
1460    NAtCalc=NAtCalc-1
1500  continue
5000  call CloseIfOpened(m40)
      call specat
9999  return
101   format(6f9.6)
      end
