      subroutine DistRewriteCommands(Klic)
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*80 t80
      call WriteKeys('dist')
      if(NDimI(KPhase).gt.0) then
        if(NDimI(KPhase).eq.1) then
          k=100
        else
          k=20
        endif
        do i=1,NDimI(KPhase)
          if(nt(i,1).ne.k) then
            write(t80,'(3i10)')(nt(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdnooft),
     1                              t80,k)
            go to 1130
          endif
        enddo
1130    do i=1,NDimI(KPhase)
          if(tfirst(i,1).ne.0.) then
            write(t80,100)(tfirst(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdtfirst),
     1                              t80,k)
            go to 1140
          endif
        enddo
1140    do i=1,NDimI(KPhase)
          if(tlast(i,1).ne.1.) then
            write(t80,100)(tlast(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdtlast),
     1                              t80,k)
            go to 1150
          endif
        enddo
      endif
1150  do i=1,NAtCalcBasic
        if(.not.BratPrvni(i).and.kswa(i).eq.KPhase) then
          t80='  '//IdDist(nCmdSelfirst)
          do j=1,NAtCalcBasic
            if(BratPrvni(j).and.kswa(j).eq.KPhase) then
              if(idel(t80).gt.70) then
                write(55,FormA) t80(:idel(t80))
                t80='  '//IdDist(nCmdSelfirst)
              endif
              t80=t80(:idel(t80))//' '//atom(j)
            endif
          enddo
          if(idel(t80).gt.idel(IdDist(nCmdSelfirst)))
     1      write(55,FormA) t80(:idel(t80))
          go to 1157
        endif
      enddo
1157  do i=1,NAtCalcBasic
        if(.not.BratDruhyATreti(i).and.kswa(i).eq.KPhase) then
          t80='  '//IdDist(nCmdSelsecnd)
          do j=1,NAtCalcBasic
            if(BratDruhyATreti(j).and.kswa(j).eq.KPhase) then
              if(idel(t80).gt.70) then
                write(55,FormA) t80(:idel(t80))
                t80='  '//IdDist(nCmdSelsecnd)
              endif
              t80=t80(:idel(t80))//' '//atom(j)
            endif
          enddo
          if(idel(t80).gt.idel(IdDist(nCmdSelsecnd)))
     1      write(55,FormA) t80(:idel(t80))
          go to 1170
        endif
      enddo
1170  do i=nCmdTorsion,nCmdCenter
        call MergeFileToM50(NactiKeywords(i))
      enddo
      write(55,'(''end dist'')')
      call DopisKeys(Klic)
      call RewriteTitle(' ')
      return
100   format(3f15.6)
      end
