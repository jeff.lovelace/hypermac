      subroutine DistPlaneWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*(*) Command
      character*256 EdwStringQuest,AtPlane,AtMore
      character*80 ErrString
      save /PlaneQuest/
      ich=0
      Command='plane'
      AtPlane=EdwStringQuest(nEdwPlane)
      call TestAtomString(AtPlane,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,ErrString)
      if(ErrString.ne.' ') go to 9000
      if(AtPlane.eq.' ') then
        ErrString='no atoms defined'
        go to 9000
      endif
      Command=Command(:idel(Command)+1)//AtPlane(:idel(AtPlane))
      AtMore=EdwStringQuest(nEdwMore)
      if(AtMore.ne.' ') then
        Command=Command(:idel(Command))//' | '//AtMore(:idel(AtMore))
        call TestAtomString(AtMore,IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
      endif
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      ich=1
9999  return
      end
