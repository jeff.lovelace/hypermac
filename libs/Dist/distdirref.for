      subroutine DistDirRef
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      character*256 StDirRef
      integer WhatHappened
      external DistDirRefReadCommand,DistDirRefWriteCommand,FeVoid
      save /DirRefQuest/
      xqd=400.
      il=3
      call RepeatCommandsProlog(id,fln(:ifln)//'_dirref.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-2.*xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Reference direction '//
     1                    'interpretation of best planes',
     1                    'C',dpom,EdwYd,0)
      nEdwDirRef=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
1300  StDirRef=' '
1350  call FeQuestStringEdwOpen(nEdwDirRef,StDirRef)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistDirRefReadCommand,DistDirRefWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
