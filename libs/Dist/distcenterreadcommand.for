      subroutine DistCenterReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/CenterQuest/ nEdwCenter1,nEdwCenter2,AtCenter1,AtCenter2
      character*256 AtCenter1,AtCenter2
      character*80  t80
      character*(*) Command
      save /CenterQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'center'.and.t80.ne.'!center') go to 8010
      if(k.ge.lenc) go to 8000
      i=index(Command,'|')
      if(i.le.0) then
        AtCenter1=Command(k+1:)
        AtCenter2=' '
      else
        AtCenter1=Command(k+1:i-1)
        AtCenter2=Command(i+1:)
      endif
      call TestAtomString(AtCenter1,IdWildNo,IdAtMolYes,IdMolNo,
     1                    IdSymmYes,IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      call TestAtomString(AtCenter2,IdWildNo,IdAtMolYes,IdMolNo,
     1                    IdSymmYes,IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
