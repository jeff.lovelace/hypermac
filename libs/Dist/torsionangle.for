      subroutine TorsionAngle(xtor,isw,Angle,SAngle)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension w(3),wg(3),daw(3),dbu(3),dbv(3),dcv(3),dcw(3),dtw(3),
     1          xtor(3,4),dtu(3),dtv(3)
      do i=1,3
        u(i)=xtor(i,2)-xtor(i,1)
        v(i)=xtor(i,3)-xtor(i,2)
        w(i)=xtor(i,4)-xtor(i,3)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call multm(MetTens(1,isw,KPhase),w,wg,3,3,1)
      uu=scalmul(u,ug)
      uv=scalmul(u,vg)
      uw=scalmul(u,wg)
      vv=scalmul(v,vg)
      vw=scalmul(v,wg)
      ww=scalmul(w,wg)
      pa=uv*vw-uw*vv
      pb=uu*vv-uv**2
      pc=vv*ww-vw**2
      do i=1,3
        dau(i)=vg(i)*vw-wg(i)*vv
        dav(i)=ug(i)*vw+wg(i)*uv-2.*vg(i)*uw
        daw(i)=vg(i)*uv-ug(i)*vv
        dbu(i)=2.*(ug(i)*vv-vg(i)*uv)
        dbv(i)=2.*(vg(i)*uu-ug(i)*uv)
        dcv(i)=2.*(vg(i)*ww-wg(i)*vw)
        dcw(i)=2.*(wg(i)*vv-vg(i)*vw)
      enddo
      pom1=1./sqrt(pb*pc)
      pom=pom1*pa
      if(abs(pom).gt..999999) then
        if(pom.gt.0) then
          Angle=0.
          pom= .999999
        else
          Angle=180.
          pom=-.999999
        endif
      else
        Angle=acos(pom)/torad
      endif
      pom2=-pom1/sqrt((1.+pom)*(1.-pom))
      pom3=pa*.5/pb
      pom4=pa*.5/pc
      do i=1,3
        dtu(i)=pom2*(dau(i)-pom3*dbu(i))
        dtv(i)=pom2*(dav(i)-pom3*dbv(i)-pom4*dcv(i))
        dtw(i)=pom2*(daw(i)-pom4*dcw(i))
      enddo
      do i=1,3
        ddx(i)=-dtu(i)
        ddx(i+3)=dtu(i)-dtv(i)
        ddx(i+6)=dtv(i)-dtw(i)
        ddx(i+9)=dtw(i)
      enddo
      SAngle=sigma(sgx,ddx,12)/torad
      if(u(1)*v(2)*w(3)+u(3)*v(1)*w(2)+u(2)*v(3)*w(1)-
     1   u(3)*v(2)*w(1)-u(1)*v(3)*w(2)-u(2)*v(1)*w(3).lt.0.)
     2   Angle=-Angle
      return
      end
