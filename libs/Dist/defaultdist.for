      subroutine DefaultDist
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      NactiInt=20
      NactiReal=20
      NactiComposed=20
      NactiKeys=NactiInt+NactiReal+NactiComposed
      if(NDimI(KPhase).le.1) then
        DefIntDist(nCmdieach)=10
      else
        DefIntDist(nCmdieach)=1
      endif
      call CopyVekI(DefIntDist,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntDist,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntDist,CmdIntDist,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealDist(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealDist(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealDist(i),CmdRealDist, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdDist(i)
      enddo
      RejectIdentical=1
      if(NDim(KPhase).eq.4) then
        j=100
      else
        j=20
      endif
      call SetIntArrayTo(nt,9,j)
      call SetRealArrayTo(tfirst,9,0.)
      call SetRealArrayTo(tlast,9,1.)
      nBondVal=0
      call SetIntArrayTo(iBondVal,400,0)
      call SetRealArrayTo(dBondVal,400,0.)
      call SetRealArrayTo(cBondVal,400,.37)
      iselPrvni=0
      iselDruhyATreti=0
      NDirRef=0
      return
      end
