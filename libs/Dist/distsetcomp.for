      subroutine DistSetComp(isw1,isw2)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension ZdT(9)
      call multm(zv(1,isw1,KPhase),zvi(1,isw2,KPhase),WPr,NDim(KPhase),
     1           NDim(KPhase),NDim(KPhase))
      call matinv(WPr,WPri,det,NDim(KPhase))
      i=NDim(KPhase)*3+1
      do j=1,3*NDimI(KPhase),3
        do l=0,2
          call CopyVek(WPr(i),Zd(j),3)
        enddo
        i=i+NDim(KPhase)
      enddo
      call TrMat(Zd,ZdT,3,NDimI(KPhase))
      call Multm(ZdT,MetTens(1,isw1,KPhase),GZd,NDimI(KPhase),3,3)
      call Multm(GZd,Zd,ZdGZd,NDimI(KPhase),3,NDimI(KPhase))
      do 1400l=1,NDimI(KPhase)
        pom=0.
        m=l
        do k=1,NDimI(KPhase)
          if(abs(ZdGZd(m)).gt..001) go to 1400
          m=m+NDimI(KPhase)
        enddo
        ZdGZd(l+(l-1)*NDimI(KPhase))=1.
1400  continue
      call MatInv(ZdGZd,ZdGZdI,pom,NDimI(KPhase))
      j=0
      do l=1,NDimI(KPhase)
        i=NDim(KPhase)*(l+2)+3
        do k=1,NDimI(KPhase)
          j=j+1
          i=i+1
          tnat(j)=WPr(i)
        enddo
      enddo
      call TrMat(qu(1,1,isw1,KPhase),TNaTI,3,NDimI(KPhase))
      call RealVectorToOpposite(TNaTI,TNaTI,3*NDimI(KPhase))
      call cultm(TNaTI,Zd,TNaT,NDimI(KPhase),3,NDimI(KPhase))
      call matinv(TNaT,TNaTi,pom,NDimI(KPhase))
      return
      end
