      subroutine DistDihedralAngle(x,y,sx,sy,isw,du,sdu)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(3),y(3),sx(3),sy(3),u(3),v(3)
      call multm(MetTensI(1,isw,KPhase),x,u,3,3,1)
      call multm(MetTensI(1,isw,KPhase),y,v,3,3,1)
      pom=scalmul(y,u)
      if(pom.ge. .999999) then
        du=0.
        sdu=0.
      else if(pom.le.-.999999) then
        du=180.
        sdu=0.
      else
        du=acos(pom)/torad
        if(u(3).ne.0.) then
          pom1=v(1)-u(1)/u(3)*v(3)
          pom2=v(2)-u(2)/u(3)*v(3)
        else
          pom1=0.
          pom2=0.
        endif
        if(v(3).ne.0.) then
          pom3=u(1)-v(1)/v(3)*u(3)
          pom4=u(2)-v(2)/v(3)*u(3)
        else
          pom3=0.
          pom4=0.
        endif
        sdu=sqrt((sx(1)*pom1**2+sx(2)*pom2**2+sx(3)*pom1*pom2+
     1            sy(1)*pom3**2+sy(2)*pom4**2+sy(3)*pom3*pom4)/
     2            (1.-pom**2))/torad
      endif
      if(du.gt.90.) then
        du=180.-du
        pom=-pom
      endif
      return
      end
