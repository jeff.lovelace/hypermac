      subroutine DistSetCommands
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      real, allocatable :: sngc(:,:,:),csgc(:,:,:)
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*80 TmpFile
      external DistSetCommandsWatch,FeVoid
      data LastListek/1/
      save /DistSelAtoms/
      call RewriteTitle('Dist-commands')
      Klic=0
      call ComSym(0,0,ich)
      if(ngc(KPhase).gt.0) then
        allocate(sngc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc),
     1           csgc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc))
        call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),
     1              NAtCalc)
        deallocate(sngc,csgc)
        if(ErrFlag.ne.0) go to 9999
      endif
      n=NAtCalc+5
      allocate(xdst(3,n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call SetLogicalArrayTo(BratPrvni,n,.true.)
      call SetLogicalArrayTo(BratDruhyATreti,n,.true.)
      go to 1000
      entry DistSetCommandsFromGrapht
      Klic=1
1000  call DistOpenCommands
      if(ErrFlag.ne.0) go to 9999
      TmpFile='jlst'
      call CreateTmpFile(TmpFile,i,0)
      call FeTmpFilesAdd(TmpFile)
      call OpenFile(lst,TmpFile,'formatted','unknown')
      call CopyVek(x,xdst,3*NAtCalc)
      call CopyVek(beta,BetaDst,6*NAtCalc)
      call SetRealArrayTo(dxm,NAtCalc,0.)
      call pisat
      call CloseListing
      call DeleteFile(TmpFile)
      call FeTmpFilesClear(TmpFile)
      if(Klic.eq.1) then
        call DistBondval
        go to 5000
      endif
      XdQuestDist=550.
      call FeKartCreate(-1.,-1.,XdQuestDist,18,'Distance commands',0,0)
      call FeCreateListek('Basic',1)
      KartIdBasic=KartLastId
      call DistBasicCommandsMake(KartIdBasic)
      call FeCreateListek('Select atoms',1)
      KartIdSelect=KartLastId
      call DistSelectAtomsMake(KartIdSelect)
      call FeCreateListek('Modulation',1)
      KartIdModulation=KartLastId
      call DistModulationCommandsMake(KartIdModulation)
      call FeCompleteKart(LastListek)
3000  call FeQuestEventWithCheck(KartId,ich,DistSetCommandsWatch,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsUpdate
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsUpdate
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsCheck
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsCheck
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsUpdate
          LastListek=1
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsUpdate
          LastListek=2
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsUpdate
          LastListek=3
        endif
      endif
      call FeDestroyKart
      if(ich.ne.0) go to 9999
5000  call DistRewriteCommands(Klic)
9999  if(Klic.eq.0)
     1  deallocate(xdst,BetaDst,dxm,BratPrvni,BratDruhyATreti,
     2             aselPrvni,aselDruhyATreti)
      call DeleteFile(PreviousM40)
      if(allocated(sngc)) deallocate(sngc,csgc)
      return
100   format(3f15.6)
      end
