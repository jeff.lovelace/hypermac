      subroutine DistBondvalWriteCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      character*(*) Command
      character*80  ErrString
      character*2   AtNames
      integer RolMenuSelectedQuest
      save /BondvalQuest/
      ich=0
      Command='bondval'
      nRolMenu=nRolMenuFirst
      do i=1,2
        j=RolMenuSelectedQuest(nRolMenu)
        if(j.le.0) go to 9100
        AtNames(i)=AtType(j,KPhase)
        call Uprat(AtNames(i))
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nRolMenu=nRolMenu+1
      enddo
      call FeQuestRealFromEdw(nEdwRho,BVRho)
      write(Cislo,100) BVRho
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwB,BVB)
      write(Cislo,100) BVB
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
100   format(f15.4)
      end
