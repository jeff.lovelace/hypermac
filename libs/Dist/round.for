      subroutine round(x,sx,n)
      include 'fepc.cmn'
      if(sx.eq.0.) go to 9999
      if(n) 1000,9999,1000
 1000 f=1.
 1500 isx=sx*f+.05
      if(isx) 9999,1800,3000
 1800 f=f*10.
      go to 1500
 3000 sx=anint(sx*f)/f
      x=anint(x*f)/f
 9999 return
      end
