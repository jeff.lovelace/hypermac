      subroutine UhelAt(i,j)
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension z0(6),z1(6),z2(3),vz(3),shk(3),shkc(3),vm(3),vmg(3),
     1          uxk(3,mxw),uyk(3,mxw),zp(6),sx0(6),z0p(6),z1p(6),z2p(3),
     2          vzp(3),vp(3),MxCell(3),z340(3),zpp(6),z3(6),z3p(6),
     3          ksh(3),tztnat(3),dttnat(3),GammaIntInv(9),t(3),tj(3),
     4          nd(3),tsp(3),xp(6),suxk(3,mxw),suyk(3,mxw),sgxm(9),
     5          umg(3),umod(6),smp(36),rmp(9),rm6p(36),s6p(6),vt6p(:,:),
     6          MxDCell(3),JCell(3)
      character*128 StHBonds
      character*80 t80,s80
      character*20 AtH(3)
      character*15 ValH(4),t15
      character*8  sp8,At3
      character*2  sp2
      logical eqrv,EqRvLT0
      allocatable vt6p
      equivalence (umod(1),damav),(umod(2),sdamav),
     1            (umod(3),dammn),(umod(4),sdammn),
     2            (umod(5),dammx),(umod(6),sdammx)
      data sp2,sp8/2*' '/
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
      if(CalcHBonds) then
        kp=1
      else
        kp=j
      endif
      do 4000k=kp,NAtCalc
        isfk=isf(k)
        iswk=iswa(k)
        if(kswa(k).ne.KPhase.or..not.BratDruhyATreti(k).or.isfk.eq.0.or.
     1     (CalcHBonds.and.isfk.eq.isfh(KPhase))) cycle
        if(SkipSplit.gt.0) then
          if(kmol(i).ne.kmol(k)) then
            do ii=1,NMolSplit
              kk=0
              do jj=1,MMolSplit(ii)
                if(IMolSplit(jj,ii).eq.kmol(i)) then
                  kk=kk+1
                else if(IMolSplit(jj,ii).eq.kmol(k)) then
                  kk=kk+1
                endif
              enddo
              if(kk.ge.2) go to 4000
            enddo
          endif
          if(kmol(i).ne.kmol(k)) then
            do ii=1,NMolSplit
              kk=0
              do jj=1,MMolSplit(ii)
                if(IMolSplit(jj,ii).eq.kmol(i)) then
                  kk=kk+1
                else if(IMolSplit(jj,ii).eq.kmol(k)) then
                  kk=kk+1
                endif
              enddo
              if(kk.ge.2) go to 4000
            enddo
          endif
          do ii=1,NAtSplit
            kk=0
            do jj=1,MAtSplit(ii)
              if(IAtSplit(jj,ii).eq.i) then
                kk=kk+1
              else if(IAtSplit(jj,ii).eq.k) then
                kk=kk+1
              endif
            enddo
            if(kk.ge.2) go to 4000
          enddo
          do ii=1,NAtSplit
            kk=0
            do jj=1,MAtSplit(ii)
              if(IAtSplit(jj,ii).eq.j) then
                kk=kk+1
              else if(IAtSplit(jj,ii).eq.k) then
                kk=kk+1
              endif
            enddo
            if(kk.ge.2) go to 4000
          enddo
        endif
        call SetRealArrayTo(xp,3,0.)
        kk=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,k).ne.0) then
            xp(1)=ax(KModA(1,k),k)
            kk=1
          else if(KFA(2,k).ne.0) then
            xp(1)=uy(1,KModA(2,k),k)
            kk=1
          endif
        endif
        call SpecPos(x(1,j),xp,kk,iswk,.01,n)
        aik=ai(k)*float(n)
        if(CalcHBonds) then
          dmhik =HAMax+dxm(i)+dxm(k)
          dmhikm=HAMax
        else
          if(dmhk.ge.0) then
            pom=1.
          else
            pom=1.+.01*ExpDMax
          endif
          dmhikm=TypicalDistUse(isf(i),isf(k))*pom
          dmhik =dmhikm+dxm(i)+dxm(k)
        endif
        dmhika=dmhik*ah
        dmhikb=dmhik*bh
        dmhikc=dmhik*ch
        dmhikq=dmhik**2
        dmdikq=dmdi**2
        do l=1,3
          MxCell(l)=dmhik*rcp(l,iswk,KPhase)+1
        enddo
        MxNCell=1
        MxDCell(3)=1
        do l=2,1,-1
          MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
        enddo
        MxNCell=MxDCell(1)*(2*MxCell(1)+1)
        do l=1,3
          z0(l)=xdst(l,k)
          z0p(l)=x(l,k)
          sx0(l)=sx(l,k)
        enddo
        ks=0
        kst=0
        do ksym=1,NSymmN(KPhase)
          ks=ks+1
          if(isa(ks,k).le.0) cycle
          iswk=ISwSymm(ksym,iswa(k),KPhase)
          iswkin=iswi*10+iswk
          if(iswk.ne.iswi) then
            if(iswkin.ne.iswlast) call DistSetComp(iswi,iswk)
          else
            call UnitMat(TNaT ,NDimI(KPhase))
            call UnitMat(TNaTi,NDimI(KPhase))
          endif
          iswlast=iswkin
          call multm(TNaTi,tfirsti,TzTNaT,NDimI(KPhase),NDimI(KPhase),
     1               1)
          call multm(TNaTi,dti,DtTNaT,NDimI(KPhase),NDimI(KPhase),1)
          if(iswk.eq.iswa(k)) then
            call CopyMat(rm (1,ksym,iswk,KPhase),rmp ,3)
            call CopyMat(rm6(1,ksym,iswk,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,ksym,iswk,KPhase), s6p,NDim(KPhase))
            do l=1,NLattVec(KPhase)
              call CopyVek(vt6(1,l,iswk,KPhase),vt6p(1,l),NDim(KPhase))
            enddo
          else
            call multm(zv(1,iswk,KPhase),zvi(1,iswa(k),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,ksym,iswa(k),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rm6p,rmp,NDim(KPhase))
            call multm(smp,s6(1,ksym,iswa(k),KPhase),s6p,NDim(KPhase),
     1                 NDim(KPhase),1)
            do l=1,NLattVec(KPhase)
              call multm(smp,vt6(1,l,iswa(k),KPhase),vt6p(1,l),
     1                   NDim(KPhase),NDim(KPhase),1)
            enddo
          endif
          call multm(rmp,z0,z1,3,3,1)
          call multm(rmp,z0p,z1p,3,3,1)
          if(EqRvLT0(sx0,3)) then
            call SetRealArrayTo(sgx(7),3,-.1)
          else
            call multmq(rmp,sx0,sgx(7),3,3,1)
          endif
          call GetGammaIntInv(RM6p,GammaIntInv)
          do l=1,3
            z1(l)=z1(l)+s6p(l)
            z1p(l)=z1p(l)+s6p(l)
          enddo
          do 7100kcenter=1,NLattVec(KPhase)
            do l=1,kcenter-1
              if(eqrv(vt6p(1,kcenter),vt6p(1,l),3,.001)) go to 7100
            enddo
            do l=4,NDim(KPhase)
              tsp(l-3)=-s6p(l)-vt6p(l,kcenter)
            enddo
            do l=1,3
              z2(l)=z1(l)+vt6p(l,kcenter)
              z2p(l)=z1p(l)+vt6p(l,kcenter)
            enddo
            if(iswk.eq.iswi) then
              call bunka(z2,shk,1)
              do l=1,3
                vzp(l)=z2p(l)-x1p(l)+shk(l)
                vz(l)=z2(l)-x1(l)
              enddo
              if(NComp(KPhase).gt.1) then
                do l=1,3
                  MxCell(l)=dmhik*rcp(l,iswa(j),KPhase)+1
                enddo
                MxDCell(3)=1
                do l=2,1,-1
                  MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                enddo
                MxNCell=MxDCell(1)*(2*MxCell(1)+1)
              endif
            else
              call multm(WPri,x1,zp,NDim(KPhase),NDim(KPhase),1)
              do l=1,3
                shk(l)=zp(l)-z2(l)
              enddo
              do l=1,3
                shk(l)=ifix(shk(l))
                z2(l)=z2(l)+shk(l)
                z2p(l)=z2p(l)+shk(l)
              enddo
              do l=1,NDim(KPhase)
                xp(l)=x1(l)
                if(l.gt.3) xp(l)=xp(l)+tfirsti(l-3)
              enddo
              do l=1,3
                pom=abs(zp(l))
                if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
              enddo
              do l=4,NDim(KPhase)
                xp(l)=xp(l)+dti(l-3)*float(nti(l-3)-1)
              enddo
              call multm(WPri,xp,zp,NDim(KPhase),NDim(KPhase),1)
              do l=1,3
                pom=abs(zp(l))
                if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
              enddo
              MxDCell(3)=1
              do l=2,1,-1
                MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
              enddo
              MxNCell=MxDCell(1)*(2*MxCell(1)+1)
            endif
            do JCellI=0,MxNCell-1
              call UnPackHKL(JCellI,JCell,MxCell,MxDCell,3)
              do l=1,3
                pom=JCell(l)
                if(iswj.eq.iswi) then
                  v(l)=vz(l)+pom
                  vp(l)=vzp(l)+pom
                endif
                z3(l)=z2(l)+pom
                z3p(l)=z2p(l)+pom
                ksh(l)=nint(shk(l))+pom
                shkc(l)=shk(l)+pom+s6p(l)+vt6p(l,kcenter)
              enddo
              kst=kst+1
              if(k.eq.j.and.kst.le.jst) cycle
              if(iswk.eq.iswi) then
                call multm(MetTens(1,iswi,KPhase),v,vg,3,3,1)
                vv=scalmul(v,vg)
                if(vv.gt.dmhikq.or.vv.lt.dmdikq.or.
     1             (vv.eq.0..and.i.eq.k)) cycle
                dv=sqrt(vv)
              else
                call DistCheckComp(iswj,z3,dv,dmhikq,ich)
                if(ich.ne.0) cycle
              endif
              call SCode(ksym,kcenter,shkc,ksh,iswk,t80,SymmLabel(3))
              t80=t80(:idel(t80))//'#'
              l=idel(SymmLabel(3))
              if(l.gt.0) t80=t80(:idel(t80))//SymmLabel(3)(:l)
              call ScodeCIF(ksym,kcenter,ksh,iswk,SymmCIF(3))
              if(CalcHBonds) then
                do l=1,3
                  if(l.eq.1) then
                    ii=j
                    ll=2
                  else if(l.eq.2) then
                    ii=i
                    ll=1
                  else if(l.eq.3) then
                    ii=k
                    ll=3
                  endif
                  AtH(l)=Atom(ii)
                  if(SymmLabel(ll).ne.' ')
     1              AtH(l)=AtH(l)(:idel(AtH(l)))//'#'//
     1                     SymmLabel(ll)(:idel(SymmLabel(ll)))
                enddo
                call DistHBond(u,ug,v,vg,sgx,MetTensS(1,iswi,KPhase),
     1                         du,sdu,dv,sdv,dw,sdw,angle,sangle)
                if(Angle.lt.DHAMin) cycle
                if(IHBonds.le.0) then
                  call NewLn(2)
                  write(lst,'(''Donor'',12x,''Hydrogen'',12x,
     1                        ''Acceptor'',12x,''D-H distance     '',
     2                        ''H...A distance   D-A distance     '',
     3                        ''A-H...D angle''/)')
                  if(NDimI(KPhase).le.0) then
                    write(LnHBonds,'(''loop_'')')
                    do l=1,nCIFHBonds
                      write(LnHBonds,FormCIF)
     1                  ' '//CifKey(CIFHBonds(l),12)
                    enddo
                  endif
                endif
                IHBonds=IHBonds+1
                call DistRoundESD(ValH(1),du,sdu,2)
                call DistRoundESD(ValH(2),dv,sdv,2)
                call DistRoundESD(ValH(3),dw,sdw,4)
                call DistRoundESD(ValH(4),Angle,SAngle,2)
                write(StHBonds,'(a18,2a20,2x,4a15)') AtH,ValH
                if(NDimI(KPhase).le.0) then
                  t80=Atom(j)(:idel(Atom(j)))//' '//
     1                Atom(i)(:idel(Atom(i)))//' '//
     2                Atom(k)(:idel(Atom(k)))//' '
                  do l=1,3
                    if(l.eq.1) then
                      ll=2
                    else if(l.eq.2) then
                      ll=1
                    else if(l.eq.3) then
                      ll=3
                    endif
                    t80=t80(:idel(t80))//' '//
     1                  SymmCIF(ll)(:idel(SymmCIF(ll)))
                  enddo
                  do l=1,4
                    t80=t80(:idel(t80))//'   '//ValH(l)(:idel(ValH(l)))
                  enddo
                  t80=t80(:idel(t80))//' ?'
                  write(LnHBonds,FormA) '  '//t80(:idel(t80))
                endif
                if(NDimI(KPhase).le.0) then
                  call NewLn(1)
                  write(lst,FormA) StHBonds(:idel(StHBonds))
                endif
              else
                call DistAng(u,ug,v,vg,uu,vv,sgx,
     1                       MetTensS(1,iswj,KPhase),angle,su)
                if(iswi.eq.iswj.and.iswi.eq.iswk) then
                  if(su.lt..0001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.
     2               EqRvLT0(sgx(7),3)) su=0.
                else
                  su=0.
                endif
                if((NDimI(KPhase).le.0.or.MaxUsedKw(KPhase).eq.0).and.
     1              VolaToDist.and.LstType.eq.1)
     2             call DistM61AngleHeader(Atom(j),Atom(i),Atom(k),
     3                                     angle,su,t80)
                if(VolaToDist) then
                  call DistModifyAtName(k,At3)
                else
                  At3=Atom(k)
                endif
                write(78) '#3',At3,isf(k),iswk,aik,angle,su,
     1                    (0.,m=1,4),t80,SymmCIF(3),1
                if(VolaToDist.and.LstType.eq.0) then
                  call fflush
                  line=line+1
                  call DistRoundESD(t15,Angle,su,2)
                  iven(line)='  '//atom(k)//'...'//t15(:idel(t15))
                  do l=3,10
                    if(iven(line)(l:l).eq.' ') iven(line)(l:l)='.'
                  enddo
                  call fflush
                endif
              endif
              if(nt(1,1).le.0.or.NDimI(KPhase).le.0) cycle
              call qbyx(z3,z3(4),iswk)
              call qbyx(z3p,z3p(4),iswk)
              do m=1,KModA(2,k)
                call multm(rmp,ux(1,m,k),uxk(1,m),3,3,1)
                call multmq(rmp,sux(1,m,k),suxk(1,m),3,3,1)
                if(KFA(2,k).eq.0.or.m.ne.KModA(2,k)) then
                  call multm(rmp,uy(1,m,k),uyk(1,m),3,3,1)
                  call multmq(rmp,sux(1,m,k),suyk(1,m),3,3,1)
                else
                  call CopyVek( uy(1,m,k), uyk(1,m),3)
                  call CopyVek(suy(1,m,k),suyk(1,m),3)
                endif
              enddo
              call qbyx(shkc,qx,iswk)
              call AddVek(qx,tsp,xp,NDimI(KPhase))
              call AddVek(xp,tztnat,qx,NDimI(KPhase))
              call multm(GammaIntInv,qx,xp,NDimI(KPhase),NDimI(KPhase),
     1                   1)
              if(TypeModFun(k).le.1.or.KModA(1,k).le.1) then
                call MakeOccMod(ok,qcnt(1,k),xp,nti,dttnat,a0(k),
     1            ax(1,k),ay(1,k),KModA(1,k),KFA(1,k),GammaIntInv)
                if(KFA(2,k).gt.0.and.KModA(2,k).gt.0)
     1            call MakeOccModSawTooth(ok,qcnt(1,k),xp,nti,dttnat,
     2              uyk(1,KModA(2,k)),uyk(2,KModA(2,k)),KFA(2,k),
     3              GammaIntInv)
              else
                call MakeOccModPol(ok,qcnt(1,k),xp,nti,dttnat,
     1            ax(1,k),ay(1,k),KModA(1,k)-1,
     2            ax(KModA(1,k),k),a0(k)*.5,GammaIntInv,
     3             TypeModFun(k))
              endif
              if(TypeModFun(k).le.1) then
                call MakePosMod(xdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                          KModA(2,k),KFA(2,k),GammaIntInv)
              else
                call MakePosModPol(xdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                            KModA(2,k),ax(1,k),a0(k)*.5,
     2                            GammaIntInv,TypeModFun(k))
              endif
              call MakePosModS(sxdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                         suxk,suyk,KModA(2,k),KFA(2,k),
     2                         GammaIntInv)
              dammx=-9999.
              dammn= 9999.
              dhmmx=-9999.
              dhmmn= 9999.
              damav=0.
              sdamav=0.
              ddmmn= 9999.
              call CopyVek(z3p(4),z340,NDimI(KPhase))
              npom=0
              do kt=1,ntall(iswi)
                call RecUnpack(kt,nd,nti,NDimI(KPhase))
                do l=1,NDimI(KPhase)
                  t(l)=tfirsti(l)+(nd(l)-1)*dti(l)
                enddo
                if(oi(kt).le.ocut.or.oj(kt).le.ocut.or.ok(kt).le.ocut)
     1            cycle
                npom=npom+1
                if(iswk.eq.iswi) then
                  do l=1,3
                    vm(l)=vp(l)+xdk(l,kt)-xdi(l,kt)
                  enddo
                else
                  call multm(TNaTI,t,tj,NDimI(KPhase),NDimI(KPhase),1)
                  do l=4,NDim(KPhase)
                    z3p(l)=z340(l-3)+tj(l-3)
                  enddo
                  call multm(WPr,z3p,zp,NDim(KPhase),NDim(KPhase),1)
                  do l=1,3
                    zp(l)=zp(l)-x1p(l)
                  enddo
                  call multm(MetTens(1,iswi,KPhase),zp,zpp,3,3,1)
                  dumm(kt)=sqrt(scalmul(zp,zpp))
                  do l=1,3
                    zp(l)=zp(l)-xdi(l,kt)
                    zpp(l)=xdj(l,kt)
                  enddo
                  call qbyx(zpp,zpp(4),iswj)
                  call cultm(WPr,zpp,zp,NDim(KPhase),NDim(KPhase),1)
                  call CopyVek(zp,vm,3)
                endif
                call multm(MetTens(1,iswi,KPhase),vm,vmg,3,3,1)
                call multm(MetTens(1,iswi,KPhase),um(1,kt),umg,3,3,1)
                if(EqRvLT0(sgx,3)) then
                  call CopyVek(sxdi(1,kt),sgxm,3)
                else
                  call AddVekQ(sgx,sxdi(1,kt),sgxm,3)
                endif
                if(EqRvLT0(sgx(4),3)) then
                  call CopyVek(sxdj(1,kt),sgxm(4),3)
                else
                  call AddVekQ(sgx(4),sxdj(1,kt),sgxm(4),3)
                endif
                if(EqRvLT0(sgx(7),3)) then
                  call CopyVek(sxdk(1,kt),sgxm(7),3)
                else
                  call AddVekQ(sgx(7),sxdk(1,kt),sgxm(7),3)
                endif
                if(CalcHBonds) then
                  call DistHBond(um(1,kt),umg,vm,vmg,sgxm,
     1              MetTensS(1,iswi,KPhase),dhm(1,kt),dhms(1,kt),
     2              dhm(2,kt),dhms(2,kt),dhm(3,kt),dhms(3,kt),dhm(4,kt),
     3              dhms(4,kt))
                  if(dhm(1,kt).gt.dammx) then
                     dammx=dhm(1,kt)
                    sdammx=dhms(1,kt)
                  endif
                  if(dhm(1,kt).lt.dammn) then
                     dammn=dhm(1,kt)
                    sdammn=dhms(1,kt)
                  endif
                  if(dhm(2,kt).gt.dhmmx) then
                     dhmmx=dhm(2,kt)
                    sdhmmx=dhms(2,kt)
                  endif
                  if(dhm(2,kt).lt.dhmmn) then
                     dhmmn=dhm(2,kt)
                    sdhmmn=dhms(2,kt)
                  endif
                else
                  vv=scalmul(vm,vmg)
                  if(vv.lt.ddmmn) ddmmn=vv
                  call DistAng(um(1,kt),umg,vm,vmg,dum(kt)**2,vv,sgxm,
     1                         MetTensS(1,iswj,KPhase),pom,spom)
                  if(spom.lt..0001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.
     2               EqRvLT0(sgx(7),3)) spom=0.
                  dam (kt)= pom
                  dams(kt)=spom
                  if(pom.gt.dammx) then
                     dammx= pom
                    sdammx=spom
                  endif
                  if(pom.lt.dammn) then
                     dammn= pom
                    sdammn=spom
                  endif
                   damav= damav+ pom
                  sdamav=sdamav+spom
                  if(VolaToDist) then
                    call DistModifyAtName(k,At3)
                  else
                    At3=Atom(k)
                  endif
                  write(78) sp2,At3,kt,kt,0.,pom,spom,oi(kt),
     1                      oj(kt),ok(kt),0.,t80,SymmCIF(3),1
                endif
              enddo
              if(CalcHBonds) then
                if(npom.le.0.or.dammn.gt.DHMax.or.
     1             dhmmn.gt.HAMax) then
                  cycle
                else
                  call NewLn(1)
                  write(lst,FormA) StHBonds(:idel(StHBonds))
                endif
              else
                if(npom.gt.0.and.sqrt(ddmmn).le.dmhikm) then
                  if(VolaToDist.and.LstType.eq.1)
     1              call DistM61AngleHeader(Atom(j),Atom(i),Atom(k),
     2                                      angle,su,t80)
                  if(iswi.eq.iswk) then
                    pom=1./float(npom)
                     damav= damav*pom
                    sdamav=sdamav*pom
                    if(VolaToDist) then
                      if(LstType.eq.0) then
                        call fflush
                        line=line+1
                      endif
                      call DistRoundESD(t15,damav,sdamav,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  ave        '//t15(:l)
                        call fflush
                        line=line+1
                      else if(LstType.eq.1) then
                        s80='       ave : '//t15(:l)
                      endif
                      call DistRoundESD(t15,dammn,sdammn,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  min        '//t15(:l)
                        call fflush
                        line=line+1
                      else if(LstType.eq.1) then
                        s80=s80(:idel(s80))//'  min : '//t15(:l)
                      endif
                      call DistRoundESD(t15,dammx,sdammx,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  max        '//t15(:l)
                        call fflush
                      else if(LstType.eq.1) then
                        s80=s80(:idel(s80))//'  max : '//t15(:l)
                      endif
                    endif
                    if(.not.CalcHBonds) then
                      if(VolaToDist) then
                        call DistModifyAtName(k,At3)
                      else
                        At3=Atom(k)
                      endif
                      write(78) '#5',At3,isf(k),iswk,aik,umod,
     1                          t80,SymmCIF(3),1
                      if(VolaToDist.and.LstType.eq.1) then
                        call newln(1)
                        write(lst,FormA) s80(:idel(s80))
                      endif
                    endif
                  endif
                  if(LstType.eq.0.and.VolaToDist) call fflush
                else
                  if(LstType.eq.0) line=max(line-1,0)
                  cycle
                endif
              endif
              if(.not.VolaToDist) cycle
              if(ieach.ne.0) then
                l=0
                do kt=1,ntall(iswi),ieach
                  call RecUnpack(kt,nd,nti,NDimI(KPhase))
                  do m=1,NDimI(KPhase)
                    t(m)=tfirsti(m)+(nd(m)-1)*dti(m)
                  enddo
                  l=l+1
                  if(oi(kt).gt.ocut.and.oj(kt).gt.ocut.and.
     1               ok(kt).gt.ocut) then
                    if(LstType.eq.0) then
                       pom=dam(kt)
                      spom=dams(kt)
                      call fflush
                      line=line+1
                      write(iven(line),'(3x,f5.3)') t(1)
                      call DistRoundESD(t15,pom,spom,2)
                      iven(line)=iven(line)(:13)//
     1                  t15(:idel(t15))
                      if(oi(kt).le.oind.or.oj(kt).le.oind
     1                                 .or.ok(kt).le.oind)
     2                  iven(line)(2:2)='*'
                      call fflush
                    else if(LstType.eq.1) then

                    else if(LstType.eq.99) then
                      call NewLn(1)
                      do ii=1,4
                        call DistRoundESD(ValH(ii),
     1                                    dhm(ii,kt),dhms(ii,kt),2)
                      enddo
                      write(Cislo,'(''t='',f9.3)') t(1)
                      call Zhusti(Cislo)
                      write(lst,'(45x,5a15)') Cislo,ValH
                    endif
                  endif
                enddo
                if(LstType.eq.99) then
                  call NewLn(1)
                  write(lst,FormA)
                endif
              endif
              if(LstType.eq.0) then
                call fflush
                line=line+1
                write(iven(line),'(26(''-''))')
                call fflush
              endif
            enddo
7100      continue
        enddo
4000  enddo
9999  if(allocated(vt6p)) deallocate(vt6p)
      return
      end
