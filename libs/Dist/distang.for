      subroutine DistAng(u,ug,v,vg,uu,vv,sgx,MetTensP,angle,su)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension u(3),ug(3),v(3),vg(3),sgx(9),ag(3,3),ddx(9)
      real MetTensP(9)
      angle=0.
      su=0.
      uv=scalmul(u,vg)
      denq=uu*vv
      den=sqrt(denq)
      if(den.lt..0001) go to 9999
      arg=uv/den
      if(abs(arg).gt..999999) then
        angle=90.-sign(90.,arg)
        su=.5
        go to 9999
c      else if(abs(arg).lt..0001) then
c       angle=90.
c       go to 9999
c       su=.2
c        go to 9999
      endif
      angle=acos(arg)/torad
      denp=1./(sqrt(1.-arg**2)*torad)
      pom=denp/den
      do l=1,3
        dtul=pom*(vg(l)-ug(l)*uv/uu)
        dtvl=pom*(ug(l)-vg(l)*uv/vv)
        ddx(l)=-dtul-dtvl
        ddx(l+3)=dtul
        ddx(l+6)=dtvl
      enddo
      su=sigma(sgx,ddx,9)
      do i=1,3
        do j=1,3
          ag(i,j)=1./denq*(u(i)*v(j)
     1                     -.5*uv/denq*(u(i)*u(j)*vv+v(i)*v(j)*uu))**2
        enddo
      enddo
      gu=0.
      m=0
      do i=1,3
        do j=1,3
          m=m+1
          gu=gu+ag(i,j)*MetTensP(m)
        enddo
      enddo
      gu=denp*sqrt(gu)
      su=sqrt(su**2+gu**2)
9999  return
      end
