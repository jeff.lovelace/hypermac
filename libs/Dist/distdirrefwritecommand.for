      subroutine DistDirRefWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      dimension xp(3)
      character*256 EdwStringQuest,StDirRef
      character*(*) Command
      character*80 ErrString
      save /DirRefQuest/
      ich=0
      Command='dirref'
      StDirRef=EdwStringQuest(nEdwDirRef)
      if(StDirRef.eq.' ') then
        ErrString='Reference direction not defined'
        go to 9000
      endif
      k=0
      call StToReal(StDirRef,k,xp,3,.false.,ich)
      if(ich.ne.0) then
        ErrString='Numnerical problem in the reference direction'
        go to 9000
      endif
      Command=Command(:idel(Command)+1)//StDirRef(:idel(StDirRef))
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      ich=1
9999  return
      end
