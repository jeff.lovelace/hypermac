      subroutine Fourier
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      logical FeYesNo
      call FouPrelim
      if(ErrFlag.ne.0) go to 9000
      call FouSynthesis
      if(ErrFlag.ne.0) go to 9000
      if(lpeaks.ne.0.and..not.ContourCallFourier) then
        call FouPeaks(0,0)
        if(ErrFlag.ne.0) go to 9000
      endif
9000  call CloseIfOpened(80)
      call CloseListing
      if(lpeaks.ne.0.and.ErrFlag.eq.0.and..not.RefineCallFourier.and.
     1   .not.ContourCallFourier.and.
     2   .not.SolutionCallFourier.and..not.Patterson) then
        if(FeYesNo(-1.,-1.,'Do you want to start the procedure for '//
     1             'including of new atoms?',1)) then
          call EM40NewAtFromFourier(ich)
          if(ich.ne.0) then
            call CopyFile(PreviousM40,fln(:ifln)//'.m40')
            call DeleteFile(PreviousM40)
          else
            call iom40only(1,0,fln(:ifln)//'.m40')
          endif
          go to 9900
        endif
      endif
      if(.not.RefineCallFourier.and..not.ContourCallFourier.and.
     1   .not.SolutionCallFourier.and.ErrFlag.eq.0.and..not.BatchMode)
     2  call FeShowListing(-1.,-1.,'FOURIER program',fln(:ifln)//'.fou',
     3                     10000)
9900  if(Patterson) then
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
      endif
      if(allocated(SinTable)) deallocate(SinTable,CosTable)
      return
      end
      subroutine FouPrelim
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*256 Veta
      integer CrlCentroSymm
      logical ExistM80,eqiv,EqIgCase,konec,ExistFile,AddBorder,EqIV0
      dimension ih(6),ihp(6),ihpp(6),mxh(6),mxd(6),hh(6),hhp(6),
     1          fr(:),fi(:),mlt(:)
      allocatable fr,fi,mlt
      call DeleteFile(fln(:ifln)//'_Fourier.tmp')
      call OpenFile(lst,fln(:ifln)//'.fou','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      uloha='Program for n-dimensional Fourier synthesis'
      OrCode=-333
      call SetRealArrayTo(xrmn,NDim(KPhase),-333.)
      ptstep=-1.0
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultFourier
      call NactiFourier
      if(ErrFlag.ne.0) go to 9999
      if(RefineCallFourier) then
        YMinFlowChart=120.
      else
        YMinFlowChart=-1.
      endif
      if(NDim(KPhase).le.3) then
        nsubs=1
      else
        nsubs=min(nsubs,NComp(KPhase))
      endif
      Patterson=mapa.le.3.or.Mapa.eq.9
      call iom50(0,1,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      ExistM80=ExistFile(fln(:ifln)//'.m80')
      if(.not.ExistM80.and.mapa.ne.1) then
        call FeChybne(-1.,-1.,'the M80 file doesn''t exist, first you'//
     1                ' have to','run REFINE to get phases and/or '//
     2                'Fcalc.',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      if(ptstep.lt.0.) ptstep=.25
      call FouDefLimits
      if(ExistM80) then
        call OpenFile(80,fln(:ifln)//'.m80','formatted','old')
        if(ErrFlag.ne.0) go to 9999
      else
        call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9999
      endif
      FileM82='jm82'
      call CreateTmpFile(FileM82,i,0)
      call FeTmpFilesAdd(FileM82)
      call OpenFile(82,FileM82,'unformatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      twov=2./CellVol(nsubs,1)
      if(Patterson) then
        srnat=0.
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          do i=1,NAtFormula(KPhase)
            srnat=srnat+AtMult(i,KPhase)
          enddo
        endif
        if(srnat.gt.0.) then
          do i=1,NAtFormula(KPhase)
            fx(i)=ffbasic(1,i,KPhase)+ffra(i,KPhase,KDatBlock)
          enddo
          pom0=0.
          do i=1,NAtFormula(KPhase)
            pom0=pom0+
     1           AtMult(i,KPhase)*sqrt((fx(i)**2+
     2                                  ffia(i,KPhase,KDatBlock)**2))
          enddo
        else
          pom0=1.
        endif
      endif
      if(lite(KPhase).eq.1) then
        rhom=1.
      else
        rhom=episq
      endif
      AddBorder=.false.
      if(ptname.ne.'[nic]') then
        if(ptname.ne.'[neco]') then
          kk=0
          call SetRealArrayTo(ptx,3,0.)
          pom=0.
1150      call Kus(ptname,kk,Veta)
          call atsym(Veta,i,hhp,hh,hh(4),j,k)
          if(i.le.0) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t on the '//'file M40',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(k.eq.3) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t correct',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(ErrFlag.ne.0) go to 9000
          pom=pom+1.
          call AddVek(ptx,hhp,ptx,3)
          if(kk.lt.idel(ptname)) go to 1150
          do i=1,3
            ptx(i)=ptx(i)/pom
          enddo
        endif
        do j=1,3
          nd=nint(.5*pts(j)/ptstep)
          if(nd.ne.0) then
            dd(j)=ptstep/CellPar(j,nsubs,KPhase)
          else
            dd(j)=1.
          endif
          xrmn(j)=ptx(j)-dd(j)*float(nd)
          xrmx(j)=ptx(j)+dd(j)*float(nd)
        enddo
      endif
      if(OrCode.eq.-333) OrCode=nop(nsubs)
      norien=OrCode
      n=norien
      i=10**(NDim(KPhase)-1)
      do j=1,NDim(KPhase)-1
        iorien(j)=n/i
        n=mod(n,i)
        i=i/10
      enddo
      iorien(NDim(KPhase))=n
      do j=1,6
        if(j.le.NDim(KPhase)) then
          if(xrmn(j).lt.-330.) then
            xrmn(j)=fourmn(j,nsubs)
            xrmx(j)=fourmx(j,nsubs)
            if(j.le.3) then
              n=max(nint((xrmx(j)-xrmn(j))*
     1              CellPar(j,nsubs,KPhase)/ptstep),1)
              dd(j)=(xrmx(j)-xrmn(j))/float(n)
            else
              dd(j)=.1
            endif
            if(IndUnit.eq.1) then
              AddBorder=.true.
            else
              xrmx(j)=xrmx(j)-dd(j)
            endif
          endif
        else
          xrmn(j)=0.
          xrmx(j)=0.
          dd(j)=1.
        endif
      enddo
      do i=1,NDim(KPhase)
        n=iorien(i)
        if(n.lt.1.or.n.gt.NDim(KPhase)) go to 1500
        do j=i+1,NDim(KPhase)
          if(n.eq.iorien(j)) go to 1500
        enddo
      enddo
      go to 1550
1500  Veta='wrong orientation'
      i=idel(Veta)+1
      write(Veta(i:i+6),'(i7)') norien
      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
      ErrFlag=1
      go to 9000
1550  do i=1,6
        if(i.le.NDim(KPhase)) then
          j=iorien(i)
          if(AddBorder.and.j.le.3) then
            xrmn(j)=xrmn(j)-dd(j)
            xrmx(j)=xrmx(j)+dd(j)
          endif
          xfmn(i)=xrmn(j)
          xfmx(i)=xrmx(j)
          xdf(i)=dd(j)
        else
          xfmn(i)=0.
          xfmx(i)=0.
          xdf(i)=1.
        endif
      enddo
      NDimSum=0
      do i=1,6
        pom=xfmx(i)-xfmn(i)
        nx(i)=nint(pom/xdf(i))+1
        if(i.le.NDim(KPhase)) then
          if(nx(i).gt.1) then
            dpom=pom/float(nx(i)-1)
            if(abs(xdf(i)-dpom).gt..00001) xdf(i)=dpom
          endif
          if(pom.lt.0.or.xdf(i).lt.0.) then
            write(Veta,'(3f8.3)') xfmn(i),xfmx(i),xdf(i)
            call ZdrcniCisla(Veta,3)
            j=idel(Veta)
            if(NDim(KPhase).eq.3) then
              Veta='Incorrect limits for '//smbx(i)//' : '//Veta(:j)
            else
              Veta='Incorrect limits for '//smbx6(i)//' : '//Veta(:j)
            endif
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            ErrFlag=1
            go to 9000
          endif
        endif
        if(nx(i).gt.1) NDimSum=NDimSum+1
      enddo
      NDimSum=min(NDimSum,NDim(KPhase))
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      if(nxny.lt.50) then
        write(Veta,'(i5,''x'',i5)') nx(1),nx(2)
        call Zhusti(Veta)
        Veta='2d sections would contain only '//Veta(:idel(Veta))//
     1      ' points.'
        call FeChybne(-1.,-1.,Veta,'Please enlarge the scope of maps.',
     1                SeriousError)
        ErrFlag=1
        go to 9000
      endif
1630  if(Patterson) then
        do i=1,NSymmN(KPhase)
          call SetRealArrayTo(s6(1,i,1,KPhase),NDim(KPhase),0.)
        enddo
        if(CrlCentroSymm().le.0) then
          call ReallocSymm(NDim(KPhase),2*NSymmN(KPhase),
     1                     NLattVec(KPhase),NComp(KPhase),NPhase,0)
          n=NSymmN(KPhase)
          do i=1,NSymmN(KPhase)
            call RealMatrixToOpposite(rm6(1,i,1,KPhase),
     1        rm6(1,i+n,1,KPhase),NDim(KPhase))
            call MatBlock3(rm6(1,i+n,1,KPhase),rm(1,i+n,1,KPhase),
     1                     NDim(KPhase))
            call CopyVek(s6(1,i,1,KPhase),s6(1,i+n,1,KPhase),
     1                   NDim(KPhase))
            ISwSymm(i+n,1,KPhase)=ISwSymm(i,1,KPhase)
          enddo
          NSymmN(KPhase)=2*n
          MaxNSymm=max(MaxNSymm,NSymm(KPhase))
          call ReallocateAtoms(0)
        endif
      endif
      do i=1,6
        if(i.le.NDim(KPhase)) then
          cx(i)=smbx6(iorien(i))
        else
          cx(i)=' '
        endif
      enddo
      if(Patterson) then
        npeaks(1)=50
      else
        if(npeaks(1).lt.0) then
          fa=0.
          fah=0.
          do i=1,NAtCalc
            if(ffbasic(1,isf(i),KPhase).lt.2.) then
              fah=fah+ai(i)
            else
              fa=fa+ai(i)
            endif
          enddo
          pom=0.
          pomh=0.
          do i=1,NAtFormula(KPhase)
            if(EqIgCase(AtType(i,KPhase),'H')) then
              pomh=pomh+AtMult(i,KPhase)
            else
              pom=pom+AtMult(i,KPhase)
            endif
          enddo
          pom=pom*float(NUnits(KPhase))/
     1        float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          pomh=pomh*float(NUnits(KPhase))/
     1      float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          nachp=nint(pomh)
          if(mapa.eq.4.or.mapa.eq.5) then
            npeaks(1)=nacp+10
          else
            npeaks(1)=max(nacp-nint(fa)+nachp-nint(fah)+10,10)
          endif
        endif
        if(npeaks(2).lt.0) then
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            npeaks(2)=10
          else
            NPeaks(2)=NPeaks(1)
          endif
        endif
        do i=1,2
          npeaks(i)=min(npeaks(i),3000)
        enddo
      endif
      Veta='Type of map : '
      if(mapa.eq.1) then
        Veta(15:)='F(obs)**2 - Patterson'
      else if(mapa.eq.2) then
        Veta(15:)='F(calc)**2 - checking Patterson'
      else if(mapa.eq.3) then
        Veta(15:)='F(obs)**2-F(calc)**2 - difference Patterson'
      else if(mapa.eq.4) then
        Veta(15:)='F(obs) - Fourier'
      else if(mapa.eq.5) then
        Veta(15:)='F(calc) - checking Fourier'
      else if(mapa.eq.6) then
        Veta(15:)='F(obs)-F(cal) - difference Fourier'
      else if(mapa.eq.7) then
        Veta(15:)='dynamic multipole deformation map'
      else if(mapa.eq.8) then
        Veta(15:)='static multipole deformation map'
      else if(mapa.eq.9) then
        Veta(15:)='1/0 - shape function'
      else
        call FeChybne(-1.,-1.,'wrong map type',' ',SeriousError)
        ErrFlag=1
        go to 9000
      endif
      call newln(2)
      write(lst,FormA1)
      write(lst,FormA1)(Veta(i:i),i=1,idel(Veta))
      if(vyber.gt.0.) then
        call newln(2)
        write(lst,'(/''Reflections with  |Fo| > '',f10.2,'' * |Fc| '',
     1               ''will not be used in the synthesis'')') vyber
      endif
      call newln(2)
      if(UseWeight.gt.0) then
        write(lst,'(/''Weighting of reflection based on chi-square '',
     1               ''will be applied'')')
      else
        write(lst,'(/''No weighting of reflections will be applied'')')
      endif
      if(snlmx.le.0.0) snlmx=10.0
      call newln(2)
      write(lst,'(/''Limits of sin(th)/lambda for acceptance are : '',
     1            2f10.6)') snlmn,snlmx
      if(NComp(KPhase).gt.1) then
        call newln(2)
        write(lst,'(/''Fourier for subsystem #'',i1,'' will be '',
     1               ''calculated'')') nsubs
      endif
      rewind 82
      call FeMouseShape(3)
      call SetIntArrayTo(mxh,6,0)
      call FouNastavM80
      if(ErrFlag.ne.0) go to 9999
3000  if(ExistM80) then
        read(80,FormA80,end=3100) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 3100
        endif
        read(Veta,Format80,end=3100,err=8000)(ih(i),i=1,maxNDim),KPh,
     1                                       fo1,fc
      else
        read(91,format91,end=3100,err=8100)
     1    (ih(i),i=1,NDim(KPhase)),fo1,fo2
        KPh=KPhase
      endif
      if(ih(1).gt.900) go to 3100
      if(KPh.ne.KPhase) go to 3000
      if(nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        do j=1,NDim(KPhase)
          mxh(j)=max(mxh(j),iabs(ihpp(iorien(j))))
        enddo
      enddo
      go to 3000
3100  iz=0
      n=1
      do i=1,NDim(KPhase)
        mxd(i)=2*mxh(i)+1
        n=n*mxd(i)
        if(mxd(i).gt.nx(i).and.i.le.NDimSum.and.
     1     FMethod.eq.FFTMethod) then
          if(i.gt.2) then
            NDimSum=i-1
          else
            nxo=nx(i)
            nx(i)=mxd(i)
            xfmxo=xfmx(i)
            xfmx(i)=xfmx(i)-xdf(i)
            xfmn(i)=xfmn(i)+xdf(i)
            xdf(i)=(xfmx(i)-xfmn(i))/float(nx(i)-2)
3150        xfmn(i)=xfmn(i)-xdf(i)
            xfmx(i)=xfmx(i)+xdf(i)
            if(xfmxo.gt.xfmx(i)) then
              nx(i)=nx(i)+2
              go to 3150
            endif
          endif
        endif
      enddo
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      allocate(fr(n),fi(n),mlt(n))
      call SetRealArrayTo(fr,n,0.)
      call SetRealArrayTo(fi,n,0.)
      call SetIntArrayTo(mlt,n,0)
      if(ExistM80) then
        rewind 80
        call FouNastavM80
        if(ErrFlag.ne.0) go to 9999
      else
        call NastavM90(91)
      endif
      mmax=0
      kolik=0
      konec=.false.
3500  if(ExistM80) then
        read(80,FormA256,end=5000) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 5000
        endif
        read(Veta,Format80,err=8000,end=5000)
     1    (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2     acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
        if(ih(1).gt.900) go to 5000
        if(KPh.ne.KPhase) go to 3500
        if(EqIV0(ih,maxNDim)) then
          bc=0.
          bcfree=0.
          bcst=0.
          bcfreest=0.
        endif
      else
        if(isPowder) then
          read(91,format91Pow,err=8100,end=5000) ih,fo1,fo2,i,i,KPh
          if(KPh.ne.KPhase) go to 3500
        else
          read(91,format91,err=8100,end=5000)(ih(i),i=1,NDim(KPhase)),
     1                                        fo1,sigfo1
          sigfo2=sigfo1
          sigfo1=0.
          sigfo2=0.
          KPh=KPhase
        endif
        fc=0.
        if(ih(1).lt.900) then
          if(fo1.gt.0.) then
            fo1=sqrt(fo1)
          else
            fo1=0.
          endif
          fo2=fo1
        else
          go to 5000
        endif
      endif
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      if(TMethod.eq.RatioMethod) then
        fo=fo1
        sigfo=sigfo1
      else
        fo=fo2
        sigfo=sigfo2
      endif
      if(vyber.gt.0.and.abs(fo).gt.vyber*abs(fc)) go to 3500
      if(sinthl.gt.snlmx.or.sinthl.lt.snlmn) go to 3500
      if(fc.ne.0.) then
        cosaq=ac/fc
        sinaq=bc/fc
      else
        cosaq=0.
        sinaq=0.
      endif
      if(NDim(KPhase).gt.3.and.nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      if(mapa.eq.1) then
        frp=fo**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.2) then
        frp=fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.3) then
        frp=fo**2-fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.4) then
        frp=fo*cosaq
        fip=fo*sinaq
        SigCoef=sigfo
        Coef=fo
      else if(mapa.eq.5) then
        frp=fc*cosaq
        fip=fc*sinaq
        SigCoef=sigfo
        Coef=fc
      else if(mapa.eq.6) then
        pom=fo-fc
        frp=pom*cosaq
        fip=pom*sinaq
        SigCoef=sigfo
        Coef=pom
      else if(mapa.eq.7) then
        frp=ac-acfree
        fip=bc-bcfree
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.8) then
        frp=acst-acfreest
        fip=bcst-bcfreest
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.9) then
        frp=1.
        fip=0.
        SigCoef=0.
        Coef=frp
      endif
      if(SigCoef.gt.1.e-30.and.UseWeight.eq.1) then
        if(abs(Coef).gt.1.e-30) then
          pom=max(1.-(SigCoef/Coef)**2,0.)
        else
          pom=0.
        endif
        frp=pom*frp
        fip=pom*fip
      endif
      if(ih(1).eq.0.and.ih(2).eq.0.and.ih(3).eq.0) then
        frp=frp/2.
        fip=fip/2.
      endif
      if(Patterson) then
        rho=sinthl**2*rhom
        if(srnat.gt.0.) then
          call SetFormF(sinthl)
          pom=0.
          do i=1,NAtFormula(KPhase)
            pom=pom+AtMult(i,KPhase)*fx(i)
          enddo
        else
          pom=1.
        endif
        pom=pom*exp(-OverAllB(KDatBlock)*rho)
        frp=frp*(pom0/pom)**2
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        arg=0.
        do j=1,NDim(KPhase)
          arg=arg-float(ih(j))*s6(j,i,nsubs,KPhase)
        enddo
        arg=arg*pi2
        cs=cos(arg)
        sn=sin(arg)
        pom1=twov*(frp*cs-fip*sn)
        pom2=twov*(frp*sn+fip*cs)
        do j=1,NDim(KPhase)
          if(ihpp(iorien(j)).lt.0) then
            pom2=-pom2
            call IntVectorToOpposite(ihpp,ihpp,NDim(KPhase))
            go to 4118
          else if(ihpp(iorien(j)).gt.0) then
            go to 4118
          endif
        enddo
4118    indp=0
        do j=NDim(KPhase),1,-1
          indp=indp*mxd(j)+ihpp(iorien(j))+mxh(j)
        enddo
        fr(indp)=fr(indp)+pom1
        fi(indp)=fi(indp)+pom2
        mlt(indp)=mlt(indp)+1
      enddo
      go to 3500
5000  call SetIntArrayTo(ihp,6,0)
      call SetIntArrayTo(hmin,6, 999999)
      call SetIntArrayTo(hmax,6,-999999)
      if(FMethod.eq.FFTMethod) then
        Redukce=.5
      else
        Redukce=1.
      endif
      do i=1,UBound(fr,1)
        indp=i
        if(mlt(indp).le.0) cycle
        pom=Redukce/float(mlt(indp))
        fr(indp)=fr(indp)*pom
        fi(indp)=fi(indp)*pom
        do j=1,NDim(KPhase)
          ihp(j)=mod(indp,mxd(j))-mxh(j)
          indp=indp/mxd(j)
          if(j.gt.3) mmax=max(mmax,iabs(ihp(j)))
        enddo
        do j=1,NDim(KPhase)
          hmax(j)=max(hmax(j),ihp(j))
          hmin(j)=min(hmin(j),ihp(j))
        enddo
        write(82) ihp,fr(i),fi(i)
        if(FMethod.eq.FFTMethod) then
          call IntVectorToOpposite(ihp,ihp,NDim(KPhase))
          do j=1,NDim(KPhase)
            hmax(j)=max(hmax(j),ihp(j))
            hmin(j)=min(hmin(j),ihp(j))
          enddo
          write(82) ihp,fr(i),-fi(i)
        endif
      enddo
      if(FMethod.eq.BLMethod) then
        n=0
        m=0
        do i=1,NDim(KPhase)
          n=max(n,hmax(i)-hmin(i)+1)
          m=max(m,nx(i))
        enddo
        allocate(SinTable(n,m,NDim(KPhase)),
     1          CosTable(n,m,NDim(KPhase)))
        do n=1,NDim(KPhase)
          do i=hmin(n),hmax(n)
            ii=i-hmin(n)+1
            fh=pi2*float(i)
            do j=1,nx(n)
              arg=fh*(xfmn(n)+(j-1)*xdf(n))
              SinTable(ii,j,n)=sin(arg)
              CosTable(ii,j,n)=cos(arg)
            enddo
          enddo
        enddo
      endif
      rewind 82
      MameSatelity=mmax.gt.0
      if(.not.MameSatelity) then
        do i=1,3
          if(iorien(i).gt.3) go to 9000
        enddo
        call SetIntArrayTo(nx(4),NDimI(KPhase),1)
        call SetRealArrayTo(xfmn(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xfmx(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xdf(4),NDimI(KPhase),0.1)
        nmap=nx(3)
      endif
      if(FMethod.eq.FFTMethod) then
        do i=1,NDimSum
          nx(i)=nint(1./xdf(i))
          n=2
6010      if(n.lt.nx(i)) then
            n=n*2
            go to 6010
          endif
          nx(i)=n
          xdf(i)=1./float(n)
          xfmn(i)=0.
          xfmx(i)=1.-xdf(i)
        enddo
        nxny=nx(1)*nx(2)
        nmap=nx(3)*nx(4)*nx(5)*nx(6)
      endif
      call newln(NDim(KPhase)+2)
      write(lst,FormA1)
      write(lst,'(''Scope of the map :'')')
      do i=1,NDim(KPhase)
        if(nx(i).gt.1) then
          write(lst,'(a2,'' from'',f8.4,'' to'',f8.4,'' step'',f7.4)')
     1          cx(i),xfmn(i),xfmx(i),xdf(i)
        else
          xfmn(i)=(xfmn(i)+xfmx(i))*.5
          xfmx(i)=xfmn(i)
          write(lst,'(a2,'' fixed to'',f8.4)') cx(i),xfmn(i)
        endif
      enddo
      call newln(2)
      write(lst,FormA1)
      write(Veta,'(''Orientation : '',6i1)')(iorien(i),i=1,NDim(KPhase))
      write(lst,FormA) Veta(:idel(Veta))
      call newln(2)
      write(lst,FormA1)
      Veta='The calculation will performed'
      if(FMethod.eq.FFTMethod) then
        Veta(idel(Veta)+2:)='by FFT method'
      else
        Veta(idel(Veta)+2:)=' by modified Beevers-Lipson algorithm'
      endif
      write(lst,FormA) Veta(:idel(Veta))
      if(NTwin.gt.1) then
        call newln(2)
        write(lst,FormA1)
        Veta='F(obs) will be corrrected for twinning'
        if(FMethod.eq.FFTMethod) then
          Veta(idel(Veta)+2:)='by substracting of F(calc)'
        else
          Veta(idel(Veta)+2:)='by in ratio of F(calc)'
        endif
        write(lst,FormA) Veta(:idel(Veta))
      endif
      go to 9999
8000  call FeReadError(80)
      go to 8900
8100  call FeReadError(91)
8900  ErrFlag=1
9000  call DeleteFile(fln(:ifln)//'.m81')
      call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
9999  if(allocated(fr)) deallocate(fr,fi,mlt)
      return
      end
      subroutine FouNastavM80
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      logical EqIgCase
      if(NDatBlock.gt.1) then
1000    read(80,FormA80,end=1020,err=8000) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(t80,k,Cislo)
          if(EqIgCase(Cislo,'begin')) go to 9999
        endif
        go to 1000
1020    call FeChybne(-1.,-1.,'the m80 file for '//
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))//
     2                ' has not been prepared.',' ',SeriousError)
        go to 8000
      endif
      go to 9999
8000  ErrFlag=1
9999  return
      end
      subroutine FouBreak
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      logical   FeYesNo
      if(FeYesNo(-1.,YBottomMessage,'Do you really want to cancel the'//
     1           ' Fourier summation?',0)) then
        call DeleteFile(fln(:ifln)//'.m81')
        call DeleteFile(FileM82)
        call FeTmpFilesClear(FileM82)
        call FeFlowChartRemove
        ErrFlag=1
        go to 9999
      endif
9999  return
      end
      subroutine DefaultFourier
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      NactiInt=20
      NactiReal=20
      NactiComposed=13
      NactiKeys=NactiInt+NactiReal+NactiComposed
      call CopyVekI(DefIntFour,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntFour,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntFour,CmdIntFour,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealFour(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealFour(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealFour(i),CmdRealFour, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdFour(i)
      enddo
      ptname='[nic]'
      ptx(1)=0.
      ptx(2)=0.
      ptx(3)=0.
      pts(1)=.5
      pts(2)=.5
      pts(3)=.5
      return
      end
      subroutine NactiFourier
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension xp(3)
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('fourier',i)
      if(i.ne.1) go to 9999
      call SetIntArrayTo(NactiRepeat,NactiKeys,0)
      PreskocVykricnik=.true.
      izpet=0
1100  call NactiCommon(M50,izpet)
      if(ErrFlag.ne.0) go to 9999
      if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntFour ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealFour,NactiReal)
        if(lite(KPhase).eq.0) then
          OverAllB(KDatBlock)=NacetlReal(nCmdUOverAll)
        else
          OverAllB(KDatBlock)=NacetlReal(nCmdBOverAll)
        endif
        go to 9999
      else if(izpet.ge.nCmdxlim.and.izpet.le.nCmdx6lim) then
        if(izpet.le.nCmdzlim) then
          j=izpet-nCmdxlim+1
        else
          j=izpet-nCmdx1lim+1
        endif
        call StToReal(NactiVeta,PosledniPozice,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        xrmn(j)=xp(1)
        xrmx(j)=xp(2)
        if(xp(3).le.0.) xp(3)=1.
        dd(j)=xp(3)
      else if(izpet.eq.nCmdcenter) then
        call StToReal(NactiVeta,PosledniPozice,ptx,3,.false.,ich)
        if(ich.eq.0) then
          ptname='[neco]'
          go to 1100
        endif
        ptname=NactiVeta(PosledniPozice+1:)
        call UprAt(ptname)
      else if(izpet.eq.nCmdscope) then
        call StToReal(NactiVeta,PosledniPozice,pts,3,.false.,ich)
        if(ich.ne.0) go to 9000
      endif
      if(izpet.ne.0) go to 1100
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      return
102   format(f15.5)
      end
      subroutine FouSynthesis
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      dimension xp(6),ix(6),ihp(6),Table(:),TableA(:),mxi(6),mni(6),
     1          mdi(6)
      integer SmallStep,SmallStep2,Delta,ss
      logical AlreadyAllocated
      allocatable Table,TableA
      call OpenMaps(81,fln(:ifln)//'.m81',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      rmax=0.
      rmin=0.
      write(81,rec=1) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,iorien,
     1                mapa,nsubs+(KPhase-1)*10,MameSatelity,
     2                nonModulated(KPhase)
      irec=1
      nn=0
      nd=NDimSum
      fn=25000000.
      do i=1,nd
        fn=fn/float(nx(i))
        if(fn.lt.1.) then
          nd=i-1
          go to 1060
        endif
      enddo
1060  ncykl=1
      do i=nd+1,6
        ncykl=ncykl*nx(i)
      enddo
      mxc=1
      do i=1,nd
        mni(i)=1
        if(FMethod.eq.FFTMethod) then
          mxi(i)=nx(i)
        else
          mxi(i)=max(nx(i),hmax(i)-hmin(i)+1)
        endif
        mdi(i)=mxi(i)-mni(i)+1
        mxc=mxc*mdi(i)
      enddo
      allocate(Table(2*mxc))
      NMaxFlow=0
      do i=nd,1,-1
        n=1
        do j=1,i
          if(FMethod.eq.FFTMethod) then
            n=n*mdi(j)
          else
            n=n*(hmax(j)-hmin(j)+1)
          endif
        enddo
        do j=i+1,nd
          n=n*mdi(j)
        enddo
        NMaxFlow=NMaxFlow+n
      enddo
      NMaxFlow=NMaxFlow*ncykl
      call FeFlowChartOpen(-1.,YMinFlowChart,nint(float(NMaxFlow)*.005),
     1                     NMaxFlow,'Fourier summation',' ',' ')
      n=0
      do i=1,nd
        n=max(n,mdi(i))
      enddo
      allocate(TableA(2*n))
      do it=1,ncykl
        call RecUnpack(it,ix,nx(nd+1),6-nd)
        do i=1,6-nd
          j=i+nd
          xp(j)=xfmn(j)+(ix(i)-1)*xdf(j)
        enddo
        call SetRealArrayTo(Table,2*mxc,0.)
1200    read(82,end=1240) ihp,ai,bi
        indp=0
        do j=nd,1,-1
          indp=indp*mdi(j)+ihp(j)-hmin(j)
        enddo
        locc=2*indp+1
        locd=locc+1
        if(nd.ne.NDim(KPhase)) then
          xlz=0.
          do j=nd+1,NDim(KPhase)
            xlz=xlz+float(ihp(j))*xp(j)
          enddo
          xlz=pi2*xlz
          sinlz=sin(xlz)
          coslz=cos(xlz)
          Table(locc)=Table(locc)+ai*coslz+bi*sinlz
          Table(locd)=Table(locd)-ai*sinlz+bi*coslz
        else
          Table(locc)=ai
          Table(locd)=bi
        endif
        go to 1200
1240    rewind 82
        do nh=nd,1,-1
          LargeStep=1.
          SmallStep=1.
          do i=1,nh-1
            SmallStep=SmallStep*mdi(i)
          enddo
          do i=nh+1,nd
            LargeStep=LargeStep*mdi(i)
          enddo
          SmallStep2=2*SmallStep
          mdinh=mdi(nh)
          mdinh2=2*mdinh
          nxnh=nx(nh)
          Delta=SmallStep*mdinh
          if(FMethod.eq.FFTMethod) then
            ms=1
            mk=mdinh2
            nxsum=mdinh
          else
            if(nxnh.ge.mdi(nh)) then
              ms=1
              mk=2*(hmax(nh)-hmin(nh)+1)
              nxsum=mdinh
            else
              ms=1
              mk=mdinh2
              nxsum=nxnh
            endif
          endif
          ksp=0
          do ls=1,LargeStep
            ks=ksp
            call SetIntArrayTo(ix,nh-1,1)
            do ss=1,SmallStep
              ks=ks+1
              if(FMethod.eq.BLMethod) then
                do i=1,nh-1
                  if(ix(i).gt.hmax(i)-hmin(i)+1) go to 1610
                enddo
                call SetRealArrayTo(TableA,mdinh2,0.)
                ih=0
              endif
              k=2*ks-1
              do m=ms,mk,2
                if(FMethod.eq.BLMethod) ih=ih+1
                call FeFlowChartEvent(nn,ie)
                if(ie.ne.0) then
                  call FouBreak
                  if(ErrFlag.ne.0) go to 9999
                endif
                chy=Table(k  )
                dhy=Table(k+1)
                if(FMethod.eq.FFTMethod) then
                  TableA(m  )=chy
                  TableA(m+1)=dhy
                else
                  if(chy.eq.0..and.dhy.eq.0.) go to 1450
                  loc1=-1
                  do i=1,nxsum
                    loc1=loc1+2
                    loc2=loc1+1
                    sinxh=SinTable(ih,i,nh)
                    cosxh=CosTable(ih,i,nh)
                    TableA(loc1)=TableA(loc1)+chy*cosxh+dhy*sinxh
                    TableA(loc2)=TableA(loc2)-chy*sinxh+dhy*cosxh
                  enddo
                endif
1450            k=k+SmallStep2
              enddo
              if(FMethod.eq.FFTMethod) then
                call four1(TableA,mdi(nh),-1)
                m=1
                do i=1,mdi(nh)
                  if(mod(i-1,2).eq.1) then
                    TableA(m  )=-TableA(m  )
                    TableA(m+1)=-TableA(m+1)
                  endif
                  m=m+2
                enddo
              endif
              k=2*ks-1
              do m=1,mdinh2,2
                Table(k  )=TableA(m  )
                Table(k+1)=TableA(m+1)
                k=k+SmallStep2
              enddo
1610          if(FMethod.eq.BLMethod) then
                do i=1,nh-1
                  ix(i)=ix(i)+1
                  if(ix(i).gt.mxi(i)) then
                    ix(i)=1
                  else
                    exit
                  endif
                enddo
              endif
            enddo
            ksp=ksp+Delta
          enddo
        enddo
        k=0
        m=-1
        do 2900i=1,mxc
          m=m+2
          call RecUnpack(i,ix,mdi,nd)
          do j=1,nd
            if(ix(j).gt.nx(j)) go to 2900
          enddo
          k=k+1
          Table(k)=Table(m)
2900    continue
        do i=1,k
          rmax=max(rmax,Table(i))
          rmin=min(rmin,Table(i))
        enddo
        IZdih=0
        do i=1,k/nxny
          irec=irec+1
          write(81,rec=irec)(table(IZdih+j),j=1,nxny)
          IZdih=IZdih+nxny
        enddo
      enddo
8000  irec=irec+1
      write(81,rec=irec) rmax,rmin
      call newln(2)
      write(lst,FormA80)
      write(Cislo,'(f15.2)') rmax
      call ZdrcniCisla(Cislo,1)
      t80='Maximal density : '//Cislo(:idel(Cislo))
      write(Cislo,'(f15.2)') rmin
      call ZdrcniCisla(Cislo,1)
      t80=t80(:idel(t80))//', minimal density : '//Cislo(:idel(Cislo))
      write(lst,FormA1)(t80(i:i),i=1,idel(t80))
      if(Mapa.eq.6) then
        if(allocated(CifKey)) then
          AlreadyAllocated=.true.
        else
          AlreadyAllocated=.false.
          allocate(CifKey(400,40),CifKeyFlag(400,40))
        endif
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.eq.0) then
          LnSum=NextLogicNumber()
          call OpenFile(LnSum,fln(:ifln)//'_Fourier.l70','formatted',
     1                  'unknown')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,'(''# Fourier'')')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,FormA)
          do i=1,2
            if(i.eq.1) then
              pom=rmax
            else
              pom=rmin
            endif
            write(Cislo,'(f8.2)') pom
            call Zhusti(Cislo)
            write(LnSum,FormCIF) CifKey(i,15),Cislo
          enddo
          call CloseIfOpened(LnSum)
        endif
        if(allocated(CifKey).and..not.AlreadyAllocated)
     1     deallocate(CifKey,CifKeyFlag)
      endif
      call FeFlowChartRemove
9999  call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
      if(allocated(Table)) deallocate(Table)
      if(allocated(TableA)) deallocate(TableA)
      close(81)
      return
      end
      subroutine FouDefLimits
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ias(:,:),bs(:,:)
      logical eqiv,eqrv
      allocatable ias,bs
      if(IndUnit.ne.1.or.FMethod.eq.FFTMethod) then
        call SetRealArrayTo(FourMn,6*NComp(KPhase),0.)
        call SetRealArrayTo(FourMx,6*NComp(KPhase),1.)
        nop(1)=0
        do i=1,NDim(KPhase)
          nop(1)=nop(1)*10+i
        enddo
        do i=2,NComp(KPhase)
          nop(i)=nop(1)
        enddo
        go to 9999
      endif
      n=2*NSymmN(KPhase)*NLattVec(KPhase)
      allocate(ias(3,n),bs(3,n))
      do isw=1,NComp(KPhase)
        n=1
        do 1300i=1,NSymmN(KPhase)
          k=0
          do j=1,3
            j2=j+3*mod(j,3)
            j3=j+3*mod(j+1,3)
            if(rm(k+j,i,isw,KPhase).eq.0..or.
     1         abs(rm(j2,i,isw,KPhase)).gt..01.or.
     1         abs(rm(j3,i,isw,KPhase)).gt..01) go to 1300
            ias(j,n)=nint(rm(k+j,i,isw,KPhase))
            bs(j,n)=s6(j,i,isw,KPhase)
            k=k+3
          enddo
          do j=1,n-1
            if(eqiv(ias(1,n),ias(1,j),3).and.
     1         eqrv(bs(1,n),bs(1,j),3,.0001)) go to 1300
          enddo
          n=n+1
1300    continue
        n=n-1
        if(Patterson) then
          call SetRealArrayTo(bs,3*n,0.)
          k=2
        else
          k=1
        endif
        call nascen(ias,bs,isw,n,m)
        call FouSetLimits(ias,bs,m,isw,k)
      enddo
      deallocate(ias,bs)
9999  return
      end
      subroutine nascs(ias,bs,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ias(3,*),bs(3,*),p(6)
      logical eqiv,eqrv
      data p/6*0./
      m=n+1
      do 2000i=1,n
        do j=1,3
          ias(j,m)=-ias(j,i)
          p(j)=1.-bs(j,i)
        enddo
        call od0do1(p,p,3)
        do j=1,m-1
          if(eqiv(ias(1,m),ias(1,j),3).and.
     1       eqrv(p,bs(1,j),3,.0001)) go to 2000
        enddo
        do j=1,3
          bs(j,m)=p(j)
        enddo
        m=m+1
2000  continue
      n=m-1
      return
      end
      subroutine nascen(ias,bs,isw,n,m)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ias(3,*),bs(3,*),p(6)
      logical   eqiv,eqrv
      data p/6*0./
      m=n+1
      do i=2,NLattVec(KPhase)
        do 2000j=1,n
          do k=1,3
            ias(k,m)=ias(k,j)
            p(k)=bs(k,j)+vt6(k,i,isw,KPhase)
          enddo
          call od0do1(p,p,3)
          do k=1,m-1
            if(eqiv(ias(1,m),ias(1,k),3).and.
     1         eqrv(p,bs(1,k),3,.0001)) go to 2000
          enddo
          do k=1,3
            bs(k,m)=p(k)
          enddo
          m=m+1
2000    continue
      enddo
      m=m-1
      return
      end
      subroutine FouSetLimits(ias,bs,m,isw,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ias(3,*),bs(3,*),idal(:)
      allocatable idal
      allocate(idal(m))
      call SetIntArrayTo(idal,m,0)
      dx=9999.
      do i=1,3
        n1=0
        do j=1,m
          if(iabs(ias(i,j)).ne.1.or.bs(i,j).ne.0.) cycle
          n1=n1+1
        enddo
        pom=float(n1)*CellPar(i,isw,KPhase)/(float(m)*ptstep)
        if(pom.lt.dx) then
          dx=pom
          ismer=i
        endif
      enddo
      nop(isw)=231
      if(ismer.eq.2) nop(isw)=312
      if(ismer.eq.3) nop(isw)=123
      if(NDim(KPhase).gt.3) then
        nd=NDimI(KPhase)
        i=10**nd
        nop(isw)=nop(isw)*i+ifix(float(i)*.456)
        do i=4,NDim(KPhase)
          fourmn(i,isw)=0.
          fourmx(i,isw)=1.
        enddo
      endif
      do i=ismer,ismer+2
        k=mod(i-1,3)+1
        n1=0
        n2=0
        bsmin=9999.
        do j=1,m
          if(idal(j).ne.0) cycle
          n2=n2+1
          if(ias(k,j).ne.1) go to 4100
          if(bs(k,j).ne.0.) go to 4200
          n1=n1+1
          cycle
4100      bsmin=min(bsmin,bs(k,j))
4200      idal(j)=1
        enddo
        if(bsmin.gt.10.) bsmin=0.
        fourmn(k,isw)=bsmin*.5
        fourmx(k,isw)=fourmn(k,isw)+float(n1)/float(n2)
      enddo
      deallocate(idal)
      return
      end
      subroutine FouPeaks(Klic,m81)
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension x456(3),xp(6),stred(3),xpp(6),xold(3),xnew(3),ix(3),
     1          xm(3),ix3(3)
      dimension kmod(:,:),RhoIntMod(:,:),RhoPointMod(:,:),nmod(:),
     1          xmd(:,:),xmod(:,:,:),am(:),ps(:,:),der(:),
     2          waven(:,:),waveo(:,:),x48(:,:),NRhoI(:),
     3          ux48(:,:,:),uy48(:,:,:),tbl(:,:,:),isw48(:)
      character*128 ven,vent
      character*80 t80,format1
      character*27 format2
      character*20 Distance
      character*8 znak
      integer RhoAve,RhoPoint
      logical PointAlreadyPresent,EqIgCase,ZapisByl,Spokojen,EqRV
      allocatable kmod,RhoIntMod,RhoPointMod,nmod,xmd,xmod,am,ps,
     1            der,waven,waveo,x48,NRhoi,ux48,uy48,tbl,isw48
      equivalence (ix3(1),i1),(ix3(2),i2),(ix3(3),i3)
      data DMez/0.8/
      TypeOfSearch=SearchCharge
      ich=0
      nzab=0
      ZapisByl=.false.
      if(m81.le.0) then
        lnr=NextLogicNumber()
        call OpenMaps(lnr,fln(:ifln)//'.m81',nxny,0)
        if(ErrFlag.ne.0) go to 9999
      else
        lnr=m81
      endif
      read(lnr,rec=1,err=9200) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,
     1                         iorien,mapa,nsubs,MameSatelity,
     2                         nonModulated(KPhase)
      do i=1,3
        Pridavat(i)=(xfmn(i).ge.0..and.xfmn(i)-xdf(i)-.001.le.0.).and.
     1              (xfmx(i).le.1..and.xfmx(i)+xdf(i)+.001.ge.1.).and.
     2              nx(i).gt.1
        if(Pridavat(i)) then
          xplus(i)=xdf(i)
        else
          xplus(i)=0.
        endif
      enddo
      do i=1,6
        if(i.le.NDim(KPhase)) then
          cx(i)=smbx6(iorien(i))
        else
          cx(i)=' '
        endif
      enddo
      if(mapa.ge.7) then
        npeaks(1)=10000
        npeaks(2)=0
      endif
      if(IntMethod.eq.1) then
        call FouPeakIntSimpleProlog(IntRad)
      else
        call FouPeakIntProlog(IntRad)
      endif
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_peaks.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1010  read(m40,FormA,end=1011) t80
      if(t80(1:10).ne.'----------'.or.
     1   (LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0.and.
     2    LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0))
     3  then
        do i=1,idel(t80)
          if(t80(i:i).ne.'*') then
            write(ln,FormA) t80(:idel(t80))
            go to 1010
          endif
        enddo
      endif
1011  call CloseIfOpened(m40)
      nsubs=mod(nsubs,10)
      ntu =nx(4)*nx(5)
      ntuv=ntu*nx(6)
      do i=1,3
        if(iorien(i).gt.3) then
          ErrFlag=1
          go to 9999
        endif
      enddo
      n=max(npeaks(1),npeaks(2))
      if(allocated(tbl)) deallocate(tbl,x48,RhoIntArr,RhoPointArr,NRho)
      allocate(tbl(nx(1),nx(2),nx(3)),x48(3,5*n),RhoIntArr(5*n),
     1         isw48(5*n),RhoPointArr(5*n),NRho(5*n))
      if(NDimI(KPhase).gt.0) then
        n=5*n
        if(allocated(kmod)) deallocate(kmod,RhoIntMod,RhoPointMod,nmod,
     1                                 xmd,xmod,NRhoI,ux48,uy48)
        allocate(kmod(n,ntuv),RhoIntMod(n,ntuv),RhoPointMod(n,ntuv),
     1           xmod(3,n,ntuv),nmod(n*ntuv),xmd(6,n*ntuv),
     2           NRhoi(n),ux48(3,kharm,n),uy48(3,kharm,n))
        call SetIntArrayTo(NRhoi,n,0)
        na48max=n
        n=n/5
      else
        na48max=n
      endif
      read(lnr,rec=nmap+2,err=9200) rmax,rmin
      rmax=max(rmax,-rmin)
      format2='(i3,''/'',i3,3f8.4,f10.2,''|'')'
      if(rmax.gt.999000.00) format2(18:22)='e10.3'
      pom=rmax*33.33/float(imax)
      redukce=1.
1025  if(pom.gt.1.) then
        redukce=redukce*.1
        pom=pom*.1
        go to 1025
      endif
      rmax=rmax*redukce
      GridVol=CellVol(nsubs,KPhase)*xdf(1)*xdf(2)*xdf(3)
      eq48=0.
      if(Klic.eq.0) then
        do i=1,3
          l=iorien(i)
          if(xfmx(i)-xfmn(i).gt.xdf(i))
     1      eq48=max(eq48,xdf(i)*CellPar(l,nsubs,KPhase))
        enddo
        eq48=max(.1,eq48)
      else
        do i=1,3
          if(xfmx(i)-xfmn(i).gt.xdf(i))
     1      eq48=max(eq48,xdf(i))
        enddo
      endif
      call OpenFile(46,fln(:ifln)//'.m46','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      nflmx=(nx(3)-2)*ntuv
      if(npeaks(2).ge.1) nflmx=2*nflmx
      if(nflmx.le.0) go to 9999
      call FeFlowChartOpen(-1.,YMinFlowChart,1,nflmx,
     1                     'Peak searching procedure',' ',' ')
      nfl=0
      RhoMax=0.
      do kzap=1,-1,-2
        call newpg(0)
        if(kzap.gt.0) then
          npik=npeaks(1)
          znak='positive'
          if(Patterson.or.NDim(KPhase).gt.4) kharm=0
        else
          npik=npeaks(2)
          znak='negative'
          kharm=0
        endif
        ScPom=float(-kzap)*.01
        if(npik.le.0) cycle
        call newln(2)
        write(lst,'(/''Searching for '',a8,'' peaks - maximum number '',
     1              ''of peaks to be found : '',i3)') znak,npik
        if(redukce.lt..9) then
          call newln(2)
          write(lst,'(/''Scaling factor '',f12.8,'' will be applied'')')
     1      redukce
        endif
        rewind 46
        read(lnr,rec=1,err=9200) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),
     1                           xdf,iorien,mapa,nsubs,MameSatelity,
     2                           nonModulated(KPhase)
        nsubs=mod(nsubs,10)
        rzap=kzap
        irec=1
        do it=1,ntuv
          call RecUnpack(it,ix,nx(4),3)
          do i=1,3
            x456(i)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
          enddo
          do k=1,nx(3)
            irec=irec+1
            read(lnr,rec=irec)((tbl(i,j,k),i=1,nx(1)),j=1,nx(2))
            if(kzap.lt.0)
     1        call RealVectorToOpposite(tbl(1,1,k),tbl(1,1,k),nxny)
          enddo
          RhoMez=0.
          ipik=0
          ix33o=-1
          do 1100ipack=1,nx(1)*nx(2)*nx(3)
            call RecUnpack(ipack,ix3,nx,3)
            do i=1,3
              xm(i)=xfmn(i)+float(ix3(i)-1)*xdf(i)
            enddo
            if(ix3(3).ne.ix33o) then
              call FeFlowChartEvent(nfl,ie)
              if(ie.ne.0) then
                ErrFlag=1
                go to 9100
              endif
              ix33o=ix3(3)
            endif
            t111=FouGetTbl(Tbl,i1,i2,i3)
            t011=FouGetTbl(Tbl,i1-1,i2,i3)
            t211=FouGetTbl(Tbl,i1+1,i2,i3)
            if(t011.gt.t111.or.t211.ge.t111) go to 1100
            t101=FouGetTbl(Tbl,i1,i2-1,i3)
            t121=FouGetTbl(Tbl,i1,i2+1,i3)
            if(t101.gt.t111.or.t121.ge.t111) go to 1100
            t001=FouGetTbl(Tbl,i1-1,i2-1,i3)
            t021=FouGetTbl(Tbl,i1-1,i2+1,i3)
            if(t001.gt.t111.or.t021.ge.t111) go to 1100
            t201=FouGetTbl(Tbl,i1+1,i2-1,i3)
            t221=FouGetTbl(Tbl,i1+1,i2+1,i3)
            if(t201.gt.t111.or.t221.ge.t111) go to 1100
            if(nx(3).le.2) then
              t000=t001
              t002=t001
              t010=t011
              t012=t011
              t020=t021
              t022=t021
              t100=t101
              t102=t101
              t110=t111
              t112=t111
              t120=t121
              t122=t121
              t200=t201
              t202=t201
              t210=t211
              t212=t211
              t220=t221
              t222=t221
            else
              t000=FouGetTbl(Tbl,i1-1,i2-1,i3-1)
              t002=FouGetTbl(Tbl,i1-1,i2-1,i3+1)
              if(t000.gt.t111.or.t002.ge.t111) go to 1100
              t010=FouGetTbl(Tbl,i1-1,i2,i3-1)
              t012=FouGetTbl(Tbl,i1-1,i2,i3+1)
              if(t010.gt.t111.or.t012.ge.t111) go to 1100
              t020=FouGetTbl(Tbl,i1-1,i2+1,i3-1)
              t022=FouGetTbl(Tbl,i1-1,i2+1,i3+1)
              if(t020.gt.t111.or.t022.ge.t111) go to 1100
              t100=FouGetTbl(Tbl,i1,i2-1,i3-1)
              t102=FouGetTbl(Tbl,i1,i2-1,i3+1)
              if(t100.gt.t111.or.t102.ge.t111) go to 1100
              t110=FouGetTbl(Tbl,i1,i2,i3-1)
              t112=FouGetTbl(Tbl,i1,i2,i3+1)
              if(t110.gt.t111.or.t112.ge.t111) go to 1100
              t120=FouGetTbl(Tbl,i1,i2+1,i3-1)
              t122=FouGetTbl(Tbl,i1,i2+1,i3+1)
              if(t120.gt.t111.or.t122.ge.t111) go to 1100
              t200=FouGetTbl(Tbl,i1+1,i2-1,i3-1)
              t202=FouGetTbl(Tbl,i1+1,i2-1,i3+1)
              if(t200.gt.t111.or.t202.ge.t111) go to 1100
              t210=FouGetTbl(Tbl,i1+1,i2,i3-1)
              t212=FouGetTbl(Tbl,i1+1,i2,i3+1)
              if(t210.gt.t111.or.t212.ge.t111) go to 1100
              t220=FouGetTbl(Tbl,i1+1,i2+1,i3-1)
              t222=FouGetTbl(Tbl,i1+1,i2+1,i3+1)
              if(t220.gt.t111.or.t222.ge.t111) go to 1100
            endif
            call vrchol(xm(1),xdf(1),t011,t111,
     1                  t211,xp(iorien(1)),r)
            call vrchol(xm(2),xdf(2),t101,t111,
     1                  t121,xp(iorien(2)),s)
            call vrchol(xm(3),xdf(3),t110,t111,t112,xp(iorien(3)),t)
            do k=1,3
              l=iorien(k)
              if(xp(l).lt.xfmn(k)-xplus(k).or.xp(l).gt.xfmx(k)+xplus(k))
     1          go to 1100
            enddo
            if(klic.eq.0) then
              if(KoincSimple(xp,x48,1,ipik,eq48,pom,nsubs).gt.0)
     1          go to 1100
            endif
            RhoPointR=(r+s+t)*redukce/3.
            if(nx(3).le.2.or.Patterson.or.Mapa.ge.7) then
              RhoMax=max(RhoMax,abs(RhoPointR))
              RhoAve=nint(-100.*RhoPointR)
              RhoPoint=RhoAve
            else
              if(IntMethod.eq.1) then
                call FouPeakIntSimpleMake(Tbl,xp,RhoPointR,RhoAveR)
              else
                call FouPeakIntMake(Tbl,xp,RhoAveR)
              endif
              RhoAveR=RhoAveR*GridVol
              RhoAve=nint(-100.*RhoAveR)
              RhoPoint=nint(-100.*RhoPointR)
              if(TypeOfSearch.eq.SearchCharge) then
                RhoMax=max(RhoMax,abs(RhoAveR))
              else
                RhoMax=max(RhoMax,abs(RhoPointR))
              endif
            endif
            if((TypeOfSearch.eq.SearchCharge.and.RhoAve.lt.RhoMez).or.
     1         (TypeOfSearch.eq.SearchDensity.and.RhoPoint.lt.RhoMez))
     2        call FouInclude(xp,RhoAve,RhoPoint,x48,ipik,npik)
1100      continue
          if(ipik.lt.npik.and.ipik.gt.0) then
            if(TypeOfSearch.eq.SearchCharge) then
              call indexx(ipik,RhoIntArr,NRho)
            else
              call indexx(ipik,RhoPointArr,NRho)
           endif
          endif
          write(46,101) ipik,x456
          na48=ipik
          do n=1,ipik
            j=NRho(n)
            write(46,102)(x48(i,j),i=1,3),
     1                    -float(RhoIntArr(j))/100.*rzap,
     2                    -float(RhoPointArr(j))/100.*rzap
          enddo
        enddo
        if(ntuv.eq.1) then
          kharm=0
          go to 5000
        endif
        rewind 46
        mxnmod=0
        do k=1,ntuv
          read(46,101) nmod(k)
          mxnmod=max(mxnmod,nmod(k))
          do j=1,nmod(k)
            read(46,102)(xmod(i,j,k),i=1,3),RhoIntMod(j,k),
     1                                      RhoPointMod(j,k)
          enddo
          call SetIntArrayTo(kmod(1,k),nmod(k),0)
        enddo
        if(NDim(KPhase).gt.4.or.kzap.eq.-1.or.Patterson) go to 4900
        nlsq=2*kharm+1
        nam=nlsq*(kharm+1)
        allocate(am(nam),ps(nlsq,3),der(nlsq),waven(nlsq,3),
     1           waveo(nlsq,3))
        der(1)=1.
        nm=0
        na48=0
        jp=1
        kp=1
2500    do k=kp,ntuv
          do j=jp,nmod(k)
            if(kmod(j,k).eq.0) go to 2530
          enddo
          jp=1
        enddo
        go to 4800
2530    jp=j
        kp=k
        nm=nm+1
        Spokojen=.false.
        call CopyVek(xmod(1,jp,kp),xnew,3)
        call od0do1(xnew,xnew,3)
        do im=1,5
          n=1
          kmod(jp,kp)=-1
          RhoInt=RhoIntMod(jp,kp)
          RhoPointR=RhoPointMod(jp,kp)
          RhoMin=RhoInt*.5
          call SetRealArrayTo(xmd(1,1),6,0.)
          call SetRealArrayTo(waveo,3*nlsq,0.)
          call CopyVek(xmod(1,jp,kp),xmd(1,1),3)
          call RecUnpack(kp,ix,nx(4),3)
          do i=1,3
            xmd(i+3,1)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
          enddo
          call od0do1(xmd(1,1),xmd(1,1),NDim(KPhase))
          call CopyVek(xmd(1,1),Stred,3)
          do k=kp,ntuv
            do j=1,nmod(k)
              if(kmod(j,k).ne.0.or.RhoIntMod(j,k).lt.RhoMin) cycle
              call CopyVek(xmod(1,j,k),xp,3)
              call RecUnpack(k,ix,nx(4),3)
              do i=1,3
                xp(i+3)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
              enddo
              if(PointAlreadyPresent(xp,xpp,xnew,1,dmax,.true.,dpom,
     1                               isym,icent,nsubs)) then
                n=n+1
                call CopyVek(xpp,xmd(1,n),NDim(KPhase))
                call AddVek(stred,xpp,stred,3)
                if(Spokojen) then
                  RhoInt=RhoInt+RhoIntMod(j,k)
                  RhoPointR=RhoPointR+RhoPointMod(j,k)
                  kmod(j,k)=-1
                endif
              endif
            enddo
          enddo
          u=1./float(n)
          RhoInt=RhoInt*u
          RhoPointR=RhoPointR*u
          do i=1,3
            Stred(i)=Stred(i)*u
          enddo
          if(Spokojen) exit
          call CopyVek(Stred,xnew,3)
          if(im.ne.1) Spokojen=EqRV(xnew,xold,3,.0001).or.im.eq.4
          call CopyVek(xnew,xold,3)
        enddo
        kmodmx=-2
        iter=0
3999    iter=iter+1
        call SetRealArrayTo(ps,3*nlsq,0.)
        call SetRealArrayTo(am,nam,0.)
        do k=1,n
          arg=pi2*(xmd(4,k)-qu(1,1,1,KPhase)*(xmd(1,k)-stred(1))
     1                     -qu(2,1,1,KPhase)*(xmd(2,k)-stred(2))
     2                     -qu(3,1,1,KPhase)*(xmd(3,k)-stred(3)))
          j=0
          do i=1,kharm
            pom=float(i)*arg
            j=j+2
            der(j)=sin(pom)
            der(j+1)=cos(pom)
          enddo
          call FouPeaksSuma(der,xmd(1,k),am,ps,nlsq)
        enddo
        call smi(am,nlsq,ising)
        if(ising.eq.1.or.n*2.lt.ntuv) then
          do i=1,3
            waven(1,i)=stred(i)
          enddo
          iter=0
          go to 4600
        endif
        do i=1,3
          call nasob(am,ps(1,i),waven(1,i),nlsq)
        enddo
        do i=1,3
          stred(i)=waven(1,i)
        enddo
        pom=0.
        do i=1,3
          do j=1,nlsq
            pom=pom+abs(waven(j,i)-waveo(j,i))
            waveo(j,i)=waven(j,i)
          enddo
        enddo
        if(pom.gt..001.and.iter.le.5) go to 3999
4600    if(iter.le.5.and.iter.gt.0) then
          do i=1,3
            xp(i)=waven(1,i)
          enddo
          j=koinc(xp,x48,1,na48,eq48,pom,nsubs,isw48)
          if(j.le.0.and.na48.lt.na48max) then
            if(na48.lt.na48max) then
              na48=na48+1
              do i=1,3
                k=2
                do j=1,kharm
                  ux48(i,j,na48)=waven(k  ,i)
                  uy48(i,j,na48)=waven(k+1,i)
                  k=k+2
                enddo
                x48(i,na48)=waven(1,i)
              enddo
              call od0do1(x48(1,na48),x48(1,na48),3)
              isw48(na48)=nsubs
              RhoIntArr(na48)=nint(-RhoInt*100.)
              RhoPointArr(na48)=nint(-RhoPointR*100.)
              kmodmx=na48
            endif
          endif
        endif
        do k=1,ntuv
          do j=1,nmod(k)
            if(kmod(j,k).eq.-1) kmod(j,k)=kmodmx
          enddo
        enddo
        go to 2500
4800    if(na48.gt.1) then
          if(TypeOfSearch.eq.SearchCharge) then
            call indexx(na48,RhoIntArr,NRho)
          else
            call indexx(na48,RhoPointArr,NRho)
          endif
        else
          NRho(1)=1
        endif
4900    do i=1,na48
          NRhoi(NRho(i))=i
        enddo
        iz=1
        m=(nx(4)-1)/3+1
        kk=mod(nx(4)-1,3)+1
        if(kzap.eq.-1.or.Patterson) then
          RhoMez=0
          ipik=0
        endif
        x456(3)=xfmn(6)
        do iv=1,nx(6)
          x456(2)=xfmn(5)
          do iu=1,nx(5)
            x456(1)=xfmn(4)
            do it=1,m
              if(it.ne.m) then
                n=2
              else
                n=kk-1
              endif
              if(line.gt.mxline-10) call newpg(0)
              call newln(3)
              write(lst,FormA128)
              if(NDim(KPhase).eq.4) then
                write(ven,104) cx(4),x456(1)
              else if(NDim(KPhase).eq.5) then
                write(ven,105) cx(4),x456(1),cx(5),x456(2)
              else
               write(ven,106) cx(4),x456(1),cx(5),x456(2),cx(6),x456(3)
              endif
              if(n.gt.0) then
                if(NDim(KPhase).eq.4) then
                  write(ven(43:),104) cx(4),x456(1)+xdf(4)
                else if(NDim(KPhase).eq.5) then
                  write(ven(43:),105) cx(4),x456(1)+xdf(4),cx(5),
     1                  x456(2)+xdf(5)
                else
                  write(ven(43:),106) cx(4),x456(1)+xdf(4),cx(5),
     1                  x456(2)+xdf(5),cx(6),x456(3)+xdf(6)
                endif
                if(n.gt.1) then
                  if(NDim(KPhase).eq.4) then
                    write(ven(85:),104) cx(4),x456(1)+2.*xdf(4)
                  else if(NDim(KPhase).eq.5) then
                    write(ven(85:),105) cx(4),x456(1)+2.*xdf(4),cx(5)
     1                   ,x456(2)+2.*xdf(5)
                  else
                    write(ven(85:),106) cx(4),x456(1)+2.*xdf(4),cx(5)
     1                   ,x456(2)+2.*xdf(5),cx(6),x456(3)+2.*xdf(6)
                  endif
                endif
              endif
              write(lst,FormA128) ven
              write(ven,108)
              if(n.gt.0) write(ven(43:),108)
              if(n.gt.1) write(ven(85:),108)
              write(lst,FormA128) ven
              do j=1,mxnmod
                call newln(1)
                ven=' '
                ii=1
                do k=iz,iz+n
                  if(j.gt.nmod(k)) go to 4940
                  if(NDim(KPhase).eq.4.and.kzap.eq.1.and..not.Patterson)
     1              then
                    if(kmod(j,k).le.0) then
                      i1=0
                    else
                      i1=NRhoi(kmod(j,k))
                    endif
                  else
                    i1=0
                    if(kzap.eq.-1.or.Patterson) then
                      ip=kzap*nint(-RhoIntMod(j,k)*100.)
                      ipp=kzap*nint(-RhoPointMod(j,k)*100.)
                      if(ip.lt.RhoMez)
     1                  call FouInclude(xmod(1,j,k),ip,ipp,x48,ipik,
     2                                  npik)
                    endif
                  endif
                  write(ven(ii:),format2) j,i1,(xmod(i,j,k),i=1,3),
     1                                    RhoIntMod(j,k)
4940              ii=ii+42
                enddo
                write(lst,FormA128) ven
              enddo
              iz=iz+n+1
              x456(1)=x456(1)+3.*xdf(4)
            enddo
            x456(2)=x456(2)+xdf(5)
          enddo
          x456(3)=x456(3)+xdf(6)
        enddo
5000    call newln(3)
        write(lst,'(/''The list of '',a8,'' peaks written to the m40 '',
     1              '' file''/)') znak
        if(NDim(KPhase).gt.3) then
          if(kzap.eq.1.and..not.Patterson) then
            if(kharm.ne.0) then
              call newln(1)
              write(lst,'(''These peaks were successfully interpreted''
     1                   ,'' with '',i1,'' harmonic wave(s)'')') kharm
            endif
          else
            if(ipik.lt.npik) then
              if(TypeOfSearch.eq.SearchCharge) then
                call indexx(ipik,RhoIntArr,NRho)
              else
                call indexx(ipik,RhoPointArr,NRho)
              endif
            endif
          endif
        endif
        if(Patterson) then
          if(.not.allocated(Atom).or.MxAtAll.le.1)
     1      call ReallocateAtoms(100)
          vent='        x      y      z          rho   rel'//
     1         '   distance to origin'
          format1='(i3,''.'',3f7.4,f12.1,i5)'
          NAtIndLen(1,KPhase)=2
          NAtCalc=2
          NAtAll=2
          call SetBasicKeysForAtom(1)
          call SetBasicKeysForAtom(2)
          Atom(1)='Origin'
          Atom(2)='Itself'
          call specat
        else
          vent='        x      y      z     charge      rho   rel'
          format1='(i3,''.'',3f7.4,f8.2,f10.2,i6)'
        endif
        vent=vent(1:65)//vent(1:65)
        n2=0
        if(line.gt.mxline-12) call newpg(0)
5100    n1=n2+1
        n2=min(n2+2*(mxline-line-2),na48)
        n=n2-n1+1
        nn=(n-1)/2+1
        call newln(nn+2)
        write(lst,FormA128) vent
        write(lst,FormA128)
        if(Patterson) call SetRealArrayTo(x,3,0.)
        do ip=0,nn-1
          ven=' '
          l=1
          ll=n1+ip
          do j=1,2
            if(ll.gt.n2) cycle
            ii=NRho(ll)
            if(TypeOfSearch.eq.SearchCharge) then
              RhoAveR=float(-RhoIntArr(ii)*kzap)*.01
            else
              RhoAveR=float(-RhoPointArr(ii)*kzap)*.01
            endif
            if(klic.eq.0) then
              if(Patterson) then
                call CopyVek(x48(1,ii),x(1,2),3)
                call DistForOneAtom(2,20.,1,0)
                do i=1,ndist
                  m=ipord(i)
                  if(.not.EqIgCase(adist(m),'Itself')) then
                    write(Distance,'(f12.3)') ddist(m)
                    go to 5220
                  endif
                enddo
                Distance='        >20'
              else
                i=KoincM40(x48(1,ii),eq48,pom,nsubs)
              endif
            else
              i=0
            endif
5220        if(Patterson) then
              write(ven(l:),format1) ll,(x48(m,ii),m=1,3),RhoAveR,
     1                               nint(RhoAveR*999./RhoMax)
              ven=ven(:idel(ven))//'   '//Distance(:idel(Distance))
            else

              write(ven(l:),format1) ll,(x48(m,ii),m=1,3),
     1                               ScPom*float(RhoIntArr(ii)),
     2                               ScPom*float(RhoPointArr(ii)),
     3                               nint(RhoAveR*999./RhoMax)
              if(i.gt.0) ven=ven(:idel(ven))//' ='//atom(i)
            endif
            l=l+65
            ll=ll+nn
          enddo
          write(lst,FormA128) ven
        enddo
        if(n2.lt.na48) then
          call newpg(0)
          go to 5100
        endif
        ZapisByl=.true.
        if(kzap.lt.0) then
          Cislo='minima'
        else
          Cislo='maxima'
        endif
        write(ln,109) Cislo(1:6)
        write(ln,'(3i5)') nsubs,KPhase,Radiation(KDatBlock)
        do i=1,na48
          if(kzap.eq.1) then
            znak='max'
          else
            znak='min'
          endif
          write(znak(4:6),'(i3)') i
          call zhusti(znak)
          ii=NRho(i)
          write(ln,'(a8,2i3,4x,4f9.6,6x,3i1,3i3)') znak,1,1,1.,
     1         (x48(j,ii),j=1,3),0,0,0,0,kharm,0
          h=-float(RhoIntArr(ii))*.01
          if(h.gt.999000.00) then
            t80='(e9.3)'
          else
            t80='(3f9.2)'
          endif
          write(ln,t80) -float(RhoPointArr(ii))*.01,
     1                  -float(RhoIntArr(ii))*.01
          do j=1,kharm
            write(ln,103)(ux48(k,j,ii),k=1,3),(uy48(k,j,ii),k=1,3)
          enddo
          if(kharm.gt.0) write(ln,103) 0.
        enddo
      enddo
9100  call FeFlowChartRemove
      if(ZapisByl) write(ln,'(72(''-''))')
      call CloseIfOpened(ln)
      call MoveFile(fln(:ifln)//'_peaks.tmp',fln(:ifln)//'.m40')
      go to 9999
9200  call FeReadError(lnr)
      ErrFlag=1
9999  call DeleteFile(fln(:ifln)//'.m46')
      call CloseIfOpened(m40)
      if(allocated(tbl)) deallocate(tbl,x48,isw48,RhoIntArr,RhoPointArr,
     1                              NRho)
      if(allocated(kmod)) deallocate(kmod,RhoIntMod,RhoPointMod,nmod,
     1                               xmd,xmod,NRhoI,ux48,uy48)
      if(allocated(am)) deallocate(am,ps,der,waven,waveo)
      call CloseIfOpened(ln)
      if(m81.eq.0) call CloseIfOpened(lnr)
      if(IntMethod.ne.1) call FouPeakIntEpilog
      return
101   format(i5,3f9.6)
102   format(3f9.6,2e15.6)
103   format(6f9.6)
104   format(16x,a2,'=',f6.3,17x)
105   format(11x,2(a2,'=',f6.3,1x),11x)
106   format( 6x,3(a2,'=',f6.3,1x), 6x)
108   format('peak/atom    x       y       z       rho |')
109   format(26('-'),3x,'Fourier ',a6,3x,26('-'))
      end
      function FouGetTbl(Tbl,i1,i2,i3)
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension Tbl(*),xp(3)
      i1p=i1
      if(Pridavat(1)) then
        if(i1.eq.nx(1)+1) then
          i1p=1
        else if(i1.eq.0) then
          i1p=nx(1)
        endif
      endif
      i2p=i2
      if(Pridavat(2)) then
        if(i2.eq.nx(2)+1) then
          i2p=1
        else if(i2.eq.0) then
          i2p=nx(2)
        endif
      endif
      i3p=i3
      if(Pridavat(3)) then
        if(i3.eq.nx(3)+1) then
          i3p=1
        else if(i3.eq.0) then
          i3p=nx(3)
        endif
      endif
      if(i1p.ge.1.and.i1p.le.nx(1).and.
     1   i2p.ge.1.and.i2p.le.nx(2).and.
     2   i3p.ge.1.and.i3p.le.nx(3)) then
        i=i1p+(i2p-1)*nx(1)+(i3p-1)*nx(1)*nx(2)
        FouGetTbl=Tbl(i)
      else
        xp(iorien(1))=xfmn(1)+(i1p-1)*xdf(1)
        xp(iorien(2))=xfmn(2)+(i2p-1)*xdf(2)
        xp(iorien(3))=xfmn(3)+(i3p-1)*xdf(3)
        FouGetTbl=FouExMap(Tbl,xp,ich)
      endif
      return
      end
      subroutine FouPeakInt
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer MezD(3)
      real DMaxPolar(:,:),Tbl(*),x(3),dx(3),dxo(3),xp(3)
      allocatable DMaxPolar
      save DMaxPolar,DMez,DMezStep,MezD,MxStep
      data delta/.01/
      entry FouPeakIntProlog(DMezIn)
      DMez=DMezIn
      DMezStep=DMez*Delta
      MxStep=nint(1./Delta)
      do i=1,3
        l=iorien(i)
        MezD(i)=DMez*rcp(l,nsubs,KPhase)/xdf(i)+1.
      enddo
      allocate(DMaxPolar(0:72,0:36))
      go to 9999
      entry FouPeakIntMake(Tbl,x,Rho)
      da=5.*ToRad
      chi=0.
      do ichi=0,36
        snc=sin(chi)
        csc=cos(chi)
        phi=0.
        do iphi=0,72
          snp=sin(phi)
          csp=cos(phi)
          dxo(1)=DMezStep*csp*snc
          dxo(2)=DMezStep*snp*snc
          dxo(3)=DMezStep*csc
          call MultM(TrToOrthoI(1,nsubs,KPhase),dxo,dx,3,3,1)
          xp=x
          den2=FouExMap(Tbl,xp,ich)
          xp=xp+dx
          den3=FouExMap(Tbl,xp,ich)
          xp=xp+dx
          den4=FouExMap(Tbl,xp,ich)
          do i=2,MxStep
            den1=den2
            den2=den3
            den3=den4
            xp=xp+dx
            den4=FouExMap(Tbl,xp,ich)
            b=.5*(den3-den1)
            c=den2
            a=-den2+.5*(den3+den1)
            if(den3.lt.0.) then
              det=b**2-4.*a*c
              if(det.gt.0.) then
                if(a.eq.0.) then
                  DMaxPolar(iphi,ichi)=-c/b
                else
                  bb=.5*(-b+sqrt(Det))/a
                  aa=.5*(-b-sqrt(Det))/a
                  DMaxPolar(iphi,ichi)=aa
                endif
              else
                DMaxPolar(iphi,ichi)=0.
              endif
              go to 1500
c            else if(den3.gt.den2*1.002.and.den4.gt.den3*1.002) then
            else if(den3.gt.den2.and.den4.gt.den3) then
              if(a.ne.0.) then
                DMaxPolar(iphi,ichi)=-.5*b/a
              else
                DMaxPolar(iphi,ichi)=0.
              endif
              go to 1500
            endif
          enddo
          DMaxPolar(iphi,ichi)=DMez
          go to 1600
1500      DMaxPolar(iphi,ichi)=(float(i)+DMaxPolar(iphi,ichi))*
     1                         DMezStep
1600      phi=phi+da
        enddo
        chi=chi+da
      enddo
      Rho=0.
      do id1=-MezD(1),MezD(1)
        io1=iorien(1)
        dx(iorien(1))=float(id1)*xdf(1)
        do id2=-MezD(2),MezD(2)
          dx(iorien(2))=float(id2)*xdf(2)
          do id3=-MezD(3),MezD(3)
            dx(iorien(3))=float(id3)*xdf(3)
            call MultM(TrToOrtho(1,nsubs,KPhase),dx,dxo,3,3,1)
            D=sqrt(scalmul(dxo,dxo))
            if(D.gt..001) then
              pom=dxo(3)/D
              if(abs(pom).lt.1.) then
                chi=acos(pom)/ToRad
                pom=1./sqrt(1.-pom**2)
                if(abs(dxo(1)).gt..001.and.abs(dxo(2)).gt..001.and.
     1            pom.gt..001) then
                  phi=atan2(dxo(2),dxo(1))/ToRad
                  if(phi.lt.0.) phi=phi+360.
                else
                  phi=0.
                endif
              else
                if(pom.gt.0.) then
                  chi=0.
                  phi=0.
                else
                  chi=180.
                  phi=0.
                endif
              endif
              iphi=nint(phi/5.)
              ichi=nint(chi/5.)
              if(D.gt.DMaxPolar(iphi,ichi)) cycle
            endif
            xp=x+dx
            pom=FouExMap(tbl,xp,ich)
            if(ich.eq.0) Rho=Rho+pom
          enddo
        enddo
      enddo
      go to 9999
      entry FouPeakIntEpilog
      if(allocated(DMaxPolar)) deallocate(DMaxPolar)
9999  return
      end
      subroutine FouPeakIntSimple
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer MezD(3)
      real x(*),xp(3),xd(3),xdp(3),Tbl(*)
      save MezD,DMez
      entry FouPeakIntSimpleProlog(DMezIn)
      DMez=DMezIn
      do i=1,3
        l=iorien(i)
        MezD(i)=DMez*rcp(l,nsubs,KPhase)/xdf(i)+1.
      enddo
      go to 9999
      entry FouPeakIntSimpleMake(Tbl,x,RhoPointR,Rho)
      Rho=0.
      do id1=-MezD(1),MezD(1)
        io1=iorien(1)
        xp(iorien(1))=x(iorien(1))+float(id1)*xdf(1)
        do id2=-MezD(2),MezD(2)
          xp(iorien(2))=x(iorien(2))+float(id2)*xdf(2)
          do id3=-MezD(3),MezD(3)
            xp(iorien(3))=x(iorien(3))+float(id3)*xdf(3)
            do k=1,3
              xd(k)=xp(k)-x(k)
            enddo
            call Multm(MetTens(1,nsubs,KPhase),xd,xdp,3,3,1)
            D=sqrt(scalmul(xd,xdp))
            if(D.le.DMez) then
              pom=FouExMap(tbl,xp,ich)
              if(D.gt..3.and.pom.gt.RhoPointR*.8.or.
     1           pom.lt.0.) cycle
              if(ich.eq.0) Rho=Rho+pom
            endif
          enddo
        enddo
      enddo
9999  return
      end
      function FouExMap(InputMap,xx,ich)
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension xx(*),xp(3),xpt(3),xd(3),n(3),x1(3)
      logical EqRV0
      real InputMap(*)
      nxnynz=nxny*nx(3)
      FouExMap=0.
      ich=0
      do i=1,NSymmN(KPhase)
        call multm(rm(1,i,nsubs,KPhase),xx,x1,3,3,1)
        call AddVek(x1,s6(1,i,nsubs,KPhase),x1,3)
        do 1500l=1,NLattVec(KPhase)
          call AddVek(x1,vt6(1,l,nsubs,KPhase),xp,3)
          do j=1,3
            xpt(j)=xp(iorien(j))
1100        if(xpt(j).ge.xfmn(j)-xplus(j)) go to 1200
            xpt(j)=xpt(j)+1.
            go to 1100
1200        if(xpt(j).gt.xfmx(j)+xplus(j)) then
              xpt(j)=xpt(j)-1.
              go to 1200
            endif
            if(xpt(j).lt.xfmn(j)-xplus(j)) go to 1500
          enddo
          do j=1,3
            xd(j)=(xpt(j)-xfmn(j))/xdf(j)+1.
            n(j)=xd(j)
            xd(j)=xd(j)-float(n(j))
            if(Pridavat(j)) then
              if(n(j).eq.0) then
                n(j)=nx(j)
              else if(n(j).eq.nx(j)+1) then
                n(j)=1
              endif
            endif
          enddo
          go to 2000
1500    continue
      enddo
      ich=1
      go to 9999
2000  i000=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
      if(EqRV0(xd,3,.001)) then
        FouExMap=InputMap(i000)
        go to 9999
      endif
      if(i000.gt.nxnynz) then
        call FouGetClosestPoint(i000,ich)
        if(ich.ne.0) go to 9999
      endif
      i100=i000+1
      if(i100.gt.nxnynz) then
        call FouGetClosestPoint(i100,ichp)
        if(ichp.ne.0) i100=i000
      endif
      i010=i000+nx(1)
      if(i010.gt.nxnynz) then
        call FouGetClosestPoint(i010,ichp)
        if(ichp.ne.0) i010=i000
      endif
      i110=i000+nx(1)+1
      if(i110.gt.nxnynz) then
        call FouGetClosestPoint(i110,ichp)
        if(ichp.ne.0) i110=i000
      endif
      i001=i000+nxny
      if(i001.gt.nxnynz) then
        call FouGetClosestPoint(i001,ichp)
        if(ichp.ne.0) i001=i000
      endif
      i101=i100+nxny
      if(i101.gt.nxnynz) then
        call FouGetClosestPoint(i101,ichp)
        if(ichp.ne.0) i101=i100
      endif
      i011=i010+nxny
      if(i011.gt.nxnynz) then
        call FouGetClosestPoint(i011,ichp)
        if(ichp.ne.0) i011=i010
      endif
      i111=i110+nxny
      if(i111.gt.nxnynz) then
        call FouGetClosestPoint(i111,ichp)
        if(ichp.ne.0) i111=i110
      endif
      f000=InputMap(i000)
      f100=InputMap(i100)
      f010=InputMap(i010)
      f110=InputMap(i110)
      f001=InputMap(i001)
      f101=InputMap(i101)
      f011=InputMap(i011)
      f111=InputMap(i111)
      f00=(f100-f000)*xd(1)+f000
      f01=(f101-f001)*xd(1)+f001
      f10=(f110-f010)*xd(1)+f010
      f11=(f111-f011)*xd(1)+f011
      f0=(f10-f00)*xd(2)+f00
      f1=(f11-f01)*xd(2)+f01
      FouExMap=(f1-f0)*xd(3)+f0
9999  return
      end
      subroutine FouGetClosestPoint(imp,ich)
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer ix(3),n(3)
      real x(3),xm(3),xp(3),xq(3)
      call RecUnpack(imp,ix,nx,3)
      do i=1,3
        x(iorien(i))=xfmn(i)+float(ix(i)-1)*xdf(i)
      enddo
      do i=1,NSymmN(KPhase)
        call multm(rm(1,i,nsubs,KPhase),x,xp,3,3,1)
        call AddVek(xp,s6(1,i,nsubs,KPhase),xp,3)
        do 1500l=1,NLattVec(KPhase)
          call AddVek(xp,vt6(1,l,nsubs,KPhase),xq,3)
          do j=1,3
            xm(j)=xq(iorien(j))
1100        if(xm(j).ge.xfmn(j)-xplus(j)) go to 1200
            xm(j)=xm(j)+1.
            go to 1100
1200        if(xm(j).gt.xfmx(j)+xplus(j)) then
              xm(j)=xm(j)-1.
              go to 1200
            endif
            if(xm(j).lt.xfmn(j)-xplus(j)) go to 1500
          enddo
          do j=1,3
            xm(j)=(xm(j)-xfmn(j))/xdf(j)+1.
            n(j)=nint(xm(j))
            if(Pridavat(j)) then
              if(n(j).eq.0) then
                n(j)=nx(j)
              else if(n(j).eq.nx(j)+1) then
                n(j)=1
              endif
            endif
          enddo
          go to 2000
1500    continue
      enddo
      go to 9000
2000  imp=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
9000  ich=0
9999  return
      end
      subroutine FouInclude(u,RhoInt,RhoPoint,XRho,n,npik)
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension u(*),XRho(3,*)
      integer RhoInt,RhoPoint
      if(n.lt.npik) then
        n=n+1
        NRho(n)=n
        kam=n
      else
        if(TypeOfSearch.eq.SearchCharge) then
          do i=1,n
            if(RhoInt.lt.RhoIntArr(NRho(i))) go to 1200
          enddo
        else
          do i=1,n
            if(RhoPoint.lt.RhoPointArr(NRho(i))) go to 1200
          enddo
        endif
        go to 9999
1200    kam=NRho(n)
        do j=n,i+1,-1
          NRho(j)=NRho(j-1)
        enddo
        NRho(i)=kam
      endif
      RhoPointArr(kam)=RhoPoint
      RhoIntArr(kam)=RhoInt
      call CopyVek(u,XRho(1,kam),3)
      if(n.ge.npik) then
        if(TypeOfSearch.eq.SearchCharge) then
          call indexx(n,RhoIntArr,NRho)
          RhoMez=RhoIntArr(NRho(npik))
        else
          call indexx(n,RhoPointArr,NRho)
          RhoMez=RhoPointArr(NRho(npik))
        endif
      endif
9999  return
      end
      function FouGetMapValue(t,xx)
      parameter (nn=10)
      dimension xp(3),t(0:2,0:2,0:2),dr(nn,0:2,0:2,0:2),px(nn),sol(nn),
     1          rx((nn*(nn+1))/2),xx(3)
      logical :: First = .true.
      save dr
      if(First) then
        do i1=0,2
          xp(1)=i1-1
          do i2=0,2
            xp(2)=i2-1
            do i3=0,2
              xp(3)=i3-1
              n=1
              dr(1,i1,i2,i3)=1.
              do j1=1,3
                n=n+1
                dr(n,i1,i2,i3)=xp(j1)
              enddo
              do j1=1,3
                do j2=j1,3
                  n=n+1
                  dr(n,i1,i2,i3)=xp(j1)*xp(j2)
                enddo
              enddo
            enddo
          enddo
        enddo
        First=.false.
      endif
      call SetRealArrayTo(px,nn,0.)
      call SetRealArrayTo(rx,(nn*(nn+1))/2,0.)
      do i1=0,2
        xp(1)=i1-1
        do i2=0,2
          xp(2)=i2-1
          do i3=0,2
            xp(3)=i3-1
            tp=t(i1,i2,i3)
            n=0
            do i=1,nn
              px(i)=px(i)+dr(i,i1,i2,i3)*tp
              do j=1,i
                n=n+1
                rx(n)=rx(n)+dr(i,i1,i2,i3)*dr(j,i1,i2,i3)
              enddo
            enddo
          enddo
        enddo
      enddo
      n=0
      do i=1,nn
        n=n+i
        sol(i)=1./sqrt(rx(n))
      enddo
      call znorm(rx,sol,nn)
      call smi(rx,nn,ising)
      call znorm(rx,sol,nn)
      if(ising.eq.0) then
        call nasob(rx,px,sol,nn)
        n=1
        px(1)=1.
        do j1=1,3
          n=n+1
          px(n)=xx(j1)
        enddo
        do j1=1,3
          do j2=j1,3
            n=n+1
            px(n)=xx(j1)*xx(j2)
          enddo
        enddo
        FouGetMapValue=VecOrtScal(sol,px,nn)
      else
        FouGetMapValue=0.
      endif
      return
      end
      subroutine FouPeaksSuma(der,x,am,ps,n)
      include 'fepc.cmn'
      dimension der(n),x(*),am(*),ps(n,*)
      im=0
      do i=1,n
        deri=der(i)
        if(deri.eq.0.) then
          im=im+i
          cycle
        endif
        do j=1,i
          im=im+1
          derj=der(j)
          if(derj.eq.0) cycle
          am(im)=am(im)+deri*derj
        enddo
        do j=1,3
          ps(i,j)=ps(i,j)+deri*x(j)
        enddo
      enddo
      return
      end
      subroutine FouSetCommands(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      data LastListek/0/
      call RewriteTitle('Fourier-commands')
      call FouOpenCommands
      if(ErrFlag.ne.0) go to 9999
      XdQuestFou=500.
      call FeKartCreate(-1.,-1.,XdQuestFou,16,'Fourier commands',0,0)
      call FeCreateListek('Basic',1)
      KartIdBasic=KartLastId
      call FouBasicCommandsMake(KartIdBasic)
      call FeCreateListek('Scope',1)
      KartIdScope=KartLastId
      call FouReadScopeMake(KartIdScope)
      call FeCreateListek('Peaks',1)
      KartIdPeaks=KartLastId
      call FouPeaksCommandsMake(KartIdPeaks)
      if(ContourCallFourier) then
        if(LastListek.eq.0) LastListek=1
      else
        LastListek=1
      endif
      call FeCompleteKart(LastListek)
2500  ErrFlag=0
3000  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsUpdate
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeUpdate
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsCheck
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeCheck
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsUpdate
          LastListek=1
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeUpdate
          LastListek=2
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsUpdate
          LastListek=3
        endif
      endif
      call FeDestroyKart
      if(ich.ne.0) go to 9999
      if(ContourCallFourier) then
        Klic=1
      else
        Klic=0
      endif
      call FouRewriteCommands(Klic)
9999  return
      end
      subroutine FouOpenCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) return
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultFourier
      OrCode=-333
      call SetRealArrayTo(xrmn,6,-333.)
      call NactiFourier
      if(ptname.eq.'[neco]') then
        write(ptname,101) ptx
        call ZdrcniCisla(ptname,3)
      endif
      call FouMakeScopeType
9999  return
101   format(3f10.6)
      end
      subroutine FouRewriteCommands(Klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      call WriteKeys('fourier')
      if(ScopeType.eq.1.or.ScopeType.eq.3) then
        ip=4
      else
        ip=1
      endif
      do i=ip,NDim(KPhase)
        if(xrmn(i).gt.-300.) then
          write(t80,101) xrmn(i),xrmx(i),dd(i)
          call ZdrcniCisla(t80,3)
        else
          t80='0 1 0.1'
        endif
        if(i.gt.3.and.t80.eq.'0 1 0.1') cycle
        if(NDim(KPhase).eq.3) then
          t80='  '//NactiKeywords(nCmdxlim+i-1)(1:4)//' '//
     1        t80(:idel(t80))
        else
          t80='  '//NactiKeywords(nCmdx1lim+i-1)(1:5)//' '//
     1        t80(:idel(t80))
        endif
        write(55,FormA1)(t80(j:j),j=1,idel(t80))
      enddo
      if(ScopeType.eq.3) then
        t80='  '//
     1      NactiKeyWords(nCmdcenter)(:idel(NactiKeyWords(nCmdcenter)))
     2    //' '//ptname(:idel(ptname))
        write(55,FormA1)(t80(j:j),j=1,idel(t80))
        write(t80,101) pts
        call ZdrcniCisla(t80,3)
        t80='  '//
     1      NactiKeywords(nCmdscope)(:idel(NactiKeyWords(nCmdscope)))//
     2      ' '//t80(:idel(t80))
        write(55,FormA1)(t80(j:j),j=1,idel(t80))
      endif
      write(55,'(''end fourier'')')
      call DopisKeys(Klic)
      call RewriteTitle(' ')
      return
101   format(3f10.6)
      end
      subroutine FouBasicCommands
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension MapTypeEnable(9)
      character*80  Veta
      integer RolMenuSelectedQuest
      logical CrwLogicQuest
      save nEdwMin,nEdwMax,nEdwOmit,nEdwComp,nEdwUiso,nCrwOmit,nCrwDiff,
     1     nCrwUseWeight,nCrwFract,nCrwSinLim,nLblOmit,nLblSinLim,
     2     nRolMenuMapType,niso,UIso
      entry FouBasicCommandsMake(id)
      il=1
      tpom=5.
      Veta='%Map type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=0.
      do i=1,9
        dpom=max(dpom,FeTxLength(MapType(i)))
        if(.not.ChargeDensities.and.i.ge.7.and.i.le.8) then
          MapTypeEnable(i)=0
        else
          MapTypeEnable(i)=1
        endif
      enddo
      dpom=dpom+EdwYd+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuMapType=RolMenuLastMade
      call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,MapType,
     1                            MapTypeEnable,9,NacetlInt(nCmdmapa))
      if(lite(KPhase).eq.0) then
        Veta='U(iso)'
        niso=nCmdUOverAll
      else
        Veta='B(iso)'
        niso=nCmdBOverAll
      endif
      Uiso=NacetlReal(niso)
      tpom=xpom+dpom+20.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwUiso=EdwLastMade
      if(NacetlInt(nCmdmapa).ge.1.and.NacetlInt(nCmdmapa).le.3)
     1  call FeQuestRealEdwOpen(nEdwUiso,Uiso,.false.,.false.)
      il=il+1
      Veta='%Omit not-matching reflections'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwOmit=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlReal(nCmdvyber).gt.0.)
      il=il+1
      Veta='%Reflections with F(obs)>'
      xpome=tpom+FeTxLengthUnder(Veta)+10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwOmit=EdwLastMade
      tpome=xpome+dpom+10.
      Veta='*F(calc) will be omitted'
      call FeQuestLblMake(id,tpome,il,Veta,'L','N')
      nLblOmit=LblLastMade
      call FeQuestLblOff(LblLastMade)
      if(CrwLogicQuest(nCrwOmit)) then
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdvyber),
     1                          .false.,.false.)
        call FeQuestLblOn(nLblOmit)
      endif
      il=il+1
      Veta='%Use weighting of reflections'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwUseWeight=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdUseWeight).gt.0)
      il=il+1
      Veta='A%pply sin(th)/lambda limits'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwSinLim=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdUseSinLim).gt.0)
      il=il+1
      Veta='sin(th)/lambda'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblSinLim=LblLastMade
      call FeQuestLblOff(LblLastMade)
      tpome=tpom+FeTxLength(Veta)+20.
      Veta='mi%n.'
      xpome=tpome+FeTxLength(Veta)+10.
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwMin=EdwLastMade
      Veta='ma%x.'
      tpome=xpome+dpom+20.
      xpome=tpome+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwMax=EdwLastMade
      if(CrwLogicQuest(nCrwSinLim)) then
        call FeQuestLblOn(nLblSinLim)
        call FeQuestRealEdwOpen(nEdwMin,NacetlReal(nCmdsnlmn),.false.,
     1                          .false.)
        call FeQuestRealEdwOpen(nEdwMax,NacetlReal(nCmdsnlmx),.false.,
     1                          .false.)
      endif
      if(NTwin.gt.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        call FeQuestLblMake(id,XdQuestFou*.5,il,'Correction of '//
     1                      'F(obs) for twinning','C','B')
        il=il+1
        pom=50.
        Veta='%Difference'
        tpom=XdQuestFou*.5-pom-FeTxLengthUnder(Veta)
        xpom=tpom-CrwgXd-10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,0,1)
        xpom=XdQuestFou*.5+pom
        tpom=xpom+CrwgXd+10.
        nCrwDiff=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      NacetlInt(nCmdTMethod).eq.SubstMethod)
        call FeQuestCrwMake(id,tpom,il,xpom,il,'Fr%action','L',CrwgXd,
     1                      CrwgYd,0,1)
        nCrwFract=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      NacetlInt(nCmdTMethod).ne.SubstMethod)
      endif
      if(NComp(KPhase).gt.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Composite part no.'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwComp=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdnsubs),.false.)
      endif
      go to 9999
      entry FouBasicCommandsCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwOmit) then
        if(CrwLogicQuest(nCrwOmit)) then
          call FeQuestRealEdwOpen(nEdwOmit,NacetlReal(nCmdvyber),
     1                            .false.,.false.)
          call FeQuestLblOn(nLblOmit)
        else
          call FeQuestEdwClose(nEdwOmit)
          call FeQuestLblOff(nLblOmit)
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSinLim) then
        if(CrwLogicQuest(nCrwSinLim)) then
          call FeQuestLblOn(nLblSinLim)
          call FeQuestRealEdwOpen(nEdwMin,NacetlReal(nCmdsnlmn),.false.,
     1                            .false.)
          call FeQuestRealEdwOpen(nEdwMax,NacetlReal(nCmdsnlmx),.false.,
     1                            .false.)
        else
          call FeQuestLblOff(nLblSinLim)
          call FeQuestEdwClose(nEdwMin)
          call FeQuestEdwClose(nEdwMax)
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuMapType) then
        NacetlInt(nCmdMapa)=RolMenuSelectedQuest(nRolMenuMapType)
        if(NacetlInt(nCmdmapa).ge.1.and.NacetlInt(nCmdmapa).le.3) then
          call FeQuestRealEdwOpen(nEdwUiso,Uiso,.false.,.false.)
        else
          Uiso=DefRealFour(niso)
          call FeQuestEdwClose(nEdwUiso)
        endif
      endif
      go to 9999
      entry FouBasicCommandsUpdate
      NacetlInt(nCmdMapa)=RolMenuSelectedQuest(nRolMenuMapType)

      if(NacetlInt(nCmdmapa).ge.1.and.NacetlInt(nCmdmapa).le.3) then
        call FeQuestRealFromEdw(nEdwUiso,NacetlReal(niso))
      else
        NacetlReal(niso)=DefRealFour(niso)
      endif
      if(CrwLogicQuest(nCrwUseWeight)) then
        NacetlInt(nCmdUseWeight)=1
      else
        NacetlInt(nCmdUseWeight)=0
      endif
      call FeQuestRealFromEdw(nEdwMin,NacetlReal(nCmdsnlmn))
      call FeQuestRealFromEdw(nEdwMax,NacetlReal(nCmdsnlmx))
      if(CrwLogicQuest(nCrwOmit)) then
        call FeQuestRealFromEdw(nEdwOmit,NacetlReal(nCmdvyber))
      else
        NacetlReal(nCmdvyber)=0
      endif
      if(CrwLogicQuest(nCrwSinLim)) then
        NacetlInt(nCmdUseSinLim)=1
        call FeQuestRealFromEdw(nEdwMin,NacetlReal(nCmdsnlmn))
        call FeQuestRealFromEdw(nEdwMax,NacetlReal(nCmdsnlmx))
      else
        NacetlInt(nCmdUseSinLim)=0
        NacetlReal(nCmdsnlmn)=DefRealFour(nCmdsnlmn)
        NacetlReal(nCmdsnlmx)=DefRealFour(nCmdsnlmx)
      endif
      if(NTwin.gt.1) then
        if(CrwLogicQuest(nCrwDiff)) then
          NacetlInt(nCmdTMethod)=0
        else
          NacetlInt(nCmdTMethod)=1
        endif
      endif
      if(NComp(KPhase).gt.1) then
        call FeQuestIntFromEdw(nEdwComp,NacetlInt(nCmdnsubs))
      else
        NacetlInt(nCmdnsubs)=1
      endif
9999  return
      end
      subroutine FouReadScope
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ip(6),prefr(3),xp(3)
      character*256 EdwStringQuest,t256
      character*80 Veta
      character*7 Label(3)
      character*2 nty
      integer EdwStateQuest,CrwStateQuest
      logical CrwLogicQuest
      logical, allocatable :: AtBrat(:)
      save nCrwAuto,nCrwExpl,nCrwPoint,nCrwDefaultOrient,nButtReset,
     1     nCrwOrientFirst,nCrwOrientLast,nCrwIndependent,
     2     nCrwRefreshScope,nLblMapTitle,nLblMapFirst,nLblMapLast,
     3     nEdwIntervalFirst,nEdwCenter,nEdwScope,nEdwStep,ip,
     4     nCrwWholeCell,nButtAtom
      data Label/'minimum','maximum','step'/
      entry FouReadScopeMake(id)
      if(NacetlInt(nCmdOrCode).lt.0) then
        call SetIntArrayTo(ip,6,-1)
      else
        write(Veta,101) NacetlInt(nCmdOrCode)
        call zhusti(Veta)
        read(Veta,100) ip
      endif
      call FouMakeScopeType
      xpom=XdQuestFou*.166667
      il=1
      ichk=1
      do i=1,3
        if(i.eq.1) then
          Veta='%automatically'
        else if(i.eq.2) then
          Veta='%explicitly'
        else
          Veta='by a central %point'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-CrwgXd*.5,il+1,Veta,'C',
     1                      CrwgXd,CrwgYd,1,ichk)
        if(i.eq.1) then
          nCrwAuto=CrwLastMade
        else if(i.eq.1) then
          nCrwExpl=CrwLastMade
        else if(i.eq.3) then
          nCrwPoint=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.ScopeType)
        xpom=xpom+XdQuestFou*.333333
      enddo
      il=il+2
      xpomm=18.
      Veta='Use %default map orientation'
      tpom=xpomm+CrwXd+3.
      call FeQuestCrwMake(id,tpom,il,xpomm,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwDefaultOrient=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ip(1).lt.0)
      il=il+1
      Veta='Map axes:'
      Veta=Veta(:idel(Veta)+2)//
     1     ' 1st=horizontal, 2nd=vertical, 3rd=section, ...'
      call FeQuestLblMake(id,xpomm,il,Veta,'L','B')
      nLblMapTitle=LblLastMade
      xpomp=XdQuestFou-230.
      il=il+1
      ilp=il
      dpom=50.
      do i=1,NDim(KPhase)
        xpom=5.
        il=il+1
        if(NDim(KPhase).eq.3) then
          Veta=smbx(i)
        else
          Veta=smbx6(i)
        endif
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        xpom=xpomp
        do j=1,3
          if(i.eq.1)
     1      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Label(j),'C','N')
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwIntervalFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      xpom=30.
      do i=1,NDim(KPhase)
        ichk=ichk+1
        il=ilp
        write(Veta,'(i1,a2)') i,nty(i)
        call FeQuestLblMake(id,xpom+10.,il,Veta,'C','N')
        if(i.eq.1) nLblMapFirst=LblLastMade
        do j=1,NDim(KPhase)
          il=il+1
          call FeQuestCrwMake(id,xpom,il,xpom,il,' ','C',CrwgXd,
     1                        CrwgYd,1,ichk)
          if(i.eq.1.and.j.eq.1) nCrwOrientFirst=CrwLastMade
        enddo
        xpom=xpom+30.
      enddo
      nLblMapLast=LblLastMade
      nCrwOrientLast=CrwLastMade
      il=il+1
      tpom=5.
      Veta='%Center'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwCenter=EdwLastMade
      Veta='Sc%ope [A]'
      xpom=xpomp
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScope=EdwLastMade
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='%Independent parallelepiped'
      ichk=ichk+1
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      ichk+1)
        if(i.eq.1) then
          nCrwIndependent=CrwLastMade
          Veta='%Whole cell'
        else
          nCrwWholeCell=CrwLastMade
        endif
      enddo
      il=il-1
      xpom=90.
      Veta='Select ato%m'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAtom=ButtonLastMade
      tpom=120.
      xpom=xpomp
      Veta='%Step [A]'
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwStep=EdwLastMade
      xpom=xpomp
      il=il+1
      Veta='%Refresh scope'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                    0,0)
      nCrwRefreshScope=CrwLastMade
      il=il+1
      Veta='Reset to de%fault'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(XdQuestFou-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtReset=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1200  nEdw=nEdwIntervalFirst
      do i=1,NDim(KPhase)
        if(xrmn(i).lt.-330.) then
          if(i.le.3) then
            if(CellPar(i,1,KPhase).gt.20.) then
              pomd=0.01
            else if(CellPar(i,1,KPhase).gt.10.) then
              pomd=0.02
            else
              pomd=0.05
            endif
          else
            pomd=0.1
          endif
        else
          pomd=dd(i)
        endif
        do j=1,3
          if((ScopeType.eq.ScopeExplicit.or.i.gt.3).and.
     1       EdwStateQuest(nEdw).ne.EdwOpened) then
            if(j.eq.1) then
              if(xrmn(i).lt.-330.) then
                pom=0.
              else
                pom=xrmn(i)
              endif
            else if(j.eq.2) then
              if(xrmn(i).lt.-330.) then
                pom=1.-pomd
              else
                pom=xrmx(i)
              endif
            else
              pom=pomd
            endif
            call FeQuestCrwClose(nCrwRefreshScope)
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          else if(ScopeType.ne.ScopeExplicit.and.
     1            EdwStateQuest(nEdw).eq.EdwOpened.and.i.le.3) then
            if(j.eq.1) xrmn(i)=-333.
            call FeQuestEdwClose(nEdw)
          endif
          nEdw=nEdw+1
        enddo
      enddo
      if(ScopeType.eq.ScopeCenter.and.
     1   EdwStateQuest(nEdwCenter).ne.EdwOpened) then
        if(ptname.eq.'[nic]') then
          Veta=' '
        else
          Veta=ptname
        endif
        call FeQuestStringEdwOpen(nEdwCenter,Veta)
        call FeQuestRealAEdwOpen(nEdwScope,pts,3,.false.,.false.)
      else if(ScopeType.ne.ScopeCenter.and.
     1        EdwStateQuest(nEdwCenter).eq.EdwOpened) then
        ptname='[nic]'
        call SetRealArrayTo(ptx,3,0.)
        call SetRealArrayTo(pts,3,.5)
        call FeQuestEdwClose(nEdwCenter)
        call FeQuestEdwClose(nEdwScope)
        call FeQuestCrwClose(nCrwRefreshScope)
        call FeQuestButtonClose(nButtAtom)
      endif
      if((ScopeType.eq.ScopeAuto.or.ScopeType.eq.ScopeCenter).and.
     1   EdwStateQuest(nEdwStep).ne.EdwOpened) then
        call FeQuestRealEdwOpen(nEdwStep,NacetlReal(nCmdptstep),.false.,
     1                          .false.)
      else if(ScopeType.eq.ScopeExplicit) then
        NacetlReal(nCmdptstep)=DefaultReal(nCmdptstep)
        call FeQuestEdwClose(nEdwStep)
      endif
1300  nCrw=nCrwOrientFirst-1
      do i=1,NDim(KPhase)
        do 1320j=1,NDim(KPhase)
          nCrw=nCrw+1
          do k=1,i-1
            if(ip(k).eq.j) then
              call FeQuestCrwClose(nCrw)
              go to 1320
            endif
          enddo
          if(ip(1).gt.0) call FeQuestCrwOpen(nCrw,j.eq.ip(i))
1320    continue
      enddo
      if(ScopeType.eq.ScopeCenter.and.ip(1).le.3.and.ip(2).gt.3) then
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        if(CrwStateQuest(nCrwRefreshScope).eq.CrwClosed) then
          call FeQuestCrwOpen(nCrwRefreshScope,.true.)
          call FeQuestButtonOpen(nButtAtom,ButtonOff)
        endif
      else
        call FeQuestCrwClose(nCrwRefreshScope)
        call FeQuestButtonClose(nButtAtom)
        if(ScopeType.eq.ScopeAuto) then
          call FeQuestCrwOpen(nCrwIndependent,
     1                        NacetlInt(nCmdIndUnit).eq.1)
          call FeQuestCrwOpen(nCrwWholeCell,
     1                        NacetlInt(nCmdIndUnit).ne.1)
        else
          NacetlInt(nCmdIndUnit)=DefaultInt(nCmdIndUnit)
          call FeQuestCrwClose(nCrwIndependent)
          call FeQuestCrwClose(nCrwWholeCell)
        endif
      endif
      go to 9999
      entry FouReadScopeCheck
1500  if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwAuto.and.CheckNumber.le.nCrwPoint) then
        ScopeType=CheckNumber-nCrwAuto+1
        go to 1600
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwDefaultOrient) then
        if(CrwLogicQuest(nCrwDefaultOrient)) then
          do i=nCrwOrientFirst,nCrwOrientLast
            call FeQuestCrwClose(i)
          enddo
          ip(1)=-1
        else
          do i=1,NDim(KPhase)
            ip(i)=i
          enddo
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwOrientFirst.and.
     2        CheckNumber.le.nCrwOrientLast) then
        irefr=ip(1)
        j=CheckNumber-nCrwOrientFirst
        i=j/NDim(KPhase)+1
        j=mod(j,NDim(KPhase))+1
        ipp=ip(i)
        ip(i)=j
        do k=i+1,NDim(KPhase)
          if(ip(k).eq.j) then
            ip(k)=ipp
            go to 1512
          endif
        enddo
1512    if(CrwLogicQuest(nCrwRefreshScope)) then
          call FeQuestRealAFromEdw(nEdwScope,prefr)
          if(ip(1).ne.irefr.and.ip(1).le.3) then
            i=mod(irefr,3)+1
            j=6-i-irefr
            pom=prefr(irefr)
            prefr(irefr)=prefr(ip(1))
            prefr(ip(1))=pom
            call FeQuestRealAEdwOpen(nEdwScope,prefr,3,.false.,.false.)
          endif
        endif
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCenter) then
        if(EventTypeSave.eq.EventCrw.and.
     1     (EventNumberSave.eq.nCrwAuto.or.
     2      EventNumberSave.eq.nCrwExpl)) then
          go to 9999
        endif
        t256=EdwStringQuest(nEdwCenter)
        k=0
        call StToReal(t256,k,xp,3,.false.,ich)
        if(ich.ne.0) then
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmYes,IdAtMolMixNo,Veta)
          if(Veta.ne.' ') then
            if(Veta.ne.'Jiz oznameno')
     1        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            go to 1520
          endif
        endif
        go to 9999
1520    EventType=EventEdw
        EventNumber=nEdwCenter
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtom)
     1  then
        nap=NAtIndFrAll(KPhase)
        nak=NAtPosToAll(KPhase)
        call SelOneAtom('Select atom as a central point',Atom(nap),ia,
     1                   nak-nap+1,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwCenter,Atom(ia))
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        go to 1600
      endif
      go to 9999
1600  call FeQuestEdwClose(nEdwStep)
      if(ScopeType.eq.ScopeAuto) then
        NacetlReal(nCmdptstep)=.1
         pom=.9
        dpom=.1
        call FeQuestCrwOn(nCrwDefaultOrient)
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
        ip(1)=-1
      else if(ScopeType.eq.ScopeExplicit) then
        call FeQuestCrwOn(nCrwDefaultOrient)
         pom=.9
        dpom=.1
        ip(1)=-1
      else if(ScopeType.eq.ScopeCenter) then
        NacetlReal(nCmdptstep)=.02
         pom=2.
        dpom=.02
        call FeQuestCrwOff(nCrwDefaultOrient)
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        if(NDimI(KPhase).gt.0) then
          ip(1)=1
          ip(2)=4
          j=1
          do i=3,NDim(KPhase)
            j=j+1
            if(j.eq.4) j=j+1
            ip(i)=j
          enddo
        else
          do i=1,3
            ip(i)=i
          enddo
        endif
        call FeQuestCrwOpen(nCrwRefreshScope,.true.)
        pts(1)=2.
        pts(2)=0.
        pts(3)=0.
        call FeQuestButtonOpen(nButtAtom,ButtonOff)
      endif
1620  if(ip(1).lt.0) then
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
      endif
      nEdw=nEdwIntervalFirst+9
      do i=4,NDim(KPhase)
        xrmn(i)=0.0
        xrmx(i)=pom
        dd(i)  =dpom
        do j=1,3
          call FeQuestEdwClose(nEdw)
          nEdw=nEdw+1
        enddo
      enddo
      go to 1200
      entry FouReadScopeUpdate
      if(CrwLogicQuest(nCrwIndependent).and.ScopeType.eq.ScopeAuto) then
        NacetlInt(nCmdIndUnit)=1
      else
        NacetlInt(nCmdIndUnit)=0
      endif
      if(ScopeType.eq.ScopeAuto.or.ScopeType.eq.ScopeCenter) then
        i1=4
      else
        i1=1
      endif
      nEdw=nEdwIntervalFirst+(i1-1)*3
      do i=i1,NDim(KPhase)
        do j=1,3
          call FeQuestRealFromEdw(nEdw,pom)
          if(j.eq.1) then
            xrmn(i)=pom
          else if(j.eq.2) then
            xrmx(i)=pom
          else
            dd(i)=pom
          endif
          nEdw=nEdw+1
        enddo
      enddo
      if(ScopeType.eq.ScopeCenter) then
        ptname=EdwStringQuest(nEdwCenter)
        call FeQuestRealAFromEdw(nEdwScope,pts)
      else
        ptname='[nic]'
      endif
      if(ScopeType.ne.ScopeExplicit)
     1  call FeQuestRealFromEdw(nEdwStep,NacetlReal(nCmdptstep))
      if(ip(1).le.0) then
        j=-333
      else
        j=ip(1)
        do i=2,NDim(KPhase)
          j=j*10+ip(i)
        enddo
      endif
      NacetlInt(nCmdOrCode)=j
9999  return
100   format(6i1)
101   format(i6)
      end
      subroutine FouMakeScopeType
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      if(ptname.ne.'[nic]') then
        ScopeType=3
      else
        ScopeType=2
        do i=1,3
          if(xrmn(i).lt.-330.) then
            ScopeType=1
            go to 3610
          endif
        enddo
      endif
3610  return
      end
      subroutine FouPeaksCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 Veta
      integer LblStateQuest,EdwStateQuest
      logical CrwLogicQuest,lpom
      save nCrwSearchForPeaks,nCrwDefault,nCrwExplicitly,nEdwPositive,
     1     nEdwNegative,nEdwNoOfHarm,nEdwDMax,nLblMaxPeaks,nLblMod,
     2     nCrwIntSphere,nEdwIntSphereRad,nCrwIntAdjust,Radius,
     3     nEdwIntAdjustRad,nLinka
      entry FouPeaksCommandsMake(id)
      il=1
      xpom=8.
      tpom=xpom+CrwXd+10.
      Veta='%search for peaks in the calculated map'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,0)
      nCrwSearchForPeaks=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdLPeaks).gt.0)
      il=il+1
      tpom=30.
      call FeQuestLblMake(id,tpom,il,'Maximum number of peaks:','L','N')
      nLblMaxPeaks=LblLastMade
      il=il+1
      tpom=xpom+CrwgXd+10.
      Veta='%Default'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      nCrwDefault=CrwLastMade
      lpom=NacetlInt(nCmdPPeaks).lt.0.and.NacetlInt(nCmdNPeaks).lt.0
      call FeQuestCrwOpen(CrwLastMade,lpom)
      il=il+1
      Veta='%Explicitly'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      nCrwExplicitly=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,.not.lpom)
      Veta='%Positive'
      tpom=xpom+100.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPositive=EdwLastMade
      tpom=xpom+dpom+30.
      Veta='%Negative'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNegative=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Charge integration:'
      xpom=XdQuestFou*.5
      call FeQuestLblMake(id,xpom,il,Veta,'C','B')
      Veta='in a %fixed sphere of radius'
      xpom=5.
      tpom=xpom+CrwgXd+10.
      dpom=60.
      Radius=NacetlReal(nCmdIntRad)
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdIntMethod))
        tpome=tpom+FeTxLengthUnder(Veta)+5.
        xpome=tpome+25.
        call FeQuestEdwMake(id,tpome,il,xpome,il,'=>','L',dpom,EdwYd,0)
        if(i.eq.1) then
          nCrwIntSphere=CrwLastMade
          nEdwIntSphereRad=EdwLastMade
          Veta='in an %adjusted shape within maximal radius'
        else
          nCrwIntAdjust=CrwLastMade
          nEdwIntAdjustRad=EdwLastMade
        endif
      enddo
      il=il+1
      Veta='The second method is more precise but slower'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        nLinka=LinkaLastMade
        il=il+1
        xpom=XdQuestFou*.5
        Veta='Interpretation of displacement waves:'
        call FeQuestLblMake(id,xpom,il,Veta,'C','B')
        nLblMod=LblLastMade
        il=il+1
        tpom=5.
        xpom=200.
        dpom=30.
        Veta='No. of %harmonics'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwNoOfHarm=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdkharm),.false.)
        il=il+1
        dpom=50.
        Veta='Ma%ximal modulation displacement'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmddmax),
     1                          .false.,.false.)
      else
        nLblMod=0
        nEdwNoOfHarm=0
        nEdwDMax=0
      endif
1400  if(CrwLogicQuest(nCrwSearchForPeaks)) then
        if(LblStateQuest(nLblMaxPeaks).eq.LblOff) then
          call FeQuestLblOn(nLblMaxPeaks)
          lpom=NacetlInt(nCmdPPeaks).lt.0.and.
     1         NacetlInt(nCmdNPeaks).lt.0
          call FeQuestCrwOpen(nCrwDefault,lpom)
          call FeQuestCrwOpen(nCrwExplicitly,.not.lpom)
          if(NDimI(KPhase).gt.0) then
            call FeQuestLinkaOn(nLinka)
            call FeQuestLblOn(nLblMod)
            call FeQuestIntEdwOpen(nEdwNoOfHarm,NacetlInt(nCmdkharm),
     1                             .false.)
            call FeQuestRealEdwOpen(nEdwDMax,NacetlReal(nCmddmax),
     1                              .false.,.false.)
          endif
        endif
        if(CrwLogicQuest(nCrwExplicitly)) then
          k=nCmdPPeaks
          nEdw=nEdwPositive
          do i=1,2
            if(NacetlInt(k).gt.0) then
              j=NacetlInt(k)
            else
              if(i.eq.1) then
                j=50
              else
                j=5
              endif
              NacetlInt(k)=j
            endif
            call FeQuestIntEdwOpen(nEdw,j,.false.)
            k=k+1
            nEdw=nEdw+1
          enddo
        else
          NacetlInt(nCmdPPeaks)=-333
          NacetlInt(nCmdNPeaks)=-333
          call FeQuestEdwClose(nEdwPositive)
          call FeQuestEdwClose(nEdwNegative)
        endif
      else
        call FeQuestLblOff(nLblMaxPeaks)
        call FeQuestCrwClose(nCrwDefault)
        call FeQuestCrwClose(nCrwExplicitly)
        call FeQuestEdwClose(nEdwPositive)
        call FeQuestEdwClose(nEdwNegative)
        if(NDimI(KPhase).gt.0) then
          call FeQuestLinkaOff(nLinka)
          call FeQuestLblOff(nLblMod)
          call FeQuestEdwClose(nEdwNoOfHarm)
          call FeQuestEdwClose(nEdwDMax)
        endif
      endif
      if(CrwLogicQuest(nCrwIntSphere)) then
        if(EdwStateQuest(nEdwIntAdjustRad).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwIntAdjustRad,Radius)
          Radius=Radius*.5
        endif
        call FeQuestEdwClose(nEdwIntAdjustRad)
        call FeQuestRealEdwOpen(nEdwIntSphereRad,Radius,.false.,.false.)
      else
        if(EdwStateQuest(nEdwIntSphereRad).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwIntSphereRad,Radius)
          Radius=Radius*2.
        endif
        call FeQuestEdwClose(nEdwIntSphereRad)
        call FeQuestRealEdwOpen(nEdwIntAdjustRad,Radius,.false.,.false.)
      endif
      go to 9999
      entry FouPeaksCommandsCheck
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwExplicitly.or.CheckNumber.eq.nCrwDefault))
     2  then
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwIntSphere.or.CheckNumber.eq.nCrwIntAdjust))
     2  then
        go to 1400
      else if(CheckType.eq.
     1        EventCrw.and.CheckNumber.eq.nCrwSearchForPeaks) then
        go to 1400
      endif
      go to 9999
      entry FouPeaksCommandsUpdate
      if(CrwLogicQuest(nCrwSearchForPeaks)) then
        NacetlInt(nCmdLPeaks)=1
        if(CrwLogicQuest(nCrwDefault)) then
          NacetlInt(nCmdPPeaks)=-333
          NacetlInt(nCmdNPeaks)=-333
        else
          call FeQuestIntFromEdw(nEdwPositive,NacetlInt(nCmdPPeaks))
          call FeQuestIntFromEdw(nEdwNegative,NacetlInt(nCmdNPeaks))
        endif
        if(NDim(KPhase).gt.3) then
          call FeQuestIntFromEdw(nEdwNoOfHarm,NacetlInt(nCmdKHarm))
          call FeQuestRealFromEdw(nEdwDMax,NacetlReal(nCmdDMax))
        endif
      else
        NacetlInt(nCmdLPeaks)=0
        NacetlInt(nCmdPPeaks)=-333
        NacetlInt(nCmdNPeaks)=-333
        if(NDim(KPhase).gt.3) then
          NacetlInt(nCmdKHarm)=DefIntFour(nCmdKHarm)
          NacetlReal(nCmdDMax)=DefRealFour(nCmdDMax)
        endif
      endif
      if(CrwLogicQuest(nCrwIntSphere)) then
        call FeQuestRealFromEdw(nEdwIntSphereRad,NacetlReal(nCmdIntRad))
        NacetlInt(nCmdIntMethod)=1
      else
        call FeQuestRealFromEdw(nEdwIntAdjustRad,NacetlReal(nCmdIntRad))
        NacetlInt(nCmdIntMethod)=2
      endif
9999  return
      end
      subroutine FouReadScopeCP(Klic,CPIndUnit,CPStep,DensityType,
     1                          CutOffDist,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ip(6),xp(3),nLblX(3),nLblMeze(3)
      character*256 EdwStringQuest,t256
      character*80 Veta
      character*25 :: MenDen(7) =
     1                (/'total density            ',
     2                  'valence density          ',
     3                  'deformation density      ',
     4                  'total density Laplacian  ',
     5                  'valence density Laplacian',
     6                  'inversed gradient vector ',
     7                  'gradient vector          '/)
      character*7 Label(3)
      integer ScopeTypeOld,CPIndUnit,DensityType,RolMenuSelectedQuest
      logical CrwLogicQuest
      data Label/'minimum','maximum','step'/
      call FouMakeScopeType
      xqd=500.
      id=NextQuestId()
      il=8
      Veta='Define scope:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      xpom=xqd*.166667
      ichk=1
      do i=1,3
        if(i.eq.1) then
          Veta='%automatically'
        else if(i.eq.2) then
          Veta='%explicitly'
        else
          Veta='by a central %point'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,ichk)
        if(i.eq.1) then
          nCrwAuto=CrwLastMade
        else if(i.eq.1) then
          nCrwExpl=CrwLastMade
        else if(i.eq.3) then
          nCrwPoint=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.ScopeType)
        xpom=xpom+xqd*.333333
      enddo
      xpomp=xqd*.5-80.
      il=il+2
      ilp=il
      dpom=50.
      do i=1,3
        il=il+1
        Veta=smbx(i)
        call FeQuestLblMake(id,xpomp-20.,il,Veta,'L','N')
        nLblX(i)=LblLastMade
        call FeQuestLblOff(LblLastMade)
        xpom=xpomp
        do j=1,3
          if(i.eq.1) then
            call FeQuestLblMake(id,xpom+dpom*.5,ilp,Label(j),'C','N')
            call FeQuestLblOff(LblLastMade)
            nLblMeze(j)=LblLastMade
          endif
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwIntervalFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      il=3
      xpom=5.
      tpom=xpom+xpom+CrwgXd+10.
      Veta='%Independent parallelepiped'
      ichk=ichk+1
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      ichk)
        if(i.eq.1) then
          nCrwIndependent=CrwLastMade
          Veta='%Whole cell'
        else
          nCrwWholeCell=CrwLastMade
        endif
      enddo
      xpom=xqd*.5+50.
      Veta='%Step [A]'
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwStep=EdwLastMade
      il=il+1
      tpom=5.
      Veta='Ce%nter'
      xpomp=tpom+FeTxLengthUnder(Veta)+20.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nEdwCenter=EdwLastMade
      Veta='Sc%ope [A]'
      xpom=xqd*.5+50.
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScope=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='C%utoff distance for atoms'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCutOffDist=EdwLastMade
      call FeQuestLblMake(id,xpom+dpom+10.,il,'[Ang]','L','N')
      call FeQuestRealEdwOpen(nEdwCutOffDist,CutOffDist,.false.,
     1                          .false.)
      if(Klic.ne.0) then
        tpom=xpom+dpom+70.
        Veta='%Calculate'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=150.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolDensityType=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolDensityType,MenDen,7,DensityType+1)
      else
        nRolDensityType=0
      endif
      ScopeTypeOld=-1
1200  if(ScopeTypeOld.eq.ScopeType) go to 1500
      if(ScopeTypeOld.eq.ScopeAuto) then
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        call FeQuestEdwClose(nEdwStep)
      else if(ScopeTypeOld.eq.ScopeExplicit) then
        nEdw=nEdwIntervalFirst
        do i=1,3
          call FeQuestLblOff(nLblX(i))
          do j=1,3
            if(i.eq.1) call FeQuestLblOff(nLblMeze(j))
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
        enddo
      else
        ptname='[nic]'
        call SetRealArrayTo(ptx,3,0.)
        call SetRealArrayTo(pts,3,.5)
        call FeQuestEdwClose(nEdwCenter)
        call FeQuestEdwClose(nEdwScope)
        call FeQuestEdwClose(nEdwStep)
      endif
      if(ScopeType.eq.ScopeAuto) then
        call FeQuestCrwOpen(nCrwIndependent,CPIndUnit.eq.1)
        call FeQuestCrwOpen(nCrwWholeCell,CPIndUnit.ne.1)
        call FeQuestRealEdwOpen(nEdwStep,CPStep,.false.,.false.)
      else if(ScopeType.eq.ScopeExplicit) then
        nEdw=nEdwIntervalFirst
        do i=1,3
          call FeQuestLblOn(nLblX(i))
          do j=1,3
            if(i.eq.1) call FeQuestLblOn(nLblMeze(j))
            if(j.eq.1) then
              if(xrmn(i).lt.-330.) then
                pom=0.
              else
                pom=xrmn(i)
              endif
            else if(j.eq.2) then
              if(xrmn(i).lt.-330.) then
                pom=1.
              else
                pom=xrmx(i)
              endif
            else
              if(xrmn(i).lt.-330.) then
                if(i.le.3) then
                  if(CellPar(i,1,KPhase).gt.20.) then
                    pom=0.01
                  else if(CellPar(i,1,KPhase).gt.10.) then
                    pom=0.02
                  else
                    pom=0.05
                  endif
                else
                   pom=0.1
                endif
              else
                pom=dd(i)
              endif
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            nEdw=nEdw+1
          enddo
        enddo
      else
        if(ptname.eq.'[nic]') then
          Veta=' '
        else
          Veta=ptname
        endif
        call FeQuestStringEdwOpen(nEdwCenter,Veta)
        call FeQuestRealAEdwOpen(nEdwScope,pts,3,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwStep,CPStep,.false.,.false.)
      endif
      ScopeTypeOld=ScopeType
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwAuto.and.CheckNumber.le.nCrwPoint) then
        ScopeType=CheckNumber-nCrwAuto+1
        go to 1200
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCenter) then
        if(EventType.eq.EventCrw.and.EventNumber.le.nCrwPoint)
     1    go to 1500
        t256=EdwStringQuest(nEdwCenter)
        k=0
        call StToReal(t256,k,xp,3,.false.,ich)
        call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                      IdAtMolMixNo,Veta)
        if(Veta.ne.' ') then
          if(Veta.ne.'Jiz oznameno')
     1      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          ich=1
          go to 1520
        endif
        go to 1500
1520    EventType=EventEdw
        EventNumber=nEdwCenter
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(CrwLogicQuest(nCrwIndependent).and.ScopeType.eq.1) then
          CPIndUnit=1
        else
          CPIndUnit=0
        endif
        if(ScopeType.eq.1.or.ScopeType.eq.3) then
          i1=4
        else
          i1=1
        endif
        nEdw=nEdwIntervalFirst+(i1-1)*3
        do i=i1,NDim(KPhase)
          do j=1,3
            call FeQuestRealFromEdw(nEdw,pom)
            if(j.eq.1) then
              xrmn(i)=pom
            else if(j.eq.2) then
              xrmx(i)=pom
            else
              dd(i)=pom
            endif
            nEdw=nEdw+1
          enddo
        enddo
        if(ScopeType.eq.3) then
          ptname=EdwStringQuest(nEdwCenter)
          call FeQuestRealAFromEdw(nEdwScope,pts)
        else
          ptname='[nic]'
        endif
        if(ScopeType.ne.2)
     1    call FeQuestRealFromEdw(nEdwStep,CPStep)
        if(Klic.ne.0)
     1    DensityType=RolMenuSelectedQuest(nRolDensityType)-1
        call FeQuestRealFromEdw(nEdwCutOffDist,CutOffDist)
      endif
      call FeQuestRemove(id)
      return
100   format(6i1)
101   format(i6)
      end
      subroutine FourierSumOldNew
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      dimension xp(6),ix(6),ihp(6),Table(:),TableP(:),mxi(6),mni(6),
     1          mdi(6),mxin(6),mnin(6),mdin(6),ixn(6),ih(6)
      logical DelaTable
      allocatable Table,TableP
      equivalence (ix(1),ix1)
      call OpenMaps(81,fln(:ifln)//'.m81',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      rmax=0.
      rmin=0.
      write(81,rec=1) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,iorien,
     1                mapa,nsubs+(KPhase-1)*10,MameSatelity,
     2                nonModulated(KPhase)
      irec=1
      nn=0
      nd=NDimSum
      ncykl=1
      do i=nd+1,6
        ncykl=ncykl*nx(i)
      enddo
      nmp=1
      do i=1,nd
        nmp=nmp*nx(i)
      enddo
      n=2
      do i=1,nd
        n=n*max(hmax(i)-hmin(i)+1,nx(i))
      enddo
      allocate(Table (n))
      allocate(TableP(n))
      do it=1,ncykl
        call RecUnpack(it,ix,nx(nd+1),6-nd)
        do i=1,6-nd
          j=i+nd
          xp(j)=xfmn(j)+(ix(i)-1)*xdf(j)
        enddo
        j=nd
        do i=1,nd
          mni(j)=hmin(i)
          mxi(j)=hmax(i)
          j=j-1
        enddo
        mxc=1
        do i=1,nd
          mdi(i)=mxi(i)-mni(i)+1
          mxc=mxc*mdi(i)
        enddo
        call SetRealArrayTo(TableP,2*mxc,0.)
1200    read(82,end=1240) ihp,ai,bi
        j=nd
        do i=1,nd
          ih(j)=ihp(i)
          j=j-1
        enddo
        locc=MatPack(ih,mni,mdi,nd)
        locd=locc+mxc
        if(nd.ne.NDim(KPhase)) then
          xlz=0.
          do i=nd+1,NDim(KPhase)
            xlz=xlz+float(ihp(i))*xp(i)
          enddo
          xlz=pi2*xlz
          sinlz=sin(xlz)
          coslz=cos(xlz)
          TableP(locc)=TableP(locc)+ai*coslz+bi*sinlz
          TableP(locd)=TableP(locd)-ai*sinlz+bi*coslz
        else
          TableP(locc)=ai
          TableP(locd)=bi
        endif
        go to 1200
1240    rewind 82
        do nh=nd,1,-1
          DelaTable=mod(nh,2).eq.mod(nd,2)
          nxnh=nx(nh)
          do i=1,nh-1
            mxin(i)=mxi(i+1)
            mnin(i)=mni(i+1)
          enddo
          mnin(nh)=1
          mxin(nh)=nxnh
          do i=nh+1,nd
            mxin(i)=mxi(i)
            mnin(i)=mni(i)
          enddo
          mxcn=1
          do i=1,nd
            mdin(i)=mxin(i)-mnin(i)+1
            mxcn=mxcn*mdin(i)
          enddo
          if(DelaTable) then
            call SetRealArrayTo(Table,2*mxcn,0.)
          else
            call SetRealArrayTo(TableP,2*mxcn,0.)
          endif
          loccd=1
          do i=1,nh-1
            loccd=loccd*mdin(i)
          enddo
          call CopyVekI(mni,ix,nd)
          ix1=ix1-1
          do n=1,mxc
            if(DelaTable) then
              chy=TableP(n)
              dhy=TableP(n+mxc)
            else
              chy=Table(n)
              dhy=Table(n+mxc)
            endif
            do i=1,nd
              last=i
              ix(i)=ix(i)+1
              if(ix(i).gt.mxi(i).and.i.le.nd) then
                ix(i)=mni(i)
              else
                go to 1620
              endif
            enddo
1620        if(last.ne.1.or.n.eq.1) then
              do i=1,nh-1
                ixn(i)=ix(i+1)
              enddo
              do i=nh+1,nd
                ixn(i)=ix(i)
              enddo
              ixn(nh)=1
              loccp=MatPack(ixn,mnin,mdin,nd)
            endif
            locc=loccp
            ii=ix1-hmin(nh)+1
            do i=1,nxnh
              sinxh=SinTable(ii,i,nh)
              cosxh=CosTable(ii,i,nh)
              pom=chy*cosxh+dhy*sinxh
              if(DelaTable) then
                Table(locc)=Table(locc)+pom
              else
                TableP(locc)=TableP(locc)+pom
              endif
              if(nh.ne.1) then
                locd=locc+mxcn
                pom=-chy*sinxh+dhy*cosxh
                if(DelaTable) then
                  Table(locd)=Table(locd)+pom
                else
                  TableP(locd)=TableP(locd)+pom
                endif
              endif
              locc=locc+loccd
            enddo
          enddo
          if(nh.ne.1) then
            call CopyVekI(mxin,mxi,nd)
            call CopyVekI(mnin,mni,nd)
            call CopyVekI(mdin,mdi,nd)
            mxc=mxcn
          endif
        enddo
        if(.not.DelaTable) call CopyVek(TableP,Table,nmp)
        do i=1,nmp
          rmax=max(rmax,Table(i))
          rmin=min(rmin,Table(i))
        enddo
        IZdih=0
        do i=1,nmp/nxny
          irec=irec+1
          write(81,rec=irec)(table(IZdih+j),j=1,nxny)
          IZdih=IZdih+nxny
        enddo
      enddo
8000  irec=irec+1
      write(81,rec=irec) rmax,rmin
      call newln(1)
      write(Cislo,'(f15.2)') rmax
      call ZdrcniCisla(Cislo,1)
      t80='Maximal density : '//Cislo(:idel(Cislo))
      write(Cislo,'(f15.2)') rmin
      call ZdrcniCisla(Cislo,1)
      t80=t80(:idel(t80))//', minimal density : '//Cislo(:idel(Cislo))
      write(lst,FormA1)(t80(i:i),i=1,idel(t80))
      if(Mapa.eq.6) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.eq.0) then
          LnSum=NextLogicNumber()
          call OpenFile(LnSum,fln(:ifln)//'_Fourier.l70','formatted',
     1                  'unknown')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,'(''# Fourier'')')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,FormA)
          do i=1,2
            if(i.eq.1) then
              pom=rmax
            else
              pom=rmin
            endif
            write(Cislo,'(f8.2)') pom
            call Zhusti(Cislo)
            write(LnSum,FormCIF) CifKey(i,15),Cislo
          enddo
          call CloseIfOpened(LnSum)
        endif
        if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      endif
      call FeFlowChartRemove
9999  call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
      if(allocated(Table)) deallocate(Table)
      if(allocated(TableP)) deallocate(TableP)
      call CloseIfOpened(81)
      return
      end
      function MatPack(ih,ihmn,ihd,n)
      include 'fepc.cmn'
      dimension ih(n),ihd(n),ihmn(n)
      MatPack=ih(n)-ihmn(n)
      do i=n-1,1,-1
        MatPack=MatPack*ihd(i)+ih(i)-ihmn(i)
      enddo
      MatPack=MatPack+1
      return
      end
      subroutine MatUnpack(m,ih,ihmn,ihd,n)
      include 'fepc.cmn'
      dimension ih(n),ihd(n),ihmn(n)
      j=m-1
      do i=1,n-1
        k=mod(j,ihd(i))
        ih(i)=k+ihmn(i)
        j=j/ihd(i)
      enddo
      ih(n)=j+ihmn(n)
      return
      end
      subroutine FouMakeDeformMap
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      dimension ih(6)
      dimension F1Arr(:),F2Arr(:),AArr(:),BArr(:),sig(:)
      integer HArr(:,:)
      logical EqIgCase,EqIV
      allocatable HArr,F1Arr,F2Arr,AArr,BArr,sig
      call OpenFile(80,fln(:ifln)//'.m80','formatted','old')
      call FouNastavM80
      NRef=0
1100  read(80,FormA,end=1200) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 1200
      endif
      read(Veta,Format80,err=8000,end=1200)
     1  (ih(i),i=1,MaxNDim),KPh
      if(ih(1).gt.900) go to 1200
      if(KPh.eq.KPhase) NRef=NRef+1
      go to 1100
1200  allocate(HArr(MaxNDim,NRef),F1Arr(NRef),F2Arr(NRef),
     1         AArr(NRef),BArr(NRef),sig(NRef))
      rewind 80
      n=0
1300  read(80,FormA,end=1500) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 1500
      endif
      read(Veta,Format80,err=8000,end=1500)
     1  (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2   acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
      if(ih(1).gt.900) go to 1300
      if(KPh.eq.KPhase) then
        n=n+1
        call CopyVekI(ih,HArr(1,n),MaxNDim)
        F1Arr(n)=fo1
        F2Arr(n)=-1.
        sig(n)=sigfo1
      endif
      go to 1300
1500  call CloseIfOpened(80)
      Veta='D:\Structures\Jana2006\Work\Dominik Schaniel\Rupy\Final\'//
     1     'GS\Rupy-GS-final.m80'
      call OpenFile(80,Veta,'formatted','old')
      call FouNastavM80
      ist=0
1600  read(80,FormA,end=2000) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 2000
      endif
      read(Veta,Format80,err=8000,end=2000)
     1  (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2   acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
      if(ih(1).gt.900) go to 2000
      if(KPh.eq.KPhase) then
        do i=1,NRef
          j=mod(i+ist-1,NRef)+1
          if(EqIV(ih,HArr(1,j),MaxNDim)) then
            ist=j
            F2Arr(j)=fo1
            AArr(j)=ac
            BArr(j)=bc
            go to 1600
          endif
        enddo
      endif
      go to 1600
2000  call CloseIfOpened(80)
      suma1=0.
      suma2=0.
      do i=1,NRef
        if(F2Arr(i).gt.0.) then
          if(sig(i).gt.0) then
            w=1./sig(i)**2
          else
            w=0.
          endif
          suma1=suma1+w*F1Arr(i)*F2Arr(i)
          suma2=suma2+w*F2Arr(i)**2
        endif
      enddo
      scpom=suma1/suma2
      do i=1,NRef
        F2Arr(i)=F2Arr(i)*scpom
      enddo
      call CopyFile(fln(:ifln)//'.m80',fln(:ifln)//'.z80')
      call OpenFile(80,fln(:ifln)//'.z80','formatted','old')
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m80','formatted','old')
      if(NDatBlock.gt.1) then
2100    read(80,FormA,err=8000) Veta
        write(ln,FormA) Veta(:idel(Veta))
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(t80,k,Cislo)
          if(EqIgCase(Cislo,'begin')) go to 9999
        endif
        go to 2100
      endif
      do i=1,NRef
        if(F2Arr(i).gt.0.) then
          write(ln,Format80)
     1      (HArr(j,i),j=1,MaxNDim),KPhase,F1Arr(i),F1Arr(i),F2Arr(i),
     2      AArr(i),BArr(i),(0.,j=1,6),sig(i),sig(i)
        endif
      enddo
      call CloseIfOpened(80)
      call CloseIfOpened(ln)
      go to 9999
8000  call FeReadError(80)
9999  return
      end

