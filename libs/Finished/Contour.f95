      subroutine Contour
      use Contour_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical FeYesNoHeader
      character*80 Veta
      if(ChargeDensities)
     1  allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     2           DenCoreDer(240,2,MaxNAtFormula),
     3           DenValDer(240,2,MaxNAtFormula))
      n=max(NAtCalc,1000)
      MaxPDF=n
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      allocate(AtBrat(NAtCalc),AtBratAnharm(5,NAtCalc))
      call ConPrelim
      if(ErrFlag.ne.0) go to 9999
      call ConRightButtons
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
9999  call zmcmlt(1)
      call DeleteFile(ConImageFile)
      if(allocated(FlagMap)) deallocate(FlagMap)
      if(allocated(ActualMap)) deallocate(ActualMap)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      if(allocated(AtBrat)) deallocate(AtBrat)
      if(allocated(AtBratAnharm)) deallocate(AtBratAnharm)
      if(NewAtoms.or.ModifiedAtoms.or.NewPoints) then
        NInfo=0
        if(NewAtoms.or.NewPoints) then
          NInfo=NInfo+1
          if(NewAtoms) then
            Veta='atom(s)'
            if(NewPoints) Veta=Veta(:idel(Veta))//' and point(s)'
          else
            Veta='point(s)'
          endif
          TextInfo(NInfo)='New '//Veta(:idel(Veta))//' were localized.'
        endif
        if(ModifiedAtoms) then
          NInfo=NInfo+1
          TextInfo(NInfo)='Some atomic positions were modified.'
        endif
        if(FeYesNoHeader(-1.,-1.,'Do you really want to accept '//
     1                   'those changes?',1))
     2    call iom40only(1,0,fln(:ifln)//'.m40')
      endif
      call DeleteFile(PreviousM40)
      do i=81,86
        call CloseIfOpened(i)
      enddo
      call DeleteAllFiles(fln(:ifln)//'.l8?')
      if(allocated(ContourArray)) deallocate(ContourArray)
      return
      end
      subroutine ConHeader
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      data t80/' '/
      if(irec.le.0) go to 9999
      write(t80,'(i6,''$'',i6)') irec,nmap
      call zhusti(t80)
      i=index(t80,'$')
      t80=t80(1:i-1)//' of '//t80(i+1:idel(t80))
      if(kpdf.eq.0) then
        t80='Fourier maps regular section : '//t80(1:idel(t80))
      else if(kpdf.eq.1) then
        t80='Fourier maps general section : '//t80(1:idel(t80))
      else if(kpdf.eq.2) then
        t80='p.d.f. : '//t80(1:idel(t80))
      else if(kpdf.eq.3) then
        t80='j.p.d.f. : '//t80(1:idel(t80))
      else if(kpdf.le.6) then
        if(DensityType.eq.0) then
          t80='Total density'
        else if(DensityType.eq.1) then
          t80='Valence density'
        else if(DensityType.eq.2) then
          t80='Deformation density'
        else if(DensityType.eq.3) then
          t80='Total density Laplacian '
        else if(DensityType.eq.4) then
          t80='Valence density Laplacian '
        else if(DensityType.eq.5) then
          t80='Inversed gradient vector'
        else if(DensityType.eq.6) then
          t80='Gradient vector'
        endif
      endif
      if(ErrDraw) t80=t80(1:idel(t80))//' - error map'
      call FeHeaderInfo(t80)
9999  return
      end
      subroutine ConRightButtons
      use Contour_mod
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension nmen1(11),xp(9),xpp(3),WorkMap(:),TMat(36),TMatI(36),
     1          rmx(9),rmb(36),CellParP(3)
      character*256 Veta,t256
      character*80 AtPDF,FileName
      character*21 form1
      character*10 SaveFormat
      character*6 t6
      integer ButtonStateQuest,FeGetSystemTime,TimeStart,Zdvih,
     1        HardCopyOld,FeMenuNew,ConGetFirstSection,HardCopyPom
      logical ExistFile,FeYesNo,WholeCell,NovyStart,Patterson
      allocatable WorkMap
      data form1/'(2(i5,''x''),i5,''='',i8)'/
      data AtPDF/' '/,SaveFormat/'(8e15.6)'/
      smapy=.false.
      NovyStart=.false.
1100  AllowResizing=.true.
      nLblMovie=0
      ConButtLabels(IdAtomsOnOff)='At%oms ON'
      ConButtLabels(IdSumOnOff)='S%um ON'
      ConButtLabels(IdTMapOnOff)='%t-map ON'
      ConButtLabels(IdErrOnOff)='%Err ON'
      CheckMouse=.true.
      call FeBottomInfo('#prazdno#')
      ContourQuest=NextQuestId()
      call FeQuestAbsCreate(ContourQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      ContourYSep(1)=pom
      j=1
      do i=1,IdOptions
        ActiveButtBas(i)=.false.
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,ConButtLabels(i))
        if(i.eq.IdQuit) then
          nButtQuit=ButtonLastMade
        else if(i.eq.IdPrint) then
          nButtPrint=ButtonLastMade
        else if(i.eq.IdSave) then
          nButtSave=ButtonLastMade
        else if(i.eq.IdRun3dMaps) then
          nButtRun3dMaps=ButtonLastMade
        else if(i.eq.IdNewMap) then
          nButtNew=ButtonLastMade
          wpom=wpom/2.-5.
          xsave=xpom
          ysave=ypom
          nButtBasFr=ButtonLastMade
        else if(i.eq.IdMMinus) then
          nButtMapaMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.IdMPlus) then
          nButtMapaPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.IdGoTo) then
          nButtGoTo=ButtonLastMade
        else if(i.eq.IdMovie) then
          nButtMovie=ButtonLastMade
        else if(i.eq.IdContours) then
          nButtContours=ButtonLastMade
        else if(i.eq.IdAtomsEdit) then
          nButtAtomsDefine=ButtonLastMade
        else if(i.eq.IdAtomsOnOff) then
          nButtAtomsDraw=ButtonLastMade
        else if(i.eq.IdAtomsFill) then
          nButtAtomsFill=ButtonLastMade
        else if(i.eq.IdX4Length) then
          nButtX4Length=ButtonLastMade
          if(NDimI(KPhase).le.0) then
            call FeQuestButtonRemove(ButtonLastMade)
            ypom=ypom+dpom
            go to 1500
          endif
        else if(i.eq.IdCurves) then
          nButtCurves=ButtonLastMade
        else if(i.eq.IdSumOnOff) then
          nButtSum=ButtonLastMade
        else if(i.eq.IdSearch) then
          nButtSearch=ButtonLastMade
        else if(i.eq.IdSearchAll) then
          nButtGlobal=ButtonLastMade
        else if(i.eq.IdTMapOnOff) then
          nButtTMaps=ButtonLastMade
          if(NDimI(KPhase).le.0) then
            call FeQuestButtonRemove(ButtonLastMade)
            cycle
          endif
        else if(i.eq.IdErrOnOff) then
          nButtErrMaps=ButtonLastMade
        else if(i.eq.IdOptions) then
          nButtOptions=ButtonLastMade
        endif
        ActiveButtBas(i)=.true.
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500    if(i.eq.IdRun3dMaps.or.i.eq.IdMovie.or.i.eq.IdX4Length.or.
     1     i.eq.IdErrOnOff) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          j=j+1
          ContourYSep(j)=ypom
          ypom=ypom-1.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+dpom-6.
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                       Gray,White)
      ContourYSep(5)=ypom
      xpom=xsave
      ypom=ysave
      do i=IdYScale,IdErrCurveOnOff
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,ConButtLabels(i))
        if(i.eq.IdYScale) then
          nButtYScale=ButtonLastMade
        else if(i.eq.IdDerOnOff) then
          nButtderOn=ButtonLastMade
        else if(i.eq.IdErrCurveOnOff) then
          nButtErrOn=ButtonLastMade
        endif
        ypom=ypom-dpom
      enddo
      ypom=ypom+dpom-6.
      ContourYSep(6)=ypom
      fpom=FeTxLength('XXX.XXX')
      call FeBoldFont
      dhl1=FeTxLength(ConWinfLabel(1))
      call FeNormalFont
      do i=1,4
        if(i.eq.1) then
          dpom=2.*fpom+5.
          xpom=XCenGrWin-.5*dhl1-dpom-20.
          ypom=YMaxBasWin-20.
        else if(i.eq.2) then
          dpom=3.*fpom+5.
          xpom=XCenGrWin+.5*dhl1+20.
        else if(i.eq.3) then
          xpom=XMaxGrWin-100.
          ypom=YMinGrWin-25.
          dpom=FeTxLength('XXXXXXX.XX')+5.
        else if(i.eq.4) then
          xpom=XMinGrWin+200.
          dpom=FeTxLength('XXX.XXX')+5.
        endif
        if(i.gt.2) then
          call FeQuestAbsLblMake(ContourQuest,xpom-10.,YBottomText,
     1                           ConWinfLabel(i-1),'R','N')
          call FeQuestLblOff(LblLastMade)
          nLblWinf(i)=LblLastMade
        endif
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      xpom=15.
      ypom=ypom+2.
      LocatorType=LocatorPosition
      do i=1,3
        if(i.eq.1) then
          Veta='$$Locate'
        else if(i.eq.2) then
          Veta='$$Distance'
        else if(i.eq.3) then
          Veta='$$Tilt'
        endif
        call FeQuestAbsCrwMake(ContourQuest,0.,ypom,xpom,ypom,Veta,'C',
     1                         CrwsXd,CrwsYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.LocatorType)
        if(i.eq.1) then
          nCrwLocPosition=CrwLastMade
        else if(i.eq.2) then
          nCrwLocDistance=CrwLastMade
        else if(i.eq.3) then
          nCrwLocNewSection=CrwLastMade
        else if(i.eq.4) then
          nCrwLocNewAtom=CrwLastMade
        endif
        xpom=xpom+CrwsXd+10.
      enddo
      write(form1(2:2),'(i1)') NDim(KPhase)-1
      NoOfDerivedMaps=0
      if(MapExists.and.NovyStart) then
        reconfig=.true.
        call KresliMapu(0)
        go to 2010
      endif
      MapExists=.false.
2000  call FeQuestButtonOff(nButtNew)
2010  NovyStart=.false.
      if(MapExists) then
        do i=nButtPrint,nButtErrMaps
          if(ActiveButtBas(i).and.
     1       i.ne.nButtNew.and.i.ne.nButtMapaMinus.and.
     2       i.ne.nButtMapaPlus)
     3    call FeQuestButtonOff(i)
        enddo
        call ConUpdateMapsButtons
        if(NDimI(KPhase).gt.0) then
          if(smapy.or.DrawPDF) then
            call FeQuestButtonDisable(nButtTMaps)
          else
            do i=4,NDim(KPhase)
              if(iorien(i).lt.4) go to 2055
            enddo
            call FeQuestButtonOff(nButtTMaps)
            go to 2060
2055        call FeQuestButtonDisable(nButtTMaps)
            if(xymap) then
              call FeQuestButtonDisable(nButtX4Length)
            else
              call FeQuestButtonOff(nButtX4Length)
            endif
          endif
        endif
      else
        do i=nButtPrint,nButtErrMaps
          if(ActiveButtBas(i).and.i.ne.nButtNew)
     1      call FeQuestButtonDisable(i)
        enddo
        call FeQuestMouseToButton(nButtNew)
      endif
2060  nCrw=nCrwLocPosition
      if(xymap.and.MapExists) then
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.LocatorType)
          nCrw=nCrw+1
        enddo
      else
        do i=1,3
          call FeQuestCrwClose(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      if(kpdf.ge.2.and.kpdf.ne.4.and.ExistFile(fln(:ifln)//'.m85').and.
     1   ErrMapActive) then
        call FeQuestButtonOff(nButtErrMaps)
      else
        call FeQuestButtonDisable(nButtErrMaps)
      endif
2090  JedeMovie=.false.
      if(MapExists) then
        call PCurves(1)
      else
        call FeQuestEvent(ContourQuest,ich)
        IPlane=0
        NoOfDerivedMaps=0
      endif
2100  HardCopy=0
      ErrFlag=0
      if(Allocated(WorkMap)) deallocate(WorkMap)
2110  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 9000
        else if(CheckNumber.eq.nButtNew) then
          ConButtLabels(IdAtomsOnOff)='At%oms ON'
          ConButtLabels(IdSumOnOff)='S%um ON'
          ConButtLabels(IdTMapOnOff)='%t-map ON'
          ConButtLabels(IdErrOnOff)='%Err ON'
          call ConNewMap(ich)
          if(ich.ne.0) go to 2000
          call iom50(0,0,fln(:ifln)//'.m50')
          if(ErrFlag.ne.0) go to 5000
          call FeClearGrWin
          MapExists=.false.
          Obecny=kpdf.ne.0.and.kpdf.ne.4
          DrawPDF=kpdf.gt.1.and.kpdf.ne.4.and.kpdf.ne.5
          isoucet=0
          if(smapy) then
            m8=81
            call FeQuestButtonLabelChange(nButtSum,
     1                                    ConButtLabels(IdSumOnOff))
            smapy=.false.
          endif
          tmapy=.false.
          call FeQuestButtonLabelChange(nButtTMaps,
     1                                  ConButtLabels(IdTMapOnOff))
          call FeQuestButtonLabelChange(nButtErrMaps,
     1                                  ConButtLabels(IdErrOnOff))
          if(kpdf.le.1.or.kpdf.eq.4.or.kpdf.eq.5) then
            call NactiM81
            if(ErrFlag.ne.0) go to 5000
            if(.not.Obecny) then
              call ConReadKeys
              IPlane=-1
            endif
          else
            if(kpdf.ge.3) nsubs=1
            do i=1,6
              if(i.le.NDim(KPhase)) then
                iorien(i)=i
              else
                iorien(i)=0
              endif
            enddo
            call Del8
          endif
          if(kpdf.ge.1.and.kpdf.ne.4) then
            if(DrawPDF) then
              WizardId=NextQuestId()
              WizardMode=.true.
              WizardTitle=.true.
              WizardLines=16
              WizardLength=650.
              call FeQuestCreate(WizardId,-1.,-1.,WizardLength,
     1                           WizardLines,'x',0,LightGray,0,0)
2200          call ConDefDensity(AtPDF,ich)
              if(ich.ne.0) go to 2350
2220          if(kpdf.ne.6.and.DensityType.eq.2) then
                call ConDefDeform(ich)
                if(ich.lt.0) then
                  go to 2350
                else if(ich.gt.0) then
                  go to 2200
                endif
              endif
2240          call ConDefGenSection(AtPDF,1,ich)
              if(ich.ne.0) then
                if(ich.lt.0) then
                  go to 2350
                else
                  if(kpdf.ne.6.and.DensityType.eq.2) then
                    go to 2220
                  else
                    go to 2200
                  endif
                endif
              endif
              if(kpdf.ge.3) then
                if(kpdf.eq.3) then
                  klic=0
                else if(kpdf.eq.6) then
                  klic=1
                else
                  klic=2
                endif
                call ConDefAtForDenCalc(klic,ich)
                if(ich.lt.0) then
                  go to 2350
                else if(ich.gt.0) then
                  go to 2240
                endif
              else
                do i=1,5
                  BratAnharmPDF(i,1)=AtBratAnharm(i,iapdf(1))
                enddo
              endif
              do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
                SelPDF(i)=.false.
                do j=1,npdf
                  if(iapdf(j).eq.i) then
                    SelPDF(i)=.true.
                    cycle
                  endif
                enddo
              enddo
              call FeQuestRemove(WizardId)
              WizardMode=.false.
            else
              if(NoOfDerivedMaps.le.0) then
                call ConDefGenSection(' ',0,ich)
              else
                call ConMakeGenSection(ich)
              endif
              if(ich.ne.0) go to 2350
              ErrMapActive=.false.
            endif
            if(kpdf.le.3.or.kpdf.eq.5.or.kpdf.eq.6) then
              call ConCalcGeneral
              if(ErrFlag.ne.0) go to 2350
              errdraw=.false.
            endif
          endif
          do i=3,6
            nxfrom(i-2)=1
            nxto(i-2)=nx(i)
            if(Obecny.and.i.eq.3) then
              nxdraw(i-2)=(nx(i)+1)/2
            else
              nxdraw(i-2)=1
            endif
          enddo
          irec=-1
          irecold=-1
          reconfig=.true.
          if(xymap) call TrPor
          call SetTr
          if(kpdf2-kpdf1.le.1) go to 5000
          call KresliMapu(0)
          KeysSaved=.false.
2350      if(WizardMode) call FeQuestRemove(WizardId)
          if(kpdf1.ge.kpdf2) call FeQuestButtonDisable(nButtNew)
          go to 2000
        else if(CheckNumber.eq.nButtSave) then
          if(HardCopy.eq.0) then
            if(Obecny) then
              i=0
              j=0
            else
              i=10
              j=0
            endif
            call FeSavePicture('section',i,j)
            if(HardCopy.lt.0) HardCopy=0
          endif
          if(HardCopy.lt.HardCopyNum) then
            if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                              HardCopy.ne.HardCopyPCX)) then
              call KresliMapu(0)
            else
              call FeHardCopy(HardCopy,'open')
              call KresliMapu(0)
              call FeHardCopy(HardCopy,'close')
            endif
          else if(HardCopy.ge.HardCopyNum) then
            if(kpdf.eq.0.and.mod(HardCopy,100).lt.HardCopyXPlor) then
              do i=2,3
                if(abs(CellPar(1,1,KPhase)-CellPar(i,1,KPhase)).gt..001)
     1            go to 2450
              enddo
              do i=4,6
                if(abs(CellPar(i,1,KPhase)-90.).gt..001) go to 2450
              enddo
              go to 2460
2450          call FeChybne(-1.,-1.,
     1                      'the maps are related to the crystal',
     2                      ' non-orthogonal system!!!',Warning)
            endif
2460        call OpenFile(85,HCFileName,'formatted','unknown')
            ivse=HardCopy/100
            HardCopyPom=mod(HardCopy,100)
            if(ivse.eq.0) then
              zmn=zmap(1)
              zmx=zmap(1)
              NzCopy=1
            else
              zmn=xmin(3)
              zmx=xmax(3)
              NzCopy=nx(3)
            endif
            allocate(WorkMap(nxny))
            if(mod(HardCopy,100).eq.HardCopySTF) then
              write(85,'(''NAME '',80a1)')
     1          (StructureName(i:i),i=1,idel(StructureName))
              write(85,'(''RANK 3'')')
              write(85,'(''DIMENSIONS '',3i5)')(nx(i),i=1,2),NzCopy
              write(85,'(''BOUNDS '',6f9.4)')(xmin(i),xmax(i),i=1,2),
     1                                       zmn,zmx
              write(85,'(''SCALAR'')')
              write(85,'(''ORDER COLUMN'')')
              write(85,'(''DATA'')')
              i=8
              k=10
              t6='stf'
            else if(mod(HardCopy,100).eq.HardCopyXPlor) then
              call ConMakeXplorFileProlog(85,NzCopy,0)
              t6='xplor'
            else if(mod(HardCopy,100).eq.HardCopyJMAP3D) then
              if(Obecny) then
                write(Veta,102)(dx(i),i=1,3)
                write(Veta(31:),102)(90.,i=1,3)
              else
                m=0
                do i=1,3
                  if(nx(i).le.1) cycle
                  m=m+1
                  CellParP(m)=CellParCon(m)*dx(i)
                enddo
                call ConCellParNorm(CellParP)
                write(Veta,102)(CellParP(i),i=1,3)
                write(Veta(31:),102)(CellParCon(i),i=4,6)
              endif
              call zdrcniCisla(Veta,6)
              write(85,FormA1)(Veta(i:i),i=1,idel(Veta))
              write(85,'(2(i4,'',''),i4)')(nx(i),i=1,2),NzCopy
              t6='JMAP3D'
            else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
              write(85,'(''INFO  DOWN, ACROSS AND SECTION'')')
              write(85,'(''TRAN'')')
              write(85,103)((trob(i,j),j=1,3),i=1,3)
              call SetRealArrayTo(xpp,3,0.)
              call Prevod(1,xpp,xp)
              write(85,103)(xp(i),i=1,3)
              write(85,'(''CELL'')')
              m=0
              do i=1,3
                if(nx(i).le.1) cycle
                m=m+1
                CellParP(m)=CellParCon(m)
              enddo
              call ConCellParNorm(CellParP)
              write(85,103)(CellParP(i),i=1,3)
              write(85,103)(CellParCon(i)*ToRad,i=4,6)
              write(85,'(''L14'')')
              write(85,103)(xmin(i),dx(i),xmax(i),1.,i=1,2),
     1                      zmn,dx(3),zmx,1.
              write(85,'(''SIZE'')')
              write(85,104)(nx(i),i=1,2),NzCopy
              t6='fou'
            else if(mod(HardCopy,100).eq.HardCopyGrd) then
              call ConMakeGrdFileProlog(85,NzCopy,0)
              t6='grd'
            else if(mod(HardCopy,100).eq.HardCopyNum) then
              if(ivse.eq.0) then
                write(85,101)(nx(i),i=1,2),(1,i=3,NDim(KPhase))
              else
                write(85,101)(nx(i),i=1,NDim(KPhase))
              endif
              t6='ASCII'
            endif
            if(ivse.eq.0) then
              read(m8,rec=irec+1,err=2510)(WorkMap(j),j=1,nxny)
              if(mod(HardCopy,100).eq.HardCopySTF) then
                write(85,SaveFormat)(WorkMap(j),j=1,nxny)
                write(85,'(''END'')')
              else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
                write(85,'(''BLOCK'')')
                write(85,104) nxny
                write(85,103)(WorkMap(j),j=1,nxny)
              else if(mod(HardCopy,100).eq.HardCopyGrd) then
                m=0
                do j=1,nx(2)
                  write(85,107)(WorkMap(k),k=m+1,m+nx(1))
                  m=m+nx(1)
                enddo
              else
                write(85,SaveFormat)(WorkMap(i),i=1,nxny)
              endif
              write(TextInfo(1),form1)(nx(i),i=1,2),
     1                                (1,i=3,NDim(KPhase)),nxny
            else
              call FeFlowChartOpen(-1.,-1.,1,NzCopy,'Transporting '//
     1          'data to the '//t6(:idel(t6))//' file',' ',' ')
              Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
              do i=1,NzCopy
                if(mod(HardCopy,100).eq.HardCopyXplor)
     1            write(85,'(i5)') i-1
                j=i
                call FeFlowChartEvent(j,is)
                if(is.ne.0) then
                  close(85,status='delete')
                  call FeFlowChartRemove
                  go to 2000
                endif
                read(m8,rec=i+Zdvih,err=2510)(WorkMap(j),j=1,nxny)
                if(mod(HardCopy,100).eq.HardCopyJMAP3D) then
                  m=0
                  do j=1,nx(2)
                    do k=1,nx(1)
                      m=m+1
                      write(85,100) k-1,j-1,i-1,WorkMap(m)
                    enddo
                  enddo
                else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
                  write(85,'(''BLOCK'')')
                  write(85,104) nxny
                  write(85,103)(WorkMap(j),j=1,nxny)
                else if(mod(HardCopy,100).eq.HardCopyGrd) then
                  m=0
                  do j=1,nx(2)
                    write(85,107)(WorkMap(k),k=m+1,m+nx(1))
                    m=m+nx(1)
                  enddo
                else if(mod(HardCopy,100).eq.HardCopyXplor) then
                  write(85,'(5e15.6)')(WorkMap(j),j=1,nxny)
                else
                  write(85,SaveFormat)(WorkMap(j),j=1,nxny)
                endif
              enddo
              if(mod(HardCopy,100).eq.HardCopySTF) write(85,'(''END'')')
              call FeFlowChartRemove
              irecold=-1
              write(TextInfo(1),form1)(nx(i),i=1,NDim(KPhase)),nxny*nmap
            endif
            go to 2550
2510        call FeReadError(81)
            close(85,status='delete')
            NInfo=1
            TextInfo(1)='No output file created'
            go to 2800
2550        call CloseIfOpened(85)
            ninfo=2
            call zhusti(TextInfo(1))
            i=idel(TextInfo(1))
            TextInfo(1)='All '//TextInfo(1)(:i)//' points were copied '
            if(mod(HardCopy,100).eq.HardCopyNum) then
              TextInfo(2)='to the ASCII file : '//HCFileName
            else
              TextInfo(2)='to the file : '//HCFileName
            endif
            if(Allocated(WorkMap)) deallocate(WorkMap)
2800        call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            call KresliMapu(0)
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            HardCopy=0
          endif
        else if(CheckNumber.eq.nButtRun3dMaps) then
          Patterson=Mapa.eq.1.or.Mapa.eq.2.or.Mapa.eq.9
          ln=NextLogicNumber()
          if(IdCall3dMaps.eq.IdCall3dNone) go to 2000
          if(xyzmap) then
            WholeCell=.true.
            do i=1,3
              pom=xmax(i)-xmin(i)+dx(i)
              if(abs(pom-1.).gt..001.and.WholeCell) then
                WholeCell=.false.
                exit
              endif
            enddo
          else
            WholeCell=.false.
          endif
          t256=' '
          if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1       IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
            FileName=fln(:ifln)//'_tmp.grd'
            if(IdCall3dMaps.eq.IdCall3dMCE.and.NAtAll.gt.0.and.
     1         .not.Obecny.and..not.Patterson) then
              t256=fln(:ifln)//'_tmp.cif'
              call CIFMakeBasicTemplate(t256,0)
              call CIFUpdate(t256,.true.,0)
            endif
          else
            if(Obecny) then
              FileName=fln(:ifln)//'_tmp.xsf'
            else
              FileName=fln(:ifln)//'_tmp.xplor'
            endif
          endif
          call OpenFile(ln,FileName,'formatted','unknown')
          if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1       IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
            if(obecny.or.IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
              i=0
            else
              i=1
            endif
            call ConMakeGrdFile(ln,nx(3),i)
          else
            do i=3,NDim(KPhase)
              if(nx(i).gt.1) then
                nx3=nx(i)
                exit
              endif
            enddo
            if(Obecny) then
              call ConMakeXSFFile(ln,nx3,0)
            else
              call ConMakeXPlorFile(ln,nx3,0)
            endif
          endif
          call CloseIfOpened(ln)
          if(IdCall3dMaps.eq.IdCall3dVESTA) then
            if(.not.Obecny) then
              MakeCIFForGraphicViewer=.true.
              call SetRealArrayTo(TMat,NDimQ(KPhase),0.)
              k=0
              do i=1,NDim(KPhase)
                do j=1,NDim(KPhase)
                  k=k+1
                  if(j.eq.iorien(i)) TMat(k)=xmax(i)-xmin(i)+dx(i)
                enddo
              enddo
              call MatInv(TMat,TMatI,pom,NDim(KPhase))
              call MatBlock3(TMatI,rmx,NDim(KPhase))
              call SrotB(rmx,rmx,rmb)
              call UnitMat(TMat,NDim(KPhase))
              call MatInv(TMat,TMatI,pom,NDim(KPhase))
              open(m40,file=fln(:ifln)//'_tmp.m40')
              do i=1,5
                write(m40,FormA)
              enddo
              NaUse=0
              if(WholeCell) then
                if(iorien(1).le.3.and.iorien(2).le.3) then
                  do i=1,NAtCalc
                    if(kswa(i).eq.KPhase) NaUse=NaUse+1
                  enddo
                endif
                do i=1,NAtCalc
                  if(kswa(i).eq.KPhase) then
                    call MultM(rmx,x(1,i),xp,3,3,1)
                    itfp=min(itf(i),2)
                    if(itf(i).gt.1) then
                      call MultM(rmb,beta(1,i),xp(4),6,6,1)
                      do j=1,6
                        beta(j,i)=beta(j,i)/urcp(j,iswa(i),KPhase)
                      enddo
                    else
                      xp(4)=beta(1,i)/episq
                      call SetRealArrayTo(xp(5),5,0.)
                    endif
                    write(m40,108) Atom(i),isf(i),itfp,ai(i),
     1                             (xp(j),j=1,9)
                  endif
                enddo
              else
                do i=1,3
                  CellPar(i,1,KPhase)=
     1              CellParCon(i)*(xmax(i)-xmin(i)+dx(i))
                enddo
                do i=4,6
                  CellPar(i,1,KPhase)=CellParCon(i)
                enddo
                call ConCellParNorm(CellPar(1,1,KPhase))
                Grupa(KPhase)='P1'
                NSymm(KPhase)=1
                NSymmN(KPhase)=1
                NLattVec(KPhase)=1
                NGrupa(KPhase)=1
                CrSystem(KPhase)=1
                Lattice(KPhase)='P'
                Veta=fln(:ifln)//'_dratoms_vesta.tmp'
                if(ExistFile(Veta)) then
                  lna=NextLogicNumber()
                  call OpenFile(lna,Veta,'formatted','unknown')
                  if(ErrFlag.ne.0) go to 2860
2820              read(lna,'(2i3,6f10.5)',end=2860) ia,NumAt,xpp
                  call MultM(rmx,xpp,xp,3,3,1)
                  do i=1,3
                    if(xp(i).lt.0.or.xp(i).gt.1.) go to 2820
                  enddo
                  NaUse=NaUse+1
                  write(m40,108) Atom(ia),isf(ia),1,1.,(xp(i),i=1,3),
     1                           .01
                  go to 2820
2860              call CloseIfOpened(lna)
                endif
              endif
              rewind m40
              do i=1,5
                read(m40,FormA)
              enddo
              n=NAtCalc
              NAtCalc=NaUse
              Veta=fln(:ifln)//'_tmp.cif'
              call CIFMakeBasicTemplate(Veta,1)
              call CIFUpdate(Veta,.true.,1)
              close(m40,status='delete')
              NAtCalc=n
              MakeCIFForGraphicViewer=.false.
              Veta=' '
            endif
            FileName=fln(:ifln)//'_tmp.vesta'
            call OpenFile(ln,FileName,'formatted','unknown')
            write(ln,FormA) '#VESTA_FORMAT_VERSION 2'
            write(ln,FormA)
            write(ln,FormA) 'IMPORT_STRUCTURE'
            if(Obecny) then
              write(ln,FormA) fln(:ifln)//'_tmp.xsf'
            else
              write(ln,FormA) fln(:ifln)//'_tmp.cif'
            endif
            write(ln,FormA)
            write(ln,FormA) 'IMPORT_DENSITY'
            if(Obecny) then
              write(ln,FormA) '+1.00000 '//fln(:ifln)//'_tmp.xsf'
            else
              write(ln,FormA) '+1.00000 '//fln(:ifln)//'_tmp.xplor'
              write(ln,FormA)
              write(ln,FormA) 'TRANM'
              write(ln,'(3f10.6,9i4)')(0.,i=1,3),(nint(TMat(i)),i=1,9)
            endif
            write(ln,FormA)
            call CloseIfOpened(ln)
          endif
          Veta=Call3dMaps(:idel(Call3dMaps))
          if(IdCall3dMaps.ne.IdCall3dMoleCoolQt) then
            Veta=Call3dMaps(:idel(Call3dMaps))//' '//
     1           FileName(:idel(FileName))//' '//t256(:idel(t256))
          else
            call FeFillTextInfo('contour1.txt',0)
            i=index(TextInfo(1),'#$%')
            TextInfo(1)(i:)=FileName(:idel(FileName))//'"'
            if(.not.Obecny) then
              do i=4,6
                if(abs(CellPar(i,1,KPhase)-90.).gt..001) then
                  NInfo=NInfo+1
                  TextInfo(NInfo)='WARNING: The maps are related to '//
     1                            'the non-orthogonal system which '//
     2                            'not supported by the grd format.'
                  NInfo=NInfo+1
                  TextInfo(NInfo)='For this reason the maps in '//
     1                            'MoleCoolQt are deformated.'
                  exit
                endif
              enddo
            endif
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeSystem(Veta)
          call DeleteFile(FileName)
          if(IdCall3dMaps.eq.IdCall3dMCE.and.t256.ne.' ')
     1       call DeleteFile(t256)
          if(IdCall3dMaps.eq.IdCall3dVESTA) then
            call DeleteFile(fln(:ifln)//'_tmp.xplor')
            call DeleteFile(fln(:ifln)//'_tmp.cif')
            call DeleteFile(fln(:ifln)//'_tmp.xsf')
            call iom40(0,0,fln(:ifln)//'.m40')
            call iom50(0,0,fln(:ifln)//'.m50')
            if(NDimI(KPhase).gt.0) call TrOrtho(0)
          endif
        else if(CheckNumber.eq.nButtMapaMinus) then
          call FeQuestButtonOff(nButtMapaMinus)
          call KresliMapu(-1)
          if(nButtInterrupt.gt.0) then
            call ConUpdateMapsButtons
            if(ButtonState(nButtMapaMinus).eq.ButtonOff) then
              CheckType=EventButton
              CheckNumber=nButtInterrupt
              go to 2110
            endif
          endif
        else if(CheckNumber.eq.nButtMapaPlus) then
          call FeQuestButtonOff(nButtMapaPlus)
          call KresliMapu( 1)
          if(nButtInterrupt.gt.0) then
            call ConUpdateMapsButtons
            if(ButtonState(nButtMapaPlus).eq.ButtonOff) then
              CheckType=EventButton
              CheckNumber=nButtInterrupt
              go to 2110
            endif
          endif
        else if(CheckNumber.eq.nButtGoto) then
          call GoToMap(ich)
          if(ich.eq.0) go to 4600
        else if(CheckNumber.eq.nButtMovie) then
          call ConDefMovie(ich)
          if(ich.ne.0) go to 2000
          if(nLblMovie.le.0) then
            xpom=350.
            ypom=YBottomMargin*.5
            Veta='Press Esc to interrupt the movie'
            call FeQuestAbsLblMake(ContourQuest,xpom,ypom,Veta,
     1                             'L','B')
            nLblMovie=LblLastMade
          else
            call FeQuestLblOn(nLblMovie)
          endif
          JedeMovie=.true.
          irpt=0
          isv=0
          HardCopyOld=HardCopy
3000      irpt=irpt+1
          if(irpt.gt.MovieRepeat.and.MovieRepeat.ne.0) go to 3200
          call CopyVekI(nxMovieFr,nxdraw,NDim(KPhase)-2)
3050      if(irpt.eq.1.and.MovieFileName.ne.' ') then
            isv=isv+1
            write(HCFileName,'(''_'',i4)') isv
            do i=1,idel(HCFileName)
              if(HCFileName(i:i).eq.' ') HCFileName(i:i)='0'
            enddo
            HCFileName=MovieFileName(:idel(MovieFileName))//
     1               HCFileName(:idel(HCFileName))//
     2               HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
          else
            HardCopy=0
          endif
          call KresliMapu( 0)
          if(ErrFlag.ne.0) go to 3200
          if(HardCopy.ne.0) then
            i=HardCopy
            HardCopy=0
            call KresliMapu( 0)
            if(ErrFlag.ne.0) go to 3200
            HardCopy=i
          endif
          TimeStart=FeGetSystemTime()
3100      call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                        ConImageFile,0)
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile)
            if(FeYesNo(-1.,-1.,
     1                  'Do you want really to interrupt it?',1))
     2        go to 3200
          else if(EventType.eq.EventAscii.and.
     1            char(EventNumber).eq.' ') then
            call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                        ConImageFile,0)
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile)
3120        call FeEvent(1)
            if(EventType.ne.EventAscii.or.char(EventNumber).ne.' ')
     1        go to 3120
          else
            if(FeGetSystemTime()-TimeStart.le.
     1         nint(DelayTimeForMovie*1000.)) go to 3100
          endif
          do i=1,NDim(KPhase)-2
            if(nxlast(i).lt.nxMovieTo(i)) go to 3150
          enddo
          go to 3000
3150      nxdraw(1)=nxdraw(1)+1
          if(nxdraw(1).gt.nxMovieTo(1)) then
            nxdraw(1)=nxMovieFr(1)
            nxdraw(2)=nxdraw(2)+1
            if(nxdraw(2).gt.nxMovieTo(2)) then
              nxdraw(2)=nxMovieFr(2)
              nxdraw(3)=nxdraw(3)+1
              if(nxdraw(3).gt.nxMovieTo(3)) then
                nxdraw(3)=nxMovieFr(3)
                nxdraw(4)=nxdraw(4)+1
                if(nxdraw(4).gt.nxdraw(4)) then
                  call CopyVekI(nxMovieTo,nxdraw,4)
                endif
              endif
            endif
          endif
          go to 3050
3200      if(MovieFileName.ne.' ') then
            TextInfo(1)='The movie maps were recorded to files: '//
     1        MovieFileName(:idel(MovieFileName))//'_####'//
     2        HCExtension(HardCopyOld)(:idel(HCExtension(HardCopyOld)))
            NInfo=1
            WaitTime=10000
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeQuestLblOff(nLblMovie)
        else if(CheckNumber.eq.nButtContours) then
          call DefContour(ich)
          if(ich.eq.0) go to 4600
        else if(CheckNumber.eq.nButtAtomsDefine) then
          call ConDrawAtomsEdit
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     ConImageFile,-1)
          ConButtLabels(IdAtomsOnOff)='At%oms OFF'
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
          AtomsOn=.true.
          call ConDrawAtoms(zmap)
        else if(CheckNumber.eq.nButtAtomsDraw) then
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     ConImageFile,-1)
          if(AtomsOn) then
            ConButtLabels(IdAtomsOnOff)='At%oms ON'
            AtomsOn=.false.
            call DeleteFile(fln(:ifln)//'_dratoms_grd.tmp')
            call DeleteFile(fln(:ifln)//'_dratoms_vesta.tmp')
          else
            if(ErrFlag.eq.0.and.DrawAtN.gt.0) then
              ConButtLabels(IdAtomsOnOff)='At%oms OFF'
              AtomsOn=.true.
              call ConDrawAtoms(zmap)
            endif
          endif
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
          go to 2000
        else if(CheckNumber.eq.nButtAtomsFill) then
          call ConFillAtoms(ich)
          ConButtLabels(IdAtomsOnOff)='At%oms OFF'
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
        else if(CheckNumber.eq.nButtCurves) then
          call PCurves(0)
        else if(CheckNumber.eq.nButtX4Length) then
          call DefLength(ich)
          reconfig=ich.eq.0
          if(reconfig) go to 4600
        else if(CheckNumber.eq.nButtSum) then
          if(smapy) then
            m8=m8Sum
            ConButtLabels(IdSumOnOff)='S%um ON'
            smapy=.false.
          else
            m8Sum=m8
            call ConSummMapsDef
            if(ErrFlag.ne.0.or..not.smapy) go to 2000
            ConButtLabels(IdSumOnOff)='S%um OFF'
          endif
          call FeQuestButtonLabelChange(nButtSum,
     1                                  ConButtLabels(IdSumOnOff))
          go to 4500
        else if(CheckNumber.eq.nButtSearch) then
          call ConSearch
        else if(CheckNumber.eq.nButtGlobal) then
          call ConSearchGlobal
        else if(CheckNumber.eq.nButtErrMaps) then
          irecold=-1
          errdraw=.not.errdraw
          if(errdraw) then
            m8=83
            DiffMapa=.false.
            DrawPos=1
            DrawNeg=0
          else
            m8=82
            if(DensityType.eq.2.or.DensityType.eq.3) then
              DiffMapa=.true.
              DrawPos=1
              DrawNeg=1
            else
              DiffMapa=.false.
              DrawPos=1
              DrawNeg=0
            endif
          endif
          pom=DminErr
          DminErr=Dmin
          Dmin=pom
          pom=DmaxErr
          DmaxErr=Dmax
          Dmax=pom
          i=ContourTypeErr
          ContourTypeErr=ContourType
          ContourType=i
          if(ErrDraw) then
            ConButtLabels(IdErrOnOff)='%Err OFF'
          else
            ConButtLabels(IdErrOnOff)='%Err ON'
          endif
          call FeQuestButtonLabelChange(nButtErrMaps,
     1                                  ConButtLabels(IdErrOnOff))
          go to 4600
        else if(CheckNumber.eq.nButtTMaps) then
          if(tmapy) then
            m8=m8TMaps
            ConButtLabels(IdTMapOnOff)='%t-map ON'
            tmapy=.false.
          else
            m8TMaps=m8
            call TrX4t
            if(ErrFlag.ne.0) go to 2000
            ConButtLabels(IdTMapOnOff)='%t-map OFF'
          endif
          call FeQuestButtonLabelChange(nButtTMaps,
     1                                  ConButtLabels(IdTMapOnOff))
          call SetTr
          go to 4500
        else if(CheckNumber.eq.nButtOptions) then
          i=ConTintType
          icp=ConPosCol
          icn=ConNegCol
          call ConOptions
          if(i.ne.ConTintType.or.icp.ne.ConPosCol.or.
     1                                icn.ne.ConNegCol) then
            call KresliMapu(0)
          else if(AtomsOn) then
            call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile,-1)
            call ConDrawAtoms(zmap)
          endif
        endif
        go to 2000
4500    read(m8,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                 iorien,mapa,nsubs,SatelityByly,
     2                 nonModulated(KPhase)
        Patterson=Mapa.eq.1.or.Mapa.eq.2.or.Mapa.eq.9
        nsubs=mod(nsubs,10)
        read(m8,rec=nmap+2) Dmax,Dmin
        call SetContours(0)
        call SetIntArrayTo(nxdraw,4,1)
        call SetIntArrayTo(nxfrom,4,1)
        call CopyVekI(nx(3),nxto,4)
        if(Obecny) then
          nxdraw(1)=(nx(3)+1)/2
        else
          nxdraw(1)=0
        endif
        irec=-1
        irecold=-1
4600    call KresliMapu(0)
        go to 2000
5000    call FeQuestButtonOff(CheckNumber)
        CheckType=EventButton
        CheckNumber=nButtQuit
        go to 2110
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown
     1        .and.MapExists) then
        do j=1,11
          if(ButtonStateQuest(nButtMovie+j).eq.ButtonOff) then
            nmen1(j)=1
          else
            nmen1(j)=0
          endif
        enddo
        j=FeMenuNew(-1.,-1.,-1.,ConButtLabels(IdContours),nmen1,1,11,
     1              1,0)
        if(j.lt.1) go to 2090
        CheckType=EventButton
        CheckNumber=nButtMovie+j
        go to 2100
      else if(CheckType.eq.EventResize) then
        NovyStart=.true.
        go to 9000
      else
        go to 2090
      endif
      go to 2000
9000  do i=1,4
        call FeWInfRemove(i)
      enddo
      call FeQuestRemove(ContourQuest)
      if(Allocated(WorkMap)) deallocate(WorkMap)
      if(NovyStart) go to 1100
      call Del8
      AllowResizing=.false.
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call FeBottomInfo(' ')
      return
100   format(3(i4,','),e20.10)
101   format(6i5)
102   format(3f10.4)
103   format(f15.8)
104   format(i8)
105   format(3i15)
106   format(3f15.4)
107   format(6e13.5)
108   format(a8,2i3,4x,4f9.6/6f9.6)
      end
      subroutine ConUpdateMapsButtons
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer ButtonStateQuest
      do j=1,NDim(KPhase)-2
        if(nxdraw(j).gt.nxfrom(j)) then
          call FeQuestButtonOff(nButtMapaMinus)
          go to 2010
        endif
      enddo
      call FeQuestButtonDisable(nButtMapaMinus)
2010  do j=1,NDim(KPhase)-2
        if(nxdraw(j).lt.nxto(j)) then
          call FeQuestButtonOff(nButtMapaPlus)
          go to 2040
        endif
      enddo
      call FeQuestButtonDisable(nButtMapaPlus)
2040  if(ButtonStateQuest(nButtMapaPlus ).eq.ButtonOff.or.
     1   ButtonStateQuest(nButtMapaMinus).eq.ButtonOff) then
        call FeQuestButtonOff(nButtGoto)
      else
        call FeQuestButtonDisable(nButtGoto)
      endif
      if(DrawAtN.gt.0) then
        call FeQuestButtonOff(nButtAtomsDraw)
      else
        call FeQuestButtonDisable(nButtAtomsDraw)
      endif
      if(xyzmap.and.NAtCalc.gt.0) then
        call FeQuestButtonOff(nButtAtomsFill)
      else
        call FeQuestButtonDisable(nButtAtomsFill)
      endif
      if(MapExists) then
c        if(Obecny) then
          i=ButtonOff
c        else
c          i=ButtonDisabled
c        endif
      else
        i=ButtonDisabled
      endif
      call FeButtonOpen(nButtRun3dMaps,i)
      return
      end
      subroutine ConMakeGrdFile(ln,NzCopy,Type)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension WorkMap(:,:),CellParP(3)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      logical ExistFile
      allocatable WorkMap
      allocate(WorkMap(nxny,NzCopy))
      Klic=0
      go to 1000
      entry ConMakeGrdFileProlog(ln,NzCopy,Type)
      Klic=1
1000  if(Type.eq.0) then
        if(NzCopy.eq.1) then
          Veta='2'
        else
          Veta='3'
        endif
        Veta(2:)='DGRDFIL  0'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='FORM     FOU'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        Veta='! Gridpoints, Origin, Physical Dimensions'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105)(nx(i),i=1,2),NzCopy
        if(Obecny) then
          write(ln,106) ShiftPlane,ScopePlane
        else
          write(ln,106)((xmax(i)-xmin(i))*.5*CellParCon(i),i=1,3),
     1                 ((xmax(i)-xmin(i))   *CellParCon(i),i=1,3)
        endif
        Veta='! Objects'
        write(ln,FormA) Veta(:idel(Veta))
        n=0
        Veta=fln(:ifln)//'_dratoms_grd.tmp'
        if(ExistFile(Veta)) then
          lna=NextLogicNumber()
          call OpenFile(lna,Veta,'formatted','unknown')
          if(ErrFlag.ne.0) go to 1200
1100      read(lna,FormA,end=1200) Veta
          n=n+1
          go to 1100
        else
          lna=0
        endif
1200    write(ln,105) n
        if(n.gt.0) then
          rewind lna
1400      read(lna,FormA,end=1600) Veta
          write(ln,FormA) Veta(:idel(Veta))
          go to 1400
        endif
1600    call CloseIfOpened(lna)
        Veta='! Connections'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105) 0
        Veta='! Values'
        write(ln,FormA) Veta(:idel(Veta))
      else
        write(ln,FormA) fln(:ifln)
        m=0
        do i=1,3
          if(nx(i).le.1) cycle
          m=m+1
          CellParP(m)=CellParCon(m)*(xmax(i)-xmin(i))
        enddo
        call ConCellParNorm(CellParP)
        write(Veta,102)(CellParP(i),i=1,3)
        write(Veta(31:),102)(CellParCon(i),i=4,6)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105)(nx(i),i=1,2),NzCopy
      endif
      if(Klic.eq.1) go to 9999
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j,i),j=1,nxny)
        if(Type.eq.0) then
          m=0
          do j=1,nx(2)
            write(ln,107)(WorkMap(l,i),l=m+1,m+nx(1))
            m=m+nx(1)
          enddo
        endif
      enddo
      if(Type.ne.0) then
        do j=1,nx(1)
          do k=1,nx(2)
            m=(k-1)*nx(1)+j
            write(ln,108)(WorkMap(m,i),i=1,NzCopy)
          enddo
        enddo
      endif
      go to 9999
5000  call FeReadError(m8)
9999  if(allocated(WorkMap)) deallocate(WorkMap)
      return
102   format(3f10.4)
105   format(3i15)
106   format(3f15.4)
107   format(6e13.5)
108   format(e13.5)
      end
      subroutine ConMakeXPlorFile(ln,NzCopy,Type)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension WorkMap(:),CellParP(6)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      allocatable WorkMap
      allocate(WorkMap(nxny))
      Klic=0
      go to 1000
      entry ConMakeXplorFileProlog(ln,NzCopy,Type)
      Klic=1
1000  write(ln,FormA)
      write(ln,'(i5)') 1
      write(ln,'(''Xplor'',a)') fln(:ifln)
      write(ln,'(9i8)')(nx(i),0,nx(i)-1,i=1,2),
     1                 NZCopy,0,NZCopy-1
      m=0
      do i=1,NDim(KPhase)
        if(nx(i).le.0) cycle
        m=m+1
        CellParP(m)=CellParCon(m)*(xmax(i)-xmin(i)+dx(i))
      enddo
      call ConCellParNorm(CellParP)
      write(Veta,102)(CellParP(i),i=1,3)
      write(Veta(idel(Veta)+1:),102)(CellParCon(i),i=4,6)
      write(ln,FormA) Veta(:idel(Veta))
      Veta='zyx'
      call Velka(Veta)
      write(ln,FormA) Veta(:idel(Veta))
      if(Klic.eq.1) go to 9999
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      Ave=0.
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j),j=1,nxny)
        write(ln,105) i-1
        write(ln,107)(WorkMap(j),j=1,nxny)
        do j=1,nxny
          Ave=Ave+WorkMap(j)
        enddo
      enddo
      write(ln,105) -9999
      write(ln,'(2e15.6)') ave/float(nxny*NzCopy)
      go to 9999
5000  call FeReadError(m8)
9999  if(allocated(WorkMap)) deallocate(WorkMap)
      return
102   format(3e12.4)
105   format(3i15)
107   format(6e12.5)
      end
      subroutine ConMakeXSFFile(ln,NzCopy,Type)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real, allocatable :: WorkMap(:)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      logical ExistFile
      allocate(WorkMap(nxny))
      write(ln,FormA) 'CRYSTAL'
      write(ln,FormA) 'PRIMVEC'
      if(Obecny) then
        write(ln,102) float(nx(1)-1)*dx(1),0.,0.
        write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
        write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      else
        write(ln,102)(TrToOrtho(i,1,KPhase),i=1,9)
      endif
      write(ln,FormA) 'CONVVEC'
      if(Obecny) then
        write(ln,102) float(nx(1)-1)*dx(1),0.,0.
        write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
        write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      else
        write(ln,102)(TrToOrtho(i,1,KPhase),i=1,9)
      endif
      Veta=fln(:ifln)//'_dratoms_vesta.tmp'
      if(ExistFile(Veta)) then
        write(ln,FormA) 'PRIMCOORD'
        lna=NextLogicNumber()
        call OpenFile(lna,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1200
        n=0
1100    read(lna,FormA,end=1150) Veta
        n=n+1
        go to 1100
1150    rewind lna
        write(ln,'(2i5)') n,1
1170    read(lna,FormA,end=1200) Veta
        idl=idel(Veta)
        Veta=Veta(4:)
        write(ln,FormA) Veta(:idel(Veta))
        go to 1170
      else
        lna=0
      endif
1200  write(ln,FormA) 'BEGIN_BLOCK_DATAGRID_3D'
      write(ln,FormA) '  '//fln(:ifln)
      write(ln,FormA) '  BEGIN_DATAGRID_3D_this_is_2Dgrid#1'
      write(ln,101)(nx(i),i=1,3)
      write(ln,102) 0.,0.,0.
      write(ln,102) float(nx(1)-1)*dx(1),0.,0.
      write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
      write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j),j=1,nxny)
        write(ln,107)(WorkMap(j),j=1,nxny)
      enddo
5000  write(ln,FormA) '  END_DATAGRID_3D'
      write(ln,FormA) 'END_BLOCK_DATAGRID_3D'
      call CloseIfOpened(lna)
      if(allocated(WorkMap)) deallocate(WorkMap)
101   format(6i5)
102   format(3f13.8)
107   format(5e15.6)
      end
      subroutine ConNewMap(ich)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension NMenu(5)
      character*256 FileName
      character*80  Veta,LabelM81,LabelD81
      character*80 :: Menu(5) =
     1   (/'Fourier map                  ',
     2     'p.d.f.                       ',
     3     'j.p.d.f.                     ',
     4     'Density map - cell fraction  ',
     5     'Density map - general section'/)
      integer Prvni,RolMenuSelectedQuest,CrwStateQuest
      logical ExistFile,ExistM80,ExistM81,CrwLogicQuest,UseOldMap,
     1        GeneralSection,ExistD81,EqIgCase,M81Opened,lpom
      data GeneralSection/.false./,KPDFOld/-1/
      save UseOldMap
      ich=0
      KPDFIn=KPDF
      if(KPDF.eq.0) then
        KPDF=1
      else if(KPDF.eq.5) then
        KPDF=4
      else if(KPDF.eq.6) then
        KPDF=5
      endif
      ExistM80=ExistFile(fln(:ifln)//'.m80')
      ExistM81=ExistFile(fln(:ifln)//'.m81')
      ExistD81=ExistFile(fln(:ifln)//'.d81')
      MapaM81=0
      MapaD81=0
      inquire(81,opened=M81Opened,name=FileName)
      if(M81Opened) then
        id=idel(CurrentDir)
        i=index(FileName,CurrentDir(:id))
        if(i.gt.0) FileName=FileName(id+1:)
        if(EqIgCase(FileName,fln(:ifln)//'.m81')) then
          MapaM81=Mapa
        else if(EqIgCase(FileName,fln(:ifln)//'.d81')) then
          MapaD81=-Mapa
        endif
      endif
      ln=NextLogicNumber()
      if(ExistM81.and.MapaM81.le.0) then
        call OpenMaps(ln,fln(:ifln)//'.m81',i,0)
        read(ln,rec=1) (i,j=1,8),(pom,pom,pom,j=1,6),
     1                 (i,j=1,6),MapaM81,i,lpom,lpom
      endif
      call CloseIfOpened(ln)
      if(ExistD81.and.MapaD81.le.0) then
        call OpenMaps(ln,fln(:ifln)//'.d81',i,0)
        read(ln,rec=1) (i,j=1,8),(pom,pom,pom,j=1,6),
     1                 (i,j=1,6),MapaD81,i,lpom,lpom
        MapaD81=-MapaD81
      endif
      call CloseIfOpened(ln)
      if(MapaM81.gt.0) then
        LabelM81=MapType(MapaM81)
      else
        LabelM81=' '
      endif
      if(MapaD81.gt.0) then
        LabelD81=MenDensity(MapaD81)
      else
        LabelD81=' '
      endif
      if(NoOfDerivedMaps.eq.0) then
        DrawAtN=0
        call SetIntArrayTo(NMenu,5,1)
        dpom=0.
        Prvni=0
        do i=1,5
          if((i.eq.1.and..not.ExistM80.and..not.ExistM81).or.
     1       (i.eq.4.and..not.ChargeDensities)) then
            NMenu(i)=0
          else
            if(Prvni.eq.0) Prvni=i
          endif
          dpom=max(dpom,FeTxLength(Menu(i)))
        enddo
        dpom=dpom+10.+EdwYd
      else
        go to 3000
      endif
      if(KPDFOld.le.0) then
        KPDFOld=Prvni
        KPDF=Prvni
      else
        if(NMenu(KPDFOld).le.0) then
          KPDFOld=Prvni
          KPDF=Prvni
        else
          KDPF=KPDFOld
        endif
      endif
1100  ich=0
      id=NextQuestId()
      xqd=450.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      il=1
      Veta='%New map to draw'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuNew=RolMenuLastMade
      call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,Menu,NMenu,5,
     1                                  max(KPDFOld,1))
      xpom=5.
      tpom=xpom+CrwgXd+5.
      Veta='Use %old maps'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwOldMap=CrwLastMade
          Veta='Calculate ne%w ones'
        else
          nCrwNewMap=CrwLastMade
        endif
      enddo
      il=il-2
      xpom=xpom+xqd*.5+20.
      tpom=tpom+xqd*.5+20.
      Veta='Draw maps as %calculated'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      2)
        if(i.eq.1) then
          nCrwAsCalculated=CrwLastMade
          Veta='Draw a %general section'
        else
          nCrwGeneralSection=CrwLastMade
        endif
      enddo
      il=il+1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,' ','L','N')
      nLblOldMap=LblLastMade
1400  if(kpdf.le.1.or.kpdf.eq.4) then
        if(kpdf.le.1) then
          UseOldMap=ExistM81
        else
          UseOldMap=ExistD81
        endif
        if(CrwStateQuest(nCrwNewMap).eq.CrwClosed.or.
     1     CrwStateQuest(nCrwNewMap).eq.CrwDisabled.or.
     2     kpdf.ne.kpdfOld) then
          call FeQuestCrwOpen(nCrwOldMap,UseOldMap)
          call FeQuestCrwOpen(nCrwNewMap,.not.UseOldMap)
          if(kpdf.le.1) then
            if(.not.ExistM81) then
              call FeQuestCrwDisable(nCrwOldMap)
              call FeQuestCrwOn(nCrwNewMap)
            endif
          else
            if(.not.ExistD81) then
              call FeQuestCrwDisable(nCrwOldMap)
              call FeQuestCrwOn(nCrwNewMap)
            endif
          endif
        endif
        if(CrwStateQuest(nCrwAsCalculated).eq.CrwClosed.or.
     1     CrwStateQuest(nCrwAsCalculated).eq.CrwDisabled.or.
     2     kpdf.ne.kpdfOld) then
          call FeQuestCrwOpen(nCrwAsCalculated,.not.GeneralSection)
          call FeQuestCrwOpen(nCrwGeneralSection,GeneralSection)
        endif
        Veta=' '
        if(kpdf.le.1) then
          if(LabelM81.ne.' ') Veta='The old map is '//LabelM81
        else
          if(LabelD81.ne.' ') Veta='The old map is '//LabelD81
        endif
        call FeQuestLblChange(nLblOldMap,Veta)
      else
        if(nLblOldMap.gt.0) call FeQuestLblOff(nLblOldMap)
        call FeQuestCrwDisable(nCrwNewMap)
        call FeQuestCrwDisable(nCrwOldMap)
        call FeQuestCrwDisable(nCrwAsCalculated)
        call FeQuestCrwDisable(nCrwGeneralSection)
      endif
      kpdfOld=kpdf
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuNew) then
        kpdf=RolMenuSelectedQuest(CheckNumber)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        kpdf=RolMenuSelectedQuest(nRolMenuNew)
        KPDFOld=kpdf
        if(CrwStateQuest(nCrwAsCalculated).ne.CrwClosed.and.
     1     CrwStateQuest(nCrwAsCalculated).ne.CrwDisabled) then
          if(CrwLogicQuest(nCrwAsCalculated).and.kpdf.eq.1) kpdf=0
          if(.not.CrwLogicQuest(nCrwAsCalculated).and.kpdf.eq.4) kpdf=5
        else if(KPDF.eq.5) then
          KPDF=6
        endif
        GeneralSection=kpdf.eq.1.or.kpdf.eq.5
        if(kpdf.le.1) then
          UseOldMap=.true.
          if(ExistM81) then
            if(CrwLogicQuest(nCrwOldMap)) go to 2000
          endif
1600      UseOldMap=.false.
          ContourCallFourier=.true.
          call CloseIfOpened(81)
          call FouSetCommands(ich)
          if(ich.ne.0) then
            ich=0
            call DefaultContour
            call NactiContour
            ContourCallFourier=.false.
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            call FeQuestRemove(id)
            if(NDimI(KPhase).gt.0) call TrOrtho(0)
            go to 1100
          endif
          call Fourier
          if(ErrFlag.ne.0) go to 1600
          call DefaultContour
          call NactiContour
          ContourCallFourier=.false.
          if(NDimI(KPhase).gt.0) call TrOrtho(0)
        else if(kpdf.ge.4.and.kpdf.le.5) then
          UseOldMap=.true.
          if(ExistD81) then
            if(CrwLogicQuest(nCrwOldMap)) go to 2000
          endif
          UseOldMap=.false.
          call CloseIfOpened(81)
          Obecny=.false.
          call ConDensityInCell(ich)
        else if(kpdf.eq.5) then
          kpdf=6
        endif
      else
        KPDF=KPDFIn
      endif
2000  call FeQuestRemove(id)
3000  return
      end
      subroutine ConCalcGeneral
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(6),ix456(3),x4p(3),DenSupl(3),WorkMap(:,:),CalcMap(:)
      integer UseTabsIn
      real InputMap(:),ErrMap(:)
      character*80 t80
      logical konec,uzje(:)
      allocatable uzje,InputMap,WorkMap,ErrMap,CalcMap
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      xpom=30.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      xpom=xpom+30.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=xpom+103.
      call FeTabsAdd(xpom,UseTabs,IdLeftTab,' ')
      xpom=xpom+18.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      konec=.false.
      nxrnyr=nxr(1)*nxr(2)
      allocate(uzje(nxrnyr),CalcMap(nxrnyr))
      if(ErrMapActive) allocate(ErrMap(nxrnyr))
      if(DrawPDF) then
        nxr(4)=ntpdf
        if(ntpdf.gt.1) then
          dxr(4)=(tpdfk-tpdfp)/(ntpdf-1)
        else
          dxr(4)=1.
        endif
        xminr(4)=tpdfp
        xmaxr(4)=tpdfk
        nxr(5)=1
        xminr(5)=0.
        xmaxr(5)=0.
        nxr(6)=1
        xminr(6)=0.
        xmaxr(6)=0.
        if(NDim(KPhase).gt.3) tmapy=.true.
        call CopyVekI(nxr(4),nx(4),3)
        if(DensityType.eq.5.or.DensityType.eq.6) then
          n=3
        else
          n=1
        endif
        allocate(WorkMap(nxrnyr,n))
      else
        npdf=1
        call CopyVekI(nx(4),nxr(4),3)
        call CopyVek(dx(4),dxr(4),3)
        call CopyVek(xmin(4),xminr(4),3)
        call CopyVek(xmax(4),xmaxr(4),3)
        nx3=nx(3)
        do i=1,NDim(KPhase)
          pom=xmax(i)-xmin(i)
          if(pom.lt.1.-dx(i)-.0001.or.pom.ge.1.) then
            dxp(i)=0.
          else
            dxp(i)=dx(i)
            if(i.eq.3) nx3=nx3+1
          endif
        enddo
        nmapa=nx3
        allocate(InputMap(nmapa*nxny))
      endif
      nmapar=nxr(3)*nxr(4)*nxr(5)*nxr(6)
      call CloseIfOpened(82)
      call OpenMaps(82,fln(:ifln)//'.l82',nxrnyr,1)
      if(ErrFlag.ne.0) go to 9000
      write(82,rec=1) nxr,nxrnyr,nmapar,(xminr(i),xmaxr(i),i=1,6),dxr,
     1                (i,i=1,6),mapa,nsubs,SatelityByly,
     2                nonModulated(KPhase)
      if(ErrMapActive) then
        call CloseIfOpened(83)
        call OpenMaps(83,fln(:ifln)//'.l83',nxrnyr,1)
        if(ErrFlag.ne.0) go to 9000
        write(83,rec=1) nxr,nxrnyr,nmapar,(xminr(i),xmaxr(i),i=1,6),dxr,
     1                  (i,i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
        error=100.
      endif
      DminErr=0.
      DmaxErr=0.
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      SumOverMap=0.
      if(DrawPDF.and.ErrMapActive) call ConReadM85
      if(kpdf.eq.1) then
        t80='Calculation of general section'
      else if(kpdf.eq.2) then
        t80='Calculation of p.d.f.'
      else if(kpdf.eq.3) then
        t80='Calculation of j.p.d.f.'
      else if(kpdf.eq.5.or.kpdf.eq.6) then
        t80=MenDensity(DensityType+1)
        t80='Calculation of '//t80(:idel(t80))//' map'
      endif
      n=0
      nmap=0
      nx456=nxr(4)*nxr(5)*nxr(6)
      if(npdf.le.0) then
        call FeChybne(-1.,-1.,'no atoms specified to calculate the'//
     1                'map.',' ',SeriousError)
        go to 9000
      endif
      nvse=npdf
      if(imax/nvse.lt.nxrnyr) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nxrnyr
      endif
      if(imax/nvse.lt.nxr(3)) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nxr(3)
      endif
      if(imax/nvse.lt.nx456) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nx456
      endif
1200  if(nvse.le.0) then
        call FeChybne(-1.,-1.,'the desired region has zero or negative '
     1                //'volume.',' ',SeriousError)
        go to 9000
      endif
      if(.not.ErrMapActive.or.kpdf.eq.1)
     1  call FeFlowChartOpen(-1.,120.,max(nint(float(nvse)*.005),10),
     2                       nvse,t80,' ',' ')
      if(allocated(FlagMap)) deallocate(FlagMap)
      allocate(FlagMap(nx456*nxr(3)))
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(nxrnyr))
      NActualMap=nxrnyr
      do it=1,nx456
        call RecUnpack(it,ix456,nx(4),3)
        do i=1,3
          j=i+3
          xx(j)=xminr(j)+(ix456(i)-1)*dxr(j)
        enddo
        do iz=1,nxr(3)
          zout=xminr(3)+dxr(3)*float(iz-1)
          izz=1
          call SetLogicalArrayTo(uzje,nxrnyr,.false.)
          nmap=nmap+1
          FlagMap(nmap)=0
          do ix4=1,nx456
            call RecUnPack(ix4,ix456,nx(4),3)
            do i=1,3
              j=i+3
              x4p(i)=xmin(j)+(ix456(i)-1)*dx(j)
            enddo
1300        if(DrawPDF) then
              if(DensityType.eq.5.or.DensityType.eq.6) then
                call SetRealArrayTo(WorkMap,3*nxrnyr,0.)
              else
                call SetRealArrayTo(CalcMap,nxrnyr,0.)
              endif
              if(ErrMapActive) then
                call SetRealArrayTo(ErrMap,nxrnyr,0.)
                call ConReadM85
                if(ErrMapActive) then
                  pom=ran1(i)
                  if(NDimI(KPhase).gt.0) then
                    Cislo='t'
                    pom=xx(4)
                  else
                    Cislo='z'
                    pom=zout
                  endif
                  write(t80,101) Cislo(1:1),Tabulator,pom,Tabulator,
     1                           Tabulator,0,Tabulator,Tabulator,error
                  if(it.eq.1.and.iz.eq.1)
     1              call FeOpenInterrupt(-1.,-1.,t80)
                  konec=.false.
                endif
              else
                mcmax=0
              endif
            else
              kk=1
              do km=1,nmapa
                if(km.gt.nx(3)) then
                  izzp=izz
                  izz=izz-2*nx(3)+km
                else
                  izz=izz+1
                endif
                read(81,rec=izz,err=8000)(InputMap(i),i=kk,kk+nxny-1)
                if(km.gt.nx(3)) izz=izzp
                kk=kk+nxny
              enddo
            endif
            imc=0
1500        if(imc.gt.mcmax) go to 5100
            if(ErrMapActive.and.imc.gt.0) then
              call ConHitMC
              call SetRealArrayTo(WorkMap,nxrnyr,0.)
            endif
            do ia=1,npdf
              if(DrawPDF) then
                ip=ipor(ia)
                fpdfi=fpdf(ip)
                if(fpdfi.le.0.) then
                  n=n+nxrnyr
                  cycle
                endif
                if(kpdf.eq.6) then
                  call ConDensity(pom,ia,0,DenSupl,ich)
                else
                  xx(1)=pdfat(xa,xx(4),ia,0,ich)
                endif
                if(ich.eq.1) then
                  if(imc.eq.0) then
                    n=iapdf(ip)
                    t80='isn''t positive definite'
                    if(NDimI(KPhase).gt.0)
     1                write(t80(25:),'(''for t='',f6.3)') xx(4)
                    call FeChybne(-1.,YBottomMessage,
     1                'ADP tensor of the atom '//
     2                atom(n)(:idel(atom(n))),t80,SeriousError)
                    if(NDimI(KPhase).le.0) then
                      if(.not.ErrMapActive.or.kpdf.eq.1) then
                        call FeFlowChartRemove
                      else if(ErrMapActive) then
                        call FeCloseInterrupt
                      endif
                      go to 9000
                    else
                      FlagMap(nmap)=1
                      go to 1900
                    endif
                  else
                    go to 1500
                  endif
                else if(ich.eq.2) then
                  FlagMap(nmap)=2
                  go to 1900
                endif
                go to 2000
1900            n=n+npdf*nxrnyr*nxr(3)
                go to 6100
              endif
2000          j=0
              do iy=1,nxr(2)
                do ix=1,nxr(1)
                  j=j+1
                  if(DrawPDF) then
                    do i=1,3
                      xx(i)=xob(i)+xr(i)*float(ix-1)+
     1                             yr(i)*float(iy-1)+zr(i)*float(iz-1)
                      if(kpdf.eq.6) xx(i)=xx(i)-xpdf(i,ip)
                    enddo
                    if(kpdf.eq.6) then
                      call multm(TrToOrtho,xx,xo,3,3,1)
                      dd=VecOrtLen(xo,3)
                      if(dd.gt.CutOffDist) then
                        if(.not.ErrMapActive)
     1                    call FeFlowChartEvent(n,is)
                        if(is.ne.0) then
                          call FeBudeBreak
                          if(ErrFlag.ne.0) go to 9000
                        endif
                        uzje(j)=.true.
                        cycle
                      endif
                    endif
                    if(imc.eq.0) then
                      if(kpdf.eq.6) then
                        call ConDensity(dd,ia,1,DenSupl,ich)
                        if(DensityType.ne.5) pom=DenSupl(1)*fpdfi
                      else
                        pom=pdfat(xx,t,ia,1,ich)*fpdfi
                      endif
                      if(DensityType.eq.5.or.DensityType.eq.6) then
                        do i=1,3
                          WorkMap(j,i)=WorkMap(j,i)+DenSupl(i)*fpdfi
                        enddo
                      else
                        CalcMap(j)=CalcMap(j)+pom
                        SumOverMap=SumOverMap+pom
                      endif
                      uzje(j)=.true.
                    else
                      if(kpdf.eq.6) then
                        call ConDensity(dd,ia,1,DenSupl,ich)
                        if(DensityType.ne.5.and.DensityType.ne.6) then
                          pom=DenSupl(1)*fpdfi
                          WorkMap(j,1)=WorkMap(j,1)+pom
                        else

                        endif
                      else
                        WorkMap(j,1)=WorkMap(j,1)+
     1                             pdfat(xx,t,ia,1,ich)*fpdfi
                      endif
                    endif
                    if(.not.ErrMapActive) then
                      call FeFlowChartEvent(n,is)
                      if(is.ne.0) then
                        call FeBudeBreak
                        if(ErrFlag.ne.0) go to 9000
                      endif
                    endif
                  else
                    if(uzje(j)) cycle
                    do i=1,3
                      xx(i)=xob(i)+xr(i)*float(ix-1)+yr(i)*float(iy-1)
     1                            +zr(i)*float(iz-1)
                    enddo
                    call exmap(InputMap,xx,CalcMap(j),uzje(j),x4p(1))
                    if(uzje(j)) then
                      SumOverMap=SumOverMap+CalcMap(j)
                      call FeFlowChartEvent(n,is)
                      if(is.ne.0) then
                        call FeBudeBreak
                        if(ErrFlag.ne.0) go to 9000
                      endif
                    endif
                  endif
                enddo
                if(ErrMapActive.and..not.konec)
     1            call FeEventInterrupt(Konec)
              enddo
            enddo
            if(mcmax.gt.0.and.imc.ne.0) then
              pom1=1./float(imc)
              pom2=pom1*float(imc-1)
              error1=0.
              error2=0.
              do j=1,nxrnyr
                pom=(WorkMap(j,1)-CalcMap(j))**2*pom1
                error1=error1+abs(ErrMap(j)*pom1-pom)
                error2=error2+ErrMap(j)
                ErrMap(j)=pom2*ErrMap(j)+pom
              enddo
              if(error2.ne.0.) then
                error=min(sqrt(error1/error2)*100.,100.)
              else
                if(imc.gt.1) then
                  error=0.
                else
                  error=100.
                endif
              endif
              if(error.lt.errlev.or.konec) go to 5100
              if(NDimI(KPhase).gt.0) then
                Cislo='t'
                pom=xx(4)
              else
                Cislo='z'
                pom=zout
              endif
              write(t80,101) Cislo(1:1),Tabulator,pom,Tabulator,
     1                       Tabulator,imc,Tabulator,Tabulator,error
              call FeOutputInterrupt(t80)
            endif
            imc=imc+1
            go to 1500
5100        if(mcmax.gt.0) then
              do ii=1,nxrnyr
                ErrMap(ii)=sqrt(ErrMap(ii))
              enddo
            endif
            npoints=0
            do ii=1,nxrnyr
              if(.not.uzje(ii)) npoints=npoints+1
            enddo
            if(npoints.eq.0) go to 6000
          enddo
          if(npoints.ne.0) then
            call FeFlowChartRemove
            call FeChybne(-1.,-1.,'Fourier maps don''t allow to'//
     1                    ' extrapolate all points.',' ',SeriousError)
            go to 9000
          endif
6000      do ii=1,nxrnyr
            if(DensityType.eq.5.or.DensityType.eq.6) then
              CalcMap(ii)=sqrt(WorkMap(ii,1)**2+WorkMap(ii,2)**2+
     1                       WorkMap(ii,3)**2)
              if(DensityType.eq.5) CalcMap(ii)=1000./(1.+CalcMap(ii))
            else

            endif
            Dmin=min(Dmin,CalcMap(ii))
            Dmax=max(Dmax,CalcMap(ii))
            if(ErrMapActive) DmaxErr=max(ErrMap(ii),DmaxErr)
          enddo
6100      write(82,rec=nmap+1)(CalcMap(i),i=1,nxrnyr)
          if(ErrMapActive) then
            write(83,rec=nmap+1)(ErrMap(i),i=1,nxrnyr)
            call obnova
          endif
        enddo
      enddo
      write(82,rec=nmap+2) Dmax,Dmin
      if(ErrMapActive) then
        write(83,rec=nmap+2) DmaxErr,DminErr
        call FeCloseInterrupt
      else
        call FeFlowChartRemove
      endif
      call SetContours(0)
      if(DrawPDF) then
        if(DensityType.eq.2.or.DensityType.eq.3.or.DensityType.eq.4.or.
     1     DensityType.eq.5) then
          DiffMapa=.true.
          DrawPos=1
          DrawNeg=1
        else
          DiffMapa=.false.
          DrawPos=1
          DrawNeg=0
        endif
      endif
      go to 9050
8000  call FeReadError(81)
9000  ErrFlag=1
9050  if(ErrFlag.eq.0) then
        m8=82
        read(m8,rec=1,err=8000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          (i,j=1,6),mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
      else
        call CloseIfOpened(82)
        m8=81
      endif
9999  if(allocated(uzje))  deallocate(uzje,CalcMap)
      if(allocated(InputMap)) deallocate(InputMap)
      if(allocated(WorkMap))  deallocate(WorkMap)
      if(allocated(ErrMap))   deallocate(ErrMap)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
101   format(a1,' =',a1,f6.3,a1,'Monte Carlo hit#',a1,i5,a1,'Error =',a,
     1       f5.1,'%')
      end
      subroutine ConDensity(dd,iat,Klic,DenSupl,ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      logical Hydrogen
      dimension FractToLocal(9),px(9),xp(3),SpX(64),SpXDer1(3,64),
     1          SpXDer2(6,64),fnorm(8),zz(8),nn(8),STOA(10),DSto(0:2),
     2          DenDer(0:2),DenDerXYZ(10),DenSupl(*),OrthoToLocal(9),
     3          RPop(680),popasi(64)
      equivalence (xp,px,pom)
      data st/.0333333/
      save FractToLocal,ip,isfi,PopValDeform,ia,fnorm,OrthoToLocal,zz,
     1     nn,lasmaxi,kapa1i,kapa2i,Hydrogen,popasi,PopCor
      ich=0
      if(Klic.eq.0) then
        ip=ipor(iat)
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isw=iswa(ia)
        isfi=isf(ia)
        if(ispdf(ip).eq.0) ispdf(ip)=1
        ISym=ispdf(ip)
        ISymAbs=abs(ISym)
        if(IsymAbs.eq.1) then
          call CopyMat(TrAt(1,ia),FractToLocal,3)
        else
          call MatInv(rm(1,ISymAbs,isw,KPhase),px,STOA(1),3)
          call MultM(TrAt(1,ia),px,FractToLocal,3,3,3)
        endif
        if(ISym.lt.0) then
          do i=1,9
            FractToLocal(i)=-FractToLocal(i)
          enddo
        endif
        call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
        call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
        jp=2
        kp=2
        if(lasmaxi.ge.1) then
          popasi(1)=popas(1,ia)
          do l=1,lasmaxi-2
            n=2*l+1
            call multm(RPop(kp),popas(jp,ia),popasi(jp),n,n,1)
            kp=kp+n**2
            jp=jp+n
          enddo
          kapa1i=kapa1(ia)
          kapa2i=kapa2(ia)
          PopValDeform=kapa1i**3*popv(ia)
        endif
        PopValFree=ffbasic(1,isfi,KPhase)-ffcore(1,isfi,KPhase)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        Hydrogen=AtType(isfi,KPhase).eq.'H'.or.
     1           AtType(isfi,KPhase).eq.'D'
      else
        if(DensityType.le.2) then
          if(lasmaxi.ge.1) then
            Density=PopValDeform*Finter(dd*kapa1i,DenVal(1,isfi),st,240)
     1              /pi4
          else
            Density=0.
          endif
          if(DensityType.eq.2.and.lasmaxi.gt.1) then
            if(Hydrogen) then
              Density=Density-PopValFree*2.148028*exp(-3.77943*dd)
            else
              Density=Density-
     1                   PopValFree*Finter(dd,DenVal(1,isfi),st,240)/pi4
            endif
          endif
          if(DensityType.eq.0)
     1      Density=Density+PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          k=0
          do l=1,lasmaxi-1
            pom=0.
            do n=1,2*l-1
              k=k+1
              pom=pom+popasi(k)*SpX(k)
            enddo
            Density=Density+
     1                 pom*fnorm(l)*SlaterDensity(dd,zz(l),nn(l))
          enddo
        else if(DensityType.eq.3.or.DensityType.eq.4.or.
     1          DensityType.eq.5.or.DensityType.eq.6) then
          if(DensityType.eq.3.or.DensityType.eq.5.or.DensityType.eq.6)
     1      then
            DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
            DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
            DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
            call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
            if(DensityType.eq.3) then
              Density=-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
            else
              call CopyVek(DenDerXYZ(2),DenSupl,3)
            endif
          else
            Density=0.
          endif
          DenDer(0)=PopValDeform*
     1      Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
          DenDer(1)=PopValDeform*kapa1i*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          DenDer(2)=PopValDeform*kapa1i**2*
     1      Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
          if(DensityType.eq.3.or.DensityType.eq.4) then
            Density=Density-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
          else
            call AddVek(DenSupl,DenDerXYZ(2),DenSupl,3)
          endif
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          k=0
          do l=1,lasmaxi-1
            call SlaterDensityDer(dd,zz(l),nn(l),DSto)
            STO=DSto(0)
            call RadialDer(xo(1),xo(2),xo(3),dd,DSto,2,STOA)
            call SetRealArrayTo(px,3,0.)
            fnorml=fnorm(l)
            do n=1,2*l-1
              k=k+1
              SpXk=SpX(k)
              popask=popasi(k)
              if(DensityType.eq.3.or.DensityType.eq.4) then
                pom=pom+popask*(
     1            SpXk*STOA(5)+2.*SpXDer1(1,k)*STOA(2)+SpXDer2(1,k)*STO+
     2            SpXk*STOA(6)+2.*SpXDer1(2,k)*STOA(3)+SpXDer2(2,k)*STO+
     3            SpXk*STOA(7)+2.*SpXDer1(3,k)*STOA(4)+SpXDer2(3,k)*STO)
              else
                do m=1,3
                  px(m)=px(m)+popask*(SpXk*STOA(m+1)+SpXDer1(m,k)*STO)
                enddo
              endif
            enddo
            if(DensityType.eq.3.or.DensityType.eq.4) then
              Density=Density-pom*fnorml
            else
              do m=1,3
                DenSupl(m)=DenSupl(m)+px(m)*fnorml
              enddo
            endif
          enddo
        endif
        if(DensityType.ne.5.and.DensityType.ne.6) DenSupl(1)=Density
      endif
9999  return
      end
      subroutine ConSetLocDensity
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pop(6),DSto(0:2)
      integer Type,PopPom
      double precision pom
      data st/.0333333/
      do icv=1,2
        do i=1,NAtFormula(KPhase)
          if(icv.eq.1) then
            call SetRealArrayTo(DenCore(1,i),240,0.)
            call SetRealArrayTo(DenCoreDer(1,1,i),240,0.)
            call SetRealArrayTo(DenCoreDer(1,2,i),240,0.)
            Type=TypeCore(i,KPhase)
            PopPom=PopCore(1,i,KPhase)
          else
            call SetRealArrayTo(DenVal(1,i),240,0.)
            call SetRealArrayTo(DenValDer(1,1,i),240,0.)
            call SetRealArrayTo(DenValDer(1,2,i),240,0.)
            Type=TypeVal(i,KPhase)
            PopPom=PopVal(1,i,KPhase)
          endif
          if(Type.eq.0) then
            fnorm=0.
            kp=0
            do l=1,7
              kp=kp+l
              k=kp
              call SetRealArrayTo(pop,6,0.)
              spop=0.
              if(icv.eq.1) then
                pom=PopCore(1,i,KPhase)
              else
                pom=PopVal(1,i,KPhase)
              endif
              if(pom.ge.0) then
                do j=l,7
                  jp=j-l+1
                  if(icv.eq.1) then
                    pop(jp)=PopCore(k,i,KPhase)
                  else
                    pop(jp)=PopVal(k,i,KPhase)
                  endif
                  spop=spop+pop(jp)
                  fnorm=fnorm+pop(jp)
                  k=k+j
                enddo
              endif
              if(spop.gt.0.) then
                if(icv.eq.1) then
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenCore(1,i),240,1,
     2                           CoreValSource(i,KPhase),ich)
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenCoreDer(1,1,i),240,2,
     2                           CoreValSource(i,KPhase),ich)
                else
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenVal(1,i),240,1,
     2                           CoreValSource(i,KPhase),ich)
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenValDer(1,1,i),240,2,
     2                           CoreValSource(i,KPhase),ich)
                endif
              endif
            enddo
            if(fnorm.gt.0) then
              fnorm=1./fnorm
              do j=1,240
                if(icv.eq.1) then
                  DenCore(j,i)=DenCore(j,i)*fnorm
                else
                  DenVal(j,i)=DenVal(j,i)*fnorm
                endif
              enddo
              do j=1,2
                do k=1,240
                  if(icv.eq.1) then
                    DenCoreDer(k,j,i)=DenCoreDer(k,j,i)*fnorm
                  else
                    DenValDer(k,j,i)=DenValDer(k,j,i)*fnorm
                  endif
                enddo
              enddo
            endif
          else if(Type.eq.-1.or.Type.eq. 1) then
            nn=FFType(KPhase)
            ds=.1
            pom=0.
            s=0.
            do j=2,nn
              s=s+ds
              if(icv.eq.1) then
                FPom=FFCore(j,i,KPhase)
              else
                FPom=FFVal(j,i,KPhase)
              endif
              pom=pom+FPom*s**2
            enddo
            if(icv.eq.1) then
              DenCore(1,i)=pi4**2*pom*ds
            else
              DenVal(1,i)=pi4**2*pom*ds
            endif
            d=0.
            do j=2,240
              d=d+st
              pom=0.
              s=0.
              do k=2,nn
                s=s+ds
                if(icv.eq.1) then
                  FPom=FFCore(k,i,KPhase)
                else
                  FPom=FFVal(k,i,KPhase)
                endif
                pom=pom+FPom*s*sin(pi2*d*s)
              enddo
              if(icv.eq.1) then
                DenCore(j,i)=2.*pi4*pom/d*ds
              else
                DenVal(j,i)=2.*pi4*pom/d*ds
              endif
            enddo
          else if(PopPom.eq.-2) then
            n=HNSlater(i,KPhase)
            dz=HZSlater(i,KPhase)/BohrRad
            rn=dz
            do j=1,n+2
              rn=rn*dz/float(j)
            enddo
            d=0.
            do j=1,240
              call SlaterDensityDer(d,dz,n,DSto)
              if(icv.eq.1) then
                DenCore(j,i)=DSto(0)*rn
                DenCoreDer(j,1,i)=DSto(1)*rn
                DenCoreDer(j,2,i)=DSto(2)*rn
              else
                DenVal(j,i)=DSto(0)*rn
                DenValDer(j,1,i)=DSto(1)*rn
                DenValDer(j,2,i)=DSto(2)*rn
              endif
              d=d+st
            enddo
          endif
        enddo
      enddo
      return
      end
      subroutine ConDensityInCell(ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real xp(6),DenSupl(3),xn(6)
      real, allocatable :: Table(:)
      character*80 Veta
      integer :: DensityTypeSave=2
      logical ExistFile
      do i=1,6
        iorien(i)=i
      enddo
      DensityType=DensityTypeSave
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      call ConSetLocDensity
      call CPReadScope(1,xmin,xmax,dx,DensityType,CutOffDist,ich)
      if(ich.ne.0) go to 9999
      call ConDefAtForDenCalc(3,ich)
      if(ich.ne.0) go to 9999
      nxny=1
      do i=1,6
        if(i.le.3) then
          nx(i)=nint((xmax(i)-xmin(i))/dx(i))+1
          if(i.le.2) then
            nxny=nxny*nx(i)
          else
            nmap=nx(i)
          endif
        else
          nx(i)=1
          xmin(i)=0.
          xmax(i)=0.
          dx(i)=.1
        endif
      enddo
      if(allocated(Table)) deallocate(Table)
      allocate(Table(nxny))
      Veta=fln(:ifln)//'.d81'
      if(ExistFile(Veta)) call DeleteFile(Veta)
      m8=NextLogicNumber()
      call OpenMaps(m8,Veta,nxny,1)
      if(ErrFlag.ne.0) go to 9999
      write(m8,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                (iorien(i),i=1,6),-DensityType-1,nsubs,
     2                SatelityByly,nonModulated(KPhase)
      nvse=npdf*nxny*nx(3)
      DensityTypeSave=DensityType
      Veta=MenDensity(DensityType+1)
      Veta='Calculation of '//Veta(:idel(Veta))//' map'
      call FeFlowChartOpen(-1.,120.,max(nint(float(nvse)*.005),10),nvse,
     1                     Veta,' ',' ')
      m=0
      Dmax=-999999.
      Dmin= 999999.
      CutOffDistA=CutOffDist*rcp(1,1,KPhase)
      CutOffDistB=CutOffDist*rcp(2,1,KPhase)
      CutOffDistC=CutOffDist*rcp(3,1,KPhase)
      xp3=xmin(3)
      do iz=1,nx(3)
        Table=0.
        do 3500ia=1,npdf
          ip=ipor(ia)
          fpdfi=fpdf(ip)
          if(fpdfi.le.0.) then
            m=m+nxny
            go to 3500
          endif
          call ConDensity(pom,ia,0,DenSupl,ich)
          xp(3)=xp3-xpdf(3,ip)
          if(abs(xp(3)).gt.CutOffDistC) then
            m=m+nxny
            go to 3500
          endif
          xp(2)=xmin(2)-xpdf(2,ip)
          n=0
          do iy=1,nx(2)
            if(abs(xp(2)).gt.CutOffDistB) then
              m=m+nx(1)
              n=n+nx(1)
              go to 3350
            endif
            xp(1)=xmin(1)-xpdf(1,ip)
            do ix=1,nx(1)
              n=n+1
              if(abs(xp(1)).gt.CutOffDistA) then
                m=m+1
                go to 3250
              endif
              call FeFlowChartEvent(m,is)
              if(is.ne.0) then
                call FeBudeBreak
                if(ErrFlag.ne.0) then
                  ich=1
                  go to 9000
                endif
              endif
              call MultM(TrToOrtho,xp,xo,3,3,1)
              dd=VecOrtLen(xo,3)
              if(dd.gt.CutOffDist) go to 3250
              call ConDensity(dd,ia,1,DenSupl,ich)
              Table(n)=Table(n)+DenSupl(1)*fpdfi
3250          xp(1)=xp(1)+dx(1)
            enddo
3350        xp(2)=xp(2)+dx(2)
          enddo
3500    continue
        do i=1,nxny
          pom=Table(i)
          Dmin=min(Dmin,pom)
          Dmax=max(Dmax,pom)
        enddo
        write(m8,rec=iz+1)(Table(i),i=1,nxny)
        xp3=xp3+dx(3)
      enddo
      write(m8,rec=nx(3)+2) Dmax,Dmin
      close(m8)
      call FeFlowChartRemove
      go to 9999
9000  close(m8,status='delete')
9999  if(allocated(Table)) deallocate(Table)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      return
      end
      subroutine exmap(InputMap,xx,Value,zapis,x4p)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(*),xp(6),xpt(6),xd(3),n(3),x1(6),x2(6),x4p(3)
      logical zapis,EqMod1
      real InputMap(*)
      NDimp=NDim(KPhase)
      if(nonModulated(KPhase).or..not.SatelityByly) NDimp=3
      do i=1,NSymmN(KPhase)
        call multm(rm6(1,i,nsubs,KPhase),xx,x1,NDim(KPhase),
     1             NDim(KPhase),1)
        do k=1,ncst
          if(k.eq.1) then
            call CopyVek(x1,x2,NDim(KPhase))
          else
            call RealVectorToOpposite(x1,x2,NDim(KPhase))
          endif
          if(Mapa.gt.3.or.Mapa.lt.0)
     1      call AddVek(x2,s6(1,i,nsubs,KPhase),x2,NDim(KPhase))
          do 1600l=1,NLattVec(KPhase)
            call AddVek(x2,vt6(1,l,nsubs,KPhase),xp,NDim(KPhase))
            call Multm(tm,xp,xpt,NDim(KPhase),NDim(KPhase),1)
            do j=1,NDimp
1100          if(xpt(j).ge.xmin(j)) go to 1200
              xpt(j)=xpt(j)+1.
              go to 1100
1200          if(xpt(j).lt.xmax(j)+dxp(j)) go to 1300
              xpt(j)=xpt(j)-1.
              go to 1200
1300          if(j.gt.3) then
                if(.not.EqMod1(xpt(j),x4p(j-3),.001)) go to 1600
              else
                if(xpt(j).lt.xmin(j)) go to 1600
              endif
            enddo
            do j=1,3
              xd(j)=(xpt(j)-xmin(j))/dx(j)+1.
              n(j)=xd(j)
              if(j.le.3) n(j)=min(n(j),nx(j))
              xd(j)=xd(j)-float(n(j))
            enddo
            if(nmapa.eq.nx3) then
              go to 2020
            else
              go to 2000
            endif
1600      continue
        enddo
      enddo
      go to 9999
2000  continue
2020  i=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
      i1=i+1
      inx=i+nx(1)
      inx1=inx+1
      if(n(1).eq.nx(1)) then
        i1  =i1  -nx(1)
        inx1=inx1-nx(1)
      endif
      if(n(2).eq.nx(2)) then
        inx =inx -nxny
        inx1=inx1-nxny
      endif
      f000=InputMap(i)
      f100=InputMap(i1)
      f010=InputMap(inx)
      f110=InputMap(inx1)
      i   =i   +nxny
      i1  =i1  +nxny
      inx =inx +nxny
      inx1=inx1+nxny
      f001=InputMap(i)
      f101=InputMap(i1)
      f011=InputMap(inx)
      f111=InputMap(inx1)
      f00=(f100-f000)*xd(1)+f000
      f01=(f101-f001)*xd(1)+f001
      f10=(f110-f010)*xd(1)+f010
      f11=(f111-f011)*xd(1)+f011
      f0=(f10-f00)*xd(2)+f00
      f1=(f11-f01)*xd(2)+f01
      Value=(f1-f0)*xd(3)+f0
      zapis=.true.
9999  return
      end
      subroutine ConDefGenSection(at,Klic,ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension p3(3),xg(3),yg(3),xgo(3)
      character*(*) at
      character*256 ta256(1),t256,EdwStringQuest,SbwStringQuest,
     1              SbwFileQuest
      character*80 Veta,StOld(9)
      character*20 DelString
      character*2 nty
      integer CrwStateQuest,RolMenuSelectedQuest,SbwItemFromQuest,
     1        SbwItemPointerQuest,SbwLnQuest,Exponent10,EdwStateQuest
      logical eqrv,CrwLogicQuest,Skip,EqIgCase,Change,FileDiff,FeYesNo,
     1        WizardModeSave
      equivalence (ta256,t256)
      data DelString/'--------------------'/
      if(NumberOfPlanes.lt.IPlaneOld) IPlaneOld=0
      Change=.false.
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
1100  id=NextQuestId()
      Veta='Plane/Volume'
      if(WizardMode) then
        call FeQuestTitleMake(id,Veta)
        xqd=WizardLength
      else
        il=11
        if(Klic.eq.1) il=il+2
        xqd=550.
        call FeQuestCreate(id,-1.,-1.,xqd,il,'Plane/Volume',0,LightGray,
     1                     0,0)
      endif
      il=0
      tpom=5.
      Veta='%Define new'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          nCrwNew=CrwLastMade
        else
          nCrwOld=CrwLastMade
        endif
        Veta='%Use old'
        call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.IPlaneOld.le.0)
      enddo
      write(Cislo,FormI15) NumberOfPlanes+1
      call Zhusti(Cislo)
      LabelPlaneNew='Plane#'//Cislo(:idel(Cislo))
      dpoms=FeTxLength(LabelPlaneNew)+15.
      do i=1,NumberOfPlanes
        dpoms=max(dpoms,FeTxLength(LabelPlane(i)))
      enddo
      dpoms=dpoms+2.*EdwMarginSize+5.
      tpom=xpom+CrwgXd+10.
      Veta='=>'
      il=il-1
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpoms,EdwYd,1)
      nEdwNewName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,LabelPlaneNew)
      il=il+1
      nLblOldPlane=0
      nRolMenuOldPlane=0
      if(NumberOfPlanes.gt.1) then
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',
     1                          dpoms+EdwYd,EdwYd,1)
        nRolMenuOldPlane=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                          NumberOfPlanes,max(IPlane,1))
      else if(NumberOfPlanes.eq.1) then
        call FeQuestLblMake(id,tpom,il,'=>     '//LabelPlane(1),'L','N')
      endif
      xpom=xpom+dpoms+EdwYd+15.
      Veta='%Rename/Delete plane'
      if(NumberOfPlanes.gt.1) then
        i=idel(Veta)+1
        Veta(i:i)='s'
      endif
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRenDel=ButtonLastMade
      il=il+1
      dpom=150.
      do i=1,3
        il=il+1
        xpom=40.
        write(Veta(1:1),'(i1)') i
        Veta=Veta(1:1)//nty(i)
        call FeQuestLblMake(id,5.,il,Veta,'L','N')
        do j=1,3
          Veta=' '
          if(i.eq.1) then
            if(j.eq.1) then
              Veta='Atom'
            else if(j.eq.2) then
              Veta='Coordinates'
            endif
          else
            if(i.eq.2.and.j.eq.3) Veta='Difference to 1st'
          endif
          call FeQuestEdwMake(id,xpom+dpom*.5,il-1,xpom,il,Veta,'C',
     1                        dpom,EdwYd,1)
          if(i.eq.1.and.j.eq.1) nEdwFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      nEdwLast=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Scope','C','B')
      xpom=5.
      il=il+2
      do i=1,3
        if(i.eq.1) then
          Veta='%Interpolation step'
          j=0
        else if(i.eq.2) then
          Veta='%Scope of section'
          j=1
        else
          Veta='%1st point put to'
          j=0
        endif
        call FeQuestEdwMake(id,xpom+dpom*.5,il-1,xpom,il,Veta,'C',dpom,
     1                      EdwYd,j)
        if(i.eq.1) then
          nEdwDelta=EdwLastMade
        else if(i.eq.2) then
          nEdwScope=EdwLastMade
        else
          nEdwShift=EdwLastMade
        endif
        if(i.ne.3) xpom=xpom+dpom+10.
      enddo
      tpom=xpom
      xpom=xpom+dpom+10.
      call FeQuestLblMake(id,xpom,il,'[Ang]','L','N')
      il=il+1
      xpom=tpom+dpom*.5
      Veta='%Adjust'
      dpom=FeTxLength(Veta)+10.
      xpom=xpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdjust=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(klic.eq.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Cutoff distance for atoms'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCutOffDist=EdwLastMade
        call FeQuestLblMake(id,xpom+dpom+10.,il,'[Ang]','L','N')
        call FeQuestRealEdwOpen(nEdwCutOffDist,CutOffDist,.false.,
     1                          .false.)
      endif
      IPlane=IPlaneOld
1200  if(NumberOfPlanes.gt.0) then
        call FeQuestCrwOpen(nCrwNew,IPlane.le.0)
        call FeQuestCrwOpen(nCrwOld,IPlane.gt.0)
        call FeQuestButtonOpen(nButtRenDel,ButtonOff)
      else
        call FeQuestCrwDisable(nCrwNew)
        call FeQuestCrwDisable(nCrwOld)
        call FeQuestButtonDisable(nButtRenDel)
        if(nRolMenuOldPlane.gt.0)
     1    call FeQuestRolMenuDisable(nRolMenuOldPlane)
        IPlane=0
      endif
1250  if(nCrwOld.ne.0) then
        if(CrwStateQuest(nCrwOld).ne.CrwDisabled) then
          if(CrwLogicQuest(nCrwOld)) then
            if(nRolMenuOldPlane.gt.0) then
              if(NumberOfPlanes.gt.1) then
                call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                                  NumberOfPlanes,max(IPlane,1))
              else
                call FeQuestButtonDisable(nButtRenDel)
                call FeQuestRolMenuDisable(nRolMenuOldPlane)
              endif
            endif
            LabelPlaneNew=EdwStringQuest(nEdwNewName)
            call FeQuestEdwDisable(nEdwNewName)
            IPlane=max(IPlane,1)
          else
            if(nRolMenuOldPlane.gt.0)
     1        call FeQuestRolMenuDisable(nRolMenuOldPlane)
            call FeQuestStringEdwOpen(nEdwNewName,LabelPlaneNew)
            IPlane=0
          endif
        endif
      else
        IPlane=0
      endif
1300  if(IPlane.gt.0) then
        call ConReadKeys
        if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 1390
        else
          go to 1400
        endif
      endif
1390  call SetIntArrayTo(SelPlane,3,0)
      call SetRealArrayTo(XPlane,9,-1111.)
      call SetRealArrayTo(DxPlane,6,-1111.)
      call SetStringArrayTo(StPlane,3,' ')
      DeltaPlane=-1111.
      call SetStringArrayTo(StPlane,3,' ')
      call SetRealArrayTo(ScopePlane,2,2.)
      ScopePlane(3)=0.
      DrawAtN=0
      do i=1,3
        ShiftPlane(i)=.5*ScopePlane(i)
      enddo
1400  call CopyVek(ScopePlane,xgo,3)
      if(DeltaPlane.lt.0.) then
        if(DrawPDF) then
          DeltaPlane=0.05
        else
          DeltaPlane=1.
          do i=1,3
            DeltaPlane=min(DeltaPlane,dx(i)*CellParCon(i))
          enddo
          rad=10.**Exponent10(DeltaPlane)
          DeltaPlane=anint(DeltaPlane/rad)*rad
          DeltaPlane=max(.01,DeltaPlane)
        endif
      endif
      if(SelPlane(1).eq.0.or.at.ne.' ') then
        if(at.ne.' ') then
          call CtiAt(at,xg,ich)
          if(ich.eq.-1) then
            StPlane(1)=at
            call CopyVek(xg,XPlane,3)
            SelPlane(1)=1
          else
            call CopyVek(xg,XPlane,3)
            SelPlane(1)=2
          endif
          do i=2,3
            if(SelPlane(i).le.2) then
              do j=1,3
                DxPlane(j,i)=XPlane(j,i)-XPlane(j,1)
              enddo
            else if(SelPlane(i).eq.3) then
              do j=1,3
                XPlane(j,i)=XPlane(j,1)+DxPlane(j,i)
              enddo
            endif
          enddo
        else
          StPlane(1)=' '
        endif
      endif
      nEdw=nEdwFirst
      do i=1,3
        do j=1,3
          if(j.eq.1) then
            call FeQuestStringEdwOpen(nEdw,StPlane(i))
          else if(j.eq.2) then
            call FeQuestRealAEdwOpen(nEdw,XPlane(1,i),3,
     1                               XPlane(1,i).lt.-1000.,.false.)
          else if(j.eq.3.and.i.gt.1) then
            call FeQuestRealAEdwOpen(nEdw,DxPlane(1,i),3,
     1                               DxPlane(1,i).lt.-1000.,.false.)
          endif
          if(SelPlane(i).eq.j) then
            call FeQuestEdwSelect(nEdw)
          else if(i.ne.1.or.j.ne.3) then
            call FeQuestEdwDeselect(nEdw)
          endif
          nEdw=nEdw+1
        enddo
      enddo
      call FeQuestRealEdwOpen(nEdwDelta,DeltaPlane,.false.,.false.)
      call FeQuestRealAEdwOpen(nEdwScope,ScopePlane,3,.false.,.false.)
      call FeQuestRealAEdwOpen(nEdwShift,ShiftPlane,3,.false.,.false.)
1500  nEdw=nEdwFirst
      do i=1,9
        if(i.ne.3) StOld(i)=EdwStringQuest(nEdw)
        nEdw=nEdw+1
      enddo
1520  IPlaneOld=IPlane
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1250
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuOldPlane) then
        IPlane=RolMenuSelectedQuest(nRolMenuOldPlane)
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNewName) then
        LabelPlaneNew=EdwStringQuest(nEdwNewName)
        IPlane=LocateInStringArray(LabelPlane,NumberOfPlanes,
     1                             LabelPlaneNew,.true.)
        if(IPlane.gt.0) then
          if(FeYesNo(-1.,-1.,'The plane "'//
     1                       LabelPlaneNew(:idel(LabelPlaneNew))//
     2                       '" already exists. Do you want to load it?'
     3                      ,1)) then
            call FeQuestCrwOff(nCrwNew)
            call FeQuestEdwDisable(nEdwNewName)
            call FeQuestCrwOn(nCrwOld)
            call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                              NumberOfPlanes,max(IPlane,1))
            call FeReleaseOutput
            call FeDeferOutput
            EdwActive=0
            go to 1300
          else
            EventType=EventEdw
            EventNumber=nEdwNewName
            IPlane=0
          endif
        endif
        go to 1520
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwFirst.and.
     1                                  CheckNumber.le.nEdwLast) then
        t256=EdwStringQuest(CheckNumber)
        iw=CheckNumber-nEdwFirst+1
        if(t256.eq.StOld(iw)) go to 1520
        ip=(iw-1)/3+1
        it=mod(iw-1,3)+1
        nEdw=(ip-1)*3+nEdwFirst
        do i=1,3
          if(ip.ne.1.or.i.lt.3) call FeQuestEdwDeselect(nEdw)
          nEdw=nEdw+1
        enddo
        if(it.eq.1) then
          if(t256.eq.' ') go to 1520
          call CtiAt(t256,xg,ich)
          if(ich.eq.-2) then
            go to 1565
          else if(ich.eq.0) then
            Veta='the atom isn''t present, try again.'
            go to 1560
          endif
          go to 1570
1560      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
1565      EventType=EventEdw
          EventNumber=CheckNumber
          go to 1520
1570      call FeQuestRealAEdwOpen(CheckNumber+1,xg,3,.false.,.false.)
          if(ip.ne.1) then
            do i=1,3
              xg(i)=xg(i)-EdwRealQuest(i,nEdwFirst+1)
            enddo
            call FeQuestRealAEdwOpen(CheckNumber+2,xg,3,.false.,.false.)
          endif
        else if(it.eq.2) then
          call FeQuestStringEdwOpen(CheckNumber-1,' ')
          if(ip.ne.1) then
            do i=1,3
              xg(i)=EdwRealQuest(i,CheckNumber)-
     1              EdwRealQuest(i,nEdwFirst+1)
            enddo
            call FeQuestRealAEdwOpen(CheckNumber+1,xg,3,.false.,.false.)
          endif
        else if(it.eq.3) then
          call FeQuestStringEdwOpen(CheckNumber-2,' ')
          do i=1,3
            xg(i)=EdwRealQuest(i,CheckNumber)+
     1            EdwRealQuest(i,nEdwFirst+1)
          enddo
          call FeQuestRealAEdwOpen(CheckNumber-1,xg,3,.false.,.false.)
        endif
        if(ip.eq.1) then
          do i=2,3
            if(SelPlane(i).eq.0) then
              cycle
            else if(SelPlane(i).lt.3) then
              k=3*i+nEdwFirst-2
              do j=1,3
                xg(j)=EdwRealQuest(j,k)-EdwRealQuest(j,nEdwFirst+1)
              enddo
              call FeQuestRealAEdwOpen(k+1,xg,3,.false.,.false.)
            else if(SelPlane(i).eq.3) then
              k=3*i+nEdwFirst-1
              do j=1,3
                xg(j)=EdwRealQuest(j,k)+EdwRealQuest(j,nEdwFirst+1)
              enddo
              call FeQuestRealAEdwOpen(k-1,xg,3,.false.,.false.)
            endif
          enddo
        endif
        SelPlane(ip)=it
        call FeQuestEdwSelect(CheckNumber)
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwScope) then
        if(EdwStringQuest(nEdwShift).eq.' ') go to 1770
        call FeQuestRealAFromEdw(nEdwScope,xg)
        call FeQuestRealAFromEdw(nEdwShift,yg)
        do i=1,3
          yg(i)=yg(i)*2.
        enddo
        if(eqrv(xgo,yg,3,.0001)) then
          go to 1770
        else
          if(eqrv(xg,yg,3,.0001)) call CopyVek(xg,xgo,3)
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdjust)
     1  then
        call FeQuestRealAFromEdw(nEdwScope,xg)
        go to 1770
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRenDel)
     1  then
        WizardModeSave=WizardMode
        WizardMode=.false.
        idp=NextQuestId()
        ln=NextLogicNumber()
        open(ln,file=fln(:ifln)//'_planes.tmp')
        write(ln,'(a)')(LabelPlane(i)(:idel(LabelPlane(i))),
     1                  i=1,NumberOfPlanes)
        close(ln)
        xqd=250.
        il=10
        call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
        xpom=5.
        il=9
        call FeQuestSbwMake(idp,xpom,il,xqd-2.*xpom-SbwPruhXd,9,1,
     1                      CutTextFromRight,SbwVertical)
        nSbwModify=SbwLastMade
        call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'_planes.tmp')
        il=il+1
        Veta='%Rename'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=xqd*.5-1.5*dpom-10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtRename=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%Delete'
        xpom=xpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtDelete=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='Re%set'
        xpom=xpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtReset=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1730    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtRename.or.CheckNumber.eq.nButtDelete.or.
     2      CheckNumber.eq.nButtReset)) then
          ItemFromOld=SbwItemFromQuest(nSbwModify)
          ItemSelOld=SbwItemPointerQuest(nSbwModify)
          if(CheckNumber.eq.nButtRename) then
            t256=SbwStringQuest(ItemSelOld,nSbwModify)
            idr=NextQuestId()
            Veta='Rename "'//t256(:idel(t256))//'" to'
            xqdr=FeTxLength(Veta)+60.
            il=1
            call FeQuestCreate(idr,-1.,-1.,xqdr,il,Veta,0,LightGray,0,0)
            xpom=5.
            dpom=xqdr-10.
            il=1
            call FeQuestEdwMake(idr,0.,il,xpom,il,' ','C',dpom,EdwYd,0)
            nEdwRename=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,t256)
1740        call FeQuestEvent(idr,ich)
            if(CheckType.ne.0) then
              call NebylOsetren
              go to 1740
            endif
            if(ich.eq.0) t256=EdwStringQuest(nEdwRename)
            call FeQuestRemove(idr)
            if(ich.ne.0) go to 1730
          else if(CheckNumber.eq.nButtDelete) then
            t256=DelString
          else if(CheckNumber.eq.nButtReset) then
            t256=LabelPlane(ItemSelOld)
          endif
          call CloseIfOpened(SbwLnQuest(nSbwModify))
          call RewriteLinesOnFile(SbwFileQuest(nSbwModify),ItemSelOld,
     1                            ItemSelOld,ta256,1)
          call FeQuestSbwMenuOpen(nSbwModify,SbwFileQuest(nSbwModify))
          call FeQuestSbwShow(nSbwModify,ItemFromOld)
          call FeQuestSbwItemOff(nSbwModify,
     1                           SbwItemFromQuest(nSbwModify))
          call FeQuestSbwItemOn(nSbwModify,ItemSelOld)
          go to 1730
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1730
        endif
        if(ich.eq.0) then
          call CloseIfOpened(SbwLnQuest(nSbwModify))
          lni=NextLogicNumber()
          call OpenFile(lni,fln(:ifln)//'.l51','formatted','old')
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'.l52','formatted','unknown')
          ln =NextLogicNumber()
          call OpenFile(ln ,fln(:ifln)//'_planes.tmp','formatted',
     1                  'unknown')
1750      read(lni,FormA,end=1760) Veta
          write(lno,FormA) Veta(:idel(Veta))
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'contour')) then
            n=0
            NumberOfPlanes=0
            Skip=.false.
1755        read(lni,FormA,end=1760) Veta
            k=0
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,idContour(nCmdLabelPlane))) then
              n=n+1
              read(ln,FormA,end=1760) Veta
              skip=EqIgCase(Veta,DelString)
              if(.not.skip) then
                NumberOfPlanes=NumberOfPlanes+1
                call ConReallocatePlanes
                LabelPlane(NumberOfPlanes)=Veta
                Veta=idContour(nCmdLabelPlane)
     1               (:idel(idContour(nCmdLabelPlane))+1)//
     2               Veta(:idel(Veta))
              else
                go to 1755
              endif
            else if(EqIgCase(Cislo,idContour(nCmdEndPlane))) then
              if(skip) then
                skip=.false.
                go to 1755
              endif
            else if(EqIgCase(Cislo,'end')) then
              write(lno,FormA) Veta(:idel(Veta))
              go to 1750
            else
              if(skip) go to 1755
            endif
            write(lno,FormA) Veta(:idel(Veta))
            go to 1755
          else
            go to 1750
          endif
1760      call CloseIfOpened(lni)
          call CloseIfOpened(lno)
          close(ln,status='delete')
          if(FileDiff(fln(:ifln)//'.l52',fln(:ifln)//'.l51')) then
            call MoveFile(fln(:ifln)//'.l52',fln(:ifln)//'.l51')
            Change=.true.
          else
            call DeleteFile(fln(:ifln)//'.l52')
          endif
        endif
        call FeQuestRemove(idp)
        WizardMode=WizardModeSave
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 1800
1770  call CopyVek(xg,xgo,3)
      do i=1,3
        xg(i)=xg(i)*.5
      enddo
      call FeQuestRealAEdwOpen(nEdwShift,xg,3,.false.,.false.)
      if(CheckType.eq.EventButton) then
        EventType=EventEdw
        EventNumber=nEdwShift
      endif
      go to 1500
1800  if(ich.eq.0) then
        if(EdwStateQuest(nEdwNewName).eq.EdwOpened) then
          LabelPlaneNew=EdwStringQuest(nEdwNewName)
        else
          LabelPlaneNew=' '
        endif
        nEdw=nEdwFirst
        do i=1,3
          StPlane(i)=EdwStringQuest(nEdw)
          nEdw=nEdw+1
          call FeQuestRealAFromEdw(nEdw,XPlane(1,i))
          nEdw=nEdw+1
          if(i.ne.1) call FeQuestRealAFromEdw(nEdw,DxPlane(1,i))
          nEdw=nEdw+1
        enddo
        call FeQuestRealFromEdw(nEdwDelta,DeltaPlane)
        call FeQuestRealAFromEdw(nEdwScope,ScopePlane)
        call FeQuestRealAFromEdw(nEdwShift,ShiftPlane)
        if(klic.eq.1) call FeQuestRealFromEdw(nEdwCutOffDist,CutOffDist)
      endif
      if(ich.ne.0) then
        if(Change) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to save changes in '//
     1       'plane definition?',1)) call iom50(0,0,fln(:ifln)//'.m50')
          call DefaultContour
          call NactiContour
          if(ErrFlag.ne.0) then
            ErrFlag=0
            call DefaultContour
          endif
        endif
        if(.not.WizardMode) call FeQuestRemove(id)
        go to 9999
      else
        MuzeZpet=1
        go to 2000
      endif
      entry ConMakeGenSection(ich)
      MuzeZpet=0
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
2000  call TrPor
      if(MuzeZpet.ne.0) then
        do i=1,3
          if(SelPlane(i).le.0) go to 9000
        enddo
      endif
      pom=0.
      do i=1,3
        xr(i)=XPlane(i,2)-XPlane(i,1)
        pom=pom+abs(xr(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTens(1,nsubs,KPhase),xr,xg,3,3,1)
      call vecnor(xr,xg)
      pom=0.
      do i=1,3
        yr(i)=XPlane(i,3)-XPlane(i,1)
        pom=pom+abs(yr(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTens(1,nsubs,KPhase),xr,xg,3,3,1)
      call multm(MetTens(1,nsubs,KPhase),yr,yg,3,3,1)
      pom=-scalmul(xr,yg)/scalmul(xr,xg)
      do i=1,3
        yr(i)=yr(i)+pom*xr(i)
      enddo
      call multm(MetTens(1,nsubs,KPhase),yr,yg,3,3,1)
      call vecnor(yr,yg)
      p3(1)=xr(2)*yr(3)-yr(2)*xr(3)
      p3(2)=xr(3)*yr(1)-yr(3)*xr(1)
      p3(3)=xr(1)*yr(2)-yr(1)*xr(2)
      pom=0.
      do i=1,3
        pom=pom+abs(p3(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTensI(1,nsubs,KPhase),p3,zr,3,3,1)
      call vecnor(zr,p3)
      do i=1,3
        trobi(i,1)=xr(i)
        trobi(i,2)=yr(i)
        trobi(i,3)=zr(i)
        xo(i)=-ShiftPlane(1)*xr(i)-ShiftPlane(2)*yr(i)
     1        -ShiftPlane(3)*zr(i)
      enddo
      do i=1,3
        xob(i)=xo(i)+XPlane(i,1)
      enddo
      call matinv(trobi,trob,pom,3)
      do i=1,3
        xr(i)=DeltaPlane*xr(i)
        yr(i)=DeltaPlane*yr(i)
        zr(i)=DeltaPlane*zr(i)
        nxr(i)=nint(ScopePlane(i)/DeltaPlane)+1
        xminr(i)=-ShiftPlane(i)
        xmaxr(i)= xminr(i)+float(nxr(i)-1)*DeltaPlane
        dxr(i)=DeltaPlane
      enddo
      if(nxr(1).le.1.or.nxr(2).le.1) go to 9000
      go to 9999
9000  call FeChybne(-1.,-1.,'the plane isn''t properly defined, '//
     1              'try again',' ',SeriousError)
      if(MuzeZpet.eq.1) then
        call FeQuestButtonOff(ButtonOk-ButtonFr+1)
        go to 1520
      else
        ich=1
      endif
9999  if(.not.WizardMode.and.MuzeZpet.ne.0) call FeQuestRemove(id)
      if(NDimI(KPhase).gt.0) call TrOrtho(0)
      return
      end
      subroutine zmcmlt(klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      j=0
      do i=1,inm(1)
        j=j+1
        if(klic.eq.0) then
          cmlt(j)=cmlt(j)*1.666667e-4
        else
          cmlt(j)=cmlt(j)/1.666667e-4
        endif
      enddo
      do i=1,inm(2)
        j=j+1
        if(klic.eq.0) then
          cmlt(j)=cmlt(j)*4.166667e-6
        else
          cmlt(j)=cmlt(j)/4.166667e-6
        endif
      enddo
      do i=1,inm(3)
        j=j+1
        if(klic.eq.0) then
          cmlt(j)=cmlt(j)*8.333333e-8
        else
          cmlt(j)=cmlt(j)/8.333333e-8
        endif
      enddo
      do i=1,inm(4)
        j=j+1
        if(klic.eq.0) then
          cmlt(j)=cmlt(j)*1.388889e-9
        else
          cmlt(j)=cmlt(j)/1.388889e-9
        endif
      enddo
      return
      end
      function rank3(c)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension c(10)
      rank3=w1*(wm3p1*c(1) +wmp2*c(4)+wmp3*c(6))+
     1      w2*(wm3p2*c(7) +wmp1*c(2)+wmp3*c(9))+
     2      w3*(wm3p3*c(10)+wmp1*c(3)+wmp2*c(8)+w12*c(5))
      return
      end
      function rank4(d)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension d(15)
      rank4=pm63_1*d(1)+pm63_2*d(11)+pm63_3*d(15)+
     1       w12*(wm3p1*d(2) +wm3p2*d(7) +wmp3*d(9))+
     2       w13*(wm3p1*d(3) +wm3p3*d(10)+wmp2*d(8))+
     3       w23*(wm3p2*d(12)+wm3p3*d(14)+wmp1*d(5))+
     4       wmp1*(wmp2*d(4)+wmp3*d(6))+wmp2*wmp3*d(13)
      return
      end
      function rank5(e)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension e(21)
      rank5=w1*(pm1015_1*e(1)+pm63_2*e(11)+pm63_3*e(15)+wm3p1*wmp2*e(4)
     1         +wmp3*(wm3p1*e(6)+wmp2*e(13))+
     2          w2*w3*(wm3p1*e(5)+wm3p2*e(12)+wm3p3*e(14)))+
     3      w2*(pm1015_2*e(16)+pm63_1*e(2)+pm63_3*e(20)+wm3p2*wmp1*e(7)
     4         +wmp3*(wm3p2*e(18)+wmp1*e(9)))+
     5      w3*(pm1015_3*e(21)+pm63_1*e(3)+pm63_2*e(17)+wm3p3*wmp1*e(10)
     6          +wmp2*(wm3p3*e(19)+wmp1*e(8)))
      return
      end
      function rank6(f)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension f(28)
      pom=wmp110_1*wmp3_1*wmp35_1*f(1)+
     1    wmp110_2*wmp3_2*wmp35_2*f(22)+
     2    wmp110_3*wmp3_3*wmp35_3*f(28)
      pom=pom+w1*(w2*(pm1015_1*f(2)+pm1015_2*f(16))+
     1            w3*(pm1015_1*f(3)+pm1015_3*f(21)))+
     2         w2*w3*(pm1015_2*f(23)+pm1015_3*f(27))+
     3           w12*(pm63_3*f(20)+wmp3*(wm3p1*f(9)+wm3p2*f(18))+
     4                wm3p1*wm3p2*f(7))+
     5           w13*(pm63_2*f(17)+wmp2*(wm3p1*f(8)+wm3p3*f(19))+
     6                wm3p1*wm3p3*f(10))+
     7           w23*(pm63_1*f(5)+wmp1*(wm3p2*f(12)+wm3p3*f(14))+
     8                wm3p2*wm3p3*f(25))
      rank6=pom+wmp1*(wmp2*wmp3*f(13)+pm63_2*f(11)+pm63_3*f(15))+
     1          wmp2*(pm63_1*f(4)+pm63_3*f(26))+
     2          wmp3*(pm63_2*f(24)+pm63_1*f(6))
      return
      end
      subroutine priprav(itf)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      w11=w1**2
      w22=w2**2
      w33=w3**2
      w12=w1*w2
      w13=w1*w3
      w23=w2*w3
      if(itf.gt.2) then
        wmp1=w11-p11
        wmp2=w22-p22
        wmp3=w33-p33
        wm3p1=w11-3.*p11
        wm3p2=w22-3.*p22
        wm3p3=w33-3.*p33
        if(itf.gt.3) then
          pom1=w11-5.449490*p11
          pom2=w11-0.550510*p11
          pm63_1=pom1*pom2
          pom1=w22-5.449490*p22
          pom2=w22-0.550510*p22
          pm63_2=pom1*pom2
          pom1=w33-5.449490*p33
          pom2=w33-0.550510*p33
          pm63_3=pom1*pom2
          if(itf.gt.4) then
            pom1=w11-1.837722*p11
            pom2=w11-8.162278*p11
            pm1015_1=pom1*pom2
            pom1=w22-1.837722*p22
            pom2=w22-8.162278*p22
            pm1015_2=pom1*pom2
            pom1=w33-1.837722*p33
            pom2=w33-8.162278*p33
            pm1015_3=pom1*pom2
            if(itf.gt.5) then
              wmp110_1=(w11-11.050687*p11)
              wmp3_1=(w11-0.380327*p11)
              wmp35_1=(w11-3.568986*p11)
              wmp110_2=(w22-11.050687*p22)
              wmp3_2=(w22-0.380327*p22)
              wmp35_2=(w22-3.568986*p22)
              wmp110_3=(w33-11.050687*p33)
              wmp3_3=(w33-0.380327*p33)
              wmp35_3=(w33-3.568986*p33)
            endif
          endif
        endif
      endif
      return
      end
      function ran1(id)
      dimension r(97)
      parameter (m1=259200,ia1=7141,ic1=54773,rm1=3.8580247e-6)
      parameter (m2=134456,ia2=8121,ic2=28411,rm2=7.4373773e-6)
      parameter (m3=243000,ia3=4561,ic3=51349)
      save r,ix1,ix2,ix3
      data iff/0/
      idum=-id
      if(idum.lt.0.or.iff.eq.0) then
        iff=1
        ix1=mod(ic1-idum,m1)
        ix1=mod(ia1*ix1+ic1,m1)
        ix2=mod(ix1,m2)
        ix1=mod(ia1*ix1+ic1,m1)
        ix3=mod(ix1,m3)
        do j=1,97
          ix1=mod(ia1*ix1+ic1,m1)
          ix2=mod(ia2*ix2+ic2,m2)
          r(j)=(float(ix1)+float(ix2)*rm2)*rm1
        enddo
        idum=1
      endif
      ix1=mod(ia1*ix1+ic1,m1)
      ix2=mod(ia2*ix2+ic2,m2)
      ix3=mod(ia3*ix3+ic3,m3)
      j=1+(97*ix3)/m3
      if(j.gt.97.or.j.lt.1) pause
      ran1=r(j)
      r(j)=(float(ix1)+float(ix2)*rm2)*rm1
      return
      end
      subroutine TrPor
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      call UnitMat(tm,NDim(KPhase))
      do i=1,NDim(KPhase)
        do j=1,NDim(KPhase)
          k=i+(j-1)*NDim(KPhase)
          if(j.eq.iorien(i)) then
            tm(k)=1.
          else
            tm(k)=0.
          endif
        enddo
      enddo
      call matinv(tm,tmi,pom,NDim(KPhase))
      call MatBlock3(tm,tm3,NDim(KPhase))
      call MatBlock3(tmi,tm3i,NDim(KPhase))
      if(mapa.gt.0.and.mapa.le.3.and.kpdf.le.1) then
        ncst=2
      else
        ncst=1
      endif
      return
      end
      subroutine GoToMap(ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*5 t5
      if(kpdf.eq.2.and.NDim(KPhase).gt.3) then
        j=1
      else
        j=0
      endif
      l=NDim(KPhase)-2+j
      id=NextQuestId()
      xqd=180.
      call FeQuestCreate(id,-1.,-1.,xqd,l,'Next map to be drawn',0,
     1                   LightGray,0,0)
      tpom=5.
      dpom=100.
      do i=1,NDim(KPhase)-2
        t5=cx(i+2)//' ='
        if(t5(2:2).eq.' ') then
          t5='%'//t5(1:1)//t5(3:4)
        else
          t5=t5(1:1)//'%'//t5(2:4)
        endif
        if(i.eq.1) xpom=tpom+FeTxLengthUnder(t5)+10.
        call FeQuestEudMake(id,tpom,i,xpom,i,t5,'L',dpom,EdwYd,0)
        pom=xmin(i+2)+(nxlast(i)-1)*dx(i+2)
        call FeQuestRealEdwOpen(i,pom,.false.,.false.)
        call FeQuestEudOpen(i,1,1,1,xmin(i+2),xmax(i+2),dx(i+2))
      enddo
      irec=nint((EdwRealQuest(1,1)-xmin(3))/dx(3))+1+
     1     nint((EdwRealQuest(1,2)-xmin(4))/dx(4))*nx(3)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NDim(KPhase)-2
          call FeQuestRealFromEdw(i,pom)
          if(dx(i+2).ne.0.) then
            nxdraw(i)=nint((pom-xmin(i+2))/dx(i+2))+1
          else
            nxdraw(i)=1
          endif
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine DefLength(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Veta
      id=NextQuestId()
      xqd=200.
      call FeQuestCreate(id,-1.,-1.,xqd,NDimI(KPhase),' ',0,LightGray,
     1                   0,0)
      tpom=5.
      Veta='Length of %'//'X'//'th vector'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-10.-xpom
      do i=1,NDimI(KPhase)
        write(Veta(12:12),'(i1)') i+3
        call FeQuestEdwMake(id,tpom,i,xpom,i,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(i,ee(i),.false.,.false.)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NDimI(KPhase)
          call FeQuestRealFromEdw(i,ee(i))
        enddo
        call SetTr
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine SetTr
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      c=1.
      alfa=90.
      bett=90.
      if(obecny) then
        do i=1,3
          write(cx(i),'(''e'',i1)') i
        enddo
        if(tmapy) then
          do i=4,6
            cx(i)=smbt(i-3)
          enddo
        else
          do i=4,6
            cx(i)=smbx6(i)
          enddo
        endif
        a=1.
        b=1.
        gama=90.
        read(m8,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                 (j,i=1,6),mapa,nsubs,SatelityByly,
     2                 nonModulated(KPhase)
      else
        i=iorien(1)
        j=iorien(2)
        k=iorien(3)
        if(i.gt.3.or.j.gt.3) then
          call ConSetCellParCon
          a=sqrt(MetTens6(i+(i-1)*NDim(KPhase),NSubs,KPhase))
          b=sqrt(MetTens6(j+(j-1)*NDim(KPhase),NSubs,KPhase))
          c=sqrt(MetTens6(k+(k-1)*NDim(KPhase),NSubs,KPhase))
          pom=MetTens6(i+(j-1)*NDim(KPhase),NSubs,KPhase)/(a*b)
          gama=acos(pom)
          if(i.gt.3) then
            gaman=atan(a/ee(i-3)*tan(gama))
            a=ee(i-3)
            b=b*sin(gama)/sin(gaman)
            gama=gaman
          endif
          if(j.gt.3) then
            gaman=atan(b/ee(j-3)*tan(gama))
            b=ee(j-3)
            a=a*sin(gama)/sin(gaman)
            gama=gaman
          endif
          gama=gama/ToRad
        else
          call ConSetCellParCon3
          a=sqrt(MetTens(i+(i-1)*3,NSubs,KPhase))
          b=sqrt(MetTens(j+(j-1)*3,NSubs,KPhase))
          c=sqrt(MetTens(k+(k-1)*3,NSubs,KPhase))
          pom=MetTens(i+(j-1)*3,NSubs,KPhase)/(a*b)
          gama=acos(pom)/ToRad
        endif
      endif
      csa=cos(torad*alfa)
      csb=cos(torad*bett)
      csg=cos(torad*gama)
      sng=sin(torad*gama)
      volume=sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      if(xymap) then
        F2O(1)=a
        F2O(2)=0.
        F2O(4)=b*csg
        F2O(5)=b*sng
      else
        F2O(1)=a*sng
        F2O(4)=0.
        if(ReverseX4) then
          F2O(2)=-a*csg
          F2O(5)=-b
        else
          F2O(2)=a*csg
          F2O(5)=b
        endif
      endif
      F2O(3)=0.
      F2O(6)=0.
      F2O(7)=c*csb
      F2O(8)=c*(csa-csb*csg)/sng
      F2O(9)=c*volume/sng
      call matinv(F2O,O2F,pom,3)
      call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
      return
      end
      subroutine ConSetCellParCon
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      m=0
      do i=1,NDim(KPhase)
        if(nx(i).le.1) cycle
        m=m+1
        j=iorien(i)
        CellParCon(m)=
     1               sqrt(MetTens6(j+(j-1)*NDim(KPhase),NSubs,KPhase))
        if(m.ge.3) exit
      enddo
      mi=0
      m=0
      do i=1,NDim(KPhase)
        if(nx(i).le.1) cycle
        j1=iorien(i)
        mi=mi+1
        mj=mi
        do j=i+1,NDim(KPhase)
          if(nx(j).le.1) cycle
          j2=iorien(j)
          mj=mj+1
          m=m+1
          pom=MetTens6(j1+(j2-1)*NDim(KPhase),NSubs,KPhase)/
     1        (CellParCon(mi)*CellParCon(mj))
          if(abs(pom).lt.1.) then
            CellParCon(9-mi-mj)=acos(pom)/ToRad
          else
            CellParCon(9-mi-mj)=90.
          endif
          if(mj.ge.3) exit
        enddo
        if(mi.ge.2) exit
      enddo
      return
      end
      subroutine ConSetCellParCon3
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      m=0
      do i=1,NDim(KPhase)
        m=m+1
        j=iorien(i)
        CellParCon(m)=
     1               sqrt(MetTens(j+(j-1)*3,NSubs,KPhase))
        if(m.ge.3) exit
      enddo
      mi=0
      m=0
      do i=1,NDim(KPhase)
        j1=iorien(i)
        mi=mi+1
        mj=mi
        do j=i+1,NDim(KPhase)
          if(nx(j).le.1) cycle
          j2=iorien(j)
          mj=mj+1
          m=m+1
          pom=MetTens(j1+(j2-1)*3,NSubs,KPhase)/
     1        (CellParCon(mi)*CellParCon(mj))
          if(abs(pom).lt.1.) then
            CellParCon(9-mi-mj)=acos(pom)/ToRad
          else
            CellParCon(9-mi-mj)=90.
          endif
          if(mj.ge.3) exit
        enddo
        if(mi.ge.2) exit
      enddo
      return
      end
      subroutine ConCellParNorm(CellParP)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension CellParP(*)
      m=0
      pom1=0.
      m1=0
      pom2=0.
      m2=0
      do i=1,NDim(KPhase)
        if(nx(i).le.1) cycle
        m=m+1
        j=iorien(i)
        if(j.le.3) then
          pom1=pom1+CellParP(m)
          m1=m1+1
        else
          pom2=pom2+CellParP(m)
          m2=m2+1
        endif
        if(m.ge.3) exit
      enddo
      if(m1.gt.0.and.m2.gt.0) then
        pom=(pom1*float(m2))/(pom2*float(m1))
        m=0
        do i=1,NDim(KPhase)
          if(nx(i).le.1) cycle
          m=m+1
          if(iorien(i).gt.3)
     1      CellParP(m)=CellParP(m)*pom
          if(m.ge.3) exit
        enddo
      endif
      return
      end
      subroutine Trx4t
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension tmin(3),tmax(3),dt(3)
      data tmin/3*0./,tmax/3*1./,dt/3*.1/
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)-2,
     1                   'Define intervals for t maps',0,LightGray,0,0)
      tpom=10.
      dpom=80.
      spom=dpom+10.
      do i=1,NDimI(KPhase)
        xpom=tpom+PropFontHeightInPixels+10.
        il=i+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,smbt(i),'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,tmin(i),.false.,.false.)
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          call FeQuestLblMake(id,xpom+spom*.5,il-1,'Min','C','N')
        endif
        xpom=xpom+spom
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        if(i.eq.1)
     1    call FeQuestLblMake(id,xpom+spom*.5,il-1,'Min','C','N')
        call FeQuestRealEdwOpen(EdwLastMade,tmax(i),.false.,.false.)
        xpom=xpom+spom
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        if(i.eq.1)
     1    call FeQuestLblMake(id,xpom+spom*.5,1,'Step','C','N')
        call FeQuestRealEdwOpen(EdwLastMade,dx(i+3),.false.,.false.)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        j=nEdwFirst
        do i=1,NDimI(KPhase)
          call FeQuestRealFromEdw(j,tmin(i))
          j=j+1
          call FeQuestRealFromEdw(j,tmax(i))
          j=j+1
          call FeQuestRealFromEdw(j,dt(i))
          j=j+1
        enddo
        call FeQuestRemove(id)
        call rezt(tmin,tmax,dt)
        if(ErrFlag.eq.0) tmapy=.true.
        go to 9999
      endif
      ErrFlag=1
      call FeQuestRemove(id)
9999  return
      end
      subroutine rezt(tmin,tmax,dt)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(3),x4(1),tt(3),nn(4),nn0(4),nn1(4),tmin(3),tmax(3),
     1          dt(3),nt(3),nd(3),
     2          dtp(:,:),irez(:),tbl(:,:),table(:)
      integer RecPack
      allocatable irez,dtp,tbl,table
      equivalence (xx(1),x),(xx(2),y),(xx(3),z),(x4(1),x4p)
      allocate(irez(nxny),tbl(nxny,2**NDimI(KPhase)),dtp(3,nxny),
     1         table(nxny))
      ntt=1
      do i=1,NDimI(KPhase)
        nt(i)=nint((tmax(i)-tmin(i))/dt(i))+1
        ntt=ntt*nt(i)
      enddo
      nmap=nx(3)*ntt
      call FeFlowChartOpen(-1.,-1.,1,nmap,
     1                     'Transformation x4 maps to t maps',' ',' ')
      call CloseIfOpened(84)
      call OpenMaps(84,fln(:ifln)//'.l84',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      irec=0
      do it=1,ntt
        call RecUnpack(it,nd,nt,NDimI(KPhase))
        do i=1,NDimI(KPhase)
          tt(i)=tmin(i)+(nd(i)-1)*dt(i)
        enddo
        if(.not.obecny) z=xmin(3)
        do iz=1,nx(3)
          if(.not.obecny) y=xmin(2)
          i=0
          do iy=1,nx(2)
            if(.not.obecny) x=xmin(1)
            do ix=1,nx(1)
              i=i+1
              if(obecny) then
                do j=1,3
                  xx(j)=xob(j)+xr(j)*float(ix-1)+yr(j)*float(iy-1)+
     1                         zr(j)*float(iz-1)
                enddo
              endif
              nn(1)=iz
              do j=1,NDimI(KPhase)
                jj=j+3
                x4p=tt(j)
                do k=1,3
                  if(Obecny) then
                    kk=k
                  else
                    kk=iorien(k)
                  endif
                  x4p=x4p+qu(kk,j,nsubs,KPhase)*xx(k)
                enddo
                call od0do1(x4,x4,1)
                dtpp=(x4p-xmin(jj))/dx(jj)+1.
                k=ifix(dtpp)
                dtp(j,i)=dtpp-float(k)
                nn(j+1)=k
                if(k.gt.nx(jj)) go to 9000
              enddo
              irez(i)=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              if(.not.obecny) x=x+dx(1)
            enddo
            if(.not.obecny) y=y+dx(2)
          enddo
          do i=1,nxny
            ir=irez(i)
            if(ir.eq.0) cycle
            read(m8,rec=ir,err=9100)(tbl(j,1),j=1,nxny)
            call RecUnpack(ir-1,nn0,nx(3),NDim(KPhase)-2)
            nn1(1)=nn0(1)
            do j=1,NDimI(KPhase)
              jj=j+3
              nn1(j+1)=nn0(j+1)+1
              if(nn1(j+1).gt.nx(jj)) nn1(j+1)=1
            enddo
            call CopyVekI(nn0,nn,NDim(KPhase)-2)
            nn(2)=nn1(2)
            k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
            read(m8,rec=k,err=9100)(tbl(j,2),j=1,nxny)
            if(NDim(KPhase).gt.4) then
              nn(2)=nn0(2)
              nn(3)=nn1(3)
              k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              read(m8,rec=k,err=9100)(tbl(j,3),j=1,nxny)
              nn(2)=nn1(2)
              k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              read(m8,rec=k,err=9100)(tbl(j,4),j=1,nxny)
              if(NDim(KPhase).gt.5) then
                nn(2)=nn0(2)
                nn(3)=nn0(3)
                nn(4)=nn1(4)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,5),j=1,nxny)
                nn(2)=nn1(2)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,6),j=1,nxny)
                nn(2)=nn0(2)
                nn(3)=nn1(3)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,7),j=1,nxny)
                nn(2)=nn1(2)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,8),j=1,nxny)
              endif
            endif
            do j=1,nxny
              if(irez(j).ne.ir) cycle
              if(NDimI(KPhase).eq.1) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),pom,pom,
     2                               pom,pom,pom,pom)
              else if(NDimI(KPhase).eq.2) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),tbl(j,3),
     2                               tbl(j,4),
     3                               pom,pom,pom,pom)
              else if(NDimI(KPhase).eq.3) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),tbl(j,3),
     2                               tbl(j,4),tbl(j,5),tbl(j,6),
     3                               tbl(j,7),tbl(j,8))
              endif
              Dmax=max(table(j),Dmax)
              Dmin=min(table(j),Dmin)
              irez(j)=0
            enddo
          enddo
          call FeFlowChartEvent(irec,is)
          if(is.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9900
          endif
          write(84,rec=irec+1)(table(i),i=1,nxny)
          z=z+dx(3)
        enddo
      enddo
      irec=irec+2
      write(84,rec=irec) Dmax,Dmin
      do i=4,NDim(KPhase)
        j=i-3
        nx(i)=nd(j)
        xmin(i)=tmin(j)
        xmax(i)=tmin(j)+(nd(j)-1)*dt(j)
        dx(i)=dt(j)
      enddo
      write(84,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                iorien,mapa,nsubs,SatelityByly,
     2                nonModulated(KPhase)
      do i=4,6
        cx(i)=smbt(i-3)
      enddo
      call SetContours(0)
      do i=3,6
        nxfrom(i-2)=1
        nxto(i-2)=nx(i)
      enddo
      nxdraw(1)=0
      do i=2,4
        nxdraw(i)=1
      enddo
      irecold=-1
      m8=84
      go to 9999
9000  call FeChybne(-1.,YBottomMessage,'Fourier maps don''t allow to '//
     1              'extrapolate all points',' ',SeriousError)
      go to 9900
9100  call FeReadError(m8)
      ErrFlag=1
9900  call DeleteFile(fln(:ifln)//'.l82')
9999  call FeFlowChartRemove
      deallocate(irez,tbl,dtp,table)
      return
      end
      function ExtrapolLin(xd,n,f000,f100,f010,f110,f001,f101,f011,f111)
      dimension xd(n)
      f00=(f100-f000)*xd(1)+f000
      if(n.gt.1) then
        f10=(f110-f010)*xd(1)+f010
        f0=(f10-f00)*xd(2)+f00
        if(n.gt.2) then
          f01=(f101-f001)*xd(1)+f001
          f11=(f111-f011)*xd(1)+f011
          f1=(f11-f01)*xd(2)+f01
          ExtrapolLin=(f1-f0)*xd(3)+f0
        else
          ExtrapolLin=f0
        endif
      else
        ExtrapolLin=f00
      endif
      return
      end
      integer function ConGetFirstSection(nn,nd,n)
      dimension nn(n),nd(n)
      integer RecPack
      j=0
      do i=1,n
        if(nd(i).gt.1) then
          j=i
          exit
        endif
      enddo
      if(j.gt.0) then
        i=nn(j)
        nn(j)=1
      endif
      ConGetFirstSection=RecPack(nn,nd,n)
      if(j.gt.0) nn(j)=i
      return
      end
      subroutine KresliMapu(idal)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      idala=idal
      if(idala.ne.0) call CopyVekI(nxlast,nxdraw,4)
1000  if(idala.eq.1) then
        nxdraw(1)=nxdraw(1)+1
        if(nxdraw(1).gt.nx(3)) then
          nxdraw(1)=1
          nxdraw(2)=nxdraw(2)+1
          if(nxdraw(2).gt.nx(4)) then
            nxdraw(2)=1
            nxdraw(3)=nxdraw(3)+1
            if(nxdraw(3).gt.nx(5)) then
              nxdraw(3)=1
              nxdraw(4)=nxdraw(4)+1
              if(nxdraw(4).gt.nx(6)) then
                do i=1,4
                  nxdraw(i)=nx(i+2)
                enddo
                go to 9999
              endif
            endif
          endif
        endif
      else if(idala.eq.-1) then
        nxdraw(1)=nxdraw(1)-1
        if(nxdraw(1).lt.1) then
          nxdraw(1)=nx(3)
          nxdraw(2)=nxdraw(2)-1
          if(nxdraw(2).lt.1) then
            nxdraw(2)=nx(4)
            nxdraw(3)=nxdraw(3)-1
            if(nxdraw(3).lt.1) then
              nxdraw(3)=nx(5)
              nxdraw(4)=nxdraw(4)-1
              if(nxdraw(4).lt.1) then
                do i=1,4
                  nxdraw(i)=1
                enddo
                go to 9999
              endif
            endif
          endif
        endif
      else if(idala.ne.0) then
        go to 9999
      endif
      if(nxdraw(1).eq.0) nxdraw(1)=1
      irec=nxdraw(1)+(nxdraw(2)-1)*nx(3)+(nxdraw(3)-1)*nx(3)*nx(4)+
     1               (nxdraw(4)-1)*nx(3)*nx(4)*nx(5)
      irec=max(1,irec)
      irec=min(nmap,irec)
      if(FlagMap(irec).eq.2) then
        if(idala.eq.0) idala=1
        go to 1000
      endif
      read(m8,rec=irec+1,err=9000)(ActualMap(i),i=1,nxny)
      do i=3,NDim(KPhase)
        zmap(i-2)=xmin(i)+float(nxdraw(i-2)-1)*dx(i)
      enddo
      irecold=irec
      call ConHeader
      MapExists=.false.
      call CopyVekI(nxdraw,nxlast,4)
2000  if(reconfig) then
        call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
        reconfig=.false.
      endif
      call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      if(ConTintType.ge.1) call ConTintMap
      call FeMakeAcFrame
      if(mapa.lt.50) then
        write(t80,LabelMapy)(cx(i),zmap(i-2),i=3,NDim(KPhase))
        call zhusti(t80)
        t80(idel(t80):)=' '
        pom=min(FeTxLength(t80),abs(XCornAcWin(1,2)-XCornAcWin(1,3)))*.5
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1               (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2               pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3               (XCornAcWin(1,2)-XCornAcWin(1,3)),t80,'C',White)
      endif
      call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
      call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
      if(FlagMap(irec).eq.1) then
        call FeOutSt(0,XCenAcWin,YCenAcWin,
     1               'The map cannot be drawn due to the previous error'
     2              ,'C',Yellow)
        go to 8500
      endif
      if(DrawPos.gt.0) then
        call Vrstvy( 1)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(DrawNeg.gt.0) then
        call Vrstvy(-1)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(HardCopy.eq.0.and..not.JedeMovie) then
        call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                    ConImageFile,0)
        call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                   ConImageFile)
      endif
      if(AtomsOn) call ConDrawAtoms(zmap)
8500  MapExists=.true.
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
9999  call FeHardCopy(HardCopy,'close')
      if(JedeMovie) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
      return
      end
      subroutine ConTintMap
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer FeRGBCompress
      logical InsideXYPlot
      dimension xp(3),xd(3)
      xd(3)=0.
      xp(1)=xmin(1)
      xp(2)=xmin(2)
      xp(3)=0.
      call FeXf2Pixels(xp,ix,iy)
      ixMin=ix
      iyMin=iy
      ixMax=ix
      iyMax=iy
      xp(2)=xmax(2)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      xp(1)=xmax(1)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      xp(2)=xmin(2)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      call FeRGBUncompress(ConPosCol,ir,ig,ib)
      RedPos=ir
      GreenPos=ig
      BluePos=ib
      call FeRGBUncompress(ConNegCol,ir,ig,ib)
      RedNeg=ir
      GreenNeg=ig
      BlueNeg=ib
      do ix=ixMin,ixMax
        do iy=iyMin,iyMax
          call FePixels2Xf(ix,iy,xp)
          call FeXf2X(xp,xo)
          if(.not.InsideXYPlot(xo(1),xo(2))) cycle
          xp(1)=(xp(1)-xmin(1))/dx(1)
          xp(2)=(xp(2)-xmin(2))/dx(2)
          i1=min(ifix(xp(1))+1,nx(1))
          i1=max(i1,1)
          i2=min(ifix(xp(2))+1,nx(2))
          i2=max(i2,1)
          xd(1)=xp(1)-float(i1-1)
          xd(2)=xp(2)-float(i2-1)
          i1p=min(i1+1,nx(1))
          i2p=min(i2+1,nx(2))
          ip=i1+(i2-1)*nx(1)
          f000=ActualMap(ip)
          ip=i1p+(i2-1)*nx(1)
          f100=ActualMap(ip)
          ip=i1+(i2p-1)*nx(1)
          f010=ActualMap(ip)
          ip=i1p+(i2p-1)*nx(1)
          f110=ActualMap(ip)
          pom=ExtrapolLin(xd,2,f000,f100,f010,f110,0.,0.,0.,0.)
          cf=abs(pom)/max(DMax,-DMin)
          if(pom.gt.0.) then
            if(InvertWhiteBlack.and.HardCopy.ne.0) then
              ir=nint((RedPos-255.)*cf)+255
              ig=nint((GreenPos-255.)*cf)+255
              ib=nint((BluePos-255.)*cf)+255
            else
              ir=nint(RedPos*cf)
              ig=nint(GreenPos*cf)
              ib=nint(BluePos*cf)
            endif
          else
            if(InvertWhiteBlack.and.HardCopy.ne.0) then
              ir=nint((RedNeg-255)*cf)+255
              ig=nint((GreenNeg-255)*cf)+255
              ib=nint((BlueNeg-255)*cf)+255
            else
              ir=nint(RedNeg*cf)
              ig=nint(GreenNeg*cf)
              ib=nint(BlueNeg*cf)
            endif
          endif
          if(ir.le.0.and.ig.le.0.and.ib.le.0) cycle
          call FePixelPoint(ix,iy,FeRGBCompress(ir,ig,ib))
        enddo
      enddo
      return
      end
      subroutine vrstvy(iznp)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical konec,minus,FeYesNo
      dimension xp(3),nh(:)
      allocatable nh
      data xp/3*0./
      allocate(nh(nxny))
      if(ContourType.eq.ContourTypeLinear) then
        if(iznp.ge.0) then
          fmz=fmzp
        else
          fmz=fmzn
        endif
        nrmin=0
        nrmax=0
        do i=1,nxny
          nri=abs(ActualMap(i))/fmz
          if(ActualMap(i).lt.0.) nri=-nri-1
          nrmin=min(nri,nrmin)
          nrmax=max(nri,nrmax)
        enddo
        if(fmz.le.0.) go to 9999
        if(iznp.le.0) then
          iv1=nrmin
        else
          iv1=1
        endif
        if(iznp.ge.0) then
          iv2=nrmax
        else
          iv2=0
        endif
      else if(ContourType.eq.ContourTypeUser) then
        iv1=1
        iv2=ContourNumber
      else if(ContourType.eq.ContourTypeAIM) then
        rmax=0.
        do i=1,nxny
          rmax=max(abs(ActualMap(i)),rmax)
        enddo
        iv2=1
        pom=1.
        rad=10.**ContourAIMOrder
        i=0
1100    if(pom*rad.le.rmax) then
          iv2=iv2+1
          i=i+1
          if(i.gt.3) then
            i=1
            rad=rad*10.
            pom=1.
          endif
          pom=pom*2.
          go to 1100
          if(iznp.le.0) then
            iv1=0
          else
            iv1=1
          endif
        endif
      endif
      kolik=0
      call FeMouseShape(3)
      ivm=max((iv2-iv1+1)/10,1)
      nButtInterrupt=0
      if(ContourType.eq.ContourTypeAIM) then
        if(iznp.ge.0) then
          fivp= 1.
        else
          fivp=-1.
        endif
        rad=10.**ContourAIMOrder
        ivp=0
      endif
      do 9000iv=iv1,iv2
        if(.not.JedeMovie) then
          if(mod(iv-iv1,ivm).eq.0.and.
     1       (OpSystem.eq.1.or.HardCopy.lt.1.or.Hardcopy.gt.5)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
          call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            if(FeYesNo(-1.,-1.,'Do you want really to interrupt it?',1))
     1        go to 9500
          else if(EventType.eq.EventButton) then
            if((EventNumber.eq.nButtMapaPlus.and.irec.lt.nmap).or.
     1         (EventNumber.eq.nButtMapaMinus.and.irec.gt.1)) then
              nButtInterrupt=EventNumber
              go to 9999
            endif
          endif
        endif
        if(ContourType.eq.ContourTypeLinear) then
          fiv=fmz*float(iv)
          if(fiv.gt.cutpos.or.fiv.lt.cutneg) go to 9000
        else if(ContourType.eq.ContourTypeUser) then
          fiv=ContourArray(iv)
        else if(ContourType.eq.ContourTypeAIM) then
          if(iv.gt.0) then
            ivp=ivp+1
            if(ivp.gt.3) then
              ivp=1
              rad=rad*10.
              if(iznp.ge.0) then
                fivp= 1.
              else
                fivp=-1.
              endif
            endif
            fivp=fivp*2.
            fiv=fivp*rad
          else
            fiv=0.
          endif
        endif
        k=0
        do j=1,nx(2)
          do 1600i=1,nx(1)
            k=k+1
            pom=ActualMap(k)
            nh(k)=0
            if(pom.lt.fiv) go to 1600
            if(i.ne.1) then
              if(ActualMap(k- 1).lt.fiv) nh(k)=nh(k)+1000
            endif
            if(i.ne.nx(1)) then
              if(ActualMap(k+ 1).lt.fiv) nh(k)=nh(k)+10
            endif
            if(j.ne.1) then
              if(ActualMap(k-nx(1)).lt.fiv) nh(k)=nh(k)+1
            endif
            if(j.ne.nx(2)) then
              if(ActualMap(k+nx(1)).lt.fiv) nh(k)=nh(k)+100
            endif
1600      continue
        enddo
        if(fiv.gt.0.) then
          call FeLineType(NormalLine)
        else if(fiv.eq.0.) then
          call FeLineType(DashedLine)
        else
          call FeLineType(DenseDashedLine)
        endif
6000    ivse=1
        if(ivse.eq.1) then
          k=0
          do j=1,nx(2)
            do i=1,nx(1)
              k=k+1
              nhs=nh(k)/1000*1000
              if(nhs.ne.0) go to 6300
            enddo
          enddo
          ivse=2
        endif
        if(ivse.eq.2) then
          k=nxny
          do j=nx(2),1,-1
            do i=nx(1),1,-1
              nhs=mod(nh(k),100)/10*10
              if(nhs.ne.0) go to 6300
              k=k-1
            enddo
          enddo
          ivse=3
        endif
        if(ivse.eq.3) then
          k=0
          do j=1,nx(2)
            do i=1,nx(1)
              k=k+1
              nhs=mod(nh(k),1000)/100*100
              if(nhs.ne.0) go to 6300
            enddo
          enddo
          ivse=4
        endif
        if(ivse.eq.4) then
          k=nxny
          do j=nx(2),1,-1
            do i=nx(1),1,-1
              nhs=mod(nh(k),10)
              if(nhs.ne.0) go to 6300
              k=k-1
            enddo
          enddo
          if(kolik.gt.1) then
            call FePolyLine(kolik,xpole,ypole,White)
            kolik=0
          endif
          go to 9000
        endif
6300    i1=i
        j1=j
        kk=k
        nhp=nhs
        xp(1)=float(i-1)*dx(1)+xmin(1)
        xp(2)=float(j-1)*dx(2)+xmin(2)
        if(nhs.eq.1000) then
          xp(1)=xp(1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-1))*dx(1)
          minus=j.eq.nx(2)
          if(minus) nh(k)=nh(k)-1000
        else if(nhs.eq.10) then
          xp(1)=xp(1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+1))*dx(1)
          minus=j.eq.1
          if(minus) nh(k)=nh(k)-10
        else if(nhs.eq.100) then
          xp(2)=xp(2)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+nx(1)))*dx(2)
          minus=i.eq.nx(1)
          if(minus) nh(k)=nh(k)-100
        else
          xp(2)=xp(2)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-nx(1)))*dx(2)
          minus=i.eq.1
          if(minus) nh(k)=nh(k)-1
        endif
        call FeXf2X(xp,xo)
        x1=xo(1)
        y1=xo(2)
6350    if(kolik.gt.1) then
          call FePolyLine(kolik,xpole,ypole,White)
          kolik=0
        endif
        xpole(1)=x1
        ypole(1)=y1
        kolik=1
7000    if(.not.minus) then
          if(nhp.eq.1000) then
            kk=kk+nx(1)-1
            i=i-1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq. 100) then
            kk=kk+nx(1)+1
            i=i+1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.  10) then
            kk=kk-nx(1)+1
            i=i+1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.   1) then
            kk=kk-nx(1)-1
            i=i-1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            go to 9500
          endif
        else
          if(nhp.eq.1000) then
            kk=kk-nx(1)-1
            i=i-1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq. 100) then
            kk=kk+nx(1)-1
            i=i-1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.  10) then
            kk=kk+nx(1)+1
            i=i+1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.   1) then
            kk=kk-nx(1)+1
            i=i+1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            go to 9500
          endif
        endif
7500    if(kam.eq.1) then
          xp(1)=dx(1)*(float(i-1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-1)))+xmin(1)
          xp(2)=float(j-1)*dx(2)+xmin(2)
          konec=(j.eq.nx(2).and..not.minus).or.(j.eq.1.and.minus)
        else if(kam.eq.2) then
          xp(1)=float(i-1)*dx(1)+xmin(1)
          xp(2)=dx(2)*(float(j-1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+nx(1))))+xmin(2)
          konec=(i.eq.nx(1).and..not.minus).or.(i.eq.1.and.minus)
        else if(kam.eq.3) then
          xp(1)=dx(1)*(float(i-1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+1)))+xmin(1)
          xp(2)=float(j-1)*dx(2)+xmin(2)
          konec=(j.eq.1.and..not.minus).or.(j.eq.nx(2).and.minus)
        else if(kam.eq.4) then
          xp(1)=float(i-1)*dx(1)+xmin(1)
          xp(2)=dx(2)*(float(j-1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-nx(1))))+xmin(2)
          konec=(i.eq.1.and..not.minus).or.(i.eq.nx(1).and.minus)
        endif
        call FeXf2X(xp,xo)
        xm=xo(1)
        ym=xo(2)
        kolik=kolik+1
        xpole(kolik)=xm
        ypole(kolik)=ym
        if(kolik.ge.mxc) then
          call FePolyLine(kolik,xpole,ypole,White)
          kolik=1
          xpole(1)=xm
          ypole(1)=ym
        endif
        nh(kk)=nh(kk)-nhp
        if(konec.or.(i.eq.i1.and.j.eq.j1.and.nhp.eq.nhs)) then
          if((i.eq.i1.and.j.eq.j1.and.nhp.eq.nhs).or.minus) go to 6000
          nh(k)=nh(k)-nhs
          if((nhs.eq.1000.and.j1.eq.1).or.(nhs.eq.10.and.j1.eq.nx(2))
     1   .or.(nhs.eq.100 .and.i1.eq.1).or.(nhs.eq.1 .and.i1.eq.nx(1)))
     2          go to 6000
          nhp=nhs
          i=i1
          j=j1
          kk=k
          minus=.true.
          go to 6350
        else
          go to 7000
        endif
9000  continue
      go to 9999
9500  ErrFlag=1
9999  call FeLineType(NormalLine)
      deallocate(nh)
      return
      end
      subroutine ConPrelim
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical exist81,first,ExistFile
      data first/.true./
      ConImageFile=fln(:ifln)//'_map.bmp'
      VolaToContour=.true.
      NewAtoms=.false.
      NewPoints=.false.
      ModifiedAtoms=.false.
      SatelityByly=NDim(KPhase).gt.3
      ContourRefLevel=0.
      call UnitMat(tm3,3)
      call UnitMat(tm,NDim(KPhase))
      M81Cteno=.false.
      ReverseX4=.false.
      if(First) then
        call SetRealArrayTo(ee,3,2.)
        First=.false.
      endif
      LabelPlaneNew=' '
      call DefaultContour
      call NactiContour
      if(ErrFlag.ne.0) then
        ErrFlag=0
        call DefaultContour
      endif
      DensityType=0
      OldNorm=.false.
      xymap =.true.
      xyzmap=.true.
      mcmax=0
      errlev=5.
      teplota=DatCollTemp(KDatBlock)
      ErrMapActive=.false.
      MaxRightBut=9
      toev=0.86170296e-1
      tmapy=.false.
      DelayTimeForMovie=.1
      MovieRepeat=0
      if(LocateSubstring(Call3dMaps,'mce.exe',.false.,.true.).gt.1)
     1  then
        IdCall3dMaps=IdCall3dMCE
      else if(LocateSubstring(Call3dMaps,'Vesta.exe',.false.,.true.)
     1       .gt.1) then
        IdCall3dMaps=IdCall3dVesta
      else if(LocateSubstring(Call3dMaps,'MoleCoolQt.exe',.false.,
     1                        .true.).gt.1) then
        IdCall3dMaps=IdCall3dMoleCoolQt
      else
        IdCall3dMaps=IdCall3dNone
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      KPhase=KPhaseBasic
      do i=1,6
        if(i.le.NDim(KPhase)) then
          iorien(i)=i
        else
          iorien(i)=0
        endif
      enddo
      Exist81=ExistFile(fln(:ifln)//'.m81')
1020  if(Exist81) then
        call CloseIfOpened(81)
        call OpenMaps(81,fln(:ifln)//'.m81',nxny,0)
        if(ErrFlag.ne.0) go to 1030
        read(81,rec=1,err=1030) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          iorien,mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
        kpdf1=0
        KPh=nsubs/10+1
        if(KPh.ne.KPhase) go to 1030
        nsubs=mod(nsubs,10)
        xymap =iorien(1).le.3.and.iorien(2).le.3
        xyzmap=xymap.and.iorien(3).le.3
        if(nx(1).lt.3.or.nx(2).lt.3) then
          call FeChybne(-1.,-1.,'the maps are too narrow to be drawn',
     1                  'you may have chosen an incorrect orientation.',
     2                  SeriousError)
          ErrFlag=1
          go to 9999
        endif
        go to 1120
1030    Exist81=.false.
        call CloseIfOpened(81)
        go to 1020
      else
        kpdf1=2
        nsubs=1
      endif
1120  call zmcmlt(0)
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
      kpdf2=7
      if(NDimI(KPhase).gt.0) call trortho(0)
      DrawAtN=0
      AtomsOn=.false.
      call SetStringArrayTo(DrawAtName,NMaxDraw,' ')
      LabelMapy='(.(a2,''='',f6.3,'',''))'
      write(LabelMapy(2:2),'(i1)') NDim(KPhase)-2
      if(ChargeDensities) call ConSetLocDensity
      call ConHeader
9999  return
      end
      subroutine ConOptions
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension XMinColor(2),XMaxColor(2),YMinColor(2),YMaxColor(2),
     1          nLblColor(2),nEdwRGB(2),ip(3,2)
      character*80 Veta
      integer EdwStateQuest,FeRGBCompress
      logical CrwLogicQuest
      id=NextQuestId()
      xqd=450.
      il=14
      if(NDimI(KPhase).gt.0) il=il+3
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,'General options','C','B')
      il=il+1
      Veta='Maximal %distance to locate atoms'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,LocDMax,.false.,.false.)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Drawing options','C','B')
      il=il+1
      Veta='%Ball-and-stick'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          tpom=tpom+xqd*.5
          Veta='%Wires'
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          nCrwBallAndStick=CrwLastMade
          j=DrawStyleBallAndStick
        else if(i.eq.2) then
          nCrwWires=CrwLastMade
          j=DrawStyleWires
        endif
        call FeQuestCrwOpen(CrwLastMade,ConDrawStyle.eq.j)
      enddo
      ilp=-10*il-15
      Veta='d(max) derived from atomic %radii'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,1,2)
      nCrwAtRad=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConDmaxExpl.le.0)
      tpomp=tpom+FeTxLengthUnder(Veta)+15.
      Veta='e%xpanded by'
      xpomp=tpomp+FeTxLengthUnder(Veta)+5.
      dpom=50.
      ilp=ilp-3
      call FeQuestEdwMake(id,tpomp,ilp,xpomp,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwExpand=EdwLastMade
      call FeQuestRealEdwOpen(nEdwExpand,ConExpDMax,.false.,.false.)
      tpomp=xpomp+dpom+5.
      call FeQuestLblMake(id,tpomp,ilp,'%','L','N')
      nLblExpand=LblLastMade
      ilp=ilp-2
      Veta='and typical distances'
      call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
      ilp=ilp-7
      Veta='d(max) as defined for individual atoms in "Atom edit"'
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,1,2)
      nCrwExplicit=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConDmaxExpl.gt.0)
      il=il+4
      xpom=5.
      Veta='Do not draw bonds between atoms of %identical atomic types'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwConSkipSame=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConSkipSame.gt.0)
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        Veta='Use occupancy %cutoff'
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,3)
          if(i.eq.1) then
            nCrwOcc=CrwLastMade
            tpomp=xpom+FeTxLengthUnder(Veta)+5.
            Veta='=>'
            xpomp=tpomp+FeTxLengthUnder(Veta)+5.
            call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,
     1                          EdwYd,0)
            nEdwCutOff=EdwLastMade
            Veta='Reduce radius of drawn atom by actual occupancy'
          endif
          call FeQuestCrwOpen(CrwLastMade,
     1                         i.eq.1.eqv.UseOccupancyCutOff)
        enddo
      else
        nEdwCutOff=0.
        nCrwOcc=0.
      endif
      tpom=xpom+CrwXd+10.
      Veta='%Project all defined atoms'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,4)
        call FeQuestCrwOpen(CrwLastMade,ConDrawDepth.le.0.eqv.i.eq.1)
        if(i.eq.1) then
          nCrwAllProj=CrwLastMade
          Veta='Project %only atoms having distance form the section <'
        else if(i.eq.2) then
          nCrwDrawDepth=CrwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      Veta='Ang'
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawDepth=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,1.,.false.,.false.)
      if(ConDrawDepth.le.0.) then
        call FeQuestEdwDisable(EdwLastMade)
        Depth=1.
      else
        Depth=ConDrawDepth
      endif
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Contours options','C','B')
      il=il+1
      xpom=5.
      Veta='Active %tinting'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwConTintType=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConTintType.gt.0)
      Veta='Drawing color for positive:'
      tpom=5.
      dpom=40.
      xpom=tpom+FeTxLengthUnder(Veta)+dpom+45.
      do j=1,2
        il=il+1
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblColor(j)=LblLastMade
        Veta='R:'
        xpomp=xpom
        do i=1,3
          tpomp=xpomp-15.
          call FeQuestEudMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,
     1                        1)
          xpomp=xpomp+80.
          if(i.eq.1) then
            nEdwRGB(j)=EdwLastMade
            Veta='G:'
          else if(i.eq.2) then
            Veta='B:'
          endif
        enddo
        XMinColor(j)=EdwXMinQuest(nEdwRGB(j)+2)-230.
        XMaxColor(j)=XMinColor(j)+dpom
        YMinColor(j)=EdwYMinQuest(nEdwRGB(j)+2)
        YMaxColor(j)=EdwYMaxQuest(nEdwRGB(j)+2)
        Veta='Drawing color for negative:'
      enddo
      do j=1,2
        nEdw=nEdwRGB(j)
        if(j.eq.1) then
          ic=ConPosCol
        else
          ic=ConNegCol
        endif
        call FeRGBUncompress(ic,ip(1,j),ip(2,j),ip(3,j))
        do i=1,3
          call FeQuestIntEdwOpen(nEdw,ip(i,j),.false.)
          call FeQuestEudOpen(nEdw,0,255,1,0.,0.,0.)
          nEdw=nEdw+1
        enddo
        call FeDrawFrame(XminColor(j),YminColor(j),
     1                   XmaxColor(j)-XminColor(j),
     2                   YmaxColor(j)-YminColor(j),
     3                   2.,White,Gray,Black,.false.)
        call FeFillRectangle(XminColor(j),XmaxColor(j),YminColor(j),
     1                       YmaxColor(j),4,0,0,ic)
      enddo
1400  if(nCrwOcc.gt.0) then
        if(UseOccupancyCutOff) then
          call FeQuestRealEdwOpen(nEdwCutOff,ConOccLim,.false.,.false.)
        else
          if(EdwStateQuest(nEdwCutOff).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdwCutOff,ConOccLim)
          call FeQuestEdwClose(nEdwCutOff)
        endif
      endif
      if(CrwLogicQuest(nCrwExplicit)) then
        call FeQuestRealFromEdw(nEdwExpand,ConDmaxExpl)
        call FeQuestEdwDisable(nEdwExpand)
        call FeQuestLblDisable(nLblExpand)
      else
        call FeQuestRealEdwOpen(nEdwExpand,ConExpDMax,.false.,.false.)
        call FeQuestLblOn(nLblExpand)
      endif
1450  if(CrwLogicQuest(nCrwDrawDepth)) then
        call FeQuestRealEdwOpen(nEdwDrawDepth,Depth,.false.,.false.)
      else
        call FeQuestRealFromEdw(nEdwDrawDepth,Depth)
        call FeQuestEdwDisable(nEdwDrawDepth)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.nCrwOcc.gt.0.and.
     1   (CheckNumber.eq.nCrwOcc.or.CheckNumber.eq.nCrwOcc+1)) then
        UseOccupancyCutOff=CrwLogicQuest(nCrwOcc)
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwAtRad.or.CheckNumber.eq.nCrwExplicit)) then
        go to 1400
      else if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwAllProj.or.
     1        CheckNumber.eq.nCrwDrawDepth)) then
        go to 1450
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.ge.nEdwRGB(1).and.
     2        CheckNumber.le.nEdwRGB(1)+5) then
        i=CheckNumber-nEdwRGB(1)+1
        if(i.gt.3) then
          i=i-3
          j=2
        else
          j=1
        endif
        call FeQuestIntFromEdw(CheckNumber,ip(i,j))
        ic=FeRGBCompress(ip(1,j),ip(2,j),ip(3,j))
        if(j.eq.1) then
          NacetlInt(nCmdConPosCol)=ic
        else
          NacetlInt(nCmdConNegCol)=ic
        endif
        call FeFillRectangle(XminColor(j),XmaxColor(j),YminColor(j),
     1                       YmaxColor(j),4,0,0,ic)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwDMax,LocDMax)
        if(NDimI(KPhase).gt.0.and.UseOccupancyCutOff)
     1    call FeQuestRealFromEdw(nEdwCutOff,ConOccLim)
        if(CrwLogicQuest(nCrwBallAndStick)) then
          NacetlInt(nCmdConDrawStyle)=DrawStyleBallAndStick
        else
          NacetlInt(nCmdConDrawStyle)=DrawStyleWires
        endif
        if(CrwLogicQuest(nCrwConSkipSame)) then
          NacetlInt(nCmdConSkipSame)=1
        else
          NacetlInt(nCmdConSkipSame)=0
        endif
        if(CrwLogicQuest(nCrwExplicit)) then
          NacetlInt(nCmdConDmaxExpl)=1
        else
          NacetlInt(nCmdConDmaxExpl)=0
          call FeQuestRealFromEdw(nEdwExpand,
     1                            NacetlReal(nCmdConExpDMax))
        endif
        if(CrwLogicQuest(nCrwConTintType)) then
          NacetlInt(nCmdConTintType)=1
        else
          NacetlInt(nCmdConTintType)=0
        endif
        if(CrwLogicQuest(nCrwDrawDepth)) then
          call FeQuestRealFromEdw(nEdwDrawDepth,
     1                            NacetlReal(nCmdDrawDepth))
        else
          NacetlReal(nCmdDrawDepth)=0.
        endif
      endif
      call FeQuestRemove(id)
      i=IPlane
      IPlane=-2
      call ConWriteKeys
      IPlane=i
      call DefaultContour
      call NactiContour
      call ConReadKeys
      return
      end
      subroutine NactiM81
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 OldFileName,M81Name
      logical M81Opened,EqIgCase
      save OldFileName
      if(kpdf.eq.4.or.kpdf.eq.5) then
        M81Name=fln(:ifln)//'.d81'
      else
        M81Name=fln(:ifln)//'.m81'
      endif
      inquire(81,opened=M81Opened,name=OldFileName)
      id=idel(CurrentDir)
      i=index(OldFileName,CurrentDir(:id))
      if(i.gt.0) OldFileName=OldFileName(id+1:)
      if(M81Opened) then
        if(.not.EqIgCase(OldFileName,M81Name)) then
          call Del8
          M81Opened=.false.
          M81Cteno=.false.
        endif
      endif
      call CloseIfOpened(81)
      call OpenMaps(81,M81Name,nxny,0)
      if(ErrFlag.ne.0) go to 9999
      read(81,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                        iorien,mapa,nsubs,SatelityByly,
     2                        nonModulated(KPhase)
      nsubs=mod(nsubs,10)
      xymap =iorien(1).le.3.and.iorien(2).le.3
      xyzmap=xymap.and.iorien(3).le.3
      do i=1,NDim(KPhase)
        if(NDim(KPhase).eq.3) then
          cx(i)=smbx(iorien(i))
        else
          cx(i)=smbx6(iorien(i))
        endif
      enddo
      if(allocated(FlagMap)) deallocate(FlagMap)
      allocate(FlagMap(nmap))
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(nxny))
      NActualMap=nxny
      call SetIntArrayTo(FlagMap,nmap,0)
      read(81,rec=nmap+2) Dmax,Dmin
      M81Cteno=.true.
      m8=81
      DiffMapa=Mapa.eq.3.or.Mapa.eq.6.or.Mapa.gt.50.or.
     1   (ChargeDensities.and.mapa.ge.7.and.mapa.le.9).or.
     2   Mapa.eq.-3
      DrawPos=1
      if(DiffMapa) then
        DrawNeg=1
      else
        DrawNeg=0
      endif
      if(DiffMapa) then
        DensityType=2
      else
        DensityType=0
      endif
      call SetContours(0)
      call ConSetCellParCon
      go to 9999
9000  call FeReadError(81)
      ErrFlag=1
9999  return
      end
      subroutine prevod(klic,xin,xout)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xin(3),xout(3),xp(3)
      if(klic.eq.0) then
        if(obecny) then
          do j=1,3
            xp(j)=xin(j)-xob(j)
          enddo
          call multm(trob,xp,xout,3,3,1)
          do i=1,3
            xout(i)=xout(i)+xminr(i)
          enddo
        else
          call multm(tm3,xin,xout,3,3,1)
        endif
      else
        if(obecny) then
          do i=1,3
            xp(i)=xin(i)-xminr(i)
          enddo
          call multm(trobi,xp,xout,3,3,1)
          do j=1,3
            xout(j)=xout(j)+xob(j)
          enddo
        else
          call multm(tm3i,xin,xout,3,3,1)
        endif
      endif
      return
      end
      subroutine ConDrawAtoms(zrez)
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      character*80 Veta
      integer Color1,Color2,DrawAtFlag(NMaxDraw)
      logical InsideXYPlot
      dimension xp(6),xpp(6),xs(3),occ(2000),xd(3,2000),uxi(3,mxw),
     1          uyi(3,mxw),x4dif(3),tdif(3),zrez(4),smp(36),
     2          x40(3),dx4(3),nx4(3),GammaIntInv(9),xoo(3),
     3          xvf(3,NMaxDraw),xvo(3,NMaxDraw),ov(NMaxDraw),
     4          isfa(NMaxDraw),rmp(9),rm6p(36),occp(2000)
      allocate(TypicalDistUse(NAtFormula(KPhase),NAtFormula(KPhase)))
      do i=1,NAtFormula(KPhase)
        do j=i,NAtFormula(KPhase)
          if(TypicalDist(i,j,KPhase).gt.0.) then
            TypicalDistUse(i,j)=TypicalDist(i,j,KPhase)
          else
            TypicalDistUse(i,j)=AtRadius(i,KPhase)+
     1                          AtRadius(j,KPhase)
          endif
          TypicalDistUse(j,i)=TypicalDistUse(i,j)
        enddo
      enddo
      xp (3)=0.
      xpp(3)=0.
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      n=0
      call SetIntArrayTo(DrawAtFlag,DrawAtN,0)
      if(xymap) then
        Veta=fln(:ifln)//'_dratoms_grd.tmp'
        call DeleteFile(Veta)
        ln1=NextLogicNumber()
        call OpenFile(ln1,Veta,'formatted','unknown')
        Veta=fln(:ifln)//'_dratoms_vesta.tmp'
        call DeleteFile(Veta)
        ln2=NextLogicNumber()
        call OpenFile(ln2,Veta,'formatted','unknown')
      else
        ln1=0
        ln2=0
      endif
      do 3000i=1,DrawAtN
        if(DrawAtSkip(i)) go to 3000
        call atsymi(DrawAtName(i),ia,xs,x4dif,tdif,isym,ich,*3000)
        isfa(i)=isf(ia)
        if(DrawAtColor(i).eq.0) then
          Color1=AtColor(isfa(i),KPhase)
        else
          Color1=ColorNumbers(DrawAtColor(i))
        endif
        if(NDimI(KPhase).gt.0) then
          isw=ISwSymm(isym,iswa(ia),KPhase)
          call DistSetComp(isw,NSubs)
          if(isw.eq.iswa(ia)) then
            call CopyMat(rm (1,isym,isw,KPhase),rmp ,3)
            call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
          else
            call multm(zv(1,isw,KPhase),zvi(1,iswa(ia),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,isym,iswa(ia),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rm6p,rmp,NDim(KPhase))
          endif
          ksw=kswa(ia)
          call GetGammaIntInv(rm6p,GammaIntInv)
          do l=1,KModA(2,ia)
            call multm(rmp,ux(1,l,ia),uxi(1,l),3,3,1)
            if(l.ne.KModA(2,ia).or.kfa(2,ia).eq.0) then
              call multm(rmp,uy(1,l,ia),uyi(1,l),3,3,1)
            else
              call CopyVek(uy(1,l,ia),uyi(1,l),3)
            endif
          enddo
          if(xymap) then
            do j=3,NDim(KPhase)
              k=iorien(j)-3
              if(k.gt.0) xpp(k)=zrez(j-2)
            enddo
            if(tmapy) then
              call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
              call AddVek(tdif,xpp,xpp,NDimI(KPhase))
            else
              call AddVek(x4dif,xpp,xpp,NDimI(KPhase))
            endif
            call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
            do j=1,NDimI(KPhase)
              nx4(j)=1
              dx4(j)=0.
              if(.not.tmapy) x40(j)=0.
            enddo
            if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
              call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1          ay(1,ia),KModA(1,ia),KFA(1,ia),GammaIntInv)
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1           uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2           GammaIntInv)
                 if(occp(1).le.0.) occ(1)=0.
              endif
            else
              call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                           ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                           ax(KModA(1,ia),ia),a0(ia)*.5,
     3                           GammaIntInv,TypeModFun(ia))
            endif
            if(TypeModFun(ia).le.1) then
              call MakePosMod(xpp,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                        KFA(2,ia),GammaIntInv)
            else
              call MakePosModPol(xpp,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                           ax(KModA(1,ia),ia),a0(ia)*.5,
     2                           GammaIntInv,TypeModFun(ia))
            endif
            if((occ(1).lt.ConOccLim.and.UseOccupancyCutOff).or.
     1          occ(1).le.0.01) go to 3000
            call AddVek(xs,xpp,xs,3)
          endif
        else
          occ(1)=1.
        endif
        if(xymap) then
          call prevod(0,xs,xp)
          j=index(DrawAtName(i),'#')-1
          if(j.le.0) j=idel(DrawAtName(i))
          Veta=DrawAtName(i)(:j)
          if(Obecny) then
            do j=1,3
              xo(j)=xp(j)+ShiftPlane(j)-ScopePlane(j)*.5
            enddo
            write(ln1,100) Veta(:6),xo
            write(ln2,101) ia,nint(AtNum(isfa(i),KPhase)),
     1                     (xo(j)+ScopePlane(j)*.5,j=1,3)
          else
            write(ln1,100) Veta(:6),
     1                     ((xs(iorien(j))-(xmax(j)-xmin(j))*.5)*
     1                      CellParCon(j),j=1,3)
            write(ln2,101) ia,nint(AtNum(isfa(i),KPhase)),(xs(j),j=1,3)
          endif
          call FeXf2X(xp,xo)
          if(InsideXYPlot(xo(1),xo(2))) then
            if(.not.DrawAtSkip(i)) then
              if(ConDrawDepth.gt.0..and..not.smapy) then
                xpp(1)=xp(1)
                xpp(2)=xp(2)
                xpp(3)=zrez(1)
                call FeXf2X(xpp,xoo)
                if(abs(xo(3)-xoo(3)).gt.ConDrawDepth) go to 3000
              endif
              DrawAtFlag(i)=1
              call CopyVek(xs,xvf(1,i),3)
              call CopyVek(xo,xvo(1,i),3)
              ov(i)=occ(1)
              go to 3000
            endif
          endif
        else
          do j=3,NDim(KPhase)
            k=iorien(j)
            xpp(k)=zrez(j-2)
          enddo
          ko1=iorien(1)
          ko2=iorien(2)
          if(ko1.gt.3) then
            kot=ko1
          else
            kot=ko2
          endif
          x4min= 99999.
          x4max=-99999.
          do j=0,1
            if(j.eq.0) then
              xpp(ko1)=xmin(1)
            else
              xpp(ko1)=xmax(1)
            endif
            do k=0,1
              if(k.eq.0) then
                xpp(ko2)=xmin(2)
              else
                xpp(ko2)=xmax(2)
              endif
              call multm(WPr,xpp,xp,NDim(KPhase),NDim(KPhase),1)
              x4min=min(x4min,xp(kot))
              x4max=max(x4max,xp(kot))
            enddo
          enddo
          dx4m=(x4max-x4min)*.001
          x4m=x4min-(x4max-x4min)*.5
          do j=3,NDim(KPhase)
            k=iorien(j)-3
            if(k.gt.0) xpp(k)=zrez(j-2)
          enddo
          xpp(kot-3)=x4m
          call AddVek(xpp,x4dif,xpp,NDimI(KPhase))
          if(kmol(ia).ne.0) then
            call qbyx(xs,xp,isw)
            do j=1,NDimI(KPhase)
              xpp(j)=xpp(j)-xp(j)+qcnt(j,ia)
            enddo
          endif
          call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
          call SetRealArrayTo(dx4,NDimI(KPhase),0.)
          call SetRealArrayTo(x40,NDimI(KPhase),0.)
          call SetRealArrayTo(dx4,NDimI(KPhase),0.)
          call SetIntArrayTo(nx4,NDimI(KPhase),1)
          dx4(kot-3)=dx4m
          nx4(kot-3)=2000
          nx4m=2000
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1        ay(1,ia),KModA(1,ia),KFA(1,ia),GammaIntInv)
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
              call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1         uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2         GammaIntInv)
               do j=1,nx4m
                 if(occp(j).le.0.) occ(j)=0.
               enddo
            endif
          else
            call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv,TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakePosMod(xd,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                      KFA(2,ia),GammaIntInv)
          else
            call MakePosModPol(xd,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                         ax(KModA(1,ia),ia),a0(ia)*.5,GammaIntInv,
     2                         TypeModFun(ia))
          endif
          Kolik=0
          do j=1,nx4m
            call AddVek(xs,xd(1,j),xp,3)
            call qbyx(xd(1,j),xp(4),isw)
            xp(kot)=x4m+xp(kot)
            call multm(WPrI,xp,xpp,NDim(KPhase),NDim(KPhase),1)
            call SetRealArrayTo(xp,NDim(KPhase),0.)
            xp(1)=xpp(ko1)
            xp(2)=xpp(ko2)
            if(xp(1).lt.xmin(1).or.xp(1).gt.xmax(1).or.
     1         xp(2).lt.xmin(2).or.xp(2).gt.xmax(2)) go to 2300
            if((occ(j).lt.ConOccLim.and.UseOccupancyCutOff).or.
     1          occ(j).le.0.01) go to 2300
            call FeXf2X(xp,xo)
            xm=xo(1)
            ym=xo(2)
            kolik=kolik+1
            xpole(kolik)=xm
            ypole(kolik)=ym
            if(kolik.ge.mxc) then
              call FePolyLineT(kolik,xpole,ypole,Color1)
              kolik=1
              xpole(kolik)=xm
              ypole(kolik)=ym
            endif
            go to 2350
2300        if(kolik.ge.1) then
              if(kolik.ge.2) call FePolyLineT(kolik,xpole,ypole,Color1)
              kolik=0
            endif
2350        x4m=x4m+dx4m
          enddo
2500      if(kolik.ge.2) call FePolyLineT(kolik,xpole,ypole,Color1)
        endif
3000  continue
      if(xymap) then
        do i=1,DrawAtN
          if(DrawAtFlag(i).le.0) cycle
          if(DrawAtColor(i).eq.0) then
            Color1=AtColor(isfa(i),KPhase)
          else
            Color1=ColorNumbers(DrawAtColor(i))
          endif
          do j=1,i-1
            if(DrawAtFlag(j).le.0) cycle
            if(ConSkipSame.gt.0.and.isfa(i).eq.isfa(j)) cycle
            do k=1,3
              xp(k)=xvf(k,i)-xvf(k,j)
            enddo
            call Multm(MetTens(1,1,KPhase),xp,xpp,3,3,1)
            d=sqrt(scalmul(xp,xpp))
            if(ConDmaxExpl.gt.0) then
              if(d.gt.DrawAtBondLim(i).or.d.gt.DrawAtBondLim(j)) cycle
            else
              if(d.gt.TypicalDistUse(isfa(i),isfa(j))*
     1                (1.+.01*ConExpDmax)) cycle
            endif
            if(ConDrawStyle.eq.DrawStyleBallAndStick) then
              xu(1)=xvo(1,i)
              yu(1)=xvo(2,i)
              xu(2)=xvo(1,j)
              yu(2)=xvo(2,j)
              call FePolyLineT(2,xu,yu,White)
              call FeLineType(NormalLine)
            else
              xu(1)=xvo(1,i)
              yu(1)=xvo(2,i)
              xu(2)=.5*(xvo(1,i)+xvo(1,j))
              yu(2)=.5*(xvo(2,i)+xvo(2,j))
              call FePolyLineT(2,xu,yu,Color1)
              call FeLineType(NormalLine)
              xu(1)=xvo(1,j)
              yu(1)=xvo(2,j)
              if(DrawAtColor(j).eq.0) then
                Color2=AtColor(isfa(j),KPhase)
              else
                Color2=ColorNumbers(DrawAtColor(j))
              endif
              call FePolyLineT(2,xu,yu,Color2)
              call FeLineType(NormalLine)
            endif
          enddo
        enddo
        if(ConDrawStyle.eq.DrawStyleBallAndStick) then
          do i=1,DrawAtN
            if(DrawAtFlag(i).gt.0) then
              if(.not.UseOccupancyCutOff)
     1          Rad=min(.2*X2XoRatio,20.)*ov(i)
              if(DrawAtColor(i).eq.0) then
                Color1=AtColor(isfa(i),KPhase)
              else
                Color1=ColorNumbers(DrawAtColor(i))
              endif
              call FeCircle(xvo(1,i),xvo(2,i),Rad,Color1)
            endif
          enddo
        endif
      endif
      call ResetIgnoreW
      call ResetIgnoreE
9999  if(xymap) then
        call CloseIfOpened(ln1)
        call CloseIfOpened(ln2)
      endif
      if(allocated(TypicalDistUse)) deallocate(TypicalDistUse)
      return
100   format(a6,3f10.5,' ATOM')
101   format(2i3,3f10.5)
      end
      subroutine ConDefDensity(AtPDF,ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xdef(3),tp(3)
      integer DensityTypeOld
      character*256 EdwStringQuest
      character*80 Veta
      character*25 :: MenDen(7) =
     1                (/'total density            ',
     2                  'valence density          ',
     3                  'deformation density      ',
     4                  'total density Laplacian  ',
     5                  'valence density Laplacian',
     6                  'inversed gradient vector ',
     7                  'gradient vector          '/)
      character*(*) AtPDF
      integer SbwLnQuest,SbwItemSelQuest,SbwItemPointerQuest,
     1        RolMenuSelectedQuest
      logical lpom,EqWild,CrwLogicQuest,BackToAdvanced
      external ConDefDensityCheck,EM40SelAtomsCheck,FeVoid
      data xdef/0.,1.,.1/,DensityTypeOld/2/
      if(kpdf.eq.2) then
        Veta='Define atom and basic options for p.d.f.'
      else if(kpdf.eq.6) then
        Veta='Define atoms and basic paramaters for charge density maps'
      else
        Veta='Define atoms and basic paramaters for j.p.d.f.'
      endif
      if(mcmax.eq.0) mcmax=1000
      id=NextQuestId()
      call FeQuestTitleMake(id,Veta)
      xqd=WizardLength
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=9
      ild=9
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      nSbwSelect=SbwLastMade
      il=il+2
      if(kpdf.eq.2) then
        nButtAll=0
        nButtRefresh=0
        nEdwInclude=0
        nButtAdvanced=0
        tpom=5.
        Veta='Selected atom:'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        tpom=tpom+FeTxLength(Veta)+5.
        i=NAtIndFrAll(KPhase)
        call FeQuestLblMake(id,tpom,il,Atom(i),'L','N')
        nLblSelectedAtom=LblLastMade
        tpom=tpom+60.
        Veta='%Symmetry code:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwSymmCode=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
      else
        dpom=100.
        xpom=60.
        Veta='%Select all'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='%Refresh'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='Select a%dvanced'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAdvanced=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        tpom=xpom+dpom+20.
        Veta='%Include:'
        xpom=tpom+FeTxLength(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwInclude=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
        il=il+1
        call FeQuestLinkaMake(id,il)
        nLblSelectedAtom=0
        nEdwSymmCode=0
      endif
      il=il+1
      ilp=il
      if(kpdf.eq.6) then
        DensityType=DensityTypeOld
        tpom=5.
        Veta='%Calculate:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=150.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolDensityType=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolDensityType,MenDen,7,DensityType+1)
        nCrwDeform=0
        nCrwOldNorm=0
        xpom=5.
        tpom=xpom+CrwgXd+10.
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        if(kpdf.eq.2) then
          Cislo='p.d.f.'
        else
          Cislo='j.p.d.f.'
        endif
        Veta='%Calculate deformation '//Cislo(:idel(Cislo))//' map'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwDeform=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,DensityType.eq.2)
        il=il+1
        Veta='Use %old normalization (JANA98)'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwOldNorm=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,OldNorm)
        nRolDensityType=0
      endif
      il=il+1
      Veta='Include %error map'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwErr=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ErrMapActive)
      tpom=tpom+FeTxLengthUnder(Veta)+5.
      Veta='=>'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblSipka=LblLastMade
      tpom=tpom+FeTxLength(Veta)+5.
      Veta='%Number of iteration steps:'
      xpom=tpom+FeTxLength(Veta)+5.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNSteps=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mcmax,.false.)
      il=il+1
      Veta='%Accuracy limit in %:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,errlev,.false.,.false.)
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
      enddo
      call CloseIfOpened(ln)
      if(NDimI(KPhase).gt.0) then
        il=ilp
        Veta='t(%min):'
        tpom=xqd*.5+80.
        xpom=tpom+FeTxLengthUnder(Veta)+15.
        dpom=70.
        do i=1,3
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            Veta='t(ma%x):'
            nEdwTmin=EdwLastMade
          else if(i.eq.2) then
            Veta='t(del%ta):'
          else if(i.eq.3) then
          endif
          call FeQuestRealEdwOpen(EdwLastMade,xdef(i),.false.,.false.)
          il=il+1
        enddo
        NSubs=1
      else
        NSubs=1
        nEdwTmin=0
      endif
      call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
      BackToAdvanced=.false.
      go to 1255
1250  call CloseIfOpened(SbwLnQuest(nSbwSelect))
1255  if(kpdf.eq.2) then
        call FeQuestSbwMenuOpen(nSbwSelect,fln(:ifln)//'_atoms.tmp')
      else
        k=0
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          k=k+1
          if(AtBrat(i)) then
            j=1
          else
            j=0
          endif
          call FeQuestSetSbwItemSel(k,nSbwSelect,j)
        enddo
        call FeQuestSbwSelectOpen(nSbwSelect,fln(:ifln)//'_atoms.tmp')
      endif
1500  if(CrwLogicQuest(nCrwErr)) then
        call FeQuestIntEdwOpen(nEdwNSteps,mcmax,.false.)
        call FeQuestRealEdwOpen(nEdwLimit,errlev,.false.,.false.)
      else
        call FeQuestIntFromEdw(nEdwNSteps,mcmax)
        call FeQuestRealFromEdw(nEdwLimit,errlev)
        call FeQuestEdwDisable(nEdwNSteps)
        call FeQuestEdwDisable(nEdwLimit)
      endif
      if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      if(kpdf.eq.2) then
        call FeQuestEventWithCheck(id,ich,ConDefDensityCheck,FeVoid)
      else
        call FeQuestEvent(id,ich)
      endif
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh)) then
        if(CheckNumber.eq.nButtAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        call SetLogicalArrayTo(AtBrat(NAtIndFrAll(KPhase)),
     1                         NAtIndLenAll(KPhase),lpom)
        go to 1250
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          AtBrat(i)=EqWild(Atom(i),Veta,.false.)
        enddo
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        WizardMode=.false.
        idp=NextQuestId()
        xqdp=300.
        il=6
        jk=2
        nRolMenuMol=0
        nButtInclMol=0
        nButtExclMol=0
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,2
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,
     1                                AtType(1,KPhase),
     2                                NAtFormula(KPhase),0)
            else
              nRolMenuMol=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,MolMenu,NMolMenu,
     1                                0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              else
                nButtInclMol=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              else
                nButtExclMol=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,EM40SelAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(EqWild(Atom(i),Veta,.false.)) AtBrat(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
          isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(isf(i).eq.isfp) AtBrat(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtType(1,KPhase),
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then

          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        WizardMode=.true.
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1250
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwErr) then
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSymmCode)
     1  then
        j=SbwItemPointerQuest(nSbwSelect)+NAtIndFrAll(KPhase)-1
        AtPDF=Atom(j)
        Veta=EdwStringQuest(nEdwSymmCode)
        if(Veta.ne.' ') AtPDF=AtPDF(:idel(AtPDF))//'#'//
     1                        Veta(:idel(Veta))
        call atsymi(AtPDF,iapdf(1),xpdf(1,1),xp,tp,ispdf(1),k,*1700)
        go to 1500
1700    EventType=EventEdw
        EventNumber=nEdwSymmCode
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        ErrMapActive=CrwLogicQuest(nCrwErr)
        if(ErrMapActive) then
          call FeQuestIntFromEdw(nEdwNSteps,mcmax)
          call FeQuestRealFromEdw(nEdwLimit,errlev)
        endif
        if(nCrwDeform.gt.0) then
          if(CrwLogicQuest(nCrwDeform)) then
            DensityType=2
          else
            DensityType=1
          endif
        else
          DensityType=RolMenuSelectedQuest(nRolDensityType)-1
          DensityTypeOld=DensityType
        endif
        j=SbwItemPointerQuest(nSbwSelect)+NAtIndFrAll(KPhase)-1
        NAtBrat=0
        if(kpdf.eq.2) then
          call SetLogicalArrayTo(AtBrat(NAtIndFrAll(KPhase)),
     1                           NAtIndLenAll(KPhase),.false.)
          AtBrat(j)=.true.
          AtPDF=Atom(j)
          itfi=max(itf(j)-1,1)
          do m=1,5
            AtBratAnharm(m,j)=m.le.itfi
          enddo
          Veta=EdwStringQuest(nEdwSymmCode)
          if(Veta.ne.' ') AtPDF=AtPDF(:idel(AtPDF))//'#'//
     1                          Veta(:idel(Veta))
        else
          k=0
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            k=k+1
            AtBrat(i)=SbwItemSelQuest(k,nSbwSelect).eq.1
            if(AtBrat(i)) then
              j=0
              NAtBrat=NAtBrat+1
              itfi=max(itf(i)-1,1)
              do m=1,5
                AtBratAnharm(m,i)=m.le.itfi
              enddo
            endif
          enddo
          if(j.ne.0) AtBrat(j)=.true.
        endif
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwTmin
          do i=1,3
            call FeQuestRealFromEdw(nEdw,xdef(i))
            nEdw=nEdw+1
          enddo
          tpdfp=xdef(1)
          tpdfk=xdef(2)
          tpdfd=xdef(3)
          ntpdf=nint((tpdfk-tpdfp)/tpdfd)+1
        else
          tpdfp=0.
          tpdfk=0.
          ntpdf=1
        endif
      else
        go to 9999
      endif
      if(kpdf.eq.2) then
        call atsymi(AtPDF,iapdf(1),xpdf(1,1),xp,tp,ispdf(1),k,*9000)
        ipor(1)=1
        npdf=1
        dpdf(1)=0.
        xp(1)=0.
        call specpos(xpdf(1,1),xp,0,1,.005,l)
        fpdf(1)=ai(iapdf(1))*float(l)
      endif
      go to 9999
9000  ich=-1
9999  return
      end
      subroutine ConDefDensityCheck
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer SbwItemPointerQuest
      i=SbwItemPointerQuest(nSbwSelect)+NAtIndFrAll(KPhase)-1
      call FeQuestLblChange(nLblSelectedAtom,Atom(i))
      return
      end
      subroutine ConHitMC
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      sp=0.
2000  k=0
      do n=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(.not.SelPDF(n)) cycle
        do j=1,DelkaKiAtomu(n)
          if(KiA(j,n).eq.1) then
            k=k+1
            rnda(k)=gasdev()
            p=rnds(k)+fsum(k)
            call KdoCo(PrvniKiAtomu(n)+j-1,at,pn,-1,p,sp)
          endif
        enddo
        if(.not.ChargeDensities) then
          if(beta(1,n).le.0..or.
     1       beta(1,n)*beta(2,n)-beta(4,n)**2.le.0..or.
     2       beta(1,n)*beta(2,n)*beta(3,n)-beta(3,n)*beta(4,n)**2-
     3       beta(1,n)*beta(6,n)**2-beta(2,n)*beta(5,n)**2+
     4       2.*beta(4,n)*beta(5,n)*beta(6,n).le.0.) go to 2000
        endif
      enddo
      do i=1,neq
        call ApeqC(i)
      enddo
      return
      end
      function gasdev()
      data iset/0/
      if(iset.eq.0) then
1       v1=2.*ran1(0)-1.
        v2=2.*ran1(0)-1.
        r=v1**2+v2**2
        if(r.ge.1.) go to 1
        fac=sqrt(-2.*log(r)/r)
        gset=v1*fac
        gasdev=v2*fac
        iset=1
      else
        gasdev=gset
        iset=0
      endif
      return
      end
      function fsum(k)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      kk=(k*(k-1))/2
      fsum=0.
      do i=1,k
        fsum=fsum+rnda(i)*covc(kk+i)
      enddo
      return
      end
      subroutine ApeqC(kterou)
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      if(npa(kterou).le.0) return
      pom=pab(kterou)
      do k=1,npa(kterou)
        call KdoCo(pnp(k,kterou),at,pn,1,p,sp)
        pom=pom+p*pko(k,kterou)
      enddo
      sp=0.
      call KdoCo(lnp(kterou),at,pn,-1,pom,sp)
      return
      end
      subroutine ConReadM85
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension covl(:),covlp(:)
      character*80 t80
      character*12 at,pn
      allocatable covl,covlp
      call OpenFile(85,fln(1:ifln)//'.m85','unformatted','old')
      if(ErrFlag.ne.0) go to 9000
      read(85,err=8000,end=8000) neq
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      mxe=neq
      mxep=5
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      do i=1,neq
        read(85,err=8000,end=8000) lnp(i),pab(i),npa(i)
        if(npa(i).gt.mxep) then
          call SetRealArrayTo(pnp(1,i),mxep,0.)
          call SetRealArrayTo(pko(1,i),mxep,0.)
          call ReallocEquations(mxe,npa(i))
        endif
        do j=1,npa(i)
          read(85,err=8000,end=8000) pnp(j,i),pko(j,i)
        enddo
      enddo
      ipocm=0
      do i=1,NDatBlock
        do j=1,MxScAll
          if(KiS(j,i).ne.0) then
            ipocm=ipocm+1
            read(85,err=8000,end=8000) pom
          endif
        enddo
      enddo
      do i=1,MxParPwd
        if(KiPwd(i).ne.0) then
          ipocm=ipocm+1
          read(85,err=8000,end=8000) pom
        endif
      enddo
      if(allocated(KIEd)) then
        do KDatB=1,NDatBlock
          do i=1,MxEDRef
            do j=1,MxEdZone
              if(KiED(i,j,KDatB).ne.0) then
                ipocm=ipocm+1
                read(85,err=8000,end=8000) pom
              endif
            enddo
          enddo
        enddo
      endif
      im=ipocm
      ic=0
      jc=0
      ijc=0
      nalloc=1000
      allocate(covl(nalloc))
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        do 4000id=1,DelkaKiAtomu(i)
          if(KiA(id,i).le.0) cycle
          im=im+1
          if(im.gt.nalloc) then
            allocate(covlp(nalloc))
            call CopyVek(covl,covlp,nalloc)
            nallocp=nalloc
            nalloc=nalloc+1000
            deallocate(covl)
            allocate(covl(nalloc))
            call CopyVeK(covlp,covl,nallocp)
            deallocate(covlp)
          endif
          read(85,err=8000,end=8000)(covl(m),m=1,im)
          if(SelPDF(i)) then
            ic=ic+1
            if(ic.gt.mxran) then
              write(t80,'(i6)') mxran
              call zhusti(t80)
              t80='the maximum number '//t80(:idel(t80))//
     1            ' of random variables exceeded'
              call FeChybne(-1.,-1.,t80,'error map resp. curve will'//
     1                      ' not be calculated.',SeriousError)
              go to 9000
            endif
            call KdoCo(id+PrvniKiAtomu(i)-1,at,pn,1,rnds(ic),sp)
            il=ipocm
            jc=0
            do j=1,i
              do jl=1,DelkaKiAtomu(j)
                if(KiA(jl,j).eq.1) then
                  il=il+1
                  if(SelPDF(j).and.jc.lt.ic) then
                    jc=jc+1
                    ijc=ijc+1
                    covc(ijc)=covl(il)
                  endif
                endif
              enddo
            enddo
            ii=ijc-jc
            if(ijc.eq.1) then
              v11=sqrt(covc(ijc))
              covc(ijc)=v11
              go to 4000
            endif
            covc(ii+1)=covc(ii+1)/v11
            do j=2,jc-1
              ij=ii+j
              jj=(j*(j-1))/2
              do l=1,j-1
                covc(ij)=covc(ij)-covc(ii+l)*covc(jj+l)
              enddo
              covc(ij)=covc(ij)/covc(jj+j)
            enddo
            do l=1,jc-1
              covc(ijc)=covc(ijc)-covc(ii+l)**2
            enddo
            covc(ijc)=sqrt(covc(ijc))
          endif
4000    continue
      enddo
      go to 9999
8000  call FeReadError(85)
9000  mcmax=0
      ErrMapActive=.false.
      ErrFlag=0
9999  call CloseIfOpened(85)
      if(allocated(covl)) deallocate(covl)
      return
      end
      subroutine obnova
      use Contour_mod
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      ic=0
      sp=0.
      do 5000i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        jj=PrvniKiAtomu(i)
        if(.not.SelPDF(i)) go to 5000
        do id=1,DelkaKiAtomu(i)
          if(kia(id,i).eq.1) then
            ic=ic+1
            call KdoCo(jj,at,pn,-1,rnds(ic),sp)
          endif
          jj=jj+1
        enddo
5000  continue
      do i=1,neq
        call ApeqC(i)
      enddo
      return
      end
      subroutine ConDefAtForDenCalc(klic,ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xpp(3),ddx(3),gddx(3)
      character*80 Veta
      character*30 Titulek
      integer SbwLnQuest,SbwItemSelQuest,ipori(:),SbwItemFromQuest,
     1        UseTabsIn
      logical atbrato(:),bratpdf(:),zmena,lpom,FeYesNoHeader
      data klico/-1/
      allocatable ipori,atbrato,bratpdf
      allocate(atbrato(NAtIndLenAll(KPhase)))
      if(ncomp(1).le.1) nsubs=1
      if(klic.eq.0) then
        Titulek='j.p.d.f.'
      else if(klic.eq.1) then
        Titulek='charge density'
      else if(klic.eq.2) then
        Titulek='*.ent'
      else if(klic.ge.3) then
        Titulek='topological analysis'
      endif
      if(Obecny) then
        ir=2
      else
        ir=1
      endif
      go to 1150
      entry ConDefAtForDragon(klic,ich)
      Veta='Dragon'
      ir=1
1150  UseTabsIn=UseTabs
      Zmena=.false.
      do i=1,3
        xpp(i)=(xmaxa(i,ir)+xmina(i,ir))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddx(iorien(1))=(xmaxa(1,ir)-xpp(1))*float(i1)
        do i2=-1,1,2
          ddx(iorien(2))=(xmaxa(2,ir)-xpp(2))*float(i2)
          do i3=-1,1,2
            ddx(iorien(3))=(xmaxa(3,ir)-xpp(3))*float(i3)
            if(obecny) then
              dd=sqrt(ddx(1)**2+ddx(2)**2+ddx(3)**2)
            else
              call multm(MetTens(1,nsubs,KPhase),ddx,gddx,3,3,1)
              dd=sqrt(scalmul(ddx,gddx))
            endif
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      dmez=dmez+CutOffDist
      if(ir.eq.2) then
        call prevod(1,xpp,xp)
      else
        call CopyVek(xpp,xp,3)
      endif
      if(NDim(KPhase).le.3) ntpdf=1
      if(.not.WizardMode) then
        if(kpdf.ge.4) then
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            atbrat(i)=.true.
          enddo
          Zmena=.true.
        else
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            atbrato(i)=atbrat(i)
          enddo
          Zmena=Zmena.or.klic.ne.klico
1520      call SelAtoms('Select atoms for distance check',
     1      Atom(NAtIndFrAll(KPhase)),AtBrat(NAtIndFrAll(KPhase)),
     2      isf(NAtIndFrAll(KPhase)),NAtIndLenAll(KPhase),ich)
          if(ich.ne.0) go to 9999
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(AtBrat(i)) go to 1540
          enddo
          TextInfo(1)='No atoms specified to calculate the map'
          NInfo=1
          if(FeYesNoHeader(-1.,-1.,'Do you really want to break the '//
     1                     'action?',0)) then
            ich=1
            go to 6000
          else
            go to 1520
          endif
1540      if(.not.zmena) then
            do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
              if(AtBrat(i).neqv.AtBrato(i)) then
                Zmena=.true.
                go to 2000
              endif
            enddo
          endif
        endif
        if(klic.eq.4) go to 9999
      endif
2000  do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(AtBrat(i)) then
          isw=iswa(i)
          exit
        endif
      enddo
      call OkoliC(xp,dmez,ir,isw,.true.)
      allocate(ipori(Npdf),BratPdf(NPdf))
      call SetLogicalArrayTo(BratPdf,NPdf,.true.)
      xp(1)=0.
      do j=1,npdf
        ia=iapdf(j)
        call specpos(xpdf(1,j),xp,0,1,.005,l)
        fpdf(j)=ai(ia)*float(l)
        if(Klic.le.0) then
          do k=1,5
            BratAnharmPDF(k,j)=AtBratAnharm(k,ia)
          enddo
        endif
        if(zmena) bratpdf(j)=.true.
      enddo
      if(kpdf.ge.4) go to 6000
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_datoms.tmp','formatted','unknown')
      do j=1,npdf
        k=ipor(j)
        ipori(k)=j
        write(Cislo,'(f6.3)') dpdf(k)
        Veta=Tabulator//atom(iapdf(k))(:idel(atom(iapdf(k))))//
     1       Tabulator//'|'//Tabulator//Cislo//
     2       Tabulator//'|'//
     3       Tabulator//scpdf(k)(:idel(scpdf(k)))
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      id=NextQuestId()
      Veta='Select individual atoms for '//Titulek
      if(WizardMode) then
        xqd=WizardLength
        call FeQuestTitleMake(id,Veta)
      else
        il=15
        xqd=400.
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      endif
      UseTabs=NextTabs()
      pom=0.
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('XXXXXXXX |')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('MM')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('XX.XXX |')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('MM')
      if(WizardMode) then
        dpom=400.
        xshift=(xqd-dpom)*.5
      else
        dpom=xqd-10.-SbwPruhXd
        xshift=5.
      endif
      il=1
      Veta='Atom'
      xpom=7.+xshift
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      Veta='Distance'
      xpom=xpom+65.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      Veta='Symmetry code'
      xpom=xpom+55.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=14
      xpom=xshift
      call FeQuestSbwMake(id,xpom,il,dpom,13,1,CutTextNone,SbwVertical)
      nSbw=SbwLastMade
      il=il+1
      dpom=60.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Refresh')
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Select all')
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
3100  do i=1,npdf
        k=ipor(i)
        if(BratPdf(k)) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbw,j)
      enddo
      ItemFromOld=SbwItemFromQuest(nSbw)
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_datoms.tmp')
      call FeQuestSbwShow(nSbw,ItemFromOld)
3200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        lpom=CheckNumber.ne.nButtRefresh
        call SetLogicalArrayTo(BratPdf,NPdf,lpom)
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 3100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3200
      endif
      if(ich.eq.0) then
        l=0
        do i=1,NPdf
          j=SbwItemSelQuest(ipori(i),nSbw)
          BratPdf(i)=j.eq.1
          if(BratPdf(i)) then
            l=i
          else
            fpdf(i)=0.
          endif
        enddo
        npdf=l
      endif
      if(.not.WizardMode) call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_datoms.tmp')
6000  klico=klic
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(allocated(ipori)) deallocate(ipori,BratPdf)
      if(allocated(atbrato)) deallocate(atbrato)
      return
      end
      subroutine DefContour(ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 ContourOld,EdwStringQuest
      character*80 Veta
      character*9  frm
      integer EdwStateQuest,Exponent10
      logical CrwLogicQuest
      data ContourOld/' '/,frm/'(f15. 6)'/
      xqd=400.
      il=9
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Contour parameters',1,
     1                   LightGray,0,0)
      k=1
      do i=1,2
        if(i.eq.1) then
          pom=Dmin
        else
          pom=Dmax
        endif
        pom=pom+ContourRefLevel
        if(abs(pom).gt.0.) then
          j=max(6-Exponent10(pom),0)
          j=min(j,6)
        else
          j=6
        endif
        write(frm(6:7),'(i2)') j
        write(Veta(k:),frm) pom
        k=k+15
      enddo
      call ZdrcniCisla(Veta,2)
      Veta='Extremals of the map(s) : '//Veta(:idel(Veta))
      il=1
      call FeQuestLblMake(id,5.,il,Veta,'L','N')
      xpom=50.
      il=il+1
      Veta='%Uniform contours'
      do i=1,3
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) then
          nCrwUniform=CrwLastMade
          Veta='%Explicit contours'
        else if(i.eq.2) then
          nCrwExplicit=CrwLastMade
          Veta='AI%M contours'
        else if(i.eq.3) then
          nCrwAIM=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,ContourType.eq.i-1)
        xpom=xpom+140.
      enddo
      il=il+1
      tpom=xqd*.5-20.
      Veta='%Positive contours'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=100.
      do i=1,4
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwPosCon=EdwLastMade
          Veta='%Negative contours'
        else if(i.eq.2) then
          nEdwNegCon=EdwLastMade
          Veta='Po%sitive cutoff'
        else if(i.eq.3) then
          nEdwPosCut=EdwLastMade
          Veta='Ne%gative cutoff'
        else if(i.eq.4) then
          nEdwNegCut=EdwLastMade
        endif
      enddo
      tpom=5.
      Veta='Draw pos%itive'
      il=il-4
      xpom=tpom+FeTxLengthUnder(Veta(:idel(Veta))//'XX')+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        if(i.eq.1) then
          nCrwDrawPos=CrwLastMade
          Veta='Draw neg%ative'
          j=DrawPos
        else if(i.eq.2) then
          nCrwDrawNeg=CrwLastMade
          j=DrawNeg
        endif
        call FeQuestCrwOpen(CrwLastMade,j.gt.0)
      enddo
      Veta='Dra%w contours:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.
      il=il+3
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawInd=EdwLastMade
      il=il+1
      Veta='Start AIM contours with decimal order:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawAIM=EdwLastMade
1300  if(EdwStateQuest(nEdwDrawInd).eq.EdwOpened) then
        ContourOld=EdwStringQuest(nEdwDrawInd)
      else if(EdwStateQuest(nEdwDrawAIM).eq.EdwOpened) then
        call FeQuestIntFromEdw(nEdwDrawAIM,ContourAIMOrder)
      endif
      if(ContourType.eq.0) then
        call FeQuestEdwDisable(nEdwDrawInd)
        call FeQuestEdwDisable(nEdwDrawAIM)
        call FeQuestCrwOpen(nCrwDrawPos,DrawPos.gt.0)
        call FeQuestCrwOpen(nCrwDrawNeg,DrawNeg.gt.0)
        if(DrawPos.gt.0) then
          if(EdwStateQuest(nEdwPosCon).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwPosCon,fmzp,.false.,.false.)
            call FeQuestRealEdwOpen(nEdwPosCut,cutpos+ContourRefLevel,
     1                              .false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwPosCon)
          call FeQuestEdwDisable(nEdwPosCut)
        endif
        if(DrawNeg.gt.0) then
          if(EdwStateQuest(nEdwNegCon).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwNegCon,fmzn,.false.,.false.)
            call FeQuestRealEdwOpen(nEdwNegCut,cutneg+ContourRefLevel,
     1                              .false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwNegCon)
          call FeQuestEdwDisable(nEdwNegCut)
        endif
      else if(ContourType.eq.ContourTypeUser.or.
     1        ContourType.eq.ContourTypeAIM) then
        call FeQuestEdwDisable(nEdwPosCon)
        call FeQuestEdwDisable(nEdwPosCut)
        call FeQuestEdwDisable(nEdwNegCon)
        call FeQuestEdwDisable(nEdwNegCut)
        if(ContourType.eq.ContourTypeUser) then
          call FeQuestEdwDisable(nEdwDrawAIM)
          call FeQuestStringEdwOpen(nEdwDrawInd,ContourOld)
          call FeQuestCrwDisable(nCrwDrawPos)
          call FeQuestCrwDisable(nCrwDrawNeg)
        else if(ContourType.eq.ContourTypeAIM) then
          call FeQuestEdwDisable(nEdwDrawInd)
          call FeQuestIntEdwOpen(nEdwDrawAIM,ContourAIMOrder,.false.)
          call FeQuestCrwOpen(nCrwDrawPos,DrawPos.gt.0)
          call FeQuestCrwOpen(nCrwDrawNeg,DrawNeg.gt.0)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        if(ContourType.eq.ContourTypeUser) then
          if(allocated(ContourArray)) deallocate(ContourArray)
          ContourOld=EdwStringQuest(nEdwDrawInd)
          if(ContourOld.eq.' ') go to 1710
          do i=1,2
            ContourNumber=0
            k=0
1600        call kus(ContourOld,k,Cislo)
            ContourNumber=ContourNumber+1
            pom=fract(Cislo,ich)
            if(ich.ne.0) go to 1700
            if(i.eq.2) ContourArray(ContourNumber)=pom
            if(k.ge.len(ContourOld)) then
              go to 1610
            else
              go to 1600
            endif
1610        if(i.eq.1) allocate(ContourArray(ContourNumber))
          enddo
          QuestCheck(id)=0
          go to 1500
1700      call FeChybne(-1.,-1.,'inacceptable real number "'//
     1                  Cislo(:idel(Cislo))//'"','try again.',
     2                  SeriousError)
          go to 1500
1710      call FeChybne(-1.,-1.,'the string is empty','try again.',
     1                  SeriousError)
          go to 1500
        endif
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwUniform.or.
     2         CheckNumber.eq.nCrwExplicit.or.
     3         CheckNumber.eq.nCrwAIM)) then
        if(CrwLogicQuest(nCrwUniform)) then
          ContourType=ContourTypeLinear
        else if(CrwLogicQuest(nCrwExplicit)) then
          ContourType=ContourTypeUser
        else
          ContourType=ContourTypeAIM
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawPos) then
        if(CrwLogicQuest(CheckNumber)) then
          DrawPos=1
        else
          DrawPos=0
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawNeg) then
        if(CrwLogicQuest(CheckNumber)) then
          DrawNeg=1
        else
          DrawNeg=0
        endif
        go to 1300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  if(ich.eq.0) then
        if(ContourType.eq.ContourTypeLinear) then
          if(CrwLogicQuest(nCrwDrawPos)) then
            call FeQuestRealFromEdw(nEdwPosCon,fmzp)
            call FeQuestRealFromEdw(nEdwPosCut,cutpos)
            cutpos=cutpos-ContourRefLevel
            DrawPos=1
          else
            DrawPos=0
          endif
          if(CrwLogicQuest(nCrwDrawNeg)) then
            call FeQuestRealFromEdw(nEdwNegCon,fmzn)
            call FeQuestRealFromEdw(nEdwNegCut,cutneg)
            cutneg=cutneg-ContourRefLevel
            DrawNeg=1
          else
            DrawNeg=0
          endif
        else if(ContourType.eq.ContourTypeAIM) then
          call FeQuestIntFromEdw(nEdwDrawAIM,ContourAIMOrder)
        else
          call ZdrcniCisla(ContourOld,ContourNumber)
        endif
      else
        ContourOld=' '
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine ConDefDeform(ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*1 t(5)
      character*2 nty
      character*80 Veta
      logical CrwLogicQuest
      dimension nh(30)
      data t/'2','3','4','5','6'/
      NFirst=NAtIndFrAll(KPhase)
      xqd=WizardLength
1100  id=NextQuestId()
      n=0
      NFrom=NFirst
      do i=NFirst,NAtIndToAll(KPhase)
        if(AtBrat(i)) then
          n=n+1
          nh(n)=i
          if(n.ge.30) go to 1110
        endif
      enddo
      i=NAtIndToAll(KPhase)
      if(n.le.0) then
        ich=0
        go to 9999
      endif
1110  NFirst=i+1
      Veta='Select of anharmonic terms for deformation maps'
      call FeQuestTitleMake(id,Veta)
      xpom=20.
      il=1
      if(n.le.15) then
        jk=1
      else
        jk=2
      endif
      do j=1,jk
        call FeQuestLblMake(id,xpom,il,'Atom','L','N')
        xpom=xpom+70.
        do i=1,5
          call FeQuestLblMake(id,xpom,il,t(i)//nty(i),'L','N')
          xpom=xpom+40.
        enddo
        xpom=xqd*.5+20.
      enddo
      if(n.gt.15) call FeQuestSvisliceFromToMake(id,xqd*.5,1,16,0)
      xpoms=20.
      do i=1,n
        il=il+1
        ia=nh(i)
        xpom=xpoms
        call FeQuestLblMake(id,xpom,il,atom(ia),'L','N')
        xpom=xpom+70.
        itfi=itf(ia)-1
        if(itfi.le.0) itfi=1
        do j=1,itfi
          call FeQuestCrwMake(id,xpom,il,xpom,il,' ','L',CrwXd,
     1                        CrwYd,0,0)
          call FeQuestCrwOpen(CrwLastMade,AtBratAnharm(j,ia))
          if(i.eq.1.and.j.eq.1) nCrwFirst=CrwLastMade
          xpom=xpom+40.
        enddo
        if(il.eq.16) then
          il=1
          xpoms=xpoms+xqd*.5
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      nCrw=nCrwFirst
      if(ich.eq.0) then
        k=0
        do i=1,n
          ia=nh(i)
          itfi=itf(ia)-1
          if(itfi.le.0) itfi=1
          do j=1,5
            if(j.le.itfi) then
              AtBratAnharm(j,ia)=CrwLogicQuest(nCrw)
              nCrw=nCrw+1
            else
              AtBratAnharm(j,ia)=.false.
            endif
          enddo
        enddo
      else
        if(ich.gt.0) then
          if(NFrom.gt.1) then
            NFirst=max(NFrom-30,1)
            go to 1100
          else
            go to 9999
          endif
        else
          go to 9999
        endif
      endif
      if(NFirst.le.NAtIndToAll(KPhase)) go to 1100
9999  return
      end
      function pdfat(xx,t,iat,klic,ich)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(3),bp(6),cp(10),dp(15),ep(21),fp(28),p(9),pinv(3,3),
     1          pa(9),rot(3,3),rotm(28,28),b(6),c(10),d(15),e(21),f(28),
     2          ccp(74),cc(74),xp(3),xf(3),occ(1),Gamma(9),x4(3),
     3          tzero(3),dt(3),nt(3)
      equivalence (cc (1),c ),(cc (11),d ),(cc (26),e ),(cc (47),f ),
     1            (ccp(1),cp),(ccp(11),dp),(ccp(26),ep),(ccp(47),fp),
     2            (xp1,xp(1)),(xp2,xp(2)),(xp3,xp(3))
      data tpisq/19.7392088/,tzero,dt/6*0./,nt/1,0,0/
      save x1a,x2a,x3a,coef,cum3,cum4,cum5,cum6,pomdef,ccp,pa,itfi
      if(klic.eq.0) then
        if(NDim(KPhase).gt.3) call UnitMat(Gamma,NDimI(KPhase))
        ip=ipor(iat)
        ia=iapdf(ip)
        itfi=itf(ia)
        isw=iswa(ia)
        if(itfi.eq.1) then
          pom=beta(1,ia)
          do j=1,6
            beta(j,ia)=pom*prcp(j,isw,KPhase)
          enddo
          itf(ia)=2
          itfi=2
        endif
        if(NDim(KPhase).gt.3) then
          x4(1)=qcnt(1,ia)+t
          if(KModA(1,ia).gt.0) then
            if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
              call MakeOccMod(occ(1),x4,tzero,nt,dt,a0(ia),ax(1,ia),
     1          ay(1,ia),KModA(1,ia),KFA(1,ia),Gamma)
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(occp,x4,tzero,nt,dt,
     1            ux(1,KModA(2,ia),ia),uy(2,KModA(2,ia),ia),KFA(2,ia),
     2            Gamma)
                if(occp.le.0.) occ(1)=0.
              endif
            else
              call MakeOccModPol(occ(1),x4,tzero,nt,dt,
     1                           ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                           ax(KModA(1,ia),ia),a0(ia)*.5,
     3                           Gamma,TypeModFun(ia))
            endif
            if(occ(1).le..001) then
              ich=2
              go to 9999
            endif
          endif
        endif
        do n=2,itfi
          nrank=TRank(n)
          if(n.eq.2) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeBetaMod(bp,x4,tzero,nt,dt,bx(1,1,ia),
     1                           by(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                           Gamma)
              else
                call MakeBetaModPol(bp,x4,tzero,nt,dt,bx(1,1,ia),
     1                              by(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                              a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(bp,beta(1,ia),bp,nrank)
            else
              call CopyVek(beta(1,ia),bp,nrank)
            endif
          else if(n.eq.3) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c3x(1,1,ia),
     1                          c3y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c3x(1,1,ia),
     1                             c3y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c3(1,ia),cp,nrank)
            else
              call CopyVek(c3(1,ia),cp,nrank)
            endif
          else if(n.eq.4) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c4x(1,1,ia),
     1                          c4y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c4x(1,1,ia),
     1                             c4y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c4(1,ia),cp,nrank)
            else
              call CopyVek(c4(1,ia),dp,nrank)
            endif
          else if(n.eq.5) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c5x(1,1,ia),
     1                          c5y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c5x(1,1,ia),
     1                             c5y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c5(1,ia),cp,nrank)
            else
              call CopyVek(c5(1,ia),ep,nrank)
            endif
          else
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c6x(1,1,ia),
     1                          c6y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c6x(1,1,ia),
     1                             c6y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c6(1,ia),cp,nrank)
            else
              call CopyVek(c6(1,ia),fp,nrank)
            endif
          endif
        enddo
        if(ispdf(ip).eq.0) ispdf(ip)=1
        k=ispdf(ip)
        l=abs(k)
        do i=1,3
          do j=1,3
            rot(i,j)=rm(i+(j-1)*3,l,isw,KPhase)
          enddo
        enddo
        if(k.lt.0) then
          do i=1,3
            do j=1,3
              rot(i,j)=-rot(i,j)
            enddo
          enddo
        endif
        i=1
        do n=2,itfi
          nrank=TRank(n)
          if(n.eq.2) then
            call srotb(rot,rot,rotm)
            call multm(rotm,bp,b,nrank,nrank,1)
          else
            call srotc(rot,n,rotm)
            call multm(rotm,ccp(i),cc(i),nrank,nrank,1)
            i=i+nrank
          endif
        enddo
        x1a=xpdf(1,ip)
        x2a=xpdf(2,ip)
        x3a=xpdf(3,ip)
        cum3=0.
        cum4=0.
        cum5=0.
        cum6=0.
        do i=1,6
          call indext(i,j,k)
          pinv(j,k)=b(i)/tpisq
        enddo
        pinv(2,1)=pinv(1,2)
        pinv(3,1)=pinv(1,3)
        pinv(3,2)=pinv(2,3)
        call matinv(pinv,p,pvol,3)
        call qln(p,rot,pdiag,3)
        call TrMat(rot,pa,3,3)
        j=1
        do n=3,itfi
          nrank=TRank(n)
          call srotc(pa,n,rotm)
          call multm(rotm,cc(j),ccp(j),nrank,nrank,1)
          j=j+nrank
        enddo
        do i=1,TRankCumul(itfi)-10
          ccp(i)=ccp(i)*cmlt(i)
        enddo
        if(BratAnharmPDF(1,ip)) then
          pomdef=1.
        else
          pomdef=0.
        endif
        pdfat=0.
        if(pvol.gt.0.) then
          ich=0
          coef=1./sqrt(pvol*pi2**3)
          if(OldNorm) go to 9999
          coef=coef/CellVol(isw,KPhase)
        else
          ich=1
        endif
      else
        xf(1)=xx(1)-x1a
        xf(2)=xx(2)-x2a
        xf(3)=xx(3)-x3a
        call MultM(TrToOrtho,xf,xo,3,3,1)
        if(VecOrtLen(xo,3).gt.CutOffDist) then
          pdfat=0.
          go to 9999
        endif
        call multm(pa,xf,xp,3,3,1)
        w1=pdiag(1)*xp1
        w2=pdiag(2)*xp2
        w3=pdiag(3)*xp3
        pdfat=coef*ExpJana(-.5*(xp1*w1+xp2*w2+xp3*w3),ich)
        if(ich.ne.0) go to 9999
        call priprav(itfi)
        if(BratAnharmPDF(2,ip)) cum3=rank3(cp)
        if(BratAnharmPDF(3,ip)) cum4=rank4(dp)
        if(BratAnharmPDF(4,ip)) cum5=rank5(ep)
        if(BratAnharmPDF(5,ip)) cum6=rank6(fp)
        pdfat=pdfat*(pomdef+cum3+cum4+cum5+cum6)
      endif
9999  return
      end
      subroutine Del8
      include 'fepc.cmn'
      include 'basic.cmn'
      call DeleteFile(fln(:ifln)//'.l81')
      call DeleteFile(fln(:ifln)//'.l82')
      return
      end
      subroutine OkoliC(x1,dmez,ir,isw,vyber)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x0(6),x1(3),y1(6),y2(3),u(3),ug(3),shj(3),
     1          uz(3),shjc(3),jsh(3),xp(3)
      character*40 t40
      logical vyber
      npdf=0
      call bunka(x1,x0,0)
      jcellxm=anint(dmez*rcp(1,nsubs,KPhase))+1
      jcellym=anint(dmez*rcp(2,nsubs,KPhase))+1
      jcellzm=anint(dmez*rcp(3,nsubs,KPhase))+1
      do j=1,NAtCalc
        if(iswa(j).ne.isw.or.kswa(j).ne.KPhase.or.
     1     (.not.atbrat(j).and.vyber)) cycle
        do l=1,NDim(KPhase)
          if(l.le.3) then
            x0(l)=x(l,j)
          else
            x0(l)=0.
          endif
        enddo
        js=0
        do jsym=1,NSymmN(KPhase)
          js=js+1
          if(isa(js,j).le.0) cycle
          call multm(rm6(1,jsym,nsubs,KPhase),x0,y1,NDim(KPhase),
     1               NDim(KPhase),1)
          do l=1,3
            y1(l)=y1(l)+s6(l,jsym,nsubs,KPhase)
          enddo
          do jcenter=1,NLattVec(KPhase)
            do l=1,3
              y2(l)=y1(l)+vt6(l,jcenter,nsubs,KPhase)
            enddo
            call bunka(y2,shj,1)
            do l=1,3
              uz(l)=y2(l)-x1(l)
            enddo
            do jcellx=-jcellxm,jcellxm
              cellxj=jcellx
              pom=uz(1)+cellxj
              jsh(1)=nint(shj(1))+jcellx
              shjc(1)=shj(1)+cellxj+s6(1,jsym,nsubs,KPhase)
     1                             +vt6(1,jcenter,nsubs,KPhase)
              u(1)=pom
              do jcelly=-jcellym,jcellym
                cellyj=jcelly
                pom=uz(2)+cellyj
                u(2)=pom
                jsh(2)=nint(shj(2))+jcelly
                shjc(2)=shj(2)+cellyj+s6(2,jsym,nsubs,KPhase)
     1                               +vt6(2,jcenter,nsubs,KPhase)
                do 1500jcellz=-jcellzm,jcellzm
                  cellzj=jcellz
                  pom=uz(3)+cellzj
                  u(3)=pom
                  jsh(3)=nint(shj(3))+jcellz
                  shjc(3)=shj(3)+cellzj+s6(3,jsym,nsubs,KPhase)
     1                                 +vt6(3,jcenter,nsubs,KPhase)
                  call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
                  d=sqrt(scalmul(u,ug))
                  if(d.ge.dmez) go to 1500
                  m=d*1000.
                  call ReallocatePDF(npdf+1)
                  npdf=npdf+1
                  do k=1,3
                    xpdf(k,npdf)=u(k)+x1(k)
                  enddo
                  if(ir.ne.0) then
                    if(ir.eq.2) then
                      call prevod(0,xpdf(1,npdf),xp)
                    else
                      call CopyVek(xpdf(1,npdf),xp,3)
                    endif
                    do k=1,3
                      if(ir.eq.1) then
                        l=iorien(k)
                      else
                        l=k
                      endif
                      if(xp(l).lt.xmina(k,ir)-CutOffDist.or.
     1                   xp(l).gt.xmaxa(k,ir)+CutOffDist) then
                        npdf=npdf-1
                        go to 1500
                      endif
                    enddo
                  endif
                  idpdf(npdf)=m
                  iapdf(npdf)=j
                  ispdf(npdf)=jsym
                  popaspdf(1:64,npdf)=0.
                  call scode(jsym,jcenter,shjc,jsh,1,scpdf(npdf),t40)
                  k=idel(t40)
                  if(k.gt.0)
     1              scpdf(npdf)=scpdf(npdf)(:idel(scpdf(npdf)))//'#'//
     2                          t40(:k)
1500            continue
              enddo
            enddo
          enddo
        enddo
      enddo
      do i=1,npdf
        dpdf(i)=float(idpdf(i))/1000.
      enddo
      call indexx(npdf,idpdf,ipor)
      return
      end
      subroutine ConMakeEntFile
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      n=0
      do i=1,3
        xmin(i)=-ShiftPlane(i)
      enddo
      call OpenFile(89,fln(1:ifln)//'.ent','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do 1510j=1,npdf
        k=ipor(j)
        if(fpdf(k).le.0.) go to 1510
        n=n+1
        ia=iapdf(k)
        i=isf(ia)
        t80='ATOM'
        l=idel(AtType(i,KPhase))
        write(t80(7:11),'(i5)') n
        t80=t80(1:14-l)//AtType(i,KPhase)(:l)
        write(t80(23:26),'(i4)') n
        call prevod(0,xpdf(1,k),ypdf(1,k))
        write(t80(31:66),'(3f8.4,2f6.3)')(ypdf(l,k),l=1,3),fpdf(k),1.
        write(89,FormA1)(t80(l:l),l=1,idel(t80))
        write(89,'(''REMARK    '',a8,3f9.6)') atom(ia),(xpdf(l,k),
     1        l=1,3)
1510  continue
      call ConMakeConnect
9999  call CloseIfOpened(89)
      return
      end
      subroutine ConMakeConnect
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension dpmin(:,:),dpmax(:,:),dpmino(:,:),dpmaxo(:,:),xp(2)
      character*80 t80
      character*8 At1,At2,At,AtArr(2)
      integer SbwLnQuest,SbwItemSelQuest
      logical EqIgCase
      allocatable dpmin,dpmax,dpmino,dpmaxo
      equivalence (AtArr(1),At1),(AtArr(2),At2)
      allocate(dpmin(NAtFormula(KPhase),NAtFormula(KPhase)),
     1         dpmax(NAtFormula(KPhase),NAtFormula(KPhase)),
     2         dpmino(NAtFormula(KPhase),NAtFormula(KPhase)),
     3         dpmaxo(NAtFormula(KPhase),NAtFormula(KPhase)))
      do j=1,NAtFormula(KPhase)
        do i=j,NAtFormula(KPhase)
          dpmin(i,j)=0.
          if(i.ne.j) dpmin(j,i)=0.
          dpmax(i,j)=(AtRadius(i,KPhase)+AtRadius(j,KPhase))*.5
          if(i.ne.j) dpmax(j,i)=dpmax(i,j)
        enddo
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_distlim.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do j=1,NAtFormula(KPhase)
        do i=j,NAtFormula(KPhase)
          dpmaxo(j,i)=dpmax(i,j)
          dpmino(j,i)=dpmin(i,j)
          write(t80,'(''distlim '',a2,1x,a2,2f10.3)')
     1      AtType(i,KPhase),AtType(j,KPhase),dpmin(i,j),dpmax(i,j)
          call ZdrcniCisla(t80,5)
          write(ln,FormA) t80(:idel(t80))
        enddo
      enddo
      call CloseIfOpened(ln)
      call AtomDistLimits
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_distlim.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1050  read(ln,FormA80,end=1100) t80
      k=0
      call kus(t80,k,Cislo)
      do l=1,2
        call kus(t80,k,Cislo)
        j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),Cislo,
     1                        .true.)
        if(j.lt.1.or.j.gt.NAtFormula(KPhase)) go to 1050
        if(l.eq.1) i=j
      enddo
      call StToReal(t80,k,xp,2,.false.,ich)
      if(ich.ne.0) go to 1050
      dpmin(i,j)=xp(1)
      dpmax(i,j)=xp(2)
      go to 1050
1100  close(ln,status='delete')
      call OpenFile(ln,fln(:ifln)//'_seldist.tmp','formatted','unknown')
      ni=0
      n=0
      do 4000i=1,npdf
        ik=ipor(i)
        if(fpdf(ik).le.0.) go to 4000
        ni=ni+1
        ia=iapdf(ik)
        isfi=isf(ia)
        nj=ni
        write(Cislo,FormI15) ni
        call zhusti(Cislo)
        At1=AtType(isfi,KPhase)(:idel(AtType(isfi,KPhase)))//
     1      Cislo(:idel(Cislo))
        do 3000j=i+1,npdf
          jk=ipor(j)
          if(fpdf(jk).le.0.) go to 3000
          nj=nj+1
          ja=iapdf(jk)
          isfj=isf(ja)
          dist=0.
          do k=1,3
            dist=dist+(ypdf(k,ik)-ypdf(k,jk))**2
          enddo
          dist=sqrt(dist)
          if(dist.le.dpmax(isfi,isfj).and.dist.gt.dpmin(isfi,isfj))
     1       then
            n=n+1
            write(Cislo,FormI15) nj
            call zhusti(Cislo)
            At2=AtType(isfj,KPhase)(:idel(AtType(isfj,KPhase)))//
     1          Cislo(:idel(Cislo))
            write(ln,100) AtArr,dist
          endif
3000    continue
4000  continue
      call CloseIfOpened(ln)
      id=NextQuestId()
      xqd=400.
      il=14
      t80='Select individual distances'
      call FeQuestCreate(id,-1.,-1.,xqd,il,t80,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.
      il=12
      ild=12
      call FeQuestSbwMake(id,xpom,il,dpom,ild,3,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      il=il+2
      dpom=60.
      xpom=xqd*.5-dpom-5.
      t80='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+5.
      t80='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      isel=1
4100  do i=1,n
        call FeQuestSetSbwItemSel(i,nSbw,isel)
      enddo
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_seldist.tmp')
4500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        if(CheckNumber.eq.nButtAll) then
          isel=1
        else
          isel=0
        endif
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 4100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 4500
      endif
      if(ich.eq.0) then
        call CloseIfOpened(SbwLnQuest(nSbw))
        call OpenFile(ln,fln(:ifln)//'_seldist.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 9999
        At=' '
        nd=0
        n=0
5000    n=n+1
        read(ln,100,end=6000,err=6000) AtArr,dist
        do k=1,2
          Cislo=AtArr(k)
          do j=1,idel(Cislo)
            if(index(Cifry(:10),Cislo(j:j)).gt.0) go to 5020
          enddo
5015      j=0
          go to 5030
5020      Cislo=Cislo(j:)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=5015) j
5030      if(k.eq.1) then
            ni=j
          else
            nj=j
          endif
        enddo
        if(.not.EqIgCase(At1,At)) then
          if(nd.gt.0) write(89,FormA) t80(:idel(t80))
          At=At1
          t80='CONECT'
          write(t80(7:11),102) ni
          ip=12
          nd=0
        endif
        if(SbwItemSelQuest(n,nSbw).eq.1) then
          if(ip.ge.32) then
            write(89,FormA) t80(:idel(t80))
            t80='CONECT'
            write(t80(7:11),102) ni
            ip=12
            nd=0
          endif
          nd=nd+1
          write(t80(ip:ip+4),102) nj
          ip=ip+5
        endif
        go to 5000
6000    if(ip.gt.12) write(89,FormA) t80(:idel(t80))
        call CloseIfOpened(ln)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call DeleteFile(fln(:ifln)//'_seldist.tmp')
9999  deallocate(dpmin,dpmax,dpmino,dpmaxo)
      call CloseIfOpened(ln)
      return
100   format(a8,1x,a8,1x,f8.3)
102   format(i5)
      end
      subroutine ConSummMapsDef
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension ior(6)
      character*3 t3
      integer EdwStateQuest
      logical CrwLogicQuest
      read(m8,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                        ior,mapa,nsubs,SatelityByly,
     2                        nonModulated(KPhase)
      nsubs=mod(nsubs,10)
      id=NextQuestId()
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)-2,'Summation',0,
     1                   LightGray,0,0)
      tpom=5.
      xpom=tpom+FeTxLength('XXX')+15.
      iw=0
      tpome1=xpom+CrwXd+15.
      xpome1=tpome1+FeTxLength('XXXX')+10.
      dpom=80.
      tpome2=tpome1+dpom+60.
      xpome2=xpome1+dpom+60.
      do i=1,NDim(KPhase)-2
        t3=cx(i+2)
        if(t3(2:2).eq.' ') then
          t3='%'//t3(1:1)
        else
          t3=t3(1:1)//'%'//t3(2:2)
        endif
        call FeQuestCrwMake(id,tpom,i,xpom,i,t3,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(i,.false.)
        call FeQuestEudMake(id,tpome1,i,xpome1,i,'from','L',dpom,EdwYd,
     1                      0)
        call FeQuestEudMake(id,tpome2,i,xpome2,i,'  to','L',dpom,EdwYd,
     1                      0)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        if(CrwLogicQuest(CheckNumber)) then
          do i=1,CheckNumber
            iw=2*i-1
            if(EdwStateQuest(iw).ne.EdwOpened) then
              call FeQuestCrwOn(i)
              call FeQuestRealEdwOpen(iw,xmin(i+2),.false.,.false.)
              call FeQuestEudOpen(iw,1,1,1,xmin(i+2),xmax(i+2),dx(i+2))
              call FeQuestRealEdwOpen(iw+1,xmax(i+2),.false.,.false.)
              call FeQuestEudOpen(iw+1,1,1,1,xmin(i+2),xmax(i+2),dx(i+2)
     1                            )
            endif
          enddo
        else
          do i=CheckNumber,NDim(KPhase)-2
            iw=2*i-1
            call FeQuestCrwOff(i)
            call FeQuestEdwClose(iw)
            call FeQuestEdwClose(iw+1)
          enddo
        endif
        EventType=EventEdw
        EventNumber=2*CheckNumber-1
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        isoucet=0
        iw=0
        do i=1,NDim(KPhase)-2
          if(CrwLogicQuest(i)) then
            isoucet=isoucet+10**(i-1)
            iw=iw+1
            if(dx(i+2).ne.0.) then
              nxfrom(i)=nint((EdwRealQuest(1,iw)-xmin(i+2))/dx(i+2))
     1                  +1
            else
              nxfrom(i)=1
            endif
            iw=iw+1
            if(dx(i+2).ne.0.) then
              nxto(i)=nint((EdwRealQuest(1,iw)-xmin(i+2))/dx(i+2))+1
            else
              nxto(i)=1
            endif
          endif
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) isoucet=0
1000  if(isoucet.eq.0) then
        read(m8,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          ior,mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
        nsubs=mod(nsubs,10)
        read(m8,rec=nmap+2,err=9000) Dmax,Dmin
        smapy=.false.
      else if(isoucet.gt.0) then
        call CloseIfOpened(86)
        call OpenMaps(86,fln(:ifln)//'.l86',nxny,1)
        if(ErrFlag.ne.0) go to 9999
        call ConSummMaps(ior)
        if(ErrFlag.ne.0) then
          isoucet=0
          ErrFlag=0
          go to 1000
        endif
        m8=86
        smapy=.true.
      endif
      call SetContours(0)
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
9999  return
      end
      subroutine ConSummMaps(ior)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),ior(6)
      real InputMap(:),table(:)
      allocatable InputMap,table
      data xp/3*0./
      allocate(InputMap(nxny),table(nxny))
      NSum=0
      nx3=nx(3)
      nx4=nx(4)
      nx5=nx(5)
      if(mod(isoucet/1000,10).eq.1) then
        xmin(6)=xmin(6)+float(nxto(4)+nxfrom(4)-2)*dx(6)*.5
        xmax(6)=xmin(6)
        nx(6)=1
      endif
      if(mod(isoucet/100,10).eq.1) then
        xmin(5)=xmin(5)+float(nxto(3)+nxfrom(3)-2)*dx(5)*.5
        xmax(5)=xmin(5)
        nx(5)=1
      endif
      if(mod(isoucet/10,10).eq.1) then
        xmin(4)=xmin(4)+float(nxto(2)+nxfrom(2)-2)*dx(4)*.5
        xmax(4)=xmin(4)
        nx(4)=1
      endif
      if(mod(isoucet,10).eq.1) then
        xmin(3)=xmin(3)+float(nxto(1)+nxfrom(1)-2)*dx(3)*.5
        xmax(3)=xmin(3)
        nx(3)=1
      endif
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      write(86,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,ior,mapa,
     1                nsubs,SatelityByly,nonModulated(KPhase)
      izz=1
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      nvse=(nxto(1)-nxfrom(1)+1)*(nxto(2)-nxfrom(2)+1)*
     1     (nxto(3)-nxfrom(3)+1)
      n=0
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nvse)*.005),10),nvse,
     1                     'Summation of maps',' ',' ')
      if(isoucet.eq.1111) then
        call SetRealArrayTo(table,nxny,0.)
        nSum=0
      endif
      irec1=nxfrom(4)-1
      do iii=nxfrom(4),nxto(4)
        if(isoucet.eq.111) then
          call SetRealArrayTo(table,nxny,0.)
          NSum=0
        endif
        irec2=irec1*nx5+nxfrom(3)-1
        do jjj=nxfrom(3),nxto(3)
          if(isoucet.eq.11) then
            call SetRealArrayTo(table,nxny,0.)
            NSum=0
          endif
          irec3=irec2*nx4+nxfrom(2)-1
          do kkk=nxfrom(2),nxto(2)
            if(isoucet.eq.1) then
              call SetRealArrayTo(table,nxny,0.)
              NSum=0
            endif
            irec=irec3*nx3+nxfrom(1)
            do lll=nxfrom(1),nxto(1)
              irec=irec+1
              call FeFlowChartEvent(n,is)
              if(is.ne.0) then
                call FeBudeBreak
                if(ErrFlag.ne.0) then
                  close(82,status='delete')
                  go to 9999
                endif
              endif
              read(m8,rec=irec,err=9000)(InputMap(i),i=1,nxny)
              NSum=NSum+1
              call AddVek(table,InputMap,table,nxny)
            enddo
            if(isoucet.eq.1) then
              pom=1./Float(NSum)
              do i=1,nxny
                table(i)=table(i)*Pom
                Dmax=max(Dmax,table(i))
                Dmin=min(Dmin,table(i))
              enddo
              izz=izz+1
              write(86,rec=izz)(table(i),i=1,nxny)
            endif
            irec3=irec3+1
          enddo
          if(isoucet.eq.11) then
            pom=1./Float(NSum)
            do i=1,nxny
              table(i)=table(i)*Pom
              Dmax=max(Dmax,table(i))
              Dmin=min(Dmin,table(i))
            enddo
            izz=izz+1
            write(86,rec=izz)(table(i),i=1,nxny)
          endif
          irec2=irec2+1
        enddo
        if(isoucet.eq.111) then
          pom=1./Float(NSum)
          do i=1,nxny
            table(i)=table(i)*Pom
            Dmax=max(Dmax,table(i))
            Dmin=min(Dmin,table(i))
          enddo
          izz=izz+1
          write(86,rec=izz)(table(i),i=1,nxny)
        endif
        irec1=irec1+1
      enddo
      call FeFlowChartRemove
      if(isoucet.eq.1111) then
        pom=1./Float(NSum)
        do i=1,nxny
          table(i)=table(i)*Pom
          Dmax=max(Dmax,table(i))
          Dmin=min(Dmin,table(i))
        enddo
        izz=izz+1
        write(86,rec=izz)(table(i),i=1,nxny)
      endif
      izz=izz+1
      write(86,rec=izz) Dmax,Dmin
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
      call FeFlowChartRemove
9999  deallocate(InputMap,table)
      return
      end
      subroutine PCurves(klic)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xdo(3),xfract(3),xp(3,10),xd(3,9),xfd(3,4),u(3),ug(3),
     1          dlk(10),v(3),vg(3),w(3),
     2          gx(:,:),gy(:,:),pty(:,:),sgymc(:,:),spty(:,:),dpty(:,:),
     3          ErrMap(:)
      character*256 t256,EdwStringQuest
      character*80  SvFile,Veta
      character*25  men1(5)
      character*15  Hustota
      character*8   NewAtName
      character*7   FormatLocal
      character*2   nty
      integer :: FeMenu,Exponent10,NExtPoints=100
      logical IncDer,IncErr,JeUvnitr,JesteSeNepsalo,CrwLogicQuest,
     1        EqIgCase,ByloCrw,ExistFile
      allocatable gx,gy,pty,sgymc,spty,dpty,ErrMap
      equivalence (xd(1,2),xfd),(xfd(1,3),u)
      data men1/'Change %y limits',
     1          'Draw %derivation curve',
     2          'Draw %error curve',
     3          'Remove %derivation curve',
     4          'Remove %error curve'/
      data Angle/90./,isfp/1/,biso/3./
      Tiskne=.false.
      ByloCrw=.false.
      if(ErrMapActive) then
        allocate(ErrMap(NActualMap),stat=i)
        if(i.ne.0) then
          call FeAllocErr('Error occur in "PCurves" point#1.',i)
          go to 9000
        endif
        read(83,rec=irec+1)(ErrMap(i),i=1,NActualMap)
      endif
      call FeMakeAcWin(80.,20.,30.,30.)
      HardCopy=0
      xhld=XMinBasWin+80.
      IncDer=.false.
      IncErr=.false.
      JesteSeNepsalo=.true.
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile)
      if(klic.eq.0) then
        if(NCurve.le.0) XTypeCurve=1
        id=NextQuestId()
        Veta='.... point/atom'
        dpom=200.
        xdq=FeTxLengthUnder(Veta)+dpom+20.
        il=15
        call FeQuestCreate(id,-1.,-1.,xdq,il,'Define polyline',0,
     1                     LightGray,0,0)
        il=1
        xpom=FeTxLengthUnder(Veta)+10.
        tpom=xpom+CrwgXd+10.
        t256='%Local coordinates'
        call FeQuestCrwMake(id,tpom,il,xpom,il,t256,'L',CrwgXd,CrwgYd,1,
     1                      1)
        nCrwLocal=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,XTypeCurve.eq.1)
        il=il+1
        t256='%Fractional coordinates'
        call FeQuestCrwMake(id,tpom,il,xpom,il,t256,'L',CrwgXd,CrwgYd,1,
     1                      1)
        nCrwFract=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,XTypeCurve.eq.2)
        tpom=5.
        xpom=FeTxLengthUnder(Veta)+15.
        do i=1,10
          il=il+1
          write(Veta(1:4),'(i2,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          if(i.eq.1) nEdwFirstPoint=EdwLastMade
          if(i.le.NCurve) then
            call ConSetPointCurve(AtCurve(i),i,ich)
            if(ich.ne.0) AtCurve(i)=' '
          endif
        enddo
        il=il+1
        Veta='%Clear all points'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xdq-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtClear=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Use %mouse to define polyline'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xdq-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtUseMouse=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='%Number of extrapolation points in each interval:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestIntEdwOpen(EdwLastMade,NExtPoints,.false.)
        nEdwExtPoints=EdwLastMade
1110    nEdw=nEdwFirstPoint
        do i=1,10
          if(i.le.NCurve) then
            if(AtCurve(i).eq.' ') then
              if(XTypeCurve.eq.1) then
                write(Veta,100)(XLocCurve(j,i),j=1,3)
              else
                write(Veta,101)(XFractCurve(j,i),j=1,3)
              endif
              call ZdrcniCisla(Veta,3)
            else
              Veta=AtCurve(i)
            endif
          else
            Veta=' '
          endif
          call FeQuestStringEdwOpen(nEdw,Veta)
          nEdw=nEdw+1
        enddo
1150    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtUseMouse)
     1    then
          NCurve=0
          ich=0
          go to 1300
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtClear)
     1    then
          NCurve=0
          go to 1110
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocal) then
          XTypeCurve=1
          go to 1110
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFract) then
          XTypeCurve=2
          go to 1110
        else if(CheckType.eq.EventEdw) then
          i=CheckNumber-nEdwFirstPoint+1
          t256=EdwStringQuest(CheckNumber)
          if(t256.eq.' ') go to 1150
          call ConSetPointCurve(t256,i,ichp)
          if(XTypeCurve.eq.1) then
            write(Veta,100)(XLocCurve(j,i),j=1,3)
          else
            write(Veta,101)(XFractCurve(j,i),j=1,3)
          endif
          call ZdrcniCisla(Veta,3)
          if(.not.EqIgCase(t256,Veta)) then
            call FeChybne(-1.,-1.,'the last point has been modified '//
     1                    'to be in the scope.',' ',Warning)
            call FeQuestStringEdwOpen(CheckNumber,Veta)
            EventType=EventEdw
            EventNumber=CheckNumber
          endif
          if(ichp.gt.0) then
            call FeQuestStringEdwOpen(CheckNumber,' ')
            EventType=EventEdw
            EventNumber=CheckNumber
          endif
          go to 1150
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        nEdw=nEdwFirstPoint
        NCurve=0
        do i=1,10
          if(EdwStringQuest(nEdw).ne.' ') NCurve=NCurve+1
          nEdw=nEdw+1
        enddo
1300    if(ich.eq.0) call FeQuestIntFromEdw(nEdwExtPoints,NExtPoints)
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9000
        if(NCurve.gt.1) go to 3111
      endif
      ip=0
      i=max(4-Exponent10(max(xmax(1),xmax(2))),0)
      i=min(i,4)
      write(FormatLocal,'(''(2f7.'',i1,'')'')') i
      if(LocatorType.eq.LocatorPosition) then
        MouseType=1
      else
        MouseType=0
      endif
      ireco=irec
2000  if(Klic.eq.0) then
        Xpos=(XminGrWin+XmaxGrWin)*.5
        Ypos=(YminGrWin+YmaxGrWin)*.5
        call GetCoord(xdo,xp,xfract)
        call FeMoveMouseTo(Xpos,Ypos)
        call FeMouseShape(0)
        JeUvnitr=.true.
        call FeHeaderInfo(' ')
        irec=0
      else
        call GetCoord(xdo,xp,xfract)
        Xposp=FeXo2X(xdo(1))
        Yposp=FeYo2Y(xdo(2))
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          call FeMouseShape(0)
          call ConHeader
          JeUvnitr=.false.
        else
          call FeMouseShape(MouseType)
          call FeHeaderInfo(' ')
          irec=0
          JeUvnitr=.true.
        endif
      endif
      if(JeUvnitr) call ConLocatorInfoOpen(xp,xfract)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
2010  call FeQuestEvent(ContourQuest,ich)
2020  if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeLeftDown) then
        if(klic.eq.0) then
          if(ip.eq.2) then
            call FePlotMode('E')
            call FePolyLine(2,xu,yu,Green)
            xu(ip)=Xpos
            yu(ip)=Ypos
            call FePlotMode('N')
            call FeArrow(xu(1),yu(1),xu(2),yu(2),LightYellow,ArrowTo)
            xu(1)=xu(2)
            yu(1)=yu(2)
            ip=1
          else
            ip=ip+1
            xu(ip)=Xpos
            yu(ip)=Ypos
          endif
          NCurve=NCurve+1
          XLocCurve(1,NCurve)=FeX2Xo(Xpos)
          XLocCurve(2,NCurve)=FeY2Yo(Ypos)
          XLocCurve(3,NCurve)=zmap(1)
          if(NCurve.ge.10) then
            call FeChybne(-1.,-1.,'the maximum of 10 lines reached.',
     1                    ' ',SeriousError)
            go to 3100
          endif
        else if(JeUvnitr) then
          if(LocatorType.eq.LocatorPosition) then
            XposP=Xpos
            YposP=Ypos
            call FeMouseShape(0)
            call GetCoord(xdo,xp,xfract)
            write(Hustota,103)
     1        ExtMap(xp(1,1),xp(2,1),ActualMap,NActualMap)+
     2        ContourRefLevel
            call Zhusti(Hustota)
            npdf=0
            if(NDim(KPhase).eq.3.or.xyzmap) then
              call OkoliC(xfract,LocDMax,0,NSubs,.false.)
              npdf=min(npdf,19)
              do i=1,npdf
                k=ipor(i)
                write(Cislo,'(f6.3)') dpdf(k)
                TextInfo(i+1)=Tabulator//
     1                        atom(iapdf(k))(:idel(atom(iapdf(k))))//
     2                        Tabulator//'|'//Tabulator//Cislo//
     3                        Tabulator//'|'//
     4                        Tabulator//scpdf(k)(:idel(scpdf(k)))
              enddo
              write(Veta,'(''Fractional coordinates : '',3f7.4)')
     1                    xfract
              Veta=Veta(:idel(Veta))//', density : '//
     1            Hustota(:idel(Hustota))
              if(npdf.gt.0) then
                Ninfo=npdf+1
                TextInfo(1)='draw'//Tabulator//'atom'//
     1                      Tabulator//'distance'//Tabulator//'symmetry'
              else
                Ninfo=1
                TextInfo(1)='No distances shorter than the limit'
              endif
            else
              TextInfo(1)='Local density : '//Hustota
              write(Cislo,FormatLocal)(xp(l,1),l=1,2)
              Veta='Local coordinates : '//Cislo
              NInfo=1
            endif
            id=NextQuestId()
            xqd=0.
            do i=1,Ninfo
              xqd=max(FeTxLength(TextInfo(i)),xqd)
              if(i.eq.1) call FeNormalFont
            enddo
            call FeBoldFont
            xqd=max(FeTxLength(Veta),xqd)+10.
            call FeNormalFont
            if(NDim(KPhase).eq.3.or.xyzmap) then
              yqd=120.+float(Ninfo)*20.
            else
              yqd=70.+float(Ninfo)*20.
            endif
            call FeQuestAbsCreate(id,-1.,-1.,xqd,yqd,' ',0,LightGray,
     1                            0,0)
            ypom=yqd-15.
            call FeQuestAbsLblMake(id,xqd*.5,ypom,Veta,'C','B')
            ypom=yqd-40.
            call FeTabsReset(0)
            call FeTabsReset(1)
            pom=40.
            call FeTabsAdd(pom,0,IdRightTab,' ')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            pom=pom+FeTxLength('XXXXXXXX |')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            pom=pom+FeTxLength('MM')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            pom=pom+FeTxLength('XX.XXX |')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            pom=pom+FeTxLength('MM')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            xpom=12.
            nCrwFirst=0
            call FeTabsAdd(pom,0,IdRightTab,' ')
            do i=1,Ninfo
              if(i.eq.1) then
                UseTabs=1
              else
                UseTabs=0
              endif
              call FeQuestAbsLblMake(id,5.,ypom,TextInfo(i),'L','N')
              if(i.gt.1) then
                call FeQuestAbsCrwMake(id,xpom,ypom,xpom,ypom-5.,' ',
     1                                 'L',CrwXd,CrwYd,0,0)
                if(i.eq.2) nCrwFirst=CrwLastMade
                k=0
                call kus(TextInfo(i),k,Veta)
                k=index(TextInfo(i),'#')
                if(k.gt.0) Veta=Veta(:idel(Veta))//TextInfo(i)(k:)
                j=LocateInStringArray(DrawAtName,DrawAtN,Veta,
     1                                IgnoreCaseYes)
                call FeQuestCrwOpen(CrwLastMade,j.gt.0)
              endif
              ypom=ypom-20.
              if(i.eq.1) ypom=ypom-5.
            enddo
            ypom=ypom-12.
            if(NDim(KPhase).eq.3.or.xyzmap) then
              Veta='Include the selected point as a %new atom in M40'
              dpom=FeTxLengthUnder(Veta)+10.
              xpom=(xqd-dpom)*.5
              call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
              nButtIncM40=ButtonLastMade
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
              ypom=ypom-25.
              Veta='Save the selected point for %future use'
              dpom=FeTxLengthUnder(Veta)+10.
              xpom=(xqd-dpom)*.5
              call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
              nButtIncPoint=ButtonLastMade
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            else
              nButtIncM40=0
              nButtIncPoint=0
            endif
            call FeTabsReset(0)
            call FeTabsReset(1)
            ich=333
2050        call FeQuestEvent(id,ich)
            if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtIncM40)
     1        then
              if(.not.allocated(AtTypeMenu)) then
                allocate(AtTypeMenu(NAtFormula(KPhase)))
                call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                                 NAtFormula(KPhase))
              endif
              NewAtName=' '
              call EM40NewAtomComplete(xfract,u,v,0,nsubs,ich)
              if(ich.eq.0) then
                 NewAtoms=.true.
              else if(ich.lt.0) then
                 ModifiedAtoms=.true.
              endif
              call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
            else if(CheckType.eq.EventButton.and.
     1              CheckNumber.eq.nButtIncPoint) then
              call ConSaveSelPoint(xfract)
              ich=0
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 2050
            endif
            n=DrawAtN
            NSkrt=0
            if(ich.eq.0) then
              nCrw=nCrwFirst
              do i=1,npdf
                k=0
                call kus(TextInfo(i+1),k,Veta)
                k=index(TextInfo(i+1),'#')
                if(k.gt.0) Veta=Veta(:idel(Veta))//TextInfo(i+1)(k:)
                j=LocateInStringArray(DrawAtName,DrawAtN,Veta,
     1                                IgnoreCaseYes)
                if(CrwLogicQuest(nCrw)) then
                  if(j.gt.0) go to 2058
                  if(DrawAtN.lt.NMaxDraw) then
                    DrawAtN=DrawAtN+1
                    DrawAtName(DrawAtN)=Veta
                    DrawAtColor(DrawAtN)=0
                    DrawAtSkip(DrawAtN)=.false.
                    if(DrawAtN.le.1) then
                      DrawAtBondLim(DrawAtN)=2.
                    else
                      DrawAtBondLim(DrawAtN)=DrawAtBondLim(DrawAtN-1)
                    endif
                    KeysSaved=.false.
                  endif
                else if(j.gt.0) then
                  NSkrt=NSkrt+1
                  DrawAtName(j)=' '
                endif
2058            nCrw=nCrw+1
              enddo
              if(NSkrt.gt.0) then
                n=0
                do i=1,DrawAtN
                  if(DrawAtName(i).eq.' ') cycle
                  n=n+1
                  DrawAtName(n)=DrawAtName(i)
                  DrawAtColor(n)=DrawAtColor(i)
                  DrawAtSkip(n)=DrawAtSkip(i)
                  DrawAtBondLim(n)=DrawAtBondLim(i)
                enddo
                DrawAtN=n
              endif
            endif
            call FeQuestRemove(id)
            if((n.lt.DrawAtN.or.NSkrt.gt.0)) then
              if(.not.AtomsOn) then
                AtomsOn=.true.
                ConButtLabels(IdAtomsOnOff)='At%oms OFF'
                AtomsOn=.true.
                call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                      ConButtLabels(IdAtomsOnOff))
                call FeQuestButtonOff(nButtAtomsDraw)
              endif
              call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                         YMaxGrWin,ConImageFile,-1)
              call ConDrawAtoms(zmap)
              call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                         YMaxGrWin,SvFile)
            endif
            call FeMouseShape(MouseType)
            call FeMoveMouseTo(XposP,YposP)
          else if(LocatorType.eq.LocatorDistance.or.
     1            LocatorType.eq.LocatorNewSection) then
            if(ip.le.1) ip=ip+1
            call CopyVek(xfract,xfd(1,ip),3)
            if(ip.eq.1) then
              xu(1)=XPos
              yu(1)=Ypos
            else
              do i=1,3
                xfd(i,3)=xfd(i,1)-xfd(i,2)
              enddo
              call multm(MetTens(1,nsubs,KPhase),xfd(1,3),xfd(1,4),3,3,
     1                   1)
              dd=sqrt(scalmul(xfd(1,3),xfd(1,4)))
              if(dd.lt..001) go to 2490
              call FeDeferOutput
              call FePlotMode('E')
              call FePolyLine(2,xu,yu,Green)
              call FePlotMode('N')
              xu(ip)=Xpos
              yu(ip)=Ypos
              if(LocatorType.eq.LocatorDistance) then
                i=ArrowFromTo
              else
                i=ArrowTo
              endif
              call FeArrow(xu(1),yu(1),xu(2),yu(2),LightYellow,i)
              if(LocatorType.eq.LocatorDistance) then
                write(Cislo,'(f8.3)') dd
                call FeWInfWrite(4,Cislo(:idel(Cislo)))
2110            call FeEvent(0)
                ByloCrw=.false.
                if(EventType.eq.EventMouse) then
                  if(EventNumber.eq.JeMove.or.EventNumber.eq.JeLeftUp
     1               .or.EventNumber.eq.JeRightUp) go to 2110
                else if(EventType.eq.EventKey) then
                  if(EventNumber.ne.JeEscape) go to 2110
                else if(EventType.eq.EventCrw) then
                  if(EventNumber.ne.nCrwLocDistance) then
                    call FeQuestCrwOff(nCrwLocDistance)
                    call FeQuestCrwOn(EventNumber)
                    CheckNumber=EventNumber
                    CheckType=EventCrw
                    NewLocatorType=EventNumber
                    EventNumber=0
                    EventType=0
                    ByloCrw=.true.
                  else
                    go to 2110
                  endif
                else if(EventType.eq.EventButton) then
                  CheckNumber=EventNumber
                  CheckType=EventButton
                  NewLocatorType=EventNumber
                  EventNumber=0
                  EventType=0
                  call FeQuestLblOff(nLblWinf(4))
                  call FeWInfClose(4)
                  JesteSeNepsalo=.true.
                  go to 2020
                else
                  go to 2110
                endif
              else
                if(.not.Obecny) then
                  ScopePlane(3)=0.
                  ShiftPlane(3)=0.
                endif
                do i=1,3
                  u(i)=xfd(i,2)-xfd(i,1)
                enddo
                call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
                uu=scalmul(u,ug)
                ScopePlaneNew=anint(100.*sqrt(uu))*.01
                ich=0
                id=NextQuestId()
                xqd=300.
                il=2
                Veta='Specify the new section'
                if(YPos.gt.YCenGrWin) then
                  ypom=YMinGrWin+5.
                else
                  ypom=YMaxGrWin-4.*QuestLineWidth-25.
                endif
                call FeQuestCreate(id,-1.,ypom,xqd,il,Veta,0,LightGray,
     1                             0,0)
                il=1
                tpom=5.
                Veta='%Width'
                dpom=80.
                xpom=tpom+FeTxLengthUnder(Veta)+10.
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwWidth=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,ScopePlaneNew,
     1                                  .false.,.false.)
                il=il+1
                Veta='%Depth'
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwDepth=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,ScopePlane(3),
     1                                  .false.,.false.)
                il=1
                tpom=xpom+dpom+25.
                Veta='%Angle'
                xpom=tpom+FeTxLengthUnder(Veta)+10.
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwAngle=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,Angle,.false.,
     1                                  .false.)
2250            call FeQuestEvent(id,ich)
                if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 2250
                endif
                if(ich.eq.0) then
                  ScopePlane(1)=ScopePlaneNew
                  call FeQuestRealFromEdw(nEdwWidth,ScopePlane(2))
                  call FeQuestRealFromEdw(nEdwDepth,ScopePlane(3))
                  call FeQuestRealFromEdw(nEdwAngle,Angle)
                  call FeQuestRemove(id)
                  ShiftPlane(1)=0.
                  ShiftPlane(2)=ScopePlane(2)*.5
                  ShiftPlane(3)=ScopePlane(3)*.5
                  call CopyVek(xfd(1,1),XPlane(1,1),3)
                  call CopyVek(xfd(1,2),XPlane(1,2),3)
                  do i=1,3
                    DxPlane(i,2)=XPlane(i,2)-XPlane(i,1)
                  enddo
                  if(Obecny) then
                    do i=1,3
                      v(i)=trobi(i,3)
                    enddo
                  else
                    call SetRealArrayTo(v,3,0.)
                    call SetRealArrayTo(w,3,0.)
                    v(iorien(1))=1.
                    w(iorien(2))=1.
                    call VecMul(v,w,vg)
                    call multm(MetTensI(1,nsubs,KPhase),vg,v,3,3,1)
                    DeltaPlane=1.
                    do i=1,3
                      DeltaPlane=min(DeltaPlane,dx(i)*CellParCon(i))
                    enddo
                    rad=10.**Exponent10(DeltaPlane)
                    DeltaPlane=anint(DeltaPlane/rad)*rad
                    DeltaPlane=max(.01,DeltaPlane)
                  endif
                  call VecMul(u,v,ug)
                  call multm(MetTensI(1,nsubs,KPhase),ug,u,3,3,1)
                  call multm(MetTens(1,nsubs,KPhase),v,vg,3,3,1)
                  call VecNor(u,ug)
                  call VecNor(v,vg)
                  sinp=sin(Angle*ToRad)
                  cosp=cos(Angle*ToRad)
                  do i=1,3
                    pom=cosp*u(i)+sinp*v(i)
                    XPlane(i,3)=XPlane(i,1)+pom
                    DxPlane(i,3)=pom
                  enddo
                  call SetIntArrayTo(SelPlane,3,2)
                  call SetStringArrayTo(StPlane,3,' ')
                  call FeQuestCrwOff(nCrwLocNewSection)
                  call FeQuestCrwOn(nCrwLocPosition)
                  LocatorType=LocatorPosition
                  NoOfDerivedMaps=NoOfDerivedMaps+1
                  CheckType=EventButton
                  CheckNumber=nButtNew
                  if(.not.obecny) then
                    obecny=.true.
                    kpdf=1
                  endif
                  go to 7000
                endif
                call FeQuestRemove(id)
              endif
2490          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                         SvFile,0)
              call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                         SvFile)
              ip=0
              if(LocatorType.eq.LocatorDistance) then
                call FeQuestLblOff(nLblWinf(4))
                call FeWInfClose(4)
                JesteSeNepsalo=.true.
              endif
              call FeReleaseOutput
              if(ByloCrw) then
                ByloCrw=.false.
                LocatorType=NewLocatorType
                go to 2010
              endif
            endif
          endif
        endif
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeMove) then
        if((Klic.eq.0.or.LocatorType.eq.LocatorDistance.or.
     1      LocatorType.eq.LocatorNewSection).and.ip.ge.1) then
          call FePlotMode('E')
          if(ip.gt.1) then
            call FePolyLine(2,xu,yu,Green)
          else if(ip.eq.1) then
            ip=2
          endif
        endif
        call GetCoord(xdo,xp,xfract)
        Xposp=FeXo2X(xdo(1))
        Yposp=FeYo2Y(xdo(2))
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          if(Klic.eq.0.or.
     1       ((LocatorType.eq.LocatorDistance.or.
     2         LocatorType.eq.LocatorNewSection).and.ip.ge.1)) then
            Xpos=Xposp
            Ypos=Yposp
            call FeMoveMouseTo(Xpos,Ypos)
          else
            if(JeUvnitr) then
              call FeMouseShape(0)
              call ConLocatorInfoClose
              irec=ireco
              call ConHeader
              JeUvnitr=.false.
            endif
          endif
        else
          if(Klic.ne.0) then
            if(.not.JeUvnitr) then
              call FeMouseShape(MouseType)
              ireco=irec
              irec=0
              call FeHeaderInfo(' ')
              call ConLocatorInfoOpen(xp,xfract)
              JeUvnitr=.true.
            endif
          endif
          Xpos=Xposp
          Ypos=Yposp
        endif
        if((klic.eq.0.or.
     1      LocatorType.eq.LocatorDistance.or.
     2      LocatorType.eq.LocatorNewSection).and.ip.ge.1) then
          xu(2)=Xpos
          yu(2)=Ypos
          if(ip.ne.0) then
            if(LocatorType.eq.LocatorDistance) then
              call CopyVek(xfract,xfd(1,2),3)
              do i=1,3
                xfd(i,3)=xfd(i,1)-xfd(i,2)
              enddo
              call multm(MetTens(1,nsubs,KPhase),xfd(1,3),xfd(1,4),3,3,
     1                   1)
              dd=sqrt(scalmul(xfd(1,3),xfd(1,4)))
              write(Cislo,'(f8.3)') dd
              call FePlotMode('N')
              if(JesteSeNepsalo) call FeQuestLblOn(nLblWinf(4))
              call FeWInfWrite(4,Cislo(:idel(Cislo)))
              JesteSeNepsalo=.true.
              call FePlotMode('E')
            endif
            call FePolyLine(2,xu,yu,Green)
          endif
          call FePlotMode('N')
        endif
        if(JeUvnitr) call ConLocatorInfoOut(xp,xfract)
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown)
     1  then
        go to 3100
      else if(CheckType.eq.EventCrw) then
        if(CheckNumber.eq.nCrwLocPosition) then
          MouseType=1
          LocatorType=LocatorPosition
          call FeWinfClose(4)
        else if(CheckNumber.eq.nCrwLocDistance) then
          MouseType=0
          LocatorType=LocatorDistance
          ndst=0
        else if(CheckNumber.eq.nCrwLocNewAtom) then
          MouseType=0
          LocatorType=LocatorNewAtom
          call FeWinfClose(4)
        else if(CheckNumber.eq.nCrwLocNewSection) then
          MouseType=0
          LocatorType=LocatorNewSection
          call FeWinfClose(4)
        endif
        go to 2000
      else if(CheckType.eq.EventButton.or.CheckType.eq.EventResize)
     1  then
        if(CheckNumber.eq.nButtNew.or.CheckNumber.eq.nButtQuit.or.
     1     CheckNumber.eq.nButtOptions) then
          if(NoOfDerivedMaps.eq.0.and..not.KeysSaved)
     1      call ConWriteKeys
          NoOfDerivedMaps=0
        endif
        go to 3100
      endif
      if(Klic.eq.0) then
        go to 2010
      else
        go to 2000
      endif
3100  TakeMouseMove=.false.
      call FePlotMode('N')
      call FeDeferOutput
      call ConLocatorInfoClose
3111  irec=ireco
      if(klic.ne.0..or.NCurve.le.1) then
        call FeMouseShape(0)
        go to 7000
      endif
      call FeQuestButtonLabelChange(nButtQuit,'%Continue')
      call FeQuestButtonDisable(nButtRun3dMaps)
      do i=nButtBasFr,nButtBasTo
        if(ActiveButtBas(i)) call FeQuestButtonClose(i)
      enddo
      if(VolaToContour) then
        call FeQuestButtonOpen(nButtYScale,ButtonOff)
        do i=nButtBasTo+2,nButtBasTo+3
          call FeQuestButtonOpen(i,ButtonDisabled)
        enddo
        j=5
      else
        j=4
      endif
      do i=3,j
        call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                         ContourYSep(i),LightGray,LightGray)
      enddo
      if(VolaToContour) call FeTwoPixLineHoriz(XMaxGrWin+1.,
     1                                         XMaxBasWin,ContourYSep(6)
     2                                        ,Gray,White)
      xomx=0.
      do i=1,NCurve
        call multm(O2F,XLocCurve(1,i),xp(1,i),3,3,1)
        call prevod(1,xp(1,i),XFractCurve(1,i))
        do j=1,2
          xp(j,i)=min(xp(j,i),xmax(j))
          xp(j,i)=max(xp(j,i),xmin(j))
        enddo
        call multm(F2O,xp(1,i),XLocCurve(1,i),3,3,1)
        if(i.gt.1) then
          do j=1,3
            xd(j,i-1)=(xp(j,i)-xp(j,i-1))/float(NExtPoints)
            xdo(j)=(XLocCurve(j,i)-XLocCurve(j,i-1))/float(NExtPoints)
          enddo
          dlk(i-1)=sqrt(scalmul(xdo,xdo))
          xomx=xomx+dlk(i-1)*float(NExtPoints)
        endif
      enddo
      if(allocated(gx)) deallocate(gx,gy,pty,sgymc,spty,dpty,stat=i)
      n=NExtPoints+1
      allocate(gx(n,9),gy(n,9),pty(n,9),sgymc(n,9),spty(n,9),dpty(n,9),
     1         stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PCurves" point#2.',i)
        go to 9000
      endif
      pom=0.
      ptymx=-99999.
      ptymn= 99999.
      do i=1,NCurve-1
        xs=xp(1,i)
        ys=xp(2,i)
        do j=1,NExtPoints+1
          gyji=ExtMap(xs,ys,ActualMap,NActualMap)+ContourRefLevel
          if(ErrMapActive) sgyji=ExtMap(xs,ys,ErrMap,NActualMap)
          if(DensityType.ne.2) then
            if(gyji.gt.0.) then
              pty(j,i)=toev*teplota*log(Dmax/gyji)
              if(ErrMapActive) spty(j,i)=toev*teplota*sgyji/gyji
            else
              pty(j,i)=50000.
              if(ErrMapActive) spty(j,i)=50000.
            endif
            ptymn=min(ptymn,pty(j,i))
            ptymx=max(ptymx,pty(j,i))
          endif
          gx(j,i)=pom
          gy(j,i)=gyji
          if(j.ne.NExtPoints+1) then
            xs=xs+xd(1,i)
            ys=ys+xd(2,i)
            pom=pom+dlk(i)
          endif
        enddo
      enddo
      xomn=0.
      yomn=Dmin+ContourRefLevel
      yomx=Dmax+ContourRefLevel
      nButtOn=0
4000  call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
4100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        if(HardCopy.eq.HardCopyNum)
     1    call ConMakeCurveLabel('density curve',XFractCurve,NCurve)
        call FeHeaderInfo('Density curve')
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'x')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'d [e/Ang^3]')
        do i=1,NCurve-1
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
          call FeXYPlot(gx(1,i),gy(1,i),NExtPoints+1,NormalLine,
     1                  NormalPlotMode,White)
          if(i.lt.NCurve-1) then
            xu(1)=FeXo2X(gx(NExtPoints+1,i))
            yu(1)=YMinAcWin
            xu(2)=xu(1)
            yu(2)=YMaxAcWin
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
            call FeLineType(NormalLine)
          endif
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
        enddo
        if(HardCopy.eq.HardCopyNum) then
          write(85,'(''# end of curve'')')
          write(85,'(''# end of file'')')
        endif
      endif
      call FeHardCopy(HardCopy,'close')
4250  if(nButtOn.ne.0) call FeButtonOff(nButtOn)
      nButtOn=0
      HardCopy=0
4300  call FeEvent(0)
      if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        nButtOn=EventNumber
        if(EventNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 4100
          else
            HardCopy=0
            go to 4250
          endif
        else if(EventNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 4100
          else
            HardCopy=0
            go to 4250
          endif
        else if(EventNumber.eq.nButtQuit) then
          call FeQuestButtonOff(EventNumber)
          if(VolaToContour) then
            if(ExistFile(fln(:ifln)//'.m85').and.kpdf.ge.2)
     1        call FeQuestButtonOff(nButtErrOn)
            call FeQuestButtonOff(nButtDerOn)
            call FeQuestButtonLabelChange(nButtQuit,'%Return')
          endif
          call FeQuestButtonOff(EventNumber)
          nButtOn=0
          go to 4400
        else if(EventNumber.eq.nButtYScale) then
          call ConReadDenLimits(yomn,yomx,ich)
          if(ich.ne.0) go to 4250
          go to 4000
        endif
      endif
      go to 4300
4400  if(DensityType.eq.2) then
        EventType=0
        EventNumber=0
        go to 7000
      endif
      yomn=0.
      yomx=min(1000.,ptymx)
      HardCopy=0
      call FeClearGrWin
      dptymx=-99999.
      do i=1,NCurve-1
        do j=2,NExtPoints
          if(pty(j+1,i).lt.49999..and.pty(j-1,i).lt.49999.) then
            dpty(j,i)=abs(pty(j+1,i)-pty(j-1,i))/(2.*dlk(i))
            if(dpty(j,i).gt.dptymx) dptymx=dpty(j,i)
          else
            dpty(j,i)=-1.
          endif
        enddo
        if(pty(1,i).lt.49999..and.pty(2,i).lt.49999.) then
          dpty(1,i)=abs(pty(2,i)-pty(1,i))/dlk(i)
          if(dpty(1,i).gt.dptymx) dptymx=dpty(1,i)
        else
          dpty(1,i)=-1.
        endif
        if(pty(NExtPoints,i)  .lt.49999..and.
     1     pty(NExtPoints+1,i).lt.49999.) then
          dpty(NExtPoints+1,i)=
     1      abs(pty(NExtPoints+1,i)-pty(NExtPoints,i))/dlk(i)
          if(dpty(NExtPoints+1,i).gt.dptymx)
     1      dptymx=dpty(NExtPoints+1,i)
        else
          dpty(NExtPoints+1,i)=-1.
        endif
      enddo
4500  call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        if(HardCopy.eq.HardCopyNum)
     1    call ConMakeCurveLabel('potential curve',XFractCurve,NCurve)
        call FeHeaderInfo('Potential curve')
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'x')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'p[meV]')
        do i=1,NCurve-1
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
          call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                  NormalPlotMode,White)
          if(i.lt.NCurve-1) then
            xu(1)=FeXo2X(gx(NExtPoints+1,i))
            yu(1)=YMinAcWin
            xu(2)=xu(1)
            yu(2)=YMaxAcWin
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
            call FeLineType(NormalLine)
          endif
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
        enddo
        if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        if(IncErr) then
          if(HardCopy.eq.HardCopyNum)
     1      call ConMakeCurveLabel('Error curve',XFractCurve,NCurve)
          do i=1,NCurve-1
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
            call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                    NormalPlotMode,White)
            pom=yomx/dptymx
            do j=1,NExtPoints+1
              if(IncErr.and..not.ErrMapActive) then
                gyji=gy(j,i)
                if(gyji.gt.0.) then
                  spty(j,i)=toev*teplota*sgymc(j,i)/gyji
                else
                  spty(j,i)=50000.
                endif
              endif
            enddo
            call FeXYPlot(gx(1,i),spty(1,i),NExtPoints+1,DashedLine,
     1                    NormalPlotMode,Green)
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
          enddo
          if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        endif
        if(IncDer) then
          if(HardCopy.eq.HardCopyNum)
     1      call ConMakeCurveLabel('Derivative curve',XFractCurve,
     2                             NCurve)
          do i=1,NCurve-1
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
            call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                    NormalPlotMode,White)
            pom=yomx/dptymx
            do j=1,NExtPoints+1
              if(dpty(j,i).ge.0.) then
                dpty(j,i)=dpty(j,i)*pom
              else
               dpty(j,i)=1.01*yomx
              endif
            enddo
            dptymx=yomx
            call FeXYPlot(gx(1,i),dpty(1,i),NExtPoints+1,DashedLine,
     1                    NormalPlotMode,Red)
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
          enddo
          if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        endif
        if(HardCopy.eq.HardCopyNum) write(85,'(''# end of file'')')
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
4750  if(nButtOn.ne.0) call FeButtonOff(nButtOn)
      nButtOn=0
4800  call FeEvent(0)
4805  if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        nButtOn=EventNumber
        if(EventNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 4500
          else
            HardCopy=0
            go to 4750
          endif
        else if(EventNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 4500
          else
            HardCopy=0
            go to 4750
          endif
        else if(EventNumber.eq.nButtQuit) then
          call FeQuestButtonOff(EventNumber)
          call FeQuestButtonLabelChange(nButtQuit,'%Quit')
          EventType=0
          EventNumber=0
          go to 7000
        else if(EventNumber.eq.nButtYScale) then
          teplold=teplota
          call ConReadPotLimits(yomn,yomx,teplota,ich)
          if(ich.ne.0) go to 4750
          do i=1,NCurve-1
            do j=1,NExtPoints+1
              pty(j,i)=pty(j,i)*teplota/teplold
              if(ErrMapActive) spty(j,i)=spty(j,i)*teplota/teplold
            enddo
          enddo
          go to 4500
        else if(EventNumber.eq.nButtderOn) then
          Veta=Men1(2)
          Men1(2)=Men1(4)
          Men1(4)=Veta
          IncDer=.not.IncDer
          if(IncDer) then
            Veta='%Der OFF'
          else
            Veta='%Der ON'
          endif
          call FeQuestButtonLabelChange(nButtderOn,Veta)
          go to 4500
        else if(EventNumber.eq.nButtErrOn) then
          if(.not.IncErr.and..not.ErrMapActive)
     1      call rezmc(XFractCurve,NCurve,NExtPoints+1,gy,sgymc,*4750)
          Veta=Men1(3)
          Men1(3)=Men1(5)
          Men1(5)=Veta
          IncErr=.not.IncErr
          if(IncErr) then
            Veta='%Err OFF'
          else
            Veta='%Err ON'
          endif
          call FeQuestButtonLabelChange(nButtErrOn,Veta)
          go to 4500
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        j=FeMenu(-1.,-1.,men1,1,3,1,0)
        EventType=EventButton
        EventNumber=nButtYScale
        go to 4805
      endif
      go to 4800
7000  call FeMakeAcWin(40.,20.,30.,30.)
      if(VolaToContour) then
        call SetTr
      else
        call GrtSetTr
      endif
      if(klic.eq.1) go to 9000
      call FeQuestButtonLabelChange(nButtQuit,'%Quit')
      reconfig=.true.
      if(IncDer) then
        Veta=Men1(2)
        Men1(2)=Men1(4)
        Men1(4)=Veta
      endif
      if(IncErr) then
        Veta=Men1(3)
        Men1(3)=Men1(5)
        Men1(5)=Veta
      endif
9000  call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile,
     1                   0)
      if(klic.eq.0) then
        do i=nButtBasTo+1,nButtBasTo+3
          call FeQuestButtonClose(i)
        enddo
        do i=nButtBasFr,nButtBasTo
          if(ActiveButtBas(i)) call FeQuestButtonOpen(i,ButtonOff)
        enddo
        do i=3,5
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                           ContourYSep(i),Gray,White)
        enddo
        call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                         ContourYSep(6),LightGray,LightGray)
      endif
      AllowChangeMouse=.true.
      QuestVystraha=.true.
      if(allocated(ErrMap)) deallocate(ErrMap,stat=i)
      if(allocated(gx)) deallocate(gx,gy,pty,sgymc,spty,dpty,stat=i)
      return
100   format(3f10.4)
101   format(3f12.6)
103   format(f10.2)
      end
      subroutine ConLocatorInfo
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(*),xfract(3)
      character*80 t80
      character*7  FormatLocal
      integer Exponent10
      save FormatLocal
      entry ConLocatorInfoOpen(xp,xfract)
      i=max(4-Exponent10(max(xmax(1),xmax(2))),0)
      i=min(i,4)
      write(FormatLocal,'(''(2f7.'',i1,'')'')') i
      call FeHeaderInfo(ConWinfLabel(1))
      call FeQuestLblOn(nLblWinf(3))
      do i=1,3
        if(i.eq.1) then
          write(t80,FormatLocal)(xp(j),j=1,2)
        else if(i.eq.2) then
          write(t80,102) xfract
        else
          write(t80,103)
     1      ExtMap(xp(1),xp(2),ActualMap,NActualMap)+ContourRefLevel
        endif
        call FeWInfWrite(i,t80)
      enddo
      go to 9999
      entry ConLocatorInfoOut(xp,xfract)
      do i=1,3
        if(i.eq.1) then
          write(t80,FormatLocal)(xp(j),j=1,2)
        else if(i.eq.2) then
          write(t80,102) xfract
        else
          write(t80,103)
     1      ExtMap(xp(1),xp(2),ActualMap,NActualMap)+ContourRefLevel
        endif
        call FeWInfWrite(i,t80)
      enddo
      go to 9999
      entry ConLocatorInfoClose
      do i=1,3
        call FeWInfClose(i)
      enddo
      call FeHeaderInfo(' ')
      call FeQuestLblOff(nLblWinf(3))
9999  return
102   format(3f7.4)
103   format(f10.3)
      end
      subroutine ConSaveSelPoint(xs)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xs(*)
      character*256 EdwStringQuest
      character*80 Veta
      logical EqIgCase,FeYesNoHeader
      ich=0
      id=NextQuestId()
      xqd=320.
      il=1
      call FeQuestCreate(id,-1.,YBottomMessage,xqd,il,' ',1,LightGray,0,
     1                   0)
      il=1
      Veta='Define an identification label of the point:'
      tpom=5.
      xpom=tpom+FeTxLength(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEwdLabel=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      ypom=YCenBasWin-105.
      kam=0
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        Veta=EdwStringQuest(nEwdLabel)
        if(Veta.eq.' ') then
          call FeChybne(-1.,ypom,'the label cannot be an empty string.',
     1                  ' ',SeriousError)
          go to 1600
        else
          do i=1,NSavedPoints
            if(EqIgCase(Veta,StSavedPoint(i))) then
              NInfo=1
              TextInfo(1)='The label "'//Veta(:idel(Veta))//
     1                    '" has been already used.'
              if(FeYesNoHeader(-1.,ypom,'Do you want to overwrite it?',
     1                        0)) then
                kam=i
              else
                go to 1600
              endif
            endif
          enddo
        endif
        QuestCheck(id)=0
        go to 1500
1600    call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        NewPoints=.true.
        if(kam.le.0) then
          if(NSavedPoints.ge.MaxSavedPoints)
     1      call ReallocateSavedPoints(MaxSavedPoints+10)
          NSavedPoints=NSavedPoints+1
          kam=NSavedPoints
          StSavedPoint(kam)=Veta
        endif
        call CopyVek(xs,XSavedPoint(1,kam),3)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine ConSetPointCurve(String,n,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) String
      dimension xdo(3),xp(3)
      if(String.eq.' ') go to 9000
      call CtiAt(String,xdo,ich)
      if(ich.gt.0) go to 9999
      if(ich.eq.-1) then
        AtCurve(n)=String
      else
        AtCurve(n)=' '
      endif
      if(XTypeCurve.eq.1.and.AtCurve(n).eq.' ') then
!        xdo(3)=0.
        call CopyVek(xdo,XLocCurve(1,n),3)
        call multm(O2F,XLocCurve(1,n),xp,3,3,1)
        do j=1,3
          xp(j)=min(xp(j),xmax(j))
          xp(j)=max(xp(j),xmin(j))
        enddo
        call multm(F2O,xp,XLocCurve(1,n),3,3,1)
        call prevod(1,xp,XFractCurve(1,n))
      else
        call CopyVek(xdo,XFractCurve(1,n),3)
        call prevod(0,XFractCurve(1,n),xp)
        do j=1,3
          xp(j)=min(xp(j),xmax(j))
          xp(j)=max(xp(j),xmin(j))
        enddo
        call multm(F2O,xp,XLocCurve(1,n),3,3,1)
      endif
9000  ich=0
9999  return
      end
      subroutine ConMakeCurveLabel(Label,xpoints,n)
      dimension xpoints(3,n)
      character*(*) Label
      character*2   nty
      write(85,'(''# '',80a1)')(Label(i:i),i=1,idel(Label))
      do i=1,n-1
        if(n.gt.2) write(85,'(''# '',i2,a2,'' segment'')') i,nty(i)
        write(85,'(''# from : '',3f10.6)')(xpoints(j,i  ),j=1,3)
        write(85,'(''# to   : '',3f10.6)')(xpoints(j,i+1),j=1,3)
      enddo
      return
      end
      subroutine ConReadDenLimits(pmn,pmx,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*40 Veta
      id=NextQuestId()
      xqd=180.
      call FeQuestCreate(id,-1.,-1.,xqd,2,'Limits for the density '//
     1                   'curve',0,LightGray,0,0)
      Veta='D(m%in)'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      dpom=100.
      il=1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,pmn,.false.,.false.)
      il=il+1
      Veta='D(m%ax)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,pmx,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwMin,pmn)
        call FeQuestRealFromEdw(nEdwMax,pmx)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine GetCoord(xdo,xp,xfract)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xdo(3),xfract(3)
      xdo(1)=FeX2Xo(Xpos)
      xdo(2)=FeY2Yo(Ypos)
      xdo(3)=0.
      call multm(O2F,xdo,xp,3,3,1)
      xp(3)=zmap(1)
      do i=1,2
        xp(i)=max(xp(i),xmin(i))
        xp(i)=min(xp(i),xmax(i))
      enddo
      call multm(F2O,xp,xdo,3,3,1)
      if(xyzmap) then
        call prevod(1,xp,xfract)
      else
        call SetRealArrayTo(xfract,3,0.)
        do i=1,NDim(KPhase)
          j=iorien(i)
          if(j.le.3) then
            if(i.le.2) then
              xfract(j)=xp(i)
            else
              xfract(j)=zmap(i-2)
            endif
          endif
        enddo
      endif
      return
      end
      subroutine ConReadPotLimits(pmn,pmx,teplota,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pmp(2)
      character*40 Veta
      pmp(1)=pmn
      pmp(2)=pmx
      id=NextQuestId()
      xqd=200.
      call FeQuestCreate(id,-1.,-1.,xqd,3,'Limits for potential '//
     1                   'curve',0,LightGray,0,0)
      il=0
      Veta='Mi%n in [meV]'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,pmp(i),.false.,.false.)
        if(i.eq.1) then
          nEdwMin=EdwLastMade
          Veta='Ma%x in [meV]'
        else
          nEdwMax=EdwLastMade
        endif
      enddo
      il=il+1
      Veta='%Temperature'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwT=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,teplota,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwMin,pmn)
        call FeQuestRealFromEdw(nEdwMax,pmx)
        call FeQuestRealFromEdw(nEdwT,Teplota)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine rezmc(xp,n,m,gy,sgy,*)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      logical konec
      real, allocatable :: gyp(:,:)
      dimension xp(3,9),xd(3),xx(3),gy(m,9),sgy(m,9)
      if(allocated(gyp)) deallocate(gyp)
      allocate(gyp(m,9))
      call ConReadM85
      call ConDefMonteCarlo(ich)
      if(ich.ne.0) go to 9000
      pom=ran1(n*nxny)
      xdp=1./float(m-1)
      write(t80,101) 0,100.
      call FeOpenInterrupt(-1.,-1.,t80)
      konec=.false.
      do iy=1,n-1
        do ix=1,m
          sgy(ix,iy)=0.
        enddo
      enddo
      k=0
1500  k=k+1
      if(k.gt.mcmax) go to 5100
      call ConHitMC
      do iy=1,n-1
        do ix=1,m
          gyp(ix,iy)=0.
        enddo
      enddo
      do ia=1,npdf
        ip=ipor(ia)
        if(fpdf(ip).le.0.) cycle
        xx(1)=pdfat(xa(1,1),0.,ia,0,ich)
        if(ich.ne.0) then
          k=k-1
          go to 1500
        endif
        do iy=1,n-1
          do i=1,3
            xd(i)=(xp(i,iy+1)-xp(i,iy))*xdp
          enddo
          do i=1,3
            xx(i)=xp(i,iy)
          enddo
          do ix=1,m
            gyp(ix,iy)=gyp(ix,iy)+pdfat(xx,0.,ia,1,ich)*fpdf(ip)
            if(ich.ne.0) cycle
            do i=1,3
              xx(i)=xx(i)+xd(i)
            enddo
          enddo
        enddo
      enddo
      if(.not.konec) call FeEventInterrupt(Konec)
      pom1=1./float(k)
      pom2=pom1*float(k-1)
      error1=0.
      error2=0.
      do iy=1,n-1
        do ix=1,m
          pom=(gyp(ix,iy)-gy(ix,iy))**2*pom1
          if(ix.ne.1.or.iy.eq.1) then
            error1=error1+abs(sgy(ix,iy)*pom1-pom)
            error2=error2+sgy(ix,iy)
          endif
          sgy(ix,iy)=pom2*sgy(ix,iy)+pom
        enddo
      enddo
      if(error2.gt.0.) then
        error=min(sqrt(error1/error2)*100.,100.)
      else
        error=100.
      endif
      if(error.lt.errlev.or.konec) go to 5100
      if(mod(k,100).eq.0.or.k.eq.1) then
        write(t80,101) k,error
        call FeOutputInterrupt(t80)
      endif
      go to 1500
5100  write(t80,101) k,error
      call FeCloseInterrupt
      do iy=1,n-1
        do ix=1,m
          sgy(ix,iy)=sqrt(sgy(ix,iy))
        enddo
      enddo
      call obnova
      go to 9999
9000  mcmax=0
      if(allocated(gyp)) deallocate(gyp)
      return1
9999  if(allocated(gyp)) deallocate(gyp)
      return
101   format('Monte Carlo hits :',i5,' Error :',f5.1,'%')
      end
      subroutine ConDefMonteCarlo(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*40 Veta
      if(mcmax.eq.0) mcmax=1000
      id=NextQuestId()
      xqd=250.
      call FeQuestCreate(id,-1.,-1.,xqd,2,'Parameters for Monte '//
     1                   'Carlo calculation',0,LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Number of iteration steps'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNSteps=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mcmax,.false.)
      il=il+1
      Veta='A%ccuracy limit in %'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ErrLev,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNSteps,mcmax)
        call FeQuestRealFromEdw(nEdwLimit,ErrLev)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine ConSearch
      use contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xt(3),xv(3,10),xemx(:,:),xemn(:,:),dmx(:),dmn(:),
     1          idm(:),ipm(:),xmo(:,:),dmo(:)
      character*11 :: FileNameTmp(2)=(/'_maxima.tmp','_minima.tmp'/)
      character*8  SvFile
      allocatable xemx,xemn,dmx,dmn,idm,ipm,xmo,dmo
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      call FeSaveImage(XMinBasWin+2.,XMaxBasWin-40.,YMinGrWin,
     1                 YMaxBasWin-14.,SvFile)
      n=0
      ip=0
      TakeMouseMove=.true.
      call FeMoveMouseTo(XCenGrWin,YCenGrWin)
1000  if(DeferredOutput) call FeReleaseOutput
1005  call FeEvent(1)
      if(EventNumber.eq.0) go to 1005
      call FeMouseShape(0)
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown) then
        n=n+1
        if(ip.eq.1) then
          call FePlotMode('E')
          call FePolyLine(5,xu,yu,Green)
          yu(2)=Ypos
          xu(3)=Xpos
          yu(3)=Ypos
          xu(4)=Xpos
          call FePlotMode('N')
          call FePolyLine(5,xu,yu,Green)
          xv(1,n)=Xpos
          xv(2,n)=Ypos
          xv(3,n)=0.
          ip=0
        else
          ip=ip+1
          do i=1,5
            xu(i)=Xpos
            yu(i)=Ypos
          enddo
          xv(1,n)=Xpos
          xv(2,n)=Ypos
          xv(3,n)=0.
        endif
        if(n.ge.10) then
          call FeChybne(-1.,-1.,'the maximum of 5 rectangules reached.',
     1                  ' ',SeriousError)
          go to 1100
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove.and.
     1  ip.ne.0) then
        call FePlotMode('E')
        call FePolyLine(5,xu,yu,Green)
        yu(2)=Ypos
        xu(3)=Xpos
        yu(3)=Ypos
        xu(4)=Xpos
        call FePolyLine(5,xu,yu,Green)
        call FePlotMode('N')
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        if(ip.eq.1) then
          call FePlotMode('E')
          call FePolyLine(5,xu,yu,Green)
        endif
        go to 1100
      endif
      go to 1000
1100  call FePlotMode('N')
      if(n.le.1) go to 9999
      do i=1,n-1,2
        if(xv(1,i).gt.xv(1,i+1)) then
          pom=xv(1,i)
          xv(1,i)=xv(1,i+1)
          xv(1,i+1)=pom
        endif
        if(xv(2,i).gt.xv(2,i+1)) then
          pom=xv(2,i)
          xv(2,i)=xv(2,i+1)
          xv(2,i+1)=pom
        endif
      enddo
      it=0
      nmx=0
      nmn=0
      xt(3)=zmap(1)
      xt(2)=xmin(2)
      MxPeak=10
      MnPeak=10
      allocate(xemx(3,MxPeak),xemn(3,MnPeak),dmx(MxPeak),dmn(MnPeak))
      do iy=1,nx(2)
        xt(1)=xmin(1)
        do ix=1,nx(1)
          it=it+1
          if(ix.eq.1.or.ix.eq.nx(1)) go to 1890
          if(iy.eq.1.or.iy.eq.nx(2)) go to 1890
          call FeXf2X(xt,xo)
          x1=xo(1)
          y1=xo(2)
          do i=1,n-1,2
            if(x1.ge.xv(1,i).and.x1.le.xv(1,i+1).and.
     1         y1.ge.xv(2,i).and.y1.le.xv(2,i+1)) go to 1300
          enddo
          go to 1890
1300      tbl=ActualMap(it)
          if(tbl.lt.ActualMap(it-1)) go to 1400
          if(tbl.le.ActualMap(it+1)) go to 1400
          if(tbl.lt.ActualMap(it-nx(1))) go to 1400
          if(tbl.le.ActualMap(it+nx(1))) go to 1400
          if(tbl.lt.ActualMap(it-nx(1)-1)) go to 1400
          if(tbl.le.ActualMap(it+nx(1)+1)) go to 1400
          if(tbl.lt.ActualMap(it-nx(1)+1)) go to 1400
          if(tbl.le.ActualMap(it+nx(1)-1)) go to 1400
          call vrchol(xt(1),dx(1),ActualMap(it-1),tbl,ActualMap(it+1),
     1                x1,pom1)
          call vrchol(xt(2),dx(2),ActualMap(it-nx(1)),tbl,
     1                ActualMap(it+nx(1)),x2,pom2)
          pom=(pom1+pom2)*.5
          if(pom.le.0.) go to 1400
          if(nmx.ge.MxPeak) then
            allocate(xmo(3,nmx),dmo(nmx))
            do i=1,nmx
              dmo(i)=dmx(i)
              call CopyVek(xemx(1,i),xmo(1,i),3)
            enddo
            deallocate(xemx,dmx)
            MxPeak=MxPeak+10
            allocate(xemx(3,MxPeak),dmx(MxPeak))
            do i=1,nmx
              dmx(i)=dmo(i)
              call CopyVek(xmo(1,i),xemx(1,i),3)
            enddo
            deallocate(xmo,dmo)
          endif
          nmx=nmx+1
          dmx(nmx)=pom
          xemx(1,nmx)=x1
          xemx(2,nmx)=x2
          xemx(3,nmx)=zmap(1)
          go to 1890
1400      if(tbl.gt.ActualMap(it-1)) go to 1890
          if(tbl.ge.ActualMap(it+1)) go to 1890
          if(tbl.gt.ActualMap(it-nx(1))) go to 1890
          if(tbl.ge.ActualMap(it+nx(1))) go to 1890
          if(tbl.gt.ActualMap(it-nx(1)-1)) go to 1890
          if(tbl.ge.ActualMap(it+nx(1)+1)) go to 1890
          if(tbl.gt.ActualMap(it-nx(1)+1)) go to 1890
          if(tbl.ge.ActualMap(it+nx(1)-1)) go to 1890
          call vrchol(xt(1),dx(1),ActualMap(it-1),tbl,ActualMap(it+1),
     1                x1,pom1)
          call vrchol(xt(2),dx(2),ActualMap(it-nx(1)),tbl,
     1                ActualMap(it+nx(1)),x2,pom2)
          pom=(pom1+pom2)*.5
          if(pom.ge.0.) go to 1890
          if(nmn.ge.MnPeak) then
            allocate(xmo(3,nmn),dmo(nmn))
            do i=1,nmn
              dmo(i)=dmn(i)
              call CopyVek(xemn(1,i),xmo(1,i),3)
            enddo
            deallocate(xemn,dmn)
            MnPeak=MnPeak+10
            allocate(xemn(3,MnPeak),dmn(MnPeak))
            do i=1,nmn
              dmn(i)=dmo(i)
              call CopyVek(xmo(1,i),xemn(1,i),3)
            enddo
            deallocate(xmo,dmo)
          endif
          nmn=nmn+1
          dmn(nmn)=pom
          xemn(1,nmn)=x1
          xemn(2,nmn)=x2
          xemn(3,nmn)=zmap(1)
1890      xt(1)=xt(1)+dx(1)
        enddo
        xt(2)=xt(2)+dx(2)
      enddo
      n=max(nmx,nmn)
      allocate(idm(n),ipm(n))
      do i=1,2
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//FileNameTmp(i),'formatted',
     1                'unknown')
        if(i.eq.1) then
          do j=1,nmx
            idm(j)=-dmx(j)*100.
          enddo
          call indexx(nmx,idm,ipm)
          do j=1,nmx
            m=ipm(j)
            call Prevod(1,xemx(1,m),xt)
            write(ln,100)(xemx(k,m),k=1,3),xt,dmx(m)
          enddo
        else
          do j=1,nmn
            idm(j)=dmn(j)*100.
          enddo
          call indexx(nmn,idm,ipm)
          do j=1,nmn
            m=ipm(j)
            call Prevod(1,xemn(1,m),xt)
            write(ln,100)(xemn(k,m),k=1,3),xt,dmn(m)
          enddo
        endif
        call CloseIfOpened(ln)
      enddo
      call ConShowMaxMin(FileNameTmp)
      call FeLoadImage(XMinBasWin+2.,XMaxBasWin-40.,YMinGrWin,
     1                 YMaxBasWin-14.,SvFile,0)
9999  TakeMouseMove=.false.
      EventType=0
      EventNumber=0
      if(allocated(xemx))
     1  deallocate(xemx,xemn,dmx,dmn)
      if(allocated(idm)) deallocate(idm,ipm)
      call FeReleaseOutput
      return
100   format(3f8.3,2x,3f8.4,f10.2)
      end
      subroutine ConSearchGlobal
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),SaveMap(:)
      character*80 Veta,t80
      character*11 :: FileNameTmp(2)=(/'_maxima.tmp','_minima.tmp'/)
      character*8 at
      character*6 :: MinMaxLabel(2)=(/'maxima','minima'/)
      logical UzPsal
      allocatable SaveMap
      allocate(SaveMap(nxny))
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyVek(ActualMap,SaveMap,nxny)
      call DefaultFourier
      call ConFouPeaks(m8)
      if(ErrFlag.ne.0) then
        write(Cislo,FormI15) m8
        call Zhusti(Cislo)
        call OpenMaps(m8,fln(:ifln)//'.l'//Cislo(:idel(Cislo)),nxny,0)
        go to 9999
      endif
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      ln=NextLogicNumber()
      do i=1,2
        UzPsal=.false.
        if(i.eq.1) then
          t80='Fourier maxima'
        else
          t80='Fourier minima'
        endif
        call OpenFile(ln,fln(:ifln)//FileNameTmp(i),'formatted',
     1                'unknown')
1100    read(m40,FormA80,end=1250) Veta
        if(Veta(1:10).ne.'----------'.or.
     1    LocateSubstring(Veta,t80(:idel(t80)),.false.,.true.).le.0)
     2    go to 1100
        read(m40,FormA,end=1250) Veta
1200    read(m40,FormA) Veta
        if(Veta(1:10).eq.'----------') go to 1250
        read(Veta,101,err=1250,end=1250) At,isfp,itfp,aip,xo,(l,k=1,5)
        do j=1,l+1
          read(m40,FormA) Veta
        enddo
        if(LocateSubstring(Veta,'e',.false.,.true.).gt.0) then
          read(Veta,'(2e9.3)',err=1250,end=1250) rho,rho
        else
          read(Veta,'(2f9.3)',err=1250,end=1250) rho,rho
        endif
        if(Obecny) then
          call Prevod(1,xo,xp)
        else
          call CopyVek(xo,xp,3)
          call Prevod(0,xp,xo)
        endif
        write(ln,103) xo,xp,rho
        UzPsal=.true.
        go to 1200
1250    if(.not.UzPsal) then
          Veta=MinMaxLabel(i)
          write(ln,'(''No '',a,'' found in whole region'')')
     1      Veta(:idel(Veta))
        endif
        call CloseIfOpened(ln)
        rewind m40
      enddo
      call ConShowMaxMin(FileNameTmp)
9999  call CloseIfOpened(m40)
      call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      call CopyVek(SaveMap,SaveMap,nxny)
      call DefaultBasicContour
      deallocate(SaveMap)
      return
100   format(i5,15x,i5)
101   format(a8,2i3,4x,4f9.6,6x,3i1,3i3)
102   format(a9)
103   format(3f8.3,2x,3f8.4,f10.2)
      end
      subroutine ConShowMaxMin(FileNameTmp)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileNameTmp(2)
      character*80 Veta
      character*6 :: MinMaxLabel(2)=(/'Maxima','Minima'/)
      id=NextQuestId()
      xqd=450.
      il=16
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
      il=1
      tpom=xqd*.5
      call FeQuestLblMake(id,tpom,il,MinMaxLabel(1),'C','B')
      nLblMinMax=LblLastMade
      il=il+1
      tpom=70.
      Veta='Local'
      do i=1,3
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        if(i.eq.1) then
          Veta='Fractional'
          tpom=tpom+140.
        else if(i.eq.2) then
          Veta='Charge'
          tpom=tpom+125.
        endif
      enddo
      xpom=5.
      il=14
      call FeQuestSbwMake(id,xpom,il,xqd-2.*xpom-SbwPruhXd,12,1,
     1                    CutTextFromRight,SbwVertical)
      nSbwText=SbwLastMade
      il=15
      xpom=7.
      tpom=xpom+CrwgXd+10.
      Veta='Show ma%xima'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      il=il+1
      call FeQuestCrwOpen(CrwLastMade,.true.)
      Veta='Show mi%nima'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      call FeQuestCrwOpen(CrwLastMade,.false.)
      KteryFile=1
2200  Veta=fln(:ifln)//FileNameTmp(KteryFile)
      call FeQuestSbwTextOpen(nSbwText,20,Veta)
      call FeQuestLblChange(nLblMinMax,MinMaxLabel(KteryFile))
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        call CloseIfOpened(SbwLn(nSbwText))
        KteryFile=3-KteryFile
        go to 2200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine ConFouPeaks(m8)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ior(6)
      call CopyVekI(iorien,ior,6)
      npeaks(1)=50
      npeaks(2)=50
      call OpenFile(lst,fln(:ifln)//'_peaks.lst','formatted','unknown')
      YMinFlowChart=-1.
      call FouPeaks(0,m8)
      call CopyVekI(ior,iorien,6)
      close(lst,status='delete')
      return
      end
      function ExtMap(xs,ys,tabp,nmx)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension tabp(*)
      gnx=(xs-xmin(1))/dx(1)+1.
      gny=(ys-xmin(2))/dx(2)+1.
      ngx=gnx
      ngy=gny
      ngxy=(ngy-1)*nx(1)+ngx
      gnx=gnx-float(ngx)
      gny=gny-float(ngy)
      gnx1=1.-gnx
      gny1=1.-gny
      ExtMap=0.
      if(ngxy.le.nmx) ExtMap=ExtMap+tabp(ngxy)*gnx1*gny1
      i=ngxy+1
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx*gny1
      i=ngxy+1+nx(1)
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx*gny
      i=ngxy+nx(1)
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx1*gny
      return
      end
      subroutine SetContours(klic)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer Exponent10
      fmz=max(Dmax,abs(Dmin))*.1
      rad=10.**Exponent10(fmz)
      fmz=fmz/rad
      if(fmz.lt.1.5) then
        fmz=1.
      else if(fmz.lt.3.5) then
        fmz=2.
      else if(fmz.lt.7.5) then
        fmz=5.
      else
        fmz=10.
      endif
      fmz=fmz*rad
      fmzp=fmz
      fmzn=fmz
      cutpos= Dmax
      cutneg= Dmin
      if(klic.eq.0) ContourType=ContourTypeLinear
      return
      end
      subroutine DefaultContour
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      Klic=0
      go to 1000
      entry DefaultBasicContour
      Klic=1
1000  NactiInt=10
      NactiReal=10
      NactiComposed=20
      NactiKeys=NactiInt+NactiReal+NactiComposed
      call CopyVekI(DefIntContour,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntContour,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntContour,CmdIntContour,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealContour(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealContour(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealContour(i),CmdRealContour, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdContour(i)
      enddo
      if(Klic.eq.1) go to 9999
      DrawAtN=0
      XTypeCurve=1
      NCurve=0
      ConDrawDepth=0.
      call SetStringArrayTo(AtCurve,10,' ')
      call SetIntArrayTo(SelPlane,3,0)
      call SetRealArrayTo(XPlane,9,-1111.)
      call SetRealArrayTo(DxPlane,6,-1111.)
      DeltaPlane=-1111.
      call SetStringArrayTo(StPlane,3,' ')
      call SetRealArrayTo(ScopePlane,2,2.)
      ScopePlane(3)=0.
      do i=1,3
        ShiftPlane(i)=.5*ScopePlane(i)
      enddo
      NumberOfPlanes=0
      NumberOfFigs=0
9999  return
      end
      subroutine NactiContour
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call Najdi('contour',i)
      if(i.ne.1) go to 9999
      PreskocVykricnik=.true.
      call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      izpet=0
      NumberOfPlanes=0
      NumberOfPlanesMax=0
1100  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0) go to 9999
1110  if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntContour ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealContour,NactiReal)
        go to 9999
      else if(izpet.eq.nCmdLabelPlane) then
        call ConReallocatePlanes
        NumberOfPlanes=NumberOfPlanes+1
        LabelPlane(NumberOfPlanes)=NactiVeta(PosledniPozice+1:)
        PosledniPozice=len(NactiVeta)+1
      endif
      if(izpet.ne.0) go to 1100
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      return
102   format(f15.5)
      end
      subroutine ConFillAtoms(ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      character*80  Veta
      dimension xp(3),xpp(3),ddx(3),gddx(3),io(3)
      integer IAtomFill(:),SbwLnQuest,RolMenuSelectedQuest,
     1        SbwItemSelQuest
      logical BratAtomFill(:),lpom,EqWild,BackToAdvanced
      external ConFillAtomsCheck,FeVoid
      allocatable BratAtomFill,IAtomFill
      CutOffDistOld=CutOffDist
      if(Obecny) then
        ir=2
        do i=1,3
          io(i)=i
        enddo
      else
        ir=1
        call CopyVekI(iorien,io,3)
      endif
      dpom=xmaxa(3,ir)-xmina(3,ir)
      if(ir.eq.1) then
        j=1+4*(io(3)-1)
        dpom=dpom*sqrt(MetTens(j,nsubs,KPhase))
      endif
      if(ConDrawDepth.le.0.) then
        if(dpom.lt..2) then
          Depth=dpom+1.
        else
          Depth=0.
        endif
      else
        Depth=ConDrawDepth
      endif
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      BackToAdvanced=.false.
      id=NextQuestId()
      xqd=650.
      Veta='Select atoms to be filled out'
      il=14
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=11
      ild=10
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      SbwDoubleClickAllowed=.true.
      SbwRightClickAllowed=.true.
      nSbwSel=SbwLastMade
      il=il+2
      dpom=120.
      xpom=.5*xqd-dpom-20.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=.5*xqd-dpom-20.
      il=il+1
      Veta='Select a%dvanced'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdvanced=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,1)
      nEdwInclude=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
      enddo
      do i=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
      enddo
      call CloseIfOpened(ln)
      allocate(BratAtomFill(NAtActive),IAtomFill(NAtActive))
      call SetLogicalArrayTo(BratAtomFill,NAtActive,.true.)
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        NAtActive=NAtActive+1
        IAtomFill(NAtActive)=i
      enddo
      do i=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
        NAtActive=NAtActive+1
        IAtomFill(NAtActive)=i
      enddo
1400  call CloseIfOpened(SbwLnQuest(nSbwSel))
      do i=1,NAtActive
        if(BratAtomFill(i)) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSel,j)
      enddo
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_atoms.tmp')
1500  if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      call FeQuestEvent(id,ich)
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(BratAtomFill,NAtActive,lpom)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        idp=NextQuestId()
        xqdp=300.
        il=6
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,2
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                                NAtFormula(KPhase),0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,ConFillAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=1,NAtActive
            if(EqWild(Atom(IAtomFill(i)),Veta,.false.))
     1        BratAtomFill(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
          isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=1,NAtActive
            if(isf(IAtomFill(i)).eq.isfp) BratAtomFill(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1400
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=1,NAtActive
          if(EqWild(Atom(i),Veta,.false.))
     1      BratAtomFill(i)=.true.
        enddo
        call FeQuestStringEdwOpen(nEdwInclude,' ')
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nButtAdvanced)
     1  then
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NAtActive
          BratAtomFill(i)=SbwItemSelQuest(i,nSbwSel).eq.1
        enddo
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      if(ich.ne.0) go to 9999
      CutOffDist=Depth
      call SetLogicalArrayTo(AtBrat,NAtCalc,.false.)
      do i=1,NAtActive
        AtBrat(i)=BratAtomFill(IAtomFill(i))
      enddo
      if(ncomp(1).le.1) nsubs=1
      do i=1,3
        l=io(i)
        xpp(l)=(xmaxa(i,ir)+xmina(i,ir))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddx(io(1))=(xmaxa(1,ir)-xpp(io(1)))*float(i1)
        do i2=-1,1,2
          ddx(io(2))=(xmaxa(2,ir)-xpp(io(2)))*float(i2)
          do i3=-1,1,2
            ddx(io(3))=(xmaxa(3,ir)-xpp(io(3)))*float(i3)
            if(obecny) then
              dd=sqrt(ddx(1)**2+ddx(2)**2+ddx(3)**2)
            else
              call multm(MetTens(1,nsubs,KPhase),ddx,gddx,3,3,1)
              dd=sqrt(scalmul(ddx,gddx))
            endif
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      if(ir.eq.2) then
        call prevod(1,xpp,xp)
      else
        call CopyVek(xpp,xp,3)
      endif
      call OkoliC(xp,dmez,ir,NSubs,.true.)
      DrawAtN=0
      do 2000i=1,npdf
        j=ipor(i)
        if(ir.eq.2) then
          call prevod(0,xpdf(1,j),xp)
        else
          call CopyVek(xpdf(1,j),xp,3)
        endif
        kk=0
        do k=1,2
          l=io(k)
          if(xp(l).lt.xmina(k,ir).or.
     1       xp(l).gt.xmaxa(k,ir)) go to 2000
        enddo
        l=io(3)
        if(xp(l).lt.xmina(3,ir)-Depth.or.
     1     xp(l).gt.xmaxa(3,ir)+Depth) cycle
        if(DrawAtN.lt.NMaxDraw) then
          DrawAtN=DrawAtN+1
          Veta=atom(IAPdf(j))
          k=index(ScPdf(j),'#')
          if(k.gt.0) Veta=Veta(:idel(Veta))//ScPdf(j)(k:)
          DrawAtName(DrawAtN)=Veta
          DrawAtColor(DrawAtN)=0
          DrawAtSkip(DrawAtN)=.false.
          DrawAtBondLim(DrawAtN)=2.
        endif
2000  continue
      ConPtatSe=.false.
      call ConWriteKeys
      call DefaultContour
      call NactiContour
      call ConReadKeys
      AtomsOn=.true.
      ConPtatSe=.true.
      call KresliMapu(0)
9999  if(allocated(BratAtomFill)) deallocate(BratAtomFill,IAtomFill)
      CutOffDist=CutOffDistOld
      return
      end
      subroutine ConFillAtomsCheck
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      if(nEdwAtoms.eq.0) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
9999  return
      end
      subroutine ConDrawAtomsEdit
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      character*80 Veta,AtName
      character*15 Men(0:20)
      integer WhatHappened,RolMenuSelectedQuest
      external ConDrawAtomReadCommand,ConDrawAtomWriteCommand,FeVoid
      save /DrawAtomQuest/
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_drawatom.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9999
      Men(0)='Default'
      do i=1,PocetBarev
        Men(i)=ColorNames(i)
      enddo
      do i=1,DrawAtN
        call ConMakeDrawAtString(i,Veta)
        write(ln,FormA1)(Veta(j:j),j=1,idel(Veta))
      enddo
      call CloseIfOpened(ln)
      xqd=500.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_drawatom.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xd=0.
      do i=0,PocetBarev
        xd=max(xd,FeTxLength(Men(i)))
      enddo
      xd=xd+20.
      xpomt=5.
      Veta='%Atom name'
      xpome=xpomt+FeTxLengthUnder(Veta)+10.
      xpom=xpome
      dpom=160.
      do i=1,3
       if(i.ne.2)
     1    call FeQuestEdwMake(id,xpomt,il,xpome,il,Veta,'L',dpom,EdwYd,
     2                        0)
       if(i.eq.1) then
          nEdwName=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
          xpomt=xpome+dpom+15.
          Veta='Colo%r'
          xpome=xpomt+FeTxLengthUnder(Veta)+10.
          dpom=xd
        else if(i.eq.2) then
          call FeQuestRolMenuMake(id,xpomt,il,xpome,il,Veta,'L',
     1                            dpom+EdwYd,EdwYd,1)
          nRolMenuColor=RolMenuLastMade
          xpomt=5.
          Veta='Bond %limit'
          xpome=xpom
          il=il+1
        else if(i.eq.3) then
          nEdwBond=EdwLastMade
        endif
      enddo
1300  icolor=1
      AtName=' '
      BondLim=2.
1350  call FeQuestStringEdwOpen(nEdwName,AtName)
      call FeQuestRolMenuOpen(nRolMenuColor,Men,PocetBarev+1,icolor)
      call FeQuestRealEdwOpen(nEdwBond,BondLim,.false.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  ConDrawAtomReadCommand,ConDrawAtomWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuColor)
     1  then
        icolor=max(RolMenuSelectedQuest(nRolMenuColor),1)
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.ne.0) then
        ErrFlag=1
      else
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_drawatom.tmp','formatted',
     1                'unknown')
        DrawAtN=0
3000    read(ln,FormA80,end=3100) Veta
        DrawAtN=DrawAtN+1
        call ConReadDrawAtString(Veta,DrawAtN)
        go to 3000
3100    close(ln,status='delete')
      endif
      call ConWriteKeys
      call DefaultContour
      call NactiContour
      call ConReadKeys
9999  return
      end
      subroutine ConDrawAtomReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      dimension xp(1)
      character*80 t80,AtName
      character*(*) Command
      integer ColorOrder
      logical EqIgCase
      save /DrawAtomQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(.not.EqIgCase(t80,'drawatom').and.
     1   .not.EqIgCase(t80,'!drawatom')) go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,AtName)
      call TestAtomString(AtName,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      icolor=max(ColorOrder(t80)+1,1)
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      kk=0
      call StToReal(t80,kk,xp,1,.false.,ich)
      if(ich.ne.0) go to 8040
      BondLim=xp(1)
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  if(t80.eq.'Jiz oznameno') then
        ich=1
        go to 9999
      endif
      t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
      subroutine ConDrawAtomWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      character*(*) Command
      character*256 EdwStringQuest
      character*80  AtName,ErrString
      save /DrawAtomQuest/
      ich=0
      Command='drawatom'
      AtName=EdwStringQuest(nEdwName)
      if(AtName.eq.' ') go to 9100
      call TestAtomString(AtName,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command)+1)//AtName(:idel(AtName))
      if(icolor.le.1) then
        Cislo='Default'
      else
        Cislo=ColorNames(icolor-1)
      endif
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwBond,BondLim)
      write(Cislo,'(f15.3)') BondLim
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  if(ErrString.eq.'Jiz oznameno') go to 9100
      call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
      subroutine ConMakeDrawAtString(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) String
      String=IdContour(nCmdDrawAt)
      if(DrawAtSkip(n)) String='!'//String(:idel(String))
      call uprat(DrawAtName(n))
      String=String(:idel(String)+1)//
     1       DrawAtName(n)(:idel(DrawAtName(n)))
      if(DrawAtColor(n).eq.0) then
        Cislo='Default'
      else
        Cislo=ColorNames(DrawAtColor(n))
      endif
      String=String(:idel(String)+1)//Cislo(:idel(Cislo))
      write(Cislo,'(f15.3)') DrawAtBondLim(n)
      call ZdrcniCisla(Cislo,1)
      String=String(:idel(String)+1)//Cislo(:idel(Cislo))
      return
      end
      subroutine ConReadDrawAtString(String,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(1)
      character*(*) String
      character*80  Veta
      integer ColorOrder
      logical EqIgCase
      k=1
1000  if(String(k:k).eq.' ') then
        k=k+1
        go to 1000
      endif
      DrawAtSkip(n)=String(k:k).eq.'!'
      k=k-1
      call kus(String,k,Cislo)
      call kus(String,k,DrawAtName(n))
      call uprat(DrawAtName(n))
      call kus(String,k,Veta)
      if(EqIgCase(Veta,'Default')) then
        DrawAtColor(n)=0
      else
        DrawAtColor(n)=ColorOrder(Veta)
      endif
      if(k.lt.len(String)) then
        call StToReal(String,k,xp,1,.false.,ich)
        if(ich.ne.0) then
          n=n-1
          go to 9999
        endif
        DrawAtBondLim(DrawAtN)=xp(1)
      else
        DrawAtBondLim(DrawAtN)=2.
      endif
9999  return
      end
      subroutine ConReadKeys
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3)
      character*80 Veta
      logical EqIgCase,MameJi
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call Najdi('contour',i)
      if(i.ne.1) go to 9999
      PreskocVykricnik=.false.
      call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      IPoints=0
      DrawAtN=0
      NCurve=0
      MameJi=.not.Obecny
      izpet=0
1210  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0.or.izpet.eq.0) then
        ErrFlag=0
        go to 9999
      endif
      if(NumberOfPlanes.gt.0) then
        if(izpet.eq.nCmdLabelPlane) then
          if(.not.Obecny) go to 9999
          DrawAtN=0
          NCurve=0
          Veta=NactiVeta(PosledniPozice+1:)
          MameJi=EqIgCase(Veta,LabelPlane(IPlane))
          PosledniPozice=len(NactiVeta)+1
          go to 1210
        else if(izpet.eq.nCmdEndPlane) then
          if(MameJi) then
            go to 9999
          else
            go to 1210
          endif
        endif
      endif
      if(.not.MameJi) go to 1210
      if(izpet.eq.nCmdDeltaPlane) then
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9900
        endif
        DeltaPlane=xp(1)
      else if(izpet.eq.nCmdPntPlane) then
        if(IPoints.lt.3) then
          IPoints=IPoints+1
          call kus(NactiVeta,PosledniPozice,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=9900) SelPlane(IPoints)
          StPlane(IPoints)=' '
          if(SelPlane(IPoints).eq.1) then
            call kus(NactiVeta,PosledniPozice,StPlane(IPoints))
            call CtiAt(StPlane(IPoints),XPlane(1,IPoints),ich)
            if(ich.ne.-1) go to 9900
            ich=0
          else if(SelPlane(IPoints).eq.2) then
            call StToReal(NactiVeta,PosledniPozice,XPlane(1,IPoints),3,
     1                    .false.,ich)
          else if(SelPlane(IPoints).eq.3) then
            if(IPoints.eq.1) then
              SelPlane(IPoints)=2
              call SetRealArrayTo(XPlane(1,IPoints),3,0.)
              IPoints=IPoints+1
              SelPlane(IPoints)=3
            endif
            call StToReal(NactiVeta,PosledniPozice,DxPlane(1,IPoints),3,
     1                    .false.,ich)
          else
            go to 9900
          endif
          if(ich.ne.0) go to 9900
          if(IPoints.gt.1) then
            if(SelPlane(IPoints).eq.3) then
              do i=1,3
                XPlane(i,IPoints)=DxPlane(i,IPoints)+XPlane(i,1)
              enddo
            else
              do i=1,3
                DxPlane(i,IPoints)=XPlane(i,IPoints)-XPlane(i,1)
              enddo
            endif
          endif
          go to 1210
        endif
      else if(izpet.eq.nCmdScopePlane) then
        call StToReal(NactiVeta,PosledniPozice,ScopePlane,3,.false.,ich)
        if(ich.ne.0) go to 9900
      else if(izpet.eq.nCmdShiftPlane) then
        call StToReal(NactiVeta,PosledniPozice,ShiftPlane,3,.false.,ich)
        if(ich.ne.0) go to 9900
      else if(izpet.eq.nCmdDrawAt) then
        Veta=NactiVeta
        if(NacetlVykricnik) Veta='!'//Veta(:idel(Veta))
        DrawAtN=DrawAtN+1
        call ConReadDrawAtString(Veta,DrawAtN)
      else if(izpet.eq.nCmdXTypeCurve) then
        call kus(NactiVeta,PosledniPozice,Cislo)
        call Posun(Cislo,0)
        read(Cislo,FormI15,err=9900) XTypeCurve
      else if(izpet.eq.nCmdPointCurve) then
        NCurve=NCurve+1
        AtCurve(NCurve)=NactiVeta(PosledniPozice+1:)
        call UprAt(AtCurve(NCurve))
      endif
      go to 1210
9900  ErrFlag=1
9999  call CloseIfOpened(m50)
      return
      end
      subroutine ConWriteKeys
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Radka
      logical EqIgCase
      SaveAs=0
      Radka=' '
      if(Obecny.and.VolaToContour) then
        if(IPlane.eq.0) then
          call ConWriteKeysSaveAs(0,Radka)
          SaveAs=-1
          if(Radka.eq.' ') go to 9999
          call ConReallocatePlanes
          NumberOfPlanes=NumberOfPlanes+1
          LabelPlane(NumberOfPlanes)=Radka
          IPlane=NumberOfPlanes
        endif
      endif
1000  call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      call WriteKeys('contour')
      izpet=0
      call NastavKeys('contour')
      PreskocVykricnik=.false.
1100  call NactiCommon(m50,izpet)
      if((Obecny.and.IZpet.ne.0).or.IPlane.lt.-1) then
        if(IZpet.ne.0) then
          Radka=NactiVeta
          if(NacetlVykricnik) Radka='!'//Radka(:idel(Radka))
          Radka='  '//Radka(:idel(Radka))
          write(55,FormA) Radka(:idel(Radka))
        endif
        if(IPlane.lt.-1) then
          if(IZpet.eq.0) then
            go to 2500
          else
            go to 1100
          endif
        endif
      endif
      if(IZpet.eq.nCmdLabelPlane) then
        if(Obecny) then
          if(IPlane.gt.0) then
            Radka=NactiVeta(PosledniPozice+1:)
            if(EqIgCase(Radka,LabelPlane(IPlane))) then
1200          read(m50,FormA,end=2000) Radka
              k=0
              call kus(Radka,k,Cislo)
              if(.not.EqIgCase(Cislo,'end').and.
     1           .not.EqIgCase(Cislo,idContour(nCmdEndPlane))) then
                go to 1200
              else
                backspace 55
                go to 2000
              endif
            endif
          endif
        else
          backspace m50
          go to 2000
        endif
      else
        if(IZpet.eq.0) go to 2000
      endif
      go to 1100
2000  if(Obecny) then
        k=0
        call WriteLabeledRecord(55,'  '//idContour(nCmdLabelPlane),
     1                          LabelPlane(IPlane),k)
        do i=1,3
          write(Radka,'(i3)') SelPlane(i)
          n=4
          if(SelPlane(i).eq.1) then
            Radka(5:)=StPlane(i)
            n=2
          else if(SelPlane(i).eq.2) then
            write(Radka(5:),100)(XPlane(j,i),j=1,3)
          else
            write(Radka(5:),100)(DxPlane(j,i),j=1,3)
          endif
          call ZdrcniCisla(Radka,n)
          Radka='    '//idContour(nCmdPntPlane)
     1          (:idel(idContour(nCmdPntPlane)))//' '//
     2          Radka(:idel(Radka))
          write(55,FormA) Radka(:idel(Radka))
        enddo
        write(Radka,101) DeltaPlane
        call ZdrcniCisla(Radka,1)
        Radka='    '//idContour(nCmdDeltaPlane)
     1        (:idel(idContour(nCmdDeltaPlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        write(Radka,101) ScopePlane
        call ZdrcniCisla(Radka,3)
        Radka='    '//idContour(nCmdScopePlane)
     1        (:idel(idContour(nCmdScopePlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        write(Radka,101) ShiftPlane
        call ZdrcniCisla(Radka,3)
        Radka='    '//idContour(nCmdShiftPlane)
     1        (:idel(idContour(nCmdShiftPlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        if(NCurve.gt.0) then
          write(Radka,'(i3)') XTypeCurve
          k=0
          call WriteLabeledRecord(55,'    '//idContour(nCmdXTypeCurve),
     1                            Radka,k)
          do i=1,NCurve
            if(AtCurve(i).eq.' ') then
              if(XTypeCurve.eq.1) then
                write(Radka,101)(XLocCurve(j,i),j=1,3)
              else
                write(Radka,100)(XFractCurve(j,i),j=1,3)
              endif
              call ZdrcniCisla(Radka,3)
            else
              Radka=AtCurve(i)
            endif
            k=0
            call WriteLabeledRecord(55,'    '//
     1                              idContour(nCmdPointCurve),Radka,k)
          enddo
        endif
      endif
      do i=1,DrawAtN
        call ConMakeDrawAtString(i,Radka)
        Cislo=' '
        if(Obecny) then
          n=4
        else
          n=2
        endif
        Radka=Cislo(:n)//Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
      enddo
      if(Obecny) write(55,FormA) '  '//
     1  idContour(nCmdEndPlane)(:idel(idContour(nCmdEndPlane)))
2100  if(IZpet.ne.0.or.IPlane.lt.-1) then
2200    read(m50,FormA,end=2500) Radka
        write(55,FormA) Radka(:idel(Radka))
        k=0
        call kus(Radka,k,Cislo)
        if(EqIgCase(Cislo,'end')) then
          go to 2550
        else
          go to 2200
        endif
      endif
2500  write(55,'(''end contour'')')
2550  if(SaveAs.eq.1) SaveAs=-1
      call DopisKeys(1)
      if(SaveAs.eq.1) then
        close(m50)
        close(55)
        go to 1000
      endif
      KeysSaved=.true.
      go to 9999
9900  call CloseIfOpened(m50)
      close(55,status='delete')
9999  return
100   format(3f12.6)
101   format(3f10.4)
      end
      subroutine ConWriteKeysSaveAs(Key,FileSaveAs)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) FileSaveAs
      character*256 EdwStringQuest
      character*80 Veta
      character*20 LabelSaveAs
      logical CrwLogicQuest,FeYesNo
      write(LabelSaveAs,'(''Plane#'',i2)') NumberOfPlanes+1
      call Zhusti(LabelSaveAs)
      if(.not.ConPtatSe) then
        if(Key.le.1) then
          if(LabelPlaneNew.ne.' ') then
            FileSaveAs=LabelPlaneNew
          else if(IPlane.gt.0) then
            FileSaveAs=LabelPlane(IPlane)
          else
            FileSaveAs=' '
          endif
        else if(Key.eq.2) then
          FileSaveAs='###NeniObecna###'
        else if(Key.eq.3) then
          FileSaveAs='###JenBasic###'
        endif
        go to 9999
      endif
      id=NextQuestId()
      if(Key.eq.0) then
        Veta='A new plane has been drawn.'
      else if(Key.eq.1) then
        Veta='The plane has been modified.'
      else if(Key.eq.2) then
        Veta='Set of drawn atoms has been modified.'
      else if(Key.eq.3) then
        Veta='Basic contour keys have been modified.'
      endif
      Veta=Veta(:idel(Veta))//' Do you want to:'
      xqd=FeTxLength(Veta)+20.
      il=3
      if(Key.eq.1) il=il+1
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
      il=1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      xpom=5.
      tpom=xpom+CrwgXd+5.
      j=0
      nCrwSaveAs=0
      Veta='%discard changes'
      do i=1,3
        if(i.eq.3.and.Key.ne.1) cycle
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) then
          nCrwDiscard=CrwLastMade
          Veta='%save it'
        else if(i.eq.2) then
          nCrwSave=CrwLastMade
          Veta='save it %as'
          tpomp=tpom+FeTxLengthUnder(Veta)+5.
        else
          nCrwSaveAs=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
      enddo
      if(Key.eq.1) then
        dpom=100.
        Veta='=>'
        tpom=tpomp
        xpom=tpomp+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSaveAs=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,LabelSaveAs)
      else
        nEdwSaveAs=0
      endif
1200  if(nEdwSaveAs.gt.0) then
        if(CrwLogicQuest(nCrwSaveAs)) then
          call FeQuestStringEdwOpen(nEdwSaveAs,LabelSaveAs)
        else
          LabelSaveAs=EdwStringQuest(nEdwSaveAs)
          call FeQuestEdwDisable(nEdwSaveAs)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      FileSaveAs=' '
      if(CrwLogicQuest(nCrwSave)) then
        if(Key.le.1) then
          if(LabelPlaneNew.ne.' ') then
            FileSaveAs=LabelPlaneNew
          else if(IPlane.gt.0) then
            FileSaveAs=LabelPlane(IPlane)
          else
            FileSaveAs=' '
          endif
        else if(Key.eq.2) then
          FileSaveAs='###NeniObecna###'
        else if(Key.eq.3) then
          FileSaveAs='###JenBasic###'
        endif
      else if(CrwLogicQuest(nCrwSaveAs)) then
        if(nEdwSaveAs.gt.0) then
          FileSaveAs=EdwStringQuest(nEdwSaveAs)
          IPlaneNew=LocateInStringArray(LabelPlane,NumberOfPlanes,
     1                                  FileSaveAs,.true.)
          if(IPlaneNew.gt.0.and.IPlaneNew.ne.IPlane) then
            if(.not.FeYesNo(-1.,YBottomMessage,'The plane "'//
     1                      FileSaveAs(:idel(FileSaveAs))//
     2                      '" already exists. Do you want to '//
     3                      'rewrite it?',1)) then
              call FeQuestButtonOff(ButtonOK-ButtonFr+1)
              go to 1500
            endif
          endif
        endif
      endif
      call FeQuestRemove(id)
9999  return
      end
      subroutine ConWriteKeysSaveAsWithRepeat(Radka)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Radka
      call ConWriteKeysSaveAs(1,Radka)
      if(Obecny) then
        IPlanePrev=IPlane
        if(Radka.ne.' ') then
          IPlane=LocateInStringArray(LabelPlane,NumberOfPlanes,Radka,
     1                               .true.)
        else
          IPlane=IPlanePrev
        endif
        if(IPlane.le.0.and.Radka.ne.' ') then
          call ConReallocatePlanes
          NumberOfPlanes=NumberOfPlanes+1
          LabelPlane(NumberOfPlanes)=Radka
          IPlane=NumberOfPlanes
          SaveAs=1
        else
          if(IPlanePrev.eq.IPlane) then
            SaveAs=0
          else
            IPlaneOld=IPlane
            SaveAs=1
          endif
        endif
      endif
      return
      end
      subroutine ConReallocatePlanes
      use Contour_mod
      if(NumberOfPlanes.ge.NumberOfPlanesMax) then
        if(NumberOfPlanes.gt.0) then
          allocate(LabelPlaneO(NumberOfPlanes))
          LabelPlaneO(1:NumberOfPlanes)=LabelPlane(1:NumberOfPlanes)
        endif
        if(allocated(LabelPlane)) deallocate(LabelPlane)
        n=2*(NumberOfPlanes+1)
        allocate(LabelPlane(n))
        NumberOfPlanesMax=n
        if(NumberOfPlanes.gt.0) then
          LabelPlane(1:NumberOfPlanes)=LabelPlaneO(1:NumberOfPlanes)
          deallocate(LabelPlaneO)
        endif
      endif
      return
      end
      subroutine ConDefMovie(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      character*80 Veta
      character*10 Label
      integer EdwStateQuest
      logical CrwLogicQuest
      data Label/'Record it:'/
      MovieFileName=' '
      il=NDim(KPhase)+8
      id=NextQuestId()
      xqd=250.
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Define parameters for movie',0,LightGray,0,0)
      il=1
      tpom=50.
      call FeQuestLblMake(id,tpom,il,'Start at','C','N')
      tpom=tpom+120.
      call FeQuestLblMake(id,tpom,il,'End at','C','N')
      tpom=5.
      xpom=30.
      dpom=80.
      call SetIntArrayTo(nxMovieFr,4,1)
      call CopyVekI(nx(3),nxMovieTo,4)
      do i=1,NDim(KPhase)-2
        il=il+1
        call FeQuestEudMake(id,tpom,il,xpom,il,cx(i+2),'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwLimFr=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,xmin(i+2),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,xmin(i+2),xmax(i+2),
     1                      dx(i+2))
        call FeQuestEudMake(id,tpom,il,xpom+dpom+40.,il,' ','L',dpom,
     1                      EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,xmax(i+2),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,xmin(i+2),xmax(i+2),
     1                      dx(i+2))
      enddo
      nEdwLimTo=EdwLastMade
      il=il+1
      Veta='%Delay time in sec'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDelayTime=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelayTimeForMovie,.false.,
     1                        .false.)
      il=il+1
      Veta='%Repeat factor'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRepeat=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,MovieRepeat,.false.)
      nEdwRepeat=EdwLastMade
      call FeQuestEudOpen(EdwLastMade,0,9999,1,1.,1.,1.)
      il=il+1
      Veta='Record the sequence of %maps'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwRecord=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,MovieFileName.ne.' ')
      il=il+1
      xpom=tpom+3.+FeTxLengthUnder(Veta)
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      Veta='%Name radix'
      dpom=110.
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+10.
      Veta='Bro%wse'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      il=il+1
      ilp=il
      tpom=FeTxLength(Label)+7.
      xpom=0.
      do i=1,5
        xpom=max(xpom,FeTxLength(HCMenu(i)))
      enddo
      xpom=tpom+xpom+5.
      do i=1,5
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      HCMenu(i)(:idel(HCMenu(i)))//'s','L',CrwgXd,
     2                      CrwgYd,0,1)
        if(i.eq.1) nCrwTypeFirst=CrwLastMade
        il=il+1
      enddo
      call FeQuestLblMake(id,5.,ilp,Label,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLbl=LblLastMade
1400  if(CrwLogicQuest(nCrwRecord)) then
        if(EdwStateQuest(nEdwFileName).ne.EdwOpened) then
          HardCopy=HardCopyPCX
          call FeQuestStringEdwOpen(nEdwFileName,MovieFileName)
          call FeQuestButtonOpen(nButtBrowse,ButtonOff)
          call FeQuestLblOn(nLbl)
          nCrw=nCrwTypeFirst
          do i=1,5
            call FeQuestCrwOpen(nCrw,i.eq.HardCopy)
            nCrw=nCrw+1
          enddo
          EventType=EventEdw
          EventNumber=nEdwFileName
        endif
      else
        HardCopy=0
        call FeQuestEdwClose(nEdwFileName)
        call FeQuestButtonClose(nButtBrowse)
        nCrw=nCrwTypeFirst
        do i=1,5
          call FeQuestCrwClose(nCrw)
          nCrw=nCrw+1
        enddo
        call FeQuestLblOff(nLbl)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRecord) then
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        call FeFileManager('Select file to derive the radix',Veta,'*.*',
     1                     0,.true.,ich)
        EventType=EventEdw
        EventNumber=nEdwFileName
        if(ich.eq.0.and.Veta.ne.' ') then
          i=idel(Veta)
1520      if(Veta(i:i).ne.'.') then
            i=i-1
            if(i.gt.1) then
              go to 1520
            else
              go to 1540
            endif
          endif
          Veta(i:)=' '
          i=i-1
1530      if(index(Cifry(:10),Veta(i:i)).gt.0) then
            i=i-1
            if(i.gt.1) then
              go to 1530
            else
              go to 1540
            endif
          endif
          if(Veta(i:i).eq.'_') Veta(i:)=' '
1540      call FeQuestStringEdwOpen(nEdwFileName,Veta)
        endif
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwDelayTime,DelayTimeForMovie)
        call FeQuestIntFromEdw(nEdwRepeat,MovieRepeat)
        i=1
        do nEdw=nEdwLimFr,nEdwLimTo
          j=(i-1)/2+1
          call FeQuestRealFromEdw(nEdw,pom)
          if(dx(j+2).ne.0.) then
            n=nint((pom-xmin(j+2))/dx(j+2))+1
          else
            n=1
          endif
          if(mod(i,2).eq.1) then
            nxMovieFr(j)=n
          else
            nxMovieTo(j)=n
          endif
          i=i+1
        enddo
        if(CrwLogicQuest(nCrwRecord)) then
          MovieFileName=EdwStringQuest(nEdwFileName)
          if(MovieFileName.eq.' ') then
            call FeChybne(-1.,-1.,'the file prefix not defined.',' ',
     1                    SeriousError)
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            go to 1400
          endif
          nCrw=nCrwTypeFirst
          do i=1,5
            if(CrwLogicQuest(nCrw)) then
              HardCopy=i
              go to 5000
            endif
            nCrw=nCrw+1
          enddo
        else
          MovieFileName=' '
          HardCopy=0
        endif
      endif
5000  call FeQuestRemove(id)
      return
      end
      subroutine FePolyLineT(n,xpole,ypole,Color)
      include 'fepc.cmn'
      dimension xpole(n),ypole(n)
      integer Color
      call FePolyLine(n,xpole,ypole,Color)
      do i=-1,1,2
        pom=float(i)
        do j=1,n
          xpole(j)=xpole(j)+pom
        enddo
        call FePolyLine(n,xpole,ypole,Color)
      enddo
      do j=1,n
        xpole(j)=xpole(j)-pom
      enddo
      return
      end
      subroutine TPCriticalPoints
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xat(3,2)
      character*256 EdwStringQuest
      character*80  Center,Veta,Atp(2)
      character*50  TypeOfSearch(4)
      character*30  Action(2)
      integer RolMenuSelectedQuest
      logical UzitStarou,FeYesNo,ExistFile
      data Center/' '/,IType/1/,nTypeOfSearch/4/
      data TypeOfSearch/'between all bonds within interval',
     1       'along specified line',
     2       'in paralleliped - cell fraction',
     3       'in paralleliped - general'/
      data Action/'Search for critical points','Integration'/
      data Atp/2*' '/
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      call ConSetLocDensity
      allocate(AtBrat(NAtCalc))
      n=max(NAtCalc,1000)
      MaxPDF=n
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      if(allocated(xcpa))
     1  deallocate(xcpa,CPRho,CPGrad,CPDiag,CPType)
      allocate(xcpa(3,100),CPRho(100),CPGrad(100),CPDiag(3,100),
     1         CPType(100),xdst(3,NAtCalc),BetaDst(6,NAtCalc),
     2         dxm(NAtCalc),BratPrvni(NAtCalc),BratDruhyATreti(NAtCalc))
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
1100  id=NextQuestId()
      xdq=400.
      xdqp=xdq*.5
      il=8
      call FeQuestCreate(id,-1.,-1.,xdq,il,
     1                   'Define parameters for CP search',1,LightGray,
     2                   0,0)
      il=1
      Veta='%Type of search'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=0.
      do i=1,nTypeOfSearch
        dpom=max(FeTxLength(TypeOfSearch(i))+20.,dpom)
      enddo
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom+EdwYd,
     1                        EdwYd,1)
      nRolMenuType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,TypeOfSearch,
     1                        nTypeOfSearch,IType)
      il=il+1
      Veta='CP %criterion - delta(Rho)<'
      xdl=FeTxLengthUnder(Veta)
      xpom=tpom+xdl+15.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCriter=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPCrit,.false.,.false.)
      il=il+1
      Veta='Max. %number of iterations'
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNIter=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,CPNIter,.false.)
      call FeQuestEudOpen(EdwLastMade,1,200,1,0.,0.,0.)
      il=il+1
      Veta='Max. iteration %step'
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxStep=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPMaxStep,.false.,.false.)
      il=il+1
      Veta='Min. %density'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRhoMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPRhoMin,.false.,.false.)
      il=il+1
      Veta='%1st point'
      xdl=FeTxLengthUnder(Veta)
      xpom=tpom+xdl+15.
      dpom=110.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdw1stPoint=EdwLastMade
      Veta='%2nd point'
      xpom=xpom+xdqp
      tpom=tpom+xdqp
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdw2ndPoint=EdwLastMade
      tpom=5.
      Veta='D(%geom)'
      pom=FeTxLengthUnder(Veta)+10.
      dpom=50.
      xpom=tpom+pom
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDGeom=EdwLastMade
      tpom=xpom+dpom+12.
      xpom=tpom+pom
      Veta='D(m%in)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMin=EdwLastMade
      tpom=xpom+dpom+24.
      xpom=tpom+pom
      Veta='D(m%ax)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
1400  if(IType.eq.1) then
        call FeQuestEdwClose(nEdw1stPoint)
        call FeQuestEdwClose(nEdw2ndPoint)
        call FeQuestRealEdwOpen(nEdwDGeom,CPDGeom,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwDMin,CPDMin,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwDMax,CPDMax,.false.,.false.)
      else if(IType.eq.2) then
        call FeQuestEdwClose(nEdwDGeom)
        call FeQuestEdwClose(nEdwDMin)
        call FeQuestEdwClose(nEdwDMax)
        call FeQuestStringEdwOpen(nEdw1stPoint,atp(1))
        call FeQuestStringEdwOpen(nEdw2ndPoint,atp(2))
      else if(IType.eq.3.or.IType.eq.4) then
        call FeQuestEdwClose(nEdwDMin)
        call FeQuestEdwClose(nEdwDMax)
        call FeQuestEdwClose(nEdw1stPoint)
        call FeQuestEdwClose(nEdw2ndPoint)
        call FeQuestRealEdwOpen(nEdwDGeom,CPDGeom,.false.,.false.)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(IType.eq.2) then
          do i=1,2
            if(i.eq.1) then
              Veta=EdwStringQuest(nEdw1stPoint)
            else
              Veta=EdwStringQuest(nEdw2ndPoint)
            endif
            call CtiAt(Veta,xat(1,i),ich)
            if(ich.eq.-1) then
              atp(i)=Veta
              kat=1
              ich=0
            else if(ich.eq.0) then
              kat=0
            else
              go to 1500
            endif
          enddo
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuType) then
        i=RolMenuSelectedQuest(nRolMenuType)
        if(i.ge.1.and.i.le.4.and.i.ne.IType) then
          IType=i
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(IType.eq.1) then
          call FeQuestRealFromEdw(nEdwDMin,CPDMin)
          call FeQuestRealFromEdw(nEdwDMax,CPDMax)
        endif
        if(IType.ne.2) call FeQuestRealFromEdw(nEdwDGeom,CPDGeom)
        call FeQuestRealFromEdw(nEdwCriter,CPCrit)
        call FeQuestIntFromEdw(nEdwNIter, CPNIter)
        call FeQuestRealFromEdw(nEdwMaxStep,CPMaxStep)
        call FeQuestRealFromEdw(nEdwRhoMin,CPRhoMin)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      UzitStarou=.false.
      do i=1,6
        iorien(i)=i
      enddo
      if(IType.eq.2) then
        do i=1,3
          xmina(i,1)=min(xat(i,1),xat(i,2))
          xmaxa(i,1)=max(xat(i,1),xat(i,2))
        enddo
      else if(IType.eq.3) then
        if(ExistFile(fln(:ifln)//'.z81')) then
          UzitStarou=FeYesNo(-1.,-1.,'Do you want to analyze the '//
     1                       'previously created map',1)

        endif
        if(UzitStarou) then
          m8=NextLogicNumber()
          call OpenMaps(m8,fln(:ifln)//'.z81',nxny,0)
          if(ErrFlag.ne.0) go to 9999
          read(m8,rec=1) nx,nxny,nmapa,(xmin(i),xmax(i),i=1,6),dx,
     1                  (iorien(i),i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
          nsubs=mod(nsubs,10)
          n=nx(3)+3
          read(m8,rec=n) npdf
          allocate(xpdf(3,npdf),popaspdf(64,npdf),idpdf(npdf),
     1             dpdf(npdf),fpdf(npdf),ypdf(3,npdf),SelPDF(npdf),
     2             scpdf(npdf),iapdf(npdf),ispdf(npdf),ipor(npdf))
          do i=1,npdf
            n=n+1
            read(m8,rec=n) ip,iapdf(ip),ispdf(ip),fpdf(ip),
     1                    (xpdf(j,ip),j=1,3)
            ipor(i)=ip
          enddo
        else
          call CPReadScope(0,xmin,xmax,dx,i,CutOffDist,ich)
        endif
      else if(IType.eq.4) then
        call ConPrelim
        DensityType=5
        mcmax=0
        kpdf=6
        DrawPdf=.true.
        obecny=.true.
        call ConDefGenSection(Center,1,ich)
        if(ich.ne.0) go to 9999
      endif
      if(.not.UzitStarou) then
        if(IType.eq.1) then
          call ConDefAtForDenCalc(4,ich)
        else
          call ConDefAtForDenCalc(3,ich)
        endif
      endif
      if(ich.ne.0) go to 9999
      if(IType.eq.1) then
        call CPSearchBond
      else if(IType.eq.2) then
        call CPSearchAlongLine(xat(1,1),xat(1,2),atp(1),atp(2),kat,0)
        go to 1100
      else if(IType.eq.3.or.IType.eq.4) then
        call CPSearchInMap(IType-3,UzitStarou)
      endif
9999  if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      if(allocated(xcpa))
     1  deallocate(xcpa,CPRho,CPGrad,CPDiag,CPType,xdst,BetaDst,dxm,
     2             BratPrvni,BratDruhyATreti)
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      if(allocated(AtBrat)) deallocate(AtBrat)
      return
      end
      subroutine TPIntegration
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension soph(nprops),soth(nprops),wp(nprops),wt(nprops),
     1          Tollo(:),xp(3)
      character*256 EdwStringQuest
      character*80 t80
      integer EdwStateQuest,RolMenuSelectedQuest
      logical EqWild
      allocatable Tollo
      allocate(Tollo(NAtCalc))
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      if(allocated(IaChd))
     1  deallocate(IaChd,IsChd,XfChd,XoChd,popasChd,Toll,RCapture,
     2             ireach,rcla)
      if(allocated(AtBrat)) deallocate(AtBrat)
      allocate(AtBrat(NAtCalc))
      call ConSetLocDensity
      call ChdGenSymmAtoms
      StepInt=0.25*BohrRad
      rsca=0.8
      StepTrajInt=0.025*BohrRad
      TInfInt=10.*BohrRad
      SizeAtInt=9.*BohrRad
      AccurInt=0.01*BohrRad
      nac=NAtIndLenAll(KPhase)
      nap=NAtIndFrAll(KPhase)
      do i=nap,nap+nac-1
        AtBrat(i)=ai(i).gt.0.
      enddo
      call SelAtoms('Select atoms for basin integration',Atom(nap),
     1              AtBrat(nap),isf(nap),nac,ich)
      if(ich.ne.0) go to 9999
      ia=nap
      do i=1,nac
        j=isf(ia)
        if(AtType(j,KPhase).eq.'H') then
          Toll(i)=0.3
        else
          Toll(i)=0.8
        endif
        ia=ia+1
      enddo
      MethZFS=1
      nPhiIntA=32
      nThIntA=24
      nRInt=96
      CutOffDist=2.8
      call CopyVek(Toll,Tollo,nac)
      id=NextQuestId()
      xqd =400.
      call FeQuestCreate(id,-1.,-1.,xqd,5,'Define parameters for '//
     1                   'integration',0,LightGray,0,0)
      il=1
      dpom=50.
      t80='Number of steps in %phi'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(t80)+25.
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNPhi=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nPhiIntA,.false.)
      il=il+1
      t80='Number of steps in %theta'
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNTh=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nThIntA,.false.)
      il=il+1
      t80='Number of steps in %R'
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNR=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nRInt,.false.)
      il=il+1
      t80='%Cutoff distance for density calculation'
      xpom=tpom+FeTxLengthUnder(t80)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwCutOff=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CutOffDist,.false.,.false.)
      il=il+1
      t80='%Define capture sphere radii'
      dpom=FeTxLengthUnder(t80)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtCapRad=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCapRad) then
        idp=NextQuestId()
        xqd =500.
        xqdp=xqd*.5
        il=min(nac,10)+4
        call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define capture radii',
     1                     0,LightGray,0,0)
        if(nac.gt.30) then
          xpom=xqd-25.
          dpom=80.
          call FeQuestButtonMake(id,xpom,10,dpom,ButYd,'%Next')
          nButtNext=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonMake(id,xpom,1,dpom,ButYd,'%Previous')
          nButtPrevious=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        else
          nButtNext=0
          nButtPrevious=0
        endif
        tpom=5.
        dpom=50.
        pom=0.
        ia=nap
        do i=1,nac
          pom=max(pom,FeTxLength(Atom(ia)))
          ia=ia+1
        enddo
        xpom=tpom+pom+10.
        il=0
        do i=1,min(30,nac)
          il=il+1
          if(mod(il,10).eq.1.and.i.ne.1) then
            il=1
            tpom=tpom+150.
            xpom=xpom+150.
          endif
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Atom(i),'L',dpom,
     1                        EdwYd,0)
          if(i.eq.1) nEdwFirst=EdwLastMade
        enddo
        if(nac.gt.10) then
          il=11
        else
          il=il+1
        endif
        call FeQuestLinkaMake(idp,il)
        il=il+1
        do i=1,2
          if(i.eq.1) then
            t80='%Set'
            dpom1=FeTxLengthUnder(t80)+20.
            xpom1=50.
          else
            t80='S%et'
          endif
          call FeQuestButtonMake(idp,xpom1,il,dpom1,ButYd,t80)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          if(i.eq.1) then
            nButtSetAtomType=ButtonLastMade
            t80='radii for atom t%ype'
            tpom2=xpom1+dpom1+15.
            xpom2=tpom2+FeTxLengthUnder(t80)+25.
            dpom2=50.+EdwYd
            call FeQuestRolMenuMake(idp,tpom2,il,xpom2,il,t80,'L',dpom2,
     1                              EdwYd,0)
            call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                              NAtFormula(KPhase),0)
            nRolMenuAtomType=RolMenuLastMade
            tpom3=xpom2+dpom2+15.
            t80='to'
            xpom3=tpom3+FeTxLengthUnder(t80)+15.
            dpom3=50.
          else
            nButtSetAtomName=ButtonLastMade
            t80='radii for atom %name'
            call FeQuestEdwMake(idp,tpom2,il,xpom2,il,t80,'L',dpom2,
     1                          EdwYd,0)
            nEdwAtomName=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
            t80='to'
          endif
          call FeQuestEdwMake(idp,tpom3,il,xpom3,il,t80,'L',dpom3,
     1                        EdwYd,0)

          call FeQuestRealEdwOpen(EdwLastMade,0.8,.false.,.false.)
          if(i.eq.1) then
            nEdwAtomTypeValue=EdwLastMade
          else
            nEdwAtomNameValue=EdwLastMade
          endif
          il=il+1
        enddo
        m=1
1140    n1=30*(m-1)+1
        n2=min(nac,n1+30-1)
        il=0
        tpom=5.
        dpom=25.
        xpom=tpom+FeTxLengthUnder('XXXXXXXX')+5.
        il=0
        nEdw=nEdwFirst
        do i=n1,n1+29
          if(i.le.n2) then
            if(EdwStateQuest(nEdw).eq.EdwOpened)
     1        call FeQuestEdwLabelChange(nEdw,Atom(i))
            call FeQuestRealEdwOpen(nEdw,toll(i),.false.,.false.)
          else if(i.le.nac) then
            call FeQuestEdwClose(nEdw)
          endif
          nEdw=nEdw+1
        enddo
        if(nac.gt.30) then
          if(n2.lt.nac) then
            call FeQuestButtonOff(nButtNext)
          else
            call FeQuestButtonDisable(nButtNext)
          endif
          if(n1.gt.1) then
            call FeQuestButtonOff(nButtPrevious)
          else
            call FeQuestButtonDisable(nButtPrevious)
          endif
        endif
1250    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtPrevious.or.
     2      CheckNumber.eq.nButtNext)) then
          if(CheckNumber.eq.nButtNext) then
            m=m+1
          else
            m=m-1
          endif
          nEdw=nEdwFirst
          do i=n1,n2
            call FeQuestRealFromEdw(nEdw,toll(i))
            nEdw=nEdw+1
          enddo
          go to 1140
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSetAtomType) then
          t80=EdwStringQuest(nEdwAtomTypeValue)
          if(t80.ne.' ') then
            call FeQuestRealFromEdw(nEdwAtomTypeValue,pom)
          else
            go to 1140
          endif
          i=RolMenuSelectedQuest(nRolMenuAtomType)
          if(i.gt.0.and.i.le.nac) then
            ia=nap
            do j=1,nac
              if(isf(j).eq.i) Toll(j)=pom
              ia=ia+1
            enddo
          endif
          go to 1140
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSetAtomName) then
          t80=EdwStringQuest(nEdwAtomNameValue)
          if(t80.ne.' ') then
            call FeQuestRealFromEdw(nEdwAtomNameValue,pom)
          else
            go to 1140
          endif
          t80=EdwStringQuest(nEdwAtomName)
          ia=nap
          do j=1,nac
            if(EqWild(Atom(j),t80,.false.)) Toll(j)=pom
            ia=ia+1
          enddo
          go to 1140
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1250
        endif
        if(ich.eq.0) then
          nEdw=nEdwFirst
          do i=n1,n2
            call FeQuestRealFromEdw(nEdw,toll(i))
            nEdw=nEdw+1
          enddo
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) call CopyVek(Tollo,Toll,nac)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNPhi,nPhiIntA)
        call FeQuestIntFromEdw(nEdwNTh,nThIntA)
        call FeQuestIntFromEdw(nEdwNR,nRInt)
        call FeQuestRealFromEdw(nEdwCutOff,CutOffDist)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      do i=1,nac
        RCapture(i)=rsca*(Toll(i)-StepTrajInt)
        call multm(TrToOrtho,x(1,i),rcla(1,i),3,3,1)
      enddo
      call OpenFile(lst,fln(:ifln)//'.inb','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      LstOpened=.true.
      uloha='Program for basin integration of static density'
      call newpg(1)
      call iom50(0,1,fln(:ifln)//'.m50')
      call comsym(0,1,ich)
      if(ErrFlag.ne.0.or.ich.ne.0) go to 9000
      call TitulekVRamecku('Capture sphere radii')
      n=(nac-1)/15+1
      n2=0
      do i=1,n
        n1=n2+1
        n2=min(n1+14,nac)
        call newln(3)
        write(lst,'(15a8)')(atom(j),j=n1,n2)
        write(lst,'(15(f6.4,2x))')(RCapture(j),j=n1,n2)
        write(lst,FormA80)
      enddo
      call TitulekVRamecku('Integration parameters')
      call newln(4)
      write(lst,'(''Number of steps in phi   :'',i3/
     1            ''Number of steps in theta :'',i3/
     2            ''Number of steps in R     :'',i3/)')
     3  nPhiIntA,nThIntA,nRInt
      call newln(2)
      write(lst,'(''The cutoff distance for calculation of density :'',
     1            f8.3/)') CutOffDist
      SummEl=0.
      do IAtInt=1,nac
        if(.not.AtBrat(IAtInt)) cycle
        write(t80,'(''Results of integration for atom : '',a8)')
     1    atom(IAtInt)
        xp=0.
        call TitulekVRamecku(t80)
        call SetRealArrayTo(soph,nprops,0.)
        soph(1)=popc(IAtInt)
        do ib=0,1
          ibeta=ib
          if(ib.eq.0) then
            nPhiInt=48
            nThInt=32
          else
            nPhiInt=nPhiIntA
            nThInt =nThIntA
          endif
          nPhiThInt=nPhiInt*nThInt
          call gauleg(0.,pi2,phi,wp,nPhiInt)
          do i=1,nPhiInt
            cphi(i)=cos(phi(i))
            sphi(i)=sin(phi(i))
          enddo
          call gauleg(0.,pi,theta,wt,nThInt)
          do i=1,nThInt
            ctheta(i)=cos(theta(i))
            stheta(i)=sin(theta(i))
          enddo
          if(ibeta.eq.1) then
            call ChdZFSDetermination
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nPhiInt.gt.2) then
            if(ibeta.eq.0) then
              t80='Integration inside of beta sphere for'
            else
              t80='Integration outside of beta sphere for'
            endif
            t80=t80(:idel(t80)+1)//atom(IAtInt)
            call FeFlowChartOpen(-1.,-1.,1,nPhiInt,t80,' ',' ')
            nFlowInt= 0
          else
            nFlowInt=-1
          endif
          call ChdIntThRAllocate
          do i=1,nPhiInt
            call ChdIntThRSumm(i,soth,wt)
            if(ErrFlag.ne.0) go to 2400
            call FeFlowChartEvent(nFlowInt,ie)
            if(ie.ne.0) then
              ErrFlag=1
              go to 2400
            endif
            wpi=wp(i)
            do j=1,nprops
              soph(j)=soph(j)+wpi*soth(j)
            enddo
          enddo
2400      call ChdIntThRDeallocate
          if(nFlowInt.ge.0) call FeFlowChartRemove
          if(ErrFlag.ne.0) go to 9000
        enddo
        call newln(2)
        write(lst,'(''Number of electrons : '',f8.3)') soph(1)
        write(lst,'(''Charge in electrons : '',f8.3)')
     1    -soph(1)+AtNum(isf(IAtInt),KPhase)*ai(IAtInt)*
     2             AtSiteMult(IAtInt)
        if(NZeroNeighAll.gt.0) then
          call NewLn(5)
          write(lst,'(/''WARNING:'')')
          write(lst,'(''During the calculation the program reached '',
     1                ''points having all distances to atoms'')')
          write(lst,'(''longer than the distance cutoff limit. This '',
     1                ''could cause numerical problems.'')')
          write(lst,'(''Please try to enlarge the the distance cutoff'',
     1                '' and run it once more.'')')
        endif
        SummEl=SummEl+soph(1)/AtSiteMult(IAtInt)
      enddo
      call newln(2)
      write(lst,'(/''Sum of electrons over all selected atoms : '',
     1            f8.3)') SummEl
9000  call CloseListing
9999  if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      deallocate(Tollo)
      if(allocated(IaChd))
     1  deallocate(IaChd,IsChd,XfChd,XoChd,popasChd,Toll,RCapture,
     2             ireach,rcla)
      if(allocated(AtBrat)) deallocate(AtBrat)
      if(allocated(iapdf))
     1  deallocate(xpdf,popaspdf,idpdf,dpdf,fpdf,ypdf,SelPDF,scpdf,
     2             iapdf,ispdf,ipor)
      return
      end
      subroutine CPSearchInMap(Klic,UzitStarou)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(6),DenSupl(3),xn(6),WorkMap(:,:),Table(:)
      character*80 Veta
      logical UzitStarou,PointAlreadyPresent
      allocatable WorkMap,Table
      if(UzitStarou) then
        allocate(WorkMap(nxny,3),Table(nxny))
        go to 3650
      endif
      if(Klic.eq.0) then
        nxny=1
        mapa=7
        DensityType=5
        do i=1,6
          if(i.le.3) then
            nx(i)=nint((xmax(i)-xmin(i))/dx(i))+1
            if(i.le.2) then
              nxny=nxny*nx(i)
            else
              nmap=nx(i)
            endif
          else
            nx(i)=1
            xmin(i)=0.
            xmax(i)=0.
            dx(i)=.1
          endif
        enddo
        allocate(WorkMap(nxny,3),Table(nxny))
        m8=NextLogicNumber()
        call OpenMaps(m8,fln(:ifln)//'.z81',nxny,1)
        if(ErrFlag.ne.0) go to 9999
        write(m8,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                  (iorien(i),i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
        nvse=npdf*nxny*nx(3)
        call FeFlowChartOpen(-1.,-1.,max(nint(float(nvse)*.005),10),
     1                       nvse,'Creation of inversed gradient map',
     2                       ' ',' ')
        m=0
        Dmax=-999999.
        Dmin= 999999.
        CutOffDistA=CutOffDist*rcp(1,1,KPhase)
        CutOffDistB=CutOffDist*rcp(2,1,KPhase)
        CutOffDistC=CutOffDist*rcp(3,1,KPhase)
        xp3=xmin(3)
        do iz=1,nx(3)
          do i=1,3
            call SetRealArrayTo(WorkMap(1,i),nxny,0.)
          enddo
          do 3500ia=1,npdf
            ip=ipor(ia)
            fpdfi=fpdf(ip)
            if(fpdfi.le.0.) then
              m=m+nxny
              go to 3500
            endif
            call ConDensity(pom,ia,0,DenSupl,ich)
            xp(3)=xp3-xpdf(3,ip)
            if(abs(xp(3)).gt.CutOffDistC) then
              m=m+nxny
              go to 3500
            endif
            xp(2)=xmin(2)-xpdf(2,ip)
            n=0
            do iy=1,nx(2)
              if(abs(xp(2)).gt.CutOffDistB) then
                m=m+nx(1)
                n=n+nx(1)
                go to 3350
              endif
              xp(1)=xmin(1)-xpdf(1,ip)
              do ix=1,nx(1)
                n=n+1
                if(abs(xp(1)).gt.CutOffDistA) then
                  m=m+1
                  go to 3250
                endif
                call FeFlowChartEvent(m,is)
                if(is.ne.0) then
                  call FeBudeBreak
                  if(ErrFlag.ne.0) go to 9000
                endif
                call MultM(TrToOrtho,xp,xo,3,3,1)
                dd=VecOrtLen(xo,3)
                if(dd.gt.CutOffDist) go to 3250
                call ConDensity(dd,ia,1,DenSupl,ich)
                do i=1,3
                  WorkMap(n,i)=WorkMap(n,i)+DenSupl(i)*fpdfi
                enddo
3250            xp(1)=xp(1)+dx(1)
              enddo
3350          xp(2)=xp(2)+dx(2)
            enddo
3500      continue
          do i=1,nxny
            pom=1000./(1.+sqrt(WorkMap(i,1)**2+WorkMap(i,2)**2+
     1                         WorkMap(i,3)**2))
            Dmin=min(Dmin,pom)
            Dmax=max(Dmax,pom)
            Table(i)=pom
          enddo
          write(m8,rec=iz+1)(Table(i),i=1,nxny)
          xp3=xp3+dx(3)
        enddo
        write(m8,rec=nx(3)+2) Dmax,Dmin
        n=nx(3)+3
        write(m8,rec=n) npdf
        do i=1,npdf
          n=n+1
          ip=ipor(i)
          write(m8,rec=n) ip,iapdf(ip),ispdf(ip),fpdf(ip),
     1                    (xpdf(j,ip),j=1,3)
        enddo
        call FeFlowChartRemove
      else
        call ConCalcGeneral
      endif
3650  call DefaultFourier
      call ConFouPeaks(m8)
      close(m8)
      call OpenFile(lst,fln(:ifln)//'.cp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call CPMakeOutputStart
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3660  read(m40,FormA,end=9999) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 3660
      read(m40,'(i5)',end=9999) n
      n=0
3670  read(m40,FormA,err=3800,end=3800) Veta
      if(Veta(1:10).ne.'----------') then
        n=n+1
        go to 3670
      endif
      n=n/2
      call FeFlowChartOpen(-1.,-1.,1,n,'Itertion of CP''s',' ',' ')
      rewind m40
3680  read(m40,FormA,end=9999) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 3680
      read(m40,'(i5)',end=9999) n
      m=0
      ncp=0
      n=0
3700  read(m40,FormA,err=3800,end=3800) Veta
      if(Veta(1:10).eq.'----------') go to 3800
      read(Veta,101) xo
      n=n+1
      if(Klic.eq.0) then
        call CopyVek(xo,xp,3)
      else
        call Prevod(1,xo,xp)
      endif
      call CPSearchFromPoint(xp,1)
      read(m40,FormA,err=5000,end=5000) Veta
      call mala(Veta)
      if(index(Veta,'e').ne.0) then
        read(Veta,'(e9.3)',err=5000,end=5000) SumCP
      else
        read(Veta,'(f9.3)',err=5000,end=5000) SumCP
      endif
      call FeFlowChartEvent(m,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) then
          call newln(1)
          write(lst,'(''The search process has been interrupted'')')
          go to 5000
        endif
      endif
      go to 3700
3800  ia=NAtIndFrAll(KPhase)
      do 3820i=1,NAtIndLenAll(KPhase)
        if(ai(ia).le.0.) go to 3820
        if(.not.PointAlreadyPresent(x(1,ia),xn,xcpa,ncp,.05,.false.,
     1                              dpom,j,j,1)) then
          ncp=ncp+1
          call CopyVek(x(1,ia),xcpa(1,ncp),3)
          CPRho(ncp)=-999.
          CPType(ncp)='(3,-3)'
        endif
        ia=ia+1
3820  continue
      call CPMakeOutputFinish(3)
5000  call FeFlowChartRemove
      close(m40)
      go to 9999
9000  close(m8,status='delete')
9999  deallocate(WorkMap,Table)
      call CloseIfOpened(m40)
      return
100   format(i5)
101   format(27x,3f9.6)
      end
      subroutine CPMakeOutputStart
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*6   CPTypeP
      character*128 Ven
      real ncpt(-3:3)
      real :: xp(1)=(/0./)
      logical EqIgCase
      LstOpened=.true.
      uloha='Program for topological analysis of static density'
      call newpg(1)
      call iom50(0,1,fln(:ifln)//'.m50')
      call comsym(0,1,ich)
      if(ErrFlag.ne.0.or.ich.ne.0) go to 9999
      call newln(4)
      write(lst,'(/'' Type       x         y         z     Mult    '',
     1             ''Rho     Lap       Grad         Hess eigenvalues'',
     2             ''        Ellip''/
     2             10x,''Distances to neighbour atoms''/)')
      go to 9999
      entry  CPMakeOutputFinish(IType)
      CPTypeP='(3,-3)'
      if(NatCalc.ge.MxAtAll) call ReallocateAtoms(10)
      NAtCalc=NAtCalc+1
      call SetBasicKeysForAtom(NAtCalc)
      atom(NAtCalc)='Itself'
      do k=-3,3
        write(CPTypeP(4:5),100) k
        ncpt(k)=0.
        do i=1,ncp
          if(CPType(i).ne.CPTypeP) cycle
          call od0do1(xcpa(1,i),xcpa(1,i),3)
          call CopyVek(xcpa(1,i),x(1,NAtCalc),3)
          call specat
          call DistForOneAtom(NAtCalc,CPDGeom,1,0)
          do j=1,ndist
            l=ipord(j)
            if(.not.EqIgCase(adist(l),'Itself')) then
              call CopyVek(xdist(1,l),xcpa(1,i),3)
              call CopyVek(xcpa(1,i),x(1,NAtCalc),3)
              exit
            endif
          enddo
          call SpecPos(xcpa(1,i),xp,0,1,.05,nocc)
          ncpt(k)=ncpt(k)+1./float(nocc)
          call DistForOneAtom(NAtCalc,CPDGeom,1,1)
          if(CpRho(i).gt.-900.) then
            if(CPType(i).eq.'(3,-1)') then
              write(ven,103) CPType(i),(xcpa(j,i),j=1,3),nocc,CPRho(i),
     1                       CPDiag(1,i)+CPDiag(2,i)+CPDiag(3,i),
     2                       CPGrad(i),(CPDiag(j,i),j=1,3),
     3                       CPDiag(1,i)/CPDiag(2,i)-1.
            else
              write(ven,103) CPType(i),(xcpa(j,i),j=1,3),nocc,CPRho(i),
     1                       CPDiag(1,i)+CPDiag(2,i)+CPDiag(3,i),
     2                       CPGrad(i),(CPDiag(j,i),j=1,3)
            endif
          else
            write(ven,103) CPType(i),(xcpa(j,i),j=1,3)
          endif
          call newln(3)
          write(lst,FormA1)(ven(m:m),m=1,idel(ven))
          il=1
          do j=1,ndist
            l=ipord(j)
            if(EqIgCase(adist(l),'Itself')) cycle
            ven=' '
            if(ddist(l).gt..001) then
              write(ven(10:),'(f7.4)') ddist(l)
              ven=ven(:idel(ven))//'A from '
            else
              ven(11:)='coincides with '
            endif
            ven=ven(:idel(ven)+1)//adist(l)(:idel(adist(l)))
            if(.not.EqIgCase(SymCodeDist(l),'x,y,z'))
     1        ven=ven(:idel(ven))//'#'//
     2            SymCodeDist(l)(:idel(SymCodeDist(l)))
            il=il+1
            if(il.gt.3) call newln(1)
            write(lst,FormA1)(ven(m:m),m=1,idel(ven))
            if(ddist(l).le..001) exit
          enddo
          il=il+1
          if(il.gt.3) call newln(1)
          write(lst,FormA1)
        enddo
      enddo
      NAtCalc=NAtCalc-1
      if(IType.lt.3) go to 9000
      call newln(3)
      write(lst,'(/''CP statistics : ''/)')
      SumCP=0.
      CPTypeP='(3,-3)'
      do k=-3,3
        if(ncpt(k).le.0.) cycle
        if(k.eq.-3.or.k.eq.1) then
          zn=1.
        else if(k.eq.-1.or.k.eq.3) then
          zn=-1.
        else
          zn=0.
        endif
        SumCP=SumCP+zn*ncpt(k)
        write(CPTypeP(4:5),100) k
        call newln(1)
        write(lst,'(a6,'' number '',f8.3)') CPTypeP,ncpt(k)
      enddo
      call newln(2)
      write(lst,'(/''Checking summ : '',f8.3)') SumCP
100   format(i2)
103   format(a6,3f10.4,i5,2f9.4,e12.4,4f9.4)
9000  call CloseListing
9999  return
      end
      subroutine CPSearchAlongLine(xat1,xat2,At1,At2,JsouToAtomy,Tisk)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80,Titulek
      character*(*)  At1,At2
      integer Tisk
      logical DvaBody,PointAlreadyPresent
      dimension xat1(3),xat2(3),xcp(3),xp(6),diag(3),xn(6)
      if(JsouToAtomy.eq.1) then
        Titulek='Search for CP along bond '//At1(:idel(At1))//'-'//
     1          At2(:idel(At2))
      else
        Titulek='Search for CP along the defined line'
      endif
      do i=1,3
        xcp(i)=xat1(i)+(xat2(i)-xat1(i))*.5
      enddo
      DvaBody=.true.
      go to 1450
      entry CPSearchFromPoint(xat1,Tisk)
      DvaBody=.false.
      Titulek='Search starting from point'
      call CopyVek(xat1,xcp,3)
1450  call CPNewton(xcp,Rho,Rho2D,Grad,Diag,NonZero,NSgn,ich)
      if(ich.eq.0.and.Rho.gt.CPRhoMin) then
        Ellip=Diag(1)/Diag(2)-1.
        if(DvaBody) then
          do i=1,3
            xp(i)=xcp(i)-xat1(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d1=sqrt(ScalMul(xp,xp(4)))
          do i=1,3
            xp(i)=xcp(i)-xat2(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d2=sqrt(ScalMul(xp,xp(4)))
          do i=1,3
            xp(i)=xat1(i)-xat2(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d0=sqrt(ScalMul(xp,xp(4)))
        endif
        write(Cislo,'(''('',i1,'','',i2,'')'')') NonZero,NSgn
        if(Tisk.eq.0.or.Tisk.eq.2) then
          NInfo=5
          write(t80,'(3f8.4)') xcp
          TextInfo(1)='Critical point of type '//Cislo(:idel(Cislo))//
     1                ' found at '//t80(:idel(t80))
          k=1
          if(DvaBody) then
            write(Cislo,100) d1
            if(JsouToAtomy.eq.1) then
              t80=At1
            else
              t80='1st point'
            endif
            TextInfo(2)='Displaced '//Cislo(:6)//' from '//
     1                  t80(:idel(t80))//' and'
            write(Cislo,100) d2
            if(JsouToAtomy.eq.1) then
              t80=At2
            else
              t80='2nd point'
            endif
            TextInfo(2)=TextInfo(2)(:idel(TextInfo(2))+1)//Cislo(:6)//
     1                  ' from '//t80(:idel(t80))
            write(Cislo,100) d1+d2
            if(JsouToAtomy.eq.1) then
              t80='bond distance'
            else
              t80='distance between points'
            endif
            TextInfo(3)='Making sum '//Cislo(:6)//' which exceeds '//
     1                  t80(:idel(t80))
            write(Cislo,100) d0
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3))+1)//Cislo(:6)
            write(Cislo,100) d1+d2-d0
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3))+1)//'by '//
     1                  Cislo(:6)
            k=3
          else
            NAtCalc=NAtCalc+1
            ai(NAtCalc)=1.
            iswa(NAtCalc)=1
            kswa(NAtCalc)=KPhase
            atom(NAtCalc)='Itself'
            call CopyVek(xcp,x(1,NAtCalc),3)
            call specat
            call DistForOneAtom(NAtCalc,CPDGeom,1,0)
            NAtCalc=NAtCalc-1
            if(ndist.gt.0) then
              do 3100i=1,ndist
                j=ipord(i)
                if(adist(j).eq.'Itself') go to 3100
                k=k+1
                write(TextInfo(k),'(''Short distance '',f8.2,'' to '',
     1                              a8)') ddist(j),adist(j)
                if(k.gt.8) go to 3110
3100          continue
            else
              k=k+1
              TextInfo(k)='There are no short distances to this point'
            endif
          endif
3110      k=k+1
          write(Cislo,101) Rho
          TextInfo(k)='Local density '//Cislo(:7)//' Laplacian'
          write(Cislo,101) Rho2d
          TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))+1)//Cislo(:7)//
     1                ' Ellipticity'
          write(Cislo,101) Ellip
          TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))+1)//Cislo(:7)
          write(t80,101)(Diag(i),i=1,3)
          k=k+1
          TextInfo(k)='Eigenvalues of Hessian matrix '//t80(:idel(t80))
          NInfo=k
          call FeInfoOut(-1.,-1.,Titulek,'L')
        else
          if(.not.PointAlreadyPresent(xcp,xn,xcpa,ncp,.05,.false.,dpom,
     1                                j,j,1)) then
            ncp=ncp+1
            call CopyVek(xcp,xcpa(1,ncp),3)
            call CopyVek(Diag,CPDiag(1,ncp),3)
            CPRho(ncp)=Rho
            CPGrad(ncp)=Grad
            CPType(ncp)=Cislo
          endif
        endif
      endif
      return
100   format(f6.4)
101   format(3f7.2)
      end
      subroutine CPReadScope(Klic,CPXMin,CPXMax,CPXSt,DensityType,
     1                       CutOffDist,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension CPXMin(3),CPXMax(3),CPXSt(3),xp(6)
      integer :: CPIndUnit=0,DensityType
      real :: CPStep=.1
      ich=0
      call DefaultFourier
      OrCode=-333
      call SetRealArrayTo(xrmn,6,-333.)
      call FouDefLimits
      call NactiFourier
      call FouReadScopeCP(Klic,CPIndUnit,CPStep,DensityType,CutOffDist,
     1                    ich)
      if(ich.ne.0) go to 9999
      if(ScopeType.eq.1) then
        do j=1,3
          if(CPIndUnit.eq.1) then
            CPXMin(j)=fourmn(j,nsubs)
            CPXMax(j)=fourmx(j,nsubs)
          else
            CPXMin(j)=0.
            CPXMax(j)=1.
          endif
          n=max(nint((CPXMax(j)-CPXMin(j))*
     1          CellPar(j,nsubs,KPhase)/CPStep),1)
          CPXSt(j)=(CPXMax(j)-CPXMin(j))/float(n)
        enddo
      else if(ScopeType.eq.2) then
        CPIndUnit=0
        call CopyVek(xrmn,CPXMin,3)
        call CopyVek(xrmx,CPXMax,3)
        call CopyVek(dd  ,CPXSt ,3)
      else if(ScopeType.eq.3) then
        CPIndUnit=0
        if(ptname.ne.'[neco]') then
          call atsym(ptname,i,ptx,xp,xp(4),j,k)
          if(i.le.0) then
            call FeChybne(-1.,-1.,'atom '//ptname(:idel(ptname))//
     1                    ' isn''t on the '//'file M40',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined',SeriousError)
          endif
          if(k.eq.3) then
             call FeChybne(-1.,-1.,'atom '//ptname(:idel(ptname))//
     1                     ' isn''t correct',
     2                     'The part of the volume to be mapped '//
     3                     'isn''t defined',SeriousError)
          endif
          if(ErrFlag.ne.0) go to 9000
        endif
        do j=1,3
          nd=nint(.5*pts(j)/ptstep)
          if(nd.ne.0) then
            CPXSt(j)=ptstep/CellPar(j,nsubs,KPhase)
          else
            CPXSt(j)=1.
          endif
          CPXMin(j)=ptx(j)-CPXSt(j)*float(nd)
          CPXMax(j)=ptx(j)+CPXSt(j)*float(nd)
        enddo
      endif
      if(CPIndUnit.eq.1) then
        do j=1,3
          CPXMin(j)=CPXMin(j)-CPXSt(j)
          CPXMax(j)=CPXMax(j)+CPXSt(j)
        enddo
      endif
      do i=1,6
        iorien(i)=i
      enddo
      go to 9999
9000  ich=1
      ErrFlag=0
9999  call DefaultContour
      call NactiContour
      return
      end
      subroutine CPSearchBond
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'contour.cmn'
      dimension xat(3,2),xp(3),ddxp(3),gddxp(3)
      character*80  t80,At2
      character*8   At1
      character*2   Label
      logical       iesd
      call DefaultDist
      dmdk=CPDmin
      dmhk=CPDmax
      iuhl=0
      fullcoo=0
      VolaToDist=.false.
      call inm40(iesd)
      PrvniSymmString=' '
      do j=1,NAtCalc
        BratPrvni(j)=AtBrat(j)
        BratDruhyATreti(j)=AtBrat(j)
      enddo
      call DistAt
      call OpenFile(lst,fln(:ifln)//'.cp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call CPMakeOutputStart
      ncp=0
      rewind 78
1300  read(78,end=9000) Label,At1,i,i,xp,xp,xp(1),t80
      if(Label.ne.'#1') go to 1300
1310  At2=' '
      read(78,end=9000) Label,At2(1:8),i,i,xp,xp,xp(1),t80
      if(Label.eq.'#2') then
        i=index(t80,'#')
        id=idel(t80)
        if(i.gt.0.and.i.ne.id) At2=At2(:idel(At2))//t80(i:idel(t80))
      else if(Label.eq.'#1') then
        At1=At2
        go to 1310
      else
        go to 1300
      endif
      call AtSymi(At1,ia,xat(1,1),xp,xp,i,ich,*1310)
      call AtSymi(At2,ia,xat(1,2),xp,xp,i,ich,*1310)
      do i=1,3
        xmina(i,1)=min(xat(i,1),xat(i,2))
        xmaxa(i,1)=max(xat(i,1),xat(i,2))
      enddo
      do i=1,3
        xp(i)=(xmaxa(i,1)+xmina(i,1))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddxp(1)=(xmaxa(1,1)-xp(1))*float(i1)
        do i2=-1,1,2
          ddxp(2)=(xmaxa(2,1)-xp(2))*float(i2)
          do i3=-1,1,2
            ddxp(3)=(xmaxa(3,1)-xp(3))*float(i3)
            call multm(MetTens(1,1,KPhase),ddxp,gddxp,3,3,1)
            dd=sqrt(scalmul(ddxp,gddxp))
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      dmez=dmez+CutOffDist
      call OkoliC(xp,dmez,1,1,.true.)
      xp(1)=0.
      do j=1,npdf
        call specpos(xpdf(1,j),xp,0,1,.005,l)
        fpdf(j)=ai(iapdf(j))*float(l)
      enddo
      if(npdf.gt.0) call CPSearchAlongLine(xat(1,1),xat(1,2),At1,At2,1,
     1                                     1)
      go to 1310
9000  call CPMakeOutputFinish(1)
      close(78,status='delete')
9999  return
      end
      subroutine CPNewton(xf,Rho,Rho2D,Grad,Diag,NonZero,NSgn,ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xf(3),Hess(9),HessI(9),RhoA(10),dxf(3),xp(3),Diag(3)
      ich=0
      dump=1.
      do n=1,CPNIter
        call CPGetRhoAndDRho(xf,2,RhoA,ich)
        if(ich.ne.0) go to 9999
        Rho=RhoA(1)
        Grad=sqrt(RhoA(2)**2+RhoA(3)**2+RhoA(4)**2)
        Hess(1)=RhoA(5)
        Hess(5)=RhoA(6)
        Hess(9)=RhoA(7)
        Hess(2)=RhoA(8)
        Hess(3)=RhoA(9)
        Hess(6)=RhoA(10)
        Hess(4)=Hess(2)
        Hess(7)=Hess(3)
        Hess(8)=Hess(6)
        call MatInv(Hess,HessI,pom,3)
        call multm(HessI,RhoA(2),xp,3,3,1)
        xpp=sqrt(xp(1)**2+xp(2)**2+xp(3)**2)
        if(xpp.gt.CPMaxStep) then
          Redukce=CPMaxStep/xpp
        else
          Redukce=1.
        endif
        call multm(TrToOrthoI,xp,dxf,3,3,1)
        do i=1,3
          xf(i)=xf(i)-dump*Redukce*dxf(i)
        enddo
        if(dxf(3).gt.CPMaxStep) then
          Redukce=CPMaxStep/dxf(3)
        else
          Redukce=1.
        endif
        if(Grad.lt.CPCrit*RHoA(1)) go to 3000
        if(n.gt.2) then
          if(abs(Grad-GradOld).gt.abs(Grad-GradOlder)) dump=dump*.5
        endif
        if(n.gt.1) GradOlder=GradOld
        GradOld=Grad
      enddo
      ich=1
      go to 9999
3000  Rho2d=RhoA(5)+RhoA(6)+RhoA(7)
      HessI(1)=Hess(1)
      HessI(2)=Hess(2)
      HessI(3)=Hess(5)
      HessI(4)=Hess(3)
      HessI(5)=Hess(6)
      HessI(6)=Hess(9)
      call ql(HessI,RhoA,diag,3)
      NonZero=0
      NSgn=0
      do i=1,3
        if(diag(i).ne.0.) then
          NonZero=NonZero+1
          NSgn=NSgn+nint(sign(1.,diag(i)))
        endif
      enddo
9999  return
      end
      subroutine CPGetRhoAndDRho(xf,Type,RhoA,ich)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      integer Type
      dimension xf(3),px(10),xp(3),zz(8),nn(8),fnorm(8),popasi(64),
     1          RhoA(*),RhoD(10),DenDer(0:2),DenDerXYZ(10),STOA(10),
     2          DSto(0:2),SpX(64),SpXDer1(3,64),SpXDer2(6,64),RPop(680),
     3          FractToLocal(9),OrthoToLocal(9)
      equivalence (xp,px)
      data st/.0333333/
      ich=0
      if(Type.eq.0) then
        nRhoA=1
      else if(Type.eq.1) then
        nRhoA=4
      else
        nRhoA=10
      endif
      call SetRealArrayTo(RhoA,nRhoA,0.)
      do iat=1,npdf
        ip=ipor(iat)
        if(fpdf(ip).le.0.) cycle
        do i=1,3
          xp(i)=xf(i)-xpdf(i,ip)
        enddo
        call MultM(TrToOrtho,xp,xo,3,3,1)
        dd=VecOrtLen(xo,3)
        if(dd.gt.CutOffDist) cycle
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isw=iswa(ia)
        isfi=isf(ia)
        if(ispdf(ip).eq.0) ispdf(ip)=1
        ISym=ispdf(ip)
        ISymAbs=abs(ISym)
        if(IsymAbs.eq.1) then
          call CopyMat(TrAt(1,ia),FractToLocal,3)
        else
          call MatInv(rm(1,ISymAbs,isw,KPhase),px,pom,3)
          call MultM(TrAt(1,ia),px,FractToLocal,3,3,3)
        endif
        if(ISym.lt.0) then
          do i=1,9
            FractToLocal(i)=-FractToLocal(i)
          enddo
        endif
        call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
        call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
        jp=2
        kp=2
        popasi(1)=popas(1,ia)
        do l=1,lasmaxi-2
          n=2*l+1
          call multm(RPop(kp),popas(jp,ia),popasi(jp),n,n,1)
          kp=kp+n**2
          jp=jp+n
        enddo
        kapa1i=kapa1(ia)
        kapa2i=kapa2(ia)
        PopValDeform=kapa1i**3*popv(ia)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
        if(Type.gt.0) then
          DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
          if(Type.gt.1)
     1      DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
          call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,RhoD)
        else
          RhoD(1)=DenDer(0)
        endif
        DenDer(0)=PopValDeform*
     1    Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
        if(Type.gt.0) then
          PopValDeform=PopValDeform*kapa1i
          DenDer(1)=PopValDeform*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          if(Type.gt.1) then
            PopValDeform=PopValDeform*kapa1i
            DenDer(2)=PopValDeform*
     1        Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          endif
        endif
        call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,DenDerXYZ)
        call AddVek(RhoD,DenDerXYZ,RhoD,nRhoA)
        call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,Type,3)
        k=0
        do l=1,lasmaxi-1
          call SlaterDensityDer(dd,zz(l),nn(l),DSto)
          call RadialDer(xo(1),xo(2),xo(3),dd,DSto,Type,STOA)
          STO=DSto(0)
          pom=0.
          call SetRealArrayTo(px,nRhoA,0.)
          do n=1,2*l-1
            k=k+1
            popask=popasi(k)
            SpXk=SpX(k)
            pom=pom+popask*SpXk
            if(Type.gt.0) then
              do m=2,4
                i=m-1
                px(m)=px(m)+popask*(SpXk*STOA(m)+SpXDer1(i,k)*STO)
              enddo
              if(Type.gt.1) then
                do m=5,10
                  mm=m-4
                  call indext(mm,i,j)
                  if(i.eq.j) then
                    p=SpXk*STOA(m)+2.*SpXDer1(i,k)*STOA(i+1)+
     1                SpXDer2(mm,k)*STO
                  else
                    p=SpXk*STOA(m)+SpXDer1(i,k)*STOA(j+1)+
     1                SpXDer1(j,k)*STOA(i+1)+SpXDer2(mm,k)*STO
                  endif
                  px(m)=px(m)+popask*p
                enddo
              endif
            endif
          enddo
          RhoD(1)=RhoD(1)+pom*fnorm(l)*STO
          do i=2,nRhoA
            RhoD(i)=RhoD(i)+px(i)*fnorm(l)
          enddo
        enddo
        pom=fpdf(ip)
        do i=1,nRhoA
          RhoD(i)=RhoD(i)*pom
        enddo
        call AddVek(RhoA,RhoD,RhoA,nRhoA)
      enddo
      return
      end
      subroutine ChdGenSymmAtoms
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x1(3),FractToLocal(9),OrthoToLocal(9),RPop(680),px(9)
      do i=1,2
        NaChd=0
        do j=1,NAtCalc
          if(kswa(j).ne.KPhase.or.ai(j).le.0) cycle
          js=0
          do jsym=1,NSymmN(KPhase)
            js=js+1
            if(isa(js,j).le.0) cycle
            call multm(rm6(1,jsym,1,KPhase),x(1,j),x1,3,3,1)
            call AddVek(x1,s6(1,jsym,1,KPhase),x1,3)
            do jcenter=1,NLattVec(KPhase)
              NaChd=NaChd+1
              if(i.eq.1) cycle
              call AddVek(x1,vt6(1,jcenter,1,KPhase),XfChd(1,NaChd),3)
              call od0do1(XfChd(1,NaChd),XfChd(1,NaChd),3)
              IaChd(NaChd)=j
              IsChd(NaChd)=jsym
              lasmaxi=lasmax(j)
              if(jsym.eq.1) then
                call CopyMat(TrAt(1,j),FractToLocal,3)
              else
                call MatInv(rm(1,jsym,1,KPhase),px,pom,3)
                call MultM(TrAt(1,j),px,FractToLocal,3,3,3)
              endif
              if(lasmaxi.gt.1) then
                call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
                call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
                jp=2
                kp=2
                popasChd(1,NaChd)=popas(1,j)
                do l=1,lasmaxi-2
                  n=2*l+1
                  call multm(RPop(kp),popas(jp,j),popasChd(jp,NaChd),n,
     1                       n,1)
                  kp=kp+n**2
                  jp=jp+n
                enddo
              endif
            enddo
          enddo
        enddo
        if(i.eq.1)
     1    allocate(IaChd(NaChd),IsChd(NaChd),XfChd(3,NaChd),
     2             XoChd(3,NaChd),popasChd(64,NaChd),Toll(NaChd),
     3             RCapture(NaChd),ireach(NaChd),rcla(3,NaChd))
      enddo
      return
      end
      subroutine ChdGetRhoAndDRho(xf,Type,RhoA,ich)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      integer Type
      dimension xf(3),px(10),xp(3),zz(8),nn(8),fnorm(8),popasi(64),
     1          RhoA(*),RhoD(10),DenDer(0:2),DenDerXYZ(10),STOA(10),
     2          DSto(0:2),SpX(64),SpXDer1(3,64),SpXDer2(6,64)
      equivalence (xp,px)
      data st/.0333333/
      ich=0
      if(Type.eq.0) then
        nRhoA=1
      else if(iabs(Type).eq.1) then
        nRhoA=4
      else
        nRhoA=10
      endif
      call SetRealArrayTo(RhoA,nRhoA,0.)
      do iat=1,npdf
        ip=ipor(iat)
        if(fpdf(ip).le.0.) cycle
        do i=1,3
          xp(i)=xf(i)-xpdf(i,ip)
        enddo
        call MultM(TrToOrtho,xp,xo,3,3,1)
        dd=VecOrtLen(xo,3)
        if(dd.gt.CutOffDist) cycle
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isfi=isf(ia)
        call CopyVek(popaspdf(1,ip),popasi,(lasmaxi-1)**2)
        kapa1i=kapa1(ia)
        if(lasmaxi.gt.1) then
          kapa2i=kapa2(ia)
        else
          kapa2i=1.
        endif
        PopValDeform=kapa1i**3*popv(ia)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        RhoD=0.
        DenDerXYZ=0.
        if(Type.ge.0) then
          DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
          if(Type.gt.0) then
            DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
            if(Type.gt.1)
     1        DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
            call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,RhoD)
          else
            RhoD(1)=DenDer(0)
          endif
c        else
c          RhoD(1)=0.
        endif
        DenDer(0)=PopValDeform*
     1    Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
        if(Type.gt.0) then
          PopValDeform=PopValDeform*kapa1i
          DenDer(1)=PopValDeform*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          if(Type.gt.1) then
            PopValDeform=PopValDeform*kapa1i
            DenDer(2)=PopValDeform*
     1        Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          endif
        endif
        call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,DenDerXYZ)
        call AddVek(RhoD,DenDerXYZ,RhoD,nRhoA)
        call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,Type,3)
        k=0
        do l=1,lasmaxi-1
          call SlaterDensityDer(dd,zz(l),nn(l),DSto)
          call RadialDer(xo(1),xo(2),xo(3),dd,DSto,Type,STOA)
          STO=DSto(0)
          pom=0.
          call SetRealArrayTo(px,nRhoA,0.)
          do n=1,2*l-1
            k=k+1
            popask=popasi(k)
            SpXk=SpX(k)
            pom=pom+popask*SpXk
            if(Type.gt.0) then
              do m=2,4
                i=m-1
                px(m)=px(m)+popask*(SpXk*STOA(m)+SpXDer1(i,k)*STO)
              enddo
              if(Type.gt.1) then
                do m=5,10
                  mm=m-4
                  call indext(mm,i,j)
                  if(i.eq.j) then
                    p=SpXk*STOA(m)+2.*SpXDer1(i,k)*STOA(i+1)+
     1                SpXDer2(mm,k)*STO
                  else
                    p=SpXk*STOA(m)+SpXDer1(i,k)*STOA(j+1)+
     1                SpXDer1(j,k)*STOA(i+1)+SpXDer2(mm,k)*STO
                  endif
                  px(m)=px(m)+popask*p
                enddo
              endif
            endif
          enddo
          RhoD(1)=RhoD(1)+pom*fnorm(l)*STO
          do i=2,nRhoA
            RhoD(i)=RhoD(i)+px(i)*fnorm(l)
          enddo
        enddo
        pom=fpdf(ip)
        do i=1,nRhoA
          RhoD(i)=RhoD(i)*pom
        enddo
        call AddVek(RhoA,RhoD,RhoA,nRhoA)
      enddo
      return
      end
      subroutine ChdSearchSource(xyza,ldone,npoints,istep,iterm,
     1                           NZeroNeigh)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical Konec,ldone(*)
      dimension xyza(*),iterm(*),NZeroNeigh(*),xyz(3*mpts),xyzg(3*mpts),
     1          dx0(3*mpts),RhoArr(4*mpts),dxi(3*mpts,6),xyzt(3),dxt(3)
      dimension abx(9,9),amx(9,10)
      data amx/  0.500000,  0.416667,  0.375000,  0.348611,  0.329861,
     1           0.315592,  0.304225,  0.294868,  0.286975,  0.500000,
     2           0.666667,  0.791667,  0.897222,  0.990972,  1.076587,
     3           1.156159,  1.231011,  1.302044,  0.000000, -0.083333,
     4          -0.208333, -0.366667, -0.554167, -0.768204, -1.006920,
     5          -1.268903, -1.553035,  0.000000,  0.000000,  0.041667,
     6           0.147222,  0.334722,  0.620106,  1.017965,  1.541931,
     7           2.204905,  0.000000,  0.000000,  0.000000, -0.026389,
     8          -0.120139, -0.334177, -0.732035, -1.386993, -2.381455,
     9           0.000000,  0.000000,  0.000000,  0.000000,  0.018750,
     a           0.104365,  0.343080,  0.867046,  1.861508,  0.000000,
     1           0.000000,  0.000000,  0.000000,  0.000000, -0.014269,
     2          -0.093841, -0.355824, -1.018799,  0.000000,  0.000000,
     3           0.000000,  0.000000,  0.000000,  0.000000,  0.011367,
     4           0.086220,  0.370352,  0.000000,  0.000000,  0.000000,
     5           0.000000,  0.000000,  0.000000,  0.000000, -0.009357,
     6          -0.080390,  0.000000,  0.000000,  0.000000,  0.000000,
     7           0.000000,  0.000000,  0.000000,  0.000000,  0.007893/
      data abx/  1.000000,  1.500000,  1.916667,  2.291667,  2.640278,
     1           2.970139,  3.285731,  3.589955,  3.884823,  0.000000,
     2          -0.500000, -1.333333, -2.458333, -3.852778, -5.502083,
     3          -7.395635, -9.525207,-11.884151,  0.000000,  0.000000,
     4           0.416667,  1.541667,  3.633333,  6.931944, 11.665823,
     5          18.054539, 26.310843,  0.000000,  0.000000,  0.000000,
     6          -0.375000, -1.769444, -5.068056,-11.379894,-22.027753,
     7         -38.540361,  0.000000,  0.000000,  0.000000,  0.000000,
     8           0.348611,  1.997917,  6.731796, 17.379654, 38.020414,
     9           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     a          -0.329861, -2.223413, -8.612128,-25.124736,  0.000000,
     1           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     2           0.315592,  2.445164, 10.701468,  0.000000,  0.000000,
     3           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     4          -0.304225, -2.663169,  0.000000,  0.000000,  0.000000,
     5           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     6           0.294868/
      stpr=StepTrajInt/float(istep)
      stpi=StepTrajInt
      call SetIntArrayTo(iterm,npoints,0)
      call SetIntArrayTo(NZeroNeigh,npoints,0)
      jl=1
      jj=0
      nall = 0
      nall3=-2
1000  npth = 0
      npth3=-2
1010  nall =nall +1
      nall3=nall3+3
      if(.not.ldone(nall)) then
        npth=npth+1
        npth3=npth3+3
        call CopyVek(xyza(nall3),xyz(npth3),3)
      endif
      if(npth.lt.mpts.and.nall.lt.npoints) go to 1010
      if(npth.le.0) go to 9999
      jj=jj+1
      nterm=0
      do ik=6,1,-1
        do il=1,istep
          call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                                   NZeroNeigh(jl),npth,1)
          imp3=0
          imp4=1
          do i=1,npth
            if(iterm(jl+i-1).eq.0) then
              pom=VecOrtLen(RhoArr(imp4+1),3)
              if(abs(pom).gt.0.) then
                bilbo=stpr/pom
              else
                bilbo=0.
              endif
              do j=1,3
                xyz(imp3+j)=xyz(imp3+j)+bilbo*RhoArr(imp4+j)
              enddo
            endif
            imp3=imp3+3
            imp4=imp4+4
          enddo
          call FeEventInterrupt(Konec)
          if(Konec) go to 9000
        enddo
        call ChdCheckIfCaptured(xyz,iterm(jl),NZeroNeigh(jl),npth,nterm,
     1                          0)
        if(ErrFlag.ne.0) go to 9999
        if(nterm.eq.npth) go to 4900
        call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                                 NZeroNeigh(jl),npth,1)
        imp3=1
        imp4=2
        do i=1,npth
          if(iterm(jl+i-1).eq.0) then
            call CopyVek(RhoArr(imp4),dxi(imp3,ik),3)
            call VecOrtNorm(dxi(imp3,ik),3)
          endif
          imp3=imp3+3
          imp4=imp4+4
        enddo
      enddo
      call SetRealArrayTo(dx0,3*npth,0.)
      icount=0
3000  icount=icount+1
      call FeEventInterrupt(Konec)
      if(Konec) go to 9000
      call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                               NZeroNeigh(jl),npth,1)
      imp3=1
      imp4=2
      do j=1,npth
        if(iterm(jl+j-1).eq.0) then
          call CopyVek(RhoArr(imp4),dxi(imp3,1),3)
          call VecOrtNorm(dxi(imp3,1),3)
        endif
        imp3=imp3+3
        imp4=imp4+4
      enddo
      imp3=1
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          if(VecOrtScal(dxi(imp3,1),dx0(imp3),3).ge.0.) go to 3450
          call CopyVek(xyz(imp3),xyzt,3)
          do ll=1,istep
            call ChdGetRhoArray(RhoArr,xyzt,1,1)
            call CopyVek(RhoArr(2),dxt,3)
            call VecOrtNorm(dxt,3)
            do j=1,3
              xyzt(j)=xyzt(j)+stpr*dxt(j)
            enddo
          enddo
          call ChdGetRhoArray(RhoArr,xyzt,1,1)
          call CopyVek(xyzt,xyz(imp3+1),3)
          call CopyVek(RhoArr(2),dxi(imp3,1),3)
          call VecOrtNorm(dxi(imp3,1),3)
        endif
3450    imp3=imp3+3
      enddo
      call ChdCheckIfCaptured(xyz,iterm(jl),NZeroNeigh(jl),npth,nterm,
     1                        icount)
      if(ErrFlag.ne.0) go to 9999
      if(nterm.eq.npth) go to 4900
      imp3=0
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          j=imp3+1
          call CopyVek(xyz(j),xyzg(j),3)
          call CopyVek(dxi(j,1),dx0(j),3)
          do j=1,6
            pom=abx(6,j)*stpi
            do k=1,3
              xyz(imp3+k)=xyz(imp3+k)+dxi(imp3+k,j)*pom
            enddo
          enddo
        endif
        imp3=imp3+3
      enddo
      call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                               NZeroNeigh(jl),npth,1)
      imp3=0
      imp4=1
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          pom=VecOrtLen(RhoArr(imp4+1),3)
          if(pom.gt.0.) then
            sqt=amx(6,1)*stpi/pom
          else
            sqt=0.
          endif
          do k=1,3
            xyz(imp3+k)=xyzg(imp3+k)+sqt*RhoArr(imp4+k)
          enddo
          do j=6,1,-1
            pom=amx(6,j+1)*stpi
            do k=1,3
              l=imp3+k
              xyz(l)=xyz(l)+dxi(l,j)*pom
              if(j.le.5) dxi(l,j+1)=dxi(l,j)
            enddo
          enddo
        endif
        imp3=imp3+3
        imp4=imp4+4
      enddo
      go to 3000
4900  jl=jl+npth
      if(nall.lt.npoints) go to 1000
      go to 9999
9000  ErrFlag=1
9999  return
      end
      subroutine ChdGetRhoArray(RhoArr,xyz,n,IType)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension RhoArr(*),xyz(*),xf(3)
      is=1
      nsubs=1
      ivs=1
      if(IType.eq.0) then
        ivd=1
      else if(iabs(IType).eq.1) then
        ivd=4
      else if(iabs(IType).eq.2) then
        ivd=10
      endif
      do i=1,n
        call multm(TrToOrthoI,xyz(is),xf,3,3,1)
        call ChdGetNeighbours(xf,CutOffDist)
        call ChdGetRhoAndDRho(xf,IType,RhoArr(ivs),ich)
        is=is+3
        ivs=ivs+ivd
      enddo
      return
      end
      subroutine ChdGetRhoArrayForSelected(RhoArr,xyz,iterm,NZeroNeigh,
     1                                     n,IType)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension RhoArr(*),xyz(*),iterm(*),NZeroNeigh(*),xf(3)
      is=1
      nsubs=1
      ivs=1
      if(IType.eq.0) then
        ivd=1
      else if(IType.eq.1) then
        ivd=4
      else if(IType.eq.2) then
        ivd=10
      endif
      do i=1,n
        if(iterm(i).eq.0) then
          call multm(TrToOrthoI,xyz(is),xf,3,3,1)
          call ChdGetNeighbours(xf,CutOffDist)
          call ChdGetRhoAndDRho(xf,IType,RhoArr(ivs),ich)
          if(npdf.le.0) then
            NZeroNeigh(i)=NZeroNeigh(i)+1
            NZeroNeighAll=NZeroNeighAll+1
          endif
        endif
        is=is+3
        ivs=ivs+ivd
      enddo
      return
      end
      subroutine ChdGetNeighbours(x1,dmez)
      use Atoms_mod
      use Contour_mod
      parameter (MxNeigh=100)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x1(3),u(3),ug(3),idist(:),dxy(3)
      allocatable idist
      if(allocated(xpdf)) then
        if(ubound(xpdf,1).eq.MxNeigh) go to 1100
        deallocate(xpdf,popaspdf,idpdf,dpdf,fpdf,ypdf,SelPDF,scpdf,
     1             iapdf,ispdf,ipor)
      endif
      allocate(idist(MxNeigh),xpdf(3,MxNeigh),popaspdf(64,MxNeigh),
     1         idpdf(MxNeigh),dpdf(MxNeigh),fpdf(MxNeigh),
     2         ypdf(3,MxNeigh),SelPDF(MxNeigh),scpdf(MxNeigh),
     3         iapdf(MxNeigh),ispdf(MxNeigh),ipor(MxNeigh))
1100  npdf=0
      dmezi=dmez
      do j=1,NaChd
        do k=1,3
          dxy(k)=XfChd(k,j)-x1(k)
          dxy(k)=dxy(k)-anint(dxy(k))
        enddo
        call od0do1(dxy,dxy,3)
        do i1=-1,0
          u(1)=dxy(1)+i1
          do i2=-1,0
            u(2)=dxy(2)+i2
            do i3=-1,0
              u(3)=dxy(3)+i3
              call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
              d=scalmul(u,ug)
              if(d.ge.dmezi**2) cycle
              d=sqrt(d)
              m=d*1000.
              if(npdf.lt.MxNeigh) then
                npdf=npdf+1
                j1=npdf
              else
                j1=ipor(MxNeigh)
              endif
              do k=1,3
                xpdf(k,j1)=u(k)+x1(k)
              enddo
              idist(j1)=m
              ia=IaChd(j)
              fpdf(j1)=ai(ia)*AtSiteMult(ia)
              iapdf(j1)=ia
              ispdf(j1)=IsChd(j)
              call CopyVek(popasChd(1,j),popaspdf(1,j1),
     1                     (lasmax(ia)-1)**2)
              if(npdf.eq.MxNeigh) then
                call indexx(MxNeigh,idist,ipor)
                dmezi=idist(ipor(npdf))/1000.
              endif
            enddo
          enddo
        enddo
      enddo
      do i=1,npdf
        dpdf(i)=float(idist(i))/1000.
      enddo
      call indexx(npdf,idist,ipor)
      deallocate(idist)
      return
      end
      integer function ChdCapturedBy(xn)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xn(3),dd(3),ddr(3)
      ChdCapturedBy=0
      do i=1,NaChd
        ii=IaChd(i)
        do m=1,3
          dd(m)=XfChd(m,i)-xn(m)
          dd(m)=dd(m)-anint(dd(m))
        enddo
        call multm(MetTens(1,1,KPhase),dd,ddr,3,3,1)
        dist=scalmul(dd,ddr)
        if(dist.gt.RCapture(ii)**2) cycle
        ChdCapturedBy=i
        go to 9999
      enddo
9999  return
      end
      subroutine ChdCheckIfCaptured(xyz,iterm,NZeroNeigh,npth,nterm,
     1                              icount)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer ChdCapturedBy
      dimension xf(3),xyz(*),iterm(*),NZeroNeigh(*)
      is=0
      do i=1,npth
        if(iterm(i).eq.0) then
          pom=0.
          do j=1,3
            pom=pom+(xyz(is+j)-rcla(j,IAtInt))**2
          enddo
          if(pom.le.RCapture(IAtInt)**2) then
            iterm(i)=1
          else
            call multm(TrToOrthoI,xyz(is+1),xf,3,3,1)
            n=ChdCapturedBy(xf)
            if(n.le.0.and.float(icount)*StepTrajInt.gt.15.) then
              n=1
            endif
            if(n.gt.0) then
              iterm(i)=2
              ireach(n)=1
            else
              go to 1500
            endif
          endif
          nterm=nterm+1
        endif
1500    is=is+3
      enddo
9999  return
      end
      subroutine ChdCalcProperties(RhoArr,rr,vp,np)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension  RhoArr(*),vp(nprops,*),rr(*)
      data thr1,thr2/1e-3,2e-3/
      kk=0
      ik=1
      zchga=AtNum(isf(IAtInt),KPhase)
      do i=1,np
        if(nFlowInt.ge.0) then
          nFlowInt=nFlowInt-1
          call FeFlowChartEvent(nFlowInt,ie)
          if(ie.ne.0) then
            ErrFlag=1
            go to 9999
          endif
        endif
        vp(1,i)=RhoArr(kk+1)
        vp(4,i)=-.25*(RhoArr(kk+5)+RhoArr(kk+6)+RhoArr(kk+7))
        rx=rr(ik)  -rcla(1,IAtInt)
        ry=rr(ik+1)-rcla(2,IAtInt)
        rz=rr(ik+2)-rcla(3,IAtInt)
        r2=rx*rx+ry*ry+rz*rz
        r=sqrt(r2)
        vp(15,i)=vp(1 ,i)/r
        vp(16,i)=vp(1 ,i)*r
        vp(17,i)=vp(16,i)*r
        vp(18,i)=vp(17,i)*r
        vp(19,i)=vp(18,i)*r
        grhox=RhoArr(kk+2)
        grhoy=RhoArr(kk+3)
        grhoz=RhoArr(kk+4)
        vp(21,i)=(rx*grhox+ry*grhoy+rz*grhoz)
        vp(20,i)=vp(21,i)/r
        vp(22,i)=vp(21,i)*r
        vp(23,i)=vp(21,i)*r2
        if(vp(1,i).ge.thr1) then
          vp(24,i)=1.
          vp(25,i)=vp(1,i)
        else
          vp(24,i)=0.
          vp(25,i)=0.
        endif
        if(vp(1,i).ge.thr2) then
          vp(26,i)=1.
          vp(27,i)=vp(1,i)
        else
          vp(26,i)=0.
          vp(27,i)=0.
        endif
        vp(28,i)=1.
        vp(29,i)=-vp(1,i)*rx
        vp(30,i)=-vp(1,i)*ry
        vp(31,i)=-vp(1,i)*rz
        vp(32,i)=-vp(1,i)*(3.*rx*rx-r2)
        vp(33,i)=-vp(1,i)*(3.*rx*ry)
        vp(34,i)=-vp(1,i)*(3.*rx*rz)
        vp(35,i)=-vp(1,i)*(3.*ry*ry-r2)
        vp(36,i)=-vp(1,i)*(3.*ry*rz)
        vp(37,i)=-vp(1,i)*(3.*rz*rz-r2)
        if(vp(1,i).gt.0.) then
          vp(38,i)=-vp(1,i)*log(vp(1,i))
        else
          vp(38,i)=0.
        endif
        vp(7 ,i)=-zchga*vp(1,i)/r
        vp(9 ,i)=(zchga*rx*vp(1,i))/(r2*r)
        vp(10,i)=(zchga*ry*vp(1,i))/(r2*r)
        vp(11,i)=(zchga*rz*vp(1,i))/(r2*r)
        vp(8,i)=0.
        vp(12,i)=0.
        vp(13,i)=0.
        vp(14,i)=0.
        ik=ik+3
        kk=kk+10
      enddo
9999  return
      end
      subroutine ChdIntThR
      use Contour_mod
      parameter (maxray=500)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension soth(*),wt(*),ia(mxth),rstar(mxth),rfin(mxth),s(3),
     1          wvp(:),r(:),xar(maxray),war(maxray),RhoArr(:),vp(:,:)
      integer dirad(12)
      allocatable wvp,r,RhoArr,vp
      data dirad/12,16,20,24,28,32,36,40,48,48,64,96/
      save wvp,r,RhoArr,vp
      entry ChdIntThRAllocate
      allocate(wvp(nRInt*nThInt),r(3*nRInt*nThInt),RhoArr(mpts*10),
     1         vp(nprops,mpts))
      go to 9999
      entry ChdIntThRSumm(iPhi,soth,wt)
      call SetRealArrayTo(soth,nprops,0.)
      RR=RCapture(IAtInt)
      if(ibeta.ne.0) then
        do i=1,nThInt
          rfin(i)=sur(i,iPhi)
          if(rfin(i).lt.TInfInt-0.01) cycle
          if(i.gt.1.and.i.lt.nThInt) then
            if(abs(rfin(i)-sur(i-1,iPhi)).le.TInfInt*.5.or.
     1         abs(rfin(i)-sur(i+1,iPhi)).le.TInfInt*.5) cycle
            rfin(i)=sur(i-1,iPhi)+(sur(i+1,iPhi)-sur(i-1,iPhi))*
     1               (theta(i)-theta(i-1))/(theta(i+1)-theta(i-1))
          else
            if(iPhi.le.1.or.iPhi.ge.nPhiInt) cycle
            if(abs(rfin(i)-sur(i,iPhi-1)).le.TInfInt*.5.or.
     1         abs(rfin(i)-sur(i,iPhi+1)).le.TInfInt*.5) cycle
            rfin(i)=sur(i,iPhi-1)+(sur(i,iPhi+1)-sur(i,iPhi-1))*
     1               (phi(iPhi)-phi(iPhi-1))/(phi(iPhi+1)-phi(iPhi-1))
          endif
        enddo
        call SetRealArrayTo(rstar,nThInt,RR)
        do i=1,nThInt
          DiffR=rfin(i)-RR
          if(DiffR.le.2.) then
            ia(i)=dirad(nint(DiffR/.2)+1)
          else
            if(DiffR.le.3.) then
              ia(i)=dirad(11)
            else
              ia(i)=dirad(12)
            endif
          endif
          ia(i)=min(ia(i),nThInt)
        enddo
      else
        call SetRealArrayTo(rstar,nThInt,0.)
        call SetRealArrayTo(rfin ,nThInt,RR)
        call SetIntArrayTo(ia,nThInt,nRInt)
      endif
      n=0
      n3=0
      call SetRealArrayTo(vp,nprops*mpts,0.)
      do i=1,nThInt
        ss=stheta(i)
        s(1)=ss*cphi(iPhi)
        s(2)=ss*sphi(iPhi)
        s(3)=ctheta(i)
        ss=wt(i)*ss
        call gauleg(rstar(i),rfin(i),xar,war,ia(i))
        do j=1,ia(i)
          do k=1,3
            n3=n3+1
            r(n3)=xar(j)*s(k)+rcla(k,IAtInt)
          enddo
          n=n+1
          wvp(n)=war(j)*ss*xar(j)**2
        enddo
      enddo
      i=0
      i3=0
      pom=0.
4000  m=min(n-i,mpts)
      if(m.le.0) go to 9999
      call ChdGetRhoArray(RhoArr,r(i3+1),m,-2)
      call ChdCalcProperties(RhoArr,r(i3+1),vp,m)
      if(ErrFlag.ne.0) go to 9999
      call cultm(vp,wvp(i+1),soth,nprops,m,1)
      i =i +mpts
      i3=i3+mpts3
      go to 4000
      entry ChdIntThRDeallocate
      deallocate(wvp,r,RhoArr,vp)
9999  return
      end
      subroutine ChdZFSDetermination
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension idist(9),xp(3)
      dimension delxyz(:),xyzi(:),rinc(:),step(:),distf(:),dist(:),
     1          iterm(:),NZeroNeigh(:)
      character*80 t80
      logical lback(:),lfor(:),ldone(:)
      allocatable delxyz,xyzi,rinc,step,distf,dist,lback,iterm,lfor,
     1            ldone,NZeroNeigh
      allocate(delxyz(nPhiThInt*3),xyzi(nPhiThInt*3),rinc(nPhiThInt),
     1         step(nPhiThInt),distf(nPhiThInt),dist(nPhiThInt),
     2         iterm(nPhiThInt),lback(nPhiThInt),lfor(nPhiThInt),
     3         ldone(nPhiThInt),NZeroNeigh(nPhiThInt))
      BetaRi=RCapture(IAtInt)
      icount=0
      NZeroNeighAll=0
      if(MethZFS.eq.1) go to 3000
3000  n=1
      do i=1,nThInt
        do j=1,NPhiInt
          delxyz(n)  =stheta(i)*cphi(j)
          delxyz(n+1)=stheta(i)*sphi(j)
          delxyz(n+2)=ctheta(i)
          n=n+3
        enddo
      enddo
      if(MethZFS.eq.1) then
        call SetRealArrayTo(sur,nPhiThInt,TInfInt)
        call SetRealArrayTo(dist,nPhiThInt,0.)
        call SetLogicalArrayTo(ldone,nPhiThInt,.false.)
        ik=0
        icount=0
        do i=1,nPhiThInt
          if(ldone(i)) icount=icount+1
          lback(i)=.false.
          lfor(i)=.false.
          rinc(i)=4.*StepInt
          if(dist(i).eq.0.) then
            ii=(i-1)/NPhiInt
            jj=mod(i-1,NPhiInt)+1
            im1=ii-1
            im2=ii-2
            jm1=jj-1
            jp1=jj+1
            if(im1.lt.0) im1=ii
            if(im2.lt.0) im2=im1
            if(jp1.gt.NPhiInt) jp1=1
            if(jm1.le.0) jm1=NPhiInt
            idist(1)=NPhiInt*im1+jj
            idist(2)=NPhiInt*im1+jp1
            idist(3)=NPhiInt*im1+jm1
            idist(4)=NPhiInt*im2+jj
            idist(5)=NPhiInt*im2+jp1
            idist(6)=NPhiInt*im2+jm1
            idist(7)=NPhiInt*ii+jj
            idist(8)=NPhiInt*ii+jp1
            idist(9)=NPhiInt*ii+jm1
            sdist=0.
            pom=0.
            do jl=2,8
              ii=idist(jl)
              if(dist(ii).ne.0.) then
                sdist=sdist+dist(ii)
                pom=pom+1.
              endif
            enddo
            if(pom.gt.0.) dist(i)=sdist/pom
          endif
          if(dist(i).ne.0.) then
            step(i)=dist(i)
          else
            step(i)=BetaRi+rinc(i)
          endif
          do j=1,3
            ik=ik+1
            xyzi(ik)=delxyz(ik)*step(i)+rcla(j,IAtInt)
          enddo
        enddo
        IVarka=1
        srinc=0.
        do i=1,nPhiThInt
          srinc=srinc+rinc(i)
        enddo
        srinc=srinc/float(nPhiThInt)
        write(t80,100) IVarka,srinc,AccurInt
        Cislo='ZFS for '//atom(IAtInt)
        t80=Cislo(:idel(Cislo))//', '//t80(:idel(t80))
        call FeOpenInterrupt(-1.,-1.,t80)
      endif
3500  kk=0
      im3=0
      call ChdSearchSource(xyzi,ldone,nPhiThInt,5,iterm,NZeroNeigh)
      if(ErrFlag.ne.0) go to 9999
      im3=-3
      k=0
      srinc=0.
      do i=1,nPhiThInt
        im3=im3+3
        if(ldone(i)) cycle
        k=k+1
        if(rinc(i).le.AccurInt) then
          dist(i)=step(i)
          ldone(i)=.true.
          icount=icount+1
          cycle
        endif
        if(iterm(k).eq.1) then
          if(lback(i)) rinc(i)=rinc(i)*0.5
          step(i)=step(i)+rinc(i)
          lfor(i)=.true.
        else
          if(lfor(i)) rinc(i)=rinc(i)*0.5
          step(i)=step(i)-rinc(i)
          lback(i)=.true.
        endif
        do j=1,3
          ik=im3+j
          xyzi(ik)=delxyz(ik)*step(i)+rcla(j,IAtInt)
        enddo
        srinc=srinc+rinc(i)
      enddo
      if(icount.ne.nPhiThInt) then
        srinc=srinc/float(k)
        IVarka=IVarka+1
        write(t80,100) IVarka,srinc,AccurInt
        Cislo='ZFS for '//atom(IAtInt)
        t80=Cislo(:idel(Cislo))//', '//t80(:idel(t80))
        call FeOutputInterrupt(t80)
        go to 3500
      endif
      n=1
      SurMin=9999.
      SurMax=  0.
      SurAve=  0.
      do i=1,nThInt
        do j=1,NPhiInt
          pom=dist(n)
          SurMin=min(SurMin,pom)
          SurMax=max(SurMax,pom)
          SurAve=SurAve+pom
          sur(i,j)=pom
          sth=sin(theta(i))
          cth=cos(theta(i))
          sph=sin(phi(j))
          cph=cos(phi(j))
          xp(1)=sth*cph*dist(n)
          xp(2)=sth*sph*dist(n)
          xp(3)=cth*dist(n)
          n=n+1
        enddo
      enddo
      call NewLn(5)
      write(lst,'(/''Minimal ZFS distance : '',f8.3 )') SurMin
      write(lst,'( ''Maximal ZFS distance : '',f8.3 )') SurMax
      write(lst,'( ''Average ZFS distance : '',f8.3/)')
     1  SurAve/float(nPhiThInt)
9999  call FeCloseInterrupt
      deallocate(delxyz,xyzi,rinc,step,distf,dist,iterm,lback,lfor,
     1           ldone,NZeroNeigh)
      return
100   format('Cycle :',i3,', step/accuracy : ',f6.4,'/',f6.4)
      end

