      subroutine SetBasicConstants
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      CumulAt(1)=85
      CumulAt(2)=CumulAt(1)+68
      do i=3,9
        j=i-1
        CumulAt(i)=CumulAt(j)+2*TRank(j-2)*mxw
        if(i.eq.3) CumulAt(i)=CumulAt(i)+1
      enddo
      CumulAt(10)=CumulAt(9)+1
      CumulAt(11)=CumulAt(10)+(2*mxw+1)*3
      CumulMol(1)=28
      CumulMol(2)=CumulMol(1)+1+2*mxw
      CumulMol(3)=CumulMol(2)+ 6*mxw
      CumulMol(4)=CumulMol(3)+ 6*mxw
      CumulMol(5)=CumulMol(4)+12*mxw
      CumulMol(6)=CumulMol(5)+12*mxw
      CumulMol(7)=CumulMol(6)+18*mxw
      CumulMol(8)=CumulMol(7)+1
      HistoryFile=RootDir(:idel(RootDir))//'jana2006.hst'
      sqrt8ln2=sqrt(8.*log(2.))
      rsqrt2=1./sqrt(2.)
      LorentzToStrain=ToRad
      GaussToStrain=sqrt8ln2*LorentzToStrain
      IShiftPwd=1
      IBackgPwd=IShiftPwd+8
      IRoughPwd=IBackgPwd+MxBackg
      ILamPwd=IRoughPwd+2
      IAsymPwd=ILamPwd+2
      ITOFAbsPwd=IAsymPwd+12
      ICellPwd=IShiftPwd+NParRecPwd*MxDatBlock
      IQuPwd=ICellPwd+6
      IGaussPwd=IQuPwd+9
      ILorentzPwd=IGaussPwd+4
      IZetaPwd=ILorentzPwd+5
      IStPwd=IZetaPwd+1
      IPrefPwd=IStPwd+35
      IBroadHKLPwd=IPrefPwd+10
      ColorSat=Green
      do i=1,MxDatBlock
        write(DatBlockName(i),'(''Block'',i2)') i
        call Zhusti(DatBlockName(i))
      enddo
      do i=1,MxRefBlock
        write(RefBlockName(i),'(''Block'',i2)') i
        call Zhusti(RefBlockName(i))
      enddo
      call SetIntArrayTo(SimColorMain,3,Yellow)
      SimColorSat  =Cyan
      SimColorPlus =Yellow
      SimColorMinus=Cyan
      SimColorUnobs=White
      SimColorTwin(1)=Yellow
      SimColorTwin(2)=Green
      SimColorTwin(3)=Red
      SimColorTwin(4)=Cyan
      SimColorTwin(5)=Magenta
      SimColorTwin(6)=Blue
      SimColorTwin(7:18)=White
      if(.not.Console) call CreateCIFBasicFiles
      do i=1,50
        TextInfo(i)=' '
        TextInfoFlags(i)='LN'
      enddo
      call SetRealArrayTo(RMatCalc,216,0.)
      UseVestaForMagDraw=.true.
      YBottomMessage=YLenBasWin*.25
      maxNDim=3
      return
      end
      subroutine SetFePCConstants
      include 'fepc.cmn'
      ObrLom=char(92)
      Tabulator=char(9)
      do i=-500,500
        IdNumbers(i)=i
      enddo
      factorial(0)=1.
      do i=1,20
        factorial(i)=factorial(i-1)*float(i)
      enddo
      call FeLineType(NormalLine)
      return
      end
      subroutine SetCommands(KteryProgram)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeMenu
      character*8  men(3)
      data men/'%Fourier','%Refine','%Dist'/
      StartProgram=.false.
      if(KteryProgram.le.0) then
        KteryProgram=FeMenu(-1.,-1.,men,1,3,1,0)
        if(KteryProgram.le.0) go to 9999
      endif
      if(KteryProgram.eq.1) then
        call FouSetCommands(ich)
      else if(KteryProgram.eq.2) then
        call RefSetCommands
      else if(KteryProgram.eq.3) then
        call DistSetCommands
      endif
9999  return
      end
      subroutine JanaReset(Change)
      use Atoms_mod
      use Basic_mod
      use Powder_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      character*256 t256
      character*80 PuvodniSoubor
      logical ExistFile,FileDiff,Change,PwdCheckTOFInD
      Change=.false.
      call CrlCleanSymmetry
      ISymmBasic=0
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
1000  if(ifln.gt.0) then
        do i=1,9
          ExistMFile(i)=ExistFile(fln(:ifln)//ExtMFile(i))
        enddo
      else
        ExistMFile=.false.
      endif
      do i=1,ubound(SpecMatrixName,1)
1100    j=index(SpecMatrixName(i),'#')
        if(j.gt.0) then
          SpecMatrixName(i)(j:j)=Tabulator
          go to 1100
        endif
      enddo
      j=1
      do 1200i=1,9
        if(ExistMFile(i)) then
          t256=fln(:ifln)//ExtMFile(i)
          call CheckFileIfNotReadOnly(t256,1)
          if(ErrFlag.ne.0) go to 1150
          call CheckEOLOnFile(t256,j)
          j=0
          if(ErrFlag.eq.0) go to 1200
1150      fln=' '
          ifln=0
          go to 1000
        endif
1200  continue
      SetMetAllowed=.true.
      if(ExistM50) then
        call CrlTestJana2000(Change)
        if(ExistFile(HistoryFileOld)) then
          if(Change) then
            call MoveFile(HistoryFileOld,HistoryFile)
          else
            call DeleteFile(HistoryFileOld)
          endif
        endif
        if(fln.eq.' ') go to 9999
      else
        call DeleteFile(HistoryFileOld)
      endif
      if(ExistM90) then
        if(ExistM95) call iom95(0,fln(:ifln)//'.m95')
        call iom90(0,fln(:ifln)//'.m90')
        isPowder=iabs(DataType(KDatBlock)).eq.2
        call PwdSetTOF(KDatBlock)
      else
        if(ExistM95) then
          NDatBlock=1
          KDatBlock=1
          call SetBasicM90(1)
          isPowder=ExistM41
          ExistPowder=isPowder
          call iom95(0,fln(:ifln)//'.m95')
          Radiation(KDatBlock)=RadiationRefBlock(1)
          if(isPowder) then
            DataType(KDatBlock)=2
          else
            DataType(KDatBlock)=1
          endif
          call PwdSetTOF(KDatBlock)
        endif
      endif
      if(ExistM50) then
        PuvodniSoubor=fln(:ifln)//'.m50'
        if(FileDiff(PuvodniSoubor,PreviousM50)) then
          call iom50(0,0,PuvodniSoubor)
          WrongM50=ErrFlag.ne.0
          call CopyFile(PuvodniSoubor,PreviousM50)
        endif
      else
        StructureName=' '
        call SetBasicM50
      endif
      if(ParentStructure) then
        PuvodniSoubor=fln(:ifln)//'.m44'
      else
        PuvodniSoubor=fln(:ifln)//'.m40'
      endif
      if(ExistM40) then
        if(FileDiff(PuvodniSoubor,PreviousM40)) go to 1330
        if(ExistPowder) then
          if(FileDiff(fln(:ifln)//'.m41',PreviousM41)) go to 1330
        endif
        go to 1350
1330    WrongM40=.false.
        WrongM41=.false.
        if(.not.WrongM50) call iom40(0,0,PuvodniSoubor)
        if(ErrFlag.eq.0) call CrlAtomNamesIni
      else
        call AllocateAtoms(1)
        call SetBasicM40(.false.)
        ExistM41=ExistPowder
        WrongM40=.false.
        WrongM41=.false.
        if(ExistM50) then
          call iom40(1,0,fln(:ifln)//'.m40')
          ExistM40=.true.
        endif
      endif
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
      else if(ErrFlag.ne.0) then
        if(ErrFlag.eq.2) then
          WrongM41=.true.
        else
          WrongM40=.true.
        endif
      endif
      call CopyFile(PuvodniSoubor,PreviousM40)
      if(ExistPowder) then
        call CopyFile(fln(:ifln)//'.m41',PreviousM41)
        if(ExistM90) then
          KDatBlockIn=KDatBlock
          do KDatBlock=1,NDatBlock
            call PwdSetTOF(KDatBlock)
            if(iabs(DataType(KDatBlock)).eq.2) call PwdM92Nacti
          enddo
          KDatBlock=KDatBlockIn
          call PwdSetTOF(KDatBlock)
        endif
        if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
      endif
1350  if(.not.WrongM40.and..not.WrongM50) call SpecAt
      if(ExistFile(fln(:ifln)//'.m42')) then
        call iom42(0,0,fln(:ifln)//'.m42')
        call CopyFile(fln(:ifln)//'.m42',PreviousM42)
      endif
      IgnoreE=.false.
      IgnoreW=.false.
      LstOpened=.false.
      MakeCIFForGraphicViewer=.false.
      ContourCallFourier=.false.
      RefineCallFourier=.false.
      if(NDatBlock.gt.1) then
        do i=1,NDatBlock
          MenuDatBlock(i)=DatBlockName(i)
          if(DataType(i).eq.1) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->single crystal'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->powder'
          endif
          if(Radiation(i).eq.NeutronRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/neutrons'
          else if(Radiation(i).eq.XRayRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/X-ray'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/electron'
          endif
        enddo
      endif
      call DRMakeMenuRefBlock
9999  call RewriteTitle(' ')
      call FeBottomInfo(' ')
      return
100   format(i1)
101   format(6i5)
      end
      subroutine TestUzavreni
      include 'fepc.cmn'
      character*80 Veta
      do i=1,mxquest
        if(QuestState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Quest '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          QuestState(i)=0
        endif
      enddo
      do i=1,mxbut
        if(ButtonState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Button '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          ButtonState(i)=0
        endif
      enddo
      do i=1,mxcrw
        if(CrwState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Crw '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          CrwState(i)=0
        endif
      enddo
      do i=1,mxedw
        if(EdwState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Edw '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          EdwState(i)=0
        endif
      enddo
      do i=1,mxIcon
        if(IconState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Icon '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          IconState(i)=0
        endif
      enddo
      do i=0,MxTabs
        if(NTabs(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''TabSystem '',i2,'' zustal otevren:'',i4)')
     1        i,NTabs(i)
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          call FeTabsReset(i)
        endif
      enddo
      UseTabs=0
      return
      end
      subroutine Najdi(Program,Nalezen)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Program
      character*80 t80
      logical EqIgCase
      Nalezen=0
1000  read(m50,FormA,end=2000) t80
      k=0
      call kus(t80,k,Cislo)
      if(EqIgCase(Cislo,program)) then
        if(NPhase.gt.1.and.k.lt.80) then
          call kus(t80,k,Cislo)
          if(.not.EqIgCase(Cislo,PhaseName(KPhase))) go to 1000
        else if(KPhase.gt.1.and..not.EqIgCase(Cislo,'refine')) then
          go to 1000
        endif
        Nalezen=1
      else
        go to 1000
      endif
2000  return
      end
      subroutine NajdiLn(Ln,Program,Nalezen)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Program
      character*80 t80
      logical EqIgCase
      Nalezen=0
1000  read(ln,FormA,end=2000) t80
      k=0
      call kus(t80,k,Cislo)
      if(EqIgCase(Cislo,program)) then
        if(NPhase.gt.1.and.k.lt.80) then
          call kus(t80,k,Cislo)
          if(.not.EqIgCase(Cislo,PhaseName(KPhase))) go to 1000
        else if(KPhase.gt.1.and..not.EqIgCase(Cislo,'refine')) then
          go to 1000
        endif
        Nalezen=1
      else
        go to 1000
      endif
2000  return
      end
      subroutine NewPg(First)
      include 'fepc.cmn'
      character*9 ch9
      character*8 ch8
      integer First
      save ch8,ch9
      if(First.eq.1) then
        call FeDateAndTime(ch9,ch8)
        page=0
        line=mxline
        go to 9000
      endif
      if(page.gt.0) then
        do i=line+1,mxline
          write(lst,'(1x)')
        enddo
      endif
      page=page+1
      write(lst,100) uloha,page,StructureName,ch8,ch9
      line=3
9000  return
100   format(a80,36x,'page=',i3/
     1       'structure : ',a80,15x,a8,' ',a9/)
      end
      subroutine newln(iz)
      include 'fepc.cmn'
      i=line+iz
      if(i.gt.mxline) then
        call newpg(0)
        line=line+iz
      else
        line=i
      endif
      return
      end
      subroutine ChybaNacteni(Ktera)
      include 'fepc.cmn'
      call FeChybne(-1.,-1.,'"'//NactiKeywords(Ktera)(1:idel(
     1              NactiKeywords(Ktera)))//'" will be skipped '//
     2              'because of syntactic error',NactiVeta,Warning)
      return
      end
      subroutine QuestionRewriteFile(m)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*2 lf
      character*80 FileL,FileM,File42,Veta
      logical FeYesNo,FileDiff,Diff,ExistFile,ExistL,Jina
      write(lf,'(i2)') m
      if(m.ne.40.and.m.ne.41.and.m.ne.50) then
        call FeChybne(-1.,-1.,'wrong argument in RewriteFile : '//lf,
     1                ' ',SeriousError)
        go to 9999
      endif
      Jina=.false.
      if(m.eq.40) then
        FileL=PreviousM40
      else if(m.eq.50) then
        FileL=PreviousM50
        if(ExistElectronData.and.ExistM42.and.CalcDyn) then
          call iom42(1,0,fln(:ifln)//'.m42')
          File42=PreviousM42
        endif
      else
        FileL=fln(:ifln)//'.l'//lf
        Jina=.true.
      endif
      FileM=fln(:ifln)//'.m'//lf
      if(ExistFile(FileM)) then
        if(Jina) call MoveFile(FileM,FileL)
      else
        call DeleteFile(FileL)
        if(m.eq.41) call DeleteFile(PreviousM50)
      endif
      ExistL=ExistFile(FileL)
      if(m.eq.50.or.m.eq.41) call iom50(1,0,fln(:ifln)//'.m50')
      if(m.eq.40.or.m.eq.41) call iom40(1,0,fln(:ifln)//'.m40')
      if(ExistFile(FileM)) then
        if(ExistL) then
          Diff=FileDiff(FileM,FileL)
          if(m.eq.41.and..not.Diff)
     1      Diff=FileDiff(fln(:ifln)//'.m50',PreviousM50)
            if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1        Diff=FileDiff(fln(:ifln)//'.m42',PreviousM42)
        else
          Diff=.true.
        endif
      else
        call CopyFile(FileL,FileM)
        go to 9999
      endif
      if(Diff) then
        if(m.eq.50.and.ExistElectronData.and.ExistM42.and.CalcDyn) then
          Veta='Do you want to rewrite M42/M50 files?'
        else
          Veta='Do you want to rewrite M'//lf//' file?'
        endif
        if(FeYesNo(-1.,-1.,Veta,1))
     1    then
          go to 9999
        else
          call CopyFile(FileL,FileM)
          if(m.eq.50.and.ExistElectronData.and.ExistM42.and.CalcDyn)
     1      call CopyFile(PreviousM42,fln(:ifln)//'.m42')
        endif
      endif
9999  if(Jina) call DeleteFile(FileL)
      return
      end
      subroutine srotc(a,n,b)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(3,3),b(*),ia(6),ja(6)
      m=inm(n-2)
      ioffmn=ioffm(n-2)
      ioffvn=ioffv(n-2)
      idenp=4**(n-1)
      do i=1,m
        k=ipoc(i+ioffmn)+ioffvn
        ip=ipec(k)
        iden=idenp
        do k=1,n
          ia(k)=ip/iden
          ip=ip-ia(k)*iden
          iden=iden/4
        enddo
        ij=i
        do j=1,m
          bij=0.
          idlcj=idlc(j+ioffmn)
          ipocj=ipoc(j+ioffmn)
          kp=ipocj
          kk=kp+idlcj-1
          do 1400k=kp,kk
            ip=ipec(k+ioffvn)
            iden=idenp
            do l=1,n
              ja(l)=ip/iden
              ip=ip-ja(l)*iden
              iden=iden/4
            enddo
            pom=1.
            do l=1,n
              p=a(ia(l),ja(l))
              if(p.eq.0.) go to 1400
              pom=pom*p
            enddo
            bij=bij+pom
1400      continue
          b(ij)=bij
          ij=ij+m
        enddo
      enddo
      return
      end
      subroutine indexc(k,n,ia)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(*)
      ioffvn=ioffv(n-2)
      ioffmn=ioffm(n-2)
      i=ipoc(k+ioffmn)+ioffvn
      ip=ipec(i)
      iden=4**(n-1)
      do i=1,n
        ia(i)=ip/iden
        ip=ip-ia(i)*iden
        iden=iden/4
      enddo
      return
      end
      subroutine NToStrainString(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String
      dimension ia(3),ifa(4)
      if(n.le.15) then
        call indexc(n,4,ifa)
        do i=1,3
          ia(i)=0
        enddo
        do i=1,4
          j=ifa(i)
          ia(j)=ia(j)+1
        enddo
        write(String,'(3i1)') ia
      else
        if(n.eq.16) then
          String=' m'
        else if(n.eq.17) then
          String=' n'
        else if(n.eq.18) then
          String=' p'
        endif
        write(String(1:1),'(i1)') NDim(KPhase)
      endif
      return
      end
      subroutine StrainIndToN(ia,n)
      dimension ia(3)
      ip=0
      kp=1
      do i=1,3
        kk=kp+ia(i)-1
        do k=kp,kk
          ip=ip*4+i
        enddo
        kp=kk+1
      enddo
      n=nindc(ip,4)
      return
      end
      subroutine NToString(n,String)
      include 'fepc.cmn'
      character*(*) String
      character*15  t15
      write(t15,FormI15) n
      call zhusti(t15)
      String=t15
      return
      end
      function nindc(k,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      ioffvn=ioffv(n-2)
      ioffmn=ioffm(n-2)
      do i=1,inv(n-2)
        if(k.eq.ipec(i+ioffvn)) go to 1100
      enddo
      nindc=-1
      go to 9999
1100  do j=1,inm(n-2)
        if(i.lt.ipoc(j+ioffmn)+idlc(j+ioffmn)) go to 2100
      enddo
      nindc=-1
      go to 9999
2100  nindc=j
9999  return
      end
      subroutine srotc4(a,n,b)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(4,4),b(*),ia(6),ja(6)
      m=35
      idenp=4**(n-1)
      do i=1,m
        k=ipoc4(i)
        ip=ipec4(k)
        do k=n,1,-1
          ia(k)=mod(ip-1,4)+1
          ip=(ip-1)/4
        enddo
        ij=i
        do j=1,m
          bij=0.
          idlcj=idlc4(j)
          ipocj=ipoc4(j)
          kp=ipocj
          kk=kp+idlcj-1
          do 1400k=kp,kk
            ip=ipec4(k)
            do l=n,1,-1
              ja(l)=mod(ip-1,4)+1
              ip=(ip-1)/4
             enddo
            pom=1.
            do l=1,n
              p=a(ia(l),ja(l))
              if(p.eq.0.) go to 1400
              pom=pom*p
            enddo
            bij=bij+pom
1400      continue
          b(ij)=bij
          ij=ij+m
        enddo
      enddo
      return
      end
      subroutine indexc4(k,n,ia)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(*)
      i=ipoc4(k)
      ip=ipec4(i)
      do i=n,1,-1
        ia(i)=mod(ip-1,4)+1
        ip=(ip-1)/4
      enddo
      return
      end
      subroutine NToStrainString4(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String
      dimension ia(4),ifa(4)
      if(n.le.35) then
        call indexc4(n,4,ifa)
        call SetIntArrayTo(ia,4,0)
        do i=1,4
          j=ifa(i)
          ia(j)=ia(j)+1
        enddo
        write(String,'(4i1)') ia
      endif
      return
      end
      function nindc4(k,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,4**n
        if(k.eq.ipec4(i)) go to 1100
      enddo
      nindc4=-1
      go to 9999
1100  do j=1,35
        if(i.lt.ipoc4(j)+idlc4(j)) go to 2100
      enddo
      nindc4=-1
      go to 9999
2100  nindc4=j
9999  return
      end
      subroutine StrainIndToN4(ia,n)
      dimension ia(4)
      ip=0
      kp=1
      do i=1,4
        kk=kp+ia(i)-1
        do k=kp,kk
          ip=ip*4+i
        enddo
        kp=kk+1
      enddo
      n=nindc4(ip,4)
      return
      end
      subroutine normtr(x,isw,klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*)
      do i=1,NDim(KPhase)
        if(x(i).ge..0001.and.x(i).lt..9999) cycle
        xp=ifix(x(i))
        if(x(i).lt.0.) xp=xp-1.
        x(i)=x(i)-xp
        if(1.-x(i).lt..0001.or.x(i).lt..0001) x(i)=0.
        if(abs(x(i)-.5).lt..0001) x(i)=.5
      enddo
      if(klic.eq.0) return
      do i=1,NDim(KPhase)
        xp=100.
        n=0
        do 3000j=1,NLattVec(KPhase)
          do k=1,i-1
            if(vt6(k,j,isw,KPhase).ne.0.) go to 3000
          enddo
          if(vt6(i,j,isw,KPhase).eq.0.) go to 3000
          if(vt6(i,j,isw,KPhase).lt.xp) then
            n=j
            xp=vt6(i,j,isw,KPhase)
          endif
3000    continue
        if(n.eq.0) cycle
3100    if(x(i).lt.xp-.0001) go to 3200
        do k=i,NDim(KPhase)
          x(k)=x(k)-vt6(k,n,isw,KPhase)
          if(x(k).lt.-.0001.and.k.ne.i) x(k)=x(k)+1.
        enddo
        go to 3100
3200    if(x(i).ge.-.0001) go to 3300
        do k=i,NDim(KPhase)
          x(k)=x(k)+vt6(k,n,isw,KPhase)
          if(x(k).ge.1..and.k.ne.i) x(k)=x(k)-1.
        enddo
        go to 3200
3300    if(abs(x(i)).lt..0001) x(i)=0.
      enddo
      return
      end
      logical function StructureExists(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*(*) StrName
      logical   ExistFile
      i=idel(StrName)
      if(i.le.0) then
        StructureExists=.false.
      else
        t256=StrName
        StructureExists=ExistFile(t256(:i)//'.m40').or.
     1                  ExistFile(t256(:i)//'.m50').or.
     2                  ExistFile(t256(:i)//'.m91')
      endif
      return
      end
      subroutine ChangeUSDFile(StrName,Lbl1,Lbl2)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName,Lbl1,Lbl2
      character*256  t256
      character*10  Slovo,Lbl1A(2),Lbl2A(2)
      character*1   String(256)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,CR
      logical ExistFile,EqIgCase,StructureExists
      equivalence (t256,String)
      data CR,LF/13,10/
      data Lbl1A/'opened','closed'/,
     1     Lbl2A/'unlocked','locked'/
      if(StrName.eq.' '.or..not.StructureExists(StrName)) go to 5000
      WorkString=StrName(:idel(StrName))//'.usd'
      if(ExistFile(WorkString)) then
        ln=FeOpenBinaryFile(WorkString(:idel(WorkString)))
        if(ln.le.0) go to 5000
        i=FeReadBinaryFile(ln,String,256)
        i=FeCloseBinaryFile(ln)
        do i=1,256
          j=ichar(t256(i:i))
          if(j.eq.CR.or.j.eq.LF) then
            t256(i:)=' '
            go to 1500
          endif
        enddo
        go to 5000
1500    k=0
        i1=0
        i2=0
2000    call kus(t256,k,Slovo)
        if(EqIgCase(Slovo,Lbl1A(1))) then
          i1=i1+1
        else if(EqIgCase(Slovo,Lbl1A(2))) then
          i1=i1+2
        else if(EqIgCase(Slovo,Lbl2A(1))) then
          i2=i2+1
        else if(EqIgCase(Slovo,Lbl2A(2))) then
          i2=i2+2
        endif
        if(k.lt.len(t256)) go to 2000
      else
        i1=1
        i2=1
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,WorkString,'formatted','unknown')
      if(EqIgCase(Lbl1,Lbl1A(1))) then
        i1=1
      else if(EqIgCase(Lbl1,Lbl1A(2))) then
        i1=2
      endif
      if(EqIgCase(Lbl2,Lbl2A(1))) then
        i2=1
      else if(EqIgCase(Lbl2,Lbl2A(2))) then
        i2=2
      endif
      write(t256,'(a10,1x,a10)') Lbl1A(i1),Lbl2A(i2)
      call ZdrcniCisla(t256,2)
      write(ln,FormA1)(t256(i:i),i=1,idel(t256))
      call CloseIfOpened(ln)
5000  return
      end
      logical function StructureOpened(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName
      character*80  t80
      character*10  Slovo
      character*1   String(80)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,CR
      logical   ExistFile,EqIgCase
      equivalence (t80,String)
      data CR,LF/13,10/
      StructureOpened=.false.
      WorkString=StrName(:idel(StrName))//'.usd'
      if(ExistFile(WorkString)) then
        ln=FeOpenBinaryFile(WorkString(:idel(WorkString)))
        if(ln.le.0) go to 5000
        i=FeReadBinaryFile(ln,String,80)
        i=FeCloseBinaryFile(ln)
        do i=1,80
          j=ichar(t80(i:i))
          if(j.eq.CR.or.j.eq.LF) then
            t80(i:)=' '
            go to 1500
          endif
        enddo
        go to 5000
1500    k=0
2000    call kus(t80,k,Slovo)
        if(EqIgCase(Slovo,'opened')) then
          StructureOpened=.true.
        else
          if(k.lt.len(t80)) go to 2000
        endif
      endif
5000  return
      end
      logical function GetStructureLocked(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName
      character*80  t80
      character*10  Slovo
      character*1   String(80)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,CR
      logical ExistFile,EqIgCase
      equivalence (t80,String)
      data CR,LF/13,10/
      GetStructureLocked=.false.
      WorkString=StrName(:idel(StrName))//'.usd'
      if(ExistFile(WorkString)) then
        ln=FeOpenBinaryFile(WorkString(:idel(WorkString)))
        if(ln.le.0) go to 5000
        i=FeReadBinaryFile(ln,String,80)
        i=FeCloseBinaryFile(ln)
        do i=1,80
          j=ichar(t80(i:i))
          if(j.eq.CR.or.j.eq.LF) then
            t80(i:)=' '
            go to 1500
          endif
        enddo
        go to 5000
1500    k=0
2000    call kus(t80,k,Slovo)
        if(EqIgCase(Slovo,'locked')) then
          GetStructureLocked=.true.
          go to 5000
        else
          if(k.lt.len(t80)) go to 2000
        endif
      endif
5000  return
      end
      subroutine OpenMaps(ln,FileName,lrec,Key)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileName
      character*80 t80
      character*40 t40
      logical   ExistFile,ef
      logical   opened
      ErrFlag=0
      inquire(file=FileName,opened=opened,err=8000)
      if(opened) then
        t80='attempt to open already opened file'
        goto 9100
      else
        ef=ExistFile(FileName)
      endif
      if(Key.eq.0) then
        if(.not.ef) then
          t80='cannot be opened as an old file, it doesn''t exist'
          go to 9000
        endif
        open(ln,file=FileName,form='unformatted',status='old',
     1       access='direct',recl=35*RecLenFacUnform,err=8000)
        read(ln,rec=1,err=8000)(lrec,i=1,7)
        call CloseIfOpened(ln)
        open(ln,file=FileName,form='unformatted',status='unknown',
     1       access='direct',recl=max(lrec,35)*RecLenFacUnform,err=8000)
      else
        call DeleteFile(FileName)
        open(ln,file=FileName,form='unformatted',status='unknown',
     1       access='direct',recl=max(lrec,35)*RecLenFacUnform,err=8100)
      endif
      go to 9999
8000  t80='the file exists but it is probably corrupted'
      go to 9000
8100  t80='undeterminable problem'
9000  ErrFlag=1
9100  call FeCutName(FileName,t40,40,CutTextFromLeft)
      if(ErrFlag.eq.0) then
        i=Warning
      else
        i=SeriousError
      endif
      call FeChybne(-1.,-1.,'the file "'//t40(:idel(t40))//'"',t80,i)
9999  return
      end
      subroutine SetIgnoreWTo(Value)
      include 'fepc.cmn'
      logical IgnoreWSave,Value
      data IgnoreWSave/.false./
      IgnoreWSave=IgnoreW
      IgnoreW=Value
      go to 9999
      entry ResetIgnoreW
      IgnoreW=IgnoreWSave
9999  return
      end
      subroutine SetIgnoreETo(Value)
      include 'fepc.cmn'
      logical IgnoreESave,Value
      data IgnoreESave/.false./
      IgnoreESave=IgnoreE
      IgnoreE=Value
      go to 9999
      entry ResetIgnoreE
      IgnoreE=IgnoreESave
9999  return
      end
      subroutine RewriteTitle(Text)
      include 'fepc.cmn'
      character*(*) Text
      character*80  t80
      t80=MasterName
      if(Text.ne.' ') t80=t80(:idel(t80))//' - '//Text
      call FeWindowTitle(t80)
      RunningProgram=Text
      return
      end
      subroutine FeBottomInfo(TextIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) TextIn
      character*256 TextLong,TextOld,Text
      logical EqIgCase
      data TextOld/' '/
      if(BatchMode.or.Console.or.YBottomText.le.0.) go to 9999
      if(YBottomText.le.0.) go to 9999
      if(TextIn.eq.' ') then
        if(EqIgCase(MasterName,'Notarius')) then
          Text=' '
        else
          TextLong=CurrentDir
          if(OpSystem.eq.1) then
            i=idel(RootDir)
            if(index(TextLong,RootDir(:i)).eq.1)
     1        TextLong='~/'//TextLong(i+1:)
          endif
          if(ifln.gt.0) then
            TextLong(idel(TextLong)+1:)=fln(:ifln)
            if(ParentStructure) then
              TextLong=TextLong(:idel(TextLong))//
     1                 ' (parent structure !!!)'
            endif
          else
            TextLong(idel(TextLong)+1:)='---'
          endif
          call FeCutNameLength(TextLong,Text,XLenBasWin-100.,
     1                         CutTextFromLeft)
          Text='Structure: '//Text(:idel(Text))
        endif
      else if(EqIgCase(TextIn,'#prazdno#')) then
        Text=' '
      else
        Text=TextIn
      endif
      call FeTextErase(0,5.,YBottomText,TextOld,'L',LightGray)
      call FeOutSt(0,5.,YBottomText,Text,'L',Black)
      TextOld=Text
9999  return
      end
      subroutine DeletePomFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      character*128 FileName
      FileName=fln
      do i=0,99
        write(FileName(ifln+1:ifln+4),'(''.l'',i2)') i
        if(i.lt.10) FileName(ifln+3:ifln+3)='0'
        call DeleteFile(FileName)
      enddo
      call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z50')
      call DeleteFile(fln(:ifln)//'.z90')
      call DeleteFile(fln(:ifln)//'.z91')
      call DeleteFile(fln(:ifln)//'.z95')
      call DeleteFile(fln(:ifln)//'.n40')
      call DeleteFile(fln(:ifln)//'.n50')
      call DeleteAllFiles('"'//fln(:ifln)//'*.tmp"')
      call ChangeUSDFile(fln,'closed','*')
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM41)
      call DeleteFile(PreviousM42)
      call DeleteFile(PreviousM50)
      return
      end
      subroutine FeHistory(NewFln,Focus)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) NewFln
      character*256 DirHist(mxhst),FileHist(mxhst),NameHist(mxhst),t256
      integer FeChdir,SbwLnQuest,SbwItemPointerQuest
      logical Focus,StructureOpened,GetStructureLocked
      ln=NextLogicNumber()
      call OpenFile(ln,HistoryFile,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_history.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9999
      n=0
1000  n=n+1
      read(ln,FormA,end=2000,err=2000) DirHist(n)
      read(ln,FormA,end=2000,err=2000) FileHist(n)
      NameHist(n)=DirHist (n)(:idel(DirHist (n)))//
     1            FileHist(n)(:idel(FileHist(n)))
      t256=NameHist(n)
      if(StructureOpened(t256))
     1  NameHist(n)=NameHist(n)(:idel(NameHist(n)))//'(opened)'
      if(GetStructureLocked(t256))
     1  NameHist(n)=NameHist(n)(:idel(NameHist(n)))//'(locked)'
      write(lno,FormA) NameHist(n)(:idel(NameHist(n)))
      if(n.ge.mxhst) then
        go to 2001
      else
        go to 1000
      endif
2000  n=n-1
2001  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      nold=n
      xqd=600.
      id=NextQuestId()
      il=max(n-1,5)
      call FeQuestCreate(id,-1.,-1.,xqd,il,'History',0,LightGray,0,0)
      Focus=.false.
      xpom=15.
      dpom=xqd-30.-SbwPruhXd
      il=max(n-2,4)
      call FeQuestSbwMake(id,xpom,il,dpom,il,1,CutTextFromLeft,
     1                    SbwVertical)
      SbwDoubleClickAllowed=.true.
      il=il+1
      nSbw=SbwLastMade
      dpom=50.
      xpom=xqd*.5-dpom*1.5-20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Delete')
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Focus')
      nButtFocus=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Clear')
      nButtClear=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2400  call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_history.tmp')
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtClear) then
          call CloseIfOpened(SbwLnQuest(nSbw))
          call DeleteFile(fln(:ifln)//'_history.tmp')
          n=0
          go to 2400
        else if(CheckNumber.eq.nButtDelete) then
          j=SbwItemPointerQuest(nSbw)
          call FeQuestSbwItemSelDel(nSbw)
          do i=j,n-1
            DirHist(i)=DirHist(i+1)
            FileHist(i)=FileHist(i+1)
          enddo
          n=n-1
          go to 2500
        else if(CheckNumber.eq.nButtFocus) then
          Focus=.true.
          ich=0
          go to 2800
        endif
        go to 2500
      else if(CheckType.eq.EventSbwDoubleClick) then
        ich=0
        go to 2800
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
2800  if(ich.eq.0) then
        if(n.gt.0) then
          i=SbwItemPointerQuest(nSbw)
          NewFln=FileHist(i)
          call DeletePomFiles
          m=FeChDir(DirHist(i))
          call FeGetCurrentDir
          if(n.ne.nold) then
            ln=NextLogicNumber()
            call OpenFile(ln,HistoryFile,'formatted','old')
            if(ErrFlag.ne.0) go to 9999
            do i=1,n
              write(ln,FormA) DirHist(i) (:idel(DirHist (i)))
              write(ln,FormA) FileHist(i)(:idel(FileHist(i)))
            enddo
          endif
        else
          call DeleteFile(HistoryFile)
        endif
      endif
5000  ErrFlag=ich
      call FeQuestRemove(id)
9999  call DeleteFile(fln(:ifln)//'_history.tmp')
      call CloseIfOpened(ln)
      return
      end
      subroutine FeListView(FileName,Kam)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*128 Veta,FindString,FindList(15)
      character*80  FindLabel(15)
      character*1   Znak
      character*(*) FileName
      integer FeMenuNew,FromSave,SbwItemNMaxQuest,SbwLinesQuest,
     1        SbwItemFromQuest,SbwLnQuest,JenKdyzPredchozi(15),
     2        FindLine(15)
      logical Forwards,CrwLogicQuest,WizardModeOld,PropFontIn
      PropFontIn=PropFont
      WizardModeOld=WizardMode
      WizardMode=.false.
      AskIfQuit=.true.
      FindString=' '
      idFindString=0
      Forwards=.true.
      FromSave=0
      ils=0
      idl=idel(FileName)
      call SetStringArrayTo(FindLabel,15,' ')
      call SetStringArrayTo(FindList ,15,' ')
      call SetIntArrayTo(JenKdyzPredchozi,15,0)
      call SetIntArrayTo(FindLine,15,0)
      FindList(1)='###zacatek###'
      FindLine(1)=1
      FindLabel(1)='TOP       <HOME>'
      if(index(FileName,'.ref').eq.idl-3) then
        FindList(2)='fo/fc list before'
        FindLabel(2)='Fo/Fc list before refinement'
        FindList(3)='statistics'
        JenKdyzPredchozi(3)=1
        FindLabel(3)='Statistics Fo,sin(th)/lambda before refinement'
        FindList(4)='fo/fc list after'
        FindLabel(4)='Fo/Fc list after refinement'
        FindList(4)='fo/fc list after'
        FindLabel(4)='Fo/Fc list after refinement'
        FindList(5)='fo/fc list of worst fitted reflections'
        FindLabel(5)='Fo/Fc list of worst fitted reflections'
        FindList(6)='statistics '
c        JenKdyzPredchozi(5)=1
        FindLabel(6)='Statistics Fo,sin(th)/lambda after refinement'
        FindList(7)='r-factors overview'
        FindLabel(7)='R-factors overview'
        FindList(8)='changes overview'
        FindLabel(8)='Changes overview'
        FindList(9)='ADP eigenvalues and eigenvectors'
        FindLabel(9)='ADP eigenvalues and eigenvectors'
        FindList(10)='List of serious warnings'
        FindLabel(10)='List of serious warnings'
        FindList(11)='blocked due to singularity'
        FindLabel(11)='Singularity report'
        if(MaxMagneticType.gt.0) then
          FindList(12)='Interpretation of magnetic parameters'
          FindLabel(12)='Interpretation of magnetic parameters'
        endif
        if(ChargeDensities.and.lasmaxm.gt.0) then
          FindList(12)='d polulations from multipoles'
          FindLabel(12)='d polulations from multipoles'
        endif
      else if(index(FileName,'.fou').eq.idl-3) then
        FindList(2)='searching for positive peaks'
        FindLabel(2)='Positive peaks'
        FindList(3)='searching for negative peaks'
        FindLabel(3)='Negative peaks'
      else if(index(FileName,'.dis').eq.idl-3) then
        FindList(2)='no  atom'
        FindLabel(2)='List of input atoms'
        FindList(3)='List of distances'
        FindLabel(3)='List of distances'
        FindList(4)='List of bond valences'
        FindLabel(4)='List of bond valences'
        FindList(5)='Hirshfeld test'
        FindLabel(5)='Hirshfeld test'
        FindList(6)='List of possible hydrogen bonds'
        FindLabel(6)='List of possible hydrogen bonds'
        FindList(7)='List of torsion angles'
        FindLabel(7)='List of torsion angles'
        FindList(8)='List of best planes'
        FindLabel(8)='List of best planes'
        FindList(9)='List dihedral angles'
        FindLabel(9)='List dihedral angles'
      else if(index(FileName,'.rre').eq.idl-3) then
        FindList(2)='Report from the import procedure'
        FindLabel(2)='Report from the import procedure'
        FindList(3)='Summary of collected reflections'
        FindLabel(3)='Summary of rejected reflections'
        FindList(4)='Summary of reflections absent'
        FindLabel(4)='Summary of systematic extinctions'
        FindList(5)='Summary from transformation to the common scale'
        FindLabel(5)='Transformation to the common scale'
        FindList(6)='Report from averaging reflections'
        FindLabel(6)='Report from averaging'
        FindList(7)='Summary after averaging'
        FindLabel(7)='Summary after averaging'
        FindList(8)='Coverage statistics'
        FindLabel(8)='Coverage statistics'
      else if(index(FileName,'.cp').eq.idl-2) then

      else if(index(FileName,'.inb').eq.idl-3) then

      endif
      nList=0
      do i=1,12
        if(FindLabel(i).ne.' ') then
          nList=i
          call Mala(FindList(i))
        endif
      enddo
      nList=min(nList+1,15)
      FindList(nList)='###konec###'
      FindLabel(nList)='BOTTOM     <END>'
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      il=0
      ilz=0
      ilp=0
      xd=max(FeTxLengthUnder(FindLabel(1)),
     1       FeTxLengthUnder(FindLabel(NList)))+2.*EdwMarginSize
      do 1500i=2,NList-1
        xd=max(xd,FeTxLengthUnder(FindLabel(i))+2.*EdwMarginSize)
        if(JenKdyzPredchozi(i).gt.0.and.FindLine(i-1).le.0) then
          FindLine(i)=0
          go to 1500
        else
1400      read(ln,FormA,end=1450,err=1450) Veta
          il=il+1
          call Mala(Veta)
          if(index(Veta,'page=').gt.0) then
            ilp=il
            go to 1400
          endif
          if(index(Veta,FindList(i)(:idel(FindList(i)))).le.0)
     1      go to 1400
          ilz=il
          FindLine(i)=ilp
          go to 1500
1450      rewind ln
          do j=1,ilz
            read(ln,FormA,end=1500,err=1500) Veta
            if(index(Veta,'page=').gt.0) ilp=j
          enddo
          il=ilz
        endif
1500  continue
      call CloseIfOpened(ln)
      call FeSetPropFont
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,
     1                      LightGray,-1,-1)
      CheckKeyboard=.true.
      xpom=XMaxBasWin-(SbwPruhXd+4.)/EnlargeFactor
      ypom=YMaxBasWin-30.
      call FeQuestAbsSbwMake(id,0.,YMinBasWin,xpom,ypom,1,
     1                       CutTextNone,SbwVertical)
      nSbwText=SbwLastMade
      call FeQuestSbwTextOpen(nSbwText,MxLine,FileName)
      FindLine(NList)=SbwItemNMaxQuest(nSbwText)
      yln=SbwYStepQuest(nSbwText)
      yln2=yln*.5
      ln=SbwLnQuest(nSbwText)
      xpom=15.
      ypom=SbwYMaxQuest(nSbwText)+8.
      dpom=35.
      do i=1,7
        if(i.eq.1) then
          Veta='%Find'
        else if(i.eq.2) then
          Veta='Find %next'
        else if(i.eq.3) then
          Veta='%Go to'
        else if(i.eq.4) then
          Veta='%Print'
        else if(i.eq.5) then
          Veta='Pg%Top'
        else if(i.eq.6) then
          Veta='Open in %editor'
        else if(i.eq.7) then
          Veta='%Close'
        endif
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtFind=ButtonLastMade
        else if(i.eq.2) then
          nButtFindNext=ButtonLastMade
        else if(i.eq.3) then
          nButtGoto=ButtonLastMade
          xgoto=xpom
        else if(i.eq.4) then
          nButtPrint=ButtonLastMade
        else if(i.eq.5) then
          nButtPgTop=ButtonLastMade
        else if(i.eq.6) then
          nButtOpenEdit=ButtonLastMade
        else if(i.eq.7) then
          nButtClose=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+15.
      enddo
      ygoto=ypom-13.-float(nList)*MenuLineWidth
      npg=(SbwItemNMaxQuest(nSbwText)-1)/SbwLinesQuest(nSbwText)+1
      if(kam.lt.0) then
        ilp=FindLine(NList)
      else if(kam.gt.0) then
        ilp=kam
      else
        ilp=0
      endif
      if(ilp.gt.0) call FeQuestSbwShow(nSbwText,ilp)
2000  BlockedActiveObj=.false.
      ActiveObj=ActSbw*10000+nSbwText+SbwFr-1
      call FeQuestActiveObjOn
      SbwActive=nSbwText+SbwFr-1
      call FeQuestEvent(id,ich)
2050  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtFind.or.CheckNumber.eq.nButtFindNext)
     1    then
          if(FromSave.ne.SbwItemFromQuest(nSbwText))
     1      ils=SbwItemFromQuest(nSbwText)
          if(CheckNumber.eq.nButtFindNext) go to 2150
          idp=NextQuestId()
          il=3
          xqd=300.
          call FeQuestCreate(idp,-1.,-1.,xqd,il,'Find',0,LightGray,0,0)
          il=1
          Veta='%Search for:'
          xpom=5.
          dpom=xqd-10.
          call FeQuestEdwMake(idp,xpom,il,xpom,il+1,Veta,'L',dpom,EdwYd,
     1                        0)
          nEdwFind=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,FindString)
          il=il+2
          xpom=5.
          tpom=CrwgXd+10.
          do i=1,2
            if(i.eq.1) then
              Veta='%Forwards'
            else
              Veta='%Backwards'
            endif
            call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,1)
            if(i.eq.1) nCrwForwards=CrwLastMade
            call FeQuestCrwOpen(CrwLastMade,Forwards.eqv.i.eq.1)
            xpom=xpom+xqd*.5
            tpom=tpom+xqd*.5
          enddo
2100      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2100
          endif
          if(ich.eq.0) then
            Forwards=CrwLogicQuest(nCrwForwards)
            FindString=EdwStringQuest(nEdwFind)
            call Mala(FindString)
          endif
          call FeQuestRemove(idp)
          if(ich.ne.0) go to 2000
          idFindString=idel(FindString)
2150      if(idFindString.le.0) go to 2000
          rewind(ln)
          il=0
          ilsn=0
2200      read(ln,FormA,end=2300,err=2300) Veta
          il=il+1
          if(Forwards.and.il.le.ils) go to 2200
          call Mala(Veta)
          KurzX=index(Veta,FindString(:idFindString))
          if(KurzX.le.0) go to 2200
          if(.not.Forwards) then
            if(il.lt.ils) then
              ilsn=il
              KurzXs=KurzX
              go to 2200
            else
              if(ilsn.gt.0) then
                il=ilsn
                KurzX=KurzXs
              else
                call FeChybne(-1.,-1.,'the top has been reached.',' ',
     1                        Warning)
                go to 2000
              endif
            endif
          endif
2250      call FeQuestSbwShow(nSbwText,il)
          FromSave=SbwItemFromQuest(nSbwText)
          ils=il
          KurzY=il-FromSave+1
          call FeSetFixFont
          XKurz=SbwXMinQuest(nSbwText)+FeTxLengthSpace(Veta(:KurzX-1))
          YKurz=SbwYMaxQuest(nSbwText)-yln*float(KurzY-1)-yln2
          if(OpSystem.le.0) call FePlotMode('E')
          call FeFillRectangle(xKurz,
     1      xKurz+FeTxLengthSpace(FindString(:idFindString)),
     2      yKurz-yln2,yKurz+yln2,4,0,0,KurzorColor)
          call FeSetPropFont
          if(OpSystem.le.0) call FePlotMode('N')
          go to 2000
2300      if(.not.Forwards) then
            il=ilsn
            KurzX=KurzXs
            go to 2250
          endif
          call FeChybne(-1.,-1.,'the bottom has been reached.',' ',
     1                  Warning)
          go to 2000
        else if(CheckNumber.eq.nButtGoto) then
          i=FeMenuNew(xgoto,ygoto,xd,FindLabel,FindLine,1,nList,1,0)
          if(i.lt.1.or.i.gt.nList) go to 2000
          ilp=FindLine(i)
          if(ilp.le.0) go to 2000
2450      call FeSetFixFont
          call FeQuestSbwShow(nSbwText,ilp)
          call FeSetPropFont
          go to 2000
        else if(CheckNumber.eq.nButtPrint) then
          ipg=(SbwItemFromQuest(nSbwText)-1)/SbwLinesQuest(nSbwText)+1
          call FeListPrint(FileName,ln,ipg,npg,kam)
          if(kam.eq.8000) go to 8000
        else if(CheckNumber.eq.nButtPgTop) then
          if(SbwItemFromQuest(nSbwText).ne.1) then
            il=((SbwItemFromQuest(nSbwText)-2)/
     1           SbwLinesQuest(nSbwText)+1)*SbwLinesQuest(nSbwText)+1
            call FeSetFixFont
            call FeQuestSbwShow(nSbwText,il)
            call FeSetPropFont
          endif
        else if(CheckNumber.eq.nButtOpenEdit) then
          call FeEdit(FileName)
        else if(CheckNumber.eq.nButtClose) then
          go to 8000
        endif
        go to 2000
      else if(CheckType.eq.EventCTRL) then
        Znak=char(CheckNumber)
        call mala (Znak)
        if(Znak.eq.'f') then
          CheckType=EventButton
          CheckNumber=nButtFind
          go to 2050
        else if(Znak.eq.'n') then
          CheckType=EventButton
          CheckNumber=nButtFindNext
          go to 2050
        else if(Znak.eq.'p') then
          CheckType=EventButton
          CheckNumber=nButtPrint
          go to 2050
        endif
        go to 2000
      else if(CheckType.eq.EventASCII) then
        go to 2000
      else if(CheckType.eq.EventAlt) then
        go to 2000
      else if(CheckType.eq.EventKey) then
        go to 2000
      else if((CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape).or.
     1    (CheckType.eq.EventGlobal.and.CheckNumber.eq.GlobalClose))
     2  then
        go to 8000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
8000  call FeQuestRemove(id)
      CheckKeyboard=.false.
      WizardMode=WizardModeOld
9999  AskIfQuit=.false.
      if(.not.PropFontIn) call FeSetFixFont
      return
      end
      subroutine FeListPrint(FileName,lnlst,ipg,npg,kam)
      include 'fepc.cmn'
      integer intpg(2,100),EdwStateQuest,RolMenuSelectedQuest
      logical lpom
      logical FeYesNo,pis,nacteno,canceled,konec,CrwLogicQuest,
     1        PrintToFile,Strankovani
      character*(*) filename
      character*1 zn
      character*10 ch10
      character*80 ch80,whichPgs,PSPrinters(100)
      character*256 radka,ch256
      character*256 EdwStringQuest,tmpout,toFile
      kam=0
      pis=.true.
      nacteno=.false.
      canceled=.false.
      nintpg=0
      i=index(FileName,'.')-1
      if(i.le.0) i=idel(FileName)
      toFile=FileName(:i)//'.ps'
      call FeGetPSPrinters(PSPrinters,nPSPrinters)
      if(nPSPrinters.gt.0) then
        PrintToFile=.false.
      else
        PrintToFile=.true.
      endif
      il=5
      id=NextQuestId()
      QuestWidth=300.
      if(nPSPrinters.gt.0) then
        il=il+1
        tpom=5.
        ch80='Printers with PostScript driver:'
        xpom=tpom+FeTxLengthUnder(ch80)+10.
        dpom=20.
        do i=1,nPSPrinters
          dpom=max(dpom,FeTxLengthUnder(PSPrinters(i))+2.*EdwMarginSize)
        enddo
        dpom=dpom+EdwYd+20.
        xpom1=xpom+dpom+5.
        QuestWidth=max(xpom1+EdwYd+5.,QuestWidth)
      endif
      call FeQuestCreate(id,-1.,-1.,QuestWidth,il,'Print listing',0,
     1                   LightGray,0,0)
      il=0
      if(nPSPrinters.gt.0) then
        il=il+1
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,
     1                          0)
        nRolMenuPrinters=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,PSPrinters,NPSPrinters,
     1                          1)
      else
        nRolMenuPrinters=0
      endif
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=1,3
        if(i.eq.1) then
          if(npg.gt.1) then
            write(ch80,'(i10)') npg
            call Zhusti(ch80)
            ch80='All '//ch80(:idel(ch80))//' pages'
          else
            ch80='One page'
          endif
        else if(i.eq.2) then
          ch80='Current page'
        else
          ch80='Pages:'
        endif
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwAll=CrwLastMade
          nCrwPageSelected=CrwLastMade
        else if(i.eq.2) then
          nCrwCur=CrwLastMade
        else
          nCrwSelP=CrwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(ch80)+5.
      ch80='Example: 1,2,4-6,30-'
      dpom=QuestWidth-xpom-5.
      call FeQuestEdwMake(id,xpom,il+1,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwSelP=EdwLastMade
      il=il+2
      xpom=5.
      tpom=xpom+CrwgXd+5.
      ch80='Print to file:'
      if(PrintToFile) then
        call FeQuestLblMake(id,xpom,il,ch80,'L','N')
        tpom=xpom
        nCrwToFile=0
      else
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwXd,CrwYd,1,0)
        nCrwToFile=CrwLastMade
        call FeQuestCrwOpen(nCrwToFile,.false.)
      endif
      xpom=tpom+FeTxLengthUnder(ch80)+5.
      ch80= ' '
      dpom=QuestWidth-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwToFile=EdwLastMade
      if(PrintToFile) call FeQuestStringEdwOpen(nEdwToFile,toFile)
1050  if(CrwLogicQuest(nCrwSelP)) then
        if(EdwStateQuest(nEdwSelP).ne.EdwOpened)
     1    call FeQuestStringEdwOpen(nEdwSelP,' ')
      else
        call FeQuestEdwClose(nEdwSelP)
      endif
      if(nCrwToFile.gt.0) then
        if(CrwLogicQuest(nCrwToFile)) then
          if(EdwStateQuest(nEdwToFile).ne.EdwOpened)
     1      call FeQuestStringEdwOpen(nEdwToFile,toFile)
        else
          call FeQuestEdwClose(nEdwToFile)
        endif
      endif
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1050
      else if(CheckType.eq.EventGlobal.and.
     1        CheckNumber.eq.GlobalClose) then
        call FeQuestRemove(id)
        kam=8000
        go to 9999
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        do i=nCrwAll,nCrwSelP
          if(CrwLogicQuest(i)) then
            nCrwPageSelected=i
            if(i.eq.nCrwSelP) then
              whichPgs=EdwStringQuest(nEdwSelP)
            else
              whichPgs=' '
              if(i.eq.nCrwCur) then
                nintpg=1
                intpg(1,1)=ipg
                intpg(2,1)=ipg
              endif
            endif
            go to 1120
          endif
        enddo
1120    if(nCrwToFile.ne.0) then
          PrintToFile=CrwLogicQuest(nCrwToFile)
          if(PrintToFile) toFile=EdwStringQuest(nEdwToFile)
        endif
        if(nPSPrinters.gt.0) PSPrinters(1)=
     1    PSPrinters(RolMenuSelectedQuest(nRolMenuPrinters))
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(nCrwPageSelected.eq.nCrwSelP) then
        call zhusti(whichPgs)
        i1=1
        i11=0
        i2=0
        i22=0
        idash=0
        ien=idel(whichPgs)
        do i=1,ien
          zn=whichPgs(i:i)
          if(zn.eq.'-') then
            idash=i
            i11=i-1
            i2=i+1
          endif
          if(zn.eq.','.or.i.eq.ien) then
            if(idash.gt.0)then
              i22=i-1
              if(i.eq.ien) i22=i
            else
              i11=i-1
              if(i.eq.ien) i11=i
            endif
            if(i1.gt.0.and.i11.ge.i1) then
              nintpg=nintpg+1
              if(nintpg.gt.100) nintpg=100
              write(ch10,'(''(i'',i5,'')'')') i11-i1+1
              call zhusti(ch10)
              read(whichPgs(i1:i11),ch10,err=1250) intpg(1,nintpg)
              intpg(2,nintpg)=intpg(1,nintpg)
            endif
            if(idash.eq.i) then
              intpg(2,nintpg)=-1
            else
              if(i2.gt.0.and.i22.ge.i2) then
                write(ch10,'(''(i'',i5,'')'')') i22-i2+1
                call zhusti(ch10)
                read(whichPgs(i2:i22),ch10,err=1250) intpg(2,nintpg)
              endif
            endif
            i1=0
            if(i.lt.ien) i1=i+1
            i11=0
            i2=0
            i22=0
            idash=0
          endif
        enddo
        go to 1300
1250    call FeMsgOut(-1.,-1.,'The selection of pages is wrong.')
        go to 9999
      endif
1300  if(PrintToFile) then
        inquire(file=toFile,exist=lpom)
        if(lpom) then
          if(.not.FeYesNo(-1.,-1.,'File '//toFile(1:idel(toFile))
     1       //' already exists - overwrite it?',1)) go to 9999
        endif
      endif
      rewind lnlst
      Strankovani=.false.
      do i=1,50
        read(lnlst,FormA,end=1350) radka
        if(index(Radka,'page=').gt.120) then
          Strankovani=.true.
          exit
        endif
      enddo
1350  rewind lnlst
      lnps=NextLogicNumber()
      if(OpSystem.le.0) then
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//'a2ps'//
     1                ObrLom//'lstprolo.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8000
      else
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//
     1                'a2ps/lstprolo.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8000
      endif
      read(lnps,FormA) radka
      if(radka(1:1).ne.'#') rewind lnps
      tmpout='WorkFile_prnj.tmp'
      lnout=NextLogicNumber()
      call OpenFile(lnout,tmpout,'formatted','unknown')
1400  read(lnps,FormA,end=1450) radka
      write(lnout,FormA) radka(:idel(radka))
      go to 1400
1450  call CloseIfOpened(lnps)
      npage=0
      nsheet=0
      write(lnout,'('' '')')
      write(lnout,'(''/docsave save def'')')
      if(OpSystem.le.0) then
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//'a2ps'//
     1                ObrLom//'lstnewpg.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8100
      else
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//
     1                'a2ps/lstnewpg.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8100
      endif
      write(ch80,'(''Preparing PostScript file, sheet '',i5,
     1  '' ,press ESC to cancel.'')') 0
      call FeTxOut(-1.,-1.,ch80)
      nn=MxLine
1500  rewind lnps
      read(lnps,FormA) radka
      if(radka(1:1).ne.'#') rewind lnps
      nsheet=nsheet+1
      write(ch80,'(''Preparing PostScript file, sheet '',i5,
     1  '' ,press ESC to cancel.'')') nsheet
      call FeTxOutCont(ch80)
      write(lnout,'(''%%Page:'',2i5)') nsheet,nsheet
1600  read(lnps,FormA,end=1700) radka
      write(lnout,FormA) radka(:idel(radka))
      go to 1600
1700  nlocal=0
2000  if(nacteno) then
        radka=ch256
        nacteno=.false.
        nlocal=1
        go to 2500
      endif
      if(mod(nsheet,10).eq.0) then
        call FeEvent(1)
        if(EventType.eq.10.and.EventNumber.eq.14) then
          if(FeYesNo(-1.,-1.,'Do you want cancel the printing?',1)) then
            canceled=.true.
            go to 6000
          endif
        endif
      endif
      read(lnlst,FormA,end=5000) radka
      if(Strankovani) then
        idr=idel(radka)
        if(idr.lt.124) then
          if(nlocal.le.1) then
            go to 2000
          else
            go to 2500
          endif
        endif
        if(index(radka,'page=').le.0) then
          if(nlocal.le.0) then
            go to 2000
          else
            go to 2500
          endif
        endif
      else
        if(nn.lt.MxLine) go to 2500
      endif
      nn=0
      nlocal=nlocal+1
      npage=npage+1
      if(nCrwPageSelected.ne.nCrwAll) then
        konec=.true.
        do i=1,nintpg
          if(npage.le.intpg(2,i).or.intpg(2,i).eq.-1)then
            konec=.false.
            if(npage.ge.intpg(1,i)) then
              pis=.true.
              go to 2200
            endif
          endif
        enddo
        if(konec) go to 5000
        nlocal=nlocal-1
        pis=.false.
      endif
2200  if(nlocal.eq.1.or..not.pis) then
        go to 2500
      else if(nlocal.eq.2) then
        write(lnout,'(''/sd 1 def'')')
        write(lnout,'(''border'')')
        write(lnout,'(''/x0 x 1 get bm add def'')')
        write(lnout,'(''/y0 y 1 get bm bfs add 0 add sub def'')')
        write(lnout,'(''x0 y0 moveto'')')
        write(lnout,'(''bf setfont'')')
        go to 2500
      else
        write(lnout,'(''/sd 0 def'')')
        write(lnout,'(i5,'' sn'')') nsheet
        write(lnout,'(''pagesave restore'')')
        write(lnout,'(''showpage'')')
        nacteno=.true.
        ch256=radka
        go to 1500
      endif
2500  if(pis) write(lnout,'(''( '',a,'') s'')') radka(:idel(radka))
      nn=nn+1
      go to 2000
5000  write(lnout,'(''/sd 0 def'')')
      write(lnout,'(i5,'' sn'')')nsheet
      write(lnout,'(''pagesave restore'')')
      write(lnout,'(''showpage'')')
      write(lnout,'('' '')')
      write(lnout,'(''%%Trailer'')')
      write(lnout,'(''%%Pages: '',i5)')nsheet
      write(lnout,'(''docsave restore end'')')
6000  call FeTxOutEnd
      call CloseIfOpened(lnps)
      call CloseIfOpened(lnout)
      if(canceled) go to 9999
      if(.not.PrintToFile.and.nsheet.gt.15) then
        write(radka,'(''Total: '',i6,
     1    '' sheets of A4 - Do you want to start printing?'')')
     2    nsheet
        call FeDelTwoSpaces(radka)
        if(.not.FeYesNo(-1.,-1.,radka,1)) go to 9999
      endif
      if(PrintToFile) then
        call MoveFile(tmpout,tofile)
      else
        call FePrintFile(PSPrinters(1),tmpout,ich)
        call DeleteFile(tmpout)
      endif
      go to 9900
8000  ch80='The file lstprolog.ps couldn''t be opened'
      go to 9000
8100  ch80='The file lstnewpg.ps couldn''t be opened'
      go to 9000
8300  write(ch80,'(i3)') ich
      ch80='printing error no.:'//ch80(1:3)
9000  call FeChybne(-1.,-1.,ch80,' ',SeriousError)
9900  call CloseIfOpened(lnps)
9999  return
      end
      subroutine FeInOutIni(klic,FileNameIni)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*(*) FileNameIni
      character*256 t256,Radka
      character*80 KeyWord(90),KeyWordRead
      character*10 SizeType(4)
      dimension iap(1),poma(1)
      integer PixelWindowWidthS,PixelWindowHeightS,
     1        PixelWindowXPosS,PixelWindowYPosS
      logical EqIgCase,ExistFile
      equivalence (iap(1),i),(poma,pom)
      equivalence
     1           (IdNumbers( 1),IdWindowWidth),
     2           (IdNumbers( 2),IdWindowHeight),
     3           (IdNumbers( 3),IdWindowColors),
     4           (IdNumbers( 4),IdWindowX),
     5           (IdNumbers( 5),IdWindowY),
     6           (IdNumbers( 6),IdWindowSizeType),
     7           (IdNumbers( 7),IdFontSize),
     8           (IdNumbers( 8),IdUseViewer),
     9           (IdNumbers( 9),IdICDDKey),
     a           (IdNumbers(10),IdICDDDir),
     1           (IdNumbers(11),IdWineKey),
     2           (IdNumbers(12),IdTmpDir),
     3           (IdNumbers(13),IdCIFSpecFile),
     4           (IdNumbers(14),IdPSPrinter),
     1           (IdNumbers(21),IdEditorCommand),
     2           (IdNumbers(22),IdDosShellCommand),
     3           (IdNumbers(23),IdXShapeCommand),
     4           (IdNumbers(24),IdGraphicCommand),
     5           (IdNumbers(25),IdGnuplotCommand),
     6           (IdNumbers(26),IdGSCommand),
     7           (IdNumbers(27),Id3dMapsCommand),
     8           (IdNumbers(28),IdDPlotCommand),
     1           (IdNumbers(41),IdSIRCommand),
     2           (IdNumbers(42),IdSIR97Command),
     3           (IdNumbers(43),IdSIR2002Command),
     4           (IdNumbers(44),IdSIR2004Command),
     5           (IdNumbers(45),IdSIR2008Command),
     6           (IdNumbers(46),IdSIR2011Command),
     7           (IdNumbers(47),IdSIR2014Command),
     1           (IdNumbers(61),IdExpoCommand),
     2           (IdNumbers(62),IdExpo2004Command),
     3           (IdNumbers(63),IdExpo2009Command),
     4           (IdNumbers(64),IdExpo2013Command),
     5           (IdNumbers(65),IdExpo2014Command),
     6           (IdNumbers(71),IdShelxtCommand),
     1           (IdNumbers(81),IdDelejKontroly),
     2           (IdNumbers(82),IdVasekTest),
     3           (IdNumbers(83),IdVasekAbs)
      save PixelWindowWidthS,PixelWindowHeightS,
     1     PixelWindowXPosS,PixelWindowYPosS
      data KeyWord/'WindowWidth','WindowHeight','WindowColors',
     1             'WindowX','WindowY','WindowSizeType','FontSize',
     2             'UseViewer','ICDDKey','ICDDDir','WineKey','TmpDir',
     3             'CIFSpecFile','PSPrinter','#15',
     4             '#16','#17','#18','#19','#20',
     5             'EditorCommand','DosShellCommand','XShapeCommand',
     6             'GraphicCommand','GnuplotCommand','GSCommand',
     7             '3dMapsCommand','DPlotCommand',
     8             '#29','#30','#31','#32','#33','#34','#35','#36',
     9             '#37','#38','#39','#40',
     a             'SIRCommand','SIR97Command','SIR2002Command',
     1             'SIR2004Command','SIR2008Command','SIR2011Command',
     2             'SIR2014Command',
     3             '#48','#49','#50','#51','#52','#53','#54','#55',
     3             '#56','#57','#58','#59','#60',
     4             'ExpoCommand','Expo2004Command','Expo2009Command',
     5             'Expo2013Command','Expo2014Command',
     6             '#66','#67','#68','#69','#70',
     7             'ShelxtCommand','#72','#73',
     7             '#74','#75','#76','#77','#78','#79','#80',
     8             'DelejKontroly','VasekTest','VasekAbs',
     9             '#84','#85','#86','#87','#88','#89','#90'/
      data SizeType/'Normal','Minimal','Fullscreen','Exactly'/
      k=klic
1000  if(k.eq.0) then
        PropFontSize=8
        if(OpSystem.eq.0) then
          mon14=.true.
        else
          mon14=.false.
        endif
        DelejKontroly=0
        VasekTest=0
        BuildInViewer=.true.
        EditorName='c:\WINDOWS\notepad.exe'
        VisualClass='PCcol'
        FirstCommand='command.com'
        CallXShape=' '
        CallGraphic=' '
        CallSIR=' '
        CallExpo=' '
        DirSIR=' '
        DirExpo=' '
        Call3dMaps=' '
        CallDPlot=' '
        CallGnuplot=' '
        CallGS=' '
        PSPrinter=' '
        ICDDDir=' '
        ICDDKey=0
        TmpDir=HomeDir(:idel(HomeDir))//'tmp'
        WineKey=0
        VasekAbs=0
        PixelWindowHeight=3*nint(.8*float(PixelScreenHeight)/3.)
        PixelWindowWidth=(4*PixelWindowHeight)/3
        WindowSizeType=WindowExactly
        PixelWindowXPos=(PixelScreenWidth-PixelWindowWidth)/2
        PixelWindowYPos=(PixelScreenHeight-PixelWindowHeight)/2
        if(ExistFile(FileNameIni)) then
          ln=NextLogicNumber()
          if(VasekDebug.ne.0) write(44,'(''PARAMS - as read from "'',a,
     1                        ''"'')') FileNameIni(:idel(FileNameIni))
          open(ln,file=FileNameIni,err=9000)
2000      read(ln,FormA,end=3000,err=9000) t256
          if(idel(t256).le.0.or.t256(1:1).eq.'#') go to 2000
          Radka=t256
          i=LocateSubstring(Radka,MasterName(:idel(MasterName))//'.',
     1                      .false.,.true.)
          if(i.le.0) go to 2000
          i=index(Radka,'.')+1
          j=index(Radka,':')
          if(j.le.i) go to 2000
          KeyWordRead=Radka(i:j-1)
          j=j+1
          do i=1,90
            if(EqIgCase(KeyWordRead,KeyWord(i))) go to 2600
          enddo
          go to 2000
2600      if(i.eq.IdWindowWidth) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowWidthE=i
              PixelWindowWidth=i
            endif
          else if(i.eq.IdWindowHeight) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowHeightE=i
              PixelWindowHeight=i
            endif
          else if(i.eq.IdWindowColors) then
            call StToInt(t256,j,iap,1,.false.,ich)
          else if(i.eq.IdWindowX) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowXPosE=i
              PixelWindowXPos=i
            endif
          else if(i.eq.IdWindowY) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowYPosE=i
              PixelWindowYPos=i
            endif
          else if(i.eq.WindowSizeType) then
            t256=t256(j:)
            call zhusti(t256)
            do i=1,4
              if(EqIgCase(t256,SizeType(i))) then
                WindowSizeType=i
                go to 2000
              endif
            enddo
          else if(i.eq.IdFontSize) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PropFontSize=i
              if(PropFontMeasured.eq.FontInPixels) then
                if(PropFontSize.lt.15) PropFontSize=15
              else if(PropFontMeasured.eq.FontInPoints) then
                if(PropFontSize.ge.15) PropFontSize=8
              endif
            endif
          else if(i.eq.IdUseViewer) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) BuildInViewer=iap(1).eq.1
          else if(i.eq.IdICDDKey) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) ICDDKey=i
          else if(i.eq.IdICDDDir) then
            ICDDDir=t256(j+1:)
            i=idel(ICDDDir)
            if(i.gt.0) then
              call SkrtniPrvniMezery(ICDDDir)
3200          if(ICDDDir(i:i).eq.'\') then
                ICDDDir(i:i)=' '
                i=i-1
                go to 3200
              endif
              i=i+1
              ICDDDir(i:i)='\'
            endif
          else if(i.eq.IdWineKey) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) WineKey=i
          else if(i.eq.IdTmpDir) then
            TmpDir=t256(j+1:)
            call SkrtniPrvniMezery(TmpDir)
            i=idel(TmpDir)
3100        if(TmpDir(i:i).eq.'\') then
              TmpDir(i:i)=' '
              i=i-1
              go to 3100
            endif
            i=i+1
            TmpDir(i:i)='\'
          else if(i.eq.IdCIFSpecFile) then
            CIFSpecificFile=t256(j+1:)
            call SkrtniPrvniMezery(CIFSpecificFile)
          else if(i.eq.IdPSPrinter) then
            PSPrinter=Radka(j+1:)
            call SkrtniPrvniMezery(PSPrinter)
          else if(i.eq.IdEditorCommand) then
            EditorName=t256(j+1:)
            call SkrtniPrvniMezery(EditorName)
          else if(i.eq.IdDosShellCommand) then
            FirstCommand=t256(j+1:)
            call SkrtniPrvniMezery(FirstCommand)
          else if(i.eq.IdXShapeCommand) then
            CallXShape=t256(j+1:)
            call SkrtniPrvniMezery(CallXShape)
          else if(i.eq.IdGraphicCommand) then
            CallGraphic=t256(j+1:)
            call SkrtniPrvniMezery(CallGraphic)
          else if(i.eq.IdGnuplotCommand) then
            CallGnuplot=t256(j+1:)
            call SkrtniPrvniMezery(CallGnuplot)
          else if(i.eq.IdGSCommand) then
            CallGS=Radka(j+1:)
            call SkrtniPrvniMezery(CallGS)
          else if(i.eq.Id3dMapsCommand) then
            Call3dMaps=t256(j+1:)
            call SkrtniPrvniMezery(Call3dMaps)
          else if(i.eq.IdDPlotCommand) then
            CallDPlot=t256(j+1:)
            call SkrtniPrvniMezery(Call3dMaps)
          else if(i.ge.IdSIRCommand.and.i.le.IdSIR2014Command) then
            t256=t256(j+1:)
            call SkrtniPrvniMezery(t256)
            k=max(i-IdSIRCommand,1)
            call ExtractFileName(t256,CallSIR(k))
            call ExtractDirectory(t256,DirSIR(k))
          else if(i.ge.IdExpoCommand.and.i.le.IdExpo2014Command) then
            t256=t256(j+1:)
            call SkrtniPrvniMezery(t256)
            call ExtractFileName(t256,CallExpo(i-IdExpoCommand+1))
            call ExtractDirectory(t256,DirExpo(i-IdExpoCommand+1))
          else if(i.eq.IdShelxtCommand) then
            CallShelxt=t256(j+1:)
            call SkrtniPrvniMezery(CallShelxt)
          else if(i.eq.IdDelejKontroly) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) DelejKontroly=i
          else if(i.eq.IdVasekTest) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) VasekTest=i
          else if(i.eq.IdVasekAbs) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) VasekAbs=i
          endif
          go to 2000
        else
          k=1
          call GetPathTo('x-shape.exe','xshape',CallXShape)
          call GetPathTo('diamond.exe','Diamond 3',CallGraphic)
          if(CallGraphic.eq.' ') then
            call GetPathTo('diamond.exe','Diamond2',CallGraphic)
            if(CallGraphic.eq.' ') call GetPathTo('atoms.exe','atoms*',
     1                                            CallGraphic)
          endif
          call GetPathTo('SIR97.exe','SIR97',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(1))
            call ExtractDirectory(t256,DirSIR(1))
          endif
          call GetPathTo('SIR2002.exe','SIR2002',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(2))
            call ExtractDirectory(t256,DirSIR(2))
          endif
          call GetPathTo('SIR2004.exe','SIR2004',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(3))
            call ExtractDirectory(t256,DirSIR(3))
          endif
          call GetPathTo('expo.exe','expo',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallExpo(1))
            call ExtractDirectory(t256,DirExpo(1))
          endif
          call GetPathTo('expo2004.exe','expo2004',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallExpo(2))
            call ExtractDirectory(t256,DirExpo(2))
          endif
          ErrFlag=0
          go to 1000
        endif
      else
        go to 4000
      endif
3000  call CloseIfOpened(ln)
4000  if(k.eq.1) then
        ln=NextLogicNumber()
        if(VasekDebug.ne.0) write(44,'(''PARAMS - as written to "'',a,
     1                          ''"'')') FileNameIni(:idel(FileNameIni))
        open(ln,file=FileNameIni,err=9100)
      else
        ln=0
      endif
      do i=1,90
        if(KeyWord(i)(1:1).eq.'#') cycle
        if(i.eq.IdWindowWidth) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowWidthS
          else
            j=PixelWindowWidthE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowHeight) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowHeightS
          else
            j=PixelWindowHeightE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowColors) then
          cycle
        else if(i.eq.IdWindowX) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowXPosS
          else
            j=PixelWindowXPosE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowY) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowYPosS
          else
            j=PixelWindowYPosE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowSizeType) then
          t256=SizeType(WindowSizeType)
        else if(i.eq.IdFontSize) then
          write(t256,FormI15) PropFontSize
        else if(i.eq.IdUseViewer) then
          if(BuildInViewer) then
            t256='1'
          else
            t256='0'
          endif
        else if(i.eq.IdICDDKey) then
          write(t256,FormI15) ICDDKey
        else if(i.eq.IdICDDDir) then
          t256=ICDDDir
          go to 4100
        else if(i.eq.IdWineKey) then
          if(WineKey.eq.0) cycle
          write(t256,FormI15) WineKey
        else if(i.eq.IdTmpDir) then
          t256=TmpDir
          go to 4100
        else if(i.eq.IdCIFSpecFile) then
          t256=CIFSpecificFile
          go to 4100
        else if(i.eq.IdPSPrinter) then
          t256=PSPrinter
          go to 4100
        else if(i.eq.IdEditorCommand) then
          t256=EditorName
          go to 4100
        else if(i.eq.IdDosShellCommand) then
          t256=FirstCommand
          go to 4100
        else if(i.eq.IdXShapeCommand) then
          t256=CallXShape
          go to 4100
        else if(i.eq.IdGraphicCommand) then
          t256=CallGraphic
          go to 4100
        else if(i.eq.IdGnuplotCommand) then
          t256=CallGnuplot
          go to 4100
        else if(i.eq.IdGSCommand) then
          t256=CallGS
          go to 4100
        else if(i.eq.Id3dMapsCommand) then
          t256=Call3dMaps
          go to 4100
        else if(i.eq.IdDPlotCommand) then
          t256=CallDPlot
          go to 4100
        else if(i.ge.IdSIRCommand.and.i.le.IdSIR2014Command) then
          k=max(i-IdSIRCommand,1)
          t256=DirSIR(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallSIR(k)(:idel(CallSIR(k)))
          endif
          go to 4100
        else if(i.ge.45.and.i.le.50) then
          k=max(i-45,1)
          t256=DirSIR(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallSIR(k)(:idel(CallSIR(k)))
          endif
          go to 4100
        else if(i.ge.IdExpoCommand.and.i.le.IdExpo2014Command) then
          k=i-IdExpoCommand+1
          t256=DirExpo(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallExpo(k)(:idel(CallExpo(k)))
          endif
          go to 4100
        else if(i.eq.IdShelxtCommand) then
          t256=CallShelxt
          go to 4100
        else if(i.eq.IdDelejKontroly) then
          if(DelejKontroly.ne.1) cycle
          write(t256,FormI15) DelejKontroly
        else if(i.eq.IdVasekTest) then
          if(VasekTest.eq.0) cycle
          write(t256,FormI15) VasekTest
        else if(i.eq.IdVasekAbs) then
          if(VasekAbs.eq.0) cycle
          write(t256,FormI15) VasekAbs
        endif
        call ZdrcniCisla(t256,1)
4100    t256=MasterName(:idel(MasterName))//'.'//
     1       KeyWord(i)(:idel(KeyWord(i)))//': '//t256(:idel(t256))
        if(ln.ne.0)
     1    write(ln,FormA1,err=9010)(t256(j:j),j=1,idel(t256))
        if(VasekDebug.ne.0) write(44,FormA1)(t256(j:j),j=1,idel(t256))
      enddo
      if(VasekDebug.ne.0) write(44,'(''PARAMS - end'')')
      call CloseIfOpened(ln)
      go to 9999
9000  TextInfo(1)=MasterName(:idel(MasterName))//'.ini : reading error.'
      t256='Please check if the file exist and has proper attributes.'
      go to 9090
9010  TextInfo(1)=MasterName(:idel(MasterName))//'.ini : writing error.'
      t256='Please check if the file is not protected or "read only".'
9090  call CloseIfOpened(ln)
      go to 9150
9100  TextInfo(1)=MasterName(:idel(MasterName))//
     1            'DIR defined but directory isn''t accessible.'
      t256='Please correct the set command (SET '//
     1     MasterName(:idel(MasterName))//'DIR=....) '//
     1     'or make the directory "'//HomeDir(:idel(HomeDir))//
     2     '" accessible.'
9150  call FeChybne(-1.,-1.,TextInfo(1),t256,FatalError)
9999  if(PixelWindowXPosE.gt.-30000.and.PixelWindowYPosE.gt.-30000)
     1  then
        PixelWindowWidthS=PixelWindowWidthE
        PixelWindowHeightS=PixelWindowHeightE
        PixelWindowXPosS=PixelWindowXPosE
        PixelWindowYPosS=PixelWindowYPosE
      endif
      return
101   format(f15.6)
      end
      subroutine FePreferences(IGrOn)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 IniFileSave,IniFilePrev
      character*80  Veta
      integer EdwIntQuest,PixelWindowXPosIn,PixelWindowYPosIn,
     1        PropFontSizeIn
      logical ChangeWindowSize,ChangeWindowPosition,CheckSet,
     1        CrwLogicQuest,Prvne,FeYesNo,ChangeFont,FileDiff,
     2        ExistFile,ReturnPrev
      IGrOn=1
      PixelWindowXPosIn=PixelWindowXPos
      PixelWindowYPosIn=PixelWindowYPos
      PropFontSizeIn=PropFontSize
      IniFileSave='jins'
      call CreateTmpFile(IniFileSave,i,0)
      call FeTmpFilesAdd(IniFileSave)
      IniFilePrev='jinp'
      call CreateTmpFile(IniFilePrev,i,0)
      call FeTmpFilesAdd(IniFilePrev)
1000  call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini',IniFileSave)
      Prvne=.true.
      MinHeight=PixelScreenHeight/2
      MinHeight=MinHeight-mod(MinHeight,3)
      MinWidth=(MinHeight*3)/4
      id=NextQuestId()
      xqd=600.
      il=8
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Preferences',0,LightGray,
     1                   0,0)
      xpom=10.+CrwgXd
      tpom=5.
      Veta='%Normal window'
      il=0
      do i=1,4
        il=il+1
        call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        if(i.eq.1) then
          nCrwWndNormal=CrwLastMade
          Veta='%Minimal window'
        else if(i.eq.2) then
          nCrwWndMinimal=CrwLastMade
          Veta='F%ull screen'
        else if(i.eq.3) then
          nCrwWndMaximal=CrwLastMade
          Veta='Exactl%y'
        else if(i.eq.4) then
          nCrwWndExactly=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.WindowSizeType)
      enddo
      xpom=xpom+FeTxLength('Exactly')+10.
      call FeQuestLblMake(id,xpom,4,'=>','L','N')
      xpomt=xpom+FeTxLength('XX')+5.
      xpom=xpomt+FeTxLength('Height')+2.+CrwgXd
      dpom=100.
      call FeQuestEudMake(id,xpomt,il,xpom,il,'%Height','L',dpom,EdwYd,
     1                    1)
      nEdwHeight=EdwLastMade
      il=il+1
      call FeQuestEudMake(id,xpomt,il,xpom,il,'%Width','L',dpom,EdwYd,
     1                    1)
      nEdwWidth=EdwLastMade
      tpom=xpomt+220.
      if(PropFontMeasured.eq.FontInPixels) then
        Veta='%Font height in pixels:'
      else
        Veta='%Font height in points:'
      endif
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPropFontSize=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,PropFontSize,.false.)
      if(PropFontMeasured.eq.FontInPixels) then
        call FeQuestEudOpen(EdwLastMade,15,25,1,0.,0.,0.)
      else
        call FeQuestEudOpen(EdwLastMade,8,14,1,0.,0.,0.)
      endif
      xpomw=xqd*.5-50.
      ilk=il
      il=1
      Veta='Window position'
      call FeQuestLblMake(id,xpomw,il,Veta,'L','N')
      xpomt=xpomw+FeTxLength(Veta)+6.
      Veta='X:'
      xpom=xpomt+FeTxLength(Veta)+6.
      dpomb=50.
      call FeQuestEudMake(id,xpomt,il,xpom,il,Veta,'L',dpomb,EdwYd,0)
      nEdwXpos=EdwLastMade
      il=il+1
      tpom=EdwXMinQuest(nEdwXpos)-QuestXMin(id)
      call FeQuestButtonMake(id,tpom,il,dpomb,ButYd,'Center X')
      nButXcen=ButtonLastMade
      xpomt=EdwUpDownXMaxQuest(nEdwXpos)-QuestXMin(id)+15.
      Veta='Y:'
      xpom=xpomt+FeTxLength(Veta)+5.
      il=il-1
      call FeQuestEudMake(id,xpomt,il,xpom,il,Veta,'L',dpomb,EdwYd,0)
      nEdwYpos=EdwLastMade
      il=il+1
      tpom=EdwXMinQuest(nEdwYpos)-QuestXMin(id)
      call FeQuestButtonMake(id,tpom,2,dpomb,ButYd,'Center Y')
      nButYcen=ButtonLastMade
      il=ilk+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,'For listings use '//
     1                        '%build-in viewer','L',CrwXd,CrwYd,0,0)
      nCrwViewer=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,BuildInViewer)
      dpomb=100.
      xpom=xqd*.5-dpomb-10.
      il=il+1
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,'Che%ck setting')
      nButCheckSet=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpomb+20.
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,
     1                       'Return to pre%vious')
      nButReturnPrev=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      NewHeight=PixelWindowHeight
      NewWidth=PixelWindowWidth
      NewWindowSizeType=WindowSizeType
      CheckSet=.false.
      ReturnPrev=.false.
1400  if(CrwLogicQuest(nCrwWndExactly)) then
        call FeQuestIntEdwOpen(nEdwHeight,NewHeight,.false.)
        call FeQuestIntEdwOpen(nEdwWidth,NewWidth,.false.)
        call FeQuestEudOpen(nEdwHeight,MinHeight,PixelScreenHeight,1,
     1                      0.,0.,0.)
        call FeQuestEudOpen(nEdwWidth,MinWidth,PixelScreenWidth,1,
     1                      0.,0.,0.)
      else
        call FeQuestEdwDisable(nEdwHeight)
        call FeQuestEdwDisable(nEdwWidth)
      endif
      if(CrwLogicQuest(nCrwWndMaximal)) then
        PixelWindowXPos=EdwIntQuest(1,nEdwXpos)
        PixelWindowYPos=EdwIntQuest(1,nEdwYpos)
        call FeQuestEdwDisable(nEdwXpos)
        call FeQuestEdwDisable(nEdwYpos)
        call FeQuestButtonDisable(nButXcen)
        call FeQuestButtonDisable(nButYcen)
      else
        call FeQuestIntEdwOpen(nEdwXpos,PixelWindowXPos,.false.)
        call FeQuestEudOpen(nEdwXpos,0,PixelScreenWidth,1,0.,0.,0.)
        call FeQuestIntEdwOpen(nEdwYpos,PixelWindowYPos,.false.)
        call FeQuestEudOpen(nEdwYpos,0,PixelScreenHeight,1,0.,0.,0.)
        call FeQuestButtonOpen(nButXcen,ButtonOff)
        call FeQuestButtonOpen(nButYcen,ButtonOff)
      endif
      if(ExistFile(IniFilePrev)) then
        call FeQuestButtonOff(nButReturnPrev)
      else
        call FeQuestButtonDisable(nButReturnPrev)
      endif
1500  call FeQuestEvent(id,ich)
      Prvne=.false.
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHeight) then
        call FeQuestIntFromEdw(nEdwHeight,NewHeight)
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwWidth) then
        call FeQuestIntFromEdw(nEdwWidth,NewWidth)
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwWndNormal.and.
     2         CheckNumber.le.nCrwWndExactly)) then
        if(CheckNumber.eq.nCrwWndNormal) then
          NewHeight=nint(float(PixelScreenHeight)*.8)
        else if(CheckNumber.eq.nCrwWndMinimal) then
          NewHeight=MinHeight
        endif
        NewWindowSizeType=CheckNumber-nCrwWndNormal+1
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButXcen) then
        i=(PixelScreenWidth-NewWidth)/2
        call FeQuestIntEdwOpen(nEdwXpos,i,.false.)
        call FeQuestButtonOff(nButXcen)
        EventType=EventEdw
        EventNumber=nEdwXpos
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButYcen) then
        i=(PixelScreenHeight-NewHeight)/2
        call FeQuestIntEdwOpen(nEdwYpos,i,.false.)
        call FeQuestButtonOff(nButYcen)
        EventType=EventEdw
        EventNumber=nEdwYpos
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButCheckSet)
     1  then
        ich=0
        CheckSet=.true.
        call CopyFile(HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini',
     1                IniFilePrev)
        go to 2000
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButReturnPrev) then
        ich=0
        ReturnPrev=.true.
        call MoveFile(IniFilePrev,
     1                HomeDir(:idel(HomeDir))//
     2                MasterName(:idel(MasterName))//'.ini')
        call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  ChangeWindowSize=.false.
      ChangeWindowPosition=.false.
      if(ich.eq.0) then
        BuildInViewer=CrwLogicQuest(nCrwViewer)
        if(NewHeight.ne.PixelWindowHeight.or.
     1     NewWidth.ne.PixelWindowWidth.or.
     2     WindowSizeType.ne.NewWindowSizeType) then
          ChangeWindowSize=.true.
        endif
        if(NewWindowSizeType.ne.WindowMaximal) then
          call FeQuestIntFromEdw(nEdwXPos,PixelWindowXPos)
          call FeQuestIntFromEdw(nEdwYPos,PixelWindowYPos)
          ChangeWindowPosition=PixelWindowXPos.ne.PixelWindowXPosIn.or.
     1                         PixelWindowYPos.ne.PixelWindowYPosIn
        else
          PixelWindowXpos=0
          PixelWindowYPos=0
          ChangeWindowPosition=ChangeWindowSize
        endif
        WindowSizeType=NewWindowSizeType
        if(NewWindowSizeType.eq.WindowNormal.or.
     1     NewWindowSizeType.eq.WindowMinimal) then
          PixelWindowHeight=NewHeight-mod(NewHeight,3)
          PixelWindowWidth=(PixelWindowHeight*4)/3
        else if(NewWindowSizeType.eq.WindowExactly) then
          PixelWindowHeight=NewHeight
          PixelWindowWidth=NewWidth
        endif
        call FeQuestIntFromEdw(nEdwPropFontSize,PropFontSize)
        ChangeFont=PropFontSize.ne.PropFontSizeIn
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
2500  call FeQuestRemove(id)
      if(ich.eq.0) then
        if(CheckSet.or.ReturnPrev) then
          call DeletePomFiles
          call FeGrQuit
          call OpenWorkSpace
          call FeMakeGrWin(0.,0.,YBottomMargin,0.)
          if(CheckSet.and.
     1       .not.FileDiff(IniFilePrev,HomeDir(:idel(HomeDir))//
     2                     MasterName(:idel(MasterName))//'.ini'))
     3      call DeleteFile(IniFilePrev)
          go to 1000
        endif
        if(FileDiff(IniFileSave,
     1              HomeDir(:idel(HomeDir))//
     2              MasterName(:idel(MasterName))//'.ini')) then
          if(.not.FeYesNo(-1.,-1.,
     1       'Do you want to save the made changes?',1)) go to 9000
          if(ChangeWindowSize.or.ChangeWindowPosition.or.ChangeFont)
     1      then
            call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                      MasterName(:idel(MasterName))//'.ini')
            call DeletePomFiles
            call FeGrQuit
            IGrOn=0
          endif
        endif
      endif
      go to 9999
9000  call CopyFile(IniFileSave,HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini')
9999  call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      call DeleteFile(IniFileSave)
      call FeTmpFilesClear(IniFileSave)
      call DeleteFile(IniFilePrev)
      call FeTmpFilesClear(IniFilePrev)
      call SetBasicConstants
      call SetFePCConstants
      return
      end
      subroutine FeSetPrograms
      use Basic_mod
      parameter (NPrograms=11)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 EdwStringQuest,IniFileSave,Veta,CurrentDirO
      character*80 t80
      character*30 :: LabelsEdw(NPrograms) =
     1             (/'E%ditor name:             ',
     2               '%X-Shape:                 ',
     3               'G%raphic viewer:          ',
     4               '%3d visualization of maps:',
     5               'DPlo%t:                   ',
     6               'S%IR                      ',
     7               '%Expo                     ',
     8               'S%helxt:                  ',
     9               'Shell comm%and:           ',
     a               'G%nuplot command:         ',
     1               '%GS command:              '/)
      integer FeChdir,RolMenuSelectedQuest
      logical FeYesNo,FileDiff
500   AllowResizing=.true.
      NSIR=1
      NExpo=1
      IniFileSave='jins'
      call CreateTmpFile(IniFileSave,i,0)
      call FeTmpFilesAdd(IniFileSave)
1000  call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini',IniFileSave)
      CurrentDirO=CurrentDir
      id=NextQuestId()
      xqd=500.
      il=NPrograms
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Programs',0,LightGray,
     1                   0,0)
      il=0
      tpom=5.
      dpom=250.
      xpom=0.
      do i=1,NPrograms
        xpom=max(FeTxLengthUnder(LabelsEdw(i)),xpom)
      enddo
      xpom=tpom+xpom+10.
      do i=1,NPrograms
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,LabelsEdw(i),'L',dpom,
     1                      EdwYd,1)
        if(i.eq.1) then
          nEdwEdit=EdwLastMade
          Veta=EditorName
          dpomb=80.
          xpomb=(xqd+xpom+dpom-dpomb)*.5
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,'Brow%se')
          nButBrowse=ButtonLastMade
          call FeQuestButtonOpen(nButBrowse,ButtonOff)
        else if(i.eq.2) then
          nEdwXShape=EdwLastMade
          Veta=CallXShape
        else if(i.eq.3) then
          nEdwGrViewer=EdwLastMade
          Veta=CallGraphic
        else if(i.eq.4) then
          nEdw3dMaps=EdwLastMade
          Veta=Call3dMaps
        else if(i.eq.5) then
          nEdwDPlot=EdwLastMade
          Veta=CallDPlot
        else if(i.eq.6) then
          nEdwSIR=EdwLastMade
          xpomp=tpom+27.
          dpomp=50.
          call FeQuestRolMenuMake(id,xpomp,il,xpomp,il,' ','L',dpomp,
     1                            EdwYd,1)
          nRolMenuSIR=RolMenuLastMade
          call FeQuestRolMenuOpen(RolMenuLastMade,SIRName,6,NSIR)
          Veta=DirSIR(NSIR)(:idel(DirSIR(NSIR)))//
     1         CallSIR(NSIR)(:idel(CallSIR(NSIR)))
        else if(i.eq.7) then
          nEdwExpo=EdwLastMade
          call FeQuestRolMenuMake(id,xpomp,il,xpomp,il,' ','L',dpomp,
     1                            EdwYd,1)
          nRolMenuExpo=RolMenuLastMade
          call FeQuestRolMenuOpen(RolMenuLastMade,ExpoName,5,NExpo)

          Veta=DirExpo(NExpo)(:idel(DirExpo(NExpo)))//
     1      CallExpo(NExpo)(:idel(CallExpo(NExpo)))
        else if(i.eq.8) then
          nEdwShelxt=EdwLastMade
          Veta=CallShelxt
        else if(i.eq.9) then
          nEdwShell=EdwLastMade
          Veta=FirstCommand
        else if(i.eq.10) then
          nEdwGnuplot=EdwLastMade
          Veta=CallGnuplot
        else if(i.eq.11) then
          nEdwGS=EdwLastMade
          Veta=CallGS
        endif
        call FeQuestStringEdwOpen(EdwLastMade,Veta)
      enddo
      MakeExternalCheck=1
1500  call FeQuestEvent(id,ich)
      izpet=0
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButBrowse)
     1  then
        Veta=EdwStringQuest(nEdwLast)
        call ExtractFileName(Veta,t80)
        call ExtractDirectory(Veta,Veta)
        i=FeChdir(Veta)
        call FeFileManager('Browser',t80,'*.*',0,.false.,ich)
        if(ich.ne.0) go to 1500
        Veta=CurrentDir(:idel(CurrentDir))//t80(:idel(t80))
        call FeQuestStringEdwOpen(nEdwLast,Veta)
        go to 1500
      else if(CheckType.eq.EventEdw) then
        nEdwLast=CheckNumber
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuSIR) then
        i=RolMenuSelectedQuest(nRolMenuSIR)
        if(i.ne.NSIR) then
          Veta=EdwStringQuest(nEdwSIR)
          call ExtractFileName(Veta,CallSIR(NSIR))
          call ExtractDirectory(Veta,DirSIR(NSIR))
          NSIR=i
          Veta=DirSIR(NSIR)(:idel(DirSIR(NSIR)))//
     1         CallSIR(NSIR)(:idel(CallSIR(NSIR)))
          call FeQuestStringEdwOpen(nEdwSIR,Veta)
          nEdwLast=nEdwSIR
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuExpo) then
        i=RolMenuSelectedQuest(nRolMenuExpo)
        if(i.ne.NExpo) then
          Veta=EdwStringQuest(nEdwExpo)
          call ExtractFileName(Veta,CallExpo(NExpo))
          call ExtractDirectory(Veta,DirExpo(NExpo))
          NExpo=i
          Veta=DirExpo(NExpo)(:idel(DirExpo(NExpo)))//
     1         CallExpo(NExpo)(:idel(CallExpo(NExpo)))
          call FeQuestStringEdwOpen(nEdwExpo,Veta)
          nEdwLast=nEdwExpo
        endif
        go to 1500
      else if(CheckType.eq.EventResize) then
        ich=0
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  if(ich.eq.0) then
        EditorName=EdwStringQuest(nEdwEdit)
        FirstCommand=EdwStringQuest(nEdwShell)
        CallXShape=EdwStringQuest(nEdwXShape)
        CallGraphic=EdwStringQuest(nEdwGrViewer)
        Call3dMaps=EdwStringQuest(nEdw3dMaps)
        CallDPlot=EdwStringQuest(nEdwDPlot)
        CallGnuplot=EdwStringQuest(nEdwGnuplot)
        CallGS=EdwStringQuest(nEdwGS)
        Veta=EdwStringQuest(nEdwSIR)
        call ExtractFileName(Veta,CallSIR(NSIR))
        call ExtractDirectory(Veta,DirSIR(NSIR))
        Veta=EdwStringQuest(nEdwExpo)
        call ExtractFileName(Veta,CallExpo(NExpo))
        call ExtractDirectory(Veta,DirExpo(NExpo))
        CallShelxt=EdwStringQuest(nEdwShelxt)
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
2500  call FeQuestRemove(id)
      if(CheckType.eq.EventResize) then
        call FeMakeGrWin(0.,0.,YBottomMargin,0.)
        call FeBottomInfo(' ')
        go to 500
      endif
      MakeExternalCheck=0
      if(ich.eq.0) then
        if(FileDiff(IniFileSave,
     1              HomeDir(:idel(HomeDir))//
     2              MasterName(:idel(MasterName))//'.ini')) then
          if(.not.FeYesNo(-1.,-1.,
     1       'Do you want to save the made changes?',1)) go to 9000
        endif
      endif
      go to 9999
9000  call CopyFile(IniFileSave,HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini')
9999  call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      AllowResizing=.false.
      return
      end
      logical function ProgramPresent(Name,CallString)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Name,CallString
      character*256 Veta
      logical ExistFile
      idl=idel(CallString)
      if(OpSystem.le.0) then
        Veta=' '
        do i=1,idl
          if(CallString(i:i).eq.'/') exit
          Veta(i:i)=CallString(i:i)
        enddo
        idl=idel(Veta)
      else
        Veta=CallString
      endif
      if(Veta(idl:idl).eq.'&') idl=idl-1
      if(ExistFile(Veta(:idl))) then
        ProgramPresent=.true.
      else
        ProgramPresent=.false.
        call FeChybne(-1.,-1.,
     1    'the '//Name(:idel(Name))//' as defined is not present. '//
     2    'Please correct the temporary defintion in "Tools->'//
     3    'Programs."',
     4    'The temporary defintion: "'//
     5    CallString(:idel(CallString))//'"',SeriousError)
      endif
      return
      end
      subroutine BatchHlavicka(Text,Znak)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text
      character*256 t256,s256
      character*1 Znak
      idl=idel(Text)+4
      t256='|'
      do i=2,idl-1
        t256(i:i)=Znak
      enddo
      t256(idl:idl)='|'
      call FeLstWriteLine(t256,-1)
      write(LogLn,FormA) t256(:idl)
      s256='| '//Text(:idl-4)//' |'
      call FeLstWriteLine(s256,-1)
      write(LogLn,FormA) s256(:idl)
      call FeLstWriteLine(t256,-1)
      write(LogLn,FormA) t256(:idl)
      return
      end


C*****************************************
C***                                   ***
C***          Graphic section          ***
C***                                   ***
C*****************************************



      subroutine FeEvent(Imm)
      include 'fepc.cmn'
      integer FeGetSystemTime
      logical FeTestIn,LeftIsDown
      character*1 Znak
      integer FeGetEdwStringPosition
      real MoveX,MoveY
      data DoubleClickX,DoubleClickY/2*0./,LeftIsDown/.false./
      data MoveX,MoveY/2*-1111./
      if(Imm.eq.-1) StartTime=FeGetSystemTime()
1000  EventType=0
      EventNumber=0
      EventNumberAbs=0
      if(Imm.le.0) then
        if(Imm.ne.-2) call FeReleaseOutput
        if(.not.TakeMouseMove) call FeMouseShape(0)
        if(Imm.eq.0) then
          call FeMessage(0)
        else
          call FeMessage(1)
          if(EventType.eq.0) then
            if(WaitTime.gt.0) then
              i=FeGetSystemTime()-StartTime
              if(i.gt.WaitTime) then
                EventType=EventSystem
                EventNumber=JeTimeOut
              else
                RemainTime=WaitTime-i
              endif
              go to 9100
            endif
            go to 1000
          endif
        endif
      else
        call FeMessage(1)
        if(EventType.eq.0) go to 9100
      endif

      if(EventType.eq.EventResize.or.
     1   (EventType.eq.EventSystem.and.EventNumber.eq.JeMimoOkno)) then
        go to 9100
      endif
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown.and.
     1   DoubleClickCount.eq.1.and.FeTimeDiff().lt..5.and.
     2   abs(DoubleClickX-Xpos).lt.1..and.
     3   abs(DoubleClickY-Ypos).lt.1.) then
        EventType=EventMouse
        EventNumber=JeDoubleClick
        DoubleClickCount=0
        go to 9990
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        LeftIsDown=.true.
        if(FeTestIn()) then
          if(EventType.eq.EventButton.or.EventType.eq.EventEdw.or.
     1       EventType.eq.EventCrw.or.EventType.eq.EventRolMenu.or.
     2       EventType.eq.EventEdwUp) then
            if(EventType.ne.EventEdw) LeftIsDown=.false.
            go to 9100
          endif
        else
          EventType=EventMouse
          EventNumber=JeLeftDown
        endif
        go to 9000
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
        LeftIsDown=.false.
        go to 9990
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        if(FeTestIn()) then
          if(EventType.eq.EventSbwMenu.and.SbwRightClickAllowed) then
            EventType=EventSbwRightClick
            go to 9100
          endif
        endif
        EventType=EventMouse
        EventNumber=JeRightDown
        go to 9100
      else if(EventType.eq.EventMouse.and.
     1        (EventNumber.eq.JeRightUp.or.
     2         EventNumber.eq.JeKoleckoKSobe.or.
     3         EventNumber.eq.JeKoleckoOdSebe)) then
        go to 9100
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(abs(MoveX-Xpos).gt.0..or.abs(MoveY-Ypos).gt.0.) then
          MoveX=Xpos
          MoveY=Ypos
          if(LeftIsDown.and.EdwActive.gt.0) then
            if(xpos.ge.EdwXmin(EdwActive).and.
     1         xpos.le.EdwXmax(EdwActive).and.
     2         ypos.ge.EdwYmin(EdwActive).and.
     3         ypos.le.EdwYmax(EdwActive)) then
              EventType=EventEdwSel
              EventNumber=FeGetEdwStringPosition(EdwActive,xpos)
              go to 9100
            endif
          endif
          if(TakeMouseMove.and.DoubleClickCount.eq.0) then
            EventType=EventMouse
            EventNumber=JeMove
            go to 9100
          endif
        else
          EventType=0
          EventNumber=0
          go to 1000
        endif
      endif
      if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1   EventType.eq.EventCtrl) then
        go to 9100
      else if((EventType.eq.EventAlt.and.EventNumber.eq.JeF4).or.
     1        (EventType.eq.EventSystem.and.EventNumber.eq.JeWinClose))
     2  then
        if(AskIfQuit) then
          EventType=EventGlobal
          EventNumber=GlobalClose
          go to 9100
        endif
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        stop
      else if(EventType.eq.EventAlt) then
        Znak=char(EventNumber)
        do i=ButtonFr,ButtonTo
          if(ButtonState(i).ne.ButtonOff.or.Znak.ne.ButtonZ(i:i)) cycle
          EventType=EventButton
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=EdwFr,EdwTo
          if(EdwState(i).ne.EdwOpened.or.Znak.ne.EdwZ(i:i)) cycle
          EventType=EventEdw
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=CrwFr,CrwTo
          if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1       CrwState(i).eq.CrwDisabled.or.CrwState(i).eq.CrwLocked.or.
     2       Znak.ne.CrwZ(i:i)) cycle
          EventType=EventCrw
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do ii=1,KartNDol
          i=KartDolButt(ii)
          if(i.eq.0) cycle
          if(ButtonState(i).ne.ButtonOff.or.Znak.ne.ButtonZ(i:i)) cycle
          if(i.eq.KartESC.or.i.eq.KartOK) then
            EventType=EventButton
          else
            EventType=EventKartButt
          endif
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=RolMenuFr,RolMenuTo
          if(RolMenuState(i).eq.RolMenuRemoved.or.
     1       RolMenuState(i).eq.RolMenuClosed.or.
     2       RolMenuState(i).eq.RolMenuDisabled.or.
     3       Znak.ne.RolMenuZ(i:i)) cycle
          EventType=EventRolMenu
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        go to 9100
      endif
      EventType=0
      EventNumber=0
      go to 1000
9000  if(DoubleClickAllowed) then
        DoubleClickCount=FeTimeDiff()
        DoubleClickCount=1
        DoubleClickX=Xpos
        DoubleClickY=Ypos
      endif
      go to 9990
9100  DoubleClickCount=0
9990  if(Imm.eq.0) then
        if((EventType.ne.EventMouse.or.EventNumber.ne.JeMove)
     1     .and.AllowChangeMouse) call FeMouseShape(3)
        if(.not.TakeMouseMove) call FeDeferOutput()
      endif
      if(EventNumberAbs.eq.0) EventNumberAbs=EventNumber
9999  return
      end
      logical function FeTestIn()
      include 'fepc.cmn'
      integer CrwLastClick,RolMenuLastClick,EdwLastClick,
     1        FeGetEdwStringPosition
      data CrwLastClick,RolMenuLastClick,EdwLastClick/3*0/
      EventType=0
      EventNumber=0
      EventNumberAbs=0
      if(.not.DelejTestIn) go to 9000
      do i=1,ButtonMax
        if(((i.lt.ButtonFr.or.i.gt.ButtonTo).and..not.ButtonAlways(i))
     1     .or.ButtonState(i).ne.ButtonOff) cycle
        if(xpos.ge.ButtonXmin(i).and.xpos.le.ButtonXmax(i).and.
     1     ypos.ge.ButtonYmin(i).and.ypos.le.ButtonYmax(i)) then
          EventType=EventButton
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do ii=1,KartNDol
        i=KartDolButt(ii)
        if(i.eq.0) cycle
        if(ButtonState(i).ne.ButtonOff) cycle
        if(xpos.ge.ButtonXmin(i).and.xpos.le.ButtonXmax(i).and.
     1     ypos.ge.ButtonYmin(i).and.ypos.le.ButtonYmax(i)) then
          if(i.eq.KartESC.or.i.eq.KartOK.or.i.eq.KartCancel) then
            EventType=EventButton
          else
            EventType=EventKartButt
          endif
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).ne.EdwOpened) cycle
        if((xpos.ge.EdwXmin(i).and.xpos.le.EdwXmax(i).and.
     1      ypos.ge.EdwYmin(i).and.ypos.le.EdwYmax(i)).or.
     2     (xpos.ge.EdwLabelXmin(i).and.xpos.le.EdwLabelXmax(i).and.
     3      ypos.ge.EdwLabelYmin(i).and.ypos.le.EdwLabelYmax(i))) then
          EventType=EventEdw
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          if(xpos.ge.EdwXmin(i).and.xpos.le.EdwXmax(i).and.
     1       ypos.ge.EdwYmin(i).and.ypos.le.EdwYmax(i)) then
            KurzorClick=FeGetEdwStringPosition(i,XPos)
          else
            KurzorClick=EdwKurzor(i)
          endif
          go to 9000
        else
          if(EdwUpDown(i).ne.0) then
            if(xpos.ge.EdwUpDownXmin(i).and.xpos.le.EdwUpDownXmax(i))
     1        then
              if(ypos.ge.EdwUpDownYmin(1,i).and.
     1           ypos.le.EdwUpDownYmax(1,i)) then
                EventType=EventEdwUp
                EventNumber=i-EdwFr+1
                EventNumberAbs=i
                go to 9000
              else if(ypos.ge.EdwUpDownYmin(2,i).and.
     1                ypos.le.EdwUpDownYmax(2,i)) then
                EventType=EventEdwDown
                EventNumber=i-EdwFr+1
                EventNumberAbs=i
                go to 9000
              endif
            endif
          endif
        endif
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).eq.EdwRemoved.or.EdwState(i).eq.EdwDisabled.or.
     1     EdwState(i).eq.EdwClosed) cycle
        if(xpos.ge.EdwXMin(i).and.xpos.le.EdwXMax(i).and.
     1     ypos.ge.EdwYMin(i).and.ypos.le.EdwYMax(i)) then
          if(EdwState(i).eq.EdwLocked) then
            if(EdwLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                EdwLastClick=0
                call FeEdwOpen(i)
                go to 1550
              endif
            else
              EdwLastClick=FeTimeDiff()
              EdwLastClick=i
            endif
          endif
          go to 9000
1550      EventType=EventEdwUnlock
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1     CrwState(i).eq.CrwLocked.or.CrwState(i).eq.CrwDisabled) cycle
        if((xpos.ge.CrwXmin(i).and.xpos.le.CrwXmax(i).and.
     1      ypos.ge.CrwYmin(i).and.ypos.le.CrwYmax(i)).or.
     2     (xpos.ge.CrwLabelXMin(i).and.xpos.le.CrwLabelXMax(i).and.
     1      ypos.ge.CrwLabelYMin(i).and.ypos.le.CrwLabelYMax(i))) then
          EventType=EventCrw
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1     CrwState(i).eq.CrwDisabled) cycle
        if(xpos.ge.CrwXmin(i).and.xpos.le.CrwXmax(i).and.
     1     ypos.ge.CrwYmin(i).and.ypos.le.CrwYmax(i)) then
          if(CrwState(i).eq.CrwLocked) then
            if(CrwLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                j=CrwExGr(i)
                CrwLastClick=0
                if(j.le.0) then
                  if(CrwLogic(i)) then
                    call FeCrwOn(i)
                  else
                    call FeCrwOff(i)
                  endif
                  go to 2040
                endif
                do k=CrwFr,CrwTo
                  if(CrwState(k).eq.CrwRemoved.or.
     1               CrwState(k).eq.CrwDisabled.or.
     1               CrwState(k).eq.CrwClosed .or.CrwExGr(k).ne.j)
     2              cycle
                  call FeCrwOff(k)
                enddo
                call FeCrwOn(i)
                go to 2040
              endif
            else
              CrwLastClick=FeTimeDiff()
              CrwLastClick=i
            endif
          endif
          go to 9000
2040      EventType=EventCrwUnlock
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=1,RolMenuMax
        if(((i.lt.RolMenuFr.or.i.gt.RolMenuTo).and.
     1      .not.RolMenuAlways(i)).or.
     2     RolMenuState(i).eq.RolMenuRemoved.or.
     3     RolMenuState(i).eq.RolMenuLocked.or.
     4     RolMenuState(i).eq.RolMenuDisabled.or.
     5     RolMenuState(i).eq.RolMenuClosed) cycle
        if((xpos.ge.RolMenuButtXMin(i).and.
     1      xpos.le.RolMenuButtXMax(i).and.
     2      ypos.ge.RolMenuButtYMin(i).and.
     3      ypos.le.RolMenuButtYMax(i)).or.
     4     (xpos.ge.RolMenuLabelXMin(i).and.
     5      xpos.le.RolMenuLabelXMax(i).and.
     6      ypos.ge.RolMenuLabelYMin(i).and.
     7      ypos.le.RolMenuLabelYMax(i))) then
          EventType=EventRolMenu
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=RolMenuFr,RolMenuTo
        if(((i.lt.RolMenuFr.or.i.gt.RolMenuTo).and.
     1      .not.RolMenuAlways(i)).or.
     2     RolMenuState(i).eq.RolMenuRemoved.or.
     3     RolMenuState(i).eq.RolMenuClosed) cycle
        if(xpos.ge.RolMenuTextXMin(i).and.
     1     xpos.le.RolMenuTextXMax(i).and.
     2     ypos.ge.RolMenuTextYMin(i).and.
     3     ypos.le.RolMenuTextYMax(i)) then
          if(RolMenuState(i).eq.RolMenuLocked) then
            if(RolMenuLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                RolMenuLastClick=0
                call FeRolMenuOpen(i)
                go to 3250
              endif
            else
              RolMenuLastClick=FeTimeDiff()
              RolMenuLastClick=i
            endif
          endif
          go to 9000
3250      EventType=EventRolMenuUnlock
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=IconFr,IconTo
        if(IconState(i).eq.IconRemoved.or.IconState(i).eq.IconDisabled)
     1     cycle
        if(xpos.ge.IconXmin(i).and.xpos.le.IconXmax(i).and.
     1     ypos.ge.IconYmin(i).and.ypos.le.IconYmax(i)) then
          EventType=EventIcon
          EventNumber=i
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=1,NKart
        if(xpos.ge.KartSwXmin(i).and.xpos.le.KartSwXMax(i).and.
     1     ypos.ge.KartSwYMin(i).and.ypos.le.KartSwYMax(i)) then
          EventType=EventKartSw
          EventNumber=i
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened.or.SbwBars(i).eq.SbwHorizontal.or.
     1     SbwDelPlovak(i).le.0.) cycle
        do j=1,2
          if(xpos.ge.SbwXminPosun(j,i).and.
     1       xpos.le.SbwXmaxPosun(j,i).and.
     2       ypos.ge.SbwYminPosun(j,i).and.
     3       ypos.le.SbwYmaxPosun(j,i)) then
            if(j.eq.1) then
              EventType=EventSbwLineUp
            else
              EventType=EventSbwLineDown
            endif
            go to 6080
          endif
        enddo
        if(xpos.ge.SbwXminPruh(1,i).and.
     1     xpos.le.SbwXmaxPruh(1,i)) then
          if(ypos.ge.SbwYmaxPlovak(i)+3..and.
     1       ypos.le.SbwYminPosun(1,i)-3.) then
            EventType=EventSbwPageUp
            go to 6080
          endif
          if(ypos.ge.SbwYmaxPosun(2,i)+3..and.
     1       ypos.le.SbwYminPlovak(i)-3.) then
            EventType=EventSbwPageDown
            go to 6080
          endif
        endif
        if(xpos.ge.SbwXminPlovak(i).and.
     1     xpos.le.SbwXmaxPlovak(i).and.
     2     ypos.ge.SbwYminPlovak(i).and.
     3     ypos.le.SbwYmaxPlovak(i)) then
          EventType=EventSbwPlovak
          go to 6080
        endif
        cycle
6080    EventNumber=i-SbwFr+1
        EventNumberAbs=i
        go to 9000
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened.or.SbwBars(i).eq.SbwVertical.or.
     1     SbwDelStrelka(i).le.0.) cycle
        do j=3,4
          if(xpos.ge.SbwXminPosun(j,i).and.
     1       xpos.le.SbwXmaxPosun(j,i).and.
     2       ypos.ge.SbwYminPosun(j,i).and.
     3       ypos.le.SbwYmaxPosun(j,i)) then
            if(j.eq.3) then
              EventType=EventSbwColumnLeft
            else
              EventType=EventSbwColumnRight
            endif
            go to 6180
          endif
        enddo
        if(ypos.ge.SbwYminPruh(2,i).and.
     1     ypos.le.SbwYmaxPruh(2,i)) then
          if(xpos.ge.SbwXmaxPosun(3,i)+3..and.
     1       xpos.le.SbwXminStrelka(i)-3.) then
            EventType=EventSbwColumnStrongLeft
            go to 6180
          endif
          if(xpos.ge.SbwXmaxStrelka(i)+3..and.
     1       xpos.le.SbwXminPosun(4,i)-3.) then
            EventType=EventSbwColumnStrongRight
            go to 6180
          endif
        endif
        if(xpos.ge.SbwXminStrelka(i).and.
     1     xpos.le.SbwXmaxStrelka(i).and.
     2     ypos.ge.SbwYminStrelka(i).and.
     3     ypos.le.SbwYmaxStrelka(i)) then
          EventType=EventSbwStrelka
          go to 6180
        endif
        cycle
6180    EventNumber=i-SbwFr+1
        EventNumberAbs=i
        go to 9000
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened) cycle
        if(xpos.ge.SbwXmin(i).and.xpos.le.SbwXmax(i).and.
     1     ypos.ge.SbwYmin(i).and.ypos.le.SbwYmax(i).and.
     2     (SbwType(i).eq.SbwMenuType.or.SbwType(i).eq.SbwSelectType))
     3    then
          iy=ifix((SbwYmax(i)-ypos)/SbwYStep(i))+1
          do ix=1,SbwStripes(i)
            if(xpos.ge.SbwXStripe(ix,i).and.xpos.le.SbwXStripe(ix+1,i))
     1        go to 6260
          enddo
          ix=1
6260      j=(ix-1)*SbwLines(i)+iy
          EventType=EventSbwMenu
          j=i+100*j
          EventNumber=j-SbwFr+1
          EventNumberAbs=j
        endif
      enddo
9000  FeTestIn=EventType.ne.0
      return
      end
      integer function FeMenu(xmi,ymi,men,i1,i2,id,jak)
      include 'fepc.cmn'
      character*(*) men(i1:i2)
      character*80 MenuTmp
      character*30 MenZ
      character*1 Znak
      dimension xup(5),yup(5)
      call FeKartRefresh(0)
      KartIdOld=KartId
      KartId=0
      xm=xmi
      ym=ymi
      xd=20.
      MenZ=' '
      do i=i1,i2
        xd=max(xd,FeTxLengthUnder(Men(i))+2.*EdwMarginSize)
      enddo
      yd=float(i2-i1+1)*MenuLineWidth
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      xm=anint(xm)
      xd=anint(xd)
      ym=anint(ym)
      yd=anint(yd)
      MenuTmp='jmnu'
      if(OpSystem.le.0) call CreateTmpFile(MenuTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp)
      if(jak.eq.0) then
        ip=LightGray
      else
        ip=White
      endif
      call FeFillRectangle(xm-1.,xm+xd+1.,ym-1.,ym+yd+1.,4,0,0,ip)
      if(Jak.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                   LastQuest.ne.0)
      else
        xup(1)=xm
        yup(1)=ym
        xup(2)=xm+xd
        yup(2)=ym
        xup(3)=xup(2)
        yup(3)=ym+yd
        xup(4)=xm
        yup(4)=yup(3)
        xup(5)=xup(1)
        yup(5)=yup(1)
      endif
      idef=i1
      yp=ym+yd
      do i=i1,i2
        j=i-i1+1
        if(i.eq.idef) then
          ipp=White
          icc=DarkBlue
          call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                       -4.)
        else
          ipp=Black
          icc=ip
        endif
        call FeWrMenuItem(xm,yp,xd,Men(i),MenZ(j:j),ipp,icc)
        yp=yp-MenuLineWidth
      enddo
      if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      call mala(MenZ)
      if(VasekTest.ne.0) then
        idefo=idel(MenZ)
        do 1600i=1,idefo-1
          if(MenZ(i:i).eq.' ') go to 1600
          do j=i+1,idefo
            if(MenZ(i:i).eq.MenZ(j:j)) go to 1610
          enddo
1600    continue
        go to 1900
1610    TextInfo(1)=MenZ
        Ninfo=2
        write(TextInfo(2),'(2i3)') i,j
        call FeInfoOut(-1.,-1.,'Nejednoznacna rychla klavesa:','L')
      endif
1900  call FeFrToChange(LastActiveQuest,0)
      call FeFrToRefresh(-1)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      call FeMouseShape(0)
2000  call FeEvent(0)
      idefo=idef
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(Xpos.ge.xm.and.Xpos.le.xm+xd.and.
     1     Ypos.ge.ym.and.Ypos.le.ym+yd) then
          idef=nint((ym+yd-Ypos)/MenuLineWidth-.5)+i1
        else
          idef=i1-1
        endif
        idef=min(idef,i2)
      else if(EventType.eq.EventKey.and.(EventNumber.eq.JeUp.or.
     1                                   EventNumber.eq.JeDown)) then
        if(idef.ge.i1) then
          if(EventNumber.eq.JeUp) then
            idef=idef-1
            if(idef.lt.i1) idef=i2
          else
            idef=idef+1
            if(idef.gt.i2) idef=i1
          endif
        endif
        call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                     -4.)
      else if(EventType.eq.EventASCII) then
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(MenZ,Znak)
        if(i.ne.0) then
          idef=i+i1-1
          go to 3000
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        if(idef.ge.i1) go to 3000
      else if((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).or.
     1        (EventType.eq.EventButton.and.EventNumber.eq. 1)) then
        idef=i1-1
        go to 3000
      endif
      if(idefo.ne.idef) then
        if(idefo.ge.i1) call FeWrMenuItem(xm,ym+yd
     1                                   -float(idefo-i1)*MenuLineWidth,
     2                                    xd,Men(idefo),Znak,Black,ip)
        if(idef. ge.i1) call FeWrMenuItem(xm,ym+yd
     1                                   -float(idef -i1)*MenuLineWidth,
     2                                    xd,Men(idef),Znak,White,
     3                                    DarkBlue)
        if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      endif
      go to 2000
3000  call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp,0)
      FeMenu=idef
      TakeMouseMove=.false.
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      if(KartIdOld.ne.0) then
        call FeKartRefresh(1)
        KartId=KartIdOld
      endif
      call FeFrToRefresh(LastActiveQuest)
      return
      end
      integer function FeMenuFixLength(xmi,ymi,xdi,men,i1,i2,id,jak)
      include 'fepc.cmn'
      character*(*) men(i1:i2)
      character*80 MenuTmp
      character*30 MenZ
      character*1 Znak
      dimension xup(5),yup(5)
      call FeKartRefresh(0)
      KartIdOld=KartId
      KartId=0
      xm=xmi
      ym=ymi
      MenZ=' '
      xd=xdi
      yd=float(i2-i1+1)*MenuLineWidth
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      xm=anint(xm)
      xd=anint(xd)
      ym=anint(ym)
      yd=anint(yd)
      MenuTmp='jmnu'
      if(OpSystem.le.0) call CreateTmpFile(MenuTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp)
      if(jak.eq.0) then
        ip=LightGray
      else
        ip=White
      endif
      call FeFillRectangle(xm-1.,xm+xd+1.,ym-1.,ym+yd+1.,4,0,0,ip)
      if(Jak.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                   LastQuest.ne.0)
      else
        xup(1)=xm
        yup(1)=ym
        xup(2)=xm+xd
        yup(2)=ym
        xup(3)=xup(2)
        yup(3)=ym+yd
        xup(4)=xm
        yup(4)=yup(3)
        xup(5)=xup(1)
        yup(5)=yup(1)
      endif
      idef=i1
      yp=ym+yd
      do i=i1,i2
        j=i-i1+1
        if(i.eq.idef) then
          ipp=White
          icc=DarkBlue
          call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                       -4.)
        else
          ipp=Black
          icc=ip
        endif
        call FeWrMenuItem(xm,yp,xd,Men(i),MenZ(j:j),ipp,icc)
        yp=yp-MenuLineWidth
      enddo
      if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      call mala(MenZ)
      if(VasekTest.ne.0) then
        idefo=idel(MenZ)
        do 1600i=1,idefo-1
          if(MenZ(i:i).eq.' ') go to 1600
          do j=i+1,idefo
            if(MenZ(i:i).eq.MenZ(j:j)) go to 1610
          enddo
1600    continue
        go to 1900
1610    TextInfo(1)=MenZ
        Ninfo=2
        write(TextInfo(2),'(2i3)') i,j
        call FeInfoOut(-1.,-1.,'Nejednoznacna rychla klavesa:','L')
      endif
1900  call FeFrToChange(LastActiveQuest,0)
      call FeFrToRefresh(-1)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      call FeMouseShape(0)
2000  call FeEvent(0)
      idefo=idef
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(Xpos.ge.xm.and.Xpos.le.xm+xd.and.
     1     Ypos.ge.ym.and.Ypos.le.ym+yd) then
          idef=nint((ym+yd-Ypos)/MenuLineWidth-.5)+i1
        else
          idef=i1-1
        endif
        idef=min(idef,i2)
      else if(EventType.eq.EventKey.and.(EventNumber.eq.JeUp.or.
     1                                   EventNumber.eq.JeDown)) then
        if(idef.ge.i1) then
          if(EventNumber.eq.JeUp) then
            idef=idef-1
            if(idef.lt.i1) idef=i2
          else
            idef=idef+1
            if(idef.gt.i2) idef=i1
          endif
        endif
        call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                     -4.)
      else if(EventType.eq.EventASCII) then
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(MenZ,Znak)
        if(i.ne.0) then
          idef=i+i1-1
          go to 3000
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        if(idef.ge.i1) go to 3000
      else if((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).or.
     1        (EventType.eq.EventButton.and.EventNumber.eq. 1)) then
        idef=i1-1
        go to 3000
      endif
      if(idefo.ne.idef) then
        if(idefo.ge.i1) call FeWrMenuItem(xm,ym+yd
     1                                   -float(idefo-i1)*MenuLineWidth,
     2                                    xd,Men(idefo),Znak,Black,ip)
        if(idef. ge.i1) call FeWrMenuItem(xm,ym+yd
     1                                   -float(idef -i1)*MenuLineWidth,
     2                                    xd,Men(idef),Znak,White,
     3                                    DarkBlue)
        if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      endif
      go to 2000
3000  call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp,0)
      FeMenuFixLength=idef
      TakeMouseMove=.false.
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      if(KartIdOld.ne.0) then
        call FeKartRefresh(1)
        KartId=KartIdOld
      endif
      call FeFrToRefresh(LastActiveQuest)
      return
      end
      integer function FeMenuNew(xmi,ymi,xdi,men,menl,i1,i2,id,jak)
      include 'fepc.cmn'
      character*(*) men(i1:i2)
      character*80 MenuTmp
      character*30 MenZ
      character*1 Znak
      dimension xup(5),yup(5),menl(i1:i2)
      call FeKartRefresh(0)
      KartIdOld=KartId
      KartId=0
      xm=xmi
      ym=ymi
      MenZ=' '
      if(xdi.lt.0.) then
        xd=20.
        do i=i1,i2
          xd=max(xd,FeTxLengthUnder(Men(i))+2.*EdwMarginSize)
        enddo
      else
        xd=xdi
      endif
      yd=float(i2-i1+1)*MenuLineWidth
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      xm=anint(xm)
      xd=anint(xd)
      ym=anint(ym)
      yd=anint(yd)
      MenuTmp='jmnu'
      if(OpSystem.le.0) call CreateTmpFile(MenuTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp)
      if(jak.eq.0) then
        ip=LightGray
      else
        ip=White
      endif
      call FeFillRectangle(xm-1.,xm+xd+1.,ym-1.,ym+yd+1.,4,0,0,ip)
      if(Jak.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                   LastQuest.ne.0)
      else
        xup(1)=xm
        yup(1)=ym
        xup(2)=xm+xd
        yup(2)=ym
        xup(3)=xup(2)
        yup(3)=ym+yd
        xup(4)=xm
        yup(4)=yup(3)
        xup(5)=xup(1)
        yup(5)=yup(1)
      endif
      idef=i1
      yp=ym+yd
      do i=i1,i2
        j=i-i1+1
        if(i.eq.idef) then
          if(menl(i).gt.0) then
            ipp=White
            icc=DarkBlue
          else
            ipp=Gray
            icc=DarkBlue
          endif
          call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                       -4.)
        else
          if(menl(i).gt.0) then
            ipp=Black
          else
            ipp=WhiteGray
          endif
          icc=ip
        endif
        call FeWrMenuItem(xm,yp,xd,Men(i),MenZ(j:j),ipp,icc)
        yp=yp-MenuLineWidth
      enddo
      if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      call mala(MenZ)
      if(VasekTest.ne.0) then
        idefo=idel(MenZ)
        do 1600i=1,idefo-1
          if(MenZ(i:i).eq.' ') go to 1600
          do j=i+1,idefo
            if(MenZ(i:i).eq.MenZ(j:j)) go to 1610
          enddo
1600    continue
        go to 1900
1610    TextInfo(1)=MenZ
        Ninfo=2
        write(TextInfo(2),'(2i3)') i,j
        call FeInfoOut(-1.,-1.,'Nejednoznacna rychla klavesa:','L')
      endif
1900  call FeFrToChange(LastActiveQuest,0)
      call FeFrToRefresh(-1)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      call FeMouseShape(0)
2000  call FeEvent(0)
      idefo=idef
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(Xpos.ge.xm.and.Xpos.le.xm+xd.and.
     1     Ypos.ge.ym.and.Ypos.le.ym+yd) then
          idef=nint((ym+yd-Ypos)/MenuLineWidth-.5)+i1
        else
          idef=i1-1
        endif
        idef=min(idef,i2)
      else if(EventType.eq.EventKey.and.(EventNumber.eq.JeUp.or.
     1                                   EventNumber.eq.JeDown)) then
        if(idef.ge.i1) then
          if(EventNumber.eq.JeUp) then
            idef=idef-1
            if(idef.lt.i1) idef=i2
          else
            idef=idef+1
            if(idef.gt.i2) idef=i1
          endif
        endif
        call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                     -4.)
      else if(EventType.eq.EventASCII) then
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(MenZ,Znak)
        if(i.ne.0) then
          idef=i+i1-1
          go to 3000
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        if(idef.ge.i1.and.menl(idef).gt.0) go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
        idef=i1-1
        go to 3000
      endif
      if(idefo.ne.idef) then
        if(idefo.ge.i1) then
          if(menl(idefo).gt.0) then
            ipp=Black
            icc=ip
          else
            ipp=WhiteGray
            icc=ip
          endif
          call FeWrMenuItem(xm,ym+yd-float(idefo-i1)*MenuLineWidth,
     1                      xd,Men(idefo),Znak,ipp,icc)
        endif
        if(idef. ge.i1) then
          if(menl(idef).gt.0) then
            ipp=White
            icc=DarkBlue
          else
            ipp=Gray
            icc=DarkBlue
          endif
          call FeWrMenuItem(xm,ym+yd-float(idef -i1)*MenuLineWidth,
     1                     xd,Men(idef),Znak,ipp,icc)
        endif
        if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      endif
      go to 2000
3000  if(idef.gt.0) then
        if(menl(idef).le.0) go to 2000
      endif
      call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp,0)
      FeMenuNew=idef
      TakeMouseMove=.false.
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      if(KartIdOld.ne.0) then
        call FeKartRefresh(1)
        KartId=KartIdOld
      endif
      call FeFrToRefresh(LastActiveQuest)
      return
      end
      subroutine FeButton
      include 'fepc.cmn'
      character*(*) text
      integer BackGroundColor,ForeGroundColor,ColorLU,ColorRD,
     1        UnderLineColor,ColorButton,ColorObtah,OpenState
      logical Plivnout
      go to 9999
      entry FeButtonMake(id,idc,xm,ym,xd,yd,text)
      ButtonXMin(id)=xm
      ButtonXMax(id)=xm+xd
      ButtonYMin(id)=ym
      ButtonYMax(id)=ym+yd
      if(idc.gt.0) then
        ButtonXMin(id)=ButtonXMin(id)+QuestXMin(idc)
        ButtonXMax(id)=ButtonXMax(id)+QuestXMin(idc)
        ButtonYMin(id)=ButtonYMin(id)+QuestYMin(idc)
        ButtonYMax(id)=ButtonYMax(id)+QuestYMin(idc)
      endif
      ButtonState(id)=ButtonClosed
      ButtonText(id)=text
      ButtonAlways(id)=.false.
      go to 9999
      entry FeButtonOpen(id,OpenState)
      Plivnout=.false.
      if(OpenState.eq.ButtonRemoved) then
        go to 5000
      else if(OpenState.eq.ButtonOff) then
        go to 1000
      else if(OpenState.eq.ButtonOn) then
        go to 2000
      else if(OpenState.eq.ButtonDisabled) then
        go to 3000
      else if(OpenState.eq.ButtonClosed) then
        go to 4000
      else
        go to 9999
      endif
      entry FeButtonOff(id)
      Plivnout=.true.
1000  if(ButtonState(id).eq.ButtonOff.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=Black
      ButtonState(id)=ButtonOff
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      PressShift=0.
      UnderLineColor=ForeGroundColor
      go to 6000
      entry FeButtonOn(id)
      Plivnout=.true.
2000  go to 2500
      entry FeButtonOnNW(id)
      Plivnout=.true.
2500  PressShift=1.
      if(ButtonState(id).eq.ButtonOn.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=Black
      ButtonState(id)=ButtonOn
      ColorRD=White
      ColorLU=Gray
      ColorButton=LightGray
      ColorObtah=Gray
      UnderLineColor=ForeGroundColor
      go to 6000
      entry FeButtonDisable(id)
      Plivnout=.false.
3000  if(ButtonState(id).eq.ButtonDisabled.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=WhiteGray
      ButtonState(id)=ButtonDisabled
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      PressShift=0.
      UnderLineColor=-1
      go to 6000
      entry FeButtonClose(id)
      Plivnout=.false.
4000  if(ButtonState(id).eq.ButtonClosed.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      ButtonState(id)=ButtonClosed
      go to 5500
      entry FeButtonRemove(id)
5000  if(ButtonState(id).eq.ButtonRemoved) go to 9999
      ButtonState(id)=ButtonRemoved
      ButtonZ(id:id)=' '
5500  dx=2.
      pom1=anint(ButtonXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(ButtonXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(ButtonYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(ButtonYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      go to 9999
6000  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(ButtonXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(ButtonXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(ButtonYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(ButtonYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
7000  if(ButtonText(id).ne.'#'.and.ButtonText(id).ne.'^'.and.
     1   ButtonText(id).ne.'$') then
        xpom=anint((ButtonXMin(id)+ButtonXMax(id))*.5*EnlargeFactor+
     1             PressShift)/EnlargeFactor
        ypom=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1             PressShift)/EnlargeFactor
        call FeOutStUnder(0,xpom,ypom,
     1                    ButtonText(id)(:idel(ButtonText(id))),'C',
     2                    ForeGroundColor,UnderLineColor,ButtonZ(id:id))
      else
        if(ButtonText(id).eq.'#'.or.ButtonText(id).eq.'^') then
          ButtonZ(id:id)=' '
          xu(1)=anint((ButtonXMin(id)+ButtonXMax(id))*.5*EnlargeFactor+
     1                PressShift-5.)/EnlargeFactor
          xu(2)=anint(xu(1)*EnlargeFactor+5.)/EnlargeFactor
          xu(3)=anint(xu(1)*EnlargeFactor+10.)/EnlargeFactor
          xu(4)=xu(2)
          if(ButtonText(id).eq.'#') then
            zn= 1.
          else
            zn=-1.
          endif
          yu(1)=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1                PressShift+5.*zn)/EnlargeFactor
          yu(2)=anint(yu(1)*EnlargeFactor-3.*zn)/EnlargeFactor
          yu(3)=yu(1)
          yu(4)=anint(yu(1)*EnlargeFactor-10.*zn)/EnlargeFactor
          call FePolygon(xu,yu,4,4,0,0,ForeGroundColor)
        else
          ButtonZ(id:id)=' '
          xu(1)=anint((ButtonXMin(id)+ButtonXMax(id))*EnlargeFactor*.5
     1                +PressShift-5.)/EnlargeFactor
          xu(2)=anint(xu(1)*EnlargeFactor+5.)/EnlargeFactor
          xu(3)=anint(xu(1)*EnlargeFactor+10.)/EnlargeFactor
          zn=-1.
          yu(1)=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1                PressShift+4.*zn)/EnlargeFactor
          yu(2)=anint(yu(1)*EnlargeFactor-8.*zn)/EnlargeFactor
          yu(3)=yu(1)
          call FePolygon(xu,yu,3,4,0,0,ForeGroundColor)
        endif
      endif
      go to 9999
      entry FeButtonActivate(id)
      ColorButton=Black
      go to 8000
      entry FeButtonDeactivate(id)
      ColorButton=LightGray
8000  xu(1)=anint(ButtonXMin(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(1)=anint(ButtonYMin(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(2)=xu(1)
      yu(2)=anint(ButtonYMax(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(ButtonXMax(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(4)=xu(3)
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorButton)
      call FePolyLine(2,xu(3),yu,ColorButton)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorButton)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorButton)
      call FeLineType(NormalLine)
9999  return
      end
      subroutine FeButtonOnAction(ib)
      include 'fepc.cmn'
      integer FeGetSystemTime,EventTypeOld,EventNumberOld,
     1        EventNumberAbsOld
      call FeQuestActiveObjOff
      call FeButtonOn(ib)
      call FeButtonActivate(ib)
      call FeReleaseOutput
      call FeDeferOutput
      StartTime=FeGetSystemTime()
      EventTypeOld=EventType
      EventNumberOld=EventNumber
      EventNumberAbsOld=EventNumberAbs
      EventTypeLost=0
      EventNumberLost=0
1000  call FeEvent(1)
      if(EventType.ne.0) then
        EventTypeLost=EventType
        EventNumberLost=EventNumber
      endif
      if(FeGetSystemTime()-StartTime.lt.200) go to 1000
      ActiveObj=ActButton*10000+ib
      EventType=EventTypeOld
      EventNumber=EventNumberOld
      EventNumberAbs=EventNumberAbsOld
      return
      end
      subroutine FeIcon
      include 'fepc.cmn'
      character*(*) Label,File
      character*80  Veta
      integer Color
      return
      entry FeIconOpen(id,xm,ym,xd,yd,Label,File)
      IconXMin(id)=xm
      IconXMax(id)=xm+xd-1./EnlargeFactor
      IconYMin(id)=ym
      IconYMax(id)=ym+yd-1./EnlargeFactor
      call FeOutStUnder(0,(IconXMin(id)+IconXMax(id))*.5,
     1                     IconYMin(id)-15./EnlargeFactor,
     2                     Label(:idel(Label)),'C',White,White,
     3                     IconZ(id:id))
      IconLabel(id)=Label
      IconFile(id)=File
      entry FeIconOff(id)
      IconState(id)=IconOff
      Veta=IconFile(id)(:idel(IconFile(id)))//'-off'
      go to 2000
      entry FeIconOn(id)
      IconState(id)=IconOn
      Veta=IconFile(id)(:idel(IconFile(id)))//'-on'
      go to 2000
      entry FeIconDisable(id)
      Veta=IconFile(id)(:idel(IconFile(id)))//'-disable'
      IconState(id)=IconDisabled
      go to 2000
      entry FeIconRemove(id)
      Color=Black
      IconState(id)=IconRemoved
      IconZ(id:id)=' '
      call FeFillRectangle(IconXMin(id),IconXMax(id),IconYMin(id),
     1                     IconYMax(id),4,0,0,Color)
      call FeOutStUnder(0,(IconXMin(id)+IconXMax(id))*.5,
     1                     IconYMin(id)-15./EnlargeFactor,
     2                     Label(:idel(Label)),'C',Black,Black,
     3                     IconZ(id:id))
      go to 9999
2000  if(OpSystem.lt.0) then
        call FeLoadImage(IconXMin(id),IconXMax(id),IconYMin(id),
     1                   IconYMax(id),Veta(:idel(Veta))//'.bmp',1)
      else
        call FeLoadImage(IconXMin(id),IconXMax(id),IconYMin(id),
     1                   IconYMax(id),Veta(:idel(Veta))//'.pcx',1)
      endif
9999  return
      end
      subroutine FeOutSt(id,xmi,ymi,Text,Justify,Color)
      include 'fepc.cmn'
      character*(*) text
      character*1 Justify
      character*256 Veta
      character*1 ChTabActual
      integer Color,TTabActual
      Veta=Text
      i=index(Veta,Tabulator)
      if(Justify.eq.'C'.or.Justify.eq.'R'.or.NTabs(UseTabs).le.0.or.
     1   i.le.0) then
        call FeCharOut(id,xmi,ymi,Veta,Justify,Color)
      else
        xm=xmi
        ym=ymi
        if(id.gt.0) then
          if(QuestState(id).ne.0) then
            xm=xm+QuestXMin(id)
            ym=ym+QuestYMin(id)
          endif
        endif
        kk=0
        it=0
        xmp=xm
        TTabActual=0
        ChTabActual=' '
1100    kp=kk+1
        i=index(Veta(kp:),Tabulator)
        if(i.le.0) then
          kk=idel(Veta)
        else
          kk=kp+i-2
        endif
        if(kk.ge.kp) then
          if(TTabActual.eq.IdRightTab) then
            xmk=xmp+FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdLeftTab) then
            xmk=xmp
            xmp=xmp-FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdCharTab) then
            i=index(Veta(kp:kk),ChTabActual)+kp-1
            xmk=xmp+FeTxLength(Veta(i:kk))
            if(i.ge.kp) xmp=xmp-FeTxLength(Veta(kp:i-1))
          else if(TTabActual.eq.IdCenterTab) then
            xmk=xmp+.5*FeTxLength(Veta(kp:kk))
            xmp=xmp-.5*FeTxLength(Veta(kp:kk))
          endif
          call FeCharOut(0,xmp,ym,Veta(kp:kk),Justify,Color)
          if(kk.ge.idel(Veta).or.(kk.eq.idel(Veta)-1.and.
     1       Veta(kk+1:kk+1).eq.Tabulator)) go to 9999
        endif
        it=it+1
        if(it.le.NTabs(UseTabs)) then
          TTabActual=TTabs(it,UseTabs)
          ChTabActual=ChTabs(it,UseTabs)
          xmp=xm+XTabs(it,UseTabs)
          kk=kk+1
          go to 1100
        else
          kp=kk+2
          kk=idel(Veta)
          if(kp.le.kk)
     1      call FeCharOut(0,xmk+5.,ym,Veta(kp:kk),Justify,Color)
        endif
      endif
9999  return
      end
      subroutine FeOutStUnder(id,xmi,ymi,Text,Justify,Color,
     1                        UnderLineColor,StFlag)
      include 'fepc.cmn'
      character*1 Justify
      character*(*) text
      character*256 Text1,Veta
      character*1 StFlag
      integer Color,UnderLineColor
      xm=xmi
      ym=ymi
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xm+QuestXMin(id)
          ym=ym+QuestYMin(id)
        endif
      endif
      StFlag=' '
      Veta=Text
      n=index(Veta,'%')
      idlt=idel(Veta)
      if(n.gt.1) then
        if(Veta(n-1:n-1).eq.ObrLom) then
          Text1=Veta(:n-1)//Veta(n+1:)
          n=0
        endif
      else if(n.le.0.or.n.ge.idlt) then
        Text1=Veta
        n=0
      endif
      if(n.ne.0) then
        StFlag=Veta(n+1:n+1)
        Text1=Veta(:n-1)//Veta(n+1:)
        if(n.eq.1) then
          xu(1)=xm
        else
          xu(1)=xm+FeTxLengthSpace(Text1(:n-1))+1.
        endif
        xu(2)=xm+FeTxLength(Text1(:n))
        yu(1)=ym-6.
        yu(2)=ym-6.
        pom=FeTxLength(Text1)
        if(Justify.ne.'L') then
          if(Justify.eq.'C') pom=pom*.5
          xu(1)=xu(1)-pom
          xu(2)=xu(2)-pom
        endif
        call FePolyline(2,xu,yu,Color)
      else
        StFlag=' '
      endif
      call FeOutSt(id,xm,ym,Text1,Justify,Color)
      call mala(StFlag)
      return
      end
      subroutine FeTextErase(id,xt,yt,Text,Justify,Color)
      include 'fepc.cmn'
      character*(*) Text,Justify
      integer Color
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xt+QuestXMin(id)
          ym=yt+QuestYMin(id)
        endif
      else if(id.eq.0) then
        xm=xt
        ym=yt
      endif
      call FeGetTextRectangle(xm,ym,Text,Justify,1,xp,xk,yp,yk,
     1                        refx,refy,conx,cony)
      call FeFillRectangle(xp,xk,yp,yk,4,0,0,Color)
      return
      end
      subroutine FePureTextErase(id,xt,yt,Text,Justify,Color)
      include 'fepc.cmn'
      character*(*) Text,Justify
      character*256 Veta
      character*1 ChTabActual
      integer Color,TTabActual
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xt+QuestXMin(id)
          ym=yt+QuestYMin(id)
        endif
      else if(id.eq.0) then
        xm=xt
        ym=yt
      endif
      Veta=Text
      i=index(Veta,Tabulator)
      if(Justify.eq.'C'.or.Justify.eq.'R'.or.NTabs(UseTabs).le.0.or.
     1   i.le.0) then
        call FeGetPureTextRectangle(xm,ym,Text,Justify,1,xp,xk,yp,yk,
     1                              refx,refy,conx,cony)
        call FeFillRectangle(xp,xk,yp,yk,4,0,0,Color)
      else
        kk=0
        it=0
        xmp=xm
        TTabActual=0
        ChTabActual=' '
1100    kp=kk+1
        i=index(Veta(kp:),Tabulator)
        if(i.le.0) then
          kk=idel(Veta)
        else
          kk=kp+i-2
        endif
        if(kk.ge.kp) then
          if(TTabActual.eq.IdRightTab) then
            xmk=xmp+FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdLeftTab) then
            xmk=xmp
            xmp=xmp-FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdCharTab) then
            i=index(Veta(kp:kk),ChTabActual)+kp-1
            xmk=xmp+FeTxLength(Veta(i:kk))
            if(i.ge.kp) xmp=xmp-FeTxLength(Veta(kp:i-1))
          else if(TTabActual.eq.IdCenterTab) then
            xmk=xmp+.5*FeTxLength(Veta(kp:kk))
            xmp=xmp-.5*FeTxLength(Veta(kp:kk))
          endif
          call FeGetPureTextRectangle(xmp,ym,Veta(kp:kk),Justify,1,xp,
     1                                xk,yp,yk,refx,refy,conx,cony)
          call FeFillRectangle(xp,xk,yp,yk,4,0,0,Color)
          if(kk.ge.idel(Veta).or.(kk.eq.idel(Veta)-1.and.
     1       Veta(kk+1:kk+1).eq.Tabulator)) go to 9999
        endif
        it=it+1
        if(it.le.NTabs(UseTabs)) then
          TTabActual=TTabs(it,UseTabs)
          ChTabActual=ChTabs(it,UseTabs)
          xmp=xm+XTabs(it,UseTabs)
          kk=kk+1
          go to 1100
        else
          kp=kk+2
          kk=idel(Veta)
          if(kp.le.kk) then
            call FeGetPureTextRectangle(xmk+5.,ym,Veta(kp:kk),Justify,1,
     1                                  xp,xk,yp,yk,refx,refy,conx,cony)
            call FeFillRectangle(xp,xk,yp,yk,4,0,0,Color)
          endif
        endif
      endif
9999  return
      end
      function FeTxLengthUnder(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaPom,VetaT
      VetaT=Veta
      i=index(VetaT,Tabulator)
      if(NTabs(UseTabs).le.0.or.i.le.0) then
        j=index(VetaT,'%')
        if(j.gt.0) then
          if(j.gt.1) then
            if(VetaT(j-1:j-1).ne.ObrLom) then
              if(j+1.le.idel(VetaT)) then
                VetaPom=VetaT(:j-1)//VetaT(j+1:)
                j=idel(VetaPom)+1
              else
                VetaPom=VetaT(:j-1)
              endif
            endif
          else
            VetaPom=VetaT(2:)
            j=idel(VetaPom)+1
          endif
        else
          VetaPom=VetaT
          j=idel(VetaPom)+1
        endif
        FeTxLengthUnder=FeTxLengthSpace(VetaPom(:j-1))
      else
        k=1
        it=0
        xm=0.
1100    i=index(VetaT(k:),Tabulator)
        if(i.le.0) go to 1500
        k=k+i
        it=it+1
        xm=xm+XTabs(it,UseTabs)
        if(it.gt.1) xm=xm-XTabs(it-1,UseTabs)
        go to 1100
1500    if(it.gt.0) then
          if(TTabs(it,UseTabs).eq.IdLeftTab) then
            FeTxLengthUnder=XTabs(it,UseTabs)
            go to 9999
          else if(TTabs(it,UseTabs).eq.IdCharTab) then
            i=index(VetaT(k:),ChTabs(it,UseTabs))
            if(i.gt.1) then
              xmp=xm-FeTxLength(VetaT(k:k+i-2))
            else
              xmp=xm
            endif
          else if(TTabs(it,UseTabs).eq.IdCenterTab) then
            xmp=xm-.5*FeTxLength(VetaT(k:))
          else
            xmp=xm
          endif
          FeTxLengthUnder=xmp+FeTxLength(VetaT(k:))
        else
          FeTxLengthUnder=FeTxLength(VetaT)
        endif
      endif
9999  return
      end
      subroutine FeTabs
      include 'fepc.cmn'
      character*1 ChTabsNew
      integer TTabsNew,Which
      entry FeTabsAdd(XTabsNew,Which,TTabsNew,ChTabsNew)
      do i=1,NTabs(Which)
        if(XTabsNew.lt.XTabs(i,Which)) then
          n=i
          go to 1100
        endif
      enddo
      n=NTabs(Which)+1
1100  NTabs(Which)=min(NTabs(Which)+1,MxTabs)
      do i=n+1,NTabs(Which)
        XTabs(i,Which)=XTabs(i-1,Which)
        TTabs(i,Which)=TTabs(i-1,Which)
        ChTabs(i,Which)=ChTabs(i-1,Which)
      enddo
      XTabs(n,Which)=XTabsNew
      TTabs(n,Which)=TTabsNew
      ChTabs(n,Which)=ChTabsNew
      go to 9999
      entry FeTabsReset(Which)
      if(Which.lt.0) then
        IFrom=0
        ITo=20
      else
        IFrom=Which
        ITo=Which
      endif
      do i=IFrom,ITo
        NTabs(i)=0
        call SetRealArrayTo(XTabs(1,i),MxTabs,0.)
        call SetIntArrayTo(TTabs(1,i),MxTabs,0)
        call SetStringArrayTo(ChTabs(1,i),MxTabs,' ')
      enddo
9999  return
      end
      function NextTabs()
      include 'fepc.cmn'
      do i=0,MxTabs
        if(NTabs(i).le.0) then
          NextTabs=i
          go to 9999
        endif
      enddo
      call FeUnforeseenError('No space for a new tabulator array.')
      NextTabs=-1
9999  return
      end
      subroutine FeWrMenuItem(x,y,xd,Veta,StFlag,TextColor,
     1                        BackGroundColor)
      include 'fepc.cmn'
      character*(*) Veta
      character*1 StFlag
      integer TextColor,BackGroundColor
      klic=0
      go to 1000
      entry FeWrLine(x,y,xd,Veta,TextColor,BackGroundColor)
      klic=1
1000  if(xd.gt.0.) then
        call FeFillRectangle(x,x+xd,y,y-MenuLineWidth,4,0,0,
     1                       BackGroundcolor)
        xpom=x+EdwMarginSize
        ypom=y-MenuLineWidth*.5
      else
        xpom=x
        ypom=y
      endif
      if(klic.eq.0) then
        call FeOutStUnder(0,xpom,ypom,Veta,'L',TextColor,TextColor,
     1                    StFlag)
      else
        call FeOutSt(0,xpom,ypom,Veta,'L',TextColor)
      endif
      return
      end
      subroutine FeEdwCtrlVAction(id)
      include 'fepc.cmn'
      character*256 Veta
      call FeGetClipboardText(Veta,n)
      KurzorOld=Kurzor
      if(EdwSelFr.ne.EdwSelTo) then
        n1=min(EdwSelFr,EdwSelTo)+1
        n2=max(EdwSelFr,EdwSelTo)
      else
        n1=Kurzor+1
        n2=n1-1
      endif
      if(n1.gt.1) then
        Veta=EdwString(id)(:n1-1)//Veta(:n)
        idl=n1+n-1
      else
        idl=n
      endif
      if(n2.lt.idel(EdwString(id))) then
        EdwString(id)=Veta(:idl)//EdwString(id)(n2+1:)
      else
        EdwString(id)=Veta(:idl)
      endif
      if(EdwSelFr.ne.EdwSelTo) call FeEdwSelClear(id)
      call FeEdwOpen(id)
      Kurzor=KurzorOld
      if(Kurzor.gt.EdwTextMax(id)) then
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      else
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2    EdwTextMax(id))
      endif
      call FeEdwBasic(id,White)
      return
      end
      subroutine FeEdwCtrlCAction(id)
      include 'fepc.cmn'
      character*256 Veta
      Klic=0
      go to 1000
      entry FeEdwCtrlXAction(id)
      Klic=1
1000  n1=min(EdwSelFr,EdwSelTo)
      n2=max(EdwSelFr,EdwSelTo)
      if(n1.ge.n2) go to 9999
      call FeSetClipboardText(EdwString(id)(n1+1:n2))
      if(Klic.eq.0) go to 9999
      if(n1.gt.1) then
        Veta=EdwString(id)(:n1)
        idl=n1
      else
        Veta=' '
        idl=0
      endif
      call FeEdwSelClear(id)
      if(n2.lt.idel(EdwString(id))) then
        if(idl.gt.0) then
          EdwString(id)=Veta(:idl)//EdwString(id)(n2+1:)
        else
          EdwString(id)=EdwString(id)(n2+1:)
        endif
      else
        EdwString(id)=Veta
      endif
      EdwTextLen(id)=idel(EdwString(id))
      EdwKurzor(id)=min(EdwKurzor(id),EdwTextLen(id))
      if(Kurzor.gt.EdwTextMax(id)) then
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      else
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2    EdwTextMax(id))
      endif
9999  return
      end
      subroutine FeZmenKurzor(id)
      include 'fepc.cmn'
      character*256 Veta
      if(KurzorEdw.le.0.or.id.le.0) go to 9999
      xp=EdwTextXmin(KurzorEdw)+1.
      if(EdwTextMin(id).ge.1.and.Kurzor.ge.0) then
        Veta=EdwString(id)
        if(Kurzor.eq.EdwTextMax(id)) then
          xp=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor))
          xk=xp+anint(.5*PropFontWidthInPixels)/EnlargeFactor
        else if(Kurzor.eq.EdwTextMin(id)-1) then
          xk=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor+1))
        else
          xk=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor+1))
          xp=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor))
        endif
      endif
      yp=(EdwYmin(KurzorEdw)+EdwYmax(KurzorEdw))*.5
      if(InsertMode) then
        KurzorX(1)=xp
        KurzorX(2)=xp
        KurzorY(1)=yp-.5*PropFontHeightInPixels/EnlargeFactor
        KurzorY(2)=yp+.5*PropFontHeightInPixels/EnlargeFactor
        KurzorStyl=4
      else
        KurzorX(1)=xp
        KurzorX(2)=xk
        KurzorY(1)=yp-.5*PropFontHeightInPixels/EnlargeFactor
        KurzorY(2)=KurzorY(1)
        KurzorStyl=4
      endif
      call FePlotMode('E')
      call FePolyline(2,KurzorX,KurzorY,KurzorColor)
      if(InsertMode) then
        KurzorX(1)=xp-1./EnlargeFactor
        KurzorX(2)=xp-1./EnlargeFactor
      else
        KurzorY(1)=KurzorY(1)+1./EnlargeFactor
        KurzorY(2)=KurzorY(1)
      endif
      call FePolyline(2,KurzorX,KurzorY,KurzorColor)
      call FePlotMode('N')
9999  return
      end
      subroutine FeKart
      include 'fepc.cmn'
      integer ExtESC,ExtOK,ExtESCIn,ExtOKIn
      logical stejnaW,WizardModeOld
      character*80 ch80
      character*(*) Text
      entry FeKartCreate(xmi,ymi,xdpom,n1,Text,ExtESCIn,ExtOKIn)
      call FeDeferOutput
      ExtOK=ExtOKIn
      ExtESC=ExtESCIn
      KartSidePruh=16.
      KartLabelPruh=30.
      KartTabsPruh=24.
      KartLowerPruh=45.+ButYd
      xdp=xdpom
      KartId0=NextQuestId()
      if(WizardMode) then
        xm=QuestXMin(KartId0)
        ym=QuestYMin(KartId0)
        xdp=QuestXLen(KartId0)
        yd=QuestYLen(KartId0)
        KartLines=nint((yd-KartLabelPruh-KartTabsPruh-KartLowerPruh)/
     1            QuestLineWidth)
        KartLowerPruh=yd-KartLabelPruh-KartTabsPruh-
     1                QuestLineWidth*float(KartLines)
        call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                       WhiteGray)
        if(ButtonOk.gt.0) then
          if(ButtonState(ButtonOk).ne.ButtonClosed) then
            ButtonState(ButtonOk)=ButtonOn
            call FeButtonOff(ButtonOk)
          endif
        endif
        if(ButtonEsc.gt.0) then
          if(ButtonState(ButtonEsc).ne.ButtonClosed) then
            ButtonState(ButtonEsc)=ButtonOn
            call FeButtonOff(ButtonEsc)
          endif
        endif
        if(ButtonCancel.gt.0) then
          if(ButtonState(ButtonCancel).ne.ButtonClosed) then
            ButtonState(ButtonCancel)=ButtonOn
            call FeButtonOff(ButtonCancel)
          endif
        endif
        KartCancel=ButtonCancel
      else
        KartLines=n1
        yd=KartLabelPruh+KartTabsPruh+
     1     QuestLineWidth*float(KartLines)+KartLowerPruh
        call FeKartQuestAbsCreate(KartId0,xmi,ymi,xdp,yd,' ',0,
     1                            WhiteGray,ExtESC,ExtOK)
        KartCancel=0
      endif
      KartXMin=QuestXMin(KartId0)
      KartXMax=QuestXMax(KartId0)
      KartYMin=QuestYMin(KartId0)
      KartYMax=QuestYMax(KartId0)
      call FeQuestAbsLblMake(KartId0,(KartXMax-KartXMin)*.5,
     1                       KartYMax-KartYMin-.5*KartLabelPruh,
     2                       Text,'C','B')
      WizardModeOld=WizardMode
      WizardMode=.false.
      KartFirstId=NextQuestId()
      WizardMode=WizardModeOld
      if(KartFirstId.eq.0) then
        call FeMsgOut(-1.,-1.,
     1                'FeCreateKart: Neni volne id pro prvni listek')
        call FeSmytec
      endif
      KartLastId=KartFirstId-1
      IKart=0
      if(ExtOK.ge.0) then
        KartOK=ButtonOK
      else
        KartOK=-1
      endif
      KartOn=.true.
      KartESC=ButtonEsc
      KartDolButt(1)=KartESC
      KartDolButt(2)=KartOK
      KartNDol=2
      if(WizardMode) then
        KartNDol=KartNDol+1
        KartDolButt(KartNDol)=KartCancel
      endif
      go to 9999
      entry FeCreateListek(Text,icheck)
      if(KartOn) then
        KartLastId=KartLastId+1
        id=KartLastId
        if(id.gt.mxquest) then
          call FeMsgOut(-1.,-1.,'FeCreateListek: presazeno mxquest')
          call FeSmytec
        endif
        if(id.gt.KartFirstId) then
          if(EdwTo.ge.EdwFr) call FeZmenKurzor(EdwActive)
          call FeSaveImage(QuestXMin(id-1),QuestXMax(id-1),
     1                     QuestYMin(id-1),QuestYMax(id-1),
     2                     QuestTmpFile(id-1))
        endif
        NKart=NKart+1
        if(NKart.gt.mxKart) then
          call FeMsgOut(-1.,-1.,'FeCreateListek: Presazeno mxKart')
          call FeSmytec
        endif
        KartListekSaved(NKart)=.false.
        KartUpdateListek(NKart)=0
        KartSwText(NKart)=Text
        sw=0.
        call FeBoldFont
        do i=1,NKart
          KartSwWidth(i)=FeTxLength(KartSwText(i))+10.
          sw=sw+KartSwWidth(i)
        enddo
        call FeNormalFont
        scw=(KartXMax-KartXMin-2.*KartSidePruh)/sw
        do i=1,NKart
          KartSwWidth(i)=KartSwWidth(i)*scw
        enddo
      endif
      WizardModeOld=WizardMode
      WizardMode=.false.
      call FeKartQuestCreate(id,0.,0.,0.,0,' ',icheck,0,KartESC,
     1                       KartOK,KartCancel)
      WizardMode=WizardModeOld
      go to 9999
      entry FeCompleteKart(nactive)
      Id=NKart+KartFirstId-1
      KartIdOld=id
      call FeSaveImage(QuestXMin(Id),QuestXMax(Id),
     1                 QuestYMin(Id),QuestYMax(Id),
     2                 QuestTmpFile(Id))
      sw=KartXMax-KartXMin-2.*KartSidePruh+1.
      swmx=0.
      swmn=sw
      do i=1,NKart
        pom1=KartSwWidth(i)
        sw=sw-pom1
        if(pom1.gt.swmx) then
          mx=i
          swmx=pom1
        endif
        if(pom1.lt.swmn) then
          mn=i
          swmn=pom1
        endif
      enddo
      i=nint(sw)
      if(i.gt.0) then
        j=mn
      else if(i.le.0) then
        j=mx
      endif
      KartSwWidth(j)=KartSwWidth(j)+float(i)
      xm=QuestXMin(KartFirstId)
      pom1=anint(QuestYMax(Id)*EnlargeFactor+3.)/EnlargeFactor
      pom2=anint((pom1+KartTabsPruh)*EnlargeFactor)/EnlargeFactor
      do i=1,NKart
        KartSwYMin(i)=pom1
        KartSwYMax(i)=pom2
        KartSwXmin(i)=xm
        xm=xm+KartSwWidth(i)
        KartSwXMax(i)=xm-1.
      enddo
      space=20.
      xd=10.
      nobu=0
      stejnaW=.true.
      sumaw=0.
      if(WizardMode) then
        ifr=4
      else
        ifr=3
      endif
      do i=ifr,KartNDol
        n=KartDolButt(i)
        if(n.le.0.or.n.gt.mxbut) cycle
        pom1=FeTxLength(ButtonText(n)(:idel(ButtonText(n)))//'x')
        xd=max(xd,pom1)
        sumaw=sumaw+pom1
        nobu=nobu+1
      enddo
1120  xm=KartXMin+.5*(KartXMax-KartXMin-float(nobu)*xd-
     1   float(nobu-1)*space)
      if(xm.le.KartXMin) then
        if(space.eq.20.) then
          space=10.
          go to 1120
        endif
        stejnaW=.false.
        space=(KartXMax-KartXMin-sumaw)/float(nobu+1)
        if(space.le.0.) space = 2.
        addw=0.
        if(space.gt.10.) then
          space=10.
          addw=(KartXMax-KartXMin-float(nobu+1)*space-sumaw)/
     1         float(nobu)
        endif
        xm=KartXMin+space
      endif
      ym=KartYMin+0.5*(KartLowerPruh+ButYd)
      do i=ifr,KartNDol
        n=KartDolButt(i)
        if(n.le.0.or.n.gt.mxbut) cycle
        ch80=ButtonText(n)
        if(.not.stejnaW) xd=FeTxLength(ch80(1:idel(ch80))//'x')+addw
        call FeButtonMake(n,0,xm,ym,xd,ButYd,ch80)
        call FeButtonOpen(n,ButtonOff)
        xm=xm+xd+space
      enddo
      go to 5000
      entry FeMalujKart(NActive)
      KartIdOld=KartId
5000  call FeFillRectangle(KartSwXMin(1)-2.,
     1                     KartSwXMax(NKart)+2.,
     2                     KartSwYMin(1)-2.,
     3                     KartSwYMax(1)+2.,4,0,0,WhiteGray)
      call FeFillRectangle(QuestXMin(KartFirstId),
     1                     QuestXMax(KartFirstId),
     2                     QuestYMin(KartFirstId),
     3                     QuestYMax(KartFirstId),
     4                     4,0,0,LightGray)
      call FeDrawFrame(QuestXMin(KartFirstId),QuestYMin(KartFirstId),
     1                 QuestXMax(KartFirstId)-QuestXMin(KartFirstId),
     2                 QuestYMax(KartFirstId)-QuestYMin(KartFirstId),
     3                 2.,Gray,White,Black,.false.)
      do i=1,NKart
        if(i.eq.NActive) cycle
        call FeDrawSwitch(KartSwXMin(i),KartSwXMax(i),KartSwYMin(i),
     1                    KartSwYMax(i),2)
        call FeOutSt(0,KartSwXmin(i)+.5*(KartSwXMax(i)-KartSwXmin(i)),
     1               KartSwYMin(i)+.5*(KartSwYMax(i)-KartSwYMin(i)),
     2               KartSwText(i),'C',Black)
      enddo
      pom1=2.
      call FeDrawSwitch(KartSwXMin(NActive)-pom1,
     1                  KartSwXMax(NActive)+pom1,
     2                  KartSwYMin(NActive)-pom1,
     3                  KartSwYMax(NActive)+pom1,2)
      call FeBoldFont
      call FeOutSt(0,KartSwXmin(NActive)
     1               +.5*(KartSwXMax(NActive)-KartSwXmin(NActive)),
     2               KartSwYMin(NActive)
     3               +.5*(KartSwYMax(NActive)-KartSwYMin(NActive))+1.,
     4               KartSwText(NActive),'C',Black)
      call FeNormalFont
      IKart=nactive
      LastActiveQuest=IKart+KartFirstId-1
      KartId=LastActiveQuest
      call FeLoadImage(QuestXMin(KartId),QuestXMax(KartId),
     1                 QuestYMin(KartId),QuestYMax(KartId),
     2                 QuestTmpFile(KartId),0)
      call FeSaveImage(KartXMin,KartXMax,KartYMin,KartYMax,
     1                 QuestTmpFile(KartId))
      KartListekSaved(nactive)=.true.
      WizardModeOld=WizardMode
      call FeFrToChange(KartIdOld,KartId)
      WizardMode=WizardModeOld
      call FeQuestActiveUpdate
      call FeQuestActiveObjOn
      go to 9999
      entry FePrepniListek(nactive)
      if(NActive.eq.IKart) go to 9999
      call FeEdwSelClear(KartId)
      if(QuestState(KartId).gt.0)
     1   call FeSaveImage(KartXMin,KartXMax,KartYMin,KartYMax,
     2                    QuestTmpFile(KartId))
      KartIdOld=KartId
      if(KartListekSaved(NActive)) then
        IKart=NActive
        LastActiveQuest=IKart+KartFirstId-1
        KartId=LastActiveQuest
        call FeLoadImage(KartXMin,KartXMax,KartYMin,KartYMax,
     1                   QuestTmpFile(KartId),-1)
        call FeFrToChange(KartIdOld,KartId)
        call FeQuestActiveUpdate
        call FeQuestActiveObjOn
      else
        go to 5000
      endif
9999  return
      end
      subroutine FeDestroyKart
      include 'fepc.cmn'
      if(WizardMode) then
        ifin=1
      else
        ifin=0
      endif
      do i=NKart,ifin,-1
        if(i.ne.0) then
          call FePrepniListek(i)
          KartUpdateListek(i)=0
        endif
        if(i.eq.ifin) KartOn=.false.
        call FeQuestRemove(KartFirstId+i-1)
      enddo
      NKart=0
      KartNDol=0
      KartId=0
      return
      end
      logical function FeYesNo(xmi,ymi,text,idf)
      include 'fepc.cmn'
      character*(*) text
      logical FeYeNo
      FeYesNo=FeYeNo(xmi,ymi,text,idf,0)
      end
      logical function FeYesNoLong(xmi,ymi,text,idf)
      include 'fepc.cmn'
      character*(*) text
      logical FeYeNo
      FeYesNoLong=FeYeNo(xmi,ymi,text,idf,NInfo)
      end
      logical function FeYesNoHeader(xmi,ymi,text,idf)
      include 'fepc.cmn'
      character*(*) text
      logical FeYeNo
      FeYesNoHeader=FeYeNo(xmi,ymi,text,idf,-NInfo)
      end
      logical function FeYeNo(xmi,ymi,text,idf,n)
      include 'fepc.cmn'
      character*(*) text
      character*4 AnoYes
      logical WizardModeIn,EqIgCase,PropFontIn
      integer QuestLineWidthIn
      PropFontIn=PropFont
      if(.not.PropFont) call FeSetPropFont
      WizardModeIn=WizardMode
      WizardMode=.false.
      AllowChangeMouse=.false.
      QuestLineWidthIn=QuestLineWidth
      QuestLineWidth=14.
      xd=max(150.,FeTxLength(Text)+10.)
      do i=1,iabs(n)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
        xd=max(FeTxLength(TextInfo(i))+10.,xd)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
      enddo
      if(WaitTime.gt.0) xd=max(FeTxLength(ItRemains)+10.,xd)
      id=NextQuestid()
      il=3
      if(iabs(n).gt.0) il=il+iabs(n)+1
      call FeQuestCreate(id,xmi,ymi,xd,il,' ',il,LightGray,-1,-1)
      CheckKeyboard=.true.
      il=0
      if(n.lt.0) then
        do i=1,iabs(n)
          il=il+1
          if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        TextInfoFlags(i)(1:1),
     2                        TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
        il=il+1
        call FeQuestLinkaMake(id,il)
      endif
      il=il+1
      call FeQuestLblMake(id,xd*.5,il,text,'C','N')
      if(n.gt.0) then
        do i=1,n
          il=il+1
          if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        TextInfoFlags(i)(1:1),
     2                        TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
      endif
      il=il+1
      dpom=40.
      i=-10*il-7
      if(Language.eq.0) then
        AnoYes='Yes'
      else
        AnoYes='Ano'
      endif
      call FeQuestButtonMake(id,xd*.5-dpom-10.,i,dpom,ButYd,AnoYes)
      nButtYes=ButtonLastMade
      if(Language.eq.0) then
        AnoYes='No'
      else
        AnoYes='Ne'
      endif
      call FeQuestButtonMake(id,xd*.5+10.,i,dpom,ButYd,AnoYes)
      nButtNo=ButtonLastMade
      if(idf.eq.1) then
        FeYeNo=.true.
      else
        FeYeNo=.false.
      endif
      call FeQuestButtonOpen(nButtYes,ButtonOff)
      call FeQuestButtonOpen(nButtNo ,ButtonOff)
      nButOld=0
2000  if(FeYeNo) then
        nButNew=nButtYes
      else
        nButNew=nButtNo
      endif
      BlockedActiveObj=.false.
      call FeQuestActiveObjOff
      ActiveObj=10000*ActButton+nButNew+ButtonFr-1
      call FeQuestActiveObjOn
      if(nButNew.ne.nButOld) then
        i=nButNew
        call FeQuestMouseToButton(nButNew)
        nButOld=nButNew
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventKey.and.CheckNumber.eq.JeReturn) then
        FeYeNo=.true.
      else if(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
        FeYeNo=.false.
      else if(CheckType.eq.EventSystem.and.CheckNumber.eq.JeTimeOut)
     1  then
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtYes.or.CheckNumber.eq.nButtNo)) then
        FeYeNo=CheckNumber.eq.nButtYes
      else if(CheckType.eq.EventKey) then
        FeYeNo=.not.FeYeNo
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      QuestLineWidth=QuestLineWidthIn
      if(.not.PropFontIn) call FeSetFixFont
      return
      end
      subroutine FeYesNoAll(xmi,ymi,text,idf,iout)
      include 'fepc.cmn'
      character*12  t12(4)
      character*(*) text
      logical WizardModeIn
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='Yes to %all'
      t12(4)='No to a%ll'
      n=4
      go to 1000
      entry FeYesNoStart(xmi,ymi,text,idf,iout)
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='Yes+%start'
      t12(4)='No+s%tart'
      n=4
      go to 1000
      entry FeYesNoCancel(xmi,ymi,text,idf,iout)
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='%Cancel'
      n=3
1000  WizardModeIn=WizardMode
      WizardMode=.false.
      gap=20.
      dbutt=0.
      do i=1,n
        dbutt=dbutt+FeTxLength(t12(i))+20.
      enddo
      dbutt=dbutt+3.*gap
      call FeBoldFont
      xd=max(dbutt+50.,FeTxLength(Text)+25.)
      call FeNormalFont
      id=NextQuestid()
      call FeQuestCreate(id,xmi,ymi,xd,1,Text,0,LightGray,-1,-1)
      CheckKeyboard=.true.
      pomx=(xd-dbutt)*.5
      do i=1,n
        w=FeTxLength(t12(i))+20.
        call FeQuestButtonMake(id,pomx,1,w,ButYd,t12(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtFirst=ButtonLastMade
        pomx=pomx+w+gap
      enddo
      iout=idf
2000  BlockedActiveObj=.false.
      call FeQuestActiveObjOff
      ActiveObj=10000*ActButton+iout+ButtonFr-1
      call FeQuestActiveObjOn
      call FeQuestMouseToButton(iout-nButtFirst+1)
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventKey.and.CheckNumber.eq.JeReturn) then
        go to 3000
      else if(CheckType.eq.EventButton) then
        iout=CheckNumber-nButtFirst+1
        go to 3000
      else if(CheckType.eq.EventKey) then
        if(CheckNumber.eq.JeRight) then
          iout=mod(iout,4)+1
        else if(CheckNumber.eq.JeLeft) then
          iout=mod(iout+2,4)+1
        endif
      endif
      go to 2000
3000  call FeWait(.2)
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      return
      end
      integer function FeSelectOnePossibility(xmi,ymi,NHeader,idflt)
      include 'fepc.cmn'
      logical CrwLogicQuest,EqIgCase
      FeSelectOnePossibility=idflt
      xd=0.
      if(WizardMode) then
        yd=QuestLineWidth*float(WizardLines)+40.
        if(WizardTitle) yd=yd+QuestLineWidth
      else
        xd=0.
        do i=1,NHeader
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
          xd=max(FeTxLength(TextInfo(i))+10.,xd)
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
        enddo
        do i=NHeader+1,NInfo
          xd=max(FeTxLength(TextInfo(i))+CrwgXd+30.,xd)
        enddo
        yd=20.+float(NInfo+1)*20.
      endif
      id=NextQuestid()
      if(.not.WizardMode)
     1  call FeQuestAbsCreate(id,xmi,ymi,xd,yd,' ',0,LightGray,-1,0)
      ypom=yd-10.
      do i=1,NHeader
        if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
          xpom=xd*.5
        else
          xpom=5.
        endif
        call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(i),
     1                         TextInfoFlags(i)(1:1),
     2                         TextInfoFlags(i)(2:2))
        TextInfoFlags(i)='LN'
        ypom=ypom-20.
      enddo
      if(NHeader.gt.0) then
        ypom=ypom+10.
        call FeQuestAbsLinkaMake(id,ypom)
        ypom=ypom-10.
      endif
      ypom=ypom-10.
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=NHeader+1,NInfo
        call FeQuestAbsCrwMake(id,tpom,ypom+5.,xpom,ypom,TextInfo(i),
     1                         'L',CrwgXd,CrwgYd,0,1)
        call FeQuestCrwOpen(CrwLastMade,i-NHeader.eq.idflt)
        ypom=ypom-20.
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NInfo-NHeader
          if(CrwLogicQuest(i)) then
            FeSelectOnePossibility=i
            go to 3000
          endif
        enddo
      else if(ich.gt.0) then
        FeSelectOnePossibility= 0
      else
        FeSelectOnePossibility=-1
      endif
3000  if(.not.WizardMode) call FeQuestRemove(id)
      return
      end
      subroutine FeSelectFromOffer(xmi,ymi,NHeader,Selected,ich)
      include 'fepc.cmn'
      logical CrwLogicQuest,EqIgCase,Selected(*)
      xd=0.
      if(WizardMode) then
        yd=QuestLineWidth*float(WizardLines)+40.
        if(WizardTitle) yd=yd+QuestLineWidth
      else
        xd=0.
        do i=1,NHeader
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
          xd=max(FeTxLength(TextInfo(i))+10.,xd)
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
        enddo
        do i=NHeader+1,NInfo
          xd=max(FeTxLength(TextInfo(i))+CrwgXd+30.,xd)
        enddo
        yd=20.+float(NInfo+1)*20.
      endif
      id=NextQuestid()
      if(.not.WizardMode)
     1  call FeQuestAbsCreate(id,xmi,ymi,xd,yd,' ',0,LightGray,-1,0)
      ypom=yd-10.
      do i=1,NHeader
        if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
          xpom=xd*.5
        else
          xpom=5.
        endif
        call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(i),
     1                         TextInfoFlags(i)(1:1),
     2                         TextInfoFlags(i)(2:2))
        TextInfoFlags(i)='LN'
        ypom=ypom-20.
      enddo
      if(NHeader.gt.0) then
        ypom=ypom+10.
        call FeQuestAbsLinkaMake(id,ypom)
        ypom=ypom-10.
      endif
      ypom=ypom-10.
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=NHeader+1,NInfo
        call FeQuestAbsCrwMake(id,tpom,ypom+5.,xpom,ypom,TextInfo(i),
     1                         'L',CrwgXd,CrwgYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,.true.)
        ypom=ypom-20.
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NInfo-NHeader
          Selected(i)=CrwLogicQuest(i)
        enddo
      endif
3000  if(.not.WizardMode) call FeQuestRemove(id)
      return
      end
      integer function FeDoSomethingOrCancel(Text,Label,n)
      include 'fepc.cmn'
      character*(*) Text,Label(n)
      character*7   Cancel
      data Cancel/'%Cancel'/
      id=NextQuestId()
      dpom=FeTxLengthUnder(Cancel)+10.
      do i=1,n
        dpom=max(dpom,FeTxLengthUnder(Label(n))+10.)
      enddo
      xd=dpom*float(n+1)+5.*float(n)
      xqd=max(xd,FeTxLength(Text))+10.
      xqd=min(xqd,260.)
      il=0
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,-1,-1)
      xpom=(xqd-xd)*.5
      il=1
      do i=1,n
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Label(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtFirst=ButtonLastMade
        xpom=xpom+dpom+5.
      enddo
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Cancel)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtCancel=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtCancel) then
          FeDoSomethingOrCancel=0
        else
          FeDoSomethingOrCancel=CheckNumber-nButtFirst+1
        endif
        call FeQuestRemove(id)
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      return
      end
      subroutine FeHeaderInfo(Text)
      include 'fepc.cmn'
      character*(*) Text
      character*80 TextOld
      data TextOld/' '/
      call FeBoldFont
      call FeTextErase(0,XCenGrWin,(YMaxGrWin+YMaxBasWin)*.5,
     1                 TextOld,'C',LightGray)
      call FeOutSt(0,XCenGrWin,(YMaxGrWin+YMaxBasWin)*.5,Text,'C',
     1             Black)
      TextOld=Text
      call FeNormalFont
      return
      end
      subroutine FeMakeGrWin(XGrLeft,XGrRight,YGrDown,YGrUp)
      include 'fepc.cmn'
      if(Console) go to 9999
      if(BatchMode) then
        call FeFillRectangle(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,4,0,0,White)
        XMinGrWin=XMinBasWin
        XMaxGrWin=XMaxBasWin
        YMinGrWin=YMinBasWin
        YMaxGrWin=YMaxBasWin
      else
        call FeFillRectangle(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,4,0,0,LightGray)
        XMinGrWin=XMinBasWin+XGrLeft
        XMaxGrWin=XMaxBasWin-XGrRight
        YMinGrWin=YMinBasWin+YGrDown
        YMaxGrWin=YMaxBasWin-YGrUp
        call FeFillRectangle(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       4,0,0,Black)
      endif
      XCenGrWin=(XMinGrWin+XMaxGrWin)*.5
      YCenGrWin=(YMinGrWin+YMaxGrWin)*.5
      XLenGrWin=XMaxGrWin-XMinGrWin
      YLenGrWin=YMaxGrWin-YMinGrWin
      call FeBottomInfo(' ')
9999  return
      end
      subroutine FeMakeAcWin(XAcLeft,XAcRight,YAcDown,YAcUp)
      include 'fepc.cmn'
      XMinAcWin=XMinGrWin+XAcLeft
      XMaxAcWin=XMaxGrWin-XAcRight
      YMinAcWin=YMinGrWin+YAcDown
      YMaxAcWin=YMaxGrWin-YAcUp
      XCenAcWin=(XMinAcWin+XMaxAcWin)*.5
      YCenAcWin=(YMinAcWin+YMaxAcWin)*.5
      XLenAcWin=XMaxAcWin-XMinAcWin
      YLenAcWin=YMaxAcWin-YMinAcWin
      return
      end
      subroutine FeSetTransXo2X(XfMin,XfMax,YfMin,YfMax,Equal)
      include 'fepc.cmn'
      dimension xf(3)
      logical Equal
      xf(3)=0.
      xf(1)=XfMin
      xf(2)=YfMin
      call multm(F2O,xf,XoCornAcWin(1,1),3,3,1)
      xf(1)=XfMin
      xf(2)=YfMax
      call multm(F2O,xf,XoCornAcWin(1,2),3,3,1)
      xf(1)=XfMax
      xf(2)=YfMax
      call multm(F2O,xf,XoCornAcWin(1,3),3,3,1)
      xf(1)=XfMax
      xf(2)=YfMin
      call multm(F2O,xf,XoCornAcWin(1,4),3,3,1)
      XoMinAcWin=XoCornAcWin(1,1)
      XoMaxAcWin=XoCornAcWin(1,1)
      YoMinAcWin=XoCornAcWin(2,1)
      YoMaxAcWin=XoCornAcWin(2,1)
      do i=2,4
        XoMinAcWin=min(XoMinAcWin,XoCornAcWin(1,i))
        XoMaxAcWin=max(XoMaxAcWin,XoCornAcWin(1,i))
        YoMinAcWin=min(YoMinAcWin,XoCornAcWin(2,i))
        YoMaxAcWin=max(YoMaxAcWin,XoCornAcWin(2,i))
      enddo
      XoCenAcWin=(XoMinAcWin+XoMaxAcWin)*.5
      YoCenAcWin=(YoMinAcWin+YoMaxAcWin)*.5
      XoLenAcWin=XoMaxAcWin-XoMinAcWin
      YoLenAcWin=YoMaxAcWin-YoMinAcWin
      X2XoRatio=XLenAcWin/XoLenAcWin
      Y2YoRatio=YLenAcWin/YoLenAcWin
      if(Equal) then
        X2XoRatio=min(X2XoRatio,Y2YoRatio)
        Y2YoRatio=X2XoRatio
      endif
      XOrgAcWin=XCenAcWin-.5*XoLenAcWin*X2XoRatio
      YOrgAcWin=YCenAcWin-.5*YoLenAcWin*Y2YoRatio
      do j=1,3
        do i=1,4
          if(j.eq.1) then
            XCornAcWin(j,i)=FeXo2X(XoCornAcWin(j,i))
          else if(j.eq.2) then
            XCornAcWin(j,i)=FeYo2Y(XoCornAcWin(j,i))
          else
            XCornAcWin(j,i)=0.
          endif
        enddo
      enddo
      return
      end
      subroutine FeXf2X(xf,x)
      include 'fepc.cmn'
      dimension xf(3),x(3),xo(3)
      call multm(F2O,xf,xo,3,3,1)
      x(1)=FeXo2X(xo(1))
      x(2)=FeYo2Y(xo(2))
      x(3)=xo(3)
      return
      end
      subroutine FeX2Xf(x,xf)
      include 'fepc.cmn'
      dimension xf(3),x(3),xo(3)
      xo(1)=FeX2Xo(x(1))
      xo(2)=FeY2Yo(x(2))
      xo(3)=x(3)
      call multm(O2F,xo,xf,3,3,1)
      return
      end
      function FeXo2X(Xo)
      include 'fepc.cmn'
      FeXo2X=(Xo-XoMinAcWin)*X2XoRatio+XOrgAcWin
      return
      end
      function FeX2Xo(X)
      include 'fepc.cmn'
      FeX2Xo=(X-XOrgAcWin)/X2XoRatio+XoMinAcWin
      return
      end
      function FeYo2Y(Yo)
      include 'fepc.cmn'
      FeYo2Y=(Yo-YoMinAcWin)*Y2YoRatio+YOrgAcWin
      return
      end
      function FeY2Yo(Y)
      include 'fepc.cmn'
      FeY2Yo=(Y-YOrgAcWin)/Y2YoRatio+YoMinAcWin
      return
      end
      subroutine FeXf2Pixels(xp,ix,iy)
      include 'fepc.cmn'
      dimension xp(3),xo(3)
      call FeXf2X(xp,xo)
      ix=nint(xo(1)*EnlargeFactor)
      iy=nint(xo(2)*EnlargeFactor)
      return
      end
      subroutine FePixels2Xf(ix,iy,xp)
      include 'fepc.cmn'
      dimension xp(3),xo(3)
      xo(1)=nint(float(ix)/EnlargeFactor)
      xo(2)=nint(float(iy)/EnlargeFactor)
      xo(3)=0.
      call FeX2Xf(xo,xp)
      return
      end
      subroutine FeMakeAcFrame
      include 'fepc.cmn'
      do i=1,4
        xu(i)=XCornAcWin(1,i)
        yu(i)=XCornAcWin(2,i)
      enddo
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,White)
      do i=1,10
        xu(1)=xu(1)-.05
        yu(1)=yu(1)-.05
        xu(2)=xu(2)-.05
        yu(2)=yu(2)+.05
        xu(3)=xu(3)+.05
        yu(3)=yu(3)+.05
        xu(4)=xu(4)+.05
        yu(4)=yu(4)-.05
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,White)
      enddo
      return
      end
      subroutine FeMakeAxisLabels(kterou,xmin,xmax,ymin,ymax,text)
      include 'fepc.cmn'
      dimension xpole(4),ypole(4),xp(3),xo(3)
      character*(*) text
      character*10 t10
      character*8 format
      data xp(3)/0./
      ix=kterou
      iy=3-kterou
      i=1
      call FeSetLabelStep(xmin,xmax,div,irad)
      if(div.le.0.) go to 9999
      if(irad.ge.0) then
        format='(i10   )'
      else
        write(format,'(''(f10.'',i2,'')'')') iabs(irad)
      endif
      xs=float(ifix(xmin/div))*div
      if(xs.lt.xmin) xs=xs+div
      xp(iy)=ymin
1210  xp(ix)=xs
      call multm(F2O,xp,xo,3,3,1)
      xpole(1)=FeXo2X(xo(1))
      ypole(1)=FeYo2Y(xo(2))
      if(kterou.eq.1) then
        if(F2O(5).lt.0.) then
          Znak=1.
        else
          Znak=-1.
        endif
        xpole(2)=xpole(1)
        ypole(2)=ypole(1)+Znak*5.
      else
        if(F2O(1).lt.0.) then
          Znak=1.
        else
          Znak=-1.
        endif
        xpole(2)=xpole(1)+Znak*5.
        ypole(2)=ypole(1)
      endif
      call FePolyLine(2,xpole,ypole,White)
      if(mod(i,2).eq.1) then
        if(irad.ge.0) then
          write(t10,format) nint(xs)
        else
          write(t10,format) xs
        endif
        call zhusti(t10)
        if(kterou.eq.1) then
          call FeOutSt(0,xpole(2),ypole(2)+Znak*15.,t10,'C',White)
        else
          call FeOutSt(0,xpole(2)+Znak*10.,ypole(2),t10,'R',White)
        endif
      endif
      xs=xs+div
      i=i+1
      if(xs.le.xmax+.001) go to 1210
      if(mod(i,2).eq.0) xs=xs-div
      xs=xs-div
      xp(kterou)=xs
      call multm(F2O,xp,xo,3,3,1)
      xp(1)=FeXo2X(xo(1))
      xp(2)=FeYo2Y(xo(2))
      if(kterou.eq.1) then
        call FeOutSt(0,xp(1),xp(2)+Znak*15.,text,'C',White)
      else
        call FeOutSt(0,xp(1)+Znak*10.,xp(2),text,'R',White)
      endif
9999  return
      end
      subroutine FeSetLabelStep(xmin,xmax,div,irad)
      include 'fepc.cmn'
      div=abs((xmax-xmin)/10.)
      if(div.le.0.) go to 9999
      pom=1.
      irad=0
1000  if(div.ge.1.) go to 2000
      div=div*10.
      pom=pom*.1
      irad=irad-1
      if(irad.lt.-10) go to 9000
      go to 1000
2000  if(div.lt.10.) go to 3000
      div=div*.1
      pom=pom*10.
      irad=irad+1
      if(irad.gt.10) go to 9000
      go to 2000
3000  if(div.gt.1..and.div.le.2.) then
        div=2.
      else if(div.gt.2..and.div.le.5.) then
        div=5.
      else if(div.gt.5..and.div.le.10.) then
        div=10.
      endif
      div=div*pom
      go to 9999
9000  div=0.
9999  return
      end
      logical function InsideXYPlot(xx,yy)
      include 'fepc.cmn'
      dimension x1(3),x2(3),x3(3),x4(3)
      InsideXYPlot=.true.
      x1(1)=xx
      x1(2)=yy
      x1(3)=0.
      do i=1,4
        if(i.lt.4) then
          k=i+1
        else
          k=1
        endif
        do j=1,3
          x2(j)=x1(j)-XCornAcWin(j,i)
          x3(j)=XCornAcWin(j,k)-XCornAcWin(j,i)
        enddo
        call VecMul(x2,x3,x4)
        if(x4(3).le.-.5) then
          InsideXYPlot=.false.
          exit
        endif
      enddo
      return
      end
      subroutine FeExpandLineToActiveWindow(xu1,yu1,xu2,yu2,
     1                                      xe1,ye1,xe2,ye2,ich)
      include 'fepc.cmn'
      logical InsideXYPlot
      ich=0
      n=0
      x11=xu1
      y11=yu1
      x12=xu2
      y12=yu2
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,1),XCornAcWin(2,1),
     2                        XCornAcWin(1,2),XCornAcWin(2,2),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          xe1=xpom
          ye1=ypom
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,2),XCornAcWin(2,2),
     2                        XCornAcWin(1,3),XCornAcWin(2,3),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,3),XCornAcWin(2,3),
     2                        XCornAcWin(1,4),XCornAcWin(2,4),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,4),XCornAcWin(2,4),
     2                        XCornAcWin(1,1),XCornAcWin(2,1),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      ich=1
9999  return
      end
      subroutine FeLineIntersection(x11,y11,x12,y12,
     1                              x21,y21,x22,y22,
     2                              xint,yint,ich)

      ich=0
      pomx=x12-x11
      pomy=y12-y11
      if(abs(pomx).gt.0.) then
        A1=-pomy/pomx
        B1=1.
        C1=-y11+pomy/pomx*x11
      else
        A1=1.
        B1=0.
        C1=-x11
      endif
      pomx=x22-x21
      pomy=y22-y21
      if(abs(pomx).gt.0.) then
        A2=-pomy/pomx
        B2=1.
        C2=-y21+pomy/pomx*x21
      else
        A2=1.
        B2=0.
        C2=-x21
      endif
      pomx=A1*B2-A2*B1
      if(pomx.ne.0.) then
        pomx=1./pomx
        xint=(-C1*B2+C2*B1)*pomx
        yint=(-A1*C2+A2*C1)*pomx
      else
        ich=1
      endif
      return
      end
      subroutine FeXYPlot(x,y,n,LineTypeDraw,PlotMode,Color)
      include 'fepc.cmn'
      dimension x(n),y(n),xpole(2),ypole(2),xline(:),yline(:)
      integer Color,PlotMode
      logical kresli
      allocatable xline,yline
      if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1  write(85,'(2f15.6)')(x(i),y(i),i=1,n)
      nn=1000
      allocate(xline(nn),yline(nn))
      if(PlotMode.eq.NormalPlotMode) then
        call FePlotMode('N')
      else
        call FePlotMode('E')
      endif
      call FeLineType(LineTypeDraw)
      k=0
      jp=1
      xpole(1)=x(1)
      ypole(1)=y(1)
      do 5000i=2,n
        Kresli=.false.
        dx=x(i)-x(i-1)
        if(y(i).gt.YoMaxAcWin.and.y(i-1).le.YoMaxAcWin) then
          xpole(2)=x(i-1)+dx*(YoMaxAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(2)=YoMaxAcWin
          Kresli=.true.
        else if(y(i).le.YoMaxAcWin.and.y(i-1).gt.YoMaxAcWin) then
          xpole(1)=x(i-1)+dx*(YoMaxAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(1)=YoMaxAcWin
          xpole(2)=x(i)
          ypole(2)=y(i)
          jp=1
        else if(y(i).lt.YoMinAcWin.and.y(i-1).ge.YoMinAcWin) then
          xpole(2)=x(i-1)+dx*(YoMinAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(2)=YoMinAcWin
          Kresli=.true.
        else if(y(i).ge.YoMinAcWin.and.y(i-1).lt.YoMinAcWin) then
          xpole(1)=x(i-1)+dx*(YoMinAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(1)=YoMinAcWin
          xpole(2)=x(i)
          ypole(2)=y(i)
          jp=1
        else if((y(i).le.YoMaxAcWin.and.y(i-1).le.YoMaxAcWin).and.
     1          (y(i).ge.YoMinAcWin.and.y(i-1).ge.YoMinAcWin)) then
          xpole(2)=x(i)
          ypole(2)=y(i)
        else
          go to 5000
        endif
        do j=jp,2
          k=k+1
          xline(k)=FeXo2X(xpole(j))
          yline(k)=FeYo2Y(ypole(j))
          if(k.eq.nn) then
            call FePolyLine(nn,xline,yline,Color)
            k=1
            xline(k)=xline(nn)
            yline(k)=yline(nn)
            jp=2
            go to 5000
          else
            jp=2
          endif
        enddo
        if(Kresli) then
          call FePolyLine(k,xline,yline,Color)
          xline(1)=xline(k)
          yline(1)=yline(k)
          k=1
          jp=1
          go to 5000
        else
          jp=2
        endif
5000  continue
      if(k.gt.1) call FePolyLine(k,xline,yline,Color)
      call FePlotMode('N')
      call FeLineType(NormalLine)
      deallocate(xline,yline)
      return
      end
      subroutine MakeGraph
      include 'fepc.cmn'
      dimension xd(1000),nd(1000)
      character*80 t80
      character*12 jmena(4)
      data jmena/'%Quit','%Print','%Save','New %graph'/
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,40.,YBottomMargin,14.)
      call FeMakeAcWin(20.,2.,10.,10.)
      pom=YMaxGrWin
      ypom=YMaxBasWin-24.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      j=1
      do i=1,4
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Jmena(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
          j=ButtonOff
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
          j=ButtonOff
        endif
        ypom=ypom-10.
        call FeQuestButtonOpen(ButtonLastMade,j)
      enddo
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit) then
        go to 9000
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSave.or.CheckNumber.eq.nButtPrint))
     2  then
        if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.ne.0) go to 2000
        else
          call FeSavePicture('graph',6,1)
          if(HardCopy.le.0) then
            HardCopy=0
            go to 2000
          endif
        endif
        call FeFlowDiagram(xd,nd,n)
        if(CheckNumber.eq.nButtPrint) then
          call FePrintFile(PSPrinter,HCFileName,ich)
          call DeleteFile(HCFileName)
          call FeTmpFilesClear(HCFileName)
        endif
        HardCopy=0
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew)
     1  then
        step=.002
        ln=NextLogicNumber()
        call OpenFile(ln,'melilit_final.m61','formatted','old')
        if(ErrFlag.ne.0) go to 2000
        dmin=999.
        dmax=0.
2300    read(ln,FormA,end=2320) t80
        if(t80.eq.' '.or.t80(1:4).eq.'----') go to 2300
        read(t80,'(2f8.3,f9.4)',err=2300,end=2320) pom,pom,pom
        dmin=min(dmin,pom)
        dmax=max(dmax,pom)
        dmin=1.90
        dmax=1.96
        go to 2300
2320    rewind ln
        dmin=aint(dmin/step-1.)*step
        dmax=aint(dmax/step+2.)*step
        n=int((dmax-dmin)/step+1.)
        do i=1,n
          nd(i)=0
          xd(i)=dmin+float(i-1)*step
        enddo
2340    read(ln,FormA,end=2360) t80
        if(t80.eq.' ') go to 2340
        read(t80,'(2f8.3,f9.4)',err=2340,end=2360) pom,pom,pom
        if(pom.ge.dmin.and.pom.le.dmax) then
          i=(pom-dmin)/step+1.
          nd(i)=nd(i)+1
        endif
        go to 2340
2360    call CloseIfOpened(ln)
        call FeFlowDiagram(xd,nd,n)
        call FeQuestButtonOpen(nButtSave,ButtonOff)
        call FeQuestButtonOpen(nButtPrint,ButtonOff)
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      go to 2000
9000  call FeQuestRemove(id)
      return
      end
      subroutine FeFlowDiagram(xd,nd,n)
      include 'fepc.cmn'
      dimension xd(n),nd(n)
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        yomx=0.
        do i=1,n
          yomx=max(yomx,float(nd(i)))
        enddo
        yomx=yomx*1.1
        xomn=xd(1)
        xomx=xd(n)
        yomn=0.
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,' ')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'freq')
        do i=1,n-1
          if(nd(i).gt.0) then
            xu(1)=xd(i)
            xu(2)=xd(i)
            xu(3)=xd(i+1)
            xu(4)=xd(i+1)
            yu(1)=0.
            yu(2)=nd(i)
            yu(3)=yu(2)
            yu(4)=0.
            do j=1,4
              xu(j)=FeXo2X(xu(j))
              yu(j)=FeYo2Y(yu(j))
            enddo
            call FePolyLine(4,xu,yu,White)
          endif
        enddo
      endif
      call FeHardCopy(HardCopy,'close')
      return
      end
      subroutine FeClearGrWin
      include 'fepc.cmn'
      call FeFillRectangle(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,4,0,
     1                     0,Black)
      return
      end
      subroutine FeQuestCreate(id,x,y,xd,n,Text,Check,ColorFill,
     1                         ExternalESC,ExternalOK)
      include 'fepc.cmn'
      character*(*) Text
      character*80  Veta
      integer Check,ColorFill,ExternalESC,ExternalOK,ButtonFrPlus,
     1        ExternalCancel,ExternalCancelIn
      logical YInLines,JeToKart
      JeToKart=.false.
      ExternalCancel=0
      go to 500
      entry FeKartQuestCreate(id,x,y,xd,n,Text,Check,ColorFill,
     1                        ExternalESC,ExternalOK,ExternalCancelIn)
      ExternalCancel=ExternalCancelIn
      JeToKart=.true.
500   YInLines=.true.
      go to 1000
      entry FeQuestAbsCreate(id,x,y,xd,ydi,Text,Check,ColorFill,
     1                       ExternalESC,ExternalOK)
      JeToKart=.false.
      ExternalCancel=0
      go to 600
      entry FeKartQuestAbsCreate(id,x,y,xd,ydi,Text,Check,ColorFill,
     1                           ExternalESC,ExternalOK)
      JeToKart=.true.
      ExternalCancel=0
600   yd=ydi
      YInLines=.false.
1000  EdwLastCheck=0
      ButtonFrPlus=0
      BlockedActiveObj=.false.
      if(JeToKart) then
        QuestKartIdPrev(id)=0
      else
        if(id.gt.1.and.KartId.ne.0) ButtonFrPlus=KartNDol-2
        call FeKartRefresh(0)
        QuestKartIdPrev(id)=KartId
        KartId=0
      endif
      if(KartOn) then
        xm=KartXMin+KartSidePruh
        ym=KartYMin+KartLowerPruh
        xdp=KartXMax-KartXMin-2.*KartSidePruh
        if(YInLines) then
          QuestLines(id)=KartLines
          QuestLength(id)=float(KartLines)*QuestLineWidth
        else
          QuestLines(id)=0
          QuestLength(id)=yd
        endif
      else
        xm=x
        ym=y
        xdp=xd
        if(YInLines) then
          QuestLines(id)=n
          if(ExternalESC.eq.0.or.ExternalOk.eq.0) then
            if(QuestLines(id).ge.0.) then
              QuestLines(id)=QuestLines(id)+1
            else
              QuestLines(id)=QuestLines(id)-10
            endif
          endif
          if(Text.ne.' ') then
            if(QuestLines(id).ge.0.) then
              QuestLines(id)=QuestLines(id)+1
            else
              QuestLines(id)=QuestLines(id)-10
            endif
          endif
          if(QuestLines(id).ge.0.) then
            QuestLength(id)=QuestLineWidth*float(QuestLines(id))
          else
            QuestLength(id)=QuestLineWidth*
     1                      (-float(QuestLines(id))*.1)
          endif
          if(ExternalESC.eq.0.or.ExternalOk.eq.0) then
            QuestLength(id)=QuestLength(id)+20.
          else
            QuestLength(id)=QuestLength(id)+10.
          endif
          if(Text.eq.' ') QuestLength(id)=QuestLength(id)+5.
        else
          QuestLines(id)=0
          QuestLength(id)=yd
        endif
      endif
      if(YInLines.and.WaitTime.gt.0) QuestLength(id)=QuestLength(id)+12.
      yd=QuestLength(id)
      if(.not.KartOn) then
        if(x.lt.0.) then
          xm=XCenBasWin-xdp*.5
        endif
        if(ym.lt.0.) then
          ym=YCenBasWin-yd*.5
        endif
      endif
      xm=anint(xm)
      xdp=anint(xdp)
      ym=anint(ym)
      yd=anint(yd)
      QuestXMin(id)=xm
      QuestXMax(id)=xm+xdp
      QuestXLen(id)=QuestXMax(id)-QuestXMin(id)
      QuestYMin(id)=ym
      QuestYMax(id)=ym+yd
      QuestYLen(id)=QuestYMax(id)-QuestYMin(id)
      QuestGetEdwActive(id)=.true.
      QuestState(id)=1
      LastQuest=id
      QuestColorFill(id)=ColorFill
      if(id.eq.1) then
        ButtonFr=1
        EdwFr=1
        CrwFr=1
        SbwFr=1
        LblFr=1
        LinkaFr=1
        SvisliceFr=1
        RolMenuFr=1
        ActiveObjFr=1
        QuestCalledFrom(id)=0
        QuestEdwNAll(id)=0
        QuestEdwActive(id)=0
        QuestSbwActive(id)=0
        QuestEventType(id)=0
        QuestEventNumber(id)=0
        QuestEventNumberAbs(id)=0
      else
        call FeFrToChange(LastActiveQuest,0)
        QuestCalledFrom(id)=LastActiveQuest
        if(EdwActive.gt.0) then
          call FeZmenKurzor(EdwActive)
          call FeEdwSelClear(EdwActive)
        endif
        ButtonFr=QuestButtonTo(id-1)+1+ButtonFrPlus
        EdwFr=QuestEdwTo(id-1)+1
        CrwFr=QuestCrwTo(id-1)+1
        SbwFr=QuestSbwTo(id-1)+1
        LblFr=QuestLblTo(id-1)+1
        LinkaFr=QuestLinkaTo(id-1)+1
        SvisliceFr=QuestSvisliceTo(id-1)+1
        RolMenuFr=QuestRolMenuTo(id-1)+1
        ActiveObjFr=QuestActiveObjTo(id-1)+1
        CheckType=0
        CheckNumber=0
      endif
      CheckMouse=.false.
      AllowChangeMouse=.false.
      CheckKeyBoard=.false.
      TakeMouseMove=.false.
      QuestButtonFr(id)=ButtonFr
      ButtonTo=ButtonFr-1
      ButtonMax=ButtonTo
      EdwTo=EdwFr-1
      CrwTo=CrwFr-1
      SbwTo=SbwFr-1
      LblTo=LblFr-1
      LinkaTo=LinkaFr-1
      SvisliceTo=SvisliceFr-1
      RolMenuTo=RolMenuFr-1
      RolMenuMax=RolMenuTo
      ActiveObjTo=ActiveObjFr-1
      QuestEdwFr(id)=EdwFr
      QuestEdwTo(id)=EdwTo
      QuestCrwFr(id)=CrwFr
      QuestCrwTo(id)=CrwTo
      QuestButtonFr(id)=ButtonFr
      QuestButtonTo(id)=ButtonTo
      QuestSbwFr(id)=SbwFr
      QuestSbwTo(id)=SbwTo
      QuestLblFr(id)=LblFr
      QuestLblTo(id)=LblTo
      QuestLinkaFr(id)=LinkaFr
      QuestLinkaTo(id)=LinkaTo
      QuestSvisliceFr(id)=SvisliceFr
      QuestSvisliceTo(id)=SvisliceTo
      QuestRolMenuFr(id)=RolMenuFr
      QuestRolMenuTo(id)=RolMenuTo
      QuestActiveObjFr(id)=ActiveObjFr
      QuestActiveObjTo(id)=ActiveObjTo
      QuestActiveObj(id)=ActiveObj
      KurzorEdw=0
      EdwActive=0
      SbwActive=0
      LastEdw=0
      ActiveObj=0
      InsertMode=.true.
      LastActiveQuest=id
      QuestTmpFile(id)='jqst'
      if(OpSystem.le.0) then
        call CreateTmpFile(QuestTmpFile(id),i,1)
      else
        call CreateTmpFileName(QuestTmpFile(id),id)
      endif
      if(KartOn) then
        call FeFillRectangle(xm,xm+xdp,ym,ym+yd,4,0,0,LightGray)
      else
        call FeSaveImage(xm-FrameWidth,xm+xdp+FrameWidth,
     1                   ym-FrameWidth,ym+yd+FrameWidth,
     2                   QuestTmpFile(id))
        if(ColorFill.gt.0) then
          call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                         ColorFill)
          call FeDrawFrame(xm,ym,xdp,yd,FrameWidth,Gray,White,Black,
     1                     id.gt.1)
        endif
      endif
      call FeQuestTitleMake(id,Text)
      i=0
      if(ExternalESC.eq.0) i=i+1
      if(ExternalOk .eq.0) i=i+1
      if(WaitTime.gt.0) then
        ysh=16.
      else
        ysh=10.
      endif
      if(WizardMode) then
        dpom=75.
      else
        dpom=45.
      endif
      if(YInLines) then
        if(QuestLines(id).ge.0) then
          j=QuestLines(id)-1
          if(QuestText(id).eq.' ') j=j+1
        else
          j=QuestLines(id)+10
          if(QuestText(id).eq.' ') j=j-10
        endif
        ypom=QuestYPosition(id,j)-ButYd*.5-10.
        idp=id
      else
        if(WaitTime.gt.0) then
          ypom=ym+16.
        else
          ypom=ym+10.
        endif
        idp=0
      endif
      if(ExternalESC.eq.0) then
        if(i.eq.1) then
          xmp=(xdp-dpom)*.5
        else
          xmp=xdp*.5-dpom-8.
        endif
        if(.not.YInLines) xmp=xm+xmp
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        QuestButtonTo(id)=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        ButtonEsc=ButtonTo
        if(WizardMode) then
          Veta='Back'
        else
          Veta='Esc'
        endif
        call FeButtonMake(ButtonEsc,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonEsc,ButtonOff)
        ButtonInfo(1,ButtonEsc)='Ignores changes and closes the form.'
        ButtonInfo(2,ButtonEsc)='Pressing Esc has the same effect.'
        ButtonInfo(3,ButtonEsc)='@'
      else if(ExternalESC.gt.0) then
        ButtonInfo(1,ButtonEsc)='@'
        if(KartOn) then
          ButtonESC=ExternalEsc
        else
          ButtonEsc=ExternalEsc+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonESC
      else
        ButtonESC=0
      endif
      QuestButtonEsc(id)=ButtonESC
      if(ExternalOK.eq.0) then
        if(i.eq.1) then
          xmp=(xdp-dpom)*.5
        else
          xmp=xdp*.5+8.
        endif
        if(.not.YInLines) xmp=xm+xmp
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        QuestButtonTo(id)=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        ButtonOk=ButtonTo
        if(WizardMode) then
          Veta='Next'
        else
          Veta='Ok'
        endif
        call FeButtonMake(ButtonOk,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonOk,ButtonOff)
        ButtonInfo(1,ButtonOk)='Accepts changes and closes the form.'
        ButtonInfo(2,ButtonOk)='Double-pressing  Enter has the same'
        ButtonInfo(3,ButtonOk)='effect.'
        ButtonInfo(4,ButtonOk)='@'
      else if(ExternalOK.gt.0) then
        ButtonInfo(1,ButtonOk)='@'
        if(KartOn) then
          ButtonOK=ExternalOK
        else
          ButtonOk=ExternalOK+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonOK
      else
        ButtonOK=0
      endif
      QuestButtonOk(id)=ButtonOk
      if(WizardMode) then
        Veta='Cancel'
        dpom=FeTxLength(Veta)+10.
        xmp=xdp-15.-dpom
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        QuestButtonTo(id)=ButtonTo
        ButtonCancel=ButtonTo
        call FeButtonMake(ButtonCancel,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonCancel,ButtonOff)
      else if(ExternalCancel.gt.0) then
        if(KartOn) then
          ButtonCancel=ExternalCancel
        else
          ButtonCancel=ExternalCancel+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonCancel
      else
        ButtonCancel=0
      endif
      if(ButtonOK.gt.0) then
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)
      else
        call FeMoveMouseTo((QuestXMin(id)+QuestXMax(id))*.5,
     1                     (QuestYMin(id)+QuestYMax(id))*.5)
      endif
      CrwZ(CrwFr:)=' '
      EdwZ(EdwFr:)=' '
      ButtonZ(ButtonFr:)=' '
      QuestCheck(id)=Check
      QuestVystraha=VasekTest.eq.0
      RemainTime=WaitTime
      call FeQuestActiveObjOn
      BlockedActiveObj=.true.
      EventTypeSave     =0
      EventNumberSave   =0
      EventNumberAbsSave=0
      EventType         =0
      EventNumber       =0
      return
      end
      subroutine FeQuestTitleMake(id,Text)
      include 'fepc.cmn'
      integer Color1,Color2
      character*(*) Text
      Color1=LightGray
      Color2=Black
      QuestText(id)=Text
      go to 2000
      entry FeQuestTitleRemove(id)
      Color1=LightGray
      Color2=LightGray
2000  ypom=QuestYPosition(id,0)
      call FeBoldFont
      if(QuestText(id).ne.' '.and.QuestColorFill(id).gt.0)
     1  call FeRewriteString(id,QuestXLen(id)*.5,ypom,QuestText(id),
     2                       QuestText(id),'C',Color1,Color2)
      call FeNormalFont
      return
      end
      subroutine FeQuestEvent(id,ich)
      include 'fepc.cmn'
      character*43 ItRemainsOld
      external FeQuestExternalCheck,FeQuestExternalMake,
     1         FeQuestUpdateListek
      integer FeGetSystemTime,FeMenuNew
      data ItRemainsOld/'It remains XX sec - press space for pause'/,
     1     imm/0/
      save xwt,ywt
      MakeExternalCheck=0
      KartUpdateListekFlag=0
      go to 1000
      entry FeQuestEventWithCheck(id,ich,FeQuestExternalCheck,
     1                            FeQuestExternalMake)
      MakeExternalCheck=1
      KartUpdateListekFlag=0
      go to 1000
      entry FeQuestEventWithKartUpdate(id,ich,FeQuestUpdateListek)
      MakeExternalCheck=0
      KartUpdateListekFlag=1
1000  ich=0
      BlockedActiveObj=.false.
      call FeQuestActiveUpdate
      SbwClicked=0
      imm=0
      LastActiveQuest=id
      if(EventTypeSave.ne.0) then
        if(CheckType.eq.EventButton) then
          CheckNumberAbs=CheckNumber+ButtonFr-1
          if(ButtonState(CheckNumberAbs).eq.ButtonOn) then
            if(QuestCheck(id).ne.QuestCheckSave(id).and.
     1         QuestCheck(id).eq.0) then
              EventType=EventButton
              EventNumberAbs=ButtonOK
              go to 2100
            endif
            ActiveObj=ActButton*10000+CheckNumberAbs
            EdwActive=0
            SbwActive=0
            call FeButtonOff(CheckNumberAbs)
            call FeButtonActivate(CheckNumberAbs)
          endif
        else if(CheckType.eq.EventCtrl.or.CheckType.eq.EventASCII.or.
     1          CheckType.eq.EventKey.or.CheckType.eq.EventCrw) then
          EventType=0
        else if(EdwActive.gt.0.and.EventType.ne.EventEdw.and.
     1          EventType.ne.EventEdwUp.and.EventType.ne.EventEdwDown)
     2    then
          if(EventType.eq.0) then
            EventType=EventTypeSave
            EventNumber=EventNumberSave
          endif
          if(EventType.ne.EventMouse.and.EventType.ne.EventKey.and.
     1       EventType.ne.EventASCII.and.EventType.ne.EventCTRL.and.
     2       EventType.ne.EventEdwUp.and.EventType.ne.EventEdwDown.and.
     3       EventType.ne.EventResize.and.EventType.ne.EventKartSw) then
            EdwActive=0
          endif
        else if(CheckType.eq.EventKartSw.and.EventType.eq.0) then
          EventType=EventTypeSave
          EventNumber=EventNumberSave
          go to 2100
        else if(CheckType.eq.EventKey) then
          if(SbwActive.eq.0.and.EdwActive.eq.0.and.
     1       EventNumber.ne.JeReturn) EventType=0
        endif
      endif
      if(EventType.ne.0) then
        if(EventType.eq.EventEdw) then
          call FeQuestActiveObjOff
          EventNumberAbs=EventNumber+EdwFr-1
          EdwActive=EventNumberAbs
          ActiveObj=ActEdw*10000+EventNumberAbs
          call FeQuestActiveObjOn
        else if(EventType.eq.EventButton) then
          EventNumberAbs=EventNumber+ButtonFr-1
        else if(EventType.eq.EventCrw) then
          EventNumberAbs=EventNumber+CrwFr-1
        else if(EventType.eq.EventRolMenu) then
          EventNumberAbs=EventNumber+RolMenuFr-1
        endif
        go to 2100
      endif
      if(VasekTest.ne.0) call FeQuestCheckForDuplicity
2000  if(MakeExternalCheck.gt.0)
     1  call FeQuestExternalCheck(FeQuestExternalMake)
      if(MakeExternalCheck.eq.-1) MakeExternalCheck=1
      if(WaitTime.gt.0) then
        if(imm.eq.0.and.RemainTime.ge.WaitTime) then
          imm=-1
          ItRemains   (33:)='for pause'
          ItRemainsOld(33:)='for pause'
        endif
      else
        imm=0
      endif
2050  call FeEvent(imm)
      if(WaitTime.gt.0) then
        if(EventType.eq.EventSystem.and.EventNumber.eq.JeTimeOut) then
          PlivejVyrez=0
          PripravVyrez=0
          if(ButtonOK.gt.0) then
            EventType=EventButton
            EventNumberAbs=ButtonOK
          else
            go to 9000
          endif
        else if(EventType.eq.0.and.EventNumber.eq.0) then
          imm=-2
          xwt=(QuestXMin(id)+QuestXMax(id))*.5
          ywt=QuestYMin(id)+10.
          write(ItRemains(12:13),'(i2)')
     1      nint(float(RemainTime)*.001+.5)
          PlivejVyrez=1
          PripravVyrez=1
          go to 2070
        else if(EventType.eq.EventASCII.and.
     1          char(EventNumber).eq.' ') then
          if(imm.lt.0) then
            imm=0
            ItRemains(33:)='to continue'
          else
            imm=-2
            ItRemains(33:)='for pause'
            StartTime=RemainTime-WaitTime+FeGetSystemTime()
          endif
          go to 2070
        endif
        PlivejVyrez=0
        PripravVyrez=0
        go to 2100
2070    if(ItRemainsOld.ne.ItRemains) then
          call FeDeferOutput
          call FeTextErase(0,xwt,ywt,ItRemainsOld,'C',LightGray)
          call FeOutSt(0,xwt,ywt,ItRemains,'C',Black)
          ItRemainsOld=ItRemains
          call FeReleaseOutput
          call FeDeferOutput
        endif
        go to 2050
      endif
2100  EdwLastCheck=EdwLastCheck+1
      if(EdwActive.gt.0) then
        EdwLastActive=EdwActive-EdwFr+1
        if((EventType.eq.EventEdw.and.EventNumberAbs.eq.EdwActive).or.
     1     (EventType.eq.EventKey.and.EventNumber.ne.JeTab.and.
     2      EventNumber.ne.JeShiftTab.and.EventNumber.ne.JeReturn).or.
     3     EventType.eq.EventEdwSel.or.
     4     EventType.eq.EventCtrl.or.EventType.eq.EventASCII.or.
     5     EventType.eq.EventMouse.or.
     6     EventType.eq.EventSystem.or.
     7     (EventType.eq.EventButton.and.
     8      EventNumberAbs.eq.ButtonEsc).or.
     9     (EventType.eq.EventButton.and.
     a      EventNumberAbs.eq.ButtonCancel).or.
     1     EdwLastCheck.ne.1) go to 2110
        if(EdwState(EdwActive).eq.EdwOpened.and.
     1     EventType.ne.EventResize) then
          call FeCheckStringEdw(EdwActive,ich)
          if(ich.eq.2) then
            ich=0
            CheckType=EventEdw
            CheckNumber=EdwActive-EdwFr+1
            CheckNumberAbs=EdwActive
            call FeReleaseOutput
            call FeDeferOutput
            go to 9999
          endif
        endif
      endif
      call FeReleaseOutput
      call FeDeferOutput
2110  if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        i=ActiveObj/10000
        if(i.eq.EventButton) then
          EventType=i
          EventNumberAbs=mod(ActiveObj,10000)
          EventNumber=mod(ActiveObj,10000)-ButtonFr+1
          go to 2110
        else if(i.eq.EventSbw) then
          EventType=0
          EventNumber=0
          go to 2100
        endif
      endif
      if((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonOk).or.
     1   ((EventType.eq.EventKey.and.EventNumber.eq.JeReturn).and.
     2     ButtonOK.gt.0)) then
        call FeButtonOnAction(ButtonOk)
        EventType=EventButton
        EventNumberAbs=ButtonOk
        EventNumber=ButtonOk-ButtonFr+1
        if(QuestCheck(id).ne.0) go to 9000
        ich=0
        go to 5000
      endif
2150  EdwLastCheck=0
      if(EventType.eq.EventASCII.and.EventNumber.eq.ichar(' ').and.
     1   EdwActive.eq.0) then
        EventType=ActiveObj/10000
        EventNumberAbs=mod(ActiveObj,10000)
        if(EventType.eq.EventButton) then
          EventNumber=EventNumberAbs-ButtonFr+1
        else if(EventType.eq.EventCrw) then
          EventNumber=EventNumberAbs-CrwFr+1
        else if(EventType.eq.EventRolMenu) then
          EventNumber=EventNumberAbs-RolMenuFr+1
        else if(EventType.eq.EventSbw) then
          EventNumber=EventNumberAbs-SbwFr+1
        endif
        go to 2100
      else if(((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonEsc).or.
     2         ((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).and.
     3          .not.WizardMode)).and.ButtonEsc.gt.0) then
        call FeButtonOnAction(ButtonEsc)
        ich=1
        go to 5000
      else if(((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonCancel).or.
     2         ((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).and.
     3           WizardMode)).and.ButtonCancel.gt.0) then
        call FeButtonOnAction(ButtonCancel)
        ich=-1
        go to 5000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
        go to 9000
      else if(EventType.eq.EventKartButt) then
        ib=EventNumberAbs
        call FeButtonOnAction(ib)
        go to 9000
      else if(EventType.eq.EventButton) then
        ib=EventNumberAbs
        call FeButtonOnAction(ib)
        go to 9000
      else if(EventType.eq.EventGlobal) then
        go to 9000
      else if(EventType.eq.EventKartSw) then
        if(QuestCheck(KartId).gt.0) then
          QuestCheck(KartId)=-1
          CheckType=EventKartSw
          CheckNumber=KartId-1
          CheckNumberAbs=KartId-1
          go to 9999
        else if(QuestCheck(KartId).lt.0) then
          QuestCheck(KartId)=1
        endif
        i=KartId
        call FePrepniListek(EventNumber)
        if(KartUpdateListekFlag.ne.0) call FeQuestUpdateListek(i)
        go to 2000
      else if(EventType.eq.EventKey.and.
     1        (EventNumber.eq.JeTab.or.EventNumber.eq.JeShiftTab)) then
        i=LocateInIntArray(ActiveObj,ActiveObjList(ActiveObjFr),
     1                     ActiveObjTo-ActiveObjFr+1)
        call FeQuestActiveObjOff
2200    if(i.gt.0) then
          if(EventNumber.eq.JeTab) then
            j=i+ActiveObjFr
            if(j.gt.ActiveObjTo) then
              j=ActiveObjFr
              i=1
            endif
          else
            j=i+ActiveObjFr-2
            if(j.lt.ActiveObjFr) then
              j=ActiveObjTo
              i=ActiveObjTo-ActiveObjFr+1
            endif
          endif
        else
          j=ActiveObjFr
        endif
        ActiveObj=ActiveObjList(j)
        call FeQuestActiveObjOn
        if(ActiveObj.lt.0) then
          if(EventNumber.eq.JeTab) then
            i=i+1
          else
            i=i-1
          endif
          go to 2200
        endif
        EventNumber=0
      else if(EventType.eq.EventCrw) then
        call FeQuestActiveObjOff
        i=EventNumberAbs
        if(CrwExGr(i).eq.0) then
          if(CrwState(i).eq.CrwOff) then
            call FeCrwOnAction(i)
          else
            call FeCrwOffAction(i)
          endif
        else
          do j=CrwFr,CrwTo
            if(CrwExGr(j).ne.CrwExGr(i).or.CrwState(j).eq.CrwClosed.or.
     1                                     CrwState(j).eq.CrwDisabled)
     2        cycle
            if(j.eq.i) then
              if(.not.CrwLogic(j)) then
                call FeCrwOnAction(j)
              endif
            else
              if(CrwLogic(j)) then
                call FeCrwOff(j)
              endif
            endif
          enddo
        endif
        if(CrwCheck(i).ne.0) go to 9000
      else if(EventType.eq.EventRolMenu) then
        i=EventNumberAbs
        call FeRolMenuOnAction(i)
        if(RolMenuType(i).eq.RolMenuUp) then
          ypom=RolMenuTextYMin(i)
        else
          ypom=RolMenuTextYMin(i)-
     1         float(RolMenuNumber(i)-1)*MenuLineWidth
        endif
        n=FeMenuNew(RolMenuTextXMin(i),ypom,
     1    RolMenuButtXMin(i)-RolMenuTextXMin(i)-3.,
     2    RolMenuText(1,i),RolMenuEnable(1,i),1,RolMenuNumber(i),1,1)
        if(n.gt.0) call FeRolMenuWriteText(i,n)
        call FeRolMenuOff(i)
        if(RolMenuCheck(i).ne.0) go to 9000
      else if(EventType.eq.EventRolMenuUnlock) then
        go to 9000
      else if(EventType.eq.EventCrwUnlock) then
        go to 9000
      else if(EventType.eq.EventEdwUnlock) then
        go to 9000
      else if(EventType.eq.EventResize) then
        go to 9000
      endif
      call FeQuestEventSbw(izpet)
      if(izpet.ne.0) go to 3000
      call FeQuestEventEdw(izpet)
      if(izpet.ne.0) go to 3000
      go to 3100
3000  if(izpet.eq.2000) then
        go to 2000
      else if(izpet.eq.2100) then
        go to 2100
      else if(izpet.eq.2110) then
        go to 2110
      else if(izpet.eq.9000) then
        go to 9000
      else if(izpet.eq.9999) then
        go to 9999
      endif
3100  if(EventType.eq.EventMouse) then
        if(CheckMouse) go to 9000
      else if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1        EventType.eq.EventAlt.or.EventType.eq.EventCtrl) then
        if(CheckKeyboard.and.EventNumber.ne.0.and.
     1     (EventType.ne.EventCtrl.or.(char(EventNumber).ne.'V'.and.
     2                                 char(EventNumber).ne.'C'.and.
     3                                 char(EventNumber).ne.'X'))) then
          if(EdwActive.gt.0) then
            if(EdwState(EdwActive).eq.EdwOpened)
     1        call FeCheckStringEdw(EdwActive,ich)
          endif
          go to 9000
        endif
      else if(EventType.eq.EventResize) then
        CheckType=EventResize
        CheckNumber=0
      endif
      go to 2000
5000  CheckType=0
      CheckNumber=0
      CheckNumberAbs=0
      go to 9999
9000  CheckType=EventType
      CheckNumber=EventNumber
      CheckNumberAbs=EventNumberAbs
      if(EdwActive.gt.0.and.CheckType.eq.EventEdw)
     1  EdwLastCheck=EdwLastCheck+1
9999  if(EventType.ne.EventMouse) then
        EventTypeSave  =EventType
        EventNumberSave=EventNumber
        EventNumberAbsSave=EventNumberAbs
      else
        EventTypeSave  =0
        EventNumberSave=0
        EventNumberAbsSave=0
      endif
      EventType=0
      EventNumber=0
      QuestCheckSave(id)=QuestCheck(id)
      BlockedActiveObj=.true.
      return
      end
      subroutine FeQuestEventSbw(izpet)
      include 'fepc.cmn'
      integer tf,SbwActiveOld,SbwActiveNew,FeGetSystemTime
      logical Drzi,TakeMouseMoveSave
      izpet=0
      if(SbwActive.ne.0) then
        if(EventType.eq.EventKey) then
          is=SbwItemPointer(SbwActive)
          if((SbwType(SbwActive).eq.SbwMenuType.or.
     1        SbwType(SbwActive).eq.SbwSelectType)) then
            if(SbwItemSel(is,SbwActive).ge.0) then
              it=SbwItemPointer(SbwActive)-SbwItemFrom(SbwActive)+1
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(SbwItemSelLast.eq.-1)
     1            SbwItemSelLast=1-SbwItemSel(is,SbwActive)
              else
                SbwItemSelLast=-1
              endif
            endif
          endif
          if(EventNumber.eq.JePageUp.or.EventNumber.eq.JePageDown) then
            if(((SbwItemPointer(SbwActive).eq.1).and.
     1          EventNumber.eq.JePageUp).or.
     2         ((SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive))
     3          .and.EventNumber.eq.JePageDown)) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JePageDown) then
                  isp=is
                else
                  isp=is-SbwLines(SbwActive)+1
                endif
                call FeSbwSetItemSel(isp,SbwLines(SbwActive))
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JePageUp) then
                n=-SbwLines(SbwActive)+1
                if(SbwStripes(SbwActive).gt.1) n=n-1
                i=is+n
                if(i.lt.1) then
                  n=1-is
                  ip=1
                else
                  if(SbwStripes(SbwActive).gt.1) then
                    ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
                  else
                    ip=i
                  endif
                endif
              else if(EventNumber.eq.JePageDown) then
                n=SbwLines(SbwActive)-1
                if(SbwStripes(SbwActive).gt.1) n=n+1
                i=is+n
                if(i.gt.SbwItemNMax(SbwActive)) then
                  n=SbwItemNMax(SbwActive)-is
                endif
                if(SbwStripes(SbwActive).gt.1) then
                  ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                   SbwItemNMax(SbwActive))
                else
                  ip=is-SbwLines(SbwActive)+1+n
                endif
              endif
              SbwItemPointer(SbwActive)=is+n
              it=it+n
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JePageUp) then
                call FeSbwPageUp(SbwActive)
              else if(EventNumber.eq.JePageDown) then
                call FeSbwPageDown(SbwActive)
              endif
            endif
            go to 2000
          else if(EventNumber.eq.JeHome.or.EventNumber.eq.JeEnd) then
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(((SbwItemPointer(SbwActive).eq.1).and.
     1            EventNumber.eq.JeHome).or.
     2           ((SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive))
     3             .and.EventNumber.eq.JeEnd)) go to 2000
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JeHome) then
                  isp=1
                  n=is
                else
                  isp=is
                  n=SbwItemNMax(SbwActive)-is+1
                endif
                call FeSbwSetItemSel(isp,n)
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeEnd) then
                is=SbwItemNMax(SbwActive)
                if(SbwStripes(SbwActive).gt.1) then
                  ip=SbwLines(SbwActive)*
     1               (SbwStripesMax(SbwActive)-SbwStripes(SbwActive))+1
                else
                  ip=is-SbwLines(SbwActive)+1
                endif
              else if(EventNumber.eq.JeHome) then
                ip=1
                is=1
              endif
              SbwItemPointer(SbwActive)=is
              it=is-SbwItemFrom(SbwActive)+1
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive),SbwItemNMax(SbwActive)))
     2          then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JeEnd) then
                ip=SbwItemNMax(SbwActive)
              else if(EventNumber.eq.JeHome) then
                ip=1
              endif
              call FeSbwShow(SbwActive,ip)
            endif
            go to 2000
          else if((EventNumber.eq.JeUp.or.EventNumber.eq.JeDown).and.
     1            .not.CtrlPressed) then
            if((SbwItemPointer(SbwActive).eq.1.and.EventNumber.eq.JeUp)
     1         .or.(SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive)
     2         .and.EventNumber.eq.JeDown)) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          call FeSbwSetItemSel(is,1)
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeUp) then
                it=it-1
                SbwItemPointer(SbwActive)=is-1
                if(SbwStripes(SbwActive).gt.1) then
                  ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
                else
                  ip=is-1
                endif
              else if(EventNumber.eq.JeDown) then
                it=it+1
                SbwItemPointer(SbwActive)=is+1
                if(SbwStripes(SbwActive).gt.1) then
                  ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                   SbwItemNMax(SbwActive))
                else
                  ip=is-SbwLines(SbwActive)+2
                endif
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JeUp) then
                call FeSbwLineUp(SbwActive)
              else if(EventNumber.eq.JeDown) then
                call FeSbwLineDown(SbwActive)
              endif
            endif
            go to 2000
          else if(EventNumber.eq.JeLeft.or.EventNumber.eq.JeRight) then
            if((SbwItemPointer(SbwActive).le.SbwLines(SbwActive).and.
     1          EventNumber.eq.JeLeft).or.
     2         (SbwItemPointer(SbwActive)+SbwLines(SbwActive).gt.
     3          SbwItemNMax(SbwActive).and.EventNumber.eq.JeRight).or.
     4         SbwStripes(SbwActive).le.1) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JeRight) then
                  isp=is
                else
                  isp=is-SbwLines(SbwActive)+1
                endif
                call FeSbwSetItemSel(isp,SbwLines(SbwActive))
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeLeft) then
                it=it-SbwLines(SbwActive)
                SbwItemPointer(SbwActive)=is-SbwLines(SbwActive)
                ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
              else if(EventNumber.eq.JeRight) then
                it=it+SbwLines(SbwActive)
                SbwItemPointer(SbwActive)=is+SbwLines(SbwActive)
                ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                 SbwItemNMax(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            endif
            go to 2000
          endif
        endif
      endif
      if(EventType.eq.EventMouse.and.SbwActive.ne.0) then
        if(XPos.gt.SbwXMin(SbwActive).and.
     1     XPos.lt.SbwXMax(SbwActive).and.
     2     YPos.gt.SbwYMin(SbwActive).and.
     3     YPos.lt.SbwYMax(SbwActive)) then
          if(EventNumber.eq.JeKoleckoKSobe) then
            call FeSbwLineDown(SbwActive)
          else if(EventNumber.eq.JeKoleckoOdSebe) then
            call FeSbwLineUp(SbwActive)
          endif
        else
          go to 9999
        endif
      endif
      SbwActiveOld=SbwActive
      SbwActiveNew=0
      if(EventType.eq.EventSbwLineUp.or.
     1   EventType.eq.EventSbwLineDown.or.
     2   EventType.eq.EventSbwColumnLeft.or.
     3   EventType.eq.EventSbwColumnRight) then
        if(EventType.eq.EventSbwLineUp) then
          itype=1
        else if(EventType.eq.EventSbwLineDown) then
          itype=2
        else if(EventType.eq.EventSbwColumnLeft) then
          itype=3
        else if(EventType.eq.EventSbwColumnRight) then
          itype=4
        endif
        SbwActiveNew=EventNumberAbs
        call FeSbwPosunOn(SbwActiveNew,itype)
        tf=FeGetSystemTime()
        if(itype.eq.1) then
          call FeSbwLineUp(SbwActiveNew)
        else if(itype.eq.2) then
          call FeSbwLineDown(SbwActiveNew)
        else if(itype.eq.3) then
          call FeSbwColumnLeft(SbwActiveNew)
        else if(itype.eq.4) then
          call FeSbwColumnRight(SbwActiveNew)
        endif
        call FeReleaseOutput
        call FeDeferOutput
        Drzi=.false.
2700    call FeEvent(1)
        if(Drzi) then
          if(FeGetSystemTime()-tf.lt.5) go to 2705
          i=SbwItemFrom(SbwActiveNew)
          j=SbwColumnFrom(SbwActiveNew)
          k=SbwStripeFrom(SbwActiveNew)
          if(itype.eq.1) then
            call FeSbwLineUp(SbwActiveNew)
          else if(itype.eq.2) then
            call FeSbwLineDown(SbwActiveNew)
          else if(itype.eq.3) then
            call FeSbwColumnLeft(SbwActiveNew)
          else if(itype.eq.4) then
            call FeSbwColumnRight(SbwActiveNew)
          endif
          tf=FeGetSystemTime()
          if(i.ne.SbwItemFrom(SbwActiveNew).or.
     1       j.ne.SbwColumnFrom(SbwActiveNew).or.
     2       k.ne.SbwStripeFrom(SbwActiveNew)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else
          Drzi=FeGetSystemTime()-tf.gt.500
        endif
2705    if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp)
     1    go to 2710
        go to 2700
2710    call FeSbwPosunOff(SbwActiveNew,itype)
      else if(EventType.eq.EventSbwPageUp.or.
     1        EventType.eq.EventSbwPageDown.or.
     2        EventType.eq.EventSbwColumnStrongRight.or.
     3        EventType.eq.EventSbwColumnStrongLeft) then
        if(EventType.eq.EventSbwPageUp) then
          itype=1
        else if(EventType.eq.EventSbwPageDown) then
          itype=2
        else if(EventType.eq.EventSbwColumnStrongLeft) then
          itype=3
        else if(EventType.eq.EventSbwColumnStrongRight) then
          itype=4
        endif
        SbwActiveNew=EventNumberAbs
        tf=FeGetSystemTime()
        if(itype.eq.1) then
          call FeSbwPageUp(SbwActiveNew)
        else if(itype.eq.2) then
          call FeSbwPageDown(SbwActiveNew)
        else if(itype.eq.3) then
          call FeSbwColumnStrongLeft(SbwActiveNew)
        else if(itype.eq.4) then
          call FeSbwColumnStrongRight(SbwActiveNew)
        endif
        call FeSbwSkokOn(SbwActiveNew,itype)
        call FeReleaseOutput
        call FeDeferOutput
        Drzi=.false.
2720    call FeEvent(1)
        if(Drzi) then
          if(SbwType(SbwActiveNew).ne.SbwPureType) then
            if(FeGetSystemTime()-tf.lt.5) go to 2725
          endif
          i=SbwItemFrom(SbwActiveNew)
          j=SbwcolumnFrom(SbwActiveNew)
          if(itype.eq.1) then
            call FeSbwPageUp(SbwActiveNew)
          else if(itype.eq.2) then
            call FeSbwPageDown(SbwActiveNew)
          else if(itype.eq.3) then
            call FeSbwColumnStrongLeft(SbwActiveNew)
          else if(itype.eq.4) then
            call FeSbwColumnStrongRight(SbwActiveNew)
          endif
          call FeSbwSkokOn(SbwActiveNew,itype)
          if(SbwType(SbwActiveNew).ne.SbwPureType) then
            tf=FeGetSystemTime()
          endif
          if(i.ne.SbwItemFrom(SbwActiveNew).or.
     1       j.ne.SbwcolumnFrom(SbwActiveNew)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else
          Drzi=FeGetSystemTime()-tf.gt.500
        endif
2725    if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp)
     1    go to 2730
        if(itype.le.2) then
          if(xpos.ge.SbwXMinPlovak(SbwActiveNew).and.
     1       xpos.le.SbwXMaxPlovak(SbwActiveNew)) then
            if(itype.eq.1) then
              if(ypos.le.SbwYMaxPlovak(SbwActiveNew)) go to 2730
            else if(itype.eq.2) then
              if(ypos.ge.SbwYMinPlovak(SbwActiveNew)) go to 2730
            endif
          endif
        else
          if(ypos.ge.SbwYMinStrelka(SbwActiveNew).and.
     1       ypos.le.SbwYMaxStrelka(SbwActiveNew)) then
            if(itype.eq.3) then
              if(xpos.ge.SbwXMinStrelka(SbwActiveNew)) go to 2730
            else if(itype.eq.4) then
              if(xpos.le.SbwXMaxStrelka(SbwActiveNew)) go to 2730
            endif
          endif
        endif
        go to 2720
2730    call FeSbwSkokOff(SbwActiveNew,itype)
      else if(EventType.eq.EventSbwPlovak) then
        SbwActiveNew=EventNumberAbs
        ypoc=ypos
        TakeMouseMoveSave=TakeMouseMove
        TakeMouseMove=.true.
2750    call FeEvent(1)
        if(EventType.eq.EventMouse) then
          if(EventNumber.eq.JeLeftUp) then
            go to 2760
          else if(EventNumber.eq.JeMove) then
            if(abs(ypoc-ypos).gt..5) then
              yin=anint((ypos-ypoc+SbwYMaxPlovak(SbwActiveNew))*
     1                  EnlargeFactor+2.)/EnlargeFactor
              call FeSbwPohniPlovak(SbwActiveNew,yin,yout)
              ypoc=ypos+yout-yin
              call FeReleaseOutput
              call FeDeferOutput
            endif
          endif
        else if(EventType.eq.EventSystem.and.
     1          EventNumber.eq.JeMimoOkno) then
          if(OpSystem.ne.1) go to 2760
        else
          go to 2750
        endif
        if(OpSystem.ne.1) then
          pom=(SbwXminPlovak(SbwActiveNew)+
     1         SbwXmaxPlovak(SbwActiveNew))*.5
          if(xpos.lt.pom-3..or.xpos.gt.pom+3.) then
            call FeMoveMouseTo(pom,ypos)
            xpos=pom
            call FeReleaseOutput
            call FeDeferOutput
          endif
        endif
        go to 2750
2760    TakeMouseMove=TakeMouseMoveSave
        go to 2000
      else if(EventType.eq.EventSbwStrelka) then
        SbwActiveNew=EventNumberAbs
        xpoc=xpos
        TakeMouseMoveSave=TakeMouseMove
        TakeMouseMove=.true.
2770    call FeEvent(1)
        if(EventType.eq.EventMouse) then
          if(EventNumber.eq.JeLeftUp) then
            go to 2780
          else if(EventNumber.eq.JeMove) then
            xin=anint((xpos-xpoc+SbwXMinStrelka(SbwActiveNew))*
     1                EnlargeFactor-2.)/EnlargeFactor
            call FeSbwPohniStrelku(SbwActiveNew,xin,xout)
            xout=xin
            xpoc=xpos+xout-xin
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else if(EventType.eq.EventSystem.and.
     1          EventNumber.eq.JeMimoOkno) then
          if(OpSystem.ne.1) go to 2780
        else
          go to 2770
        endif
        if(OpSystem.ne.1) then
          pom=(SbwYminStrelka(SbwActiveNew)+
     1                        SbwYmaxStrelka(SbwActiveNew))*.5
          if(ypos.lt.pom-1..or.ypos.gt.pom+1.) then
            call FeMoveMouseTo(xpos,pom)
            ypos=pom
            call FeReleaseOutput
            call FeDeferOutput
          endif
        endif
        go to 2770
2780    TakeMouseMove=TakeMouseMoveSave
        call FeWInfRemove(1)
        go to 2000
      else if(EventType.eq.EventSbwMenu) then
        SbwActiveNew=mod(EventNumberAbs,100)
        if(SbwType(SbwActiveNew).eq.SbwMenuType.or.
     1     SbwType(SbwActiveNew).eq.SbwSelectType) then
          if(SbwItemPointer(SbwActiveNew).gt.0) then
            ifr=SbwItemPointer(SbwActiveNew)
            it=SbwItemPointer(SbwActiveNew)-SbwItemFrom(SbwActiveNew)+1
            if(it.ge.1.and.
     1         it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew)
     2                  ,SbwItemNMax(SbwActiveNew)-
     3                   SbwItemFrom(SbwActiveNew)+1))
     4        call FeSbwItemOff(SbwActiveNew,it)
            itold=it
          else
            itold=0
          endif
          it=EventNumberAbs/100
          if(it.ge.1.and.
     1       it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew),
     2                 SbwItemNMax(SbwActiveNew)-
     3                 SbwItemFrom(SbwActiveNew)+1)) then
            if(SbwType(SbwActiveNew).eq.SbwSelectType) then
              if(CtrlPressed) then
                ip=it+SbwItemFrom(SbwActiveNew)-1
                SbwItemSel(ip,SbwActiveNew)=
     1            1-SbwItemSel(ip,SbwActiveNew)
              else if(ShiftPressed) then
                ito=it+SbwItemFrom(SbwActiveNew)-1
                j=1-SbwItemSel(ifr,SbwActiveNew)
                if(ito.gt.ifr) then
                  i1=ifr
                  i2=ito
                else
                  i1=ito
                  i2=ifr
                endif
                do i=i1,i2
                  SbwItemSel(i,SbwActiveNew)=j
                  if(i.ne.ito) then
                    k=i-SbwItemFrom(SbwActiveNew)+1
                    if(k.gt.0) call FeSbwItemOff(SbwActiveNew,k)
                  endif
                enddo
              endif
            endif
            call FeSbwItemOn(SbwActiveNew,it)
          else
            call FeSbwItemOn(SbwActiveNew,itold)
          endif
          if(SbwDoubleClickAllowed) then
            if(SbwActiveNew.eq.SbwClicked.and.it.eq.itold) then
              if(FeTimeDiff().lt..3) then
                EventType=EventSbwDoubleClick
                go to 9000
              endif
            else
              SbwClicked=FeTimeDiff()
              SbwClicked=SbwActiveNew
            endif
          else
            SbwClicked=0
          endif
        endif
      else if(EventType.eq.EventSbwRightClick) then
        SbwActiveNew=mod(EventNumberAbs,100)
        if(SbwType(SbwActiveNew).eq.SbwSelectType) then
          it=EventNumberAbs/100
          if(it.ge.1.and.
     1       it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew),
     2                 SbwItemNMax(SbwActiveNew))) then
            ip=it+SbwItemFrom(SbwActiveNew)-1
            if(SbwItemSel(ip,SbwActiveNew).eq.0) then
              do i=1,SbwItemNMax(SbwActiveNew)
                SbwItemSel(i,SbwActiveNew)=0
                k=i-SbwItemFrom(SbwActiveNew)+1
                if(k.gt.0) call FeSbwItemOff(SbwActiveNew,k)
              enddo
              call FeSbwItemOn(SbwActiveNew,it)
              SbwItemSel(ip,SbwActiveNew)=1
            endif
            go to 9000
          endif
        endif
      endif
      go to 9100
2000  izpet=2000
      SbwActiveOld=SbwActive
      SbwActiveNew=0
      go to 9100
9000  izpet=9000
9100  if(SbwActiveOld.ne.SbwActiveNew.and.SbwActiveNew.ne.0) then
        call FeQuestActiveObjOff
        SbwActive=SbwActiveNew
        if(SbwActive.ne.0) then
          ActiveObj=ActSbw*10000+SbwActive
          call FeSbwActivate(SbwActive)
        endif
      endif
9999  if(SbwActive.gt.0) then
        if(SbwType(SbwActive).eq.SbwSelectType) then
          SbwItemSelN(SbwActive)=0
          do i=1,SbwItemNMax(SbwActive)
            if(SbwItemSel(i,SbwActive).gt.0)
     1        SbwItemSelN(SbwActive)=SbwItemSelN(SbwActive)+1
          enddo
          call FeQuestEventSbwReset(SbwActive)
        endif
      endif
      return
      end
      subroutine FeQuestEventSbwReset(id)
      include 'fepc.cmn'
      integer ButtonStateQuest
      if(SbwType(id).eq.SbwSelectType) then
        if(RepeatMode) then
          j1=0
          j2=0
          if(SbwItemSelN(id).gt.1) then
            if(ButtonStateQuest(nButtRepeatClone).eq.ButtonOff) then
              j1=ButtonOff
              j2=ButtonDisabled
            endif
          else
            if(ButtonStateQuest(nButtRepeatClone).eq.ButtonOff) then
              j1=ButtonDisabled
              j2=ButtonOff
            endif
          endif
          if(j2.gt.0) then
            call FeQuestButtonOpen(nButtRepeatRead,j2)
            call FeQuestButtonOpen(nButtRepeatWrite,j2)
          endif
        endif
      endif
      return
      end
      subroutine FeQuestEventEdw(izpet)
      include 'fepc.cmn'
      integer EdwClicked
      character*256 Veta
      data EdwClicked,KurzorClicked,NClicked/0,0,0/
      izpet=0
      if(EventType.eq.EventEdw.or.EventType.eq.EventEdwUp.or.
     1   EventType.eq.EventEdwDown) then
        if(EdwActive.eq.0.or.EventNumberAbs.ne.EdwActive) then
          iwnew=EventNumberAbs
          if(iwnew.eq.0) go to 9999
          if(EdwState(iwnew).ne.EdwOpened) go to 9999
          call FeQuestActiveObjOff
          EdwActive=iwnew
          ActiveObj=ActEdw*10000+EdwActive
          call FeQuestActiveObjOn
        endif
        if(EventType.eq.EventEdw.and.KurzorClick.gt.0) then
          if(KurzorClick.eq.KurzorClicked.and.NClicked.ne.0.and.
     1       EdwActive.eq.EdwClicked.and.FeTimeDiff().lt..5) then
            Veta=EdwString(EdwActive)
            if(NClicked.eq.1) then
              if(Veta(KurzorClicked:KurzorClicked).ne.' ') then
                k=KurzorClicked
1100            if(k.gt.1) then
                  if(Veta(k-1:k-1).ne.' ') then
                    k=k-1
                    go to 1100
                  endif
                endif
                EdwSelFr=k-1
                k=KurzorClicked
1200            if(k.lt.idel(Veta)) then
                  if(Veta(k+1:k+1).ne.' ') then
                    k=k+1
                    go to 1200
                  endif
                endif
                EdwSelTo=k
                NClicked=NClicked+1
              else
                EdwSelFr=0
                EdwSelTo=idel(Veta)
                NClicked=0
              endif
              KurzorClick=-1
            else if(NClicked.eq.2) then
              EdwSelFr=0
              EdwSelTo=idel(Veta)
              NClicked=0
              KurzorClick=-1
            endif
          else
            i=FeTimeDiff()
            NClicked=1
            EdwClicked=EdwActive
            KurzorClicked=KurzorClick
          endif
        endif
      endif
      if(EdwActive.ne.0) then
        if(EventType.eq.EventKey) then
          if(EventNumber.ne.JeEscape.and.EventNumber.ne.JeReturn)
     1      then
            call FeEdwMakeAction(EdwActive)
            if(CheckKeyboard.and.EventNumber.ne.0) then
              go to 9000
            else
              go to 99999
            endif
          endif
        else if(EventType.eq.EventCtrl) then
          if(char(EventNumber).eq.'V') then
            call FeEdwCtrlVAction(EdwActive)
            EventNumber=0
          else if(char(EventNumber).eq.'X') then
            call FeEdwCtrlXAction(EdwActive)
            EventNumber=0
          else if(char(EventNumber).eq.'C') then
            call FeEdwCtrlCAction(EdwActive)
            EventNumber=0
          endif
        else if(EventType.eq.EventEdwUp.or.EventType.eq.EventEdwDown)
     1    then
          if(EventType.eq.EventEdwUp) then
            iz= 1
            i=1
          else
            iz=-1
            i=2
          endif
          call FeEudOn(EdwActive,i)
          call FeReleaseOutput
          call FeDeferOutput
          call FeWait(.1)
          call FeCheckEud(EdwActive,iz)
          EventType=EventEdw
          if(EdwCheck(EdwActive).ne.0) go to 9000
        else if(EventType.eq.EventEdwSel) then
          if((Kurzor.eq.EdwSelFr.and.EdwSelTo.eq.EventNumber).or.
     1       (Kurzor.eq.EdwSelTo.and.EdwSelFr.eq.EventNumber)) then
            go to 2000
          else
            EdwSelFr=min(Kurzor,EventNumber)
            EdwSelTo=max(Kurzor,EventNumber)
          endif
        endif
      endif
      call FeEdwMakeAction(EdwActive)
      if(CheckKeyboard.and.
     1   (EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     2    EventType.eq.EventCtrl).and.EventNumber.ne.0) go to 9000
      go to 99999
2000  izpet=2000
      go to 99999
2100  izpet=2100
      go to 99999
9000  izpet=9000
      go to 99999
9999  izpet=9999
99999 return
      end
      subroutine FeQuestCheckForDuplicity
      include 'fepc.cmn'
      character*128 TextInfoOld(50)
      character*2   TextInfoFlagsOld(50)
      integer UsedLetter(26)
      call SetIntArrayTo(UsedLetter,26,0)
      if(NInfo.gt.0) then
        do i=1,NInfo
          TextInfoOld(i)=TextInfo(i)
          TextInfoFlagsOld(i)=TextInfoFlags(i)
          TextInfoFlags(i)='LN'
        enddo
        NInfoOld=NInfo
      else
        NInfoOld=0
      endif
      TextInfo(1)=' '
      do i=ButtonFr,ButtonTo
        if(ButtonState(i).eq.ButtonClosed.or.ButtonZ(i:i).eq.' ') cycle
        j=ichar(ButtonZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,ButtonTo
          if(ButtonState(j).eq.ButtonClosed) cycle
          if(ButtonZ(i:i).eq.ButtonZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Button : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=ButtonText(j)
          endif
        enddo
        do j=CrwFr,CrwTo
          if(CrwState(j).eq.CrwClosed) cycle
          if(ButtonZ(i:i).eq.CrwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Crw : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=CrwLabel(j)
          endif
        enddo
        do j=EdwFr,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(ButtonZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Edw : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(ButtonZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/RolMenu : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwClosed.or.CrwZ(i:i).eq.' ') cycle
        j=ichar(CrwZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,CrwTo
          if(CrwState(j).eq.CrwClosed) cycle
          if(CrwZ(i:i).eq.CrwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/Crw : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=CrwLabel(j)
          endif
        enddo
        do j=EdwFr,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(CrwZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/Edw : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(CrwZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/RolMenu : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).eq.EdwClosed.or.EdwZ(i:i).eq.' ') cycle
        j=ichar(EdwZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(EdwZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Edw/Edw : '',2i3)') i,j
            TextInfo(2)=EdwLabel(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(EdwZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''EdwZ/RolMenu : '',2i3)') i,j
            TextInfo(2)=EdwLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=RolMenuFr,RolMenuTo
        if(RolMenuState(i).eq.RolMenuClosed.or.RolMenuZ(i:i).eq.' ')
     1    cycle
        j=ichar(RolMenuZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(RolMenuZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''RolMenu/RolMenu : '',2i3)') i,j
            TextInfo(2)=RolMenuLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      if(TextInfo(1).eq.' ') go to 9999
      Ninfo=8
      TextInfo(4)=ButtonZ(ButtonFr:ButtonTo)
      TextInfo(5)=CrwZ(CrwFr:CrwTo)
      TextInfo(6)=EdwZ(EdwFr:EdwTo)
      TextInfo(7)=RolMenuZ(RolMenuFr:RolMenuTo)
      j=0
      TextInfo(8)=' '
      do i=1,26
        if(UsedLetter(i).eq.0) then
          j=j+1
          TextInfo(8)(j:j)=char(i+ichar('a')-1)
        endif
      enddo
      i=idel(TextInfo(8))
      TextInfo(8)='Free letters: '//TextInfo(8)(:i)
      call FeInfoOut(-1.,-1.,'Duplicate identification :','L')
9999  if(NInfoOld.gt.0) then
        do i=1,NInfoOld
          TextInfo(i)=TextInfoOld(i)
          TextInfoFlags(i)=TextInfoFlagsOld(i)
        enddo
        NInfo=NInfoOld
      endif
      return
      end
      subroutine FeQuestWaitBegin
      include 'fepc.cmn'
      call FeMouseShape(3)
      if(ButtonOK    .gt.0) call FeButtonDisable(ButtonOK)
      if(ButtonEsc   .gt.0) call FeButtonDisable(ButtonESC)
      if(ButtonCancel.gt.0) call FeButtonDisable(ButtonCancel)
      go to 9999
      entry FeQuestWaitEnd
      call FeMouseShape(3)
      if(ButtonOK    .gt.0) call FeButtonOff(ButtonOK)
      if(ButtonEsc   .gt.0) call FeButtonOff(ButtonESC)
      if(ButtonCancel.gt.0) call FeButtonOff(ButtonCancel)
9999  return
      end
      subroutine FeQuestRemove(id)
      include 'fepc.cmn'
      Klic=0
      go to 1000
      entry FeQuestReset(id)
      Klic=1
1000  if(QuestState(id).le.0) go to 9999
      call FeQuestActiveObjOff
      call FeQuestTitleRemove(id)
      do i=1,EdwTo-EdwFr+1
        call FeQuestEdwRemove(i)
      enddo
      do i=1,CrwTo-CrwFr+1
        call FeQuestCrwRemove(i)
      enddo
      if(Klic.eq.1) then
        if(WizardMode) then
          IFrom=4
        else
          IFrom=3
        endif
      else
        IFrom=1
      endif
      do i=IFrom,ButtonTo-ButtonFr+1
        call FeQuestButtonRemove(i)
      enddo
      do i=1,IFrom-1
        call FeQuestButtonOff(i)
      enddo
      do i=1,KartNDol
        j=KartDolButt(i)
        call FeButtonRemove(j)
      enddo
      do i=1,SbwTo-SbwFr+1
        call FeQuestSbwRemove(i)
      enddo
      SbwActive=0
      do i=1,LblTo-LblFr+1
        call FeQuestLblRemove(i)
      enddo
      do i=1,LinkaTo-LinkaFr+1
        call FeQuestLinkaRemove(i)
      enddo
      do i=1,SvisliceTo-SvisliceFr+1
        call FeQuestSvisliceRemove(i)
      enddo
      do i=1,RolMenuTo-RolMenuFr+1
        call FeQuestRolMenuRemove(i)
      enddo
      if(Klic.ne.0) then
        EdwTo=EdwFr-1
        CrwTo=CrwFr-1
        ButtonTo=ButtonFr+IFrom-2
        ButtonMax=ButtonTo
        KartNDol=0
        SbwTo=SbwFr-1
        LblTo=LblFr-1
        LinkaTo=LinkaFr-1
        SvisliceTo=SvisliceFr-1
        RolMenuTo=RolMenuFr-1
        RolMenuMax=RolMenuTo
        ActiveObjTo=ActiveObjFr-1
        go to 9999
      endif
      ipom=0
      if(ButtonEsc.gt.0) call FeButtonRemove(ButtonEsc)
      if(ButtonOK .gt.0) call FeButtonRemove(ButtonOk)
      if(KartOn) then
        fw=0.
        QXMn=KartXMin
        QXMx=KartXMax
        QYMn=KartYMin
        QYMx=KartYMax
      else
        QXMn=QuestXMin(id)
        QXMx=QuestXMax(id)
        QYMn=QuestYMin(id)
        QYMx=QuestYMax(id)
        fw=FrameWidth
      endif
      call FeLoadImage(QXMn-fw,QXMx+fw,QYMn-fw,QYMx+fw,QuestTmpFile(id),
     1                 0)
      if(QuestKartIdPrev(id).gt.0) then
        KartId=QuestKartIdPrev(id)
        call FeKartRefresh(1)
      endif
      call FeFrToRefresh(QuestCalledFrom(id))
      call FeQuestActiveObjOn
      if(id.eq.LastQuest) then
        LastQuest=QuestCalledFrom(id)
        LastActiveQuest=LastQuest
      endif
      QuestState(id)=0
      QuestCheck(id)=0
      QuestCalledFrom(id)=0
      if(WizardMode.and.id.eq.WizardId) then
        WizardMode=.false.
        WizardId=0
      endif
      WaitTime=0
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      call FeDeferOutput
9999  return
      end
      subroutine FeQuestResetIndividual(Text)
      include 'fepc.cmn'
      character*(*) Text
      go to 9999
      entry FeQuestUpravPocty(iedw,icrw,ibutt,iupd)
      EdwTo=max(0,iedw)
      CrwTo=max(0,icrw)
      ButtonTo=max(30,ibutt)
      ButtonMax=ButtonTo
9999  return
      end
      subroutine FeQuestLblMake(id,xt,n1,Text,Justify,Font)
      include 'fepc.cmn'
      integer Color
      character*(*) Text
      character*1 Justify,Font
      integer UseTabsOld,ColorErase
      logical EqIgCase,Remove
      yt=QuestYPosition(id,n1)
      go to 1000
      entry FeQuestAbsLblMake(id,xt,yti,Text,Justify,Font)
      yt=yti
1000  LblTo=LblTo+1
      LblLastMade=LblTo-LblFr+1
      QuestLblTo(id)=LblTo
      LblText(LblTo)=Text
      LblJustify(LblTo)=Justify
      LblFont(LblTo)=Font
      LblState(LblTo)=LblOn
      LblXPos(LblTo)=xt
      LblYPos(LblTo)=yt
      LblTabs(LblTo)=UseTabs
      i=LblTo
      go to 2000
      entry FeQuestLblOn(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved.or.LblState(i).eq.LblOn) go to 9999
2000  LblState(i)=LblOn
      Color=Black
      go to 3000
      entry FeQuestLblOff(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved) go to 9999
      LblState(i)=LblOff
      Color=LightGray
      go to 3000
      entry FeQuestLblDisable(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved) go to 9999
      LblState(i)=LblDisabled
      Color=White
      go to 3000
      entry FeQuestLblRemove(iwa)
      i=iwa+LblFr-1
      LblState(i)=LblRemoved
      Color=LightGray
      Remove=.true.
      go to 3000
      entry FeQuestLblChange(iwa,Text)
      i=iwa+LblFr-1
      UseTabsOld=UseTabs
      UseTabs=LblTabs(i)
      if(LblState(i).eq.LblOn) then
        if(EqIgCase(LblFont(i),'B')) call FeBoldFont
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),LightGray)
        if(EqIgCase(LblFont(i),'B')) call FeNormalFont
      endif
      LblState(i)=LblOn
      LblText(i)=Text
      Color=Black
3000  UseTabsOld=UseTabs
3100  if(EqIgCase(LblFont(i),'B')) call FeBoldFont
      UseTabs=LblTabs(i)
      ColorErase=QuestColorFill(LastActiveQuest)
      if(ColorErase.le.0) ColorErase=LightGray
      if(Color.ne.ColorErase) then
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),ColorErase)
        call FeOutSt(LastActiveQuest,LblXPos(i),LblYPos(i),LblText(i),
     1               LblJustify(i),Color)
      else
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),ColorErase)
      endif
      if(EqIgCase(LblFont(i),'B')) call FeNormalFont
      UseTabs=UseTabsOld
9999  return
      end
      subroutine FeQuestLinkaMake(id,n1)
      include 'fepc.cmn'
      integer Color1,Color2
      XFrom=QuestXMin(id)+2.5
      XTo=QuestXMax(id)-2.5
      go to 1000
      entry FeQuestLinkaFromToMake(id,XFromIn,XToIn,n1)
      XFrom=QuestXMin(id)+XFromIn
      XTo=QuestXMin(id)+XToIn
1000  ym=QuestYPosition(id,n1)
      go to 1200
      entry FeQuestAbsLinkaMake(id,ymi)
      XFrom=QuestXMin(id)+2.5
      XTo=QuestXMax(id)-2.5
      go to 1100
      entry FeQuestAbsLinkaFromToMake(id,XFromIn,XToIn,ymi)
      XFrom=QuestXMin(id)+XFromIn
      XTo=QuestXMin(id)+XToIn
1100  ym=ymi
1200  LinkaTo=LinkaTo+1
      LinkaLastMade=LinkaTo-LinkaFr+1
      QuestLinkaTo(id)=LinkaTo
      LinkaState(LinkaTo)=LinkaOn
      LinkaXMin(LinkaTo)=XFrom
      LinkaXMax(LinkaTo)=XTo
      LinkaYPos(LinkaTo)=QuestYMin(id)+ym
      i=LinkaTo
      go to 1300
      entry FeQuestLinkaOn(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaOn
1300  Klic=0
      go to 2000
      entry FeQuestLinkaRemove(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaRemoved
      go to 1400
      entry FeQuestLinkaOff(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaOff
1400  Klic=1
2000  if(Klic.eq.0) then
        Color1=Gray
        Color2=White
      else
        Color1=LightGray
        Color2=LightGray
      endif
      call FeTwoPixLineHoriz(LinkaXMin(i),LinkaXMax(i),LinkaYPos(i),
     1                       Color1,Color2)
9999  return
      end
      subroutine FeQuestSvisliceMake(id,xmi,Shape)
      include 'fepc.cmn'
      integer Color1,Color2,Shape
      YFrom=0.
      YTo=QuestYMax(id)-QuestYMin(id)
      go to 1200
      entry FeQuestSvisliceFromToMake(id,xmi,ilFrom,ilTo,Shape)
      YFrom=QuestYPosition(id,ilFrom)+.5*QuestLineWidth
      YTo=QuestYPosition(id,ilTo)-.5*QuestLineWidth
      go to 1200
      entry FeQuestAbsSvisliceFromToMake(id,xmi,YFromIn,YToIn,Shape)
      YFrom=YFromIn
      YTo=YToIn
1200  xm=xmi
      SvisliceTo=SvisliceTo+1
      SvisliceLastMade=SvisliceTo-SvisliceFr+1
      QuestSvisliceTo(id)=SvisliceTo
      SvisliceState(SvisliceTo)=SvisliceOn
      SvisliceYMin(SvisliceTo)=YFrom
      SvisliceYMax(SvisliceTo)=YTo
      SvisliceXPos(SvisliceTo)=xm
      SvisliceShape(SvisliceTo)=Shape
      i=SvisliceTo
      go to 1300
      entry FeQuestSvisliceOn(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceOn
1300  Klic=0
      go to 2000
      entry FeQuestSvisliceRemove(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceRemoved
      go to 1400
      entry FeQuestSvisliceOff(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceOff
1400  Klic=1
2000  if(Klic.eq.0) then
        if(SvisliceShape(i).eq.0) then
          Color1=Gray
          Color2=White
        else
          Color1=Black
          Color2=Black
        endif
      else
        Color1=LightGray
        Color2=LightGray
      endif
      if(SvisliceShape(i).eq.1) then
        call FeOnePixLineVertic(SvisliceXPos(i)+QuestXMin(id),
     1                          SvisliceYMin(i)+QuestYMin(id),
     2                          SvisliceYMax(i)+QuestYMin(id),
     3                          Color1,Color2)
      else
        call FeTwoPixLineVertic(SvisliceXPos(i)+QuestXMin(id),
     1                          SvisliceYMin(i)+QuestYMin(id),
     2                          SvisliceYMax(i)+QuestYMin(id),
     3                          Color1,Color2)
      endif
9999  return
      end
      function SvisliceYMaxQuest(i)
      include 'fepc.cmn'
      SvisliceYMaxQuest=SvisliceYMax(i+SvisliceFr-1)
      return
      end
      function SvisliceYMinQuest(i)
      include 'fepc.cmn'
      SvisliceYMinQuest=SvisliceYMin(i+SvisliceFr-1)
      return
      end
      function SvisliceXPosQuest(i)
      include 'fepc.cmn'
      SvisliceXPosQuest=SvisliceXPos(i+SvisliceFr-1)
      return
      end
      subroutine FeQuestEdwMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                          Check)
      include 'fepc.cmn'
      character*(*) Text
      character*1   Justify
      integer Check,Color
      logical Erase
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 1000
      entry FeQuestAbsEdwMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check)
      yt=yti
      yw=ywi
1000  ient=0
      go to 2000
      entry FeQuestEudMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,Check)
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 1100
      entry FeQuestAbsEudMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check)
      yt=yti
      yw=ywi
1100  ient=1
2000  EdwTo=EdwTo+1
      EdwLastMade=EdwTo-EdwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActEdw+EdwTo
      QuestEdwTo(id)=EdwTo
      EdwCheck(EdwTo)=Check
      EdwLabel(EdwTo)=Text
      EdwJustify(EdwTo)=Justify
      EdwLabelX(EdwTo)=xt+QuestXMin(LastQuest)
      EdwLabelY(EdwTo)=yt+QuestYMin(LastQuest)
      ym=yw
      call FeEdwMake(EdwTo,id,xw,ym,xdw,ydw)
      if(ient.eq.0) then
        EdwUpDown(EdwTo)=0
      else
        EdwUpDown(EdwTo)=1
        call FeEudMake(EdwTo)
      endif
      go to 9999
      entry FeQuestEudOpen(iwa,imn,imx,istep,rmn,rmx,rstep)
      iwp=iwa+EdwFr-1
      if(EdwType(iwp).lt.0) then
        EdwInt(2,iwp)=imn
        EdwInt(3,iwp)=imx
        EdwInt(4,iwp)=istep
        EdwInt(5,iwp)=nint(float(imx-imn)/float(istep))
      else if(EdwType(iwp).gt.0) then
        EdwReal(2,iwp)=rmn
        EdwReal(3,iwp)=rmx
        EdwReal(4,iwp)=abs(rstep)
      endif
      call FeCheckEud(iwp,0)
      go to 9999
      entry FeQuestEdwLock(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwLocked) call FeEdwLock(iwp)
      go to 9999
      entry FeQuestEdwDisable(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwDisabled) call FeEdwDisable(iwp)
      Color=White
      go to 6000
      entry FeQuestEdwClose(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwClosed.and.
     1   EdwState(iwp).ne.EdwRemoved) then
        Erase=.true.
        call FeEdwClose(iwp)
      else
        Erase=.false.
      endif
      go to 4000
      entry FeQuestEdwRemove(iwa)
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwRemoved) then
        Erase=EdwState(iwp).ne.EdwClosed
        call FeEdwRemove(iwp)
      else
        Erase=.false.
      endif
      go to 4000
      entry FeQuestEdwSelect(iwa)
      call FeEdwSelect(iwa+EdwFr-1)
      go to 9999
      entry FeQuestEdwDeselect(iwa)
      call FeEdwDeselect(iwa+EdwFr-1)
      go to 9999
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActEdw.and.j.eq.iwp) then
        call FeEdwDeactivate(iwp)
        ActiveObj=0
      endif
5000  if(EdwLabel(iwp).ne.' '.and.Erase) then
        call FeTextErase(0,EdwLabelX(iwp),EdwLabelY(iwp),
     1                   EdwLabel(iwp),EdwJustify(iwp),LightGray)
      endif
      go to 9999
6000  if(EdwLabel(iwp).ne.' ') then
        call FeTextErase(0,EdwLabelX(iwp),EdwLabelY(iwp),EdwLabel(iwp),
     1                   EdwJustify(iwp),LightGray)
        call FeOutStUnder(0,EdwLabelX(iwp),EdwLabelY(iwp),EdwLabel(iwp),
     1                    EdwJustify(iwp),Color,Color,EdwZ(iwp:iwp))
      endif
9999  return
      end
      subroutine FeQuestEdwLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      iw=iwp+EdwFr-1
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActEdw.and.j.eq.iw) then
        j=1
      else
        j=0
      endif
      if(EdwState(iw).eq.EdwOpened.or.EdwState(iw).eq.EdwDisabled) then
        if(EdwLabel(iw).ne.' ')
     1    call FeTextErase(0,EdwLabelX(iw),EdwLabelY(iw),
     2                     EdwLabel(iw),EdwJustify(iw),LightGray)
        if(j.ne.0) call FeEdwDeactivate(iw)
      endif
      if(Text.ne.' ') then
        call FeGetTextRectangle(EdwLabelX(iw),EdwLabelY(iw),
     1    Text,EdwJustify(iw),1,x1,x2,y1,y2,refx,refy,conx,
     2    cony)
        EdwLabelXMin(iw)=x1
        EdwLabelXMax(iw)=x2
        EdwLabelYMin(iw)=y1
        EdwLabelYMax(iw)=y2
      else
        EdwZ(iw:iw)=' '
        EdwLabelXMin(iw)=0.
        EdwLabelXMax(iw)=0.
        EdwLabelYMin(iw)=0.
        EdwLabelYMax(iw)=0.
      endif
      if(EdwState(iw).eq.EdwOpened) then
        if(Text.ne.' ') then
          call FeOutStUnder(0,EdwLabelX(iw),EdwLabelY(iw),
     2                      Text,EdwJustify(iw),Black,Black,
     3                      EdwZ(iw:iw))
          if(j.ne.0) call FeEdwActivate(iw)
        endif
      endif
      EdwLabel(iw)=Text
      return
      end
      subroutine FeQuestCrwLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer UseTabsOld
      UseTabsOld=UseTabs
      iw=iwp+CrwFr-1
      UseTabs=CrwLabelTabs(iw)
      if(CrwLabel(iw).ne.' ')
     1  call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),
     2                   CrwLabel(iw),CrwJustify(iw),LightGray)
      if(CrwState(iw).ne.CrwClosed) then
        if(Text.ne.' ') then
          call FeGetTextRectangle(CrwLabelX(iw),CrwLabelY(iw),
     1      Text,CrwJustify(iw),1,x1,x2,y1,y2,refx,refy,
     2      conx,cony)
          CrwLabelXMin(iw)=x1
          CrwLabelXMax(iw)=x2
          CrwLabelYMin(iw)=y1
          CrwLabelYMax(iw)=y2
          call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),
     1                     Text,CrwJustify(iw),LightGray)
          call FeOutStUnder(0,CrwLabelX(iw),CrwLabelY(iw),
     1                      Text,CrwJustify(iw),Black,Black,
     2                      CrwZ(iw:iw))
        else
          CrwZ(iw:iw)=' '
        endif
      endif
      CrwLabel(iw)=Text
      UseTabs=UseTabsOld
      return
      end
      subroutine FeQuestButtonMake(id,xw,n1,xdw,ydw,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer OpenState
      ym=QuestYPosition(id,n1)-ydw*.5
      go to 1000
      entry FeQuestAbsButtonMake(id,xw,yw,xdw,ydw,Text)
      ym=yw
1000  ButtonTo=ButtonTo+1
      ButtonMax=ButtonTo
      ButtonLastMade=ButtonTo-ButtonFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
      QuestButtonTo(id)=ButtonTo
      call FeButtonMake(ButtonTo,id,xw,ym,xdw,ydw,Text)
      go to 9999
      entry FeQuestButtonOpen(iwa,OpenState)
      call FeButtonOpen(iwa+ButtonFr-1,OpenState)
      go to 9999
      entry FeQuestButtonDisable(iwa)
      call FeButtonDisable(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonOff(iwa)
      iw=iwa+ButtonFr-1
      call FeButtonOff(iw)
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton.and.iw.eq.j) call FeButtonActivate(iw)
      go to 9999
      entry FeQuestButtonOn(iwa)
      call FeButtonOn(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonClose(iwa)
      if(iwa.le.0) go to 9999
      iw=iwa+ButtonFr-1
      call FeButtonClose(iw)
      go to 4000
      entry FeQuestButtonActivate(iwa)
      call FeButtonActivate(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonDeactivate(iwa)
      call FeButtonDeactivate(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonRemove(iwa)
      iw=iwa+ButtonFr-1
      call FeButtonRemove(iw)
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton.and.iw.eq.j) then
        call FeButtonDeactivate(iw)
        ActiveObj=0
      endif
9999  return
      end
      subroutine FeQuestMouseToButton(nButt)
      include 'fepc.cmn'
      i=nButt+ButtonFr-1
      call FeMoveMouseTo((ButtonXMin(i)+ButtonXMax(i))*.5,
     1                   (ButtonYMin(i)+ButtonYMax(i))*.5)
      return
      end
      subroutine FeQuestButtonLabelChange(iw,Text)
      include 'fepc.cmn'
      character*(*) Text
      iwa=iw+ButtonFr-1
      ButtonText(iwa)=Text
      i=ButtonState(iwa)
      if(i.ne.ButtonClosed) then
        ButtonState(iwa)=ButtonClosed
        call FeQuestButtonOpen(iw,i)
      endif
      return
      end
      subroutine FeQuestCrwMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                          Check,ExGr)
      include 'fepc.cmn'
      character*(*) Text
      character*1   Justify
      integer Check,ExGr,Color
      logical   YesNo
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-xdw*.5
      go to 1000
      entry FeQuestAbsCrwMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check,ExGr)
      yw=ywi
      yt=yti
1000  CrwTo=CrwTo+1
      CrwLastMade=CrwTo-CrwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActCrw+CrwTo
      QuestCrwTo(id)=CrwTo
      CrwCheck(CrwTo)=Check
      CrwLabel(CrwTo)=Text
      CrwJustify(CrwTo)=Justify
      CrwLabelX(CrwTo)=xt+QuestXMin(LastQuest)
      CrwLabelY(CrwTo)=yt+QuestYMin(LastQuest)
      CrwExGr(CrwTo)=ExGr
      CrwType(CrwTo)=CrwTypeRadio
      if(idel(Text).ge.2) then
        if(Text(1:2).eq.'##') then
          CrwType(CrwTo)=CrwTypeLetter
        else if(Text(1:2).eq.'$$') then
          CrwType(CrwTo)=CrwTypeBMP
          CrwBMPName(CrwTo)=Text(3:)
        endif
      endif
      call FeCrwMake(CrwTo,id,xw,yw,xdw,ydw)
      go to 9999
      entry FeQuestCrwOpen(iwa,YesNo)
      i=iwa+CrwFr-1
      if(CrwLabel(i).ne.' '.and.CrwType(i).eq.CrwTypeRadio) then
        call FeGetTextRectangle(CrwLabelX(i),CrwLabelY(i),
     1    CrwLabel(i),CrwJustify(i),1,x1,x2,y1,y2,refx,refy,conx,cony)
        CrwLabelXMin(i)=x1
        CrwLabelXMax(i)=x2
        CrwLabelYMin(i)=y1
        CrwLabelYMax(i)=y2
      else
        CrwZ(i:i)=' '
        CrwLabelXMax(i)=0.
        CrwLabelYMax(i)=0.
      endif
      call FeCrwOpen(i,YesNo)
      Color=Black
      go to 6000
      entry FeQuestCrwOn(iwa)
      i=iwa+CrwFr-1
      call FeCrwOn(i)
      Color=Black
      go to 6000
      entry FeQuestCrwOff(iwa)
      iw=iwa+CrwFr-1
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      call FeCrwOff(iw)
      if(i.eq.ActCrw.and.iw.eq.j) call FeCrwActivate(iw)
      Color=Black
      i=iw
      go to 6000
      entry FeQuestCrwClose(iwa)
      if(iwa.le.0) go to 9999
      iw=iwa+CrwFr-1
      if(CrwState(iw).eq.CrwClosed.or.
     1   CrwState(iw).eq.CrwRemoved) go to 5100
      call FeCrwClose(iw)
      go to 4000
      entry FeQuestCrwRemove(iwa)
      iw=iwa+CrwFr-1
      if(CrwState(iw).eq.CrwRemoved) go to 5100
      call FeCrwRemove(iw)
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActCrw.and.iw.eq.j) then
        call FeCrwActivate(iw)
        ActiveObj=0
      endif
5000  if(CrwLabel(iw).ne.' '.and.CrwType(iw).eq.CrwTypeRadio)
     1  call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),CrwLabel(iw),
     2                   CrwJustify(iw),LightGray)
5100  CrwZ(iw:iw)=' '
      go to 9999
      entry FeQuestCrwLock(iwa)
      i=iwa+CrwFr-1
      call FeCrwLock(i)
      Color=Black
      go to 6000
      entry FeQuestCrwDisable(iwa)
      i=iwa+CrwFr-1
      call FeCrwDisable(i)
      Color=White
      go to 6000
      entry FeQuestCrwActivate(iwa)
      i=iwa+CrwFr-1
      call FeCrwActivate(i)
      go to 9999
      entry FeQuestCrwDeactivate(iwa)
      i=iwa+CrwFr-1
      call FeCrwDeactivate(i)
      go to 9999
6000  if(CrwLabel(i).ne.' '.and.CrwType(i).ne.CrwTypeLetter.and.
     1   CrwType(i).ne.CrwTypeBMP) then
        call FeTextErase(0,CrwLabelX(i),CrwLabelY(i),CrwLabel(i),
     1                   CrwJustify(i),LightGray)
        call FeOutStUnder(0,CrwLabelX(i),CrwLabelY(i),CrwLabel(i),
     1                    CrwJustify(i),Color,Color,CrwZ(i:i))
      endif
9999  return
      end
      subroutine FeQuestSbwMake(id,xw,nn,xdw,nd,nstrip,CutText,BarType)
      include 'fepc.cmn'
      character*(*) FileName
      integer CutText,BarType
      yw=QuestYPosition(id,nn)-QuestLineWidth*.5+8.
      pom=QuestLineWidth*float(nd)-6.
      i=max(ifix(pom/MenuLineWidth),1)
      ydw=float(i)*MenuLineWidth
      yw=yw+(pom-ydw)*.5
      go to 1000
      entry FeQuestAbsSbwMake(id,xw,ywi,xdw,ydwi,nstrip,CutText,BarType)
      yw=ywi
      ydw=ydwi
1000  SbwTo=SbwTo+1
      SbwLastMade=SbwTo-SbwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActSbw+SbwTo
      QuestSbwTo(id)=SbwTo
      SbwCutText(SbwTo)=CutText
      SbwBars(SbwTo)=BarType
      SbwStripes(SbwTo)=max(nstrip,1)
      call SetIntArrayTo(SbwItemSel(1,SbwTo),1000,0)
      call FeSbwMake(SbwTo,id,xw,yw,xdw,ydw,nstrip)
      SbwDoubleClickAllowed=.false.
      SbwRightClickAllowed=.false.
      go to 9999
      entry FeQuestSbwMenuOpen(iwa,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwMenuOpen(i)
      go to 9999
      entry FeQuestSbwSelectOpen(iwa,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwSelectOpen(i)
      go to 9999
      entry FeQuestSbwTextOpen(iwa,ly,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwTextOpen(i,ly)
      go to 9999
      entry FeQuestSbwPureOpen(iwa,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      i=iwa+SbwFr-1
      SbwFile(i)=' '
      call FeSbwPureOpen(i,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      go to 9999
      entry FeQuestSbwClose(iwa)
      if(iwa.le.0) go to 9999
      i=iwa+SbwFr-1
      call FeSbwClose(i)
      go to 9999
      entry FeQuestSbwShow(iwa,nn)
      i=iwa+SbwFr-1
      call FeSbwShow(i,nn)
      go to 9999
      entry FeQuestSbwItemOn(iwa,nn)
      i=iwa+SbwFr-1
      j=min(nn,SbwItemNMax(i))-SbwItemFrom(i)+1
      if(j.ge.1) call FeSbwItemOn(i,j)
      go to 9999
      entry FeQuestSbwItemOff(iwa,nn)
      i=iwa+SbwFr-1
      j=min(nn,SbwItemNMax(i))-SbwItemFrom(i)+1
      if(j.ge.1.and.nn.le.SbwItemNMax(i)) call FeSbwItemOff(i,j)
      go to 9999
      entry FeQuestSbwRemove(iwa)
      i=iwa+SbwFr-1
      call CloseIfOpened(SbwLn(i))
      SbwFile(i)=' '
      call FeSbwRemove(i)
      go to 9999
      entry FeQuestSbwActivate(iwa)
      i=iwa+SbwFr-1
      call FeSbwActivate(i)
      go to 9999
      entry FeQuestSbwDeactivate(iwa)
      i=iwa+SbwFr-1
      call FeSbwDeactivate(i)
9999  return
      end
      subroutine FeQuestSbwItemSelDel(iwa)
      include 'fepc.cmn'
      i=iwa+SbwFr-1
      if(SbwType(i).ne.SbwMenuType.and.
     1   SbwType(i).ne.SbwSelectType) go to 9999
      ItemFromOld=SbwItemFrom(i)
      ItemSelOld=SbwItemPointer(i)
      call CloseIfOpened(SbwLn(i))
      if(SbwType(i).eq.SbwMenuType) then
        call DeleteLinesFromFile(SbwFile(i),ItemSelOld,ItemSelOld)
        call FeSbwMenuOpen(i)
      else if(SbwType(i).eq.SbwSelectType) then
        if(SbwItemSelN(i).gt.0) then
          do j=SbwItemNMax(i),1,-1
            if(SbwItemSel(j,i).ne.0) then
              call DeleteLinesFromFile(SbwFile(i),j,j)
              SbwItemSel(j,i)=0
              if(j.le.ItemSelOld) ItemSelOld=ItemSelOld-1
            endif
          enddo
        else
          call DeleteLinesFromFile(SbwFile(i),ItemSelOld,ItemSelOld)
        endif
        call FeSbwSelectOpen(i)
      endif
      call FeSbwShow(i,ItemFromOld)
      call FeQuestSbwItemOff(iwa,SbwItemFrom(i))
9999  call FeQuestSbwItemOn(iwa,ItemSelOld)
      return
      end
      subroutine FeQuestRolMenuMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                              Check)
      include 'fepc.cmn'
      character*(*) Text,Menu(NMenu)
      character*1   Justify
      integer Check,MenuEnable(*)
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 2000
      entry FeQuestAbsRolMenuMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                            Check)
      yt=yti
      yw=ywi
2000  RolMenuTo=RolMenuTo+1
      RolMenuMax=RolMenuTo
      QuestRolMenuTo(id)=RolMenuTo
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActRolMenu+RolMenuTo
      RolMenuLastMade=RolMenuTo-RolMenuFr+1
      RolMenuCheck(RolMenuTo)=Check
      RolMenuLabel(RolMenuTo)=Text
      RolMenuJustify(RolMenuTo)=Justify
      RolMenuLabelX(RolMenuTo)=xt+QuestXMin(LastQuest)
      RolMenuLabelY(RolMenuTo)=yt+QuestYMin(LastQuest)
      ym=yw
      call FeRolMenuMake(RolMenuTo,id,xw,ym,xdw,ydw)
      go to 9999
      entry FeQuestRolMenuOpen(iwa,Menu,NMenu,KMenu)
      Klic=0
      go to 2050
      entry FeQuestRolMenuWithEnableOpen(iwa,Menu,MenuEnable,NMenu,
     1                                   KMenu)
      Klic=1
2050  iwp=iwa+RolMenuFr-1
      do i=1,NMenu
        RolMenuText(i,iwp)=Menu(i)
        if(Klic.eq.0) then
          RolMenuEnable(i,iwp)=1
        else
          RolMenuEnable(i,iwp)=MenuEnable(i)
        endif
      enddo
      RolMenuNumber  (iwp)=NMenu
      RolMenuSelected(iwp)=KMenu
      call FeRolMenuOpen(iwp)
      go to 9000
      entry FeQuestRolMenuLock(iwa)
      call FeRolMenuLock(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuDisable(iwa)
      call FeRolMenuDisable(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuClose(iwa)
      iwap=iwa+RolMenuFr-1
      if(RolMenuState(iwap).ne.RolMenuClosed)
     1  call FeQuestRolMenuLabelRemove(iwa)
      call FeRolMenuClose(iwap)
      go to 9999
      entry FeQuestRolMenuRemove(iwa)
      iwap=iwa+RolMenuFr-1
      if(RolMenuState(iwap).ne.RolMenuRemoved)
     1  call FeQuestRolMenuLabelRemove(iwa)
      call FeRolMenuRemove(iwap)
      go to 9000
      entry FeQuestRolMenuActivate(iwa)
      call FeRolMenuActivate(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuDeactivate(iwa)
      call FeRolMenuDeactivate(iwa+RolMenuFr-1)
9000  call FeQuestRolMenuLabelMake(iwa,RolMenuLabel(iwa+RolMenuFr-1))
9999  return
      end
      subroutine FeQuestRolMenuLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer Color
      Klic=0
      go to 1000
      entry FeQuestRolMenuLabelRemove(iwp)
      Klic=1
1000  iw=iwp+RolMenuFr-1
      if(RolMenuLabel(iw).ne.' ') then
        call FeTextErase(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                   RolMenuLabel(iw),RolMenuJustify(iw),LightGray)
      endif
      if(Klic.eq.0) then
        Color=Black
        go to 2000
      else
        go to 9999
      endif
      entry FeQuestRolMenuLabelMake(iwp,Text)
      iw=iwp+RolMenuFr-1
      if(RolMenuState(iw).eq.RolMenuDisabled) then
        Color=White
      else
        Color=Black
      endif
2000  RolMenuLabel(iw)=Text
      if(Text.ne.' ') then
        call FeGetTextRectangle(RolMenuLabelX(iw),RolMenuLabelY(iw),
     1    RolMenuLabel(iw),RolMenuJustify(iw),1,x1,x2,y1,y2,refx,refy,
     2    conx,cony)
        RolMenuLabelXMin(iw)=x1
        RolMenuLabelXMax(iw)=x2
        RolMenuLabelYMin(iw)=y1
        RolMenuLabelYMax(iw)=y2
        call FeTextErase(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                   RolMenuLabel(iw),RolMenuJustify(iw),LightGray)
        call FeOutStUnder(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                    RolMenuLabel(iw),RolMenuJustify(iw),Color,
     2                    Color,RolMenuZ(iw:iw))
      else
        RolMenuLabelXMin(iw)=0.
        RolMenuLabelXMax(iw)=0.
        RolMenuLabelYMin(iw)=0.
        RolMenuLabelYMax(iw)=0.
        RolMenuZ(iw:iw)=' '
      endif
9999  return
      end
      subroutine FeQuestGetFromEdw(iwa,IntArray,TextOut,RealArray,
     1                             Change)
      include 'fepc.cmn'
      dimension IntArray(*),RealArray(*)
      character*(*) TextOut
      logical   Change
      iwp=iwa+EdwFr-1
      go to 3400
      entry FeGetFromEdw(iwa,IntArray,TextOut,RealArray,Change)
      iwp=iwa
3400  if(EdwType(iwp).eq.0) then
        Change=TextOut.ne.EdwString(iwp)
        TextOut=EdwString(iwp)
      else
        Change=.false.
        do i=1,mod(iabs(EdwType(iwp)),10)
          if(EdwType(iwp).lt.0) then
            Change=Change.or.IntArray(i).ne.EdwInt(i,iwp)
            IntArray(i)=EdwInt(i,iwp)
          else
            Change=Change.or.RealArray(i).ne.EdwReal(i,iwp)
            RealArray(i)=EdwReal(i,iwp)
          endif
        enddo
      endif
      return
      end
      subroutine FeQuestIntAFromEdw(iw,X)
      include 'fepc.cmn'
      integer X(*)
      iwp=iw+EdwFr-1
      do i=1,iabs(EdwType(iwp))
        X(i)=EdwInt(i,iwp)
      enddo
      return
      end
      subroutine FeQuestIntFromEdw(iw,X)
      include 'fepc.cmn'
      integer X
      X=EdwInt(1,iw+EdwFr-1)
      return
      end
      subroutine FeQuestRealAFromEdw(iw,X)
      include 'fepc.cmn'
      dimension X(*)
      iwp=iw+EdwFr-1
      do i=1,EdwType(iwp)
        X(i)=EdwReal(i,iwp)
      enddo
      return
      end
      subroutine FeQuestRealFromEdw(iw,X)
      include 'fepc.cmn'
      X=EdwReal(1,iw+EdwFr-1)
      return
      end
      subroutine FeQuestRealAEdwOpen(iw,X,n,Prazdny,Zlomek)
      include 'fepc.cmn'
      dimension X(n),xp(1)
      integer Exponent10
      logical Prazdny,Zlomek
      call FeQuestEdwOpenInic(iw,iwp,n,Prazdny)
      do i=1,n
        if(Prazdny) then
          EdwReal(i,iwp)=0.
        else
          if(Zlomek) then
            call ToFract(X(i),Cislo,10)
          else
            Cislo(1:1)='?'
          endif
          if(Cislo(1:1).eq.'?') then
            if(abs(X(i)).gt.0.) then
              j=max(6-Exponent10(X(i)),0)
              j=min(j,6)
            else
              j=6
            endif
            write(RealFormat(6:7),'(i2)') j
            write(Cislo,RealFormat) X(i)
          endif
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          EdwReal(i,iwp)=xp(1)
          call ZdrcniCisla(Cislo,1)
          if(i.eq.1) then
            EdwString(iwp)=Cislo
          else
            EdwString(iwp)=EdwString(iwp)(:idel(EdwString(iwp)))//
     1                     ' '//Cislo
          endif
        endif
      enddo
      call FeEdwOpen(iwp)
      return
      end
      subroutine FeQuestRealEdwOpen(iw,X,Prazdny,Zlomek)
      include 'fepc.cmn'
      dimension xp(1)
      integer Exponent10
      logical   Prazdny,Zlomek
      call FeQuestEdwOpenInic(iw,iwp,1,Prazdny)
      if(Prazdny) then
        EdwReal(1,iwp)=0.
      else
        if(Zlomek) then
          call ToFract(X,Cislo,10)
        else
          Cislo(1:1)='?'
        endif
        if(Cislo(1:1).eq.'?') then
          if(abs(X).gt.0.) then
            j=max(6-Exponent10(X),0)
            j=min(j,6)
          else
            j=6
          endif
          write(RealFormat(6:7),'(i2)') j
          write(Cislo,RealFormat) X
        endif
        call ZdrcniCisla(Cislo,1)
        k=0
        call StToReal(Cislo,k,xp,1,.false.,ich)
        EdwReal(1,iwp)=xp(1)
        EdwString(iwp)=Cislo
      endif
      call FeEdwOpen(iwp)
      return
      end
      subroutine FeQuestIntAEdwOpen(iw,X,n,Prazdny)
      include 'fepc.cmn'
      integer X(n)
      logical   Prazdny
      call FeQuestEdwOpenInic(iw,iwp,-n,Prazdny)
      do i=1,n
        if(Prazdny) then
          EdwInt(i,iwp)=0
        else
          write(Cislo,FormI15) X(i)
          EdwInt(i,iwp)=X(i)
          call ZdrcniCisla(Cislo,1)
          if(i.eq.1) then
            EdwString(iwp)=Cislo
          else
            EdwString(iwp)=EdwString(iwp)(:idel(EdwString(iwp)))//
     1                     ' '//Cislo
          endif
        endif
      enddo
      call FeEdwOpen(iwp)
      return
      end
      subroutine FeQuestIntEdwOpen(iw,X,Prazdny)
      include 'fepc.cmn'
      integer X
      logical   Prazdny
      call FeQuestEdwOpenInic(iw,iwp,-1,Prazdny)
      if(Prazdny) then
        EdwInt(1,iwp)=0
      else
        EdwInt(1,iwp)=X
        write(Cislo,FormI15) X
        call ZdrcniCisla(Cislo,1)
        EdwString(iwp)=Cislo
      endif
      call FeEdwOpen(iwp)
      return
      end
      subroutine FeQuestStringEdwOpen(iw,String)
      include 'fepc.cmn'
      character*(*) String
      call FeQuestEdwOpenInic(iw,iwp,0,.false.)
      EdwString(iwp)=String
      call FeEdwOpen(iwp)
      return
      end
      subroutine FeQuestEdwLabelMake(id,iw)
      include 'fepc.cmn'
      if(EdwLabel(iw).ne.' ') then
        call FeGetTextRectangle(EdwLabelX(iw),EdwLabelY(iw),
     1    EdwLabel(iw),EdwJustify(iw),1,x1,x2,y1,y2,refx,refy,conx,
     2    cony)
        EdwLabelXMin(iw)=x1
        EdwLabelXMax(iw)=x2
        EdwLabelYMin(iw)=y1
        EdwLabelYMax(iw)=y2
        call FeTextErase(0,EdwLabelX(iw),EdwLabelY(iw),EdwLabel(iw),
     1                   EdwJustify(iw),LightGray)
        call FeOutStUnder(0,EdwLabelX(iw),EdwLabelY(iw),EdwLabel(iw),
     1                    EdwJustify(iw),Black,Black,EdwZ(iw:iw))
      else
        EdwLabelXMin(iw)=0.
        EdwLabelXMax(iw)=0.
        EdwLabelYMin(iw)=0.
        EdwLabelYMax(iw)=0.
        EdwZ(iw:iw)=' '
      endif
      return
      end
      subroutine FeQuestEdwOpenInic(iw,iwp,n,Prazdny)
      include 'fepc.cmn'
      logical Prazdny
      iwp=iw+EdwFr-1
      call FeQuestEdwLabelMake(LastQuest,iwp)
      EdwType(iwp)=n
      if(Prazdny) EdwString(iwp)=' '
      return
      end
      integer function ButtonStateQuest(i)
      include 'fepc.cmn'
      ButtonStateQuest=ButtonState(i+ButtonFr-1)
      return
      end
      integer function EdwStateQuest(i)
      include 'fepc.cmn'
      EdwStateQuest=EdwState(i+EdwFr-1)
      return
      end
      integer function LblStateQuest(i)
      include 'fepc.cmn'
      LblStateQuest=LblState(i+LblFr-1)
      return
      end
      integer function EdwSelectedQuest(i)
      include 'fepc.cmn'
      EdwSelectedQuest=EdwSelected(i+EdwFr-1)
      return
      end
      integer function CrwStateQuest(i)
      include 'fepc.cmn'
      CrwStateQuest=CrwState(i+CrwFr-1)
      return
      end
      integer function RolMenuStateQuest(i)
      include 'fepc.cmn'
      RolMenuStateQuest=RolMenuState(i+RolMenuFr-1)
      return
      end
      integer function RolMenuSelectedQuest(i)
      include 'fepc.cmn'
      RolMenuSelectedQuest=RolMenuSelected(i+RolMenuFr-1)
      return
      end
      integer function RolMenuNumberQuest(i)
      include 'fepc.cmn'
      RolMenuNumberQuest=RolMenuNumber(i+RolMenuFr-1)
      return
      end
      function RolMenuTextQuest(n,i)
      include 'fepc.cmn'
      character*80 RolMenuTextQuest
      RolMenuTextQuest=RolMenuText(n,i+RolMenuFr-1)
      return
      end
      subroutine FeQuestSetSbwItemSel(i,j,k)
      include 'fepc.cmn'
      SbwItemSel(i,j+SbwFr-1)=k
      return
      end
      subroutine FeQuestSetSbwItemSelN(j,k)
      include 'fepc.cmn'
      SbwItemSelN(j+SbwFr-1)=k
      return
      end
      function SbwXMinQuest(i)
      include 'fepc.cmn'
      SbwXMinQuest=SbwXMin(i+SbwFr-1)
      return
      end
      function SbwXMaxQuest(i)
      include 'fepc.cmn'
      SbwXMaxQuest=SbwXMax(i+SbwFr-1)
      return
      end
      function SbwXMinPruhQuest(j,i)
      include 'fepc.cmn'
      SbwXMinPruhQuest=SbwXMinPruh(j,i+SbwFr-1)
      return
      end
      function SbwXMaxPruhQuest(j,i)
      include 'fepc.cmn'
      SbwXMaxPruhQuest=SbwXMaxPruh(j,i+SbwFr-1)
      return
      end
      function SbwYMinQuest(i)
      include 'fepc.cmn'
      SbwYMinQuest=SbwYMin(i+SbwFr-1)
      return
      end
      function SbwYMaxQuest(i)
      include 'fepc.cmn'
      SbwYMaxQuest=SbwYMax(i+SbwFr-1)
      return
      end
      function SbwYStepQuest(i)
      include 'fepc.cmn'
      SbwYStepQuest=SbwYStep(i+SbwFr-1)
      return
      end
      integer function SbwItemSelQuest(j,i)
      include 'fepc.cmn'
      SbwItemSelQuest=SbwItemSel(j,i+SbwFr-1)
      return
      end
      integer function SbwLnQuest(i)
      include 'fepc.cmn'
      SbwLnQuest=SbwLn(i+SbwFr-1)
      return
      end
      integer function SbwItemPointerQuest(i)
      include 'fepc.cmn'
      SbwItemPointerQuest=SbwItemPointer(i+SbwFr-1)
      return
      end
      integer function SbwItemFromQuest(i)
      include 'fepc.cmn'
      SbwItemFromQuest=SbwItemFrom(i+SbwFr-1)
      return
      end
      integer function SbwItemToQuest(i)
      include 'fepc.cmn'
      SbwItemToQuest=SbwItemTo(i+SbwFr-1)
      return
      end
      integer function SbwLinesQuest(i)
      include 'fepc.cmn'
      SbwLinesQuest=SbwLines(i+SbwFr-1)
      return
      end
      integer function SbwItemNMaxQuest(i)
      include 'fepc.cmn'
      SbwItemNMaxQuest=SbwItemNMax(i+SbwFr-1)
      return
      end
      integer function SbwItemSelNQuest(i)
      include 'fepc.cmn'
      SbwItemSelNQuest=SbwItemSelN(i+SbwFr-1)
      return
      end
      function SbwFileQuest(i)
      include 'fepc.cmn'
      character*256 SbwFileQuest
      SbwFileQuest=SbwFile(i+SbwFr-1)
      return
      end
      function SbwStringQuest(n,m)
      include 'fepc.cmn'
      character*256 SbwStringQuest
      id=m+SbwFr-1
      if(n.gt.SbwItemNMax(id)) then
        SbwStringQuest=' '
        go to 9999
      endif
      rewind SbwLn(id)
      do i=1,n
        read(SbwLn(id),FormA) SbwStringQuest
      enddo
9999  return
      end
      logical function CrwLogicQuest(i)
      include 'fepc.cmn'
      CrwLogicQuest=CrwLogic(i+CrwFr-1)
      return
      end
      real function EdwRealQuest(i,j)
      include 'fepc.cmn'
      EdwRealQuest=EdwReal(i,j+EdwFr-1)
      return
      end
      integer function EdwIntQuest(i,j)
      include 'fepc.cmn'
      EdwIntQuest=EdwInt(i,j+EdwFr-1)
      return
      end
      function EdwStringQuest(i)
      include 'fepc.cmn'
      character*256 EdwStringQuest
      EdwStringQuest=EdwString(i+EdwFr-1)
      return
      end
      function EdwXMinQuest(i)
      include 'fepc.cmn'
      EdwXMinQuest=EdwXMin(i+EdwFr-1)
      return
      end
      function EdwXMaxQuest(i)
      include 'fepc.cmn'
      EdwXMaxQuest=EdwXMax(i+EdwFr-1)
      return
      end
      function EdwUpDownXMinQuest(i)
      include 'fepc.cmn'
      EdwUpDownXMinQuest=EdwUpDownXMin(i+EdwFr-1)
      return
      end
      function EdwUpDownXMaxQuest(i)
      include 'fepc.cmn'
      EdwUpDownXMaxQuest=EdwUpDownXMax(i+EdwFr-1)
      return
      end
      function EdwYMinQuest(i)
      include 'fepc.cmn'
      EdwYMinQuest=EdwYMin(i+EdwFr-1)
      return
      end
      function EdwYMaxQuest(i)
      include 'fepc.cmn'
      EdwYMaxQuest=EdwYMax(i+EdwFr-1)
      return
      end
      function EdwLabelYQuest(i)
      include 'fepc.cmn'
      EdwLabelYQuest=EdwLabelY(i+EdwFr-1)
      return
      end
      function RolMenuTextXMinQuest(i)
      include 'fepc.cmn'
      RolMenuTextXMinQuest=RolMenuTextXMin(i+RolMenuFr-1)
      return
      end
      function RolMenuTextXMaxQuest(i)
      include 'fepc.cmn'
      RolMenuTextXMaxQuest=RolMenuTextXMax(i+RolMenuFr-1)
      return
      end
      function RolMenuTextYMinQuest(i)
      include 'fepc.cmn'
      RolMenuTextYMinQuest=RolMenuTextYMin(i+RolMenuFr-1)
      return
      end
      function RolMenuTextYMaxQuest(i)
      include 'fepc.cmn'
      RolMenuTextYMaxQuest=RolMenuTextYMax(i+RolMenuFr-1)
      return
      end
      function RolMenuLabelXQuest(i)
      include 'fepc.cmn'
      RolMenuLabelXQuest=RolMenuLabelX(i+RolMenuFr-1)
      return
      end
      function RolMenuLabelYQuest(i)
      include 'fepc.cmn'
      RolMenuLabelYQuest=RolMenuLabelY(i+RolMenuFr-1)
      return
      end
      function CrwXMinQuest(i)
      include 'fepc.cmn'
      CrwXMinQuest=CrwXMin(i+CrwFr-1)
      return
      end
      function CrwXMaxQuest(i)
      include 'fepc.cmn'
      CrwXMaxQuest=CrwXMax(i+CrwFr-1)
      return
      end
      function CrwYMinQuest(i)
      include 'fepc.cmn'
      CrwYMinQuest=CrwYMin(i+CrwFr-1)
      return
      end
      function CrwYMaxQuest(i)
      include 'fepc.cmn'
      CrwYMaxQuest=CrwYMax(i+CrwFr-1)
      return
      end
      function ButtonXMinQuest(i)
      include 'fepc.cmn'
      ButtonXMinQuest=ButtonXMin(i+ButtonFr-1)
      return
      end
      function ButtonXMaxQuest(i)
      include 'fepc.cmn'
      ButtonXMaxQuest=ButtonXMax(i+ButtonFr-1)
      return
      end
      function ButtonYMinQuest(i)
      include 'fepc.cmn'
      ButtonYMinQuest=ButtonYMin(i+ButtonFr-1)
      return
      end
      function ButtonYMaxQuest(i)
      include 'fepc.cmn'
      ButtonYMaxQuest=ButtonYMax(i+ButtonFr-1)
      return
      end
      subroutine FeDrawSwitch(xmns,xmxs,ymns,ymxs,npix)
      include 'fepc.cmn'
      dimension xp(8),yp(8),xpp(8),ypp(8),xc(8),yc(8)
      fpix=npix
      dpix=2.*fpix
      xp(1)=xmns
      yp(1)=ymns
      xp(2)=xp(1)
      yp(2)=ymxs-dpix
      xp(3)=xmns+dpix
      yp(3)=ymxs
      xp(4)=xmxs-dpix
      yp(4)=ymxs
      xp(5)=xp(4)-fpix+1.
      yp(5)=ymxs-fpix+1.
      xp(6)=xmns+(dpix+fpix)-1.
      yp(6)=yp(5)
      xp(7)=xmns+fpix-1.
      yp(7)=ymxs-(dpix+fpix)+1.
      xp(8)=xp(7)
      yp(8)=yp(1)
      xpp(1)=xmxs
      ypp(1)=ymns
      xpp(2)=xpp(1)
      ypp(2)=ymxs-dpix
      xpp(3)=xp(4)
      ypp(3)=yp(4)
      xpp(4)=xp(5)
      ypp(4)=yp(5)
      xpp(5)=xmxs-fpix+1.
      ypp(5)=ymxs-(dpix+fpix)+1.
      xpp(6)=xpp(5)
      ypp(6)=ymns
      xc(1)=xpp(6)
      yc(1)=ypp(6)
      xc(2)=xpp(5)
      yc(2)=ypp(5)
      xc(3)=xpp(4)
      yc(3)=ypp(4)
      xc(4)=xp(6)
      yc(4)=yp(6)
      xc(5)=xp(7)
      yc(5)=yp(7)
      xc(6)=xp(8)
      yc(6)=yp(8)
      if(OpSystem.eq.1) then
        do i=1,8
          xp(i)=xp(i)
          yp(i)=yp(i)
        enddo
        do i=1,6
          xpp(i)=xpp(i)
          ypp(i)=ypp(i)
          xc(i)=xc(i)
          yc(i)=yc(i)
        enddo
      endif
      call FePolygon(xc,yc,6,4,0,0,LightGray)
      call FePolygon(xp,yp,8,4,0,0,White)
      call FePolygon(xpp,ypp,6,4,0,0,Gray)
      return
      end
      subroutine FeDrawPixFrame(xmi,xma,ymi,yma,klic)
C klic = 1 ... vnejsi ram s ostrymi rohy
C klic = 2 ... vnejsi ram s kulatymi rohy
C klic = 3 ... line
C klic = 4 ... ram uvnitr plochy
      include 'fepc.cmn'
      real px(5),py(5),xmi,xma,ymi,yma,ppx(2),ppy(2)
      px(1)=xmi
      py(1)=ymi
      px(2)=xmi
      py(2)=yma
      px(3)=xma
      py(3)=yma
      px(4)=xma
      py(4)=ymi
      px(5)=px(1)
      py(5)=py(1)
      if(klic.eq.1) then
        call FePolyLine(3,px(1),py(1),LightGray)
        call FePolyLine(3,px(3),py(3),Black)
        do i=1,5
          if(px(i).eq.xmi) then
            px(i)=px(i)+1.
          else
            px(i)=px(i)-1.
          endif
          if(py(i).eq.ymi) then
            py(i)=py(i)+1.
          else
            py(i)=py(i)-1.
          endif
        enddo
        call FePolyLine(3,px(1),py(1),White)
        call FePolyLine(3,px(3),py(3),Gray)
      else if(klic.eq.2) then
        ppx(1)=px(1)
        ppx(2)=ppx(1)
        ppy(1)=py(1)+2.
        ppy(2)=py(2)-2.
        call FePolyLine(2,ppx,ppy,White)
        ppx(1)=ppx(1)+1.
        ppx(2)=ppx(2)+1.
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(2)+1.
        call FePolyLine(2,ppx,ppy,White)
        ppy(1)=ppy(1)+1.
        ppy(2)=ppy(2)-1.
        call FePolyLine(2,ppx,ppy,LightGray)
        ppx(1)=px(4)
        ppx(2)=px(4)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(2)+1.
        ppx(1)=ppx(1)-1.
        ppx(2)=ppx(1)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(1)=ppy(1)+1.
        ppy(2)=ppy(2)-1.
        call FePolyLine(2,ppx,ppy,Gray)
        ppx(1)=px(1)+2.
        ppx(2)=px(4)-2.
        ppy(1)=py(2)
        ppy(2)=py(2)
        call FePolyLine(2,ppx,ppy,White)
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(1)
        call FePolyLine(2,ppx,ppy,LightGray)
        ppy(1)=py(1)
        ppy(2)=ppy(1)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(2)=ppy(2)+1.
        ppy(1)=ppy(2)
        call FePolyLine(2,ppx,ppy,Gray)
      else if(klic.eq.3) then
        px(1)=xmi
        py(1)=ymi
        px(2)=xma
        py(2)=yma
        call FePolyLine(2,px,py,Gray)
        py(1)=py(1)-1.
        py(2)=py(2)-1.
        call FePolyLine(2,px,py,White)
      else
        call FeChybne(-1.,-1.,'Spatny klic',' ',SeriousError)
      endif
      return
      end
      subroutine FeTwoPixLineHoriz(xmi,xma,y,ColorUp,ColorDown)
      include 'fepc.cmn'
      integer ColorUp,ColorDown
      xu(1)=xmi
      xu(2)=xma
      yu(1)=y
      yu(2)=yu(1)
      call FePolyLine(2,xu,yu,ColorUp)
      yu(1)=yu(1)-1.
      yu(2)=yu(1)
      call FePolyLine(2,xu,yu,ColorDown)
      return
      end
      subroutine FeTwoPixLineVertic(x,ymi,yma,ColorLeft,ColorRight)
      include 'fepc.cmn'
      integer ColorLeft,ColorRight
      Klic=2
      go to 1100
      entry FeOnePixLineVertic(x,ymi,yma,ColorLeft,ColorRight)
      Klic=1
1100  yu(1)=ymi
      yu(2)=yma
      xu(1)=x
      xu(2)=xu(1)
      call FePolyLine(2,xu,yu,ColorLeft)
      if(Klic.gt.1) then
        xu(1)=xu(1)-1.
        xu(2)=xu(1)
        call FePolyLine(2,xu,yu,ColorRight)
      endif
      return
      end
      subroutine FeKartRefresh(klic)
      include 'fepc.cmn'
      integer ButtonStateSave(mxdbutt)
      save NKartSave,KartNDolSave,ButtonStateSave
      if(klic.eq.0) then
        if(KartOn) then
          NKartSave=NKart
          KartNDolSave=KartNDol
          do i=1,KartNDol
            ButtonStateSave(i)=ButtonState(KartDolButt(i))
          enddo
          KartNDol=0
          NKart=0
        endif
      else
        NKart=NKartSave
        KartNDol=KartNDolSave
        do i=1,KartNDol
          ButtonState(KartDolButt(i))=ButtonStateSave(i)
        enddo
      endif
      KartOn=klic.ne.0
      return
      end
      subroutine FeSmytec
      include 'fepc.cmn'
      call DeletePomFiles
      call FeTmpFilesDelete
      call FeGrQuit
      stop
      end
      subroutine FePause
      include 'fepc.cmn'
      itype=EventType
      inumber=EventNumber
500   call FeEvent(0)
      if(EventType.ne.EventMouse.or.EventNumber.ne.JeLeftDown) go to 500
      EventType=itype
      EventNumber=inumber
      if(EventType.eq.EventEdw) then
        EventNumberAbs=EventNumber+EdwFr-1
      else if(EventType.eq.EventButton) then
        EventNumberAbs=EventNumber+ButtonFr-1
      else if(EventType.eq.EventCrw) then
        EventNumberAbs=EventNumber+CrwFr-1
      endif
      return
      end
      subroutine CreateTmpFileName(FileName,id)
      include 'fepc.cmn'
      character*(*) FileName
      character*4   t4
      write(t4,'(i4)') id
      call zhusti(t4)
      FileName=FileName(:idel(FileName))//t4
      return
      end
      subroutine FeTmpFileName
      include 'fepc.cmn'
      character*(*) TmpFileName
      character*256 TmpFileNameArr(100)
      logical EqIgCase
      data TmpFileNameArr/100*' '/
      entry FeTmpFilesAdd(TmpFileName)
      do i=1,100
        if(EqIgCase(TmpFileNameArr(i),' ')) then
          TmpFileNameArr(i)=TmpFileName
          go to 9999
        endif
      enddo
      go to 9999
      entry FeTmpFilesClear(TmpFileName)
      do i=1,100
        if(EqIgCase(TmpFileNameArr(i),TmpFileName)) then
          TmpFileNameArr(i)=' '
          go to 9999
        endif
      enddo
      go to 9999
      entry FeTmpFilesDelete
      do i=1,100
        if(.not.EqIgCase(TmpFileNameArr(i),' ')) then
          if(index(TmpFileNameArr(i),'*').gt.0.or.
     1       index(TmpFileNameArr(i),'?').gt.0) then
            call DeleteAllFiles('"'//
     1        TmpFileNameArr(i)(:idel(TmpFileNameArr(i)))//'"')
          else
            call DeleteFile(TmpFileNameArr(i))
          endif
        endif
      enddo
9999  return
      end
      subroutine FeFrToRefresh(id)
      include 'fepc.cmn'
      if(id.eq.0) then
        EdwFr=1
        EdwTo=mxedw
        CrwFr=1
        CrwTo=mxcrw
        ButtonFr=1
        ButtonTo=mxbut
        SbwFr=1
        SbwTo=MxSbw
        LblFr=1
        LblTo=MxLbl
        LinkaFr=1
        SvisliceFr=1
        LinkaTo=mxLinka
        SvisliceTo=mxSvislice
        RolMenuFr=1
        RolMenuTo=MxRolMenu
      else if(id.gt.0) then
        EdwFr=QuestEdwFr(id)
        EdwTo=QuestEdwTo(id)
        CrwFr=QuestCrwFr(id)
        CrwTo=QuestCrwTo(id)
        ButtonFr=QuestButtonFr(id)
        ButtonTo=QuestButtonTo(id)
        SbwFr=QuestSbwFr(id)
        SbwTo=QuestSbwTo(id)
        LblFr=QuestLblFr(id)
        LblTo=QuestLblTo(id)
        LinkaFr=QuestLinkaFr(id)
        LinkaTo=QuestLinkaTo(id)
        SvisliceFr=QuestSvisliceFr(id)
        SvisliceTo=QuestSvisliceTo(id)
        RolMenuTo=QuestRolMenuTo(id)
        RolMenuFr=QuestRolMenuFr(id)
        ActiveObjFr=QuestActiveObjFr(id)
        ActiveObjTo=QuestActiveObjTo(id)
        ActiveObj=QuestActiveObj(id)
        EdwNAll=QuestEdwNAll(id)
        EdwActive=QuestEdwActive(id)
        EdwLastActive=EdwActive-EdwFr+1
        SbwActive=QuestSbwActive(id)
        LastEdw=EdwActive
        KurzorEdw=EdwActive
        EventType=QuestEventType(id)
        EventNumber=QuestEventNumber(id)
        EventNumberAbs=QuestEventNumberAbs(id)
        CheckType=QuestCheckType(id)
        CheckNumber=QuestCheckNumber(id)
        CheckNumberAbs=QuestCheckNumberAbs(id)
        EventTypeSave=QuestEventTypeSave(id)
        EventNumberSave=QuestEventNumberSave(id)
        ButtonOk=QuestButtonOk(id)
        ButtonEsc=QuestButtonEsc(id)
        ButtonCancel=QuestButtonCancel(id)
        CheckMouse=QuestCheckMouse(id)
        TakeMouseMove=QuestTakeMouseMove(id)
        AllowChangeMouse=QuestAllowChangeMouse(id)
        CheckKeyBoard=QuestCheckKeyBoard(id)
      else
        EdwFr=1
        EdwTo=0
        CrwFr=1
        CrwTo=0
        ButtonFr=1
        ButtonTo=0
        SbwFr=1
        SbwTo=0
        LblFr=1
        LblTo=0
        LinkaFr=1
        LinkaTo=0
        SvisliceFr=1
        SvisliceTo=0
        RolMenuFr=1
        RolMenuTo=0
        ActiveObjFr=1
        ActiveObjTo=0
      endif
      return
      end
      subroutine FeFrToChange(IdOld,IdNew)
      include 'fepc.cmn'
      if(IdOld.gt.0) then
        QuestEdwFr(IdOld)=EdwFr
        QuestEdwTo(IdOld)=EdwTo
        QuestCrwFr(IdOld)=CrwFr
        QuestCrwTo(IdOld)=CrwTo
        QuestButtonFr(IdOld)=ButtonFr
        QuestButtonTo(IdOld)=ButtonTo
        QuestSbwFr(IdOld)=SbwFr
        QuestSbwTo(IdOld)=SbwTo
        QuestLblFr(IdOld)=LblFr
        QuestLblTo(IdOld)=LblTo
        QuestLinkaFr(IdOld)=LinkaFr
        QuestLinkaTo(IdOld)=LinkaTo
        QuestSvisliceFr(IdOld)=SvisliceFr
        QuestSvisliceTo(IdOld)=SvisliceTo
        QuestRolMenuFr(IdOld)=RolMenuFr
        QuestRolMenuTo(IdOld)=RolMenuTo
        QuestActiveObjFr(IdOld)=ActiveObjFr
        QuestActiveObjTo(IdOld)=ActiveObjTo
        QuestActiveObj(IdOld)=ActiveObj
        QuestEdwNAll(IdOld)=EdwNAll
        QuestEdwActive(IdOld)=EdwActive
        QuestSbwActive(IdOld)=SbwActive
        QuestEventType(IdOld)=EventType
        QuestEventNumber(IdOld)=EventNumber
        QuestEventNumberAbs(IdOld)=EventNumberAbs
        QuestEventTypeSave(IdOld)=EventTypeSave
        QuestEventNumberSave(IdOld)=EventNumberSave
        QuestCheckType(IdOld)=CheckType
        QuestCheckNumber(IdOld)=CheckNumber
        QuestCheckNumberAbs(IdOld)=CheckNumberAbs
        QuestButtonOk(IdOld)=ButtonOk
        QuestButtonCancel(IdOld)=ButtonCancel
        QuestButtonEsc(IdOld)=ButtonEsc
        QuestWizardMode(IdOld)=WizardMode
        QuestCheckMouse(IdOld)=CheckMouse
        QuestTakeMouseMove(IdOld)=TakeMouseMove
        QuestAllowChangeMouse(IdOld)=AllowChangeMouse
        QuestCheckKeyBoard(IdOld)=CheckKeyBoard
      endif
      if(IdNew.gt.0) then
        EdwFr=QuestEdwFr(IdNew)
        EdwTo=QuestEdwTo(IdNew)
        CrwFr=QuestCrwFr(IdNew)
        CrwTo=QuestCrwTo(IdNew)
        ButtonFr=QuestButtonFr(IdNew)
        ButtonTo=QuestButtonTo(IdNew)
        SbwFr=QuestSbwFr(IdNew)
        SbwTo=QuestSbwTo(IdNew)
        LblFr=QuestLblFr(IdNew)
        LblTo=QuestLblTo(IdNew)
        LinkaFr=QuestLinkaFr(IdNew)
        LinkaTo=QuestLinkaTo(IdNew)
        SvisliceFr=QuestSvisliceFr(IdNew)
        SvisliceTo=QuestSvisliceTo(IdNew)
        RolMenuFr=QuestRolMenuFr(IdNew)
        RolMenuTo=QuestRolMenuTo(IdNew)
        ActiveObjFr=QuestActiveObjFr(IdNew)
        ActiveObjTo=QuestActiveObjTo(IdNew)
        ActiveObj=QuestActiveObj(IdNew)
        EdwNAll=QuestEdwNAll(IdNew)
        EdwActive=QuestEdwActive(IdNew)
        EdwLastActive=EdwActive-EdwFr+1
        SbwActive=QuestSbwActive(IdNew)
        LastEdw=EdwActive
        KurzorEdw=EdwActive
        EventType=QuestEventType(IdNew)
        EventNumber=QuestEventNumber(IdNew)
        EventNumberAbs=QuestEventNumberAbs(IdNew)
        CheckType=QuestCheckType(IdNew)
        CheckNumber=QuestCheckNumber(IdNew)
        CheckNumberAbs=QuestCheckNumberAbs(IdNew)
        EventTypeSave=QuestEventTypeSave(IdNew)
        EventNumberSave=QuestEventNumberSave(IdNew)
        ButtonOk=QuestButtonOk(IdNew)
        ButtonEsc=QuestButtonEsc(IdNew)
        ButtonCancel=QuestButtonCancel(IdNew)
        WizardMode=QuestWizardMode(IdNew)
        CheckMouse=QuestCheckMouse(IdNew)
        TakeMouseMove=QuestTakeMouseMove(IdNew)
        AllowChangeMouse=QuestAllowChangeMouse(IdNew)
        CheckKeyBoard=QuestCheckKeyBoard(IdNew)
      endif
      return
      end
      function NextQuestId()
      include 'fepc.cmn'
      if(WizardMode) then
        call FeQuestReset(WizardId)
        xm=QuestXMin(WizardId)
        xdp=QuestXLen(WizardId)
        ym=QuestYMin(WizardId)
        yd=QuestYLen(WizardId)
        if(KartOn) then
          call FeFillRectangle(xm,xm+xdp,ym,ym+yd,4,0,0,LightGray)
        else if(QuestColorFill(WizardId).gt.0) then
          call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                         QuestColorFill(WizardId))
        endif
        if(ButtonOk.gt.0) then
          if(ButtonState(ButtonOk).ne.ButtonClosed) then
            ButtonState(ButtonOk)=ButtonOn
            call FeButtonOff(ButtonOk)
          endif
        endif
        if(ButtonEsc.gt.0) then
          if(ButtonState(ButtonEsc).ne.ButtonClosed) then
            ButtonState(ButtonEsc)=ButtonOn
            call FeButtonOff(ButtonEsc)
          endif
        endif
        if(ButtonCancel.gt.0) then
          if(ButtonState(ButtonCancel).ne.ButtonClosed) then
            ButtonState(ButtonCancel)=ButtonOn
            call FeButtonOff(ButtonCancel)
          endif
        endif
        NextQuestId=WizardId
        QuestCheck(WizardId)=0
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)
        EventTypeSave=0
        EventNumberSave=0
      else
        do i=1,mxquest
          if(QuestState(i).eq.0) then
            NextQuestId=i
            go to 2000
          endif
        enddo
        NextQuestId=0
      endif
2000  return
      end
      function QuestYPosition(id,n)
      include 'fepc.cmn'
      if(n.ge.0) then
        if(n.gt.QuestLines(id).and.QuestLines(id).gt.0) then
          QuestYPosition=20.
        else
          if(n.eq.0) then
            i=0
          else
            i=n-1
            if(QuestText(id).ne.' ') i=i+1
          endif
          QuestYPosition=-QuestLineWidth*(float(i)+.5)
        endif
      else
        i=n+10
        if(QuestText(id).ne.' ') i=i-10
        QuestYPosition=-QuestLineWidth*(-float(i)*.1+.5)
      endif
      QuestYPosition=QuestLength(id)+QuestYPosition
      if(QuestText(id).eq.' ') QuestYPosition=QuestYPosition-5.
      return
      end
      subroutine NebylOsetren
      include 'fepc.cmn'
      Ninfo=1
      write(TextInfo(1),'(''Type : '',i3,'' Number : '',i3)')
     1        CheckType,CheckNumber
      call FeInfoOut(-1.,-1.,'Kontrolni bod Questu nebyl osetren','L')
      return
      end
      subroutine FeCheckStringEdw(iw,ich)
      include 'fepc.cmn'
      ich=0
      if(iw.eq.0) go to 9999
      if(EdwType(iw).gt.0) then
        k=0
        call StToReal(EdwString(iw),k,EdwReal(1,iw),
     1                mod(EdwType(iw),10),.true.,ich)
      else if(EdwType(iw).lt.0) then
        k=0
        call StToInt(EdwString(iw),k,EdwInt(1,iw),-EdwType(iw),.true.,
     1               ich)
      endif
      if(ich.eq.0) then
        if(EdwUpDown(iw).ne.0) call FeCheckEud(iw,0)
        if(EdwCheck(iw).ne.0.and.EventType.ne.EventMouse) then
          CheckType=EventEdw
          CheckNumber=iw-EdwFr+1
          ich=2
        endif
      else
        EventType=EventEdw
        EventNumber=iw-EdwFr+1
        EventNumberAbs=iw
        KurzorClick=-1
        ich=1
      endif
9999  return
      end
      subroutine FeCheckEud(iw,iz)
      include 'fepc.cmn'
      integer Exponent10
      if(EdwType(iw).lt.0) then
        n=nint(float(EdwInt(1,iw)-EdwInt(2,iw))/float(EdwInt(4,iw)))+iz
        EdwInt(5,iw)=nint(float(EdwInt(3,iw)-EdwInt(2,iw))/
     1                    float(EdwInt(4,iw)))
        n=min(n,EdwInt(5,iw))
        n=max(n,0)
        EdwInt(1,iw)=EdwInt(2,iw)+n*EdwInt(4,iw)
        write(EdwString(iw),FormI15) EdwInt(1,iw)
      else if(EdwType(iw).gt.0) then
        pom=max(abs(EdwReal(2,iw)),abs(EdwReal(3,iw)))
        if(pom.gt.0.) then
          j=max(6-Exponent10(pom),0)
          j=min(j,6)
        else
          j=6
        endif
        pom=EdwReal(1,iw)+float(iz)*EdwReal(4,iw)
1100    if(pom.gt.EdwReal(3,iw)+EdwReal(4,iw)*.5) then
          pom=pom-EdwReal(4,iw)
          go to 1100
        endif
1200    if(pom.lt.EdwReal(2,iw)-EdwReal(4,iw)*.5) then
          pom=pom+EdwReal(4,iw)
          go to 1200
        endif
        EdwReal(1,iw)=pom
        if(EventType.eq.EventEdwUp.or.EventType.eq.EventEdwDown.or.
     1     (EventType.eq.EventKey.and.
     2      (EventNumber.eq.JeUp.or.EventNumber.eq.JeDown))) then
          write(RealFormat(6:7),'(i2)') j
          write(EdwString(iw),RealFormat) EdwReal(1,iw)
        else
          Cislo=EdwString(iw)
          call posun(Cislo,1)
          read(Cislo,'(f15.0)') EdwReal(1,iw)
        endif
      endif
      call ZdrcniCisla(EdwString(iw),1)
      Kurzor=idel(EdwString(iw))
      EdwTextLen(iw)=Kurzor
      if(iz.eq.0) then
        j=0
      else
        j=1
      endif
      call FeEdwOpen(iw)
      if(.not.BlockedActiveObj) ActiveObj=ActEdw*10000+iw
      call FeEudOff(iw,1)
      call FeEudOff(iw,2)
      return
      end
      subroutine FeEdw
      include 'fepc.cmn'
      integer ColorSel
      logical PrepisEdw,PrepisKurzoru
      character*1 ChKey
      go to 9999
      entry FeEdwMake(id,idc,xm,ym,xd,yd)
      EdwXMin(id)=xm
      EdwXMax(id)=xm+xd
      EdwYMin(id)=ym
      EdwYMax(id)=ym+yd
      if(idc.gt.0) then
        EdwXMin(id)=EdwXMin(id)+QuestXMin(idc)
        EdwXMax(id)=EdwXMax(id)+QuestXMin(idc)
        EdwYMin(id)=EdwYMin(id)+QuestYMin(idc)
        EdwYMax(id)=EdwYMax(id)+QuestYMin(idc)
      endif
      EdwTextXMin(id)=EdwXMin(id)+EdwMarginSize
      EdwTextXMax(id)=EdwXMax(id)-EdwMarginSize
      EdwTextXLen(id)=EdwTextXMax(id)-EdwTextXMin(id)
      EdwState(id)=EdwClosed
      LastEdw=0
      EdwSelected(id)=0
      go to 9999
      entry FeEdwActivate(id)
      PrepisKurzoru=.true.
      ColorSel=Black
      go to 1500
      entry FeEdwDeactivate(id)
      PrepisKurzoru=.false.
      ColorSel=LightGray
1500  if(EdwLabel(id).ne.' ') then
        x1=EdwLabelXMin(id)
        x2=EdwLabelXMax(id)
        y1=EdwLabelYMin(id)
        y2=EdwLabelYMax(id)
        xu(1)=x1-1.
        yu(1)=y1-1.
        xu(2)=xu(1)
        yu(2)=y2+1.
        call FeLineType(DenseDottedLine)
        call FePolyLine(2,xu,yu,ColorSel)
        xu(3)=x2+1.
        xu(4)=xu(3)
        call FePolyLine(2,xu(3),yu,ColorSel)
        xu(2)=xu(3)
        yu(3)=yu(1)
        yu(4)=yu(1)
        call FePolyLine(2,xu,yu(3),ColorSel)
        yu(1)=yu(2)
        call FePolyLine(2,xu,yu,ColorSel)
        call FeLineType(NormalLine)
      endif
      if(EdwState(id).ne.EdwClosed.and.EdwState(id).ne.EdwRemoved) then
        PrepisEdw=.true.
        KurzorEdw=0
        Kurzor=EdwKurzor(id)
        go to 4000
      else
        go to 9999
      endif
      entry FeEdwOpen(id)
      if(id.ne.LastEdw) then
        if(LastEdw.ne.0) call FeEdwHighlight(LastEdw)
        EdwSelFr=0
        EdwSelTo=0
      endif
      call FeEdwBasic(id,White)
      EdwState(id)=EdwOpened
      LastEdw=0
      if(id.eq.KurzorEdw) KurzorEdw=0
      PrepisKurzoru=id.eq.EdwActive
      PrepisEdw=.true.
      EdwTextLen(id)=idel(EdwString(id))
      call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),1,1,
     1  EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      if(EdwTextLen(id).gt.EdwTextMax(id))
     1  call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2    EdwTextLen(id),-1,EdwTextXLen(id),EdwTextMin(id),
     3    EdwTextMax(id))
      Kurzor=EdwTextMax(id)
      EdwKurzor(id)=Kurzor
      go to 4000
      entry FeEdwMakeAction(id)
      if(id.le.0) go to 9999
      Kurzor=EdwKurzor(id)
      if(KurzorClick.ge.0.or.id.ne.LastEdw) then
        if(LastEdw.ne.0) call FeEdwHighlight(LastEdw)
        EdwSelFr=0
        EdwSelTo=0
      endif
      PrepisKurzoru=.true.
      PrepisEdw=.true.
      if(EventType.eq.EventEdw) then
        if(KurzorClick.ge.0) then
          if(EventNumberAbs.eq.id) then
            Kurzor=min(KurzorClick,EdwTextLen(id))
            KurzorClick=-1
          else
            go to 2500
          endif
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
        go to 2500
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        go to 9999
      else if(EventType.eq.EventASCII.and.
     1        (EdwTextLen(id).lt.256.or..not.InsertMode)) then
        if(EdwSelTo.ne.EdwSelFr) call FeEdwSelDelete(id)
        Key=EventNumber
        ChKey=char(Key)
        if(Kurzor.lt.EdwTextLen(id)) then
          if(InsertMode) then
            EdwString(id)=EdwString(id)(1:Kurzor)//ChKey//
     1                    EdwString(id)(Kurzor+1:EdwTextLen(id))
          else
            EdwString(id)(Kurzor+1:Kurzor+1)=ChKey
          endif
        else
          EdwString(id)=EdwString(id)(:Kurzor)//ChKey
        endif
        if(InsertMode.or.Kurzor.ge.EdwTextLen(id))
     1    EdwTextLen(id)=EdwTextLen(id)+1
        Kurzor=Kurzor+1
        EventNumber=0
        if(Kurzor.gt.EdwTextMax(id)) then
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
        else
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2      EdwTextMax(id))
        endif
      else if(EventType.eq.EventKey) then
        if(EventNumber.eq.JeRight) then
          EventNumber=0
          if(Kurzor.lt.EdwTextLen(id)) then
            if(ShiftPressed) then
              if(EdwSelTo.ne.EdwSelFr) then
                EdwSelTo=EdwSelTo+1
              else
                EdwSelFr=Kurzor
                EdwSelTo=Kurzor+1
              endif
            else
              call FeEdwSelClear(id)
            endif
            Kurzor=Kurzor+1
            if(Kurzor.gt.EdwTextMax(id))
     1        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2          Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          else
            go to 2500
          endif
        else if(EventNumber.eq.JeLeft) then
          EventNumber=0
          if(Kurzor.gt.0) then
            if(ShiftPressed) then
              if(EdwSelTo.ne.EdwSelFr) then
                EdwSelTo=EdwSelTo-1
              else
                EdwSelFr=Kurzor
                EdwSelTo=Kurzor-1
              endif
            else
              call FeEdwSelClear(id)
            endif
            Kurzor=Kurzor-1
            if(Kurzor.lt.EdwTextMin(id).and.Kurzor.gt.0)
     1        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2          Kurzor,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          else
            go to 2500
          endif
        else if(EventNumber.eq.JeInsert) then
          EventNumber=0
          InsertMode=.not.InsertMode
        else if(EventNumber.eq.JeHome) then
          EventNumber=0
          if(ShiftPressed) then
            if(EdwSelTo.ne.EdwSelFr) then
              EdwSelTo=0
            else
              EdwSelFr=Kurzor
              EdwSelTo=0
            endif
          else
            call FeEdwSelClear(id)
          endif
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      1,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          Kurzor=0
        else if(EventNumber.eq.JeEnd) then
          EventNumber=0
          if(ShiftPressed) then
            if(EdwSelTo.ne.EdwSelFr) then
              EdwSelTo=EdwTextLen(id)
            else
              EdwSelFr=Kurzor
              EdwSelTo=EdwTextLen(id)
            endif
          else
            call FeEdwSelClear(id)
          endif
          Kurzor=EdwTextLen(id)
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
        else if(EventNumber.eq.JeBackspace) then
          EventNumber=0
          if(EdwSelTo.eq.EdwSelFr) then
            if(Kurzor.gt.0) then
              if(Kurzor.lt.EdwTextLen(id)) then
                EdwString(id)=EdwString(id)(1:Kurzor-1)//
     1                        EdwString(id)(Kurzor+1:EdwTextLen(id))
              else if(Kurzor.eq.EdwTextLen(id)) then
                EdwString(id)(EdwTextLen(id):)=' '
              endif
              if(Kurzor.le.EdwTextLen(id))
     1          EdwTextLen(id)=EdwTextLen(id)-1
              Kurzor=Kurzor-1
              k=min(Kurzor,EdwTextMin(id))
              call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1          k,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
            endif
          else
            call FeEdwSelDelete(id)
          endif
        else if(EventNumber.eq.JeDeleteUnder) then
          EventNumber=0
          if(EdwSelTo.eq.EdwSelFr) then
            if(Kurzor.lt.EdwTextLen(id)) then
              if(Kurzor.lt.EdwTextLen(id)-1) then
                EdwString(id)=EdwString(id)(:Kurzor)//
     1                        EdwString(id)(Kurzor+2:EdwTextLen(id))
              else
                EdwString(id)=EdwString(id)(:Kurzor)
              endif
              EdwTextLen(id)=EdwTextLen(id)-1
            endif
            call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1        EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2        EdwTextMax(id))
          else
            call FeEdwSelDelete(id)
          endif
        else
          go to 2500
        endif
      endif
      go to 4000
2500  LastEdw=id
      EdwKurzor(id)=Kurzor
      go to 9999
      entry FeEdwLock(id)
      if(EdwState(id).eq.EdwClosed.or.EdwState(id).eq.EdwRemoved.or.
     1   EdwState(id).eq.EdwLocked) go to 9999
      call FeEdwBasic(id,LightYellow)
      EdwState(id)=EdwLocked
      go to 9999
      entry FeEdwDisable(id)
      if(EdwState(id).eq.EdwDisabled) go to 9999
      call FeEdwBasic(id,WhiteGray)
      EdwState(id)=EdwDisabled
      go to 9999
      entry FeEdwClose(id)
      if(EdwState(id).eq.EdwClosed.or.EdwState(id).eq.EdwRemoved)
     1   go to 9999
      i=EdwClosed
      go to 2900
      entry FeEdwRemove(id)
      if(EdwState(id).eq.EdwRemoved) go to 9999
      i=EdwRemoved
2900  LastEdw=0
      if(EdwUpDown(id).ne.0) then
        call FeEudClose(id,1)
        call FeEudClose(id,2)
      endif
      if(KurzorEdw.eq.id) KurzorEdw=0
      dx=2.
      call FeFillRectangle(EdwXmin(id)-dx,EdwXmax(id)+dx,EdwYmin(id)-dx,
     1                     EdwYmax(id)+dx,4,0,0,LightGray)
      EdwState(id)=i
      go to 9999
4000  xp=EdwTextXMin(id)
      yp=(EdwYmin(id)+EdwYmax(id))*.5
      call FeEdwBasic(id,White)
      if(EdwTextMin(id).ge.1.and.EdwTextMax(id).ge.1)
     1  call FeOutSt(0,xp,yp,
     2  EdwString(id)(EdwTextMin(id):EdwTextMax(id)),'L',Black)
      EdwKurzor(id)=Kurzor
      if(PrepisKurzoru) then
        KurzorEdw=id
        call FeZmenKurzor(id)
      endif
      call FeEdwHighlight(id)
      LastEdw=id
      if(EdwSelected(id).ne.1) then
        go to 9999
      else
        ColorSel=Black
        go to 5000
      endif
      go to 9999
      entry FeEdwSelect(id)
      ColorSel=Black
      EdwSelected(id)=1
      go to 5000
      entry FeEdwDeselect(id)
      ColorSel=White
      EdwSelected(id)=0
5000  xu(1)=EdwXMin(id)
      yu(1)=EdwYMin(id)
      xu(2)=EdwXMin(id)+2.
      yu(2)=EdwYMin(id)
      xu(3)=EdwXMin(id)
      yu(3)=EdwYMin(id)+2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMax(id)
      yu(1)=EdwYMax(id)
      xu(2)=EdwXMax(id)-2.
      yu(2)=EdwYMax(id)
      xu(3)=EdwXMax(id)
      yu(3)=EdwYMax(id)-2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMax(id)
      yu(1)=EdwYMin(id)
      xu(2)=EdwXMax(id)-2.
      yu(2)=EdwYMin(id)
      xu(3)=EdwXMax(id)
      yu(3)=EdwYMin(id)+2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMin(id)
      yu(1)=EdwYMax(id)
      xu(2)=EdwXMin(id)+2.
      yu(2)=EdwYMax(id)
      xu(3)=EdwXMin(id)
      yu(3)=EdwYMax(id)-2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
9999  return
      end
      subroutine FeGetVisibleSubstring(String,NStart,NDirection,Length,
     1                                 NFirst,NLast)
      include 'fepc.cmn'
      character*(*) String
      real Length
      if(FeTxLengthSpace(String).le.Length) then
        NFirst=1
        NLast=idel(String)
        go to 9999
      endif
      if(NDirection.gt.0) then
        KFirst=idel(String)
        KLast=NStart
        KDir=-1
      else
        KFirst=1
        KLast=NStart
        KDir=1
      endif
      do k=KFirst,KLast,KDir
        if(NDirection.gt.0) then
          k1=KLast
          k2=k
        else
          k1=k
          k2=KLast
        endif
        if(FeTxLengthSpace(String(k1:k2)).le.Length) then
          NFirst=k1
          NLast=k2
          go to 9999
        endif
      enddo
1200  if(NDirection.lt.0.and.NFirst.le.1) then
        do k=NFirst,idel(String)
          if(FeTxLengthSpace(String(NFirst:k)).le.Length) then
            NLast=k
          else
            go to 9999
          endif
        enddo
      endif
9999  return
      end
      integer function FeGetEdwStringPosition(id,XPosition)
      include 'fepc.cmn'
      character*256 Veta
      Veta=EdwString(id)
      k1=EdwTextMin(id)
      k2=EdwTextMax(id)
      XLength=FeTxLengthSpace(Veta(k1:k2))
      XPom=XPosition-EdwTextXMin(id)
      if(XPom.lt.0.) then
        FeGetEdwStringPosition=0
        go to 9999
      else if(XPom.gt.XLength) then
        FeGetEdwStringPosition=k2
        go to 9999
      endif
      DPom1=0.
      do k=k1,k2
        DPom2=FeTxLengthSpace(Veta(k1:k))
        if(XPom.ge.DPom1.and.XPom.lt.DPom2) then
          if(abs(XPom-DPom1).lt.abs(XPom-DPom2)) then
            FeGetEdwStringPosition=k-1
          else
            FeGetEdwStringPosition=k
          endif
          go to 9999
        endif
        DPom1=DPom2
      enddo
      FeGetEdwStringPosition=k2
9999  return
      end
      subroutine FeEud
      include 'fepc.cmn'
      integer ColorArrow,ColorUpDown,ColorRD,ColorLU,ColorObtah
      entry FeEudMake(id)
      ydw=EdwYMax(id)-EdwYMin(id)
      ydwp=anint(ydw*.5*EnlargeFactor-3.)/EnlargeFactor
      EdwUpDownXMin(id)=anint(EdwXMax(id)*EnlargeFactor+5.)/
     1                  EnlargeFactor
      EdwUpDownXMax(id)=Anint((EdwUpDownXMin(id)+ydw*.6)*EnlargeFactor)/
     1                  EnlargeFactor
      EdwUpDownYMin(1,id)=anint((EdwYMin(id)+ydwp)*EnlargeFactor+5.)/
     1                    EnlargeFactor
      EdwUpDownYMax(1,id)=EdwUpDownYMin(1,id)+ydwp
      EdwUpDownYMin(2,id)=EdwYMin(id)
      EdwUpDownYMax(2,id)=EdwUpDownYMin(2,id)+ydwp
      go to 9999
      entry FeEudOff(id,iud)
      ColorUpDown=LightGray
      ColorArrow=Black
      ColorLU=White
      ColorRD=Gray
      ColorObtah=Gray
      PressShift=0.
      go to 2000
      entry FeEudOn(id,iud)
      ColorUpDown=LightGray
      ColorArrow=Black
      ColorLU=Gray
      ColorRD=White
      ColorObtah=Gray
      PressShift=1.
      go to 2000
      entry FeEudClose(id,iud)
      entry FeEudRemove(id,iud)
      ColorUpDown=LightGray
      ColorArrow=LightGray
      ColorLU=LightGray
      ColorRD=LightGray
      ColorObtah=LightGray
      PressShift=0.
2000  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(EdwUpDownXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(EdwUpDownXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(EdwUpDownYMin(iud,id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(EdwUpDownYMax(iud,id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorUpDown)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
      pomx=anint((EdwUpDownXMax(id)+EdwUpDownXMin(id))*.5*
     1           EnlargeFactor)
      pomy=anint((EdwUpDownYMax(iud,id)+EdwUpDownYMin(iud,id))*.5*
     1           EnlargeFactor)
      fnx=4.
      fny=2.
      xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
      xu(2)=anint(pomx+PressShift)/EnlargeFactor
      xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
      if(iud.eq.1) then
        yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
      else
        yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
      endif
      yu(3)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,ColorArrow)
9999  return
      end
      subroutine FeEdwHighlight(id)
      include 'fepc.cmn'
      character*256 Veta
      Veta=EdwString(id)
      if(EdwSelFr.eq.EdwSelTo) go to 9999
      n1=max(min(EdwSelFr,EdwSelTo),EdwTextMin(id)-1)
      n2=min(max(EdwSelFr,EdwSelTo),EdwTextMax(id))
      xp=EdwTextXMin(id)+
     1   FeTxLengthSpace(Veta(EdwTextMin(id):n1))
      yp=(EdwYmin(id)+EdwYmax(id))*.5
      call FeGetTextRectangle(xp,yp,Veta(n1+1:n2),'L',1,xpold,
     1                        xkold,ypold,ykold,refx,refy,conx,cony)
      call FePlotMode('E')
      xpold=xp
      xkold=xp+FeTxLengthSpace(Veta(n1+1:n2))
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,White)
      call FePlotMode('N')
9999  return
      end
      subroutine FeEdwSelClear(id)
      include 'fepc.cmn'
      call FeEdwHighlight(id)
      EdwSelFr=0
      EdwSelTo=0
      return
      end
      subroutine FeEdwBasic(id,ColorBckg)
      include 'fepc.cmn'
      integer ColorBckg
      call FeDrawFrame(EdwXMin(id),EdwYMin(id),
     1                 EdwXMax(id)-EdwXMin(id),
     2                 EdwYMax(id)-EdwYMin(id),2.,White,Gray,
     3                 Black,.false.)
      call FeFillRectangle(EdwXMin(id),EdwXMax(id),EdwYMin(id),
     1                     EdwYMax(id),4,0,0,ColorBckg)
      xu(1)=anint(EdwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(EdwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=anint(EdwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(2)=anint(EdwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(EdwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(3)=anint(EdwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=anint(EdwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(4)=anint(EdwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      return
      end
      subroutine FeEdwSelDelete(id)
      include 'fepc.cmn'
      character*256 Veta
      n1=min(EdwSelFr,EdwSelTo)+1
      n2=max(EdwSelFr,EdwSelTo)
      if(n1.gt.n2) go to 9999
      if(n1.gt.1) then
        Veta=EdwString(id)(:n1-1)
        idl=n1-1
      else
        Veta=' '
        idl=0
      endif
      call FeEdwSelClear(id)
      EdwString(id)=Veta(:idl)//EdwString(id)(n2+1:)
      Kurzor=min(Kurzor,n1-1)
      EdwTextLen(id)=idel(EdwString(id))
9999  return
      end
      subroutine FeCrw
      include 'fepc.cmn'
      integer ColorLU,ColorRD,ColorCrw,ColorObtah,ColorSel,
     1        ColorBkg,ColorFrg
      character*20 Veta
      logical YesNo
      entry FeCrwMake(id,idc,xm,ym,xd,yd)
      if(CrwType(id).eq.CrwTypeBMP) then
        pom=1./EnlargeFactor
      else
        pom=1.
      endif
      CrwXMin(id)=xm
      CrwXMax(id)=xm+xd*pom
      CrwYMin(id)=ym
      CrwYMax(id)=ym+yd*pom
      if(idc.gt.0) then
        CrwXMin(id)=CrwXMin(id)+QuestXMin(idc)
        CrwXMax(id)=CrwXMax(id)+QuestXMin(idc)
        CrwYMin(id)=CrwYMin(id)+QuestYMin(idc)
        CrwYMax(id)=CrwYMax(id)+QuestYMin(idc)
      endif
      CrwState(id)=CrwClosed
      CrwLabelTabs(id)=UseTabs
      go to 9999
      entry FeCrwOpen(id,YesNo)
      if(YesNo) then
        go to 2000
      else
        go to 1000
      endif
      entry FeCrwOff(id)
1000  if(CrwState(id).eq.CrwOff.or.CrwState(id).eq.CrwRemoved)
     1  go to 9999
      CrwState(id)=CrwOff
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,White)
      else
        ColorRD=Gray
        ColorLU=White
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=White
        ColorFrg=Black
        go to 6000
      endif
      go to 9999
      entry FeCrwOn(id)
2000  if(CrwState(id).eq.CrwOn.or.CrwState(id).eq.CrwRemoved) go to 9999
      CrwState(id)=CrwOn
      CrwLogic(id)=.true.
      if(CrwExGr(id).eq.0) then
        if(CrwState(id).ne.CrwOff) call FeCrwBasic(id,White)
        xdp=CrwXmax(id)-CrwXmin(id)
        ydp=CrwYmax(id)-CrwYmin(id)
        xu(1)=CrwXmin(id)+xdp*.1
        yu(1)=(CrwYmax(id)+CrwYmin(id))*.5
        xu(2)=CrwXmin(id)+xdp*.333333
        yu(2)=CrwYmin(id)+ydp*.1
        xu(3)=CrwXmax(id)-xdp*.1
        yu(3)=CrwYmax(id)-ydp*.1
        xu(4)=xu(2)
        yu(4)=CrwYmin(id)+ydp*.333333
        call FePolygon(xu,yu,4,4,0,0,Black)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=White
        ColorFrg=Black
        go to 6000
      endif
      go to 9999
      entry FeCrwLock(id)
      if(CrwState(id).eq.CrwClosed.or.CrwState(id).eq.CrwRemoved.or.
     1   CrwState(id).eq.CrwDisabled.or.CrwState(id).eq.CrwLocked)
     3  go to 9999
      CrwState(id)=CrwLocked
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,LightYellow)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=LightYellow
        ColorFrg=LightYellow
        go to 6000
      endif
      go to 9999
      entry FeCrwDisable(id)
      if(CrwState(id).eq.CrwDisabled) go to 9999
      CrwState(id)=CrwDisabled
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,WhiteGray)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=WhiteGray
        ColorFrg=WhiteGray
        go to 6000
      endif
      go to 9999
      entry FeCrwClose(id)
      if(CrwState(id).eq.CrwClosed.or.
     1   CrwState(id).eq.CrwRemoved) go to 9999
      ir=0
      go to 5000
      entry FeCrwRemove(id)
      if(CrwState(id).eq.CrwRemoved) go to 9999
      ir=1
5000  dx=3.
      pom1=anint(CrwXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(CrwXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(CrwYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(CrwYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      if(ir.eq.1) then
        CrwState(id)=CrwRemoved
      else
        CrwState(id)=CrwClosed
      endif
      CrwLogic(id)=.false.
      go to 9999
6000  xpom=(CrwXMin(id)+CrwXMax(id))*.5
      ypom=(CrwYMin(id)+CrwYMax(id))*.5
      if(CrwType(id).eq.CrwTypeLetter) then
        if(CrwLogic(id)) then
          dx =1.
          dxx=1.
        else
          dx=2.
          dxx=0.
        endif
        xLD=anint(CrwXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
        xRU=anint(CrwXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
        wp=xRU-xLD
        yLD=anint(CrwYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
        yRU=anint(CrwYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
        hp=yRU-yLD
        call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorCrw)
        call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                   .false.)
        xpom=(CrwXMin(id)+CrwXMax(id))*.5
        ypom=(CrwYMin(id)+CrwYMax(id))*.5
        if(CrwLabel(id)(3:3).eq.'P') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'D') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'S') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'X') then
          go to 9999
        endif
      else if(CrwType(id).eq.CrwTypeBMP) then
        Veta=CrwBMPName(id)(:idel(CrwBMPName(id)))
        if(CrwState(id).eq.CrwOn) then
          Veta=Veta(:idel(Veta))//'-on.bmp'
        else if(CrwState(id).eq.CrwOff) then
          Veta=Veta(:idel(Veta))//'-off.bmp'
        else if(CrwState(id).eq.CrwDisabled) then
          Veta=Veta(:idel(Veta))//'-disable.bmp'
        else if(CrwState(id).eq.CrwLocked) then
          Veta=Veta(:idel(Veta))//'-disable.bmp'
        endif
        call FeLoadImage(CrwXMin(id)-2.,CrwXMax(id)+2.,
     1                   CrwYMin(id)-2.,CrwYMax(id)+2.,
     2                   Veta,1)
      else
        Rad=(CrwXMax(id)-CrwXMin(id))*.5
        call FeArc(xpom,ypom,Rad, 45.,180.,Gray)
        call FeArc(xpom,ypom,Rad,225.,180.,White)
        Rad=anint(Rad*EnlargeFactor-1.)/EnlargeFactor
        call FeCircle(xpom,ypom,Rad,Gray)
        Rad=anint(Rad*EnlargeFactor-1.)/EnlargeFactor
        call FeCircle(xpom,ypom,Rad,ColorBkg)
      endif
      if(CrwLogic(id)) then
        if(CrwType(id).eq.CrwTypeRadio) then
          call FeCircle(xpom,ypom,2.,ColorFrg)
        else if(CrwType(id).eq.CrwTypeLetter) then
          call FeOutSt(0,xpom,ypom,CrwLabel(id)(3:3),'C',White)
        endif
      else
        if(CrwType(id).eq.CrwTypeLetter) then
          call FeOutSt(0,xpom,ypom,CrwLabel(id)(3:3),'C',Black)
        endif
      endif
      go to 9999
      entry FeCrwActivate(id)
      ColorSel=Black
      go to 8000
      entry FeCrwDeactivate(id)
      ColorSel=LightGray
8000  if(CrwType(id).eq.CrwTypeLetter.or.CrwType(id).eq.CrwTypeBMP)
     1  go to 9999
      if(CrwLabel(id).ne.' ') then
        x1=CrwLabelXMin(id)
        x2=CrwLabelXMax(id)
        y1=CrwLabelYMin(id)
        y2=CrwLabelYMax(id)
      else if(CrwExGr(id).eq.0) then
        x1=anint(CrwXmin(id)*EnlargeFactor+1.)/EnlargeFactor
        x2=anint(CrwXmax(id)*EnlargeFactor-1.)/EnlargeFactor
        y1=anint(CrwYmin(id)*EnlargeFactor+1.)/EnlargeFactor
        y2=anint(CrwYmax(id)*EnlargeFactor-1.)/EnlargeFactor
        if(ColorSel.eq.LightGray) ColorSel=White
      else
        go to 9999
      endif
      xu(1)=anint(x1*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(y1*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=xu(1)
      yu(2)=anint(y2*EnlargeFactor+1.)/EnlargeFactor
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorSel)
      xu(3)=anint(x2*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=xu(3)
      call FePolyLine(2,xu(3),yu,ColorSel)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorSel)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorSel)
      call FeLineType(NormalLine)
9999  return
      end
      subroutine FeCrwBasic(id,Color)
      include 'fepc.cmn'
      integer Color
      call FeFillRectangle(CrwXMin(id),CrwXMax(id),CrwYMin(id),
     1                     CrwYMax(id),4,0,0,Color)
      call FeDrawFrame(CrwXMin(id),CrwYMin(id),
     1                 CrwXMax(id)-CrwXMin(id),
     2                 CrwYMax(id)-CrwYMin(id),2.,White,Gray,
     3                 Black,.false.)
      xu(1)=anint(CrwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(CrwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=anint(CrwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(2)=anint(CrwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(CrwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(3)=anint(CrwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=anint(CrwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(4)=anint(CrwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      return
      end
      subroutine FeCrwOnAction(ic)
      include 'fepc.cmn'
      call FeQuestActiveObjOff
      call FeCrwOn(ic)
      call FeCrwActivate(ic)
      ActiveObj=ActCrw*10000+ic
      return
      end
      subroutine FeCrwOffAction(ic)
      include 'fepc.cmn'
      call FeQuestActiveObjOff
      call FeCrwOff(ic)
      call FeCrwActivate(ic)
      ActiveObj=ActCrw*10000+ic
      return
      end
      subroutine FeRolMenu
      include 'fepc.cmn'
      integer ColorArrow,ColorButt,ColorRD,ColorLU,ColorObtah,ColorSel
      logical Opening
      go to 9999
      entry FeRolMenuMake(id,idc,xm,ym,xd,yd)
      RolMenuTextXMin(id)=xm
      RolMenuTextXMax(id)=xm+xd
      RolMenuTextYMin(id)=ym
      RolMenuTextYMax(id)=ym+yd
      if(idc.gt.0) then
        RolMenuTextXMin(id)=RolMenuTextXMin(id)+QuestXMin(idc)
        RolMenuTextXMax(id)=RolMenuTextXMax(id)+QuestXMin(idc)
        RolMenuTextYMin(id)=RolMenuTextYMin(id)+QuestYMin(idc)
        RolMenuTextYMax(id)=RolMenuTextYMax(id)+QuestYMin(idc)
      endif
      RolMenuButtYMin(id)=RolMenuTextYMin(id)+2.
      RolMenuButtYMax(id)=RolMenuTextYMax(id)-2.
      RolMenuButtXMax(id)=RolMenuTextXMax(id)-2.
      RolMenuButtXMin(id)=RolMenuButtXMax(id)-yd+4.
      RolMenuState(id)=RolMenuClosed
      if(RolMenuButtYMax(id).gt.YCenGrWin) then
        RolMenuType(id)=RolMenuDown
      else
        RolMenuType(id)=RolMenuUp
      endif
      RolMenuAlways(id)=.false.
      go to 9999
      entry FeRolMenuOpen(id)
      Opening=.true.
      call FeDrawFrame(RolMenuTextXMin(id),RolMenuTextYMin(id),
     1                 RolMenuTextXMax(id)-RolMenuTextXMin(id),
     2                 RolMenuTextYMax(id)-RolMenuTextYMin(id),
     3                 2.,White,Gray,Black,.false.)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,White)
      xu(1)=RolMenuTextXMin(id)-1.
      yu(1)=RolMenuTextYMin(id)-1.
      xu(2)=RolMenuTextXMax(id)+1.
      yu(2)=RolMenuTextYMin(id)-1.
      xu(3)=RolMenuTextXMax(id)+1.
      yu(3)=RolMenuTextYMax(id)+1.
      xu(4)=RolMenuTextXMin(id)-1.
      yu(4)=RolMenuTextYMax(id)+1.
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      RolMenuState(id)=RolMenuOpened
      go to 2000
      entry FeRolMenuLock(id)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,LightYellow)
      RolMenuState(id)=RolMenuLocked
      Opening=.false.
      go to 2000
      entry FeRolMenuDisable(id)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,WhiteGray)
      RolMenuState(id)=RolMenuDisabled
      Opening=.false.
      go to 2000
      entry FeRolMenuOff(id)
      if(RolMenuState(id).ne.RolMenuOpened) go to 9999
      Opening=.false.
2000  ColorButt=LightGray
      ColorArrow=Black
      ColorLU=White
      ColorRD=Gray
      ColorObtah=Gray
      PressShift=0.
      go to 2200
      entry FeRolMenuOn(id)
      if(RolMenuState(id).ne.RolMenuOpened) go to 9999
      Opening=.false.
      ColorButt=LightGray
      ColorArrow=Black
      ColorLU=Gray
      ColorRD=White
      ColorObtah=Gray
      PressShift=1.
2200  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(RolMenuButtXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(RolMenuButtXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(RolMenuButtYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(RolMenuButtYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButt)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
      if(RolMenuState(id).eq.RolMenuRemoved) go to 9999
      pomx=anint((RolMenuButtXMin(id)+RolMenuButtXMax(id))*.5*
     1           EnlargeFactor)
      pomy=anint((RolMenuButtYMin(id)+RolMenuButtYMax(id))*.5*
     1           EnlargeFactor)
      fnx=4.
      fny=3.
      xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
      xu(2)=anint(pomx+PressShift)/EnlargeFactor
      xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
      if(RolMenuType(id).eq.RolMenuUp) then
        yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
      else
        yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
      endif
      yu(3)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,ColorArrow)
      if(Opening.and.RolMenuSelected(id).gt.0) then
        go to 3000
      else
        go to 9999
      endif
      entry FeRolMenuWriteText(id,NSel)
      RolMenuSelected(id)=NSel
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,White)
3000  xp=RolMenuTextXMin(id)+EdwMarginSize
      yp=(RolMenuTextYMin(id)+RolMenuTextYMax(id))*.5
      call FeOutSt(0,xp,yp,RolMenuText(RolMenuSelected(id),id),'L',
     1             Black)
      go to 9999
      entry FeRolMenuClose(id)
      i=RolMenuClosed
      go to 4000
      entry FeRolMenuRemove(id)
      i=RolMenuRemoved
4000  if(RolMenuState(id).eq.i) go to 9999
      dx=2.
      pom1=anint(RolMenuTextXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(RolMenuTextXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(RolMenuTextYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(RolMenuTextYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      RolMenuState(id)=i
      RolMenuSelected(id)=0
      go to 9999
      entry FeRolMenuActivate(id)
      ColorSel=Black
      go to 8000
      entry FeRolMenuDeactivate(id)
      ColorSel=LightGray
8000  x1=RolMenuLabelXMin(id)
      x2=RolMenuLabelXMax(id)
      y1=RolMenuLabelYMin(id)
      y2=RolMenuLabelYMax(id)
      xu(1)=x1-1.
      yu(1)=y1-1.
      xu(2)=xu(1)
      yu(2)=y2+1.
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorSel)
      xu(3)=x2+1.
      xu(4)=xu(3)
      call FePolyLine(2,xu(3),yu,ColorSel)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorSel)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorSel)
      call FeLineType(NormalLine)
9999  return
      end
      subroutine FeRolMenuOnAction(ic)
      include 'fepc.cmn'
      call FeQuestActiveObjOff
      call FeRolMenuOn(ic)
      call FeRolMenuActivate(ic)
      ActiveObj=ActRolMenu*10000+ic
      return
      end
      subroutine FeSbw
      include 'fepc.cmn'
      integer ColorLU,ColorRD,ColorButton,ColorObtah,ColorSel
      character*256 t256
      logical Show
      entry FeSbwMake(id,idc,xm,ym,xd,yd,nstrip)
      SbwXMin(id)=xm
      SbwXMax(id)=xm+xd
      SbwYMin(id)=ym
      SbwYMax(id)=ym+yd
      if(idc.gt.0) then
        SbwXMin(id)=SbwXMin(id)+QuestXMin(idc)
        SbwXMax(id)=SbwXMax(id)+QuestXMin(idc)
        SbwYMin(id)=SbwYMin(id)+QuestYMin(idc)
        SbwYMax(id)=SbwYMax(id)+QuestYMin(idc)
      endif
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        SbwXMinPruh(1,id)=anint(SbwXMax(id)*EnlargeFactor+2.)/
     1                    EnlargeFactor
        SbwXMaxPruh(1,id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+
     1                          SbwPruhXd+1.)/EnlargeFactor
        SbwYMinPruh(1,id)=anint(SbwYMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwYMaxPruh(1,id)=(SbwYMax(id)*EnlargeFactor+2.)/EnlargeFactor
        SbwXMinPosun(1,id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(1,id)=anint(SbwXMaxPruh(1,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(1,id)=anint(SbwYMaxPruh(1,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwYMinPosun(1,id)=anint(SbwYMaxPosun(1,id)*EnlargeFactor-
     1                           SbwPruhXd+4.)/EnlargeFactor
        SbwXMinPosun(2,id)=SbwXMinPosun(1,id)
        SbwXMaxPosun(2,id)=SbwXMaxPosun(1,id)
        SbwYMinPosun(2,id)=anint(SbwYMinPruh(1,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(2,id)=anint(SbwYMinPosun(2,id)*EnlargeFactor+
     1                           SbwPruhYd-4.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        SbwXMinPruh(2,id)=anint(SbwXMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwXMaxPruh(2,id)=anint(SbwXMax(id)*EnlargeFactor+2.)/
     1                    EnlargeFactor
        SbwYMaxPruh(2,id)=anint(SbwYMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwYMinPruh(2,id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-
     1                          SbwPruhYd-2.)/EnlargeFactor
        SbwXMinPosun(3,id)=anint(SbwXMinPruh(2,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(3,id)=anint(SbwXMinPosun(3,id)*EnlargeFactor+
     1                           SbwPruhXd-4.)/EnlargeFactor
        SbwYMinPosun(3,id)=anint(SbwYMinPruh(2,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(3,id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(4,id)=anint(SbwXMaxPruh(2,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwXMinPosun(4,id)=anint(SbwXMaxPosun(4,id)*EnlargeFactor-
     1                           SbwPruhXd+4.)/EnlargeFactor
        SbwYMinPosun(4,id)=SbwYMinPosun(3,id)
        SbwYMaxPosun(4,id)=SbwYMaxPosun(3,id)
      endif
      SbwLn(id)=0
      pom=SbwXMin(id)
      pomd=(SbwXMax(id)-SbwXMin(id))/float(nstrip)
      do i=1,nstrip+1
        SbwXStripe(i,id)=pom
        pom=pom+pomd
      enddo
      go to 9999
      entry FeSbwMenuOpen(id)
      SbwYStep(id)=MenuLineWidth
      SbwLines(id)=nint((SbwYMax(id)-SbwYMin(id))/MenuLineWidth)
      SbwType(id)=SbwMenuType
      go to 1500
      entry FeSbwSelectOpen(id)
      SbwYStep(id)=MenuLineWidth
      SbwLines(id)=nint((SbwYMax(id)-SbwYMin(id))/MenuLineWidth)
      SbwType(id)=SbwSelectType
      go to 1500
      entry FeSbwTextOpen(id,ly)
      SbwType(id)=SbwTextType
      SbwLines(id)=ly
      SbwYStep(id)=(SbwYMax(id)-SbwYMin(id)-3.)/float(ly)
      go to 1500
      entry FeSbwPureOpen(id,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      SbwType(id)=SbwPureType
1500  if(SbwType(id).ne.SbwPureType) then
        if(SbwType(id).eq.SbwMenuType.or.SbwType(id).eq.SbwSelectType)
     1    then
          pom=EdwMarginSize
        else if(SbwType(id).eq.SbwTextType) then
          pom=1.
        endif
        SbwColumnsXMin(id)=(SbwXMin(id)*EnlargeFactor+pom)/EnlargeFactor
        SbwColumnsXMax(id)=(SbwXMax(id)*EnlargeFactor-pom)/EnlargeFactor
        SbwColumnsXLen(id)=SbwColumnsXMax(id)-SbwColumnsXMin(id)
        SbwColumns(id)=SbwColumnsXLen(id)/FeTxLength('H')
        SbwLn(id)=NextLogicNumber()
        call FeDrawFrame(SbwXMin(id),SbwYMin(id),
     1                   SbwXMax(id)-SbwXMin(id),
     2                   SbwYMax(id)-SbwYMin(id),
     3                   2./EnlargeFactor,White,Gray,Black,.false.)
        call FeFillRectangle(SbwXMin(id),SbwXMax(id),SbwYMin(id),
     1                       SbwYMax(id),4,0,0,White)
        xu(1)=anint(SbwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
        yu(1)=anint(SbwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
        xu(2)=anint(SbwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
        yu(2)=anint(SbwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
        xu(3)=anint(SbwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
        yu(3)=anint(SbwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
        xu(4)=anint(SbwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
        yu(4)=anint(SbwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,Black)
        call OpenFile(Sbwln(id),SbwFile(id),'formatted','unknown')
        n=0
        m=0
        call SetStringArrayTo(SbwItem(1,id),60,' ')
        call SetStringArrayTo(SbwItemShort(1,id),60,' ')
2000    read(Sbwln(id),FormA,end=2010) t256
        n=n+1
        m=max(m,idel(t256))
        go to 2000
2010    SbwItemNMax(id)=n
        SbwItemFrom(id)=1
        SbwItemTo(id)=SbwLines(id)
        SbwColumnsMax(id)=m
        SbwColumnFrom(id)=1
        SbwColumnTo(id)=SbwColumns(id)
        SbwStripesMax(id)=(n-1)/SbwLines(id)+1
        SbwStripeFrom(id)=1
        SbwStripeTo(id)=max(nstrip,1)
        SbwItemPointer(id)=0
        if((SbwType(id).eq.SbwMenuType.or.
     1      SbwType(id).eq.SbwSelectType).and.n.gt.0)
     2    SbwItemPointer(id)=1
      else
        SbwItemNMax(id)=NYAll
        SbwItemFrom(id)=NYFr
        SbwItemTo(id)=NYTo
        SbwLines(id)=NYTo-NYFr+1
        n=NYAll
        SbwColumnsMax(id)=NXAll
        SbwColumnFrom(id)=NXFr
        SbwColumnTo(id)=NXTo
        SbwColumns(id)=NXTo-NXFr+1
        m=NXAll
      endif
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        call FeFillRectangle(SbwXMinPruh(1,id),
     1                       SbwXMaxPruh(1,id),
     2                       SbwYMinPruh(1,id),
     3                       SbwYMaxPruh(1,id),4,0,0,WhiteGray)
        xu(1)=anint(SbwXMaxPruh(1,id)*EnlargeFactor+1.)/EnlargeFactor
        yu(1)=SbwYMinPruh(1,id)
        xu(2)=anint(SbwXMaxPruh(1,id)*EnlargeFactor+1.)/EnlargeFactor
        yu(2)=SbwYMaxPruh(1,id)
        call FePolyLine(2,xu,yu,Black)
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        call FeFillRectangle(SbwXMinPruh(2,id),SbwXMaxPruh(2,id),
     1                       SbwYMinPruh(2,id),SbwYMaxPruh(2,id),
     2                       4,0,0,WhiteGray)
        xu(1)=SbwXMinPruh(2,id)
        yu(1)=anint(SbwYMinPruh(2,id)*EnlargeFactor-1.)/EnlargeFactor
        xu(2)=SbwXMaxPruh(2,id)
        yu(2)=anint(SbwYMinPruh(2,id)*EnlargeFactor-1.)/EnlargeFactor
        call FePolyLine(2,xu,yu,Black)
      endif
      PressShift=0.
      ip=0
      ik=0
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        if(n.gt.SbwLines(id)) then
          fn=float(n)
          SbwDelPlovak(id)=max(nint(float(SbwLines(id))/fn*
     1                         ((SbwYMinPosun(1,id)-SbwYMaxPosun(2,id))*
     2                         EnlargeFactor-2.)/EnlargeFactor),10)
        else
          SbwDelPlovak(id)=0
        endif
        ip=1
        ik=2
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        if(SbwStripes(id).le.1) then
          if(m.gt.SbwColumns(id)) then
            fm=float(m)
            SbwDelStrelka(id)=max(nint(float(SbwColumns(id))/fm*
     1                            ((SbwXMinPosun(4,id)-
     2                              SbwXMaxPosun(3,id))*EnlargeFactor-
     3                              2.)/EnlargeFactor),10)
          else
            SbwDelStrelka(id)=0.
          endif
        else
          if(SbwStripesMax(id).gt.SbwStripes(id)) then
            SbwDelStrelka(id)=max(nint(float(SbwStripes(id))/
     1                                 float(SbwStripesMax(id))*
     2                            ((SbwXMinPosun(4,id)-
     3                              SbwXMaxPosun(3,id))*EnlargeFactor-
     4                              2.)/EnlargeFactor),10)
          else
            SbwDelStrelka(id)=0.
          endif
        endif
      endif
      if(ip.eq.0) then
        ip=3
        ik=4
      endif
      Show=.true.
      SbwState(id)=SbwOpened
      go to 5000
      entry FeSbwClose(id)
      if(SbwState(id).eq.SbwClosed.or.
     1   SbwState(id).eq.SbwRemoved) go to 9999
      ir=0
      go to 2200
      entry FeSbwRemove(id)
      if(SbwState(id).eq.SbwRemoved) go to 9999
      ir=1
2200  if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        xpom=SbwXMaxPruh(1,id)
      else
        xpom=anint(SbwXMax(id)*EnlargeFactor+2.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        ypom=SbwYMinPruh(2,id)
      else
        ypom=anint(SbwYMin(id)*EnlargeFactor-2.)/EnlargeFactor
      endif
      call FeFillRectangle(anint(SbwXMin(id)*EnlargeFactor-2.)/
     1                     EnlargeFactor,
     2                     xpom,ypom,
     3                     anint(SbwYMax(id)*EnlargeFactor+2.)/
     4                     EnlargeFactor,4,0,0,LightGray)
      if(ir.eq.1) then
        SbwState(id)=SbwRemoved
      else
        SbwState(id)=SbwClosed
      endif
      go to 9999
      entry FeSbwPosunOff(id,it)
      ip=it
      ik=it
      Show=.false.
5000  PressShift=0.
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      go to 5100
      entry FeSbwPosunOn(id,it)
      PressShift=1.
      ip=it
      ik=it
      PressShift=1.
      ColorRD=Gray
      ColorLU=Gray
      ColorButton=LightGray
      ColorObtah=Gray
      Show=.false.
5100  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      do i=ip,ik
        xLD=anint(SbwXMinPosun(i,id)*EnlargeFactor-dxx)/EnlargeFactor
        xRU=anint(SbwXMaxPosun(i,id)*EnlargeFactor+dxx)/EnlargeFactor
        wp=xRU-xLD
        yLD=anint(SbwYMinPosun(i,id)*EnlargeFactor-dxx)/EnlargeFactor
        yRU=anint(SbwYMaxPosun(i,id)*EnlargeFactor+dxx)/EnlargeFactor
        hp=yRU-yLD
        call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
        call FeDrawFrame(xLD,yLD,wp,hp,dx/EnlargeFactor,ColorRD,ColorLU,
     1                   ColorObtah,.false.)
        pomx=anint((SbwXMinPosun(i,id)+SbwXMaxPosun(i,id))*.5*
     1             EnlargeFactor)
        pomy=anint((SbwYMinPosun(i,id)+SbwYMaxPosun(i,id))*.5*
     1             EnlargeFactor)
        if(i.le.2) then
          fnx=4.
          fny=3.
          xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
          xu(2)=anint(pomx+PressShift)/EnlargeFactor
          xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
          if(i.eq.1) then
            yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
            yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
          else
            yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
            yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
          endif
          yu(3)=yu(1)
        else
          fny=4.
          fnx=3.
          yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
          yu(2)=anint(pomy-PressShift)/EnlargeFactor
          yu(3)=anint(pomy-fny-PressShift)/EnlargeFactor
          if(i.eq.3) then
            xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
            xu(2)=anint(pomx-fnx+PressShift)/EnlargeFactor
          else
            xu(1)=anint(pomx-fnx+PressShift)/EnlargeFactor
            xu(2)=anint(pomx+fnx+PressShift)/EnlargeFactor
          endif
          xu(3)=xu(1)
        endif
        call FePolygon(xu,yu,3,4,0,0,Black)
      enddo
      if(Show) call FeSbwShow(id,1)
      go to 9999
      entry FeSbwSkokOn(id,it)
      ColorButton=Black
      go to 7000
      entry FeSbwSkokOff(id,it)
      ColorButton=WhiteGray
7000  if(it.le.2) then
        xLD=SbwXminPruh(1,id)
        xRU=SbwXmaxPruh(1,id)
        if(it.eq.1) then
          yLD=anint(SbwYmaxPlovak(id)*EnlargeFactor+3.)/EnlargeFactor
          yRU=anint(SbwYminPosun(1,id)*EnlargeFactor-3.)/EnlargeFactor
        else if(it.eq.2) then
          yLD=anint(SbwYmaxPosun(2,id)*EnlargeFactor+3.)/EnlargeFactor
          yRU=anint(SbwYminPlovak(id)*EnlargeFactor-3.)/EnlargeFactor
        endif
      else
        yLD=SbwYminPruh(2,id)
        yRU=SbwYmaxPruh(2,id)
        if(it.eq.3) then
          xLD=anint(SbwXmaxPosun(3,id)*EnlargeFactor+3.)/EnlargeFactor
          xRU=anint(SbwXminStrelka(id)*EnlargeFactor-3.)/EnlargeFactor
        else
          xLD=anint(SbwXmaxStrelka(id)*EnlargeFactor+3.)/EnlargeFactor
          xRU=anint(SbwXminPosun(4,id)*EnlargeFactor-3.)/EnlargeFactor
        endif
      endif
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
      go to 9999
      entry FeSbwActivate(id)
      ColorSel=Black
      go to 8000
      entry FeSbwDeactivate(id)
      ColorSel=LightGray
8000  if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        xpmax=anint(SbwXMaxPruh(1,id)*EnlargeFactor+2.)/EnlargeFactor
      else
        xpmax=anint(SbwXMax(id)*EnlargeFactor+3.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        ypmin=anint(SbwYMinPruh(2,id)*EnlargeFactor-2.)/EnlargeFactor
      else
        ypmin=anint(SbwYMin(id)*EnlargeFactor-3.)/EnlargeFactor
      endif
      xpmin=anint(SbwXMin(id)*EnlargeFactor-3.)/EnlargeFactor
      ypmax=anint(SbwYMax(id)*EnlargeFactor+3.)/EnlargeFactor
      xu(1)=xpmin
      yu(1)=ypmin
      xu(2)=xu(1)
      yu(2)=ypmax
      xu(3)=xpmax
      xu(4)=xu(3)
9999  return
      end
      subroutine FeSbwShow(id,From)
      include 'fepc.cmn'
      integer From,BackgroundColor,TextColor
      logical KresliPlovak,KresliStrelku,Nacitat
      SbwItemFrom(id)=From
      SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
      Nacitat=.true.
      go to 2000
      entry FeSbwShowPohniUpDown(id,From)
      SbwItemFrom(id)=From
      SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
      KresliPlovak= .false.
      KresliStrelku=.false.
      Nacitat=.true.
      go to 2010
      entry FeSbwShowPohniLeftRight(id,From)
      if(SbwStripes(id).gt.1) then
        SbwItemFrom(id)=From
        SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
        Nacitat=.true.
      else
        SbwColumnFrom(id)=From
        SbwColumnTo  (id)=From+SbwColumns(id)-1
        Nacitat=.false.
      endif
      KresliPlovak= .false.
      KresliStrelku=.false.
      go to 2010
      entry FeSbwLineDown(id)
      SbwItemFrom(id)=SbwItemFrom(id)+1
      SbwItemTo  (id)=SbwItemTo  (id)+1
      Nacitat=.true.
      go to 2000
      entry FeSbwLineUp(id)
      SbwItemFrom(id)=SbwItemFrom(id)-1
      SbwItemTo  (id)=SbwItemTo  (id)-1
      Nacitat=.true.
      go to 2000
      entry FeSbwPageDown(id)
      SbwItemFrom(id)=SbwItemFrom(id)+SbwLines(id)
      SbwItemTo  (id)=SbwItemTo  (id)+SbwLines(id)
      Nacitat=.true.
      go to 2000
      entry FeSbwPageUp(id)
      SbwItemFrom(id)=SbwItemFrom(id)-SbwLines(id)
      SbwItemTo  (id)=SbwItemTo  (id)-SbwLines(id)
      Nacitat=.true.
      go to 2000
      entry FeSbwColumnLeft(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)-SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)-SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)-1
        SbwColumnTo  (id)=SbwColumnTo  (id)-1
      endif
      go to 2000
      entry FeSbwColumnRight(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)+SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)+SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)+1
        SbwColumnTo  (id)=SbwColumnTo  (id)+1
      endif
      go to 2000
      entry FeSbwColumnStrongLeft(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)-SbwStripes(id)*SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)-SbwStripes(id)*SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)-SbwColumns(id)
        SbwColumnTo  (id)=SbwColumnTo  (id)-SbwColumns(id)
      endif
      go to 2000
      entry FeSbwColumnStrongRight(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)+SbwStripes(id)*SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)+SbwStripes(id)*SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)+SbwColumns(id)
        SbwColumnTo  (id)=SbwColumnTo  (id)+SbwColumns(id)
      endif
      go to 2000
2000  KresliPlovak =SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth
      KresliStrelku=SbwBars(id).eq.SbwHorizontal.or.
     1              SbwBars(id).eq.SbwBoth
2010  if(SbwColumnFrom(id).lt.1) then
        SbwColumnFrom(id)=1
        SbwColumnTo  (id)=SbwColumns(id)
      endif
      if(SbwColumnTo(id).gt.SbwColumnsMax(id)) then
        SbwColumnTo  (id)=SbwColumnsMax(id)
        SbwColumnFrom(id)=max(SbwColumnsMax(id)-SbwColumns(id)+1,1)
      endif
      if(SbwItemFrom(id).lt.1) then
        SbwItemFrom(id)=1
        SbwItemTo  (id)=SbwLines(id)*SbwStripes(id)
      endif
      if(SbwItemTo(id).gt.SbwItemNMax(id)) then
        if(SbwStripes(id).gt.1) then
          SbwItemFrom(id)=max(SbwLines(id)*(SbwStripesMax(id)-
     1                                      SbwStripes(id))+1,1)
          SbwItemTo(id)=SbwItemNMax(id)
        else
          SbwItemTo  (id)=SbwItemNMax(id)
          SbwItemFrom(id)=max(SbwItemNMax(id)-SbwLines(id)+1,1)
        endif
      endif
      if(SbwStripes(id).gt.1) then
        SbwStripeFrom(id)=max(nint(float(SbwItemFrom(id)-1)/
     1                             float(SbwLines(id)))+1,1)
        SbwStripeTo  (id)=min(nint(float(SbwItemTo(id))/
     1                             float(SbwLines(id))),
     2                        SbwStripesMax(id)-SbwStripes(id))
      endif
      call FeSbwAction(id,Nacitat,ich)
      if(ich.ne.0) go to 9999
      if(KresliPlovak) call FeSbwKresliPlovak(id)
      if(KresliStrelku) call FeSbwKresliStrelku(id)
      go to 9999
      entry FeSbwItemOn(id,ik)
      i=ik+SbwItemFrom(id)-1
      if(SbwType(id).eq.SbwMenuType) then
        BackGroundColor=DarkBlue
        if(ik.ge.1) then
          if(SbwItemColor(ik,id).eq.Black) then
            TextColor=White
          else
            TextColor=SbwItemColor(ik,id)
          endif
          SbwItemPointer(id)=i
        endif
        go to 4000
      else if(SbwType(id).eq.SbwSelectType) then
        BackGroundColor=SnowGray
        if(SbwItemSel(i,id).eq.1) then
          TextColor=Red
        else
          if(SbwItemSel(i,id).eq.0) then
            TextColor=SbwItemColor(ik,id)
          else
            TextColor=Gray
          endif
        endif
        SbwItemPointer(id)=i
        go to 4000
      else
        SbwItemPointer(id)=0
        go to 9999
      endif
      entry FeSbwItemOff(id,ik)
      i=ik+SbwItemFrom(id)-1
      BackgroundColor=White
      SbwItemPointer(id)=0
      if(SbwType(id).eq.SbwMenuType) then
        TextColor=SbwItemColor(ik,id)
        go to 4000
      else if(SbwType(id).eq.SbwSelectType) then
        if(SbwItemSel(i,id).eq.1) then
          TextColor=Red
        else
          if(SbwItemSel(i,id).eq.0) then
            TextColor=SbwItemColor(ik,id)
          else
            TextColor=Gray
          endif
        endif
        go to 4000
      else
        go to 9999
      endif
4000  if(ik.ge.1.and.ik.le.SbwStripes(id)*SbwLines(id)) then
        ix=(ik-1)/SbwLines(id)+1
        iy=mod(ik-1,SbwLines(id))+1
        xpom=SbwXStripe(ix,id)
        ypom=SbwYMax(id)-float(iy-1)*SbwYStep(id)
        call FeWrLine(xpom,ypom,SbwXStripe(ix+1,id)-SbwXStripe(ix,id),
     1                SbwItemShort(ik,id),TextColor,
     2                BackgroundColor)
        do i=2,SbwStripes(id)
          xu(1)=SbwXStripe(i,id)
          xu(2)=xu(1)
          yu(1)=SbwYMin(id)
          yu(2)=SbwYMax(id)
          call FePolyLine(2,xu,yu,Black)
        enddo
      endif
9999  return
      end
      subroutine FeSbwAction(id,Nacitat,ich)
      include 'fepc.cmn'
      character*256 t256
      character*1 Zn
      integer BackgroundColor,TextColor
      logical Konec,Nacitat
      ich=0
      if(SbwType(id).ne.SbwPureType) then
        n=0
        if(Nacitat) then
          ln=SbwLn(id)
          rewind ln
          Konec=.false.
          do i=1,SbwItemFrom(id)-1
            n=n+1
            read(ln,FormA1,end=9999) Zn
          enddo
        endif
        if(SbwType(id).eq.SbwTextType) then
          xd=0.
          xpom=SbwXStripe(1,id)
          ypom=SbwYMax(id)-SbwYStep(id)*.5
          xpom=xpom+1.
        else
          xpom=SbwXStripe(1,id)
          ypom=SbwYMax(id)
          xd=SbwXStripe(2,id)-SbwXStripe(1,id)
        endif
        call FeFillRectangle(SbwXMin(id),SbwXMax(id),SbwYMin(id),
     1                       SbwYMax(id),4,0,0,White)
        do i=1,min(SbwLines(id)*SbwStripes(id),SbwItemNMax(id))
          n=n+1
          if(SbwType(id).eq.SbwTextType) then
            call FeSetFixFont
            call FeWrLine(xpom,ypom,xd,SbwItemShort(i,id),White,White)
            call FeSetPropFont
          endif
          if(.not.Konec.and.Nacitat) then
            read(ln,FormA,end=2200) SbwItem(i,id)
            t256=SbwItem(i,id)
            j=index(t256,'#$color')
            if(j.gt.0) then
              Cislo=t256(j+7:)
              call posun(Cislo,0)
              read(Cislo,FormI15) SbwItemColor(i,id)
              t256=t256(:j-1)
              SbwItem(i,id)=t256
            else
              SbwItemColor(i,id)=Black
            endif
            call FeCutNameLength(t256,SbwItemShort(i,id),
     1        SbwColumnsXLen(id),SbwCutText(id))
          endif
          go to 2500
2200      Konec=.true.
2500      if(SbwType(id).eq.SbwTextType) call FeSetFixFont
          call FeWrLine(xpom,ypom,xd,' ',Black,White)
          if(SbwType(id).eq.SbwTextType) call FeSetPropFont
          if(.not.Konec) then
            if(n.eq.SbwItemPointer(id)) then
              if(SbwType(id).eq.SbwMenuType) then
                BackGroundColor=DarkBlue
                if(SbwItemColor(i,id).eq.Black) then
                  TextColor=White
                else
                  TextColor=SbwItemColor(i,id)
                endif
              else if(SbwType(id).eq.SbwSelectType) then
                BackGroundColor=SnowGray
                if(SbwItemSel(n,id).eq.1) then
                  TextColor=Red
                else
                  if(SbwItemSel(n,id).eq.0) then
                    TextColor=SbwItemColor(i,id)
                  else
                    TextColor=Gray
                  endif
                endif
              endif
            else
              BackgroundColor=White
              if(SbwType(id).eq.SbwMenuType) then
                TextColor=SbwItemColor(i,id)
              else if(SbwType(id).eq.SbwSelectType) then
                if(SbwItemSel(n,id).eq.1) then
                  TextColor=Red
                else
                  if(SbwItemSel(n,id).eq.0) then
                    TextColor=SbwItemColor(i,id)
                  else
                    TextColor=Gray
                  endif
                endif
              else
                TextColor=Black
              endif
            endif
            if(SbwType(id).ne.SbwPureType) then
              if((SbwBars(id).eq.SbwHorizontal.or.
     1            SbwBars(id).eq.SbwBoth).and.SbwStripes(id).le.1) then
                t256=SbwItem(i,id)
              else
                t256=SbwItemShort(i,id)
              endif
              if(SbwType(id).eq.SbwTextType) call FeSetFixFont
              call FeWrLine(xpom,ypom,xd,t256,TextColor,BackgroundColor)
              if(SbwType(id).eq.SbwTextType) call FeSetPropFont
            endif
          endif
          if(mod(i,SbwLines(id)).le.0) then
            j=i/SbwLines(id)+1
            xpom=SbwXStripe(j,id)
            if(SbwType(id).eq.SbwTextType) then
              xpom=xpom+1.
              ypom=SbwYMax(id)-SbwYStep(id)*.5
              xd=0.
            else
              ypom=SbwYMax(id)
              xd=SbwXStripe(j+1,id)-xpom
            endif
          else
            ypom=ypom-SbwYStep(id)
          endif
        enddo
        do i=2,SbwStripes(id)
          xu(1)=SbwXStripe(i,id)
          xu(2)=xu(1)
          yu(1)=SbwYMin(id)
          yu(2)=SbwYMax(id)
          call FePolyLine(2,xu,yu,Black)
        enddo
      else
        call FeSbwActionSpecial(id)
      endif
      go to 9999
9900  ich=1
9999  return
      end
      subroutine FeSbwKresliPlovak(id)
      include 'fepc.cmn'
      klic=0
      go to 1000
      entry FeSbwPohniPlovak(id,yin,yout)
      klic=1
      ypom=yin
1000  if(SbwDelPlovak(id).le.0) go to 9999
      XMinSloupek=SbwXMinPruh(1,id)
      XMaxSloupek=SbwXMaxPruh(1,id)
      YMinSloupek=anint(SbwYMaxPosun(2,id)*EnlargeFactor+3.)/
     1            EnlargeFactor
      YMaxSloupek=anint(SbwYMinPosun(1,id)*EnlargeFactor-3.)/
     1            EnlargeFactor
      YDelSloupek=anint((YMaxSloupek-YMinSloupek)*EnlargeFactor-
     1                  float(SbwDelPlovak(id)))/EnlargeFactor
      call FeFillRectangle(XMinSloupek,XMaxSloupek,
     1                     YMinSloupek,YMaxSloupek,4,0,0,WhiteGray)
      if(Klic.eq.0) then
        ypom=YMaxSloupek-YDelSloupek*float(SbwItemFrom(id)-1)/
     1                               float(SbwItemNMax(id)-SbwLines(id))
      else
        ypom=min(ypom,YMaxSloupek)
        ypom=max(ypom,anint(YMinSloupek*EnlargeFactor+
     1                      float(SbwDelPlovak(id)))/EnlargeFactor)
        yout=ypom
        n=nint((YMaxSloupek-ypom)/YDelSloupek*
     1         float(SbwItemNMax(id)-SbwLines(id)))+1
        call FeSbwShowPohniUpDown(id,n)
      endif
      SbwXMinPlovak(id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+2.)/
     1                  EnlargeFactor
      SbwXMaxPlovak(id)=anint(SbwXMaxPruh(1,id)*EnlargeFactor-2.)/
     1                  EnlargeFactor
      SbwYMaxPlovak(id)=anint(ypom*EnlargeFactor-2.)/EnlargeFactor
      SbwYMinPlovak(id)=anint(SbwYMaxPlovak(id)*EnlargeFactor-
     1                   float(SbwDelPlovak(id)-4))/EnlargeFactor
      xLD=SbwXMinPlovak(id)
      xRU=SbwXMaxPlovak(id)
      wp=xRU-xLD
      yLD=SbwYMinPlovak(id)
      yRU=SbwYMaxPlovak(id)
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,LightGray)
      call FeDrawFrame(xLD,yLD,wp,hp,2./EnlargeFactor,Gray,White,Gray,
     1                 .false.)
9999  return
      end
      subroutine FeSbwKresliStrelku(id)
      include 'fepc.cmn'
      klic=0
      go to 1000
      entry FeSbwPohniStrelku(id,xin,xout)
      klic=1
      xpom=xin
1000  if(SbwDelStrelka(id).le.0) go to 9999
      XMinSloupek=anint(SbwXMaxPosun(3,id)*EnlargeFactor+3.)/
     1            EnlargeFactor
      XMaxSloupek=anint(SbwXMinPosun(4,id)*EnlargeFactor-3.)/
     1            EnlargeFactor
      XDelSloupek=anint((XMaxSloupek-XMinSloupek)*EnlargeFactor-
     1                  float(SbwDelStrelka(id)))/EnlargeFactor
      YMinSloupek=SbwYMinPruh(2,id)
      YMaxSloupek=SbwYMaxPruh(2,id)
      call FeFillRectangle(XMinSloupek,XMaxSloupek,
     1                     YMinSloupek,YMaxSloupek,
     2                     4,0,0,WhiteGray)
      if(Klic.eq.0) then
        if(SbwStripes(id).gt.1) then
          xpom=XMinSloupek+XDelSloupek*float(SbwStripeFrom(id)-1)/
     1                     float(SbwStripesMax(id)-SbwStripes(id))
        else
          xpom=XMinSloupek+XDelSloupek*float(SbwColumnFrom(id)-1)/
     1                     float(SbwColumnsMax(id)-SbwColumns(id))
        endif
      else
        xpom=min(xpom,anint(XMaxSloupek*EnlargeFactor-
     1                float(SbwDelStrelka(id)))/EnlargeFactor)
        xpom=max(xpom,XMinSloupek)
        xout=xpom
        if(SbwStripes(id).gt.1) then
          n=(nint((xpom-XMinSloupek)/XDelSloupek*
     1       float(SbwStripesMax(id)-SbwStripes(id))))*SbwLines(id)+1
        else
          n=nint((xpom-XMinSloupek)/XDelSloupek*
     1           float(SbwColumnsMax(id)-SbwColumns(id)))+1
        endif
        call FeSbwShowPohniLeftRight(id,n)
      endif
      SbwYMinStrelka(id)=anint(SbwYMinPruh(2,id)*EnlargeFactor+2.)/
     1                   EnlargeFactor
      SbwYMaxStrelka(id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-2.)/
     1                   EnlargeFactor
      SbwXMinStrelka(id)=anint(xpom*EnlargeFactor+2.)/EnlargeFactor
      SbwXMaxStrelka(id)=anint(SbwXMinStrelka(id)*EnlargeFactor+
     1                         float(SbwDelStrelka(id))-4)/
     2                   EnlargeFactor
      xLD=SbwXMinStrelka(id)
      xRU=SbwXMaxStrelka(id)
      wp=xRU-xLD
      yLD=SbwYMinStrelka(id)
      yRU=SbwYMaxStrelka(id)
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,LightGray)
      call FeDrawFrame(xLD,yLD,wp,hp,2./EnlargeFactor,Gray,White,Gray,
     1                 .false.)
9999  return
      end
      subroutine FeSbwSetItemSel(isp,n)
      include 'fepc.cmn'
      do is=isp,isp+n-1
        if(SbwItemSel(is,SbwActive).ge.0)
     1    SbwItemSel(is,SbwActive)=SbwItemSelLast
      enddo
      return
      end
      subroutine FeFlowChart
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) text,TextBut1,TextBut2
      character*80  TextOld,t80
      integer FeGetSystemTime
      logical ButtonActive(3),AlreadyOpened,WizardModeIn
      save ButtonActive,xm,ym,xmnp,xmxp,xd,yd,ymnp,ymxp,id,
     1     NFlowModSave,NFlowMaxSave,FlowRatio,istart,xold,Procenta,
     2     t80
      data AlreadyOpened,WizardModeIn/2*.false./
      go to 9999
      entry FeFlowChartOpen(xmi,ymi,NFlowMod,NFlowMax,Text,TextBut1,
     1                       TextBut2)
      if(SilentRun) go to 9999
      t80='0%'
      WizardModeIn=WizardMode
      WizardMode=.false.
      if(AlreadyOpened) go to 9999
      if(NFlowMax.le.0) then
        NFlowMaxSave=1000
      else
        NFlowMaxSave=NFlowMax
      endif
      if(NFlowMod.le.0) then
        NFlowModSave=10
      else
        NFlowModSave=NFlowMod
      endif
      if(BatchMode.or.Console) then
        m=0
        call FeLstWriteLine(Text,-1)
        ilb=-1
      else
        istart=-1
        xold=-1.
        call FeBoldFont
        xd=max(FeTxLength(Text)+20.,200.)
        call FeNormalFont
        TextOld='%Cancel'
        xdt=max(FeTxLengthUnder(TextOld),FeTxLengthUnder(TextBut1),
     1          FeTxLengthUnder(TextBut2))+10.
        nBut=1
        if(TextBut1.ne.' ') nBut=nBut+1
        if(TextBut2.ne.' ') nBut=nBut+1
        xd=max(xd,xdt*float(nBut)+10.*float(nBut-1)+20.)
        yd=80.
        id=NextQuestid()
        call FeQuestAbsCreate(id,xmi,ymi,xd,yd,Text,0,LightGray,-1,-1)
        xm=QuestXMin(id)
        ym=QuestYMin(id)
        xmnp=anint(xm*EnlargeFactor+20.)/EnlargeFactor
        xmxp=anint((xm+xd)*EnlargeFactor-20.)/EnlargeFactor
        ymxp=anint((ym+yd)*EnlargeFactor-25.)/EnlargeFactor
        ymnp=anint((ym+yd)*EnlargeFactor-41.)/EnlargeFactor

        xmnp=anint((xm+20.)*EnlargeFactor)/EnlargeFactor
        xmxp=anint((xm+xd-20.)*EnlargeFactor)/EnlargeFactor
        ymxp=anint((ym+yd-25.)*EnlargeFactor)/EnlargeFactor
        ymnp=anint((ym+yd-41.)*EnlargeFactor)/EnlargeFactor
        call FeDrawFrame(xmnp,ymnp,xmxp-xmnp,ymxp-ymnp,2.,White,
     1                   Gray,Black,.false.)
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xu(1)=anint((xmnp-1.)*EnlargeFactor)/EnlargeFactor
        yu(1)=anint((ymnp-1.)*EnlargeFactor)/EnlargeFactor
        xu(2)=anint((xmxp+1.)*EnlargeFactor)/EnlargeFactor
        yu(2)=yu(1)
        xu(3)=xu(2)
        yu(3)=anint((ymxp+1.)*EnlargeFactor)/EnlargeFactor
        xu(4)=xu(1)
        yu(4)=yu(3)
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,Gray)
        xpom=xd*.5-xdt*float(nBut)*.5-10.*float((nBut-1))*.5
        ypom=10.
        do i=1,3
          if(i.eq.1) then
            t80=TextBut1
          else if(i.eq.2) then
            t80=TextBut2
          else if(i.eq.3) then
            t80=TextOld
          endif
          ButtonActive(i)=t80.ne.' '
          call FeQuestAbsButtonMake(id,xpom,ypom,xdt,ButYd,t80)
          if(ButtonActive(i)) xpom=xpom+xdt+10.
          if(i.eq.1) nButPrvni=ButtonLastMade
        enddo
        nBut=nButPrvni
        j=0
        do i=1,3
          if(ButtonActive(i)) then
            call FeQuestButtonOpen(nBut,ButtonOff)
            if(j.eq.0) j=nBut+ButtonFr-1
          endif
          nBut=nBut+1
        enddo
        call FeMoveMouseTo((ButtonXMin(j)+ButtonXMax(j))*.5,
     1                     (ButtonYMin(j)+ButtonYMax(j))*.5)
        FlowRatio=(xmxp-xmnp)/float(NFlowMaxSave)
        TakeMouseMove=.true.
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      AlreadyOpened=.true.
      m=0
      go to 3000
      entry FeFlowChartReopen(Text)
      if(SilentRun) go to 9999
      Procenta=0.
      if(BatchMode.or.Console) then
        call FeLstWriteLine(Text,-1)
        ilb=-1
      else
        call FeQuestTitleRemove(id)
        call FeQuestTitleMake(id,Text)
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      m=0
      go to 3000
      entry FeFlowChartRefresh
      if(SilentRun) go to 9999
      if(BatchMode.or.Console) then
        Procenta=0.
        ilb=0
      else
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      m=0
      go to 3000
      entry FeFlowChartEvent(n,ie)
      ie=0
      if(SilentRun) go to 9999
      if(BatchMode.or.Console) then
        if(BatchMode) then
          call FeEvent(1)
          if(EventType.eq.EventCtrl) then
            if(char(EventNumber).eq.'C') then
              ie=3
            endif
          endif
        endif
      else
        ipom=FeGetSystemTime()
        if(istart.eq.-1) then
          istart=ipom
          icas=99999
        else
          icas=ipom-istart
          if(icas.ge.50) istart=ipom
        endif
        if(icas.ge.50) then
          call FeFlush
          call FeEvent(1)
          if(XPos.ge.xm.and.XPos.le.xm+xd.and.
     1       YPos.ge.ym.and.YPos.le.ym+yd) then
            call FeMouseShape(0)
          else
            call FeMouseShape(3)
          endif
          if(EventType.eq.EventButton.and.EventNumber.ge.1.and.
     1       EventNumber.le.3) then
            ie=EventNumber
          else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape)
     1      then
            ie=3
          else
            ie=0
          endif
        endif
      endif
      if(n.lt.imax) then
        n=n+1
      else
        go to 9999
      endif
      if(mod(n,NFlowModSave).ne.0.and.n.ne.NFlowMaxSave) go to 9999
      m=min(n,NFlowMaxSave)
      ilb=0
3000  if(BatchMode.or.Console) then
        i=nint(float(m)/float(NFlowMaxSave)*100.)
        if(mod(i,10).eq.0) then
          write(t80,'(i4,''%'')') i
          call FeLstWriteLine(t80,ilb)
        endif
      else
        pom=xmnp+float(m)*FlowRatio
        if(pom-xold.ge.1..or.pom.ge.xmxp-.1) then
          xold=pom
          call FeFillRectangle(xmnp,pom,ymnp,ymxp,4,0,0,Gray)
          if(pom.lt.xmxp-.1)
     1      call FeFillRectangle(pom,xmxp,ymnp,ymxp,4,0,0,White)
          if(DeferredOutput) then
            call FeReleaseOutput
            call FeDeferOutput
          else
            call FeFlush
          endif
        endif
      endif
      go to 9999
      entry FeFlowChartRemove
      if(SilentRun.or.Console) go to 9000
      if((id.gt.0.or.BatchMode).and.AlreadyOpened) then
        TakeMouseMove=.false.
        call FeMouseShape(0)
        if(BatchMode) then
          call FeLstBackSpace
          call FeLstBackSpace
        else
          call FeQuestRemove(id)
        endif
        id=0
        WizardMode=WizardModeIn
      endif
9000  AlreadyOpened=.false.
9999  return
      end
      subroutine FeQuestActiveObjOff
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        call FeButtonDeactivate(j)
      else if(i.eq.ActEdw) then
        call FeEdwHighlight(j)
        EdwSelFr=0
        EdwSelTo=0
        call FeEdwDeactivate(j)
        EdwActive=0
      else if(i.eq.ActCrw) then
        call FeCrwDeactivate(j)
      else if(i.eq.ActRolMenu) then
        call FeRolMenuDeactivate(j)
      else if(i.eq.ActSbw) then
        call FeSbwDeactivate(j)
        SbwActive=0
      else
        go to 9999
      endif
      ActiveObj=0
9999  return
      end
      subroutine FeQuestActiveUpdate
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      if(ActiveObj.le.0) then
        if(SbwTo.ge.SbwFr) then
          ActiveObj=10000*ActSbw+SbwTo
          go to 1200
        else if(EdwTo.ge.EdwFr) then
          do nEdw=EdwFr,EdwTo
            if(EdwState(nEdw).eq.EdwOpened) then
              ActiveObj=10000*ActEdw+nEdw
              go to 1200
            endif
          enddo
        endif
        if(ButtonOK.ne.0) then
          i=ButtonOK
        else if(ButtonESC.ne.0) then
          i=ButtonESC
        else
          go to 9999
        endif
        ActiveObj=10000*ActButton+i
1200    call FeQuestActiveObjOn
        go to 9999
      endif
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        if(ButtonState(j).eq.ButtonClosed.or.
     1     ButtonState(j).eq.ButtonDisabled) go to 1500
      else if(i.eq.ActEdw) then
        if(EdwState(j).eq.EdwClosed.or.
     1     EdwState(j).eq.EdwDisabled) go to 1500
      else if(i.eq.ActCrw) then
        if(CrwState(j).eq.CrwClosed.or.
     1     CrwState(j).eq.CrwDisabled) go to 1500
      else if(i.eq.ActRolMenu) then
        if(RolMenuState(j).eq.RolMenuClosed.or.
     1     RolMenuState(j).eq.RolMenuDisabled) go to 1500
      else if(i.eq.ActSbw) then
        if(SbwState(j).eq.SbwClosed) go to 1500
      else if(ActiveObj.eq.0) then
        go to 1500
      endif
      go to 9999
1500  i=LocateInIntArray(ActiveObj,ActiveObjList(ActiveObjFr),
     1                   ActiveObjTo-ActiveObjFr+1)
2200  if(i.gt.0) then
        j=i+ActiveObjFr
        if(j.gt.ActiveObjTo) j=ActiveObjFr
      else
        j=ActiveObjFr
      endif
      ActiveObj=ActiveObjList(j)
      call FeQuestActiveObjOn
      if(ActiveObj.lt.0) then
        i=mod(i,ActiveObjTo-ActiveObjFr+1)+1
        go to 2200
      endif
      go to 9999
9000  ActiveObj=-1
9999  return
      end
      subroutine FeQuestActiveObjOn
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        if(ButtonState(j).ne.ButtonClosed.and.
     1     ButtonState(j).ne.ButtonDisabled) then
          call FeButtonActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActEdw) then
        if(EdwState(j).ne.EdwClosed.and.
     1     EdwState(j).ne.EdwDisabled.and.
     2     EdwState(j).ne.EdwLocked) then
          EdwActive=j
          call FeEdwActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActCrw) then
        if(CrwState(j).ne.CrwClosed.and.
     1     CrwState(j).ne.CrwDisabled.and.
     2     CrwState(j).ne.CrwLocked) then
          call FeCrwActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActRolMenu) then
        if(RolMenuState(j).ne.RolMenuClosed) then
          call FeRolMenuActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActSbw) then
        SbwActive=j
        call FeSbwActivate(j)
      else
        ActiveObj=0
      endif
      go to 9999
9000  ActiveObj=-1
9999  return
      end
      subroutine Roleta(x,y,n,
     1                  RolI,RolM,RolZ,RolA,RolN,
     2                  SubI,SubM,SubZ,SubA,SubN,
     3                  Action)
      include 'fepc.cmn'
      integer RolN,SubN(n),RolI,RolIo,SubI,SubIo,Color1,Color2
      character*(*) RolM(RolN),action,RolZ,SubM(n,RolN),SubZ(n)
      logical RolA(RolN),SubA(n,RolN)
      common/subrol/ SubState,xsub,ysub,xdsub,ydsub
      integer SubState
      save xrol,xdrol,yrol,ydrol,RolIo,SubIo
      data RolIo/0/
      if(Action.eq.'create') then
        call FeDeferOutput
        xrol=x+2.
        yrol=y-2.
        ydrol=float(RolN)*MenuLineWidth
        xdrol=0.
        xpom=5.
        do i=1,RolN
          if(SubN(i).gt.0) xpom=30.
          xdrol=max(FeTxLengthUnder(RolM(i)),xdrol)
        enddo
        xdrol=xdrol+xpom+2.*EdwMarginSize
        xrol=anint(xrol)
        xdrol=anint(xdrol)
        yrol=anint(yrol)
        ydrol=anint(ydrol)
        call FeSaveImage(xrol-2.,xrol+xdrol+2.,yrol-ydrol-2.,yrol+2.,
     1                   'roleta.bmp')
        call FeFillRectangle(xrol-1.,xrol+xdrol+1.,yrol+1.,
     1                       yrol-ydrol-1.,4,0,0,LightGray)
        call FeDrawFrame(xrol,yrol-ydrol,xdrol,ydrol,2.,Gray,
     1                   White,Black,.false.)
        ypom=yrol
        do i=1,RolN
          if(RolA(i)) then
            if(i.eq.RolI) then
              Color1=White
              Color2=DarkBlue
              k=1
            else
              Color1=Black
              Color2=LightGray
              k=0
            endif
          else
            if(i.eq.RolI) then
              Color1=Gray
              Color2=DarkBlue
              k=1
            else
              Color1=WhiteGray
              Color2=LightGray
              k=0
            endif
          endif
          call FeWrMenuItemO(xrol,ypom,xdrol,RolM(i),RolZ(i:i),
     1                       SubM(1,i),SubZ(i),SubA(1,i),SubN(i),
     2                       Color1,Color2,k)
          if(VasekTest.eq.1.and.index(RolZ(:i-1),RolZ(i:i)).gt.0) then
            EdwString(1)='Duplicate identification : '//RolZ
            call FeMsgOut(-1.,-1.,EdwString(1))
          endif
          ypom=ypom-MenuLineWidth
        enddo
      else if(Action.eq.'remove') then
        call FeDeferOutput
        if(SubState.eq.1) then
          call FeLoadImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                     'subrol.bmp',0)
          SubState=0
        endif
        call FeLoadImage(xrol-2.,xrol+xdrol+2.,yrol-ydrol-2.,yrol+2.,
     1                   'roleta.bmp',0)
        RolI=0
        SubI=0
      else if(Action.eq.'test') then
        if(RolI.gt.0.and.SubState.ne.0) then
          if(Xpos.ge.xsub.and.Xpos.le.xsub+xdsub.and.
     1       Ypos.ge.ysub-ydsub.and.Ypos.le.ysub) then
            l=min(ifix((ysub-Ypos)/MenuLineWidth)+1,SubN(RolI))
          else if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1            Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
            l=0
          else
            l=-1
          endif
          if(l.ne.SubI) then
            call FeDeferOutput
            if(SubI.gt.0) then
              if(SubA(SubI,RolI)) then
                Color1=Black
                Color2=LightGray
              else
                Color1=WhiteGray
                Color2=LightGray
              endif
              call FeWrMenuItem(xsub,ysub-float(SubI-1)*MenuLineWidth,
     1                          xdsub,SubM(SubI,RolI),
     2                          SubZ(RolI)(SubI:SubI),
     3                          Color1,Color2)
            endif
            if(l.gt.0) then
              if(SubA(l,RolI)) then
                Color1=White
                Color2=DarkBlue
              else
                Color1=Gray
                Color2=DarkBlue
              endif
              call FeWrMenuItem(xsub,ysub-float(l-1)*MenuLineWidth,
     1                          xdsub,SubM(l,RolI),SubZ(RolI)(l:l),
     2                          Color1,Color2)
            endif
          endif
          SubI=l
        endif
        if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1     Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
          i=min(ifix((yrol-Ypos)/MenuLineWidth)+1,RolN)
        else if(SubState.eq.0) then
          i=0
        else
          i=RolI
        endif
        if(i.ne.RolI.and.SubI.eq.0) then
          call FeDeferOutput
          if(RolI.gt.0) then
            if(RolA(RolI)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolI-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolI),RolZ(RolI:RolI),
     3                         SubM(1,RolI),SubZ(RolI),SubA(1,RolI),
     4                         SubN(RolI),Color1,Color2,-1)
          endif
          if(i.gt.0) then
            if(RolA(i)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItemO(xrol,yrol-float(i-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(i),RolZ(i:i),
     3                         SubM(1,i),SubZ(i),SubA(1,i),
     4                         SubN(i),Color1,Color2,1)
          endif
          RolI=i
        endif
      else if(Action.eq.'check') then
        if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1     Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
          RolI=min(ifix((yrol-Ypos)/MenuLineWidth)+1,RolN)
        else
          RolI=0
        endif
        go to 9999
      else if(Action.eq.'switch') then
        if(RolIo.ne.RolI) then
          call FeDeferOutput
          if(RolIo.gt.0) then
            if(RolA(RolIo)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolIo-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolIo),RolZ(RolIo:RolIo),
     3                         SubM(1,RolIo),SubZ(RolIo),
     4                         SubA(1,RolIo),SubN(RolIo),Color1,Color2,
     5                         -1)
          endif
          if(RolI.gt.0) then
            if(RolA(RolI)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolI-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolI),RolZ(RolI:RolI),
     3                         SubM(1,RolI),SubZ(RolI),
     4                         SubA(1,RolI),SubN(RolI),Color1,Color2,1)
          endif
        else if(SubIo.ne.SubI) then
          call FeDeferOutput
          if(SubIo.gt.0) then
            if(SubA(SubIo,RolI)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItem(xsub,ysub-float(SubIo-1)*MenuLineWidth,
     1                        xdsub,
     2                        SubM(SubIo,RolI),SubZ(RolI)(SubIo:SubIo),
     3                        Color1,Color2)
          endif
          if(SubI.gt.0) then
            if(SubA(SubI,RolI)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItem(xsub,ysub-float(SubI-1)*MenuLineWidth,
     1                        xdsub,
     2                        SubM(SubI,RolI),SubZ(RolI)(SubI:SubI),
     3                        Color1,Color2)
          endif
        endif
      endif
      RolIo=RolI
      SubIo=SubI
9999  return
      end
      subroutine FeWrMenuItemO(x,y,xd,Veta,StFlag,SubM,SubZ,SubA,SubN,
     1                         TextColor,BackGroundColor,klic)
      include 'fepc.cmn'
      common/subrol/ SubState,xsub,ysub,xdsub,ydsub
      integer SubState,SubN
      character*(*) Veta,SubM(SubN),SubZ
      character*1 StFlag
      integer TextColor,BackGroundColor
      logical SubA(SubN)
      if(SubN.gt.0.and.klic.eq.1) then
        xsub=x+xd-1.+2.
        ysub=y
        ydsub=float(SubN)*MenuLineWidth
        xdsub=0.
        do i=1,SubN
          xdsub=max(FeTxLengthUnder(SubM(i)),xdsub)
        enddo
        xdsub=xdsub+2.*EdwMarginSize
        xsub=anint(xsub)
        xdsub=anint(xdsub)
        ysub=anint(ysub)
        ydsub=anint(ydsub)
        call FeSaveImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                   'subrol.bmp')
      endif
      call FeFillRectangle(x,x+xd,y,y-MenuLineWidth,4,0,0,
     1                     BackGroundcolor)
      xpom=x+EdwMarginSize
      ypom=y-MenuLineWidth*.5
      call FeOutStUnder(0,xpom,ypom,Veta,'L',TextColor,TextColor,StFlag)
      if(SubN.gt.0) then
        yu(1)=ypom
        yu(2)=ypom-3.
        yu(3)=ypom+3.
        xu(1)=x+xd-3.
        xu(2)=xu(1)-4.
        xu(3)=xu(2)
        call FePolygon(xu,yu,3,4,0,0,TextColor)
        if(klic.eq.-1.and.SubState.eq.1) then
          call FeLoadImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                     'subrol.bmp',0)
          SubState=0
        else if(klic.eq.0) then
          SubState=0
        else if(klic.eq.1) then
          call FeFillRectangle(xsub-1.,xsub+xdsub+1.,ysub+1.,
     1                         ysub-ydsub-1.,4,0,0,LightGray)
          call FeDrawFrame(xsub,ysub-ydsub,xdsub,ydsub,2.,Gray,
     1                     White,Black,.false.)
          xp=xsub
          yp=ysub
          do i=1,SubN
            if(SubA(i)) then
              call FeWrMenuItem(xp,yp,xdsub,SubM(i),SubZ(i:i),Black,
     1                          LightGray)
            else
              call FeWrMenuItem(xp,yp,xdsub,SubM(i),SubZ(i:i),WhiteGray,
     1                          LightGray)
            endif
            yp=yp-MenuLineWidth
          enddo
          SubState=1
        endif
      else
        xsub =0.
        xdsub=0.
        ysub =0.
        ydsub=0.
        SubState=0
      endif
      return
      end
      subroutine FeToolBarButton(xp,xk,yp,yk,Text,State,klic)
      include 'fepc.cmn'
      character*(*) Text
      character*1 StFlag
      integer ColorRD,ColorLU,State
      if(State.eq.klic) go to 9999
      xpX=xp+2.
      xkX=xk-2.
      ypX=yp+2.
      ykX=yk-2.
      wp=xkX-xpX
      hp=ykX-ypX
      if(klic.eq.0) then
        ColorRD=LightGray
        ColorLU=LightGray
        xt=0.
        yt=0.
      else if(klic.lt.0) then
        ColorLU=White
        ColorRD=Gray
        xt=0.
        yt=0.
      else
        ColorRD=White
        ColorLU=Gray
        xt= 1.
        yt=-1.
      endif
      call FeDeferOutput
      call FeDrawFrame(xpX,ypX,wp,hp,2.,ColorRD,ColorLU,Black,.false.)
      call FeFillRectangle(xpX,xkX,ypX,ykX,4,0,0,LightGray)
      call FeOutStUnder(0,xp+10.+xt,(yp+yk)*.5+yt,Text,'L',Black,Black,
     1                  StFlag)
      State=klic
9999  return
      end
      subroutine FeOpenInterrupt(xmi,ymi,text)
      include 'fepc.cmn'
      character*(*) text
      logical Konec,WizardModeOld
      integer FeGetSystemTime
      save istart,nLbl,nButtInterrupt,id,WizardModeOld
      WizardModeOld=WizardMode
      WizardMode=.false.
      xd=max(50.,FeTxLength(text)+10.)
      id=NextQuestid()
      call FeQuestCreate(id,xmi,ymi,xd,2,' ',0,LightGray,-1,-1)
      call FeQuestLblMake(id,5.,1,Text,'L','N')
      nLbl=LblLastMade
      Cislo='%Interrupt'
      dpom=FeTxLength(Cislo)+20.
      call FeQuestButtonMake(id,(xd-dpom)*.5,2,dpom,ButYd,Cislo)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtInterrupt=ButtonLastMade
      go to 2000
      entry FeOutputInterrupt(Text)
      call FeQuestLblChange(nLbl,Text)
2000  istart=FeGetSystemTime()
      go to 9000
      entry FeEventInterrupt(Konec)
      call FeEvent(id)
      konec=(EventType.eq.EventButton.and.EventNumber.eq.1).or.
     1      (EventType.eq.EventKey.and.EventNumber.eq.JeEscape)
      ipom=FeGetSystemTime()
      icas=ipom-istart
      if(icas.ge.50) then
        call FeQuestLblOn(nLbl)
        istart=ipom
        go to 9000
      else
        go to 9999
      endif
      entry FeCloseInterrupt
      call FeQuestRemove(id)
9000  if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  WizardMode=WizardModeOld
      return
      end
      subroutine FeTxOut(xmi,ymi,text)
      include 'fepc.cmn'
      character*(*) text
      character*80 TextOld,TxoutTmp
      save xm,ym,xd,yd,TextOld,TxOutTmp
      xd=max(50.,FeTxLength(text)+30.)
      yd=40.
      xm=xmi
      ym=ymi
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      TxOutTmp='jtxo'
      if(OpSystem.le.0) call CreateTmpFile(TxOutTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,
     1                 ym-FrameWidth,ym+yd+FrameWidth,TxOutTmp)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,LightGray)
      call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                 LastQuest.ne.0)
      call FeOutSt(0,xm+10.,ym+yd-20.,Text(:idel(text)),'L',Black)
      TextOld=Text
      go to 9999
      entry FeTxOutCont(Text)
      call FeEvent(1)
      call FeRewriteString(0,xm+10.,ym+yd-20.,TextOld,Text,'L',
     1                     LightGray,Black)
c      if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
c      else
c        call FeFlush
c      endif
      TextOld=Text
      go to 9999
      entry FeTxOutEnd
      if(TxOutTmp.eq.' ') go to 9999
      call FeWait(.5)
      call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,
     1                 ym-FrameWidth,ym+yd+FrameWidth,TxOutTmp,0)
      TxOutTmp=' '
9999  return
      end
      integer function FeWhatToDo(xmi,ymi,ButtonLables,n,ist)
      include 'fepc.cmn'
      logical EqIgCase
      character*(*) ButtonLables(n)
      xd=0.
      do i=1,Ninfo
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
        xd=max(FeTxLength(TextInfo(i)),xd)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
      enddo
      gap=10.
      xb=0.
      do i=1,n
        xb=xb+FeTxLength(ButtonLables(i))+20.
      enddo
      xb=xb+float(n-1)*gap
      if(xb.gt.xd) then
        XButPoc=10.
        xd=xb+20.
      else
        XButPoc=(xd-xb)*.5+10.
        xd=xd+20.
      endif
      XTextPoc=10.
      yd=45.+float(Ninfo)*16.
      id=NextQuestid()
      call FeQuestAbsCreate(id,xmi,ymi,xd,yd,' ',0,LightGray,-1,-1)
      ypom=yd-10.
      do i=1,Ninfo
        if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
          xpom=xd*.5
        else
          xpom=XTextPoc
        endif
        call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(i),
     1                         TextInfoFlags(i)(1:1),
     2                         TextInfoFlags(i)(2:2))
        TextInfoFlags(i)='LN'
        ypom=ypom-16.
      enddo
      xpom=XButPoc
      ypom=15.
      do i=1,n
        dpom=FeTxLength(ButtonLables(i))+20.
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,
     1                            ButtonLables(i))
        if(i.eq.1) nButtFirst=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+gap
      enddo
      call FeQuestMouseToButton(ist)
2000  call FeEvent(0)
      if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        FeWhatToDo=EventNumber
        call FeQuestRemove(id)
        return
      endif
      go to 2000
      end
      subroutine FeMarker(x,y,Size,MarkerType,Color)
      include 'fepc.cmn'
      integer Color
      if(iabs(MarkerType).eq.1) then
        call FeCircle(x,y,Size,Color)
        if(MarkerType.lt.0) call FeCircle(x,y,Size-1.,Black)
      else if(iabs(MarkerType).eq.2) then
        call FeSquare(x,y,2.*Size,Color)
        if(MarkerType.lt.0) call FeSquare(x,y,2.*(Size-1.),Black)
      else if(iabs(MarkerType).eq.3) then
        call FeTriangl(x,y,2.*Size,Color)
        if(MarkerType.lt.0) call FeTriangl(x,y,2.*(Size-1.),Black)
      endif
      return
      end
      subroutine FeSquare(x,y,size,Color)
      include 'fepc.cmn'
      integer Color
      pom=Size/2.
      xp=x
      yp=y
      call FeFillRectangle(xp-pom,xp+pom,yp-pom,yp+pom,4,0,0,Color)
      return
      end
      subroutine FeTriangl(x,y,size,Color)
      include 'fepc.cmn'
      integer Color
      pom=Size*sqrt(3.)*.5
      xp=x
      yp=y
      xu(1)=xp-pom
      xu(2)=xp+pom
      xu(3)=xp
      pom=Size
      yu(3)=yp+pom*.66666667
      yu(1)=yu(3)-pom
      yu(2)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,Color)
      return
      end
      subroutine FeChybne(xmi,ymi,text1,text2,ErrType)
      include 'fepc.cmn'
      character*128 Ven1,Ven2,Veta
      character*(*) text1,text2
      integer ErrType,QuestLineWidthIn,UseTabsIn
      logical CrwLogicQuest,WizardModeIn,PropFontIn
      if(.not.ICDDBatch.and..not.Console) then
        PropFontIn=PropFont
        if(.not.PropFont) call FeSetPropFont
        WizardModeIn=WizardMode
        WizardMode=.false.
        QuestLineWidthIn=QuestLineWidth
        QuestLineWidth=16.
        if(DelejResize) go to 9999
      endif
      if(Text2.eq.' ') then
        Ven2=' '
      else
        Ven2=' '//Tabulator//' '//text2(:idel(text2))
      endif
      if(ErrType.eq.FatalError) then
        if(Language.eq.1) then
          Ven1='Z�sadn� chyba -'
        else
          Ven1='Fatal -'
        endif
      else if(ErrType.eq.SeriousError) then
        if(Language.eq.1) then
          Ven1='Chyba -'
        else
          Ven1='Error -'
        endif
      else
        if(Language.eq.1) then
          Ven1='Varov�n� - '
        else
          Ven1='Warning -'
        endif
      endif
      if(.not.ICDDBatch.and..not.Console) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        call FeTabsAdd(FeTxLength(Ven1),UseTabs,IdRightTab,' ')
      endif
      Ven1=Ven1(:idel(Ven1))//Tabulator//' '//text1(:idel(text1))
      idv1=idel(Ven1)
      idv2=idel(Ven2)
      if(.not.ICDDBatch.and..not.Console) then
        if((ErrType.eq.SeriousError.and.IgnoreE).or.
     1     ((ErrType.eq.Warning.or.
     2       ErrType.eq.WarningWithESC).and.IgnoreW)) then
          if(AniNaListing) then
            go to 9000
          else
            go to 2000
          endif
        endif
        xqd=max(FeTxLength(Ven1),FeTxLength(Ven2))
        il=1
        if(idv2.gt.0) il=il+1
        if(ErrType.eq.WarningWithESC) il=il+4
        xqd=xqd+10.
        if(ErrType.eq.WarningWithESC.or.ErrType.eq.Warning) then
          WaitTime=max(nint(30.*float(idv1+idv2)/160.),5)*1000
          xqd=max(xqd,FeTxLength(ItRemains)+10.)
        endif
        id=NextQuestid()
        call FeQuestCreate(id,xmi,ymi,xqd,il,' ',0,LightGray,-1,0)
        tpom=5.
        il=1
        call FeQuestLblMake(id,tpom,il,Ven1,'L','N')
        if(idv2.gt.0) then
          il=il+1
          call FeQuestLblMake(id,tpom,il,Ven2,'L','N')
        endif
        if(ErrType.eq.WarningWithESC) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          j=idel(RunningProgram)
          Veta='%Continue '//RunningProgram(:j)
          xdl=FeTxLengthUnder(Veta)+CrwgXd+15.
          xpom=(xqd-xdl)*.5
          tpom=xpom+CrwgXd+15.
          do i=1,3
            il=il+1
            if(i.eq.2) then
              nCrwFirst=CrwLastMade
              Veta='%Skip warnings'
            else if(i.eq.3) then
              Veta='%Break'
            endif
            if(i.gt.2.and.j.ne.0)
     1        Veta=Veta(:idel(Veta)+1)//RunningProgram(:j)
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,1)
            call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          enddo
        else
          nCrwFirst=0
        endif
        if(TakeMouseMove) call FeMouseShape(0)
1000    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
          go to 1100
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1000
        endif
        if(ErrType.eq.WarningWithESC) then
          nCrw=nCrwFirst
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.2) then
                IgnoreW=.true.
              else if(i.eq.3) then
                ErrFlag=1
              endif
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
1100    call FeQuestRemove(id)
        call FeReleaseOutput
        call FeDeferOutput
      endif
2000  if(LstOpened.or.ICDDBatch.or.Console) then
        call DelChar(Ven1,Tabulator)
        idv1=idv1-1
        if(idv2.gt.0) then
          call DelChar(Ven2,Tabulator)
          call DeleteFirstSpaces(Ven2)
          j=index(Ven1,'-')+1
          do i=1,j
            Ven2=' '//Ven2(:idel(Ven2))
          enddo
          idv2=idel(Ven2)
        endif
        if(LstOpened) then
          if(idv2.ne.0) then
            call newln(2)
          else
            call newln(1)
          endif
          write(lst,FormA) Ven1(:idv1)
          if(idv2.gt.0) write(lst,FormA) Ven2(:idv2)
        endif
        if(ICDDBatch) then
          call FeLstWriteLine(Ven1,-1)
          if(idv2.gt.0) call FeLstWriteLine(Ven2,-1)
          if(LogLn.ne.0) then
            write(LogLn,FormA) Ven1(:idv1)
            if(idv2.gt.0) write(LogLn,FormA) Ven2(:idv2)
          endif
        else if(Console) then
          write(6,FormA) ' '//Ven1(:idv1)
          if(idv2.gt.0) write(6,FormA) ' '//Ven2(:idv2)
        endif
      endif
      if(Console) go to 9999
9000  if(ErrType.eq.FatalError) then
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        stop
      endif
      if(.not.ICDDBatch) then
        WizardMode=WizardModeIn
        QuestLineWidth=QuestLineWidthIn
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
        if(.not.PropFontIn) call FeSetFixFont
      endif
      if(VasekTest.eq.999.and..not.IgnoreW.and..not.IgnoreE)
     1  call FeWinMessage('Cesta?',' ')
9999  return
      end
      subroutine FeErrAllocMessage(String1,String2)
      include 'fepc.cmn'
      character*(*) String1,String2
      NInfo=2
      TextInfo(1)='It has happened during allocation of '//
     1            String1(:idel(String1))
      TextInfo(2)=String2
      call FeInfoOut(-1.,-1.,'ALLOCATION ERROR','L')
      return
      end
      subroutine FeMojeChyba(ch)
      include 'fepc.cmn'
      character*(*) ch
      TextInfo(1)='This is a bug; its name is "'//ch(1:idel(ch))//'".'
      TextInfo(2)='Please write down the name and inform the authors.'
      NInfo = 2
      call FeInfoOut(-1.,-1.,'Fatal','L')
      return
      end
      subroutine FeReadRealMat(xmi,ymi,Label,SmbV,ChangeStyle,RMat,
     1                         RMatI,n,CheckSing,CheckPosDef,ich)
      include 'fepc.cmn'
      character*(*) SmbV(n),Label
      character*80 t80
      integer ChangeStyle
      logical CheckSing,CheckPosDef
      dimension RMat(n,n),RMatI(n,n),RMatTr(:,:),tpoma(:),xpoma(:)
      allocatable RMatTr,tpoma,xpoma
      allocate(RMatTr(n,n),tpoma(n),xpoma(n))
      ich=0
      id=NextQuestId()
      xqd=150.
      dpom=50.
      xqd=10.+dpom*float(n)+(FeTxLength(SmbV(1)//'XX')+12.)*
     1                      float(n+1)
      xqd=max(FeTxLength(Label)+20.,xqd)
      il=n+2
      call FeQuestCreate(id,xmi,ymi,xqd,il,Label,1,LightGray,0,0)
      il=0
      do i=1,n
        il=il+1
        tpom=5.
        t80=SmbV(i)
        if(ChangeStyle.eq.IdChangeCase) then
          call ChangeCase(t80)
        else if(ChangeStyle.eq.IdAddPrime) then
          t80=t80(:idel(t80))//''''
        endif
        t80=t80(:idel(t80))//'='
        call FeQuestLblMake(id,tpom,il,t80,'L','N')
        do j=1,n
          if(i.eq.1) then
            if(j.eq.1) then
              xpoma(j)=tpom+FeTxLength(t80)+6.
            else
              xpoma(j)=tpoma(j-1)+FeTxLength(t80)+6.
            endif
          endif
          t80='*'//SmbV(j)(:idel(SmbV(j)))
          if(j.ne.n) t80=t80(:idel(t80))//'+'
          if(i.eq.1) tpoma(j)=xpoma(j)+dpom+6.
          call FeQuestEdwMake(id,tpoma(j),il,xpoma(j),il,t80,'L',dpom,
     1                        EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwFirst=1
          call FeQuestRealEdwOpen(EdwLastMade,RMat(j,i),.false.,.false.)
        enddo
      enddo
      il=il+1
      t80='Set %unit matrix'
      tpom=FeTxLength(t80)+20.
      xpom=xqd*.5-tpom-10.
      call FeQuestButtonMake(id,xpom,-10*il-2,tpom,ButYd,t80)
      nButtUnit=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      t80='Set to %zeros'
      xpom=xpom+20.+tpom
      call FeQuestButtonMake(id,xpom,-10*il-2,tpom,ButYd,t80)
      nButtZeros=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      t80='Matrix %calculator'
      dpom=FeTxLengthUnder(t80)+10.
      tpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,tpom,-10*il-2,dpom,ButYd,t80)
      nButtMatCalc=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        nEdw=nEdwFirst
        Suma=0.
        do i=1,n
          do j=1,n
            call FeQuestRealFromEdw(nEdw,RMat(j,i))
            Suma=max(Suma,abs(RMat(j,i)))
            nEdw=nEdw+1
          enddo
        enddo
        if(Suma.le.0.) then
          Suma=.0001
          pom=0.
          go to 2150
        else
          Suma=Suma*.0001
        endif
        call matinv(RMat,RMati,pom,n)
2150    if(CheckSing.or.CheckPosDef) then
          if(abs(pom).lt.Suma.and.CheckSing) then
            call FeChybne(-1.,YBottomMessage,
     1                    'The matrix is singular - try again',
     2                    ' ',SeriousError)
            go to 1500
          else if(pom.lt.0..and.CheckPosDef) then
            call FeChybne(-1.,YBottomMessage,
     1                    'The matrix has negative determinant '//
     2                    '- try again',' ',SeriousError)
            go to 1500
          endif
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtUnit) then
          call UnitMat(RMat,n)
        else if(CheckNumber.eq.nButtZeros) then
          call SetRealArrayTo(RMat,n**2,0.)
        else
          NRow=n
          NCol=n
          nEdw=nEdwFirst
          do i=1,n
            do j=1,n
              call FeQuestRealFromEdw(nEdw,RMat(j,i))
              nEdw=nEdw+1
            enddo
          enddo
          call TrMat(RMat,RMatTr,n,n)
          call MatrixCalculatorInOut(RMatTr,NRow,NCol,ichp)
          if(ichp.eq.0) then
            call TrMat(RMatTr,RMat,n,n)
          else
            go to 1500
          endif
        endif
        nEdw=nEdwFirst
        do i=1,n
          do j=1,n
            call FeQuestRealEdwOpen(nEdw,RMat(j,i),.false.,.false.)
            nEdw=nEdw+1
          enddo
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      deallocate(RMatTr,tpoma,xpoma)
      return
      end
      subroutine FeInfoOut(xmi,ymi,Text,Justify)
      include 'fepc.cmn'
      character*(*) Text
      character*1 Justify,JustifyActual,JustifyFinal
      logical WizardModeIn,EqIgCase,PropFontIn
      if(SilentRun) go to 9999
      JustifyActual=Justify
      go to 1200
      entry FeMsgOut(xmi,ymi,Text)
      if(SilentRun) go to 9999
      NInfo=0
      JustifyActual='?'
1200  if(Console) then
        write(6,FormA) ' '//Text(:idel(Text))
        do i=1,Ninfo
          write(6,FormA) ' '//TextInfo(i)(:idel(TextInfo(i)))
        enddo
      else
        PropFontIn=PropFont
        if(.not.PropFont) call FeSetPropFont
        WizardModeIn=WizardMode
        WizardMode=.false.
        call FeBoldFont
        xd=FeTxLength(Text)
        call FeNormalFont
        do i=1,Ninfo
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=0
          endif
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
          xd=max(FeTxLength(TextInfo(i)),xd)
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
        enddo
        xd=xd+10.
        if(WaitTime.gt.0) xd=max(FeTxLength(ItRemains)+10.,xd)
        id=NextQuestid()
        il=-float(NInfo)*6
        call FeQuestCreate(id,xmi,ymi,xd,il,Text,0,LightGray,-1,0)
        il=-2
        do i=1,Ninfo
          il=il-6
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=0
          endif
          if(EqIgCase(JustifyActual,'?')) then
            JustifyFinal=TextInfoFlags(i)(1:1)
          else
            JustifyFinal=JustifyActual
          endif
          if(EqIgCase(JustifyFinal,'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        JustifyFinal,TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
1500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          go to 1500
        endif
        call FeQuestRemove(id)
        WizardMode=WizardModeIn
        UseTabs=0
        if(.not.PropFontIn) call FeSetFixFont
      endif
9999  return
      end
      subroutine FeWizardTextInfo(Title,Back,Next,Cancel,ich)
      include 'fepc.cmn'
      character*(*) Title
      integer Back,Next,Cancel
      id=NextQuestId()
      ypom=QuestYLen(id)*.5+float(NInfo)*6.
      if(Title.ne.' ') then
        ypom=ypom+12.
        call FeQuestAbsLblMake(id,WizardLength*.5,ypom,Title,'C','B')
        ypom=ypom-12.
      endif
      tpom=(WizardLength-FeTxLength(TextInfo(1)))*.5
      do i=1,NInfo
        ypom=ypom-12.
        call FeQuestAbsLblMake(id,tpom,ypom,TextInfo(i),'L','N')
      enddo
      if(Back.eq.0)
     1  call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
      if(Next.eq.0) then
        call FeQuestButtonDisable(ButtonOK-ButtonFr+1)
      else if(Next.lt.0) then
        call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
      endif
      if(Cancel.eq.0)
     1  call FeQuestButtonDisable(ButtonCancel-ButtonFr+1)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.gt.0) then
        if(Next.lt.0)
     1    call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
      endif
      return
      end
      subroutine FeFillTextInfo(FileTxt,Label)
      include 'fepc.cmn'
      character*(*) FileTxt
      character*128  Veta
      logical EqIgCase
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        WorkString=HomeDir(:idel(HomeDir))//'txt'//ObrLom//
     1             FileTxt(:idel(FileTxt))
      else
        WorkString=HomeDir(:idel(HomeDir))//'txt/'//
     1             FileTxt(:idel(FileTxt))
      endif
      call OpenFile(ln,WorkString,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      NInfo=0
      if(Label.ne.0) then
        write(Cislo,'(''block#'',i2)') Label
        call Zhusti(Cislo)
1000    read(ln,FormA,end=9999) Veta
        if(.not.EqIgCase(Veta,Cislo)) go to 1000
        Cislo='end '//Cislo(:idel(Cislo))
      endif
1500  NInfo=NInfo+1
      read(ln,FormA,end=2000) Veta
      if(Label.ne.0) then
        if(EqIgCase(Veta,Cislo)) go to 2000
      endif
      TextInfo(NInfo)=Veta
      TextInfoFlags(NInfo)='LN'
      go to 1500
2000  NInfo=NInfo-1
      if(TextInfo(NInfo).eq.' ') go to 2000
9999  call CloseIfOpened(ln)
      return
      end
      subroutine CekejNaEnter
      include 'fepc.cmn'
      if(OpSystem.le.0) then
        write(out,'('' Press enter to continue '',$)')
      else
        write(out,'(''Press enter to continue '',$)')
      endif
      read(dta,FormA)
      return
      end
      subroutine FeMsgShow(id,xmi,ymi,Poprve)
      include 'fepc.cmn'
      dimension xm(10),ym(10),xd(10),yd(10),NInfoMax(10)
      character*80 TextOld(20,10),MsgTmp(10),Ven
      logical Poprve,MsgUsed(10)
      save xm,ym,xd,yd,TextOld,MsgTmp,NInfoMax
      equivalence (TextOld,Ven)
      data MsgUsed/10*.false./
      MsgUsed(id)=.true.
      if(BatchMode.or.Console) then
        do i=1,Ninfo
          Ven=TextInfo(i)
          idl=idel(Ven)
          if(index(Ven,'^').gt.0) then
            do j=idel(Ven),1,-1
              if(Ven(j:j).eq.':'.or.Ven(j:j).eq.'^'.or.Ven(j:j).eq.' ')
     1          then
                Ven(j:j)=' '
              else
                exit
              endif
            enddo
          endif
          call FeLstWriteLine(Ven,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Ven(:idel(Ven))
        enddo
      else
        if(SilentRun) go to 9999
        if(.not.DeferredOutput) call FeDeferOutput
        call FeEvent(1)
        if(Poprve) NInfoMax(id)=NInfo
        xdp=0.
        do i=1,Ninfo
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=0
          endif
          xdp=max(FeTxLength(TextInfo(i))+10.,xdp)
          if(Poprve) TextOld(i,id)=' '
        enddo
        if(Poprve.or.xdp.gt.xd(id)) then
          if(.not.Poprve)
     1      call FeLoadImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     2                       ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     3                       MsgTmp(id),0)
          xd(id)=xdp
          if(xmi.lt.0.) then
c            xm(id)=XCenGrWin-xd(id)*.5
            xm(id)=XCenBasWin-xd(id)*.5
          else
            xm(id)=xmi-xd(id)
          endif
          if(Poprve) then
            yd(id)=float(Ninfo)*16.+10.
            if(ymi.lt.0.) then
              ym(id)=YCenGrWin-yd(id)*.5
            else
              ym(id)=ymi-yd(id)
            endif
            MsgTmp(id)='jmsg'
            if(OpSystem.le.0) then
              call CreateTmpFile(MsgTmp(id),i,1)
            else
              call CreateTmpFileName(MsgTmp(id),id)
            endif
          endif
          call FeSaveImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     1                     ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     2                     MsgTmp(id))
          call FeFillRectangle(xm(id)-1.,xm(id)+xd(id)+1.,ym(id)-1.,
     1                         ym(id)+yd(id)+1.,4,0,0,LightGray)
          call FeDrawFrame(xm(id),ym(id),xd(id),yd(id),FrameWidth,Gray,
     1                     White,Black,LastQuest.ne.0)
        endif
        ypom=ym(id)+yd(id)-12.
        xpom=xm(id)+5.
        do i=1,NinfoMax(id)
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=0
          endif
          j=idel(TextInfo(i))
          if(index(TextInfo(i),'^').gt.0) then
            do j=idel(TextInfo(i)),1,-1
              if(TextInfo(i)(j:j).eq.':'.or.TextInfo(i)(j:j).eq.'^')
     1          then
                TextInfo(i)(j:j)=' '
              else
                exit
              endif
            enddo
          endif
          if(i.le.NInfo) then
            call FeRewritePureString(0,xpom,ypom,TextOld(i,id),
     1                               TextInfo(i),'L',LightGray,Black)
          else
            call FeRewritePureString(0,xpom,ypom,TextOld(i,id),
     1                               TextOld(i,id),'L',LightGray,Black)
          endif
          TextOld(i,id)=TextInfo(i)
          ypom=ypom-16.
        enddo
        if(DeferredOutput) then
          call FeReleaseOutput
          call FeDeferOutput
        endif
      endif
      go to 9999
      entry FeMsgClear(id)
      if(SilentRun) go to 9999
      if(MsgUsed(id).and..not.BatchMode.and..not.Console)
     1  call FeLoadImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     2                   ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     3                   MsgTmp(id),0)
      MsgUsed(id)=.false.
9999  return
      end
      subroutine FeLstMake(xmi,ymi,idx,idy,Type,Header)
      include 'fepc.cmn'
      character*128  LstText(100),LstTextOld(100)
      character*1 FirstChar
      character*(*) Text,Header
      logical :: Overwrite=.false.
      integer Type
      save xd,yd,xm,ym,LstLine,LstLineMax,LstText,LstTextOld,LstFirst,
     1     MaxLength
      LstLine=0
      LstFirst=1
      if(Console) go to 9999
      if(.not.DeferredOutput) call FeDeferOutput
      call SetStringArrayTo(LstText,100,' ')
      LstLineMax=idy
      xd=float(idx)*FixFontWidthInPixels
      MaxLength=idx
      yd=float(idy)*FixFontHeightInPixels+FixFontHeightInPixels*.5
      if(xmi.lt.0.) then
        xm=XCenGrWin-xd*.5
      else
        xm=xmi
      endif
      if(ymi.lt.0.) then
        ym=YCenGrWin-yd*.5
      else
        ym=ymi-yd
      endif
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,White)
      if(Type.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,LightGray,Black,
     1                   LastQuest.ne.0)
      endif
      call SetStringArrayTo(LstTextOld,100,' ')
      if(Header.ne.' ') then
        LstText(1)=Header
        LstText(1)(MaxLength+1:)=' '
        LstLine=1
        LstFirst=2
      endif
      go to 9000
      entry FeLstBackSpace
      LstLine=LstLine-1
      if(Console) Overwrite=.true.
      go to 9000
      entry FeLstWriteLine(Text,il)
      if(Console) then
        if(il.eq.0.or.Overwrite) then
          FirstChar='+'
        else
          FirstChar=' '
        endif
        LstText(1)=' '
        do i=1,idel(Text)
          if(Text(i:i).eq.Tabulator) then
            LstText(1)(i:i)=' '
          else
            LstText(1)(i:i)=Text(i:i)
          endif
        enddo
        if(OpSystem.le.0) then
          write(6,FormA) FirstChar//LstText(1)(:idel(LstText(1)))
        else
          if(FirstChar.eq.'+') then
            write(6,FormA,advance='no') char(13)//
     +                LstText(1)(:idel(LstText(1)))
          else
            write(6,FormA) LstText(1)(:idel(LstText(1)))
          endif
        endif
        Overwrite=.false.
      else
        if(il.eq.0) then
          ila=max(LstLine,0)
          if(ila.le.0) ila=1
        else if(il.lt.0) then
          ila=LstLine+1
        else
          ila=il
        endif
        if(ila.gt.LstLineMax) then
          do i=LstFirst+1,LstLineMax
            LstText(i-1)=LstText(i)
          enddo
          ila=LstLineMax
        endif
        LstText(ila)=' '
        do i=1,idel(Text)
          if(Text(i:i).eq.Tabulator) then
            LstText(ila)(i:i)=' '
          else
            LstText(ila)(i:i)=Text(i:i)
          endif
        enddo
        LstText(ila)(MaxLength+1:)=' '
        LstLine=max(LstLine,ila)
        ypom=ym+yd-.6667*FixFontHeightInPixels
        xpom=xm+3.
        do i=1,LstLine
          call FeRewritePureString(0,xpom,ypom,LstTextOld(i),LstText(i),
     1                             'L',White,Black)
          LstTextOld(i)=LstText(i)
          LstTextOld(i)(MaxLength+1:)=' '
          ypom=ypom-FixFontHeightInPixels
        enddo
      endif
      go to 9000
      entry FeLstRemove
9000  if(Console) go to 9999
      if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  return
      end
      subroutine OpenWorkSpace
      include 'fepc.cmn'
      call Inic
      return
      end
      subroutine FeArrow(xfrom,yfrom,xto,yto,Color,Type)
      include 'fepc.cmn'
      dimension xp(6),yp(6)
      integer Color,Type
      Angle=30.
      cosfi=cos(angle*ToRad)
      sinfi=sin(angle*ToRad)
      xp(1)=xto
      yp(1)=yto
      xp(2)=xfrom
      yp(2)=yfrom
      call FePolyLine(2,xp,yp,color)
      d=sqrt((xto-xfrom)**2+(yto-yfrom)**2)
      if(d.gt.5.) then
        dl=6.
      else
        dl=4.
      endif
      d=dl/d
      dx=(xfrom-xto)*d
      dy=(yfrom-yto)*d
      if(Type.eq.ArrowFromTo.or.Type.eq.ArrowTo) then
        xp(2)=xto+dx*cosfi-dy*sinfi
        yp(2)=yto+dx*sinfi+dy*cosfi
        xp(3)=xto+dx*cosfi+dy*sinfi
        yp(3)=yto-dx*sinfi+dy*cosfi
        call FePolygon(xp,yp,3,4,0,0,Color)
      endif
      if(Type.eq.ArrowFromTo.or.Type.eq.ArrowFrom) then
        xp(1)=xfrom
        yp(1)=yfrom
        xp(2)=xfrom-dx*cosfi+dy*sinfi
        yp(2)=yfrom-dx*sinfi-dy*cosfi
        xp(3)=xfrom-dx*cosfi-dy*sinfi
        yp(3)=yfrom+dx*sinfi-dy*cosfi
        call FePolygon(xp,yp,3,4,0,0,Color)
      endif
      return
      end
      subroutine FeReadFileName(FileName,Text,Filter,ich)
      include 'fepc.cmn'
      character*(*) FileName,Text,Filter
      character*256 EdwStringQuest
      character*80  Veta
      call FeBoldFont
      xd=max(FeTxLength(Text)+10.,350.)
      call FeNormalFont
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xd,1,Text,0,LightGray,0,0)
      if(Language.eq.0) then
        Veta='%File name'
      else
        Veta='%Jm�no souboru'
      endif
      bpom=80.
      tpom=5.
      xpom=FeTxLength(Veta)+10.
      dpom=xd-xpom-bpom-15.
      call FeQuestEdwMake(id,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      if(Language.eq.0) then
        Veta='%Browse'
      else
        Veta='%Hledej'
      endif
      call FeQuestButtonMake(id,xd-bpom-5.,1,bpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtBrowse=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        call FeFileManager(Text,FileName,Filter,0,.true.,ich)
        call FeQuestStringEdwOpen(nEdwName,FileName)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) FileName=EdwStringQuest(nEdwName)
      call FeQuestRemove(id)
      return
      end
      subroutine FeFileManager(Header,FileName,FilterIn,Key,KeepOrgDir,
     1                         ich)
      include 'fepc.cmn'
      character*(*) Header,FileName,FilterIn
      character*2 MenD(26)
      character*80 FileNameO,Radka,Filter,RolMenuTextQuest
      character*256 EdwStringQuest,SbwStringQuest,CurrentDirIn,
     1              CurrentDirO
      character*128 ListDir,ListFile
      integer FeChdir,ErrChdir,RolMenuSelectedQuest,ActualDrive,
     1        RolMenuNumberQuest,SbwItemPointerQuest
      logical KeepOrgDir,Protected,EqIgCase,CrwLogicQuest,FeYesNoHeader,
     1        WizardModeIn,Finish
      ich=0
      WizardModeIn=WizardMode
      WizardMode=.false.
      id=NextQuestId()
      CurrentDirIn=CurrentDir
      FileNameO=FileName
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      if(Key.eq.0.or.OpSystem.le.0) then
        il=17
      else
        il=16
      endif
      if(Key.ge.0) then
        xqd=600.
        xqdp=xqd*.5
      else
        xqd=300.
        xqdp=xqd
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Header,0,LightGray,0,0)
      xpom=15.
      dpom=xqdp-50.
      tpom=xpom+dpom*.5
      if(Language.eq.1) then
        Radka='Adres��'
      else
        Radka='Directory'
      endif
      call FeQuestLblMake(id,tpom,1,Radka,'C','N')
      ilp=14
      call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                    SbwVertical)
      SbwDoubleClickAllowed=.true.
      nSbwDir=SbwLastMade
      dpome=dpom+SbwXMaxPruh(1,SbwLastMade)-SbwXMinPruh(1,SbwLastMade)
      il=ilp+1
      call FeQuestButtonMake(id,tpom-ButYd*.5,il,ButYd,ButYd,'#')
      nButtDir=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      i=ButtonLastMade+ButtonFr-1
      ButtonZ(i:i)='v'
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpome,EdwYd,1)
      nEdwDir=EdwLastMade
      if(Key.ge.0) then
        xpom=xpom+xqdp
        xpomf=xpom
        tpom=tpom+xqdp
        if(Language.eq.1) then
          Radka='Soubor'
        else
          if(Key.eq.0) then
            Radka='File'
          else
            Radka='Structure'
          endif
        endif
        call FeQuestLblMake(id,tpom,1,Radka,'C','N')
        call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                      SbwVertical)
        SbwDoubleClickAllowed=.true.
        nSbwFile=SbwLastMade
        il=ilp+1
        call FeQuestButtonMake(id,tpom-ButYd*.5,il,ButYd,ButYd,'#')
        nButtFile=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        i=ButtonLastMade+ButtonFr-1
        ButtonZ(i:i)='w'
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpome,EdwYd,0)
        nEdwFile=EdwLastMade
      else
        nSbwFile=0
        nButtFile=0
        nEdwFile=0
      endif
      il=il+1
      if(OpSystem.le.0) then
        tpom=15.
        if(Language.eq.1) then
          Radka='%Jednotka'
        else
          Radka='%Drive'
        endif
        xpom=tpom+FeTxLengthUnder(Radka)+12.
        dpom=40.+EdwYd
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,
     1                          1)
        call GetActiveDrives(MenD,NMenD)
        do i=1,NMenD
          if(EqIgCase(CurrentDir(1:2),MenD(i))) then
            KMenD=i
            go to 1110
          endif
        enddo
        KMenD=1
1110    nRolMenuDrive=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
        xpom=xpom+dpom+20.
      else
        xpom=15.
        nRolMenuDrive=0
      endif
      Radka='%Make new (sub)directory'
      tpom=FeTxLengthUnder(Radka)+15.
      call FeQuestButtonMake(id,xpom,il,tpom,ButYd,Radka)
      nButtNewDir=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(Key.eq.0) then
        xpom=xpomf
        tpom=xpom+CrwXd+10.
        if(Language.eq.1) then
          Radka='Pou�ij %filtr'
        else
          Radka='Use %filter'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Radka,'L',CrwXd,CrwYd,1,
     1                      0)
        nCrwFilter=CrwLastMade
        if(FilterIn.eq.'*') then
          i=CrwOff
        else
          i=CrwOn
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.CrwOn)
        tpom=tpom+FeTxLengthUnder(Radka)+5.
        Radka='=>'
        xpom=tpom+FeTxLengthUnder(Radka)+7.
        dpom=xqd-xpom-19.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,1)
        nEdwFilter=EdwLastMade
        Filter=FilterIn
        if(i.eq.CrwOn) call FeQuestStringEdwOpen(EdwLastMade,Filter)
      else
        Filter=' '
        nCrwFilter=0
        nEdwFilter=0
      endif
      CurrentDirO=CurrentDir
      tpom=27.
      xpom=65.
      call FeQuestLblMake(id,tpom,ilp+1,'Write','L','N')
      nLblWrite=LblLastMade
      call FeQuestLblOff(LblLastMade)
      call FeQuestLblMake(id,xpom,ilp+1,'protected','L','N')
      nLblProtected=LblLastMade
      call FeQuestLblOff(LblLastMade)
1500  do i=1,5
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.le.0) go to 1530
      enddo
1530  Protected=ErrChdir.lt.0
      if(ErrChdir.gt.0) then
        ErrChdir=FeChdir(CurrentDirO)
        NInfo=1
        TextInfo(1)='The directory doesn''t exist'
        if(FeYesNoHeader(-1.,-1.,'Do you want to create it?',0)) then
          if(OpSystem.le.0) then
            call FeMkDir(CurrentDir,ichp)
          else
            call FeSystem('mkdir '//CurrentDir)
          endif
          ErrChdir=FeChdir(CurrentDir)
          if(ErrChdir.ne.0) then
            TextInfo(1)='Sorry, the action has not been successful.'
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          else
            go to 1560
          endif
        endif
        ErrChdir=FeChdir(CurrentDirO)
        CurrentDir=CurrentDirO
1560    EventType=EventEdw
        EventNumber=nEdwDir
      else if(ErrChdir.lt.0) then
        call FeQuestLblOn(nLblWrite)
        call FeQuestLblOn(nLblProtected)
      else
        call FeQuestLblOff(nLblWrite)
        call FeQuestLblOff(nLblProtected)
      endif
      call FeGetCurrentDir
      if(Key.ge.0) then
        call CloseIfOpened(SbwLn(nSbwFile))
        call CloseIfOpened(SbwLn(nSbwDir))
      endif
      call FeDir(ListDir,ListFile,Filter,Key)
      call FeQuestSbwMenuOpen(nSbwDir,ListDir)
      call FeQuestStringEdwOpen(nEdwDir,CurrentDir)
      if(Key.ge.0) then
        call FeQuestSbwMenuOpen(nSbwFile,ListFile)
        call FeQuestStringEdwOpen(nEdwFile,FileName)
      endif
2000  call GetActiveDrives(MenD,NMenD)
      if(NMenD.eq.RolMenuNumberQuest(nRolMenuDrive)) then
        do i=1,NMenD
          if(MenD(i).ne.RolMenuTextQuest(i,nRolMenuDrive)) go to 2020
        enddo
        go to 2060
      endif
2020  do i=1,NMenD
        if(EqIgCase(CurrentDir(1:2),MenD(i))) then
          KMenD=i
          go to 2040
        endif
      enddo
      KMenD=1
2040  call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
2060  ActualDrive=RolMenuSelectedQuest(nRolMenuDrive)
      Finish=.false.
      call FeQuestEvent(id,ich)
2100  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtDir) then
          CurrentDirO=CurrentDir
          i=SbwItemPointerQuest(nSbwDir)
          CurrentDir=SbwStringQuest(i,nSbwDir)
          go to 1500
        else if(CheckNumber.eq.nButtFile) then
          i=SbwItemPointerQuest(nSbwFile)
          if(i.gt.0) then
            FileName=SbwStringQuest(i,nSbwFile)
            if(Key.eq.1) then
              i=index(FileName,'#$color')
              if(i.gt.0) FileName(i:)=' '
            endif
            call FeQuestStringEdwOpen(nEdwFile,FileName)
            EventType=EventEdw
            EventNumber=nEdwFile
          else
            go to 2060
          endif
        else if(CheckNumber.eq.nButtNewDir) then
          idp=NextQuestId()
          xqdp=400.
          il=1
          call FeQuestCreate(idp,-1.,-1.,xqdp,1,' ',0,LightGray,0,0)
          tpom=5.
          Radka='New subdirectory'
          xpom=tpom+FeTxLengthUnder(Radka)+10.
          dpom=xqdp-xpom-5.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,
     1                        0)
          nEdwNewDir=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,' ')
2200      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2200
          endif
          if(ich.eq.0) then
            Radka=EdwStringQuest(nEdwNewDir)
            if(OpSystem.le.0) then
              call FeMkDir(Radka,ichp)
            else
              call FeSystem('mkdir '//Radka)
            endif
            if(LocateSubstring(Radka,':',.false.,.true.).gt.0) then
              CurrentDir=Radka
            else
              CurrentDir=CurrentDir(:idel(CurrentDir))//
     1                   Radka(:idel(Radka))
            endif
          endif
          call FeQuestRemove(idp)
          EventType=EventEdw
          EventNumber=nEdwDir
          if(ich.eq.0) go to 1500
        endif
        if(.Not.Finish) go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDrive) then
        i=RolMenuSelectedQuest(nRolMenuDrive)
        CurrentDirO=CurrentDir
        CurrentDir=MenD(i)
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.ne.0) then
          call FeChybne(-1.,-1.,'The drive isn''t accessible',' ',
     1                  SeriousError)
          CurrentDir=CurrentDirO
          ErrChdir=FeChdir(CurrentDir)
          call FeQuestRolMenuOpen(nRolMenuDrive,MenD,NMenD,ActualDrive)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFilter) then
        if((OpSystem.le.0.and.
     1      EqIgCase(Filter,EdwStringQuest(nEdwFilter))).or.
     2     (OpSystem.eq.1.and.
     3      Filter.eq.EdwStringQuest(nEdwFilter))) then
          go to 2000
        else
          Filter=EdwStringQuest(nEdwFilter)
          go to 1500
        endif
      else if(CheckType.eq.EventSbwDoubleClick) then
        CheckType=EventButton
        i=mod(CheckNumber,100)
        if(i.eq.nSbwDir) then
          CheckNumber=nButtDir
        else
          CheckNumber=nButtFile
          Finish=.true.
        endif
        go to 2100
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDir) then
        if(EdwStringQuest(nEdwDir).ne.CurrentDir) then
          CurrentDir=EdwStringQuest(nEdwDir)
          go to 1500
        endif
        go to 2000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFilter) then
        if(CrwLogicQuest(nCrwFilter)) then
          call FeQuestStringEdwOpen(nEdwFilter,Filter)
        else
          Filter='*'
          call FeQuestEdwClose(nEdwFilter)
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0) then
        if(Key.ge.0) then
          FileName=EdwStringQuest(nEdwFile)
          if(FileName.eq.' ') then
            if(Key.eq.0) then
              Radka='file'
            else
              Radka='structure'
            endif
            call FeChybne(-1.,YBottomMessage,'The empty '//
     1                    Radka(:idel(Radka))//
     2                    ' name isn''t acceptable, try again',' ',
     3                    SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 2060
          endif
        else
          FileName=EdwStringQuest(nEdwDir)
          if(FileName.eq.' ') then
            Radka='Directory'
            call FeChybne(-1.,YBottomMessage,
     1                    'The empty '//Radka(:idel(Radka))//
     2                    ' name isn''t acceptable, try again',' ',
     3                    SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 2060
          endif
        endif
        if(Key.eq.1) then
          i=index(FileName,'#$color')
          if(i.gt.0) FileName(i:)=' '
        endif
      endif
      call FeQuestRemove(id)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      if(ich.ne.0) then
        FileName=FileNameO
        go to 9000
      endif
      if(KeepOrgDir) then
        if(FileName.ne.' '.and.CurrentDir.ne.CurrentDirIn)
     1    FileName=CurrentDir(:idel(CurrentDir))//
     2    FileName(:idel(FileName))
        go to 9000
      else
        go to 9999
      endif
9000  CurrentDir=CurrentDirIn
      i=FeChdir(CurrentDir(:idel(CurrentDir)))
9999  WizardMode=WizardModeIn
      return
      end
      subroutine FeDir(ListDir,ListFile,Filter,Key)
      include 'fepc.cmn'
      character*(*) ListDir,ListFile,Filter
      character*256 Nazev,NazevP
      character*128 WorkFile,WorkFileP
      character*80  t80,TmpFile,FilterArr(:)
      integer FeGetSystemTime,tf
      logical ExistFile,EqWild,Dlouho,EqIgCase
      allocatable FilterArr
      ich=0
      tf=FeGetSystemTime()
      Dlouho=.false.
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      TmpFile='jcmd'
      call CreateTmpFile(TmpFile,i,0)
      call FeTmpFilesAdd(TmpFile)
      if(OpSystem.le.0) then
        call FeDirList(TmpFile)
        if(ErrFlag.ne.0) go to 9999
      else if(OpSystem.eq.1) then
        call FeSystem(ObrLom//'ls -F -x -1 > '//TmpFile)
      endif
      ln1=NextLogicNumber()
      call OpenFile(ln1,TmpFile,'formatted','unknown')
      ln2=NextLogicNumber()
      call OpenFile(ln2,ListDir,'formatted','unknown')
      write(ln2,'(''.'')')
      if((OpSystem.le.0.and.CurrentDir(2:).ne.':'//ObrLom).or.
     1   (OpSystem.eq.1.and.CurrentDir.ne.'/')) write(ln2,'(''..'')')
      ln3=NextLogicNumber()
      if(Key.eq.0) then
        WorkFile=ListFile
        NFilter=0
        k=0
1100    call KusAp(Filter,k,t80)
        NFilter=NFilter+1
        if(k.lt.len(Filter)) go to 1100
        allocate(FilterArr(NFilter))
        i=0
        k=0
1200    i=i+1
        call KusAp(Filter,k,FilterArr(i))
        if(k.lt.len(Filter)) go to 1200
      else
        WorkFileP='jwork'
        ln4=ln3
        call CreateTmpFile(WorkFileP,i,0)
        call FeTmpFilesAdd(WorkFileP)
        call OpenFile(ln4,WorkFileP,'formatted','unknown')
        ln3=NextLogicNumber()
        WorkFile='jwork'
        call CreateTmpFile(WorkFile,i,0)
        call FeTmpFilesAdd(WorkFile)
      endif
      call OpenFile(ln3,WorkFile,'formatted','unknown')
2000  read(ln1,FormA,end=5000) Nazev
      call FeDirCheckTime(Dlouho,tf,ich)
      call Skrtni09(Nazev)
      if(Nazev.eq.' ') then
        if(ich.eq.0) then
          go to 2000
        else
          go to 5000
        endif
      endif
      i=idel(Nazev)
      if(OpSystem.eq.1) then
        do ip=i,1,-1
         ipom=ichar(Nazev(ip:ip))
         if(ipom.gt.32.and.ipom.lt.127) go to 2200
        enddo
2200    i=ip
        if(Nazev(ip:ip).eq.'*') then
          Nazev(ip:ip)=' '
          ip=ip-1
        endif
      endif
      if(OpSystem.le.0.and.Nazev(1:1).eq.'[') then
        i=i-1
        Nazev=Nazev(2:)
        if(Nazev(i:i).eq.']') Nazev=Nazev(:i-1)
        if(Nazev.eq.'.'.or.Nazev.eq.'..') then
          if(ich.eq.0) then
            go to 2000
          else
            go to 5000
          endif
        endif
        ln=ln2
      else if(OpSystem.eq.1.and.Nazev(i:i).eq.'/') then
        Nazev=Nazev(:i-1)
        ln=ln2
      else
        if(idel(Nazev).eq.0) go to 4000
        if(Key.ne.0) then
          ip=1
2500      i=index(Nazev(ip:),'.')
          if(i.gt.0) then
            ip=ip+i
            go to 2500
          endif
          if(OpSystem.le.0) then
            if(EqIgCase(Nazev(ip:),'tmp')) go to 4000
          else
            if(Nazev(ip:).eq.'tmp') go to 4000
          endif
          if(ip.gt.1) Nazev=Nazev(:ip-2)
          rewind ln3
          rewind ln4
3000      read(ln3,FormA,end=3100,err=3100) NazevP
          write(ln4,FormA) NazevP(:idel(NazevP))
          call FeDirCheckTime(Dlouho,tf,ich)
          if(OpSystem.eq.1) then
            if(Nazev.eq.NazevP) then
              go to 4000
            else
              go to 3000
            endif
          else
            if(EqIgCase(Nazev,NazevP)) then
              go to 4000
            else
              go to 3000
            endif
          endif
3100      i=ln4
          ln4=ln3
          ln3=i
        else
          do i=1,NFilter
            if(EqWild(Nazev,FilterArr(i),OpSystem.gt.0)) go to 3200
          enddo
          go to 4000
        endif
3200    ln=ln3
      endif
      write(ln,FormA) Nazev(:idel(Nazev))
4000  if(ich.eq.0) go to 2000
5000  call CloseIfOpened(ln1)
      call DeleteFile(TmpFile)
      call FeTmpFilesClear(TmpFile)
      if(Key.ne.0) then
        rewind ln3
        call OpenFile(ln1,ListFile,'formatted','unknown')
6000    read(ln3,FormA,end=6100) Nazev
        call FeDirCheckTime(Dlouho,tf,ich)
        if(ich.ne.0) go to 9999
        if(ExistFile(Nazev(:idel(Nazev))//'.m50')) then
          i=idel(Nazev)
          write(Nazev(i+2:),'(''#$color'',i10)') Red
          go to 6050
        endif
        if(ExistFile(Nazev(:idel(Nazev))//'.cad').or.
     1     ExistFile(Nazev(:idel(Nazev))//'.col').or.
     2     ExistFile(Nazev(:idel(Nazev))//'.hkl').or.
     3     ExistFile(Nazev(:idel(Nazev))//'.dat').or.
     4     ExistFile(Nazev(:idel(Nazev))//'.p4o').or.
     5     ExistFile(Nazev(:idel(Nazev))//'.raw').or.
     6     ExistFile(Nazev(:idel(Nazev))//'.psi')) then
          if(index(Nazev,'#$color').le.0) then
            i=idel(Nazev)
            write(Nazev(i+2:),'(''#$color'',i10)') Green
          endif
        endif
6050    write(ln1,FormA) Nazev(:idel(Nazev))
        go to 6000
6100    close(ln3,status='delete')
        close(ln4,status='delete')
        if(Key.ne.0) then
          call FeTmpFilesClear(WorkFile)
          call FeTmpFilesClear(WorkFileP)
        endif
        call CloseIfOpened(ln1)
      else
        call CloseIfOpened(ln3)
      endif
      call CloseIfOpened(ln2)
9999  if(Dlouho) then
        call FeMsgClear(1)
        call FeReleaseOutput
        call FeDeferOutput
        if(ich.ne.0) then
          NInfo=1
          TextInfo(1)='The list of files/structures isn''t complete.'
          call FeMsgShow(1,-1.,-1.,.true.)
          call FeWait(1.)
          call FeReleaseOutput
          call FeDeferOutput
          call FeMsgClear(1)
          call CloseIfOpened(ln1)
          call CloseIfOpened(ln2)
          call CloseIfOpened(ln3)
        endif
      endif
      if(allocated(FilterArr)) deallocate(FilterArr)
      return
      end
      subroutine GetPathTo(PrgName,Directory,Path)
      include 'fepc.cmn'
      character*(*) PrgName,Directory,Path
      character*256 Veta,Filter,ListDir,ListFile,CurrentDirO
      character*80  PathTested
      integer FeChDir
      logical EqWild,EqIgCase
      ln=0
      CurrentDirO=CurrentDir
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      Path=' '
      do 3000i=1,2
        if(i.eq.1) then
          PathTested='C:\Program files\'
        else
          PathTested='C:\'
        endif
        j=FeChDir(PathTested)
        call FeDir(ListDir,ListFile,'*.*',0)
        if(ErrFlag.ne.0) go to 9999
        ln=NextLogicNumber()
        open(ln,file=ListDir)
1000    read(ln,FormA,err=3000,end=3000) Veta
        if(EqWild(Veta,Directory,OpSystem.gt.0)) then
          call CloseIfOpened(ln)
          j=FeChDir(Veta)
          call FeGetCurrentDir
          call FeDir(ListDir,ListFile,'*.*',0)
          if(ErrFlag.ne.0) go to 9999
          open(ln,file=ListFile)
1200      read(ln,FormA,err=3000,end=3000) Veta
          if(EqIgCase(Veta,PrgName)) then
            j=idel(CurrentDir)
            Path=CurrentDir(:j)
            if(Path(j:j).ne.'\') then
              j=j+1
              Path(j:j)='\'
            endif
            Path=Path(:j)//PrgName(:idel(PrgName))
            go to 9999
          endif
          go to 1200
        else
          go to 1000
        endif
3000  continue
      Path=' '
9999  i=FeChDir(CurrentDirO)
      call CloseIfOpened(ln)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      return
      end
      subroutine FeDirCheckTime(Dlouho,tf,ich)
      include 'fepc.cmn'
      logical Dlouho,FeYesNo
      integer FeGetSystemTime,tf
      if(Dlouho) then
        call FeEvent(1)
        if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
          if(FeYesNo(-1.,60.,'Do you really want to cancel the '//
     1               'action?',0)) ich=1
          call FeReleaseOutput
          call FeDeferOutput
        endif
      else
        if(FeGetSystemTime()-tf.gt.3000) then
          Dlouho=.true.
          NInfo=1
          TextInfo(1)='Reading file list, press the ESC key to '//
     1                'cancel...'
          call FeMsgShow(1,-1.,-1.,.true.)
        endif
      endif
      return
      end
      subroutine FeGetCurrentDir
      include 'fepc.cmn'
      character*80 TmpFile
      character Znak
      if(OpSystem.le.0) then
        call FeDirName(CurrentDir)
        Znak=Obrlom
      else if(OpSystem.eq.1) then
        TmpFile='jcmd'
        call CreateTmpFile(TmpFile,i,0)
        call FeTmpFilesAdd(TmpFile)
        call FeSystem(ObrLom//'pwd > '//TmpFile)
        Znak='/'
        ln=NextLogicNumber()
        call OpenFile(ln,TmpFile,'formatted','old')
        read(ln,FormA128) CurrentDir
        call CloseIfOpened(ln)
        call DeleteFile(TmpFile)
        call FeTmpFilesClear(TmpFile)
      endif
      i=idel(CurrentDir)
      if(CurrentDir(i:i).ne.Znak) CurrentDir=CurrentDir(:i)//Znak
      return
      end
      subroutine FeCutName(LongText,ShortText,is,CutText)
      include 'fepc.cmn'
      character*(*) LongText,ShortText
      integer CutText
      id=idel(LongText)
      if(id.le.is) then
        ShortText=LongText
      else
        if(CutText.eq.CutTextFromLeft) then
          ShortText='...'//LongText(id-is+4:id)
        else if(CutText.eq.CutTextFromRight) then
          ShortText=LongText(:is-3)//'...'
        else if(CutText.eq.CutTextNone) then
          ShortText=LongText(:is)
        endif
      endif
      return
      end
      subroutine FeCutNameLength(LongText,ShortText,Length,CutText)
      include 'fepc.cmn'
      character*(*) LongText,ShortText
      integer CutText
      real Length
      if(FeTxLength(LongText).le.Length) then
        ShortText=LongText
      else
        idll=idel(LongText)
        if(CutText.eq.CutTextFromLeft) then
          do i=idll,1,-1
            ShortText='...'//LongText(i:idll)
            if(FeTxLength(ShortText).gt.Length) then
              ShortText='...'//LongText(i+1:idll)
              go to 9999
            endif
          enddo
        else if(CutText.eq.CutTextFromRight.or.CutText.eq.CutTextNone)
     1    then
          do i=idll,1,-1
            if(CutText.eq.CutTextFromRight) then
              ShortText=LongText(:i)//'...'
            else
              ShortText=LongText(:i)
            endif
            if(FeTxLength(ShortText).le.Length) go to 9999
          enddo
        endif
      endif
9999  return
      end
      subroutine PosunMenu(ln,ip,ik,kolik,xmnn,ymxn,xd,MenuLong,
     1                     Menu)
      include 'fepc.cmn'
      character*25  Menu(14)
      character*256 MenuLong(14)
      character*1 Zn
      rewind ln
      ip=ip+kolik
      ik=ik+kolik
      do i=1,ip-1
        read(ln,FormA)
      enddo
      do i=ip,ik
        j=i-ip+1
        read(ln,FormA) MenuLong(j)
        call FeCutName(MenuLong(j),Menu(j),25,CutTextFromLeft)
        call FeWrMenuItem(xmnn,ymxn-float(j-1)*8.,xd,' ',Zn,Black,White)
        call FeWrMenuItem(xmnn,ymxn-float(j-1)*8.,xd,Menu(j),Zn,Black,
     1                    White)
      enddo
      return
      end
      subroutine FeFlowIMan(xp,yp,yk,y1,y2,yshift)
      include 'fepc.cmn'
      integer BarvaNad,BarvaPod
      if(abs(yshift).lt.y2-y1) then
        if(yshift.gt.0.) then
          BarvaNad=White
          BarvaPod=LightGray
        else if(yshift.lt.0.) then
          BarvaPod=White
          BarvaNad=LightGray
        else
          go to 9999
        endif
        call FeFillRectangle(xp+1.,xp+7.,y1,y1+yshift,4,0,0,BarvaPod)
        call FeFillRectangle(xp+1.,xp+7.,y2,y2+yshift,4,0,0,BarvaNad)
        y1=y1+yshift
        y2=y2+yshift
      else
        call FeFillRectangle(xp+1.,xp+7.,y1,y2,4,0,0,LightGray)
        y1=y1+yshift
        y2=y2+yshift
        call FeFillRectangle(xp+1.,xp+7.,y1,y2,4,0,0,White)
      endif
9999  return
      end
      subroutine DeleteFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      logical opened
      i1=1
      i2=idel(FileName)
      if(FileName(i1:i1).eq.'#'.and.FileName(i2:i2).eq.'#') then
        i1=i1+1
        i2=i2-1
      endif
      inquire(file=FileName(i1:i2),opened=opened,number=i)
      if(.not.opened) then
        i=NextLogicNumber()
        open(i,file=FileName(i1:i2),err=9999)
      endif
      close(i,status='delete',err=9999)
9999  return
      end
      subroutine DeleteFileIfEmpty(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      character*256 Veta
      logical opened
      inquire(file=FileName,opened=opened,number=ln)
      if(opened) then
        rewind ln
      else
        ln=NextLogicNumber()
        open(ln,file=FileName,err=9999)
      endif
1100  read(ln,FormA,end=9000,err=9999) Veta
      if(idel(Veta).gt.0) then
        if(.not.opened) call CloseIfOpened(ln)
        go to 9999
      else
        go to 1100
      endif
9000  close(ln,status='delete',err=9999)
9999  return
      end
      subroutine DeleteAllFiles(Filter)
      include 'fepc.cmn'
      character*(*) Filter
      character*256 t256,ListDir,ListFile,CurrentDirO,CurrentDirN,
     1              FilterPure
      integer FeChdir
      CurrentDirO=CurrentDir
      j=0
      idlf=idel(Filter)
      t256=' '
      do i=1,idlf
        if(j.eq.0.or.i.eq.idlf) then
          if(Filter(i:i).eq.' '.or.Filter(i:i).eq.'"') cycle
        endif
        j=j+1
        t256(j:j)=Filter(i:i)
      enddo
      call ExtractDirectory(t256,CurrentDirN)
      if(CurrentDirN(1:1).ne.'.'.and.CurrentDirN.ne.' ') then
        i=FeChdir(CurrentDirN)
        CurrentDir=CurrentDirN
       call ExtractFileName(t256,FilterPure)
      else
        FilterPure=t256
      endif
      ListDir='jdir'
      ListFile='jfile'
      call CreateTmpFile(ListDir,id1,0)
      call CreateTmpFile(ListFile,id2,0)
      call FeTmpFilesAdd(ListDir)
      call FeTmpFilesAdd(ListFile)
      call FeDir(ListDir,ListFile,FilterPure,0)
      call DeleteFile(ListDir)
      call FeTmpFilesClear(ListDir)
      ln=NextLogicNumber()
      open(ln,file=ListFile)
1000  read(ln,FormA,end=2000) t256
      call DeleteFile(t256)
      go to 1000
2000  close(ln,status='delete')
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      call FeTmpFilesClear(ListFile)
      return
      end
      subroutine ExtractDirectory(FullName,Directory)
      include 'fepc.cmn'
      character*(*) FullName,Directory
      j=0
1000  i=index(FullName(j+1:),DirectoryDelimitor)
      if(i.gt.0) then
        j=j+i
        go to 1000
      endif
      if(j.gt.0) then
        Directory=FullName(:j)
      else
        Directory=' '
      endif
      return
      end
      subroutine ExtractFileName(FullName,FileName)
      include 'fepc.cmn'
      character*(*) FullName,FileName
      j=0
1000  i=index(FullName(j+1:),DirectoryDelimitor)
      if(i.gt.0) then
        j=j+i
        go to 1000
      endif
      FileName=FullName(j+1:)
      return
      end
      subroutine GetPureFileName(FileName,PureFileName)
      character*(*) FileName,PureFileName
      i=idel(FileName)
1000  if(FileName(i:i).ne.'.') then
        i=i-1
        if(i.gt.0) go to 1000
      endif
      if(i.gt.0) then
        PureFileName=FileName(:i-1)
      else
        PureFileName=FileName
      endif
      return
      end
      subroutine GetExtentionOfFileName(FileName,Extension)
      include 'fepc.cmn'
      character*(*) FileName,Extension
      i=idel(FileName)
1000  if(FileName(i:i).ne.'.') then
        i=i-1
        if(i.gt.0) go to 1000
      endif
      if(i.gt.0) then
        Extension=FileName(i+1:)
      else
        Extension=' '
      endif
      return
      end
      function NextLogicNumber()
      include 'fepc.cmn'
      logical Opened
      NextLogicNumber=10
1000  inquire(NextLogicNumber,opened=Opened)
      if(opened) then
        NextLogicNumber=NextLogicNumber+1
        if(NextLogicNumber.lt.40) then
          go to 1000
        else
          NextLogicNumber=-1
        endif
      endif
      return
      end
      subroutine FeListSmazE(VetaE,DPruh)
      include 'fepc.cmn'
      character*(*) VetaE
      if(VetaE.ne.' ') then
        call FeTextErase(0,85.,YMinBasWin+DPruh*0.5,VetaE,'L',LightGray)
        VetaE=' '
      endif
      return
      end
      subroutine FeShowListing(xm,ym,WhatEnds,FileLst,WaitTimeIn)
      include 'fepc.cmn'
      character*(*) WhatEnds,FileLst
      character*80 Radka
      integer WaitTimeIn
      logical FeYesNoHeader
      if(.not.AlwaysOpenListing) then
        NInfo=1
        TextInfo(1)=' '
        Radka='Regular end of '//WhatEnds(:idel(WhatEnds))
        WaitTime=WaitTimeIn
        if(WaitTime.gt.0) then
          i=max((45-idel(Radka))/2+1,1)
        else
          i=1
        endif
        TextInfo(1)(i:)=Radka
        if(.not.FeYesNoHeader(xm,ym,'Open the listing?',0)) go to 9999
      endif
      call CloseIfOpened(lst)
      if(BuildInViewer) then
        call FeListView(FileLst,0)
      else
        if(OpSystem.le.0) then
          call FeEdit(FileLst)
        endif
      endif
9999  return
      end
      subroutine FeRewriteString(id,xp,yp,StringOld,StringNew,Justify,
     1                           EraseColor,WriteColor)
      include 'fepc.cmn'
      character*(*) StringOld,StringNew,Justify
      integer EraseColor,WriteColor
      logical Deferred
      Deferred=DeferredOutput
      if(.not.Deferred) call FeDeferOutput
      call FeGetTextRectangle(xp,yp,StringOld,Justify,1,xpold,xkold,
     1                        ypold,ykold,refx,refy,conx,cony)
      if(id.gt.0) then
        xpold=xpold+QuestXMin(id)
        xkold=xkold+QuestXMin(id)
        ypold=ypold+QuestYMin(id)
        ykold=ykold+QuestYMin(id)
      endif
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,EraseColor)
      call FeOutSt(id,xp,yp,StringNew,Justify,WriteColor)
      if(.not.Deferred) call FeReleaseOutput
      return
      end
      subroutine FeRewritePureString(id,xp,yp,StringOld,StringNew,
     1                               Justify,EraseColor,WriteColor)
      include 'fepc.cmn'
      character*(*) StringOld,StringNew,Justify
      integer EraseColor,WriteColor
      logical Deferred
      Deferred=DeferredOutput
      if(.not.Deferred) call FeDeferOutput
      call FeGetPureTextRectangle(xp,yp,StringOld,Justify,1,xpold,xkold,
     1                            ypold,ykold,refx,refy,conx,cony)
      ypold=ypold-2./EnlargeFactor
      if(id.gt.0) then
        xpold=xpold+QuestXMin(id)
        xkold=xkold+QuestXMin(id)
        ypold=ypold+QuestYMin(id)
        ykold=ykold+QuestYMin(id)
      endif
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,EraseColor)
      call FeOutSt(id,xp,yp,StringNew,Justify,WriteColor)
      if(.not.Deferred) call FeReleaseOutput
      return
      end
      subroutine FeDrawFrame(xm,ym,xd,yd,dfx,ColorRD,ColorLU,
     1                       ColorObtah,ObtahniFrame)
      include 'fepc.cmn'
      dimension obx(6),oby(6)
      integer ColorRD,ColorLU,ColorObtah
      logical ObtahniFrame
      obx(1)=anint(xm*EnlargeFactor-dfx)/EnlargeFactor
      oby(1)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
      obx(2)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
      oby(2)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
      obx(3)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
      oby(3)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
      obx(4)=anint((xm+xd)*EnlargeFactor+1.)/EnlargeFactor
      oby(4)=anint((ym+yd)*EnlargeFactor+1.)/EnlargeFactor
      obx(5)=anint((xm+xd)*EnlargeFactor+1.)/EnlargeFactor
      oby(5)=anint(ym*EnlargeFactor-1.)/EnlargeFactor
      obx(6)=anint(xm*EnlargeFactor-1.)/EnlargeFactor
      oby(6)=anint(ym*EnlargeFactor-1.)/EnlargeFactor
      if(dfx.gt.0.) call FePolygon(obx,oby,6,4,0,0,ColorRD)
      obx(2)=anint(xm*EnlargeFactor-dfx)/EnlargeFactor
      oby(2)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
      obx(5)=anint(xm*EnlargeFactor-1.)/EnlargeFactor
      oby(5)=anint((ym+yd)*EnlargeFactor+1.)/EnlargeFactor
      if(dfx.gt.0.) call FePolygon(obx,oby,6,4,0,0,ColorLU)
      if(ObtahniFrame) then
        obx(2)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
        oby(2)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
        obx(4)=obx(1)
        oby(4)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
        obx(5)=obx(1)
        oby(5)=oby(1)
        call FePolyline(5,obx,oby,ColorObtah)
      endif
      return
      end
      subroutine RotatePoints(px,py,n,xc,yc,phi)
      include 'fepc.cmn'
      real px(*),py(*),phi,sphi,cphi,pomx,pomy,xc,yc
      integer n,i
      if(phi.eq.0.or.n.le.0) go to 9900
      sphi=sin(phi*torad)
      cphi=cos(phi*torad)
      do i=1,n
        px(i)=px(i)-xc
        py(i)=py(i)-yc
        pomx=px(i)*cphi-py(i)*sphi
        pomy=px(i)*sphi+py(i)*cphi
        px(i)=pomx+xc
        py(i)=pomy+yc
      enddo
9900  continue
      end
      function LineNumberInQuest(id,y,klic)
      include 'fepc.cmn'
      LineNumberInQuest=0
      corr=0
      if(klic.eq.1) corr=4.
      n=nint((QuestLength(id)-y-corr)/QuestLineWidth)
      if(n.le.0) then
        n=1
        goto 600
      endif
500   pom=QuestYPosition(id,n)
      if(pom.lt.y) then
        n=n-1
        go to 500
      endif
600   ipom=-n*10
      dif=abs(QuestYPosition(id,ipom)-y-corr+QuestYMin(id))
1000  ipom=ipom-1
      pom=abs(QuestYPosition(id,ipom)-y-corr+QuestYMin(id))
      if(pom.lt.dif) then
        dif=pom
        go to 1000
      endif
      LineNumberInQuest=ipom+1
      return
      end
      subroutine FeInfoWin(xmi,ymi)
      include 'fepc.cmn'
      logical WizardModeIn,InfoWinOpened
      character*80 NapTmp
      data InfoWinOpened/.false./
      save xm,ym,xd,yd,NapTmp
      if(xmi.le.0) then
        xm=XPos
      else
        xm=xmi
      endif
      if(ymi.le.0) then
        ym=YPos
      else
        ym=Ymi
      endif
      xd=0.
      do i=1,NInfo
        xd=max(FeTxLength(TextInfo(i))+8.,xd)
      enddo
      yd=float(NInfo)*PropFontHeightInPixels
      if(xm+xd.gt.XMaxBasWin) xm=xm-xd
      NapTmp='jnap'
      if(OpSystem.le.0) call CreateTmpFile(NapTmp,i,1)
      call FeSaveImage(xm,xm+xd,ym,ym+yd,NapTmp)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,LightYellow)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,0,0,0,Black)
      xp=xm+5.
      yp=ym+yd-PropFontHeightInPixels*.5
      do i=1,NInfo
        call FeOutSt(0,xp,yp,TextInfo(i),'L',Black)
        yp=yp-PropFontHeightInPixels
      enddo
      InfoWinOpened=.true.
      go to 9999
      entry FeKillInfoWin
      if(InfoWinOpened) call FeLoadImage(xm,xm+xd,ym,ym+yd,NapTmp,0)
      InfoWinOpened=.false.
9999  return
      end
      subroutine FeWInf
      include 'fepc.cmn'
      parameter (mxwinf=10)
      character*(*) Text
      character*80 WInfTmpFile(mxwinf)
      dimension    WInfXMin(mxwinf),WInfXMax(mxwinf),WInfYMin(mxwinf),
     1             WInfYMax(mxwinf)
      integer      WInfState(mxwinf),WInfRemoved,WInfClosed,WInfOpened
      equivalence (IdNumbers(0),WInfRemoved),
     1            (IdNumbers(1),WInfClosed),
     2            (IdNumbers(2),WInfOpened)
      save WInfXMin,WInfXMax,WInfYMin,WInfYMax,WInfState,WInfTmpFile
      data WInfState/mxwinf*0/
      entry FeWInfMake(id,idc,xm,ym,xd,yd)
      WInfXMin(id)=xm
      WInfXMax(id)=xm+xd
      WInfYMin(id)=ym
      WInfYMax(id)=ym+yd
      if(idc.gt.0) then
        WInfXMin(id)=WInfXMin(id)+QuestXMin(idc)
        WInfXMax(id)=WInfXMax(id)+QuestXMin(idc)
        WInfYMin(id)=WInfYMin(id)+QuestYMin(idc)
        WInfYMax(id)=WInfYMax(id)+QuestYMin(idc)
      endif
      WInfState(id)=WInfOpened
      WInfTmpFile(id)='jnap'
      if(OpSystem.le.0) then
        call CreateTmpFile(WInfTmpFile(id),i,1)
      else
        call CreateTmpFileName(WInfTmpFile(id),id)
      endif
      call FeSaveImage(WInfXMin(id),WInfXMax(id),
     1                 WInfYMin(id),WInfYMax(id),WInfTmpFile(id))
      go to 9999
      entry FeWInfWrite(id,Text)
      if(WInfState(id).eq.WInfRemoved) go to 9999
      call FeDeferOutput
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),4,0,0,LightYellow)
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),0,0,0,Black)
      call FeOutSt(0,WInfXMin(id)+3.,(WInfYMin(id)+WInfYMax(id))*.5,
     1             Text,'L',Black)
      call FeReleaseOutput
      WInfState(id)=WInfOpened
      go to 9999
      entry FeWInfClose(id)
      if(WInfState(id).ne.WInfOpened) go to 9999
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),4,0,0,LightGray)
      WInfState(id)=WInfClosed
      go to 9999
      entry FeWInfRemove(id)
      if(WInfState(id).eq.WInfRemoved) go to 9999
      call FeLoadImage(WInfXMin(id),WInfXMax(id),
     1                 WInfYMin(id),WInfYMax(id),WInfTmpFile(id),0)
      WInfState(id)=WInfRemoved
9999  return
      end
      subroutine FeBudeBreak
      include 'fepc.cmn'
      logical FeYesNo
      if(FeYesNo(-1.,100.,'Do you really want to cancel the '//
     1           'action?',0)) then
       call FeFlowChartRemove
       ErrFlag=1
      endif
      return
      end
      subroutine FeReadError(ln)
      include 'fepc.cmn'
      character*20  Formatted
      character*256 t256,p256
      logical   EqIgCase
      if(IgnoreE) go to 9000
      inquire(ln,name=p256,form=Formatted)
      il=len(TextInfo(1))
      if(EqIgCase(formatted,'formatted')) then
        backspace ln
        read(ln,FormA,end=1000,err=1100) t256
        go to 1500
1000    t256='### EOF detected ###'
        go to 1500
1100    t256='### not readable by A-conversion ###'
1500    TextInfo(3)='Record     : '//t256
        il=len(TextInfo(3))
        if(idel(t256).gt.il-13) TextInfo(3)(il-1:il)='..'
      else
        TextInfo(3)='Unformatted file'
      endif
      call FeCutName(p256,t256,il-13,CutTextFromLeft)
      TextInfo(1)='File name : '//p256(:idel(p256))
      write(TextInfo(2),'(i2)') ln
      TextInfo(2)='Logical number : '//TextInfo(2)
      Ninfo=3
      call FeInfoOut(-1.,YBottomMessage,'READING ERROR','L')
      if(VasekTest.eq.999) call FeWinMessage('Cesta?',' ')
9000  call CloseIfOpened(ln)
      return
      end
      subroutine FeMakeParEdwCrw(IdQuest,NamePos,ParPos,QuestLine,
     1                           ParName,nEdw,nCrw)
      include 'fepc.cmn'
      character*(*) ParName
      integer QuestLine
      real NamePos
      call FeQuestEdwMake(IdQuest,NamePos,QuestLine,ParPos,QuestLine,
     1                    ParName,'R',70.,EdwYd,0)
      nEdw=EdwLastMade
      call FeQuestCrwMake(IdQuest,5.,QuestLine,ParPos+75.,QuestLine,
     1                    ' ','L',CrwXd,CrwYd,0,0)
      nCrw=CrwLastMade
      return
      end
      subroutine FeOpenParEdwCrw(nEdw,nCrw,ParName,ParValue,ParRefKey,
     1                           ParToFraction)
      include 'fepc.cmn'
      character*(*) ParName
      integer ParRefKey
      logical ParToFraction,EqIgCase
      if(.not.EqIgCase(ParName,'#keep#'))
     1  call FeQuestEdwLabelChange(nEdw,ParName)
      call FeQuestRealEdwOpen(nEdw,ParValue,.false.,ParToFraction)
      call FeQuestCrwOpen(nCrw,mod(ParRefKey,10).ne.0)
      if(ParRefKey.ge.100) call FeQuestEdwLock(nEdw)
      if(mod(ParRefKey,100).ge.10) call FeQuestCrwLock(nCrw)
      return
      end
      subroutine FeEditParameterField(Par,SPar,ParDef,KiPar,n,lPar,xqd,
     1                                Text,tpoma,xpoma,NToInd)
      include 'fepc.cmn'
      dimension Par(n),SPar(n),ParDef(n),KiPar(n),tpoma(3),xpoma(3)
      character*(*) lPar,Text
      character*80 Veta
      logical :: CrwLogicQuest,InSig = .false.
      external NToInd
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,(n-1)/3+2,Text,0,LightGray,0,0)
      Veta=lPar
      idv=idel(Veta)+1
      do k=1,2
        do i=1,n
          im=mod(i-1,3)+1
          il=(i-1)/3+1
          call NToInd(i,Veta(idv:))
          if(k.eq.1) then
            call FeQuestLblMake(id,tpoma(im),il,Veta,'R','N')
            if(i.eq.1) nLblPrv=LblLastMade
            call FeQuestLblOff(LblLastMade)
            call FeQuestLblMake(id,xpoma(im)+6.,il,' ','L','N')
            call FeQuestLblOff(LblLastMade)
          else
            call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,
     1                           nCrw)
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',Par(i),kiPar(i),
     1                           .false.)
            if(i.eq.1) then
              nEdwPrv=nEdw
              nCrwPrv=nCrw
            endif
          endif
        enddo
      enddo
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      go to 2000
1400  nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblPrv
      if(InSig) then
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,Par,kiPar,n)
        do i=1,n
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
          call FeQuestLblOn(nLbl)
          if(SPar(i).gt.0.) then
            pom=abs(Par(i)/SPar(i))
          else
            pom=-1.
          endif
          if(pom.lt.0.) then
            Cislo='fixed'
          else if(pom.lt.3.) then
            Cislo='< 3*su'
          else
            write(Cislo,'(f15.1)') pom
            call ZdrcniCisla(Cislo,1)
            Cislo=Cislo(:idel(Cislo))//'*su'
          endif
          call FeQuestLblChange(nLbl+1,Cislo)
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        do i=1,n
          call FeQuestLblOff(nLbl)
          call FeQuestLblOff(nLbl+1)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',Par(i),kiPar(i),
     1                         .false.)
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtRefineAll) then
          i=CrwOn
        else if(CheckNumber.eq.nButtFixAll) then
          i=CrwOff
        else if(CheckNumber.eq.nButtReset) then
          i=0
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do j=1,n
            if(CrwLogicQuest(nCrw)) then
              pom=ParDef(j)
            else
              pom=0.
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else if(CheckNumber.eq.nButtInSig) then
          InSig=.not.InSig
          if(InSig) then
            Veta='%Edit mode'
          else
            Veta='Show %p/sig(p)'
          endif
          call FeQuestButtonLabelChange(nButtInSig,Veta)
          go to 1400
        endif
        if(i.ne.0) then
          do nCrw=nCrwPrv,nCrwPrv+n-1
            call FeQuestCrwOpen(nCrw,i.eq.CrwOn)
          enddo
        endif
        go to 2000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0)
     1  call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,Par,kiPar,n)
      call FeQuestRemove(id)
      return
      end
      subroutine FeUpdateParamAndKeys(nEdwFirst,nCrwFirst,Param,KiParam,
     1                                n)
      include 'fepc.cmn'
      dimension Param(n),KiParam(n)
      integer EdwStateQuest,CrwStateQuest
      logical CrwLogicQuest
      if(nEdwFirst.eq.0.or.nCrwFirst.eq.0) go to 9999
      nEdw=nEdwFirst
      nCrw=nCrwFirst
      do i=1,n
        if(EdwStateQuest(nEdw).eq.EdwOpened)
     1    call FeQuestRealFromEdw(nEdw,Param(i))
        if(CrwStateQuest(nCrw).eq.CrwOff.or.
     1     CrwStateQuest(nCrw).eq.CrwOn) then
          if(CrwLogicQuest(nCrw)) then
            KiParam(i)=1
          else
            KiParam(i)=0
          endif
        else
          KiParam(i)=0
        endif
        if(CrwStateQuest(nCrw).eq.CrwLocked) KiParam(i)=KiParam(i)+10
        if(EdwStateQuest(nEdw).eq.EdwLocked) KiParam(i)=KiParam(i)+100
        nEdw=nEdw+1
        nCrw=nCrw+1
      enddo
9999  return
      end
      subroutine TitulekVRamecku(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      character*128 t128
      idl=idel(Veta)
      t128='* '//Veta(:idl)//' *'
      idl=idel(t128)
      call newln(5)
      write(lst,FormA1)
      write(lst,FormA1)('*',i=1,idl)
      write(lst,FormA1)(t128(i:i),i=1,idl)
      write(lst,FormA1)('*',i=1,idl)
      write(lst,FormA1)
      return
      end
      subroutine TitulekPodtrzeny(Veta,Podtrzitko)
      include 'fepc.cmn'
      character*(*) Veta
      character*1   Podtrzitko
      idl=idel(Veta)
      call newln(2)
      write(lst,FormA1)(Veta(i:i),i=1,idl)
      write(lst,FormA1)(Podtrzitko,i=1,idl)
      return
      end
      subroutine MinMultMaxFract(Ia,n,MinMult,MaxFract)
      include 'fepc.cmn'
      dimension Ia(n),MaxArr(30),MinArr(30)
      integer PrimeExp(30)
      logical Poprve
      Poprve=.true.
      do i=1,n
        if(Ia(i).ne.0) then
          call SplitIntoPrimes(iabs(Ia(i)),PrimeExp)
          if(Poprve) then
            call CopyVekI(PrimeExp,MaxArr,30)
            call CopyVekI(PrimeExp,MinArr,30)
            Poprve=.false.
          else
            do j=1,30
              MaxArr(j)=max(MaxArr(j),PrimeExp(j))
              MinArr(j)=min(MinArr(j),PrimeExp(j))
            enddo
          endif
        endif
      enddo
      MinMult=1
      MaxFract=1
      do i=1,30
        MinMult=MinMult*PrimeNumbers(i)**MaxArr(i)
        MaxFract=MaxFract*PrimeNumbers(i)**MinArr(i)
      enddo
      return
      end
      subroutine SplitIntoPrimes(ix,PrimeExp)
      include 'fepc.cmn'
      integer PrimeExp(30)
      call SetIntArrayTo(PrimeExp,30,0)
      iy=ix
      do i=1,30
        ip=PrimeNumbers(i)
        if(ip.gt.iy) go to 9999
        n=0
1000    if(mod(iy,ip).eq.0) then
          iy=iy/ip
          n=n+1
          go to 1000
        endif
        PrimeExp(i)=n
      enddo
9999  return
      end
      subroutine FeVoid()
      include 'fepc.cmn'
      return
      end
      subroutine PisOperator(ln,rm,s,zn,n)
      include 'fepc.cmn'
      dimension rm(n,n),s(n)
      character*28 FormOut
      data FormOut/'(1x,'' |'',3i3,'' |'',f6.3,'' |'')'/
      write(FormOut(10:10),'(i1)') n
      write(ln,FormOut)((nint(zn*rm(i,j)),j=1,n),s(i),i=1,n)
      return
      end
      subroutine FeUnforeseenError(Text)
      include 'fepc.cmn'
      character*(*) Text
      NInfo=1
      if(Text.ne.' ') then
        TextInfo(1)=Text
        NInfo=NInfo+1
      endif
      TextInfo(NInfo)='Unforeseen error, please contact authors.'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      return
      end
      subroutine PisSymetry(ln,rm,s,n,nd,ndm)
      dimension rm(ndm**2,n),s(ndm,n)
      character*1 Znak
      do i=1,n
        write(ln,'('' Operation#'',i3)') i
        call PisOperator(ln,rm(1,i),s(1,i),1.,nd)
        if(i.eq.n) then
          Znak='='
        else
          Znak='-'
        endif
        write(ln,'(1x,333a1)')(znak,j=1,13+nd*3)
      enddo
      return
      end
      subroutine Latin2ToWindows(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      dimension ILat2(39),IWin(39)
      data IWin /z"8A",z"8D",z"8E",z"9A",z"9D",z"9E",z"C1",z"C8",z"C9",
     1           z"CC",z"CD",z"CF",z"D2",z"D3",z"D8",z"D9",z"DA",z"DD",
     2           z"E1",z"E8",z"E9",z"EC",z"D8",z"ED",z"EF",z"F2",z"F3",
     3           z"F8",z"F9",z"FA",z"FD",z"C4",z"CB",z"D6",z"DC",z"E4",
     4           z"EB",z"F6",z"FC"/
      data ILat2/z"E6",z"9B",z"A6",z"E7",z"9C",z"A7",z"B5",z"AC",z"90",
     1           z"B7",z"D6",z"D2",z"D5",z"E0",z"FC",z"DE",z"E9",z"ED",
     2           z"A0",z"9F",z"82",z"D8",z"FC",z"A1",z"D4",z"E5",z"A2",
     3           z"FD",z"85",z"A3",z"EC",z"8E",z"D3",z"99",z"9A",z"84",
     4           z"89",z"94",z"81"/
      Klic=0
      go to 1000
      entry WindowsToLatin2(Veta)
      Klic=1
1000  do i=1,idel(Veta)
        ichp=ichar(Veta(i:i))
        do j=1,39
          if(Klic.eq.0) then
            if(ILat2(j).eq.ichp) go to 1100
          else
            if(IWin(j).eq.ichp) go to 1100
          endif
        enddo
        j=0
1100    if(j.gt.0) then
          if(Klic.eq.0) then
            Veta(i:i)=char(IWin(j))
          else
            Veta(i:i)=char(ILat2(j))
          endif
        endif
      enddo
      return
      end
      integer function SelFromLongList(Text,Menu,NMenu)
      include 'fepc.cmn'
      character*(*) Menu(*),Text
      character*256 FileName
      integer SbwItemPointerQuest
      SelFromLongList=0
      ln=NextLogicNumber()
      FileName=TmpDir(:idel(TmpDir))//'vyber.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      do i=1,NMenu
        write(ln,FormA) Menu(i)(:idel(Menu(i)))
      enddo
      call CloseIfOpened(ln)
      nrow=NMenu/2
      ncol=2
      il=nint(float(nrow)*.9)
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=il
      ild=il
      call FeQuestSbwMake(id,xpom,il,dpom,ild,ncol,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,FileName)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) SelFromLongList=SbwItemPointerQuest(nSbw)
      call FeQuestRemove(id)
      go to 9999
9000  SelFromLongList=-1
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      return
      end
      subroutine FeRewriteIni(KeyWord,Value)
      include 'fepc.cmn'
      character*(*) Keyword,Value
      character*256 t256,p256
      logical EqIgCase
      call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini','#ini#.tmp')
      t256=MasterName(:idel(MasterName))//'.'//KeyWord(:idel(KeyWord))//
     1     ':'
      idt=idel(t256)
      t256=t256(:idt)//' '//Value(:idel(Value))
      lni=NextLogicNumber()
      open(lni,file='#ini#.tmp')
      lno=NextLogicNumber()
      open(lno,file=HomeDir(:idel(HomeDir))//
     1     MasterName(:idel(MasterName))//'.ini')
1000  read(lni,FormA,end=2000) p256
      if(EqIgCase(t256(:idt),p256(:idt))) then
        p256=t256
        t256=' '
      endif
      write(lno,FormA1)(p256(i:i),i=1,idel(p256))
      go to 1000
2000  if(t256.ne.' ') write(lno,FormA1)(t256(i:i),i=1,idel(t256))
      call DeleteFile('#ini#.tmp')
      call CloseIfOpened(lno)
      return
      end
      subroutine QuestZaklad(ich)
      include 'fepc.cmn'
      ich=0
      id=NextQuestId()
      xqd=150.
      il=5
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Nejaky text',0,LightGray,
     1                   0,0)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then

      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then

      endif
9999  return
      end

