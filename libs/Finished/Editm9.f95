      subroutine EM9SetBasic
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      nDiffTypes=22
      OrderDiffTypes( 1)=IdCAD4
      OrderDiffTypes( 2)=IdNoniusCCD
      OrderDiffTypes( 3)=IdSiemensP4
      OrderDiffTypes( 4)=IdBrukerCCD
      OrderDiffTypes( 5)=IdKumaCCD
      OrderDiffTypes( 6)=IdKumaPD
      OrderDiffTypes( 7)=IdRigakuCCD
      OrderDiffTypes( 8)=IdIPDSStoe
      OrderDiffTypes( 9)=IdD9ILL
      OrderDiffTypes(10)=IdVivaldi
      OrderDiffTypes(11)=IdSXD
      OrderDiffTypes(12)=IdTopaz
      OrderDiffTypes(13)=IdKoala
      OrderDiffTypes(14)=IdSCDLANL
      OrderDiffTypes(15)=IdHasyLabF1
      OrderDiffTypes(16)=IdHasyLabHuber
      OrderDiffTypes(17)=IdXDS
      OrderDiffTypes(18)=Id6T2LBB
      OrderDiffTypes(19)=IdPets
      OrderDiffTypes(20)=IdSENJU
      OrderDiffTypes(21)=IdPolNeutrons
      OrderDiffTypes(22)=IdSHELXINoAbsCorr

      nSingleTypes=12
      OrderSingleTypes( 1)=IdImportSHELXF
      OrderSingleTypes( 2)=IdImportSHELXI
      OrderSingleTypes( 3)=IdImportHKLF5
      OrderSingleTypes( 4)=IdImportIPDS
      OrderSingleTypes( 5)=IdImportCCDBruker
      OrderSingleTypes( 6)=IdImportCIF
      OrderSingleTypes( 7)=IdImportGraindex
      OrderSingleTypes( 8)=IdImportFullProf
      OrderSingleTypes( 9)=IdImportXD
      OrderSingleTypes(10)=IdImportDABEX
      OrderSingleTypes(11)=IdImportGeneralF
      OrderSingleTypes(12)=IdImportGeneralI

      nCWPowderTypes=20
      OrderCWPowderTypes( 1)=IdDataMAC
      OrderCWPowderTypes( 2)=IdDataGSAS
      OrderCWPowderTypes( 3)=IdDataRiet7
      OrderCWPowderTypes( 4)=IdDataD1AD2BOld
      OrderCWPowderTypes( 5)=IdDataD1AD2B
      OrderCWPowderTypes( 6)=IdDataD1BD20
      OrderCWPowderTypes( 7)=IdDataG41
      OrderCWPowderTypes( 8)=IdDataSaclay
      OrderCWPowderTypes( 9)=IdDataPSI
      OrderCWPowderTypes(10)=IdData11BM
      OrderCWPowderTypes(11)=IdDataAPS
      OrderCWPowderTypes(12)=IdDataCPI
      OrderCWPowderTypes(13)=IdDataUXD
      OrderCWPowderTypes(14)=IdDataM92
      OrderCWPowderTypes(15)=IdDataXRDML
      OrderCWPowderTypes(16)=IdDataPwdRigaku
      OrderCWPowderTypes(17)=IdDataPwdHuber
      OrderCWPowderTypes(18)=IdDataPwdStoe
      OrderCWPowderTypes(19)=IdDataFreeOnlyI
      OrderCWPowderTypes(20)=IdDataFree

      nTOFPowderTypes=7
      OrderTOFPowderTypes( 1)=IdTOFGSAS
      OrderTOFPowderTypes( 2)=IdTOFISISD
      OrderTOFPowderTypes( 3)=IdTOFISIST
      OrderTOFPowderTypes( 4)=IdTOFISISFullProf
      OrderTOFPowderTypes( 5)=IdTOFVega
      OrderTOFPowderTypes( 6)=IdTOFFree
      OrderTOFPowderTypes( 7)=IdEDInel
      end
      subroutine EM9CreateM90(Klic,ich)
      use Basic_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      logical ExistFile
      dimension ip(1)
      character*256 t256,Veta,Radka(:)
      character*80  FileNameLst,VetaPom
      character*20  RadString(MxDatBlock)
      integer SbwItemPointerQuest,Tisk,RefDatCorrespondOld(MxRefBlock),
     1        RefDatCorrespondPom(MxRefBlock)
      logical CrwLogicQuest,Prazdno,WizardModeIn
      allocatable Radka
      WizardModeIn=WizardMode
      ich=0
      KDatBlockIn=KDatBlock
      KPhaseIn=KPhase
      if(StatusM50.gt.100) then
        call FeChybne(-1.,-1.,'M50 doesn''t contain either parameters '
     1              //'or symmetry information.',' ',SeriousError)
        ich=1
        go to 9900
      endif
      if(kcommenMax.ne.0) then
        if(LstOpened) then
          Tisk=1
        else
          Tisk=0
        endif
        call comsym(0,Tisk,ich)
        call iom50(0,0,fln(:ifln)//'.m50')
      endif
      if(ExistFile(fln(:ifln)//'.m95')) then
        ExistM95=.true.
      else
        call FeChybne(-1.,-1.,'M90 file cannot be created as file',
     1                fln(:ifln)//'.m95 doesn''t exist.',SeriousError)
        ich=1
        go to 9900
      endif
1000  call iom95(0,fln(:ifln)//'.m95')
      call CopyVekI(RefDatCorrespond,RefDatCorrespondOld,MxRefBlock)
      if(ExistM90) then
        call iom90(0,fln(:ifln)//'.m90')
      else if(ParentM90.ne.' ') then
        call iom90(0,ParentM90)
      else
        NDatBlock=0
        LamAve(1)=-1.
      endif
      NDatBlockOld=NDatBlock
      do i=1,NDatBlock
        if(LamAve(i).lt.0.) then
          call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          NDatBlock=0
          cycle
        endif
      enddo
      do i=1,NRefBlock
        k=RefDatCorrespond(i)
        if(k.gt.NDatBlock.or.k.le.0) then
          call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          NDatBlock=0
          exit
        endif
      enddo
      n=0
      if(ParentM90.ne.' ') call iom90(0,ParentM90)
      do 1120i=1,NRefBlock
        k=RefDatCorrespond(i)
        if(DifCode(i).lt.100.and.
     1     (RadiationRefBlock(i).ne.ElectronRadiation.or.
     2      .not.CalcDyn)) n=n+1
        if(k.le.0) then
          if(DifCode(i).lt.100.and.
     1       (RadiationRefBlock(i).ne.ElectronRadiation.or.
     2      .not.CalcDyn)) then
            do j=1,NDatBlock
              if(RadiationRefBlock(i).eq.Radiation(j))
     1          then
                if(Radiation(j).eq.NeutronRadiation.or.
     1             abs(LamAve(j)-
     2                 LamAveRefBlock(i)).lt..00001) then
                  RefDatCorrespond(i)=j
                  ModifyDatBlock(j)=.true.
                  if(ParentM90.eq.' ') then
                    call SetBasicM90(NDatBlock)
                    call SetBasicM90FromM95(i,NDatBlock)
                  endif
                  go to 1120
                endif
              endif
            enddo
          endif
          NDatBlock=NDatBlock+1
          ModifyDatBlock(NDatBlock)=.true.
          RefDatCorrespond(i)=NDatBlock
          k=NDatBlock
        else
          ModifyDatBlock(k)=ModifiedRefBlock(i).or.ModifyDatBlock(k)
        endif
        if(ParentM90.eq.' '.and.ModifyDatBlock(k)) then
          call SetBasicM90(NDatBlock)
          call SetBasicM90FromM95(i,NDatBlock)
        endif
1120  continue
      if(n.gt.1) then
        if(SilentRun) go to 1300
        Veta='Modify grouping of data collection blocks into '//
     1       'refinement blocks'
        id=NextQuestId()
        xqd=600.
        il=nint(float((NRefBlock+2)*8)*.1)
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
        il=-18
        xpom=5.
        Veta='Data collection block'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        tpom=0.
        do i=1,NRefBlock
          il=il-8
          call ExtractFileName(SourceFileRefBlock(i),Veta)
          if(DifCode(i).gt.200) then
            t256='->Powder TOF or ED'
          else if(DifCode(i).gt.100) then
            t256='->Powder CW'
          else
            t256='->Single crystal'
          endif
          Veta=Veta(:idel(Veta))//t256(:idel(t256))
          if(RadiationRefBlock(i).eq.XRayRadiation) then
            Cislo='/X-rays/'
          else if(RadiationRefBlock(i).eq.NeutronRadiation) then
            Cislo='/Neutrons/'
          else if(RadiationRefBlock(i).eq.ElectronRadiation) then
            Cislo='/Electrons/'
          else
            Cislo='/-/'
          endif
          Veta=Veta(:idel(Veta))//Cislo
          k=KLamRefBlock(i)
          if(RadiationRefBlock(i).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAveRefBlock(i).gt.0.) then
              write(Cislo,'(f10.6)') LamAveRefBlock(i)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          Veta=Veta(:idel(Veta))//Cislo
          call FeQuestLblMake(id,xpom,il,Veta,'L','N')
          tpom=max(tpom,FeTxLength(Veta)+20.)
        enddo
        il=-10
        xpom=tpom
        Veta='Data refinement block'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        il=il-8
        do i=1,NRefBlock
          write(Veta,'(''#'',i5)') i
          call Zhusti(Veta)
          call FeQuestLblMake(id,xpom,il,Veta,'L','N')
          xpom=xpom+30.
        enddo
        do i=1,NRefBlock
          il=il-8
          xpom=tpom+3.
          do j=1,i
            call FeQuestCrwMake(id,tpom,il,xpom,il,' ','L',CrwXd,CrwYd,
     1                          0,i)
            call FeQuestCrwOpen(CrwLastMade,j.eq.RefDatCorrespond(i))
            if(i.eq.1.and.j.eq.1) nCrwPrv=CrwLastMade
            xpom=xpom+30.
          enddo
        enddo
1200    call FeQuestEVent(id,ich)
        if(CheckType.eq.EVentButton.and.CheckNumberAbs.eq.ButtonOk) then
          n=0
          nCrw=nCrwPrv
          do i=1,NRefBlock
            do j=1,i
              if(CrwLogicQuest(nCrw)) then
                RefDatCorrespondPom(i)=j
                n=max(j,n)
              endif
              nCrw=nCrw+1
            enddo
          enddo
          Prazdno=.false.
          do j=1,n
            m=0
            mp=0
            mm=0
            mn=0
            mx=0
            me=0
            do i=1,NRefBlock
              if(RefDatCorrespondPom(i).eq.j) then
                m=m+1
                if(DifCode(i).gt.100) then
                  mp=mp+1
                else
                  if(RadiationRefBlock(i).eq.XRayRadiation) then
                    mx=mx+1
                  else if(RadiationRefBlock(i).eq.NeutronRadiation) then
                    mn=mn+1
                  else if(RadiationRefBlock(i).eq.ElectronRadiation)
     1              then
                    me=me+1
                  endif
                endif
              endif
            enddo
            if(m.eq.0) then
              Prazdno=.true.
              jp=j
            else if(m.gt.0.and.Prazdno) then
              write(Cislo,'(i5)') jp
              call Zhusti(Cislo)
              Veta='the reflection block #'//Cislo(:idel(Cislo))//
     1             ' is empty but higher block(s) are not.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
            write(Cislo,'(i5)') j
            call Zhusti(Cislo)
            if(mp.gt.1) then
              Veta='the reflection block #'//Cislo(:idel(Cislo))//
     1             ' contains more than one powder diffraction set.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
            if((mx.gt.0.and.(mn.gt.0.or.me.gt.0)).or.
     1         (mn.gt.0.and.(mx.gt.0.or.me.gt.0)).or.
     2         (me.gt.0.and.(mx.gt.0.or.mn.gt.0))) then
              Veta='the combined reflection block #'//
     1             Cislo(:idel(Cislo))//
     2             ' contains data collected by different type of '//
     3             'radiation.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
          enddo
          QuestCheck(id)=0
          go to 1200
1220      call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1200
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1200
        endif
        if(ich.eq.0) then
          nCrw=nCrwPrv
          do i=1,NRefBlock
            do j=1,i
              if(CrwLogicQuest(nCrw)) then
                if(j.ne.RefDatCorrespond(i)) then
                  ModifyDatBlock(j)=.true.
                  RefDatCorrespond(i)=j
                  if(j.gt.NDatBlock) then
                    NDatBlock=j
                    call SetBasicM90(j)
                    call SetBasicM90FromM95(j,i)
                  endif
                endif
              endif
              nCrw=nCrw+1
            enddo
          enddo
        endif
        call FeQuestRemove(id)
        go to 1500
1300    ModifyDatBlock=.true.
        RefDatCorrespondOld=0
      endif
1500  do i=1,NDatBlock
        if(ModifyDatBlock(i)) then
          do j=1,NRefBlock
            if(RefDatCorrespond(j).eq.i) then
              if(RefDatCorrespond(j).ne.RefDatCorrespondOld(j).or.
     1           RefDatCorrespondOld(j).eq.0)
     2          call SetBasicM90FromM95(j,i)
              ich=0
              go to 2500
            endif
          enddo
        endif
      enddo
      if(Klic.eq.0) go to 9900
      if(NDatBlock.eq.1.or.SilentRun) then
        ModifyDatBlock(KDatBlock)=.true.
        go to 2500
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_m90list.tmp','formatted',
     1              'unknown')
      n=0
      do i=1,NDatBlock
        if(NRef90(i).le.0) cycle
        if(KRefBlock.le.0) KRefBlock=i
        n=n+1
        k=1
        Veta=' '
        do j=1,NRefBlock
          if(RefDatCorrespond(j).eq.i) then
            write(Cislo,'(i2,'','')') j
            k=k+1
            call Zhusti(Cislo)
            idl=idel(Veta)
            Veta(idl+1:)=Cislo(:idel(Cislo))
          endif
        enddo
        j=idel(Veta)
        Veta(j:j)=' '
        Veta='#'//Veta(:j-1)
        j=j+1
        if(k.gt.1) Veta='s'//Veta(:j)
        write(Cislo,100) i
        call Zhusti(Cislo)
        Veta=Cislo(:idel(Cislo))//'-made of Refblock'//
     1       Veta(:idel(Veta))
        Veta=Veta(:idel(Veta))//Tabulator//'|'//Tabulator
        if(iabs(DataType(i)).eq.2) then
          VetaPom='Powder'
        else if(DataType(i).eq.1) then
          VetaPom='Single crystal'
        endif
        Veta=Veta(:idel(Veta))//VetaPom(:idel(VetaPom))//Tabulator//
     1       '|'//Tabulator
        if(Radiation(i).eq.XRayRadiation) then
          RadString(i)='X-rays'
        else if(Radiation(i).eq.NeutronRadiation) then
          RadString(i)='Neutrons'
        else
          RadString(i)=' '
        endif
        k=KLam(i)
        if(Radiation(i).eq.XRayRadiation.and.k.gt.0) then
          Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
        else
          if(LamAve(i).gt.0.) then
            write(Cislo,'(f10.6)') LamAve(i)
            call ZdrcniCisla(Cislo,1)
          else
            Cislo='TOF'
          endif
        endif
        RadString(i)=RadString(i)(:idel(RadString(i))+1)//
     1               Cislo(:idel(Cislo))
        Veta=Veta(:idel(Veta))//RadString(i)(:idel(RadString(i)))
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      id=NextQuestId()
      xqd=600.
      il=1
      Veta='Create refinement reflection file'
      WizardMode=.false.
      ilk=min(ifix((float(n)*MenuLineWidth+2.)/QuestLineWidth)+3,15)
      ilk=max(ilk,6)
      dpom=xqd-20.-SbwPruhXd
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,-1,0)
      call FeQuestLblMake(id,           20.,il,'File'     ,'L','N')
      call FeQuestLblMake(id,   dpom/2.+10.,il,'Type'     ,'L','N')
      call FeQuestLblMake(id,3.*dpom/4.+10.,il,'Radiation','L','N')
      il=ilk-1
      xpom=10.+EdwMarginSize
      UseTabs=NextTabs()
      call FeTabsReset(UseTabs)
      call FeTabsAdd(dpom/2.-xpom,UseTabs,IdRightTab,' ')
      call FeTabsAdd(dpom/2.-xpom+10.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(3.*dpom/4.-xpom,UseTabs,IdRightTab,' ')
      call FeTabsAdd(3.*dpom/4.-xpom+10.,UseTabs,IdRightTab,' ')
      xpom=10.
      il=ilk-1
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_m90list.tmp')
      dpom=xqd-10.
      xpom=5.
      il=ilk
      dpom=70.
      spom=15.
      tpom=dpom+spom
      xpom=xqd*.5-dpom*1.5-spom*.5
      do i=1,3
        if(i.eq.1) then
          Veta='%Info'
        else if(i.eq.2) then
          nButtInfo=ButtonLastMade
          Veta='%Rebuild'
        else if(i.eq.3) then
          nButtRebuild=ButtonLastMade
          Veta='%Delete'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(n.le.0) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+tpom
      enddo
      nButtDelete=ButtonLastMade
2150  call FeQuestEVent(id,ich)
      if(CheckType.eq.EVentButton) then
        k=SbwItemPointerQuest(nSbw)
2162    if(CheckNumber.eq.nButtInfo) then
          NInfo=1
          write(Cislo,100) k
          TextInfo(NInfo)='DatBlock'//Cislo(:idel(Cislo))//
     1                    ' is made from file(s):'
          do i=1,NRefBlock
            if(RefDatCorrespond(i).ne.k) cycle
            NInfo=NInfo+1
            TextInfo(NInfo)=SourceFileRefBlock(i)
          enddo
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          go to 2150
        else if(CheckNumber.eq.nButtDelete) then
          NRef90(k)=0
          call FeQuestRemove(id)
          call FeTabsReset(UseTabs)
          do i=1,NRefBlock
            if(RefDatCorrespond(i).eq.k) then
              nref95(i)=0
            else if(RefDatCorrespond(i).gt.k) then
              RefDatCorrespond(i)=RefDatCorrespond(i)-1
            endif
          enddo
          KDatBlock=1
          call UpdateDatBlocksM40
          call CompleteM95(0)
          call CompleteM90
          if(ExistM40) call iom40(1,0,fln(:ifln)//'.m40')
          if(ExistM90) go to 1000
        else if(CheckNumber.eq.nButtRebuild) then
          ModifyDatBlock(k)=.true.
          call FeQuestRemove(id)
          call DeleteFile(fln(:ifln)//'_m90list.tmp')
          go to 2500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2150
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_m90list.tmp')
      call FeTabsReset(UseTabs)
      go to 9900
2500  NDatBlockSingle=0
      NDatBlockPowder=0
      do KDatBlock=1,NDatBlock
        if(DataType(KDatBlock).eq.1) then
          NDatBlockSingle=NDatBlockSingle+1
        else
          NDatBlockPowder=NDatBlockPowder+1
        endif
      enddo
      call FeTabsReset(UseTabs)
      Veta=fln(:ifln)//'.rre'
      if(ExistFile(Veta)) then
        call OpenFile(lst,fln(:ifln)//'.rre','formatted','unknown')
        ln=NextLogicNumber()
        Konec=0
        allocate(Radka(mxline))
        FileNameLst=' '
2610    do i=1,mxline
          read(lst,FormA256,end=2630) Radka(i)
        enddo
        i=mxline
        go to 2635
2630    Konec=1
2635    do j=1,i
          do k=1,5
            l=LocateSubstring(Radka(j),
     1                        LabelsEditm9(k)(:idel(LabelsEditm9(k))),
     2                        .false.,.true.)
            if(l.gt.0) go to 2645
          enddo
          cycle
2645      l=LocateSubstring(Radka(j),'#',.false.,.true.)
          if(l.ne.0) then
            Cislo=Radka(j)(l+1:l+2)
            kk=0
            call StToInt(Cislo,kk,ip,1,.false.,ich)
            if(ich.eq.0) then
              l=ip(1)
            else
              l=1
            endif
          else
            l=1
          endif
          write(Cislo,'(i2,''_'',i2)') k,l
          call Zhusti(Cislo)
          FileNameLst=fln(:ifln)//'_em9_'//Cislo(:idel(Cislo))//'.tmp'
          call CloseIfOpened(ln)
          call OpenFile(ln,FileNameLst,'formatted','unknown')
          exit
        enddo
        if(FileNameLst.ne.' ') then
          do j=1,i
            write(ln,FormA) Radka(j)(:idel(Radka(j)))
          enddo
        endif
        if(Konec.eq.0) go to 2610
        call CloseIfOpened(ln)
        deallocate(Radka)
      endif
!      if(.not.ExistM90) then
!        ExistM90=.true.
!        call iom50(1,0,fln(:ifln)//'.m50')
!        ExistM90=.false.
!      endif
      n=0
      do KDatBlock=1,NDatBlock
        if(ModifyDatBlock(KDatBlock)) then
          if(DataType(KDatBlock).eq.1) then
            n=n+1
            isPowder=.false.
            KPh=0
            if(maxNDim.gt.3) then
              do KPhP=1,NPhase
                if(maxNDim.eq.NDim(KPhP)) then
                  KPh=KPhP
                endif
              enddo
            endif
            KPhase=max(KPh,1)
            call SetSatGroups
            call EM9CreateM90Single(n,ich)
          else
            isPowder=.true.
            call EM9CreateM90Powder(ich)
          endif
          if(ich.ne.0) then
            call CloseIfOpened(lst)
            go to 9900
          endif
          ModifyDatBlock(KDatBlock)=.false.
        endif
      enddo
      if(NDatBlockSingle.ge.1) then
        call OpenFile(lst,fln(:ifln)//'.rre','formatted','unknown')
        do KDatBlock=1,NDatBlock
          ln=NextLogicNumber()
          npg=0
          do i=1,5
            FileNameLst=fln(:ifln)//'_em9_'
            write(Cislo,FormI15) i
            call Zhusti(Cislo)
            FileNameLst=FileNameLst(:idel(FileNameLst))//
     1                  Cislo(:idel(Cislo))//'_'
            write(Cislo,FormI15) KDatBlock
            call Zhusti(Cislo)
            FileNameLst=FileNameLst(:idel(FileNameLst))//
     1                  Cislo(:idel(Cislo))//'.tmp'
            if(.not.ExistFile(FileNameLst)) cycle
            call OpenFile(ln,FileNameLst,'formatted','unknown')
3020        read(ln,FormA256,end=3040) t256
            j=index(t256,'page=')
            if(j.gt.0) then
              npg=npg+1
              write(Cislo,'(i3)') npg
              t256(j+5:)=Cislo(:3)
            endif
            write(lst,FormA) t256(:idel(t256))
            go to 3020
3040        call CloseIfOpened(ln)
            call DeleteFile(FileNameLst)
          enddo
        enddo
        call CloseIfOpened(lst)
      endif
      KDatBlock=1
      if(ParentStructure) then
        call SSG2QMag(fln)
        call iom40Only(0,0,fln(:ifln)//'.m40')
      endif
      call UpdateDatBlocksM40
      if(ParentStructure) then
        call iom40Only(1,0,fln(:ifln)//'.m40')
        call QMag2SSG(fln,0)
        call iom40Only(0,0,fln(:ifln)//'.m44')
      endif
      call CompleteM95(0)
      call CompleteM90
      if(UpdateAnomalous) then
        call ReallocFormF(NAtFormula(1),NPhase,NDatBlock)
        do KPh=1,NPhase
          do i=1,NAtFormula(KPh)
            do KDatB=NDatBlockOld+1,NDatBlock
              if(Radiation(KDatB).eq.XRayRadiation) then
                if(KAnRef(KDatB).ne.1) then
                  if(klam(KDatB).gt.0) then
                    call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f''',
     1                           ffra(i,KPh,KDatB),klam(KDatB),ich)
                    call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f"',
     1                           ffia(i,KPh,KDatB),klam(KDatB),ich)
                  else
                    call EM50ReadAnom(AtTypeFull(i,KPh),
     1                ffra(i,KPh,KDatB),ffia(i,KPh,KDatB),KDatB)
                  endif
                endif
              else if(Radiation(KDatB).eq.NeutronRadiation) then
                ffra(i,KPh,KDatB)=0.
                ffia(i,KPh,KDatB)=0.
              endif
            enddo
          enddo
        enddo
      endif
      if(ExistM40) then
        if(ParentStructure) then
          call iom40Only(1,0,fln(:ifln)//'.m44')
        else
          call iom40Only(1,0,fln(:ifln)//'.m40')
        endif
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(Klic.ne.0.and.NDatBlock.gt.1.and..not.SilentRun) go to 1500
9900  WizardMode=.false.
      KDatBlock=min(KDatBlockIn,NDatBlock)
      KPhase=KPhaseIn
      call DeleteFile(PreviousM50)
      WizardMode=WizardModeIn
      return
100   format('#',i2)
      end
      subroutine EM9CreateM90Single(n,ich)
      use Basic_mod
      use EDZones_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      dimension h(6),hp(6),hpp(6),sp(:),spo(:),ntwq(-mxsc:mxsc),
     1          ntwqo(-mxsc:mxsc),difi(3),ihi(6),mlim(3,MxPhases),
     2          mlimp(3,MxPhases),ihtw(6,50),itwa(50),IqOff(MxRefBlock)
      character*256 Veta
      character*128 Ven(4)
      character*80 t80,p80,FileNameAve,FileNameExp,FileNameLst
      character*2 nty
      logical FeYesNoHeader,SystExtRef,CrwLogicQuest,Nulova,RedukceDolu,
     1        HKLF5P,AlreadyAllocated,OnlyNext,MatRealEqUnitMat,lpom
      integer AcceptM91,YesNoSkip,UseTabsIn
      real LamAvePom,LamUsed
      allocatable sp,spo
      data difi/3*.01/
      NoOfScales=1
      LnSum=0
      AlreadyAllocated=.false.
      HKLF5P=.false.
      nsp=10000
      allocate(sp(nsp))
      if(.not.SilentRun) then
        if(NDatBlock.gt.1) then
          TitleSingleCrystal='Create refinement reflection file for : '
     1       //DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
     2       //'->Single crystal'
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            Veta='X-rays'
          else if(Radiation(KDatBlock).eq.NeutronRadiation) then
            Veta='neutrons'
          else if(Radiation(KDatBlock).eq.ElectronRadiation) then
            Veta='electrons'
          else
            Veta=' '
          endif
          k=KLam(KDatBlock)
          if(Radiation(KDatBlock).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAve(KDatBlock).gt.0.) then
              write(Cislo,'(f10.6)') LamAve(KDatBlock)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          TitleSingleCrystal=
     1      TitleSingleCrystal(:idel(TitleSingleCrystal))//'/'//
     2      Veta(:idel(Veta))//'/'//Cislo(:idel(Cislo))
          WizardTitle=.true.
          WizardLength=550.
          WizardLines=16
        else
          TitleSingleCrystal=' '
          WizardTitle=.false.
          WizardLength=450.
          WizardLines=15
        endif
        WizardId=NextQuestId()
        WizardMode=.true.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     TitleSingleCrystal,0,LightGray,0,0)
      endif
      RealIndices=.false.
      OnlyNext=.true.
      if(allocated(CifKey)) then
        AlreadyAllocated=.true.
      else
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9900
      endif
1050  NRefP=0
      NoRefItems(KDatBlock)=0
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          NRefP=NRefP+nref95(i)
          NoRefItems(KDatBlock)=NoRefItems(KDatBlock)+1
        endif
      enddo
      if(NRefP.le.0) NRefP=10000
      if(SilentRun) then
        UseEFormat91=Radiation(KDatBlock).eq.NeutronRadiation.and.
     1               MagPolFlag(KDatBlock).ne.0
        if(UseEFormat91) then
          Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
        else
          Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
        endif
        write(Format91(2:2),'(i1)') maxNDim
      else
        id=NextQuestId()
        if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1     TitleSingleCrystal(:idel(TitleSingleCrystal)))
        il=1
        tpom=5.
        t80='Reflections I<'
        call FeQuestLblMake(id,tpom,il,t80,'L','N')
        xpom=tpom+FeTxLength(t80)+5.
        dpom=40.
        tpom=xpom+dpom+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                      '*sig(I) will be sorted as unobserved','L',
     2                      dpom,EdwYd,0)
        nEdwFLim=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,EM9ObsLim(KDatBlock),
     1                          .false.,.false.)
        il=il+1
        xpom=5.
        t80='Note: this number is not interpreted by REFINE'
        call FeQuestLblMake(id,xpom,il,t80,'L','N')
        tpom=xpom+CrwgXd+5.
        il=il+1
        if(n.eq.1) then
          t80='%use in output file E-format (recommended for data '//
     1        'with large dynamical range)'
          call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,CrwgYd,
     1                        0,0)
          if(ExistM90) then
            lpom=UseEFormat91
          else
            if(Radiation(KDatBlock).eq.NeutronRadiation.and.
     1         MagPolFlag(KDatBlock).ne.0) then
              lpom=.true.
            else
              lpom=.false.
            endif
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if(Radiation(KDatBlock).eq.NeutronRadiation.and.
     1       MagPolFlag(KDatBlock).ne.0) then
            UseEFormat91=.true.
            call FeQuestCrwDisable(CrwLastMade)
          endif
          nCrwUseEFormat=CrwLastMade
        else
          if(UseEFormat91) then
            t80='e-format will be use in output file'
          else
            t80='f-format will be use in output file'
          endif
          call FeQuestLblMake(id,xpom,il,t80,'L','N')
          nCrwUseEFormat=0
        endif
        if(NoRefItems(KDatBlock).gt.1) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          write(Cislo,FormI15) KDatBlock
          call zhusti(Cislo)
          t80='Dat-block#'//Cislo(:idel(Cislo))//' consists of'
          write(Cislo,FormI15) NoRefItems(KDatBlock)
          call zhusti(Cislo)
          t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))//
     1        ' ref-blocks being:'
          il=il+1
          call FeQuestLblMake(id,tpom,il,t80,'L','N')
          xpom=5.
          tpom=xpom+CrwgXd+5.
          t80='on %different scales'
          do i=1,2
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,
     1                          CrwgYd,0,1)
            if(i.eq.1) then
              t80='on the %same scale'
              nCrwDiffScale=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.DiffScales(KDatBlock))
          enddo
        endif
        call FeQuestButtonDisable(ButtonEsc)
1150    call FeQuestEVent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        if(ich.eq.0) then
          call FeQuestRealFromEdw(nEdwFLim,EM9ObsLim(KDatBlock))
          if(NoRefItems(KDatBlock).gt.1) then
            nCrw=nCrwDiffScale
            do i=1,2
              if(CrwLogicQuest(nCrw)) then
                DiffScales(KDatBlock)=i
                exit
              endif
              nCrw=nCrw+1
            enddo
          else
            DiffScales(KDatBlock)=1
          endif
          if(Radiation(KDatBlock).ne.NeutronRadiation.or.
     1       MagPolFlag(KDatBlock).eq.0) then
            if(nCrwUseEFormat.gt.0) then
              UseEFormat91=CrwLogicQuest(nCrwUseEFormat)
            else
              UseEFormat91=.false.
            endif
          else
            UseEFormat91=.true.
          endif
          if(UseEFormat91) then
            Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
          else
            Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
          endif
          write(Format91(2:2),'(i1)') maxNDim
        endif
        if(ich.ne.0) go to 9900
      endif
      t80=fln(:ifln)//'.l91'
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      call OpenFile(91,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      if(LnSum.ne.0) call CloseIfOpened(LnSum)
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_MakeRefFile.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# MakeRefFile'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      EM9Sc=1.
      RedukceDolu=.false.
      iz=0
      izu=0
      call FeFlowChartOpen(-1.,-1.,max(nint(float(NRefP)*.005),10),
     1  NRefP,'Creating reflection file',' ',' ')
      if(ErrFlag.ne.0) go to 9900
      YesNoSkip=1
1400  StructureName=' '
      Uloha='Creating of the JANA reflection file'
      FileNameLst=fln(:ifln)//'_em9_1_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(1)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      AniNaListing=.true.
      call iom50(0,1,fln(:ifln)//'.m50')
      AniNaListing=.false.
      if(ErrFlag.ne.0) go to 9900
      call CloseListing
      FileNameLst=fln(:ifln)//'_em9_2_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(2)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call SetIntArrayTo(ihmin,maxNDim, 9999)
      call SetIntArrayTo(ihmax,maxNDim,-9999)
      call SetIntArrayTo(mlim,MxPhases*(maxNDim-3),0)
      SinThLMin= 9999.
      SinThLMax=-9999.
      NExt(1)=0
      NExtObs(1)=0
      NExt500(1)=0
      RIExtMax(1)=0.
      SumRelI(1)=0.
      NExt(2)=0
      NExtObs(2)=0
      NExt500(2)=0
      RIExtMax(2)=0.
      SumRelI(2)=0.
      rcc=0.
      rjc=0.
      rcn=0.
      rjn=0.
      NRef91=0
      NRef91Obs=0
      NRead=0
      NReadObs=0
      fomax=0.
      fsmax=0.
      do i=-mxsc,mxsc
        ntwq(i)=0
      enddo
      nq=0
      nqp=0
      itwmax=0
      itwmin=1
      ntw=0
      KRefBlockUsed=-1
      m=0
      if(kcommenMax.ne.0) call comsym(0,1,ich)
      do KRefBlock=1,NRefBlock
        if(RefDatCorrespond(KRefBlock).ne.KDatBlock) cycle
        m=m+1
        if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          IqOff(m)=nqp
        else
          if(DiffScales(KDatBlock).eq.1) then
            IqOff(m)=nq
          else if(DiffScales(KDatBlock).eq.2) then
            IqOff(m)=0
          else if(DiffScales(KDatBlock).eq.3) then
            if(ScNoScheme(m,KDatBlock).eq.m) then
              IqOff(m)=nq
            else
              IqOff(m)=IqOff(ScNoScheme(m,KDatBlock))
            endif
          endif
        endif
        if(KRefBlockUsed.lt.0) KRefBlockUsed=KRefBlock
        if(CorrLp(KRefBlock).eq.0) then
          call FeChybne(-1.,-1.,'LP correction has not been applied.',
     1                  ' ',SeriousError)
          go to 9900
        endif
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
        HKLF5P=HKLF5RefBlock(KRefBlock).eq.1
2100    call DRGetReflectionFromM95(95,iend,ich)
        if(ich.ne.0) go to 9000
        if(iend.ne.0) go to 2550
        iq=iflg(1)+IqOff(m)
        itw=iflg(2)
        ReadLam=DRLam
        call CopyVekI(ih,ihi,maxNDim)
        if(itw.eq.0) itw=1
        iatw=mod(iabs(itw),100)
        KPhase=KPhaseTwin(iatw)
        if(ReadLam.gt.0.) then
          LamAvePom=ReadLam
        else
          LamAvePom=LamAve(KDatBlock)
        endif
        if(iatw.gt.NTwin) then
          write(t80,format91)(ihi(i),i=1,maxNDim),ri,rs,iq,0,itw
          call FeChybne(-1.,-1.,'disagreement between twin flag and '//
     1                  'number of twin domains','for reflection : '//
     2                  t80(:idel(t80)),SeriousError)

          go to 9100
        endif
        if(iz.ge.izu) call FeFlowChartEVent(iz,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9100
        endif
        if(iq.lt.0) go to 2100
        if(no.gt.0) then
          pom1=corrf(1)*corrf(2)*EM9Sc
          if(RunScN(KRefBlock).gt.0) then
            ikde=min((RunCCD-1)/RunScGr(KRefBlock)+1,
     1                RunScN(KRefBlock))
            pom1=pom1*ScFrame(ikde,KRefBlock)
          endif
          ri=ri*pom1
          rs=rs*pom1
          if(rs.le.0.05.and..not.UseEFormat91) then
            if(.not.RedukceDolu.and.rs.gt.0.) then
2200          if(rs.le..05) then
                EM9Sc=EM9Sc*10.
                rs=rs*10.
                go to 2200
              endif
              rewind 95
              rewind 91
              izu=iz
              call CloseIfOpened(95)
              go to 1400
            endif
            rs=0.1
          else if(rs.le.0.) then
            rs=max(sqrt(ri),.1)
          endif
        else
          go to 2100
        endif
        NRead=NRead+1
        Nulova=.true.
        if(anint(ri*10.).gt.EM9ObsLim(KDatBlock)*anint(rs*10.)) then
          Nulova=.false.
          NReadObs=NReadObs+1
        endif
        call SetIntArrayTo(mlimp,3*Mxphases,9999)
        if(HKLF5P) then
!          KPhase=KPhaseTwin(1)
          if(KPhase.eq.1) then
            call MultmIRI(ihi,trmp,ih,1,maxNDim,maxNDim)
          else
            do i=1,maxNDim
              ih(i)=ihi(i)
            enddo
          endif
          do i=1,maxNDim
            hp(i)=ih(i)
          enddo
          do i=1,3
            do j=1,NDimI(KPhase)
              hp(i)=hp(i)+qu(i,j,1,KPhase)*hp(j+3)
            enddo
          enddo
          do isw=1,NComp(KPhase)
            call MultmIRI(ih,zvi(1,isw,KPhase),ihp,1,NDim(KPhase),
     1                 NDim(KPhase))
            do i=1,NDimI(KPhase)
              mlimp(i,1)=min(mlimp(i,1),iabs(ihp(i+3)))
            enddo
          enddo
          do i=1,NDimI(KPhase)
            mlim(i,1)=max(mlim(i,1),mlimp(i,1))
          enddo
          if(SystExtRef(ih,.false.,1)) then
            if(itw.lt.0) then
              if(.not.Nulova) NReadObs=NReadObs-1
              NRead=NRead-1
              go to 2100
            else
              if(ntw.le.0) then
                iswp=0
              else
                itwa(ntw)=iabs(mod(itwa(ntw),100))
                iswp=1
              endif
            endif
          else
            ntw=ntw+1
            if(ntw.le.50) then
              call CopyVekI(ih,ihtw(1,ntw),maxNDim)
              itwa(ntw)=itw
            endif
            if(itw.lt.0) then
              if(.not.Nulova) NReadObs=NReadObs-1
              NRead=NRead-1
              go to 2100
            endif
            if(ntw.gt.50) then
              ntw=0
              go to 2100
            endif
            iswp=1
          endif
        else
          mmax=0
          if(KCommen(KPhase).gt.0) then
            call CrlRestoreSymmetry(ISymmSSG(KPhase))
            do i=1,3
              do j=1,NDimI(KPhase)
                hp(i)=float(ihi(i))+qu(i,j,1,KPhase)*float(ihi(j+3))
              enddo
            enddo
            call IndFromIndReal(hp,NCommQProduct(1,KPhase),difi,ih,itw,
     1                          iswp,-1.,CheckExtRefYes)
            do i=4,NDim(KPhase)
              j=iabs(ih(i))
              mmax=max(j,mmax)
              mlimp(i-3,1)=min(mlimp(i-3,1),j)
            enddo
            call CrlRestoreSymmetry(ISymmCommen(KPhase))
          endif
          do i=1,maxNDim
            hp(i)=ihi(i)
          enddo
          call Multm(hp,trmp,h,1,maxNDim,maxNDim)
          do isw=1,NComp(KPhase)
            call Multm(h,zvi(1,isw,KPhase),hpp,1,NDim(KPhase),
     1                 NDim(KPhase))
            do i=4,NDim(KPhase)
              j=iabs(nint(hpp(i)))
              mmax=max(j,mmax)
              mlimp(i-3,1)=min(mlimp(i-3,1),j)
            enddo
          enddo
          if(kcommenMax.ne.0.and.mmax.eq.0) mmax=1
          do i=1,NDimI(KPhase)
            mlim(i,1)=max(mlim(i,1),mlimp(i,1))
          enddo
          do i=1,NDim(KPhase)
            ihp(i)=nint(h(i))
          enddo
          do i=1,3
            do j=1,NDimI(KPhase)
              h(i)=h(i)+qu(i,j,1,KPhase)*h(j+3)
            enddo
          enddo
          call multm(h,rtwi(1,iatw),hp,1,3,3)
          call IndFromIndReal(hp,mmax,difi,ih,itw,iswp,-1.,
     1                        CheckExtRefNo)
          if(iswp.le.0) then
            call EM9NejdouPotvory(2,ihi,hp,ri,rs,maxNDim,RealIndices,
     1                            EM9ObsLim(KDatBlock))
            go to 2100
          endif
          if(KCommen(KPhase).gt.0) then
            call CrlRestoreSymmetry(ISymmSSG(KPhase))
          endif
          call IndFromIndReal(hp,mmax,difi,ih,itw,iswp,-1.,
     1                        CheckExtRefYes)
          if(KCommen(KPhase).gt.0) then
            call CrlRestoreSymmetry(ISymmCommen(KPhase))
            ihp(1:NDim(KPhase))=ih(1:NDim(KPhase))
          endif
          if(iswp.gt.0.and.itw.eq.iatw)
     1      call CopyVekI(ih,ihp,NDim(KPhase))
        endif
        if(iswp.le.0) then
          call EM9NejdouPotvory(1,ih,hp,ri,rs,maxNDim,RealIndices,
     1                          EM9ObsLim(KDatBlock))
          go to 2100
        endif
        if(itw.eq.iatw) call CopyVekI(ihp,ih,NDim(KPhase))
        if(HKLF5P) then
          KPhase=KPhaseTwin(mod(itw,100))
        else
          KPhase=KPhaseTwin(1)
        endif
        call FromIndSinthl(ih,hp,sinthl,sinthlq,1,1)
        KPhase=KPhaseTwin(mod(itw,100))
        if(LamAvePom.gt.0.) then
          pom1=sinthl*LamAvePom
          if(abs(pom1).gt..999999.and.YesNoSkip.ne.0) then
            NInfo=2
            write(t80,'(''#'',i10,''...('',6(i5,'',''))')
     1        no,(ihi(i),i=1,maxNDim)
            call zhusti(t80)
            i=idel(t80)
            if(t80(i:i).eq.',') i=i-1
            write(t80(i+1:),'('')...sin(th)='',f6.3)') pom1
            i=idel(t80)
            TextInfo(1)='The reflection '//t80(:i)//
     1                  ' has unrealistic indices.'
            TextInfo(2)='It gives sin(theta)>1 and it will be skipped.'
            call FeMouseShape(0)
            if(FeYesNoHeader(-1.,YBottomMessage,
     1                       'Do you want to stop the whole '//
     2                       'procedure?',YesNoSkip)) then
              go to 9100
            else
              call FeMouseShape(3)
              YesNoSkip=0
              pom1=.999999
              go to 2100
            endif
          endif
        endif
        if(SinThL.gt.SinThLMax) then
          SinThLMax=SinThL
          LamUsed=LamAvePom
        endif
        SinThLMin=min(SinThLMin,SinThL)
        fomax=max(fomax,ri)
        fsmax=max(fsmax,rs)
        if(fomax.gt.999999.9.and..not.UseEFormat91) then
          RedukceDolu=.true.
2300      if(fomax.gt.999999.9) then
            EM9Sc=EM9Sc*.1
            fomax=fomax*.1
            fsmax=fsmax*.1
            go to 2300
          endif
2400      if(fsmax.gt.9999.9) then
            EM9Sc=EM9Sc*.1
            fsmax=fsmax*.1
            go to 2400
          endif
          rewind 95
          rewind 91
          call FeFlowChartRefresh
          call CloseIfOpened(95)
          go to 1400
        endif
        do i=1,6
          ihmax(i)=max(ihmax(i),ih(i))
          ihmin(i)=min(ihmin(i),ih(i))
        enddo
        rcc=rcc+rs
        rjc=rjc+ri
        NRef91=NRef91+1
        if(NRef91.gt.nsp) then
          allocate(spo(nsp))
          call CopyVek(sp,spo,nsp)
          deallocate(sp)
          nspo=nsp
          nsp=nsp+10000
          allocate(sp(nsp))
          call CopyVek(spo,sp,nspo)
          deallocate(spo)
        endif
        sp(NRef91)=sinthl
        if(.not.Nulova) then
          rcn=rcn+rs
          rjn=rjn+ri
          NRef91Obs=NRef91Obs+1
        endif
        if(iq.eq.0) iq=1
        ii=mod(iabs(itw),100)
        if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          if(CalcDyn) iq=RunCCD
          nqp=max(nqp,iq)
          if(DiffScales(KDatBlock).eq.1.and..not.CalcDyn) then
            ntwq(ii)=max(ntwq(ii),iq)
            nq=nqp
          else if(DiffScales(KDatBlock).eq.2) then
            nq=1
            ntwq(ii)=1
          endif
        else
          ntwq(ii)=max(ntwq(ii),iq)
          nq=max(nq,iq)
          nqp=nq
        endif
        itwmin=min(itwmin,ii)
        itwmax=max(itwmax,ii)
        nxx=iflg(3)
        if(HKLF5P) then
          do j=1,ntw
            write(91,format91)(ihtw(i,j),i=1,maxNDim),ri,rs,iq,nxx,
     1                         itwa(j),tbar,ReadLam,DirCos,0.,
     2                         1000*no+KRefBlock
          enddo
          ntw=0
        else
          write(91,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,
     1                       tbar,ReadLam,DirCos,0.,
     2                       1000*no+KRefBlock
        endif
        go to 2100
2550    call CloseIfOpened(95)
        ModifiedRefBlock(KRefBlock)=.false.
      enddo
      KRefBlock=KRefBlockUsed
      call FeFlowChartRemove
      NInfo=2
      call FeTabsReset(-1)
      write(t80,100) NReadObs,NRead
      call Zhusti(t80)
      Ven(1)=t80
      j=index(t80,'/')
      Cislo=' '
      if(j.lt.7) Ven(1)=Cislo(:7-j)//Ven(1)(:idel(Ven(1)))
      xpom=FeTxLength(t80(:j))
      call FeTabsAdd(xpom,0,IdCharTab,'/')
      xpom=FeTxLength(t80(:idel(t80))//'X')
      call FeTabsAdd(xpom,0,IdRightTab,' ')
      t80=Tabulator//t80(:idel(t80))//Tabulator
      TextInfo(1)=t80(:idel(t80))//' reflections read from input file'
      Ven(1)=Ven(1)(:idel(Ven(1)))//' reflections read from input file'
      write(t80,100) NRef91Obs,NRef91
      call Zhusti(t80)
      Ven(2)=t80
      j=index(t80,'/')
      Cislo=' '
      if(j.lt.7) Ven(2)=Cislo(:7-j)//Ven(2)(:idel(Ven(2)))
      t80=Tabulator//t80(:idel(t80))//Tabulator
      TextInfo(2)=t80(:idel(t80))//' reflections written to output file'
      Ven(2)=Ven(2)(:idel(Ven(2)))//
     1       ' reflections written to output file'
      if(NExt(2).gt.0) then
        write(t80,100) NExtObs(2),NExt(2)
        call Zhusti(t80)
        NInfo=NInfo+1
        Ven(NInfo)=t80
        j=index(t80,'/')
        Cislo=' '
        if(j.lt.10)
     1    Ven(NInfo)=Cislo(:7-j)//Ven(NInfo)(:idel(Ven(NInfo)))
        t80=Tabulator//t80(:idel(t80))//Tabulator
        TextInfo(NInfo)=t80(:idel(t80))//
     1    ' reflections gave unacceptable indices'
        Ven(NInfo)=Ven(NInfo)(:idel(Ven(NInfo)))//
     1    ' reflections gave unacceptable indices'
      endif
      NInfo=NInfo+1
      if(NExt(1).gt.0) then
        write(t80,100) NExtObs(1),NExt(1)
        call Zhusti(t80)
        Ven(NInfo)=t80
        j=index(t80,'/')
        Cislo=' '
        if(j.lt.7)
     1    Ven(NInfo)=Cislo(:7-j)//Ven(NInfo)(:idel(Ven(NInfo)))
        t80=Tabulator//t80(:idel(t80))//Tabulator
        TextInfo(NInfo)=t80(:idel(t80))//
     1    ' reflections rejected as systematically extinct'
        Ven(NInfo)=Ven(NInfo)(:idel(Ven(NInfo)))//
     1    ' reflections rejected as systematically extinct'
      else
        TextInfo(NInfo)='There were no rejections due to systematic '//
     1                  'extinctions'
        Ven(NInfo)='There were no rejections due to systematic '//
     1             'extinctions'
      endif
      call FeInfoOut(-1.,-1.,'Import statistics - obs/all','L')
      call FeTabsReset(-1)
      if(NRef91.le.0) then
        call FeChybne(-1.,-1.,'All reflections were discarded.',' ',
     1                Warning)
        ich=-1
        go to 9900
      endif
      call CrlGetReflectionConditions
      if(NExtRefCond.gt.0) then
        call Newln(2)
        write(lst,'(''The following reflection conditions will be '',
     1              ''applied''/)')
        do i=1,NExtRefCond
          call Newln(1)
          k=ExtRefPor(i)
          call CrlMakeExtString(ExtRefGroup(1,k),ExtRefCond(1,k),t80,
     1                          p80)
          if(ExtRefFlag(k).ne.0) then
            Veta=' <= induced from above conditions'
          else
            Veta=' '
          endif
          write(lst,FormA1) ' ',(t80(j:j),j=1,idel(t80)),' ',':',' ',
     1                          (p80(j:j),j=1,idel(p80)),
     2                          (Veta(j:j),j=1,idel(Veta))
        enddo
      else
        call Newln(2)
        write(lst,'(''No reflection conditions will be applied''/)')
      endif
      if(.not.MatRealEqUnitMat(TrMP,maxNDim,.0001)) then
        call Newln(maxNDim+3)
        write(lst,'(/''The following transformation will be applied '',
     1               ''to the indices from the data collection''/)')
        do i=1,maxNDim
          write(lst,'(1x,6f8.4)')(trmp(i+(j-1)*maxNDim),
     1                                    j=1,maxNDim)
        enddo
      endif
      call Newln(3)
      write(lst,'(/''Reflections with I<'',f5.2,''*sig(I) will be '',
     1             ''classified as unobserved''/)') EM9ObsLim(KDatBlock)
      if(abs(EM9Sc-1.).gt..5) then
        call newln(1)
        if(EM9Sc.gt.1.) then
          write(Cislo,'(f15.0)') EM9Sc
        else
          write(Cislo,'(f15.5)') EM9Sc
        endif
        call ZdrcniCisla(Cislo,1)
        write(lst,'(''Intesities from M95 were rescaled by the factor ''
     1              ,a)') Cislo(:idel(Cislo))
        call newln(1)
        write(lst,FormA)
      endif
      do i=1,NInfo
        call newln(1)
        write(lst,FormA) Ven(i)(:idel(Ven(i)))
      enddo
      call Newln(maxNDim+1)
      write(lst,FormA)
      Ninfo=maxNDim+3
      write(lst,105)(indices(i),ihmin(i),indices(i),ihmax(i),
     1                                           i=1,maxNDim)
      rn=rcn/rjn*100.
      rc=rcc/rjc*100.
      call Newln(2)
      write(lst,FormA)
      write(lst,104) rn,rc
      write(Cislo,FormI15) NRef91
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(93,10),Cislo(:idel(Cislo))
      if(LamUsed.gt.0.) then
        do i=1,2
          if(i.eq.1) then
            pom=asin(min(SinThLMin*LamUsed,.999999))/Torad
          else
            ThMax=asin(min(SinThLMax*LamUsed,.999999))/Torad
            pom=thmax
          endif
          write(Cislo,'(f8.2)') pom
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(98-i,10),Cislo
        enddo
      else
        ThMax=SinThLMax
      endif
      m=88
      do i=1,2*NDim(KPhase)
        j=(i-1)/2+1
        k=mod(i-1,2)+1
        if(mod(i,2).eq.1) then
          l=ihmin(j)
        else
          l=ihmax(j)
        endif
        write(Cislo,FormI15) l
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(m,10),Cislo(:idel(Cislo))
        if(i.eq.6) m=140
        if(mod(i,2).eq.1) then
          m=m-1
        else
          m=m+3
        endif
      enddo
      call EM9OutputOfRejected(2)
      call EM9OutputOfRejected(1)
      call HeapR(NRef91,sp,1)
      sinmez(8)=sp(NRef91)+.000001
      d=0.
      dd=float(NRef91)*.125
      do i=1,7
        d=d+dd
        j=d
        sinmez(i)=(sp(j)+sp(j+1))*.5
      enddo
      call CloseIfOpened(91)
      FileNameExp=fln(:ifln)//'.l91'
c      if(nq.gt.18) then
c        DataAve(KDatBlock)=0
c        FileNameAve=FileNameExp
c        go to 6000
c      endif
      nrefav=0
      nrefsc=NRef91*(nq+2)
      do i=itwmin,itwmax
        nrefav=nrefav+2*ntwq(i)*NRef91
      enddo
      if(nq.le.1.or.
     1   DifCode(KRefBlock).eq.IdVivaldi.or.
     2   DifCode(KRefBlock).eq.IdSXD.or.
     3   DifCode(KRefBlock).eq.IdTopaz.or.
     4   DifCode(KRefBlock).eq.IdSCDLANL.or.
     5   DifCode(KRefBlock).eq.IdSENJU) go to 5000
      do i=itwmin,itwmax
        ntwqo(i)=ntwq(i)
      enddo
4000  call EM9MakeScales(nq,0,ich)
      if(ich.gt.0) then
        go to 1050
      else if(ich.lt.0) then
        go to 9900
      endif
      if(EM9ScaleLim(KDatBlock).lt.0.) then
        FileNameExp=fln(:ifln)//'.l91'
        NRef90(KDatBlock)=NRef91
        nrefav=0
        do i=itwmin,itwmax
          ntwq(i)=ntwqo(i)
          nrefav=nrefav+2*ntwq(i)*NRef91
        enddo
        go to 5000
      else if(EM9ScaleLim(KDatBlock).eq.0.) then
        call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        FileNameExp=fln(:ifln)//'.l92'
        call OpenFile(92,FileNameExp,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
4100    read(91,format91,end=4200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar
        if(nxx.ne.0) go to 4100
        iq=1
        write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar
        go to 4100
4200    call CloseIfOpened(91)
        call CloseIfOpened(92)
        go to 5000
      endif
      t80=fln(:ifln)//'.l92'
      call CloseListing
      FileNameLst=fln(:ifln)//'_em9_3_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(3)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call OpenFile(92,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nrefsc)*.005),10),
     1                     nrefsc,
     2                     'Transformation to common scale',' ',
     3                     ' ')
      call EM9MakeScales(1,1,ich)
      if(ich.ne.0) go to 4500
      do j=2,nq
        call EM9MakeScales(j,2,ich)
        if(ich.ne.0) then
          call CloseIfOpened(91)
          call CloseIfOpened(92)
          call FeFlowChartRemove
          go to 4000
        endif
      enddo
      sckor(1)=1.
      sckors(1)=0.
      sccom(1)=0
      sccom1(1)=0
      scpairs(1)=0
      sckorm=0.
      do i=1,nq
        sckorm=max(sckorm,sckor(i))
      enddo
      do i=1,nq
        sckor (i)=sckor (i)/sckorm
        sckors(i)=sckors(i)/sckorm
      enddo
      call EM9MakeScales(1,3,ich)
4500  call FeFlowChartRemove
      call CloseIfOpened(91)
      call CloseIfOpened(92)
      if(ich.eq.1) then
        go to 5000
      else if(ich.eq.2) then
        go to 8000
      endif
      Ninfo=nq+3
      TextInfo(1)='           Reference set      Actual set'
      TextInfo(2)='         #common/#gt-ones  #common/#gt-ones    '//
     1            '#used pairs    Scale'
      TextInfo(3)='Set'
      do j=1,nq
        write(p80,106) sccom1(j),scref(1)
        call Zhusti(p80)
        if(15-idel(p80).gt.0) then
          n=index(p80,'/')-8
          if(n.gt.0) then
            p80=p80(n:)
          else if(n.lt.0) then
            do i=1,-n
              p80=' '//p80
            enddo
          endif
        endif
        write(t80,106) sccom(j),scref(j)
        call Zhusti(t80)
        if(15-idel(t80).gt.0) then
          n=index(t80,'/')-8
          if(n.gt.0) then
            t80=t80(n:)
          else if(n.lt.0) then
            do ii=1,-n
              t80=' '//t80
            enddo
          endif
        endif
        write(TextInfo(j+3),'(i2,7x,a15,3x,a15,3x,i10,5x,f8.4)') j,
     1    p80(:15),t80(:15),scpairs(j),sckor(j)
        if(j.gt.1) then
          write(t80,'(''('',i15,'')'')') nint(sckors(j)*10000.)
          call zhusti(t80)
          TextInfo(j+3)=TextInfo(j+3)(:idel(TextInfo(j+3)))//
     1                  t80(:idel(t80))
        else
          TextInfo(j+3)(10:25)='    ---/---    '
          TextInfo(j+3)(28:42)='    ---/---    '
          TextInfo(j+3)(46:55)='       ---'
        endif
      enddo
      t80='Summary from transformation to the common scale'
      call TitulekVRamecku(t80)
      call NewLn(Ninfo)
      do i=1,NInfo
        write(lst,FormA) TextInfo(i)(:idel(TextInfo(i)))
      enddo
      TextInfo(1)='              Reference set             Actual set'
      TextInfo(2)='         #common/#gt-ones  #common/#gt-ones    '//
     1            '#used pairs    Scale'
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd( 10.,UseTabs,IdLeftTab,' ')
      call FeTabsAdd( 75.,UseTabs,IdCharTab,'/')
      call FeTabsAdd(175.,UseTabs,IdCharTab,'/')
      call FeTabsAdd(275.,UseTabs,IdLeftTab,' ')
      call FeTabsAdd(310.,UseTabs,IdCharTab,'.')
      do j=1,nq
        if(j.eq.1) then
          p80='---/---'
          t80='---/---'
          Cislo='---'
        else
          write(p80,106) sccom1(j),scref(1)
          call Zhusti(p80)
          write(t80,106) sccom(j),scref(j)
          call Zhusti(t80)
          write(Cislo,'(i10)') scpairs(j)
        endif
        write(TextInfo(j+3),'(a1,i2,a1,a,a1,a,a1,a,a1,f8.4)')
     1    Tabulator,j,Tabulator,p80(:idel(p80)),Tabulator,
     2    t80(:idel(t80)),Tabulator,Cislo(:idel(Cislo)),Tabulator,
     3    sckor(j)
        if(j.gt.1) then
          write(t80,'(''('',i15,'')'')') nint(sckors(j)*10000.)
          call zhusti(t80)
          TextInfo(j+3)=TextInfo(j+3)(:idel(TextInfo(j+3)))//
     1                  t80(:idel(t80))
        endif
      enddo
      t80='Summary from transformation to the common scale'
      call FeInfoOut(-1.,-1.,t80,'L')
      do i=itwmin,itwmax
        ntwqo(i)=ntwq(i)
        ntwq (i)=1
      enddo
      nrefav=2*(itwmax-itwmin+1)*NRef91
      FileNameExp=fln(:ifln)//'.l92'
      iqmax=1
5000  call CloseListing
      if(DifCode(KRefBlock).eq.IdPets.and.CalcDyn) then
        DataAve(KDatBlock)=0
        FileNameAve=FileNameExp
        go to 6000
      endif
      FileNameLst=fln(:ifln)//'_em9_4_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      LstOpened=.true.
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      call NewPg(1)
      Veta=LabelsEditm9(4)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call EM9Average(0,0,0,ich)
      if(ich.gt.0) then
        call CloseListing
        call DeleteFile(FileNameLst)
        if(nq.gt.1) then
          go to 4000
        else
          go to 1050
        endif
      else if(ich.lt.0) then
        go to 9900
      endif
      call EM9AverageStatReset
      if(DataAve(KDatBlock).le.0) then
        FileNameAve=FileNameExp
        go to 6000
      endif
      t80=fln(:ifln)//'.l93'
      call OpenFile(91,FileNameExp,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call OpenFile(92,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      NRef92=0
      Prumer=.false.
      call SetRealArrayTo(rnum ,11,0.)
      call SetRealArrayTo(rden ,11,0.)
      call SetRealArrayTo(ronum,11,0.)
      call SetRealArrayTo(roden,11,0.)
      call SetRealArrayTo(rexpnum ,11,0.)
      call SetRealArrayTo(rexpden ,11,0.)
      call SetRealArrayTo(roexpnum,11,0.)
      call SetRealArrayTo(roexpden,11,0.)
      call SetIntArrayTo(nrefi ,11,0)
      call SetIntArrayTo(norefi,11,0)
      call SetIntArrayTo(nrefa ,11,0)
      call SetIntArrayTo(norefa,11,0)
      mmMax=1
      call TitulekVRamecku('Report from averaging reflections')
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nrefav)*.005),10),
     1  nrefav,'Averaging reflections from reflection file',' ',' ')
      do i=itwmin,itwmax
        if(i.eq.0) cycle
        do j=1,ntwq(i)
          if(line.gt.40) call NewPg(0)
          if(itwmax.gt.itwmin.or.ntwq(i).gt.1) then
            call newln(3)
            if(itwmax.gt.itwmin) then
              if(ntwq(i).le.1) then
                write(lst,'(/''Averaging from '',i2,a2,
     1                       '' domain fraction''/)') i,nty(i)
              else
                write(lst,'(/''Averaging of '',i2,a2,
     1                       '' domain fraction, scale #'',i2/)')
     2            i,nty(i),j
              endif
            else
              write(lst,'(/''Averaging date with scale #'',i2/)') j
            endif
          endif
          call EM9Average(i,j,1,ich)
          if(ich.ne.0) go to 9100
        enddo
      enddo
      call FeFlowChartRemove
      if(HKLF5P) then
        rewind 91
5100    read(91,format91,end=5150)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar,(pom,i=1,8),NoRefBlock
        if(itw.lt.0) then
          if(ntw.eq.0) nxxm=0
          nxxm=max(nxxm,nxx)
          ntw=ntw+1
          write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar,
     1                      (pom,i=1,8),NoRefBlock
          go to 5100
        else if(ntw.gt.0) then
          nxxm=max(nxxm,nxx)
          if(nxxm.gt.0) then
            do i=1,ntw
              backspace 92
            enddo
          else
            write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar,
     1                        (pom,i=1,8),NoRefBlock
          endif
          ntw=0
          go to 5100
        endif
        go to 5100
      endif
5150  close(91)
      call NewPg(0)
      call TitulekVRamecku('Summary after averaging')
      i=maxNDim+6
      if(ncull.gt.0) i=i+1
      call Newln(i)
      do i=1,maxNDim
        ihmin(i)= 9999
        ihmax(i)=-9999
      enddo
      rcc=0.
      rjc=0.
      rcn=0.
      rjn=0.
      NRef92=0
      NRef92Obs=0
      rewind 92
5200  read(92,format91,end=5300)(ih(k),k=1,maxNDim),ri,rs,iq,nxx,
     1                           itw,tbar
      if(HKLF5P.and.itw.lt.0) go to 5200
      KPhase=KPhaseTwin(mod(itw,100))
      if(KPhase.eq.1) then
        do i=1,6
          ihmax(i)=max(ihmax(i),ih(i))
          ihmin(i)=min(ihmin(i),ih(i))
        enddo
      endif
      do i=1,3
        h(i)=ih(i)
        do j=1,NDimI(KPhase)
          h(i)=h(i)+qu(i,j,1,KPhase)*float(ih(j+3))
        enddo
      enddo
      if(HKLF5P) then
        KPhase=KPhaseTwin(mod(itw,100))
        hp(1:3)=h(1:3)
      else
        call multm(h,rtwi(1,mod(iatw,100)),hp,1,3,3)
        KPhase=KPhaseTwin(1)
      endif
!      KPhase=KPhaseTwin(1)
      call FromIndSinthl(ih,hp,sinthl,sinthlq,1,1)
      rcc=rcc+rs
      rjc=rjc+ri
      NRef92=NRef92+1
      if(NRef92.gt.nsp) then
        allocate(spo(nsp))
        call CopyVek(sp,spo,nsp)
        deallocate(sp)
        nspo=nsp
        nsp=nsp+10000
        allocate(sp(nsp))
        call CopyVek(spo,sp,nspo)
        deallocate(spo)
      endif
      sp(NRef92)=sinthl
      if(anint(ri*10.).gt.EM9ObsLim(KDatBlock)*anint(rs*10.)) then
        rcn=rcn+rs
        rjn=rjn+ri
        NRef92Obs=NRef92Obs+1
      endif
      go to 5200
5300  Ninfo=maxNDim+4
      if(ncull.ge.0) Ninfo=Ninfo+1
      if(prumer) then
        rave=rnum(1)/rden(1)
        if(roden(1).gt.0.) then
          write(t80,'(f6.2,''/'',f6.2)') ronum(1)/roden(1)*100.,
     1                                   rave*100.
        else
          if(rden(1).gt.0.) then
            write(t80,'(''-----/'',f6.2)') rnum(1)/rden(1)*100.
          else
            write(t80,'(''-----/-----'')')
          endif
        endif
      else
        write(t80,'(''-----/-----'')')
      endif
      call Zhusti(t80)
      TextInfo(1)='Rint(obs/all) = '//t80(:idel(t80))
      write(t80,100) NRef92Obs,NRef92
      call Zhusti(t80)
      j=idel(TextInfo(1))
      TextInfo(1)=TextInfo(1)(:j)//' for '//t80(:idel(t80))//
     1            ' reflections'
      write(t80,100) NRef91Obs,NRef91
      call Zhusti(t80)
      TextInfo(2)=' '
      TextInfo(2)=TextInfo(2)(:j-9)//'averaged from '//t80(:idel(t80))
     1          //' reflections'
      if(NRef92.gt.0) then
        write(t80,'(f10.3)') float(NRef91)/float(NRef92)
      else
        t80='0.0'
      endif
      call ZdrcniCisla(t80,1)
      TextInfo(3)='Redundancy = '//t80(:idel(t80))
      do 5315k=1,3
        write(lst,FormA) TextInfo(k)(:idel(TextInfo(k)))
        if(k.le.2) then
          kk=0
          n=0
5310      kp=kk
          call kus(TextInfo(k),kk,Cislo)
          ic=index(Cislo,'/')
          if(ic.gt.0) then
            n=n+1
            if((k.eq.1.and.n.eq.3).or.
     1         (k.eq.2.and.n.eq.1)) then
              if(k.eq.1) then
                xpom=FeTxLengthSpace(TextInfo(k)(:kp+ic))
                tpom=FeTxLength(TextInfo(k)(:kp))
              endif
              TextInfo(k)=TextInfo(k)(:kp)//Tabulator//
     1                    TextInfo(k)(kp+1:)
              if(k.eq.2) TextInfo(k)=Tabulator//
     1                               TextInfo(k)(:idel(TextInfo(k)))
              go to 5315
            endif
          endif
          if(kk.lt.len(TextInfo(k))) go to 5310
        endif
5315  continue
      call FeTabsReset(-1)
      call FeTabsAdd(xpom,1,IdCharTab,'/')
      call FeTabsAdd(tpom,2,IdLeftTab,' ')
      call FeTabsAdd(xpom,2,IdCharTab,'/')
      write(lst,FormA)
      do i=1,maxNDim
        write(TextInfo(i+3),105) indices(i),ihmin(i),
     1                           indices(i),ihmax(i)
        write(lst,FormA) TextInfo(i+3)(:idel(TextInfo(i+3)))
        p80=' '
        t80=TextInfo(i+3)
        k=0
5320    call kus(t80,k,Cislo)
        xpom=FeTxLength('XXXXXXXXXXX')
        if(idel(p80).le.0) then
          p80=Tabulator//Cislo
        else
          p80=p80(:idel(p80))//Tabulator//Cislo
        endif
        if(k.lt.len(t80)) go to 5320
        TextInfo(i+3)=p80
        call FeTabsAdd(xpom,i+3,IdRightTab,' ')
        xpom=xpom+FeTxLengthSpace('h(min) ')
        call FeTabsAdd(xpom,i+3,IdRightTab,' ')
        xpom=xpom+FeTxLengthSpace('XXXXXX')
        call FeTabsAdd(xpom,i+3,IdLeftTab,' ')
        xpom=xpom+FeTxLengthSpace('X')
        call FeTabsAdd(xpom,i+3,IdRightTab,' ')
        xpom=xpom+FeTxLengthSpace('h(max) ')
        call FeTabsAdd(xpom,i+3,IdRightTab,' ')
        xpom=xpom+FeTxLengthSpace('XXXXXX')
        call FeTabsAdd(xpom,i+3,IdLeftTab,' ')
      enddo
      rn=rcn/rjn*100.
      rc=rcc/rjc*100.
      write(lst,FormA)
      i=maxNDim+4
      write(TextInfo(i),104) rn,rc
      write(lst,FormA ) TextInfo(i)(:idel(TextInfo(i)))
      if(ncull.ge.0) then
        if(ncull.gt.0) then
          write(t80,FormI15) ncull
          call zhusti(t80)
          t80=t80(:idel(t80))//' reflections were culled'
        else
          t80='no reflection was culled'
        endif
        i=i+1
        TextInfo(i)='Information from culling : '//t80(:idel(t80))
        write(lst,FormA) TextInfo(i)(:idel(TextInfo(i)))
      endif
      call FeInfoOut(-1.,-1.,'Summary after averaging','L')
      if(maxNDim.gt.3) then
        call TitulekVRamecku('Rint and other characteritics as a '//
     1                       'function of satellite index')
        call Newln(11)
        if(maxNDim.eq.4) then
          Ven(1)='Satelite index'
        else
          Ven(1)='Satelite indices'
        endif
        l=30
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          if(i.eq.2) then
            Veta='('
          else
            Veta='+-('
          endif
          do j=1,NDimI(KPhase)
            write(Cislo,FormI15) HSatGroups(j,i-2)
            call zhusti(Cislo)
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
          enddo
          j=idel(Veta)
          Veta(j:j)=')'
          Ven(1)(l-j+1:l)=Veta(:j)
          l=l+11
        enddo
        write(lst,FormA) Ven(1)(:idel(Ven(1)))
        write(lst,FormA)
        Ven(1)='Obs  Rexp'
        Ven(2)='     Rint'
        Ven(3)='     Number'
        Ven(4)='     Averaged from'
        l=20
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          write(Ven(1)(l:),'(f11.2)') roexpnum(i)/roexpden(i)*100.
          if(roden(i).gt.0.) then
            write(Ven(2)(l:),'(f11.2)') ronum(i)/roden(i)*100.
          else
            Ven(2)(l:)='        ----'
          endif
          write(Ven(3)(l:),'(i11)') norefa(i)
          write(Ven(4)(l:),'(i11)') norefi(i)
          l=l+11
        enddo
        do i=1,4
          write(lst,FormA) Ven(i)(:idel(Ven(i)))
        enddo
        write(lst,FormA)
        Ven(1)='All  Rexp'
        l=20
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          write(Ven(1)(l:),'(f11.2)') rexpnum(i)/rexpden(i)*100.
          if(rden(i).gt.0.) then
            write(Ven(2)(l:),'(f11.2)') rnum(i)/rden(i)*100.
          else
            Ven(2)(l:)='        ----'
          endif
          write(Ven(3)(l:),'(i11)') nrefa(i)
          write(Ven(4)(l:),'(i11)') nrefi(i)
          l=l+11
        enddo
        do i=1,4
          write(lst,FormA ) Ven(i)(:idel(Ven(i)))
        enddo
      endif
      call EM9AverageStatOutput
      if(NRef92.gt.7) then
        call HeapR(NRef92,sp,1)
        sinmez(8)=sp(NRef92)+.000001
        d=0.
        dd=float(NRef92)*.125
        do i=1,7
          d=d+dd
          j=d
          sinmez(i)=(sp(j)+sp(j+1))*.5
        enddo
      endif
      do i=85,86
        if(i.eq.85) then
          if(Prumer) then
            pom=rave
          else
            pom=0.
          endif
          ii=85
        else
          ii=130
          pom=rc*.01
        endif
        write(Cislo,'(f8.4)') pom
        call ZdrcniCisla(Cislo,1)
        write(LnSum,FormCIF) CifKey(ii,10),Cislo
      enddo
      FileNameAve=fln(:ifln)//'.l93'
      call CloseIfOpened(91)
      call CloseIfOpened(92)
      NRef90(KDatBlock)=NRef92
      go to 7000
6000  NRef90(KDatBlock)=NRef91
      write(Cislo,'(f8.4)') rc*.01
      call ZdrcniCisla(Cislo,1)
      write(LnSum,FormCIF) CifKey(130,10),Cislo(:idel(Cislo))
      call CloseIfOpened(91)
7000  do i=1,6
        ihmax(i)=max(iabs(ihmin(i)),iabs(ihmax(i)))
      enddo
8000  call CloseListing
      FileNameLst=fln(:ifln)//'_em9_5_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(5)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      if(.not.SilentRun) then
        id=NextQuestId()
        if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1     TitleSingleCrystal(:idel(TitleSingleCrystal)))
        Veta='Accept the new DatBlock and calculate coverage'
        xpom=5.
        tpom=xpom+CrwgXd+10.
        il=0
        do i=1,3
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,1)
          if(i.eq.1) then
            nCrwFirst=CrwLastMade
            Veta='Accept the new DatBlock'
          else if(i.eq.2) then
            Veta='Discard the new DatBlock'
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        enddo
        call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
8100    call FeQuestEVent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 8100
        endif
        AcceptM91=0
        if(ich.eq.0) then
          nCrw=nCrwFirst
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              AcceptM91=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
      else
        AcceptM91=1
      endif
      if(AcceptM91.eq.0) then
        if(.not.SilentRun)
     1    call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
        if(nq.gt.6.or.HKLF5P) then
          go to 1050
        else
          go to 5000
        endif
      else if(AcceptM91.le.2) then
        write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        DatBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        call MoveFile(FileNameAve,DatBlockFileName)
        if(AcceptM91.eq.1) call EM9CheckCompleteness(ThMax,LamUsed,mlim)
        M91Changed=.true.
        if(PerLimit.lt.101.) then
          write(Cislo,'(f8.2)') ThFull
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(95,10),Cislo
          write(Cislo,'(f8.2)') PerLimitMax*.01
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(6,10),Cislo
          write(Cislo,'(f8.2)') PerLimit*.01
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(5,10),Cislo
        endif
      else
        call DeleteFile(PreviousM50)
      endif
      if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1   DifCode(KRefBlock).eq.IdSXD.or.
     2   DifCode(KRefBlock).eq.IdTopaz.or.
     3   DifCode(KRefBlock).eq.IdSCDLANL.or.
     4   DifCode(KRefBlock).eq.IdSENJU) then
        if(nq.gt.mxscutw) then
          mxscu=nq
          mxscutw=mxscu+NTwin
        endif
        do i=1,nq
          if(sc(i,KDatBlock).le.0.) sc(i,KDatBlock)=1.
        enddo
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
      call DeleteFile(fln(:ifln)//'.l91')
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      go to 9900
9000  call FeReadError(95)
9100  call FeFlowChartRemove
9900  if(.not.SilentRun) call FeQuestRemove(WizardId)
      call CloseListing
      do i=89,91
        call CloseIfOpened(i)
      enddo
      call CloseIfOpened(95)
      call CloseListing
      if(allocated(sp)) deallocate(sp)
      call DeleteFile(fln(:ifln)//'.l91')
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      call FeTabsReset(-1)
      if(allocated(CIFKey).and..not.AlreadyAllocated)
     1  deallocate(CifKey,CifKeyFlag)
      call CloseIfOpened(LnSum)
      return
100   format(i15,'/'i15)
101   format(6i4)
102   format(1x,3f9.1)
103   format('Exported ',i8,' reflections, ',i8,' observed ones')
104   format('R(obs/all) from e.s.d. of I : ',f5.2,'/',f5.2)
105   format(17x,a1,'(min) = ',i4, ', ',a1,'(max) = ',i4)
106   format(i15,'/',i15)
108   format('Rave:',2f8.4)
      end
      subroutine EM9CreateM90Powder(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*256 Veta
      logical EqIgCase,First
      ich=0
      EM9Sc=1.
      First=.true.
      do 3000i=1,NRefBlock
        if(RefDatCorrespond(i).ne.KDatBlock) go to 3000
        if(First) then
          write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
          if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
          DatBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
          call OpenFile(90,DatBlockFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          NRef90(KDatBlock)=0
          First=.false.
        endif
        call OpenRefBlockM95(95,i,fln(:ifln)//'.m95')
        if(ErrFlag.ne.0) go to 3000
2000    read(95,FormA,end=2500) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 2500
        write(90,FormA) Veta(:idel(Veta))
        NRef90(KDatBlock)=NRef90(KDatBlock)+1
        go to 2000
2500    call CloseIfOpened(95)
        call CloseIfOpened(90)
        ModifiedRefBlock(i)=.false.
3000  continue
9999  return
      end
      subroutine EM9OutputOfRejected(n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*80 t80,p80,ta80(2),Veta
      equivalence (t80,ta80(1)),(p80,ta80(2))
      if(NExt(n).gt.0) then
        if(n.eq.1) then
          t80='Summary of reflections absent due to systematic '//
     1        'extinctions'
        else
          t80='Summary of reflections which could not be transformed '//
     1        'to integer indices'
        endif
        call TitulekVRamecku(t80)
        avis=SumRelI(n)/float(NExt(n))
        write(t80,'(''n(all) :'',i7,'', n(obs) :'',i7)')
     1    NExt(n),NExtObs(n)
        call newln(1)
        write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        NInfo=1
        TextInfo(NInfo)=t80
        write(t80,'(''Average(I/Sig(I)) : '',f5.2)') avis
        call newln(1)
        write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        NInfo=NInfo+1
        TextInfo(NInfo)=t80
        call newln(1)
        write(lst,FormA1)
        NInfo=NInfo+1
        if(NExt500(n).le.0) then
          if(N.eq.1) then
            t80='All absent'
          else
            t80='All rejected'
          endif
          t80=t80(:idel(t80))//' reflections classified as unobserved'
          TextInfo(NInfo)=t80
          call NewLn(1)
          write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        else
          if(NExt500(n).ne.500) call Indexx(NExt500(n),ria(1,n),
     1                                      OrderExtRef(1,n))
          if(Line.gt.50) call NewPg(0)
          if(n.eq.1) then
            p80='List of the strongest absent reflections:'
          else
            p80='List of the strongest rejected reflections:'
          endif
          call NewLn(1)
          write(lst,FormA1)(p80(i:i),i=1,idel(p80))
          if(n.eq.2) then
            Veta='Indices are related to the original cell !!!'
            call NewLn(1)
            write(lst,FormA1)(Veta(i:i),i=1,idel(p80))
          endif
          call NewLn(1)
          write(lst,FormA1)
          TextInfo(NInfo)=p80
          if(n.eq.2) then
            NInfo=NInfo+1
            TextInfo(NInfo)=Veta
          endif
          xpom=FeTxLength('0000')
          call FeTabsReset(0)
          xx=0.
          do i=1,maxNDim
            xx=xx+xpom
            call FeTabsAdd(xx,0,IdLeftTab,' ')
          enddo
          xpom=FeTxLength('0000000.0')
          do i=1,3
            xx=xx+xpom
            call FeTabsAdd(xx,0,IdLeftTab,' ')
          enddo
          write(t80,EM9Form1)(indices(i),i=1,maxNDim)
          t80(idel(t80)+1:)='       I      sig(I)  I/sig(I)'
          idlp=idel(t80)
          call newln(1)
          write(lst,FormA1)(t80(i:i),i=1,idlp),(' ',i=1,10),
     1                     (t80(i:i),i=1,idlp)
          k=0
          p80=' '
          do i=1,maxNDim+3
            call kus(t80,k,Cislo)
            if(i.eq.1) then
              p80=Tabulator//Cislo
            else
              p80=p80(:idel(p80))//Tabulator//Cislo
            endif
          enddo
          NInfo=NInfo+1
          TextInfo(NInfo)=p80
          do i=1,min(18-NInfo,NExt500(n))
            kk=OrderExtRef(i,n)
            write(t80,EM9Form2)(IHExt(j,kk,n),j=1,maxNDim)
            pom1=-float(ria(kk,n))*.0001
            pom2= float(rsa(kk,n))*.01
            idl=idel(t80)+1
            write(t80(idl:),100) pom1*pom2,pom2,pom1
            k=0
            do j=1,maxNDim+3
              call kus(t80,k,Cislo)
              if(j.eq.1) then
                p80=Tabulator//Cislo
              else
                p80=p80(:idel(p80))//Tabulator//Cislo
              endif
            enddo
            Ninfo=Ninfo+1
            TextInfo(NInfo)=p80
          enddo
          m=min(2*(mxline-Line),NExt500(n))
          ks=0
2000      k=ks
          if(mod(m,2).eq.1) m=m+1
          do i=1,m
            ip=mod(i-1,2)+1
            ks=ks+1
            if(ks.gt.NExt500(n)) go to 2300
            if(ip.eq.1) then
              k=k+1
              kk=OrderExtRef(k,n)
            else
              kk=OrderExtRef(k+m/2,n)
            endif
            write(ta80(ip),EM9Form2)(IHExt(j,kk,n),j=1,maxNDim)
            pom1=-float(ria(kk,n))*.0001
            pom2= float(rsa(kk,n))*.01
            write(ta80(ip)(idl:),100) pom1*pom2,pom2,pom1
2300        if(ip.eq.2) then
              call newln(1)
              write(lst,FormA1)(ta80(1)(j:j),j=1,idlp),(' ',j=1,10),
     1                         (ta80(2)(j:j),j=1,idlp)
              ta80(2)=' '
            endif
          enddo
          if(ks.lt.NExt500(n)) then
            m=min(2*(mxline-3),NExt500(n)-ks)
            go to 2000
          endif
        endif
        if(n.eq.1) then
          t80='Summary of systematic extinctions'
        else
          t80='Summary of rejected reflections'
        endif
        call FeInfoOut(-1.,-1.,t80,'L')
      endif
9999  call FeTabsReset(-1)
      return
100   format(1x,3f9.1)
      end
      subroutine EM9Average(KItwIn,KIqIn,KlicIn,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ihpp(6),ihmx(6),ihd(6),itr(6),icull(:),kpor(:),idiff(:),
     1          kpoint(:),hp(3),ihov(6),iporp(:),tp(:),dircosa(:,:,:),
     2          sj(3),soj(3),RIArO(:),RSArO(:)
      character*80 Veta,ShForm
      character*30 LabelMult
      character*25 LabelSlow,LabelFast
      character*23 LabelRef
      character*20 FormRefAve
      character*19 LabelSigma
      character*42 iven(mxl4)
      character*4  iven1
      character*1  Znak
      integer EM9SortIndex,EdwStateQuest,CrlCentroSymm,FromWhere,
     1        UseTabsIn,RunCCDa(:)
      logical CrwLogicQuest,lpom,Psal,lcull,tisk,ExistFile,FeYesNoHeader
      allocatable iporp,tp,icull,kpor,idiff,kpoint,DirCosA,RIArO,RSArO,
     1            RunCCDa
      save mxl34,ncsp,izp,Psal,FormRefAve,flimp,fcullp,ihov,ihov4,jcomm,
     1     RIArO,RSArO,lns
      data iven/mxl4*' '/
      data LabelSlow,LabelFast/'The slowest varying index',
     1                         'The fastest varying index'/
      data LabelRef/'Reflections |I-I(ave)|>'/
      data LabelSigma/'Sigma(I(ave)) from:'/
      data LabelMult/'Multiply measured reflections:'/
      KItw=KItwIn
      KIq=KIqIn
      Klic=KlicIn
      FromWhere=AveFromEditM9
      go to 1100
      entry DRAverage(Key,KItwIn,ich)
      FromWhere=Key
      Klic=0
      KItw=KItwIn
      KIq=1
1100  if(Klic.eq.0) then
        lns=0
        ncsp=2
        do i=1,NPhase
          KPhase=i
          if(CrlCentroSymm().le.0) then
            ncsp=1
            exit
          endif
        enddo
        if(FromWhere.ne.AveFromEditM9) then
          if(FromWhere.eq.AveFromManualCull.or.
     1       FromWhere.eq.AveFromRSViewer.or.
     2       FromWhere.eq.AveFromXShape.or.
     3       FromWhere.eq.AveForSemiEmpir) then
            if(FromWhere.eq.AveFromXShape.or.
     1         FromWhere.eq.AveForSemiEmpir) then
              if(FromWhere.eq.AveFromXShape) then
                Veta='Creating "eqv" file for X-shape'
                NDimUse=3
              else
                Veta='Creating "eqv" file for frame scale and/or '//
     1               'empirical absorption corrections'
                NDimUse=maxNDim
              endif
              n=2*nref95(KRefBlock)
              if(ExistFile(RefBlockFileName)) then
                call OpenFile(95,RefBlockFileName,'formatted','old')
              else
                call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
              endif
              if(ErrFlag.ne.0) go to 9999
              ShForm=FormatShelx
              write(ShForm(2:2),'(i1)') NDimUse
              i=idel(ShForm)
              ShForm(i:)=',i8)'
              RIMax=0.
              nobsa=0
            else
              Veta='Averaging reflections'
              n=2*NRefRead
            endif
            call FeFlowChartOpen(-1.,-1.,max(nint(float(n)*.01),10),n,
     1                           Veta,' ',' ')
          endif
          call SetRealArrayTo(rnum ,11,0.)
          call SetRealArrayTo(rden ,11,0.)
          call SetRealArrayTo(ronum,11,0.)
          call SetRealArrayTo(roden,11,0.)
          call SetRealArrayTo(rexpnum ,11,0.)
          call SetRealArrayTo(rexpden ,11,0.)
          call SetRealArrayTo(roexpnum,11,0.)
          call SetRealArrayTo(roexpden,11,0.)
          call SetIntArrayTo(nrefi ,11,0)
          call SetIntArrayTo(norefi,11,0)
          call SetIntArrayTo(nrefa ,11,0)
          call SetIntArrayTo(norefa,11,0)
          IndFastest(KDatBlock)=3
          IndSlowest(KDatBlock)=1
          go to 1600
        endif
        if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1     DifCode(KRefBlock).eq.IdSXD.or.
     2     DifCode(KRefBlock).eq.IdTopaz.or.
     3     DifCode(KRefBlock).eq.IdSCDLANL.or.
     4     DifCode(KRefBlock).eq.IdSENJU) then
          DataAve(KDatBlock)=0
          go to 9999
        endif
        if(.not.SilentRun) then
          id=NextQuestId()
          if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1       TitleSingleCrystal(:idel(TitleSingleCrystal)))
          il=0
          Veta='Perform a%veraging'
          xpom=(WizardLength-FeTxLengthUnder(Veta)-CrwgXd-6.)*.5
          tpom=xpom+CrwgXd+6.
          do i=1,2
            il=il-9
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='Use %non-averaged data'
              nCrwAverage=CrwLastMade
            else
              nCrwNoAverage=CrwLastMade
            endif
            if((i.eq.1.and.DataAve(KDatBlock).gt.0).or.
     1         (i.eq.2.and.DataAve(KDatBlock).le.0)) then
              lpom=.true.
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
          enddo
          il=il-9
          call FeQuestLinkaMake(id,il)
          xpom=FeTxLength(LabelSlow)+20.
          pom=xpom
          il=il-9
          do i=1,3
            call FeQuestCrwMake(id,xpom,il,xpom-4.,il-9,indices(i),'C',
     1                          CrwgXd,CrwgYd,1,2)
            if(i.eq.1) then
              nCrwSlowFrom=CrwLastMade
            else if(i.eq.3) then
              nCrwSlowTo=CrwLastMade
            endif
            xpom=xpom+20.
          enddo
          xpom=5.
          il=il-9
          call FeQuestLblMake(id,xpom,il,LabelSlow,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblSlow=LblLastMade
          il=il-9
          call FeQuestLblMake(id,xpom,il,LabelFast,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblFast=LblLastMade
          xpom=pom
          do i=1,3
            call FeQuestCrwMake(id,xpom,il,xpom-4.,il,' ','C',CrwgXd,
     1                          CrwgYd,1,3)
            if(i.eq.1) then
              nCrwFastFrom=CrwLastMade
            else if(i.eq.3) then
              nCrwFastTo=CrwLastMade
            endif
            xpom=xpom+20.
          enddo
          il=il-9
          call FeQuestLinkaMake(id,il)
          nLine1=LinkaLastMade
          il=il-9
          tpom=5.
          Veta='%Full print'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwPrint=CrwLastMade
          Veta='Apply c%ulling'
          tpom=xpom+CrwXd+20.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwCull=CrwLastMade
          Veta='A%dd center of symmetry'
          tpom=xpom+CrwXd+20.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          nCrwCSym=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,AddCentrSymm(KDatBlock).gt.0)
          if(ncsp.ne.1) call FeQuestCrwDisable(CrwLastMade)
          il=il-9
          tpom=5.
          Veta='Apply 1/sig(I) %weights in averaging'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          call FeQuestCrwOpen(CrwLastMade,AveSigWt(KDatBlock).gt.0)
          nCrwSigWt=CrwLastMade
          il=il-9
          xpom=5.
          call FeQuestLblMake(id,xpom,il,LabelRef,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblPrint=LblLastMade
          xpom=FeTxLength(LabelRef)+xpom+5.
          dpom=40.
          tpom=xpom+dpom+5.
          Veta='*sig(I(ave)) will be %printed'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwPrint=EdwLastMade
          il=il-9
          xpomp=5.
          call FeQuestLblMake(id,xpomp,il,LabelRef,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblCull=LblLastMade
          Veta='*sig(I(ave)) will be %culled'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwCull=EdwLastMade
          il=il-9
          call FeQuestLinkaMake(id,il)
          nLine2=LinkaLastMade
          il=il-9
          tpom=5.
          ilb5=il
          call FeQuestLblMake(id,tpom,il,LabelSigma,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblSigma=LblLastMade
          xpom=tpom+60.
          do i=1,3
            il=il-9
            if(i.eq.1) then
              Veta='P%oisson'
            else if(i.eq.2) then
              Veta='%Equivalents'
            else
              Veta='%Maximum'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,4)
            if(i.eq.1) then
              nCrwPoisson=CrwLastMade
            else if(i.eq.2) then
              nCrwEquivalent=CrwLastMade
            else
              nCrwMaximum=CrwLastMade
            endif
          enddo
          if(DifCode(KRefBlock).eq.IdKumaPD) then
            tpomm=tpom+WizardLength*.5
            call FeQuestLblMake(id,tpomm,ilb5,LabelMult,'L','N')
            call FeQuestLblDisable(LblLastMade)
            nLblMult=LblLastMade
            xpom=xpom+WizardLength*.5
            il=ilb5
            do i=1,3
              il=il-9
              if(i.eq.1) then
                Veta='use %all'
              else if(i.eq.2) then
                Veta='use f%irst'
              else
                Veta='use %last'
              endif
              call FeQuestCrwMake(id,tpomm,il,xpom,il,Veta,'L',CrwgXd,
     1                            CrwgYd,0,5)
              if(i.eq.1) then
                nCrwUseAll=CrwLastMade
              else if(i.eq.2) then
                nCrwUseFirst=CrwLastMade
              else
                nCrwUseLast=CrwLastMade
              endif
            enddo
          endif
          fcullp=20.
          flimp=5.
1400      xpom=5.
          if(CrwLogicQuest(nCrwAverage)) then
            nCrw=nCrwSlowFrom
            do i=1,3
              call FeQuestCrwOpen(nCrw,i.eq.IndSlowest(KDatBlock))
              nCrw=nCrw+1
            enddo
            call FeQuestLblOn(nLblSlow)
            call FeQuestLblOn(nLblFast)
            nCrw=nCrwFastFrom
            do i=1,3
              if(i.ne.IndSlowest(KDatBlock))
     1          call FeQuestCrwOpen(nCrw,i.eq.IndFastest(KDatBlock))
              nCrw=nCrw+1
            enddo
            call FeQuestCrwOpen(nCrwPrint,FLimPrint(KDatBlock).lt.0.)
            call FeQuestCrwOpen(nCrwCull,FLimCull(KDatBlock).gt.0.)
            if(ncsp.eq.1)
     1        call FeQuestCrwOpen(nCrwCSym,AddCentrSymm(KDatBlock).gt.0)
            if(FLimPrint(KDatBlock).ge.0.) then
              call FeQuestRealEdwOpen(nEdwPrint,FLimPrint(KDatBlock),
     1                                .false.,.false.)
              call FeQuestLblOn(nLblPrint)
            endif
            if(FLimCull(KDatBlock).ge.0.) then
              call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                                .false.,.false.)
              call FeQuestLblOn(nLblCull)
            endif
            call FeQuestLblOn(nLblSigma)
            nCrw=nCrwPoisson
            do i=1,3
              call FeQuestCrwOpen(nCrw,i.eq.SigIMethod(KDatBlock))
              nCrw=nCrw+1
            enddo
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              call FeQuestLblOn(nLblMult)
              nCrw=nCrwUseAll
              do i=1,3
                call FeQuestCrwOpen(nCrw,i.eq.MultAve(KDatBlock))
                nCrw=nCrw+1
              enddo
            endif
            call FeQuestCrwOpen(nCrwSigWt,AveSigWt(KDatBlock).gt.0)
          else
            nCrw=nCrwSlowFrom
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            call FeQuestLblDisable(nLblSlow)
            call FeQuestLblDisable(nLblFast)
            nCrw=nCrwFastFrom
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            call FeQuestCrwDisable(nCrwPrint)
            call FeQuestCrwDisable(nCrwCull)
            if(ncsp.eq.1) call FeQuestCrwDisable(nCrwCSym)
            call FeQuestLblDisable(nLblPrint)
            call FeQuestEdwDisable(nEdwPrint)
            call FeQuestLblDisable(nLblCull)
            call FeQuestEdwDisable(nEdwCull)
            call FeQuestLblDisable(nLblSigma)
            call FeQuestLblDisable(nCrwSigWt)
            nCrw=nCrwPoisson
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              call FeQuestLblDisable(nLblMult)
              nCrw=nCrwUseAll
              do i=1,3
                call FeQuestCrwDisable(nCrw)
                nCrw=nCrw+1
              enddo
            endif
          endif
1500      call FeQuestEvent(id,ich)
          if(CheckType.eq.EventCrw) then
            if(CheckNumber.eq.nCrwAverage.or.
     1         CheckNumber.eq.nCrwNoAverage) then
              go to 1400
            else if(CheckNumber.ge.nCrwSlowFrom.and.
     1              CheckNumber.le.nCrwSlowTo)  then
              izn=CheckNumber-nCrwSlowFrom+1
              if(IndSlowest(KDatBlock).ne.izn) then
                call FeQuestCrwDisable(izn+nCrwFastFrom-1)
                call FeQuestCrwOpen(
     1            IndSlowest(KDatBlock)-1+nCrwFastFrom,
     2            IndFastest(KDatBlock).eq.izn)
                if(IndFastest(KDatBlock).eq.izn)
     1            IndFastest(KDatBlock)=IndSlowest(KDatBlock)
                IndSlowest(KDatBlock)=izn
              endif
            else if(CheckNumber.ge.nCrwFastFrom.and.
     1              CheckNumber.le.nCrwFastTo)  then
              IndFastest(KDatBlock)=CheckNumber-nCrwFastFrom+1
            else if(CheckNumber.eq.nCrwPrint) then
              if(CrwLogicQuest(CheckNumber)) then
                if(EdwStateQuest(nEdwPrint).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdwPrint,flimp)
                  call FeQuestEdwDisable(nEdwPrint)
                endif
                call FeQuestLblDisable(nLblPrint)
                FLimPrint(KDatBlock)=-1.
              else
                FLimPrint(KDatBlock)=flimp
                call FeQuestLblOn(nLblPrint)
                call FeQuestRealEdwOpen(nEdwPrint,FLimPrint(KDatBlock),
     1                                  .false.,.false.)
              endif
            else if(CheckNumber.eq.nCrwCull) then
              if(CrwLogicQuest(CheckNumber)) then
                FLimCull(KDatBlock)=fcullp
                call FeQuestLblOn(nLblCull)
                call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                                  .false.,.false.)
              else
                if(EdwStateQuest(nEdwCull).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdwCull,fcullp)
                  call FeQuestEdwDisable(nEdwCull)
                endif
                call FeQuestLblDisable(nLblCull)
                FLimCull(KDatBlock)=-1.
              endif
            endif
            go to 1500
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1500
          endif
          if(ich.eq.0) then
            if(CrwLogicQuest(nCrwAverage)) then
              DataAve(KDatBlock)=1
            else
              DataAve(KDatBlock)=0
              go to 9999
            endif
            if(CrwLogicQuest(nCrwPrint)) then
              FLimPrint(KDatBlock)=-1.
            else
              call FeQuestRealFromEdw(nEdwPrint,FLimPrint(KDatBlock))
            endif
            if(CrwLogicQuest(nCrwCull)) then
              call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
            else
              FLimCull(KDatBlock)=-1.
            endif
            if(CrwLogicQuest(nCrwSigWt)) then
              AveSigWt(KDatBlock)=1
            else
              AveSigWt(KDatBlock)=0
            endif
            if(CrwLogicQuest(nCrwPoisson)) then
              SigIMethod(KDatBlock)=1
            else if(CrwLogicQuest(nCrwEquivalent)) then
              SigIMethod(KDatBlock)=2
            else if(CrwLogicQuest(nCrwMaximum)) then
              SigIMethod(KDatBlock)=3
            endif
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              if(CrwLogicQuest(nCrwUseAll)) then
                MultAve(KDatBlock)=1
              else if(CrwLogicQuest(nCrwUseFirst)) then
                MultAve(KDatBlock)=2
              else if(CrwLogicQuest(nCrwUseLast)) then
                MultAve(KDatBlock)=3
              endif
            endif
            if(ncsp.eq.1) then
              if(CrwLogicQuest(nCrwCSym)) then
                AddCentrSymm(KDatBlock)=1
                ncsp=2
              else
                AddCentrSymm(KDatBlock)=0
              endif
            endif
          endif
        endif
        if(ich.ne.0) go to 9999
1600    if(FromWhere.eq.AveFromRSViewer.or.
     1     FromWhere.eq.AveFromXShape.or.
     2     FromWhere.eq.AveForSemiEmpir) then
          ncull=0
          jcomm=1
          if(FromWhere.eq.AveFromRSViewer) then
            call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
          else
            call OpenFile(90,fln(:ifln)//'.eqv','formatted','unknown')
          endif
          go to 2500
        endif
        if(maxNDim.gt.3) then
          mxl34=3*(mxline-5)
        else
          mxl34=4*(mxline-5)
        endif
        FormRefAve='(3i4,f 9.1,f 7.1,a4)'
        if(maxNDim.gt.3) then
          n=20-2*maxNDim
          write(FormRefAve(2:2),102) maxNDim
          write(FormRefAve(7:8),106) n
          write(FormRefAve(13:14),106) n-2
          FormLabAve='(3(''   h   k   l'
          if(maxNDim.gt.3)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   m'
          if(maxNDim.gt.4)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   n'
          if(maxNDim.gt.5)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   p'
          Veta=''',  x,''I '',  x,''sig(I)'',3x)/)'
          write(Veta(3:4),106) n-2
          write(Veta(12:13),106) n-7
          FormLabAve=FormLabAve(:idel(FormLabAve))//Veta(:idel(Veta))
        else
          FormLabAve='(4(''   h   k   l      I    sig(I)   '')/)'
        endif
        npom=2
        do i=1,NPhase
          KPhase=i
          if(CrlCentroSymm().le.0) then
            npom=1
            go to 2120
          endif
        enddo
2120    if(AddCentrSymm(KDatBlock).gt.0.and.ncsp.eq.1) then
          call NewLn(2)
          write(lst,'(''Center of symmetry will be applied even if '',
     1                ''the space group is acentric''/)')
        endif
        if(AveSigWt(KDatBlock).gt.0) then
          call NewLn(2)
          write(lst,'(''Weighting by 1/sig(I) will be used in '',
     1                ''averaging''/)')
        endif
        call NewLn(2)
        if(FLimPrint(KDatBlock).ne.0.) then
          if(FLimPrint(KDatBlock).gt.0.) then
            write(lst,'(''Only reflections |I-I(ave)|>'',f5.1,
     1                  ''*sig(I(ave)) will be printed'')')
     2        FLimPrint(KDatBlock)
            write(lst,FormA1)
          else
            write(lst,'(''Full print averaged reflections'')')
            write(lst,FormA1)
          endif
          call newln(4)
          write(lst,'(
     1      ''Symbol +    means that for the relevant reflection '',
     2      '' 3*sig(I(ave)) < I(i)-I(ave) <  5*sig(I(ave))''/
     3      ''Symbol ++               -- " --                    '',
     4      '' 5*sig(I(ave)) < I(i)-I(ave) < 10*sig(I(ave))''/
     5      ''Symbol +++              -- " --                    '',
     6      ''10*sig(I(ave)) < I(i)-I(ave) < 20*sig(I(ave))''/
     7      ''Symbol ++++             -- " --                    '',
     8      ''20*sig(I(ave)) < I(i)-I(ave)'')')
          call newln(5)
          write(lst,'(
     1      ''Symbol -                -- " --                    '',
     2      '' 3*sig(I(ave)) < I(ave)-I(i) <  5*sig(I(ave))''/
     3      ''Symbol --               -- " --                    '',
     4      '' 5*sig(I(ave)) < I(ave)-I(i) < 10*sig(I(ave))''/
     5      ''Symbol ---              -- " --                    '',
     6      ''10*sig(I(ave)) < I(ave)-I(i) < 20*sig(I(ave))''/
     7      ''Symbol ----             -- " --                    '',
     8      ''20*sig(I(ave)) < I(ave)-I(i)'')')
          write(lst,FormA1)
        else
          write(lst,'(''Print averaged reflections suppressed'')')
          write(lst,FormA1)
        endif
        if(FLimCull(KDatBlock).gt.0.) then
          call NewLn(2)
          write(lst,'(''Reflections |I-I(ave)|>'',f5.1,
     1                ''*sig(I(ave)) will be culled'')')
     2      FLimCull(KDatBlock)
          write(lst,FormA1)
        endif
        if(FLimCull(KDatBlock).gt.0.) then
          ncull=0
        else
          ncull=-1
        endif
        if(NDimI(KPhase).eq.1.and.KCommen(KPhase).ne.0) then
          pom=NCommQProduct(1,KPhase)
          ihov4=NCommQProduct(1,KPhase)
          do j=1,3
            ihov(j)=nint(qu(j,1,1,KPhase)*pom)
          enddo
!          jcomm=2
          jcomm=1
        else
          jcomm=1
        endif
2500    itr(3)=IndFastest(KDatBlock)
        itr(1)=IndSlowest(KDatBlock)
        itr(2)=6-itr(1)-itr(3)
        do i=4,maxNDim
          itr(i)=i
        enddo
        izp=0
        if(FromWhere.eq.AveFromEditM9) then
          if(allocated(RIAr)) then
            NRefReadO=NRefRead
            allocate(RIArO(NRefReadO),RSArO(NRefReadO))
            call CopyVek(RIAr,RIArO,NRefReadO)
            call CopyVek(RSAr,RSArO,NRefReadO)
            deallocate(RIAr,RSAr)
          endif
          lns=NextLogicNumber()
          call OpenFile(lns,fln(:ifln)//'.m89','formatted','unknown')
          go to 9999
        endif
      endif
      n=0
      nn=0
      call SetIntArrayTo(ihmax,6,0)
      call SetIntArrayTo(ihmx,6,0)
      if(FromWhere.eq.AveFromEditM9) rewind 91
      KPhase=1
      ntw=0
3000  n=n+1
3030  izpet=0
      nn=nn+1
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromSGTest.or.
     2   FromWhere.eq.AveFromRSViewer) then
        if(nn.gt.NRefRead) go to 3200
        itw=0
        do i=1,NTwin
          if(HCondAr(i,nn).gt.0) then
            itw=i
            exit
          endif
        enddo
        if(itw.eq.0) go to 3030
        call IndUnPack(HCondAr(itw,nn),ihp,HCondLn,HCondMx,NDim(KPhase))
        if(FromWhere.eq.AveFromSGTest) then
          call indtr(ihp,TrMP,ih,NDim(KPhase))
        else
          call CopyVekI(ihp,ih,NDim(KPhase))
        endif
!        itw=1
        iq=kiq
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call DRGetReflectionFromM95(95,iend,ichp)
        if(ichp.ne.0) go to 9000
        if(iend.ne.0) go to 3200
        if(no.le.0.or.iflg(1).lt.0.or.iflg(2).ne.1) then
          izpet=1
          go to 3040
        endif
        if(FromWhere.eq.AveFromXShape) then
          do i=4,NDim(KPhase)
            if(ih(i).ne.0) then
              izpet=1
              go to 3040
            endif
          enddo
        endif
        ri=ri*corrf(1)
        RIMax=max(RIMax,ri)
        itw=1
        iq=kiq
      else
        read(91,format91,end=3200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar
        if(nxx.gt.1) nxx=0
      endif
3040  if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) then
        call FeFlowChartEvent(izp,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9000
        endif
        if(itw.lt.0) then
          ntw=ntw+1
          go to 3030
        else if(ntw.gt.0) then
          ntw=0
          go to 3030
        endif
        if(iq.ne.kiq.or.itw.ne.KItw.or.izpet.ne.0) go to 3030
      endif
      if(FromWhere.ne.AveFromEditM9.and.FromWhere.ne.AveFromXShape.and.
     1   FromWhere.ne.AveForSemiEmpir) then
        RIAr(n)=RIAr(nn)
        RSAr(n)=RSAr(nn)
        ICullAr(n)=ICullAr(nn)
        RefBlockAr(n)=RefBlockAr(nn)
        RunCCDAr(n)=RunCCDAr(nn)
      endif
      do i=1,maxNDim
        ihmax(i)=max(ihmax(i),iabs(ih(i)))
      enddo
      do jj=1,jcomm
        if(jj.eq.2) then
          if(iabs(ih(4)).eq.ihov4/2) then
            izn=isign(1,ih(4))
            do i=1,3
              ih(i)=ih(i)+izn*ihov(i)
            enddo
            ih(4)=ih(4)-izn*ihov4
          else
            cycle
          endif
        endif
        do 3100i=1,NSymmN(KPhase)
          do j=1,ncsp
            if(j.eq.1) then
              ii=i
            else
              ii=-i
            endif
            call GetSymIndRespectTwin(ih,ihp,itr,KItw,ii,ncsp,ichp)
            if(ichp.ne.0) cycle
            if(jcomm.gt.1) then
              if(iabs(ihp(4)).gt.ihov4/2) then
                izn=isign(1,ihp(4))
                do ii=1,3
                  ihp(ii)=ihp(ii)+izn*ihov(itr(ii))
                enddo
                ihp(4)=ihp(4)-izn*ihov4
              endif
            endif
            do k=1,maxNDim
              ihmx(k)=max(ihmx(k),iabs(ihp(k)))
            enddo
          enddo
3100    continue
      enddo
      go to 3000
3200  n=n-1
      if(n.le.0) then
        if(FromWhere.eq.AveFromRSViewer) then
          call CloseIfOpened(91)
        else
          call CloseIfOpened(90)
        endif
        go to 9999
      endif
      allocate(irec(n),irecp(n),ireca(n),ipor(n),iporp(n),tp(n))
      if(FromWhere.eq.AveFromEditM9.or.FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        allocate(RIAr(n),RSAr(n))
        if(FromWhere.eq.AveFromEditM9) then
          allocate(ICullAr(n),RefBlockAr(n))
          rewind 91
        else
          if(ExistFile(RefBlockFileName)) then
            rewind 95
          else
            call CloseIfOpened(95)
            call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
          endif
          allocate(ICullAr(n),RefBlockAr(n),inca(n),dircosa(3,2,n),
     1             RunCCDa(n))
        endif
      endif
      call SetIntArrayTo(ihd,6,1)
      j=1
      do i=1,NDim(KPhase)
        ihd(i)=2*ihmx(i)+1
        if(imax/j.lt.ihd(i)) go to 8000
        j=j*ihd(i)
      enddo
      ntw=0
      n=0
      nn=0
3300  n=n+1
3330  izpet=0
      nn=nn+1
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromSGTest.or.
     2   FromWhere.eq.AveFromRSViewer) then
        if(nn.gt.NRefRead) go to 3500
        itw=0
        do i=1,NTwin
          if(HCondAr(i,nn).gt.0) then
            itw=i
            exit
          endif
        enddo
        if(itw.eq.0) go to 3330
        call IndUnPack(HCondAr(itw,nn),ihp,HCondLn,HCondMx,NDim(KPhase))
        if(FromWhere.eq.AveFromSGTest) then
          call indtr(ihp,TrMP,ih,NDim(KPhase))
        else
          call CopyVekI(ihp,ih,NDim(KPhase))
        endif
!        itw=1
        iq=kiq
        if(ICullAr(n).le.1) then
          nxx=ICullAr(nn)
        else
          ICullAr(nn)=0
          nxx=0
        endif
        tbar=0.
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call DRGetReflectionFromM95(95,iend,ichp)
        if(ichp.ne.0) go to 9000
        if(iend.ne.0) go to 3500
        if(no.le.0.or.iflg(1).lt.0.or.iflg(2).ne.1) then
          izpet=1
          go to 3340
        endif
        if(FromWhere.eq.AveFromXShape) then
          do i=4,NDim(KPhase)
            if(ih(i).ne.0) then
              izpet=1
              go to 3340
            endif
          enddo
        endif
        ri=ri*corrf(1)
        rs=rs*corrf(1)
        itw=1
        iq=kiq
        tbar=0.
      else
        read(91,format91,end=3500)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar,(pom,i=1,8),NoRefBlock
        if(nxx.gt.1) nxx=0
      endif
3340  if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) then
        call FeFlowChartEvent(izp,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9000
        endif
        if(itw.lt.0) then
          ntw=ntw+1
          go to 3330
        else if(ntw.gt.0) then
          ntw=0
          go to 3330
        endif
        if(iq.ne.kiq.or.itw.ne.KItw.or.izpet.ne.0) go to 3330
        if(FromWhere.eq.AveFromEditM9) RefBlockAr(n)=NoRefBlock
      endif
      if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromXShape.or.
     2   FromWhere.eq.AveForSemiEmpir) then
        RIAr(n)=ri
        RSAr(n)=rs
        if(FromWhere.eq.AveFromXShape.or.
     1     FromWhere.eq.AveForSemiEmpir) then
          nxx=0
          do j=1,2
            do i=1,3
              dircosa(i,j,n)=dircos(i,j)*rcp(i,1,KPhase)
            enddo
            call multm(MetTens(1,1,KPhase),dircosa(1,j,n),sj,3,3,1)
            pom=sqrt(scalmul(dircosa(1,j,n),sj))
            if(pom.gt.0.) then
              pom=1./pom
            else
              pom=1.
            endif
            do i=1,3
              dircosa(i,j,n)=dircosa(i,j,n)*pom
            enddo
          enddo
          RunCCDa(n)=RunCCD
        endif
      endif
      tp(n)=tbar
      do i=1,NDim(KPhase)
        ihp(i)=ih(itr(i))
      enddo
      mx=IndPack(ihp,ihd,ihmx,maxNDim)
      irec(n)=mx
      ICullAr(n)=nxx
      mx =0
      mxx=0
      kk=0
      do k=1,3-ncsp
        if(k.eq.2) call IntVectorToOpposite(ih,ih,maxNDim)
        do jj=1,jcomm
          if(jj.eq.2) then
            if(iabs(ih(4)).eq.ihov4/2) then
              izn=isign(1,ih(4))
              do i=1,3
                ih(i)=ih(i)+izn*ihov(i)
              enddo
              ih(4)=ih(4)-izn*ihov4
            else
              cycle
            endif
          endif
          do i=1,NSymmN(KPhase)
            do j=1,ncsp
              if(j.eq.1) then
                ii=i
              else
                ii=-i
               endif
              call GetSymIndRespectTwin(ih,ihp,itr,KItw,ii,ncsp,ichp)
              if(ichp.ne.0) cycle
              if(jcomm.gt.1) then
                if(iabs(ihp(4)).gt.ihov4/2) then
                  izn=isign(1,ihp(4))
                  do ii=1,3
                    ihp(ii)=ihp(ii)+izn*ihov(itr(ii))
                  enddo
                  ihp(4)=ihp(4)-izn*ihov4
                endif
              endif
              mxq=IndPack(ihp,ihd,ihmx,maxNDim)
              if(k.eq.1) then
                if(mxq.gt.mxx) then
                  mxx=mxq
                  kk=k
                endif
                if(mxq.gt.mx ) mx =mxq
              else
                if(mxq.gt.mxx) then
                  mxx=mxq
                  kk=k
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      irecp(n)=mx
      ireca(n)=2*mxx+(kk-1)
      go to 3300
3500  n=n-1
      if(FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        pom=99999./RIMax
        if(pom.lt.1.) then
          do i=1,n
            riar(i)=riar(i)*pom
            rsar(i)=rsar(i)*pom
          enddo
          RIMax=RIMax*pom
        endif
        RIMax=RIMax*.01
      endif
      call indexx(n,ireca,ipor)
      if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromSGTest) then
        lines=line
        if(maxNDim.eq.3) then
          mxln=mxl34-(line-3)*4
        else
          mxln=mxl34-(line-3)*3
        endif
        Psal=.false.
        line=0
      endif
      kk=0
      NCull=0
      if(FromWhere.ne.AveFromEditM9) then
        NAve=0
        NTotallyCulled=0
      endif
      mxg=0
      incmax=0
      nuni=0
4000  lcull=.false.
      kp=kk+1
      if(kp.gt.n) go to 5000
      kn=0
      j=ipor(kp)
      irecpHledany=irecp(j)
      call IndUnPack(irecpHledany,ihpp,ihd,ihmx,maxNDim)
      do i=1,maxNDim
        ihp(itr(i))=ihpp(i)
      enddo
4050  kk=kk+1
      if(kk.gt.n) go to 5000
      if(irecp(ipor(kk)).eq.irecpHledany) then
        kn=kn+1
        if(kk.lt.n) go to 4050
      else
        kk=kk-1
      endif
      if(kn.gt.mxg) then
        if(allocated(KPor)) deallocate(KPor,ICull,KPoint,IDiff)
        allocate(KPor(kn),ICull(kn),KPoint(kn),IDiff(kn))
        mxg=kn
      endif
      call SetIntArrayTo(ICull,kn,0)
      do i=1,kn
        KPoint(i)=IPor(kp+i-1)
        ICull(i)=ICullAr(KPoint(i))
        if(ICull(i).gt.0) NCull=NCull+1
      enddo
      if(MultAve(KDatBlock).gt.1) then
        do i=1,kn-1
          ki=KPoint(i)
          if(ki.le.0) cycle
          do j=i+1,kn
            kj=KPoint(j)
            if(kj.le.0) cycle
            if(irec(ki).eq.irec(kj)) then
              if(MultAve(KDatBlock).eq.2) then
                KPoint(j)=-kj
              else
                KPoint(i)=-ki
                exit
              endif
            endif
          enddo
        enddo
      endif
4100  s1=0.
      s2=0.
      s4=0.
      s5=0.
      npr=0
      DiffMax=0.
      jprv=0
      do i=1,kn
        j=KPoint(i)
        if(j.le.0) cycle
        if(s2.le.0.) jprv=j
        if(icull(i).ne.0) cycle
        if(AveSigWt(KDatBlock).gt.0) then
          if(RSAr(j).gt.0.) then
            wt=1./RSAr(j)
          else if(RIAr(j).gt.0.) then
            wt=1./sqrt(RIAr(j))
          else
            wt=0.
          endif
        else
          wt=1.
        endif
        s1=s1+wt*RIAr(j)
        s2=s2+wt
        s4=s4+(wt*RSAr(j))**2
        s5=s5+wt*tp(j)
        npr=npr+1
      enddo
      if(npr.gt.1) then
        s1=s1/s2
        s4=sqrt(s4)/s2
        s5=s5/s2
        prumer=.true.
        s2=0.
        s3=0.
        do i=1,kn
          j=KPoint(i)
          if(j.gt.0) then
            if(AveSigWt(KDatBlock).gt.0) then
              if(RSAr(j).gt.0.) then
                wt=1./RSAr(j)
              else if(RIAr(j).gt.0.) then
                wt=1./sqrt(RIAr(j))
              else
                wt=0.
              endif
            else
              wt=1.
            endif
            pom=RIAr(j)-s1
            Diff=abs(pom)
            IDiff(i)=nint(pom*1000.)
            if(icull(i).eq.0) then
              s2=s2+wt
              s3=s3+wt*Diff**2
            endif
          endif
        enddo
        call indexx(kn,IDiff,kpor)
        pom=sqrt(s3/(s2*float(npr-1)))
        if(lns.gt.0) write(lns,'(3i4,3f9.1)')(ihp(i),i=1,3),s1,s4,pom
        if(SigIMethod(KDatBlock).eq.1) then
          s3=s4
        else if(SigIMethod(KDatBlock).eq.2) then
          s4=s4*.5
          if(pom.lt.s4) then
            s3=s4
          else
            s3=pom
          endif
        else
          s3=max(pom,s4)
        endif
        if(s3.le.0.) then
          if(s1.gt.0.) then
            s3=FLimPrint(KDatBlock)*s1
          else
            s3=.1
          endif
          s4=s3
        endif
        tisk=.false.
        do i=1,kn
          if(icull(i).ne.0) cycle
          j=KPoint(i)
          if(j.gt.0) then
            Diff=abs(RIAr(j)-s1)
            tisk=tisk.or.(Diff.gt.FLimPrint(KDatBlock)*s3)
            Diff=Diff/s3
c            tisk=tisk.or.(Diff.gt.FLimPrint(KDatBlock)*RSAr(j))
c            Diff=Diff/RSAr(j)
            if(Diff.gt.DiffMax) then
              DiffMax=Diff
              kmax=i
            endif
          else
            Tisk=.true.
          endif
        enddo
        if(FLimCull(KDatBlock).gt.0..and.
     1     DiffMax.gt.FLimCull(KDatBlock).and.npr.gt.2) then
          icull(kmax)=2
          if(FromWhere.eq.AveFromManualCull.or.
     1       FromWhere.eq.AveFromSGTest.or.
     2       FromWhere.eq.AveFromEditM9) ICullAr(KPoint(kmax))=2
          lcull=.true.
          ncull=ncull+1
          go to 4100
        endif
        if((FromWhere.eq.AveFromEditM9.or.
     1      FromWhere.eq.AveFromManualCull.or.
     2      FromWhere.eq.AveFromSGTest).and.
     3     (tisk.or.lcull.or.FLimPrint(KDatBlock).lt.0.)) then
          line=line+1
          write(iven(line),FormRefAve)(ihp(k),k=1,maxNDim),s1,s3,
     1                                '    '
          do k=1,4
            if(iven(line)(k:k).eq.' ') iven(line)(k:k)='>'
          enddo
          if(line.eq.mxln) then
            call EM9OutSta(iven)
            call newpg(0)
            line=0
            Psal=.true.
            mxln=mxl34
          endif
          do i=1,kn
            j=KPoint(kpor(i))
            ja=iabs(j)
            pom=abs(RIAr(ja)-s1)/s3
            if(RIAr(ja).gt.s1) then
              Znak='+'
            else
              Znak='-'
            endif
            if(j.le.0) then
              iven1='Skip'
            else if(icull(kpor(i)).ne.0) then
              iven1='Cull'
            else
              iven1=' '
              if(pom.gt. 3.) iven1=Znak
              if(pom.gt. 5.) iven1=iven1(:idel(iven1))//Znak
              if(pom.gt.10.) iven1=iven1(:idel(iven1))//Znak
              if(pom.gt.20.) iven1=iven1(:idel(iven1))//Znak
            endif
            l=irec(ja)
            if(l.le.0) cycle
            call IndUnPack(l,ihpp,ihd,ihmx,maxNDim)
            do j=1,maxNDim
              ih(itr(j))=ihpp(j)
            enddo
            line=line+1
            write(iven(line),FormRefAve)(ih(l),l=1,maxNDim),
     1                                   RIAr(ja),RSAr(ja),iven1
            if(line.eq.mxln) then
              Psal=.true.
              call EM9OutSta(iven)
              call newpg(0)
              line=0
              mxln=mxl34
            endif
          enddo
          line=line+1
          iven(line)=' '
          if(line.eq.mxln) then
            Psal=.true.
            call EM9OutSta(iven)
            call newpg(0)
            line=0
            mxln=mxl34
          endif
        else if(FromWhere.eq.AveFromXShape.or.
     1          FromWhere.eq.AveForSemiEmpir) then
          do i=1,kn
            if(icull(kpor(i)).ne.0) cycle
            j=KPoint(kpor(i))
            ja=iabs(j)
            if(FromWhere.eq.AveFromXShape) then
              if(CutMaxXShape.gt.0..and.
     1           riar(ja).gt.CutMaxXShape*rimax) go to 4000
              if(CutMinXShape.gt.0..and.
     1           riar(ja).lt.CutMinXShape*rsar(ja)) go to 4000
            else
              if(CutMaxAve(KRefBlock).gt.0..and.
     1           riar(ja).gt.CutMaxAve(KRefBlock)*rimax) go to 4000
              if(CutMinAve(KRefBlock).gt.0..and.
     1           riar(ja).lt.CutMinAve(KRefBlock)*rsar(ja)) go to 4000
            endif
          enddo
          inc=0
          nuni=nuni+1
          do 4070i=1,kn
            if(icull(kpor(i)).ne.0) cycle
            j=KPoint(kpor(i))
            ja=iabs(j)
            if(FromWhere.eq.AveForSemiEmpir) then
              call IndUnPack(irec(ja),ihpp,ihd,ihmx,maxNDim)
              do k=1,maxNDim
                ihp(itr(k))=ihpp(k)
              enddo
            endif
            call multm(MetTens(1,1,KPhase),dircosa(1,1,ja),soj,3,3,1)
            call multm(MetTens(1,1,KPhase),dircosa(1,2,ja),sj ,3,3,1)
            do k=1,i-1
              if(icull(kpor(k)).ne.0) cycle
              m=KPoint(kpor(k))
              ma=iabs(m)
              if(scalmul(dircosa(1,1,ma),soj).gt..999.and.
     1           scalmul(dircosa(1,2,ma), sj).gt..999)
     2          go to 4070
            enddo
            write(90,ShForm)(ihp(l),l=1,NDimUse),riar(ja),rsar(ja),1,
     1                      (-dircosa(l,1,ja)/rcp(l,1,KPhase),
     2                        dircosa(l,2,ja)/rcp(l,1,KPhase),l=1,3),
     3                       RunCCDa(ja)
            nobsa=nobsa+1
            inc=inc+1
4070      continue
          if(inc.eq.1) then
            nobsa=nobsa-1
            nuni=nuni-1
            backspace 90
          else
            inca(nuni)=inc
            incmax=max(inc,incmax)
          endif
          go to 4000
        endif
      else if(npr.gt.0) then
       if(FromWhere.eq.AveFromXShape.or.
     1    FromWhere.eq.AveForSemiEmpir) go to 4000
        s1=RIAr(jprv)
        s3=RSAr(jprv)
        s5=tp(jprv)
        kpor(1)=1
      else
       if(FromWhere.eq.AveFromXShape.or.
     1    FromWhere.eq.AveForSemiEmpir) go to 4000
        s1=0.
        s3=1.
        kpor(1)=1
c          NTotallyCulled=NTotallyCulled+1
c          do i=1,kn
c            j=KPoint(i)
c            if(j.le.0) cycle
c            Ave2Org(i,NAve)=j
c          enddo
c          go to 4200
      endif
      mm=EM9SortIndex(ihp)
      call FromIndSinthl(ihp,hp,sinthl,sinthlq,1,0)
      if(FromWhere.ne.AveFromEditM9) then
        if(NAve+1.gt.NAveMax) call ReallocateDRAve(1000,0)
        if(kn.gt.NAveRedMax) call ReallocateDRAve(0,max(10,kn))
        NAve=NAve+1
        if(npr.lt.1) NTotallyCulled=NTotallyCulled+1
        do i=1,kn
          j=KPoint(i)
          if(j.le.0) cycle
          Ave2Org(i,NAve)=j
        enddo
        HCondAveAr(NAve)=IndPack(ihp,HCondLn,HCondMx,NDim(KPhase))
        RIAveAr(NAve)=s1
        RSAveAr(NAve)=s3
        NAveRedAr(NAve)=kn
        NAveRed=max(NAveRed,kn)
        DiffAveAr(NAve)=DiffMax
      endif
      if(npr.gt.1) then
        do i=1,kn
          j=KPoint(i)
          if(j.le.0) cycle
          if(icull(i).eq.0) then
            pom=RIAr(j)
            apom=abs(pom-s1)
            rnum(1)=rnum(1)+apom
            rden(1)=rden(1)+pom
            if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.))
     1        then
              ronum(1)=ronum(1)+apom
              roden(1)=roden(1)+pom
            endif
            if(maxNDim.gt.3) then
              mmp=min(mm,10)
              mmMax=max(mmp,mmMax)
              rnum(mmp)=rnum(mmp)+apom
              rden(mmp)=rden(mmp)+pom
              nrefi(mmp)=nrefi(mmp)+1
              if(i.eq.1) nrefa(mmp)=nrefa(mmp)+1
              if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.))
     1          then
                ronum(mmp)=ronum(mmp)+apom
                roden(mmp)=roden(mmp)+pom
                norefi(mmp)=norefi(mmp)+1
                if(i.eq.1) norefa(mmp)=norefa(mmp)+1
              endif
            endif
            call EM9AverageStatRInt(sinthl,mmp,s1,s3,apom,pom,i)
          endif
          if(FromWhere.ne.AveFromEditM9) Ave2Org(i,NAve)=j
        enddo
      else if(npr.gt.0) then
        if(maxNDim.gt.3) then
          mmp=min(mm,10)
          mmMax=max(mmp,mmMax)
          nrefa(mmp)=nrefa(mmp)+1
          nrefi(mmp)=nrefi(mmp)+1
          if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
            norefa(mmp)=norefa(mmp)+1
            norefi(mmp)=norefi(mmp)+1
          endif
          call EM9AverageStatRInt(sinthl,mmp,s1,s3,-1.,-1.,1)
        endif
      else
        go to 4000
      endif
      if(maxNDim.gt.3) then
        rexpnum(mmp)=rexpnum(mmp)+s3
        rexpden(mmp)=rexpden(mmp)+s1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          roexpnum(mmp)=roexpnum(mmp)+s3
          roexpden(mmp)=roexpden(mmp)+s1
        endif
      endif
      call EM9AverageStatrexp(sinthl,mmp,s1,s3)
      if(FromWhere.eq.AveFromEditM9) then
        write(92,format91)(ihp(k),k=1,maxNDim),s1,s3,kiq,0,KItw,s5
      else if(FromWhere.eq.AveFromRSViewer) then
        write(91,Format83e)(ihp(l),l=1,NDim(KPhase)),s1*SimScale,
     1                                               s1*SimScale,
     2                                               s3*SimScale
      endif
      go to 4000
5000  if(FromWhere.eq.AveFromRSViewer) then
        call CloseIfOpened(91)
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call CloseIfOpened(95)
        if(FromWhere.eq.AveFromXShape) then
          npom=NRefMaxXShape
        else
          npom=NRefMaxAve(KRefBlock)
        endif
        if(npom.gt.0.and.nuni.gt.npom) then
          ln=NextLogicNumber()
          open(ln,file=fln(:ifln)//'.l90')
          inc=incmax
          n=0
5050      i=0
5100      i=i+1
          if(inca(i).eq.inc) then
            inca(i)=-inca(i)
            n=n+1
            if(n.eq.npom) go to 5200
          endif
          if(i.ge.nuni) then
            inc=inc-1
            go to 5050
          else
            go to 5100
          endif
5200      rewind 90
5300      nobsa=0
          do i=1,nuni
            iz=inca(i)
            do j=1,iabs(iz)
              read(90,ShForm,end=5500)(ih(l),l=1,NDimUse),riar(j),
     1                                 rsar(j),m,dircos,RunCCD
              if(iz.lt.0) then
                write(ln,ShForm)(ih(l),l=1,NDimUse),riar(j),rsar(j),m,
     1                           dircos,RunCCD
                nobsa=nobsa+1
              endif
            enddo
          enddo
          nuni=npom
5500      close(90)
          close(ln)
          call MoveFile(fln(:ifln)//'.l90',fln(:ifln)//'.eqv')
        endif
        call CloseIfOpened(90)
        if(NUni.lt.10) then
          NInfo=1
          TextInfo(1)='The number of unique reflections smaller then '//
     1                '10.'
          if(FeYesNoHeader(-1.,YBottomMessage,'Do you want to repeat '//
     1                     'the selection procedure?',0)) then
            go to 9990
          else
            go to 9000
          endif
        else
          if(CovTest.gt.0.and.FromWhere.eq.AveForSemiEmpir) then
            if(allocated(ScBrat)) deallocate(ScBrat)
            n=0
            do i=1,NRuns(KRefBlock)
              n=n+RunNFrames(i,KRefBlock)
            enddo
            RunScN(KRefBlock)=(n-1)/RunScGr(KRefBlock)+1
            allocate(ScBrat(RunScN(KRefBlock),KRefBlock))
            ScBrat=.false.
            call DRTestFrScCoverage(n,m)
            if(n.gt.0) then
              NInfo=2
              TextInfo(1)='The number of frames for one scale does '//
     1                    'not allow refining of scale factors.'
              TextInfo(2)='Please enlarge this parametr and try again.'
              call FeInfoOut(-1.,-1.,'WARNING','L')
              go to 9990
            else if(CovTest.eq.1) then
              NInfo=1
              TextInfo(1)='The number of frames for one scale allows '//
     1                    'refining scale of factors.'
              call FeInfoOut(-1.,-1.,'INFORMATION','L')
              go to 9990
            endif
          else
            NUniXShape=NUni
            NAllXShape=NObsA
!            UseTabsIn=UseTabs
!            UseTabs=NextTabs()
!            call FeTabsAdd(150.,UseTabs,IdRightTab,' ')
!            id=NextQuestId()
!            xqd=250.
!            il=3
!            call FeQuestCreate(id,-1.,YBottomMessage,xqd,il,' ',0,
!     1                         LightGray,-1,-1)
!            il=1
!            write(Cislo,FormI15) NUni
!            call Zhusti(Cislo)
!            Veta='Number of unique reflections:'//Tabulator//
!     1           Cislo(:idel(Cislo))
!            tpom=5.
!            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
!            il=il+1
!            write(Cislo,FormI15) NObsA
!            call Zhusti(Cislo)
!            Veta='Total number of reflections:'//Tabulator//
!     1           Cislo(:idel(Cislo))
!            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
!            il=il+1
!            Veta='%Repeat selection'
!            dpom=FeTxLengthUnder(Veta)+20.
!            tpom=xqd*.5-dpom-10.
!            do i=1,2
!              call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
!              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
!              if(i.eq.1) then
!                tpom=xqd*.5+10.
!                Veta='%Continue'
!                nButtRepeat=ButtonLastMade
!              else
!                nButtContinue=ButtonLastMade
!              endif
!            enddo
!5600        call FeQuestEvent(id,ich)
!            if(CheckType.eq.EventButton) then
!              if(CheckNumber.eq.nButtRepeat) then
!                call FeQuestRemove(id)
!                call FeTabsReset(UseTabs)
!                go to 9990
!              endif
!            else if(CheckType.ne.0) then
!              call NebylOsetren
!              go to 5600
!            endif
!            call FeQuestRemove(id)
!            call FeTabsReset(UseTabs)
!            UseTabs=UseTabsIn
          endif
        endif
      else
        if(line.gt.0) then
          i=line
          call EM9OutSta(iven)
          if(maxNDim.eq.3) then
            if(mxln.eq.mxl34) then
              line=(i-1)/4+6
            else
              line=lines+(i-1)/4+3
            endif
          else
            if(mxln.eq.mxl34) then
              line=(i-1)/3+6
            else
              line=lines+(i-1)/3+3
            endif
          endif
        else
          if(Psal) then
            line=3
          else
            line=lines
          endif
        endif
      endif
      if(FromWhere.eq.AveFromEditM9.and..not.SilentRun.and.KITw.eq.1)
     1  then
        MaxRefBlockAr=0
        NRefBlockAr=0
        do IRefBlock=1,NRefBlock
          if(RefDatCorrespond(IRefBlock).eq.1) then
            MaxRefBlockAr=max(MaxRefBlockAr,IRefBlock)
            NRefBlockAr=NRefBlockAr+1
          endif
        enddo
        call EM9ManualCullUpdate(1)
      endif
      if(lns.gt.0) then
        Suma1=0.
        Suma2=0.
        rewind lns
        n=0
5700    read(lns,100,end=5750)(i,j=1,3),pom,pomc,poms
        n=n+1
        Suma1=Suma1+pom**4
        Suma2=Suma2+(poms**2-pomc**2)*pom**2
        go to 5700
5750    if(n.gt.10) then
          pom=Suma2/Suma1
          call newln(2)
          if(pom.ge.0.) then
            write(lst,'(/'' Refined instability factor : '',f8.5)')
     1        sqrt(pom)*.5
            else if(pom.lt.0.) then
              write(lst,'(/'' Instability has negative value'')')
            endif
        else
          write(lst,'(/'' Instability factor could not be refined, ''
     1                 '' there too few or no redundacy'')')
        endif
        call CloseIfOpened(lns)
        call DeleteFile(fln(:ifln)//'.m89')
        lns=0
      endif
      go to 9999
8000  call FeChybne(-1.,-1.,'diffraction indices are too large to be '//
     1              'sorted and averaged.',' ',SeriousError)
9000  ich=1
      ErrFlag=0
      go to 9999
9990  ich=-1
9999  if(allocated(irec)) deallocate(irec,irecp,ireca,ipor,iporp,tp)
      if(allocated(KPor)) deallocate(KPor,ICull,KPoint,IDiff)
      if(FromWhere.eq.AveFromEditM9.or.FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        if(allocated(RIAr)) deallocate(RIAr,RSAr)
        if(allocated(inca)) deallocate(inca,dircosa,RunCCDa)
        if(allocated(ICullAr)) deallocate(ICullAr,RefBlockAr)
        if(allocated(RIArO).and.Klic.ne.0) then
          allocate(RIAr(NRefReadO),RSAr(NRefReadO))
          call CopyVek(RIArO,RIAr,NRefReadO)
          call CopyVek(RSArO,RSAr,NRefReadO)
          deallocate(RIArO,RSArO)
          NRefRead=NRefReadO
        endif
      endif
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromRSViewer.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) call FeFlowChartRemove
      return
100   format(3i4,3f9.1)
102   format(i1)
106   format(i2)
      end
      subroutine EM9AverageStatReset
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension rnums(8,11,2),rdens(8,11,2),
     1          rexpdens(8,11,2),rexpnums(8,11,2),
     2          nrefis(8,11,2),nrefas(8,11,2)
      character*128 Ven
      save rnums,rdens,rexpdens,rexpnums,nrefis,nrefas
      call SetRealArrayTo(rnums,176,0.)
      call SetRealArrayTo(rdens,176,0.)
      call SetRealArrayTo(rexpnums,176,0.)
      call SetRealArrayTo(rexpdens,176,0.)
      call SetIntArrayTo(nrefis,176,0)
      call SetIntArrayTo(nrefas,176,0)
      go to 9999
      entry EM9AverageStatRInt(sinthl,mmp,s1,s3,apom,pom,iprv)
      do j=1,8
        if(sinthl.le.sinmez(j)) go to 1200
      enddo
      j=8
1200  if(apom.ge.0.) then
        rnums(j,1,1)=rnums(j,1,1)+apom
        rdens(j,1,1)=rdens(j,1,1)+pom
      endif
      nrefis(j,1,1)=nrefis(j,1,1)+1
      if(iprv.eq.1) nrefas(j,1,1)=nrefas(j,1,1)+1
      if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
        if(apom.ge.0.) then
          rnums(j,1,2)=rnums(j,1,2)+apom
          rdens(j,1,2)=rdens(j,1,2)+pom
        endif
        nrefis(j,1,2)=nrefis(j,1,2)+1
        if(iprv.eq.1) nrefas(j,1,2)=nrefas(j,1,2)+1
      endif
      if(maxNDim.gt.3) then
        if(apom.ge.0.) then
          rnums(j,mmp,1)=rnums(j,mmp,1)+apom
          rdens(j,mmp,1)=rdens(j,mmp,1)+pom
        endif
        nrefis(j,mmp,1)=nrefis(j,mmp,1)+1
        if(iprv.eq.1) nrefas(j,mmp,1)=nrefas(j,mmp,1)+1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          if(apom.ge.0.) then
            rnums(j,mmp,2)=rnums(j,mmp,2)+apom
            rdens(j,mmp,2)=rdens(j,mmp,2)+pom
          endif
          nrefis(j,mmp,2)=nrefis(j,mmp,2)+1
          if(iprv.eq.1) nrefas(j,mmp,2)=nrefas(j,mmp,2)+1
        endif
      endif
      go to 9999
      entry EM9AverageStatrexp(sinthl,mmp,s1,s3)
      do j=1,8
        if(sinthl.le.sinmez(j)) go to 2200
      enddo
      j=8
2200  rexpnums(j,1,1)=rexpnums(j,1,1)+s3
      rexpdens(j,1,1)=rexpdens(j,1,1)+s1
      if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
        rexpnums(j,1,2)=rexpnums(j,1,2)+s3
        rexpdens(j,1,2)=rexpdens(j,1,2)+s1
      endif
      if(maxNDim.gt.3) then
        rexpnums(j,mmp,1)=rexpnums(j,mmp,1)+s3
        rexpdens(j,mmp,1)=rexpdens(j,mmp,1)+s1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          rexpnums(j,mmp,2)=rexpnums(j,mmp,2)+s3
          rexpdens(j,mmp,2)=rexpdens(j,mmp,2)+s1
        endif
      endif
      go to 9999
      entry EM9AverageStatOutput
      call NewPg(0)
      call TitulekVRamecku('Rint and other characteritics as a '//
     1                     'function of sin(th)/lambda')
      do i=1,mmMax
        nn=0
        do j=1,8
          nn=nn+nrefis(j,i,2)
        enddo
        if(nn.le.0) cycle
        if(maxNDim.gt.3) then
          call NewLn(3+2*7)
          if(i.eq.1) then
            Ven='Main reflections + satellites'
          else if(i.eq.2) then
            Ven='Main reflections'
          else
            Ven='Satellites +-('
            do j=1,NDimI(KPhase)
              write(Cislo,FormI15) HSatGroups(j,i-2)
              call zhusti(Cislo)
              Ven=Ven(:idel(Ven))//Cislo(:idel(Cislo))//','
            enddo
            j=idel(Ven)
            Ven(j:j)=')'
          endif
          write(lst,FormA)
          write(lst,FormA) Ven(:idel(Ven))
          write(lst,FormA1)('-',j=1,idel(Ven))
        else
          call NewLn(2*7)
        endif
        do l=1,2
          if(l.eq.1) then
            write(lst,'(/''All reflections'')')
          else
            write(lst,'(/''Observed reflections'')')
          endif
          write(lst,'(''sin(theta)/lambda'',8f10.6)') sinmez
          Ven=' '
          k=1
          do j=1,8
            if(rexpdens(j,i,l).gt.0.) then
              write(Ven(k:),'(f10.2)')
     1          rexpnums(j,i,l)/rexpdens(j,i,l)*100.
            else
              Ven(k:)='    ------'
            endif
            k=k+10
          enddo
          Ven='Rexp             '//Ven(:idel(Ven))
          write(lst,FormA) Ven(:idel(Ven))
          Ven=' '
          k=1
          do j=1,8
            if(rdens(j,i,l).gt.0.) then
              write(Ven(k:),'(f10.2)') rnums(j,i,l)/rdens(j,i,l)*100.
            else
              Ven(k:)='    ------'
            endif
            k=k+10
          enddo
          Ven='Rint             '//Ven(:idel(Ven))
          write(lst,FormA) Ven(:idel(Ven))
          write(lst,'(''Averaged from    '',8i10)')(nrefis(j,i,l),j=1,8)
          write(lst,'(''Number           '',8i10)')(nrefas(j,i,l),j=1,8)
        enddo
      enddo
9999  return
      end
      subroutine EM9OutStA(iven)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*42 iven(*)
      if(line.gt.0) then
        write(lst,FormLabAve)
        if(maxNDim.gt.3) then
          n=(line-1)/3+1
        else
          n=(line-1)/4+1
        endif
        do i=1,n
          if(maxNDim.gt.3) then
            write(lst,'(3a42)')(iven(i+(j-1)*n),j=1,3)
          else
            write(lst,'(4a32)')(iven(i+(j-1)*n)(1:32),j=1,4)
          endif
        enddo
        do i=1,line
          iven(line)=' '
        enddo
        line=mxline
      endif
      return
      end
      subroutine EM9MakeScales(kiqIn,KlicIn,ich)
      use Basic_mod
      use EDZones_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*256 Veta
      character*14 :: Label = 'Reflections I>'
      integer, allocatable :: Used1(:)
      integer EdwStateQuest,ihmx(6),ihd(6)
      logical lpom,CrwLogicQuest
      real, allocatable :: fia(:),sigfia(:)
      save iz,N1Obs,flimp,fia,sigfia
      kiq=kiqIn
      Klic=KlicIn
      FromWhere=AveFromEditM9
      go to 1100
      entry DRMakeScales(kiqIn,KlicIn,ich)
      kiq=kiqIn
      Klic=KlicIn
      FromWhere=AveFromManualCull
      iz=0
1100  ich=0
      if(Klic.eq.0) then
        if(EM9ScaleLim(KDatBlock).le.0.) then
          flimp=10.
        else
          flimp=EM9ScaleLim(KDatBlock)
        endif
        if(.not.SilentRun) then
          id=NextQuestId()
          if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1       TitleSingleCrystal(:idel(TitleSingleCrystal)))
          il=0
          Veta='%determine a unique scale'
          xpom=5.
          tpom=xpom+CrwgXd+10.
          do i=1,3
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='Use %multi-scale data'
              nCrwUniqueScale=CrwLastMade
            else if(i.eq.2) then
              Veta='%Ignore scale flags'
              nCrwMultiScale=CrwLastMade
            else
              nCrwIgnoreScale=CrwLastMade
            endif
            if((i.eq.1.and.EM9ScaleLim(KDatBlock).gt.0.).or.
     1         (i.eq.2.and.EM9ScaleLim(KDatBlock).lt.0.).or.
     2         (i.eq.3.and.EM9ScaleLim(KDatBlock).eq.0.)) then
              lpom=.true.
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
          enddo
          if(kiqIn.gt.18) then
            call FeQuestCrwOn(nCrwIgnoreScale)
            call FeQuestCrwDisable(nCrwMultiScale)
            call FeQuestCrwDisable(nCrwUniqueScale)
          endif
          il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          tpom=5.
          call FeQuestLblMake(id,tpom,il,Label,'L','N')
          nLblRefl=LblLastMade
          xpom=tpom+FeTxLength(Label)+10.
          dpom=40.
          tpom=xpom+dpom+10.
          call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                        '*sig(I) will be %used in the process',
     2                        'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,FLimP,.false.,.false.)
          nEdwFLim=EdwLastMade
1200      if(CrwLogicQuest(nCrwUniqueScale)) then
            call FeQuestLblOn(nLblRefl)
            call FeQuestRealEdwOpen(nEdwFLim,FLimP,.false.,.false.)
            EM9ScaleLim(KDatBlock)=FLimP
            NoOfScales=1
          else
            call FeQuestLblDisable(nLblRefl)
            if(EdwStateQuest(nEdwFLim).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdwFLim,FLimP)
              call FeQuestEdwDisable(nEdwFLim)
              NoOfScales(KDatBlock)=1
            endif
            if(CrwLogicQuest(nCrwMultiScale)) then
              EM9ScaleLim(KDatBlock)=-1.
              NoOfScales(KDatBlock)=kiq
            else
              EM9ScaleLim(KDatBlock)=0.
              NoOfScales(KDatBlock)=1
            endif
          endif
1500      call FeQuestEvent(id,ich)
          if(CheckType.eq.EventCrw) then
            go to 1200
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1500
          endif
          if(ich.eq.0) then
            if(CrwLogicQuest(nCrwUniqueScale))
     1        call FeQuestRealFromEdw(nEdwFLim,EM9ScaleLim(KDatBlock))
          endif
          if(ich.ne.0) go to 9999
        endif
        iz=0
      else if(Klic.eq.1) then
        N1Obs=0
        call SetIntArrayTo(ihmx,6,0)
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
3000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=3200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 3000
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 3200
          if(ICullAr(nr).ne.0) go to 3000
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
        do i=1,NSymmN(1)
          call IndTr(ih,rm6(1,i,1,1),ihp,maxNDim)
          do j=1,maxNDim
            ihmx(j)=max(ihmx(j),iabs(ihp(j)))
          enddo
        enddo
        if(iq.ne.1.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 3000
        N1Obs=N1Obs+1
        go to 3000
3200    if(allocated(fia)) deallocate(fia,sigfia,irec,irecp,Used1)
        allocate(fia(N1Obs),sigfia(N1Obs),irec(N1Obs),irecp(N1Obs),
     1           Used1(N1Obs))
        scref(1)=N1Obs
        if(N1Obs.lt.5) then
          call FeChybne(-1.,YBottomMessage,'the number of selected '//
     1                  'reflections for the first scale too small.',
     1                  ' ',SeriousError)
          ich=1
          go to 9900
        endif
        call SetIntArrayTo(ihd,6,1)
        j=1
        do i=1,NDim(KPhase)
          ihd(i)=2*ihmx(i)+1
          if(imax/j.lt.ihd(i)) go to 8000
          j=j*ihd(i)
        enddo
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
        n=0
3300    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=9999)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 3300
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 9999
          if(ICullAr(nr).ne.0) go to 3300
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          ri=RIAr(nr)
          rs=RSAr(nr)
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
        endif
        if(iq.ne.1.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 3300
        n=n+1
        mx=IndPack(ih,ihd,ihmx,maxNDim)
        fia(n)=ri
        sigfia(n)=rs
        irec(n)=mx
        irecp(n)=iatw
        go to 3300
      else if(Klic.eq.2) then
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
        N2Used=0
        N2Obs=0
        NPairs=0
        swt=0.
        srel=0.
        srelq=0.
        Used1=0
4000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=4200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 4000
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 4200
          if(ICullAr(nr).ne.0) go to 4000
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
        if(iq.ne.kiq.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 4000
        N2Obs=N2Obs+1
        m=0
        do i=1,NSymmN(1)
          call IndTr(ih,rm6(1,i,1,1),ihp,maxNDim)
          mx=IndPack(ihp,ihd,ihmx,maxNDim)
          do j=1,N1Obs
            if(mx.eq.irec(j).and.iatw.eq.irecp(j).and.
     1         fia(j).ge.EM9ScaleLim(KDatBlock)*sigfia(j)) then
              Used1(j)=1
              if(m.eq.0) N2Used=N2Used+1
              m=m+1
              NPairs=NPairs+1
              rel=fia(j)/ri
              wt=1./(rel**2*((sigfia(j)/fia(j))**2+(rs/ri)**2))
              srel=srel+wt*rel
              srelq=srelq+(wt*rel)**2
              swt=swt+wt
            endif
          enddo
        enddo
        go to 4000
4200    if(N2Used.lt.5) then
          call FeChybne(-1.,YBottomMessage,'the number of common '//
     1                  'reflections too small.',
     2                  'Try another method or modify the minimal '//
     3                  'I/sig(I) ratio.',
     4                  SeriousError)
          ich=1
          go to 9900
        else
          sckor(kiq)=srel/swt
!          sckors(kiq)=sqrt((srelq-srel**2/swt)/(float(m-1)*swt))
          sckors(kiq)=sqrt(srelq)/swt
          N1Used=0
          do i=1,N1Obs
            if(Used1(i).gt.0) N1Used=N1Used+1
          enddo
          sccom1(kiq)=N1Used
          sccom(kiq)=N2Used
          scref(kiq)=N2Obs
          scpairs=NPairs
          go to 9999
        endif
      else if(Klic.eq.3) then
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
5000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=9900)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          iqp=iq
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
            iq=KRefBlock
            if(.not.CalcDyn) iqp=1
          endif
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 9900
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          iqp=iq
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
!        if(iq.ne.1) then
          ri=ri*sckor(iq)
          rs=rs*sckor(iq)
          iqp=1
!        endif
        if(FromWhere.eq.AveFromEditM9) then
          write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iqp,nxx,itw,
     1                       tbar,ReadLam,DirCos,0.,NoRefBlock
        else
          RIAr(nr)=ri
          RSAr(nr)=rs
        endif
        go to 5000
      endif
      go to 9999
8000  call FeChybne(-1.,YBottomMessage,'diffraction indices are too '//
     1              'large to be sorted.',' ',SeriousError)
      ich=2
      go to 9900
9000  ich=1
      ErrFlag=0
9900  if(allocated(fia)) deallocate(fia,sigfia,irec,irecp,Used1)
9999  return
      end
      subroutine EM9ImportWizard(KlicIn)
      use Atoms_mod
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'powder.cmn'
      common/EM9CheckCommon/ nSbw,nButtInfo,nButtReimport,nButtModify,
     1                       nButtDelete,nButtUndelete
      character*128 Veta,VetaPom
      character*80  FormatOut,FormatIn,FlnOld
      character*20  RadString(MxRefBlock)
      dimension NRef95Old(MxRefBlock),xp(3)
      integer WhatToDo,SbwItemPointerQuest,FirstPowder,FirstSingle,
     1        CoDal,DifCodePrev
      integer ::  WhatToDoLast = 0
      logical CrwLogicQuest,ExistFile,Skrt,First,Change,FeYesNo,
     1        OnlyModify,EqIgCase,ImportMagnetic,StructureExists,
     2        ChangeSGTest,FeYesNoHeader,RefineEnd
      external EM9CheckRefBlock,FeVoid
      equivalence (IdNumbers( 1),IdImportSingleDatRed),
     1            (IdNumbers( 2),IdImportSingle),
     2            (IdNumbers( 3),IdImportCWPowder),
     3            (IdNumbers( 4),IdImportTOFPowder),
     4            (IdNumbers( 5),IdFullProfImport),
     5            (IdNumbers( 6),IdShelxImport),
     6            (IdNumbers( 7),IdCifImport),
     7            (IdNumbers( 8),IdXDImport),
     8            (IdNumbers( 9),IdJana2000Import),
     9            (IdNumbers(10),IdPDBImport),
     a            (IdNumbers(11),IdMagneticManual),
     1            (IdNumbers(12),IdMagneticSHELX),
     2            (IdNumbers(13),IdMagneticCIF),
     3            (IdNumbers(14),IdMagneticJana)
      save /EM9CheckCommon/
      Klic=KlicIn
      call CopyFile(fln(:ifln)//'.l51',fln(:ifln)//'.m51')
      call DeletePomFiles
      call MoveFile(fln(:ifln)//'.m51',fln(:ifln)//'.l51')
      if(ExistM95) call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
      if(ExistM90) call CopyFile(fln(:ifln)//'.m90',fln(:ifln)//'.z90')
      if(ExistM50) call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      if(ExistM40) call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      First=.true.
      Change=.false.
      ImportMagnetic=.false.
      NChange=0
      KnownM50=ExistM50
      KPhaseDR=1
      if(KnownM50) then
        do i=1,NPhase
          if(maxNDim.eq.NDim(i)) then
            KPhaseDR=i
            exit
          endif
        enddo
      endif
      call iom95(0,fln(:ifln)//'.m95')
      call SetLogicalArrayTo(ModifiedRefBlock,MxRefBlock,.false.)
      NRefBlockIn=NRefBlock
1050  call iom95(1,fln(:ifln)//'.l95')
      KRefBlock=0
      n=0
      do i=1,NRefBlock
        if(NRef95(i).gt.0) then
          if(KRefBlock.le.0) KRefBlock=i
          n=n+1
        endif
      enddo
      if(First) then
        norg=n
        First=.false.
      endif
      k=1
      if(norg.gt.0.or.n.gt.0) then
        if(SilentRun) go to 1170
1055    id=NextQuestId()
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_m95list.tmp','formatted',
     1                'unknown')
        xqd=600.
        dpom=xqd-20.-SbwPruhXd
        n=0
        do i=1,NRefBlock
          if(KRefBlock.le.0) KRefBlock=i
          n=n+1
          Veta=SourceFileRefBlock(i)
          j=idel(Veta)
          if(Veta(1:1).eq.'?') then
            if(EqIgCase(Veta(2:5),'bank')) then
              Veta='    -"-    '//Veta(2:idel(Veta))
            else
              Veta='not specified'
            endif
          else
            call FeCutNameLength(SourceFileRefBlock(i),Veta,
     1        dpom*.5-2.*EdwMarginSize,CutTextFromLeft)
          endif
          if(NRef95(i).gt.0) then
            Veta=Veta(:idel(Veta))//Tabulator//'|'//Tabulator
          else
            Veta=Veta(:idel(Veta))//Tabulator//' --- Deleted ---'
            go to 1065
          endif
          if(DifCode(i).gt.200) then
            VetaPom='Powder TOF/ED'
          else if(DifCode(i).gt.100) then
            VetaPom='Powder CW'
          else if(DifCode(i).gt.0) then
            VetaPom='Single crystal'
          else
            if(DifCode(i).eq.IdImportSHELXF.or.
     1         DifCode(i).eq.IdImportGeneralF.or.
     2         DifCode(i).eq.IdImportDABEX) then
              VetaPom='F(hkl) imported'
            else
              VetaPom='I(hkl) imported'
            endif
          endif
          Veta=Veta(:idel(Veta))//VetaPom(:idel(VetaPom))//Tabulator//
     1         '|'//Tabulator
          if(RadiationRefBlock(i).eq.XRayRadiation) then
            RadString(i)='X-rays'
          else if(RadiationRefBlock(i).eq.NeutronRadiation) then
            RadString(i)='Neutrons'
          else if(RadiationRefBlock(i).eq.ElectronRadiation) then
            RadString(i)='Electrons'
          else
            RadString(i)=' '
          endif
          k=KLamRefBlock(i)
          if(RadiationRefBlock(i).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAveRefBlock(i).gt.0.) then
              write(Cislo,'(f10.6)') LamAveRefBlock(i)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          RadString(i)=RadString(i)(:idel(RadString(i))+1)//
     1                 Cislo(:idel(Cislo))
          Veta=Veta(:idel(Veta))//RadString(i)(:idel(RadString(i)))
1065      write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        ilk=min(ifix((float(n)*MenuLineWidth+2.)/QuestLineWidth)+3,15)
        ilk=max(ilk,6)
        Veta='Data repository'
        WizardMode=.false.
        il=1
        call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,0,0)
        call FeQuestLblMake(id,           30.,il,'File'     ,'L','N')
        call FeQuestLblMake(id,   dpom/2.+20.,il,'Type'     ,'L','N')
        call FeQuestLblMake(id,3.*dpom/4.+20.,il,'Radiation','L','N')
        UseTabs=NextTabs()
        pom=dpom/2.-EdwMarginSize
        call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
        call FeTabsAdd(pom+EdwMarginSize,UseTabs,IdRightTab,' ')
        pom=3.*dpom/4.-EdwMarginSize
        call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
        call FeTabsAdd(pom+EdwMarginSize,UseTabs,IdRightTab,' ')
        xpom=10.
        il=ilk-1
        call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                      SbwVertical)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_m95list.tmp')
        dpom=xqd-10.
        xpom=5.
        il=ilk
        dpom=70.
        spom=15.
        tpom=dpom+spom
        xpom=xqd*.5-tpom*3.+spom*.5
        do i=1,6
          if(i.eq.1) then
            Veta='%Info'
          else if(i.eq.2) then
            nButtInfo=ButtonLastMade
            Veta='%Reimport'
          else if(i.eq.3) then
            nButtReimport=ButtonLastMade
            Veta='%Modify'
          else if(i.eq.4) then
            nButtModify=ButtonLastMade
            Veta='%Delete'
          else if(i.eq.5) then
            nButtDelete=ButtonLastMade
            Veta='%Undelete'
          else
            nButtUndelete=ButtonLastMade
            Veta='Im%port new'
          endif
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(n.le.0.and.i.lt.4) then
            j=ButtonDisabled
          else
            j=ButtonOff
          endif
          call FeQuestButtonOpen(ButtonLastMade,j)
          xpom=xpom+tpom
        enddo
        nButtNew=ButtonLastMade
1150    call FeQuestEventWithCheck(id,ich,EM9CheckRefBlock,FeVoid)
        KRefBlock=SbwItemPointerQuest(nSbw)
        if(CheckType.eq.EventButton) then
          if(CheckNumber.eq.nButtInfo) then
            NInfo=1
            if(SourceFileRefBlock(KRefBlock)(1:1).eq.'?') then
              TextInfo(NInfo)=SourceFileRefBlock(KRefBlock)(2:)
            else
              TextInfo(NInfo)='Imported file: '//
     1                        SourceFileRefBlock(KRefBlock)
              NInfo=NInfo+1
              TextInfo(NInfo)='Date/time    : '//
     1          SourceFileDateRefBlock(KRefBlock)//' '//
     2          SourceFileTimeRefBlock(KRefBlock)
            endif
            NInfo=NInfo+1
            j=DifCode(KRefBlock)
            if(DifCode(KRefBlock).gt.200) then
              Veta='???'
              do i=1,nTOFPowderTypes
                if(OrderTOFPowderTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men4(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Powder TOF data from: '//
     1                        Veta(:idel(Veta))
            else if(DifCode(KRefBlock).gt.100) then
              Veta='???'
              do i=1,nCWPowderTypes
                if(OrderCWPowderTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men3(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Powder data from: '//Veta(:idel(Veta))
            else if(DifCode(KRefBlock).gt.0) then
              Veta='???'
              do i=1,nDiffTypes
                if(OrderDiffTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men1(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Data collection from: '//
     1                        Veta(:idel(Veta))
            else
              Veta='???'
              do i=1,nSingleTypes
                if(OrderSingleTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men2(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Imported from: '//Veta(:idel(Veta))
            endif
            call DelChar(TextInfo(NInfo),'%')
            NInfo=NInfo+1
            TextInfo(NInfo)='Radiation: '//RadString(KRefBlock)
            NInfo=NInfo+1
            j=0
            if(PolarizationRefBlock(KRefBlock).eq.PolarizedCircular)
     1        then
              Veta='Circular'
            else if(PolarizationRefBlock(KRefBlock).eq.PolarizedMonoPer)
     1        then
              Veta='by monochromator in perpendicular setting'
              j=1
            else if(PolarizationRefBlock(KRefBlock).eq.PolarizedMonoPar)
     1        then
              Veta='by monochromator in parallel setting'
              j=1
            else
              Veta='Linearly'
            endif
            TextInfo(NInfo)='Polarization: '//Veta(:idel(Veta))
            if(j.gt.0) then
              NInfo=NInfo+1
              write(TextInfo(NInfo),'(''Glancing angle of the '',
     1                                ''monochromator: '',f8.3)')
     2          AngleMonRefBlock(KRefBlock)
              NInfo=NInfo+1
              write(TextInfo(NInfo),'(''Fraction of the perfectness '',
     1                                ''of monochromator: '',f8.3)')
     2          FractPerfMonRefBlock(KRefBlock)
            endif
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            go to 1150
          else if(CheckNumber.eq.nButtDelete) then
            if(NRef95(KRefBlock).gt.0) then
              NRef95Old(KRefBlock)=NRef95(KRefBlock)
              NRef95(KRefBlock)=0
              KRefBlock=0
              call FeQuestRemove(id)
              call FeTabsReset(UseTabs)
              NChange=NChange+1
              go to 1055
            else
              go to 1150
            endif
          else if(CheckNumber.eq.nButtUndelete) then
            if(NRef95(KRefBlock).le.0) then
              NRef95(KRefBlock)=NRef95Old(KRefBlock)
              call FeQuestRemove(id)
              call FeTabsReset(UseTabs)
              NChange=NChange-1
              go to 1055
            else
              go to 1150
            endif
          else if(CheckNumber.eq.nButtReimport.or.
     1            CheckNumber.eq.nButtModify) then
            if(SourceFileRefBlock(KRefBlock)(1:1).eq.'?'.and.
     1         CheckNumber.eq.nButtReimport) then
              go to 1150
            else if(NRef95(KRefBlock).le.0) then
              go to 1150
            endif
            NChange=NChange+1
          endif
          CreateNewRefBlock=CheckNumber.eq.nButtNew
          ReimportRefBlock=CheckNumber.eq.nButtReimport
          if(CheckNumber.ne.nButtReimport.and.
     1       CheckNumber.ne.nButtModify) then
            NRefBlock=NRefBlock+1
            KRefBlock=NRefBlock
            NChange=NChange+1
          endif
          call FeQuestRemove(id)
          call DeleteFile(fln(:ifln)//'_m95list.tmp')
          OnlyModify=CheckNumber.eq.nButtModify
          WhatToDo=0
          call FeTabsReset(UseTabs)
          go to 1190
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        call FeTabsReset(UseTabs)
        if(ich.eq.0) then
          if(NChange.gt.0) then
            if(.not.FeYesNo(-1.,-1.,
     1         'Do you want to accept made changes?',1)) go to 4000
          endif
        else
          if(NChange.gt.0) then
            if(FeYesNo(-1.,-1.,
     1         'Do you want to discard made changes?',0)) go to 4000
          endif
        endif
        call FeQuestRemove(id)
        call DeleteFile(fln(:ifln)//'_m95list.tmp')
1170    if(NChange.gt.0.or.SilentRun) then
          do j=1,NRefBlock
            if(NRef95(j).le.0) ModifiedRefBlock(j)=.true.
          enddo
          Skrt=.false.
          do 1175i=1,NDatBlock
            do j=1,NRefBlock
              if((RefDatCorrespond(j).eq.i.and.NRef95(j).gt.0).or.
     1            RefDatCorrespond(j).le.0) go to 1175
            enddo
            Skrt=.true.
            NRef90(i)=0
1175      continue
          if(Skrt.and.ExistM40) call UpdateDatBlocksM40
          call CompleteM95(0)
          call CompleteM90
          if(ExistM40.and.Skrt) then
            call iom40(1,0,fln(:ifln)//'.m40')
            call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          endif
        endif
        go to 3000
      else
        KRefBlock=1
        NRefBlock=1
        CreateNewRefBlock=.true.
        OnlyModify=.false.
        ReimportRefBlock=.false.
        NChange=NChange+1
      endif
      go to 1190
      entry EM9HKLReimport
      call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
      Klic=1
      First=.true.
      KPhaseDR=1
      CreateNewRefBlock=.false.
      ReimportRefBlock=.true.
      OnlyModify=.false.
      NChange=1
      ImportMagnetic=.false.
      NRefBlockIn=NRefBlock
      call DeleteFile(fln(:ifln)//'.m90')
      ExistM40=ExistFile(fln(:ifln)//'.m40')
      ExistM90=.false.
      WizardMode=.false.
      go to 1195
1190  WizardId=NextQuestId()
      WizardMode=.true.
      WizardTitle=.true.
      WizardLines=17
      WizardLength=600.
      call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                   'x',0,LightGray,0,0)
1195  if(OnlyModify) go to 1400
      if(.not.CreateNewRefBlock) then
        if(DifCode(KRefBlock).gt.200) then
          WhatToDo=IdImportTOFPowder
          TOFInD=DifCode(KRefBlock).eq.IdTOFISISD
        else if(DifCode(KRefBlock).gt.100) then
          WhatToDo=IdImportCWPowder
        else if(DifCode(KRefBlock).gt.0) then
          WhatToDo=IdImportSingleDatRed
          call DRSetCell(0)
        else
          WhatToDo=IdImportSingle
        endif
        go to 1400
      endif
1200  ErrFlag=0
      id=NextQuestId()
      call FeQuestTitleMake(id,'Specify type of the file to be '//
     1                         'imported')
      if(KRefBlock.gt.1.or.KnownM50) then
        n=4
        if(WhatToDoLast.gt.0.and.WhatToDoLast.le.n) then
          m=WhatToDoLast
        else if(KRefBlock.gt.1) then
          if(DifCode(KRefBlock-1).gt.200) then
            m=IdImportTOFPowder
          else if(DifCode(KRefBlock-1).gt.100) then
            m=IdImportCWPowder
          else if(DifCode(KRefBlock-1).gt.0) then
            m=IdImportSingleDatRed
          else
            m=IdImportSingle
          endif
        else
          m=IdImportCWPowder
        endif
      else
        n=14
        m=IdImportSingleDatRed
      endif
      il=0
      xpoml=5.
      Veta='Single crystal:'
      j=1
      call FeBoldFont
      xpom=xpoml+FeTxLength(Veta)+80.
      call FeNormalFont
      tpom=xpom+CrwgXd+10.
      do i=1,n
        il=il+1
        if(i.eq.3) then
          Veta='Powder data:'
          j=1
        else if(i.eq.6) then
          Veta='Structure:'
          j=1
        else if(i.eq.11) then
          Veta='Magnetic parent structure:'
          j=1
        endif
        if(j.ne.0) then
          call FeQuestLblMake(id,xpoml,il,Veta,'L','B')
          j=0
        endif
        if(i.eq.IdImportSingleDatRed) then
          Veta='%known diffractometer formats'
        else if(i.eq.IdImportSingle) then
          Veta='%reflection file corrected for LP and absorption'
        else if(i.eq.IdImportCWPowder) then
          Veta='various C%W formats'
        else if(i.eq.IdImportTOFPowder) then
          Veta='various %TOF/ED formats'
        else if(i.eq.IdFullProfImport) then
          Veta='from Full%Prof'
        else if(i.eq.IdShelxImport) then
          Veta='from S%HELX'
        else if(i.eq.IdCifImport) then
          Veta='from %CIF'
        else if(i.eq.IdXDImport) then
          Veta='from %XD'
        else if(i.eq.IdJana2000Import) then
          Veta='from %Jana2000'
        else if(i.eq.IdPDBImport) then
          Veta='from PD%B'
        else if(i.eq.IdMagneticManual) then
          Veta='nuclear model made i%nteractively'
        else if(i.eq.IdMagneticSHELX) then
          Veta='nuclear model from SHE%LX'
        else if(i.eq.IdMagneticCIF) then
          Veta='nuclear model from CI%F'
        else if(i.eq.IdMagneticJana) then
          Veta='nuclear model from Jana200%6'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i.eq.m)
        if(ImportMagnetic.and.KRefBlock.gt.1.and.
     1     ((DifCode(1).gt.100.and.i.le.2).or.
     2      (DifCode(1).lt.100.and.i.gt.2)))
     3    call FeQuestCrwDisable(CrwLastMade)
      enddo
      call FeQuestButtonDisable(ButtonEsc)
1250  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1250
      endif
      if(ich.eq.0) then
        nCrw=nCrwFirst
        do i=1,n
          if(CrwLogicQuest(nCrw)) then
            WhatToDo=i
            go to 1300
          endif
          nCrw=nCrw+1
        enddo
      endif
1300  if(ich.ne.0) then
        if(CreateNewRefBlock) then
          NRefBlock=NRefBlock-1
          NChange=NChange-1
        endif
        if(ExistM95.or.ReimportRefBlock) then
          call FeQuestRemove(WizardId)
          go to 1050
        else
          go to 9900
        endif
      endif
      WhatToDoLast=WhatToDo
1400  if(OnlyModify) then
        isPowder=DifCode(KRefBlock).gt.100
        isTOF=DifCode(KRefBlock).gt.200.and.DifCode(KRefBlock).le.300
        isED=DifCode(KRefBlock).gt.300
      else
        isPowder=WhatToDo.eq.IdImportCWPowder.or.
     1           WhatToDo.eq.IdImportTOFPowder
        isTOF=WhatToDo.eq.IdImportTOFPowder
      endif
      if(ParentStructure) then
        ExistSingle=.not.isPowder
        call SSG2QMag(fln)
        call QMag2SSG(fln,0)
      endif
      if(WhatToDo.eq.IdImportSingleDatRed.or.
     1   WhatToDo.eq.IdImportSingle.or.
     2   WhatToDo.eq.IdImportCWPowder.or.
     3   WhatToDo.eq.IdImportTOFPowder.or.
     4   OnlyModify) then
        if(CreateNewRefBlock) then
          if(KRefBlock.eq.1) then
            CellRefBlock(1,0)=-1.
            call SetRealArrayTo(QuRefBlock(1,1,0),9,-333.)
            if(KnownM50) then
              call CopyVek(CellPar(1,1,1),CellRefBlock(1,0),6)
              call CopyVek(CellParSU(1,1,1),CellRefBlockSU(1,0),6)
              call CopyVek(Qu(1,1,1,1),QuRefBlock(1,1,0),
     1                     3*NDimI(KPhaseDR))
              if(isPowder) call SetBasicM41(0)
            endif
          endif
          call SetBasicM95(KRefBlock)
          if(KRefBlock.gt.1) then
            DifCodePrev=DifCode(KRefBlock-1)
          else
            DifCodePrev=0
          endif
          if(KnownM50) then
             NDim95(KRefBlock)=NDim(KPhaseDR)
             call CopyVek(CellRefBlock(1,0),CellRefBlock(1,KRefBlock),6)
             call CopyVek(QuRefBlock(1,1,0),QuRefBlock(1,1,KRefBlock),
     1                    3*NDimI(KPhaseDR))
             if(MagneticType(KPhaseDR).ne.0) then
               RadiationRefBlock(KRefBlock)=NeutronRadiation
               if(KRefBlock.le.1.and..not.ReimportRefBlock) then
                 if(LamAveRefBlock(KRefBlock).le.0.and.
     1              .not.isTOF) then
                   LamAveRefBlock(KRefBlock)=1.
                   LamA1RefBlock(KRefBlock)=1.
                   LamA2RefBlock(KRefBlock)=1.
                   LamRatRefBlock(KRefBlock)=0.
                   TempRefBlock(KRefBlock)=10.
                 endif
               else if(KRefBlock.gt.1) then
                 LamAveRefBlock(KRefBlock)=LamAveRefBlock(KRefBlock-1)
                 LamA1RefBlock(KRefBlock)=LamA1RefBlock(KRefBlock-1)
                 LamA2RefBlock(KRefBlock)=LamA2RefBlock(KRefBlock-1)
                 LamRatRefBlock(KRefBlock)=LamRatRefBlock(KRefBlock-1)
                 TempRefBlock(KRefBlock)=TempRefBlock(KRefBlock-1)
               endif
             else
               if(KRefBlock.le.1.and..not.ReimportRefBlock.and.
     1            LamAveRefBlock(KRefBlock).le.0.) then
                 LamAveRefBlock(KRefBlock)=LamAve(1)
                 LamA1RefBlock(KRefBlock)=LamAve(1)
                 LamA2RefBlock(KRefBlock)=LamAve(1)
               endif
            endif
          endif
          if(WhatToDo.eq.IdImportSingle.or.
     1       WhatToDo.eq.IdImportSingleDatRed) then
            if(WhatToDo.eq.IdImportSingle) then
              if(DifCodePrev.gt.-100.and.DifCodePrev.lt.0) then
                DifCode(KRefBlock)=DifCodePrev
              else
                DifCode(KRefBlock)=IdImportSHELXI
              endif
            else
              if(DifCodePrev.gt.0.and.DifCodePrev.lt.100) then
                DifCode(KRefBlock)=DifCodePrev
              else
                DifCode(KRefBlock)=IdKumaCCD
              endif
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
            if(LamAveRefBlock(KRefBlock).lt.0.) then
              LamAveRefBlock(KRefBlock)=LamAveD(6)
              LamA1RefBlock(KRefBlock)=LamA1D(6)
              LamA2RefBlock(KRefBlock)=LamA2D(6)
              LamRatRefBlock(KRefBlock)=LamRatD(6)
            endif
          else if(WhatToDo.eq.IdImportCWPowder) then
            if(DifCodePrev.gt.100.and.DifCodePrev.lt.200) then
              DifCode(KRefBlock)=DifCodePrev
            else
              DifCode(KRefBlock)=IdDataFree
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
            if(LamAveRefBlock(KRefBlock).lt.0.) then
              LamAveRefBlock(KRefBlock)=LamAveD(5)
              LamA1RefBlock(KRefBlock)=LamA1D(5)
              LamA2RefBlock(KRefBlock)=LamA2D(5)
              LamRatRefBlock(KRefBlock)=LamRatD(5)
            endif
          else if(WhatToDo.eq.IdImportTOFPowder) then
            if(DifCodePrev.gt.200.and.DifCodePrev.lt.300) then
              DifCode(KRefBlock)=DifCodePrev
            else
              DifCode(KRefBlock)=IdTOFGSAS
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedLinear
          endif
        endif
        call EM9ImportSavePar
        write(Cislo,'(''.l'',i2)') KRefBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        if(OnlyModify) then
          ICorrZpet=0
          go to 1600
        endif
        if(.not.ReimportRefBlock) call DeleteFile(RefBlockFileName)
        Navrat=0
1500    ErrFlag=0
        if(WhatToDo.eq.IdImportSingleDatRed) then
          call EM9ImportDatRedData(Navrat)
        else if(WhatToDo.eq.IdImportSingle) then
          call EM9ImportSingle(Navrat)
        else if(WhatToDo.eq.IdImportCWPowder) then
          call EM9ImportCWPowder(Navrat)
        else if(WhatToDo.eq.IdImportTOFPowder) then
          call EM9ImportTOFPowder(Navrat)
        endif
        if(ErrFlag.gt.0) then
          if(CreateNewRefBlock) then
            NRefBlock=NRefBlock-1
            NChange=NChange-1
          endif
          if(ExistM95.or.ReimportRefBlock) then
            call FeQuestRemove(WizardId)
            go to 1050
          else
            go to 9900
          endif
        else if(ErrFlag.lt.0) then
          if(CreateNewRefBlock) then
            go to 1200
          else
            call FeQuestRemove(WizardId)
            go to 1050
          endif
        endif
        Navrat=1
        ICorrZpet=0
1600    ErrFlag=0
        if(isPowder) then
          if(OnlyModify) then
            i=-1
            RadiationDetails=.true.
            call EM9CompleteBasic(i,ich)
            ModifiedRefBlock(KRefBlock)=.true.
          endif
        else
          if(OnlyModify.and.DifCode(KRefBlock).lt.0) then
            i=-1
            RadiationDetails=.true.
            call EM9CompleteBasic(i,ich)
            ModifiedRefBlock(KRefBlock)=.true.
            go to 1700
          endif
          if(CorrLp(KRefBlock).ge.0.and.ICorrZpet.lt.2) then
            call DRCorrLPDecay(OnlyModify)
            if(ErrFlag.gt.0) then
              if(OnlyModify) then
                call FeQuestRemove(WizardId)
                go to 1050
              else
                go to 9900
              endif
            else if(ErrFlag.lt.0) then
              go to 1500
            endif
          endif
          ErrFlag=0
          if(CorrAbs(KRefBlock).ge.0) then
            call DRAbsCorr(OnlyModify.and.CorrLp(KRefBlock).lt.0)
            if(ErrFlag.gt.0) then
              if(.not.OnlyModify) NRef95(KRefBlock)=0
              call FeQuestRemove(WizardId)
              go to 1050
            else if(ErrFlag.lt.0) then
              if(CorrLp(KRefBlock).ge.0) then
                go to 1600
              else
                go to 1500
              endif
            endif
          endif
        endif
1700    if(SilentRun) go to 1750
        call FeFillTextInfo('em9importdatreddata2.txt',0)
        call FeWizardTextInfo('INFORMATION',1,-1,1,ich)
        if(ich.ne.0) then
          if(ich.gt.0) then
            if(CorrAbs(KRefBlock).ge.0) then
              ICorrZpet=1
              go to 1600
            else if(CorrLp(KRefBlock).ge.0) then
              ICorrZpet=2
              go to 1600
            else
              go to 1500
            endif
          else
            go to 9900
          endif
        endif
1750    call FeQuestRemove(WizardId)
        go to 1050
      endif
      if(WhatToDo.ne.IdMagneticManual.and.
     1   WhatToDo.ne.IdMagneticSHELX.and.
     2   WhatToDo.ne.IdMagneticCIF.and.
     3   WhatToDo.ne.IdMagneticJana) then
        call FeQuestRemove(WizardId)
        WizardMode=.false.
      endif
2000  if(WhatToDo.eq.IdShelxImport) then
        call ReadSHELX(0,ich)
        go to 9999
      else if(WhatToDo.eq.IdCIFImport) then
        call ReadCIF(0,CoDal)
        if(ErrFlag.ne.0) go to 9999
        KnownM50=.true.
        if(CoDal.eq.1) then
          go to 1190
        else
          go to 9999
        endif
      else if(WhatToDo.eq.IdXDImport) then
        call ZXD
        go to 9999
      else if(WhatToDo.eq.IdPDBImport) then
        call ReadPDB
        go to 9999
      else if(WhatToDo.eq.IdMagneticManual.or.
     1        WhatToDo.eq.IdMagneticSHELX.or.
     2        WhatToDo.eq.IdMagneticCIF.or.
     3        WhatToDo.eq.IdMagneticJana) then
        call EM9ImportMagnetic(WhatToDo-IdMagneticManual,CoDal)
        if(CoDal.lt.0) then
          go to 9900
        else if(CoDal.gt.0) then
          go to 1200
        endif
        KnownM50=.true.
        ImportMagnetic=.true.
        call FeQuestRemove(WizardId)
        go to 1190
      endif
3000  FirstSingle=0
      FirstPowder=0
      FirstSingle=0
      FirstPowder=0
      do i=1,NRefBlock
        if(ModifiedRefBlock(i)) then
          if(DifCode(i).lt.100) then
            if(FirstSingle.eq.0) FirstSingle=i
          else
            if(FirstPowder.eq.0) FirstPowder=i
          endif
        endif
      enddo
      if(FirstSingle.gt.0) then
        KRefBlock=FirstSingle
        isPowder=.false.
      else if(FirstPowder.gt.0) then
        KRefBlock=FirstPowder
        isPowder=.true.
      else
        go to 9999
      endif
      if(ImportMagnetic) then
        if(NRefBlock.gt.0) then
          SilentRun=NRefBlock.le.1
          call EM9CreateM90(0,ich)
          call iom90(0,fln(:ifln)//'.m90')
          if(isPowder) then
            call SetBasicM41(KRefBlock)
            do i=1,NDatBlock
              if(Radiation(i).eq.NeutronRadiation) then
                j=0
                if(TOFSigma(1,i).gt.-3000.) then
                  j=j+1
                  call CopyVek(TOFSigma(1,i),GaussPwd(1,1,i),3)
                endif
                if(TOFGamma(1,i).gt.-3000.) then
                  j=j+2
                  call CopyVek(TOFGamma(1,i),LorentzPwd(1,1,i),3)
                endif
                if(j.gt.0) KProfPwd(1,i)=j
                TOFKey(i)=1
                if(TOFAlfbe(1,i).gt.-3000.) then
                  AsymPwd(1,i)=TOFAlfbe(1,i)
                  AsymPwd(2,i)=TOFAlfbe(3,i)
                  AsymPwd(3,i)=TOFAlfbe(2,i)
                  AsymPwd(4,i)=TOFAlfbe(4,i)
                  KAsym(i)=IdPwdAsymTOF1
                  NAsym(i)=4
                endif
                if(TOFAlfbt(1,i).gt.-3000.) then
                  AsymPwd(5,i)=TOFAlfbt(1,i)
                  AsymPwd(6,i)=TOFAlfbt(3,i)
                  AsymPwd(7,i)=TOFAlfbt(2,i)
                  AsymPwd(8,i)=TOFAlfbt(4,i)
                  KAsym(i)=IdPwdAsymTOF2
                  NAsym(i)=8
                endif
              endif
            enddo
            call iom41(1,0,fln(:ifln)//'.m41')
          endif
          ExistM90=.true.
          ExistM95=.true.
          SilentRun=.false.
        else
          ExistM90=.false.
          ExistM95=.false.
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        if(ExistM95.and.isPowder) then
          FlnOld=Fln
          iflnOld=ifln
          Fln=Fln(:ifln)//'_tmp'
          iFln=idel(Fln)
          i=0
3050      if(StructureExists(Fln)) then
            i=i+1
            write(Cislo,'(i5)') i
            call Zhusti(Cislo)
            Fln=Fln(:iFln)//'_'//Cislo(:idel(Cislo))
            go to 3050
          endif
          iFln=idel(Fln)
        endif
        WizardId=NextQuestId()
        WizardMode=.true.
        WizardTitle=.true.
        WizardLines=10
        WizardLength=570.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     'x',0,LightGray,0,0)
        if(isPowder) then
          if(ExistM90) then
            Veta='em9importsolvepowder.txt'
          else
            go to 3400
          endif
        else
          go to 3450
c          if(ExistM95) then
c            Veta='em9importsolvesinglecrystal.txt'
c          else
c            go to 3400
c          endif
        endif
        call FeFillTextInfo(Veta,0)
        call FeWizardTextInfo('INFORMATION',0,1,1,ich)
        call DeleteFile(Fln(:iFln)//'.m70')
        if(isPowder.and.ExistM90) then
          NTwin=1
          k=1
          call DeleteFile(fln(:ifln)//'.m70')
3100      if(NDimI(KPhaseDR).gt.0) then
            do i=1,NSymm(KPhaseDR)
              if(k.eq.1) then
                call SetRealArrayTo(s6(1,i,1,KPhaseDR),NDim(KPhaseDR),
     1                              0.)
              else
                call CopyMat(rm(1,i,1,KPhaseDR),rm6(1,i,1,KPhaseDR),3)
              endif
            enddo
            if(k.ne.1) then
              NDimIP=NDimI(KPhaseDR)
              NDim(KPhaseDR)=3
              NDimq(KPhaseDR)=9
              NDimI(KPhaseDR)=0
            endif
          endif
          KQMagOld=KQMag(KPhaseDR)
          KQMag(KPhaseDR)=0
          ParentStructure=.false.
          call iom50(1,0,fln(:ifln)//'.m50')
          if(k.eq.1) then
            call SetBasicM40(.true.)
            call iom40(1,0,fln(:ifln)//'.m40')
          else
            call CopyFile(FlnOld(:iFlnOld)//'.m44',Fln(:ifln)//'.m40')
            call SetIntArrayTo(kiPwd,MxParPwd,0)
            call iom41(1,0,fln(:ifln)//'.m41')
          endif
          call CopyFile(FlnOld(:iFlnOld)//'.m90',Fln(:ifln)//'.m90')
          call CopyFile(FlnOld(:iFlnOld)//'.m95',Fln(:ifln)//'.m95')
          if(NDatBlock.gt.1) then
            do i=1,NDatBlock
              MenuDatBlock(i)=DatBlockName(i)
              if(DataType(i).eq.1) then
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     2            '->single crystal'
              else
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'->powder'
              endif
              if(Radiation(i).eq.NeutronRadiation) then
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'/neutrons'
              else
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'/X-ray'
              endif
            enddo
          endif
          if(k.eq.1) then
            call CopyFile(fln(:ifln)//'.m40',PreviousM40)
            call CopyFile(fln(:ifln)//'.m41',PreviousM41)
            call CopyFile(fln(:ifln)//'.m50',PreviousM50)
3110        call CrlLeBailMaster(ich)
            if(ich.ne.0) then
              if(ich.gt.0) then
                go to 3110
              else
                go to 3130
              endif
            else
              if(.not.FeYesNo(-1.,-1.,'Do you really want to leave '//
     1                        'the form for le Bail refinement?',
     2                        0)) go to 3110
            endif
            call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            if(KQMagOld.gt.0) call CopyVek(QuPwd(1,1,KPhaseDR),QMag,3)
            call CopyVek(CellPwd(1,KPhaseDR),CellPar(1,1,KPhaseDR),6)
            call iom50(1,0,flnOld(:iFlnOld)//'.m50')
            call iom41(1,0,flnOld(:iFlnOld)//'.m41')
            call CopyFile(Fln(:ifln)//'.m90',FlnOld(:iFlnOld)//'.m90')
            call CopyFile(Fln(:ifln)//'.m95',FlnOld(:iFlnOld)//'.m95')
            call MoveFile(fln(:ifln)//'.m70',flnOld(:iflnOld)//'.m70')
          else
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
3112        read(lni,FormA,end=3114) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 3112
3114        call CloseIfOpened(lni)
            Veta='fixed all *'
            write(lno,FormA) Veta(:idel(Veta))
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            call RefRewriteCommands(1)
            call iom50(0,0,fln(:ifln)//'.m50')
3120        call CrlRietveldMaster(ich)
            if(ich.ne.0) then
              if(ich.gt.0) then
                k=k-1
                call MoveFile(flnOld(:iflnOld)//'.m70',
     1                        fln(:ifln)//'.m70')
                go to 3100
              else
                go to 3130
              endif
            else
              if(.not.FeYesNo(-1.,-1.,'Do you really want to leave '//
     1                        'the form for Rietveld refinement?',
     2                        0)) go to 3120
            endif
            if(NDimIP.ne.NDimI(KPhaseDR)) then
              NDim(KPhaseDR)=3+NDimIP
              NDimq(KPhaseDR)=NDim(KPhaseDR)**2
              NDimI(KPhaseDR)=NDimIP
              if(allocated(qcnt)) deallocate(qcnt,phf,sphf,OrthoX40,
     1                                       OrthoDelta,OrthoEps)
              n=NAtAll
              allocate(qcnt(3,n),phf(n),sphf(n),
     1                 OrthoX40(n),OrthoDelta(n),OrthoEps(n))
              do ia=1,NAtAll
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                qcnt(1:3,ia)=0.
                 phf(ia)=0.
                sphf(ia)=0.
                OrthoX40(ia)=0.
                OrthoDelta(ia)=1.
                OrthoEps(ia)=.95
              enddo
              call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            endif
            call CopyFile(fln(:ifln)//'.m40',flnOld(:iFlnOld)//'.m43')
          endif
          go to 3140
3130      NInfo=2
          TextInfo(1)='You are about to leave the wizard for handling'//
     1                ' of magnetic structures from powder.'
          TextInfo(2)='Your work will not be lost but from now you '//
     1                'have to select your own way.'
          if(FeYesNoHeader(-1.,-1.,'Do you really want do leave the '//
     1                     'wizard?',0)) then
            call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            if(KQMagOld.gt.0) call CopyVek(QuPwd(1,1,KPhaseDR),QMag,3)
            call CopyVek(CellPwd(1,KPhase),CellPar(1,1,KPhaseDR),6)
            call iom50(1,0,flnOld(:iFlnOld)//'.m50')
            call iom41(1,0,flnOld(:iFlnOld)//'.m41')
            call CopyFile(Fln(:ifln)//'.m90',FlnOld(:iFlnOld)//'.m90')
            call CopyFile(Fln(:ifln)//'.m95',FlnOld(:iFlnOld)//'.m95')
            call MoveFile(fln(:ifln)//'.m70',flnOld(:iflnOld)//'.m70')
          else
            go to 3100
          endif
3140      call DeletePomFiles
          call DeleteAllFiles(Fln(:ifln)//'*')
          call iom50(0,0,flnOld(:iflnOld)//'.m50')
          call iom40(0,0,flnOld(:iflnOld)//'.m40')
          call iom90(0,flnOld(:iflnOld)//'.m90')
          call iom95(0,flnOld(:iflnOld)//'.m95')
          if(ich.eq.0.and.k.eq.1) then
            k=k+1
            go to 3100
          endif
          if(ich.lt.0.or.k.eq.2) then
            fln=flnOld
            ifln=iFlnOld
          endif
          if(ich.lt.0) then
            call FeQuestRemove(WizardId)
            WizardMode=.false.
            go to 9999
          endif
        endif
        if(.not.isPowder.and.ExistM95) then
3300      call CopyFile(FlnOld(:iFlnOld)//'.m40',Fln(:ifln)//'.m40')
          KQMagOld=KQMag(KPhaseDR)
          KQMag(KPhaseDR)=0
          ParentStructure=.false.
          MagneticType(KPhaseDR)=0
          call SSG2QMag(fln)
          call iom50(1,0,fln(:ifln)//'.m50')
          FormatOut=Format95
          FormatIn =Format95
          if(KQMagOld.gt.0) then
            FormatIn(5:5)='4'
          else
            FormatIn(5:5)='3'
          endif
          FormatOut(5:5)='3'
          call iom95(0,flnOld(:iflnOld)//'.m95')
          NDimOld=4
          do KRefB=1,NRefBlock
            call OpenRefBlockM95(95,KRefB,flnOld(:iflnOld)//'.m95')
            if(ErrFlag.ne.0) go to 3350
            write(Cislo,'(''.l'',i2)') KRefB
            if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
            RefBlockFileName=Fln(:ifln)//Cislo(:idel(Cislo))
            call OpenFile(96,RefBlockFileName,'formatted','unknown')
            if(ErrFlag.ne.0) go to 3350
            NRef95(KRefB)=0
            NLines95(KRefB)=0
3320        NDim(KPhaseDR)=NDimOld
            MaxNDim=NDimOld
            Format95=FormatIn
            call DRGetReflectionFromM95(95,iend,ich)
            if(ich.ne.0) go to 3350
            if(iend.ne.0) go to 3350
            do i=4,NDim(KPhaseDR)
              if(ih(i).ne.0) go to 3320
            enddo
            pom1=corrf(1)*corrf(2)
            if(RunScN(KRefBlock).gt.0) then
              ikde=min((RunCCD-1)/RunScGr(KRefBlock)+1,
     1                  RunScN(KRefBlock))
              pom1=pom1*ScFrame(ikde,KRefBlock)
            endif
            ri=ri*pom1
            rs=rs*pom1
            corrf(1)=1.
            corrf(2)=1.
            NProf=0
            NDim(KPhaseDR)=3
            maxNDim=3
            Format95=FormatOut
            call DRPutReflectionToM95(96,nl)
            NRef95(KRefB)=NRef95(KRefB)+1
            NLines95(KRefB)=NLines95(KRefB)+nl
            NDim(KPhaseDR)=NDimOld
            Format95=FormatIn
            go to 3320
3350        call CloseIfOpened(95)
            call CloseIfOpened(96)
            SourceFileRefBlock(KRefB)='?The strucure created from "'//
     1                                flnOld(:iflnOld)//'"'
            SourceFileDateRefBlock(KRefB)='?'
            SourceFileTimeRefBlock(KRefB)='?'
            UseTrRefBlock(KRefB)=.false.
            NDim95(KRefB)=3
            CellReadIn(KRefB)=.false.
            if(NLines95(KRefB).lt.0) NLines95(KRefB)=NRef95(KRefB)
          enddo
          call iom95(1,fln(:ifln)//'.m95')
          call FeQuestRemove(WizardId)
          WizardMode=.false.
          call CompleteM95(0)
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          UpdateAnomalous=.false.
          SilentRun=NRefBlock.le.1
          call EM9CreateM90(0,ich)
          SilentRun=.false.
          call iom90(0,fln(:ifln)//'.m90')
          WizardId=NextQuestId()
          WizardMode=.true.
          WizardTitle=.true.
          WizardLines=10
          WizardLength=570.
          call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                       'x',0,LightGray,0,0)
          call CrlSingleCrystalMaster(ich)
          if(ich.ne.0) then
            if(ich.gt.0) then
              go to 3300
            else
              go to 3400
            endif
          endif
          call iom50(0,0,flnOld(:iflnOld)//'.m50')
          KQMag(KPhaseDR)=KQMagOld
          ParentStructure=.true.
          MagneticType(KPhaseDR)=1
          call CopyFile(fln(:ifln)//'.l51',flnOld(:iflnOld)//'.l51')
          call iom50(1,0,flnOld(:iflnOld)//'.m50')
          call CopyFile(fln(:ifln)//'.m40',flnOld(:iflnOld)//'.m40')
          call DeletePomFiles
          call DeleteAllFiles(Fln(:ifln)//'*')
          fln=flnOld
          ifln=iFlnOld
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CrlSetMagMoments(KQMag(KPhaseDR))
          call iom40(1,0,fln(:ifln)//'.m40')
          ExistM90=.false.
          call iom95(0,fln(:ifln)//'.m95')
        endif
3400    call RefOpenCommands
        NacetlInt(nCmdncykl)=100
        NacetlReal(nCmdtlum)=.1
        call RefRewriteCommands(1)
3450    id=NextQuestId()
        call FeFillTextInfo('em9importrepanalysis.txt',0)
        il=3
        call FeQuestLblMake(id,WizardLength*.5,il,'INFORMATION','C','B')
        il=il+1
        tpom=(WizardLength-FeTxLength(TextInfo(1)))*.5
        il=-10*il
        do i=1,NInfo
          il=il-5
          call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
        enddo
        call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
3500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 3500
        endif
        if(ich.lt.0) then
          go to 9900
        endif
        call FeQuestRemove(WizardId)
        WizardMode=.false.
        call UseIrreps
        go to 9999
      else
        ich=0
        if(ExistM40) then
          NDatBlockIn=NDatBlock
        else
          call SetBasicM40(.false.)
          call iom40(1,0,fln(:ifln)//'.m40')
          NDatBlockIn=0
        endif
        if(NRefBlockIn.gt.0.or.isPowder) then
          call FeTabsReset(-1)
          call EM9CreateM90(0,ich)
          call iom90(0,fln(:ifln)//'.m90')
          ExistM90=.true.
        endif
        if(.not.ReimportRefBlock.and.isPowder) then
          call SetBasicM41(0)
          do i=1,NDatBlock
            if(Radiation(i).eq.NeutronRadiation) then
              j=0
              if(TOFSigma(1,i).gt.-3000.) then
                j=j+1
                call CopyVek(TOFSigma(1,i),GaussPwd(1,1,i),3)
              endif
              if(TOFGamma(1,i).gt.-3000.) then
                j=j+2
                call CopyVek(TOFGamma(1,i),LorentzPwd(1,1,i),3)
              endif
              if(j.gt.0) KProfPwd(1,i)=j
              TOFKey(i)=1
              if(TOFAlfbe(1,i).gt.-3000.) then
                AsymPwd(1,i)=TOFAlfbe(1,i)
                AsymPwd(2,i)=TOFAlfbe(3,i)
                AsymPwd(3,i)=TOFAlfbe(2,i)
                AsymPwd(4,i)=TOFAlfbe(4,i)
                KAsym(i)=IdPwdAsymTOF1
                NAsym(i)=4
              endif
              if(TOFAlfbt(1,i).gt.-3000.) then
                AsymPwd(5,i)=TOFAlfbt(1,i)
                AsymPwd(6,i)=TOFAlfbt(3,i)
                AsymPwd(7,i)=TOFAlfbt(2,i)
                AsymPwd(8,i)=TOFAlfbt(4,i)
                KAsym(i)=IdPwdAsymTOF2
                NAsym(i)=8
              endif
            endif
          enddo
          call iom41(1,0,fln(:ifln)//'.m41')
        endif
        if(NRefBlockIn.le.0) then
          if(NAtCalc.le.0) then
            if(isPowder) then
              id=NextQuestId()
              il=8
              xqd=400.
              call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
              Veta='The program can now continue with a wizard to '//
     1             'refine profile parameters,'
              tpom=5.
              il=-10
              call FeQuestLblMake(id,tpom,il,Veta,'L','N')
              Veta='make a space group test and solve the structure:'
              tpom=5.
              il=il-6
              call FeQuestLblMake(id,tpom,il,Veta,'L','N')
              Veta='%Yes, I would like to continue with the wizard'
              xpom=5.
              tpom=xpom+CrwgXd+10.
              il=il-10
              do i=1,2
                call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,
     1                              CrwYd,1,1)
                if(i.eq.1) then
                  nCrwYes=CrwLastMade
                  Veta='%No, I shall go by my own way'
                endif
                call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                il=il-7
              enddo
              il=il-3
              call FeQuestLinkaMake(id,il)
              nLinka=LinkaLastMade
              call FeFillTextInfo('em9lebailbeforesgtest.txt',0)
              il=il-10
              call FeQuestLblMake(id,xqd*.5,il,'INFORMATION','C','N')
              nLblFirst=LblLastMade
              il=il-10
              tpom=5.
              do i=1,NInfo
                call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
                il=il-6
              enddo
              nLblLast=LblLastMade
3550          call FeQuestEvent(id,ich)
              if(CheckType.eq.EventCrw) then
                if(CrwLogicQuest(nCrwYes)) then
                  call FeQuestLinkaOn(nLinka)
                else
                  call FeQuestLinkaOff(nLinka)
                endif
                do nLbl=nLblFirst,nLblLast
                  if(CrwLogicQuest(nCrwYes)) then
                    call FeQuestLblOn(nLbl)
                  else
                    call FeQuestLblOff(nLbl)
                  endif
                enddo
                go to 3550
              else if(CheckType.ne.0) then
                call NebylOsetren
                go to 3550
              endif
              if(CrwLogicQuest(nCrwYes)) then
                ich=0
              else
                ich=1
              endif
              call FeQuestRemove(id)
              if(ich.ne.0) go to 9999
              call CrlLeBailMaster(ich)
              if(ich.ne.0) go to 3700
            endif
            call DRMakeMenuRefBlock
            if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1         DifCode(KRefBlock).eq.IdSXD.or.
     2         DifCode(KRefBlock).eq.IdTopaz.or.
     3         DifCode(KRefBlock).eq.IdSCDLANL.or.
     4         DifCode(KRefBlock).eq.IdSENJU) then
              ChangeSGTest=.false.
            else
              call DRSGTest(10,ChangeSGTest)
            endif
          else
            ChangeSGTest=.false.
          endif
          if(isPowder.and.ChangeSGTest) then
            NInfo=1
            TextInfo(1)='The last le Bail refinement has used a lower'//
     1                  ' space group.'
            if(FeYesNoHeader(-1.,-1.,'Do you want refine profile '//
     1                       'parameters once more?',1)) then
              call iom50(0,0,fln(:ifln)//'.m50')
              call Refine(0,RefineEnd)
            endif
          else
            call EM9CreateM90(0,ich)
            call iom90(0,fln(:ifln)//'.m90')
          endif
        endif
3700    WizardMode=.false.
        if(ExistM40.and.NDatBlockIn.gt.0) then
          if(ParentStructure) then
            call SSG2QMag(fln)
            call iom40Only(0,0,fln(:ifln)//'.m40')
          endif
          do i=NDatBlockIn+1,NRefBlock
            if(isPowder.and.(Radiation(i).ne.NeutronRadiation.or.
     1         (TOFSigma(1,i).lt.-3000..and.TOFGamma(1,i).lt.-3000.)))
     2        call SetBasicM41(i)
            sc(1,i)=1.
          enddo
          call iom40(1,0,fln(:ifln)//'.m40')
          if(ParentStructure) call QMag2SSG(fln,0)
        endif
        if(ParentStructure) then
          call iom40Only(0,0,fln(:ifln)//'.m44')
        else
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        if(Klic.eq.0.and.NAtCalc.le.0.and.ich.eq.0) then
          if(isPowder.and.ChangeSGTest) then
          endif
          call FeFillTextInfo('em9runsolution.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          call RunSolution
        endif
      endif
      go to 9999
4000  call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_m95list.tmp')
      if(ExistFile(fln(:ifln)//'.z95')) then
        call MoveFile(fln(:ifln)//'.z95',fln(:ifln)//'.m95')
      else
        call DeleteFile(fln(:ifln)//'.m95')
      endif
      if(ExistFile(fln(:ifln)//'.z90')) then
        call CopyFile(fln(:ifln)//'.z90',fln(:ifln)//'.m90')
      else
        call DeleteFile(fln(:ifln)//'.m90')
      endif
      if(ExistM50) then
        call MoveFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
      endif
      if(ExistM40) then
        call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      endif
      go to 9999
9900  call FeQuestRemove(WizardId)
      WizardMode=.false.
9999  call DeleteFile(fln(:ifln)//'.l95')
      call DeleteFile(fln(:ifln)//'.z95')
      call DeleteFile(fln(:ifln)//'.z90')
      call DeleteFile(fln(:ifln)//'.z50')
      call DeleteFile(fln(:ifln)//'.z40')
      return
      end
      subroutine EM9ImportDatRedData(Navrat)
      use Basic_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'profil.cmn'
      include 'powder.cmn'
      character*256 EdwStringQuest,Veta,p80,mene(25),FileKoala(2)
      character*1   ChNDim
      dimension QSymm(3),TrMP3(3,3),TrMP6(36),xp(3)
      logical ExistFile,FeYesNoHeader,EqIgCase,DelejBasic,
     1        CrwLogicQuest,EqIV0,ObnovCell
      integer Order(25),OrderSel,DatRedData,WhatToDo,EdwStateQuest,
     1        BratJineItw,StavVolani,FeCloseBinaryFile
      real CellParOld(6),CellParSUOld(6),QuOld(3,3)
      equivalence (DatRedData     ,IdNumbers(1)),
     1            (ImportSingle   ,IdNumbers(2)),
     2            (ImportCWPowder ,IdNumbers(3)),
     4            (ImportTOFPowder,IdNumbers(4))
      save NSkip
      Order(1:nDiffTypes)=OrderDiffTypes(1:nDiffTypes)
      call CopyStringArray(mene1,mene,nDiffTypes)
      nCrw=nDiffTypes
      WhatToDo=DatRedData
      go to 1010
      entry EM9ImportSingle(Navrat)
      Order(1:nSingleTypes)=OrderSingleTypes(1:nSingleTypes)
      call CopyStringArray(mene2,mene,nSingleTypes)
      WhatToDo=ImportSingle
      nCrw=nSingleTypes
      go to 1010
      entry EM9ImportCWPowder(Navrat)
      Order(1:nCWPowderTypes)=OrderCWPowderTypes(1:nCWPowderTypes)
      call CopyStringArray(mene3,mene,nCWPowderTypes)
      nCrw=nCWPowderTypes
      WhatToDo=ImportCWPowder
      go to 1010
      entry EM9ImportTOFPowder(Navrat)
      Order(1:nTOFPowderTypes)=OrderTOFPowderTypes(1:nTOFPowderTypes)
      call CopyStringArray(mene4,mene,nTOFPowderTypes)
      nCrw=nTOFPowderTypes
      WhatToDo=ImportTOFPowder
1010  if(Navrat.eq.1) then
        call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted',
     1                'old')
        if(ErrFlag.ne.0) go to 9999
        if(isPowder) then
          do i=1,NSkip
            read(71,FormA) Veta
          enddo
          if(DifCode(KRefBlock).eq.IdDataRiet7) then
            read(71,FormA) Veta
          endif
!   Nevim co pro jine
          go to 1600
        else
          go to 1700
        endif
      endif
1015  if(SilentRun) go to 1080
      if(nCrw.gt.8) then
         nCrwZalom=(nCrw+1)/2+1
      else
         nCrwZalom=nCrw+1
      endif
      if(WhatToDo.eq.DatRedData) then
        Veta='Data reduction file from:'
      else if(WhatToDo.eq.ImportSingle) then
        Veta='Single crystal data from:'
      else if(WhatToDo.eq.ImportCWPowder) then
        Veta='Powder data from:'
      else if(WhatToDo.eq.ImportTOFPowder) then
        nCrwZalom=7
        Veta='TOF/ED powder data from:'
      endif
      id=NextQuestId()
      call FeQuestTitleMake(id,Veta)
      QuestCheck(id)=1
      ilm=nCrwZalom+2
      il=1
      Veta='Bro%wse'
      dpom=FeTxLengthUnder(Veta)+10.
      xpomb=WizardLength-dpom-5.
      call FeQuestButtonMake(id,xpomb,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      Veta='Fi%le name'
      tpom=5.
      xpom=tpom+10.+FeTxLengthUnder(Veta)
      dpom=xpomb-xpom-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      if(WhatToDo.eq.DatRedData) then
        xpom=25.
        Veta='import merged file "'
        do i=1,2
          tpom=xpom+CrwgYd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,0,1)
          if(i.eq.1) then
            nCrwImportMerged=CrwLastMade
            Veta='import unmerged file "'
            xpom=xpom+WizardLength*.5
          else
            nCrwImportUnmerged=CrwLastMade
          endif
        enddo
      else
        nCrwImportMerged=0
        nCrwImportUnmerged=0
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=15.
      tpom=xpom+CrwgYd+10.
      do i=1,nCrw
        if(i.ne.nCrwZalom) then
          il=il+1
        else
          il=3
          xpom=xpom+WizardLength*.5
          tpom=tpom+WizardLength*.5
        endif
        if(WhatToDo.eq.DatRedData) then
          Veta=men1(i)
        else if(WhatToDo.eq.ImportSingle) then
          Veta=men2(i)
        else if(WhatToDo.eq.ImportCWPowder) then
          Veta=men3(i)
        else if(WhatToDo.eq.ImportTOFPowder) then
          Veta=men4(i)
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) nCrwTypeFr=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      Order(i).eq.DifCode(KRefBlock))
        if(Order(i).eq.DifCode(KRefBlock)) OrderSel=i
      enddo
      nCrwTypeTo=CrwLastMade
      if(WhatToDo.eq.ImportSingle) then
        Veta='Inp%ut format:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=WizardLength-xpom-5.
        call FeQuestEdwMake(id,tpom,ilm,xpom,ilm,Veta,'L',dpom,EdwYd,0)
        nEdwFormat=EdwLastMade
      else
        nEdwFormat=0
      endif
      if(WhatToDo.eq.ImportCWPowder) then
        Veta='Show de%tails about the selected format'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=(WizardLength-dpom)*.5
        call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
        nButtPowderDetails=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=15.
        tpom=xpom+CrwgYd+10.
        il=ilm
        do i=1,4
          il=il+1
          if(i.eq.1) then
            Veta='%Debye-Scherrer method'
          else if(i.eq.2) then
            nCrwMethod=CrwLastMade
            Veta='Bragg-Brentanno method - %Fixed Divergence Slit'
          else if(i.eq.3) then
            Veta='Bragg-Brentanno method - %Variable Divergence Slit'
          else if(i.eq.4) then
            Veta='%Another/unknown method'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,0,3)
          call FeQuestCrwOpen(CrwLastMade,
     1                        mod(i,4).eq.PwdMethodRefBlock(KRefBlock))
        enddo
      else
        nButtPowderDetails=0
        nCrwMethod=0
      endif
1020  if(SourceFileRefBlock(KRefBlock).eq.' ') then
        if(KRefBlock.gt.1.and.
     1     DifCode(KRefBlock).eq.DifCode(KRefBlock-1)) then
          SourceFileRefBlock(KRefBlock)=SourceFileRefBlock(KRefBlock-1)
        else
          k=0
          call Kus(mene(OrderSel),k,Cislo)
          SourceFileRefBlock(KRefBlock)=fln(:ifln)//Cislo(:idel(Cislo))
        endif
      endif
1030  if(DifCode(KRefBlock).eq.IdImportGeneralF.or.
     1   DifCode(KRefBlock).eq.IdImportGeneralI) then
        if(EdwStateQuest(nEdwFormat).ne.EdwOpened)
     1    call FeQuestStringEdwOpen(nEdwFormat,
     2                              FormatRefBlock(KRefBlock))
      else
        call FeQuestEdwDisable(nEdwFormat)
      endif
      if(DifCode(KRefBlock).eq.IdKoala) then
        Veta='Laue4_jana.out'
        if(ExistFile(Veta)) then
          ln=NextLogicNumber()
          call OpenFile(ln,'Laue4_jana.out','formatted','old')
          do i=1,3
            read(ln,FormA,err=1035,end=1035)
          enddo
          do i=1,2
            read(ln,FormA,err=1035,end=1035) FileKoala(i)
          enddo
          call CloseIfOpened(ln)
          call FeQuestEdwClose(nEdwFileName)
          call FeQuestButtonClose(nButtBrowse)
          call FeQuestCrwOpen(nCrwImportMerged,.false.)
          Veta='import merged file "'//
     1         FileKoala(1)(:idel(FileKoala(1)))//'"'
          call FeQuestCrwLabelChange(nCrwImportMerged,Veta)
          call FeQuestCrwOpen(nCrwImportUnmerged,.true.)
          Veta='import unmerged file "'//
     1         FileKoala(2)(:idel(FileKoala(2)))//'"'
          call FeQuestCrwLabelChange(nCrwImportUnmerged,Veta)
          go to 1050
1035      call CloseIfOpened(ln)
          go to 1050
        endif
      endif
      if(nCrwImportMerged.gt.0) then
        call FeQuestCrwClose(nCrwImportMerged)
        call FeQuestCrwClose(nCrwImportUnmerged)
      endif
      call FeQuestStringEdwOpen(nEdwFileName,
     1                          SourceFileRefBlock(KRefBlock))
      call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      if(.not.CreateNewRefBlock) call FeQuestButtonDisable(ButtonEsc)
1050  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        if(nCrwImportMerged.gt.0) then
          if(DifCode(KRefBlock).eq.IdKoala) then
            if(CrwLogicQuest(nCrwImportMerged)) then
              SourceFileRefBlock(KRefBlock)=FileKoala(1)
            else
              SourceFileRefBlock(KRefBlock)=FileKoala(2)
            endif
          else
            SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
          endif
        else
          SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
        endif
        if(ExistFile(SourceFileRefBlock(KRefBlock))) then
          QuestCheck(id)=0
        else
          call FeChybne(-1.,-1.,'the input file "'//
     1                  SourceFileRefBlock(KRefBlock)(:
     2                    idel(SourceFileRefBlock(KRefBlock)))//
     3                  '" does not exist',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFileName
        endif
        go to 1050
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwTypeFr.and.
     1        CheckNumber.le.nCrwTypeTo) then
        if(Order(OrderSel).eq.IdRigakuCCD) then
          Veta='shelx.hkl'
        else
          k=0
          call Kus(mene(OrderSel),k,Cislo)
          Veta=fln(:ifln)//Cislo(:idel(Cislo))
        endif
        OrderSel=CheckNumber-nCrwTypeFr+1
        DifCode(KRefBlock)=Order(OrderSel)
        if(EqIgCase(EdwStringQuest(nEdwFileName),Veta)) then
          if(Order(OrderSel).eq.IdRigakuCCD) then
            Veta='shelx.hkl'
          else
            k=0
            call Kus(mene(OrderSel),k,Cislo)
            Veta=fln(:ifln)//Cislo(:idel(Cislo))
          endif
          SourceFileRefBlock(KRefBlock)=Veta
        endif
        go to 1030
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        Veta='Browse for the data collection file'
        SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
        p80=' '
        k=0
1070    call Kus(mene(OrderSel),k,Cislo)
        p80=p80(:idel(p80))//' *'//Cislo(:idel(Cislo))
        if(k.lt.len(mene(OrderSel))) go to 1070
        call FeFileManager(Veta,SourceFileRefBlock(KRefBlock),p80,0,
     1                     .true.,ich)
        go to 1020
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPowderDetails) then
        i=Order(OrderSel)-100
        call FeFillTextInfo('pwddataformats.txt',i)
        if(NInfo.le.0) then
          call FeChybne(-1.,YBottomMessage,
     1                  'information not yet distributed.',' ',
     1                  WarningWithESC)
        else
          call FeInfoOut(-1.,-1.,' ','L')
        endif
        go to 1050
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1030
      endif
      if(ich.eq.0) then
        if(nEdwFormat.ne.0) then
          if(DifCode(KRefBlock).ne.IdImportGraindex.and.
     1       DifCode(KRefBlock).ne.IdImportCIF)
     2      FormatRefBlock(KRefBlock)=EdwStringQuest(nEdwFormat)
        endif
        if(nCrwMethod.ne.0) then
          nCrwP=nCrwMethod
          do i=1,4
            if(CrwLogicQuest(nCrwP)) then
              PwdMethodRefBlock(KRefBlock)=mod(i,4)
              exit
            endif
            nCrwP=nCrwP+1
          enddo
        endif
      endif
      if(ich.ne.0) then
        call EM9ImportRestorePar
        if(ich.gt.0) then
          ErrFlag=-1
        else
          ErrFlag= 1
        endif
        go to 9999
      endif
1080  call CheckEOLOnFile(SourceFileRefBlock(KRefBlock),2)
      if(ErrFlag.ne.0) go to 9000
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted','old')
      if(ErrFlag.ne.0) go to 9000
      RealIndices=.false.
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        if(DifCode(KRefBlock).ne.IdImportCIF.and.
     1     DifCode(KRefBlock).ne.IdPets.and.
     2     DifCode(KRefBlock).ne.IdImportSHELXF.and.
     3     DifCode(KRefBlock).ne.IdImportSHELXI.and.
     4     DifCode(KRefBlock).ne.IdImportHKLF5.and.
     5     DifCode(KRefBlock).ne.IdImportGraindex.and.
     6     DifCode(KRefBlock).ne.IdImportXD.and.
     7     DifCode(KRefBlock).ne.IdSENJU.and.
     8     DifCode(KRefBlock).ne.Id6T2LBB) then
          call DRTestNDim(NDim95(KRefBlock))
          if(NDim95(KRefBlock).lt.3) then
            NDim95(KRefBlock)=3
            RealIndices=.true.
          endif
        endif
      endif
      NBankActual=0
      DelejBasic=.true.
      FormulaRefBlock=' '
      if(.not.ExistM50) then
        NTwin=1
        NComp(KPhaseDR)=1
        call UnitMat(zv (1,1,KPhaseDR),NDim(KPhaseDR))
        call UnitMat(zvi(1,1,KPhaseDR),NDim(KPhaseDR))
        KPhaseTwin(1)=KPhaseDR
      endif
1090  if(WhatToDo.eq.DatRedData) then
        PocitatUhly=DifCode(KRefBlock).ne.IdSiemensP4.and.
     1              DifCode(KRefBlock).ne.IdCAD4.and.
     2              DifCode(KRefBlock).ne.IdD9ILL.and.
     3              DifCode(KRefBlock).ne.IdHasylabF1.and.
     4              DifCode(KRefBlock).ne.IdHasylabHuber.and.
     5              DifCode(KRefBlock).ne.IdKumaPD.and.
     6              DifCode(KRefBlock).ne.IdSXD.and.
     7              DifCode(KRefBlock).ne.IdTopaz.and.
     8              DifCode(KRefBlock).ne.IdVivaldi.and.
     9              DifCode(KRefBlock).ne.IdSENJU.and.
     a              DifCode(KRefBlock).ne.IdSCDLANL.and.
     1              DifCode(KRefBlock).ne.IdKoala
        Profil=.false.
        ThOmRatio=2.
        call SetRealArrayTo(SenseOfAngle,4,-1.)
        SenseOfAngle(2)=1.
        NDimP=NDim95(KRefBlock)
        if(DifCode(KRefBlock).eq.IdCAD4) then
1100      read(71,FormA) Veta
1110      if(idel(Veta).lt.4) go to 1100
          if(ichar(Veta(1:1)).lt.32) then
            Veta=Veta(2:)
            go to 1110
          endif
          if(Veta(1:4).eq.'    ') go to 1100
          jentri=Veta(4:4).eq.' '
          rewind 71
          call DRBasicCad
        else if(DifCode(KRefBlock).eq.IdSiemensP4) then
          call DRBasicP4
        else if(DifCode(KRefBlock).eq.IdIPDSStoe) then
          call DRBasicIpStoe
        else if(DifCode(KRefBlock).eq.IdD9ILL) then
          WizardMode=.false.
          call DRBasicMonstrum
          WizardMode=.true.
          NDimP=NDim95(KRefBlock)
        else if(DifCode(KRefBlock).eq.IdHasyLabF1) then
          call DRBasicMonstrum
        else if(DifCode(KRefBlock).eq.IdHasyLabHuber) then
          call DRBasicMonstrum
        else if(DifCode(KRefBlock).eq.IdKumaCCD) then
          call DRBasicKumaCCD
        else if(DifCode(KRefBlock).eq.IdKumaPD) then
          call DRBasicKumaPD
        else if(DifCode(KRefBlock).eq.IdNoniusCCD) then
          call DRBasicNoniusCCD
        else if(DifCode(KRefBlock).eq.IdBrukerCCD) then
          call DRBasicBrukerCCD
          if(ExistM50.and.StatusM50.lt.10000)
     1      call iom50(0,0,fln(:ifln)//'.m50')
        else if(DifCode(KRefBlock).eq.IdSXD) then
          call DRBasicSXD
        else if(DifCode(KRefBlock).eq.IdTopaz) then
          call DRBasicTopaz
        else if(DifCode(KRefBlock).eq.IdKoala) then
          call DRBasicKoala
        else if(DifCode(KRefBlock).eq.IdSENJU) then
          call DRBasicSENJU
        else if(DifCode(KRefBlock).eq.IdVivaldi) then
          call DRBasicVivaldi
        else if(DifCode(KRefBlock).eq.IdSCDLANL) then
          call DRBasicSCDLANL
        else if(DifCode(KRefBlock).eq.IdXDS) then
          call DRBasicXDS
        else if(DifCode(KRefBlock).eq.IdRigakuCCD) then
          call DRBasicRigakuCCD
        else if(DifCode(KRefBlock).eq.IdPets) then
          call DRBasicPets
        else if(DifCode(KRefBlock).eq.Id6T2LBB) then
          call DRBasic6T2LBB
        else if(DifCode(KRefBlock).eq.IdSHELXINoAbsCorr) then
          call DRBasicSHELXINoAbsCorr
        else if(DifCode(KRefBlock).eq.IdPolNeutrons) then
          call DRPolNeutrons
        else
          FormatRefBlock(KRefBlock)='(.i4,2f8.4,i4,6f8.5)'
          write(FormatRefBlock(KRefBlock)(2:2),101) NDim95(KRefBlock)
        endif
        if(ReimportRefBlock) NDim95(KRefBlock)=NDimP
        if(ErrFlag.ne.0) then
          ErrFlag=0
          call CloseIfOpened(71)
          go to 1015
        endif
      else if(WhatToDo.eq.ImportSingle) then
        PocitatDirCos=.false.
        PocitatUhly=.true.
        if(DifCode(KRefBlock).eq.IdImportCIF) then
          call DRBasicRefCIF(71,0)
        else if(DifCode(KRefBlock).eq.IdImportXD) then
          call DRBasicRefXD
        else if(DifCode(KRefBlock).eq.IdImportGraindex) then
          call DRBasicRefGraindex
        else if(DifCode(KRefBlock).eq.IdImportFullProf) then
          call DRBasicFullProf
        else if(DifCode(KRefBlock).eq.IdImportDABEX) then
          call DRBasicDABEX
        else
          if(DifCode(KRefBlock).ne.IdImportGeneralF.and.
     1       DifCode(KRefBlock).ne.IdImportGeneralI) then
            FormatRefBlock(KRefBlock)='(.i4,2f8.2,i4,6f8.5)'
            write(FormatRefBlock(KRefBlock)(2:2),101) NDim95(KRefBlock)
            if(DifCode(KRefBlock).eq.IdImportHKLF5.and.
     1         (.not.ReimportRefBlock.or.HKLF5RefBlock(KRefBlock).eq.0))
     2        HKLF5RefBlock(KRefBlock)=1
          endif
          call Zhusti(FormatRefBlock(KRefBlock))
          call mala(FormatRefBlock(KRefBlock))
        endif
        if(ErrFlag.ne.0) then
          ErrFlag=0
          call CloseIfOpened(71)
          go to 1015
        endif
        NDimP=NDim95(KRefBlock)
      else if(WhatToDo.eq.ImportCWPowder.or.
     1        WhatToDo.eq.ImportTOFPowder) then
        NImpPwd=1
        if(DifCode(KRefBlock).eq.IdDataMAC.or.
     1     DifCode(KRefBlock).eq.IdDataCPI) then
          NSkip=1
        else if(DifCode(KRefBlock).eq.IdDataPSI.or.
     1          DifCode(KRefBlock).eq.IdDataG41) then
          NSkip=0
          PwdInputInt=.true.
        else if(DifCode(KRefBlock).eq.IdDataRiet7) then
          NSkip=0
1410      read(71,FormA) Veta
          k=0
          call StToReal(Veta,k,xp,3,.false.,ich)
          if(ich.ne.0) then
            NSkip=NSkip+1
            if(NSkip.lt.6) then
              go to 1410
            else
              NSkip=4
            endif
          endif
          rewind 71
        else
          NSkip=0
        endif
        do i=1,NSkip
          read(71,FormA,end=1510) Veta
        enddo
        go to 1520
1510    call FeChybne(-1.,-1.,'the file does not contain relevant '//
     1                'information',' ',SeriousError)
        go to 1010
1520    NBankActual=NBankActual+1
        if(NBankActual.eq.1) then
          call PwdBasic(ich)
          IncSpectFileOpened=.false.
          if(ich.ne.0) then
            ErrFlag=0
            go to 1015
          endif
          if(DifCode(KRefBlock).eq.IdTOFGSAS.and.
     1       IncSpectFileRefBlock(KRefBlock).ne.' ') then
            call OpenFile(72,IncSpectFileRefBlock(KRefBlock),
     1                    'formatted','old')
            IncSpectFileOpened=.true.
          endif
        endif
        call PwdSetBank(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 5000
        endif
        NDimP=NDim95(KRefBlock)
      endif
      RadiationDetails=DifCode(KRefBlock).ne.IdD9ILL.and.
     1                 DifCode(KRefBlock).ne.IdSXD.and.
     2                 DifCode(KRefBlock).ne.IdTopaz.and.
     3                 DifCode(KRefBlock).ne.IdSENJU.and.
     4                 DifCode(KRefBlock).ne.IdHasyLabF1.and.
     5                 DifCode(KRefBlock).ne.IdHasyLabHuber.and.
     6                 DifCode(KRefBlock).ne.IdDataD1AD2B.and.
     7                 DifCode(KRefBlock).ne.IdDataD1AD2BOld.and.
     8                 DifCode(KRefBlock).ne.IdDataSaclay.and.
     9                 DifCode(KRefBlock).ne.IdDataPSI.and.
     a                 DifCode(KRefBlock).ne.IdImportFullProf.and.
     1                 DifCode(KRefBlock).ne.IdVivaldi.and.
     2                 DifCode(KRefBlock).ne.IdSCDLANL.and.
     3                 DifCode(KRefBlock).ne.IdData11BM.and.
     4                 DifCode(KRefBlock).ne.IdKoala
      RadiationDetails=RadiationDetails.and.
     1  (RadiationRefBlock(KRefBlock).eq.XRayRadiation.or.
     2   DifCode(KRefBlock).gt.200)
      if(CreateNewRefBlock) then
        if(RadiationDetails.and.DifCode(KRefBlock).lt.200) then
          AngleMonRefBlock(KRefBlock)=
     1      CrlMonAngle(MonCell(1,3),MonH(1,3),
     2                  LamAveRefBlock(KRefBlock))
        else
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        endif
      endif
      if(.not.ReimportRefBlock.and.KRefBlock.eq.1) NDimP=0
      StavVolani=0
1600  if(DelejBasic) then
        NDimPom=NDim95(KRefBlock)
        call EM9CompleteBasic(StavVolani,ich)
        if(ich.ne.0) then
          if(ich.gt.0) then
            call CloseIfOpened(70)
            call CloseIfOpened(71)
            Navrat=0
            go to 1010
          else
            ErrFlag=-1
            go to 9000
          endif
        endif
        if(NDimPom.ne.NDim95(KRefBlock)) then
          write(ChNDim,'(i1)') NDim95(KRefBlock)
          if(DifCode(KRefBlock).ne.IdImportGeneralF.and.
     1       DifCode(KRefBlock).ne.IdImportGeneralI) then
            FormatRefBlock(KRefBlock)='('//ChNDim//'i4,'
            i=idel(FormatRefBlock(KRefBlock))
            FormatRefBlock(KRefBlock)=FormatRefBlock(KRefBlock)(:i)//
     1                                '2f8.2,i4,6f8.5)'
          endif
          call Zhusti(FormatRefBlock(KRefBlock))
          call mala(FormatRefBlock(KRefBlock))
        endif
      endif
      if(CellRefBlock(1,0).lt.0.) then
        call CopyVek(CellRefBlock(1,KRefBlock),CellRefBlock(1,0),6)
        call CopyVek(CellRefBlockSU(1,KRefBlock),CellRefBlockSU(1,0),6)
        call CopyVek(CellRefBlock(1,0),CellPar(1,1,KPhaseDR),6)
        call CopyVek(CellRefBlockSU(1,0),CellParSU(1,1,KPhaseDR),6)
      else if(isPowder.and.KRefBlock.le.1) then
        call CopyVek(CellRefBlock(1,0),CellPar(1,1,KPhaseDR),6)
      endif
      do i=1,3
        if(QuRefBlock(1,i,0).lt.-300.) then
          call CopyVek(QuRefBlock(1,i,KRefBlock),QuRefBlock(1,i,0),3)
          call CopyVek(QuRefBlock(1,i,0),Qu(1,i,1,KPhaseDR),3)
        else if(isPowder.and.KRefBlock.le.1) then
          call CopyVek(QuRefBlock(1,i,0),Qu(1,i,1,KPhaseDR),3)
        endif
      enddo
      if(maxNDim.ne.NDimP.and..not.ReimportRefBlock) then
        call UnitMat(TrMP ,maxNDim)
        call UnitMat(TrMPI,maxNDim)
        if(.not.ExistM50) then
          call UnitMat(zv (1,1,KPhaseDR),maxNDim)
          call UnitMat(zvi(1,1,KPhaseDR),maxNDim)
        endif
        NDimP=maxNDim
      endif
1700  ObnovCell=.false.
      if(ExistM50) then
        CellParOld(1:6)=CellPar(1:6,1,KPhaseDR)
        CellParSUOld(1:6)=CellParSU(1:6,1,KPhaseDR)
        CellParSU(1:6,1,KPhaseDR)=CellRefBlockSU(1:6,0)
        maxNDimOld=maxNDim
        if(maxNDim.gt.3) then
          call CopyVek(Qu(1,1,1,KPhaseDR),QuOld,3*NDimI(KPhaseDR))
          call CopyVek(QuRefBlock(1,1,0),Qu(1,1,1,KPhaseDR),
     1                 3*NDimI(KPhaseDR))
        endif
        ObnovCell=.true.
      endif
      if(.not.isPowder.and..not.SilentRun) then
        call EM9CompleteTrans(StavVolani,ich)
        if(ich.ne.0) then
          if(ich.gt.0) then
            go to 1600
          else
            ErrFlag=-1
            go to 9000
          endif
        endif
      endif
      if(maxNDim.ne.NDimP.and..not.ReimportRefBlock) then
        call UnitMat(TrMP ,maxNDim)
        call UnitMat(TrMPI,maxNDim)
        if(.not.ExistM50) then
          call UnitMat(zv (1,1,KPhaseDR),maxNDim)
          call UnitMat(zvi(1,1,KPhaseDR),maxNDim)
        endif
        NDimP=maxNDim
      endif
      ModifiedRefBlock(KRefBlock)=.true.
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      nread=0
      if(ReimportRefBlock) NRef95Old=NRef95(KRefBlock)
      NRef95(KRefBlock)=0
      NLines95(KRefBlock)=0
      Veta='00000000'
      UseTabs=NextTabs()
      call FeTabsAdd(FeTxLength(Veta),UseTabs,IdLeftTab,' ')
      call FeTabsAdd(FeTxLengthSpace(Veta(:8)//' '),UseTabs,IdRightTab,
     1               ' ')
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        CellPar(1:6,1,KPhaseDR)=CellRefBlock(1:6,0)
        call CopyVek(QuRefBlock(1,1,0),Qu(1,1,1,KPhaseDR),
     1               3*NDimI(KPhaseDR))
        call DRSetCell(0)
        CellVol(1,KPhaseDR)=TrToOrtho(1,1,KPhaseDR)*
     1                      TrToOrtho(5,1,KPhaseDR)*
     1                      TrToOrtho(9,1,KPhaseDR)
        write(Format95(5:5),'(i1)') maxNDim
        if(ub(1,1,KRefBlock).gt.-300.)
     1    call matinv(ub(1,1,KRefBlock),ubi(1,1,KRefBlock),pom,3)
        Veta=Tabulator//'       0'//Tabulator//
     1       'reflections already read'
        call FeTxOut(-1.,-1.,Veta)
        expold=0.
        addtime=0.
        expos0=-1.
        call SetRealArrayTo(uhly,4,0.)
        call SetRealArrayTo(dircos,6,0.)
        call SetRealArrayTo(corrf,2,1.)
        call SetIntArrayTo(iflg,2,1)
        iflg(3)=0
        tbar=0.
        noa=0
        IntFromProfile=DifCode(KRefBlock).eq.IdSiemensP4.or.
     1                 DifCode(KRefBlock).eq.IdKumaPD
        KProf=0
        NProf=0
        NExt(1)=0
        NExtObs(1)=0
        NExt500(1)=0
        RIExtMax(1)=0.
        SumRelI(1)=0.
        ReadLam=0.
        RunCCDMax(KRefBlock)=0
        ScMaxRead(KRefBlock)=0
        BratJineItw=-1
        FreeFormat=index(FormatRefBlock(KRefBlock),'*').gt.0
2000    nread=nread+1
2010    call DRReadRef(ich)
        if(KProf.eq.1) call CopyVekI(IProf(1,1),IProf(1,2),NProf)
        Konec=0
        if(ich.eq.1) then
          Konec=1
          go to 2020
        else if(ich.eq.2) then
          go to 4000
        else if(ich.eq.3) then
          call FeTabsReset(UseTabs)
          go to 5000
        else if(ich.eq.4) then
          call FeTabsReset(UseTabs)
          go to 5000
        else if(ich.eq.5) then
          go to 2000
        endif
        if(maxNDim.gt.3) then
          if(ImportOnlySatellites(KRefBlock).and.
     1       EqIV0(ih(4),maxNDim-3)) go to 2000
        endif
        if(HKLF5RefBlock(KRefBlock).ne.1) then
          if(iflg(2).lt.ITwRead(KRefBlock)) then
            if(BratJineItw.lt.0) then
              call FeFillTextInfo('em9importdatreddata1.txt',0)
              if(FeYesNoHeader(-1.,-1.,'Do you want to suppress '//
     1                         'possibly duplicate reflections?',1))
     2          then
                BratJineItw=0
              else
                BratJineItw=1
              endif
            endif
            if(BratJineItw.eq.0) go to 2000
          endif
        endif
        if(rs.le.0.) go to 2010
        nref95(KRefBlock)=nref95(KRefBlock)+1
        if(WhatToDo.eq.ImportSingle.or.DifCode(KRefBlock).eq.IdPets)
     1    no=nref95(KRefBlock)
        ScMaxRead(KRefBlock)=max(ScMaxRead(KRefBlock),iflg(1))
        ri=ri*ScaleRefBlock(KRefBlock)
        rs=rs*ScaleRefBlock(KRefBlock)
        call DRPutReflectionToM95(95,nl)
        NLines95(KRefBlock)=NLines95(KRefBlock)+nl
2020    if(mod(nread,50).eq.0) then
          write(Veta(2:9),100) nread
          call FeTxOutCont(Veta)
        endif
        if(Konec.eq.0) then
          go to 2000
        endif
      else
        if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        nalloc=10000
        allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
        if(IncSpectFileOpened) then
          NPnts=0
          NInel=0
          ipointA=0
          iorderA=0
          ipointI=0
          iorderI=0
          p80=TypeBank
          Veta=FormatBank
          TypeBank=TypeBankInc
          FormatBank=FormatBankInc
3050      nread=nread+1
          call PwdImport(72,Konec,nalloc,ich)
          if(ich.ne.0) then
            ErrFlag=1
            call FeTabsReset(UseTabs)
            go to 5000
          endif
          if(Konec.eq.0) go to 3050
          TypeBank=p80
          FormatBank=Veta
          nread=0
        endif
        Veta=Tabulator//'       0'//Tabulator//'records already read'
        call FeTxOut(-1.,-1.,Veta)
        NPnts=0
        NInel=0
        ipointA=0
        iorderA=0
        ipointI=0
        iorderI=0
3100    nread=nread+1
        call PwdImport(71,Konec,nalloc,ich)
        if(ich.ne.0) then
          ErrFlag=1
          call FeTabsReset(UseTabs)
          go to 5000
        endif
        if(mod(nread,50).eq.0) then
          write(Veta(2:9),100) nread
          call FeTxOutCont(Veta)
        endif
        if(Konec.eq.0) go to 3100
        if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1     DifCode(KRefBlock).eq.IdTOFISIST.or.
     2     (DifCode(KrefBlock).eq.IdTOFGSAS.and.LSLOG).or.
     3     (DifCode(KrefBlock).eq.IdTOFGSAS.and.LRALF)) then
          if(DifCode(KRefBlock).ne.IdTOFISISD) then
            do i=1,NPnts
              if(i.lt.NPnts) then
                pom=1./(XPwd(i+1)-XPwd(i))
              else
                pom=1./(XPwd(i)-XPwd(i-1))
              endif
              YoPwd(i)=YoPwd(i)*pom
              if(IncSpectFileOpened) YiPwd(i)=YiPwd(i)*pom
              YsPwd(i)=YsPwd(i)*pom
            enddo
          endif
          pom=0.
          pomi=0.
          do i=1,NPnts
            pom=max(pom,YoPwd(i))
            if(IncSpectFileOpened) pomi=max(pomi,YiPwd(i))
          enddo
          if(pom.lt.100..or.pom.gt.10000.) then
            pom=1000./pom
            do i=1,NPnts
              YoPwd(i)=YoPwd(i)*pom
              YsPwd(i)=YsPwd(i)*pom
            enddo
          endif
          if(IncSpectFileOpened) then
            do i=1,NPnts
              YiPwd(i)=YiPwd(i)/pomi
            enddo
          endif
        endif
        call PwdMakeFormat92
        IncSpectNorm=.false.
        call PwdPutRecordToM95(95)
        if(NBankActual.lt.NBanks) then
          nread=nread-1
          write(Veta(2:9),100) nread
          call FeTxOutEnd
          call FeReleaseOutput
          call FeDeferOutput
          KRefBlock=KRefBlock+1
          NRefBlock=max(NRefBlock,KRefBlock)
          call SetBasicM95(KRefBlock)
          write(Cislo,'(''?bank#'',i5)') NBankActual+1
          call Zhusti(Cislo)
          SourceFileRefBlock(KRefBlock)=Cislo
          if(IncSpectFileOpened) IncSpectFileRefBlock(KRefBlock)=Cislo
          SourceFileDateRefBlock(KRefBlock)=
     1      SourceFileDateRefBlock(KRefBlock-1)
          SourceFileTimeRefBlock(KRefBlock)=
     1      SourceFileTimeRefBlock(KRefBlock-1)
          DifCode(KRefBlock)=DifCode(KRefBlock-1)
          PolarizationRefBlock(KRefBlock)=
     1      PolarizationRefBlock(KRefBlock-1)
          LamAveRefBlock(KRefBlock)=LamAveRefBlock(KRefBlock-1)
          LamA1RefBlock(KRefBlock)=LamA1RefBlock(KRefBlock-1)
          LamA2RefBlock(KRefBlock)=LamA2RefBlock(KRefBlock-1)
          LamRatRefBlock(KRefBlock)=LamRatRefBlock(KRefBlock-1)
          KLamRefBlock(KRefBlock)=KLamRefBlock(KRefBlock-1)
          NAlfaRefBlock(KRefBlock)=NAlfaRefBlock(KRefBlock-1)
          write(Cislo,'(''.l'',i2)') KRefBlock
          if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
          RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
          call DeleteFile(RefBlockFileName)
          DelejBasic=DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1               DifCode(KRefBlock).eq.IdTOFISIST
          go to 1090
        endif
      endif
4000  nread=nread-1
      write(Veta(2:9),100) nread
      call FeTxOutCont(Veta)
      call FeTxOutEnd
      call FeTabsReset(UseTabs)
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        if(NExt500(1).eq.0) then
          NInfo=1
          write(Cislo,100) nread
          call Zhusti(Cislo)
          TextInfo(1)='All '//Cislo(:idel(Cislo))//
     1                ' input reflections were properly handled'
          Veta='INFORMATION'
        else
          if(NExt(1).ne.0) then
            SumRelI(1)=SumRelI(1)/float(NExt(1))
          else
            SumRelI(1)=0.
          endif
          write(TextInfo(1),'(''n(all) :'',i8,'' n(obs) :'',i8)')
     1      NExt(1),NExtObs(1)
          write(TextInfo(2),'(''Average(I/Sig(I)) : '',f5.2)')
     1          SumRelI(1)
          if(NExt500(1).ne.500) call Indexx(NExt500(1),ria(1,1),
     1                                      OrderExtRef(1,1))
          TextInfo(3)='List of the strongest ones:'
          NInfo=4
          if(UseTrRefBlock(KRefBlock)) then
            TextInfo(NInfo)='Indices are related to the original cell'//
     1                      ' !!!'
            NInfo=NInfo+1
          endif
          if(RealIndices) then
            write(Veta,'(3(7x,a1))')(indices(i),i=1,3)
            xpom=FeTxLength('0000.000')
            n=6
          else
            write(Veta,EM9Form1)(indices(i),i=1,NDim95(KRefBlock))
            xpom=FeTxLength('0000')
            n=NDim95(KRefBlock)+3
          endif
          Veta(idel(Veta)+1:)='       I      sig(I)  I/sig(I)'
          k=0
          p80=' '
          do i=1,n
            call kus(Veta,k,Cislo)
            if(i.eq.1) then
              p80=Tabulator//Cislo
            else
              p80=p80(:idel(p80))//Tabulator//Cislo
            endif
          enddo
          TextInfo(NInfo)=p80
          UseTabs=NextTabs()
          xx=0.
          do i=1,NDim95(KRefBlock)
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          xpom=FeTxLength('0000000.0')
          do i=1,3
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          do i=1,min(NExt500(1),15)
            k=OrderExtRef(i,1)
            if(RealIndices) then
              write(Veta,'(3f8.3)')(HExt(j,k,1),j=1,NDim95(KRefBlock))
            else
              write(Veta,EM9Form2)(IHExt(j,k,1),j=1,NDim95(KRefBlock))
            endif
            pom1=-float(ria(k,1))*.0001
            pom2= float(rsa(k,1))*.01
            write(Veta(idel(Veta)+1:),'(1x,3f9.1)')
     1        pom1*pom2,pom2,pom1
            k=0
            p80=' '
            do j=1,n
              call kus(Veta,k,Cislo)
              if(j.eq.1) then
                p80=Tabulator//Cislo
              else
                p80=p80(:idel(p80))//Tabulator//Cislo
              endif
            enddo
            NInfo=NInfo+1
            TextInfo(NInfo)=p80
          enddo
          Veta='Summary of reflections which couldn''t be imported'
        endif
        call FeInfoOut(-1.,-1.,Veta,'L')
        call FeTabsReset(UseTabs)
      endif
      if(WhatToDo.eq.DatRedData) then
        if(DifCode(KRefBlock).eq.IdSXD.or.
     1     DifCode(KRefBlock).eq.IdTopaz.or.
     2     DifCode(KRefBlock).eq.IdSENJU.or.
     3     DifCode(KRefBlock).eq.IdSCDLANL.or.
     4     DifCode(KRefBlock).eq.IdVivaldi.or.
     5     DifCode(KRefBlock).eq.IdKoala) then
          CorrAbs(KRefBlock)=-1
        else
          CorrAbs(KRefBlock)=0
        endif
        if(DifCode(KRefBlock).eq.IdCAD4.or.
     1     DifCode(KRefBlock).eq.IdSiemensP4.or.
     2     DifCode(KRefBlock).eq.IdKumaPD) then
          CorrLp(KRefBlock)= 0
        else
          CorrLp(KRefBlock)=-1
        endif
      else
        CorrAbs(KRefBlock)=-1
        CorrLp(KRefBlock) =-1
      endif
      if(ExistM50) then
        if((ReimportRefBlock.or.ObnovCell).and.KRefBlock.le.1) then
          call MatBlock3(TrMP,TrMP3,maxNDim)
          call UnitMat(TrMP6,maxNDim)
          call DRMatTrCellQ(TrMP3,CellPar(1,1,KPhaseDR),
     1                      qui(1,1,KPhaseDR),quir(1,1,KPhaseDR),TrMP6)
          call SetMet(0)
          maxNDimI=max(maxNDimI,NDimI(KPhaseDR))
          if(ObnovCell.and.maxNDimOld.ne.maxNDim) then
            if(NDim(KPhaseDR).gt.3) then
              call SetIntArrayTo(kw(1,1,KPhaseDR),3*mxw,0)
              if(NDim(KPhaseDR).eq.4) then
                j=mxw
              else
                j=NDimI(KPhaseDR)
              endif
              do i=1,j
                kw(mod(i-1,NDimI(KPhaseDR))+1,i,KPhaseDR)=
     1                                           (i-1)/NDimI(KPhaseDR)+1
              enddo
            endif
            call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                       NLattVec(KPhase),NComp(KPhase),NPhase,1)
            call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QSymm,ich)
            if(ExistM40) then
              call iom40(0,0,fln(:ifln)//'.m40')
              call iom40(1,0,fln(:ifln)//'.m40')
            endif
          endif
          call EM50CellSymmTest(1,ich)
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
      else
        if(StatusM50.ge.10000) then
          if(NDim(KPhaseDR).ne.4) then
            Grupa(KPhaseDR)='P1'
          else
            Grupa(KPhaseDR)='P1(abg)'
          endif
          call AllocateSymm(1,1,1,1)
          call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QSymm,ich)
          Radiation(1)=RadiationRefBlock(1)
          LamAve(1)=LamAveRefBlock(1)
          call iom50(1,0,fln(:ifln)//'.m50')
        else if(Formula(KPhaseDR).eq.' ') then
          Formula(KPhaseDR)=FormulaRefBlock
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(DifCode(KRefBlock).eq.IdPets) then
        call iom42(1,0,fln(:ifln)//'.m42')
        ExistM42=.true.
      endif
      ModifiedRefBlock(KRefBlock)=.true.
      go to 9999
5000  call FeTxOutEnd
      if(ich.eq.3) then
        call FeReadError(71)
        if(ReimportRefBlock) NRef95(KRefBlock)=NRef95Old
        ErrFlag=1
      else if(ich.eq.4) then
        if(ReimportRefBlock) NRef95(KRefBlock)=NRef95Old
        ErrFlag=1
      endif
      go to 9999
9000  call EM9ImportRestorePar
9999  if(DifCode(KRefBlock).eq.IdDataXRDML) then
        close(71,status='delete')
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        i=FeCloseBinaryFile(LnStoe)
      else
        call CloseIfOpened(71)
        call CloseIfOpened(72)
      endif
      call CloseIfOpened(95)
      if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(TMapTOF)) deallocate(TMapTOF,ClckWdtTOF,
     1                                  NPointsTimeMaps)
      return
100   format(i8)
101   format(i1)
      end
      subroutine EM9CheckRefBlock(Void)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer SbwItemPointerQuest
      common/EM9CheckCommon/ nSbw,nButtInfo,nButtReimport,nButtModify,
     1                       nButtDelete,nButtUndelete
      save /EM9CheckCommon/
      external Void
      i=SbwItemPointerQuest(nSbw)
      if(i.eq.0) go to 9999
      if(NRef95(i).gt.0) then
        call FeQuestButtonDisable(nButtUndelete)
        call FeQuestButtonOff(nButtDelete)
        call FeQuestButtonOff(nButtModify)
      else
        call FeQuestButtonDisable(nButtDelete)
        call FeQuestButtonOff(nButtUndelete)
        call FeQuestButtonDisable(nButtModify)
      endif
      if(SourceFileRefBlock(i)(1:1).eq.'?') then
        call FeQuestButtonDisable(nButtReimport)
      else
        call FeQuestButtonOff(nButtReimport)
      endif
9999  return
      end
      subroutine EM9CompleteBasic(StavVolani,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*256 EdwStringQuest
      character*80  Veta
      character*22  LabelRatio
      character*21  LabelT
      character*11  LabelWaveLength
      character*14  LabelWaveLength1
      character*2   nty
      integer FeMenu,EdwStateQuest,LblStateQuest,Polarization,
     1        StavVolani,UzTuByl
      real xp(6)
      logical CrwLogicQuest,lpom,SelectRadiation
      equivalence (nEdwZero,nEdwZeroT),(nEdwDIFC,nEdwDT),
     1            (nEdwDIFA,nEdwAT)
      data LabelRatio/'%I(#2)/I(#1)'/,
     1     LabelWaveLength /'Wave length'/,
     2     LabelWaveLength1/'Wave length #1'/
      data LabelT/'%Number of domains'/
      if(StavVolani.ge.0) then
        UzTuByl=mod(StavVolani,10)
      else
        UzTuByl=0
      endif
      SelectRadiation=DifCode(KRefBlock).lt.0.or.
     1                (DifCode(KRefBlock).gt.100.and.
     2                 DifCode(KRefBlock).ne.IdDataD1AD2B.and.
     3                 DifCode(KRefBlock).ne.IdDataD1AD2BOld.and.
     4                 DifCode(KRefBlock).ne.IdDataD1BD20.and.
     5                 DifCode(KRefBlock).ne.IdDataSaclay.and.
     6                 DifCode(KRefBlock).ne.IdDataPSI.and.
     7                 DifCode(KRefBlock).ne.IdData11BM.and.
     8                 DifCode(KRefBlock).le.200)
      id=NextQuestId()
      if(NBanks.gt.1) then
        write(Cislo,'(i5)') NBankActual
        call Zhusti(Cislo)
        Cislo=' for bank#'//Cislo(:idel(Cislo))
      else
        Cislo=' '
      endif
      Veta='Complete/correct experimental parameters'
      if(Cislo.ne.' ') Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestTitleMake(id,Veta)
      if(SilentRun) then
        ich=0
        go to 9999
      endif
      il=1
      tpom=5.
      if(isPowder.and.KRefBlock.le.1) then
        CellReadIn(KRefBlock)=CellRefBlock(1,0).gt.0.
      else
        CellReadIn(KRefBlock)=CellRefBlock(1,KRefBlock).gt.0.
      endif
      if(isPowder.and.KRefBlock.gt.1) then
        write(Veta,'(''Cell parameters:'',3f8.4,3f8.3)')
     1    CellPar(1:6,1,1)
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        write(Veta,'(''Target dimension:'',i2)') maxNDim
        il=il+1
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        do i=1,maxNDim-3
          il=il+1
          write(Veta,'(i1,a2,'' modulation vector'',3f7.4)')
     1      i,nty(i),Qu(1:3,i,1,1)
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        enddo
        il=il+6-maxNDim
        nEdwCell=0
        nEdwModFr=0
        nEdwModTo=0
        nEdwNDim95=0
        xpom=75.
        dpom=WizardLength*.5+20.-xpom
      else
        Veta='Cell para%meters:'
        xpom=140.
        dpom=WizardLength-xpom-5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCell=EdwLastMade
        if(isPowder) then
          k=0
        else
          k=KRefBlock
        endif
        call FeQuestRealAEdwOpen(EdwLastMade,CellRefBlock(1,k),6,
     1                           .not.CellReadIn(KRefBlock),.false.)
        il=il+1
        tpom=5.
        dpom=45.
        if(isPowder) then
          Veta='Target %dimension:'
          if(.not.KnownM50) NDim95(KRefBlock)=3
        else
          Veta='%Number of input indices:'
        endif
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNDim95=EdwLastMade
        call FeQuestIntEdwOpen(nEdwNDim95,NDim95(KRefBlock),.false.)
        call FeQuestEudOpen(nEdwNDim95,3,6,1,0.,0.,0.)
        tpom=xpom+dpom+50.
        Veta='Info about metrics parameters'
        dpom=FeTxLengthUnder(Veta)+5.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtInfoCell=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%1st modulation vector:'
        tpom=5.
        dpom=WizardLength*.5+20.-xpom
        do i=1,3
          il=il+1
          write(Veta(2:4),'(i1,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            nEdwModFr=EdwLastMade
          else if(i.eq.3) then
            nEdwModTo=EdwLastMade
          endif
          call FeQuestRealAEdwOpen(EdwLastMade,
     1      QuRefBlock(1,i,k),3,QuRefBlock(1,i,k).lt.-300..and.
     2      .not.isPowder,.false.)
          if(i.gt.NDim95(KRefBlock)-3)
     1      call FeQuestEdwDisable(EdwLastMade)
        enddo
      endif
      il=il+1
      ilp=il
      xpom=5.
      if(SelectRadiation) then
        tpom=xpom+CrwgXd+10.
        do i=1,3
          if(i.eq.1) then
            Veta='%X-rays'
            lpom=RadiationRefBlock(KRefBlock).eq.XRayRadiation
          else if(i.eq.2) then
            Veta='Ne%utrons'
            lpom=RadiationRefBlock(KRefBlock).eq.NeutronRadiation
          else if(i.eq.3) then
            Veta='%Electrons'
            lpom=RadiationRefBlock(KRefBlock).eq.ElectronRadiation
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) then
            nCrwRadFirst=CrwLastMade
            ilb=il
          else
            xpomb=tpom+FeTxLength(Veta)+10.
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          il=il+1
        enddo
        nCrwRadLast=CrwLastMade
      else
        if(RadiationRefBlock(KRefBlock).eq.XRayRadiation.and.
     1     DifCode(KRefBlock).ne.IdData11BM.and.
     2     DifCode(KRefBlock).ne.IdEDInel) then
          ilb=il
          xpomb=xpom
          il=il+1
        else
          ilb=0
        endif
        nCrwRadFirst=0
        nCrwRadLast=0
      endif
      if(ilb.gt.0) then
        Veta='X%-ray tube'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
        nButtTube=ButtonLastMade
        if(nCrwRadFirst.eq.0)
     1    call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtTube=0
      endif
      tpom=5.
      nCrwDouble=0
      nEdwWL2=0
      nEdwRat=0
      if(DifCode(KRefBlock).ne.IdSXD.and.
     1   DifCode(KRefBlock).ne.IdTopaz.and.
     2   DifCode(KRefBlock).ne.IdSENJU.and.
     3   DifCode(KRefBlock).ne.IdSCDLANL.and.
     4   DifCode(KRefBlock).ne.IdVivaldi.and.
     5   DifCode(KRefBlock).ne.IdKoala.and.
     6   DifCode(KRefBlock).le.200) then
        if(isPowder.and.RadiationRefBlock(KRefBlock).eq.XRayRadiation
     1     .and.DifCode(KRefBlock).ne.IdData11BM) then
          Veta='Kalpha1/Kalpha2 dou%blet'
          xpom=tpom+CrwXd+10.
          call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwDouble=CrwLastMade
          il=il+1
          call FeQuestCrwOpen(nCrwDouble,NAlfaRefBlock(KRefBlock).gt.1)
          Veta=LabelWaveLength1
          xpom=tpom+FeTxLength(LabelWaveLength1)+10.
        else
          Veta=LabelWaveLength
          xpom=tpom+FeTxLength(Veta)+10.
        endif
        dpom=90.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwWL1=EdwLastMade
        if(isPowder) then
          il=il+1
          i=idel(LabelWaveLength1)
          Veta(i:i)='2'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwWL2=EdwLastMade
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                        EdwYd,0)
          nEdwRat=EdwLastMade
          call FeQuestRealEdwOpen(nEdwWL1,LamA1RefBlock(KRefBlock),
     1                       LamA1RefBlock(KRefBlock).lt.0.,.false.)
        else
          call FeQuestRealEdwOpen(EdwLastMade,LamAveRefBlock(KRefBlock),
     1                       LamAveRefBlock(KRefBlock).lt.0.,.false.)
        endif
        il=il+1
      else
        nButtTube=0
        nEdwWL1=0
        tpom=5.
        dpom=90.
      endif
      il=il+1
      Veta='%Temperature'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwT=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,TempRefBlock(KRefBlock),
     1                        .false.,.false.)
      nLblPol=0
      nCrwPolarizationFirst=0
      nCrwPolarizationLast=0
      nLblMono=0
      nEdwMonAngle=0
      nButtSetMonAngle=0
      nEdwPerfMon=0
      nButtInfoPerpendicular=0
      nButtInfoParallel=0
      nEdwDIFC=0
      nEdwDIFA=0
      nEdwZERO=0
      nEdwTTh=0
      nEdwWCross=0
      nEdwTCross=0
      nEdwDE=0
      nEdwZeroE=0
      nCrwJason=0
      if(RadiationDetails) then
        il=ilp
        if(DifCode(KRefBlock).le.200) then
          xpom=xpom+dpom+80.
          tpom=xpom+CrwgXd+10.
          Veta='Polarization correction:'
          call FeQuestLblMake(id,xpom+30.,il,Veta,'L','B')
          nLblPol=LblLastMade
          do i=1,5
            il=il+1
            if(i.eq.1) then
              Veta='Circul%ar polarization'
              j=PolarizedCircular
            else if(i.eq.2) then
              Veta='%Perpendicular setting'
             j=PolarizedMonoPer
            else if(i.eq.3) then
              Veta='Pa%rallel setting'
              j=PolarizedMonoPar
            else if(i.eq.4) then
              Veta='Guinier %camera'
              j=PolarizedGuinier
            else
              Veta='%Linearly polarized beam'
              j=PolarizedLinear
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,2)
            call FeQuestCrwOpen(CrwLastMade,
     1                          PolarizationRefBlock(KRefBlock).eq.j)
            if(i.eq.1) then
              nCrwPolarizationFirst=CrwLastMade
            else if(i.eq.2.or.i.eq.3) then
              if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
              Veta='Info'
              dpomb=FeTxLengthUnder(Veta)+10.
              call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
              if(i.eq.2) then
                nButtInfoPerpendicular=ButtonLastMade
              else
                nButtInfoParallel=ButtonLastMade
              endif
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            endif
          enddo
          nCrwPolarizationLast=CrwLastMade
          il=il+1
          tpom=xpom
          Veta='Monochromator parameters:'
          call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
          call FeQuestLblOff(LblLastMade)
          nLblMono=LblLastMade
          il=il+1
          Veta='Per%fectness'
          xpom=tpom+FeTxLengthUnder(Veta)+80.
          dpom=90.
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwPerfMon=EdwLastMade
          il=il+1
          Veta='Glancing an%gle'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwMonAngle=EdwLastMade
          Veta='%Set glancing angle'
          dpomb=FeTxLengthUnder(Veta)+5.
          call FeQuestButtonMake(id,xpom+dpom+15.,il,dpomb,ButYd,Veta)
          nButtSetMonAngle=ButtonLastMade
          Veta='Mo%nochromator angle'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwGAlpha=EdwLastMade
          il=il+1
          Veta='A%symmetry parameter Psi'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwGBeta=EdwLastMade
          Polarization=PolarizationRefBlock(KRefBlock)
        else if(DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1          DifCode(KRefBlock).eq.IdTOFISIST.or.
     2          DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     3          DifCode(KRefBlock).eq.IdTOFVEGA.or.
     4          DifCode(KRefBlock).eq.IdTOFFree) then
          dpom=90.
          tpom=xpom+dpom+35.
          Polarization=PolarizedLinear
          Veta='TOF instrument parameters:'
          call FeQuestLblMake(id,tpom+110.,il,Veta,'L','B')
          il=il+1
          Veta='Use formula developed by Jason %Hodges'
          xpom=tpom+CrwXd+86.
          call FeQuestCrwMake(id,xpom,il,tpom+76.,il,Veta,'L',CrwXd,
     1                        CrwYd,1,0)
          call FeQuestCrwOpen(CrwLastMade,
     1                        TOFJasonRefBlock(KRefBlock).ge.1)
          nCrwJason=CrwLastMade
          Veta='Detector an%gle'
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          do i=1,5
            il=il+1
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) then
              nEdwTTh=EdwLastMade
              Veta='%ZERO'
            else if(i.eq.2) then
              nEdwZERO=EdwLastMade
              Veta='DIF%C'
            else if(i.eq.3) then
              nEdwDIFC=EdwLastMade
              Veta='DIF%A'
            else if(i.eq.4) then
              nEdwDIFA=EdwLastMade
              Veta='%WCross'
            else
              nEdwWCross=EdwLastMade
            endif
            if(i.eq.1) then
              pom=TOFTThRefBlock(KRefBlock)
              call FeQuestRealEdwOpen(EdwLastMade,pom,pom.lt.-3000.,
     1                                .false.)
            endif
          enddo
          tpom=xpom+dpom+20.
          xpom=tpom+80.
          Veta='ZER%OE'
          il=il-4
          do i=1,3
            il=il+1
            if(i.eq.3) il=il+1
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) then
              nEdwZEROE=EdwLastMade
              Veta='C%E'
            else if(i.eq.2) then
              nEdwDE=EdwLastMade
              Veta='TCro%ss'
            else
              nEdwTCross=EdwLastMade
            endif
          enddo
        endif
      endif
      NDim95Old=NDim95(KRefBlock)
      if(StavVolani.lt.0) call FeQuestButtonDisable(ButtonEsc)
1300  if(nCrwRadFirst.gt.0) then
        if(CrwLogicQuest(nCrwRadFirst)) then
          call FeQuestButtonOff(nButtTube)
          if(isPowder) call FeQuestCrwOpen(nCrwDouble,
     1                                   NAlfaRefBlock(KRefBlock).gt.1)
          RadiationRefBlock(KRefBlock)=XRayRadiation
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(CrwLogicQuest(nCrwRadLast)) then
            RadiationRefBlock(KRefBlock)=ElectronRadiation
          else
            RadiationRefBlock(KRefBlock)=NeutronRadiation
          endif
        endif
      endif
1320  if(nCrwDouble.ne.0) then
        if(CrwLogicQuest(nCrwDouble)) then
          call FeQuestRealEdwOpen(nEdwWL2,LamA2RefBlock(KRefBlock),
     1                          LamA2RefBlock(KRefBlock).lt.0.,.false.)
          call FeQuestRealEdwOpen(nEdwRat,LamRatRefBlock(KRefBlock),
     1                          LamA2RefBlock(KRefBlock).lt.0.,.false.)
          call FeQuestEdwLabelChange(nEdwWL1,LabelWaveLength1)
        else
          call FeQuestEdwClose(nEdwWL2)
          call FeQuestEdwClose(nEdwRat)
          call FeQuestEdwLabelChange(nEdwWL1,LabelWaveLength)
        endif
      endif
1340  if(RadiationDetails.and.DifCode(KRefBlock).le.200) then
        if(RadiationRefBlock(KRefBlock).eq.NeutronRadiation.or.
     1     RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            Polarization=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            Polarization=PolarizationRefBlock(KRefBlock)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(Polarization.eq.PolarizedMonoPer.or.
     1     Polarization.eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,
     1        FractPerfMonRefBlock(KRefBlock),.false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,
     1        AngleMonRefBlock(KRefBlock),.false.,.false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(Polarization.eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,
     1        FractPerfMonRefBlock(KRefBlock),.false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,
     1        AlphaGMonRefBlock(KRefBlock),.false.,.false.)
            call FeQuestRealEdwOpen(nEdwGBeta,
     1        BetaGMonRefBlock(KRefBlock),.false.,.false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
1350  if(nEdwTTh.gt.0) then
        if(CrwLogicQuest(nCrwJason)) then
          pom=TOFWCrossRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwWCross,pom,pom.lt.-3000.,.false.)
          pom=TOFTCrossRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwTCross,pom,pom.lt.-3000.,.false.)
          pom=TOFZeroERefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwZEROE,pom,pom.lt.-3000.,.false.)
          pom=TOFCeRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwDE,pom,pom.lt.-3000.,.false.)
          if(EdwStateQuest(nEdwZERO).ne.EdwOpened) then
            pom=TOFZeroTRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwZEROT,pom,pom.lt.-3000.,.false.)
            pom=TOFCtRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDT,pom,pom.lt.-3000.,.false.)
            pom=TOFAtRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwAT,pom,pom.lt.-3000.,.false.)
          endif
          call FeQuestEdwLabelChange(nEdwZEROT,'%ZEROT')
          call FeQuestEdwLabelChange(nEdwDT,'%CT')
          call FeQuestEdwLabelChange(nEdwAT,'%AT')
        else
          call FeQuestEdwClose(nEdwWCross)
          call FeQuestEdwClose(nEdwTCross)
          call FeQuestEdwClose(nEdwZEROE)
          call FeQuestEdwClose(nEdwDE)
          if(EdwStateQuest(nEdwZERO).ne.EdwOpened) then
            pom=TOFZeroRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwZERO,pom,pom.lt.-3000.,.false.)
            pom=TOFDifcRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDIFC,pom,pom.lt.-3000.,.false.)
            pom=TOFDifaRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDIFA,pom,pom.lt.-3000.,.false.)
          endif
          call FeQuestRealEdwOpen(nEdwDIFA,pom,pom.lt.-3000.,.false.)
          call FeQuestEdwLabelChange(nEdwZERO,'%ZERO')
          call FeQuestEdwLabelChange(nEdwDIFC,'DIF%C')
          call FeQuestEdwLabelChange(nEdwDIFA,'DIF%A')
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNDim95) then
        call FeQuestIntFromEdw(nEdwNDim95,NDim95(KRefBlock))
        nEdw=nEdwModFr
        do i=1,3
          if(i+3.le.NDim95(KRefBlock)) then
            if(EdwStateQuest(nEdw).ne.EdwOpened)
     1        call FeQuestRealAEdwOpen(nEdw,QuRefBlock(1,i,KRefBlock),3,
     2                         QuRefBlock(1,i,KRefBlock).lt.0.,.false.)
          else
            call FeQuestEdwDisable(nEdw)
          endif
          nEdw=nEdw+1
        enddo
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwRadFirst.and.
     2         CheckNumber.le.nCrwRadLast)) then
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        EventType=EventEdw
        EventNumber=nEdwWL1
        go to 1320
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          if(isPowder) then
            if(CrwLogicQuest(nCrwDouble)) then
              call FeQuestRealEdwOpen(nEdwWL1,LamA1D(k),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2D(k),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRatD(k),.false.,
     1                                .false.)
            else
              call FeQuestRealEdwOpen(nEdwWL1,LamA1D(k),.false.,
     1                                .false.)
              LamA2RefBlock (KRefBlock)=LamA2D (k)
              LamRatRefBlock(KRefBlock)=LamRatD(k)
            endif
          else
            call FeQuestRealEdwOpen(nEdwWL1,LamAveD(k),.false.,.false.)
          endif
        endif
        EventType=EventEdw
        EventNumber=nEdwWL1
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoCell.or.
     2         CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoCell) then
          if(isPowder) then
            call FeFillTextInfo('completebasic1.txt',0)
          else
            call FeFillTextInfo('completebasic2.txt',0)
          endif
        else if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(isPowder) then
          call FeQuestRealFromEdw(nEdwWL1,
     1                            LamA1RefBlock(KRefBlock))
          if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
            if(CrwLogicQuest(nCrwDouble)) then
              call FeQuestRealFromEdw(nEdwWL2,
     1                                LamA2RefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwRat,
     1                                LamRatRefBlock(KRefBlock))
              LamAveRefBlock(KRefBlock)=
     1          (LamA1RefBlock(KRefBlock)+
     2           LamA2RefBlock(KRefBlock)*LamRatRefBlock(KRefBlock))/
     3          (1.+LamRatRefBlock(KRefBlock))
            else
              LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
            endif
          else
            LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
          endif
        else
          call FeQuestRealFromEdw(nEdwWL1,
     1                            LamAveRefBlock(KRefBlock))
        endif
        AngleMonPom=AngleMonRefBlock(KRefBlock)
        call CrlCalcMonAngle(AngleMonPom,LamAveRefBlock(KRefBlock),ichp)
        if(ichp.eq.0) then
          call FeQuestRealEdwOpen(nEdwMonAngle,AngleMonPom,.false.,
     1                            .false.)
          AngleMonRefBlock(KRefBlock)=AngleMonPom
          EventType=EventEdw
          EventNumber=nEdwMonAngle
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              Polarization=PolarizedCircular
            else if(i.eq.2) then
              Polarization=PolarizedMonoPer
            else if(i.eq.3) then
              Polarization=PolarizedMonoPar
            else if(i.eq.4) then
              Polarization=PolarizedGuinier
            else
              Polarization=PolarizedLinear
            endif
          endif
          nCrw=nCrw+1
        enddo
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwJason) then
        go to 1350
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.ge.0) then
        if(nEdwNDim95.gt.0) then
          call FeQuestIntFromEdw(nEdwNDim95,NDim95(KRefBlock))
          do i=NDim95(KRefBlock)-2,3
            call SetRealArrayTo(QuRefBlock(1,i,KRefBlock),3,-333.)
          enddo
        endif
        if(UzTuByl.eq.0) then
          if(KRefBlock.eq.1.and.CreateNewRefBlock) then
            if(maxNDim.le.3.or.maxNDim.gt.NDim95(KRefBlock)) then
              n=3
              do i=1,3
                if(QuRefBlock(1,i,0).gt.-300.) n=n+1
              enddo
              NDim(KPhaseDR)=max(NDim95(KRefBlock),n)
            endif
            NDimI(KPhaseDR)=NDim(KPhaseDR)-3
            NDimQ(KPhaseDR)=NDim(KPhaseDR)**2
            call UnitMat(TrRefBlock(1,KRefBlock),NDim(KPhaseDR))
            maxNDim=max(maxNDim,NDim(KPhaseDR))
          endif
        endif
        if(nEdwCell.gt.0) then
          Veta=EdwStringQuest(nEdwCell)
          if(Veta.eq.' ') then
            if(ich.eq.0.and.KRefBlock.eq.1) then
              Veta='cell parameters not defined'
              nEdw=nEdwCell
              go to 1800
            endif
          else
            if(isPowder) then
              k=0
            else
              k=KRefBlock
            endif
            call FeQuestRealAFromEdw(nEdwCell,CellRefBlock(1,k))
            CellReadIn(KRefBlock)=CellRefBlock(1,KRefBlock).gt.0.
          endif
          nEdw=nEdwModFr
          do i=1,NDim95(KRefBlock)-3
            Veta=EdwStringQuest(nEdw)
            if(Veta.eq.' ') cycle
            call FeQuestRealAFromEdw(nEdw,QuRefBlock(1,i,KRefBlock))
            pom=0.
            do j=1,3
              pom=pom+abs(QuRefBlock(j,i,KRefBlock))
            enddo
            if(pom.lt..00001) then
              write(Veta,'(i1,a2,'' modulation vector isn''''t '',
     1                     ''acceptable'')') i,nty(i)
              go to 1800
            endif
            nEdw=nEdw+1
          enddo
        endif
        if(nCrwRadFirst.gt.0) then
          nCrw=nCrwRadFirst
          do i=1,2
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.1) then
                RadiationRefBlock(KRefBlock)=XRayRadiation
              else if(i.eq.2) then
                RadiationRefBlock(KRefBlock)=NeutronRadiation
              endif
            endif
            nCrw=nCrw+1
          enddo
        endif
        if(nEdwWL1.gt.0) then
          Veta=EdwStringQuest(nEdwWL1)
          if(Veta.eq.' ') then
            if(ich.eq.0) then
              Veta='wavelength not defined'
              nEdw=nEdwWL1
              go to 1800
            endif
          else
            if(isPowder) then
              call FeQuestRealFromEdw(nEdwWL1,
     1                                LamA1RefBlock(KRefBlock))
              if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
                KLamRefBlock(KRefBlock)=
     1            LocateInArray(LamA1RefBlock(KRefBlock),LamA1D,7,
     2                          .0001)
                if(CrwLogicQuest(nCrwDouble)) then
                  NAlfaRefBlock(KRefBlock)=2
                  call FeQuestRealFromEdw(nEdwWL2,
     1                                    LamA2RefBlock(KRefBlock))
                  call FeQuestRealFromEdw(nEdwRat,
     1                                    LamRatRefBlock(KRefBlock))
                  if(KLamRefBlock(KRefBlock).ne.0)
     1              KLamRefBlock(KRefBlock)=
     2                LocateInArray(LamA2RefBlock(KRefBlock),LamA2D,7,
     3                              .0001)
                  LamAveRefBlock(KRefBlock)=
     1                        (2.*LamA1RefBlock(KRefBlock)+
     2                            LamA2RefBlock(KRefBlock))/3.
                else
                  NAlfaRefBlock(KRefBlock)=1
                  LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
                endif
              else
                LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
                KLamRefBlock(KRefBlock)=0
                NAlfaRefBlock(KRefBlock)=1
              endif
            else
              call FeQuestRealFromEdw(nEdwWL1,
     1                                LamAveRefBlock(KRefBlock))
              if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
                KLamRefBlock(KRefBlock)=
     1            LocateInArray(LamAveRefBlock(KRefBlock),LamAveD,7,
     2                          .0001)
              else
                KLamRefBlock(KRefBlock)=0
              endif
            endif
          endif
          if(RadiationDetails) then
            PolarizationRefBlock(KRefBlock)=Polarization
            if(Polarization.ne.PolarizedCircular.and.
     1         Polarization.ne.PolarizedLinear.and.
     2         Polarization.ne.PolarizedGuinier) then
              call FeQuestRealFromEdw(nEdwMonAngle,
     1                                AngleMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwPerfMon,
     1                                FractPerfMonRefBlock(KRefBlock))
            else if(Polarization.eq.PolarizedGuinier) then
              call FeQuestRealFromEdw(nEdwGAlpha,
     1                                AlphaGMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwGBeta,
     1                                BetaGMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwPerfMon,
     1                                FractPerfMonRefBlock(KRefBlock))

            endif
          endif
        endif
        if(nEdwTTh.gt.0) then
          if(CrwLogicQuest(nCrwJason)) then
            TOFJasonRefBlock(KRefBlock)=1
          else
            TOFJasonRefBlock(KRefBlock)=0
          endif
          nEdw=nEdwTTh
          do i=1,4
            Veta=EdwStringQuest(nEdw)
            if(Veta.eq.' ') then
              if(i.eq.1) then
                Veta='detector angle'
              else if(i.eq.2) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='%ZERO'
                else
                  Veta='ZERO%E'
                endif
              else if(i.eq.3) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='DIF%C'
                else
                  Veta='%CT'
                endif
              else if(i.eq.4) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='DIF%A'
                else
                  Veta='%AT'
                endif
              endif
              Veta=Veta(:idel(Veta))//' not defined'
              go to 1800
            endif
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.eq.1) then
              TOFTThRefBlock(KRefBlock)=pom
            else if(i.eq.2) then
              TOFZeroRefBlock(KRefBlock)=pom
            else if(i.eq.3) then
              TOFDifcRefBlock(KRefBlock)=pom
            else if(i.eq.4) then
              TOFDifaRefBlock(KRefBlock)=pom
            endif
            nEdw=nEdw+1
          enddo
          if(TOFJasonRefBlock(KRefBlock).ne.0) then
            nEdw=nEdwWCross
            do i=1,4
              Veta=EdwStringQuest(nEdw)
              if(Veta.eq.' ') then
                if(i.eq.1) then
                  Veta='WCross'
                else if(i.eq.2) then
                  Veta='ZERO%E'
                else if(i.eq.3) then
                  Veta='CE'
                else if(i.eq.4) then
                  Veta='TCross'
                endif
                Veta=Veta(:idel(Veta))//' not defined'
                go to 1800
              endif
              call FeQuestRealFromEdw(nEdw,pom)
              if(i.eq.1) then
                TOFWCrossRefBlock(KRefBlock)=pom
              else if(i.eq.2) then
                TOFZeroERefBlock(KRefBlock)=pom
              else if(i.eq.3) then
                TOFCeRefBlock(KRefBlock)=pom
              else
                TOFTCrossRefBlock(KRefBlock)=pom
              endif
              nEdw=nEdw+1
            enddo
          endif
        endif
        call FeQuestRealFromEdw(nEdwT,TempRefBlock(KRefBlock))
        if(UzTuByl.eq.0.and.StavVolani.ge.0) StavVolani=StavVolani+1
        go to 9999
1800    call FeChybne(-1.,-1.,Veta,'Please complete the form',
     1                SeriousError)
1850    call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        if(nEdw.ne.0) then
          EventType=EventEdw
          EventNumber=nEdw
        endif
        go to 1500
      endif
9999  if(ich.ge.0.and..not.isPowder) then
        call CopyVek(CellRefBlock(1,KRefBlock),xp,6)
        do i=4,6
          xp(i)=cos(xp(i)*ToRad)
        enddo
        call Recip(xp,DRRcp,pom)
      endif
      return
      end
      subroutine EM9CompleteTrans(StavVolani,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension PomMat(36),TrRefBlockI(36),CellPom(6),QuPom(3,3),
     1          TrMatP(3,3),xp(3)
      character*256 EdwStringQuest
      character*80  Veta
      character*21  LabelT
      character*2   nty
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      integer EdwStateQuest,StavVolani,UzTuByl,UseTabsIn,UseTabsP
      logical CrwLogicQuest,EqRV,MatRealEqUnitMat,FeYesNoHeader,
     1        UpdateRefCell,UzSePtal,EqRVM,VolatHide,Poprve
      data LabelT/'%Number of domains'/
      save NDimLim
      UseTabsIn=UseTabs
      UzTuByl=mod(StavVolani/10,10)
      UpdateRefCell=.false.
      UzSePtal=.false.
      VolatHide=.false.
      Poprve=.true.
      if(ReimportRefBlock) then
        call CopyVek(CellRefBlock(1,0),CellPom,6)
        call CopyVek(QuRefBlock(1,1,0),QuPom,3*(maxNDim-3))
        call MatInv(TrRefBlock(1,KRefBlock),TrRefBlockI,pom,maxNDim)
        call CrlCellQTrans(TrRefBlockI,CellPom,QuPom,maxNDim,ich)
        if(ich.ne.0) go to 1100
        if(.not.EqRV(CellRefBlock(1,KRefBlock),CellPom,3,.001).or.
     1     .not.EqRV(CellRefBlock(4,KRefBlock),CellPom(4),3,.01)) then
          call FindCellTrans(CellRefBlock(1,KRefBlock),CellPom,
     1                       TrMatP,ich)
          if(ich.eq.0) then
            call UnitMat(PomMat,maxNDim)
            do i=1,3
              do j=1,3
                k=j+(i-1)*maxNDim
                PomMat(k)=TrMatP(j,i)
              enddo
            enddo
            do i=4,maxNDim
              call MultM(QuPom(1,i-3),TrMatP,xp,1,3,3)
              if(EqRVM(xp,QuRefBlock(1,1,KRefBlock),3,.01)) then
                j=i+(i-1)*maxNDim
                PomMat(j)=-1.
              endif
            enddo
            if(MatRealEqUnitMat(PomMat,maxNDim,.001)) go to 1100
            call MultM(PomMat,TrRefBlock(1,KRefBlock),TrRefBlockI,
     1                 maxNDim,maxNDim,maxNDim)
            call CopyMat(TrRefBlockI,TrRefBlock(1,KRefBlock),maxNDim)
            UseTrRefBlock(KRefBlock)=.not.
     1        MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001)
            if(KRefBlock.eq.1) then
              call CopyVek(CellRefBlock(1,KRefBlock),CellPom,6)
              call CopyVek(QuRefBlock(1,1,KRefBlock),QuPom,
     1                     3*(maxNDim-3))
              call CrlCellQTrans(TrRefBlock(1,KRefBlock),CellPom,QuPom,
     1                           maxNDim,ich)
              UpdateRefCell=.not.EqRV(CellRefBlock(1,0),CellPom,3,.001)
     1                .or..not.EqRV(CellRefBlock(4,0),CellPom(4),3,.01)
            endif
          else
            UseTabs=NextTabs()
            xpom=20.
            do i=1,6
              call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
              xpom=xpom+50.
            enddo
            NInfo=6
            TextInfo(1)='relationship between the reimported cell '//
     1                  'parameters:'
            write(TextInfo(2),102)(Tabulator,CellRefBlock(i,KRefBlock),
     1                             i=1,6)
            TextInfo(3)='and the original ones:'
            write(TextInfo(4),102)(Tabulator,CellPom(i),i=1,6)
            TextInfo(5)='could not be estabilished and therefore the '//
     1                  'reimport will not'
            TextInfo(6)='be accomplished.'
            call FeInfoOut(-1.,-1.,'ERROR','L')
            ich=-1
            go to 9999
          endif
        endif
      endif
1100  id=NextQuestId()
      CheckKeyboard=.true.
      if(KRefBlock.eq.1.and.CreateNewRefBlock) then
        Veta='Define'
      else
        Veta='Relationship to'
      endif
      Veta=Veta(:idel(Veta))//
     1     ' the reference cell/split by twinning'
      call FeQuestTitleMake(id,Veta)
      il=0
      tpom=5.
      xpom=50.
      il=il+1
      write(Veta,100)(CellRefBlock(i,0),i=1,6)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblCell=LblLastMade
      il=il+1
      if(KRefBlock.gt.1.or..not.CreateNewRefBlock) then
        write(Veta,'(''Target dimension:'',i2)') maxNDim
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        NDimLim=maxNDim
        nEdwNDim=0
      else
        Veta='Tar%get dimension'
        dpom=45.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        if(UzTuByl.eq.0) then
          NDimLim=3
          do i=1,3
            if(QuRefBlock(1,i,0).gt.-300.) NDimLim=NDimLim+1
          enddo
        endif
        NDim(KPhase)=NDimLim
        NDimI(KPhase)=NDimLim-3
        NDimQ(KPhase)=NDimLim**2
        maxNDim=max(maxNDim,NDimLim)
        call UnitMat(TrRefBlock(1,KRefBlock),maxNDim)
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNDim=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,maxNDim,.false.)
        call FeQuestEudOpen(EdwLastMade,NDimLim,6,1,0.,0.,0.)
      endif
      nEdwModFr=0
      nEdwModTo=0
      nLblModFr=0
      nLblModTo=0
      do i=1,3
        il=il+1
        write(Veta,'(''%'',i1,a2,'' modulation vector'')') i,nty(i)
        if(i+3.le.NDimLim+1) xpom=tpom+FeTxLengthUnder(Veta)+10.
        if(i+3.le.NDimLim) then
          write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          if(i.eq.1) nLblModFr=LblLastMade
          nLblModTo=LblLastMade
        else if(KRefBlock.eq.1) then
          dpom=WizardLength*.5-xpom+10.
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i+2.eq.NDimLim) then
            nEdwModFr=EdwLastMade
          else if(i.eq.3) then
            nEdwModTo=EdwLastMade
          endif
          if(i.le.maxNDim-3) then
            call FeQuestRealAEdwOpen(EdwLastMade,
     1        QuRefBlock(1,i,0),3,QuRefBlock(1,i,0).lt.-300.,.false.)
          else
            call FeQuestEdwDisable(EdwLastMade)
          endif
        endif
      enddo
      il=il+1
      Veta='%Max. satellite index'
      dpom=60.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestEdwDisable(EdwLastMade)
      nEdwMMax=EdwLastMade
      Veta='A%ccuracy'
      il=il+1
      dpom=120.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestEdwDisable(EdwLastMade)
      nEdwDiffSat=EdwLastMade
      il=il+1
      tpom=50.
      Veta='%Define transformation matrix'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtDefineTrans=ButtonLastMade
      il=il+1
      tpom=20.
      Veta='Transformation matrix applied to input indices:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblLabelMatrix=LblLastMade
      call FeQuestLblOff(LblLastMade)
      YShow=QuestYPosition(id,il+7)+QuestYMin(id)
      XShow=QuestXMin(id)
      il=2
      Veta='%Twinning'
      tpom=WizardLength*.5+50.
      xpom=tpom+CrwXd+10.
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwTwin=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NTwin.gt.1)
      xpom=xpom+FeTxLengthUnder(Veta)+20.
      Veta='T%winning matrices'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonDisable(ButtonLastMade)
      nButtTwinMat=ButtonLastMade
      il=il+1
      dpom=60.
      xpom=WizardLength-dpom-EdwYd-30.
      call FeQuestEudMake(id,tpom,il,xpom,il,LabelT,'L',dpom,EdwYd,1)
      call FeQuestIntEdwOpen(EdwLastMade,NTwin,.false.)
      call FeQuestEudOpen(EdwLastMade,2,mxsc,1,0.,0.,0.)
      nEdwNTwin=EdwLastMade
      il=il+1
      Veta='Data %related to domain#'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,ITwRead(KRefBlock),.false.)
      call FeQuestEudOpen(EdwLastMade,1,NTwin,1,0.,0.,0.)
      if(HKLF5RefBlock(KRefBlock).ne.0)
     1  call FeQuestEdwDisable(EdwLastMade)
      nEdwITwin=EdwLastMade
      NTwinOld=NTwin
      NDimOld=maxNDim
      il=il+1
      dpom=dpom+15.
      Veta='Multipl%y input F(hkl)/I(hkl) by'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScale=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ScaleRefBlock(KRefBlock),
     1                        .false.,.false.)
      if(HKLF5RefBlock(KRefBlock).ne.0) then
        il=il+1
        Veta='%HKLF5 file by Jana tools'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwHKLF5=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,HKLF5RefBlock(KRefBlock).eq.2)
        if(Ntwin.le.1) then
          NTwin=1
1200      read(71,FormatRefBlock(KRefBlock),err=1250,end=1250)
     1      (k,i=1,maxNDim),pom,pom,k
          NTwin=max(NTwin,iabs(k))
          go to 1200
1250      rewind 71
          do i=2,NTwin
            call UnitMat(rtw (1,i),3)
            call UnitMat(rtwi(1,i),3)
          enddo
          if(NTwin.gt.1) call FeQuestCrwOn(nCrwTwin)
        endif
      else
        nCrwHKLF5=0
      endif
      if(maxNDim.gt.3) then
        il=il+1
        Veta='Import %only satellites'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwImpOnlySat=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,ImportOnlySatellites(KRefBlock))
      else
        nCrwImpOnlySat=0
      endif
1300  if(NTwin.gt.1) then
        call FeQuestButtonOpen(nButtTwinMat,ButtonOff)
        call FeQuestIntEdwOpen(nEdwNTwin,NTwin,.false.)
        call FeQuestEudOpen(nEdwNTwin,2,mxsc,1,0.,0.,0.)
        call FeQuestIntEdwOpen(nEdwITwin,ITwRead(KRefBlock),.false.)
        call FeQuestEudOpen(nEdwITwin,1,NTwin,1,0.,0.,0.)
        if(HKLF5RefBlock(KRefBlock).ne.0)
     1    call FeQuestEdwDisable(nEdwITwin)
      else
        call FeQuestButtonDisable(nButtTwinMat)
        call FeQuestEdwDisable(nEdwNTwin)
        call FeQuestEdwDisable(nEdwITwin)
      endif
      if(NDim95(KRefBlock).lt.maxNDim) then
        call FeQuestIntEdwOpen(nEdwMMax,MMaxRefBlock(KRefBlock),
     1                         .false.)
        call FeQuestEudOpen(nEdwMMax,0,111,1,0.,0.,0.)
        call FeQuestRealAEdwOpen(nEdwDiffSat,
     1                           DiffSatRefBlock(1,KRefBlock),3,
     2                           .false.,.false.)
      else
        call FeQuestEdwDisable(nEdwMMax)
        call FeQuestEdwDisable(nEdwDiffSat)
      endif
      if(maxNDim.ne.NDimOld) then
        k=0
        call CopyMat(TrRefBlock(1,KRefBlock),PomMat,NDimOld)
        call UnitMat(TrRefBlock(1,KRefBlock),maxNDim)
        do i=1,NDimOld
          do j=1,6
            if(j.le.NDimOld) then
              k=k+1
              if(i.le.maxNDim.and.j.le.maxNDim) then
                l=j+(i-1)*maxNDim
                TrRefBlock(l,KRefBlock)=PomMat(j+(i-1)*NDimOld)
              endif
            endif
          enddo
        enddo
      endif
      if(UpdateRefCell) then
        UseTabsP=UseTabs
        UseTabs=NextTabs()
        xpom=80.
        do i=1,6
          call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
          xpom=xpom+50.
        enddo
        NInfo=3
        TextInfo(1)='Reimported data file leads to different '//
     1              'reference cell parameters'
        write(Veta,102)(Tabulator,CellRefBlock(i,0),i=1,6)
        TextInfo(2)='Old ones:'//Veta(:idel(Veta))
        write(Veta,102)(Tabulator,CellPom(i),i=1,6)
        TextInfo(3)='New ones:'//Veta(:idel(Veta))
        Veta='Do you want to update them?'
        if(FeYesNoHeader(-1.,-1.,Veta,1)) then
          call CopyVek(CellPom,CellRefBlock(1,0),6)
          write(Veta,100)(CellRefBlock(i,0),i=1,6)
          call FeQuestLblChange(nLblCell,Veta)
          nLbl=nLblModFr
          do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
            call CopyVek(QuPom(1,i),QuRefBlock(1,i,0),3)
            write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
        endif
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsP
        UpdateRefCell=.false.
      endif
      if(MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001))
     1  then
        call FeQuestLblOff(nLblLabelMatrix)
        if(VolatHide) then
          call CrlMatEqHide
          VolatHide=.false.
        endif
      else
        if(VolatHide) call CrlMatEqHide
        call CrlMatEqShow(XShow,YShow,6,TrRefBlock(1,KRefBlock),0,
     1                    IndicesCapital,Indices,maxNDim)
        VolatHide=.true.
        call FeQuestLblOn(nLblLabelMatrix)
      endif
      if(Poprve) then
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)

        Poprve=.false.
      endif
      NDimOld=maxNDim
1500  call FeQuestEvent(id,ich)
      if(nEdwNTwin.gt.0) then
        if(EdwStateQuest(nEdwNTwin).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwNTwin,Ntwin)
          do i=NTwinOld+1,NTwin
            call UnitMat(rtw(1,i),3)
          enddo
        endif
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNDim) then
        call FeQuestIntFromEdw(nEdwNDim,maxNDim)
        NDim(KPhaseDR)=maxNDim
        NDimI(KPhaseDR)=maxNDim-3
        NDimQ(KPhaseDR)=maxNDim**2
        nEdw=nEdwModFr
        do i=1,3
          if(i+3.gt.NDimLim) then
            if(i+3.le.maxNDim) then
              if(EdwStateQuest(nEdw).ne.EdwOpened)
     1          call FeQuestRealAEdwOpen(nEdw,QuRefBlock(1,i,KRefBlock),
     2                        3,QuRefBlock(1,i,KRefBlock).lt.0.,.false.)
            else
              call FeQuestEdwDisable(nEdw)
            endif
            nEdw=nEdw+1
          endif
        enddo
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNTwin) then
        call FeQuestIntEdwOpen(nEdwITwin,
     1                         min(ITwRead(KRefBlock),NTwin),.false.)
        call FeQuestEudOpen(nEdwITwin,1,NTwin,1,0.,0.,0.)
        if(HKLF5RefBlock(KRefBlock).ne.0)
     1    call FeQuestEdwDisable(nEdwITwin)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwTwin) then
        if(CrwLogicQuest(CheckNumber)) then
          NTwin=2
          EventType=EventEdw
          EventNumber=nEdwITwin
        else
          NTwin=1
        endif
        go to 1300
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDefineTrans) then
        UzSePtal=.true.
        WizardMode=.false.
        Veta='Define the transformation matrix'
        call FeReadRealMat(-1.,-1.,Veta,Indices,IdChangeCase,
     1                     TrRefBlock(1,KRefBlock),TrRefBlockI,
     1                     maxNDim,.true.,.false.,ichp)
        if(ichp.ne.0) go to 1600
        if(KRefBlock.le.1.and.CreateNewRefBlock.and.
     1     .not.ParentStructure) then
          call EM9TrMatApply(nLblCell,nLblModFr,1,PomMat,ichp)
        else
          call EM9TrMatApply(nLblCell,nLblModFr,0,
     1                       TrDirCos(1,KRefBlock),ichp)
        endif
1600    WizardMode=.true.
        WizardTitle=.true.
        if(ichp.eq.0) then
          go to 1300
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtTwinMat) then
        WizardMode=.false.
        if(.not.MatRealEqUnitMat(TrMP,MaxNDim,.001)) then
          NInfo=2
          TextInfo(1)='The repository reference cell parameters are '//
     1                'different from those used in the refinement'
          TextInfo(2)='as the cell has been transformed. The twinning'//
     1                ' matri'
          if(NTwin.gt.2) then
            Veta='ces are'
          else
            Veta='x is'
          endif
          TextInfo(2)=TextInfo(2)(:idel(TextInfo(2)))//
     1                Veta(:idel(Veta))//' related to the final cell '//
     2                'parameters.'
          call FeInfoOut(-1.,-1.,'WARNING','L')
        endif
        call ReadTw(Rtw,Rtwi,NTwin,CellRefBlock(1,0),ichp)
        if(ichp.eq.0) NTwinOld=NTwin
        WizardMode=.true.
        WizardTitle=.true.
        go to 1500
      else if(CheckType.eq.EventASCII) then
        go to 1500
      else if(CheckType.eq.EventKey) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(.not.UzSePtal) then
          call EM9TrMatApply(nLblCell,nLblModFr,0,
     1                       TrDirCos(1,KRefBlock),ichp)
          if(ichp.ne.0) then
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1300
          endif
        endif
        if(nCrwHKLF5.gt.0) then
          if(CrwLogicQuest(nCrwHKLF5)) then
            HKLF5RefBlock(KRefBlock)=2
          else
            HKLF5RefBlock(KRefBlock)=1
          endif
        else
          HKLF5RefBlock(KRefBlock)=0
        endif
        if(nCrwImpOnlySat.gt.0) then
          ImportOnlySatellites(KRefBlock)=CrwLogicQuest(nCrwImpOnlySat)
        endif
        UseTrRefBlock(KRefBlock)=.not.
     1    MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001)
        if(EdwStateQuest(nEdwITwin).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwITwin,ITwRead(KRefBlock))
        else
          ITwRead(KRefBlock)=1
        endif
        if(nEdwModFr.gt.0.and.CreateNewRefBlock) then
          nEdw=nEdwModFr
          do i=1,maxNDim-3
            if(i.gt.NDimLim-3) then
              Veta=EdwStringQuest(nEdw)
              if(Veta.eq.' ') then
                if(ich.eq.0.and.KRefBlock.eq.1) then
                  write(Veta,'(i1,a2,
     1                  '' modulation vector not defined'')') i,nty(i)
                  go to 1800
                endif
              endif
              call FeQuestRealAFromEdw(nEdw,QuRefBlock(1,i,0))
              nEdw=nEdw+1
            endif
          enddo
        endif
        if(NDim95(KRefBlock).lt.maxNDim) then
          call FeQuestIntFromEdw(nEdwMMax,MMaxRefBlock(KRefBlock))
          call FeQuestRealAFromEdw(nEdwDiffSat,
     1                             DiffSatRefBlock(1,KRefBlock))
        endif
        call FeQuestRealFromEdw(nEdwScale,ScaleRefBlock(KRefBlock))
        if(UzTuByl.eq.0) StavVolani=StavVolani+10
        go to 9999
1800    call FeChybne(-1.,-1.,Veta,'Please complete the form.',
     1                SeriousError)
      endif
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(VolatHide) then
        call CrlMatEqHide
        VolatHide=.false.
      endif
      return
100   format('Cell parameters:',3f8.4,3f8.3)
101   format(i1,a2,' modulation vector',5x,3f7.4)
102   format(3(a1,f10.4),3(a1,f10.3))
      end
      subroutine EM9TrMatApply(nLblCell,nLblModFr,Key,DirCosTr,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension DirCosTr(*)
      character*80 Veta
      character*2  nty
      ich=0
      if(Key.eq.0) then
        KRefB=0
      else
        KRefB=KRefBlock
      endif
      call CopyVek(CellRefBlock(1,KRefB),CellPar(1,1,KPhaseDR),6)
      call CopyVek(CellRefBlockSU(1,KRefB),CellParSU(1,1,KPhaseDR),6)
      do i=1,maxNDim-3
        call CopyVek(QuRefBlock(1,i,KRefB),Qu(1,i,1,KPhaseDR),3)
      enddo
      call SetMet(0)
      call EM9SetTransform(DirCosTr,Key,ich)
      if(ich.eq.0.and.Key.ne.0) then
        write(Veta,100)(CellRefBlock(i,0),i=1,6)
        call FeQuestLblChange(nLblCell,Veta)
        if(nLblModFr.gt.0) then
          nLbl=nLblModFr
          do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
            write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
        endif
      endif
      return
100   format('Cell parameters:',3f8.4,3f8.3)
101   format(i1,a2,' modulation vector  ',3f7.4)
      end
      subroutine EM9SetTransform(DirCosTr,Key,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ZVPom(36),ZVIPom(36),ZSigP(9),ZSigIP(9),PomMat1(9),
     1          PomMat2(9),CellOut(6),VSig(9),RcpOut(3),DirCosTr(9),
     2          QuOut(3,3),CellOutSU(6)
      logical Psat,EqRV,FeYesNoHeader
      real MTPom(9),MTIPom(9)
      Psat=.false.
      if(Key.eq.0) then
        TextInfo(1)='Cell parameters'
        idl=idel(TextInfo(1))
        if(maxNDim.gt.3) then
          TextInfo(1)(idl+2:)='and modulation vector'
          idl=idel(TextInfo(1))
          if(maxNDim.gt.4) then
            TextInfo(1)(idl+1:)='s'
            idl=idl+1
          endif
        endif
        i=maxNDim
        j=idel(TextInfo(1))
        TextInfo(i)=TextInfo(1)(:j)//' as used in data collection'
        TextInfo(1)=TextInfo(1)(:j)//
     1              ' as follows from the reference ones'
      endif
      ich=0
      if(Key.eq.0) then
        call CopyMat(TrRefBlock(1,KRefBlock),ZVPom,maxNDim)
        call MatInv(ZVPom,ZVIPom,pom,maxNDim)
      else
        call CopyMat(TrRefBlock(1,KRefBlock),ZVIPom,maxNDim)
        call MatInv(ZVIPom,ZVPom,pom,maxNDim)
      endif
      if(abs(pom).le.0.) then
        call FeChybne(-1.,-1.,'transformation matrix is singular.',
     1               ' ',SeriousError)
        ich=1
        go to 9999
      endif
      if(maxNDim.gt.3) then
        m=0
        do j=1,3
          do k=1,3
            m=m+1
            pom=ZVPom(k+(j-1)*maxNDim)
            do l=4,maxNDim
              if(Key.eq.0) then
                KRefB=0
              else
                KRefB=KRefBlock
              endif
              pom=pom+ZVPom(k+(l-1)*maxNDim)*QuRefBlock(j,l-3,KRefB)
            enddo
            ZSigP(m)=pom
          enddo
        enddo
        call matinv(ZSigP,ZSigIP,pom,3)
        if(abs(pom).le.0.) then
          call FeChybne(-1.,-1.,'3x3 upper block is singular.',' ',
     1                   SeriousError)
          ich=1
          go to 9999
        endif
      else
        call CopyMat(ZV Pom,ZSigP ,3)
        call CopyMat(ZVIPom,ZSigIP,3)
      endif
      call trmat(ZSigP,PomMat1,3,3)
      call multm(ZSigP,MetTensI(1,1,KPhaseDR),PomMat2,3,3,3)
      call multm(PomMat2,PomMat1,MTIPom,3,3,3)
      call matinv(MTIPom,MTPom,pom,3)
      m=1
      do j=1,3
        CellOut(j)=sqrt(MTPom(m))
        RcpOut(j)=sqrt(MTIPom(m))
        m=m+4
      enddo
      k=0
      do i=1,3
        do j=1,3
          k=k+1
          DirCosTr(k)=ZSigIP(k)/rcp(j,1,KPhaseDR)*RcpOut(i)
        enddo
      enddo
      do j=4,6
        call indext(10-j,l,k)
        m=l+(k-1)*3
        CellOut(j)=MTPom(m)/(CellOut(l)*CellOut(k))
        if(abs(CellOut(j)).le..000001) CellOut(j)=0.
        CellOut(j)=acos(CellOut(j))/torad
      enddo
      do j=1,9
        MetTensS(j,1,KPhaseDR)=sqrt(MetTensS(j,1,KPhaseDR))
      enddo
      call trmat(ZSigIP,PomMat1,3,3)
      call MultMQ(ZSigIP,MetTensS(1,1,KPhaseDR),PomMat2,3,3,3)
      call MultMQ(PomMat2,PomMat1,MTIPom,3,3,3)
      m=1
      do j=1,3
        CellOutSU(j)=MTIPom(m)*.5/CellOut(j)
        m=m+4
      enddo
      do j=1,2
        do k=j+1,3
          m=k+(j-1)*3
          l=6-j-k
          pom=MTPom(m)
          if(abs(pom).gt..0001) then
            pom=(MTIPom(m)**2-
     1           (pom/CellOut(j)*CellOutSU(j))**2-
     2           (pom/CellOut(k)*CellOutSU(k))**2)/
     3           (pom*tan(CellOut(l+3)*ToRad)*ToRad)**2
            if(pom.gt.0.) then
              CellOutSU(l+3)=sqrt(pom)
            else
              CellOutSU(l+3)=0.
            endif
          else
            CellOutSU(l+3)=0.
          endif
        enddo
      enddo
      if(maxNDim.gt.3) then
        m=0
        do j=1,3
          do k=4,maxNDim
            m=m+1
            pom=ZVPom(k+(j-1)*maxNDim)
            do l=4,maxNDim
              if(Key.eq.0) then
                KRefB=0
              else
                KRefB=KRefBlock
              endif
              pom=pom+ZVPom(k+(l-1)*maxNDim)*
     1                QuRefBlock(j,l-3,KRefB)
            enddo
            VSig(m)=pom
          enddo
        enddo
        call multm(VSig,ZSigIP,PomMat1,maxNDim-3,3,3)
        call trmat(PomMat1,QuOut,maxNDim-3,3)
      endif
      if(Key.eq.0) then
        write(TextInfo(2),100) CellOut
        do i=1,maxNDim-3
          write(TextInfo(2+i),101)(QuOut(j,i),j=1,3)
        enddo
        Ninfo=maxNDim-1
        if(CellRefBlock(1,KRefBlock).gt.0.) then
          NInfo=NInfo+2
          write(TextInfo(NInfo),100)(CellRefBlock(i,KRefBlock),i=1,6)
          Psat=.not.EqRV(CellRefBlock(1,KRefBlock),CellOut,3,.001).or.
     1         .not.EqRV(CellRefBlock(4,KRefBlock),CellOut(4),3,.01)
          do i=1,maxNDim-3
            if(QuRefBlock(1,i,KRefBlock).gt.-300.) then
              write(TextInfo(NInfo+i),101)(QuRefBlock(j,i,KRefBlock),
     1                                     j=1,3)
              if(.not.Psat)
     1          Psat=.not.EqRV(QuRefBlock(1,i,KRefBlock),QuOut(1,i),3,
     2                         .001)
            else
              write(TextInfo(NInfo+i),102)
            endif
          enddo
          NInfo=NInfo+maxNDim-3
          if(Psat) then
            if(.not.FeYesNoHeader(-1.,-1.,'The difference seems to be'//
     1                            ' too large, accept it anyhow?',0))
     2        ich=1
          endif
        endif
      else
        call CopyVek(CellOut,CellRefBlock(1,0),6)
        call CopyVek(CellOutSU,CellRefBlockSU(1,0),6)
        do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
          call CopyVek(QuOut(1,i),QuRefBlock(1,i,0),3)
        enddo
      endif
9999  return
100   format(3f9.4,3f9.3)
101   format(13x,3f9.4)
102   format(13x,3('   ------'))
      end
      subroutine EM9ImportSavePar
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      dimension TrIn(36),DiffSatIn(3),QuIn(3,3),UbIn(9),CellIn(6)
      character*256 SourceFileIn
      character*10  SourceFileDateIn
      character*8   SourceFileTimeIn
      integer       DifCodeIn,RadiationIn,PolarizationIn,HKLF5RefBlockIn
     1             ,PwdMethodRefBlockIn
      logical       UseTrRefBlockIn
      real          LamAveIn,LamA1In,LamA2In
      save SourceFileIn,SourceFileDateIn,SourceFileTimeIn,NDimIn,TrIn,
     1     NDim95In,DifCodeIn,ITwReadIn,UseTrRefBlockIn,
     2     MMaxIn,DiffSatIn,QuIn,UBIn,LamAveIn,CellIn,RadiationIn,
     3     AngleMonIn,FractPerfMonIn,PolarizationIn,
     4     LamA1In,LamA2In,TOFDifcRefBlockIn,TOFDifaRefBlockIn,
     5     TOFZeroRefBlockIn,TOFTThRefBlockIn,ScaleRefBlockIn,
     6     PwdMethodRefBlockIn,EDOffsetRefBlockIn,EDSlopeRefBlockIn,
     7     EDTThRefBlockIn,TempRefBlockIn,HKLF5RefBlockIn
      SourceFileIn=SourceFileRefBlock(KRefBlock)
      SourceFileDateIn=SourceFileDateRefBlock(KRefBlock)
      SourceFileTimeIn=SourceFileTimeRefBlock(KRefBlock)
      NDimIn=maxNDim
      NDim95In=NDim95(KRefBlock)
      DifCodeIn=DifCode(KRefBlock)
      ITwReadIn=ITwRead(KRefBlock)
      UseTrRefBlockIn=UseTrRefBlock(KRefBlock)
      RadiationIn=RadiationRefBlock(KRefBlock)
      PolarizationIn=PolarizationRefBlock(KRefBlock)
      AngleMonIn=AngleMonRefBlock(KRefBlock)
      FractPerfMonIn=FractPerfMonRefBlock(KRefBlock)
      TOFDifcRefBlockIn=TOFDifcRefBlock(KRefBlock)
      TOFDifaRefBlockIn=TOFDifaRefBlock(KRefBlock)
      TOFZeroRefBlockIn=TOFZeroRefBlock(KRefBlock)
      TOFTThRefBlockIn=TOFZeroRefBlock(KRefBlock)
      EDOffsetRefBlockIn=EDOffsetRefBlock(KRefBlock)
      EDSlopeRefBlockIn=EDSlopeRefBlock(KRefBlock)
      EDTThRefBlockIn=EDTThRefBlock(KRefBlock)
      ScaleRefBlockIn=ScaleRefBlock(KRefBlock)
      PwdMethodRefBlockIn=PwdMethodRefBlock(KRefBlock)
      TempRefBlockIn=TempRefBlock(KRefBlock)
      HKLF5RefBlockIn=HKLF5RefBlock(KRefBlock)
      if(UseTrRefBlockIn)
     1  call CopyVek(TrRefBlock(1,KRefBlock),TrIn,maxNDim**2)
      if(maxNDim.gt.3.and..not.isPowder) then
        call CopyVek(DiffSatRefBlock(1,KRefBlock),DiffSatIn,3)
        MMaxIn=MMaxRefBlock(KRefBlock)
      endif
      if(DifCode(KRefBlock).gt.0.and.DifCode(KRefBlock).le.100) then
        call CopyMat(UB(1,1,KRefBlock),UBIn,3)
        LamAveIn=LamAveRefBlock(KRefBlock)
        if(CellReadIn(KRefBlock)) then
          call CopyVek(CellRefBlock(1,KRefBlock),CellIn,6)
          call CopyVek(QuRefBlock(1,1,KRefBlock),QuIn,3*(maxNDim-3))
        else
          call SetRealArrayTo(CellIn,6,0.)
          call SetRealArrayTo(QuIn,3*(maxNDim-3),0.)
        endif
      else
        LamA1In=LamA1RefBlock(KRefBlock)
        LamA2In=LamA2RefBlock(KRefBlock)
      endif
      go to 9999
      entry EM9ImportRestorePar
      SourceFileRefBlock(KRefBlock)=SourceFileIn
      SourceFileDateRefBlock(KRefBlock)=SourceFileDateIn
      SourceFileTimeRefBlock(KRefBlock)=SourceFileTimeIn
      maxNDim=NDimIn
      NDim95(KRefBlock)=NDim95In
      DifCode(KRefBlock)=DifCodeIn
      ITwRead(KRefBlock)=ITwReadIn
      UseTrRefBlock(KRefBlock)=UseTrRefBlockIn
      RadiationRefBlock(KRefBlock)=RadiationIn
      PolarizationRefBlock(KRefBlock)=PolarizationIn
      AngleMonRefBlock(KRefBlock)=AngleMonIn
      FractPerfMonRefBlock(KRefBlock)=FractPerfMonIn
      TOFDifcRefBlock(KRefBlock)=TOFDifcRefBlockIn
      TOFDifaRefBlock(KRefBlock)=TOFDifaRefBlockIn
      TOFZeroRefBlock(KRefBlock)=TOFZeroRefBlockIn
      TOFZeroRefBlock(KRefBlock)=TOFTThRefBlockIn
      EDOffsetRefBlock(KRefBlock)=EDOffsetRefBlockIn
      EDSlopeRefBlock(KRefBlock)=EDSlopeRefBlockIn
      EDTThRefBlock(KRefBlock)=EDTThRefBlockIn
      ScaleRefBlock(KRefBlock)=ScaleRefBlockIn
      PwdMethodRefBlock(KRefBlock)=PwdMethodRefBlockIn
      TempRefBlock(KRefBlock)=TempRefBlockIn
      if(UseTrRefBlockIn)
     1  call CopyVek(TrIn,TrRefBlock(1,KRefBlock),maxNDim**2)
      if(maxNDim.gt.3.and..not.isPowder) then
        call CopyVek(DiffSatIn,DiffSatRefBlock(1,KRefBlock),3)
        MMaxRefBlock(KRefBlock)=MMaxIn
      endif
      if(DifCode(KRefBlock).gt.0.and.DifCode(KRefBlock).le.100) then
        call CopyMat(UBIn,UB(1,1,KRefBlock),3)
        LamAveRefBlock(KRefBlock)=LamAveIn
        if(CellReadIn(KRefBlock)) then
          call CopyVek(CellIn,CellRefBlock(1,KRefBlock),6)
          call CopyVek(QuIn,QuRefBlock(1,1,KRefBlock),3*(maxNDim-3))
        endif
      else
        LamA1RefBlock(KRefBlock)=LamA1In
        LamA2RefBlock(KRefBlock)=LamA2In
      endif
9999  return
      end
      subroutine EM9NejdouPotvory(n,ih,h,ri,rs,nd,RealIndices,ObsLim)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension ih(*),h(*)
      logical RealIndices
      pom=anint(ri*10.)/anint(rs*10.)
      SumRelI(n)=SumRelI(n)+pom
      NExt(n)=NExt(n)+1
      if(pom.gt.ObsLim) then
        NExtObs(n)=NExtObs(n)+1
        if(pom.gt.RIExtMax(n)) then
          if(NExt500(n).lt.500) then
           NExt500(n)=NExt500(n)+1
            k=NExt500(n)
          else
            k=OrderExtRef(500,n)
          endif
          ria(k,n)=-nint(pom*10000.)
          rsa(k,n)= nint( rs*100.)
          if(RealIndices) then
            call CopyVek(h,HExt(1,k,n),3)
          else
            call CopyVekI(ih,IHExt(1,k,n),nd)
          endif
          if(NExt500(n).ge.500) then
            call Indexx(500,ria(1,n),OrderExtRef(1,n))
            RIExtMax(n)=-float(ria(OrderExtRef(500,n),n))*.0001
          endif
        endif
      endif
      return
      end
      integer function EM9SortIndex(ihref)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ihref(6),hh(6),hhp(6),ihp(6),ihpp(6),mmabs(3)
      logical EqIV,EqIVM
      EM9SortIndex=9
      do i=1,NComp(KPhase)
        mmabs(i)=NSatGroups+1
        mm=0
        do j=1,NDim(KPhase)
          hh(j)=ihref(j)
        enddo
        call multm(hh,zvi(1,i,KPhase),hhp,1,NDim(KPhase),NDim(KPhase))
        do j=1,NDim(KPhase)
          ihp(j)=nint(hhp(j))
        enddo
        do j=1,NSymm(KPhase)
          call MultMIRI(ihp,Rm6(1,j,i,KPhase),ihpp,1,NDim(KPhase),
     1                  NDim(KPhase))
          do m=0,NSatGroups
            if(EqIV (ihpp(4:),HSatGroups(1,m),NDimI(KPhase)).or.
     1         EqIVM(ihpp(4:),HSatGroups(1,m),NDimI(KPhase))) then
              mm=1
              mmabs(i)=min(mmabs(i),m)
            endif
          enddo
          if(mm.eq.0) mmabs(i)=min(mmabs(i),NSatGroups+1)
        enddo
        EM9SortIndex=min(mmabs(i),EM9SortIndex)
      enddo
      EM9SortIndex=EM9SortIndex+2
      return
      end
      subroutine EM9CheckCompleteness(ThMax,LamUsed,mmax)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension sp(:),ih(6),hn(6),har(:,:),h(6),hp(6),NGen(8),NMer(8),
     1          PerArr(8),mmax(3,*),rtw6(36,MxPhases),rtw6i(36,MxPhases)
      character*40 Veta
      integer flag(:),KPhA(:)
      logical EqRV
      real LamUsed
      allocatable sp,flag,har,KPhA
      do KPhase=1,NPhase
        if(NDim(KPhase).eq.MaxNDim) go to 1050
      enddo
      KPhase=1
1050  call iom50(0,0,fln(:ifln)//'.m50')
      if(kcommenMax.ne.0) call comsym(0,1,ich)
      call OpenFile(91,fln(:ifln)//'.l93','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(LamUsed.gt.0.) then
        snthlmx=sin(ThMax*ToRad)/LamUsed
      else
        snthlmx=ThMax
        LamUsed=.5/snthlmx
        ThMax=asin(.5)/ToRad
      endif
      call GenerRef('Calculating coverage statistics',snthlmx,mmax,
     1              .true.,0)
      if(ErrFlag.ne.0) go to 9999
      if(KCommen(KPhase).eq.0) then
        NDimP=NDim(KPhase)
      else
        NDimP=3
      endif
      rewind 91
      nref=0
1100  read(91,Format91,end=1200)(ih(i),i=1,maxNDim),f,f,i,i,KPh,s
      if(ih(1).gt.900) go to 1200
      nref=nref+1
      go to 1100
1200  allocate(har(NDim(KPhase),nref),sp(nref),Flag(nref),KPhA(nref))
      rewind 91
      k=0
1300  read(91,Format91,end=1500)(ih(i),i=1,maxNDim),f,f,i,i,KPh,s
      if(ih(1).gt.900) go to 1500
      k=k+1
      KPhase=KPh
      call FromIndSinthl(ih,h,sp(k),sinthlq,1,0)
      call CopyVek(h,har(1,k),3)
      KPhA(k)=KPh
      go to 1300
1500  close(91,status='delete')
      do KPh=1,NPhase
        do i=1,NTwin
          if(KPhaseTwin(i).eq.KPh) then
            call CopyMat(rtw (1,i),rtw6 (1,KPh),3)
            call matinv(rtw6(1,KPh),rtw6i(1,KPh),pom,3)
c            call CopyMat(rtwi(1,i),rtw6i(1,KPh),3)
            exit
          endif
        enddo
      enddo
      do KPh=1,NPhase
        KPhase=KPh
        if(Grupa(KPh).ne.' '.and.index(Grupa(KPh),'?').le.0) then
          Veta='Coverage statistics for '//Grupa(KPh)(:idel(Grupa(KPh)))
        else
          Veta='Coverage statistics for symmetry defined above'
        endif
        if(NPhase.gt.1)
     1    Veta=Veta(:idel(Veta))//' - '//PhaseName(KPh)
        call newln(3)
        call TitulekVRamecku(Veta)
        Veta='sin(theta)/lambda '
        write(lst,'(a39,8f10.6)') Veta,sinmez
        call OpenFile(91,DatBlockFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do k=1,2
          call SetIntArrayTo(Flag,nref,0)
          rewind 91
2100      read(91,Format91,end=3000)(ih(i),i=1,maxNDim),f,sf,i,i,
     1                              itw
          if(ih(1).gt.900) go to 3000
          if(itw.lt.0) then
            itw=mod(-itw,100)
          else if(itw.gt.100) then
            itw=mod(itw,100)
          endif
          KPhP=KPhaseTwin(itw)
          KPhase=KPhP
          call FromIndSinthl(ih,h,s,sinthlq,1,0)
          if(KPhp.ne.KPh) then
            call MultM(h,Rtw6I(1,KPhP),hp,1,3,3)
            call MultM(hp,RTw6(1,KPh),h,1,3,3)
          endif
          if(k.eq.1.and.f.lt.EM9ObsLim(KDatBlock)*sf) go to 2100
          ip=1
          ik=NRef
2150      if(ik.le.ip+1) go to 2200
          ipul=(ik+ip)/2
          if(s.lt.sp(ipul)) then
            ik=ipul
            go to 2150
          else if(s.gt.sp(ipul)) then
            ip=ipul
            go to 2150
          else
            ip=ipul
            ik=ipul
            go to 2200
          endif
2200      ip=max(ip-10,1)
          ik=min(ik+10,NRef)
          do j=1,NSymmN(KPh)
            call MultM(h,rm(1,j,1,KPh),hp,1,3,3)
            call RealVectorToOpposite(hp,hn,3)
            do i=ip,ik
              if(KPhA(i).ne.KPh) cycle
              if(eqrv(hp,har(1,i),3,.001).or.eqrv(hn,har(1,i),3,.001))
     1          then
                flag(i)=1
                go to 2100
              endif
            enddo
          enddo
          go to 2100
3000      call SetIntArrayTo(NMer,8,0)
          call SetIntArrayTo(NGen,8,0)
          do i=1,NRef
            if(KPhA(i).ne.KPh) cycle
            do j=1,8
              if(sp(i).le.sinmez(j)) then
                if(Flag(i).gt.0) NMer(j)=NMer(j)+1
                NGen(j)=NGen(j)+1
              endif
            enddo
          enddo
          do i=1,8
            PerArr(i)=float(NMer(i))/float(NGen(i))*100.
          enddo
          if(k.eq.1) then
            Veta='Coverage in  % for observed reflections'
          else
            Veta='Coverage in  % for all reflections'
          endif
          write(lst,'(a39,8f10.2)') Veta,PerArr
        enddo
        close(91)
        PerLimitMax=min(PerArr(8),100.)
        ppp=min(sinmez(8)*LamUsed,.999999)
        ThMax=asin(ppp)/ToRad
        PerLimit=111.
        call newln(3)
        Veta='Coverage        Theta'
        write(lst,'(/5x,a/)') Veta(:idel(Veta))
        if(PerLimitMax.ge.98.) then
          PerLimit=PerLimitMax
          ThFull=ThMax
          write(lst,100) PerLimit,ThFull
          go to 9999
        endif
        do n=0,100
          PerLimitP=float(ifix(PerLimitMax))+float(n)
          if(PerLimitP.gt.100.1) cycle
          PerLimitP=min(PerLimitP,99.96)
          do i=8,1,-1
            if(PerArr(i).gt.PerLimitP) go to 3540
          enddo
          go to 3590
3540      if(i.eq.8) then
            ThFullP=ThMax
            PerLimitP=PerArr(8)
            go to 3580
          endif
          SThP=sinmez(i)
          SThK=sinmez(i+1)
3550      SThPul=(SThK+SThP)*.5
          if(SThK-SthP.le..00001) then
            pom=min(SThPul*LamUsed,.999999)
            ThFullP=asin(pom)/ToRad
            go to 3580
          endif
          NMerP=0
          NGenP=0
          do i=1,NRef
            if(KPhA(i).ne.KPh) cycle
            if(sp(i).le.SThPul) then
              if(Flag(i).gt.0) NMerP=NMerP+1
              NGenP=NGenP+1
            endif
          enddo
          pom=float(NMerP)/float(NGenP)*100.
          if(pom.gt.PerLimitP) then
            SThP=SThPul
          else if(pom.lt.PerLimitP) then
            SThK=SThPul
          else
            SThP=SThPul
            SThK=SThPul
          endif
          go to 3550
3580      call newln(1)
          write(lst,100) PerLimitP,ThFullP
          if(PerLimitP.le.98.) then
            PerLimit=PerLimitP
            ThFull=ThFullP
          endif
          cycle
3590      call newln(1)
          write(lst,'(5x,f6.2,''%       ---------'')') PerLimitP
          exit
        enddo
        call newln(3)
        write(lst,FormA)
        write(lst,'('' Fraction '',f6.2,''% for theta(max) '',f8.2)')
     1    PerLimitMax*.01,ThMax
        write(lst,'('' Fraction '',f6.2,''% for theta(full)'',f8.2)')
     1    PerLimit*.01,ThFull
      enddo
9999  if(kcommenMax.ne.0) call iom50(0,0,fln(:ifln)//'.m50')
      if(allocated(Flag)) deallocate(flag,har,sp,KPhA)
      return
100   format(5x,f6.2,'%       ',f5.2,' deg')
      end
      subroutine EM9ImportMagnetic(WhatToDo,CoDal)
      use Atoms_mod
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(36),xp(3)
      character*80  Veta
      integer WhatToDo,CoDal
      logical StructureExists,UzTuByl
      equivalence (IdNumbers(0),IdMagneticManual),
     1            (IdNumbers(1),IdMagneticSHELX),
     2            (IdNumbers(2),IdMagneticCIF),
     3            (IdNumbers(3),IdMagneticJana)
      UzTuByl=.false.
1100  if(WhatToDo.eq.IdMagneticManual) then
        if(UzTuByl) then
          go to 1300
        else
          call FeFillTextInfo('em9importmagnetic1.txt',0)
          call FeWizardTextInfo('INFORMATION',1,1,1,ich)
          if(ich.ne.0) then
            CoDal=ich
            go to 9999
          endif
          call SetBasicM40(.true.)
        endif
1200    ExistM90=.true.
        call EditM50(ich)
        ExistM90=.false.
        call iom50(0,0,Fln(:iFln)//'.m50')
        if(ich.ne.0) then
          CoDal=ich
          go to 9999
        endif
        UzTuByl=.true.
        call CopyFile(Fln(:iFln)//'.m50',PreviousM50)
1300    if(NAtFormula(KPhase).gt.0) then
          call EM40NewAt(ich)
          if(ich.ne.0) then
            if(ich.gt.0) then
              UzTuByl=.false.
              go to 1200
            else
              CoDal=ich
              go to 9999
            endif
          endif
          call iom40Only(1,0,Fln(:iFln)//'.m40')
          call CopyFile(Fln(:iFln)//'.m40',PreviousM40)
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticSHELX) then
        call ReadSHELX(-1,ich)
        if(ErrFlag.ne.0) then
          ich=1
          go to 9900
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticCIF) then
        call DeleteFile(fln(:ifln)//'.m90')
        call DeleteFile(fln(:ifln)//'.m95')
        ExistM90=.false.
        ExistM95=.false.
        call ReadCIF(1,CoDal)
        if(ErrFlag.ne.0) then
          ich=1
          go to 9900
        endif
        if(isPowder) then
          call DeleteFile(fln(:ifln)//'.m41')
          isPowder=.false.
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticJana) then
        Veta=' '
2100    call FeFileManager('Define input Jana structure',Veta,' ',1,
     1                     .false.,ich)
        if(ich.ne.0) go to 9900
        if(.not.StructureExists(Veta)) then
          call FeChybne(-1.,-1.,'The structure "'//
     1                  Veta(:idel(Veta))//'" doesn''t exist.',
     2                  ' ',SeriousError)
          go to 2100
        endif
        call iom50(0,0,Veta(:idel(Veta))//'.m50')
        call iom40Only(0,0,Veta(:idel(Veta))//'.m40')
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      NPhase=1
      KPhase=1
      KQMag(KPhase)=0
      call SetRealArrayTo(QMag(1,KPhase),3,0.)
      call DeleteFile(Veta(:idel(Veta))//'.l51')
      call SetRealArrayTo(sc ,MxSc,0.)
      call SetRealArrayTo(scs,MxSc,0.)
      sc(1,1)=1.
      call SetRealArrayTo(sctw ,MxSc,0.)
      call SetRealArrayTo(sctws,MxSc,0.)
      OverAllB (1)=0.
      OverAllBs(1)=0.
      call SetRealArrayTo(ec ,12,0.)
      call SetRealArrayTo(ecs,12,0.)
      call SetRealArrayTo(ecMag ,2,0.)
      call SetRealArrayTo(ecsMag,2,0.)
      call CrlAtomNamesIni
      if(NDimI(KPhase).gt.0) then
        do j=1,NComp(KPhase)
          do i=1,NSymm(KPhase)
            call MatBlock3(rm6(1,i,j,KPhase),rmp,NDim(KPhase))
            call CopyMat(rmp,rm6(1,i,j,KPhase),3)
            call SetRealArrayTo(s6(4,i,j,KPhase),NDimI(KPhase),0.)
            ZMag(i,j,KPhase)=1.
          enddo
          do i=1,NLattVec(KPhase)
            call SetRealArrayTo(vt6(4,i,j,KPhase),NDimI(KPhase),0.)
          enddo
        enddo
        NDim(KPhase)=3
        NDimI(KPhase)=0
        NDimQ(KPhase)=9
        call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      endif
      do i=1,NAtAll
        MagPar(i)=0
        call SetIntArrayTo(KModA(1,i),7,0)
        call SetIntArrayTo(KFA(1,i),7,0)
        sai(i)=0.
        if(itf(i).ge.2) then
          call SetRealArrayTo(sx(1,i),3,0.)
          call SetRealArrayTo(sbeta(1,i),6,0.)
          call ZmTF21(i)
        endif
      enddo
      MagneticType(KPhase)=1
      MaxMagneticType=1
      if(.not.allocated(ZMag)) then
        allocate(ZMag(NSymm(KPhase),NComp(KPhase),NPhase),
     1           RMag(9,NSymm(KPhase),NComp(KPhase),NPhase))
        call SetRealArrayTo(ZMag,NSymm(KPhase)*NComp(KPhase)*NPhase,1.)
        call SetRealArrayTo(RMag,9*NSymm(KPhase)*NComp(KPhase)*NPhase,
     1                      0.)
      endif
      n=NAtAll
      if(.not.allocated(NamePolar))
     1  allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
      call EM9ImportMagneticFF(CoDal)
      if(CoDal.eq.0) then
        Radiation(1)=NeutronRadiation
        LamAve(1)=1.
        LamA1(1)=1.
        LamA2(1)=1.
        LamRat(1)=0.
        LPFactor(1)=PolarizedLinear
        ParentStructure=.true.
        NCommQProduct(1,KPhase)=1
        if(KQMag(KPhase).gt.0) then
          do 3250j=1,2
            do i=1,3
              xp(i)=QMag(i,KPhase)*float(j)
              if(abs(anint(xp(i))-xp(i)).gt..001) go to 3250
            enddo
            KCommen(KPhase)=1
            ICommen(KPhase)=0
            KCommenMax=1
            do i=1,3
              if(xp(i).le.0.) then
                NCommen(i,1,KPhase)=1
              else
                NCommen(i,1,KPhase)=j
              endif
            enddo
            NCommQProduct(1,KPhase)=NCommQProduct(1,KPhase)*j
            trez(1,1,KPhase)=0.
            exit
3250      continue
        endif
        call QMag2SSG(fln,0)
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40Only(0,0,fln(:ifln)//'.m44')
      else if(WhatToDo.eq.IdMagneticManual) then
        if(CoDal.gt.0) go to 1100
      endif
      go to 9999
9900  if(ich.ne.0) then
        CoDal=-1
      endif
9999  return
      end
      subroutine EM9ImportMagneticFF(ich)
      use Atoms_mod
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80  Veta,FormulaOld
      character*7   At,AtTypeMagP
      character*5  MenuJ(10,5)
      integer RolMenuSelectedQuest,nCrwJType(0:5),nRolMenuJType(5),
     1        NMenuJ(5),IMenuJ(5)
      logical CrwLogicQuest,EqRV0,MagneticAtom,EqIgCase,ExistM90In
1100  ExistXRayData=.false.
      ExistNeutronData=.true.
      ExistM90In=ExistM90
      ExistM90=.true.
      id=NextQuestId()
      Veta='Define magnetic propagation vector and form factors'
      call FeQuestTitleMake(id,Veta)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+5.
      Veta='Use non-zero %magnetic propagation vector'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseQMag=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KQMag(KPhase).gt.0)
      tpom=tpom+FeTxLengthUnder(Veta)+10.
      Veta='=>'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=WizardLength-xpom-100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwQMag=EdwLastMade
      if(KQMag(KPhase).gt.0)
     1  call FeQuestRealAEdwOpen(nEdwQMag,QMag(1,KPhase),3,.false.,
     2                           .false.)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Formula %units'
      xpome=tpom+FeTxLengthUnder(Veta)+10.
      dpom=300.
      call FeQuestEdwMake(id,tpom,il,xpome,il,'%Formula','L',dpom,EdwYd,
     1                    1)
      nEdwFormula=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
      il=il+1
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwZ=EdwLastMade
      if(NUnits(KPhase).le.0) then
        if(NSymmN(KPhase).gt.0.and.NLattVec(KPhase).gt.0) then
          NUnits(KPhase)=NLattVec(KPhase)*NSymmN(KPhase)
        else
          NUnits(KPhase)=1
        endif
      endif
      call FeQuestIntEdwOpen(nEdwZ,NUnits(KPhase),.false.)
      Veta='Calculate %density'
      xpom=xpome+dpom+36.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      if(Formula(KPhase).ne.' ') call FeQuestButtonOff(ButtonLastMade)
      xpom=xpom+dpom+5.
      il=il+1
      Veta='%Atom type'
      dpomr=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpome,il,Veta,'L',dpomr,EdwYd,
     1                        1)
      nRolMenuAtType=RolMenuLastMade
      xpom=xpome+dpomr+20.
      Veta='Own scattering length for %neutrons'
      dpom=FeTxLengthUnder(Veta)+10
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefineFF=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=xpom+dpom+20.
      Veta='Use as a ma%gnetic atom'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwMagnetic=CrwLastMade
      tpom=15.
      Veta='%Own formfactors'
      xpom=tpom
      tpom=xpom+CrwXd+10.
      xpomp=tpom+FeTxLengthUnder(Veta)+100.
      dpom=80.
      MenuJ(1,1)=' '
      NMenuJ(1)=1
      do i=0,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                      1)
        if(i.eq.0) then
          Veta='%Edit'
          call FeQuestButtonMake(id,xpomp,il,dpom,ButYd,Veta)
          nButtOwn=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonDisable(ButtonLastMade)
        else
          call FeQuestRolMenuMake(id,tpom,il,xpomp,il,'  ','L',dpom,
     1                            EdwYd,1)
          call FeQuestRolMenuOpen(RolMenuLastMade,MenuJ(1,1),NMenuJ(1),
     1                            1)
          call FeQuestRolMenuDisable(RolMenuLastMade)
          nRolMenuJType(i)=RolMenuLastMade
        endif
        if(i.lt.4) then
          write(Veta,'(''Magnetic formfactor <j%'',i1,''>'')') 2*i
        else if(i.eq.4) then
          Veta='Magnetic formfactor <j0>%+c<j2>'
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.0)
        call FeQuestCrwDisable(CrwLastMade)
        nCrwJType(i)=CrwLastMade
      enddo
      LastAtomOld=-1
      LastAtom=1
      if(NAtFormula(KPhase).gt.0) then
        MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
      else
        MagneticAtom=.false.
      endif
1400  if(LastAtom.ne.LastAtomOld.and.NAtFormula(KPhase).gt.0) then
        call FeQuestRolMenuOpen(nRolMenuAtType,AtType(1,KPhase),
     1                          NAtFormula(KPhase),LastAtom)
        call FeQuestCrwOpen(nCrwMagnetic,MagneticAtom)
      endif
      if(MagneticAtom) then
        if(LastAtom.ne.LastAtomOld) then
          call EM50ReadMagneticFFLabels(AtType(LastAtom,KPhase),MenuJ,
     1                                  NMenuJ,ierr)
          if(AtTypeMag(LastAtom,KPhase).eq.' ') then
          if(NMenuJ(1).gt.0) then
              AtTypeMag(LastAtom,KPhase)=MenuJ(NMenuJ(1),1)
              AtTypeMagJ(LastAtom,KPhase)='j0'
              Veta='Magnetic_formfactor_<j0>'
              call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1                               AtTypeMag(LastAtom,KPhase),Veta,
     2                               FFMag(1,LastAtom,KPhase),ich)
            else
              AtTypeMag(LastAtom,KPhase)=AtType(LastAtom,KPhase)
              AtTypeMagJ(LastAtom,KPhase)='own'
              TypeFFMag(LastAtom,KPhase)=0
              call SetRealArrayTo(FFMag(1,LastAtom,KPhase),7,0.)
            endif
          endif
          if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'own')) then
            JAtP=0
          else if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'j0j2')) then
            JAtP=5
          else
            JAtP=1
            read(AtTypeMagJ(LastAtom,KPhase)(2:2),'(i1)',err=1450) i
            JAtP=i/2+1
          endif
1450      call FeQuestCrwOpen(nCrwJType(0),JAtP.eq.0)
          if(JAtP.eq.0) then
            call FeQuestButtonOpen(nButtOwn,ButtonOff)
            nRolMenuLast=0
          else
            call FeQuestButtonDisable(nButtOwn)
          endif
          do i=1,5
            if(NMenuJ(i).gt.0) then
              call FeQuestCrwOpen(nCrwJType(i),JAtP.eq.i)
            else
              call FeQuestCrwDisable(nCrwJType(i))
            endif
          enddo
          AtTypeMagP=AtTypeMag(LastAtom,KPhase)
          do i=1,5
            if(NMenuJ(i).gt.0) then
              IMenuJ(i)=
     1          max(LocateInStringArray(MenuJ(1,i),NMenuJ(i),
     2                                  AtTypeMagP,IgnoreCaseYes),1)
              if(i.eq.JAtP) then
                call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                  NMenuJ(i),IMenuJ(i))
                nRolMenuLast=nRolMenuJType(i)
              else
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            else
              IMenuJ(i)=0
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            endif
          enddo
        endif
      else
        call FeQuestButtonDisable(nButtOwn)
        call FeQuestCrwDisable(nCrwJType(0))
        do i=1,5
          call FeQuestCrwDisable(nCrwJType(i))
          call FeQuestRolMenuDisable(nRolMenuJType(i))
        enddo
      endif
      LastAtomOld=LastAtom
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseQMag) then
        if(CrwLogicQuest(CheckNumber)) then
          KQMag(KPhase)=1
          call FeQuestRealAEdwOpen(nEdwQMag,QMag(1,KPhase),3,
     1                             EqRV0(QMag(1,KPhase),3,.0001),
     2                             .false.)
        else
          call FeQuestRealAFromEdw(nEdwQMag,QMag(1,KPhase))
          call FeQuestEdwClose(nEdwQMag)
          KQMag(KPhase)=0
        endif
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFormula) then
        FormulaOld=Formula(KPhase)
        NAtFormulaOld=NAtFormula(KPhase)
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        Veta=Formula(KPhase)
        call EM50SaveFormFactors
        do i=1,idel(Veta)
          if(index(Cifry,Veta(i:i)).gt.0) Veta(i:i)=' '
        enddo
        do i=1,idel(FormulaOld)
          if(index(Cifry,FormulaOld(i:i)).gt.0) FormulaOld(i:i)=' '
        enddo
        call PitFor(1,ichp)
        if(ichp.ne.0) go to 1520
        if(NAtFormula(KPhase).gt.0) then
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          if(NAtFormulaOld.le.0) FFType(KPhase)=-62
        else
          go to 1500
        endif
        call PitFor(0,ichp)
        if(ichp.ne.0) go to 1520
        if(NAtFormulaOld.gt.0) then
          call EM50SmartUpdateFormFactors
        else
          do i=1,NAtFormula(KPhase)
            FFBasic(1,i,KPhase)=-3333.
            AtRadius(i,KPhase)=-3333.
          enddo
        endif
        call FeQuestButtonOff(nButtCalculateDensity)
        do i=1,NAtFormula(KPhase)
          if(AtRadius(i,KPhase).lt.-3000.)
     1      call EM50ReadOneFormFactor(i)
          if(FFBasic(1,i,KPhase).lt.-3000.)
     1      call EM50OneFormFactorSet(i)
        enddo
        LastAtomOld=-1
        go to 1400
1520    EventType=CheckType
        EventNumber=CheckNumber
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtCalculateDensity) then
        ExistM90=ExistM90In
        call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        if(Formula(KPhase).ne.' ') call PitFor(0,ichp)
        NInfo=0
        call EM50CalcDenAbs(ichp)
        if(ichp.ne.0)
     1    TextInfo(1)='Error during calculation of density and '//
     2                'absorption coefficient.'
        NInfo=NInfo-1
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        ExistM90=.true.
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwJType(0).and.
     2         CheckNumber.le.nCrwJType(5))) then
        if(CheckNumber.eq.nCrwJType(0)) then
          call FeQuestButtonOff(nButtOwn)
          do i=1,5
            call FeQuestRolMenuDisable(nRolMenuJType(i))
          enddo
          RolMenuLast=0
          AtTypeMag(LastAtom,KPhase)=AtTypeMag(LastAtom,KPhase)
          AtTypeMagJ(LastAtom,KPhase)='own'
          call EM50ReadOwnFormFactorMag(LastAtom)
        else
          call FeQuestButtonDisable(nButtOwn)
          if(nRolMenuLast.gt.0)
     1      IMenuJ(nRolMenuLast-nRolMenuJType(1)+1)=
     2        RolMenuSelectedQuest(nRolMenuLast)
          do i=1,5
            if(CheckNumber.eq.nCrwJType(i)) then
              call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                NMenuJ(i),IMenuJ(i))
              nRolMenuLast=nRolMenuJType(i)
              AtTypeMag(LastAtom,KPhase)=MenuJ(IMenuJ(i),i)
              if(i.eq.5) then
                AtTypeMagJ(LastAtom,KPhase)='j0j2'
                Veta='Magnetic_formfactor_<j0>+c<j2>'
              else
                write(Cislo,'(i1)') 2*(i-1)
                AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
                Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
              endif
            else
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            endif
          enddo
          call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1      AtTypeMag(LastAtom,KPhase),Veta,
     2      FFMag(1,LastAtom,KPhase),ich)
          Klic=0
          go to 1400
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtType) then
        LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
        if(LastAtomOld.ne.LastAtom) then
          MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
          Klic=0
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.ge.nRolMenuJType(1).and.
     2         CheckNumber.le.nRolMenuJType(5))) then
        nRolMenuLast=CheckNumber
        i=nRolMenuLast-nRolMenuJType(1)+1
        AtTypeMag(LastAtom,KPhase)=
     1    MenuJ(RolMenuSelectedQuest(nRolMenuLast),i)
        if(i.eq.5) then
          AtTypeMagJ(LastAtom,KPhase)='j0j2'
          Veta='Magnetic_formfactor_<j0>+c<j2>'
        else
          write(Cislo,'(i1)') 2*(i-1)
          AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
          Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
        endif
        call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1    AtTypeMag(LastAtom,KPhase),Veta,
     2    FFMag(1,LastAtom,KPhase),ich)
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMagnetic)
     1  then
        MagneticAtom=CrwLogicQuest(nCrwMagnetic)
        if(.not.MagneticAtom) then
          AtTypeMag(LastAtom,KPhase)=' '
          AtTypeMagJ(LastAtom,KPhase)=' '
        endif
        LastAtomOld=-1
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOwn) then
        call EM50ReadOwnFormFactorMag(LastAtom)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefineFF)
     1  then
        if(NAtFormula(KPhase).gt.0) then
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
        endif
        call EM50ReadOwnFormFactor(LastAtom)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealAFromEdw(nEdwQMag,QMag(1,KPhase))
        if(EqRV0(QMag(1,KPhase),3,.0001)) then
          call SetRealArrayTo(QMag(1,KPhase),3,0.)
          KQMag(KPhase)=0
        else
          KQMag(KPhase)=1
        endif
      else
        go to 9999
      endif
      ExistM90=ExistM90In
      call FeFillTextInfo('em9importmagnetic2.txt',0)
      call FeWizardTextInfo('INFORMATION',1,-1,1,ich)
      if(ich.gt.0) go to 1100
9999  return
      end
      subroutine ChangeOrderM90
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,RefSt(:)
      integer isina(:),ipor(:)
      allocatable RefSt,isina,ipor
      if(iabs(DataType(KDatBlock)).eq.2) go to 9999
      call OpenDatBlockM90(90,KDatBlock,fln(:ifln)//'.m90')
      write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
      allocate(RefSt(NRef90(KDatBlock)),isina(NRef90(KDatBlock)),
     1         ipor(NRef90(KDatBlock)))
1100  do i=1,NRef90(KDatBlock)
        read(90,FormA) RefSt(i)
        read(RefSt(i),'(6i4)')(ih(j),j=1,NDim(KPhase))
        call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
        isina(i)=nint(sinthl*1000000.)
      enddo
      close(90)
      call indexx(NRef90(KDatBlock),isina,ipor)
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      Veta=fln(:ifln)//Cislo(:idel(Cislo))
      call OpenFile(90,Veta,'formatted','unknown')
      do i=1,NRef90(KDatBlock)
        j=ipor(i)
        write(90,FormA) RefSt(j)(:idel(RefSt(j)))
      enddo
      close(90)
      call CompleteM90
9999  if(allocated(RefSt)) deallocate(RefSt,isina,ipor)
      return
      end
      subroutine EM9ManualCull
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension xp(3),xo(3),xsel(5),ysel(5),RFac(5),xoc(3),ICullArIn(:),
     1          ICullArOld(:)
      character*256 Veta,SavedM40
      character*80  SvFile,t80
      character*60  format83a
      character*17 :: Labels(19) =
     1              (/'%Quit            ',
     2                'X=I(Min)         ',
     3                'X=I(ave)         ',
     4                'X=I(max)         ',
     5                'X=theta          ',
     6                'Un%zoom          ',
     7                'Run %average     ',
     8                'A%verage report  ',
     9                '%Edit atoms      ',
     a                'Edit m%40 file   ',
     1                'R%un refine      ',
     2                'Refine re%port   ',
     3                'Refine %commands ',
     4                'Run %Fourier     ',
     5                'Fourier co%mmands',
     6                'Run Co%ntour     ',
     7                '%Reset culled    ',
     8                'Satellite fil%ter',
     9                '%Options         '/)
      character*50 :: Menu(6) =
     1           (/'%Escape                                        ',
     2             'Make %zoom                                     ',
     3             'Start culling %dialog                          ',
     4             'Cull %minimal unculled item for each reflection',
     5             'Cull ma%ximal unculled item for each reflection',
     6             'Cull all items - delete reflections            '/)
      integer CullQuest,Color,ColorGasket,ColorDiamond,FeRGBCompress,
     1        CoDal,FeMenu,XUseOld,YUseOld,EM9SortIndex
      logical FeYesNo,EqIV,ExistFile,Diff,FileDiff,RefineEnd
      allocatable ICullArIn,ICullArOld
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          KRefBlock=i
          exit
        endif
      enddo
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      SavedM40='jm40'
      call CreateTmpFile(SavedM40,i,0)
      call FeTmpFilesAdd(SavedM40)
      call EM9ManualFilter(0)
      call EM9ManualCullOptions(0)
      call FeRGBUncompress(Magenta,IRed,IGreen,IBlue)
      IRed=IRed*.2
      IGreen=IGreen*.2
      IBlue=IBlue*.2
      ColorGasket=FeRGBCompress(IRed,IGreen,IBlue)
      call FeRGBUncompress(Cyan,IRed,IGreen,IBlue)
      IRed=IRed*.2
      IGreen=IGreen*.2
      IBlue=IBlue*.2
      ColorDiamond=FeRGBCompress(IRed,IGreen,IBlue)
      NAve=0
      NAveRed=0
      NAveMax=0
      NAveRedMax=0
      NAveRed=0
      NRefRead=0
      if(allocated(HCondAveAr))
     1  deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     2             Ave2Org,RunCCDAr)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      call DRSetRefArrays(1)
      if(NRefRead.le.0) then
        call FeChybne(-1.,-1.,'No reflection read from your '//
     1                'repository file.',
     2                'The file does not contaion symmetry related '//
     3                'reflections.',SeriousError)
        go to 9999
      endif
      allocate(ICullArIn(NRefRead),ICullArOld(NRefRead))
      call CopyVekI(ICullAr,ICullArIn,NRefRead)
      FLimCullIn=FLimCull(KDatBlock)
      CullQuest=NextQuestId()
      CheckMouse=.true.
      call FeQuestAbsCreate(CullQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,120.,2.*YBottomMargin,24.)
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=100.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      k=1
      ik=19
      do i=1,ik
        call FeQuestAbsButtonMake(CullQuest,xpom,ypom,wpom,ButYd,
     1                            Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtIMin=ButtonLastMade
          nButtFobs=ButtonLastMade
        else if(i.eq.3) then
          nButtIAve=ButtonLastMade
        else if(i.eq.4) then
          nButtIMax=ButtonLastMade
        else if(i.eq.5) then
          nButtTheta=ButtonLastMade
        else if(i.eq.6) then
          nButtUnzoom=ButtonLastMade
        else if(i.eq.7) then
          nButtAverage=ButtonLastMade
        else if(i.eq.8) then
          nButtAverageReport=ButtonLastMade
        else if(i.eq.9) then
          nButtEditAtoms=ButtonLastMade
        else if(i.eq.10) then
          nButtEditM40=ButtonLastMade
        else if(i.eq.11) then
          nButtRefine=ButtonLastMade
        else if(i.eq.12) then
          nButtRefineReport=ButtonLastMade
        else if(i.eq.13) then
          nButtRefineCommands=ButtonLastMade
        else if(i.eq.14) then
          nButtFourier=ButtonLastMade
        else if(i.eq.15) then
          nButtFourierCommands=ButtonLastMade
        else if(i.eq.16) then
          nButtContour=ButtonLastMade
        else if(i.eq.17) then
          nButtResetCulled=ButtonLastMade
        else if(i.eq.18) then
          nButtFilter=ButtonLastMade
        else if(i.eq.19) then
          nButtOptions=ButtonLastMade
        endif
        if(i.eq.18.and.NDimI(KPhase).le.0) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.1.or.i.eq.6.or.i.eq.8.or.i.eq.16.or.i.eq.19) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          ypom=ypom+2.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      Veta=' '
      do i=1,7
        if(i.eq.1.or.i.eq.5) then
          xpom=10.
          if(i.eq.1) then
            ypom=YBottomMargin+YBottomText-5.
          else
            ypom=YBottomText+5.
          endif
        else
          xpom=xpom+125.
          if(i.ne.4) xpom=xpom+65.
        endif
        call FeQuestAbsLblMake(CullQuest,xpom,ypom,Veta,'L','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) then
          nLblRint=LblLastMade
        else if(i.eq.2) then
          nLblPocet=LblLastMade
        else if(i.eq.3) then
          nLblRedundancy=LblLastMade
        else if(i.eq.4) then
          nLblCulled=LblLastMade
        else if(i.eq.5) then
          nLblRF=LblLastMade
        else if(i.eq.6) then
          nLblRwF=LblLastMade
        else if(i.eq.7) then
          nLblGOF=LblLastMade
        endif
      enddo
      XUseOld=-1
      YUseOld=-1
      ICullArOld(:)=-1
      FLimCullOld=FLimCull(KDatBlock)
1100  if(.not.EqIV(ICullAr,ICullArOld,NRefRead).or.
     1   FLimCull(KDatBlock).ne.FLimCullOld) then
        if(FLimCull(KDatBlock).ne.FLimCullOld) then
          call OpenFile(lst,fln(:ifln)//'.ave','formatted','unknown')
          LstOpened=.true.
          Uloha='From manual culling'
          call NewPg(1)
          call DRAverage(AveFromManualCull,1,ich)
          call CloseListing
        endif
        NInfo=2
        TextInfo(1)=' '
        TextInfo(2)=' '
        if(NRefBlockAr.gt.1.and.DiffScales(1).eq.1) then
          call DRMakeScales(1,1,ich)
          do i=2,MaxRefBlockAr
            call DRMakeScales(i,2,ich)
            if(ich.ne.0) go to 1110
          enddo
          call DRMakeScales(1,3,ich)
        endif
1110    call OpenFile(lst,fln(:ifln)//'.ave','formatted','unknown')
        LstOpened=.true.
        Uloha='From manual culling'
        call NewPg(1)
        call DRAverage(AveFromManualCull,1,ich)
        call CloseListing
        call CopyVekI(ICullAr,ICullArOld,NRefRead)
        FLimCullOld=FLimCull(KDatBlock)
      endif
      if(YUse.eq.YUseIAve) NPocet=NAve
      RIMaxMin=-99999999.
      RIMaxMax=-99999999.
      RIMaxAve=-99999999.
      RIMinMin= 99999999.
      RIMinMax= 99999999.
      RIMinAve= 99999999.
      DMax=0.
      NAveKresli=0
      do i=1,NAve
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
          else
            n=0
            do j=1,NAveRedAr(i)
              m=Ave2Org(j,i)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1120
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              cycle
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1120
            else
              cycle
            endif
          endif
        endif
1120    k=HCondAveAr(i)
        call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        RIMax=-99999999.
        RIMin= 99999999.
        nn=0
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).eq.1) cycle
          nn=nn+1
          RIMax=max(RIMax,RIAr(m))
          RIMin=min(RIMin,RIAr(m))
        enddo
        if(nn.le.0) cycle
        NAveKresli=NAveKresli+1
        RIMaxMin=max(RIMaxMin,RIMin)
        RIMaxMax=max(RIMaxMax,RIMax)
        RIMaxAve=max(RIMaxAve,RIAveAr(i))
        RIMinMin=min(RIMinMin,RIMin)
        RIMinMax=min(RIMinMax,RIMax)
        RIMinAve=min(RIMinAve,RIAveAr(i))
        DMax=max(DMax,DiffAveAr(i))
      enddo
      Veta=fln(:ifln)//'.m83'
      if(ExistFile(Veta)) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 1170
        endif
        read(ln,FormA) Veta
        i=LocateSubstring(Veta,'e',.false.,.true.)
        if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
          format83a=format83e
        else
          format83a=format83
        endif
        rewind ln
        NRefWdF=0
        FoMax=0.
        FcMax=0.
        wdFMax=0.
        NRefWdFMax=0
        call ReallocateWdFAr(NRefRead)
        go to 1150
1140    NRefWdF=NRefWdF-1
1150    read(ln,Format83a,end=1170,err=1170)(ih(i),i=1,maxNDim),pom,pom,
     1                                      pom,Cislo,itw,wdy,
     2                                      FoPom,FcPom,FsPom
        if(itw.le.0) go to 1150
        if(NRefWdF.ge.NRefWdFMax)
     1    call ReallocateWdFAr(NRefWdFMax+1000)
        NRefWdF=NRefWdF+1
        HCondWdFAr(NRefWdF)=IndPack(ih,HCondLn,HCondMx,NDim(KPhase))
        FoAr(NRefWdF)=FoPom
        FcAr(NRefWdF)=FcPom
        FsAr(NRefWdF)=FsPom
        WdFAr(NRefWdF)=wdy
        j=0
        do i=1,NSymm(KPhase)
          call MultMIRI(ih,rm6(1,i,1,KPhase),ihp,1,NDim(KPhase),
     1                  NDim(KPhase))
          k=IndPack(ihp,HCondLn,HCondMx,NDim(KPhase))
          j=LocateInIntArray(k,HCondAveAr,NAve)
          if(j.gt.0) exit
        enddo
        if(j.le.0) go to 1140
        FoInd2Ave(NRefWdF)=j
        i=j
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(FoAr(NRefWdF).le.2.*ObsLevelCull*FsAr(i)) go to 1140
          else
            n=0
            do j=1,NAveRedAr(i)
              m=Ave2Org(j,i)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1160
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              go to 1160
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3               n.eq.1)) then
              go to 1160
            else
              go to 1140
            endif
          endif
        endif
1160    k=HCondAveAr(i)
        call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) go to 1140
        endif
        FoMax=max(FoMax,FoPom)
        FcMax=max(FcMax,FcPom)
        wdFMax=max(wdFMax,abs(wdy))
        go to 1150
1170    call CloseIfOpened(ln)
        if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) NPocet=NRefWdF
      endif
      ThetaMin=0.
      ThetaMax=0.
      do i=1,NPocet
        if(YUse.eq.YUseIAve) then
          ii=i
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          ii=FoInd2Ave(i)
        endif
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(YUse.eq.YUseIAve) then
              if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
            else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
              if(FoAr(i).le.2.*ObsLevelCull*FsAr(i)) cycle
            endif
          else
            n=0
            do j=1,NAveRedAr(ii)
              m=Ave2Org(j,ii)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1180
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              cycle
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1180
            else
              cycle
            endif
          endif
        endif
1180    if(YUse.eq.YUseIAve) then
          k=HCondAveAr(i)
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          k=HCondWdFAr(i)
        endif
        call IndUnPack(k,ih,HCondLn,HCondMx,NDim(KPhase))
        call FromIndSinthl(ih,xo,sinthl,sinthlq,1,0)
        pom=min(sinthl*LamAveRefBlock(KRefBlock),.999999)
        Theta=asin(pom)/ToRad
        ThetaMax=max(ThetaMax,Theta)
      enddo
1200  if((YUse.eq.YUseIAve.and.NAveKresli.le.0).or.
     1   ((YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc).and.NRefWdF.le.0)) then
        call FeClearGrWin
        NInfo=1
        TextInfo(1)='No reflections fulfil the selection '//
     1              'criteria.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      endif
      if(XUse.ne.XUseOld.or.YUse.ne.YUseOld) Zoomed=.false.
      if(YUse.eq.YUseIAve) then
        yomn=0.
        yomx=DMax
        if(XUse.eq.XUseIMin) then
          xomn=RIMinMin
          xomx=RIMaxMin
          Veta='I(min)'
        else if(XUse.eq.XUseIAve) then
          xomn=RIMinAve
          xomx=RIMaxAve
          Veta='I(ave)'
        else if(XUse.eq.XUseIMax) then
          xomn=RIMinMax
          xomx=RIMaxMax
          Veta='I(max)'
        endif
      else if(YUse.eq.YUseWdF) then
        yomn=-wdFMax
        yomx= wdFMax
        Veta='F(obs)'
        if(XUse.eq.XUseFobs) then
          xomn=0.
          xomx=FoMax
        endif
      else if(YUse.eq.YUseFcalc) then
        yomn=0
        yomx=max(FoMax,FcMax)
        Veta='F(obs)'
        xomn=0.
        xomx=yomx
      endif
      if(XUse.eq.XUseTheta) then
        xomn=ThetaMin
        xomx=ThetaMax
        Veta='Theta'
      endif
      if(yomx.le.0.) yomx=1.
      xomx=xomx*1.01
      yomn=yomn*1.01
      yomx=yomx*1.01
      if(Zoomed) then
        xomn=xomnz
        xomx=xomxz
        yomn=yomnz
        yomx=yomxz
      endif
      call FeClearGrWin
      call UnitMat(F2O,3)
      call UnitMat(O2F,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
      if(YUse.eq.YUseIAve) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Diff')
      else if(YUse.eq.YUseWdF) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'WdF')
      else if(YUse.eq.YUseFcalc) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'F(calc)')
      endif
      xp(3)=0.
      if(.not.Zoomed.and.YUse.eq.YUseFcalc.and.XUse.eq.XUseFobs) then
        xp(1)=xomn
        xp(2)=yomn
        call FeXf2X(xp,xo)
        xu(1)=xo(1)
        yu(1)=xo(2)
        xp(1)=xomx
        xp(2)=yomx
        call FeXf2X(xp,xo)
        xu(2)=xo(1)
        yu(2)=xo(2)
        call FePolyLine(2,xu,yu,Red)
      endif
      if(XUse.eq.XUseTheta) then
        if(DiamondUsed.gt.0) then
          do i=1,22
            if(DiamondTheta(i).le.0..or.DiamondTheta(i).gt.ThetaMax)
     1        exit
            xp(1)=max(DiamondTheta(i)-DiamondDeltaTheta,1.)
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)+1.
            xp(2)=yomx
            call FeXf2X(xp,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)-1.
            xp(1)=min(DiamondTheta(i)+DiamondDeltaTheta,ThetaMax-1.)
            call FeXf2X(xp,xo)
            xu(3)=xo(1)
            yu(3)=xo(2)-1.
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(4)=xo(1)
            yu(4)=xo(2)+1.
            call FeFillRectangle(xu(1),xu(4),yu(1),yu(2),4,0,0,
     1                           ColorDiamond)
            call FePolyLine(2,xu,yu,ColorDiamond)
          enddo
        endif
        if(GasketUsed.gt.0) then
          do i=1,22
            if(GasketTheta(i).le.0..or.GasketTheta(i).gt.ThetaMax) exit
            xp(1)=max(GasketTheta(i)-GasketDeltaTheta,1.)
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)+1.
            xp(2)=yomx
            call FeXf2X(xp,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)-1.
            xp(1)=min(GasketTheta(i)+GasketDeltaTheta,ThetaMax-1.)
            call FeXf2X(xp,xo)
            xu(3)=xo(1)
            yu(3)=xo(2)-1.
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(4)=xo(1)
            yu(4)=xo(2)+1.
            call FeFillRectangle(xu(1),xu(4),yu(1),yu(2),4,0,0,
     1                           ColorGasket)
            call FePolyLine(2,xu,yu,ColorGasket)
          enddo
        endif
      endif
      xp(3)=0.
      NCullAuto=0
      NCullManual=0
      do i=1,NAve
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).ne.0) then
            if(ICullAr(m).eq.1) then
              NCullManual=NCullManual+1
            else if(ICullAr(m).eq.2) then
              NCullAuto=NCullAuto+1
            endif
          endif
        enddo
      enddo
      do i=1,NPocet
        if(YUse.eq.YUseIAve) then
          ii=i
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          ii=FoInd2Ave(i)
        endif
        n=0
        nc=0
        if(ii.ne.0) then
          do j=1,NAveRedAr(ii)
            m=Ave2Org(j,ii)
            if(ICullAr(m).ne.0) then
              nc=nc+1
            else if(ICullAr(m).eq.0) then
              n=n+1
            endif
          enddo
        endif
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(YUse.eq.YUseIAve) then
              if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
            else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
              if(FoAr(i).le.2.*ObsLevelCull*FsAr(i)) cycle
            endif
          else if(ii.gt.0) then
            if(DisplaySelect.eq.DisplayCulled) then
              if(nc.gt.0) then
                go to 1250
              else
                cycle
              endif
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1250
            else
              cycle
            endif
          endif
        endif
1250    if(YUse.eq.YUseIAve) then
          k=HCondAveAr(i)
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          k=HCondWdFAr(i)
        endif
        call IndUnPack(k,ih,HCondLn,HCondMx,NDim(KPhase))
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        call FromIndSinthl(ih,xo,sinthl,sinthlq,1,0)
        pom=min(sinthl*LamAveRefBlock(KRefBlock),.999999)
        Theta=asin(pom)/ToRad
        Color=White
        if(XUse.eq.XUseTheta) then
          xp(1)=Theta
        else
          if(YUse.eq.YUseIAve) then
            RIMax=-99999999.
            RIMin= 99999999.
            do j=1,NAveRedAr(i)
              m=Ave2Org(j,i)
              if(ICullAr(m).eq.1) cycle
              RIMax=max(RIMax,RIAr(m))
              RIMin=min(RIMin,RIAr(m))
            enddo
            if(XUse.eq.XUseIMin) then
              xp(1)=RIMin
            else if(XUse.eq.XUseIAve) then
              xp(1)=RIAveAr(i)
            else if(XUse.eq.XUseIMax) then
              xp(1)=RIMax
            endif
          else if(YUse.eq.YUseWdF) then
            xp(1)=FoAr(i)
          else if(YUse.eq.YUseFcalc) then
            xp(1)=FoAr(i)
          endif
        endif
        if(YUse.eq.YUseIAve) then
          xp(2)=DiffAveAr(i)
        else if(YUse.eq.YUseWdF) then
          xp(2)=WdFAr(i)
        else
          xp(2)=FcAr(i)
        endif
        if(DiamondUsed.gt.0) then
          do j=1,22
            if(Theta.ge.DiamondTheta(j)-DiamondDeltaTheta.and.
     1         Theta.le.DiamondTheta(j)+DiamondDeltaTheta) then
              Color=Cyan
              exit
            endif
          enddo
        endif
        if(GasketUsed.gt.0) then
          do j=1,22
            if(Theta.ge.GasketTheta(j)-GasketDeltaTheta.and.
     1         Theta.le.GasketTheta(j)+GasketDeltaTheta) then
              Color=Magenta
              exit
             endif
          enddo
        endif
        if(xp(1).lt.xomn.or.xp(1).gt.xomx.or.
     1     xp(2).lt.yomn.or.xp(2).gt.yomx) cycle
        call FeXf2X(xp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,Color)
      enddo
1300  NAveObs=0
      do i=1,NAve
        if(RIAveAr(i).gt.EM9ObsLim(KDatBlock)*RSAveAr(i))
     1    NAveObs=NAveObs+1
      enddo
      if(rden(1).gt.0.) rave=rnum(1)/rden(1)*100.
      if(roden(1).gt.0.) then
        write(Cislo,'(f6.2,''/'',f6.2)') ronum(1)/roden(1)*100.,
     1                                   rave
      else
        if(rden(1).gt.0.) then
          write(Cislo,'(''-----/'',f6.2)') rave
        else
          write(Cislo,'(''-----/-----'')')
        endif
      endif
      call Zhusti(Cislo)
      write(Veta,'(''Rint(obs)/Rint(all) : '',a)') Cislo
      call FeQuestLblChange(nLblRint,Veta)
      write(t80,100) NAveObs,NAve-NTotallyCulled
      call Zhusti(t80)
      Veta='N(obs)/N(all) : '//t80(:idel(t80))
      call FeQuestLblChange(nLblPocet,Veta)
      write(Cislo,'(f10.3)') float(NRefRead)/float(NAve)
      call ZdrcniCisla(Cislo,1)
      Veta='Redundancy : '//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRedundancy,Veta)
      write(t80,100) NCullManual,NCullAuto
      call Zhusti(t80)
      Veta='Culled(man)/Culled(auto) : '//t80(:idel(t80))
      call FeQuestLblChange(nLblCulled,Veta)
      call SetRealArrayTo(RFac,5,0.)
      Veta=fln(:ifln)//'.m70'
      if(ExistFile(Veta)) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1400
1320    read(ln,FormA,end=1340) Veta
        m=23
        n=15
        do i=1,5
          j=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                      .false.,.true.)
          if(j.gt.0) then
            k=j+idel(CIFKey(m,n))
            call StToReal(Veta,k,RFac(i),1,.false.,ich)
            if(ich.ne.0) RFac(i)=99.99
            if(i.ne.5) RFac(i)=RFac(i)*100.
            go to 1320
          endif
          if(i.eq.1) then
            m=22
          else if(i.eq.2) then
            m=38
          else if(i.eq.3) then
            m=37
          else if(i.eq.4) then
            m=12
          endif
        enddo
        go to 1320
1340    call CloseIfOpened(ln)
      endif
1400  Veta='R(obs)/R(all) : '
      if(RFac(1).gt.0.) then
        write(Cislo,101) RFac(1)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta='R(obs)/R(all) : '//Cislo(:idel(Cislo))//'/'
      if(RFac(2).gt.0.) then
        write(Cislo,101) RFac(2)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRF,Veta)
      if(RFac(3).gt.0.) then
        write(Cislo,101) RFac(3)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta='Rw(obs)/Rw(all) : '//Cislo(:idel(Cislo))//'/'
      if(RFac(4).gt.0.) then
        write(Cislo,101) RFac(4)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRwF,Veta)
      write(Cislo,'(f15.3)') RFac(5)
      call Zhusti(Cislo)
      Veta='GOF : '//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblGOF,Veta)
      if(YUse.eq.YUseIAve) then
        Veta='versus |I(i)-I(ave)|/sig(I(ave))'
      else if(YUse.eq.YUseWdF) then
        Veta='versus w.|F(obs)-F(calc)|'
      else if(YUse.eq.YUseFcalc) then
        Veta='versus F(calc)'
      endif
      if(XUse.eq.XUseTheta) then
        Veta='Theta '//Veta(:idel(Veta))
      else
        if(YUse.eq.YUseIAve) then
          if(XUse.eq.XUseIMin) then
            Veta='I(min) '//Veta(:idel(Veta))
          else if(XUse.eq.XUseIAve) then
            Veta='I(ave) '//Veta(:idel(Veta))
          else if(XUse.eq.XUseIMax) then
            Veta='I(max) '//Veta(:idel(Veta))
          endif
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          Veta='F(obs) '//Veta(:idel(Veta))
        endif
      endif
      call FeHeaderInfo(Veta)
      AllowChangeMouse=.false.
      CheckMouse=.true.
1500  k=0
      CulledExist=.false.
      TwiceMeasuredExist=.false.
      OnceMeasuredExist=.false.
      CompletelyExcludedExist=.false.
      do i=1,NAve
        n=0
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).ne.0.and..not.CulledExist) then
            CulledExist=.true.
            k=k+1
          endif
          if(ICullAr(m).eq.0) n=n+1
        enddo
        if(n.eq.0) then
          if(.not.CompletelyExcludedExist) then
            CompletelyExcludedExist=.true.
            k=k+1
          endif
        else if(n.eq.1) then
          if(.not.OnceMeasuredExist) then
            OnceMeasuredExist=.true.
            k=k+1
          endif
        else if(n.eq.2) then
          if(.not.TwiceMeasuredExist) then
            TwiceMeasuredExist=.true.
            k=k+1
          endif
        endif
        if(k.ge.4) exit
      enddo
      if(YUse.eq.YUseIAve) then
        call FeQuestButtonLabelChange(nButtIMin,Labels(2))
        call FeQuestButtonOff(nButtIAve)
        call FeQuestButtonOff(nButtIMax)
        call FeQuestButtonOff(nButtTheta)
      else
        call FeQuestButtonDisable(nButtIAve)
        call FeQuestButtonDisable(nButtIMax)
        if(YUse.eq.YUseFcalc) then
          call FeQuestButtonDisable(nButtTheta)
        else
          call FeQuestButtonOff(nButtTheta)
        endif
        call FeQuestButtonLabelChange(nButtIMin,'X=F(obs)')
      endif
      if(Zoomed) then
        call FeQuestButtonOff(nButtUnzoom)
      else
        call FeQuestButtonDisable(nButtUnzoom)
      endif
      XUseOld=XUse
      YUseOld=YUse
      call FeQuestEvent(CullQuest,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          call FeQuestRemove(CullQuest)
          go to 5000
        else if(CheckNumber.eq.nButtIMin) then
          XUse=XUseIMin
          go to 1200
        else if(CheckNumber.eq.nButtIAve) then
          XUse=XUseIAve
          go to 1200
        else if(CheckNumber.eq.nButtIMax) then
          XUse=XUseIMax
          go to 1200
        else if(CheckNumber.eq.nButtTheta) then
          XUse=XUseTheta
          go to 1200
        else if(CheckNumber.eq.nButtUnzoom) then
          Zoomed=.false.
          go to 1200
        else if(CheckNumber.eq.nButtAverage) then
          go to 1100
        else if(CheckNumber.eq.nButtAverageReport) then
          call FeListView(fln(:ifln)//'.ave',0)
          go to 1500
        else if(CheckNumber.eq.nButtEditAtoms) then
          call CopyFile(fln(:ifln)//'.m40',SavedM40)
          call EM40Atoms(ich)
          call iom40(1,0,fln(:ifln)//'.m40')
          Diff=FileDiff(fln(:ifln)//'.m40',SavedM40)
          if(ich.ne.0) then
            if(Diff) then
              if(FeYesNo(-1.,-1.,'Do you want to discard made '//
     1                   'changes?',0))
     2          call CopyFile(SavedM40,fln(:ifln)//'.m40')
            endif
          else
            if(Diff) then
              if(.not.FeYesNo(-1.,-1.,'Do you want to rewrite changed'//
     1                        ' files?',1))
     2          call CopyFile(SavedM40,fln(:ifln)//'.m40')
            endif
          endif
          call iom40(0,0,fln(:ifln)//'.m40')
          go to 1500
        else if(CheckNumber.eq.nButtEditM40) then
          call FeEdit(fln(:ifln)//'.m40')
          call iom40(0,0,fln(:ifln)//'.m40')
          call iom40(1,0,fln(:ifln)//'.m40')
          go to 1500
        else if(CheckNumber.eq.nButtRefine.or.
     1          CheckNumber.eq.nButtFourier.or.
     2          CheckNumber.eq.nButtContour) then
          Uloha='From manual culling'
          call NewPg(1)
          if(CheckNumber.eq.nButtRefine) then
            call EM9ManualCullUpdate(0)
            call NewPg(1)
            call Refine(0,RefineEnd)
          else if(CheckNumber.eq.nButtFourier) then
            call Fourier
          else if(CheckNumber.eq.nButtContour) then
            SvFile='jmcl'
            if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
            call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,SvFile)
            call Contour
            call FeMakeGrWin(0.,120.,2.*YBottomMargin,24.)
            call FeMakeAcWin(60.,20.,30.,30.)
            call FeBottomInfo('#prazdno#')
            call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,SvFile,0)
          endif
          call UpdateSummary
          if(CheckNumber.eq.nButtRefine) call DRSetRefArrays(1)
          if(YUse.eq.YUseIAve) then
            go to 1300
          else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
            go to 1100
          endif
        else if(CheckNumber.eq.nButtRefineReport) then
          call FeListView(fln(:ifln)//'.ref',0)
          go to 1500
        else if(CheckNumber.eq.nButtRefineCommands.or.
     1          CheckNumber.eq.nButtFourierCommands) then
          if(CheckNumber.eq.nButtRefineCommands) then
            call RefSetCommands
          else
            call FouSetCommands(ichp)
          endif
          if(StartProgram) then
            Uloha='From manual culling'
            if(CheckNumber.eq.nButtRefine) call EM9ManualCullUpdate(0)
            call NewPg(1)
            if(CheckNumber.eq.nButtRefineCommands) then
              call Refine(0,RefineEnd)
            else
              call Fourier
            endif
            call UpdateSummary
            if(CheckNumber.eq.nButtRefine) call DRSetRefArrays(1)
          endif
          if(YUse.eq.YUseIAve) then
            go to 1300
          else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
            go to 1100
          endif
        else if(CheckNumber.eq.nButtResetCulled) then
          do i=1,NRefRead
            if(ICullAr(i).ne.2) ICullAr(i)=0
          enddo
          go to 1100
        else if(CheckNumber.eq.nButtFilter) then
          call EM9ManualFilter(1)
          go to 1100
        else if(CheckNumber.eq.nButtOptions) then
          call EM9ManualCullOptions(1)
          go to 1100
        endif
      else if(CheckType.eq.EventMouse) then
        if(CheckNumber.eq.JeLeftDown) then
          xsel(1)=XPos
          ysel(1)=YPos
          do i=1,5
            xsel(i)=XPos
            ysel(i)=YPos
          enddo
          xo(1)=FeX2Xo(XPos)
          xo(2)=FeY2Yo(YPOs)
          xo(3)=0.
          call multm(O2F,xo,xp,3,3,1)
          XMinP=xp(1)
          YMinP=xp(2)
          call FePlotMode('E')
          ip=2
          call FePolyLine(2,xsel,ysel,Green)
          TakeMouseMove=.true.
          if(DeferredOutput) call FeReleaseOutput
2200      call FeEvent(1)
          if(EventType.eq.0) go to 2200
2250      if(EventType.eq.EventMouse) then
            if(EventNumber.eq.JeMove) then
              call FePolyLine(ip,xsel,ysel,Green)
              ysel(2)=YPos
              xsel(3)=XPos
              ysel(3)=YPos
              xsel(4)=XPos
              if(xsel(2).ne.xsel(3)) then
                ip=5
              else
                ip=2
              endif
              call FePolyLine(ip,xsel,ysel,Green)
              go to 2200
            else if(EventNumber.eq.JeLeftUp) then
              TakeMouseMove=.false.
              xo(1)=FeX2Xo(xsel(3))
              xo(2)=FeY2Yo(ysel(3))
              xo(3)=0.
              call multm(O2F,xo,xp,3,3,1)
              XMin=min(XMinP,xp(1))
              YMin=min(YMinP,xp(2))
              XMax=max(XMinP,xp(1))
              YMax=max(YMinP,xp(2))
              DMin=999999.
              do mm=1,2
                if(mm.eq.1) then
                  Kolik=0
                else
                  Mame=0
                endif
                do i=1,NPocet
                  if(YUse.eq.YUseIAve) then
                    ii=i
                  else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                    ii=FoInd2Ave(i)
                  endif
                  if(DisplaySelect.ne.DisplayAll) then
                    if(DisplaySelect.eq.DisplayObserved) then
                      if(YUse.eq.YUseIAve) then
                        if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
                      else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                        if(FoAr(i).le.2.*ObsLevelCull*FsAr(i)) cycle
                      endif
                    else
                      n=0
                      do j=1,NAveRedAr(ii)
                        m=Ave2Org(j,ii)
                        if(ICullAr(m).ne.0.and.
     1                     DisplaySelect.eq.DisplayCulled) then
                          go to 2500
                        else if(ICullAr(m).eq.0) then
                          n=n+1
                        endif
                      enddo
                      if(DisplaySelect.eq.DisplayCulled) then
                        cycle
                      else if((DisplaySelect.eq.DisplayMeasuredTwice
     1                         .and.n.eq.2).or.
     2                        (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                          n.eq.1)) then
                        go to 2500
                      else
                        cycle
                      endif
                    endif
                  endif
2500              if(YUse.eq.YUseWdF) then
                    xp(2)=WdFAr(i)
                  else if(YUse.eq.YUseFcalc) then
                    xp(2)=FcAr(i)
                  else
                    xp(2)=DiffAveAr(i)
                  endif
                  if(XUse.eq.XUseTheta) then
                    if(YUse.eq.YUseIAve) then
                      k=HCondAveAr(i)
                    else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                      k=HCondWdFAr(i)
                    endif
                    call IndUnPack(k,ih,HCondLn,HCondMx,NDim(KPhase))
                    call FromIndSinthl(ih,xo,xp(1),sinthlq,1,0)
                    pom=min(xp(1)*LamAveRefBlock(KRefBlock),.999999)
                    xp(1)=asin(pom)/ToRad
                  else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                    xp(1)=FoAr(i)
                  else
                    RIMax=-999999.
                    RIMin= 999999.
                    do j=1,NAveRedAr(i)
                      m=Ave2Org(j,i)
                      if(ICullAr(m).eq.1) cycle
                      RIMax=max(RIMax,RIAr(m))
                      RIMin=min(RIMin,RIAr(m))
                    enddo
                    if(XUse.eq.XUseIMin) then
                      xp(1)=RIMin
                    else if(XUse.eq.XUseIAve) then
                      xp(1)=RIAveAr(i)
                    else if(XUse.eq.XUseIMax) then
                      xp(1)=RIMax
                    endif
                  endif
                  if(xp(1).lt.xomn.or.xp(1).gt.xomx.or.
     1               xp(2).lt.yomn.or.xp(2).gt.yomx) cycle
                  call FeXf2X(xp,xo)
                  if(mm.eq.1) then
                    xu(1)=(XMin+XMax)*.5
                    xu(2)=(YMin+YMax)*.5
                    xu(3)=0.
                    call FeXf2X(xu,yu)
                    pom=sqrt((xo(1)-yu(1))**2+(xo(2)-yu(2))**2)
                    if(pom.lt.DMin) then
                      DMin=min(DMin,pom)
                      call CopyVek(xo,xoc,3)
                      ic=i
                    endif
                  endif
                  if(xp(1).lt.XMin.or.xp(1).ge.XMax.or.
     1               xp(2).lt.YMin.or.xp(2).ge.YMax) then
                    cycle
                  endif
                  if(mm.eq.1) then
                    Kolik=Kolik+1
                    cycle
                  else
                    Mame=Mame+1
                  endif
                  if(Mame.eq.1) then
3200                call FeEvent(1)
                    if(EventType.eq.0) go to 3200
                    if(EventType.eq.EventMouse) then
                      if(EventNumber.eq.JeLeftUp) then
                        CoDal=1
                        call FePolyLine(ip,xsel,ysel,Green)
                        call FePlotMode('N')
                      else if(EventNumber.eq.JeRightDown) then
                        call FePlotMode('N')
                        CoDal=max(FeMenu(-1.,-1.,Menu,1,6,1,0)-1,0)
                        call FePlotMode('E')
                        call FePolyLine(ip,xsel,ysel,Green)
                        call FePlotMode('N')
                        if(CoDal.le.0) go to 1500
                      else
                        go to 3200
                      endif
                      if(CoDal.eq.1) then
                        Zoomed=.true.
                        xomnz=XMin
                        xomxz=XMax
                        yomnz=YMin
                        yomxz=YMax
                        go to 1100
                      endif
                    else
                      go to 3200
                    endif
                  endif
                  if(YUse.eq.YUseIAve) then
                    IRefAve=i
                  else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                    IRefAve=FoInd2Ave(i)
                  endif
                  if(CoDal.eq.2) then
                    if(IRefAve.gt.0) then
                      call EM9ManualCullQuest(xo,IRefAve,
     1                  YUse.ne.YUseIAve,Mame.eq.Kolik,Konec)
                    else
                      call FeChybne(-1.,-1.,'Nenasel ji.',' ',
     1                              SeriousError)
                      Konec=0
                    endif
                    if(Konec.eq.1) go to 1100
                  else
                    MinInt=0
                    MaxInt=0
                    RIMax=-99999999.
                    RIMin= 99999999.
                    do j=1,NAveRedAr(IRefAve)
                      m=Ave2Org(j,IRefAve)
                      if(ICullAr(m).eq.1) cycle
                      if(CoDal.eq.5) then
                        ICullAr(m)=1
                      else
                        if(RIAr(m).ge.RIMax) then
                          RIMax=RIAr(m)
                          MaxInt=m
                        endif
                        if(RIAr(m).le.RIMin) then
                          RIMin=RIAr(m)
                          MinInt=m
                        endif
                      endif
                    enddo
                    if(MinInt.eq.0.and.MaxInt.eq.0) cycle
                    if(CoDal.eq.3) then
                      ICullAr(MinInt)=1
                    else if(CoDal.eq.4) then
                      ICullAr(MaxInt)=1
                    endif
                  endif
                enddo
                if(mm.eq.2.and.Mame.ne.0) then
                  call FePolyLine(ip,xsel,ysel,Green)
                  call FePlotMode('N')
                  go to 1100
                endif
              enddo
              if(Kolik.eq.0.and.Mame.eq.0.and.DMin.lt.5.) then
                call FePolyLine(ip,xsel,ysel,Green)
                call FePlotMode('N')
                if(YUse.eq.YUseIAve) then
                  IRefAve=ic
                else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                  IRefAve=FoInd2Ave(ic)
                endif
                if(IRefAve.gt.0) then
                  call EM9ManualCullQuest(xoc,IRefAve,YUse.ne.YUseIAve,
     1                                    .true.,Konec)
                else
                  call FeChybne(-1.,-1.,'Nenasel ji.',' ',
     1                          SeriousError)
                endif
              else
                call FePolyLine(ip,xsel,ysel,Green)
                call FePlotMode('N')
                go to 1500
              endif
              go to 1100
            else
              go to 2200
            endif
          else
            go to 2200
          endif
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 1500
5000  if(.not.EqIV(ICullAr,ICullArIn,NRefRead).or.
     1   FLimCull(KDatBlock).ne.FLimCullIn) then
        if(FeYesNo(-1.,-1.,'Do you want to accept the changes?',1))
     1    call EM9ManualCullUpdate(0)
      endif
9999  if(allocated(HCondAveAr)) then
        deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     1             Ave2Org)
        NAveMax=0
        NAveRedMax=0
        NAve=0
        NAveRed=0
      endif
      call DeleteFile(fln(:ifln)//'.ave')
      if(allocated(ICullArIn)) deallocate(ICullArIn,ICullArOld)
      if(allocated(HCondAr)) deallocate(HCondAr,riar,rsar)
      if(allocated(ICullAr)) deallocate(ICullAr,RefBlockAr,RunCCDAr)
      if(allocated(FoAr))
     1  deallocate(FoAr,FcAr,FsAr,WdFAr,HCondWdFAr,FoInd2Ave)
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      call FeTmpFilesClear(SavedM40)
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      return
100   format(i15,'/',i15)
101   format(f15.2)
      end
      subroutine EM9ManualCullQuest(xo,IRefAve,InF,LastOne,Konec)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xo(3),xsel(5),ysel(5)
      integer UseTabsIn,UseTabsInt,UseTabsDef,SbwLnQuest,ISel(:),
     1        SbwItemSelQuest,SbwItemPointerQuest,IDiff(:),IPorP(:),
     2        ICull(:),ICullOld(:),Konec
      character*80 Veta,t80
      character*30 FormRefAve,FormRefAveDef
      character*8  Flags
      character*2  Znak
      logical InF,LastOne,EqIV
      allocatable ISel,IDiff,IPorP,ICull,ICullOld
      UseTabsIn=UseTabs
      n=NAveRedAr(IRefAve)
      allocate(ISel(n),IPorP(n),IDiff(n),ICull(n),ICullOld(n))
      call SetIntArrayTo(ISel,NAveRedAr(IRefAve),0)
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        if(ICullAr(m).eq.1) then
          ICull(i)=1
        else
          ICull(i)=0
        endif
        ICullOld(i)=ICull(i)
      enddo
      FormRefAve='(a,3(a1,i4),2(a1,f10.1),a1,a)'
      FormRefAveDef='(a,3(a1,i4),2(a1,a))'
      write(FormRefAve   (4:4),100) NDim(KPhase)
      write(FormRefAveDef(4:4),100) NDim(KPhase)
      id=NextQuestId()
      ild=min(NAveRed+4,12)
      Veta='Define/Modify culling flags'
      xq=-1.
      yq=-1.
      xqd=420.+float(NDimI(KPhase))*20.
      call FeDeferOutput
      n=0
1100  call FeQuestCreate(id,xq,yq,xqd,ild,Veta,0,LightGray,-1,-1)
      XQMin=QuestXMin(id)
      XQMax=QuestXMax(id)
      YQMin=QuestYMin(id)
      YQMax=QuestYMax(id)
      if(xo(1).gt.XQMin-9.5.and.xo(1).lt.XQMax+9.5.and.
     1   xo(2).gt.YQMin-9.5.and.xo(2).lt.YQMax+9.5.and.n.lt.5) then
        if(xo(1)-(XQMin+XQMax)*.5.ge.0.) then
          xq=xo(1)+XQMin-XQMax-10.
        else
          xq=xo(1)+10.
        endif
        if(xo(2)-(YQMin+YQMax)*.5.ge.0.) then
          yq=xo(2)+YQMin-YQMax-10.
        else
          yq=xo(2)+10.
        endif
        xq=anint(xq)
        yq=anint(yq)
        call FeQuestRemove(id)
        n=n+1
        go to 1100
      endif
      xsel(1)=xo(1)-6.
      ysel(1)=xo(2)-6.
      xsel(2)=xsel(1)+12.
      ysel(2)=ysel(1)
      xsel(3)=xsel(1)+12.
      ysel(3)=ysel(1)+12.
      xsel(4)=xsel(1)
      ysel(4)=ysel(1)+12.
      xsel(5)=xsel(1)
      ysel(5)=ysel(1)
      call FePlotMode('E')
      call FePolyLine(5,xsel,ysel,Red)
      call FePlotMode('N')
      call IndUnPack(HCondAveAr(IRefAve),ih,HCondLn,HCondMx,
     1               NDim(KPhase))
      il=1
      UseTabsInt=NextTabs()
      xpom=120.
      do i=1,NDim(KPhase)
        call FeTabsAdd(xpom,UseTabsInt,IdLeftTab,' ')
        xpom=xpom+20.
      enddo
      xpom=xpom+30.
      do i=1,2
        call FeTabsAdd(xpom,UseTabsInt,IdCharTab,'.')
        xpom=xpom+50.
      enddo
      xpom=xpom-15.
      call FeTabsAdd(xpom,UseTabsInt,IdRightTab,' ')
      UseTabsDef=NextTabs()
      xpom=120.
      do i=1,NDim(KPhase)
        call FeTabsAdd(xpom,UseTabsDef,IdLeftTab,' ')
        xpom=xpom+20.
      enddo
      xpom=xpom+40.
      do i=1,2
        call FeTabsAdd(xpom,UseTabsDef,IdLeftTab,' ')
        xpom=xpom+50.
      enddo
      tpom=10.
      UseTabs=UseTabsInt
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblAveraged1=LblLastMade
      UseTabs=UseTabsDef
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblAveraged2=LblLastMade
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,ild-3,dpom,ild-4,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwSel=SbwLastMade
      il=ild-2
      dpom=80.
      spom=15.
      xpom=.5*xqd-dpom-spom*.5
      Veta='%Select all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+spom
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=.5*xqd-dpom-spom*.5
      Veta='%Uncull'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtUncull=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+spom
      Veta='%Cull'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCull=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=xqd*.5-2.*dpom-spom*1.5
      Veta='%Avoid'
      do i=1,4
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtAvoid=ButtonLastMade
          Veta='%Next'
        else if(i.eq.2) then
          nButtNext=ButtonLastMade
          Veta='%End'
        else if(i.eq.3) then
          nButtEnd=ButtonLastMade
          Veta='A%pply'
        else
          nButtApply=ButtonLastMade
        endif
        xpom=xpom+dpom+spom
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      if(LastOne) then
        call FeQuestButtonDisable(nButtNext)
        call FeQuestMouseToButton(nButtEnd)
      else
        call FeQuestMouseToButton(nButtNext)
      endif
      nLblLast=nLblAveraged2
1250  call CloseIfOpened(SbwLnQuest(nSbwSel))
      do i=1,NAveRedAr(IRefAve)
        if(ICull(i).eq.2) ICull(i)=0
      enddo
1350  s1=0.
      s2=0.
      s4=0.
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        if(ICull(i).gt.0) cycle
        s1=s1+RIAr(m)
        s2=s2+1.
        s4=s4+RSAr(m)**2
      enddo
      if(s2.gt.1.5) then
        s1=s1/s2
        s4=sqrt(s4)/s2
        s3=0.
        do i=1,NAveRedAr(IRefAve)
          m=Ave2Org(i,IRefAve)
          if(ICull(i).gt.0) cycle
          Diff=abs(RIAr(m)-s1)
          s3=s3+Diff**2
        enddo
        pom=sqrt(s3/(s2*(s2-1.)))
        if(SigIMethod(KDatBlock).eq.1) then
          s3=s4
        else if(SigIMethod(KDatBlock).eq.2) then
          s4=s4*.5
          if(pom.lt.s4) then
            s3=s4
          else
            s3=pom
          endif
        else
          s3=max(pom,s4)
        endif
      else
        s3=sqrt(s4)
      endif
      if(s2.gt..5) then
        RIAveAr(IRefAve)=s1
        RSAveAr(IRefAve)=s3
        DiffMax=0.
        do i=1,NAveRedAr(IRefAve)
          if(icull(i).ne.0) cycle
          m=Ave2Org(i,IRefAve)
c          Diff=abs(RIAr(m)-s1)/RSAr(m)
          Diff=abs(RIAr(m)-s1)/s3
          if(Diff.gt.DiffMax) then
            DiffMax=Diff
            kmax=i
          endif
        enddo
        if(FLimCull(KDatBlock).gt.0..and.
     1     DiffMax.gt.FLimCull(KDatBlock).and.s2.gt.2.5) then
          icull(kmax)=2
          m=Ave2Org(kmax,IRefAve)
          ICullAr(m)=2
          go to 1350
        endif
        if(InF) then
          if(RIAveAr(IRefAve).gt.0.) then
            PomI=sqrt(RIAveAr(IRefAve))
            PomS=min(100.,2.*RSAveAr(IRefAve)/PomI)
          else
            PomI=0.
            PomS=1.
          endif
        else
          PomI=RIAveAr(IRefAve)
          PomS=RSAveAr(IRefAve)
        endif
        UseTabs=UseTabsInt
        write(Veta,FormRefAve) 'Averaged:',
     1                        (Tabulator,ih(j),j=1,NDim(KPhase)),
     2                         Tabulator,PomI,
     3                         Tabulator,PomS
        if(nLblLast.ne.nLblAveraged1) then
          call FeQuestLblOff(nLblLast)
          nLblLast=nLblAveraged1
          call FeQuestLblOn(nLblLast)
        endif
        call FeQuestLblChange(nLblLast,Veta)
      else
        UseTabs=UseTabsDef
        write(Veta,FormRefAveDef) 'Averaged:',
     1                        (Tabulator,ih(j),j=1,NDim(KPhase)),
     2                        (Tabulator,'---------',i=1,2)
        if(nLblLast.ne.nLblAveraged2) then
          call FeQuestLblOff(nLblLast)
          nLblLast=nLblAveraged2
          call FeQuestLblOn(nLblLast)
        endif
        call FeQuestLblChange(nLblLast,Veta)
      endif
      do i=1,NAveRedAr(IRefAve)
        call FeQuestSetSbwItemSel(i,nSbwSel,ISel(i))
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_ref.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        IDiff(i)=nint(1000.*
     1                (RIAr(m)-RIAveAr(IRefAve))/RSAveAr(IRefAve))
      enddo
      call indexx(NAveRedAr(IRefAve),IDiff,IPorP)
      UseTabs=UseTabsInt
      do i=1,NAveRedAr(IRefAve)
        j=IPorP(i)
        m=Ave2Org(j,IRefAve)
        call IndUnPack(HCondAr(1,m),ih,HCondLn,HCondMx,NDim(KPhase))
        pom=abs(float(IDiff(j)))*.001
        if(IDiff(j).gt.0) then
          Znak='+'
        else
          Znak='--'
        endif
        if(ICull(j).eq.1) then
          Flags='Cull-m'
        else if(ICull(j).eq.2) then
          Flags='Cull-a'
        else
          Flags=' '
          if(pom.gt. 3.) Flags=Znak
          if(pom.gt. 5.) Flags=Flags(:idel(Flags))//Znak
          if(pom.gt.10.) Flags=Flags(:idel(Flags))//Znak
          if(pom.gt.20.) Flags=Flags(:idel(Flags))//Znak
        endif
        if(NRefBlock.gt.1) then
          write(Cislo,'(''%'',i10)') mod(RefBlockAr(m),1000)
        else
          Cislo=' '
        endif
        k=RefBlockAr(m)/1000
        write(t80,'(''#'',i10)') k
        t80=Cislo(:idel(Cislo))//t80(:idel(t80))
        if(RunCCDMax(KRefBlock).gt.1) then
          l=0
          do nr=1,NRuns(KRefBlock)
            if(l+RunNFrames(nr,KRefBlock).lt.RunCCDAr(m)) then
              l=l+RunNFrames(nr,KRefBlock)
            else
              exit
            endif
          enddo
          l=RunCCDAr(m)-l
          write(Cislo,'(''-'',i6,''/'',i6)') nr,l
          t80=t80(:idel(t80))//Cislo(:idel(Cislo))
        endif
        call Zhusti(t80)
        if(InF) then
          if(RIAr(m).gt.0.) then
            PomI=sqrt(RIAr(m))
            PomS=min(100.,2.*RSAr(m)/PomI)
          else
            PomI=0.
            PomS=1.
          endif
        else
          PomI=RIAr(m)
          PomS=RSAr(m)
        endif
        write(ln,FormRefAve) t80,
     1                      (Tabulator,ih(k),k=1,NDim(KPhase)),
     1                       Tabulator,PomI,
     2                       Tabulator,PomS,
     3                       Tabulator,Flags
      enddo
      call CloseIfOpened(ln)
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_ref.tmp')
1500  if(EqIV(ICull,ICullOld,NAveRedAr(IRefAve))) then
        call FeQuestButtonOff(nButtEnd)
        if(LastOne) then
          call FeQuestMouseToButton(nButtEnd)
        else
          call FeQuestButtonOff(nButtNext)
          call FeQuestMouseToButton(nButtNext)
        endif
        call FeQuestButtonDisable(nButtApply)
        call FeQuestButtonDisable(nButtAvoid)
      else
        call FeQuestButtonDisable(nButtEnd)
        if(.not.LastOne) call FeQuestButtonDisable(nButtNext)
        call FeQuestButtonOff(nButtApply)
        call FeQuestButtonOff(nButtAvoid)
        call FeQuestMouseToButton(nButtApply)
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAvoid) then
        do i=1,NAveRedAr(IRefAve)
          ICullAr(Ave2Org(i,IRefAve))=ICullOld(i)
        enddo
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtApply)
     1  then
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNext)
     1  then
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEnd)
     1  then
        Konec=1
      else if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh)) then
        if(CheckNumber.eq.nButtAll) then
          j=1
        else
          j=0
        endif
        call SetIntArrayTo(ISel,NAveRedAr(IRefAve),j)
        go to 1250
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtCull.or.CheckNumber.eq.nButtUnCull))
     2  then
        if(CheckNumber.eq.nButtCull) then
          j=1
        else
          j=0
        endif
        n=0
        do i=1,NAveRedAr(IRefAve)
          if(SbwItemSelQuest(i,nSbwSel).eq.1) then
            n=n+1
            k=IPorP(i)
            m=Ave2Org(k,IRefAve)
            ICullAr(m)=j
            ICull(k)=j
          endif
        enddo
        if(n.le.0) then
          k=IPorP(SbwItemPointerQuest(nSbwSel))
          m=Ave2Org(k,IRefAve)
          ICullAr(m)=j
          ICull(k)=j
        endif
        go to 1250
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
9999  call FeTabsReset(UseTabsInt)
      call FeTabsReset(UseTabsDef)
      UseTabs=UseTabsIn
      call FePlotMode('E')
      call FePolyLine(5,xsel,ysel,Red)
      call FePlotMode('N')
      if(allocated(ISel)) deallocate(ISel,IPorP,IDiff,ICull,ICullOld)
      return
100   format(i1)
      end
      subroutine EM9ManualCullUpdate(Klic)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      logical WizardModeIn
      dimension RefDatCorrespondOld(:)
      allocatable RefDatCorrespondOld
      WizardModeIn=WizardMode
      SilentRun=.true.
      allocate(RefDatCorrespondOld(NRefBlock))
      do i=1,NRefBlock
        RefDatCorrespondOld(i)=RefDatCorrespond(i)
      enddo
      call iom95(0,fln(:ifln)//'.m95')
      do i=1,NRefBlock
        RefDatCorrespond(i)=RefDatCorrespondOld(i)
      enddo
      deallocate(RefDatCorrespondOld)
      n=0
      do IRefBlock=1,MaxRefBlockAr
        call OpenRefBlockM95(95,IRefBlock,fln(:ifln)//'.m95')
        write(Cislo,'(''.l'',i2)') IRefBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        call OpenFile(97,RefBlockFileName,'formatted','unknown')
        if(ErrFlag.ne.0) then
          call CloseIfOpened(95)
          call CloseIfOpened(97)
          go to 9999
        endif
1100    call DRGetReflectionFromM95(95,iend,ich)
        if(iend.ne.0.or.ich.ne.0) go to 1200
        if(no.gt.0.and.iflg(1).ge.0.and.iflg(2).eq.1) then
          n=n+1
          if(n.gt.ubound(RefBlockAr,1)) then
            n=n-1
            go to 1150
          endif
          if(mod(RefBlockAr(n),1000).eq.IRefBlock.and.
     1           RefBlockAr(n)/1000.eq.no) then
            iflg(3)=ICullAr(n)
          else
            n=n-1
          endif
        endif
1150    call DRPutReflectionToM95(97,nl)
        go to 1100
1200    call CloseIfOpened(95)
        call CloseIfOpened(97)
      enddo
      call CompleteM95(1)
      if(Klic.eq.0) then
        if(allocated(riar))
     1    deallocate(riar,rsar,ICullAr,RefBlockAr,HCondAr,RunCCDAr)
        call CompleteM90
        call EM9CreateM90(1,ich)
      endif
9999  SilentRun=.false.
      WizardMode=WizardModeIn
      return
      end
      subroutine EM9ManualCullOptions(Key)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*80 Veta
      character*2 :: Gasket(3) = (/'Fe','W ','Re'/)
      integer RolMenuSelectedQuest,EdwStateQuest,XUseOld,YUseOld
      logical CrwLogicQuest,UseAutomaticCulling,ExistFile
      real GasketD(22,3),DiamondD(22)
      data (GasketD(i,1),i=1,8) /2.027,1.433,1.170,1.013,0.906,0.827,
     1                           0.766,-1.000/
      data (GasketD(i,2),i=1,9) /2.238,1.582,1.292,1.119,1.001,0.914,
     1                           0.846,0.791,-1.000/
      data (GasketD(i,3),i=1,22)/2.392,2.228,2.108,1.630,1.381,1.262,
     1                           1.196,1.174,1.155,1.114,1.054,1.010,
     2                           0.932,0.904,0.886,0.867,0.838,0.835,
     3                           0.815,0.797,0.785,-1.000/
      data DiamondD/2.0595,1.2612,1.0755,1.0297,0.8918,0.8184,0.7281,
     1              0.6865,0.6306,0.6030,0.5945,0.5640,0.5440,0.5370,
     2              0.5149,0.4995,0.4767,0.4644,0.4360,0.4209,0.4119,
     3              0.4092/
      if(Key.eq.0) then
        GasketUsed=0
        GasketDeltaTheta=.2
        DiamondUsed=0
        DiamondDeltaTheta=.3
        DisplaySelect=DisplayAll
        XUse=XUseIAve
        YUse=YUseIAve
        ObsLevelCull=3.
        XUseOld=-1
        YUseOld=-1
      else
        XUseOld=XUse
        YUseOld=YUse
        id=NextQuestId()
        xqd=500.
        il=16
        Veta='Options'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=1
        tpomb=5.
        Veta='Draw:'
        call FeQuestLblMake(id,tpomb,il,Veta,'L','B')
        xpom=tpomb+FeTxLength(Veta)+25.
        Veta='|I(i)-I(ave)|/sig(I(ave)) from averaging'
        tpom=xpom+CrwgXd+10.
        do i=1,3
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                        1)
          call FeQuestCrwOpen(CrwLastMade,i.eq.YUse)
          if(i.eq.1) then
            nCrwDrawIAve=CrwLastMade
            Veta='w.|F(obs)-F(calc)| from %refinement'
          else if(i.eq.2) then
            nCrwDrawWdF=CrwLastMade
            Veta='F(obs) v.s. F(%calc) from refinement'
          else if(i.eq.3) then
            nCrwDrawFcalc=CrwLastMade
          endif
          if(i.gt.1.and..not.ExistFile(fln(:ifln)//'.m83'))
     1      call FeQuestCrwDisable(CrwLastMade)
          il=il+1
        enddo
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Display:'
        call FeQuestLblMake(id,tpomb,il,Veta,'L','B')
        Veta='%all reflections'
        do i=1,5
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        2)
          call FeQuestCrwOpen(CrwLastMade,i-1.eq.DisplaySelect)
          if(i.eq.1) then
            nCrwDisplayAll=CrwLastMade
            Veta='Display only o%bserved reflections'
          else if(i.eq.2) then
            nCrwDisplayObserved=CrwLastMade
            tpome=xpom+FeTxLength(Veta)+15.
            Veta='=> observabitity level I>'
            xpome=tpome+FeTxLength(Veta)+10.
            dpom=50.
            call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,
     1                          EdwYd,0)
            nEdwSigLevel=EdwLastMade
            tpome=xpome+dpom+5.
            Veta='*sig(I)'
            call FeQuestLblMake(id,tpome,il,Veta,'L','N')
            nLblSigLevel=LblLastMade
            call FeQuestLblOff(LblLastMade)
            Veta='Displa%y only reflections with culling'
          else if(i.eq.3) then
            if(.not.CulledExist) call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayCulled=CrwLastMade
            Veta='Display reflections averaged only from t%wo'
          else if(i.eq.4) then
            if(.not.TwiceMeasuredExist)
     1        call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayMeasuredTwice=CrwLastMade
            Veta='Display reflections measured only o%nce'
          else
            if(.not.OnceMeasuredExist)
     1        call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayMeasuredOnce=CrwLastMade
          endif
          il=il+1
        enddo
        call FeQuestLinkaMake(id,il)
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+15.
        Veta='Apply automatic c%ulling'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwCull=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,FLimCull(KDatBlock).gt.0.)
        il=il+1
        xpom=25.
        Veta='Reflections |I-I(ave)|>'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        nLblCull=LblLastMade
        call FeQuestLblOff(LblLastMade)
        xpom=FeTxLength(Veta)+xpom+5.
        dpom=40.
        tpom=xpom+dpom+5.
        Veta='*sig(I(ave)) will be culle%d'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCull=EdwLastMade
        if(FLimCull(KDatBlock).lt.0.) then
          UseAutomaticCulling=.false.
          FLimCull(KDatBlock)=20.
        else
          UseAutomaticCulling=.true.
        endif
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+15.
        Veta='%High-pressure data collection'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwHighPressure=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,DiamondUsed.gt.0)
        il=il+1
        tpom=25.
        Veta='Gasket %material:'
        xpom=tpom+150.
        dpom=60.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolMenuGasket=RolMenuLastMade
        il=il+1
        Veta='Diam%ond theta width in degs:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDiamondDelta=EdwLastMade
        il=il+1
        Veta='%Gasket theta width in degs:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGasketDelta=EdwLastMade
1100    if(UseAutomaticCulling) then
          call FeQuestLblOn(nLblCull)
          call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                            .false.,.false.)
        else
          if(EdwStateQuest(nEdwCull).eq.EdwOpened) then
            call FeQuestLblOff(nLblCull)
            call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
            call FeQuestEdwClose(nEdwCull)
          endif
        endif
1200    if(DiamondUsed.gt.0) then
          call FeQuestRolMenuOpen(nRolMenuGasket,Gasket,3,GasketUsed)
          call FeQuestRealEdwOpen(nEdwDiamondDelta,2.*DiamondDeltaTheta,
     1                            .false.,.false.)
          call FeQuestRealEdwOpen(nEdwGasketDelta,2.*GasketDeltaTheta,
     1                            .false.,.false.)
        else
          call FeQuestRolMenuClose(nRolMenuGasket)
          if(EdwStateQuest(nEdwDiamondDelta).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwDiamondDelta,pom)
            DiamondDeltaTheta=pom*.5
            call FeQuestEdwClose(nEdwDiamondDelta)
          endif
          if(EdwStateQuest(nEdwGasketDelta).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwGasketDelta,pom)
            GasketDeltaTheta=pom*.5
            call FeQuestEdwClose(nEdwGasketDelta)
          endif
        endif
1400    if(CrwLogicQuest(nCrwDisplayObserved)) then
          call FeQuestRealEdwOpen(nEdwSigLevel,3.,.false.,.false.)
          call FeQuestLblOn(nLblSigLevel)
        else
          call FeQuestRealFromEdw(nEdwSigLevel,ObsLevelCull)
          call FeQuestEdwClose(nEdwSigLevel)
          call FeQuestLblOff(nLblSigLevel)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwHighPressure)
     1    then
          if(CrwLogicQuest(nCrwHighPressure)) then
            DiamondUsed=1
            GasketUsed=1
          else
            DiamondUsed=0
          endif
          go to 1200
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.eq.nCrwDisplayAll.or.
     2           CheckNumber.eq.nCrwDisplayObserved.or.
     3           CheckNumber.eq.nCrwDisplayCulled.or.
     4           CheckNumber.eq.nCrwDisplayMeasuredTwice.or.
     5           CheckNumber.eq.nCrwDisplayMeasuredOnce)) then
          go to 1400
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCull) then
          UseAutomaticCulling=CrwLogicQuest(nCrwCull)
          go to 1100
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          GasketUsed=RolMenuSelectedQuest(nRolMenuGasket)
          call FeQuestRealFromEdw(nEdwDiamondDelta,pom)
          DiamondDeltaTheta=pom*.5
          call FeQuestRealFromEdw(nEdwGasketDelta,pom)
          GasketDeltaTheta=pom*.5
          if(.not.UseAutomaticCulling) then
            FLimCull(KDatBlock)=-1.
          else
            call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
          endif
          if(CrwLogicQuest(nCrwDisplayAll)) then
            DisplaySelect=DisplayAll
          else if(CrwLogicQuest(nCrwDisplayObserved)) then
            DisplaySelect=DisplayObserved
            call FeQuestRealFromEdw(nEdwSigLevel,ObsLevelCull)
          else if(CrwLogicQuest(nCrwDisplayCulled)) then
            DisplaySelect=DisplayCulled
          else if(CrwLogicQuest(nCrwDisplayMeasuredTwice)) then
            DisplaySelect=DisplayMeasuredTwice
          else if(CrwLogicQuest(nCrwDisplayMeasuredOnce)) then
            DisplaySelect=DisplayMeasuredOnce
          endif
          if(CrwLogicQuest(nCrwDrawIAve)) then
            if(XUse.ne.XUseTheta) XUse=XUseIAve
            YUse=YUseIAve
          else if(CrwLogicQuest(nCrwDrawWdF)) then
            if(XUse.ne.XUseTheta) XUse=XUseFobs
            YUse=YUseWdF
          else
            XUse=XUseFobs
            YUse=YUseFcalc
          endif
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) then
          XUse=XUseOld
          YUse=YUseOld
          go to 9999
        endif
      endif
      call SetRealArrayTo(GasketTheta,22,0.)
      if(GasketUsed.gt.0) then
        do i=1,22
          if(GasketD(i,GasketUsed).lt.0.) exit
          pom=.5/GasketD(i,GasketUsed)*LamAveRefBlock(KRefBlock)
          if(pom.gt..99) then
            GasketTheta(i)=-20.
          else
            GasketTheta(i)=asin(pom)/ToRad
          endif
        enddo
      endif
      if(DiamondUsed.gt.0) then
        do i=1,22
          pom=.5/DiamondD(i)*LamAveRefBlock(KRefBlock)
          if(pom.gt..99) then
            DiamondTheta(i)=-20.
          else
            DiamondTheta(i)=asin(pom)/ToRad
          endif
        enddo
      endif
      if(YUse.eq.YUseOld) then
        XUse=XUseOld
      else
        Zoomed=.false.
      endif
9999  return
      end
      subroutine EM9ManualFilter(Key)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*80 Veta
      integer SatInd,SatIndMax,EM9SortIndex
      logical CrwLogicQuest,lpom
      if(Key.eq.0) then
        ReflUse=ReflUseAll
      else
        SatIndMax=0
        do i=1,NAve
          if(DisplaySelect.ne.DisplayAll) then
            if(DisplaySelect.eq.DisplayObserved) then
              if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
            else
              n=0
              do j=1,NAveRedAr(i)
                m=Ave2Org(j,i)
                if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1            then
                  go to 1120
                else if(ICullAr(m).eq.0) then
                  n=n+1
                endif
              enddo
              if(DisplaySelect.eq.DisplayCulled) then
                cycle
              else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1                 n.eq.2).or.
     2                (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
                go to 1120
              else
                cycle
              endif
            endif
          endif
1120      k=HCondAveAr(i)
          call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
          SatIndMax=max(SatIndMax,EM9SortIndex(ih)-2)
        enddo
        SatInd=max(ReflUse,1)
        SatInd=min(SatInd,SatIndMax)
        id=NextQuestId()
        xqd=300.
        il=4
        Veta='Reflections used in refinement:'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=0
        xpom=5.
        tpom=xpom+CrwXd+10.
        ichk=1
        igrp=1
        Veta='%All reflections'
        do i=-2,1
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          if(i.eq.-2) then
            nCrwSelRefFirst=CrwLastMade
            Veta='%Main reflections'
          else if(i.eq.-1) then
            Veta='All %satellites'
          else if(i.eq.0) then
            Veta='S%pecified satellites'
          else if(i.eq.1) then
            nCrwSelRefLast=CrwLastMade
          endif
          if(i.le.0) then
            lpom=i.eq.ReflUse
          else
            lpom=ReflUse.gt.0
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if((i.eq.0.and.SatIndMax.le.0).or.
     1       (i.eq.1.and.SatIndMax.le.1))
     2      call FeQuestCrwDisable(CrwLastMade)
        enddo
        tpom=tpom+FeTxLengthUnder(Veta)+5.
        Veta='=>'
        xpom=tpom+FeTxLengthUnder(Veta)+7.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSatInd=EdwLastMade
1400    if(CrwLogicQuest(nCrwSelRefLast)) then
          call FeQuestIntEdwOpen(nEdwSatInd,SatInd,.false.)
          call FeQuestEudOpen(nEdwSatInd,1,2,1,0.,0.,0.)
        else
          call FeQuestEdwClose(nEdwSatInd)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw) then
          go to 1400
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          do nCrw=nCrwSelRefFirst,nCrwSelRefLast
            if(CrwLogicQuest(nCrw)) then
              ReflUse=ReflUseAll+nCrw-nCrwSelRefFirst
              exit
            endif
          enddo
          if(ReflUse.eq.1) call FeQuestIntFromEdw(nEdwSatInd,ReflUse)
        endif
        call FeQuestRemove(id)
      endif
9999  return
      end
      subroutine EM9HKLUpdate
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      logical ExistFile
      KRefBlock=1
      ExistM95=ExistFile(fln(:ifln)//'.m95')
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom95(0,fln(:ifln)//'.m95')
      call iom40(0,0,fln(:ifln)//'.m40')
      ParentM90=' '
      SilentRun=.true.
      call EM9HKLReimport
      SilentRun=.false.
      return
      end
