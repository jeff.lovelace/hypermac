      subroutine dist
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*128 Radka,Hlavicka
      character*80, allocatable ::  AtPlane(:)
      character*80  t80,AtTor(4)
      character*15  t15
      character*11  FormD
      character*8 text(2),nazev
      dimension fmp(4,20),sfmp(4,20),ip(2),xs(3),xp(6),nx4(3),dx4(3),
     1          t(3),nd(4)
      logical iesd,ExistFile,PrvniTorzniUhel,OldPlane,PrvniRovina
      logical CIFAllocated
      real, allocatable :: sngc(:,:,:),csgc(:,:,:)
      data text/'torsion','plane'/
      Uloha='Calculating of distances, angles, torsion angles and '//
     1      'best planes'
      t80=fln(:ifln)//'_dist.tmp'
      call DeleteFile(t80)
      call OpenFile(lst,fln(:ifln)//'.dis','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      LnDist=NextLogicNumber()
      call OpenFile(LnDist,fln(:ifln)//'_dist.txt','formatted',
     1              'unknown')
      LnAngle=NextLogicNumber()
      call OpenFile(LnAngle,fln(:ifln)//'_angle.txt','formatted',
     1              'unknown')
      LnTors=NextLogicNumber()
      call OpenFile(LnTors,fln(:ifln)//'_tors.txt','formatted',
     1              'unknown')
      LnHBonds=NextLogicNumber()
      call OpenFile(LnHBonds,fln(:ifln)//'_hbonds.txt','formatted',
     1              'unknown')
      IDist=0
      IAngle=0
      ITors=0
      LstOpened=.true.
      VolaToDist=.true.
      call iom50(0,1,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9910
      call ComSym(0,1,ich)
      if(ngc(KPhase).gt.0) then
        allocate(sngc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc),
     1           csgc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc))
        call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),NAtCalc)
        if(ErrFlag.ne.0) go to 9999
      endif
      call specat
      n=NAtCalc+5
      allocate(xdst(3,n),AtPlane(n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call DefaultDist
      call NactiDist
      if(ErrFlag.ne.0) go to 9999
      ncvt=NSymmN(KPhase)
      if(iuhl.eq.1) FullCoo=1
      write(FormTD(2:2),'(i1)') NDimI(KPhase)
      write(FormTA(2:2),'(i1)') NDimI(KPhase)
      if(MaxUsedKw(KPhase).le.0.and.NComp(KPhase).le.1)
     1  call SetIntArrayTo(nt,9,0)
      if(nt(1,1).ne.0) then
        if(kcommen(KPhase).le.0) then
          do i=1,NDimI(KPhase)
            nt(i,1)=nt(i,1)+1
            dt(i,1)=(tlast(i,1)-tfirst(i,1))/float(nt(i,1)-1)
            do j=2,NComp(KPhase)
              nt(i,j)=nt(i,1)
              tfirst(i,j)=tfirst(i,1)
              tlast(i,j)=tlast(i,1)
              dt(i,j)=(tlast(i,j)-tfirst(i,j))/float(nt(i,j)-1)
            enddo
          enddo
        else
          do isw=1,NComp(KPhase)
            call CopyVek(trez(1,isw,KPhase),tfirst(1,isw),NDimI(KPhase))
            call CopyVekI(NCommQ(1,isw,KPhase),nt(1,isw),NDimI(KPhase))
            do i=1,NDimI(KPhase)
              nt(i,isw)=nt(i,isw)
              dt(i,isw)=1./float(nt(i,isw))
              tlast(i,isw)=tfirst(i,isw)+float(nt(i,isw)-1)*dt(i,isw)
            enddo
          enddo
        endif
      endif
      do i=1,NComp(KPhase)
        ntall(i)=1
        do j=1,NDimI(KPhase)
          ntall(i)=ntall(i)*nt(j,i)
        enddo
      enddo
      if(ntall(1).le.1.or.NDimI(KPhase).ne.1) ittab=0
      if(ittab.ne.0.and.ieach.eq.0) ieach=max(ntall(1)/10,1)
      pom=0.
      call inm40(iesd)
      if(ErrFlag.ne.0) go to 9900
      if(iesd) then
        call newln(2)
        write(lst,'(/''Input e.s.d of fraction coordinates of atoms '',
     1               ''included'')')
      else
        irnd=0
      endif
      call pisat
      call distat
      if(ErrFlag.ne.0) go to 9900
      if(NAtH(KPhase).gt.0.and..not.ICDDBatch) then
        CalcHBonds=.true.
        call DistAt
        CalcHBonds=.false.
      endif
4000  nr=0
      if(.not.ExistFile(fln(:ifln)//'_torpl.tmp')) go to 9000
      rewind 55
      PrvniTorzniUhel=.true.
      PrvniRovina=.true.
4100  read(55,FormA,err=7000,end=7000) Radka
      if(Radka(1:1).eq.'*') go to 4100
      kk=0
      call mala(Radka)
      call kus(Radka,kk,nazev)
      if(nazev.eq.'end') go to 7000
      i=islovo(nazev,text,2)
      if(i.le.0) go to 4100
      go to (5000,6000),i
5000  if(PrvniTorzniUhel) then
        call NewPg(0)
        call TitulekVRamecku('List of torsion angles')
        PrvniTorzniUhel=.false.
      endif
      do i=1,4
        call kus(Radka,kk,attor(i))
        call uprat(attor(i))
      enddo
      call DistTorsionAngle(AtTor,Radka)
      go to 4100
6000  if(PrvniRovina) then
        call NewPg(0)
        call TitulekVRamecku('List of best planes')
        if(NDimI(KPhase).gt.0)
     1    call OpenFile(79,fln(:ifln)//'_plane.txt','formatted',
     2                  'unknown')
        PrvniRovina=.false.
      endif
      kkp=kk
      call StToInt(Radka,kk,ip,2,.false.,ich)
      if(ich.eq.0) then
        n1=ip(1)
        n2=ip(2)
        if(n1.lt.3) then
          call FeChybne(-1.,-1.,Radka,'number of atoms defining plane'//
     1                  ' than 3.',SeriousError)
          go to 4100
        endif
        OldPlane=.true.
        kk=len(Radka)
        nn=n2
      else
        kk=kkp
        nn=NAtAll
        OldPlane=.false.
        n1=0
      endif
      do i=1,nn
        if(kk.eq.len(Radka)) then
          if(OldPlane) then
            read(55,FormA) Radka
            call mala(Radka)
            kk=0
          else
            n2=i-1
            if(n1.le.0) n1=n2
            go to 6025
          endif
        endif
6010    call kus(Radka,kk,t80)
        if(.not.OldPlane) then
          if(t80.eq.'|') then
            n1=i-1
            go to 6010
          else if(t80.eq.'&') then
            read(55,FormA) Radka
            call mala(Radka)
            kk=0
            go to 6010
          endif
        endif
        AtPlane(i)=t80
        call uprat(AtPlane(i))
        call atsym(AtPlane(i),n,xs,xp,xp(4),jsym,ich)
        if(n.le.0) then
          t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' doesn''t '//
     1        'exist.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        else
          if(i.eq.1) then
            isw=iswa(n)
          else
            if(isw.ne.iswa(n)) then
              t80='atoms belong to different composite parts.'
              call FeChybne(-1.,-1.,Radka,t80,SeriousError)
              go to 4100
            endif
          endif
        endif
        if(ich.eq.1) then
          t80='string contains non-numerical character.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        else if(ich.eq.3) then
          t80='atom '//AtPlane(i)(:idel(AtPlane(i)))//' has incorrect '
     1        //'symmetry part.'
          call FeChybne(-1.,-1.,Radka,t80,SeriousError)
          go to 4100
        endif
      enddo
6025  call DistBestPlane(AtPlane,n1,n2,iesd,isw,nr,fmp,sfmp)
      go to 4100
7000  close(55,status='delete')
      if(nr.lt.1.or.(nr.lt.2.and.NDirRef.le.0)) go to 9000
      if(NDimI(KPhase).gt.0) then
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                               sxdk)
        allocate(oi(n),oj(n),ok(n),xdi(3,n),xdj(3,n),xdk(3,n),sxdi(3,n),
     1           sxdj(3,n),sxdk(3,n))
        call SetIntArrayTo (nx4,NDimI(KPhase),1)
        call SetRealArrayTo(dx4,NDimI(KPhase),0.)
        call SetRealArrayTo(t,3,0.)
        FormD='( f8.3,a11)'
        if(NDimI(KPhase).eq.1) then
          Hlavicka='    t       Angle'
          NFormD=19
        else
          Hlavicka='    t(1)    t(2)'
          NFormD=27
          if(NDimI(KPhase).eq.3) then
            Hlavicka(17:)='    t(3)'
            NFormD=NFormD+8
          endif
          Hlavicka=Hlavicka(:idel(Hlavicka))//'    Angle'
          write(FormD(2:2),'(i1)') NDimI(KPhase)
        endif
        m=128/NFormD
        do i=1,m-1
          Hlavicka(NFormD*i+1:)=Hlavicka(:NFormD)
        enddo
      endif
      do id=1,2
        if(id.eq.1) then
          if(nr.le.2) cycle
          Radka='List dihedral angles'
        else
          if(NDirRef.le.0) cycle
          Radka='List of angles betwen calculated planes and '//
     1          'reference directions'
        endif
        call NewPg(0)
        call TitulekVRamecku(Radka)
        ichi=0
        ichj=0
        do i=1,nr
          if(i.eq.nr.and.id.eq.1) cycle
          if(NDimI(KPhase).gt.0) call DistReadPlane(i,oi,xdi,sxdi,ichi)
          write(Nazev,'(i5)') i
          call Zhusti(Nazev)
          if(id.eq.1) then
            jp=i+1
            jk=nr
          else
            jp=1
            jk=NDirRef
          endif
          do j=jp,jk
            if(id.eq.2) then
              call CopyVek(DirRef(1,j),xp,3)
              call multm(MetTensI(1,isw,KPhase),xp,xs,3,3,1)
              call VecNor(xp,xs)
            endif
            if(NDimI(KPhase).gt.0.and.ichi.eq.0) then
              if(id.eq.1) then
                call DistReadPlane(j,oj,xdj,sxdj,ichj)
              else
                do kt=1,ntall(isw)
                  oj(kt)=1.
                  xdj(1,kt)=xp(1)
                  xdj(2,kt)=xp(2)
                  xdj(3,kt)=xp(3)
                  sxdj(1,kt)=0.
                  sxdj(2,kt)=0.
                  sxdj(3,kt)=0.
                enddo
                ichj=0
              endif
               dumav=0.
              sdumav=0.
               dummx=0.
              sdummx=0.
              dummn=9999.
              sdummn=0.
              n=0
              if(ichj.eq.0) then
                do kt=1,ntall(isw)
                  ok(kt)=0.
                  call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
                  do k=1,NDimI(KPhase)
                    t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
                  enddo
                  if(oi(kt).le.0..or.oj(kt).le.0.) cycle
                  ok(kt)=1.
                  call DistDihedralAngle(xdi(1,kt),xdj(1,kt),sxdi(1,kt),
     1                                   sxdj(1,kt),isw,xdk(1,kt),
     2                                   sxdk(1,kt))
                  if(xdk(1,kt).lt.dummn) then
                     dummn= xdk(1,kt)
                    sdummn=sxdk(1,kt)
                  endif
                  if(xdk(1,kt).gt.dummx) then
                     dummx= xdk(1,kt)
                    sdummx=sxdk(1,kt)
                  endif
                   dumav= dumav+ xdk(1,kt)
                  sdumav=sdumav+sxdk(1,kt)
                  n=n+1
                enddo
                if(n.gt.0) then
                   dumav= dumav/float(n)
                  sdumav=sdumav/float(n)
                endif
              endif
            endif
            if(id.eq.1) then
              write(Cislo,FormI15) j
              call Zhusti(Cislo)
              Radka='Dihedral angle between plane#'//
     1              Nazev(:idel(Nazev))//
     2              ' and plane#'//Cislo(:idel(Cislo))
            else
              write(t80,'(3f10.3)')(DirRef(k,j),k=1,3)
              call ZdrcniCisla(t80,3)
              t80='('//t80(:idel(t80))//')'
              do k=1,idel(t80)
                if(t80(k:k).eq.' ') t80(k:k)=','
              enddo
              Radka='Angle between plane#'//Nazev(:idel(Nazev))//
     2              ' and direction '//t80(:idel(t80))
            endif
            if(NDimI(KPhase).gt.0) then
              Radka(idel(Radka)+1:)=' - Average : '
              call DistRoundESD(t15,dumav,sdumav,2)
              Radka=Radka(:idel(Radka)+1)//t15(:idel(t15))
              Radka=Radka(:idel(Radka))//', Minimum : '
              call DistRoundESD(t15,dummn,sdummn,2)
              Radka=Radka(:idel(Radka)+1)//t15(:idel(t15))
              Radka=Radka(:idel(Radka))//', Maximum : '
              call DistRoundESD(t15,dummx,sdummx,2)
              Radka=Radka(:idel(Radka)+1)//t15(:idel(t15))
            else
              if(id.eq.1) then
                k=j
              else
                k=nr+1
                call CopyVek(xp,fmp(1,k),3)
                sfmp(1,k)=0.
                sfmp(2,k)=0.
                sfmp(3,k)=0.
              endif
              call DistDihedralAngle(fmp(1,i),fmp(1,k),sfmp(1,i),
     1                               sfmp(1,k),isw,du,sdu)
              call DistRoundESD(t15,du,sdu,2)
              Radka=Radka(:idel(Radka))//': '//t15(:idel(t15))
            endif
            call NewLn(1)
            write(lst,FormA)
            call NewLn(1)
            write(lst,FormA) Radka(:idel(Radka))
            if(NDimI(KPhase).gt.0.and.ieach.gt.0) then
              call NewLn(3)
              write(lst,FormA)
              write(lst,FormA) Hlavicka(:idel(Hlavicka))
              write(lst,FormA)
              Radka=' '
              n=1
              do kt=1,ntall(isw),ieach
                if(ok(kt).le.0.) cycle
                call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
                do k=1,NDimI(KPhase)
                  t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
                enddo
                if(n.gt.128-NFormD) then
                  callNewLn(1)
                  write(lst,FormA) Radka(:idel(Radka))
                  n=1
                  Radka=' '
                endif
                call DistRoundESD(t15,xdk(1,kt),sxdk(1,kt),2)
                write(Radka(n:),FormD)(t(k),k=1,NDimI(KPhase)),
     1                                 t15(:idel(t15))
                n=n+NFormD
              enddo
              if(Radka.ne.' ') then
                callNewLn(1)
                write(lst,FormA) Radka(:idel(Radka))
              endif
            endif
          enddo
        enddo
      enddo
9000  call CloseListing
      close(79,status='delete')
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_Dist.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# Dist'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      if(IDist.gt.0) then
        rewind LnDist
9010    read(LnDist,FormA,end=9020) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9010
      endif
9020  if(IAngle.gt.0) then
        rewind LnAngle
        write(LnSum,FormA)
9030    read(LnAngle,FormA,end=9040) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9030
      endif
9040  if(ITors.gt.0) then
        rewind LnTors
        write(LnSum,FormA)
9050    read(LnTors,FormA,end=9060) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9050
      endif
9060  if(IHBonds.gt.0) then
        rewind LnHBonds
        write(LnSum,FormA)
9070    read(LnHBonds,FormA,end=9080) t80
        write(LnSum,FormA) t80(:idel(t80))
        go to 9070
      endif
9080  call CloseIfOpened(LnSum)
      call CloseIfOpened(m50)
      if(.not.BatchMode)
     1  call FeShowListing(-1.,-1.,'DIST program',fln(:ifln)//'.dis',
     2                     10000)
9900  call DeleteFile(PreviousM40)
      close(LnDist,Status='delete')
      close(LnAngle,Status='delete')
      close(LnTors,Status='delete')
      close(LnHBonds,Status='delete')
9910  if(ngcMax.ne.0) call DeleteFile(PreviousM50)
9999  if(allocated(xdst)) deallocate(xdst,AtPlane,BetaDst,dxm,BratPrvni,
     1  BratDruhyATreti,aselPrvni,aselDruhyATreti)
      if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,sxdk)
      if(.not.CIFAllocated) deallocate(CifKey,CifKeyFlag)
      if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
      if(allocated(DirRef)) then
        deallocate(DirRef)
        NDirRefMax=0
      endif
      if(allocated(sngc)) deallocate(sngc,csgc)
      if(NDimI(KPhase).gt.0) call TrOrthoS(1)
      call iom50(0,0,fln(:ifln)//'.m50')
      return
      end
      subroutine DistBestPlane(AtPlane,n1,n2,iesd,isw,nr,fn,sfn)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension fn(4,*),sfn(4,*),XPlane0(:,:),SXPlane0(:,:),XPlane(:,:),
     1          SXPlane(:,:),x4dif(3),tdif(:,:),nx4(3),dx4(3),oall(:,:),
     2          uxi(:,:,:),uyi(:,:,:),suxi(:,:,:),suyi(:,:,:),
     3          GammaIntInv(:,:),
     4          isig(4),t(3),xp(3),xpp(3),iarr(:),dx(:),idx(:),fnp(4),
     5          sfnp(4),nd(4),ish(3)
      character*(*) AtPlane(*)
      character*128 Ven1,Ven2,Ven3
      logical iesd,AllAtomsOccupied
      allocatable oall,XPlane0,SXPlane0,XPlane,SXPlane,tdif,uxi,uyi,
     1            suxi,suyi,GammaIntInv,iarr,dx,idx
      call CopyVek (dt(1,isw),dti,NDimI(KPhase))
      n=NAtAll
      allocate(XPlane0(3,n),SXPlane0(3,n),XPlane(3,n),SXPlane(3,n),
     1         tdif(3,n),uxi(3,mxw,n),uyi(3,mxw,n),suxi(3,mxw,n),
     2         suyi(3,mxw,n),GammaIntInv(9,n),iarr(n),dx(n),idx(n))
      do i=1,n2
        call AtSymSpec(AtPlane(i),ia,XPlane0(1,i),x4dif,tdif(1,i),isym,
     1                 icenter,ish,ich)
        iarr(i)=ia
        if(NDim(KPhase).le.3) go to 1400
        call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv(1,i))
        do l=1,KModA(2,ia)
          call multm (rm(1,isym,isw,KPhase), ux(1,l,ia), uxi(1,l,i),3,3,
     1                1)
          call multmq(rm(1,isym,isw,KPhase),sux(1,l,ia),suxi(1,l,i),3,3,
     1                1)
          if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
             call multm (rm(1,isym,isw,KPhase), uy(1,l,ia), uyi(1,l,i),
     1                   3,3,1)
             call multmq(rm(1,isym,isw,KPhase),suy(1,l,ia),suyi(1,l,i),
     1                   3,3,1)
          else
             call CopyVek( uy(1,l,ia), uyi(1,l,i),3)
             call CopyVek(suy(1,l,ia),suyi(1,l,i),3)
          endif
          if(isym.lt.0) then
            do j=1,3
              uxi(j,l,i)=-uxi(j,l,i)
              if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0)
     1          uyi(j,l,i)=-uyi(j,l,i)
            enddo
          endif
        enddo
1400    call multmq(rm(1,isym,isw,KPhase),sx(1,ia),SXPlane0(1,i),3,3,1)
      enddo
      nr=nr+1
      if(NDimI(KPhase).gt.0) then
        allocate(oall(1,n2))
        if(VolaToDist) write(79,'(''plane'',i2)') nr
      endif
      if(VolaToDist) then
        Ven1='Equation of best plane through atoms :'
        idlp=idel(Ven1)+1
        idl =idlp
        do j=1,n1
          idla=idel(AtPlane(j))
          if(idla+idl.gt.127) then
            call newln(1)
            write(lst,FormA) Ven1(:idel(Ven1))
            Ven1=' '
            idl=idlp
          endif
          Ven1=Ven1(:idl)//AtPlane(j)(:idla)
          idl=idl+idla+1
        enddo
        if(idl.gt.idlp) then
          call newln(1)
          write(lst,FormA)
          call newln(1)
          write(lst,FormA) Ven1(:idel(Ven1))
        endif
        call newln(1)
        write(lst,FormA)
        call DistGetOneBestPlane(XPlane0,SXPlane0,n1,n2,iesd,isw,
     1                           fn(1,nr),sfn(1,nr),isig,chiq,dx,idx)
        Ven2=' '
        do i=1,4
          write(Cislo,'(f8.4)') fn(i,nr)
          call Zhusti(Cislo)
          if(i.eq.1) then
            Ven1=Cislo
          else
            if(Cislo(1:1).ne.'-') Cislo='+'//Cislo(:idel(Cislo))
            Ven1=Ven1(:idel(Ven1))//Cislo(:idel(Cislo))
          endif
          idl=idel(Ven1)
          if(i.le.3) then
            Ven1=Ven1(:idl)//'*'//smbx(i)
          else
            Ven1=Ven1(:idl)//' = 0'
          endif
          write(Cislo,FormI15) isig(i)
          call Zhusti(Cislo)
          Cislo='('//Cislo(:idel(Cislo))//')'
          j=idel(Cislo)
          Ven2(idl-j+2:)=Cislo(:j)
        enddo
        write(Cislo,'(f15.3)') chiq
        call Zhusti(Cislo)
        write(Ven1(idel(Ven1)+5:),'(''ChiQ = '',a)') Cislo(:idel(Cislo))
        call newln(2)
        write(lst,FormA) Ven1(:idel(Ven1)),Ven2(:idel(Ven2))
        call newln(1)
        write(lst,FormA)
        Ven1='Atom name :'
        Ven2='Distance from plane :'
        Ven3=' '
        l=22
        do i=1,n2
          Ven1=Ven1(1:l+5)//(AtPlane(i)(1:8))
          if(idel(AtPlane(i)).gt.8) Ven3=Ven3(1:l+4)//(AtPlane(i)(9:16))
          write(Ven2(l+1:),'(f7.4)') dx(i)
          write(Cislo,FormI15) idx(i)
          call Zhusti(Cislo)
          Cislo='('//Cislo(:idel(Cislo))//')'
          Ven2=Ven2(:idel(Ven2))//Cislo(:idel(Cislo))
          l=l+12
          if(l.gt.117) then
            idl=idel(Ven3)
            if(idl.gt.0) then
              j=4
            else
              j=3
            endif
            call NewLn(j)
            write(lst,FormA)
            write(lst,FormA) Ven1(:idel(Ven1))
            if(idl.gt.0) write(lst,FormA) Ven3(:idel(Ven3))
            write(lst,FormA) Ven2(:idel(Ven2))
            l=22
            Ven1(23:)=' '
            Ven2(23:)=' '
            Ven3(23:)=' '
          endif
        enddo
        if(l.gt.22) then
          idl=idel(Ven3)
          if(idl.gt.0) then
            j=4
          else
            j=3
          endif
          call NewLn(j)
          write(lst,FormA)
          write(lst,FormA) Ven1(:idel(Ven1))
          if(idl.gt.0) write(lst,FormA) Ven3(:idel(Ven3))
          write(lst,FormA) Ven2(:idel(Ven2))
        endif
        if(NDimI(KPhase).le.0) go to 9999
        Ven1=' '
        j=5
        call NewLn(1)
        write(lst,FormA)
        do i=1,NDimI(KPhase)
          Ven1(j:j)=smbt(i)
          j=j+8
        enddo
        j=NDimI(KPhase)*8+8
        do i=0,3
          Ven1(j:j)=char(ichar('A')+i)
          j=j+9
        enddo
        Ven1(j+25:)='Distances'
        call NewLn(2)
        write(lst,FormA) Ven1(:idel(Ven1))
        write(lst,FormA)
      else
        call OpenFile(78,fln(:ifln)//'.scr','unformatted','unknown')
      endif
      call SetIntArrayTo (nx4,NDimI(KPhase),1)
      call SetRealArrayTo(dx4,NDimI(KPhase),0.)
      call SetRealArrayTo(t,3,0.)
      do 2000kt=1,ntall(isw)
        call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
        do k=1,NDimI(KPhase)
          t(k)=tfirst(k,isw)+(nd(k)-1)*dti(k)
        enddo
        AllAtomsOccupied=.true.
        do i=1,n2
          ia=iarr(i)
          call AddVek(t,tdif(1,i),xpp,NDimI(KPhase))
          call multm(GammaIntInv(1,i),xpp,xp,NDimI(KPhase),
     1               NDimI(KPhase),1)
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(oall(1,i),qcnt(1,ia),xp,nx4,dx4,a0(ia),
     1                      ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                      GammaIntInv(1,i))
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1        call MakeOccModSawTooth(oall(1,i),qcnt(1,ia),xp,nx4,dx4,
     2          uyi(1,KModA(2,ia),i),uyi(2,KModA(2,ia),i),KFA(2,ia),
     3          GammaIntInv(1,i))
          else
            call MakeOccModPol(oall(1,i),qcnt(1,ia),xp,nx4,dx4,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv(1,i),TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakePosMod(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i),uyi(1,1,i),KModA(2,ia),KFA(2,ia),
     2                      GammaIntInv(1,i))
          else
            call MakePosModPol(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                         uxi(1,1,i),uyi(1,1,i),KModA(2,ia),
     2                         ax(1,ia),a0(ia)*.5,GammaIntInv(1,i),
     3                         TypeModFun(ia))
          endif
          call AddVek(XPlane0(1,i),xpp,XPlane(1,i),3)
          call MakePosModS(xpp,qcnt(1,ia),xp,nx4,dx4,
     1                      uxi(1,1,i), uyi(1,1,i),
     2                     suxi(1,1,i),suyi(1,1,i),KModA(2,ia),
     3                     KFA(2,ia),GammaIntInv(1,i))
          call AddVekQ(SXPlane0(1,i),xpp,SXPlane(1,i),3)
          if(i.le.n1.and.oall(1,i).le.ocut) then
            if(VolaToDist) then
              write(79,100)(0.,j=1,7)
            else
              write(78) kt,(0.,j=1,4)
            endif
            go to 2000
          endif
        enddo
        call SetRealArrayTo(sfnp,4,0.)
        call DistGetOneBestPlane(XPlane,SXPlane,n1,n2,iesd,isw,fnp,sfnp,
     1                           isig,chiq,dx,idx)
        if(VolaToDist) then
          write(79,100) 1.,(fnp(i),sfnp(i),i=1,3)
          if(ieach.le.0) cycle
          if(mod(kt,ieach).eq.1.or.ieach.eq.1) then
            j=1
            Ven1=' '
            do i=1,NDimI(KPhase)
              write(Ven1(j:),'(f8.3)') t(i)
              j=j+8
            enddo
            j=j+3
            do i=1,4
              if(AllAtomsOccupied) then
                write(Ven1(j:),'(f9.4)') fnp(i)
              else
                write(Ven1(j:),'(4x,5(''-''))')
              endif
              j=j+9
            enddo
            j=j+3
            jp=j
            do i=1,n2
              write(Ven1(j:),'(f7.3)') dx(i)
              j=j+7
              if(j.gt.120) then
                call NewLn(1)
                write(lst,FormA) Ven1(:idel(Ven1))
                Ven1=' '
                j=jp
              endif
            enddo
            if(j.gt.jp) then
              call NewLn(1)
              write(lst,FormA) Ven1(:idel(Ven1))
            endif
          endif
        else
          write(78) kt,(fnp(i),i=1,3),1.
        endif
2000  continue
9999  if(NDimI(KPhase).gt.0) deallocate(oall)
      deallocate(XPlane0,SXPlane0,XPlane,SXPlane,tdif,uxi,uyi,suxi,
     1           suyi,GammaIntInv,iarr,dx,idx)
      return
100   format(f8.3,6f15.6)
      end
      subroutine DistGetOneBestPlane(XPlane,SXPlane,n1,n2,iesd,isw,
     1                               abcd,sabcd,isabcd,chiq,dx,idx)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension XPlane(3,*),SXPlane(3,*),dx(*),idx(*),w(3),wg(3),
     1          abcd(4),sabcd(4),isabcd(4),wt(n1)
      logical iesd
      do i=1,3
        u(i)=xplane(i,1)-xplane(i,2)
        v(i)=xplane(i,3)-xplane(i,2)
      enddo
      call VecMul(u,v,wg)
      call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
      call VecNor(w,wg)
      do i=1,10
        i3=1
        if(abs(w(2)).gt.abs(w(1))) i3=2
        if(abs(w(3)).gt.abs(w(2)).and.abs(w(3)).gt.abs(w(1))) i3=3
        i1=mod(i3,3)+1
        i2=6-i1-i3
        avx=0.
        avy=0.
        avz=0.
        pom=0.
        do j=1,n1
          if(iesd) then
            pom1=0.
            do k=1,3
              pom1=pom1+(wg(k)*sxplane(k,j))**2
            enddo
            if(pom1.gt.0.) pom1=1./pom1
          else
            pom1=1.
          endif
          avx=avx+xplane(i1,j)*pom1
          avy=avy+xplane(i2,j)*pom1
          avz=avz+xplane(i3,j)*pom1
          pom=pom+pom1
          wt(j)=pom1
        enddo
        avx=avx/pom
        avy=avy/pom
        avz=avz/pom
        pom3=w(i1)/w(i3)
        pom4=w(i2)/w(i3)
        cx=0.
        cy=0.
        cz=0.
        p1=0.
        p2=0.
        do j=1,n1
          xxx=xplane(i1,j)-avx
          yyy=xplane(i2,j)-avy
          zzz=xplane(i3,j)-avz
          uu=xxx*wg(i1)+yyy*wg(i2)+zzz*wg(i3)
          pom=wt(j)
          pom1=xxx-pom3*zzz
          pom2=yyy-pom4*zzz
          cx=cx+pom1**2  *pom
          cy=cy+pom1*pom2*pom
          cz=cz+pom2**2  *pom
          p1=p1+pom1*uu  *pom
          p2=p2+pom2*uu  *pom
        enddo
        pom=1./(cx*cz-cy**2)
        cz= cz*pom
        cy=-cy*pom
        cx= cx*pom
        w(i1)=cz*p1+cy*p2
        w(i2)=cy*p1+cx*p2
        w(i3)=-pom3*w(i1)-pom4*w(i2)
        pom1=0.
        pom2=0.
        do j=1,3
          wg(j)=wg(j)-w(j)
          pom1=pom1+abs(w(j))
          pom2=pom2+abs(wg(j))
        enddo
        call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
        call vecnor(w,wg)
        if(pom1/pom2.le..00001) go to 1600
      enddo
1600  if(wg(1).lt.0.) call RealVectorToOpposite(wg,wg,3)
      call CopyVek(wg,abcd,3)
      call SetRealArrayTo(sabcd,3,0.)
      call SetIntArrayTo(isabcd,4,0)
      if(iesd) then
        isabcd(i1)=nint(sqrt(cz)*10000.)
        isabcd(i2)=nint(sqrt(cx)*10000.)
        pom3=w(i1)/w(i3)
        pom4=w(i2)/w(i3)
        pom=pom3**2*cz+pom4**2*cx+2.*pom3*pom4*cy
        isabcd(i3)=nint(sqrt(pom)*10000.)
        if(i1.eq.1) then
          sabcd(1)=cz
          sabcd(2)=cx
          sabcd(3)=cy*2.
        else if(i1.eq.2) then
          sabcd(1)=pom
          sabcd(2)=cz
          sabcd(3)=-2.*(pom3*cz+pom4*cy)
        else if(i1.eq.3) then
          sabcd(1)=cx
          sabcd(2)=pom
          sabcd(3)=-2.*(pom3*cy+pom4*cx)
        endif
      endif
      abcd(4)=-wg(i1)*avx-wg(i2)*avy-wg(i3)*avz
      chiq=0.
      do i=1,n2
        xxx=xplane(i1,i)-avx
        yyy=xplane(i2,i)-avy
        zzz=xplane(i3,i)-avz
        vv=wg(i1)*xxx+wg(i2)*yyy+wg(i3)*zzz
        dx(i)=vv
        idx(i)=0
        if(iesd) then
          pom1=xxx-pom3*zzz
          pom2=yyy-pom4*zzz
          pom=pom1**2*cz+pom2**2*cx+2.*pom1*pom2*cy
          do j=1,3
            pom=pom+(wg(j)*sxplane(j,i))**2
          enddo
          idx(i)=nint(10000.*sqrt(pom))
          if(i.le.n1) chiq=chiq+vv**2*wt(i)
        endif
      enddo
      if(iesd) then
        pom1=avx-pom3*avz
        pom2=avy-pom4*avz
        isabcd(4)=nint(sqrt(pom1**2*cz+pom2**2*cx+
     1                      2.*pom1*pom2*cy)*10000.)
      endif
      return
      end
      subroutine DistDihedralAngle(x,y,sx,sy,isw,du,sdu)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(3),y(3),sx(3),sy(3),u(3),v(3)
      call multm(MetTensI(1,isw,KPhase),x,u,3,3,1)
      call multm(MetTensI(1,isw,KPhase),y,v,3,3,1)
      pom=scalmul(y,u)
      if(pom.ge. .999999) then
        du=0.
        sdu=0.
      else if(pom.le.-.999999) then
        du=180.
        sdu=0.
      else
        du=acos(pom)/torad
        if(u(3).ne.0.) then
          pom1=v(1)-u(1)/u(3)*v(3)
          pom2=v(2)-u(2)/u(3)*v(3)
        else
          pom1=0.
          pom2=0.
        endif
        if(v(3).ne.0.) then
          pom3=u(1)-v(1)/v(3)*u(3)
          pom4=u(2)-v(2)/v(3)*u(3)
        else
          pom3=0.
          pom4=0.
        endif
        sdu=sqrt((sx(1)*pom1**2+sx(2)*pom2**2+sx(3)*pom1*pom2+
     1            sy(1)*pom3**2+sy(2)*pom4**2+sy(3)*pom3*pom4)/
     2            (1.-pom**2))/torad
      endif
      if(du.gt.90.) then
        du=180.-du
        pom=-pom
      endif
      return
      end
      subroutine DistReadPlane(n,o,xd,sxd,ich)
      use Dist_mod
      include 'fepc.cmn'
      character*256 Radka
      dimension o(*),xd(3,*),sxd(3,*)
      logical EqIgCase
      ich=0
      rewind 79
1100  read(79,FormA,end=9000) Radka
      k=0
      call Kus(Radka,k,Cislo)
      if(EqIgCase(Cislo,'plane')) then
        call Kus(Radka,k,Cislo)
        call posun(Cislo,0)
        read(Cislo,FormI15,err=9000) i
        if(i.eq.n) go to 1200
      endif
      go to 1100
1200  i=0
1300  read(79,FormA,end=9999) Radka
      if(Radka.eq.' ') go to 1300
      k=0
      call Kus(Radka,k,Cislo)
      if(EqIgCase(Cislo,'plane')) go to 9999
      i=i+1
      read(Radka,100,err=9000,end=9000) o(i),(xd(j,i),sxd(j,i),j=1,3)
      go to 1300
9000  ich=1
9999  return
100   format(f8.3,6f15.6)
      end
      subroutine DistTorsionAngle(AtTor,Radka)
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xtor(3,4),xtorm(3,4),x4dif(3),tdif(3),nd(3),uxi(3,mxw),
     1          uyi(3,mxw),suxi(3,mxw),suyi(3,mxw),GammaIntInv(9),xp(3),
     2          xpp(3),t(3),sgx0(12),ish(3),oall(:,:),xdall(:,:,:),
     3          sxdall(:,:,:)
      character*(*) attor(4),Radka
      character*8 At(4)
      character*10 t10(4)
      character*15 t15
      character*80 t80
      character*128 ven
      allocatable oall,xdall,sxdall
      if(NDimI(KPhase).gt.0) then
        n=0
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        allocate(oall(n,4),xdall(3,n,4),sxdall(3,n,4))
      endif
      k1=1
      do i=1,4
        call AtSymSpec(attor(i),ia,xtor(1,i),x4dif,tdif,isym,icenter,
     1                 ish,ich)
        j=index(attor(i),'#')
        if(j.gt.0) then
          At(i)=attor(i)(:j-1)
        else
          At(i)=attor(i)
        endif
        if(ia.le.0) then
          t80='atom "'//attor(i)(:idel(attor(i)))//
     1        '" isn''t of the file M40'
          go to 9900
        else
          if(i.eq.1) then
            isw=iswa(ia)
          else
            if(isw.ne.iswa(ia)) then
              t80='atoms belong to different composite parts'
              go to 9900
            endif
          endif
        endif
        call CopyVek (dt(1,isw),dti,NDimI(KPhase))
        if(ich.eq.1) then
          t80='string contains non-numerical character'
          go to 9900
        else if(ich.eq.3) then
          t80='atom "'//attor(i)(:idel(attor(i)))//
     1        '" has incorrect symmetry part'
          go to 9900
        endif
        if(isym.gt.0) then
          icntrsm=1
        else
          icntrsm=2
        endif
        call ScodeCIF(isym,icenter,ish,isw,t10(i))
        if(NDim(KPhase).le.3) go to 1400
        call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv)
        do l=1,KModA(2,ia)
          call multm (rm(1,isym,isw,KPhase), ux(1,l,ia), uxi(1,l),3,3,1)
          call multmq(rm(1,isym,isw,KPhase),sux(1,l,ia),suxi(1,l),3,3,1)
          if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
             call multm (rm(1,isym,isw,KPhase), uy(1,l,ia), uyi(1,l),3,
     1                   3,1)
             call multmq(rm(1,isym,isw,KPhase),suy(1,l,ia),suyi(1,l),3,
     1                   3,1)
          else
             call CopyVek( uy(1,l,ia), uyi(1,l),3)
             call CopyVek(suy(1,l,ia),suyi(1,l),3)
          endif
          if(isym.lt.0) then
            do j=1,3
              uxi(j,l)=-uxi(j,l)
              if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) uyi(j,l)=-uyi(j,l)
            enddo
          endif
        enddo
        call AddVek(tfirst(1,isw),tdif,xpp,3)
        call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
        if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
          call MakeOccMod(oall(1,i),qcnt(1,ia),xp,nt,dt,a0(ia),
     1                    ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                    GammaIntInv)
          if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1      call MakeOccModSawTooth(oall(1,i),qcnt(1,ia),xp,nt,dt,
     2        uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     3        GammaIntInv)
        else
          call MakeOccModPol(oall(1,i),qcnt(1,ia),xp,nt,dt,
     1                       ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                       ax(KModA(1,ia),ia),a0(ia)*.5,
     3                       GammaIntInv,TypeModFun(ia))
        endif
        if(TypeModFun(ia).le.1) then
          call MakePosMod(xdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,
     1                    KModA(2,ia),KFA(2,ia),GammaIntInv)
        else
          call MakePosModPol(xdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,
     1                       KModA(2,ia),ax(1,ia),a0(ia)*.5,GammaIntInv,
     2                       TypeModFun(ia))
        endif
        call MakePosModS(sxdall(1,1,i),qcnt(1,ia),xp,nt,dt,uxi,uyi,suxi,
     1                   suyi,KModA(2,ia),KFA(2,ia),GammaIntInv)
1400    call multmq(rm(1,isym,isw,KPhase),sx(1,ia),sgx(k1),3,3,1)
        k1=k1+3
      enddo
      call CopyVek(sgx,sgx0,12)
      call TorsionAngle(xtor,isw,Angle,SAngle)
      if(VolaToDist) then
        j=1
        do i=1,4
          write(ven(j:),FormA) attor(i)(:idel(attor(i)))
          j=idel(ven)+1
          if(i.ne.4) then
            ven(j:j)='-'
          else
            ven(j:j+2)=' : '
            j=j+2
          endif
          j=j+1
        enddo
        call DistRoundESD(t15,Angle,SAngle,2)
        i=idel(t15)
        ven(j:)=t15(:i)
        call newln(1)
        write(lst,FormA) Ven(:idel(Ven))
        if(ITors.le.0) then
          write(LnTors,'(''loop_'')')
          do i=34,42
            if(i.eq.38) cycle
            j=i
            if(i.ge.39.and.i.le.42.and.NDimI(KPhase).gt.0) j=j+24
            write(LnTors,FormCIF) ' '//CifKey(j,12)
          enddo
          if(NDimI(KPhase).le.0) then
            write(LnTors,FormCIF) ' '//CifKey(33,12)
            write(LnTors,FormCIF) ' '//CifKey(38,12)
          else
            do i=62,60,-1
              write(LnTors,FormCIF) ' '//CifKey(i,12)
            enddo
          endif
        endif
        ITors=ITors+1
        t80=At(1)
        do i=2,4
          t80=t80(:idel(t80))//' '//At(i)(:idel(At(i)))
        enddo
        do i=1,4
          t80=t80(:idel(t80))//' '//t10(i)(:idel(t10(i)))
        enddo
        if(NDimI(KPhase).le.0) then
          t80=t80(:idel(t80))//' '//t15(:idel(t15))//' ?'
          write(LnTors,FormA) '  '//t80(:idel(t80))
          go to 9999
        endif
      else
        call OpenFile(78,fln(:ifln)//'.scr','unformatted','unknown')
      endif
      dummx=-9999.
      dummn= 9999.
      dumav=0.
      sdumav=0.
      npom=0
      do kt=1,ntall(isw)
        call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
        k1=1
        do i=1,4
          if(oAll(kt,i).le.ocut) then
            oAll(kt,1)=0.
             dumkt=333.
            sdumkt=0.
            go to 2500
          endif
          call AddVek (xtor(1,i),xdall(1,kt,i),xtorm(1,i),3)
          call AddVekQ(sgx0(k1),sxdall(1,kt,i),sgx(k1),3)
        enddo
        call TorsionAngle(xtorm,isw,dumkt,sdumkt)
2500    if(VolaToDist) then
          if(dumkt.lt.300.) then
            dum (kt)= dumkt
            dums(kt)=sdumkt
            if(dumkt.gt.dummx) then
               dummx= dumkt
              sdummx=sdumkt
            endif
            if(dumkt.lt.dummn) then
               dummn= dumkt
              sdummn=sdumkt
            endif
             dumav= dumav+ dumkt
            sdumav=sdumav+sdumkt
            npom=npom+1
          endif
        else
          write(78) kt,dumkt,OAll(kt,1)
        endif
      enddo
      if(VolaToDist) then
        ven=' '
        l=1
        if(ieach.ne.0.and.npom.gt.0) then
          call newln(2)
          write(lst,'(/''Individual values:'')')
          do kt=1,ntall(isw),ieach
            call RecUnpack(kt,nd,nt(1,isw),NDimI(KPhase))
            do i=1,NDimI(KPhase)
              t(i)=tfirsti(i)+(nd(i)-1)*dti(i)
            enddo
            if(oi(kt).le.ocut) cycle
            call DistRoundESD(t15,dum(kt),dums(kt),2)
            write(ven(l:),'(f6.3,1x,a)') t(1),t15(:idel(t15))
            l=l+17
            if(l.gt.111) then
              call newln(1)
              write(lst,FormA) Ven(:idel(Ven))
              l=1
            endif
          enddo
          if(l.gt.1) then
            call newln(1)
            write(lst,FormA) Ven(:idel(Ven))
          endif
        endif
        if(npom.gt.0) then
          pom=1./float(npom)
           dumav= dumav*pom
          sdumav=sdumav*pom
          ven='Average : '
          call DistRoundESD(t15,dumav,sdumav,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
          ven=ven(:idel(ven))//', Minimum : '
          call DistRoundESD(t15,dummn,sdummn,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
          ven=ven(:idel(ven))//', Maximum : '
          call DistRoundESD(t15,dummx,sdummx,2)
          ven=ven(:idel(ven)+1)//t15(:idel(t15))
        else
          ven='The selected atoms are not coexisting'
        endif
        call newln(1)
        write(lst,FormA) Ven(:idel(Ven))
        call newln(1)
        write(lst,FormA)
      endif
      call RoundESD(t15,dumav,sdumav,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      call RoundESD(t15,dummn,sdummn,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      call RoundESD(t15,dummx,sdummx,2,0,0)
      t80=t80(:idel(t80))//' '//t15(:idel(t15))
      write(LnTors,FormA) '  '//t80(:idel(t80))//' ?'
      go to 9999
9900  call FeChybne(-1.,-1.,Radka,t80,SeriousError)
9999  if(NDimI(KPhase).gt.0) deallocate(oall,xdall,sxdall)
      return
116   format('torsion: ',4(a8,1x),2f9.3/9x,4(a7,2x))
      end
      subroutine TorsionAngle(xtor,isw,Angle,SAngle)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension w(3),wg(3),daw(3),dbu(3),dbv(3),dcv(3),dcw(3),dtw(3),
     1          xtor(3,4),dtu(3),dtv(3)
      do i=1,3
        u(i)=xtor(i,2)-xtor(i,1)
        v(i)=xtor(i,3)-xtor(i,2)
        w(i)=xtor(i,4)-xtor(i,3)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call multm(MetTens(1,isw,KPhase),w,wg,3,3,1)
      uu=scalmul(u,ug)
      uv=scalmul(u,vg)
      uw=scalmul(u,wg)
      vv=scalmul(v,vg)
      vw=scalmul(v,wg)
      ww=scalmul(w,wg)
      pa=uv*vw-uw*vv
      pb=uu*vv-uv**2
      pc=vv*ww-vw**2
      do i=1,3
        dau(i)=vg(i)*vw-wg(i)*vv
        dav(i)=ug(i)*vw+wg(i)*uv-2.*vg(i)*uw
        daw(i)=vg(i)*uv-ug(i)*vv
        dbu(i)=2.*(ug(i)*vv-vg(i)*uv)
        dbv(i)=2.*(vg(i)*uu-ug(i)*uv)
        dcv(i)=2.*(vg(i)*ww-wg(i)*vw)
        dcw(i)=2.*(wg(i)*vv-vg(i)*vw)
      enddo
      pom1=1./sqrt(pb*pc)
      pom=pom1*pa
      if(abs(pom).gt..999999) then
        if(pom.gt.0) then
          Angle=0.
          pom= .999999
        else
          Angle=180.
          pom=-.999999
        endif
      else
        Angle=acos(pom)/torad
      endif
      pom2=-pom1/sqrt((1.+pom)*(1.-pom))
      pom3=pa*.5/pb
      pom4=pa*.5/pc
      do i=1,3
        dtu(i)=pom2*(dau(i)-pom3*dbu(i))
        dtv(i)=pom2*(dav(i)-pom3*dbv(i)-pom4*dcv(i))
        dtw(i)=pom2*(daw(i)-pom4*dcw(i))
      enddo
      do i=1,3
        ddx(i)=-dtu(i)
        ddx(i+3)=dtu(i)-dtv(i)
        ddx(i+6)=dtv(i)-dtw(i)
        ddx(i+9)=dtw(i)
      enddo
      SAngle=sigma(sgx,ddx,12)/torad
      if(u(1)*v(2)*w(3)+u(3)*v(1)*w(2)+u(2)*v(3)*w(1)-
     1   u(3)*v(2)*w(1)-u(1)*v(3)*w(2)-u(2)*v(1)*w(3).lt.0.)
     2   Angle=-Angle
      return
      end
      subroutine round(x,sx,n)
      include 'fepc.cmn'
      if(sx.eq.0.) go to 9999
      if(n) 1000,9999,1000
 1000 f=1.
 1500 isx=sx*f+.05
      if(isx) 9999,1800,3000
 1800 f=f*10.
      go to 1500
 3000 sx=anint(sx*f)/f
      x=anint(x*f)/f
 9999 return
      end
      function sigma(sx,dx,n)
      include 'fepc.cmn'
      dimension sx(n),dx(n)
      s=0.
      do i=1,n
        s=s+(sx(i)*dx(i))**2
      enddo
      sigma=sqrt(s)
      return
      end
      subroutine DefaultDist
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      NactiInt=20
      NactiReal=20
      NactiComposed=20
      NactiKeys=NactiInt+NactiReal+NactiComposed
      if(NDimI(KPhase).le.1) then
        DefIntDist(nCmdieach)=10
      else
        DefIntDist(nCmdieach)=1
      endif
      call CopyVekI(DefIntDist,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntDist,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntDist,CmdIntDist,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealDist(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealDist(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealDist(i),CmdRealDist, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdDist(i)
      enddo
      RejectIdentical=1
      if(NDim(KPhase).eq.4) then
        j=100
      else
        j=20
      endif
      call SetIntArrayTo(nt,9,j)
      call SetRealArrayTo(tfirst,9,0.)
      call SetRealArrayTo(tlast,9,1.)
      nBondVal=0
      call SetIntArrayTo(iBondVal,400,0)
      call SetRealArrayTo(dBondVal,400,0.)
      call SetRealArrayTo(cBondVal,400,.37)
      iselPrvni=0
      iselDruhyATreti=0
      NDirRef=0
      return
      end
      subroutine NactiDist
      use Basic_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension ip(2),xp(1)
      character*80 t80,p80
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      ln=NextLogicNumber()
      call OpenFile(ln, fln(:ifln)//'.l55','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1000  read(m50,FormA,end=1090) t80
      write(ln,FormA) t80(:idel(t80))
      call mala(t80)
      k=0
      call kus(t80,k,Cislo)
      if(Cislo.ne.'dist') go to 1000
1010  read(m50,FormA,err=9999) t80
      k=0
      call kus(t80,k,Cislo)
      if(Cislo.eq.'plane') then
        call StToInt(t80,k,ip,2,.false.,ich)
        if(ich.ne.0) then
          go to 1050
        else
          n1=ip(1)
          n2=ip(2)
          p80='plane'
          n=0
          idp=6
1020      read(m50,FormA,end=9999) t80
          k=0
1030      call kus(t80,k,Cislo)
          idc=idel(Cislo)
          if(n.eq.n1) then
            if(idp+2.le.79) then
              p80=p80(:idp)//'|'
              idp=idp+2
            else
              p80=p80(:idp)//'&'
              write(ln,FormA) p80(:idp+1)
              p80=' |'
              idp=3
            endif
          endif
          if(idp+idc.le.79) then
            p80=p80(:idp)//Cislo(:idc)
            idp=idp+idc+1
            n=n+1
          else
            p80=p80(:idp)//'&'
            write(ln,FormA) p80(:idp+1)
            p80=Cislo
            idp=idc+1
          endif
          if(n.ge.n2) then
            t80=p80
            Cislo=' '
            go to 1050
          else
            if(k.lt.80) then
              go to 1030
            else
              go to 1020
            endif
          endif
        endif
      endif
1050  write(ln,FormA) t80(:idel(t80))
      if(Cislo.eq.'end') then
        go to 1000
      else
        go to 1010
      endif
1090  close(m50)
      close(ln)
      call MoveFile(fln(:ifln)//'.l55',fln(:ifln)//'.l51')
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call OpenFile(55,fln(:ifln)//'_torpl.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('dist',i)
      if(i.ne.1) go to 9999
      i=NactiInt+NactiReal+2
      call SetIntArrayTo(NactiRepeat,i,0)
      call SetIntArrayTo(NactiRepeat(i+1),NactiKeys-i,-1)
      PreskocVykricnik=.true.
      izpet=0
      nr=0
1100  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0) go to 9999
1110  if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntDist ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealDist,NactiReal)
        go to 9999
      else if(izpet.eq.nCmdnooft) then
        do i=1,NDimI(KPhase)
          call kus(NactiVeta,PosledniPozice,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15) nt(i,1)
        enddo
      else if(izpet.eq.nCmdtFirst.or.izpet.eq.nCmdtzero) then
        call StToReal(NactiVeta,PosledniPozice,tfirst,NDimI(KPhase),
     1                .false.,ich)
        if(ich.ne.0) go to 8000
      else if(izpet.eq.nCmdtlast) then
        call StToReal(NactiVeta,PosledniPozice,tlast,NDimI(KPhase),
     1                .false.,ich)
        if(ich.ne.0) go to 8000
      else if(izpet.eq.nCmdselect.or.izpet.eq.nCmdSelfirst) then
2100    if(iselPrvni.lt.NAtCalc) iselPrvni=iselPrvni+1
        call kus(NactiVeta,PosledniPozice,aselPrvni(iselPrvni))
        call uprat(aselPrvni(iselPrvni))
        if(PosledniPozice.ne.len(NactiVeta)) go to 2100
      else if(izpet.eq.nCmdSelsecnd) then
2200    if(iselDruhyATreti.lt.NAtCalc) iselDruhyATreti=iselDruhyATreti+1
        call kus(NactiVeta,PosledniPozice,
     1           aselDruhyATreti(iselDruhyATreti))
        call uprat(aselDruhyATreti(iselDruhyATreti))
        if(PosledniPozice.ne.len(NactiVeta)) go to 2200
      else if(izpet.eq.nCmdtorsion.or.izpet.eq.nCmdplane) then
        nr=nr+1
3000    i=idel(NactiVeta)
        write(55,FormA) NactiVeta(:i)
        if(NactiVeta(i:i).eq.'&') then
          read(m50,FormA,end=1100) NactiVeta
          go to 3000
        else
          go to 1100
        endif
      else if(izpet.eq.nCmdbondval) then
        n=nBondVal+1
        k=PosledniPozice
        call kus(NactiVeta,k,t80)
        if(index(Cifry,t80(1:1)).gt.0) then
          call StToInt(NactiVeta,PosledniPozice,ip,2,.false.,ich)
          if(ich.ne.0) go to 8000
        else
          call uprat(t80)
          ip(1)=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(ip(1).lt.0.or.ip(1).gt.NAtFormula(KPhase)) go to 8000
          call kus(NactiVeta,k,t80)
          call uprat(t80)
          ip(2)=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(ip(2).lt.0.or.ip(2).gt.NAtFormula(KPhase)) go to 8000
          PosledniPozice=k
        endif
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.ne.0) go to 8000
        dBondVal(ip(1),ip(2))=xp(1)
        dBondVal(ip(2),ip(1))=xp(1)
        iBondVal(ip(1),ip(2))=1
        iBondVal(ip(2),ip(1))=1
        nBondVal=n
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.eq.0) then
          cBondVal(ip(1),ip(2))=xp(1)
          cBondVal(ip(2),ip(1))=xp(1)
        else
          cBondVal(ip(1),ip(2))=.37
          cBondVal(ip(2),ip(1))=.37
        endif
        go to 1100
      else if(izpet.eq.nCmdDirRef) then
        if(NDirRef.ge.NDirRefMax) call ReallocDirDef(5)
        NDirRef=NDirRef+1
        call StToReal(NactiVeta,PosledniPozice,DirRef(1,NDirRef),3,
     1                .false.,ich)
        if(ich.ne.0) go to 8000
        go to 1100
      endif
      if(izpet.ne.0) go to 1100
8000  PosledniPozice=len(NactiVeta)
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      if(nr.le.0) close(55,status='delete')
      return
102   format(f15.5)
      end
      subroutine inm40(iesd)
      use Basic_mod
      use Dist_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(6),xpp(6),GammaIntInv(9)
      integer Tisk
      logical iesd
      call CopyVek(x,xdst,3*NAtCalc)
      call CopyVek(beta,betadst,6*NAtCalc)
      if(NDimI(KPhase).gt.0) then
        call TrOrthoS(0)
        if(kcommen(KPhase).gt.0) then
          if(VolaToDist) then
            Tisk=1
          else
            Tisk=0
          endif
          n=NAtCalcBasic
          do j=1,ngc(KPhase)
            do i=1,NAtCalcBasic
              if(kswa(i).ne.KPhase) cycle
              isw=iswa(i)
              n=n+1
              call qbyx(x(1,n),qcnt(1,n),isw)
              call CopyVek(xdst(1,i),xp,3)
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              call multm(rm6gc(1,j,isw,KPhase),xp,xpp,NDim(KPhase),
     1                   NDim(KPhase),1)
              call multmq(rmgc(1,j,isw,KPhase),sx(1,i),sx(1,n),3,3,1)
              do k=1,3
                xdst(k,n)=xpp(k)+s6gc(k,j,isw,KPhase)
              enddo
              call multm(rtgc(1,j,isw,KPhase),betadst(1,i),betadst(1,n),
     1                   6,6,1)
              call multmq(rtgc(1,j,isw,KPhase),sbeta(1,i),sbeta(1,n),
     1                    6,6,1)
              do k=1,KModA(2,i)
                if(KFA(2,i).ne.1.or.k.ne.KModA(2,i)) then
                  call multmq(rmgc(1,j,isw,KPhase),sux(1,k,i),sux(1,k,n)
     1                        ,3,3,1)
                  call multmq(rmgc(1,j,isw,KPhase),sux(1,k,i),sux(1,k,n)
     1                        ,3,3,1)
                else
                  call CopyVek(sux(1,k,i),sux(1,k,n),3)
                  call CopyVek(suy(1,k,i),suy(1,k,n),3)
                endif
              enddo
            enddo
          enddo
        endif
        call UnitMat(GammaIntInv,NDimI(KPhase))
        n=0
        do i=1,NComp(KPhase)
          n=max(n,ntall(i))
        enddo
        if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                               sxdk)
        allocate(oi(n),oj(n),ok(n),xdi(3,n),xdj(3,n),xdk(3,n),sxdi(3,n),
     1           sxdj(3,n),sxdk(3,n))
        if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
        allocate(um(3,n),dum(n),dam(n),dums(n),dams(n),dumm(n),dhm(4,n),
     1           dhms(4,n))
      endif
      iesd=.false.
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        dxm(i)=0.
        if(NDimI(KPhase).gt.0..and..not.NonModulated(KPhase)) then
          isw=iswa(i)
          if(TypeModFun(i).le.1.or.KModA(1,i).le.1) then
            call MakeOccMod(oi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1                      dt(1,isw),a0(i),ax(1,i),ay(1,i),KModA(1,i),
     2                      KFA(1,i),GammaIntInv)
            if(KFA(2,i).gt.0.and.KModA(2,i).gt.0)
     1        call MakeOccModSawTooth(oi,qcnt(1,i),tfirst(1,isw),
     2          nt(1,isw),dt(1,isw),uy(1,KModA(2,i),i),
     3          uy(2,KModA(2,i),i),KFA(2,i),GammaIntInv)
          else
            call MakeOccModPol(oi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ax(1,i),ay(1,i),KModA(1,i)-1,
     2        ax(KModA(1,i),i),a0(i)*.5,GammaIntInv,TypeModFun(i))
          endif
          if(TypeModFun(i).le.1) then
            call MakePosMod(xdi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ux(1,1,i),uy(1,1,i),KModA(2,i),KFA(2,i),
     2        GammaIntInv)
          else
            call MakePosModPol(xdi,qcnt(1,i),tfirst(1,isw),nt(1,isw),
     1        dt(1,isw),ux(1,1,i),uy(1,1,i),KModA(2,i),ax(1,i),
     2        a0(i)*.5,GammaIntInv,TypeModFun(i))
          endif
          do j=1,nt(1,isw)
            if(oi(j).le.ocut) cycle
            do k=1,3
              xdi(k,j)=xdi(k,j)+x(k,i)-xdst(k,i)
            enddo
            call multm(MetTens(1,isw,KPhase),xdi(1,j),xp,3,3,1)
            dxm(i)=max(dxm(i),sqrt(scalmul(xdi(1,j),xp)))
          enddo
        endif
        if(.not.iesd) then
          do j=1,3
            if(sx(j,i).gt.0.) then
              iesd=.true.
              exit
            endif
          enddo
        endif
      enddo
9999  return
      end
      subroutine DistAng(u,ug,v,vg,uu,vv,sgx,MetTensP,angle,su)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension u(3),ug(3),v(3),vg(3),sgx(9),ag(3,3),ddx(9)
      real MetTensP(9)
      angle=0.
      su=0.
      uv=scalmul(u,vg)
      denq=uu*vv
      den=sqrt(denq)
      if(den.lt..0001) go to 9999
      arg=uv/den
      if(abs(arg).gt..999999) then
        angle=90.-sign(90.,arg)
        su=.5
        go to 9999
c      else if(abs(arg).lt..0001) then
c       angle=90.
c       go to 9999
c       su=.2
c        go to 9999
      endif
      angle=acos(arg)/torad
      denp=1./(sqrt(1.-arg**2)*torad)
      pom=denp/den
      do l=1,3
        dtul=pom*(vg(l)-ug(l)*uv/uu)
        dtvl=pom*(ug(l)-vg(l)*uv/vv)
        ddx(l)=-dtul-dtvl
        ddx(l+3)=dtul
        ddx(l+6)=dtvl
      enddo
      su=sigma(sgx,ddx,9)
      do i=1,3
        do j=1,3
          ag(i,j)=1./denq*(u(i)*v(j)
     1                     -.5*uv/denq*(u(i)*u(j)*vv+v(i)*v(j)*uu))**2
        enddo
      enddo
      gu=0.
      m=0
      do i=1,3
        do j=1,3
          m=m+1
          gu=gu+ag(i,j)*MetTensP(m)
        enddo
      enddo
      gu=denp*sqrt(gu)
      su=sqrt(su**2+gu**2)
9999  return
      end
      subroutine inclat
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xdsto(:,:),dxmo(:),BetaDstO(:,:)
      character*80 t80
      logical BratPrvniO(:),BratDruhyATretiO(:)
      allocatable xdsto,BratPrvniO,BratDruhyATretiO,dxmo,BetaDstO
      Nxdst=ubound(xdst,2)
      if(incl.eq.0) go to 9999
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      inclp=0
1000  if(inclp.ge.11) go to 1100
      read(m40,FormA,end=5000) t80
      if(t80(1:10).ne.'----------'.or.
     1   (LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0
     2     .and.
     3    LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0))
     4  go to 1000
      read(m40,'(i5)',end=5000) isw
      read(m40,FormA,end=5000) t80
      call mala(t80)
      if(index(t80,'max').gt.0.and.mod(inclp,10).eq.0) then
        inclp=inclp+1
        go to 1000
      else if(index(t80,'min').gt.0.and.inclp/10.eq.0) then
        inclp=inclp+10
        go to 1000
      endif
1100  if(inclp.eq.1) then
        incl=min(incl,1)
      else if(inclp.eq.10) then
        if(incl.eq.1) incl=0
      endif
      if(incl.eq.0) then
        close(m40)
        go to 9999
      endif
      if(incl.gt.0) then
        call newln(1)
        if(incl.eq.1) then
          write(lst,'(''Positive peaks from the previous Fourier '',
     1                ''calculation will be included'')')
        else if(incl.eq.2) then
          write(lst,'(''Negative peaks from the previous Fourier '',
     1                ''calculation will be included'')')
        else
          write(lst,'(''Both positive and negative peaks from the '',
     1                ''previous Fourier calculation will be included'')
     2                 ')
        endif
      else
        go to 9999
      endif
      l1=1
      l2=1
      if(incl.ge.2) then
        l2=2
        if(incl.eq.2) l1=2
      endif
      do 1500l=l1,l2
        rewind m40
        rewind m40
1300    read(m40,FormA,end=5000) t80
        if(t80(1:10).ne.'----------') go to 1300
        if(l.eq.1) then
          if(LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0)
     1      go to 1300
        else
          if(LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0)
     1      go to 1300
        endif
        read(m40,'(2i5)',end=1500) isw,KPh
        KPh=max(KPh,1)
        if(KPh.ne.KPhase) go to 1300
1400    if(NAtCalc.ge.MxAtAll) then
          NAtAll=max(NAtCalc,NAtAll)
          call ReallocateAtoms(NAtCalc)
        endif
        if(NAtCalc.ge.Nxdst) then
          allocate(xdsto(3,NAtCalc),BratPrvniO(NAtCalc),
     1             BratDruhyATretiO(NAtCalc),dxmo(NAtCalc),
     2             BetaDstO(6,NAtCalc))
          do i=1,NAtCalc
            BratPrvniO(i)=BratPrvni(i)
            BratDruhyATretiO(i)=BratDruhyATreti(i)
            dxmo(i)=dxm(i)
            call CopyVek(xdst(1,i),xdsto(1,i),3)
            call CopyVek(BetaDst(1,i),BetaDstO(1,i),6)
          enddo
          deallocate(xdst,BratPrvni,BratDruhyATreti,dxm,BetaDst)
          Nxdst=Nxdst+NAtCalc
          allocate(xdst(3,Nxdst),BetaDst(6,Nxdst),dxm(Nxdst),
     1             BratPrvni(Nxdst),BratDruhyATreti(Nxdst))
          do i=1,NAtCalc
            BratPrvni(i)=BratPrvniO(i)
            BratDruhyATreti(i)=BratDruhyATretiO(i)
            dxm(i)=dxmo(i)
            call CopyVek(xdsto(1,i),xdst(1,i),3)
            call CopyVek(BetaDstO(1,i),BetaDst(1,i),6)
          enddo
          deallocate(xdstO,BratPrvniO,BratDruhyATretiO,dxmO,BetaDstO)
        endif
        NAtCalc=NAtCalc+1
        read(m40,FormA,end=1460) t80
        if(index(t80,'--------').gt.0) go to 1460
        call SetBasicKeysForAtom(NAtCalc)
        read(t80,'(a8,2i3,4x,4f9.6,6x,3i1,3i3)')
     1    atom(NAtCalc),isf(NAtCalc),itf(NAtCalc),ai(NAtCalc),
     2    (xdst(k,NAtCalc),k=1,3),j,j,j,j,KModA(2,NAtCalc)
        read(m40,FormA,end=1460) t80
        if(index(t80,'---------').gt.0) go to 1460
        read(t80,101) pom
        iswa(NAtCalc)=isw
        kswa(NAtCalc)=KPh
        call CopyVek(xdst(1,NAtCalc),x(1,NAtCalc),3)
        if(MaxNDimI.gt.0) call qbyx(x(1,NAtCalc),qcnt(1,NAtCalc),isw)
        call SetRealArrayTo(beta(1,NAtCalc),6,0.)
        call SetRealArrayTo(BetaDst(1,NAtCalc),6,0.)
        BratPrvni(NAtCalc)=.true.
        BratDruhyATreti(NAtCalc)=.true.
        dxm(NAtCalc)=0.
        if(KModA(2,NAtCalc).ne.0) then
          do j=1,KModA(2,NAtCalc)
            read(m40,FormA,end=1460) t80
            if(index(t80,'--------').gt.0) go to 1460
            read(t80,101)(ux(k,j,NAtCalc),k=1,3),(uy(k,j,NAtCalc),k=1,3)
          enddo
          read(m40,FormA,end=1460) t80
          if(index(t80,'---------').gt.0) go to 1460
          read(t80,101) pom
        endif
        go to 1400
1460    NAtCalc=NAtCalc-1
1500  continue
5000  call CloseIfOpened(m40)
      call specat
9999  return
101   format(6f9.6)
      end
      subroutine deleq
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      logical IdenticalAtoms
      call newln(1)
      write(lst,FormA)
      do j=1,NAtCalc
        BratDruhyATreti(j)=BratDruhyATreti(j).and.ai(j).gt.0.
        BratPrvni(j)=BratPrvni(j).and.ai(j).gt.0.
      enddo
      if(RejectIdentical.eq.1) then
        do i=1,NAtCalc-1
          do j=i+1,NAtCalc
            if(IdenticalAtoms(i,j,.001).and.BratPrvni(i)) then
              call newln(1)
              write(lst,'(''The atom '',a8,'' deleted from the '',
     1                    ''distance calculation as coincides with : '',
     2                    a8)') atom(j),atom(i)
              BratPrvni(j)=.false.
              BratDruhyATreti(j)=.false.
            endif
          enddo
        enddo
      endif
      return
      end
      subroutine pisat
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(3),xs(3)
      logical eqwild,lpomPrvni,lpomDruhyATreti,Zprava
      character*80 FormOut,Veta
      character*8 at
      character*1 znak1,znak2
      data
     1  FormOut/'(i3,2x,a8,a1,1x,a1,6f10.6,3f9.4,f9.6,''('',i5,'')'')'/
      lpomPrvni=iselPrvni.le.0
      lpomDruhyATreti=iselDruhyATreti.le.0
      do j=1,NAtCalc
        BratPrvni(j)=lpomPrvni
        BratDruhyATreti(j)=lpomDruhyATreti
      enddo
      call inclat
      Zprava=.false.
      if(iselPrvni.gt.0) then
        fullcoo=1
        do i=1,iselPrvni
          if(index(aselPrvni(i),'*').ne.0) then
            do j=1,NAtCalc
              lpomPrvni=eqwild(atom(j),aselPrvni(i),.false.)
              BratPrvni(j)=(BratPrvni(j).or.lpomPrvni).and.
     1                      kswa(j).eq.KPhase
            enddo
          else
            j=ktat(atom,NAtCalc,aselPrvni(i))
            if(j.le.0) then
              at=aselPrvni(i)
              k=min(idel(at),7)
              at=at(:k)//''''
              j=ktat(atom,NAtCalc,at)
            endif
            if(j.gt.0) then
              lpomPrvni=j.gt.0
              BratPrvni(j)=(BratPrvni(j).or.lpomPrvni).and.
     1                      kswa(j).eq.KPhase
            else if(.not.Zprava) then
              Veta='Atom "'//aselPrvni(i)(:idel(aselPrvni(i)))//
     1             '" selected for calculation not present in the '//
     2             'structure'
              call FeChybne(-1.,-1.,Veta,'It may exist more spurious '//
     1          'atoms but only first one is announced.',Warning)
              Zprava=.true.
            endif
          endif
        enddo
      endif
      if(iselDruhyATreti.gt.0) then
        do i=1,iselDruhyATreti
          if(index(aselDruhyATreti(i),'*').ne.0) then
            do j=1,NAtCalc
              lpomDruhyATreti=eqwild(atom(j),aselDruhyATreti(i),.false.)
              BratDruhyATreti(j)=
     1          (BratDruhyATreti(j).or.lpomDruhyATreti).and.
     2           kswa(j).eq.KPhase
            enddo
          else
            j=ktat(atom,NAtCalc,aselDruhyATreti(i))
            if(j.le.0) then
              at=aselDruhyATreti(i)
              k=min(idel(at),7)
              at=at(:k)//''''
              j=ktat(atom,NAtCalc,at)
            endif
            if(j.gt.0) then
              lpomDruhyATreti=j.gt.0
              BratDruhyATreti(j)=
     1          (BratDruhyATreti(j).or.lpomDruhyATreti).and.
     2           kswa(j).eq.KPhase
            else if(.not.Zprava) then
              Veta='Atom "'//
     1             aselDruhyATreti(i)(:idel(aselDruhyATreti(i)))//
     2             '" selected for calculation not present in the '//
     3             'structure'
              call FeChybne(-1.,-1.,Veta,'It may exist more spurious '//
     1          'atoms but only first one is announced.',Warning)
              Zprava=.true.
            endif
          endif
        enddo
      endif
      if(ngc(KPhase).gt.0) then
        n=NAtCalcBasic
        do j=1,ngc(KPhase)
          do i=1,NAtCalcBasic
            if(kswa(i).ne.KPhase) cycle
            n=n+1
            BratPrvni(n)=BratPrvni(i)
            BratDruhyATreti(n)=BratDruhyATreti(i)
          enddo
        enddo
      endif
      call deleq
      KPhase=KPhaseBasic
      call newln(3)
      write(lst,'(/38x,''fractional'',36x,''cartesian'')')
      if(lite(KPhase).eq.1) then
        write(lst,'('' no'',2x,''atom'',13x,''xf'',8x,''yf'',8x,''zf'',
     1              5x,''sigxf'',5x,''sigyf'',5x,''sigzf'',8x,''x'',8x,
     2              ''y'',8x,''z'',6x,''Biso'')')
        FormOut(34:36)='6.2'
        fact=100.
      else
        write(lst,'('' no'',2x,''atom'',13x,''xf'',8x,''yf'',8x,''zf'',
     1              5x,''sigxf'',5x,''sigyf'',5x,''sigzf'',8x,''x'',8x,
     2              ''y'',8x,''z'',6x,''Uiso'')')
        FormOut(34:36)='9.6'
        fact=1000000.
      endif
      do i=1,NAtCalc
        m=isf(i)
        if(m.eq.0.or.kswa(i).ne.KPhase) cycle
        do j=1,3
          call round(xdst(j,i),sx(j,i),irnd)
        enddo
        if(itf(i).eq.1) then
          if(lite(KPhase).eq.0) then
            bizo=beta(1,i)/episq
            pom=sbeta(1,i)/episq
          else
            bizo=beta(1,i)
            pom=sbeta(1,i)
          endif
        else
          call boueq(betadst(1,i),sbeta(1,i),lite(KPhase),bizo,pom,1)
        endif
        call MultM(TrToOrtho(1,1,KPhase),xdst(1,i),xp,3,3,1)
        if(BratPrvni(i)) then
          znak1='+'
        else
          znak1='-'
        endif
        if(BratDruhyATreti(i)) then
          znak2='+'
        else
          znak2='-'
        endif
        call newln(1)
        do j=1,3
          xs(j)=max(sx(j,i),0.)
        enddo
        write(lst,FormOut) i,atom(i),znak1,znak2,(xdst(j,i),j=1,3),
     1                     xs,xp,bizo,nint(pom*fact)
      enddo
      return
      end
      subroutine DistAt
      use Atoms_mod
      use Basic_mod
      use EditM40_mod
      use Dist_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      real :: BI(6),BJ(6),sBI(6),sBJ(6),TrMat(36),rmpt(36),tlp(6),
     1        tpisq=19.7392088
      dimension y0(3),y1(3),y2(3),uz(3),shj(3),umg(3),sx0(6),shic(6),
     1          shjc(3),ish(3),jsh(3),
     2          uxj(3,mxw),uyj(3,mxw),y0p(6),y1p(3),y2p(3),uzp(3),up(3),
     3          MxCell(3),MxDCell(3),y340(3),yp(6),ypp(6),
     4          idtab(30),iptab(30),nutab(30),utab(30,30),tztnat(3),
     5          dttnat(3),GammaIntInv(9),tsp(3),xp(6),t(3),tj(3),nd(3),
     6          suxj(3,mxw),suyj(3,mxw),sgxm(6),dstab(30),sutab(30,30),
     7          dmod(6),y3(6),y3p(6),smp(36),rmp(9),rm6p(36),s6p(6),
     8          vt6p(:,:),ddg(3),JCell(3)
      character*128 ltab(500),Veta
      character*80 t80,s80
      character*15 t15
      character*10 t101,t102,t103
      character*8 at1,at2,at3,attab(30),atutab(30,30),sp8
      character*2 nty,sp2,Label,Lab2,Lab3
      allocatable vt6p
      logical iuhl0,Prvne,Konec,Nacitame,JeToUhel,eqrv,EqRvLT0
      equivalence (dmod(1),dumav),(dmod(2),sdumav),
     1            (dmod(3),dummn),(dmod(4),sdummn),
     2            (dmod(5),dummx),(dmod(6),sdummx)
      data sp2,sp8/2*' '/
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)),
     1         TypicalDistUse(NAtFormula(KPhase),NAtFormula(KPhase)))
      if(VolaToDist.and..not.CalcHBonds) then
        lnh=NextLogicNumber()
        call OpenFile(lnh,fln(:ifln)//'.l66','formatted','unknown')
      else
        lnh=0
      endif
      iswlast=0
      if(CalcHBonds) then
        if(VolaToDist) then
          if(isfh(KPhase).le.0) go to 9999
          call newpg(0)
          call TitulekVRamecku('List of possible hydrogen bonds')
          call newln(3)
          write(lst,'(/''Maximal donor-hydrogen bond :'',f6.3,5x,
     1                 ''Maximal hydrogen-acceptor bond:'',f6.3/)')
     2         DHMax,HAMax
          iuhl0=.false.
          LstType=99
        endif
      else
        call SetRealArrayTo(dmd,NAtFormula(KPhase),dmdk)
        if(dmhk.ge.0.) then
          call SetRealArrayTo(dmh,NAtFormula(KPhase),dmhk*.5)
        else
          call CopyVek(AtRadius(1,KPhase),dmh,NAtFormula(KPhase))
        endif
        pom=0.
        do i=1,NAtFormula(KPhase)
          if(2.*dmh(i).lt.dmd(i)) dmh(i)=0.
          pom=pom+dmh(i)
        enddo
        iuhl0=iuhl.eq.0
        if(dmhk.ge.0.) then
          call SetRealArrayTo(TypicalDistUse,NAtFormula(KPhase)**2,
     1                        dmhk)
        else
          do i=1,NAtFormula(KPhase)
            do j=i,NAtFormula(KPhase)
              if(TypicalDist(i,j,KPhase).gt.0.) then
                TypicalDistUse(i,j)=TypicalDist(i,j,KPhase)
              else
                TypicalDistUse(i,j)=AtRadius(i,KPhase)+
     1                              AtRadius(j,KPhase)
              endif
              TypicalDistUse(j,i)=TypicalDistUse(i,j)
            enddo
          enddo
        endif
        if(VolaToDist) then
          t80='List of distances'
          if(.not.iuhl0) t80=t80(:idel(t80))//' and angles'
          call NewPg(0)
          call TitulekVRamecku(t80)
          if(pom.le.0.) then
            call newln(2)
            write(lst,'(/''Distances and angles will not be '',
     1                   ''calculated'')')
            go to 9999
          endif
          if(LstType.eq.0) call SetStringArrayTo(iven,mxl4,' ')
          call NewLn(3)
          if(fullcoo.eq.1) then
            write(lst,'(/''Full coordination of atoms will be printed'')
     1                  ')
          else
            write(lst,'(/''Reduced coordination of atoms will be '',
     1                   ''printed'')')
          endif
          if(LstType.eq.0) then
            write(lst,'(''The results will be printed in columns '',
     1                  ''without symmetry codes'')')
          else if(LstType.eq.1) then
            write(lst,'(''The results will be printed in one column '',
     1                  ''with symmetry codes'')')
          endif
          if(iuhl0) then
            Veta='Distances will be calculated'
          else
            Veta='Distances and angles will be calculated'
          endif
          Veta=Veta(:idel(Veta))//' from d(min)='
          write(Veta(idel(Veta)+1:),105) dmdk
          if(dmhk.ge.0.) then
            Veta=Veta(:idel(Veta))//' to d(max)='
            write(Veta(idel(Veta)+1:),105) dmhk
          else
            Veta=Veta(:idel(Veta))//' to specific maximal distances '//
     1           'as listed below'
            if(ExpDMax.gt.0.) then
              write(Cislo,'(f5.2)') ExpDMax
              call ZdrcniCisla(Cislo,1)
              Veta=Veta(:idel(Veta))//' expanded by '//
     1             Cislo(:idel(Cislo))//'%'
            endif
            Veta=Veta(:idel(Veta))//':'
          endif
          call newln(3)
          write(lst,FormA)
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          if(dmhk.lt.0.) then
            do i=1,NAtFormula(KPhase)
              idl=1
              do j=i,NAtFormula(KPhase)
                write(t80,'(a2,''-'',a2)') AtType(i,KPhase),
     1                                     AtType(j,KPhase)
                call Zhusti(t80)
                if(TypicalDist(i,j,KPhase).gt.0.) then
                  Label=' T'
                else
                  Label=' R'
                endif
                write(Cislo,'(f6.3,a2)') TypicalDistUse(i,j),Label
                Veta(idl:)=t80(:5)//Cislo
                idl=idl+18
                if(idl.gt.120) then
                  call NewLn(1)
                  write(lst,FormA) Veta(:idel(Veta))
                  idl=1
                endif
              enddo
              if(idl.gt.1) then
                call NewLn(1)
                write(lst,FormA) Veta(:idel(Veta))
              endif
            enddo
            call NewLn(4)
            write(lst,'(/''T ... typical distances taken either from '',
     1                   ''the file distributed with Jana program or '',
     2                   ''defined by user'')')
            write(lst,'( ''R ... distance as derived from atomic '',
     1                   ''radii:''/)')
            call newln(NAtFormula(KPhase)+2)
            write(lst,'(45x,''Atom type    Atom radius'')')
            do i=1,NAtFormula(KPhase)
              write(lst,'(47x,a2,10x,f8.3)')
     1          AtType(i,KPhase),AtRadius(i,KPhase)
            enddo
            write(lst,FormA)
          endif
        else

        endif
      endif
      if(NDimI(KPhase).gt.0.and.nt(1,1).gt.0.and.VolaToDist) then
        call NewLn(NDimI(KPhase)+2)
        Veta='The calculation will run'
        idl=idel(Veta)+2
        do i=1,NDimI(KPhase)
          write(t80,'(''<'',f9.3,'','',f9.3,''>'')')
     1      tfirst(i,1),tlast(i,1)
          call zhusti(t80)
          write(Veta(idl:),'(''for '',a1,'' from the interval '',a,
     1                '' at '',i3,'' equidistant steps'')')
     2      smbt(i),t80(:idel(t80)),nt(i,1)
          write(lst,FormA) Veta(:idel(Veta))
          idl=1
        enddo
        if((LstType.eq.0.or.LstType.eq.99).and.ieach.ne.0) then
          if(ieach.eq.1) then
            write(lst,'(''Each step will be printed''/)')
          else
            write(lst,'(''Each'',i3,a2,'' step will be printed''/)')
     1        ieach,nty(ieach)
          endif
        else
          write(lst,'(''Print of steps supressed''/)')
        endif
      endif
      if(VolaToDist) then
        if(line.gt.40) then
          call newpg(0)
          if(LstType.eq.0) mxl=mxl4
        else
          if(LstType.eq.0) mxl=(mxline-line)*4
        endif
        if(LstType.eq.0) line=0
      endif
      if(.not.CalcHBonds) then
        call OpenFile(78,fln(:ifln)//'.l61','unformatted','unknown')
        if(ErrFlag.ne.0) go to 9999
      endif
      NaCelkem=0
      if(VolaToDist) then
        do i=1,NAtCalc
          if(.not.BratPrvni(i).or.isf(i).le.0.or.kswa(i).ne.KPhase.or.
     1       (CalcHBonds.and.isf(i).ne.isfh(KPhase))) cycle
          if(fullcoo.eq.1) then
            jp=1
          else
            jp=i
          endif
          do j=jp,NAtCalc
            if(.not.BratDruhyATreti(j).or.isf(j).le.0.or.
     1         kswa(j).ne.KPhase.or.
     2       (CalcHBonds.and.isf(j).eq.isfh(KPhase))) cycle
            NaCelkem=NaCelkem+1
          enddo
        enddo
        if(NaCelkem.gt.1) then
          if(CalcHBonds) then
            t80='Search for hydrogen bonds'
          else
            t80='Distance calculation'
          endif
          call FeFlowChartOpen(-1.,-1.,1,NaCelkem,t80,' ',' ')
        else
          if(NaCelkem.le.0) go to 9999
        endif
      endif
      nfloat=0
      if(CalcHBonds) then
        IHBonds=0
      else
        n61=0
      endif
      nmo=0
      iswo=0
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or..not.BratPrvni(i).or.isf(i).le.0.or.
     1     (CalcHBonds.and.isf(i).ne.isfh(KPhase))) cycle
        if(CalcHBonds) then
          dmdi=0.
        else
          dmdi=dmd(isf(i))
c          dmhi=dmh(isf(i))
        endif
        iswi=iswa(i)
        if(VolaToDist) then
          t80='x,y,z#'
          SymmCIF(1)='.'
          SymmLabel(1)=' '
          if(itf(i).eq.2) then
            call CopyVek( Beta(1,i), BI,6)
            call CopyVek(sBeta(1,i),sBI,6)
            do j=1,6
               BI(j)= BI(j)/tpisq
              sBI(j)=sBI(j)/tpisq
            enddo
          endif
        else
          if(PrvniSymmString.ne.' ') then
            NTrans=1
            if(allocated(TransMat)) call DeallocateTrans
            if(NAtCalc.ge.MxAtAll) call ReallocateAtoms(5)
            call AllocateTrans
            call AtCopy(i,NAtCalc+1)
            call CopyVek(xdst(1,i),xdst(1,NAtCalc+1),3)
            call AtomSymCode(PrvniSymmString,ia,isym,icenter,shic,ish,
     1                       ich)
            iswi=ISwSymm(isym,iswa(i),KPhase)
            if(iswi.eq.iswa(i)) then
              call CopyMat(rm (1,isym,iswi,KPhase),rmp ,3)
              call CopyMat(rm6(1,isym,iswi,KPhase),rm6p,NDim(KPhase))
              call CopyVek(shic,s6p,NDim(KPhase))
            else
              call multm(zv(1,iswi,KPhase),zvi(1,iswa(i),KPhase),smp,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call multm(smp,rm6(1,isym,iswa(i),KPhase),rm6p,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call MatBlock3(rm6p,rmp,NDim(KPhase))
              call multm(smp,shic,s6p,NDim(KPhase),NDim(KPhase),1)
            endif
            call CopyMat(rm6p,TransMat(1,1,iswi),NDim(KPhase))
            call CopyVek(shic,TransVec(1,1,iswi),NDim(KPhase))
            call Multm(rmp,xdst(1,i),x1p,3,3,1)
            call AddVek(x1p,shic,xdst(1,i),3)
            call SCode(isym,icenter,shic,ish,iswi,t80,SymmLabel(1))
            call EM40SetTr(0)
            call EM40TransAtFromTo(i,i,ich)
            call qbyx(x(1,i),qcnt(1,i),iswi)
            call ScodeCIF(isym,icenter,ish,iswi,SymmCIF(1))
          else
            t80='x,y,z#'
            SymmCIF(1)='.'
          endif
        endif
        call SetRealArrayTo(xp,3,0.)
        k=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,i).ne.0) then
            xp(1)=ax(KModA(1,i),i)
            k=1
          else if(KFA(2,i).ne.0) then
            xp(1)=uy(1,KModA(2,i),i)
            k=1
          endif
        endif
        call SpecPos(x(1,i),xp,k,iswi,.01,n)
        aii=ai(i)*float(n)
        ah=rcp(1,iswi,KPhase)
        bh=rcp(2,iswi,KPhase)
        ch=rcp(3,iswi,KPhase)
        call CopyVek(tfirst(1,iswi),tfirsti,NDimI(KPhase))
        call CopyVek (dt(1,iswi),dti,NDimI(KPhase))
        call CopyVekI(nt(1,iswi),nti,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          TLastI(j)=TFirstI(j)+dti(j)*float(nti(j)-1)
        enddo
        call bunka(xdst(1,i),x1,0)
        call CopyVek(x(1,i),x1p,3)
        call qbyx(x1,x1(4),iswi)
        if(.not.CalcHBonds) then
          write(78) '#1',atom(i),isf(i),iswi,aii,(0.,m=1,6),t80,
     1               SymmCIF(1),1
          n61=n61+1
          if(VolaToDist) then
            if(LstType.eq.0) then
              call fflush
              line=line+1
              write(iven(line),102)
              call fflush
              line=line+1
              write(iven(line),103) atom(i)
              call fflush
              line=line+1
              write(iven(line),102)
              call fflush
            else if(LstType.eq.1) then
              call newln(2)
              if(iuhl0) then
                write(lst,'(''Distances concerning atom '',a8/
     1                      34(''=''))') atom(i)
              else
                write(lst,'(''Distances and angles concerning atom '',
     1                      a8/45(''=''))') atom(i)
              endif
            endif
          endif
        endif
        call CopyVek(sx(1,i),sgx(1),3)
        if(ntall(iswi).gt.0.and.NDimI(KPhase).gt.0) then
          call UnitMat(GammaIntInv,NDimI(KPhase))
          if(NonModulated(KPhase)) then
            call SetRealArrayTo(oi,ntall(iswi),1.)
            call SetRealArrayTo(xdi,3*ntall(iswi),0.)
            call SetRealArrayTo(sxdi,3*ntall(iswi),0.)
          else
            if(TypeModFun(i).le.1.or.KModA(1,i).le.1) then
              call MakeOccMod(oi,qcnt(1,i),tfirsti,nti,dti,
     1                        a0(i),ax(1,i),ay(1,i),KModA(1,i),
     2                        KFA(1,i),GammaIntInv)
              if(KFA(2,i).gt.0.and.KModA(2,i).gt.0)
     1          call MakeOccModSawTooth(oi,qcnt(1,i),tfirsti,nti,dti,
     2            uy(1,KModA(2,i),i),uy(2,KModA(2,i),i),KFA(2,i),
     3            GammaIntInv)
            else
              call MakeOccModPol(oi,qcnt(1,i),tfirsti,nti,dti,
     1          ax(1,i),ay(1,i),KModA(1,i)-1,
     2          ax(KModA(1,i),i),a0(i)*.5,GammaIntInv,TypeModFun(i))
            endif
            if(TypeModFun(i).le.1) then
              call MakePosMod(xdi,qcnt(1,i),tfirsti,nti,dti,ux(1,1,i),
     1          uy(1,1,i),KModA(2,i),KFA(2,i),GammaIntInv)
            else
              call MakePosModPol(xdi,qcnt(1,i),tfirsti,nti,dti,
     1          ux(1,1,i),uy(1,1,i),KModA(2,i),ax(1,i),a0(i)*.5,
     2          GammaIntInv,TypeModFun(i))
            endif
            call MakePosModS(sxdi,qcnt(1,i),tfirsti,nti,dti,ux(1,1,i),
     1                       uy(1,1,i),sux(1,1,i),suy(1,1,i),KModA(2,i),
     2                       KFA(2,i),GammaIntInv)
          endif
        endif
        if(fullcoo.eq.1.or.CalcHBonds) then
          jp=1
        else
          jp=i
        endif
        if(.not.VolaToDist) then
          if(PrvniSymmString.ne.' ') then
            call AtCopy(NAtCalc+1,i)
            call CopyVek(xdst(1,NAtCalc+1),xdst(1,i),3)
            call qbyx(x(1,i),qcnt(1,i),iswi)
          endif
        endif
        do j=jp,NAtCalc
          if(.not.BratDruhyATreti(j).or.kswa(j).ne.KPhase.or.
     1       isf(j).eq.0.or.(CalcHBonds.and.isf(j).eq.isfh(KPhase)))
     2      cycle
          if(SkipSplit.gt.0) then
            if(kmol(i).ne.kmol(j)) then
              do ii=1,NMolSplit
                k=0
                do jj=1,MMolSplit(ii)
                  if(IMolSplit(jj,ii).eq.kmol(i)) then
                    k=k+1
                  else if(IMolSplit(jj,ii).eq.kmol(j)) then
                    k=k+1
                  endif
                enddo
                if(k.ge.2) go to 4000
              enddo
            endif
            do ii=1,NAtSplit
              k=0
              do jj=1,MAtSplit(ii)
                if(IAtSplit(jj,ii).eq.i) then
                  k=k+1
                else if(IAtSplit(jj,ii).eq.j) then
                  k=k+1
                endif
              enddo
              if(k.ge.2) go to 4000
            enddo
          endif
          call SetRealArrayTo(xp,3,0.)
          k=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,j).ne.0) then
              xp(1)=ax(KModA(1,j),j)
              k=1
            else if(KFA(2,j).ne.0) then
              xp(1)=uy(1,KModA(2,j),j)
              k=1
            endif
          endif
          call SpecPos(x(1,j),xp,k,iswa(j),.01,n)
          aij=ai(j)*float(n)
          if(VolaToDist.and.NaCelkem.gt.1) then
            call FeFlowChartEvent(nfloat,ie)
            if(ie.ne.0) then
              call DistBudeBreak
              if(ErrFlag.ne.0) go to 9900
            endif
          endif
          if(CalcHBonds) then
            dmhij =DHMax+dxm(i)+dxm(j)
            dmhijm=DHMax
          else
            if(dmhk.ge.0) then
              pom=1.
            else
              pom=1.+.01*ExpDMax
            endif
            dmhijm=TypicalDistUse(isf(i),isf(j))*pom
            dmhij =dmhijm+dxm(i)+dxm(j)
          endif
          dmhija=dmhij*ah
          dmhijb=dmhij*bh
          dmhijc=dmhij*ch
          dmhijq=dmhij**2
          dmdijq=dmdi**2
          do l=1,3
            MxCell(l)=dmhij*rcp(l,iswa(j),KPhase)+1
          enddo
          MxDCell(3)=1
          do l=2,1,-1
            MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
          enddo
          MxNCell=MxDCell(1)*(2*MxCell(1)+1)
          do l=1,3
            y0(l)=xdst(l,j)
            y0p(l)=x(l,j)
            sx0(l)=sx(l,j)
          enddo
          js=0
          jst=0
          do jsym=1,NSymmN(KPhase)
            js=js+1
            if(isa(js,j).le.0) cycle
            iswj=ISwSymm(jsym,iswa(j),KPhase)
            iswjin=iswi*10+iswj
            if(iswj.ne.iswi) then
              if(iswjin.ne.iswlast) call DistSetComp(iswi,iswj)
            else
              call UnitMat(TNaT ,NDimI(KPhase))
              call UnitMat(TNaTi,NDimI(KPhase))
            endif
            iswlast=iswjin
            call multm(TNaTi,tfirsti,TzTNaT,NDimI(KPhase),NDimI(KPhase),
     1                 1)
            call multm(TNaTi,dti,DtTNaT,NDimI(KPhase),NDimI(KPhase),1)
            if(iswj.eq.iswa(j)) then
              call CopyMat(rm (1,jsym,iswj,KPhase),rmp ,3)
              call CopyMat(rm6(1,jsym,iswj,KPhase),rm6p,NDim(KPhase))
              call CopyVek( s6(1,jsym,iswj,KPhase), s6p,NDim(KPhase))
              do l=1,NLattVec(KPhase)
                call CopyVek(vt6(1,l,iswj,KPhase),vt6p(1,l),
     1                       NDim(KPhase))
              enddo
            else
              call multm(zv(1,iswj,KPhase),zvi(1,iswa(j),KPhase),smp,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call multm(smp,rm6(1,jsym,iswa(j),KPhase),rm6p,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call MatBlock3(rm6p,rmp,NDim(KPhase))
              call multm(smp,s6(1,jsym,iswa(j),KPhase),s6p,NDim(KPhase),
     1                   NDim(KPhase),1)
              do l=1,NLattVec(KPhase)
                call multm(smp,vt6(1,l,iswa(j),KPhase),vt6p(1,l),
     1                     NDim(KPhase),NDim(KPhase),1)
              enddo
            endif
            call multm(rmp,y0,y1,3,3,1)
            call multm(rmp,y0p,y1p,3,3,1)
            if(itf(j).eq.2) then
              call srotb(rmp,rmp,rmpt)
              call MultM(rmpt,  Beta(1,j), BJ,6,6,1)
              call MultMQ(rmpt,sBeta(1,j),sBJ,6,6,1)
              do l=1,6
                 BJ(l)= BJ(l)/tpisq
                sBJ(l)=sBJ(l)/tpisq
              enddo
            endif
            if(EqRvLT0(sx0,3)) then
              call SetRealArrayTo(sgx(4),3,-.1)
            else
              call multmq(rmp,sx0,sgx(4),3,3,1)
            endif
            call GetGammaIntInv(rm6p,GammaIntInv)
            do l=1,3
              y1(l)=y1(l)+s6p(l)
              y1p(l)=y1p(l)+s6p(l)
            enddo
            do 3100jcenter=1,NLattVec(KPhase)
              do l=1,jcenter-1
                if(eqrv(vt6p(1,jcenter),vt6p(1,l),3,.001)) go to 3100
              enddo
              do l=4,NDim(KPhase)
                tsp(l-3)=-s6p(l)
     1                   -vt6p(l,jcenter)
              enddo
              do l=1,3
                y2(l)=y1(l)+vt6p(l,jcenter)
                y2p(l)=y1p(l)+vt6p(l,jcenter)
              enddo
              if(iswj.eq.iswi) then
                call bunka(y2,shj,1)
                do l=1,3
                  uzp(l)=y2p(l)-x1p(l)+shj(l)
                  uz(l)=y2(l)-x1(l)
                enddo
                if(NComp(KPhase).gt.1) then
                  do l=1,3
                    MxCell(l)=dmhij*rcp(l,iswa(j),KPhase)+1
                  enddo
                  MxDCell(3)=1
                  do l=2,1,-1
                    MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                  enddo
                  MxNCell=MxDCell(1)*(2*MxCell(1)+1)
                endif
              else
                call multm(WPri,x1,yp,NDim(KPhase),NDim(KPhase),1)
                do l=1,3
                  shj(l)=yp(l)-y2(l)
                enddo
                do l=1,3
                  shj(l)=ifix(shj(l))
                  y2(l)=y2(l)+shj(l)
                  y2p(l)=y2p(l)+shj(l)
                enddo
                do l=1,NDim(KPhase)
                  xp(l)=x1(l)
                  if(l.gt.3) xp(l)=xp(l)+tfirsti(l-3)
                enddo
                do l=1,3
                  pom=abs(yp(l))
                  if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
                enddo
                do l=4,NDim(KPhase)
                  xp(l)=xp(l)+dti(l-3)*float(nti(l-3)-1)
                enddo
                call multm(WPri,xp,yp,NDim(KPhase),NDim(KPhase),1)
                do l=1,3
                  pom=abs(yp(l))
                  if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
                enddo
                MxDCell(3)=1
                do l=2,1,-1
                  MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                enddo
                MxNCell=MxDCell(1)*(2*MxCell(1)+1)
              endif
              do JCellI=0,MxNCell-1
                call UnPackHKL(JCellI,JCell,MxCell,MxDCell,3)
                do l=1,3
                  pom=JCell(l)
                  if(iswj.eq.iswi) then
                    u(l)=uz(l)+pom
                    up(l)=uzp(l)+pom
                  endif
                  y3(l)=y2(l)+pom
                  y3p(l)=y2p(l)+pom
                  jsh(l)=nint(shj(l))+pom
                  shjc(l)=shj(l)+pom+s6p(l)+vt6p(l,jcenter)
                enddo
                jst=jst+1
                if(iswj.eq.iswi) then
                  call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
                  uu=scalmul(u,ug)
                  if(uu.gt.dmhijq.or.uu.lt.dmdijq.or.
     1               (uu.eq.0..and.i.eq.j)) cycle
                  du=sqrt(uu)
                  do l=1,3
                    if(du.ne.0.) then
                      ddx(l+3)=ug(l)/du
                      ddg(l)=u(l)**2
                    else
                      ddx(l+3)=0.
                      ddg(l)=0.
                    endif
                    ddx(l)=-ddx(l+3)
                  enddo
                else
                  call DistCheckComp(iswj,y3,du,dmhijq,ich)
                  if(ich.ne.0) cycle
                endif
2520            call qbyx(y3,y3(4),iswj)
                call qbyx(y3p,y3p(4),iswj)
                if(iswi.eq.iswj) then
                  sdu=sigma(sgx,ddx,6)
                  gdu=0.
                  m=0
                  do l=1,3
                    do k=1,3
                      m=m+1
                      gdu=gdu+MetTensS(m,iswi,KPhase)*ddg(l)*ddg(k)
                    enddo
                  enddo
                  if(du.gt.0.) then
                    gdu=sqrt(gdu)/du
                  else
                    gdu=0.
                  endif
                  sdu=sqrt(sdu**2+gdu**2)
                  if(sdu.lt..00001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3)) sdu=0.
                  if(itf(i).eq.2.and.itf(j).eq.2.and.lnh.gt.0.and.
     1              du.gt.0.) then
                    pom=1./du
                    do l=1,3
                      xp(l)=ug(l)*pom
                    enddo
                    pom=abs((BI(1)-BJ(1))*xp(1)**2+
     1                      (BI(2)-BJ(2))*xp(2)**2+
     2                      (BI(3)-BJ(3))*xp(3)**2+
     3                      (BI(4)-BJ(4))*2.*xp(1)*xp(2)+
     4                      (BI(5)-BJ(5))*2.*xp(1)*xp(3)+
     5                      (BI(6)-BJ(6))*2.*xp(2)*xp(3))
                    spom=sqrt((sBI(1)*xp(1)**2)**2+
     1                       (sBJ(1)*xp(1)**2)**2+
     2                       (sBI(2)*xp(2)**2)**2+
     3                       (sBJ(2)*xp(2)**2)**2+
     4                       (sBI(3)*xp(3)**2)**2+
     5                       (sBJ(3)*xp(3)**2)**2+
     6                       (sBI(4)*2.*xp(1)*xp(2))**2+
     7                       (sBJ(4)*2.*xp(1)*xp(2))**2+
     8                       (sBI(5)*2.*xp(1)*xp(3))**2+
     9                       (sBJ(5)*2.*xp(1)*xp(3))**2+
     a                       (sBI(6)*2.*xp(2)*xp(3))**2+
     1                       (sBJ(6)*2.*xp(2)*xp(3))**2)
                    write(lnh,107) atom(i),atom(j),du,sdu,pom,spom
                  endif
                  if(kmol(i).eq.kmol(j).and.kmol(i).gt.0.and.
     1               itf(i).eq.2.and.itf(j).eq.2.and..not.CalcHBonds)
     2              then
                    nm=kmol(i)
                    if(KTLS((nm-1)/mxp+1).gt.0) then
                      if(nm.ne.nmo.or.iswo.ne.iswi) then
                        call srotb(TrToOrtho(1,iswi,KPhase),
     1                             TrToOrtho(1,iswi,KPhase),TrMat)
                        call MultM(TrMat,tl(1,nm),tlp,6,6,1)
                        do l=1,6
                           tlp(l)=tlp(l)/tpisq
                        enddo
                        iswo=iswi
                        nmo=nm
                      endif
                      call MultM(TrToOrtho(1,iswi,KPhase),u,xp,3,3,1)
                      dif=tlp(1)*(xp(2)**2+xp(3)**2)+
     1                    tlp(2)*(xp(1)**2+xp(3)**2)+
     2                    tlp(3)*(xp(1)**2+xp(2)**2)-
     3                    2.*(tlp(4)*xp(1)*xp(2)+
     4                        tlp(5)*xp(1)*xp(3)+
     5                        tlp(6)*xp(2)*xp(3))
                      dif=.5*dif/du
                      dutls=du+dif
                    else
                      dutls=-1.
                    endif
                  else
                    dutls=-1.
                  endif
                else
                  sdu=0.
                endif
                call scode(jsym,jcenter,shjc,jsh,iswj,t80,SymmLabel(2))
                call ScodeCIF(jsym,jcenter,jsh,iswj,SymmCIF(2))
                t80=t80(:idel(t80))//'#'
                l=idel(SymmLabel(2))
                if(l.gt.0) t80=t80(:idel(t80))//SymmLabel(2)(:l)
                call zhusti(t80)
                if((NDimI(KPhase).eq.0.or.MaxUsedKw(KPhase).eq.0).and.
     1             VolaToDist.and.LstType.eq.1)
     2            call DistM61DistHeader(atom(i),atom(j),du,sdu,t80)
                if(j.lt.i.and.BratPrvni(j).and.BratDruhyaTreti(i)) then
                  iflg1=0
                else
                  iflg1=1
                endif
                if(.not.CalcHBonds) then
                  write(78) '#2',atom(j),isf(j),iswj,aij,du,sdu,
     1                      (0.,m=1,4),t80,SymmCIF(2),iflg1
                  n61=n61+1
                  if(VolaToDist.and.LstType.eq.0) then
                    call fflush
                    line=line+1
                    call DistRoundESD(t15,du,sdu,4)
                    iven(line)=atom(j)//'.....'//
     1                         t15(:idel(t15))
                    do k=1,8
                      if(iven(line)(k:k).eq.' ')
     1                  iven(line)(k:k)='.'
                    enddo
                    if(dutls.gt.0.) then
                      call fflush
                      line=line+1
                      call DistRoundESD(t15,dutls,sdu,4)
                      iven(line)='    TLS      '//
     1                           t15(:idel(t15))
!                      do k=1,8
!                        if(iven(line)(k:k).eq.' ')
!     1                    iven(line)(k:k)='.'
!                      enddo
                    endif
                  endif
                endif
                if(ntall(iswi).le.0.or.NDimI(KPhase).le.0) go to 3000
                do m=1,KModA(2,j)
                  call multm(rmp,ux(1,m,j),uxj(1,m),3,3,1)
                  call multmq(rmp,sux(1,m,j),suxj(1,m),3,3,1)
                  if(KFA(2,j).eq.0.or.m.ne.KModA(2,j)) then
                    call multm(rmp,uy(1,m,j),uyj(1,m),3,3,1)
                    call multmq(rmp,suy(1,m,j),suyj(1,m),3,3,1)
                  else
                    call CopyVek( uy(1,m,j), uyj(1,m),3)
                    call CopyVek(suy(1,m,j),suyj(1,m),3)
                  endif
                enddo
                call qbyx(shjc,qx,iswj)
                call AddVek(qx,tsp,xp,NDimI(KPhase))
                call AddVek(xp,tztnat,qx,NDimI(KPhase))
                call multm(GammaIntInv,qx,xp,NDimI(KPhase),
     1                     NDimI(KPhase),1)
                if(NonModulated(KPhase)) then
                  call SetRealArrayTo(oj,ntall(iswj),1.)
                  call SetRealArrayTo(xdj,3*ntall(iswj),0.)
                  call SetRealArrayTo(sxdj,3*ntall(iswj),0.)
                else
                  if(TypeModFun(j).le.1.or.KModA(1,j).le.1) then
                    call MakeOccMod(oj,qcnt(1,j),xp,nti,dttnat,a0(j),
     1                ax(1,j),ay(1,j),KModA(1,j),KFA(1,j),GammaIntInv)
                    if(KFA(2,j).gt.0.and.KModA(2,j).gt.0)
     1                call MakeOccModSawTooth(oj,qcnt(1,j),xp,nti,
     2                  dttnat,uyj(1,KModA(2,j)),uyj(2,KModA(2,j)),
     3                  KFA(2,j),GammaIntInv)
                  else
                    call MakeOccModPol(oj,qcnt(1,j),xp,nti,dttnat,
     1                ax(1,j),ay(1,j),KModA(1,j)-1,
     2                ax(KModA(1,j),j),a0(j)*.5,GammaIntInv,
     3                TypeModFun(j))
                  endif
                  if(TypeModFun(j).le.1) then
                    call MakePosMod(xdj,qcnt(1,j),xp,nti,dttnat,uxj,
     1                uyj,KModA(2,j),KFA(2,j),GammaIntInv)
                  else
                    call MakePosModPol(xdj,qcnt(1,j),xp,nti,dttnat,
     1                uxj,uyj,KModA(2,j),ax(1,j),a0(j)*.5,GammaIntInv,
     2                TypeModFun(j))
                  endif
                  call MakePosModS(sxdj,qcnt(1,j),xp,nti,dttnat,
     1              uxj,uyj,suxj,suyj,KModA(2,j),KFA(2,j),GammaIntInv)
                endif
                dummx=-9999.
                sdummp=-9999.
                dummn= 9999.
                dumav=0.
                sdumav=0.
                call CopyVek(y3p(4),y340,NDimI(KPhase))
                npom=0
                do kt=1,ntall(iswi)
                  call RecUnpack(kt,nd,nti,NDimI(KPhase))
                  do k=1,NDimI(KPhase)
                    t(k)=tfirsti(k)+(nd(k)-1)*dti(k)
                  enddo
                  if(oi(kt).le.ocut.or.oj(kt).le.ocut) then
                     dumkt=0.
                    sdumkt=0.
                    cycle
                  endif
                  npom=npom+1
                  if(iswj.eq.iswi) then
                    do l=1,3
                      um(l,kt)=up(l)+xdj(l,kt)-xdi(l,kt)
                    enddo
                    dumm(kt)=sqrt(uu)
                  else
                    call multm(TNaTI,t,tj,NDimI(KPhase),
     1                         NDimI(KPhase),1)
                    do l=4,NDim(KPhase)
                      y3p(l)=y340(l-3)+tj(l-3)
                    enddo
                    call multm(WPr,y3p,yp,NDim(KPhase),
     1                         NDim(KPhase),1)
                    do l=1,3
                      yp(l)=yp(l)-x1p(l)
                    enddo
                    call multm(MetTens(1,iswi,KPhase),yp,ypp,3,3,
     1                         1)
                    dumm(kt)=sqrt(scalmul(yp,ypp))
                    do l=1,3
                      yp(l)=yp(l)-xdi(l,kt)
                      ypp(l)=xdj(l,kt)
                    enddo
                    call qbyx(ypp,ypp(4),iswj)
                    call cultm(WPr,ypp,yp,NDim(KPhase),NDim(KPhase),
     1                         1)
                    call CopyVek(yp,um(1,kt),3)
                  endif
                  call multm(MetTens(1,iswi,KPhase),um(1,kt),umg,3,
     1                       3,1)
                  dumkt=sqrt(scalmul(um(1,kt),umg))
                  dum(kt)=dumkt
                  do l=1,3
                    if(dumkt.ne.0.) then
                      ddx(l+3)=umg(l)/dumkt
                      ddg(l)=um(1,kt)**2
                    else
                      ddx(l+3)=0.
                      ddg(l)=0.
                    endif
                    ddx(l)=-ddx(l+3)
                  enddo
                  if(EqRvLT0(sgx,3)) then
                    call CopyVek(sxdi(1,kt),sgxm,3)
                  else
                    call AddVekQ(sgx,sxdi(1,kt),sgxm,3)
                  endif
                  if(EqRvLT0(sgx(4),3)) then
                    call CopyVek(sxdj(1,kt),sgxm(4),3)
                  else
                    call AddVekQ(sgx(4),sxdj(1,kt),sgxm(4),3)
                  endif
                  sdumkt=sigma(sgxm,ddx,6)
                  gdu=0.
                  m=0
                  do l=1,3
                    do k=1,3
                      m=m+1
                      gdu=gdu+MetTensS(m,iswi,KPhase)*ddg(l)*ddg(k)
                    enddo
                  enddo
                  if(dumkt.gt.0.) then
                    gdu=sqrt(gdu)/dumkt
                  else
                    gdu=0.
                  endif
                  sdumkt=sqrt(sdumkt**2+gdu**2)
                  dums(kt)=sdumkt
                  sdummp=max(sdummp,sdumkt)
                  if(dumkt.gt.dummx) then
                     dummx= dumkt
                    sdummx=sdumkt
                  endif
                  if(dumkt.lt.dummn) then
                     dummn= dumkt
                    sdummn=sdumkt
                    ktmin=kt
                  endif
                   dumav= dumav+ dumkt
                  sdumav=sdumav+sdumkt
                  if(.not.CalcHBonds) then
                    write(78) sp2,atom(j),kt,kt,dumm(kt),dumkt,
     1                  sdumkt,oi(kt),oj(kt),0.,0.,t80,SymmCIF(2),iflg1
                    n61=n61+1
                  endif
                enddo
!                if(sdummx.le.0.) sdummx=sdummp
!                if(sdummn.le.0.) sdummn=sdummp
                sdummx=sdummp
                sdummn=sdummp
                if(npom.le.0.or.dummn.gt.dmhijm) then
                  if(VolaToDist.and.LstType.eq.0) line=max(line-1,0)
                  cycle
                endif
                if(VolaToDist) then
                  if(LstType.eq.0) then
                    call fflush
                  else if(LstType.eq.1) then
                    call DistM61DistHeader(atom(i),atom(j),du,sdu,t80)
                  endif
                endif
                if(iswi.eq.iswj) then
                  dumkt=1./float(npom)
                   dumav= dumav*dumkt
                  sdumav=sdumav*dumkt
                  if(VolaToDist) then
                    if(LstType.eq.0) then
                      call fflush
                      line=line+1
                    endif
                    call DistRoundESD(t15,dumav,sdumav,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' ave         '//t15(:l)
                      call fflush
                      line=line+1
                    else if(LstType.eq.1) then
                      s80='  ave : '//t15(:l)
                    endif
                    call DistRoundESD(t15,dummn,sdummn,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' min         '//t15(:l)
                      call fflush
                      line=line+1
                    else if(LstType.eq.1) then
                      s80=s80(:idel(s80))//'  min : '//t15(:l)
                    endif
                    call DistRoundESD(t15,dummx,sdummx,4)
                    l=idel(t15)
                    if(LstType.eq.0) then
                      iven(line)=' max         '//t15(:l)
                      call fflush
                    else if(LstType.eq.1) then
                      s80=s80(:idel(s80))//'  max : '//t15(:l)
                    endif
                  endif
                else if(VolaToDist) then
                  if(LstType.eq.0) then
                    call fflush
                    line=line+1
                  endif
                  call DistRoundESD(t15,dummn,sdummn,4)
                  l=idel(t15)
                  if(LstType.eq.0) then
                    iven(line)=' min         '//t15(:l)
                    if(ktmin.eq.1.or.ktmin.eq.nti(1))
     1                    iven(line)(9:9)='<'
                    call fflush
                  else if(LstType.eq.1) then
                    s80='  min : '//t15(:l)
                  endif
                endif
                if(.not.CalcHBonds)
     1            write(78) '#4',atom(j),isf(j),iswj,aij,dmod,t80,
     2                       SymmCIF(2),iflg1
                if(.not.VolaToDist) go to 3000
                if(LstType.eq.0) then
                  call fflush
                else if(LstType.eq.1) then
                  call newln(1)
                  write(lst,FormA) s80(:idel(s80))
                endif
                if(ieach.ne.0) then
                  l=0
                  do kt=1,ntall(iswi),ieach
                    call RecUnpack(kt,nd,nti,NDimI(KPhase))
                    do m=1,NDimI(KPhase)
                      t(m)=tfirsti(m)+(nd(m)-1)*dti(m)
                    enddo
                    l=l+1
                    if(oi(kt).gt.ocut.and.oj(kt).gt.ocut) then
                       dumkt=dum (kt)
                      sdumkt=dums(kt)
                      if(LstType.eq.0) then
                        call fflush
                        line=line+1
                        write(iven(line),'(2x,f5.3)') t(1)
                      endif
                      call DistRoundESD(t15,dumkt,sdumkt,4)
                      if(LstType.eq.0) then
                        iven(line)=iven(line)(:13)//
     1                    t15(:idel(t15))
                        if(oi(kt).le.oind.or.oj(kt).le.oind)
     1                     iven(line)(1:1)='*'
                        call fflush
                      else if(LstType.eq.1) then
                      endif
                    endif
                  enddo
                endif
                if(LstType.eq.0) then
                  call fflush
                  line=line+1
                  write(iven(line),'(26(''-''))')
                  if(LstType.eq.0) call fflush
                endif
3000            if(.not.iuhl0) then
                  call uhelat(i,j)
                  if(ErrFlag.ne.0) go to 9900
                  if(LstType.eq.0) call fflush
                endif
              enddo
3100        continue
          enddo
          cycle
4000      if(VolaToDist.and.NaCelkem.gt.1) then
            call FeFlowChartEvent(nfloat,ie)
            if(ie.ne.0) then
              call DistBudeBreak
              if(ErrFlag.ne.0) go to 9900
            endif
          endif
        enddo
        if(VolaToDist) then
          if(LstType.eq.0) then
            call fflush
            line=line+1
            iven(line)='*'
            call fflush
          else if(LstType.eq.1) then
            call newln(1)
            write(lst,120)
          endif
        endif
      enddo
      if(.not.VolaToDist) go to 9999
      if(NaCelkem.gt.1) call FeFlowChartRemove
      if(line.gt.0.and.LstType.eq.0) then
        call OutSt
        line=mxline
      endif
      if(nBondVal.ne.0.and..not.CalcHBonds) then
        call newpg(0)
        call TitulekVRamecku('List of bond valence sums')
        nfloat=0
        if(NDimI(KPhase).gt.0) then
          call FeFlowChartOpen(-1.,-1.,1000,n61,
     1                         'Summation of bond valences sums',' ',
     2                         ' ')
        endif
        Nacitame=.false.
        Konec=.false.
        Prvne=.true.
        n=0
        rewind 78
5000    read(78,err=5010,end=5010) Label,At2,j,iswip,pom,du,sdu,occi,
     1                             occj
        if(NDimI(KPhase).gt.0) then
          call FeFlowChartEvent(nfloat,ie)
          if(ie.ne.0) then
            call DistBudeBreak
            if(ErrFlag.ne.0) go to 9900
          endif
        endif
        go to 5020
5010    Konec=.true.
        Label='#1'
5020    if(Label.eq.'#1') then
          if(n.gt.0) then
            if(Prvne) then
              call Newln(1)
              write(lst,FormA)
              Prvne=.false.
            endif
            if(NDimI(KPhase).eq.0) then
              call Newln(1)
              sdumav=sqrt(sdumav)
              call DistRoundESD(t15,dumav,sdumav,3)
              t80='Bond valence for : '//at1//t15
              write(lst,FormA) t80(:idel(t80))
            else
              dummx=-9999.
              dummn= 9999.
              dumav=0.
              sdumav=0.
              sdummp=-9999.
              npom=0
              do kt=1,ntall(iswi)
                 pom=dum (kt)
                spom=dums(kt)
                sdummp=max(sdummp,sdumkt)
                if(pom.gt.0.) then
                  if(pom.gt.dummx) then
                     dummx= pom
                    sdummx=spom
                  endif
                  if(pom.lt.dummn) then
                     dummn= pom
                    sdummn=spom
                  endif
                   dumav=dumav+pom
                  sdumav=sdumav+spom
                  npom=npom+1
                endif
              enddo
              sdummx=sdummp
              sdummn=sdummp
               dumav= dumav/float(npom)
              sdumav=sdumav/float(npom)
              t80='Bond valence for : '//at1//' average : '
              call DistRoundESD(t15,dumav,sdumav,3)
              s80=t15
              call DistRoundESD(t15,dummn,sdummn,3)
              s80=s80(:12)//' minumum : '//t15
              call DistRoundESD(t15,dummx,sdummx,3)
              s80=s80(:33)//'  maximum : '//t15
              call Newln(2)
              write(lst,FormA)
              write(lst,FormA) t80(:idel(t80))//' '//s80(:idel(s80))
            endif
            if(ieach.ne.0.and.npom.gt.0) then
              Veta='Individual values:'
              l=20
              do kt=1,ntall(iswi),ieach
                call RecUnpack(kt,nd,nt(1,iswi),NDimI(KPhase))
                do i=1,NDimI(KPhase)
                  t(i)=tfirsti(i)+(nd(i)-1)*dti(i)
                enddo
                call DistRoundESD(t15,dum(kt),dums(kt),2)
                write(Veta(l:),'(f6.3,1x,a)') t(1),t15(:idel(t15))
                l=l+17
                if(l.gt.111) then
                  call newln(1)
                  write(lst,FormA) Veta(:idel(Veta))
                  Veta=' '
                  l=20
                endif
              enddo
              if(l.gt.1) then
                call newln(1)
                write(lst,FormA) Veta(:idel(Veta))
              endif
            endif
          endif
          if(NDimI(KPhase).gt.0) then
            call SetRealArrayTo(dum ,ntall(iswi),0.)
            call SetRealArrayTo(dums,ntall(iswi),0.)
          endif
          if(Konec) go to 5100
          At1=At2
          isfi=j
          aii=pom
          n=0
           dumav=0.
          sdumav=0.
          Nacitame=.false.
          iswi=iswip
        else if(Label.eq.'#2') then
          isfj=j
          aij=pom
          if(NDimI(KPhase).le.0) then
            if(iBondVal(isfi,isfj).ne.0) then
              n=n+1
              pom=exp((dBondVal(isfi,isfj)-du)/cBondVal(isfi,isfj))*aij
               dumav= dumav+pom
              sdumav=sdumav+(pom*sdu/cBondVal(isfi,isfj))**2
            endif
          else
            Nacitame=.true.
          endif
        else if(Label.eq.' '.and.Nacitame) then
          kt=j
          if(iBondVal(isfi,isfj).ne.0.and.
     1       occi.gt.ocut.and.occj.gt.ocut) then
            n=n+1
            ppp=exp((dBondVal(isfi,isfj)-du)/cBondVal(isfi,isfj))*aij*
     1          occj
            dum (kt)=dum (kt)+ppp
            dums(kt)=dums(kt)+(ppp*sdu/cBondVal(isfi,isfj))**2
          endif
        else
          Nacitame=.false.
        endif
        go to 5000
5100    call FeFlowChartRemove
      endif
      if(VolaToDist.and..not.CalcHBonds) then
          call newpg(0)
          call TitulekVRamecku('Hirshfeld test')
          call CloseIfOpened(lnh)
          call OpenFile(lnh,fln(:ifln)//'.l66','formatted','unknown')
          n=0
5200      read(lnh,107,err=5250,end=5250) at1,at2,du,sdu,pom,spom
          if(pom.gt.HirshLev*spom) then
            if(n.le.0) then
              Veta='Bond             Distance          Uij projection'
              call newln(1)
              write(lst,FormA) Veta(:idel(Veta))
              call newln(1)
              write(lst,FormA)
            endif
            n=n+1
            Veta=At1(:idel(At1))//'-'//At2(:idel(At2))
            call DistRoundESD(t15,du,sdu,4)
            Veta=Veta(:17)//t15
            call DistRoundESD(t15,pom,spom,6)
            Veta=Veta(:35)//t15
            call newln(1)
            write(lst,FormA) Veta(:idel(Veta))
          endif
          go to 5200
5250      if(n.le.0) then
            Veta='There were no bonds violating Hirshfeld condition'
            call newln(1)
            write(lst,FormA) Veta(:idel(Veta))
          endif
          close(lnh,status='delete')
      endif
      if(VolaToDist.and..not.CalcHBonds) then
        rewind 78
        if(NDimI(KPhase).le.0) then
          Lab2='#2'
          Lab3='#3'
        else
          Lab2='#4'
          Lab3='#5'
        endif
6000    read(78,end=6500) Label,At3,isfj,iswj,pom,dmod,t80,t103,iflg3
        if(Label.eq.'#1') then
          At1=At3
          t101=t103
          iflg1=iflg3
        else if(Label.eq.Lab2) then
          At2=At3
          t102=t103
          iflg2=iflg3
          if(iflg1.eq.1.and.iflg2.eq.1) then
            if(IDist.le.0) then
              write(LnDist,'(''loop_'')')
              do i=10,15
                if(i.eq.12.or.i.eq.13) cycle
                j=i
                if(i.ge.14.and.i.le.15.and.NDimI(KPhase).gt.0) j=j+39
                write(LnDist,FormCIF) ' '//CifKey(j,12)
              enddo
              if(NDimI(KPhase).le.0) then
                write(LnDist,FormCIF) ' '//CifKey(12,12)
              else
                do i=52,50,-1
                  write(LnDist,FormCIF) ' '//CifKey(i,12)
                enddo
              endif
              write(LnDist,FormCIF) ' '//CifKey(13,12)
            endif
            IDist=IDist+1
            t80=At1(:idel(At1))//' '//At2(:idel(At2))//' . '//
     1          t102(:idel(t102))
            if(NDimI(KPhase).le.0) then
              n=1
            else
              n=3
            endif
            do i=2,2*n,2
              call RoundESD(t15,dmod(i-1),dmod(i),4,0,0)
              t80=t80(:idel(t80))//' '//t15(:idel(t15))
            enddo
            write(LnDist,FormA) '  '//t80(:idel(t80))//' ?'
          endif
        else if(Label.eq.Lab3) then
          if(iflg1.eq.1.and.iflg3.eq.1) then
            if(IAngle.le.0) then
              write(LnAngle,'(''loop_'')')
              do i=3,9
                if(i.eq.6) cycle
                j=i
                if(i.ge.7.and.i.le.9.and.NDimI(KPhase).gt.0) j=j+40
                write(LnAngle,FormCIF) ' '//CifKey(j,12)
              enddo
              if(NDimI(KPhase).le.0) then
                write(LnAngle,FormCIF) ' '//CifKey(2,12)
              else
                do i=46,44,-1
                  write(LnAngle,FormCIF) ' '//CifKey(i,12)
                enddo
              endif
              write(LnAngle,FormCIF) ' '//CifKey(6,12)
            endif
            IAngle=IAngle+1
            t80=At2(:idel(At2))//' '//At1(:idel(At1))//' '//
     1          At3(:idel(At3))//' '//t102(:idel(t102))//' . '//
     2          t103(:idel(t103))
            if(NDimI(KPhase).le.0) then
              n=1
            else
              n=3
            endif
            do i=2,2*n,2
              call RoundESD(t15,dmod(i-1),dmod(i),2,0,0)
              t80=t80(:idel(t80))//' '//t15(:idel(t15))
            enddo
            write(LnAngle,FormA) '  '//t80(:idel(t80))//' ?'
          endif
        endif
        go to 6000
      endif
6500  if(ittab.eq.0.or.CalcHBonds) go to 9900
      call newln(2)
      write(lst,FormA)
      write(lst,FormA)
      do i=1,NAtCalc
        if(isf(i).le.0.or..not.BratPrvni(i).or.kswa(i).ne.KPhase) cycle
        iswi=iswa(i)
        call newln(3)
        write(lst,101)
        write(lst,'(44x,''** Coordination for atom : '',a8,'' **'')')
     1        atom(i)
        write(lst,101)
        call CopyVek (dt(1,iswi),dti,NDimI(KPhase))
        call CopyVekI(nt(1,iswi),nti,NDimI(KPhase))
        call CopyVek(tfirst(1,iswi),tfirsti,NDimI(KPhase))
        t(1)=tfirsti(1)
        l=0
        ip=333
        do kt=1,ntall(iswi),ieach
          l=l+1
          ntb=0
          call SetIntArrayTo(nutab,30,0)
          rewind 78
8200      read(78,end=8340) Label,At1,isfi
          if(Label.ne.'#1'.or.At1.ne.Atom(i)) go to 8200
8300      read(78,end=8340) Label,At2,isfj
8310      if(Label.ne.'#2'.and.Label.ne.'#3') then
            if(Label.eq.'#1') then
              go to 8340
            else
              go to 8300
            endif
          else
            if(dmhk.ge.0) then
              pom=1.
            else
              pom=1.+.01*ExpDMax
            endif
            dmhijm=TypicalDistUse(isfi,isfj)*pom
            JeToUhel=Label.eq.'#3'
          endif
8320      read(78,end=8340) Label,At1,k,k,pom,pom,spom,occi,occk
          if(Label.ne.sp2) then
            At2=At1
            go to 8310
          else
            if(k.lt.kt) then
              go to 8320
            else if(k.gt.kt) then
              go to 8300
            endif
          endif
          if(JeToUhel) then
            if(occi.gt.ocut.and.occj.gt.ocut.and.occk.gt.ocut.and.
     1         pomd.le.dmhijm) then
              if(nutab(ntb).le.30) then
                nutb=nutab(ntb)+1
                nutab(ntb)=nutb
                atutab(nutb,ntb)=at2
                utab(nutb,ntb)=pom
                sutab(nutb,ntb)=spom
              else
                call FeChybne(-1.,-1.,'the maximum number of 30 '//
     1                        'neighbours exceeded.',' ',SeriousError)
                ErrFlag=1
                go to 9900
              endif
            endif
          else
            occj=occk
            pomd=pom
            if(occi.gt.ocut.and.occj.gt.ocut.and.pomd.le.dmhijm) then
              if(ntb.lt.30) then
                ntb=ntb+1
                attab(ntb)=at2
                idtab(ntb)=nint(pom*10000.)
                dstab(ntb)=spom
              else
                call FeChybne(-1.,-1.,'the maximum number of 30 '//
     1                        'neighbours exceeded.',' ',SeriousError)
                ErrFlag=1
                go to 9900
              endif
            endif
          endif
          go to 8300
8340      if(ip.gt.111) then
            if(ip.ne.333) then
              do n=1,lmax
                if(n.eq.1) then
                  call newln(3)
                  write(lst,120)
                  write(lst,FormA) ltab(n)(:idel(ltab(n)))
                  write(lst,120)
                else
                  call newln(1)
                  write(lst,FormA) ltab(n)(:idel(ltab(n)))
                endif
              enddo
            endif
            lmax=0
            do n=1,444
              ltab(n)=' '
              do ip=1,128,25
                ltab(n)(ip:ip)='|'
              enddo
            enddo
            ip=3
          endif
          n=0
          n=n+1
          write(ltab(n)(ip:ip+22),'(6x,'' t='',f6.3)') t(1)
          if(ntb.gt.0) then
            call indexx(ntb,idtab,iptab)
            do j=1,ntb
              k=iptab(j)
              n=n+1
              call DistRoundESD(t15,float(idtab(k))*.0001,dstab(k),4)
              jj=max(idel(t15),index(t15,'.')+8)
              ltab(n)(ip:ip+7)=attab(k)
              ltab(n)(ip+22-jj:ip+21)=t15(:jj)
              do m=0,10
                if(ltab(n)(ip+m:ip+m).eq.' ') ltab(n)(ip+m:ip+m)='-'
              enddo
              do m=1,nutab(k)
                n=n+1
                call DistRoundESD(t15,utab(m,k),sutab(m,k),2)
                jj=idel(t15)
                ltab(n)(ip+1:ip+8)=atutab(m,k)
                jj=max(idel(t15),index(t15,'.')+6)
                ltab(n)(ip+23-jj:ip+22)=t15(:jj)
                do mm=1,10
                  if(ltab(n)(ip+mm:ip+mm).eq.' ')
     1                                    ltab(n)(ip+mm:ip+mm)='.'
                enddo
              enddo
            enddo
          endif
8500      t(1)=t(1)+dti(1)*float(ieach)
          lmax=max(n,lmax)
          ip=ip+25
        enddo
        if(ip.gt.3) then
          do l=1,lmax
            if(l.eq.1) then
              call newln(3)
              write(lst,120)
              write(lst,FormA) ltab(l)(:idel(ltab(l)))
              write(lst,120)
            else
              call newln(1)
              write(lst,FormA) ltab(l)(:idel(ltab(l)))
            endif
          enddo
        endif
        call newln(1)
        write(lst,120)
      enddo
9900  call CloseIfOpened(78)
9999  if(.not.VolaToDist) then
        if(PrvniSymmString.ne.' ') call iom40(0,0,fln(:ifln)//'.m40')
      endif
      if(ICDDBatch)
     1  call CopyFile(fln(:ifln)//'.l61',fln(:ifln)//'.m61')
      if(allocated(TransMat)) call DeallocateTrans
      if(allocated(vt6p)) deallocate(vt6p,TypicalDistUse)
      return
100   format(a1,a8,i6,3f10.6)
101   format(44x,38('*'))
102   format(26('*'),6x)
103   format('* atom ',a8,10x,'*',6x)
104   format(a1,i6,f9.4)
105   format(f8.3)
106   format(1x,a3,9x,f7.4,12x)
107   format(2a8,6f10.6)
116   format('distance: ',2(a8,1x),2f9.4,' .',7x,a7)
117   format('angle: ',3(a8,1x),2f9.4,1x,a7,' .',7x,a7)
118   format('distance: ',2(a8,1x),6f9.4,' .',7x,a7)
119   format('angle: ',3(a8,1x),6f9.4,1x,a7,' .',7x,a7)
120   format(126('-'))
      end
      subroutine DistSetComp(isw1,isw2)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension ZdT(9)
      call multm(zv(1,isw1,KPhase),zvi(1,isw2,KPhase),WPr,NDim(KPhase),
     1           NDim(KPhase),NDim(KPhase))
      call matinv(WPr,WPri,det,NDim(KPhase))
      i=NDim(KPhase)*3+1
      do j=1,3*NDimI(KPhase),3
        do l=0,2
          call CopyVek(WPr(i),Zd(j),3)
        enddo
        i=i+NDim(KPhase)
      enddo
      call TrMat(Zd,ZdT,3,NDimI(KPhase))
      call Multm(ZdT,MetTens(1,isw1,KPhase),GZd,NDimI(KPhase),3,3)
      call Multm(GZd,Zd,ZdGZd,NDimI(KPhase),3,NDimI(KPhase))
      do 1400l=1,NDimI(KPhase)
        pom=0.
        m=l
        do k=1,NDimI(KPhase)
          if(abs(ZdGZd(m)).gt..001) go to 1400
          m=m+NDimI(KPhase)
        enddo
        ZdGZd(l+(l-1)*NDimI(KPhase))=1.
1400  continue
      call MatInv(ZdGZd,ZdGZdI,pom,NDimI(KPhase))
      j=0
      do l=1,NDimI(KPhase)
        i=NDim(KPhase)*(l+2)+3
        do k=1,NDimI(KPhase)
          j=j+1
          i=i+1
          tnat(j)=WPr(i)
        enddo
      enddo
      call TrMat(qu(1,1,isw1,KPhase),TNaTI,3,NDimI(KPhase))
      call RealVectorToOpposite(TNaTI,TNaTI,3*NDimI(KPhase))
      call cultm(TNaTI,Zd,TNaT,NDimI(KPhase),3,NDimI(KPhase))
      call matinv(TNaT,TNaTi,pom,NDimI(KPhase))
      return
      end
      subroutine DistCheckComp(isw,y1,du,dmez,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension y1(*),yp(6),ypp(6),tmina(3),Right(3)
      ich=0
      call qbyx(y1,y1(4),isw)
      call multm(WPr,y1,yp,NDim(KPhase),NDim(KPhase),1)
      do l=1,3
        u(l)=x1(l)-yp(l)
      enddo
      call multm(GZd,u,Right,NDimI(KPhase),3,1)
      call multm(ZdGZdI,Right,tmina,NDimI(KPhase),NDimI(KPhase),1)
      call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
      u(1)=-scalmul(u,ug)
      pom=-u(1)
      call multm(Right,ZdGZdI,ug,1,NDimI(KPhase),NDimI(KPhase))
      call cultm(ug,Right,u,1,NDimI(KPhase),1)
      uu=max(-u(1),0.)
      du=sqrt(uu)
      if(uu.gt.dmez) go to 5000
      if(NDimI(KPhase).eq.1) then
        TMin=tmina(1)*TNaT(1)
        if(TMin.lt.TFirstI(1).or.TMin.gt.TLastI(1)) then
          call CopyVek(y1,yp,NDim(KPhase))
          yp(4)=yp(4)+TFirstI(1)*tnati(1)
          k=0
1500      call multm(WPr,yp,ypp,NDim(KPhase),NDim(KPhase),1)
          do l=1,3
            u(l)=ypp(l)-x1(l)
          enddo
          call multm(MetTens(1,iswi,KPhase),u,ug,3,3,1)
          pom=scalmul(u,ug)
          if(pom.lt.dmez) go to 9999
          if(k.gt.0) go to 5000
          k=1
          yp(4)=yp(4)+(TLastI(1)-TFirstI(1))*tnati(1)
          go to 1500
        endif
      endif
      go to 9999
5000  ich=1
9999  return
      end
      subroutine UhelAt(i,j)
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension z0(6),z1(6),z2(3),vz(3),shk(3),shkc(3),vm(3),vmg(3),
     1          uxk(3,mxw),uyk(3,mxw),zp(6),sx0(6),z0p(6),z1p(6),z2p(3),
     2          vzp(3),vp(3),MxCell(3),z340(3),zpp(6),z3(6),z3p(6),
     3          ksh(3),tztnat(3),dttnat(3),GammaIntInv(9),t(3),tj(3),
     4          nd(3),tsp(3),xp(6),suxk(3,mxw),suyk(3,mxw),sgxm(9),
     5          umg(3),umod(6),smp(36),rmp(9),rm6p(36),s6p(6),vt6p(:,:),
     6          MxDCell(3),JCell(3)
      character*128 StHBonds
      character*80 t80,s80
      character*20 AtH(3)
      character*15 ValH(4),t15
      character*8  sp8
      character*2  sp2
      logical eqrv,EqRvLT0
      allocatable vt6p
      equivalence (umod(1),damav),(umod(2),sdamav),
     1            (umod(3),dammn),(umod(4),sdammn),
     2            (umod(5),dammx),(umod(6),sdammx)
      data sp2,sp8/2*' '/
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
      if(CalcHBonds) then
        kp=1
      else
        kp=j
      endif
      do 4000k=kp,NAtCalc
        isfk=isf(k)
        iswk=iswa(k)
        if(kswa(k).ne.KPhase.or..not.BratDruhyATreti(k).or.isfk.eq.0.or.
     1     (CalcHBonds.and.isfk.eq.isfh(KPhase))) cycle
        if(SkipSplit.gt.0) then
          if(kmol(i).ne.kmol(k)) then
            do ii=1,NMolSplit
              kk=0
              do jj=1,MMolSplit(ii)
                if(IMolSplit(jj,ii).eq.kmol(i)) then
                  kk=kk+1
                else if(IMolSplit(jj,ii).eq.kmol(k)) then
                  kk=kk+1
                endif
              enddo
              if(kk.ge.2) go to 4000
            enddo
          endif
          if(kmol(i).ne.kmol(k)) then
            do ii=1,NMolSplit
              kk=0
              do jj=1,MMolSplit(ii)
                if(IMolSplit(jj,ii).eq.kmol(i)) then
                  kk=kk+1
                else if(IMolSplit(jj,ii).eq.kmol(k)) then
                  kk=kk+1
                endif
              enddo
              if(kk.ge.2) go to 4000
            enddo
          endif
          do ii=1,NAtSplit
            kk=0
            do jj=1,MAtSplit(ii)
              if(IAtSplit(jj,ii).eq.i) then
                kk=kk+1
              else if(IAtSplit(jj,ii).eq.k) then
                kk=kk+1
              endif
            enddo
            if(kk.ge.2) go to 4000
          enddo
          do ii=1,NAtSplit
            kk=0
            do jj=1,MAtSplit(ii)
              if(IAtSplit(jj,ii).eq.j) then
                kk=kk+1
              else if(IAtSplit(jj,ii).eq.k) then
                kk=kk+1
              endif
            enddo
            if(kk.ge.2) go to 4000
          enddo
        endif
        call SetRealArrayTo(xp,3,0.)
        kk=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,k).ne.0) then
            xp(1)=ax(KModA(1,k),k)
            kk=1
          else if(KFA(2,k).ne.0) then
            xp(1)=uy(1,KModA(2,k),k)
            kk=1
          endif
        endif
        call SpecPos(x(1,j),xp,kk,iswk,.01,n)
        aik=ai(k)*float(n)
        if(CalcHBonds) then
          dmhik =HAMax+dxm(i)+dxm(k)
          dmhikm=HAMax
        else
          if(dmhk.ge.0) then
            pom=1.
          else
            pom=1.+.01*ExpDMax
          endif
          dmhikm=TypicalDistUse(isf(i),isf(k))*pom
          dmhik =dmhikm+dxm(i)+dxm(k)
        endif
        dmhika=dmhik*ah
        dmhikb=dmhik*bh
        dmhikc=dmhik*ch
        dmhikq=dmhik**2
        dmdikq=dmdi**2
        do l=1,3
          MxCell(l)=dmhik*rcp(l,iswk,KPhase)+1
        enddo
        MxNCell=1
        MxDCell(3)=1
        do l=2,1,-1
          MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
        enddo
        MxNCell=MxDCell(1)*(2*MxCell(1)+1)
        do l=1,3
          z0(l)=xdst(l,k)
          z0p(l)=x(l,k)
          sx0(l)=sx(l,k)
        enddo
        ks=0
        kst=0
        do ksym=1,NSymmN(KPhase)
          ks=ks+1
          if(isa(ks,k).le.0) cycle
          iswk=ISwSymm(ksym,iswa(k),KPhase)
          iswkin=iswi*10+iswk
          if(iswk.ne.iswi) then
            if(iswkin.ne.iswlast) call DistSetComp(iswi,iswk)
          else
            call UnitMat(TNaT ,NDimI(KPhase))
            call UnitMat(TNaTi,NDimI(KPhase))
          endif
          iswlast=iswkin
          call multm(TNaTi,tfirsti,TzTNaT,NDimI(KPhase),NDimI(KPhase),
     1               1)
          call multm(TNaTi,dti,DtTNaT,NDimI(KPhase),NDimI(KPhase),1)
          if(iswk.eq.iswa(k)) then
            call CopyMat(rm (1,ksym,iswk,KPhase),rmp ,3)
            call CopyMat(rm6(1,ksym,iswk,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,ksym,iswk,KPhase), s6p,NDim(KPhase))
            do l=1,NLattVec(KPhase)
              call CopyVek(vt6(1,l,iswk,KPhase),vt6p(1,l),NDim(KPhase))
            enddo
          else
            call multm(zv(1,iswk,KPhase),zvi(1,iswa(k),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,ksym,iswa(k),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rm6p,rmp,NDim(KPhase))
            call multm(smp,s6(1,ksym,iswa(k),KPhase),s6p,NDim(KPhase),
     1                 NDim(KPhase),1)
            do l=1,NLattVec(KPhase)
              call multm(smp,vt6(1,l,iswa(k),KPhase),vt6p(1,l),
     1                   NDim(KPhase),NDim(KPhase),1)
            enddo
          endif
          call multm(rmp,z0,z1,3,3,1)
          call multm(rmp,z0p,z1p,3,3,1)
          if(EqRvLT0(sx0,3)) then
            call SetRealArrayTo(sgx(7),3,-.1)
          else
            call multmq(rmp,sx0,sgx(7),3,3,1)
          endif
          call GetGammaIntInv(RM6p,GammaIntInv)
          do l=1,3
            z1(l)=z1(l)+s6p(l)
            z1p(l)=z1p(l)+s6p(l)
          enddo
          do 7100kcenter=1,NLattVec(KPhase)
            do l=1,kcenter-1
              if(eqrv(vt6p(1,kcenter),vt6p(1,l),3,.001)) go to 7100
            enddo
            do l=4,NDim(KPhase)
              tsp(l-3)=-s6p(l)-vt6p(l,kcenter)
            enddo
            do l=1,3
              z2(l)=z1(l)+vt6p(l,kcenter)
              z2p(l)=z1p(l)+vt6p(l,kcenter)
            enddo
            if(iswk.eq.iswi) then
              call bunka(z2,shk,1)
              do l=1,3
                vzp(l)=z2p(l)-x1p(l)+shk(l)
                vz(l)=z2(l)-x1(l)
              enddo
              if(NComp(KPhase).gt.1) then
                do l=1,3
                  MxCell(l)=dmhik*rcp(l,iswa(j),KPhase)+1
                enddo
                MxDCell(3)=1
                do l=2,1,-1
                  MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
                enddo
                MxNCell=MxDCell(1)*(2*MxCell(1)+1)
              endif
            else
              call multm(WPri,x1,zp,NDim(KPhase),NDim(KPhase),1)
              do l=1,3
                shk(l)=zp(l)-z2(l)
              enddo
              do l=1,3
                shk(l)=ifix(shk(l))
                z2(l)=z2(l)+shk(l)
                z2p(l)=z2p(l)+shk(l)
              enddo
              do l=1,NDim(KPhase)
                xp(l)=x1(l)
                if(l.gt.3) xp(l)=xp(l)+tfirsti(l-3)
              enddo
              do l=1,3
                pom=abs(zp(l))
                if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
              enddo
              do l=4,NDim(KPhase)
                xp(l)=xp(l)+dti(l-3)*float(nti(l-3)-1)
              enddo
              call multm(WPri,xp,zp,NDim(KPhase),NDim(KPhase),1)
              do l=1,3
                pom=abs(zp(l))
                if(float(MxCell(l)).lt.pom) MxCell(l)=ifix(pom)+1
              enddo
              MxDCell(3)=1
              do l=2,1,-1
                MxDCell(l)=MxDCell(l+1)*(2*MxCell(l+1)+1)
              enddo
              MxNCell=MxDCell(1)*(2*MxCell(1)+1)
            endif
            do JCellI=0,MxNCell-1
              call UnPackHKL(JCellI,JCell,MxCell,MxDCell,3)
              do l=1,3
                pom=JCell(l)
                if(iswj.eq.iswi) then
                  v(l)=vz(l)+pom
                  vp(l)=vzp(l)+pom
                endif
                z3(l)=z2(l)+pom
                z3p(l)=z2p(l)+pom
                ksh(l)=nint(shk(l))+pom
                shkc(l)=shk(l)+pom+s6p(l)+vt6p(l,kcenter)
              enddo
              kst=kst+1
              if(k.eq.j.and.kst.le.jst) cycle
              if(iswk.eq.iswi) then
                call multm(MetTens(1,iswi,KPhase),v,vg,3,3,1)
                vv=scalmul(v,vg)
                if(vv.gt.dmhikq.or.vv.lt.dmdikq.or.
     1             (vv.eq.0..and.i.eq.k)) cycle
                dv=sqrt(vv)
              else
                call DistCheckComp(iswj,z3,dv,dmhikq,ich)
                if(ich.ne.0) cycle
              endif
              call SCode(ksym,kcenter,shkc,ksh,iswk,t80,SymmLabel(3))
              t80=t80(:idel(t80))//'#'
              l=idel(SymmLabel(3))
              if(l.gt.0) t80=t80(:idel(t80))//SymmLabel(3)(:l)
              call ScodeCIF(ksym,kcenter,ksh,iswk,SymmCIF(3))
              if(CalcHBonds) then
                do l=1,3
                  if(l.eq.1) then
                    ii=j
                    ll=2
                  else if(l.eq.2) then
                    ii=i
                    ll=1
                  else if(l.eq.3) then
                    ii=k
                    ll=3
                  endif
                  AtH(l)=Atom(ii)
                  if(SymmLabel(ll).ne.' ')
     1              AtH(l)=AtH(l)(:idel(AtH(l)))//'#'//
     1                     SymmLabel(ll)(:idel(SymmLabel(ll)))
                enddo
                call DistHBond(u,ug,v,vg,sgx,MetTensS(1,iswi,KPhase),
     1                         du,sdu,dv,sdv,dw,sdw,angle,sangle)
                if(Angle.lt.DHAMin) cycle
                if(IHBonds.le.0) then
                  call NewLn(2)
                  write(lst,'(''Donor'',12x,''Hydrogen'',12x,
     1                        ''Acceptor'',12x,''D-H distance     '',
     2                        ''H...A distance   D-A distance     '',
     3                        ''A-H...D angle''/)')
                  if(NDimI(KPhase).le.0) then
                    write(LnHBonds,'(''loop_'')')
                    do l=1,nCIFHBonds
                      write(LnHBonds,FormCIF)
     1                  ' '//CifKey(CIFHBonds(l),12)
                    enddo
                  endif
                endif
                IHBonds=IHBonds+1
                call DistRoundESD(ValH(1),du,sdu,2)
                call DistRoundESD(ValH(2),dv,sdv,2)
                call DistRoundESD(ValH(3),dw,sdw,4)
                call DistRoundESD(ValH(4),Angle,SAngle,2)
                write(StHBonds,'(a18,2a20,2x,4a15)') AtH,ValH
                if(NDimI(KPhase).le.0) then
                  t80=Atom(j)(:idel(Atom(j)))//' '//
     1                Atom(i)(:idel(Atom(i)))//' '//
     2                Atom(k)(:idel(Atom(k)))//' '
                  do l=1,3
                    if(l.eq.1) then
                      ll=2
                    else if(l.eq.2) then
                      ll=1
                    else if(l.eq.3) then
                      ll=3
                    endif
                    t80=t80(:idel(t80))//' '//
     1                  SymmCIF(ll)(:idel(SymmCIF(ll)))
                  enddo
                  do l=1,4
                    t80=t80(:idel(t80))//'   '//ValH(l)(:idel(ValH(l)))
                  enddo
                  t80=t80(:idel(t80))//' ?'
                  write(LnHBonds,FormA) '  '//t80(:idel(t80))
                endif
                if(NDimI(KPhase).le.0) then
                  call NewLn(1)
                  write(lst,FormA) StHBonds(:idel(StHBonds))
                endif
              else
                call DistAng(u,ug,v,vg,uu,vv,sgx,
     1                       MetTensS(1,iswj,KPhase),angle,su)
                if(iswi.eq.iswj.and.iswi.eq.iswk) then
                  if(su.lt..0001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.
     2               EqRvLT0(sgx(7),3)) su=0.
                else
                  su=0.
                endif
                if((NDimI(KPhase).le.0.or.MaxUsedKw(KPhase).eq.0).and.
     1              VolaToDist.and.LstType.eq.1)
     2             call DistM61AngleHeader(Atom(j),Atom(i),Atom(k),
     3                                     angle,su,t80)
                write(78) '#3',atom(k),isf(k),iswk,aik,angle,su,
     1                    (0.,m=1,4),t80,SymmCIF(3),1
                if(VolaToDist.and.LstType.eq.0) then
                  call fflush
                  line=line+1
                  call DistRoundESD(t15,Angle,su,2)
                  iven(line)='  '//atom(k)//'...'//t15(:idel(t15))
                  do l=3,10
                    if(iven(line)(l:l).eq.' ') iven(line)(l:l)='.'
                  enddo
                  call fflush
                endif
              endif
              if(nt(1,1).le.0.or.NDimI(KPhase).le.0) cycle
              call qbyx(z3,z3(4),iswk)
              call qbyx(z3p,z3p(4),iswk)
              do m=1,KModA(2,k)
                call multm(rmp,ux(1,m,k),uxk(1,m),3,3,1)
                call multmq(rmp,sux(1,m,k),suxk(1,m),3,3,1)
                if(KFA(2,k).eq.0.or.m.ne.KModA(2,k)) then
                  call multm(rmp,uy(1,m,k),uyk(1,m),3,3,1)
                  call multmq(rmp,sux(1,m,k),suyk(1,m),3,3,1)
                else
                  call CopyVek( uy(1,m,k), uyk(1,m),3)
                  call CopyVek(suy(1,m,k),suyk(1,m),3)
                endif
              enddo
              call qbyx(shkc,qx,iswk)
              call AddVek(qx,tsp,xp,NDimI(KPhase))
              call AddVek(xp,tztnat,qx,NDimI(KPhase))
              call multm(GammaIntInv,qx,xp,NDimI(KPhase),NDimI(KPhase),
     1                   1)
              if(TypeModFun(k).le.1.or.KModA(1,k).le.1) then
                call MakeOccMod(ok,qcnt(1,k),xp,nti,dttnat,a0(k),
     1            ax(1,k),ay(1,k),KModA(1,k),KFA(1,k),GammaIntInv)
                if(KFA(2,k).gt.0.and.KModA(2,k).gt.0)
     1            call MakeOccModSawTooth(ok,qcnt(1,k),xp,nti,dttnat,
     2              uyk(1,KModA(2,k)),uyk(2,KModA(2,k)),KFA(2,k),
     3              GammaIntInv)
              else
                call MakeOccModPol(ok,qcnt(1,k),xp,nti,dttnat,
     1            ax(1,k),ay(1,k),KModA(1,k)-1,
     2            ax(KModA(1,k),k),a0(k)*.5,GammaIntInv,
     3             TypeModFun(k))
              endif
              if(TypeModFun(k).le.1) then
                call MakePosMod(xdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                          KModA(2,k),KFA(2,k),GammaIntInv)
              else
                call MakePosModPol(xdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                            KModA(2,k),ax(1,k),a0(k)*.5,
     2                            GammaIntInv,TypeModFun(k))
              endif
              call MakePosModS(sxdk,qcnt(1,k),xp,nti,dttnat,uxk,uyk,
     1                         suxk,suyk,KModA(2,k),KFA(2,k),
     2                         GammaIntInv)
              dammx=-9999.
              dammn= 9999.
              dhmmx=-9999.
              dhmmn= 9999.
              damav=0.
              sdamav=0.
              ddmmn= 9999.
              call CopyVek(z3p(4),z340,NDimI(KPhase))
              npom=0
              do kt=1,ntall(iswi)
                call RecUnpack(kt,nd,nti,NDimI(KPhase))
                do l=1,NDimI(KPhase)
                  t(l)=tfirsti(l)+(nd(l)-1)*dti(l)
                enddo
                if(oi(kt).le.ocut.or.oj(kt).le.ocut.or.ok(kt).le.ocut)
     1            cycle
                npom=npom+1
                if(iswk.eq.iswi) then
                  do l=1,3
                    vm(l)=vp(l)+xdk(l,kt)-xdi(l,kt)
                  enddo
                else
                  call multm(TNaTI,t,tj,NDimI(KPhase),NDimI(KPhase),1)
                  do l=4,NDim(KPhase)
                    z3p(l)=z340(l-3)+tj(l-3)
                  enddo
                  call multm(WPr,z3p,zp,NDim(KPhase),NDim(KPhase),1)
                  do l=1,3
                    zp(l)=zp(l)-x1p(l)
                  enddo
                  call multm(MetTens(1,iswi,KPhase),zp,zpp,3,3,1)
                  dumm(kt)=sqrt(scalmul(zp,zpp))
                  do l=1,3
                    zp(l)=zp(l)-xdi(l,kt)
                    zpp(l)=xdj(l,kt)
                  enddo
                  call qbyx(zpp,zpp(4),iswj)
                  call cultm(WPr,zpp,zp,NDim(KPhase),NDim(KPhase),1)
                  call CopyVek(zp,vm,3)
                endif
                call multm(MetTens(1,iswi,KPhase),vm,vmg,3,3,1)
                call multm(MetTens(1,iswi,KPhase),um(1,kt),umg,3,3,1)
                if(EqRvLT0(sgx,3)) then
                  call CopyVek(sxdi(1,kt),sgxm,3)
                else
                  call AddVekQ(sgx,sxdi(1,kt),sgxm,3)
                endif
                if(EqRvLT0(sgx(4),3)) then
                  call CopyVek(sxdj(1,kt),sgxm(4),3)
                else
                  call AddVekQ(sgx(4),sxdj(1,kt),sgxm(4),3)
                endif
                if(EqRvLT0(sgx(7),3)) then
                  call CopyVek(sxdk(1,kt),sgxm(7),3)
                else
                  call AddVekQ(sgx(7),sxdk(1,kt),sgxm(7),3)
                endif
                if(CalcHBonds) then
                  call DistHBond(um(1,kt),umg,vm,vmg,sgxm,
     1              MetTensS(1,iswi,KPhase),dhm(1,kt),dhms(1,kt),
     2              dhm(2,kt),dhms(2,kt),dhm(3,kt),dhms(3,kt),dhm(4,kt),
     3              dhms(4,kt))
                  if(dhm(1,kt).gt.dammx) then
                     dammx=dhm(1,kt)
                    sdammx=dhms(1,kt)
                  endif
                  if(dhm(1,kt).lt.dammn) then
                     dammn=dhm(1,kt)
                    sdammn=dhms(1,kt)
                  endif
                  if(dhm(2,kt).gt.dhmmx) then
                     dhmmx=dhm(2,kt)
                    sdhmmx=dhms(2,kt)
                  endif
                  if(dhm(2,kt).lt.dhmmn) then
                     dhmmn=dhm(2,kt)
                    sdhmmn=dhms(2,kt)
                  endif
                else
                  vv=scalmul(vm,vmg)
                  if(vv.lt.ddmmn) ddmmn=vv
                  call DistAng(um(1,kt),umg,vm,vmg,dum(kt)**2,vv,sgxm,
     1                         MetTensS(1,iswj,KPhase),pom,spom)
                  if(spom.lt..0001.or.
     1               EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.
     2               EqRvLT0(sgx(7),3)) spom=0.
                  dam (kt)= pom
                  dams(kt)=spom
                  if(pom.gt.dammx) then
                     dammx= pom
                    sdammx=spom
                  endif
                  if(pom.lt.dammn) then
                     dammn= pom
                    sdammn=spom
                  endif
                   damav= damav+ pom
                  sdamav=sdamav+spom
                  write(78) sp2,atom(k),kt,kt,0.,pom,spom,oi(kt),
     1                      oj(kt),ok(kt),0.,t80,SymmCIF(3),1
                endif
              enddo
              if(CalcHBonds) then
                if(npom.le.0.or.dammn.gt.DHMax.or.
     1             dhmmn.gt.HAMax) then
                  cycle
                else
                  call NewLn(1)
                  write(lst,FormA) StHBonds(:idel(StHBonds))
                endif
              else
                if(npom.gt.0.and.sqrt(ddmmn).le.dmhikm) then
                  if(VolaToDist.and.LstType.eq.1)
     1              call DistM61AngleHeader(Atom(j),Atom(i),Atom(k),
     2                                      angle,su,t80)
                  if(iswi.eq.iswk) then
                    pom=1./float(npom)
                     damav= damav*pom
                    sdamav=sdamav*pom
                    if(VolaToDist) then
                      if(LstType.eq.0) then
                        call fflush
                        line=line+1
                      endif
                      call DistRoundESD(t15,damav,sdamav,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  ave        '//t15(:l)
                        call fflush
                        line=line+1
                      else if(LstType.eq.1) then
                        s80='       ave : '//t15(:l)
                      endif
                      call DistRoundESD(t15,dammn,sdammn,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  min        '//t15(:l)
                        call fflush
                        line=line+1
                      else if(LstType.eq.1) then
                        s80=s80(:idel(s80))//'  min : '//t15(:l)
                      endif
                      call DistRoundESD(t15,dammx,sdammx,2)
                      l=idel(t15)
                      if(LstType.eq.0) then
                        iven(line)='  max        '//t15(:l)
                        call fflush
                      else if(LstType.eq.1) then
                        s80=s80(:idel(s80))//'  max : '//t15(:l)
                      endif
                    endif
                    if(.not.CalcHBonds) then
                      write(78) '#5',atom(k),isf(k),iswk,aik,umod,
     1                          t80,SymmCIF(3),1
                      if(VolaToDist.and.LstType.eq.1) then
                        call newln(1)
                        write(lst,FormA) s80(:idel(s80))
                      endif
                    endif
                  endif
                  if(LstType.eq.0.and.VolaToDist) call fflush
                else
                  if(LstType.eq.0) line=max(line-1,0)
                  cycle
                endif
              endif
              if(.not.VolaToDist) cycle
              if(ieach.ne.0) then
                l=0
                do kt=1,ntall(iswi),ieach
                  call RecUnpack(kt,nd,nti,NDimI(KPhase))
                  do m=1,NDimI(KPhase)
                    t(m)=tfirsti(m)+(nd(m)-1)*dti(m)
                  enddo
                  l=l+1
                  if(oi(kt).gt.ocut.and.oj(kt).gt.ocut.and.
     1               ok(kt).gt.ocut) then
                    if(LstType.eq.0) then
                       pom=dam(kt)
                      spom=dams(kt)
                      call fflush
                      line=line+1
                      write(iven(line),'(3x,f5.3)') t(1)
                      call DistRoundESD(t15,pom,spom,2)
                      iven(line)=iven(line)(:13)//
     1                  t15(:idel(t15))
                      if(oi(kt).le.oind.or.oj(kt).le.oind
     1                                 .or.ok(kt).le.oind)
     2                  iven(line)(2:2)='*'
                      call fflush
                    else if(LstType.eq.1) then

                    else if(LstType.eq.99) then
                      call NewLn(1)
                      do ii=1,4
                        call DistRoundESD(ValH(ii),
     1                                    dhm(ii,kt),dhms(ii,kt),2)
                      enddo
                      write(Cislo,'(''t='',f9.3)') t(1)
                      call Zhusti(Cislo)
                      write(lst,'(45x,5a15)') Cislo,ValH
                    endif
                  endif
                enddo
                if(LstType.eq.99) then
                  call NewLn(1)
                  write(lst,FormA)
                endif
              endif
              if(LstType.eq.0) then
                call fflush
                line=line+1
                write(iven(line),'(26(''-''))')
                call fflush
              endif
            enddo
7100      continue
        enddo
4000  enddo
9999  if(allocated(vt6p)) deallocate(vt6p)
      return
      end
      subroutine DistHBond(u,ug,v,vg,sgx,MetTensP,du,sdu,
     1                     dv,sdv,dw,sdw,angle,sangle)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension u(3),ug(3),v(3),vg(3),sgx(9),ag(3,3),ddx(9),ddg(6),
     1          sgxp(6),w(3),wg(3)
      logical EqRvLT0
      real MetTensP(9)
      uu=scalmul(u,ug)
      du=sqrt(uu)
      do l=1,3
        if(du.ne.0.) then
          ddx(l+3)=ug(l)/du
          ddg(l)=u(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      sdu=sigma(sgx,ddx,6)
      if(sdu.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3)) then
        sdu=0.
        go to 1100
      endif
      gdu=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdu=gdu+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(du.gt.0.) then
        gdu=sqrt(gdu)/du
      else
        gdu=0.
      endif
      sdu=sqrt(sdu**2+gdu**2)
1100  vv=scalmul(v,vg)
      dv=sqrt(vv)
      do l=1,3
        if(dv.ne.0.) then
          ddx(l+3)=vg(l)/dv
          ddg(l)=v(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      call CopyVek(sgx,sgxp,3)
      call CopyVek(sgx(7),sgxp(4),3)
      sdv=sigma(sgxp,ddx,6)
      if(sdv.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(7),3)) then
        sdv=0.
        go to 1200
      endif
      gdv=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdv=gdv+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(dv.gt.0.) then
        gdv=sqrt(gdv)/dv
      else
        gdv=0.
      endif
      sdv=sqrt(sdv**2+gdv**2)
1200  do i=1,3
        w(i)=v(i)-u(i)
        wg(i)=vg(i)-ug(i)
      enddo
      ww=scalmul(w,wg)
      dw=sqrt(ww)
      do l=1,3
        if(dw.ne.0.) then
          ddx(l+3)=wg(l)/dw
          ddg(l)=w(l)**2
        else
          ddx(l+3)=0.
          ddg(l)=0.
        endif
        ddx(l)=-ddx(l+3)
      enddo
      call CopyVek(sgx(4),sgxp,3)
      call CopyVek(sgx(7),sgxp(4),3)
      sdw=sigma(sgxp,ddx,6)
      if(sdw.lt..00001.or.
     1   EqRvLT0(sgx(4),3).or.EqRvLT0(sgx(7),3)) then
       sdw=0.
       go to 1300
      endif
      gdw=0.
      m=0
      do l=1,3
        do k=1,3
          m=m+1
          gdw=gdw+MetTensP(m)*ddg(l)*ddg(k)
        enddo
      enddo
      if(dw.gt.0.) then
        gdw=sqrt(gdw)/dw
      else
        gdw=0.
      endif
      sdw=sqrt(sdw**2+gdw**2)
1300  angle=0.
      sangle=0.
      uv=scalmul(u,vg)
      denq=uu*vv
      den=sqrt(denq)
      if(abs(den).lt..0001) go to 9999
      arg=uv/den
      if(abs(arg).gt..999999) then
        angle=90.-sign(90.,arg)
        go to 9999
      endif
      if(abs(arg).lt..0001) then
        angle=90.
        go to 9999
      endif
      denp=1./(sqrt(1.-arg**2)*torad)
      pom=1./(den*(sqrt(1.-arg**2)*torad))
      angle=acos(arg)/torad
      do l=1,3
        dtul=pom*(vg(l)-ug(l)*uv/uu)
        dtvl=pom*(ug(l)-vg(l)*uv/vv)
        ddx(l)=-dtul-dtvl
        ddx(l+3)=dtul
        ddx(l+6)=dtvl
      enddo
      sangle=sigma(sgx,ddx,9)
      if(sangle.lt..00001.or.
     1   EqRvLT0(sgx,3).or.EqRvLT0(sgx(4),3).or.EqRvLT0(sgx(4),3)) then
        sangle=0.
        go to 9999
      endif
      do i=1,3
        do j=1,3
          ag(i,j)=1./denq*(u(i)*v(j)
     1                     -.5*uv/denq*(u(i)*u(j)*vv+v(i)*v(j)*uu))**2
        enddo
      enddo
      gu=0.
      m=0
      do i=1,3
        do j=1,3
          m=m+1
          gu=gu+ag(i,j)*MetTensP(m)
        enddo
      enddo
      gu=denp*sqrt(gu)
      sangle=sqrt(sangle**2+gu**2)
9999  return
      end
      subroutine DistM61DistHeader(Atom1,Atom2,Value,ESD,Symmetry)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*15  t15
      character*(*) Atom1,Atom2,Atom3,Symmetry
      Klic=0
      i=4
      go to 1000
      entry DistM61AngleHeader(Atom1,Atom2,Atom3,Value,ESD,Symmetry)
      Klic=1
      i=3
1000  call RoundESD(t15,Value,ESD,i,0,0)
      call zhusti(t15)
      t256=Atom1(:idel(Atom1))//'-'//Atom2(:idel(Atom2))
      if(Klic.eq.1) t256='     '//t256(:idel(t256))//'-'//
     1                            Atom3(:idel(Atom3))
      if(Klic.eq.0) then
        i=19
      else
        i=32
      endif
      t256(i:)=t15(:idel(t15))
      if(Klic.eq.0) then
        i=i+28
        t256(i:)=' 2nd: '//Atom2(:idel(Atom2))
      else
        i=i+15
        t256(i:)=' 3rd: '//Atom3(:idel(Atom3))
      endif
      j=index(Symmetry,'#')
      k=idel(Symmetry)
      if(k.gt.j) t256=t256(:idel(t256))//'#'//
     1                Symmetry(j+1:idel(Symmetry))
      i=i+30
      t256(i:)=Symmetry(:j-1)
      call newln(1)
      write(lst,FormA) t256(:idel(t256))
      return
      end
      subroutine DistBudeBreak
      include 'fepc.cmn'
      include 'basic.cmn'
      logical FeYesNo
      if(FeYesNo(-1.,YBottomMessage,'Do you really want to cancel the'//
     1           ' distance calculation?',0)) then
        call DeleteFile(fln(:ifln)//'.m61')
        call FeFlowChartRemove
        ErrFlag=1
      endif
9999  return
      end
      subroutine outst
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      n=(mxl-1)/4+1
      do i=1,n
       write(lst,'(4a32)')(iven(i+(j-1)*n),j=1,4)
      enddo
      call SetStringArrayTo(iven,line,' ')
      return
      end
      subroutine fflush
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      if(line.eq.mxl) then
        call outst
        line=mxline
        call newpg(0)
        line=0
        mxl=mxl4
      endif
      return
      end
      subroutine ScodeCIF(jsym,jcenter,jsh,isw,t10)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension jsh(3),jshp(3)
      character*(*) t10
      do i=1,3
        jshp(i)=jsh(i)+5
        pom=vt6(i,jcenter,isw,KPhase)+s6(i,jsym,isw,KPhase)
1000    if(pom.gt..999) then
          jshp(i)=jshp(i)+1
          pom=pom-1.
          go to 1000
        endif
1100    if(pom.lt.-.999) then
          jshp(i)=jshp(i)+1
          pom=pom+1.
          go to 1100
        endif
      enddo
      write(t10,'(i3,''_'',6i1)') (jcenter-1)*ncvt+jsym,jshp
      do i=1,NDimI(KPhase)
        t10=t10(:idel(t10))//'5'
      enddo
      call zhusti(t10)
      if(t10(1:5).eq.'1_555') t10='.'
      return
      end
      subroutine DistSetCommands
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      real, allocatable :: sngc(:,:,:),csgc(:,:,:)
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*80 TmpFile
      external DistSetCommandsWatch,FeVoid
      data LastListek/1/
      save /DistSelAtoms/
      call RewriteTitle('Dist-commands')
      Klic=0
      call ComSym(0,0,ich)
      if(ngc(KPhase).gt.0) then
        allocate(sngc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc),
     1           csgc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc))
        call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),
     1              NAtCalc)
        deallocate(sngc,csgc)
        if(ErrFlag.ne.0) go to 9999
      endif
      n=NAtCalc+5
      allocate(xdst(3,n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call SetLogicalArrayTo(BratPrvni,n,.true.)
      call SetLogicalArrayTo(BratDruhyATreti,n,.true.)
      go to 1000
      entry DistSetCommandsFromGrapht
      Klic=1
1000  call DistOpenCommands
      if(ErrFlag.ne.0) go to 9999
      TmpFile='jlst'
      call CreateTmpFile(TmpFile,i,0)
      call FeTmpFilesAdd(TmpFile)
      call OpenFile(lst,TmpFile,'formatted','unknown')
      call CopyVek(x,xdst,3*NAtCalc)
      call CopyVek(beta,BetaDst,6*NAtCalc)
      call SetRealArrayTo(dxm,NAtCalc,0.)
      call pisat
      call CloseListing
      call DeleteFile(TmpFile)
      call FeTmpFilesClear(TmpFile)
      if(Klic.eq.1) then
        call DistBondval
        go to 5000
      endif
      XdQuestDist=550.
      call FeKartCreate(-1.,-1.,XdQuestDist,19,'Distance commands',0,0)
      call FeCreateListek('Basic',1)
      KartIdBasic=KartLastId
      call DistBasicCommandsMake(KartIdBasic)
      call FeCreateListek('Select atoms',1)
      KartIdSelect=KartLastId
      call DistSelectAtomsMake(KartIdSelect)
      call FeCreateListek('Modulation',1)
      KartIdModulation=KartLastId
      call DistModulationCommandsMake(KartIdModulation)
      call FeCompleteKart(LastListek)
3000  call FeQuestEventWithCheck(KartId,ich,DistSetCommandsWatch,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsUpdate
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsUpdate
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsCheck
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsCheck
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdBasic) then
          call DistBasicCommandsUpdate
          LastListek=1
        else if(KartId.eq.KartIdSelect) then
          call DistSelectAtomsUpdate
          LastListek=2
        else if(KartId.eq.KartIdModulation) then
          call DistModulationCommandsUpdate
          LastListek=3
        endif
      endif
      call FeDestroyKart
      if(ich.ne.0) go to 9999
5000  call DistRewriteCommands(Klic)
9999  if(Klic.eq.0)
     1  deallocate(xdst,BetaDst,dxm,BratPrvni,BratDruhyATreti,
     2             aselPrvni,aselDruhyATreti)
      call DeleteFile(PreviousM40)
      if(allocated(sngc)) deallocate(sngc,csgc)
      return
100   format(3f15.6)
      end
      subroutine DistSetCommandsWatch
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      save /DistSelAtoms/
      if(KartId.ne.KartIdSelect) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
9999  return
      end
      subroutine DistOpenCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) return
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultDist
      NacetlReal(nCmddmhk)=-1.
      call NactiDist
      call DeleteFile(fln(:ifln)//'_torpl.tmp')
      do i=nCmdTorsion,nCmdDirRef
        call MakeKeyWordFile(NactiKeywords(i)(:idel(NactiKeywords(i))),
     1                       'dist',NactiActive(i),NactiPassive(i))
      enddo
9999  return
      end
      subroutine DistRewriteCommands(Klic)
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*80 t80
      call WriteKeys('dist')
      if(NDimI(KPhase).gt.0) then
        if(NDimI(KPhase).eq.1) then
          k=100
        else
          k=20
        endif
        do i=1,NDimI(KPhase)
          if(nt(i,1).ne.k) then
            write(t80,'(3i10)')(nt(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdnooft),
     1                              t80,k)
            go to 1130
          endif
        enddo
1130    do i=1,NDimI(KPhase)
          if(tfirst(i,1).ne.0.) then
            write(t80,100)(tfirst(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdtfirst),
     1                              t80,k)
            go to 1140
          endif
        enddo
1140    do i=1,NDimI(KPhase)
          if(tlast(i,1).ne.1.) then
            write(t80,100)(tlast(j,1),j=1,NDimI(KPhase))
            k=0
            call WriteLabeledRecord(55,'  '//NactiKeywords(nCmdtlast),
     1                              t80,k)
            go to 1150
          endif
        enddo
      endif
1150  do i=1,NAtCalcBasic
        if(.not.BratPrvni(i).and.kswa(i).eq.KPhase) then
          t80='  '//IdDist(nCmdSelfirst)
          do j=1,NAtCalcBasic
            if(BratPrvni(j).and.kswa(j).eq.KPhase) then
              if(idel(t80).gt.70) then
                write(55,FormA) t80(:idel(t80))
                t80='  '//IdDist(nCmdSelfirst)
              endif
              t80=t80(:idel(t80))//' '//atom(j)
            endif
          enddo
          if(idel(t80).gt.idel(IdDist(nCmdSelfirst)))
     1      write(55,FormA) t80(:idel(t80))
          go to 1157
        endif
      enddo
1157  do i=1,NAtCalcBasic
        if(.not.BratDruhyATreti(i).and.kswa(i).eq.KPhase) then
          t80='  '//IdDist(nCmdSelsecnd)
          do j=1,NAtCalcBasic
            if(BratDruhyATreti(j).and.kswa(j).eq.KPhase) then
              if(idel(t80).gt.70) then
                write(55,FormA) t80(:idel(t80))
                t80='  '//IdDist(nCmdSelsecnd)
              endif
              t80=t80(:idel(t80))//' '//atom(j)
            endif
          enddo
          if(idel(t80).gt.idel(IdDist(nCmdSelsecnd)))
     1      write(55,FormA) t80(:idel(t80))
          go to 1170
        endif
      enddo
1170  do i=nCmdTorsion,nCmdDirRef
         call MergeFileToM50(NactiKeywords(i))
      enddo
      write(55,'(''end dist'')')
      call DopisKeys(Klic)
      call RewriteTitle(' ')
      return
100   format(3f15.6)
      end
      subroutine DistBasicCommands
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      character*80 Veta
      integer CrwStateQuest
      logical CrwLogicQuest
      save nCrwRound,nCrwAngles,nCrwFullCoor,nEdwDmin,nEdwDmax,
     1     nCrwAtRad,nButtTorsion,nButtPlane,nCrwLstType,nEdwDHAMin,
     2     nCrwFromFourier,nButtBondVal,nEdwDHMax,nEdwHAMax,
     3     nEdwExpand,nLblExpand,nButtDirRef,nEdwHirshLev
      entry DistBasicCommandsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      il=1
      tpom=5.
      Veta='Ro%und input coordinates'
      xpom1=tpom+FeTxLength(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom1,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwRound=CrwLastMade
      tpom=xpom1+70.
      Veta='Calculate:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      tpom=tpom+FeTxLength(Veta)+15.
      Veta='An%gles'
      xpom=tpom+FeTxLength(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwAngles=CrwLastMade
      xpom=xpom+40.
      Veta='%Torsion angles'
      dpom=FeTxLengthUnder(Veta)+40.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtTorsion=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='%Planes'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPlane=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Reference directi%on'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDirRef=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=5.
      Veta='List %full coordination'
      call FeQuestCrwMake(id,tpom,il,xpom1,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwFullCoor=CrwLastMade
      do i=1,3
        if(i.eq.1) then
          j=nCmdirnd
          nCrw=nCrwRound
        else if(i.eq.2) then
          j=nCmdiuhl
          nCrw=nCrwAngles
        else
          j=nCmdfullcoo
          nCrw=nCrwFullCoor
        endif
        call FeQuestCrwOpen(nCrw,NacetlInt(j).eq.1)
      enddo
      tpom=xqd*.5+50.
      dpom=60.
      do i=1,2
        if(i.eq.1) then
          Veta='d(mi%n)'
        else
          Veta='d(ma%x)'
        endif
        il=il+1
        if(i.eq.1) then
          ilp=il
        else
          ilp=-10*il-5
        endif
        if(i.eq.1) xpom=tpom+FeTxLengthUnder(Veta)+12.
        call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwDmin=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmddmdk),
     1                            .false.,.false.)
        else
          nEdwDmax=EdwLastMade
          tpomp=25.
          Veta='d(max) derived from atomic %radii'
          xpomp=tpomp+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpomp,il,xpomp,ilp,Veta,'L',CrwXd,
     1                        CrwYd,1,0)
          nCrwAtRad=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,NacetlReal(nCmddmhk).lt.0.)
          Veta='e%xpanded by'
          tpom=xpom-FeTxLengthUnder(Veta)-5.
          call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,
     1                        0)
          nEdwExpand=EdwLastMade
          tpom=xpom+dpom+5.
          call FeQuestLblMake(id,tpom,ilp,'%','L','N')
          nLblExpand=LblLastMade
          call FeQuestLblOff(nLblExpand)
          ilp=-10*il-7
          Veta='and typical distances'
          call FeQuestLblMake(id,tpomp,ilp,Veta,'L','N')
        endif
      enddo
      xpom=10.
      Veta='Define %coefficients for bond valences'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il-1,dpom,ButYd,Veta)
      nButtBondVal=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      ilp=-10*il-7
      Veta='Report violations of the Hirshfeld''s condition if U(proj)>'
      tpom=5.
      call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
      xpom=tpom+FeTxLength(Veta)+5.
      Veta='*s.u.'
      dpom=60.
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdHirshLev),
     1                        .false.,.false.)
      nEdwHirshLev=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,-10*il-4)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Listing form','C','B')
      dpom=xqd*.5
      xpom=dpom*.5
      il=il+1
      do i=1,2
        if(i.eq.1) then
          Veta='Without %symmetry code'
        else if(i.eq.2) then
          Veta='With s%ymmetry code'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,0,1)
        if(i.eq.1) nCrwLstType=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdLstType).eq.i-1)
        xpom=xpom+dpom
      enddo
      il=il+2
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Include peaks from Fourier '//
     1                    'calculation','C','B')
      dpom=xqd*.25
      xpom=dpom*.5
      il=il+1
      do i=1,4
        if(i.eq.1) then
          Veta='non%e'
        else if(i.eq.2) then
          Veta='m%axima'
        else if(i.eq.3) then
          Veta='m%inima'
        else
          Veta='%both'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,0,2)
        if(i.eq.1) nCrwFromFourier=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdincl).eq.i-1)
        xpom=xpom+dpom
      enddo
      if(NAtH(KPhase).gt.0) then
        il=il+2
        call FeQuestLinkaMake(id,il)
        il=il+1
        call FeQuestLblMake(id,xqd*.5,il,'H-bonds limits','C','B')
        il=il+1
        tpom=5.
        Veta='Maximal distance %D-H:'
        pom=FeTxLengthUnder(Veta)+25.
        xpom=tpom+pom
        dpom=60.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDHMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdDHMax),
     1                          .false.,.false.)
        tpom=xpom+dpom+25.
        xpom=tpom+pom
        Veta='Maximal distance %H...A:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwHAMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdHAMax),
     1                          .false.,.false.)
        Veta='%Minimal angle D-H...A:'
        il=il+1
        tpom=5.
        xpom=tpom+pom
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDHAMin=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdDHAMin),
     1                          .false.,.false.)
      else
        nEdwDHMax=0
        nEdwHAMax=0
        nEdwDHAMin=0
      endif
      go to 2000
      entry DistBasicCommandsCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAtRad) then
        if(CrwLogicQuest(nCrwAtRad)) then
          NacetlReal(nCmddmhk)=-1.
          call FeQuestEdwClose(nEdwDmax)
          call FeQuestRealEdwOpen(nEdwExpand,ExpDMax,.false.,.false.)
          call FeQuestLblOn(nLblExpand)
        else
          NacetlReal(nCmddmhk)=2.5
          EventType=EventEdw
          EventNumber=nEdwDmax
          call FeQuestEdwClose(nEdwExpand)
          call FeQuestLblOff(nLblExpand)
          go to 2000
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAngles) then
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTorsion)
     1  then
        call DistTorsion
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPlane)
     1  then
        call DistPlane
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBondVal)
     1  then
        call DistBondVal
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDirRef)
     1  then
        call DistDirRef
      endif
      go to 9999
2000  if(CrwLogicQuest(nCrwAtRad)) then
        call FeQuestRealEdwOpen(nEdwExpand,ExpDMax,.false.,.false.)
        call FeQuestLblOn(nLblExpand)
      else
        call FeQuestRealEdwOpen(nEdwDmax,NacetlReal(nCmddmhk),.false.,
     1                          .false.)
      endif
      if(CrwLogicQuest(nCrwAngles)) then
        call FeQuestCrwClose(nCrwFullCoor)
        NacetlInt(nCmdfullcoo)=1
      else
        if(CrwStateQuest(nCrwFullCoor).eq.CrwClosed)
     1    call FeQuestCrwOpen(nCrwFullCoor,NacetlInt(nCmdfullcoo).eq.1)
      endif
      go to 9999
      entry DistBasicCommandsUpdate
      do i=1,3
        if(i.eq.1) then
          j=nCmdirnd
          nCrw=nCrwRound
        else if(i.eq.2) then
          j=nCmdiuhl
          nCrw=nCrwAngles
        else
          j=nCmdfullcoo
          nCrw=nCrwFullCoor
        endif
        if(CrwStateQuest(nCrw).ne.CrwClosed) then
          if(CrwLogicQuest(nCrw)) then
            nacetlInt(j)=1
          else
            nacetlInt(j)=0
          endif
        endif
      enddo
      nCrw=nCrwLstType
      do i=1,2
        if(CrwLogicQuest(nCrw)) then
          NacetlInt(nCmdLstType)=i-1
          go to 3150
        endif
        nCrw=nCrw+1
      enddo
3150  nCrw=nCrwFromFourier
      do i=1,4
        if(CrwLogicQuest(nCrw)) then
          NacetlInt(nCmdincl)=i-1
          go to 3300
        endif
        nCrw=nCrw+1
      enddo
3300  call FeQuestRealFromEdw(nEdwDMin,NacetlReal(nCmddmdk))
      if(CrwLogicQuest(nCrwAtRad)) then
        call FeQuestRealFromEdw(nEdwExpand,NacetlReal(nCmdExpDMax))
      else
        call FeQuestRealFromEdw(nEdwDMax,NacetlReal(nCmddmhk))
      endif
      call FeQuestRealFromEdw(nEdwHirshLev,NacetlReal(nCmdHirshLev))
      if(nEdwDHMax.gt.0) then
        call FeQuestRealFromEdw(nEdwDHMax,NacetlReal(nCmdDHMax))
        call FeQuestRealFromEdw(nEdwHAMax,NacetlReal(nCmdHAMax))
        call FeQuestRealFromEdw(nEdwDHAMin,NacetlReal(nCmdDHAMin))
      endif
9999  return
      end
      subroutine DistSelectAtoms
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      common/DistSelAtoms/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                     nButtInclTypes,nButtExclTypes,nRolMenuTypes,
     2                     KartIdBasic,KartIdSelect,KartIdModulation
      character*256 EdwStringQuest
      character*80 Veta
      integer SbwLnQuest,SbwItemPointerQuest,SbwItemSelQuest,
     1        RolMenuSelectedQuest
      logical CrwLogicQuest,lpom,PrvniSelected,PrvniSelectedOld,EqWild
      save nCrwSelCenter,nCrwSelNeighbour,nSbwSelect,nButtAll,
     1     nButtRefresh,nCrwSkipSplit,nButtAtMolSplit
      save /DistSelAtoms/
      entry DistSelectAtomsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      il=0
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Select %central atom(s)'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
        if(i.eq.1) then
          nCrwSelCenter=CrwLastMade
          Veta='Select neighbour atom(s)'
        else
          nCrwSelNeighbour=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      xpom=5.
      dpom=xqd-10.
      il=11
      ild=9
      call FeQuestSbwMake(id,xpom,il,dpom,ild,6,CutTextFromRight,
     1                    SbwHorizontal)
      nSbwSelect=SbwLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_lista.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NAtCalcBasic
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
      enddo
      call CloseIfOpened(ln)
      il=il+1
      dpom=80.
      xpom=xqd*.5-dpom-10.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,-10*il-5,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+10.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,-10*il-5,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=5.
      dpom=80.
      tpom=xpom+dpom+10.
      il=il+1
      ilp=-10*(il+1)-5.
      call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,'=>','L',dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      xpom=tpom+30.
      dpom=100.
      Veta='%Include atoms'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtInclAtoms=ButtonLastMade
          Veta='%Exclude atoms'
        else
          nButtExclAtoms=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
      enddo
      il=il-2
      xpom=xqd*.5+5.
      dpom=80.
      tpom=xpom+dpom+10.
      ilp=-10*(il+1)-5
      call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,'=>','L',dpom,EdwYd,
     1                        0)
      nRolMenuTypes=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                        NAtFormula(KPhase),0)
      xpom=tpom+30.
      dpom=100.
      Veta='Include atom %types'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtInclTypes=ButtonLastMade
          Veta='Exclude atom t%ypes'
        else
          nButtExclTypes=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Skip split atoms/molecules'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdSkipSplit).ne.0)
      nCrwSkipSplit=CrwLastMade
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      Veta='%Define/Edit split atoms/molecules'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAtMolSplit=ButtonLastMade
      go to 2500
      entry DistSelectAtomsCheck
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwSelCenter.or.
     2    CheckNumber.eq.nCrwSelNeighbour)) then
        PrvniSelectedOld=.not.CrwLogicQuest(nCrwSelCenter)
        j=SbwItemPointerQuest(nSbwSelect)
        do i=1,NAtCalcBasic
          lpom=SbwItemSelQuest(i,nSbwSelect).eq.1
          if(lpom) j=0
          if(PrvniSelectedOld) then
            BratPrvni(i)=lpom
          else
            BratDruhyATreti(i)=lpom
          endif
        enddo
        if(j.ne.0) then
          if(PrvniSelectedOld) then
            BratPrvni(j)=.true.
          else
            BratDruhyATreti(j)=.true.
          endif
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        if(CrwLogicQuest(nCrwSelCenter)) then
          call SetLogicalArrayTo(BratPrvni,NAtCalcBasic,lpom)
        else
          call SetLogicalArrayTo(BratDruhyATreti,NAtCalcBasic,lpom)
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclAtoms.or.
     2         CheckNumber.eq.nButtExclAtoms)) then
        lpom=CheckNumber.eq.nButtInclAtoms
        Veta=EdwStringQuest(nEdwAtoms)
        if(Veta.eq.' ') go to 9999
        call mala(Veta)
        do i=1,NAtCalcBasic
          if(EqWild(Atom(i),Veta,.false.)) then
            if(CrwLogicQuest(nCrwSelCenter)) then
              BratPrvni(i)=lpom
            else
              BratDruhyATreti(i)=lpom
            endif
          endif
        enddo
        call FeQuestStringEdwOpen(nEdwAtoms,' ')
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclTypes.or.
     2         CheckNumber.eq.nButtExclTypes)) then
        lpom=CheckNumber.eq.nButtInclTypes
        isfp=RolMenuSelectedQuest(nRolMenuTypes)
        do i=1,NAtCalcBasic
          if(isf(i).eq.isfp) then
            if(CrwLogicQuest(nCrwSelCenter)) then
              BratPrvni(i)=lpom
            else
              BratDruhyATreti(i)=lpom
            endif
          endif
        enddo
        call FeQuestRolMenuOpen(nRolMenuTypes,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSkipSplit)
     1  then
        if(CrwLogicQuest(nCrwSkipSplit)) then
          NacetlInt(nCmdSkipSplit)=1
        else
          NacetlInt(nCmdSkipSplit)=0
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtAtMolSplit) then
        call EditAtMolSplit
      endif
      go to 9999
2500  call CloseIfOpened(SbwLnQuest(nSbwSelect))
      PrvniSelected=CrwLogicQuest(nCrwSelCenter)
      do i=1,NAtCalcBasic
        if(PrvniSelected) then
          lpom=BratPrvni(i)
        else
          lpom=BratDruhyATreti(i)
        endif
        if(lpom) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSelect,j)
      enddo
      call FeQuestSbwSelectOpen(nSbwSelect,fln(:ifln)//'_lista.tmp')
      go to 9999
      entry DistSelectAtomsUpdate
      PrvniSelected=CrwLogicQuest(nCrwSelCenter)
      j=SbwItemPointerQuest(nSbwSelect)
      do i=1,NAtCalcBasic
        lpom=SbwItemSelQuest(i,nSbwSelect).eq.1
        if(lpom) j=0
        if(PrvniSelected) then
          BratPrvni(i)=lpom
        else
          BratDruhyATreti(i)=lpom
        endif
      enddo
      if(j.ne.0) then
        if(PrvniSelected) then
          BratPrvni(j)=.true.
        else
          BratDruhyATreti(j)=.true.
        endif
      endif
9999  return
      end
      subroutine DistModulationCommands
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension xp(3)
      character*80 Veta
      logical CrwLogicQuest
      save nEdwTFirst,nEdwTLast,nEdwTStep,nEdwTPrint,nCrwTPrint,
     1     nCrwTCoor,nEdwOind,nEdwOcut
      entry DistModulationCommandsMake(id)
      xqd=XdQuestDist-2.*KartSidePruh
      if(NDimI(KPhase).le.0) then
        Veta='Not applicable, the structure is not modulated.'
        call FeQuestLblMake(id,xqd*.5,7,Veta,'C','B')
        go to 9999
      endif
      il=1
      nEdwTFirst=0
      nEdwTLast=0
      nEdwNoOfT=0
      nCrwTPrint=0
      dpom=50.
      if(kcommen(KPhase).le.0) then
        tpom=5.
        Veta='Calculate distances %from t='
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        xpomp=xpom
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwTFirst=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,TFirst,NDimI(KPhase),
     1                           .false.,.false.)
        tpom=xpom+dpom+10.
        Veta='t%ill t='
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwTLast=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,TLast,NDimI(KPhase),
     1                           .false.,.false.)
        tpom=xpom+dpom+10.
        Veta='with %step'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        do i=1,NDimI(KPhase)
          if(nt(i,1).le.0) then
            xp(i)=.01
          else
            xp(i)=(tlast(i,1)-tfirst(i,1))/float(nt(i,1))
          endif
        enddo
        call FeQuestRealAEdwOpen(EdwLastMade,xp,NDimI(KPhase),.false.,
     1                           .false.)
        nEdwTStep=EdwLastMade
        il=il+1
        Veta='%Print each'
        tpom=5.
        call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
        tpom=xpomp+dpom+10.
        Veta='th value'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nEdwTPrint=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdieach),.false.)
      else
        tpom=5.
        Veta='Print %individual distances'
        xpom=tpom+FeTxLengthUnder(Veta)+25.
        xpomp=xpom
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdieach).gt.0)
        nCrwTPrint=CrwLastMade
      endif
      il=il+1
      tpom=5.
      Veta='Make coordination t-%tables'
      call FeQuestCrwMake(id,tpom,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwTCoor=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdittab).eq.1)
      il=il+1
      tpom=5.
      Veta='%Calculate if occupancies of all relevant atoms are '//
     1     'larger then:'
      xpom=tpom+FeTxLengthUnder(Veta)+50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOcut=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdocut),.false.,
     1                        .false.)
      il=il+1
      Veta='Indicate by "%*" if occupancy of one of relevant atoms '//
     1     'is smaller then:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOind=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdoind),.false.,
     1                        .false.)
      go to 9999
      entry DistModulationCommandsCheck
      go to 9999
      entry DistModulationCommandsUpdate
      if(NDimI(KPhase).le.0) go to 9999
      if(CrwLogicQuest(nCrwTCoor)) then
        NacetlInt(nCmdittab)=1
      else
        NacetlInt(nCmdittab)=0
      endif
      if(kcommen(KPhase).le.0) then
        call FeQuestRealAFromEdw(nEdwTFirst,tfirst)
        call FeQuestRealAFromEdw(nEdwTLast ,tlast)
        call FeQuestRealAFromEdw(nEdwTStep ,xp)
        do i=1,NDimI(KPhase)
          nt(i,1)=nint((TLast(i,1)-TFirst(i,1))/xp(i))
        enddo
        call FeQuestIntFromEdw(nEdwTPrint,NacetlInt(nCmdieach))
      else
        if(CrwLogicQuest(nCrwTPrint)) then
          NacetlInt(nCmdieach)=1
        else
          NacetlInt(nCmdieach)=0
        endif
      endif
      call FeQuestRealFromEdw(nEdwOcut,NacetlReal(nCmdocut))
      call FeQuestRealFromEdw(nEdwOind,NacetlReal(nCmdoind))
9999  return
      end
      subroutine DistTorsion
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*80 AtNames
      character*4  t4
      character*2  nty
      integer WhatHappened
      external DistTorsionReadCommand,DistTorsionWriteCommand,FeVoid
      save /TorsionQuest/
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_torsion.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      dpom=80.
      tpom=xpom+dpom*.5
      do i=1,4
        write(t4,'(''%'',i1,a2)') i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il+1,t4//' atom','C',
     1                      dpom,EdwYd,0)
        nEdw=EdwLastMade
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        endif
        xpom=xpom+dpom+20.
        tpom=tpom+dpom+20.
      enddo
1300  call SetStringArrayTo(AtNames,4,' ')
1350  nEdw=nEdwFirst
      do i=1,4
        call FeQuestStringEdwOpen(nEdw,AtNames(i))
        nEdw=nEdw+1
      enddo
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistTorsionReadCommand,DistTorsionWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
      subroutine DistTorsionReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*80 t80,AtNames
      character*(*) Command
      save /TorsionQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'torsion'.and.t80.ne.'!torsion') go to 8010
      do i=1,4
        if(k.ge.lenc) go to 8000
        call kus(Command,k,AtNames(i))
        call TestAtomString(AtNames(i),IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,t80)
        if(t80.ne.' ') go to 8100
      enddo
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
      subroutine DistTorsionWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TorsionQuest/ nEdwFirst,AtNames(4)
      character*(*) Command
      character*256 EdwStringQuest
      character*80 AtNames,ErrString
      save /TorsionQuest/
      ich=0
      Command='torsion'
      nEdw=nEdwFirst
      do i=1,4
        AtNames(i)=EdwStringQuest(nEdw)
        if(AtNames(i).eq.' ') go to 9100
        call TestAtomString(AtNames(i),IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nEdw=nEdw+1
      enddo
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
      subroutine DistPlane
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*256 AtPlane,AtMore
      integer WhatHappened
      external DistPlaneReadCommand,DistPlaneWriteCommand,FeVoid
      save /PlaneQuest/
      xqd=400.
      il=4
      call RepeatCommandsProlog(id,fln(:ifln)//'_plane.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-2.*xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Atoms defining the '//
     1                    'best %plane','C',dpom,EdwYd,0)
      nEdwPlane=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      il=il+2
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'%Additional atoms '//
     1                    'for calculating distanes from the plane',
     2                    'C',dpom,EdwYd,0)
      nEdwMore=EdwLastMade
1300  AtPlane=' '
      AtMore =' '
1350  call FeQuestStringEdwOpen(nEdwPlane,AtPlane)
      call FeQuestStringEdwOpen(nEdwMore ,AtMore )
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistPlaneReadCommand,DistPlaneWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
      subroutine DistPlaneReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*256 AtPlane,AtMore
      character*80  t80
      character*(*) Command
      save /PlaneQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'plane'.and.t80.ne.'!plane') go to 8010
      if(k.ge.lenc) go to 8000
      i=index(Command,'|')
      if(i.le.0) then
        AtPlane=Command(k+1:)
        AtMore=' '
      else
        AtPlane=Command(k+1:i-1)
        AtMore=Command(i+1:)
      endif
      call TestAtomString(AtPlane,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      call TestAtomString(AtMore ,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
      subroutine DistPlaneWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/PlaneQuest/ nEdwPlane,nEdwMore,AtPlane,AtMore
      character*(*) Command
      character*256 EdwStringQuest,AtPlane,AtMore
      character*80 ErrString
      save /PlaneQuest/
      ich=0
      Command='plane'
      AtPlane=EdwStringQuest(nEdwPlane)
      call TestAtomString(AtPlane,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,ErrString)
      if(ErrString.ne.' ') go to 9000
      if(AtPlane.eq.' ') then
        ErrString='no atoms defined'
        go to 9000
      endif
      Command=Command(:idel(Command)+1)//AtPlane(:idel(AtPlane))
      AtMore=EdwStringQuest(nEdwMore)
      if(AtMore.ne.' ') then
        Command=Command(:idel(Command))//' | '//AtMore(:idel(AtMore))
        call TestAtomString(AtMore,IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
      endif
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      ich=1
9999  return
      end
      subroutine DistDirRef
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      character*256 StDirRef
      integer WhatHappened
      external DistDirRefReadCommand,DistDirRefWriteCommand,FeVoid
      save /DirRefQuest/
      xqd=400.
      il=3
      call RepeatCommandsProlog(id,fln(:ifln)//'_dirref.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-2.*xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Reference direction '//
     1                    'interpretation of best planes',
     1                    'C',dpom,EdwYd,0)
      nEdwDirRef=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
1300  StDirRef=' '
1350  call FeQuestStringEdwOpen(nEdwDirRef,StDirRef)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistDirRefReadCommand,DistDirRefWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
      subroutine DistDirRefReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      dimension xp(3)
      character*256 StDirRef
      character*80  t80
      character*(*) Command
      save /DirRefQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'dirref'.and.t80.ne.'!dirref') go to 8010
      if(k.ge.lenc) go to 8000
      StDirRef=Command(k+1:)
      call StToReal(Command,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
      subroutine DistDirRefWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DirRefQuest/ nEdwDirRef,StDirRef
      dimension xp(3)
      character*256 EdwStringQuest,StDirRef
      character*(*) Command
      character*80 ErrString
      save /DirRefQuest/
      ich=0
      Command='dirref'
      StDirRef=EdwStringQuest(nEdwDirRef)
      if(StDirRef.eq.' ') then
        ErrString='Reference direction not defined'
        go to 9000
      endif
      k=0
      call StToReal(StDirRef,k,xp,3,.false.,ich)
      if(ich.ne.0) then
        ErrString='Numnerical problem in the reference direction'
        go to 9000
      endif
      Command=Command(:idel(Command)+1)//StDirRef(:idel(StDirRef))
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      ich=1
9999  return
      end
      subroutine DistBondVal
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      dimension ip(2),xp(2)
      character*256 t256
      character*80  t80,BVFromFile(10)
      character*2   nty,AtNames,At(2)
      integer WhatHappened,RolMenuSelectedQuest,SbwItemPointerQuest
      logical EqIgCase
      external DistBondvalReadCommand,DistBondvalWriteCommand,FeVoid
      save /BondvalQuest/
      equivalence (t80,t256)
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_bondval.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      dpom=40.
      spom=80.
      tpom=30.
      do i=1,4
        if(i.le.2) then
          write(t80,'(''%'',i1,a2,'' atom type'')') i,nty(i)
        else if(i.eq.3) then
          t80='BV-R%ho'
          dpom=60.
          spom=80.
          tpom=15.
        else if(i.eq.4) then
          t80='B%V-B'
        endif
        if(i.le.2) then
          call FeQuestRolMenuMake(id,xpom+tpom,il,xpom,il+1,t80,'C',
     1                            dpom+EdwYd,EdwYd,0)
          if(i.eq.1) nRolMenuFirst=RolMenuLastMade
        else
          call FeQuestEdwMake(id,xpom+tpom,il,xpom,il+1,t80,'C',dpom,
     1                        EdwYd,0)
        endif
        if(i.eq.3) then
          nEdwRho=EdwLastMade
          nEdwRepeatCheck=0
        else if(i.eq.4) then
          nEdwB=EdwLastMade
        endif
        xpom=xpom+spom
      enddo
      il=il+1
      dpom=60.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'F%rom file')
      nButtReadIn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1300  nRolMenu=nRolMenuFirst
      do i=1,2
        call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        nRolMenu=nRolMenu+1
      enddo
      BVRho=2.
      BVB=.37
1350  call FeQuestRealEdwOpen(nEdwRho,BVRho,.false.,.false.)
      call FeQuestRealEdwOpen(nEdwB,BVB,.false.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  DistBondvalReadCommand,DistBondvalWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        nRolMenu=nRolMenuFirst
        do i=1,2
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtNames(i),AtType(j,KPhase))) then
              call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                                NAtFormula(KPhase),j)
              go to 2025
            endif
          enddo
2025      nRolMenu=nRolMenu+1
        enddo
        go to 1350
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReadIn)
     1  then
        nRolMenu=nRolMenuFirst
        do i=1,2
          AtNames(i)=AtType(RolMenuSelectedQuest(nRolMenu),KPhase)
          if(AtNames(i).eq.' ') go to 2000
          call Uprat(AtNames(i))
          nRolMenu=nRolMenu+1
        enddo
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                'bvparm.cif'
        else
          t256=HomeDir(:idel(HomeDir))//'bondval/bvparm.cif'
        endif
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.ne.0) go to 2200
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_bvfile.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 2200
2110    read(ln,FormA,end=2200) t80
        call Mala(t80)
        if(index(t80,'_valence_param_details').le.0) go to 2110
        n=0
2120    read(ln,FormA,end=2130) t80
        if(t80.eq.' ') go to 2120
        if(t80(1:1).eq.'#') go to 2120
        k=0
        do i=1,2
          call Kus(t80,k,At(i))
          call StToInt(t80,k,ip(i),1,.false.,ich)
        enddo
        if( (EqIgCase(At(1),AtNames(1)).and.EqIgCase(At(2),AtNames(2)))
     1  .or.(EqIgCase(At(1),AtNames(2)).and.EqIgCase(At(2),AtNames(1))))
     2    then
          call StToReal(t80,k,xp(1),2,.false.,ich)
          n=n+1
          do i=1,2
            if(i.eq.1) then
              BVFromFile(n)=At(i)
            else
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//At(i)
            endif
            if(ip(i).ne.9) then
              write(Cislo,'(i2)') iabs(ip(i))
              call ZdrcniCisla(Cislo,1)
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//
     1                      Cislo(:idel(Cislo))
              if(ip(i).gt.0) then
                BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'+'
              else
                BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'-'
              endif
            else
              BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n)))//'..'
            endif
          enddo
          write(Cislo,'(f15.4)') xp(1)
          call ZdrcniCisla(Cislo,1)
          BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//
     1                  ' coefficients: '//Cislo(:idel(Cislo))
          write(Cislo,'(f15.4)') xp(2)
          call ZdrcniCisla(Cislo,1)
          BVFromFile(n)=BVFromFile(n)(:idel(BVFromFile(n))+1)//
     1                  Cislo(:idel(Cislo))
          write(lno,FormA) BVFromFile(n)(:idel(BVFromFile(n)))
        endif
        if(n.lt.10) go to 2120
2130    call CloseIfOpened(ln)
        call CloseIfOpened(lno)
        if(n.eq.0) then
          t80='the combination of atomic types is not on the file'
          go to 2150
        else
          idp=NextQuestId()
          xqd=300.
          call FeQuestCreate(idp,-1.,-1.,xqd,n,'Select the proper '//
     1                       'pair of ions',0,LightGray,0,0)
          xpom=5.
          dpom=xqd-10.-SbwPruhXd
          if(n.eq.1) then
            i=0
          else
            i=SbwVertical
          endif
          call FeQuestSbwMake(idp,xpom,n,dpom,n,1,CutTextFromRight,i)
          nSbw=SbwLastMade
          call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_bvfile.tmp')
2135      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
             call NebylOsetren
             go to 2135
          endif
          if(ich.eq.0) then
            i=SbwItemPointerQuest(nSbw)
            k=0
            call kus(BVFromFile(i),k,Cislo)
            call kus(BVFromFile(i),k,Cislo)
            call kus(BVFromFile(i),k,Cislo)
            call StToReal(BVFromFile(i),k,xp,2,.false.,ich)
          endif
2145      call FeQuestRemove(idp)
          call DeleteFile(fln(:ifln)//'_bvfile.tmp')
          if(ich.ne.0) go to 2200
        endif
        BVRho=xp(1)
        BVB=xp(2)
        go to 2200
2150    call FeChybne(-1.,-1.,t80,' ',SeriousError)
2200    call FeQuestButtonOff(nButtReadIn)
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
      subroutine DistBondvalReadCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      dimension ip(2),xp(2)
      character*80 t80
      character*2  AtNames
      character*(*) Command
      equivalence (ip,xp)
      save /BondvalQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'bondval'.and.t80.ne.'!bondval') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        if(index(Cifry,t80(1:1)).gt.0) then
          kk=0
          call StToInt(t80,kk,ip,1,.false.,ich)
          j=ip(1)
          if(ich.ne.0) go to 8020
          if(j.lt.1.or.j.gt.NAtFormula(KPhase)) then
            t80='the atom type number is out of range'
            go to 8100
          endif
        else
          j=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
          if(j.le.0) then
            t80='the atom type "'//t80(:idel(t80))//'" doesn''t exist'
            go to 8100
          endif
        endif
        AtNames(i)=AtType(j,KPhase)
      enddo
      if(k.ge.lenc) go to 8000
      kk=k
      call StToReal(Command,k,xp,2,.false.,ich)
      if(ich.eq.0) then
        BVRho=xp(1)
        BVB=xp(2)
      else
        call StToReal(Command,kk,xp,1,.false.,ich)
        if(ich.ne.0) go to 8030
        BVRho=xp(1)
        BVB=.37
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8020  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command',
     1              SeriousError)
9999  return
      end
      subroutine DistBondvalWriteCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/BondvalQuest/ nRolMenuFirst,nEdwRho,nEdwB,BVRho,BVB,
     1                     AtNames(2)
      character*(*) Command
      character*80  ErrString
      character*2   AtNames
      integer RolMenuSelectedQuest
      save /BondvalQuest/
      ich=0
      Command='bondval'
      nRolMenu=nRolMenuFirst
      do i=1,2
        j=RolMenuSelectedQuest(nRolMenu)
        if(j.le.0) go to 9100
        AtNames(i)=AtType(j,KPhase)
        call Uprat(AtNames(i))
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nRolMenu=nRolMenu+1
      enddo
      call FeQuestRealFromEdw(nEdwRho,BVRho)
      write(Cislo,100) BVRho
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwB,BVB)
      write(Cislo,100) BVB
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
100   format(f15.4)
      end
      subroutine DistRoundESD(String,Value,SValue,DecimalPoints)
      include 'fepc.cmn'
      character*(*) String
      integer DecimalPoints
      character*8 :: FormPom = ('(f__.__)')
      call RoundESD(String,Value,SValue,6,0,0)
      if(index(String,'(').le.0) then
        if(String.ne.'0') then
          write(FormPom(3:4),'(i2)') DecimalPoints+index(String,'.')
        else
          write(FormPom(3:4),'(i2)') DecimalPoints+3
        endif
        write(FormPom(6:7),'(i2)') DecimalPoints
        write(String,FormPom) Value
        call Zhusti(String)
      endif
      return
      end

