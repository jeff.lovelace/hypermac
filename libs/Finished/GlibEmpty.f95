

!          **************************
!          ***      Okna          ***
!          **************************


      subroutine inic
9999  return
      end
      subroutine FeGrQuit
      return
      end
      subroutine FeWindowTitle(Text)
      character*(*) Text
      return
      end
      subroutine FeMessage(Imm)
9999  return
      end
      subroutine FeMouseShape(n)
      return
      end
      subroutine writememfile
      return
      end



C          ****************************
C          ***      System          ***
C          ****************************


      subroutine FeZacatek
3000  return
      end
      subroutine FeGetFileDirList(Directory)
9999  return
      end
      subroutine FeDirName(CurrentDir)
9999  return
      end
      integer function FeChDir(Directory)
      character*(*) Directory
      FeChDir=0
      return
      end
      subroutine FeMkDir(Veta,ich)
      return
      end
      subroutine FeDirList(List)
      return
      end
      subroutine GetActiveDrives(Drives,n)
      return
      end
      subroutine FeExceptionInfo()
      return
      end
      subroutine GetFln
9999  return
      end
      subroutine FeEdit(File)
      return
      end
      subroutine FeGraphicViewer(FileName,Navrat)
      character*(*) FileName
      entry FeResetGraphicProgram
      return
      end
      subroutine FeSetGraphicProgram
9999  return
      end
      logical function FeGetStringFromReg(RegSubKey,ValueName,String)
      character*(*) RegSubKey,ValueName,String
      FeGetStringFromReg=.false.
      return
      end
      subroutine FeOsVariable(VariableName,Variable)
      return
      end
      subroutine CreateTmpFile(FileName,LFileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      character*4 Frmt
      logical exist
      FileName=TmpDir(:idel(TmpDir))//FileName(:idel(FileName))
      if(klic.eq.0) then
        FileName=FileName(:idel(FileName))//'0000.tmp'
      else if(klic.eq.1) then
        FileName=FileName(:idel(FileName))//'0000.bmp'
      else if(klic.eq.2) then
        FileName=FileName(:idel(FileName))//'0000.ps'
      endif
      LFileName=idel(FileName)
      j=LFileName-4
      k=j
      i=0
      Frmt='(i1)'
1200  inquire(file=FileName,exist=exist)
      if(Exist) then
        i=i+1
        if(i.eq.10) then
          k=k-1
          Frmt(3:3)='2'
        else if(i.eq.100) then
          k=k-1
          Frmt(3:3)='3'
        else if(i.eq.1000) then
          k=k-1
          Frmt(3:3)='4'
        endif
        write(FileName(k:j),Frmt) i
        go to 1200
      endif
      return
      end
      subroutine FeDateAndTime(ch9,ch8)
      include 'fepc.cmn'
      character*10 ch10
      character*(*) ch8,ch9
      dimension it(8)
      call Date_and_Time(ch8,ch10,ch9,it)
      ch9=ch8(7:8)//'-'//ch8(5:6)//'-'//ch8(3:4)
      ch8=ch10(1:2)//':'//ch10(3:4)//':'//ch10(5:6)
      if(it(1).lt.0) ch9='???'
      return
      end
      function FeEtime()
      call CPU_Time(CPUTime)
      FeETime=CPUTime
      return
      end
      function FeTimeDiff()
      include 'fepc.cmn'
      logical First
      integer FeGetSystemTime
      save TimeOld
      data first/.true./
      Time=float(FeGetSystemTime())/1000.
      if(First) then
        TimeOld=Time
        First=.false.
      endif
      FeTimeDiff=Time-TimeOld
      TimeOld=Time
      return
      end
      subroutine FeWait(seconds)
      return
      end
      subroutine FeWaitInfo(VetaIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) VetaIn
      return
      end
      integer function FeGetSystemTime()
      include 'fepc.cmn'
      call timer(i)
      FeGetSystemTime=i*10
      return
      end
      subroutine FeMessageBox(Veta2,Veta1)
      return
      end
      subroutine FeCopyFile(File1,File2)
      character*(*) File1,File2
      character*256 Command
      Command='copy '//File1(:idel(File1))//' '//File2(:idel(File2))//
     1        ' > cmd.log'
      call DeleteFile('cmd.log')
      call system(Command)
      return
      end
      subroutine FeAllocErr(Text,i)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text
      NInfo=4
!      call iostat_msg(i,TextInfo(1))
      TextInfo(1)='Stalo se ani nevim co'
      TextInfo(2)=Text
      TextInfo(3)='Please contact Jana authors and send them this '//
     1            'message and'
      TextInfo(4)='files m40, [m41], m50, m90 and m95.'
      call FeInfoOut(-1.,-1.,'ALLOCATION ERROR','L')
      ErrFlag=1
      return
      end      
      subroutine FeWinMessage(St1,St2)
      return
      end
      integer function FeFileSize(FileName)
      include 'fepc.cmn'
      character*(*) FileName
!      inquire(file=FileName,flen=FeFileSize,err=9000)
!      go to 9999
9000  FeFileSize=-1
9999  return
      end
      integer function FeOpenBinaryFile(FileName)
      character*(*) FileName
      ln=NextLogicNumber()
!      open(ln,file=FileName,access='TRANSPARENT',form='BINARY')
      FeOpenBinaryFile=ln
      return
      end
      integer function FeCloseBinaryFile(ln)
      include 'fepc.cmn'
      close(ln)
      FeCloseBinaryFile=0
      return
      end
      integer function FeReadBinaryFile(ln,String,Size)
      include 'fepc.cmn'
      character*1 String(*)
      integer Size
      do i=1,Size
        read(ln,err=9000) String(i)
      enddo
      FeReadBinaryFile=Size
      go to 9999
9000  FeReadBinaryFile=i
9999  return
      end
      integer function FeWriteBinaryFile(ln,String,Size)
      include 'fepc.cmn'
      character*1 String(*)
      integer Size
      write(ln)(String(i),i=1,Size)
      FeWriteBinaryFile=0
      return
      end
      subroutine FeNull()
      include 'fepc.cmn'
      return
      end
      logical function FeReadOnly(FileName)
      character*(*) FileName
      FeReadOnly=.false.
      return
      end
      subroutine FeResetReadOnly(FileName)
      character*(*) FileName
      return
      end
      subroutine FeSystem(Command)
      include 'fepc.cmn'
      character*(*) Command
      return
      end
      subroutine FeSystemCommand(CommandLine,HowToStart)
9999  return
      end
      logical function FeSystemCommandFinished()
      FeSystemCommandFinished=.false.
      return
      end
      subroutine FeShell
      include 'fepc.cmn'
      character*256 t256
      i=index(FirstCommand,'%p')
      if(i.ne.0) then
        t256=FirstCommand(:i-1)//'"'//CurrentDir(:idel(CurrentDir))//'"'
        if(i+1.lt.idel(FirstCommand))
     1    t256=t256(:idel(t256))//FirstCommand(i+2:)
      else
        t256=FirstCommand
      endif
      call FeSystem(t256)
      return
      end
      subroutine FeShellExecute(Veta)
      return
      end


C          ****************************
C          **       Kresleni        ***
C          ****************************


      subroutine FeDrawSegments(n,px1,py1,px2,py2,Color)
      include 'fepc.cmn'
      return
      end
      subroutine FeCircle(x,y,r,Color)
      integer Color
      entry FeCircleOpen(x,y,r,Color)
      return
      end
      subroutine FePoint(x,y,Color)
      integer Color
      entry FePixelPoint(xj,yj,Color)
      return
      end
      subroutine FeArc(x,y,r,sa,aa,Color)
      return
      end
      subroutine FePolyline(n,x,y,Color)
      return
      end
      subroutine FeDottedLine(x,y,ik,Color)
9000  return
      end
      subroutine carka(x,y,ik,Color)
9000  return
      end
      subroutine FeMoveTo(x,y,Color)
      return
      end
      subroutine FeLineTo(x,y,Color)
      return
      end
      subroutine FeLineType(LineTypeIn)
      return
      end
      subroutine FeCharOut(id,xmi,ymi,Text,Justify,Color)
      return
      end
      subroutine FeNormalFont
      entry FeBoldFont
      entry FeUnderlineFont
9999  return
      end
      subroutine FeSetFixFont
      return
      end
      subroutine FeSetPropFont
      return
      end
      subroutine FeGetTextRectangle(x,y,String,Justify,RecType,xmin,
     1                              xmax,ymin,ymax,refx,refy,conx,cony)
      return
      end
      subroutine FeGetPureTextRectangle(x,y,String,Justify,RecType,xmin,
     1                                  xmax,ymin,ymax,refx,refy,conx,
     2                                  cony)
      include 'fepc.cmn'
      character*(*) Justify,String
      integer RecType
      return
      end
      subroutine FeTextOut(hDCP,ix1,ix2,iy1,iy2,Color,String)
      integer hDCP,Color
      character*(*) String
      return
      end
      subroutine FeFillRectangle(xmin,xmax,ymin,ymax,istyle,idense,
     1                           iangle,color)
      include 'fepc.cmn'
      integer color
      return
      end
      subroutine FePolygon(x,y,n,istyle,idense,iangle,color)
      include 'fepc.cmn'
      dimension x(n),y(n)
      integer Color
      return
      end
      subroutine FeFillPolygon(hDCP,x,y,n,Color)
      integer hDCP,Color
      integer x(*),y(*)
      return
      end
      subroutine FePolylineVarious(hDCP,x,y,n,Color,Klic)
      integer hDCP,Color
      integer x(*),y(*)
      return
      end
      subroutine FeMoveMouseTo(X,Y)
      return
      end
      subroutine FePlotMode(Style)
      character*(*) Style
      return
      end


C          ****************************
C          ***    Pomocne funkce    ***
C          ****************************


      subroutine JanaToClient(xj,yj,xc,yc)
      include 'fepc.cmn'
      real xj,yj
      integer xc,yc
      Klic=0
      go to 1000
      entry JanaToClientReverse(xj,yj,xc,yc)
      Klic=2
      go to 1000
      entry JanaToScreen(xj,yj,xs,ys)
      Klic=1
1000  return
      end
      subroutine ClientToJana(xc,yc,xj,yj)
      include 'fepc.cmn'
      real xj,yj
      integer xc,yc
      go to 1000
      entry ScreenToJana(xj,yj,xs,ys)
1000  return
      end
      subroutine FeScreenToClient(hWndP,sx,sy,cx,cy)
      integer hWndP,cx,cy,sx,sy
      return
      end
      subroutine FeClientToScreen(hWndP,cx,cy,sx,sy)
      integer hWndP,cx,cy,sx,sy
      return
      end
      subroutine JanaPixelToClient(xj,yj,xc,yc)
      include 'fepc.cmn'
      integer xj,yj,xc,yc,xs,ys,x,y
      return
      end
      subroutine JanaPixelToClientReverse(xj,yj,xc,yc)
      include 'fepc.cmn'
      integer xj,yj,xc,yc,xs,ys,x,y
      return
      end
      function FeTxLength(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      FeTxLength=0.
      return
      end
      function FeTxLengthSpace(Veta)
      include 'fepc.cmn'
      character*(*) Veta
	  FeTxLengthSpace=0.
      return
      end
      function FeTxHeight(Veta)
      include 'fepc.cmn'
      character*(*) Veta
	  FeTxHeight=0.
      return
      end
      subroutine FeSaveImage(xlow,xhigh,ylow,yhigh,File)
      include 'fepc.cmn'
      character*(*) File
9999  return
      end
      subroutine FeLoadImage(xlow,xhigh,ylow,yhigh,File,klic)
      include 'fepc.cmn'
      character*(*) File
      entry FeCleanImage(xlow,xhigh,ylow,yhigh,File,klic)
      return
      end
      integer function RGB(Red,Green,Blue)
      integer Red,Green,Blue
      RGB=(Blue*256+Green)*256+Red
      return
      end
      integer function LoWord(n)
      include 'fepc.cmn'
      parameter (i2na16=65536)
      LoWord=mod(n,i2na16)
      return
      end
      integer function HiWord(n)
      include 'fepc.cmn'
      parameter (i2na16=65536)
      HiWord=n/i2na16
      return
      end
      subroutine FeBitmapGet(hBitmap,ix1,iy1,ix2,iy2)
      include 'fepc.cmn'
      integer hBitmap
      return
      end
      subroutine FeBitmapPut(hBitmap,ix1,iy1,ix2,iy2)
      include 'fepc.cmn'
      integer hBitmap
      return
      end
      subroutine FeBitmapDestroy(hBitmap)
      return
      end
      subroutine FeReleaseOutput
      include 'fepc.cmn'
      return
      end
      subroutine FeDeferOutput
      include 'fepc.cmn'
      return
      end
      subroutine FeFlush
      include 'fepc.cmn'
      return
      end
      subroutine FeUpdateDisplay(ix1,ix2,iy1,iy2)
      include 'fepc.cmn'
      return
      end
      subroutine FeGraphicXYFromMessage(lParam,X,Y)
      include 'fepc.cmn'
      integer lParam
      return
      end
      subroutine FeGetClipboardText(Veta,n)
      character(*) Veta
      entry FeGetClipboardLongText(Veta,n)
      return
      end
      subroutine FeSetClipboardText(Veta)
      character(*) Veta
      return
      end
      subroutine FeMakeLowerPriority
      go to 9999
      entry FeReturnOriginalPriority
9999  return
      end
      subroutine FeGetFileTime(FileName,FileDate,FileTimeP)
      character*(*) FileName,FileDate,FileTimeP
9999  return
      end
      subroutine FeGetPSPrinters(PSPrinters,n)
      include 'fepc.cmn'
      character*(*) PSPrinters(20)
      return
      end
      subroutine FePrintFile(PrinterName,FileName,ich)
      include 'fepc.cmn'
      character*(*) FileName,PrinterName
9999  return
      end
      subroutine FeHardCopy(Type,action)
      include 'fepc.cmn'
      character*(*) Action
      integer Type
      return
      end
      subroutine FeSavePicture(WhatToSave,NFormat,Key)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) WhatToSave
      return
      end
      subroutine FeOpenHC(Type,FileName)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer :: Type
      character*(*) FileName
      go to 1100
      entry FeCloseHC(Type,FileName)
1100  ErrFlag=0
9999  return
      end
      subroutine FePrintPicture(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
9999  return
      end
      subroutine fesbwactionspecial(id)
      return
      end
      subroutine FortFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      entry FortFilesCheck
      entry FortFilesClean
      return
      end
      double precision function qexp(x)
      real*16 x
      qexp=exp(x)
      return
      end
