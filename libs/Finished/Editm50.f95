      subroutine EditM50(ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      integer TypTextu
      real CellParOld(6)
      logical eqrv,ExistFile,FeYesNo,M50JesteNeexistovala,Zpet,EqIgCase,
     1        FeYesNoHeader,ChargeDensitiesIn,SymmetryNotChanged,
     2        PrepisM95
      external EM50UpdateListek
      character*256 Veta,t256
      dimension rmo(36),rmp(36),QrPom(3,3)
      ich=0
      KartIdZpet=1
1000  M50JesteNeexistovala=.not.ExistM50
      if(ExistM50) then
        CellParOld(1:6)=CellPar(1:6,1,KPhase)
        KCommenOld=KCommen(KPhase)
        if(StatusM50.lt.11000) call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
        IStSymmOrg=0
        call CrlStoreSymmetry(IStSymmOrg)
        IStSymmOrgComm=0
        if(KCommen(KPhase).gt.0) then
          call ComSym(0,0,ichp)
          call CrlStoreSymmetry(IStSymmOrgComm)
          if(StatusM50.lt.11000) call iom50(0,0,fln(:ifln)//'.m50')
          if(ParentStructure) call SSG2QMag(fln)
        endif
      else
        KCommenOld=0
        call SetBasicM50
        call AllocateSymm(1,1,1,1)
        NSymm(KPhase)=1
        MaxNSymm=1
        BratSymm(1,KPhase)=.true.
        do i=1,NSymm(KPhase)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                  symmc(1,i,1,KPhase),0)
        enddo
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=1
        m=121
        i=1
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4           AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7           FFra(n,NPhase,i),FFia(n,NPhase,i),
     8           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9           TypeFFMag(n,NPhase),
     a           FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1           PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2           ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3           HNSlater(n,NPhase),HZSlater(n,NPhase),
     4           ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5           NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6           CoreValSource(n,NPhase))
        FFCoreD=0.
        FFValD=0.
        CoreValSource='Default'
        FFType(KPhase)=-62
        do i=1,n
          AtType(i,KPhase)='Fe'
          AtTypeMag(i,KPhase)=' '
          AtTypeMagJ(i,KPhase)=' '
          AtMult(i,KPhase)=1.
          AtTypeFull(i,KPhase)=AtType(i,KPhase)
          AtNum(1,KPhase)=1.
          call EM50ReadOneFormFactor(i)
          call EM50OneFormFactorSet(i)
        enddo
      endif
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      if(ExistM40) call EM40AtomTypeSave
      MagneticTypeO=MagneticType(KPhase)
      NDimo=NDim(KPhase)
      maxNDimO=maxNDim
      if(WizardMode) then
        id=NextQuestId()
        QuestColorFill(id)=WhiteGray
        xdq=WizardLength
        il=WizardLines
        nButtOK=0
        nButtESC=0
      else
        xdq=600.
        il=17
        nButtOK=OKForBasicFiles
        nButtESC=0
      endif
      if(ExistM90) then
        AllowChargeDensities=ExistXRayData
      else
        AllowChargeDensities=Radiation(1).eq.XRayRadiation
      endif
      Veta='Define/modify basic structural parameters:'
      if(NPhase.gt.1) Veta=Veta(:idel(Veta))//' for phase : '//
     1                     PhaseName(KPhase)
      call FeKartCreate(-1.,-1.,xdq,il,Veta,nButtESC,nButtOK)
      xdqp=xdq-2.*KartSidePruh
      if(NPhase.gt.1) then
        ypom=35.
        dpom=0.
        do i=1,NPhase
          dpom=max(dpom,FeTxLength(PhaseName(i)))
        enddo
        dpom=dpom+2.*EdwMarginSize+EdwYd
        Veta=' '
        xpom=xdq*.5-dpom*.5
        tpom=xpom
        call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                             Veta,'L',dpom,EdwYd,1)
        nRolMenuPhase=RolMenuLastMade+RolMenuFr-1
        RolMenuAlways(nRolMenuPhase)=.true.
        call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                          KPhase)
      else
        nRolMenuPhase=0
      endif
      ChargeDensitiesIn=ChargeDensities
      call FeCreateListek('Cell',1)
      KartIdCell=KartLastId
      call EM50CellMake(KartIdCell)
      if(NDatBlock.le.1.and..not.ExistM90.and..not.ExistM95) then
        call FeCreateListek('Radiation',1)
        KartIdRadiation=KartLastId
        call EM50RadiationMake(KartIdRadiation)
      else
        KartIdRadiation=0
      endif
      call FeCreateListek('Symmetry',1)
      KartIdSymmetry=KartLastId
      call EM50SymmetryMake(KartIdSymmetry,0)
      call EM50SymmetryRefresh
      call FeCreateListek('Composition',1)
      KartIdComposition=KartLastId
      call EM50CompositionMake(KartIdComposition)
      call FeCreateListek('Multipole parameters',1)
      KartIdMultipols=KartLastId
      call EM50MultipolesMake(KartIdMultipols)
      call FeCreateListek('Magnetic parameters',1)
      KartIdMagnetic=KartLastId
      call EM50MagneticMake(KartIdMagnetic)
      call FeCompleteKart(KartIdZpet)
2500  ErrFlag=0
3000  Zpet=.false.
3010  call FeQuestEventWithKartUpdate(KartId,ich,
     1                                EM50UpdateListek)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(NComp(KPhase).gt.ncompold) then
          call FeChybne(-1.,-1.,'new composite matrices were not yet'//
     1                  ' defined.',' ',SeriousError)
          call FePrepniListek(KartIdCell-1)
          CheckType=EventButton
          CheckNumberAbs=ButtonOk
          CheckNumber=ButtonOk-ButtonFr+1
          EventType=EventButton
          EventNumber=nButtCompMat
          go to 3000
        endif
        if(KCommen(KPhase).gt.0) then
          if(NCommen(1,1,KPhase)*NCommen(2,1,KPhase)*
     1       NCommen(3,1,KPhase).le.0) then
            write(Veta,'(2(i5,''x''),i5)')(NCommen(i,1,KPhase),i=1,3)
            call Zhusti(Veta)
            call FeChybne(-1.,-1.,'unrealistic supercell: '//
     1                    Veta(:idel(Veta)),' ',SeriousError)
            call FePrepniListek(KartIdCell-1)
            CheckType=EventButton
            CheckNumberAbs=ButtonOk
            CheckNumber=ButtonOk-ButtonFr+1
            EventType=EventEdw
            EventNumber=nEdwSupCell
            go to 3000
          endif
        endif
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
          call EM50CellUpdate(ich)
          if(ich.ne.0) go to 3000
        else if(KartId.eq.KartIdRadiation) then
          call EM50RadiationUpDate
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryComplete
        else if(KartId.eq.KartIdComposition) then
          call EM50CompositionUpDate
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesUpDate
        endif
        call EM50CellSymmTest(0,ich)
        if(ich.ne.0) then
          EventType=EventKartSw
          EventNumber=KartIdCell+1-KartFirstId
          ich=0
          go to 3000
        endif
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        if(.not.Zpet) go to 3000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumberAbs.eq.nRolMenuPhase) then
        KPhaseNew=RolMenuSelected(nRolMenuPhase)
        if(KPhaseNew.gt.0.and.KPhaseNew.ne.KPhase) then
          ich=0
          Zpet=.true.
          EventType=EventButton
          EventNumber=ButtonOk-ButtonFr+1
          EventNumberAbs=ButtonOk
          KartIdZpet=KartId-1
          go to 3010
        else
          go to 3000
        endif
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
          call EM50CellUpdate(ich)
          if(ich.ne.0) QuestCheck(KartId)=1
        else if(KartId.eq.KartIdRadiation) then
          call EM50RadiationUpDate
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryComplete
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesUpDate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryCheck
          if(ErrFlag.eq.0) then
            call EM50SymmetryRefresh
          else
            go to 2500
          endif
        else if(KartId.eq.KartIdComposition) then
          call EM50CompositionCheck
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesCheck
        else if(KartId.eq.KartIdMagnetic) then
          call EM50MagneticCheck
        else
          if(KartIdRadiation.gt.0) call EM50RadiationCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        MaxNSymm=max(MaxNSymm,NSymm(KPhase))
        if(allocated(isa)) deallocate(isa)
        allocate(isa(MaxNSymm,MxAtAll))
        if(NUnits(KPhase).le.0)
     1    NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        if(ParentStructure) call QMag2SSG(fln,0)
        if(Zpet) then
          call iom50(1,0,fln(:ifln)//'.m50')
        else
          call QuestionRewriteFile(50)
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
        if(isPowder.and.ExistFile(fln(:ifln)//'.m41')) then
          call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
          if(ParentStructure) then
            call CopyVek(QMag(1,KPhase),QuPwd(1,1,KPhase),3)
          else
            call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),
     1                   3*NDimI(KPhase))
          endif
        endif
        do i=1,MxAtAll
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        if(ExistM40) then
          if(NAtFormula(KPhase).gt.0) call EM40AtomTypeModify
        else
          call SetBasicM40(.false.)
        endif
        if(NDim(KPhase).ne.NDimo.or.m50JesteNeexistovala) then
          if(NDim(KPhase).gt.3) then
            do i=1,mxw
              call SetIntArrayTo(kw(1,i,KPhase),3,0)
              if(i.le.NDimI(KPhase).or.NDim(KPhase).eq.4)
     1          kw(mod(i-1,NDimI(KPhase))+1,i,KPhase)=
     2            (i-1)/NDimI(KPhase)+1
            enddo
          else
            NComp(KPhase)=1
            do i=2,3
              NAtIndFr(i,KPhase)=NAtIndTo(1,KPhase)+1
              NAtIndTo(i,KPhase)=NAtIndTo(1,KPhase)
              NAtIndLen(i,KPhase)=0
              NMolecFr(i,KPhase)=NMolecTo(1,KPhase)+1
              NMolecTo(i,KPhase)=NMolecTo(1,KPhase)
              NMolecLen(i,KPhase)=0
            enddo
          endif
        endif
        if(ChargeDensitiesIn.neqv.ChargeDensities) then
          if(ChargeDensitiesIn) then
            do i=1,NAtInd
              if(kswa(i).eq.KPhase) lasmax(i)=0
            enddo
          endif
        endif
        PrepisM95=.false.
        if(NDimO.ne.NDim(KPhase)) then
          MaxNDim=max(NDim(KPhase),MaxNDim)
          MaxNDimI=MaxNDim-3
          call iom40Only(0,0,fln(:ifln)//'.m40')
          if(ExistM95) then
            call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
            call MatBlock3(TrMPI,rmo,maxNDimO)
            do i=4,MaxNDim
              if(i.gt.MaxNDimO) then
                call MultM(Qu(1,i-3,1,KPhase),rmo,
     1                     QuRefBlock(1,i-3,0),1,3,3)
              endif
            enddo
            call UnitMat(rmo,maxNDim)
            n=min(maxNDimO,maxNDim)
            do i=1,n
              do j=1,n
                rmo(i+(j-1)*maxNDim)=TrMP(i+(j-1)*maxNDimO)
              enddo
            enddo
            call CopyMat(rmo,TrMP,maxNDim)
            call MatInv(TrMP,TrMPI,pom,maxNDim)
            PrepisM95=.true.
          endif
        endif
        if(ExistM95.and.KPhase.eq.1.and.
     1     (.not.EqRV(CellParOld(1),CellPar(1,1,KPhase),3,.0001).or.
     2      .not.EqRV(CellParOld(4),CellPar(4,1,KPhase),3,.001))) then                        then
          if(.not.PrepisM95)
     1      call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
          call MatBlock3(TrMPI,rmo,maxNDim)
          call UnitMat(rmp,maxNDim)
          CellRefBlock(1:6,0)=CellPar(1:6,1,KPhase)
          do i=1,NDimI(KPhase)
            QuRefBlock(1:3,i,0)=Qu(1:3,i,1,KPhase)
            QrPom(1:3,i)=0.
          enddo
          call DRMatTrCellQ(rmo,CellRefBlock(1,0),QuRefBlock(1:3,i,0),
     1                      QrPom,rmp)
          PrepisM95=.true.
        endif
        if(PrepisM95) then
          call iom95(1,fln(:ifln)//'.m95')
          lni=NextLogicNumber()
          call OpenFile(lni,fln(:ifln)//'.z95','formatted','old')
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'.m95','formatted','unknown')
4100      read(lni,FormA) Veta
          if(.not.EqIgCase(Veta,'end')) go to 4100
4200      read(lno,FormA) Veta
          if(.not.EqIgCase(Veta,'end')) go to 4200
4300      read(lni,FormA,end=4400) Veta
          write(lno,FormA) Veta(:idel(Veta))
          go to 4300
4400      call CloseIfOpened(lni)
          call CloseIfOpened(lno)
        endif
        if(NTwinin.ne.NTwin) then
          j=mxscutw-NTwinIn+2
          call SetRealArrayTo(sc(j,KDatBlock),NTwinIn-1,0.)
          call SetIntArrayTo(KiS(j,KDatBlock),NTwinIn-1,0)
          pom=1./float(NTwin)
          call SetRealArrayTo(sctw(1,KDatBlock),NTwin,pom)
        endif
        if(MagneticTypeO.gt.0.and.MagneticType(KPhase).eq.0) then
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase) cycle
            MagPar(i)=0
          enddo
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
        if(Zpet) then
          KPhase=KPhaseNew
          call FeDestroyKart
          go to 1000
        endif
      else
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
      endif
      call FeDestroyKart
      KPhase=KPhaseBasic
      if(StatusM50.le.100.and.ich.eq.0.and.
     1   (.not.ExistM90.and.ExistM95)) then
        if(FeYesNo(-1.,-1.,'Do you want to create refinement '//
     1             'reflection file?',1)) then
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
        endif
        go to 9999
      endif
      if(isPowder.or..not.ExistM95.or.ich.ne.0) go to 9999
      if(.not.M50JesteNeexistovala) then
        TypTextu=1
        call CrlCompareSymmetry(IStSymmOrg,SymmetryNotChanged)
        if(.not.SymmetryNotChanged) go to 5000
        if(NTwinIn.ne.NTwin) go to 5000
        do 4500i=1,NTwin
          do j=1,NTwin
            if(EqRV(RTwIn(1,i),RTw(1,j),9,.001)) go to 4500
          enddo
          go to 5000
4500    continue
        if(StatusM50.lt.11000) then
          call iom50(0,0,fln(:ifln)//'.m50')
          if(ParentStructure) call SSG2QMag(fln)
        endif
        if(KCommen(KPhase).ne.KCommenOld) then
          TypTextu=2
          go to 5000
        else if(KCommen(KPhase).gt.0) then
          call ComSym(0,0,ich)
          call CrlCompareSymmetry(IStSymmOrgComm,SymmetryNotChanged)
          if(StatusM50.lt.11000) then
            call iom50(0,0,fln(:ifln)//'.m50')
            if(ParentStructure) call SSG2QMag(fln)
          endif
          if(.not.SymmetryNotChanged) go to 5000
        endif
      endif
      go to 9999
5000  if(ExistSingle) then
        NInfo=2
        if(TypTextu.eq.1) then
          TextInfo(1)='The program has detected a change of the '//
     1                'symmetry/twinning which call for'
          TextInfo(2)='re-creation of the reflection file.'
        else
          Veta='Description of the structure has been changed from'
          if(KCommen(KPhase).gt.0) then
            TextInfo(1)=Veta(:idel(Veta)+1)//
     1                  'incommensurate to commensurate'
          else
            TextInfo(1)=Veta(:idel(Veta)+1)//
     1                  'commensurate to incommensurate'
          endif
          TextInfo(2)='which call for a new averaging.'
        endif
        if(FeYesNoHeader(-1.,-1.,'Do you want to re-create '//
     1                   'refinement reflection file just now?',1)) then
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          if(NPhase.gt.1) then
            call EM9ImportWizard(1)
          else
            call EM9CreateM90(0,ich)
          endif
        endif
        Veta=fln(:ifln)//'.inflip'
        if(ExistFile(Veta)) call DeleteFile(Veta)
      endif
9999  call DeleteFile(fln(:ifln)//'.l52')
      call CrlCleanSymmetry
      if(WizardMode) QuestColorFill(id)=LightGray
      if(allocated(RTwIn)) deallocate(RTwIn)
      return
      end
      subroutine EM50CellSymmTest(Klic,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real ShSgS(6),gpp(36),gppi(36),CellNew(6),QuNew(3,3),trezS(3)
      real, allocatable :: RMagS(:,:),ZMagS(:)
      character*80 Veta
      character*4  LatticeS
      integer CrSystemSave,MonoclinicSave,NCommenOld(3)
      logical EqRV,FeYesNoHeader,EqIgCase
      real MetTens6NewI(36),MetTens6OldI(36),RCommenOld(9)
      ich=0
      KCommenOld=KCommen(KPhase)
      ICommenOld=ICommen(KPhase)
      if(KCommen(KPhase).ne.0) then
        call CopyVekI(NCommen(1,1,KPhase),NCommenOld,3)
        call CopyMat(RCommen(1,1,KPhase),RCommenOld,3)
        ISymmStore=0
        call CrlStoreSymmetry(ISymmStore)
        allocate(RMagS(9,NSymm(KPhase)),ZMagS(NSymm(KPhase)))
        nssn =NSymmN(KPhase)
        LatticeS=Lattice(KPhase)
        CrSystemSave=CrSystem(KPhase)
        MonoclinicSave=Monoclinic(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(RMag(1,i,1,KPhase),RMagS(1,i),3)
          ZMagS(i)=ZMag(i,1,KPhase)
        enddo
        call CopyVek(ShSg(1,KPhase),ShSgS,NDim(KPhase))
        call CopyVek(trez(1,1,1),trezS,NDimI(KPhase))
        call comsym(0,0,ich)
      endif
      if(ParentStructure.and.KQMag(KPhase).eq.1) then
        call CopyMat(MetTensI(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call CopyMat(MetTensI(1,1,KPhase),MetTens6OldI,NDim(KPhase))
      else
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6OldI,NDim(KPhase))
      endif
      call EM50MetTensSymm(MetTens6NewI)
      if(KCommenOld.ne.0) then
        call CrlRestoreSymmetry(ISymmStore)
        NDimI(KPhase)=NDim(KPhase)-3
        NDimQ(KPhase)=NDim(KPhase)**2
        NSymmN(KPhase)=nssn
        Lattice(KPhase)=LatticeS
        do i=1,NSymm(KPhase)
          call CopyMat(RMagS(1,i),RMag(1,i,1,KPhase),3)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                  symmc(1,i,1,KPhase),0)
          ZMag(i,1,KPhase)=ZMagS(i)
        enddo
        call CopyVek(ShSgS,ShSg(1,KPhase),NDim(KPhase))
        ngc(KPhase)=0
        KCommen(KPhase)=1
        ICommen(KPhase)=ICommenOld
        call CopyVekI(NCommenOld,NCommen(1,1,KPhase),3)
        call CopyMat(RCommenOld,RCommen(1,1,KPhase),3)
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        SwitchedToComm=.false.
        call CopyVek(trezS,trez(1,1,1),NDimI(KPhase))
        deallocate(RMagS,ZMagS)
      endif
      if(.not.eqrv(MetTens6OldI,MetTens6NewI,NDimQ(KPhase),1.e-6)) then
        call MatBlock3(MetTens6NewI,gppi,NDim(KPhase))
        call MatInv(gppi,gpp,det,3)
        CellNew(1)=sqrt(gpp(1))
        CellNew(2)=sqrt(gpp(5))
        CellNew(3)=sqrt(gpp(9))
        CellNew(4)=acos(gpp(6)/(CellNew(2)*CellNew(3)))/ToRad
        CellNew(5)=acos(gpp(3)/(CellNew(1)*CellNew(3)))/ToRad
        CellNew(6)=acos(gpp(2)/(CellNew(1)*CellNew(2)))/ToRad
        if(NDimI(KPhase).gt.0) then
          m=3*NDim(KPhase)+1
          do i=1,NDimI(KPhase)
            call CopyVek(MetTens6NewI(m),gppi,3)
            call multm(gpp,gppi,QuNew(1,i),3,3,1)
            m=m+NDim(KPhase)
          enddo
        endif
        if(ICDDBatch) then
          TextInfo(1)='Cell parameters'
          if(NDimI(KPhase).gt.0) then
            Veta=' and/or modulation vector'
            if(NDimI(KPhase).gt.1) Veta=Veta(:idel(Veta))//'s'
          endif
          TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                Veta(:idel(Veta))//
     2                'are inconsistent with symmetry.'
          call FeChybne(-1.,-1.,TextInfo(1),' ',SeriousError)
          TextInfo(1)='Original cell parameters:'
          if(NDimI(KPhase).gt.0) then
            Veta=' and modulation vector'
            if(NDimI(KPhase).eq.1) then
              Veta=Veta(:idel(Veta))//':'
            else
              Veta=Veta(:idel(Veta))//'s:'
            endif
            Veta=TextInfo(1)(:idel(TextInfo(1))-1)//
     1                  Veta(:idel(Veta))
          endif
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          write(Veta,100)(CellPar(i,1,KPhase),i=1,6)
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          do i=1,NDimI(KPhase)
            write(Veta,102)(Qu(j,i,1,KPhase),j=1,3)
            call FeLstWriteLine(Veta,-1)
            if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          enddo
          TextInfo(1)='New cell parameters:'
          if(NDimI(KPhase).gt.0) then
            Veta=' and modulation vector'
            if(NDimI(KPhase).eq.1) then
              Veta=Veta(:idel(Veta))//':'
            else
              Veta=Veta(:idel(Veta))//'s:'
            endif
            Veta=TextInfo(1)(:idel(TextInfo(1))-1)//
     1                  Veta(:idel(Veta))
          endif
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          write(Veta,100)(CellNew(i),i=1,6)
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          do i=1,NDimI(KPhase)
            write(Veta,102)(QuNew(j,i),j=1,3)
            call FeLstWriteLine(Veta,-1)
            if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          enddo
          call CopyVek(CellNew,CellPar(1,1,KPhase),6)
          do i=1,NDimI(KPhase)
            call CopyVek(QuNew(1,i),Qu(1,i,1,KPhase),3)
          enddo
        else
          if(Klic.eq.0) then
            k=1
            TextInfo(k)='Original cell parameters:'
            TextInfoFlags(k)='CB'
            if(NDimI(KPhase).gt.0) then
              Veta=' and modulation vector'
              if(NDimI(KPhase).eq.1) then
                Veta=Veta(:idel(Veta))//':'
              else
                Veta=Veta(:idel(Veta))//'s:'
              endif
              TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))-1)//
     1                    Veta(:idel(Veta))
            endif
            k=k+1
            write(TextInfo(k),100)(CellPar(i,1,KPhase),i=1,6)
            do i=1,NDimI(KPhase)
              k=k+1
              write(TextInfo(k),101)(Qu(j,i,1,KPhase),j=1,3)
            enddo
            k=k+1
            TextInfo(k)='New cell parameters:'
            TextInfoFlags(k)='CB'
            if(NDimI(KPhase).gt.0)
     1        TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))-1)//
     2                    Veta(:idel(Veta))
            k=k+1
            write(TextInfo(k),100) CellNew
            do i=1,NDimI(KPhase)
              k=k+1
              write(TextInfo(k),101)(QuNew(j,i),j=1,3)
            enddo
            Ninfo=k
            do k=1,NInfo
              call DeleteFirstSpaces(TextInfo(k))
              if(.not.EqIgCase(TextInfoFlags(k),'CB'))
     1          TextInfoFlags(k)='CN'
            enddo
            if(NDimI(KPhase).le.0) then
              Veta='Do you want to accept the symmetrized cell '//
     1             'parameters?'
            else
              Veta='Do you want to accept the symmetrized cell '//
     1             'parameters'//Veta(:idel(Veta)-1)//'?'
            endif
            if(FeYesNoHeader(-1.,YBottomMessage,Veta,0)) then
              go to 4500
            else
              ich=1
            endif
          else
            go to 4500
          endif
        endif
      endif
      go to 5000
4500  call CopyVek(CellNew,CellPar(1,1,KPhase),6)
      do i=1,NDimI(KPhase)
         call CopyVek(QuNew(1,i),Qu(1,i,1,KPhase),3)
      enddo
      call GetQiQr(qu(1,1,1,KPhase),qui(1,1,KPhase),quir(1,1,KPhase),
     1             NDimI(KPhase),1)
5000  i=mod(CrSystem(KPhase),10)
      if(i.eq.CrSystemTriclinic) then
        go to 9999
      else if(i.eq.CrSystemMonoclinic) then
        j=Monoclinic(KPhase)
      else
        j=0
      endif
      do k=1,3
        if(k.ne.j) CellParSU(k+3,1,KPhase)=0.
      enddo
      if(i.gt.CrSystemOrthorhombic) then
        if(i.eq.CrSystemCubic) then
          kk=3
        else
          kk=2
        endif
        pom=0.
        do k=1,kk
          pom=pom+CellParSU(k,1,KPhase)
        enddo
        pom=pom/float(kk)
        do k=1,kk
          CellParSU(k,1,KPhase)=pom
        enddo
      endif
9999  return
100   format(3f9.4,3f9.3)
101   format(13x,3f9.4)
102   format(3f9.4)
      end
      subroutine EM50MetTensSymm(MetTens6NewI)
      use Basic_mod
      dimension GSuma(36),rmm(36),gpp(36)
      real MetTens6NewI(*)
      include 'fepc.cmn'
      include 'basic.cmn'
      call SetRealArrayTo(GSuma,36,0.)
      do i=1,NSymm(KPhase)
        call trmat(rm6(1,i,1,KPhase),rmm,NDim(KPhase),NDim(KPhase))
        call multm(rm6(1,i,1,KPhase),MetTens6NewI,gpp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call Cultm(gpp,rmm,GSuma,NDim(KPhase),NDim(KPhase),NDim(KPhase))
      enddo
      do j=1,NDimQ(KPhase)
        MetTens6NewI(j)=GSuma(j)/float(NSymm(KPhase))
      enddo
      return
      end
      subroutine EM50Cell
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension tzero(3),rm6p(36),pp(6),pq(6),mpp(3),qup(3,3),
     1          RCommPom(9),RCommPomI(9)
      character*256 EdwStringQuest
      character*80 Veta,t80
      character*2 nty
      integer EdwStateQuest,CrwStateQuest,ButtonStateQuest,CrlCentroSymm
      logical CrwLogicQuest,AlreadyApplied,EqRV0
      save nCrwTwin,nEdwTwin,nEdwDim,nEdwComp,nCrwCommen,nButtShowSG,
     1     nButtSelectSG,nButtOriginSG,nEdw,NDims,nCrwSimpleSupercell,
     2     nButtSupercellMatrix,nCrwUseQMag,nEdwQMag
      data AlreadyApplied/.true./
      entry EM50CellMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      ncompold=NComp(KPhase)
      NTwinOld=NTwin
      NTwinIn=NTwin
      if(allocated(RTwIn)) deallocate(RTwIn)
      allocate(RTwIn(9,NTwin))
      call CopyVek(RTw,RTwIn,9*NTwin)
      il=1
      Veta='Cell %parameters'
      tpom=5.
      xpom=FeTxLengthUnder(Veta)+15.
      dpom=400.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'S%tructure','L',dpom,
     1                    EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(nEdwName,StructureName)
      if(NPhase.gt.1) then
        il=il+1
        dpomp=120.
        call FeQuestEdwMake(id,tpom,il,xpom,il,'P%hase label','L',dpomp,
     1                      EdwYd,0)
        nEdwPhase=EdwLastMade
        call FeQuestStringEdwOpen(nEdwPhase,PhaseName(KPhase))
      else
        nEdwPhase=0
      endif
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCell=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwCell,CellPar(1,1,KPhase),6,
     1                         StatusM50.ge.10000,.false.)
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,'%E.s.d.''s','L',dpom,
     1                    EdwYd,0)
      nEdwEsd=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwEsd,CellParSU(1,1,KPhase),6,
     1                         StatusM50.ge.10000,.false.)
      if(ExistSingle.or..not.Existm90) then
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,'T%winning','L',CrwXd,
     1                      CrwYd,1,0)
        nCrwTwin=CrwLastMade
        call FeQuestCrwOpen(nCrwTwin,NTwin.gt.1)
      else
        nCrwTwin=0
      endif
      tpomp=xqd*.5
      xpom=tpomp+FeTxLengthUnder(Veta)+15.
      dpomp=50.
      tpomb=xpom+dpomp+2.*EdwYd
      dpomb=xqd-15.-tpomb
      if(ExistSingle.or..not.Existm90) then
        call FeQuestEudMake(id,tpomp,il,xpom,il,'#twi%n domains','L',
     1                      dpomp,EdwYd,1)
        nEdwTwin=EdwLastMade
        call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'%Matrices')
        nButtTwMat=ButtonLastMade
      else
        nEdwTwin=0
        nButtTwMat=0
      endif
      il=il+1
      Veta='#%composite parts'
      call FeQuestEudMake(id,tpomp,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
      nEdwComp=EdwLastMade
      call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'M%atrices')
      nButtCompMat=ButtonLastMade
      if(nEdwTwin.gt.0.and.NTwin.gt.1) then
        call FeQuestIntEdwOpen(nEdwTwin,NTwin,.false.)
        call FeQuestEudOpen(nEdwTwin,2,mxscutw,1,0.,0.,0.)
        call FeQuestButtonOpen(nButtTwMat,ButtonOff)
      endif
      if(NDimI(KPhase).gt.0) then
        call FeQuestIntEdwOpen(nEdwComp,NComp(KPhase),.false.)
        call FeQuestEudOpen(nEdwComp,1,3,1,0.,0.,0.)
        if(NComp(KPhase).gt.1)
     1    call FeQuestButtonOpen(nButtCompMat,ButtonOff)
      endif
      if((.not.ExistSingle.or.(.not.ExistM90.and..not.ExistM95).or.
     1    StatusM50.gt.10000).and..not.WizardMode.and.
     2    .not.ParentStructure) then
        Veta='%Dimension'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpomp=50.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
        nEdwDim=EdwLastMade
        call FeQuestIntEdwOpen(nEdwDim,NDim(KPhase),.false.)
        if(NPhase.gt.1.and.ExistSingle.and.StatusM50.gt.10000) then
          i=maxNDim
        else
          i=6
        endif
        call FeQuestEudOpen(nEdwDim,3,i,1,0.,0.,0.)
      else
        nEdwDim=0
        write(Veta,'(i1)') NDim(KPhase)
        Veta='Dimension = '//Veta(1:1)
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      endif
      if(ParentStructure) then
        il=il+1
        Veta='Magnetic %propagation vector:'
        xpom=5.
        tpom=xpom+CrwXd+5.
        Veta='Use non%-zero magnetic propagation vector'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwUseQMag=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KQMag(KPhase).gt.0)
        tpom=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=150.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwQMag=EdwLastMade
        call FeQuestRealAEdwOpen(nEdwQMag,QMag(1,KPhase),3,.false.,
     1                           .true.)
        if(KQMag(KPhase).le.0) call FeQuestEdwDisable(nEdwQMag)
      else
        write(Veta,'(''%'',i1,a2,'' modulation vector'')') 1,nty(1)
        xpom=FeTxLength(Veta)+10.
        dpomp=200.
        do i=1,3
          il=il+1
          write(Veta(2:4),'(i1,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,0)
          if(i.eq.1) nEdwModVek=EdwLastMade
          if(i.le.NDimI(KPhase))
     1      call FeQuestRealAEdwOpen(nEdwModVek+i-1,qu(1,i,1,KPhase),3,
     2                               StatusM50.ge.10000,.true.)
        enddo
        nCrwUseQMag=0
        nEdwQMag=0
      endif
      il=il+1
      tpom=5.
      Veta='Commens%urate case'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwCommen=CrwLastMade
      if(NDim(KPhase).gt.3)
     1  call FeQuestCrwOpen(CrwLastMade,KCommen(KPhase).ne.0)
      il=il+1
      Veta='Use s%imple supercell'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwSimpleSupercell=CrwLastMade
      if(KCommen(KPhase).gt.0.and.NDim(KPhase).eq.4)
     1  call FeQuestCrwOpen(CrwLastMade,ICommen(KPhase).eq.0)
      il=il-1
      tpom=xpom+CrwXd+50.
      Veta='%Supercell'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpomp=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
      nEdwSupCell=EdwLastMade
      Veta='Define %supercell matrix'
      call FeQuestButtonMake(id,tpom,il,dpomp+55.,ButYd,Veta)
      nButtSupercellMatrix=ButtonLastMade
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,'T%zero','L',dpomp,EdwYd,0)
      nEdwTzero=EdwLastMade
      il=il-1
      Veta='Show supercell %group'
      tpom=(xdqp+xpom+dpomp)*.5
      xpom=FeTxLengthUnder(Veta)+10.
      tpom=tpom-xpom*.5
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButtShowSG=ButtonLastMade
      il=il+1
      Veta='Se%lect supercell group'
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButtSelectSG=ButtonLastMade
      il=il+1
      Veta='Select its %origin'
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButtOriginSG=ButtonLastMade
      if(KCommen(KPhase).ne.0) then
        if(ICommen(KPhase).eq.0) then
          call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                            .false.)
        else
          call FeQuestButtonOff(nButtSupercellMatrix)
        endif
        call FeQuestRealAEdwOpen(nEdwTzero,trez(1,1,KPhase),
     1                           NDimI(KPhase),.false.,.true.)
        if(NSymm(KPhase).gt.0) then
          i=ButtonOff
        else
          i=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtShowSG,i)
        if(NDim(KPhase).lt.5) then
          call FeQuestButtonOpen(nButtSelectSG,i)
          call FeQuestButtonOpen(nButtOriginSG,i)
        endif
      endif
      go to 9999
      entry EM50CellCheck
      if(nEdwDim.gt.0) then
        NDimo=NDim(KPhase)
        call FeQuestIntFromEdw(nEdwDim,NDim(KPhase))
        NDimI(KPhase)=NDim(KPhase)-3
        NDimQ(KPhase)=NDim(KPhase)**2
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDim) then
        nEdw=nEdwModVek-1
        do 1100i=1,3
          nEdw=nEdw+1
          if(i+3.le.NDim(KPhase)) then
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              go to 1100
            else
              call FeQuestRealAEdwOpen(nEdw,qu(1,i,1,KPhase),3,.true.,
     1                                 .false.)
            endif
          else
            call FeQuestEdwClose(nEdw)
          endif
1100    continue
        if(NDim(KPhase).gt.3) then
          if(EdwStateQuest(nEdwComp).ne.EdwOpened) then
            NComp(KPhase)=1
            call FeQuestIntEdwOpen(nEdwComp,NComp(KPhase),.false.)
            call FeQuestEudOpen(nEdwComp,1,3,1,0.,0.,0.)
          endif
          if(CrwStateQuest(nCrwCommen).eq.CrwClosed) then
            KCommen(KPhase)=0
            call FeQuestCrwOpen(nCrwCommen,.false.)
          endif
        else
          call FeQuestEdwClose(nEdwComp)
          call FeQuestCrwClose(nCrwCommen)
          call FeQuestCrwClose(nCrwSimpleSupercell)
          call FeQuestEdwClose(nEdwSupCell)
          call FeQuestButtonClose(nButtSupercellMatrix)
          call FeQuestEdwClose(nEdwTzero)
          call FeQuestButtonClose(nButtShowSG)
          call FeQuestButtonClose(nButtSelectSG)
          call FeQuestButtonClose(nButtOriginSG)
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwComp) then
        call FeQuestIntFromEdw(nEdwComp,NComp(KPhase))
        do i=ncompOld+1,NComp(KPhase)
          call UnitMat(zv(1,i,KPhase),NDim(KPhase))
        enddo
        if(NComp(KPhase).gt.1) then
          if(ButtonStateQuest(nButtCompMat).eq.ButtonClosed)
     1      call FeQuestButtonOpen(nButtCompMat,ButtonOff)
        else
          call FeQuestButtonClose(nButtCompMat)
        endif
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call FinSym(0)
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSupCell) then
        call FeQuestIntAFromEdw(nEdwSupCell,NCommen(1,1,KPhase))
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwQMag) then
        call FeQuestRealAFromEdw(nEdwQMag,QMag(1,KPhase))
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTwin) then
        call FeQuestIntFromEdw(nEdwTwin,NTwin)
        mxscu=mxscutw-NTwin+1
        call FeDeferOutput
        if(NTwin.le.1) then
          call FeQuestCrwOff(nCrwTwin)
          call FeQuestEdwClose(nEdwTwin)
          call FeQuestButtonClose(nButtTwMat)
          EventType=0
          EventNumber=0
          EventTypeSave=0
          EventNumberSave=0
        else
          do i=NTwinold+1,NTwin
            call UnitMat(rtw(1,i),3)
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwSimpleSupercell) then
        ICommen(KPhase)=1-ICommen(KPhase)
        if(ICommen(KPhase).eq.0) then
          call FeQuestButtonClose(nButtSupercellMatrix)
          call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                            .false.)
        else
          call FeQuestEdwClose(nEdwSupCell)
          call FeQuestButtonOff(nButtSupercellMatrix)
          do j=1,NDimI(KPhase)
            call MultM(qu(1,j,1,KPhase),RCommen(1,1,KPhase),pp,1,3,3)
            do k=1,3
              if(abs(anint(pp(k))-pp(k)).gt..0001) then
                call UnitMat(RCommen (1,1,KPhase),3)
                call UnitMat(RCommenI(1,1,KPhase),3)
                go to 1800
              endif
            enddo
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwTwin) then
        if(CrwLogicQuest(nCrwTwin)) then
          if(EdwState(nEdwTwin).eq.EdwClosed) then
            NTwin=2
            mxscu=mxscutw-NTwin+1
            call UnitMat(rtw(1,2),3)
            if(CrlCentroSymm().le.0)
     1        call RealMatrixToOpposite(rtw(1,2),rtw(1,2),3)
            call FeQuestIntEdwOpen(nEdwTwin,NTwin,.false.)
            call FeQuestEudOpen(nEdwTwin,1,mxscutw,1,0.,0.,0.)
            call FeQuestButtonOpen(nButtTwMat,ButtonOff)
          endif
          EventType=EventEdw
          EventNumber=nEdwTwin
        else
          NTwin=1
          mxscu=mxscutw-NTwin+1
          call FeQuestEdwClose(nEdwTwin)
          call FeQuestButtonClose(nButtTwMat)
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCommen) then
        if(CrwLogicQuest(nCrwCommen)) then
          if(ICommen(KPhase).eq.0) then
            call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                              .false.)
          else
            call FeQuestButtonOff(nButtSupercellMatrix)
          endif
          call SetRealArrayTo(trez(1,1,KPhase),NDimI(KPhase),0.)
          call FeQuestRealAEdwOpen(nEdwTzero,trez(1,1,KPhase),
     1                             NDimI(KPhase),.false.,.true.)
          if(NDim(KPhase).eq.4)
     1      call FeQuestCrwOpen(nCrwSimpleSupercell,
     2                          ICommen(KPhase).eq.0)
          if(NSymm(KPhase).gt.0) then
            i=ButtonOff
          else
            i=ButtonDisabled
          endif
          call FeQuestButtonOpen(nButtShowSG,i)
          if(NDim(KPhase).lt.5) then
            call FeQuestButtonOpen(nButtSelectSG,i)
            call FeQuestButtonOpen(nButtOriginSG,i)
          endif
          EventType=EventEdw
          EventNumber=nEdwSupCell
          KCommen(KPhase)=1
          KCommenMax=1
          if(NComp(KPhase).gt.1)
     1      call FeQuestButtonOpen(nButtCompMat,ButtonOff)
        else
          call FeQuestCrwClose(nCrwSimpleSupercell)
          call FeQuestEdwClose(nEdwSupCell)
          call FeQuestEdwClose(nEdwTzero)
          call FeQuestButtonClose(nButtShowSG)
          call FeQuestButtonClose(nButtSelectSG)
          call FeQuestButtonClose(nButtOriginSG)
          call FeQuestButtonClose(nButtSupercellMatrix)
          KCommen(KPhase)=0
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseQMag) then
        if(CrwLogicQuest(CheckNumber)) then
          KQMag(KPhase)=1
          call FeQuestRealAEdwOpen(nEdwQMag,QMag(1,KPhase),3,
     1                             EqRV0(QMag(1,KPhase),3,.0001),
     2                             .false.)
        else
          call FeQuestRealAFromEdw(nEdwQMag,QMag(1,KPhase))
          call FeQuestEdwDisable(nEdwQMag)
          KQMag(KPhase)=0
        endif
      else if(CheckType.eq.EventButton) then
        call EM50CellUpdate(ich)
        if(CheckNumber.gt.0) then
          if(CheckNumber.eq.nButtTwMat) then
            call ReadTw(Rtw,Rtwi,NTwin,CellPar(1,1,1),ich)
            if(ich.eq.0) NTwinold=NTwin
          else if(CheckNumber.eq.nButtCompMat) then
            call ReadCompMat(ich)
            if(ich.eq.0) ncompold=NComp(KPhase)
          else if(CheckNumber.eq.nButtShowSG) then
            if(ich.eq.0) call EM50ShowSuperSG(0,Veta,pq,t80,rm6p,pp)
          else if(CheckNumber.eq.nButtSelectSG.or.
     1            CheckNumber.eq.nButtOriginSG) then
            if(ich.eq.0) then
              if(CheckNumber.eq.nButtSelectSG) then
                Key=0
              else
                Key=1
              endif
              call CopyVek(trez(1,1,KPhase),tzero,3)
              call EM50SelectSuperSG(Key,tzero,ichp)
              if(ichp.eq.0)
     1          call FeQuestRealAEdwOpen(nEdwTzero,tzero,NDimI(KPhase),
     2                                   .false.,.true.)
            endif
          else if(CheckNumber.eq.nButtSupercellMatrix) then
            Veta='Define the supercell matrix'
            call CopyMat(RCommen (1,1,KPhase),RCommPom ,3)
            call CopyMat(RCommenI(1,1,KPhase),RCommPomI,3)
1500        call FeReadRealMat(-1.,-1.,Veta,SmbABC,IdChangeCase,
     1        RCommen(1,1,KPhase),RCommenI(1,1,KPhase),3,CheckSingYes,
     2        CheckPosDefYes,ich)
            if(ich.ne.0) then
              call CopyMat(RCommPom ,RCommen (1,1,KPhase),3)
              call CopyMat(RCommPomI,RCommenI(1,1,KPhase),3)
              go to 1800
            endif
            do j=1,NDimI(KPhase)
              call MultM(qu(1,j,1,KPhase),RCommen(1,1,KPhase),pp,1,3,3)
              do k=1,3
                if(abs(anint(pp(k))-pp(k)).gt..0001) then
                  call FeChybne(-1.,-1.,'the matrix isn''t consistent'//
     1                          ' with modulation vector(s), try again.'
     2                         ,' ',SeriousError)
                  go to 1500
                endif
              enddo
            enddo
          endif
        endif
      endif
1800  if(nEdwDim.eq.0) go to 9999
      if(NDim(KPhase).ne.NDimo.or..not.AlreadyApplied) then
        if(.not.AlreadyApplied) NDimo=NDims
        AlreadyApplied=.false.
        if(NDim(KPhase).gt.3) then
          nEdw=nEdwModVek
          do i=1,NDimI(KPhase)
            if(EdwStringQuest(nEdw).ne.' ') then
              call FeQuestRealAFromEdw(nEdw,qu(1,i,1,KPhase))
            else
              NDims=NDimo
              go to 9999
            endif
            nEdw=nEdw+1
          enddo
        endif
2025    nso=NSymm(KPhase)
        nvto=NLattVec(KPhase)
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,nvto
          call SetRealArrayTo(vt6(NDimo+1,i,1,KPhase),
     1                        NDim(KPhase)-NDimo,0.)
        enddo
        NSymm(KPhase)=0
        do 2080i=1,nso
          call UnitMat(rm6p,NDim(KPhase))
          call SetRealArrayTo(s6(NDimo+1,i,1,KPhase),
     1                        NDim(KPhase)-NDimo,0.)
          m=0
          do j=1,NDimo
            do k=1,NDimo
              m=m+1
              rm6p(k+(j-1)*NDim(KPhase))=rm6(m,i,1,KPhase)
            enddo
          enddo
          do j=1,NDimI(KPhase)
            call CopyVek(qu(1,j,1,KPhase),qup(1,j),3)
            nnul=0
            KdeNenul=0
            do k=1,3
              if(abs(qup(k,j)).lt..0001) then
                nnul=nnul+1
              else
                if(KdeNenul.le.0) KdeNenul=k
              endif
            enddo
            qup(KdeNenul,j)=1./sqrt(3.)
          enddo
          do j=4,NDim(KPhase)
            do k=1,3
              pom=0.
              do l=1,3
                pom=pom+qup(l,j-3)*rm6p(l+(k-1)*NDim(KPhase))
              enddo
              pp(k)=pom
            enddo
            do k=-1,1
              do l=-1,1
                do 2060m=-1,1
                  do n=1,3
                    pq(n)=float(m)*qup(n,1)-pp(n)
                    if(NDim(KPhase).gt.4)
     1                pq(n)=pq(n)+float(l)*qup(n,2)
                    if(NDim(KPhase).gt.5)
     1                pq(n)=pq(n)+float(k)*qup(n,3)
                    mpp(n)=nint(pq(n))
                    if(abs(pq(n)-float(mpp(n))).gt..0005) go to 2060
                  enddo
                  do n=1,NLattVec(KPhase)
                    pom=ScalMul(vt6(1,n,1,KPhase),pq)
                    if(abs(pom-anint(pom)).gt..0005) go to 2060
                  enddo
                  do n=1,3
                    rm6p(j+NDim(KPhase)*(n-1))=-mpp(n)
                  enddo
                  rm6p(j+NDim(KPhase)*3)=m
                  if(NDim(KPhase).gt.4) rm6p(j+NDim(KPhase)*4)=l
                  if(NDim(KPhase).gt.5) rm6p(j+NDim(KPhase)*5)=k
                  go to 2075
2060            continue
              enddo
            enddo
          enddo
          if(NDim(KPhase).gt.3) go to 2080
2075      NSymm(KPhase)=NSymm(KPhase)+1
          BratSymm(NSymm(KPhase),KPhase)=.true.
          call CopyMat(rm6p,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
          call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                  s6(1,NSymm(KPhase),1,KPhase),
     1                  symmc(1,NSymm(KPhase),1,KPhase),0)
2080    continue
        AlreadyApplied=.true.
        Veta='???'
        call FindSmbSg(Veta,.true.,1)
        Grupa(KPhase)=Veta
      endif
9999  return
      end
      subroutine EM50ShowSuperSG(Key,Grp,ShSgOrg,GrpTr,TrMatOut,ShSgOut)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrSystemSave,MonoclinicSave,CrSystemNew,CrSystemNewTr
      integer, allocatable :: IswSymmS(:,:)
      logical, allocatable :: BratSymmS(:)
      real, allocatable :: rm6s(:,:,:),rms(:,:,:),s6s(:,:,:),
     1                     vt6s(:,:,:),RMagS(:,:,:),ZMagS(:,:)
      real ShSgOrg(*),TrMatOut(*),ShSGOut(*),MetTensO(9,3),rmp(9),
     1     rmq(9),ShSgS(6)
      character*(*) GrpTr,Grp
      character*4  LatticeS
      allocate(rm6s(NDimQ(KPhase),NSymm(KPhase),NComp(KPhase)),
     1         rms(9,NSymm(KPhase),NComp(KPhase)),
     2         IswSymms(NSymm(KPhase),NComp(KPhase)),
     3         s6s(NDim(KPhase),NSymm(KPhase),NComp(KPhase)),
     4         BratSymmS(NSymm(KPhase)),
     5         RMagS(9,NSymm(KPhase),NComp(KPhase)),
     6         ZMagS(NSymm(KPhase),NComp(KPhase)),
     4         vt6s(NDim(KPhase),NLattVec(KPhase),NComp(KPhase)))
      nss =NSymm(KPhase)
      nvts=NLattVec(KPhase)
      LatticeS=Lattice(KPhase)
      NDims=NDim(KPhase)
      CrSystemSave=CrSystem(KPhase)
      MonoclinicSave=Monoclinic(KPhase)
      NGrupaSave=NGrupa(KPhase)
      do isw=1,NComp(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,isw,KPhase),rm6s(1,i,isw),NDim(KPhase))
          call CopyMat(rm(1,i,isw,KPhase),rms(1,i,isw),3)
          call CopyMat(RMag(1,i,isw,KPhase),RMagS(1,i,isw),3)
          ZMagS(i,isw)=ZMag(i,isw,KPhase)
          call CopyVek(s6(1,i,isw,KPhase),s6s(1,i,isw),NDim(KPhase))
          IswSymmS(i,isw)=IswSymm(i,isw,KPhase)
          BratSymmS(i)=BratSymm(i,KPhase)
        enddo
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6(1,i,1,KPhase),vt6s(1,i,isw),NDim(KPhase))
        enddo
        call CopyMat(MetTens(1,isw,KPhase),MetTensO(1,isw),3)
      enddo
      call CopyVek(ShSg(1,KPhase),ShSgS,NDim(KPhase))
      call ComSym(0,0,ich)
      call TrMat(RCommen(1,1,KPhase),rmq,3,3)
      call MultM(rmq,MetTens(1,1,KPhase),rmp,3,3,3)
      call MultM(rmp,RCommen(1,1,KPhase),MetTens(1,1,KPhase),3,3,3)
      call SuperSGToSuperCellSG(ich)
      if(ich.eq.0) then
        NDim(KPhase)=3
        NDimI(KPhase)=0
        NDimQ(KPhase)=NDim(KPhase)**2
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        call CrlOrderMagSymmetry
        call SetIgnoreWTo(.true.)
        call SetIgnoreETo(.true.)
        call FindSmbSgTr(Grp,ShSgOrg,GrpTr,n,TrMatOut,ShSgOut,
     1                   CrSystemNew,CrSystemNewTr)
        MonoclinicNewTr=CrSystemNewTr/10
        if(mod(CrSystemNewTr,10).eq.CrSystemMonoclinic)
     1    GrpTr=GrpTr(:idel(GrpTr))//' - '//SmbABC(MonoclinicNewTr)//
     2        ' setting'
        MonoclinicNew=CrSystemNew/10
        if(mod(CrSystemNew,10).eq.CrSystemMonoclinic)
     1    Grp=Grp(:idel(Grp))//' - '//SmbABC(MonoclinicNew)//
     2        ' setting'
        call SetIgnoreWTo(.false.)
        call SetIgnoreETo(.false.)
        if(Key.eq.0) then
          NInfo=6
          TextInfo(1)='Space group symbol (original setting) : '//Grp
          call OriginShift2String(ShSgOrg,3,TextInfo(2))
          TextInfo(2)='Origin shift: '//TextInfo(2)
          TextInfo(3)=' '
          TextInfo(4)='Space group symbol (standard setting) : '//GrpTr
          call TrMat2String(TrMatOut,3,TextInfo(6))
          TextInfo(5)='Transformation matrix: '//TextInfo(6)
          call OriginShift2String(ShSgOut,3,TextInfo(6))
          TextInfo(6)='Origin shift: '//TextInfo(6)
          del=0.
          do i=1,6
            del=max(del,FeTxLength(TextInfo(i)))
          enddo
          do while(FeTxLength(TextInfo(3)).lt.del)
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//'-'
          enddo
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
        endif
      else
        if(Key.eq.0) call FeUnforeseenError(' ')
      endif
      NDim(KPhase)=NDims
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      NSymm(KPhase)=nss
      NLattVec(KPhase)=nvts
      Lattice(KPhase)=LatticeS
      do isw=1,NComp(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(rm6s(1,i,isw),rm6(1,i,isw,KPhase),NDim(KPhase))
          call CopyMat(rms(1,i,isw),rm(1,i,isw,KPhase),3)
          call CopyVek(s6s(1,i,isw),s6(1,i,isw,KPhase),NDim(KPhase))
          call CopyMat(RMagS(1,i,isw),RMag(1,i,isw,KPhase),3)
          ZMag(i,isw,KPhase)=ZMagS(i,isw)
          IswSymm(i,isw,KPhase)=IswSymmS(i,isw)
          BratSymm(i,KPhase)=BratSymmS(i)
          call CodeSymm(rm6(1,i,isw,KPhase),s6(1,i,isw,KPhase),
     1                  symmc(1,i,isw,KPhase),0)
        enddo
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6s(1,i,isw),vt6(1,i,isw,KPhase),NDim(KPhase))
        enddo
        call CopyMat(MetTensO(1,isw),MetTens(1,isw,KPhase),3)
      enddo
      call CopyVek(ShSgS,ShSg(1,KPhase),NDim(KPhase))
      ngc(KPhase)=0
      KCommen(KPhase)=1
      deallocate(rm6s,rms,s6s,vt6s,RMagS,ZMagS,IswSymmS,BratSymmS)
      CrSystem(KPhase)=CrSystemSave
      Monoclinic(KPhase)=MonoclinicSave
      NGrupa(KPhase)=NGrupaSave
      SwitchedToComm=.false.
      return
      end
      subroutine EM50SelectSuperSG(Key,tzero,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ta(:),tap(:),tzero(*),xp(6),TrPom(9)
      character*80 Veta,GrP,t80
      character*20 General
      integer SbwItemPointerQuest,CrSystemSave,MonoclinicSave,UseTabsIn,
     1        SelNt
      logical EqIgCase
      allocatable ta,tap
      UseTabsIn=UseTabs
      ntm=100
      Allocate(ta(ntm))
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_ssg.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      nlim=NCommQ(1,1,KPhase)
      tlim=1./float(nlim)
      ta(1)=tzero(1)
      CrSystemSave=CrSystem(KPhase)
      MonoclinicSave=Monoclinic(KPhase)
      if(Key.eq.0) then
        itgrid=24
        nt=1
        ta(nt)=1./sqrt(2.)*tlim
        trez(1,1,KPhase)=ta(nt)
        call EM50ShowSuperSG(1,GrP,xp,t80,TrPom,xp)
        General=GrP
        Veta='General'//Tabulator//GrP
        write(ln,FormA) Veta(:idel(Veta))
      else
        nt=0
        itgrid=nlim
      endif
      do j=0,itgrid-1
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        if(Key.eq.0) then
          do k=j,1,-1
            if(mod(itgrid*nlim,k).eq.0.and.mod(j,k).eq.0) then
              iz=(itgrid*nlim)/k
              jz=j/k
              go to 1540
            endif
          enddo
          iz=itgrid*nlim
          jz=j
1540      trez(1,1,KPhase)=float(jz)/float(iz)
        else
          pom=tzero(1)+float(j)*tlim
1550      if(pom.gt..9999) then
            pom=pom-1.
            go to 1550
          endif
          do k=1,24
            fk=k
            jz=nint(pom*fk)
            if(abs(pom-float(jz)/fk).lt..0001) then
              iz=k
              go to 1570
            endif
          enddo
          iz=0
          jz=0
1570      trez(1,1,KPhase)=pom
        endif
        call EM50ShowSuperSG(1,t80,xp,GrP,TrPom,xp)
        if(iz.gt.0) then
          if(jz.gt.0) then
            write(Veta,'(i4,''/'',i4)') jz,iz
            call zhusti(Veta)
          else
            Veta='0'
          endif
          if(Key.eq.0)  then
            write(Cislo,'(i4)') nlim
            call zhusti(Cislo)
            Veta=Veta(:idel(Veta))//' + n/'//Cislo(:idel(Cislo))
          endif
        else
          write(Veta,'(f10.4)') pom
          call zhusti(Veta)
        endif
        if(Key.eq.0) then
          if(EqIgCase(General,t80)) cycle
          Veta=Veta(:idel(Veta))//
     1         Tabulator//t80
        else
          write(t80,'(3f10.5)')(ShSg(i,KPhase),i=1,3)
          call ZdrcniCisla(t80,3)
          Veta=Veta(:idel(Veta))//
     1         Tabulator//GrP(:idel(GrP))//Tabulator//t80(:idel(t80))
        endif
        nt=nt+1
        if(nt.ge.ntm) then
           Allocate(tap(ntm))
           call CopyVek(ta,tap,ntm)
           ntm=ntm+100
           Deallocate(ta)
           Allocate(ta(ntm))
           call CopyVek(tap,tap,nt)
           Deallocate(tap)
        endif
        write(ln,FormA) Veta(:idel(Veta))
        ta(nt)=trez(1,1,KPhase)
        if(abs(ta(nt))-tzero(1).lt..0001) SelNt=Nt
      enddo
      call CloseIfOpened(ln)
1650  if(nt.gt.100) then
        call FeMsgOut(-1.,-1.,
     1              'Warning: Not enough space to complete the table')
      endif
      id=NextQuestId()
      if(Key.eq.0) then
        xqd=250.
      else
        xqd=400.
      endif
      nmax=min(nt,15)
      ilk=min(ifix((float(nmax+1)*MenuLineWidth+2.)/QuestLineWidth)+2,
     1        15)
      ilk=max(ilk,6)
      Veta='Select supercell spacegroup'
      call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,0,0)
      xpom=xqd*.5-50.
      il=1
      call FeQuestLblMake(id, 10.,il,'Tzero','L','N')
      call FeQuestLblMake(id,110.,il,'Space group','L','N')
      if(Key.ne.0)
     1  call FeQuestLblMake(id,210.,il,'Origin shift','L','N')
      UseTabs=NextTabs()
      xpom=100.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=200.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      il=ilk
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_ssg.tmp')
      call FeQuestSbwShow(nSbw,1)
      call FeQuestSbwItemOff(nSbw,1)
      call FeQuestSbwItemOn(nSbw,SelNt)
3500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 3500
      endif
      if(ich.eq.0) tzero(1)=ta(SbwItemPointerQuest(nSbw))
5200  call FeQuestRemove(id)
9999  Deallocate(ta)
      call DeleteFile(fln(:ifln)//'_ssg.tmp')
      CrSystem(KPhase)=CrSystemSave
      Monoclinic(KPhase)=MonoclinicSave
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
      end
      subroutine EM50CellUpdate(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*256 EdwStringQuest
      logical CrSystemDiff
      ich=0
      StructureName=EdwStringQuest(nEdwName)
      if(nEdwPhase.gt.0) PhaseName(KPhase)=EdwStringQuest(nEdwPhase)
      if(EdwStringQuest(nEdwCell).ne.' ') then
        call FeQuestRealAFromEdw(nEdwCell,CellPar(1,1,KPhase))
        if(CellPar(1,1,KPhase).le.0..or.CellPar(2,1,KPhase).le.0..or.
     1     CellPar(3,1,KPhase).le.0.) then
          call FeChybne(-1.,-1.,'negative volume - try again.',' ',
     1                  SeriousError)
          go to 1200
        endif
        csa=cos(CellPar(4,1,KPhase)*torad)
        csb=cos(CellPar(5,1,KPhase)*torad)
        csg=cos(CellPar(6,1,KPhase)*torad)
        pom=1.-csa**2-csb**2-csg**2+2.*csa*csb*csg
        if(pom.le.0.) then
          call FeChybne(-1.,-1.,'angles are incompatible - try '//
     1                  'again.',' ',SeriousError)
          go to 1200
        endif
        CellVol(1,KPhase)=CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1                    CellPar(3,1,KPhase)*sqrt(pom)
        call CheckSystem(CellPar(1,1,KPhase),i,j)
        if(CrSystemDiff(j,CrSystem(KPhase))) then
          Monoclinic(KPhase)=i
          CrSystem(KPhase)=j
        endif
      else
        call FeChybne(-1.,-1.,'you must first define cell parameters.',
     1                ' ',SeriousError)
        go to 1200
      endif
      go to 1300
1200  EventType=EventEdw
      EventNumber=nEdwCell
      go to 9000
1300  call FeQuestRealAFromEdw(nEdwEsd,CellParSU(1,1,KPhase))
      nEdw=nEdwModVek-1
      do 2000i=1,NDimI(KPhase)
        nEdw=nEdw+1
        if(EdwStringQuest(nEdw).ne.' ') then
          call FeQuestRealAFromEdw(nEdw,qu(1,i,1,KPhase))
          pom=0.
          do j=1,3
            pom=pom+abs(qu(j,i,1,KPhase))
          enddo
          if(pom.lt..0001) go to 1500
        else
          go to 1600
        endif
        go to 2000
1500    call FeChybne(-1.,-1.,'the modulation vector cannot be a zero'//
     1                ' vector.',' ',SeriousError)
        go to 1900
1600    call FeChybne(-1.,-1.,'you must first define modulation '//
     1                'vector(s).',' ',SeriousError)
1900    EventType=EventEdw
        EventNumber=nEdwModVek
        go to 9000
2000  continue
      if(KCommen(KPhase).ne.0) then
        if(ICommen(KPhase).eq.0) then
          if(EdwStringQuest(nEdwSupCell).ne.' ') then
            call FeQuestIntAFromEdw(nEdwSupCell,NCommen(1,1,KPhase))
          else
            go to 9999
          endif
        endif
        if(EdwStringQuest(nEdwTzero).ne.' ') then
          call FeQuestRealAFromEdw(nEdwTzero,trez(1,1,KPhase))
        else
          go to 9999
        endif
      endif
      call SetMet(0)
      go to 9999
9000  ich=1
9999  return
      end
      subroutine EM50Radiation
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*22  LabelRatio
      character*14  LabelWaveLength1
      character*11  LabelWaveLength
      character*80 Veta
      integer RadiationOld,EdwStateQuest,FeMenu,LblStateQuest
      logical CrwLogicQuest,lpom
      save nCrwRadFirst,nCrwRadLast,nButtTube,nCrwDouble,nEdwWL,nEdwWL1,
     1     nEdwWL2,nEdwRat,RadiationOld,nLblMono,nEdwMonAngle,nLblPol,
     2     nCrwPolarizationFirst,nCrwPolarizationLast,nButtSetMonAngle,
     3     nEdwPerfMon,LPFactorOld,nButtInfoPerpendicular,nEdwGAlpha,
     4     nButtInfoParallel,nEdwGBeta
      data LabelRatio/'%I(#2)/I(#1)'/,
     1     LabelWaveLength /'Wave length'/,
     2     LabelWaveLength1/'Wave length #1'/
      entry EM50RadiationMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      xpom=5.
      tpom=xpom+CrwgXd+10.
      ichk=1
      igrp=1
      il=1
      do i=1,3
        if(i.eq.1) then
          Veta='%X-rays'
          lpom=Radiation(1).eq.XRayRadiation
        else if(i.eq.2) then
          Veta='Ne%utrons'
          lpom=Radiation(1).eq.NeutronRadiation
        else if(i.eq.3) then
          Veta='%Electrons'
          lpom=Radiation(1).eq.ElectronRadiation
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        if(i.eq.1) then
          nCrwRadFirst=CrwLastMade
          ilb=il
        else
          xpomb=tpom+FeTxLength(Veta)+10.
        endif
        call FeQuestCrwOpen(CrwLastMade,lpom)
        il=il+1
      enddo
      nCrwRadLast=CrwLastMade
      Veta='X%-ray tube'
      dpomb=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
      nButtTube=ButtonLastMade
      tpom=5.
      Veta='Kalpha1/Kalpha2 dou%blet'
      xpom=tpom+CrwXd+10.
      ichk=1
      igrp=0
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,ichk,
     1                    igrp)
      nCrwDouble=CrwLastMade
      il=il+1
      xpom=tpom+FeTxLength(LabelWaveLength1)+10.
      dpom=100.
      Veta=LabelWaveLength
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwWL=EdwLastMade
      Veta=LabelWaveLength1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwWL1=EdwLastMade
      il=il+1
      i=idel(Veta)
      Veta(i:i)='2'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwWL2=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                    EdwYd,0)
      nEdwRat=EdwLastMade
      il=1
      xpom=xpom+dpom+45.
      tpom=xpom+CrwgXd+10.
      Veta='     Polarization correction:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      nLblPol=LblLastMade
      ichk=1
      igrp=2
      do i=1,5
        il=il+1
        if(i.eq.1) then
          Veta='Circul%ar polarization'
          j=PolarizedCircular
        else if(i.eq.2) then
          Veta='%Perpendicular setting'
         j=PolarizedMonoPer
        else if(i.eq.3) then
          Veta='Pa%rallel setting'
          j=PolarizedMonoPar
        else if(i.eq.4) then
          Veta='Guinier %camera'
          j=PolarizedGuinier
        else
          Veta='%Linearly polarized beam'
          j=PolarizedLinear
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,ichk,igrp)
        call FeQuestCrwOpen(CrwLastMade,LPFactor(1).eq.j)
        if(i.eq.1) then
          nCrwPolarizationFirst=CrwLastMade
        else if(i.eq.2.or.i.eq.3) then
          if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
          Veta='Info'
          dpomb=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
          if(i.eq.2) then
            nButtInfoPerpendicular=ButtonLastMade
          else
            nButtInfoParallel=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        endif
      enddo
      nCrwPolarizationLast=CrwLastMade
      il=il+1
      Veta='     Monochromator parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      call FeQuestLblOff(LblLastMade)
      nLblMono=LblLastMade
      il=il+1
      Veta='Per%fectness'
      xpom=tpom+FeTxLengthUnder(Veta)+35.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPerfMon=EdwLastMade
      il=il+1
      Veta='Glancing an%gle'
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMonAngle=EdwLastMade
      Veta='%Set glancing angle'
      dpomb=FeTxLengthUnder(Veta)+5.
      call FeQuestButtonMake(id,xpom+dpom+15.,il,dpomb,ButYd,Veta)
      nButtSetMonAngle=ButtonLastMade
      Veta='Alp%ha(Guinier)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwGAlpha=EdwLastMade
      il=il+1
      Veta='Beta(Gui%nier)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwGBeta=EdwLastMade
      RadiationOld=-1
      LPFactorOld=-1
      go to 2500
      entry EM50RadiationCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwRadFirst.and.CheckNumber.le.nCrwRadLast)
     2  then
        AllowChargeDensities=.false.
        if(CrwLogicQuest(nCrwRadFirst)) then
          Radiation(1)=XRayRadiation
          AllowChargeDensities=.true.
        else if(CrwLogicQuest(nCrwRadFirst+1)) then
          Radiation(1)=NeutronRadiation
        else
          Radiation(1)=ElectronRadiation
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              LPFactor(1)=PolarizedCircular
            else if(i.eq.2) then
              LPFactor(1)=PolarizedMonoPer
            else if(i.eq.3) then
              LPFactor(1)=PolarizedMonoPar
            else if(i.eq.4) then
              LPFactor(1)=PolarizedGuinier
            else
              LPFactor(1)=PolarizedLinear
            endif
            exit
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        if(CrwLogicQuest(nCrwDouble)) then
          NAlfa(1)=2
        else
          NAlfa(1)=1
        endif
        RadiationOld=-1
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          LamA1(1)=LamA1D(k)
          LamA2(1)=LamA2D(k)
          LamAve(1)=LamAveD(k)
          LamRat(1)=LamRatD(k)
          if(CrwLogicQuest(nCrwDouble)) then
            call FeQuestRealEdwOpen(nEdwWL1,LamA1(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwWL2,LamA2(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwRat,LamRat(1),.false.,
     1                              .false.)
          else
            call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,
     1                              .false.)
          endif
        endif
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(NAlfa(1).gt.1) then
          call FeQuestRealFromEdw(nEdwWL1,LamA1(1))
          call FeQuestRealFromEdw(nEdwWL2,LamA2(1))
          call FeQuestRealFromEdw(nEdwRat,LamRat(1))
          LamAve(1)=(LamA1(1)+
     1               LamA2(1)*LamRat(1))/(1.+LamRat(1))
        else
          call FeQuestRealFromEdw(nEdwWL,LamAve(1))
          LamA1(1)=LamAve(1)
          LamA2(1)=LamAve(2)
        endif
        AngleMonPom=AngleMon(1)
        call CrlCalcMonAngle(AngleMonPom,LamAve(1),ichp)
        if(ichp.ne.0) go to 9999
        AngleMon(1)=AngleMonPom
        call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(1),.false.,
     1                          .false.)
      endif
2500  if(Radiation(1).ne.RadiationOld) then
        if(Radiation(1).eq.XRayRadiation) then
          call FeQuestButtonOff(nButtTube)
          call FeQuestCrwOpen(nCrwDouble,NAlfa(1).gt.1)
          if(NAlfa(1).gt.1) then
            if(EdwStateQuest(nEdwWL1).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL)
              call FeQuestRealEdwOpen(nEdwWL1,LamA1(1),.false.,.false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2(1),.false.,.false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRat(1),.false.,.false.)
            endif
          else
            if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL1)
              call FeQuestEdwClose(nEdwWL2)
              call FeQuestEdwClose(nEdwRat)
              call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,.false.)
            endif
          endif
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
            call FeQuestEdwClose(nEdwWL1)
            call FeQuestEdwClose(nEdwWL2)
            call FeQuestEdwClose(nEdwRat)
            call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,.false.)
          endif
        endif
      endif
      if(Radiation(1).ne.RadiationOld.or.LPFactor(1).ne.LPFactorOld)
     1  then
        if(Radiation(1).eq.NeutronRadiation.or.
     1     Radiation(1).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            LPFactor(1)=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(LPFactor(1).eq.PolarizedMonoPer.or.
     1     LPFactor(1).eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(1),.false.,
     1                              .false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(1),.false.,
     1                              .false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(LPFactor(1).eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(1),.false.,
     1                              .false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,AlphaGMon(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwGBeta,BetaGMon(1),.false.,
     1                              .false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
      RadiationOld=Radiation(1)
      LPFactorOld=LPFactor(1)
      go to 9999
      entry EM50RadiationUpdate
      if(Radiation(1).eq.NeutronRadiation) NAlfa(1)=1
      if(NAlfa(1).gt.1) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(1))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(1))
        call FeQuestRealFromEdw(nEdwRat,LamRat(1))
        LamAve(1)=(LamA1(1)+LamRat(1)*LamA2(1))/(1.+LamRat(1))
      else
        call FeQuestRealFromEdw(nEdwWL,LamAve(1))
        LamA1(1)=LamAve(1)
        LamA2(1)=LamAve(2)
      endif
      klam(1)=LocateInArray(LamAve(1),LamAveD,7,.0001)
      if(LpFactor(1).ne.PolarizedCircular.and.
     1   LpFactor(1).ne.PolarizedLinear) then
        call FeQuestRealFromEdw(nEdwPerfMon,FractPerfMon(1))
        if(LpFactor(1).eq.PolarizedGuinier) then
          call FeQuestRealFromEdw(nEdwGAlpha,AlphaGMon(1))
          call FeQuestRealFromEdw(nEdwGBeta,BetaGMon(1))
        else
          call FeQuestRealFromEdw(nEdwMonAngle,AngleMon(1))
        endif
      endif
9999  return
      end
      subroutine EM50Symmetry
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      include 'datred.cmn'
      dimension rmp(36),sp(6),nx(4),ni(4),QuSymm(3,3),QuOld(3),
     1          QuPom(3,3),gpp(9),gppi(9)
      dimension rm6o(:,:),s6o(:,:),vt6o(:,:)
      allocatable rm6o,s6o,vt6o
      character*256 EdwStringQuest,SbwStringQuest
      character*80 Veta,VetaArr(2)
      character*60 GrupaOld,GrupaNew,Grp,GrpOut
      character*43 ::
     1  Text1='The operators derived from the group symbol',
     2  Text2='The group symbol derived from the operators'
      character*20 GrFull,GrOrigin,StQ(20)
      character*15 SymmCodePom(6)
      character*10 GrPerm
      character*8  GrShort
      character*6  GrPoint
      character*5  GrNum
      character*4  GrX4
      character*3  StQA(3,20),StP
      character*1  SmbL(8),SmbQ(3)
      integer ZeSymbolu,Zvenku,RolMenuSelectedQuest,UseTabsIn,
     1        SbwItemPointerQuest,SbwLnQuest
      logical eqrv,NicTamNeni,CrwLogicQuest,FirstTime,EqIgCase,
     1        AskForDelta,FeYesNoHeader,Change,FromEditM50,GrupaZListu,
     2        QCompatible,lpom
      real MetTens6NewI(36)
      save nEdwGrupa,nEdwShift,nEdwOperator,nButtSGList,
     1     nRolMenuCellCentr,nButtComplete,nButtDelete,nButtClean,
     2     nButtAdd,nButtRewrite,nButtRunTest,nButtLoad,nSbwSymmCodes,
     3     ZeSymbolu,ItemSel,nLblText,FromEditM50,IdSave,NDimShift,
     4     nButtLocalSymm,nButtForStokes
      data SmbL/'P','A','B','C','I','R','F','X'/,SmbQ/'a','b','g'/,
     1     GrupaOld/' '/
      equivalence (Veta,VetaArr(1))
      entry EM50SymmetryMake(id,Key)
      FromEditM50=Key.eq.0
      if(.not.FromEditM50) IdSave=id
      GrupaOld=Grupa(KPhase)
      if(NDim(KPhase).ne.4) then
        Veta='Space %group'
      else
        Veta='Superspace %group'
      endif
      il=1
      VetaArr(2)='%Select from list'
      dpom=FeTxLengthUnder(VetaArr(2))+10.
      tpomp=5.
      xpomp=FeTxLengthUnder(Veta)+15.
      dpomp=QuestXLen(id)-xpomp-dpom-15.
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpomp,EdwYd,1)
      nEdwGrupa=EdwLastMade
      NicTamNeni=index(Grupa(KPhase),'?').gt.0
      Veta=VetaArr(2)
      tpom=xpomp+dpomp+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtSGList=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      tpom=tpomp
      Veta='%Origin shift'
      xpom=xpomp
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwShift=EdwLastMade
      NDimShift=NDim(KPhase)
      if(NDimShift.gt.4) NDimShift=3
      call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                         NicTamNeni,.true.)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,QuestXLen(id)*.5,il,' ','C','N')
      nLblText=LblLastMade
      if(NicTamNeni) then
        ZeSymbolu=0
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
        if(Grupa(KPhase).eq.' ') then
          ZeSymbolu=0
        else
          call FeQuestLblChange(nLblText,Text1)
          ZeSymbolu=1
        endif
      endif
      xpom=5.
      dpom=300.
      call FeQuestSbwMake(id,xpom,15,dpom,11,1,CutTextFromRight,
     1                    SbwVertical)
      nSbwSymmCodes=SbwLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l52','formatted','unknown')
      do i=1,NSymm(KPhase)
        call MakeSymmSt(Veta,symmc(1,i,1,KPhase))
        if(MagneticType(KPhase).gt.0.and..not.ParentStructure) then
          if(ZMag(i,1,KPhase).gt.0) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(Cislo,100) i
        call Zhusti(Cislo)
        write(ln,FormA1)(Cislo(j:j),j=1,idel(Cislo)),' ',
     1                  (Veta(j:j),j=1,idel(Veta))
      enddo
      close(ln)
      call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'.l52')
      tpom=xpom+dpom+30.
      tpomp=tpom
      il=5
      Veta='%Load ->'
      dpom=FeTxLengthUnder(Veta)+5.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtLoad=ButtonLastMade
      xpom=tpom+dpom+15.
      dpom=QuestXLen(id)-xpom-5.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwOperator=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      Veta='<- %Add'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdd=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='<- %Rewrite'
      xpom=xpom+dpom+20.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRewrite=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      tpom=tpom+15.
      Veta='%Delete operator'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='%Clean out'
      xpom=tpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtClean=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
      il=il+1
      Veta='C%ell centering'
      dpom=30.+EdwYd
      xpom=tpomp+FeTxLength(Veta)+10.
      call FeQuestRolMenuMake(id,tpomp,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuCellCentr=RolMenuLastMade
      i=LocateInStringArray(SmbL,8,Lattice(KPhase),IgnoreCaseYes)
      call FeQuestRolMenuOpen(RolMenuLastMade,SmbL,8,i)
      xpom=xpom+dpom+5.
      il=il+1
      call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
      il=il+1
      Veta='Com%plete the set'
      dpom=220.
      xpom=(tpomp+QuestXLen(id)-5.-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtComplete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRunTest=0
      nButtForStokes=0
      if(.not.FromEditM50.or.NRefBlock.eq.1) then
        if(DifCode(KRefBlock).lt.100) then
          il=il+1
          Veta='%Make test'
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          nButtRunTest=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestMouseToButton(ButtonLastMade)
          if(.not.ExistM95) call FeQuestButtonDisable(ButtonLastMade)
        endif
      endif
      if(NDimI(KPhase).gt.0) then
        il=il+1
        Veta='Run Stokes & Camp%bell SSG-test'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtForStokes=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestMouseToButton(ButtonLastMade)
      endif
      if(FromEditM50) then
        il=il+1
        call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
        il=il+1
        Veta='De%fine local symmetry operators'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtLocalSymm=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtLocSymm=0
      endif
      go to 9999
      entry EM50SymmetryRefresh
      i=nSbwSymmCodes+SbwFr-1
      ItemFromOld=SbwItemFrom(i)
      ItemSelOld=SbwItemPointer(i)
      call CloseIfOpened(SbwLn(i))
      ln=NextLogicNumber()
      call OpenFile(ln,SbwFile(i),'formatted','unknown')
      do j=1,NSymm(KPhase)
        call MakeSymmSt(Veta,symmc(1,j,1,KPhase))
        if(MagneticType(KPhase).gt.0.and..not.ParentStructure) then
          if(ZMag(j,1,KPhase).gt.0) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(Cislo,100) j
        call Zhusti(Cislo)
        write(ln,FormA1)(Cislo(k:k),k=1,idel(Cislo)),' ',
     1                  (Veta(k:k),k=1,idel(Veta))
      enddo
      close(ln)
      call FeQuestSbwMenuOpen(nSbwSymmCodes,SbwFile(i))
      call FeQuestSbwShow(nSbwSymmCodes,ItemFromOld)
      call FeQuestSbwItemOff(nSbwSymmCodes,SbwItemFrom(i))
      call FeQuestSbwItemOn(nSbwSymmCodes,ItemSelOld)
      n=RolMenuSelectedQuest(nRolMenuCellCentr)
      i=LocateInStringArray(SmbL,8,Lattice(KPhase),IgnoreCaseYes)
      if(i.ne.n) call FeQuestRolMenuOpen(nRolMenuCellCentr,SmbL,8,i)
      if(Grupa(KPhase).eq.' '.or.index(Grupa(KPhase),'?').gt.0) then
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      if(NSymm(KPhase).gt.0) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtLoad,i)
      go to 9999
      entry EM50SymmetryCheck
      if(FromEditM50) then
        id=KartId
      else
        id=IdSave
      endif
      GrupaZListu=.false.
2000  if((CheckType.eq.EventEdw.and.
     1    (CheckNumber.eq.nEdwGrupa.or.CheckNumber.eq.nEdwShift)).or.
     3   GrupaZListu) then
        Grupa(KPhase)=EdwStringQuest(nEdwGrupa)
        if(Grupa(KPhase).eq.' ') go to 3250
        if(CheckNumber.ne.nEdwGrupa.or.
     1     EventTypeSave.eq.EventCrw.or.
     2     EventTypeSave.eq.EventRolMenu.or.
     3     (EventTypeSave.eq.EventButton.and.
     4      EventNumberSave.eq.nButtClean)) then
          AskForDelta=.false.
        else
          AskForDelta=.true.
        endif
        if(GrupaZListu) AskForDelta=.true.
        if(EdwStringQuest(nEdwShift).eq.' ') then
          call SetRealArrayTo(shsg(1,KPhase),NDim(KPhase),0.)
          call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                             .false.,.true.)
        endif
        call FeQuestRealAFromEdw(nEdwShift,shsg(1,KPhase))
        StdSg(KPhase)=1
        do i=1,NDim(KPhase)
          if(abs(shsg(i,KPhase)).gt..0001) then
            StdSg(KPhase)=0
            go to 2015
          endif
        enddo
2015    Grupa(KPhase)=EdwStringQuest(nEdwGrupa)
        FirstTime=.not.EqIgCase(Grupa(KPhase),GrupaOld).and.
     1            .not.GrupaZListu
        if(FromEditM50.and.(EventType.eq.EventKartSw.and.
     1      EventNumber.eq.KartIdCell+1-KartFirstId))
     2    QuestCheck(id)=-1
        GrupaOld=Grupa(KPhase)
        call EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
        if(ich.eq.0) then
          call EM50MakeStandardOrder
          call CrlOrderMagSymmetry
          if(Grupa(KPhase).ne.' '.and.ZeSymbolu.ne.1) then
            ZeSymbolu=1
            call FeQuestLblChange(nLblText,Text1)
          endif
        else
          EdwLastCheck=1
          if(QuestCheck(Id).ne.-1) then
            if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwGrupa.or.CheckNumber.eq.nEdwShift))
     2        then
              EventType=EventEdw
              EventNumber=nEdwGrupa
            endif
            ErrFlag=1
          endif
          GrupaOld=' '
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwOperator)
     1  then
        Veta=EdwStringQuest(CheckNumber)
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuCellCentr) then
        n=RolMenuSelectedQuest(nRolMenuCellCentr)
        go to 3100
      else if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtSGList) then
          UseTabsIn=UseTabs
          UseTabs=NextTabs()
          xpom=0.
          do i=1,4
            if(i.eq.1) then
              pom=4.
            else if(i.eq.2) then
              pom=8.
            else if(i.eq.3) then
              pom=12.
            else if(i.eq.4) then
              pom=7.
            endif
            xpom=xpom+pom*PropFontWidthInPixels
            call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
            if(i.eq.4) then
              xpom=xpom+5.*PropFontWidthInPixels
            else
              xpom=xpom+PropFontWidthInPixels
            endif
            call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
          enddo
          idp=NextQuestId()
          xqdp=xpom+100.
          Veta='Select space group'
          il=20
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
          il=1
          tpom=15.
          call FeQuestLblMake(idp,tpom,il,'#','L','N')
          tpom=tpom+4.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Short symbol','L','N')
          tpom=tpom+9.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Full symbol','L','N')
          tpom=tpom+14.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Permutation','L','N')
          tpom=tpom+10.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Origin at','L','N')
          xpom=5.
          dpom=xqdp-10.-SbwPruhXd
          il=20
          call FeQuestSbwMake(idp,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                        SbwVertical)
          nSbw=SbwLastMade
          SbwDoubleClickAllowed=.true.
          lni=NextLogicNumber()
          if(OpSystem.le.0) then
            Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1           'spgroup_list.dat'
          else
            Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_list.dat'
          endif
          call OpenFile(lni,Veta,'formatted','old')
          if(ErrFlag.ne.0) go to 9999
          read(lni,FormA) Veta
          if(Veta(1:1).ne.'#') rewind lni
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'_list.tmp','formatted',
     1                  'unknown')
2100      read(lni,101,end=2120) GrNum,GrShort,GrFull,GrPerm,GrPoint,
     1                           GrOrigin
          Cislo=GrNum
          i=index(Cislo,'/')
          if(i.gt.0) Cislo(i:)=' '
          call Posun(Cislo,0)
          read(Cislo,FormI15) isg
          if(NDimI(KPhase).eq.1.and.isg.ge.195) go to 2120
          write(lno,'(17(a))')
     1      GrNum(:idel(GrNum)),Tabulator,'|',Tabulator,
     2      GrShort(:idel(GrShort)),Tabulator,'|',Tabulator,
     3      GrFull(:idel(GrFull)),Tabulator,'|',Tabulator,
     4      GrPerm(:max(idel(GrPerm),1)),Tabulator,'|',Tabulator,
     5      GrPoint(:max(idel(GrPoint),1))
          go to 2100
2120      call CloseIfOpened(lno)
          call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_list.tmp')
2200      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventSbwDoubleClick) then
            ich=0
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2200
          endif
          if(ich.eq.0) then
            n=SbwItemPointerQuest(nSbw)
            rewind lni
            read(lni,FormA) Veta
            if(Veta(1:1).ne.'#') rewind lni
            do i=1,n
              read(lni,101,end=2120) GrNum,GrShort,GrFull,GrPerm,
     1                               GrPoint,GrOrigin
            enddo
            Cislo=GrNum
            i=index(Cislo,'/')
            if(i.gt.0) Cislo(i:)=' '
            call Posun(Cislo,0)
            read(Cislo,FormI15) isg
            if(isg.ge.3.and.isg.le.15) then
              k=0
              call Kus(GrFull,k,Cislo)
              do i=1,3
                call Kus(GrFull,k,Cislo)
                if(.not.EqIgCase(Cislo,'1')) then
                  Monoclinic(KPhase)=i
                  exit
                endif
              enddo
            endif
            if(NDimI(KPhase).eq.1) then
              i=index(GrNum,'/')
              if(i.gt.0) GrNum(i:)=' '
              call Posun(GrNum,0)
              read(GrNum,102) isg
              if(isg.le.2) then
                Veta=GrShort(:idel(GrShort))//'(abg)0'
                go to 2250
              else if(isg.le.74) then
                nq=0
                if(EqIgCase(GrShort(1:1),'P')) then
                  StP='1/2'
                else
                  StP='1'
                endif
                if(isg.le.15) then
                  nq=nq+1
                  call CopyStringArray(SmbQ,StQA(1,nq),3)
                  StQA(Monoclinic(KPhase),nq)='0'
                  nq=nq+1
                  call CopyStringArray(StQA(1,1),StQA(1,nq),3)
                  StQA(Monoclinic(KPhase),nq)=StP
                  ip=Monoclinic(KPhase)
                  ik=Monoclinic(KPhase)
                else
                  ip=1
                  ik=3
                endif
                do i=ip,ik
                  i1=mod(i,3)+1
                  i2=6-i1-i
                  nq=nq+1
                  call SetStringArrayTo(StQA(1,nq),3,'0')
                  StQA(i,nq)=SmbQ(i)
                  nqp=nq
                  do j=1,3
                    nq=nq+1
                    call CopyStringArray(StQA(1,nqp),StQA(1,nq),3)
                    if(j.eq.1) then
                      StQA(i1,nq)=StP
                    else if(j.eq.2) then
                      StQA(i2,nq)=StP
                    else if(j.eq.3) then
                      StQA(i1,nq)=StP
                      StQA(i2,nq)=StP
                    endif
                  enddo
                enddo
                do j=1,nq
                  StQ(j)='('
                  do i=1,3
                    StQ(j)=StQ(j)(:idel(StQ(j)))//
     1                     StQA(i,j)(:idel(StQA(i,j)))
                  enddo
                  StQ(j)=StQ(j)(:idel(StQ(j)))//')'
                enddo
              else if(isg.le.142) then
                StQ(1)='(00g)'
                StQ(2)='(1/21/2g)'
                nq=2
              else if(isg.le.167) then
                StQ(1)='(00g)'
                StQ(2)='(1/31/3g)'
                nq=2
              else
                StQ(1)='(00g)'
                nq=1
              endif
              if(isg.le. 9.or.
     1          (isg.ge. 75.and.isg.le. 82).or.
     2          (isg.ge.143.and.isg.le.148).or.
     3          (isg.ge.168.and.isg.le.174)) then
                nn=1
              else if((isg.ge. 10.and.isg.le. 15).or.
     1                (isg.ge. 83.and.isg.le. 88).or.
     2                (isg.ge.175.and.isg.le.176).or.
     2                 GrShort(1:1).eq.'R') then
                nn=2
              else if((isg.ge. 16.and.isg.le. 74).or.
     1                (isg.ge. 89.and.isg.le.122).or.
     2                (isg.ge.149.and.isg.le.167).or.
     3                (isg.ge.177.and.isg.le.190)) then
                nn=3
              else
                nn=4
              endif
              if(iPGSel.le.15) then
                m=3
              else
                m=5
              endif
              if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
              allocate(rm6o(NDimQ(KPhase),NSymm(KPhase)),
     1                 s6o(NDim(KPhase),NSymm(KPhase)),
     2                 vt6o(NDim(KPhase),NLattVec(KPhase)))
              nso=NSymm(KPhase)
              nvto=NLattVec(KPhase)
              do i=1,NLattVec(KPhase)
                call CopyVek(vt6(1,i,1,KPhase),vt6o(1,i),NDim(KPhase))
              enddo
              do i=1,NSymm(KPhase)
                call CopyMat(rm6(1,i,1,KPhase),rm6o(1,i),NDim(KPhase))
                call CopyVek(s6(1,i,1,KPhase),s6o(1,i),NDim(KPhase))
              enddo
              lno=NextLogicNumber()
              call OpenFile(lno,fln(:ifln)//'_list_all.tmp','formatted',
     1                      'unknown')
              if(ErrFlag.ne.0) go to 9999
              lnq=NextLogicNumber()
              call OpenFile(lnq,fln(:ifln)//'_list_q.tmp','formatted',
     1                      'unknown')
              if(ErrFlag.ne.0) go to 9999
              call SetIntArrayTo(nx,nn,m)
              n=m**nn
              call SetIgnoreWTo(.true.)
              call SetIgnoreETo(.true.)
              ns4=0
              ns4q=0
              do iq=1,nq
                i=idel(StQ(iq))
                Grp=GrShort(:idel(GrShort))//StQ(iq)(:i)
                idl=idel(Grp)
                QCompatible=.false.
                Cislo=StQ(iq)(2:i-1)
                call mala(Cislo)
                do i=1,3
                  if(idel(Cislo).le.0) go to 2240
                  k=1
                  if(Cislo(k:k).eq.SmbQ(i)) k=k+1
                  k=k-1
                  if(k.eq.0) then
                    j=index(Cislo,'/')
                    if(j.le.0.or.j.gt.3.or.
     1                 (j.eq.3.and.Cislo(1:1).ne.'-')) then
                      k=1
                    else
                      k=j+1
                    endif
                    pom=fract(Cislo(:k),ich)
                    if(ich.ne.0) go to 2240
                    if(abs(pom-Qu(i,1,1,KPhase)).gt..001) go to 2240
                  endif
                  Cislo=Cislo(k+1:)
                enddo
                QCompatible=.true.
2240            do i=1,n
                  call RecUnPack(i,ni,nx,nn)
                  GrX4=' '
                  do j=1,nn
                    k=ni(j)
                    GrX4(j:j)=SmbSymmT(k)
                  enddo
                  Grupa(KPhase)=Grp(:idl)//GrX4(:nn)
                  call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,
     1                            QuSymm,ichp)
                  if(ichp.ne.0) cycle
                  if(NDim(KPhase).eq.4) then
                    call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
                    call CopyVek(QuSymm,Qu(1,1,1,KPhase),
     1                           3*NDimI(KPhase))
                  endif
                  call FindSmbSgOrgShiftNo(GrpOut,.false.,1)
                  if(NDim(KPhase).eq.4)
     1              call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
                  do j=1,idel(GrpOut)
                    if(EqIgCase(GrpOut(j:j),'e'))
     1                GrpOut(j:j)=Grupa(KPhase)(j:j)
                  enddo
                  if(EqIgCase(GrpOut,Grupa(KPhase))) then
                    ns4=ns4+1
                    if(ns4.eq.1) Veta=Grupa(KPhase)
                    write(lno,'(a)') Grupa(KPhase)(:idel(Grupa(KPhase)))
                    if(QCompatible) then
                      ns4q=ns4q+1
                      write(lnq,'(a)')
     1                  Grupa(KPhase)(:idel(Grupa(KPhase)))
                    endif
                  endif
                enddo
              enddo
              NSymm(KPhase)=nso
              NLattVec(KPhase)=nvto
              do i=1,NLattVec(KPhase)
                call CopyVek(vt6o(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
              enddo
              do i=1,NSymm(KPhase)
                call CopyMat(rm6o(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
                call CopyVek(s6o(1,i),s6(1,i,1,KPhase),NDim(KPhase))
                call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                        symmc(1,i,1,KPhase),0)
              enddo
              Grupa(KPhase)=GrupaOld
              Lattice(KPhase)=GrupaOld(1:1)
              if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
            else
              lnq=0
              Veta=GrShort
            endif
          endif
2250      call FeQuestRemove(idp)
          call CloseIfOpened(lni)
          call CloseIfOpened(lno)
          call CloseIfOpened(lnq)
          call SetIgnoreWTo(.false.)
          call SetIgnoreETo(.false.)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsIn
          if(ich.eq.0) then
            if(NDimI(KPhase).eq.1.and.isg.ge.3) then
              if(ns4.gt.1) then
                idp=NextQuestId()
                xqdp=250.
                Veta='Select superspace group'
                ilk=min(ifix((float(ns4)*MenuLineWidth+2.)/
     1                        QuestLineWidth)+1,14)
                if(ns4q.gt.0) ilk=ilk+2
                call FeQuestCreate(idp,-1.,-1.,xqdp,ilk,Veta,0,
     1                             LightGray,0,0)
                xpom=5.
                dpom=xqdp-10.-SbwPruhXd
                il=ilk
                if(ns4q.gt.0) il=il-2
                call FeQuestSbwMake(idp,xpom,il,dpom,il,1,
     1                              CutTextFromLeft,SbwVertical)
                nSbw=SbwLastMade
                SbwDoubleClickAllowed=.true.
                call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                  '_list_all.tmp')
                if(ns4q.gt.0) then
                  xpom=5.
                  tpom=xpom+CrwgXd+5.
                  do i=1,2
                    il=il+1
                    if(i.eq.1) then
                      Veta='Show %relevant superspace groups'
                    else
                      nCrwFirst=CrwLastMade
                      Veta='Show q-%compatible superspace groups'
                    endif
                    call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',
     1                                  CrwgXd,CrwgYd,1,1)
                    call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                  enddo
                else
                  nCrwFirst=0
                endif
2300            call FeQuestEvent(idp,ichp)
                if(CheckType.eq.EventCrw) then
                  call CloseIfOpened(SbwLnQuest(nSbw))
                  if(CheckNumber.eq.nCrwFirst) then
                    call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                      '_list_all.tmp')
                  else
                    call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                      '_list_q.tmp')
                  endif
                  go to 2300
                else if(CheckType.eq.EventSbwDoubleClick) then
                  ichp=0
                else if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 2300
                endif
                if(ichp.eq.0) then
                  n=SbwItemPointerQuest(nSbw)
                  if(ns4q.gt.0) lpom=CrwLogicQuest(nCrwFirst)
                endif
                call FeQuestRemove(idp)
                if(ichp.eq.0) then
                  if(ns4q.gt.0) then
                    if(lpom) then
                      Veta=fln(:ifln)//'_list_all.tmp'
                    else
                      Veta=fln(:ifln)//'_list_q.tmp'
                    endif
                  else
                    Veta=fln(:ifln)//'_list_all.tmp'
                  endif
                  call OpenFile(lno,Veta,'formatted','unknown')
                  do i=1,n
                    read(lno,FormA) Veta
                  enddo
                  call CloseIfOpened(lno)
                else
                  go to 9999
                endif
              else if(ns4.le.0) then
                call FeUnforeseenError('No corresponding superspace '//
     1                                 'group found.')
                go to 9999
              endif
            endif
            call DeleteFile(fln(:ifln)//'_list_all.tmp')
            call DeleteFile(fln(:ifln)//'_list_q.tmp')
            GrupaZListu=.true.
            call FeQuestStringEdwOpen(nEdwGrupa,Veta)
            call SetRealArrayTo(shsg(1,KPhase),NDim(KPhase),0.)
            n=idel(GrOrigin)
            if(n.gt.0) then
              do i=1,n
                if(GrOrigin(i:i).eq.',') GrOrigin(i:i)=' '
              enddo
              k=0
              call StToReal(GrOrigin,k,shsg(1,KPhase),3,.false.,ich)
            endif
            call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                               .false.,.true.)
            go to 2000
          else
            go to 9999
          endif
        else if(CheckNumber.eq.nButtComplete) then
          Zvenku=0
          go to 3000
        else if(CheckNumber.eq.nButtDelete) then
          i=nSbwSymmCodes+SbwFr-1
          ItemSel=SbwItemPointer(i)
          if(ItemSel.ne.1) then
            do i=ItemSel+1,NSymm(KPhase)
              call CopySymmOperator(i,i-1,1)
            enddo
            NSymm(KPhase)=NSymm(KPhase)-1
          endif
          go to 3250
        else if(CheckNumber.eq.nButtClean) then
          NSymm(KPhase)=1
          go to 3250
        else if(CheckNumber.eq.nButtAdd.or.CheckNumber.eq.nButtRewrite)
     1    then
          Veta=EdwStringQuest(nEdwOperator)
          call mala(Veta)
          if(Veta.ne.' ') then
            call ReadSymm(Veta,rmp,sp,SymmCodePom,ZMagP,0)
            if(ErrFlag.ne.0) go to 2550
            call od0do1(sp,sp,NDim(KPhase))
            do i=1,NSymm(KPhase)
              if(eqrv(sp,s6(1,i,1,KPhase),NDim(KPhase),.0001).and.
     1           eqrv(rmp,rm6(1,i,1,KPhase),NDimQ(KPhase),.001)) then
                if(abs(ZMagP-ZMag(i,1,KPhase)).lt..001) go to 2500
              endif
            enddo
            i=nSbwSymmCodes+SbwFr-1
            ItemFromOld=SbwItemFrom(i)
            ItemSelOld=SbwItemPointer(i)
            call CloseIfOpened(SbwLn(i))
            if(CheckNumber.eq.nButtRewrite) then
              ItemSel=ItemSelOld
              if(ItemSel.eq.1) go to 2600
            else
              NSymm(KPhase)=NSymm(KPhase)+1
              call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1          NLattVec(KPhase),NComp(KPhase),NPhase,1)
              ISwSymm(NSymm(KPhase),1,KPhase)=1
              ItemSel=NSymm(KPhase)
              write(Cislo,100) ItemSel
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' '//Veta(:idel(Veta))
              call AppendFile(SbwFile(i),VetaArr,1)
            endif
            call FeQuestStringEdwOpen(nEdwOperator,' ')
            write(Cislo,100) ItemSel
            call Zhusti(Cislo)
            Veta=Cislo(:idel(Cislo))//' '//Veta(:idel(Veta))
            if(CheckNumber.eq.nButtRewrite) then
              call RewriteLinesOnFile(SbwFile(i),ItemSelOld,
     1                                ItemSelOld,VetaArr,1)
            else
              call AppendFile(SbwFile(i),VetaArr,1)
            endif
            call CopyMat(rmp,rm6(1,ItemSel,1,KPhase),NDim(KPhase))
            call CopyVek(sp,s6(1,ItemSel,1,KPhase),NDim(KPhase))
            call CopyStringArray(SymmCodePom,symmc(1,ItemSel,1,KPhase),
     1                           NDim(KPhase))
            ZMag(ItemSel,1,KPhase)=ZMagP
            call FeQuestSbwMenuOpen(nSbwSymmCodes,SbwFile(i))
            call FeQuestSbwShow(nSbwSymmCodes,ItemFromOld)
            call FeQuestSbwItemOff(nSbwSymmCodes,SbwItemFrom(i))
            call FeQuestSbwItemOn(nSbwSymmCodes,ItemSelOld)
            go to 2600
          endif
          go to 2550
2500      call FeChybne(-1.,-1.,'the symmetry operator already present',
     1                  'try again.',SeriousError)
2550      EventNumber=nEdwOperator
          EventType=EventEdw
          go to 9999
        else if(CheckNumber.eq.nButtLoad) then
          i=nSbwSymmCodes+SbwFr-1
          ItemSel=SbwItemPointer(i)
          Veta=SbwStringQuest(ItemSel,i)
          k=0
          call kus(Veta,k,Cislo)
          Veta=Veta(k+1:)
          call FeQuestStringEdwOpen(nEdwOperator,Veta)
          EventNumber=nEdwOperator
          EventType=EventEdw
          go to 9999
        else if(CheckNumber.eq.nButtRunTest) then
          if(FromEditM50) then
            k=1
          else
            k=0
          endif
          if(NRefBlock.gt.1.and.k.eq.0) then
            call SetMenuRefBlock
            TextInfo(1)='Select refblock to be used for the space '//
     1                  'group test'
            call CopyStringArray(MenuRefBlock,TextInfo(2),NRefBlock)
            NInfo=NRefBlock+1
            allocate(UseInSGTest(NRefBlock))
            call FeSelectFromOffer(-1.,-1.,1,UseInSGTest,ich)
            if(ich.ne.0) go to 9999
          endif
          call DRSGTest(k,Change)
          if(Change) then
            call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
            call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                               .false.,.true.)
            GrupaOld=Grupa(KPhase)
          endif
          if(allocated(UseInSGTest)) deallocate(UseInSGTest)
          go to 9999
        else if(CheckNumber.eq.nButtForStokes) then
          call EM50SymbolsForStokes
          go to 9999
        else if(CheckNumber.eq.nButtLocalSymm) then
          call EM50DefineLocalSymm
        endif
2600    EventNumber=nEdwOperator
        EventType=EventEdw
        go to 3250
      else if(CheckType.eq.EventCrw) then
        go to 3200
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 9999
      entry EM50SymmetryComplete
      if(ZeSymbolu.eq.1.and.Grupa(KPhase).ne.' ') go to 9999
      Zvenku=1
3000  call CompleteSymm(0,ich)
      if(ich.eq.0) then
        go to 3010
      else
        go to 3050
      endif
3010  GrupaNew='???'
      if(NDimI(KPhase).gt.0) then
        call CopyVek(Qu(1,1,1,KPhase),QuPom,3*NDimI(KPhase))
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call EM50MetTensSymm(MetTens6NewI)
        call MatBlock3(MetTens6NewI,gppi,NDim(KPhase))
        call MatInv(gppi,gpp,det,3)
        m=3*NDim(KPhase)+1
        do i=1,NDimI(KPhase)
          call CopyVek(MetTens6NewI(m),gppi,3)
          call multm(gpp,MetTens6NewI(m),Qu(1,i,1,KPhase),3,3,1)
          m=m+NDim(KPhase)
        enddo
      endif
      call FindSmbSg(GrupaNew,ChangeOrderYes,1)
      if(NDimI(KPhase).gt.0)
     1  call CopyVek(QuPom,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      if(index(GrupaNew,'?').gt.0) go to 3040
      if(Zvenku.eq.0.and..not.EqIgCase(GrupaNew,Grupa(KPhase)).and.
     1  Grupa(KPhase).ne.' '.and.Grupa(KPhase)(1:1).ne.'?') then
        NInfo=2
        TextInfo(1)=Grupa(KPhase)(:idel(Grupa(KPhase)))//'->'//
     1              GrupaNew(:idel(GrupaNew))
        TextInfo(2)='The program is offering you the standard space '//
     1              'group symbol'
        if(FeYesNoHeader(-1.,-1.,
     1                   'Do you want to accept the new symbol?',0))
     2    then
          Grupa(KPhase)=GrupaNew
        else
          go to 3040
        endif
      else
        Grupa(KPhase)=GrupaNew
      endif
      if(ZeSymbolu.eq.1) call FeQuestLblOff(nLblText)
      if(Grupa(KPhase).eq.' '.or.index(Grupa(KPhase),'?').gt.0) then
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      n=NDim(KPhase)
      if(n.gt.4) n=3
      call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),n,.false.,
     1                         .true.)
      if(ZeSymbolu.ne.-1) then
        ZeSymbolu=-1
        call FeQuestLblChange(nLblText,Text2)
      endif
3040  if(Zvenku.eq.1) go to 9999
3050  EventType=EventEdw
      EventNumber=nEdwOperator
      go to 9999
3100  if(Lattice(KPhase).ne.SmbL(n).or.Lattice(KPhase).eq.'X') then
        call EM50GenVecCentr(n,.true.,ich)
        Lattice(KPhase)=SmbL(n)
        Grupa(KPhase)=' '
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
        Zvenku=0
        go to 3000
      else
        go to 9999
      endif
3200  EventType=EventEdw
      EventNumber=nEdwOperator
3250  if(EdwStringQuest(nEdwGrupa).ne.' ') then
        if(ZeSymbolu.eq.1) then
          call FeQuestLblOff(nLblText)
        else if(ZeSymbolu.eq.-1) then
          call FeQuestLblOff(nLblText)
        endif
        ZeSymbolu=0
        Grupa(KPhase)=' '
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      if(EdwStringQuest(nEdwShift).ne.' ') then
        n=NDim(KPhase)
        if(n.gt.4) n=3
        call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),n,.true.,
     1                           .true.)
      endif
9999  return
100   format('(',i5,')')
101   format(a5,1x,a8,1x,a20,1x,a10,1x,a6,1x,a20)
102   format(i5)
      end
      subroutine EM50SymbolsForStokes
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*80  Veta
      character*1 :: smbs(6) = (/'x','y','z','t','u','v'/),String(32767)
      logical AbsYes,Prvni
      ln=NextLogicNumber()
      lno=0
      call OpenFile(ln,fln(:ifln)//'_stokes.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      call FeFillTextInfo('forstokes.txt',0)
      do i=1,NInfo
        write(ln,FormA) TextInfo(i)(:idel(TextInfo(i)))
      enddo
      write(ln,'(/''Centering vectors:''/)')
      do i=2,NLattVec(KPhase)
        Veta=' '
        do j=1,NDim(KPhase)
          call ToFract(vt6(j,i,1,KPhase),Cislo,6)
          if(Veta.eq.' ') then
            Veta=Cislo
          else
            Veta=Veta(:idel(Veta))//','//Cislo(:idel(Cislo))
          endif
        enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      write(ln,'(/''Symmetry operations:''/)')
      do is=2,NSymmN(KPhase)
        Veta=' '
        do i=1,NDim(KPhase)
          pom=s6(i,is,1,KPhase)
          if(pom.ne.0) then
            call ToFract(pom,Cislo,6)
            if(Veta.eq.' ') then
              Veta=Cislo
            else
              Veta=Veta(:idel(Veta))//','//Cislo(:idel(Cislo))
            endif
            AbsYes=.true.
          else
            if(Veta.ne.' ') Veta=Veta(:idel(Veta))//','
            AbsYes=.false.
          endif
          k=i-NDim(KPhase)
          Prvni=.true.
          do j=1,NDim(KPhase)
            k=k+NDim(KPhase)
            pom=rm6(k,is,1,KPhase)
            if(pom.eq.0.) then
              cycle
            else
              call ToFract(pom,Cislo,6)
            endif
            if(pom.gt.0.) then
              if(.not.Prvni.or.AbsYes) Veta=Veta(:idel(Veta))//'+'
              if(Cislo.ne.'1')
     1          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
            else
              if(Cislo(2:2).eq.'1') then
                Veta=Veta(:idel(Veta))//'-'
              else
                Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
              endif
            endif
            Prvni=.false.
            Veta=Veta(:idel(Veta))//smbs(j)
          enddo
          enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
!      if(CallWGet.ne.' ') then
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_stokes.txt','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 9999
        rewind ln
        k=0
        call FillStringArray(String,k,'centering=')
1500    read(ln,FormA,end=9999) Veta
        if(LocateSubstring(Veta,'Centering vectors:',.false.,.true.)
     1     .le.0) go to 1500
1600    read(ln,FormA,end=9999) Veta
        if(Veta.eq.' ') go to 1600
        if(LocateSubstring(Veta,'Symmetry operations:',.false.,.true.)
     1     .le.0) then
          Veta=Veta(:idel(Veta))//';'
          call TranslateToPostData(Veta,t256)
          call FillStringArray(String,k,t256)
          go to 1600
        endif
        call FillStringArray(String,k,'&ops=')
1700    read(ln,FormA,end=2000) Veta
        if(Veta.eq.' ') go to 1700
        Veta=Veta(:idel(Veta))//';'
        call TranslateToPostData(Veta,t256)
        call FillStringArray(String,k,t256)
        go to 1700
2000    write(lno,'(32767a1)') String(1:k)
        close(ln,status='delete')
        call CloseIfOpened(lno)
        t256=HomeDir(:idel(HomeDir))//'gnu'//ObrLom//'wget'//ObrLom//
     1       'wget.exe'
        t256='"'//t256(:idel(t256))//'" -q '//
     1       'http://stokes.byu.edu/iso/findssgform.php --post-file '//
     2       fln(:ifln)//'_stokes.txt -O '//fln(:ifln)//'_stokes.html'
        WaitTime=5000
        NInfo=3
        TextInfo(1)='Starting GNU Wget'
        TextInfo(2)='Free software for retrieving files using HTTP, '//
     1              'HTTPS and FTP.'
        TextInfo(3)='For details see http://www.gnu.org/software/wget'
        call FeInfoOut(-1.,-1.,'INFORMATION','C')
        call FeSystem(t256)
        Veta=fln(:ifln)//'_stokes.html'
        call FeShellExecute(Veta)
!      else
!        NInfo=4
!        TextInfo(1)='Important note : after downloading, installing '//
!     1              'WGet program'
!        TextInfo(2)='(e.g. http://gnuwin32.sourceforge.net/packages/'//
!     1              'wget.htm) and'
!        TextInfo(3)='defining WGet command in Jana2006 (Tools->'//
!     1              'Programs)'
!        TextInfo(4)='you can make SSG-check faster without pasting '//
!     1              'data.'
!        call FeInfoOut(-1.,-1.,'INFORMATION','L')
!        call CloseIfOpened(ln)
!        Veta='http://stokes.byu.edu/iso/findssg.html'
!        call FeShellExecute(Veta)
!        Veta=fln(:ifln)//'_stokes.tmp'
!        call FeEdit(Veta)
!        call DeleteFile(Veta)
!      endif
      go to 9999
9000  ErrFlag=0
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      return
      end
      subroutine EM50DefineLocalSymm
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*25 :: Label=('Local symmetry operator #')
      dimension RMat(:,:,:),SVec(:,:),RMatO(:,:,:),SVecO(:,:),
     1          rm6lo(:,:,:),rmlo(:,:,:),s6lo(:,:,:)
      allocatable RMat,SVec,RMatO,SVecO,rm6lo,rmlo,s6lo
      NMatMax=max(16,MaxNSymmL)
      n=NDim(KPhase)
      allocate(RMat(n,n,NMatMax),SVec(n,NMatMax),
     1         rm6lo(n**2,NMatMax,NPhase),rmlo(9,NMatMax,NPhase),
     2         s6lo(n,NMatMax,NPhase))
      do KPh=1,NPhase
        do i=1,NSymmL(KPh)
          call CopyMat(rm6l(1,i,1,KPh),rm6lo(1,i,KPh),n)
          call CopyMat(rml (1,i,1,KPh),rmlo (1,i,KPh),3)
          call CopyVek(s6l(1,i,1,KPh),s6lo(1,i,KPh),n)
        enddo
      enddo
      do i=1,NSymmL(KPhase)
        call CopyMat(rm6l(1,i,1,KPhase),RMat(1,1,i),n)
        call CopyVek(s6l(1,i,1,KPhase),SVec(1,i),n)
      enddo
      ns=NSymmL(KPhase)
      ich=0
      id=NextQuestId()
      Veta=' '
      xqd=40.*float(n+3)
      if(n.eq.3) then
        xqd=xqd+PropFontWidthInPixels*float(3+4*n)
      else
        xqd=xqd+PropFontWidthInPixels*float(4+5*n)
      endif
      il=n+3
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,Label,'C','B')
      nLblLabel=LblLastMade
      call FeQuestLblOff(LblLastMade)
      dpom=40.
      do i=1,n
        tpom=20.
        il=il+1
        if(n.le.3) then
          Veta=SmbX(i)//'''='
        else
          Veta=SmbX6(i)//'''='
        endif
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) dll=FeTxLength(Veta)+5.
        xpom=tpom+dll
        tpom=xpom+dpom+5.
        if(i.eq.1) nLblFirst=LblLastMade
        do j=1,n+1
          tpom=xpom+dpom+5.
          if(j.le.n) then
            if(n.le.3) then
              Veta='* '//SmbX(j)//'+'
            else
              Veta='* '//SmbX6(j)//'+'
            endif
          else
            Veta=' '
            dpom=dpom*2.
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                        0)
          if(j.eq.n+1) dpom=dpom*.5
          if(i.eq.1.and.j.eq.1) then
            nEdwMatFirst=EdwLastMade
            dlp=FeTxLength(Veta)+5.
          endif
          xpom=tpom+dlp
        enddo
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      dpom=60.
      Veta='%New'
      tpom=xqd*.5-dpom*1.5-10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtNew=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+10.
      Veta='%Delete'
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+10.
      Veta='%Go to #'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwGoTo=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,0,ns,1,0.,0.,0.)
      NActOld=-1
      if(ns.gt.0) then
        NAct=1
      else
        NAct=0
      endif
1400  if(NAct.gt.0.and.NAct.ne.NActOld) then
        write(Cislo,FormI15) NAct
        call Zhusti(Cislo)
        call FeQuestLblChange(nLblLabel,Label//Cislo(:idel(Cislo)))
        nEdw=nEdwMatFirst
        nLbl=nLblFirst
        do i=1,n
          call FeQuestLblOn(nLbl)
          do j=1,n+1
            if(j.le.n) then
              pom=RMat(j,i,NAct)
            else
              pom=SVec(i,NAct)
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.true.)
            nEdw=nEdw+1
          enddo
          nLbl=nLbl+1
        enddo
      else if(NAct.le.0) then
        call FeQuestLblOff(nLblLabel)
        nEdw=nEdwMatFirst
        nLbl=nLblFirst
        do i=1,n
          call FeQuestLblOff(nLbl)
          do j=1,n+1
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
          nLbl=nLbl+1
        enddo
      endif
      NActOld=NAct
      if(ns.le.0) then
        call FeQuestButtonDisable(nButtDelete)
        call FeQuestEdwDisable(nEdwGoTo)
      else
        call FeQuestButtonOff(nButtDelete)
        call FeQuestIntEdwOpen(nEdwGoTo,NAct,.false.)
        call FeQuestEudOpen(nEdwGoTo,0,ns,1,0.,0.,0.)
      endif
1500  call FeQuestEvent(id,ich)
      if(NAct.gt.0.and.
     1   (CheckType.ne.EventButton.or.CheckNumber.ne.nButtDelete)) then
        nEdw=nEdwMatFirst
        do i=1,n
          do j=1,n+1
            call FeQuestRealFromEdw(nEdw,pom)
            if(j.le.n) then
              RMat(j,i,NAct)=pom
            else
              SVec(i,NAct)=pom
            endif
            nEdw=nEdw+1
          enddo
        enddo
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwGoTo) then
        call FeQuestIntFromEdw(nEdwGoTo,NAct)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew) then
        if(ns.ge.NMatMax) then
          allocate(RMatO(n,n,ns),SVecO(n,ns))
          do i=1,ns
            call CopyMat(RMat(1,1,i),RMatO(1,1,i),n)
            call CopyVek(SVec(1,i),SVecO(1,i),n)
          enddo
          deallocate(RMat,SVec)
          NMatMax=2*NMatMax
          allocate(RMat(n,n,NMatMax),SVec(n,NMatMax))
          do i=1,ns
            call CopyMat(RMatO(1,1,i),RMat(1,1,i),n)
            call CopyVek(SVecO(1,i),SVec(1,i),n)
          enddo
          deallocate(RMatO,SVecO)
        endif
        ns=ns+1
        NAct=ns
        call UnitMat(RMat(1,1,ns),n)
        call SetRealArrayTo(SVec(1,ns),n,0.)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDelete)
     1  then
        do i=NAct+1,ns
          call CopyMat(RMat(1,1,i),RMat(1,1,i-1),n)
          call CopyVek(SVec(1,i),SVec(1,i-1),n)
        enddo
        ns=ns-1
        if(ns.gt.0) then
          NAct=max(NAct-1,1)
          NActOld=-1
        else
          NAct=0
        endif
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(allocated(rm6l)) deallocate(rm6l,rml,s6l,ISwSymmL,KwSymL,rtl,
     1                               rc3l,rc4l,rc5l,rc6l,GammaIntL,
     2                               LXYZMode,XYZMode,NXYZMode,
     3                               MXYZMode)
      MaxNSymmL=max(MaxNSymmL,ns)
      if(MaxNSymmL.gt.0) then
        allocate(rm6l     (MaxNDim**2,MaxNSymmL  ,MaxNComp,NPhase),
     1           rml      (9         ,MaxNSymmL  ,MaxNComp,NPhase),
     2           s6l      (MaxNDim   ,MaxNSymmL  ,MaxNComp,NPhase),
     3           rtl      (36        ,MaxNSymmL  ,MaxNComp,NPhase),
     4           rc3l     (100       ,MaxNSymmL  ,MaxNComp,NPhase),
     5           rc4l     (225       ,MaxNSymmL  ,MaxNComp,NPhase),
     6           rc5l     (441       ,MaxNSymmL  ,MaxNComp,NPhase),
     7           rc6l     (784       ,MaxNSymmL  ,MaxNComp,NPhase),
     8           GammaIntL(9         ,MaxNSymmL  ,MaxNComp,NPhase),
     9           KwSymL  (mxw       ,MaxNSymmL   ,MaxNComp,NPhase),
     a           ISwSymmL           (MaxNSymmL   ,MaxNComp,NPhase),
     1           LXYZMode(3*(MaxNSymmL+1),NPhase),
     2           XYZMode (3*(MaxNSymmL+1),3*(MaxNSymmL+1),NPhase),
     3           NXYZMode(NPhase),MXYZMode(NPhase))
        call SetIntArrayTo(NXYZMode,NPhase,0)
        call SetIntArrayTo(MXYZMode,NPhase,0)
        do KPh=1,NPhase
          if(KPh.eq.KPhase) cycle
          do i=1,NSymmL(KPh)
            call CopyMat(rm6lo(1,i,KPh),rm6l(1,i,1,KPh),NDim(KPh))
            call CopyMat(rmlo (1,i,KPh),rml (1,i,1,KPh),3)
            call CopyVek(s6lo(1,i,KPh),s6l(1,i,1,KPh),NDim(KPh))
          enddo
        enddo
        do i=1,ns
          call CopyMat(RMat(1,1,i),rm6l(1,i,1,KPhase),n)
          call MatBlock3(RMat(1,1,i),rml(1,i,1,KPhase),n)
          call CopyVek(SVec(1,i),s6l(1,i,1,KPhase),n)
        enddo
        NSymmL(KPhase)=ns
      endif
9999  if(allocated(RMat)) deallocate(RMat,SVec,rm6lo,rmlo,s6lo)
      return
      end
      subroutine EM50MakeStandardOrder
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iar(1),RM6Pom(:,:),s6Pom(:,:),RMPom(9),Tr(9),TrI(9),
     1          RMPomIn(9),ZMagPom(:),RMagPom(:,:),io(3)
      character*80 Veta
      character*8  PointGroup
      character*3  t3
      character*2  t2m
      allocatable RM6Pom,s6Pom,ZMagPom,RMagPom
      logical EqIgCase,EqRV
      n2=0
      if(NGrupa(KPhase).le.1) go to 9999
      if(mod(iabs(CrSystem(KPhase)),10).eq.CrSystemMonoclinic) then
        i=iabs(CrSystem(KPhase))/10
        if(i.eq.1) then
          i=312
        else if(i.eq.2) then
          i=123
        else if(i.eq.3) then
          i=231
        endif
        call SetPermutMat(Tr,3,i,ich)
        call MatInv(Tr,TrI,pom,3)
      else
        call UnitMat(Tr ,3)
        call UnitMat(Tri,3)
      endif
      if(NGrupa(KPhase).eq.2) then
        ipg=2
      else if(NGrupa(KPhase).ge.3.and.NGrupa(KPhase).le.5) then
        ipg=3
      else if(NGrupa(KPhase).ge.6.and.NGrupa(KPhase).le.9) then
        ipg=4
      else if(NGrupa(KPhase).ge.10.and.NGrupa(KPhase).le.15) then
        ipg=5
      else if(NGrupa(KPhase).ge.16.and.NGrupa(KPhase).le.24) then
        ipg=6
      else if(NGrupa(KPhase).ge.25.and.NGrupa(KPhase).le.46) then
        k=0
        do i=1,NSymmN(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,t3,t2m,
     1               io,n,det)
          if(t3(1:1).eq.'2') then
            do j=1,3
              if(io(j).ne.0) then
                if(j.eq.1) then
                  k=231
                else if(j.eq.2) then
                  k=312
                else if(j.eq.3) then
                  k=123
                endif
                go to 1100
              endif
            enddo
          endif
        enddo
1100    ipg=7
        if(k.ne.0) then
          call SetPermutMat(Tr,3,k,ich)
          call MatInv(Tr,TrI,pom,3)
        endif
      else if(NGrupa(KPhase).ge.47.and.NGrupa(KPhase).le.74) then
        ipg=8
      else if(NGrupa(KPhase).ge.75.and.NGrupa(KPhase).le.80) then
        ipg=9
      else if(NGrupa(KPhase).ge.81.and.NGrupa(KPhase).le.82) then
        ipg=10
      else if(NGrupa(KPhase).ge.83.and.NGrupa(KPhase).le.88) then
        ipg=11
      else if(NGrupa(KPhase).ge.89.and.NGrupa(KPhase).le.98) then
        ipg=12
      else if(NGrupa(KPhase).ge.99.and.NGrupa(KPhase).le.110) then
        ipg=13
      else if(NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.122) then
        ipg=14
        if((NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.114).or.
     1     (NGrupa(KPhase).ge.121.and.NGrupa(KPhase).le.122)) then
          ipgp=14
        else
          ipgp=15
        endif
      else if(NGrupa(KPhase).ge.123.and.NGrupa(KPhase).le.142) then
        ipg=15
      else if(NGrupa(KPhase).ge.143.and.NGrupa(KPhase).le.146) then
        ipg=16
      else if(NGrupa(KPhase).ge.147.and.NGrupa(KPhase).le.148) then
        ipg=17
      else if(NGrupa(KPhase).ge.149.and.NGrupa(KPhase).le.155) then
        ipg=18
        if(NGrupa(KPhase).eq.155) then
          ipgp=25
C          32
        else if(mod(NGrupa(KPhase)-148,2).eq.1) then
          ipgp=19
C          312
        else
          ipgp=22
C          321
        endif
      else if(NGrupa(KPhase).ge.156.and.NGrupa(KPhase).le.161) then
        ipg=19
        if(NGrupa(KPhase).ge.160) then
          ipgp=26
C          3m
        else if(mod(NGrupa(KPhase)-155,2).eq.1) then
          ipgp=23
C          3m1
        else
          ipgp=20
C          31m
        endif
      else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.167) then
        ipg=20
        if(NGrupa(KPhase).ge.166) then
          ipgp=27
C          -3m
        else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.163) then
          ipgp=21
C          -31m
        else if(NGrupa(KPhase).ge.164.and.NGrupa(KPhase).le.165) then
          ipgp=24
C          -3m1
        endif
      else if(NGrupa(KPhase).ge.168.and.NGrupa(KPhase).le.173) then
        ipg=21
      else if(NGrupa(KPhase).ge.174.and.NGrupa(KPhase).le.174) then
        ipg=22
      else if(NGrupa(KPhase).ge.175.and.NGrupa(KPhase).le.176) then
        ipg=23
      else if(NGrupa(KPhase).ge.177.and.NGrupa(KPhase).le.182) then
        ipg=24
      else if(NGrupa(KPhase).ge.183.and.NGrupa(KPhase).le.186) then
        ipg=25
      else if(NGrupa(KPhase).ge.187.and.NGrupa(KPhase).le.190) then
        ipg=26
        if(NGrupa(KPhase).le.188) then
          ipgp=33
        else
          ipgp=34
        endif
      else if(NGrupa(KPhase).ge.191.and.NGrupa(KPhase).le.194) then
        ipg=27
      else if(NGrupa(KPhase).ge.195.and.NGrupa(KPhase).le.199) then
        ipg=28
      else if(NGrupa(KPhase).ge.200.and.NGrupa(KPhase).le.206) then
        ipg=29
      else if(NGrupa(KPhase).ge.207.and.NGrupa(KPhase).le.214) then
        ipg=30
      else if(NGrupa(KPhase).ge.215.and.NGrupa(KPhase).le.220) then
        ipg=31
      else if(NGrupa(KPhase).ge.221.and.NGrupa(KPhase).le.230) then
        ipg=32
      endif
      if(ipg.le.13) then
        ipgp=ipg
      else if(ipg.ge.15.and.ipg.le.17) then
        ipgp=ipg+1
      else if(ipg.ge.21.and.ipg.le.25) then
        ipgp=ipg+7
      else if(ipg.ge.27) then
        ipgp=ipg+8
      endif
      PointGroup=SmbPGI(ipgp)
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'pgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9900
1200  read(ln,FormA80,end=9900) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1   go to 1200
      call Kus(Veta,k,Cislo)
      call StToInt(Veta,k,iar,1,.false.,ich)
      if(ich.ne.0) go to 9900
      n=iar(1)
      allocate(RM6Pom(NDimQ(KPhase),n),s6Pom(NDim(KPhase),n))
      allocate(ZMagPom(n),RMagPom(9,n))
      do 1500i=1,n
        read(ln,FormA80,end=9900) Veta
        k=0
        call StToReal(Veta,k,RMPom,9,.false.,ich)
        if(ich.ne.0) go to 9900
        call Multm(TrI,RMPom,RMPomIn,3,3,3)
        call Multm(RMPomIn,Tr,RMPom,3,3,3)
        do j=1,n
          if(EqRV(RM(1,j,1,KPhase),RMPom,9,.001)) then
            call CopyMat(RM6(1,j,1,KPhase),RM6Pom(1,i),NDim(KPhase))
            call CopyVek(s6 (1,j,1,KPhase),s6Pom (1,i),NDim(KPhase))
            ZMagPom(i)=ZMag(j,1,KPhase)
            call CopyMat(RMag(1,j,1,KPhase),RMagPom(1,i),3)
            go to 1500
          endif
        enddo
        go to 9900
1500  continue
      do i=1,n
        call CopyMat(RM6Pom(1,i),RM6(1,i,1,KPhase),NDim(KPhase))
        call MatBlock3(RM6(1,i,1,KPhase),RM(1,i,1,KPhase),NDim(KPhase))
        call CopyVek(s6Pom (1,i),s6 (1,i,1,KPhase),NDim(KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        ZMag(i,1,KPhase)=ZMagPom(i)
        call CopyMat(RMagPom(1,i),RMag(1,i,1,KPhase),3)
      enddo
9900  call CloseIfOpened(ln)
      if(allocated(RM6Pom)) deallocate(RM6Pom,s6Pom)
      if(allocated(ZMagPom)) deallocate(ZMagPom,RMagPom)
9999  return
      end
      subroutine EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension ieps(5),tau(5),ie(5),rp(36),trm(36),ipor(5),xref(3),
     1          QuOld(3),QuiOld(3),QuSymm(3),MonoclinicA(3)
      character*256 Veta,StSymm(3)
      character*80 t80
      character*60 itxt,itxti,GrupaPom,GrupaHledej,GrupaRidka
      character*11 iqs
      character*8 GrupaI
      character*6 smbtau,SmbMagCentr
      character*5 smbq(3)
      character*2 nty
      logical StdSmb,SearchForPrimitive,FirstTime,AskForDelta,
     1        EqIgCase,EqRV,ItIsFd3c,EqRV0,Choice1,CrwLogicQuest,lpom,
     2        WizardModeIn
      integer EM50IrrType,CrlCentroSymm,UseTabsIn
      data iqs/'PABCLMNUVWR'/,smbtau/'01stqh'/,
     1     smbq/'alfa','beta','gamma'/
      call Zhusti(Grupa(KPhase))
      id=idel(Grupa(KPhase))
      if(NDimI(KPhase).eq.1) then
        call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
        call CopyVek(Qui(1,1,KPhase),QuiOld,3)
        i1=index(Grupa(KPhase),'(')
        if(i1.gt.0.and.i1.lt.id) then
          i2=index(Grupa(KPhase)(i1+1:),')')+i1
        else
          i2=0
        endif
        i3=idel(Grupa(KPhase))
        if(i1.le.0.or.i2.le.i1) go to 8150
        GrupaPom=Grupa(KPhase)(1:i1-1)
        t80=Grupa(KPhase)(i1+1:i2-1)
        call mala(t80)
        do i=1,3
          if(idel(t80).le.0) go to 8160
          k=1
1010      if(t80(k:k).eq.smbq(i)(k:k)) then
            k=k+1
            go to 1010
          endif
          k=k-1
          if(k.eq.0) then
            j=index(t80,'/')
            if(j.le.0.or.j.gt.3.or.(j.eq.3.and.t80(1:1).ne.'-')) then
              k=1
            else
              k=j+1
            endif
            quir(i,1,KPhase)=fract(t80(1:k),ich)
            qui(i,1,KPhase)=0.
            if(ich.ne.0) go to 8210
          else
            quir(i,1,KPhase)=0.
            qui(i,1,KPhase)=sqrt(float(i)*.1)
          endif
          t80=t80(k+1:)
        enddo
        n=0
        ic=0
        in=0
        do i=1,3
          if(abs(quir(i,1,KPhase)).lt..0001) then
            n=n+1
            if(in.eq.0) in=i
          else if(ic.eq.0) then
            ic=i
          endif
        enddo
        nq=0
        if(n.eq.3) then
          nq=1
        else if(n.eq.2) then
          if(abs(quir(ic,1,KPhase)-.5).lt..0001) then
            nq=ic+1
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001) then
            nq=ic+4
          endif
        else if(n.eq.1) then
          if(abs(quir(1,1,KPhase)-.333333).lt..0001.and.
     1       abs(quir(2,1,KPhase)-.333333).lt..0001) then
            nq=11
          else if(abs(quir(ic,1,KPhase)-.5).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-.5).lt..0001) then
            nq=7+in
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-1.).lt..0001) then
            nq=11+in
          endif
        endif
        if(nq.le.0) go to 8160
        call AddVek(Qui(1,1,KPhase),Quir(1,1,KPhase),Qu(1,1,1,KPhase),3)
        iqv=EM50IrrType(qui(1,1,KPhase))
      else
        do i=1,NDimI(KPhase)
          do j=1,3
            qui(j,i,KPhase)=qu(j,i,1,KPhase)
            quir(j,i,KPhase)=0.
          enddo
        enddo
        GrupaPom=Grupa(KPhase)
        iqv=0
      endif
1100  SmbMagCentr=' '
      if(GrupaPom(1:1).eq.'-') then
        call Uprat(GrupaPom(2:))
      else
        if(GrupaPom(2:2).eq.'[') then
          i=index(GrupaPom,']')
          if(i.gt.0) then
            SmbMagCentr=GrupaPom(2:i)
            GrupaPom=GrupaPom(1:1)//GrupaPom(i+1:)
            TauMagCenter=.5
          endif
        endif
        call Uprat(GrupaPom)
      endif
      i=index(GrupaPom,'.')
      idp=idel(GrupaPom)
      MagInv=0
      if(i.gt.0) then
        if(GrupaPom(i+1:i+2).eq.'1''') then
          i1p=index(GrupaPom,'[')
          i2p=index(GrupaPom,']')
          SmbMagCentr=' '
          if((i1p.eq.0.and.i2p.gt.0).or.(i1p.gt.0.and.i2p-i1p.ne.2))
     1      then
            go to 8190
          else if(i1p.gt.0) then
            SmbMagCentr=GrupaPom(i1p:i2p)
            i3=idel(Grupa(KPhase))
            if(EqIgCase(Grupa(KPhase)(i3:i3),'s')) then
              TauMagCenter=.5
            else if(EqIgCase(Grupa(KPhase)(i3:i3),'0')) then
              TauMagCenter=0.
            else
              go to 8105
            endif
            i3=idl-1
          else
            MagInv=1
          endif
          GrupaPom(i:)=' '
        else
          go to 8190
        endif
      else if(idp-2.gt.0) then
        if(NDimI(KPhase).eq.0) then
          MagInv=0
        else
          if(GrupaPom(idp-2:idp).eq. '.1'''.and.
     1       GrupaPom(idp-3:idp).ne.'.-1''') then
            GrupaPom(idp-1:)=' '
            MagInv=1
          endif
        endif
      endif
      i=index(GrupaPom,';')
      j=index(GrupaPom(2:),'x')
      k=index(GrupaPom,'y')
      l=index(GrupaPom,'z')
      if(i.le.0.and.j.le.0.and.k.le.0.and.l.le.0.and.
     1   grupa(KPhase)(1:1).ne.'-') then
        StdSmb=.true.
        Lattice(KPhase)=GrupaPom(1:1)
        if(Lattice(KPhase).eq.'X') GrupaPom(1:1)='P'
        SearchForPrimitive=.false.
        j=0
        GrupaHledej=' '
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).ne.'''') then
            j=j+1
            GrupaHledej(j:j)=GrupaPom(i:i)
          endif
        enddo
        if(j.ne.idel(GrupaPom).and.MagneticType(KPhase).le.0) go to 8180
1200    imd=0
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1          'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 8700
        read(ln,FormA256) Veta
        if(Veta(1:1).ne.'#') rewind ln
        if(FirstTime) NInfo=0
2000    read(ln,FormA,err=8610,end=2010) Veta
        read(Veta,FormSG,err=8610) igi,ipgi,idli,GrupaI,itxti,
     1                             GrupaRidka,GrupaRidka,GrupaRidka
        if(imd.ne.0) imd=imd+1
        if((GrupaI.eq.'P2'.and.imd.eq.0).or.
     1      GrupaI.eq.'Pmm2') imd=1
        if(GrupaHledej.eq.GrupaI.or.
     1     (Lattice(KPhase).eq.'X'.and.GrupaHledej(2:).eq.GrupaI(2:)))
     2    then
          if(ipgi.ge.3.and.ipgi.le.5) then
            if(FirstTime.and.NInfo.lt.3) then
              NInfo=NInfo+1
              StSymm(NInfo)=Veta
              Veta=GrupaRidka
              if(EqIgCase(Lattice(KPhase),'X')) Veta(1:1)='X'
              write(TextInfo(NInfo),'(a,i3,2a)') Tabulator,ipgi,
     1                                 Tabulator,Veta(:idel(Veta))
              MonoclinicA(NInfo)=mod(imd-1,3)+1
            endif
            t80=GrupaRidka
            k=0
            call kus(t80,k,Cislo)
            GrupaRidka=Cislo
            do i=1,4
              call kus(t80,k,Cislo)
              if(Cislo.ne.'1')
     1          GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     2                     Cislo(:idel(Cislo))
            enddo
            if(.not.FirstTime.and.mod(imd-1,3)+1.eq.Monoclinic(KPhase))
     1        then
              go to 2100
            else
              go to 2000
            endif
          endif
          go to 2100
        endif
        go to 2000
2010    call CloseIfOpened(ln)
        if(NInfo.gt.1.and.FirstTime) then
          UseTabsIn=UseTabs
          UseTabs=NextTabs()
          WizardModeIn=WizardMode
          WizardMode=.false.
          call FeTabsAdd( 50.,UseTabs,IdRightTab,' ')
          call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
          id=NextQuestId()
          xdq=300.
          il=Ninfo+1
          Veta='Select relevant space group'
          call FeQuestCreate(id,-1.,-1.,xdq,il,Veta,0,LightGray,-1,0)
          il=1
          tpom=30.
          call FeQuestLblMake(id,tpom,il,'Space group number','L','N')
          tpom=150.
          call FeQuestLblMake(id,tpom,il,'Space group symbol','L','N')
          xpom=5.
          tpom=xpom+10.+CrwgXd
          n=0
          do i=1,NInfo
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                          TextInfo(i),'L',CrwgXd,CrwgYd,0,1)
            if(MonoclinicA(i).eq.Monoclinic(KPhase)) then
              lpom=.true.
              n=i
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
            if(i.eq.1) nCrwFirst=CrwLastMade
          enddo
          if(n.eq.0) call FeQuestCrwOn(nCrwFirst)
2050      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2050
          endif
          nCrw=nCrwFirst
          do i=1,NInfo
            if(CrwLogicQuest(nCrw)) then
              read(StSymm(i),FormSG,err=8610)
     1          igi,ipgi,idli,GrupaI,itxti,GrupaRidka,GrupaRidka,
     2          GrupaRidka
              t80=GrupaRidka
              k=0
              call kus(t80,k,Cislo)
              GrupaRidka=Cislo
              do j=1,3
                call kus(t80,k,Cislo)
                if(Cislo.ne.'1') then
                  GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     1                       Cislo(:idel(Cislo))
                  Monoclinic(KPhase)=j
                  exit
                endif
              enddo
            endif
            nCrw=nCrw+1
          enddo
          call FeQuestRemove(id)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsIn
          WizardMode=WizardModeIn
          go to 2100
        endif
        if(SearchForPrimitive.or.GrupaHledej(1:1).eq.'P') then
          go to 8170
        else
          SearchForPrimitive=.true.
          GrupaHledej(1:1)='P'
          go to 1200
        endif
2100    call CloseIfOpened(ln)
        k=index(itxti,'#')
        if(k.gt.0) then
          itxt=itxti(1:k-1)
          Veta=itxti(k+1:idel(itxti))
          do j=1,idel(Veta)
            if(Veta(j:j).eq.',') Veta(j:j)=' '
          enddo
          k=0
          call StToReal(Veta,k,ShiftSg,3,.false.,ich)
          call RealVectorToOpposite(ShiftSg,ShiftSg,3)
          call SetRealArrayTo(ShiftSg(4),NDimI(KPhase),0.)
        else
          itxt=itxti
          call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
        endif
        ipg=ipgi
        if(Lattice(KPhase).eq.'X'.or.SearchForPrimitive) then
          ngrupa(KPhase)=0
        else
          ngrupa(KPhase)=igi
        endif
        idl=idli
        if(MagInv.gt.0) idl=idl+1
        Poradi=.true.
        if(ipg.le.2) then
          CrSystem(KPhase)=CrSystemTriclinic
        else if(ipg.ge.3.and.ipg.le.5) then
          imd=mod(imd-1,3)+1
          if(NDimI(KPhase).eq.1.and.
     1       Monoclinic(KPhase).ne.iabs(iqv)) go to 8210
          CrSystem(KPhase)=CrSystemMonoclinic+Monoclinic(KPhase)*10
        else if(ipg.ge.6.and.ipg.le.8) then
          Poradi=.false.
          imd=mod(imd+1,3)+1
          if(NDimI(KPhase).eq.1.and.iqv.lt.1) go to 8210
          CrSystem(KPhase)=CrSystemOrthorhombic
        else if(ipg.ge.9.and.ipg.le.15) then
          if(NDimI(KPhase).eq.1.and.iqv.ne.3) go to 8210
          CrSystem(KPhase)=CrSystemTetragonal
        else if(ipg.ge.16.and.ipg.le.27) then
          if(NDimI(KPhase).eq.1.and.iqv.ne.3) go to 8210
          if(ipg.le.20) then
            CrSystem(KPhase)=CrSystemTrigonal
          else
            CrSystem(KPhase)=CrSystemHexagonal
          endif
        else if(ipg.ge.28.and.ipg.le.32) then
          CrSystem(KPhase)=CrSystemCubic
        endif
        if(mod(CrSystem(KPhase),10).ne.2) Monoclinic(KPhase)=0
        GrupaPom(1:1)=Lattice(KPhase)
        GrupaHledej(1:1)=Lattice(KPhase)
        if(itxt(1:1).eq.'-') then
          itxt(2:2)=Lattice(KPhase)
        else
          itxt(1:1)=Lattice(KPhase)
        endif
      else
        StdSmb=.false.
        itxt=GrupaPom
        idl=1
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).eq.';') idl=idl+1
        enddo
        call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
      endif
      if(NDimI(KPhase).eq.1) then
        Veta=Grupa(KPhase)(i2+1:i3)
        call mala(Veta)
        i=0
        n=0
        id=idel(Veta)
        Znak=1.
2200    i=i+1
        if(i.gt.id) go to 2300
        if(Veta(i:i).eq.'-') then
          Znak=-1.
          go to 2200
          i=i+1
          if(Veta(i:i).eq.'1') then
            n=n+1
            ieps(n)=-1
            tau(n)=0
            go to 2200
          else
            go to 8100
          endif
        else
          j=index(smbtau,Veta(i:i))
          if(j.le.0) go to 8100
          if(j.eq.1) then
            n=n+1
            ieps(n)=0
            tau(n)=0.
            go to 2200
          else if(j.eq.2) then
            go to 8100
          endif
          if(j.ne.6) j=j-1
          n=n+1
          ieps(n)=1
          if(j.ne.1) then
            if(Znak.gt.0.) then
              tau(n)= 1./float(j)
            else
              tau(n)=-1./float(j)
            endif
          else
            tau(n)=0.
          endif
          Znak=1.
          go to 2200
        endif
2300    if(n.ne.idl) then
          if(n.gt.idl) then
            Veta='"'//Veta(1:id)//'" too long.'
          else
            do i=n+1,idl
              ieps(i)=0
              tau(i)=0.
            enddo
            go to 2400
          endif
          id=idel(Veta)
          go to 8220
        endif
2400    if(StdSmb) then
          if(MagInv.gt.0) idl=idl-1
          call EM50SetEps(ie,ipor,ieps,tau,idl,ipg,ngrupa(KPhase),imd,
     1                    itxt,ich)
          if(ich.ne.0) go to 8162
          if(MagInv.gt.0) then
            idl=idl+1
            ie(idl)=1
          endif
          do i=1,idl
            if(ie(i).ne.ieps(i)) then
              if(ieps(i).eq.0) ieps(i)=ie(i)
            endif
          enddo
        endif
      endif
      call EM50GetMagFlags(GrupaPom,GrupaRidka,MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8190
      call EM50OrderMagFlags(ipg,NGrupa(KPhase),MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8200
      call EM50GenCentr(itxt,nc,FirstTime,*8110,*9900)
      call EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,ich)
      if(ich.ne.0) go to 9900
      do i=1,NLattVec(KPhase)
        do 2500j=1,NSymm(KPhase)
          call MultM(rm6(1,j,1,KPhase),vt6(1,i,1,KPhase),rp,
     1               NDim(KPhase),NDim(KPhase),1)
          call od0do1(rp,rp,NDim(KPhase))
          do k=1,NLattVec(KPhase)
            if(EqRV(vt6(1,k,1,KPhase),rp,NDim(KPhase),.001)) go to 2500
          enddo
          go to 8230
2500    continue
      enddo
      call EM50OriginShift(ShiftSg,0)
      i=CrlCentroSymm()
      call SetRealArrayTo(xref,3,0.)
      if(EqIgCase(Grupa(KPhase),'Fddd')) then
        call SetRealArrayTo(xref,3,.25)
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          do i=1,NSymm(KPhase)
            ipul=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.5).lt..001) ipul=ipul+1
            enddo
            if(ipul.eq.2) call SetRealArrayTo(s6(1,i,1,KPhase),3,0.)
          enddo
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          do 5020i=1,NSymm(KPhase)
            call MatInv(rm(1,i,1,KPhase),trm,Det,3)
            if(Det.gt.0.) then
              pom=.75
            else
              pom=.25
            endif
            do j=1,NLattVec(KPhase)
              call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
              call od0do1(rp,rp,3)
              nnul=0
              npom=0
              do k=1,3
                if(abs(rp(k)).lt..001) then
                  nnul=nnul+1
                else if(abs(rp(k)-pom).lt..001) then
                  npom=npom+1
                endif
              enddo
              if(nnul+npom.eq.3.and.(npom.eq.0.or.npom.eq.2)) then
                call CopyVek(rp,s6(1,i,1,KPhase),3)
                go to 5020
              endif
            enddo
5020      continue
        endif
        go to 5300

      else if(EqIgCase(Grupa(KPhase),'I41md').or.
     1        EqIgCase(Grupa(KPhase),'I41cd')) then
        k=1
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs( s6(1,i,1,KPhase)).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs( s6(1,i,1,KPhase)).lt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'I-42d')) then
        do 5040i=1,NSymm(KPhase)
          if(abs(s6(2,i,1,KPhase)).lt..001) go to 5040
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5040    continue
      else if(EqIgCase(Grupa(KPhase),'I41/a').or.
     1        EqIgCase(Grupa(KPhase),'I41/amd').or.
     2        EqIgCase(Grupa(KPhase),'I41/acd')) then
        xref(2)=.5
        xref(3)=.25
        k=2
        l=NDim(KPhase)+2
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          pomp1=.5
          pomm1=0.
          pomp2=0.
          pomm2=.5
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          if(EqIgCase(Grupa(KPhase),'I41/a')) then
            pomp1=.25
            pomm1=.75
          else
            pomp1=.75
            pomm1=.25
          endif
          pomp2=0.
          pomm2=0.
        else
          go to 5300
        endif
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp1).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm1).gt..001).or.
     4       (abs(RM6(l,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp2).gt..001).or.
     2       (abs(RM6(l,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm2).gt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3')) then
        call SetRealArrayTo(xref,3,.25)
        Choice1=EqRV(s6(1,i,1,KPhase),xref,3,.001)
        do 5050i=1,NSymm(KPhase)
          call MatInv(rm(1,i,1,KPhase),trm,Det,3)
          if(Det.gt.0.) then
            if(Choice1) then
              pom=0.
              nsum=3
            else
              pom=.25
              nsum=2
            endif
          else
            if(Choice1) then
              pom=.25
              nsum=3
            else
              pom=.75
              nsum=2
            endif
          endif
          do j=1,NLattVec(KPhase)
            call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
            call od0do1(rp,rp,3)
            npom=0
            do k=1,3
              if(abs(rp(k)-pom).lt..001) npom=npom+1
            enddo
            if(npom.eq.nsum) then
              call CopyVek(rp,s6(1,i,1,KPhase),3)
              go to 5050
            endif
          enddo
5050    continue
      else if(EqIgCase(Grupa(KPhase),'I-43d')) then
        do i=1,NSymm(KPhase)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.1.and.inul.eq.2).or.ipul.eq.3.or.
     1       (i34 .eq.1.and.i14 .eq.2).or.i34.eq.3) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3m').or.
     1        EqIgCase(Grupa(KPhase),'Fd-3c')) then
        Choice1=.not.EqRV(s6(1,i,1,KPhase),xref,3,.001)
        if(Choice1) then
          ItIsFd3c=EqIgCase(Grupa(KPhase),'Fd-3c')
          do i=1,NSymm(KPhase)
            if(ItIsFd3c) then
              call MatInv(RM(1,i,1,KPhase),rp,det,3)
            else
              det=1.
            endif
            i34=0
            i14=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
              if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
            enddo
            if(i34.gt.0.or.i14.gt.0) then
              Znak=-1.
            else
              Znak= 1.
            endif
            KteryZnak=0
            KdeZnak=0
            KdeSoused=0
            do j=1,3
              do k=1,3
                l=(k-1)*3+j
                if(abs(RM(l,i,1,KPhase)-Znak).lt..001) then
                  if(KdeZnak.le.0) then
                    KdeZnak=j
                    KteryZnak=k
                  else
                    KdeZnak=-1
                    go to 5090
                  endif
                endif
              enddo
            enddo
            k=mod(KteryZnak+1,3)+1
            k=(k-1)*3+1
            do j=1,3
              if(abs(RM(k,i,1,KPhase)).gt..001) then
                KdeSoused=j
                go to 5090
              endif
              k=k+1
            enddo
5090        if(Znak.lt.0.) then
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.75
              else
                pom=.25
              endif
            else
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.5
              else
                pom=0.
              endif
            endif
            call SetRealArrayTo(s6(1,i,1,KPhase),3,pom)
            if(KdeZnak.gt.0) then
              s6(KdeZnak,i,1,KPhase)=s6(KdeZnak,i,1,KPhase)+.5
              s6(KdeSoused,i,1,KPhase)=s6(KdeSoused,i,1,KPhase)+.5
            endif
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          enddo
        endif
      else if(EqIgCase(Grupa(KPhase),'Ia-3d')) then
        do 5120i=1,NSymm(KPhase)
          call MatInv(RM(1,i,1,KPhase),rp,det,3)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.2.and.inul.eq.1).or.inul.eq.3) go to 5120
          if(det.gt.0.) then
            if((i34.eq.1.and.i14.eq.2).or.i34.eq.3) go to 5120
          else
            if((i34.eq.2.and.i14.eq.1).or.i34.eq.0) go to 5120
          endif
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5120    continue
      endif
5300  call EM50OriginShift(ShSg(1,KPhase),0)
      do i=1,NSymm(KPhase)
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        do j=1,NDim(KPhase)
          if(index(symmc(j,i,1,KPhase),'?').gt.0) then
            NSymm(KPhase)=0
            go to 8140
          endif
        enddo
      enddo
      if(idel(SmbMagCentr).eq.3.and.SmbMagCentr(1:1).eq.'['.and.
     1                              SmbMagCentr(3:3).eq.']' ) then
        if(SmbMagCentr(2:2).eq.'I') then
          rp=.5
          if(CrSystem(KPhase).eq.CrSystemTrigonal) then
            rp(1)=0.
            rp(2)=0.
          endif
          go to 5350
        else if(SmbMagCentr(2:2).eq.'c') then
          rp=0.
          rp(3)=.5
          go to 5350
        else
          i=ichar(SmbMagCentr(2:2))-ichar('a')+1
          rp=0.
          if(i.ge.1.and.i.le.3) then
            rp(i)=.5
            go to 5350
          endif
          i=ichar(SmbMagCentr(2:2))-ichar('A')+1
          rp=.5
          if(i.ge.1.and.i.le.3) then
            rp(i)=0.
            go to 5350
          endif
        endif
        go to 5400
5350    nss=NSymm(KPhase)
        call ReallocSymm(NDim(KPhase),2*nss,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,NSymm(KPhase)
          nss=nss+1
          call CopyVek(rm6(1,i,1,KPhase),rm6(1,nss,1,KPhase),
     1                 NDimQ(KPhase))
          call CopyVek(rm (1,i,1,KPhase),rm (1,nss,1,KPhase),9)
          call AddVek(s6(1,i,1,KPhase),rp,s6(1,nss,1,KPhase),
     1                 NDim(KPhase))
          if(NDimI(KPhase).gt.0)
     1      s6(4,nss,1,KPhase)=s6(4,nss,1,KPhase)+TauMagCenter
          call NormCentr(s6(1,nss,1,KPhase))
          call CodeSymm(rm6(1,nss,1,KPhase),s6(1,nss,1,KPhase),
     1                  symmc(1,nss,1,KPhase),0)
          ZMag(nss,1,KPhase)=-Zmag(i,1,KPhase)
        enddo
        NSymm(KPhase)=nss
      endif
5400  ich=0
      call GetQiQr(qu(1,1,1,KPhase),qui(1,1,KPhase),
     1             quir(1,1,KPhase),NDimI(KPhase),1)
      go to 9999
8100  write(t80,'(''Incorrect additional symbol of the '',i1,a2,
     1            '' generator.'')') n+1,nty(n+1)
      go to 8500
8105  write(t80,'(''Incorrect additional symbol for the time inverion'',
     1            '' operator.'')')
      go to 8500
8110  t80='The cell centring symbol is not correct.'
      go to 8500
8120  write(t80,'(''Opposite sign of additional symbol of the '',i1,a2,
     1            '' generator.'')') ipor(i),nty(ipor(i))
      go to 8500
8130  t80='The additional symbols are not consistent.'
      go to 8500
8140  t80='Origin shift is not acceptable.'
      go to 8500
8150  t80='Neither colon nor parenthesis section found.'
      go to 8500
8160  t80='The symbol for modulation vector is incorrrect.'
      go to 8500
8162  t80='The symbol for modulation vector isn''t consitent with the'//
     1    ' point group .'
      go to 8500
8170  t80='The symbol "'//Grupa(KPhase)(:idel(Grupa(KPhase)))//
     1    '" wasn''t found on the list.'
      go to 8500
8180  t80='The symbol contains time inversion flags for non-magnetic '//
     1    'structure.'
      go to 8500
8190  t80='During decoding time inversion flags.'
      go to 8500
8200  t80='The time inversion flags are inacceptable.'
      go to 8500
8210  t80='The modulation vector is inconsistent with the superspace '//
     1    'symbol.'
      go to 8500
8220  t80='The symbol for addition translation part '//Veta(:idel(Veta))
      go to 8500
8230  t80='The cell centring is not acceptable for the point group.'
      go to 8500
8500  if(NDimI(KPhase).eq.1) then
        Veta='incorrect superspace group symbol.'
      else
        Veta='incorrect space group symbol.'
      endif
      go to 9000
8610  call FeReadError(ln)
      call CloseIfOpened(ln)
8700  call DeletePomFiles
      call FeTmpFilesDelete
      call FeGrQuit
      stop
9000  call FeChybne(-1.,-1.,Veta,t80,SeriousError)
9900  ich=1
9999  if(NDimI(KPhase).eq.1) then
        call CopyVek(Qu(1,1,1,KPhase),QuSymm,3)
        call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
        call CopyVek(QuiOld,Qui(1,1,KPhase),3)
      endif
      return
      end
      subroutine EM50GenCentr(itxt,nc,FirstTime,*,*)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) itxt
      logical FirstTime
      if(itxt(1:1).eq.'-') then
        NCSymm(KPhase)=2
        itxt=itxt(2:)
      else if(itxt(1:1).eq.'+') then
        NCSymm(KPhase)=1
        itxt=itxt(2:)
      else
        NCSymm(KPhase)=1
      endif
      nc=index(smbc,itxt(1:1))
      if(nc.gt.0.and.nc.le.8) then
        call EM50GenVecCentr(nc,FirstTime,ich)
        if(ich.ne.0) return2
      else
        return1
      endif
      Lattice(KPhase)=itxt(1:1)
      itxt=itxt(2:)
      return
      end
      subroutine EM50GetMagFlags(Grp,GrupaRidka,MagFlag,NMagFlag,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension MagFlag(*)
      character*(*) Grp,GrupaRidka
      logical EqIgCase
      ich=0
      NMagFlag=0
      k=0
      call kus(GrupaRidka,k,Cislo)
      i=2
1000  if(k+1.ge.len(GrupaRidka)) go to 9999
      call kus(GrupaRidka,k,Cislo)
      j=index(Cislo,'/')
      if(j.gt.0) then
        Cislo(j:)=' '
        k=index(GrupaRidka,'/')
      endif
      idl=idel(Cislo)
      if(EqIgCase(Grp(i:i+idl-1),Cislo)) then
        i=i+idl
        NMagFlag=NMagFlag+1
        if(Grp(i:i).eq.'''') then
          MagFlag(NMagFlag)=-1
          i=i+1
        else
          MagFlag(NMagFlag)= 1
        endif
        if(Grp(i:i).eq.'/') i=i+1
        go to 1000
      else
        ich=1
        go to 9999
      endif
9999  return
      end
      subroutine EM50OrderMagFlags(ipg,isg,MagFlag,NMagFlag,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension MagFlag(*)
      ich=0
      if(ipg.eq.5.or.ipg.eq.11.or.ipg.eq.23) then
        NMagFlag=NMagFlag+1
        MagFlag(NMagFlag)=MagFlag(1)*MagFlag(2)
      else if(ipg.eq.8) then
        i=1
        do n=1,NMagFlag
          i=i*MagFlag(n)
        enddo
        NMagFlag=NMagFlag+1
        MagFlag(NMagFlag)=i
      else if(ipg.eq.15.or.ipg.eq.27) then
        if(ipg.eq.27) then
          i=MagFlag(1)*MagFlag(2)
        else
          i=MagFlag(2)
        endif
        do n=3,NMagFlag
          MagFlag(n-1)=MagFlag(n)
        enddo
        MagFlag(NMagFlag)=i
      else if(ipg.ge.16.and.ipg.le.20) then
        if(ipg.ne.17.and.ipg.ne.20.and.MagFlag(1).lt.0) go to 9000
        if((mod(isg-148,2).eq.1.and.ipg.eq.18).or.
     1     (mod(isg-155,2).eq.1.and.ipg.eq.19).or.
     2     isg.eq.162.or.isg.eq.163) then
          if(NMagFlag.gt.2) then
            do n=3,NMagFlag
              MagFlag(n-1)=MagFlag(n)
            enddo
            NMagFlag=NMagFlag-1
          endif
        endif
        if(ipg.eq.17.or.ipg.eq.20) then
          NMagFlag=NMagFlag+1
          MagFlag(NMagFlag)=MagFlag(1)
          MagFlag(1)=1
        endif
      else if(ipg.eq.28) then
        if(MagFlag(1).lt.0.or.MagFlag(2).lt.0) go to 9000
        NMagFlag=3
        MagFlag(3)=1
        MagFlag(2)=MagFlag(1)
      else if(ipg.eq.29) then
        NMagFlag=4
        MagFlag(4)=MagFlag(2)
        MagFlag(3)=1
        MagFlag(2)=MagFlag(1)*MagFlag(4)
        MagFlag(1)=MagFlag(1)*MagFlag(4)
      else if(ipg.eq.30.or.ipg.eq.31.or.ipg.eq.32) then
        i=MagFlag(3)
        MagFlag(3)=MagFlag(2)
        MagFlag(2)=i
        if(ipg.eq.30.or.ipg.eq.31) then
          if(MagFlag(1).ne.MagFlag(2)) go to 9000
          MagFlag(2)=1
        else if(ipg.eq.32) then
          if(MagFlag(1)*MagFlag(3).lt.0) go to 9000
          NMagFlag=4
          MagFlag(4)=MagFlag(3)
          MagFlag(3)=1
          i=MagFlag(1)
          MagFlag(1)=i*MagFlag(2)
          MagFlag(2)=i*MagFlag(4)
        endif
        if(ipg.ne.32.and.MagFlag(3).lt.0) go to 9000
      endif
      if(ipg.ge.24.and.ipg.le.27) then
        i=MagFlag(3)
        MagFlag(3)=MagFlag(2)
        MagFlag(2)=i
      endif
      go to 9999
9000  ich=1
9999  return
      end
      subroutine EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension tau(idl),vtp(6),tv(3,8),tvo(3,6),pp(6),pq(3),mpp(3),
     1          rmp(36),sp(6),spp(6),QuPom(3),QuSum(3),RM6Old(:,:),
     2          S6Old(:,:)
      character*(*) itxt
      character*2 nty
      character*5 ir
      character*20 ito
      character*60 HallSymbol
      character*80 t80,errtxt
      real NulVek(6)
      logical AskForDelta,EQRV0,EQRV
      data ir/'12346'/,ito/'xyz"''*12345abcnuvwd;'/
      data tv/.5,.0,.0,.0,.5,.0,.0,.0,.5,.5,.5,.5,.25,.0,.0,
     1        .0,.25,.0,.0,.0,.25,.25,.25,.25/
      data tvo/1.,0.,0.,0.,1.,0.,0.,0.,1.,1.,1.,0.,1.,-1.,0.,1.,1.,1./
      data NulVek/6*0./
      allocatable RM6Old,S6Old
      ich=0
      Tetra=.false.
      if(NDimI(KPhase).gt.1) then
        NSymmOld=NSymm(KPhase)
        allocate(RM6Old(NDimQ(KPhase),NSymmOld),
     1            S6Old(NDim(KPhase) ,NSymmOld))
        do i=1,NSymmOld
          call CopyMat(RM6(1,i,1,KPhase),RM6Old(1,i),NDim(KPhase))
          call CopyVek( S6(1,i,1,KPhase), S6Old(1,i),NDim(KPhase))
        enddo
      endif
      NSymm(KPhase)=1
      call SetRealArrayTo(rm6(1,1,1,KPhase),maxNDim**2,0.)
      call SetRealArrayTo(s6 (1,1,1,KPhase),maxNDim,0.)
      call SetStringArrayTo(symmc(1,1,1,KPhase),maxNDim,' ')
      call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
      call UnitMat(rm(1,1,1,KPhase),3)
      call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
      call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1              symmc(1,1,1,KPhase),0)
      ISwSymm(NSymm(KPhase),1,KPhase)=1
      ZMag(1,1,KPhase)=1
      call UnitMat(RMag(1,1,1,KPhase),3)
      BratSymm(NSymm(KPhase),KPhase)=.true.
      HallSymbol=itxt
      id=idel(itxt)
1000  if(id.le.0) go to 9999
      if(itxt(1:1).eq.'-') then
        iz=-1
        itxt=itxt(2:)
        id=id-1
      else if(itxt(1:1).eq.'+') then
        iz= 1
        itxt=itxt(2:)
        id=id-1
      else
        iz= 1
      endif
      if(id.le.0) then
        errtxt='rotational information is missing.'
        go to 8000
      endif
      nr=index(ir,itxt(1:1))
      itxt=itxt(2:)
      if(nr.le.0) then
        errtxt='incorrect rotational part.'
        go to 8000
      else if(nr.eq.1) then
        if(idel(itxt).gt.0) then
          errtxt='incorrect rotational part.'
          go to 8000
        else
          go to 5000
        endif
      endif
      Tetra=Tetra.or.nr.eq.4
      ior=-1
      call SetRealArrayTo(vtp,NDim(KPhase),0.)
      pom=0.
1100  if(idel(itxt).gt.0) then
        i=index(ito,itxt(1:1))
        if(i.gt.0) then
          itxt=itxt(2:)
        else
          errtxt='incorrect orientation or translation part.'
          go to 8000
        endif
      else
        i=20
      endif
      if(i.le.6) then
        if(ior.eq.-1) then
          ior=i
          go to 1100
        else
          errtxt='orientation is doubled.'
          go to 8000
        endif
      else if(i.le.19) then
        nt=i-6
        if(nt.le.5) then
          p=nr
          if(nr.eq.5) then
            p=6.
          else if(nr.eq.6.or.nr.eq.7) then
            p=2.
          else if(nr.eq.8) then
            p=3.
          endif
          pom=pom+float(nt)/p
        else
          do i=1,3
            vtp(i)=vtp(i)+tv(i,nt-5)
          enddo
        endif
      else if(i.eq.20) then
        if(ior.lt.0) then
          if(NSymm(KPhase).eq.1) then
            ior=3
          else if(NSymm(KPhase).eq.2.or.NSymm(KPhase).eq.3) then
            if(nr.eq.2.and.(nrold.eq.2.or.nrold.eq.4)) ior=1
            if(nr.eq.2.and.(nrold.eq.3.or.nrold.eq.5)) ior=5
            if(nr.eq.3) ior=6
          endif
        endif
        if(ior.lt.0) then
          errtxt='orientation is not defined.'
          go to 8000
        endif
        if(ior.eq.6) then
          if(nr.eq.3) then
            nr=8
            ior=1
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        else if(ior.eq.4.or.ior.eq.5) then
          if(nr.eq.2) then
            nr=ior+2
            if(NSymm(KPhase).ne.1) then
              ior=iorold
            else
              ior=3
            endif
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        endif
        call EM50SetGen(rmp,nr-1,ior,NDim(KPhase))
        if(iz.eq.-1) call realMatrixToOpposite(rmp,rmp,NDim(KPhase))
        do i=1,3
          sp(i)=vtp(i)+pom*tvo(i,ior)
        enddo
        if(NDim(KPhase).gt.3) then
          k1=0
          k2=0
          n1=0
          n2=0
          if(NDim(KPhase).gt.4) then
            k1=-2
            k2= 2
            if(NDim(KPhase).gt.5) then
              n1=-2
              n2= 2
            endif
          endif
        endif
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)*sqrt(1.2)
            enddo
          enddo
        endif
        do 1600i=1,NDimI(KPhase)
          do j=1,3
            pom=0.
            do k=1,3
              pom=pom+(qui(k,i,KPhase)+quir(k,i,KPhase))*
     1                rmp(k+(j-1)*NDim(KPhase))
            enddo
            pp(j)=pom
          enddo
          jknMn=999999
          do n=n1,n2
            do k=k1,k2
              do 1560j=-4,4
                mmpa=0
                do l=1,3
                  pq(l)=float(j)*(qui(l,1,KPhase)+quir(l,1,KPhase))-
     1                            pp(l)
                  if(NDim(KPhase).gt.4) pq(l)=pq(l)+float(k)*
     1                                (qui(l,2,KPhase)+quir(l,2,KPhase))
                  if(NDim(KPhase).gt.5) pq(l)=pq(l)+float(n)*
     1                                (qui(l,3,KPhase)+quir(l,3,KPhase))
                  mpp(l)=nint(pq(l))
                  mmpa=mmpa+iabs(mpp(l))
                  if(abs(pq(l)-float(mpp(l))).gt..0005) go to 1560
                enddo
                do ivt=1,NLattVec(KPhase)
                  pom=ScalMul(vt6(1,ivt,1,KPhase),pq)
                  if(abs(pom-anint(pom)).gt..0005) go to 1560
                enddo
                jkn=iabs(j)+iabs(k)+iabs(n)+mmpa*10
                if(jkn.lt.jknMn) then
                  ip3=i+3
                  do l=1,3
                    rmp(ip3+NDim(KPhase)*(l-1))=-mpp(l)
                  enddo
                  rmp(ip3+NDim(KPhase)*3)=j
                  if(NDim(KPhase).gt.4)
     1              rmp(ip3+NDim(KPhase)*4)=k
                  if(NDim(KPhase).gt.5)
     1              rmp(ip3+NDim(KPhase)*5)=n
                  jknMn=jkn
                endif
1560          continue
            enddo
          enddo
          if(jknMn.ge.999990) then
            errtxt='generator inconsistent with the modulation vector.'
            go to 8000
          endif
1600    continue
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)/sqrt(1.2)
            enddo
          enddo
        endif
        call SetRealArrayTo(sp(4),NDimI(KPhase),0.)
        if(NDimI(KPhase).gt.1) then
          call od0do1(sp,sp,NDim(KPhase))
          do i=1,NSymmOld
            if(eqrv(rmp,rm6Old(1,i),NDimQ(KPhase),.001)) then
              call CopyVek(s6Old(1,i),spp,NDim(KPhase))
              call od0do1(spp,spp,NDim(KPhase))
              call CopyVek(spp(4),sp(4),NDimI(KPhase))
              exit
            endif
          enddo
        endif
2200    NSymm(KPhase)=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,NSymm(KPhase),1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,NSymm(KPhase),1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,NSymm(KPhase),1,KPhase),maxNDim,
     1                        ' ')
        call CopyMat(rmp,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call CopyVek(sp,s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1                 rm(1,NSymm(KPhase),1,KPhase),
     2                 NDim(KPhase))
        call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                s6(1,NSymm(KPhase),1,KPhase),
     2                symmc(1,NSymm(KPhase),1,KPhase),0)
        ISwSymm(NSymm(KPhase),1,KPhase)=1
        BratSymm(NSymm(KPhase),KPhase)=.true.
        if(Poradi) then
          j=NSymm(KPhase)-1
        else
          j=mod(ior-1,3)+1
        endif
        ZMag(NSymm(KPhase),1,KPhase)=MagFlag(j)
        call CrlMakeRMag(NSymm(KPhase),1)
        if(NDimI(KPhase).gt.0) then
          if(NDimI(KPhase).eq.1) then
          call multm(rm6(1,NSymm(KPhase),1,KPhase),ShiftSg,sp,
     1               NDim(KPhase),NDim(KPhase),1)
          do i=1,NDim(KPhase)
            sp(i)=s6(i,NSymm(KPhase),1,KPhase)+ShiftSg(i)-sp(i)
          enddo
          call od0do1(sp,sp,NDim(KPhase))
          s6(4,NSymm(KPhase),1,KPhase)=tau(j)+sp(4)+
     1                                 scalmul(quir(1,1,KPhase),sp)
          else
            if(NDimI(KPhase).eq.3) then
              m1=-2
              m2= 2
            else
              m1=0
              m2=0
            endif
            i=NSymm(KPhase)
            do 2270j=1,NDimI(KPhase)
              call Multm(Qu(1,j,1,KPhase),rm(1,i,1,KPhase),QuPom,1,3,3)
              do k=-2,2
                fk=k
                do l=-2,2
                  fl=l
                  do m=m1,m2
                    fmm=m
                    call CopyVek(QuPom,QuSum,3)
                    do n=1,3
                      QuSum(n)=QuSum(n)+Qu(n,1,1,KPhase)*fk
     1                                 +Qu(n,2,1,KPhase)*fl
                      if(NDimI(KPhase).eq.3)
     1                  QuSum(n)=QuSum(n)+Qu(n,3,1,KPhase)*fmm
                      QuSum(n)=QuSum(n)-anint(QuSum(n))
                    enddo
                    if(EqRV0(QuSum,3,.001)) go to 2270
                  enddo
                enddo
              enddo
              call FeChybne(-1.,-1.,'The set of modulation vectors '//
     1                      'is not complete with respect to the '//
     2                      'symmetry.',' ',SeriousError)
              go to 8100
2270        continue
            if(AskForDelta) then
              n=NSymm(KPhase)
              call CopyVek(s6(1,n,1,KPhase),sp,3)
              call EM50OriginShift(ShSg(1,KPhase),n)
              call EM50ReadDelta(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                           symmc(1,n,1,KPhase))
              call CopyVek(sp,s6(1,n,1,KPhase),3)
              call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                      symmc(1,n,1,KPhase),0)
            endif
          endif
        endif
        call od0do1(s6(1,NSymm(KPhase),1,KPhase),
     1              s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        iorold=ior
        nrold=nr
        if(idel(itxt).gt.0) then
          go to 1000
        else
          go to 5000
        endif
      endif
      go to 1100
5000  if(NCSymm(KPhase).eq.2) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call RealMatrixToOpposite(rm6(1,n,1,KPhase),rm6(1,n,1,KPhase),
     1                            NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call RealMatrixToOpposite(rm(1,n,1,KPhase),rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        BratSymm(n,KPhase)=.true.
        ZMag(n,1,KPhase)=MagFlag(NMagFlag)
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
        NCSymm(KPhase)=1
      endif
      if(MagInv.gt.0) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        if(NDimI(KPhase).eq.1) then
          s6(4,n,1,KPhase)=tau(idl)-
     1      scalmul(quir(1,1,KPhase),s6(1,n,1,KPhase))
        endif
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        ZMag(n,1,KPhase)=-1.
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
      endif
      call CompleteSymm(0,ich)
      go to 9999
8000  write(t80,'(''error in the '',i1,a2,'' Hall''''s generator: '',
     1      a)') NSymm(KPhase)+1,nty(NSymm(KPhase)+1),
     2           HallSymbol(:idel(HallSymbol))
      call FeChybne(-1.,-1.,t80,errtxt(1:idel(errtxt)),SeriousError)
8100  ich=1
9999  if(allocated(RM6Old)) deallocate(RM6Old,S6Old)
      return
      end
      integer function EM50IrrType(q)
      include 'fepc.cmn'
      dimension q(3)
      EM50IrrType=0
      do i=1,3
        if(abs(q(i)).gt..0001) EM50IrrType=EM50IrrType+1
      enddo
      if(EM50IrrType.eq.1) then
        do EM50IrrType=1,3
          if(abs(q(EM50IrrType)).gt..0001) go to 9999
        enddo
      else if(EM50IrrType.eq.2) then
        do i=1,3
          if(abs(q(i)).le..0001) then
            EM50IrrType=-i
            go to 9999
          endif
        enddo
      else if(EM50IrrType.eq.3) then
        EM50IrrType=4
      endif
9999  return
      end
      subroutine EM50GenVecCentr(nc,FirstTime,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical FirstTime
      ich=0
      if(nc.lt.8) then
        NLattVec(KPhase)=1
        call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
      endif
      if(nc.gt.1.and.nc.le.5) then
        NLattVec(KPhase)=2
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,3
          vt6(i,2,1,KPhase)=.5
        enddo
        if(nc.ne.5) vt6(nc-1,2,1,KPhase)=0.
      else if(nc.eq.6) then
        NLattVec(KPhase)=3
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        vt6(1,2,1,KPhase)=.666666667
        vt6(2,2,1,KPhase)=.333333333
        vt6(3,2,1,KPhase)=.333333333
        vt6(1,3,1,KPhase)=.333333333
        vt6(2,3,1,KPhase)=.666666667
        vt6(3,3,1,KPhase)=.666666667
      else if(nc.eq.7) then
        NLattVec(KPhase)=4
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do j=2,4
          do i=1,3
            if(i.ne.j-1) then
              vt6(i,j,1,KPhase)=.5
            else
              vt6(i,j,1,KPhase)=0.
            endif
          enddo
        enddo
      else if(nc.eq.8) then
        if(FirstTime) then
          call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                     NComp(KPhase),NPhase,1)
          call EM50ReadCellCentr(ich)
        endif
      endif
      if(nc.lt.7.and.NDimI(KPhase).gt.0) then
        do i=2,NLattVec(KPhase)
          call SetRealArrayTo(vt6(4,i,1,KPhase),NDimI(KPhase),0.)
        enddo
      endif
      do i=1,NLattVec(KPhase)
        do j=NDim(KPhase)+1,MaxNDim
          vt6(j,i,1,KPhase)=0.
        enddo
      enddo
      return
      end
      subroutine EM50SetGen(rm,nr,md,ndim)
      include 'fepc.cmn'
      dimension irm(9,7),rm(*)
      data irm/-1,0,0,0,-1,0,0,0,1,0,1,0,-1,-1,0,0,0,1,0,1,0,-1,0,0,
     1         0,0,1,1,1,0,-1,0,0,0,0,1,0,1,0,1,0,0,0,0,-1,0,-1,0,-1,0,0
     2        ,0,0,-1,0,1,0,0,0,1,1,0,0/
      id=md*3
      i1=-3
      mdd=md-1
      call SetRealArrayTo(rm,ndim**2,0.)
      do i=1,3
        i1=i1+3
        i1p=mod(i1+id,9)+1
        do j=1,3
          ij=i1+j
          ijp=i1p+mod(j+mdd,3)
          im=(ijp-1)/3+1
          jm=mod(ijp-1,3)+1
          rm(jm+(im-1)*ndim)=irm(ij,nr)
        enddo
      enddo
      return
      end
      subroutine EM50SetEps(ie,ipr,ieps,tau,idl,ipg,ig,imd,itxt,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension ie(*),ieps(*),tau(*),ipr(*)
      character*(*) itxt
      character*8 t8(3)
      ich=0
      do i=1,5
        ipr(i)=i
      enddo
      if(ipg.eq.1.or.ipg.eq.9.or.ipg.eq.21) then
        ie(1)=1
      else if(ipg.eq.2.or.ipg.eq.10.or.ipg.eq.22) then
        ie(1)=-1
      else if(ipg.eq.3.or.ipg.eq.4) then
        iz=7-2*ipg
        if(Monoclinic(KPhase).eq.iqv) then
          ie(1)= iz
        else
          ie(1)=-iz
        endif
      else if(ipg.eq.5) then
        if(Monoclinic(KPhase).eq.iqv) then
          ie(1)= 1
          ie(2)=-1
          tau(2)=tau(1)
        else
          ie(1)=-1
          tau(1)=tau(2)
          ie(2)= 1
        endif
      else if(ipg.ge.6.and.ipg.le.8) then
        if(iqv.gt.3) go to 9100
        iz=7-ipg
        if(iz.eq.0) iz=-1
        do i=1,3
          if(i.eq.iqv) then
            ie(i)= iz
          else
            ie(i)=-iz
          endif
        enddo
        if(ipg.eq.7) ie(imd)=-ie(imd)
        i1=0
        i2=0
        i3=0
        do i=1,3
          if(ie(i).eq.-1) cycle
          if(i1.eq.0) then
            i1=i
          else if(i2.eq.0) then
            i2=i
          else if(i3.eq.0) then
            i3=i
          endif
        enddo
        if(itxt(1:1).eq.'-') then
          kp=2
        else
          kp=1
        endif
        k=kp
        do i=1,idel(itxt)
          if(itxt(i:i).eq.';') itxt(i:i)=' '
        enddo
        do i=1,3
          call kus(itxt,k,t8(i))
        enddo
        if(i2.eq.0) then
          i2=i1+1
          if(i2.gt.3) i2=1
        endif
        itxt=itxt(1:kp)//t8(i1)(:idel(t8(i1)))//';'//
     1                   t8(i2)(:idel(t8(i2)))
      else if(ipg.eq.11.or.ipg.eq.23) then
        ie(1)=1
        ie(2)=-1
        tau(2)=tau(1)
      else if(ipg.eq.12.or.ipg.eq.24) then
        ie(1)=1
        ie(2)=-1
        ie(3)=-1
        tau(3)=tau(2)+tau(1)
        if(ig.ge.177) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.13.or.ipg.eq.25) then
        ie(1)=1
        ie(2)=1
        ie(3)=1
c        if(abs(tau(1)-.25).lt..0001.and.abs(tau(2)-.25).lt..0001) then
c          if(ig.eq.102) then
c            if(abs(tau(3)-.5).lt..0001) tau(2)=-.25
c          else
c            if(abs(tau(3))   .lt..0001) tau(2)=-.25
c          endif
c        endif
        if(ig.ge.183) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.14.or.ipg.eq.26) then
        ie(1)=-1
        if((ig.ge.115.and.ig.le.120).or.ig.eq.187.or.ig.eq.188) then
          ie(2)=1
          ie(3)=-1
          tau(3)=tau(2)
        else
          ie(2)=-1
          ie(3)=1
          tau(2)=tau(3)
        endif
        if(ig.ge.187) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.15.or.ipg.eq.27) then
        ie(1)=1
        ie(2)=-1
        ie(3)=1
        ie(4)=1
c        if(abs(tau(1)-.25).lt..0001.and.abs(tau(3)-.25).lt..0001) then
c          if(ig.eq.133.or.ig.eq.134) then
c            if(abs(tau(4))   .lt..0001) tau(3)=-.25
c          else
c            if(abs(tau(4)-.5).lt..0001) tau(3)=-.25
c          endif
c        endif
        if(ig.ge.191) then
          j=4
        else
          j=3
        endif
        call EM50ChngOrder(ie,ieps,tau,ipr,2,j)
      else if(ipg.eq.16) then
        ie(1)=1
        ieps(1)=1
      else if(ipg.eq.17) then
        ie(1)=1
        ieps(1)=-ieps(1)
        if(ig.eq.162.or.ig.eq.163) call EM50ChngOrder(ie,ieps,tau,ipr,
     1                                                2,3)
      else if(ipg.eq.18) then
        ie(1)=1
        ie(2)=-1
        if(idl.ne.2) ie(3)=1
        if(ig.eq.149.or.ig.eq.151.or.ig.eq.153)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.19) then
        do i=1,idl
          ie(i)=1
        enddo
        if(ig.eq.157.or.ig.eq.159)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.20) then
        do i=1,idl
          ie(i)=1
        enddo
        ieps(1)=-ieps(1)
        if(ig.eq.162.or.ig.eq.163)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      endif
      go to 9999
9100  ich=1
9999  return
      end
      subroutine EM50ChngOrder(ie,ieps,tau,ipor,i1,i2)
      include 'fepc.cmn'
      dimension ie(4),ieps(4),tau(4),ipor(4)
      ipor(i1)=i2
      ipor(i2)=i1
      i=ieps(i1)
      ieps(i1)=ieps(i2)
      ieps(i2)=i
      i=ie(i1)
      ie(i1)=ie(i2)
      ie(i2)=i
      p=tau(i1)
      tau(i1)=tau(i2)
      tau(i2)=p
      return
      end
      subroutine EM50ReadDelta(rmp,s,SymmString)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(NDim(KPhase),NDim(KPhase)),s(NDim(KPhase))
      character*(*) SymmString(NDim(KPhase))
      character*80 Veta,Radka
      call FeBoldFont
      Radka='Complete translational part for the operator:'
      xqd=FeTxLength(Radka)+10.
      call FeNormalFont
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)+2,Radka,0,
     1                   LightGray,-1,0)
      dpom=40.
      xpom=float(NDim(KPhase))*FeTxLength('XXX')+dpom+10.
      tpom=(xqd-xpom)*.5
      xpom=(xqd+xpom)*.5-dpom
      il=1
      call CodeSymm(rmp,s,SymmString,0)
      call MakeSymmSt(Radka,SymmString)
      call FeQuestLblMake(id,xqd*.5,il,Radka,'C','B')
      il=il+1
      call FeQuestLblMake(id,tpom+10.,il,'Rotation','L','N')
      call FeQuestLblMake(id,xpom-15.,il,'Translation','L','N')
      il=il+1
      do i=1,NDim(KPhase)
        write(Veta,'(6i3)')(nint(rmp(i,j)),j=1,NDim(KPhase))
        if(i.le.3) then
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          call ToFract(s(i),Cislo,15)
          call FeQuestLblMake(id,xpom+EdwMarginSize,il,Cislo,'L','N')
        else
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.4) nEdwFirst=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,s(i),.false.,.true.)
        endif
        il=il+1
      enddo
      call FeQuestEvent(id,ich)
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=4,NDim(KPhase)
          call FeQuestRealFromEdw(nEdw,s(i))
          nEdw=nEdw+1
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine EM50OriginShift(Shift,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6),Shift(*)
      if(Klic.eq.0) then
        i1=1
        i2=NSymm(KPhase)
      else
        i1=Klic
        i2=Klic
      endif
      do i=i1,i2
        call multm(rm6(1,i,1,KPhase),Shift,xp,NDim(KPhase),
     1             NDim(KPhase),1)
        do j=1,NDim(KPhase)
          s6(j,i,1,KPhase)=s6(j,i,1,KPhase)-Shift(j)+xp(j)
        enddo
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      return
      end
      subroutine EM50Composition
      use Basic_mod
      use Atoms_mod
      use EditM50_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension fmult(:),ip(3)
      character*256 EdwStringQuest,Veta
      character*80  FormulaNew,FormulaOld
      character*7   MenuIon(8)
      character*2   LastType
      integer RolMenuSelectedQuest,FFTypeOld,iar(1),FeRGBCompress,
     1        AtNumMax,CrwStateQuest
      integer :: WFMaxAtNum(3) = (/86,35,54/)
      integer :: WFChybi(4,3) = reshape
     1                          ((/ 0, 0, 0, 0,
     2                             10,18,21, 0,
     3                             57,58,60,62/),shape(WFChybi))
      logical FeYesNo,eqrv,EqIgCase,CrwLogicQuest,ChargeDensitiesOld,
     1        ExistFile,Chybi
      allocatable fmult
      save nEdwFormula,nEdwZ,nRolMenuAtType,nRolMenuIon,MenuIon,ip,
     1     nButtCalculateDensity,nButtCalculateFormula,nCrwFFTypeFirst,
     2     LastType,nCrwFFTypeLast,nButtAllDefault,nEdwAtRadius,
     3     ChargeDensitiesOld,nButtDefault,nButtDefine,nEdwRGB,
     4     XminColor,XmaxColor,YminColor,YmaxColor,nLblColor,
     5     nButtTypicalDist,nCrwJana,nCrwMolly,nCrwCoppens,nCrwEDAbs,
     6     nLblCharge1,nLblCharge2
      entry EM50CompositionMake(id)
      nnn=0
      ChargeDensitiesOld=ChargeDensities
      il=1
      tpom=5.
      Veta='Formula %units'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=300.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'%Formula','L',dpom,EdwYd,
     1                    1)
      nEdwFormula=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
      il=il+1
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwZ=EdwLastMade
      if(NUnits(KPhase).le.0) then
        if(NSymm(KPhase).gt.0.and.NLattVec(KPhase).gt.0) then
          NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        else
          NUnits(KPhase)=1
        endif
      endif
      call FeQuestIntEdwOpen(nEdwZ,NUnits(KPhase),.false.)
      Veta='For%mula from M40'
      xpom=xpom+dpom+50.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateFormula=ButtonLastMade
      il=il+1
      Veta='Calculate %density'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      xpom=xpom+dpom+5.
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='%Atom type:'
      xpom=tpom+FeTxLengthUnder(Veta)+30.
      dpom=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      Veta='%Ion (for X-xay):'
      tpom=xpom+dpom+25.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuIon=RolMenuLastMade
      Veta='Atom %radius:'
      tpom=xpom+dpom+25.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwAtRadius=EdwLastMade
      il=il+1
      Veta='Drawing color:'
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblColor=LblLastMade
      dpom=40.
      tpom=tpom+FeTxLength(Veta)+dpom+20.
      Veta='R:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      do i=1,3
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        xpom=xpom+80.
        tpom=xpom-15.
        if(i.eq.1) then
          nEdwRGB=EdwLastMade
          Veta='G:'
        else if(i.eq.2) then
          Veta='B:'
        endif
      enddo
      XMinColor=EdwXMinQuest(nEdwRGB+2)-230.
      XMaxColor=XMinColor+dpom
      YMinColor=EdwYMinQuest(nEdwRGB+2)
      YMaxColor=EdwYMaxQuest(nEdwRGB+2)
      xpom=xpom+10.
      Veta='D%efine/modify typical distances'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtTypicalDist=ButtonLastMade
      il=il+1
      Veta='%Set to default'
      dpom=FeTxLengthUnder(Veta)+10.
      xsh=20.
      xpom=xdqp*.5-dpom*1.5-xsh
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefault=ButtonLastMade
      Veta='A%ll to default'
      xpom=xpom+xsh+dpom
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAllDefault=ButtonLastMade
      Veta='Define o%wn'
      xpom=xpom+xsh+dpom
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefine=ButtonLastMade
      if(AllowChargeDensities) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Form factors from:'
        call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
        il=il+1
        xpom=15.
        tpom=xpom+CrwgXd+5.
!        ilp=-10*il
        ilp=il
!        il=ilp
        xsh=xdqp*.333333
        do i=1,6
          if(i.eq.1) then
            Veta='IT Vol.C 6.1.1.1-6.1.1.3'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='s%teps as in IT'
          else if(i.eq.2) then
            nCrwFFTypeFirst=CrwLastMade
            Veta='e%quidistant step 0.05'
          else if(i.eq.3) then
            il=ilp
            xpom=xpom+xsh
            tpom=tpom+xsh
            Veta='IT Vol.C 6.1.1.4'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='anal%ytical form'
          else if(i.eq.4) then
            xpom=xpom+xsh
            tpom=tpom+xsh
            il=ilp
            Veta='STO wave functions'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='JA%NA'
          else if(i.eq.5) then
            Veta='M%OLLY'
          else if(i.eq.6) then
            Veta='Co%ppens WEB page'
          else if(i.eq.7) then
            Veta='XD-CR'
          else if(i.eq.8) then
            Veta='XD-BBB'
          else if(i.eq.9) then
            Veta='XD-SCM'
          else if(i.eq.10) then
            Veta='XD-VM'
          endif
!          il=il-7
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.4) then
            nCrwJana=CrwLastMade
          else if(i.eq.5) then
            nCrwMolly=CrwLastMade
          else if(i.eq.6) then
            nCrwCoppens=CrwLastMade
          else if(i.eq.7) then
            nCrwXDCR=CrwLastMade
          else if(i.eq.8) then
            nCrwXDBBB=CrwLastMade
          else if(i.eq.9) then
            nCrwXDSCM=CrwLastMade
          else if(i.eq.10) then
            nCrwXDVM=CrwLastMade
          endif
        enddo
        nCrwFFTypeLast=CrwLastMade
!        il=il-7
        il=il+1
        Veta='INFORMATION: STO wave functions are taken by default '//
     1       'from the selected source.'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        tpom=tpom+FeTxLength(Veta(1:12))+3.
        Veta='However the source can modified in "Multipole '//
     1       'parameters" individually for each atomic type.'
        nLblCharge1=LblLastMade
        ilp=-10*il-7
!        il=il-7
        call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
        nLblCharge2=LblLastMade
        nCrwEDAbs=0
      else if(Radiation(KDatBlock).eq.ElectronRadiation) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Form factors:'
        call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
        xpom=15.
        tpom=xpom+CrwgXd+5.
        Veta='from IT Vol.C %4.3.1.1'
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          nCrw=CrwLastMade
          if(i.eq.1) then
            nCrwFFTypeFirst=CrwLastMade
            Veta='%calculated by Weickenmeier and Kohl procedure'
          else if(i.eq.2) then
            nCrwFFTypeLast=CrwLastMade
            il=-il*10-8
            tpom=tpom+40.
            xpom=xpom+40.
            Veta='a%pply absorption effects'
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,0)
            nCrwEDAbs=CrwLastMade
            call FeQuestCrwOpen(CrwLastMade,AbsFlagED.eq.3)
          endif
          call FeQuestCrwOpen(nCrw,i.eq.2.eqv.UseWKS)
        enddo
      else
        nCrwFFTypeFirst=0
        nCrwFFTypeLast =0
        nCrwEDAbs=0
      endif
      LastAtom=1
      go to 2000
      entry EM50CompositionRefresh
2000  if(Formula(KPhase).eq.' ') then
        i=ButtonDisabled
      else
        i=ButtonOff
      endif
      call FeQuestButtonOpen(nButtCalculateDensity,i)
      if(i.eq.ButtonOff.and.NAtCalc.le.0) i=ButtonDisabled
      call FeQuestButtonOpen(nButtCalculateFormula,i)
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
        AtNumMax=0
        do i=1,NAtFormula(KPhase)
          AtNumMax=max(AtNumMax,nint(AtNum(i,KPhase)))
        enddo
      endif
      if(LastAtom.gt.NAtFormula(KPhase)) then
        LastType=' '
      else
        LastType=AtTypeMenu(LastAtom)
      endif
      if(LastType.ne.' ') then
        call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                          NAtFormula(KPhase),LastAtom)
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1                'ions.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'formfac/ions.dat'
        endif
        if(.not.ExistFile(Veta)) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        read(ln,FormA) Veta
        if(Veta(1:1).ne.'#') rewind ln
2100    read(ln,FormA,end=2120) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,AtType(LastAtom,KPhase))) go to 2100
        call CloseIfOpened(ln)
        call StToInt(Veta,k,iar,1,.false.,ichp)
        if(ichp.ne.0) go to 2120
        n=iar(1)
        do i=1,n
          call Kus(Veta,k,MenuIon(i))
          if(EqIgCase(MenuIon(i),AtTypeFull(LastAtom,KPhase))) m=i
        enddo
        go to 2140
2120    n=1
        m=1
        MenuIon(1)=AtType(LastAtom,KPhase)
2140    call FeQuestRolMenuOpen(nRolMenuIon,MenuIon,n,m)
        call FeQuestRealEdwOpen(nEdwAtRadius,AtRadius(LastAtom,KPhase),
     1                          .false.,.false.)
        call FeQuestLblOn(nLblColor)
        nEdw=nEdwRGB
        call FeRGBUncompress(AtColor(LastAtom,KPhase),ip(1),ip(2),ip(3))
        do i=1,3
          call FeQuestIntEdwOpen(nEdw,ip(i),.false.)
          call FeQuestEudOpen(nEdw,0,255,1,0.,0.,0.)
          nEdw=nEdw+1
        enddo
        call FeDrawFrame(XminColor,YminColor,
     1                   XmaxColor-XminColor,YmaxColor-YminColor,
     2                   2.,White,Gray,Black,.false.)
        call FeFillRectangle(XminColor,XmaxColor,YminColor,YmaxColor,
     1                       4,0,0,AtColor(LastAtom,KPhase))
        call FeQuestButtonOpen(nButtDefault,ButtonOff)
        call FeQuestButtonOpen(nButtAllDefault,ButtonOff)
        call FeQuestButtonOpen(nButtDefine,ButtonOff)
        call FeQuestButtonOpen(nButtTypicalDist,ButtonOff)
      else
        call FeQuestLblOff(nLblColor)
      endif
      if(AllowChargeDensities) then
        if(NDim(KPhase).gt.3) then
          i2=3
          ChargeDensities=.false.
        else
          i2=6
        endif
        if(ChargeDensities) then
          do i=1,3
            if(EqIgCase(NameOfWaveFile,MenuWF(i))) then
              ntp=i+3
              exit
            endif
          enddo
          call FeQuestLblOn(nLblCharge1)
          call FeQuestLblOn(nLblCharge2)
        else
          if(FFType(KPhase).eq.-62) then
            ntp=1
          else if(FFType(KPhase).eq.-9) then
            ntp=3
          else
            ntp=2
          endif
          call FeQuestLblOff(nLblCharge1)
          call FeQuestLblOff(nLblCharge2)
        endif
        nCrw=nCrwFFTypeFirst
        do i=1,6
!          if(i.le.i2.and.(i.le.6.or.XDBnkDir.ne.' ')) then
          if(i.le.i2) then
            call FeQuestCrwOpen(nCrw,i.eq.ntp)
          else
            call FeQuestCrwDisable(nCrw)
          endif
          nCrw=nCrw+1
        enddo
        if(ChargeDensities) then
          nCrw=nCrwJana
          nCrwMaHo=0
          ntpp=0
          do i=1,3
            Chybi=.false.
!            do j=1,4
!              if(WFChybi(j,i).le.0) cycle
!              do k=1,NAtFormula(KPhase)
!                if(nint(AtNum(k,KPhase)).eq.WFChybi(j,i)) then
!                  Chybi=.true.
!                  go to 2150
!                endif
!              enddo
!            enddo
!2150        if(AtNumMax.gt.WFMaxAtNum(i).or.Chybi) then
!              call FeQuestCrwOff(nCrw)
!              call FeQuestCrwDisable(nCrw)
!              if(ntp.eq.i+3) ntp=0
!            else if(ntpp.eq.0) then
!              nCrwMaHo=nCrw
!              ntpp=i+3
!            endif
!            nCrw=nCrw+1
          enddo
          if(ntp.eq.0) then
            if(ntpp.ne.0) then
              call FeQuestCrwOn(nCrwMaHo)
              NameOfWaveFile=MenuWF(ntpp-3)
            endif
          endif
        endif
      endif
      go to 9999
      entry EM50CompositionCheck
      Klic=0
      go to 2200
      entry EM50CompositionUpdate
      Klic=1
2200  call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
      call EM50SaveFormFactors
      if(Klic.eq.1) go to 9999
      if(CheckType.eq.EventEdw) then
        if(CheckNumber.eq.nEdwFormula) then
          FormulaOld=Formula(KPhase)
          NAtFormulaOld=NAtFormula(KPhase)
          Formula(KPhase)=EdwStringQuest(nEdwFormula)
          Veta=Formula(KPhase)
          do i=1,idel(Veta)
            if(index(Cifry,Veta(i:i)).gt.0) Veta(i:i)=' '
          enddo
          do i=1,idel(FormulaOld)
            if(index(Cifry,FormulaOld(i:i)).gt.0) FormulaOld(i:i)=' '
          enddo
          call PitFor(1,ich)
          if(ich.ne.0) go to 2290
          if(NAtFormula(KPhase).gt.0) then
            call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
            if(NAtFormulaOld.le.0) FFType(KPhase)=-62
          else
            go to 9999
          endif
          call PitFor(0,ich)
          if(ich.ne.0) go to 2290
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
          if(NAtFormulaOld.gt.0) then
            call EM50SmartUpdateFormFactors
          else
            do i=1,NAtFormula(KPhase)
              FFBasic(1,i,KPhase)=-3333.
              AtRadius(i,KPhase)=-3333.
            enddo
          endif
          do i=1,NAtFormula(KPhase)
            if(AtRadius(i,KPhase).lt.-3000.)
     1        call EM50ReadOneFormFactor(i)
            if(FFBasic(1,i,KPhase).lt.-3000.) then
              if(ChargeDensities) then
                CoreValSource(i,KPhase)='Default'
                TypeCore(i,KPhase)=0
                TypeVal(i,KPhase)=0
                HNSlater(i,KPhase)=0
                HZSlater(i,KPhase)=0.
              endif
              call EM50OneFormFactorSet(i)
            endif
          enddo
          call FeQuestButtonOff(nButtCalculateDensity)
          if(NAtCalc.gt.0) call FeQuestButtonOff(nButtCalculateFormula)
          go to 2000
        else if(CheckNumber.eq.nEdwAtRadius) then
          call FeQuestRealAFromEdw(CheckNumber,
     1                             AtRadius(LastAtom,KPhase))
          go to 9999
        else if(CheckNumber.ge.nEdwRGB.and.CheckNumber.le.nEdwRGB+2)
     1    then
          i=CheckNumber-nEdwRGB+1
          call FeQuestIntFromEdw(CheckNumber,ip(i))
          AtColor(LastAtom,KPhase)=FeRGBCompress(ip(1),ip(2),ip(3))
          call FeFillRectangle(XminColor,XmaxColor,YminColor,YmaxColor,
     1                         4,0,0,AtColor(LastAtom,KPhase))
          go to 9999
        endif
2290    EVentType=CheckType
        EVentNumber=CheckNumber
      else if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtCalculateDensity.or.
     1          CheckNumber.eq.nButtCalculateFormula) then
          if(allocated(fmult)) deallocate(fmult)
          allocate(fmult(NAtFormula(KPhase)))
          call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
          if(CheckNumber.eq.nButtCalculateDensity) then
            Formula(KPhase)=EdwStringQuest(nEdwFormula)
            if(Formula(KPhase).ne.' ') call PitFor(0,ich)
            NInfo=0
          else
            pom=float(NLattVec(KPhase)*NSymm(KPhase))/
     1          float(NUnits(KPhase))
            call CopyVek(AtMult(1,KPhase),fmult,NAtFormula(KPhase))
            call SetRealArrayTo(AtMult(1,KPhase),NAtFormula(KPhase),0.)
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              j=isf(i)
              pomm=ai(i)*pom*CellVol(1,KPhase)/CellVol(iswa(i),KPhase)
              if(KModA(1,i).gt.0) then
                pomm=pomm*a0(i)
              else if(KFA(2,i).gt.0) then
                pomm=pomm*uy(2,KModA(2,i),i)
              endif
              AtMult(j,KPhase)=AtMult(j,KPhase)+pomm
            enddo
            do i=1,NAtFormula(KPhase)
              if(i.eq.1) then
                FormulaNew=AtType(i,KPhase)
              else
                FormulaNew=FormulaNew(:idel(FormulaNew)+1)//
     1               AtType(i,KPhase)(:idel(AtType(i,KPhase)))
              endif
              if(abs(AtMult(i,KPhase)-1.).lt..0001) cycle
              write(Cislo,'(f15.3)') AtMult(i,KPhase)
              call ZdrcniCisla(Cislo,1)
              FormulaNew=FormulaNew(:idel(FormulaNew))//
     1                   Cislo(:idel(Cislo))
            enddo
            NInfo=1
            write(TextInfo(NInfo),'(''Formula from M40 : '',a)')
     1        FormulaNew(:idel(FormulaNew))
          endif
          call EM50CalcDenAbs(ich)
          if(ich.ne.0) then
            TextInfo(1)='Error during calculation of density and '//
     1                  'absorption coefficient.'
            ich=0
          endif
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          if(CheckNumber.eq.nButtCalculateFormula.and.
     1       .not.eqrv(AtMult(1,KPhase),fmult,NAtFormula(KPhase),.001))
     2      then
            if(FeYesNo(-1.,-1.,'Do you want to update the formula '//
     1                      'correspondingly?',0)) then
              Formula(KPhase)=FormulaNew
              call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
            else
              call CopyVek(fmult,AtMult(1,KPhase),NAtFormula(KPhase))
            endif
          endif
          deallocate(fmult)
          go to 9999
        else if(CheckNumber.eq.nButtDefault) then
          call EM50ReadOneFormFactor(LastAtom)
          call EM50OneFormFactorSet(LastAtom)
        else if(CheckNumber.eq.nButtAllDefault) then
          call EM50ReadAllFormFactors
          call EM50FormFactorsSet
        else if(CheckNumber.eq.nButtDefine) then
          call EM50ReadOwnFormFactor(LastAtom,0)
        else if(CheckNumber.eq.nButtTypicalDist) then
          call EM50TypicalDist
        endif
        go to 2000
      else if(CheckType.eq.EVentRolMenu) then
        if(CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
        else if(CheckNumber.eq.nRolMenuIon) then
          Veta=AtTypeFull(LastAtom,KPhase)
          AtTypeFull(LastAtom,KPhase)=
     1      MenuIon(RolMenuSelectedQuest(nRolMenuIon))
          if(.not.EqIgCase(Veta,AtTypeFull(LastAtom,KPhase))) then
            call EM50OneFormFactorSet(LastAtom)
            call EM50ReadOneFormFactor(LastAtom)
          endif
        endif
        go to 2000
      else if(CheckType.eq.EventCrw) then
        if(Radiation(KDatBlock).ne.ElectronRadiation) then
          nCrw=nCrwFFTypeFirst
          ChargeDensities=.false.
          FFTypeOld=FFType(KPhase)
          do i=1,6
            if(CrwLogicQuest(nCrw)) then
              ChargeDensities=.false.
              if(i.eq.1) then
                FFType(KPhase)=-62
              else if(i.eq.2) then
                FFType(KPhase)=121
              else if(i.eq.3) then
                FFType(KPhase)=-9
              else if(i.ge.4) then
                ChargeDensities=.true.
                if(.not.ChargeDensitiesOld) then
                  call EM50ReadAllMultipoles
                  ChargeDensitiesOld=.true.
                endif
                FFType(KPhase)=121
                NameOfWaveFile=MenuWF(i-3)
              endif
            endif
            nCrw=nCrw+1
          enddo
          if(FFType(KPhase).ne.FFTypeOld) call EM50FormFactorsSet
        else
          if(CheckNumber.ge.nCrwFFTypeFirst.and.
     1       CheckNumber.le.nCrwFFTypeLast) then
            UseWKS=CrwLogicQuest(nCrwFFTypeLast)
            if(UseWKS) then
              if(CrwStateQuest(nCrwEDAbs).eq.CrwDisabled)
     1          call FeQuestCrwOpen(nCrwEDAbs,AbsFlagED.eq.3)
            else
              if(CrwStateQuest(nCrwEDAbs).ne.CrwDisabled) then
                if(CrwLogicQuest(nCrwEDAbs)) then
                  AbsFlagED=3
                else
                  AbsFlagED=0
                endif
                call FeQuestCrwDisable(nCrwEDAbs)
              endif
            endif
          else if(CheckNumber.eq.nCrwEDAbs) then
            if(CrwLogicQuest(nCrwEDAbs)) then
              AbsFlagED=3
            else
              AbsFlagED=0
            endif
          endif
        endif
        go to 2000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
      endif
9999  return
      end
      subroutine EM50CalcDenAbs(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*60 Veta
      logical Poprve
      call PwdSetTOF(KDatBlock)
      ich=0
      wmol=0.
      ami=0.
      Poprve=.true.
      do i=1,NAtFormula(KPhase)
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atweight',pomw,
     1                        0,ich)
        if(ich.ne.0) go to 9999
        pomm=AtMult(i,KPhase)
        if(.not.AtAbsCoeffOwn(KPhase)) then
          if(Poprve.and..not.isED.and..not.isTOF) then
            call CrlReadAbsCoeff(1,AtType(i,KPhase),
     1                           AtAbsCoeff(i,KPhase),ich)
            if(ich.ne.0) Poprve=.false.
          else
            AtAbsCoeff(i,KPhase)=0.
          endif
        endif
        wmol=wmol+pomw*pomm
        ami=ami+AtAbsCoeff(i,KPhase)*pomm*.1
      enddo
      NInfo=NInfo+1
      write(TextInfo(NInfo),'(''Molecular weight = '',f8.2)') wmol
      dx=float(NUnits(KPhase))*wmol*1.e24/
     1   (CellVol(1,KPhase)*AvogadroNumber)
      NInfo=NInfo+1
      write(TextInfo(NInfo),'(''Calculated density ='',f8.4,
     1                     '' g.cm**(-3)'')') dx
      ami=ami*float(NUnits(KPhase))/CellVol(1,KPhase)
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        if(.not.isED) then
          if(klam(KDatBlock).gt.0) then
            write(Veta,'(''('',a2,''-Kalfa) ='',f9.3,'' mm**-1'')')
     1        LamTypeD(klam(KDatBlock)),ami
          else
            write(Veta,'('' ='',f9.3,'' mm**-1'')') ami
          endif
        endif
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        write(Veta,'('' ='',f9.6,'' mm**-1'')') ami
      endif
      if(.not.isED.and.Radiation(KDatBlock).ne.ElectronRadiation) then
        NInfo=NInfo+1
        TextInfo(NInfo)='Absorption coefficient mi'//Veta(:idel(Veta))
      endif
9999  return
      end
      subroutine EM50TypicalDist
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TypicalDistC/ nRolMenuFirst,nEdwDist,TypDist,AtNames(2)
      dimension xp(2)
      character*256 t256
      character*80  t80
      character*2   nty,AtNames,At(2)
      integer WhatHappened,RolMenuSelectedQuest
      logical EqIgCase
      external EM50TypicalDistReadCommand,EM50TypicalDistWriteCommand,
     1         FeVoid
      save /TypicalDistC/
      equivalence (t80,t256)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_typdist.tmp','formatted','unknown')
      if(NTypicalDistMax.gt.0) then
        do i=1,NTypicalDist(KPhase)
          write(Cislo,'(f10.3)') DTypicalDist(i,KPhase)
          t80='typdist '//AtTypicalDist(1,i,KPhase)//' '//
     1                    AtTypicalDist(2,i,KPhase)//' '//
     2        Cislo(:idel(Cislo))
          call ZdrcniCisla(t80,4)
          write(ln,FormA) t80(:idel(t80))
        enddo
      endif
      call CloseIfOpened(ln)
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_typdist.tmp',xqd,
     1                          il,ilp,0)
      il=ilp+1
      xpom=5.
      dpom=40.
      spom=100.
      tpom=30.
      do i=1,3
        if(i.le.2) then
          write(t80,'(''%'',i1,a2,'' atom type'')') i,nty(i)
        else if(i.eq.3) then
          t80='Dist%ance'
          dpom=60.
          tpom=30.
        endif
        if(i.le.2) then
          call FeQuestRolMenuMake(id,xpom+tpom,il,xpom,il+1,t80,'C',
     1                            dpom+EdwYd,EdwYd,0)
          if(i.eq.1) nRolMenuFirst=RolMenuLastMade
        else
          call FeQuestEdwMake(id,xpom+tpom,il,xpom,il+1,t80,'C',dpom,
     1                        EdwYd,0)
        endif
        if(i.eq.3) then
          nEdwDist=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        endif
        xpom=xpom+spom
      enddo
      il=il+1
      dpom=60.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'F%rom file')
      nButtReadIn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1300  nRolMenu=nRolMenuFirst
      do i=1,2
        call FeQuestRolMenuOpen(nRolMenu,AtTypeMenu,
     1                          NAtFormula(KPhase),0)
        nRolMenu=nRolMenu+1
      enddo
      TypDist=-1.
1350  call FeQuestRealEdwOpen(nEdwDist,TypDist,TypDist.le.0.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  EM50TypicalDistReadCommand,EM50TypicalDistWriteCommand,FeVoid,
     2  FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        nRolMenu=nRolMenuFirst
        do i=1,2
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtNames(i),AtType(j,KPhase))) then
              call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                                NAtFormula(KPhase),j)
              go to 2025
            endif
          enddo
2025      nRolMenu=nRolMenu+1
        enddo
        go to 1350
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReadIn)
     1  then
        nRolMenu=nRolMenuFirst
        do i=1,2
          j=RolMenuSelectedQuest(nRolMenu)
          if(j.le.0) go to 2000
          AtNames(i)=AtType(j,KPhase)
          call Uprat(AtNames(i))
          nRolMenu=nRolMenu+1
        enddo
        if(OpSystem.le.0) then
          t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                'typical_distances.dat'
        else
          t256=HomeDir(:idel(HomeDir))//'bondval/typical_distances.dat'
        endif
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.ne.0) go to 2000
2120    read(ln,FormA80,end=2130) t80
        if(t80.eq.' ') go to 2120
        if(t80(1:1).eq.'#') go to 2120
        read(t80,'(2(a2,2x),f6.3)') At,pom
        if((EqIgCase(AtNames(1),At(1)).and.
     1      EqIgCase(AtNames(2),At(2))).or.
     2     (EqIgCase(AtNames(1),At(2)).and.
     3      EqIgCase(AtNames(2),At(1)))) then
          TypDist=pom
        else
          go to 2120
        endif
2130    call CloseIfOpened(ln)
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.eq.0) then
        call OpenFile(ln,fln(:ifln)//'_typdist.tmp','formatted',
     1                'unknown')
        n=0
3000    read(ln,FormA,end=3100) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,'typdist')) then
          n=n+1
          go to 3000
        endif
3100    call ReallocateTypicalDist(n)
        rewind ln
        n=0
3300    read(ln,FormA,end=3400) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,'typdist')) then
          n=n+1
          do i=1,2
            call Kus(t80,k,AtTypicalDist(i,n,KPhase))
          enddo
          call StToReal(t80,k,xp,1,.false.,ich)
          if(ich.ne.0) then
            n=n-1
          else
            DTypicalDist(n,KPhase)=xp(1)
          endif
          go to 3300
        endif
3400    close(ln,status='delete')
        NTypicalDist(KPhase)=n
      endif
      return
      end
      subroutine EM50TypicalDistReadCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TypicalDistC/ nRolMenuFirst,nEdwDist,TypDist,AtNames(2)
      dimension xp(1)
      character*80 t80
      character*2  AtNames
      character*(*) Command
      equivalence (ip,xp)
      save /TypicalDistC/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'typdist'.and.t80.ne.'!typdist') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        j=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
        if(j.le.0) then
          t80='the atom type "'//t80(:idel(t80))//'" doesn''t exist'
          go to 8100
        endif
        AtNames(i)=AtType(j,KPhase)
      enddo
      if(k.ge.lenc) go to 8000
      kk=k
      call StToReal(Command,k,xp,1,.false.,ich)
      if(ich.ne.0) go to 8030
      TypDist=xp(1)
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command',
     1              SeriousError)
9999  return
      end
      subroutine EM50TypicalDistWriteCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TypicalDistC/ nRolMenuFirst,nEdwDist,TypDist,AtNames(2)
      character*(*) Command
      character*80  ErrString
      character*2   AtNames
      integer RolMenuSelectedQuest
      save /TypicalDistC/
      ich=0
      Command='typdist'
      nRolMenu=nRolMenuFirst
      do i=1,2
        AtNames(i)=AtType(RolMenuSelectedQuest(nRolMenu),KPhase)
        call Uprat(AtNames(i))
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nRolMenu=nRolMenu+1
      enddo
      call FeQuestRealFromEdw(nEdwDist,TypDist)
      write(Cislo,100) TypDist
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
100   format(f15.4)
      end
      subroutine EM50SetAtTypeMenu(AtType,AtTypeMenu,n)
      character*(*) AtType(*),AtTypeMenu(*)
      character*2 t2
      logical EqIgCase
      do i=1,n
        AtTypeMenu(i)=AtType(i)
      enddo
      do i=1,n-1
        nn=1
        do j=i+1,n
          if(EqIgCase(AtType(i),AtType(j))) then
            nn=nn+1
            if(nn.eq.2)
     1        AtTypeMenu(i)=AtType(i)(:idel(AtType(i)))//'#1'
            write(t2,'(i2)') nn
            call Zhusti(t2)
            AtTypeMenu(j)=AtType(i)(:idel(AtType(i)))//'#'//
     1                    t2(:idel(t2))
          endif
        enddo
      enddo
      return
      end
      subroutine EM50Multipoles
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*256 InputFile,EdwStringQuest
      character*80 Veta,NotApplicable,MenuWFAct(6)
      character*30 Label(3)
      integer TypVodiku,RolMenuSelectedQuest,PopPom,PopCoreSum,AtNumP,
     1        RolMenuStateQuest,KMenuWFAct(6)
      logical EqIgCase,CrwLogicQuest,MameVodik,Cation,STOKnown,FFKnown,
     1        ExistFile
      real SingleZeta(28)
      save nButtMore,nCrwSDS,nCrwClementi,nLblFirst,nLblNotApplicable,
     1     nRolMenuAtType,nEdwPopFirst,nEdwNSlater,nEdwZetaSlater,
     2     nEdwHN,nEdwHZeta,TypVodiku,nCrwFromFile,nLblDeform,nLinka,
     3     nLblZetaSlater,nLblNSlater,nButtZetaRecalc,MenuWFAct,
     4     nRolMenuWaveFSource,KMenuWFAct,NButtImport,nButtEdit,
     5     STOKnown,FFKnown,LastAtomOld
      data Label/'Core','Valence','<-   Orbital populations   ->'/,
     1     NotApplicable/' '/
      entry EM50MultipolesMake(id)
      MenuWFAct(1)='STO-Default'
      MenuWFAct(2)='STO-JANA'
      MenuWFAct(3)='STO-MOLLY'
      MenuWFAct(4)='STO-Coppens WEB page'
      MenuWFAct(5)='STO-table'
      MenuWFAct(6)='FF-table'
      il=1
      tpom=5.
      call FeQuestLblMake(id,xdqp*.5,6,' ','C','B')
      call FeQuestLblOff(LblLastMade)
      nLblNotApplicable=LblLastMade
      Veta='%Atom type'
      xpomp=tpom+FeTxLengthUnder(Veta)+35.
      dpom=40.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      xpom=xpomp+dpom+110.
      do i=1,3
        tpom=xpom+CrwXd+10.
        if(i.eq.1) then
          Veta='S%DS FF'
        else if(i.eq.2) then
          Veta='Simple S%later'
          xpom0=xpom
        else
          Veta='S%TO or FF'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      2)
        if(i.eq.1) then
          nCrwSDS=CrwLastMade
        else if(i.eq.3) then
          nCrwClementi=CrwLastMade
        endif
        xpom=xpom+110.
      enddo
      il=il+1
      ilp=il
      tpom=5.
      Veta='%STO/FF source'
      dpom=FeTxLength(MenuWFAct(4))+25.
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuWaveFSource=RolMenuLastMade
      xpom=xpomp+dpom+20.
      Veta='Import STO-table'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtImport=ButtonLastMade
      xpom=xpom+dpom+20.
      Veta='Edit STO-table'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtEdit=ButtonLastMade
      tpom=xpom0+5.
      xpom=tpom+45.
      dpom=50.
      do i=1,2
        if(i.eq.1) then
          Veta='%H-N'
        else
          Veta='H-z%eta'
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwHN=EdwLastMade
        else
          nEdwHZeta=EdwLastMade
        endif
        il=il+1
      enddo
      il=il+1
      tpom=13.
      xpom=30.
      tpom0=13.
      xpom0=30.
      dpom=30.
      do 1250i=1,2
        il=ilp
        il=il+1
        call FeQuestLblMake(id,xdqp*(.25+float(i-1)*.5),il,Label(i),
     1                      'C','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) then
          nLblFirst=LblLastMade
          Veta='%Edit higher orbitals'
          dpomb=FeTxLength(Veta)+20.
          call FeQuestButtonMake(id,(xdq-dpomb)*.5,il+5,dpomb,ButYd,
     1                           Veta)
          nButtMore=ButtonLastMade
        endif
        if(i.eq.1) then
          call FeQuestLblMake(id,xdqp*.5,il,Label(3),'C','B')
          call FeQuestLblOff(LblLastMade)
        endif
        do j=1,4
          il=ilp+1
          do k=1,j
            il=il+1
            write(Veta,'(i1,a1)') j,OrbitName(k)
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,0)
            if(i.eq.1.and.j.eq.1.and.k.eq.1) nEdwPopFirst=EdwLastMade
          enddo
          tpom=tpom+dpom+30.
          xpom=xpom+dpom+30.
        enddo
        if(i.eq.2) go to 1250
        tpom=tpom0+xdqp*.5+10.
        xpom=xpom0+xdqp*.5+10.
1250  continue
      il=il+2
      call FeQuestLinkaMake(id,il)
      nLinka=LinkaLastMade
      call FeQuestLinkaOff(LinkaLastMade)
      il=il+1
      Veta='Deformation basis'
      call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
      nLblDeform=LblLastMade
      call FeQuestLblOff(LblLastMade)
      il=il+2
      Veta='Zeta-Slater:'
      xpom0=FeTxLength(Veta)+10.
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblZetaSlater=LblLastMade
      Veta='N-Slater:'
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblNSlater=LblLastMade
      dpom=45.
      xpom=xpom0
      tpom=xpom+dpom*.5
      sh=55.
      do i=1,8
        write(Veta,'(''l='',i1)') i-1
        call FeQuestEdwMake(id,tpom,il-1,xpom,il,Veta,'C',dpom,EdwYd,0)
        if(i.eq.1) nEdwZetaSlater=EdwLastMade
        xpom=xpom+sh
        tpom=tpom+sh
      enddo
      il=il+1
      xpom=xpom0
      tpom=xpom+dpom*.5
      do i=1,8
        call FeQuestEdwMake(id,tpom,il-1,xpom,il,' ','C',dpom,EdwYd,0)
        if(i.eq.1) nEdwNSlater=EdwLastMade
        xpom=xpom+sh
        tpom=tpom+sh
      enddo
      il=il+1
      Veta='%Recalculate zeta''s from single zeta exponents'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xdqp-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtZetaRecalc=ButtonLastMade
      LastAtom=1
      LastAtomOld=-1
      KartId=id
      go to 2000
      entry EM50MultipoleRefresh
2000  if(ChargeDensities) then
        NotApplicable=' '
        if(LastAtomOld.ne.LastAtom) then
          STOKnown=.false.
          FFKnown=.false.
        endif
        call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
        call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                          NAtFormula(KPhase),LastAtom)
        TypVodiku=3
        MameVodik=EqIgCase(AtType(LastAtom,KPhase),'H')
        if(MameVodik) then
          if(PopVal(1,LastAtom,KPhase).eq.-1) then
            TypVodiku=1
          else if(PopVal(1,LastAtom,KPhase).eq.-2) then
            TypVodiku=2
          endif
        endif
        if(.not.MameVodik.or.TypVodiku.eq.3) then
          if(EqIgCase(CoreValSource(LastAtom,KPhase),'Default').or.
     1       EqIgCase(CoreValSource(LastAtom,KPhase),NameOfWaveFile))
     2      then
            CoreValSource(LastAtom,KPhase)='Default'
            n=1
            call FeQuestButtonClose(nButtImport)
            call FeQuestButtonClose(nButtEdit)
          else
            n=1
            do i=1,4
              if(EqIgCase(CoreValSource(LastAtom,KPhase),MenuWF(i)))
     1          then
                n=i+1
                exit
              endif
            enddo
            if(n.eq.1) then
              if(EqIgCase(CoreValSource(LastAtom,KPhase),'FF-table'))
     1          then
                n=6
                call FeQuestButtonLabelChange(nButtImport,
     1                                        'Import FF-table')
                call FeQuestButtonLabelChange(nButtEdit,
     1                                        'Edit FF-table')
              else if(EqIgCase(CoreValSource(LastAtom,KPhase),
     1                         'STO-table')) then
                n=5
                call FeQuestButtonLabelChange(nButtImport,
     1                                        'Import STO-table')
                call FeQuestButtonLabelChange(nButtEdit,
     1                                        'Edit STO-table')
              endif
              call FeQuestButtonOpen(nButtImport,ButtonOff)
              call FeQuestButtonOpen(nButtEdit,ButtonOff)
            else
              call FeQuestButtonClose(nButtImport)
              call FeQuestButtonClose(nButtEdit)
            endif
          endif
          KMenuWFAct=1
          do i=1,3
            if(EqIgCase(NameOfWaveFile,MenuWF(i))) then
              KMenuWFAct(i+1)=0
              exit
            endif
          enddo
          call FeQuestRolMenuWithEnableOpen(nRolMenuWaveFSource,
     1                                      MenuWFAct,KMenuWFAct,6,n)
        else
          call FeQuestRolMenuDisable(nRolMenuWaveFSource)
        endif
        do i=1,8
          call FeQuestRealEdwOpen(nEdwZetaSlater+i-1,
     1                            ZSlater(i,LastAtom,KPhase),
     2                            .false.,.false.)
          call FeQuestIntEdwOpen(nEdwNSlater+i-1,
     1                           NSlater(i,LastAtom,KPhase),.false.)
        enddo
        call FeQuestButtonOff(nButtZetaRecalc)
        if(TypVodiku.ne.2.and.MameVodik.or..not.MameVodik) then
          call FeQuestEdwClose(nEdwHN)
          call FeQuestEdwClose(nEdwHZeta)
        endif
        nEdw=nEdwPopFirst
        nLbl=nLblFirst
        do i=1,2
          if(TypVodiku.ne.3.and.MameVodik) then
            call FeQuestLblOff(nLbl)
            if(i.eq.1) call FeQuestLblOff(nLbl+2)
            call FeQuestButtonClose(nButtMore)
          else
            call FeQuestLblOn(nLbl)
            if(i.eq.1) call FeQuestLblOn(nLbl+2)
            call FeQuestButtonOpen(nButtMore,ButtonOff)
          endif
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              if(TypVodiku.ne.3) then
                call FeQuestEdwClose(nEdw)
              else
                if(i.eq.1) then
                  PopPom=PopCore(nn,LastAtom,KPhase)
                else
                  PopPom=PopVal(nn,LastAtom,KPhase)
                endif
                call FeQuestIntEdwOpen(nEdw,PopPom,.false.)
              endif
              nEdw=nEdw+1
            enddo
          enddo
          nLbl=nLbl+1
        enddo
        nCrw=nCrwSDS
        do i=1,3
          if(MameVodik) then
            call FeQuestCrwOpen(nCrw,TypVodiku.eq.i)
          else
            call FeQuestCrwClose(nCrw)
          endif
          nCrw=nCrw+1
        enddo
        if(MameVodik) then
          if(TypVodiku.eq.2) then
            call FeQuestIntEdwOpen(nEdwHN,HNSlater(LastAtom,KPhase),
     1                             .false.)
            call FeQuestRealEdwOpen(nEdwHZeta,
     1                              HZSlater(LastAtom,KPhase),.false.,
     2                              .false.)
          endif
        endif
        call FeQuestLinkaOn(nLinka)
        call FeQuestLblOn(nLblDeform)
        call FeQuestLblOn(nLblZetaSlater)
        call FeQuestLblOn(nLblNSlater)
      else
        call FeQuestCrwClose(nCrwFromFile)
        do i=1,8
          call FeQuestEdwClose(nEdwNSlater+i-1)
          call FeQuestEdwClose(nEdwZetaSlater+i-1)
        enddo
        call FeQuestButtonClose(nButtZetaRecalc)
        if(RolMenuStateQuest(nRolMenuAtType).eq.RolMenuOpened)
     1    call FeQuestRolMenuClose(nRolMenuAtType)
        call FeQuestEdwClose(nEdwHN)
        call FeQuestEdwClose(nEdwHZeta)
        nEdw=nEdwPopFirst
        nLbl=nLblFirst
        do i=1,2
          call FeQuestLblOff(nLbl)
          if(i.eq.1) call FeQuestLblOff(nLbl+2)
          call FeQuestButtonClose(nButtMore)
          do j=1,4
            do k=1,j
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          enddo
          nLbl=nLbl+1
        enddo
        il=1
        if(NDim(KPhase).gt.3.or..not.AllowChargeDensities) then
          NotApplicable='The multipole option is not applicable.'
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        else if(.not.ChargeDensities) then
          NotApplicable='The multipole option not yet activated.'
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        else
          NotApplicable=' '
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        endif
        call FeQuestLinkaOff(nLinka)
        call FeQuestLblOff(nLblDeform)
        call FeQuestLblOff(nLblZetaSlater)
        call FeQuestLblOff(nLblNSlater)
      endif
      go to 9999
      entry EM50MultipolesCheck
      Klic=0
      go to 2100
      entry EM50MultipolesUpDate
      Klic=1
2100  if(NDim(KPhase).gt.3.or..not.ChargeDensities) go to 9999
      LastAtomOld=LastAtom
      if(TypVodiku.eq.2) then
        call FeQuestIntFromEdw(nEdwHN,HNSlater(LastAtom,KPhase))
        call FeQuestRealFromEdw(nEdwHZeta,
     1                          HZSlater(LastAtom,KPhase))
      else if(TypVodiku.eq.3) then
        nEdw=nEdwPopFirst
        do i=1,2
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
      endif
      do i=1,8
        call FeQuestRealFromEdw(nEdwZetaSlater+i-1,
     1                          ZSlater(i,LastAtom,KPhase))
        call FeQuestIntFromEdw(nEdwNSlater+i-1,
     1                         NSlater(i,LastAtom,KPhase))
      enddo
      if(Klic.eq.1) go to 9900
      if(CheckType.eq.EventRolMenu) then
        if(CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(CheckNumber)
          go to 9900
        else if(CheckNumber.eq.nRolMenuWaveFSource) then
          i=RolMenuSelectedQuest(CheckNumber)
          Veta=CoreValSource(LastAtom,KPhase)
          if(i.le.1) then
            CoreValSource(LastAtom,KPhase)='Default'
          else if(i.le.4) then
            CoreValSource(LastAtom,KPhase)=MenuWF(i-1)
          else if(i.eq.5) then
            if(.not.STOKnown) then
              if(EqIgCase(Veta,'Default').or.EqIgCase(Veta,'FF-table'))
     1          then
                Veta=HomeDir(:idel(HomeDir))//'FORMFAC'//
     1               DirectoryDelimitor//
     2               NameOfWaveFile(:idel(NameOfWaveFile))
              else
                Veta=CoreValSource(LastAtom,KPhase)
              endif
              call ReadAndSaveWaveFunction(AtType(LastAtom,KPhase),
     1                                     LastAtom,Veta,ich)
              STOKnown=.true.
            endif
            CoreValSource(LastAtom,KPhase)='STO-table'
          else if(i.eq.6) then
            if(.not.EqIgCase(Veta,'FF-table').and..not.FFKnown) then
              call SetCoreVal(LastAtom)
              FFKnown=.true.
            endif
            CoreValSource(LastAtom,KPhase)='FF-table'
          endif
          go to 9900
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMore)
     1  then
        call EM50PopOrbMore
        go to 9900
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtImport)
     1  then
        xqdp=300.
        Veta='Specify input file:'
        InputFile=' '
        idp=NextQuestId()
        call FeQuestCreate(idp,-1.,-1.,xqdp,1,Veta,0,LightGray,0,0)
        il=1
        bpom=50.
        xpom=5.
        tpom=5.
        dpom=xqdp-bpom-20.
        call FeQuestEdwMake(idp,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        nEdwName=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,InputFile)
        Veta='%Browse'
        xpom=tpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,bpom,ButYd,Veta)
        nButtBrowse=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EVentButton.and.CheckNumber.eq.nButtBrowse) then
          InputFile=EdwStringQuest(nEdwName)
          call FeFileManager('Select input structure',InputFile,'*.*',
     1                        0,.true.,ichp)
          if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,InputFile)
          go to 1500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          InputFile=EdwStringQuest(nEdwName)
          if(.not.ExistFile(InputFile)) then
            Veta='The file "'//InputFile(:idel(InputFile))//
     1           '" doesn''t exist, try again.'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
          call ReadAndSaveWaveFunction(AtType(LastAtom,KPhase),
     1                                 LastAtom,InputFile,ich)
        endif
        call FeQuestRemove(idp)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEdit)
     1  then
        Klic=0
        if(EqIgCase(CoreValSource(LastAtom,KPhase),'FF-table')) then
          Klic=1
        else if(EqIgCase(CoreValSource(LastAtom,KPhase),'STO-table'))
     1    then
          Klic=2
        endif
        if(Klic.gt.0) call EM50ReadOwnFormFactor(LastAtom,Klic)
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtZetaRecalc) then
        nEdw=nEdwPopFirst
        do i=1,2
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
        call RealAFromAtomFile(AtTypeFull(LastAtom,KPhase),
     1                         'Single_zeta_exponents',SingleZeta,28,
     2                         ich)
        if(ich.eq.0) then
          SumZ=0.
          SumN=0.
          do i=1,28
            pom=PopVal(i,LastAtom,KPhase)
            SumZ=SumZ+pom*SingleZeta(i)
            SumN=SumN+pom
          enddo
          if(SumN.gt.0.) then
            write(Cislo,100) 2.*SumZ/SumN
          else
            PopCoreSum=0
            do i=1,28
              PopCoreSum=PopCoreSum+PopCore(i,LastAtom,KPhase)
            enddo
            NumAt=nint(AtNum(LastAtom,KPhase))
            Cation=PopCoreSum.lt.NumAt
            if(PopCoreSum.eq.2) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(2)
                else
                  pom=(SingleZeta(2)+SingleZeta(3))/2.
                endif
              else
                pom=SingleZeta(1)
              endif
            else if(PopCoreSum.eq.10) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(4)
                else
                  pom=(SingleZeta(4)+SingleZeta(5))/2.
                endif
              else
                pom=SingleZeta(3)
              endif
            else if(PopCoreSum.eq.18) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(7)
                else
                  if(NumAt.ge.31.and.NumAt.le.35) then
                    pom=(SingleZeta(7)+SingleZeta(8))/2.
                  else if(NumAt.ge.21.and.NumAt.le.30) then
                    pom=SingleZeta(6)
                  endif
                endif
              else
                pom=SingleZeta(5)
              endif
            else if(PopCoreSum.eq.36) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(11)
                else if(NumAt.ge.49.and.NumAt.le.53) then
                  pom=(SingleZeta(11)+SingleZeta(12))/2.
                else if(NumAt.ge.39.and.NumAt.le.48) then
                  pom=SingleZeta(9)
                endif
              else
                pom=SingleZeta(8)
              endif
            else if(PopCoreSum.eq.54) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(16)
                else if(NumAt.ge.81.and.NumAt.le.85) then
                  pom=(SingleZeta(16)+SingleZeta(17))/2.
                else if(NumAt.ge.71.and.NumAt.le.80) then
                  pom=SingleZeta(13)
                endif
              else
                pom=SingleZeta(12)
              endif
            else if(PopCoreSum.eq.86) then
              pom=SingleZeta(17)
            endif
            pom=2.*pom
          endif
          read(Cislo,100) pom
          ZSlater(1:8,LastAtom,KPhase)=pom
          do i=1,8
            call FeQuestRealEdwOpen(nEdwZetaSlater+i-1,
     1                              ZSlater(i,LastAtom,KPhase),
     2                              .false.,.false.)
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwSDS.and.
     1        CheckNumber.le.nCrwClementi) then
        nCrw=nCrwSDS
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            if(i.le.2) then
              PopVal(1,LastAtom,KPhase)=-i
              if(i.eq.2) then
                HNSlater(LastAtom,KPhase)=0
                HZSlater(LastAtom,KPhase)=2.3
              endif
            else
              call SetIntArrayTo(PopCore(1,LastAtom,KPhase),28,0)
              call SetIntArrayTo(PopVal(1,LastAtom,KPhase),28,0)
              PopVal(1,LastAtom,KPhase)=1
            endif
            go to 9900
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 9999
9900  call EM50OneMultipoleSet(LastAtomOld)
      go to 2000
9999  return
100   format(f10.3)
      end
      subroutine ReadAndSaveWaveFunction(Atom,iat,AtWaveFile,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xp(3)
      integer ip(2)
      character*(*) AtWaveFile,Atom
      character*256 t256,STOString
      character*1   Orbit
      logical EqIgCase
      ich=0
      ln=NextLogicNumber()
      n=0
      do i=1,3
        if(EqIgCase(AtWaveFile,MenuWF(i))) then
          n=i
          exit
        endif
      enddo
      call OpenFile(ln,AtWaveFile,'formatted','old')
      if(ErrFlag.ne.0) go to 9100
1100  read(ln,FormA,end=9100) t256
      k=0
      call Kus(t256,k,Cislo)
      if(EqIgCase(Cislo,Atom)) then
1250    read(ln,FormA,end=9100) t256
        if(t256.eq.' ') go to 1250
        if(t256(1:1).eq.' ') then
          read(t256,'(i2,a1,i3)',end=9100,err=9100) i,Orbit,nsto
          j=0
          do ii=1,7
            if(EqIgCase(OrbitName(ii),Orbit)) then
              j=ii
              exit
            endif
          enddo
          if(i.le.0) go to 9100
          NCoefSTOA(i,j,iat,Kphase)=nsto
          read(ln,'(4(f12.8,f12.4,i2))')(CSTOA(i,j,k,iat,KPhase),
     1                                   ZSTOA(i,j,k,iat,KPhase),
     2                                   NSTOA(i,j,k,iat,KPhase),
     1                                   k=1,nsto)
          go to 1250
        endif
      else if(EqIgCase(Cislo,':'//Atom(:idel(Atom)))) then
        read(ln,FormA,end=9100) t256
        read(ln,FormA,end=9100) t256
        read(ln,FormA,end=9100) STOString
        k=0
        call kus(STOString,k,Cislo)
        if(.not.EqIgCase(Cislo,'STO')) go to 9100
1300    call kus(STOString,k,Cislo)
        Orbit=Cislo(2:2)
        j=0
        do i=1,7
          if(EqIgCase(OrbitName(i),Orbit)) then
            j=i
            exit
          endif
        enddo
        if(j.le.0) go to 9100
        read(Cislo(1:1),'(i1)',err=9100) i
        call StToInt(STOString,k,ip,2,.false.,ich)
        if(ich.ne.0) go to 9100
        NCoefSTOA(i,j,iat,KPhase)=ip(2)
        ii=0
1350    if(mod(ii,4).eq.0) then
          read(ln,FormA,end=9100) t256
          kk=0
        endif
        ii=ii+1
        call StToReal(t256,kk,xp,3,.false.,ich)
        NSTOA(i,j,ii,iat,KPhase)=nint(xp(3))
        CSTOA(i,j,ii,iat,KPhase)=xp(1)
        ZSTOA(i,j,ii,iat,KPhase)=xp(2)
        if(ii.lt.NCoefSTOA(i,j,iat,KPhase)) go to 1350
        if(k.lt.len(STOString)) go to 1300
      else
        go to 1100
      endif
      go to 9999
9100  ich=1
9999  call CloseIfOpened(ln)
      return
      end
      subroutine EM50FormFactorsSet
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ff(62),popc(7),popv(7),popp(7)
      character*80 Veta,VetaBasic
      logical EqIgCase,ExistWaveF
      i1=1
      i2=NAtFormula(KPhase)
      Klic=0
      go to 1000
      entry EM50OneFormFactorSet(n)
      i1=n
      i2=n
      Klic=0
      go to 1000
      entry EM50OneMultipoleSet(n)
      i1=n
      i2=n
      Klic=1
1000  if(FFType(KPhase).gt.0.or.FFType(KPhase).eq.-62.or.
     1   FFType(KPhase).eq.-56) then
        VetaBasic='Xray_form_factor_in_steps'
        nt=62
      else if(FFType(KPhase).eq.-9) then
        VetaBasic='Xray_form_factor_analytical'
        nt=9
      endif
      do 5000i=i1,i2
        if(Klic.eq.1) go to 2400
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1              'Neutron_scattering_length_real',ff,2,ich)
        ffn(i,KPhase)=ff(2)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1              'Neutron_scattering_length_imaginary',ff,2,ich)
        ffni(i,KPhase)=ff(2)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1   'Electron_form_factor_in_steps',FFEl(1,i,KPhase),62,ich)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),VetaBasic,ff,nt,ich)
        if(AtTypeMag(i,KPhase).ne.' ') then
          if(LocateSubstring(AtTypeMag(i,KPhase),'own',.false.,.true.)
     1       .le.0) then
            j=idel(AtTypeMag(i,KPhase))
            if(EqIgCase(AtTypeMag(i,KPhase)(j:j),'m')) then
              Veta='Magnetic_formfactor_<j0>+c<j2>'
            else
              Veta='Magnetic_formfactor_<j0>'
            endif
            call MagFFFromAtomFile(AtTypeFull(i,KPhase),
     1        AtTypeMag(i,KPhase),Veta,FFMag(1,i,KPhase),ich)
          endif
        endif
        if(FFType(KPhase).gt.0) then
          call TableFFToEquidistant(ff,FFBasic(1,i,KPhase),
     1                              AtTypeFull(i,KPhase))
        else if(FFType(KPhase).eq.-9) then
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        else if(FFType(KPhase).eq.-62.or.FFType(KPhase).eq.-56) then
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        else
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        endif
        do KDatB=1,NDatBlock
          ffra(i,KPhase,KDatB)=0.
          ffia(i,KPhase,KDatB)=0.
        enddo
        do KDatB=1,NDatBlock
          if(Radiation(KDatB).eq.XRayRadiation) then
            if(KAnRef(KDatB).ne.1) then
              if(klam(KDatB).gt.0) then
                call RealFromAtomFile(AtTypeFull(i,KPhase),'Xray_f''',
     1                       ffra(i,KPhase,KDatB),klam(KDatB),ich)
                call RealFromAtomFile(AtType(i,KPhase),'Xray_f"',
     1                       ffia(i,KPhase,KDatB),klam(KDatB),ich)
              else
                call EM50ReadAnom(AtTypeFull(i,KPhase),
     1            ffra(i,KPhase,KDatB),ffia(i,KPhase,KDatB),KDatB)
              endif
            endif
          else if(Radiation(KDatB).eq.NeutronRadiation) then
            ffra(i,KPhase,KDatB)=0.
            ffia(i,KPhase,KDatB)=0.
          endif
        enddo
        j=LocateInStringArray(atn,98,AtType(i,KPhase),IgnoreCaseYes)
        if(j.gt.0) then
          AtNum(i,KPhase)=float(j)
        else
          AtNum(i,KPhase)=0.
        endif
2400    if(.not.ChargeDensities) go to 5000
        if(EqIgCase(CoreValSource(i,KPhase),'FF-table')) then
          go to 5000
        else if(EqIgCase(AtType(i,KPhase),'H').and.
     1          PopVal(1,i,KPhase).lt.0) then
          if(PopVal(1,i,KPhase).eq.-1) then
            call RealAFromAtomFile(AtType(i,KPhase),VetaBasic,ff,62,ich)
            call TableFFToEquidistant(ff,FFBasic(1,i,KPhase),
     1                                AtType(i,KPhase))
          else if(PopVal(1,i,KPhase).eq.-2) then
            s=0.
            pomk=HZSlater(i,KPhase)/.52918
            do j=1,FFType(KPhase)
              FFBasic(j,i,KPhase)=
     1          SlaterFB(HNSlater(i,KPhase),0,s,pomk,pom,pom,ich)/
     2          pi4
              s=s+.05
            enddo
          endif
          call CopyVek(FFBasic(1,i,KPhase),FFVal(1,i,KPhase),62)
          go to 5000
        else
          ExistWaveF=.true.
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          popp=0.
          call ReadWaveF(AtType(i,KPhase),i,OrbitName(1),popp,FF,121,0,
     1                   CoreValSource(i,KPhase),ich)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ich.ne.0) then
            ExistWaveF=.false.
            go to 5000
          endif
          call SetRealArrayTo(FFCore(1,i,KPhase),FFType(KPhase),0.)
          call SetRealArrayTo(FFVal (1,i,KPhase),FFType(KPhase),0.)
          kp=0
          fnormv=0.
          do l=1,7
            kp=kp+l
            k=kp
            spopc=0.
            spopv=0.
            do j=l,7
              jp=j-l+1
              popc(jp)=PopCore(k,i,KPhase)
              spopc=spopc+popc(jp)
              popv(jp)=PopVal(k,i,KPhase)
              spopv=spopv+popv(jp)
              fnormv=fnormv+popv(jp)
              k=k+j
            enddo
            if(spopc.gt.0..and.ExistWaveF) then
              call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),popc,
     1                       FFCore(1,i,KPhase),FFType(KPhase),0,
     2                       CoreValSource(i,KPhase),ich)
              if(ich.ne.0) then
                ExistWaveF=.false.
                go to 5000
              endif
            endif
            if(spopv.ne.0.) then
              ich=0
              if(ExistWaveF) then
                call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),popv,
     1                         FFVal(1,i,KPhase),FFType(KPhase),0,
     2                         CoreValSource(i,KPhase),ich)
                if(ich.ne.0) then
                  call FeChybne(-1.,-1.,'STA wave function for "'//
     1                     AtType(i,KPhase)(:idel(AtType(i,KPhase)))//
     2                     '" is not on the database file "'//
     3                     NameOfWaveFile(:idel(NameOfWaveFile))//'".',
     4                     'Use another database or define own core '//
     5                     'and valence form factors',Warning)
                  go to 5000
                endif
              endif
            endif
          enddo
        endif
        do j=1,FFType(KPhase)
          FFBasic(j,i,KPhase)=FFCore(j,i,KPhase)+FFVal(j,i,KPhase)
        enddo
        if(fnormv.gt.0.) then
          fnormv=1./fnormv
          do j=1,FFType(KPhase)
            FFVal(j,i,KPhase)=FFVal(j,i,KPhase)*fnormv
          enddo
        endif
5000  continue
      return
      end
      subroutine EM50SaveFormFactors
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension AtRadiusIn(:),HZSlaterIn(:),NSlaterIn(:,:),AtMultIn(:),
     1          ZSlaterIn(:,:),FFnIn(:),FFniIn(:),FFraIn(:,:),
     2          FFiaIn(:,:),FFBasicIn(:,:),FFCoreIn(:,:),FFValIn(:,:),
     3          FFMagIn(:,:),FFElIn(:,:)
      character*256, allocatable :: CoreValSourceIn(:)
      character*7  AtTypeFullIn(:),AtTypeMagIn(:),AtTypeMagJIn(:),
     1             AtTypeMenuIn(:),AtP,AtPIn
      character*2  AtTypeIn(:)
      integer HNSlaterIn(:),TypeCoreIn(:),TypeValIn(:),PopCoreIn(:,:),
     1        PopValIn(:,:),AtColorIn(:),UzHoMa(:),TypeFFMagIn(:)
      logical EqIgCase
      allocatable AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     1            AtRadiusIn,AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,
     2            FFValIn,FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,
     3            TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,ZSlaterIn,
     4            NSlaterIn,HNSlaterIn,HZSlaterIn,FFElIn,UzHoMa,
     5            AtTypeMenuIn,TypeFFMagIn
      save nf,ntab,ntaba,AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     1     AtRadiusIn,AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,
     2     FFValIn,FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,FFElIn,
     3     TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,ZSlaterIn,
     4     NSlaterIn,HNSlaterIn,HZSlaterIn,AtTypeMenuIn,TypeFFMagIn
      nf=NAtFormula(KPhase)
      if(allocated(AtTypeIn))
     1  deallocate(AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     2             AtRadiusIn,AtTypeMenuIn,
     3             AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,FFValIn,
     4             FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,TypeFFMagIn,
     5             TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,
     6             ZSlaterIn,NSlaterIn,HNSlaterIn,HZSlaterIn,
     7             FFElIn,CoreValSourceIn)
      m=121
      allocate(AtTypeIn(nf),AtTypeFullIn(nf),AtTypeMagIn(nf),
     1         AtTypeMagJIn(nf),AtTypeMenuIn(nf),
     2         AtRadiusIn(nf),AtColorIn(nf),AtMultIn(nf),
     3         FFBasicIn(m,nf),FFCoreIn(m,nf),FFValIn(m,nf),
     4         FFraIn(nf,NDatBlock),FFiaIn(nf,NDatBlock),
     5         FFnIn(nf),FFniIn(nf),FFMagIn(7,nf),TypeFFMagIn(nf),
     6         TypeCoreIn(nf),TypeValIn(nf),PopCoreIn(28,nf),
     7         PopValIn(28,nf),ZSlaterIn(8,nf),NSlaterIn(8,nf),
     8         HNSlaterIn(nf),HZSlaterIn(nf),FFElIn(m,nf),
     9         CoreValSourceIn(nf))
      ntab=FFType(KPhase)
      ntaba=abs(FFType(KPhase))
      if(.not.allocated(AtTypeMenu)) then
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      do i=1,nf
        AtTypeIn(i)=AtType(i,KPhase)
        AtTypeMenuIn(i)=AtTypeMenu(i)
        AtTypeFullIn(i)=AtTypeFull(i,KPhase)
        AtTypeMagIn(i)=AtTypeMag(i,KPhase)
        AtTypeMagJIn(i)=AtTypeMagJ(i,KPhase)
        AtRadiusIn(i)=AtRadius(i,KPhase)
        AtColorIn(i)=AtColor(i,KPhase)
        AtMultIn(i)=AtMult(i,KPhase)
        call CopyVek(FFBasic(1,i,KPhase),FFBasicIn(1,i),ntaba)
        call CopyVek(FFEl(1,i,KPhase),FFElIn(1,i),62)
        do j=1,NDatBlock
          FFraIn(i,j)=FFra(i,KPhase,j)
          FFiaIn(i,j)=FFia(i,KPhase,j)
        enddo
        if(ChargeDensities) then
          call CopyVek(FFCore(1,i,KPhase),FFCoreIn(1,i),ntaba)
          call CopyVek(FFVal (1,i,KPhase),FFValIn (1,i),ntaba)
          TypeCoreIn(i)=TypeCore(i,KPhase)
          TypeValIn (i)=TypeVal (i,KPhase)
          call CopyVekI(PopCore(1,i,KPhase),PopCoreIn(1,i),28)
          call CopyVekI(PopVal(1,i,KPhase),PopValIn(1,i),28)
          call CopyVek (ZSlater(1,i,KPhase),ZSlaterIn(1,i),8)
          call CopyVekI(NSlater(1,i,KPhase),NSlaterIn(1,i),8)
          HNSlaterIn(i)=HNSlater(i,KPhase)
          HZSlaterIn(i)=HZSlater(i,KPhase)
          CoreValSourceIn(i)=CoreValSource(i,KPhase)
        endif
        FFnIn (i)=FFn (i,KPhase)
        FFniIn(i)=FFni(i,KPhase)
        if(AtTypeMagIn(i).ne.' ') then
          call CopyVek(FFMag(1,i,KPhase),FFMagIn(1,i),7)
          TypeFFMagIn(i)=TypeFFMag(i,KPhase)
        endif
      enddo
      go to 9999
      entry EM50SmartUpdateFormFactors
      allocate(UzHoMa(nf))
      call SetIntArrayTo(UzHoMa,nf,0)
      do 5000n=1,NAtFormula(KPhase)
        FFBasic(1,n,KPhase)=-3333.
        do i=1,nf
          AtP=AtTypeMenu(n)
          AtPIn=AtTypeMenuIn(i)
          j=index(AtTypeMenuIn(i),'#')
          k=index(AtTypeMenu(n),'#')
          if(j.le.0) then
            if(k.gt.0) AtP=AtTypeMenu(n)(:k-1)
          else
            if(k.le.0) AtPIn=AtTypeMenuIn(i)(:j-1)
          endif
          if(EqIgCase(AtPIn,AtP).and.UzHoMa(i).le.0) then
            AtTypeFull(n,KPhase)=AtTypeFullIn(i)
            AtTypeMenu(n)=AtTypeMenuIn(i)
            AtTypeMag(n,KPhase)=AtTypeMagIn(i)
            AtTypeMagJ(n,KPhase)=AtTypeMagJIn(i)
            AtRadius(n,KPhase)=AtRadiusIn(i)
            AtColor(n,KPhase)=AtColorIn(i)
            AtMult(n,KPhase)=AtMultIn(i)
            if(ntab.eq.FFType(KPhase))
     1        call CopyVek(FFBasicIn(1,i),FFBasic(1,n,KPhase),ntaba)
            call CopyVek(FFElIn(1,i),FFEl(1,n,KPhase),62)
            do j=1,NDatBlock
              FFra(n,KPhase,j)=FFraIn(i,j)
              FFia(n,KPhase,j)=FFiaIn(i,j)
            enddo
            if(ChargeDensities) then
              if(ntaba.eq.FFType(KPhase)) then
                call CopyVek(FFCoreIn(1,i),FFCore(1,n,KPhase),ntaba)
                call CopyVek(FFValIn (1,i),FFVal (1,n,KPhase),ntaba)
              endif
              TypeCore(n,KPhase)=TypeCoreIn(i)
              TypeVal (n,KPhase)=TypeValIn (i)
              call CopyVekI(PopCoreIn(1,i),PopCore(1,n,KPhase),28)
              call CopyVekI(PopValIn(1,i),PopVal(1,n,KPhase),28)
              call CopyVek (ZSlaterIn(1,i),ZSlater(1,n,KPhase),8)
              call CopyVekI(NSlaterIn(1,i),NSlater(1,n,KPhase),8)
              HNSlater(i,KPhase)=HNSlaterIn(i)
              HZSlater(i,KPhase)=HZSlaterIn(i)
              CoreValSource(i,KPhase)=CoreValSourceIn(i)
            endif
            FFn (n,KPhase)=FFnIn (i)
            FFni(n,KPhase)=FFniIn(i)
            if(AtTypeMagIn(i).ne.' ') then
              call CopyVek(FFMagIn(1,i),FFMag(1,n,KPhase),7)
              TypeFFMag(i,KPhase)=TypeFFMagIn(i)
            endif
            UzHoMa(i)=1
            go to 5000
          endif
        enddo
        AtRadius(n,KPhase)=-3333.
5000  continue
      deallocate(UzHoMa)
9999  return
      end
      subroutine EM50ReadOwnFormFactor(ia,Klic)
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real ff(121),CSTOPom(7,7,40),ZSTOPom(7,7,40)
      integer NCoefSTOPom(7,7),idt(MxDatBlock),NSTOPom(7,7,40)
      character*256 FileName,Veta,Slovo
      character*12 id(6)
      logical EqIgCase,WizardModeIn
      equivalence (IdNumbers(1) ,IdFFBasic),
     1            (IdNumbers(2) ,IdFFCore),
     2            (IdNumbers(3) ,IdFFVal),
     3            (IdNumbers(4) ,IdFFAnom),
     4            (IdNumbers(5) ,IdFFNeutron),
     5            (IdNumbers(6) ,IdSTO)
      data id/'ffbasic','ffcore','ffval','f''f"','ffneutron',
     1        'STO-function'/
      WizardModeIn=WizardMode
      WizardMode=.false.
      ln=NextLogicNumber()
      FileName=fln(:ifln)//'_ffedit.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(Klic.le.1) then
        Veta='# Here you can supply and edit form factors for atom '//
     1       'type: '//AtTypeMenu(ia)(:idel(AtTypeMenu(ia)))//' #'
      else
        Veta='# Here you can supply and edit STO wave function for '//
     1       'atom type: '//AtTypeMenu(ia)(:idel(AtTypeMenu(ia)))//' #'
      endif
      idl=idel(Veta)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA) Veta(:idl)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA)
      if(AllowChargeDensities) then
        if(ChargeDensities.eqv.Klic.eq.1) then
          Veta='# Form factors are to be typed as'
          write(ln,FormA) Veta(:idel(Veta))
          if(FFType(KPhase).eq.-9) then
            Veta='# coefficients for analytical approximation '//
     1           'according to IT vol.C 6.1.1.4'
          else
            if(FFType(KPhase).gt.0) then
              write(Cislo,'(i5)') FFType(KPhase)
              call zhusti(Cislo)
            else
              Cislo='62'
            endif
            Veta='# a table defined in '//Cislo(:idel(Cislo))//
     1           ' points of sin(theta)/lambda distributed'
          endif
          write(ln,FormA) Veta(:idel(Veta))
          if(FFType(KPhase).ne.-9) then
            if(FFType(KPhase).gt.0) then
              write(Cislo,'(f10.2)') float(FFType(KPhase)-1)*.05
              call Zhusti(Cislo)
              Veta='# equidistanly '
            else
              Veta='# according to IT vol.C table 6.1.1.1'
              Cislo='6.00'
            endif
            Veta=Veta(:idel(Veta)+1)//'over the interval from 0.00 '//
     1           'to '//Cislo(:idel(Cislo))
            write(ln,FormA) Veta(:idel(Veta))
          endif
          write(ln,FormA)
          if(ChargeDensities) then
            ik=3
          else
            ik=1
          endif
          nt=iabs(FFType(KPhase))
          do i=1,ik
            if(i.eq.1) then
              call CopyVek(FFBasic(1,ia,KPhase),ff,nt)
              k=IdFFBasic
            else if(i.eq.2) then
              call CopyVek(FFCore(1,ia,KPhase),ff,nt)
              k=IdFFCore
            else
              call CopyVek(FFVal(1,ia,KPhase),ff,nt)
              k=IdFFVal
            endif
            write(ln,100) id(k)(:idel(id(k)))
            if(FFType(KPhase).eq.-9) then
              write(ln,'(7f11.6)')(ff(j),j=1,nt)
            else
              write(ln,'(8f9.4)')(ff(j),j=1,nt)
            endif
            write(ln,101) id(k)(:idel(id(k)))
          enddo
        else if(ChargeDensities.eqv.Klic.eq.2) then
          Veta='# STO wave function'
          write(ln,FormA) Veta(:idel(Veta))
          write(ln,100) id(6)(:idel(id(6)))
          do j=1,7
            do i=1,7
              if(NCoefSTOA(i,j,ia,KPhase).le.0) cycle
              write(ln,102) i,OrbitName(j),NCoefSTOA(i,j,ia,KPhase)
              write(ln,103)(CSTOA(i,j,k,ia,KPhase),
     1                      ZSTOA(i,j,k,ia,KPhase),
     2                      NSTOA(i,j,k,ia,KPhase),
     3                      k=1,NCoefSTOA(i,j,ia,Kphase))
            enddo
          enddo
          write(ln,101) id(6)(:idel(id(6)))
        endif
        if(Klic.le.0) then
          write(ln,FormA)
          write(ln,100) id(IdFFAnom)(:idel(id(IdFFAnom)))
            ndt=0
            do i=1,NDatBlock
              if(Radiation(i).eq.XRayRadiation.or..not.ExistM90) then
                ndt=ndt+1
                idt(ndt)=i
                write(Veta,'(2f8.4)') FFra(ia,KPhase,i),
     1                                FFia(ia,KPhase,i)
                if(.not.ExistM90) then
                  Veta(21:)='! the wave length not yet specified'
                else
                  write(Cislo,'(f15.5)') LamAve(i)
                  call zhusti(Cislo)
                  Veta(21:)='! the wave length '//Cislo(:idel(Cislo))
                endif
                write(ln,FormA) Veta(:idel(Veta))
              endif
            enddo
            write(ln,101) id(IdFFAnom)(:idel(id(IdFFAnom)))
        endif
      endif
      if(ExistNeutronData.or..not.ExistM90) then
        write(ln,FormA)
        Veta='# scattering lengths for neutrons'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='# the first number defines the real component'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='# the second one the imaginary component'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFNeutron)(:idel(id(IdFFNeutron)))
        write(Veta,'(2f8.4)') FFn(ia,KPhase),FFni(ia,KPhase)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFNeutron)(:idel(id(IdFFNeutron)))
      endif
      call CloseIfOpened(ln)
      call FeEdit(FileName)
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3000  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3000
      k=0
      call kus(Veta,k,Slovo)
      if(.not.EqIgCase(Slovo,'begin')) go to 3000
      call kus(Veta,k,Slovo)
      i=LocateInStringArray(id,6,Slovo,IgnoreCaseYes)
      if(i.eq.IdFFBasic.or.i.eq.IdFFCore.or.i.eq.IdFFVal) then
        no=1
        nr=nt
      else if(i.eq.IdFFAnom) then
        no=ndt
        nr=2
      else if(i.eq.IdFFNeutron) then
        no=1
        nr=2
      else if(i.eq.IdSTO) then
        NCoefSTOPom=0
        go to 3200
      endif
      ip=1
3100  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3100
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) go to 4000
      call vykric(Veta)
      k=0
      call StToReal(Veta,k,ff(ip),nr,.false.,ich)
      if(ich.gt.1) then
        n=ich-2
        ip=ip+n
        nr=nr-n
      else if(ich.ne.0) then
        go to 3000
      else if(i.eq.IdFFAnom) then
        j=idt(ndt-no+1)
        FFra(ia,KPhase,j)=ff(1)
        FFia(ia,KPhase,j)=ff(2)
        no=no-1
      endif
      go to 3100
3200  read(ln,FormA,end=3000) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3200
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) then
        do j=1,7
          do i=1,7
            NCoefSTOA(i,j,ia,KPhase)=NCoefSTOPom(i,j)
            if(NCoefSTOPom(i,j).eq.0) cycle
            do ii=1,NCoefSTOPom(i,j)
              CSTOA(i,j,ii,ia,KPhase)=CSTOPom(i,j,ii)
              ZSTOA(i,j,ii,ia,KPhase)=ZSTOPom(i,j,ii)
              NSTOA(i,j,ii,ia,KPhase)=NSTOPom(i,j,ii)
            enddo
          enddo
        enddo
        go to 4000
      endif
      k=0
      call Kus(Veta,k,Cislo)
      read(Cislo,'(i1)',err=3000) i
      j=0
      do ii=1,7
        if(EqIgCase(OrbitName(ii),Cislo(2:2))) then
          j=ii
          exit
        endif
      enddo
      if(j.eq.0) go to 3000
      call StToInt(Veta,k,NCoefSTOPom(i,j),1,.false.,ich)
      if(ich.ne.0) go to 3000
      do ii=1,NCoefSTOPom(i,j)
        if(mod(ii-1,4)+1.eq.1) then
          read(ln,FormA,end=3000) Veta
          k=0
        endif
        call StToReal(Veta,k,ff,3,.false.,ich)
        if(ich.ne.0) go to 3000
        CSTOPom(i,j,ii)=ff(1)
        ZSTOPom(i,j,ii)=ff(2)
        NSTOPom(i,j,ii)=nint(ff(3))
      enddo
      go to 3200
4000  if(i.eq.IdFFBasic) then
        call CopyVek(ff,FFBasic(1,ia,KPhase),nt)
      else if(i.eq.IdFFCore) then
        call CopyVek(ff,FFCore(1,ia,KPhase),nt)
      else if(i.eq.IdFFVal) then
        call CopyVek(ff,FFVal(1,ia,KPhase),nt)
      else if(i.eq.IdFFNeutron) then
        FFn(ia,KPhase)=ff(1)
        FFni(ia,KPhase)=ff(2)
      endif
      no=no-1
      if(no.le.0) then
        go to 3000
      else
        go to 3100
      endif
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      WizardMode=WizardModeIn
      return
100   format('begin ',a)
101   format('end ',a)
102   format(i2,a1,i3)
103   format(4(f12.8,f12.4,i2))
      end
      subroutine EM50ReadOwnFormFactorMag(ia)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ff(7)
      character*256 FileName,Veta,Slovo
      character*10 :: id(2) =(/'ffmag    ',
     1                         'ffmagtype'/)
      logical EqIgCase,WizardModeIn
      equivalence (IdNumbers(1),IdFFMag),
     1            (IdNumbers(2),IdFFMagType)
      WizardModeIn=WizardMode
      WizardMode=.false.
      ln=NextLogicNumber()
      FileName=fln(:ifln)//'_ffedit.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      Veta='# Here you can supply and edit magnetic form factors for '//
     1     'atom type: '//
     1     AtTypeMag(ia,KPhase)(:idel(AtTypeMag(ia,KPhase)))//' #'
      idl=idel(Veta)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA) Veta(:idl)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA)
      if(ExistNeutronData.or..not.ExistM90) then
        write(ln,FormA)
        Veta='# seven coefficients of magnetic form factor as in IT'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFMag)(:idel(id(IdFFMag)))
        write(Veta,'(8f8.3)')(FFMag(i,ia,KPhase),i=1,7)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFMag)(:idel(id(IdFFMag)))
        write(ln,FormA)
        Veta='# FFMagType = 0  <j0> type - as it is'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='#           = 1  non <j0> type - individual values '//
     1       'will be multiplied by (sin(th)/Lambda)^2'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFMagType)(:idel(id(IdFFMagType)))
        if(TypeFFMag(ia,KPhase).eq.0) then
          Veta='   0'
        else
          Veta='   1'
        endif
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFMagType)(:idel(id(IdFFMagType)))
      endif
      call CloseIfOpened(ln)
      call FeEdit(FileName)
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3000  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3000
      k=0
      call kus(Veta,k,Slovo)
      if(.not.EqIgCase(Slovo,'begin')) go to 3000
      call kus(Veta,k,Slovo)
      i=LocateInStringArray(id,2,Slovo,IgnoreCaseYes)
      if(i.eq.IdFFMag) then
        nt=7
        nr=nt
      else if(i.eq.IdFFMagType) then
        nt=1
        nr=1
      endif
      ip=1
3100  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3100
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) go to 4000
      call vykric(Veta)
      k=0
      if(i.eq.IdFFMag) then
        call StToReal(Veta,k,ff(ip),nr,.false.,ich)
        if(ich.gt.1) then
          n=ich-2
          ip=ip+n
          nr=nr-n
        else if(ich.ne.0) then
          go to 3000
        endif
      else if(i.eq.IdFFMagType) then
        call StToInt(Veta,k,TypeFFMag(ia,KPhase),nt,.false.,ich)
        if(TypeFFMag(ia,KPhase).ne.0) TypeFFMag(ia,KPhase)=1
      endif
      go to 3100
4000  if(i.eq.IdFFMag) then
        call CopyVek(ff,FFMag(1,ia,KPhase),nt)
      endif
      go to 3000
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      WizardMode=WizardModeIn
      return
100   format('begin ',a)
101   format('end ',a)
      end
      subroutine EM50ReadAllFormFactors
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeRGBCompress,npom(3)
      i1=1
      i2=NAtFormula(KPhase)
      go to 1000
      entry EM50ReadOneFormFactor(n)
      i1=n
      i2=n
1000  do i=i1,i2
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atweight',
     1                        AtWeight(i,KPhase),0,ich)
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atradius',
     1                        AtRadius(i,KPhase),0,ich)
        call IntAFromAtomFile(AtTypeFull(i,KPhase),'color',npom,3,ich)
        AtColor(i,KPhase)=
     1    FeRGBCompress(npom(1),npom(2),npom(3))
        if(AllowChargeDensities.and.ChargeDensities) then
          call RealAFromAtomFile(AtTypeFull(i,KPhase),'ZSlater',
     1                           ZSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'NSlater',
     1                          NSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'PopCore',
     1                          PopCore(1,i,KPhase),28,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'PopVal',
     1                          PopVal(1,i,KPhase),28,ich)
        endif
      enddo
      return
      end
      subroutine EM50ReadAllMultipoles
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,NAtFormula(KPhase)
        if(AllowChargeDensities.and.ChargeDensities) then
          call RealAFromAtomFile(AtType(i,KPhase),'ZSlater',
     1                           ZSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'NSlater',
     1                          NSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'PopCore',
     1                          PopCore(1,i,KPhase),28,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'PopVal',
     1                          PopVal(1,i,KPhase),28,ich)
        endif
      enddo
      return
      end
      subroutine EM50ReadAnom(At,ffrOut,ffiOut,KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*256 t256
      character*80  Radka
      character*2   Atp
      logical ExistFile,EqIgCase
      dimension xx(4),ff(4,2),xp(3)
      equivalence (t256,t80)
      ln=0
      if(LamAve(KDatB).le.0.) then
        ffrOut=-1.
        ffiOut=-1.
        go to 9999
      else
        ffrOut=0.
        ffiOut=0.
      endif
      if(EqIgCase(At,'D')) then
        AtP='H'
      else
        AtP=At
      endif
      Energy=ToKeV/LamAve(KDatB)
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1              'anom.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/anom.dat'
      endif
      if(.not.ExistFile(t256)) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA80) Radka
      if(Radka(1:1).ne.'#') rewind ln
2100  read(ln,FormA80,end=9999) Radka
      k=0
      call kus(Radka,k,Cislo)
      if(.not.EqIgCase(Cislo,AtP)) go to 2100
2200  read(ln,FormA80) Radka
      j=1
2300  read(ln,*,err=9999,end=9999) xp
      jo=j
      if(xp(1).gt.Energy.or.j.lt.2) j=j+1
      if(jo.eq.j) then
        ff(j-1,1)=ff(j,1)
        ff(j-1,2)=ff(j,2)
        xx(j-1)  =xx(j)
      endif
      xx(j)  =xp(1)
      ff(j,1)=xp(2)
      ff(j,2)=xp(3)
      if(j.ge.4) then
        ffrOut=anint(Finter4(ff(1,1),xx,Energy)*10000.)*.0001
        ffiOut=anint(Finter4(ff(1,2),xx,Energy)*10000.)*.0001
        go to 9999
      else
        go to 2300
      endif
9999  call CloseIfOpened(ln)
      return
      end
      function EM50ReadAbsor(At,LamAveUse)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*256 t256
      character*80  Radka
      character*2   AtSearch
      logical ExistFile,EqIgCase
      real LamAveUse
      dimension xx(4),ff(4),xp(2)
      equivalence (t256,t80)
      EM50ReadAbsor=0.
      Energy=ToKeV/LamAveUse
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1              'absor.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/absor.dat'
      endif
      if(.not.ExistFile(t256)) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      read(ln,FormA80) Radka
      if(Radka(1:1).ne.'#') rewind ln
      if(ErrFlag.ne.0) go to 9999
      if(EqIgCase(At,'D')) then
        AtSearch='H'
      else
        AtSearch=At
      endif
      IAtSearch=KtAt(AtN,98,AtSearch)
      n=0
      if(IAtSearch.le.0) then
        EM50ReadAbsor=0.
        go to 9999
      endif
2100  read(ln,FormA80,end=2500) Radka
      k=0
      call kus(Radka,k,Cislo)
      if(.not.EqIgCase(Cislo,AtSearch)) go to 2100
2200  read(ln,FormA80) Radka
      j=1
2300  read(ln,*,err=9999,end=9999) xp
      jo=j
      if(xp(1).gt.Energy.or.j.lt.2) j=j+1
      if(jo.eq.j) then
        ff(j-1)=ff(j)
        xx(j-1)=xx(j)
      endif
      xx(j)=xp(1)
      ff(j)=xp(2)
      if(j.ge.4) then
        EM50ReadAbsor=anint(Finter4(ff,xx,Energy))
        go to 9999
      else
        go to 2300
      endif
2500  n=n-1
      AtSearch=AtN(IAtSearch+n)
      rewind ln
      go to 2100
9999  call CloseIfOpened(ln)
      return
      end
      subroutine EM50UpdateListek(KartIdOld)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      logical lpom
      lpom=BlockedActiveObj
      BlockedActiveObj=.true.
      if(KartId.eq.KartIdSymmetry) then
        call Em50SymmetryRefresh
      else if(KartId.eq.KartIdComposition) then
        call EM50CompositionRefresh
      else if(KartId.eq.KartIdMultipols) then
        call EM50MultipoleRefresh
      else if(KartId.eq.KartIdMagnetic) then
        call EM50MagneticRefresh
      endif
      BlockedActiveObj=lpom
      return
      end
      subroutine TRANSO(nn,l1,z,s,ff)
      include 'fepc.cmn'
      dimension ff(9),a(28)
      data a(1)/0.0/
      d=s**2+z**2
      a(2)=1./d
      n=nn-1
      tz=z+z
      ts=s+s
      do ll=1,l1
        l=ll-1
        if(ll.eq.1) go to 1000
        a(ll+1)=a(ll)*ts*float(l)/d
        a(ll)=0.0
1000    do nx=ll,n
          i1=nx
          i2=nx+1
          i3=i2+1
          a(i3)=(tz*float(nx)*a(i2)-float((nx+l)*(nx-ll))*a(i1))/d
        enddo
        ff(ll)=a(i3)
      enddo
      return
      end
      subroutine TableFFToEquidistant(FFIn,FFOut,AtTypeIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension FFIn(*),FFOut(*)
      character*(*) AtTypeIn
      dimension am(4,4),ami(4,4),b(4),AFFIn(4,62)
      n=62
      do i=1,n
        if(i.eq.1) then
          jp=0
        else if(i.gt.n-2) then
          jp=n-4
        else
          jp=i-2
        endif
        if(AtTypeIn.eq.'H') then
          pom=1./(ffxh(jp+1)-ffxh(jp+2))
        else
          pom=1./(ffx(jp+1)-ffx(jp+2))
        endif
        do j=1,4
          b(j)=FFIn(jp+j)-FFIn(jp+2)
          if(AtTypeIn.eq.'H') then
            xp=(ffxh(jp+j)-ffxh(jp+2))*pom
          else
            xp=(ffx(jp+j)-ffx(jp+2))*pom
          endif
          xs=1.
          do k=1,4
            am(j,k)=xs
            xs=xs*xp
          enddo
        enddo
        call matinv(am,ami,xp,4)
        call multm(ami,b,AFFIn(1,i),4,4,1)
        AFFIn(1,i)=AFFIn(1,i)+FFIn(jp+2)
      enddo
      sinthl=0.
      do i=1,121
        do j=1,n
          if(AtTypeIn.eq.'H') then
            ffxj=ffxh(j)
          else
            ffxj=ffx(j)
          endif
          if(sinthl.lt.ffxj.or.j.eq.n) then
            jp=min(j,n-2)
            pom=AFFIn(4,jp)
            if(AtTypeIn.eq.'H') then
              xp=(sinthl-ffxh(jp))/(ffxh(jp-1)-ffxh(jp))
            else
              xp=(sinthl-ffx(jp))/(ffx(jp-1)-ffx(jp))
            endif
            do k=3,1,-1
              pom=pom*xp+AFFIn(k,jp)
            enddo
            go to 4500
          endif
        enddo
4500    FFOut(i)=pom
        sinthl=sinthl+.05
      enddo
      return
      end
      subroutine EM50PopOrbMore
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      integer PopPom
      character*8 Veta
      character*7  Label(2)
      data Label/'Core','Valence'/
      id=NextQuestId()
      xdqq=400.
      call FeQuestCreate(id,-1.,-1.,xdqq,8,
     1                   'Define additional orbital populations',0,
     2                   LightGray,0,0)
      tpom=13.
      xpom=30.
      tpom0=13.
      xpom0=30.
      dpom=30.
      ilp=0
      do 1250i=1,2
        nn=10
        il=ilp
        il=il+1
        call FeQuestLblMake(id,xdqq*(.25+float(i-1)*.5),il,Label(i),'C',
     1                      'B')
        do j=5,7
          il=ilp+1
          do k=1,j
            nn=nn+1
            il=il+1
            write(Veta,'(i1,a1)') j,OrbitName(k)
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,0)
            if(i.eq.1) then
              PopPom=PopCore(nn,LastAtom,KPhase)
            else
              PopPom=PopVal(nn,LastAtom,KPhase)
            endif
            call FeQuestIntEdwOpen(EdwLastMade,PopPom,.false.)
            if(nn.eq.11.and.i.eq.1) nEdwFirst=EdwLastMade
          enddo
          tpom=tpom+dpom+30.
          xpom=xpom+dpom+30.
        enddo
        if(i.eq.2) go to 1250
        tpom=tpom0+xdqq*.5+10.
        xpom=xpom0+xdqq*.5+10.
1250  continue
      call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
      endif
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=1,2
          nn=10
          do j=5,7
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine EM50ReadCellCentr(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension vt6p(:,:),xp(6)
      character*256 EdwStringQuest
      character*80 t80
      character*2 nty
      allocatable vt6p
      logical eqrv
      allocate(vt6p(NDim(KPhase),1000))
      nvtp=NLattVec(KPhase)
      if(nvtp.le.0) then
        nvtp=1
        call SetRealArrayTo(vt6p,NDim(KPhase),0.)
      else
        do i=1,nvtp
          call CopyVek(vt6(1,i,1,KPhase),vt6p(1,i),NDim(KPhase))
        enddo
      endif
      ik=0
      id=NextQuestId()
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,9,'Centring vectors',1,
     1                   LightGray,0,0)
      t80='%Complete the set'
      dpom=FeTxLengthUnder(t80)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,9,dpom,ButYd,t80)
      nButtComplete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      dpom=80.
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,9,dpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonMake(id,xpom,0,dpom,ButYd,'%Previous')
      nButtPrevious=ButtonLastMade
      tpom=5.
      dpom=120.
      xpom=tpom+FeTxLength('XXXX')+15.
      pom=xpom
      il=1
      do i=1,16
        write(cislo,100) i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il,Cislo,'L',dpom,EdwYd,
     1                      1)
        if(i.eq.1) nEdwCentrFirst=EdwLastMade
        if(mod(il,8).eq.0) then
          xpom=xpom+xqd*.5
          tpom=tpom+xqd*.5
          il=0
        endif
        il=il+1
      enddo
      m=1
1200  n1=(m-1)*16+1
      n2=min(n1+15,nvtp)
      nEdw=nEdwCentrFirst
      do i=n1,n1+15
        write(cislo,100) i,nty(i)
        if(i.ne.1) then
          call FeQuestRealAEdwOpen(nEdw,vt6p(1,i),NDim(KPhase),
     1                             i.gt.nvtp,.true.)
          write(cislo,100) i,nty(i)
          call FeQuestEdwLabelChange(nEdw,Cislo)
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestLblMake(id,tpom,1,Cislo,'L','N')
          t80='0 0 0'
          if(NDim(KPhase).gt.3) t80=t80(1:5)//' 0'
          if(NDim(KPhase).gt.4) t80=t80(1:7)//' 0'
          if(NDim(KPhase).gt.5) t80=t80(1:9)//' 0'
          call FeQuestLblMake(id,pom+EdwMarginSize,1,t80,'L','N')
        endif
        nEdw=nEdw+1
      enddo
1500  if(nvtp.gt.16) then
        if(n1.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
        if(n2.lt.nvtp) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        call EM50CompleteCentr(1,vt6p,ubound(vt6p,1),nvtp,1000,ich)
        if(ich.eq.0) then
          QuestCheck(id)=0
        else
          call FeChybne(-1.,-1.,'the set of vectors isn''t complete.',
     1                  ' ',SeriousError)
          EventType=EventEdw
          EventNumber=min(nvtp-n1+2,16)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw) then
        nEdw=CheckNumber
        il=nEdw+n1-1
        if(EdwStringQuest(nEdw).eq.' ') then
          if(il.le.nvtp) then
            do i=il+1,nvtp
              call CopyVek(vt6p(1,i),vt6p(1,i-1),NDim(KPhase))
            enddo
            ipp=il
            nvtp=nvtp-1
            go to 1200
          else
            go to 1500
          endif
        endif
        do i=1,nvtp
          call FeQuestRealAFromEdw(nEdw,xp)
          if(i.ne.il.and.eqrv(xp,vt6p(1,i),NDim(KPhase),.0001)) then
            call FeChybne(-1.,-1.,'the centring vector already present',
     1                    'try again.',SeriousError)
            EventType=EventEdw
            EventNumber=nEdw
            go to 1500
          endif
        enddo
        if(il.gt.nvtp) nvtp=nvtp+1
        call FeQuestRealAFromEdw(nEdw,vt6p(1,il))
        call FeQuestRealAEdwOpen(nEdw,vt6p(1,il),NDim(KPhase),.false.,
     1                           .true.)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtComplete)
     1  then
        call EM50CompleteCentr(0,vt6p,ubound(vt6p,1),nvtp,1000,ich)
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     1         CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          m=m+1
        else
          m=m-1
        endif
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),nvtp,
     1                   NComp(KPhase),NPhase,1)
        NLattVec(KPhase)=nvtp
        do i=1,nvtp
          call CopyVek(vt6p(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
        enddo
      endif
      call FeQuestRemove(id)
9999  deallocate(vt6p)
      return
100   format(i2,a2)
      end
      subroutine EM50CompleteCentr(klic,vt6p,nvt,nvtp,nlim,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension vt6p(nvt,*)
      dimension xp(6)
      logical eqrv
      ich=0
      nvp=nvtp
      i=1
1600  i=i+1
      if(i.gt.nvtp) go to 9999
      j=1
1620  j=j+1
      if(j.gt.nvtp) go to 1600
      do k=1,NDim(KPhase)
        xp(k)=vt6p(k,i)+vt6p(k,j)
      enddo
      call od0do1(xp,xp,NDim(KPhase))
      do k=1,nvtp
        if(eqrv(xp,vt6p(1,k),NDim(KPhase),.0001)) go to 1620
      enddo
      if(klic.eq.0) then
        nvtp=nvtp+1
        if(nvtp.gt.nlim) then
          nvtp=nvp
          write(Cislo,'(i5)') nlim
          call zhusti(Cislo)
          call FeChybne(-1.,-1.,'maximun number of '//
     1                  Cislo(:idel(Cislo))//
     2                  ' centring vectors exceeded.',' ',SeriousError)
          go to 9000
        endif
      else
        go to 9000
      endif
      call copyvek(xp,vt6p(1,nvtp),NDim(KPhase))
      go to 1620
9000  ich=1
9999  return
      end
      subroutine EM50DefSpaceGroup(Klic,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      ich=0
      id=NextQuestId()
      xqd=600.-2.*KartSidePruh
      il=14
      Veta='Define '
      k=idel(Veta)+2
      if(NDim(KPhase).gt.3) then
        Veta(k:)='super'
        k=idel(Veta)+1
      endif
      Veta(k:)='space group'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,0)
      call EM50SymmetryMake(id,1)
      call EM50SymmetryRefresh
      ErrFlag=0
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call EM50SymmetryCheck
        if(ErrFlag.eq.0) call EM50SymmetryRefresh
        go to 1500
      endif
      if(ich.eq.0) then
        if(NUnits(KPhase).le.0)
     1    NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        if(Klic.eq.0) then
          call QuestionRewriteFile(50)
          call iom50(0,0,fln(:ifln)//'.m50')
        endif
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'.l52')
      return
      end
      subroutine GenerPGFile
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension io(3),QuSymm(3,3)
      character*256 Veta
      character*8 SPG(39)
      character*3 t3
      character*2 t2m
      logical FirstTime
      data Spg/'P1','P-1',
     1         'P2','Pm','P2/m',
     2         'P222','Pmm2','Pmmm',
     3         'P4','P-4','P4/m','P422','P4mm','P-4m2','P4/mmm',
     4         'P3','P-3','P312','P321','R32','P3m1','P31m','R3m',
     5         'P-3m1','P-31m','R32',
     7         'P6','P-6','P6/m','P622','P6mm','P-6m2','P-62m','P6/mmm',
     8         'P23','Pm-3','P432','P-43m','Pm-3m'/
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      KPhase=1
      FirstTime=.true.
      if(.not.allocated(rm)) then
        n=1
        call AllocateSymm(1,1,1,NPhase)
      endif
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'pgroup-pom.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup-pom.dat'
      endif
      call OpenFile(46,Veta,'formatted','unknown')
      call SetRealArrayTo(ShSg(1,KPhase),6,0.)
      do i=1,39
        Grupa(KPhase)=SPG(i)
        Monoclinic(KPhase)=2
        call EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
        write(46,'(a7,1x,a,i4)') SPG(i)(2:8),SmbPGO(i),NSymm(KPhase)
        do j=1,NSymm(KPhase)
          call SmbOp(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),1,1.,t3,t2m,io,
     1               n,det)
          write(Veta,'(''('',2(i2,'',''),i2,'')'')') io
          call zhusti(Veta)
          write(46,'(9i3,5x,a2,1x,a)')(nint(RM6(k,j,1,KPhase)),k=1,9),
     1                                t3,Veta(:idel(Veta))
        enddo
      enddo
      go to 9999
9000  call FeChybne(-1.,-1.,'Konex souboru.',' ',SeriousError)
9999  return
      end
      subroutine EM50Magnetic
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*80 Veta
      character*7  At,AtTypeMagP
      character*5  MenuJ(10,5)
      integer RolMenuSelectedQuest,nCrwJType(0:5),nRolMenuJType(5),
     1        NMenuJ(5),IMenuJ(5)
      logical CrwLogicQuest,EqIgCase,MagneticAtom
      save nRolMenuAtType,nCrwJType,nButtOwn,nRolMenuLast,nCrwMagnetic,
     1     MagneticAtom,LastAtomOld,MenuJ,NMenuJ,nLblNejde,
     2     nCrwActivateMagnetic,nRolMenuJType
      entry EM50MagneticMake(id)
      MenuJ(1,1)='Prvni'
      MenuJ(2,1)='Druhy'
      NMenuJ(1)=2
      Veta=' '
      call FeQuestLblMake(id,xdqp*.5,6,' ','C','B')
      nLblNejde=LblLastMade
      call FeQuestLblOff(nLblNejde)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+10.
      tpoms=tpom
      Veta='Activate the ma%gnetic option'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwActivateMagnetic=CrwLastMade
      il=il+1
      Veta='%Atom type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      tpom=xpom+dpom+30.
      Veta='%Use as a magnetic atom'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,.true.)
      call FeQuestCrwDisable(CrwLastMade)
      nCrwMagnetic=CrwLastMade
      Veta='%Own formfactors'
      xpom=tpom
      tpom=xpom+CrwXd+10.
      xpomp=tpom+FeTxLengthUnder(Veta)+100.
      dpom=80.
      do i=0,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                      1)
        if(i.eq.0) then
          Veta='%Edit'
          call FeQuestButtonMake(id,xpomp,il,dpom,ButYd,Veta)
          nButtOwn=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonDisable(ButtonLastMade)
        else
          call FeQuestRolMenuMake(id,tpom,il,xpomp,il,'  ','L',dpom,
     1                            EdwYd,1)
          call FeQuestRolMenuOpen(RolMenuLastMade,MenuJ(1,1),NMenuJ(1),
     1                            1)
          call FeQuestRolMenuDisable(RolMenuLastMade)
          nRolMenuJType(i)=RolMenuLastMade
        endif
        if(i.lt.4) then
          write(Veta,'(''Magnetic formfactor <j%'',i1,''>'')') 2*i
        else if(i.eq.4) then
          Veta='Magnetic formfactor <j0>%+c<j2>'
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.0)
        call FeQuestCrwDisable(CrwLastMade)
        nCrwJType(i)=CrwLastMade
      enddo
      LastAtomOld=-1
      LastAtom=1
      KartId=id
      entry EM50MagneticRefresh
      Klic=0
      if(.not.ExistNeutronData.and.
     1   (ExistM90.or.Radiation(1).ne.NeutronRadiation)) then
        Veta='The magnetic option is not applicable.'
      else if(NAtFormula(KPhase).le.0) then
        Veta='The chemical formula has not been yet defined.'
      else
        Veta=' '
      endif
      if(Veta.ne.' ') then
        call FeQuestLblChange(nLblNejde,Veta)
        do i=0,5
          il=il+1
          call FeQuestCrwClose(nCrwJType(i))
          if(i.gt.0) call FeQuestRolMenuClose(nRolMenuJType(i))
        enddo
        call FeQuestCrwClose(nCrwMagnetic)
        call FeQuestButtonClose(nButtOwn)
      else
        call FeQuestLblOff(nLblNejde)
        call FeQuestCrwOpen(nCrwActivateMagnetic,
     1                      MagneticType(KPhase).ne.0)
        if(NAtFormula(KPhase).gt.0) then
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
          if(LastAtom.gt.NAtFormula(KPhase)) LastAtom=1
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),LastAtom)
          MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
        else
          MagneticAtom=.false.
        endif
      endif
      go to 2000
      entry EM50MagneticCheck
      Klic=1
2000  if(.not.ExistNeutronData.and.
     1   (ExistM90.or.Radiation(1).ne.NeutronRadiation)) go to 9999
2100  if(CrwLogicQuest(nCrwActivateMagnetic)) then
        MagneticType(KPhase)=1
        if(.not.allocated(rmag)) then
          allocate(rmag(9,MaxNSymm,MaxNComp,NPhase),
     1             zmag(MaxNSymm,MaxNComp,NPhase))
          do i=1,MaxNSymm
            ZMag(i,1,KPhase)=1.
            call CrlMakeRMag(i,1)
          enddo
        endif
      else
        MagneticType(KPhase)=0
      endif
      MaxMagenticType=0
      do i=1,NPhase
        MaxMagneticType=max(MaxMagneticType,MagneticType(i))
      enddo
2150  if(MagneticType(KPhase).ne.0) then
        if(LastAtom.ne.LastAtomOld) then
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),LastAtom)
          call FeQuestCrwOpen(nCrwMagnetic,MagneticAtom)
        endif
        if(MagneticAtom) then
          if(LastAtom.ne.LastAtomOld) then
            call EM50ReadMagneticFFLabels(AtType(LastAtom,KPhase),
     1        MenuJ,NMenuJ,ierr)
            if(AtTypeMag(LastAtom,KPhase).eq.' ') then
              if(NMenuJ(1).gt.0) then
                AtTypeMag(LastAtom,KPhase)=MenuJ(NMenuJ(1),1)
                AtTypeMagJ(LastAtom,KPhase)='j0'
                Veta='Magnetic_formfactor_<j0>'
                call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1                                 AtTypeMag(LastAtom,KPhase),Veta,
     2                                 FFMag(1,LastAtom,KPhase),ich)
              else
                AtTypeMag(LastAtom,KPhase)=AtType(LastAtom,KPhase)
                AtTypeMagJ(LastAtom,KPhase)='own'
                TypeFFMag(LastAtom,KPhase)=0
                call SetRealArrayTo(FFMag(1,LastAtom,KPhase),7,0.)
              endif
            endif
            if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'own')) then
              JAtP=0
            else if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'j0j2')) then
              JAtP=5
            else
              JAtP=1
              read(AtTypeMagJ(LastAtom,KPhase)(2:2),'(i1)',err=2200) i
              JAtP=i/2+1
            endif
2200        call FeQuestCrwOpen(nCrwJType(0),JAtP.eq.0)
            if(JAtP.eq.0) then
              call FeQuestButtonOpen(nButtOwn,ButtonOff)
              nRolMenuLast=0
            else
              call FeQuestButtonDisable(nButtOwn)
            endif
            do i=1,5
              if(NMenuJ(i).gt.0) then
                call FeQuestCrwOpen(nCrwJType(i),JAtP.eq.i)
              else
                call FeQuestCrwDisable(nCrwJType(i))
              endif
            enddo
            AtTypeMagP=AtTypeMag(LastAtom,KPhase)
            do i=1,5
              if(NMenuJ(i).gt.0) then
                IMenuJ(i)=
     1            max(LocateInStringArray(MenuJ(1,i),NMenuJ(i),
     2                                    AtTypeMagP,IgnoreCaseYes),1)
                if(i.eq.JAtP) then
                  call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                    NMenuJ(i),IMenuJ(i))
                  nRolMenuLast=nRolMenuJType(i)
                else
                  call FeQuestRolMenuDisable(nRolMenuJType(i))
                endif
              else
                IMenuJ(i)=0
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            enddo
          endif
        else
          call FeQuestButtonDisable(nButtOwn)
          call FeQuestCrwDisable(nCrwJType(0))
          do i=1,5
            call FeQuestCrwDisable(nCrwJType(i))
            call FeQuestRolMenuDisable(nRolMenuJType(i))
          enddo
        endif
        LastAtomOld=LastAtom
        if(Klic.eq.0) go to 9999
        if(CheckType.eq.EventCrw.and.
     1     CheckNumber.eq.nCrwActivateMagnetic) then
          Klic=0
          LastAtomOld=-1
          go to 2100
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.ge.nCrwJType(0).and.
     2           CheckNumber.le.nCrwJType(5))) then
          if(CheckNumber.eq.nCrwJType(0)) then
            call FeQuestButtonOff(nButtOwn)
            do i=1,5
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            enddo
            RolMenuLast=0
            AtTypeMag(LastAtom,KPhase)=AtTypeMag(LastAtom,KPhase)
            AtTypeMagJ(LastAtom,KPhase)='own'
            call EM50ReadOwnFormFactorMag(LastAtom)
          else
            call FeQuestButtonDisable(nButtOwn)
            if(nRolMenuLast.gt.0)
     1        IMenuJ(nRolMenuLast-nRolMenuJType(1)+1)=
     2          RolMenuSelectedQuest(nRolMenuLast)
            do i=1,5
              if(CheckNumber.eq.nCrwJType(i)) then
                call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                  NMenuJ(i),IMenuJ(i))
                nRolMenuLast=nRolMenuJType(i)
                AtTypeMag(LastAtom,KPhase)=MenuJ(IMenuJ(i),i)
                if(i.eq.5) then
                  AtTypeMagJ(LastAtom,KPhase)='j0j2'
                  Veta='Magnetic_formfactor_<j0>+c<j2>'
                else
                  write(Cislo,'(i1)') 2*(i-1)
                  AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
                  Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
                endif
              else
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            enddo
            call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1        AtTypeMag(LastAtom,KPhase),Veta,
     2        FFMag(1,LastAtom,KPhase),ich)
            Klic=0
            go to 2150
          endif
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
          if(LastAtomOld.ne.LastAtom) then
            MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
            Klic=0
            go to 2150
          endif
        else if(CheckType.eq.EventRolMenu.and.
     1          (CheckNumber.ge.nRolMenuJType(1).and.
     2           CheckNumber.le.nRolMenuJType(5))) then
          nRolMenuLast=CheckNumber
          i=nRolMenuLast-nRolMenuJType(1)+1
          AtTypeMag(LastAtom,KPhase)=
     1      MenuJ(RolMenuSelectedQuest(nRolMenuLast),i)
          if(i.eq.5) then
            AtTypeMagJ(LastAtom,KPhase)='j0j2'
            Veta='Magnetic_formfactor_<j0>+c<j2>'
          else
            write(Cislo,'(i1)') 2*(i-1)
            AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
            Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
          endif
          call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1      AtTypeMag(LastAtom,KPhase),Veta,
     2      FFMag(1,LastAtom,KPhase),ich)
          Klic=0
          go to 2150
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMagnetic)
     1    then
          MagneticAtom=CrwLogicQuest(nCrwMagnetic)
          if(.not.MagneticAtom) then
            AtTypeMag(LastAtom,KPhase)=' '
            AtTypeMagJ(LastAtom,KPhase)=' '
          endif
          LastAtomOld=-1
          Klic=0
          go to 2150
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOwn)
     1    then
          call EM50ReadOwnFormFactorMag(LastAtom)
        endif
      else
        call FeQuestCrwClose(nCrwMagnetic)
        call FeQuestRolMenuClose(nRolMenuAtType)
        call FeQuestCrwClose(nCrwJType(0))
        call FeQuestButtonClose(nButtOwn)
        do i=1,5
          call FeQuestRolMenuClose(nRolMenuJType(i))
          call FeQuestCrwClose(nCrwJType(i))
        enddo
      endif
      go to 9999
9999  return
      end
      subroutine EM50ReadMagneticFFLabels(AtomTypeToRead,Ionts,NIonts,
     1                                    ierr)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) AtomTypeToRead,Ionts(10,5)
      character*256 t256
      character*80  t80,Label
      character*40 :: LabelSearch(5) = (/
     1                'Magnetic_formfactor_<j0>      ',
     2                'Magnetic_formfactor_<j2>      ',
     3                'Magnetic_formfactor_<j4>      ',
     4                'Magnetic_formfactor_<j6>      ',
     5                'Magnetic_formfactor_<j0>+c<j2>'/)
      character*7 At
      integer NIonts(5)
      logical EqIgCase
      equivalence (t80,t256)
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//'atoms.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/data/atoms.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9200
      read(ln,FormA) t256
      if(t256(1:1).ne.'#') rewind ln
      ierr=0
      NIonts=0
      Ionts=' '
1000  read(ln,'(a7)',end=9000,err=9100) At
      if(EqIgCase(At,AtomTypeToRead)) then
2000    read(ln,FormA,end=9000,err=9100) t256
2100    if(LocateSubstring(t256,'Electron_form_factor',.false.,.true.)
     1     .gt.0.or.EqIgCase(t256,'end')) go to 9999
        k=0
        call kus(t256,k,Label)
        m=LocateInStringArray(LabelSearch,5,Label(:idel(Label)),
     1                        IgnoreCaseYes)
        if(m.gt.0) then
          NIonts(m)=0
2500      read(ln,FormA,end=9050,err=9100) t256
          if(Index(t256,'_').gt.0) go to 2100
          k=0
          NIonts(m)=NIonts(m)+1
          call kus(t256,k,Ionts(NIonts(m),m))
          go to 2500
        else
          go to 2000
        endif
      else
3000    read(ln,FormA,end=9000,err=9100) t256
        if(.not.EqIgCase(t256,'end')) go to 3000
      endif
      go to 1000
9000  iErr=1
      go to 9999
9050  iErr=2
      go to 9999
9100  call FeReadError(ln)
      iErr=3
      go to 9999
9200  iErr=4
9999  call CloseIfOpened(ln)
      return
      end
      subroutine TestGrup
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ShSgIn(3),ShSgD(3),QuSymm(3,3),QuOld(3)
      character*256 Veta,Radka
      character*8 GrupaO,GrupaN
      logical Poprve
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      KPhase=1
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      if(.not.allocated(rm)) call AllocateSymm(1,1,1,NPhase)
      TimeStart=FeEtime()
      call OpenFile(46,'grtest.txt','formatted','unknown')
      n=71
1200  n=n+1
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      do i=1,n
        read(45,FormA256,end=5000) Radka
        read(Radka,FormSG) ipg,j,j,Grupa(KPhase)
      enddo
      close(45)
      Monoclinic(KPhase)=mod(n,3)+1
      GrupaO=Grupa(KPhase)
      write(46,'(''Grupa : '',a)') GrupaO(:idel(GrupaO))
      write(46,'(80a1)')('=',i=1,8+idel(GrupaO))
      nn=7
      fnn=nn+1
      Poprve=.true.
      do i1=-nn,nn
        ShSgIn(1)=i1/fnn
        do i2=-nn,nn
          ShSgIn(2)=i2/fnn
          do i3=-nn,nn
            ShSgIn(3)=i3/fnn
            call CopyVek(ShSgIn,ShSg(1,KPhase),3)
            call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,ich)
            if(NDim(KPhase).eq.4) then
              call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
              call CopyVek(QuSymm,Qu(1,1,1,KPhase),3)
            endif
            call FindSmbSg(GrupaN,ChangeOrderNo,1)
            if(NDim(KPhase).eq.4)
     1        call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
            if(Poprve) then
              if(GrupaO.ne.GrupaN) then
                write(46,'(''Grupa nactena  : '',a)')
     1            GrupaO(:idel(GrupaO))
                write(46,'(''Grupa zjistena : '',a)')
     1            GrupaN(:idel(GrupaN))
              endif
            endif
            Poprve=.false.
            pom=0.
            do i=1,3
              ShSgD(i)=ShSg(i,KPhase)-ShSgIn(i)
              pom=pom+abs(ShSgD(i))
            enddo
            if(pom.gt..0001) then
              call CopyVek(ShSgD,ShSg(1,KPhase),3)
              call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,
     1                        ich)
              if(NDim(KPhase).eq.4) then
                call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
                call CopyVek(QuSymm,Qu(1,1,1,KPhase),3*NDimI(KPhase))
              endif
              call FindSmbSg(GrupaN,ChangeOrderNo,1)
              if(NDim(KPhase).eq.4)
     1          call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
              pom=0.
              do i=1,3
                pom=pom+abs(ShSg(i,KPhase))
              enddo
              if(pom.gt..0001) then
                write(46,'('' Nesouhlas : '',3f8.3,'' <=>'',3f8.3)')
     1            ShSgIn,(ShSgIn(i)+ShSgD(i),i=1,3)
              endif
            endif
          enddo
        enddo
      enddo
      go to 1200
5000  write(46,'(''Cas: '',f8.2)') FeEtime()-TimeStart
      close(46)
      return
      end
      subroutine TestGrupOrlov
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension QuSymm(3,3)
      character*256 Veta
      character*80  GruFix(775),GruFixOld(775),t80
      character*5   smbq(3)
      data smbq/'alfa','beta','gamma'/
      logical EqIgCase
      GruFix(1)='P1(abg)'
      GruFix(2)='P-1(abg)'
      GruFix(3)='P2(ab0)'
      GruFix(4)='P2(ab1/2)'
      GruFix(5)='P2(00g)'
      GruFix(6)='P2(00g)s'
      GruFix(7)='P2(1/20g)'
      GruFix(8)='P21(ab0)'
      GruFix(9)='P21(00g)'
      GruFix(10)='P21(1/20g)'
      GruFix(11)='B2(ab0)'
      GruFix(12)='B2(00g)'
      GruFix(13)='B2(00g)s'
      GruFix(14)='B2(01/2g)'
      GruFix(15)='Pm(ab0)'
      GruFix(16)='Pm(ab0)s'
      GruFix(17)='Pm(ab1/2)'
      GruFix(18)='Pm(00g)'
      GruFix(19)='Pm(1/20g)'
      GruFix(20)='Pb(ab0)'
      GruFix(21)='Pb(ab1/2)'
      GruFix(22)='Pb(00g)'
      GruFix(23)='Pb(1/20g)'
      GruFix(24)='Bm(ab0)'
      GruFix(25)='Bm(ab0)s'
      GruFix(26)='Bm(00g)'
      GruFix(27)='Bm(01/2g)'
      GruFix(28)='Bb(ab0)'
      GruFix(29)='Bb(00g)'
      GruFix(30)='P2/m(ab0)'
      GruFix(31)='P2/m(ab0)0s'
      GruFix(32)='P2/m(ab1/2)'
      GruFix(33)='P2/m(00g)'
      GruFix(34)='P2/m(00g)s0'
      GruFix(35)='P2/m(1/20g)'
      GruFix(36)='P21/m(ab0)'
      GruFix(37)='P21/m(ab0)0s'
      GruFix(38)='P21/m(00g)'
      GruFix(39)='P21/m(1/20g)'
      GruFix(40)='B2/m(ab0)'
      GruFix(41)='B2/m(ab0)0s'
      GruFix(42)='B2/m(00g)'
      GruFix(43)='B2/m(00g)s0'
      GruFix(44)='B2/m(01/2g)'
      GruFix(45)='P2/b(ab0)'
      GruFix(46)='P2/b(ab1/2)'
      GruFix(47)='P2/b(00g)'
      GruFix(48)='P2/b(00g)s0'
      GruFix(49)='P2/b(1/20g)'
      GruFix(50)='P21/b(ab0)'
      GruFix(51)='P21/b(00g)'
      GruFix(52)='P21/b(1/20g)'
      GruFix(53)='B2/b(ab0)'
      GruFix(54)='B2/b(00g)'
      GruFix(55)='B2/b(00g)s0'
      GruFix(56)='P222(00g)'
      GruFix(57)='P222(00g)00s'
      GruFix(58)='P222(01/2g)'
      GruFix(59)='P222(1/21/2g)'
      GruFix(60)='P2221(00g)'
      GruFix(61)='P2221(01/2g)'
      GruFix(62)='P2221(1/21/2g)'
      GruFix(63)='P2122(00g)'
      GruFix(64)='P2122(00g)00s'
      GruFix(65)='P2122(01/2g)'
      GruFix(66)='P21212(00g)'
      GruFix(67)='P21212(00g)00s'
      GruFix(68)='P21221(00g)'
      GruFix(69)='P21221(01/2g)'
      GruFix(70)='P212121(00g)'
      GruFix(71)='C2221(00g)'
      GruFix(72)='C2221(10g)'
      GruFix(73)='A2122(00g)'
      GruFix(74)='A2122(00g)00s'
      GruFix(75)='C222(00g)'
      GruFix(76)='C222(00g)00s'
      GruFix(77)='C222(10g)'
      GruFix(78)='C222(10g)00s'
      GruFix(79)='A222(00g)'
      GruFix(80)='A222(00g)00s'
      GruFix(81)='A222(1/20g)'
      GruFix(82)='F222(00g)'
      GruFix(83)='F222(00g)00s'
      GruFix(84)='F222(10g)'
      GruFix(85)='I222(00g)'
      GruFix(86)='I222(00g)00s'
      GruFix(87)='I212121(00g)'
      GruFix(88)='I212121(00g)00s'
      GruFix(89)='Pmm2(00g)'
      GruFix(90)='Pmm2(00g)s0s'
      GruFix(91)='Pmm2(00g)ss0'
      GruFix(92)='Pmm2(01/2g)'
      GruFix(93)='Pmm2(01/2g)s0s'
      GruFix(94)='Pmm2(1/21/2g)'
      GruFix(95)='Pm2m(01/2g)'
      GruFix(96)='Pm2m(01/2g)s00'
      GruFix(97)='P2mm(00g)'
      GruFix(98)='P2mm(00g)0s0'
      GruFix(99)='P2mm(01/2g)'
      GruFix(100)='P2mm(1/21/2g)'
      GruFix(101)='Pmc21(00g)'
      GruFix(102)='Pmc21(00g)s0s'
      GruFix(103)='Pmc21(01/2g)'
      GruFix(104)='Pmc21(01/2g)s0s'
      GruFix(105)='Pcm21(01/2g)'
      GruFix(106)='Pmc21(1/21/2g)'
      GruFix(107)='P21am(00g)'
      GruFix(108)='P21am(00g)0s0'
      GruFix(109)='P21mn(00g)'
      GruFix(110)='P21ma(00g)0s0'
      GruFix(111)='P21am(01/2g)'
      GruFix(112)='P21ma(01/2g)'
      GruFix(113)='Pcc2(00g)'
      GruFix(114)='Pcc2(00g)s0s'
      GruFix(115)='Pcc2(01/2g)'
      GruFix(116)='Pcc2(1/21/2g)'
      GruFix(117)='P2aa(00g)'
      GruFix(118)='P2aa(00g)0s0'
      GruFix(119)='P2aa(01/2g)'
      GruFix(120)='Pma2(00g)'
      GruFix(121)='Pma2(00g)s0s'
      GruFix(122)='Pma2(00g)ss0'
      GruFix(123)='Pma2(00g)0ss'
      GruFix(124)='Pma2(01/2g)'
      GruFix(125)='Pma2(01/2g)s0s'
      GruFix(126)='Pm2a(01/2g)'
      GruFix(127)='Pm2a(01/2g)s00'
      GruFix(128)='Pc2m(01/2g)'
      GruFix(129)='P2cm(00g)'
      GruFix(130)='P2mb(00g)'
      GruFix(131)='P2mb(00g)0s0'
      GruFix(132)='P2cm(01/2g)'
      GruFix(133)='P2cm(1/21/2g)'
      GruFix(134)='Pca21(00g)'
      GruFix(135)='Pca21(00g)0ss'
      GruFix(136)='Pca21(01/2g)'
      GruFix(137)='P21ca(00g)'
      GruFix(138)='P21ab(00g)'
      GruFix(139)='P21ab(00g)0s0'
      GruFix(140)='P21ca(01/2g)'
      GruFix(141)='Pcn2(00g)'
      GruFix(142)='Pcn2(00g)s0s'
      GruFix(143)='Pcn2(01/2g)'
      GruFix(144)='P2na(00g)'
      GruFix(145)='P2an(00g)'
      GruFix(146)='P2an(00g)0s0'
      GruFix(147)='P2na(01/2g)'
      GruFix(148)='P2an(1/21/2g)0q0'
      GruFix(149)='Pmn21(00g)'
      GruFix(150)='Pmn21(00g)s0s'
      GruFix(151)='Pmn21(01/2g)'
      GruFix(152)='Pmn21(01/2g)s0s'
      GruFix(153)='P21nm(00g)'
      GruFix(154)='P21mn(00g)'
      GruFix(155)='P21mn(00g)0s0'
      GruFix(156)='P21nm(01/2g)'
      GruFix(157)='Pba2(00g)'
      GruFix(158)='Pba2(00g)s0s'
      GruFix(159)='Pba2(00g)ss0'
      GruFix(160)='Pba2(1/21/2g)qq0'
      GruFix(161)='Pc2a(01/2g)'
      GruFix(162)='P2cb(00g)'
      GruFix(163)='Pbn21(00g)'
      GruFix(164)='Pbn21(00g)s0s'
      GruFix(165)='Pbn21(1/21/2g)qq0'
      GruFix(166)='P21nb(00g)'
      GruFix(167)='P21cn(00g)'
      GruFix(168)='Pnn2(00g)'
      GruFix(169)='Pnn2(00g)s0s'
      GruFix(170)='Pnn2(1/21/2g)qq0'
      GruFix(171)='P2nn(00g)'
      GruFix(172)='P2nn(1/21/2g)0q0'
      GruFix(173)='Cmm2(00g)'
      GruFix(174)='Cmm2(00g)s0s'
      GruFix(175)='Cmm2(00g)ss0'
      GruFix(176)='Cmm2(10g)'
      GruFix(177)='Cmm2(10g)s0s'
      GruFix(178)='Cmm2(10g)ss0'
      GruFix(179)='A2mm(00g)'
      GruFix(180)='A2mm(00g)0s0'
      GruFix(181)='A2mm(1/20g)'
      GruFix(182)='A2mm(1/20g)0s0'
      GruFix(183)='Cmc21(00g)'
      GruFix(184)='Cmc21(00g)s0s'
      GruFix(185)='Cmc21(10g)'
      GruFix(186)='Cmc21(10g)s0s'
      GruFix(187)='A21am(00g)'
      GruFix(188)='A21am(00g)0s0'
      GruFix(189)='A21ma(00g)'
      GruFix(190)='A21ma(00g)0s0'
      GruFix(191)='Ccc2(00g)'
      GruFix(192)='Ccc2(00g)s0s'
      GruFix(193)='Ccc2(10g)'
      GruFix(194)='Ccc2(10g)s0s'
      GruFix(195)='A2aa(00g)'
      GruFix(196)='A2aa(00g)0s0'
      GruFix(197)='C2mm(00g)'
      GruFix(198)='C2mm(00g)0s0'
      GruFix(199)='C2mm(10g)'
      GruFix(200)='C2mm(10g)0s0'
      GruFix(201)='Amm2(00g)'
      GruFix(202)='Amm2(00g)s0s'
      GruFix(203)='Amm2(00g)ss0'
      GruFix(204)='Amm2(00g)0ss'
      GruFix(205)='Amm2(1/20g)'
      GruFix(206)='Amm2(1/20g)0ss'
      GruFix(207)='Am2m(00g)'
      GruFix(208)='Am2m(00g)s00'
      GruFix(209)='Am2m(1/20g)'
      GruFix(210)='C2me(00g)'
      GruFix(211)='C2me(00g)0s0'
      GruFix(212)='C2me(10g)'
      GruFix(213)='C2me(10g)0s0'
      GruFix(214)='Aem2(00g)'
      GruFix(215)='Aem2(00g)s0s'
      GruFix(216)='Aem2(00g)ss0'
      GruFix(217)='Aem2(00g)0ss'
      GruFix(218)='Aem2(1/20g)'
      GruFix(219)='Aem2(1/20g)0ss'
      GruFix(220)='Ae2m(00g)'
      GruFix(221)='Ae2m(00g)s00'
      GruFix(222)='Ae2m(1/20g)'
      GruFix(223)='C2cm(00g)'
      GruFix(224)='C2cm(10g)'
      GruFix(225)='Ama2(00g)'
      GruFix(226)='Ama2(00g)s0s'
      GruFix(227)='Ama2(00g)ss0'
      GruFix(228)='Ama2(00g)0ss'
      GruFix(229)='Am2a(00g)'
      GruFix(230)='Am2a(00g)s00'
      GruFix(231)='C2ce(00g)'
      GruFix(232)='C2ce(10g)'
      GruFix(233)='Aea2(00g)'
      GruFix(234)='Aea2(00g)s0s'
      GruFix(235)='Aea2(00g)ss0'
      GruFix(236)='Aea2(00g)0ss'
      GruFix(237)='Ae2a(00g)'
      GruFix(238)='Ae2a(00g)s00'
      GruFix(239)='Fmm2(00g)'
      GruFix(240)='Fmm2(00g)s0s'
      GruFix(241)='Fmm2(00g)ss0'
      GruFix(242)='Fmm2(10g)'
      GruFix(243)='Fmm2(10g)s0s'
      GruFix(244)='Fmm2(10g)ss0'
      GruFix(245)='F2mm(00g)'
      GruFix(246)='F2mm(00g)0s0'
      GruFix(247)='F2mm(10g)'
      GruFix(248)='F2mm(10g)0s0'
      GruFix(249)='Fdd2(00g)'
      GruFix(250)='Fdd2(00g)s0s'
      GruFix(251)='F2dd(00g)'
      GruFix(252)='Imm2(00g)'
      GruFix(253)='Imm2(00g)s0s'
      GruFix(254)='Imm2(00g)ss0'
      GruFix(255)='I2mm(00g)'
      GruFix(256)='I2mm(00g)0s0'
      GruFix(257)='Iba2(00g)'
      GruFix(258)='Iba2(00g)s0s'
      GruFix(259)='Iba2(00g)ss0'
      GruFix(260)='I2cb(00g)'
      GruFix(261)='I2cb(00g)0s0'
      GruFix(262)='Ima2(00g)'
      GruFix(263)='Ima2(00g)s0s'
      GruFix(264)='Ima2(00g)ss0'
      GruFix(265)='Ima2(00g)0ss'
      GruFix(266)='I2mb(00g)'
      GruFix(267)='I2mb(00g)0s0'
      GruFix(268)='I2cm(00g)'
      GruFix(269)='I2cm(00g)0s0'
      GruFix(270)='Pmmm(00g)'
      GruFix(271)='Pmmm(00g)s00'
      GruFix(272)='Pmmm(00g)ss0'
      GruFix(273)='Pmmm(01/2g)'
      GruFix(274)='Pmmm(01/2g)s00'
      GruFix(275)='Pmmm(1/21/2g)'
      GruFix(276)='Pnnn(00g)'
      GruFix(277)='Pnnn(00g)s00'
      GruFix(278)='Pnnn(1/21/2g)qq0'
      GruFix(279)='Pccm(00g)'
      GruFix(280)='Pccm(00g)s00'
      GruFix(281)='Pmaa(00g)'
      GruFix(282)='Pmaa(00g)s00'
      GruFix(283)='Pmaa(00g)ss0'
      GruFix(284)='Pmaa(00g)0s0'
      GruFix(285)='Pccm(01/2g)'
      GruFix(286)='Pmaa(01/2g)'
      GruFix(287)='Pmaa(01/2g)s00'
      GruFix(288)='Pccm(1/21/2g)'
      GruFix(289)='Pban(00g)'
      GruFix(290)='Pban(00g)s00'
      GruFix(291)='Pban(00g)ss0'
      GruFix(292)='Pcna(00g)'
      GruFix(293)='Pcna(00g)s00'
      GruFix(294)='Pcna(01/2g)'
      GruFix(295)='Pban(1/21/2g)qq0'
      GruFix(296)='Pmma(00g)'
      GruFix(297)='Pmma(00g)s00'
      GruFix(298)='Pmma(00g)ss0'
      GruFix(299)='Pmma(00g)0s0'
      GruFix(300)='Pmam(00g)'
      GruFix(301)='Pmam(00g)s00'
      GruFix(302)='Pmam(00g)ss0'
      GruFix(303)='Pmam(00g)0s0'
      GruFix(304)='Pmcm(00g)'
      GruFix(305)='Pmcm(00g)s00'
      GruFix(306)='Pmma(01/2g)'
      GruFix(307)='Pmma(01/2g)s00'
      GruFix(308)='Pmam(01/2g)'
      GruFix(309)='Pmam(01/2g)s00'
      GruFix(310)='Pmcm(01/2g)'
      GruFix(311)='Pmcm(01/2g)s00'
      GruFix(312)='Pcmm(01/2g)'
      GruFix(313)='Pcmm(1/21/2g)'
      GruFix(314)='Pnna(00g)'
      GruFix(315)='Pnna(00g)s00'
      GruFix(316)='Pbnn(00g)'
      GruFix(317)='Pbnn(00g)s00'
      GruFix(318)='Pcnn(00g)'
      GruFix(319)='Pcnn(00g)s00'
      GruFix(320)='Pbnn(1/21/2g)qq0'
      GruFix(321)='Pmna(00g)'
      GruFix(322)='Pmna(00g)s00'
      GruFix(323)='Pcnm(00g)'
      GruFix(324)='Pcnm(00g)s00'
      GruFix(325)='Pbmn(00g)'
      GruFix(326)='Pbmn(00g)s00'
      GruFix(327)='Pbmn(00g)ss0'
      GruFix(328)='Pbmn(00g)0s0'
      GruFix(329)='Pmna(01/2g)'
      GruFix(330)='Pmna(01/2g)s00'
      GruFix(331)='Pcnm(01/2g)'
      GruFix(332)='Pcca(00g)'
      GruFix(333)='Pcca(00g)s00'
      GruFix(334)='Pcaa(00g)'
      GruFix(335)='Pcaa(00g)0s0'
      GruFix(336)='Pbab(00g)'
      GruFix(337)='Pbab(00g)s00'
      GruFix(338)='Pbab(00g)ss0'
      GruFix(339)='Pbab(00g)0s0'
      GruFix(340)='Pcca(01/2g)'
      GruFix(341)='Pcaa(01/2g)'
      GruFix(342)='Pbam(00g)'
      GruFix(343)='Pbam(00g)s00'
      GruFix(344)='Pbam(00g)ss0'
      GruFix(345)='Pcma(00g)'
      GruFix(346)='Pcma(00g)0s0'
      GruFix(347)='Pcma(01/2g)'
      GruFix(348)='Pccn(00g)'
      GruFix(349)='Pccn(00g)s00'
      GruFix(350)='Pbnb(00g)'
      GruFix(351)='Pbnb(00g)s00'
      GruFix(352)='Pcam(00g)'
      GruFix(353)='Pcam(00g)0s0'
      GruFix(354)='Pmca(00g)'
      GruFix(355)='Pmca(00g)s00'
      GruFix(356)='Pbma(00g)'
      GruFix(357)='Pbma(00g)s00'
      GruFix(358)='Pbma(00g)ss0'
      GruFix(359)='Pbma(00g)0s0'
      GruFix(360)='Pcam(01/2g)'
      GruFix(361)='Pmca(01/2g)'
      GruFix(362)='Pmca(01/2g)s00'
      GruFix(363)='Pnnm(00g)'
      GruFix(364)='Pnnm(00g)s00'
      GruFix(365)='Pmnn(00g)'
      GruFix(366)='Pmnn(00g)s00'
      GruFix(367)='Pmmn(00g)'
      GruFix(368)='Pmmn(00g)s00'
      GruFix(369)='Pmmn(00g)ss0'
      GruFix(370)='Pmnm(00g)'
      GruFix(371)='Pmnm(00g)s00'
      GruFix(372)='Pmnm(01/2g)'
      GruFix(373)='Pmnm(01/2g)s00'
      GruFix(374)='Pbcn(00g)'
      GruFix(375)='Pbcn(00g)s00'
      GruFix(376)='Pnca(00g)'
      GruFix(377)='Pnca(00g)s00'
      GruFix(378)='Pbna(00g)'
      GruFix(379)='Pbna(00g)s00'
      GruFix(380)='Pbca(00g)'
      GruFix(381)='Pbca(00g)s00'
      GruFix(382)='Pnma(00g)'
      GruFix(383)='Pnma(00g)0s0'
      GruFix(384)='Pbnm(00g)'
      GruFix(385)='Pbnm(00g)s00'
      GruFix(386)='Pmcn(00g)'
      GruFix(387)='Pmcn(00g)s00'
      GruFix(388)='Cmcm(00g)'
      GruFix(389)='Cmcm(00g)s00'
      GruFix(390)='Cmcm(10g)'
      GruFix(391)='Cmcm(10g)s00'
      GruFix(392)='Amam(00g)'
      GruFix(393)='Amam(00g)s00'
      GruFix(394)='Amam(00g)ss0'
      GruFix(395)='Amam(00g)0s0'
      GruFix(396)='Amma(00g)'
      GruFix(397)='Amma(00g)s00'
      GruFix(398)='Amma(00g)ss0'
      GruFix(399)='Amma(00g)0s0'
      GruFix(400)='Cmce(00g)'
      GruFix(401)='Cmce(00g)s00'
      GruFix(402)='Cmce(10g)'
      GruFix(403)='Cmce(10g)s00'
      GruFix(404)='Aema(00g)'
      GruFix(405)='Aema(00g)s00'
      GruFix(406)='Aema(00g)ss0'
      GruFix(407)='Aema(00g)0s0'
      GruFix(408)='Aeam(00g)'
      GruFix(409)='Aeam(00g)s00'
      GruFix(410)='Aeam(00g)ss0'
      GruFix(411)='Aeam(00g)0s0'
      GruFix(412)='Cmmm(00g)'
      GruFix(413)='Cmmm(00g)s00'
      GruFix(414)='Cmmm(00g)ss0'
      GruFix(415)='Cmmm(10g)'
      GruFix(416)='Cmmm(10g)s00'
      GruFix(417)='Cmmm(10g)ss0'
      GruFix(418)='Ammm(00g)'
      GruFix(419)='Ammm(00g)s00'
      GruFix(420)='Ammm(00g)ss0'
      GruFix(421)='Ammm(00g)0s0'
      GruFix(422)='Ammm(1/20g)'
      GruFix(423)='Ammm(1/20g)0s0'
      GruFix(424)='Cccm(00g)'
      GruFix(425)='Cccm(00g)s00'
      GruFix(426)='Cccm(10g)'
      GruFix(427)='Cccm(10g)s00'
      GruFix(428)='Amaa(00g)'
      GruFix(429)='Amaa(00g)s00'
      GruFix(430)='Amaa(00g)ss0'
      GruFix(431)='Amaa(00g)0s0'
      GruFix(432)='Cmme(00g)'
      GruFix(433)='Cmme(00g)s00'
      GruFix(434)='Cmme(00g)ss0'
      GruFix(435)='Cmme(10g)'
      GruFix(436)='Cmme(10g)s00'
      GruFix(437)='Cmme(10g)ss0'
      GruFix(438)='Aemm(00g)'
      GruFix(439)='Aemm(00g)s00'
      GruFix(440)='Aemm(00g)ss0'
      GruFix(441)='Aemm(00g)0s0'
      GruFix(442)='Aemm(1/20g)'
      GruFix(443)='Aemm(1/20g)0s0'
      GruFix(444)='Ccce(00g)'
      GruFix(445)='Ccce(00g)s00'
      GruFix(446)='Ccce(10g)'
      GruFix(447)='Ccce(10g)s00'
      GruFix(448)='Aeaa(00g)'
      GruFix(449)='Aeaa(00g)s00'
      GruFix(450)='Aeaa(00g)ss0'
      GruFix(451)='Aeaa(00g)0s0'
      GruFix(452)='Fmmm(00g)'
      GruFix(453)='Fmmm(00g)s00'
      GruFix(454)='Fmmm(00g)ss0'
      GruFix(455)='Fmmm(10g)'
      GruFix(456)='Fmmm(10g)s00'
      GruFix(457)='Fmmm(10g)ss0'
      GruFix(458)='Fddd(00g)'
      GruFix(459)='Fddd(00g)s00'
      GruFix(460)='Immm(00g)'
      GruFix(461)='Immm(00g)s00'
      GruFix(462)='Immm(00g)ss0'
      GruFix(463)='Ibam(00g)'
      GruFix(464)='Ibam(00g)s00'
      GruFix(465)='Ibam(00g)ss0'
      GruFix(466)='Imcb(00g)'
      GruFix(467)='Imcb(00g)s00'
      GruFix(468)='Imcb(00g)ss0'
      GruFix(469)='Imcb(00g)0s0'
      GruFix(470)='Ibca(00g)'
      GruFix(471)='Ibca(00g)s00'
      GruFix(472)='Ibca(00g)ss0'
      GruFix(473)='Imma(00g)'
      GruFix(474)='Imma(00g)s00'
      GruFix(475)='Imma(00g)ss0'
      GruFix(476)='Ibmm(00g)'
      GruFix(477)='Ibmm(00g)s00'
      GruFix(478)='Ibmm(00g)ss0'
      GruFix(479)='Ibmm(00g)0s0'
      GruFix(480)='P4(00g)'
      GruFix(481)='P4(00g)q'
      GruFix(482)='P4(00g)s'
      GruFix(483)='P4(1/21/2g)'
      GruFix(484)='P4(1/21/2g)q'
      GruFix(485)='P41(00g)'
      GruFix(486)='P41(1/21/2g)'
      GruFix(487)='P42(00g)'
      GruFix(488)='P42(00g)q'
      GruFix(489)='P42(1/21/2g)'
      GruFix(490)='P42(1/21/2g)q'
      GruFix(491)='P43(00g)'
      GruFix(492)='P43(1/21/2g)q'
      GruFix(493)='I4(00g)'
      GruFix(494)='I4(00g)q'
      GruFix(495)='I4(00g)s'
      GruFix(496)='I41(00g)'
      GruFix(497)='I41(00g)q'
      GruFix(498)='P4(00g)'
      GruFix(499)='P-4(1/21/2g)'
      GruFix(500)='I-4(00g)'
      GruFix(501)='P4/m(00g)'
      GruFix(502)='P4/m(00g)s0'
      GruFix(503)='P4/m(1/21/2g)'
      GruFix(504)='P42/m(00g)'
      GruFix(505)='P42/m(1/21/2g)'
      GruFix(506)='P4/n(00g)'
      GruFix(507)='P4/n(00g)s0'
      GruFix(508)='P4/n(1/21/2g)q0'
      GruFix(509)='P42/n(00g)'
      GruFix(510)='P42/n(1/21/2g)q0'
      GruFix(511)='I4/m(00g)'
      GruFix(512)='I4/m(00g)s0'
      GruFix(513)='I41/a(00g)'
      GruFix(514)='P422(00g)'
      GruFix(515)='P422(00g)q00'
      GruFix(516)='P422(00g)s00'
      GruFix(517)='P422(1/21/2g)'
      GruFix(518)='P422(1/21/2g)q00'
      GruFix(519)='P4212(00g)'
      GruFix(520)='P4212(00g)q00'
      GruFix(521)='P4212(00g)s00'
      GruFix(522)='P4122(00g)'
      GruFix(523)='P4122(1/21/2g)'
      GruFix(524)='P41212(00g)'
      GruFix(525)='P4222(00g)'
      GruFix(526)='P4222(00g)q00'
      GruFix(527)='P4222(1/21/2g)'
      GruFix(528)='P4222(1/21/2g)q00'
      GruFix(529)='P42212(00g)'
      GruFix(530)='P42212(00g)q00'
      GruFix(531)='P4322(00g)'
      GruFix(532)='P4322(1/21/2g)'
      GruFix(533)='P43212(00g)'
      GruFix(534)='I422(00g)'
      GruFix(535)='I422(00g)q00'
      GruFix(536)='I422(00g)s00'
      GruFix(537)='I4122(00g)'
      GruFix(538)='I4122(00g)q00'
      GruFix(539)='P4mm(00g)'
      GruFix(540)='P4mm(00g)ss0'
      GruFix(541)='P4mm(00g)0ss'
      GruFix(542)='P4mm(00g)s0s'
      GruFix(543)='P4mm(1/21/2g)'
      GruFix(544)='P4mm(1/21/2g)0ss'
      GruFix(545)='P4bm(00g)'
      GruFix(546)='P4bm(00g)ss0'
      GruFix(547)='P4bm(00g)0ss'
      GruFix(548)='P4bm(00g)s0s'
      GruFix(549)='P4bm(1/21/2g)qq0'
      GruFix(550)='P4bm(1/21/2g)qqs'
      GruFix(551)='P42cm(00g)'
      GruFix(552)='P42cm(00g)0ss'
      GruFix(553)='P42cm(1/21/2g)'
      GruFix(554)='P42cm(1/21/2g)0ss'
      GruFix(555)='P42nm(00g)'
      GruFix(556)='P42nm(00g)0ss'
      GruFix(557)='P42nm(1/21/2g)qq0'
      GruFix(558)='P42nm(1/21/2g)qqs'
      GruFix(559)='P4cc(00g)'
      GruFix(560)='P4cc(00g)ss0'
      GruFix(561)='P4cc(1/21/2g)'
      GruFix(562)='P4nc(00g)'
      GruFix(563)='P4nc(00g)ss0'
      GruFix(564)='P4nc(1/21/2g)qq0'
      GruFix(565)='P42mc(00g)'
      GruFix(566)='P42mc(00g)ss0'
      GruFix(567)='P42mc(1/21/2g)'
      GruFix(568)='P42bc(00g)'
      GruFix(569)='P42bc(00g)ss0'
      GruFix(570)='P42bc(1/21/2g)qq0'
      GruFix(571)='I4mm(00g)'
      GruFix(572)='I4mm(00g)ss0'
      GruFix(573)='I4mm(00g)0ss'
      GruFix(574)='I4mm(00g)s0s'
      GruFix(575)='I4cm(00g)'
      GruFix(576)='I4cm(00g)ss0'
      GruFix(577)='I4cm(00g)0ss'
      GruFix(578)='I4cm(00g)s0s'
      GruFix(579)='I41md(00g)'
      GruFix(580)='I41md(00g)ss0'
      GruFix(581)='I41cd(00g)'
      GruFix(582)='I41cd(00g)ss0'
      GruFix(583)='P-42m(00g)'
      GruFix(584)='P-42m(00g)00s'
      GruFix(585)='P-42m(1/21/2g)'
      GruFix(586)='P-42m(1/21/2g)00s'
      GruFix(587)='P-42c(00g)'
      GruFix(588)='P-42c(1/21/2g)'
      GruFix(589)='P-421m(00g)'
      GruFix(590)='P-421m(00g)00s'
      GruFix(591)='P-421c(00g)'
      GruFix(592)='P-4m2(00g)'
      GruFix(593)='P-4m2(00g)0s0'
      GruFix(594)='P-4m2(1/21/2g)'
      GruFix(595)='P-4c2(00g)'
      GruFix(596)='P-4c2(1/21/2g)'
      GruFix(597)='P-4b2(00g)'
      GruFix(598)='P-4b2(00g)0s0'
      GruFix(599)='P-4b2(1/21/2g)0q0'
      GruFix(600)='P-4n2(00g)'
      GruFix(601)='P-4n2(1/21/2g)0q0'
      GruFix(602)='I-4m2(00g)'
      GruFix(603)='I-4m2(00g)0s0'
      GruFix(604)='I-4c2(00g)'
      GruFix(605)='I-4c2(00g)0s0'
      GruFix(606)='I-42m(00g)'
      GruFix(607)='I-42m(00g)00s'
      GruFix(608)='I-42d(00g)'
      GruFix(609)='P4/mmm(00g)'
      GruFix(610)='P4/mmm(00g)s0s0'
      GruFix(611)='P4/mmm(00g)00ss'
      GruFix(612)='P4/mmm(00g)s00s'
      GruFix(613)='P4/mmm(1/21/2g)'
      GruFix(614)='P4/mmm(1/21/2g)00ss'
      GruFix(615)='P4/mcc(00g)'
      GruFix(616)='P4/mcc(00g)s0s0'
      GruFix(617)='P4/mcc(1/21/2g)'
      GruFix(618)='P4/nbm(00g)'
      GruFix(619)='P4/nbm(00g)s0s0'
      GruFix(620)='P4/nbm(00g)00ss'
      GruFix(621)='P4/nbm(00g)s00s'
      GruFix(622)='P4/nbm(1/21/2g)q0q0'
      GruFix(623)='P4/nbm(1/21/2g)q0qs'
      GruFix(624)='P4/nnc(00g)'
      GruFix(625)='P4/nnc(00g)s0s0'
      GruFix(626)='P4/nnc(1/21/2g)q0q0'
      GruFix(627)='P4/mbm(00g)'
      GruFix(628)='P4/mbm(00g)s0s0'
      GruFix(629)='P4/mbm(00g)00ss'
      GruFix(630)='P4/mbm(00g)s00s'
      GruFix(631)='P4/mnc(00g)'
      GruFix(632)='P4/mnc(00g)s0s0'
      GruFix(633)='P4/nmm(00g)'
      GruFix(634)='P4/nmm(00g)s0s0'
      GruFix(635)='P4/nmm(00g)00ss'
      GruFix(636)='P4/nmm(00g)s00s'
      GruFix(637)='P4/ncc(00g)'
      GruFix(638)='P4/ncc(00g)s0s0'
      GruFix(639)='P42/mmc(00g)'
      GruFix(640)='P42/mmc(00g)s0s0'
      GruFix(641)='P42/mmc(1/21/2g)'
      GruFix(642)='P42/mcm(00g)'
      GruFix(643)='P42/mcm(00g)00ss'
      GruFix(644)='P42/mcm(1/21/2g)'
      GruFix(645)='P42/mcm(1/21/2g)00ss'
      GruFix(646)='P42/nbc(00g)'
      GruFix(647)='P42/nbc(00g)s0s0'
      GruFix(648)='P42/nbc(1/21/2g)q0q0'
      GruFix(649)='P42/nnm(00g)'
      GruFix(650)='P42/nnm(00g)00ss'
      GruFix(651)='P42/nnm(1/21/2g)q0q0'
      GruFix(652)='P42/nnm(1/21/2g)q0qs'
      GruFix(653)='P42/mbc(00g)'
      GruFix(654)='P42/mbc(00g)s0s0'
      GruFix(655)='P42/mnm(00g)'
      GruFix(656)='P42/mnm(00g)00ss'
      GruFix(657)='P42/nmc(00g)'
      GruFix(658)='P42/nmc(00g)s0s0'
      GruFix(659)='P42/ncm(00g)'
      GruFix(660)='P42/ncm(00g)00ss'
      GruFix(661)='I4/mmm(00g)'
      GruFix(662)='I4/mmm(00g)s0s0'
      GruFix(663)='I4/mmm(00g)00ss'
      GruFix(664)='I4/mmm(00g)s00s'
      GruFix(665)='I4/mcm(00g)'
      GruFix(666)='I4/mcm(00g)s0s0'
      GruFix(667)='I4/mcm(00g)00ss'
      GruFix(668)='I4/mcm(00g)s00s'
      GruFix(669)='I41/amd(00g)'
      GruFix(670)='I41/amd(00g)s0s0'
      GruFix(671)='I41/acd(00g)'
      GruFix(672)='I41/acd(00g)s0s0'
      GruFix(673)='P3(1/31/3g)'
      GruFix(674)='P3(00g)'
      GruFix(675)='P3(00g)t'
      GruFix(676)='P31(1/31/3g)'
      GruFix(677)='P31(00g)'
      GruFix(678)='P32(1/31/3g)'
      GruFix(679)='P32(00g)'
      GruFix(680)='R3(00g)'
      GruFix(681)='R3(00g)t'
      GruFix(682)='P-3(1/31/3g)'
      GruFix(683)='P-3(00g)'
      GruFix(684)='R-3(00g)'
      GruFix(685)='P312(1/31/3g)'
      GruFix(686)='P312(00g)'
      GruFix(687)='P312(00g)t00'
      GruFix(688)='P321(00g)'
      GruFix(689)='P321(00g)t00'
      GruFix(690)='P3112(1/31/3g)'
      GruFix(691)='P3112(00g)'
      GruFix(692)='P3121(00g)'
      GruFix(693)='P3212(1/31/3g)'
      GruFix(694)='P3212(00g)'
      GruFix(695)='P3221(00g)'
      GruFix(696)='R32(00g)'
      GruFix(697)='R32(00g)t0'
      GruFix(698)='P3m1(00g)'
      GruFix(699)='P3m1(00g)0s0'
      GruFix(700)='P31m(1/31/3g)'
      GruFix(701)='P31m(1/31/3g)00s'
      GruFix(702)='P31m(00g)'
      GruFix(703)='P31m(00g)00s'
      GruFix(704)='P3c1(00g)'
      GruFix(705)='P31c(1/31/3g)'
      GruFix(706)='P31c(00g)'
      GruFix(707)='R3m(00g)'
      GruFix(708)='R3m(00g)0s'
      GruFix(709)='R3c(00g)'
      GruFix(710)='P-31m(1/31/3g)'
      GruFix(711)='P-31m(1/31/3g)00s'
      GruFix(712)='P-31m(00g)'
      GruFix(713)='P-31m(00g)00s'
      GruFix(714)='P-31c(1/31/3g)'
      GruFix(715)='P-31c(00g)'
      GruFix(716)='P-3m1(00g)'
      GruFix(717)='P-3m1(00g)0s0'
      GruFix(718)='P-3c1(00g)'
      GruFix(719)='R-3m(00g)'
      GruFix(720)='R-3m(00g)0s'
      GruFix(721)='R-3c(00g)'
      GruFix(722)='P6(00g)'
      GruFix(723)='P6(00g)h'
      GruFix(724)='P6(00g)t'
      GruFix(725)='P6(00g)s'
      GruFix(726)='P61(00g)'
      GruFix(727)='P65(00g)'
      GruFix(728)='P62(00g)'
      GruFix(729)='P62(00g)h'
      GruFix(730)='P64(00g)'
      GruFix(731)='P64(00g)h'
      GruFix(732)='P63(00g)'
      GruFix(733)='P63(00g)h'
      GruFix(734)='P-6(00g)'
      GruFix(735)='P6/m(00g)'
      GruFix(736)='P6/m(00g)s0'
      GruFix(737)='P63/m(00g)'
      GruFix(738)='P622(00g)'
      GruFix(739)='P622(00g)h00'
      GruFix(740)='P622(00g)t00'
      GruFix(741)='P622(00g)s00'
      GruFix(742)='P6122(00g)'
      GruFix(743)='P6222(00g)'
      GruFix(744)='P6222(00g)'
      GruFix(745)='P6222(00g)h00'
      GruFix(746)='P6422(00g)'
      GruFix(747)='P6422(00g)h00'
      GruFix(748)='P6322(00g)'
      GruFix(749)='P6322(00g)h00'
      GruFix(750)='P6mm(00g)'
      GruFix(751)='P6mm(00g)ss0'
      GruFix(752)='P6mm(00g)0ss'
      GruFix(753)='P6mm(00g)s0s'
      GruFix(754)='P6cc(00g)'
      GruFix(755)='P6cc(00g)s0s'
      GruFix(756)='P63cm(00g)'
      GruFix(757)='P63cm(00g)0ss'
      GruFix(758)='P63mc(00g)'
      GruFix(759)='P63mc(00g)0ss'
      GruFix(760)='P-6m2(00g)'
      GruFix(761)='P-6m2(00g)0s0'
      GruFix(762)='P-6c2(00g)'
      GruFix(763)='P-62m(00g)'
      GruFix(764)='P-62m(00g)00s'
      GruFix(765)='P-62c(00g)'
      GruFix(766)='P6/mmm(00g)'
      GruFix(767)='P6/mmm(00g)s0s0'
      GruFix(768)='P6/mmm(00g)00ss'
      GruFix(769)='P6/mmm(00g)s00s'
      GruFix(770)='P6/mcc(00g)'
      GruFix(771)='P6/mcc(00g)s00s'
      GruFix(772)='P63/mcm(00g)'
      GruFix(773)='P63/mcm(00g)00ss'
      GruFix(774)='P63/mmc(00g)'
      GruFix(775)='P63/mmc(00g)00ss'
      GruFixOld(1)='P1(abg)'
      GruFixOld(2)='P-1(abg)'
      GruFixOld(3)='P2(ab0)'
      GruFixOld(4)='P2(ab1/2)'
      GruFixOld(5)='P2(00g)'
      GruFixOld(6)='P2(00g)s'
      GruFixOld(7)='P2(1/20g)'
      GruFixOld(8)='P21(ab0)'
      GruFixOld(9)='P21(00g)'
      GruFixOld(10)='P21(1/20g)'
      GruFixOld(11)='B2(ab0)'
      GruFixOld(12)='B2(00g)'
      GruFixOld(13)='B2(00g)s'
      GruFixOld(14)='B2(01/2g)'
      GruFixOld(15)='Pm(ab0)'
      GruFixOld(16)='Pm(ab0)s'
      GruFixOld(17)='Pm(ab1/2)'
      GruFixOld(18)='Pm(00g)'
      GruFixOld(19)='Pm(1/20g)'
      GruFixOld(20)='Pb(ab0)'
      GruFixOld(21)='Pb(ab1/2)'
      GruFixOld(22)='Pb(00g)'
      GruFixOld(23)='Pb(1/20g)'
      GruFixOld(24)='Bm(ab0)'
      GruFixOld(25)='Bm(ab0)s'
      GruFixOld(26)='Bm(00g)'
      GruFixOld(27)='Bm(01/2g)'
      GruFixOld(28)='Bb(ab0)'
      GruFixOld(29)='Bb(00g)'
      GruFixOld(30)='P2/m(ab0)'
      GruFixOld(31)='P2/m(ab0)0s'
      GruFixOld(32)='P2/m(ab1/2)'
      GruFixOld(33)='P2/m(00g)'
      GruFixOld(34)='P2/m(00g)s0'
      GruFixOld(35)='P2/m(1/20g)'
      GruFixOld(36)='P21/m(ab0)'
      GruFixOld(37)='P21/m(ab0)0s'
      GruFixOld(38)='P21/m(00g)'
      GruFixOld(39)='P21/m(1/20g)'
      GruFixOld(40)='B2/m(ab0)'
      GruFixOld(41)='B2/m(ab0)0s'
      GruFixOld(42)='B2/m(00g)'
      GruFixOld(43)='B2/m(00g)s0'
      GruFixOld(44)='B2/m(1/20g)'
      GruFixOld(45)='P2/b(ab0)'
      GruFixOld(46)='P2/b(ab1/2)'
      GruFixOld(47)='P2/b(00g)'
      GruFixOld(48)='P2/b(00g)s0'
      GruFixOld(49)='P2/b(1/20g)'
      GruFixOld(50)='P21/b(ab0)'
      GruFixOld(51)='P21/b(00g)'
      GruFixOld(52)='P21/b(1/20g)'
      GruFixOld(53)='B2/b(ab0)'
      GruFixOld(54)='B2/b(00g)'
      GruFixOld(55)='B2/b(00g)s0'
      GruFixOld(56)='P222(00g)'
      GruFixOld(57)='P222(00g)00s'
      GruFixOld(58)='P222(01/2g)'
      GruFixOld(59)='P222(1/21/2g)'
      GruFixOld(60)='P2221(00g)'
      GruFixOld(61)='P2221(01/2g)'
      GruFixOld(62)='P2221(1/21/2g)'
      GruFixOld(63)='P2122(00g)'
      GruFixOld(64)='P2122(00g)00s'
      GruFixOld(65)='P2122(01/2g)'
      GruFixOld(66)='P21212(00g)'
      GruFixOld(67)='P21212(00g)00s'
      GruFixOld(68)='P21221(00g)'
      GruFixOld(69)='P21221(01/2g)'
      GruFixOld(70)='P212121(00g)'
      GruFixOld(71)='C2221(00g)'
      GruFixOld(72)='C2221(10g)'
      GruFixOld(73)='A2122(00g)'
      GruFixOld(74)='A2122(00g)00s'
      GruFixOld(75)='C222(00g)'
      GruFixOld(76)='C222(00g)00s'
      GruFixOld(77)='C222(10g)'
      GruFixOld(78)='C222(10g)00s'
      GruFixOld(79)='A222(00g)'
      GruFixOld(80)='A222(00g)00s'
      GruFixOld(81)='A222(1/20g)'
      GruFixOld(82)='F222(00g)'
      GruFixOld(83)='F222(00g)00s'
      GruFixOld(84)='F222(10g)'
      GruFixOld(85)='I222(00g)'
      GruFixOld(86)='I222(00g)00s'
      GruFixOld(87)='I212121(00g)'
      GruFixOld(88)='I212121(00g)00s'
      GruFixOld(89)='Pmm2(00g)'
      GruFixOld(90)='Pmm2(00g)s0s'
      GruFixOld(91)='Pmm2(00g)ss0'
      GruFixOld(92)='Pmm2(01/2g)'
      GruFixOld(93)='Pmm2(01/2g)s0s'
      GruFixOld(94)='Pmm2(1/21/2g)'
      GruFixOld(95)='Pm2m(01/2g)'
      GruFixOld(96)='Pm2m(01/2g)s00'
      GruFixOld(97)='P2mm(00g)'
      GruFixOld(98)='P2mm(00g)0s0'
      GruFixOld(99)='P2mm(01/2g)'
      GruFixOld(100)='P2mm(1/21/2g)'
      GruFixOld(101)='Pmc21(00g)'
      GruFixOld(102)='Pmc21(00g)s0s'
      GruFixOld(103)='Pmc21(01/2g)'
      GruFixOld(104)='Pmc21(01/2g)s0s'
      GruFixOld(105)='Pcm21(01/2g)'
      GruFixOld(106)='Pmc21(1/21/2g)'
      GruFixOld(107)='P21am(00g)'
      GruFixOld(108)='P21am(00g)0s0'
      GruFixOld(109)='P21mn(00g)'
      GruFixOld(110)='P21ma(00g)0s0'
      GruFixOld(111)='P21am(01/2g)'
      GruFixOld(112)='P21ma(01/2g)'
      GruFixOld(113)='Pcc2(00g)'
      GruFixOld(114)='Pcc2(00g)s0s'
      GruFixOld(115)='Pcc2(01/2g)'
      GruFixOld(116)='Pcc2(1/21/2g)'
      GruFixOld(117)='P2aa(00g)'
      GruFixOld(118)='P2aa(00g)0s0'
      GruFixOld(119)='P2aa(01/2g)'
      GruFixOld(120)='Pma2(00g)'
      GruFixOld(121)='Pma2(00g)s0s'
      GruFixOld(122)='Pma2(00g)ss0'
      GruFixOld(123)='Pma2(00g)0ss'
      GruFixOld(124)='Pma2(01/2g)'
      GruFixOld(125)='Pma2(01/2g)s0s'
      GruFixOld(126)='Pm2a(01/2g)'
      GruFixOld(127)='Pm2a(01/2g)s00'
      GruFixOld(128)='Pc2m(01/2g)'
      GruFixOld(129)='P2cm(00g)'
      GruFixOld(130)='P2mb(00g)'
      GruFixOld(131)='P2mb(00g)0s0'
      GruFixOld(132)='P2cm(01/2g)'
      GruFixOld(133)='P2cm(1/21/2g)'
      GruFixOld(134)='Pca21(00g)'
      GruFixOld(135)='Pca21(00g)0ss'
      GruFixOld(136)='Pca21(01/2g)'
      GruFixOld(137)='P21ca(00g)'
      GruFixOld(138)='P21ab(00g)'
      GruFixOld(139)='P21ab(00g)0s0'
      GruFixOld(140)='P21ca(01/2g)'
      GruFixOld(141)='Pcn2(00g)'
      GruFixOld(142)='Pcn2(00g)s0s'
      GruFixOld(143)='Pcn2(01/2g)'
      GruFixOld(144)='P2na(00g)'
      GruFixOld(145)='P2an(00g)'
      GruFixOld(146)='P2an(00g)0s0'
      GruFixOld(147)='P2na(01/2g)'
      GruFixOld(148)='P2an(1/21/2g)0q0'
      GruFixOld(149)='Pmn21(00g)'
      GruFixOld(150)='Pmn21(00g)s0s'
      GruFixOld(151)='Pmn21(01/2g)'
      GruFixOld(152)='Pmn21(01/2g)s0s'
      GruFixOld(153)='P21nm(00g)'
      GruFixOld(154)='P21mn(00g)'
      GruFixOld(155)='P21mn(00g)0s0'
      GruFixOld(156)='P21nm(01/2g)'
      GruFixOld(157)='Pba2(00g)'
      GruFixOld(158)='Pba2(00g)s0s'
      GruFixOld(159)='Pba2(00g)ss0'
      GruFixOld(160)='Pba2(1/21/2g)qq0'
      GruFixOld(161)='Pc2a(01/2g)'
      GruFixOld(162)='P2cb(00g)'
      GruFixOld(163)='Pbn21(00g)'
      GruFixOld(164)='Pbn21(00g)s0s'
      GruFixOld(165)='Pbn21(1/21/2g)qq0'
      GruFixOld(166)='P21nb(00g)'
      GruFixOld(167)='P21cn(00g)'
      GruFixOld(168)='Pnn2(00g)'
      GruFixOld(169)='Pnn2(00g)s0s'
      GruFixOld(170)='Pnn2(1/21/2g)qq0'
      GruFixOld(171)='P2nn(00g)'
      GruFixOld(172)='P2nn(1/21/2g)0q0'
      GruFixOld(173)='Cmm2(00g)'
      GruFixOld(174)='Cmm2(00g)s0s'
      GruFixOld(175)='Cmm2(00g)ss0'
      GruFixOld(176)='Cmm2(10g)'
      GruFixOld(177)='Cmm2(10g)s0s'
      GruFixOld(178)='Cmm2(10g)ss0'
      GruFixOld(179)='A2mm(00g)'
      GruFixOld(180)='A2mm(00g)0s0'
      GruFixOld(181)='A2mm(1/20g)'
      GruFixOld(182)='A2mm(1/20g)0s0'
      GruFixOld(183)='Cmc21(00g)'
      GruFixOld(184)='Cmc21(00g)s0s'
      GruFixOld(185)='Cmc21(10g)'
      GruFixOld(186)='Cmc21(10g)s0s'
      GruFixOld(187)='A21am(00g)'
      GruFixOld(188)='A21am(00g)0s0'
      GruFixOld(189)='A21ma(00g)'
      GruFixOld(190)='A21ma(00g)0s0'
      GruFixOld(191)='Ccc2(00g)'
      GruFixOld(192)='Ccc2(00g)s0s'
      GruFixOld(193)='Ccc2(10g)'
      GruFixOld(194)='Ccc2(10g)s0s'
      GruFixOld(195)='A2aa(00g)'
      GruFixOld(196)='A2aa(00g)0s0'
      GruFixOld(197)='C2mm(00g)'
      GruFixOld(198)='C2mm(00g)0s0'
      GruFixOld(199)='C2mm(10g)'
      GruFixOld(200)='C2mm(10g)0s0'
      GruFixOld(201)='Amm2(00g)'
      GruFixOld(202)='Amm2(00g)s0s'
      GruFixOld(203)='Amm2(00g)ss0'
      GruFixOld(204)='Amm2(00g)0ss'
      GruFixOld(205)='Amm2(1/20g)'
      GruFixOld(206)='Amm2(1/20g)0ss'
      GruFixOld(207)='Am2m(00g)'
      GruFixOld(208)='Am2m(00g)s00'
      GruFixOld(209)='Am2m(1/20g)'
      GruFixOld(210)='C2mb(00g)'
      GruFixOld(211)='C2mb(00g)0s0'
      GruFixOld(212)='C2mb(10g)'
      GruFixOld(213)='C2mb(10g)0s0'
      GruFixOld(214)='Abm2(00g)'
      GruFixOld(215)='Abm2(00g)s0s'
      GruFixOld(216)='Abm2(00g)ss0'
      GruFixOld(217)='Abm2(00g)0ss'
      GruFixOld(218)='Abm2(1/20g)'
      GruFixOld(219)='Abm2(1/20g)0ss'
      GruFixOld(220)='Ac2m(00g)'
      GruFixOld(221)='Ac2m(00g)s00'
      GruFixOld(222)='Ac2m(1/20g)'
      GruFixOld(223)='C2cm(00g)'
      GruFixOld(224)='C2cm(10g)'
      GruFixOld(225)='Ama2(00g)'
      GruFixOld(226)='Ama2(00g)s0s'
      GruFixOld(227)='Ama2(00g)ss0'
      GruFixOld(228)='Ama2(00g)0ss'
      GruFixOld(229)='Am2a(00g)'
      GruFixOld(230)='Am2a(00g)s00'
      GruFixOld(231)='C2cb(00g)'
      GruFixOld(232)='C2cb(10g)'
      GruFixOld(233)='Aba2(00g)'
      GruFixOld(234)='Aba2(00g)s0s'
      GruFixOld(235)='Aba2(00g)ss0'
      GruFixOld(236)='Aba2(00g)0ss'
      GruFixOld(237)='Ac2a(00g)'
      GruFixOld(238)='Ac2a(00g)s00'
      GruFixOld(239)='Fmm2(00g)'
      GruFixOld(240)='Fmm2(00g)s0s'
      GruFixOld(241)='Fmm2(00g)ss0'
      GruFixOld(242)='Fmm2(10g)'
      GruFixOld(243)='Fmm2(10g)s0s'
      GruFixOld(244)='Fmm2(10g)ss0'
      GruFixOld(245)='F2mm(00g)'
      GruFixOld(246)='F2mm(00g)0s0'
      GruFixOld(247)='F2mm(10g)'
      GruFixOld(248)='F2mm(10g)0s0'
      GruFixOld(249)='Fdd2(00g)'
      GruFixOld(250)='Fdd2(00g)s0s'
      GruFixOld(251)='F2dd(00g)'
      GruFixOld(252)='Imm2(00g)'
      GruFixOld(253)='Imm2(00g)s0s'
      GruFixOld(254)='Imm2(00g)ss0'
      GruFixOld(255)='I2mm(00g)'
      GruFixOld(256)='I2mm(00g)0s0'
      GruFixOld(257)='Iba2(00g)'
      GruFixOld(258)='Iba2(00g)s0s'
      GruFixOld(259)='Iba2(00g)ss0'
      GruFixOld(260)='I2cb(00g)'
      GruFixOld(261)='I2cb(00g)0s0'
      GruFixOld(262)='Ima2(00g)'
      GruFixOld(263)='Ima2(00g)s0s'
      GruFixOld(264)='Ima2(00g)ss0'
      GruFixOld(265)='Ima2(00g)0ss'
      GruFixOld(266)='I2mb(00g)'
      GruFixOld(267)='I2mb(00g)0s0'
      GruFixOld(268)='I2cm(00g)'
      GruFixOld(269)='I2cm(00g)0s0'
      GruFixOld(270)='Pmmm(00g)'
      GruFixOld(271)='Pmmm(00g)s00'
      GruFixOld(272)='Pmmm(00g)ss0'
      GruFixOld(273)='Pmmm(01/2g)'
      GruFixOld(274)='Pmmm(01/2g)s00'
      GruFixOld(275)='Pmmm(1/21/2g)'
      GruFixOld(276)='Pnnn(00g)'
      GruFixOld(277)='Pnnn(00g)s00'
      GruFixOld(278)='Pnnn(1/21/2g)qq0'
      GruFixOld(279)='Pccm(00g)'
      GruFixOld(280)='Pccm(00g)s00'
      GruFixOld(281)='Pmaa(00g)'
      GruFixOld(282)='Pmaa(00g)s00'
      GruFixOld(283)='Pmaa(00g)ss0'
      GruFixOld(284)='Pmaa(00g)0s0'
      GruFixOld(285)='Pccm(01/2g)'
      GruFixOld(286)='Pmaa(01/2g)'
      GruFixOld(287)='Pmaa(01/2g)s00'
      GruFixOld(288)='Pccm(1/21/2g)'
      GruFixOld(289)='Pban(00g)'
      GruFixOld(290)='Pban(00g)s00'
      GruFixOld(291)='Pban(00g)ss0'
      GruFixOld(292)='Pcna(00g)'
      GruFixOld(293)='Pcna(00g)s00'
      GruFixOld(294)='Pcna(01/2g)'
      GruFixOld(295)='Pban(1/21/2g)qq0'
      GruFixOld(296)='Pmma(00g)'
      GruFixOld(297)='Pmma(00g)s00'
      GruFixOld(298)='Pmma(00g)ss0'
      GruFixOld(299)='Pmma(00g)0s0'
      GruFixOld(300)='Pmam(00g)'
      GruFixOld(301)='Pmam(00g)s00'
      GruFixOld(302)='Pmam(00g)ss0'
      GruFixOld(303)='Pmam(00g)0s0'
      GruFixOld(304)='Pmcm(00g)'
      GruFixOld(305)='Pmcm(00g)s00'
      GruFixOld(306)='Pmma(01/2g)'
      GruFixOld(307)='Pmma(01/2g)s00'
      GruFixOld(308)='Pmam(01/2g)'
      GruFixOld(309)='Pmam(01/2g)s00'
      GruFixOld(310)='Pmcm(01/2g)'
      GruFixOld(311)='Pmcm(01/2g)s00'
      GruFixOld(312)='Pcmm(01/2g)'
      GruFixOld(313)='Pcmm(1/21/2g)'
      GruFixOld(314)='Pnna(00g)'
      GruFixOld(315)='Pnna(00g)s00'
      GruFixOld(316)='Pbnn(00g)'
      GruFixOld(317)='Pbnn(00g)s00'
      GruFixOld(318)='Pcnn(00g)'
      GruFixOld(319)='Pcnn(00g)s00'
      GruFixOld(320)='Pbnn(1/21/2g)qq0'
      GruFixOld(321)='Pmna(00g)'
      GruFixOld(322)='Pmna(00g)s00'
      GruFixOld(323)='Pcnm(00g)'
      GruFixOld(324)='Pcnm(00g)s00'
      GruFixOld(325)='Pbmn(00g)'
      GruFixOld(326)='Pbmn(00g)s00'
      GruFixOld(327)='Pbmn(00g)ss0'
      GruFixOld(328)='Pbmn(00g)0s0'
      GruFixOld(329)='Pmna(01/2g)'
      GruFixOld(330)='Pmna(01/2g)s00'
      GruFixOld(331)='Pcnm(01/2g)'
      GruFixOld(332)='Pcca(00g)'
      GruFixOld(333)='Pcca(00g)s00'
      GruFixOld(334)='Pcaa(00g)'
      GruFixOld(335)='Pcaa(00g)0s0'
      GruFixOld(336)='Pbab(00g)'
      GruFixOld(337)='Pbab(00g)s00'
      GruFixOld(338)='Pbab(00g)ss0'
      GruFixOld(339)='Pbab(00g)0s0'
      GruFixOld(340)='Pcca(01/2g)'
      GruFixOld(341)='Pcaa(01/2g)'
      GruFixOld(342)='Pbam(00g)'
      GruFixOld(343)='Pbam(00g)s00'
      GruFixOld(344)='Pbam(00y)ss0'
      GruFixOld(345)='Pcma(00g)'
      GruFixOld(346)='Pcma(00g)0s0'
      GruFixOld(347)='Pcma(01/2g)'
      GruFixOld(348)='Pccn(00g)'
      GruFixOld(349)='Pccn(00g)s00'
      GruFixOld(350)='Pbnb(00g)'
      GruFixOld(351)='Pbnb(00g)s00'
      GruFixOld(352)='Pcam(00g)'
      GruFixOld(353)='Pcam(00g)0s0'
      GruFixOld(354)='Pmca(00g)'
      GruFixOld(355)='Pmca(00g)s00'
      GruFixOld(356)='Pbma(00g)'
      GruFixOld(357)='Pbma(00g)s00'
      GruFixOld(358)='Pbma(00g)ss0'
      GruFixOld(359)='Pbma(00g)0s0'
      GruFixOld(360)='Pcam(01/2g)'
      GruFixOld(361)='Pmca(01/2g)'
      GruFixOld(362)='Pmca(01/2g)s00'
      GruFixOld(363)='Pnnm(00g)'
      GruFixOld(364)='Pnnm(00g)s00'
      GruFixOld(365)='Pmnn(00g)'
      GruFixOld(366)='Pmnn(00g)s00'
      GruFixOld(367)='Pmmn(00g)'
      GruFixOld(368)='Pmmn(00g)s00'
      GruFixOld(369)='Pmmn(00g)ss0'
      GruFixOld(370)='Pmnm(00g)'
      GruFixOld(371)='Pmnm(00g)s00'
      GruFixOld(372)='Pmnm(01/2g)'
      GruFixOld(373)='Pmnm(01/2g)s00'
      GruFixOld(374)='Pbcn(00g)'
      GruFixOld(375)='Pbcn(00g)s00'
      GruFixOld(376)='Pnca(00g)'
      GruFixOld(377)='Pnca(00g)s00'
      GruFixOld(378)='Pbna(00g)'
      GruFixOld(379)='Pbna(00g)s00'
      GruFixOld(380)='Pbca(00g)'
      GruFixOld(381)='Pbca(00g)s00'
      GruFixOld(382)='Pnma(00g)'
      GruFixOld(383)='Pnma(00g)0s0'
      GruFixOld(384)='Pbnm(00g)'
      GruFixOld(385)='Pbnm(00g)s00'
      GruFixOld(386)='Pmcn(00g)'
      GruFixOld(387)='Pmcn(00g)s00'
      GruFixOld(388)='Cmcm(00g)'
      GruFixOld(389)='Cmcm(00g)s00'
      GruFixOld(390)='Cmcm(10g)'
      GruFixOld(391)='Cmcm(10g)s00'
      GruFixOld(392)='Amam(00g)'
      GruFixOld(393)='Amam(00g)s00'
      GruFixOld(394)='Amam(00g)ss0'
      GruFixOld(395)='Amam(00g)0s0'
      GruFixOld(396)='Amma(00g)'
      GruFixOld(397)='Amma(00g)s00'
      GruFixOld(398)='Amma(00g)ss0'
      GruFixOld(399)='Amma(00g)0s0'
      GruFixOld(400)='Cmca(00g)'
      GruFixOld(401)='Cmca(00g)s00'
      GruFixOld(402)='Cmca(10g)'
      GruFixOld(403)='Cmca(10y)s00'
      GruFixOld(404)='Abma(00g)'
      GruFixOld(405)='Abma(00g)s00'
      GruFixOld(406)='Abma(00g)ss0'
      GruFixOld(407)='Abma(00g)0s0'
      GruFixOld(408)='Acam(00g)'
      GruFixOld(409)='Acam(00g)s00'
      GruFixOld(410)='Acam(00g)ss0'
      GruFixOld(411)='Acam(00g)0s0'
      GruFixOld(412)='Cmmm(00g)'
      GruFixOld(413)='Cmmm(00g)s00'
      GruFixOld(414)='Cmmm(00g)ss0'
      GruFixOld(415)='Cmmm(10g)'
      GruFixOld(416)='Cmmm(10g)s00'
      GruFixOld(417)='Cmmm(10g)ss0'
      GruFixOld(418)='Ammm(00g)'
      GruFixOld(419)='Ammm(00g)s00'
      GruFixOld(420)='Ammm(00g)ss0'
      GruFixOld(421)='Ammm(00g)0s0'
      GruFixOld(422)='Ammm(1/20g)'
      GruFixOld(423)='Ammm(1/20g)0s0'
      GruFixOld(424)='Cccm(00g)'
      GruFixOld(425)='Cccm(00g)s00'
      GruFixOld(426)='Cccm(10g)'
      GruFixOld(427)='Cccm(10y)s00'
      GruFixOld(428)='Amaa(00g)'
      GruFixOld(429)='Amaa(00g)s00'
      GruFixOld(430)='Amaa(00g)ss0'
      GruFixOld(431)='Amaa(00g)0s0'
      GruFixOld(432)='Cmma(00g)'
      GruFixOld(433)='Cmma(00g)s00'
      GruFixOld(434)='Cmma(00g)ss0'
      GruFixOld(435)='Cmma(10g)'
      GruFixOld(436)='Cmma(10g)s00'
      GruFixOld(437)='Cmma(10g)ss0'
      GruFixOld(438)='Acmm(00g)'
      GruFixOld(439)='Acmm(00g)s00'
      GruFixOld(440)='Acmm(00g)ss0'
      GruFixOld(441)='Acmm(00g)0s0'
      GruFixOld(442)='Acmm(1/20g)'
      GruFixOld(443)='Acmm(1/20g)0s0'
      GruFixOld(444)='Ccca(00g)'
      GruFixOld(445)='Ccca(00g)s00'
      GruFixOld(446)='Ccca(10g)'
      GruFixOld(447)='Ccca(10g)s00'
      GruFixOld(448)='Acaa(00g)'
      GruFixOld(449)='Acaa(00g)s00'
      GruFixOld(450)='Acaa(00g)ss0'
      GruFixOld(451)='Acaa(00g)0s0'
      GruFixOld(452)='Fmmm(00g)'
      GruFixOld(453)='Fmmm(00g)s00'
      GruFixOld(454)='Fmmm(00g)ss0'
      GruFixOld(455)='Fmmm(10g)'
      GruFixOld(456)='Fmmm(10g)s00'
      GruFixOld(457)='Fmmm(10g)ss0'
      GruFixOld(458)='Fddd(00g)'
      GruFixOld(459)='Fddd(00g)s00'
      GruFixOld(460)='Immm(00g)'
      GruFixOld(461)='Immm(00g)s00'
      GruFixOld(462)='Immm(00g)ss0'
      GruFixOld(463)='Ibam(00g)'
      GruFixOld(464)='Ibam(00g)s00'
      GruFixOld(465)='Ibam(00g)ss0'
      GruFixOld(466)='Imcb(00g)'
      GruFixOld(467)='Imcb(00g)s00'
      GruFixOld(468)='Imcb(00g)ss0'
      GruFixOld(469)='Imcb(00g)0s0'
      GruFixOld(470)='Ibca(00g)'
      GruFixOld(471)='Ibca(00g)s00'
      GruFixOld(472)='Ibca(00g)ss0'
      GruFixOld(473)='Imma(00g)'
      GruFixOld(474)='Imma(00g)s00'
      GruFixOld(475)='Imma(00g)ss0'
      GruFixOld(476)='Icmm(00g)'
      GruFixOld(477)='Icmm(00g)s00'
      GruFixOld(478)='Icmm(00g)ss0'
      GruFixOld(479)='Icmm(00g)0s0'
      GruFixOld(480)='P4(00g)'
      GruFixOld(481)='P4(00g)q'
      GruFixOld(482)='P4(00g)s'
      GruFixOld(483)='P4(1/21/2g)'
      GruFixOld(484)='P4(1/21/2g)q'
      GruFixOld(485)='P41(00g)'
      GruFixOld(486)='P41(1/21/2g)'
      GruFixOld(487)='P42(00g)'
      GruFixOld(488)='P42(00g)q'
      GruFixOld(489)='P42(1/21/2g)'
      GruFixOld(490)='P42(1/21/2g)q'
      GruFixOld(491)='P43(00g)'
      GruFixOld(492)='P43(1/21/2g)q'
      GruFixOld(493)='I4(00g)'
      GruFixOld(494)='I4(00g)q'
      GruFixOld(495)='I4(00g)s'
      GruFixOld(496)='I41(00g)'
      GruFixOld(497)='I41(00g)q'
      GruFixOld(498)='P4(00g)'
      GruFixOld(499)='P-4(1/21/2g)'
      GruFixOld(500)='I-4(00g)'
      GruFixOld(501)='P4/m(00g)'
      GruFixOld(502)='P4/m(00g)s0'
      GruFixOld(503)='P4/m(1/21/2g)'
      GruFixOld(504)='P42/m(00g)'
      GruFixOld(505)='P42/m(1/21/2g)'
      GruFixOld(506)='P4/n(00g)'
      GruFixOld(507)='P4/n(00g)s0'
      GruFixOld(508)='P4/n(1/21/2g)q0'
      GruFixOld(509)='P42/n(00g)'
      GruFixOld(510)='P42/n(1/21/2g)q0'
      GruFixOld(511)='I4/m(00g)'
      GruFixOld(512)='I4/m(00g)s0'
      GruFixOld(513)='I41/a(00g)'
      GruFixOld(514)='P422(00g)'
      GruFixOld(515)='P422(00g)q00'
      GruFixOld(516)='P422(00g)s00'
      GruFixOld(517)='P422(1/21/2g)'
      GruFixOld(518)='P422(1/21/2g)q00'
      GruFixOld(519)='P4212(00g)'
      GruFixOld(520)='P4212(00g)q00'
      GruFixOld(521)='P4212(00g)s00'
      GruFixOld(522)='P4122(00g)'
      GruFixOld(523)='P4122(1/21/2g)'
      GruFixOld(524)='P41212(00g)'
      GruFixOld(525)='P4222(00g)'
      GruFixOld(526)='P4222(00g)q00'
      GruFixOld(527)='P4222(1/21/2g)'
      GruFixOld(528)='P4222(1/21/2g)q00'
      GruFixOld(529)='P42212(00g)'
      GruFixOld(530)='P42212(00g)q00'
      GruFixOld(531)='P4322(00g)'
      GruFixOld(532)='P4322(1/21/2g)'
      GruFixOld(533)='P43212(00g)'
      GruFixOld(534)='I422(00g)'
      GruFixOld(535)='I422(00g)q00'
      GruFixOld(536)='I422(00g)s00'
      GruFixOld(537)='I4122(00g)'
      GruFixOld(538)='I4122(00g)q00'
      GruFixOld(539)='P4mm(00g)'
      GruFixOld(540)='P4mm(00g)ss0'
      GruFixOld(541)='P4mm(00g)0ss'
      GruFixOld(542)='P4mm(00g)s0s'
      GruFixOld(543)='P4mm(1/21/2g)'
      GruFixOld(544)='P4mm(1/21/2g)0ss'
      GruFixOld(545)='P4bm(00g)'
      GruFixOld(546)='P4bm(00g)ss0'
      GruFixOld(547)='P4bm(00g)0ss'
      GruFixOld(548)='P4bm(00y)s0s'
      GruFixOld(549)='P4bm(1/21/2g)qq0'
      GruFixOld(550)='P4bm(1/21/2g)qqs'
      GruFixOld(551)='P42cm(00g)'
      GruFixOld(552)='P42cm(00g)0ss'
      GruFixOld(553)='P42cm(1/21/2g)'
      GruFixOld(554)='P42cm(1/21/2g)0ss'
      GruFixOld(555)='P42nm(00g)'
      GruFixOld(556)='P42nm(00g)0ss'
      GruFixOld(557)='P42nm(1/21/2g)qq0'
      GruFixOld(558)='P42nm(1/21/2g)qqs'
      GruFixOld(559)='P4cc(00g)'
      GruFixOld(560)='P4cc(00g)ss0'
      GruFixOld(561)='P4cc(1/21/2g)'
      GruFixOld(562)='P4nc(00g)'
      GruFixOld(563)='P4nc(00g)ss0'
      GruFixOld(564)='P4nc(1/21/2g)qq0'
      GruFixOld(565)='P42mc(00g)'
      GruFixOld(566)='P42mc(00g)ss0'
      GruFixOld(567)='P42mc(1/21/2g)'
      GruFixOld(568)='P42bc(00g)'
      GruFixOld(569)='P42bc(00g)ss0'
      GruFixOld(570)='P42bc(1/21/2g)qq0'
      GruFixOld(571)='I4mm(00g)'
      GruFixOld(572)='I4mm(00g)ss0'
      GruFixOld(573)='I4mm(00g)0ss'
      GruFixOld(574)='I4mm(00g)s0s'
      GruFixOld(575)='I4cm(00g)'
      GruFixOld(576)='I4cm(00g)ss0'
      GruFixOld(577)='I4cm(00g)0ss'
      GruFixOld(578)='I4cm(00g)s0s'
      GruFixOld(579)='I41md(00g)'
      GruFixOld(580)='I41md(00g)ss0'
      GruFixOld(581)='I41cd(00g)'
      GruFixOld(582)='I41cd(00g)ss0'
      GruFixOld(583)='P-42m(00g)'
      GruFixOld(584)='P-42m(00g)00s'
      GruFixOld(585)='P-42m(1/21/2g)'
      GruFixOld(586)='P-42m(1/21/2g)00s'
      GruFixOld(587)='P-42c(00g)'
      GruFixOld(588)='P-42c(1/21/2g)'
      GruFixOld(589)='P-421m(00g)'
      GruFixOld(590)='P-421m(00g)00s'
      GruFixOld(591)='P-421c(00g)'
      GruFixOld(592)='P-4m2(00g)'
      GruFixOld(593)='P-4m2(00g)0s0'
      GruFixOld(594)='P-4m2(1/21/2g)'
      GruFixOld(595)='P-4c2(00g)'
      GruFixOld(596)='P-4c2(1/21/2g)'
      GruFixOld(597)='P-4b2(00g)'
      GruFixOld(598)='P-4b2(00g)0s0'
      GruFixOld(599)='P-4b2(1/21/2g)0q0'
      GruFixOld(600)='P-4n2(00g)'
      GruFixOld(601)='P-4n2(1/21/2g)0q0'
      GruFixOld(602)='I-4m2(00g)'
      GruFixOld(603)='I-4m2(00g)0s0'
      GruFixOld(604)='I-4c2(00g)'
      GruFixOld(605)='I-4c2(00g)0s0'
      GruFixOld(606)='I-42m(00g)'
      GruFixOld(607)='I-42m(00g)00s'
      GruFixOld(608)='I42d(00g)'
      GruFixOld(609)='P4/mmm(00g)'
      GruFixOld(610)='P4/mmm(00g)s0s0'
      GruFixOld(611)='P4/mmm(00g)00ss'
      GruFixOld(612)='P4/mmm(00g)s00s'
      GruFixOld(613)='P4/mmm(1/21/2g)'
      GruFixOld(614)='P4/mmm(1/21/2g)00ss'
      GruFixOld(615)='P4/mcc(00g)'
      GruFixOld(616)='P4/mcc(00g)s0s0'
      GruFixOld(617)='P4/mcc(1/21/2g)'
      GruFixOld(618)='P4/nbm(00g)'
      GruFixOld(619)='P4/nbm(00g)s0s0'
      GruFixOld(620)='P4/nbm(00g)00ss'
      GruFixOld(621)='P4/nbm(00g)s00s'
      GruFixOld(622)='P4/nbm(1/21/2g)q0q0'
      GruFixOld(623)='P4/nbm(1/21/2g)q0qs'
      GruFixOld(624)='P4/nnc(00g)'
      GruFixOld(625)='P4/nnc(00g)s0s0'
      GruFixOld(626)='P4/nnc(1/21/2g)q0q0'
      GruFixOld(627)='P4/mbm(00g)'
      GruFixOld(628)='P4/mbm(00g)s0s0'
      GruFixOld(629)='P4/mbm(00g)00ss'
      GruFixOld(630)='P4/mbm(00g)s00s'
      GruFixOld(631)='P4/mnc(00g)'
      GruFixOld(632)='P4/mnc(00g)s0s0'
      GruFixOld(633)='P4/nmm(00g)'
      GruFixOld(634)='P4/nmm(00g)s0s0'
      GruFixOld(635)='P4/nmm(00g)00ss'
      GruFixOld(636)='P4/nmm(00g)s00s'
      GruFixOld(637)='P4/ncc(00g)'
      GruFixOld(638)='P4/ncc(00g)s0s0'
      GruFixOld(639)='P42/mmc(00g)'
      GruFixOld(640)='P42/mmc(00g)s0s0'
      GruFixOld(641)='P42/mmc(1/21/2g)'
      GruFixOld(642)='P42/mcm(00g)'
      GruFixOld(643)='P42/mcm(00g)00ss'
      GruFixOld(644)='P42/mcm(1/21/2g)'
      GruFixOld(645)='P42/mcm(1/21/2g)00ss'
      GruFixOld(646)='P42/nbc(00g)'
      GruFixOld(647)='P42/nbc(00g)s0s0'
      GruFixOld(648)='P42/nbc(1/21/2g)q0q0'
      GruFixOld(649)='P42/nnm(00g)'
      GruFixOld(650)='P42/nnm(00g)00ss'
      GruFixOld(651)='P42/nnm(1/21/2g)q0q0'
      GruFixOld(652)='P42/nnm(1/21/2g)q0qs'
      GruFixOld(653)='P42/mbc(00g)'
      GruFixOld(654)='P42/mbc(00g)s0s0'
      GruFixOld(655)='P42/mnm(00g)'
      GruFixOld(656)='P42/mnm(00g)00ss'
      GruFixOld(657)='P42/nmc(00g)'
      GruFixOld(658)='P42/nmc(00g)s0s0'
      GruFixOld(659)='P42/ncm(00g)'
      GruFixOld(660)='P42/ncm(00g)00ss'
      GruFixOld(661)='I4/mmm(00g)'
      GruFixOld(662)='I4/mmm(00g)s0s0'
      GruFixOld(663)='I4/mmm(00g)00ss'
      GruFixOld(664)='I4/mmm(00g)s00s'
      GruFixOld(665)='I4/mcm(00g)'
      GruFixOld(666)='I4/mcm(00g)s0s0'
      GruFixOld(667)='I4/mcm(00g)00ss'
      GruFixOld(668)='I4/mcm(00g)s00s'
      GruFixOld(669)='I41/amd(00g)'
      GruFixOld(670)='I41/amd(00g)s0s0'
      GruFixOld(671)='I41/acd(00g)'
      GruFixOld(672)='I41/acd(00g)s0s0'
      GruFixOld(673)='P3(1/31/3g)'
      GruFixOld(674)='P3(00g)'
      GruFixOld(675)='P3(00g)t'
      GruFixOld(676)='P31(1/31/3g)'
      GruFixOld(677)='P31(00g)'
      GruFixOld(678)='P32(1/31/3g)'
      GruFixOld(679)='P32(00g)'
      GruFixOld(680)='R3(00g)'
      GruFixOld(681)='R3(00g)t'
      GruFixOld(682)='P-3(1/31/3g)'
      GruFixOld(683)='P-3(00g)'
      GruFixOld(684)='R-3(00g)'
      GruFixOld(685)='P312(1/31/3g)'
      GruFixOld(686)='P312(00g)'
      GruFixOld(687)='P312(00g)t00'
      GruFixOld(688)='P321(00g)'
      GruFixOld(689)='P321(00g)t00'
      GruFixOld(690)='P3112(1/31/3g)'
      GruFixOld(691)='P3112(00g)'
      GruFixOld(692)='P3121(00g)'
      GruFixOld(693)='P3212(1/31/3g)'
      GruFixOld(694)='P3212(00g)'
      GruFixOld(695)='P3221(00g)'
      GruFixOld(696)='R32(00g)'
      GruFixOld(697)='R32(00g)t0'
      GruFixOld(698)='P3m1(00g)'
      GruFixOld(699)='P3m1(00g)0s0'
      GruFixOld(700)='P31m(1/31/3g)'
      GruFixOld(701)='P31m(1/31/3g)00s'
      GruFixOld(702)='P31m(00g)'
      GruFixOld(703)='P31m(00g)00s'
      GruFixOld(704)='P3cl(00g)'
      GruFixOld(705)='P31c(1/31/3g)'
      GruFixOld(706)='P31c(00g)'
      GruFixOld(707)='R3m(00g)'
      GruFixOld(708)='R3m(00g)0s'
      GruFixOld(709)='R3c(00g)'
      GruFixOld(710)='P-31m(1/31/3g)'
      GruFixOld(711)='P-31m(1/31/3g)00s'
      GruFixOld(712)='P-31m(00g)'
      GruFixOld(713)='P-31m(00g)00s'
      GruFixOld(714)='P-31c(1/31/3g)'
      GruFixOld(715)='P-31c(00g)'
      GruFixOld(716)='P-3m1(00g)'
      GruFixOld(717)='P-3ml(00g)0s0'
      GruFixOld(718)='P-3cl(00g)'
      GruFixOld(719)='R-3m(00g)'
      GruFixOld(720)='R-3m(00g)0s'
      GruFixOld(721)='R-3c(00g)'
      GruFixOld(722)='P6(00g)'
      GruFixOld(723)='P6(00g)h'
      GruFixOld(724)='P6(00g)t'
      GruFixOld(725)='P6(00g)s'
      GruFixOld(726)='P61(00g)'
      GruFixOld(727)='P65(00g)'
      GruFixOld(728)='P62(00g)'
      GruFixOld(729)='P62(00g)h'
      GruFixOld(730)='P64(00g)'
      GruFixOld(731)='P64(00g)h'
      GruFixOld(732)='P63(00g)'
      GruFixOld(733)='P63(00g)h'
      GruFixOld(734)='P-6(00g)'
      GruFixOld(735)='P6/m(00g)'
      GruFixOld(736)='P6/m(00g)s0'
      GruFixOld(737)='P63/m(00g)'
      GruFixOld(738)='P622(00g)'
      GruFixOld(739)='P622(00g)h00'
      GruFixOld(740)='P622(00g)t00'
      GruFixOld(741)='P622(00g)s00'
      GruFixOld(742)='P6122(00g)'
      GruFixOld(743)='P6222(00g)'
      GruFixOld(744)='P6222(00g)'
      GruFixOld(745)='P6222(00g)h00'
      GruFixOld(746)='P6422(00g)'
      GruFixOld(747)='P6422(00g)h00'
      GruFixOld(748)='P6322(00g)'
      GruFixOld(749)='P6322(00g)h00'
      GruFixOld(750)='P6mm(00g)'
      GruFixOld(751)='P6mm(00g)ss0'
      GruFixOld(752)='P6mm(00g)0ss'
      GruFixOld(753)='P6mm(00g)s0s'
      GruFixOld(754)='P6cc(00g)'
      GruFixOld(755)='P6cc(00g)s0s'
      GruFixOld(756)='P63cm(00g)'
      GruFixOld(757)='P63cm(00g)0ss'
      GruFixOld(758)='P63mc(00g)'
      GruFixOld(759)='P63mc(00g)0ss'
      GruFixOld(760)='P6m2(00g)'
      GruFixOld(761)='P-6m2(00g)0s0'
      GruFixOld(762)='P-6c2(00g)'
      GruFixOld(763)='P-62m(00g)'
      GruFixOld(764)='P-62m(00g)00s'
      GruFixOld(765)='P-62c(00g)'
      GruFixOld(766)='P6/mmm(00g)'
      GruFixOld(767)='P6/mmm(00g)s0s0'
      GruFixOld(768)='P6/mmm(00g)00ss'
      GruFixOld(769)='P6/mmm(00g)s00s'
      GruFixOld(770)='P6/mcc(00g)'
      GruFixOld(771)='P6/mcc(00g)s00s'
      GruFixOld(772)='P63/mcm(00g)'
      GruFixOld(773)='P63/mcm(00g)00ss'
      GruFixOld(774)='P63/mmc(00g)'
      GruFixOld(775)='P63/mmc(00g)00ss'
      NDim(KPhase)=4
      NDimI(1)=NDim(KPhase)-3
      NDimQ(1)=NDim(KPhase)**2
      KPhase=1
      call SetRealArrayTo(ShSg,4,0.)
      n=0
      call OpenFile(46,'grtest.txt','formatted','unknown')
      call OpenFile(47,'diff.txt','formatted','unknown')
1200  n=n+1
      Monoclinic(KPhase)=3
      Grupa(KPhase)=GruFix(n)
      if(.not.EqIgCase(GruFixOld(n),GruFix(n))) then
        write(47,'(i5,2x,a15,''=>  '',a20)')
     1    n,GruFixOld(n),GruFix(n)
      endif
      write(46,'(''Grupa : '',a)') Grupa(KPhase)(:idel(Grupa(KPhase)))
      write(46,FormA1)('=',i=1,8+idel(Grupa(KPhase)))
      write(46,FormA1)
      i1=index(Grupa(KPhase),'(')
      i2=index(Grupa(KPhase)(i1+1:),')')+i1
      Veta=Grupa(KPhase)(i1+1:i2-1)
      call mala(Veta)
      do i=1,3
        if(idel(Veta).le.0) then
          call FeWinMessage(Grupa(KPhase),
     1                   'Chyba 1 pri dekodovani modulacniho vektoru')
          go to 1200
        endif
        k=1
1210    if(Veta(k:k).eq.smbq(i)(k:k)) then
          k=k+1
          go to 1210
        endif
        k=k-1
        if(k.eq.0) then
          j=index(Veta,'/')
          if(j.le.0.or.j.gt.3.or.(j.eq.3.and.Veta(1:1).ne.'-')) then
            k=1
          else
            k=j+1
          endif
          quir(i,1,KPhase)=fract(Veta(1:k),ich)
          qu(i,1,1,KPhase)=quir(i,1,KPhase)
          if(ich.ne.0) then
            call FeWinMessage(Grupa(KPhase),
     1                    'Chyba 2 pri dekodovani modulacniho vektoru')
            go to 1200
          endif
        else
          qu(i,1,1,KPhase)=sqrt(float(i)*.05)
          quir(i,1,KPhase)=0.
        endif
        Veta=Veta(k+1:)
      enddo
      call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,ich)
      Veta='(0,0,0,0)+'
      do i=2,NLattVec(KPhase)
        t80='('
        do j=1,NDim(KPhase)
          call ToFract(vt6(j,i,1,KPhase),Cislo,15)
          call zhusti(Cislo)
          t80=t80(:idel(t80))//Cislo(:idel(Cislo))
          if(j.lt.NDim(KPhase)) then
            t80=t80(:idel(t80))//','
          else
            t80=t80(:idel(t80))//')+'
          endif
        enddo
        Veta=Veta(:idel(Veta)+1)//t80(:idel(t80))
      enddo
      write(46,FormA1)(Veta(i:i),i=1,idel(Veta))
      write(46,FormA1)
      nn=0
      do j=1,NSymm(KPhase)
        call CodeSymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                symmc(1,j,1,KPhase),0)
        do k=1,NDim(KPhase)
          nn=max(nn,idel(symmc(k,j,1,KPhase))+1)
         enddo
      enddo
      if(nn.lt.5) nn=5
      do j=1,NSymm(KPhase)
        Veta=' '
         k=1
        do l=1,NDim(KPhase)
          Veta(k+nn-idel(symmc(l,j,1,KPhase)):)=symmc(l,j,1,KPhase)
          k=k+nn
        enddo
        call newln(1)
        write(46,FormA1)(Veta(l:l),l=1,idel(Veta))
      enddo
      write(46,FormA1)
      if(n.lt.775) go to 1200
      close(46)
      close(47)
      return
      end
      subroutine TestGrup2
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrM(3,3),RmP(3,3),s(6),ShSgO(3),TrMi(3,3),QuSymm(3),
     1          QuOld(3)
      character*256 Veta,Radka
      character*8 GrupaO,GrupaN
      NDim(KPhase)=3
      NDimI(1)=NDim(KPhase)-3
      NDimQ(1)=NDim(KPhase)**2
      KPhase=1
      call SetRealArrayTo(TrM,9,0.)
      call SetRealArrayTo(s,6,0.)

      TrM(1,1)= 1.
      TrM(1,2)=-1.
      TrM(2,1)= 1.
      TrM(3,3)=1.

      call MatInv(TrM,TrMi,pom,3)
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call FeLstMake(-1.,-1.,65,30,' ')
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      n=0
1100  n=n+1
      read(45,FormA256,end=5000) Radka
      read(Radka,FormSG) isg,ipg,i,Grupa(KPhase)
      if(isg.lt.143) go to 1100
      close(45)
      n=n-1
1200  n=n+1
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      do i=1,n
        read(45,FormA256,end=5000) Radka
        read(Radka,'(2i3,4x,a8)') isg,ipg,Grupa(KPhase)
      enddo
      if(isg.ge.195) go to 5000
      close(45)
      Monoclinic(KPhase)=mod(n,3)+1
      GrupaO=Grupa(KPhase)
      write(Radka,'('' Grupa : '',a)') GrupaO(:idel(GrupaO))
      call FeLstWriteLine(Radka,-1)
      write(Radka,'(80a1)') ' ',('=',i=1,8+idel(GrupaO))
      call FeLstWriteLine(Radka,-1)
      call SetRealArrayTo(ShSG,3,0.)
      call SetRealArrayTo(ShSGO,3,0.)
      call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,ich)
      do i=1,NSymm(1)
        call multm(trm,rm6(1,i,1,1),rmp,3,3,3)
        call multm(rmp,trmi,rm6(1,i,1,1),3,3,3)
        call multm(trm,s6(1,i,1,1),s,3,3,1)
        call od0do1(s,s,3)
        call CopyVek(s,s6(1,i,1,1),3)
      enddo
      do i=1,NLattVec(KPhase)
        call multm(trm,vt6(1,i,1,1),s,3,3,1)
        call od0do1(s,s,3)
        call CopyVek(s,vt6(1,i,1,1),3)
      enddo
      if(NDim(KPhase).eq.4) then
        call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
        call CopyVek(QuSymm,Qu(1,1,1,KPhase),3)
      endif
      call FindSmbSg(GrupaN,ChangeOrderNo,1)
      if(NDim(KPhase).eq.4)
     1  call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
      write(Radka,'('' Grupa nactena : '',a8,'' Shift:'',3f8.3)')
     1      GrupaO,(ShSgO(i),i=1,3)
      call FeLstWriteLine(Radka,-1)
      write(Radka,'('' Grupa zjistena: '',a8,'' Shift:'',3f8.3)')
     1      GrupaN,(ShSg(i,1),i=1,3)
      call FeLstWriteLine(Radka,-1)
      if(GrupaN.ne.GrupaO) pause

      go to 1200
5000  call FeLstRemove
      return
      end
      subroutine TestGroupList
      use Basic_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iar(1),TrPom(9),TrPomI(9),TrShift(3),NGrpA(20),
     1          QSymm(3,3)
      integer PorGrpA(20)
      real ListGroupO(:,:)
      character*256 Veta
      character*80  ListGroup(:),t80,Grp,GrpA(20)
      character*8   PointGroup
      character*2   t2m
      integer ListGroupN(:),Choice,Sel(:,:)
      logical EqIgCase,IsNormal
      allocatable ListGroup,ListGroupN,ListGroupO,Sel
      allocate(ListGroup(230),ListGroupN(230),ListGroupO(3,230))
      call SetIntArrayTo(ListGroupN,230,1)
      call SetRealArrayTo(ListGroupO,3*230,0.)
      ln=0
      ListGroup(1)='P1'
      ListGroup(2)='P-1'
      ListGroup(3)='P2'
      ListGroup(4)='P21'
      ListGroup(5)='C2'
      ListGroup(6)='Pm'
      ListGroup(7)='Pc'
      ListGroup(8)='Cm'
      ListGroup(9)='Cc'
      ListGroup(10)='P2/m'
      ListGroup(11)='P21/m'
      ListGroup(12)='C2/m'
      ListGroup(13)='P2/c'
      ListGroup(14)='P21/c'
      ListGroup(15)='C2/c'
      ListGroup(16)='P222'
      ListGroup(17)='P2221'
      ListGroup(18)='P21212'
      ListGroup(19)='P212121'
      ListGroup(20)='C2221'
      ListGroup(21)='C222'
      ListGroup(22)='F222'
      ListGroup(23)='I222'
      ListGroup(24)='I212121'
      ListGroup(25)='Pmm2'
      ListGroup(26)='Pmc21'
      ListGroup(27)='Pcc2'
      ListGroup(28)='Pma2'
      ListGroup(29)='Pca21'
      ListGroup(30)='Pnc2'
      ListGroup(31)='Pmn21'
      ListGroup(32)='Pba2'
      ListGroup(33)='Pna21'
      ListGroup(34)='Pnn2'
      ListGroup(35)='Cmm2'
      ListGroup(36)='Cmc21'
      ListGroup(37)='Ccc2'
      ListGroup(38)='Amm2'
      ListGroup(39)='Aem2'
      ListGroup(40)='Ama2'
      ListGroup(41)='Aea2'
      ListGroup(42)='Fmm2'
      ListGroup(43)='Fdd2'
      ListGroup(44)='Imm2'
      ListGroup(45)='Iba2'
      ListGroup(46)='Ima2'
      ListGroup(47)='Pmmm'
      ListGroup(48)='Pnnn'
      ListGroupN(48)=2
      call SetRealArrayTo(ListGroupO(1,48),3,-.25)
      ListGroup(49)='Pccm'
      ListGroup(50)='Pban'
      ListGroupN(50)=2
      call SetRealArrayTo(ListGroupO(1,50),2,-.25)
      ListGroup(51)='Pmma'
      ListGroup(52)='Pnna'
      ListGroup(53)='Pmna'
      ListGroup(54)='Pcca'
      ListGroup(55)='Pbam'
      ListGroup(56)='Pccn'
      ListGroup(57)='Pbcm'
      ListGroup(58)='Pnnm'
      ListGroup(59)='Pmmn'
      ListGroupN(59)=2
      call SetRealArrayTo(ListGroupO(1,59),2,-.25)
      ListGroup(60)='Pbcn'
      ListGroup(61)='Pbca'
      ListGroup(62)='Pnma'
      ListGroup(63)='Cmcm'
      ListGroup(64)='Cmce'
      ListGroup(65)='Cmmm'
      ListGroup(66)='Cccm'
      ListGroup(67)='Cmme'
      ListGroup(68)='Ccce'
      ListGroupN(68)=2
      call SetRealArrayTo(ListGroupO(2,68),2,-.25)
      ListGroup(69)='Fmmm'
      ListGroup(70)='Fddd'
      ListGroupN(70)=2
      call SetRealArrayTo(ListGroupO(1,70),3,.125)
      ListGroup(71)='Immm'
      ListGroup(72)='Ibam'
      ListGroup(73)='Ibca'
      ListGroup(74)='Imma'
      ListGroup(75)='P4'
      ListGroup(76)='P41'
      ListGroup(77)='P42'
      ListGroup(78)='P43'
      ListGroup(79)='I4'
      ListGroup(80)='I41'
      ListGroup(81)='P-4'
      ListGroup(82)='I-4'
      ListGroup(83)='P4/m'
      ListGroup(84)='P42/m'
      ListGroup(85)='P4/n'
      ListGroupN(85)=2
      ListGroupO(1,85)= .25
      ListGroupO(2,85)=-.25
      ListGroup(86)='P42/n'
      ListGroupN(86)=2
      call SetRealArrayTo(ListGroupO(1,86),3,.25)
      ListGroup(87)='I4/m'
      ListGroup(88)='I41/a'
      ListGroupN(88)=2
      ListGroupO(2,88)=.25
      ListGroupO(3,88)=.125
      ListGroup(89)='P422'
      ListGroup(90)='P4212'
      ListGroup(91)='P4122'
      ListGroup(92)='P41212'
      ListGroup(93)='P4222'
      ListGroup(94)='P42212'
      ListGroup(95)='P4322'
      ListGroup(96)='P43212'
      ListGroup(97)='I422'
      ListGroup(98)='I4122'
      ListGroup(99)='P4mm'
      ListGroup(100)='P4bm'
      ListGroup(101)='P42cm'
      ListGroup(102)='P42nm'
      ListGroup(103)='P4cc'
      ListGroup(104)='P4nc'
      ListGroup(105)='P42mc'
      ListGroup(106)='P42bc'
      ListGroup(107)='I4mm'
      ListGroup(108)='I4cm'
      ListGroup(109)='I41md'
      ListGroup(110)='I41cd'
      ListGroup(111)='P-42m'
      ListGroup(112)='P-42c'
      ListGroup(113)='P-421m'
      ListGroup(114)='P-421c'
      ListGroup(115)='P-4m2'
      ListGroup(116)='P-4c2'
      ListGroup(117)='P-4b2'
      ListGroup(118)='P-4n2'
      ListGroup(119)='I-4m2'
      ListGroup(120)='I-4c2'
      ListGroup(121)='I-42m'
      ListGroup(122)='I-42d'
      ListGroup(123)='P4/mmm'
      ListGroup(124)='P4/mcc'
      ListGroup(125)='P4/nbm'
      ListGroupN(125)=2
      call SetRealArrayTo(ListGroupO(1,125),2,.25)
      ListGroup(126)='P4/nnc'
      ListGroupN(126)=2
      call SetRealArrayTo(ListGroupO(1,126),3,.25)
      ListGroup(127)='P4/mbm'
      ListGroup(128)='P4/mnc'
      ListGroup(129)='P4/nmm'
      ListGroupN(129)=2
      ListGroupO(1,129)= .25
      ListGroupO(2,129)=-.25
      ListGroup(130)='P4/ncc'
      ListGroupN(130)=2
      ListGroupO(1,130)= .25
      ListGroupO(2,130)=-.25
      ListGroup(131)='P42/mmc'
      ListGroup(132)='P42/mcm'
      ListGroup(133)='P42/nbc'
      ListGroupN(133)=2
      ListGroupO(1,133)= .25
      ListGroupO(2,133)=-.25
      ListGroupO(3,133)= .25
      ListGroup(134)='P42/nnm'
      ListGroupN(134)=2
      ListGroupO(1,134)= .25
      ListGroupO(2,134)=-.25
      ListGroupO(3,134)= .25
      ListGroup(135)='P42/mbc'
      ListGroup(136)='P42/mnm'
      ListGroup(137)='P42/nmc'
      ListGroupN(137)=2
      ListGroupO(1,137)= .25
      ListGroupO(2,137)=-.25
      ListGroupO(3,137)= .25
      ListGroup(138)='P42/ncm'
      ListGroupN(138)=2
      ListGroupO(1,138)= .25
      ListGroupO(2,138)=-.25
      ListGroupO(3,138)= .25
      ListGroup(139)='I4/mmm'
      ListGroup(140)='I4/mcm'
      ListGroup(141)='I41/amd'
      ListGroupN(141)=2
      ListGroupO(2,141)=-.25
      ListGroupO(3,141)= .125
      ListGroup(142)='I41/acd'
      ListGroupN(142)=2
      ListGroupO(2,142)=-.25
      ListGroupO(3,142)= .125
      ListGroup(143)='P3'
      ListGroup(144)='P31'
      ListGroup(145)='P32'
      ListGroup(146)='R3'
      ListGroup(147)='P-3'
      ListGroup(148)='R-3'
      ListGroup(149)='P312'
      ListGroup(150)='P321'
      ListGroup(151)='P3112'
      ListGroup(152)='P3121'
      ListGroup(153)='P3212'
      ListGroup(154)='P3221'
      ListGroup(155)='R32'
      ListGroup(156)='P3m1'
      ListGroup(157)='P31m'
      ListGroup(158)='P3c1'
      ListGroup(159)='P31c'
      ListGroup(160)='R3m'
      ListGroup(161)='R3c'
      ListGroup(162)='P-31m'
      ListGroup(163)='P-31c'
      ListGroup(164)='P-3m1'
      ListGroup(165)='P-3c1'
      ListGroup(166)='R-3m'
      ListGroup(167)='R-3c'
      ListGroup(168)='P6'
      ListGroup(169)='P61'
      ListGroup(170)='P65'
      ListGroup(171)='P62'
      ListGroup(172)='P64'
      ListGroup(173)='P63'
      ListGroup(174)='P-6'
      ListGroup(175)='P6/m'
      ListGroup(176)='P63/m'
      ListGroup(177)='P622'
      ListGroup(178)='P6122'
      ListGroup(179)='P6522'
      ListGroup(180)='P6222'
      ListGroup(181)='P6422'
      ListGroup(182)='P6322'
      ListGroup(183)='P6mm'
      ListGroup(184)='P6cc'
      ListGroup(185)='P63cm'
      ListGroup(186)='P63mc'
      ListGroup(187)='P-6m2'
      ListGroup(188)='P-6c2'
      ListGroup(189)='P-62m'
      ListGroup(190)='P-62c'
      ListGroup(191)='P6/mmm'
      ListGroup(192)='P6/mcc'
      ListGroup(193)='P63/mcm'
      ListGroup(194)='P63/mmc'
      ListGroup(195)='P23'
      ListGroup(196)='F23'
      ListGroup(197)='I23'
      ListGroup(198)='P213'
      ListGroup(199)='I213'
      ListGroup(200)='Pm-3'
      ListGroup(201)='Pn-3'
      ListGroupN(201)=2
      call SetRealArrayTo(ListGroupO(1,201),3,.25)
      ListGroup(202)='Fm-3'
      ListGroup(203)='Fd-3'
      ListGroupN(203)=2
      call SetRealArrayTo(ListGroupO(1,203),3,.125)
      ListGroup(204)='Im-3'
      ListGroup(205)='Pa-3'
      ListGroup(206)='Ia-3'
      ListGroup(207)='P432'
      ListGroup(208)='P4232'
      ListGroup(209)='F432'
      ListGroup(210)='F4132'
      ListGroup(211)='I432'
      ListGroup(212)='P4332'
      ListGroup(213)='P4132'
      ListGroup(214)='I4132'
      ListGroup(215)='P-43m'
      ListGroup(216)='F-43m'
      ListGroup(217)='I-43m'
      ListGroup(218)='P-43n'
      ListGroup(219)='F-43c'
      ListGroup(220)='I-43d'
      ListGroup(221)='Pm-3m'
      ListGroup(222)='Pn-3n'
      ListGroupN(222)=2
      call SetRealArrayTo(ListGroupO(1,222),3,.25)
      ListGroup(223)='Pm-3n'
      ListGroup(224)='Pn-3m'
      ListGroupN(224)=2
      call SetRealArrayTo(ListGroupO(1,224),3,.25)
      ListGroup(225)='Fm-3m'
      ListGroup(226)='Fm-3c'
      ListGroup(227)='Fd-3m'
      ListGroupN(227)=2
      call SetRealArrayTo(ListGroupO(1,227),3,.125)
      ListGroup(228)='Fd-3c'
      ListGroupN(228)=2
      call SetRealArrayTo(ListGroupO(1,228),3,.375)
      ListGroup(229)='Im-3m'
      ListGroup(230)='Ia-3d'
      MaxNSymm=1
      MaxNDim=3
      MaxNDimI=0
      MaxNComp=1
      MaxNLattVec=1
      call UnitMat(TrMP,3)
      call AllocateSymm(MaxNSymm,MaxNLattVec,MaxNComp,NPhase)
      KPhase=1
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      NLattVec(KPhase)=1
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      call AllocateSymm(1,1,1,1)
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      CellPar(1,1,KPhase)=5.
      CellPar(2,1,KPhase)=6.
      CellPar(3,1,KPhase)=7.
      CellPar(4,1,KPhase)=90.
      CellPar(5,1,KPhase)=95.
      CellPar(6,1,KPhase)=90.
      Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1     'spgr3test.txt'
      call OpenFile(46,Veta,'formatted','unknown')
      do n=1,230
c      do n=22,22
        if(n.gt.2.and.n.lt.16) then
          Monoclinic(KPhase)=2
          CrSystem(KPhase)=22
        else
          Monoclinic(KPhase)=0
        endif
        Grupa(KPhase)=ListGroup(n)
        Choice=1
        call SetRealArrayTo(ShSg,4,0.)
1100    Veta='Group : '//Grupa(KPhase)(:idel(Grupa(KPhase)))
        if(ListGroupN(n).gt.1) then
          write(Cislo,'('' - choice : '',i1)') Choice
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(46,FormA) Veta(:idel(Veta))
        write(46,FormA1)('=',i=1,idel(Veta))
        write(46,FormA1)
        call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
        call EM50MakeStandardOrder
        Veta='(0,0,0,0)+'
        do i=2,NLattVec(KPhase)
          t80='('
          do j=1,NDim(KPhase)
            call ToFract(vt6(j,i,1,KPhase),Cislo,15)
            call zhusti(Cislo)
            t80=t80(:idel(t80))//Cislo(:idel(Cislo))
            if(j.lt.NDim(KPhase)) then
              t80=t80(:idel(t80))//','
            else
              t80=t80(:idel(t80))//')+'
            endif
          enddo
          Veta=Veta(:idel(Veta)+1)//t80(:idel(t80))
        enddo
        write(46,FormA1)(Veta(i:i),i=1,idel(Veta))
        write(46,FormA1)
        nn=0
        do j=1,NSymm(KPhase)
          call codesymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                  symmc(1,j,1,KPhase),0)
          do k=1,NDim(KPhase)
            kk=idel(symmc(k,j,1,KPhase))+3
            if(symmc(k,j,1,KPhase)(1:1).ne.'-') kk=kk+1
            nn=max(nn,kk)
          enddo
        enddo
        if(nn.lt.5) nn=5
        do j=1,NSymm(KPhase)
          t80=' '
          write(t80,'(''('',i5,'')'')') j
          call Zhusti(t80)
          k=10
          do l=1,NDim(KPhase)
            if(symmc(l,j,1,KPhase)(1:1).eq.'-') then
              kk=k
            else
              kk=k+1
            endif
            t80(kk:)=symmc(l,j,1,KPhase)
            k=k+nn
          enddo
          call newln(1)
          write(46,FormA1)(t80(l:l),l=1,idel(t80))
        enddo
        write(46,FormA1)
        if(NGrupa(KPhase).le.1) go to 1800
        if(NGrupa(KPhase).eq.2) then
          ipg=2
        else if(NGrupa(KPhase).ge.3.and.NGrupa(KPhase).le.5) then
          ipg=3
        else if(NGrupa(KPhase).ge.6.and.NGrupa(KPhase).le.9) then
          ipg=4
        else if(NGrupa(KPhase).ge.10.and.NGrupa(KPhase).le.15) then
          ipg=5
        else if(NGrupa(KPhase).ge.16.and.NGrupa(KPhase).le.24) then
          CellPar(5,1,KPhase)=90.
          ipg=6
        else if(NGrupa(KPhase).ge.25.and.NGrupa(KPhase).le.46) then
          ipg=7
        else if(NGrupa(KPhase).ge.47.and.NGrupa(KPhase).le.74) then
          ipg=8
        else if(NGrupa(KPhase).ge.75.and.NGrupa(KPhase).le.80) then
          CellPar(1,1,KPhase)=5.
          CellPar(2,1,KPhase)=5.
          ipg=9
        else if(NGrupa(KPhase).ge.81.and.NGrupa(KPhase).le.82) then
          ipg=10
        else if(NGrupa(KPhase).ge.83.and.NGrupa(KPhase).le.88) then
          ipg=11
        else if(NGrupa(KPhase).ge.89.and.NGrupa(KPhase).le.98) then
          ipg=12
        else if(NGrupa(KPhase).ge.99.and.NGrupa(KPhase).le.110) then
          ipg=13
        else if(NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.122) then
          ipg=14
          if((NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.114).or.
     1       (NGrupa(KPhase).ge.121.and.NGrupa(KPhase).le.122)) then
            ipgp=14
          else
            ipgp=15
          endif
        else if(NGrupa(KPhase).ge.123.and.NGrupa(KPhase).le.142) then
          ipg=15
        else if(NGrupa(KPhase).ge.143.and.NGrupa(KPhase).le.146) then
          CellPar(6,1,KPhase)=120.
          ipg=16
        else if(NGrupa(KPhase).ge.147.and.NGrupa(KPhase).le.148) then
          ipg=17
        else if(NGrupa(KPhase).ge.149.and.NGrupa(KPhase).le.155) then
          CellPar(6,1,KPhase)=120.
          ipg=18
          if(NGrupa(KPhase).eq.155) then
            ipgp=25
C            32
          else if(mod(NGrupa(KPhase)-148,2).eq.1) then
            ipgp=19
C            312
          else
            ipgp=22
C            321
          endif
        else if(NGrupa(KPhase).ge.156.and.NGrupa(KPhase).le.161) then
          ipg=19
          if(NGrupa(KPhase).ge.160) then
            ipgp=26
C            3m
          else if(mod(NGrupa(KPhase)-155,2).eq.1) then
            ipgp=23
C            3m1
          else
            ipgp=20
C            31m
          endif
        else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.167) then
          ipg=20
          if(NGrupa(KPhase).ge.166) then
            ipgp=27
C            -3m
          else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.163) then
            ipgp=21
C            -31m
          else if(NGrupa(KPhase).ge.164.and.NGrupa(KPhase).le.165) then
            ipgp=24
C            -3m1
          endif
        else if(NGrupa(KPhase).ge.168.and.NGrupa(KPhase).le.173) then
          ipg=21
        else if(NGrupa(KPhase).ge.174.and.NGrupa(KPhase).le.174) then
          ipg=22
        else if(NGrupa(KPhase).ge.175.and.NGrupa(KPhase).le.176) then
          ipg=23
        else if(NGrupa(KPhase).ge.177.and.NGrupa(KPhase).le.182) then
          ipg=24
        else if(NGrupa(KPhase).ge.183.and.NGrupa(KPhase).le.186) then
          ipg=25
        else if(NGrupa(KPhase).ge.187.and.NGrupa(KPhase).le.190) then
          ipg=26
          if(NGrupa(KPhase).le.188) then
            ipgp=33
          else
            ipgp=34
          endif
        else if(NGrupa(KPhase).ge.191.and.NGrupa(KPhase).le.194) then
          ipg=27
        else if(NGrupa(KPhase).ge.195.and.NGrupa(KPhase).le.199) then
          ipg=28
        else if(NGrupa(KPhase).ge.200.and.NGrupa(KPhase).le.206) then
          ipg=29
        else if(NGrupa(KPhase).ge.207.and.NGrupa(KPhase).le.214) then
          ipg=30
        else if(NGrupa(KPhase).ge.215.and.NGrupa(KPhase).le.220) then
          ipg=31
        else if(NGrupa(KPhase).ge.221.and.NGrupa(KPhase).le.230) then
          ipg=32
        endif
        if(ipg.le.13) then
          ipgp=ipg
        else if(ipg.ge.15.and.ipg.le.17) then
          ipgp=ipg+1
        else if(ipg.ge.21.and.ipg.le.25) then
          ipgp=ipg+7
        else if(ipg.ge.27) then
          ipgp=ipg+8
        endif
        PointGroup=SmbPGI(ipgp)
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'pgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
1700    read(ln,FormA80,end=1800) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1    go to 1700
        call Kus(Veta,k,Cislo)
        call StToInt(Veta,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 1800
        m=iar(1)
        do i=1,m
          read(ln,FormA80,end=1800) Veta
        enddo
        read(ln,FormA80,end=1800) Veta
        k=0
        call StToInt(Veta,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 1800
        NSubGr=iar(1)
        if(NSubGr.le.0) go to 1800
        allocate(sel(NSymm(KPhase),NSubGr))
        call SetIntArrayTo(Sel,NSymm(KPhase)*NSubGr,0)
        do i=1,NSubGr
          read(ln,FormA80,end=1800) Veta
          k=0
          call StToInt(Veta,k,Sel(1,i),NSymm(KPhase),.false.,ich)
        enddo
        call CloseIfOpened(ln)
        m=NSymm(KPhase)*NLattVec(KPhase)
        allocate(rm6s(NDimQ(KPhase),m),s6s(NDim(KPhase),m),ipri(m),
     1           BratSymmSub(m),vt6s(NDim(KPhase),m),io(3,m),
     2           OpSmb(m),ios(3,m),OpSmbS(m),IswSymms(m),
     3           ZMagS(m),RMagS(9,m))
        do i=1,NSymm(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),
     1               t2m,io(1,i),m,det)
          ipri(i)=i
        enddo
        call ReallocSymm(NDim(KPhase),m,NLattVec(KPhase),NComp(KPhase),
     1                   NPhase,0)
        do i=1,NSubGr
          call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.false.)
          jk=NSymm(KPhase)
          do j=1,NSymm(KPhase)
            k=Sel(j,i)
            if(k.gt.0) then
              BratSymmSub(k)=.true.
            else
              jk=j-1
              go to 1750
            endif
          enddo
1750      call GoToSubGrGetGrSmb(nss,nvts,Grp,GrpA(i),NGrp,ncoset,
     1                           TrPom,TrPomI,TrShift,icrsys,IsNormal,0)
          NGrpA(i)=ind*100000*NLattVec(KPhase)/nvts+(300-NGrp)*100+i
        enddo
        call indexx(NSubGr,NGrpA,PorGrpA)
        do i=1,NSubGr
          m=PorGrpA(i)
          do j=1,NSymm(KPhase)
            k=Sel(j,m)
            if(k.le.0) then
              jk=j-1
              go to 1770
            endif
          enddo
1770      write(46,FormA)
          write(46,'(''Kombinace:'',60i3)')(Sel(j,m),j=1,jk)
          write(46,'(''Subgroup :  '',a)') GrpA(m)(:idel(GrpA(m)))
          write(46,FormA)
        enddo
        deallocate(rm6s,s6s,ipri,BratSymmSub,vt6s,io,OpSmb,ios,OpSmbS,
     1             IswSymms,ZMagS,RMagS)
        deallocate(Sel)
1800    call CloseIfOpened(ln)
        if(Choice.lt.ListGroupN(n)) then
          Choice=Choice+1
          call CopyVek(ListGroupO(1,n),ShSg,3)
          go to 1100
        endif
      enddo
      close (46)
      deallocate(ListGroup,ListGroupN,ListGroupO)
      call CloseIfOpened(ln)
      return
      end
      subroutine TestComp
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Z3(9),Zd(9),PomMat(9),RelMat(3,3),RelMatI(3,3),xx(3),
     1          xf(3),xg(3)
      character*80 Veta
      call MatBlock3(zv(1,2,KPhase),Z3,NDim(KPhase))
      i=NDim(KPhase)*3+1
      do j=1,3*NDimI(KPhase),3
        do l=0,2
          call CopyVek(zv(i,2,KPhase),Zd(j),3)
        enddo
        i=i+NDim(KPhase)
      enddo
      call CopyMat(Z3,RelMat,3)
      call TrMat(Qu(1,1,1,KPhase),PomMat,3,NDimI(KPhase))
      call Cultm(Zd,PomMat,RelMat,3,NDimI(KPhase),3)
      call MatInv(RelMat,RelMatI,pom,3)
      call FeLstMake(0.,-1.,60,20,' ')
      do i=1,3
        write(Veta,'(3f10.4)')(RelMat(i,j),j=1,3)
        call FeLstWriteLine(Veta,-1)
      enddo
      do i=1,3
        call SetRealArrayTo(xx,3,0.)
        xx(i)=1.
        call Multm(MetTens(1,1,KPhase),xx,xg,3,3,1)
        do j=1,3
          call SetRealArrayTo(xx,3,0.)
          xx(j)=1.
          call Multm(RelMatI,xx,xf,3,3,1)
          pom=ScalMul(xf,xg)
          uhel=acos(pom/(CellPar(i,1,KPhase)*CellPar(j,2,KPhase)))/ToRad
          write(Veta,'(''uhel'',i1,''-'',i1,'':'',5f10.4)') i,j,uhel
          call FeLstWriteLine(Veta,-1)
        enddo
      enddo
5000  call FeEvent(0)
      if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1   (EventType.eq.EventMouse.and.
     2    (EventNumber.eq.JeLeftDown.or.EventNumber.eq.JeRightDown)))
     3  then

      else
        go to 5000
      endif
9999  return
      end
      subroutine MakeSpGroupAlias
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      character*8   GrupaR,GrupaA(16)
      character*2   t2(3,2)
      character*1   abc(3)
      dimension ip(3,3),ic(3),ia(3),iz(3)
      logical EqIgCase,EqIV0
      data abc/'a','b','c'/
      lni=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      lno=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup_alias_new.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_alias_new.dat'
      endif
      call OpenFile(lno,Veta,'formatted','unknown')
      read(lni,FormA256) Veta
      if(Veta(1:1).ne.'#') rewind lni
1200  read(lni,FormSG,end=9000) ig,ipg,idl,GrupaR
      if(ig.lt.16.or.EqIgCase(GrupaR(1:1),'P').or.
     1               EqIgCase(GrupaR(1:1),'F')) go to 1200
      if(EqIgCase(GrupaR,'I222').or.
     1   EqIgCase(GrupaR,'I212121')) go to 1200
      if(ig.gt.74) go to 9000
      call SetIntArrayTo(ic,3,1)
      if(EqIgCase(GrupaR(1:1),'A')) then
        ic(1)=0
      else if(EqIgCase(GrupaR(1:1),'B')) then
        ic(2)=0
      else if(EqIgCase(GrupaR(1:1),'C')) then
        ic(3)=0
      endif
      i=2
      k=1
1300  call SetIntArrayTo(ip(1,k),3,0)
      if(GrupaR(i:i).eq.'2') then
        iz(k)=1
        call SetIntArrayTo(ip(1,k),3,0)
        if(GrupaR(i+1:i+1).eq.'1') then
          i=i+1
          ip(k,k)=1
        endif
      else if(EqIgCase(GrupaR(i:i),'m')) then
        iz(k)=-1
      else if(EqIgCase(GrupaR(i:i),'a')) then
        iz(k)=-1
        ip(1,k)=1
      else if(EqIgCase(GrupaR(i:i),'b')) then
        iz(k)=-1
        ip(2,k)=1
      else if(EqIgCase(GrupaR(i:i),'c')) then
        iz(k)=-1
        ip(3,k)=1
      else if(EqIgCase(GrupaR(i:i),'n')) then
        iz(k)=-1
        call SetIntArrayTo(ip(1,k),3,1)
        ip(k,k)=0
      else if(EqIgCase(GrupaR(i:i),'e')) then
        iz(k)=-1
        call SetIntArrayTo(ip(1,k),3,1)
      endif
      if(k.lt.3) then
        i=i+1
        k=k+1
        go to 1300
      endif
      do i=1,3
        do j=1,2
          n=0
          do k=1,3
            n=n+ip(k,i)
          enddo
          if(n.lt.3) then
            if(j.eq.1) then
              call CopyVekI(ip(1,i),ia,3)
            else
              call AddVekI(ip(1,i),ic,ia,3)
            endif
            do k=1,3
              ia(k)=mod(ia(k),2)
              if((k.eq.i.and.iz(i).lt.0).or.
     1           (k.ne.i.and.iz(i).gt.0)) ia(k)=0
            enddo
          else
            call CopyVekI(ip(1,i),ia,3)
          endif
          if(iz(i).gt.0) then
            if(EqIV0(ia,3)) then
              t2(i,j)='2'
            else
              t2(i,j)='21'
            endif
          else
            NNenul=0
            KdeNenul=0
            do k=1,3
              if(ia(k).ne.0) then
                NNenul=NNenul+1
                KdeNenul=k
              endif
            enddo
            if(NNenul.eq.0) then
              t2(i,j)='m'
            else if(NNenul.eq.1) then
              t2(i,j)=abc(KdeNenul)
            else if(NNenul.eq.2) then
              t2(i,j)='n'
            else if(NNenul.eq.3) then
              t2(i,j)='e'
            endif
          endif
        enddo
      enddo
      n=0
      do i1=1,2
        do i2=1,2
          do 2000i3=1,2
            Veta=GrupaR(1:1)//t2(1,i1)//t2(2,i2)//t2(3,i3)
            call Zhusti(Veta)
            if(EqIgCase(Veta,GrupaR)) go to 2000
            do k=1,n
              if(EqIgCase(Veta,GrupaA(k))) go to 2000
            enddo
            write(lno,'(a8,1x,a8)') Veta(1:8),GrupaR
            n=n+1
            GrupaA(n)=Veta
2000      continue
        enddo
      enddo
      go to 1200
9000  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      return
      end
      subroutine UdelejSeznamGrup
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call FeLstMake(-1.,-1.,65,30,' ')
      call OpenFile(45,Veta,'formatted','old')
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'seznam.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/seznam.dat'
      endif
      call OpenFile(46,Veta,'formatted','unknown')
      read(45,FormA256) Veta
      if(Veta(1:1).ne.'#') rewind 45
      n=1
1100  read(45,FormA256,end=5000) Veta
      read(Veta,FormSG) isg,ipg,i,Grupa(KPhase)
      if(isg.eq.n) then
        write(Cislo,'(''('',i3,'')'')') n
        call Zhusti(Cislo)
        Veta='      ListGroup'//Cislo(:idel(Cislo))//'='''//

     1       Grupa(KPhase)(:idel(Grupa))//''''
        write(46,'(a)') Veta(:idel(Veta))
        n=n+1
      endif
      go to 1100
5000  return
      end
      subroutine TestTypDist
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*20 dist(2)
      character*2 At1(2),At2(2)
      logical EqIgCase
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1              'typical_distances.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'bondval/typical_distances.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      n=1
1000  rewind ln
      do i=1,n
        read(ln,FormA256,end=9999) t256
      enddo
1100  read(ln,FormA256) t256
      k=0
      do i=1,2
        call kus(t256,k,At1(i))
      enddo
      call kus(t256,k,dist(1))
      n=n+1
1200  read(ln,FormA256,end=2000) t256
      k=0
      do i=1,2
        call kus(t256,k,At2(i))
      enddo
      call kus(t256,k,dist(2))
      if((EqIgCase(At1(1),At2(1)).and.EqIgCase(At1(2),At2(2))).or.
     1   (EqIgCase(At1(1),At2(2)).and.EqIgCase(At1(2),At2(1)))) then
        pause
      else
        go to 1200
      endif
2000  go to 1000
9999  return
      end
