      subroutine ConMakeEntFile
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      n=0
      do i=1,3
        xmin(i)=-ShiftPlane(i)
      enddo
      call OpenFile(89,fln(1:ifln)//'.ent','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do 1510j=1,npdf
        k=ipor(j)
        if(fpdf(k).le.0.) go to 1510
        n=n+1
        ia=iapdf(k)
        i=isf(ia)
        t80='ATOM'
        l=idel(AtType(i,KPhase))
        write(t80(7:11),'(i5)') n
        t80=t80(1:14-l)//AtType(i,KPhase)(:l)
        write(t80(23:26),'(i4)') n
        call prevod(0,xpdf(1,k),ypdf(1,k))
        write(t80(31:66),'(3f8.4,2f6.3)')(ypdf(l,k),l=1,3),fpdf(k),1.
        write(89,FormA1)(t80(l:l),l=1,idel(t80))
        write(89,'(''REMARK    '',a8,3f9.6)') atom(ia),(xpdf(l,k),
     1        l=1,3)
1510  continue
      call ConMakeConnect
9999  call CloseIfOpened(89)
      return
      end
