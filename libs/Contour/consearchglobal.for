      subroutine ConSearchGlobal
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),SaveMap(:)
      character*80 Veta,t80
      character*11 :: FileNameTmp(2)=(/'_maxima.tmp','_minima.tmp'/)
      character*8 at
      character*6 :: MinMaxLabel(2)=(/'maxima','minima'/)
      logical UzPsal
      allocatable SaveMap
      allocate(SaveMap(nxny))
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyVek(ActualMap,SaveMap,nxny)
      call DefaultFourier
      call ConFouPeaks(m8)
      if(ErrFlag.ne.0) then
        write(Cislo,FormI15) m8
        call Zhusti(Cislo)
        call OpenMaps(m8,fln(:ifln)//'.l'//Cislo(:idel(Cislo)),nxny,0)
        go to 9999
      endif
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      ln=NextLogicNumber()
      do i=1,2
        UzPsal=.false.
        if(i.eq.1) then
          t80='Fourier maxima'
        else
          t80='Fourier minima'
        endif
        call OpenFile(ln,fln(:ifln)//FileNameTmp(i),'formatted',
     1                'unknown')
1100    read(m40,FormA80,end=1250) Veta
        if(Veta(1:10).ne.'----------'.or.
     1    LocateSubstring(Veta,t80(:idel(t80)),.false.,.true.).le.0)
     2    go to 1100
        read(m40,FormA,end=1250) Veta
1200    read(m40,FormA) Veta
        if(Veta(1:10).eq.'----------') go to 1250
        read(Veta,101,err=1250,end=1250) At,isfp,itfp,aip,xo,(l,k=1,5)
        do j=1,l+1
          read(m40,FormA) Veta
        enddo
        if(LocateSubstring(Veta,'e',.false.,.true.).gt.0) then
          read(Veta,'(2e9.3)',err=1250,end=1250) rho,rho
        else
          read(Veta,'(2f9.3)',err=1250,end=1250) rho,rho
        endif
        if(Obecny) then
          call Prevod(1,xo,xp)
        else
          call CopyVek(xo,xp,3)
          call Prevod(0,xp,xo)
        endif
        write(ln,103) xo,xp,rho
        UzPsal=.true.
        go to 1200
1250    if(.not.UzPsal) then
          Veta=MinMaxLabel(i)
          write(ln,'(''No '',a,'' found in whole region'')')
     1      Veta(:idel(Veta))
        endif
        call CloseIfOpened(ln)
        rewind m40
      enddo
      call ConShowMaxMin(FileNameTmp)
9999  call CloseIfOpened(m40)
      call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      call CopyVek(SaveMap,SaveMap,nxny)
      call DefaultBasicContour
      deallocate(SaveMap)
      return
100   format(i5,15x,i5)
101   format(a8,2i3,4x,4f9.6,6x,3i1,3i3)
102   format(a9)
103   format(3f8.3,2x,3f8.4,f10.2)
      end
