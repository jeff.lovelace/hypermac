      subroutine ConReadDenLimits(pmn,pmx,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*40 Veta
      id=NextQuestId()
      xqd=180.
      call FeQuestCreate(id,-1.,-1.,xqd,2,'Limits for the density '//
     1                   'curve',0,LightGray,0,0)
      Veta='D(m%in)'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      dpom=100.
      il=1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,pmn,.false.,.false.)
      il=il+1
      Veta='D(m%ax)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,pmx,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwMin,pmn)
        call FeQuestRealFromEdw(nEdwMax,pmx)
      endif
      call FeQuestRemove(id)
      return
      end
