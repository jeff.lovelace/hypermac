      subroutine ConFillAtoms(ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      character*80  Veta
      dimension xp(3),xpp(3),ddx(3),gddx(3),io(3)
      integer IAtomFill(:),SbwLnQuest,RolMenuSelectedQuest,
     1        SbwItemSelQuest
      logical BratAtomFill(:),lpom,EqWild,BackToAdvanced
      external ConFillAtomsCheck,FeVoid
      allocatable BratAtomFill,IAtomFill
      CutOffDistOld=CutOffDist
      if(Obecny) then
        ir=2
        do i=1,3
          io(i)=i
        enddo
      else
        ir=1
        call CopyVekI(iorien,io,3)
      endif
      dpom=xmaxa(3,ir)-xmina(3,ir)
      if(ir.eq.1) then
        j=1+4*(io(3)-1)
        dpom=dpom*sqrt(MetTens(j,nsubs,KPhase))
      endif
      if(ConDrawDepth.le.0.) then
        if(dpom.lt..2) then
          Depth=dpom+1.
        else
          Depth=0.
        endif
      else
        Depth=ConDrawDepth
      endif
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      BackToAdvanced=.false.
      id=NextQuestId()
      xqd=650.
      Veta='Select atoms to be filled out'
      il=14
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=11
      ild=10
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      SbwRightClickAllowed=.true.
      nSbwSel=SbwLastMade
      il=il+2
      dpom=120.
      xpom=.5*xqd-dpom-20.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=.5*xqd-dpom-20.
      il=il+1
      Veta='Select a%dvanced'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdvanced=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,1)
      nEdwInclude=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
      enddo
      do i=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
      enddo
      call CloseIfOpened(ln)
      allocate(BratAtomFill(NAtActive),IAtomFill(NAtActive))
      call SetLogicalArrayTo(BratAtomFill,NAtActive,.true.)
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        NAtActive=NAtActive+1
        IAtomFill(NAtActive)=i
      enddo
      do i=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
        NAtActive=NAtActive+1
        IAtomFill(NAtActive)=i
      enddo
1400  call CloseIfOpened(SbwLnQuest(nSbwSel))
      do i=1,NAtActive
        if(BratAtomFill(i)) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSel,j)
      enddo
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_atoms.tmp')
1500  if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      call FeQuestEvent(id,ich)
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(BratAtomFill,NAtActive,lpom)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        idp=NextQuestId()
        xqdp=300.
        il=6
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,2
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                                NAtFormula(KPhase),0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,ConFillAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=1,NAtActive
            if(EqWild(Atom(IAtomFill(i)),Veta,.false.))
     1        BratAtomFill(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
          isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=1,NAtActive
            if(isf(IAtomFill(i)).eq.isfp) BratAtomFill(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1400
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=1,NAtActive
          if(EqWild(Atom(i),Veta,.false.))
     1      BratAtomFill(i)=.true.
        enddo
        call FeQuestStringEdwOpen(nEdwInclude,' ')
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nButtAdvanced)
     1  then
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NAtActive
          BratAtomFill(i)=SbwItemSelQuest(i,nSbwSel).eq.1
        enddo
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      if(ich.ne.0) go to 9999
      CutOffDist=Depth
      call SetLogicalArrayTo(AtBrat,NAtCalc,.false.)
      do i=1,NAtActive
        AtBrat(i)=BratAtomFill(IAtomFill(i))
      enddo
      if(ncomp(1).le.1) nsubs=1
      do i=1,3
        l=io(i)
        xpp(l)=(xmaxa(i,ir)+xmina(i,ir))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddx(io(1))=(xmaxa(1,ir)-xpp(io(1)))*float(i1)
        do i2=-1,1,2
          ddx(io(2))=(xmaxa(2,ir)-xpp(io(2)))*float(i2)
          do i3=-1,1,2
            ddx(io(3))=(xmaxa(3,ir)-xpp(io(3)))*float(i3)
            if(obecny) then
              dd=sqrt(ddx(1)**2+ddx(2)**2+ddx(3)**2)
            else
              call multm(MetTens(1,nsubs,KPhase),ddx,gddx,3,3,1)
              dd=sqrt(scalmul(ddx,gddx))
            endif
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      if(ir.eq.2) then
        call prevod(1,xpp,xp)
      else
        call CopyVek(xpp,xp,3)
      endif
      call OkoliC(xp,dmez,ir,NSubs,.true.)
      DrawAtN=0
      do 2000i=1,npdf
        j=ipor(i)
        if(ir.eq.2) then
          call prevod(0,xpdf(1,j),xp)
        else
          call CopyVek(xpdf(1,j),xp,3)
        endif
        kk=0
        do k=1,2
          l=io(k)
          if(xp(l).lt.xmina(k,ir).or.
     1       xp(l).gt.xmaxa(k,ir)) go to 2000
        enddo
        l=io(3)
        if(xp(l).lt.xmina(3,ir)-Depth.or.
     1     xp(l).gt.xmaxa(3,ir)+Depth) cycle
        if(DrawAtN.lt.NMaxDraw) then
          DrawAtN=DrawAtN+1
          Veta=atom(IAPdf(j))
          k=index(ScPdf(j),'#')
          if(k.gt.0) Veta=Veta(:idel(Veta))//ScPdf(j)(k:)
          DrawAtName(DrawAtN)=Veta
          DrawAtColor(DrawAtN)=0
          DrawAtSkip(DrawAtN)=.false.
          DrawAtBondLim(DrawAtN)=2.
        endif
2000  continue
      ConPtatSe=.false.
      call ConWriteKeys
      call DefaultContour
      call NactiContour
      call ConReadKeys
      AtomsOn=.true.
      ConPtatSe=.true.
      call KresliMapu(0)
9999  if(allocated(BratAtomFill)) deallocate(BratAtomFill,IAtomFill)
      CutOffDist=CutOffDistOld
      return
      end
