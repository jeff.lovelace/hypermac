      subroutine rezt(tmin,tmax,dt)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(3),x4(1),tt(3),nn(4),nn0(4),nn1(4),tmin(3),tmax(3),
     1          dt(3),nt(3),nd(3),
     2          dtp(:,:),irez(:),tbl(:,:),table(:)
      integer RecPack
      allocatable irez,dtp,tbl,table
      equivalence (xx(1),x),(xx(2),y),(xx(3),z),(x4(1),x4p)
      allocate(irez(nxny),tbl(nxny,2**NDimI(KPhase)),dtp(3,nxny),
     1         table(nxny))
      ntt=1
      do i=1,NDimI(KPhase)
        nt(i)=nint((tmax(i)-tmin(i))/dt(i))+1
        ntt=ntt*nt(i)
      enddo
      nmap=nx(3)*ntt
      call FeFlowChartOpen(-1.,-1.,1,nmap,
     1                     'Transformation x4 maps to t maps',' ',' ')
      call CloseIfOpened(84)
      call OpenMaps(84,fln(:ifln)//'.l84',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      irec=0
      do it=1,ntt
        call RecUnpack(it,nd,nt,NDimI(KPhase))
        do i=1,NDimI(KPhase)
          tt(i)=tmin(i)+(nd(i)-1)*dt(i)
        enddo
        if(.not.obecny) z=xmin(3)
        do iz=1,nx(3)
          if(.not.obecny) y=xmin(2)
          i=0
          do iy=1,nx(2)
            if(.not.obecny) x=xmin(1)
            do ix=1,nx(1)
              i=i+1
              if(obecny) then
                do j=1,3
                  xx(j)=xob(j)+xr(j)*float(ix-1)+yr(j)*float(iy-1)+
     1                         zr(j)*float(iz-1)
                enddo
              endif
              nn(1)=iz
              do j=1,NDimI(KPhase)
                jj=j+3
                x4p=tt(j)
                do k=1,3
                  if(Obecny) then
                    kk=k
                  else
                    kk=iorien(k)
                  endif
                  x4p=x4p+qu(kk,j,nsubs,KPhase)*xx(k)
                enddo
                call od0do1(x4,x4,1)
                dtpp=(x4p-xmin(jj))/dx(jj)+1.
                k=ifix(dtpp)
                dtp(j,i)=dtpp-float(k)
                nn(j+1)=k
                if(k.gt.nx(jj)) go to 9000
              enddo
              irez(i)=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              if(.not.obecny) x=x+dx(1)
            enddo
            if(.not.obecny) y=y+dx(2)
          enddo
          do i=1,nxny
            ir=irez(i)
            if(ir.eq.0) cycle
            read(m8,rec=ir,err=9100)(tbl(j,1),j=1,nxny)
            call RecUnpack(ir-1,nn0,nx(3),NDim(KPhase)-2)
            nn1(1)=nn0(1)
            do j=1,NDimI(KPhase)
              jj=j+3
              nn1(j+1)=nn0(j+1)+1
              if(nn1(j+1).gt.nx(jj)) nn1(j+1)=1
            enddo
            call CopyVekI(nn0,nn,NDim(KPhase)-2)
            nn(2)=nn1(2)
            k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
            read(m8,rec=k,err=9100)(tbl(j,2),j=1,nxny)
            if(NDim(KPhase).gt.4) then
              nn(2)=nn0(2)
              nn(3)=nn1(3)
              k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              read(m8,rec=k,err=9100)(tbl(j,3),j=1,nxny)
              nn(2)=nn1(2)
              k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
              read(m8,rec=k,err=9100)(tbl(j,4),j=1,nxny)
              if(NDim(KPhase).gt.5) then
                nn(2)=nn0(2)
                nn(3)=nn0(3)
                nn(4)=nn1(4)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,5),j=1,nxny)
                nn(2)=nn1(2)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,6),j=1,nxny)
                nn(2)=nn0(2)
                nn(3)=nn1(3)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,7),j=1,nxny)
                nn(2)=nn1(2)
                k=RecPack(nn,nx(3),NDim(KPhase)-2)+1
                read(m8,rec=k,err=9100)(tbl(j,8),j=1,nxny)
              endif
            endif
            do j=1,nxny
              if(irez(j).ne.ir) cycle
              if(NDimI(KPhase).eq.1) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),pom,pom,
     2                               pom,pom,pom,pom)
              else if(NDimI(KPhase).eq.2) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),tbl(j,3),
     2                               tbl(j,4),
     3                               pom,pom,pom,pom)
              else if(NDimI(KPhase).eq.3) then
                table(j)=ExtrapolLin(dtp(1,j),NDimI(KPhase),
     1                               tbl(j,1),tbl(j,2),tbl(j,3),
     2                               tbl(j,4),tbl(j,5),tbl(j,6),
     3                               tbl(j,7),tbl(j,8))
              endif
              Dmax=max(table(j),Dmax)
              Dmin=min(table(j),Dmin)
              irez(j)=0
            enddo
          enddo
          call FeFlowChartEvent(irec,is)
          if(is.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9900
          endif
          write(84,rec=irec+1)(table(i),i=1,nxny)
          z=z+dx(3)
        enddo
      enddo
      irec=irec+2
      write(84,rec=irec) Dmax,Dmin
      do i=4,NDim(KPhase)
        j=i-3
        nx(i)=nd(j)
        xmin(i)=tmin(j)
        xmax(i)=tmin(j)+(nd(j)-1)*dt(j)
        dx(i)=dt(j)
      enddo
      write(84,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                iorien,mapa,nsubs,SatelityByly,
     2                nonModulated(KPhase)
      do i=4,6
        cx(i)=smbt(i-3)
      enddo
      call SetContours(0)
      do i=3,6
        nxfrom(i-2)=1
        nxto(i-2)=nx(i)
      enddo
      nxdraw(1)=0
      do i=2,4
        nxdraw(i)=1
      enddo
      irecold=-1
      m8=84
      go to 9999
9000  call FeChybne(-1.,YBottomMessage,'Fourier maps don''t allow to '//
     1              'extrapolate all points',' ',SeriousError)
      ErrFlag=1
      go to 9900
9100  call FeReadError(m8)
      ErrFlag=1
9900  call DeleteFile(fln(:ifln)//'.l84')
9999  call FeFlowChartRemove
      deallocate(irez,tbl,dtp,table)
      return
      end
