      function ExtrapolLin(xd,n,f000,f100,f010,f110,f001,f101,f011,f111)
      dimension xd(n)
      f00=(f100-f000)*xd(1)+f000
      if(n.gt.1) then
        f10=(f110-f010)*xd(1)+f010
        f0=(f10-f00)*xd(2)+f00
        if(n.gt.2) then
          f01=(f101-f001)*xd(1)+f001
          f11=(f111-f011)*xd(1)+f011
          f1=(f11-f01)*xd(2)+f01
          ExtrapolLin=(f1-f0)*xd(3)+f0
        else
          ExtrapolLin=f0
        endif
      else
        ExtrapolLin=f00
      endif
      return
      end
