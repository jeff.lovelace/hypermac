      subroutine DefLength(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Veta
      id=NextQuestId()
      xqd=200.
      call FeQuestCreate(id,-1.,-1.,xqd,NDimI(KPhase),' ',0,LightGray,
     1                   0,0)
      tpom=5.
      Veta='Length of %'//'X'//'th vector'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-10.-xpom
      do i=1,NDimI(KPhase)
        write(Veta(12:12),'(i1)') i+3
        call FeQuestEdwMake(id,tpom,i,xpom,i,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(i,ee(i),.false.,.false.)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NDimI(KPhase)
          call FeQuestRealFromEdw(i,ee(i))
        enddo
        call SetTr
      endif
      call FeQuestRemove(id)
      return
      end
