      subroutine OkoliC(x1,dmez,ir,isw,vyber)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x0(6),x1(3),y1(6),y2(3),u(3),ug(3),shj(3),
     1          uz(3),shjc(3),jsh(3),xp(3)
      character*40 t40
      logical vyber
      npdf=0
      call bunka(x1,x0,0)
      jcellxm=anint(dmez*rcp(1,nsubs,KPhase))+1
      jcellym=anint(dmez*rcp(2,nsubs,KPhase))+1
      jcellzm=anint(dmez*rcp(3,nsubs,KPhase))+1
      do j=1,NAtCalc
        if(iswa(j).ne.isw.or.kswa(j).ne.KPhase.or.
     1     (.not.atbrat(j).and.vyber)) cycle
        do l=1,NDim(KPhase)
          if(l.le.3) then
            x0(l)=x(l,j)
          else
            x0(l)=0.
          endif
        enddo
        js=0
        do jsym=1,NSymmN(KPhase)
          js=js+1
          if(isa(js,j).le.0) cycle
          call multm(rm6(1,jsym,nsubs,KPhase),x0,y1,NDim(KPhase),
     1               NDim(KPhase),1)
          do l=1,3
            y1(l)=y1(l)+s6(l,jsym,nsubs,KPhase)
          enddo
          do jcenter=1,NLattVec(KPhase)
            do l=1,3
              y2(l)=y1(l)+vt6(l,jcenter,nsubs,KPhase)
            enddo
            call bunka(y2,shj,1)
            do l=1,3
              uz(l)=y2(l)-x1(l)
            enddo
            do jcellx=-jcellxm,jcellxm
              cellxj=jcellx
              pom=uz(1)+cellxj
              jsh(1)=nint(shj(1))+jcellx
              shjc(1)=shj(1)+cellxj+s6(1,jsym,nsubs,KPhase)
     1                             +vt6(1,jcenter,nsubs,KPhase)
              u(1)=pom
              do jcelly=-jcellym,jcellym
                cellyj=jcelly
                pom=uz(2)+cellyj
                u(2)=pom
                jsh(2)=nint(shj(2))+jcelly
                shjc(2)=shj(2)+cellyj+s6(2,jsym,nsubs,KPhase)
     1                               +vt6(2,jcenter,nsubs,KPhase)
                do 1500jcellz=-jcellzm,jcellzm
                  cellzj=jcellz
                  pom=uz(3)+cellzj
                  u(3)=pom
                  jsh(3)=nint(shj(3))+jcellz
                  shjc(3)=shj(3)+cellzj+s6(3,jsym,nsubs,KPhase)
     1                                 +vt6(3,jcenter,nsubs,KPhase)
                  call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
                  d=sqrt(scalmul(u,ug))
                  if(d.ge.dmez) go to 1500
                  m=d*1000.
                  call ReallocatePDF(npdf+1)
                  npdf=npdf+1
                  do k=1,3
                    xpdf(k,npdf)=u(k)+x1(k)
                  enddo
                  if(ir.ne.0) then
                    if(ir.eq.2) then
                      call prevod(0,xpdf(1,npdf),xp)
                    else
                      call CopyVek(xpdf(1,npdf),xp,3)
                    endif
                    do k=1,3
                      if(ir.eq.1) then
                        l=iorien(k)
                      else
                        l=k
                      endif
                      if(xp(l).lt.xmina(k,ir)-CutOffDist.or.
     1                   xp(l).gt.xmaxa(k,ir)+CutOffDist) then
                        npdf=npdf-1
                        go to 1500
                      endif
                    enddo
                  endif
                  idpdf(npdf)=m
                  iapdf(npdf)=j
                  ispdf(npdf)=jsym
                  popaspdf(1:64,npdf)=0.
                  call scode(jsym,jcenter,shjc,jsh,1,scpdf(npdf),t40)
                  k=idel(t40)
                  if(k.gt.0)
     1              scpdf(npdf)=scpdf(npdf)(:idel(scpdf(npdf)))//'#'//
     2                          t40(:k)
1500            continue
              enddo
            enddo
          enddo
        enddo
      enddo
      do i=1,npdf
        dpdf(i)=float(idpdf(i))/1000.
      enddo
      call indexx(npdf,idpdf,ipor)
      return
      end
