      subroutine DefContour(ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 ContourOld,EdwStringQuest
      character*80 Veta
      character*9  frm
      integer EdwStateQuest,Exponent10
      logical CrwLogicQuest
      data ContourOld/' '/,frm/'(f15. 6)'/
      xqd=400.
      il=9
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Contour parameters',1,
     1                   LightGray,0,0)
      k=1
      do i=1,2
        if(i.eq.1) then
          pom=Dmin
        else
          pom=Dmax
        endif
        pom=pom+ContourRefLevel
        if(abs(pom).gt.0.) then
          j=max(6-Exponent10(pom),0)
          j=min(j,6)
        else
          j=6
        endif
        write(frm(6:7),'(i2)') j
        write(Veta(k:),frm) pom
        k=k+15
      enddo
      call ZdrcniCisla(Veta,2)
      Veta='Extremals of the map(s) : '//Veta(:idel(Veta))
      il=1
      call FeQuestLblMake(id,5.,il,Veta,'L','N')
      xpom=50.
      il=il+1
      Veta='%Uniform contours'
      do i=1,3
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) then
          nCrwUniform=CrwLastMade
          Veta='%Explicit contours'
        else if(i.eq.2) then
          nCrwExplicit=CrwLastMade
          Veta='AI%M contours'
        else if(i.eq.3) then
          nCrwAIM=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,ContourType.eq.i-1)
        xpom=xpom+140.
      enddo
      il=il+1
      tpom=xqd*.5-20.
      Veta='%Positive contours'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=100.
      do i=1,4
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwPosCon=EdwLastMade
          Veta='%Negative contours'
        else if(i.eq.2) then
          nEdwNegCon=EdwLastMade
          Veta='Po%sitive cutoff'
        else if(i.eq.3) then
          nEdwPosCut=EdwLastMade
          Veta='Ne%gative cutoff'
        else if(i.eq.4) then
          nEdwNegCut=EdwLastMade
        endif
      enddo
      tpom=5.
      Veta='Draw pos%itive'
      il=il-4
      xpom=tpom+FeTxLengthUnder(Veta(:idel(Veta))//'XX')+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        if(i.eq.1) then
          nCrwDrawPos=CrwLastMade
          Veta='Draw neg%ative'
          j=DrawPos
        else if(i.eq.2) then
          nCrwDrawNeg=CrwLastMade
          j=DrawNeg
        endif
        call FeQuestCrwOpen(CrwLastMade,j.gt.0)
      enddo
      Veta='Dra%w contours:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.
      il=il+3
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawInd=EdwLastMade
      il=il+1
      Veta='Start AIM contours with decimal order:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawAIM=EdwLastMade
1300  if(EdwStateQuest(nEdwDrawInd).eq.EdwOpened) then
        ContourOld=EdwStringQuest(nEdwDrawInd)
      else if(EdwStateQuest(nEdwDrawAIM).eq.EdwOpened) then
        call FeQuestIntFromEdw(nEdwDrawAIM,ContourAIMOrder)
      endif
      if(ContourType.eq.0) then
        call FeQuestEdwDisable(nEdwDrawInd)
        call FeQuestEdwDisable(nEdwDrawAIM)
        call FeQuestCrwOpen(nCrwDrawPos,DrawPos.gt.0)
        call FeQuestCrwOpen(nCrwDrawNeg,DrawNeg.gt.0)
        if(DrawPos.gt.0) then
          if(EdwStateQuest(nEdwPosCon).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwPosCon,fmzp,.false.,.false.)
            call FeQuestRealEdwOpen(nEdwPosCut,cutpos+ContourRefLevel,
     1                              .false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwPosCon)
          call FeQuestEdwDisable(nEdwPosCut)
        endif
        if(DrawNeg.gt.0) then
          if(EdwStateQuest(nEdwNegCon).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwNegCon,fmzn,.false.,.false.)
            call FeQuestRealEdwOpen(nEdwNegCut,cutneg+ContourRefLevel,
     1                              .false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwNegCon)
          call FeQuestEdwDisable(nEdwNegCut)
        endif
      else if(ContourType.eq.ContourTypeUser.or.
     1        ContourType.eq.ContourTypeAIM) then
        call FeQuestEdwDisable(nEdwPosCon)
        call FeQuestEdwDisable(nEdwPosCut)
        call FeQuestEdwDisable(nEdwNegCon)
        call FeQuestEdwDisable(nEdwNegCut)
        if(ContourType.eq.ContourTypeUser) then
          call FeQuestEdwDisable(nEdwDrawAIM)
          call FeQuestStringEdwOpen(nEdwDrawInd,ContourOld)
          call FeQuestCrwDisable(nCrwDrawPos)
          call FeQuestCrwDisable(nCrwDrawNeg)
        else if(ContourType.eq.ContourTypeAIM) then
          call FeQuestEdwDisable(nEdwDrawInd)
          call FeQuestIntEdwOpen(nEdwDrawAIM,ContourAIMOrder,.false.)
          call FeQuestCrwOpen(nCrwDrawPos,DrawPos.gt.0)
          call FeQuestCrwOpen(nCrwDrawNeg,DrawNeg.gt.0)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        if(ContourType.eq.ContourTypeUser) then
          if(allocated(ContourArray)) deallocate(ContourArray)
          ContourOld=EdwStringQuest(nEdwDrawInd)
          if(ContourOld.eq.' ') go to 1710
          do i=1,2
            ContourNumber=0
            k=0
1600        call kus(ContourOld,k,Cislo)
            ContourNumber=ContourNumber+1
            pom=fract(Cislo,ich)
            if(ich.ne.0) go to 1700
            if(i.eq.2) ContourArray(ContourNumber)=pom
            if(k.ge.len(ContourOld)) then
              go to 1610
            else
              go to 1600
            endif
1610        if(i.eq.1) allocate(ContourArray(ContourNumber))
          enddo
          QuestCheck(id)=0
          go to 1500
1700      call FeChybne(-1.,-1.,'inacceptable real number "'//
     1                  Cislo(:idel(Cislo))//'"','try again.',
     2                  SeriousError)
          go to 1500
1710      call FeChybne(-1.,-1.,'the string is empty','try again.',
     1                  SeriousError)
          go to 1500
        endif
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwUniform.or.
     2         CheckNumber.eq.nCrwExplicit.or.
     3         CheckNumber.eq.nCrwAIM)) then
        if(CrwLogicQuest(nCrwUniform)) then
          ContourType=ContourTypeLinear
        else if(CrwLogicQuest(nCrwExplicit)) then
          ContourType=ContourTypeUser
        else
          ContourType=ContourTypeAIM
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawPos) then
        if(CrwLogicQuest(CheckNumber)) then
          DrawPos=1
        else
          DrawPos=0
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawNeg) then
        if(CrwLogicQuest(CheckNumber)) then
          DrawNeg=1
        else
          DrawNeg=0
        endif
        go to 1300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  if(ich.eq.0) then
        if(ContourType.eq.ContourTypeLinear) then
          if(CrwLogicQuest(nCrwDrawPos)) then
            call FeQuestRealFromEdw(nEdwPosCon,fmzp)
            call FeQuestRealFromEdw(nEdwPosCut,cutpos)
            cutpos=cutpos-ContourRefLevel
            DrawPos=1
          else
            DrawPos=0
          endif
          if(CrwLogicQuest(nCrwDrawNeg)) then
            call FeQuestRealFromEdw(nEdwNegCon,fmzn)
            call FeQuestRealFromEdw(nEdwNegCut,cutneg)
            cutneg=cutneg-ContourRefLevel
            DrawNeg=1
          else
            DrawNeg=0
          endif
        else if(ContourType.eq.ContourTypeAIM) then
          call FeQuestIntFromEdw(nEdwDrawAIM,ContourAIMOrder)
        else
          call ZdrcniCisla(ContourOld,ContourNumber)
        endif
      else
        ContourOld=' '
      endif
      call FeQuestRemove(id)
      return
      end
