      subroutine ConPrelim
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical exist81,first,ExistFile
      data first/.true./
      ConImageFile=fln(:ifln)//'_map.bmp'
      VolaToContour=.true.
      NewAtoms=.false.
      NewPoints=.false.
      ModifiedAtoms=.false.
      SatelityByly=NDim(KPhase).gt.3
      ContourRefLevel=0.
      call UnitMat(tm3,3)
      call UnitMat(tm,NDim(KPhase))
      M81Cteno=.false.
      ReverseX4=.false.
      if(First) then
        call SetRealArrayTo(ee,3,2.)
        First=.false.
      endif
      LabelPlaneNew=' '
      call DefaultContour
      call NactiContour
      if(ErrFlag.ne.0) then
        ErrFlag=0
        call DefaultContour
      endif
      DensityType=0
      OldNorm=.false.
      xymap =.true.
      xyzmap=.true.
      mcmax=0
      errlev=5.
      teplota=DatCollTemp(KDatBlock)
      ErrMapActive=.false.
      MaxRightBut=9
      toev=0.86170296e-1
      tmapy=.false.
      DelayTimeForMovie=.1
      MovieRepeat=0
      if(LocateSubstring(Call3dMaps,'mce.exe',.false.,.true.).gt.1)
     1  then
        IdCall3dMaps=IdCall3dMCE
      else if(LocateSubstring(Call3dMaps,'Vesta.exe',.false.,.true.)
     1       .gt.1) then
        IdCall3dMaps=IdCall3dVesta
      else if(LocateSubstring(Call3dMaps,'MoleCoolQt.exe',.false.,
     1                        .true.).gt.1) then
        IdCall3dMaps=IdCall3dMoleCoolQt
      else
        IdCall3dMaps=IdCall3dNone
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      KPhase=KPhaseBasic
      do i=1,6
        if(i.le.NDim(KPhase)) then
          iorien(i)=i
        else
          iorien(i)=0
        endif
      enddo
      Exist81=ExistFile(fln(:ifln)//'.m81')
1020  if(Exist81) then
        call CloseIfOpened(81)
        call OpenMaps(81,fln(:ifln)//'.m81',nxny,0)
        if(ErrFlag.ne.0) go to 1030
        read(81,rec=1,err=1030) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          iorien,mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
        kpdf1=0
        KPh=nsubs/10+1
        if(KPh.ne.KPhase) go to 1030
        nsubs=mod(nsubs,10)
        xymap =iorien(1).le.3.and.iorien(2).le.3
        xyzmap=xymap.and.iorien(3).le.3
        if(nx(1).lt.3.or.nx(2).lt.3) then
          call FeChybne(-1.,-1.,'the maps are too narrow to be drawn',
     1                  'you may have chosen an incorrect orientation.',
     2                  SeriousError)
          ErrFlag=1
          go to 9999
        endif
        go to 1120
1030    Exist81=.false.
        call CloseIfOpened(81)
        go to 1020
      else
        kpdf1=2
        nsubs=1
      endif
1120  call zmcmlt(0)
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
      kpdf2=7
      if(NDimI(KPhase).gt.0) call trortho(0)
      DrawAtN=0
      AtomsOn=.false.
      call SetStringArrayTo(DrawAtName,NMaxDraw,' ')
      LabelMapy='(.(a2,''='',f6.3,'',''))'
      write(LabelMapy(2:2),'(i1)') NDim(KPhase)-2
      if(ChargeDensities) call ConSetLocDensity
      call ConHeader
9999  return
      end
