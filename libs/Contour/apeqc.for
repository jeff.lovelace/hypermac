      subroutine ApeqC(kterou)
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      if(npa(kterou).le.0) return
      pom=pab(kterou)
      do k=1,npa(kterou)
        call KdoCo(pnp(k,kterou),at,pn,1,p,sp)
        pom=pom+p*pko(k,kterou)
      enddo
      sp=0.
      call KdoCo(lnp(kterou),at,pn,-1,pom,sp)
      return
      end
