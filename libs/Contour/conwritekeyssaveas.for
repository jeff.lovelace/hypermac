      subroutine ConWriteKeysSaveAs(Key,FileSaveAs)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) FileSaveAs
      character*256 EdwStringQuest
      character*80 Veta
      character*20 LabelSaveAs
      logical CrwLogicQuest,FeYesNo
      write(LabelSaveAs,'(''Plane#'',i2)') NumberOfPlanes+1
      call Zhusti(LabelSaveAs)
      if(.not.ConPtatSe) then
        if(Key.le.1) then
          if(LabelPlaneNew.ne.' ') then
            FileSaveAs=LabelPlaneNew
          else if(IPlane.gt.0) then
            FileSaveAs=LabelPlane(IPlane)
          else
            FileSaveAs=' '
          endif
        else if(Key.eq.2) then
          FileSaveAs='###NeniObecna###'
        else if(Key.eq.3) then
          FileSaveAs='###JenBasic###'
        endif
        go to 9999
      endif
      id=NextQuestId()
      if(Key.eq.0) then
        Veta='A new plane has been drawn.'
      else if(Key.eq.1) then
        Veta='The plane has been modified.'
      else if(Key.eq.2) then
        Veta='Set of drawn atoms has been modified.'
      else if(Key.eq.3) then
        Veta='Basic contour keys have been modified.'
      endif
      Veta=Veta(:idel(Veta))//' Do you want to:'
      xqd=FeTxLength(Veta)+20.
      il=3
      if(Key.eq.1) il=il+1
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
      il=1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      xpom=5.
      tpom=xpom+CrwgXd+5.
      j=0
      nCrwSaveAs=0
      Veta='%discard changes'
      do i=1,3
        if(i.eq.3.and.Key.ne.1) cycle
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) then
          nCrwDiscard=CrwLastMade
          Veta='%save it'
        else if(i.eq.2) then
          nCrwSave=CrwLastMade
          Veta='save it %as'
          tpomp=tpom+FeTxLengthUnder(Veta)+5.
        else
          nCrwSaveAs=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
      enddo
      if(Key.eq.1) then
        dpom=100.
        Veta='=>'
        tpom=tpomp
        xpom=tpomp+FeTxLengthUnder(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSaveAs=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,LabelSaveAs)
      else
        nEdwSaveAs=0
      endif
1200  if(nEdwSaveAs.gt.0) then
        if(CrwLogicQuest(nCrwSaveAs)) then
          call FeQuestStringEdwOpen(nEdwSaveAs,LabelSaveAs)
        else
          LabelSaveAs=EdwStringQuest(nEdwSaveAs)
          call FeQuestEdwDisable(nEdwSaveAs)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      FileSaveAs=' '
      if(CrwLogicQuest(nCrwSave)) then
        if(Key.le.1) then
          if(LabelPlaneNew.ne.' ') then
            FileSaveAs=LabelPlaneNew
          else if(IPlane.gt.0) then
            FileSaveAs=LabelPlane(IPlane)
          else
            FileSaveAs=' '
          endif
        else if(Key.eq.2) then
          FileSaveAs='###NeniObecna###'
        else if(Key.eq.3) then
          FileSaveAs='###JenBasic###'
        endif
      else if(CrwLogicQuest(nCrwSaveAs)) then
        if(nEdwSaveAs.gt.0) then
          FileSaveAs=EdwStringQuest(nEdwSaveAs)
          IPlaneNew=LocateInStringArray(LabelPlane,NumberOfPlanes,
     1                                  FileSaveAs,.true.)
          if(IPlaneNew.gt.0.and.IPlaneNew.ne.IPlane) then
            if(.not.FeYesNo(-1.,YBottomMessage,'The plane "'//
     1                      FileSaveAs(:idel(FileSaveAs))//
     2                      '" already exists. Do you want to '//
     3                      'rewrite it?',1)) then
              call FeQuestButtonOff(ButtonOK-ButtonFr+1)
              go to 1500
            endif
          endif
        endif
      endif
      call FeQuestRemove(id)
9999  return
      end
