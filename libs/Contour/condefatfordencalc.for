      subroutine ConDefAtForDenCalc(klic,ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xpp(3),ddx(3),gddx(3)
      character*80 Veta
      character*30 Titulek
      integer SbwLnQuest,SbwItemSelQuest,ipori(:),SbwItemFromQuest,
     1        UseTabsIn
      logical atbrato(:),bratpdf(:),zmena,lpom,FeYesNoHeader
      data klico/-1/
      allocatable ipori,atbrato,bratpdf
      allocate(atbrato(NAtIndLenAll(KPhase)))
      if(ncomp(1).le.1) nsubs=1
      if(klic.eq.0) then
        Titulek='j.p.d.f.'
      else if(klic.eq.1) then
        Titulek='charge density'
      else if(klic.eq.2) then
        Titulek='*.ent'
      else if(klic.ge.3) then
        Titulek='topological analysis'
      endif
      if(Obecny) then
        ir=2
      else
        ir=1
      endif
      go to 1150
      entry ConDefAtForDragon(klic,ich)
      Veta='Dragon'
      ir=1
1150  UseTabsIn=UseTabs
      Zmena=.false.
      do i=1,3
        xpp(i)=(xmaxa(i,ir)+xmina(i,ir))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddx(iorien(1))=(xmaxa(1,ir)-xpp(1))*float(i1)
        do i2=-1,1,2
          ddx(iorien(2))=(xmaxa(2,ir)-xpp(2))*float(i2)
          do i3=-1,1,2
            ddx(iorien(3))=(xmaxa(3,ir)-xpp(3))*float(i3)
            if(obecny) then
              dd=sqrt(ddx(1)**2+ddx(2)**2+ddx(3)**2)
            else
              call multm(MetTens(1,nsubs,KPhase),ddx,gddx,3,3,1)
              dd=sqrt(scalmul(ddx,gddx))
            endif
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      dmez=dmez+CutOffDist
      if(ir.eq.2) then
        call prevod(1,xpp,xp)
      else
        call CopyVek(xpp,xp,3)
      endif
      if(NDim(KPhase).le.3) ntpdf=1
      if(.not.WizardMode) then
        if(kpdf.ge.4) then
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            atbrat(i)=.true.
          enddo
          Zmena=.true.
        else
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            atbrato(i)=atbrat(i)
          enddo
          Zmena=Zmena.or.klic.ne.klico
1520      call SelAtoms('Select atoms for distance check',
     1      Atom(NAtIndFrAll(KPhase)),AtBrat(NAtIndFrAll(KPhase)),
     2      isf(NAtIndFrAll(KPhase)),NAtIndLenAll(KPhase),ich)
          if(ich.ne.0) go to 9999
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(AtBrat(i)) go to 1540
          enddo
          TextInfo(1)='No atoms specified to calculate the map'
          NInfo=1
          if(FeYesNoHeader(-1.,-1.,'Do you really want to break the '//
     1                     'action?',0)) then
            ich=1
            go to 6000
          else
            go to 1520
          endif
1540      if(.not.zmena) then
            do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
              if(AtBrat(i).neqv.AtBrato(i)) then
                Zmena=.true.
                go to 2000
              endif
            enddo
          endif
        endif
        if(klic.eq.4) go to 9999
      endif
2000  do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(AtBrat(i)) then
          isw=iswa(i)
          exit
        endif
      enddo
      call OkoliC(xp,dmez,ir,isw,.true.)
      allocate(ipori(Npdf),BratPdf(NPdf))
      call SetLogicalArrayTo(BratPdf,NPdf,.true.)
      xp(1)=0.
      do j=1,npdf
        ia=iapdf(j)
        call specpos(xpdf(1,j),xp,0,1,.005,l)
        fpdf(j)=ai(ia)*float(l)
        if(Klic.le.0) then
          do k=1,5
            BratAnharmPDF(k,j)=AtBratAnharm(k,ia)
          enddo
        endif
        if(zmena) bratpdf(j)=.true.
      enddo
      if(kpdf.ge.4) go to 6000
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_datoms.tmp','formatted','unknown')
      do j=1,npdf
        k=ipor(j)
        ipori(k)=j
        write(Cislo,'(f6.3)') dpdf(k)
        Veta=Tabulator//atom(iapdf(k))(:idel(atom(iapdf(k))))//
     1       Tabulator//'|'//Tabulator//Cislo//
     2       Tabulator//'|'//
     3       Tabulator//scpdf(k)(:idel(scpdf(k)))
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      id=NextQuestId()
      Veta='Select individual atoms for '//Titulek
      if(WizardMode) then
        xqd=WizardLength
        call FeQuestTitleMake(id,Veta)
      else
        il=15
        xqd=400.
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      endif
      UseTabs=NextTabs()
      pom=0.
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('XXXXXXXX |')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('MM')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('XX.XXX |')
      call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
      pom=pom+FeTxLength('MM')
      if(WizardMode) then
        dpom=400.
        xshift=(xqd-dpom)*.5
      else
        dpom=xqd-10.-SbwPruhXd
        xshift=5.
      endif
      il=1
      Veta='Atom'
      xpom=7.+xshift
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      Veta='Distance'
      xpom=xpom+65.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      Veta='Symmetry code'
      xpom=xpom+55.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=14
      xpom=xshift
      call FeQuestSbwMake(id,xpom,il,dpom,13,1,CutTextNone,SbwVertical)
      nSbw=SbwLastMade
      il=il+1
      dpom=60.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Refresh')
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Select all')
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
3100  do i=1,npdf
        k=ipor(i)
        if(BratPdf(k)) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbw,j)
      enddo
      ItemFromOld=SbwItemFromQuest(nSbw)
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_datoms.tmp')
      call FeQuestSbwShow(nSbw,ItemFromOld)
3200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        lpom=CheckNumber.ne.nButtRefresh
        call SetLogicalArrayTo(BratPdf,NPdf,lpom)
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 3100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3200
      endif
      if(ich.eq.0) then
        l=0
        do i=1,NPdf
          j=SbwItemSelQuest(ipori(i),nSbw)
          BratPdf(i)=j.eq.1
          if(BratPdf(i)) then
            l=i
          else
            fpdf(i)=0.
          endif
        enddo
        npdf=l
      endif
      if(.not.WizardMode) call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_datoms.tmp')
6000  klico=klic
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(allocated(ipori)) deallocate(ipori,BratPdf)
      if(allocated(atbrato)) deallocate(atbrato)
      return
      end
