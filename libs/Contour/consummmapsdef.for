      subroutine ConSummMapsDef
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension ior(6)
      character*3 t3
      integer EdwStateQuest
      logical CrwLogicQuest
      read(m8,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                        ior,mapa,nsubs,SatelityByly,
     2                        nonModulated(KPhase)
      nsubs=mod(nsubs,10)
      id=NextQuestId()
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)-2,'Summation',0,
     1                   LightGray,0,0)
      tpom=5.
      xpom=tpom+FeTxLength('XXX')+15.
      iw=0
      tpome1=xpom+CrwXd+15.
      xpome1=tpome1+FeTxLength('XXXX')+10.
      dpom=80.
      tpome2=tpome1+dpom+60.
      xpome2=xpome1+dpom+60.
      do i=1,NDim(KPhase)-2
        t3=cx(i+2)
        if(t3(2:2).eq.' ') then
          t3='%'//t3(1:1)
        else
          t3=t3(1:1)//'%'//t3(2:2)
        endif
        call FeQuestCrwMake(id,tpom,i,xpom,i,t3,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(i,.false.)
        call FeQuestEudMake(id,tpome1,i,xpome1,i,'from','L',dpom,EdwYd,
     1                      0)
        call FeQuestEudMake(id,tpome2,i,xpome2,i,'  to','L',dpom,EdwYd,
     1                      0)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        if(CrwLogicQuest(CheckNumber)) then
          do i=1,CheckNumber
            iw=2*i-1
            if(EdwStateQuest(iw).ne.EdwOpened) then
              call FeQuestCrwOn(i)
              call FeQuestRealEdwOpen(iw,xmin(i+2),.false.,.false.)
              call FeQuestEudOpen(iw,1,1,1,xmin(i+2),xmax(i+2),dx(i+2))
              call FeQuestRealEdwOpen(iw+1,xmax(i+2),.false.,.false.)
              call FeQuestEudOpen(iw+1,1,1,1,xmin(i+2),xmax(i+2),dx(i+2)
     1                            )
            endif
          enddo
        else
          do i=CheckNumber,NDim(KPhase)-2
            iw=2*i-1
            call FeQuestCrwOff(i)
            call FeQuestEdwClose(iw)
            call FeQuestEdwClose(iw+1)
          enddo
        endif
        EventType=EventEdw
        EventNumber=2*CheckNumber-1
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        isoucet=0
        iw=0
        do i=1,NDim(KPhase)-2
          if(CrwLogicQuest(i)) then
            isoucet=isoucet+10**(i-1)
            iw=iw+1
            if(dx(i+2).ne.0.) then
              nxfrom(i)=nint((EdwRealQuest(1,iw)-xmin(i+2))/dx(i+2))
     1                  +1
            else
              nxfrom(i)=1
            endif
            iw=iw+1
            if(dx(i+2).ne.0.) then
              nxto(i)=nint((EdwRealQuest(1,iw)-xmin(i+2))/dx(i+2))+1
            else
              nxto(i)=1
            endif
          endif
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) isoucet=0
1000  if(isoucet.eq.0) then
        read(m8,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          ior,mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
        nsubs=mod(nsubs,10)
        read(m8,rec=nmap+2,err=9000) Dmax,Dmin
        smapy=.false.
      else if(isoucet.gt.0) then
        call CloseIfOpened(86)
        call OpenMaps(86,fln(:ifln)//'.l86',nxny,1)
        if(ErrFlag.ne.0) go to 9999
        call ConSummMaps(ior)
        if(ErrFlag.ne.0) then
          isoucet=0
          ErrFlag=0
          go to 1000
        endif
        m8=86
        smapy=.true.
      endif
      call SetContours(0)
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
9999  return
      end
