      function rank6(f)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension f(28)
      pom=wmp110_1*wmp3_1*wmp35_1*f(1)+
     1    wmp110_2*wmp3_2*wmp35_2*f(22)+
     2    wmp110_3*wmp3_3*wmp35_3*f(28)
      pom=pom+w1*(w2*(pm1015_1*f(2)+pm1015_2*f(16))+
     1            w3*(pm1015_1*f(3)+pm1015_3*f(21)))+
     2         w2*w3*(pm1015_2*f(23)+pm1015_3*f(27))+
     3           w12*(pm63_3*f(20)+wmp3*(wm3p1*f(9)+wm3p2*f(18))+
     4                wm3p1*wm3p2*f(7))+
     5           w13*(pm63_2*f(17)+wmp2*(wm3p1*f(8)+wm3p3*f(19))+
     6                wm3p1*wm3p3*f(10))+
     7           w23*(pm63_1*f(5)+wmp1*(wm3p2*f(12)+wm3p3*f(14))+
     8                wm3p2*wm3p3*f(25))
      rank6=pom+wmp1*(wmp2*wmp3*f(13)+pm63_2*f(11)+pm63_3*f(15))+
     1          wmp2*(pm63_1*f(4)+pm63_3*f(26))+
     2          wmp3*(pm63_2*f(24)+pm63_1*f(6))
      return
      end
