      subroutine vrstvy(iznp)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical konec,minus,FeYesNo
      dimension xp(3),nh(:)
      allocatable nh
      data xp/3*0./
      allocate(nh(nxny))
      if(ContourType.eq.ContourTypeLinear) then
        if(iznp.ge.0) then
          fmz=fmzp
        else
          fmz=fmzn
        endif
        nrmin=0
        nrmax=0
        do i=1,nxny
          nri=abs(ActualMap(i))/fmz
          if(ActualMap(i).lt.0.) nri=-nri-1
          nrmin=min(nri,nrmin)
          nrmax=max(nri,nrmax)
        enddo
        if(fmz.le.0.) go to 9999
        if(iznp.le.0) then
          iv1=nrmin
        else
          iv1=1
        endif
        if(iznp.ge.0) then
          iv2=nrmax
        else
          iv2=0
        endif
      else if(ContourType.eq.ContourTypeUser) then
        iv1=1
        iv2=ContourNumber
      else if(ContourType.eq.ContourTypeAIM) then
        rmax=0.
        do i=1,nxny
          rmax=max(abs(ActualMap(i)),rmax)
        enddo
        iv2=1
        pom=1.
        rad=10.**ContourAIMOrder
        i=0
1100    if(pom*rad.le.rmax) then
          iv2=iv2+1
          i=i+1
          if(i.gt.3) then
            i=1
            rad=rad*10.
            pom=1.
          endif
          pom=pom*2.
          go to 1100
          if(iznp.le.0) then
            iv1=0
          else
            iv1=1
          endif
        endif
      endif
      kolik=0
      call FeMouseShape(3)
      ivm=max((iv2-iv1+1)/10,1)
      nButtInterrupt=0
      if(ContourType.eq.ContourTypeAIM) then
        if(iznp.ge.0) then
          fivp= 1.
        else
          fivp=-1.
        endif
        rad=10.**ContourAIMOrder
        ivp=0
      endif
      do 9000iv=iv1,iv2
        if(.not.JedeMovie) then
          if(mod(iv-iv1,ivm).eq.0.and.
     1       (OpSystem.eq.1.or.HardCopy.lt.1.or.Hardcopy.gt.5)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
          call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            if(FeYesNo(-1.,-1.,'Do you want really to interrupt it?',1))
     1        go to 9500
          else if(EventType.eq.EventButton) then
            if((EventNumber.eq.nButtMapaPlus.and.irec.lt.nmap).or.
     1         (EventNumber.eq.nButtMapaMinus.and.irec.gt.1)) then
              nButtInterrupt=EventNumber
              go to 9999
            endif
          endif
        endif
        if(ContourType.eq.ContourTypeLinear) then
          fiv=fmz*float(iv)
          if(fiv.gt.cutpos.or.fiv.lt.cutneg) go to 9000
        else if(ContourType.eq.ContourTypeUser) then
          fiv=ContourArray(iv)
        else if(ContourType.eq.ContourTypeAIM) then
          if(iv.gt.0) then
            ivp=ivp+1
            if(ivp.gt.3) then
              ivp=1
              rad=rad*10.
              if(iznp.ge.0) then
                fivp= 1.
              else
                fivp=-1.
              endif
            endif
            fivp=fivp*2.
            fiv=fivp*rad
          else
            fiv=0.
          endif
        endif
        k=0
        do j=1,nx(2)
          do 1600i=1,nx(1)
            k=k+1
            pom=ActualMap(k)
            nh(k)=0
            if(pom.lt.fiv) go to 1600
            if(i.ne.1) then
              if(ActualMap(k- 1).lt.fiv) nh(k)=nh(k)+1000
            endif
            if(i.ne.nx(1)) then
              if(ActualMap(k+ 1).lt.fiv) nh(k)=nh(k)+10
            endif
            if(j.ne.1) then
              if(ActualMap(k-nx(1)).lt.fiv) nh(k)=nh(k)+1
            endif
            if(j.ne.nx(2)) then
              if(ActualMap(k+nx(1)).lt.fiv) nh(k)=nh(k)+100
            endif
1600      continue
        enddo
        if(fiv.gt.0.) then
          call FeLineType(NormalLine)
        else if(fiv.eq.0.) then
          call FeLineType(DashedLine)
        else
          call FeLineType(DenseDashedLine)
        endif
6000    ivse=1
        if(ivse.eq.1) then
          k=0
          do j=1,nx(2)
            do i=1,nx(1)
              k=k+1
              nhs=nh(k)/1000*1000
              if(nhs.ne.0) go to 6300
            enddo
          enddo
          ivse=2
        endif
        if(ivse.eq.2) then
          k=nxny
          do j=nx(2),1,-1
            do i=nx(1),1,-1
              nhs=mod(nh(k),100)/10*10
              if(nhs.ne.0) go to 6300
              k=k-1
            enddo
          enddo
          ivse=3
        endif
        if(ivse.eq.3) then
          k=0
          do j=1,nx(2)
            do i=1,nx(1)
              k=k+1
              nhs=mod(nh(k),1000)/100*100
              if(nhs.ne.0) go to 6300
            enddo
          enddo
          ivse=4
        endif
        if(ivse.eq.4) then
          k=nxny
          do j=nx(2),1,-1
            do i=nx(1),1,-1
              nhs=mod(nh(k),10)
              if(nhs.ne.0) go to 6300
              k=k-1
            enddo
          enddo
          if(kolik.gt.1) then
            call FePolyLine(kolik,xpole,ypole,White)
            kolik=0
          endif
          go to 9000
        endif
6300    i1=i
        j1=j
        kk=k
        nhp=nhs
        xp(1)=float(i-1)*dx(1)+xmin(1)
        xp(2)=float(j-1)*dx(2)+xmin(2)
        if(nhs.eq.1000) then
          xp(1)=xp(1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-1))*dx(1)
          minus=j.eq.nx(2)
          if(minus) nh(k)=nh(k)-1000
        else if(nhs.eq.10) then
          xp(1)=xp(1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+1))*dx(1)
          minus=j.eq.1
          if(minus) nh(k)=nh(k)-10
        else if(nhs.eq.100) then
          xp(2)=xp(2)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+nx(1)))*dx(2)
          minus=i.eq.nx(1)
          if(minus) nh(k)=nh(k)-100
        else
          xp(2)=xp(2)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-nx(1)))*dx(2)
          minus=i.eq.1
          if(minus) nh(k)=nh(k)-1
        endif
        call FeXf2X(xp,xo)
        x1=xo(1)
        y1=xo(2)
6350    if(kolik.gt.1) then
          call FePolyLine(kolik,xpole,ypole,White)
          kolik=0
        endif
        xpole(1)=x1
        ypole(1)=y1
        kolik=1
7000    if(.not.minus) then
          if(nhp.eq.1000) then
            kk=kk+nx(1)-1
            i=i-1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq. 100) then
            kk=kk+nx(1)+1
            i=i+1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.  10) then
            kk=kk-nx(1)+1
            i=i+1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.   1) then
            kk=kk-nx(1)-1
            i=i-1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            go to 9500
          endif
        else
          if(nhp.eq.1000) then
            kk=kk-nx(1)-1
            i=i-1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq. 100) then
            kk=kk+nx(1)-1
            i=i-1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            kk=kk+1
            i=i+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.  10) then
            kk=kk+nx(1)+1
            i=i+1
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            kk=kk-nx(1)
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),1000)/100*100
              if(nhp.ne.0) then
                kam=2
                go to 7500
              endif
            endif
            go to 9500
          else if(nhp.eq.   1) then
            kk=kk-nx(1)+1
            i=i+1
            j=j-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=nh(kk)/1000*1000
              if(nhp.ne.0) then
                kam=1
                go to 7500
              endif
            endif
            kk=kk+nx(1)
            j=j+1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),10)
              if(nhp.ne.0) then
                kam=4
                go to 7500
              endif
            endif
            kk=kk-1
            i=i-1
            if(kk.ge.1.and.kk.le.nxny) then
              nhp=mod(nh(kk),100)/10*10
              if(nhp.ne.0) then
                kam=3
                go to 7500
              endif
            endif
            go to 9500
          endif
        endif
7500    if(kam.eq.1) then
          xp(1)=dx(1)*(float(i-1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-1)))+xmin(1)
          xp(2)=float(j-1)*dx(2)+xmin(2)
          konec=(j.eq.nx(2).and..not.minus).or.(j.eq.1.and.minus)
        else if(kam.eq.2) then
          xp(1)=float(i-1)*dx(1)+xmin(1)
          xp(2)=dx(2)*(float(j-1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+nx(1))))+xmin(2)
          konec=(i.eq.nx(1).and..not.minus).or.(i.eq.1.and.minus)
        else if(kam.eq.3) then
          xp(1)=dx(1)*(float(i-1)+(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk+1)))+xmin(1)
          xp(2)=float(j-1)*dx(2)+xmin(2)
          konec=(j.eq.1.and..not.minus).or.(j.eq.nx(2).and.minus)
        else if(kam.eq.4) then
          xp(1)=float(i-1)*dx(1)+xmin(1)
          xp(2)=dx(2)*(float(j-1)-(ActualMap(kk)-fiv)/
     1                (ActualMap(kk)-ActualMap(kk-nx(1))))+xmin(2)
          konec=(i.eq.1.and..not.minus).or.(i.eq.nx(1).and.minus)
        endif
        call FeXf2X(xp,xo)
        xm=xo(1)
        ym=xo(2)
        kolik=kolik+1
        xpole(kolik)=xm
        ypole(kolik)=ym
        if(kolik.ge.mxc) then
          call FePolyLine(kolik,xpole,ypole,White)
          kolik=1
          xpole(1)=xm
          ypole(1)=ym
        endif
        nh(kk)=nh(kk)-nhp
        if(konec.or.(i.eq.i1.and.j.eq.j1.and.nhp.eq.nhs)) then
          if((i.eq.i1.and.j.eq.j1.and.nhp.eq.nhs).or.minus) go to 6000
          nh(k)=nh(k)-nhs
          if((nhs.eq.1000.and.j1.eq.1).or.(nhs.eq.10.and.j1.eq.nx(2))
     1   .or.(nhs.eq.100 .and.i1.eq.1).or.(nhs.eq.1 .and.i1.eq.nx(1)))
     2          go to 6000
          nhp=nhs
          i=i1
          j=j1
          kk=k
          minus=.true.
          go to 6350
        else
          go to 7000
        endif
9000  continue
      go to 9999
9500  ErrFlag=1
9999  call FeLineType(NormalLine)
      deallocate(nh)
      return
      end
