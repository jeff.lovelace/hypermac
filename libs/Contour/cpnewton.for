      subroutine CPNewton(xf,Rho,Rho2D,Grad,Diag,NonZero,NSgn,ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xf(3),Hess(9),HessI(9),RhoA(10),dxf(3),xp(3),Diag(3)
      ich=0
      dump=1.
      do n=1,CPNIter
        call CPGetRhoAndDRho(xf,2,RhoA,ich)
        if(ich.ne.0) go to 9999
        Rho=RhoA(1)
        Grad=sqrt(RhoA(2)**2+RhoA(3)**2+RhoA(4)**2)
        Hess(1)=RhoA(5)
        Hess(5)=RhoA(6)
        Hess(9)=RhoA(7)
        Hess(2)=RhoA(8)
        Hess(3)=RhoA(9)
        Hess(6)=RhoA(10)
        Hess(4)=Hess(2)
        Hess(7)=Hess(3)
        Hess(8)=Hess(6)
        call MatInv(Hess,HessI,pom,3)
        call multm(HessI,RhoA(2),xp,3,3,1)
        xpp=sqrt(xp(1)**2+xp(2)**2+xp(3)**2)
        if(xpp.gt.CPMaxStep) then
          Redukce=CPMaxStep/xpp
        else
          Redukce=1.
        endif
        call multm(TrToOrthoI,xp,dxf,3,3,1)
        do i=1,3
          xf(i)=xf(i)-dump*Redukce*dxf(i)
        enddo
        if(dxf(3).gt.CPMaxStep) then
          Redukce=CPMaxStep/dxf(3)
        else
          Redukce=1.
        endif
        if(Grad.lt.CPCrit*RHoA(1)) go to 3000
        if(n.gt.2) then
          if(abs(Grad-GradOld).gt.abs(Grad-GradOlder)) dump=dump*.5
        endif
        if(n.gt.1) GradOlder=GradOld
        GradOld=Grad
      enddo
      ich=1
      go to 9999
3000  Rho2d=RhoA(5)+RhoA(6)+RhoA(7)
      HessI(1)=Hess(1)
      HessI(2)=Hess(2)
      HessI(3)=Hess(5)
      HessI(4)=Hess(3)
      HessI(5)=Hess(6)
      HessI(6)=Hess(9)
      call ql(HessI,RhoA,diag,3)
      NonZero=0
      NSgn=0
      do i=1,3
        if(diag(i).ne.0.) then
          NonZero=NonZero+1
          NSgn=NSgn+nint(sign(1.,diag(i)))
        endif
      enddo
9999  return
      end
