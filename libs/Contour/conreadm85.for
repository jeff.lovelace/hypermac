      subroutine ConReadM85
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension covl(:),covlp(:)
      character*80 t80
      character*12 at,pn
      allocatable covl,covlp
      call OpenFile(85,fln(1:ifln)//'.m85','unformatted','old')
      if(ErrFlag.ne.0) go to 9000
      read(85,err=8000,end=8000) neq
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      mxe=neq
      mxep=5
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      do i=1,neq
        read(85,err=8000,end=8000) lnp(i),pab(i),npa(i)
        if(npa(i).gt.mxep) then
          call SetRealArrayTo(pnp(1,i),mxep,0.)
          call SetRealArrayTo(pko(1,i),mxep,0.)
          call ReallocEquations(mxe,npa(i))
        endif
        do j=1,npa(i)
          read(85,err=8000,end=8000) pnp(j,i),pko(j,i)
        enddo
      enddo
      ipocm=0
      do i=1,NDatBlock
        do j=1,MxScAll
          if(KiS(j,i).ne.0) then
            ipocm=ipocm+1
            read(85,err=8000,end=8000) pom
          endif
        enddo
      enddo
      do i=1,MxParPwd
        if(KiPwd(i).ne.0) then
          ipocm=ipocm+1
          read(85,err=8000,end=8000) pom
        endif
      enddo
      if(allocated(KIEd)) then
        do KDatB=1,NDatBlock
          do i=1,MxEDRef
            do j=1,MxEdZone
              if(KiED(i,j,KDatB).ne.0) then
                ipocm=ipocm+1
                read(85,err=8000,end=8000) pom
              endif
            enddo
          enddo
        enddo
      endif
      im=ipocm
      ic=0
      jc=0
      ijc=0
      nalloc=1000
      allocate(covl(nalloc))
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        do 4000id=1,DelkaKiAtomu(i)
          if(KiA(id,i).le.0) cycle
          im=im+1
          if(im.gt.nalloc) then
            allocate(covlp(nalloc))
            call CopyVek(covl,covlp,nalloc)
            nallocp=nalloc
            nalloc=nalloc+1000
            deallocate(covl)
            allocate(covl(nalloc))
            call CopyVeK(covlp,covl,nallocp)
            deallocate(covlp)
          endif
          read(85,err=8000,end=8000)(covl(m),m=1,im)
          if(SelPDF(i)) then
            ic=ic+1
            if(ic.gt.mxran) then
              write(t80,'(i6)') mxran
              call zhusti(t80)
              t80='the maximum number '//t80(:idel(t80))//
     1            ' of random variables exceeded'
              call FeChybne(-1.,-1.,t80,'error map resp. curve will'//
     1                      ' not be calculated.',SeriousError)
              go to 9000
            endif
            call KdoCo(id+PrvniKiAtomu(i)-1,at,pn,1,rnds(ic),sp)
            il=ipocm
            jc=0
            do j=1,i
              do jl=1,DelkaKiAtomu(j)
                if(KiA(jl,j).eq.1) then
                  il=il+1
                  if(SelPDF(j).and.jc.lt.ic) then
                    jc=jc+1
                    ijc=ijc+1
                    covc(ijc)=covl(il)
                  endif
                endif
              enddo
            enddo
            ii=ijc-jc
            if(ijc.eq.1) then
              v11=sqrt(covc(ijc))
              covc(ijc)=v11
              go to 4000
            endif
            covc(ii+1)=covc(ii+1)/v11
            do j=2,jc-1
              ij=ii+j
              jj=(j*(j-1))/2
              do l=1,j-1
                covc(ij)=covc(ij)-covc(ii+l)*covc(jj+l)
              enddo
              covc(ij)=covc(ij)/covc(jj+j)
            enddo
            do l=1,jc-1
              covc(ijc)=covc(ijc)-covc(ii+l)**2
            enddo
            covc(ijc)=sqrt(covc(ijc))
          endif
4000    continue
      enddo
      go to 9999
8000  call FeReadError(85)
9000  mcmax=0
      ErrMapActive=.false.
      ErrFlag=0
9999  call CloseIfOpened(85)
      if(allocated(covl)) deallocate(covl)
      return
      end
