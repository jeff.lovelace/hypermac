      subroutine NactiContour
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call Najdi('contour',i)
      if(i.ne.1) go to 9999
      PreskocVykricnik=.true.
      call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      izpet=0
      NumberOfPlanes=0
      NumberOfPlanesMax=0
1100  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0) go to 9999
1110  if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntContour ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealContour,NactiReal)
        go to 9999
      else if(izpet.eq.nCmdLabelPlane) then
        call ConReallocatePlanes
        NumberOfPlanes=NumberOfPlanes+1
        LabelPlane(NumberOfPlanes)=NactiVeta(PosledniPozice+1:)
        PosledniPozice=len(NactiVeta)+1
      endif
      if(izpet.ne.0) go to 1100
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      return
102   format(f15.5)
      end
