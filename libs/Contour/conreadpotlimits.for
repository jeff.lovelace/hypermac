      subroutine ConReadPotLimits(pmn,pmx,teplota,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pmp(2)
      character*40 Veta
      pmp(1)=pmn
      pmp(2)=pmx
      id=NextQuestId()
      xqd=200.
      call FeQuestCreate(id,-1.,-1.,xqd,3,'Limits for potential '//
     1                   'curve',0,LightGray,0,0)
      il=0
      Veta='Mi%n in [meV]'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,pmp(i),.false.,.false.)
        if(i.eq.1) then
          nEdwMin=EdwLastMade
          Veta='Ma%x in [meV]'
        else
          nEdwMax=EdwLastMade
        endif
      enddo
      il=il+1
      Veta='%Temperature'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwT=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,teplota,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwMin,pmn)
        call FeQuestRealFromEdw(nEdwMax,pmx)
        call FeQuestRealFromEdw(nEdwT,Teplota)
      endif
      call FeQuestRemove(id)
      return
      end
