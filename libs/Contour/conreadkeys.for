      subroutine ConReadKeys
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3)
      character*80 Veta
      logical EqIgCase,MameJi
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      call Najdi('contour',i)
      if(i.ne.1) go to 9999
      PreskocVykricnik=.false.
      call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      IPoints=0
      DrawAtN=0
      NCurve=0
      MameJi=.not.Obecny
      izpet=0
1210  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0.or.izpet.eq.0) then
        ErrFlag=0
        go to 9999
      endif
      if(NumberOfPlanes.gt.0) then
        if(izpet.eq.nCmdLabelPlane) then
          if(.not.Obecny) go to 9999
          DrawAtN=0
          NCurve=0
          Veta=NactiVeta(PosledniPozice+1:)
          MameJi=EqIgCase(Veta,LabelPlane(IPlane))
          PosledniPozice=len(NactiVeta)+1
          go to 1210
        else if(izpet.eq.nCmdEndPlane) then
          if(MameJi) then
            go to 9999
          else
            go to 1210
          endif
        endif
      endif
      if(.not.MameJi) go to 1210
      if(izpet.eq.nCmdDeltaPlane) then
        call StToReal(NactiVeta,PosledniPozice,xp,1,.false.,ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9900
        endif
        DeltaPlane=xp(1)
      else if(izpet.eq.nCmdPntPlane) then
        if(IPoints.lt.3) then
          IPoints=IPoints+1
          call kus(NactiVeta,PosledniPozice,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=9900) SelPlane(IPoints)
          StPlane(IPoints)=' '
          if(SelPlane(IPoints).eq.1) then
            call kus(NactiVeta,PosledniPozice,StPlane(IPoints))
            call CtiAt(StPlane(IPoints),XPlane(1,IPoints),ich)
            if(ich.ne.-1) go to 9900
            ich=0
          else if(SelPlane(IPoints).eq.2) then
            call StToReal(NactiVeta,PosledniPozice,XPlane(1,IPoints),3,
     1                    .false.,ich)
          else if(SelPlane(IPoints).eq.3) then
            if(IPoints.eq.1) then
              SelPlane(IPoints)=2
              call SetRealArrayTo(XPlane(1,IPoints),3,0.)
              IPoints=IPoints+1
              SelPlane(IPoints)=3
            endif
            call StToReal(NactiVeta,PosledniPozice,DxPlane(1,IPoints),3,
     1                    .false.,ich)
          else
            go to 9900
          endif
          if(ich.ne.0) go to 9900
          if(IPoints.gt.1) then
            if(SelPlane(IPoints).eq.3) then
              do i=1,3
                XPlane(i,IPoints)=DxPlane(i,IPoints)+XPlane(i,1)
              enddo
            else
              do i=1,3
                DxPlane(i,IPoints)=XPlane(i,IPoints)-XPlane(i,1)
              enddo
            endif
          endif
          go to 1210
        endif
      else if(izpet.eq.nCmdScopePlane) then
        call StToReal(NactiVeta,PosledniPozice,ScopePlane,3,.false.,ich)
        if(ich.ne.0) go to 9900
      else if(izpet.eq.nCmdShiftPlane) then
        call StToReal(NactiVeta,PosledniPozice,ShiftPlane,3,.false.,ich)
        if(ich.ne.0) go to 9900
      else if(izpet.eq.nCmdDrawAt) then
        Veta=NactiVeta
        if(NacetlVykricnik) Veta='!'//Veta(:idel(Veta))
        DrawAtN=DrawAtN+1
        call ConReadDrawAtString(Veta,DrawAtN)
      else if(izpet.eq.nCmdXTypeCurve) then
        call kus(NactiVeta,PosledniPozice,Cislo)
        call Posun(Cislo,0)
        read(Cislo,FormI15,err=9900) XTypeCurve
      else if(izpet.eq.nCmdPointCurve) then
        NCurve=NCurve+1
        AtCurve(NCurve)=NactiVeta(PosledniPozice+1:)
        call UprAt(AtCurve(NCurve))
      endif
      go to 1210
9900  ErrFlag=1
9999  call CloseIfOpened(m50)
      return
      end
