      function ExtMap(xs,ys,tabp,nmx)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension tabp(*)
      gnx=(xs-xmin(1))/dx(1)+1.
      gny=(ys-xmin(2))/dx(2)+1.
      ngx=gnx
      ngy=gny
      ngxy=(ngy-1)*nx(1)+ngx
      gnx=gnx-float(ngx)
      gny=gny-float(ngy)
      gnx1=1.-gnx
      gny1=1.-gny
      ExtMap=0.
      if(ngxy.le.nmx) ExtMap=ExtMap+tabp(ngxy)*gnx1*gny1
      i=ngxy+1
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx*gny1
      i=ngxy+1+nx(1)
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx*gny
      i=ngxy+nx(1)
      if(i.le.nmx) ExtMap=ExtMap+tabp(i)*gnx1*gny
      return
      end
