      subroutine ConRightButtons
      use Contour_mod
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension nmen1(11),xp(9),xpp(3),WorkMap(:),TMat(36),TMatI(36),
     1          rmx(9),rmb(36),CellParP(3)
      character*256 Veta,t256
      character*80 AtPDF,FileName
      character*21 form1
      character*10 SaveFormat
      character*6 t6
      integer ButtonStateQuest,FeGetSystemTime,TimeStart,Zdvih,
     1        HardCopyOld,FeMenuNew,ConGetFirstSection,HardCopyPom
      logical ExistFile,FeYesNo,WholeCell,NovyStart,Patterson
      allocatable WorkMap
      data form1/'(2(i5,''x''),i5,''='',i8)'/
      data AtPDF/' '/,SaveFormat/'(8e15.6)'/
      smapy=.false.
      NovyStart=.false.
1100  AllowResizing=.true.
      nLblMovie=0
      ConButtLabels(IdAtomsOnOff)='At%oms ON'
      ConButtLabels(IdSumOnOff)='S%um ON'
      ConButtLabels(IdTMapOnOff)='%t-map ON'
      ConButtLabels(IdErrOnOff)='%Err ON'
      CheckMouse=.true.
      call FeBottomInfo('#prazdno#')
      ContourQuest=NextQuestId()
      call FeQuestAbsCreate(ContourQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      ContourYSep(1)=pom
      j=1
      do i=1,IdOptions
        ActiveButtBas(i)=.false.
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,ConButtLabels(i))
        if(i.eq.IdQuit) then
          nButtQuit=ButtonLastMade
        else if(i.eq.IdPrint) then
          nButtPrint=ButtonLastMade
        else if(i.eq.IdSave) then
          nButtSave=ButtonLastMade
        else if(i.eq.IdRun3dMaps) then
          nButtRun3dMaps=ButtonLastMade
        else if(i.eq.IdNewMap) then
          nButtNew=ButtonLastMade
          wpom=wpom/2.-5.
          xsave=xpom
          ysave=ypom
          nButtBasFr=ButtonLastMade
        else if(i.eq.IdMMinus) then
          nButtMapaMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.IdMPlus) then
          nButtMapaPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.IdGoTo) then
          nButtGoTo=ButtonLastMade
        else if(i.eq.IdMovie) then
          nButtMovie=ButtonLastMade
        else if(i.eq.IdContours) then
          nButtContours=ButtonLastMade
        else if(i.eq.IdAtomsEdit) then
          nButtAtomsDefine=ButtonLastMade
        else if(i.eq.IdAtomsOnOff) then
          nButtAtomsDraw=ButtonLastMade
        else if(i.eq.IdAtomsFill) then
          nButtAtomsFill=ButtonLastMade
        else if(i.eq.IdX4Length) then
          nButtX4Length=ButtonLastMade
          if(NDimI(KPhase).le.0) then
            call FeQuestButtonRemove(ButtonLastMade)
            ypom=ypom+dpom
            go to 1500
          endif
        else if(i.eq.IdCurves) then
          nButtCurves=ButtonLastMade
        else if(i.eq.IdSumOnOff) then
          nButtSum=ButtonLastMade
        else if(i.eq.IdSearch) then
          nButtSearch=ButtonLastMade
        else if(i.eq.IdSearchAll) then
          nButtGlobal=ButtonLastMade
        else if(i.eq.IdTMapOnOff) then
          nButtTMaps=ButtonLastMade
          if(NDimI(KPhase).le.0) then
            call FeQuestButtonRemove(ButtonLastMade)
            cycle
          endif
        else if(i.eq.IdErrOnOff) then
          nButtErrMaps=ButtonLastMade
        else if(i.eq.IdOptions) then
          nButtOptions=ButtonLastMade
        endif
        ActiveButtBas(i)=.true.
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500    if(i.eq.IdRun3dMaps.or.i.eq.IdMovie.or.i.eq.IdX4Length.or.
     1     i.eq.IdErrOnOff) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          j=j+1
          ContourYSep(j)=ypom
          ypom=ypom-1.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+dpom-6.
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                       Gray,White)
      ContourYSep(5)=ypom
      xpom=xsave
      ypom=ysave
      do i=IdYScale,IdErrCurveOnOff
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,ConButtLabels(i))
        if(i.eq.IdYScale) then
          nButtYScale=ButtonLastMade
        else if(i.eq.IdDerOnOff) then
          nButtderOn=ButtonLastMade
        else if(i.eq.IdErrCurveOnOff) then
          nButtErrOn=ButtonLastMade
        endif
        ypom=ypom-dpom
      enddo
      ypom=ypom+dpom-6.
      ContourYSep(6)=ypom
      fpom=FeTxLength('XXX.XXX')
      call FeBoldFont
      dhl1=FeTxLength(ConWinfLabel(1))
      call FeNormalFont
      do i=1,4
        if(i.eq.1) then
          dpom=2.*fpom+5.
          xpom=XCenGrWin-.5*dhl1-dpom-20.
          ypom=YMaxBasWin-20.
        else if(i.eq.2) then
          dpom=3.*fpom+5.
          xpom=XCenGrWin+.5*dhl1+20.
        else if(i.eq.3) then
          xpom=XMaxGrWin-100.
          ypom=YMinGrWin-25.
          dpom=FeTxLength('XXXXXXX.XX')+5.
        else if(i.eq.4) then
          xpom=XMinGrWin+200.
          dpom=FeTxLength('XXX.XXX')+5.
        endif
        if(i.gt.2) then
          call FeQuestAbsLblMake(ContourQuest,xpom-10.,YBottomText,
     1                           ConWinfLabel(i-1),'R','N')
          call FeQuestLblOff(LblLastMade)
          nLblWinf(i)=LblLastMade
        endif
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      xpom=15.
      ypom=ypom+2.
      LocatorType=LocatorPosition
      do i=1,3
        if(i.eq.1) then
          Veta='$$Locate'
        else if(i.eq.2) then
          Veta='$$Distance'
        else if(i.eq.3) then
          Veta='$$Tilt'
        endif
        call FeQuestAbsCrwMake(ContourQuest,0.,ypom,xpom,ypom,Veta,'C',
     1                         CrwsXd,CrwsYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.LocatorType)
        if(i.eq.1) then
          nCrwLocPosition=CrwLastMade
        else if(i.eq.2) then
          nCrwLocDistance=CrwLastMade
        else if(i.eq.3) then
          nCrwLocNewSection=CrwLastMade
        else if(i.eq.4) then
          nCrwLocNewAtom=CrwLastMade
        endif
        xpom=xpom+CrwsXd+10.
      enddo
      write(form1(2:2),'(i1)') NDim(KPhase)-1
      NoOfDerivedMaps=0
      if(MapExists.and.NovyStart) then
        reconfig=.true.
        call KresliMapu(0)
        go to 2010
      endif
      MapExists=.false.
2000  call FeQuestButtonOff(nButtNew)
2010  NovyStart=.false.
      if(MapExists) then
        do i=nButtPrint,nButtErrMaps
          if(ActiveButtBas(i).and.
     1       i.ne.nButtNew.and.i.ne.nButtMapaMinus.and.
     2       i.ne.nButtMapaPlus)
     3    call FeQuestButtonOff(i)
        enddo
        call ConUpdateMapsButtons
        if(NDimI(KPhase).gt.0) then
          if(smapy.or.DrawPDF) then
            call FeQuestButtonDisable(nButtTMaps)
          else
            do i=4,NDim(KPhase)
              if(iorien(i).lt.4) go to 2055
            enddo
            call FeQuestButtonOff(nButtTMaps)
            go to 2060
2055        call FeQuestButtonDisable(nButtTMaps)
            if(xymap) then
              call FeQuestButtonDisable(nButtX4Length)
            else
              call FeQuestButtonOff(nButtX4Length)
            endif
          endif
        endif
      else
        do i=nButtPrint,nButtErrMaps
          if(ActiveButtBas(i).and.i.ne.nButtNew)
     1      call FeQuestButtonDisable(i)
        enddo
        call FeQuestMouseToButton(nButtNew)
      endif
2060  nCrw=nCrwLocPosition
      if(xymap.and.MapExists) then
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.LocatorType)
          nCrw=nCrw+1
        enddo
      else
        do i=1,3
          call FeQuestCrwClose(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      if(kpdf.ge.2.and.kpdf.ne.4.and.ExistFile(fln(:ifln)//'.m85').and.
     1   ErrMapActive) then
        call FeQuestButtonOff(nButtErrMaps)
      else
        call FeQuestButtonDisable(nButtErrMaps)
      endif
2090  JedeMovie=.false.
      if(MapExists) then
        call PCurves(1)
      else
        call FeQuestEvent(ContourQuest,ich)
        IPlane=0
        NoOfDerivedMaps=0
      endif
2100  HardCopy=0
      ErrFlag=0
      if(Allocated(WorkMap)) deallocate(WorkMap)
2110  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 9000
        else if(CheckNumber.eq.nButtNew) then
          ConButtLabels(IdAtomsOnOff)='At%oms ON'
          ConButtLabels(IdSumOnOff)='S%um ON'
          ConButtLabels(IdTMapOnOff)='%t-map ON'
          ConButtLabels(IdErrOnOff)='%Err ON'
          call ConNewMap(ich)
          if(ich.ne.0) go to 2000
          call iom50(0,0,fln(:ifln)//'.m50')
          if(ErrFlag.ne.0) go to 5000
          call FeClearGrWin
          MapExists=.false.
          Obecny=kpdf.ne.0.and.kpdf.ne.4
          DrawPDF=kpdf.gt.1.and.kpdf.ne.4.and.kpdf.ne.5
          isoucet=0
          if(smapy) then
            m8=81
            call FeQuestButtonLabelChange(nButtSum,
     1                                    ConButtLabels(IdSumOnOff))
            smapy=.false.
          endif
          tmapy=.false.
          call FeQuestButtonLabelChange(nButtTMaps,
     1                                  ConButtLabels(IdTMapOnOff))
          call FeQuestButtonLabelChange(nButtErrMaps,
     1                                  ConButtLabels(IdErrOnOff))
          if(kpdf.le.1.or.kpdf.eq.4.or.kpdf.eq.5) then
            call NactiM81
            if(ErrFlag.ne.0) go to 5000
            if(.not.Obecny) then
              call ConReadKeys
              IPlane=-1
            endif
          else
            if(kpdf.ge.3) nsubs=1
            do i=1,6
              if(i.le.NDim(KPhase)) then
                iorien(i)=i
              else
                iorien(i)=0
              endif
            enddo
            call Del8
          endif
          if(kpdf.ge.1.and.kpdf.ne.4) then
            if(DrawPDF) then
              WizardId=NextQuestId()
              WizardMode=.true.
              WizardTitle=.true.
              WizardLines=16
              WizardLength=650.
              call FeQuestCreate(WizardId,-1.,-1.,WizardLength,
     1                           WizardLines,'x',0,LightGray,0,0)
2200          call ConDefDensity(AtPDF,ich)
              if(ich.ne.0) go to 2350
2220          if(kpdf.ne.6.and.DensityType.eq.2) then
                call ConDefDeform(ich)
                if(ich.lt.0) then
                  go to 2350
                else if(ich.gt.0) then
                  go to 2200
                endif
              endif
2240          call ConDefGenSection(AtPDF,1,ich)
              if(ich.ne.0) then
                if(ich.lt.0) then
                  go to 2350
                else
                  if(kpdf.ne.6.and.DensityType.eq.2) then
                    go to 2220
                  else
                    go to 2200
                  endif
                endif
              endif
              if(kpdf.ge.3) then
                if(kpdf.eq.3) then
                  klic=0
                else if(kpdf.eq.6) then
                  klic=1
                else
                  klic=2
                endif
                call ConDefAtForDenCalc(klic,ich)
                if(ich.lt.0) then
                  go to 2350
                else if(ich.gt.0) then
                  go to 2240
                endif
              else
                do i=1,5
                  BratAnharmPDF(i,1)=AtBratAnharm(i,iapdf(1))
                enddo
              endif
              do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
                SelPDF(i)=.false.
                do j=1,npdf
                  if(iapdf(j).eq.i) then
                    SelPDF(i)=.true.
                    cycle
                  endif
                enddo
              enddo
              call FeQuestRemove(WizardId)
              WizardMode=.false.
            else
              if(NoOfDerivedMaps.le.0) then
                call ConDefGenSection(' ',0,ich)
              else
                call ConMakeGenSection(ich)
              endif
              if(ich.ne.0) go to 2350
              ErrMapActive=.false.
            endif
            if(kpdf.le.3.or.kpdf.eq.5.or.kpdf.eq.6) then
              call ConCalcGeneral
              if(ErrFlag.ne.0) go to 2350
              errdraw=.false.
            endif
          endif
          do i=3,6
            nxfrom(i-2)=1
            nxto(i-2)=nx(i)
            if(Obecny.and.i.eq.3) then
              nxdraw(i-2)=(nx(i)+1)/2
            else
              nxdraw(i-2)=1
            endif
          enddo
          irec=-1
          irecold=-1
          reconfig=.true.
          if(xymap) call TrPor
          call SetTr
          if(kpdf2-kpdf1.le.1) go to 5000
          call KresliMapu(0)
          KeysSaved=.false.
2350      if(WizardMode) call FeQuestRemove(WizardId)
          if(kpdf1.ge.kpdf2) call FeQuestButtonDisable(nButtNew)
          go to 2000
        else if(CheckNumber.eq.nButtSave) then
          if(HardCopy.eq.0) then
            if(Obecny) then
              i=0
              j=0
            else
              i=10
              j=0
            endif
            call FeSavePicture('section',i,j)
            if(HardCopy.lt.0) HardCopy=0
          endif
          if(HardCopy.lt.HardCopyNum) then
            if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                              HardCopy.ne.HardCopyPCX)) then
              call KresliMapu(0)
            else
              call FeHardCopy(HardCopy,'open')
              call KresliMapu(0)
              call FeHardCopy(HardCopy,'close')
            endif
          else if(HardCopy.ge.HardCopyNum) then
            if(kpdf.eq.0.and.mod(HardCopy,100).lt.HardCopyXPlor) then
              do i=2,3
                if(abs(CellPar(1,1,KPhase)-CellPar(i,1,KPhase)).gt..001)
     1            go to 2450
              enddo
              do i=4,6
                if(abs(CellPar(i,1,KPhase)-90.).gt..001) go to 2450
              enddo
              go to 2460
2450          call FeChybne(-1.,-1.,
     1                      'the maps are related to the crystal',
     2                      ' non-orthogonal system!!!',Warning)
            endif
2460        call OpenFile(85,HCFileName,'formatted','unknown')
            ivse=HardCopy/100
            HardCopyPom=mod(HardCopy,100)
            if(ivse.eq.0) then
              zmn=zmap(1)
              zmx=zmap(1)
              NzCopy=1
            else
              zmn=xmin(3)
              zmx=xmax(3)
              NzCopy=nx(3)
            endif
            allocate(WorkMap(nxny))
            if(mod(HardCopy,100).eq.HardCopySTF) then
              write(85,'(''NAME '',80a1)')
     1          (StructureName(i:i),i=1,idel(StructureName))
              write(85,'(''RANK 3'')')
              write(85,'(''DIMENSIONS '',3i5)')(nx(i),i=1,2),NzCopy
              write(85,'(''BOUNDS '',6f9.4)')(xmin(i),xmax(i),i=1,2),
     1                                       zmn,zmx
              write(85,'(''SCALAR'')')
              write(85,'(''ORDER COLUMN'')')
              write(85,'(''DATA'')')
              i=8
              k=10
              t6='stf'
            else if(mod(HardCopy,100).eq.HardCopyXPlor) then
              call ConMakeXplorFileProlog(85,NzCopy,0)
              t6='xplor'
            else if(mod(HardCopy,100).eq.HardCopyJMAP3D) then
              if(Obecny) then
                write(Veta,102)(dx(i),i=1,3)
                write(Veta(31:),102)(90.,i=1,3)
              else
                m=0
                do i=1,3
                  if(nx(i).le.1) cycle
                  m=m+1
                  CellParP(m)=CellParCon(m)*dx(i)
                enddo
                call ConCellParNorm(CellParP)
                write(Veta,102)(CellParP(i),i=1,3)
                write(Veta(31:),102)(CellParCon(i),i=4,6)
              endif
              call zdrcniCisla(Veta,6)
              write(85,FormA1)(Veta(i:i),i=1,idel(Veta))
              write(85,'(2(i4,'',''),i4)')(nx(i),i=1,2),NzCopy
              t6='JMAP3D'
            else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
              write(85,'(''INFO  DOWN, ACROSS AND SECTION'')')
              write(85,'(''TRAN'')')
              write(85,103)((trob(i,j),j=1,3),i=1,3)
              call SetRealArrayTo(xpp,3,0.)
              call Prevod(1,xpp,xp)
              write(85,103)(xp(i),i=1,3)
              write(85,'(''CELL'')')
              m=0
              do i=1,3
                if(nx(i).le.1) cycle
                m=m+1
                CellParP(m)=CellParCon(m)
              enddo
              call ConCellParNorm(CellParP)
              write(85,103)(CellParP(i),i=1,3)
              write(85,103)(CellParCon(i)*ToRad,i=4,6)
              write(85,'(''L14'')')
              write(85,103)(xmin(i),dx(i),xmax(i),1.,i=1,2),
     1                      zmn,dx(3),zmx,1.
              write(85,'(''SIZE'')')
              write(85,104)(nx(i),i=1,2),NzCopy
              t6='fou'
            else if(mod(HardCopy,100).eq.HardCopyGrd) then
              call ConMakeGrdFileProlog(85,NzCopy,0)
              t6='grd'
            else if(mod(HardCopy,100).eq.HardCopyNum) then
              if(ivse.eq.0) then
                write(85,101)(nx(i),i=1,2),(1,i=3,NDim(KPhase))
              else
                write(85,101)(nx(i),i=1,NDim(KPhase))
              endif
              t6='ASCII'
            endif
            if(ivse.eq.0) then
              read(m8,rec=irec+1,err=2510)(WorkMap(j),j=1,nxny)
              if(mod(HardCopy,100).eq.HardCopySTF) then
                write(85,SaveFormat)(WorkMap(j),j=1,nxny)
                write(85,'(''END'')')
              else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
                write(85,'(''BLOCK'')')
                write(85,104) nxny
                write(85,103)(WorkMap(j),j=1,nxny)
              else if(mod(HardCopy,100).eq.HardCopyGrd) then
                m=0
                do j=1,nx(2)
                  write(85,107)(WorkMap(k),k=m+1,m+nx(1))
                  m=m+nx(1)
                enddo
              else
                write(85,SaveFormat)(WorkMap(i),i=1,nxny)
              endif
              write(TextInfo(1),form1)(nx(i),i=1,2),
     1                                (1,i=3,NDim(KPhase)),nxny
            else
              call FeFlowChartOpen(-1.,-1.,1,NzCopy,'Transporting '//
     1          'data to the '//t6(:idel(t6))//' file',' ',' ')
              Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
              do i=1,NzCopy
                if(mod(HardCopy,100).eq.HardCopyXplor)
     1            write(85,'(i5)') i-1
                j=i
                call FeFlowChartEvent(j,is)
                if(is.ne.0) then
                  close(85,status='delete')
                  call FeFlowChartRemove
                  go to 2000
                endif
                read(m8,rec=i+Zdvih,err=2510)(WorkMap(j),j=1,nxny)
                if(mod(HardCopy,100).eq.HardCopyJMAP3D) then
                  m=0
                  do j=1,nx(2)
                    do k=1,nx(1)
                      m=m+1
                      write(85,100) k-1,j-1,i-1,WorkMap(m)
                    enddo
                  enddo
                else if(mod(HardCopy,100).eq.HardCopyMCEFou) then
                  write(85,'(''BLOCK'')')
                  write(85,104) nxny
                  write(85,103)(WorkMap(j),j=1,nxny)
                else if(mod(HardCopy,100).eq.HardCopyGrd) then
                  m=0
                  do j=1,nx(2)
                    write(85,107)(WorkMap(k),k=m+1,m+nx(1))
                    m=m+nx(1)
                  enddo
                else if(mod(HardCopy,100).eq.HardCopyXplor) then
                  write(85,'(5e15.6)')(WorkMap(j),j=1,nxny)
                else
                  write(85,SaveFormat)(WorkMap(j),j=1,nxny)
                endif
              enddo
              if(mod(HardCopy,100).eq.HardCopySTF) write(85,'(''END'')')
              call FeFlowChartRemove
              irecold=-1
              write(TextInfo(1),form1)(nx(i),i=1,NDim(KPhase)),nxny*nmap
            endif
            go to 2550
2510        call FeReadError(81)
            close(85,status='delete')
            NInfo=1
            TextInfo(1)='No output file created'
            go to 2800
2550        call CloseIfOpened(85)
            ninfo=2
            call zhusti(TextInfo(1))
            i=idel(TextInfo(1))
            TextInfo(1)='All '//TextInfo(1)(:i)//' points were copied '
            if(mod(HardCopy,100).eq.HardCopyNum) then
              TextInfo(2)='to the ASCII file : '//HCFileName
            else
              TextInfo(2)='to the file : '//HCFileName
            endif
            if(Allocated(WorkMap)) deallocate(WorkMap)
2800        call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            call KresliMapu(0)
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            HardCopy=0
          endif
        else if(CheckNumber.eq.nButtRun3dMaps) then
          Patterson=Mapa.eq.1.or.Mapa.eq.2.or.Mapa.eq.9
          ln=NextLogicNumber()
          if(IdCall3dMaps.eq.IdCall3dNone) go to 2000
          if(xyzmap) then
            WholeCell=.true.
            do i=1,3
              pom=xmax(i)-xmin(i)+dx(i)
              if(abs(pom-1.).gt..001.and.WholeCell) then
                WholeCell=.false.
                exit
              endif
            enddo
          else
            WholeCell=.false.
          endif
          t256=' '
          if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1       IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
            FileName=fln(:ifln)//'_tmp.grd'
            if(IdCall3dMaps.eq.IdCall3dMCE.and.NAtAll.gt.0.and.
     1         .not.Obecny.and..not.Patterson) then
              t256=fln(:ifln)//'_tmp.cif'
              call CIFMakeBasicTemplate(t256,0)
              call CIFUpdate(t256,.true.,0)
            endif
          else
            if(Obecny) then
              FileName=fln(:ifln)//'_tmp.xsf'
            else
              FileName=fln(:ifln)//'_tmp.xplor'
            endif
          endif
          call OpenFile(ln,FileName,'formatted','unknown')
          if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1       IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
            if(obecny.or.IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
              i=0
            else
              i=1
            endif
            call ConMakeGrdFile(ln,nx(3),i)
          else
            do i=3,NDim(KPhase)
              if(nx(i).gt.1) then
                nx3=nx(i)
                exit
              endif
            enddo
            if(Obecny) then
              call ConMakeXSFFile(ln,nx3,0)
            else
              call ConMakeXPlorFile(ln,nx3,0)
            endif
          endif
          call CloseIfOpened(ln)
          if(IdCall3dMaps.eq.IdCall3dVESTA) then
            if(.not.Obecny) then
              MakeCIFForGraphicViewer=.true.
              call SetRealArrayTo(TMat,NDimQ(KPhase),0.)
              k=0
              do i=1,NDim(KPhase)
                do j=1,NDim(KPhase)
                  k=k+1
                  if(j.eq.iorien(i)) TMat(k)=xmax(i)-xmin(i)+dx(i)
                enddo
              enddo
              call MatInv(TMat,TMatI,pom,NDim(KPhase))
              call MatBlock3(TMatI,rmx,NDim(KPhase))
              call SrotB(rmx,rmx,rmb)
              call UnitMat(TMat,NDim(KPhase))
              call MatInv(TMat,TMatI,pom,NDim(KPhase))
              open(m40,file=fln(:ifln)//'_tmp.m40')
              do i=1,5
                write(m40,FormA)
              enddo
              NaUse=0
              if(WholeCell) then
                if(iorien(1).le.3.and.iorien(2).le.3) then
                  do i=1,NAtCalc
                    if(kswa(i).eq.KPhase) NaUse=NaUse+1
                  enddo
                endif
                do i=1,NAtCalc
                  if(kswa(i).eq.KPhase) then
                    call MultM(rmx,x(1,i),xp,3,3,1)
                    itfp=min(itf(i),2)
                    if(itf(i).gt.1) then
                      call MultM(rmb,beta(1,i),xp(4),6,6,1)
                      do j=1,6
                        beta(j,i)=beta(j,i)/urcp(j,iswa(i),KPhase)
                      enddo
                    else
                      xp(4)=beta(1,i)/episq
                      call SetRealArrayTo(xp(5),5,0.)
                    endif
                    write(m40,108) Atom(i),isf(i),itfp,ai(i),
     1                             (xp(j),j=1,9)
                  endif
                enddo
              else
                do i=1,3
                  CellPar(i,1,KPhase)=
     1              CellParCon(i)*(xmax(i)-xmin(i)+dx(i))
                enddo
                do i=4,6
                  CellPar(i,1,KPhase)=CellParCon(i)
                enddo
                call ConCellParNorm(CellPar(1,1,KPhase))
                Grupa(KPhase)='P1'
                NSymm(KPhase)=1
                NSymmN(KPhase)=1
                NLattVec(KPhase)=1
                NGrupa(KPhase)=1
                CrSystem(KPhase)=1
                Lattice(KPhase)='P'
                Veta=fln(:ifln)//'_dratoms_vesta.tmp'
                if(ExistFile(Veta)) then
                  lna=NextLogicNumber()
                  call OpenFile(lna,Veta,'formatted','unknown')
                  if(ErrFlag.ne.0) go to 2860
2820              read(lna,'(2i3,6f10.5)',end=2860) ia,NumAt,xpp
                  call MultM(rmx,xpp,xp,3,3,1)
                  do i=1,3
                    if(xp(i).lt.0.or.xp(i).gt.1.) go to 2820
                  enddo
                  NaUse=NaUse+1
                  write(m40,108) Atom(ia),isf(ia),1,1.,(xp(i),i=1,3),
     1                           .01
                  go to 2820
2860              call CloseIfOpened(lna)
                endif
              endif
              rewind m40
              do i=1,5
                read(m40,FormA)
              enddo
              n=NAtCalc
              NAtCalc=NaUse
              Veta=fln(:ifln)//'_tmp.cif'
              call CIFMakeBasicTemplate(Veta,1)
              call CIFUpdate(Veta,.true.,1)
              close(m40,status='delete')
              NAtCalc=n
              MakeCIFForGraphicViewer=.false.
              Veta=' '
            endif
            FileName=fln(:ifln)//'_tmp.vesta'
            call OpenFile(ln,FileName,'formatted','unknown')
            write(ln,FormA) '#VESTA_FORMAT_VERSION 2'
            write(ln,FormA)
            write(ln,FormA) 'IMPORT_STRUCTURE'
            if(Obecny) then
              write(ln,FormA) fln(:ifln)//'_tmp.xsf'
            else
              write(ln,FormA) fln(:ifln)//'_tmp.cif'
            endif
            write(ln,FormA)
            write(ln,FormA) 'IMPORT_DENSITY'
            if(Obecny) then
              write(ln,FormA) '+1.00000 '//fln(:ifln)//'_tmp.xsf'
            else
              write(ln,FormA) '+1.00000 '//fln(:ifln)//'_tmp.xplor'
              write(ln,FormA)
              write(ln,FormA) 'TRANM'
              write(ln,'(3f10.6,9i4)')(0.,i=1,3),(nint(TMat(i)),i=1,9)
            endif
            write(ln,FormA)
            call CloseIfOpened(ln)
          endif
          Veta=Call3dMaps(:idel(Call3dMaps))
          if(IdCall3dMaps.ne.IdCall3dMoleCoolQt) then
            Veta=Call3dMaps(:idel(Call3dMaps))//' '//
     1           FileName(:idel(FileName))//' '//t256(:idel(t256))
          else
            call FeFillTextInfo('contour1.txt',0)
            i=index(TextInfo(1),'#$%')
            TextInfo(1)(i:)=FileName(:idel(FileName))//'"'
            if(.not.Obecny) then
              do i=4,6
                if(abs(CellPar(i,1,KPhase)-90.).gt..001) then
                  NInfo=NInfo+1
                  TextInfo(NInfo)='WARNING: The maps are related to '//
     1                            'the non-orthogonal system which '//
     2                            'not supported by the grd format.'
                  NInfo=NInfo+1
                  TextInfo(NInfo)='For this reason the maps in '//
     1                            'MoleCoolQt are deformated.'
                  exit
                endif
              enddo
            endif
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeSystem(Veta)
          call DeleteFile(FileName)
          if(IdCall3dMaps.eq.IdCall3dMCE.and.t256.ne.' ')
     1       call DeleteFile(t256)
          if(IdCall3dMaps.eq.IdCall3dVESTA) then
            call DeleteFile(fln(:ifln)//'_tmp.xplor')
            call DeleteFile(fln(:ifln)//'_tmp.cif')
            call DeleteFile(fln(:ifln)//'_tmp.xsf')
            call iom40(0,0,fln(:ifln)//'.m40')
            call iom50(0,0,fln(:ifln)//'.m50')
            if(NDimI(KPhase).gt.0) call TrOrtho(0)
          endif
        else if(CheckNumber.eq.nButtMapaMinus) then
          call FeQuestButtonOff(nButtMapaMinus)
          call KresliMapu(-1)
          if(nButtInterrupt.gt.0) then
            call ConUpdateMapsButtons
            if(ButtonState(nButtMapaMinus).eq.ButtonOff) then
              CheckType=EventButton
              CheckNumber=nButtInterrupt
              go to 2110
            endif
          endif
        else if(CheckNumber.eq.nButtMapaPlus) then
          call FeQuestButtonOff(nButtMapaPlus)
          call KresliMapu( 1)
          if(nButtInterrupt.gt.0) then
            call ConUpdateMapsButtons
            if(ButtonState(nButtMapaPlus).eq.ButtonOff) then
              CheckType=EventButton
              CheckNumber=nButtInterrupt
              go to 2110
            endif
          endif
        else if(CheckNumber.eq.nButtGoto) then
          call GoToMap(ich)
          if(ich.eq.0) go to 4600
        else if(CheckNumber.eq.nButtMovie) then
          call ConDefMovie(ich)
          if(ich.ne.0) go to 2000
          if(nLblMovie.le.0) then
            xpom=350.
            ypom=YBottomMargin*.5
            Veta='Press Esc to interrupt the movie'
            call FeQuestAbsLblMake(ContourQuest,xpom,ypom,Veta,
     1                             'L','B')
            nLblMovie=LblLastMade
          else
            call FeQuestLblOn(nLblMovie)
          endif
          JedeMovie=.true.
          irpt=0
          isv=0
          HardCopyOld=HardCopy
3000      irpt=irpt+1
          if(irpt.gt.MovieRepeat.and.MovieRepeat.ne.0) go to 3200
          call CopyVekI(nxMovieFr,nxdraw,NDim(KPhase)-2)
3050      if(irpt.eq.1.and.MovieFileName.ne.' ') then
            isv=isv+1
            write(HCFileName,'(''_'',i4)') isv
            do i=1,idel(HCFileName)
              if(HCFileName(i:i).eq.' ') HCFileName(i:i)='0'
            enddo
            HCFileName=MovieFileName(:idel(MovieFileName))//
     1               HCFileName(:idel(HCFileName))//
     2               HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
          else
            HardCopy=0
          endif
          call KresliMapu( 0)
          if(ErrFlag.ne.0) go to 3200
          if(HardCopy.ne.0) then
            i=HardCopy
            HardCopy=0
            call KresliMapu( 0)
            if(ErrFlag.ne.0) go to 3200
            HardCopy=i
          endif
          TimeStart=FeGetSystemTime()
3100      call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                        ConImageFile,0)
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile)
            if(FeYesNo(-1.,-1.,
     1                  'Do you want really to interrupt it?',1))
     2        go to 3200
          else if(EventType.eq.EventAscii.and.
     1            char(EventNumber).eq.' ') then
            call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                        ConImageFile,0)
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile)
3120        call FeEvent(1)
            if(EventType.ne.EventAscii.or.char(EventNumber).ne.' ')
     1        go to 3120
          else
            if(FeGetSystemTime()-TimeStart.le.
     1         nint(DelayTimeForMovie*1000.)) go to 3100
          endif
          do i=1,NDim(KPhase)-2
            if(nxlast(i).lt.nxMovieTo(i)) go to 3150
          enddo
          go to 3000
3150      nxdraw(1)=nxdraw(1)+1
          if(nxdraw(1).gt.nxMovieTo(1)) then
            nxdraw(1)=nxMovieFr(1)
            nxdraw(2)=nxdraw(2)+1
            if(nxdraw(2).gt.nxMovieTo(2)) then
              nxdraw(2)=nxMovieFr(2)
              nxdraw(3)=nxdraw(3)+1
              if(nxdraw(3).gt.nxMovieTo(3)) then
                nxdraw(3)=nxMovieFr(3)
                nxdraw(4)=nxdraw(4)+1
                if(nxdraw(4).gt.nxdraw(4)) then
                  call CopyVekI(nxMovieTo,nxdraw,4)
                endif
              endif
            endif
          endif
          go to 3050
3200      if(MovieFileName.ne.' ') then
            TextInfo(1)='The movie maps were recorded to files: '//
     1        MovieFileName(:idel(MovieFileName))//'_####'//
     2        HCExtension(HardCopyOld)(:idel(HCExtension(HardCopyOld)))
            NInfo=1
            WaitTime=10000
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeQuestLblOff(nLblMovie)
        else if(CheckNumber.eq.nButtContours) then
          call DefContour(ich)
          if(ich.eq.0) go to 4600
        else if(CheckNumber.eq.nButtAtomsDefine) then
          call ConDrawAtomsEdit
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     ConImageFile,-1)
          ConButtLabels(IdAtomsOnOff)='At%oms OFF'
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
          AtomsOn=.true.
          call ConDrawAtoms(zmap)
        else if(CheckNumber.eq.nButtAtomsDraw) then
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     ConImageFile,-1)
          if(AtomsOn) then
            ConButtLabels(IdAtomsOnOff)='At%oms ON'
            AtomsOn=.false.
            call DeleteFile(fln(:ifln)//'_dratoms_grd.tmp')
            call DeleteFile(fln(:ifln)//'_dratoms_vesta.tmp')
          else
            if(ErrFlag.eq.0.and.DrawAtN.gt.0) then
              ConButtLabels(IdAtomsOnOff)='At%oms OFF'
              AtomsOn=.true.
              call ConDrawAtoms(zmap)
            endif
          endif
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
          go to 2000
        else if(CheckNumber.eq.nButtAtomsFill) then
          call ConFillAtoms(ich)
          ConButtLabels(IdAtomsOnOff)='At%oms OFF'
          call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                  ConButtLabels(IdAtomsOnOff))
        else if(CheckNumber.eq.nButtCurves) then
          call PCurves(0)
        else if(CheckNumber.eq.nButtX4Length) then
          call DefLength(ich)
          reconfig=ich.eq.0
          if(reconfig) go to 4600
        else if(CheckNumber.eq.nButtSum) then
          if(smapy) then
            m8=m8Sum
            ConButtLabels(IdSumOnOff)='S%um ON'
            smapy=.false.
          else
            m8Sum=m8
            call ConSummMapsDef
            if(ErrFlag.ne.0.or..not.smapy) go to 2000
            ConButtLabels(IdSumOnOff)='S%um OFF'
          endif
          call FeQuestButtonLabelChange(nButtSum,
     1                                  ConButtLabels(IdSumOnOff))
          go to 4500
        else if(CheckNumber.eq.nButtSearch) then
          call ConSearch
        else if(CheckNumber.eq.nButtGlobal) then
          call ConSearchGlobal
        else if(CheckNumber.eq.nButtErrMaps) then
          irecold=-1
          errdraw=.not.errdraw
          if(errdraw) then
            m8=83
            DiffMapa=.false.
            DrawPos=1
            DrawNeg=0
          else
            m8=82
            if(DensityType.eq.2.or.DensityType.eq.3) then
              DiffMapa=.true.
              DrawPos=1
              DrawNeg=1
            else
              DiffMapa=.false.
              DrawPos=1
              DrawNeg=0
            endif
          endif
          pom=DminErr
          DminErr=Dmin
          Dmin=pom
          pom=DmaxErr
          DmaxErr=Dmax
          Dmax=pom
          i=ContourTypeErr
          ContourTypeErr=ContourType
          ContourType=i
          if(ErrDraw) then
            ConButtLabels(IdErrOnOff)='%Err OFF'
          else
            ConButtLabels(IdErrOnOff)='%Err ON'
          endif
          call FeQuestButtonLabelChange(nButtErrMaps,
     1                                  ConButtLabels(IdErrOnOff))
          go to 4600
        else if(CheckNumber.eq.nButtTMaps) then
          if(tmapy) then
            m8=m8TMaps
            ConButtLabels(IdTMapOnOff)='%t-map ON'
            tmapy=.false.
          else
            m8TMaps=m8
            call TrX4t
            if(ErrFlag.ne.0) go to 2000
            ConButtLabels(IdTMapOnOff)='%t-map OFF'
          endif
          call FeQuestButtonLabelChange(nButtTMaps,
     1                                  ConButtLabels(IdTMapOnOff))
          call SetTr
          go to 4500
        else if(CheckNumber.eq.nButtOptions) then
          i=ConTintType
          icp=ConPosCol
          icn=ConNegCol
          call ConOptions
          if(i.ne.ConTintType.or.icp.ne.ConPosCol.or.
     1                                icn.ne.ConNegCol) then
            call KresliMapu(0)
          else if(AtomsOn) then
            call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       ConImageFile,-1)
            call ConDrawAtoms(zmap)
          endif
        endif
        go to 2000
4500    read(m8,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                 iorien,mapa,nsubs,SatelityByly,
     2                 nonModulated(KPhase)
        Patterson=Mapa.eq.1.or.Mapa.eq.2.or.Mapa.eq.9
        nsubs=mod(nsubs,10)
        read(m8,rec=nmap+2) Dmax,Dmin
        call SetContours(0)
        call SetIntArrayTo(nxdraw,4,1)
        call SetIntArrayTo(nxfrom,4,1)
        call CopyVekI(nx(3),nxto,4)
        if(Obecny) then
          nxdraw(1)=(nx(3)+1)/2
        else
          nxdraw(1)=0
        endif
        irec=-1
        irecold=-1
4600    call KresliMapu(0)
        go to 2000
5000    call FeQuestButtonOff(CheckNumber)
        CheckType=EventButton
        CheckNumber=nButtQuit
        go to 2110
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown
     1        .and.MapExists) then
        do j=1,11
          if(ButtonStateQuest(nButtMovie+j).eq.ButtonOff) then
            nmen1(j)=1
          else
            nmen1(j)=0
          endif
        enddo
        j=FeMenuNew(-1.,-1.,-1.,ConButtLabels(IdContours),nmen1,1,11,
     1              1,0)
        if(j.lt.1) go to 2090
        CheckType=EventButton
        CheckNumber=nButtMovie+j
        go to 2100
      else if(CheckType.eq.EventResize) then
        NovyStart=.true.
        go to 9000
      else
        go to 2090
      endif
      go to 2000
9000  do i=1,4
        call FeWInfRemove(i)
      enddo
      call FeQuestRemove(ContourQuest)
      if(Allocated(WorkMap)) deallocate(WorkMap)
      if(NovyStart) go to 1100
      call Del8
      AllowResizing=.false.
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call FeBottomInfo(' ')
      return
100   format(3(i4,','),e20.10)
101   format(6i5)
102   format(3f10.4)
103   format(f15.8)
104   format(i8)
105   format(3i15)
106   format(3f15.4)
107   format(6e13.5)
108   format(a8,2i3,4x,4f9.6/6f9.6)
      end
