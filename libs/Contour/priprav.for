      subroutine priprav(itf)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      w11=w1**2
      w22=w2**2
      w33=w3**2
      w12=w1*w2
      w13=w1*w3
      w23=w2*w3
      if(itf.gt.2) then
        wmp1=w11-p11
        wmp2=w22-p22
        wmp3=w33-p33
        wm3p1=w11-3.*p11
        wm3p2=w22-3.*p22
        wm3p3=w33-3.*p33
        if(itf.gt.3) then
          pom1=w11-5.449490*p11
          pom2=w11-0.550510*p11
          pm63_1=pom1*pom2
          pom1=w22-5.449490*p22
          pom2=w22-0.550510*p22
          pm63_2=pom1*pom2
          pom1=w33-5.449490*p33
          pom2=w33-0.550510*p33
          pm63_3=pom1*pom2
          if(itf.gt.4) then
            pom1=w11-1.837722*p11
            pom2=w11-8.162278*p11
            pm1015_1=pom1*pom2
            pom1=w22-1.837722*p22
            pom2=w22-8.162278*p22
            pm1015_2=pom1*pom2
            pom1=w33-1.837722*p33
            pom2=w33-8.162278*p33
            pm1015_3=pom1*pom2
            if(itf.gt.5) then
              wmp110_1=(w11-11.050687*p11)
              wmp3_1=(w11-0.380327*p11)
              wmp35_1=(w11-3.568986*p11)
              wmp110_2=(w22-11.050687*p22)
              wmp3_2=(w22-0.380327*p22)
              wmp35_2=(w22-3.568986*p22)
              wmp110_3=(w33-11.050687*p33)
              wmp3_3=(w33-0.380327*p33)
              wmp35_3=(w33-3.568986*p33)
            endif
          endif
        endif
      endif
      return
      end
