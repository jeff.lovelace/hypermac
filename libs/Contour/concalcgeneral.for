      subroutine ConCalcGeneral
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(6),ix456(3),x4p(3),DenSupl(3),WorkMap(:,:),CalcMap(:)
      integer UseTabsIn
      real InputMap(:),ErrMap(:)
      character*80 t80
      logical konec,uzje(:)
      allocatable uzje,InputMap,WorkMap,ErrMap,CalcMap
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      xpom=30.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      xpom=xpom+30.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=xpom+103.
      call FeTabsAdd(xpom,UseTabs,IdLeftTab,' ')
      xpom=xpom+18.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      konec=.false.
      nxrnyr=nxr(1)*nxr(2)
      allocate(uzje(nxrnyr),CalcMap(nxrnyr))
      if(ErrMapActive) allocate(ErrMap(nxrnyr))
      if(DrawPDF) then
        nxr(4)=ntpdf
        if(ntpdf.gt.1) then
          dxr(4)=(tpdfk-tpdfp)/(ntpdf-1)
        else
          dxr(4)=1.
        endif
        xminr(4)=tpdfp
        xmaxr(4)=tpdfk
        nxr(5)=1
        xminr(5)=0.
        xmaxr(5)=0.
        nxr(6)=1
        xminr(6)=0.
        xmaxr(6)=0.
        if(NDim(KPhase).gt.3) tmapy=.true.
        call CopyVekI(nxr(4),nx(4),3)
        if(DensityType.eq.5.or.DensityType.eq.6) then
          n=3
        else
          n=1
        endif
        allocate(WorkMap(nxrnyr,n))
      else
        npdf=1
        call CopyVekI(nx(4),nxr(4),3)
        call CopyVek(dx(4),dxr(4),3)
        call CopyVek(xmin(4),xminr(4),3)
        call CopyVek(xmax(4),xmaxr(4),3)
        nx3=nx(3)
        do i=1,NDim(KPhase)
          pom=xmax(i)-xmin(i)
          if(pom.lt.1.-dx(i)-.0001.or.pom.ge.1.) then
            dxp(i)=0.
          else
            dxp(i)=dx(i)
            if(i.eq.3) nx3=nx3+1
          endif
        enddo
        nmapa=nx3
        allocate(InputMap(nmapa*nxny))
      endif
      nmapar=nxr(3)*nxr(4)*nxr(5)*nxr(6)
      call CloseIfOpened(82)
      call OpenMaps(82,fln(:ifln)//'.l82',nxrnyr,1)
      if(ErrFlag.ne.0) go to 9000
      write(82,rec=1) nxr,nxrnyr,nmapar,(xminr(i),xmaxr(i),i=1,6),dxr,
     1                (i,i=1,6),mapa,nsubs,SatelityByly,
     2                nonModulated(KPhase)
      if(ErrMapActive) then
        call CloseIfOpened(83)
        call OpenMaps(83,fln(:ifln)//'.l83',nxrnyr,1)
        if(ErrFlag.ne.0) go to 9000
        write(83,rec=1) nxr,nxrnyr,nmapar,(xminr(i),xmaxr(i),i=1,6),dxr,
     1                  (i,i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
        error=100.
      endif
      DminErr=0.
      DmaxErr=0.
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      SumOverMap=0.
      if(DrawPDF.and.ErrMapActive) call ConReadM85
      if(kpdf.eq.1) then
        t80='Calculation of general section'
      else if(kpdf.eq.2) then
        t80='Calculation of p.d.f.'
      else if(kpdf.eq.3) then
        t80='Calculation of j.p.d.f.'
      else if(kpdf.eq.5.or.kpdf.eq.6) then
        t80=MenDensity(DensityType+1)
        t80='Calculation of '//t80(:idel(t80))//' map'
      endif
      n=0
      nmap=0
      nx456=nxr(4)*nxr(5)*nxr(6)
      if(npdf.le.0) then
        call FeChybne(-1.,-1.,'no atoms specified to calculate the'//
     1                'map.',' ',SeriousError)
        go to 9000
      endif
      nvse=npdf
      if(imax/nvse.lt.nxrnyr) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nxrnyr
      endif
      if(imax/nvse.lt.nxr(3)) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nxr(3)
      endif
      if(imax/nvse.lt.nx456) then
        nvse=imax
        go to 1200
      else
        nvse=nvse*nx456
      endif
1200  if(nvse.le.0) then
        call FeChybne(-1.,-1.,'the desired region has zero or negative '
     1                //'volume.',' ',SeriousError)
        go to 9000
      endif
      if(.not.ErrMapActive.or.kpdf.eq.1)
     1  call FeFlowChartOpen(-1.,120.,max(nint(float(nvse)*.005),10),
     2                       nvse,t80,' ',' ')
      if(allocated(FlagMap)) deallocate(FlagMap)
      allocate(FlagMap(nx456*nxr(3)))
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(nxrnyr))
      NActualMap=nxrnyr
      do it=1,nx456
        call RecUnpack(it,ix456,nx(4),3)
        do i=1,3
          j=i+3
          xx(j)=xminr(j)+(ix456(i)-1)*dxr(j)
        enddo
        do iz=1,nxr(3)
          zout=xminr(3)+dxr(3)*float(iz-1)
          izz=1
          call SetLogicalArrayTo(uzje,nxrnyr,.false.)
          nmap=nmap+1
          FlagMap(nmap)=0
          do ix4=1,nx456
            call RecUnPack(ix4,ix456,nx(4),3)
            do i=1,3
              j=i+3
              x4p(i)=xmin(j)+(ix456(i)-1)*dx(j)
            enddo
1300        if(DrawPDF) then
              if(DensityType.eq.5.or.DensityType.eq.6) then
                call SetRealArrayTo(WorkMap,3*nxrnyr,0.)
              else
                call SetRealArrayTo(CalcMap,nxrnyr,0.)
              endif
              if(ErrMapActive) then
                call SetRealArrayTo(ErrMap,nxrnyr,0.)
                call ConReadM85
                if(ErrMapActive) then
                  pom=ran1(i)
                  if(NDimI(KPhase).gt.0) then
                    Cislo='t'
                    pom=xx(4)
                  else
                    Cislo='z'
                    pom=zout
                  endif
                  write(t80,101) Cislo(1:1),Tabulator,pom,Tabulator,
     1                           Tabulator,0,Tabulator,Tabulator,error
                  if(it.eq.1.and.iz.eq.1)
     1              call FeOpenInterrupt(-1.,-1.,t80)
                  konec=.false.
                endif
              else
                mcmax=0
              endif
            else
              kk=1
              do km=1,nmapa
                if(km.gt.nx(3)) then
                  izzp=izz
                  izz=izz-2*nx(3)+km
                else
                  izz=izz+1
                endif
                read(81,rec=izz,err=8000)(InputMap(i),i=kk,kk+nxny-1)
                if(km.gt.nx(3)) izz=izzp
                kk=kk+nxny
              enddo
            endif
            imc=0
1500        if(imc.gt.mcmax) go to 5100
            if(ErrMapActive.and.imc.gt.0) then
              call ConHitMC
              call SetRealArrayTo(WorkMap,nxrnyr,0.)
            endif
            do ia=1,npdf
              if(DrawPDF) then
                ip=ipor(ia)
                fpdfi=fpdf(ip)
                if(fpdfi.le.0.) then
                  n=n+nxrnyr
                  cycle
                endif
                if(kpdf.eq.6) then
                  call ConDensity(pom,ia,0,DenSupl,ich)
                else
                  xx(1)=pdfat(xa,xx(4),ia,0,ich)
                endif
                if(ich.eq.1) then
                  if(imc.eq.0) then
                    n=iapdf(ip)
                    t80='isn''t positive definite'
                    if(NDimI(KPhase).gt.0)
     1                write(t80(25:),'(''for t='',f6.3)') xx(4)
                    call FeChybne(-1.,YBottomMessage,
     1                'ADP tensor of the atom '//
     2                atom(n)(:idel(atom(n))),t80,SeriousError)
                    if(NDimI(KPhase).le.0) then
                      if(.not.ErrMapActive.or.kpdf.eq.1) then
                        call FeFlowChartRemove
                      else if(ErrMapActive) then
                        call FeCloseInterrupt
                      endif
                      go to 9000
                    else
                      FlagMap(nmap)=1
                      go to 1900
                    endif
                  else
                    go to 1500
                  endif
                else if(ich.eq.2) then
                  FlagMap(nmap)=2
                  go to 1900
                endif
                go to 2000
1900            n=n+npdf*nxrnyr*nxr(3)
                go to 6100
              endif
2000          j=0
              do iy=1,nxr(2)
                do ix=1,nxr(1)
                  j=j+1
                  if(DrawPDF) then
                    do i=1,3
                      xx(i)=xob(i)+xr(i)*float(ix-1)+
     1                             yr(i)*float(iy-1)+zr(i)*float(iz-1)
                      if(kpdf.eq.6) xx(i)=xx(i)-xpdf(i,ip)
                    enddo
                    if(kpdf.eq.6) then
                      call multm(TrToOrtho,xx,xo,3,3,1)
                      dd=VecOrtLen(xo,3)
                      if(dd.gt.CutOffDist) then
                        if(.not.ErrMapActive)
     1                    call FeFlowChartEvent(n,is)
                        if(is.ne.0) then
                          call FeBudeBreak
                          if(ErrFlag.ne.0) go to 9000
                        endif
                        uzje(j)=.true.
                        cycle
                      endif
                    endif
                    if(imc.eq.0) then
                      if(kpdf.eq.6) then
                        call ConDensity(dd,ia,1,DenSupl,ich)
                        if(DensityType.ne.5) pom=DenSupl(1)*fpdfi
                      else
                        pom=pdfat(xx,t,ia,1,ich)*fpdfi
                      endif
                      if(DensityType.eq.5.or.DensityType.eq.6) then
                        do i=1,3
                          WorkMap(j,i)=WorkMap(j,i)+DenSupl(i)*fpdfi
                        enddo
                      else
                        CalcMap(j)=CalcMap(j)+pom
                        SumOverMap=SumOverMap+pom
                      endif
                      uzje(j)=.true.
                    else
                      if(kpdf.eq.6) then
                        call ConDensity(dd,ia,1,DenSupl,ich)
                        if(DensityType.ne.5.and.DensityType.ne.6) then
                          pom=DenSupl(1)*fpdfi
                          WorkMap(j,1)=WorkMap(j,1)+pom
                        else

                        endif
                      else
                        WorkMap(j,1)=WorkMap(j,1)+
     1                             pdfat(xx,t,ia,1,ich)*fpdfi
                      endif
                    endif
                    if(.not.ErrMapActive) then
                      call FeFlowChartEvent(n,is)
                      if(is.ne.0) then
                        call FeBudeBreak
                        if(ErrFlag.ne.0) go to 9000
                      endif
                    endif
                  else
                    if(uzje(j)) cycle
                    do i=1,3
                      xx(i)=xob(i)+xr(i)*float(ix-1)+yr(i)*float(iy-1)
     1                            +zr(i)*float(iz-1)
                    enddo
                    call exmap(InputMap,xx,CalcMap(j),uzje(j),x4p(1))
                    if(uzje(j)) then
                      SumOverMap=SumOverMap+CalcMap(j)
                      call FeFlowChartEvent(n,is)
                      if(is.ne.0) then
                        call FeBudeBreak
                        if(ErrFlag.ne.0) go to 9000
                      endif
                    endif
                  endif
                enddo
                if(ErrMapActive.and..not.konec)
     1            call FeEventInterrupt(Konec)
              enddo
            enddo
            if(mcmax.gt.0.and.imc.ne.0) then
              pom1=1./float(imc)
              pom2=pom1*float(imc-1)
              error1=0.
              error2=0.
              do j=1,nxrnyr
                pom=(WorkMap(j,1)-CalcMap(j))**2*pom1
                error1=error1+abs(ErrMap(j)*pom1-pom)
                error2=error2+ErrMap(j)
                ErrMap(j)=pom2*ErrMap(j)+pom
              enddo
              if(error2.ne.0.) then
                error=min(sqrt(error1/error2)*100.,100.)
              else
                if(imc.gt.1) then
                  error=0.
                else
                  error=100.
                endif
              endif
              if(error.lt.errlev.or.konec) go to 5100
              if(NDimI(KPhase).gt.0) then
                Cislo='t'
                pom=xx(4)
              else
                Cislo='z'
                pom=zout
              endif
              write(t80,101) Cislo(1:1),Tabulator,pom,Tabulator,
     1                       Tabulator,imc,Tabulator,Tabulator,error
              call FeOutputInterrupt(t80)
            endif
            imc=imc+1
            go to 1500
5100        if(mcmax.gt.0) then
              do ii=1,nxrnyr
                ErrMap(ii)=sqrt(ErrMap(ii))
              enddo
            endif
            npoints=0
            do ii=1,nxrnyr
              if(.not.uzje(ii)) npoints=npoints+1
            enddo
            if(npoints.eq.0) go to 6000
          enddo
          if(npoints.ne.0) then
            call FeFlowChartRemove
            call FeChybne(-1.,-1.,'Fourier maps don''t allow to'//
     1                    ' extrapolate all points.',' ',SeriousError)
            go to 9000
          endif
6000      do ii=1,nxrnyr
            if(DensityType.eq.5.or.DensityType.eq.6) then
              CalcMap(ii)=sqrt(WorkMap(ii,1)**2+WorkMap(ii,2)**2+
     1                       WorkMap(ii,3)**2)
              if(DensityType.eq.5) CalcMap(ii)=1000./(1.+CalcMap(ii))
            else

            endif
            Dmin=min(Dmin,CalcMap(ii))
            Dmax=max(Dmax,CalcMap(ii))
            if(ErrMapActive) DmaxErr=max(ErrMap(ii),DmaxErr)
          enddo
6100      write(82,rec=nmap+1)(CalcMap(i),i=1,nxrnyr)
          if(ErrMapActive) then
            write(83,rec=nmap+1)(ErrMap(i),i=1,nxrnyr)
            call obnova
          endif
        enddo
      enddo
      write(82,rec=nmap+2) Dmax,Dmin
      if(ErrMapActive) then
        write(83,rec=nmap+2) DmaxErr,DminErr
        call FeCloseInterrupt
      else
        call FeFlowChartRemove
      endif
      call SetContours(0)
      if(DrawPDF) then
        if(DensityType.eq.2.or.DensityType.eq.3.or.DensityType.eq.4.or.
     1     DensityType.eq.5) then
          DiffMapa=.true.
          DrawPos=1
          DrawNeg=1
        else
          DiffMapa=.false.
          DrawPos=1
          DrawNeg=0
        endif
      endif
      go to 9050
8000  call FeReadError(81)
9000  ErrFlag=1
9050  if(ErrFlag.eq.0) then
        m8=82
        read(m8,rec=1,err=8000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                          (i,j=1,6),mapa,nsubs,SatelityByly,
     2                          nonModulated(KPhase)
      else
        call CloseIfOpened(82)
        m8=81
      endif
9999  if(allocated(uzje))  deallocate(uzje,CalcMap)
      if(allocated(InputMap)) deallocate(InputMap)
      if(allocated(WorkMap))  deallocate(WorkMap)
      if(allocated(ErrMap))   deallocate(ErrMap)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
101   format(a1,' =',a1,f6.3,a1,'Monte Carlo hit#',a1,i5,a1,'Error =',a,
     1       f5.1,'%')
      end
