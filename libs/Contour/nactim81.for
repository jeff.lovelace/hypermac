      subroutine NactiM81
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 OldFileName,M81Name
      logical M81Opened,EqIgCase
      save OldFileName
      if(kpdf.eq.4.or.kpdf.eq.5) then
        M81Name=fln(:ifln)//'.d81'
      else
        M81Name=fln(:ifln)//'.m81'
      endif
      inquire(81,opened=M81Opened,name=OldFileName)
      id=idel(CurrentDir)
      i=index(OldFileName,CurrentDir(:id))
      if(i.gt.0) OldFileName=OldFileName(id+1:)
      if(M81Opened) then
        if(.not.EqIgCase(OldFileName,M81Name)) then
          call Del8
          M81Opened=.false.
          M81Cteno=.false.
        endif
      endif
      call CloseIfOpened(81)
      call OpenMaps(81,M81Name,nxny,0)
      if(ErrFlag.ne.0) go to 9999
      read(81,rec=1,err=9000) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                        iorien,mapa,nsubs,SatelityByly,
     2                        nonModulated(KPhase)
      nsubs=mod(nsubs,10)
      xymap =iorien(1).le.3.and.iorien(2).le.3
      xyzmap=xymap.and.iorien(3).le.3
      do i=1,NDim(KPhase)
        if(NDim(KPhase).eq.3) then
          cx(i)=smbx(iorien(i))
        else
          cx(i)=smbx6(iorien(i))
        endif
      enddo
      if(allocated(FlagMap)) deallocate(FlagMap)
      allocate(FlagMap(nmap))
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(nxny))
      NActualMap=nxny
      call SetIntArrayTo(FlagMap,nmap,0)
      read(81,rec=nmap+2) Dmax,Dmin
      M81Cteno=.true.
      m8=81
      DiffMapa=Mapa.eq.3.or.Mapa.eq.6.or.Mapa.gt.50.or.
     1   (ChargeDensities.and.mapa.ge.7.and.mapa.le.9).or.
     2   Mapa.eq.-3.or.Mapa.eq.10
      DrawPos=1
      if(DiffMapa) then
        DrawNeg=1
      else
        DrawNeg=0
      endif
      if(DiffMapa) then
        DensityType=2
      else
        DensityType=0
      endif
      call SetContours(0)
      call ConSetCellParCon
      go to 9999
9000  call FeReadError(81)
      ErrFlag=1
9999  return
      end
