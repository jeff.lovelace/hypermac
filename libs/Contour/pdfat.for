      function pdfat(xx,t,iat,klic,ich)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(3),bp(6),cp(10),dp(15),ep(21),fp(28),p(9),pinv(3,3),
     1          pa(9),rot(3,3),rotm(28,28),b(6),c(10),d(15),e(21),f(28),
     2          ccp(74),cc(74),xp(3),xf(3),occ(1),Gamma(9),x4(3),
     3          tzero(3),dt(3),nt(3)
      equivalence (cc (1),c ),(cc (11),d ),(cc (26),e ),(cc (47),f ),
     1            (ccp(1),cp),(ccp(11),dp),(ccp(26),ep),(ccp(47),fp),
     2            (xp1,xp(1)),(xp2,xp(2)),(xp3,xp(3))
      data tpisq/19.7392088/,tzero,dt/6*0./,nt/1,0,0/
      save x1a,x2a,x3a,coef,cum3,cum4,cum5,cum6,pomdef,ccp,pa,itfi
      if(klic.eq.0) then
        if(NDim(KPhase).gt.3) call UnitMat(Gamma,NDimI(KPhase))
        ip=ipor(iat)
        ia=iapdf(ip)
        itfi=itf(ia)
        isw=iswa(ia)
        if(itfi.eq.1) then
          pom=beta(1,ia)
          do j=1,6
            beta(j,ia)=pom*prcp(j,isw,KPhase)
          enddo
          itf(ia)=2
          itfi=2
        endif
        if(NDim(KPhase).gt.3) then
          x4(1)=qcnt(1,ia)+t
          if(KModA(1,ia).gt.0) then
            if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
              call MakeOccMod(occ(1),x4,tzero,nt,dt,a0(ia),ax(1,ia),
     1          ay(1,ia),KModA(1,ia),KFA(1,ia),Gamma)
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(occp,x4,tzero,nt,dt,
     1            ux(1,KModA(2,ia),ia),uy(2,KModA(2,ia),ia),KFA(2,ia),
     2            Gamma)
                if(occp.le.0.) occ(1)=0.
              endif
            else
              call MakeOccModPol(occ(1),x4,tzero,nt,dt,
     1                           ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                           ax(KModA(1,ia),ia),a0(ia)*.5,
     3                           Gamma,TypeModFun(ia))
            endif
            if(occ(1).le..001) then
              ich=2
              go to 9999
            endif
          endif
        endif
        do n=2,itfi
          nrank=TRank(n)
          if(n.eq.2) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeBetaMod(bp,x4,tzero,nt,dt,bx(1,1,ia),
     1                           by(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                           Gamma)
              else
                call MakeBetaModPol(bp,x4,tzero,nt,dt,bx(1,1,ia),
     1                              by(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                              a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(bp,beta(1,ia),bp,nrank)
            else
              call CopyVek(beta(1,ia),bp,nrank)
            endif
          else if(n.eq.3) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c3x(1,1,ia),
     1                          c3y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c3x(1,1,ia),
     1                             c3y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c3(1,ia),cp,nrank)
            else
              call CopyVek(c3(1,ia),cp,nrank)
            endif
          else if(n.eq.4) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c4x(1,1,ia),
     1                          c4y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c4x(1,1,ia),
     1                             c4y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c4(1,ia),cp,nrank)
            else
              call CopyVek(c4(1,ia),dp,nrank)
            endif
          else if(n.eq.5) then
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c5x(1,1,ia),
     1                          c5y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c5x(1,1,ia),
     1                             c5y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c5(1,ia),cp,nrank)
            else
              call CopyVek(c5(1,ia),ep,nrank)
            endif
          else
            if(NDimI(KPhase).gt.0.and.KModA(n+1,ia).gt.0) then
              if(TypeModFun(ia).le.1) then
                call MakeAnhMod(cp,nrank,x4,tzero,nt,dt,c6x(1,1,ia),
     1                          c6y(1,1,ia),KModA(n+1,ia),KFA(n+1,ia),
     2                          Gamma)
              else
                call MakeAnhModPol(cp,nrank,x4,tzero,nt,dt,c6x(1,1,ia),
     1                             c6y(1,1,ia),KModA(n+1,ia),ax(1,ia),
     2                             a0(ia)*.5,Gamma,TypeModFun(ia))
              endif
              call AddVek(cp,c6(1,ia),cp,nrank)
            else
              call CopyVek(c6(1,ia),fp,nrank)
            endif
          endif
        enddo
        if(ispdf(ip).eq.0) ispdf(ip)=1
        k=ispdf(ip)
        l=abs(k)
        do i=1,3
          do j=1,3
            rot(i,j)=rm(i+(j-1)*3,l,isw,KPhase)
          enddo
        enddo
        if(k.lt.0) then
          do i=1,3
            do j=1,3
              rot(i,j)=-rot(i,j)
            enddo
          enddo
        endif
        i=1
        do n=2,itfi
          nrank=TRank(n)
          if(n.eq.2) then
            call srotb(rot,rot,rotm)
            call multm(rotm,bp,b,nrank,nrank,1)
          else
            call srotc(rot,n,rotm)
            call multm(rotm,ccp(i),cc(i),nrank,nrank,1)
            i=i+nrank
          endif
        enddo
        x1a=xpdf(1,ip)
        x2a=xpdf(2,ip)
        x3a=xpdf(3,ip)
        cum3=0.
        cum4=0.
        cum5=0.
        cum6=0.
        do i=1,6
          call indext(i,j,k)
          pinv(j,k)=b(i)/tpisq
        enddo
        pinv(2,1)=pinv(1,2)
        pinv(3,1)=pinv(1,3)
        pinv(3,2)=pinv(2,3)
        call matinv(pinv,p,pvol,3)
        call qln(p,rot,pdiag,3)
        call TrMat(rot,pa,3,3)
        j=1
        do n=3,itfi
          nrank=TRank(n)
          call srotc(pa,n,rotm)
          call multm(rotm,cc(j),ccp(j),nrank,nrank,1)
          j=j+nrank
        enddo
        do i=1,TRankCumul(itfi)-10
          ccp(i)=ccp(i)*cmlt(i)
        enddo
        if(BratAnharmPDF(1,ip)) then
          pomdef=1.
        else
          pomdef=0.
        endif
        pdfat=0.
        if(pvol.gt.0.) then
          ich=0
          coef=1./sqrt(pvol*pi2**3)
          if(OldNorm) go to 9999
          coef=coef/CellVol(isw,KPhase)
        else
          ich=1
        endif
      else
        xf(1)=xx(1)-x1a
        xf(2)=xx(2)-x2a
        xf(3)=xx(3)-x3a
        call MultM(TrToOrtho,xf,xo,3,3,1)
        if(VecOrtLen(xo,3).gt.CutOffDist) then
          pdfat=0.
          go to 9999
        endif
        call multm(pa,xf,xp,3,3,1)
        w1=pdiag(1)*xp1
        w2=pdiag(2)*xp2
        w3=pdiag(3)*xp3
        pdfat=coef*ExpJana(-.5*(xp1*w1+xp2*w2+xp3*w3),ich)
        if(ich.ne.0) go to 9999
        call priprav(itfi)
        if(BratAnharmPDF(2,ip)) cum3=rank3(cp)
        if(BratAnharmPDF(3,ip)) cum4=rank4(dp)
        if(BratAnharmPDF(4,ip)) cum5=rank5(ep)
        if(BratAnharmPDF(5,ip)) cum6=rank6(fp)
        pdfat=pdfat*(pomdef+cum3+cum4+cum5+cum6)
      endif
9999  return
      end
