      subroutine ConShowMaxMin(FileNameTmp)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileNameTmp(2)
      character*80 Veta
      character*6 :: MinMaxLabel(2)=(/'Maxima','Minima'/)
      id=NextQuestId()
      xqd=450.
      il=16
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
      il=1
      tpom=xqd*.5
      call FeQuestLblMake(id,tpom,il,MinMaxLabel(1),'C','B')
      nLblMinMax=LblLastMade
      il=il+1
      tpom=70.
      Veta='Local'
      do i=1,3
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        if(i.eq.1) then
          Veta='Fractional'
          tpom=tpom+140.
        else if(i.eq.2) then
          Veta='Charge'
          tpom=tpom+125.
        endif
      enddo
      xpom=5.
      il=14
      call FeQuestSbwMake(id,xpom,il,xqd-2.*xpom-SbwPruhXd,12,1,
     1                    CutTextFromRight,SbwVertical)
      nSbwText=SbwLastMade
      il=15
      xpom=7.
      tpom=xpom+CrwgXd+10.
      Veta='Show ma%xima'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      il=il+1
      call FeQuestCrwOpen(CrwLastMade,.true.)
      Veta='Show mi%nima'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      call FeQuestCrwOpen(CrwLastMade,.false.)
      KteryFile=1
2200  Veta=fln(:ifln)//FileNameTmp(KteryFile)
      call FeQuestSbwTextOpen(nSbwText,20,Veta)
      call FeQuestLblChange(nLblMinMax,MinMaxLabel(KteryFile))
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        call CloseIfOpened(SbwLn(nSbwText))
        KteryFile=3-KteryFile
        go to 2200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      call FeQuestRemove(id)
      return
      end
