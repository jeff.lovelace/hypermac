      subroutine CPSearchBond
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'contour.cmn'
      dimension xat(3,2),xp(3),ddxp(3),gddxp(3)
      character*80  t80,At2
      character*8   At1
      character*2   Label
      logical       iesd
      call DefaultDist
      dmdk=CPDmin
      dmhk=CPDmax
      iuhl=0
      fullcoo=0
      VolaToDist=.false.
      call inm40(iesd)
      PrvniSymmString=' '
      do j=1,NAtCalc
        BratPrvni(j)=AtBrat(j)
        BratDruhyATreti(j)=AtBrat(j)
      enddo
      call DistAt
      call OpenFile(lst,fln(:ifln)//'.cp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call CPMakeOutputStart
      ncp=0
      rewind 78
1300  read(78,end=9000) Label,At1,i,i,xp,xp,xp(1),t80
      if(Label.ne.'#1') go to 1300
1310  At2=' '
      read(78,end=9000) Label,At2(1:8),i,i,xp,xp,xp(1),t80
      if(Label.eq.'#2') then
        i=index(t80,'#')
        id=idel(t80)
        if(i.gt.0.and.i.ne.id) At2=At2(:idel(At2))//t80(i:idel(t80))
      else if(Label.eq.'#1') then
        At1=At2
        go to 1310
      else
        go to 1300
      endif
      call AtSymi(At1,ia,xat(1,1),xp,xp,i,ich,*1310)
      call AtSymi(At2,ia,xat(1,2),xp,xp,i,ich,*1310)
      do i=1,3
        xmina(i,1)=min(xat(i,1),xat(i,2))
        xmaxa(i,1)=max(xat(i,1),xat(i,2))
      enddo
      do i=1,3
        xp(i)=(xmaxa(i,1)+xmina(i,1))*.5
      enddo
      dmez=0.
      do i1=-1,1,2
        ddxp(1)=(xmaxa(1,1)-xp(1))*float(i1)
        do i2=-1,1,2
          ddxp(2)=(xmaxa(2,1)-xp(2))*float(i2)
          do i3=-1,1,2
            ddxp(3)=(xmaxa(3,1)-xp(3))*float(i3)
            call multm(MetTens(1,1,KPhase),ddxp,gddxp,3,3,1)
            dd=sqrt(scalmul(ddxp,gddxp))
            dmez=max(dd,dmez)
          enddo
        enddo
      enddo
      dmez=dmez+CutOffDist
      call OkoliC(xp,dmez,1,1,.true.)
      xp(1)=0.
      do j=1,npdf
        call specpos(xpdf(1,j),xp,0,1,.005,l)
        fpdf(j)=ai(iapdf(j))*float(l)
      enddo
      if(npdf.gt.0) call CPSearchAlongLine(xat(1,1),xat(1,2),At1,At2,1,
     1                                     1)
      go to 1310
9000  call CPMakeOutputFinish(1)
      close(78,status='delete')
9999  return
      end
