      subroutine ConDensity(dd,iat,Klic,DenSupl,ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      logical Hydrogen
      dimension FractToLocal(9),px(9),xp(3),SpX(64),SpXDer1(3,64),
     1          SpXDer2(6,64),fnorm(8),zz(8),nn(8),STOA(10),DSto(0:2),
     2          DenDer(0:2),DenDerXYZ(10),DenSupl(*),OrthoToLocal(9),
     3          RPop(680),popasi(64)
      equivalence (xp,px,pom)
      data st/.0333333/
      save FractToLocal,ip,isfi,PopValDeform,ia,fnorm,OrthoToLocal,zz,
     1     nn,lasmaxi,kapa1i,kapa2i,Hydrogen,popasi,PopCor
      ich=0
      if(Klic.eq.0) then
        ip=ipor(iat)
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isw=iswa(ia)
        isfi=isf(ia)
        if(ispdf(ip).eq.0) ispdf(ip)=1
        ISym=ispdf(ip)
        ISymAbs=abs(ISym)
        if(IsymAbs.eq.1) then
          call CopyMat(TrAt(1,ia),FractToLocal,3)
        else
          call MatInv(rm(1,ISymAbs,isw,KPhase),px,STOA(1),3)
          call MultM(TrAt(1,ia),px,FractToLocal,3,3,3)
        endif
        if(ISym.lt.0) then
          do i=1,9
            FractToLocal(i)=-FractToLocal(i)
          enddo
        endif
        call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
        call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
        jp=2
        kp=2
        if(lasmaxi.ge.1) then
          popasi(1)=popas(1,ia)
          do l=1,lasmaxi-2
            n=2*l+1
            call multm(RPop(kp),popas(jp,ia),popasi(jp),n,n,1)
            kp=kp+n**2
            jp=jp+n
          enddo
          kapa1i=kapa1(ia)
          kapa2i=kapa2(ia)
          PopValDeform=kapa1i**3*popv(ia)
        endif
        PopValFree=ffbasic(1,isfi,KPhase)-ffcore(1,isfi,KPhase)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        Hydrogen=AtType(isfi,KPhase).eq.'H'.or.
     1           AtType(isfi,KPhase).eq.'D'
      else
        if(DensityType.le.2) then
          if(lasmaxi.ge.1) then
            Density=PopValDeform*Finter(dd*kapa1i,DenVal(1,isfi),st,240)
     1              /pi4
          else
            Density=0.
          endif
          if(DensityType.eq.2.and.lasmaxi.gt.1) then
            if(Hydrogen) then
              Density=Density-PopValFree*2.148028*exp(-3.77943*dd)
            else
              Density=Density-
     1                   PopValFree*Finter(dd,DenVal(1,isfi),st,240)/pi4
            endif
          endif
          if(DensityType.eq.0)
     1      Density=Density+PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          k=0
          do l=1,lasmaxi-1
            pom=0.
            do n=1,2*l-1
              k=k+1
              pom=pom+popasi(k)*SpX(k)
            enddo
            Density=Density+
     1                 pom*fnorm(l)*SlaterDensity(dd,zz(l),nn(l))
          enddo
        else if(DensityType.eq.3.or.DensityType.eq.4.or.
     1          DensityType.eq.5.or.DensityType.eq.6) then
          if(DensityType.eq.3.or.DensityType.eq.5.or.DensityType.eq.6)
     1      then
            DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
            DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
            DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
            call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
            if(DensityType.eq.3) then
              Density=-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
            else
              call CopyVek(DenDerXYZ(2),DenSupl,3)
            endif
          else
            Density=0.
          endif
          DenDer(0)=PopValDeform*
     1      Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
          DenDer(1)=PopValDeform*kapa1i*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          DenDer(2)=PopValDeform*kapa1i**2*
     1      Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
          if(DensityType.eq.3.or.DensityType.eq.4) then
            Density=Density-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
          else
            call AddVek(DenSupl,DenDerXYZ(2),DenSupl,3)
          endif
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          k=0
          do l=1,lasmaxi-1
            call SlaterDensityDer(dd,zz(l),nn(l),DSto)
            STO=DSto(0)
            call RadialDer(xo(1),xo(2),xo(3),dd,DSto,2,STOA)
            call SetRealArrayTo(px,3,0.)
            fnorml=fnorm(l)
            do n=1,2*l-1
              k=k+1
              SpXk=SpX(k)
              popask=popasi(k)
              if(DensityType.eq.3.or.DensityType.eq.4) then
                pom=pom+popask*(
     1            SpXk*STOA(5)+2.*SpXDer1(1,k)*STOA(2)+SpXDer2(1,k)*STO+
     2            SpXk*STOA(6)+2.*SpXDer1(2,k)*STOA(3)+SpXDer2(2,k)*STO+
     3            SpXk*STOA(7)+2.*SpXDer1(3,k)*STOA(4)+SpXDer2(3,k)*STO)
              else
                do m=1,3
                  px(m)=px(m)+popask*(SpXk*STOA(m+1)+SpXDer1(m,k)*STO)
                enddo
              endif
            enddo
            if(DensityType.eq.3.or.DensityType.eq.4) then
              Density=Density-pom*fnorml
            else
              do m=1,3
                DenSupl(m)=DenSupl(m)+px(m)*fnorml
              enddo
            endif
          enddo
        endif
        if(DensityType.ne.5.and.DensityType.ne.6) DenSupl(1)=Density
      endif
9999  return
      end
