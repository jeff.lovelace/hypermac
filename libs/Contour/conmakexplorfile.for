      subroutine ConMakeXPlorFile(ln,NzCopy,Type)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension WorkMap(:),CellParP(6)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      allocatable WorkMap
      allocate(WorkMap(nxny))
      Klic=0
      go to 1000
      entry ConMakeXplorFileProlog(ln,NzCopy,Type)
      Klic=1
1000  write(ln,FormA)
      write(ln,'(i5)') 1
      write(ln,'(''Xplor'',a)') fln(:ifln)
      write(ln,'(9i8)')(nx(i),0,nx(i)-1,i=1,2),
     1                 NZCopy,0,NZCopy-1
      m=0
      do i=1,NDim(KPhase)
        if(nx(i).le.0) cycle
        m=m+1
        CellParP(m)=CellParCon(m)*(xmax(i)-xmin(i)+dx(i))
      enddo
      call ConCellParNorm(CellParP)
      write(Veta,102)(CellParP(i),i=1,3)
      write(Veta(idel(Veta)+1:),102)(CellParCon(i),i=4,6)
      write(ln,FormA) Veta(:idel(Veta))
      Veta='zyx'
      call Velka(Veta)
      write(ln,FormA) Veta(:idel(Veta))
      if(Klic.eq.1) go to 9999
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      Ave=0.
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j),j=1,nxny)
        write(ln,105) i-1
        write(ln,107)(WorkMap(j),j=1,nxny)
        do j=1,nxny
          Ave=Ave+WorkMap(j)
        enddo
      enddo
      write(ln,105) -9999
      write(ln,'(2e15.6)') ave/float(nxny*NzCopy)
      go to 9999
5000  call FeReadError(m8)
9999  if(allocated(WorkMap)) deallocate(WorkMap)
      return
102   format(3e12.4)
105   format(3i15)
107   format(6e12.5)
      end
