      subroutine ChdSearchSource(xyza,ldone,npoints,istep,iterm,
     1                           NZeroNeigh)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical Konec,ldone(*)
      dimension xyza(*),iterm(*),NZeroNeigh(*),xyz(3*mpts),xyzg(3*mpts),
     1          dx0(3*mpts),RhoArr(4*mpts),dxi(3*mpts,6),xyzt(3),dxt(3)
      dimension abx(9,9),amx(9,10)
      data amx/  0.500000,  0.416667,  0.375000,  0.348611,  0.329861,
     1           0.315592,  0.304225,  0.294868,  0.286975,  0.500000,
     2           0.666667,  0.791667,  0.897222,  0.990972,  1.076587,
     3           1.156159,  1.231011,  1.302044,  0.000000, -0.083333,
     4          -0.208333, -0.366667, -0.554167, -0.768204, -1.006920,
     5          -1.268903, -1.553035,  0.000000,  0.000000,  0.041667,
     6           0.147222,  0.334722,  0.620106,  1.017965,  1.541931,
     7           2.204905,  0.000000,  0.000000,  0.000000, -0.026389,
     8          -0.120139, -0.334177, -0.732035, -1.386993, -2.381455,
     9           0.000000,  0.000000,  0.000000,  0.000000,  0.018750,
     a           0.104365,  0.343080,  0.867046,  1.861508,  0.000000,
     1           0.000000,  0.000000,  0.000000,  0.000000, -0.014269,
     2          -0.093841, -0.355824, -1.018799,  0.000000,  0.000000,
     3           0.000000,  0.000000,  0.000000,  0.000000,  0.011367,
     4           0.086220,  0.370352,  0.000000,  0.000000,  0.000000,
     5           0.000000,  0.000000,  0.000000,  0.000000, -0.009357,
     6          -0.080390,  0.000000,  0.000000,  0.000000,  0.000000,
     7           0.000000,  0.000000,  0.000000,  0.000000,  0.007893/
      data abx/  1.000000,  1.500000,  1.916667,  2.291667,  2.640278,
     1           2.970139,  3.285731,  3.589955,  3.884823,  0.000000,
     2          -0.500000, -1.333333, -2.458333, -3.852778, -5.502083,
     3          -7.395635, -9.525207,-11.884151,  0.000000,  0.000000,
     4           0.416667,  1.541667,  3.633333,  6.931944, 11.665823,
     5          18.054539, 26.310843,  0.000000,  0.000000,  0.000000,
     6          -0.375000, -1.769444, -5.068056,-11.379894,-22.027753,
     7         -38.540361,  0.000000,  0.000000,  0.000000,  0.000000,
     8           0.348611,  1.997917,  6.731796, 17.379654, 38.020414,
     9           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     a          -0.329861, -2.223413, -8.612128,-25.124736,  0.000000,
     1           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     2           0.315592,  2.445164, 10.701468,  0.000000,  0.000000,
     3           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     4          -0.304225, -2.663169,  0.000000,  0.000000,  0.000000,
     5           0.000000,  0.000000,  0.000000,  0.000000,  0.000000,
     6           0.294868/
      stpr=StepTrajInt/float(istep)
      stpi=StepTrajInt
      call SetIntArrayTo(iterm,npoints,0)
      call SetIntArrayTo(NZeroNeigh,npoints,0)
      jl=1
      jj=0
      nall = 0
      nall3=-2
1000  npth = 0
      npth3=-2
1010  nall =nall +1
      nall3=nall3+3
      if(.not.ldone(nall)) then
        npth=npth+1
        npth3=npth3+3
        call CopyVek(xyza(nall3),xyz(npth3),3)
      endif
      if(npth.lt.mpts.and.nall.lt.npoints) go to 1010
      if(npth.le.0) go to 9999
      jj=jj+1
      nterm=0
      do ik=6,1,-1
        do il=1,istep
          call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                                   NZeroNeigh(jl),npth,1)
          imp3=0
          imp4=1
          do i=1,npth
            if(iterm(jl+i-1).eq.0) then
              pom=VecOrtLen(RhoArr(imp4+1),3)
              if(abs(pom).gt.0.) then
                bilbo=stpr/pom
              else
                bilbo=0.
              endif
              do j=1,3
                xyz(imp3+j)=xyz(imp3+j)+bilbo*RhoArr(imp4+j)
              enddo
            endif
            imp3=imp3+3
            imp4=imp4+4
          enddo
          call FeEventInterrupt(Konec)
          if(Konec) go to 9000
        enddo
        call ChdCheckIfCaptured(xyz,iterm(jl),NZeroNeigh(jl),npth,nterm,
     1                          0)
        if(ErrFlag.ne.0) go to 9999
        if(nterm.eq.npth) go to 4900
        call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                                 NZeroNeigh(jl),npth,1)
        imp3=1
        imp4=2
        do i=1,npth
          if(iterm(jl+i-1).eq.0) then
            call CopyVek(RhoArr(imp4),dxi(imp3,ik),3)
            call VecOrtNorm(dxi(imp3,ik),3)
          endif
          imp3=imp3+3
          imp4=imp4+4
        enddo
      enddo
      call SetRealArrayTo(dx0,3*npth,0.)
      icount=0
3000  icount=icount+1
      call FeEventInterrupt(Konec)
      if(Konec) go to 9000
      call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                               NZeroNeigh(jl),npth,1)
      imp3=1
      imp4=2
      do j=1,npth
        if(iterm(jl+j-1).eq.0) then
          call CopyVek(RhoArr(imp4),dxi(imp3,1),3)
          call VecOrtNorm(dxi(imp3,1),3)
        endif
        imp3=imp3+3
        imp4=imp4+4
      enddo
      imp3=1
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          if(VecOrtScal(dxi(imp3,1),dx0(imp3),3).ge.0.) go to 3450
          call CopyVek(xyz(imp3),xyzt,3)
          do ll=1,istep
            call ChdGetRhoArray(RhoArr,xyzt,1,1)
            call CopyVek(RhoArr(2),dxt,3)
            call VecOrtNorm(dxt,3)
            do j=1,3
              xyzt(j)=xyzt(j)+stpr*dxt(j)
            enddo
          enddo
          call ChdGetRhoArray(RhoArr,xyzt,1,1)
          call CopyVek(xyzt,xyz(imp3+1),3)
          call CopyVek(RhoArr(2),dxi(imp3,1),3)
          call VecOrtNorm(dxi(imp3,1),3)
        endif
3450    imp3=imp3+3
      enddo
      call ChdCheckIfCaptured(xyz,iterm(jl),NZeroNeigh(jl),npth,nterm,
     1                        icount)
      if(ErrFlag.ne.0) go to 9999
      if(nterm.eq.npth) go to 4900
      imp3=0
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          j=imp3+1
          call CopyVek(xyz(j),xyzg(j),3)
          call CopyVek(dxi(j,1),dx0(j),3)
          do j=1,6
            pom=abx(6,j)*stpi
            do k=1,3
              xyz(imp3+k)=xyz(imp3+k)+dxi(imp3+k,j)*pom
            enddo
          enddo
        endif
        imp3=imp3+3
      enddo
      call ChdGetRhoArrayForSelected(RhoArr,xyz,iterm(jl),
     1                               NZeroNeigh(jl),npth,1)
      imp3=0
      imp4=1
      do i=1,npth
        if(iterm(jl+i-1).eq.0) then
          pom=VecOrtLen(RhoArr(imp4+1),3)
          if(pom.gt.0.) then
            sqt=amx(6,1)*stpi/pom
          else
            sqt=0.
          endif
          do k=1,3
            xyz(imp3+k)=xyzg(imp3+k)+sqt*RhoArr(imp4+k)
          enddo
          do j=6,1,-1
            pom=amx(6,j+1)*stpi
            do k=1,3
              l=imp3+k
              xyz(l)=xyz(l)+dxi(l,j)*pom
              if(j.le.5) dxi(l,j+1)=dxi(l,j)
            enddo
          enddo
        endif
        imp3=imp3+3
        imp4=imp4+4
      enddo
      go to 3000
4900  jl=jl+npth
      if(nall.lt.npoints) go to 1000
      go to 9999
9000  ErrFlag=1
9999  return
      end
