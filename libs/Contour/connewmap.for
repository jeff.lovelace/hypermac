      subroutine ConNewMap(ich)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension NMenu(5)
      character*256 FileName
      character*80  Veta,LabelM81,LabelD81
      character*80 :: Menu(5) =
     1   (/'Fourier map                  ',
     2     'p.d.f.                       ',
     3     'j.p.d.f.                     ',
     4     'Density map - cell fraction  ',
     5     'Density map - general section'/)
      integer Prvni,RolMenuSelectedQuest,CrwStateQuest
      logical ExistFile,ExistM80,ExistM81,CrwLogicQuest,UseOldMap,
     1        GeneralSection,ExistD81,EqIgCase,M81Opened,lpom
      data GeneralSection/.false./,KPDFOld/-1/
      save UseOldMap
      ich=0
      KPDFIn=KPDF
      if(KPDF.eq.0) then
        KPDF=1
      else if(KPDF.eq.5) then
        KPDF=4
      else if(KPDF.eq.6) then
        KPDF=5
      endif
      ExistM80=ExistFile(fln(:ifln)//'.m80')
      ExistM81=ExistFile(fln(:ifln)//'.m81')
      ExistD81=ExistFile(fln(:ifln)//'.d81')
      MapaM81=0
      MapaD81=0
      inquire(81,opened=M81Opened,name=FileName)
      if(M81Opened) then
        id=idel(CurrentDir)
        i=index(FileName,CurrentDir(:id))
        if(i.gt.0) FileName=FileName(id+1:)
        if(EqIgCase(FileName,fln(:ifln)//'.m81')) then
          MapaM81=Mapa
        else if(EqIgCase(FileName,fln(:ifln)//'.d81')) then
          MapaD81=-Mapa
        endif
      endif
      ln=NextLogicNumber()
      if(ExistM81.and.MapaM81.le.0) then
        call OpenMaps(ln,fln(:ifln)//'.m81',i,0)
        read(ln,rec=1) (i,j=1,8),(pom,pom,pom,j=1,6),
     1                 (i,j=1,6),MapaM81,i,lpom,lpom
      endif
      call CloseIfOpened(ln)
      if(ExistD81.and.MapaD81.le.0) then
        call OpenMaps(ln,fln(:ifln)//'.d81',i,0)
        read(ln,rec=1) (i,j=1,8),(pom,pom,pom,j=1,6),
     1                 (i,j=1,6),MapaD81,i,lpom,lpom
        MapaD81=-MapaD81
      endif
      call CloseIfOpened(ln)
      if(MapaM81.gt.0) then
        LabelM81=MapType(MapaM81)
      else
        LabelM81=' '
      endif
      if(MapaD81.gt.0) then
        LabelD81=MenDensity(MapaD81)
      else
        LabelD81=' '
      endif
      if(NoOfDerivedMaps.eq.0) then
        DrawAtN=0
        call SetIntArrayTo(NMenu,5,1)
        dpom=0.
        Prvni=0
        do i=1,5
          if((i.eq.1.and..not.ExistM80.and..not.ExistM81).or.
     1       (i.eq.4.and..not.ChargeDensities)) then
            NMenu(i)=0
          else
            if(Prvni.eq.0) Prvni=i
          endif
          dpom=max(dpom,FeTxLength(Menu(i)))
        enddo
        dpom=dpom+10.+EdwYd
      else
        go to 3000
      endif
      if(KPDFOld.le.0) then
        KPDFOld=Prvni
        KPDF=Prvni
      else
        if(NMenu(KPDFOld).le.0) then
          KPDFOld=Prvni
          KPDF=Prvni
        else
          KDPF=KPDFOld
        endif
      endif
1100  ich=0
      id=NextQuestId()
      xqd=450.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      il=1
      Veta='%New map to draw'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuNew=RolMenuLastMade
      call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,Menu,NMenu,5,
     1                                  max(KPDFOld,1))
      xpom=5.
      tpom=xpom+CrwgXd+5.
      Veta='Use %old maps'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwOldMap=CrwLastMade
          Veta='Calculate ne%w ones'
        else
          nCrwNewMap=CrwLastMade
        endif
      enddo
      il=il-2
      xpom=xpom+xqd*.5+20.
      tpom=tpom+xqd*.5+20.
      Veta='Draw maps as %calculated'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      2)
        if(i.eq.1) then
          nCrwAsCalculated=CrwLastMade
          Veta='Draw a %general section'
        else
          nCrwGeneralSection=CrwLastMade
        endif
      enddo
      il=il+1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,' ','L','N')
      nLblOldMap=LblLastMade
1400  if(kpdf.le.1.or.kpdf.eq.4) then
        if(kpdf.le.1) then
          UseOldMap=ExistM81
        else
          UseOldMap=ExistD81
        endif
        if(CrwStateQuest(nCrwNewMap).eq.CrwClosed.or.
     1     CrwStateQuest(nCrwNewMap).eq.CrwDisabled.or.
     2     kpdf.ne.kpdfOld) then
          call FeQuestCrwOpen(nCrwOldMap,UseOldMap)
          call FeQuestCrwOpen(nCrwNewMap,.not.UseOldMap)
          if(kpdf.le.1) then
            if(.not.ExistM81) then
              call FeQuestCrwDisable(nCrwOldMap)
              call FeQuestCrwOn(nCrwNewMap)
            endif
          else
            if(.not.ExistD81) then
              call FeQuestCrwDisable(nCrwOldMap)
              call FeQuestCrwOn(nCrwNewMap)
            endif
          endif
        endif
        if(CrwStateQuest(nCrwAsCalculated).eq.CrwClosed.or.
     1     CrwStateQuest(nCrwAsCalculated).eq.CrwDisabled.or.
     2     kpdf.ne.kpdfOld) then
          call FeQuestCrwOpen(nCrwAsCalculated,.not.GeneralSection)
          call FeQuestCrwOpen(nCrwGeneralSection,GeneralSection)
        endif
        Veta=' '
        if(kpdf.le.1) then
          if(LabelM81.ne.' ') Veta='The old map is '//LabelM81
        else
          if(LabelD81.ne.' ') Veta='The old map is '//LabelD81
        endif
        call FeQuestLblChange(nLblOldMap,Veta)
      else
        if(nLblOldMap.gt.0) call FeQuestLblOff(nLblOldMap)
        call FeQuestCrwDisable(nCrwNewMap)
        call FeQuestCrwDisable(nCrwOldMap)
        call FeQuestCrwDisable(nCrwAsCalculated)
        call FeQuestCrwDisable(nCrwGeneralSection)
      endif
      kpdfOld=kpdf
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuNew) then
        kpdf=RolMenuSelectedQuest(CheckNumber)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        kpdf=RolMenuSelectedQuest(nRolMenuNew)
        KPDFOld=kpdf
        if(CrwStateQuest(nCrwAsCalculated).ne.CrwClosed.and.
     1     CrwStateQuest(nCrwAsCalculated).ne.CrwDisabled) then
          if(CrwLogicQuest(nCrwAsCalculated).and.kpdf.eq.1) kpdf=0
          if(.not.CrwLogicQuest(nCrwAsCalculated).and.kpdf.eq.4) kpdf=5
        else if(KPDF.eq.5) then
          KPDF=6
        endif
        GeneralSection=kpdf.eq.1.or.kpdf.eq.5
        if(kpdf.le.1) then
          UseOldMap=.true.
          if(ExistM81) then
            if(CrwLogicQuest(nCrwOldMap)) go to 2000
          endif
1600      UseOldMap=.false.
          ContourCallFourier=.true.
          call CloseIfOpened(81)
          call FouSetCommands(ich)
          if(ich.ne.0) then
            ich=0
            call DefaultContour
            call NactiContour
            ContourCallFourier=.false.
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            call FeQuestRemove(id)
            if(NDimI(KPhase).gt.0) call TrOrtho(0)
            go to 1100
          endif
          call Fourier
          if(ErrFlag.ne.0) go to 1600
          call DefaultContour
          call NactiContour
          ContourCallFourier=.false.
          if(NDimI(KPhase).gt.0) call TrOrtho(0)
        else if(kpdf.ge.4.and.kpdf.le.5) then
          UseOldMap=.true.
          if(ExistD81) then
            if(CrwLogicQuest(nCrwOldMap)) go to 2000
          endif
          UseOldMap=.false.
          call CloseIfOpened(81)
          Obecny=.false.
          call ConDensityInCell(ich)
        else if(kpdf.eq.5) then
          kpdf=6
        endif
      else
        KPDF=KPDFIn
      endif
2000  call FeQuestRemove(id)
3000  return
      end
