      subroutine obnova
      use Contour_mod
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      ic=0
      sp=0.
      do 5000i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        jj=PrvniKiAtomu(i)
        if(.not.SelPDF(i)) go to 5000
        do id=1,DelkaKiAtomu(i)
          if(kia(id,i).eq.1) then
            ic=ic+1
            call KdoCo(jj,at,pn,-1,rnds(ic),sp)
          endif
          jj=jj+1
        enddo
5000  continue
      do i=1,neq
        call ApeqC(i)
      enddo
      return
      end
