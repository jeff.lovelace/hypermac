      subroutine ConFillAtomsCheck
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      if(nEdwAtoms.eq.0) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
9999  return
      end
