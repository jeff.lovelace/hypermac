      subroutine ConMakeConnect
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension dpmin(:,:),dpmax(:,:),dpmino(:,:),dpmaxo(:,:),xp(2)
      character*80 t80
      character*8 At1,At2,At,AtArr(2)
      integer SbwLnQuest,SbwItemSelQuest
      logical EqIgCase
      allocatable dpmin,dpmax,dpmino,dpmaxo
      equivalence (AtArr(1),At1),(AtArr(2),At2)
      allocate(dpmin(NAtFormula(KPhase),NAtFormula(KPhase)),
     1         dpmax(NAtFormula(KPhase),NAtFormula(KPhase)),
     2         dpmino(NAtFormula(KPhase),NAtFormula(KPhase)),
     3         dpmaxo(NAtFormula(KPhase),NAtFormula(KPhase)))
      do j=1,NAtFormula(KPhase)
        do i=j,NAtFormula(KPhase)
          dpmin(i,j)=0.
          if(i.ne.j) dpmin(j,i)=0.
          dpmax(i,j)=(AtRadius(i,KPhase)+AtRadius(j,KPhase))*.5
          if(i.ne.j) dpmax(j,i)=dpmax(i,j)
        enddo
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_distlim.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do j=1,NAtFormula(KPhase)
        do i=j,NAtFormula(KPhase)
          dpmaxo(j,i)=dpmax(i,j)
          dpmino(j,i)=dpmin(i,j)
          write(t80,'(''distlim '',a2,1x,a2,2f10.3)')
     1      AtType(i,KPhase),AtType(j,KPhase),dpmin(i,j),dpmax(i,j)
          call ZdrcniCisla(t80,5)
          write(ln,FormA) t80(:idel(t80))
        enddo
      enddo
      call CloseIfOpened(ln)
      call AtomDistLimits
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_distlim.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1050  read(ln,FormA80,end=1100) t80
      k=0
      call kus(t80,k,Cislo)
      do l=1,2
        call kus(t80,k,Cislo)
        j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),Cislo,
     1                        .true.)
        if(j.lt.1.or.j.gt.NAtFormula(KPhase)) go to 1050
        if(l.eq.1) i=j
      enddo
      call StToReal(t80,k,xp,2,.false.,ich)
      if(ich.ne.0) go to 1050
      dpmin(i,j)=xp(1)
      dpmax(i,j)=xp(2)
      go to 1050
1100  close(ln,status='delete')
      call OpenFile(ln,fln(:ifln)//'_seldist.tmp','formatted','unknown')
      ni=0
      n=0
      do 4000i=1,npdf
        ik=ipor(i)
        if(fpdf(ik).le.0.) go to 4000
        ni=ni+1
        ia=iapdf(ik)
        isfi=isf(ia)
        nj=ni
        write(Cislo,FormI15) ni
        call zhusti(Cislo)
        At1=AtType(isfi,KPhase)(:idel(AtType(isfi,KPhase)))//
     1      Cislo(:idel(Cislo))
        do 3000j=i+1,npdf
          jk=ipor(j)
          if(fpdf(jk).le.0.) go to 3000
          nj=nj+1
          ja=iapdf(jk)
          isfj=isf(ja)
          dist=0.
          do k=1,3
            dist=dist+(ypdf(k,ik)-ypdf(k,jk))**2
          enddo
          dist=sqrt(dist)
          if(dist.le.dpmax(isfi,isfj).and.dist.gt.dpmin(isfi,isfj))
     1       then
            n=n+1
            write(Cislo,FormI15) nj
            call zhusti(Cislo)
            At2=AtType(isfj,KPhase)(:idel(AtType(isfj,KPhase)))//
     1          Cislo(:idel(Cislo))
            write(ln,100) AtArr,dist
          endif
3000    continue
4000  continue
      call CloseIfOpened(ln)
      id=NextQuestId()
      xqd=400.
      il=14
      t80='Select individual distances'
      call FeQuestCreate(id,-1.,-1.,xqd,il,t80,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.
      il=12
      ild=12
      call FeQuestSbwMake(id,xpom,il,dpom,ild,3,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      il=il+2
      dpom=60.
      xpom=xqd*.5-dpom-5.
      t80='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+5.
      t80='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      isel=1
4100  do i=1,n
        call FeQuestSetSbwItemSel(i,nSbw,isel)
      enddo
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_seldist.tmp')
4500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        if(CheckNumber.eq.nButtAll) then
          isel=1
        else
          isel=0
        endif
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 4100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 4500
      endif
      if(ich.eq.0) then
        call CloseIfOpened(SbwLnQuest(nSbw))
        call OpenFile(ln,fln(:ifln)//'_seldist.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 9999
        At=' '
        nd=0
        n=0
5000    n=n+1
        read(ln,100,end=6000,err=6000) AtArr,dist
        do k=1,2
          Cislo=AtArr(k)
          do j=1,idel(Cislo)
            if(index(Cifry(:10),Cislo(j:j)).gt.0) go to 5020
          enddo
5015      j=0
          go to 5030
5020      Cislo=Cislo(j:)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=5015) j
5030      if(k.eq.1) then
            ni=j
          else
            nj=j
          endif
        enddo
        if(.not.EqIgCase(At1,At)) then
          if(nd.gt.0) write(89,FormA) t80(:idel(t80))
          At=At1
          t80='CONECT'
          write(t80(7:11),102) ni
          ip=12
          nd=0
        endif
        if(SbwItemSelQuest(n,nSbw).eq.1) then
          if(ip.ge.32) then
            write(89,FormA) t80(:idel(t80))
            t80='CONECT'
            write(t80(7:11),102) ni
            ip=12
            nd=0
          endif
          nd=nd+1
          write(t80(ip:ip+4),102) nj
          ip=ip+5
        endif
        go to 5000
6000    if(ip.gt.12) write(89,FormA) t80(:idel(t80))
        call CloseIfOpened(ln)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call DeleteFile(fln(:ifln)//'_seldist.tmp')
9999  deallocate(dpmin,dpmax,dpmino,dpmaxo)
      call CloseIfOpened(ln)
      return
100   format(a8,1x,a8,1x,f8.3)
102   format(i5)
      end
