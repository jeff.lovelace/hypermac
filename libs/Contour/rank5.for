      function rank5(e)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension e(21)
      rank5=w1*(pm1015_1*e(1)+pm63_2*e(11)+pm63_3*e(15)+wm3p1*wmp2*e(4)
     1         +wmp3*(wm3p1*e(6)+wmp2*e(13))+
     2          w2*w3*(wm3p1*e(5)+wm3p2*e(12)+wm3p3*e(14)))+
     3      w2*(pm1015_2*e(16)+pm63_1*e(2)+pm63_3*e(20)+wm3p2*wmp1*e(7)
     4         +wmp3*(wm3p2*e(18)+wmp1*e(9)))+
     5      w3*(pm1015_3*e(21)+pm63_1*e(3)+pm63_2*e(17)+wm3p3*wmp1*e(10)
     6          +wmp2*(wm3p3*e(19)+wmp1*e(8)))
      return
      end
