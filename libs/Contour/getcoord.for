      subroutine GetCoord(xdo,xp,xfract)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xdo(3),xfract(3)
      xdo(1)=FeX2Xo(Xpos)
      xdo(2)=FeY2Yo(Ypos)
      xdo(3)=0.
      call multm(O2F,xdo,xp,3,3,1)
      xp(3)=zmap(1)
      do i=1,2
        xp(i)=max(xp(i),xmin(i))
        xp(i)=min(xp(i),xmax(i))
      enddo
      call multm(F2O,xp,xdo,3,3,1)
      if(xyzmap) then
        call prevod(1,xp,xfract)
      else
        call SetRealArrayTo(xfract,3,0.)
        do i=1,NDim(KPhase)
          j=iorien(i)
          if(j.le.3) then
            if(i.le.2) then
              xfract(j)=xp(i)
            else
              xfract(j)=zmap(i-2)
            endif
          endif
        enddo
      endif
      return
      end
