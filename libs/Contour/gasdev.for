      function gasdev()
      data iset/0/
      if(iset.eq.0) then
1       v1=2.*ran1(0)-1.
        v2=2.*ran1(0)-1.
        r=v1**2+v2**2
        if(r.ge.1.) go to 1
        fac=sqrt(-2.*log(r)/r)
        gset=v1*fac
        gasdev=v2*fac
        iset=1
      else
        gasdev=gset
        iset=0
      endif
      return
      end
