      subroutine TPCriticalPoints
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xat(3,2)
      character*256 EdwStringQuest
      character*80  Center,Veta,Atp(2)
      character*50  TypeOfSearch(4)
      character*30  Action(2)
      integer RolMenuSelectedQuest
      logical UzitStarou,FeYesNo,ExistFile
      data Center/' '/,IType/1/,nTypeOfSearch/4/
      data TypeOfSearch/'between all bonds within interval',
     1       'along specified line',
     2       'in paralleliped - cell fraction',
     3       'in paralleliped - general'/
      data Action/'Search for critical points','Integration'/
      data Atp/2*' '/
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      call ConSetLocDensity
      allocate(AtBrat(NAtCalc))
      n=max(NAtCalc,1000)
      MaxPDF=n
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      if(allocated(xcpa))
     1  deallocate(xcpa,CPRho,CPGrad,CPDiag,CPType)
      allocate(xcpa(3,100),CPRho(100),CPGrad(100),CPDiag(3,100),
     1         CPType(100),xdst(3,NAtCalc),BetaDst(6,NAtCalc),
     2         dxm(NAtCalc),BratPrvni(NAtCalc),BratDruhyATreti(NAtCalc))
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
1100  id=NextQuestId()
      xdq=400.
      xdqp=xdq*.5
      il=8
      call FeQuestCreate(id,-1.,-1.,xdq,il,
     1                   'Define parameters for CP search',1,LightGray,
     2                   0,0)
      il=1
      Veta='%Type of search'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=0.
      do i=1,nTypeOfSearch
        dpom=max(FeTxLength(TypeOfSearch(i))+20.,dpom)
      enddo
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom+EdwYd,
     1                        EdwYd,1)
      nRolMenuType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,TypeOfSearch,
     1                        nTypeOfSearch,IType)
      il=il+1
      Veta='CP %criterion - delta(Rho)<'
      xdl=FeTxLengthUnder(Veta)
      xpom=tpom+xdl+15.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCriter=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPCrit,.false.,.false.)
      il=il+1
      Veta='Max. %number of iterations'
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNIter=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,CPNIter,.false.)
      call FeQuestEudOpen(EdwLastMade,1,200,1,0.,0.,0.)
      il=il+1
      Veta='Max. iteration %step'
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxStep=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPMaxStep,.false.,.false.)
      il=il+1
      Veta='Min. %density'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRhoMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CPRhoMin,.false.,.false.)
      il=il+1
      Veta='%1st point'
      xdl=FeTxLengthUnder(Veta)
      xpom=tpom+xdl+15.
      dpom=110.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdw1stPoint=EdwLastMade
      Veta='%2nd point'
      xpom=xpom+xdqp
      tpom=tpom+xdqp
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdw2ndPoint=EdwLastMade
      tpom=5.
      Veta='D(%geom)'
      pom=FeTxLengthUnder(Veta)+10.
      dpom=50.
      xpom=tpom+pom
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDGeom=EdwLastMade
      tpom=xpom+dpom+12.
      xpom=tpom+pom
      Veta='D(m%in)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMin=EdwLastMade
      tpom=xpom+dpom+24.
      xpom=tpom+pom
      Veta='D(m%ax)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
1400  if(IType.eq.1) then
        call FeQuestEdwClose(nEdw1stPoint)
        call FeQuestEdwClose(nEdw2ndPoint)
        call FeQuestRealEdwOpen(nEdwDGeom,CPDGeom,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwDMin,CPDMin,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwDMax,CPDMax,.false.,.false.)
      else if(IType.eq.2) then
        call FeQuestEdwClose(nEdwDGeom)
        call FeQuestEdwClose(nEdwDMin)
        call FeQuestEdwClose(nEdwDMax)
        call FeQuestStringEdwOpen(nEdw1stPoint,atp(1))
        call FeQuestStringEdwOpen(nEdw2ndPoint,atp(2))
      else if(IType.eq.3.or.IType.eq.4) then
        call FeQuestEdwClose(nEdwDMin)
        call FeQuestEdwClose(nEdwDMax)
        call FeQuestEdwClose(nEdw1stPoint)
        call FeQuestEdwClose(nEdw2ndPoint)
        call FeQuestRealEdwOpen(nEdwDGeom,CPDGeom,.false.,.false.)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(IType.eq.2) then
          do i=1,2
            if(i.eq.1) then
              Veta=EdwStringQuest(nEdw1stPoint)
            else
              Veta=EdwStringQuest(nEdw2ndPoint)
            endif
            call CtiAt(Veta,xat(1,i),ich)
            if(ich.eq.-1) then
              atp(i)=Veta
              kat=1
              ich=0
            else if(ich.eq.0) then
              kat=0
            else
              go to 1500
            endif
          enddo
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuType) then
        i=RolMenuSelectedQuest(nRolMenuType)
        if(i.ge.1.and.i.le.4.and.i.ne.IType) then
          IType=i
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(IType.eq.1) then
          call FeQuestRealFromEdw(nEdwDMin,CPDMin)
          call FeQuestRealFromEdw(nEdwDMax,CPDMax)
        endif
        if(IType.ne.2) call FeQuestRealFromEdw(nEdwDGeom,CPDGeom)
        call FeQuestRealFromEdw(nEdwCriter,CPCrit)
        call FeQuestIntFromEdw(nEdwNIter, CPNIter)
        call FeQuestRealFromEdw(nEdwMaxStep,CPMaxStep)
        call FeQuestRealFromEdw(nEdwRhoMin,CPRhoMin)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      UzitStarou=.false.
      do i=1,6
        iorien(i)=i
      enddo
      if(IType.eq.2) then
        do i=1,3
          xmina(i,1)=min(xat(i,1),xat(i,2))
          xmaxa(i,1)=max(xat(i,1),xat(i,2))
        enddo
      else if(IType.eq.3) then
        if(ExistFile(fln(:ifln)//'.z81')) then
          UzitStarou=FeYesNo(-1.,-1.,'Do you want to analyze the '//
     1                       'previously created map',1)

        endif
        if(UzitStarou) then
          m8=NextLogicNumber()
          call OpenMaps(m8,fln(:ifln)//'.z81',nxny,0)
          if(ErrFlag.ne.0) go to 9999
          read(m8,rec=1) nx,nxny,nmapa,(xmin(i),xmax(i),i=1,6),dx,
     1                  (iorien(i),i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
          nsubs=mod(nsubs,10)
          n=nx(3)+3
          read(m8,rec=n) npdf
          allocate(xpdf(3,npdf),popaspdf(64,npdf),idpdf(npdf),
     1             dpdf(npdf),fpdf(npdf),ypdf(3,npdf),SelPDF(npdf),
     2             scpdf(npdf),iapdf(npdf),ispdf(npdf),ipor(npdf))
          do i=1,npdf
            n=n+1
            read(m8,rec=n) ip,iapdf(ip),ispdf(ip),fpdf(ip),
     1                    (xpdf(j,ip),j=1,3)
            ipor(i)=ip
          enddo
        else
          call CPReadScope(0,xmin,xmax,dx,i,CutOffDist,ich)
        endif
      else if(IType.eq.4) then
        call ConPrelim
        DensityType=5
        mcmax=0
        kpdf=6
        DrawPdf=.true.
        obecny=.true.
        call ConDefGenSection(Center,1,ich)
        if(ich.ne.0) go to 9999
      endif
      if(.not.UzitStarou) then
        if(IType.eq.1) then
          call ConDefAtForDenCalc(4,ich)
        else
          call ConDefAtForDenCalc(3,ich)
        endif
      endif
      if(ich.ne.0) go to 9999
      if(IType.eq.1) then
        call CPSearchBond
      else if(IType.eq.2) then
        call CPSearchAlongLine(xat(1,1),xat(1,2),atp(1),atp(2),kat,0)
        go to 1100
      else if(IType.eq.3.or.IType.eq.4) then
        call CPSearchInMap(IType-3,UzitStarou)
      endif
9999  if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      if(allocated(xcpa))
     1  deallocate(xcpa,CPRho,CPGrad,CPDiag,CPType,xdst,BetaDst,dxm,
     2             BratPrvni,BratDruhyATreti)
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      if(allocated(AtBrat)) deallocate(AtBrat)
      return
      end
