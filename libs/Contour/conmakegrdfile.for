      subroutine ConMakeGrdFile(ln,NzCopy,Type)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension WorkMap(:,:),CellParP(3)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      logical ExistFile
      allocatable WorkMap
      allocate(WorkMap(nxny,NzCopy))
      Klic=0
      go to 1000
      entry ConMakeGrdFileProlog(ln,NzCopy,Type)
      Klic=1
1000  if(Type.eq.0) then
        if(NzCopy.eq.1) then
          Veta='2'
        else
          Veta='3'
        endif
        Veta(2:)='DGRDFIL  0'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='FORM     FOU'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        Veta='! Gridpoints, Origin, Physical Dimensions'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105)(nx(i),i=1,2),NzCopy
        if(Obecny) then
          write(ln,106) ShiftPlane,ScopePlane
        else
          write(ln,106)((xmax(i)-xmin(i))*.5*CellParCon(i),i=1,3),
     1                 ((xmax(i)-xmin(i))   *CellParCon(i),i=1,3)
        endif
        Veta='! Objects'
        write(ln,FormA) Veta(:idel(Veta))
        n=0
        Veta=fln(:ifln)//'_dratoms_grd.tmp'
        if(ExistFile(Veta)) then
          lna=NextLogicNumber()
          call OpenFile(lna,Veta,'formatted','unknown')
          if(ErrFlag.ne.0) go to 1200
1100      read(lna,FormA,end=1200) Veta
          n=n+1
          go to 1100
        else
          lna=0
        endif
1200    write(ln,105) n
        if(n.gt.0) then
          rewind lna
1400      read(lna,FormA,end=1600) Veta
          write(ln,FormA) Veta(:idel(Veta))
          go to 1400
        endif
1600    call CloseIfOpened(lna)
        Veta='! Connections'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105) 0
        Veta='! Values'
        write(ln,FormA) Veta(:idel(Veta))
      else
        write(ln,FormA) fln(:ifln)
        m=0
        do i=1,3
          if(nx(i).le.1) cycle
          m=m+1
          CellParP(m)=CellParCon(m)*(xmax(i)-xmin(i))
        enddo
        call ConCellParNorm(CellParP)
        write(Veta,102)(CellParP(i),i=1,3)
        write(Veta(31:),102)(CellParCon(i),i=4,6)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,105)(nx(i),i=1,2),NzCopy
      endif
      if(Klic.eq.1) go to 9999
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j,i),j=1,nxny)
        if(Type.eq.0) then
          m=0
          do j=1,nx(2)
            write(ln,107)(WorkMap(l,i),l=m+1,m+nx(1))
            m=m+nx(1)
          enddo
        endif
      enddo
      if(Type.ne.0) then
        do j=1,nx(1)
          do k=1,nx(2)
            m=(k-1)*nx(1)+j
            write(ln,108)(WorkMap(m,i),i=1,NzCopy)
          enddo
        enddo
      endif
      go to 9999
5000  call FeReadError(m8)
9999  if(allocated(WorkMap)) deallocate(WorkMap)
      return
102   format(3f10.4)
105   format(3i15)
106   format(3f15.4)
107   format(6e13.5)
108   format(e13.5)
      end
