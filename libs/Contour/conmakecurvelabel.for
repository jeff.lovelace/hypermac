      subroutine ConMakeCurveLabel(Label,xpoints,n)
      dimension xpoints(3,n)
      character*(*) Label
      character*2   nty
      write(85,'(''# '',80a1)')(Label(i:i),i=1,idel(Label))
      do i=1,n-1
        if(n.gt.2) write(85,'(''# '',i2,a2,'' segment'')') i,nty(i)
        write(85,'(''# from : '',3f10.6)')(xpoints(j,i  ),j=1,3)
        write(85,'(''# to   : '',3f10.6)')(xpoints(j,i+1),j=1,3)
      enddo
      return
      end
