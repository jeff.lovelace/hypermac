      subroutine CPMakeOutputStart
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*6   CPTypeP
      character*128 Ven
      real ncpt(-3:3)
      real :: xp(1)=(/0./)
      logical EqIgCase
      LstOpened=.true.
      uloha='Program for topological analysis of static density'
      call newpg(1)
      call iom50(0,1,fln(:ifln)//'.m50')
      call comsym(0,1,ich)
      if(ErrFlag.ne.0.or.ich.ne.0) go to 9999
      call newln(4)
      write(lst,'(/'' Type       x         y         z     Mult    '',
     1             ''Rho     Lap       Grad         Hess eigenvalues'',
     2             ''        Ellip''/
     2             10x,''Distances to neighbour atoms''/)')
      go to 9999
      entry  CPMakeOutputFinish(IType)
      CPTypeP='(3,-3)'
      if(NatCalc.ge.MxAtAll) call ReallocateAtoms(10)
      NAtCalc=NAtCalc+1
      call SetBasicKeysForAtom(NAtCalc)
      atom(NAtCalc)='Itself'
      do k=-3,3
        write(CPTypeP(4:5),100) k
        ncpt(k)=0.
        do i=1,ncp
          if(CPType(i).ne.CPTypeP) cycle
          call od0do1(xcpa(1,i),xcpa(1,i),3)
          call CopyVek(xcpa(1,i),x(1,NAtCalc),3)
          call specat
          call DistForOneAtom(NAtCalc,CPDGeom,1,0)
          do j=1,ndist
            l=ipord(j)
            if(.not.EqIgCase(adist(l),'Itself')) then
              call CopyVek(xdist(1,l),xcpa(1,i),3)
              call CopyVek(xcpa(1,i),x(1,NAtCalc),3)
              exit
            endif
          enddo
          call SpecPos(xcpa(1,i),xp,0,1,.05,nocc)
          ncpt(k)=ncpt(k)+1./float(nocc)
          call DistForOneAtom(NAtCalc,CPDGeom,1,1)
          if(CpRho(i).gt.-900.) then
            if(CPType(i).eq.'(3,-1)') then
              write(ven,103) CPType(i),(xcpa(j,i),j=1,3),nocc,CPRho(i),
     1                       CPDiag(1,i)+CPDiag(2,i)+CPDiag(3,i),
     2                       CPGrad(i),(CPDiag(j,i),j=1,3),
     3                       CPDiag(1,i)/CPDiag(2,i)-1.
            else
              write(ven,103) CPType(i),(xcpa(j,i),j=1,3),nocc,CPRho(i),
     1                       CPDiag(1,i)+CPDiag(2,i)+CPDiag(3,i),
     2                       CPGrad(i),(CPDiag(j,i),j=1,3)
            endif
          else
            write(ven,103) CPType(i),(xcpa(j,i),j=1,3)
          endif
          call newln(3)
          write(lst,FormA1)(ven(m:m),m=1,idel(ven))
          il=1
          do j=1,ndist
            l=ipord(j)
            if(EqIgCase(adist(l),'Itself')) cycle
            ven=' '
            if(ddist(l).gt..001) then
              write(ven(10:),'(f7.4)') ddist(l)
              ven=ven(:idel(ven))//'A from '
            else
              ven(11:)='coincides with '
            endif
            ven=ven(:idel(ven)+1)//adist(l)(:idel(adist(l)))
            if(.not.EqIgCase(SymCodeDist(l),'x,y,z'))
     1        ven=ven(:idel(ven))//'#'//
     2            SymCodeDist(l)(:idel(SymCodeDist(l)))
            il=il+1
            if(il.gt.3) call newln(1)
            write(lst,FormA1)(ven(m:m),m=1,idel(ven))
            if(ddist(l).le..001) exit
          enddo
          il=il+1
          if(il.gt.3) call newln(1)
          write(lst,FormA1)
        enddo
      enddo
      NAtCalc=NAtCalc-1
      if(IType.lt.3) go to 9000
      call newln(3)
      write(lst,'(/''CP statistics : ''/)')
      SumCP=0.
      CPTypeP='(3,-3)'
      do k=-3,3
        if(ncpt(k).le.0.) cycle
        if(k.eq.-3.or.k.eq.1) then
          zn=1.
        else if(k.eq.-1.or.k.eq.3) then
          zn=-1.
        else
          zn=0.
        endif
        SumCP=SumCP+zn*ncpt(k)
        write(CPTypeP(4:5),100) k
        call newln(1)
        write(lst,'(a6,'' number '',f8.3)') CPTypeP,ncpt(k)
      enddo
      call newln(2)
      write(lst,'(/''Checking summ : '',f8.3)') SumCP
100   format(i2)
103   format(a6,3f10.4,i5,2f9.4,e12.4,4f9.4)
9000  call CloseListing
9999  return
      end
