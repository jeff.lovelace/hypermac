      subroutine TrPor
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      call UnitMat(tm,NDim(KPhase))
      do i=1,NDim(KPhase)
        do j=1,NDim(KPhase)
          k=i+(j-1)*NDim(KPhase)
          if(j.eq.iorien(i)) then
            tm(k)=1.
          else
            tm(k)=0.
          endif
        enddo
      enddo
      call matinv(tm,tmi,pom,NDim(KPhase))
      call MatBlock3(tm,tm3,NDim(KPhase))
      call MatBlock3(tmi,tm3i,NDim(KPhase))
      if(mapa.gt.0.and.mapa.le.3.and.kpdf.le.1) then
        ncst=2
      else
        ncst=1
      endif
      return
      end
