      subroutine ConDefDeform(ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*1 t(5)
      character*2 nty
      character*80 Veta
      logical CrwLogicQuest
      dimension nh(30)
      data t/'2','3','4','5','6'/
      NFirst=NAtIndFrAll(KPhase)
      xqd=WizardLength
1100  id=NextQuestId()
      n=0
      NFrom=NFirst
      do i=NFirst,NAtIndToAll(KPhase)
        if(AtBrat(i)) then
          n=n+1
          nh(n)=i
          if(n.ge.30) go to 1110
        endif
      enddo
      i=NAtIndToAll(KPhase)
      if(n.le.0) then
        ich=0
        go to 9999
      endif
1110  NFirst=i+1
      Veta='Select of anharmonic terms for deformation maps'
      call FeQuestTitleMake(id,Veta)
      xpom=20.
      il=1
      if(n.le.15) then
        jk=1
      else
        jk=2
      endif
      do j=1,jk
        call FeQuestLblMake(id,xpom,il,'Atom','L','N')
        xpom=xpom+70.
        do i=1,5
          call FeQuestLblMake(id,xpom,il,t(i)//nty(i),'L','N')
          xpom=xpom+40.
        enddo
        xpom=xqd*.5+20.
      enddo
      if(n.gt.15) call FeQuestSvisliceFromToMake(id,xqd*.5,1,16,0)
      xpoms=20.
      do i=1,n
        il=il+1
        ia=nh(i)
        xpom=xpoms
        call FeQuestLblMake(id,xpom,il,atom(ia),'L','N')
        xpom=xpom+70.
        itfi=itf(ia)-1
        if(itfi.le.0) itfi=1
        do j=1,itfi
          call FeQuestCrwMake(id,xpom,il,xpom,il,' ','L',CrwXd,
     1                        CrwYd,0,0)
          call FeQuestCrwOpen(CrwLastMade,AtBratAnharm(j,ia))
          if(i.eq.1.and.j.eq.1) nCrwFirst=CrwLastMade
          xpom=xpom+40.
        enddo
        if(il.eq.16) then
          il=1
          xpoms=xpoms+xqd*.5
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      nCrw=nCrwFirst
      if(ich.eq.0) then
        k=0
        do i=1,n
          ia=nh(i)
          itfi=itf(ia)-1
          if(itfi.le.0) itfi=1
          do j=1,5
            if(j.le.itfi) then
              AtBratAnharm(j,ia)=CrwLogicQuest(nCrw)
              nCrw=nCrw+1
            else
              AtBratAnharm(j,ia)=.false.
            endif
          enddo
        enddo
      else
        if(ich.gt.0) then
          if(NFrom.gt.1) then
            NFirst=max(NFrom-30,1)
            go to 1100
          else
            go to 9999
          endif
        else
          go to 9999
        endif
      endif
      if(NFirst.le.NAtIndToAll(KPhase)) go to 1100
9999  return
      end
