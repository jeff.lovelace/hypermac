      subroutine ConWriteKeysSaveAsWithRepeat(Radka)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Radka
      call ConWriteKeysSaveAs(1,Radka)
      if(Obecny) then
        IPlanePrev=IPlane
        if(Radka.ne.' ') then
          IPlane=LocateInStringArray(LabelPlane,NumberOfPlanes,Radka,
     1                               .true.)
        else
          IPlane=IPlanePrev
        endif
        if(IPlane.le.0.and.Radka.ne.' ') then
          call ConReallocatePlanes
          NumberOfPlanes=NumberOfPlanes+1
          LabelPlane(NumberOfPlanes)=Radka
          IPlane=NumberOfPlanes
          SaveAs=1
        else
          if(IPlanePrev.eq.IPlane) then
            SaveAs=0
          else
            IPlaneOld=IPlane
            SaveAs=1
          endif
        endif
      endif
      return
      end
