      subroutine CPSearchAlongLine(xat1,xat2,At1,At2,JsouToAtomy,Tisk)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80,Titulek
      character*(*)  At1,At2
      integer Tisk
      logical DvaBody,PointAlreadyPresent
      dimension xat1(3),xat2(3),xcp(3),xp(6),diag(3),xn(6)
      if(JsouToAtomy.eq.1) then
        Titulek='Search for CP along bond '//At1(:idel(At1))//'-'//
     1          At2(:idel(At2))
      else
        Titulek='Search for CP along the defined line'
      endif
      do i=1,3
        xcp(i)=xat1(i)+(xat2(i)-xat1(i))*.5
      enddo
      DvaBody=.true.
      go to 1450
      entry CPSearchFromPoint(xat1,Tisk)
      DvaBody=.false.
      Titulek='Search starting from point'
      call CopyVek(xat1,xcp,3)
1450  call CPNewton(xcp,Rho,Rho2D,Grad,Diag,NonZero,NSgn,ich)
      if(ich.eq.0.and.Rho.gt.CPRhoMin) then
        Ellip=Diag(1)/Diag(2)-1.
        if(DvaBody) then
          do i=1,3
            xp(i)=xcp(i)-xat1(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d1=sqrt(ScalMul(xp,xp(4)))
          do i=1,3
            xp(i)=xcp(i)-xat2(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d2=sqrt(ScalMul(xp,xp(4)))
          do i=1,3
            xp(i)=xat1(i)-xat2(i)
          enddo
          call multm(MetTens(1,1,KPhase),xp,xp(4),3,3,1)
          d0=sqrt(ScalMul(xp,xp(4)))
        endif
        write(Cislo,'(''('',i1,'','',i2,'')'')') NonZero,NSgn
        if(Tisk.eq.0.or.Tisk.eq.2) then
          NInfo=5
          write(t80,'(3f8.4)') xcp
          TextInfo(1)='Critical point of type '//Cislo(:idel(Cislo))//
     1                ' found at '//t80(:idel(t80))
          k=1
          if(DvaBody) then
            write(Cislo,100) d1
            if(JsouToAtomy.eq.1) then
              t80=At1
            else
              t80='1st point'
            endif
            TextInfo(2)='Displaced '//Cislo(:6)//' from '//
     1                  t80(:idel(t80))//' and'
            write(Cislo,100) d2
            if(JsouToAtomy.eq.1) then
              t80=At2
            else
              t80='2nd point'
            endif
            TextInfo(2)=TextInfo(2)(:idel(TextInfo(2))+1)//Cislo(:6)//
     1                  ' from '//t80(:idel(t80))
            write(Cislo,100) d1+d2
            if(JsouToAtomy.eq.1) then
              t80='bond distance'
            else
              t80='distance between points'
            endif
            TextInfo(3)='Making sum '//Cislo(:6)//' which exceeds '//
     1                  t80(:idel(t80))
            write(Cislo,100) d0
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3))+1)//Cislo(:6)
            write(Cislo,100) d1+d2-d0
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3))+1)//'by '//
     1                  Cislo(:6)
            k=3
          else
            NAtCalc=NAtCalc+1
            ai(NAtCalc)=1.
            iswa(NAtCalc)=1
            kswa(NAtCalc)=KPhase
            atom(NAtCalc)='Itself'
            call CopyVek(xcp,x(1,NAtCalc),3)
            call specat
            call DistForOneAtom(NAtCalc,CPDGeom,1,0)
            NAtCalc=NAtCalc-1
            if(ndist.gt.0) then
              do 3100i=1,ndist
                j=ipord(i)
                if(adist(j).eq.'Itself') go to 3100
                k=k+1
                write(TextInfo(k),'(''Short distance '',f8.2,'' to '',
     1                              a8)') ddist(j),adist(j)
                if(k.gt.8) go to 3110
3100          continue
            else
              k=k+1
              TextInfo(k)='There are no short distances to this point'
            endif
          endif
3110      k=k+1
          write(Cislo,101) Rho
          TextInfo(k)='Local density '//Cislo(:7)//' Laplacian'
          write(Cislo,101) Rho2d
          TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))+1)//Cislo(:7)//
     1                ' Ellipticity'
          write(Cislo,101) Ellip
          TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))+1)//Cislo(:7)
          write(t80,101)(Diag(i),i=1,3)
          k=k+1
          TextInfo(k)='Eigenvalues of Hessian matrix '//t80(:idel(t80))
          NInfo=k
          call FeInfoOut(-1.,-1.,Titulek,'L')
        else
          if(.not.PointAlreadyPresent(xcp,xn,xcpa,ncp,.05,.false.,dpom,
     1                                j,j,1)) then
            ncp=ncp+1
            call CopyVek(xcp,xcpa(1,ncp),3)
            call CopyVek(Diag,CPDiag(1,ncp),3)
            CPRho(ncp)=Rho
            CPGrad(ncp)=Grad
            CPType(ncp)=Cislo
          endif
        endif
      endif
      return
100   format(f6.4)
101   format(3f7.2)
      end
