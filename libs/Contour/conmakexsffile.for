      subroutine ConMakeXSFFile(ln,NzCopy,Type)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real, allocatable :: WorkMap(:)
      character*80 Veta
      integer Zdvih,Type,ConGetFirstSection
      logical ExistFile
      allocate(WorkMap(nxny))
      write(ln,FormA) 'CRYSTAL'
      write(ln,FormA) 'PRIMVEC'
      if(Obecny) then
        write(ln,102) float(nx(1)-1)*dx(1),0.,0.
        write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
        write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      else
        write(ln,102)(TrToOrtho(i,1,KPhase),i=1,9)
      endif
      write(ln,FormA) 'CONVVEC'
      if(Obecny) then
        write(ln,102) float(nx(1)-1)*dx(1),0.,0.
        write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
        write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      else
        write(ln,102)(TrToOrtho(i,1,KPhase),i=1,9)
      endif
      Veta=fln(:ifln)//'_dratoms_vesta.tmp'
      if(ExistFile(Veta)) then
        write(ln,FormA) 'PRIMCOORD'
        lna=NextLogicNumber()
        call OpenFile(lna,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1200
        n=0
1100    read(lna,FormA,end=1150) Veta
        n=n+1
        go to 1100
1150    rewind lna
        write(ln,'(2i5)') n,1
1170    read(lna,FormA,end=1200) Veta
        idl=idel(Veta)
        Veta=Veta(4:)
        write(ln,FormA) Veta(:idel(Veta))
        go to 1170
      else
        lna=0
      endif
1200  write(ln,FormA) 'BEGIN_BLOCK_DATAGRID_3D'
      write(ln,FormA) '  '//fln(:ifln)
      write(ln,FormA) '  BEGIN_DATAGRID_3D_this_is_2Dgrid#1'
      write(ln,101)(nx(i),i=1,3)
      write(ln,102) 0.,0.,0.
      write(ln,102) float(nx(1)-1)*dx(1),0.,0.
      write(ln,102) 0.,float(nx(2)-1)*dx(2),0.
      write(ln,102) 0.,0.,float(nx(3)-1)*dx(3)
      Zdvih=ConGetFirstSection(NxDraw,nx(3),NDim(KPhase)-2)
      do i=1,NzCopy
        read(m8,rec=i+Zdvih,err=5000)(WorkMap(j),j=1,nxny)
        write(ln,107)(WorkMap(j),j=1,nxny)
      enddo
5000  write(ln,FormA) '  END_DATAGRID_3D'
      write(ln,FormA) 'END_BLOCK_DATAGRID_3D'
      call CloseIfOpened(lna)
      if(allocated(WorkMap)) deallocate(WorkMap)
101   format(6i5)
102   format(3f13.8)
107   format(5e15.6)
      end
