      subroutine ConSetCellParCon3
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      m=0
      do i=1,NDim(KPhase)
        m=m+1
        j=iorien(i)
        CellParCon(m)=
     1               sqrt(MetTens(j+(j-1)*3,NSubs,KPhase))
        if(m.ge.3) exit
      enddo
      mi=0
      m=0
      do i=1,NDim(KPhase)
        j1=iorien(i)
        mi=mi+1
        mj=mi
        do j=i+1,NDim(KPhase)
          if(nx(j).le.1) cycle
          j2=iorien(j)
          mj=mj+1
          m=m+1
          pom=MetTens(j1+(j2-1)*3,NSubs,KPhase)/
     1        (CellParCon(mi)*CellParCon(mj))
          if(abs(pom).lt.1.) then
            CellParCon(9-mi-mj)=acos(pom)/ToRad
          else
            CellParCon(9-mi-mj)=90.
          endif
          if(mj.ge.3) exit
        enddo
        if(mi.ge.2) exit
      enddo
      return
      end
