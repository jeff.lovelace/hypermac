      subroutine ConMakeDrawAtString(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) String
      String=IdContour(nCmdDrawAt)
      if(DrawAtSkip(n)) String='!'//String(:idel(String))
      call uprat(DrawAtName(n))
      String=String(:idel(String)+1)//
     1       DrawAtName(n)(:idel(DrawAtName(n)))
      if(DrawAtColor(n).eq.0) then
        Cislo='Default'
      else
        Cislo=ColorNames(DrawAtColor(n))
      endif
      String=String(:idel(String)+1)//Cislo(:idel(Cislo))
      write(Cislo,'(f15.3)') DrawAtBondLim(n)
      call ZdrcniCisla(Cislo,1)
      String=String(:idel(String)+1)//Cislo(:idel(Cislo))
      return
      end
