      subroutine ConSummMaps(ior)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),ior(6)
      real InputMap(:),table(:)
      allocatable InputMap,table
      data xp/3*0./
      allocate(InputMap(nxny),table(nxny))
      NSum=0
      nx3=nx(3)
      nx4=nx(4)
      nx5=nx(5)
      if(mod(isoucet/1000,10).eq.1) then
        xmin(6)=xmin(6)+float(nxto(4)+nxfrom(4)-2)*dx(6)*.5
        xmax(6)=xmin(6)
        nx(6)=1
      endif
      if(mod(isoucet/100,10).eq.1) then
        xmin(5)=xmin(5)+float(nxto(3)+nxfrom(3)-2)*dx(5)*.5
        xmax(5)=xmin(5)
        nx(5)=1
      endif
      if(mod(isoucet/10,10).eq.1) then
        xmin(4)=xmin(4)+float(nxto(2)+nxfrom(2)-2)*dx(4)*.5
        xmax(4)=xmin(4)
        nx(4)=1
      endif
      if(mod(isoucet,10).eq.1) then
        xmin(3)=xmin(3)+float(nxto(1)+nxfrom(1)-2)*dx(3)*.5
        xmax(3)=xmin(3)
        nx(3)=1
      endif
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      write(86,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,ior,mapa,
     1                nsubs,SatelityByly,nonModulated(KPhase)
      izz=1
      Dmin= 1.0e+30
      Dmax=-1.0e+30
      nvse=(nxto(1)-nxfrom(1)+1)*(nxto(2)-nxfrom(2)+1)*
     1     (nxto(3)-nxfrom(3)+1)
      n=0
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nvse)*.005),10),nvse,
     1                     'Summation of maps',' ',' ')
      if(isoucet.eq.1111) then
        call SetRealArrayTo(table,nxny,0.)
        nSum=0
      endif
      irec1=nxfrom(4)-1
      do iii=nxfrom(4),nxto(4)
        if(isoucet.eq.111) then
          call SetRealArrayTo(table,nxny,0.)
          NSum=0
        endif
        irec2=irec1*nx5+nxfrom(3)-1
        do jjj=nxfrom(3),nxto(3)
          if(isoucet.eq.11) then
            call SetRealArrayTo(table,nxny,0.)
            NSum=0
          endif
          irec3=irec2*nx4+nxfrom(2)-1
          do kkk=nxfrom(2),nxto(2)
            if(isoucet.eq.1) then
              call SetRealArrayTo(table,nxny,0.)
              NSum=0
            endif
            irec=irec3*nx3+nxfrom(1)
            do lll=nxfrom(1),nxto(1)
              irec=irec+1
              call FeFlowChartEvent(n,is)
              if(is.ne.0) then
                call FeBudeBreak
                if(ErrFlag.ne.0) then
                  close(82,status='delete')
                  go to 9999
                endif
              endif
              read(m8,rec=irec,err=9000)(InputMap(i),i=1,nxny)
              NSum=NSum+1
              call AddVek(table,InputMap,table,nxny)
            enddo
            if(isoucet.eq.1) then
              pom=1./Float(NSum)
              do i=1,nxny
                table(i)=table(i)*Pom
                Dmax=max(Dmax,table(i))
                Dmin=min(Dmin,table(i))
              enddo
              izz=izz+1
              write(86,rec=izz)(table(i),i=1,nxny)
            endif
            irec3=irec3+1
          enddo
          if(isoucet.eq.11) then
            pom=1./Float(NSum)
            do i=1,nxny
              table(i)=table(i)*Pom
              Dmax=max(Dmax,table(i))
              Dmin=min(Dmin,table(i))
            enddo
            izz=izz+1
            write(86,rec=izz)(table(i),i=1,nxny)
          endif
          irec2=irec2+1
        enddo
        if(isoucet.eq.111) then
          pom=1./Float(NSum)
          do i=1,nxny
            table(i)=table(i)*Pom
            Dmax=max(Dmax,table(i))
            Dmin=min(Dmin,table(i))
          enddo
          izz=izz+1
          write(86,rec=izz)(table(i),i=1,nxny)
        endif
        irec1=irec1+1
      enddo
      call FeFlowChartRemove
      if(isoucet.eq.1111) then
        pom=1./Float(NSum)
        do i=1,nxny
          table(i)=table(i)*Pom
          Dmax=max(Dmax,table(i))
          Dmin=min(Dmin,table(i))
        enddo
        izz=izz+1
        write(86,rec=izz)(table(i),i=1,nxny)
      endif
      izz=izz+1
      write(86,rec=izz) Dmax,Dmin
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
      call FeFlowChartRemove
9999  deallocate(InputMap,table)
      return
      end
