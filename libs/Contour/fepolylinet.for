      subroutine FePolyLineT(n,xpole,ypole,Color)
      include 'fepc.cmn'
      dimension xpole(n),ypole(n)
      integer Color
      call FePolyLine(n,xpole,ypole,Color)
      do i=-1,1,2
        pom=float(i)
        do j=1,n
          xpole(j)=xpole(j)+pom
        enddo
        call FePolyLine(n,xpole,ypole,Color)
      enddo
      do j=1,n
        xpole(j)=xpole(j)-pom
      enddo
      return
      end
