      subroutine ConReallocatePlanes
      use Contour_mod
      if(NumberOfPlanes.ge.NumberOfPlanesMax) then
        if(NumberOfPlanes.gt.0) then
          allocate(LabelPlaneO(NumberOfPlanes))
          LabelPlaneO(1:NumberOfPlanes)=LabelPlane(1:NumberOfPlanes)
        endif
        if(allocated(LabelPlane)) deallocate(LabelPlane)
        n=2*(NumberOfPlanes+1)
        allocate(LabelPlane(n))
        NumberOfPlanesMax=n
        if(NumberOfPlanes.gt.0) then
          LabelPlane(1:NumberOfPlanes)=LabelPlaneO(1:NumberOfPlanes)
          deallocate(LabelPlaneO)
        endif
      endif
      return
      end
