      subroutine ConHitMC
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*12 at,pn
      sp=0.
2000  k=0
      do n=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(.not.SelPDF(n)) cycle
        do j=1,DelkaKiAtomu(n)
          if(KiA(j,n).eq.1) then
            k=k+1
            rnda(k)=gasdev()
            p=rnds(k)+fsum(k)
            call KdoCo(PrvniKiAtomu(n)+j-1,at,pn,-1,p,sp)
          endif
        enddo
        if(.not.ChargeDensities) then
          if(beta(1,n).le.0..or.
     1       beta(1,n)*beta(2,n)-beta(4,n)**2.le.0..or.
     2       beta(1,n)*beta(2,n)*beta(3,n)-beta(3,n)*beta(4,n)**2-
     3       beta(1,n)*beta(6,n)**2-beta(2,n)*beta(5,n)**2+
     4       2.*beta(4,n)*beta(5,n)*beta(6,n).le.0.) go to 2000
        endif
      enddo
      do i=1,neq
        call ApeqC(i)
      enddo
      return
      end
