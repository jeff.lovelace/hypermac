      subroutine ConDrawAtomWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      character*(*) Command
      character*256 EdwStringQuest
      character*80  AtName,ErrString
      save /DrawAtomQuest/
      ich=0
      Command='drawatom'
      AtName=EdwStringQuest(nEdwName)
      if(AtName.eq.' ') go to 9100
      call TestAtomString(AtName,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command)+1)//AtName(:idel(AtName))
      if(icolor.le.1) then
        Cislo='Default'
      else
        Cislo=ColorNames(icolor-1)
      endif
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwBond,BondLim)
      write(Cislo,'(f15.3)') BondLim
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  if(ErrString.eq.'Jiz oznameno') go to 9100
      call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
