      subroutine ConReadDrawAtString(String,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(1)
      character*(*) String
      character*80  Veta
      integer ColorOrder
      logical EqIgCase
      k=1
1000  if(String(k:k).eq.' ') then
        k=k+1
        go to 1000
      endif
      DrawAtSkip(n)=String(k:k).eq.'!'
      k=k-1
      call kus(String,k,Cislo)
      call kus(String,k,DrawAtName(n))
      call uprat(DrawAtName(n))
      call kus(String,k,Veta)
      if(EqIgCase(Veta,'Default')) then
        DrawAtColor(n)=0
      else
        DrawAtColor(n)=ColorOrder(Veta)
      endif
      if(k.lt.len(String)) then
        call StToReal(String,k,xp,1,.false.,ich)
        if(ich.ne.0) then
          n=n-1
          go to 9999
        endif
        DrawAtBondLim(DrawAtN)=xp(1)
      else
        DrawAtBondLim(DrawAtN)=2.
      endif
9999  return
      end
