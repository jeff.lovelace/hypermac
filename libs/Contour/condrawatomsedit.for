      subroutine ConDrawAtomsEdit
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      character*80 Veta,AtName
      character*15 Men(0:20)
      integer WhatHappened,RolMenuSelectedQuest
      external ConDrawAtomReadCommand,ConDrawAtomWriteCommand,FeVoid
      save /DrawAtomQuest/
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_drawatom.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9999
      Men(0)='Default'
      do i=1,PocetBarev
        Men(i)=ColorNames(i)
      enddo
      do i=1,DrawAtN
        call ConMakeDrawAtString(i,Veta)
        write(ln,FormA1)(Veta(j:j),j=1,idel(Veta))
      enddo
      call CloseIfOpened(ln)
      xqd=500.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_drawatom.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xd=0.
      do i=0,PocetBarev
        xd=max(xd,FeTxLength(Men(i)))
      enddo
      xd=xd+20.
      xpomt=5.
      Veta='%Atom name'
      xpome=xpomt+FeTxLengthUnder(Veta)+10.
      xpom=xpome
      dpom=160.
      do i=1,3
       if(i.ne.2)
     1    call FeQuestEdwMake(id,xpomt,il,xpome,il,Veta,'L',dpom,EdwYd,
     2                        0)
       if(i.eq.1) then
          nEdwName=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
          xpomt=xpome+dpom+15.
          Veta='Colo%r'
          xpome=xpomt+FeTxLengthUnder(Veta)+10.
          dpom=xd
        else if(i.eq.2) then
          call FeQuestRolMenuMake(id,xpomt,il,xpome,il,Veta,'L',
     1                            dpom+EdwYd,EdwYd,1)
          nRolMenuColor=RolMenuLastMade
          xpomt=5.
          Veta='Bond %limit'
          xpome=xpom
          il=il+1
        else if(i.eq.3) then
          nEdwBond=EdwLastMade
        endif
      enddo
1300  icolor=1
      AtName=' '
      BondLim=2.
1350  call FeQuestStringEdwOpen(nEdwName,AtName)
      call FeQuestRolMenuOpen(nRolMenuColor,Men,PocetBarev+1,icolor)
      call FeQuestRealEdwOpen(nEdwBond,BondLim,.false.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  ConDrawAtomReadCommand,ConDrawAtomWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuColor)
     1  then
        icolor=max(RolMenuSelectedQuest(nRolMenuColor),1)
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.ne.0) then
        ErrFlag=1
      else
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_drawatom.tmp','formatted',
     1                'unknown')
        DrawAtN=0
3000    read(ln,FormA80,end=3100) Veta
        DrawAtN=DrawAtN+1
        call ConReadDrawAtString(Veta,DrawAtN)
        go to 3000
3100    close(ln,status='delete')
      endif
      call ConWriteKeys
      call DefaultContour
      call NactiContour
      call ConReadKeys
9999  return
      end
