      subroutine ChdIntThR
      use Contour_mod
      parameter (maxray=500)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension soth(*),wt(*),ia(mxth),rstar(mxth),rfin(mxth),s(3),
     1          wvp(:),r(:),xar(maxray),war(maxray),RhoArr(:),vp(:,:)
      integer dirad(12)
      allocatable wvp,r,RhoArr,vp
      data dirad/12,16,20,24,28,32,36,40,48,48,64,96/
      save wvp,r,RhoArr,vp
      entry ChdIntThRAllocate
      allocate(wvp(nRInt*nThInt),r(3*nRInt*nThInt),RhoArr(mpts*10),
     1         vp(nprops,mpts))
      go to 9999
      entry ChdIntThRSumm(iPhi,soth,wt)
      call SetRealArrayTo(soth,nprops,0.)
      RR=RCapture(IAtInt)
      if(ibeta.ne.0) then
        do i=1,nThInt
          rfin(i)=sur(i,iPhi)
          if(rfin(i).lt.TInfInt-0.01) cycle
          if(i.gt.1.and.i.lt.nThInt) then
            if(abs(rfin(i)-sur(i-1,iPhi)).le.TInfInt*.5.or.
     1         abs(rfin(i)-sur(i+1,iPhi)).le.TInfInt*.5) cycle
            rfin(i)=sur(i-1,iPhi)+(sur(i+1,iPhi)-sur(i-1,iPhi))*
     1               (theta(i)-theta(i-1))/(theta(i+1)-theta(i-1))
          else
            if(iPhi.le.1.or.iPhi.ge.nPhiInt) cycle
            if(abs(rfin(i)-sur(i,iPhi-1)).le.TInfInt*.5.or.
     1         abs(rfin(i)-sur(i,iPhi+1)).le.TInfInt*.5) cycle
            rfin(i)=sur(i,iPhi-1)+(sur(i,iPhi+1)-sur(i,iPhi-1))*
     1               (phi(iPhi)-phi(iPhi-1))/(phi(iPhi+1)-phi(iPhi-1))
          endif
        enddo
        call SetRealArrayTo(rstar,nThInt,RR)
        do i=1,nThInt
          DiffR=rfin(i)-RR
          if(DiffR.le.2.) then
            ia(i)=dirad(nint(DiffR/.2)+1)
          else
            if(DiffR.le.3.) then
              ia(i)=dirad(11)
            else
              ia(i)=dirad(12)
            endif
          endif
          ia(i)=min(ia(i),nThInt)
        enddo
      else
        call SetRealArrayTo(rstar,nThInt,0.)
        call SetRealArrayTo(rfin ,nThInt,RR)
        call SetIntArrayTo(ia,nThInt,nRInt)
      endif
      n=0
      n3=0
      call SetRealArrayTo(vp,nprops*mpts,0.)
      do i=1,nThInt
        ss=stheta(i)
        s(1)=ss*cphi(iPhi)
        s(2)=ss*sphi(iPhi)
        s(3)=ctheta(i)
        ss=wt(i)*ss
        call gauleg(rstar(i),rfin(i),xar,war,ia(i))
        do j=1,ia(i)
          do k=1,3
            n3=n3+1
            r(n3)=xar(j)*s(k)+rcla(k,IAtInt)
          enddo
          n=n+1
          wvp(n)=war(j)*ss*xar(j)**2
        enddo
      enddo
      i=0
      i3=0
      pom=0.
4000  m=min(n-i,mpts)
      if(m.le.0) go to 9999
      call ChdGetRhoArray(RhoArr,r(i3+1),m,-2)
      call ChdCalcProperties(RhoArr,r(i3+1),vp,m)
      if(ErrFlag.ne.0) go to 9999
      call cultm(vp,wvp(i+1),soth,nprops,m,1)
      i =i +mpts
      i3=i3+mpts3
      go to 4000
      entry ChdIntThRDeallocate
      deallocate(wvp,r,RhoArr,vp)
9999  return
      end
