      subroutine SetContours(klic)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer Exponent10
      fmz=max(Dmax,abs(Dmin))*.1
      rad=10.**Exponent10(fmz)
      fmz=fmz/rad
      if(fmz.lt.1.5) then
        fmz=1.
      else if(fmz.lt.3.5) then
        fmz=2.
      else if(fmz.lt.7.5) then
        fmz=5.
      else
        fmz=10.
      endif
      fmz=fmz*rad
      fmzp=fmz
      fmzn=fmz
      cutpos= Dmax
      cutneg= Dmin
      if(klic.eq.0) ContourType=ContourTypeLinear
      return
      end
