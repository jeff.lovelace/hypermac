      subroutine ChdGenSymmAtoms
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x1(3),FractToLocal(9),OrthoToLocal(9),RPop(680),px(9)
      do i=1,2
        NaChd=0
        do j=1,NAtCalc
          if(kswa(j).ne.KPhase.or.ai(j).le.0) cycle
          js=0
          do jsym=1,NSymmN(KPhase)
            js=js+1
            if(isa(js,j).le.0) cycle
            call multm(rm6(1,jsym,1,KPhase),x(1,j),x1,3,3,1)
            call AddVek(x1,s6(1,jsym,1,KPhase),x1,3)
            do jcenter=1,NLattVec(KPhase)
              NaChd=NaChd+1
              if(i.eq.1) cycle
              call AddVek(x1,vt6(1,jcenter,1,KPhase),XfChd(1,NaChd),3)
              call od0do1(XfChd(1,NaChd),XfChd(1,NaChd),3)
              IaChd(NaChd)=j
              IsChd(NaChd)=jsym
              lasmaxi=lasmax(j)
              if(jsym.eq.1) then
                call CopyMat(TrAt(1,j),FractToLocal,3)
              else
                call MatInv(rm(1,jsym,1,KPhase),px,pom,3)
                call MultM(TrAt(1,j),px,FractToLocal,3,3,3)
              endif
              if(lasmaxi.gt.1) then
                call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
                call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
                jp=2
                kp=2
                popasChd(1,NaChd)=popas(1,j)
                do l=1,lasmaxi-2
                  n=2*l+1
                  call multm(RPop(kp),popas(jp,j),popasChd(jp,NaChd),n,
     1                       n,1)
                  kp=kp+n**2
                  jp=jp+n
                enddo
              endif
            enddo
          enddo
        enddo
        if(i.eq.1)
     1    allocate(IaChd(NaChd),IsChd(NaChd),XfChd(3,NaChd),
     2             XoChd(3,NaChd),popasChd(64,NaChd),Toll(NaChd),
     3             RCapture(NaChd),ireach(NaChd),rcla(3,NaChd))
      enddo
      return
      end
