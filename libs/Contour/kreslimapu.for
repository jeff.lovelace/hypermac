      subroutine KresliMapu(idal)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      idala=idal
      if(idala.ne.0) call CopyVekI(nxlast,nxdraw,4)
1000  if(idala.eq.1) then
        nxdraw(1)=nxdraw(1)+1
        if(nxdraw(1).gt.nx(3)) then
          nxdraw(1)=1
          nxdraw(2)=nxdraw(2)+1
          if(nxdraw(2).gt.nx(4)) then
            nxdraw(2)=1
            nxdraw(3)=nxdraw(3)+1
            if(nxdraw(3).gt.nx(5)) then
              nxdraw(3)=1
              nxdraw(4)=nxdraw(4)+1
              if(nxdraw(4).gt.nx(6)) then
                do i=1,4
                  nxdraw(i)=nx(i+2)
                enddo
                go to 9999
              endif
            endif
          endif
        endif
      else if(idala.eq.-1) then
        nxdraw(1)=nxdraw(1)-1
        if(nxdraw(1).lt.1) then
          nxdraw(1)=nx(3)
          nxdraw(2)=nxdraw(2)-1
          if(nxdraw(2).lt.1) then
            nxdraw(2)=nx(4)
            nxdraw(3)=nxdraw(3)-1
            if(nxdraw(3).lt.1) then
              nxdraw(3)=nx(5)
              nxdraw(4)=nxdraw(4)-1
              if(nxdraw(4).lt.1) then
                do i=1,4
                  nxdraw(i)=1
                enddo
                go to 9999
              endif
            endif
          endif
        endif
      else if(idala.ne.0) then
        go to 9999
      endif
      if(nxdraw(1).eq.0) nxdraw(1)=1
      irec=nxdraw(1)+(nxdraw(2)-1)*nx(3)+(nxdraw(3)-1)*nx(3)*nx(4)+
     1               (nxdraw(4)-1)*nx(3)*nx(4)*nx(5)
      irec=max(1,irec)
      irec=min(nmap,irec)
      if(FlagMap(irec).eq.2) then
        if(idala.eq.0) idala=1
        go to 1000
      endif
      read(m8,rec=irec+1,err=9000)(ActualMap(i),i=1,nxny)
      do i=3,NDim(KPhase)
        zmap(i-2)=xmin(i)+float(nxdraw(i-2)-1)*dx(i)
      enddo
      irecold=irec
      call ConHeader
      MapExists=.false.
      call CopyVekI(nxdraw,nxlast,4)
2000  if(reconfig) then
        call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
        reconfig=.false.
      endif
      call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      if(ConTintType.ge.1) call ConTintMap
      call FeMakeAcFrame
      if(mapa.lt.50) then
        write(t80,LabelMapy)(cx(i),zmap(i-2),i=3,NDim(KPhase))
        call zhusti(t80)
        t80(idel(t80):)=' '
        pom=min(FeTxLength(t80),abs(XCornAcWin(1,2)-XCornAcWin(1,3)))*.5
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1               (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2               pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3               (XCornAcWin(1,2)-XCornAcWin(1,3)),t80,'C',White)
      endif
      call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
      call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
      if(FlagMap(irec).eq.1) then
        call FeOutSt(0,XCenAcWin,YCenAcWin,
     1               'The map cannot be drawn due to the previous error'
     2              ,'C',Yellow)
        go to 8500
      endif
      if(DrawPos.gt.0) then
        call Vrstvy( 1)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(DrawNeg.gt.0) then
        call Vrstvy(-1)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(HardCopy.eq.0.and..not.JedeMovie) then
        call FeCleanImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                    ConImageFile,0)
        call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                   ConImageFile)
      endif
      if(AtomsOn) call ConDrawAtoms(zmap)
8500  MapExists=.true.
      go to 9999
9000  call FeReadError(m8)
      ErrFlag=1
9999  call FeHardCopy(HardCopy,'close')
      if(JedeMovie) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
      return
      end
