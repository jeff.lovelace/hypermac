      subroutine ConSetLocDensity
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pop(7),DSto(0:2)
      integer Type,PopPom
      double precision pom
      data st/.0333333/
      do icv=1,2
        do i=1,NAtFormula(KPhase)
          if(icv.eq.1) then
            call SetRealArrayTo(DenCore(1,i),240,0.)
            call SetRealArrayTo(DenCoreDer(1,1,i),240,0.)
            call SetRealArrayTo(DenCoreDer(1,2,i),240,0.)
            Type=TypeCore(i,KPhase)
            PopPom=PopCore(1,i,KPhase)
          else
            call SetRealArrayTo(DenVal(1,i),240,0.)
            call SetRealArrayTo(DenValDer(1,1,i),240,0.)
            call SetRealArrayTo(DenValDer(1,2,i),240,0.)
            Type=TypeVal(i,KPhase)
            PopPom=PopVal(1,i,KPhase)
          endif
          if(Type.eq.0) then
            fnorm=0.
            kp=0
            do l=1,7
              kp=kp+l
              k=kp
              call SetRealArrayTo(pop,6,0.)
              spop=0.
              if(icv.eq.1) then
                pom=PopCore(1,i,KPhase)
              else
                pom=PopVal(1,i,KPhase)
              endif
              if(pom.ge.0) then
                do j=l,7
                  jp=j-l+1
                  if(icv.eq.1) then
                    pop(jp)=PopCore(k,i,KPhase)
                  else
                    pop(jp)=PopVal(k,i,KPhase)
                  endif
                  spop=spop+pop(jp)
                  fnorm=fnorm+pop(jp)
                  k=k+j
                enddo
              endif
              if(spop.gt.0.) then
                if(icv.eq.1) then
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenCore(1,i),240,1,
     2                           CoreValSource(i,KPhase),ich)
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenCoreDer(1,1,i),240,2,
     2                           CoreValSource(i,KPhase),ich)
                else
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenVal(1,i),240,1,
     2                           CoreValSource(i,KPhase),ich)
                  call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),pop,
     1                           DenValDer(1,1,i),240,2,
     2                           CoreValSource(i,KPhase),ich)
                endif
              endif
            enddo
            if(fnorm.gt.0) then
              fnorm=1./fnorm
              do j=1,240
                if(icv.eq.1) then
                  DenCore(j,i)=DenCore(j,i)*fnorm
                else
                  DenVal(j,i)=DenVal(j,i)*fnorm
                endif
              enddo
              do j=1,2
                do k=1,240
                  if(icv.eq.1) then
                    DenCoreDer(k,j,i)=DenCoreDer(k,j,i)*fnorm
                  else
                    DenValDer(k,j,i)=DenValDer(k,j,i)*fnorm
                  endif
                enddo
              enddo
            endif
          else if(Type.eq.-1.or.Type.eq. 1) then
            nn=FFType(KPhase)
            ds=.1
            pom=0.
            s=0.
            do j=2,nn
              s=s+ds
              if(icv.eq.1) then
                FPom=FFCore(j,i,KPhase)
              else
                FPom=FFVal(j,i,KPhase)
              endif
              pom=pom+FPom*s**2
            enddo
            if(icv.eq.1) then
              DenCore(1,i)=pi4**2*pom*ds
            else
              DenVal(1,i)=pi4**2*pom*ds
            endif
            d=0.
            do j=2,240
              d=d+st
              pom=0.
              s=0.
              do k=2,nn
                s=s+ds
                if(icv.eq.1) then
                  FPom=FFCore(k,i,KPhase)
                else
                  FPom=FFVal(k,i,KPhase)
                endif
                pom=pom+FPom*s*sin(pi2*d*s)
              enddo
              if(icv.eq.1) then
                DenCore(j,i)=2.*pi4*pom/d*ds
              else
                DenVal(j,i)=2.*pi4*pom/d*ds
              endif
            enddo
          else if(PopPom.eq.-2) then
            n=HNSlater(i,KPhase)
            dz=HZSlater(i,KPhase)/BohrRad
            rn=dz
            do j=1,n+2
              rn=rn*dz/float(j)
            enddo
            d=0.
            do j=1,240
              call SlaterDensityDer(d,dz,n,DSto)
              if(icv.eq.1) then
                DenCore(j,i)=DSto(0)*rn
                DenCoreDer(j,1,i)=DSto(1)*rn
                DenCoreDer(j,2,i)=DSto(2)*rn
              else
                DenVal(j,i)=DSto(0)*rn
                DenValDer(j,1,i)=DSto(1)*rn
                DenValDer(j,2,i)=DSto(2)*rn
              endif
              d=d+st
            enddo
          endif
        enddo
      enddo
      return
      end
