      subroutine Contour
      use Contour_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      logical FeYesNoHeader
      character*80 Veta
      if(ChargeDensities)
     1  allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     2           DenCoreDer(240,2,MaxNAtFormula),
     3           DenValDer(240,2,MaxNAtFormula))
      n=max(NAtCalc,1000)
      MaxPDF=n
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      allocate(AtBrat(NAtCalc),AtBratAnharm(5,NAtCalc))
      call ConPrelim
      if(ErrFlag.ne.0) go to 9999
      call ConRightButtons
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
9999  call zmcmlt(1)
      call DeleteFile(ConImageFile)
      if(allocated(ActualMap)) deallocate(ActualMap)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      if(allocated(AtBrat)) deallocate(AtBrat)
      if(allocated(AtBratAnharm)) deallocate(AtBratAnharm)
      if(NewAtoms.or.ModifiedAtoms.or.NewPoints) then
        NInfo=0
        if(NewAtoms.or.NewPoints) then
          NInfo=NInfo+1
          if(NewAtoms) then
            Veta='atom(s)'
            if(NewPoints) Veta=Veta(:idel(Veta))//' and point(s)'
          else
            Veta='point(s)'
          endif
          TextInfo(NInfo)='New '//Veta(:idel(Veta))//' were localized.'
        endif
        if(ModifiedAtoms) then
          NInfo=NInfo+1
          TextInfo(NInfo)='Some atomic positions were modified.'
        endif
        if(FeYesNoHeader(-1.,-1.,'Do you really want to accept '//
     1                   'those changes?',1))
     2    call iom40only(1,0,fln(:ifln)//'.m40')
      endif
      call DeleteFile(PreviousM40)
      do i=81,86
        call CloseIfOpened(i)
      enddo
      call DeleteAllFiles(fln(:ifln)//'.l8?')
      if(allocated(ContourArray)) deallocate(ContourArray)
      return
      end
