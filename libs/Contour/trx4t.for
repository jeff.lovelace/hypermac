      subroutine Trx4t
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension tmin(3),tmax(3),dt(3)
      data tmin/3*0./,tmax/3*1./,dt/3*.1/
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)-2,
     1                   'Define intervals for t maps',0,LightGray,0,0)
      tpom=10.
      dpom=80.
      spom=dpom+10.
      do i=1,NDimI(KPhase)
        xpom=tpom+PropFontHeightInPixels+10.
        il=i+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,smbt(i),'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,tmin(i),.false.,.false.)
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          call FeQuestLblMake(id,xpom+spom*.5,il-1,'Min','C','N')
        endif
        xpom=xpom+spom
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        if(i.eq.1)
     1    call FeQuestLblMake(id,xpom+spom*.5,il-1,'Min','C','N')
        call FeQuestRealEdwOpen(EdwLastMade,tmax(i),.false.,.false.)
        xpom=xpom+spom
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        if(i.eq.1)
     1    call FeQuestLblMake(id,xpom+spom*.5,1,'Step','C','N')
        call FeQuestRealEdwOpen(EdwLastMade,dx(i+3),.false.,.false.)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        j=nEdwFirst
        do i=1,NDimI(KPhase)
          call FeQuestRealFromEdw(j,tmin(i))
          j=j+1
          call FeQuestRealFromEdw(j,tmax(i))
          j=j+1
          call FeQuestRealFromEdw(j,dt(i))
          j=j+1
        enddo
        call FeQuestRemove(id)
        call rezt(tmin,tmax,dt)
        if(ErrFlag.eq.0) tmapy=.true.
        go to 9999
      endif
      ErrFlag=1
      call FeQuestRemove(id)
9999  return
      end
