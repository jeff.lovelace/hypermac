      subroutine CPReadScope(Klic,CPXMin,CPXMax,CPXSt,DensityType,
     1                       CutOffDist,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension CPXMin(3),CPXMax(3),CPXSt(3),xp(6)
      integer :: CPIndUnit=0,DensityType
      real :: CPStep=.1
      ich=0
      call DefaultFourier
      OrCode=-333
      call SetRealArrayTo(xrmn,6,-333.)
      call FouDefLimits
      call NactiFourier
      call FouReadScopeCP(Klic,CPIndUnit,CPStep,DensityType,CutOffDist,
     1                    ich)
      if(ich.ne.0) go to 9999
      if(ScopeType.eq.1) then
        do j=1,3
          if(CPIndUnit.eq.1) then
            CPXMin(j)=fourmn(j,nsubs)
            CPXMax(j)=fourmx(j,nsubs)
          else
            CPXMin(j)=0.
            CPXMax(j)=1.
          endif
          n=max(nint((CPXMax(j)-CPXMin(j))*
     1          CellPar(j,nsubs,KPhase)/CPStep),1)
          CPXSt(j)=(CPXMax(j)-CPXMin(j))/float(n)
        enddo
      else if(ScopeType.eq.2) then
        CPIndUnit=0
        call CopyVek(xrmn,CPXMin,3)
        call CopyVek(xrmx,CPXMax,3)
        call CopyVek(dd  ,CPXSt ,3)
      else if(ScopeType.eq.3) then
        CPIndUnit=0
        if(ptname.ne.'[neco]') then
          call atsym(ptname,i,ptx,xp,xp(4),j,k)
          if(i.le.0) then
            call FeChybne(-1.,-1.,'atom '//ptname(:idel(ptname))//
     1                    ' isn''t on the '//'file M40',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined',SeriousError)
          endif
          if(k.eq.3) then
             call FeChybne(-1.,-1.,'atom '//ptname(:idel(ptname))//
     1                     ' isn''t correct',
     2                     'The part of the volume to be mapped '//
     3                     'isn''t defined',SeriousError)
          endif
          if(ErrFlag.ne.0) go to 9000
        endif
        do j=1,3
          nd=nint(.5*pts(j)/ptstep)
          if(nd.ne.0) then
            CPXSt(j)=ptstep/CellPar(j,nsubs,KPhase)
          else
            CPXSt(j)=1.
          endif
          CPXMin(j)=ptx(j)-CPXSt(j)*float(nd)
          CPXMax(j)=ptx(j)+CPXSt(j)*float(nd)
        enddo
      endif
      if(CPIndUnit.eq.1) then
        do j=1,3
          CPXMin(j)=CPXMin(j)-CPXSt(j)
          CPXMax(j)=CPXMax(j)+CPXSt(j)
        enddo
      endif
      do i=1,6
        iorien(i)=i
      enddo
      go to 9999
9000  ich=1
      ErrFlag=0
9999  call DefaultContour
      call NactiContour
      return
      end
