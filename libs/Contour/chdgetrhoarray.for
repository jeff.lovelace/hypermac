      subroutine ChdGetRhoArray(RhoArr,xyz,n,IType)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension RhoArr(*),xyz(*),xf(3)
      is=1
      nsubs=1
      ivs=1
      if(IType.eq.0) then
        ivd=1
      else if(iabs(IType).eq.1) then
        ivd=4
      else if(iabs(IType).eq.2) then
        ivd=10
      endif
      do i=1,n
        call multm(TrToOrthoI,xyz(is),xf,3,3,1)
        call ChdGetNeighbours(xf,CutOffDist)
        call ChdGetRhoAndDRho(xf,IType,RhoArr(ivs),ich)
        is=is+3
        ivs=ivs+ivd
      enddo
      return
      end
