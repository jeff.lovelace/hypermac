      subroutine CPGetRhoAndDRho(xf,Type,RhoA,ich)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      integer Type
      dimension xf(3),px(10),xp(3),zz(8),nn(8),fnorm(8),popasi(64),
     1          RhoA(*),RhoD(10),DenDer(0:2),DenDerXYZ(10),STOA(10),
     2          DSto(0:2),SpX(64),SpXDer1(3,64),SpXDer2(6,64),RPop(680),
     3          FractToLocal(9),OrthoToLocal(9)
      equivalence (xp,px)
      data st/.0333333/
      ich=0
      if(Type.eq.0) then
        nRhoA=1
      else if(Type.eq.1) then
        nRhoA=4
      else
        nRhoA=10
      endif
      call SetRealArrayTo(RhoA,nRhoA,0.)
      do iat=1,npdf
        ip=ipor(iat)
        if(fpdf(ip).le.0.) cycle
        do i=1,3
          xp(i)=xf(i)-xpdf(i,ip)
        enddo
        call MultM(TrToOrtho,xp,xo,3,3,1)
        dd=VecOrtLen(xo,3)
        if(dd.gt.CutOffDist) cycle
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isw=iswa(ia)
        isfi=isf(ia)
        if(ispdf(ip).eq.0) ispdf(ip)=1
        ISym=ispdf(ip)
        ISymAbs=abs(ISym)
        if(IsymAbs.eq.1) then
          call CopyMat(TrAt(1,ia),FractToLocal,3)
        else
          call MatInv(rm(1,ISymAbs,isw,KPhase),px,pom,3)
          call MultM(TrAt(1,ia),px,FractToLocal,3,3,3)
        endif
        if(ISym.lt.0) then
          do i=1,9
            FractToLocal(i)=-FractToLocal(i)
          enddo
        endif
        call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
        call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
        jp=2
        kp=2
        popasi(1)=popas(1,ia)
        do l=1,lasmaxi-2
          n=2*l+1
          call multm(RPop(kp),popas(jp,ia),popasi(jp),n,n,1)
          kp=kp+n**2
          jp=jp+n
        enddo
        kapa1i=kapa1(ia)
        kapa2i=kapa2(ia)
        PopValDeform=kapa1i**3*popv(ia)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
        if(Type.gt.0) then
          DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
          if(Type.gt.1)
     1      DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
          call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,RhoD)
        else
          RhoD(1)=DenDer(0)
        endif
        DenDer(0)=PopValDeform*
     1    Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
        if(Type.gt.0) then
          PopValDeform=PopValDeform*kapa1i
          DenDer(1)=PopValDeform*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          if(Type.gt.1) then
            PopValDeform=PopValDeform*kapa1i
            DenDer(2)=PopValDeform*
     1        Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          endif
        endif
        call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,Type,DenDerXYZ)
        call AddVek(RhoD,DenDerXYZ,RhoD,nRhoA)
        call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,Type,3)
        k=0
        do l=1,lasmaxi-1
          call SlaterDensityDer(dd,zz(l),nn(l),DSto)
          call RadialDer(xo(1),xo(2),xo(3),dd,DSto,Type,STOA)
          STO=DSto(0)
          pom=0.
          call SetRealArrayTo(px,nRhoA,0.)
          do n=1,2*l-1
            k=k+1
            popask=popasi(k)
            SpXk=SpX(k)
            pom=pom+popask*SpXk
            if(Type.gt.0) then
              do m=2,4
                i=m-1
                px(m)=px(m)+popask*(SpXk*STOA(m)+SpXDer1(i,k)*STO)
              enddo
              if(Type.gt.1) then
                do m=5,10
                  mm=m-4
                  call indext(mm,i,j)
                  if(i.eq.j) then
                    p=SpXk*STOA(m)+2.*SpXDer1(i,k)*STOA(i+1)+
     1                SpXDer2(mm,k)*STO
                  else
                    p=SpXk*STOA(m)+SpXDer1(i,k)*STOA(j+1)+
     1                SpXDer1(j,k)*STOA(i+1)+SpXDer2(mm,k)*STO
                  endif
                  px(m)=px(m)+popask*p
                enddo
              endif
            endif
          enddo
          RhoD(1)=RhoD(1)+pom*fnorm(l)*STO
          do i=2,nRhoA
            RhoD(i)=RhoD(i)+px(i)*fnorm(l)
          enddo
        enddo
        pom=fpdf(ip)
        do i=1,nRhoA
          RhoD(i)=RhoD(i)*pom
        enddo
        call AddVek(RhoA,RhoD,RhoA,nRhoA)
      enddo
      return
      end
