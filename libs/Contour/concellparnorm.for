      subroutine ConCellParNorm(CellParP)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension CellParP(*)
      m=0
      pom1=0.
      m1=0
      pom2=0.
      m2=0
      do i=1,NDim(KPhase)
        if(nx(i).le.1) cycle
        m=m+1
        j=iorien(i)
        if(j.le.3) then
          pom1=pom1+CellParP(m)
          m1=m1+1
        else
          pom2=pom2+CellParP(m)
          m2=m2+1
        endif
        if(m.ge.3) exit
      enddo
      if(m1.gt.0.and.m2.gt.0) then
        pom=(pom1*float(m2))/(pom2*float(m1))
        m=0
        do i=1,NDim(KPhase)
          if(nx(i).le.1) cycle
          m=m+1
          if(iorien(i).gt.3)
     1      CellParP(m)=CellParP(m)*pom
          if(m.ge.3) exit
        enddo
      endif
      return
      end
