      subroutine GoToMap(ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*5 t5
      if(kpdf.eq.2.and.NDim(KPhase).gt.3) then
        j=1
      else
        j=0
      endif
      l=NDim(KPhase)-2+j
      id=NextQuestId()
      xqd=180.
      call FeQuestCreate(id,-1.,-1.,xqd,l,'Next map to be drawn',0,
     1                   LightGray,0,0)
      tpom=5.
      dpom=100.
      do i=1,NDim(KPhase)-2
        t5=cx(i+2)//' ='
        if(t5(2:2).eq.' ') then
          t5='%'//t5(1:1)//t5(3:4)
        else
          t5=t5(1:1)//'%'//t5(2:4)
        endif
        if(i.eq.1) xpom=tpom+FeTxLengthUnder(t5)+10.
        call FeQuestEudMake(id,tpom,i,xpom,i,t5,'L',dpom,EdwYd,0)
        pom=xmin(i+2)+(nxlast(i)-1)*dx(i+2)
        call FeQuestRealEdwOpen(i,pom,.false.,.false.)
        call FeQuestEudOpen(i,1,1,1,xmin(i+2),xmax(i+2),dx(i+2))
      enddo
      irec=nint((EdwRealQuest(1,1)-xmin(3))/dx(3))+1+
     1     nint((EdwRealQuest(1,2)-xmin(4))/dx(4))*nx(3)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NDim(KPhase)-2
          call FeQuestRealFromEdw(i,pom)
          if(dx(i+2).ne.0.) then
            nxdraw(i)=nint((pom-xmin(i+2))/dx(i+2))+1
          else
            nxdraw(i)=1
          endif
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
