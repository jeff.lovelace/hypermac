      subroutine ConTintMap
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer FeRGBCompress
      logical InsideXYPlot
      dimension xp(3),xd(3)
      xd(3)=0.
      xp(1)=xmin(1)
      xp(2)=xmin(2)
      xp(3)=0.
      call FeXf2Pixels(xp,ix,iy)
      ixMin=ix
      iyMin=iy
      ixMax=ix
      iyMax=iy
      xp(2)=xmax(2)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      xp(1)=xmax(1)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      xp(2)=xmin(2)
      call FeXf2Pixels(xp,ix,iy)
      ixMin=min(ix,ixMin)
      iyMin=min(iy,iyMin)
      ixMax=max(ix,ixMax)
      iyMax=max(iy,iyMax)
      call FeRGBUncompress(ConPosCol,ir,ig,ib)
      RedPos=ir
      GreenPos=ig
      BluePos=ib
      call FeRGBUncompress(ConNegCol,ir,ig,ib)
      RedNeg=ir
      GreenNeg=ig
      BlueNeg=ib
      do ix=ixMin,ixMax
        do iy=iyMin,iyMax
          call FePixels2Xf(ix,iy,xp)
          call FeXf2X(xp,xo)
          if(.not.InsideXYPlot(xo(1),xo(2))) cycle
          xp(1)=(xp(1)-xmin(1))/dx(1)
          xp(2)=(xp(2)-xmin(2))/dx(2)
          i1=min(ifix(xp(1))+1,nx(1))
          i1=max(i1,1)
          i2=min(ifix(xp(2))+1,nx(2))
          i2=max(i2,1)
          xd(1)=xp(1)-float(i1-1)
          xd(2)=xp(2)-float(i2-1)
          i1p=min(i1+1,nx(1))
          i2p=min(i2+1,nx(2))
          ip=i1+(i2-1)*nx(1)
          f000=ActualMap(ip)
          ip=i1p+(i2-1)*nx(1)
          f100=ActualMap(ip)
          ip=i1+(i2p-1)*nx(1)
          f010=ActualMap(ip)
          ip=i1p+(i2p-1)*nx(1)
          f110=ActualMap(ip)
          pom=ExtrapolLin(xd,2,f000,f100,f010,f110,0.,0.,0.,0.)
          cf=abs(pom)/max(DMax,-DMin)
          if(pom.gt.0.) then
            if(InvertWhiteBlack.and.HardCopy.ne.0) then
              ir=nint((RedPos-255.)*cf)+255
              ig=nint((GreenPos-255.)*cf)+255
              ib=nint((BluePos-255.)*cf)+255
            else
              ir=nint(RedPos*cf)
              ig=nint(GreenPos*cf)
              ib=nint(BluePos*cf)
            endif
          else
            if(InvertWhiteBlack.and.HardCopy.ne.0) then
              ir=nint((RedNeg-255)*cf)+255
              ig=nint((GreenNeg-255)*cf)+255
              ib=nint((BlueNeg-255)*cf)+255
            else
              ir=nint(RedNeg*cf)
              ig=nint(GreenNeg*cf)
              ib=nint(BlueNeg*cf)
            endif
          endif
          if(ir.le.0.and.ig.le.0.and.ib.le.0) cycle
          call FePixelPoint(ix,iy,FeRGBCompress(ir,ig,ib))
        enddo
      enddo
      return
      end
