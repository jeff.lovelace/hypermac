      subroutine ConOptions
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension XMinColor(2),XMaxColor(2),YMinColor(2),YMaxColor(2),
     1          nLblColor(2),nEdwRGB(2),ip(3,2)
      character*80 Veta
      integer EdwStateQuest,FeRGBCompress
      logical CrwLogicQuest,ExistFile
      id=NextQuestId()
      xqd=450.
      il=14
      if(NDimI(KPhase).gt.0) il=il+3
      if(Mapa.eq.6) il=il+1
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,'General options','C','B')
      il=il+1
      Veta='Maximal %distance to locate atoms'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,LocDMax,.false.,.false.)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Drawing options','C','B')
      il=il+1
      Veta='%Ball-and-stick'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          tpom=tpom+xqd*.5
          Veta='%Wires'
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          nCrwBallAndStick=CrwLastMade
          j=DrawStyleBallAndStick
        else if(i.eq.2) then
          nCrwWires=CrwLastMade
          j=DrawStyleWires
        endif
        call FeQuestCrwOpen(CrwLastMade,ConDrawStyle.eq.j)
      enddo
      ilp=-10*il-15
      Veta='d(max) derived from atomic %radii'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,1,2)
      nCrwAtRad=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConDmaxExpl.le.0)
      tpomp=tpom+FeTxLengthUnder(Veta)+15.
      Veta='e%xpanded by'
      xpomp=tpomp+FeTxLengthUnder(Veta)+5.
      dpom=50.
      ilp=ilp-3
      call FeQuestEdwMake(id,tpomp,ilp,xpomp,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwExpand=EdwLastMade
      call FeQuestRealEdwOpen(nEdwExpand,ConExpDMax,.false.,.false.)
      tpomp=xpomp+dpom+5.
      call FeQuestLblMake(id,tpomp,ilp,'%','L','N')
      nLblExpand=LblLastMade
      ilp=ilp-2
      Veta='and typical distances'
      call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
      ilp=ilp-7
      Veta='d(max) as defined for individual atoms in "Atom edit"'
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,1,2)
      nCrwExplicit=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConDmaxExpl.gt.0)
      il=il+4
      xpom=5.
      Veta='Do not draw bonds between atoms of %identical atomic types'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwConSkipSame=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConSkipSame.gt.0)
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        Veta='Use occupancy %cutoff'
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,3)
          if(i.eq.1) then
            nCrwOcc=CrwLastMade
            tpomp=xpom+FeTxLengthUnder(Veta)+5.
            Veta='=>'
            xpomp=tpomp+FeTxLengthUnder(Veta)+5.
            call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,
     1                          EdwYd,0)
            nEdwCutOff=EdwLastMade
            Veta='Reduce radius of drawn atom by actual occupancy'
          endif
          call FeQuestCrwOpen(CrwLastMade,
     1                         i.eq.1.eqv.UseOccupancyCutOff)
        enddo
      else
        nEdwCutOff=0.
        nCrwOcc=0.
      endif
      tpom=xpom+CrwXd+10.
      if(Mapa.eq.6) then
        il=il+1
        Veta='Draw %critical points'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,ConDrawCP.gt.0)
        if(.not.ExistFile(fln(:ifln)//'.cp'))
     1    call FeQuestCrwDisable(CrwLastMade)
        nCrwDrawCP=CrwLastMade
      else
        nCrwDrawCP=0
      endif
      Veta='%Project all defined atoms'
      if(Mapa.eq.6.and.ExistFile(fln(:ifln)//'.cp'))
     1  Veta=Veta(:idel(Veta))//'/critical points'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,4)
        call FeQuestCrwOpen(CrwLastMade,ConDrawDepth.le.0.eqv.i.eq.1)
        if(i.eq.1) then
          nCrwAllProj=CrwLastMade
          Veta='Project %only atoms'
          if(Mapa.eq.6.and.ExistFile(fln(:ifln)//'.cp'))
     1      Veta=Veta(:idel(Veta))//'/critical points'
          Veta=Veta(:idel(Veta))//' having distance form the section <'
        else if(i.eq.2) then
          nCrwDrawDepth=CrwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      Veta='Ang'
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDrawDepth=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,1.,.false.,.false.)
      if(ConDrawDepth.le.0.) then
        call FeQuestEdwDisable(EdwLastMade)
        Depth=1.
      else
        Depth=ConDrawDepth
      endif
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Contours options','C','B')
      il=il+1
      xpom=5.
      Veta='Active %tinting'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwConTintType=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ConTintType.gt.0)
      Veta='Drawing color for positive:'
      tpom=5.
      dpom=40.
      xpom=tpom+FeTxLengthUnder(Veta)+dpom+45.
      do j=1,2
        il=il+1
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblColor(j)=LblLastMade
        Veta='R:'
        xpomp=xpom
        do i=1,3
          tpomp=xpomp-15.
          call FeQuestEudMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,
     1                        1)
          xpomp=xpomp+80.
          if(i.eq.1) then
            nEdwRGB(j)=EdwLastMade
            Veta='G:'
          else if(i.eq.2) then
            Veta='B:'
          endif
        enddo
        XMinColor(j)=EdwXMinQuest(nEdwRGB(j)+2)-230.
        XMaxColor(j)=XMinColor(j)+dpom
        YMinColor(j)=EdwYMinQuest(nEdwRGB(j)+2)
        YMaxColor(j)=EdwYMaxQuest(nEdwRGB(j)+2)
        Veta='Drawing color for negative:'
      enddo
      do j=1,2
        nEdw=nEdwRGB(j)
        if(j.eq.1) then
          ic=ConPosCol
        else
          ic=ConNegCol
        endif
        call FeRGBUncompress(ic,ip(1,j),ip(2,j),ip(3,j))
        do i=1,3
          call FeQuestIntEdwOpen(nEdw,ip(i,j),.false.)
          call FeQuestEudOpen(nEdw,0,255,1,0.,0.,0.)
          nEdw=nEdw+1
        enddo
        call FeDrawFrame(XminColor(j),YminColor(j),
     1                   XmaxColor(j)-XminColor(j),
     2                   YmaxColor(j)-YminColor(j),
     3                   2.,White,Gray,Black,.false.)
        call FeFillRectangle(XminColor(j),XmaxColor(j),YminColor(j),
     1                       YmaxColor(j),4,0,0,ic)
      enddo
1400  if(nCrwOcc.gt.0) then
        if(UseOccupancyCutOff) then
          call FeQuestRealEdwOpen(nEdwCutOff,ConOccLim,.false.,.false.)
        else
          if(EdwStateQuest(nEdwCutOff).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdwCutOff,ConOccLim)
          call FeQuestEdwClose(nEdwCutOff)
        endif
      endif
      if(CrwLogicQuest(nCrwExplicit)) then
        call FeQuestRealFromEdw(nEdwExpand,ConDmaxExpl)
        call FeQuestEdwDisable(nEdwExpand)
        call FeQuestLblDisable(nLblExpand)
      else
        call FeQuestRealEdwOpen(nEdwExpand,ConExpDMax,.false.,.false.)
        call FeQuestLblOn(nLblExpand)
      endif
1450  if(CrwLogicQuest(nCrwDrawDepth)) then
        call FeQuestRealEdwOpen(nEdwDrawDepth,Depth,.false.,.false.)
      else
        call FeQuestRealFromEdw(nEdwDrawDepth,Depth)
        call FeQuestEdwDisable(nEdwDrawDepth)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.nCrwOcc.gt.0.and.
     1   (CheckNumber.eq.nCrwOcc.or.CheckNumber.eq.nCrwOcc+1)) then
        UseOccupancyCutOff=CrwLogicQuest(nCrwOcc)
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwAtRad.or.CheckNumber.eq.nCrwExplicit)) then
        go to 1400
      else if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwAllProj.or.
     1        CheckNumber.eq.nCrwDrawDepth)) then
        go to 1450
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.ge.nEdwRGB(1).and.
     2        CheckNumber.le.nEdwRGB(1)+5) then
        i=CheckNumber-nEdwRGB(1)+1
        if(i.gt.3) then
          i=i-3
          j=2
        else
          j=1
        endif
        call FeQuestIntFromEdw(CheckNumber,ip(i,j))
        ic=FeRGBCompress(ip(1,j),ip(2,j),ip(3,j))
        if(j.eq.1) then
          NacetlInt(nCmdConPosCol)=ic
        else
          NacetlInt(nCmdConNegCol)=ic
        endif
        call FeFillRectangle(XminColor(j),XmaxColor(j),YminColor(j),
     1                       YmaxColor(j),4,0,0,ic)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwDMax,LocDMax)
        if(NDimI(KPhase).gt.0.and.UseOccupancyCutOff)
     1    call FeQuestRealFromEdw(nEdwCutOff,ConOccLim)
        if(CrwLogicQuest(nCrwBallAndStick)) then
          NacetlInt(nCmdConDrawStyle)=DrawStyleBallAndStick
        else
          NacetlInt(nCmdConDrawStyle)=DrawStyleWires
        endif
        if(CrwLogicQuest(nCrwConSkipSame)) then
          NacetlInt(nCmdConSkipSame)=1
        else
          NacetlInt(nCmdConSkipSame)=0
        endif
        if(CrwLogicQuest(nCrwExplicit)) then
          NacetlInt(nCmdConDmaxExpl)=1
        else
          NacetlInt(nCmdConDmaxExpl)=0
          call FeQuestRealFromEdw(nEdwExpand,
     1                            NacetlReal(nCmdConExpDMax))
        endif
        if(CrwLogicQuest(nCrwConTintType)) then
          NacetlInt(nCmdConTintType)=1
        else
          NacetlInt(nCmdConTintType)=0
        endif
        if(CrwLogicQuest(nCrwDrawDepth)) then
          call FeQuestRealFromEdw(nEdwDrawDepth,
     1                            NacetlReal(nCmdDrawDepth))
        else
          NacetlReal(nCmdDrawDepth)=0.
        endif
        if(CrwLogicQuest(nCrwDrawCP)) then
          NacetlInt(nCmdConDrawCP)=1
        else
          NacetlInt(nCmdConDrawCP)=0
        endif
      endif
      call FeQuestRemove(id)
      i=IPlane
      IPlane=-2
      call ConWriteKeys
      IPlane=i
      call DefaultContour
      call NactiContour
      call ConReadKeys
      return
      end
