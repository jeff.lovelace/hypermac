      integer function ChdCapturedBy(xn)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xn(3),dd(3),ddr(3)
      ChdCapturedBy=0
      do i=1,NaChd
        ii=IaChd(i)
        do m=1,3
          dd(m)=XfChd(m,i)-xn(m)
          dd(m)=dd(m)-anint(dd(m))
        enddo
        call multm(MetTens(1,1,KPhase),dd,ddr,3,3,1)
        dist=scalmul(dd,ddr)
        if(dist.gt.RCapture(ii)**2) cycle
        ChdCapturedBy=i
        go to 9999
      enddo
9999  return
      end
