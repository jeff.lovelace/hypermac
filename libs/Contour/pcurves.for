      subroutine PCurves(klic)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xdo(3),xfract(3),xp(3,10),xd(3,9),xfd(3,4),u(3),ug(3),
     1          dlk(10),v(3),vg(3),w(3),
     2          gx(:,:),gy(:,:),pty(:,:),sgymc(:,:),spty(:,:),dpty(:,:),
     3          ErrMap(:)
      character*256 t256,EdwStringQuest
      character*80  SvFile,Veta
      character*25  men1(5)
      character*15  Hustota
      character*8   NewAtName
      character*7   FormatLocal
      character*2   nty
      integer :: FeMenu,Exponent10,NExtPoints=100
      logical IncDer,IncErr,JeUvnitr,JesteSeNepsalo,CrwLogicQuest,
     1        EqIgCase,ByloCrw,ExistFile
      allocatable gx,gy,pty,sgymc,spty,dpty,ErrMap
      equivalence (xd(1,2),xfd),(xfd(1,3),u)
      data men1/'Change %y limits',
     1          'Draw %derivation curve',
     2          'Draw %error curve',
     3          'Remove %derivation curve',
     4          'Remove %error curve'/
      data Angle/90./,isfp/1/,biso/3./
      Tiskne=.false.
      ByloCrw=.false.
      if(ErrMapActive) then
        allocate(ErrMap(NActualMap),stat=i)
        if(i.ne.0) then
          call FeAllocErr('Error occur in "PCurves" point#1.',i)
          go to 9000
        endif
        read(83,rec=irec+1)(ErrMap(i),i=1,NActualMap)
      endif
      call FeMakeAcWin(80.,20.,30.,30.)
      HardCopy=0
      xhld=XMinBasWin+80.
      IncDer=.false.
      IncErr=.false.
      JesteSeNepsalo=.true.
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile)
      if(klic.eq.0) then
        if(NCurve.le.0) XTypeCurve=1
        id=NextQuestId()
        Veta='.... point/atom'
        dpom=200.
        xdq=FeTxLengthUnder(Veta)+dpom+20.
        il=15
        call FeQuestCreate(id,-1.,-1.,xdq,il,'Define polyline',0,
     1                     LightGray,0,0)
        il=1
        xpom=FeTxLengthUnder(Veta)+10.
        tpom=xpom+CrwgXd+10.
        t256='%Local coordinates'
        call FeQuestCrwMake(id,tpom,il,xpom,il,t256,'L',CrwgXd,CrwgYd,1,
     1                      1)
        nCrwLocal=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,XTypeCurve.eq.1)
        il=il+1
        t256='%Fractional coordinates'
        call FeQuestCrwMake(id,tpom,il,xpom,il,t256,'L',CrwgXd,CrwgYd,1,
     1                      1)
        nCrwFract=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,XTypeCurve.eq.2)
        tpom=5.
        xpom=FeTxLengthUnder(Veta)+15.
        do i=1,10
          il=il+1
          write(Veta(1:4),'(i2,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          if(i.eq.1) nEdwFirstPoint=EdwLastMade
          if(i.le.NCurve) then
            call ConSetPointCurve(AtCurve(i),i,ich)
            if(ich.ne.0) AtCurve(i)=' '
          endif
        enddo
        il=il+1
        Veta='%Clear all points'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xdq-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtClear=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Use %mouse to define polyline'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xdq-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtUseMouse=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='%Number of extrapolation points in each interval:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestIntEdwOpen(EdwLastMade,NExtPoints,.false.)
        nEdwExtPoints=EdwLastMade
1110    nEdw=nEdwFirstPoint
        do i=1,10
          if(i.le.NCurve) then
            if(AtCurve(i).eq.' ') then
              if(XTypeCurve.eq.1) then
                write(Veta,100)(XLocCurve(j,i),j=1,3)
              else
                write(Veta,101)(XFractCurve(j,i),j=1,3)
              endif
              call ZdrcniCisla(Veta,3)
            else
              Veta=AtCurve(i)
            endif
          else
            Veta=' '
          endif
          call FeQuestStringEdwOpen(nEdw,Veta)
          nEdw=nEdw+1
        enddo
1150    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtUseMouse)
     1    then
          NCurve=0
          ich=0
          go to 1300
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtClear)
     1    then
          NCurve=0
          go to 1110
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocal) then
          XTypeCurve=1
          go to 1110
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFract) then
          XTypeCurve=2
          go to 1110
        else if(CheckType.eq.EventEdw) then
          i=CheckNumber-nEdwFirstPoint+1
          t256=EdwStringQuest(CheckNumber)
          if(t256.eq.' ') go to 1150
          call ConSetPointCurve(t256,i,ichp)
          if(XTypeCurve.eq.1) then
            write(Veta,100)(XLocCurve(j,i),j=1,3)
          else
            write(Veta,101)(XFractCurve(j,i),j=1,3)
          endif
          call ZdrcniCisla(Veta,3)
          if(.not.EqIgCase(t256,Veta)) then
            call FeChybne(-1.,-1.,'the last point has been modified '//
     1                    'to be in the scope.',' ',Warning)
            call FeQuestStringEdwOpen(CheckNumber,Veta)
            EventType=EventEdw
            EventNumber=CheckNumber
          endif
          if(ichp.gt.0) then
            call FeQuestStringEdwOpen(CheckNumber,' ')
            EventType=EventEdw
            EventNumber=CheckNumber
          endif
          go to 1150
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        nEdw=nEdwFirstPoint
        NCurve=0
        do i=1,10
          if(EdwStringQuest(nEdw).ne.' ') NCurve=NCurve+1
          nEdw=nEdw+1
        enddo
1300    if(ich.eq.0) call FeQuestIntFromEdw(nEdwExtPoints,NExtPoints)
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9000
        if(NCurve.gt.1) go to 3111
      endif
      ip=0
      i=max(4-Exponent10(max(xmax(1),xmax(2))),0)
      i=min(i,4)
      write(FormatLocal,'(''(2f7.'',i1,'')'')') i
      if(LocatorType.eq.LocatorPosition) then
        MouseType=1
      else
        MouseType=0
      endif
      ireco=irec
2000  if(Klic.eq.0) then
        Xpos=(XminGrWin+XmaxGrWin)*.5
        Ypos=(YminGrWin+YmaxGrWin)*.5
        call GetCoord(xdo,xp,xfract)
        call FeMoveMouseTo(Xpos,Ypos)
        call FeMouseShape(0)
        JeUvnitr=.true.
        call FeHeaderInfo(' ')
        irec=0
      else
        call GetCoord(xdo,xp,xfract)
        Xposp=FeXo2X(xdo(1))
        Yposp=FeYo2Y(xdo(2))
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          call FeMouseShape(0)
          call ConHeader
          JeUvnitr=.false.
        else
          call FeMouseShape(MouseType)
          call FeHeaderInfo(' ')
          irec=0
          JeUvnitr=.true.
        endif
      endif
      if(JeUvnitr) call ConLocatorInfoOpen(xp,xfract)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
2010  call FeQuestEvent(ContourQuest,ich)
2020  if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeLeftDown) then
        if(klic.eq.0) then
          if(ip.eq.2) then
            call FePlotMode('E')
            call FePolyLine(2,xu,yu,Green)
            xu(ip)=Xpos
            yu(ip)=Ypos
            call FePlotMode('N')
            call FeArrow(xu(1),yu(1),xu(2),yu(2),LightYellow,ArrowTo)
            xu(1)=xu(2)
            yu(1)=yu(2)
            ip=1
          else
            ip=ip+1
            xu(ip)=Xpos
            yu(ip)=Ypos
          endif
          NCurve=NCurve+1
          XLocCurve(1,NCurve)=FeX2Xo(Xpos)
          XLocCurve(2,NCurve)=FeY2Yo(Ypos)
          XLocCurve(3,NCurve)=zmap(1)
          if(NCurve.ge.10) then
            call FeChybne(-1.,-1.,'the maximum of 10 lines reached.',
     1                    ' ',SeriousError)
            go to 3100
          endif
        else if(JeUvnitr) then
          if(LocatorType.eq.LocatorPosition) then
            XposP=Xpos
            YposP=Ypos
            call FeMouseShape(0)
            call GetCoord(xdo,xp,xfract)
            write(Hustota,103)
     1        ExtMap(xp(1,1),xp(2,1),ActualMap,NActualMap)+
     2        ContourRefLevel
            call Zhusti(Hustota)
            npdf=0
            if(NDim(KPhase).eq.3.or.xyzmap) then
              call OkoliC(xfract,LocDMax,0,NSubs,.false.)
              npdf=min(npdf,19)
              do i=1,npdf
                k=ipor(i)
                write(Cislo,'(f6.3)') dpdf(k)
                TextInfo(i+1)=Tabulator//
     1                        atom(iapdf(k))(:idel(atom(iapdf(k))))//
     2                        Tabulator//'|'//Tabulator//Cislo//
     3                        Tabulator//'|'//
     4                        Tabulator//scpdf(k)(:idel(scpdf(k)))
              enddo
              write(Veta,'(''Fractional coordinates : '',3f7.4)')
     1                    xfract
              Veta=Veta(:idel(Veta))//', density : '//
     1            Hustota(:idel(Hustota))
              if(npdf.gt.0) then
                Ninfo=npdf+1
                TextInfo(1)='draw'//Tabulator//'atom'//
     1                      Tabulator//'distance'//Tabulator//'symmetry'
              else
                Ninfo=1
                TextInfo(1)='No distances shorter than the limit'
              endif
            else
              TextInfo(1)='Local density : '//Hustota
              write(Cislo,FormatLocal)(xp(l,1),l=1,2)
              Veta='Local coordinates : '//Cislo
              NInfo=1
            endif
            id=NextQuestId()
            xqd=0.
            do i=1,Ninfo
              xqd=max(FeTxLength(TextInfo(i)),xqd)
              if(i.eq.1) call FeNormalFont
            enddo
            call FeBoldFont
            xqd=max(FeTxLength(Veta),xqd)+10.
            call FeNormalFont
            if(NDim(KPhase).eq.3.or.xyzmap) then
              yqd=120.+float(Ninfo)*20.
            else
              yqd=70.+float(Ninfo)*20.
            endif
            call FeQuestAbsCreate(id,-1.,-1.,xqd,yqd,' ',0,LightGray,
     1                            0,0)
            ypom=yqd-15.
            call FeQuestAbsLblMake(id,xqd*.5,ypom,Veta,'C','B')
            ypom=yqd-40.
            call FeTabsReset(0)
            call FeTabsReset(1)
            pom=40.
            call FeTabsAdd(pom,0,IdRightTab,' ')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            pom=pom+FeTxLength('XXXXXXXX |')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            pom=pom+FeTxLength('MM')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            pom=pom+FeTxLength('XX.XXX |')
            call FeTabsAdd(pom,0,IdRightTab,' ')
            pom=pom+FeTxLength('MM')
            call FeTabsAdd(pom,1,IdRightTab,' ')
            xpom=12.
            nCrwFirst=0
            call FeTabsAdd(pom,0,IdRightTab,' ')
            do i=1,Ninfo
              if(i.eq.1) then
                UseTabs=1
              else
                UseTabs=0
              endif
              call FeQuestAbsLblMake(id,5.,ypom,TextInfo(i),'L','N')
              if(i.gt.1) then
                call FeQuestAbsCrwMake(id,xpom,ypom,xpom,ypom-5.,' ',
     1                                 'L',CrwXd,CrwYd,0,0)
                if(i.eq.2) nCrwFirst=CrwLastMade
                k=0
                call kus(TextInfo(i),k,Veta)
                k=index(TextInfo(i),'#')
                if(k.gt.0) Veta=Veta(:idel(Veta))//TextInfo(i)(k:)
                j=LocateInStringArray(DrawAtName,DrawAtN,Veta,
     1                                IgnoreCaseYes)
                call FeQuestCrwOpen(CrwLastMade,j.gt.0)
              endif
              ypom=ypom-20.
              if(i.eq.1) ypom=ypom-5.
            enddo
            ypom=ypom-12.
            if(NDim(KPhase).eq.3.or.xyzmap) then
              Veta='Include the selected point as a %new atom in M40'
              dpom=FeTxLengthUnder(Veta)+10.
              xpom=(xqd-dpom)*.5
              call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
              nButtIncM40=ButtonLastMade
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
              ypom=ypom-25.
              Veta='Save the selected point for %future use'
              dpom=FeTxLengthUnder(Veta)+10.
              xpom=(xqd-dpom)*.5
              call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
              nButtIncPoint=ButtonLastMade
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            else
              nButtIncM40=0
              nButtIncPoint=0
            endif
            call FeTabsReset(0)
            call FeTabsReset(1)
            ich=333
2050        call FeQuestEvent(id,ich)
            if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtIncM40)
     1        then
              if(.not.allocated(AtTypeMenu)) then
                allocate(AtTypeMenu(NAtFormula(KPhase)))
                call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                                 NAtFormula(KPhase))
              endif
              NewAtName=' '
              call EM40NewAtomComplete(xfract,u,v,0,nsubs,ich)
              if(ich.eq.0) then
                 NewAtoms=.true.
              else if(ich.lt.0) then
                 ModifiedAtoms=.true.
              endif
              call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
            else if(CheckType.eq.EventButton.and.
     1              CheckNumber.eq.nButtIncPoint) then
              call ConSaveSelPoint(xfract)
              ich=0
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 2050
            endif
            n=DrawAtN
            NSkrt=0
            if(ich.eq.0) then
              nCrw=nCrwFirst
              do i=1,npdf
                k=0
                call kus(TextInfo(i+1),k,Veta)
                k=index(TextInfo(i+1),'#')
                if(k.gt.0) Veta=Veta(:idel(Veta))//TextInfo(i+1)(k:)
                j=LocateInStringArray(DrawAtName,DrawAtN,Veta,
     1                                IgnoreCaseYes)
                if(CrwLogicQuest(nCrw)) then
                  if(j.gt.0) go to 2058
                  if(DrawAtN.lt.NMaxDraw) then
                    DrawAtN=DrawAtN+1
                    DrawAtName(DrawAtN)=Veta
                    DrawAtColor(DrawAtN)=0
                    DrawAtSkip(DrawAtN)=.false.
                    if(DrawAtN.le.1) then
                      DrawAtBondLim(DrawAtN)=2.
                    else
                      DrawAtBondLim(DrawAtN)=DrawAtBondLim(DrawAtN-1)
                    endif
                    KeysSaved=.false.
                  endif
                else if(j.gt.0) then
                  NSkrt=NSkrt+1
                  DrawAtName(j)=' '
                endif
2058            nCrw=nCrw+1
              enddo
              if(NSkrt.gt.0) then
                n=0
                do i=1,DrawAtN
                  if(DrawAtName(i).eq.' ') cycle
                  n=n+1
                  DrawAtName(n)=DrawAtName(i)
                  DrawAtColor(n)=DrawAtColor(i)
                  DrawAtSkip(n)=DrawAtSkip(i)
                  DrawAtBondLim(n)=DrawAtBondLim(i)
                enddo
                DrawAtN=n
              endif
            endif
            call FeQuestRemove(id)
            if((n.lt.DrawAtN.or.NSkrt.gt.0)) then
              if(.not.AtomsOn) then
                AtomsOn=.true.
                ConButtLabels(IdAtomsOnOff)='At%oms OFF'
                AtomsOn=.true.
                call FeQuestButtonLabelChange(nButtAtomsDraw,
     1                                      ConButtLabels(IdAtomsOnOff))
                call FeQuestButtonOff(nButtAtomsDraw)
              endif
              call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                         YMaxGrWin,ConImageFile,-1)
              call ConDrawAtoms(zmap)
              call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                         YMaxGrWin,SvFile)
            endif
            call FeMouseShape(MouseType)
            call FeMoveMouseTo(XposP,YposP)
          else if(LocatorType.eq.LocatorDistance.or.
     1            LocatorType.eq.LocatorNewSection) then
            if(ip.le.1) ip=ip+1
            call CopyVek(xfract,xfd(1,ip),3)
            if(ip.eq.1) then
              xu(1)=XPos
              yu(1)=Ypos
            else
              do i=1,3
                xfd(i,3)=xfd(i,1)-xfd(i,2)
              enddo
              call multm(MetTens(1,nsubs,KPhase),xfd(1,3),xfd(1,4),3,3,
     1                   1)
              dd=sqrt(scalmul(xfd(1,3),xfd(1,4)))
              if(dd.lt..001) go to 2490
              call FeDeferOutput
              call FePlotMode('E')
              call FePolyLine(2,xu,yu,Green)
              call FePlotMode('N')
              xu(ip)=Xpos
              yu(ip)=Ypos
              if(LocatorType.eq.LocatorDistance) then
                i=ArrowFromTo
              else
                i=ArrowTo
              endif
              call FeArrow(xu(1),yu(1),xu(2),yu(2),LightYellow,i)
              if(LocatorType.eq.LocatorDistance) then
                write(Cislo,'(f8.3)') dd
                call FeWInfWrite(4,Cislo(:idel(Cislo)))
2110            call FeEvent(0)
                ByloCrw=.false.
                if(EventType.eq.EventMouse) then
                  if(EventNumber.eq.JeMove.or.EventNumber.eq.JeLeftUp
     1               .or.EventNumber.eq.JeRightUp) go to 2110
                else if(EventType.eq.EventKey) then
                  if(EventNumber.ne.JeEscape) go to 2110
                else if(EventType.eq.EventCrw) then
                  if(EventNumber.ne.nCrwLocDistance) then
                    call FeQuestCrwOff(nCrwLocDistance)
                    call FeQuestCrwOn(EventNumber)
                    CheckNumber=EventNumber
                    CheckType=EventCrw
                    NewLocatorType=EventNumber
                    EventNumber=0
                    EventType=0
                    ByloCrw=.true.
                  else
                    go to 2110
                  endif
                else if(EventType.eq.EventButton) then
                  CheckNumber=EventNumber
                  CheckType=EventButton
                  NewLocatorType=EventNumber
                  EventNumber=0
                  EventType=0
                  call FeQuestLblOff(nLblWinf(4))
                  call FeWInfClose(4)
                  JesteSeNepsalo=.true.
                  go to 2020
                else
                  go to 2110
                endif
              else
                if(.not.Obecny) then
                  ScopePlane(3)=0.
                  ShiftPlane(3)=0.
                endif
                do i=1,3
                  u(i)=xfd(i,2)-xfd(i,1)
                enddo
                call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
                uu=scalmul(u,ug)
                ScopePlaneNew=anint(100.*sqrt(uu))*.01
                ich=0
                id=NextQuestId()
                xqd=300.
                il=2
                Veta='Specify the new section'
                if(YPos.gt.YCenGrWin) then
                  ypom=YMinGrWin+5.
                else
                  ypom=YMaxGrWin-4.*QuestLineWidth-25.
                endif
                call FeQuestCreate(id,-1.,ypom,xqd,il,Veta,0,LightGray,
     1                             0,0)
                il=1
                tpom=5.
                Veta='%Width'
                dpom=80.
                xpom=tpom+FeTxLengthUnder(Veta)+10.
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwWidth=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,ScopePlaneNew,
     1                                  .false.,.false.)
                il=il+1
                Veta='%Depth'
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwDepth=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,ScopePlane(3),
     1                                  .false.,.false.)
                il=1
                tpom=xpom+dpom+25.
                Veta='%Angle'
                xpom=tpom+FeTxLengthUnder(Veta)+10.
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwAngle=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,Angle,.false.,
     1                                  .false.)
2250            call FeQuestEvent(id,ich)
                if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 2250
                endif
                if(ich.eq.0) then
                  ScopePlane(1)=ScopePlaneNew
                  call FeQuestRealFromEdw(nEdwWidth,ScopePlane(2))
                  call FeQuestRealFromEdw(nEdwDepth,ScopePlane(3))
                  call FeQuestRealFromEdw(nEdwAngle,Angle)
                  call FeQuestRemove(id)
                  ShiftPlane(1)=0.
                  ShiftPlane(2)=ScopePlane(2)*.5
                  ShiftPlane(3)=ScopePlane(3)*.5
                  call CopyVek(xfd(1,1),XPlane(1,1),3)
                  call CopyVek(xfd(1,2),XPlane(1,2),3)
                  do i=1,3
                    DxPlane(i,2)=XPlane(i,2)-XPlane(i,1)
                  enddo
                  if(Obecny) then
                    do i=1,3
                      v(i)=trobi(i,3)
                    enddo
                  else
                    call SetRealArrayTo(v,3,0.)
                    call SetRealArrayTo(w,3,0.)
                    v(iorien(1))=1.
                    w(iorien(2))=1.
                    call VecMul(v,w,vg)
                    call multm(MetTensI(1,nsubs,KPhase),vg,v,3,3,1)
                    DeltaPlane=1.
                    do i=1,3
                      DeltaPlane=min(DeltaPlane,dx(i)*CellParCon(i))
                    enddo
                    rad=10.**Exponent10(DeltaPlane)
                    DeltaPlane=anint(DeltaPlane/rad)*rad
                    DeltaPlane=max(.01,DeltaPlane)
                  endif
                  call VecMul(u,v,ug)
                  call multm(MetTensI(1,nsubs,KPhase),ug,u,3,3,1)
                  call multm(MetTens(1,nsubs,KPhase),v,vg,3,3,1)
                  call VecNor(u,ug)
                  call VecNor(v,vg)
                  sinp=sin(Angle*ToRad)
                  cosp=cos(Angle*ToRad)
                  do i=1,3
                    pom=cosp*u(i)+sinp*v(i)
                    XPlane(i,3)=XPlane(i,1)+pom
                    DxPlane(i,3)=pom
                  enddo
                  call SetIntArrayTo(SelPlane,3,2)
                  call SetStringArrayTo(StPlane,3,' ')
                  call FeQuestCrwOff(nCrwLocNewSection)
                  call FeQuestCrwOn(nCrwLocPosition)
                  LocatorType=LocatorPosition
                  NoOfDerivedMaps=NoOfDerivedMaps+1
                  CheckType=EventButton
                  CheckNumber=nButtNew
                  if(.not.obecny) then
                    obecny=.true.
                    kpdf=1
                  endif
                  go to 7000
                endif
                call FeQuestRemove(id)
              endif
2490          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                         SvFile,0)
              call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                         SvFile)
              ip=0
              if(LocatorType.eq.LocatorDistance) then
                call FeQuestLblOff(nLblWinf(4))
                call FeWInfClose(4)
                JesteSeNepsalo=.true.
              endif
              call FeReleaseOutput
              if(ByloCrw) then
                ByloCrw=.false.
                LocatorType=NewLocatorType
                go to 2010
              endif
            endif
          endif
        endif
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeMove) then
        if((Klic.eq.0.or.LocatorType.eq.LocatorDistance.or.
     1      LocatorType.eq.LocatorNewSection).and.ip.ge.1) then
          call FePlotMode('E')
          if(ip.gt.1) then
            call FePolyLine(2,xu,yu,Green)
          else if(ip.eq.1) then
            ip=2
          endif
        endif
        call GetCoord(xdo,xp,xfract)
        Xposp=FeXo2X(xdo(1))
        Yposp=FeYo2Y(xdo(2))
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          if(Klic.eq.0.or.
     1       ((LocatorType.eq.LocatorDistance.or.
     2         LocatorType.eq.LocatorNewSection).and.ip.ge.1)) then
            Xpos=Xposp
            Ypos=Yposp
            call FeMoveMouseTo(Xpos,Ypos)
          else
            if(JeUvnitr) then
              call FeMouseShape(0)
              call ConLocatorInfoClose
              irec=ireco
              call ConHeader
              JeUvnitr=.false.
            endif
          endif
        else
          if(Klic.ne.0) then
            if(.not.JeUvnitr) then
              call FeMouseShape(MouseType)
              ireco=irec
              irec=0
              call FeHeaderInfo(' ')
              call ConLocatorInfoOpen(xp,xfract)
              JeUvnitr=.true.
            endif
          endif
          Xpos=Xposp
          Ypos=Yposp
        endif
        if((klic.eq.0.or.
     1      LocatorType.eq.LocatorDistance.or.
     2      LocatorType.eq.LocatorNewSection).and.ip.ge.1) then
          xu(2)=Xpos
          yu(2)=Ypos
          if(ip.ne.0) then
            if(LocatorType.eq.LocatorDistance) then
              call CopyVek(xfract,xfd(1,2),3)
              do i=1,3
                xfd(i,3)=xfd(i,1)-xfd(i,2)
              enddo
              call multm(MetTens(1,nsubs,KPhase),xfd(1,3),xfd(1,4),3,3,
     1                   1)
              dd=sqrt(scalmul(xfd(1,3),xfd(1,4)))
              write(Cislo,'(f8.3)') dd
              call FePlotMode('N')
              if(JesteSeNepsalo) call FeQuestLblOn(nLblWinf(4))
              call FeWInfWrite(4,Cislo(:idel(Cislo)))
              JesteSeNepsalo=.true.
              call FePlotMode('E')
            endif
            call FePolyLine(2,xu,yu,Green)
          endif
          call FePlotMode('N')
        endif
        if(JeUvnitr) call ConLocatorInfoOut(xp,xfract)
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown)
     1  then
        go to 3100
      else if(CheckType.eq.EventCrw) then
        if(CheckNumber.eq.nCrwLocPosition) then
          MouseType=1
          LocatorType=LocatorPosition
          call FeWinfClose(4)
        else if(CheckNumber.eq.nCrwLocDistance) then
          MouseType=0
          LocatorType=LocatorDistance
          ndst=0
        else if(CheckNumber.eq.nCrwLocNewAtom) then
          MouseType=0
          LocatorType=LocatorNewAtom
          call FeWinfClose(4)
        else if(CheckNumber.eq.nCrwLocNewSection) then
          MouseType=0
          LocatorType=LocatorNewSection
          call FeWinfClose(4)
        endif
        go to 2000
      else if(CheckType.eq.EventButton.or.CheckType.eq.EventResize)
     1  then
        if(CheckNumber.eq.nButtNew.or.CheckNumber.eq.nButtQuit.or.
     1     CheckNumber.eq.nButtOptions) then
          if(NoOfDerivedMaps.eq.0.and..not.KeysSaved)
     1      call ConWriteKeys
          NoOfDerivedMaps=0
        endif
        go to 3100
      endif
      if(Klic.eq.0) then
        go to 2010
      else
        go to 2000
      endif
3100  TakeMouseMove=.false.
      call FePlotMode('N')
      call FeDeferOutput
      call ConLocatorInfoClose
3111  irec=ireco
      if(klic.ne.0..or.NCurve.le.1) then
        call FeMouseShape(0)
        go to 7000
      endif
      call FeQuestButtonLabelChange(nButtQuit,'%Continue')
      call FeQuestButtonDisable(nButtRun3dMaps)
      do i=nButtBasFr,nButtBasTo
        if(ActiveButtBas(i)) call FeQuestButtonClose(i)
      enddo
      if(VolaToContour) then
        call FeQuestButtonOpen(nButtYScale,ButtonOff)
        do i=nButtBasTo+2,nButtBasTo+3
          call FeQuestButtonOpen(i,ButtonDisabled)
        enddo
        j=5
      else
        j=4
      endif
      do i=3,j
        call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                         ContourYSep(i),LightGray,LightGray)
      enddo
      if(VolaToContour) call FeTwoPixLineHoriz(XMaxGrWin+1.,
     1                                         XMaxBasWin,ContourYSep(6)
     2                                        ,Gray,White)
      xomx=0.
      do i=1,NCurve
        call multm(O2F,XLocCurve(1,i),xp(1,i),3,3,1)
        call prevod(1,xp(1,i),XFractCurve(1,i))
        do j=1,2
          xp(j,i)=min(xp(j,i),xmax(j))
          xp(j,i)=max(xp(j,i),xmin(j))
        enddo
        call multm(F2O,xp(1,i),XLocCurve(1,i),3,3,1)
        if(i.gt.1) then
          do j=1,3
            xd(j,i-1)=(xp(j,i)-xp(j,i-1))/float(NExtPoints)
            xdo(j)=(XLocCurve(j,i)-XLocCurve(j,i-1))/float(NExtPoints)
          enddo
          dlk(i-1)=sqrt(scalmul(xdo,xdo))
          xomx=xomx+dlk(i-1)*float(NExtPoints)
        endif
      enddo
      if(allocated(gx)) deallocate(gx,gy,pty,sgymc,spty,dpty,stat=i)
      n=NExtPoints+1
      allocate(gx(n,9),gy(n,9),pty(n,9),sgymc(n,9),spty(n,9),dpty(n,9),
     1         stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PCurves" point#2.',i)
        go to 9000
      endif
      pom=0.
      ptymx=-99999.
      ptymn= 99999.
      do i=1,NCurve-1
        xs=xp(1,i)
        ys=xp(2,i)
        do j=1,NExtPoints+1
          gyji=ExtMap(xs,ys,ActualMap,NActualMap)+ContourRefLevel
          if(ErrMapActive) sgyji=ExtMap(xs,ys,ErrMap,NActualMap)
          if(DensityType.ne.2) then
            if(gyji.gt.0.) then
              pty(j,i)=toev*teplota*log(Dmax/gyji)
              if(ErrMapActive) spty(j,i)=toev*teplota*sgyji/gyji
            else
              pty(j,i)=50000.
              if(ErrMapActive) spty(j,i)=50000.
            endif
            ptymn=min(ptymn,pty(j,i))
            ptymx=max(ptymx,pty(j,i))
          endif
          gx(j,i)=pom
          gy(j,i)=gyji
          if(j.ne.NExtPoints+1) then
            xs=xs+xd(1,i)
            ys=ys+xd(2,i)
            pom=pom+dlk(i)
          endif
        enddo
      enddo
      xomn=0.
      yomn=Dmin+ContourRefLevel
      yomx=Dmax+ContourRefLevel
      nButtOn=0
4000  call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
4100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        if(HardCopy.eq.HardCopyNum)
     1    call ConMakeCurveLabel('density curve',XFractCurve,NCurve)
        call FeHeaderInfo('Density curve')
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'x')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'d [e/Ang^3]')
        do i=1,NCurve-1
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
          call FeXYPlot(gx(1,i),gy(1,i),NExtPoints+1,NormalLine,
     1                  NormalPlotMode,White)
          if(i.lt.NCurve-1) then
            xu(1)=FeXo2X(gx(NExtPoints+1,i))
            yu(1)=YMinAcWin
            xu(2)=xu(1)
            yu(2)=YMaxAcWin
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
            call FeLineType(NormalLine)
          endif
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
        enddo
        if(HardCopy.eq.HardCopyNum) then
          write(85,'(''# end of curve'')')
          write(85,'(''# end of file'')')
        endif
      endif
      call FeHardCopy(HardCopy,'close')
4250  if(nButtOn.ne.0) call FeButtonOff(nButtOn)
      nButtOn=0
      HardCopy=0
4300  call FeEvent(0)
      if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        nButtOn=EventNumber
        if(EventNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 4100
          else
            HardCopy=0
            go to 4250
          endif
        else if(EventNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 4100
          else
            HardCopy=0
            go to 4250
          endif
        else if(EventNumber.eq.nButtQuit) then
          call FeQuestButtonOff(EventNumber)
          if(VolaToContour) then
            if(ExistFile(fln(:ifln)//'.m85').and.kpdf.ge.2)
     1        call FeQuestButtonOff(nButtErrOn)
            call FeQuestButtonOff(nButtDerOn)
            call FeQuestButtonLabelChange(nButtQuit,'%Return')
          endif
          call FeQuestButtonOff(EventNumber)
          nButtOn=0
          go to 4400
        else if(EventNumber.eq.nButtYScale) then
          call ConReadDenLimits(yomn,yomx,ich)
          if(ich.ne.0) go to 4250
          go to 4000
        endif
      endif
      go to 4300
4400  if(DensityType.eq.2) then
        EventType=0
        EventNumber=0
        go to 7000
      endif
      yomn=0.
      yomx=min(1000.,ptymx)
      HardCopy=0
      call FeClearGrWin
      dptymx=-99999.
      do i=1,NCurve-1
        do j=2,NExtPoints
          if(pty(j+1,i).lt.49999..and.pty(j-1,i).lt.49999.) then
            dpty(j,i)=abs(pty(j+1,i)-pty(j-1,i))/(2.*dlk(i))
            if(dpty(j,i).gt.dptymx) dptymx=dpty(j,i)
          else
            dpty(j,i)=-1.
          endif
        enddo
        if(pty(1,i).lt.49999..and.pty(2,i).lt.49999.) then
          dpty(1,i)=abs(pty(2,i)-pty(1,i))/dlk(i)
          if(dpty(1,i).gt.dptymx) dptymx=dpty(1,i)
        else
          dpty(1,i)=-1.
        endif
        if(pty(NExtPoints,i)  .lt.49999..and.
     1     pty(NExtPoints+1,i).lt.49999.) then
          dpty(NExtPoints+1,i)=
     1      abs(pty(NExtPoints+1,i)-pty(NExtPoints,i))/dlk(i)
          if(dpty(NExtPoints+1,i).gt.dptymx)
     1      dptymx=dpty(NExtPoints+1,i)
        else
          dpty(NExtPoints+1,i)=-1.
        endif
      enddo
4500  call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        if(HardCopy.eq.HardCopyNum)
     1    call ConMakeCurveLabel('potential curve',XFractCurve,NCurve)
        call FeHeaderInfo('Potential curve')
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'x')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'p[meV]')
        do i=1,NCurve-1
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
          call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                  NormalPlotMode,White)
          if(i.lt.NCurve-1) then
            xu(1)=FeXo2X(gx(NExtPoints+1,i))
            yu(1)=YMinAcWin
            xu(2)=xu(1)
            yu(2)=YMaxAcWin
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
            call FeLineType(NormalLine)
          endif
          if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1      write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
        enddo
        if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        if(IncErr) then
          if(HardCopy.eq.HardCopyNum)
     1      call ConMakeCurveLabel('Error curve',XFractCurve,NCurve)
          do i=1,NCurve-1
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
            call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                    NormalPlotMode,White)
            pom=yomx/dptymx
            do j=1,NExtPoints+1
              if(IncErr.and..not.ErrMapActive) then
                gyji=gy(j,i)
                if(gyji.gt.0.) then
                  spty(j,i)=toev*teplota*sgymc(j,i)/gyji
                else
                  spty(j,i)=50000.
                endif
              endif
            enddo
            call FeXYPlot(gx(1,i),spty(1,i),NExtPoints+1,DashedLine,
     1                    NormalPlotMode,Green)
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
          enddo
          if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        endif
        if(IncDer) then
          if(HardCopy.eq.HardCopyNum)
     1      call ConMakeCurveLabel('Derivative curve',XFractCurve,
     2                             NCurve)
          do i=1,NCurve-1
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# begin of '',i2,a2,'' segment'')') i,nty(i)
            call FeXYPlot(gx(1,i),pty(1,i),NExtPoints+1,NormalLine,
     1                    NormalPlotMode,White)
            pom=yomx/dptymx
            do j=1,NExtPoints+1
              if(dpty(j,i).ge.0.) then
                dpty(j,i)=dpty(j,i)*pom
              else
               dpty(j,i)=1.01*yomx
              endif
            enddo
            dptymx=yomx
            call FeXYPlot(gx(1,i),dpty(1,i),NExtPoints+1,DashedLine,
     1                    NormalPlotMode,Red)
            if(NCurve.gt.2.and.HardCopy.eq.HardCopyNum)
     1        write(85,'(''# end   of '',i2,a2,'' segment'')') i,nty(i)
          enddo
          if(HardCopy.eq.HardCopyNum) write(85,'(''# end of curve'')')
        endif
        if(HardCopy.eq.HardCopyNum) write(85,'(''# end of file'')')
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
4750  if(nButtOn.ne.0) call FeButtonOff(nButtOn)
      nButtOn=0
4800  call FeEvent(0)
4805  if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        nButtOn=EventNumber
        if(EventNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 4500
          else
            HardCopy=0
            go to 4750
          endif
        else if(EventNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 4500
          else
            HardCopy=0
            go to 4750
          endif
        else if(EventNumber.eq.nButtQuit) then
          call FeQuestButtonOff(EventNumber)
          call FeQuestButtonLabelChange(nButtQuit,'%Quit')
          EventType=0
          EventNumber=0
          go to 7000
        else if(EventNumber.eq.nButtYScale) then
          teplold=teplota
          call ConReadPotLimits(yomn,yomx,teplota,ich)
          if(ich.ne.0) go to 4750
          do i=1,NCurve-1
            do j=1,NExtPoints+1
              pty(j,i)=pty(j,i)*teplota/teplold
              if(ErrMapActive) spty(j,i)=spty(j,i)*teplota/teplold
            enddo
          enddo
          go to 4500
        else if(EventNumber.eq.nButtderOn) then
          Veta=Men1(2)
          Men1(2)=Men1(4)
          Men1(4)=Veta
          IncDer=.not.IncDer
          if(IncDer) then
            Veta='%Der OFF'
          else
            Veta='%Der ON'
          endif
          call FeQuestButtonLabelChange(nButtderOn,Veta)
          go to 4500
        else if(EventNumber.eq.nButtErrOn) then
          if(.not.IncErr.and..not.ErrMapActive)
     1      call rezmc(XFractCurve,NCurve,NExtPoints+1,gy,sgymc,*4750)
          Veta=Men1(3)
          Men1(3)=Men1(5)
          Men1(5)=Veta
          IncErr=.not.IncErr
          if(IncErr) then
            Veta='%Err OFF'
          else
            Veta='%Err ON'
          endif
          call FeQuestButtonLabelChange(nButtErrOn,Veta)
          go to 4500
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        j=FeMenu(-1.,-1.,men1,1,3,1,0)
        EventType=EventButton
        EventNumber=nButtYScale
        go to 4805
      endif
      go to 4800
7000  call FeMakeAcWin(40.,20.,30.,30.)
      if(VolaToContour) then
        call SetTr
      else
        call GrtSetTr
      endif
      if(klic.eq.1) go to 9000
      call FeQuestButtonLabelChange(nButtQuit,'%Quit')
      reconfig=.true.
      if(IncDer) then
        Veta=Men1(2)
        Men1(2)=Men1(4)
        Men1(4)=Veta
      endif
      if(IncErr) then
        Veta=Men1(3)
        Men1(3)=Men1(5)
        Men1(5)=Veta
      endif
9000  call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile,
     1                   0)
      if(klic.eq.0) then
        do i=nButtBasTo+1,nButtBasTo+3
          call FeQuestButtonClose(i)
        enddo
        do i=nButtBasFr,nButtBasTo
          if(ActiveButtBas(i)) call FeQuestButtonOpen(i,ButtonOff)
        enddo
        do i=3,5
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                           ContourYSep(i),Gray,White)
        enddo
        call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,
     1                         ContourYSep(6),LightGray,LightGray)
      endif
      AllowChangeMouse=.true.
      QuestVystraha=.true.
      if(allocated(ErrMap)) deallocate(ErrMap,stat=i)
      if(allocated(gx)) deallocate(gx,gy,pty,sgymc,spty,dpty,stat=i)
      return
100   format(3f10.4)
101   format(3f12.6)
103   format(f10.2)
      end
