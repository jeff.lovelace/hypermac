      subroutine ChdZFSDetermination
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension idist(9),xp(3)
      dimension delxyz(:),xyzi(:),rinc(:),step(:),distf(:),dist(:),
     1          iterm(:),NZeroNeigh(:)
      character*80 t80
      logical lback(:),lfor(:),ldone(:)
      allocatable delxyz,xyzi,rinc,step,distf,dist,lback,iterm,lfor,
     1            ldone,NZeroNeigh
      allocate(delxyz(nPhiThInt*3),xyzi(nPhiThInt*3),rinc(nPhiThInt),
     1         step(nPhiThInt),distf(nPhiThInt),dist(nPhiThInt),
     2         iterm(nPhiThInt),lback(nPhiThInt),lfor(nPhiThInt),
     3         ldone(nPhiThInt),NZeroNeigh(nPhiThInt))
      BetaRi=RCapture(IAtInt)
      icount=0
      NZeroNeighAll=0
      if(MethZFS.eq.1) go to 3000
3000  n=1
      do i=1,nThInt
        do j=1,NPhiInt
          delxyz(n)  =stheta(i)*cphi(j)
          delxyz(n+1)=stheta(i)*sphi(j)
          delxyz(n+2)=ctheta(i)
          n=n+3
        enddo
      enddo
      if(MethZFS.eq.1) then
        call SetRealArrayTo(sur,nPhiThInt,TInfInt)
        call SetRealArrayTo(dist,nPhiThInt,0.)
        call SetLogicalArrayTo(ldone,nPhiThInt,.false.)
        ik=0
        icount=0
        do i=1,nPhiThInt
          if(ldone(i)) icount=icount+1
          lback(i)=.false.
          lfor(i)=.false.
          rinc(i)=4.*StepInt
          if(dist(i).eq.0.) then
            ii=(i-1)/NPhiInt
            jj=mod(i-1,NPhiInt)+1
            im1=ii-1
            im2=ii-2
            jm1=jj-1
            jp1=jj+1
            if(im1.lt.0) im1=ii
            if(im2.lt.0) im2=im1
            if(jp1.gt.NPhiInt) jp1=1
            if(jm1.le.0) jm1=NPhiInt
            idist(1)=NPhiInt*im1+jj
            idist(2)=NPhiInt*im1+jp1
            idist(3)=NPhiInt*im1+jm1
            idist(4)=NPhiInt*im2+jj
            idist(5)=NPhiInt*im2+jp1
            idist(6)=NPhiInt*im2+jm1
            idist(7)=NPhiInt*ii+jj
            idist(8)=NPhiInt*ii+jp1
            idist(9)=NPhiInt*ii+jm1
            sdist=0.
            pom=0.
            do jl=2,8
              ii=idist(jl)
              if(dist(ii).ne.0.) then
                sdist=sdist+dist(ii)
                pom=pom+1.
              endif
            enddo
            if(pom.gt.0.) dist(i)=sdist/pom
          endif
          if(dist(i).ne.0.) then
            step(i)=dist(i)
          else
            step(i)=BetaRi+rinc(i)
          endif
          do j=1,3
            ik=ik+1
            xyzi(ik)=delxyz(ik)*step(i)+rcla(j,IAtInt)
          enddo
        enddo
        IVarka=1
        srinc=0.
        do i=1,nPhiThInt
          srinc=srinc+rinc(i)
        enddo
        srinc=srinc/float(nPhiThInt)
        write(t80,100) IVarka,srinc,AccurInt
        Cislo='ZFS for '//atom(IAtInt)
        t80=Cislo(:idel(Cislo))//', '//t80(:idel(t80))
        call FeOpenInterrupt(-1.,-1.,t80)
      endif
3500  kk=0
      im3=0
      call ChdSearchSource(xyzi,ldone,nPhiThInt,5,iterm,NZeroNeigh)
      if(ErrFlag.ne.0) go to 9999
      im3=-3
      k=0
      srinc=0.
      do i=1,nPhiThInt
        im3=im3+3
        if(ldone(i)) cycle
        k=k+1
        if(rinc(i).le.AccurInt) then
          dist(i)=step(i)
          ldone(i)=.true.
          icount=icount+1
          cycle
        endif
        if(iterm(k).eq.1) then
          if(lback(i)) rinc(i)=rinc(i)*0.5
          step(i)=step(i)+rinc(i)
          lfor(i)=.true.
        else
          if(lfor(i)) rinc(i)=rinc(i)*0.5
          step(i)=step(i)-rinc(i)
          lback(i)=.true.
        endif
        do j=1,3
          ik=im3+j
          xyzi(ik)=delxyz(ik)*step(i)+rcla(j,IAtInt)
        enddo
        srinc=srinc+rinc(i)
      enddo
      if(icount.ne.nPhiThInt) then
        srinc=srinc/float(k)
        IVarka=IVarka+1
        write(t80,100) IVarka,srinc,AccurInt
        Cislo='ZFS for '//atom(IAtInt)
        t80=Cislo(:idel(Cislo))//', '//t80(:idel(t80))
        call FeOutputInterrupt(t80)
        go to 3500
      endif
      n=1
      SurMin=9999.
      SurMax=  0.
      SurAve=  0.
      do i=1,nThInt
        do j=1,NPhiInt
          pom=dist(n)
          SurMin=min(SurMin,pom)
          SurMax=max(SurMax,pom)
          SurAve=SurAve+pom
          sur(i,j)=pom
          sth=sin(theta(i))
          cth=cos(theta(i))
          sph=sin(phi(j))
          cph=cos(phi(j))
          xp(1)=sth*cph*dist(n)
          xp(2)=sth*sph*dist(n)
          xp(3)=cth*dist(n)
          n=n+1
        enddo
      enddo
      call NewLn(5)
      write(lst,'(/''Minimal ZFS distance : '',f8.3 )') SurMin
      write(lst,'( ''Maximal ZFS distance : '',f8.3 )') SurMax
      write(lst,'( ''Average ZFS distance : '',f8.3/)')
     1  SurAve/float(nPhiThInt)
9999  call FeCloseInterrupt
      deallocate(delxyz,xyzi,rinc,step,distf,dist,iterm,lback,lfor,
     1           ldone,NZeroNeigh)
      return
100   format('Cycle :',i3,', step/accuracy : ',f6.4,'/',f6.4)
      end
