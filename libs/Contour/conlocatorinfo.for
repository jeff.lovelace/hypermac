      subroutine ConLocatorInfo
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(*),xfract(3)
      character*80 t80
      character*7  FormatLocal
      integer Exponent10
      save FormatLocal
      entry ConLocatorInfoOpen(xp,xfract)
      i=max(4-Exponent10(max(xmax(1),xmax(2))),0)
      i=min(i,4)
      write(FormatLocal,'(''(2f7.'',i1,'')'')') i
      call FeHeaderInfo(ConWinfLabel(1))
      call FeQuestLblOn(nLblWinf(3))
      do i=1,3
        if(i.eq.1) then
          write(t80,FormatLocal)(xp(j),j=1,2)
        else if(i.eq.2) then
          write(t80,102) xfract
        else
          write(t80,103)
     1      ExtMap(xp(1),xp(2),ActualMap,NActualMap)+ContourRefLevel
        endif
        call FeWInfWrite(i,t80)
      enddo
      go to 9999
      entry ConLocatorInfoOut(xp,xfract)
      do i=1,3
        if(i.eq.1) then
          write(t80,FormatLocal)(xp(j),j=1,2)
        else if(i.eq.2) then
          write(t80,102) xfract
        else
          write(t80,103)
     1      ExtMap(xp(1),xp(2),ActualMap,NActualMap)+ContourRefLevel
        endif
        call FeWInfWrite(i,t80)
      enddo
      go to 9999
      entry ConLocatorInfoClose
      do i=1,3
        call FeWInfClose(i)
      enddo
      call FeHeaderInfo(' ')
      call FeQuestLblOff(nLblWinf(3))
9999  return
102   format(3f7.4)
103   format(f10.3)
      end
