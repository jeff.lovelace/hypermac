      function fsum(k)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      kk=(k*(k-1))/2
      fsum=0.
      do i=1,k
        fsum=fsum+rnda(i)*covc(kk+i)
      enddo
      return
      end
