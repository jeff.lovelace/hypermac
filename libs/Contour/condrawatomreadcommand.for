      subroutine ConDrawAtomReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DrawAtomQuest/ nEdwName,nEdwBond,icolor,BondLim,AtName
      dimension xp(1)
      character*80 t80,AtName
      character*(*) Command
      integer ColorOrder
      logical EqIgCase
      save /DrawAtomQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(.not.EqIgCase(t80,'drawatom').and.
     1   .not.EqIgCase(t80,'!drawatom')) go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,AtName)
      call TestAtomString(AtName,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                    IdAtMolMixNo,t80)
      if(t80.ne.' ') go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      icolor=max(ColorOrder(t80)+1,1)
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      kk=0
      call StToReal(t80,kk,xp,1,.false.,ich)
      if(ich.ne.0) go to 8040
      BondLim=xp(1)
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  if(t80.eq.'Jiz oznameno') then
        ich=1
        go to 9999
      endif
      t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
