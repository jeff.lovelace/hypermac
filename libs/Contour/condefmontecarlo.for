      subroutine ConDefMonteCarlo(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*40 Veta
      if(mcmax.eq.0) mcmax=1000
      id=NextQuestId()
      xqd=250.
      call FeQuestCreate(id,-1.,-1.,xqd,2,'Parameters for Monte '//
     1                   'Carlo calculation',0,LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Number of iteration steps'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNSteps=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mcmax,.false.)
      il=il+1
      Veta='A%ccuracy limit in %'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ErrLev,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNSteps,mcmax)
        call FeQuestRealFromEdw(nEdwLimit,ErrLev)
      endif
      call FeQuestRemove(id)
      return
      end
