      function rank4(d)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension d(15)
      rank4=pm63_1*d(1)+pm63_2*d(11)+pm63_3*d(15)+
     1       w12*(wm3p1*d(2) +wm3p2*d(7) +wmp3*d(9))+
     2       w13*(wm3p1*d(3) +wm3p3*d(10)+wmp2*d(8))+
     3       w23*(wm3p2*d(12)+wm3p3*d(14)+wmp1*d(5))+
     4       wmp1*(wmp2*d(4)+wmp3*d(6))+wmp2*wmp3*d(13)
      return
      end
