      subroutine CPSearchInMap(Klic,UzitStarou)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(6),DenSupl(3),xn(6),WorkMap(:,:),Table(:)
      character*80 Veta
      logical UzitStarou,PointAlreadyPresent
      allocatable WorkMap,Table
      if(UzitStarou) then
        allocate(WorkMap(nxny,3),Table(nxny))
        go to 3650
      endif
      if(Klic.eq.0) then
        nxny=1
        mapa=7
        DensityType=5
        do i=1,6
          if(i.le.3) then
            nx(i)=nint((xmax(i)-xmin(i))/dx(i))+1
            if(i.le.2) then
              nxny=nxny*nx(i)
            else
              nmap=nx(i)
            endif
          else
            nx(i)=1
            xmin(i)=0.
            xmax(i)=0.
            dx(i)=.1
          endif
        enddo
        allocate(WorkMap(nxny,3),Table(nxny))
        m8=NextLogicNumber()
        call OpenMaps(m8,fln(:ifln)//'.z81',nxny,1)
        if(ErrFlag.ne.0) go to 9999
        write(m8,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                  (iorien(i),i=1,6),mapa,nsubs,SatelityByly,
     2                  nonModulated(KPhase)
        nvse=npdf*nxny*nx(3)
        call FeFlowChartOpen(-1.,-1.,max(nint(float(nvse)*.005),10),
     1                       nvse,'Creation of inversed gradient map',
     2                       ' ',' ')
        m=0
        Dmax=-999999.
        Dmin= 999999.
        CutOffDistA=CutOffDist*rcp(1,1,KPhase)
        CutOffDistB=CutOffDist*rcp(2,1,KPhase)
        CutOffDistC=CutOffDist*rcp(3,1,KPhase)
        xp3=xmin(3)
        do iz=1,nx(3)
          do i=1,3
            call SetRealArrayTo(WorkMap(1,i),nxny,0.)
          enddo
          do 3500ia=1,npdf
            ip=ipor(ia)
            fpdfi=fpdf(ip)
            if(fpdfi.le.0.) then
              m=m+nxny
              go to 3500
            endif
            call ConDensity(pom,ia,0,DenSupl,ich)
            xp(3)=xp3-xpdf(3,ip)
            if(abs(xp(3)).gt.CutOffDistC) then
              m=m+nxny
              go to 3500
            endif
            xp(2)=xmin(2)-xpdf(2,ip)
            n=0
            do iy=1,nx(2)
              if(abs(xp(2)).gt.CutOffDistB) then
                m=m+nx(1)
                n=n+nx(1)
                go to 3350
              endif
              xp(1)=xmin(1)-xpdf(1,ip)
              do ix=1,nx(1)
                n=n+1
                if(abs(xp(1)).gt.CutOffDistA) then
                  m=m+1
                  go to 3250
                endif
                call FeFlowChartEvent(m,is)
                if(is.ne.0) then
                  call FeBudeBreak
                  if(ErrFlag.ne.0) go to 9000
                endif
                call MultM(TrToOrtho,xp,xo,3,3,1)
                dd=VecOrtLen(xo,3)
                if(dd.gt.CutOffDist) go to 3250
                call ConDensity(dd,ia,1,DenSupl,ich)
                do i=1,3
                  WorkMap(n,i)=WorkMap(n,i)+DenSupl(i)*fpdfi
                enddo
3250            xp(1)=xp(1)+dx(1)
              enddo
3350          xp(2)=xp(2)+dx(2)
            enddo
3500      continue
          do i=1,nxny
            pom=1000./(1.+sqrt(WorkMap(i,1)**2+WorkMap(i,2)**2+
     1                         WorkMap(i,3)**2))
            Dmin=min(Dmin,pom)
            Dmax=max(Dmax,pom)
            Table(i)=pom
          enddo
          write(m8,rec=iz+1)(Table(i),i=1,nxny)
          xp3=xp3+dx(3)
        enddo
        write(m8,rec=nx(3)+2) Dmax,Dmin
        n=nx(3)+3
        write(m8,rec=n) npdf
        do i=1,npdf
          n=n+1
          ip=ipor(i)
          write(m8,rec=n) ip,iapdf(ip),ispdf(ip),fpdf(ip),
     1                    (xpdf(j,ip),j=1,3)
        enddo
        call FeFlowChartRemove
      else
        call ConCalcGeneral
      endif
3650  call DefaultFourier
      call ConFouPeaks(m8)
      close(m8)
      call OpenFile(lst,fln(:ifln)//'.cp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call CPMakeOutputStart
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3660  read(m40,FormA,end=9999) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 3660
      read(m40,'(i5)',end=9999) n
      n=0
3670  read(m40,FormA,err=3800,end=3800) Veta
      if(Veta(1:10).ne.'----------') then
        n=n+1
        go to 3670
      endif
      n=n/2
      call FeFlowChartOpen(-1.,-1.,1,n,'Itertion of CP''s',' ',' ')
      rewind m40
3680  read(m40,FormA,end=9999) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 3680
      read(m40,'(i5)',end=9999) n
      m=0
      ncp=0
      n=0
3700  read(m40,FormA,err=3800,end=3800) Veta
      if(Veta(1:10).eq.'----------') go to 3800
      read(Veta,101) xo
      n=n+1
      if(Klic.eq.0) then
        call CopyVek(xo,xp,3)
      else
        call Prevod(1,xo,xp)
      endif
      call CPSearchFromPoint(xp,1)
      read(m40,FormA,err=5000,end=5000) Veta
      call mala(Veta)
      if(index(Veta,'e').ne.0) then
        read(Veta,'(e9.3)',err=5000,end=5000) SumCP
      else
        read(Veta,'(f9.3)',err=5000,end=5000) SumCP
      endif
      call FeFlowChartEvent(m,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) then
          call newln(1)
          write(lst,'(''The search process has been interrupted'')')
          go to 5000
        endif
      endif
      go to 3700
3800  ia=NAtIndFrAll(KPhase)
      do 3820i=1,NAtIndLenAll(KPhase)
        if(ai(ia).le.0.) go to 3820
        if(.not.PointAlreadyPresent(x(1,ia),xn,xcpa,ncp,.05,.false.,
     1                              dpom,j,j,1)) then
          ncp=ncp+1
          call CopyVek(x(1,ia),xcpa(1,ncp),3)
          CPRho(ncp)=-999.
          CPType(ncp)='(3,-3)'
        endif
        ia=ia+1
3820  continue
      call CPMakeOutputFinish(3)
5000  call FeFlowChartRemove
      close(m40)
      go to 9999
9000  close(m8,status='delete')
9999  deallocate(WorkMap,Table)
      call CloseIfOpened(m40)
      return
100   format(i5)
101   format(27x,3f9.6)
      end
