      subroutine ChdGetNeighbours(x1,dmez)
      use Atoms_mod
      use Contour_mod
      parameter (MxNeigh=100)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension x1(3),u(3),ug(3),idist(:),dxy(3)
      allocatable idist
      if(allocated(xpdf)) then
        if(ubound(xpdf,1).eq.MxNeigh) go to 1100
        deallocate(xpdf,popaspdf,idpdf,dpdf,fpdf,ypdf,SelPDF,scpdf,
     1             iapdf,ispdf,ipor)
      endif
      allocate(idist(MxNeigh),xpdf(3,MxNeigh),popaspdf(64,MxNeigh),
     1         idpdf(MxNeigh),dpdf(MxNeigh),fpdf(MxNeigh),
     2         ypdf(3,MxNeigh),SelPDF(MxNeigh),scpdf(MxNeigh),
     3         iapdf(MxNeigh),ispdf(MxNeigh),ipor(MxNeigh))
1100  npdf=0
      dmezi=dmez
      do j=1,NaChd
        do k=1,3
          dxy(k)=XfChd(k,j)-x1(k)
          dxy(k)=dxy(k)-anint(dxy(k))
        enddo
        call od0do1(dxy,dxy,3)
        do i1=-1,0
          u(1)=dxy(1)+i1
          do i2=-1,0
            u(2)=dxy(2)+i2
            do i3=-1,0
              u(3)=dxy(3)+i3
              call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
              d=scalmul(u,ug)
              if(d.ge.dmezi**2) cycle
              d=sqrt(d)
              m=d*1000.
              if(npdf.lt.MxNeigh) then
                npdf=npdf+1
                j1=npdf
              else
                j1=ipor(MxNeigh)
              endif
              do k=1,3
                xpdf(k,j1)=u(k)+x1(k)
              enddo
              idist(j1)=m
              ia=IaChd(j)
              fpdf(j1)=ai(ia)*AtSiteMult(ia)
              iapdf(j1)=ia
              ispdf(j1)=IsChd(j)
              call CopyVek(popasChd(1,j),popaspdf(1,j1),
     1                     (lasmax(ia)-1)**2)
              if(npdf.eq.MxNeigh) then
                call indexx(MxNeigh,idist,ipor)
                dmezi=idist(ipor(npdf))/1000.
              endif
            enddo
          enddo
        enddo
      enddo
      do i=1,npdf
        dpdf(i)=float(idist(i))/1000.
      enddo
      call indexx(npdf,idist,ipor)
      deallocate(idist)
      return
      end
