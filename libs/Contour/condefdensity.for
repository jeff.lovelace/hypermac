      subroutine ConDefDensity(AtPDF,ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xp(3),xdef(3),tp(3)
      integer DensityTypeOld
      character*256 EdwStringQuest
      character*80 Veta
      character*25 :: MenDen(7) =
     1                (/'total density            ',
     2                  'valence density          ',
     3                  'deformation density      ',
     4                  'total density Laplacian  ',
     5                  'valence density Laplacian',
     6                  'inversed gradient vector ',
     7                  'gradient vector          '/)
      character*(*) AtPDF
      integer SbwLnQuest,SbwItemSelQuest,SbwItemPointerQuest,
     1        RolMenuSelectedQuest
      logical lpom,EqWild,CrwLogicQuest,BackToAdvanced
      external ConDefDensityCheck,EM40SelAtomsCheck,FeVoid
      data xdef/0.,1.,.1/,DensityTypeOld/2/
      if(kpdf.eq.2) then
        Veta='Define atom and basic options for p.d.f.'
      else if(kpdf.eq.6) then
        Veta='Define atoms and basic paramaters for charge density maps'
      else
        Veta='Define atoms and basic paramaters for j.p.d.f.'
      endif
      if(mcmax.eq.0) mcmax=1000
      id=NextQuestId()
      call FeQuestTitleMake(id,Veta)
      xqd=WizardLength
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=9
      ild=9
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      nSbwSelect=SbwLastMade
      il=il+2
      if(kpdf.eq.2) then
        nButtAll=0
        nButtRefresh=0
        nEdwInclude=0
        nButtAdvanced=0
        tpom=5.
        Veta='Selected atom:'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        tpom=tpom+FeTxLength(Veta)+5.
        i=NAtIndFrAll(KPhase)
        call FeQuestLblMake(id,tpom,il,Atom(i),'L','N')
        nLblSelectedAtom=LblLastMade
        tpom=tpom+60.
        Veta='%Symmetry code:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwSymmCode=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
      else
        dpom=100.
        xpom=60.
        Veta='%Select all'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='%Refresh'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='Select a%dvanced'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAdvanced=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        tpom=xpom+dpom+20.
        Veta='%Include:'
        xpom=tpom+FeTxLength(Veta)+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwInclude=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
        il=il+1
        call FeQuestLinkaMake(id,il)
        nLblSelectedAtom=0
        nEdwSymmCode=0
      endif
      il=il+1
      ilp=il
      if(kpdf.eq.6) then
        DensityType=DensityTypeOld
        tpom=5.
        Veta='%Calculate:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=150.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolDensityType=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolDensityType,MenDen,7,DensityType+1)
        nCrwDeform=0
        nCrwOldNorm=0
        xpom=5.
        tpom=xpom+CrwgXd+10.
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        if(kpdf.eq.2) then
          Cislo='p.d.f.'
        else
          Cislo='j.p.d.f.'
        endif
        Veta='%Calculate deformation '//Cislo(:idel(Cislo))//' map'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwDeform=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,DensityType.eq.2)
        il=il+1
        Veta='Use %old normalization (JANA98)'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwOldNorm=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,OldNorm)
        nRolDensityType=0
      endif
      il=il+1
      Veta='Include %error map'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwErr=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ErrMapActive)
      tpom=tpom+FeTxLengthUnder(Veta)+5.
      Veta='=>'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblSipka=LblLastMade
      tpom=tpom+FeTxLength(Veta)+5.
      Veta='%Number of iteration steps:'
      xpom=tpom+FeTxLength(Veta)+5.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNSteps=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mcmax,.false.)
      il=il+1
      Veta='%Accuracy limit in %:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,errlev,.false.,.false.)
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
      enddo
      call CloseIfOpened(ln)
      if(NDimI(KPhase).gt.0) then
        il=ilp
        Veta='t(%min):'
        tpom=xqd*.5+80.
        xpom=tpom+FeTxLengthUnder(Veta)+15.
        dpom=70.
        do i=1,3
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            Veta='t(ma%x):'
            nEdwTmin=EdwLastMade
          else if(i.eq.2) then
            Veta='t(del%ta):'
          else if(i.eq.3) then
          endif
          call FeQuestRealEdwOpen(EdwLastMade,xdef(i),.false.,.false.)
          il=il+1
        enddo
        NSubs=1
      else
        NSubs=1
        nEdwTmin=0
      endif
      call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
      BackToAdvanced=.false.
      go to 1255
1250  call CloseIfOpened(SbwLnQuest(nSbwSelect))
1255  if(kpdf.eq.2) then
        call FeQuestSbwMenuOpen(nSbwSelect,fln(:ifln)//'_atoms.tmp')
      else
        k=0
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          k=k+1
          if(AtBrat(i)) then
            j=1
          else
            j=0
          endif
          call FeQuestSetSbwItemSel(k,nSbwSelect,j)
        enddo
        call FeQuestSbwSelectOpen(nSbwSelect,fln(:ifln)//'_atoms.tmp')
      endif
1500  if(CrwLogicQuest(nCrwErr)) then
        call FeQuestIntEdwOpen(nEdwNSteps,mcmax,.false.)
        call FeQuestRealEdwOpen(nEdwLimit,errlev,.false.,.false.)
      else
        call FeQuestIntFromEdw(nEdwNSteps,mcmax)
        call FeQuestRealFromEdw(nEdwLimit,errlev)
        call FeQuestEdwDisable(nEdwNSteps)
        call FeQuestEdwDisable(nEdwLimit)
      endif
      if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      if(kpdf.eq.2) then
        call FeQuestEventWithCheck(id,ich,ConDefDensityCheck,FeVoid)
      else
        call FeQuestEvent(id,ich)
      endif
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh)) then
        if(CheckNumber.eq.nButtAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        call SetLogicalArrayTo(AtBrat(NAtIndFrAll(KPhase)),
     1                         NAtIndLenAll(KPhase),lpom)
        go to 1250
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          AtBrat(i)=EqWild(Atom(i),Veta,.false.)
        enddo
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        WizardMode=.false.
        idp=NextQuestId()
        xqdp=300.
        il=6
        jk=2
        nRolMenuMol=0
        nButtInclMol=0
        nButtExclMol=0
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,2
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,
     1                                AtType(1,KPhase),
     2                                NAtFormula(KPhase),0)
            else
              nRolMenuMol=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,MolMenu,NMolMenu,
     1                                0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              else
                nButtInclMol=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              else
                nButtExclMol=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,EM40SelAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(EqWild(Atom(i),Veta,.false.)) AtBrat(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
          isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(isf(i).eq.isfp) AtBrat(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtType(1,KPhase),
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then

          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        WizardMode=.true.
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1250
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwErr) then
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSymmCode)
     1  then
        j=SbwItemPointerQuest(nSbwSelect)+NAtIndFrAll(KPhase)-1
        AtPDF=Atom(j)
        Veta=EdwStringQuest(nEdwSymmCode)
        if(Veta.ne.' ') AtPDF=AtPDF(:idel(AtPDF))//'#'//
     1                        Veta(:idel(Veta))
        call atsymi(AtPDF,iapdf(1),xpdf(1,1),xp,tp,ispdf(1),k,*1700)
        go to 1500
1700    EventType=EventEdw
        EventNumber=nEdwSymmCode
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        ErrMapActive=CrwLogicQuest(nCrwErr)
        if(ErrMapActive) then
          call FeQuestIntFromEdw(nEdwNSteps,mcmax)
          call FeQuestRealFromEdw(nEdwLimit,errlev)
        endif
        if(nCrwDeform.gt.0) then
          if(CrwLogicQuest(nCrwDeform)) then
            DensityType=2
          else
            DensityType=1
          endif
        else
          DensityType=RolMenuSelectedQuest(nRolDensityType)-1
          DensityTypeOld=DensityType
        endif
        j=SbwItemPointerQuest(nSbwSelect)+NAtIndFrAll(KPhase)-1
        NAtBrat=0
        if(kpdf.eq.2) then
          call SetLogicalArrayTo(AtBrat(NAtIndFrAll(KPhase)),
     1                           NAtIndLenAll(KPhase),.false.)
          AtBrat(j)=.true.
          AtPDF=Atom(j)
          itfi=max(itf(j)-1,1)
          do m=1,5
            AtBratAnharm(m,j)=m.le.itfi
          enddo
          Veta=EdwStringQuest(nEdwSymmCode)
          if(Veta.ne.' ') AtPDF=AtPDF(:idel(AtPDF))//'#'//
     1                          Veta(:idel(Veta))
        else
          k=0
          do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            k=k+1
            AtBrat(i)=SbwItemSelQuest(k,nSbwSelect).eq.1
            if(AtBrat(i)) then
              j=0
              NAtBrat=NAtBrat+1
              itfi=max(itf(i)-1,1)
              do m=1,5
                AtBratAnharm(m,i)=m.le.itfi
              enddo
            endif
          enddo
          if(j.ne.0) AtBrat(j)=.true.
        endif
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwTmin
          do i=1,3
            call FeQuestRealFromEdw(nEdw,xdef(i))
            nEdw=nEdw+1
          enddo
          tpdfp=xdef(1)
          tpdfk=xdef(2)
          tpdfd=xdef(3)
          ntpdf=nint((tpdfk-tpdfp)/tpdfd)+1
        else
          tpdfp=0.
          tpdfk=0.
          ntpdf=1
        endif
      else
        go to 9999
      endif
      if(kpdf.eq.2) then
        call atsymi(AtPDF,iapdf(1),xpdf(1,1),xp,tp,ispdf(1),k,*9000)
        ipor(1)=1
        npdf=1
        dpdf(1)=0.
        xp(1)=0.
        call specpos(xpdf(1,1),xp,0,1,.005,l)
        fpdf(1)=ai(iapdf(1))*float(l)
      endif
      go to 9999
9000  ich=-1
9999  return
      end
