      subroutine ConUpdateMapsButtons
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer ButtonStateQuest
      do j=1,NDim(KPhase)-2
        if(nxdraw(j).gt.nxfrom(j)) then
          call FeQuestButtonOff(nButtMapaMinus)
          go to 2010
        endif
      enddo
      call FeQuestButtonDisable(nButtMapaMinus)
2010  do j=1,NDim(KPhase)-2
        if(nxdraw(j).lt.nxto(j)) then
          call FeQuestButtonOff(nButtMapaPlus)
          go to 2040
        endif
      enddo
      call FeQuestButtonDisable(nButtMapaPlus)
2040  if(ButtonStateQuest(nButtMapaPlus ).eq.ButtonOff.or.
     1   ButtonStateQuest(nButtMapaMinus).eq.ButtonOff) then
        call FeQuestButtonOff(nButtGoto)
      else
        call FeQuestButtonDisable(nButtGoto)
      endif
      if(DrawAtN.gt.0) then
        call FeQuestButtonOff(nButtAtomsDraw)
      else
        call FeQuestButtonDisable(nButtAtomsDraw)
      endif
      if(xyzmap.and.NAtCalc.gt.0) then
        call FeQuestButtonOff(nButtAtomsFill)
      else
        call FeQuestButtonDisable(nButtAtomsFill)
      endif
      if(MapExists) then
c        if(Obecny) then
          i=ButtonOff
c        else
c          i=ButtonDisabled
c        endif
      else
        i=ButtonDisabled
      endif
      call FeButtonOpen(nButtRun3dMaps,i)
      return
      end
