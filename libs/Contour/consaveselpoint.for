      subroutine ConSaveSelPoint(xs)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xs(*)
      character*256 EdwStringQuest
      character*80 Veta
      logical EqIgCase,FeYesNoHeader
      ich=0
      id=NextQuestId()
      xqd=320.
      il=1
      call FeQuestCreate(id,-1.,YBottomMessage,xqd,il,' ',1,LightGray,0,
     1                   0)
      il=1
      Veta='Define an identification label of the point:'
      tpom=5.
      xpom=tpom+FeTxLength(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEwdLabel=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      ypom=YCenBasWin-105.
      kam=0
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        Veta=EdwStringQuest(nEwdLabel)
        if(Veta.eq.' ') then
          call FeChybne(-1.,ypom,'the label cannot be an empty string.',
     1                  ' ',SeriousError)
          go to 1600
        else
          do i=1,NSavedPoints
            if(EqIgCase(Veta,StSavedPoint(i))) then
              NInfo=1
              TextInfo(1)='The label "'//Veta(:idel(Veta))//
     1                    '" has been already used.'
              if(FeYesNoHeader(-1.,ypom,'Do you want to overwrite it?',
     1                        0)) then
                kam=i
              else
                go to 1600
              endif
            endif
          enddo
        endif
        QuestCheck(id)=0
        go to 1500
1600    call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        NewPoints=.true.
        if(kam.le.0) then
          if(NSavedPoints.ge.MaxSavedPoints)
     1      call ReallocateSavedPoints(MaxSavedPoints+10)
          NSavedPoints=NSavedPoints+1
          kam=NSavedPoints
          StSavedPoint(kam)=Veta
        endif
        call CopyVek(xs,XSavedPoint(1,kam),3)
      endif
      call FeQuestRemove(id)
      return
      end
