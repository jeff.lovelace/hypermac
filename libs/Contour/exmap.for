      subroutine exmap(InputMap,xx,Value,zapis,x4p)
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xx(*),xp(6),xpt(6),xd(3),n(3),x1(6),x2(6),x4p(3)
      logical zapis,EqMod1
      real InputMap(*)
      NDimp=NDim(KPhase)
      if(nonModulated(KPhase).or..not.SatelityByly) NDimp=3
      do i=1,NSymmN(KPhase)
        call multm(rm6(1,i,nsubs,KPhase),xx,x1,NDim(KPhase),
     1             NDim(KPhase),1)
        do k=1,ncst
          if(k.eq.1) then
            call CopyVek(x1,x2,NDim(KPhase))
          else
            call RealVectorToOpposite(x1,x2,NDim(KPhase))
          endif
          if(Mapa.gt.3.or.Mapa.lt.0)
     1      call AddVek(x2,s6(1,i,nsubs,KPhase),x2,NDim(KPhase))
          do 1600l=1,NLattVec(KPhase)
            call AddVek(x2,vt6(1,l,nsubs,KPhase),xp,NDim(KPhase))
            call Multm(tm,xp,xpt,NDim(KPhase),NDim(KPhase),1)
            do j=1,NDimp
1100          if(xpt(j).ge.xmin(j)) go to 1200
              xpt(j)=xpt(j)+1.
              go to 1100
1200          if(xpt(j).lt.xmax(j)+dxp(j)) go to 1300
              xpt(j)=xpt(j)-1.
              go to 1200
1300          if(j.gt.3) then
                if(.not.EqMod1(xpt(j),x4p(j-3),.001)) go to 1600
              else
                if(xpt(j).lt.xmin(j)) go to 1600
              endif
            enddo
            do j=1,3
              xd(j)=(xpt(j)-xmin(j))/dx(j)+1.
              n(j)=xd(j)
              if(j.le.3) n(j)=min(n(j),nx(j))
              xd(j)=xd(j)-float(n(j))
            enddo
            if(nmapa.eq.nx3) then
              go to 2020
            else
              go to 2000
            endif
1600      continue
        enddo
      enddo
      go to 9999
2000  continue
2020  i=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
      i1=i+1
      inx=i+nx(1)
      inx1=inx+1
      if(n(1).eq.nx(1)) then
        i1  =i1  -nx(1)
        inx1=inx1-nx(1)
      endif
      if(n(2).eq.nx(2)) then
        inx =inx -nxny
        inx1=inx1-nxny
      endif
      f000=InputMap(i)
      f100=InputMap(i1)
      f010=InputMap(inx)
      f110=InputMap(inx1)
      i   =i   +nxny
      i1  =i1  +nxny
      inx =inx +nxny
      inx1=inx1+nxny
      f001=InputMap(i)
      f101=InputMap(i1)
      f011=InputMap(inx)
      f111=InputMap(inx1)
      f00=(f100-f000)*xd(1)+f000
      f01=(f101-f001)*xd(1)+f001
      f10=(f110-f010)*xd(1)+f010
      f11=(f111-f011)*xd(1)+f011
      f0=(f10-f00)*xd(2)+f00
      f1=(f11-f01)*xd(2)+f01
      Value=(f1-f0)*xd(3)+f0
      zapis=.true.
9999  return
      end
