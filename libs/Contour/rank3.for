      function rank3(c)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension c(10)
      rank3=w1*(wm3p1*c(1) +wmp2*c(4)+wmp3*c(6))+
     1      w2*(wm3p2*c(7) +wmp1*c(2)+wmp3*c(9))+
     2      w3*(wm3p3*c(10)+wmp1*c(3)+wmp2*c(8)+w12*c(5))
      return
      end
