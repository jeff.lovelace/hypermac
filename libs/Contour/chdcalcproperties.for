      subroutine ChdCalcProperties(RhoArr,rr,vp,np)
      use Basic_mod
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension  RhoArr(*),vp(nprops,*),rr(*)
      data thr1,thr2/1e-3,2e-3/
      kk=0
      ik=1
      zchga=AtNum(isf(IAtInt),KPhase)
      do i=1,np
        if(nFlowInt.ge.0) then
          nFlowInt=nFlowInt-1
          call FeFlowChartEvent(nFlowInt,ie)
          if(ie.ne.0) then
            ErrFlag=1
            go to 9999
          endif
        endif
        vp(1,i)=RhoArr(kk+1)
        vp(4,i)=-.25*(RhoArr(kk+5)+RhoArr(kk+6)+RhoArr(kk+7))
        rx=rr(ik)  -rcla(1,IAtInt)
        ry=rr(ik+1)-rcla(2,IAtInt)
        rz=rr(ik+2)-rcla(3,IAtInt)
        r2=rx*rx+ry*ry+rz*rz
        r=sqrt(r2)
        vp(15,i)=vp(1 ,i)/r
        vp(16,i)=vp(1 ,i)*r
        vp(17,i)=vp(16,i)*r
        vp(18,i)=vp(17,i)*r
        vp(19,i)=vp(18,i)*r
        grhox=RhoArr(kk+2)
        grhoy=RhoArr(kk+3)
        grhoz=RhoArr(kk+4)
        vp(21,i)=(rx*grhox+ry*grhoy+rz*grhoz)
        vp(20,i)=vp(21,i)/r
        vp(22,i)=vp(21,i)*r
        vp(23,i)=vp(21,i)*r2
        if(vp(1,i).ge.thr1) then
          vp(24,i)=1.
          vp(25,i)=vp(1,i)
        else
          vp(24,i)=0.
          vp(25,i)=0.
        endif
        if(vp(1,i).ge.thr2) then
          vp(26,i)=1.
          vp(27,i)=vp(1,i)
        else
          vp(26,i)=0.
          vp(27,i)=0.
        endif
        vp(28,i)=1.
        vp(29,i)=-vp(1,i)*rx
        vp(30,i)=-vp(1,i)*ry
        vp(31,i)=-vp(1,i)*rz
        vp(32,i)=-vp(1,i)*(3.*rx*rx-r2)
        vp(33,i)=-vp(1,i)*(3.*rx*ry)
        vp(34,i)=-vp(1,i)*(3.*rx*rz)
        vp(35,i)=-vp(1,i)*(3.*ry*ry-r2)
        vp(36,i)=-vp(1,i)*(3.*ry*rz)
        vp(37,i)=-vp(1,i)*(3.*rz*rz-r2)
        if(vp(1,i).gt.0.) then
          vp(38,i)=-vp(1,i)*log(vp(1,i))
        else
          vp(38,i)=0.
        endif
        vp(7 ,i)=-zchga*vp(1,i)/r
        vp(9 ,i)=(zchga*rx*vp(1,i))/(r2*r)
        vp(10,i)=(zchga*ry*vp(1,i))/(r2*r)
        vp(11,i)=(zchga*rz*vp(1,i))/(r2*r)
        vp(8,i)=0.
        vp(12,i)=0.
        vp(13,i)=0.
        vp(14,i)=0.
        ik=ik+3
        kk=kk+10
      enddo
9999  return
      end
