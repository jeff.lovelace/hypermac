      subroutine ConSetPointCurve(String,n,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) String
      dimension xdo(3),xp(3)
      if(String.eq.' ') go to 9000
      call CtiAt(String,xdo,ich)
      if(ich.gt.0) go to 9999
      if(ich.eq.-1) then
        AtCurve(n)=String
      else
        AtCurve(n)=' '
      endif
      if(XTypeCurve.eq.1.and.AtCurve(n).eq.' ') then
!        xdo(3)=0.
        call CopyVek(xdo,XLocCurve(1,n),3)
        call multm(O2F,XLocCurve(1,n),xp,3,3,1)
        do j=1,3
          xp(j)=min(xp(j),xmax(j))
          xp(j)=max(xp(j),xmin(j))
        enddo
        call multm(F2O,xp,XLocCurve(1,n),3,3,1)
        call prevod(1,xp,XFractCurve(1,n))
      else
        call CopyVek(xdo,XFractCurve(1,n),3)
        call prevod(0,XFractCurve(1,n),xp)
        do j=1,3
          xp(j)=min(xp(j),xmax(j))
          xp(j)=max(xp(j),xmin(j))
        enddo
        call multm(F2O,xp,XLocCurve(1,n),3,3,1)
      endif
9000  ich=0
9999  return
      end
