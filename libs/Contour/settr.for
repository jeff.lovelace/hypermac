      subroutine SetTr
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      c=1.
      alfa=90.
      bett=90.
      if(obecny) then
        do i=1,3
          write(cx(i),'(''e'',i1)') i
        enddo
        if(tmapy) then
          do i=4,6
            cx(i)=smbt(i-3)
          enddo
        else
          do i=4,6
            cx(i)=smbx6(i)
          enddo
        endif
        a=1.
        b=1.
        gama=90.
        read(m8,rec=1) nx,nxny,nmap,(xmin(j),xmax(j),j=1,6),dx,
     1                 (j,i=1,6),mapa,nsubs,SatelityByly,
     2                 nonModulated(KPhase)
      else
        i=iorien(1)
        j=iorien(2)
        k=iorien(3)
        if(i.gt.3.or.j.gt.3) then
          call ConSetCellParCon
          a=sqrt(MetTens6(i+(i-1)*NDim(KPhase),NSubs,KPhase))
          b=sqrt(MetTens6(j+(j-1)*NDim(KPhase),NSubs,KPhase))
          c=sqrt(MetTens6(k+(k-1)*NDim(KPhase),NSubs,KPhase))
          pom=MetTens6(i+(j-1)*NDim(KPhase),NSubs,KPhase)/(a*b)
          gama=acos(pom)
          if(i.gt.3) then
            gaman=atan(a/ee(i-3)*tan(gama))
            a=ee(i-3)
            b=b*sin(gama)/sin(gaman)
            gama=gaman
          endif
          if(j.gt.3) then
            gaman=atan(b/ee(j-3)*tan(gama))
            b=ee(j-3)
            a=a*sin(gama)/sin(gaman)
            gama=gaman
          endif
          gama=gama/ToRad
        else
          call ConSetCellParCon3
          a=sqrt(MetTens(i+(i-1)*3,NSubs,KPhase))
          b=sqrt(MetTens(j+(j-1)*3,NSubs,KPhase))
          c=sqrt(MetTens(k+(k-1)*3,NSubs,KPhase))
          pom=MetTens(i+(j-1)*3,NSubs,KPhase)/(a*b)
          gama=acos(pom)/ToRad
        endif
      endif
      csa=cos(torad*alfa)
      csb=cos(torad*bett)
      csg=cos(torad*gama)
      sng=sin(torad*gama)
      volume=sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      if(xymap) then
        F2O(1)=a
        F2O(2)=0.
        F2O(4)=b*csg
        F2O(5)=b*sng
      else
        F2O(1)=a*sng
        F2O(4)=0.
        if(ReverseX4) then
          F2O(2)=-a*csg
          F2O(5)=-b
        else
          F2O(2)=a*csg
          F2O(5)=b
        endif
      endif
      F2O(3)=0.
      F2O(6)=0.
      F2O(7)=c*csb
      F2O(8)=c*(csa-csb*csg)/sng
      F2O(9)=c*volume/sng
      call matinv(F2O,O2F,pom,3)
      call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
      return
      end
