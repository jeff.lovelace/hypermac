      subroutine ConDrawAtoms(zrez)
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      character*80 Veta,t80
      integer Color1,Color2,DrawAtFlag(NMaxDraw)
      logical InsideXYPlot,EqIgCase,ExistFile,EqRV
      dimension xp(6),xpp(6),xs(3),occ(2000),xd(3,2000),uxi(3,mxw),
     1          uyi(3,mxw),x4dif(3),tdif(3),zrez(4),smp(36),gddx(3),
     2          x40(3),dx4(3),nx4(3),GammaIntInv(9),xoo(3),x0(3),y0(3),
     3          y1(3),y2(3),y3(3),shj(3),uz(3),
     3          xvf(3,NMaxDraw),xvo(3,NMaxDraw),ov(NMaxDraw),
     4          isfa(NMaxDraw),rmp(9),rm6p(36),occp(2000)
      allocate(TypicalDistUse(NAtFormula(KPhase),NAtFormula(KPhase)))
      do i=1,NAtFormula(KPhase)
        do j=i,NAtFormula(KPhase)
          if(TypicalDist(i,j,KPhase).gt.0.) then
            TypicalDistUse(i,j)=TypicalDist(i,j,KPhase)
          else
            TypicalDistUse(i,j)=AtRadius(i,KPhase)+
     1                          AtRadius(j,KPhase)
          endif
          TypicalDistUse(j,i)=TypicalDistUse(i,j)
        enddo
      enddo
      xp (3)=0.
      xpp(3)=0.
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      n=0
      call SetIntArrayTo(DrawAtFlag,DrawAtN,0)
      if(xymap) then
        Veta=fln(:ifln)//'_dratoms_grd.tmp'
        call DeleteFile(Veta)
        ln1=NextLogicNumber()
        call OpenFile(ln1,Veta,'formatted','unknown')
        Veta=fln(:ifln)//'_dratoms_vesta.tmp'
        call DeleteFile(Veta)
        ln2=NextLogicNumber()
        call OpenFile(ln2,Veta,'formatted','unknown')
      else
        ln1=0
        ln2=0
      endif
      do 3000i=1,DrawAtN
        if(DrawAtSkip(i)) go to 3000
        call atsymi(DrawAtName(i),ia,xs,x4dif,tdif,isym,ich,*3000)
        isfa(i)=isf(ia)
        if(DrawAtColor(i).eq.0) then
          Color1=AtColor(isfa(i),KPhase)
        else
          Color1=ColorNumbers(DrawAtColor(i))
        endif
        if(NDimI(KPhase).gt.0) then
          isw=ISwSymm(isym,iswa(ia),KPhase)
          call DistSetComp(isw,NSubs)
          if(isw.eq.iswa(ia)) then
            call CopyMat(rm (1,isym,isw,KPhase),rmp ,3)
            call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
          else
            call multm(zv(1,isw,KPhase),zvi(1,iswa(ia),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,isym,iswa(ia),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rm6p,rmp,NDim(KPhase))
          endif
          ksw=kswa(ia)
          call GetGammaIntInv(rm6p,GammaIntInv)
          do l=1,KModA(2,ia)
            call multm(rmp,ux(1,l,ia),uxi(1,l),3,3,1)
            if(l.ne.KModA(2,ia).or.kfa(2,ia).eq.0) then
              call multm(rmp,uy(1,l,ia),uyi(1,l),3,3,1)
            else
              call CopyVek(uy(1,l,ia),uyi(1,l),3)
            endif
          enddo
          if(xymap) then
            do j=3,NDim(KPhase)
              k=iorien(j)-3
              if(k.gt.0) xpp(k)=zrez(j-2)
            enddo
            if(tmapy) then
              call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
              call AddVek(tdif,xpp,xpp,NDimI(KPhase))
            else
              call AddVek(x4dif,xpp,xpp,NDimI(KPhase))
            endif
            call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
            do j=1,NDimI(KPhase)
              nx4(j)=1
              dx4(j)=0.
              if(.not.tmapy) x40(j)=0.
            enddo
            if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
              call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1          ay(1,ia),KModA(1,ia),KFA(1,ia),GammaIntInv)
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1           uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2           GammaIntInv)
                 if(occp(1).le.0.) occ(1)=0.
              endif
            else
              call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                           ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                           ax(KModA(1,ia),ia),a0(ia)*.5,
     3                           GammaIntInv,TypeModFun(ia))
            endif
            if(TypeModFun(ia).le.1) then
              call MakePosMod(xpp,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                        KFA(2,ia),GammaIntInv)
            else
              call MakePosModPol(xpp,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                           ax(KModA(1,ia),ia),a0(ia)*.5,
     2                           GammaIntInv,TypeModFun(ia))
            endif
            if((occ(1).lt.ConOccLim.and.UseOccupancyCutOff).or.
     1          occ(1).le.0.01) go to 3000
            call AddVek(xs,xpp,xs,3)
          endif
        else
          occ(1)=1.
        endif
        if(xymap) then
          call prevod(0,xs,xp)
          j=index(DrawAtName(i),'#')-1
          if(j.le.0) j=idel(DrawAtName(i))
          Veta=DrawAtName(i)(:j)
          if(Obecny) then
            do j=1,3
              xo(j)=xp(j)+ShiftPlane(j)-ScopePlane(j)*.5
            enddo
            write(ln1,100) Veta(:6),xo,' ATOM'
            write(ln2,101) ia,nint(AtNum(isfa(i),KPhase)),
     1                     (xo(j)+ScopePlane(j)*.5,j=1,3)
          else
            write(ln1,100) Veta(:6),
     1                     ((xs(iorien(j))-(xmax(j)-xmin(j))*.5)*
     1                      CellParCon(j),j=1,3)
            write(ln2,101) ia,nint(AtNum(isfa(i),KPhase)),(xs(j),j=1,3)
          endif
          call FeXf2X(xp,xo)
          if(InsideXYPlot(xo(1),xo(2))) then
            if(.not.DrawAtSkip(i)) then
              if(ConDrawDepth.gt.0..and..not.smapy) then
                xpp(1)=xp(1)
                xpp(2)=xp(2)
                xpp(3)=zrez(1)
                call FeXf2X(xpp,xoo)
                if(abs(xo(3)-xoo(3)).gt.ConDrawDepth) go to 3000
              endif
              DrawAtFlag(i)=1
              call CopyVek(xs,xvf(1,i),3)
              call CopyVek(xo,xvo(1,i),3)
              ov(i)=occ(1)
              go to 3000
            endif
          endif
        else
          do j=3,NDim(KPhase)
            k=iorien(j)
            xpp(k)=zrez(j-2)
          enddo
          ko1=iorien(1)
          ko2=iorien(2)
          if(ko1.gt.3) then
            kot=ko1
          else
            kot=ko2
          endif
          x4min= 99999.
          x4max=-99999.
          do j=0,1
            if(j.eq.0) then
              xpp(ko1)=xmin(1)
            else
              xpp(ko1)=xmax(1)
            endif
            do k=0,1
              if(k.eq.0) then
                xpp(ko2)=xmin(2)
              else
                xpp(ko2)=xmax(2)
              endif
              call multm(WPr,xpp,xp,NDim(KPhase),NDim(KPhase),1)
              x4min=min(x4min,xp(kot))
              x4max=max(x4max,xp(kot))
            enddo
          enddo
          dx4m=(x4max-x4min)*.001
          x4m=x4min-(x4max-x4min)*.5
          do j=3,NDim(KPhase)
            k=iorien(j)-3
            if(k.gt.0) xpp(k)=zrez(j-2)
          enddo
          xpp(kot-3)=x4m
          call AddVek(xpp,x4dif,xpp,NDimI(KPhase))
          if(kmol(ia).ne.0) then
            call qbyx(xs,xp,isw)
            do j=1,NDimI(KPhase)
              xpp(j)=xpp(j)-xp(j)+qcnt(j,ia)
            enddo
          endif
          call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
          call SetRealArrayTo(dx4,NDimI(KPhase),0.)
          call SetRealArrayTo(x40,NDimI(KPhase),0.)
          call SetRealArrayTo(dx4,NDimI(KPhase),0.)
          call SetIntArrayTo(nx4,NDimI(KPhase),1)
          dx4(kot-3)=dx4m
          nx4(kot-3)=2000
          nx4m=2000
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1        ay(1,ia),KModA(1,ia),KFA(1,ia),GammaIntInv)
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
              call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1         uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2         GammaIntInv)
               do j=1,nx4m
                 if(occp(j).le.0.) occ(j)=0.
               enddo
            endif
          else
            call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv,TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakePosMod(xd,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                      KFA(2,ia),GammaIntInv)
          else
            call MakePosModPol(xd,x40,xp,nx4,dx4,uxi,uyi,KModA(2,ia),
     1                         ax(KModA(1,ia),ia),a0(ia)*.5,GammaIntInv,
     2                         TypeModFun(ia))
          endif
          Kolik=0
          do j=1,nx4m
            call AddVek(xs,xd(1,j),xp,3)
            call qbyx(xd(1,j),xp(4),isw)
            xp(kot)=x4m+xp(kot)
            call multm(WPrI,xp,xpp,NDim(KPhase),NDim(KPhase),1)
            call SetRealArrayTo(xp,NDim(KPhase),0.)
            xp(1)=xpp(ko1)
            xp(2)=xpp(ko2)
            if(xp(1).lt.xmin(1).or.xp(1).gt.xmax(1).or.
     1         xp(2).lt.xmin(2).or.xp(2).gt.xmax(2)) go to 2300
            if((occ(j).lt.ConOccLim.and.UseOccupancyCutOff).or.
     1          occ(j).le.0.01) go to 2300
            call FeXf2X(xp,xo)
            xm=xo(1)
            ym=xo(2)
            kolik=kolik+1
            xpole(kolik)=xm
            ypole(kolik)=ym
            if(kolik.ge.mxc) then
              call FePolyLineT(kolik,xpole,ypole,Color1)
              kolik=1
              xpole(kolik)=xm
              ypole(kolik)=ym
            endif
            go to 2350
2300        if(kolik.ge.1) then
              if(kolik.ge.2) call FePolyLineT(kolik,xpole,ypole,Color1)
              kolik=0
            endif
2350        x4m=x4m+dx4m
          enddo
2500      if(kolik.ge.2) call FePolyLineT(kolik,xpole,ypole,Color1)
        endif
3000  continue
      if(xymap) then
        do i=1,DrawAtN
          if(DrawAtFlag(i).le.0) cycle
          if(DrawAtColor(i).eq.0) then
            Color1=AtColor(isfa(i),KPhase)
          else
            Color1=ColorNumbers(DrawAtColor(i))
          endif
          do j=1,i-1
            if(DrawAtFlag(j).le.0) cycle
            if(ConSkipSame.gt.0.and.isfa(i).eq.isfa(j)) cycle
            do k=1,3
              xp(k)=xvf(k,i)-xvf(k,j)
            enddo
            call Multm(MetTens(1,1,KPhase),xp,xpp,3,3,1)
            d=sqrt(scalmul(xp,xpp))
            if(ConDmaxExpl.gt.0) then
              if(d.gt.DrawAtBondLim(i).or.d.gt.DrawAtBondLim(j)) cycle
            else
              if(d.gt.TypicalDistUse(isfa(i),isfa(j))*
     1                (1.+.01*ConExpDmax)) cycle
            endif
            if(ConDrawStyle.eq.DrawStyleBallAndStick) then
              xu(1)=xvo(1,i)
              yu(1)=xvo(2,i)
              xu(2)=xvo(1,j)
              yu(2)=xvo(2,j)
              call FePolyLineT(2,xu,yu,White)
              call FeLineType(NormalLine)
            else
              xu(1)=xvo(1,i)
              yu(1)=xvo(2,i)
              xu(2)=.5*(xvo(1,i)+xvo(1,j))
              yu(2)=.5*(xvo(2,i)+xvo(2,j))
              call FePolyLineT(2,xu,yu,Color1)
              call FeLineType(NormalLine)
              xu(1)=xvo(1,j)
              yu(1)=xvo(2,j)
              if(DrawAtColor(j).eq.0) then
                Color2=AtColor(isfa(j),KPhase)
              else
                Color2=ColorNumbers(DrawAtColor(j))
              endif
              call FePolyLineT(2,xu,yu,Color2)
              call FeLineType(NormalLine)
            endif
          enddo
        enddo
        if(ConDrawStyle.eq.DrawStyleBallAndStick) then
          do i=1,DrawAtN
            if(DrawAtFlag(i).gt.0) then
              if(.not.UseOccupancyCutOff)
     1          Rad=min(.2*X2XoRatio,10.)*ov(i)
              if(DrawAtColor(i).eq.0) then
                Color1=AtColor(isfa(i),KPhase)
              else
                Color1=ColorNumbers(DrawAtColor(i))
              endif
              call FeCircle(xvo(1,i),xvo(2,i),Rad,Color1)
            endif
          enddo
        endif
      endif
      Veta=fln(:ifln)//'.cp'
      if(Mapa.eq.6.and.ExistFile(Veta).and.ConDrawCP.gt.0.and.Obecny)
     1  then
        ir=2
        do i=1,3
          xpp(i)=(xmaxa(i,ir)+xmina(i,ir))*.5
        enddo
        dmez=0.
        do i1=-1,1,2
          ddx(1)=(xmaxa(1,ir)-xpp(1))*float(i1)
          do i2=-1,1,2
            ddx(2)=(xmaxa(2,ir)-xpp(2))*float(i2)
            do i3=-1,1,2
              ddx(3)=(xmaxa(3,ir)-xpp(3))*float(i3)
              dd=sqrt(ddx(1)**2+ddx(2)**2+ddx(3)**2)
              dmez=max(dd,dmez)
            enddo
          enddo
        enddo
        call prevod(1,xpp,x0)
        call bunka(x0,y0,0)
        jcellxm=anint(dmez*rcp(1,nsubs,KPhase))+1
        jcellym=anint(dmez*rcp(2,nsubs,KPhase))+1
        jcellzm=anint(dmez*rcp(3,nsubs,KPhase))+1
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
        mm=0
4000    read(ln,FormA,end=4500) Veta
        k=0
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'(3,-1)')) go to 4000
        call StToReal(Veta,k,y0,3,.false.,ich)
        if(ich.ne.0) go to 4000
        nn=0
        do jsym=1,NSymmN(KPhase)
          call multm(rm6(1,jsym,nsubs,KPhase),y0,y1,NDim(KPhase),
     1               NDim(KPhase),1)
          do l=1,3
            y1(l)=y1(l)+s6(l,jsym,nsubs,KPhase)
          enddo
          do jcenter=1,NLattVec(KPhase)
            do l=1,3
              y2(l)=y1(l)+vt6(l,jcenter,nsubs,KPhase)
            enddo
            call bunka(y2,xpp,1)
            do jcellx=-jcellxm,jcellxm
              y3(1)=y2(1)+float(jcellx)
              do jcelly=-jcellym,jcellym
                y3(2)=y2(2)+float(jcelly)
                do jcellz=-jcellzm,jcellzm
                  y3(3)=y2(3)+float(jcellz)
                  u=y3-x0
                  call multm(MetTens(1,nsubs,KPhase),u,ug,3,3,1)
                  d=sqrt(scalmul(u,ug))
                  if(d.ge.dmez) cycle
                  call prevod(0,y3,xp)
                  call FeXf2X(xp,xo)
                  if(InsideXYPlot(xo(1),xo(2))) then
                    if(ConDrawDepth.gt.0..and..not.smapy) then
                      xpp(1)=xp(1)
                      xpp(2)=xp(2)
                      xpp(3)=zrez(1)
                      call FeXf2X(xpp,xoo)
                      if(abs(xo(3)-xoo(3)).gt.ConDrawDepth) cycle
                      j=0
                      do i=1,nn
                        if(EqRV(xo,xvo(1,i),3,.001)) then
                          j=1
                          exit
                        endif
                      enddo
                      if(j.eq.0) then
                        nn=nn+1
                        mm=mm+1
                        write(Cislo,'(''CP'',i5)') mm
                        call Zhusti(Cislo)
                        write(ln1,100) Cislo(:6),xp(1:3),' CP'
                        xvo(1:3,nn)=xo(1:3)
                      endif
                    endif
                  endif
                enddo
              enddo
            enddo
          enddo
        enddo
        do i=1,nn
          call FeCircle(xvo(1,i),xvo(2,i),8.,White)
        enddo
        go to 4000
4500    call CloseIfOpened(ln)
      endif
      call ResetIgnoreW
      call ResetIgnoreE
9999  if(xymap) then
        call CloseIfOpened(ln1)
        call CloseIfOpened(ln2)
      endif
      if(allocated(TypicalDistUse)) deallocate(TypicalDistUse)
      return
100   format(a6,3f10.5,a)
101   format(2i3,3f10.5)
      end
