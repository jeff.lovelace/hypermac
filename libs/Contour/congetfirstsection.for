      integer function ConGetFirstSection(nn,nd,n)
      dimension nn(n),nd(n)
      integer RecPack
      j=0
      do i=1,n
        if(nd(i).gt.1) then
          j=i
          exit
        endif
      enddo
      if(j.gt.0) then
        i=nn(j)
        nn(j)=1
      endif
      ConGetFirstSection=RecPack(nn,nd,n)
      if(j.gt.0) nn(j)=i
      return
      end
