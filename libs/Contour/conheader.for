      subroutine ConHeader
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      data t80/' '/
      if(irec.le.0) go to 9999
      write(t80,'(i6,''$'',i6)') irec,nmap
      call zhusti(t80)
      i=index(t80,'$')
      t80=t80(1:i-1)//' of '//t80(i+1:idel(t80))
      if(kpdf.eq.0) then
        t80='Fourier maps regular section : '//t80(1:idel(t80))
      else if(kpdf.eq.1) then
        t80='Fourier maps general section : '//t80(1:idel(t80))
      else if(kpdf.eq.2) then
        t80='p.d.f. : '//t80(1:idel(t80))
      else if(kpdf.eq.3) then
        t80='j.p.d.f. : '//t80(1:idel(t80))
      else if(kpdf.le.6) then
        if(DensityType.eq.0) then
          t80='Total density'
        else if(DensityType.eq.1) then
          t80='Valence density'
        else if(DensityType.eq.2) then
          t80='Deformation density'
        else if(DensityType.eq.3) then
          t80='Total density Laplacian '
        else if(DensityType.eq.4) then
          t80='Valence density Laplacian '
        else if(DensityType.eq.5) then
          t80='Inversed gradient vector'
        else if(DensityType.eq.6) then
          t80='Gradient vector'
        endif
      endif
      if(ErrDraw) t80=t80(1:idel(t80))//' - error map'
      call FeHeaderInfo(t80)
9999  return
      end
