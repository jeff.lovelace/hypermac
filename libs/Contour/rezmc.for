      subroutine rezmc(xp,n,m,gy,sgy,*)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 t80
      logical konec
      real, allocatable :: gyp(:,:)
      dimension xp(3,9),xd(3),xx(3),gy(m,9),sgy(m,9)
      if(allocated(gyp)) deallocate(gyp)
      allocate(gyp(m,9))
      call ConReadM85
      call ConDefMonteCarlo(ich)
      if(ich.ne.0) go to 9000
      pom=ran1(n*nxny)
      xdp=1./float(m-1)
      write(t80,101) 0,100.
      call FeOpenInterrupt(-1.,-1.,t80)
      konec=.false.
      do iy=1,n-1
        do ix=1,m
          sgy(ix,iy)=0.
        enddo
      enddo
      k=0
1500  k=k+1
      if(k.gt.mcmax) go to 5100
      call ConHitMC
      do iy=1,n-1
        do ix=1,m
          gyp(ix,iy)=0.
        enddo
      enddo
      do ia=1,npdf
        ip=ipor(ia)
        if(fpdf(ip).le.0.) cycle
        xx(1)=pdfat(xa(1,1),0.,ia,0,ich)
        if(ich.ne.0) then
          k=k-1
          go to 1500
        endif
        do iy=1,n-1
          do i=1,3
            xd(i)=(xp(i,iy+1)-xp(i,iy))*xdp
          enddo
          do i=1,3
            xx(i)=xp(i,iy)
          enddo
          do ix=1,m
            gyp(ix,iy)=gyp(ix,iy)+pdfat(xx,0.,ia,1,ich)*fpdf(ip)
            if(ich.ne.0) cycle
            do i=1,3
              xx(i)=xx(i)+xd(i)
            enddo
          enddo
        enddo
      enddo
      if(.not.konec) call FeEventInterrupt(Konec)
      pom1=1./float(k)
      pom2=pom1*float(k-1)
      error1=0.
      error2=0.
      do iy=1,n-1
        do ix=1,m
          pom=(gyp(ix,iy)-gy(ix,iy))**2*pom1
          if(ix.ne.1.or.iy.eq.1) then
            error1=error1+abs(sgy(ix,iy)*pom1-pom)
            error2=error2+sgy(ix,iy)
          endif
          sgy(ix,iy)=pom2*sgy(ix,iy)+pom
        enddo
      enddo
      if(error2.gt.0.) then
        error=min(sqrt(error1/error2)*100.,100.)
      else
        error=100.
      endif
      if(error.lt.errlev.or.konec) go to 5100
      if(mod(k,100).eq.0.or.k.eq.1) then
        write(t80,101) k,error
        call FeOutputInterrupt(t80)
      endif
      go to 1500
5100  write(t80,101) k,error
      call FeCloseInterrupt
      do iy=1,n-1
        do ix=1,m
          sgy(ix,iy)=sqrt(sgy(ix,iy))
        enddo
      enddo
      call obnova
      go to 9999
9000  mcmax=0
      if(allocated(gyp)) deallocate(gyp)
      return1
9999  if(allocated(gyp)) deallocate(gyp)
      return
101   format('Monte Carlo hits :',i5,' Error :',f5.1,'%')
      end
