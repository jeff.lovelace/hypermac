      subroutine ChdCheckIfCaptured(xyz,iterm,NZeroNeigh,npth,nterm,
     1                              icount)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      integer ChdCapturedBy
      dimension xf(3),xyz(*),iterm(*),NZeroNeigh(*)
      is=0
      do i=1,npth
        if(iterm(i).eq.0) then
          pom=0.
          do j=1,3
            pom=pom+(xyz(is+j)-rcla(j,IAtInt))**2
          enddo
          if(pom.le.RCapture(IAtInt)**2) then
            iterm(i)=1
          else
            call multm(TrToOrthoI,xyz(is+1),xf,3,3,1)
            n=ChdCapturedBy(xf)
            if(n.le.0.and.float(icount)*StepTrajInt.gt.15.) then
              n=1
            endif
            if(n.gt.0) then
              iterm(i)=2
              ireach(n)=1
            else
              go to 1500
            endif
          endif
          nterm=nterm+1
        endif
1500    is=is+3
      enddo
9999  return
      end
