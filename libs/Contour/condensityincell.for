      subroutine ConDensityInCell(ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real xp(6),DenSupl(3),xn(6)
      real, allocatable :: Table(:)
      character*80 Veta
      integer :: DensityTypeSave=2
      logical ExistFile
      do i=1,6
        iorien(i)=i
      enddo
      DensityType=DensityTypeSave
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      call ConSetLocDensity
      call CPReadScope(1,xmin,xmax,dx,DensityType,CutOffDist,ich)
      if(ich.ne.0) go to 9999
      call ConDefAtForDenCalc(3,ich)
      if(ich.ne.0) go to 9999
      nxny=1
      do i=1,6
        if(i.le.3) then
          nx(i)=nint((xmax(i)-xmin(i))/dx(i))+1
          if(i.le.2) then
            nxny=nxny*nx(i)
          else
            nmap=nx(i)
          endif
        else
          nx(i)=1
          xmin(i)=0.
          xmax(i)=0.
          dx(i)=.1
        endif
      enddo
      if(allocated(Table)) deallocate(Table)
      allocate(Table(nxny))
      Veta=fln(:ifln)//'.d81'
      if(ExistFile(Veta)) call DeleteFile(Veta)
      m8=NextLogicNumber()
      call OpenMaps(m8,Veta,nxny,1)
      if(ErrFlag.ne.0) go to 9999
      write(m8,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                (iorien(i),i=1,6),-DensityType-1,nsubs,
     2                SatelityByly,nonModulated(KPhase)
      nvse=npdf*nxny*nx(3)
      DensityTypeSave=DensityType
      Veta=MenDensity(DensityType+1)
      Veta='Calculation of '//Veta(:idel(Veta))//' map'
      call FeFlowChartOpen(-1.,120.,max(nint(float(nvse)*.005),10),nvse,
     1                     Veta,' ',' ')
      m=0
      Dmax=-999999.
      Dmin= 999999.
      CutOffDistA=CutOffDist*rcp(1,1,KPhase)
      CutOffDistB=CutOffDist*rcp(2,1,KPhase)
      CutOffDistC=CutOffDist*rcp(3,1,KPhase)
      xp3=xmin(3)
      do iz=1,nx(3)
        Table=0.
        do 3500ia=1,npdf
          ip=ipor(ia)
          fpdfi=fpdf(ip)
          if(fpdfi.le.0.) then
            m=m+nxny
            go to 3500
          endif
          call ConDensity(pom,ia,0,DenSupl,ich)
          xp(3)=xp3-xpdf(3,ip)
          if(abs(xp(3)).gt.CutOffDistC) then
            m=m+nxny
            go to 3500
          endif
          xp(2)=xmin(2)-xpdf(2,ip)
          n=0
          do iy=1,nx(2)
            if(abs(xp(2)).gt.CutOffDistB) then
              m=m+nx(1)
              n=n+nx(1)
              go to 3350
            endif
            xp(1)=xmin(1)-xpdf(1,ip)
            do ix=1,nx(1)
              n=n+1
              if(abs(xp(1)).gt.CutOffDistA) then
                m=m+1
                go to 3250
              endif
              call FeFlowChartEvent(m,is)
              if(is.ne.0) then
                call FeBudeBreak
                if(ErrFlag.ne.0) then
                  ich=1
                  go to 9000
                endif
              endif
              call MultM(TrToOrtho,xp,xo,3,3,1)
              dd=VecOrtLen(xo,3)
              if(dd.gt.CutOffDist) go to 3250
              call ConDensity(dd,ia,1,DenSupl,ich)
              Table(n)=Table(n)+DenSupl(1)*fpdfi
3250          xp(1)=xp(1)+dx(1)
            enddo
3350        xp(2)=xp(2)+dx(2)
          enddo
3500    continue
        do i=1,nxny
          pom=Table(i)
          Dmin=min(Dmin,pom)
          Dmax=max(Dmax,pom)
        enddo
        write(m8,rec=iz+1)(Table(i),i=1,nxny)
        xp3=xp3+dx(3)
      enddo
      write(m8,rec=nx(3)+2) Dmax,Dmin
      close(m8)
      call FeFlowChartRemove
      go to 9999
9000  close(m8,status='delete')
9999  if(allocated(Table)) deallocate(Table)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      return
      end
