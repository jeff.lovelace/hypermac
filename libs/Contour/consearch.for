      subroutine ConSearch
      use contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xt(3),xv(3,10),xemx(:,:),xemn(:,:),dmx(:),dmn(:),
     1          idm(:),ipm(:),xmo(:,:),dmo(:)
      character*11 :: FileNameTmp(2)=(/'_maxima.tmp','_minima.tmp'/)
      character*8  SvFile
      allocatable xemx,xemn,dmx,dmn,idm,ipm,xmo,dmo
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      call FeSaveImage(XMinBasWin+2.,XMaxBasWin-40.,YMinGrWin,
     1                 YMaxBasWin-14.,SvFile)
      n=0
      ip=0
      TakeMouseMove=.true.
      call FeMoveMouseTo(XCenGrWin,YCenGrWin)
1000  if(DeferredOutput) call FeReleaseOutput
1005  call FeEvent(1)
      if(EventNumber.eq.0) go to 1005
      call FeMouseShape(0)
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown) then
        n=n+1
        if(ip.eq.1) then
          call FePlotMode('E')
          call FePolyLine(5,xu,yu,Green)
          yu(2)=Ypos
          xu(3)=Xpos
          yu(3)=Ypos
          xu(4)=Xpos
          call FePlotMode('N')
          call FePolyLine(5,xu,yu,Green)
          xv(1,n)=Xpos
          xv(2,n)=Ypos
          xv(3,n)=0.
          ip=0
        else
          ip=ip+1
          do i=1,5
            xu(i)=Xpos
            yu(i)=Ypos
          enddo
          xv(1,n)=Xpos
          xv(2,n)=Ypos
          xv(3,n)=0.
        endif
        if(n.ge.10) then
          call FeChybne(-1.,-1.,'the maximum of 5 rectangules reached.',
     1                  ' ',SeriousError)
          go to 1100
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove.and.
     1  ip.ne.0) then
        call FePlotMode('E')
        call FePolyLine(5,xu,yu,Green)
        yu(2)=Ypos
        xu(3)=Xpos
        yu(3)=Ypos
        xu(4)=Xpos
        call FePolyLine(5,xu,yu,Green)
        call FePlotMode('N')
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        if(ip.eq.1) then
          call FePlotMode('E')
          call FePolyLine(5,xu,yu,Green)
        endif
        go to 1100
      endif
      go to 1000
1100  call FePlotMode('N')
      if(n.le.1) go to 9999
      do i=1,n-1,2
        if(xv(1,i).gt.xv(1,i+1)) then
          pom=xv(1,i)
          xv(1,i)=xv(1,i+1)
          xv(1,i+1)=pom
        endif
        if(xv(2,i).gt.xv(2,i+1)) then
          pom=xv(2,i)
          xv(2,i)=xv(2,i+1)
          xv(2,i+1)=pom
        endif
      enddo
      it=0
      nmx=0
      nmn=0
      xt(3)=zmap(1)
      xt(2)=xmin(2)
      MxPeak=10
      MnPeak=10
      allocate(xemx(3,MxPeak),xemn(3,MnPeak),dmx(MxPeak),dmn(MnPeak))
      do iy=1,nx(2)
        xt(1)=xmin(1)
        do ix=1,nx(1)
          it=it+1
          if(ix.eq.1.or.ix.eq.nx(1)) go to 1890
          if(iy.eq.1.or.iy.eq.nx(2)) go to 1890
          call FeXf2X(xt,xo)
          x1=xo(1)
          y1=xo(2)
          do i=1,n-1,2
            if(x1.ge.xv(1,i).and.x1.le.xv(1,i+1).and.
     1         y1.ge.xv(2,i).and.y1.le.xv(2,i+1)) go to 1300
          enddo
          go to 1890
1300      tbl=ActualMap(it)
          if(tbl.lt.ActualMap(it-1)) go to 1400
          if(tbl.le.ActualMap(it+1)) go to 1400
          if(tbl.lt.ActualMap(it-nx(1))) go to 1400
          if(tbl.le.ActualMap(it+nx(1))) go to 1400
          if(tbl.lt.ActualMap(it-nx(1)-1)) go to 1400
          if(tbl.le.ActualMap(it+nx(1)+1)) go to 1400
          if(tbl.lt.ActualMap(it-nx(1)+1)) go to 1400
          if(tbl.le.ActualMap(it+nx(1)-1)) go to 1400
          call vrchol(xt(1),dx(1),ActualMap(it-1),tbl,ActualMap(it+1),
     1                x1,pom1)
          call vrchol(xt(2),dx(2),ActualMap(it-nx(1)),tbl,
     1                ActualMap(it+nx(1)),x2,pom2)
          pom=(pom1+pom2)*.5
          if(pom.le.0.) go to 1400
          if(nmx.ge.MxPeak) then
            allocate(xmo(3,nmx),dmo(nmx))
            do i=1,nmx
              dmo(i)=dmx(i)
              call CopyVek(xemx(1,i),xmo(1,i),3)
            enddo
            deallocate(xemx,dmx)
            MxPeak=MxPeak+10
            allocate(xemx(3,MxPeak),dmx(MxPeak))
            do i=1,nmx
              dmx(i)=dmo(i)
              call CopyVek(xmo(1,i),xemx(1,i),3)
            enddo
            deallocate(xmo,dmo)
          endif
          nmx=nmx+1
          dmx(nmx)=pom
          xemx(1,nmx)=x1
          xemx(2,nmx)=x2
          xemx(3,nmx)=zmap(1)
          go to 1890
1400      if(tbl.gt.ActualMap(it-1)) go to 1890
          if(tbl.ge.ActualMap(it+1)) go to 1890
          if(tbl.gt.ActualMap(it-nx(1))) go to 1890
          if(tbl.ge.ActualMap(it+nx(1))) go to 1890
          if(tbl.gt.ActualMap(it-nx(1)-1)) go to 1890
          if(tbl.ge.ActualMap(it+nx(1)+1)) go to 1890
          if(tbl.gt.ActualMap(it-nx(1)+1)) go to 1890
          if(tbl.ge.ActualMap(it+nx(1)-1)) go to 1890
          call vrchol(xt(1),dx(1),ActualMap(it-1),tbl,ActualMap(it+1),
     1                x1,pom1)
          call vrchol(xt(2),dx(2),ActualMap(it-nx(1)),tbl,
     1                ActualMap(it+nx(1)),x2,pom2)
          pom=(pom1+pom2)*.5
          if(pom.ge.0.) go to 1890
          if(nmn.ge.MnPeak) then
            allocate(xmo(3,nmn),dmo(nmn))
            do i=1,nmn
              dmo(i)=dmn(i)
              call CopyVek(xemn(1,i),xmo(1,i),3)
            enddo
            deallocate(xemn,dmn)
            MnPeak=MnPeak+10
            allocate(xemn(3,MnPeak),dmn(MnPeak))
            do i=1,nmn
              dmn(i)=dmo(i)
              call CopyVek(xmo(1,i),xemn(1,i),3)
            enddo
            deallocate(xmo,dmo)
          endif
          nmn=nmn+1
          dmn(nmn)=pom
          xemn(1,nmn)=x1
          xemn(2,nmn)=x2
          xemn(3,nmn)=zmap(1)
1890      xt(1)=xt(1)+dx(1)
        enddo
        xt(2)=xt(2)+dx(2)
      enddo
      n=max(nmx,nmn)
      allocate(idm(n),ipm(n))
      do i=1,2
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//FileNameTmp(i),'formatted',
     1                'unknown')
        if(i.eq.1) then
          do j=1,nmx
            idm(j)=-dmx(j)*100.
          enddo
          call indexx(nmx,idm,ipm)
          do j=1,nmx
            m=ipm(j)
            call Prevod(1,xemx(1,m),xt)
            write(ln,100)(xemx(k,m),k=1,3),xt,dmx(m)
          enddo
        else
          do j=1,nmn
            idm(j)=dmn(j)*100.
          enddo
          call indexx(nmn,idm,ipm)
          do j=1,nmn
            m=ipm(j)
            call Prevod(1,xemn(1,m),xt)
            write(ln,100)(xemn(k,m),k=1,3),xt,dmn(m)
          enddo
        endif
        call CloseIfOpened(ln)
      enddo
      call ConShowMaxMin(FileNameTmp)
      call FeLoadImage(XMinBasWin+2.,XMaxBasWin-40.,YMinGrWin,
     1                 YMaxBasWin-14.,SvFile,0)
9999  TakeMouseMove=.false.
      EventType=0
      EventNumber=0
      if(allocated(xemx))
     1  deallocate(xemx,xemn,dmx,dmn)
      if(allocated(idm)) deallocate(idm,ipm)
      call FeReleaseOutput
      return
100   format(3f8.3,2x,3f8.4,f10.2)
      end
