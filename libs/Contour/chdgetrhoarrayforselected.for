      subroutine ChdGetRhoArrayForSelected(RhoArr,xyz,iterm,NZeroNeigh,
     1                                     n,IType)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension RhoArr(*),xyz(*),iterm(*),NZeroNeigh(*),xf(3)
      is=1
      nsubs=1
      ivs=1
      if(IType.eq.0) then
        ivd=1
      else if(IType.eq.1) then
        ivd=4
      else if(IType.eq.2) then
        ivd=10
      endif
      do i=1,n
        if(iterm(i).eq.0) then
          call multm(TrToOrthoI,xyz(is),xf,3,3,1)
          call ChdGetNeighbours(xf,CutOffDist)
          call ChdGetRhoAndDRho(xf,IType,RhoArr(ivs),ich)
          if(npdf.le.0) then
            NZeroNeigh(i)=NZeroNeigh(i)+1
            NZeroNeighAll=NZeroNeighAll+1
          endif
        endif
        is=is+3
        ivs=ivs+ivd
      enddo
      return
      end
