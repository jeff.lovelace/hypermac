      subroutine ConWriteKeys
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*80 Radka
      logical EqIgCase
      SaveAs=0
      Radka=' '
      if(Obecny.and.VolaToContour) then
        if(IPlane.eq.0) then
          call ConWriteKeysSaveAs(0,Radka)
          SaveAs=-1
          if(Radka.eq.' ') go to 9999
          call ConReallocatePlanes
          NumberOfPlanes=NumberOfPlanes+1
          LabelPlane(NumberOfPlanes)=Radka
          IPlane=NumberOfPlanes
        endif
      endif
1000  call SetIntArrayTo(NactiRepeat,NactiKeys,-1)
      call WriteKeys('contour')
      izpet=0
      call NastavKeys('contour')
      PreskocVykricnik=.false.
1100  call NactiCommon(m50,izpet)
      if((Obecny.and.IZpet.ne.0).or.IPlane.lt.-1) then
        if(IZpet.ne.0) then
          Radka=NactiVeta
          if(NacetlVykricnik) Radka='!'//Radka(:idel(Radka))
          Radka='  '//Radka(:idel(Radka))
          write(55,FormA) Radka(:idel(Radka))
        endif
        if(IPlane.lt.-1) then
          if(IZpet.eq.0) then
            go to 2500
          else
            go to 1100
          endif
        endif
      endif
      if(IZpet.eq.nCmdLabelPlane) then
        if(Obecny) then
          if(IPlane.gt.0) then
            Radka=NactiVeta(PosledniPozice+1:)
            if(EqIgCase(Radka,LabelPlane(IPlane))) then
1200          read(m50,FormA,end=2000) Radka
              k=0
              call kus(Radka,k,Cislo)
              if(.not.EqIgCase(Cislo,'end').and.
     1           .not.EqIgCase(Cislo,idContour(nCmdEndPlane))) then
                go to 1200
              else
                backspace 55
                go to 2000
              endif
            endif
          endif
        else
          backspace m50
          go to 2000
        endif
      else
        if(IZpet.eq.0) go to 2000
      endif
      go to 1100
2000  if(Obecny) then
        k=0
        call WriteLabeledRecord(55,'  '//idContour(nCmdLabelPlane),
     1                          LabelPlane(IPlane),k)
        do i=1,3
          write(Radka,'(i3)') SelPlane(i)
          n=4
          if(SelPlane(i).eq.1) then
            Radka(5:)=StPlane(i)
            n=2
          else if(SelPlane(i).eq.2) then
            write(Radka(5:),100)(XPlane(j,i),j=1,3)
          else
            write(Radka(5:),100)(DxPlane(j,i),j=1,3)
          endif
          call ZdrcniCisla(Radka,n)
          Radka='    '//idContour(nCmdPntPlane)
     1          (:idel(idContour(nCmdPntPlane)))//' '//
     2          Radka(:idel(Radka))
          write(55,FormA) Radka(:idel(Radka))
        enddo
        write(Radka,101) DeltaPlane
        call ZdrcniCisla(Radka,1)
        Radka='    '//idContour(nCmdDeltaPlane)
     1        (:idel(idContour(nCmdDeltaPlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        write(Radka,101) ScopePlane
        call ZdrcniCisla(Radka,3)
        Radka='    '//idContour(nCmdScopePlane)
     1        (:idel(idContour(nCmdScopePlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        write(Radka,101) ShiftPlane
        call ZdrcniCisla(Radka,3)
        Radka='    '//idContour(nCmdShiftPlane)
     1        (:idel(idContour(nCmdShiftPlane)))//' '//
     2        Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
        if(NCurve.gt.0) then
          write(Radka,'(i3)') XTypeCurve
          k=0
          call WriteLabeledRecord(55,'    '//idContour(nCmdXTypeCurve),
     1                            Radka,k)
          do i=1,NCurve
            if(AtCurve(i).eq.' ') then
              if(XTypeCurve.eq.1) then
                write(Radka,101)(XLocCurve(j,i),j=1,3)
              else
                write(Radka,100)(XFractCurve(j,i),j=1,3)
              endif
              call ZdrcniCisla(Radka,3)
            else
              Radka=AtCurve(i)
            endif
            k=0
            call WriteLabeledRecord(55,'    '//
     1                              idContour(nCmdPointCurve),Radka,k)
          enddo
        endif
      endif
      do i=1,DrawAtN
        call ConMakeDrawAtString(i,Radka)
        Cislo=' '
        if(Obecny) then
          n=4
        else
          n=2
        endif
        Radka=Cislo(:n)//Radka(:idel(Radka))
        write(55,FormA) Radka(:idel(Radka))
      enddo
      if(Obecny) write(55,FormA) '  '//
     1  idContour(nCmdEndPlane)(:idel(idContour(nCmdEndPlane)))
2100  if(IZpet.ne.0.or.IPlane.lt.-1) then
2200    read(m50,FormA,end=2500) Radka
        write(55,FormA) Radka(:idel(Radka))
        k=0
        call kus(Radka,k,Cislo)
        if(EqIgCase(Cislo,'end')) then
          go to 2550
        else
          go to 2200
        endif
      endif
2500  write(55,'(''end contour'')')
2550  if(SaveAs.eq.1) SaveAs=-1
      call DopisKeys(1)
      if(SaveAs.eq.1) then
        close(m50)
        close(55)
        go to 1000
      endif
      KeysSaved=.true.
      go to 9999
9900  call CloseIfOpened(m50)
      close(55,status='delete')
9999  return
100   format(3f12.6)
101   format(3f10.4)
      end
