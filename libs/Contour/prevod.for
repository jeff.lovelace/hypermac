      subroutine prevod(klic,xin,xout)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension xin(3),xout(3),xp(3)
      if(klic.eq.0) then
        if(obecny) then
          do j=1,3
            xp(j)=xin(j)-xob(j)
          enddo
          call multm(trob,xp,xout,3,3,1)
          do i=1,3
            xout(i)=xout(i)+xminr(i)
          enddo
        else
          call multm(tm3,xin,xout,3,3,1)
        endif
      else
        if(obecny) then
          do i=1,3
            xp(i)=xin(i)-xminr(i)
          enddo
          call multm(trobi,xp,xout,3,3,1)
          do j=1,3
            xout(j)=xout(j)+xob(j)
          enddo
        else
          call multm(tm3i,xin,xout,3,3,1)
        endif
      endif
      return
      end
