      subroutine TPIntegration
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension soph(nprops),soth(nprops),wp(nprops),wt(nprops),
     1          Tollo(:),xp(3)
      character*256 EdwStringQuest
      character*80 t80
      integer EdwStateQuest,RolMenuSelectedQuest
      logical EqWild
      allocatable Tollo
      allocate(Tollo(NAtCalc))
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      if(allocated(IaChd))
     1  deallocate(IaChd,IsChd,XfChd,XoChd,popasChd,Toll,RCapture,
     2             ireach,rcla)
      if(allocated(AtBrat)) deallocate(AtBrat)
      allocate(AtBrat(NAtCalc))
      call ConSetLocDensity
      call ChdGenSymmAtoms
      StepInt=0.25*BohrRad
      rsca=0.8
      StepTrajInt=0.025*BohrRad
      TInfInt=10.*BohrRad
      SizeAtInt=9.*BohrRad
      AccurInt=0.01*BohrRad
      nac=NAtIndLenAll(KPhase)
      nap=NAtIndFrAll(KPhase)
      do i=nap,nap+nac-1
        AtBrat(i)=ai(i).gt.0.
      enddo
      call SelAtoms('Select atoms for basin integration',Atom(nap),
     1              AtBrat(nap),isf(nap),nac,ich)
      if(ich.ne.0) go to 9999
      ia=nap
      do i=1,nac
        j=isf(ia)
        if(AtType(j,KPhase).eq.'H') then
          Toll(i)=0.3
        else
          Toll(i)=0.8
        endif
        ia=ia+1
      enddo
      MethZFS=1
      nPhiIntA=32
      nThIntA=24
      nRInt=96
      CutOffDist=2.8
      call CopyVek(Toll,Tollo,nac)
      id=NextQuestId()
      xqd =400.
      call FeQuestCreate(id,-1.,-1.,xqd,5,'Define parameters for '//
     1                   'integration',0,LightGray,0,0)
      il=1
      dpom=50.
      t80='Number of steps in %phi'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(t80)+25.
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNPhi=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nPhiIntA,.false.)
      il=il+1
      t80='Number of steps in %theta'
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNTh=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nThIntA,.false.)
      il=il+1
      t80='Number of steps in %R'
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNR=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nRInt,.false.)
      il=il+1
      t80='%Cutoff distance for density calculation'
      xpom=tpom+FeTxLengthUnder(t80)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwCutOff=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,CutOffDist,.false.,.false.)
      il=il+1
      t80='%Define capture sphere radii'
      dpom=FeTxLengthUnder(t80)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtCapRad=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCapRad) then
        idp=NextQuestId()
        xqd =500.
        xqdp=xqd*.5
        il=min(nac,10)+4
        call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define capture radii',
     1                     0,LightGray,0,0)
        if(nac.gt.30) then
          xpom=xqd-25.
          dpom=80.
          call FeQuestButtonMake(id,xpom,10,dpom,ButYd,'%Next')
          nButtNext=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonMake(id,xpom,1,dpom,ButYd,'%Previous')
          nButtPrevious=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        else
          nButtNext=0
          nButtPrevious=0
        endif
        tpom=5.
        dpom=50.
        pom=0.
        ia=nap
        do i=1,nac
          pom=max(pom,FeTxLength(Atom(ia)))
          ia=ia+1
        enddo
        xpom=tpom+pom+10.
        il=0
        do i=1,min(30,nac)
          il=il+1
          if(mod(il,10).eq.1.and.i.ne.1) then
            il=1
            tpom=tpom+150.
            xpom=xpom+150.
          endif
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Atom(i),'L',dpom,
     1                        EdwYd,0)
          if(i.eq.1) nEdwFirst=EdwLastMade
        enddo
        if(nac.gt.10) then
          il=11
        else
          il=il+1
        endif
        call FeQuestLinkaMake(idp,il)
        il=il+1
        do i=1,2
          if(i.eq.1) then
            t80='%Set'
            dpom1=FeTxLengthUnder(t80)+20.
            xpom1=50.
          else
            t80='S%et'
          endif
          call FeQuestButtonMake(idp,xpom1,il,dpom1,ButYd,t80)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          if(i.eq.1) then
            nButtSetAtomType=ButtonLastMade
            t80='radii for atom t%ype'
            tpom2=xpom1+dpom1+15.
            xpom2=tpom2+FeTxLengthUnder(t80)+25.
            dpom2=50.+EdwYd
            call FeQuestRolMenuMake(idp,tpom2,il,xpom2,il,t80,'L',dpom2,
     1                              EdwYd,0)
            call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                              NAtFormula(KPhase),0)
            nRolMenuAtomType=RolMenuLastMade
            tpom3=xpom2+dpom2+15.
            t80='to'
            xpom3=tpom3+FeTxLengthUnder(t80)+15.
            dpom3=50.
          else
            nButtSetAtomName=ButtonLastMade
            t80='radii for atom %name'
            call FeQuestEdwMake(idp,tpom2,il,xpom2,il,t80,'L',dpom2,
     1                          EdwYd,0)
            nEdwAtomName=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
            t80='to'
          endif
          call FeQuestEdwMake(idp,tpom3,il,xpom3,il,t80,'L',dpom3,
     1                        EdwYd,0)

          call FeQuestRealEdwOpen(EdwLastMade,0.8,.false.,.false.)
          if(i.eq.1) then
            nEdwAtomTypeValue=EdwLastMade
          else
            nEdwAtomNameValue=EdwLastMade
          endif
          il=il+1
        enddo
        m=1
1140    n1=30*(m-1)+1
        n2=min(nac,n1+30-1)
        il=0
        tpom=5.
        dpom=25.
        xpom=tpom+FeTxLengthUnder('XXXXXXXX')+5.
        il=0
        nEdw=nEdwFirst
        do i=n1,n1+29
          if(i.le.n2) then
            if(EdwStateQuest(nEdw).eq.EdwOpened)
     1        call FeQuestEdwLabelChange(nEdw,Atom(i))
            call FeQuestRealEdwOpen(nEdw,toll(i),.false.,.false.)
          else if(i.le.nac) then
            call FeQuestEdwClose(nEdw)
          endif
          nEdw=nEdw+1
        enddo
        if(nac.gt.30) then
          if(n2.lt.nac) then
            call FeQuestButtonOff(nButtNext)
          else
            call FeQuestButtonDisable(nButtNext)
          endif
          if(n1.gt.1) then
            call FeQuestButtonOff(nButtPrevious)
          else
            call FeQuestButtonDisable(nButtPrevious)
          endif
        endif
1250    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtPrevious.or.
     2      CheckNumber.eq.nButtNext)) then
          if(CheckNumber.eq.nButtNext) then
            m=m+1
          else
            m=m-1
          endif
          nEdw=nEdwFirst
          do i=n1,n2
            call FeQuestRealFromEdw(nEdw,toll(i))
            nEdw=nEdw+1
          enddo
          go to 1140
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSetAtomType) then
          t80=EdwStringQuest(nEdwAtomTypeValue)
          if(t80.ne.' ') then
            call FeQuestRealFromEdw(nEdwAtomTypeValue,pom)
          else
            go to 1140
          endif
          i=RolMenuSelectedQuest(nRolMenuAtomType)
          if(i.gt.0.and.i.le.nac) then
            ia=nap
            do j=1,nac
              if(isf(j).eq.i) Toll(j)=pom
              ia=ia+1
            enddo
          endif
          go to 1140
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSetAtomName) then
          t80=EdwStringQuest(nEdwAtomNameValue)
          if(t80.ne.' ') then
            call FeQuestRealFromEdw(nEdwAtomNameValue,pom)
          else
            go to 1140
          endif
          t80=EdwStringQuest(nEdwAtomName)
          ia=nap
          do j=1,nac
            if(EqWild(Atom(j),t80,.false.)) Toll(j)=pom
            ia=ia+1
          enddo
          go to 1140
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1250
        endif
        if(ich.eq.0) then
          nEdw=nEdwFirst
          do i=n1,n2
            call FeQuestRealFromEdw(nEdw,toll(i))
            nEdw=nEdw+1
          enddo
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) call CopyVek(Tollo,Toll,nac)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNPhi,nPhiIntA)
        call FeQuestIntFromEdw(nEdwNTh,nThIntA)
        call FeQuestIntFromEdw(nEdwNR,nRInt)
        call FeQuestRealFromEdw(nEdwCutOff,CutOffDist)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      do i=1,nac
        RCapture(i)=rsca*(Toll(i)-StepTrajInt)
        call multm(TrToOrtho,x(1,i),rcla(1,i),3,3,1)
      enddo
      call OpenFile(lst,fln(:ifln)//'.inb','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      LstOpened=.true.
      uloha='Program for basin integration of static density'
      call newpg(1)
      call iom50(0,1,fln(:ifln)//'.m50')
      call comsym(0,1,ich)
      if(ErrFlag.ne.0.or.ich.ne.0) go to 9000
      call TitulekVRamecku('Capture sphere radii')
      n=(nac-1)/15+1
      n2=0
      do i=1,n
        n1=n2+1
        n2=min(n1+14,nac)
        call newln(3)
        write(lst,'(15a8)')(atom(j),j=n1,n2)
        write(lst,'(15(f6.4,2x))')(RCapture(j),j=n1,n2)
        write(lst,FormA80)
      enddo
      call TitulekVRamecku('Integration parameters')
      call newln(4)
      write(lst,'(''Number of steps in phi   :'',i3/
     1            ''Number of steps in theta :'',i3/
     2            ''Number of steps in R     :'',i3/)')
     3  nPhiIntA,nThIntA,nRInt
      call newln(2)
      write(lst,'(''The cutoff distance for calculation of density :'',
     1            f8.3/)') CutOffDist
      SummEl=0.
      do IAtInt=1,nac
        if(.not.AtBrat(IAtInt)) cycle
        write(t80,'(''Results of integration for atom : '',a8)')
     1    atom(IAtInt)
        xp=0.
        call TitulekVRamecku(t80)
        call SetRealArrayTo(soph,nprops,0.)
        soph(1)=popc(IAtInt)
        do ib=0,1
          ibeta=ib
          if(ib.eq.0) then
            nPhiInt=48
            nThInt=32
          else
            nPhiInt=nPhiIntA
            nThInt =nThIntA
          endif
          nPhiThInt=nPhiInt*nThInt
          call gauleg(0.,pi2,phi,wp,nPhiInt)
          do i=1,nPhiInt
            cphi(i)=cos(phi(i))
            sphi(i)=sin(phi(i))
          enddo
          call gauleg(0.,pi,theta,wt,nThInt)
          do i=1,nThInt
            ctheta(i)=cos(theta(i))
            stheta(i)=sin(theta(i))
          enddo
          if(ibeta.eq.1) then
            call ChdZFSDetermination
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nPhiInt.gt.2) then
            if(ibeta.eq.0) then
              t80='Integration inside of beta sphere for'
            else
              t80='Integration outside of beta sphere for'
            endif
            t80=t80(:idel(t80)+1)//atom(IAtInt)
            call FeFlowChartOpen(-1.,-1.,1,nPhiInt,t80,' ',' ')
            nFlowInt= 0
          else
            nFlowInt=-1
          endif
          call ChdIntThRAllocate
          do i=1,nPhiInt
            call ChdIntThRSumm(i,soth,wt)
            if(ErrFlag.ne.0) go to 2400
            call FeFlowChartEvent(nFlowInt,ie)
            if(ie.ne.0) then
              ErrFlag=1
              go to 2400
            endif
            wpi=wp(i)
            do j=1,nprops
              soph(j)=soph(j)+wpi*soth(j)
            enddo
          enddo
2400      call ChdIntThRDeallocate
          if(nFlowInt.ge.0) call FeFlowChartRemove
          if(ErrFlag.ne.0) go to 9000
        enddo
        call newln(2)
        write(lst,'(''Number of electrons : '',f8.3)') soph(1)
        write(lst,'(''Charge in electrons : '',f8.3)')
     1    -soph(1)+AtNum(isf(IAtInt),KPhase)*ai(IAtInt)*
     2             AtSiteMult(IAtInt)
        if(NZeroNeighAll.gt.0) then
          call NewLn(5)
          write(lst,'(/''WARNING:'')')
          write(lst,'(''During the calculation the program reached '',
     1                ''points having all distances to atoms'')')
          write(lst,'(''longer than the distance cutoff limit. This '',
     1                ''could cause numerical problems.'')')
          write(lst,'(''Please try to enlarge the the distance cutoff'',
     1                '' and run it once more.'')')
        endif
        SummEl=SummEl+soph(1)/AtSiteMult(IAtInt)
      enddo
      call newln(2)
      write(lst,'(/''Sum of electrons over all selected atoms : '',
     1            f8.3)') SummEl
9000  call CloseListing
9999  if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      deallocate(Tollo)
      if(allocated(IaChd))
     1  deallocate(IaChd,IsChd,XfChd,XoChd,popasChd,Toll,RCapture,
     2             ireach,rcla)
      if(allocated(AtBrat)) deallocate(AtBrat)
      if(allocated(iapdf))
     1  deallocate(xpdf,popaspdf,idpdf,dpdf,fpdf,ypdf,SelPDF,scpdf,
     2             iapdf,ispdf,ipor)
      return
      end
