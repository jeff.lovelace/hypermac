      subroutine ConFouPeaks(m8)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ior(6)
      call CopyVekI(iorien,ior,6)
      npeaks(1)=50
      npeaks(2)=50
      call OpenFile(lst,fln(:ifln)//'_peaks.lst','formatted','unknown')
      YMinFlowChart=-1.
      call FouPeaks(0,m8)
      call CopyVekI(ior,iorien,6)
      close(lst,status='delete')
      return
      end
