      subroutine ConDefGenSection(at,Klic,ich)
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      dimension p3(3),xg(3),yg(3),xgo(3)
      character*(*) at
      character*256 ta256(1),t256,EdwStringQuest,SbwStringQuest,
     1              SbwFileQuest
      character*80 Veta,StOld(9)
      character*20 DelString
      character*2 nty
      integer CrwStateQuest,RolMenuSelectedQuest,SbwItemFromQuest,
     1        SbwItemPointerQuest,SbwLnQuest,Exponent10,EdwStateQuest
      logical eqrv,CrwLogicQuest,Skip,EqIgCase,Change,FileDiff,FeYesNo,
     1        WizardModeSave
      equivalence (ta256,t256)
      data DelString/'--------------------'/
      if(NumberOfPlanes.lt.IPlaneOld) IPlaneOld=0
      Change=.false.
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
1100  id=NextQuestId()
      Veta='Plane/Volume'
      if(WizardMode) then
        call FeQuestTitleMake(id,Veta)
        xqd=WizardLength
      else
        il=11
        if(Klic.eq.1) il=il+2
        xqd=550.
        call FeQuestCreate(id,-1.,-1.,xqd,il,'Plane/Volume',0,LightGray,
     1                     0,0)
      endif
      il=0
      tpom=5.
      Veta='%Define new'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          nCrwNew=CrwLastMade
        else
          nCrwOld=CrwLastMade
        endif
        Veta='%Use old'
        call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.IPlaneOld.le.0)
      enddo
      write(Cislo,FormI15) NumberOfPlanes+1
      call Zhusti(Cislo)
      LabelPlaneNew='Plane#'//Cislo(:idel(Cislo))
      dpoms=FeTxLength(LabelPlaneNew)+15.
      do i=1,NumberOfPlanes
        dpoms=max(dpoms,FeTxLength(LabelPlane(i)))
      enddo
      dpoms=dpoms+2.*EdwMarginSize+5.
      tpom=xpom+CrwgXd+10.
      Veta='=>'
      il=il-1
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpoms,EdwYd,1)
      nEdwNewName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,LabelPlaneNew)
      il=il+1
      nLblOldPlane=0
      nRolMenuOldPlane=0
      if(NumberOfPlanes.gt.1) then
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',
     1                          dpoms+EdwYd,EdwYd,1)
        nRolMenuOldPlane=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                          NumberOfPlanes,max(IPlane,1))
      else if(NumberOfPlanes.eq.1) then
        call FeQuestLblMake(id,tpom,il,'=>     '//LabelPlane(1),'L','N')
      endif
      xpom=xpom+dpoms+EdwYd+15.
      Veta='%Rename/Delete plane'
      if(NumberOfPlanes.gt.1) then
        i=idel(Veta)+1
        Veta(i:i)='s'
      endif
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRenDel=ButtonLastMade
      il=il+1
      dpom=150.
      do i=1,3
        il=il+1
        xpom=40.
        write(Veta(1:1),'(i1)') i
        Veta=Veta(1:1)//nty(i)
        call FeQuestLblMake(id,5.,il,Veta,'L','N')
        do j=1,3
          Veta=' '
          if(i.eq.1) then
            if(j.eq.1) then
              Veta='Atom'
            else if(j.eq.2) then
              Veta='Coordinates'
            endif
          else
            if(i.eq.2.and.j.eq.3) Veta='Difference to 1st'
          endif
          call FeQuestEdwMake(id,xpom+dpom*.5,il-1,xpom,il,Veta,'C',
     1                        dpom,EdwYd,1)
          if(i.eq.1.and.j.eq.1) nEdwFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      nEdwLast=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Scope','C','B')
      xpom=5.
      il=il+2
      do i=1,3
        if(i.eq.1) then
          Veta='%Interpolation step'
          j=0
        else if(i.eq.2) then
          Veta='%Scope of section'
          j=1
        else
          Veta='%1st point put to'
          j=0
        endif
        call FeQuestEdwMake(id,xpom+dpom*.5,il-1,xpom,il,Veta,'C',dpom,
     1                      EdwYd,j)
        if(i.eq.1) then
          nEdwDelta=EdwLastMade
        else if(i.eq.2) then
          nEdwScope=EdwLastMade
        else
          nEdwShift=EdwLastMade
        endif
        if(i.ne.3) xpom=xpom+dpom+10.
      enddo
      tpom=xpom
      xpom=xpom+dpom+10.
      call FeQuestLblMake(id,xpom,il,'[Ang]','L','N')
      il=il+1
      xpom=tpom+dpom*.5
      Veta='%Adjust'
      dpom=FeTxLength(Veta)+10.
      xpom=xpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdjust=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(klic.eq.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Cutoff distance for atoms'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCutOffDist=EdwLastMade
        call FeQuestLblMake(id,xpom+dpom+10.,il,'[Ang]','L','N')
        call FeQuestRealEdwOpen(nEdwCutOffDist,CutOffDist,.false.,
     1                          .false.)
      endif
      IPlane=IPlaneOld
1200  if(NumberOfPlanes.gt.0) then
        call FeQuestCrwOpen(nCrwNew,IPlane.le.0)
        call FeQuestCrwOpen(nCrwOld,IPlane.gt.0)
        call FeQuestButtonOpen(nButtRenDel,ButtonOff)
      else
        call FeQuestCrwDisable(nCrwNew)
        call FeQuestCrwDisable(nCrwOld)
        call FeQuestButtonDisable(nButtRenDel)
        if(nRolMenuOldPlane.gt.0)
     1    call FeQuestRolMenuDisable(nRolMenuOldPlane)
        IPlane=0
      endif
1250  if(nCrwOld.ne.0) then
        if(CrwStateQuest(nCrwOld).ne.CrwDisabled) then
          if(CrwLogicQuest(nCrwOld)) then
            if(nRolMenuOldPlane.gt.0) then
              if(NumberOfPlanes.gt.1) then
                call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                                  NumberOfPlanes,max(IPlane,1))
              else
                call FeQuestButtonDisable(nButtRenDel)
                call FeQuestRolMenuDisable(nRolMenuOldPlane)
              endif
            endif
            LabelPlaneNew=EdwStringQuest(nEdwNewName)
            call FeQuestEdwDisable(nEdwNewName)
            IPlane=max(IPlane,1)
          else
            if(nRolMenuOldPlane.gt.0)
     1        call FeQuestRolMenuDisable(nRolMenuOldPlane)
            call FeQuestStringEdwOpen(nEdwNewName,LabelPlaneNew)
            IPlane=0
          endif
        endif
      else
        IPlane=0
      endif
1300  if(IPlane.gt.0) then
        call ConReadKeys
        if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 1390
        else
          go to 1400
        endif
      endif
1390  call SetIntArrayTo(SelPlane,3,0)
      call SetRealArrayTo(XPlane,9,-1111.)
      call SetRealArrayTo(DxPlane,6,-1111.)
      call SetStringArrayTo(StPlane,3,' ')
      DeltaPlane=-1111.
      call SetStringArrayTo(StPlane,3,' ')
      call SetRealArrayTo(ScopePlane,2,2.)
      ScopePlane(3)=0.
      DrawAtN=0
      do i=1,3
        ShiftPlane(i)=.5*ScopePlane(i)
      enddo
1400  call CopyVek(ScopePlane,xgo,3)
      if(DeltaPlane.lt.0.) then
        if(DrawPDF) then
          DeltaPlane=0.05
        else
          DeltaPlane=1.
          do i=1,3
            DeltaPlane=min(DeltaPlane,dx(i)*CellParCon(i))
          enddo
          rad=10.**Exponent10(DeltaPlane)
          DeltaPlane=anint(DeltaPlane/rad)*rad
          DeltaPlane=max(.01,DeltaPlane)
        endif
      endif
      if(SelPlane(1).eq.0.or.at.ne.' ') then
        if(at.ne.' ') then
          call CtiAt(at,xg,ich)
          if(ich.eq.-1) then
            StPlane(1)=at
            call CopyVek(xg,XPlane,3)
            SelPlane(1)=1
          else
            call CopyVek(xg,XPlane,3)
            SelPlane(1)=2
          endif
          do i=2,3
            if(SelPlane(i).le.2) then
              do j=1,3
                DxPlane(j,i)=XPlane(j,i)-XPlane(j,1)
              enddo
            else if(SelPlane(i).eq.3) then
              do j=1,3
                XPlane(j,i)=XPlane(j,1)+DxPlane(j,i)
              enddo
            endif
          enddo
        else
          StPlane(1)=' '
        endif
      endif
      nEdw=nEdwFirst
      do i=1,3
        do j=1,3
          if(j.eq.1) then
            call FeQuestStringEdwOpen(nEdw,StPlane(i))
          else if(j.eq.2) then
            call FeQuestRealAEdwOpen(nEdw,XPlane(1,i),3,
     1                               XPlane(1,i).lt.-1000.,.false.)
          else if(j.eq.3.and.i.gt.1) then
            call FeQuestRealAEdwOpen(nEdw,DxPlane(1,i),3,
     1                               DxPlane(1,i).lt.-1000.,.false.)
          endif
          if(SelPlane(i).eq.j) then
            call FeQuestEdwSelect(nEdw)
          else if(i.ne.1.or.j.ne.3) then
            call FeQuestEdwDeselect(nEdw)
          endif
          nEdw=nEdw+1
        enddo
      enddo
      call FeQuestRealEdwOpen(nEdwDelta,DeltaPlane,.false.,.false.)
      call FeQuestRealAEdwOpen(nEdwScope,ScopePlane,3,.false.,.false.)
      call FeQuestRealAEdwOpen(nEdwShift,ShiftPlane,3,.false.,.false.)
1500  nEdw=nEdwFirst
      do i=1,9
        if(i.ne.3) StOld(i)=EdwStringQuest(nEdw)
        nEdw=nEdw+1
      enddo
1520  IPlaneOld=IPlane
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1250
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuOldPlane) then
        IPlane=RolMenuSelectedQuest(nRolMenuOldPlane)
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNewName) then
        LabelPlaneNew=EdwStringQuest(nEdwNewName)
        IPlane=LocateInStringArray(LabelPlane,NumberOfPlanes,
     1                             LabelPlaneNew,.true.)
        if(IPlane.gt.0) then
          if(FeYesNo(-1.,-1.,'The plane "'//
     1                       LabelPlaneNew(:idel(LabelPlaneNew))//
     2                       '" already exists. Do you want to load it?'
     3                      ,1)) then
            call FeQuestCrwOff(nCrwNew)
            call FeQuestEdwDisable(nEdwNewName)
            call FeQuestCrwOn(nCrwOld)
            call FeQuestRolMenuOpen(nRolMenuOldPlane,LabelPlane,
     1                              NumberOfPlanes,max(IPlane,1))
            call FeReleaseOutput
            call FeDeferOutput
            EdwActive=0
            go to 1300
          else
            EventType=EventEdw
            EventNumber=nEdwNewName
            IPlane=0
          endif
        endif
        go to 1520
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwFirst.and.
     1                                  CheckNumber.le.nEdwLast) then
        t256=EdwStringQuest(CheckNumber)
        iw=CheckNumber-nEdwFirst+1
        if(t256.eq.StOld(iw)) go to 1520
        ip=(iw-1)/3+1
        it=mod(iw-1,3)+1
        nEdw=(ip-1)*3+nEdwFirst
        do i=1,3
          if(ip.ne.1.or.i.lt.3) call FeQuestEdwDeselect(nEdw)
          nEdw=nEdw+1
        enddo
        if(it.eq.1) then
          if(t256.eq.' ') go to 1520
          call CtiAt(t256,xg,ich)
          if(ich.eq.-2) then
            go to 1565
          else if(ich.eq.0) then
            Veta='the atom isn''t present, try again.'
            go to 1560
          endif
          go to 1570
1560      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
1565      EventType=EventEdw
          EventNumber=CheckNumber
          go to 1520
1570      call FeQuestRealAEdwOpen(CheckNumber+1,xg,3,.false.,.false.)
          if(ip.ne.1) then
            do i=1,3
              xg(i)=xg(i)-EdwRealQuest(i,nEdwFirst+1)
            enddo
            call FeQuestRealAEdwOpen(CheckNumber+2,xg,3,.false.,.false.)
          endif
        else if(it.eq.2) then
          call FeQuestStringEdwOpen(CheckNumber-1,' ')
          if(ip.ne.1) then
            do i=1,3
              xg(i)=EdwRealQuest(i,CheckNumber)-
     1              EdwRealQuest(i,nEdwFirst+1)
            enddo
            call FeQuestRealAEdwOpen(CheckNumber+1,xg,3,.false.,.false.)
          endif
        else if(it.eq.3) then
          call FeQuestStringEdwOpen(CheckNumber-2,' ')
          do i=1,3
            xg(i)=EdwRealQuest(i,CheckNumber)+
     1            EdwRealQuest(i,nEdwFirst+1)
          enddo
          call FeQuestRealAEdwOpen(CheckNumber-1,xg,3,.false.,.false.)
        endif
        if(ip.eq.1) then
          do i=2,3
            if(SelPlane(i).eq.0) then
              cycle
            else if(SelPlane(i).lt.3) then
              k=3*i+nEdwFirst-2
              do j=1,3
                xg(j)=EdwRealQuest(j,k)-EdwRealQuest(j,nEdwFirst+1)
              enddo
              call FeQuestRealAEdwOpen(k+1,xg,3,.false.,.false.)
            else if(SelPlane(i).eq.3) then
              k=3*i+nEdwFirst-1
              do j=1,3
                xg(j)=EdwRealQuest(j,k)+EdwRealQuest(j,nEdwFirst+1)
              enddo
              call FeQuestRealAEdwOpen(k-1,xg,3,.false.,.false.)
            endif
          enddo
        endif
        SelPlane(ip)=it
        call FeQuestEdwSelect(CheckNumber)
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwScope) then
        if(EdwStringQuest(nEdwShift).eq.' ') go to 1770
        call FeQuestRealAFromEdw(nEdwScope,xg)
        call FeQuestRealAFromEdw(nEdwShift,yg)
        do i=1,3
          yg(i)=yg(i)*2.
        enddo
        if(eqrv(xgo,yg,3,.0001)) then
          go to 1770
        else
          if(eqrv(xg,yg,3,.0001)) call CopyVek(xg,xgo,3)
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdjust)
     1  then
        call FeQuestRealAFromEdw(nEdwScope,xg)
        go to 1770
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRenDel)
     1  then
        WizardModeSave=WizardMode
        WizardMode=.false.
        idp=NextQuestId()
        ln=NextLogicNumber()
        open(ln,file=fln(:ifln)//'_planes.tmp')
        write(ln,'(a)')(LabelPlane(i)(:idel(LabelPlane(i))),
     1                  i=1,NumberOfPlanes)
        close(ln)
        xqd=250.
        il=10
        call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
        xpom=5.
        il=9
        call FeQuestSbwMake(idp,xpom,il,xqd-2.*xpom-SbwPruhXd,9,1,
     1                      CutTextFromRight,SbwVertical)
        nSbwModify=SbwLastMade
        call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'_planes.tmp')
        il=il+1
        Veta='%Rename'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=xqd*.5-1.5*dpom-10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtRename=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%Delete'
        xpom=xpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtDelete=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='Re%set'
        xpom=xpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtReset=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1730    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtRename.or.CheckNumber.eq.nButtDelete.or.
     2      CheckNumber.eq.nButtReset)) then
          ItemFromOld=SbwItemFromQuest(nSbwModify)
          ItemSelOld=SbwItemPointerQuest(nSbwModify)
          if(CheckNumber.eq.nButtRename) then
            t256=SbwStringQuest(ItemSelOld,nSbwModify)
            idr=NextQuestId()
            Veta='Rename "'//t256(:idel(t256))//'" to'
            xqdr=FeTxLength(Veta)+60.
            il=1
            call FeQuestCreate(idr,-1.,-1.,xqdr,il,Veta,0,LightGray,0,0)
            xpom=5.
            dpom=xqdr-10.
            il=1
            call FeQuestEdwMake(idr,0.,il,xpom,il,' ','C',dpom,EdwYd,0)
            nEdwRename=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,t256)
1740        call FeQuestEvent(idr,ich)
            if(CheckType.ne.0) then
              call NebylOsetren
              go to 1740
            endif
            if(ich.eq.0) t256=EdwStringQuest(nEdwRename)
            call FeQuestRemove(idr)
            if(ich.ne.0) go to 1730
          else if(CheckNumber.eq.nButtDelete) then
            t256=DelString
          else if(CheckNumber.eq.nButtReset) then
            t256=LabelPlane(ItemSelOld)
          endif
          call CloseIfOpened(SbwLnQuest(nSbwModify))
          call RewriteLinesOnFile(SbwFileQuest(nSbwModify),ItemSelOld,
     1                            ItemSelOld,ta256,1)
          call FeQuestSbwMenuOpen(nSbwModify,SbwFileQuest(nSbwModify))
          call FeQuestSbwShow(nSbwModify,ItemFromOld)
          call FeQuestSbwItemOff(nSbwModify,
     1                           SbwItemFromQuest(nSbwModify))
          call FeQuestSbwItemOn(nSbwModify,ItemSelOld)
          go to 1730
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1730
        endif
        if(ich.eq.0) then
          call CloseIfOpened(SbwLnQuest(nSbwModify))
          lni=NextLogicNumber()
          call OpenFile(lni,fln(:ifln)//'.l51','formatted','old')
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'.l52','formatted','unknown')
          ln =NextLogicNumber()
          call OpenFile(ln ,fln(:ifln)//'_planes.tmp','formatted',
     1                  'unknown')
1750      read(lni,FormA,end=1760) Veta
          write(lno,FormA) Veta(:idel(Veta))
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'contour')) then
            n=0
            NumberOfPlanes=0
            Skip=.false.
1755        read(lni,FormA,end=1760) Veta
            k=0
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,idContour(nCmdLabelPlane))) then
              n=n+1
              read(ln,FormA,end=1760) Veta
              skip=EqIgCase(Veta,DelString)
              if(.not.skip) then
                NumberOfPlanes=NumberOfPlanes+1
                call ConReallocatePlanes
                LabelPlane(NumberOfPlanes)=Veta
                Veta=idContour(nCmdLabelPlane)
     1               (:idel(idContour(nCmdLabelPlane))+1)//
     2               Veta(:idel(Veta))
              else
                go to 1755
              endif
            else if(EqIgCase(Cislo,idContour(nCmdEndPlane))) then
              if(skip) then
                skip=.false.
                go to 1755
              endif
            else if(EqIgCase(Cislo,'end')) then
              write(lno,FormA) Veta(:idel(Veta))
              go to 1750
            else
              if(skip) go to 1755
            endif
            write(lno,FormA) Veta(:idel(Veta))
            go to 1755
          else
            go to 1750
          endif
1760      call CloseIfOpened(lni)
          call CloseIfOpened(lno)
          close(ln,status='delete')
          if(FileDiff(fln(:ifln)//'.l52',fln(:ifln)//'.l51')) then
            call MoveFile(fln(:ifln)//'.l52',fln(:ifln)//'.l51')
            Change=.true.
          else
            call DeleteFile(fln(:ifln)//'.l52')
          endif
        endif
        call FeQuestRemove(idp)
        WizardMode=WizardModeSave
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 1800
1770  call CopyVek(xg,xgo,3)
      do i=1,3
        xg(i)=xg(i)*.5
      enddo
      call FeQuestRealAEdwOpen(nEdwShift,xg,3,.false.,.false.)
      if(CheckType.eq.EventButton) then
        EventType=EventEdw
        EventNumber=nEdwShift
      endif
      go to 1500
1800  if(ich.eq.0) then
        if(EdwStateQuest(nEdwNewName).eq.EdwOpened) then
          LabelPlaneNew=EdwStringQuest(nEdwNewName)
        else
          LabelPlaneNew=' '
        endif
        nEdw=nEdwFirst
        do i=1,3
          StPlane(i)=EdwStringQuest(nEdw)
          nEdw=nEdw+1
          call FeQuestRealAFromEdw(nEdw,XPlane(1,i))
          nEdw=nEdw+1
          if(i.ne.1) call FeQuestRealAFromEdw(nEdw,DxPlane(1,i))
          nEdw=nEdw+1
        enddo
        call FeQuestRealFromEdw(nEdwDelta,DeltaPlane)
        call FeQuestRealAFromEdw(nEdwScope,ScopePlane)
        call FeQuestRealAFromEdw(nEdwShift,ShiftPlane)
        if(klic.eq.1) call FeQuestRealFromEdw(nEdwCutOffDist,CutOffDist)
      endif
      if(ich.ne.0) then
        if(Change) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to save changes in '//
     1       'plane definition?',1)) call iom50(0,0,fln(:ifln)//'.m50')
          call DefaultContour
          call NactiContour
          if(ErrFlag.ne.0) then
            ErrFlag=0
            call DefaultContour
          endif
        endif
        if(.not.WizardMode) call FeQuestRemove(id)
        go to 9999
      else
        MuzeZpet=1
        go to 2000
      endif
      entry ConMakeGenSection(ich)
      MuzeZpet=0
      if(NDimI(KPhase).gt.0) call TrOrtho(1)
2000  call TrPor
      if(MuzeZpet.ne.0) then
        do i=1,3
          if(SelPlane(i).le.0) go to 9000
        enddo
      endif
      pom=0.
      do i=1,3
        xr(i)=XPlane(i,2)-XPlane(i,1)
        pom=pom+abs(xr(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTens(1,nsubs,KPhase),xr,xg,3,3,1)
      call vecnor(xr,xg)
      pom=0.
      do i=1,3
        yr(i)=XPlane(i,3)-XPlane(i,1)
        pom=pom+abs(yr(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTens(1,nsubs,KPhase),xr,xg,3,3,1)
      call multm(MetTens(1,nsubs,KPhase),yr,yg,3,3,1)
      pom=-scalmul(xr,yg)/scalmul(xr,xg)
      do i=1,3
        yr(i)=yr(i)+pom*xr(i)
      enddo
      call multm(MetTens(1,nsubs,KPhase),yr,yg,3,3,1)
      call vecnor(yr,yg)
      p3(1)=xr(2)*yr(3)-yr(2)*xr(3)
      p3(2)=xr(3)*yr(1)-yr(3)*xr(1)
      p3(3)=xr(1)*yr(2)-yr(1)*xr(2)
      pom=0.
      do i=1,3
        pom=pom+abs(p3(i))
      enddo
      if(pom.lt..0001) go to 9000
      call multm(MetTensI(1,nsubs,KPhase),p3,zr,3,3,1)
      call vecnor(zr,p3)
      do i=1,3
        trobi(i,1)=xr(i)
        trobi(i,2)=yr(i)
        trobi(i,3)=zr(i)
        xo(i)=-ShiftPlane(1)*xr(i)-ShiftPlane(2)*yr(i)
     1        -ShiftPlane(3)*zr(i)
      enddo
      do i=1,3
        xob(i)=xo(i)+XPlane(i,1)
      enddo
      call matinv(trobi,trob,pom,3)
      do i=1,3
        xr(i)=DeltaPlane*xr(i)
        yr(i)=DeltaPlane*yr(i)
        zr(i)=DeltaPlane*zr(i)
        nxr(i)=nint(ScopePlane(i)/DeltaPlane)+1
        xminr(i)=-ShiftPlane(i)
        xmaxr(i)= xminr(i)+float(nxr(i)-1)*DeltaPlane
        dxr(i)=DeltaPlane
      enddo
      if(nxr(1).le.1.or.nxr(2).le.1) go to 9000
      go to 9999
9000  call FeChybne(-1.,-1.,'the plane isn''t properly defined, '//
     1              'try again',' ',SeriousError)
      if(MuzeZpet.eq.1) then
        call FeQuestButtonOff(ButtonOk-ButtonFr+1)
        go to 1520
      else
        ich=1
      endif
9999  if(.not.WizardMode.and.MuzeZpet.ne.0) call FeQuestRemove(id)
      if(NDimI(KPhase).gt.0) call TrOrtho(0)
      return
      end
