      subroutine FeHardCopy(Type,action)
      include 'fepc.cmn'
      character*256 Veta,t256
      character*240 FePrikaz
      character*8  SvFile
      character*(*) Action
      integer Type,WhiteOld,BlackOld
      save WhiteOld,BlackOld
      if(Type.eq.0) return
      if(Action.eq.'open') then
        if(Type.lt.HardCopyNum) then
          call FeOpenHC(Type,HCFileName)
          if(InvertWhiteBlack) then
            WhiteOld=White
            BlackOld=Black
            White=BlackOld
            Black=WhiteOld
            ColorNumbers(1)=Black
            ColorNumbers(2)=White
            SvFile='pcxp.bmp'
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       SvFile)
          endif
        else
          call OpenFile(85,HCFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
        endif
      else if(Action.eq.'close') then
        if(Type.lt.HardCopyNum) then
          call FeCloseHC(Type,HCFileName)
          if(InvertWhiteBlack) then
            White=WhiteOld
            Black=BlackOld
            ColorNumbers(1)=Black
            ColorNumbers(2)=White
            call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       SvFile,0)
          endif
          if(Tiskne) then
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            Tiskne=.false.
          endif
        else
          close(85)
        endif
      endif
9999  ErrFlag=0
      return
      end
