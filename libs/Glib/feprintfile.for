      subroutine FePrintFile(PrinterName,FileName,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) FileName,PrinterName
      character*1 Bytes(:)
      character*80 Veta1,Veta2
      integer FeFileSize,FeReadBinaryFile,FeOpenBinaryFile,
     1        FeCloseBinaryFile,hPrinter,dwBytesWritten,BytesWritten
      allocatable Bytes
      type(DOC_INFO_1) DocInfo
      nBytes=FeFileSize(FileName)
      if(allocated(Bytes)) deallocate(Bytes)
      allocate(Bytes(nBytes))
      ln=FeOpenBinaryFile(FileName)
      i=FeReadBinaryFile(ln,Bytes,nBytes)
      i=FeCloseBinaryFile(ln)
      Veta1=PrinterName(:idel(PrinterName))//char(0)
      i=OpenPrinterA(carg(offset(Veta1)),carg(offset(hPrinter)),
     1               carg(NULL))
      ich=0
      if(i.eq.0) then
        ich=11
        go to 9999
      endif
      Veta1='My Document'//char(0)
      Veta2='RAW'//char(0)
      DocInfo.pDocName=offset(Veta1)
      DocInfo.pOutputFile=NULL
      DocInfo.pDatatype=offset(Veta2)
      if(StartDocPrinterA(carg(hPrinter),carg(1),carg(offset(DocInfo)))
     1   .eq.0) then
        call ClosePrinter(carg(hPrinter))
        ich=2
        go to 9999
      endif
      if(StartPagePrinter(carg(hPrinter)).eq.0) then
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=3
        go to 9999
      endif
      if(WritePrinter(carg(hPrinter),carg(offset(Bytes)),carg(nBytes),
     1                carg(offset(BytesWritten))).eq.0) then
        i=EndPagePrinter(carg(hPrinter))
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=4
        go to 9999
      endif
      if(EndPagePrinter(carg(hPrinter)).eq.0) then
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=5
        go to 9999
      endif
      if(EndDocPrinter(carg(hPrinter)).eq.0) then
        call ClosePrinter(carg(hPrinter))
        ich=6
        go to 9999
      endif
      call ClosePrinter(carg(hPrinter))
      if(ich.eq.0.and.PrinterName.ne.PSPrinter) then
        call FeRewriteIni('PSPrinter',PrinterName)
        PSPrinter=PrinterName
      endif
9999  if(allocated(Bytes)) deallocate(Bytes)
      return
      end
