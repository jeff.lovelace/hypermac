      subroutine FeGraphicViewer(FileName,Navrat)
      include 'fepc.cmn'
      character*(*) FileName
      character*256 t256
      i=idel(CallGraphic)
      if(CallGraphic(i:i).eq.'&') i=i-1
      t256=CallGraphic
      if(LocateSubstring(t256,'atoms',.false.,.true.).gt.0) then
        NInfo=3
        TextInfo(1)='JANA2006 will now start the ATOMS program to '//
     1              'draw your structure.'
        TextInfo(2)='The pre-prepared file named "'//
     1               FileName(:idel(FileName))//'" can be read in '
        TextInfo(3)='by the internal import command.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        Klic=1
      else
        Klic=0
      endif
      if(Klic.eq.1) then
        t256=CallGraphic(:i)
      else
        t256=CallGraphic(:i)//' '//FileName(:idel(FileName))
      endif
      if(Navrat.eq.0) t256=t256(:idel(t256))//'&'
      call FeSystem(t256)
      return
      end
