      subroutine FeSaveImage(xlow,xhigh,ylow,yhigh,File)
      include 'fepc.cmn'
      character*(*) File
      integer FeOpenBinaryFile,FeCloseBinaryFile,FeWriteBinaryFile
      logical EqIgCase
      call JanaToClient(xlow,ylow,ix1,iy1)
      call JanaToClient(xhigh,yhigh,ix2,iy2)
      if(EqIgCase(File,HCFileName)) go to 2000
      iu=0
      ip=0
      do i=1,MxBmp
        if(EqIgCase(File,BmpName(i))) then
          iu=i
          go to 1500
        endif
        if(ip.eq.0.and.BmpName(i).eq.' ') ip=i
      enddo
      if(ip.eq.0) go to 2000
      iu=ip
1500  BmpName(iu)=File
      if(iu.eq.ip) then
        IBmp(iu)=0
      else
        call FeBitmapDestroy(IBmp(iu))
      endif
      call FeBitmapGet(IBmp(iu),ix1,iy1,ix2,iy2)
      ln=NextLogicNumber()
      open(ln,file=File)
      write(ln,'(i5)') iu
      close(ln)
      if(iu.eq.ip) then
        go to 9000
      else
        go to 9999
      endif
2000  call FeSaveBMPFile(File,ix1,ix2,iy1,iy2,ich)
      if(EqIgCase(File,HCFileName)) go to 9999
9000  call FeTmpFilesAdd(File)
9999  return
      end
