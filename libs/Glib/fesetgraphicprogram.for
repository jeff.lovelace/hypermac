      subroutine FeSetGraphicProgram
      include 'fepc.cmn'
      include 'basic.cmn'
      character*4 String
      character*55 :: RegSubKey(2) = (/
     1   'Software\Crystal Impact\Diamond 3\Import Assistant\',
     2   'Software\Crystal Impact\Diamond 3\Picture\         '/)
      character*15 :: ValueName(2) = (/'Enable     ',
     1                                 'AutoStartTS'/)
      integer ::
     1        ValueOld(2),ValueNew(2)=(/0,1/),ValueDefault(2)=(/1,2/)
      logical FeGetStringFromReg
      save ValueOld
      RegSubKey(1)=RegSubKey(1)(:idel(RegSubKey(1)))//char(0)
      RegSubKey(2)=RegSubKey(2)(:idel(RegSubKey(2)))//char(0)
      ValueName(1)=ValueName(1)(:idel(ValueName(1)))//char(0)
      ValueName(2)=ValueName(2)(:idel(ValueName(2)))//char(0)
      if(LocateSubstring(CallGraphic,'Diamond.exe',
     1                   .false.,.true.).le.0) go to 9999
      do i=1,2
        if(FeGetStringFromReg(RegSubKey(i),ValueName(i),String)) then
          ValueOld(i)=0
          do k=4,1,-1
            ValueOld(i)=256*ValueOld(i)+ichar(String(k:k))
          enddo
        else
          ValueOld(i)=ValueDefault(i)
        endif
        if(ValueNew(i).ne.ValueOld(i)) then
          k=ValueNew(i)
          do j=1,4
            kk=mod(k,256)
            String(j:j)=char(kk)
            k=k/256
          enddo
          call FeSetRegDWORD(RegSubKey(i),ValueName(i),String)
        endif
      enddo
      go to 9999
      entry FeResetGraphicProgram
      if(LocateSubstring(CallGraphic,'Diamond.exe',
     1                   .false.,.true.).le.0) go to 9999
      do i=1,2
        if(ValueNew(i).ne.ValueOld(i)) then
          k=ValueOld(i)
          do j=1,4
            kk=mod(k,256)
            String(j:j)=char(kk)
            k=k/256
          enddo
          call FeSetRegDWORD(RegSubKey(i),ValueName(i),String)
        endif
      enddo
9999  return
      end
