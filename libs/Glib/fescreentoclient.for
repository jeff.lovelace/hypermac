      subroutine FeScreenToClient(hWndP,sx,sy,cx,cy)
      use Jana_windows
      integer hWndP,cx,cy,sx,sy
      type(POINT) pnt
      pnt.x=sx
      pnt.y=sy
      i=ScreenToClient(carg(hWndP),carg(offset(pnt)))
      cx=pnt.x
      cy=pnt.y
      return
      end
