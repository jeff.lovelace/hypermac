      subroutine FeTextOut(hDCP,ix1,ix2,iy1,iy2,Color,String)
      use Jana_windows
      integer hDCP,Color,RGB
      character*(*) String
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SetBkMode(carg(hDCP),carg(TRANSPARENT))
      call SetTextColor(carg(hDCP),carg(RGB(IRed,IGreen,IBlue)))
      idl=idel(String)
      call TextOutA(carg(hDCP),carg(ix1),carg(iy1),carg(String(:idl)),
     1              carg(idl))
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      if(hDCMeta.ne.0) then
        call SetBkMode(carg(hDCMeta),carg(TRANSPARENT))
        call SetTextColor(carg(hDCMeta),carg(RGB(IRed,IGreen,IBlue)))
        call TextOutA(carg(hDCMeta),carg(ix1),carg(iy1),
     1                carg(String(:idl)),carg(idl))
      endif
      return
      end
