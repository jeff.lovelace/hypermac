      subroutine FePolylineVarious(hDCP,x,y,n,Color,Klic)
      use Jana_windows
      integer hDCP,Color,RGB,hPen
      integer x(*),y(*)
      type(POINT) pnt(:)
      character*80 Veta
      allocatable pnt
      allocate(pnt(n))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do i=1,n
        pnt(i).x=x(i)
        pnt(i).y=y(i)
        ix1=min(ix1,x(i))
        ix2=max(ix2,x(i))
        iy1=min(iy1,y(i))
        iy2=max(iy2,y(i))
      enddo
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      if(Klic.eq.1) then
        i=PS_SOLID
      else
        i=PS_DOT
      endif
      hPen=CreatePen(carg(i),carg(0),
     1               carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCP),carg(hPen))
      call SelectObject(carg(hDCP),carg(hDCBitmap))
      call Polyline(carg(hDCP),carg(offset(Pnt)),carg(n))
      if(hDCMeta.ne.0) then
       call SelectObject(carg(hDCMeta),carg(hPen))
       call Polyline(carg(hDCMeta),carg(offset(Pnt)),carg(n))
      endif
      call DeleteObject(carg(hPen))
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      deallocate(pnt)
      return
      end
