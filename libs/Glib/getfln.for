      subroutine GetFln
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 p256,s256
      integer FeChdir,Delka
      logical EqIgCase
      BatchMode=.false.
      HomeDir=' '
      call getcl(CommandLine)

c    absoft
c      call get_command(CommandLine,Delka,ich)
c      if(ich.ne.0) go to 9999
c    absoft
      k=0
      n=0
      VasekDebug=0
      HKLUpdate=.false.
      DirectoryDelimitor=ObrLom
1000  kp=k
      call kusap(CommandLine,k,p256)
      if(p256(1:1).eq.'@') then
        BatchMode=.true.
        BatchFile=p256(2:)
        open(BatchLN,file=BatchFile)
        call FeGetCurrentDir
        BatchDir=CurrentDir
        n=n+10000
      else if(EqIgCase(p256,'-debug')) then
        VasekDebug=1
        n=n+1000
      else if(EqIgCase(p256,'-autohklupdate')) then
        HKLUpdate=.true.
        n=n+100
      else if(EqIgCase(p256,'-HomeDir')) then
        call kusap(CommandLine,k,HomeDir)
        n=n+10
      else
        if(p256(1:1).ne.'"'.and.k.lt.256) then
1500      i=k
          call kusap(CommandLine,k,s256)
          if(s256(1:1).ne.'@'.and..not.EqIgCase(s256,'-debug').and.
     1       .not.EqIgCase(s256,'-autohklupdate').and.
     2       .not.EqIgCase(s256,'-HomeDir')) then
            if(k.lt.256) then
              go to 1500
            else
              i=k
            endif
          endif
          p256=CommandLine(kp+1:i)
          k=i
        endif
        ifln=idel(p256)
        if(ifln.le.0) then
          fln=' '
        else
          call ExtractDirectory(p256,s256)
          i=FeChdir(s256)
          call ExtractFileName(p256,s256)
          call GetPureFileName(s256,fln)
          ifln=idel(fln)
          n=n+1
        endif
      endif
      if(n.ne.11111.and.k.lt.256) go to 1000
9999  return
      end
