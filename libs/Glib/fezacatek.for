      subroutine FeZacatek
      use FileDirList_mod
      include 'fepc.cmn'
      character*128 FileToDelete
      character*5  ExtVen(20)
      integer DeleteReadOnly
      logical EqWild,FeYesNoHeader,FeReadOnly
      data ExtVen/'*.bmp','*.tmp','*.m40','*.m50','*.m80',
     1            '*.m81','*.m91','*.s40','*.s50','*.fou',
     2            '*.ref','*.m70','     ','     ','     ',
     3            '     ','     ','     ','     ','     '/
      call FeGetFileDirList(TmpDir)
      j=0
      do 1200i=1,NFiles
        do l=1,20
          if(ExtVen(l).eq.' ') go to 1200
          if(EqWild(FileList(i),ExtVen(l),.false.)) then
            j=j+1
            exit
          endif
        enddo
1200  continue
      DeleteReadOnly=1
      if(j.gt.30.and..not.BatchMode) then
        Ninfo=1
        write(TextInfo(1),'(i5)') j-1
        call zhusti(TextInfo(1))
        TextInfo(1)='There are '//TextInfo(1)(:idel(TextInfo(1)))//
     1              ' temporary files which are probably idle'
        if(FeYesNoHeader(-1.,-1.,'Do you want to delete them?',1)) then
          do 2000i=1,NFiles
            do j=1,20
              if(ExtVen(j).eq.' ') go to 2000
              if(EqWild(FileList(i),ExtVen(j),.false.)) then
                FileToDelete=TmpDir(:idel(TmpDir))//FileList(i)
                if(FeReadOnly(FileToDelete)) then
                  if(DeleteReadOnly.le.2) then
                    call FeYesNoAll(-1.,-1.,
     1                'File "'//FileToDelete(:idel(FileToDelete))//
     2                '" is READONLY. Delete it?',1,DeleteReadOnly)
                  endif
                  if(DeleteReadOnly.eq.1.or.DeleteReadOnly.eq.3) then
                    call FeResetReadOnly(FileToDelete)
                    call DeleteFile(FileToDelete)
                  endif
                else
                  call DeleteFile(FileToDelete)
                endif
                go to 2000
              endif
            enddo
2000      continue
        endif
      endif
c3000  call ndpexc()
3000  return
      end
