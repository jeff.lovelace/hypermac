      subroutine FeSetRegDWORD(RegSubKey,ValueName,String)
      use Jana_windows
      character*(*) RegSubKey,ValueName,String
      integer RegKey
      i=RegOpenKeyExA(carg(HKEY_CURRENT_USER),
     1                carg(offset(RegSubKey)),carg(NULL),
     2                carg(KEY_ALL_ACCESS),carg(offset(RegKey)))
      if(i.eq.0) then
        i=RegSetValueExA(carg(RegKey),carg(offset(ValueName)),
     1                   carg(NULL),carg(REG_DWORD),
     2                   carg(offset(String)),carg(4))
      endif
      return
      end
