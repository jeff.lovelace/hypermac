      function FeTxLength(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      VetaP=Veta
      i=index(VetaP,Tabulator)
      if(NTabs(UseTabs).le.0.or.i.le.0) then
        FeTxLength=FeTxLengthSpace(Veta(:idel(Veta)))
      else
        k=1
        it=0
        xm=0.
1100    i=index(VetaP(k:),Tabulator)
        if(i.le.0) go to 1500
        k=k+i
        it=it+1
        xm=xm+XTabs(it,UseTabs)
        if(it.gt.1) xm=xm-XTabs(it-1,UseTabs)
        go to 1100
1500    if(it.gt.0) then
          if(TTabs(it,UseTabs).eq.IdLeftTab) then
            xmp=xm-FeTxLengthSpace(Veta(k:idel(Veta)))
          else if(TTabs(it,UseTabs).eq.IdCharTab) then
            i=index(VetaP(k:),ChTabs(it,UseTabs))
            if(i.gt.1) then
              xmp=xm-FeTxLengthSpace(Veta(k:k+i-2))
            else
              xmp=xm
            endif
          else if(TTabs(it,UseTabs).eq.IdCenterTab) then
            xmp=xm-.5*FeTxLengthSpace(Veta(k:idel(Veta)))
          else
            xmp=xm
          endif
          FeTxLength=xmp+FeTxLengthSpace(Veta(k:idel(Veta)))
        else
          FeTxLength=FeTxLengthSpace(Veta(:idel(Veta)))
        endif
      endif
      return
      end
