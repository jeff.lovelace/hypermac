      subroutine FeGetFileDirList(Directory)
      use Jana_windows
      use FileDirList_mod
      character*(*) Directory
      character*260 FileListO(:),DirListO(:)
      character*256 Veta
      character*1   Flag
      type (WIN32_FIND_DATA) FindData
      integer hSearchFile
      logical EqIgCase
      allocatable FileListO,DirListO
      if(.not.allocated(FileList)) then
        allocate(FileList(1),DirList(1))
        NFilesMax=1
        NDirsMax=1
      endif
      NFiles=0
      NDirs=0
      Veta=Directory(:idel(Directory))//'\*'
      hSearchFile=FindFirstFileA(carg(offset(Veta)),
     1                           carg(offset(FindData)))
      if(hSearchFile.eq.INVALID_HANDLE_VALUE) go to 9999
      do while(FindNextFileA(carg(hSearchFile),
     1                       carg(offset(FindData))))
        if(iand(FindData.dwFileAttributes,FILE_ATTRIBUTE_DIRECTORY).ne.
     1          FILE_ATTRIBUTE_DIRECTORY) then
          if(NFiles.ge.NFilesMax) then
            allocate(FileListO(NFilesMax))
            do i=1,NFiles
              FileListO(i)=FileList(i)
            enddo
            deallocate(FileList)
            NFilesMax=2*NFilesMax
            allocate(FileList(NFilesMax))
            do i=1,NFiles
              FileList(i)=FileListO(i)
            enddo
            deallocate(FileListO)
          endif
          NFiles=NFiles+1
          Veta=FindData.cFileName
          i=index(Veta,char(0))
          FileList(NFiles)=Veta(:i-1)
          cycle
        endif
        if(iand(FindData.dwFileAttributes,FILE_ATTRIBUTE_DIRECTORY).eq.
     1          FILE_ATTRIBUTE_DIRECTORY) then
          if(NDirs.ge.NDirsMax) then
            allocate(DirListO(NDirsMax))
            do i=1,NDirs
              DirListO(i)=DirList(i)
            enddo
            deallocate(DirList)
            NDirsMax=2*NDirsMax
            allocate(DirList(NDirsMax))
            do i=1,NDirs
              DirList(i)=DirListO(i)
            enddo
            deallocate(DirListO)
          endif
          NDirs=NDirs+1
          Veta=FindData.cFileName
          i=index(Veta,char(0))
          DirList(NDirs)=Veta(:i-1)
        endif
      enddo
      call FindClose(carg(hSearchFile))
9999  return
      end
