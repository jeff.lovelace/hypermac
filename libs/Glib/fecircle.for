      subroutine FeCircle(x,y,r,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,xss,yss,xjj,yjj,rs,Style,RGB
      character*80 Veta
      Style=0
      go to 1100
      entry FeCircleOpen(x,y,r,Color)
      Style=1
1100  call JanaToClient(x,y,xs,ys)
      rs=nint(r*EnlargeFactor)
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do ix=-rs,rs
        do iy=-rs,rs
          pom=sqrt(float(ix)**2+float(iy)**2)-float(rs)
          if(pom.lt..5) then
            if(Style.eq.0.or.pom.ge.-.5) then
              xjj=xs+ix
              yjj=ys+iy
              call SetPixel(carg(hDCComp),carg(xjj),carg(yjj),
     1                      carg(RGB(IRed,IGreen,IBlue)))
              if(hDCMeta.ne.0)
     1          call SetPixel(carg(hDCMeta),carg(xjj),carg(yjj),
     2                        carg(RGB(IRed,IGreen,IBlue)))
              ix1=min(ix1,xjj)
              ix2=max(ix2,xjj)
              iy1=min(iy1,yjj)
              iy2=max(iy2,yjj)
              if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS)
     1            .and.LnPS.gt.0) then
                if(Color.ne.LastColorPS) then
                  write(LnPS,'(''s'')')
                  write(Veta,'(3f8.3,'' g'')')
     1              float(IRed)/255.,float(IGreen)/255.,
     2              float(IBlue)/255.
                  call ZdrcniCisla(Veta,4)
                  write(LnPS,FormA) Veta(:idel(Veta))
                  LastColorPS=Color
                endif
                call JanaPixelToClientReverse(xjj,yjj,xss,yss)
                xp=float(xss-XPocPS)*ScalePS
                yp=float(yss-YPocPS)*ScalePS
                write(Veta,'(2f10.2,'' m cb'')') xp,yp
                call ZdrcniCisla(Veta,4)
                write(LnPS,FormA) Veta(:idel(Veta))
              endif
            endif
          endif
        enddo
      enddo
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
