      function FeTxHeight(Veta)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      integer ip1,ip2
      type(SIZE) :: Sz
      VetaP=Veta
      idl=len(Veta)
      call GetTextExtentPoint32A(carg(hDCComp),carg(Veta(:idel(Veta))),
     1                          carg(idel(Veta)),carg(offset(Sz)))
      FeTxHeight=float(Sz.cy)/EnlargeFactor
      return
      end
