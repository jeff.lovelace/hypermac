      subroutine FeAllocErr(Text,i)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text
      NInfo=4
      call iostat_msg(i,TextInfo(1))
      TextInfo(2)=Text
      TextInfo(3)='Please contact Jana authors and send them this '//
     1            'message and'
      TextInfo(4)='files m40, [m41], m50, m90 and m95.'
      call FeInfoOut(-1.,-1.,'ALLOCATION ERROR','L')
      ErrFlag=1
      return
      end
