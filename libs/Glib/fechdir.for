      integer function FeChDir(Directory)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Directory
      character*260 Veta
      character*4 Flt
      logical ExistFile,io3
      FeChDir=0
      i=idel(Directory)
      if(i.le.0) go to 9999
      Veta=Directory(:i)//char(0)
      call FeMouseShape(3)
      n=0
1000  io3=SetCurrentDirectoryA(carg(offset(Veta)))
      if(.not.io3.and.n.lt.50) then
        n=n+1
        go to 1000
      endif
      call FeGetCurrentDir
      call FeMouseShape(0)
      if(.not.io3) then
        FeChDir=1
      else
        ln=NextLogicNumber()
        Flt='xy00'
        i=0
1500    write(Flt(3:4),100) i
        if(Flt(3:3).eq.' ') Flt(3:3)='0'
        if(ExistFile(Flt)) then
          i=i+1
          if(i.gt.50) go to 2000
          go to 1500
        endif
        open(ln,file=Flt,err=2000)
        i=-1
        write(ln,100,err=2000) i
        go to 3000
2000    FeChDir=-1
      endif
3000  if(i.eq.-1) close(ln,status='delete')
9999  return
100   format(i2)
      end
