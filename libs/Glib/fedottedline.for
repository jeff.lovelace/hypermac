      subroutine FeDottedLine(x,y,ik,Color)
      include 'fepc.cmn'
      integer Color
      save xs,ys,ic
      if(ik.le.0) then
        xs=x
        ys=y
        call FePoint(xs,ys,Color)
        ic=1
      else
2100    difx=x-xs
        dify=y-ys
        delka=sqrt(difx**2+dify**2)
        if(ic.eq.0) then
          fus=0.
        else
          if(LineType.eq.DottedLine) then
            fus=3.
          else
            fus=2.
          endif
        endif
        if(delka.lt.fus.or.delka.le.0.) go to 9000
        ic=mod(ic+1,2)
        dx=fus*difx/delka
        dy=fus*dify/delka
        xs=anint((xs+dx)*EnlargeFactor)/EnlargeFactor
        ys=anint((ys+dy)*EnlargeFactor)/EnlargeFactor
        if(ic.eq.0) call FePoint(xs,ys,Color)
        go to 2100
      endif
9000  return
      end
