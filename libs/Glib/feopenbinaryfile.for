      integer function FeOpenBinaryFile(FileName)
      use Jana_windows
      character*(*) FileName
      ln=NextLogicNumber()
      open(ln,file=FileName,access='TRANSPARENT',form='BINARY')
      FeOpenBinaryFile=ln
      return
      end
