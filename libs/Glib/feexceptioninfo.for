      subroutine FeExceptionInfo()
      include 'fepc.cmn'
      character*80 t80
      character*25 ErrString(0:4)
      logical BTest
      data ErrString/'"Invalid Operation"','"Denormalized Number"',
     1               '"Divide by Zero"','"Overflow"','"Underflow"'/
      j=ndperr(.true.)
      NInfo=0
      do 1000i=0,4
        if((i.eq.1.or.i.eq.4).and.DelejKontroly.le.1) go to 1000
        if(BTest(j,i)) then
          NInfo=Ninfo+1
          TextInfo(Ninfo)=ErrString(i)
        endif
1000  continue
      if(NInfo.ne.0) then
        if(NInfo.eq.1) then
          t80=' has'
        else
          t80='s have'
        endif
        t80='The following exception'//t80(:idel(t80))//
     1      ' occured during the last action:'
        call FeInfoOut(-1.,-1.,t80,'L')
      endif
      call ndpexc()
      return
      end
