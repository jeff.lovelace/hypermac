      subroutine FeSetClipboardText(Veta)
      use Jana_windows
      character(*) Veta
      character*256 VetaP
      integer hMem
      call OpenClipboard(carg(hWnd))
      call EmptyClipboard()
      VetaP=Veta(:idel(Veta))//char(0)
      idl=idel(VetaP)+1
      hMem=GlobalAlloc(carg(GHND),carg(idl))
      lp=GlobalLock(carg(hMem))
      call lstrcpy(carg(lp),carg(offset(VetaP)))
      call SetClipboardData(carg(CF_TEXT),carg(hMem))
      call GlobalUnlock(hMem)
      call CloseClipboard()
      return
      end
