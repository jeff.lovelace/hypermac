      subroutine FePolygon(x,y,n,istyle,idense,iangle,color)
      use Jana_windows
      include 'fepc.cmn'
      dimension x(n),y(n)
      integer xp(:),yp(:),xs,ys,Color
      character*80 Veta
      allocatable xp,yp
      Klic=0
      go to 1100
      entry FePolylineSolid(x,y,n,color)
      Klic=1
      go to 1100
      entry FePolylineDotted(x,y,n,color)
      Klic=2
1100  allocate(xp(n),yp(n))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do i=1,n
        call JanaToClient(x(i),y(i),xp(i),yp(i))
        ix1=min(ix1,xp(i))
        ix2=max(ix2,xp(i))
        iy1=min(iy1,yp(i))
        iy2=max(iy2,yp(i))
      enddo
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        call FeRGBUncompress(Color,IRed,IGreen,IBlue)
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x(1),y(1),xs,ys)
        xpp=float(xs-XPocPS)*ScalePS
        ypp=float(ys-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m'')') xpp,ypp
        call ZdrcniCisla(Veta,3)
        write(LnPS,FormA) Veta(:idel(Veta))
      endif
      if(Klic.eq.0) then
        call FeFillPolygon(hDCComp,xp,yp,n,Color)
        if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1      LnPS.gt.0) then
          do i=2,n
            call JanaToClientReverse(x(i),y(i),xs,ys)
            xpp=float(xs-XPocPS)*ScalePS
            ypp=float(ys-YPocPS)*ScalePS
            write(Veta,'(2f10.2,'' l'')') xpp,ypp
            call ZdrcniCisla(Veta,3)
            write(LnPS,FormA) Veta(:idel(Veta))
          enddo
          write(LnPS,FormA) 'fill'
        endif
      else
        call FePolylineVarious(hDCComp,xp,yp,n,Color,Klic)
        if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1      LnPS.gt.0) then
          if(Klic.eq.2) write(LnPS,FormA) '[1 1] 0 setdash'
          do i=2,n
            call JanaToClientReverse(x(i),y(i),xs,ys)
            xpp=float(xs-XPocPS)*ScalePS
            ypp=float(ys-YPocPS)*ScalePS
            write(Veta,'(2f10.2,'' l'')') xpp,ypp
            call ZdrcniCisla(Veta,3)
            write(LnPS,FormA) Veta(:idel(Veta))
          enddo
          write(LnPS,FormA) 's'
          if(Klic.eq.2) write(LnPS,FormA) '[] 0 setdash'
        endif
      endif
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      deallocate(xp,yp)
      return
      end
