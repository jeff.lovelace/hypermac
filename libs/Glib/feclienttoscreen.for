      subroutine FeClientToScreen(hWndP,cx,cy,sx,sy)
      use Jana_windows
      integer hWndP,cx,cy,sx,sy
      type(POINT) pnt
      pnt.x=cx
      pnt.y=cy
      i=ClientToScreen(carg(hWndP),carg(offset(pnt)))
      sx=pnt.x
      sy=pnt.y
      return
      end
