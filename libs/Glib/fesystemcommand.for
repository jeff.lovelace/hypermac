      subroutine FeSystemCommand(CommandLine,HowToStart)
      use Jana_windows
      character*(*) CommandLine
      character*260 VetaP
      integer HowToStart
      logical lpom
      type(STARTUPINFO) si
      type(PROCESS_INFORMATION) pi
      common/SysCommand/ pi
      si.cb=17*4
      si.lpReserved=0
      si.lpDesktop=0
      si.lpTitle=0
      si.dwX=0
      si.dwY=0
      si.dwXSize=0
      si.dwYSize=0
      si.dwXCountChars=0
      si.dwYCountChars=0
      si.dwFillAttribute=0
      if(HowToStart.eq.1) then
        si.dwFlags=1
        si.wShowWindow=7
      else
        si.dwFlags=0
        si.wShowWindow=0
      endif
      si.cbReserved2=0
      si.lpReserved2=0
      si.hStdInput=0
      si.hStdOutput=0
      si.hStdError=0
      pi.hProcess=0
      pi.hThread=0
      pi.dwProcessId=0
      pi.dwThreadId=0
      VetaP=CommandLine(:idel(CommandLine))//char(0)
      lpom=CreateProcessA(carg(NULL),carg(offset(VetaP)),
     1                   carg(NULL),carg(NULL),carg(FALSE),carg(0),
     2                   carg(NULL),carg(NULL),
     3                   carg(offset(si)),carg(offset(pi)))
9999  return
      end
