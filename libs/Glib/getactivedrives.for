      subroutine GetActiveDrives(Drives,n)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Drives(*)
      Mask=GetLogicalDrives()
      n=0
      do i=0,25
        if(ibits(Mask,i,1).eq.1) then
          n=n+1
          Drives(n)=char(ichar('A')+i)//':'
        endif
      enddo
      return
      end
