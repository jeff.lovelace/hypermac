      subroutine FeBitmapPut(hBitmap,ix1,iy1,ix2,iy2)
      use Jana_windows
      include 'fepc.cmn'
      integer hBitmap,Width,Height,hDCPom
      if(ix1.ne.0.or.ix2.ne.0.or.iy1.ne.0.or.iy2.ne.0) then
        Width=iabs(ix2-ix1)+1
        Height=iabs(iy2-iy1)+1
        ixp=min(ix1,ix2)
        iyp=min(iy1,iy2)
      else
        Width=PixelClientWidth
        Height=PixelClientHeight
        ixp=0
        iyp=0
      endif
      hDCPom=CreateCompatibleDC(carg(hdc))
      call SelectObject(carg(hDCPom),carg(hBitmap))
      call SelectObject(carg(hDCComp),carg(hDCBitMap))
      call BitBlt(carg(hDCComp),carg(ixp),carg(iyp),
     1            carg(Width),carg(Height),carg(hDCPom),
     2            carg(0),carg(0),carg(SRCCOPY))
      call DeleteDC(carg(hDCPom))
      return
      end
