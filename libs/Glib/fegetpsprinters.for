      subroutine FeGetPSPrinters(PSPrinters,n)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) PSPrinters(20)
      character*256 Printer,Veta
      character*10 ::  CPom
      integer :: Reserved=1600,Flags,hDCP,Returned=0
      logical EqIgCase
      type(PRINTER_INFO_1) PrInfo(:)
      allocatable PrInfo
      n=0
      do i=1,20
        PSPrinters(i)=' '
      enddo
1100  allocate(PrInfo(Reserved/16))
      Flags=ior(PRINTER_ENUM_LOCAL,PRINTER_ENUM_CONNECTIONS)
      Printer=char(0)
      i=EnumPrintersA(carg(Flags),carg(offset(Printer)),carg(1),
     1                carg(offset(PrInfo)),
     2                carg(Reserved),carg(offset(Needed)),
     3                carg(offset(Returned)))
      if(i.eq.0) then
        deallocate(PrInfo)
        Reserved=Needed+16
        go to 1100
      else
        do i=1,Returned
          Printer=' '
          call lstrcpy(carg(offset(Printer)),carg(PrInfo(i).pName))
          CPom='WINSPOOL'//char(0)
          hDCP=CreateDCA(carg(offset(CPom)),carg(offset(Printer)),
     1                   carg(NULL),carg(NULL))
          Veta=' '
          j=ExtEscape(carg(hDCP),carg(QUERYESCSUPPORT),carg(n),
     1                carg(offset(Veta)),carg(NULL),carg(NULL))
          if(j.eq.0) then
            Veta=' '
            j=ExtEscape(carg(hDCP),carg(GETTECHNOLOGY),
     1                  carg(NULL),carg(NULL),carg(256),
     2                  carg(offset(Veta)))
            k=0
1500        call DeleteString(Veta,char(0),k)
            if(k.gt.0) go to 1500
            if(EqIgCase(Veta,'Postscript')) then
              n=n+1
              PSPrinters(n)=Printer
            endif
          endif
          call DeleteDC(carg(hDCP))
        enddo
      endif
      j=0
      do i=1,n
        k=0
1600    call DeleteString(PSPrinters(i),char(0),k)
        if(k.gt.0) go to 1600
        if(PSPrinters(i).eq.PSPrinter.and.i.ne.1) j=i
      enddo
      if(j.gt.0) then
        PSPrinters(j)=PSPrinters(1)
        PSPrinters(1)=PSPrinter
      endif
9999  if(allocated(PrInfo)) deallocate(PrInfo)
      return
      end
