      function FeTxLengthSpace(Veta)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      type(SIZE) :: Sz
      VetaP=Veta
      idl=len(Veta)
      call GetTextExtentPoint32A(carg(hDCComp),carg(VetaP(:idl)),
     1                           carg(idl),carg(offset(Sz)))
      FeTxLengthSpace=float(Sz.cx)/EnlargeFactor
      return
      end
