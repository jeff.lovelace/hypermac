      subroutine FeMessage(Imm)
      use Jana_windows
      include 'fepc.cmn'
      character*1 Znak
      integer XWin,YWin
      integer wParam,hwndp,FeGetSystemTime,StTime,StTimeNew
      logical lpom
      type(MSG) zprava
      type(RECT) rct
      data XWin,YWin/2*-1111/,XPosLast,YPosLast/0.,0./,
     1     StTime/-1111./
      Zprava%hwnd=0
      Zprava%Message=0
      Zprava%wParam=0
      Zprava%lParam=0
      hwndp  =Zprava%hwnd
      Message=Zprava%Message
      wParam =Zprava%wParam
      lParam =Zprava%lParam
      StTime=FeGetSystemTime()
      KurzorColor=White
1000  EventType=0
      EventNumber=0
      if(imm.eq.0.and.KurzorEdw.le.0) then
        lpom=GetMessageA(carg(offset(Zprava)),carg(hwnd),carg(0),
     1                   carg(0))
      else
        lpom=PeekMessageA(carg(offset(Zprava)),carg(hwnd),carg(0),
     1                    carg(0),carg(PM_REMOVE))
      endif
      if(lpom) then
        hwndp  =Zprava%hwnd
        Message=Zprava%Message
        wParam =Zprava%wParam
        lParam =Zprava%lParam
        if(Message.eq.WM_KEYDOWN.or.Message.eq.WM_SYSKEYDOWN) then
          if(Message.eq.WM_KEYDOWN) then
            EventType=EventKey
          else
            EventType=EventAlt
          endif
          if(wParam.eq.VK_BACK) then
            EventNumber=JeBackspace
          else if(wParam.eq.VK_TAB) then
            if(ShiftPressed) then
              EventNumber=JeShiftTab
            else
              EventNumber=JeTab
            endif
          else if(wParam.eq.VK_RETURN) then
            EventNumber=JeReturn
          else if(wParam.eq.VK_ESCAPE) then
            EventNumber=JeEscape
          else if(wParam.eq.VK_PRIOR) then
            EventNumber=JePageUp
          else if(wParam.eq.VK_NEXT) then
            EventNumber=JePageDown
          else if(wParam.eq.VK_END) then
            if(CtrlPressed) then
              EventNumber=JeDownExtreme
            else
              EventNumber=JeEnd
            endif
          else if(wParam.eq.VK_HOME) then
            if(CtrlPressed) then
              EventNumber=JeUpExtreme
            else
              EventNumber=JeHome
            endif
          else if(wParam.eq.VK_LEFT) then
            if(CtrlPressed) then
              EventNumber=JeHome
            else
              EventNumber=JeLeft
            endif
          else if(wParam.eq.VK_UP) then
            EventNumber=JeUp
          else if(wParam.eq.VK_RIGHT) then
            if(CtrlPressed) then
              EventNumber=JeEnd
            else
              EventNumber=JeRight
            endif
          else if(wParam.eq.VK_DOWN) then
            EventNumber=JeDown
          else if(wParam.eq.VK_INSERT) then
            EventNumber=JeInsert
          else if(wParam.eq.VK_DELETE) then
            EventNumber=JeDeleteUnder
          else if(wParam.eq.VK_F1) then
            EventNumber=JeF1
          else if(wParam.eq.VK_F4) then
            EventNumber=JeF4
          else if(wParam.eq.VK_SPACE) then
            EventType=EventASCII
            EventNumber=ichar(' ')
          else if(wParam.eq.VK_SHIFT) then
            if(EventType.ne.EventAlt) then
              ShiftPressed=.true.
            else
              i=ActivateKeyboardLayout(carg(HKL_NEXT),carg(0))
            endif
            go to 1000
          else if(wParam.eq.VK_CONTROL) then
            CtrlPressed=.true.
            go to 1000
          else if(wParam.eq.VK_menu) then
            CtrlPressed=.false.
            ShiftPressed=.false.
            go to 1000
          endif
          if(EventNumber.ne.0) go to 8000
        else if(Message.eq.WM_KEYUP) then
          if(wParam.eq.VK_SHIFT) then
            ShiftPressed=.false.
          else if(wParam.eq.VK_CONTROL) then
            CtrlPressed=.false.
          endif
          go to 1000
        else if(Message.eq.WM_DESTROY) then
          EventType=EventSystem
          EventNumber=JeWinClose
        endif
        call TranslateMessage(carg(offset(Zprava)))
        hwndp  =Zprava%hwnd
        Message=Zprava%Message
        wParam =Zprava%wParam
        lParam =Zprava%lParam
        if(Message.eq.WM_CHAR) then
          EventType=EventASCII
          EventNumber=wParam
          if(EventNumber.ge.1.and.EventNumber.le.26) then
            EventType=EventCtrl
            EventNumber=EventNumber+64
          else if(EventNumber.eq.VK_RETURN) then
            EventType=EventCtrl
            EventNumber=ichar('M')
          else if(EventNumber.eq.VK_BACK) then
            EventType=EventCtrl
            EventNumber=ichar('H')
          else if(EventNumber.eq.VK_TAB) then
            EventType=EventCtrl
            EventNumber=ichar('I')
          endif
        else if(Message.eq.WM_SYSCHAR) then
          EventType=EventAlt
          Znak=char(wParam)
          call mala(Znak)
          EventNumber=ichar(Znak)
        else if(Message.eq.WM_LBUTTONDOWN.or.
     1          Message.eq.WM_LBUTTONDBLCLK) then
          EventType=EventMouse
          EventNumber=JeLeftDown
        else if(Message.eq.WM_LBUTTONUP) then
          EventType=EventMouse
          EventNumber=JeLeftUp
        else if(Message.eq.WM_RBUTTONDOWN) then
          EventType=EventMouse
          EventNumber=JeRightDown
        else if(Message.eq.WM_RBUTTONUP) then
          EventType=EventMouse
          EventNumber=JeRightUp
        else if(Message.eq.WM_MOUSEMOVE) then
          EventType=EventMouse
          EventNumber=JeMove
        else if(Message.eq.WM_MOUSEWHEEL) then
          EventType=EventMouse
          if(wParam.gt.0) then
            EventNumber=JeKoleckoOdSebe
          else
            EventNumber=JeKoleckoKSobe
          endif
        else if(Message.eq.0) then
          go to 8000
        else if(Message.ne.WM_SYSKEYUP.and.
     1          Message.ne.WM_SYSKEYDOWN) then
          if((Message.eq.WM_NCLBUTTONDOWN.and.wParam.eq.20).or.
     1       (Message.eq.WM_SYSCOMMAND.and.wParam.eq.SC_Close)) then
            EventType=EventSystem
            EventNumber=JeWinClose
            go to 9900
          endif
          i=DefWindowProcA(carg(hwndp),carg(message),carg(wParam),
     1                     carg(lParam))
          if(Message.eq.WM_NCMOUSEMOVE) then
            EventType=EventSystem
            EventNumber=JeMimoOkno
            go to 9900
          else if(Message.eq.WM_PAINT) then
            call FeFlush
            go to 9999
          else if(Message.eq.WM_NCLBUTTONDOWN.and.wParam.eq.9) then
            if(WindowSizeType.eq.WindowMaximal) then
              WindowSizeType=WindowExactly
            else
              WindowSizeType=WindowMaximal
            endif
            go to 8000
          else if(Message.ge.WM_NCLBUTTONDOWN.and.
     1            Message.le.WM_NCLBUTTONDOWN) then
            go to 8000
          endif
          go to 8000
        endif
      endif
8000  if(imm.eq.0) then
        if(KurzorEdw.gt.0) then
          if(lpom.and.Message.ne.0.and.
     1       (EventType.ne.EventMouse.or.EventNumber.ne.JeMove)) then
            if(KurzorColor.eq.Black) then
              call FeZmenKurzor(EdwActive)
              KurzorColor=White
              call FeZmenKurzor(EdwActive)
              call FeUpdateDisplay(-1,-1,-1,-1)
            endif
          else
            StTimeNew=FeGetSystemTime()
            if(StTimeNew.gt.StTime+500) then
              call FeZmenKurzor(EdwActive)
              StTime=StTimeNew
              if(KurzorColor.eq.White) then
                KurzorColor=Black
              else
                KurzorColor=White
              endif
              call FeZmenKurzor(EdwActive)
              call FeUpdateDisplay(-1,-1,-1,-1)
            else if(.not.lpom.or.Message.eq.0) then
              if(StTimeNew-StTime.gt.100) call FeWait(.1)
            endif
            if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
              go to 9900
            else
              go to 1000
            endif
          endif
        else
          if(Message.eq.0) go to 1000
        endif
      endif
9900  call GetWindowRect(carg(hWnd),carg(offset(rct)))
!      if(rct.left.lt.-4) go to 9999
      PixelWindowXPos=rct.left
      PixelWindowYPos=rct.top
      if( WindowSizeType.eq.WindowExactly) then
        PixelWindowXPosE=rct.left
        PixelWindowYPosE=rct.top
      endif
      i=rct.right-rct.left
      j=rct.bottom-rct.top
      if((PixelWindowWidth.ne.i.or.PixelWindowHeight.ne.j).and.
     1    AllowResizing) then
        PixelWindowWidth=i
        PixelWindowHeight=j
        if( WindowSizeType.eq.WindowExactly) then
          PixelWindowWidthE=i
          PixelWindowHeightE=j
        endif
        call GetClientRect(carg(hWnd),carg(offset(rct)))
        PixelClientHeight=rct.bottom-rct.top+1
        PixelClientWidth=rct.right-rct.left+1
        hDCBitMap=CreateCompatibleBitmap(carg(hDC),
     1                                   carg(PixelClientWidth),
     2                                   carg(PixelClientHeight))
        XMinBasWin=0.
        XMaxBasWin=anint(float(PixelClientWidth)/EnlargeFactor)
        YMinBasWin=0.
        YMaxBasWin=anint(float(PixelClientHeight)/EnlargeFactor)
        XCenBasWin=anint((XMinBasWin+XMaxBasWin)*.5)
        YCenBasWin=anint((YMinBasWin+YMaxBasWin)*.5)
        XLenBasWin=anint(XMaxBasWin-XMinBasWin)
        YLenBasWin=anint(YMaxBasWin-YMinBasWin)
        EventType=EventResize
        EventNumber=0
        if(IsZoomed(carg(hWnd)).gt.0) then
          WindowSizeType=WindowMaximal
        else
          WindowSizeType=WindowExactly
        endif
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
      call FeGraphicXYFromMessage(lParam,XPos,YPos)
      if(EventType.ne.0) then
        if(EventType.eq.EventMouse.and.
     1     (EventNumber.eq.JeKoleckoOdSebe.or.
     2      EventNumber.eq.JeKoleckoKSobe)) then
          XPos=XPosLast
          YPos=YPosLast
        else if(EventType.eq.EventMouse) then
          XPosLast=XPos
          YPosLast=YPos
        endif
      endif
      call FeClientToScreen(hWnd,0,0,i,j)
      if(XWin.gt.-1000.or.XWin.gt.-1000) then
        if(i.ne.XWin.or.j.ne.YWin) then
          call FeFlush
          XWin=i
          YWin=j
        endif
      else
        XWin=i
        YWin=j
      endif
9999  return
      end
