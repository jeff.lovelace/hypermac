      subroutine FeOpenHC(Type,FileName)
      use Jana_windows
      include 'fepc.cmn'
      integer Type
      character*(*) FileName
      character*256 Veta
      character*3   Ext
      save Ext
      Klic=0
      LnPS=0
      go to 1100
      entry FeCloseHC(Type,FileName)
      Klic=1
1100  ErrFlag=0
      if(Type.eq.HardCopyHPGL) then
        NInfo=1
        InvertWhiteBlack=.false.
        TextInfo='Sorry, the option is not yet implemented.'
        if(Klic.eq.1) call FeInfoOut(-1.,-1.,'INFORMATION','L')
      else if(Type.eq.HardCopyPS.or.Type.eq.HardCopyEPS) then
        if(Type.eq.HardCopyPS) then
          Ext='ps'
        else
          Ext='eps'
        endif
        if(Klic.eq.0) then
          ln=NextLogicNumber()
          call OpenFile(ln,HomeDir(:idel(HomeDir))//'a2ps'//
     1                  ObrLom//'GraphStart.'//Ext,'formatted','old')
          read(ln,FormA) Veta
          if(Veta(1:1).ne.'#') rewind ln
          LnPS=NextLogicNumber()
          call OpenFile(LnPS,FileName,'formatted','unknown')
1200      read(ln,FormA,end=1220) Veta
          write(LnPS,FormA) Veta(:idel(Veta))
          go to 1200
1220      call CloseIfOpened(ln)
          call JanaToClientReverse(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClientReverse(XMaxGrWin,YMaxGrWin,ix2,iy2)
          WidthPS=iabs(ix2-ix1)+1
          HeightPS=iabs(iy2-iy1)+1
          XPocPS=min(ix1,ix2)
          YPocPS=min(iy1,iy2)
          ScalePS=min(756./float(WidthPS),540./float(HeightPS))
          LastXPS=-9999.
          LastYPS=-9999.
          LastColorPS=-1
        else
          ln=NextLogicNumber()
          call OpenFile(ln,HomeDir(:idel(HomeDir))//'a2ps'//
     1                  ObrLom//'GraphEnd.'//Ext,'formatted','old')
          read(ln,FormA) Veta
          if(Veta(1:1).ne.'#') rewind ln
1240      read(ln,FormA,end=1260) Veta
          write(LnPS,FormA) Veta(:idel(Veta))
          go to 1240
1260      call CloseIfOpened(ln)
          call CloseIfOpened(LnPS)
          LnPS=0
        endif
      else if(Type.eq.HardCopyPCX) then
        if(Klic.eq.1.eqv.InvertWhiteBlack) then
          call JanaToClient(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClient(XMaxGrWin,YMaxGrWin,ix2,iy2)
          call FeSavePCXFile(FileName,ix1,ix2,iy1,iy2,ich)
        endif
      else if(Type.eq.HardCopyBMP) then
        if(Klic.eq.1.eqv.InvertWhiteBlack) then
          call JanaToClient(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClient(XMaxGrWin,YMaxGrWin,ix2,iy2)
          call FeSaveBMPFile(FileName,ix1,ix2,iy1,iy2,ich)
        endif
      else if(Type.eq.HardCopyWMF) then
        Veta=FileName(:idel(FileName))//char(0)
        if(Klic.eq.0) then
          hDCMeta=CreateEnhMetaFileA(carg(NULL),carg(offset(Veta)),
     1                               carg(NULL),carg(NULL))
        else
          call CloseEnhMetaFile(carg(hDCMeta))
          hCDMeta=0
        endif
      else if(Type.eq.10) then
        call FeWinMessage('Sorry, not yet implemented',' ')
      else
        go to 9999
      endif
9999  return
      end
