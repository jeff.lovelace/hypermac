      subroutine FeCharOut(id,xmi,ymi,Text,Justify,Color)
      use Jana_windows
      include 'fepc.cmn'
      character*1 Justify
      character*(*) Text
      character*256 TextP,Veta
      integer Color,xs,ys
      xm=xmi
      ym=ymi
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xm+QuestXMin(id)
          ym=ym+QuestYMin(id)
        endif
      endif
      if(Justify.eq.'C') then
        xm=xm-FeTxLength(Text)*.5
      else if(Justify.eq.'R') then
        xm=xm-FeTxLength(Text)
      endif
      yd=PropFontHeightInPixels/EnlargeFactor
      ymm=ym+.5*yd
      call JanaToClient(xm,ymm,ix1,iy1)
      call JanaToClient(xm+FeTxLength(Text),ymm-yd,ix2,iy2)
      TextP=Text
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call FeTextOut(hDCComp,ix1,ix2,iy1,iy2,Color,TextP)
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          call FeRGBUncompress(Color,IRed,IGreen,IBlue)
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(xm,ymm-yd,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m'')') xp,yp
        call ZdrcniCisla(Veta,3)
        write(LnPS,FormA) Veta(:idel(Veta))
        write(LnPS,FormA) '('//Text(:idel(Text))//') lsh'
      endif
      return
      end
