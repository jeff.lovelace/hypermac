      subroutine FePolyline(n,x,y,Color)
      include 'fepc.cmn'
      dimension x(n),y(n)
      integer Color
      logical DeferredOutputIn
      DeferredOutputIn=DeferredOutput
      if(.not.DeferredOutput) call FeDeferOutput
      if(LineType.eq.NormalLine) then
        call FeMoveTo(x(1),y(1),Color)
      else if(LineType.eq.DashedLine.or.
     1        LineType.eq.DenseDashedLine) then
        call Carka(x(1),y(1),0,Color)
      else
        call FeDottedLine(x(1),y(1),0,Color)
      endif
      do i=2,n
        if(LineType.eq.NormalLine) then
          call FeLineTo(x(i),y(i),Color)
        else if(LineType.eq.DashedLine.or.
     1          LineType.eq.DenseDashedLine) then
          call Carka(x(i),y(i),1,Color)
        else
          call FeDottedLine(x(i),y(i),1,Color)
        endif
      enddo
      if(.not.DeferredOutputIn) DeferredOutput=.false.
      call FeUpdateDisplay(-1,-1,-1,-1)
      return
      end
