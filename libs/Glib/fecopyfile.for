      subroutine FeCopyFile(File1,File2)
      use Jana_windows
      logical lpom
      character*(*) File1,File2
      character*260 File1p,File2p
      File1p=File1(:idel(File1))//char(0)
      File2p=File2(:idel(File2))//char(0)
      lpom=CopyFileA(carg(offset(File1p)),carg(offset(File2p)),
     1               carg(FALSE))
      return
      end
