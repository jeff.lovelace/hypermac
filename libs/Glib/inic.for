      subroutine inic
      use Jana_windows
      use FileDirList_mod
      include 'fepc.cmn'
      dimension x(10),y(10)
      integer FeRGBCompress
      logical lpom,FeReadOnly
      character*256 t256
      type(RECT) rctc
!      if(Language.eq.1) i=ActivateKeyboardLayout(carg(HKL_NEXT),carg(0))
      PropFontMeasured=FontInPoints
      OpSystem=-1
      DirectoryDelimitor=ObrLom
      if(HomeDir.eq.' ') then
        t256=MasterName(:idel(MasterName))//'DIR'
        call Velka(t256)
        call FeOsVariable(t256,HomeDir)
        if(HomeDir.eq.' ') then
          TextInfo(1)='The environment variable '//t256(:idel(t256))//
     1                ' is not defined.'//char(0)
          if(LocateSubstring(MasterName,'jana',.false.,.true.).gt.0)
     1      then
            t256='See www-xray.fzu.cz/jana/Jana2006/environ.html for '//
     1           'more information.'//char(0)
          else
            t256=' Volej Vaska.'//char(0)
          endif
          call FeMessageBox(TextInfo(1),t256)
          stop
        endif
      endif
      i=idel(HomeDir)
1000  if(HomeDir(i:i).eq.'\') then
        HomeDir(i:i)=' '
        i=i-1
        go to 1000
      endif
      i=i+1
      HomeDir(i:i)='\'
      RootDir=HomeDir
      open(44,file=HomeDir(:idel(HomeDir))//
     1             MasterName(:idel(MasterName))//'.log')
      write(44,'(''Command line: '',a)') CommandLine(:idel(CommandLine))
      if(VasekDebug.ne.0) write(44,'(''INIC - begin'')')
      if(VasekDebug.ne.0)
     1  write(44,'(''HomeDir : '',256a1)')(HomeDir(j:j),j=1,i)
      Black      =FeRGBCompress(  0,  0,  0)
      White      =FeRGBCompress(255,255,255)
      Red        =FeRGBCompress(255,  0,  0)
      Green      =FeRGBCompress(  0,255,  0)
      Blue       =FeRGBCompress(  0,  0,255)
      Cyan       =FeRGBCompress(  0,255,255)
      Magenta    =FeRGBCompress(255,  0,255)
      Yellow     =FeRGBCompress(255,255,  0)
      WhiteGray  =FeRGBCompress(227,227,227)
      SnowGray   =FeRGBCompress(215,215,215)
      LightGray  =FeRGBCompress(200,200,200)
      Gray       =FeRGBCompress(124,124,124)
      LightYellow=FeRGBCompress(255,255,200)
      DarkBlue   =FeRGBCompress(  0,  0,130)
      Khaki      =FeRGBCompress(155,155,  0)
      ColorNumbers(1) = Black
      ColorNames(1)   ='Black'
      ColorNumbers(2) = White
      ColorNames(2)   ='White'
      ColorNumbers(3) = Red
      ColorNames(3)   ='Red'
      ColorNumbers(4) = Green
      ColorNames(4)   ='Green'
      ColorNumbers(5) = Blue
      ColorNames(5)   ='Blue'
      ColorNumbers(6) = Cyan
      ColorNames(6)   ='Cyan'
      ColorNumbers(7) = Magenta
      ColorNames(7)   ='Magenta'
      ColorNumbers(8) = Yellow
      ColorNames(8)   ='Yellow'
      ColorNumbers(9) = WhiteGray
      ColorNames(9)   ='WhiteGray'
      ColorNumbers(10)= SnowGray
      ColorNames(10)  ='SnowGray'
      ColorNumbers(11)= LightGray
      ColorNames(11)  ='LightGray'
      ColorNumbers(12)= Gray
      ColorNames(12)  ='Gray'
      ColorNumbers(13)= LightYellow
      ColorNames(13)  ='LightYellow'
      ColorNumbers(14)= DarkBlue
      ColorNames(14)  ='DarkBlue'
      ColorNumbers(15)= Khaki
      ColorNames(15)  ='Khaki'
      PocetBarev=15
      call FeRegisterAndOpenBasicWindow(hwnd,0,0,100,100)
      hDC=GetDC(carg(hWnd))
      BitsPixelNew=GetDeviceCaps(carg(hDC),carg(BITSPIXEL))
      PixelScreenWidth=GetDeviceCaps(carg(hDC),carg(HORZRES))
      PixelScreenHeight=GetDeviceCaps(carg(hDC),carg(VERTRES))
      call DestroyWindow(carg(hWnd))
      call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      hDCMeta=0
      call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      iii=WindowSizeType
      n=0
      hDCMeta=0
1100  call FeOpenBasicWindow(hWnd,PixelWindowXpos,PixelWindowYPos,
     1                       PixelWindowWidth,PixelWindowHeight)
      lpom=ShowWindow(carg(hwnd),carg(SW_SHOW))
      if(IsZoomed(carg(hWnd)).gt.0) then
        jjj=WindowMaximal
      else
        jjj=WindowExactly
      endif
      if(iii.ne.jjj.and.n.le.3) then
        n=n+1
        call DestroyWindow(carg(hWnd))
        go to 1100
      endif
      lpom=UpdateWindow(carg(hwnd))
      call FeMouseShape(0)
      XMinAbs=0.
      YMinAbs=0.
      XMaxAbs=1.
      YMaxAbs=1.
      do i=1,MxBmp
        BmpName(i)=' '
      enddo
      EnlargeFactor=1.
      PropFont=.true.
      KurzorClick=-1
      KurzorColor=White
      IconXLength=50.
      IconYLength=50.
      DelejResize=.false.
      Resized=.true.
      DockalSeConf=.true.
      call GetClientRect(carg(hWnd),carg(offset(rctc)))
      PixelClientHeight=rctc.bottom-rctc.top+1
      PixelClientWidth=rctc.right-rctc.left+1
      if(VasekDebug.ne.0) write(44,'(''INIC - end'')')
      hDC=GetDC(carg(hWnd))
      BitsPixelNew=GetDeviceCaps(carg(hDC),carg(BITSPIXEL))
      PixelScreenWidth=GetDeviceCaps(carg(hDC),carg(HORZRES))
      PixelScreenHeight=GetDeviceCaps(carg(hDC),carg(VERTRES))
      hDCComp=CreateCompatibleDC(carg(hDC))
      hDCBitMap=CreateCompatibleBitmap(carg(hDC),
     1                                 carg(PixelClientWidth),
     2                                 carg(PixelClientHeight))
      PropFontName='Tahoma'
      i=-nint(8.*GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HPropFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_DEVICE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      call FeSetPropFont
      t256='12345678901234567890123456789012345678901234567890'//
     1     '12345678901234567890123456789012345678901234567890'//
     2     '12345678901234567890123456789012345678901234567890'//
     3     '12345678901234567890123456789012345678901234567890'
      idl1=FeTxLength(t256)
      i=-nint(float(PropFontSize)*
     1  GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HPropFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_DEVICE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      HPropFontB=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(700),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_OUTLINE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      FixFontName='Courier New'
      i=-nint(float(PropFontSize)*
     1  GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HFixFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_OUTLINE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(FixFontName(:idel(FixFontName))))
      HFixFontB=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(700),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_TT_PRECIS),
     3                       carg(OUT_OUTLINE_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(FixFontName(:idel(FixFontName))))
      call FeSetFixFont
      FixFontWidthInPixels=FeTxLength(char(0))
      FixFontHeightInPixels=FeTxHeight(char(0)//'gy_')
      call FeSetPropFont
      PropFontWidthInPixels=FeTxLength(char(0))
      PropFontHeightInPixels=FeTxHeight(char(0)//'gy_')
      idl2=FeTxLength(t256)
      EnlargeFactor=max(PropFontWidthInPixels/11.,
     1                  float(idl2)/float(idl1),
     2                  PropFontHeightInPixels/13.)
      XMinBasWin=0.
      XMaxBasWin=anint(float(PixelClientWidth)/EnlargeFactor)
      YMinBasWin=0.
      YMaxBasWin=anint(float(PixelClientHeight)/EnlargeFactor)
      XCenBasWin=anint((XMinBasWin+XMaxBasWin)*.5)
      YCenBasWin=anint((YMinBasWin+YMaxBasWin)*.5)
      XLenBasWin=anint(XMaxBasWin-XMinBasWin)
      YLenBasWin=anint(YMaxBasWin-YMinBasWin)
      DeferredOutput=.false.
      call FePlotMode('N')
      call FeDeferOutput
      return
      end
