      subroutine FeMoveTo(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer xs,ys,Color
      character*40 Veta
      call JanaToClient(x,y,xs,ys)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call MoveToEx(carg(hDCComp),carg(xs),carg(ys),carg(NULL))
      if(hDCMeta.ne.0)
     1  call MoveToEx(carg(hDCMeta),carg(xs),carg(ys),carg(NULL))
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          call FeRGBUncompress(Color,IRed,IGreen,IBlue)
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x,y,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        if(abs(xp-LastXPS).gt..01.or.abs(yp-LastYPS).gt..01) then
          write(Veta,'(2f10.2,'' m'')') xp,yp
          call ZdrcniCisla(Veta,3)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastXPS=xp
          LastYPS=yp
        endif
      endif
      call FeUpdateDisplay(xs,xs,ys,ys)
      return
      end
