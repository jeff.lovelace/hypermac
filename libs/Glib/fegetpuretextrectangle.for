      subroutine FeGetPureTextRectangle(x,y,String,Justify,RecType,xmin,
     1                                  xmax,ymin,ymax,refx,refy,conx,
     2                                  cony)
      include 'fepc.cmn'
      character*(*) Justify,String
      integer RecType
      xd=FeTxLength(String)
      if(Justify.eq.'L') then
        xmin=x
      else if(Justify.eq.'C') then
        xmin=x-xd*.5
      else if(Justify.eq.'R') then
        xmin=x-xd
      endif
      ys=y
      yd=FeTxHeight(String)*.5
      if(.not.PropFont) yd=yd-2.
      ymin=ys-yd
      ymax=ys+yd
      xmax=xmin+xd
      xmin=xmin-1.
      xmax=xmax+1.
      refx=x
      refy=y
      conx=xmax
      cony=y
      return
      end
