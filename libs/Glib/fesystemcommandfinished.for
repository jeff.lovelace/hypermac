      logical function FeSystemCommandFinished()
      use Jana_windows
      integer ExitCode
      type(PROCESS_INFORMATION) pi
      common/SysCommand/ pi
      ExitCode=0
      call GetExitCodeProcess(carg(pi.hProcess),
     1                        carg(offset(ExitCode)))
      FeSystemCommandFinished=ExitCode.ne.STILL_ACTIVE
      return
      end
