      subroutine FeWaitInfo(VetaIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) VetaIn
      character*256 Veta
c      integer StTime,FeGetSystemTime
      logical FeSystemCommandFinished
      id=NextQuestId()
      Veta='The Jana2006 program is waiting for '//VetaIn(:idel(VetaIn))
      xqd=FeTxLength(Veta)+20.
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,il,'INFORMATION:',0,
     1                   LightGray,-1,-1)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','N')
      call FeReleaseOutput
      call FeDeferOutput
c      StTime=FeGetSystemTime()
c      call FeMakeLowerPriority
1500  call FeEvent(1)
      call FeMouseShape(0)
      if(.not.FeSystemCommandFinished()) then
        call FeWait(.1)
        call FeReleaseOutput
        call FeDeferOutput
        go to 1500
      endif
      call FeReturnOriginalPriority
      call FeQuestRemove(id)
      return
      end
