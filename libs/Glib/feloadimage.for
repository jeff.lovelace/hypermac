      subroutine FeLoadImage(xlow,xhigh,ylow,yhigh,File,klic)
      include 'fepc.cmn'
      character*(*) File
      character*256  Veta
      logical EqIgCase,Load
      Load=.true.
      go to 1100
      entry FeCleanImage(xlow,xhigh,ylow,yhigh,File,klic)
      Load=.false.
1100  iu=0
      if(klic.eq.1) then
        Veta=HomeDir(:idel(HomeDir))//'bmp\'//File
      else
        Veta=File
      endif
      call JanaToClient(xlow,ylow,ix1,iy1)
      call JanaToClient(xhigh,yhigh,ix2,iy2)
      if(klic.ne.1) then
        do i=1,MxBmp
          if(EqIgCase(Veta,BmpName(i))) then
            if(Load) call FeBitmapPut(IBmp(i),ix1,iy1,ix2,iy2)
            iu=i
            go to 9000
          endif
        enddo
      endif
      if(Load) call FeLoadBMPFile(Veta,ix1,ix2,iy1,iy2,ich)
9000  if(klic.eq.0) then
        call DeleteFile(Veta)
        call FeTmpFilesClear(Veta)
        if(iu.ne.0) then
          BmpName(iu)=' '
          call FeBitmapDestroy(IBmp(iu))
          IBmp(iu)=0
        endif
      endif
      if(Load) call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
