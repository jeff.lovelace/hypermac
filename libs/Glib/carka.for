      subroutine carka(x,y,ik,Color)
      include 'fepc.cmn'
      dimension dc(2)
      integer Color
      save xs,ys,ic,zbytek
      data dc/5.,5./
      if(ik.le.0) then
        xs=x
        ys=y
        call FeMoveTo(x,y,Color)
        ic=2
        zbytek=0.
      else
        difx=x-xs
        dify=y-ys
        delka=sqrt(difx**2+dify**2)
        if(delka.gt.0.) then
          dx=difx/delka
          dy=dify/delka
        else
          dx=0.
          dy=0.
          go to 9000
        endif
        di=delka
2100    if(zbytek.le.0.) then
          ic=mod(ic,2)+1
          if(LineType.eq.DenseDashedLine) then
            fus=dc(ic)
          else
            fus=2.*dc(ic)
          endif
        else
          fus=zbytek
        endif
        if(fus.le.di) then
          zbytek=0.
        else
          zbytek=fus-di
          fus=di
        endif
        xs=anint(xs+fus*dx)
        ys=anint(ys+fus*dy)
        if(ic.eq.1) then
          call FeLineTo(xs,ys,Color)
        else
          call FeMoveTo(xs,ys,Color)
        endif
        di=di-fus
        if(di.gt.0.) go to 2100
      endif
9000  return
      end
