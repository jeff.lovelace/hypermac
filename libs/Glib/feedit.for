      subroutine FeEdit(File)
      include 'fepc.cmn'
      character*(*) File
      character*256 t256
      logical ExistFile,WizardModeOld
      WizardModeOld=WizardMode
      WizardMode=.false.
      if(.not.ExistFile(EditorName)) then
        call FeChybne(-1.,-1.,'the editor "'//
     1                EditorName(:idel(EditorName))//
     2                '" could not be found.',
     3                'Please redefine the editor name in '//
     4                'Tools->Programs.',SeriousError)
        go to 9999
      endif
      t256=EditorName(:idel(EditorName))//' "'//
     1                file(:idel(file))//'"'
      if(WineKey.eq.0) then
        call FeSystemCommand(t256,0)
        call FeWaitInfo('end of your editing.')
      else
        call FeSystem(t256)
      endif
9999  WizardMode=WizardModeOld
      return
      end
