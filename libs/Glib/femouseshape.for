      subroutine FeMouseShape(n)
      use Jana_windows
      integer hCursor
      include 'fepc.cmn'
      if(n.eq.0) then
        m=IDC_ARROW
      else if(n.eq.1) then
        m=IDC_CROSS
      else if(n.eq.3) then
        m=IDC_WAIT
      endif
      hCursor=LoadCursorA(carg(NULL),carg(m))
      call SetCursor(carg(hCursor))
      return
      end
