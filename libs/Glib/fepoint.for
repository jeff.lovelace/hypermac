      subroutine FePoint(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,xss,yss,xj,yj,xjj,yjj,RGB
      character*80 Veta
      call JanaToClient(x,y,xs,ys)
      xjj=nint(x*EnlargeFactor)
      yjj=nint(y*EnlargeFactor)
      go to 1100
      entry FePixelPoint(xj,yj,Color)
      xjj=xj
      yjj=yj
      call JanaPixelToClient(xj,yj,xs,ys)
1100  call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call SetPixel(carg(hDCComp),carg(xs),carg(ys),
     1              carg(RGB(IRed,IGreen,IBlue)))
      call FeUpdateDisplay(xs,xs,ys,ys)
      if(hDCMeta.ne.0)
     1  call SetPixel(carg(hDCMeta),carg(xs),carg(ys),
     2                carg(RGB(IRed,IGreen,IBlue)))
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaPixelToClientReverse(xjj,yjj,xss,yss)
        xp=float(xss-XPocPS)*ScalePS
        yp=float(yss-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m cb'')') xp,yp
        call ZdrcniCisla(Veta,4)
        write(LnPS,FormA) Veta(:idel(Veta))
      endif
      return
      end
