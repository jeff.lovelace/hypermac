      subroutine FePlotMode(Style)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Style
      integer PlotMode
      logical EqIgCase
      if(EqIgCase(Style,'E')) then
        PlotMode=R2_XORPEN
      else
        PlotMode=R2_COPYPEN
      endif
      call SetROP2(carg(hDCComp),carg(PlotMode))
      return
      end
