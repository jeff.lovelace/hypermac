      subroutine FeDrawSegments(n,px1,py1,px2,py2,Color)
      include 'fepc.cmn'
      dimension px1(*),py1(*),px2(*),py2(*),x(2),y(2)
      integer Color(*)
      do i=1,n
        x(1)=px1(i)
        x(2)=px2(i)
        y(1)=py1(i)
        y(2)=py2(i)
        call FePolyLineSolid(x,y,2,Color(i))
      enddo
      return
      end
