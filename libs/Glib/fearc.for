      subroutine FeArc(x,y,r,sa,aa,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,rs,RGB
      call JanaToClient(x,y,xs,ys)
      rs=nint(r*EnlargeFactor)
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do ix=-rs,rs
        do iy=-rs,rs
          if(ix.ne.0.or.iy.ne.0) then
            Uhel=atan2(float(-iy),float(ix))/ToRad
          else
            Uhel=(sa+aa)*.5
          endif
1100      if(Uhel.lt.sa) then
            Uhel=Uhel+360.
            go to 1100
          endif
1200      if(Uhel.gt.sa+aa) then
            Uhel=Uhel-360.
            go to 1200
          endif
          if(Uhel.lt.sa) cycle
          pom=sqrt(float(ix)**2+float(iy)**2)-float(rs)
          if(pom.lt..5.and.pom.ge.-.5) then
            call SetPixel(carg(hDCComp),carg(xs+ix),carg(ys+iy),
     1                    carg(RGB(IRed,IGreen,IBlue)))
            ix1=min(ix1,xs+ix)
            ix2=max(ix2,xs+ix)
            iy1=min(iy1,ys+iy)
            iy2=max(iy2,ys+iy)
          endif
        enddo
      enddo
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
