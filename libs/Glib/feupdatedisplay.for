      subroutine FeUpdateDisplay(ix1,ix2,iy1,iy2)
      use Jana_windows
      include 'fepc.cmn'
      integer ixp,iyp,Width,Height
      save ixp,iyp,ixm,iym,Width,Height
      if(ix1.eq.0.and.ix2.eq.0.and.iy1.eq.0.and.iy2.eq.0) then
        Width=PixelClientWidth
        Height=PixelClientHeight
        ixp=0
        iyp=0
      else if(ix1.gt.0.or.ix2.gt.0.or.iy1.gt.0.or.iy2.gt.0) then
        Width=max(iabs(ix2-ix1)+1,Width)
        Height=max(iabs(iy2-iy1)+1,Height)
        ixp=min(ix1,ix2,ixp)
        iyp=min(iy1,iy2,iyp)
        ixm=max(ix1,ix2,ixm)
        iym=max(iy1,iy2,iym)
        Width=ixm-ixp+1
        Height=iym-iyp+1
      endif
      if(.not.DeferredOutput) then
        call SelectObject(carg(hDCComp),carg(hDCBitMap))
        call BitBlt(carg(hDC),carg(ixp),carg(iyp),
     1              carg(Width),carg(Height),
     2              carg(hDCComp),carg(ixp),carg(iyp),carg(SRCCOPY))
        ixp= 99999
        iyp= 99999
        ixm=-99999
        iym=-99999
        Width=0
        Height=0
      endif
      return
      end
