      logical function FeReadOnly(FileName)
      use Jana_windows
      character*(*) FileName
      i=GetFileAttributesA(carg(FileName))
      FeReadOnly=iand(i,FILE_ATTRIBUTE_READONLY).ne.0
      return
      end
