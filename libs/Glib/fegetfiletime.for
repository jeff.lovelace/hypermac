      subroutine FeGetFileTime(FileName,FileDate,FileTimeP)
      use Jana_windows
      integer Date,Time,hFile
      character*(*) FileName,FileDate,FileTimeP
      character*256 Veta
      type(FILETIME) fTime
      Date=0
      Time=0
      FileDate=' '
      FileTimeP=' '
      Veta=FileName(:idel(FileName))//char(0)
      hFile=CreateFileA(carg(offset(Veta)),carg(GENERIC_READ),
     1                 carg(NULL),carg(NULL),carg(OPEN_ALWAYS),
     2                 carg(FILE_ATTRIBUTE_NORMAL),carg(NULL))
      if(hFile.le.0) go to 9999
      call GetFileTime(carg(hFile),carg(NULL),carg(NULL),
     1                 carg(offset(fTime)))
      call FileTimeToDosDateTime(carg(offset(fTime)),
     1                           carg(offset(Date)),
     2                           carg(offset(Time)))
      call CloseHandle(carg(hFile))
      write(FileDate,'(i2,''/'',i2,''/'',i4)')
     1  mod(Date,32),mod(Date/32,16),Date/512+1980
      do i=1,idel(FileDate)
        if(FileDate(i:i).eq.' ') FileDate(i:i)='0'
      enddo
      write(FileTimeP,'(i2,'':'',i2,'':'',i2)')
     1  Time/2048,mod(Time/32,64),2*mod(Time,32)
      do i=1,idel(FileTimeP)
        if(FileTimeP(i:i).eq.' ') FileTimeP(i:i)='0'
      enddo
9999  return
      end
