      logical function FeGetStringFromReg(RegSubKey,ValueName,String)
      use Jana_windows
      character*(*) RegSubKey,ValueName,String
      integer RegKey
      i=RegOpenKeyExA(carg(HKEY_CURRENT_USER),
     1                carg(offset(RegSubKey)),carg(NULL),
     2                carg(KEY_READ),carg(offset(RegKey)))
      if(i.eq.0) then
        idl=len(String)
        i=RegQueryValueExA(carg(RegKey),
     1                     carg(offset(ValueName)),carg(NULL),
     2                     carg(offset(j)),carg(offset(String)),
     3                     carg(offset(idl)))
        FeGetStringFromReg=i.eq.0
      else
        FeGetStringFromReg=.false.
      endif
      return
      end
