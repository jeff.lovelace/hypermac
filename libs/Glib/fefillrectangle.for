      subroutine FeFillRectangle(xmin,xmax,ymin,ymax,istyle,idense,
     1                           iangle,color)
      include 'fepc.cmn'
      dimension xv(5),yv(5)
      integer color
      xv(1)=xmin
      yv(1)=ymin
      xv(2)=xmax
      yv(2)=ymin
      xv(3)=xmax
      yv(3)=ymax
      xv(4)=xmin
      yv(4)=ymax
      if(istyle.gt.0) then
        call FePolygon(xv,yv,4,istyle,idense,iangle,Color)
      else
        xv(5)=xmin
        yv(5)=ymin
        call FePolylineSolid(xv,yv,5,Color)
      endif
      return
      end
