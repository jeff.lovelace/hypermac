      subroutine FeMakeLowerPriority
      use Jana_windows
      integer OldPriority
      data OldPriority,NewPriority/32,16384/
      i=GetCurrentProcess()
      OldPriority=GetPriorityClass(carg(i))
      call SetPriorityClass(carg(i),carg(NewPriority))
      go to 9999
      entry FeReturnOriginalPriority
      i=GetCurrentProcess()
      call SetPriorityClass(carg(i),carg(OldPriority))
9999  return
      end
