      subroutine FeLineTo(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer xs,ys,Color,RGB,hpen
      character*40 Veta
      call JanaToClient(x,y,xs,ys)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      hpen=CreatePen(carg(PS_SOLID),carg(0),
     1               carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCComp),carg(hPen))
      call LineTo(carg(hDCComp),carg(xs),carg(ys))
      if(hDCMeta.ne.0) then
        call SelectObject(carg(hDCMeta),carg(hPen))
        call LineTo(carg(hDCMeta),carg(xs),carg(ys))
      endif
      call DeleteObject(carg(hpen))
      call FeUpdateDisplay(xs,xs,ys,ys)
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x,y,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        if(abs(xp-LastXPS).gt..01.or.abs(yp-LastYPS).gt..01) then
          write(Veta,'(2f10.2,'' l'')') xp,yp
          call ZdrcniCisla(Veta,3)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastXPS=xp
          LastYPS=yp
          LastColorPS=Color
        endif
      endif
      return
      end
