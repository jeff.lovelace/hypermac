      function FeTimeDiff()
      include 'fepc.cmn'
      logical First
      integer FeGetSystemTime
      save TimeOld
      data first/.true./
      Time=float(FeGetSystemTime())/1000.
      if(First) then
        TimeOld=Time
        First=.false.
      endif
      FeTimeDiff=Time-TimeOld
      TimeOld=Time
      return
      end
