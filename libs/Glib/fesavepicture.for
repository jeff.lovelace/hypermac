      subroutine FeSavePicture(WhatToSave,NFormat,Key)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) WhatToSave
      character*256 EdwStringQuest
      character*128 FileName(:)
      character*80  Veta
      character*1   ch1,ch2
      integer HardCopyLast,CrwStateQuest
      logical :: ExistFile,ThisSection=.true.,CrwLogicQuest,EqIgCase
      data HardCopyLast/2/
      allocatable FileName
      allocate(FileName(12))
      if(NFormat.le.0) then
        nCrwMax=12
      else
        nCrwMax=NFormat
      endif
      if(HardCopy.eq.0.or.mod(HardCopy,100).gt.nCrwMax)
     1  HardCopy=HardCopyLast
      ThisSection=HardCopy.lt.100
      id=NextQuestId()
      NameStandard=1
      do j=1,nCrwMax
        k=0
1000    write(Cislo,'(''_'',i3)') k
        call Zhusti(Cislo)
        FileName(j)=fln(:ifln)//Cislo(:idel(Cislo))//HCExtension(j)
        if(ExistFile(FileName(j))) then
          k=k+1
          go to 1000
        endif
      enddo
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,nCrwMax+2,' ',0,LightGray,0,0)
      il=1
      tpom=5.
      Veta='%File name'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+20.
      Veta='Br%owse'
      dpom=xqd-xpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      pom=0.
      do i=1,12
        pom=max(pom,FeTxLength(HCMenu(i)))
      enddo
      xpom=xqd-10.-CrwgYd
      tpom=xqd-CrwgYd-20.-pom
      j=0
      do i=1,12
        j=j+1
        if(i.gt.nCrwMax) exit
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,HCMenu(j),'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) nCrwTypeFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,j.eq.mod(HardCopy,100))
      enddo
      nCrwTypeLast=CrwLastMade
      idl=idel(WhatToSave)
      if(Key.eq.0) then
        il=2
        tpom=5.
        Veta='Save t%his '//WhatToSave(:idl)
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,2)
          if(i.eq.1) then
            Veta='Save %all '//WhatToSave(:idl)//'s'
            il=il+6
            nCrwThis=CrwLastMade
          else
            nCrwAll=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.ThisSection)
        enddo
      else
        nCrwThis=0
        nCrwAll=0
        call FeQuestLblMake(id,5.,2,'Save this '//WhatToSave(:idl),'L',
     1                      'N')
      endif
      HCFileName=FileName(mod(HardCopy,100))
      il=nCrwMax+2
      xpom=5.
      tpom=xpom+CrwXd+5.
      Veta='I%nvert White for Black and vice versa'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,InvertWhiteBlack)
      nCrwInvert=CrwLastMade
1300  call FeQuestStringEdwOpen(nEdwFileName,HCFileName)
      nCrw=nCrwTypeFirst
      if(ThisSection) then
        do i=1,nCrwMax
          call FeQuestCrwOpen(nCrw,i.eq.mod(HardCopy,100))
          nCrw=nCrw+1
        enddo
      else
        do i=1,HardCopyNum-1
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      if(mod(HardCopy,100).ge.HardCopyNum) then
        if(CrwStateQuest(nCrwInvert).eq.CrwOn.or.
     1     CrwStateQuest(nCrwInvert).eq.CrwOff) then
          InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
          call FeQuestCrwDisable(nCrwInvert)
        endif
      else
        call FeQuestCrwOpen(nCrwInvert,InvertWhiteBlack)
      endif
1500  call FeQuestEvent(id,ich)
      HCFileName=EdwStringQuest(nEdwFileName)
      id1=idel(HCFileName)
      id2=idel(HCExtension(mod(HardCopy,100)))
      j=id1-id2
      do i=1,id2
        j=j+1
        ch1=HCExtension(mod(HardCopy,100))(i:i)
        ch2=HCFileName(j:j)
        if(OpSystem.le.0) then
          call mala(ch1)
          call mala(ch2)
        endif
        if(ch1.ne.ch2) then
          NameStandard=-1
          go to 1610
        endif
      enddo
      if(.not.EqIgCase(HCFileName,FileName(mod(HardCopy,100))))
     1  go to 1607
      NameStandard=1
      FileName(mod(HardCopy,100))=HCFileName
      go to 1610
1607  NameStandard=0
1610  if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        call FeFileManager('Select output file',HCFileName,
     1                     '*'//HCExtension(mod(HardCopy,100)),0,.true.,
     2                     ich)
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwTypeFirst.and.
     2        CheckNumber.le.nCrwTypeLast) then
        HardCopy=CheckNumber
        if(NameStandard.eq.1) then
          HCFileName=FileName(HardCopy)
        else if(NameStandard.eq.0) then
          i=LocateSubstring(HCFileName,'.',.false.,.false.)
          if(i.gt.0) HCFileName=HCFileName(:i-1)//
     1     HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwThis.or.CheckNumber.eq.nCrwAll)) then
        ThisSection=CrwLogicQuest(nCrwThis)
        if(CheckNumber.eq.nCrwAll.and.
     1     mod(HardCopy,100).le.HardCopyNum-1) then
          HardCopy=HardCopyNum
          HCFileName=FileName(HardCopy)
          nCrw=nCrwTypeFirst-1+HardCopyNum
          call FeQuestCrwOn(nCrw)
        endif
        go to 1300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
        if(nCrwAll.gt.0) then
          if(CrwLogicQuest(nCrwAll)) then
            HardCopy=HardCopy+100
          else
            HardCopy=mod(HardCopy,100)
          endif
        endif
        HardCopyLast=HardCopy
      else
        HardCopy=HardCopyLast
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) HardCopy=0
9999  deallocate(FileName)
      return
      end
