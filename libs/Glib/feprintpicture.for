      subroutine FePrintPicture(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer EdwStateQuest,RolMenuSelectedQuest
      logical CrwLogicQuest
      character*256 EdwStringQuest
      character*80  Veta,PSPrinters(100)
      call FeGetPSPrinters(PSPrinters,nPSPrinters)
      ich=0
      xqd=300.
      if(nPSPrinters.gt.0) then
        tpom=5.
        Veta='Printers with PostScript driver:'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=20.
        do i=1,nPSPrinters
          dpom=max(dpom,FeTxLengthUnder(PSPrinters(i))+2.*EdwMarginSize)
        enddo
        dpom=dpom+EdwYd+20.
        xpom1=xpom+dpom+5.
        xqd=max(xpom1+EdwYd+5.,xqd)
      else
        call FeChybne(-1.,-1.,'no postscipt printer found.',' ',
     1                SeriousError)
        ich=1
        go to 9999
      endif
      il=2
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Define printer',0,LightGray,
     1                   0,0)
      il=1
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nRolMenuPrinters=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,PSPrinters,NPSPrinters,1)
      Veta='I%nvert White for Black and vice versa'
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,InvertWhiteBlack)
      nCrwInvert=CrwLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
        PSPrinters(1)=PSPrinters(RolMenuSelectedQuest(nRolMenuPrinters))
        PSPrinter=PSPrinters(1)
        HCFileName='jtps'
        call CreateTmpFile(HCFileName,i,2)
        call FeTmpFilesAdd(HCFileName)
        HardCopy=HardCopyPS
        Tiskne=.true.
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
9999  return
      end
