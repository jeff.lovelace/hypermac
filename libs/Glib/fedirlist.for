      subroutine FeDirList(List)
      use FileDirList_mod
      include 'fepc.cmn'
      character*(*) List
      character*256 t256
      integer ipor(:)
      allocatable ipor
      call FeGetFileDirList(CurrentDir)
      ln=NextLogicNumber()
      open(ln,file=List,err=9000)
      allocate(ipor(NDirs))
      call SortTextIgnoreCase(NDirs,DirList,ipor)
      do i=1,NDirs
        k=ipor(i)
        write(ln,FormA) '['//DirList(k)(:idel(DirList(k)))//']'
      enddo
      deallocate(ipor)
      allocate(ipor(NFiles))
      call SortTextIgnoreCase(NFiles,FileList,ipor)
      do i=1,NFiles
        k=ipor(i)
        write(ln,FormA) FileList(k)(:idel(FileList(k)))
      enddo
      deallocate(ipor)
      go to 9900
9000  ErrFlag=1
9900  call CloseIfOpened(ln)
      return
      end
