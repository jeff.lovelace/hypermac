      subroutine EM40GetAtom(ia,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                       MagParA,PrvKi)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodan(*),kfan(*)
      character*8  AtName
      integer PrvKi
      AtName=Atom(ia)
      itfa=itf(ia)
      isfa=isf(ia)
      do i=1,7
        kmodan(i)=KModA(i,ia)
      enddo
      do i=1,7
        kfan(i)=KFA(i,ia)
      enddo
      lasmaxa=lasmax(ia)+1
      MagParA=MagPar(ia)
      PrvKi=1
      return
      end
