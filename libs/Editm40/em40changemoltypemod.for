      subroutine EM40ChangeMolTypeMod(imp,TypeModFunNew,OrthoEpsNew)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      real xpp(6),qcntp(3),TransOld(3),RotVec(3),PomVec(3),RotMat(3,3),
     1     PomMat(3,3)
      real, allocatable :: xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1                                 OrthoMatOld(:),OrthoMatOldI(:),
     2                     FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),
     3                     RSol(:),Y1Old(:),Y1New(:)
      character*256 Veta
      integer TypeModFunNew
      integer, allocatable :: OrthoSelOld(:),OrthoSelNew(:)
      write(Cislo,'(2i5)') TypeModFunMol(imp),TypeModFunNew
      if(TypeModFunNew.eq.TypeModFunMol(imp).and.TypeModFunNew.ne.1)
     1   go to 9999
      if(KModM(2,imp).le.0.and.KModM(3,imp).le.0) go to 9999
      im=(imp-1)/mxp+1
      m=max(OrthoOrd,2*MaxUsedKwAll+1)
      allocate(xp(m),yp(m),OrthoSelNew(m),OrthoSelOld(m),
     1         OrthoMatNew(m**2),OrthoMatNewI(m**2),
     2         OrthoMatOld(m**2),OrthoMatOldI(m**2),
     3         FOld(m,NPoints),FNew(m,NPoints),YOld(NPoints),
     4         RMat(m*(m+1)),RSide(m),RSol(m),Y1Old(NPoints),
     5         Y1New(NPoints))
      X40=axm(1,imp)
      Delta=a0(imp)
      TransOld=Trans(1:3,imp)
      if(TypeModFunMol(imp).eq.1) then
        write(Veta,'(3f10.6)') OrthoX40Mol(imp),OrthoDeltaMol(imp),
     1                      OrthoEpsMol(imp)
        call UpdateOrtho(imp,OrthoX40Mol(imp),OrthoDeltaMol(imp),
     1                   OrthoEpsMol(imp),OrthoSelOld,OrthoOrd)
        call MatOr(X40,OrthoX40Mol(imp),OrthoSelOld,OrthoOrd,
     1             OrthoMatOld,OrthoMatOldI,imp)
      endif
      if(TypeModFunNew.eq.1) then
        if(OrthoOrd.le.0) OrthoOrd=m
        OrthoX40Mol(imp)=X40
        OrthoDeltaMol(imp)=Delta
        OrthoEpsMol(imp)=OrthoEpsNew
        call UpdateOrtho(imp,X40,Delta,OrthoEpsNew,OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,imp)
      endif
      if(KCommen(KPhase).gt.0) then
        call AddVek(xm(1,im),trans(1,imp),xpp,3)
        call qbyx(xpp,qcntp,iswmol(im))
        x4=trez(1,1,KPhase)+qcntp(1)
        n=2*NCommQProduct(iswmol(im),KPhase)
        dx4=1./float(n)
        NPointsUsed=n
      else
        NPointsUsed=NPoints
        x4=X40-Delta*.5
        dx4=Delta/float(NPointsUsed-1)
      endif
      nn=0
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        nn=nn+1
        if(TypeModFunMol(imp).eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FOld(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FOld(k,m)=sin(Pi2*km*x4)
            else
              FOld(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunMol(imp).eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelOld(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatOld,yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FOld(1,m),2*MaxUsedKwAll+1,
     1                 TypeModFunMol(imp))
        endif
        if(TypeModFunNew.eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FNew(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FNew(k,m)=sin(Pi2*km*x4)
            else
              FNew(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunNew.eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FNew(1,m),2*MaxUsedKwAll+1,TypeModFunNew)
        endif
        x4=x4p+float(m)*dx4
      enddo
      if(TypeModFunMol(imp).eq.1) then
        nn=min(OrthoOrd,nn)
      else
        nn=min(2*MaxUsedKwAll+1,nn)
      endif
      RMat=0.
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcntp(1)
      else
        x4=X40-Delta*.5
      endif
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        l=0
        do i=1,nn
          do j=1,i
            l=l+1
            RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
          enddo
        enddo
        x4=x4p+float(m)*dx4
      enddo
      call smi(RMat,nn,ISing)
      do it=2,3
        kmod=KModM(it,imp)
        if(kmod.le.0) cycle
        if(it.eq.2) then
          irm=2
        else
          irm=3
        endif
        do ir=1,irm
          if(it.eq.2) then
            n=3
          else if(it.eq.3.and.ir.le.2) then
            n=6
          else
            n=9
          endif
          do i=1,n
            xp=0.
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
!                if(it.eq.2.and.ir.eq.1) xp(k)=trans(i,imp)
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  if(ir.eq.1) then
                    xp(k)=utx(i,km,imp)
                  else
                    xp(k)=urx(i,km,imp)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    xp(k)=ttx(i,km,imp)
                  else if(ir.eq.2) then
                    xp(k)=tlx(i,km,imp)
                  else
                    xp(k)=tsx(i,km,imp)
                  endif
                endif
              else
                if(it.eq.2) then
                  if(ir.eq.1) then
                    xp(k)=uty(i,km,imp)
                  else
                    xp(k)=ury(i,km,imp)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    xp(k)=tty(i,km,imp)
                  else if(ir.eq.2) then
                    xp(k)=tly(i,km,imp)
                  else
                    xp(k)=tsy(i,km,imp)
                  endif
                endif
              endif
            enddo
            RSide=0.
            if(KCommen(KPhase).gt.0) then
              x4=trez(1,1,KPhase)+qcntp(1)
            else
              x4=X40-Delta*.5
            endif
            x4p=x4
            do m=1,NPointsUsed
              pom=x4-X40
              j=pom
              if(pom.lt.0.) j=j-1
              pom=pom-float(j)
              if(pom.gt..5) pom=pom-1.
              pom=2.*pom/Delta
              if(pom.lt.-1.01.or.pom.gt.1.01) then
                x4=x4p+float(m)*dx4
                cycle
              endif
              call MultM(FOld(1,m),xp,YOld(m),1,nn,1)
              if(it.eq.2.and.i.eq.1) Y1Old(m)=YOld(m)
              do j=1,nn
                RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
              enddo
              x4=x4p+float(m)*dx4
            enddo
            call nasob(RMat,RSide,RSol,nn)
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
                if(it.eq.2.and.ir.eq.1) then
                  trans(i,imp)=RSol(k)+TransOld(i)
                else if(it.eq.2.and.ir.eq.2) then
                  RotVec(i)=RSol(k)
                endif
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  if(ir.eq.1) then
                    utx(i,km,imp)=RSol(k)
                  else
                    urx(i,km,imp)=RSol(k)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    ttx(i,km,imp)=RSol(k)
                  else if(ir.eq.2) then
                    tlx(i,km,imp)=RSol(k)
                  else
                    tsx(i,km,imp)=RSol(k)
                  endif
                endif
              else
                if(it.eq.2) then
                  if(ir.eq.1) then
                    uty(i,km,imp)=RSol(k)
                  else
                    ury(i,km,imp)=RSol(k)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    tty(i,km,imp)=RSol(k)
                  else if(ir.eq.2) then
                    tly(i,km,imp)=RSol(k)
                  else
                    tsy(i,km,imp)=RSol(k)
                  endif
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      PomMat=0.
      RotVec(1:3)=RotVec(1:3)*CellVol(iswmol(im),KPhase)
      PomMat(1,2)=-RotVec(3)
      PomMat(1,3)= RotVec(2)
      PomMat(2,1)= RotVec(3)
      PomMat(2,3)=-RotVec(1)
      PomMat(3,1)=-RotVec(2)
      PomMat(3,2)= RotVec(1)
      call Multm(MetTensI(1,iswmol(im),KPhase),PomMat,PomMat,3,3,3)
      PomMat(1,1)=1.+PomMat(1,1)
      PomMat(2,2)=1.+PomMat(2,2)
      PomMat(3,3)=1.+PomMat(3,3)
      call MultM(RotMol(1,imp),PomMat,RotMat,3,3,3)
      call MultM(TrMol(1,imp),RotMat,PomMat,3,3,3)
      call MultM(PomMat,TriMol(1,imp),RotMat,3,3,3)
      RotVec(1:3)=RotMat(1,1:3)
      call VecOrtNorm(RotVec,3)
      RotMat(1,1:3)=RotVec(1:3)
      PomVec(1:3)=RotMat(1,1:3)
      RotVec(1:3)=RotMat(2,1:3)
      pom=-VecOrtScal(RotVec,PomVec,3)
      do i=1,3
        RotVec(i)=pom*PomVec(i)+RotVec(i)
      enddo
      call VecOrtNorm(RotVec,3)
      RotMat(2,1:3)=RotVec(1:3)
      PomVec(1:3)=RotMat(1,1:3)
      RotVec(1:3)=RotMat(2,1:3)
      call VecMul(PomVec,RotVec,xpp)
      call VecOrtNorm(xpp,3)
      RotMat(3,1:3)=xpp(1:3)
      call MatInv(RotMat,PomMat,pom,3)
      call EM40GetAngles(RotMat,irot(KPhase),euler(1,imp))
      do i=1,3
        xpp(i)=trans(i,imp)-TransOld(i)
      enddo
      call qbyx(xpp,qcntp,iswmol(im))
      axm(1,imp)=X40+qcntp(1)
      TypeModFunMol(imp)=TypeModFunNew
      deallocate(xp,yp,OrthoSelNew,OrthoSelOld,OrthoMatNew,OrthoMatNewI,
     1           OrthoMatOld,OrthoMatOldI,FOld,FNew,YOld,RMat,RSide,
     2           RSol,Y1Old,Y1New)
9999  return
      end
