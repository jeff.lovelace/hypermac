      subroutine ZmMag(ia,MagParA)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PrvKiMag
      call ReallocateAtomParams(NAtAll,itfmax,IFrMax,KModAMax,MagParA)
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModA(1,ia),MagParA,
     1               .false.)
      if(MagParA.gt.MagPar(ia)) then
        kip=PrvKiMag(ia)
        if(MagPar(ia).le.0) then
          call SetRealArrayTo( sm0(1,ia),3,.1)
          call SetRealArrayTo(ssm0(1,ia),3,.0)
          call SetIntArrayTo(KiA(kip,ia),3,1)
        endif
        i=max(MagPar(ia),1)
        n=(MagParA-i)*3
        kip=kip+3+(i-1)*6
        if(n.gt.0) then
          call SetRealArrayTo( smx(1,i,ia),n,.1)
          call SetRealArrayTo(ssmx(1,i,ia),n,.0)
          call SetRealArrayTo( smy(1,i,ia),n,.1)
          call SetRealArrayTo(ssmy(1,i,ia),n,.0)
          call SetIntArrayTo(KiA(kip,ia),2*n,1)
        endif
      endif
      MagPar(ia)=MagParA
      return
      end
