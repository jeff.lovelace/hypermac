      subroutine AllocateTrans
      use EditM40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      allocate(TransMat(NDimQ(KPhase),NTrans,NComp(KPhase)),
     1         TransVec(NDim (KPhase),NTrans,NComp(KPhase)),
     2         TransX    ( 9,NTrans,NComp(KPhase)),
     3         TransM    ( 9,NTrans,NComp(KPhase)),
     4         TransTemp (36,NTrans,NComp(KPhase)),
     5         TransTempS(81,NTrans,NComp(KPhase)),
     6         SignTransX(NTrans),TransZM(NTrans),
     7         TransC3(100,NTrans,NComp(KPhase)),
     8         TransC4(225,NTrans,NComp(KPhase)),
     9         TransC5(441,NTrans,NComp(KPhase)),
     a         TransC6(784,NTrans,NComp(KPhase)))
      if(MaxUsedKw(KPhase).gt.0)
     1  allocate(TransKwSym(MaxUsedKw(KPhase),NTrans))
      end
