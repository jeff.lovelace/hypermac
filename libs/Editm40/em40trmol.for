      subroutine EM40TrMol(ii,io,isw,ntr,Klic)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*8 MolNamePom
      dimension px(9),py(9),xp(6),xpp(6),phi(3),xps(6,2),
     1          snm(:),csm(:),eps(:),pxa(:),pya(:),spxa(:),
     2          spya(:)
      allocatable snm,csm,eps,pxa,pya,spxa,spya
      n=MaxUsedKw(KPhase)
      allocate(snm(n),csm(n),eps(n),pxa(9*n),pya(9*n),spxa(9*n),
     1         spya(9*n))
      im=(io-1)/mxp+1
      iak=NAtMolFr(1,1)-1
      do i=1,im
        iap=iak+1
        iak=iak+iam(i)/KPoint(i)
      enddo
      if(io.ne.ii) then
        aimol(io)=aimol(ii)
        saimol(io)=saimol(ii)
        LocMolSystType(io)=LocMolSystType(ii)
        TypeModFunMol(io)=TypeModFunMol(ii)
        do i=1,max(2,LocMolSystType(io))
          if(i.gt.LocMolSystType(io)) then
            LocMolSystAx(i,io)='zy'
          else
            LocMolSystAx(i,io)=LocMolSystAx(i,ii)
          endif
          do j=1,2
            LocMolSystSt(j,i,io)=LocMolSystSt(j,i,ii)
            call CopyVek(LocMolSystX(1,j,i,ii),LocMolSystX(1,j,i,io),3)
          enddo
        enddo
        call CopyVek(TrMol(1,ii),TrMol(1,io),9)
        call CopyVek(TriMol(1,ii),TriMol(1,io),9)
        AtTrans(io)=' '
        call CopyVekI(KFM(1,ii),KFM(1,io),3)
        call CopyVekI(KModM(1,ii),KModM(1,io),3)
        aimol(io)=aimol(ii)
        if(NDimI(KPhase).gt.0) then
          phfm(io)=phfm(ii)
          sphfm(io)=sphfm(ii)
        endif
        if(ktls(im).ne.0) then
          call CopyVek(tt(1,ii),tt(1,io),6)
          call CopyVek(tl(1,ii),tl(1,io),6)
          call CopyVek(ts(1,ii),ts(1,io),9)
        endif
      else
        call CopyVek(TrToOrtho(1,isw,KPhase),TrMol(1,io),9)
        call CopyVek(TrToOrthoI(1,isw,KPhase),TriMol(1,io),9)
      endif
      call ShiftKiMol(io,ktls(im),KModM(1,io),KModM(2,io),
     1                KModM(3,io),.false.)
      call CopyVekI(KiMol(1,ii),KiMol(1,io),DelkaKiMolekuly(io))
      RotSign(io)=nint(SignTransX(ntr))*RotSign(ii)
      call AddVek(xm(1,im),trans(1,ii),xp,3)
      call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
      call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),1)
      do j=1,NDim(KPhase)
        xpp(j)=xpp(j)+TransVec(j,ntr,isw)
        if(j.ge.4) phi(j-3)=-xpp(j)*pi2
      enddo
      call CopyVek(STrans(1,ii),STrans(1,io),3)
      if(Klic.eq.0) then
        do i=1,3
          trans(i,io)=xpp(i)-xm(i,im)
        enddo
        call multm(TransX(1,ntr,isw),RotMol(1,ii),px,3,3,3)
        call CopyVek(px,RotMol(1,io),9)
      else
        call multm(TransX(1,ntr,isw),RotMol(1,ii),px,3,3,3)
        call MatInv(TransX(1,ntr,isw),py,pom,3)
        call multm(px,py,RotMol(1,io),3,3,3)
        call multm(TransX(1,ntr,isw),trans(1,ii),px,3,3,1)
        do j=1,3
          trans(j,io)=px(j)+TransVec(j,ntr,isw)
        enddo
      endif
      call MatInv(RotMol(1,io),RotIMol(1,io),pom,3)
      call multm(TrMol(1,io),RotMol(1,io),px,3,3,3)
      call multm(px,TriMol(1,io),py,3,3,3)
      call EM40GetAngles(py,irot(KPhase),euler(1,io))
      call CopyVek(SEuler(1,ii),SEuler(1,io),3)
      call CopyVek(DRotF(1,ii),DRotF(1,io),9)
      call CopyVek(DRotC(1,ii),DRotC(1,io),9)
      call CopyVek(DRotP(1,ii),DRotP(1,io),9)
      call CopyVek(RotB(1,ii),RotB(1,io),36)
      call CopyVek(DRotBF(1,ii),DRotBF(1,io),36)
      call CopyVek(DRotBC(1,ii),DRotBC(1,io),36)
      call CopyVek(DRotBP(1,ii),DRotBP(1,io),36)
      a0m(io)=a0m(ii)
      sa0m(io)=sa0m(ii)
      if(NDimI(KPhase).le.0) go to 9999
      write(Cislo,'(''#'',i5)') mod(ii-1,mxp)+1
      call Zhusti(Cislo)
      MolNamePom=molname(im)(:idel(molname(im)))//Cislo(:idel(Cislo))
      do k=1,MaxUsedKw(KPhase)
        eps(k)=isign(1,TransKwSym(k,ntr))
        l=iabs(TransKwSym(k,ntr))
        pom=0.
        if(TypeModFunMol(io).le.1) then
          do m=1,NDimI(KPhase)
            pom=pom+float(kw(m,l,KPhase))*phi(m)
          enddo
        endif
        snp=sin(pom)
        csp=cos(pom)
        if(abs(snp).lt..001) snp=0.
        if(abs(csp).lt..001) csp=0.
        if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
        if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
        snm(k)=snp
        csm(k)=csp
      enddo
      do n=0,2
        kmodp=KModM(n+1,ii)
        if(kmodp.le.0) cycle
        kfap=KFM(n+1,ii)
        if(n.eq.0) then
          mk=1
        else if(n.eq.1) then
          mk=2
        else if(n.eq.2) then
          mk=3
        endif
        NRank=TRank(n)
        if(kfap.ne.0) then
          if(n.eq.0) then
            NDimIp=NDimI(KPhase)
          else
            NDimIp=1
          endif
        endif
        do m=1,mk
          if(n.eq.2.and.m.eq.3) NRank=9
          nn=1
          do k=1,kmodp
            if(n.eq.0) then
              call CopyVek( axm(k,ii), pxa(nn),NRank)
              call CopyVek( aym(k,ii), pya(nn),NRank)
              call CopyVek(saxm(k,ii),spxa(nn),NRank)
              call CopyVek(saym(k,ii),spya(nn),NRank)
            else if(n.eq.1) then
              if(m.eq.1) then
                if(Klic.eq.0) then
                  call CopyVek( utx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( uty(1,k,ii), pya(nn),NRank)
                  call CopyVek(sutx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(suty(1,k,ii),spya(nn),NRank)
                else
                  call Multm (TransX(1,ntr,isw),utx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm (TransX(1,ntr,isw),uty(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),sutx(1,k,ii),spxa(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),suty(1,k,ii),spya(nn),
     1                        NRank,NRank,1)
                endif
              else
                if(Klic.eq.0) then
                  call CopyVek( urx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( ury(1,k,ii), pya(nn),NRank)
                  call CopyVek(surx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(sury(1,k,ii),spya(nn),NRank)
                else
                  call Multm (TransX(1,ntr,isw),urx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm (TransX(1,ntr,isw),ury(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),surx(1,k,ii),spxa(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),sury(1,k,ii),spya(nn),
     1                        NRank,NRank,1)
                endif
              endif
            else if(n.eq.2) then
              if(m.eq.1) then
                if(Klic.eq.0) then
                  call CopyVek( ttx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tty(1,k,ii), pya(nn),NRank)
                  call CopyVek(sttx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stty(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTemp(1,ntr,isw),ttx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTemp(1,ntr,isw),tty(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),sttx(1,k,ii),spxa(nn)
     1                       ,NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stty(1,k,ii),spya(nn)
     1                       ,NRank,NRank,1)
                endif
              else if(m.eq.2) then
                if(Klic.eq.0) then
                  call CopyVek( tlx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tly(1,k,ii), pya(nn),NRank)
                  call CopyVek(stlx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stly(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTemp(1,ntr,isw),tlx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTemp(1,ntr,isw),tly(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stlx(1,k,ii),
     1                        spxa(nn),NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stly(1,k,ii),
     1                        spya(nn),NRank,NRank,1)
                endif
              else
                if(Klic.eq.0) then
                  call CopyVek( tsx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tsy(1,k,ii), pya(nn),NRank)
                  call CopyVek(stsx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stsy(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTempS(1,ntr,isw),tsx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTempS(1,ntr,isw),tsy(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTempS(1,ntr,isw),stsx(1,k,ii),
     1                        spxa(nn),NRank,NRank,1)
                  call MultmQ(TransTempS(1,ntr,isw),stsy(1,k,ii),
     1                        spya(nn),NRank,NRank,1)
                endif
              endif
            endif
            nn=nn+nrank
          enddo
          nn=0
          do k=1,kmodp
            iw=iabs(TransKwSym(k,ntr))
            p11= eps(k)*csm(k)
            p21= eps(k)*snm(k)
            p12=-snm(k)
            p22= csm(k)
1100        if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
              do j=1,nrank
                jj=nn+j
                ppx=p11*pxa(jj)+p12*pya(jj)
                ppy=p21*pxa(jj)+p22*pya(jj)
                sppx=abs(p11*spxa(jj)+p12*spya(jj))
                sppy=abs(p21*spxa(jj)+p22*spya(jj))
                if(n.eq.0) then
                  axm(iw,io)=ppx
                  aym(iw,io)=ppy
                  saxm(iw,io)=sppx
                  saym(iw,io)=sppy
                else if(n.eq.1) then
                  if(m.eq.1) then
                     utx(j,iw,io)= ppx
                     uty(j,iw,io)= ppy
                    sutx(j,iw,io)=sppx
                    suty(j,iw,io)=sppy
                  else
                     urx(j,iw,io)= ppx
                     ury(j,iw,io)= ppy
                    surx(j,iw,io)=sppx
                    sury(j,iw,io)=sppy
                  endif
                else if(n.eq.2) then
                  if(m.eq.1) then
                     ttx(j,iw,io)= ppx
                     tty(j,iw,io)= ppy
                    sttx(j,iw,io)=sppx
                    stty(j,iw,io)=sppy
                  else if(m.eq.2) then
                     tlx(j,iw,io)= ppx
                     tly(j,iw,io)= ppy
                    stlx(j,iw,io)=sppx
                    stly(j,iw,io)=sppy
                  else
                     tsx(j,iw,io)= ppx
                     tsy(j,iw,io)= ppy
                    stsx(j,iw,io)=sppx
                    stsy(j,iw,io)=sppy
                  endif
                endif
              enddo
            else
              kk=k-kmodp+NDimI(KPhase)
              epsp=eps(kk)
              if(n.eq.0) then
                if(NDimI(KPhase).eq.1) then
                  delta=a0m(ii)*.5
                else
                  delta=pya(nn+1)*.5
                endif
                call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
                do j=1,2
                  if(j.eq.1) then
                    xp(kk+3)=pxa(nn+1)-delta
                  else
                    xp(kk+3)=pxa(nn+1)+delta
                  endif
                  call multm(TransMat(1,ntr,isw),xp,xps(1,j),
     1                       NDim(KPhase),NDim(KPhase),1)
                  call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                        NDim(KPhase))
                enddo
                do j=4,NDim(KPhase)
                  delta=abs(xps(j,2)-xps(j,1))
                  if(delta.gt..00001) then
                    jj=j+kmodp-NDimI(KPhase)-3
                    go to 2270
                  endif
                enddo
2270            pom=(xps(j,2)+xps(j,1))*.5
                j=pom
                if(pom.lt.0.) j=j-1
                axm(jj,io)=pom-float(j)
                saxm(jj,io)=spxa(nn+1)
                if(NDimI(KPhase).gt.1) then
                  aym(jj,io)=delta
                  saym(jj,io)=spxa(nn+1)
                else
                  aym(jj,io)=0.
                  saym(jj,io)=0.
                endif
              else if(n.eq.1) then
                if(m.eq.1) then
                  xp(4)=pya(nn+1)
                  call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                       NDim(KPhase),1)
                  call AddVek(xpp,TransVec(1,ntr,isw),xpp,NDim(KPhase))
                  pom=xpp(4)
                  j=pom
                  if(pom.lt.0.) j=j-1
                  uty(1,k,io)=pom-float(j)
                  uty(2,k,io)=pya(nn+2)
                  uty(3,k,io)=0.
                  call CopyVek(spya(nn+1),suty(1,k,io),3)
                  do j=1,3
                    jj=nn+j
                    utx(j,k,io)=epsp*pxa(jj)
                  enddo
                  call CopyVek(spxa(nn+1),sutx(1,k,io),3)
                else
                  call SetRealArrayTo( urx(1,k,io),3,0.)
                  call SetRealArrayTo( ury(1,k,io),3,0.)
                  call SetRealArrayTo(surx(1,k,io),3,0.)
                  call SetRealArrayTo(sury(1,k,io),3,0.)
                endif
              endif
            endif
            nn=nn+nrank
          enddo
        enddo
      enddo
      do i=iap,iak
        do n=0,2
          kmodp=KModA(n+1,i)
          if(kmodp.le.0) cycle
          kfap=KFA(n+1,i)
          nrank=TRank(n)
          if(kfap.ne.0) then
            if(n.eq.0) then
              NDimIp=NDimI(KPhase)
            else
              NDimIp=1
            endif
          endif
          nn=kmodp*nrank
          if(n.eq.0) then
            a0(i)=a0(ii)
            call CopyVek( ax(1,i), pxa,nn)
            call CopyVek( ay(1,i), pya,nn)
            call CopyVek(sax(1,i),spxa,nn)
            call CopyVek(say(1,i),spya,nn)
          else if(n.eq.1) then
            call CopyVek( ux(1,1,i), pxa,nn)
            call CopyVek( uy(1,1,i), pya,nn)
            call CopyVek(sux(1,1,i),spxa,nn)
            call CopyVek(suy(1,1,i),spya,nn)
          else if(n.eq.2) then
            call CopyVek( bx(1,1,i), pxa,nn)
            call CopyVek( by(1,1,i), pya,nn)
            call CopyVek(sbx(1,1,i),spxa,nn)
            call CopyVek(sby(1,1,i),spya,nn)
          endif
          nn=0
          do k=1,kmodp
            iw=iabs(TransKwSym(k,ntr))
            snp=snm(k)
            csp=csm(k)
            epsp=eps(k)
            if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
              do j=1,nrank
                jj=nn+j
                ppx=epsp*csp*pxa(jj)-snp*pya(jj)
                ppy=epsp*snp*pxa(jj)+csp*pya(jj)
                sppx=abs(epsp*csp*spxa(jj)-snp*spya(jj))
                sppy=abs(epsp*snp*spxa(jj)+csp*spya(jj))
                if(n.eq.0) then
                  ax(iw,i)=ppx
                  ay(iw,i)=ppy
                  sax(iw,i)=sppx
                  say(iw,i)=sppy
                else if(n.eq.1) then
                  ux(j,iw,i)= ppx
                  uy(j,iw,i)= ppy
                  sux(j,iw,i)=sppx
                  suy(j,iw,i)=sppy
                else if(n.eq.2) then
                  bx(j,iw,i)= ppx
                  by(j,iw,i)= ppy
                  bx(j,iw,i)=sppx
                  by(j,iw,i)=sppy
                endif
              enddo
            else
              kk=k-kmodp+NDimI(KPhase)
              epsp=eps(kk)
              if(n.eq.0) then
                if(NDimI(KPhase).eq.1) then
                  delta=a0(i)*.5
                else
                  delta=pya(nn+1)*.5
                endif
                call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
                do j=1,2
                  if(j.eq.1) then
                    xp(kk+3)=pxa(nn+1)-delta
                  else
                    xp(kk+3)=pxa(nn+1)+delta
                  endif
                  call multm(TransMat(1,ntr,isw),xp,xps(1,j),
     1                       NDim(KPhase),NDim(KPhase),1)
                  call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                        NDim(KPhase))
                enddo
                do j=4,NDim(KPhase)
                  delta=abs(xps(j,2)-xps(j,1))
                  if(delta.gt..00001) then
                    jj=j+kmodp-NDimI(KPhase)-3
                    go to 3270
                  endif
                enddo
3270            pom=(xps(j,2)+xps(j,1))*.5
                j=pom
                if(pom.lt.0.) j=j-1
                ax(jj,i)=pom-float(j)
                sax(jj,i)=spxa(nn+1)
                if(NDimI(KPhase).gt.1) then
                  ay(jj,i)=delta
                  say(jj,i)=spxa(nn+1)
                else
                  ay(jj,i)=0.
                  say(jj,i)=0.
                endif
              else if(n.eq.1) then
                xp(4)=pya(nn+1)
                call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                     NDim(KPhase),1)
                call AddVek(xpp,TransVec(1,ntr,isw),xpp,NDim(KPhase))
                pom=xpp(4)
                j=pom
                if(pom.lt.0.) j=j-1
                uy(1,k,i)=pom-float(j)
                uy(2,k,i)=pya(nn+2)
                uy(3,k,i)=0.
                call CopyVek(spya(nn+1),suy(1,k,i),3)
                do j=1,3
                  jj=nn+j
                  utx(j,k,io)=epsp*pxa(jj)
                enddo
                call CopyVek(spya(nn+1),sux(1,k,i),3)
              endif
            endif
          enddo
          nn=nn+nrank
        enddo
      enddo
      if(TypeModFunMol(ii).eq.1) then
        OrthoEpsMol(io)=OrthoEpsMol(ii)
        OrthoDeltaMol(io)=OrthoDeltaMol(ii)
        xp(4)=OrthoX40Mol(ii)
        call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),
     1             1)
        call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
        pom=xp(4)
        l=pom
        if(pom.lt.0.) l=l-1
        OrthoX40Mol(io)=pom-float(l)
      endif
      if(ii.ne.io) TypeModFun(io)=TypeModFun(ii)
9999  deallocate(snm,csm,eps,pxa,pya,spxa,spya)
      return
      end
