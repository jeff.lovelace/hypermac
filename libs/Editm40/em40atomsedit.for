      subroutine EM40AtomsEdit
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),lmodan(8)
      character*256 EdwStringQuest
      character*80 Veta
      character*12 at,pn
      character*2  nty
      integer PrvKi,EdwStateQuest,CrwStateQuest,PrvKiMag
      logical lpom,Prvni,FeYesNoHeader,ExistFile
      save nEdwNo,nButtAtomList,NAtSelOld,nLblAtomName,nLblAtomType,
     1     nButtRefineAll,nButtFixAll,nButtReset,nEdwPrvPar,nCrwPrvPar,
     2     ltfo,PrvKi,npar,nButtEditADP,nButtSymmetry,
     3     nButtShowRest,nButtResetOcc,nButtEditModFirst,
     4     nButtEditModLast,nButtEditMult,lmodan,nEdwPrvParMag,
     5     nCrwPrvParMag
      entry EM40AtomsEditMake(id)
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      il=1
      tpom=5.
      if(NSel.eq.1) then
        Veta='%#'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNo=EdwLastMade
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
        call FeQuestEudOpen(nEdwNo,1,NAtActive,1,0.,0.,0.)
        xpom=xpom+EdwYd+dpom+10.
        Veta='%List'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAtomList=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='Name'
        tpom=xpom+dpom+20.
        xpom=tpom+FeTxLengthUnder(Veta)+10.+EdwMarginSize
        dpom=FeTxLengthUnder('XXXXXXXX')+5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblMake(id,xpom,il,' ','L','N')
        nLblAtomName=LblLastMade
        tpom=xpom+FeTxLengthUnder('XXXXXXXX')+30.-EdwMarginSize
        Veta='Type'
        xpom=tpom+FeTxLengthUnder(Veta)+10.+EdwMarginSize
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblMake(id,xpom,il,' ','L','N')
        nLblAtomType=LblLastMade
      else
        nEdwNo=0
        nButtAtomList=0
        nLblAtomName=0
        nLblAtomType=0
      endif
      il=il+1
      xqd=XdQuestEM40-2.*KartSidePruh
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      PrvKi=PrvniKiAtomu(NAtSelAbs)
      kip=PrvKi
      j=0
      do i=1,10
        call kdoco(kip,at,pn,1,pom,spom)
        if(i.gt.4.and.pn.eq.' ') pn='Uiso'
        j=j+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        kip=kip+1
        if(i.eq.1) then
          nEdwPrvPar=nEdw
          nCrwPrvPar=nCrw
        endif
        if(mod(i,4).eq.0) then
          il=il+1
          j=0
        endif
      enddo
      if(CMagPar.gt.-2) then
        if(CMagPar.ne.0) kip=PrvKiMag(NAtSelAbs)+PrvKi-1
        j=0
        il=il+1
        do i=1,3
          if(CMagPar.ne.0) then
            call kdoco(kip,at,pn,1,pom,spom)
            if(idel(pn).ne.3) pn='Mx0'
          else
            pn='Mx0'
          endif
          j=j+1
          call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
          if(CMagPar.ne.0) kip=kip+1
          if(i.eq.1) then
            nEdwPrvParMag=nEdw
            nCrwPrvParMag=nCrw
          endif
        enddo
      else
        nEdwPrvParMag=0
        nCrwPrvParMag=0
      endif
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          Veta='%Refine all'
        else if(i.eq.2) then
          Veta='Fi%x all'
        else if(i.eq.3) then
          Veta='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        if(i.ne.3.or.ctfa.ge.0) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(NSel.gt.1) then
        Veta='R%eset site occupancy'
      else
        Veta='Sho%w/reset site occupancy'
      endif
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=xqd*.5-1.5*dpom-15.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        j=ButtonOff
        if(i.eq.1) then
          nButtResetOcc=ButtonLastMade
          Veta='Appl%y site symmetry'
        else if(i.eq.2) then
          nButtSymmetry=ButtonLastMade
          Veta='S%how symmetry restrictions'
        else if(i.eq.3) then
          nButtShowRest=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+20.
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Edit special parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      nLblEditSpec=LblLastMade
      if(NDimI(KPhase).eq.0) then
        if(ChargeDensities) then
          iek=2
        else
          iek=1
        endif
      else
        if(NAtSelMol.eq.0) then
          iek=9
        else
          iek=5
        endif
        Veta='Edit modulation parameters:'
        call FeQuestLblMake(id,5.,il+1,Veta,'L','B')
      endif
      call FeBoldFont
      xpom0=tpom+FeTxLength(Veta)+10.
      call FeNormalFont
      xpom=xpom0
      do i=1,iek
        if(i.eq.1) then
          Veta='A%DP'
        else if(i.eq.2) then
          if(NDimI(KPhase).eq.0) then
            if(.not.ChargeDensities) cycle
            Veta='%Multipole(s)'
          else
            Veta='%Occupancy'
          endif
        else if(i.eq.3) then
          Veta='%Position'
        else if(i.eq.4) then
          Veta='Ma%gnetic'
        else
          j=i-3
          write(Veta,100) j,nty(j)
        endif
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtEditADP=ButtonLastMade
        else if(i.eq.2) then
          if(NDimI(KPhase).eq.0) then
            nButtEditMult=ButtonLastMade
            nButtEditModFirst=0
            nButtEditModLast=0
          else
            nButtEditMult=0
            nButtEditModFirst=ButtonLastMade
          endif
        endif
        if((i.eq.1.or.i.eq.4).and.NDimI(KPhase).gt.0) then
          il=il+1
          xpom=xpom0
        else
          xpom=xpom+dpom+pom
        endif
      enddo
      if(NDimI(KPhase).gt.0) nButtEditModLast=ButtonLastMade
      NAtSelOld=-1
      go to 2000
      entry EM40AtomsEditUpdate
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      entry EM40AtomsEditRenew
2000  call EM40GetAtom(NAtSelAbs,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                 MagParA,PrvKi)
      if(ctfa.gt.0) call FeQuestButtonOpen(nButtReset,ButtonOff)
      AtTypeName=AtTypeMenu(isfa)
      ltfa=0
      llasmaxa=0
      lmodan=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          ltfa=max(ltfa,itf(ia))
          llasmaxa=max(llasmaxa,lasmax(ia))
          do j=1,7
            lmodan(j)=max(lmodan(j),KModA(j,ia))
          enddo
          lmodan(8)=max(lmodan(8),MagPar(ia)-1)
        endif
      enddo
      if(ltfa.eq.0) then
        npar=4
      else if(ltfa.eq.1) then
        npar=5
      else
        npar=10
      endif
      if(NAtSel.ne.NAtSelOld) ltfo=-1
      if(NSel.eq.1)
     1  call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      if(ltfa.le.2) then
        call FeQuestButtonDisable(nButtEditADP)
      else
        call FeQuestButtonOff(nButtEditADP)
      endif
      if(NDimI(KPhase).eq.0) then
        if(ChargeDensities) then
          if(lasmaxa.le.1) then
            call FeQuestButtonDisable(nButtEditMult)
          else
            call FeQuestButtonOff(nButtEditMult)
          endif
        endif
      else
        nButt=nButtEditModFirst
        k=0
        do i=1,8
          if(i.eq.3) then
            k=8
          else
            k=k+1
          endif
          if(lmodan(k).gt.0) then
            call FeQuestButtonOff(nButt)
          else
            call FeQuestButtonDisable(nButt)
          endif
          if(i.eq.3) k=2
          nButt=nButt+1
        enddo
      endif
      ltfo=-1
      if(ltfa.ne.ltfo) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        do j=1,10
          Prvni=.true.
          if(j.le.npar) then
            do i=1,NAtActive
              if(.not.LAtActive(i)) cycle
              ia=IAtActive(i)
              kip=PrvniKiAtomu(ia)+j-1
              call kdoco(kip,at,pn,1,pom,spom)
              if(Prvni) then
                call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                call FeQuestCrwOpen(nCrw,KiA(j,ia).ne.0)
                call FeQuestEdwLabelChange(nEdw,pn)
                pomc=pom
                kic=KiA(j,ia)
                Prvni=.false.
              else
                if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1             abs(pom-pomc).gt..000001)
     2            call FeQuestEdwLock(nEdw)
                if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1             KiA(j,ia).ne.kic)
     2            call FeQuestCrwLock(nCrw)
              endif
            enddo
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(CMagPar.gt.-2) then
        nEdw=nEdwPrvParMag
        nCrw=nCrwPrvParMag
        do j=1,3
          Prvni=.true.
          if(MagPar(ia).gt.0) then
            do i=1,NAtActive
              if(.not.LAtActive(i)) cycle
              ia=IAtActive(i)
              kip=PrvKiMag(ia)+j-1
              call kdoco(kip+PrvniKiAtomu(ia)-1,at,pn,1,pom,spom)
              if(Prvni) then
                call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                call FeQuestCrwOpen(nCrw,KiA(kip,ia).ne.0)
                call FeQuestEdwLabelChange(nEdw,pn)
                pomc=pom
                kic=KiA(kip,ia)
                Prvni=.false.
              else
                if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1             abs(pom-pomc).gt..000001)
     2            call FeQuestEdwLock(nEdw)
                if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1             KiA(kip,ia).ne.kic)
     2            call FeQuestCrwLock(nCrw)
              endif
            enddo
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(nLblAtomName.gt.0) then
        call FeQuestLblChange(nLblAtomName,AtName)
        call FeQuestLblChange(nLblAtomType,AtTypeName)
      endif
      NAtSelOld=NAtSel
      ltfo=ltfa
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      go to 9999
      entry EM40AtomsEditCheck
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNo) then
        call FeQuestIntFromEdw(nEdwNo,NAtSel)
        NAtSelAbs=IAtActive(NAtSel)
        call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
        LAtActive(NAtSel)=.true.
        if(NAtSel.ne.NAtSelOld) go to 5000
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtomName)
     1  then
        AtName=EdwStringQuest(nEdwAtomName)
        call UprAt(AtName)
        call AtCheck(AtName,i,j)
        if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the atom name.'
          else if(i.eq.2) then
            if(j.ne.NAtSel) then
              Veta='The atom name already exists.'
            else
              go to 2200
            endif
          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwAtomName
          go to 9999
        endif
2200    call FeQuestStringEdwOpen(nEdwAtomName,AtName)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtomList)
     1  then
        call SelOneAtom('Select next atom',NameAtActive,NAtSel,
     1                  NAtActive,ich)
        NAtSelAbs=IAtActive(NAtSel)
        if(NAtSel.ne.NAtSelOld.and.ich.eq.0) then
          go to 5000
        else
          NAtSel=NAtSelOld
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrvPar
        do i=1,npar
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,NAtSel)
        pom=2.5
        isw=iswa(NAtSel)
        if(ltfa.ge.2) then
          do i=1,6
            beta(i,NAtSel)=pom*prcp(i,isw,KPhase)
          enddo
        else if(ltfa.eq.1) then
          beta(1,NAtSel)=pom
        endif
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,NAtSel)
        nEdw=nEdwPrvPar+4
        do i=1,npar-4
          call FeQuestRealEdwOpen(nEdw,beta(i,NAtSel),.false.,.false.)
          nEdw=nEdw+1
        enddo
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditADP)
     1  then
        call EM40EditADP
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.ge.nButtEditModFirst.and.
     2        CheckNumber.le.nButtEditModLast) then
        n=CheckNumber-nButtEditModFirst+1
        if(n.eq.3) then
          n=8
        else if(n.gt.3) then
          n=n-1
        endif
        call EM40EditAtomModPar(n,lmodan(n))
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditMult)
     1  then
        call EM40EditMultipole
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSymmetry.or.
     2         CheckNumber.eq.nButtShowRest)) then
        Veta=fln(:ifln)//'_pom.tmp'
        KPhaseIn=KPhase
        nvai=0
        neq=0
        neqs=0
        LstOpened=.true.
        uloha='Symmetry restrictions'
        call OpenFile(lst,Veta,'formatted','unknown')
        call NewPg(1)
        MaxUsedItfAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          MaxUsedItf(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh) cycle
            MaxUsedItf(KPh)=max(MaxUsedItf(KPh),itf(i))
            if(NDimI(KPh).le.0) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
          MaxUsedItfAll=max(MaxUsedItfAll,MaxUsedItf(KPh))
        enddo
        if(Allocated(KWSym)) deallocate(KwSym)
        KPhase=kswa(ia)
        if(NDimI(KPhase).gt.0) then
          allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
          call SetSymmWaves
        endif
        call MagParToCell(0)
        ia=IAtActive(NAtSelOld)
        call atspec(ia,ia,NAtSelMolPos,NAtSelMol,NAtSelPos)
        do i=1,neq
          KiA(lnp(i)-PrvniKiAtomu(ia)+1,NAtSelOld)=0
        enddo
        call RefAppEq(0,0,0)
        call MagParToBohrMag(0)
        call CloseListing
        LstOpened=.false.
        if(.not.ExistFile(Veta)) then
          LstOpened=.true.
          uloha='Symmetry restrictions'
          call OpenFile(lst,Veta,'formatted','unknown')
          call NewPg(1)
          call newln(2)
          write(lst,FormA)
          write(lst,FormA) 'No symmetry restrictions -  the atom '//
     1                     'is located at a general position'
          call CloseListing
          LstOpened=.false.
        endif
        if(CheckNumber.eq.nButtShowRest) call FeListView(Veta,0)
        call DeleteFile(Veta)
        NAtSelOld=0
        KPhase=KPhaseIn
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtResetOcc)
     1  then
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            xpoma(1)=0.
            k=0
            if(NDimI(KPhase).eq.1) then
              if(KFA(1,ia).ne.0) then
                xpoma(1)=ax(KModA(1,ia),ia)
                k=1
              else if(KFA(2,ia).ne.0) then
                xpoma(1)=uy(1,KModA(2,ia),ia)
                k=1
              endif
            endif
            call SpecPos(x(1,ia),xpoma,k,iswa(ia),.1,nocc)
            pom=1./float(nocc)
            if(NSel.eq.1) then
              NInfo=1
              write(Cislo,'(f9.6)') 1./float(nocc)
              TextInfo(1)='The full site occupacy is: '//
     1                    Cislo(:idel(Cislo))
              if(abs(ai(ia)-pom).lt..000001) then
                call FeInfoOut(-1.,-1.,'Information','C')
              else
                if(FeYesNoHeader(-1.,-1.,'Do you want to reset the '//
     1                           'actual occupancy?',0)) then
                  ai(ia)=pom
                  NAtSelOld=0
                  go to 2000
                endif
              endif
            else
              ai(ia)=pom
            endif
          endif
        enddo
        go to 2000
      endif
      go to 9999
5000  if(NAtSelOld.ne.0) then
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      endif
      ChargeDensities=.false.
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(lasmax(i).gt.0) then
          ChargeDensities=.true.
          go to 2000
        endif
      enddo
      go to 2000
9999  return
100   format('ADP %',i1,a2)
      end
