      subroutine EM40EditMolModPar(im,ModPar)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4)
      real, allocatable :: px(:,:),py(:,:),spx(:,:),spy(:,:),pxp(:)
      character*80 Veta
      character*12 at,pn,pompn
      integer PrvKiMod,Order,PrvPar,PrvParOld,Harm,HarmOld,HarmP,
     1        HarmPOld
      integer, allocatable :: kip(:),kipp(:)
      logical :: lpom,Konec,InSig = .false.
      Konec=.false.
      if(ModPar.eq.1) then
        Order=1
        lmod=KModM(1,im)
      else if(ModPar.le.3) then
        Order=3
        lmod=KModM(2,im)
      else if(ModPar.le.5) then
        Order=6
        lmod=KModM(3,im)
      else if(ModPar.le.6) then
        Order=9
        lmod=KModM(3,im)
      endif
      nki=2*lmod*Order
      if(ModPar.eq.1) nki=nki+1
      allocate(px(Order,lmod),py(Order,lmod),spx(Order,lmod),
     1         spy(Order,lmod),kip(nki),pxp(2*Order+1),kipp(2*Order+1))
      l=lmod*Order
      nki=2*l
      if(ModPar.eq.1) then
        nki=nki+1
        p0=a0m(im)
        sp0=sa0m(im)
        call CopyVek( axm(1,im), px,l)
        call CopyVek( aym(1,im), py,l)
        call CopyVek(saxm(1,im),spx,l)
        call CopyVek(saym(1,im),spy,l)
      else if(ModPar.eq.2) then
        call CopyVek( utx(1,1,im), px,l)
        call CopyVek( uty(1,1,im), py,l)
        call CopyVek(sutx(1,1,im),spx,l)
        call CopyVek(suty(1,1,im),spy,l)
      else if(ModPar.eq.3) then
        call CopyVek( urx(1,1,im), px,l)
        call CopyVek( ury(1,1,im), py,l)
        call CopyVek(surx(1,1,im),spx,l)
        call CopyVek(sury(1,1,im),spy,l)
      else if(ModPar.eq.4) then
        call CopyVek( ttx(1,1,im), px,l)
        call CopyVek( tty(1,1,im), py,l)
        call CopyVek(sttx(1,1,im),spx,l)
        call CopyVek(stty(1,1,im),spy,l)
      else if(ModPar.eq.5) then
        call CopyVek( tlx(1,1,im), px,l)
        call CopyVek( tly(1,1,im), py,l)
        call CopyVek(stlx(1,1,im),spx,l)
        call CopyVek(stly(1,1,im),spy,l)
      else if(ModPar.le.6) then
        call CopyVek( tsx(1,1,im), px,l)
        call CopyVek( tsy(1,1,im), py,l)
        call CopyVek(stsx(1,1,im),spx,l)
        call CopyVek(stsy(1,1,im),spy,l)
      endif
      InSig=.false.
      PrvKiMod=8
      if(ktls((im-1)/mxp+1).gt.0) PrvKiMod=PrvKiMod+21
      if(ModPar.gt.1.and.KModM(1,im).gt.0)
     1  PrvKiMod=PrvKiMod+1+2*KModM(1,im)
      if(ModPar.gt.2) PrvKiMod=PrvKiMod+6*KModM(2,im)
      if(ModPar.gt.3) PrvKiMod=PrvKiMod+6*KModM(2,im)
      if(ModPar.gt.4) PrvKiMod=PrvKiMod+12*KModM(3,im)
      if(ModPar.gt.5) PrvKiMod=PrvKiMod+12*KModM(3,im)
      call CopyVekI(KiMol(PrvKiMod,im),kip,nki)
!      if(ModPar.le.4.or.ModPar.eq.8) then
        nn=2
!      else
!        nn=1
!      endif
      no=nn*Order
      if(ModPar.eq.1) then
        il=3
      else
        il=nn*((Order-1)/3+2)+1
      endif
      if(ModPar.eq.1.or.ModPar.eq.2) then
        if(KFM(2,im).ne.0) then
          pompn='x-slope'
        else
          pompn='xcos33'
        endif
      else
        pompn='U33cos33'
      endif
      xqd=550.+3.*(FeTxLength(pompn)-FeTxLength('F111111cos11'))
      xqdp=xqd*.5
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      pom=FeTxLengthUnder(pompn)+5.
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit modulation amplitudes',
     2                   1,LightGray,0,OKForBasicFiles)
      il=1
      at='%Wave'
      xpom=xqdp
      tpom=xpom-FeTxLength(at)-5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwHarm=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,1,(2*lmod)/nn,1,0.,0.,0.)
      il=il+1
      j=1
      if(ModPar.eq.1) no=no+1
      do i=1,no
        if(i.eq.Order+1.and.ModPar.gt.1) then
          if(j.ne.1) il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          j=1
        endif
        call kdoco(PrvKiMod+PrvniKiMolekuly(im)+i-2,at,pn,1,pom,spom)
        if(ModPar.ne.1.or.i.ne.1) pn=pompn
        call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
        call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        if(.not.InSig) then
          call FeQuestLblOff(LblLastMade-1)
          call FeQuestLblOff(LblLastMade)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kip(i),
     1                         .false.)
        endif
        if(i.eq.1) then
          nLblFirst=LblLastMade-1
          nEdwPrv=nEdw
          nCrwPrv=nCrw
          PrvPar=i
        else if(i.eq.no) then
          exit
        endif
        if(j.eq.3) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      iki=1
      Harm=1
      HarmOld=1
      PrvParOld=PrvPar
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
1400  k=0
      m=0
      idf=nn*(Harm-1)*Order+1-iki
      if(nn.eq.1) then
        HarmP=(Harm-1)/2+1
        HarmPOld=(HarmOld-1)/2+1
        lold=mod(HarmOld-1,2)+1
        l=mod(Harm-1,2)+1
      else
        HarmP=Harm
        HarmPOld=HarmOld
        l=1
        lold=1
      endif
      if(.not.InSig) call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,
     1                                         kipp,no)
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblFirst
      do i=iki,iki+no-1
        if(ModPar.eq.1) then
          if(i.eq.iki) then
            j=1
          else
            j=i
            k=k+1
          endif
        else
          j=i
          k=k+1
        endif
        if(k.gt.Order) then
          k=1
          l=2
          lold=2
        endif
        if(.not.InSig) then
          m=m+1
          kip(j)=kipp(m)
          if(k.eq.0) then
            if(kip(j).lt.100) p0=pxp(m)
          else
            if(lold.eq.1) then
              if(kip(j).lt.100) px(k,HarmPOld)=pxp(m)
            else
              if(kip(j).lt.100) py(k,HarmPOld)=pxp(m)
            endif
          endif
        endif
        if(ModPar.eq.1.and.i.eq.iki) then
          pom=p0
          poms=sp0
        else
          if(l.eq.1) then
            pom=px(k,HarmP)
            poms=spx(k,HarmP)
          else
            pom=py(k,HarmP)
            poms=spy(k,HarmP)
          endif
        endif
        if(InSig) then
          if(poms.gt.0.) then
            pom=abs(pom/poms)
          else
            pom=-1.
          endif
        endif
        if(ModPar.ne.1.or.i.ne.iki) j=j+idf
        call kdoco(PrvKiMod+PrvniKiMolekuly(im)+j-2,at,pn,1,pp,ss)
        if(InSig) then
          if(ModPar.ne.1.or.i.ne.iki) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            call FeQuestLblChange(nLbl,pn)
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          endif
        else
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,kip(j),.false.)
        endif
        if(i.eq.iki+no-1.and.
     1     ((ModPar.eq.1.and.KFM(1,im).ne.0.and.
     2       HarmP.eq.KModM(1,im)).or.
     3      (ModPar.eq.2.and.KFM(2,im).ne.0.and.
     4       HarmP.eq.KModM(2,im)))) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
1410    nEdw=nEdw+1
        nCrw=nCrw+1
        nLbl=nLbl+2
      enddo
      if(Konec) go to 3000
      if(idf.ne.0) then
        iki=iki+idf
        HarmOld=Harm
      endif
      if(InSig) then
        j=ButtonDisabled
      else
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        Konec=.true.
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHarm) then
        call FeQuestIntFromEdw(nEdwHarm,Harm)
        if(Harm.ne.HarmOld) then
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,no
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        if(ModPar.eq.1) then
          pom=.01
        else if(ModPar.eq.2) then
          pom=.001
        else if(ModPar.eq.3) then
          pom=.0001
        else
          pom=.00001
        endif
        do i=1,no
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        if(nn.eq.1) then
          l=mod(Harm-1,2)+1
        else
          l=1
        endif
        m=0
        k=0
        if(InSig) then
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              go to 1700
            else
              k=k+1
              if(k.gt.Order) then
                k=1
                l=2
              endif
              if(l.eq.1) then
                pom=px(k,HarmP)
              else if(l.eq.2) then
                pom=py(k,HarmP)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,kipp(m).ne.0)
            endif
1700        nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else
          call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,kipp,no)
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              j=1
              l=0
            else
              j=i
              k=k+1
            endif
            if(k.gt.Order) then
              k=1
              l=2
            endif
            kip(j)=kipp(m)
            if(l.eq.1) then
              px(k,HarmP)=pxp(m)
            else if(l.eq.2) then
              py(k,HarmP)=pxp(m)
            endif
          enddo
        endif
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        nButt=nButtRefineAll
        go to 1400
      else if(CheckType.eq.EventCrwUnlock.or.
     1        CheckType.eq.EventEdwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(ich.eq.0) then
        l=1
        if(ModPar.eq.1) then
          kk=PrvKiMod+l-1
          call kdoco(kk+PrvniKiMolekuly(im)-1,at,pn,-1,p0,spom)
          KiMol(kk,im)=kip(l)
          l=l+1
        endif
        do j=1,lmod
          do m=1,Order
            kk=PrvKiMod+l-1
            call kdoco(kk+PrvniKiMolekuly(im)-1,at,pn,-1,px(m,j),
     1                 spx(m,j))
            KiMol(kk,im)=kip(l)
            l=l+1
            k=k+1
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      deallocate(px,py,spx,spy,kip,pxp,kipp)
      return
      end
