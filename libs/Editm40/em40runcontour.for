      subroutine EM40RunContour(TorsAngle,ich)
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      character*12 Labels(6)
      character*80 SaveFile,Veta
      dimension xdo(3),xp(3),xpo(3),xfract(3)
      integer FeGetSystemTime,TimeQuestEvent
      logical Drzi
      data Labels/'%Escape','%Apply','R%+','R%-','%Step','%Optimal'/
      ich=0
      TorsAngle=KeepAngleH(1)
      ContourQuest=NextQuestId()
      call FeQuestAbsCreate(ContourQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      CheckMouse=.true.
      QuestGetEdwActive(ContourQuest)=.false.
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtEsc=ButtonLastMade
        else if(i.eq.2) then
          nButtApply=ButtonLastMade
          wpom=wpom/2.-2.5
        else if(i.eq.3) then
          nButtRotPlus=ButtonLastMade
          xpom=xpom+wpom+5.
          ypom=ypom+dpom
        else if(i.eq.4) then
          nButtRotMinus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.5) then
          nButtDefStep=ButtonLastMade
        else if(i.eq.6) then
          nButtOptimal=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.2) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          ypom=ypom-1.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+dpom-6.
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,White)
      TakeMouseMove=.true.
      call FeClearGrWin
      kpdf=1
      obecny=kpdf.gt.0
      DrawPDF=kpdf.gt.1
      isoucet=0
      smapy=.false.
      tmapy=.false.
      AtomsOn=.false.
      call NactiM81
      if(ErrFlag.ne.0) go to 9000
      call ConMakeGenSection(ich)
      if(ich.ne.0) go to 9000
      call ConCalcGeneral
      if(ErrFlag.ne.0) go to 9000
      do i=3,6
        nxdraw(i-2)=1
        nxfrom(i-2)=1
        nxto(i-2)=nx(i)
      enddo
      nxdraw(1)=0
      irec=-1
      irecold=-1
      reconfig=.true.
      if(xyzmap) call TrPor
      call SetTr
      i=ConTintType
      ConTintType=1
      call KresliMapu(0)
      ConTintType=i
      call FeHeaderInfo(' ')
      do i=1,2
        if(i.eq.1) then
          Veta='Density'
          xpom=XMinBasWin+50.
          ypom=YMaxBasWin-20.
          ypomt=YMaxBasWin-12.
          dpom=60.
          xpomt=xpom+dpom+20.
        else if(i.eq.2) then
          Veta='Torsion angle'
          xpom=xpom+600.
          xpomt=xpom-FeTxLength(Veta)-20.
        endif
        call FeQuestAbsLblMake(ContourQuest,xpomt,ypomt,Veta,'L','N')
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      AngStep=.1
      do i=1,2
        if(i.eq.1) then
          write(Veta,100) EM40HDensity()
        else if(i.eq.2) then
          write(Veta,101) KeepAngleH(1)
        endif
        call Zhusti(Veta)
        call FeWInfWrite(i,Veta)
      enddo
      SaveFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SaveFile,i,1)
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SaveFile)
2000  call FeClearGrWin
      call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SaveFile,
     1                 2)
      call ConDrawAtoms(zmap)
      do i=1,2
        if(i.eq.1) then
          write(Veta,100) EM40HDensity()
        else if(i.eq.2) then
          write(Veta,101) KeepAngleH(1)
        endif
        call Zhusti(Veta)
        call FeWInfWrite(i,Veta)
      enddo
2500  call FeQuestEvent(ContourQuest,ich)
      TimeQuestEvent=FeGetSystemTime()
      Drzi=.false.
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtESC.or.CheckNumber.eq.nButtApply) then
          call Del8
          call FeMakeGrWin(0.,0.,YBottomMargin,0.)
          call CloseIfOpened(m8)
          if(CheckNumber.eq.nButtESC) then
            ich=1
          else
            ich=0
            TorsAngle=KeepAngleH(1)
          endif
          call FeQuestRemove(ContourQuest)
        else if(CheckNumber.eq.nButtRotPlus.or.
     1          CheckNumber.eq.nButtRotMinus) then
2600      if(CheckNumber.eq.nButtRotPlus) then
            KeepAngleH(1)=KeepAngleH(1)+AngStep
          else
            KeepAngleH(1)=KeepAngleH(1)-AngStep
          endif
2605      if(KeepAngleH(1).gt.180.) then
            KeepAngleH(1)=KeepAngleH(1)-360.
            go to 2605
          endif
2610      if(KeepAngleH(1).le.-180.) then
            KeepAngleH(1)=KeepAngleH(1)+360.
            go to 2610
          endif
          call EM40SetNewH(1)
          call FeClearGrWin
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     SaveFile,2)
          call ConDrawAtoms(zmap)
          do i=1,2
            if(i.eq.1) then
              write(Veta,100) EM40HDensity()
            else if(i.eq.2) then
              write(Veta,101) KeepAngleH(1)
            endif
            call Zhusti(Veta)
            call FeWInfWrite(i,Veta)
          enddo
          call FeReleaseOutput
          call FeDeferOutput
2700      call FeEvent(1)
          i=CheckNumber+ButtonFr-1
          if(EventType.eq.0) then
            if(FeGetSystemTime()-TimeQuestEvent.lt.500) then
              go to 2700
            else
              Drzi=.true.
              go to 2600
            endif
          endif
          Drzi=.false.
          go to 2500
        else if(CheckNumber.eq.nButtDefStep) then
          idp=NextQuestId()
          xqd=160.
          il=1
          call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
          Veta='Angle step'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          dpom=60.
          call FeQuestEudMake(idp,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
          nEdwAngStep=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,AngStep,.false.,.false.)
          call FeQuestEudOpen(EdwLastMade,0,0,0,.1,180.,.1)
3000      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3000
          endif
          if(ich.eq.0) call FeQuestRealFromEdw(nEdwAngStep,AngStep)
          call FeQuestRemove(idp)
          call FeReleaseOutput
          go to 2000
        else if(CheckNumber.eq.nButtOptimal) then
          if(KeepType(1).eq.IdKeepHTetraHed) then
            n=1200
            KeepAngleH(1)=KeepAngleH(1)-60.
          else
            n=1800
            KeepAngleH(1)=KeepAngleH(1)-90.
          endif
          pom=-99999.
          do i=1,n
            KeepAngleH(1)=KeepAngleH(1)+.1
            call EM40SetNewH(1)
            Density=EM40HDensity()
            if(Density.gt.pom) then
              pom =Density
              poma=KeepAngleH(1)
            endif
          enddo
3205      if(poma.gt.180.) then
            poma=poma-360.
            go to 3205
          endif
3210      if(poma.le.-180.) then
            poma=poma+360.
            go to 3210
          endif
          KeepAngleH(1)=poma
          call EM40SetNewH(1)
          go to 2000
        endif
      else if(CheckType.eq.EventMouse) then
        call GetCoord(xdo,xpo,xfract)
        AngOld=atan2(xpo(2),xpo(1))/ToRad
        if(CheckNumber.eq.JeLeftDown) then
          TakeMouseMove=.true.
3250      call FeEvent(1)
          if(EventType.eq.EventMouse) then
            if(EventNumber.eq.JeMove) then
              call GetCoord(xdo,xp,xfract)
              if(xp(1).ne.xpo(1).or.xp(2).ne.xpo(2)) then
                Ang=atan2(xp(2),xp(1))/ToRad
                call CopyVek(xp,xpo,3)
                if(Ang.ne.AngOld) then
                  KeepAngleH(1)=KeepAngleH(1)+Ang-AngOld
3255              if(KeepAngleH(1).gt.180.) then
                    KeepAngleH(1)=KeepAngleH(1)-360.
                    go to 3255
                  endif
3260              if(KeepAngleH(1).le.-180.) then
                    KeepAngleH(1)=KeepAngleH(1)+360.
                    go to 3260
                  endif
                  AngOld=Ang
                  call EM40SetNewH(1)
                  call FeClearGrWin
                  call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                             YMaxGrWin,SaveFile,2)
                  call ConDrawAtoms(zmap)
                  do i=1,2
                    if(i.eq.1) then
                      write(Veta,100) EM40HDensity()
                    else if(i.eq.2) then
                      write(Veta,101) KeepAngleH(1)
                    endif
                    call Zhusti(Veta)
                    call FeWInfWrite(i,Veta)
                  enddo
                  call FeReleaseOutput
                  call FeDeferOutput
                endif
                go to 3250
              endif
            else if(EventNumber.eq.JeLeftUp) then
              TakeMouseMove=.false.
              go to 2500
            endif
          else
            go to 3250
          endif
        endif
        TakeMouseMove=.false.
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      go to 9999
9000  call FeQuestRemove(ContourQuest)
      ErrFlag=0
      ich=1
9999  if(allocated(AtBrat)) deallocate(AtBrat)
      call CloseIfOpened(m8)
      call CloseIfOpened(81)
      return
100   format(f8.3)
101   format(f8.1)
      end
