      subroutine EM40DeleteAtoms
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      id=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(id.ne.3) call FeYesNoAll(-1.,-1.,'Do you want to delete '//
     1                  'atom '//atom(ia)(1:idel(atom(ia)))//'?',2,id)
          if(id.eq.4) then
            exit
          else if(id.eq.2) then
            cycle
          else
            isf(ia)=0
            call CrlAtomNamesSave(Atom(ia),'#delete#',1)
          endif
        endif
      enddo
      call EM40CleanAtoms
      return
      end
