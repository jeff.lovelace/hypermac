      subroutine DeallocateTrans
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      deallocate(TransMat,TransVec,TransX,TransM,TransTemp,TransTempS,
     1           SignTransX,TransZM,TransC3,TransC4,TransC5,TransC6)
      if(allocated(TransKwSym)) deallocate(TransKwSym)
      end
