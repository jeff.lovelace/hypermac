      subroutine ZmTF21(i)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension KModAP(7)
      itf(i)=1
      call boueq(beta(1,i),sbeta(1,i),1,pom,spom,iswa(i))
      beta(1,i)=pom
      sbeta(1,i)=spom
      call SetRealArrayTo( beta(2,i),5,0.)
      call SetRealArrayTo(sbeta(2,i),5,0.)
      call SetIntArrayTo(KiA(6,i),5,0)
      call SetIntArrayTo(KModAP,7,0)
      KModAP(1)=KModA(1,i)
      KModAP(2)=KModA(2,i)
      call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),KModAP,MagPar(i),
     1               .false.)
      do j=3,7
        KModA(j,i)=0
      enddo
      return
      end
