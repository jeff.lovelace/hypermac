      subroutine EM40RecalcOrtho(x40,delta,x,ux,uy,n,kmod,kmodo,kf,
     1                           iseln,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iseln(*),x(n),ux(n,*),uy(n,*),der(mxw21),
     1          am((mxw21*(mxw21+1))/2),yc(36),ps(mxw21,36),sol(mxw21)
      ich=0
      kmodp=kmod
      if(kf.ne.0) kmodp=kmodp-1
      nw=2*kmodp+1
      kmodop=kmodo
      if(kf.ne.0) kmodop=kmodop-1
      nwo=2*kmodop+1
      k=0
      do i=1,mxw21
        if(iseln(i).ne.0) then
          k=k+1
          if(k.ge.nwo) then
            nwn=i
            go to 1050
          endif
        endif
      enddo
      ich=1
      go to 9999
1050  kmodnp=nwn/2
      kmod=kmodnp
      if(kf.ne.0) then
        kmod=kmod+1
        call CopyVek(ux(1,kmodp+1),ux(1,kmod),n)
        call CopyVek(uy(1,kmodp+1),uy(1,kmod),n)
      endif
      call SetRealArrayTo(sol,mxw21,0.)
      call SetRealArrayTo(am,(nwo*(nwo+1))/2,0.)
      call SetRealArrayTo(ps,36*mxw21,0.)
      x4=x40-delta*.5
      dx4=.01*delta
      do i=1,100
        k=0
        do j=1,nwn
          if(j.gt.1) arg=pi2*float(kw(1,j/2,KPhase))*x4
          if(iseln(j).ne.0) then
            k=k+1
            if(j.eq.1) then
              der(k)=1.
            else if(mod(j,2).eq.0) then
              der(k)=sin(arg)
            else
              der(k)=cos(arg)
            endif
          endif
        enddo
        call CopyVek(x,yc,n)
        do j=1,kmodp
          arg=pi2*float(kw(1,j,KPhase))*x4
          sn=sin(arg)
          cs=cos(arg)
          do k=1,n
            yc(k)=yc(k)+ux(k,j)*sn+uy(k,j)*cs
          enddo
        enddo
        m=0
        do j=1,nwo
          derj=der(j)
          do k=1,j
            m=m+1
            am(m)=am(m)+derj*der(k)
          enddo
          do k=1,n
            ps(j,k)=ps(j,k)+derj*yc(k)
          enddo
        enddo
        x4=x4+dx4
      enddo
      ij=0
      do i=1,nwo
        ij=ij+i
        der(i)=1./sqrt(am(ij))
      enddo
      call znorm(am,der,nwo)
      call smi(am,nwo,ising)
      if(ising.gt.0) go to 9999
      call znorm(am,der,nwo)
      do l=1,n
        call nasob(am,ps(1,l),sol,nwo)
        k=0
        do j=1,nwn
          iw=j/2
          if(iseln(j).eq.0) then
            pom=0.
          else
            k=k+1
            pom=sol(k)
          endif
          if(j.eq.1) then
            x(l)=pom
          else if(mod(j,2).eq.0) then
            ux(l,iw)=pom
          else
            uy(l,iw)=pom
          endif
        enddo
      enddo
9999  return
      end
