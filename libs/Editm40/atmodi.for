      subroutine AtModi(ia,kmodap)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension kmodanp(7),kmn(7),kmodap(7),kiold(:)
      real :: xp(1)=(/0./)
      integer PrvKi,PrvKiOld
      allocatable kiold
      i1=ia
      i2=ia
      AtBrat(ia)=.true.
      itfp=itf(ia)+1
      call CopyVekI(kmodap,kmodanp,7)
      nalloc=0
      do i=i1,i2
        if((i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)).or..not.AtBrat(i)) cycle
        k=max(TRankCumul(itf(i)),10)
        PrvKi=k+1
        PrvKiOld=1
        nkiold=DelkaKiAtomu(i)-k
        if(nkiold.gt.nalloc) then
          if(allocated(kiold)) deallocate(kiold)
          allocate(kiold(nkiold))
          nalloc=nkiold
        endif
        if(nkiold.gt.0) call CopyVekI(KiA(PrvKi,i),kiold,nkiold)
        do j=1,7
          if(kmodanp(j).ge.0) then
            kmn(j)=kmodanp(j)
          else
            kmn(j)=KModA(j,i)
          endif
        enddo
        call ReallocateAtomParams(NAtAll,itfmax,IFrMax,kmn,MagParMax)
        call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),kmn,MagPar(i),.false.)
        do j=1,itfp
          if(KModA(j,i).gt.0) then
            KiA(DelkaKiAtomu(i),i)=kiold(nkiold)
            go to 1450
          endif
        enddo
        do j=1,itfp
          if(kmn(j).gt.0) then
            phf(i)=0.
            sphf(i)=0.
            KiA(DelkaKiAtomu(i),i)=0
            go to 1450
          endif
        enddo
1450    km=kmol(i)
        do j=1,3
          if(km.gt.0) then
            kmn(j)=KModM(j,km)
          else
            kmn(j)=0
          endif
        enddo
        if(kmodanp(1).gt.0) then
          if(KModA(1,i).le.0) then
            call SpecPos(x(1,i),xp,0,iswa(i),.05,n)
            pom=1./float(n)
            a0(i)=ai(i)/pom
            ai(i)=pom
            sa0(i)=0.
            KiA(PrvKi,i)=0
            KiA(1,i)=0
          endif
        else if(kmodanp(1).eq.0) then
          if(KModA(1,i).gt.0) then
            ai(i)=ai(i)*a0(i)
            KiA(1,i)=0
          endif
        endif
        do 2000n=1,itfp
          j1=max(KModA(n,i),0)
          j2=kmodanp(n)
          nrank=TRank(n-1)
          nrank2=2*nrank
          if(j2.lt.0) then
            j2=j1
          else if(j2.eq.0) then
            go to 2000
          endif
          if(j1.eq.j2) go to 1800
          if(KFA(n,i).ne.0.and.j1.ge.1) then
            if(nkiold.gt.0) then
              l1=PrvKiOld+nrank2*(j1-1)
              if(n.eq.1) l1=l1+1
              k1=PrvKi+nrank2*(j2-1)
              if(n.eq.1) k1=k1+1
              call CopyVekI(kiold(l1),KiA(k1,i),nrank2)
            endif
            if(n.eq.1) then
              ax(j2,i)=ax(j1,i)
              ay(j2,i)=ay(j1,i)
              sax(j2,i)=sax(j1,i)
              say(j2,i)=say(j1,i)
            else if(n.eq.2) then
              call CopyVek(ux(1,j1,i),ux(1,j2,i),nrank)
              call CopyVek(uy(1,j1,i),uy(1,j2,i),nrank)
              call CopyVek(sux(1,j1,i),sux(1,j2,i),nrank)
              call CopyVek(suy(1,j1,i),suy(1,j2,i),nrank)
            else if(n.eq.3) then
              call CopyVek(bx(1,j1,i),bx(1,j2,i),nrank)
              call CopyVek(by(1,j1,i),by(1,j2,i),nrank)
              call CopyVek(sbx(1,j1,i),sbx(1,j2,i),nrank)
              call CopyVek(sby(1,j1,i),sby(1,j2,i),nrank)
            else if(n.eq.4) then
              call CopyVek(c3x(1,j1,i),c3x(1,j2,i),nrank)
              call CopyVek(c3y(1,j1,i),c3y(1,j2,i),nrank)
              call CopyVek(sc3x(1,j1,i),sc3x(1,j2,i),nrank)
              call CopyVek(sc3y(1,j1,i),sc3y(1,j2,i),nrank)
            else if(n.eq.5) then
              call CopyVek(c4x(1,j1,i),c4x(1,j2,i),nrank)
              call CopyVek(c4y(1,j1,i),c4y(1,j2,i),nrank)
              call CopyVek(sc4x(1,j1,i),sc4x(1,j2,i),nrank)
              call CopyVek(sc4y(1,j1,i),sc4y(1,j2,i),nrank)
            else if(n.eq.6) then
              call CopyVek(c5x(1,j1,i),c5x(1,j2,i),nrank)
              call CopyVek(c5y(1,j1,i),c5y(1,j2,i),nrank)
              call CopyVek(sc5x(1,j1,i),sc5x(1,j2,i),nrank)
              call CopyVek(sc5y(1,j1,i),sc5y(1,j2,i),nrank)
            else
              call CopyVek(c6x(1,j1,i),c6x(1,j2,i),nrank)
              call CopyVek(c6y(1,j1,i),c6y(1,j2,i),nrank)
              call CopyVek(sc6x(1,j1,i),sc6x(1,j2,i),nrank)
              call CopyVek(sc6y(1,j1,i),sc6y(1,j2,i),nrank)
            endif
            j1=j1-1
            j2=j2-1
          endif
          l1=PrvKi+nrank2*j1
          if(n.eq.1) l1=l1+1
          l2=l1+nrank
          do j=j1+1,j2
            call SetIntArrayTo(KiA(l1,i),nrank,1)
            call SetIntArrayTo(KiA(l2,i),nrank,1)
            if(j.gt.kmn(n)) then
              if(n.eq.1) then
                pom=.01
              else if(n.eq.2) then
                pom=.001
              else if(n.eq.3) then
                pom=.0001
              else
                pom=.00001
              endif
            else
              pom=0.
            endif
            if(n.eq.1) then
              ax(j,i)=pom
              ay(j,i)=pom
              sax(j,i)=.0
              say(j,i)=.0
            else if(n.eq.2) then
              call SetRealArrayTo( ux(1,j,i),nrank,pom)
              call SetRealArrayTo( uy(1,j,i),nrank,pom)
              call SetRealArrayTo(sux(1,j,i),nrank,0.)
              call SetRealArrayTo(suy(1,j,i),nrank,0.)
            else if(n.eq.3) then
              call SetRealArrayTo( bx(1,j,i),nrank,pom)
              call SetRealArrayTo( by(1,j,i),nrank,pom)
              call SetRealArrayTo(sbx(1,j,i),nrank,0.)
              call SetRealArrayTo(sby(1,j,i),nrank,0.)
            else if(n.eq.4) then
              call SetRealArrayTo( c3x(1,j,i),nrank,pom)
              call SetRealArrayTo( c3y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc3x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc3y(1,j,i),nrank,0.)
            else if(n.eq.5) then
              call SetRealArrayTo( c4x(1,j,i),nrank,pom)
              call SetRealArrayTo( c4y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc4x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc4y(1,j,i),nrank,0.)
            else if(n.eq.6) then
              call SetRealArrayTo( c5x(1,j,i),nrank,pom)
              call SetRealArrayTo( c5y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc5x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc5y(1,j,i),nrank,0.)
            else
              call SetRealArrayTo( c6x(1,j,i),nrank,pom)
              call SetRealArrayTo( c6y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc6x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc6y(1,j,i),nrank,0.)
            endif
            l1=l1+nrank2
            l2=l2+nrank2
          enddo
          if(KFA(n,i).ne.0.and.n.ne.1) j2=j2+1
1800      PrvKi   =PrvKi   +nrank2*j2
          PrvKiOld=PrvKiOld+nrank2*j2
          if(n.eq.1.and.j2.gt.0) then
            PrvKi   =PrvKi   +1
            PrvKiOld=PrvKiOld+1
          endif
2000    continue
        do j=1,7
          if(kmodanp(j).ge.0) KModA(j,i)=kmodanp(j)
        enddo
      enddo
9999  return
      end
