      subroutine EM40Atoms(ich)
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      parameter (NMenuCoDal=12)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension IAtActiveOld(:)
      character*256 EdwStringQuest
      character*80 Veta
      character*50 MenuCoDal(NMenuCoDal)
      character*8  NameAtActiveOld(:)
      external EM40AtomsUpdateListek,EM40SelAtomsCheck,FeVoid,
     1         EM40AtomsUpdate
      integer FeMenu,SbwLnQuest,RolMenuSelectedQuest,
     1        SbwItemPointerQuest,FeMenuNew,CoDal,
     2        MenuCoDalEnable(NMenuCoDal),SbwItemFromQuest
      logical lpom,EqWild,BackToAdvanced,Poprve,
     1        LAtActiveOld(:),FeYesNo,FromCursorPosition
      allocatable IAtActiveOld,LAtActiveOld,NameAtActiveOld
      equivalence (IdNumbers(1) ,IdDeleteAtoms),
     1            (IdNumbers(2) ,IdMergeAtoms),
     2            (IdNumbers(3) ,IdTransAtoms),
     3            (IdNumbers(4) ,IdExpandAtoms),
     4            (IdNumbers(5) ,IdMolToAt),
     5            (IdNumbers(6) ,IdAddH),
     6            (IdNumbers(7) ,IdRename),
     7            (IdNumbers(8) ,IdRenameMan),
     8            (IdNumbers(9) ,IdMotifs),
     9            (IdNumbers(10),IdAtSplit),
     a            (IdNumbers(11),IdResetModMag),
     1            (IdNumbers(12),IdEditAtoms)
      data MenuCoDal/'%Delete selected atoms',
     1               'Mer%ge selected atoms',
     2               '%Transform selected atoms',
     3               'E%xpand selected atoms',
     4               'Atoms from m%olecule to atomic part',
     5               '%Adding of hydrogen atoms',
     6               '%Rename selected atoms to "atom_type"+number',
     7               'Re%name selected atoms manually',
     8               'Make symmetrically contiguous moti%fs',
     9               '%Split atomic position',
     a               'Reset %modulation/magnetic parameters',
     1               '%Edit/Define atoms'/
      allocate(MolMenu(mxpm))
      call SetIntArrayTo(MenuCoDalEnable,NMenuCoDal,1)
      if(StructureLocked)
     1  call SetIntArrayTo(MenuCoDalEnable,NMenuCoDal-1,0)
      KPhaseIn=KPhase
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      mxe=100
      mxep=5
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      Poprve=.true.
      ich=0
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
1000  if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      call EM40SwitchMag(0)
1100  if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      if(.not.Poprve.and.ich.eq.0) call CrlAtomNamesApply
      Poprve=.false.
      call CrlAtomNamesIni
      NMolMenu=0
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        NMolMenu=NMolMenu+1
        MolMenu(NMolMenu)=MolName(i)
      enddo
      il=17
      id=NextQuestId()
      xqd=650.
      Veta=' '
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      il=1
      tpom=5.
      LabelSel='Step #1: Select atoms to be used'
      if(NPhase.gt.1)
     1  LabelSel=LabelSel(:idel(LabelSel)+1)//'; Phase : '//
     1           PhaseName(KPhase)(:idel(PhaseName(KPhase)))
      DelLabelSel=idel(LabelSel)
      LabelSel(DelLabelSel+2:)=' -> 0 selected'
      call FeQuestLblMake(id,tpom,il,LabelSel,'L','N')
      nLblLabelSel=LblLastMade
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=11
      ild=10
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      SbwRightClickAllowed=.true.
      nSbwSel=SbwLastMade
      il=il+2
      dpom=120.
      xpom=.5*xqd-1.5*dpom-20.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      Veta='Select re%jected'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRejected=ButtonLastMade
      xpom=xpom+dpom+20.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      if(NPhase.gt.1) then
        n=3
      else
        n=2
      endif
      xpom=.5*xqd-dpom*float(n)*.5-10.*float(n-1)
      if(NPhase.gt.1) then
        Veta='S%witch phase'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtPhase=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
      else
        nButtPhase=0
      endif
      Veta='Select a%dvanced'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdvanced=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,1)
      nEdwInclude=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Step #2: Select action by right mouse click or by this '//
     1     'button:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpomp=5.+FeTxLengthSpace(Veta(:9))
      xpom=tpom+FeTxLength(Veta)+10.
      Veta='A%ction'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAction=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Left mouse double click starts the Edit/Define action'
      call FeQuestLblMake(id,tpomp,il,Veta,'L','N')
      NAtActiveOld=0
      BackToAdvanced=.false.
      ItemSelOld=1
1200  call CloseIfOpened(SbwLnQuest(nSbwSel))
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NAtActive=0
      NAtRejected=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
        if(ai(i).eq.0.) NAtRejected=NAtRejected+1
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iak+iam(im)/KPoint(im)
          do ia=iap,iak
            write(Veta,'('' #$color'',i10)') Blue
            write(ln,FormA) Atom(ia)(:idel(Atom(ia)))//
     1                      Veta(:idel(Veta))
            NAtActive=NAtActive+1
            if(ai(ia).eq.0.) NAtRejected=NAtRejected+1
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      call CloseIfOpened(ln)
      if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0) then
        if(allocated(IAtActive))
     1    deallocate(IAtActive,LAtActive,NameAtActive)
        n=max(NAtActive,1)
        allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      endif
      j=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        j=j+1
        IAtActive(j)=i
        if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0)
     1    LAtActive(j)=.false.
        NameAtActive(j)=Atom(i)
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iak+iam(im)/KPoint(im)
          do ia=iap,iak
            j=j+1
            IAtActive(j)=ia
            if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0)
     1        LAtActive(j)=.false.
            NameAtActive(j)=Atom(ia)
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      if(NAtRejected.gt.0) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtRejected,i)
      go to 1255
1250  call CloseIfOpened(SbwLnQuest(nSbwSel))
1255  NAtActiveSel=0
      NAtomicSel=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          NAtActiveSel=NAtActiveSel+1
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=NAtomicSel+1
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSel,j)
      enddo
      write(Cislo,'(i10)') NSel
      call Zhusti(Cislo)
      LabelSel(DelLabelSel+2:)=' -> '//Cislo(:idel(Cislo))//' selected'
      call FeQuestLblChange(nLblLabelSel,LabelSel)
      NAtActiveOld=NAtActive
      ItemFromOld=SbwItemFromQuest(nSbwSel)
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_atoms.tmp')
      call FeQuestSbwShow(nSbwSel,ItemFromOld)
      call FeQuestSbwItemOff(nSbwSel,SbwItemPointerQuest(nSbwSel))
      call FeQuestSbwItemOn(nSbwSel,ItemSelOld)
1500  if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      call FeQuestEventWithCheck(id,ich,EM40AtomsUpdate,FeVoid)
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(LAtActive,NAtActive,lpom)
        ItemSelOld=SbwItemPointerQuest(nSbwSel)
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRejected)
     1  then
        do i=1,NAtActive
          LAtActive(i)=ai(IAtActive(i)).eq.0.
        enddo
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        idp=NextQuestId()
        xqdp=300.
        if(NMolMenu.gt.0) then
          il=9
          jk=3
        else
          il=6
          jk=2
          nRolMenuMol=0
          nButtInclMol=0
          nButtExclMol=0
        endif
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,jk
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          else if(j.eq.3) then
            Veta='Molecules'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                                NAtFormula(KPhase),0)
            else
              nRolMenuMol=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,MolMenu,NMolMenu,
     1                                0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              else
                nButtInclMol=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              else
                nButtExclMol=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,EM40SelAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=1,NAtActive
            if(EqWild(Atom(IAtActive(i)),Veta,.false.))
     1        LAtActive(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
           isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=1,NAtActive
            if(isf(IAtActive(i)).eq.isfp) LAtActive(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclMol.or.
     2           CheckNumber.eq.nButtExclMol)) then
          lpom=CheckNumber.eq.nButtInclMol
          kmolp=mxp*(RolMenuSelectedQuest(nRolMenuMol)-1)+1
          do i=1,NAtActive
            if(kmol(IAtActive(i)).eq.kmolp) LAtActive(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1250
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=1,NAtActive
          if(EqWild(Atom(IAtActive(i)),Veta,.false.))
     1      LAtActive(i)=.true.
        enddo
        call FeQuestStringEdwOpen(nEdwInclude,' ')
        go to 1250
      else if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtAction)
     1        .or.CheckType.eq.EventSbwRightClick.or.
     2            CheckType.eq.EventSbwDoubleClick) then
        if(NAtActiveSel.le.0) then
          call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
          i=SbwItemPointerQuest(nSbwSel)
          LAtActive(i)=.true.
          NAtActiveSel=1
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=1
          FromCursorPosition=.true.
        else
          FromCursorPosition=.false.
        endif
        if(NMolMenu.le.0.or.NAtomicSel.gt.0) then
          MenuCoDalEnable(IdMolToAt)=0
        else
          MenuCoDalEnable(IdMolToAt)=1
        endif
        if(NDimI(KPhase).le.0.and.MaxMagneticType.le.0) then
          MenuCoDalEnable(IdResetModMag)=0
        else
          MenuCoDalEnable(IdResetModMag)=1
        endif
        if(CheckType.eq.EventSbwDoubleClick) then
          CoDal=IdEditAtoms
        else
          CoDal=FeMenuNew(-1.,-1.,-1.,MenuCoDal,MenuCoDalEnable,1,
     1                    NMenuCoDal,1,0)
        endif
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPhase)
     1  then
        i=FeMenu(-1.,-1.,PhaseName,1,NPhase,1,0)
        if(i.gt.0.and.i.ne.KPhase) then
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
          KPhase=i
          call FeDeferOutput
          call FeQuestRemove(id)
          go to 1000
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(allocated(IAtActiveOld)) then
          if(NAtActiveOld.eq.NAtActive) then
            do i=1,NAtActive
              if(LAtActiveOld(i).neqv.LAtActive(i).or.
     1           IAtActiveOld(i).ne.  IAtActive(i)) go to 1810
            enddo
            go to 1820
          endif
        else
          if(NAtActiveSel.le.0) go to 1820
        endif
1810    Veta='Your selection will be lost. Do you really want to '//
     1       'leave the form?'
        if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) then
          call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1500
        endif
      endif
1820  call FeQuestRemove(id)
      go to 9999
2000  ItemSelOld=max(SbwItemPointerQuest(nSbwSel),1)
      idal=1100
      if(CoDal.eq.IdDeleteAtoms) then
        call EM40DeleteAtoms
        ItemSelOld=1
      else if(CoDal.eq.IdMergeAtoms) then
        call EM40MergeAtoms(ich)
      else if(CoDal.eq.IdTransAtoms) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40TransAtSelected(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdExpandAtoms) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40ExpandAtSelected(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdMolToAt) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40MolAtomsToAtomic
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdAddH) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40NewAtH(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdRename) then
        call EM40RenameAccordingToAtomType(ich)
      else if(CoDal.eq.IdRenameMan) then
        call EM40RenameManually(ich)
      else if(CoDal.eq.IdMotifs) then
        call EM40MakeMotifs
      else if(CoDal.eq.IdAtSplit) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40SplitAtoms
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdResetModMag) then
        call EM40ResetModMag
      else if(CoDal.eq.IdEditAtoms) then
        go to 2200
      else
        go to 1500
      endif
2050  if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      if(FromCursorPosition) then
        call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
        NAtActiveSel=0
        NAtomicSel=0
      else
        allocate(IAtActiveOld(NAtActive),LAtActiveOld(NAtActive),
     1           NameAtActiveOld(NAtActive))
        do i=1,NAtActive
          IAtActiveOld(i)=IAtActive(i)
          LAtActiveOld(i)=LAtActive(i)
          NameAtActiveOld(i)=NameAtActive(i)
        enddo
      endif
      if(idal.eq.1100) then
        call FeQuestRemove(id)
        go to 1100
      else if(idal.eq.1200) then
        go to 1200
      endif
2200  XdQuestEM40=600.
      if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
      if(NAtXYZMode.gt.0) call TrXYZMode(0)
      if(NAtMagMode.gt.0) call TrMagMode(0)
      call iom40(1,0,fln(:ifln)//'.l40')
      if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      if(NAtXYZMode.gt.0) call TrXYZMode(1)
      if(NAtMagMode.gt.0) call TrMagMode(1)
      il=12
      call FeKartCreate(-1.,-1.,XdQuestEM40,il,'Atom edit',0,
     1                  OKForBasicFiles)
      call FeCreateListek('Define',1)
      KartIdDefine=KartLastId
      call EM40AtomsDefineMake(KartIdDefine)
      call FeCreateListek('Edit',1)
      KartIdBasic=KartLastId
      call EM40AtomsEditMake(KartIdBasic)
      call FeCompleteKart(1)
3000  call FeQuestEventWithKartUpdate(KartId,ich,
     1                                EM40AtomsUpdateListek)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineUpdate
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineCheck
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditCheck
        endif
        KartIdOld=KartId
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineUpdate
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditUpdate
        endif
        call CrlAtomNamesApply
        call CrlAtomNamesIni
      else
        Veta=fln(:ifln)//'.l40'
        call iom40(0,0,Veta)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
        if(NAtXYZMode.gt.0) call TrXYZMode(1)
        if(NAtMagMode.gt.0) call TrMagMode(1)
        call DeleteFile(Veta)
      endif
      call FeDestroyKart
      idal=1200
      go to 2050
9999  if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
      KPhase=KPhaseIn
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      deallocate(MolMenu)
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call EM40SwitchMag(1)
      return
      end
