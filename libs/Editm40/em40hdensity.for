      function EM40HDensity()
      use Atoms_mod
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      dimension xp(3)
      EM40HDensity=0.
      do i=1,KeepNH(1)
        ia=KeepNAtH(1,i)
        call prevod(0,x(1,ia),xp)
        EM40HDensity=EM40HDensity+ExtMap(xp(1),xp(2),ActualMap,
     1                                   NActualMap)
      enddo
      return
      end
