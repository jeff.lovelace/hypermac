      subroutine EM40DefPGSyst(imol,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 Veta,ErrSt
      character*27 LocPGSystStO(2)
      character*2 LocPGSystAxO
      integer RolMenuSelectedQuest
      logical CrwLogicQuest
      dimension nRolMenuSystAx(2),nEdwSystSt(2),px(3)
      do j=1,2
        LocPGSystAxO(j:j)=LocPGSystAx(imol)(j:j)
        LocPGSystStO(j)=LocPGSystSt(j,imol)
      enddo
      id=NextQuestId()
      xdq=300.
      xdqp=xdq*.5
      il=2
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Define PG coordinate'//
     1                   ' system',0,LightGray,0,OKForBasicFiles)
      il=1
      Veta='%Use local system'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwLocSyst=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UsePGSyst(imol))
      xpom=xpom+CrwXd+15.
      dpom=20.+EdwYd
      xpp=xpom+dpom+15.
      dpp=xdq-xpp-5.
      do j=1,2
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,' ','L',dpom,EdwYd,1)
        nRolMenuSystAx(j)=RolMenuLastMade
        call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
        nEdwSystSt(j)=EdwLastMade
        il=il+1
      enddo
1400  if(UsePGSyst(imol)) then
        do i=1,2
          j=LocateInStringArray(SmbX,3,LocPGSystAx(imol)(i:i),.true.)
          call FeQuestRolMenuOpen(nRolMenuSystAx(i),SmbX,3,j)
          Veta=LocPGSystSt(i,imol)
          if(Veta(1:1).eq.' ') Veta=Veta(2:)
          k=0
          call StToReal(Veta,k,px,3,.false.,ich)
          if(ich.eq.0) then
            write(Veta,'(3f12.6)') px
            call ZdrcniCisla(Veta,3)
          endif
          call FeQuestStringEdwOpen(nEdwSystSt(i),Veta)
        enddo
      else
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuSystAx(i))
          call FeQuestEdwClose(nEdwSystSt(i))
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocSyst) then
        UsePGSyst(imol)=CrwLogicQuest(nCrwLocSyst)
        go to 1400
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuSystAx(1).or.
     2         CheckNumber.eq.nRolMenuSystAx(2))) then
        if(CheckNumber.eq.nRolMenuSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocPGSystAx(imol)(j:j)=Smbx(k)
        EventType=EventEdw
        EventNumber=nEdwSystSt(j)
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwSystSt(1).or.
     2         CheckNumber.eq.nEdwSystSt(2))) then
        Veta=EdwStringQuest(CheckNumber)
        call CrlGetXFromAtString(Veta,imol,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=CheckNumber
        else
          ich=0
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(UsePGSyst(imol)) then
          do i=1,2
            Veta=EdwStringQuest(nEdwSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocPGSystSt(i,imol)=Veta
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        do j=1,2
          LocPGSystAx(imol)(j:j)=LocPGSystAxO(j:j)
          LocPGSystSt(j,imol)=LocPGSystStO(j)
        enddo
      endif
9999  return
      end
