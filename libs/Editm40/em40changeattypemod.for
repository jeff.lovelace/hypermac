      subroutine EM40ChangeAtTypeMod(ia,TypeModFunNew,OrthoEpsNew)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      dimension xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1          FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),RSol(:),
     2          Y1Old(:),Y1New(:),kmodp(7),XPom(3),QPom(3)
      character*256 Veta
      integer OrthoSelNew(:),TypeModFunNew
      allocatable xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1            YOld,RMat,RSide,RSol,Y1Old,Y1New
      if(TypeModFunNew.eq.TypeModFun(ia).and.TypeModFunNew.ne.1)
     1   go to 9999
      MaxUsedKwP=0
      do j=1,7
        MaxUsedKwP=max(MaxUsedKwP,KModA(j,ia))
      enddo
      m=max(OrthoOrd,2*MaxUsedKwAll+1)
      allocate(xp(m),yp(m),OrthoSelNew(m),OrthoMatNew(m**2),
     1         OrthoMatNewI(m**2),FOld(m,NPoints),FNew(m,NPoints),
     2         YOld(NPoints),RMat(m*(m+1)),RSide(m),RSol(m),
     3         Y1Old(NPoints),Y1New(NPoints))
      X40=ax(1,ia)
      Delta=a0(ia)
      if(TypeModFunNew.eq.1) then
        if(OrthoOrd.le.0) OrthoOrd=m
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        call UpdateOrtho(ia,X40,Delta,OrthoEpsNew,OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,ia)

      endif
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
        n=2*NCommQProduct(iswa(ia),KPhase)
        dx4=1./float(n)
        NPointsUsed=n
      else
        NPointsUsed=NPoints
        x4=X40-Delta*.5
        dx4=Delta/float(NPointsUsed-1)
      endif
      nn=0
      x4p=x4
      XPom(1:3)=x(1:3,ia)
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        nn=nn+1
        if(TypeModFun(ia).eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FOld(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FOld(k,m)=sin(Pi2*km*x4)
            else
              FOld(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFun(ia).eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSel(k,ia)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMat(1,ia),yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FOld(1,m),2*MaxUsedKwAll+1,TypeModFun(ia))
!          call GetFPol(pom,FOld(1,m),2*MaxUsedKwP+1,TypeModFun(ia))
        endif
        if(TypeModFunNew.eq.0) then
          do k=1,2*MaxUsedKwAll+1
!          do k=1,2*MaxUsedKwP+1
            km=k/2
            if(k-1.eq.0) then
              FNew(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FNew(k,m)=sin(Pi2*km*x4)
            else
              FNew(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunNew.eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FNew(1,m),2*MaxUsedKwAll+1,TypeModFunNew)
        endif
        x4=x4p+float(m)*dx4
      enddo
      if(TypeModFun(ia).eq.1) then
        nn=min(OrthoOrd,nn)
      else
        nn=min(2*MaxUsedKwP+1,nn)
      endif
      RMat=0.
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
      else
        x4=X40-Delta*.5
      endif
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        l=0
        do i=1,nn
          do j=1,i
            l=l+1
            RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
          enddo
        enddo
        x4=x4p+float(m)*dx4
      enddo
      call smi(RMat,nn,ISing)
      do it=2,itf(ia)+1
        kmod=KModA(it,ia)
        if(kmod.le.0) cycle
        n=TRank(it-1)
        do i=1,n
          xp=0.
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                xp(k)=x(i,ia)
              else if(it.eq.3) then
                xp(k)=beta(i,ia)
              else if(it.eq.4) then
                xp(k)=c3(i,ia)
              else if(it.eq.5) then
                xp(k)=c4(i,ia)
              else if(it.eq.6) then
                xp(k)=c5(i,ia)
              else if(it.eq.7) then
                xp(k)=c6(i,ia)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                xp(k)=ux(i,km,ia)
              else if(it.eq.3) then
                xp(k)=bx(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3x(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4x(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5x(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6x(i,km,ia)
              endif
            else
              if(it.eq.2) then
                xp(k)=uy(i,km,ia)
              else if(it.eq.3) then
                xp(k)=by(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3y(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4y(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5y(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6y(i,km,ia)
              endif
            endif
          enddo
          RSide=0.
          if(KCommen(KPhase).gt.0) then
            x4=trez(1,1,KPhase)+qcnt(1,ia)
          else
            x4=X40-Delta*.5
          endif
          x4p=x4
          do m=1,NPointsUsed
            pom=x4-X40
            j=pom
            if(pom.lt.0.) j=j-1
            pom=pom-float(j)
            if(pom.gt..5) pom=pom-1.
            pom=2.*pom/Delta
            if(pom.lt.-1.01.or.pom.gt.1.01) then
              x4=x4p+float(m)*dx4
              cycle
            endif
            call MultM(FOld(1,m),xp,YOld(m),1,nn,1)
            if(it.eq.2.and.i.eq.1) Y1Old(m)=YOld(m)
            do j=1,nn
              RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
            enddo
            x4=x4p+float(m)*dx4
          enddo
          RSol=0.
          call nasob(RMat,RSide,RSol,nn)
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                x(i,ia)=RSol(k)
              else if(it.eq.3) then
                beta(i,ia)=RSol(k)
              else if(it.eq.4) then
                c3(i,ia)=RSol(k)
              else if(it.eq.5) then
                c4(i,ia)=RSol(k)
              else if(it.eq.6) then
                c5(i,ia)=RSol(k)
              else if(it.eq.7) then
                c6(i,ia)=RSol(k)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                ux(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                bx(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3x(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4x(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5x(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6x(i,km,ia)=RSol(k)
              endif
            else
              if(it.eq.2) then
                uy(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                by(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3y(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4y(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5y(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6y(i,km,ia)=RSol(k)
              endif
            endif
          enddo
        enddo
      enddo
      TypeModFun(ia)=TypeModFunNew
      if(TypeModFunNew.eq.1) then
        if(.not.allocated(OrthoSel))
     1    allocate(OrthoSel(OrthoOrd,NAtAll+1))
        m=OrthoOrd**2
        if(.not.allocated(OrthoMat))
     1     allocate(OrthoMat(m,NAtAll+1),OrthoMatI(m,NAtAll+1))
        call CopyVekI(OrthoSelNew,OrthoSel(1,ia),OrthoOrd)
        call CopyMat(OrthoMatNew ,OrthoMat (1,ia),OrthoOrd)
        call CopyMat(OrthoMatNewI,OrthoMatI(1,ia),OrthoOrd)
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        OrthoEps(ia)=OrthoEpsNew
        call trortho(0)
!        x(1:3,ia)=XPom(1:3)
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          kmodp=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
              kmodp(j)=max(kmodp(j),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        call trortho(1)
        if(Allocated(KWSym)) deallocate(KwSym)
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      else
        do i=1,3
          XPom(i)=x(i,ia)-XPom(i)
        enddo
        call qbyx(XPom,QPom,iswa(ia))
        ax(1,ia)=ax(1,ia)+QPom(1)
        X40=ax(1,ia)
      endif
      call SetRealArrayTo(xp,3,0.)
      xp(1)=X40
      call SpecPos(x(1,ia),xp,1,iswa(ia),.2,nocc)
      Veta=fln(:ifln)//'_pom.tmp'
      KPhaseIn=KPhase
      nvai=0
      neq=0
      neqs=0
      LstOpened=.true.
      uloha='Symmetry restrictions'
      call OpenFile(lst,Veta,'formatted','unknown')
      call NewPg(1)
      call MagParToCell(0)
      if(kmol(ia).gt.0) then
        im=kmol(ia)
        ji=(im-1)/mxp+1
        nap=NAtPosFrAll(KPhase)
        do i=1,im-1
          nap=nap+iam(i)*mam(i)
        enddo
      else
        im=0
        ji=0
        nap=0
      endif
      call atspec(ia,ia,ji,im,nap)
      call RefAppEq(0,0,0)
      call MagParToBohrMag(0)
      call CloseListing
      LstOpened=.false.
      call DeleteFile(Veta)
      deallocate(xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1           YOld,RMat,RSide,RSol,Y1Old,Y1New)
9999  return
      end
