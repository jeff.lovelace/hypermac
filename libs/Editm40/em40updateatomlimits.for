      subroutine EM40UpdateAtomLimits
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      NAtInd=0
      NAtPosPlus=NAtCalc-NAtCalcBasic
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtIndFr(i,KPh)=1
          else
            NAtIndFr(i,KPh)=NAtInd+1
          endif
          NAtIndTo(i,KPh)=NAtIndFr(i,KPh)+NAtIndLen(i,KPh)-1
          NAtInd=NAtIndTo(i,KPh)
        enddo
      enddo
      NAtPos=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtPosFr(i,KPh)=NAtInd+1
          else if(i.eq.1) then
            NAtPosFr(i,KPh)=NAtPosFr(i,KPh-1)+NAtPosLen(i,KPh-1)
          else
            NAtPosFr(i,KPh)=NAtPosFr(i-1,KPh)+NAtPosLen(i-1,KPh)
          endif
          NAtPosTo(i,KPh)=NAtPosFr(i,KPh)+NAtPosLen(i,KPh)-1
          NAtPos=NAtPos+NAtPosLen(i,KPh)
        enddo
      enddo
      NAtMol=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtMolFr(i,KPh)=NAtInd+NAtPos+1
          else if(i.eq.1) then
            NAtMolFr(i,KPh)=NAtMolFr(i,KPh-1)+NAtMolLen(i,KPh-1)
          else
            NAtMolFr(i,KPh)=NAtMolFr(i-1,KPh)+NAtMolLen(i-1,KPh)
          endif
          NAtMolTo(i,KPh)=NAtMolFr(i,KPh)+NAtMolLen(i,KPh)-1
          NAtMol=NAtMol+NAtMolLen(i,KPh)
        enddo
      enddo
      do KPh=1,NPhase
        NAtIndFrAll(KPh)=NAtIndFr(1,KPh)
        NAtPosFrAll(KPh)=NAtPosFr(1,KPh)
        NAtMolFrAll(KPh)=NAtMolFr(1,KPh)
        NAtIndLenAll(KPh)=0
        NAtPosLenAll(KPh)=0
        NAtMolLenAll(KPh)=0
        do i=1,NComp(KPh)
          NAtIndLenAll(KPh)=NAtIndLenAll(KPh)+NAtIndLen(i,KPh)
          NAtPosLenAll(KPh)=NAtPosLenAll(KPh)+NAtPosLen(i,KPh)
          NAtMolLenAll(KPh)=NAtMolLenAll(KPh)+NAtMolLen(i,KPh)
        enddo
        NAtIndToAll(KPh)=NAtIndFr(1,KPh)+NAtIndLenAll(KPh)-1
        NAtPosToAll(KPh)=NAtPosFr(1,KPh)+NAtPosLenAll(KPh)-1
        NAtMolToAll(KPh)=NAtMolFr(1,KPh)+NAtMolLenAll(KPh)-1
      enddo
      NAtCalc=NAtInd+NAtPos+NAtPosPlus
      NAtCalcBasic=NAtInd+NAtPos
      NAtAll=NAtInd+NAtPos+NAtMol
      return
      end
