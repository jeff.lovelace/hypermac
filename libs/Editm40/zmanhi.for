      subroutine ZmAnhi(i,itfn)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do k=itfn+2,itf(i)+1
        KFA(k,i)=0
        KModA(k,i)=0
      enddo
      call ReallocateAtomParams(NAtAll,itfn,IFrMax,KModAMax,MagParMax)
      call ShiftKiAt(i,itfn,ifr(i),lasmax(i),KModA(1,i),MagPar(i),
     1               .false.)
      m=TRankCumul(itf(i))
      do k=itf(i)+1,itfn
        if(k.eq.2) then
          pom=beta(1,i)
          spom=sbeta(1,i)
        endif
        nrank=TRank(k)
        do j=1,nrank
          if(k.eq.2) then
            beta(j,i)=pom*prcp(j,iswa(i),KPhase)
            sbeta(j,i)=spom*prcp(j,iswa(i),KPhase)
          else if(k.eq.3) then
            c3(j,i)=0.
            sc3(j,i)=0.
          else if(k.eq.4) then
            c4(j,i)=0.
            sc4(j,i)=0.
          else if(k.eq.5) then
            c5(j,i)=0.
            sc5(j,i)=0.
          else
            c6(j,i)=0.
            sc6(j,i)=0.
          endif
          m=m+1
          KiA(m,i)=1
        enddo
      enddo
      if(itf(i).gt.1.and.itfn.eq.1) call ZmTF21(i)
      itf(i)=itfn
      itfmax=max(itfmax,itfn)
      return
      end
