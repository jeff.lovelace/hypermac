      subroutine EM40DefWaves(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kwo(3,mxw),kw0(3),GammaIntP(9),GammaIntPI(9),kwp(3)
      character*80 Veta
      character*3 qc(3)
      character*2 nty
      integer GammaInt(:,:)
      logical Prvne,eqiv
      data qc/'*q1','*q2','*q3'/,kw0/3*0/
      allocatable GammaInt
      allocate(GammaInt(9,NSymm(KPhase)))
      Prvne=.true.
      call CopyVekI(kw(1,1,KPhase),kwo,3*mxw)
      xqd=120.+80.*NDimI(KPhase)
      n1=1
      n2=min(mxw,8)
      id=NextQuestId()
      ilm=n2-n1+3
      if(NDimI(KPhase).gt.1) ilm=ilm+1
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,'Modulation waves',0,
     1                   LightGray,0,0)
      if(mxw.gt.8) then
        il=ilm-1
        dpom=50.
        xpom=xqd-dpom-5.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
        nButtNext=ButtonLastMade
        il=1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
        nButtPrevious=ButtonLastMade
      endif
      if(NDimI(KPhase).gt.1) then
        il=ilm
        Veta='%Complete the set'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtComplete=ButtonLastMade
        call FeQuestButtonOpen(nButtComplete,ButtonOff)
        do is=1,NSymm(KPhase)
          do j=4,NDim(KPhase)
            do i=4,NDim(KPhase)
              GammaIntP(i-3+(j-4)*NDimI(KPhase))=
     1          rm6(i+(j-1)*NDim(KPhase),is,1,KPhase)
            enddo
          enddo
          call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
          do j=1,NDimI(KPhase)*NDimI(KPhase)
            GammaInt(j,is)=nint(GammaIntPI(j))
          enddo
        enddo
      endif
      il=1
      do i=1,8
        il=il+1
        call FeQuestLblMake(id,5.,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblNFirst=LblLastMade
      enddo
      il=1
      do i=1,NDimI(KPhase)
        il=il+1
        xpom=60.+EdwMarginSize
        do j=1,NDimI(KPhase)
          call FeQuestLblMake(id,xpom,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          if(i.eq.1.and.j.eq.1) nLblWFirst=LblLastMade
          xpom=xpom+80.
        enddo
      enddo
1500  n=n2-n1+1
      if(mxw.gt.8) then
        if(n2.lt.mxw) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(n1.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      endif
      j=n1
      iw=0
      nLblN=nLblNFirst
      nLblW=nLblWFirst
      il=1
      do i=1,n
        il=il+1
        write(Veta,100) j,nty(j)
        call FeQuestLblChange(nLblN,Veta)
        xpom=60.
        dpom=40.
        do k=1,NDimI(KPhase)
          iw=iw+1
          if(Prvne) call FeQuestEdwMake(id,5.,i,xpom,il,' ','L',dpom,
     1                                  EdwYd,0)
          if(j.gt.NDimI(KPhase)) then
            call FeQuestIntEdwOpen(iw,kw(k,j,KPhase),.false.)
          else
            write(Veta,'(i5)') kw(k,j,KPhase)
            call Zhusti(Veta)
            call FeQuestLblChange(nLblW,Veta)
            nLblW=nLblW+1
          endif
          if(Prvne) then
            Veta=qc(k)(:min(NDimI(KPhase)+1,3))
            if(k.lt.NDimI(KPhase)) Veta=Veta(:idel(Veta))//'+'
            call FeQuestLblMake(id,xpom+dpom+5.,il,Veta,'L','N')
          endif
          xpom=xpom+80.
        enddo
        j=j+1
        nLblN=nLblN+1
      enddo
      Prvne=.false.
2000  call FeQuestEvent(id,ich)
      iw=0
      j=n1
      do i=1,n
        do k=1,NDimI(KPhase)
          iw=iw+1
          if(j.gt.NDimI(KPhase)) then
            call FeQuestIntFromEdw(iw,kw(k,j,KPhase))
            call FeQuestEdwClose(iw)
          endif
        enddo
        j=j+1
      enddo
      if(CheckType.eq.EventButton.and.(CheckNumber.eq.nButtPrevious.or.
     1                                 CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          n1=n1+8
          n2=min(n2+8,mxw)
        else
          n1=max(n1-8,1)
          n2=n2-8
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtComplete)
     1  then
        do i=1,mxw
          if(eqiv(kw(1,i,KPhase),kw0,NDimI(KPhase))) go to 2210
        enddo
        i=mxw+1
2210    NwLast=i-1
        NwLastOld=NwLast
        i=0
2220    i=i+1
        if(i.gt.NwLast) go to 1500
        do 2300is=1,NSymm(KPhase)
          call multmi(kw(1,i,KPhase),GammaInt(1,is),kwp,1,NDimI(KPhase),
     1                NDimI(KPhase))
          do iz=1,2
            do j=1,NwLast
              if(eqiv(kwp,kw(1,j,KPhase),NDimI(KPhase))) go to 2300
            enddo
            call IntVectorToOpposite(kwp,kwp,NDimI(KPhase))
          enddo
          if(NwLast.lt.mxw) then
            NwLast=NwLast+1
            do i=1,NDimI(KPhase)
              if(kwp(i).lt.0) then
                call IntVectorToOpposite(kwp,kwp,NDimI(KPhase))
                go to 2260
              else if(kwp(i).gt.0) then
                go to 2260
              endif
            enddo
2260        call CopyVekI(kwp,kw(1,NwLast,KPhase),NDimI(KPhase))
          else
            write(Veta,FormI15) mxw
            call Zhusti(Veta)
            Veta='number of generated waves exceeds the limit of '//
     1           Veta(:idel(Veta))//' waves'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            call SetIntArrayTo(kw(1,NwLastOld+1,KPhase),
     1                         3*(mxw-NwLastOld),0)
            go to 1500
          endif
2300    continue
        go to 2220
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) call CopyVekI(kwo,kw(1,1,KPhase),3*mxw)
      deallocate(GammaInt)
      return
100   format(i2,a2,' wave')
      end
