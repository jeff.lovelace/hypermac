      subroutine EM40SwitchMag(Key)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
        if(kswa(i).ne.KPhase.or.MagPar(i).le.0) cycle
        if(KUsePolar(i).ne.0) cycle
        isw=iswa(i)
        do k=1,3
          if(Key.eq.0) then
            pom=CellPar(k,isw,KPhase)
          else
            pom=1./CellPar(k,isw,KPhase)
          endif
          sm0(k,i)=sm0(k,i)*pom
          do j=1,MagPar(i)-1
            smx(k,j,i)=smx(k,j,i)*pom
            smy(k,j,i)=smy(k,j,i)*pom
          enddo
        enddo
      enddo
      return
      end
