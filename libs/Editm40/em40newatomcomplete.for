      subroutine EM40NewAtomComplete(xi,uxi,uyi,kmodxi,isw,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest
      character*80 Veta
      character*12 menp(0:50)
      character*8  At,AtPom
      data menp(0)/'Atomic part'/,Biso/3./,isfp/1/,At/' '/
      dimension xi(*),uxi(3,*),uyi(3,*),xp(3),ip(1)
      integer RolMenuSelectedQuest
      logical Novy,FeYesNo,NUsed(:),WizardModeIn
      allocatable NUsed
      data AtPom/' '/
      WizardModeIn=WizardMode
      WizardMode=.false.
      NAtTest=1000
      allocate(NUsed(NAtTest))
      call EM40SetMolName
      i=idel(At)
      if(i.gt.0) then
        if(At(i:i).ne.'*') At=' '
      endif
      MolPart=0
      do i=1,MaxMolPos
        menp(i)=MolMenu(i)
      enddo
      xp(1)=0.
      call SpecPos(xi,xp,0,isw,.2,nocc)
      if(MaxMolPos.gt.0) then
        iw=6
      else
        iw=5
      endif
1100  id=NextQuestId()
      xqd=300.
      Veta='Complete information for the new atom'
      call FeQuestCreate(id,-1.,-1.,xqd,iw,Veta,1,LightGray,0,0)
      il=1
      tpom=5.
      xpom=150.
      dpom=xqd-xpom-60.
      Veta='%Name of the atom'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwAtName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,AtPom)
      Veta='%Info'
      dpomb=FeTxLengthUnder(Veta)+10.
      xpomb=xpom+dpom+15.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtInfo=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(lite(KPhase).eq.0) then
        Veta='%Uiso'
        Biso=Biso/episq
      else
        Veta='%Biso'
      endif
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwUiso=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Biso,.false.,.false.)
      if(Lite(KPhase).eq.0) Biso=Biso*episq
      il=il+1
      Veta='Site symmetry order:'
      write(Cislo,'(i3)') nocc
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      xpom=xpom+15.
      dpom=dpom-15.
      il=il+1
      Veta='Occupancy %reduction'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      Reduction=1.
      nEdwReduction=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Reduction,.false.,.false.)
      il=il+1
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,'Atomic %type','L',
     1                        dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                        NAtFormula(KPhase),isfp)
      if(MaxMolPos.gt.0) then
        il=il+1
        Veta='%Part of structure'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=xqd-xpom-10.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,
     1                          Veta,'L',dpom,EdwYd,1)
        nRolMenuPart=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,menp,MaxMolPos+1,
     1                          MolPart+1)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(AtPom.eq.' ') then
          call FeChybne(-1.,YBottomMessage,'Atom name cannot be an '//
     1                  'empty string.',' ',SeriousError)
          go to 1500
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInfo)
     1  then
        call FeFillTextInfo('em40newatomcomplete.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      else if(CheckType.eq.EventEdw) then
        if(CheckNumber.eq.nEdwAtName) then
          AtPom=EdwStringQuest(nEdwAtName)
          if(AtPom.eq.' ') go to 1500
          call zhusti(AtPom)
          At=AtPom
          idl=idel(AtPom)
          if(AtPom(idl:idl).eq.'*') then
            idl=idl-1
            if(idl.le.0) go to 1650
            call SetLogicalArrayTo(NUsed,NAtTest,.false.)
            do j=1,2
              if(j.eq.1) then
                i1=1
                i2=NAtInd
              else
                i1=NAtMolFr(1,1)
                i2=NAtAll
              endif
              do i=i1,i2
                Veta=Atom(i)
                if(LocateSubstring(Veta,AtPom(:idl),.false.,.true.)
     1             .gt.0) then
                  k=idl
                  call StToInt(Veta,k,ip,1,.false.,ichp)
                  if(ichp.ne.0) cycle
                  NUsed(ip(1))=.true.
                endif
              enddo
            enddo
            do i=1,NAtTest
              if(.not.NUsed(i)) then
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                At=AtPom(:idl)//Cislo(:idel(Cislo))
                go to 1650
              endif
            enddo
          endif
1650      call uprat(At)
          call AtCheck(At,ichp,j)
          if(ichp.eq.1) then
            call FeChybne(-1.,YBottomMessage,'Unacceptable symbol in '//
     1                    'the names string, try again.',' ',
     2                    SeriousError)
            go to 1500
          endif
          call FeQuestStringEdwOpen(nEdwAtName,At)
          n=0
          do j=1,NAtFormula(KPhase)
            k=idel(AtType(j,KPhase))
            if(k.eq.0) cycle
            k=LocateSubstring(At,AtType(j,KPhase)(:k),.false.,.true.)
            if(k.ne.1) then
              cycle
            else
              n=j
              if(idel(AtType(j,KPhase)).eq.2) go to 1730
            endif
          enddo
          if(n.eq.0) go to 1500
1730      isfp=n
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfp)
          go to 1500
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtType) then
        isfp=RolMenuSelectedQuest(nRolMenuAtType)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuPart) then
        MolPart=RolMenuSelectedQuest(nRolMenuPart)-1
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwUiso,Biso)
        if(Lite(KPhase).eq.0) Biso=Biso*episq
        call FeQuestRealFromEdw(nEdwReduction,reduction)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(MolPart.gt.0) then
        ji=-ktatmol(menp(MolPart))
        im=(ji-1)/mxp+1
      else
        ji=0
        im=0
      endif
      if(im.gt.0) then
        do j=1,3
          xi(j)=xi(j)-trans(j,ji)-xm(j,im)
        enddo
        call multm(RotiMol(1,ji),xi,xp,3,3,1)
        call AddVek(xp,xm(1,im),xi,3)
        do j=1,kmodxi
          call multm(RotiMol(1,ji),uxi(1,j),xp,3,3,1)
          call CopyVek(xp,uxi(1,j),3)
          call multm(RotiMol(1,ji),uyi(1,j),xp,3,3,1)
          call CopyVek(xp,uyi(1,j),3)
        enddo
      endif
      kam=ktatmol(at)
      if(kam.le.0) then
        if(im.eq.0) then
          if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
          kam=NAtIndTo(isw,KPhase)+1
          call AtSun(kam,NAtAll,kam+1)
          NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
          call EM40UpdateAtomLimits
        else
          n=KPoint(im)*mam(im)
          if(NAtAll+n.ge.MxAtAll) call ReallocateAtoms(100*(n+1))
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+n)
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+n
          NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+KPoint(im)
          call EM40UpdateAtomLimits
          kam=NAtMolFr(1,1)+1
          do i=1,im-1
            kam=kam+iam(i)/KPoint(i)
          enddo
          kam=kam+iam(im)/KPoint(im)-1
          call AtSun(kam,NAtAll-KPoint(im),kam+KPoint(im))
          iam(im)=iam(im)+KPoint(im)
        endif
        call SetBasicKeysForAtom(kam)
        if(kam.eq.1) then
          PrvniKiAtomu(kam)=ndoff
        else
          PrvniKiAtomu(kam)=PrvniKiAtomu(kam-1)+DelkaKiAtomu(kam-1)
        endif
        DelkaKiAtomu(kam)=0
        Novy=.true.
      else
        if(.not.FeYesNo(-1.,-1.,'Do you really want to '//
     1    'modify/replace the atom '//at(1:idel(at))//'?',1)) then
          go to 1100
        endif
        Novy=.false.
        ich=-1
      endif
      ai(kam)=Reduction/float(nocc)
      sai(kam)=0.
      call CopyVek(xi(1),x(1,kam),3)
      isf(kam)=isfp
      do j=1,kmodxi
        call CopyVek(uxi(1,j),ux(1,j,kam),3)
        call CopyVek(uyi(1,j),uy(1,j,kam),3)
        call SetRealArrayTo(sux(1,j,kam),3,0.)
        call SetRealArrayTo(suy(1,j,kam),3,0.)
         phf(kam)=0.
        sphf(kam)=0.
      enddo
      if(Novy) then
        Atom(kam)=at
        iswa(kam)=isw
        kswa(kam)=KPhase
        kmol(kam)=ji
        KModA(2,kam)=kmodxi
        KFA(2,kam)=0
        beta(1,kam)=Biso
        call SetRealArrayTo( beta(2,kam),5,0.)
        call SetRealArrayTo(sbeta(1,kam),6,0.)
      else
        KModA(2,kam)=kmodxi
        KFA(2,kam)=0
      endif
      call ShiftKiAt(kam,itf(kam),ifr(kam),lasmax(kam),KModA(1,kam),
     1               MagPar(kam),.false.)
      call SetIntArrayTo(KiA(1,kam),DelkaKiAtomu(kam),0)
      if(itf(kam).eq.2.and.kmodxi.gt.0) then
        do j=1,3
          xp(j)=sqrt(ux(j,1,kam)**2+uy(j,1,kam)**2)*pi
        enddo
        do j=1,6
          call indext(j,k,l)
          beta(j,kam)=beta(j,kam)-xp(k)*xp(l)
        enddo
      endif
      if(NMolec.gt.0) then
        NAtCalc=NAtInd
        call SetMol(0,0)
      endif
9999  deallocate(NUsed)
      deallocate(MolMenu)
      WizardMode=WizardModeIn
      return
      end
