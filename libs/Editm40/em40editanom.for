      subroutine EM40EditAnom(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(1),kip(1)
      character*80 Veta
      character*12 at
      integer RolMenuSelectedQuest
      logical lpom,Zpet
      KDatBlockIn=KDatBlock
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(kanref(KDatBlock).eq.0)
     1  call SetIntArrayTo(KiS(MxSc+19,KDatBlock),2*MaxNAtFormula,0)
1040  xqd=500.
      il=(NAtFormula(KPhase)-1)/2+4
      if(NDatBlock.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,
     1                   OKForBasicFiles)
      Zpet=.false.
      Veta='%Allow f'',f" refinement'
      xpom=(xqd+FeTxLength(Veta))*.5+10.
      il=1
      call FeQuestCrwMake(id,xqd*.5,il,xpom,il,Veta,'C',CrwXd,CrwYd,1,0)
      nCrwAllow=CrwLastMade
      call FeQuestCrwOpen(nCrwAllow,kanref(KDatBlock).ne.0)
      il=il+1
      ip=MxSc+18+(KPhase-1)*2*MaxNAtFormula
      do i=1,NAtFormula(KPhase)
        ip=ip+1
        if(mod(i,2).eq.1) then
          xpom=25.
          il=il+1
        else
          xpom=xqd*.5+25.
        endif
        tpom=xpom-5.
        call FeQuestLblMake(id,xpom+30.,il-1,'f''','C','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblFirst=LblLastMade
        call FeMakeParEdwCrw(id,tpom,xpom,il,AtTypeMenu,nEdw,nCrw)
        if(KAnRef(KDatBlock).le.0) then
          pom=FFra(i,KPhase,KDatBlock)
        else
          pom=FFrRef(i,KPhase,KDatBlock)
        endif
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiS(ip,KDatBlock),
     1                       .false.)
        if(i.eq.1) then
          nCrwPrv=nCrw
          nEdwPrv=nEdw
        endif
        xpom=xpom+120.
        call FeQuestLblMake(id,xpom+30.,il-1,'f"','C','N')
        call FeQuestLblOff(LblLastMade)
        call FeMakeParEdwCrw(id,tpom,xpom,il,AtTypeMenu(i),nEdw,nCrw)
        if(KAnRef(KDatBlock).le.0) then
          pom=FFia(i,KPhase,KDatBlock)
        else
          pom=FFiRef(i,KPhase,KDatBlock)
        endif
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,
     1                       KiS(ip+MaxNAtFormula,KDatBlock),.false.)
      enddo
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        il=il+1
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
1200  nLbl=nLblFirst
      do i=1,2
        if(kanref(KDatBlock).eq.1) then
          call FeQuestLblOn(nLbl)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
        if(kanref(KDatBlock).eq.1) then
          call FeQuestLblOn(nLbl)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
      enddo
      nedw=nEdwPrv
      ncrw=nCrwPrv
      j=0
      ip=MxSc+18+(KPhase-1)*2*MaxNAtFormula
      do i=1,2*NAtFormula(KPhase)
        if(kanref(KDatBlock).eq.1) then
          if(mod(i,2).eq.0) then
            pom=FFiRef(j,KPhase,KDatBlock)
            k=k+MaxNAtFormula
          else
            j=j+1
            k=ip+j
            pom=FFrRef(j,KPhase,KDatBlock)
          endif
          call FeQuestRealEdwOpen(nedw,pom,.false.,.false.)
          call FeQuestCrwOpen(ncrw,KiS(k,KDatBlock).ne.0)
        else
          call FeQuestEdwClose(nedw)
          call FeQuestCrwClose(ncrw)
        endif
        nedw=nedw+1
        ncrw=ncrw+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAllow) then
        k=kanref(KDatBlock)
        n=0
        do KDatB=1,NDatBlock
          n=n+kanref(KDatB)
        enddo
        if(n.le.0) then
          if(.not.allocated(FFrRef))
     1      allocate( FFrRef(MaxNAtFormula,NPhase,NDatBlock),
     2                FFiRef(MaxNAtFormula,NPhase,NDatBlock),
     3               sFFrRef(MaxNAtFormula,NPhase,NDatBlock),
     4               sFFiRef(MaxNAtFormula,NPhase,NDatBlock))
        endif
        kanref(KDatBlock)=1-kanref(KDatBlock)
        if(kanref(KDatBlock).eq.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatBlock),FFra(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatBlock),FFia(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
          enddo
        else
          do KPh=1,NPhase
            call CopyVek(FFra(1,KPh,KDatBlock),FFrRef(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call CopyVek(FFia(1,KPh,KDatBlock),FFiRef(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call SetRealArrayTo(sFFrRef(1,KPh,KDatBlock),
     1                          NAtFormula(KPh),0)
            call SetRealArrayTo(sFFiRef(1,KPh,KDatBlock),
     1                          NAtFormula(KPh),0)
          enddo
        endif
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,2*NAtFormula(KPhase)
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        go to 1200
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelectedQuest(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nedw=nEdwPrv
        ncrw=nCrwPrv
        if(kanref(KDatBlock).eq.1) then
          do i=1,2*NAtFormula(KPhase)
            j=(i+1)/2
            k=mod(i-1,2)+1
            call FeUpdateParamAndKeys(nEdw,nCrw,xp,kip,1)
            if(k.eq.1) then
              FFrRef(j,KPhase,KDatBlock)=xp(1)
            else
              FFiRef(j,KPhase,KDatBlock)=xp(1)
            endif
            ip=MxSc+18+j+(k-1)*MaxNAtFormula+(KPhase-1)*2*MaxNAtFormula
            KiS(ip,KDatBlock)=kip(1)
            nedw=nedw+1
            ncrw=ncrw+1
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
