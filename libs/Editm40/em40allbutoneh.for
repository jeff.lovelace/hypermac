      subroutine EM40AllButOneH(xb,xc,xh,n,DistH,AngleH,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xb(3),xc(3,*),xh(3,*),trp(3,3),trpi(9)
      dimension u(3),ug(3),v(3),vg(3),w(3),wg(3)
      do i=1,3
        u(i)=xb(i)-xc(i,1)
      enddo
      do i=1,3
        v(i)=xc(i,2)-xc(i,1)
      enddo
1150  call RefMakeTrMat(u,v,3,1,trp,trpi)
      if(n.eq.3) then
        pom1=0.3333333
        pom2=0.9428090
        DAngle=120.
      else
        pom1=0.5
        pom2=0.8660254
        DAngle=180.
      endif
      w(3)= pom1*DistH
      pom=  pom2*DistH
      Angle=AngleH
      do i=1,n
        w(1)=pom*cos(Angle*ToRad)
        w(2)=pom*sin(Angle*ToRad)
        call CopyVek(xb,xh(1,i),3)
        call cultm(trpi,w,xh(1,i),3,3,1)
        Angle=Angle+DAngle
      enddo
      go to 9999
      entry EM40AddApicalH(xb,xc,n,xh,DistH,isw)
      call SetRealArrayTo(u,3,0.)
      do j=1,n
        do i=1,3
          v(i)=xb(i)-xc(i,j)
        enddo
        call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
        call vecnor(v,vg)
        call AddVek(u,v,u,3)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call vecnor(u,ug)
      do i=1,3
        xh(i,1)=xb(i)+u(i)*DistH
      enddo
      go to 9999
      entry EM40TetraAdd2H(xb,xc,xh,DistH,isw)
      do i=1,3
        u(i)=xc(i,1)-xb(i)
        v(i)=xc(i,2)-xb(i)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call vecnor(u,ug)
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call vecnor(v,vg)
      call VecMul(u,v,wg)
      call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
      call vecnor(w,wg)
      do i=1,3
        v(i)=-v(i)-u(i)
      enddo
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call vecnor(v,vg)
      pom1=-1./(3.*scalmul(u,vg))
      if(abs(pom1).gt..99) pom1=.577350
      pom2=sqrt(1.-pom1**2)
      do j=1,2
        do i=1,3
          xh(i,j)=xb(i)+(v(i)*pom1+w(i)*pom2)*DistH
        enddo
        call RealVectorToOpposite(w,w,3)
      enddo
9999  return
      end
