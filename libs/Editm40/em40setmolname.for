      subroutine EM40SetMolName
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      allocate(MolMenu(mxpm))
      MaxMolPos=0
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        do j=1,mam(i)
          MaxMolPos=MaxMolPos+1
          if(mam(i).gt.1) then
            write(MolMenu(MaxMolPos),'(a8,''#'',i2)') molname(i),j
            call zhusti(MolMenu(MaxMolPos))
          else
            MolMenu(MaxMolPos)=molname(i)
          endif
        enddo
      enddo
      return
      end
