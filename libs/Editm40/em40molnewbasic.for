      subroutine EM40MolNewBasic(im,imp,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                           kmodamn,nEdwModOcc,nCrwCrenel,
     2                           nCrwSawTooth,nCrwZigZag,nCrwHarm,
     3                           nEdwHarmLimit)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodamn(3)
      integer PrvKi,CrwStateQuest
      logical   CrwLogicQuest
      if(imp.gt.0) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        kip=PrvKi
        call FeUpdateParamAndKeys(nEdw,nCrw,aimol(imp),KiMol(kip,imp),1)
        nEdw=nEdw+1
        nCrw=nCrw+1
        kip=kip+1
        call FeUpdateParamAndKeys(nEdw,nCrw,Euler(1,imp),KiMol(kip,imp),
     1                            3)
        nEdw=nEdw+3
        nCrw=nCrw+3
        kip=kip+3
        call FeUpdateParamAndKeys(nEdw,nCrw,trans(1,imp),KiMol(kip,imp),
     1                            3)
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModOcc
          do i=1,3
            call FeQuestIntFromEdw(nEdw,kmodamn(i))
            nEdw=nEdw+1
          enddo
          kfso=KFM(1,imp)
          if(CrwStateQuest(nCrwCrenel).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwCrenel)) then
              kfsn=1
            else
              kfsn=0
            endif
            KFM(1,imp)=min(kfso,kfsn)
          else
            kfsn=kfso
          endif
          kfxo=KFM(2,imp)
          if(CrwStateQuest(nCrwSawTooth).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              kfxn=1
            else if(CrwLogicQuest(nCrwZigZag)) then
              kfxn=2
            else
              kfxn=0
            endif
            KFM(2,imp)=min(kfxo,kfxn)
          endif
          call MolModi(imp,im,kmodamn)
          nEdw=nEdwPrvPar
          call FeQuestRealEdwOpen(nEdwPrvPar,aimol(imp),.false.,.false.)
          KFM(1,imp)=kfsn
          KFM(2,imp)=kfxn
          if(ktls(im).gt.0) then
            kip=kip+24
          else
            kip=kip+3
          endif
          k=KModM(1,imp)-NDimI(KPhase)
          if(k.ge.0) then
            if(kfsn.ne.kfso.and.kfsn.ne.0) then
              if(NDimI(KPhase).eq.1) then
                pom=0.
              else
                if(a0m(imp).gt.0.) then
                  pom=a0m(imp)**(1./float(NDimI(KPhase)))
                else
                  pom=0.
                endif
              endif
              do i=1,NDimI(KPhase)
                k=k+1
                axm(k,imp)=0.
                aym(k,imp)=pom
                j=kip+(k-1)*2+1
                KiMol(kip,imp)=0
                KiMol(j  ,imp)=0
                KiMol(j+1,imp)=0
              enddo
            endif
          endif
          if(k.gt.0) kip=kip+2*k+1
          i=KModM(2,imp)
          if(i.gt.0) then
            if(kfxn.ne.kfxo.and.kfxn.eq.1) then
              j=kip+(i-1)*6
              call SetRealArrayTo(utx(1,i,imp),3,.001)
              call SetRealArrayTo(uty(1,i,imp),3,0.)
              uty(2,i,imp)=1.
              call SetIntArrayTo(KiMol(j,imp),6,0)
              j=j+i*6
              call SetRealArrayTo(urx(1,i,imp),3,0.)
              call SetRealArrayTo(ury(1,i,imp),3,0.)
              call SetIntArrayTo(KiMol(j,imp),6,0)
            endif
          endif
        endif
      endif
      return
      end
