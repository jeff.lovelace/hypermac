      subroutine EM40MakeMotifs
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension Motif(:),xp(6),xopt(3),kwp(3),kwz(3),GammaIntP(9),
     1          GammaIntPI(9),xpp(6)
      integer GammaInt(9)
      logical PointAlreadyPresent,eqiv
      data dmez/3./
      allocatable Motif
      iauto=0
      go to 2000
      entry EM40MakeMotifsAuto
      iauto=1
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      allocate(IAtActive(NAtInd),LAtActive(NAtInd),NameAtActive(NAtInd))
1500  NAtActive=0
      do i=1,NAtInd
        if(kswa(i).eq.KPhase) then
          NAtActive=NAtActive+1
          IAtActive(NAtActive)=i
          LAtActive(NAtActive)=.true.
        endif
      enddo
2000  NTrans=1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      if(allocated(Motif)) deallocate(Motif)
      allocate(Motif(NAtInd))
      call SetIntArrayTo(Motif,NAtInd,0)
      MotifActual=0
3010  MotifActual=MotifActual+1
      nold=0
      n=0
      nuz=0
3050  do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          isfi=isf(ia)
          call SetRealArrayTo(xpp(4),3,0.)
          call CopyVek(x(1,ia),xpp,3)
          if(Motif(ia).le.0) then
            if(n.eq.0) then
              n=n+1
              Motif(ia)=MotifActual
              isw=iswa(ia)
            else
              if(iswa(ia).ne.isw) cycle
              dopt=1.5
              isymopt=1
              icentopt=1
              do j=1,NAtActive
                if(LAtActive(j)) then
                  ja=IAtActive(j)
                  if(iswa(ja).ne.isw) cycle
                  isfj=isf(ja)
                  if(Motif(ja).eq.MotifActual) then
                    if(PointAlreadyPresent(xpp,xp,x(1,ja),1,dmez,.false.
     1                                    ,dist,isym,icent,isw)) then
                      if(TypicalDist(isfi,isfj,KPhase).gt.0.) then
                        pom=dist/TypicalDist(isfi,isfj,KPhase)
                      else
                        pom=dist/(AtRadius(isfi,KPhase)+
     1                            AtRadius(isfj,KPhase))
                      endif
                      if(pom.lt.dopt.and.pom.gt.0.) then
                        dopt=pom
                        isymopt=isym
                        icentopt=icent
                        call CopyVek(xp,xopt,3)
                      endif
                    endif
                  endif
                endif
              enddo
              if(dopt.lt.1.1) then
                n=n+1
                if(isymopt.gt.1) then
                  call CopyVek(s6(1,isymopt,isw,KPhase),
     1                         TransVec(1,1,isw),NDim(KPhase))
                  call AddVek(vt6(1,icentopt,isw,KPhase),
     1                        TransVec(1,1,isw),TransVec(1,1,isw),
     2                        NDim(KPhase))
                  call CopyMat(rm6(1,isymopt,isw,KPhase),
     1                         TransMat(1,1,isw),NDim(KPhase))
                  call EM40SetTr(isw)
                  do j=4,NDim(KPhase)
                    do k=4,NDim(KPhase)
                      GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1                        TransMat(k+(j-1)*NDim(KPhase),1,isw)
                    enddo
                  enddo
                  call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
                  do j=1,NDimI(KPhase)*NDimI(KPhase)
                    GammaInt(j)=nint(GammaIntPI(j))
                  enddo
                  kmodmx=MaxUsedKw(KPhase)
c                  do k=1,7
c                    kmodmx=max(kmodmx,KModA(k,ia))
c                  enddo
                  do 3250j=1,kmodmx
                    call multmi(kw(1,j,KPhase),GammaInt,kwp,1,
     1                          NDimI(KPhase),NDimI(KPhase))
                    call IntVectorToOpposite(kwp,kwz,NDimI(KPhase))
                    do k=1,mxw
                      if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                        TransKwSym(j,1)= k
                        go to 3250
                      else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase)))
     1                  then
                        TransKwSym(j,1)=-k
                        go to 3250
                      endif
                    enddo
3250              continue
                  call EM40TrAt(ia,ia,isw,1)
                endif
                call CopyVek(xopt,x(1,ia),3)
                Motif(ia)=MotifActual
              endif
            endif
          endif
        endif
      enddo
      if(n.ne.nold) then
        nold=n
        go to 3050
      endif
      if(n.ne.0) go to 3010
      if(iauto.eq.1) call iom40(1,0,fln(:ifln)//'.m40')
      if(allocated(Motif)) deallocate(Motif)
      if(allocated(IAtActive).and.iauto.eq.1)
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(TransMat)) call DeallocateTrans
      return
      end
