      subroutine EM40EditParameters(ParType,ParBlock)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'main.cmn'
      include 'datred.cmn'
      character*256 InputM40,InputM50
      integer ParType,ParBlock
      logical FileDiff,FeYesNo,Diff,DiffM95,PwdCheckTOFInD
      call PwdSetTOF(KDatBlock)
      if(ParType.eq.IdParamExt.or.ParType.eq.IdParamAnom.or.
     1   ParType.eq.IdParamScale) call RefOpenCommands
      if(ParBlock.ne.IdParamAtomsNew) then
        InputM40='jm40'
        call CreateTmpFile(InputM40,i,0)
        call FeTmpFilesAdd(InputM40)
        InputM50='jm50'
        call CreateTmpFile(InputM50,i,0)
        call FeTmpFilesAdd(InputM50)
        call CopyFile(fln(:ifln)//'.m40',InputM40)
        call CopyFile(fln(:ifln)//'.m50',InputM50)
        if(ParentStructure) then
          call SSG2QMag(fln)
          call iom40Only(0,0,fln(:ifln)//'.m40')
        else
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
      endif
      if(Allocated(KWSym)) deallocate(KwSym)
      if(NDim(KPhase).gt.3) then
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      endif
      if(allocated(AtBrat)) deallocate(AtBrat,MolAtBrat,isfn,MolAt,
     1                                 MolAtisf)
      n=NAtAll
      allocate(AtBrat(n),MolAtBrat(n),isfn(n),MolAt(n),MolAtisf(n))
      ich=0
      ied=0
      DiffM95=.false.
      if(NAtXYZMode.gt.0) call TrXYZMode(1)
      if(NAtMagMode.gt.0) call TrMagMode(1)
      if(ParType.eq.IdParamOptions) then
        call EM40ParamOptions(ich)
      else if(ParType.eq.IdParamScale) then
        call EM40EditScales(ich)
      else if(ParType.eq.IdParamTwin) then
        call EM40EditTwVols(ich)
      else if(ParType.eq.IdParamExt) then
        call EM40Extinction(ich)
      else if(ParType.eq.IdParamAnom) then
        call EM40EditAnom(ich)
      else if(ParType.eq.IdParamPowder) then
        if(ExistM95.and..not.isTOF.and..not.isED)
     1    call iom95(1,fln(:ifln)//'.z95')
        call PwdOptions(ich)
      else if(ParType.eq.IdParamAtoms) then
        if(ParBlock.eq.IdParamAtomsNew) then
          call EM40NewAt(ich)
        else if(ParBlock.eq.IdParamAtomsEdit) then
          call EM40Atoms(ich)
        endif
      else if(ParType.eq.IdParamMolec) then
        if(ParBlock.eq.IdParamMolecNew) then
          call EM40NewMol(ich)
        else if(ParBlock.eq.IdParamMolecPosNew) then
          call EM40NewMolPos(ich)
        else if(ParBlock.eq.IdParamMolecTrans) then
          call EM40TransMol(ich)
        else if(ParBlock.eq.IdParamMolecExpand) then
          call EM40ExpandMol(ich)
        else if(ParBlock.eq.IdParamMolecEdit) then
          call EM40Molecules(ich)
        endif
      else if(ParType.eq.IdParamMagPolar) then
        call EM40DefMagPolar(ich)
      else if(ParType.eq.IdParamElDiff) then
        call EM40ElDiff(ich)
        ied=1
      endif
      if(ich.eq.0) then
        if(NAtXYZMode.gt.0) call TrXYZMode(0)
        if(NAtMagMode.gt.0) call TrMagMode(0)
        if(NMolec.gt.0.and.ISymmBasic.gt.0) then
          call CrlRestoreSymmetry(ISymmBasic)
          call CrlCleanSymmetry
        endif
        call iom40only(1,0,fln(:ifln)//'.m40')
!        if(ParentStructure) call QMag2SSG(Fln,0)
        call iom50(1,0,fln(:ifln)//'.m50')
        if(ExistPowder.and.ExistM41) call iom41(1,0,fln(:ifln)//'.m41')
        if(ParType.eq.IdParamExt.or.ParType.eq.IdParamAnom.or.
     1     ParType.eq.IdParamScale) then
          call RefRewriteCommands(1)
        else if(ParType.eq.IdParamPowder) then
          if(ExistM95.and..not.isTOF.and..not.isED) then
            LamAveRefBlock(KRefBlock)=LamAve(KDatBlock)
            LamA1RefBlock(KRefBlock)=LamA1(KDatBlock)
            LamA2RefBlock(KRefBlock)=LamA2(KDatBlock)
            NAlfaRefBlock(KRefBlock)=NAlfa(KDatBlock)
            LamRatRefBlock(KRefBlock)=LamRat(KDatBlock)
            PolarizationRefBlock(KRefBlock)=LpFactor(KDatBlock)
            AngleMonRefBlock(KRefBlock)=AngleMon(KDatBlock)
            AlphaGMonRefBlock(KRefBlock)=AlphaGMon(KDatBlock)
            BetaGMonRefBlock(KRefBlock)=BetaGMon(KDatBlock)
            FractPerfMonRefBlock(KRefBlock)=FractPerfMon(KDatBlock)
            RadiationRefBlock(KRefBlock)=Radiation(KDatBlock)
            if(ExistM95) then
              call iom95(1,fln(:ifln)//'.l95')
              DiffM95=FileDiff(fln(:ifln)//'.z95',fln(:ifln)//'.l95')
            else
              DiffM95=.false.
            endif
          endif
        else if(ParBlock.eq.IdParamAtomsNew) then
          go to 9999
        endif
      else
        if(ParBlock.eq.IdParamAtomsNew) go to 9000
      endif
      if(ied.eq.1) go to 9999
      call MoveFile(InputM40,PreviousM40)
      call MoveFile(InputM50,PreviousM50)
      call FeTmpFilesClear(InputM40)
      call FeTmpFilesClear(InputM50)
      Diff=FileDiff(fln(:ifln)//'.m40',PreviousM40).or.
     1     FileDiff(fln(:ifln)//'.m50',PreviousM50)
      if(.not.Diff.and.ExistPowder)
     1  Diff=Diff.or.FileDiff(fln(:ifln)//'.m41',PreviousM41)
      Diff=Diff.or.DiffM95
      if(ich.ne.0) then
        if(Diff) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to discard made '//
     1                    'changes?',0)) go to 9100
        endif
      else
        if(Diff) then
          if(FeYesNo(-1.,-1.,'Do you want to rewrite changed files?',
     1               1)) go to 9100
        endif
      endif
9000  call CopyFile(PreviousM40,fln(:ifln)//'.m40')
      call DeleteFile(PreviousM40)
      call CopyFile(PreviousM50,fln(:ifln)//'.m50')
      call DeleteFile(PreviousM50)
      if(ExistPowder) then
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call DeleteFile(PreviousM41)
      endif
      go to 9999
9100  if(ParType.eq.IdParamPowder.and.ExistM95.and.DiffM95) then
        call CompleteM95(0)
        call CompleteM90
        call iom90(0,fln(:ifln)//'.m90')
      endif
9999  if(allocated(AtBrat)) deallocate(AtBrat,MolAtBrat,isfn,MolAt,
     1                                 MolAtisf)
      call DeleteFile(fln(:ifln)//'.z95')
      call DeleteFile(fln(:ifln)//'.l95')
      return
      end
