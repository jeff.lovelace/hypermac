      subroutine EM40SetDefaultKeepH(n)
      use Refine_mod
      KeepNAtCentr(n)=0
      KeepAtCentr(n)=' '
      KeepKiXCentr(n)=0
      KeepNNeigh(n)=0
      do i=1,5
        KeepNAtNeigh(n,i)=0
        KeepKiXNeigh(n,i)=0
        KeepAtNeigh(n,i)=' '
      enddo
      KeepKiXAnchor(n)=0
      KeepNAtAnchor(n)=0
      KeepAtAnchor(n)=' '
      do i=1,4
        KeepKiXH(n,i)=0
        KeepNAtH(n,i)=0
        KeepAtH(n,i)=' '
      enddo
      KeepAngleH(n)=180.
      KeepADPExtFac(n)=1.2
      KeepDistH(n)=1.0
      KeepN(n)=0
      do i=1,NKeepAtMax
        KeepNAt(n,i)=0
        KeepKiX(n,i)=0
        KeepAt(n,i)=' '
      enddo
      return
      end
