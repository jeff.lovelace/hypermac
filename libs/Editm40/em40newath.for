      subroutine EM40NewAtH(ich)
      use EditM40_mod
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'editm40.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun
      character*256 EdwStringQuest,t256
      character*80 Veta,AtFill(5)
      character*8 at
      character*2 nty
      logical SaveKeepCommands,CrwLogicQuest,EqIgCase,FeYesNo,
     1        MapAlreadyUsed,Recalculate,TryAutomaticRun,PridavaVodiky,
     2        TryAutomaticRunIn,FeYesNoHeader
      integer AtomToBeFilled,EdwStateQuest,RolMenuSelectedQuest
      external RefKeepUpdateQuest,FeVoid
      real KeepDistHOld
      save /KeepQuest/
      KeepDistHDefault=-1.
      SaveKeepCommands=.true.
      ich=0
      BlowUpFactor=1.2
      MapAlreadyUsed=.false.
      isfhp=isfh(KPhase)
      if(isfhp.le.0) then
        pom=100000.
        do i=1,NAtFormula(KPhase)
          if(ffbasic(1,i,KPhase).lt.pom) then
            pom=AtNum(i,KPhase)
            isfhp=i
          endif
        enddo
      endif
      PridavaVodiky=EqIgCase(AtType(isfhp,KPhase),'H')
1030  id=NextQuestId()
      il=3
      if(PridavaVodiky) il=il+1
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Adding of "hydrogen" '//
     1                   'atoms',0,LightGray,0,0)
      il=1
      Veta='"%Hydrogen" atomic type'
      xpom=5.
      dpom=40.
      tpom=xpom+dpom+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuHType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                        NAtFormula(KPhase),isfhp)
      il=il+1
      tpom=xpom+20.
      Veta='%generate the "keep" commands for REFINE'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwSaveKeep=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SaveKeepCommands)
      if(PridavaVodiky) then
        il=il+1
        Veta='%try automatic run for non-"N" and non-"O" atoms'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwAutomatic=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SaveKeepCommands)
      else
        nCrwAutomatic=0
      endif
      il=il+1
      Veta='Default value for %ADP ext. factor'
      dpom=100.
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFactor=EdwLastMade
      call FeQuestRealEdwOpen(nEdwFactor,BlowUpFactor,.false.,
     1                        .false.)
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuHType)
     1  then
        isfhp=RolMenuSelectedQuest(nRolMenuHType)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        SaveKeepCommands=CrwLogicQuest(nCrwSaveKeep)
        if(nCrwAutomatic.le.0) then
          TryAutomaticRunIn=.false.
        else
          TryAutomaticRunIn=CrwLogicQuest(nCrwAutomatic)
        endif
        call FequestRealFromEdw(nEdwFactor,BlowUpFactor)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      call iom40(1,0,fln(:ifln)//'.m40')
      xdq=600.
      Veta='Lo%cate position in map'
      dpoma=FeTxLengthUnder(Veta)+10.
      id=NextQuestId()
      if(SaveKeepCommands) then
        IgnoreW=.true.
        call RefOpenCommands
        IgnoreW=.false.
      endif
      nEdwCentr=0
      napp=0
      iaw=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(.not.TryAutomaticRunIn.or.
     1       EqIgCase(AtType(isf(ia),KPhase),'N').or.
     2       EqIgCase(AtType(isf(ia),KPhase),'O')) iaw=iaw+1
          iaLast=ia
        endif
      enddo
      iaa=0
      if(TryAutomaticRunIn) then
        NRepeatFrom=0
        if(iaw.gt.0) then
          NRepeatTo=1
        else
          NRepeatTo=0
        endif
      else
        NRepeatFrom=1
        NRepeatTo=1
      endif
      if(NKeepMax.le.0) call ReallocKeepCommands(1,8)
      do IRepeat=NRepeatFrom,NRepeatTo
        TryAutomaticRun=TryAutomaticRunIn.and.IRepeat.eq.0
        do iat=1,NAtActive
          if(ISymmBasic.gt.0) call CrlRestoreSymmetry(ISymmBasic)
          call SpecAt
          if(.not.LAtActive(iat)) cycle
          ia=IAtActive(iat)
          if(EqIgCase(AtType(isf(ia),KPhase),'N').or.
     1       EqIgCase(AtType(isf(ia),KPhase),'O')) then
            if(TryAutomaticRun) cycle
          else
            if(TryAutomaticRunIn.and.IRepeat.ne.0) cycle
          endif
          KeepAngleH(1)=180.
          iaa=iaa+1
          isw=iswa(ia)
          if(kmol(ia).le.0) then
            im=0
            iap=1
            iak=NAtInd+NAtPos
            if(NMolec.gt.0) call CrlRestoreSymmetry(ISymmBasic)
          else
            im=(kmol(ia)-1)/mxp+1
            iap=NAtMolFr(1,1)
            do i=1,im-1
              iap=iap+iam(i)
            enddo
            iak=iap+iam(im)-1
            call CrlRestoreSymmetry(ISymmMolec(im))
          endif
          call EM40SetKeepForNewH(ia,iap,iak,isfhp)
          NNeigh=KeepNNeigh(1)
          UseAnchor=.false.
          if(TryAutomaticRun) then
            if(.not.EqIgCase(AtType(isf(ia),KPhase),'N').and.
     1         .not.EqIgCase(AtType(isf(ia),KPhase),'O')) then
              if(KeepNH(1).le.0.or.KeepAtNeigh(1,1).eq.' ') cycle
              NAtMax=KeepNH(1)+KeepNNeigh(1)
              call EM40NewAtomHMake(1,ia,im,isw,ich)
              NButtLocate=0
              NButtApply=1
              CheckNumber=1
              cycle
            endif
          endif
          il=8
          Veta='Adding "hydrogen" atoms for "'//
     1         atom(ia)(:idel(atom(ia)))//'"'
          call FeQuestCreate(id,-1.,-1.,xdq,il,Veta,0,LightGray,-1,-1)
          il=0
          xpom=5.
          tpom=xpom+10.+CrwgXd
          Veta='T%etrahedral'
          xpom1=tpom+FeTxLengthUnder(Veta)+66.
          nTypeHydro=3
          do i=1,nTypeHydro
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='T%rigonal'
              nCrwTetra=CrwLastMade
            else if(i.eq.2) then
              Veta='A%pical'
              nCrwTriangl=CrwLastMade
            else if(i.eq.3) then
              nCrwApical=CrwLastMade
            endif
          enddo
          il=1
          dpom=100.
          xpom=xpom1
          tpom=xpom+dpom+10.
          Veta='H di%st.'
          do i=1,2
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          1)
            if(i.eq.1) then
              nEdwHDist=EdwLastMade
              xpom=tpom+FeTxLengthUnder(Veta)+59.
              xpom=xpom1+dpoma+2.*(xdq*.5-xpom1-dpoma)
              Veta='%ADP ext. factor'
              tpom=xpom+dpom+10.
              xpom2=xpom
            else if(i.eq.2) then
              nEdwFactor=EdwLastMade
            endif
          enddo
          il=il+1
          Veta='%Neighbor(s)'
          xpom=xpom1
          dpom1=25.
          tpom=xpom+dpom1+20.
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom1,EdwYd,1)
          nEdwNNeigh=EdwLastMade
          ilh=il
          xpomh=xpom2+dpom*.5
          call FeQuestLblMake(id,xpomh,il,'Hydrogen(s)','C','N')
          xpom=xpom1
          ilp=il
          do j=1,2
            tpom=xpom+dpom+10.
            do i=1,5
              il=il+1
              write(Veta,'(i1,a2)') i,nty(i)
              call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,1)
              if(j.eq.1) then
                if(i.eq.1) nEdwNeighFirst=EdwLastMade
              else
                if(i.eq.1) nEdwHFirst=EdwLastMade
              endif
            enddo
            if(j.eq.1) then
              nEdwNeighLast=EdwLastMade
              il=ilp
              xpom=xpom2
            else
              nEdwHLast=EdwLastMade
            endif
          enddo
          il=il-1
          Veta='%Use anchoring    =>'
          xpom=5.
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwUseAnchor=CrwLastMade
          Veta='Anch%or'
          xpom=xpom1
          do i=1,2
            tpom=xpom+dpom+10.
            if(i.eq.1) then
              j=1
            else
              j=0
            endif
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          j)
            if(i.eq.1) then
              nEdwAnchor=EdwLastMade
              Veta='Tors.an%gle'
              xpom=xpom2
            else if(i.eq.2) then
              nEdwTorsAngle=EdwLastMade
            endif
          enddo
          il=il+1
          xpom=xpom1
          Veta='Lo%cate position in map'
          call FeQuestButtonMake(id,xpom,il,dpoma,ButYd,Veta)
          nButtLocate=ButtonLastMade
          xpom=xpom2
          Veta='Se%lect neighbors'
          call FeQuestButtonMake(id,xpom,il,dpoma,ButYd,Veta)
          nButtSelect=ButtonLastMade
          il=il+1
          Veta='Avoi%d'
          if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
          dpomb=FeTxLengthUnder('%Quit')+20.
          dpom=dpoma
          xpom=xpom1
          do i=1,3
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
            if(i.eq.1) then
              Veta='%Quit'
              nButtAvoid=ButtonLastMade
            else if(i.eq.2) then
              Veta='Appl%y'
              if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
              nButtQuit=ButtonLastMade
            else if(i.eq.3) then
              nButtApply=ButtonLastMade
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            if(i.eq.1) then
              dpom=dpomb
              xpom=xdq*.5-dpomb*.5
            else if(i.eq.2) then
              dpom=dpoma
              xpom=xpom2
            endif
          enddo
          call FeQuestMouseToButton(nButtApply)
2350      KeepTypeOld=0
2400      KeepTypeMain=KeepType(1)/10
          KeepTypeAdd=mod(KeepType(1)-1,10)+1
          KeepTypeMainOld=KeepTypeOld/10
          KeepTypeAddOld=mod(KeepTypeOld-1,10)+1
          if(KeepType(1).ne.KeepTypeOld) then
            nCrw=nCrwTetra
            do i=1,nTypeHydro
              call FeQuestCrwOpen(nCrw,i.eq.KeepTypeAdd)
              nCrw=nCrw+1
            enddo
          endif
          if(KeepType(1).eq.IdKeepHApical) then
            NAtMax=6
            KeepNH(1)=1
            UseAnchor=.false.
          else if(KeepType(1).eq.IdKeepHTetraHed) then
            NAtMax=4
            if(KeepTypeOld.eq.IdKeepHTriangl) then
!              NNeigh=NNeigh+1
            else if(KeepTypeOld.eq.IdKeepHApical) then
              NNeigh=min(NNeigh,NAtMax-1)
            endif
            KeepNH(1)=4-NNeigh
          else if(KeepType(1).eq.IdKeepHTriangl) then
            NAtMax=3
            if(KeepTypeOld.eq.IdKeepHTetraHed) then
              if(NNeigh.eq.1) then
                KeepNH(1)=KeepNH(1)-1
              else
                NNeigh=NNeigh-1
              endif
            else if(KeepTypeOld.eq.IdKeepHApical) then
              NNeigh=min(NNeigh,NAtMax-1)
            endif
            KeepNH(1)=3-NNeigh
          endif
          KeepNNeigh(1)=NNeigh
          if(UseAnchor) KeepNNeigh(1)=KeepNNeigh(1)+1
2500      if(KeepNH(1).ne.NAtMax-1) then
            call FeQuestCrwClose(nCrwUseAnchor)
            call FeQuestButtonClose(nButtLocate)
            UseAnchor=.false.
          else if(im.gt.0) then
            call FeQuestButtonDisable(nButtLocate)
          endif
          if(.not.UseAnchor) then
            call FeQuestEdwClose(nEdwAnchor)
            call FeQuestEdwClose(nEdwTorsAngle)
          endif
          call FeQuestIntEdwOpen(nEdwNNeigh,NNeigh,.false.)
          call FeQuestEudOpen(nEdwNNeigh,1,NAtMax-1,1,0.,0.,0.)
          call FeQuestRealEdwOpen(nEdwHDist,KeepDistH(1),.false.,
     1                            .false.)
          call FeQuestRealEdwOpen(nEdwFactor,BlowUpFactor,.false.,
     1                            .false.)
          do j=1,2
            if(j.eq.1) then
              nEdw=nEdwNeighFirst
              NMaxP=NNeigh
            else
              nEdw=nEdwHFirst
              NMaxP=KeepNH(1)
            endif
            do i=1,5
              if(i.le.NMaxP) then
                if(j.eq.1) then
                  Veta=KeepAtNeigh(1,i)
                else
                  Veta=KeepAtH(1,i)
                endif
                if(Veta.eq.' ') then
                  write(Cislo,FormI15) i
                  call Zhusti(Cislo)
                  Veta='H'//Cislo(:idel(Cislo))//
     1                          Atom(ia)(:idel(Atom(ia)))
                  KeepAtH(1,i)=Veta
                endif
                call FeQuestStringEdwOpen(nEdw,Veta)
              else
                call FeQuestEdwClose(nEdw)
              endif
              nEdw=nEdw+1
            enddo
          enddo
          if(KeepNH(1).eq.NAtMax-1) then
            UseAnchor=NAtMax.lt.KeepNNeigh(1)+KeepNH(1)
            call FeQuestCrwOpen(nCrwUseAnchor,UseAnchor)
            if(im.eq.0) then
              call FeQuestButtonOpen(nButtLocate,ButtonOff)
              if(.not.ExistM90) call FeQuestButtonDisable(nButtLocate)
            endif
          endif
          if(UseAnchor) then
            call FeQuestStringEdwOpen(nEdwAnchor,KeepAtAnchor(1))
            call FeQuestRealEdwOpen(nEdwTorsAngle,KeepAngleH(1),.false.,
     1                              .false.)
          endif
          call FeQuestButtonOpen(nButtSelect,ButtonOff)
          KeepTypeOld=KeepType(1)
          KeepNHOld=KeepNH(1)
          KeepNNeighOld=KeepNNeigh(1)
3000      MakeExternalCheck=1
          call FeQuestEventWithCheck(id,ich,RefKeepUpdateQuest,
     1                               FeVoid)
          if(CheckType.eq.EventCrw) then
            if(CheckNumber.eq.nCrwTetra) then
              KeepType(1)=IdKeepHTetraHed
            else if(CheckNumber.eq.nCrwTriangl) then
              KeepType(1)=IdKeepHTriangl
            else if(CheckNumber.eq.nCrwApical) then
              KeepType(1)=IdKeepHApical
            else if(CheckNumber.eq.nCrwUseAnchor) then
              UseAnchor=CrwLogicQuest(nCrwUseAnchor)
              if(UseAnchor) then
                KeepNNeigh(1)=NNeigh+1
              else
                KeepNNeigh(1)=NNeigh
              endif
              EventType=EventEdw
              EventNumber=nEdwAnchor
              go to 2500
            endif
            go to 2400
          else if(CheckType.eq.EventEdw) then
            if(CheckNumber.eq.nEdwNNeigh) then
              call FeQuestIntFromEdw(nEdwNNeigh,NNeigh)
              if(KeepType(1).eq.IdKeepHTetraHed.or.
     1           KeepType(1).eq.IdKeepHTriangl) then
                KeepNH(1)=NAtMax-NNeigh
                KeepNNeigh(1)=NNeigh
                if(UseAnchor.and.KeepNNeigh(1).eq.1)
     1            KeepNNeigh(1)=KeepNNeigh(1)+1
              else if(KeepType(1).eq.IdKeepHApical) then
                KeepNNeigh(1)=NNeigh
              endif
              if(KeepNH(1).ne.KeepNHOld.or.
     1           KeepNNeigh(1).ne.KeepNNeighOld) then
                EventType=EventEdw
                EventNumber=nEdwNNeigh
                go to 2500
              endif
            else if(CheckNumber.ge.nEdwNeighFirst.and.
     1              CheckNumber.le.nEdwNeighLast) then
              i=CheckNumber-nEdwNeighFirst+1
              KeepAtNeigh(1,i)=EdwStringQuest(CheckNumber)
              KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
            else if(CheckNumber.ge.nEdwHFirst.and.
     1              CheckNumber.le.nEdwHLast) then
              i=CheckNumber-nEdwHFirst+1
              KeepAtH(1,i)=EdwStringQuest(CheckNumber)
              KeepNAtH(1,i)=ktatmol(KeepAtH(1,i))
            else if(CheckNumber.eq.nEdwAnchor) then
              KeepAtAnchor(1)=EdwStringQuest(CheckNumber)
              KeepNAtAnchor(1)=ktatmol(KeepAtAnchor(1))
            else if(CheckNumber.eq.nEdwHDist) then
              call FequestRealFromEdw(nEdwHDist,KeepDistH(1))
              if(EqIgCase(AtType(isf(ia),KPhase),'C')) then
                KeepDistHDefault(1)=KeepDistH(1)
              else if(EqIgCase(AtType(isf(ia),KPhase),'N')) then
                KeepDistHDefault(2)=KeepDistH(1)
              else if(EqIgCase(AtType(isf(ia),KPhase),'O')) then
                KeepDistHDefault(3)=KeepDistH(1)
              endif
            else if(CheckNumber.eq.nEdwFactor) then
              call FequestRealFromEdw(nEdwFactor,BlowUpFactor)
            endif
            go to 3000
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtSelect) then
            EventType=EventEdw
            if(AtomToBeFilled.eq.SelectedAnchor) then
              EventNumber=nEdwAnchor
              Veta=EdwStringQuest(nEdwNeighFirst)
              if(Veta.ne.' ') then
                call SelNeighborAtoms('Select the anchor atom',3.,Veta,
     1                                AtFill,1,n,isw,ich)
                if(ich.ne.0) go to 3000
              else
                call FeChybne(-1.,-1.,'first you have to define the '//
     1                       'neighbor atom.',' ',SeriousError)
                EventNumber=nEdwNeighFirst
                go to 3000
              endif
              KeepAtAnchor(1)=AtFill(1)
            else
              nsp=NSymm(KPhase)
              if(AtomToBeFilled.eq.SelectedNeigh) then
                EventNumber=nEdwNeighFirst
                nmax=NNeigh
                Veta='Select the neighbor atoms'
              else if(AtomToBeFilled.eq.SelectedHydro) then
                EventNumber=nEdwHFirst
                nmax=KeepNH(1)
                Veta='Select the hydrogen atom'
                if(KeepNH(1).gt.1) Veta=Veta(:idel(Veta))//'s'
                NSymm(KPhase)=1
              endif
              call SelNeighborAtoms(Veta,3.,KeepAtCentr(1),AtFill,nmax,
     1                              n,isw,ich)
              if(ich.ne.0) go to 3000
              NSymm(KPhase)=nsp
              if(AtomToBeFilled.eq.SelectedNeigh) then
                do i=1,NNeigh
                  if(i.le.n) then
                    KeepAtNeigh(1,i)=AtFill(i)
                    KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
                  else
                    KeepAtNeigh(1,i)=' '
                  endif
                enddo
              else if(AtomToBeFilled.eq.SelectedHydro) then
                do i=1,KeepNH(1)
                  j=index(AtFill(i),'#')
                  if(j.gt.0) AtFill(i)(j:)=' '
                  if(i.le.n) then
                    KeepAtH(1,i)=AtFill(i)
                  else
                    KeepAtH(1,i)=' '
                  endif
                enddo
              endif
            endif
            go to 2500
          else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1      then
            Veta=' '
            if(napp.gt.0) then
              write(Veta,'(i5)') napp
              call zhusti(Veta)
              Veta='Do you want to discard '//Veta(:idel(Veta))//
     1             ' new hydrogen atom'
              if(napp.gt.1) Veta=Veta(:idel(Veta))//'s?'
              if(FeYesNo(-1.,-1.,Veta,0)) then
                ich=1
              else
                ich=-1
              endif
            else
              Veta='Do you really want quit the procedure for this'
              if(iaa.lt.iaw) then
                write(Cislo,'(i5)') iaw-iaa+1
                call zhusti(Cislo)
                Veta=Veta(:idel(Veta))//' and remaining '//
     1               Cislo(:idel(Cislo))//' atom'
                if(iaw-iaa+1.gt.1) Veta=Veta(:idel(Veta))//'s?'
              else
                Veta=Veta(:idel(Veta))//' atom?'
              endif
              if(.not.FeYesNo(-1.,-1.,Veta,0)) then
                go to 3000
              else
                ich=1
              endif
            endif
            go to 3900
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtAvoid) then
            ich=0
            go to 3900
          else if(CheckType.eq.EventButton.and.
     1            (CheckNumber.eq.nButtApply.or.
     2             CheckNumber.eq.nButtLocate)) then
            if(KeepNH(1).le.0) then
              ich=0
              go to 3900
            endif
            nEdw=nEdwNeighFirst
            do i=1,NNeigh
              Veta=EdwStringQuest(nEdw)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the neighbor atom'
                go to 3820
              endif
              j=index(Veta,'#')-1
              if(j.lt.0) j=idel(Veta)
              at=Veta(:j)
              call AtCheck(at,ichp,j)
              if(ichp.eq.1) go to 3810
              j=ktat(Atom(iap),iak-iap+1,at)
              if(j.le.0) go to 3800
              KeepAtNeigh(1,i)=Veta
              KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
              nEdw=nEdw+1
            enddo
            nEdw=nEdwHFirst
            do i=1,KeepNH(1)
              Veta=EdwStringQuest(nEdw)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the hydrogen atom'
                go to 3820
              endif
              call AtCheck(Veta,ichp,j)
              if(ichp.eq.1) go to 3810
              j=index(Veta,'#')
              if(j.gt.0) Veta=Veta(:j-1)
              KeepAtH(1,i)=Veta
              nEdw=nEdw+1
            enddo
            if(UseAnchor) then
              Veta=EdwStringQuest(nEdwAnchor)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the anchor atom'
                go to 3820
              endif
              j=index(Veta,'#')-1
              if(j.lt.0) j=idel(Veta)
              at=Veta(:j)
              call AtCheck(at,ichp,j)
              if(ichp.eq.1) go to 3810
              j=ktat(atom(iap),iak-iap+1,at)
              if(j.le.0) go to 3800
              KeepAtAnchor(1)=Veta
            else
               KeepAtAnchor(1)='#unknown#'
            endif
            if(CheckNumber.eq.nButtLocate) then
              if(SaveKeepCommands) call RefRewriteCommands(1)
              call iom40Only(1,0,fln(:ifln)//'.m40')
            endif
            call FequestRealFromEdw(nEdwHDist,KeepDistH(1))
            if(EdwStateQuest(nEdwTorsAngle).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdwTorsAngle,KeepAngleH(1))
            endif
            call FeQuestRealFromEdw(nEdwFactor,BlowUpFactor)
            call EM40NewAtomHMake(0,ia,im,isw,ich)
            if(CheckNumber.eq.nButtLocate) then
              go to 3000
            else
              go to 3900
            endif
3800        Veta='atom "'//Veta(:idel(Veta))//'" not present.'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            go to 3830
3810        call FeChybne(-1.,-1.,'the atom name "'//Veta(:idel(Veta))//
     1                    '" contains unacceptable symbol.',' ',
     2                    SeriousError)
            go to 3830
3820        NInfo=1
            call Velka(Veta(1:1))
            TextInfo(1)=Veta(:idel(Veta))//' has not been specified.'
            Veta='Do you want to leave this atom'
            if(ia.ne.iaLast) then
              Veta=Veta(:idel(Veta))//' and go to the next one?'
            else
              Veta=Veta(:idel(Veta))//'?'
            endif
            if(FeYesNoHeader(-1.,-1.,Veta,1)) then
              ich=0
              go to 3900
            endif
3830        EventType=EventEdw
            EventNumber=nEdw
            go to 3000
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtLocate) then
            dd=0
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3000
          endif
3900      call FeQuestRemove(id)
          if(ich.ne.0) then
            if(ich.gt.0) then
              call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
              call CopyFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
              call iom50(0,0,fln(:ifln)//'.m50')
              call iom40(0,0,fln(:ifln)//'.m40')
              go to 9999
            endif
            exit
          endif
        enddo
      enddo
      if(SaveKeepCommands) call RefRewriteCommands(1)
9999  call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      if(MapAlreadyUsed) then
        t256='"'//TmpDir(:idel(TmpDir))//fln(:ifln)//'*.*"'
        call DeleteAllFiles(t256)
        call FeTmpFilesClear(t256)
      endif
      call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z50')
      if(NMolec.gt.0.and.ISymmBasic.gt.0) then
        call CrlRestoreSymmetry(ISymmBasic)
        call CrlCleanSymmetry
      endif
      return
      end
