      subroutine EM40EditMultipole
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),PopAsP(68),PopAsPS(68),KiPopAsP(68)
      character*80 Veta
      character*12 at,pn
      integer Order,OrderOld,DelP
      logical :: lpom,InSigOld,InSig = .false.
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(lasmax(ia).eq.llasmaxa) then
            KiPocP=max(TRankCumul(itf(ia)),10)
            if(llasmaxa.lt.2) then
              DelP=3
            else
              DelP=(llasmaxa-1)**2+4
            endif
            KiP=KiPocP
            do j=1,DelP
              call kdoco(KiP+PrvniKiAtomu(ia),at,pn,1,PopAsP(j),
     1                   PopAsPS(j))
              KiP=KiP+1
              KiPopAsP(j)=KiA(KiP,ia)
            enddo
            exit
          endif
        endif
      enddo
      if(NAtActive.gt.0) then
        NAtEdit=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            NAtEdit=NAtEdit+1
            iap=IAtActive(i)
            if(lasmax(iap).lt.3) then
              jmx=3
            else
              jmx=(lasmax(iap)-1)**2+4
            endif
            KiP=max(TRankCumul(itf(iap)),10)
            do j=1,jmx
              call kdoco(KiP+PrvniKiAtomu(iap),at,pn,1,pom,spom)
              KiP=KiP+1
              if(abs(PopAsP(j)-pom).gt..000001.and.KiPopAsP(j).lt.100)
     1          KiPopAsP(j)=KiPopAsP(j)+100
              if(mod(KiPopAsP(j),10).ne.KiA(KiP,iap).and.
     1           mod(KiPopAsP(j),100).lt.10) KiPopAsP(j)=KiPopAsP(j)+10
            enddo
          endif
        enddo
      endif
      id=NextQuestId()
      xqd=600.
      nlasmax=2*llasmaxa-3
      if(nlasmax.gt.0) then
        il=5+(nlasmax-1)/4
      else
        il=1
      endif
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit multipole parameters',
     2                   0,LightGray,0,OKForBasicFiles)
      if(llasmaxa.lt.2) then
        ib=3
      else
        ib=4
      endif
      il=1
      KiP=KiPocP
      do i=1,ib
        call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(i),xpoma(i),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiPopAsP(i),
     1                      .false.)
        if(i.eq.1) then
          nEdwBasic=nEdw
          nCrwBasic=nCrw
        endif
        KiP=KiP+1
      enddo
      if(llasmaxa.ge.2) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        at='%Order'
        xpom=xqd*.5-7.5
        tpom=xpom-FeTxLength(at)-5.-EdwYd
        dpom=30.
        call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
        nEdwMultP=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,0,.false.)
        call FeQuestEudOpen(EdwLastMade,0,lasmaxa-3,1,0.,0.,0.)
        ilp=il+1
        do k=1,2
          il=ilp
          j=1
          call kdoco(KiP+PrvniKiAtomu(ia),at,pn,1,pom,spom)
          do i=1,nlasmax
            if(k.eq.1) then
              call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
              if(i.eq.1) nLblPop=LblLastMade
              call FeQuestLblOff(LblLastMade)
              call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
              call FeQuestLblOff(LblLastMade)
            else
              call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
              if(i.eq.1) then
                nEdwPop=nEdw
                nCrwPop=nCrw
              endif
            endif
            if(j.eq.4) then
              j=1
              il=il+1
            else
              j=j+1
            endif
          enddo
        enddo
        Order=0
        OrderOld=-1
        NOrder=Order*2+1
        NOrderOld=OrderOld*2+1
        InSigOld=InSig
        il=il+1
        dpom=80.
        pom=15.
        xpom=(xqd-4.*dpom-2.5*pom)*.5
        Veta='%Refine all'
        do i=1,4
          j=ButtonOff
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(i.eq.1) then
            nButtRefineAll=ButtonLastMade
            Veta='%Fix all'
          else if(i.eq.2) then
            nButtFixAll=ButtonLastMade
            Veta='Re%set'
          else if(i.eq.3) then
            nButtReset=ButtonLastMade
            Veta='Show %p/sig(p)'
          else if(i.eq.4) then
            nButtInSig=ButtonLastMade
            if(NAtEdit.gt.1) j=ButtonDisabled
          endif
          call FeQuestButtonOpen(ButtonLastMade,j)
          xpom=xpom+dpom+pom
        enddo
      else
        Order=1
        OrderOld=1
        InSigOld=.false.
        NOrder=0
        NOrderOld=0
        nEdwMultP=0
        nEdwPop=0
        nCrwPop=0
        nLblPop=0
        nButtRefineAll=0
        nButtFixAll=0
        nButtReset=0
        nButtInSig=0
      endif
1400  if(llasmaxa.lt.2) go to 1500
      if(OrderOld.ge.0) call FeQuestIntFromEdw(nEdwMultP,Order)
      NOrder=Order*2+1
      if(OrderOld.gt.1.and..not.InSigOld) then
        iskip=OrderOld**2+4
        call FeUpdateParamAndKeys(nEdwPop,nCrwPop,PopAsP(iskip+1),
     1                            KiPopAsP(iskip+1),NOrderOld)
      endif
      nEdw=nEdwPop
      nCrw=nCrwPop
      nLbl=nLblPop
      iskip=Order**2+4
      KiP=KiPocP+iskip
      k=iskip
      if(InSig) then
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          if(i.le.NOrder) then
            k=k+1
            call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
            call FeQuestLblChange(nLbl,pn)
            if(PopAsPS(k).gt.0.) then
              pom=abs(PopAsP(k)/PopAsPS(k))
            else
              pom=-1.
            endif
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
            KiP=KiP+1
          else
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        if(Order.ne.OrderOld.or.InSig.neqv.InSigOld) then
          do i=1,max(NOrder,NOrderOld)
            if(InSig.neqv.InSigOld) then
              call FeQuestLblOff(nLbl)
              call FeQuestLblOff(nLbl+1)
            endif
            if(i.le.NOrder) then
              k=k+1
              call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
              call FeQuestEdwLabelChange(nEdw,pn)
              call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',PopAsP(k),
     1                             KiPopAsP(k),.false.)
              KiP=KiP+1
            else
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            nLbl=nLbl+2
          enddo
        endif
        j=ButtonOff
      endif
      OrderOld=Order
      NOrderOld=NOrder
      InSigOld=InSig
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMultP) then
        ich=-1
        go to 1600
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPop
        do i=1,NOrder
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPop
        if(Order.eq.0) then
          call FeQuestRealFromEdw(nEdw,pom)
          call FeQuestRealFromEdw(nEdwBasic+1,pomv)
          pom=pom+pomv
          call FeQuestRealEdwOpen(nEdwBasic+1,pom,.false.,.false.)
        endif
        do i=1,NOrder
          call FeQuestRealEdwOpen(nEdw,0.,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        go to 1400
      else if(CheckType.eq.EventEdwUnlock.or.
     1        CheckType.eq.EventCrwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
1600  if(ich.le.0) then
        call FeUpdateParamAndKeys(nEdwBasic,nCrwBasic,PopAsP,KiPopAsP,
     1                            ib)
        if(nEdwMultP.gt.0) then
          call FeQuestIntFromEdw(nEdwMultP,Order)
          NOrder=Order*2+1
          iskip=OrderOld**2+4
          call FeUpdateParamAndKeys(nEdwPop,nCrwPop,PopAsP(iskip+1),
     1                              KiPopAsP(iskip+1),NOrderOld)
        endif
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(lasmax(ia).lt.2) then
              DelP=3
            else
              DelP=(lasmax(ia)-1)**2+4
            endif
            KiP=KiPocP
            do j=1,DelP
              if(KiPopAsP(j).lt.100)
     1          call kdoco(KiP+PrvniKiAtomu(ia),at,pn,-1,PopAsP(j),
     2                     PopAsPS(j))
              KiP=KiP+1
              if(mod(KiPopAsP(j),100).lt.10)
     1          KiA(KiP,ia)=mod(KiPopAsP(j),10)
            enddo
          endif
        enddo
        if(ich.lt.0) go to 1400
      endif
      call FeQuestRemove(id)
      return
      end
