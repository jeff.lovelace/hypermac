      subroutine EM40DefMolLocSyst(imp,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 Veta,ErrSt
      character*27 LocMolSystStO(2,2)
      character*2 LocMolSystAxO(2)
      integer RolMenuSelectedQuest,CrwStateQuest
      logical CrwLogicQuest
      real LocMolSystXO(3,2,2)
      dimension nRolMenuModelSystAx (2),nEdwModelSystSt (2),
     1          nRolMenuActualSystAx(2),nEdwActualSystSt(2),px(3)
      LocMolSystTypeO=LocMolSystType(imp)
      do i=1,2
        do j=1,2
          LocMolSystAxO(i)(j:j)=LocMolSystAx(i,imp)(j:j)
          LocMolSystStO(j,i)=LocMolSystSt(j,i,imp)
          call CopyVek(LocMolSystX(1,j,i,imp),LocMolSystXO(1,j,i),3)
        enddo
      enddo
      id=NextQuestId()
      xdq=300.
      xdqp=xdq*.5
      il=6
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Define local coordinate'//
     1                   ' system',0,LightGray,0,OKForBasicFiles)
      il=0
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='%Model  molecule'
        else
          Veta='%Actual molecule'
        endif
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        if(i.eq.1) then
          nCrwLocForModel=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,LocMolSystType(imp).gt.0)
        else
          nCrwLocForActual=CrwLastMade
        endif
        xpom=xpom+CrwXd+15.
        dpom=20.+EdwYd
        xpp=xpom+dpom+15.
        dpp=xdq-xpp-5.
        do j=1,2
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,' ','L',dpom,EdwYd,
     1                            1)
          if(i.eq.1) then
            nRolMenuModelSystAx(j)=RolMenuLastMade
          else
            nRolMenuActualSystAx(j)=RolMenuLastMade
          endif
          call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
          if(i.eq.1) then
            nEdwModelSystSt(j)=EdwLastMade
          else
            nEdwActualSystSt(j)=EdwLastMade
          endif
          il=il+1
        enddo
      enddo
1400  if(LocMolSystType(imp).gt.0) then
        if(CrwStateQuest(nCrwLocForActual).eq.CrwClosed) then
          call FeQuestCrwOpen(nCrwLocForActual,LocMolSystType(imp).gt.1)
          do i=1,2
            j=LocateInStringArray(SmbX,3,LocMolSystAx(1,imp)(i:i),
     1                            .true.)
            call FeQuestRolMenuOpen(nRolMenuModelSystAx(i),SmbX,3,j)
            Veta=LocMolSystSt(i,1,imp)
            if(Veta(1:1).eq.' ') Veta=Veta(2:)
            k=0
            call StToReal(Veta,k,px,3,.false.,ich)
            if(ich.eq.0) then
              write(Veta,'(3f12.6)') px
              call ZdrcniCisla(Veta,3)
            endif
            call FeQuestStringEdwOpen(nEdwModelSystSt(i),Veta)
          enddo
        endif
      else
        call FeQuestCrwClose(nCrwLocForActual)
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuModelSystAx(i))
          call FeQuestEdwClose(nEdwModelSystSt(i))
        enddo
      endif
      if(LocMolSystType(imp).gt.1) then
        do i=1,2
          j=LocateInStringArray(SmbX,3,LocMolSystAx(2,imp)(i:i),.true.)
          call FeQuestRolMenuOpen(nRolMenuActualSystAx(i),SmbX,3,j)
          Veta=LocMolSystSt(i,2,imp)
          if(Veta(1:1).eq.' ') Veta=Veta(2:)
          k=0
          call StToReal(Veta,k,px,3,.false.,ich)
          if(ich.eq.0) then
            write(Veta,'(3f12.6)') px
            call ZdrcniCisla(Veta,3)
          endif
          call FeQuestStringEdwOpen(nEdwActualSystSt(i),Veta)
        enddo
      else
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuActualSystAx(i))
          call FeQuestEdwClose(nEdwActualSystSt(i))
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocForModel) then
        if(CrwLogicQuest(nCrwLocForModel)) then
          LocMolSystType(imp)=max(LocMolSystType(imp),1)
        else
          LocMolSystType(imp)=min(LocMolSystType(imp),0)
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwLocForActual) then
        if(CrwLogicQuest(nCrwLocForActual)) then
          LocMolSystType(imp)=2
        else
          LocMolSystType(imp)=1
        endif
        go to 1400
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuModelSystAx(1).or.
     2         CheckNumber.eq.nRolMenuModelSystAx(2))) then
        if(CheckNumber.eq.nRolMenuModelSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocMolSystAx(1,imp)(j:j)=Smbx(k)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuActualSystAx(1).or.
     2         CheckNumber.eq.nRolMenuActualSystAx(2))) then
        if(CheckNumber.eq.nRolMenuActualSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocMolSystAx(2,imp)(j:j)=Smbx(k)
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwModelSystSt(1).or.
     2         CheckNumber.eq.nEdwModelSystSt(2).or.
     1         CheckNumber.eq.nEdwActualSystSt(1).or.
     2         CheckNumber.eq.nEdwActualSystSt(2))) then
        if(CheckNumber.eq.nEdwModelSystSt(1).or.
     1     CheckNumber.eq.nEdwModelSystSt(2)) then
          imol=(imp-1)/mxp+1
        else
          imol=0
        endif
        Veta=EdwStringQuest(CheckNumber)
        call CrlGetXFromAtString(Veta,imol,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=CheckNumber
        else
          ich=0
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(LocMolSystType(imp).gt.0) then
          do i=1,2
            Veta=EdwStringQuest(nEdwModelSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocMolSystSt(i,1,imp)=Veta
          enddo
        endif
        if(LocMolSystType(imp).gt.1) then
          do i=1,2
            Veta=EdwStringQuest(nEdwActualSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocMolSystSt(i,2,imp)=Veta
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        LocMolSystType(imp)=LocMolSystTypeO
        do i=1,2
          do j=1,2
            LocMolSystAx(i,imp)(j:j)=LocMolSystAxO(i)(j:j)
            LocMolSystSt(j,i,imp)=LocMolSystStO(j,i)
            call CopyVek(LocMolSystXO(1,j,i),LocMolSystX(1,j,i,imp),3)
          enddo
        enddo
      endif
9999  return
      end
