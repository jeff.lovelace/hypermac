      subroutine EM40SwitchBetaU(Klic,n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension poma(6,3)
      if(Klic.eq.0) then
        pom=1./episq
        do isw=1,NComp(KPhase)
          do j=1,6
            poma(j,isw)=1./urcp(j,isw,KPhase)
          enddo
        enddo
      else
        pom=episq
        call CopyVek(urcp(1,1,KPhase),poma,6*NComp(KPhase))
      endif
      if(n.le.0) then
        ip=1
        ik=NAtAll
      else
        ip=NAtIndFrAll(KPhase)+n-1
        ik=ip
      endif
      do i=ip,ik
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
        if(kswa(i).ne.KPhase) cycle
        isw=iswa(i)
        if(itf(i).eq.1) then
          beta(1,i)=beta(1,i)*pom
        else
          do j=1,6
            beta(j,i)=beta(j,i)*poma(j,isw)
            do k=1,kmoda(3,i)
              bx(j,k,i)=bx(j,k,i)*poma(j,isw)
              by(j,k,i)=by(j,k,i)*poma(j,isw)
            enddo
          enddo
        endif
      enddo
      return
      end
