      subroutine EM40NewAtomHMake(iaut,ia,im,isw,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun
      character*256 t256,p256,Command(1),CurrentDirO
      character*80 Veta,KeepAtNeighOld(:),KeepAtHOld(:)
      integer FeChdir,EdwStateQuest,KeepNAtHOld(:),KeepNAtNeighOld(:)
      logical Recalculate,SaveKeepCommands,EqIgCase,MapAlreadyUsed,
     1        TryAutomaticRun,RefineEnd
      real :: xp(1)=(/0./)
      allocatable KeepAtHOld,KeepAtNeighOld,KeepNAtHOld,KeepNAtNeighOld
      save /KeepQuest/
      call SpecPos(x(1,ia),xp,0,isw,.05,nocc)
      Recalculate=.false.
      iako=iak
      do i=1,KeepNH(1)
        kam=ktatmol(KeepAtH(1,i))
        if(kam.le.0) then
          if(im.le.0) then
            kam=NAtIndTo(isw,KPhase)+1
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
            call AtSun(kam,NAtAll,kam+1)
            NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
            do j=1,NAtActive
              if(IAtActive(j).ge.kam) IAtActive(j)=IAtActive(j)+1
            enddo
            call EM40UpdateAtomLimits
          else
            n=kpoint(im)*mam(im)
            IAtMol=NAtMolFr(1,1)
            if(NAtAll+n+kpoint(im).ge.MxAtAll)
     1        call ReallocateAtoms(100*(n+kpoint(im)))
            call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+n)
            NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+n
            iap=iap+n
            iak=iak+n
            iako=iako+n
            ia=ia+n
            iaLast=iaLast+n
            do j=1,NAtActive
              if(IAtActive(j).ge.IAtMol)
     1          IAtActive(j)=IAtActive(j)+n
            enddo
            do j=1,i-1
              KeepNAtH(1,j)=KeepNAtH(1,j)+n
            enddo
            NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+kpoint(im)
            call EM40UpdateAtomLimits
            kam=NAtMolFr(1,1)
            do j=1,im-1
              kam=kam+iam(j)
            enddo
            kam=kam+iam(im)/kpoint(im)
            call AtSun(kam,NAtAll-kpoint(im),kam+kpoint(im))
            iam(im)=iam(im)+kpoint(im)
          endif
          PrvniKiAtomu(kam)=PrvniKiAtomu(kam-1)+DelkaKiAtomu(kam-1)
          DelkaKiAtomu(kam)=0
        else
          Recalculate=Recalculate.or.ai(kam).gt.0.
        endif
        KeepNAtH(1,i)=kam
        call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModA(1,ia),
     1                 MagPar(ia),.false.)
        call AtCopy(ia,kam)
        isf(kam)=isfhp
        atom(kam)=KeepAtH(1,i)
        call SetIntArrayTo(KiA(1,kam),DelkaKiAtomu(kam),0)
        if(im.gt.0) then
          NAtCalc=NAtInd
          if(NMolec.gt.0) call SetMol(0,0)
        endif
      enddo
      call EM40SetNewH(1)
      do i=1,KeepNH(1)
        iah=KeepNAtH(1,i)
        call SpecPos(x(1,iah),xp,0,isw,.1,nocc)
        if(CheckNumber.eq.nButtLocate) then
          ai(iah)=0.
        else
          ai(iah)=ai(ia)
        endif
        sai(iah)=0.
        call SetRealArrayTo(sbeta(1,iah),6,0.)
        if(itf(iah).ge.2) then
          call boueq(beta(1,iah),sbeta(1,iah),1,bizo,sbizo,isw)
          itf(iah)=1
        else
          bizo=beta(1,iah)
        endif
        call SetRealArrayTo(beta(1,iah),6,0.)
        beta(1,iah)=bizo*BlowUpFactor
        do j=3,7
          KFA(j,iah)=0
          KModA(j,iah)=0
        enddo
        call ShiftKiAt(iah,itf(iah),ifr(iah),lasmax(iah),
     1                 KModA(1,iah),MagPar(iah),.false.)
      enddo
      if(SaveKeepCommands.and.
     1   (CheckNumber.eq.nButtApply.or.TryAutomaticRun)) then
        do m=1,2
          if(iaut.eq.1) then
            call EM40KeepWriteCommandAut(Command(1),ich)
          else
            call EM40KeepWriteCommand(Command(1),ich)
          endif
          if(ich.ne.0) go to 9999
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'_keep.tmp','formatted',
     1                  'unknown')
          j=0
3375      read(ln,FormA256,end=3380) t256
          call mala(t256)
          j=j+1
          k=0
          call kus(t256,k,Cislo)
          if(.not.EqIgCase(Cislo,'keep').and.
     1       .not.EqIgCase(Cislo,'!keep')) go to 3375
          call kus(t256,k,Cislo)
          if(m.eq.1) then
            ii=IdKeepHydro
          else
            ii=IdKeepADP
          endif
          if(.not.EqIgCase(Cislo,CKeepType(ii))) go to 3375
          call kus(t256,k,Cislo)
          call kus(t256,k,Cislo)
          call UprAt(Cislo)
          if(.not.EqIgCase(Cislo,KeepAtCentr(1))) go to 3375
          call CloseIfOpened(ln)
          call RewriteLinesOnFile(fln(:ifln)//'_keep.tmp',j,j,
     1                            Command,1)
          go to 3382
3380      call CloseIfOpened(ln)
          call AppendFile(fln(:ifln)//'_keep.tmp',Command,1)
3382      if(m.eq.1) then
            KeepDistHOld=KeepDistH(1)
            KeepADPExtFac(1)=BlowUpFactor
            KeepType(1)=IdKeepARiding
          else
            KeepType(1)=KeepTypeOld
            KeepDistH(1)=KeepDistHOld
          endif
        enddo
        napp=napp+KeepNH(1)
      else if(CheckNumber.eq.nButtLocate) then
        allocate(KeepAtNeighOld(KeepNNeighOld),
     1           KeepNAtNeighOld(KeepNNeighOld),KeepAtHOld(KeepNHOld),
     2           KeepNAtHOld(KeepNHOld))
        do i=1,KeepNNeighOld
          KeepAtNeighOld(i)=KeepAtNeigh(1,i)
          KeepNAtNeighOld(i)=KeepNAtNeigh(1,i)
        enddo
        do i=1,KeepNHOld
          KeepAtHOld(i)=KeepAtH(1,i)
          KeepNAtHOld(i)=KeepNAtH(1,i)
        enddo
        call EM40KeepWriteCommand(Command(1),ich)
        t256=fln(:ifln)//'.m40'
        j=idel(t256)-2
        p256=TmpDir(:idel(TmpDir))//fln(:ifln)//'.m40'
        k=idel(p256)-2
        do i=1,3
          call CopyFile(t256,p256)
          if(i.eq.1) then
            t256(j:)='m50'
            p256(k:)='m50'
          else if(i.eq.2) then
            t256(j:)='m90'
            p256(k:)='m90'
          endif
        enddo
        call iom40(1,0,fln(:ifln)//'.m40')
        p256=TmpDir(:idel(TmpDir))//fln(:ifln)//'*.*'
        call FeTmpFilesAdd(p256)
        CurrentDirO=CurrentDir
        i=FeChdir(TmpDir)
        call FeGetCurrentDir
        call iom40(1,0,fln(:ifln)//'.m40')
        IgnoreW=.true.
        Veta=KeepAtAnchor(1)
        call RefOpenCommands
        IgnoreW=.false.
        call DeleteFile(fln(:ifln)//'_keep.tmp')
        call RefRewriteCommands(1)
        call RefKeepReadCommand(Command(1),ich)
        KeepAtAnchor(1)=Veta
        if(.not.MapAlreadyUsed.or.Recalculate) then
          call EM40SetCommandsRefine
          call EM40SetCommandsFourier
          ShowInfoOnScreen=.false.
          call refine(0,RefineEnd)
          MapAlreadyUsed=.true.
        endif
        call EM40SetCommandsContour
        if(ErrFlag.ne.0) go to 3520
        call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.m45')
        call EM40RunContour(TorsAngle,ich)
3520    i=FeChdir(CurrentDirO)
        call FeGetCurrentDir
        if(EdwStateQuest(nEdwTorsAngle).eq.EdwOpened.and.ich.eq.0)
     1     call FeQuestRealEdwOpen(nEdwTorsAngle,TorsAngle,.false.,
     2                             .false.)
        if(SaveKeepCommands) then
          IgnoreW=.true.
          call RefOpenCommands
          IgnoreW=.false.
        endif
        iak=iako
        KeepAtCentr(1)=atom(ia)
        KeepNAtCentr(1)=ia
        KeepNH(1)=KeepNHOld
        KeepNNeigh(1)=KeepNNeighOld
        KeepAngleH(1)=TorsAngle
        KeepType(1)=KeepTypeOld
        do i=1,KeepNNeighOld
          KeepAtNeigh(1,i)=KeepAtNeighOld(i)
          KeepNAtNeigh(1,i)=KeepNAtNeighOld(i)
        enddo
        do i=1,KeepNHOld
          KeepAtH(1,i)=KeepAtHOld(i)
          KeepNAtH(1,i)=KeepNAtHOld(i)
          iah=KeepNAtH(1,i)
          ai(iah)=ai(ia)
        enddo
        if(KeepAtAnchor(1).eq.' ') then
          KeepAtAnchor(1)='#unknown#'
          call EM40SetNewH(1)
          KeepAtAnchor(1)='#unknown#'
        endif
        deallocate(KeepAtNeighOld,KeepNAtNeighOld,KeepAtHOld,
     1             KeepNAtHOld)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
9999  return
      end
