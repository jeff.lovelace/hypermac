      subroutine EM40CleanAtoms
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      j=0
      NAtMolPosun=0
      NAtMolDel=0
      NAtIndDel=0
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) then
          j=j+1
          cycle
        endif
        isw=iswa(i)
        ksw=kswa(i)
        if(isf(i).ne.0) then
          j=j+1
          if(i.ne.j) call AtSun(i,i,j)
        else
          if(i.le.NAtInd) then
            NAtIndLen(isw,ksw)=NAtIndLen(isw,ksw)-1
            NAtIndDel=NAtIndDel+1
          else
            im=(kmol(i)-1)/mxp+1
            iam(im)=iam(im)-KPoint(im)
            NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)-KPoint(im)
            NAtPosLen(isw,ksw)=NAtPosLen(isw,ksw)-mam(im)*KPoint(im)
            NAtMolDel=NAtMolDel+KPoint(im)
            NAtMolPosun=NAtMolPosun+mam(im)*KPoint(im)
          endif
        endif
      enddo
      if(NMolec.gt.0) then
        call AtSun(NAtMolFr(1,1)-NAtIndDel,NAtAll-NAtMolDel-NAtIndDel,
     1             NAtMolFr(1,1)-NAtMolPosun-NAtIndDel)
        call DelMol
      endif
      call EM40UpdateAtomLimits
      return
      end
