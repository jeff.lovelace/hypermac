      subroutine EM40EditAtomModPar(ModPar,lmod)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4),px(:,:),py(:,:),spx(:,:),spy(:,:),
     1          kip(:),pxp(:),kipp(:)
      character*80 Veta
      character*12 at,pn,pompn
      integer PrvKiMod,Order,PrvPar,PrvParOld,Harm,HarmOld,PrvKiMag,
     1        HarmP,HarmPOld
      logical :: lpom,Konec,InSig = .false.
      allocatable px,py,spx,spy,kip,pxp,kipp
      Konec=.false.
      if(ModPar.le.7) then
        Order=TRank(ModPar-1)
      else
        Order=3
      endif
      nki=2*lmod*Order
      if(ModPar.eq.1) nki=nki+1
      allocate(px(Order,lmod),py(Order,lmod),spx(Order,lmod),
     1         spy(Order,lmod),kip(nki),pxp(2*Order+1),kipp(2*Order+1))
      NAtEdit=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          NAtEdit=NAtEdit+1
          ia=IAtActive(i)
          l=lmod*Order
          if(ModPar.le.7) then
            if(KModA(ModPar,ia).eq.lmod) then
              if(ModPar.eq.1) then
                p0=a0(ia)
                sp0=sa0(ia)
                call CopyVek(ax(1,ia),px,l)
                call CopyVek(ay(1,ia),py,l)
                call CopyVek(sax(1,ia),spx,l)
                call CopyVek(say(1,ia),spy,l)
              else if(ModPar.eq.2) then
                call CopyVek(ux(1,1,ia),px,l)
                call CopyVek(uy(1,1,ia),py,l)
                call CopyVek(sux(1,1,ia),spx,l)
                call CopyVek(suy(1,1,ia),spy,l)
              else if(ModPar.eq.3) then
                if(lite(KPhase).eq.0) then
                  do k=1,lmod
                    do j=1,6
                       bx(j,k,ia)= bx(j,k,ia)/urcp(j,iswa(ia),KPhase)
                       by(j,k,ia)= by(j,k,ia)/urcp(j,iswa(ia),KPhase)
                      sbx(j,k,ia)=sbx(j,k,ia)/urcp(j,iswa(ia),KPhase)
                      sby(j,k,ia)=sby(j,k,ia)/urcp(j,iswa(ia),KPhase)
                    enddo
                  enddo
                endif
                call CopyVek(bx(1,1,ia),px,l)
                call CopyVek(by(1,1,ia),py,l)
                call CopyVek(sbx(1,1,ia),spx,l)
                call CopyVek(sby(1,1,ia),spy,l)
              else if(ModPar.eq.4) then
                call CopyVek(c3x(1,1,ia),px,l)
                call CopyVek(c3y(1,1,ia),py,l)
                call CopyVek(sc3x(1,1,ia),spx,l)
                call CopyVek(sc3y(1,1,ia),spy,l)
              else if(ModPar.eq.5) then
                call CopyVek(c4x(1,1,ia),px,l)
                call CopyVek(c4y(1,1,ia),py,l)
                call CopyVek(sc4x(1,1,ia),spx,l)
                call CopyVek(sc4y(1,1,ia),spy,l)
              else if(ModPar.eq.6) then
                call CopyVek(c5x(1,1,ia),px,l)
                call CopyVek(c5y(1,1,ia),py,l)
                call CopyVek(sc5x(1,1,ia),spx,l)
                call CopyVek(sc5y(1,1,ia),spy,l)
              else if(ModPar.eq.7) then
                call CopyVek(c6x(1,1,ia),px,l)
                call CopyVek(c6y(1,1,ia),py,l)
                call CopyVek(sc6x(1,1,ia),spx,l)
                call CopyVek(sc6y(1,1,ia),spy,l)
              endif
              PrvKiMod=max(TRankCumul(itf(ia)),10)+1
              do j=1,ModPar-1
                if(KModA(j,ia).gt.0) then
                    PrvKiMod=PrvKiMod+2*TRank(j-1)*KModA(j,ia)
                  if(j.eq.1) PrvKiMod=PrvKiMod+1
                endif
              enddo
            endif
          else
            call CopyVek(smx(1,1,ia),px,l)
            call CopyVek(smy(1,1,ia),py,l)
            call CopyVek(ssmx(1,1,ia),spx,l)
            call CopyVek(ssmy(1,1,ia),spy,l)
            PrvKiMod=PrvKiMag(ia)+3
          endif
          call CopyVekI(KiA(PrvKiMod,ia),kip,nki)
          go to 1200
        endif
      enddo
      call FeWinMessage('Co, co, co???',' ')
1200  if(NAtEdit.gt.1) InSig=.false.
      if(NAtActive.gt.0) then
        do 1250i=1,NAtActive
          if(LAtActive(i)) then
            iap=IAtActive(i)
            if(ModPar.le.7) then
              if(KModA(ModPar,iap).le.0) go to 1250
              l=max(TRankCumul(itf(iap)),10)+1
              do j=1,ModPar-1
                if(KModA(j,iap).gt.0) then
                    l=l+2*TRank(j-1)*KModA(j,iap)
                  if(j.eq.1) l=l+1
                endif
              enddo
              kmodp=KModA(ModPar,iap)
            else
              l=PrvKiMag(iap)+3
              kmodp=MagPar(iap)-1
            endif
            k=1
            if(ModPar.eq.1) then
              if(abs(p0-a0(iap)).gt..000001.and.kip(k).lt.100)
     1          kip(k)=kip(k)+100
              if(mod(kip(k),10).ne.KiA(l,iap).and.mod(kip(k),100).lt.10)
     1          kip(k)=kip(k)+10
              k=k+1
              l=l+1
            endif
            do jj=1,kmodp
              do ii=1,Order
                call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                if(abs(px(ii,jj)-pom).gt..000001.and.kip(k).lt.100)
     1            kip(k)=kip(k)+100
                if(mod(kip(k),10).ne.KiA(l,iap).and.
     1             mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                l=l+1
                k=k+1
              enddo
              do ii=1,Order
                call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                if(abs(py(ii,jj)-pom).gt..000001.and.kip(k).lt.100)
     1            kip(k)=kip(k)+100
                if(mod(kip(k),10).ne.KiA(l,iap).and.
     1             mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                l=l+1
                k=k+1
              enddo
            enddo
          endif
1250    continue
      endif
      if(ModPar.le.4.or.ModPar.eq.8) then
        nn=2
      else
        nn=1
      endif
      no=nn*Order
      if(ModPar.eq.1) then
        il=3
      else
        il=nn*((Order-1)/3+2)+1
      endif
      if(ModPar.eq.1.or.ModPar.eq.2) then
        if(KFA(2,ia).ne.0) then
          pompn='x-slope'
        else
          pompn='xcos33'
        endif
      else if(ModPar.eq.3) then
        pompn='U33cos33'
      else if(ModPar.eq.4) then
        pompn='C333cos33'
      else if(ModPar.eq.5) then
        pompn='D3333cos33'
      else if(ModPar.eq.6) then
        pompn='E33333cos33'
      else if(ModPar.eq.7) then
        pompn='F333333cos33'
      else
        pompn='Mxsin33'
      endif
      xqd=550.+3.*(FeTxLength(pompn)-FeTxLength('F111111cos11'))
      xqdp=xqd*.5
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      pom=FeTxLengthUnder(pompn)+5.
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit modulation amplitudes',
     2                   1,LightGray,0,OKForBasicFiles)
      il=1
      at='%Wave'
      xpom=xqdp
      tpom=xpom-FeTxLength(at)-5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwHarm=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,1,(2*lmod)/nn,1,0.,0.,0.)
      il=il+1
      j=1
      if(ModPar.eq.1) no=no+1
      do i=1,no
        if(i.eq.Order+1.and.ModPar.gt.1) then
          if(j.ne.1) il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          j=1
        endif
        call kdoco(PrvKiMod+PrvniKiAtomu(ia)+i-2,at,pn,1,pom,spom)
        if(ModPar.ne.1.or.i.ne.1) pn=pompn
        call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
        call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        if(.not.InSig) then
          call FeQuestLblOff(LblLastMade-1)
          call FeQuestLblOff(LblLastMade)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kip(i),
     1                         .false.)
        endif
        if(i.eq.1) then
          nLblFirst=LblLastMade-1
          nEdwPrv=nEdw
          nCrwPrv=nCrw
          PrvPar=i
        else if(i.eq.no) then
          exit
        endif
        if(j.eq.3) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      iki=1
      Harm=1
      HarmOld=1
      PrvParOld=PrvPar
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
          if(NAtEdit.gt.1) j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
1400  k=0
      m=0
      idf=nn*(Harm-1)*Order+1-iki
      if(nn.eq.1) then
        HarmP=(Harm-1)/2+1
        HarmPOld=(HarmOld-1)/2+1
        lold=mod(HarmOld-1,2)+1
        l=mod(Harm-1,2)+1
      else
        HarmP=Harm
        HarmPOld=HarmOld
        l=1
        lold=1
      endif
      if(.not.InSig) call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,
     1                                         kipp,no)
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblFirst
      do i=iki,iki+no-1
        if(ModPar.eq.1) then
          if(i.eq.iki) then
            j=1
          else
            j=i
            k=k+1
          endif
        else
          j=i
          k=k+1
        endif
        if(k.gt.Order) then
          k=1
          l=2
          lold=2
        endif
        if(.not.InSig) then
          m=m+1
          kip(j)=kipp(m)
          if(k.eq.0) then
            if(kip(j).lt.100) p0=pxp(m)
          else
            if(lold.eq.1) then
              if(kip(j).lt.100) px(k,HarmPOld)=pxp(m)
            else
              if(kip(j).lt.100) py(k,HarmPOld)=pxp(m)
            endif
          endif
        endif
        if(ModPar.eq.1.and.i.eq.iki) then
          pom=p0
          poms=sp0
        else
          if(l.eq.1) then
            pom=px(k,HarmP)
            poms=spx(k,HarmP)
          else
            pom=py(k,HarmP)
            poms=spy(k,HarmP)
          endif
        endif
        if(InSig) then
          if(poms.gt.0.) then
            pom=abs(pom/poms)
          else
            pom=-1.
          endif
        endif
        if(ModPar.ne.1.or.i.ne.iki) j=j+idf
        call kdoco(PrvKiMod+PrvniKiAtomu(ia)+j-2,at,pn,1,pp,ss)
        if(InSig) then
          if(ModPar.ne.1.or.i.ne.iki) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            call FeQuestLblChange(nLbl,pn)
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          endif
        else
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,kip(j),.false.)
        endif
        if(i.eq.iki+no-1.and.
     1     ((ModPar.eq.1.and.KFA(1,ia).ne.0.and.
     2       HarmP.eq.KModA(1,ia)).or.
     3      (ModPar.eq.2.and.KFA(2,ia).ne.0.and.
     4       HarmP.eq.KModA(2,ia)))) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
1410    nEdw=nEdw+1
        nCrw=nCrw+1
        nLbl=nLbl+2
      enddo
      if(Konec) go to 3000
      if(idf.ne.0) then
        iki=iki+idf
        HarmOld=Harm
      endif
      if(InSig) then
        j=ButtonDisabled
      else
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        Konec=.true.
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHarm) then
        call FeQuestIntFromEdw(nEdwHarm,Harm)
        if(Harm.ne.HarmOld) then
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,no
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        if(ModPar.eq.1) then
          pom=.01
        else if(ModPar.eq.2) then
          pom=.001
        else if(ModPar.eq.3) then
          pom=.0001
        else if(ModPar.eq.8) then
          pom=.1
        else
          pom=.00001
        endif
        do i=1,no
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        if(nn.eq.1) then
          l=mod(Harm-1,2)+1
        else
          l=1
        endif
        m=0
        k=0
        if(InSig) then
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              go to 1700
            else
              k=k+1
              if(k.gt.Order) then
                k=1
                l=2
              endif
              if(l.eq.1) then
                pom=px(k,HarmP)
              else if(l.eq.2) then
                pom=py(k,HarmP)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,kipp(m).ne.0)
            endif
1700        nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else
          call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,kipp,no)
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              j=1
              l=0
            else
              j=i
              k=k+1
            endif
            if(k.gt.Order) then
              k=1
              l=2
            endif
            kip(j)=kipp(m)
            if(l.eq.1) then
              px(k,HarmP)=pxp(m)
            else if(l.eq.2) then
              py(k,HarmP)=pxp(m)
            endif
          enddo
        endif
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        nButt=nButtRefineAll
        go to 1400
      else if(CheckType.eq.EventCrwUnlock.or.
     1        CheckType.eq.EventEdwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(ich.eq.0) then
        spom=0.
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(ModPar.le.7) then
              if(KModA(ModPar,ia).le.0) cycle
              PrvKiMod=max(TRankCumul(itf(ia)),10)+1
              do j=1,ModPar-1
                if(KModA(j,ia).gt.0) then
                    PrvKiMod=PrvKiMod+2*TRank(j-1)*KModA(j,ia)
                  if(j.eq.1) PrvKiMod=PrvKiMod+1
                endif
              enddo
              kmodp=KModA(ModPar,ia)
            else
              PrvKiMod=PrvKiMag(ia)+3
              kmodp=MagPar(ia)-1
            endif
            l=1
            if(ModPar.eq.1) then
              kk=PrvKiMod+l-1
              if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,pn,
     1                                     -1,p0,spom)
              if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
              l=l+1
            endif
            do j=1,kmodp
              do m=1,Order
                if(ModPar.eq.3) then
                   px(m,j)= px(m,j)*urcp(m,iswa(ia),KPhase)
                  spx(m,j)=spx(m,j)*urcp(m,iswa(ia),KPhase)
                endif
                kk=PrvKiMod+l-1
                if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,
     1                                       pn,-1,px(m,j),spx(m,j))
                if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
                l=l+1
                k=k+1
              enddo
              do m=1,Order
                if(ModPar.eq.3) then
                   py(m,j)= py(m,j)*urcp(m,iswa(ia),KPhase)
                  spy(m,j)=spy(m,j)*urcp(m,iswa(ia),KPhase)
                endif
                kk=PrvKiMod+l-1
                if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,
     1                                       pn,-1,py(m,j),spy(m,j))
                if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
                l=l+1
                k=k+1
              enddo
            enddo
          endif
        enddo
      else
        if(lite(KPhase).eq.0.and.ModPar.eq.3) then
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              do k=1,lmod
                 do j=1,6
                    bx(j,k,ia)= bx(j,k,ia)*urcp(j,iswa(ia),KPhase)
                    by(j,k,ia)= by(j,k,ia)*urcp(j,iswa(ia),KPhase)
                   sbx(j,k,ia)=sbx(j,k,ia)*urcp(j,iswa(ia),KPhase)
                   sby(j,k,ia)=sby(j,k,ia)*urcp(j,iswa(ia),KPhase)
                 enddo
              enddo
            endif
          enddo
        endif
      endif
      call FeQuestRemove(id)
      deallocate(px,py,spx,spy,kip,pxp,kipp)
      return
      end
