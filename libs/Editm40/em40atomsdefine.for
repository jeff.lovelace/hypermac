      subroutine EM40AtomsDefine
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension nRolMenuLocAtSystAx(2),nEdwLocAtSystSt(2),px(3),
     1          xx(6),ip(1)
      character*256 EdwStringQuest
      character*80  Veta,ErrSt,AtFill(5)
      character*27  Label(3)
      character*8   cSmbPGAt,iSmbPGAt
      character*2   Sipka,nty
      integer PrvKi,RolMenuSelectedQuest,CrwStateQuest,EdwStateQuest,
     1        RolMenuStateQuest
      logical CrwLogicQuest,EqIgCase,NUsed(:)
      allocatable NUsed
      save nEdwNo,nEdwAtomName,nRolMenuAtomType,nCrwIso,nCrwAniso,
     1     nCrwTLS,nCrwADP,nCrwMultNone,nCrwMultKappa,nCrwMultOrder,
     2     nEdwLocAtSystSt,nCrwRightHanded,nEdwADP,nEdwPointGroup,
     3     nButtPGInter,nButtPGSchoen,nEdwModPos,nRolMenuLocAtSystAx,
     4     nButtAtomList,nEdwModLast,NAtSelOld,isfo,itfo,MagParO,PrvKi,
     5     lasmaxo,nLblLocal,nLblUseCrenel,NMolSel,nLblPG,cSmbPGAt,
     6     nLblUseSawTooth,NAtMolSel,nLblTypeModFun,nButtNeigh
      data Sipka/'->'/
      data Label/'ADP parameter(s):','Multipole parameter(s):',
     1           'Modulation waves:'/
      entry EM40AtomsDefineMake(id)
      if(NKeepMax.le.0) call ReallocKeepCommands(1,8)
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      xqd=QuestXMax(id)-QuestXMin(id)
      NSel=0
      NAtSel=0
      NAtMolSel=0
      ctfa=0
      csfa=0
      clasmaxa=0
      call SetIntArrayTo(cmoda,7,-10)
      call SetIntArrayTo(ckfa,7,-10)
      CMagPar=-10
      KMagPar=-10
      CTypeModFun=-10
      CHarmLimit=-10.
      cSmbPGAt=' '
      iSmbPGAt=' '
      do i=1,NAtActive
        if(LAtActive(i)) then
          if(NAtSel.eq.0) NAtSel=i
          NSel=NSel+1
          ia=IAtActive(i)
          if(kmol(ia).gt.0) NAtMolSel=NAtMolSel+1
          if(ctfa.eq.0) then
            ctfa=itf(ia)
          else if(ctfa.gt.0) then
             if(itf(ia).ne.ctfa) ctfa=-1
          endif
          if(clasmaxa.eq.0) then
            clasmaxa=lasmax(ia)
          else if(clasmaxa.gt.0) then
             if(lasmax(ia).ne.clasmaxa) clasmaxa=-1
          endif
          if(ChargeDensities) then
            if(cSmbPGAt.eq.' ') then
              cSmbPGAt=SmbPGAt(ia)
              iSmbPGAt=SmbPGAt(ia)
            else if(.not.EqIgCase(cSmbPGAt,'ruzne')) then
              if(.not.EqIgCase(cSmbPGAt,SmbPGAt(i))) cSmbPGAt='ruzne'
            endif
          endif
          if(csfa.eq.0) then
            csfa=isf(ia)
          else if(csfa.gt.0) then
             if(isf(ia).ne.csfa) csfa=-1
          endif
          do j=1,7
            if(j.gt.2) then
              if(j.gt.itf(ia)+1) cycle
            endif
            if(cmoda(j).eq.-10) then
              cmoda(j)=KModA(j,ia)
            else if(cmoda(j).ge.0) then
              if(KModA(j,ia).ne.cmoda(j)) cmoda(j)=-1
            endif
          enddo
          do j=1,7
            if(cKFA(j).eq.-10) then
              cKFA(j)=KFA(j,ia)
            else if(cKFA(j).gt.0) then
              if(KFA(j,ia).ne.cKFA(j)) cKFA(j)=-1
            endif
          enddo
          if(CTypeModFun.eq.-10) then
            CTypeModFun=TypeModFun(ia)
          else if(CTypeModFun.ge.0) then
            if(CTypeModFun.ne.TypeModFun(ia)) CTypeModFun=-1
          endif
          if(NDimI(KPhase).gt.0) then
            if(TypeModFun(ia).eq.1) then
              if(CHarmLimit.lt.-9.) then
                CHarmLimit=OrthoEps(ia)
              else if(CHarmLimit.ge.0.) then
                if(CHarmLimit.ne.OrthoEps(ia)) CHarmLimit=-1.
              endif
            endif
          endif
          if(ia.lt.NAtMolFr(1,1)) then
            if(MagneticType(KPhase).gt.0) then
              if(CMagPar.le.-10) then
                CMagPar=MagPar(ia)
              else if(CMagPar.ne.-1) then
                if(CMagPar.ne.MagPar(ia)) CMagPar=-1
              endif
            endif
          endif
        endif
      enddo
      NAtSelOld=-1
      NAtSelAbs=IAtActive(NAtSel)
      il=1
      tpom=5.
      if(NSel.eq.1) then
        Veta='%#'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNo=EdwLastMade
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
        call FeQuestEudOpen(nEdwNo,1,NAtActive,1,0.,0.,0.)
        xpom=xpom+EdwYd+dpom+10.
        Veta='%List'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAtomList=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%Name'
        tpom=xpom+dpom+20.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=FeTxLengthUnder('XXXXXXXX')+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwAtomName=EdwLastMade
        tpom=xpom+dpom+20.
      else
        nEdwNo=0
        nButtAtomList=0
        nEdwAtomName=0
      endif
      Veta='%Type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom+EdwYd,
     1                        EdwYd,1)
      nRolMenuAtomType=RolMenuLastMade
      il=il+1
      xpom=5.
      call FeQuestLblMake(id,xpom,il,Label(1),'L','B')
      if(NDim(KPhase).le.3) then
        if(ChargeDensities)
     1    call FeQuestLblMake(id,180.,il,Label(2),'L','B')
      else
        call FeQuestLblMake(id,180.,il,Label(3),'L','B')
      endif
      ilp=il
      xpom=5.
      tpom=xpom+20.
      Veta='%isotropic'
      ICrwGr=2
      do i=1,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      ICrwGr)
        if(i.eq.1) then
          nCrwIso=CrwLastMade
          Veta='%harmonic (anisotropic)'
        else if(i.eq.2) then
          nCrwAniso=CrwLastMade
          Veta='%anharmonic'
        else if(i.eq.3) then
          nCrwADP=CrwLastMade
          tpomp=tpom+FeTxLengthUnder(Veta)+5.
          xpomp=tpomp+FeTxLength(Sipka)+5.
          dpom=30.
          call FeQuestEudMake(id,tpomp,il,xpomp,il,Sipka,'L',dpom,EdwYd,
     1                        1)
          nEdwADP=EdwLastMade
          Veta='%Use TLS'
        else if(i.eq.4) then
          nCrwTLS=CrwLastMade
          ICrwGr=0
          if(NDimI(KPhase).gt.0) then
            il=9
          else
            il=il+1
          endif
          Veta='Use ma%gnetic'
        else if(i.eq.5) then
          nCrwMagnetic=CrwLastMade
        endif
      enddo
      nEdwMagnetic=0
      nCrwMultNone=0
      nCrwMultKappa=0
      nCrwMultOrder=0
      nEdwMult=0
      nLblLocal=0
      do i=1,2
        nRolMenuLocAtSystAx(i)=0
        nEdwLocAtSystSt(i)=0
      enddo
      nCrwRightHanded=0
      nEdwPointGroup=0
      nButtPGInter=0
      nButtPGSchoen=0
      nButtNeigh=0
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          il=ilp
          xpom=180.
          tpom=xpom+20.
          Veta='n%one'
          do i=1,3
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,3)
            if(i.eq.1) then
              Veta='%kappa'
              nCrwMultNone=CrwLastMade
            else if(i.eq.2) then
              Veta='o%rder'
              nCrwMultKappa=CrwLastMade
            else
              nCrwMultOrder=CrwLastMade
            endif
          enddo
          tpom=tpom+FeTxLength('XXXXX')+5.
          xpom=tpom+FeTxLength(Sipka)+5.
          dpom=30.
          call FeQuestEudMake(id,tpom,il,xpom,il,Sipka,'L',dpom,EdwYd,1)
          nEdwMult=EdwLastMade
          xlocal=xpom+110.
          il=ilp
          tpom=xlocal
          Veta='Local coordinate system:'
          call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
          nLblLocal=LblLastMade
          call FeQuestLblDisable(LblLastMade)
          xpom=xlocal
          dpom=25.+EdwYd
          xp=xpom+dpom+5.
          xpp=xp+EdwYd+5.
          dpp=120.
          do i=1,2
            il=il+1
            call FeQuestRolMenuMake(id,tpom,ilp,xpom,il,' ','L',dpom,
     1                              EdwYd,1)
            nRolMenuLocAtSystAx(i)=RolMenuLastMade
            j=LocateInStringArray(SmbX,3,LocAtSystAx(NAtSelAbs)(i:i),
     1                            .true.)
            call FeQuestRolMenuOpen(RolMenuLastMade,SmbX,3,j)
            call FeQuestRolMenuDisable(RolMenuLastMade)
            call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
            nEdwLocAtSystSt(i)=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
            call FeQuestEdwDisable(EdwLastMade)
          enddo
          il=il+1
          Veta='Select neigh%bor atoms'
          dpom=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(id,xpp,il,dpom,ButYd,Veta)
          nButtNeigh=ButtonLastMade
          il=il+1
          Veta='Ri%ght handed system'
          xpom=tpom+CrwXd+35.
          call FeQuestCrwMake(id,xpom,il,tpom+30.,il,Veta,'L',CrwXd,
     1                          CrwYd,1,0)
          nCrwRightHanded=CrwLastMade
!          call FeQuestCrwOpen(CrwLastMade,.true.)
          call FeQuestCrwDisable(CrwLastMade)
          il=il+1
          Veta='Point group:'
          xpom=tpom+60.
          dpom=60.
          call FeQuestLblMake(id,xpom+dpom*.5,il,Veta,'C','B')
          nLblPG=LblLastMade
          call FeQuestLblDisable(LblLastMade)
          il=il+1
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,1)
          nEdwPointGroup=EdwLastMade
          call FeQuestEdwDisable(EdwLastMade)
          Veta='Select Her%mann-Mauguin short symbol'
          dpom=FeTxLengthUnder(Veta)+10.
          do i=1,2
            il=il+1
            call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            call FeQuestButtonDisable(ButtonLastMade)
            if(i.eq.1) then
              nButtPGInter=ButtonLastMade
              Veta='Select S%choenflies symbol'
            else
              nButtPGSchoen=ButtonLastMade
            endif
          enddo
        endif
        nEdwModFirst=0
        nEdwModLast=0
        nLblUseCrenel=0
        nLblUseSawTooth=0
        nCrwCrenel=0
        nCrwSawTooth=0
        nCrwZigZag=0
        nEdwMagnetic=0
        nLblTypeModFun=0
        nCrwHarm=0
        nCrwOrtho=0
        nCrwLegendre=0
        nCrwXHarm=0
        nEdwHarmLimit=0
      else
        il=ilp
        tpom=180.
        xpom=tpom+FeTxLength('ADP XXX')+EdwYd+5.
        dpom=30.
        xuse=xpom+dpom+EdwYd+20.
        xp=xuse+FeTxLength('XXXX')+15.
        tp=xp+CrwXd+5.
        xpp=tp+70.
        tpp=xpp+CrwXd+5.
        Veta='%Occupancy'
        do i=1,7
          il=il+1
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          if(i.le.2) then
            call FeQuestLblMake(id,xuse,il,'Use:','L','N')
            if(i.eq.1) then
              nLblUseCrenel=LblLastMade
            else
              nLblUseSawTooth=LblLastMade
            endif
          endif
          if(i.eq.1) then
            Veta='%Position'
            nEdwModOcc=EdwLastMade
            nEdwModFirst=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%crenel','L',CrwXd,
     1                          CrwYd,1,0)
            nCrwCrenel=CrwLastMade
          else if(i.eq.2) then
            write(Veta,100) i,nty(i)
            nEdwModPos=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%saw-tooth','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwSawTooth=CrwLastMade
            call FeQuestCrwMake(id,tpp,il,xpp,il,'%zig-zag','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwZigZag=CrwLastMade
            ilp=il+1
          else
            write(Veta,100) i,nty(i)
          endif
        enddo
        nEdwModLast=EdwLastMade
        if(MagneticType(KPhase).gt.0) then
          il=il+1
          Veta='%Magnetic'
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          nEdwMagnetic=EdwLastMade
        endif
        il=ilp
        call FeQuestLblMake(id,xuse,il,'Type of modulation functions:',
     1                      'L','B')
        nLblTypeModFun=LblLastMade
        xpom=xuse
        tpom=xuse+CrwgXd+5.
        ICrwGr=3
        Veta='harmonics (0,1)'
        do i=1,4
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGr)
          if(i.eq.1) then
            Veta='harmonics (0,1) orthogonalized to '//
     1           'crenel interval'
            nCrwHarm=CrwLastMade
          else if(i.eq.2) then
            Veta='Legendre polynomials in crenel interval'
            nCrwOrtho=CrwLastMade
          else if(i.eq.3) then
            Veta='x-harmonics in crenel interval'
            nCrwLegendre=CrwLastMade
          else if(i.eq.4) then
            nCrwXHarm=CrwLastMade
          endif
        enddo
        il=il+1
        tpom=xpom
        Veta='Selection limit for harmonics:'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwHarmLimit=EdwLastMade
      endif
      go to 2000
      entry EM40AtomsDefineUpdate
2000  if(NAtSel.ne.NAtSelOld) then
        itfo=-1
        isfo=-1
        MagParO=-1
        lasmaxo=-1
        if(kmol(NAtSelAbs).gt.0) then
          NMolSelPos=kmol(NAtSelAbs)
          NMolSel=(NMolSelPos-1)/mxp+1
          if(CrwStateQuest(NCrwTLS).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwADP)
            call FeQuestCrwOpen(nCrwTLS,.false.)
          endif
          NAtSelPos=0
          do i=1,NMolSel
            NAtSelPos=NAtSelPos+iam(i)*mam(i)
          enddo
          if(NSel.le.1) NAtMolSel=1
        else
          NMolSelPos=0
          NMolSel=0
          if(CrwStateQuest(NCrwADP).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwTLS)
            call FeQuestCrwOpen(nCrwADP,.false.)
          endif
          if(NSel.le.1) NAtMolSel=0
        endif
        if(NSel.le.1) then
          ctfa=itf(NAtSelAbs)
          clasmaxa=lasmax(NAtSelAbs)
          csfa=isf(NAtSelAbs)
          do j=1,7
           cmoda(j)=KModA(j,NAtSelAbs)
          enddo
          do j=1,7
            cKFA(j)=KFA(j,NAtSelAbs)
          enddo
          CTypeModFun=TypeModFun(NAtSelAbs)
          if(NDimI(KPhase).gt.0) then
            CHarmLimit=OrthoEps(NAtSelAbs)
          else
            CHarmLimit=-10.
          endif
          CMagPar=MagPar(NAtSelAbs)
          if(ChargeDensities) then
            cSmbPGAt=SmbPGAt(NAtSelAbs)
            iSmbPGAt=SmbPGAt(NAtSelAbs)
          endif
        endif
        call EM40GetAtom(NAtSelAbs,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                   MagParA,PrvKi)
        if(NSel.eq.1) then
          call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
          call FeQuestStringEdwOpen(nEdwAtomName,AtName)
        else
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              if((NMolSel.gt.0.and.kmol(ia).le.0).or.
     1           (NMolSel.le.0.and.kmol(ia).gt.0)) then
                NMolSel=-1
              endif
            endif
          enddo
          if(NMolSel.lt.0) then
            call FeQuestCrwClose(nCrwTLS)
            call FeQuestCrwClose(nCrwADP)
          endif
        endif
        if(NDimI(KPhase).gt.0) then
          call FeQuestLblOn(nLblUseCrenel)
          call FeQuestLblOn(nLblUseSawTooth)
          call FeQuestCrwOpen(nCrwCrenel,KFA(1,NAtSelAbs).ne.0)
          call FeQuestCrwOpen(nCrwSawTooth,KFA(2,NAtSelAbs).eq.1)
          call FeQuestCrwOpen(nCrwZigZag,KFA(2,NAtSelAbs).eq.2)
        endif
      endif
      if(itfa.ne.itfo) then
        if(itfo.lt.0) then
          nCrw=nCrwIso
          j=min(itfa,3)
          if(j.eq.0) j=4
          do i=1,4
            call FeQuestCrwOpen(nCrw,i.eq.j)
            if(NAtMolSel.gt.0.and.i.eq.3) then
              call FeQuestCrwDisable(nCrw)
            else if(NAtMolSel.lt.NSel.and.i.eq.4) then
              call FeQuestCrwDisable(nCrw)
            else
              if(ctfa.lt.0) then
                call FeQuestCrwLock(nCrw)
                if(itfa.gt.2) then
                  call FeQuestIntEdwOpen(nEdwADP,itfa,.false.)
                  call FeQuestEudOpen(nEdwADP,3,6,1,0.,0.,0.)
                  call FeQuestEdwLock(nEdwADP)
                endif
              endif
            endif
2100        nCrw=nCrw+1
          enddo
        else
          if(itfa.lt.0) then
            itfa=0
            if(ctfa.lt.0) ctfa=0
          endif
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
          do i=1,NAtActive
            if(LAtActive(i).and.ctfa.ge.0) then
              ia=IAtActive(i)
              if(itfa.eq.1) then
                if(itf(ia).eq.0) call ZmTF02(ia)
                if(itf(ia).ne.1) call ZmTF21(ia)
              else if(itfa.eq.2) then
                if(itf(ia).eq.1) then
                  call ZmTF12(ia)
                else if(itf(ia).eq.0) then
                  call ZmTF02(ia)
                endif
              else if(itfa.gt.2) then
                if(itf(ia).eq.0) call ZmTF02(ia)
                call ZmAnhi(ia,itfa)
              else if(itfa.eq.0) then
                if(itf(ia).eq.1) call ZmTF12(ia)
                if(itf(ia).ne.0) call ZmTF20(ia)
              endif
            endif
          enddo
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
        endif
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModFirst
          j=itfa+1
          if(NMolSel.gt.0.and.j.eq.1) j=3
          do i=1,7
            if(EdwStateQuest(nEdw).ne.EdwLocked) then
              if(i.le.2.or.i.le.j) then
                call FeQuestIntEdwOpen(nEdw,kmodan(i),.false.)
                call FeQuestEudOpen(nEdw,0,mxw,1,0.,0.,0.)
              else
                call FeQuestEdwClose(nEdw)
                kmodan(i)=0
              endif
            endif
            nEdw=nEdw+1
          enddo
        endif
      endif
      itfo=itfa
      if(itfa.ge.3.and.NMolSel.eq.0) then
        if(ctfa.ge.0) then
          call FeQuestIntEdwOpen(nEdwADP,itfa,.false.)
          call FeQuestEudOpen(nEdwADP,3,6,1,0.,0.,0.)
        endif
      else
        call FeQuestEdwClose(nEdwADP)
      endif
      if(NDimI(KPhase).gt.0) then
        nEdw=nEdwModFirst
        do i=1,7
          if(cmoda(i).lt.0) then
            call FeQuestEdwLock(nEdw)
          else
c            call FeQuestIntEdwOpen(nEdw,kmodan(i),.false.)
          endif
          if(cKFA(i).lt.0) then
            if(i.eq.1) then
              call FeQuestCrwLock(nCrwCrenel)
            else if(i.eq.2) then
              call FeQuestCrwLock(nCrwSawTooth)
              call FeQuestCrwLock(nCrwZigZag)
            endif
          else
            if(i.eq.1) then
              call FeQuestCrwOpen(nCrwCrenel,cKFA(1).ne.0)
            else if(i.eq.2) then
              call FeQuestCrwOpen(nCrwSawTooth,cKFA(2).eq.1)
              call FeQuestCrwOpen(nCrwZigZag,cKFA(2).eq.2)
            endif
          endif
          nEdw=nEdw+1
        enddo
        nCrw=nCrwHarm
        if(CrwLogicQuest(NCrwCrenel)) then
          call FeQuestLblOn(nLblTypeModFun)
          do i=1,4
            call FeQuestCrwOpen(nCrw,i-1.eq.CTypeModFun)
            if(CTypeModFun.lt.0) then
              call FeQuestCrwLock(nCrw)
              call FeQuestEdwDisable(nEdwHarmLimit)
            endif
            if(i.eq.2) then
              if(CTypeModFun.eq.1) then
                if(CHarmLimit.lt.0.) then
                  if(EdwStateQuest(nEdwHarmLimit).ne.EdwOpened)
     1               call FeQuestRealEdwOpen(nEdwHarmLimit,
     2                                       OrthoEps(NAtSelAbs),
     3                                       .false.,.false.)
                  call FeQuestEdwLock(nEdwHarmLimit)
                else
                  call FeQuestRealEdwOpen(nEdwHarmLimit,
     1                                    CHarmLimit,
     2                                    .false.,.false.)
                endif
              else
                call FeQuestEdwDisable(nEdwHarmLimit)
              endif
            endif
            nCrw=nCrw+1
          enddo
        else
          call FeQuestLblDisable(nLblTypeModFun)
          do i=1,4
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestEdwDisable(nEdwHarmLimit)
        endif
      endif
      if(NDimI(KPhase).le.0.and.ChargeDensities) then
        nCrw=nCrwMultNone
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.min(lasmaxa,3))
          if(clasmaxa.lt.0) call FeQuestCrwLock(nCrw)
          nCrw=nCrw+1
        enddo
        cSmbPGAt=' '
        iSmbPGAt=' '
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(lasmaxa.ne.lasmaxo.and.clasmaxa.ge.0) then
            if(lasmaxo.gt.2.and.lasmaxa.le.2)
     1        PopV(ia)=PopV(ia)+PopAs(1,ia)
            call ZmMult(ia,lasmaxa-1)
          endif
          if(lasmaxo.gt.0.and.lasmaxo.le.2.and.lasmaxa.gt.2)
     1      call CrlSetLocAtSyst(ia)
          if(cSmbPGAt.eq.' ') then
            cSmbPGAt=SmbPGAt(ia)
            iSmbPGAt=SmbPGAt(ia)
          else if(.not.EqIgCase(cSmbPGAt,'ruzne')) then
            if(.not.EqIgCase(cSmbPGAt,SmbPGAt(i))) cSmbPGAt='ruzne'
          endif
        enddo
        if(lasmaxa.ge.3) then
          if(lasmaxo.ne.lasmaxa.or.
     1       (clasmaxa.ge.0.and.EdwStateQuest(nEdwMult).ne.EdwOpened))
     2      then
            call FeQuestIntEdwOpen(nEdwMult,lasmaxa-3,.false.)
            call FeQuestEudOpen(nEdwMult,0,7,1,0.,0.,0.)
            if(clasmaxa.lt.0) call FeQuestEdwLock(nEdwMult)
          endif
          if(NSel.eq.1) then
            do i=1,2
              n=nRolMenuLocAtSystAx(i)
              j=LocateInStringArray(SmbX,3,LocAtSystAx(NAtSelAbs)(i:i),
     1                              .true.)
              call FeQuestRolMenuOpen(n,SmbX,3,j)
              Veta=LocAtSystSt(i,NAtSelAbs)
              k=0
              call StToReal(Veta,k,xx,3,.false.,ich)
              if(ich.eq.0) then
                write(Veta,101) xx(1:3)
                call ZdrcniCisla(Veta,3)
              endif
              call FeQuestStringEdwOpen(nEdwLocAtSystSt(i),Veta)
            enddo
            call FeQuestLblOn(nLblLocal)
            call FeQuestCrwOpen(nCrwRightHanded,
     1                          .not.LocAtSense(NAtSelAbs).eq.'-')
            call FeQuestButtonOff(nButtNeigh)
            cSmbPGAt=SmbPGAt(NAtSelAbs)
            iSmbPGAt=SmbPGAt(NAtSelAbs)
          endif
          call FeQuestLblOn(nLblPG)
          if(EqIgCase(cSmbPGAt,'ruzne')) then
            call FeQuestStringEdwOpen(nEdwPointGroup,SmbPGAt(NAtSelAbs))
            call FeQuestEdwLock(nEdwPointGroup)
            call FeQuestButtonDisable(nButtPGInter,ButtonOff)
            call FeQuestButtonDisable(nButtPGSchoen,ButtonOff)
            call FeQuestButtonDisable(nButtNeigh)
          else
            call FeQuestStringEdwOpen(nEdwPointGroup,cSmbPGAt)
            call FeQuestButtonOpen(nButtPGInter,ButtonOff)
            call FeQuestButtonOpen(nButtPGSchoen,ButtonOff)
            call FeQuestButtonOff(nButtNeigh)
          endif
        else
          call FeQuestEdwDisable(nEdwMult)
          call FeQuestLblDisable(nLblLocal)
          call FeQuestLblDisable(nLblPG)
          do i=1,2
            if(RolMenuStateQuest(nRolMenuLocAtSystAx(i)).eq.
     1         RolMenuOpened) then
              j=RolMenuSelectedQuest(nRolMenuLocAtSystAx(i))
              LocAtSystAx(NAtSel)(i:i)=Smbx(j)
            endif
            call FeQuestRolMenuDisable(nRolMenuLocAtSystAx(i))
            if(EdwStateQuest(nEdwLocAtSystSt(i)).eq.EdwOpened)
     1        LocAtSystSt(i,NAtSel)=EdwStringQuest(nEdwLocAtSystSt(i))
            call FeQuestEdwDisable(nEdwLocAtSystSt(i))
            call FeQuestRolMenuDisable(nRolMenuLocAtSystAx(i))
            call FeQuestEdwDisable(nEdwLocAtSystSt(i))
          enddo
          call FeQuestCrwDisable(nCrwRightHanded)
          call FeQuestButtonDisable(nButtNeigh)
          call FeQuestButtonDisable(nButtNeigh)
          call FeQuestEdwDisable(nEdwPointGroup)
          call FeQuestButtonDisable(nButtPGInter)
          call FeQuestButtonDisable(nButtPGSchoen)
        endif
        lasmaxo=lasmaxa
      endif
      if(isfa.ne.isfo) then
        if(RolMenuStateQuest(nRolMenuAtomType).ne.RolMenuDisabled) then
          AtTypeName=AtTypeMenu(isfa)
          call FeQuestRolMenuOpen(nRolMenuAtomType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfa)
        endif
        if(csfa.lt.0)
     1    call FeQuestRolMenuLock(nRolMenuAtomType)
      endif
      if(MagParA.ne.MagParO) then
        if(AtTypeMag(isfa,KPhase).eq.' ') then
          call FeQuestCrwClose(nCrwMagnetic)
          if(NDimI(KPhase).gt.0) call FeQuestEdwClose(nEdwMagnetic)
        else
          call FeQuestCrwOpen(nCrwMagnetic,MagParA.gt.0)
          if(CMagPar.lt.0) call FeQuestCrwLock(nCrwMagnetic)
          if(NDimI(KPhase).gt.0) then
            if(MagParA.gt.0) then
              if(MagParO.le.0) then
                call FeQuestIntEdwOpen(nEdwMagnetic,MagParA-1,.false.)
                call FeQuestEudOpen(nEdwMagnetic,0,mxw,1,0.,0.,0.)
                if(CMagPar.lt.0) call FeQuestEdwLock(nEdwMagnetic)
              endif
            else
              call FeQuestEdwClose(nEdwMagnetic)
            endif
          endif
        endif
        if(MagParA.ge.0) then
          do i=1,NAtActive
            if(LAtActive(i).and.CMagPar.ge.0) then
              ia=IAtActive(i)
              call ZmMag(ia,MagParA)
            endif
          enddo
        endif
      endif
      itfo=itfa
      isfo=isfa
      MagParO=MagParA
      NAtSelOld=NAtSel
      call EM40PutAtom(NAtSelAbs,NMolSel)
      go to 9999
      entry EM40AtomsDefineCheck
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNo) then
        call FeQuestIntFromEdw(nEdwNo,NAtSel)
        if(NAtSel.ne.NAtSelOld) then
          go to 3000
        else
          go to 9999
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtomList)
     1  then
        call SelOneAtom('Select next atom',NameAtActive,NAtSel,
     1                  NAtActive,ich)
        if(NAtSel.ne.NAtSelOld.and.ich.eq.0) then
          go to 3000
        else
          NAtSel=NAtSelOld
          go to 9999
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtomName)
     1  then
        AtName=EdwStringQuest(nEdwAtomName)
        idl=idel(AtName)
        if(AtName(idl:idl).eq.'*') then
          NAtTest=1000
          allocate(NUsed(NAtTest))
          idl=idl-1
          if(idl.le.0) then
            i=1
            go to 2200
          endif
          call SetLogicalArrayTo(NUsed,NAtTest,.false.)
          do j=1,2
            if(j.eq.1) then
              i1=1
              i2=NAtInd
            else
              i1=NAtMolFr(1,1)
              i2=NAtAll
            endif
            do i=i1,i2
              Veta=Atom(i)
              if(LocateSubstring(Veta,AtName(:idl),.false.,.true.)
     1           .gt.0) then
                k=idl
                call StToInt(Veta,k,ip,1,.false.,ich)
                if(ich.ne.0) cycle
                NUsed(ip(1))=.true.
              endif
            enddo
          enddo
          do i=1,NAtTest
            if(.not.NUsed(i)) then
              write(Cislo,FormI15) i
              call Zhusti(Cislo)
              AtName=AtName(:idl)//Cislo(:idel(Cislo))
              exit
            endif
          enddo
          deallocate(NUsed)
        endif
        call UprAt(AtName)
        call AtCheck(AtName,i,j)
2200    if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the atom name'
          else if(i.eq.2) then
            if(j.ne.NAtSel) then
              Veta='The atom name already exists'
            else
              go to 2300
            endif
          else if(i.eq.3) then

          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwAtomName
          go to 9999
        endif
2300    call FeQuestStringEdwOpen(nEdwAtomName,AtName)
      else if(CheckType.eq.EventRolMenuUnlock.and.
     1        CheckNumber.eq.nRolMenuAtomType) then
        csfa=isfa
        isfo=0
        go to 2000
      else if(CheckType.eq.EventEdwUnlock.and.
     1        CheckNumber.eq.nEdwADP) then
        ctfa=itfa
        itfo=-1
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtomType) then
        isfa=RolMenuSelectedQuest(nRolMenuAtomType)
        AtTypeName=AtTypeFull(isfa,KPhase)
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.ge.nCrwIso.and.
     3        (CheckNumber.le.nCrwADP.or.CheckNumber.le.nCrwTLS)) then
        if(CheckNumber.eq.nCrwIso) then
          itfa=1
        else if(CheckNumber.eq.nCrwAniso) then
          itfa=2
        else
          if(NMolSel.le.0) then
            itfa=3
          else
            itfa=-1
          endif
        endif
        if(CheckType.eq.EventCrwUnlock) then
          ctfa=itfa
          itfo=0
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2        CheckNumber.ge.nCrwMultNone.and.
     3        CheckNumber.le.nCrwMultOrder) then
        if(CheckNumber.eq.nCrwMultNone) then
          lasmaxa=1
        else if(CheckNumber.eq.nCrwMultKappa) then
          lasmaxa=2
        else if(CheckNumber.eq.nCrwMultOrder) then
          lasmaxa=3
          EventType=EventEdw
          EventNumber=nEdwMult
        endif
        if(CheckType.eq.EventCrwUnlock) then
          clasmaxa=lasmaxa
          lasmaxo=-1
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.ge.nCrwHarm.and.
     3         CheckNumber.le.nCrwXHarm) then
        CTypeModFun=CheckNumber-nCrwHarm
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            call EM40ChangeAtTypeMod(ia,CTypeModFun,OrthoEps(ia))
          endif
        enddo
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.eq.nCrwMagnetic) then
        if(CrwLogicQuest(nCrwMagnetic)) then
          MagParA=max(MagParO,1)
        else
          MagParA=0
        endif
        if(CheckType.eq.EventCrwUnlock) then
          CMagPar=MagParA
          MagParO=0
        endif
        go to 2000
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwADP) then
        call FeQuestIntFromEdw(nEdwADP,itfa)
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2         CheckNumber.ge.nEdwModFirst.and.
     3         CheckNumber.le.nEdwModLast.and.NDimI(KPhase).gt.0) then
        if(CheckType.eq.EventEdwUnlock) then
          j=CheckNumber-nEdwModFirst+1
          cmoda(j)=kmodan(j)
          call FeQuestIntEdwOpen(CheckNumber,kmodan(j),.false.)
        endif
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.CheckNumber.eq.nEdwMult)
     2  then
        if(CheckType.eq.EventEdwUnlock) then
          clasmaxa=llasmaxa
        endif
        call FeQuestIntFromEdw(nEdwMult,i)
        lasmaxa=i+3
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2         CheckNumber.eq.nEdwMagnetic) then
        if(CheckType.eq.EventEdwUnlock) then
          CMagPar=MagParA
          MagParO=0
          call FeQuestCrwOpen(nCrwMagnetic,MagParA.gt.0)
        else
          call FeQuestIntFromEdw(nEdwMagnetic,i)
          MagParA=i+1
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.eq.nCrwCrenel) then
        if(CheckType.eq.EventCrwUnlock) then
          cmoda(1)=kmodan(1)
          n=cmoda(1)
          cKFA(1)=kfan(1)
        else
          call FeQuestIntFromEdw(nEdwModOcc,n)
          call FeQuestIntFromEdw(nEdwModPos,m)
          if(CrwLogicQuest(nCrwCrenel)) then
            mm=m
            if(CrwLogicQuest(nCrwSawTooth)) then
              call FeQuestCrwOff(nCrwSawTooth)
              cKFA(2)=0
              m=m-1
            endif
            if(CrwLogicQuest(nCrwZigZag)) then
              call FeQuestCrwOff(nCrwZigZag)
              cKFA(2)=0
              m=m-1
            endif
            if(mm.ne.m) call FeQuestIntEdwOpen(nEdwModPos,m,.false.)
            n=n+1
            cKFA(1)=1
          else
            n=n-1
            cKFA(1)=0
          endif
        endif
        call FeQuestIntEdwOpen(nEdwModOcc,n,.false.)
        call FeQuestEudOpen(nEdwModOcc,0,mxw,1,0.,0.,0.)
        EventType=EventEdw
        EventNumber=nEdwModOcc
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2        (CheckNumber.eq.nCrwSawTooth.or.
     3         CheckNumber.eq.nCrwZigZag)) then
        if(CheckType.eq.EventCrwUnlock) then
          cmoda(2)=kmodan(2)
          n=cmoda(2)
          cKFA(2)=kfan(2)
        else
          call FeQuestIntFromEdw(nEdwModPos,n)
          call FeQuestIntFromEdw(nEdwModOcc,m)
          mm=m
          if(CheckNumber.eq.nCrwSawTooth) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              if(CrwLogicQuest(nCrwCrenel)) then
                call FeQuestCrwOff(nCrwCrenel)
                m=m-1
              endif
              if(CrwLogicQuest(nCrwZigZag)) then
                call FeQuestCrwOff(nCrwZigZag)
              else
                n=n+1
              endif
              cKFA(2)=1
            else
              n=n-1
              cKFA(2)=0
            endif
          else if(CheckNumber.eq.nCrwZigZag) then
            if(CrwLogicQuest(nCrwZigZag)) then
              if(CrwLogicQuest(nCrwCrenel)) then
                call FeQuestCrwOff(nCrwCrenel)
                m=m-1
              endif
              if(CrwLogicQuest(nCrwSawTooth)) then
                call FeQuestCrwOff(nCrwSawTooth)
              else
                n=n+1
              endif
              cKFA(2)=2
            else
              n=n-1
              cKFA(2)=0
            endif
          endif
          if(mm.ne.m) then
            call FeQuestIntEdwOpen(nEdwModOcc,m,.false.)
            cKFA(1)=0
          endif
        endif
        call FeQuestIntEdwOpen(nEdwModPos,n,.false.)
        call FeQuestEudOpen(nEdwModPos,0,mxw,1,0.,0.,0.)
        EventType=EventEdw
        EventNumber=nEdwModPos
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2        CheckNumber.eq.nEdwHarmLimit) then
        if(CheckType.eq.EventEdw) then
          call FeQuestRealFromEdw(nEdwHarmLimit,CHarmLimit)
        else
          CHarmLimit=OrthoEps(NAtSelAbs)
          call FeQuestRealEdwOpen(nEdwHarmLimit,CHarmLimit,.false.,
     1                            .false.)
        endif
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            call EM40ChangeAtTypeMod(ia,TypeModFun(ia),CHarmLimit)
          endif
        enddo
        go to 2000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRightHanded)
     1  then
        if(CrwLogicQuest(nCrwRightHanded)) then
          LocAtSense(NAtSel)=' '
        else
          LocAtSense(NAtSel)='-'
        endif
        EventType=EventEdw
        EventNumber=nEdwLocAtSystSt(1)
        go to 9999
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuLocAtSystAx(1).or.
     2         CheckNumber.eq.nRolMenuLocAtSystAx(2))) then
        if(CheckNumber.eq.nRolMenuLocAtSystAx(1)) then
          j=1
        else
          j=2
        endif
        i=RolMenuSelectedQuest(CheckNumber)
        LocAtSystAx(NAtSel)(j:j)=Smbx(i)
        go to 9999
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwLocAtSystSt(1).or.
     2         CheckNumber.eq.nEdwLocAtSystSt(2))) then
        if(CheckNumber.eq.nEdwLocAtSystSt(1)) then
          j=1
        else
          j=2
        endif
        i=nEdwLocAtSystSt(j)
        Veta=EdwStringQuest(i)
        if(Veta.eq.' ') go to 9999
        call CrlGetXFromAtString(Veta,0,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=i
        else
          LocAtSystSt(j,NAtSel)=Veta
          ich=0
        endif
        go to 9999
      else if((CheckType.eq.EventEdw.or.CheckType.eq.EventEdwUnlock)
     1         .and.CheckNumber.eq.nEdwPointGroup) then
        if(CheckType.eq.EventCrwUnlock) then
          cSmbPGAt=iSmbPGAt
          Veta=iSmbPGAt
        else
          Veta=EdwStringQuest(CheckNumber)
        endif
        do i=1,nSmbPG
          if(EqIgCase(Veta,SmbPGI(i))) then
            Veta=SmbPGI(i)
            go to 2460
          endif
        enddo
        do i=1,nSmbPG
          if(EqIgCase(Veta,SmbPGO(i))) then
            Veta=SmbPGO(i)
            go to 2460
          endif
        enddo
        call FeChybne(-1.,-1.,'wrong point group symbol.',' ',
     1                SeriousError)
        EventType=EventEdw
        EventNumber=CheckNumber
        go to 9999
2460    call FeQuestStringEdwOpen(nEdwPointGroup,Veta)
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            SmbPGAt(ia)=Veta
          endif
        enddo
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPGInter.or.
     2         CheckNumber.eq.nButtPGSchoen)) then
        ipg=LocateInStringArray(SmbPGI,nSmbPG,SmbPGAt(NAtSelAbs),.true.)
        if(ipg.le.0) ipg=LocateInStringArray(SmbPGO,nSmbPG,
     1                                       SmbPGAt(NAtSelAbs),.true.)
        Veta='Select point group'
        if(CheckNumber.eq.nButtPGInter) then
          call SelOneAtom(Veta,SmbPGI,ipg,nSmbPG,ich)
        else
          call SelOneAtom(Veta,SmbPGO,ipg,nSmbPG,ich)
        endif
        if(ich.eq.0) then
          if(CheckNumber.eq.nButtPGInter) then
            Veta=SmbPGI(ipg)
          else
            Veta=SmbPGI(ipg)
          endif
          call FeQuestStringEdwOpen(nEdwPointGroup,Veta)
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              SmbPGAt(ia)=Veta
            endif
          enddo
        endif
        go to 9999
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNeigh)
     1  then
        Veta='Select atoms for local coordinate system'
        call SelNeighborAtoms(Veta,3.,Atom(NAtSelAbs),AtFill,5,n,
     1                        iswa(NAtSelAbs),ich)
        if(ich.eq.0) then
          if(n.gt.0) then
            LocAtSystSt(1,NAtSelAbs)=AtFill(1)
            call FeQuestStringEdwOpen(nEdwLocAtSystSt(1),AtFill(1))
            if(n.gt.1) then
              LocAtSystSt(2,NAtSelAbs)=AtFill(2)
              call FeQuestStringEdwOpen(nEdwLocAtSystSt(2),AtFill(2))
            endif
          endif
        endif
        go to 9999
      endif
3000  if(NAtSelOld.gt.0.and.nEdwNo.gt.0)
     1  call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      NAtSelAbs=IAtActive(NAtSel)
      call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
      LAtActive(NAtSel)=.true.
      ChargeDensities=.false.
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(lasmax(i).gt.0) then
          ChargeDensities=.true.
          go to 2000
        endif
      enddo
      go to 2000
9999  return
100   format('ADP %',i1,a2)
101   format(3f22.6)
      end
