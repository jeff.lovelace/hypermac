      subroutine EM40SetKeepForNewH(ia,iap,iak,isfhp)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension xc(3),xb(3,3),uxb(3,mxw),uyb(3,mxw),x40b(3),RMiib(9)
      character*2  AtTypeCentral,AtTypeNeigh,AtTypeNeighUsed
      logical EqIgCase
      do i=1,NKeepAtMax
        KeepAt(1,i)=' '
      enddo
      KeepAtCentr(1)=' '
      KeepAtAnchor(1)=' '
      do i=1,5
        KeepAtNeigh(1,i)=' '
        if(i.le.4) KeepAtH(1,i)=' '
      enddo
      KeepNNeigh(1)=3
      KeepNH(1)=1
      KeepType(1)=IdKeepHTetraHed
      AtTypeCentral=AtType(isf(ia),KPhase)
      if(EqIgCase(AtTypeCentral,'C')) then
        DNeighH=2.4
        DNeighL=2.
        if(KeepDistHDefault(1).lt.0.) then
          KeepDistH(1)=0.96
        else
          KeepDistH(1)=KeepDistHDefault(1)
        endif
      else if(EqIgCase(AtTypeCentral,'N')) then
        DNeighH=2.4
        DNeighL=2.
        if(KeepDistHDefault(2).lt.0.) then
          KeepDistH(1)=0.87
        else
          KeepDistH(1)=KeepDistHDefault(2)
        endif
      else if(EqIgCase(AtTypeCentral,'O')) then
        DNeighH=2.5
        DNeighL=2.
        if(KeepDistHDefault(3).lt.0.) then
          KeepDistH(1)=0.82
        else
          KeepDistH(1)=KeepDistHDefault(3)
        endif
      else
        KeepDistH(1)=1.
        go to 5000
      endif
      call DistFromAtom(Atom(ia),3.,iswa(ia))
      call CopyVek(x(1,ia),xc,3)
      nh=0
      do i=1,NDist
        k=ipord(i)
        iah=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
        if(iah.le.0) cycle
        if(DDist(k).gt.1.1) cycle
        if(SymCodeJanaDist(k).ne.' ') cycle
        if(isf(iah).eq.isfhp.and.nh.lt.ubound(KeepAtH,2)) then
          nh=nh+1
          KeepAtH(1,nh)=ADist(k)
          j=idel(SymCodeJanaDist(k))
          if(j.gt.0) KeepAtH(1,nh)=KeepAtH(1,nh)(:idel(KeepAtH(1,nh)))//
     1                             '#'//SymCodeJanaDist(k)(:j)
        else
          go to 5000
        endif
      enddo
      fnn=0.
      nn=0
      DstMin=10.
      DstMax=0.
      DstAve=0.
      AtTypeNeighUsed=' '
      do i=1,NDist
        k=ipord(i)
        ian=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
        if(ian.le.0) then
          cycle
        else
          isfi=isf(ian)
          AtTypeNeigh=AtType(isfi,KPhase)
        endif
        if(EqIgCase(AtTypeNeigh,'H')) cycle
        if((DDist(k).gt.DNeighL.and.AtNum(isfi,KPhase).le.18.).or.
     1     (DDist(k).gt.DNeighH.and.AtNum(isfi,KPhase).gt.18.)) cycle
        fnn=fnn+1.
        nn=nn+1
        yy=0.
        if(EqIgCase(AtTypeCentral,'C')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.53-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.442-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'B')) then
            yy=exp((1.7-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.390-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'F')) then
            yy=exp((1.32-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'S')) then
            yy=exp((1.770-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Cl')) then
            yy=exp((1.76-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Br')) then
            yy=exp((1.9-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Fe')) then
            yy=exp((1.689-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Co')) then
            yy=exp((1.634-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Cu')) then
            yy=exp((1.446-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Ru')) then
            yy=exp((2.2-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'P')) then
            yy=exp((1.89-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Si')) then
            yy=exp((1.883-DDist(k))/.37)-1.
          else
            yy=-1.
          endif
          if(yy.lt.-.5) then
            fnn=fnn-1.
            nn=nn-1.
            cycle
          endif
        else if(EqIgCase(AtTypeCentral,'N')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.442-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.361-DDist(k))/.37)-1.
          endif
        else if(EqIgCase(AtTypeCentral,'O')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.390-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.361-DDist(k))/.37)-1.
          endif
        endif
        AtTypeNeighUsed=AtTypeNeigh
        fnn=fnn+yy
        if(nn.gt.5) cycle
        KeepAtNeigh(1,nn)=ADist(k)
        KeepNAtNeigh(1,nn)=ktatmol(KeepAtNeigh(1,nn))
        NAtNeigh=ktat(Atom(iap),iak-iap+1,KeepAtNeigh(1,nn))+iap-1
        j=idel(SymCodeJanaDist(k))
        if(j.gt.0) KeepAtNeigh(1,nn)=
     1               KeepAtNeigh(1,nn)(:idel(KeepAtNeigh(1,nn)))//
     2               '#'//SymCodeJanaDist(k)(:j)
        if(nn.le.3)
     1    call EM40GetXFromAtName(KeepAtNeigh(1,nn),ib,xb(1,nn),uxb,uyb,
     2                            x40b,RMiib,ich)
        DstMin=min(DstMin,DDist(k))
        DstMax=max(DstMax,DDist(k))
        DstAve=DstAve+DDist(k)
      enddo
      if(nn.gt.0) DstAve=DstAve/float(nn)
      KeepNNeigh(1)=nn
      if(anint(fnn).gt.4) then
        KeepType(1)=IdKeepHApical
        if(EqIgCase(AtTypeCentral,'C').or.
     1     EqIgCase(AtTypeCentral,'N').or.
     2     EqIgCase(AtTypeNeigh,'O')) then
          KeepNH(1)=0
        else
          KeepNH(1)=1
        endif
      else if(anint(fnn).eq.4) then
        KeepType(1)=IdKeepHApical
        KeepNH(1)=0
      else if(anint(fnn).eq.3..or.
     1        (EqIgCase(AtTypeCentral,'N').and.nn.eq.3)) then
        if(EqIgCase(AtTypeCentral,'C')) then
          if(nn.eq.2) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=1
          else
            KeepType(1)=IdKeepHTetraHed
            KeepNH(1)=1
          endif
        else if(EqIgCase(AtTypeCentral,'N')) then
          if(nn.eq.3) then
            pom=0.
            do i=1,2
              do j=i+1,3
                pom=pom+AngleFromThreePoints(xc,xb(1,i),xb(1,j))
              enddo
            enddo
            if(pom.lt.345.) then
              KeepType(1)=IdKeepHTetraHed
              KeepNH(1)=1
            else
              KeepType(1)=IdKeepHTriangl
              KeepNH(1)=0
            endif
          else
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          endif
        else
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=0
        endif
      else if(fnn.gt.1.5) then
        if(EqIgCase(AtTypeCentral,'O')) then
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=0
        else
          if(nn.ge.2) then
            if(EqIgCase(AtTypeCentral,'C')) then
              KeepType(1)=IdKeepHTetraHed
              KeepNH(1)=2
            else
              if(nn.eq.2) then
                KeepType(1)=IdKeepHTriangl
                KeepNH(1)=1
              else
                KeepType(1)=IdKeepHApical
                KeepNH(1)=0
              endif
            endif
          else
            if(EqIgCase(AtTypeCentral,'C')) then
              KeepType(1)=IdKeepHTriangl
              KeepNH(1)=2
            else
              KeepType(1)=IdKeepHApical
              KeepNH(1)=1
            endif
          endif
        endif
      else if(anint(fnn).gt.0) then
        if(NAtNeigh.gt.0) then
          AtTypeNeigh=AtType(isf(NAtNeigh),KPhase)
          if((EqIgCase(AtTypeCentral,'O').and.
     1        EqIgCase(AtTypeNeigh,'Cl'))) then
            KeepType(1)=IdKeepHApical
            KeepNH(1)=0
            go to 3000
          endif
        else
          AtTypeNeigh=' '
        endif
        if(EqIgCase(AtTypeCentral,'O')) then
          if(DstMin.lt.1.2) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          else if(DstMin.lt.1.42) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          else
            KeepType(1)=IdKeepHApical
            KeepNH(1)=1
          endif
        else if(EqIgCase(AtTypeCentral,'C')) then
          KeepType(1)=IdKeepHTetraHed
          KeepNH(1)=3
        else if(EqIgCase(AtTypeCentral,'N')) then
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=2
        endif
        if(ian.le.0) go to 3000
        call DistFromAtom(KeepAtNeigh(1,nn),3.,iswa(ia))
        KeepAtAnchor(1)='#unknown#'
        do i=1,NDist
          k=ipord(i)
          ian=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
          if(ian.le.0) cycle
          isfi=isf(ian)
          if((DDist(k).gt.DNeighL.and.AtNum(isfi,KPhase).le.16.).or.
     1       (DDist(k).gt.DNeighH.and.AtNum(isfi,KPhase).gt.16.)) cycle
          if(isfi.eq.isfhp.or.
     1       EqIgCase(ADist(k),Atom(ia))) cycle
          KeepAtAnchor(1)=ADist(k)
          j=idel(SymCodeJanaDist(k))
          if(j.gt.0) KeepAtAnchor(1)=
     1                 KeepAtAnchor(1)(:idel(KeepAtAnchor(1)))//
     2                 '#'//SymCodeJanaDist(k)(:j)
          exit
        enddo
      else if(nn.eq.0.or.anint(fnn).le.0.) then
        KeepType(1)=IdKeepHApical
        KeepNH(1)=0
      endif
3000  do i=nh+1,KeepNH(1)
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        KeepAtH(1,i)='H'//Cislo(:idel(Cislo))//Atom(ia)(:idel(Atom(ia)))
      enddo
5000  KeepAtCentr(1)=Atom(ia)
      KeepNAtCentr(1)=ia
      return
      end
