      subroutine EM40UpdateMolecLimits
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      NMolec=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NMolecFr(i,KPh)=1
          else
            NMolecFr(i,KPh)=NMolec+1
          endif
          NMolecTo(i,KPh)=NMolecFr(i,KPh)+NMolecLen(i,KPh)-1
          NMolec=NMolec+NMolecLen(i,KPh)
        enddo
      enddo
      do KPh=1,NPhase
        NMolecFrAll(KPh)=NMolecFr(1,KPh)
        NMolecLenAll(KPh)=0
        do i=1,NComp(KPh)
          NMolecLenAll(KPh)=NMolecLenAll(KPh)+NMolecLen(i,KPh)
        enddo
        NMolecToAll(KPh)=NMolecFr(1,KPh)+NMolecLenAll(KPh)-1
      enddo
      return
      end
