      subroutine EM40EditADP
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),cp(:),cps(:),kip(:)
      character*80 Veta
      character*12 at,pn
      integer Order,OrderOld,PrvKiAnh
      logical :: lpom,InSigOld,InSig = .false.
      allocatable cp,cps,kip
      allocate(cp(74),cps(74),kip(74))
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(itf(ia).eq.ltfa) then
            nki=1
            do j=3,ltfa
              n=TRank(j)
              if(j.eq.3) then
                call CopyVek( c3(1,ia),cp (nki),n)
                call CopyVek(sc3(1,ia),cps(nki),n)
              else if(j.eq.4) then
                call CopyVek( c4(1,ia),cp (nki),n)
                call CopyVek(sc4(1,ia),cps(nki),n)
              else if(j.eq.5) then
                call CopyVek( c5(1,ia),cp (nki),n)
                call CopyVek(sc5(1,ia),cps(nki),n)
              else
                call CopyVek( c6(1,ia),cp (nki),n)
                call CopyVek(sc6(1,ia),cps(nki),n)
              endif
              nki=nki+n
            enddo
            nki=nki-1
            PrvKiAnh=11
            call CopyVekI(KiA(PrvKiAnh,ia),kip,nki)
            go to 1200
          endif
        endif
      enddo
1200  if(NAtActive.gt.0) then
        NAtEdit=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            NAtEdit=NAtEdit+1
            iap=IAtActive(i)
            if(itf(iap).gt.2) then
              l=PrvKiAnh
              k=1
              do j=3,itf(iap)
                n=TRank(j)
                do ii=1,n
                  call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                  if(abs(cp(k)-pom).gt..000001.and.kip(k).lt.100)
     1              kip(k)=kip(k)+100
                  if(mod(kip(k),10).ne.KiA(l,iap).and.
     1               mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                  l=l+1
                  k=k+1
                enddo
              enddo
            endif
          endif
        enddo
      endif
      id=NextQuestId()
      xqd=600.
      NParMax=TRank(ltfa)
      il=3+(nParMax-1)/4
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit ADP parameters',0,LightGray,0,
     2                   OKForBasicFiles)
      il=1
      at='%Order'
      xpom=xqd*.5-7.5
      tpom=xpom-FeTxLength(at)-5.-EdwYd
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwOrder=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,3,.false.)
      call FeQuestEudOpen(EdwLastMade,3,ltfa,1,0.,0.,0.)
      ilp=il+1
      do k=1,2
        il=ilp
        j=1
        call kdoco(PrvKiAnh+PrvniKiAtomu(ia)-1,at,pn,1,pom,spom)
        do i=1,NParMax
          if(k.eq.1) then
            call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
            if(i.eq.1) nLblPrv=LblLastMade
            call FeQuestLblOff(LblLastMade)
            call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
            call FeQuestLblOff(LblLastMade)
          else
            call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
            if(i.eq.1) then
              nEdwPrv=nEdw
              nCrwPrv=nCrw
            endif
          endif
          if(j.eq.4.and.i.ne.NParMax) then
            j=1
            il=il+1
          else
            j=j+1
          endif
        enddo
      enddo
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
          if(NAtEdit.gt.1) j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      Order=3
      OrderOld=-1
      NOrder=10
      NOrderOld=10
      InSigOld=InSig
1400  if(OrderOld.ge.0) then
        call FeQuestIntFromEdw(nEdwOrder,Order)
        NOrder=TRank(Order)
        k=TRankCumul(OrderOld-1)-9
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,cp(k),kip(k),
     1                            NOrderOld)
      endif
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblPrv
      k=TRankCumul(Order-1)-9
      l=PrvKiAnh+PrvniKiAtomu(ia)+k-2
      if(InSig) then
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          if(i.le.NOrder) then
            call kdoco(l,at,pn,1,pom,spom)
            call FeQuestLblChange(nLbl,pn)
            if(cps(k).gt.0.) then
              pom=abs(cp(k)/cps(k))
            else
              pom=-1.
            endif
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          else
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          k=k+1
          l=l+1
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          if(i.le.NOrder) then
            call kdoco(l,at,pn,1,pom,spom)
            call FeOpenParEdwCrw(nEdw,nCrw,pn,cp(k),kip(k),.false.)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          k=k+1
          l=l+1
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonOff
      endif
      OrderOld=Order
      NOrderOld=NOrder
      InSigOld=InSig
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwOrder) then



        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,NOrder
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        do i=1,NOrder
          call FeQuestRealEdwOpen(nEdw,0.,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwOrder,Order)
        NOrder=TRank(Order)
        k=TRankCumul(OrderOld-1)-9
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,cp(k),kip(k),
     1                            NOrderOld)
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(itf(ia).gt.2) then
            k=PrvKiAnh
            do j=1,TRankCumul(itf(ia))-10
              if(kip(j).lt.100) call kdoco(k+PrvniKiAtomu(ia)-1,at,pn,
     1                                     -1,cp(j),cps(j))
              if(mod(kip(j),100).lt.10) KiA(k,ia)=mod(kip(j),10)
              k=k+1
            enddo
          endif
        enddo
      endif
      call FeQuestRemove(id)
      deallocate(cp,cps,kip)
      return
      end
