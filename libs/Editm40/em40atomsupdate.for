      subroutine EM40AtomsUpdate
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      integer SbwItemSelQuest
      Klic=0
      NAtActiveSel=0
      NAtomicSel=0
      do i=1,NAtActive
        if(SbwItemSelQuest(i,nSbwSel).eq.1) then
          NAtActiveSel=NAtActiveSel+1
          LAtActive(i)=.true.
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=NAtomicSel+1
        else
          LAtActive(i)=.false.
        endif
      enddo
      write(Cislo,'(i10)') NAtActiveSel
      call Zhusti(Cislo)
      LabelSel(DelLabelSel+2:)=' -> '//Cislo(:idel(Cislo))//' selected'
      call FeQuestLblChange(nLblLabelSel,LabelSel)
      return
      end
