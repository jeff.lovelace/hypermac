      subroutine EM40NewMol(ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest,FileNameM45,FileNameM45Old,
     1              FileNameJPG,Veta,CurrentDirO
      character*80  AtP(3),StRefPointP,PositionName,t80
      character*8 AtM(3),NewMolName,ScDistSt
      character*8, allocatable :: AtActual(:)
      character*2 nty
      integer FeMenu,EdwStateQuest,Actual,UseTabsIn,FeChdir,
     1        RolMenuSelectedQuest
      integer, allocatable :: IAtActual(:),ip(:)
      logical FeYesNo,ExistFile,CrwLogicQuest,k45,ShowCoinc,EqIgCase,
     1        EqRV,Znovu,lpom
      real par(6),pa(9),pb(9),pc(9),trl(9),xx(3,3),xo(3,3),xxo(3,3),
     1     paa(3),pbb(3),paav(3),pbbv(3),trp(9),qcmol(3),AiModel(3),
     2     AiActual(3)
      real, allocatable :: xp(:,:)
      external EM40NewMolUpdateQuest,FeVoid
      common/EM40NewMolC/ nEdwFirstPoint,nEdwSelect,nButtSelect,nEdwOcc,
     1                    nButtCalc,nButtShowCoinc,Model(3),Actual(3),
     2                    nButtApplyNext,nButtApplyEnd,nButtPoint,Znovu
      data dco/0.3/
      save /EM40NewMolC/
      allocate(xp(3,3000),ip(3000))
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      if(NMolecLenAll(KPhase).le.0) irot(KPhase)=1
      isw=1
      k45=NAtIndLenAll(KPhase).le.0
      FileNameM45=' '
      FileNameM45Old=' '
      WizardId=NextQuestId()
      WizardMode=.true.
      WizardTitle=.false.
      WizardLength=450.
      WizardLines=11
      call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                   ' ',0,LightGray,0,0)
1100  id=NextQuestId()
      call FeQuestTitleMake(id,'Atoms of the new molecule from:')
      QuestCheck(id)=1
      il=1
      do i=1,2
        if(i.eq.1) then
          Veta='Mo%del file'
          tpom=WizardLength*.5-50.
        else
          Veta='%Atomic part'
          tpom=WizardLength*.5+50.
        endif
        xpom=tpom-CrwgXd
        call FeQuestCrwMake(id,tpom,il,xpom,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) then
          nCrwFromM45=CrwLastMade
        else
          nCrwFromM40=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
        if(k45) call FeQuestCrwDisable(CrwLastMade)
      enddo
      il=il+2
      call FeQuestLinkaMake(id,il)
      tpom=5.
      Veta='%Name of the molecule'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=WizardLength-dpom-5.
      call FeQuestButtonMake(id,pom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='Model %filename'
      dpom=pom-10.-xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      call FeQuestStringEdwOpen(EdwLastMade,FileNameM45)
      nEdwFileNameM45=EdwLastMade
      il=il+1
      Veta='%Predefined models'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=WizardLength-dpom-5.
      call FeQuestButtonMake(id,pom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtPreDefined=ButtonLastMade
      Veta='%Scaling distance'
      pom=1.5
      xpom1=tpom+FeTxLength(Veta)
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      nEdwScDist=EdwLastMade
      il=il+1
      Veta='Sho%w the model molecule'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom1=(WizardLength-dpom)*.5
      call FeQuestButtonMake(id,xpom1,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtShowModel=ButtonLastMade
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Composite part'
        dpom=50.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwComp=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
        call FeQuestEudOpen(EdwLastMade,1,NComp(KPhase),1,0.,0.,0.)
        if(NComp(KPhase).le.1) call FeQuestEdwDisable(EdwLastMade)
      else
        nEdwComp=0
      endif
      call FeQuestButtonDisable(ButtonEsc)
      ScDist=-1.
      ScDistSt=' '
1150  if(k45) then
        call FeQuestStringEdwOpen(nEdwFileNameM45,FileNameM45)
        call FeQuestButtonOff(nButtBrowse)
        call FeQuestButtonOff(nButtPreDefined)
        if(FileNameM45.ne.' ') then
          if(ExistFile(FileNameM45).and.
     1       .not.EqIgCase(FileNameM45,FileNameM45Old)) then
            ScDist=-1.
            ScDistSt=' '
            n=0
            call GetPureFileName(FileNameM45,FileNameJPG)
            FileNameJPG=FileNameJPG(:idel(FileNameJPG))//'.jpg'
            call OpenFile(45,FileNameM45,'formatted','old')
            if(ErrFlag.ne.0) go to 1170
1160        read(45,FormA,end=1170) Veta
            k=0
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,'scdist')) then
              n=n+1
              call kus(Veta,k,ScDistSt)
              call StToReal(Veta,k,pa,1,.false.,ichp)
              if(ichp.ne.0) then
                ScDist=-1.
                ScDistSt=' '
              else
                ScDist=pa(1)
              endif
            else
              go to 1160
            endif
1170        call CloseIfOpened(45)
            FileNameM45Old=FileNameM45
          endif
        else if(.not.ExistFile(FileNameM45)) then
          ScDist=-1.
          ScDistSt=' '
          FileNameJPG=' '
          FileNameM45Old=' '
        endif
        if(ScDist.gt.0.) then
          Veta='%Scaling distance '//ScDistSt(:idel(ScDistSt))
          call FeQuestEdwLabelChange(nEdwScDist,Veta)
          call FeQuestRealEdwOpen(nEdwScDist,ScDist,.false.,.false.)
        else
          call FeQuestEdwDisable(nEdwScDist)
        endif
        if(ExistFile(FileNameJPG)) then
          call FeQuestButtonOff(nButtShowModel)
        else
          call FeQuestButtonDisable(nButtShowModel)
        endif
      else
        call FeQuestEdwDisable(nEdwFileNameM45)
        call FeQuestButtonDisable(nButtBrowse)
        call FeQuestButtonDisable(nButtPreDefined)
        call FeQuestButtonDisable(nButtShowModel)
        call FeQuestRealFromEdw(nEdwScDist,ScDist)
        call FeQuestEdwDisable(nEdwScDist)
      endif
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        NewMolName=EdwStringQuest(nEdwName)
        call zhusti(NewMolName)
        call uprat(NewMolName)
        call atcheck(NewMolName,ichp,i)
        if(ichp.ne.0.or.NewMolName.eq.' ') then
          if(ichp.eq.1.or.NewMolName.eq.' ') then
            Veta='Unacceptable symbol in the name "'//
     1           NewMolName(:idel(NewMolName))//'"'
          else if(ichp.eq.2) then
            Veta='The name "'//NewMolName(:idel(NewMolName))//
     1           '" already exists'
          endif
          call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                  SeriousError)
          go to 1250
        endif
        if(NAtIndLenAll(KPhase).gt.0) k45=CrwLogicQuest(nCrwFromM45)
        if(k45) then
          FileNameM45=EdwStringQuest(nEdwFileNameM45)
          if(.not.ExistFile(FileNameM45)) then
            Veta='The model file "'//FileNameM45(:idel(FileNameM45))//
     1           '" doesn''t exist'
            call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                    SeriousError)
            go to 1250
          endif
        endif
        QuestCheck(id)=0
        go to 1200
1250    EventType=EventEdw
        EventNumber=nEdwName
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.eq.nCrwFromM45.or.CheckNumber.eq.nCrwFromM40))
     2  then
        k45=CrwLogicQuest(nCrwFromM45)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtBrowse.or.
     2         CheckNumber.eq.nButtPreDefined)) then
        if(CheckNumber.eq.nButtPreDefined) then
          CurrentDirO=CurrentDir
          i=FeChdir(HomeDir(:idel(HomeDir))//'Molecular patterns\')
          lpom=.false.
        else
          lpom=.true.
        endif
        call FeFileManager('Select model file',FileNameM45,'*.m45',0,
     1                     lpom,ich)
        if(ich.eq.0) then
          if(CheckNumber.eq.nButtPreDefined.and.
     1       index(FileNameM45,'\').le.0)
     2      FileNameM45=CurrentDir(:idel(CurrentDir))//
     3                  FileNameM45(:idel(FileNameM45))
          call FeQuestStringEdwOpen(nEdwFileNameM45,FileNameM45)
        endif
        EventType=EventEdw
        EventNumber=nEdwFileNameM45
        if(CheckNumber.eq.nButtPreDefined)
     1    i=FeChdir(CurrentDirO)
        go to 1150
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.eq.nEdwFileNameM45) then
        FileNameM45=EdwStringQuest(nEdwFileNameM45)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowModel) then
        call FeShellExecute(FileNameJPG)
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      if(ich.eq.0) then
        if(NComp(KPhase).gt.1) call FeQuestIntFromEdw(nEdwComp,isw)
        if(EdwStateQuest(nEdwScDist).eq.EdwOpened)
     1    call FeQuestRealFromEdw(nEdwScDist,ScDist)
        nap=NAtIndFr(isw,KPhase)
        nak=NAtIndTo(isw,KPhase)
        if(NMolec.ge.mxm) then
          if(NMolec.le.0) then
            if(allocated(iam)) deallocate(iam,mam)
            allocate(iam(1),mam(1))
            iam(1)=0
            mam(1)=1
            call AllocateMolecules(1,1)
          else
            call ReallocateMolecules(1,0)
          endif
        endif
        nm=NMolecTo(isw,KPhase)+1
        call MolSun(nm,NMolec,nm+1)
        do j=KPhase,NPhase
          if(j.eq.KPhase) then
            kp=isw
          else
            kp=1
          endif
          do k=kp,NComp(j)
            if(j.ne.KPhase.or.k.ne.kp) NMolecFr(k,j)=NMolecFr(k,j)+1
            NMolecTo(k,j)=NMolecTo(k,j)+1
          enddo
          if(j.ne.KPhase) NMolecFrAll(j)=NMolecFrAll(j)+1
          NMolecToAll(j)=NMolecToAll(j)+1
        enddo
        NMolecLen(isw,KPhase)=NMolecLen(isw,KPhase)+1
        NMolecLenAll(KPhase)=NMolecLenAll(KPhase)+1
        NMolec=NMolec+1
        MolName(nm)=NewMolName
        KTLS(nm)=0
        SmbPGMol(nm)='1'
        ISwMol(nm)=isw
        KSwMol(nm)=KPhase
        NPoint(nm)=1
        KPoint(nm)=1
        IPoint(1,nm)=1
        call UnitMat(RPoint(1,1,nm),3)
        call UnitMat(TrPG(1,nm),3)
        call UnitMat(TrIPG(1,nm),3)
        call SetRealArrayTo(SPoint(1,1,nm),3,0.)
        call srotb(RPoint(1,1,nm),RPoint(1,1,nm),TPoint(1,1,nm))
        UsePGSyst(nm)=.false.
        LocPGSystAx(nm)='xy'
        LocPGSystSt(1,nm)=' 1.000000 0.000000 0.000000'
        LocPGSystSt(2,nm)=' 0.000000 1.000000 0.000000'
        call SetRealArrayTo(LocPGSystX(1,1,nm),6,0.)
        LocPGSystX(1,1,nm)=1.
        LocPGSystX(2,2,nm)=1.
        nk=NAtMolTo(isw,KPhase)
        np=nk+1
      endif
      if(ich.ne.0) then
        call FeQuestRemove(WizardId)
        go to 9999
      endif
      if(k45) then
        ScDistSt=' '
        isfmx=0
        call OpenFile(45,FileNameM45,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        n=0
1400    read(45,FormA,end=1450) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'cell')) then
          n=n+1
          read(Veta(k+1:),*,err=9100) par
        else if(EqIgCase(Cislo,'pgroup')) then
          n=n+1
          call kus(Veta,k,SmbPGMol(nm))
          if(SmbPGMol(nm).ne.' ') then
            call GenPg(SmbPGMol(nm),rpoint(1,1,nm),npoint(nm),ich)
            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
            kpoint(nm)=npoint(nm)
            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
c            pa=0.
c            call SpecPg(pa,rpoint(1,1,nm),isw,npoint(i))
c            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
c            do k=2,n
c              do 2800l=1,npoint(i)
c                if(ipoint(l,i).le.0) cycle
c                call multm(rpgp(1,k),rpoint(1,l,i),px,3,3,3)
c                do m=2,npoint(i)
c                  if(ipoint(m,i).le.0) cycle
c                  if(eqrv(px,rpoint(1,m,i),9,.0001)) then
c                    ipoint(m,i)=0
c                    go to 2800
c                  endif
c                enddo
c2800          continue
c            enddo
c            k=0
c            do l=1,npoint(i)
c              if(ipoint(l,i).gt.0) k=k+1
c              call srotb(rpoint(1,l,i),rpoint(1,l,i),tpoint(1,l,i))
c              call multm(rpoint(1,l,i),xm(1,i),spoint(1,l,i),3,3,1)
c              do m=1,3
c                spoint(m,l,i)=xm(m,i)-spoint(m,l,i)
c              enddo
c            enddo
c            kpoint(i)=k
c            iam(i)=iam(i)*k
          endif
        else if(EqIgCase(Cislo,'scdist')) then
          n=n+1
          call kus(Veta,k,ScDistSt)
          call StToReal(Veta,k,pa,1,.false.,ichp)
          ScDistO=pa(1)
          if(ichp.ne.0) go to 9100
        endif
        if(n.lt.3) then
          go to 1400
        else
          go to 1460
        endif
1450    rewind 45
        read(45,*,err=9100,end=9200) par
1460    do i=4,6
          par(i)=cos(par(i)*torad)
        enddo
        sng=sqrt(1.-par(6)**2)
        volume=sqrt(1.-par(4)**2-par(5)**2-par(6)**2
     1              +2.*par(4)*par(5)*par(6))
        if(ScDistSt.eq.' ') then
          pom=1.
        else
          pom=ScDist/ScDistO
        endif
        pa(1)=par(1)*pom
        pa(2)=0.
        pa(3)=0.
        pa(4)=par(2)*par(6)*pom
        pa(5)=par(2)*sng*pom
        pa(6)=0.
        pa(7)=par(3)*par(5)*pom
        pa(8)=par(3)*pom*(par(4)-par(5)*par(6))/sng
        pa(9)=par(3)*pom*volume/sng
        call matinv(TrToOrtho(1,isw,KPhase),pb,pom,3)
        call multm(pb,pa,trl,3,3,3)
      else
        call SetLogicalArrayTo(AtBrat(nap),nak-nap+1,.false.)
        WizardMode=.false.
        call SelAtoms('Select atoms for the molecule',Atom(nap),
     1                AtBrat(nap),isf(nap),nak-nap+1,ich)
        WizardMode=.true.
        if(ich.ne.0) then
          call FeQuestRemove(WizardId)
          go to 9900
        endif
      endif
      i=nap
      iamp=0
      nas=0
      call ReallocateAtoms(0)
      AiMax=-999999.
2000  if(k45) then
        read(45,FormA,end=2100) Veta
        if(Veta.eq.' ') go to 2000
        read(Veta,'(a8,i3,7x,4f9.6,i3)') atm(1),isfp,aip,paa,ksymm
        if(ksymm.le.0) nas=nas+1
      else
        if(i.gt.nak) go to 2100
        if(isf(i).le.0.or..not.AtBrat(i)) then
          i=i+1
          go to 2000
        endif
      endif
      if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
      nk=nk+1
      call atsun(nk,NAtMol,nk+1)
      iamp=iamp+1
      NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+1
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      call SetBasicKeysForAtom(nk)
      if(k45) then
        Atom(nk)=atm(1)
        call multm(trl,paa,x(1,nk),3,3,1)
        beta(1,nk)=3.
        isfmx=max(isfmx,isfp)
        isf(nk)=-isfp-1000*ksymm
        ai(nk)=aip
      else
        Atom(nk)=Atom(i)
        iswa(nk)=isw
        kswa(nk)=KPhase
        kmol(nk)=nm
        isf(nk)=isf(i)
        isf(i)=-isf(i)
        itf(nk)=itf(i)
        itf(i)=-itf(i)
        ifr(nk)=ifr(i)
        call CopyVekI(KModA(1,i),KModA(1,nk),3)
        call CopyVekI(KFA(1,i),KFA(1,nk),3)
        if(NDimI(KPhase).gt.0)
     1    call CopyVek(qcnt(1,i),qcnt(1,nk),NDimI(KPhase))
        ai(nk)=ai(i)
        call CopyVek(x(1,i),x(1,nk),3)
        call CopyVek(beta(1,i),beta(1,nk),6)
        k=KModA(1,i)
        j=0
        if(k.gt.0) then
          a0(nk)=a0(i)
          call CopyVek(ax(1,i),ax(1,nk),k)
          call CopyVek(ay(1,i),ay(1,nk),k)
          call SetRealArrayTo(sax(1,nk),k,0.)
          call SetRealArrayTo(say(1,nk),k,0.)
          j=1
        endif
        k=3*KModA(2,i)
        if(k.gt.0) then
          call CopyVek(ux(1,1,i),ux(1,1,nk),k)
          call CopyVek(uy(1,1,i),uy(1,1,nk),k)
          call SetRealArrayTo(sux(1,1,nk),k,0.)
          call SetRealArrayTo(suy(1,1,nk),k,0.)
          j=1
        endif
        k=6*KModA(3,i)
        if(k.gt.0) then
          call CopyVek(bx(1,1,i),bx(1,1,nk),k)
          call CopyVek(by(1,1,i),by(1,1,nk),k)
          call SetRealArrayTo(sbx(1,1,nk),k,0.)
          call SetRealArrayTo(sby(1,1,nk),k,0.)
          j=1
        endif
        if(j.gt.0) then
          phf(nk)=phf(i)
          sphf(nk)=sphf(i)
        endif
      endif
      AiMax=max(AiMax,ai(nk))
      if(nk.eq.NAtMolFr(1,1)) then
        if(NAtCalc.eq.0) then
          PrvniKiAtomu(nk)=ndoff
        else
          PrvniKiAtomu(nk)=PrvniKiAtomu(NAtCalc)+
     1                     DelkaKiAtomu(NAtCalc)
        endif
      else
        PrvniKiAtomu(nk)=PrvniKiAtomu(nk-1)+DelkaKiAtomu(nk-1)
      endif
      DelkaKiAtomu(nk)=0
      i=i+1
      go to 2000
2100  if(iamp.le.0) then
        Veta='The atom selection failed, no atoms for the new '//
     1       'molecule read in.'
        call FeQuestRemove(WizardId)
        go to 9900
      endif
      iampp=iamp
      if(k45) then
        iamp=kpoint(nm)*nas
        if(NAtAll+iamp-iampp.ge.MxAtAll)
     1    call ReallocateAtoms(NAtAll+iamp-iampp)
        do i=nk+1,nk+iamp-iampp
          NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+1
          NAtCalcBasic=NAtCalc
          call EM40UpdateAtomLimits
          call SetBasicKeysForAtom(i)
          write(Cislo,'(i5)') i
          call Zhusti(Cislo)
          Atom(i)='#nic'//Cislo(:idel(Cislo))
        enddo
      else

      endif
      if(NAtAll+iamp.ge.MxAtAll) call ReallocateAtoms(NAtAll+iamp)
      call atsun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+iamp)
      np=np+iamp
      nk=nk+iamp
      NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+iamp
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      if(k45) then
        id=NextQuestId()
        Veta='Specify atomic types in the model molecule:'
        call FeQuestTitleMake(id,Veta)
        il=0
        xpom=5.
        dpom=50.
        tpom=xpom+dpom+10.
        do i=1,isfmx
          il=il+1
          Veta=' '
          k=1
          t80=' '
          do j=np,nk
            if(-mod(-isf(j),1000).eq.-i) then
              Veta(k:)=Atom(j)
              k=idel(Veta)+2
              if(t80.eq.' ') then
                do l=1,idel(Atom(j))
                  if(index(Cifry(1:10),Atom(j)(l:l)).le.0)
     1              t80=t80(:idel(t80))//Atom(j)(l:l)
                enddo
              endif
              if(k.gt.50) then
                Veta(k:)=' ... '
                exit
              endif
            endif
          enddo
          idl=min(idel(t80),2)
          do j=idl,1,-1
            k=LocateInStringArray(AtTypeMenu,NAtFormula(KPhase),t80(:j),
     1                            IgnoreCaseYes)
            if(k.gt.0) exit
          enddo
          if(k.le.0) k=1
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,0)
          call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                            NAtFormula(KPhase),k)
          if(i.eq.1) nRolMenuAtType=RolMenuLastMade
        enddo
2500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        nRolMenu=nRolMenuAtType
        if(ich.gt.0) then
          go to 1100
        else if(ich.lt.0) then
          call FeQuestRemove(WizardId)
          go to 9900
        endif
        do i=1,isfmx
          isfp=RolMenuSelected(nRolMenu)
          do j=np,nk
            if(isf(j).eq.-i) isf(j)=isfp
          enddo
          nRolMenu=nRolMenu+1
        enddo
      endif
      iam(nm)=iamp
      do i=np,nk
        ai(i)=ai(i)/AiMax
      enddo
      mamp=0
      id=NextQuestId()
      call FeQuestTitleMake(id,'Define the molecular reference point:')
      QuestCheck(id)=1
      il=1
      spom=90.
      tpom=WizardLength*.5-spom
      do i=1,3
        if(i.eq.1) then
          Veta='%Explicit'
        else if(i.eq.2) then
          Veta='%Gravity center'
        else
          Veta='Ge%om. center'
        endif
        xpom=tpom-CrwgXd*.5
        call FeQuestCrwMake(id,tpom,il,xpom,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,3)
        if(i.eq.1) then
          nCrwCenterFirst=CrwLastMade
        else if(i.eq.3) then
          nCrwCenterLast=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        tpom=tpom+spom
      enddo
      il=il+1
      tpom=5.
      Veta='%Reference point'
      xpom=tpom+FeTxLength(Veta)+15.
      dpom=150.
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRef=EdwLastMade
      call FeQuestStringEdwOpen(nEdwRef,' ')
      xpom=xpom+dpom+15.
      Veta='%Select from the list'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtList=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2600  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=nCrwCenterFirst,nCrwCenterLast
          if(CrwLogicQuest(i)) then
            RefPoint(nm)=i-nCrwCenterFirst
            exit
          endif
        enddo
        if(RefPoint(nm).le.0) then
          StRefPointP=EdwStringQuest(nEdwRef)
          if(StRefPointP.eq.' ') then
            Veta='Please fill the reference point'
            go to 2650
          endif
          k=0
          call StToReal(StRefPointP,k,xm(1,nm),3,.false.,ichp)
          if(ichp.eq.0) then
            StRefPointP=' '
          else
            call zhusti(StRefPointP)
            call uprat(StRefPointP)
            k=ktat(Atom(np),iamp,StRefPointP)
            if(k.le.0) then
              if(index(Cifry,StRefPointP(1:1)).gt.0) then
                Veta='in the numerical string defining the reference '//
     1               'point'
              else
                Veta='The atom defining the reference point is n''t '//
     1               'an atom of the molecule'
              endif
              go to 2650
            endif
            k=k+np-1
            do i=1,3
              xm(i,nm)=x(i,k)
            enddo
          endif
          StRefPoint(nm)=StRefPointP
        else
          suma=0.
          call SetRealArrayTo(xm(1,nm),3,0.)
          do i=np,nk
            if(RefPoint(nm).eq.1) then
              pom=AtWeight(mod(isf(i),1000),KPhase)
            else
              pom=1.
            endif
            do j=1,3
              xm(j,nm)=xm(j,nm)+pom*x(j,i)
            enddo
            suma=suma+pom
          enddo
          do i=1,3
            xm(i,nm)=xm(i,nm)/suma
          enddo
          StRefPoint(nm)=' '
        endif
        QuestCheck(id)=0
        go to 2600
2650    call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                SeriousError)
        EventType=EventEdw
        EventNumber=nEdwRef
        go to 2600
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwCenterFirst.and.
     2        CheckNumber.le.nCrwCenterLast) then
        i=EdwStateQuest(nEdwRef)
        if(EdwStateQuest(nEdwRef).ne.EdwOpened.and.
     1    CheckNumber.eq.nCrwCenterFirst) then
          call FeQuestStringEdwOpen(nEdwRef,' ')
          call FeQuestButtonOpen(nButtList,ButtonOff)
          EventType=EventEdw
          EventNumber=nEdwRef
        else if(CheckNumber.ne.nCrwCenterFirst) then
          call FeQuestEdwDisable(nEdwRef)
          call FeQuestButtonDisable(nButtList)
        endif
        go to 2600
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtList)
     1  then
        WizardMode=.false.
        call SelOneAtom('Select the reference atom:',Atom(np),ia,iampp,
     1                  ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwRef,Atom(np+ia-1))
        WizardMode=.true.
        go to 2600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2600
      endif
      if(ich.eq.0) then
        if(NDimI(KPhase).gt.0) then
          call qbyx(xm,qcmol,isw)
          do i=np,nk
            kk=0
            do k=1,KModA(1,i)
              if(k.gt.KModA(1,i)-NDimI(KPhase).and.KFA(1,i).ne.0) then
                kk=kk+1
                ax(k,i)=ax(k,i)+qcmol(kk)-qcnt(kk,i)
              else
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                xpp=ax(k,i)
                ypp=ay(k,i)
                ax(k,i)= xpp*cosfik+ypp*sinfik
                ay(k,i)=-xpp*sinfik+ypp*cosfik
              endif
            enddo
            do k=1,KModA(2,i)
              if(k.eq.KModA(2,i).and.KFA(2,i).ne.0) then
                uy(1,k,i)=uy(1,k,i)+qcmol(1)-qcnt(1,i)
              else
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,3
                  xpp=ux(l,k,i)
                  ypp=uy(l,k,i)
                  ux(l,k,i)= xpp*cosfik+ypp*sinfik
                  uy(l,k,i)=-xpp*sinfik+ypp*cosfik
                enddo
              endif
            enddo
            do k=1,KModA(3,i)
              fik=0.
              do m=1,NDimI(KPhase)
                fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
              enddo
              sinfik=sin(pi2*fik)
              cosfik=cos(pi2*fik)
              do l=1,6
                xpp=bx(l,k,i)
                ypp=by(l,k,i)
                bx(l,k,i)= xpp*cosfik+ypp*sinfik
                by(l,k,i)=-xpp*sinfik+ypp*cosfik
              enddo
            enddo
          enddo
        endif
      else if(ich.gt.0) then
        go to 1100
      else if(ich.lt.0) then
        call FeQuestRemove(WizardId)
        go to 9900
      endif
      call FeQuestRemove(WizardId)
      go to 3500
      entry EM40NewMolPos(ich)
      allocate(xp(3,3000),ip(3000))
      k45=.false.
      if(NMolec.gt.1) then
        id=NextQuestId()
        xqd=300.
        il=NMolec
        Veta='Select the molecule for adding a new position'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        xpom=5.
        tpom=xpom+CrwgXd+15.
        do il=1,NMolec
          call FeQuestCrwMake(id,tpom,il,xpom,il,MolName(il),'L',
     1                        CrwgXd,CrwgYd,0,1)
          call FeQuestCrwOpen(CrwLastMade,il.eq.1)
          if(il.eq.1) nCrwFirst=CrwLastMade
        enddo
3100    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 3100
        endif
        if(ich.eq.0) then
          do nm=1,NMolec
            if(CrwLogicQuest(nm)) exit
          enddo
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      else
        nm=1
      endif
      isw=iswmol(nm)
      mamp=mam(nm)
      iampp=iam(nm)
      np=NAtMolFr(1,1)
      do i=1,nm-1
        np=np+iam(i)
      enddo
      nk=np+iampp-1
      AiMax=-999999.
      do i=np,nk
        AiMax=max(AiMax,ai(i))
      enddo
      nap=NAtIndFr(isw,KPhase)
      nak=NAtIndTo(isw,KPhase)
3500  NAtActual=nak-nap+1
      allocate(AtActual(NAtActual),IAtActual(NAtActual))
      n=0
      do i=nap,nak
        n=n+1
         AtActual(n)=Atom(i)
        IAtActual(n)=i
      enddo
      mampp=mamp+1
      ji=mxp*(nm-1)+mamp
4000  Znovu=.true.
      mamp=mamp+1
      if(mamp.gt.mxp) then
        call ReallocateMolecules(0,1)
        ji=mxp*(nm-1)+mamp-1
        call ReallocateAtoms(iam(nm))
      endif
      if(mamp.eq.mampp) then
        call SetStringArrayTo(AtM,3,' ')
        call SetStringArrayTo(AtP,3,' ')
        call SetIntArrayTo(Model,3,0)
        call SetIntArrayTo(Actual,3,0)
      endif
      ji=ji+1
      call UnitMat(RotMol (1,ji),3)
      call UnitMat(RotIMol(1,ji),3)
      RotSign(ji)=1
       aimol(ji)=AiMax
       AiPom=AiMax
      saimol(ji)=0.
       a0m(ji)=1.
      sa0m(ji)=0.
      LocMolSystType(ji)=0
      TypeModFunMol(ji)=0
      call SetRealArrayTo(DRotF(1,ji),9,0.)
      call SetRealArrayTo(DRotC(1,ji),9,0.)
      call SetRealArrayTo(DRotP(1,ji),9,0.)
      call SetRealArrayTo(RotB(1,ji),36,0.)
      call SetRealArrayTo(DRotBF(1,ji),36,0.)
      call SetRealArrayTo(DRotBC(1,ji),36,0.)
      call SetRealArrayTo(DRotBP(1,ji),36,0.)
      call SetStringArrayTo(LocMolSystAx(1,ji),2,' ')
      call SetStringArrayTo(LocMolSystSt(1,1,ji),4,' ')
      call SetRealArrayTo(LocMolSystX(1,1,1,ji),12,0.)
      AtTrans(ji)=' '
      call CopyMat(TrToOrtho(1,isw,KPhase),TrMol(1,ji),3)
      call CopyMat(TrToOrtho(1,isw,KPhase),trp,3)
      call matinv(trp,TriMol(1,ji),pom,3)
      if(KTLS(nm).gt.0) then
        call SetRealArrayTo(tt(1,ji),6,0.)
        call SetRealArrayTo(stt(1,ji),6,0.)
        call SetRealArrayTo(tl(1,ji),6,0.)
        call SetRealArrayTo(stl(1,ji),6,0.)
        call SetRealArrayTo(ts(1,ji),9,0.)
        call SetRealArrayTo(sts(1,ji),9,0.)
      endif
      if(NDimI(KPhase).gt.0) then
        phfm(ji)=0.
        sphfm(ji)=0.
        OrthoX40Mol(ji)=0.
        OrthoDeltaMol(ji)=0.
        OrthoEpsMol(ji)=0.
      endif
      write(PositionName,'('' molecular position #'',i2)') mamp
      if(mamp.eq.1.and..not.k45) then
        call SetRealArrayTo(euler(1,ji),3,0.)
        call SetRealArrayTo(trans(1,ji),3,0.)
      endif
      id=NextQuestId()
      xqd=400.
      if(mamp.gt.1.or.k45) then
        Veta='Define and complete'
      else
        Veta='Complete'
      endif
      Veta=Veta(:idel(Veta))//PositionName(:idel(PositionName))//':'
      if(mamp.gt.1.or.k45) then
        il=17
      else
        il=5
      endif
      call FeQuestCreate(Id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=1
      if(mamp.gt.1.or.k45) then
        xpom=5.
        tpom=xpom+CrwgXd+15.
        Veta='Apply %inversion'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,RotSign(ji).ne.1)
        nCrwInversion=CrwLastMade
        il=il+1
        call FeQuestLblMake(id,110.,il,'Model atom','C','N')
        call FeQuestLblMake(id,240.,il,'Actual position/atom','C','N')
        tpom=5.
        xpom1=tpom+FeTxLength('XXXXXXXXX')+10
        dpom1=100.
        xpom2=xpom1+dpom1+10.
        dpom2=200.
        do i=1,3
          write(Veta,'(i1,a2,'' point'')') i,nty(i)
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom1,il,Veta,'L',dpom1,EdwYd,
     1                      1)
          if(i.eq.1) nEdwFirstPoint=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,AtM(i))
          call FeQuestEdwMake(id,tpom,il,xpom2,il,' ','L',dpom2,EdwYd,1)
          call FeQuestStringEdwOpen(EdwLastMade,AtP(i))
        enddo
        nEdwLastPoint=EdwLastMade
        il=il+1
        Veta='%Select the model atom'
        dpom=FeTxLengthUnder(Veta)+15.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtSelect=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Fill by a saved %point'
        dpom=FeTxLengthUnder(Veta)+15.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtPoint=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Options for removing of atoms from atomic block'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        il=-10*il-7
        Veta='coinciding with those generated from'//
     1       PositionName(:idel(PositionName))//':'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        Veta='Ma%ximal coincidence distance'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=80.
        il=il-10
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwMaxCoinc=EdwLastMade
        call FeQuestRealEdwOpen(nEdwMaxCoinc,dco,.false.,.false.)
        xpom=xpom+dpom+15.
        Veta='Sho%w coinciding atoms'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtShowCoinc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il-10
        Veta='%Calculate molecular parameters'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il-10
        call FeQuestLinkaMake(id,il)
        il=il-10
        ncoinc=0
        call SetRealArrayTo(AiModel,3,0.)
        call SetRealArrayTo(AiActual,3,0.)
      else
        nEdwFirstPoint=0
        nEdwLastPoint=0
        nCrwInversion=0
        nButtCalc=0
        nButtSelect=0
        nEdwMaxCoinc=0
        nButtShowCoinc=0
        il=-10*il
        ncoinc=iamp
      endif
      tpom=5.
      Veta='Molecular parameters:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      nLblMolPar=LblLastMade
      if(mamp.gt.1.or.k45) call FeQuestLblOff(LblLastMade)
      il=il-7
      Veta=' '
      if(mamp.eq.1.and..not.k45)
     1  write(Veta,102)(euler(i,ji),i=1,3),RotSign(ji)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblRot=LblLastMade
      if(mamp.eq.1.and..not.k45)
     1  write(Veta,103)(trans(i,ji),i=1,3)
      il=il-7
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblTrans=LblLastMade
      tpom=5.
      dpom=80.
      il=il-10
      Veta='%Occupancy'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOcc=EdwLastMade
      if(mamp.eq.1.and..not.k45) then
        call FeQuestRealEdwOpen(nEdwOcc,aimol(ji),.false.,.false.)
        AiMolPom=aimol(ji)
      endif
      xpom=xpom+dpom+30.
      Veta='Coincidence ratio:'
      write(Cislo,104) ncoinc,iampp
      call Zhusti(Cislo)
      Veta(idel(Veta)+2:)=Cislo
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      if(mamp.gt.1.or.k45) call FeQuestLblOff(LblLastMade)
      nLblCoinc=LblLastMade
      il=il-15
      Veta='%Apply+Next position'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=dpom+10.
      xpom=xqd*.5-dpom*1.5-10.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtApplyNext=ButtonLastMade
          Veta='Appl%y+End'
        else if(i.eq.2) then
          Veta='%Quit'
          nButtApplyEnd=ButtonLastMade
        else if(i.eq.3) then
          nButtQuit=ButtonLastMade
        endif
        xpom=xpom+pom
      enddo
4100  call FeQuestEventWithCheck(id,ich,EM40NewMolUpdateQuest,FeVoid)
      Znovu=.false.
      iw=nEdwSelect-nEdwFirstPoint+1
      iwp=(iw-1)/2+1
      iwm=mod(iw-1,2)+1
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit.or.
     1   CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
        Konec=3
        go to 4350
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtApplyNext.or.
     2         CheckNumber.eq.nButtApplyEnd)) then
        if(CheckNumber.eq.nButtApplyNext) then
          Konec=1
        else
          Konec=2
        endif
        if(mamp.gt.1.or.k45) then
          do i=nEdwFirstPoint,nEdwLastPoint
            if(EdwStringQuest(i).eq.' ') then
              call FeChybne(-1.,-1.,'Information isn''t complete.',' ',
     1                      SeriousError)
              EventType=EventEdw
              EventNumber=i
              go to 4100
            endif
          enddo
          ShowCoinc=.false.
          go to 4200
        else
          go to 4300
        endif
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.ge.nEdwFirstPoint.and.
     2        CheckNumber.le.nEdwLastPoint) then
        if(iwm.eq.1) then
          AtM(iwp)=EdwStringQuest(CheckNumber)
          if(EventTypeSave.eq.EventButton.and.
     2       (EventNumberSave.eq.nButtSelect.or.
     3         EventNumberSave.eq.nButtPoint)) go to 4100
          if(AtM(iwp).ne.' ') then
            call zhusti(AtM(iwp))
            call uprat(AtM(iwp))
            Model(iwp)=ktat(Atom(np),iampp,AtM(iwp))+np-1
            AiModel(iwp)=Ai(Model(iwp))
            ichp=0
            if(Model(iwp).le.np-1) then
              call FeChybne(-1.,-1.,'Atom "'//AtM(iwp)(:idel(AtM(iwp)))
     1           //'" doesn''t exist in the molecule, try again.',' ',
     2             SeriousError)
              ichp=1
            endif
            if(ichp.eq.0) then
              do i=1,3
                if(Model(iwp).eq.Model(i).and.iwp.ne.i) then
                  call FeChybne(-1.,-1.,'duplicite occurence, '//
     1                          'try again.',' ',SeriousError)
                  ichp=1
                  exit
                endif
              enddo
            endif
          else
            Model(iwp)=0
          endif
        else
          Veta=EdwStringQuest(CheckNumber)
          if(EventTypeSave.eq.EventButton.and.
     1       (EventNumberSave.eq.nButtSelect.or.
     2        EventNumberSave.eq.nButtPoint)) go to 4100
          if(Veta.ne.' ') then
            if(Veta(1:1).ne.'%') then
              i=NAtCalc
              NAtCalc=NAtInd
              pom=YBottomMessage
              YBottomMessage=-1.
              call CtiAt(Veta,xx(1,iwp),ichp)
              YBottomMessage=pom
              NAtCalc=i
              if(ichp.eq.-1) then
                AtP(iwp)=Veta
                i=index(Veta,'#')-1
                if(i.lt.0) i=idel(Veta)
                k=KtAt(AtActual,NAtActual,Veta(:i))
                if(k.gt.0) then
                  AiActual(iwp)=ai(IAtActual(k))
                  Actual(iwp)=1
                endif
                ichp=0
              else if(ichp.eq.0) then
                AtP(iwp)=' '
                AiActual(iwp)=1.
                Actual(iwp)=1
              endif
            else
              ichp=1
              do i=1,NSavedPoints
                if(EqIgCase(StSavedPoint(i),Veta(2:))) then
                  AtP(iwp)=Veta
                  call CopyVek(XSavedPoint(1,i),xx(1,iwp),3)
                  AiActual(iwp)=1.
                  Actual(iwp)=1
                  ichp=0
                endif
              enddo
              if(ichp.ne.0) then
                Veta='the saved point with label "'//Veta(2:idel(Veta))
     1             //'" is not present, try again.'
                call FeChybne(-1.,-1.,Veta,' ',SeriousError)
              endif
            endif
            if(ichp.eq.0) then
              do i=1,3
                if(Actual(i).le.0) cycle
                if(iwp.ne.i.and.(EqIgCase(Veta,AtP(i)).or.
     1                           EqRV(xx(1,iwp),xx(1,i),3,.00001))) then
                  call FeChybne(-1.,-1.,'duplicite occurence, '//
     1                          'try again.',' ',SeriousError)
                  ichp=1
                  exit
                endif
              enddo
            endif
          else
            Actual(iwp)=0
            AtP(iwp)=' '
          endif
        endif
        if(ichp.ne.0) then
          AtP(iwp)=' '
          EventType=EventEdw
          EventNumber=iw
          ichp=0
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelect)
     1  then
        if(iwm.eq.1) then
4120      call SelOneAtom('Select the model atom:',Atom(np),ia,iampp,
     1                    ichp)
          ia=np+ia-1
          if(ichp.eq.0) then
            do i=1,3
              if(i.eq.iwp) cycle
              if(ia.eq.Model(i)) then
                call FeChybne(-1.,-1.,'duplicite occurence, try again.',
     1                        ' ',SeriousError)
                go to 4120
              endif
            enddo
            AtM(iwp)=Atom(ia)
            call FeQuestStringEdwOpen(nEdwSelect,AtM(iwp))
            Model(iwp)=ia
            AiModel(iwp)=Ai(ia)
          endif
        else
4140      call SelOneAtom('Select the actual atom:',AtActual,ia,
     1                    NAtActual,ichp)
          ia=IAtActual(ia)
          AiActual(iwp)=ai(ia)
          if(ichp.eq.0) then
            do i=1,3
              if(i.eq.iwp) cycle
              if(EqIgCase(Atom(ia),AtP(i))) then
                call FeChybne(-1.,-1.,'duplicite occurence, try again.',
     1                        ' ',SeriousError)
                go to 4140
              endif
            enddo
            AtP(iwp)=Atom(ia)
            call ctiat(AtP(iwp),xx(1,iwp),ichp)
            Actual(iwp)=1
            call FeQuestStringEdwOpen(nEdwSelect,Atom(ia))
          endif
        endif
        EventType=EventEdw
        if(iw.lt.5) then
          EventNumber=iw+nEdwFirstPoint+1
        else if(iw.eq.5) then
          EventNumber=nEdwFirstPoint+1
        else
          EventNumber=nEdwMaxCoinc
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPoint)
     1  then
        if(iwm.ne.1) then
          write(Cislo,'(i1,a2)') iwp,nty(iwp)
          call SelSavedPoint('Select the '//Cislo(:3)//
     1                       ' actual position:',nsel,paa)
          if(nsel.gt.0) then
            call CopyVek(paa,xx(1,iwp),3)
            Actual(iwp)=1
            AtP(iwp)='%'//StSavedPoint(nsel)(:idel(StSavedPoint(nsel)))
            call FeQuestStringEdwOpen(nEdwSelect,AtP(iwp))
          endif
        endif
        EventType=EventEdw
        if(iw.lt.5) then
          EventNumber=iw+nEdwFirstPoint+1
        else if(iw.eq.5) then
          EventNumber=nEdwFirstPoint+1
        else
          EventNumber=nEdwMaxCoinc
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCalc)
     1  then
        ShowCoinc=.false.
        Konec=0
        go to 4200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowCoinc) then
        ShowCoinc=.true.
        Konec=0
        go to 4200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 4100
      endif
      go to 4300
4200  if(CrwLogicQuest(nCrwInversion)) then
        RotSign(ji)=-1
      else
        RotSign(ji)= 1
      endif
      do i=1,3
        k=model(i)
        if(i.eq.1) it=k-np+1
        call multm(TrMol(1,ji),x(1,k),xo(1,i),3,3,1)
        if(RotSign(ji).lt.0)
     1    call RealVectorToOpposite(xo(1,i),xo(1,i),3)
        call multm(trp,xx(1,i),xxo(1,i),3,3,1)
      enddo
      do i=1,3
        paa(i)=xxo(i,2)-xxo(i,1)
        pbb(i)=xxo(i,3)-xxo(i,1)
        paav(i)=xo(i,2)-xo(i,1)
        pbbv(i)=xo(i,3)-xo(i,1)
      enddo
      call VecOrtNorm(paa ,3)
      call VecOrtNorm(paav,3)
      do i=1,3
        pa(i)=paa(i)
        pb(i)=paav(i)
      enddo
      fs=-scalmul(paa,pbb)
      fsv=-scalmul(paav,pbbv)
      do i=1,3
        pbb(i)=pbb(i)+fs*paa(i)
        pbbv(i)=pbbv(i)+fsv*paav(i)
      enddo
      call VecOrtNorm(pbb ,3)
      call VecOrtNorm(pbbv,3)
      do i=1,3
        pa(i+3)=pbb(i)
        pb(i+3)=pbbv(i)
      enddo
      call vecmul(paa,pbb,pa(7))
      call VecOrtNorm(pa(7),3)
      call vecmul(paav,pbbv,pb(7))
      call VecOrtNorm(pb(7),3)
      call matinv(pb,pc,pom,3)
      call multm(pa,pc,RotMol(1,ji),3,3,3)
      call VecOrtNorm(RotMol(1,ji),3)
      call VecOrtNorm(RotMol(4,ji),3)
      call VecOrtNorm(RotMol(7,ji),3)
      call EM40GetAngles(RotMol(1,ji),irot(KPhase),euler(1,ji))
      k=0
      do i=np,nk
        k=k+1
        do j=1,3
          pbb(j)=x(j,i)-xm(j,nm)
        enddo
        call multm(TrMol(1,ji),pbb,paa,3,3,1)
        call multm(RotMol(1,ji),paa,pbb,3,3,1)
        call multm(TriMol(1,ji),pbb,paa,3,3,1)
        do j=1,3
          if(RotSign(ji).lt.0) paa(j)=-paa(j)
          xp(j,k)=paa(j)+xm(j,nm)
        enddo
      enddo
      do i=1,3
        trans(i,ji)=xx(i,1)-xp(i,it)
      enddo
      call FeQuestLblOn(nLblMolPar)
      write(Veta,102)(euler(i,ji),i=1,3),RotSign(ji)
      call FeQuestLblChange(nLblRot,Veta)
      write(Veta,103)(trans(i,ji),i=1,3)
      call FeQuestLblChange(nLblTrans,Veta)
      call FeQuestRealFromEdw(nEdwMaxCoinc,dco)
      k=0
      Ninfo=1
      TextInfo(1)=' '
      TextInfo(1)(23:)='Individual atomic positions'
      TextInfo(1)(57:)='distance'
      TextInfo(1)(69:)='to'
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      xpom=55.
      do i=1,4
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        if(i.lt.3) then
          xpom=xpom+55.
        else if(i.eq.3) then
          xpom=xpom+60.
        else
          xpom=xpom+30.
        endif
      enddo
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      ncoinc=0
      do i=np,nk
        k=k+1
        ip(k)=0
        do j=1,3
          xp(j,k)=xp(j,k)+trans(j,ji)
        enddo
        Ninfo=Ninfo+1
        write(Veta,'(a8,3(a1,f10.6))') Atom(i),(Tabulator,xp(j,k),j=1,3)
        call Zhusti(Veta)
        if(mamp.ne.1.or.k45) then
          j=koinc(xp(1,k),x,nap,nak,dco,dst,isw,iswa)
          jp=0
          if(j.gt.0) then
            if(isf(j).ne.0) then
              jp=j
              dstp=dst
            endif
          endif
4250      if(j.ne.0) then
            if(isf(j).gt.0) then
              if(ai(i).gt.0.) then
                isf(j)=-isf(j)-100
                ip(k)=j
              endif
            else
              if(j.lt.nak) then
                j=koinc(xp(1,k),x,j+1,nak,dco,dst,isw,iswa)
                go to 4250
              endif
            endif
            write(Veta(idel(Veta)+1:),105)
     1            Tabulator,dst,Tabulator,Atom(j)
            call Zhusti(Veta)
            ncoinc=ncoinc+1
          else
            if(jp.gt.0) then
              write(Veta(idel(Veta)+1:),105)
     1              Tabulator,dstp,Tabulator,Atom(jp)
              call Zhusti(Veta)
              ip(k)=jp
              ncoinc=ncoinc+1
            else
              Veta(idel(Veta)+1:)='     ----------------'
            endif
          endif
          TextInfo(Ninfo)=Veta
        endif
        if(Ninfo.ge.15) then
          if(Ninfo.gt.1.and.ShowCoinc) call FeInfoOut(-1.,-1.,' ','L')
          Ninfo=1
        endif
      enddo
      if(Ninfo.gt.1.and.ShowCoinc) call FeInfoOut(-1.,-1.,' ','L')
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      Veta='Coincidence ratio:'
      write(Cislo,104) ncoinc,iampp
      call Zhusti(Cislo)
      Veta(idel(Veta)+2:)=Cislo
      call FeQuestLblChange(nLblCoinc,Veta)
      if(EdwStateQuest(nEdwOcc).ne.EdwOpened) then
        AiMol(ji)=1.
        do i=1,3
          if(AiModel(i).ne.0.) then
            if(AiActual(i).ne.0.) then
              if(k45) then
                pa=0.
                call SpecPos(x(1,IAtActual(i)),pa,0,isw,.01,nocc)
                AiMol(ji)=AiActual(i)*float(nocc)
              else
                AiMol(ji)=AiActual(i)/AiModel(i)
              endif
              exit
            endif
          endif
        enddo
        AiPom=AiMol(ji)
        AiMol(ji)=AiMol(ji)*AiMax
        call FeQuestRealEdwOpen(nEdwOcc,aimol(ji),.false.,.false.)
      endif
      if(Konec.le.0) then
        k=0
        do i=np,nk
          k=k+1
          if(ip(k).gt.0) isf(ip(k))=-isf(ip(k))-100
        enddo
        go to 4100
      endif
4300  if(ich.eq.0) then
        if(Konec.ne.3) then
          call FeQuestRealFromEdw(nEdwOcc,aimol(ji))
          AiMax=aimol(ji)/aipom
        endif
      endif
4350  call FeQuestRemove(id)
      if(Konec.eq.3) then
        mamp=mamp-1
        if(mamp.ge.mampp) then
          Veta='Do you want discard'
          if(mamp.eq.mampp) then
            Veta(idel(Veta)+1:)=' new molecular position?'
          else
            Veta(idel(Veta)+1:)=' all new molecular positions?'
          endif
          if(FeYesNo(-1.,-1.,Veta,0)) then
            call iom40(0,0,fln(:ifln)//'.m40')
            go to 9999
          else
            go to 5000
          endif
        else
          call iom40(0,0,fln(:ifln)//'.m40')
          go to 9999
        endif
      endif
      call SetRealArrayTo(seuler(1,ji),3,0.)
      call SetRealArrayTo(strans(1,ji),3,0.)
      call SetIntArrayTo(KModM(1,ji),3,0)
      call SetIntArrayTo(KFM(1,ji),3,0)
      mam(nm)=mamp
      if(ji.eq.1) then
        PrvniKiMolekuly(ji)=1
      else
        if(mod(ji,mxp).eq.1) then
          jim=ji-mxp+mam(nm-1)-1
        else
          jim=ji-1
        endif
        PrvniKiMolekuly(ji)=PrvniKiMolekuly(jim)+DelkaKiMolekuly(jim)
      endif
      DelkaKiMolekuly(ji)=0
      call ShiftKiMol(ji,ktls(nm),KModM(1,ji),KModM(2,ji),
     1                KModM(3,ji),.true.)
      call SetIntArrayTo(KiMol(1,ji),DelkaKiMolekuly(ji),0)
      do i=np,nk
        call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),KModA(1,i),MagPar(i),
     1                 .true.)
      enddo
      do i=nap,nak
        if(isf(i).lt.-100) isf(i)=isf(i)+100
      enddo
      if(Konec.eq.1) then
        Konec=0
        go to 4000
      endif
5000  if(mamp.le.0) go to 9900
      if(k45) then
        j=np-1
        do i=np,nk
          if(isf(i).gt.0) then
            j=j+1
            if(i.ne.j) call AtCopy(i,j)
          endif
        enddo
      endif
      k=0
      nma=NAtMolFr(1,1)
      do i=nak,nap,-1
        if(isf(i).lt.0) then
          do j=1,mam(nm)
            ji=(nm-1)*mxp+j
            if(AtTrans(ji).eq.Atom(i)) AtTrans(ji)=' '
          enddo
!   Skrtani atomu je sporne. Podminky, ktere platin pro puvodni atomy
!   mohou mit vyznam i pro molekuly. Napr. reference point. Sktrnout by
!   se mely jen ty co delaji restrikce mezi atomy molekuly a
!   individualnimi atomy.
          if(itf(i).gt.0) call CrlAtomNamesSave(Atom(i),'#delete#',1)
          call atsun(i+1,NAtInd-k,i)
          call atsun(nma,NAtAll-k,nma-1)
          nma=nma-1
          k=k+1
        endif
      enddo
      NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)-k
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      call CrlAtomNamesApply
      call CrlAtomNamesIni
      ich=0
      call DeleteFile(fln(:ifln)//'_tmp.cif')
      go to 9999
9100  call FeReadError(45)
      go to 9800
9200  call FeChybne(-1.,YBottomMessage,'the file M45 doesn''t contain'//
     1              ' any atom.',' ',SeriousError)
9800  call CloseIfOpened(45)
      call FeQuestRemove(WizardId)
9900  ich=1
9999  if(allocated(xp)) deallocate(xp,ip)
      if(allocated(AtActual)) deallocate(AtActual,IAtActual)
      return
100   format(i2)
101   format(3f9.6)
102   format('Phi =',f8.2,' Chi =',f8.2,' Psi =',f8.2,' determinant =',
     1       i2)
103   format('Translation vector : ',3f10.6)
104   format(i5,'/',i5)
105   format(a1,f8.3,a1,a8)
      end
