      subroutine EM40EditTwVols(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(3),xpoma(3)
      character*80 Veta
      character*12 at,pn
      integer TwShow,UseTabsIn
      logical lpom,Zpet
      KDatBlockIn=KDatBlock
1040  xqd=450.
      il=3+(itwph-2)/3
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      if(isPowder) then
        Veta='Edit phase fractions'
      else
        Veta='Edit twin fractions'
      endif
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
        il=il+1
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      Zpet=.false.
      il=1
      kip=mxscu+1
      j=1
      do i=2,itwph
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiS(kip,KDatBlock),
     1                       .false.)
        EdwCheck(nEdw+EdwFr-1)=1
        if(i.eq.2) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          if(i.ne.itwph) il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      il=-10*(il+1)-3
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il-10
      Veta='Show %twinning matrix'
      dpom=FeTxLength(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtShowTwinMatrix=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=il-10
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=2,itwph
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        dpom=1./float(itwph)
        do i=2,itwph
          call FeQuestRealEdwOpen(nEdw,dpom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventEdw) then
        TwShow=CheckNumber-nEdwPrv+2
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowTwinMatrix) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        m=0
        do i=1,3
          TextInfo(i)=Indices(i)//'='
          if(i.eq.1) pomt=FeTxLength(TextInfo(i))
          do j=1,3
            m=m+1
            pom=Rtw(j+(i-1)*3,TwShow)
            if(i.eq.1) then
              if(j.eq.1) pomc=FeTxLength('XXXXXXX*X')
              pomt=pomt+pomc
              call FeTabsAdd(pomt,0,IdLeftTab,' ')
            endif
            write(At,'(f7.3,''*'',a1)') pom,Indices(j)
            call Velka(At)
            if(pom.ge.0..and.j.ne.1) then
              l=1
1600          if(At(l:l).eq.' ') then
                l=l+1
                go to 1600
              endif
              l=l-1
              if(l.gt.0) At(l:l)='+'
            endif
            TextInfo(i)=TextInfo(i)(:idel(TextInfo(i)))//
     1                  Tabulator//At(:idel(At))
          enddo
        enddo
        NInfo=3
        write(Cislo,'(''matrix#'',i2)') TwShow
        call Zhusti(Cislo)
        Veta='Twinning '//Cislo(:idel(Cislo))
        call FeInfoOut(-1.,-1.,Veta,'L')
        call FeTabsReset(UseTabs)
        UseTabsIn=UseTabs
        EventType=EventEdw
        EventNumber=TwShow-2+nEdwPrv
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=mxscu+1
        call FeUpdateParamAndKeys(nEdw,nCrw,sctw(2,KDatBlock),
     1                            KiS(kip,KDatBlock),itwph-1)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
