      subroutine EM40GrThick
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(2),ih(3)
      dimension RFak(:),Thick(:)
      character*256 Veta
      character*17 :: Labels(6) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Z%-              ',
     5                'Z%+              ',
     6                '%Go to           '/)
      allocatable RFak,Thick
      Tiskne=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(80.,40.,30.,40.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          wpom=wpom/2.-5.
          xsave=xpom
          ysave=ypom
        else if(i.eq.4) then
          nButtZMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.5) then
          nButtZPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.6) then
          nButtGoto=ButtonLastMade
        endif
        if(i.le.1) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.6) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      NZoneMax=0
      do i=1,NEDZone(1)
        if(UseEdZone(i,KRefBlock)) NZoneMax=NZoneMax+1
      enddo
      NZone=1
1000  ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.edout_thick','formatted','unknown')
      NZoneL=0
      m=0
      do i=1,NEDZone(KRefBlock)
        if(UseEdZone(i,KRefBlock)) then
          m=m+1
          if(m.eq.NZone) then
            NZoneL=i
            exit
          endif
        endif
      enddo
      do i=1,NZone-1
1100    read(ln,FormA,end=1150) Veta
        if(Veta(1:3).ne.'===') go to 1100
      enddo
1150  n=0
1200  read(ln,FormA,end=1300) Veta
      if(Veta(1:3).eq.'===') go to 1300
      n=n+1
      go to 1200
1300  rewind ln
      do i=1,NZone-1
1350    read(ln,FormA,end=1400) Veta
        if(Veta(1:3).ne.'===') go to 1350
      enddo
1400  if(allocated(Thick)) deallocate(Thick,RFak)
      allocate(RFak(n),Thick(n))
      rewind ln
      do i=1,NZone-1
1450    read(ln,FormA,end=1500) Veta
        if(Veta(1:3).ne.'===') go to 1450
      enddo
1500  n=0
      YMax=0.
      XMax=0.
      YMin=0.
      XMin=99999999999999.
1550  read(ln,FormA,end=1600) Veta
      if(Veta.ne.' ') then
        n=n+1
        read(Veta,100) Thick(n),RFak(n)
        YMax=max(YMax,RFak(n))
        XMin=min(XMin,Thick(n))
        XMax=max(XMax,Thick(n))
        go to 1550
      endif
      read(ln,100) ThickOpt,RFakOpt
1600  close(ln)
      call FeQuestButtonOff(nButtPrint)
      call FeQuestButtonOff(nButtSave)
      if(NZoneMax.gt.1) then
        if(NZone.le.1) then
          call FeQuestButtonDisable(nButtZMinus)
        else
          call FeQuestButtonOff(nButtZMinus)
        endif
        if(NZone.ge.NZoneMax) then
          call FeQuestButtonDisable(nButtZPlus)
        else
          call FeQuestButtonOff(nButtZPlus)
        endif
        call FeQuestButtonOff(nButtGoTo)
      endif
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        xomn=XMin
        xomx=XMax
        yomn=YMin
        yomx=YMax
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        write(Cislo,FormI15) NZoneL
        call Zhusti(Cislo)
        Veta='R-factor/Thickess for zone #'//Cislo(:idel(Cislo))
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1                 (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2                 pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3                 (XCornAcWin(1,2)-XCornAcWin(1,3)),Veta,'C',White)
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Thinkness')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'R-factor')
        xpp(3)=0.
        do i=1,n
          xpp(1)=Thick(i)
          xpp(2)=RFak(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
        enddo
2300    xpp(1)=ThickOpt
        xpp(2)=YMin
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=ThickOpt
        xpp(2)=YMax
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtZMinus) then
          NZone=max(NZone-1,1)
          go to 1000
        else if(CheckNumber.eq.nButtZPlus) then
          NZone=min(NZone+1,NZoneMax)
          go to 1000
        else if(CheckNumber.eq.nButtGoto) then
          idp=NextQuestId()
          xqd=200.
          call FeQuestCreate(idp,-1.,-1.,xqd,1,' ',1,LightGray,0,0)
          tpom=5.
          dpom=50.
          Veta='Next zone to be drawn:'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestEudMake(idp,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
          call FeQuestIntEdwOpen(EdwLastMade,NZoneL,.false.,.false.)
          call FeQuestEudOpen(EdwLastMade,1,NEDZone,1,1.,1.,1.)
          nEdwGoto=EdwLastMade
3500      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK)
     1      then
            call FeQuestIntFromEdw(nEdwGoto,NZoneL)
            if(UseEDZone(NZoneL,KRefBlock)) then
              QuestCheck(idp)=0
              NZone=0
              do i=1,NZoneL
                if(UseEdZone(i,KRefBlock)) NZone=NZone+1
              enddo
            else
              write(Cislo,FormI15) NZoneL
              call Zhusti(Cislo)
              Veta='thinkness for the zone #'//Cislo(:idel(Cislo))//
     1             ' has not been optimized in the last run.'
              call FeChybne(-1.,300.,Veta,' ',SeriousError)
              call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            endif
            go to 3500
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          call FeQuestRemove(idp)
          if(ich.eq.0) then
            go to 1000
          else
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
      if(allocated(Thick)) deallocate(Thick,RFak)
9999  return
100   format(3f14.6)
      end
