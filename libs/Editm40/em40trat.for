      subroutine EM40TrAt(ii,io,isw,ntr)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension xp(6),xpp(6),px(28),py(28),spx(28),spy(28),phi(3),
     1          xps(6,2),snm(:),csm(:),eps(:),pxa(:),pya(:),
     2          spxa(:),spya(:),xorg(3)
      allocatable snm,csm,eps,pxa,pya,spxa,spya
      n=MaxUsedKw(KPhase)
      allocate(snm(n),csm(n),eps(n),pxa(28*n),pya(28*n),spxa(28*n),
     1         spya(28*n))
      call CopyVek(x(1,ii),xorg,3)
      if(io.ne.ii) then
        call CopyBasicKeysForAtom(ii,io)
        i=min(5,idel(atom(ii)))
        if(ntr.le.1) atom(ii)=atom(ii)(1:i)//'_1'
        write(Cislo,'(''_'',i3)') ntr+1
        call Zhusti(Cislo)
        atom(io)=atom(io)(1:i)//Cislo(:idel(Cislo))
        if(NDimI(KPhase).gt.0) then
           phf(io)= phf(ii)
          sphf(io)=sphf(ii)
        endif
        call ShiftKiAt(io,itf(io),ifr(io),lasmax(io),KModA(1,io),
     1                 MagPar(io),.true.)
        call SetIntArrayTo(KiA(1,io),DelkaKiAtomu(io),0)
      endif
      if(MagPar(ii).gt.0) then
        if(KUsePolar(ii).gt.0) then
          do j=0,MagPar(ii)-1
            if(j.eq.0) then
              call ShpCoor2Fract( sm0(1,ii),px)
              call ShpCoor2Fract(ssm0(1,ii),px)
            else
              call ShpCoor2Fract( smx(1,j,ii),px)
              call ShpCoor2Fract(ssmx(1,j,ii),px)
              call ShpCoor2Fract( smy(1,j,ii),px)
              call ShpCoor2Fract(ssmy(1,j,ii),px)
            endif
          enddo
        endif
      endif
      call CopyVek(x(1,ii),xp,3)
      call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
      call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),1)
      do j=1,NDim(KPhase)
        xp(j)=xpp(j)+TransVec(j,ntr,isw)
        if(j.lt.4) then
          x(j,io)=xp(j)
        else
          phi(j-3)=-xp(j)*pi2
        endif
      enddo
      if(itf(ii).le.1) then
        beta(1,io)=beta(1,ii)
        sbeta(1,io)=sbeta(1,ii)
      endif
      if(NDimI(KPhase).gt.0) then
        call qbyx(x(1,io),qcnt(1,io),isw)
        do k=1,MaxUsedKw(KPhase)
          eps(k)=isign(1,TransKwSym(k,ntr))
          l=iabs(TransKwSym(k,ntr))
          pom=0.
          do m=1,NDimI(KPhase)
            pom=pom+float(kw(m,l,KPhase))*phi(m)
          enddo
          snp=sin(pom)
          csp=cos(pom)
          if(abs(snp).lt..001) snp=0.
          if(abs(csp).lt..001) csp=0.
          if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
          if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
          snm(k)=snp
          csm(k)=csp
        enddo
      endif
      if(MagneticType(KPhase).gt.0) then
        nk=itf(ii)+1
      else
        nk=itf(ii)
      endif
      do n=0,nk
        if(n.gt.itf(ii)) then
          NRank=3
        else
          NRank=TRank(n)
        endif
        if(n.gt.itf(ii)) then
          if(MagPar(ii).gt.0) then
            do j=1,NRank
              px(j)=sm0(j,ii)
              spx(j)=ssm0(j,ii)
            enddo
            call multm (TransM(1,ntr,isw), px, sm0(1,io),NRank,NRank,1)
            call multmq(TransM(1,ntr,isw),spx,ssm0(1,io),NRank,NRank,1)
          endif
        else if(n.eq.0) then
          ai(io)=ai(ii)
          sai(io)=sai(ii)
          if(KModA(1,io).gt.0) then
            a0(io)=a0(ii)
            sa0(io)=sa0(ii)
          endif
        else if(n.eq.1) then
          do j=1,NRank
            spx(j)=sx(j,ii)
          enddo
          call multmq(TransX(1,ntr,isw),spx,sx(1,io),NRank,NRank,1)
        else if(n.eq.2) then
          do j=1,NRank
            px(j)= beta(j,ii)
            spx(j)=sbeta(j,ii)
          enddo
          call multm (TransTemp(1,ntr,isw), px, beta(1,io),NRank,NRank,
     1                1)
          call multmq(TransTemp(1,ntr,isw),spx,sbeta(1,io),NRank,NRank,
     1                1)
        else if(n.eq.3) then
          do j=1,NRank
            px(j)=c3(j,ii)
            spx(j)=sc3(j,ii)
          enddo
          call multm (TransC3(1,ntr,isw), px, c3(1,io),NRank,NRank,1)
          call multmq(TransC3(1,ntr,isw),spx,sc3(1,io),NRank,NRank,1)
        else if(n.eq.4) then
          do j=1,NRank
            px(j)=c4(j,ii)
            spx(j)=sc4(j,ii)
          enddo
          call multm (TransC4(1,ntr,isw), px, c4(1,io),NRank,NRank,1)
          call multmq(TransC4(1,ntr,isw),spx,sc4(1,io),NRank,NRank,1)
        else if(n.eq.5) then
          do j=1,NRank
            px(j)=c5(j,ii)
            spx(j)=sc5(j,ii)
          enddo
          call multm (TransC5(1,ntr,isw), px, c5(1,io),NRank,NRank,1)
          call multmq(TransC5(1,ntr,isw),spx,sc5(1,io),NRank,NRank,1)
        else
          do j=1,NRank
            px(j)=c6(j,ii)
            spx(j)=sc6(j,ii)
          enddo
          call multm (TransC6(1,ntr,isw), px, c6(1,io),NRank,NRank,1)
          call multmq(TransC6(1,ntr,isw),spx,sc6(1,io),NRank,NRank,1)
        endif
        if(n.gt.itf(ii)) then
          kfap=0
          kmodp=MagPar(ii)-1
        else
          kfap=KFA(n+1,ii)
          kmodp=KModA(n+1,ii)
        endif
        if(kmodp.le.0) cycle
        nn=kmodp*NRank
        if(kfap.ne.0) then
          if(n.eq.0) then
            NDimIp=NDimI(KPhase)
          else
            NDimIp=1
          endif
        else
          NDimIp=0
        endif
        if(n.gt.itf(ii)) then
          if(MagPar(ii).gt.0) then
            call CopyVek( smx(1,1,ii), pxa,nn)
            call CopyVek( smy(1,1,ii), pya,nn)
            call CopyVek(ssmx(1,1,ii),spxa,nn)
            call CopyVek(ssmy(1,1,ii),spya,nn)
          endif
        else if(n.eq.0) then
          call CopyVek( ax(1,ii), pxa,nn)
          call CopyVek( ay(1,ii), pya,nn)
          call CopyVek(sax(1,ii),spxa,nn)
          call CopyVek(say(1,ii),spya,nn)
        else if(n.eq.1) then
          call CopyVek( ux(1,1,ii), pxa,nn)
          call CopyVek( uy(1,1,ii), pya,nn)
          call CopyVek(sux(1,1,ii),spxa,nn)
          call CopyVek(suy(1,1,ii),spya,nn)
        else if(n.eq.2) then
          call CopyVek( bx(1,1,ii), pxa,nn)
          call CopyVek( by(1,1,ii), pya,nn)
          call CopyVek(sbx(1,1,ii),spxa,nn)
          call CopyVek(sby(1,1,ii),spya,nn)
        else if(n.eq.3) then
          call CopyVek( c3x(1,1,ii), pxa,nn)
          call CopyVek( c3y(1,1,ii), pya,nn)
          call CopyVek(sc3x(1,1,ii),spxa,nn)
          call CopyVek(sc3y(1,1,ii),spya,nn)
        else if(n.eq.4) then
          call CopyVek( c4x(1,1,ii), pxa,nn)
          call CopyVek( c4y(1,1,ii), pya,nn)
          call CopyVek(sc4x(1,1,ii),spxa,nn)
          call CopyVek(sc4y(1,1,ii),spya,nn)
        else if(n.eq.5) then
          call CopyVek( c5x(1,1,ii), pxa,nn)
          call CopyVek( c5y(1,1,ii), pya,nn)
          call CopyVek(sc5x(1,1,ii),spxa,nn)
          call CopyVek(sc5y(1,1,ii),spya,nn)
        else if(n.eq.6) then
          call CopyVek( c6x(1,1,ii), pxa,nn)
          call CopyVek( c6y(1,1,ii), pya,nn)
          call CopyVek(sc6x(1,1,ii),spxa,nn)
          call CopyVek(sc6y(1,1,ii),spya,nn)
        endif
        nn=1
        do k=1,kmodp
          if(TypeModFun(ii).le.1) then
            iw=iabs(TransKwSym(k,ntr))
            snp=snm(k)
            csp=csm(k)
          else
            iw=k
            snp=0.
            csp=1.
          endif
          epsp=eps(k)
          if(n.gt.itf(ii)) then
            if(MagPar(ii).gt.0) then
              call multm (TransM(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
              call multmq(TransM(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
              call multm (TransM(1,ntr,isw), pya(nn), py,NRank,NRank,1)
              call multmq(TransM(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
            endif
          else if(n.eq.0) then
             px(1)= pxa(nn)
             py(1)= pya(nn)
            spx(1)=spxa(nn)
            spy(1)=spya(nn)
          else if(n.eq.1) then
            call multm (TransX(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransX(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            if(kfap.ne.1.or.k.ne.kmodp.or.TypeModFun(ii).gt.1) then
              call multm (TransX(1,ntr,isw), pya(nn), py,NRank,NRank,1)
              call multmq(TransX(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
            endif
          else if(n.eq.2) then
            call multm (TransTemp(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransTemp(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            if(kfap.ne.1.or.k.ne.kmodp.or.TypeModFun(ii).gt.1) then
              call multm (TransTemp(1,ntr,isw), pya(nn), py,NRank,NRank,
     1                    1)
              call multmq(TransTemp(1,ntr,isw),spya(nn),spy,NRank,NRank,
     1                    1)
            endif
          else if(n.eq.3) then
            call multm (TransC3(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC3(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC3(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC3(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else if(n.eq.4) then
            call multm (TransC4(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC4(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC4(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC4(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else if(n.eq.5) then
            call multm (TransC5(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC5(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC5(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC5(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else
            call multm (TransC6(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC6(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC6(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC6(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          endif
          if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
            do j=1,NRank
              ppx=epsp*csp*px(j)-snp*py(j)
              ppy=epsp*snp*px(j)+csp*py(j)
              sppx=abs(epsp*csp*spx(j)-snp*spy(j))
              sppy=abs(epsp*snp*spx(j)+csp*spy(j))
              if(n.gt.itf(ii)) then
                 smx(j,iw,io)=ppx
                 smy(j,iw,io)=ppy
                ssmx(j,iw,io)=sppx
                ssmy(j,iw,io)=sppy
              else if(n.eq.0) then
                ax(iw,io)=ppx
                ay(iw,io)=ppy
                sax(iw,io)=sppx
                say(iw,io)=sppy
              else if(n.eq.1) then
                ux(j,iw,io)=ppx
                uy(j,iw,io)=ppy
                sux(j,iw,io)=sppx
                suy(j,iw,io)=sppy
              else if(n.eq.2) then
                bx(j,iw,io)=ppx
                by(j,iw,io)=ppy
                sbx(j,iw,io)=sppx
                sby(j,iw,io)=sppy
              else if(n.eq.3) then
                c3x(j,iw,io)=ppx
                c3y(j,iw,io)=ppy
                sc3x(j,iw,io)=sppx
                sc3y(j,iw,io)=sppy
              else if(n.eq.4) then
                c4x(j,iw,io)=ppx
                c4y(j,iw,io)=ppy
                sc4x(j,iw,io)=sppx
                sc4y(j,iw,io)=sppy
              else if(n.eq.5) then
                c5x(j,iw,io)=ppx
                c5y(j,iw,io)=ppy
                sc5x(j,iw,io)=sppx
                sc5y(j,iw,io)=sppy
              else
                c6x(j,iw,io)=ppx
                c6y(j,iw,io)=ppy
                sc6x(j,iw,io)=sppx
                sc6y(j,iw,io)=sppy
              endif
            enddo
          else
            kk=k-kmodp+NDimI(KPhase)
            epsp=eps(kk)
            if(n.eq.0) then
              call CopyVek(xorg,xp,3)
              if(NDimI(KPhase).eq.1) then
                delta=a0(ii)*.5
              else
                delta=py(1)*.5
              endif
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              do j=1,2
                if(j.eq.1) then
                  xp(kk+3)=px(1)-delta
                else
                  xp(kk+3)=px(1)+delta
                endif
                call multm(TransMat(1,ntr,isw),xp,xps(1,j),NDim(KPhase),
     1                     NDim(KPhase),1)
                call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                      NDim(KPhase))
              enddo
              do j=4,NDim(KPhase)
                delta=abs(xps(j,2)-xps(j,1))
                if(delta.gt..00001) then
                  jj=j+kmodp-NDimI(KPhase)-3
                  go to 2270
                endif
              enddo
2270          pom=(xps(4,2)+xps(4,1))*.5
              j=pom
              if(pom.lt.0.) j=j-1
              ax(jj,io)=pom-float(j)
              sax(jj,io)=sax(k,ii)
              if(NDimI(KPhase).gt.1) then
                ay(jj,io)=delta
                say(jj,io)=say(kk,ii)
              else
                ay(jj,io)=0.
                say(jj,io)=0.
              endif
            else if(n.eq.1) then
              call multm (TransX(1,ntr,isw), ux(1,k,ii), px,3,3,1)
              call multmq(TransX(1,ntr,isw),sux(1,k,ii),spx,3,3,1)
              do j=1,3
                 ux(j,k,io)=epsp* px(j)
                sux(j,k,io)=abs(spx(j))
              enddo
              call CopyVek(x(1,ii),xp,3)
              xp(4)=uy(1,k,ii)
              call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                   NDim(KPhase),1)
              call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
              pom=xp(4)
              j=pom
              if(pom.lt.0.) j=j-1
              uy(1,k,io)=pom-float(j)
              uy(2,k,io)=uy(2,k,ii)
              suy(1,k,io)=suy(1,k,ii)
              suy(2,k,io)=suy(2,k,ii)
            else if(n.eq.2) then
              call multm (TransTemp(1,ntr,isw), bx(1,k,ii), px,6,6,1)
              call multmq(TransTemp(1,ntr,isw),sbx(1,k,ii),spx,6,6,1)
              do j=1,3
                bx(j,k,io)=epsp*px(j)
                sbx(j,k,io)=abs(spx(j))
              enddo
              pom=epsp*by(1,k,ii)+xp(4)
              j=pom
              if(pom.lt.0.) j=j-1
              by(1,k,io)=pom-float(j)
              by(2,k,io)=by(2,k,ii)
              sby(1,k,io)=sby(1,k,ii)
              sby(2,k,io)=sby(2,k,ii)
            endif
          endif
          nn=nn+NRank
        enddo
      enddo
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          NPGAt(io)=NPGAt(ii)
          do j=1,NPGAt(ii)
            call CopyMat(RPGAt(1,j,ii),RPGAt(1,j,io),3)
          enddo
          SmbPGAt(io)=SmbPGAt(ii)
          LocAtSystSt(1,io)=LocAtSystSt(1,ii)
          LocAtSystSt(2,io)=LocAtSystSt(2,ii)
          LocAtSystAx(io)=LocAtSystAx(ii)
          LocAtSense(io)=LocAtSense(ii)
          call CopyVek(LocAtSystX(1,1,ii),LocAtSystX(1,1,io),6)
          call CopyMat(TrAt(1,ii),TrAt(1,io),3)
          call CopyMat(TriAt(1,ii),TriAt(1,io),3)
          call CopyMat(TroAt(1,ii),TroAt(1,io),3)
          call CopyMat(TroiAt(1,ii),TroiAt(1,io),3)
          kapa1(io)=kapa1(ii)
          skapa1(io)=skapa1(ii)
          kapa2(io)=kapa2(ii)
          skapa2(io)=skapa2(ii)
          popc(io)=popc(ii)
          spopc(io)=spopc(ii)
          popv(io)=popv(ii)
          spopv(io)=spopv(ii)
          if(lasmax(ii).gt.1) then
            call CopyVek( popas(1,ii), popas(1,io),lasmax(ii)**2)
            call CopyVek(spopas(1,ii),spopas(1,io),lasmax(ii)**2)
          endif
        endif
        go to 9999
      endif
      OrthoEps(io)=OrthoEps(ii)
      OrthoX40(io)=OrthoX40(ii)
      OrthoDelta(io)=OrthoDelta(ii)
      if(TypeModFun(ii).eq.1.and.kmol(ii).eq.0) then
        xp(4)=OrthoX40(ii)
        call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),
     1             1)
        call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
        pom=xp(4)
        l=pom
        if(pom.lt.0.) l=l-1
        OrthoX40(io)=pom-float(l)
      endif
9999  if(MagPar(ii).gt.0) then
        KUsePolar(io)=KUsePolar(ii)
        if(KUsePolar(ii).gt.0) then
          do j=0,MagPar(ii)-1
            if(j.eq.0) then
              call Fract2ShpCoor( sm0(1,io))
              call Fract2ShpCoor(ssm0(1,io))
              call Fract2ShpCoor( sm0(1,ii))
              call Fract2ShpCoor(ssm0(1,ii))
            else
              call Fract2ShpCoor( smx(1,j,io))
              call Fract2ShpCoor(ssmx(1,j,io))
              call Fract2ShpCoor( smy(1,j,io))
              call Fract2ShpCoor(ssmy(1,j,io))
              call Fract2ShpCoor( smx(1,j,ii))
              call Fract2ShpCoor(ssmx(1,j,ii))
              call Fract2ShpCoor( smy(1,j,ii))
              call Fract2ShpCoor(ssmy(1,j,ii))
            endif
          enddo
        endif
      endif
      deallocate(snm,csm,eps,pxa,pya,spxa,spya)
      return
      end
