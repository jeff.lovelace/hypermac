      subroutine EM40ResetModMag
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,t256
      integer KModMx(5),EdwStateQuest
      logical UseKMod(5),CrwLogicQuest,Randomly
      real :: AmpMax(5)=(/.1,.01,.001,.1,.1/)
      KModMx=0
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        do j=1,3
          KModMx(j)=max(KModMx(j),KModA(j,ia))
        enddo
        if(KModMx(4).le.0.and.MagPar(ia).ge.1) KModMx(4)=1
        KModMx(5)=max(KModMx(5),MagPar(ia))
      enddo
      ich=0
      id=NextQuestId()
      xqd=550.
      il=7
      Veta='Reset modulation/magnetic parameters'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='O%ccupational modulation'
      t256='=> maximal amplitude'
      tpome=tpom+FeTxLengthUnder(Veta)+35.
      xpome=tpome+FeTxLengthUnder(t256)+35.
      dpom=70.
      tpoml=xpome+dpom+5.
      do i=1,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(CrwLastMade,.true.)
        call FeQuestEdwMake(id,tpome,il,xpome,il,t256,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,AmpMax(i),.false.,.false.)
        if(i.eq.1) then
          nCrwOcc=CrwLastMade
          nEdwOcc=EdwLastMade
          Veta='%Positional parameters'
        else if(i.eq.2) then
          nCrwX=CrwLastMade
          nEdwX=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems','L','N')
          nLblX=LblLastMade
          Veta='A%DP parameters'
        else if(i.eq.3) then
          nCrwADP=CrwLastMade
          nEdwADP=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems^2','L','N')
          nLblADP=LblLastMade
          Veta='%Magnetic parameters (k=0)'
        else if(i.eq.4) then
          nCrwMag0=CrwLastMade
          nEdwMag0=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'Bohr magnetons','L','N')
          nLblMag0=LblLastMade
          Veta='Ma%gnetic parameters (k<>0)'
        else if(i.eq.5) then
          nCrwMag=CrwLastMade
          nEdwMag=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'Bohr magnetons','L','N')
          nLblMag=LblLastMade
        endif
        if(KModMx(i).le.0) then
          UseKMod(i)=.false.
          call FeQuestCrwDisable(CrwLastMade)
          call FeQuestEdwDisable(EdwLastMade)
          call FeQuestLblDisable(LblLastMade)
        else
          UseKMod(i)=.true.
        endif
      enddo
      Veta='%Reset parameters to pmax (pmax=maximal ampitude)'
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      il=-10*il
      do i=1,2
        il=il-7
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
        if(i.eq.1) then
          Veta='R%eset parameters randomly in the interval (-pmax,pmax)'
          nCrwMax=CrwLastMade
        else
          nCrwRandom=CrwLastMade
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwOcc) then
        UseKMod(1)=CrwLogicQuest(nCrwOcc)
        if(UseKMod(1)) then
          if(EdwStateQuest(nEdwOcc).ne.EdwOpened)
     1      call FeQuestRealEdwOpen(nEdwOcc,AmpMax(1),.false.,.false.)
        else
          if(EdwStateQuest(nEdwOcc).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwOcc,AmpMax(1))
            call FeQuestEdwDisable(nEdwOcc)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwX) then
        UseKMod(2)=CrwLogicQuest(nCrwX)
        if(UseKMod(2)) then
          if(EdwStateQuest(nEdwX).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwX,AmpMax(2),.false.,.false.)
            call FeQuestLblOn(nLblX)
          endif
        else
          if(EdwStateQuest(nEdwX).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwX,AmpMax(2))
            call FeQuestEdwDisable(nEdwX)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwADP) then
        UseKMod(3)=CrwLogicQuest(nCrwADP)
        if(UseKMod(3)) then
          if(EdwStateQuest(nEdwADP).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwADP,AmpMax(3),.false.,.false.)
            call FeQuestLblOn(nLblADP)
          endif
        else
          if(EdwStateQuest(nEdwADP).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwADP,AmpMax(3))
            call FeQuestEdwDisable(nEdwADP)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMag0) then
        UseKMod(4)=CrwLogicQuest(nCrwMag0)
        if(UseKMod(4)) then
          if(EdwStateQuest(nEdwMag0).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwMag0,AmpMax(4),.false.,.false.)
            call FeQuestLblOn(nLblMag0)
          endif
        else
          if(EdwStateQuest(nEdwMag0).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwMag0,AmpMax(4))
            call FeQuestEdwDisable(nEdwMag0)
            call FeQuestLblDisable(nLblMag0)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMag) then
        UseKMod(5)=CrwLogicQuest(nCrwMag)
        if(UseKMod(5)) then
          if(EdwStateQuest(nEdwMag).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwMag,AmpMax(5),.false.,.false.)
            call FeQuestLblOn(nLblMag)
          endif
        else
          if(EdwStateQuest(nEdwMag).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwMag,AmpMax(5))
            call FeQuestEdwDisable(nEdwMag)
            call FeQuestLblDisable(nLblMag)
          endif
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        Randomly=CrwLogicQuest(nCrwRandom)
        nEdw=nEdwOcc
        do i=1,5
          if(EdwStateQuest(nEdw).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdw,AmpMax(i))
          nEdw=nEdw+1
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(Randomly) call Random_Seed()
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        do j=1,5
          if(.not.UseKMod(j).or.(j.le.3.and.KModA(j,ia).le.0).or.
     1       (j.eq.4.and.MagPar(ia).le.0)) cycle
          if(j.eq.1) then
            do k=1,KModA(j,ia)
              if(Randomly) then
                call Random_Number(pom)
                pom=2.*pom-1.
                ax(k,ia)=AmpMax(1)*pom
                call Random_Number(pom)
                pom=2.*pom-1.
                ay(k,ia)=AmpMax(1)*pom
              else
                ax(k,ia)=AmpMax(1)
                ay(k,ia)=AmpMax(1)
              endif
            enddo
          else if(j.eq.2) then
            do k=1,KModA(j,ia)
              do l=1,3
                ppp=1./CellPar(l,iswa(ia),kswa(ia))
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  ux(l,k,ia)=AmpMax(2)*pom*ppp
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  uy(l,k,ia)=AmpMax(2)*pom*ppp
                else
                  ux(l,k,ia)=AmpMax(2)*ppp
                  uy(l,k,ia)=AmpMax(2)*ppp
                endif
              enddo
            enddo
          else if(j.eq.3) then
            do k=1,KModA(j,ia)
              do l=1,6
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  bx(l,k,ia)=AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  by(l,k,ia)=AmpMax(3)*pom
                else
                  bx(l,k,ia)=AmpMax(3)
                  by(l,k,ia)=AmpMax(3)
                endif
              enddo
            enddo
          else if(j.eq.4) then
            do l=1,3
              if(Randomly) then
                call Random_Number(pom)
                pom=2.*pom-1.
                sm0(l,ia)=pom*AmpMax(4)
              else
                sm0(l,ia)=AmpMax(4)
              endif
            enddo
          else if(j.eq.5) then
            do k=2,MagPar(ia)
              do l=1,3
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  smx(l,k-1,ia)=AmpMax(5)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  smy(l,k-1,ia)=AmpMax(5)*pom
                else
                  smx(l,k-1,ia)=AmpMax(5)
                  smy(l,k-1,ia)=AmpMax(5)
                endif
              enddo
            enddo
          endif
        enddo
      enddo
9999  return
      end
