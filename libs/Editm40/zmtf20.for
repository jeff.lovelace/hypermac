      subroutine ZmTF20(ia)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension KModAP(7)
      l=(kmol(ia)-1)/mxp+1
      call ReallocateMoleculeParams(NMolec,mxp,1,KModMMax)
      if(KTLS(l).le.0) then
        do k=kmol(ia),kmol(ia)+mam(l)-1
          call ShiftKiMol(k,1,KModM(1,k),KModM(2,k),KModM(3,k),
     1                    .false.)
          if(itf(ia).eq.1) call ZmTF12(ia)
          call CopyVek(beta(1,ia),tt(1,k),6)
          call SetRealArrayTo(tt(4,k),3,0.)
          call SetRealArrayTo(stt(1,k),6,0.)
          call SetRealArrayTo(tl(1,k),6,0.)
          call SetRealArrayTo(stl(1,k),6,0.)
          call SetRealArrayTo(ts(1,k),9,0.)
          call SetRealArrayTo(sts(1,k),9,0.)
          call SetIntArrayTo(KiMol(8,k),21,1)
        enddo
        KTLS(l)=1
      else
        KTLS(l)=KTLS(l)+1
      endif
      call SetRealArrayTo(beta(1,ia),6,0.)
      call SetRealArrayTo(sbeta(1,ia),6,0.)
      call SetIntArrayTo(KiA(5,ia),6,0)
      call SetIntArrayTo(KModAP,7,0)
      KModAP(1)=KModA(1,ia)
      KModAP(2)=KModA(2,ia)
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModAP,MagPar(ia),
     1               .false.)
      itf(ia)=0
      KModA(3,ia)=0
      return
      end
