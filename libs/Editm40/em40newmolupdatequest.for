      subroutine EM40NewMolUpdateQuest(Void)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EM40NewMolC/ nEdwFirstPoint,nEdwSelect,nButtSelect,nEdwOcc,
     1                    nButtCalc,nButtShowCoinc,Model(3),Actual(3),
     2                    nButtApplyNext,nButtApplyEnd,nButtPoint,Znovu
      character*80 Veta
      integer ButtonStateQuest,EdwStateQuest,Actual
      logical Znovu
      external Void
      save /EM40NewMolC/
      if(nButtCalc.le.0) go to 9999
      if(Znovu) go to 1100
      do i=1,3
        if(Model(i).le.0) go to 1100
        if(Actual(i).le.0) go to 1100
      enddo
      call FeQuestButtonOff(nButtCalc)
      call FeQuestButtonOff(nButtShowCoinc)
      if(EdwStateQuest(nEdwOcc).eq.EdwOpened) then
        call FeQuestButtonOff(nButtApplyNext)
        call FeQuestButtonOff(nButtApplyEnd)
      else
        call FeQuestButtonDisable(nButtApplyNext)
        call FeQuestButtonDisable(nButtApplyEnd)
      endif
      go to 1200
1100  call FeQuestButtonDisable(nButtCalc)
      call FeQuestButtonDisable(nButtApplyNext)
      call FeQuestButtonDisable(nButtApplyEnd)
1200  if(nButtSelect.le.0) go to 9999
      nEdwSelect=EdwActive-EdwFr+1
      i=nEdwSelect-nEdwFirstPoint+1
      j=mod(i,2)
      if(i.lt.1.or.i.gt.nEdwFirstPoint+5) then
        if(ButtonStateQuest(nButtSelect).ne.ButtonClosed) then
          call FeQuestButtonDisable(nButtSelect)
          go to 9999
        endif
        if(ButtonStateQuest(nButtSelect).ne.ButtonClosed) then
          call FeQuestButtonDisable(nButtSelect)
          go to 9999
        endif
      else if(j.eq.1) then
        Veta='%Select the model atom'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        call FeQuestButtonDisable(nButtPoint)
        go to 2000
      else if(j.eq.0) then
        Veta='%Select the actual atom'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        if(NSavedPoints.gt.0) then
          call FeQuestButtonOff(nButtPoint)
        else
          call FeQuestButtonDisable(nButtPoint)
        endif
        go to 2000
      endif
2000  if(ButtonStateQuest(nButtSelect).ne.ButtonOff)
     1   call FeQuestButtonOff(nButtSelect)
9999  return
      end
