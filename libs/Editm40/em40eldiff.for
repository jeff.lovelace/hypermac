      subroutine EM40ElDiff(ich)
      use EDZones_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real tpoma(4),xpoma(4),xp(6),pomc(6)
      character*256 EdwStringQuest
      character*80 Veta,VetaPrev
      character*2  nty
      integer :: SbwLnQuest,SbwItemSelQuest,EdwStateQuest,CrwStateQuest,
     1           kip(6),kic(6),RolMenuSelectedQuest,NFrom=1,NEach=3,
     2           ip(1)
      integer, allocatable :: NThickEDZoneO(:)
      logical FeYesNo,FileDiff,CrwLogicQuest,CalcDynOld,ExistFile,
     1        EqIgCase,RefineEnd,Prvni,lpom
      logical, allocatable :: UseEDZoneO(:),LZone(:),LZoneO(:),LZoneR(:)
      if(allocated(LZone)) deallocate(LZone)
      n=0
      do i=1,NRefBlock
        n=max(n,NEDZone(i))
      enddo
      allocate(LZone(n),LZoneO(n))
      KDatBlockO=KDatBlock
      KRefBlockO=KRefBlock
      LZone=.false.
      CalcDynOld=CalcDyn
      call DeleteFile(fln(:ifln)//'.edout_all')
1100  xqd=700.
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      il=18
      id=NextQuestId()
      Veta='Parameters for electron diffraction data'
      if(NRefBlock.gt.1) then
        write(Cislo,'(i5)') KRefBlock
        call Zhusti(Cislo)
        Veta=Veta(:idel(Veta))//' - Refblock#'//Cislo
        il=il+1
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      il=1
      tpom=5.
      Veta='Orientation matrix:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      tpom=tpom+FeTxLength(Veta)+25.
      xpom=tpom+25.
      dpom=70.
      do i=1,3
        xpomp=xpom
        tpomp=tpom
        do j=1,3
          write(Cislo,'(''U'',2i1)') i,j
          call FeQuestEdwMake(id,tpomp,il,xpomp,il,Cislo,'L',dpom,EdwYd,
     1                        0)
          call FeQuestRealEdwOpen(EdwLastMade,
     1                            OrMatEDZone(i,j,KRefBlock),.false.,
     2                            .false.)
          if(i.eq.1.and.j.eq.1) nEdwUB=EdwLastMade
          xpomp=xpomp+110.
          tpomp=tpomp+110.
        enddo
        il=il+1
      enddo
      tpom=5.
      Veta='Maximal diffraction vector g%(max):'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,GMaxEDZone(KRefBlock),.false.,
     1                        .false.)
      nEdwGMax=EdwLastMade
      tpomp=tpom+xqd*.5-70.
      xpomp=xpom+xqd*.5-90.
      dpomp=40.
      Veta='%Number integration steps:'
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpomp,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,EDIntSteps(KRefBlock),.false.)
      nEdwIntSteps=EdwLastMade
      xpompp=xpomp+dpomp+40.
      tpompp=xpompp+CrwgXd+10.
      Veta='Geometry %PEDT'
      call FeQuestCrwMake(id,tpompp,il,xpompp,il,Veta,'L',CrwXd,CrwYd,0,
     1                    1)
      call FeQuestCrwOpen(CrwLastMade,EDGeometryIEDT(KRefBlock).le.0)
      nCrwPEDT=CrwLastMade
      il=il+1
      Veta='Maximal %excitation error (Matrix):'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,SGMaxMEDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwSGMaxM=EdwLastMade
      Veta='%Use dynamic approach:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,CalcDyn)
      nCrwCalcDyn=CrwLastMade
      Veta='Geometry %IEDT'
      call FeQuestCrwMake(id,tpompp,il,xpompp,il,Veta,'L',CrwXd,CrwYd,0,
     1                    1)
      call FeQuestCrwOpen(CrwLastMade,EDGeometryIEDT(KRefBlock).gt.0)
      nCrwIEDT=CrwLastMade
      il=il+1
      Veta='Maximal e%xcitation error (Refine):'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,SGMaxREDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwSGMaxR=EdwLastMade
      Veta='%Apply correction for crystal tilt:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,EDTiltCorr(KRefBlock).eq.1)
      nCrwTiltCorr=CrwLastMade
      il=il+1
      Veta='%Limit on RSg:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,CSGMaxREDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwCSGMaxR=EdwLastMade
      Veta='For Fourier rescale to %Fcalc:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,RescaleToFCalc)
      nCrwRescaleToFCalc=CrwLastMade
      il=il+1
      Veta='Nu%mber of threads:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,EDThreads(KRefBlock),.false.)
      nEdwThreads=EdwLastMade
      Veta='Select %zones for refinement'
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtSelect=ButtonLastMade
      tpomp=tpomp+dpom+20.
      Veta='Define zones of e%qual thicknesses'
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtThick=ButtonLastMade
      il=il+1
      Veta='%Dyngo commands:'
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,EDCommands)
      nEdwCommands=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Run optimalizations'
      dpom=FeTxLengthUnder(Veta)+20.
      tpom=70.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(NAtCalc.le.0) call FeQuestButtonDisable(ButtonLastMade)
      nButtOptimalization=ButtonLastMade
      tpom=tpom+dpom+20.
      Veta='except of scale optimize also:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      if(NAtCalc.le.0) call FeQuestLblDisable(LblLastMade)
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      tpom=xpom+CrwgXd+10.
      Veta='%Thickness'
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,.false.)
        if(NAtCalc.le.0) call FeQuestCrwDisable(CrwLastMade)
        if(i.eq.1) then
          nCrwOptThick=CrwLastMade
          tpomp=tpom+FeTxLengthUnder(Veta)+20.
          Veta='%Show thickness plots'
          dpom=FeTxLengthUnder(Veta)+20.
          call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          nButtGraphThick=ButtonLastMade
          Veta='%Orientation'
        else
          nCrwOptOrient=CrwLastMade
        endif
        il=il+1
      enddo
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Zone%#:'
      nz=1
      LZone(1)=.true.
      NZones=1
      dpom=40.
      tpom=240.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      xpom=xpoma(1)
      tpom=tpoma(1)
      ilp=-10*il+3
      call FeQuestEudMake(id,tpom,ilp,xpom,ilp,Veta,'R',dpom,EdwYd,1)
      call FeQuestIntEdwOpen(EdwLastMade,nz,.false.)
      call FeQuestEudOpen(EdwLastMade,1,NEDZone(KRefBlock),1,0.,0.,0.)
      nEdwNZ=EdwLastMade
      tpom=xpom+100.
      call FeQuestLblMake(id,tpom,ilp,'R(all)= ---------','L','N')
      nLblRFac=LblLastMade
      Veta='Sele%ct zones for editing'
      tpom=tpom+100.
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilp,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtEdit=ButtonLastMade
      il=il+1
      kip=1
      Cislo='H'
      j=0
      dpom=70.
      ilp=-10*il+3
      do i=1,6
        j=mod(j,4)+1
        call FeQuestEdwMake(id,tpoma(j),ilp,xpoma(j),ilp,Cislo,'R',dpom,
     1                      EdwYd,0)
        if(i.eq.1) then
          nEdwH=EdwLastMade
          Cislo='K'
        else if(i.eq.2) then
          Cislo='L'
        else if(i.eq.3) then
          Cislo='prec.angle'
        else if(i.eq.4) then
          nEdwPrecAngle=EdwLastMade
          ilp=ilp-10
          Cislo='alpha'
        else if(i.eq.5) then
          nEdwAplha=EdwLastMade
          Cislo='beta'
        else if(i.eq.6) then
          nEdwBeta=EdwLastMade
        endif
      enddo
      tpom=5.
      Veta='Selected zones:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblTitleSelected=LblLastMade
      tpom=tpom+FeTxLength(Veta)+3.
      Veta=' '
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblSelected=LblLastMade
      il=il+1
      j=0
      il=il+1
      do i=1,MxEDRef
        j=mod(j,4)+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,lEDVar(i),nEdw,
     1                       nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        else if(i.eq.4) then
          il=il+1
        endif
      enddo
      if(NRefBlock.gt.1) then
        ilp=-10*il-12
        Veta=' '
        dpom=70.
        xpom=(xqd-dpom)*.5
        tpom=xpom
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,
     1                          EdwYd,1)
        call FeQuestRolMenuOpen(RolMenuLastMade,RefBlockName,NRefBlock,
     1                          KRefBlock)
        nRolMenuRefBlock=RolMenuLastMade
      else
        nRolMenuRefBlock=0
      endif
      nz=1
      LZone=.false.
      LZone(1)=.true.
      NZones=1
      nzo=-1
1400  if(NZones.le.1) then
        if(nz.eq.nzo) go to 1500
      else
        if(NZones.eq.NZonesO) then
          do i=1,NEDZone(KRefBlock)
            if(LZone(i).neqv.LZoneO(i)) go to 1420
          enddo
          go to 1500
        endif
      endif
1420  nEdw=nEdwH
      if(NZones.le.1) then
        call FeQuestLblOff(nLblTitleSelected)
        call FeQuestLblOff(nLblSelected)
        call FeQuestIntEdwOpen(nEdwNZ,nz,.false.)
        call FeQuestEudOpen(nEdwNZ,1,NEDZone(KRefBlock),1,0.,0.,0.)
        if(RFacEDZone(nz,KRefBlock).le.0.) then
          Veta='R(all)= ---------'
        else
          write(Cislo,'(f8.4)') RFacEDZone(nz,KRefBlock)
          Veta='R(all)='//Cislo(:idel(Cislo))//'%'
        endif
        call FeQuestLblChange(nLblRFac,Veta)
        do i=1,6
          if(i.eq.1) then
            pom=HEDZone(1,nz,KRefBlock)
          else if(i.eq.2) then
            pom=HEDZone(2,nz,KRefBlock)
          else if(i.eq.3) then
            pom=HEDZone(3,nz,KRefBlock)
          else if(i.eq.4) then
            pom=PrAngEDZone(nz,KRefBlock)
          else if(i.eq.5) then
            pom=AlphaEDZone(nz,KRefBlock)
          else if(i.eq.6) then
            pom=BetaEDZone(nz,KRefBlock)
          endif
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
      else
        call FeQuestEdwDisable(nEdwNZ)
        Veta='R(all)= ---------'
        call FeQuestLblChange(nLblRFac,Veta)
        do i=1,6
          call FeQuestEdwClose(nEdw)
          nEdw=nEdw+1
        enddo
        call FeQuestLblOn(nLblTitleSelected)
        Veta=' '
        i1=0
        i2=0
        do i=1,NEDZone(KRefBlock)
          if(LZone(i)) then
            if(i1.gt.0) then
              i2=i
            else
              i1=i
              i2=i
            endif
          endif
          if(.not.LZone(i).or.i.eq.NEDZone(KRefBlock)) then
            if(i1.gt.0) then
              if(i2.gt.i1) then
                write(Cislo,'(i5,''-'',i5)') i1,i2
              else
                write(Cislo,'(i5)') i1
              endif
              call Zhusti(Cislo)
              idl=idel(Veta)
              if(idl.le.0) then
                Veta=Cislo
              else
                Veta=Veta(:idl)//','//Cislo
              endif
              i1=0
              i2=0
            endif
          endif
        enddo
        call FeQuestLblOn(nLblTitleSelected)
        call FeQuestLblChange(nLblSelected,Veta)
      endif
      if(nz.gt.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        do i=1,MxEDRef
          Prvni=.true.
          do j=1,NEDZone(KRefBlock)
            if(.not.LZone(j)) cycle
            if(i.eq.1) then
              pom=ScEDZone(j,KRefBlock)
            else if(i.eq.2) then
              pom=ThickEDZone(j,KRefBlock)
            else if(i.eq.3) then
              pom=XNormEDZone(j,KRefBlock)
            else if(i.eq.4) then
              pom=YNormEDZone(j,KRefBlock)
            else if(i.eq.5) then
              pom=PhiEDZone(j,KRefBlock)
            else
              pom=ThetaEDZone(j,KRefBlock)
            endif
            if(Prvni) then
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,KiED(i,j,KRefBlock).ne.0)
              pomc(i)=pom
              kic(i)=KiED(i,j,KRefBlock)
              Prvni=.false.
            else
              if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1           abs(pom-pomc(i)).gt..0001)
     2          call FeQuestEdwLock(nEdw)
              if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1           KiED(i,j,KRefBlock).ne.kic(i))
     2          call FeQuestCrwLock(nCrw)
            endif
          enddo
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(NZones.le.1) then
        nzo=nz
      else
        nzo=-1
      endif
      LZoneO=LZone
      NZonesO=NZones
1450  if(CalcDyn) then
        call FeQuestCrwOff(nCrwOptOrient)
        call FeQuestCrwOff(nCrwOptThick)
      else
        call FeQuestCrwDisable(nCrwOptOrient)
        call FeQuestCrwDisable(nCrwOptThick)
      endif
      if(ExistFile(fln(:ifln)//'.edout_thick')) then
        call FeQuestButtonOff(nButtGraphThick)
      else
        call FeQuestButtonDisable(nButtGraphThick)
      endif
      Navrat=0
1500  call FeQuestEvent(id,ich)
1510  if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNZ) then
        call FeQuestIntFromEdw(nEdwNZ,nz)
        LZone=.false.
        LZone(nz)=.true.
        NZones=1
        Navrat=2
        go to 3000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCalcDyn) then
        if(.not.CalcDynOld) then
          CalcDyn=.true.
          call FeFillTextInfo('em40eldiff1.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          call DeleteFile(fln(:ifln)//'.m90')
          ExistM90=.false.
          call EM9CreateM90(1,ich)
          CalcDynOld=.true.
        endif
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtOptimalization) then
        ActionDatBlock=RefDatCorrespond(KRefBlock)
        if(Navrat.eq.0) then
          Navrat=1
          go to 3000
        endif
        if(CrwLogicQuest(nCrwOptThick)) then
          if(CrwLogicQuest(nCrwOptOrient)) then
            ActionED=5
          else
            ActionED=4
          endif
        else
          if(CrwLogicQuest(nCrwOptOrient)) then
            ActionED=3
          else
            ActionED=2
          endif
        endif
        Veta=fln(:ifln)//'.edout_all'
        call DeleteFile(Veta)
        call DeleteFile(fln(:ifln)//'.edout')
        call iom90(0,fln(:ifln)//'.m90')
        KDatBlock=ActionDatBlock
        call Refine(0,RefineEnd)
        if(ErrFlag.ne.0) go to 1450
        if(ActionED.eq.4) call CopyFile(Veta,fln(:ifln)//'.edout_thick')
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
1530    read(ln,FormA,end=1540) Veta
        if(LocateSubstring(Veta,'NaN',.false.,.true.).gt.0) then
          go to 1590
        else
          go to 1530
        endif
1540    rewind ln
        n=0
        xp=0
        VetaPrev='NicTamNeni'
1550    read(ln,FormA,end=1600) Veta
        if(Veta(1:3).eq.'===') then
1560      n=n+1
          if(n.le.NEdZone(KRefBlock)) then
            if(.not.UseEdZone(n,KRefBlock)) go to 1560
          endif
          if(EqIgCase(VetaPrev,'NicTamNeni')) go to 1600
          read(VetaPrev,100,err=1600) xp
          if(ActionED.eq.2) then
            RFacEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
          else if(ActionED.eq.3) then
            PhiEDZone(n,KRefBlock)=xp(1)
            ThetaEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
            RFacEDZone(n,KRefBlock)=xp(4)
          else if(ActionED.eq.4) then
            ThickEDZone(n,KRefBlock)=xp(1)
            RFacEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
          else if(ActionED.eq.5) then
            PhiEDZone(n,KRefBlock)=xp(1)
            ThetaEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
            ThickEDZone(n,KRefBlock)=xp(4)
            RFacEDZone(n,KRefBlock)=xp(5)
          endif
        endif
        VetaPrev=Veta
        go to 1550
1590    call FeReadError(ln)
1600    call CloseIfOpened(ln)
        nzo=0
        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtGraphThick) then
        call EM40GrThick
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSelect.or.
     2         CheckNumber.eq.nButtThick.or.
     3         CheckNumber.eq.nButtEdit)) then
        CheckNumberMain=CheckNumber
        if(CheckNumberMain.eq.nButtSelect) then
          if(allocated(UseEDZoneO)) deallocate(UseEDZoneO)
          allocate(UseEDZoneO(NEDZone(KRefBlock)))
          UseEDZoneO(1:NEDZone(KRefBlock))=
     1      UseEDZone(1:NEDZone(KRefBlock),KRefBlock)
          Veta='Select zones for refinement'
        else if(CheckNumber.eq.nButtThick) then
          if(allocated(NThickEDZoneO)) deallocate(NThickEDZoneO)
          allocate(NThickEDZoneO(NEDZone(1)))
          NThickEDZoneO(1:NEDZone(KRefBlock))=
     1      NThickEDZone(1:NEDZone(KRefBlock),KRefBlock)
          NThick=1
          Veta='Define zones of equal thichnesses'
        else if(CheckNumber.eq.nButtEdit) then
          if(allocated(LZoneR)) deallocate(LZoneR)
          allocate(LZoneR(NEDZone(KRefBlock)))
          LZoneR=LZone
          NZonesR=NZones
          Veta='Define zones for editing'
        endif
        idp=NextQuestId()
        xqd=500.
        il=15
        if(CheckNumberMain.eq.nButtThick) il=il+1
        call FeQuestCreate(idp,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=12
        ild=12
        xpom=5.
        dpom=xqd-10.-SbwPruhXd
        call FeQuestSbwMake(idp,xpom,il,dpom,ild,3,CutTextFromRight,
     1                       SbwHorizontal)
        nSbwSel=SbwLastMade
        ln=NextLogicNumber()
        call DeleteFile(fln(:ifln)//'_zones.tmp')
        call OpenFile(ln,fln(:ifln)//'_zones.tmp','formatted','unknown')
        do i=1,NEDZone(KRefBlock)
          Veta=' - ['
          do j=1,3
            write(Cislo,'(f15.3)') HEDZone(j,i,KRefBlock)
            call ZdrcniCisla(Cislo,1)
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
          enddo
          idl=idel(Veta)
          Veta(idl:idl)=']'
          write(Cislo,FormI15) i
          call Zhusti(Cislo)
          Veta='Zone#'//Cislo(:idel(Cislo))//Veta(:idl)
          write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        il=il+1
        if(CheckNumberMain.eq.nButtThick) then
          il=il+1
          Veta='Thickness %#'
          dpom=50.
          xpom=.5*xqd-dpom*.5-7.
          tpom=xpom-5.
          ilp=-10*il+4
          call FeQuestEudMake(idp,tpom,ilp,xpom,ilp,Veta,'R',dpom,
     1                        EdwYd,1)
          nEdwThick=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
        endif
        ilp=-il*10-7
        Veta='select each'
        dpomb=FeTxLengthUnder(Veta)+10.
        xpomb=(xqd-dpomb)*.5
        call FeQuestButtonMake(idp,xpomb,ilp,dpomb,ButYd,Veta)
        nButtSelectEach=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='From the zone#'
        dpom=50.
        xpom=xpomb-dpom-10.
        tpom=xpom-FeTxLengthUnder(Veta)-4.
        call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
        nEdwSelectFrom=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NFrom,.false.)
        Veta=' '
        xpom=xpomb+dpomb+10.
        dpom=30.
        call FeQuestEudMake(idp,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,1)
        nEdwSelectEach=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NEach,.false.)
        call FeQuestEudOpen(EdwLastMade,1,NEDZone(KRefBlock),1,0.,0.,0.)
        Veta=nty(NEach)//' zone'
        tpom=xpom+dpom+20.
        call FeQuestLblMake(idp,tpom,ilp,Veta,'L','N')
        nLblSelectEach=LblLastMade
        il=il+2
        dpom=120.
        xpom=.5*xqd-1.*dpom-10.
        Veta='Select %all'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='%Refresh'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2100    call CloseIfOpened(SbwLnQuest(nSbwSel))
        NThickMax=NEDZone(KRefBlock)
        do i=1,NEDZone(KRefBlock)
          if(CheckNumberMain.eq.nButtSelect) then
            if(UseEDZone(i,KRefBlock)) then
              j=1
            else
              j=0
            endif
          else if(CheckNumberMain.eq.nButtThick) then
            if(NThickEDZone(i,KRefBlock).eq.NThick) then
              j=1
            else
              j=0
            endif
            NThickMax=max(NThickMax,NThickEDZone(i,KRefBlock)+1)
          else if(CheckNumberMain.eq.nButtEdit) then
            if(LZone(i)) then
              j=1
            else
              j=0
            endif
          endif
          call FeQuestSetSbwItemSel(i,nSbwSel,j)
        enddo
        call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_zones.tmp')
        if(CheckNumberMain.eq.nButtThick)
     1    call FeQuestEudOpen(nEdwThick,1,NThickMax,1,0.,0.,0.)
2500    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2    then
          if(CheckNumberMain.eq.nButtSelect) then
            lpom=CheckNumber.eq.nButtAll
            do i=1,NEDZone(KRefBlock)
              UseEDZone(i,KRefBlock)=lpom
            enddo
          else if(CheckNumberMain.eq.nButtThick) then
            do i=1,NEDZone(KRefBlock)
              if(CheckNumber.eq.nButtAll) then
                NThickEDZone(i,KRefBlock)=NThick
              else
                if(NThickEDZone(i,KRefBlock).eq.NThick)
     1             NThickEDZone(i,KRefBlock)=0
              endif
            enddo
          else if(CheckNumberMain.eq.nButtEdit) then
            LZone=CheckNumber.eq.nButtAll
            if(.not.LZone(1)) then
              LZone(1)=.true.
              nz=1
              NZones=1
            endif
          endif
          go to 2100
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSelectEach) then
          call FeQuestIntFromEdw(nEdwSelectEach,NEach)
          call FeQuestIntFromEdw(nEdwSelectFrom,NFrom)
          do i=1,NEDZone(KRefBlock)
            if(CheckNumberMain.eq.nButtSelect) then
              UseEDZone(i,KRefBlock)=.false.
            else if(CheckNumberMain.eq.nButtEdit) then
              LZone(i)=.false.
            endif
          enddo
          do i=NFrom,NEDZone(KRefBlock),NEach
            if(CheckNumberMain.eq.nButtSelect) then
              UseEDZone(i,KRefBlock)=.true.
            else if(CheckNumberMain.eq.nButtThick) then
              NThickEDZone(i,KRefBlock)=NThick
            else if(CheckNumberMain.eq.nButtEdit) then
              LZone(i)=.true.
            endif
          enddo
          go to 2100
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwThick) then
          n=0
          do i=1,NEDZone(KRefBlock)
            if(SbwItemSelQuest(i,nSbwSel).eq.1) then
              NThickEDZone(i,KRefBlock)=NThick
              n=n+1
            else
              if(NThickEDZone(i,KRefBlock).eq.NThick)
     1          NThickEDZone(i,KRefBlock)=0
            endif
          enddo
          call FeQuestIntFromEdw(nEdwThick,m)
          if(n.gt.0.or.m.le.NThick) then
            NThick=m
            go to 2100
          else
            call FeQuestIntEdwOpen(nEdwThick,NThick,.false.)
            call FeQuestEudOpen(nEdwThick,1,NThickMax,1,0.,0.,0.)
            go to 2500
          endif
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSelectEach)
     1    then
          call FeQuestIntFromEdw(nEdwSelectEach,NEach)
          Veta=nty(NEach)//' zone'
          call FeQuestLblChange(nLblSelectEach,Veta)
          go to 2500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        if(ich.eq.0) then
          if(CheckNumberMain.eq.nButtEdit) NZones=0
          do i=1,NEDZone(KRefBlock)
            if(SbwItemSelQuest(i,nSbwSel).eq.1) then
              if(CheckNumberMain.eq.nButtSelect) then
                UseEDZone(i,KRefBlock)=.true.
              else if(CheckNumberMain.eq.nButtThick) then
                NThickEDZone(i,KRefBlock)=NThick
              else if(CheckNumberMain.eq.nButtEdit) then
                NZones=NZones+1
                LZone(i)=.true.
              endif
            else
              if(CheckNumberMain.eq.nButtSelect) then
                UseEDZone(i,KRefBlock)=.false.
              else if(CheckNumberMain.eq.nButtThick) then
                if(NThickEDZone(i,KRefBlock).eq.NThick)
     1             NThickEDZone(i,KRefBlock)=0
              else if(CheckNumberMain.eq.nButtEdit) then
                LZone(i)=.false.
              endif
            endif
          enddo
        else
          if(CheckNumberMain.eq.nButtSelect) then
            UseEDZone(1:NEDZone(KRefBlock),KRefBlock)=
     1        UseEDZoneO(1:NEDZone(KRefBlock))
          else if(CheckNumberMain.eq.nButtThick) then
            NThickEDZone(1:NEDZone(KRefBlock),KRefBlock)=
     1        NThickEDZoneO(1:NEDZone(KRefBlock))
          else if(CheckNumberMain.eq.nButtEdit) then
            NZones=NZonesR
            LZone=LZoneR
          endif
        endif
        call FeQuestRemove(idp)
        if(allocated(UseEDZoneO)) deallocate(UseEDZoneO)
        if(allocated(NThickEDZoneO)) deallocate(NThickEDZoneO)
        if(allocated(LZoneR)) deallocate(LZoneR)
        if(CheckNumberMain.eq.nButtEdit) then
          nz=0
          do i=1,NEDZone(KRefBlock)
            if(LZone(i)) then
              nz=i
              exit
            endif
          enddo
          if(nz.le.0) nz=1
          Navrat=2
          go to 3000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdwUnlock.and.
     1        (CheckNumber.ge.nEdwPrv.and.CheckNumber.le.nEdwPrv+5))
     2  then
        nEdw=CheckNumber
        i=nEdw-nEdwPrv+1
        call FeQuestRealEdwOpen(nEdw,pomc(i),.false.,.false.)
        go to 1500
      else if(CheckType.eq.EventCrwUnlock.and.
     1        (CheckNumber.ge.nCrwPrv.and.CheckNumber.le.nCrwPrv+5))
     2  then
        nCrw=CheckNumber
        i=nCrw-nCrwPrv+1
        call FeQuestCrwOpen(nCrw,kic(i).gt.0)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuRefBlock) then
        KRefBlockNew=RolMenuSelectedQuest(nRolMenuRefBlock)
        Navrat=3
        go to 3000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      Navrat=0
3000  Veta=fln(:ifln)//'.m42'
      if(ich.eq.0) then
        CalcDyn=CrwLogicQuest(nCrwCalcDyn)
        RescaleToFCalc=CrwLogicQuest(nCrwRescaleToFCalc)
        if(NZonesO.le.1) then
          nEdw=nEdwH
          do i=1,6
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.eq.1) then
              HEDZone(1,nzo,KRefBlock)=pom
            else if(i.eq.2) then
              HEDZone(2,nzo,KRefBlock)=pom
            else if(i.eq.3) then
              HEDZone(3,nzo,KRefBlock)=pom
            else if(i.eq.4) then
              PrAngEDZone(nzo,KRefBlock)=pom
            else if(i.eq.5) then
              AlphaEDZone(nzo,KRefBlock)=pom
            else
              BetaEDZone(nzo,KRefBlock)=pom
            endif
            nEdw=nEdw+1
          enddo
        endif
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,xp,kip,6)
        do i=1,MxEDRef
          do j=1,NEDZone(KRefBlock)
            if(LZoneO(j)) then
              if(mod(kip(i),100).lt.10)
     1          KiED(i,j,KRefBlock)=mod(kip(i),10)
              if(kip(i)/100.eq.0) then
                if(i.eq.1) then
                  ScEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.2) then
                  ThickEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.3) then
                  XNormEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.4) then
                  YNormEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.5) then
                  PhiEDZone(j,KRefBlock)=xp(i)
                else  if(i.eq.6) then
                  ThetaEDZone(j,KRefBlock)=xp(i)
                endif
              endif
            endif
          enddo
        enddo
        nEdw=nEdwUB
        do i=1,3
          do j=1,3
            call FeQuestRealFromEdw(nEdw,OrMatEDZone(i,j,KRefBlock))
            nEdw=nEdw+1
          enddo
        enddo
        call FeQuestRealFromEdw(nEdwGMax,GMaxEDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwSGMaxM,SGMaxMEDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwSGMaxR,SGMaxREDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwCSGMaxR,CSGMaxREDZone(KRefBlock))
        call FeQuestIntFromEdw(nEdwIntSteps,EDIntSteps(KRefBlock))
        call FeQuestIntFromEdw(nEdwThreads,EDThreads(KRefBlock))
        if(CrwLogicQuest(nCrwTiltCorr)) then
          EDTiltCorr(KRefBlock)=1
        else
          EDTiltCorr(KRefBlock)=0
        endif
        if(CrwLogicQuest(nCrwPEDT)) then
          EDGeometryIEDT(KRefBlock)=0
        else
          EDGeometryIEDT(KRefBlock)=1
        endif
        EDCommands=EdwStringQuest(nEdwCommands)
        if(Navrat.eq.1) then
          Navrat=-1
          go to 1510
        else if(Navrat.eq.2) then
          Navrat=0
          go to 1400
        endif
        call iom42(1,0,Veta)
        if(Navrat.eq.3) then
          call FeQuestRemove(id)
          KRefBlock=KRefBlockNew
          go to 1100
        endif
        if(FileDiff(Veta,PreviousM42)) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to rewrite M42 file?',
     1                    1)) then
            call CopyFile(PreviousM42,Veta)
            ich=1
            go to 5000
          endif
          if(CalcDynOld.neqv.CalcDyn) then
            call FeFillTextInfo('em40eldiff1.txt',0)
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            call DeleteFile(fln(:ifln)//'.m90')
            ExistM90=.false.
            call EM9CreateM90(1,ich)
            if(.not.CalcDyn) then
              do i=1,mxsc
                if(i.le.NoOfScales(KRefBlock)) then
                  if(sc(i,KRefBlock).le.0.) sc(i,KRefBlock)=1.
                  kis(i,KRefBlock)=1
                else
                  sc(i,KRefBlock)=0.
                  kis(i,KRefBlock)=0
                endif
              enddo
            endif
          endif
        endif
      else if(Navrat.eq.2) then
        Navrat=0
        go to 1400
      endif
5000  call FeQuestRemove(id)
      if(ich.ne.0) call iom42(0,0,Veta)
      ActionED=1
      if(allocated(LZone)) deallocate(LZone,LZoneO)
      KDatBlock=KDatBlockO
      KRefBlock=KRefBlockO
      return
100   format(6f14.6)
      end
