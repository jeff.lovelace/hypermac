      subroutine ZmMult(ia,lasmaxn)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmaxn,KModA(1,ia),
     1               MagPar(ia),.false.)
      kip=max(TRankCumul(itf(ia)),10)+1
      n=nint(AtNum(isf(ia),KPhase))
      if(lasmax(ia).le.0.and.lasmaxn.gt.0) then
        popc(ia)=0.
        do i=1,28
          popc(ia)=popc(ia)+PopCore(i,isf(ia),KPhase)
        enddo
        spopc(ia)=0.
         popv(ia)=float(n)-popc(ia)
        spopv(ia)=0.
         kapa1(ia)=1.
        skapa1(ia)=0.
        KiA(kip  ,ia)=0
        KiA(kip+1,ia)=1
        KiA(kip+2,ia)=1
      endif
      if(lasmax(ia).le.1.and.lasmaxn.gt.1) then
         kapa2(ia)=1.
        skapa2(ia)=0.
        KiA(kip+3,ia)=0
        KiA(kip+4,ia)=0
      endif
      if(lasmaxn.gt.1.and.lasmax(ia).lt.lasmaxn) then
        if(lasmax(ia).gt.1) then
          j=(lasmax(ia)-1)**2+1
        else
          j=2
          popas(1,ia)=0.
        endif
        n=(lasmaxn-1)**2-j+1
        call SetRealArrayTo( popas(j,ia),n,0.)
        call SetRealArrayTo(spopas(j,ia),n,0.)
        kip=kip+3+j
        call SetIntArrayTo(KiA(kip,ia),n,1)
      endif
      lasmax(ia)=lasmaxn
9999  return
      end
