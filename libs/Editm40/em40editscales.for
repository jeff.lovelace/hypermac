      subroutine EM40EditScales(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      dimension tpoma(3),xpoma(3)
      character*80 t80
      character*12 at,pn
      logical Zpet
      KDatBlockIn=KDatBlock
1040  xqd=450.
      mxscs=36-NTwin+1
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      mxscuold=mxscu
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(isPowder) then
        n=1
        if(KManBackg(KDatBlock).gt.0) n=n+1
      else
        n=mxscs
      endif
      il=(n-1)/3+5
      if(NDatBlock.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Edit scale parameters',0,
     1                   LightGray,0,OKForBasicFiles)
      Zpet=.false.
      il=1
      Cislo='TOverall'
      call FeMakeParEdwCrw(id,tpoma(1),xpoma(1),il,Cislo,nEdwOver,
     1                     nCrwOver)
      kip=MxSc+1
      call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
      call FeOpenParEdwCrw(nEdwOver,nCrwOver,pn,pom,KiS(kip,KDatBlock),
     1                     .false.)
      call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Cislo,nEdwLam2,
     1                     nCrwLam2)
      kip=MxSc+4
      call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
      call FeOpenParEdwCrw(nEdwLam2,nCrwLam2,pn,pom,KiS(kip,KDatBlock),
     1                     .false.)
      if(Lam2Corr.ne.1.or.Radiation(KDatBlock).ne.XRayRadiation.or.
     1   isPowder) then
        call FeQuestEdwDisable(nEdwLam2)
        call FeQuestCrwDisable(nCrwLam2)
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      t80='%Maximal number of scales'
      xpom=xqd*.5-20.
      tpom=xpom-8.
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,t80,'R',dpom,EdwYd,1)
      nEdwNumber=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mxscu,.false.)
      call FeQuestEudOpen(EdwLastMade,7-itwph,mxsc-itwph,1,0.,0.,0.)
      if(isPowder) call FeQuestEdwDisable(EdwLastMade)
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
      nButtPrevious=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      il=4
      j=1
      do i=1,n
        k=0
        Cislo='Scale11'
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,Cislo,nEdw,nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      if(n.le.2) il=il+1
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      dpom=60.
      pom=20.
      xpom=(xqd-2.*dpom-pom)*.5
      do i=1,2
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
      mp=1
1250  kip=mp
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      do i=1,n
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        if(i.le.mxscu) then
          k=KiS(kip,KDatBlock)
        else
          k=0
        endif
        if(i.le.mxscu) then
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,k,.false.)
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
        kip=kip+1
      enddo
1450  if(.not.isPowder) then
        if(mxscu.gt.n) then
          call FeQuestButtonOpen(nButtPrevious,ButtonOff)
          call FeQuestButtonOpen(nButtNext,ButtonOff)
        else
          call FeQuestButtonClose(nButtPrevious)
          call FeQuestButtonClose(nButtNext)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNumber) then
        call FeQuestIntFromEdw(nEdwNumber,mxscu)
        if(mxscu.gt.mxscuold) then
          nn=mxscu-mxscuold
          do i=mxscuold+NTwin-1,mxscuold+1,-1
            KiS(i+nn,KDatBlock)=KiS(i,KDatBlock)
            KiS(i,KDatBlock)=0
          enddo
        else if(mxscu.lt.mxscuold) then
          nn=mxscuold-mxscu
          do i=mxscuold+1,mxscuold+NTwin-1
            KiS(i-nn,KDatBlock)=KiS(i,KDatBlock)
            KiS(i,KDatBlock)=0
          enddo
        endif
        mxscutw=mxscu+itwph-1
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=mp
        do i=1,n
          if(kip.gt.mxscuold.and.kip.le.mxscu) then
            call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
            k=KiS(kip,KDatBlock)
            call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,k,.false.)
          else if(kip.gt.mxscu) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          kip=kip+1
        enddo
        mxscuold=mxscu
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          k=1
        else
          k=0
        endif
        call SetIntArrayTo(KiS,mxscu*MxDatBlock,k)
        nCrw=nCrwPrv
        kip=mp
        do i=1,36
          if(kip.le.mxscu) then
            call FeQuestCrwOpen(nCrw,k.eq.1)
          else
            go to 1450
          endif
          nCrw=nCrw+1
          kip=kip+1
        enddo
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     1         CheckNumber.eq.nButtNext)) then
        mpp=mp
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        call FeUpdateParamAndKeys(nEdw,nCrw,sc(mp,KDatBlock),
     1                            KiS(mp,KDatBlock),mp+n)
        if(CheckNumber.eq.nButtNext) then
           i=mp+n
           if(i.lt.mxscu) then
             mp=i
           else
             go to 1500
           endif
        else if(CheckNumber.eq.nButtPrevious) then
           i=mp-n
           if(i.gt.0) then
             mp=i
           else
             go to 1500
           endif
        endif
        go to 1250
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        kip=MxSc+1
        call FeUpdateParamAndKeys(nEdwOver,nCrwOver,OverAllB(KDatBlock),
     1                            KiS(kip,KDatBlock),1)
        if(Lam2Corr.eq.1.and.Radiation(KDatBlock).eq.XRayRadiation.and.
     1     .not.isPowder) then
          kip=MxSc+4
          call FeUpdateParamAndKeys(nEdwLam2,nCrwLam2,ScLam2(KDatBlock),
     1                              KiS(kip,KDatBlock),1)
        endif
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        call FeUpdateParamAndKeys(nEdw,nCrw,sc(mp,KDatBlock),
     1                            KiS(mp,KDatBlock),mp+MxScU-1)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
