      subroutine EM40RenameAccordingToAtomType(ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*8 AtNew
      dimension natp(:)
      logical EqIgCase
      allocatable natp
      allocate(natp(NAtFormula(KPhase)))
      ich=0
      call SetIntArrayTo(natp,NAtFormula(KPhase),0)
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          jj=isf(ia)
          do j=1,jj
            if(EqIgCase(AtType(jj,KPhase),AtType(j,KPhase))) exit
          enddo
1100      natp(j)=natp(j)+1
          write(AtNew,'(a2,i4)') AtType(j,KPhase),natp(j)
          call zhusti(AtNew)
          do k=1,NAtActive
            if(.not.LAtActive(k)) then
              if(EqIgCase(AtNew,Atom(IAtActive(k)))) go to 1100
            endif
          enddo
          call CrlAtomNamesSave(Atom(ia),AtNew,1)
          Atom(ia)=AtNew
        endif
      enddo
      deallocate(natp)
      return
      end
