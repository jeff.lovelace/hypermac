      subroutine EM40SplitAtoms
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Shift(3),trpom(36),upom(9),cpom(3,3),rpom(3,3)
      integer RolMenuSelectedQuest,ip(1)
      logical CrwLogicQuest,EqIgCase,FeYesNo,EqRV0
      logical, allocatable :: NUsed(:)
      character*256 EdwStringQuest
      character*80  Veta,t80
      character*8   At,AtPom
      data tpisq/19.7392088/
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      napp=0
      iaw=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          iaw=iaw+1
          iaLast=IAtActive(i)
        endif
      enddo
      NAtTest=1000
      if(allocated(NUsed)) deallocate(NUsed)
      allocate(NUsed(NAtTest))
      xqd=400.
      id=NextQuestId()
      iaa=0
      do iac=1,NAtActive
        if(.not.LAtActive(iac)) cycle
        call SetRealArrayTo(Shift,3,0.)
        ia=IAtActive(iac)
        iaa=iaa+1
        isw=iswa(ia)
        ksw=kswa(ia)
        isfp=isf(ia)
        Veta='Split atomic position for : '//Atom(ia)(:idel(Atom(ia)))
        il=7
        if(itf(ia).gt.1) il=il+3
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
        il=1
        tpom=5.
        Veta='%Name of the split atom:'
        xpom=tpom+FeTxLengthUnder(Veta)+70.
        dpom=80.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        Veta=Atom(ia)(:idel(Atom(ia)))//''''
        call FeQuestStringEdwOpen(EdwLastMade,Veta)
        nEdwAtName=EdwLastMade
        Veta='%Info'
        dpomb=FeTxLengthUnder(Veta)+30.
        xpomb=xpom+dpom+15.
        call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
        nButtInfo=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,'Atomic %type','L',
     1                          dpom,EdwYd,1)
        nRolMenuAtType=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                          NAtFormula(KPhase),isfp)
        il=il+1
        Veta='%Relative occupancy of the split atom:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,.5,.false.,.false.)
        nEdwOcc=EdwLastMade
        if(itf(ia).gt.1) then
          tpomc=5.
          il=il+1
          Veta='Shift of the split atom:'
          call FeQuestLblMake(id,tpomc,il,Veta,'L','N')
          xpomc=tpomc+50.
          tpomc=xpomc+CrwgXd+5.
          Veta='defined %manually'
          do j=1,2
            il=il+1
            call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                          CrwYd,1,1)
            if(j.eq.1) then
              nCrwShiftManual=CrwLastMade
              tpom=tpomc+FeTxLengthUnder(Veta)+5.
              call FeQuestEdwMake(id,tpom,il,xpom,il,'->','L',dpom,
     1                            EdwYd,1)
              nEdwShift=EdwLastMade
              Veta='derived from %ADP'
            else
              nCrwShiftFromADP=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,j.eq.2)
          enddo
          xpomc=xpomc-50.
          tpomc=tpomc-50.
          il=il+1
          Veta='Re%set ADP to isotropic'
          call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                        CrwYd,0,0)
          nCrwToIso=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,.true.)
        else
          il=il+1
          Veta='%Shift of the split atom:'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                        EdwYd,1)
          nEdwShift=EdwLastMade
          call FeQuestRealAEdwOpen(nEdwShift,Shift,3,.false.,
     1                             .false.)
          nCrwShiftManual=0
          nCrwShiftFromADP=0
          nCrwToIso=0
          xpomc=5.
          tpomc=xpomc+CrwgXd+5.
        endif
        il=il+1
        Veta='%Generate restrict commands for REFINE'
        call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                      CrwYd,1,0)
        nCrwGenRest=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        tpomc=tpomc+15.
        xpomc=xpomc+15.
        Veta='R%estrict the split atoms to have identical positions'
        call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                      CrwYd,0,0)
        nCrwRestIdentical=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        Veta='Avoi%d'
        if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
        dpoma=FeTxLengthUnder(Veta)+20.
        dpomb=FeTxLengthUnder('XXXX')+20.
        xpom=xqd*.5-dpoma-dpomb*.5-20.
        dpom=dpoma
        do i=1,3
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(i.eq.1) then
            Veta='%Quit'
            nButtAvoid=ButtonLastMade
          else if(i.eq.2) then
            Veta='Appl%y'
            if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
            nButtQuit=ButtonLastMade
          else if(i.eq.3) then
            nButtApply=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          xpom=xpom+dpom+20.
          if(i.eq.1) then
            dpom=dpomb
          else
            dpom=dpoma
          endif
        enddo
        call FeQuestMouseToButton(nButtApply)
1400    if(nCrwShiftManual.ne.0) then
          if(CrwLogicQuest(nCrwShiftManual)) then
            call FeQuestRealAEdwOpen(nEdwShift,Shift,3,.false.,
     1                               .false.)
            if(CrwLogicQuest(nCrwGenRest).and.EqRV0(Shift,3,.000001))
     1        then
              call FeQuestCrwOpen(nCrwRestIdentical,.true.)
            else
              call FeQuestCrwDisable(nCrwRestIdentical)
            endif
          else
            call FeQuestEdwDisable(nEdwShift)
            call FeQuestCrwDisable(nCrwRestIdentical)
          endif
        else
          if(CrwLogicQuest(nCrwGenRest).and.EqRV0(Shift,3,.000001)) then
            call FeQuestCrwOpen(nCrwRestIdentical,.true.)
          else
            call FeQuestCrwDisable(nCrwRestIdentical)
          endif
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw.and.
     1     (CheckNumber.eq.nCrwShiftManual.or.
     2      CheckNumber.eq.nCrwShiftFromADP)) then
          go to 1400
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInfo)
     1    then
          call FeFillTextInfo('em40newatomcomplete.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          go to 1500
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuAtType) then
          isfp=RolMenuSelectedQuest(nRolMenuAtType)
          go to 1500
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwGenRest)
     1    then
          isfp=RolMenuSelectedQuest(nRolMenuAtType)
          go to 1400
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwShift) then
          call FeQuestRealAFromEdw(nEdwShift,Shift)
          go to 1400
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtName)
     1    then
          AtPom=EdwStringQuest(nEdwAtName)
          if(AtPom.eq.' ') go to 1500
          call zhusti(AtPom)
          At=AtPom
          idl=idel(AtPom)
          if(AtPom(idl:idl).eq.'*') then
            idl=idl-1
            if(idl.le.0) go to 1650
            call SetLogicalArrayTo(NUsed,NAtTest,.false.)
            do j=1,2
              if(j.eq.1) then
                i1=1
                i2=NAtInd
              else
                i1=NAtMolFr(1,1)
                i2=NAtAll
              endif
              do i=i1,i2
                Veta=Atom(i)
                if(LocateSubstring(Veta,AtPom(:idl),.false.,.true.)
     1             .gt.0) then
                  k=idl
                  call StToInt(Veta,k,ip,1,.false.,ichp)
                  if(ichp.ne.0) cycle
                  NUsed(ip(1))=.true.
                endif
              enddo
            enddo
            do i=1,NAtTest
              if(.not.NUsed(i)) then
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                At=AtPom(:idl)//Cislo(:idel(Cislo))
                go to 1650
              endif
            enddo
          endif
1650      call uprat(At)
          call AtCheck(At,ichp,j)
          if(ichp.eq.1) then
            call FeChybne(-1.,YBottomMessage,'Unacceptable symbol in '//
     1                    'the names string, try again.',' ',
     2                    SeriousError)
            go to 1500
          endif
          call FeQuestStringEdwOpen(nEdwAtName,At)
          n=0
          do j=1,NAtFormula(KPhase)
            k=idel(AtType(j,KPhase))
            if(k.eq.0) cycle
            k=LocateSubstring(At,AtType(j,KPhase)(:k),.false.,.true.)
            if(k.ne.1) then
              cycle
            else
              n=j
              if(idel(AtType(j,KPhase)).eq.2) go to 1730
            endif
          enddo
          if(n.eq.0) go to 1500
1730      isfp=n
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfp)
          go to 1500
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1    then
          Veta=' '
          if(napp.gt.0) then
            write(Veta,'(i5)') napp
            call zhusti(Veta)
            Veta='Do you want to discard '//Veta(:idel(Veta))//
     1           ' split atom'
            if(napp.gt.1) Veta=Veta(:idel(Veta))//'s?'
            if(FeYesNo(-1.,YBottomMessage,Veta,0)) then
              ich=1
            else
              ich=-1
            endif
          else
            Veta='Do you really want quit the procedure for this'
            if(iaa.lt.iaw) then
              write(Cislo,'(i5)') iaw-iaa+1
              call zhusti(Cislo)
              Veta=Veta(:idel(Veta))//' and remaining '//
     1             Cislo(:idel(Cislo))//' atom'
              if(iaw-iaa+1.gt.1) Veta=Veta(:idel(Veta))//'s?'
            else
              Veta=Veta(:idel(Veta))//' atom?'
            endif
            if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) then
              go to 1500
            else
              ich=1
            endif
          endif
          go to 2500
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAvoid)
     1    then
          ich=0
          go to 2500
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtApply) then
          napp=napp+1
          if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
          NAtCalc=NAtCalc+1
          NAtCalcBasic=NAtCalcBasic+1
          if(kmol(ia).gt.0) then
            im=(kmol(ia)-1)/mxp+1
            iam(im)=iam(im)+1
            NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)+1
            NAtPosLen(isw,ksw)=NAtPosLen(isw,ksw)+mam(im)
            call AtSun(NAtPosFr(1,1),NAtAll,NAtPosFr(1,1)+mam(im))
            ia=ia+mam(im)
            call AtSun(ia+1,NAtAll+mam(im),ia+2)
            do i=iac+1,NAtActive
              IAtActive(i)=IAtActive(i)+mam(im)
            enddo
          else
            NAtIndLen(isw,ksw)=NAtIndLen(isw,ksw)+1
            call AtSun(ia+1,NAtAll,ia+2)
          endif
          call EM40UpdateAtomLimits
          call AtCopy(ia,ia+1)
          isf(ia+1)=isfp
          Atom(ia+1)=At
          call FeQuestRealFromEdw(nEdwOcc,pom)
          ai(ia)=(1.-pom)*ai(ia)
          ai(ia+1)=pom*ai(ia+1)
          if(nCrwToIso.gt.0) then
            if(CrwLogic(nCrwShiftManual)) then
              call FeQuestRealAFromEdw(nEdwShift,Shift)
            else
              call srotb(TrToOrtho(1,isw,ksw),
     1                   TrToOrtho(1,isw,ksw),trpom)
              call MultM(trpom,beta(1,ia),upom,6,6,1)
              do j=1,6
                upom(j)=upom(j)/tpisq
              enddo
              do m=1,6
                call indext(m,j,k)
                cpom(j,k)=upom(m)
                if(j.ne.k) cpom(k,j)=upom(m)
              enddo
              call qln(cpom,rpom,upom,3)
              jmin=1
              jmax=1
              pmin=upom(1)
              pmax=upom(1)
              do j=2,3
                if(upom(j).gt.pmax) then
                  pmax=max(pmax,upom(j))
                  jmax=j
                endif
                if(upom(j).lt.pmin) then
                  pmin=min(pmin,upom(j))
                  jmin=j
                endif
              enddo
              pom=1.5*sqrt(pmax-pmin)
              do j=1,3
                upom(j)=pom*rpom(j,jmax)
              enddo
              call Multm(TrToOrthoI(1,isw,ksw),upom,Shift,3,3,1)
              do j=1,3
                Shift(j)=Shift(j)*CellPar(j,isw,ksw)
              enddo
c              call trmat(rpom,trpom,3,3)
c              call UnitMat(cpom,3)
c              do j=1,3
c                cpom(j,j)=upom(j)
c              enddo
c              call MultM(cpom,trpom,upom,3,3,3)
c              call MultM(rpom,upom,trpom,3,3,3)
c              if(lite(ksw).eq.0) call EM40SwitchBetaU(0,0)
            endif
            if(CrwLogicQuest(nCrwToIso)) then
              call ZMTF21(ia)
              call ZMTF21(ia+1)
            endif
          else
            call FeQuestRealAFromEdw(nEdwShift,Shift)
          endif
          if(CrwLogicQuest(nCrwGenRest)) then
            call iom40(1,0,fln(:ifln)//'.m40')
            IgnoreW=.true.
            call RefOpenCommands
            IgnoreW=.false.
            lni=NextLogicNumber()
            call OpenFile(lni,fln(:ifln)//'_restric.tmp','formatted',
     1                    'unknown')
            lno=NextLogicNumber()
            call OpenFile(lno,fln(:ifln)//'_restrin.tmp','formatted',
     1                    'unknown')
2000        read(lni,FormA,end=2200) Veta
            k=0
            call kus(Veta,k,t80)
            if(t80(1:1).eq.'!') go to 2100
            call kus(Veta,k,t80)
            if(.not.EqIgCase(t80,Atom(ia)).and.
     1         .not.EqIgCase(t80,Atom(ia+1))) go to 2100
            call kus(Veta,k,t80)
            call kus(Veta,k,t80)
            if(.not.EqIgCase(t80,Atom(ia)).and.
     1         .not.EqIgCase(t80,Atom(ia+1))) go to 2100
            if(k.lt.len(Veta)) go to 2100
            go to 2000
2100        write(lno,FormA) Veta(:idel(Veta))
            go to 2000
2200        if(CrwLogicQuest(nCrwRestIdentical)) then
              Cislo=' 1 '
            else
              Cislo=' 2 '
            endif
            Veta='restric '//Atom(ia)(:idel(Atom(ia)))//Cislo(1:3)//
     1           Atom(ia+1)(:idel(Atom(ia+1)))
            write(lno,FormA) Veta(:idel(Veta))
            close(lni)
            close(lno)
            call MoveFile(fln(:ifln)//'_restrin.tmp',
     1                    fln(:ifln)//'_restric.tmp')
            call RefRewriteCommands(1)
          endif
          do j=1,3
            x(j,ia+1)=x(j,ia)+ai(ia+1)*Shift(j)/CellPar(j,isw,ksw)
            x(j,ia  )=x(j,ia)-ai(ia)  *Shift(j)/CellPar(j,isw,ksw)
          enddo
          PrvniKiAtomu(ia+1)=PrvniKiAtomu(ia)+DelkaKiAtomu(ia)
          DelkaKiAtomu(ia+1)=0
          call ShiftKiAt(ia+1,itf(ia+1),ifr(ia+1),lasmax(ia+1),
     1                   KModA(1,ia+1),MagPar(ia+1),.false.)
          do j=iac+1,NAtActive
            IAtActive(j)=IAtActive(j)+1
          enddo
          iaLast=iaLast+1
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
2500    call FeQuestRemove(id)
        if(ich.ne.0) go to 3000
      enddo
      go to 3500
3000  if(ich.gt.0) then
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
        call CopyFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
3500  call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z50')
      if(allocated(NUsed)) deallocate(NUsed)
      return
      end
