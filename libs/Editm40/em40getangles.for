      subroutine EM40GetAngles(rot,irot,euler)
      include 'fepc.cmn'
      dimension rot(9),euler(3),rotp(9)
      call matinv(rot,rotp,pom,3)
      if(pom.ge.0.) then
        zn=1.
      else
        zn=-1.
      endif
      if(irot.eq.0) then
        if(abs(zn*rot(9)).gt..99995) then
          euler(2)=90.-sign(90.,zn*rot(9))
          euler(3)=0.
          ps=zn*rot(2)
          pc=zn*rot(1)
        else
          euler(2)=acos(zn*rot(9))/torad
          if(abs(rot(3)).le..0001.and.abs(rot(6)).le..0001) then
            euler(3)=0.
          else
            euler(3)=atan2(zn*rot(3),zn*rot(6))/torad
          endif
          ps= zn*rot(7)
          pc=-zn*rot(8)
        endif
      else
        if(abs(zn*rot(3)).gt..99995) then
          euler(2)=-sign(90.,zn*rot(3))
          euler(3)=0.
          ps=-zn*rot(4)
          pc= zn*rot(5)
        else
          euler(2)=-asin(zn*rot(3))/torad
          if(abs(rot(6)).le..0001.and.abs(rot(9)).le..0001) then
            euler(3)=0.
          else
            euler(3)=atan2(zn*rot(6),zn*rot(9))/torad
          endif
          ps=zn*rot(2)
          pc=zn*rot(1)
        endif
      endif
      euler(1)=atan2(ps,pc)/torad
      return
      end
