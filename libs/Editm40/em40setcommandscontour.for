      subroutine EM40SetCommandsContour
      use Atoms_mod
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      character*256 t256
      integer ColorOrder
      dimension xb(3),xc(3),u(3),ug(3),v(3),vg(3),trp(9),trpi(9)
      equivalence (t80,t256)
      n=NAtCalc
      allocate(AtBrat(n))
      call ConPrelim
      DrawAtN=KeepNH(1)+1
      DrawAtName(1)=KeepAtCentr(1)
      DrawAtColor(1)=ColorOrder('Red')
      call SetRealArrayTo(DrawAtBondLim,4,1.2)
      call SetLogicalArrayTo(DrawAtSkip,4,.false.)
      call SetIntArrayTo(DrawAtColor(2),3,ColorOrder('Green'))
      do i=1,KeepNH(1)
        DrawAtName(i+1)=KeepAtH(1,i)
      enddo
      call SetRealArrayTo(XPlane(1,1),3,0.)
      do i=1,KeepNH(1)
        call AddVek(x(1,KeepNAtH(1,i)),XPlane(1,1),XPlane(1,1),3)
      enddo
      pom=1./float(KeepNH(1))
      do i=1,3
        XPlane(i,1)=XPlane(i,1)*pom
      enddo
      call CopyVek(x(1,KeepNAtCentr(1)),xb,3)
      call atsym(KeepAtNeigh(1,1),i,xc,v,vg,ISym,ich)
      if(ich.ne.0) then
        call FeChybne(-1.,-1.,'the neighbour atom "'//
     1                KeepAtNeigh(1,1)(:idel(KeepAtNeigh(1,1)))//
     2                '" isn''t present on M40.',' ',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      isw=iswa(i)
      do i=1,3
        u(i)=xb(i)-xc(i)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      uu=scalmul(u,ug)
      pomm=2.
      do i=1,3
        call SetRealArrayTo(v,3,0.)
        v(i)=1.
        call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
        uv=scalmul(u,vg)
        vv=scalmul(v,vg)
        pom=abs(uv/sqrt(uu*vv))
        if(pom.lt.pomm) then
          pomm=pom
          call CopyVek(v,xc,3)
        endif
      enddo
      call RefMakeTrMat(u,xc,3,1,trp,trpi)
      if(KeepType(1).eq.IdKeepHTetraHed) then
        pom1=0.3333333
        pom2=0.9428090
      else
        pom1=0.5
        pom2=0.8660254
      endif
      DAngle=90.
      v(3)=pom1
      pom= pom2
      Angle=0.
      do i=2,3
        v(1)=pom*cos(Angle*ToRad)
        v(2)=pom*sin(Angle*ToRad)
        call CopyVek(xb,XPlane(1,i),3)
        call cultm(trpi,v,XPlane(1,i),3,3,1)
        Angle=Angle+DAngle
      enddo
      DeltaPlane=0.1
      ScopePlane(1)=3.
      ScopePlane(2)=3.
      ScopePlane(3)=0.
      ShiftPlane(1)=1.5
      ShiftPlane(2)=1.5
      ShiftPlane(3)=0.
9999  return
      end
