      subroutine EM40TransAtSelected(ich)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension GammaIntP(9),GammaIntPI(9),kwz(3),kwp(3)
      integer GammaInt(9,24),CoDelat
      logical eqiv,BratMol(mxpm),SwitchedToHarmIn,DeallocAt,Expand,
     1        DeallocTr
      equivalence (IdNumbers(1),JenAtomy),
     1            (IdNumbers(2),JenMolekuly),
     2            (IdNumbers(3),JakAtomyTakMolekuly)
      Expand=.false.
      go to 1100
      entry EM40ExpandAtSelected(ich)
      Expand=.true.
1100  DeallocAt=.false.
      call SetLogicalArrayTo(BratMol,mxp*mxm,.false.)
      isw=-1
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        if(isw.lt.0) then
          isw=iswa(ia)
        else if(isw.ne.iswa(ia)) then
          isw=0
          go to 1130
        endif
      enddo
1130  CoDelat=JenAtomy
      go to 1200
      entry EM40TransMol(ich)
      Expand=.false.
      go to 1150
      entry EM40ExpandMol(ich)
      Expand=.true.
1150  CoDelat=JenMolekuly
      DeallocAt=.false.
      call EM40DefMolGroup(BratMol,ich)
      if(ich.ne.0) go to 9999
      isw=-1
      n=0
      do i=1,NMolec
        do j=1,mam(i)
          n=n+1
          if(BratMol(n)) then
            if(isw.lt.0) then
              isw=iswmol(i)
            else if(isw.ne.iswmol(i)) then
              isw=0
              go to 1200
            endif
          endif
        enddo
      enddo
1200  if(Expand) then
        NTrans=24
      else
        NTrans=1
      endif
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      DeallocTr=.true.
      call EM40ReadTr(Expand,isw,ich)
      if(ich.ne.0) go to 9999
      go to 2000
      entry EM40TransAtFromTo(ifrom,ito,ich)
      Expand=.false.
      go to 1300
      entry EM40ExpandAtFromTo(ifrom,ito,ich)
      Expand=.true.
1300  i1=ifrom
      i2=ito
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      CoDelat=JenAtomy
      go to 1500
      entry EM40TransAll(ich)
      Expand=.false.
      go to 1400
      entry EM40ExpandAll(ich)
      Expand=.true.
1400  i1=1
      i2=NAtInd
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      n=0
      do i=1,NMolec
        do j=1,mam(i)
          n=n+1
          BratMol(n)=kswmol(i).eq.KPhase
        enddo
      enddo
      CoDelat=JakAtomyTakMolekuly
1500  if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      n=max(i2-i1+1,1)
      allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      DeallocAt=.true.
      DeallocTr=.false.
      ich=0
      NAtActive=0
      do i=i1,i2
        if(kswa(i).eq.KPhase) then
          NAtActive=NAtActive+1
          IAtActive(NAtActive)=i
          LAtActive(NAtActive)=.true.
        endif
      enddo
2000  SwitchedToHarmIn=SwitchedToHarm
      if(.not.SwitchedToHarmIn) call TrOrtho(0)
      if(CoDelat.eq.JenMolekuly) go to 3000
      iswo=-1
      kmodxp=1
      do i=NAtActive,1,-1
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        isw=iswa(ia)
        if(isw.ne.iswo) then
          do ntr=1,ntrans
            do j=4,NDim(KPhase)
              do k=4,NDim(KPhase)
                GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1                    TransMat(k+(j-1)*NDim(KPhase),ntr,isw)
              enddo
            enddo
            call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
            do j=1,NDimI(KPhase)*NDimI(KPhase)
              GammaInt(j,ntr)=nint(GammaIntPI(j))
            enddo
            iswo=isw
          enddo
          kmodxp=1
        endif
        kmodmx=MaxUsedKw(KPhase)
        do ntr=1,ntrans
          do 2200j=kmodxp,kmodmx
            call multmi(kw(1,j,KPhase),GammaInt(1,ntr),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do k=1,NDimI(KPhase)
              kwz(k)=-kwp(k)
            enddo
            do k=1,mxw
              if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                TransKwSym(j,ntr)= k
                go to 2200
              else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                TransKwSym(j,ntr)=-k
                go to 2200
              endif
            enddo
            call FeChybne(-1.,-1.,'some of modulation waves are not'//
     1                    ' defined.','The transformation cannot be '//
     2                    'performed.',SeriousError)
            ErrFlag=1
            go to 5000
2200      continue
        enddo
        kmodxp=kmodmx+1
        if(Expand) then
          do ntr=ntrans,1,-1
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(1)
            call AtSun(ia+1,NAtAll,ia+2)
            DelkaKiAtomu(ia+1)=0
            PrvniKiAtomu(ia+1)=PrvniKiAtomu(ia)+DelkaKiAtomu(ia)
            NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
            NAtIndLenAll(KPhase)=NAtIndLenAll(KPhase)+1
            NAtInd=NAtInd+1
            NAtAll=NAtAll+1
            NAtCalc=NAtCalc+1
            NAtCalcBasic=NAtCalcBasic+1
            call EM40TrAt(ia,ia+1,isw,ntr)
          enddo
        else
          call EM40TrAt(ia,ia,isw,1)
        endif
      enddo
      call EM40UpdateAtomLimits
3000  if(CoDelat.eq.JenAtomy.or.NMolec.le.0) go to 5000
      n=0
      iswo=-1
      kmodxp=1
      if(Expand) then
        call ReallocateMolecules(0,mxp*NTrans)
        call ReallocateAtoms(mxp*NTrans)
      endif
      NAtMolSh=0
      do i=1,NMolec
        if(kswmol(i).ne.KPhase) cycle
        mami=mam(i)
        jip=mami+mxp*(i-1)
        isw=iswmol(i)
        if(isw.ne.iswo) then
          do ntr=1,ntrans
            do j=4,NDim(KPhase)
              do k=4,NDim(KPhase)
                GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1            TransMat(k+(j-1)*NDim(KPhase),ntr,isw)
              enddo
            enddo
            call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
            do j=1,NDimI(KPhase)*NDimI(KPhase)
              GammaInt(j,ntr)=nint(GammaIntPI(j))
            enddo
            iswo=isw
          enddo
          kmodxp=1
        endif
        kmodmx=MaxUsedKw(KPhase)
        do ntr=1,ntrans
          do 3500j=kmodxp,kmodmx
            call multmi(kw(1,j,KPhase),GammaInt(1,ntr),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do k=1,NDimI(KPhase)
              kwz(k)=-kwp(k)
            enddo
            do k=1,mxw
              if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                TransKwSym(j,ntr)= k
                go to 3500
              else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                TransKwSym(j,ntr)=-k
                go to 3500
              endif
            enddo
            call FeChybne(-1.,-1.,'some of modulation waves are not'//
     1                    ' defined.','The transformation cannot be '//
     2                    'performed.',SeriousError)
            ErrFlag=1
            go to 5000
3500      continue
        enddo
        kmodxp=kmodmx+1
        do ip=1,mami
          n=n+1
          if(.not.BratMol(n)) cycle
          ji=ip+mxp*(i-1)
          if(Expand) then
            do ntr=1,ntrans
              jip=jip+1
              DelkaKiMolekuly(jip)=0
              PrvniKiMolekuly(jip)=PrvniKiMolekuly(jip-1)+
     1                             DelkaKiMolekuly(jip-1)
              call EM40TrMol(ji,jip,isw,ntr,0)
              mam(i)=mam(i)+1
              NAtMolSH=NAtMolSH+iam(i)
              NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+iam(i)
            enddo
          else
            call EM40TrMol(ji,ji,isw,1,0)
          endif
        enddo
      enddo
      if(NAtMolSh.gt.0) then
        call ReallocateAtoms(NAtMolSh)
        call AtSun(NAtMolFrAll(1),NAtAll,NAtMolFrAll(1)+NAtMolSh)
      endif
5000  if(.not.SwitchedToHarmIn) call TrOrtho(1)
      call EM40UpdateAtomLimits
9999  if(DeallocAt.and.allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(DeallocTr.and.allocated(TransMat)) call DeallocateTrans
      return
      end
