      subroutine MolSun(iod,ido,jod)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(jod.le.iod) then
        i1=iod
        i2=ido
        id=1
      else
        i1=ido
        i2=iod
        id=-1
      endif
      do i=i1,i2,id
        k=i+jod-iod
        molname(k)=molname(i)
        iswmol(k)=iswmol(i)
        kswmol(k)=kswmol(i)
        StRefPoint(k)=StRefPoint(i)
        iam(k)=iam(i)
        mam(k)=mam(i)
        npoint(k)=npoint(i)
        kpoint(k)=kpoint(i)
        SmbPGMol(k)=SmbPGMol(i)
        do j=1,npoint(i)
          call CopyVek(rpoint(1,j,i),rpoint(1,j,k),9)
          call CopyVek(tpoint(1,j,i),rpoint(1,j,k),36)
          ipoint(j,k)=ipoint(j,i)
        enddo
        call CopyVek(xm(1,i),xm(1,k),3)
        jk=(k-1)*mxp
        ji=(i-1)*mxp
        do j=1,mam(k)
          ji=ji+1
          jk=jk+1
          aimol(jk)=aimol(ji)
          saimol(jk)=saimol(ji)
          TypeModFunMol(jk)=TypeModFunMol(ji)
          call CopyVekI(KFM(1,ji),KFM(1,jk),3)
          call CopyVekI(KModM(1,ji),KModM(1,jk),3)
          call CopyVekI(KiMol(1,ji),KiMol(1,jk),DelkaKiMolekuly(ji))
          RotSign(jk)=RotSign(ji)
          AtTrans(jk)=AtTrans(ji)
          call CopyVek( trans(1,ji), trans(1,jk),3)
          call CopyVek(strans(1,ji),strans(1,jk),3)
          call CopyVek( euler(1,ji), euler(1,jk),3)
          call CopyVek(seuler(1,ji),seuler(1,jk),3)
          LocMolSystType(jk)=LocMolSystType(ji)
          do k=1,LocMolSystType(ji)
            LocMolSystAx(k,jk)=LocMolSystAx(k,ji)
            do l=1,2
              LocMolSystSt(l,k,jk)=LocMolSystSt(l,k,ji)
              call CopyVek(LocMolSystX(1,l,k,ji),LocMolSystX(1,l,k,jk),
     1                     3)
            enddo
          enddo
          if(ktls(i).ne.0) then
            call CopyVek( tt(1,ji), tt(1,jk),6)
            call CopyVek(stt(1,ji),stt(1,jk),6)
            call CopyVek( tl(1,ji), tl(1,jk),6)
            call CopyVek(stl(1,ji),stl(1,jk),6)
            call CopyVek( ts(1,ji), ts(1,jk),9)
            call CopyVek(sts(1,ji),sts(1,jk),9)
          endif
          if(KModM(1,jk).gt.0) then
            a0m(jk)=a0m(ji)
            sa0m(jk)=sa0m(ji)
            k=KModM(1,jk)
            call CopyVek( axm(1,ji), axm(1,jk),k)
            call CopyVek(saxm(1,ji),saxm(1,jk),k)
            call CopyVek( aym(1,ji), aym(1,jk),k)
            call CopyVek(saym(1,ji),saym(1,jk),k)
          endif
          k=3*KModM(2,jk)
          if(k.gt.0) then
            call CopyVek( utx(1,1,ji), utx(1,1,jk),k)
            call CopyVek(sutx(1,1,ji),sutx(1,1,jk),k)
            call CopyVek( uty(1,1,ji), uty(1,1,jk),k)
            call CopyVek(suty(1,1,ji),suty(1,1,jk),k)
            call CopyVek( urx(1,1,ji), urx(1,1,jk),k)
            call CopyVek(surx(1,1,ji),surx(1,1,jk),k)
            call CopyVek( ury(1,1,ji), ury(1,1,jk),k)
            call CopyVek(sury(1,1,ji),sury(1,1,jk),k)
          endif
          k=6*KModM(3,jk)
          if(k.gt.0) then
            call CopyVek( ttx(1,1,ji), ttx(1,1,jk),k)
            call CopyVek(sttx(1,1,ji),sttx(1,1,jk),k)
            call CopyVek( tty(1,1,ji), tty(1,1,jk),k)
            call CopyVek(stty(1,1,ji),stty(1,1,jk),k)
            call CopyVek( tlx(1,1,ji), tlx(1,1,jk),k)
            call CopyVek(stlx(1,1,ji),stlx(1,1,jk),k)
            call CopyVek( tly(1,1,ji), tly(1,1,jk),k)
            call CopyVek(stly(1,1,ji),stly(1,1,jk),k)
            k=9*KModM(3,jk)
            call CopyVek( tsx(1,1,ji), tsx(1,1,jk),k)
            call CopyVek(stsx(1,1,ji),stsx(1,1,jk),k)
            call CopyVek( tsy(1,1,ji), tsy(1,1,jk),k)
            call CopyVek(stsy(1,1,ji),stsy(1,1,jk),k)
          endif
          if(KModM(1,jk).gt.0.or.KModM(2,jk).gt.0.or.
     1       KModM(3,jk).gt.0) then
            phfm(jk)=phfm(ji)
            sphfm(jk)=sphfm(ji)
          endif
          PrvniKiMolekuly(jk)=PrvniKiMolekuly(ji)
          DelkaKiMolekuly(jk)=DelkaKiMolekuly(ji)
        enddo
      enddo
      return
      end
