      subroutine EM40PutAtom(iao,im)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      integer CrwStateQuest,EdwStateQuest
      logical CrwLogicQuest
      integer TypeModFunN
      dimension kmodanp(7)
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          if(lasmaxa.ge.3) then
            if(EdwStateQuest(nEdwMult).eq.EdwOpened) then
              call FeQuestIntFromEdw(nEdwMult,lasmaxa)
              lasmaxa=lasmaxa+3
            endif
          endif
        endif
      else
        nEdw=nEdwModFirst
        j=itfa+1
        if(im.gt.0.and.j.eq.1) j=3
        do i=1,7
          if(i.le.j) then
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              call FeQuestIntFromEdw(nEdw,kmodan(i))
              cmoda(i)=kmodan(i)
            else
              kmodan(i)=KModA(i,iao)
            endif
            nEdw=nEdw+1
          else
            kmodan(i)=0
          endif
        enddo
        TypeModFunN=0
        if(CrwStateQuest(nCrwCrenel).eq.CrwLocked) then
          kfsn=-1
        else
          kfsn=0
          if(CrwStateQuest(nCrwCrenel).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwCrenel)) then
              kfsn=1
              nCrw=nCrwHarm
              do i=1,4
                if(CrwLogicQuest(nCrw)) then
                  TypeModFunN=i-1
                  exit
                endif
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        if(CrwStateQuest(nCrwSawTooth).eq.CrwLocked) then
          kfxn=-1
        else
          kfxn=0
          if(CrwStateQuest(nCrwSawTooth).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              kfxn=1
            else if(CrwLogicQuest(nCrwZigZag)) then
              kfxn=2
            endif
          endif
        endif
        if(nEdwMagnetic.gt.0) then
          if(CrwLogicQuest(nCrwMagnetic)) then
            call FeQuestIntFromEdw(nEdwMagnetic,i)
            MagParA=i+1
          else
            MagParA=0
          endif
        endif
      endif
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(NSel.eq.1) then
            call CrlAtomNamesSave(Atom(ia),AtName,-1)
            Atom(ia)=AtName
          endif
          if(ctfa.gt.0) then
            call ZmAnhi(ia,itfa)
            itf(ia)=itfa
          endif
          if(csfa.gt.0) isf(ia)=isfa
          if(NDimI(KPhase).le.0) then
            if(ChargeDensities) then
              if(clasmaxa.gt.0) call ZmMult(ia,lasmaxa-1)
            endif
          else
            do j=1,7
              k=itf(ia)+1
              if(im.gt.0.and.k.eq.1) k=3
              if(cmoda(j).ge.0.and.j.le.k) then
                kmodanp(j)=kmodan(j)
              else
                kmodanp(j)=-1
              endif
            enddo
            call AtModi(ia,kmodanp)
            if(cmoda(1).gt.0.and.kfsn.ge.0) then
              kfso=KFA(1,ia)
              KFA(1,ia)=min(kfso,kfsn)
              TypeModFun(ia)=TypeModFunN
              if(CHarmLimit.gt.0.) OrthoEps(ia)=CHarmLimit
            endif
            if(cmoda(2).gt.0.and.kfxn.ge.0) then
              kfxo=KFA(2,ia)
              KFA(2,ia)=min(kfxo,kfxn)
              if(KFA(2,ia).gt.0) TypeModFun(ia)=0
            endif
            kip=max(TRankCumul(itf(ia)),10)+1
            if(cmoda(1).gt.0.and.kfsn.ge.0) then
              KFA(1,ia)=kfsn
              k=KModA(1,ia)-NDimI(KPhase)
              if(k.ge.0) then
                if(kfsn.ne.kfso.and.kfsn.ne.0) then
                  if(NDimI(KPhase).eq.1) then
                    pom=0.
                  else
                    if(a0(ia).gt.0.) then
                      pom=a0(ia)**(1./float(NDimI(KPhase)))
                    else
                      pom=0.
                    endif
                  endif
                  do j=1,NDimI(KPhase)
                    k=k+1
                    ax(k,ia)=0.
                    ay(k,ia)=pom
                    KiA(kip,ia)=0
                    KiA(kip+(k-1)*2+1,ia)=0
                    KiA(kip+(k-1)*2+2,ia)=0
                  enddo
                  if(NSel.eq.1) call EM40EditAtomModPar(1,1)
                endif
              endif
              if(k.gt.0) kip=kip+2*k+1
            endif
            if(cmoda(2).gt.0.and.kfxn.gt.0) then
              KFA(2,ia)=kfxn
              k=KModA(2,ia)
              if(k.gt.0) then
                if(kfxn.ne.kfxo.and.kfxn.eq.1) then
                  call SetRealArrayTo(ux(1,k,ia),3,.001)
                  call SetRealArrayTo(uy(1,k,ia),3,0.)
                  uy(2,k,ia)=1.
                  call SetIntArrayTo(KiA(kip+(k-1)*6,ia),6,0)
                endif
              endif
            endif
          endif
        endif
      enddo
      return
      end
