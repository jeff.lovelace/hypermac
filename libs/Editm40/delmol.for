      subroutine DelMol
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*11 jmeno
      i=1
1000  if(i.gt.NMolec) go to 9999
      if(iam(i)/KPoint(i).le.0) then
        isw=iswmol(i)
        KPh=kswmol(i)
        do j=1,mam(i)
          jmeno=MolName(i)
          write(jmeno(9:11),'(''#'',i2)') j
          call zhusti(jmeno)
        enddo
        call molsun(i+1,NMolec,i)
        do j=KPh,KPhase
          if(j.eq.KPh) then
            kp=isw
          else
            kp=1
          endif
          do k=kp,NComp(j)
            if(j.ne.KPh.or.k.ne.kp) NMolecFr(k,j)=NMolecFr(k,j)-1
            NMolecTo(k,j)=NMolecTo(k,j)-1
          enddo
          if(j.ne.KPh) NMolecFrAll(j)=NMolecFrAll(j)-1
          NMolecToAll(j)=NMolecToAll(j)-1
        enddo
        NMolecLen(isw,KPh)=NMolecLen(isw,KPh)-1
        NMolec=NMolec-1
      else
        i=i+1
      endif
      go to 1000
9999  call EM40UpdateMolecLimits
      return
      end
