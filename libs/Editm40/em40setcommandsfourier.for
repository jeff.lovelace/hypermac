      subroutine EM40SetCommandsFourier
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      call DefaultFourier
      call FouRewriteCommands(1)
      call FouOpenCommands
      ScopeType=1
      NacetlInt(nCmdUseWeight)=1
      NacetlInt(nCmdmapa)=6
      NacetlReal(nCmdptstep)=0.1
      NacetlReal(nCmdsnlmxf)=0.5
      NacetlInt(nCmdLPeaks)=0
      NacetlInt(nCmdPPeaks)=-333
      NacetlInt(nCmdNPeaks)=-333
      if(NDimI(KPhase).gt.0) then
        do i=4,NDim(KPhase)
          xrmn(i)=0.
          xrmx(i)=0.
          dd(i)=.1
        enddo
      endif
      call FouRewriteCommands(1)
      return
      end
