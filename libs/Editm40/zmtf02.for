      subroutine ZmTF02(i)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      k=kmol(i)
      l=(k-1)/mxp+1
      itf(i)=2
      do j=1,6
        beta(j,i)=beta(j,i)+tt(j,k)
        sbeta(j,i)=0.
      enddo
      n=NAtMolFr(1,1)-1
      call cultm(tztl(1,i-n),tl(1,k),beta(1,i),6,6,1)
      call cultm(tzts(1,i-n),ts(1,k),beta(1,i),6,9,1)
      if(ktls(l).le.1) then
        do k=kmol(i),kmol(i)+mam(l)-1
          call ShiftKiMol(k,0,KModM(1,k),KModM(2,k),
     1                    KModM(3,k),.false.)
        enddo
      endif
      ktls(l)=ktls(l)-1
      call SetIntArrayTo(KiA(5,i),6,1)
      return
      end
