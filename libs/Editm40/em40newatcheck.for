      subroutine EM40NewAtCheck
      use Atoms_mod
      use Editm40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension KModPom(7)
      character*80 t80,Veta
      integer SbwItemPointerQuest,SbwLnQuest
      data LastMax/-1/
      if(nSbwSel.gt.0) then
        nmx=SbwItemPointerQuest(nSbwSel)
        if(nmx.eq.LastMax) go to 9999
      else
        if(nan.eq.1) then
          nmx=1
        else
          nmx=0
        endif
      endif
      if(nmx.gt.0) then
        if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
        if(kmodxn(nmx).gt.KModAMax(2)) then
          call CopyVekI(KModAMax,KModPom,7)
          KModPom(2)=kmodxn(nmx)
          call ReallocateAtomParams(NAtAll,itfmax,IFrMax,KModPom,
     1                              MagParMax)
        endif
        call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+1)
        NAtCalc=NAtCalc+1
        NAtCalcBasic=NAtCalcBasic+1
        call SetBasicKeysForAtom(NAtCalc)
        call CopyVek(xn(1,nmx),x(1,NAtCalc),3)
        do k=1,kmodxn(nmx)
          call CopyVek(uxn(1,k,nmx),ux(1,k,NAtCalc),3)
          call CopyVek(uyn(1,k,nmx),uy(1,k,NAtCalc),3)
        enddo
        KModA(2,NAtCalc)=kmodxn(nmx)
        iswa(NAtCalc)=iswn
        kswa(NAtCalc)=KPhase
        atom(NAtCalc)='Itself'
        call SpecAt
        call DistForOneAtom(NAtCalc,DMezN,iswn,0)
        call AtSun(NAtMolFr(1,1)+1,NAtAll+1,NAtMolFr(1,1))
        NAtCalc=NAtCalc-1
        NAtCalcBasic=NAtCalcBasic-1
      endif
      call CloseIfOpened(SbwLnQuest(nSbwList))
      call DeleteFile(fln(:ifln)//'_newat.tmp')
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_newat.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(nmx.gt.0) then
        n=1
        MaxOrder(n)=0
        write(t80,101)(xn(m,nmx),m=1,3)
        k=0
        Veta=' '
1412    call kus(t80,k,Cislo)
        idl=idel(Veta)
        if(idl.le.0) then
          Veta=Tabulator//Cislo
        else
          Veta=Veta(:idl)//Tabulator//Cislo
        endif
        if(k.lt.len(t80)) go to 1412
        if(l48) then
          Veta=Veta(:idel(Veta))//' - as read in'
        else
          Veta=Veta(:idel(Veta))//' - as typed in'
        endif
        write(ln,FormA) Veta(:idel(Veta))
        dmin=999.
        do j=1,ndist
          k=ipord(j)
          if(adist(k).eq.'Itself') cycle
          if(dmin.gt.900.) dmin=ddist(k)
          n=n+1
          MaxOrder(n)=k
          write(t80,101)(xdist(m,k),m=1,3),ddist(k),adist(k)
          k=0
          Veta=' '
1414      call kus(t80,k,Cislo)
          idl=idel(Veta)
          if(idl.le.0) then
            Veta=Tabulator//Cislo
          else
            Veta=Veta(:idl)//Tabulator//Cislo
          endif
          if(k.lt.len(t80)) go to 1414
          write(ln,FormA) Veta(:idel(Veta))
        enddo
      endif
1450  call CloseIfOpened(ln)
      call FeQuestSbwMenuOpen(nSbwList,fln(:ifln)//'_newat.tmp')
      if(ndist.gt.1) then
        call FeQuestSbwItemOff(nSbwList,1)
        call FeQuestSbwItemOn(nSbwList,2)
      endif
      if(nLblRho.gt.0.and.nmx.gt.0) then
        write(Cislo,'(f15.3)') Rho(nmx)
        call Zhusti(Cislo)
        Veta='Peak : '//Atomn(nmx)(:idel(Atomn(nmx)))//'    Charge : '//
     1       Cislo(:idel(Cislo))
        call FeQuestLblChange(nLblRho,Veta)
        LastMax=nmx
      endif
      go to 9999
      entry EM40NewAtCheckReset
      LastMax=-1
      return
9999  return
101   format(3(f9.6,1x),f8.2,1x,a8)
      end
