      subroutine EM40MolAtomsToAtomic
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension qrat(3)
      logical MolBrat(mxm),EqIgCase
      call SetLogicalArrayTo(MolBrat,mxm,.false.)
      do 1020i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          do im=1,NMolec
            if(kswmol(im).ne.KPhase) cycle
            if(im.eq.1) then
              NaFrom=NAtMolFr(1,1)
              NaTo=NaFrom+iam(im)/KPoint(im)-1
            else
              NaFrom=NaFrom+iam(im-1)
              NaTo=NaFrom+iam(im)/KPoint(im)-1
            endif
            if(ia.ge.NaFrom.and.ia.le.NaTo) then
              MolBrat(im)=.true.
              go to 1020
            endif
          enddo
        endif
1020  continue
      ibp=NAtMolFr(1,1)
      iap=NAtPosFr(1,1)
      call trortho(0)
      i=0
      do im=1,NMolec
        if(.not.MolBrat(im)) go to 2900
        do id=0,iam(im)-1
          ia=iap+id
          ib=ibp+id
          do j=1,NAtActive
            if(LAtActive(j)) then
              if(ib.eq.IAtActive(j)) go to 1060
            endif
          enddo
          cycle
1060      do j=1,kpoint(im)
            isf(ib)=0
            do ji=1,mam(im)
              kmol(ia)=-kmol(ia)
              if(TypeModFun(ia).eq.1) TypeModFun(ia)=0
              call qbyx(x(1,ia),qrat,iswa(ia))
              kk=0
              do k=1,KModA(1,ia)
                if(k.gt.KModA(1,ia)-NDimI(KPhase).and.KFA(1,ia).ne.0)
     1            then
                  kk=kk+1
                  ax(k,ia)=ax(k,ia)+qrat(kk)-qcnt(kk,ia)
                else
                  fik=0.
                  do m=1,NDimI(KPhase)
                    fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                  enddo
                  sinfik=sin(pi2*fik)
                  cosfik=cos(pi2*fik)
                  xp=ax(k,ia)
                  yp=ay(k,ia)
                  ax(k,ia)= xp*cosfik+yp*sinfik
                  ay(k,ia)=-xp*sinfik+yp*cosfik
                  sxp=sax(k,ia)**2
                  syp=say(k,ia)**2
                  sax(k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                  say(k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                endif
              enddo
              do k=1,KModA(2,ia)
                if(k.eq.KModA(2,ia).and.KFA(2,ia).ne.0) then
                  uy(1,k,ia)=uy(1,k,ia)+qrat(1)-qcnt(1,ia)
                else
                  fik=0.
                  do m=1,NDimI(KPhase)
                    fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                  enddo
                  sinfik=sin(pi2*fik)
                  cosfik=cos(pi2*fik)
                  do l=1,3
                    xp=ux(l,k,ia)
                    yp=uy(l,k,ia)
                    ux(l,k,ia)= xp*cosfik+yp*sinfik
                    uy(l,k,ia)=-xp*sinfik+yp*cosfik
                    sxp=sux(l,k,ia)**2
                    syp=suy(l,k,ia)**2
                    sux(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                    suy(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                  enddo
                endif
              enddo
              do k=1,KModA(3,ia)
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,6
                  xp=bx(l,k,ia)
                  yp=by(l,k,ia)
                  bx(l,k,ia)= xp*cosfik+yp*sinfik
                  by(l,k,ia)=-xp*sinfik+yp*cosfik
                  sxp=sbx(l,k,ia)**2
                  syp=sby(l,k,ia)**2
                  sbx(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                  sby(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                enddo
              enddo
              ia=ia+iam(im)/KPoint(im)
            enddo
            ib=ib+iam(im)/KPoint(im)
          enddo
        enddo
2900    iap=iap+iam(im)*mam(im)
        ibp=ibp+iam(im)
      enddo
      do i=NAtInd+1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        isw=iswa(i)
        kam=NAtIndTo(isw,KPhase)+1
        if(kmol(i).lt.0) then
          im=(-kmol(i)-1)/mxp+1
          call atsave(i,0)
          call atsun(kam,i-1,kam+1)
          call atsave(kam,1)
          NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)-1
          call EM40UpdateAtomLimits
          if(mam(im).eq.1) then
            idl=idel(Atom(kam))
            if(EqIgCase(Atom(kam)(idl:idl),'a'))
     1        Atom(kam)(idl:idl)=' '
          endif
          kmol(i)=0
        endif
      enddo
      i=NAtMolFr(1,1)
3200  if(i.gt.NAtAll) go to 4000
      ji=kmol(i)
      im=(ji-1)/mxp+1
      ksw=kswmol(im)
      isw=iswmol(im)
      if(isf(i).le.0) then
        call atsun(i+1,NAtAll,i)
        NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)-1
        iam(im)=iam(im)-1
        NAtAll=NAtAll-1
      else
        i=i+1
      endif
      go to 3200
4000  call EM40UpdateAtomLimits
      call delmol
      call trortho(1)
      n=NAtInd+NAtMol
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        NAtActive=NAtActive+1
        IAtActive(NAtActive)=i
        NameAtActive(NAtActive)=Atom(i)
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iap+iam(im)/KPoint(im)-1
          do ia=iap,iak
            NAtActive=NAtActive+1
            IAtActive(NAtActive)=ia
            NameAtActive(NAtActive)=Atom(ia)
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      call SetLogicalArrayTo(LAtActive,NAtActive,.true.)
      call EM40MergeAtomsRun(.1,ich)
9999  return
      end
