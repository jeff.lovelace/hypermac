      subroutine EM40Extinction(ich)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension tpoma(3),xpoma(3)
      character*12 at,pn
      character*80 Veta
      dimension ECOld(6,2),ECPom(12)
      integer EdwStateQuest,ExtTypeOld,ExtTensorOld(2),KiPom(12),
     1        CrwStateQuest
      logical CrwLogicQuest,lpom,Zpet
      ECOld=0.
      ECOld(1,1)=.01
      ECOld(1,2)=.01
      ExtTensorOld=0
      if(ExtTensor(KDatBlock).eq.1) then
        if(ExtType(KDatBlock).eq.1) then
          ECOld(1,2)=ec(7,KDatBlock)
          ExtTensorOld(2)=1
        else if(ExtType(KDatBlock).eq.2) then
          ECOld(1,1)=ec(1,KDatBlock)
          ExtTensorOld(1)=1
        else
          ECOld(1,2)=ec(7,KDatBlock)
          ECOld(1,1)=ec(1,KDatBlock)
          ExtTensorOld(1)=1
          ExtTensorOld(2)=1
        endif
      else if(ExtTensor(KDatBlock).eq.2) then
        if(ExtType(KDatBlock).eq.1) then
          call CopyVek(ec(7,KDatBlock),ECOld(1,2),6)
          ExtTensorOld(2)=2
        else if(ExtType(KDatBlock).eq.2) then
          call CopyVek(ec(1,KDatBlock),ECOld(1,1),6)
          ExtTensorOld(1)=2
        endif
      endif
      KDatBlockIn=KDatBlock
      xqd=450.
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      n=0
      if(NDatBlock.gt.1) then
        dpomr=0.
        KDatB=0
        do i=1,NDatBlock
          if(DataType(i).eq.2) then
            MenuDatBlockUse(i)=0
          else
            if(KDatB.le.0) KDatB=i
            MenuDatBlockUse(i)=1
            n=n+1
          endif
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(DataType(KDatBlock).ne.1) KDatBlock=KDatB
      if(n.gt.1) then
        il=11
      else
        il=10
      endif
      if(MaxMagneticType.gt.0) il=il+3
1040  id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Extinction model:',
     2                   0,LightGray,0,OKForBasicFiles)
      Zpet=.false.
      xpom=65.
      Veta='%None'
      do i=1,3
        call FeQuestCrwMake(id,5.,i,xpom,i,Veta,'L',CrwgXd,CrwgYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.ExtTensor(KDatBlock))
        if(i.eq.1) then
          nCrwExtFirst=CrwLastMade
          Veta='%Isotropic'
        else
          Veta='%Anisotropic'
        endif
      enddo
      nCrwExtLast=CrwLastMade
      tpom=xpom+80.
      Veta='Type %1'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      do i=1,3
        call FeQuestCrwMake(id,tpom,i,xpom,i,Veta,'L',CrwgXd,CrwgYd,1,
     1                      2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.ExtType(KDatBlock))
        if(i.eq.1) then
          Veta='Type %2'
          nCrwType1=CrwLastMade
        else if(i.eq.2) then
          Veta='%Mixed'
          nCrwType2=CrwLastMade
        else
          nCrwMixed=CrwLastMade
        endif
      enddo
      tpom=xpom+80.
      xpom=tpom+55.
      il=1
      Veta='%Gaussian'
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      3)
        call FeQuestCrwOpen(CrwLastMade,i.eq.ExtDistr(KDatBlock))
        if(i.eq.1) then
          nCrwGauss=CrwLastMade
          Veta='%Lorentzian'
        else if(i.eq.2) then
          nCrwLorentz=CrwLastMade
        endif
        il=il+1
      enddo
      il=il+1
      tpom=5.
      Veta='Ra%dius [cm]'
      xpom=FeTxLengthUnder(Veta)+15.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ExtRadius(KDatBlock),
     1                        .false.,.false.)
      tpom=xpom+dpom+10.
      Veta='used only if tbar not present on M90 file'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Extinction parameters:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      il=il+1
      kip=MxSc+7
      j=1
      do i=1,6
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,' ',nEdw,nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          if(i.ne.6) il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      if(MaxMagneticType.gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Correction factors for magnetic reflections:'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        kip=MxSc+2
        il=il+1
        j=1
        do i=1,2
          call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
          call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
          if(i.eq.1) then
            nEdwPrvMag=nEdw
            nCrwPrvMag=nCrw
          endif
          kip=kip+1
          j=j+1
        enddo
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,60.,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(n.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
      ExtTypeOld=-1
c      if(ExtType(KDatBlock).eq.1) then
c        ec(1,KDatBlock)=.01
c      else if(ExtType(KDatBlock).eq.2) then
c        ec(7,KDatBlock)=.01
c      endif
1500  nCrw=nCrwExtFirst
      do i=1,3
        if(ExtType(KDatBlock).eq.3.and.i.eq.3) then
          call FeQuestCrwDisable(nCrw)
        else
          call FeQuestCrwOpen(nCrw,i-1.eq.ExtTensor(KDatBlock))
        endif
        nCrw=nCrw+1
      enddo
      if(ExtType(KDatBlock).eq.1) then
        iec=7
        iecp=2
      else
        iec=1
        iecp=1
      endif
      nCrw=nCrwType1
      do i=1,3
        if(ExtTensor(KDatBlock).eq.0.or.
     1     (ExtTensor(KDatBlock).eq.2.and.i.eq.3)) then
          call FeQuestCrwDisable(nCrw)
        else
          call FeQuestCrwOpen(nCrw,i.eq.ExtType(KDatBlock))
        endif
        nCrw=nCrw+1
      enddo
1600  if((ExtType(KDatBlock).eq.1.or.ExtType(KDatBlock).eq.3).and.
     1    .not.CrwLogicQuest(nCrwExtFirst)) then
        nCrw=nCrwGauss
        do i=1,2
          call FeQuestCrwOpen(nCrw,i.eq.ExtDistr(KDatBlock))
          nCrw=nCrw+1
        enddo
      else
        do i=nCrwGauss,nCrwLorentz
          if(CrwStateQuest(i).ne.CrwClosed.and.
     1       CrwStateQuest(i).ne.CrwDisabled) then
            if(CrwLogicQuest(i)) ExtDistr(KDatBlock)=i-nCrwGauss+1
          endif
          call FeQuestCrwDisable(i)
        enddo
      endif
      if(ExtTypeOld.gt.0) then
        if(ExtTypeOld.eq.2.or.ExtTypeOld.eq.3) then
          j=1
        else if(ExtTypeOld.eq.1) then
          j=2
        else
          j=0
        endif
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        do i=1,6
          if(EdwStateQuest(nEdw).eq.EdwOpened.and.j.ne.0) then
            if(ExtTypeOld.ne.ExtType(KDatBlock)) then
              call FeQuestRealFromEdw(nEdw,ECOld(i,j))
              if(ExtTypeOld.eq.3) j=2
            endif
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(ExtType(KDatBlock).eq.1) then
        kip=MxSc+13
        iec=7
        iecp=2
      else
        kip=MxSc+7
        iec=1
        iecp=1
      endif
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      if(ExtTensor(KDatBlock).eq.0) then
        imx=0
      else if(ExtTensor(KDatBlock).eq.1) then
        imx=1
      else if(ExtTensor(KDatBlock).eq.2) then
        imx=6
      endif
      if(ExtType(KDatBlock).eq.3.and.ExtTensor(KDatBlock).gt.0) imx=2
      k=0
      do i=1,imx
        k=k+1
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeQuestEdwLabelChange(nEdw,pn)
        call FeQuestRealEdwOpen(nEdw,ECOld(k,iecp),.false.,.false.)
        call FeQuestCrwOpen(nCrw,.true.)
        if(imx.ne.2) then
          kip=kip+1
        else
          kip=kip+6
          k=0
          iec=7
          iecp=2
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
      enddo
      if(MaxMagneticType.gt.0) then
        if(ExtTypeOld.gt.0) then
          if(ExtTypeOld.eq.1) then
            kip=MxSc+3
          else
            kip=MxSc+2
          endif
          nEdw=nEdwPrvMag
          nCrw=nCrwPrvMag
          do i=1,2
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdw,pom)
              if(CrwLogicQuest(nCrw)) then
                KiS(kip,KDatBlock)=1
              else
                KiS(kip,KDatBlock)=0
              endif
              if(ExtTypeOld.eq.1) then
                ecMag(2,KDatBlock)=pom
              else if(ExtTypeOld.eq.2) then
                ecMag(1,KDatBlock)=pom
              else
                ecMag(i,KDatBlock)=pom
              endif
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            kip=kip+1
          enddo
        endif
        if(ExtType(KDatBlock).eq.1) then
          kip=MxSc+3
        else
          kip=MxSc+2
        endif
        if(ExtType(KDatBlock).eq.0) then
          imxm=0
        else
        endif
        if(ExtTensor(KDatBlock).eq.0) then
          imxm=0
        else
          if(ExtType(KDatBlock).eq.3) then
            imxm=2
          else
            imxm=1
          endif
        endif
        nEdw=nEdwPrvMag
        nCrw=nCrwPrvMag
        do i=1,imxm
          call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
          call FeQuestEdwLabelChange(nEdw,pn)
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          call FeQuestCrwOpen(nCrw,KiS(kip,KDatBlock).gt.0)
          nEdw=nEdw+1
          nCrw=nCrw+1
          kip=kip+1
        enddo
        call FeReleaseOutput
        call FeDeferOutput
      endif
      ExtTypeOld=ExtType(KDatBlock)
      if(ExtTypeOld.eq.1) then
        ExtTensorOld(2)=ExtTensor(KDatBlock)
      else if(ExtTypeOld.eq.2) then
        ExtTensorOld(1)=ExtTensor(KDatBlock)
      endif
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwExtLast) then
        if(ExtTypeOld.eq.1) then
          ExtTensorOld(2)=ExtTensor(KDatBlock)
        else if(ExtTypeOld.eq.2) then
          ExtTensorOld(1)=ExtTensor(KDatBlock)
        endif
        do i=nCrwExtFirst,nCrwExtLast
          if(CrwLogicQuest(i)) then
            ExtTensor(KDatBlock)=i-nCrwExtFirst
            exit
          endif
        enddo
        if(ExtTensor(KDatBlock).eq.0) then
          go to 1500
        else if(ExtTensor(KDatBlock).eq.1) then
          if(ExtTensorOld(iecp).le.1) then
            go to 1500
          else
            go to 3000
          endif
        else if(ExtTensor(KDatBlock).eq.2) then
          if(ExtTensorOld(iecp).eq.0) then
            go to 3100
          else if(ExtTensorOld(iecp).eq.1) then
            call FeQuestRealFromEdw(nEdwPrv,ECOld(1,iecp))
            go to 3100
          else if(ExtTensorOld(iecp).eq.2) then
            go to 1500
          endif
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwMixed)
     1  then
        do i=nCrwType1,nCrwMixed
          if(CrwLogicQuest(i)) then
            ExtType(KDatBlock)=i-nCrwType1+1
            exit
          endif
        enddo
        if(ExtType(KDatBlock).eq.1) then
          iec=7
          iecp=2
        else if(ExtType(KDatBlock).eq.2) then
          iec=1
          iecp=1
        else if(ExtType(KDatBlock).eq.3) then
          iec=8-iec
          iecp=3-iecp
        endif
        if(max(ExtTensorOld(iecp),1).eq.ExtTensor(KDatBlock)) then
          go to 1500
        else
          if(ExtTensorOld(iecp).le.1) then
            go to 3100
          else
            go to 3020
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwLorentz) then
        do i=nCrwGauss,nCrwLorentz
          if(CrwLogicQuest(i)) then
            ExtDistr(KDatBlock)=i-nCrwGauss+1
            exit
          endif
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,imx
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        nCrw=nCrwPrvMag
        do i=1,imxm
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 2500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        ECOld=0.
        if(ExtTensor(KDatBlock).eq.1) then
          ECOld=0.
          ec(iec,KDatBlock)=0.01
          if(ExtType(KDatBlock).eq.3) ec(8-iec,KDatBlock)=.01
        else if(ExtTensor(KDatBlock).eq.2) then
          pom=0.0001
          if(ExtType(KDatBlock).eq.2) pom=1./pom
          call SetRealArrayTo(ec(iec,KDatBlock),3,pom)
          call SetRealArrayTo(ec(iec+3,KDatBlock),3,0.)
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 2500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      go to 3200
3000  nEdw=nEdwPrv
      do i=1,6
        call FeQuestRealFromEdw(nEdw,ECOld(i,iecp))
        nEdw=nEdw+1
      enddo
3020  det=ECOld(1,iecp)*ECOld(2,iecp)*ECOld(3,iecp)+
     1    2.*ECOld(4,iecp)*ECOld(5,iecp)*ECOld(6,iecp)-
     2    ECOld(1,iecp)*ECOld(6,iecp)**2-
     3    ECOld(2,iecp)*ECOld(5,iecp)**2-
     4    ECOld(3,iecp)*ECOld(4,iecp)**2
      if(det.gt.0.) then
        ECOld(1,iecp)=.001*(CellVol(1,KPhase)**2/det)**(.1666667)
      else
        ECOld(1,iecp)=0.01
      endif
      ExtTensorOld(iecp)=1
3050  call SetRealArrayTo(ECOld(2,iecp),5,0.)
      call SetIntArrayTo(KiS(MxSc+7,KDatBlock),12,0)
      KiS(MxSc+iec+6,KDatBlock)=1
      go to 1500
3100  pom=1./(ECOld(1,iecp)*1000.)**2
      ECOld(1,iecp)=MetTens(1,1,KPhase)*pom
      ECOld(2,iecp)=MetTens(5,1,KPhase)*pom
      ECOld(3,iecp)=MetTens(9,1,KPhase)*pom
      ECOld(4,iecp)=MetTens(2,1,KPhase)*pom
      ECOld(5,iecp)=MetTens(3,1,KPhase)*pom
      ECOld(6,iecp)=MetTens(6,1,KPhase)*pom
      ExtTensorOld(iecp)=2
      go to 1500
3200  if(ich.eq.0) then
        do i=nCrwGauss,nCrwLorentz
          if(CrwLogicQuest(i)) then
            ExtDistr(KDatBlock)=i-nCrwGauss+1
            exit
          endif
        enddo
3250    call FeQuestRealFromEdw(nEdwRadius,ExtRadius(KDatBlock))
        call SetRealArrayTo(ec(1,KDatBlock),12,0.)
        call SetIntArrayTo(KiS(MxSc+7,KDatBlock),12,0)
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,EcPom,KiPom,imx)
        if(ExtTensor(KDatBlock).eq.1) then
          if(ExtType(KDatBlock).eq.1) then
            ec(7,KDatBlock)=EcPom(1)
            KiS(MxSc+13,KDatBlock)=KiPom(1)
          else if(ExtType(KDatBlock).eq.2) then
            ec(1,KDatBlock)=EcPom(1)
            KiS(MxSc+7 ,KDatBlock)=KiPom(1)
          else if(ExtType(KDatBlock).eq.3) then
            ec(1,KDatBlock)=EcPom(1)
            ec(7,KDatBlock)=EcPom(2)
            KiS(MxSc+7 ,KDatBlock)=KiPom(1)
            KiS(MxSc+13,KDatBlock)=KiPom(2)
          endif
        else if(ExtTensor(KDatBlock).eq.2) then
          if(ExtType(KDatBlock).eq.1) then
            ec(7:12,KDatBlock)=EcPom(1:6)
            KiS(MxSc+13:MxSc+18,KDatBlock)=KiPom(1:6)
          else if(ExtType(KDatBlock).eq.2) then
            ec(1:6,KDatBlock)=EcPom(1:6)
            KiS(MxSc+7:MxSc+12,KDatBlock)=KiPom(1:6)
          endif
        endif
        if(MaxMagneticType.gt.0) then
          call SetRealArrayTo(ecMag(1,KDatBlock),2,1.)
          call SetIntArrayTo(KiS(MxSc+2,KDatBlock),2,0)
          if(ExtType(KDatBlock).eq.1) then
            j=2
            kip=MxSc+3
          else if(ExtType(KDatBlock).eq.2.or.ExtType(KDatBlock).eq.3)
     1      then
            j=1
            kip=MxSc+2
          endif
          call FeUpdateParamAndKeys(nEdwPrvMag,nCrwPrvMag,
     1         ecMag(j,KDatBlock),KiS(kip,KDatBlock),imxm)
        endif
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
