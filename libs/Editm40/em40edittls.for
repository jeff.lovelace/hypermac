      subroutine EM40EditTLS(imp)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(3),xpoma(3)
      character*12 at,pn
      integer PrvKi
      logical lpom
      xdq=400.
      il=8
      pom=(xdq-3.*75.-10.)/3.
      pom=(xdq-80.-pom)/2.
      xpoma(3)=xdq-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Edit TLS tensors',0,
     1                   LightGray,0,OKForBasicFiles)
      il=1
      j=1
      PrvKi=8
      kip=PrvKi
      do i=1,21
        call kdoco(kip+PrvniKiMolekuly(imp)-1,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiMol(kip,imp),
     1                       .false.)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      dpom=60.
      pom=20.
      xpom=(xdq-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        kip=PrvKi
        do i=1,21
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
          kip=kip+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        pom=.001
        do i=1,21
          if(i.eq.4) pom=0.
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=PrvKi
        call FeUpdateParamAndKeys(nEdw,nCrw,tT(1,imp),KiMol(kip,imp),6)
        nEdw=nEdw+6
        nCrw=nCrw+6
        kip=kip+6
        call FeUpdateParamAndKeys(nEdw,nCrw,tL(1,imp),KiMol(kip,imp),6)
        nEdw=nEdw+6
        nCrw=nCrw+6
        kip=kip+6
        call FeUpdateParamAndKeys(nEdw,nCrw,tS(1,imp),KiMol(kip,imp),9)
      endif
      call FeQuestRemove(id)
      return
      end
