      subroutine ZmTF12(i)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PrvKi
      itf(i)=2
      pom=beta(1,i)
      spom=sbeta(1,i)
      PrvKi=5
      do j=1,6
        beta(j,i)=pom*prcp(j,iswa(i),KPhase)
        sbeta(j,i)=spom*prcp(j,iswa(i),KPhase)
        KiA(PrvKi,i)=1
        PrvKi=PrvKi+1
      enddo
      return
      end
