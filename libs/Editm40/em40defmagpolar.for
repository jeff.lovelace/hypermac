      subroutine EM40DefMagPolar(ich)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RMP(9)
      character*80  Veta
      character*12  Names(:)
      integer IPolar(:)
      logical CrwLogicQuest,lpom
      allocatable Names,IPolar
      ich=0
      nn=0
      do i=1,NAtInd
        if(MagPar(i).ne.0) nn=nn+1
      enddo
      allocate(Names(nn),IPolar(nn))
      nn=0
      do i=1,NAtInd
        if(MagPar(i).ne.0) then
          nn=nn+1
          Names(nn)=Atom(i)
        endif
      enddo
      id=NextQuestId()
      ilm=min(nn,10)+1
      Veta='Define atoms using polar coordinates for magnetic moments'
      call FeBoldFont
      xqd=FeTxLength(Veta)+10.
      call FeNormalFont
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,Veta,0,LightGray,0,0)
      xpom=10.
      tpom=xpom+CrwXd+10.
      il=0
      do i=1,nn
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Names(i),'L',CrwXd,
     1                      CrwYd,0,0)
        iat=ktat(NamePolar,NPolar,Names(i))
        if(iat.gt.0) then
          IPolar(i)=1
        else
          IPolar(i)=0
        endif
        call FeQuestCrwOpen(CrwLastMade,iat.gt.0)
        if(i.eq.1) nCrwFirst=CrwLastMade
        if(mod(i,10).eq.0) then
          il=0
          xpom=xpom+60.
          tpom=tpom+60.
        endif
      enddo
      il=-ilm*10-3
      Veta='Select all'
      dpom=FeTxLength(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAll=ButtonLastMade
      Veta='Refresh'
      xpom=xpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        lpom=CheckNumber.eq.nButtAll
        nCrw=nCrwFirst
        do i=1,nn
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        NPolar=0
        nCrw=nCrwFirst
        do i=1,nn
          iat=ktat(Atom,NAtInd,Names(i))
          if(CrwLogicQuest(nCrw)) then
            NPolar=NPolar+1
            NamePolar(NPolar)=Names(i)
            if(IPolar(i).eq.0) then
              do j=1,MagPar(iat)
                if(j.eq.1) then
                  call Fract2ShpCoor(sm0(1,iat))
                else
                  call Fract2ShpCoor(smx(1,j-1,iat))
                  call Fract2ShpCoor(smy(1,j-1,iat))
                endif
              enddo
            endif
          else
            if(IPolar(i).ne.0) then
              do j=1,MagPar(iat)
                if(j.eq.1) then
                  call ShpCoor2Fract(sm0(1,iat),RMP)
                else
                  call ShpCoor2Fract(smx(1,j-1,iat),RMP)
                  call ShpCoor2Fract(smy(1,j-1,iat),RMP)
                endif
              enddo
            endif
          endif
          nCrw=nCrw+1
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
9999  Deallocate(Names,IPolar)
      return
      end
