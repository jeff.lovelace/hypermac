      subroutine EM40SelAtomsCheck
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      if(nEdwAtoms.eq.0) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
      if(nRolMenuMol.gt.0) then
        if(RolMenuSelectedQuest(nRolMenuMol).eq.0) then
          call FeQuestButtonDisable(nButtInclMol)
          call FeQuestButtonDisable(nButtExclMol)
          call FeQuestActiveUpdate
        else
          call FeQuestButtonOff(nButtInclMol)
          call FeQuestButtonOff(nButtExclMol)
        endif
      endif
9999  return
      end
