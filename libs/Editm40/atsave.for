      subroutine AtSave(n,Klic)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*8 atoms
      integer DelkaKiAtomuS,AtSiteMultS
      dimension kfas(7),kmodas(7),axs(mxw),ays(mxw),
     1          xs(3),uxs(3,mxw),uys(3,mxw),
     2          betas(6),bxs(6,mxw),bys(6,mxw),
     3          c3s(10),c3xs(10,mxw),c3ys(10,mxw),
     4          c4s(15),c4xs(15,mxw),c4ys(15,mxw),
     5          c5s(21),c5xs(21,mxw),c5ys(21,mxw),
     6          c6s(28),c6xs(28,mxw),c6ys(28,mxw),
     7          saxs(mxw),says(mxw),sxs(3),suxs(3,mxw),suys(3,mxw),
     8          sbetas(6),sbxs(6,mxw),sbys(6,mxw),
     9          sc3s(10),sc3xs(10,mxw),sc3ys(10,mxw),
     a          sc4s(15),sc4xs(15,mxw),sc4ys(15,mxw),
     1          sc5s(21),sc5xs(21,mxw),sc5ys(21,mxw),
     2          sc6s(28),sc6xs(28,mxw),sc6ys(28,mxw),
     3          popass(64),isas(:),kmodaos(7),
     4          durdrs(9),KiAs(:)
      allocatable isas,KiAs
      save atoms,isfs,itfs,ifrs,iswas,kswas,kmols,lasmaxs,kfas,
     1     kmodas,ais,sais,a0s,sa0s,axs,ays,saxs,says,xs,sxs,uxs,uys,
     2     suxs,suys,betas,sbetas,bxs,bys,sbxs,sbys,
     3     c3s,c3xs,c3ys,c4s,c4xs,c4ys,c5s,c5xs,c5ys,c6s,c6xs,c6ys,
     4     sc3s,sc3xs,sc3ys,sc4s,sc4xs,sc4ys,sc5s,sc5xs,sc5ys,
     5     sc6s,sc6xs,sc6ys,phfs,sphfs,AtSiteMultS,KUsePolarS,
     6     popass,popvs,kapa1s,kapa2s,DelkaKiAtomuS,kias,
     7     MagParS,TypeModFuns,isas,kmodaos,durdrs
      if(Klic.eq.0) then
        if(allocated(isas)) deallocate(isas,KiAS)
        allocate(isas(MaxNSymm),KiAS(mxda))
        Atoms=Atom(n)
        isfs=isf(n)
        itfs=itf(n)
        ifrs=ifr(n)
        iswas=iswa(n)
        kswas=kswa(n)
        kmols=kmol(n)
        MagParS=MagPar(n)
        if(MagPar(n).gt.0) KUsePolarS=KUsePolar(n)
        lasmaxs=lasmax(n)
        TypeModFuns=TypeModFun(n)
        AtSiteMultS=AtSiteMult(n)
        call CopyVekI(KFA(1,n),kfas,7)
        call CopyVekI(KModA(1,n),kmodas,7)
        call CopyVekI(KModAO(1,n),kmodaos,7)
        call CopyVekI(isa(1,n),isas,MaxNSymm)
         ais= ai(n)
        sais=sai(n)
         a0s= a0(n)
        sa0s=sa0(n)
        if(NMolec.gt.0) call CopyVek(durdr(1,n),durdrs,9)
        call CopyVekI(KiA(1,n),KiAS,DelkaKiAtomu(n))
        if(kmodas(1).gt.0) then
          i=kmodas(1)
          if(i.gt.0) then
            call CopyVek(ax(1,n),axs,i)
            call CopyVek(ay(1,n),ays,i)
            call CopyVek(sax(1,n),saxs,i)
            call CopyVek(say(1,n),says,i)
          endif
        endif
        i=3
        call CopyVek(x(1,n),xs,i)
        call CopyVek(sx(1,n),sxs,i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(ux(1,1,n),uxs,i)
          call CopyVek(uy(1,1,n),uys,i)
          call CopyVek(sux(1,1,n),suxs,i)
          call CopyVek(suy(1,1,n),suys,i)
        endif
        i=6
        call CopyVek(beta(1,n),betas,i)
        call CopyVek(sbeta(1,n),sbetas,i)
        i=kmodas(3)*i
        if(i.gt.0) then
          call CopyVek(bx(1,1,n),bxs,i)
          call CopyVek(by(1,1,n),bys,i)
          call CopyVek(sbx(1,1,n),sbxs,i)
          call CopyVek(sby(1,1,n),sbys,i)
        endif
        if(itfs.gt.2) then
          i=10
          call CopyVek(c3(1,n),c3s,i)
          call CopyVek(sc3(1,n),sc3s,i)
          i=kmodas(2)*i
          if(i.gt.0) then
            call CopyVek(c3x(1,1,n),c3xs,i)
            call CopyVek(c3y(1,1,n),c3ys,i)
            call CopyVek(sc3x(1,1,n),sc3xs,i)
            call CopyVek(sc3y(1,1,n),sc3ys,i)
          endif
          if(itfs.gt.3) then
            i=15
            call CopyVek(c4(1,n),c4s,i)
            call CopyVek(sc4(1,n),sc4s,i)
            i=kmodas(2)*i
            if(i.gt.0) then
              call CopyVek(c4x(1,1,n),c4xs,i)
              call CopyVek(c4y(1,1,n),c4ys,i)
              call CopyVek(sc4x(1,1,n),sc4xs,i)
              call CopyVek(sc4y(1,1,n),sc4ys,i)
            endif
            if(itfs.gt.4) then
              i=21
              call CopyVek(c5(1,n),c5s,i)
              call CopyVek(sc5(1,n),sc5s,i)
              i=kmodas(2)*i
              if(i.gt.0) then
                call CopyVek(c5x(1,1,n),c5xs,i)
                call CopyVek(c5y(1,1,n),c5ys,i)
                call CopyVek(sc5x(1,1,n),sc5xs,i)
                call CopyVek(sc5y(1,1,n),sc5ys,i)
              endif
              if(itfs.gt.5) then
                i=28
                call CopyVek(c6(1,n),c6s,i)
                call CopyVek(sc6(1,n),sc6s,i)
                i=kmodas(2)*i
                if(i.gt.0) then
                  call CopyVek(c6x(1,1,n),c6xs,i)
                  call CopyVek(c6y(1,1,n),c6ys,i)
                  call CopyVek(sc6x(1,1,n),sc6xs,i)
                  call CopyVek(sc6y(1,1,n),sc6ys,i)
                endif
              endif
            endif
          endif
          if(NDimI(kswa(n)).gt.0) then
            phfs=phf(n)
            sphfs=sphf(n)
          endif
        endif
        if(ChargeDensities) then
          kapa1s=kapa1(n)
          kapa2s=kapa2(n)
          popcs=popc(n)
          popvs=popv(n)
          if(lasmax(n).gt.1)
     1      call CopyVek(popas(1,n),popass,lasmax(n)**2)
        endif
1750    DelkaKiAtomuS=DelkaKiAtomu(n)
      else
        Atom(n)=Atoms
        isf(n)=isfs
        itf(n)=itfs
        ifr(n)=ifrs
        iswa(n)=iswas
        kswa(n)=kswas
        kmol(n)=kmols
        MagPar(n)=MagParS
        if(MagParS.gt.0) KUsePolar(n)=KUsePolarS
        lasmax(n)=lasmaxs
        TypeModFun(n)=TypeModFuns
        AtSiteMult(n)=AtSiteMultS
        call CopyVekI(kfas,KFA(1,n),7)
        call CopyVekI(kmodas,KModA(1,n),7)
        call CopyVekI(kmodaos,KModAO(1,n),7)
        call CopyVekI(isas,isa(1,n),MaxNSymm)
         ai(n)= ais
        sai(n)=sais
         a0(n)= a0s
        sa0(n)=sa0s
        if(NMolec.gt.0) call CopyVek(durdrs,durdr(1,n),9)
        call CopyVekI(kias,KiA(1,n),DelkaKiAtomuS)
        if(kmodas(1).gt.0) then
          i=kmodas(1)
          if(i.gt.0) then
            call CopyVek(axs,ax(1,n),i)
            call CopyVek(ays,ay(1,n),i)
            call CopyVek(saxs,sax(1,n),i)
            call CopyVek(says,say(1,n),i)
          endif
        endif
        i=3
        call CopyVek(xs,x(1,n),i)
        call CopyVek(sxs,sx(1,n),i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(uxs,ux(1,1,n),i)
          call CopyVek(uys,uy(1,1,n),i)
          call CopyVek(suxs,sux(1,1,n),i)
          call CopyVek(suys,suy(1,1,n),i)
        endif
        i=6
        call CopyVek(betas,beta(1,n),i)
        call CopyVek(sbetas,sbeta(1,n),i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(bxs,bx(1,1,n),i)
          call CopyVek(bys,by(1,1,n),i)
          call CopyVek(sbxs,sbx(1,1,n),i)
          call CopyVek(sbys,sby(1,1,n),i)
        endif
        if(itfs.gt.2) then
          i=10
          call CopyVek(c3s,c3(1,n),i)
          call CopyVek(sc3s,sc3(1,n),i)
          i=kmodas(2)*i
          if(i.gt.0) then
            call CopyVek(c3xs,c3x(1,1,n),i)
            call CopyVek(c3ys,c3y(1,1,n),i)
            call CopyVek(sc3xs,sc3x(1,1,n),i)
            call CopyVek(sc3ys,sc3y(1,1,n),i)
          endif
          if(itfs.gt.3) then
            i=15
            call CopyVek(c4s,c4(1,n),i)
            call CopyVek(sc4s,sc4(1,n),i)
            i=kmodas(2)*i
            if(i.gt.0) then
              call CopyVek(c4xs,c4x(1,1,n),i)
              call CopyVek(c4ys,c4y(1,1,n),i)
              call CopyVek(sc4xs,sc4x(1,1,n),i)
              call CopyVek(sc4ys,sc4y(1,1,n),i)
            endif
            if(itfs.gt.4) then
              i=21
              call CopyVek(c5s,c5(1,n),i)
              call CopyVek(sc5s,sc5(1,n),i)
              i=kmodas(2)*i
              if(i.gt.0) then
                call CopyVek(c5xs,c5x(1,1,n),i)
                call CopyVek(c5ys,c5y(1,1,n),i)
                call CopyVek(sc5xs,sc5x(1,1,n),i)
                call CopyVek(sc5ys,sc5y(1,1,n),i)
              endif
              if(itfs.gt.5) then
                i=28
                call CopyVek(c6s,c6(1,n),i)
                call CopyVek(sc6s,sc6(1,n),i)
                i=kmodas(2)*i
                if(i.gt.0) then
                  call CopyVek(c6xs,c6x(1,1,n),i)
                  call CopyVek(c6ys,c6y(1,1,n),i)
                  call CopyVek(sc6xs,sc6x(1,1,n),i)
                  call CopyVek(sc6ys,sc6y(1,1,n),i)
                endif
              endif
            endif
          endif
          if(NDimI(kswa(n)).gt.0) then
            phf(n)=phfs
            sphf(n)=sphfs
          endif
        endif
2750    call ShiftKiAt(n,itfs,ifrs,lasmaxs,kmodas,MagPars,.false.)
      endif
      return
      end
