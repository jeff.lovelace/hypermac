      subroutine EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                            nEdwPrvParMag,nCrwPrvParMag,MagParP)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kip(10),p(10)
      integer PrvKiMag,EdwStateQuest,CrwStateQuest
      call SetRealArrayTo(p,10,0.)
      call FeUpdateParamAndKeys(nEdwPrvPar,nCrwPrvPar,p,kip,npar)
      do j=1,npar
        if(kip(j).ge.110) cycle
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(kip(j)/100.eq.0) then
              if(j.eq.1) then
                ai(ia)=p(j)
              else if(j.le.4) then
                x(j-1,ia)=p(j)
              else
                beta(j-4,ia)=p(j)
              endif
            endif
            if(mod(kip(j),100).lt.10)
     1        KiA(j,ia)=mod(kip(j),10)
          endif
        enddo
      enddo
      j=0
      if(nEdwPrvParMag.le.0) go to 9999
      if(MagParP.gt.0) then
        call FeUpdateParamAndKeys(nEdwPrvParMag,nCrwPrvParMag,p,kip,3)
        nEdw=nEdwPrvParMag
        nCrw=nCrwPrvParMag
        do j=1,3
          if(kip(j).ge.110) cycle
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              k=PrvKiMag(ia)+j-1
              if(kip(j)/100.eq.0.and.EdwStateQuest(nEdw).eq.EdwOpened)
     1          sm0(j,ia)=p(j)
              if(mod(kip(j),100).lt.10.and.
     1          (CrwStateQuest(nCrw).eq.CrwOff.or.
     2           CrwStateQuest(nCrw).eq.CrwOn)) KiA(k,ia)=mod(kip(j),10)
            endif
          enddo
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
9999  return
      end
