      subroutine EM40GetXFromAtName(AtName,ia,XAt,UxAt,UyAt,x40,RmiiAt,
     1                              ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension XAt(3),dsym(6),xp(6),RmAt(9),Rm6At(36),RMiiAt(9),ic(3),
     1          xpp(6),x40(3),UxAt(3,*),UyAt(3,*)
      character*(*) AtName
      if(NDimI(KPhase).gt.0) then
        call AtomSymCode(AtName,ia,ISym,ICentr,dsym,ic,ich)
        if(ich.eq.0) then
          call SetRealArrayTo(UxAt,KModA(2,ia)*3,0.)
          call SetRealArrayTo(UyAt,KModA(2,ia)*3,0.)
          call SetRealArrayTo(x40,NDimI(KPhase),0.)
          call UnitMat(RMiiAt,NDimI(KPhase))
        endif
      else
        call atsym(AtName,ia,XAt,dsym,xp,ISym,ich)
      endif
      if(ich.eq.0) then
        ksw=kswa(ia)
        isw=iswa(ia)
      endif
      if(ich.ne.0) then
        if(NMolec.gt.0) then
          ia=ktat(Atom(NAtMolFrAll(1)),NAtMol,AtName)
          if(ia.gt.0) then
            ia=ia+NAtMolFrAll(1)-1
            call CopyVek(x(1,ia),XAt,3)
            if(NDimI(KPhase).gt.0) then
              call CopyVek(ux(1,1,ia),UxAt,3*KModA(2,ia))
              call CopyVek(uy(1,1,ia),UyAt,3*KModA(2,ia))
              call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
            endif
            call UnitMat(RMiiAt,NDimI(KPhase))
            ich=0
          endif
        endif
      else if(NDimI(KPhase).gt.0) then
        j=iabs(ISym)
        call CopyMat(rm(1,j,isw,ksw),RmAt,3)
        call CopyMat(rm6(1,j,isw,ksw),Rm6At,NDim(KPhase))
        if(ISym.lt.0) then
          call RealMatrixToOpposite(RmAt,RmAt,3)
          call RealMatrixToOpposite(Rm6At,Rm6At,NDim(KPhase))
        endif
        call GetGammaIntInv(rm6(1,j,isw,ksw),RmiiAt)
        call CopyVek(x(1,ia),xp,3)
        call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
        call multm(Rm6At,xp,xpp,NDim(KPhase),NDim(KPhase),1)
        call AddVek(xpp,dsym,xp,NDim(KPhase))
        call CopyVek(xp,XAt,3)
        call qbyx(dsym,xp,isw)
        do j=1,NDimI(KPhase)
          xp(j)=xp(j)-dsym(j+3)
        enddo
        call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
        call cultm(RMiiAt,xp,x40,NDimI(KPhase),NDimI(KPhase),1)

        do j=1,KModA(2,ia)
          call multm(RmAt,ux(1,j,ia),UxAt(1,j),3,3,1)
          call multm(RmAt,uy(1,j,ia),UyAt(1,j),3,3,1)
        enddo
      endif
9999  return
      end
