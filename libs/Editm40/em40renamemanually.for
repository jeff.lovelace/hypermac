      subroutine EM40RenameManually(ich)
      use EditM40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*8 AtNameNew(:)
      integer IAt(:)
      logical EqIgCase
      allocatable AtNameNew,IAt
      allocate(AtNameNew(NAtAll),IAt(NAtAll))
      ich=0
      na=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          na=na+1
          AtNameNew(na)=Atom(IAtActive(i))
          IAt(na)=IAtActive(i)
        endif
      enddo
      id=NextQuestId()
      xqd=550.
      if(na.gt.45) then
        il=16
      else
        il=15
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Edit individual atom names',
     1                   0,LightGray,0,0)
      tpom=5.
      xpom=80.
      dpom=80.
      il=0
      do i=1,min(na,45)
        il=il+1
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        call FeQuestEdwMake(id,xpom-5.,il,xpom,il,' ','R',dpom,EdwYd,
     1                      1)
        if(mod(i,15).eq.0) then
          call FeQuestSvisliceFromToMake(id,xpom+dpom+15.,1,15,0)
          il=0
          tpom=tpom+180.
          xpom=xpom+180.
        endif
        if(i.eq.1) then
          nLblFr=LblLastMade
          nEdwFr=EdwLastMade
        endif
      enddo
      if(na.gt.0) then
        il=-165
        tpom=15.
        dpom=60.
        Cislo='Previous'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtPrevious=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        tpom=xqd-dpom-15.
        Cislo='Next'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtPrevious=0
        nButtNext=0
      endif
      NFrom=1
      NTo=min(na,45)
1400  nLbl=nLblFr
      nEdw=nEdwFr
      do i=NFrom,NTo
        write(Cislo,'(''#'',i5)') i
        call Zhusti(Cislo)
        call FeQuestLblChange(nLbl,Cislo)
        Cislo=Atom(IAt(i))
        Cislo=Cislo(:idel(Cislo))//'=>'
        call FeQuestEdwLabelChange(nEdw,Cislo)
        call FeQuestStringEdwOpen(nEdw,AtNameNew(i))
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      do i=NTo+1,NFrom+44
        call FeQuestLblOff(nLbl)
        call FeQuestEdwClose(nEdw)
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      if(nButtPrevious.gt.0) then
        if(NFrom.le.1) then
          call FeQuestButtonDisable(nButtPrevious)
        else
          call FeQuestButtonOff(nButtPrevious)
        endif
        if(NTo.ge.na) then
          call FeQuestButtonDisable(nButtNext)
        else
          call FeQuestButtonOff(nButtNext)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtPrevious) then
          NFrom=max(NFrom-45,1)
          NTo=min(NFrom+44,na)
        else
          NFrom=max(NFrom+45,1)
          NTo=min(NFrom+44,na)
        endif
        go to 1400
      else if(CheckType.eq.EventEdw) then
        Cislo=EdwStringQuest(CheckNumber)
        call AtCheck(Cislo,ichp,i)
        if(ichp.eq.1) then
          call FeChybne(-1.,-1.,'unacceptable symbol in atom string '//
     1                  'try again.',' ',SeriousError)
          go to 1500
        endif
        AtNameNew(nFrom+CheckNumber-nEdwFr)=Cislo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        j=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            j=j+1
            ia=IAtActive(i)
            call CrlAtomNamesSave(Atom(ia),AtNameNew(j),1)
            Atom(ia)=AtNameNew(j)
          endif
        enddo
        do i=1,NAtAll
          if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
          do j=i+1,NAtAll
            if(j.gt.NAtInd.and.j.lt.NAtMolFr(1,1)) cycle
            if(EqIgCase(Atom(i),Atom(j))) then
              call CrlCorrectAtomNames(ich)
              go to 9000
            endif
          enddo
        enddo
      endif
      call FeQuestRemove(id)
9000  if(allocated(AtNameNew)) deallocate(AtNameNew,IAt)
      return
      end
