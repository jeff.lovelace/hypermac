      subroutine EM40ReadTr(Expand,isw,ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension trp(36)
      character*80 Veta
      character*21 Hlavicka
      character*2 nty
      integer EdwStateQuest,SbwItemPointerQuest
      logical Expand,CrwLogicQuest
      if(Expand) then
        NTrans=0
      else
        NTrans=1
      endif
      iswp=max(isw,1)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_symm.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do j=1,NSymm(KPhase)
        Veta=' '
        do i=1,NDim(KPhase)
          Veta=Veta(:idel(Veta))//' '//
     1      symmc(i,j,iswp,KPhase)(:idel(symmc(i,j,iswp,KPhase)))
        enddo
        if(MagneticType(KPhase).gt.0) then
          if(ZMag(j,1,KPhase).gt.0.) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      if(Expand) then
        ilm=NDim(KPhase)+5
      else
        ilm=NDim(KPhase)+4
      endif
      if(MagneticType(KPhase).gt.0) ilm=ilm+1
      id=NextQuestId()
      xqd=280.
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,'Choice',1,LightGray,0,0)
      il=1
      Veta='Fill by a %symmetry operation'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtSymmetry=ButtonLastMade
      if(Expand) then
        k=ButtonDisabled
      else
        k=ButtonOff
      endif
      call FeQuestButtonOpen(ButtonLastMade,k)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,' ','C','B')
      nLblHlavicka=LblLastMade
      dpom=120.
      xpom=(xqd-dpom)*.5
      write(Veta,100) 1,nty(1)
      tpom=xpom-FeTxLengthUnder(Veta)-5.
      do i=1,NDim(KPhase)
        il=il+1
        write(Veta,100) i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwMatrixFirst=EdwLastMade
      enddo
      Veta='Translation vector'
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      il=il+1
      call FeQuestEdwMake(id,0.,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwShift=EdwLastMade
      if(MagneticType(KPhase).gt.0) then
        il=il+1
        xpom=tpom
        tpom=xpom+CrwXd+5.
        Veta='Apply %time inversion'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      0,0)
        nCrwTimeInversion=CrwLastMade
      endif
      if(Expand) then
        il=il+1
        Veta='%Next matrix'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(nButtNext,ButtonOff)
      else
        nButtNext=0
      endif
      if(Expand) then
        Hlavicka='Expansion matrix #1'
      else
        Hlavicka='Transformation matrix'
        call FeQuestLblChange(nLblHlavicka,Hlavicka)
      endif
1140  if(NTrans.le.0) go to 1200
      call UnitMat(TransMat(1,NTrans,iswp),NDim(KPhase))
      call SetRealArrayTo(TransVec(1,NTrans,iswp),NDim(KPhase),0.)
      if(MagneticType(KPhase).gt.0) TransZM(NTrans)=1.
1150  if(NTrans.gt.0) then
        nEdw=nEdwMatrixFirst
        do i=1,NDim(KPhase)
          do j=1,NDim(KPhase)
            trp(j)=TransMat(i+(j-1)*NDim(KPhase),NTrans,iswp)
          enddo
          call FeQuestRealAEdwOpen(nEdw,trp,NDim(KPhase),.false.,.true.)
          nEdw=nEdw+1
        enddo
        call FeQuestRealAEdwOpen(nEdw,TransVec(1,NTrans,iswp),
     1                           NDim(KPhase),.false.,.true.)
      endif
      if(MagneticType(KPhase).gt.0)
     1  call FeQuestCrwOpen(nCrwTimeInversion,TransZM(NTrans).lt.0.)
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumberAbs.eq.ButtonOk.or.CheckNumber.eq.nButtNext)) then
        if(EdwStateQuest(nEdwMatrixFirst).eq.EdwOpened) then
          nEdw=nEdwMatrixFirst
          do j=1,NDim(KPhase)
            call FeQuestRealAFromEdw(nEdw,trp)
            do i=1,NDim(KPhase)
              TransMat(j+(i-1)*NDim(KPhase),NTrans,iswp)=trp(i)
            enddo
            nEdw=nEdw+1
          enddo
          call FeQuestRealAFromEdw(nEdwShift,TransVec(1,NTrans,iswp))
          call matinv(TransMat(1,NTrans,iswp),trp,pom,NDim(KPhase))
          if(abs(pom).lt..0001) then
            call FeChybne(-1.,-1.,'The transformation matrix is '//
     1                    'singular, try again.',' ',SeriousError)
            EventType=EventEdw
            EventNumber=nEdwMatrixFirst
            go to 1200
          endif
          if(abs(abs(pom)-1.).gt..0001.and.lite(KPhase).eq.0) then
            WaitTime=10000
            NInfo=2
            TextInfo(1)='The transformation is not orthogonal and '//
     1                  'therefore'
            TextInfo(2)='ADP parameters were changed from U''s to '//
     2                  'beta''s'
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            lite(KPhase)=1
          endif
          if(MagneticType(KPhase).gt.0) then
            if(CrwLogicQuest(nCrwTimeInversion)) then
              TransZM(NTrans)=-1.
            else
              TransZM(NTrans)= 1.
            endif
          endif
        endif
        if(CheckNumber.eq.nButtNext) then
          call FeQuestButtonOff(nButtSymmetry)
          NTrans=NTrans+1
          write(Cislo,'(i2)') NTrans
          call Zhusti(Cislo)
          Hlavicka(19:)=Cislo
          call FeQuestLblChange(nLblHlavicka,Hlavicka)
          go to 1140
        else
          QuestCheck(id)=0
          go to 1200
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtSymmetry)
     1  then
        xqd=300.
        idp=NextQuestId()
        il=max(NSymm(KPhase)+1,5)
        il=min(NSymm(KPhase)+1,15)
        call FeQuestCreate(idp,-1.,-1.,xqd,il,'Select symmetry '//
     1                     'operation',0,LightGray,0,0)
        xpom=15.
        dpom=xqd-30.-SbwPruhXd
        il=il-1
        call FeQuestSbwMake(idp,xpom,il,dpom,il,1,CutTextFromLeft,
     1                      SbwVertical)
        call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_symm.tmp')
        il=il+1
        xpom=90.
        tpom=xpom+CrwXd+5.
        Veta='Apply %inversion centre'
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      0,0)
        nCrwInver=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
1300    call FeQuestEvent(idp,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1300
        endif
        if(ich.eq.0) then
          i=SbwItemPointerQuest(nSbw)
          call CopyVek(s6(1,i,1,KPhase),TransVec(1,NTrans,iswp),
     1                 NDim(KPhase))
          call CopyMat(rm6(1,i,1,KPhase),TransMat(1,NTrans,iswp),
     1                 NDim(KPhase))
          if(CrwLogicQuest(nCrwInver))
     1      call RealMatrixToOpposite(TransMat(1,NTrans,iswp),
     2        TransMat(1,NTrans,iswp),NDim(KPhase))
          TransZM(NTrans)=ZMag(i,1,KPhase)
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1200
        else
          go to 1150
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      call FeQuestRemove(id)
      if(ich.ne.0.or.NTrans.le.0) go to 9999
      do it=1,NTrans
        do i=1,NComp(KPhase)
          call CopyMat(TransMat(1,it,iswp),TransMat(1,it,i),
     1                 NDim(KPhase))
          call CopyVek(TransVec(1,it,iswp),TransVec(1,it,i),
     1                 NDim(KPhase))
        enddo
      enddo
      go to 1600
      entry EM40SetTr(isw)
1600  iswp=isw
      if(iswp.eq.0) then
        do it=1,NTrans
          do i=2,NComp(KPhase)
            call multm(zv(1,i,KPhase),TransMat(1,it,1),trp,NDim(KPhase),
     1                 NDim(KPhase),NDim(KPhase))
            call multm(trp,zvi(1,i,KPhase),TransMat(1,it,i),
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(zv(1,i,KPhase),TransVec(1,it,1),TransVec(1,it,i),
     1                 NDim(KPhase),NDim(KPhase),1)
          enddo
        enddo
      else
        do i=1,NComp(KPhase)
          if(i.ne.iswp) then
            do j=1,NTrans
              call UnitMat(TransMat(1,j,i),NDim(KPhase))
              call SetRealArrayTo(TransVec(1,j,i),NDim(KPhase),0.)
            enddo
          endif
        enddo
      endif
2000  do it=1,NTrans
        do i=1,NComp(KPhase)
          call MatBlock3(TransMat(1,it,i),TransX(1,it,i),NDim(KPhase))
          if(i.eq.1) then
            call matinv(TransX(1,it,1),trp,pom,3)
            if(pom.lt.0.) then
              SigNTransX(it)=-1.
            else
              SigNTransX(it)= 1.
            endif
          endif
          if(MagneticType(KPhase).ne.0) then
            call CopyMat(TransX(1,it,i),TransM(1,it,i),3)
            if(SignTransX(it)*TransZM(it).lt.0.)
     1        call RealMatrixToOpposite(TransM(1,it,i),TransM(1,it,i),3)
          endif
          call CopyVek(TransX(1,it,i),trp,9)
          call srotb(trp,trp,TransTemp(1,it,i))
          call srotss(trp,trp,TransTempS(1,it,i))
          call srotc(trp,3,TransC3(1,it,i))
          call srotc(trp,4,TransC4(1,it,i))
          call srotc(trp,5,TransC5(1,it,i))
          call srotc(trp,6,TransC6(1,it,i))
        enddo
      enddo
9999  return
100   format('%',i1,a2,' row')
      end
