      subroutine EM40Molecules(ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4),poma(1),kmodamn(3),Ktera(6),Rot(9),
     1          XCentr(3),trp(9),trpi(9),Rotp(9),rpgp(9,48),px(9),py(9),
     2          NAtMult(:)
      character*256 EdwStringQuest
      character*80 Veta
      character*12 men(6),menp(6)
      character*12 at,pn
      character*8 SmbPGMolOld,SmbPGMolIn(:)
      integer PrvKi,FeMenu,pocder,EdwStateQuest,TypeModFunNew
      logical Konec,lpom,CrwLogicQuest,eqrv,EqIgCase
      allocatable NAtMult,SmbPGMolIn
      equivalence (pom,poma)
      data men/'Occupancy','Translation','Rotation','Tensor T',
     1         'Tensor L','Tensor S'/
      if(allocated(SmbPGMolIn)) deallocate(SmbPGMolIn)
      allocate(SmbPGMolIn(NMolec))
      call CopyStringArray(SmbPGMol,SmbPGMolIn,NMolec)
      ich=0
      call CrlAtomNamesIni
      im=NMolecFrAll(KPhase)
      imo=0
      ip=1
      ipo=0
      imp=1
      impo=0
      Konec=.false.
      id=NextQuestId()
      xdq=550.
      xdqp=xdq*.5
      if(NDimI(KPhase).eq.0) then
        il=9
      else
        il=18
      endif
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Molecule edit',0,
     1                   LightGray,0,OKForBasicFiles)
      il=1
      Veta='%Molecule #'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwMolNo=EdwLastMade
      call FeQuestIntEdwOpen(nEdwMolNo,im-NMolecFrAll(KPhase)+1,.false.)
      call FeQuestEudOpen(nEdwMolNo,1,NMolecLenAll(KPhase),1,0.,0.,0.)
      Veta='%List'
      dpom=FeTxLengthUnder(Veta)+20.
      tpom=xpom+dpom+2.*EdwMarginSize+EdwYd+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtMolList=ButtonLastMade
      if(NMolecLenAll(KPhase).gt.1) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,i)
      Veta='%Name'
      tpom=tpom+dpom+30.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=FeTxLengthUnder('XXXXXXXX')+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwMolName=EdwLastMade
      Veta='%Position #'
      tpom=xpom+dpom+10.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=35.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPosNo=EdwLastMade
      call FeQuestIntEdwOpen(nEdwPosNo,ip,.false.)
      il=il+1
      call FeQuestLblMake(id,30.,il,'Reference point:','L','N')
      tpom=5.
      xpom=tpom+FeTxLength('XXXXXXXXXXXXXX')+5.
      ilp=il
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='%Explicitly'
        else if(i.eq.2) then
          Veta='%Gravity center'
        else
          Veta='Ge%om. center'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        if(i.eq.1) then
          nCrwCenterFirst=CrwLastMade
          tp=xpom+CrwgXd+5.
          xp=tp+FeTxLength('XX')+5.
          dpom=200.
          call FeQuestEdwMake(id,tp,il,xp,il,' ','L',dpom,EdwYd,1)
          nEdwRef=EdwLastMade
        else if(i.eq.3) then
          nCrwCenterLast=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      il=ilp
      Veta='Point group'
      tpom=.75*xdq
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,Veta,'C',dpom,EdwYd,1)
      nEdwPG=EdwLastMade
      il=il+2
      Veta='%Hermann-Mauguin short'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPGInter=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Schoen%flies'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPGSchoen=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=ilp+4
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Imp%roper rotation'
      xpom=5.
      tpom=xpom+CrwXd+5.
      ilm=-10*il+2
      call FeQuestCrwMake(id,tpom,ilm,xpom,ilm,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwImprop=CrwLastMade
      tpom=tpom+FeTxLengthUnder(Veta)+100.
      Veta='%Define local coordinate system'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
      nButtDefLocSyst=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+20.
      Veta='Appl%y site symmetry'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
      nButtSymmetry=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      pom=(xdq-4.*75.-10.)/4.
      pom=(xdq-80.-pom)/3.
      xpoma(4)=xdq-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      il=il+1
      j=0
      do i=1,7
        call kdoco(PrvniKiMolekuly(imp)+i-1,at,pn,1,pom,spom)
        k=idel(pn)
        j=j+1
        if(i.eq.5) j=j+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiMol(i,imp),
     1                       .false.)
        if(i.eq.1) then
          nEdwPrvPar=nEdw
          nCrwPrvPar=nCrw
        endif
        if(mod(i,4).eq.0) then
          il=il+1
          j=0
        endif
      enddo
      Veta='Edit TLS'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpoma(1),il,dpom,ButYd,Veta)
      nButtEditTLS=ButtonLastMade
      if(NDimI(KPhase).gt.0) then
        il=il+1
        tpom=5.
        Veta='Modulations:'
        call FeQuestLblMake(id,xdqp,il,Veta,'C','B')
        xpom=tpom+15.+FeTxLengthUnder('%TLS parameters')
        dpom=30.
        xuse=xpom+dpom+2.*EdwMarginSize+EdwYd+50.
        xp=xuse+30.
        tp=xp+CrwXd+5.
        xpp=tp+70.
        tpp=xpp+CrwXd+5.
        do i=1,3
          il=il+1
          if(i.eq.1) then
            Veta='Occ%upancy'
          else if(i.eq.2) then
            Veta='Po%sition'
          else if(i.eq.3) then
            Veta='%TLS parameters'
            ilp=il
          endif
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          call FeQuestIntEdwOpen(EdwLastMade,KModM(i,imp),.false.)
          call FeQuestEudOpen(EdwLastMade,0,mxw,1,0.,0.,0.)
          if(i.eq.1.or.i.eq.2) call FeQuestLblMake(id,xuse,il,'Use:',
     1                                             'L','N')
          if(i.eq.1) then
            nEdwModOcc=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%Crenel','L',CrwXd,
     1                          CrwYd,1,0)
            nCrwCrenel=CrwLastMade
          else if(i.eq.2) then
            nEdwModPos=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'Sa%w-tooth','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwSawTooth=CrwLastMade
            call FeQuestCrwMake(id,tpp,il,xpp,il,'%zig-zag','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwZigZag=CrwLastMade
          else if(i.eq.3) then
            nEdwModTLS=EdwLastMade
          endif
        enddo
        il=il+1
        xpom=10.
        Veta='Ed%it modulation parameters'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtEditMod=ButtonLastMade
        il=ilp
        call FeQuestLblMake(id,xuse,il,'Type of modulation functions:',
     1                      'L','B')
        nLblTypeModFun=LblLastMade
        xpom=xuse
        tpom=xuse+CrwgXd+5.
        ICrwGr=3
        Veta='harmonics (0,1)'
        do i=1,4
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGr)
          if(i.eq.1) then
            Veta='harmonics (0,1) orthogonalized to '//
     1           'crenel interval'
            nCrwHarm=CrwLastMade
          else if(i.eq.2) then
            Veta='Legendre polynomials in crenel interval'
            nCrwOrtho=CrwLastMade
          else if(i.eq.3) then
            Veta='x-harmonics in crenel interval'
            nCrwLegendre=CrwLastMade
          else if(i.eq.4) then
            nCrwXHarm=CrwLastMade
          endif
        enddo
        il=il+1
        tpom=xpom
        Veta='Selection limit for harmonics:'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwHarmLimit=EdwLastMade
      else
        nEdwModOcc=0
        nEdwModPos=0
        nEdwModTLS=0
        nCrwCrenel=0
        nCrwSawTooth=0
        nCrwZigZag=0
        nButtEditMod=0
        nEdwHarmLimit=0
        nCrwHarm=0
        nCrwOrtho=0
        nCrwLegendre=0
        nCrwXHarm=0
      endif
      ipg=LocateInStringArray(SmbPGI,nSmbPG,SmbPGMol(im),.true.)
      if(ipg.le.0)
     1  ipg=LocateInStringArray(SmbPGO,nSmbPG,SmbPGMol(im),.true.)
      if(ipg.le.0) then
        SmbPGMol(im)='1'
        ipg=1
      endif
c      SmbPGMolOld='xyz'
1200  if(im.ne.imo) then
        call FeQuestIntEdwOpen(nEdwMolNo,im-NMolecFrAll(KPhase)+1,
     1                         .false.)
        call FeQuestEudOpen(nEdwPosNo,1,mam(im),1,0.,0.,0.)
        call FeQuestStringEdwOpen(nEdwMolName,MolName(im))
        do i=nCrwCenterFirst,nCrwCenterLast
          lpom=RefPoint(im).eq.i-nCrwCenterFirst
          call FeQuestCrwOpen(i,lpom)
          if(i.eq.nCrwCenterFirst) then
            if(lpom.or.ipg.gt.1) then
              Veta=StRefPoint(im)
              if(Veta.eq.' ') then
                write(Veta,100)(xm(j,im),j=1,3)
                call ZdrcniCisla(Veta,3)
              endif
              call FeQuestStringEdwOpen(nEdwRef,Veta)
            else
              call FeQuestEdwClose(nEdwRef)
            endif
          endif
        enddo
1230    SmbPGMolOld=SmbPGMol(im)
        KPointOld=KPoint(im)
        call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
        if(ktls(im).gt.0) then
          i=ButtonOff
        else
          i=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtEditTLS,i)
      endif
      imp=(im-1)*mxp+ip
      PrvKi=1
      if(imp.ne.impo) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        do i=1,7
          call kdoco(PrvniKiMolekuly(imp)+i-1,at,pn,1,pom,spom)
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          call FeQuestCrwOpen(nCrw,KiMol(i,imp).ne.0)
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        call FeQuestCrwOpen(nCrwImprop,RotSign(imp).lt.0)
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModOcc
          do i=1,3
            kmodamn(i)=KModM(i,imp)
            call FeQuestIntEdwOpen(nEdw,kmodamn(i),.false.)
            nEdw=nEdw+1
          enddo
          call FeQuestCrwOpen(nCrwCrenel,KFM(1,imp).ne.0)
          call FeQuestCrwOpen(nCrwSawTooth,KFM(2,imp).eq.1)
          call FeQuestCrwOpen(nCrwZigZag,KFM(2,imp).eq.2)
        endif
      endif
      imo=im
      ipo=ip
      impo=imp
      isw=iswmol(im)
      if(NDimI(KPhase).gt.0) then
        if(KModM(1,impo).gt.0.or.KModM(2,impo).gt.0.or.
     1     KModM(3,impo).gt.0)
     2    then
          i=ButtonOff
        else
          i=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtEditMod,i)
        nCrw=nCrwHarm
        if(KFM(1,imp).gt.0) then
          call FeQuestLblOn(nLblTypeModFun)
        else
          call FeQuestLblDisable(nLblTypeModFun)
        endif
        do i=1,4
          call FeQuestCrwOpen(nCrw,i-1.eq.TypeModFunMol(imp))
          if(KFM(1,imp).le.0) call FeQuestCrwDisable(nCrw)
          if(i.eq.2) then
            if(TypeModFunMol(imp).eq.1.and.KFM(1,imp).gt.0) then
              call FeQuestRealEdwOpen(nEdwHarmLimit,OrthoEpsMol(imp),
     1                                .false.,.false.)
            else
              call FeQuestEdwDisable(nEdwHarmLimit)
            endif
          endif
          nCrw=nCrw+1
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMolNo) then
        call FeQuestIntFromEdw(nEdwMolNo,im)
        im=im+NMolecFrAll(KPhase)-1
        if(im.ne.imo) then
          ip=1
          impo=-impo
          call FeQuestIntEdwOpen(nEdwPosNo,ip,.false.)
          go to 2000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMolName)
     1  then
        Veta=EdwStringQuest(nEdwMolName)
        call UprAt(Veta)
        call AtCheck(Veta,i,j)
        if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the molecule name.'
          else if(i.eq.2) then
            if(j.ne.-im) then
              Veta='The name already exists.'
            else
              go to 1600
            endif
          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwMolName
          go to 1500
        else
          MolName(im)=Veta
        endif
1600    call FeQuestStringEdwOpen(nEdwMolName,MolName(im))
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwPosNo) then
        call FeQuestIntFromEdw(nEdwPosNo,ip)
        if(ip.ne.ipo) then
          go to 2000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.(CheckNumber.eq.nEdwModOcc.or.
     1        CheckNumber.eq.nEdwModPos.or.CheckNumber.eq.nEdwModTLS))
     2  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMolList)
     1  then
        call SelOneAtom('Select next molecule',
     1                  MolName(NMolecFrAll(KPhase)),im,
     2                  NMolecLenAll(KPhase),ich)
        im=im+NMolecFrAll(KPhase)-1
        if(im.ne.imo.and.ich.eq.0) then
          ip=1
          impo=-impo
          go to 2000
        else
          im=imo
          go to 1500
        endif
      else if((CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwCenterFirst
     1                              .and.CheckNumber.le.nCrwCenterLast)
     2         .or.CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwRef)
     3  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        call CrlSetRotMat(imp)
        ic=CheckType
        if(ic.eq.EventCrw) then
          i=CheckNumber-nCrwCenterFirst
          if(i.ne.RefPoint(im)) RefPoint(im)=i
        endif
        if(EdwStateQuest(nEdwRef).eq.EdwOpened) then
          Veta=EdwStringQuest(nEdwRef)
        else
          Veta=' '
        endif
        call CrlDefMolRefPoint(im,Veta,ich)
        if(ich.gt.0) then
          if(EdwStateQuest(nEdwRef).eq.EdwOpened) then
            EventType=EventEdw
            EventNumber=nEdwRef
            go to 1500
          endif
        else if(ich.eq.0) then
          StRefPoint(im)=Veta
        else if(ich.eq.-1) then
          StRefPoint(im)=' '
        endif
        imo=0
        impo=0
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDefLocSyst) then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        call CrlSetRotMat(imp)
        call EM40DefMolLocSyst(imp,ich)
        if(ich.eq.0) then
          call CopyMat(RotMol(1,imp),Rot,3)
          call CopyVek(TrToOrtho(1,isw,KPhase),TrMol(1,imp),9)
          call CopyVek(TrToOrthoI(1,isw,KPhase),TriMol(1,imp),9)
          do l=1,2
            if(l.eq.1) then
              call CopyVek(xm(1,im),XCentr,3)
            else
              call AddVek(XCentr,trans(1,imp),XCentr,3)
            endif
            if(LocMolSystType(imp).ge.l) then
              if(l.eq.1) then
                ii=im
              else
                ii=0
              endif
              Veta=molname(im)(:idel(molname(im)))
              write(Cislo,'(i2)') imp
              call zhusti(Cislo)
              Veta=Veta(:idel(Veta))//'#'//Cislo(:idel(Cislo))
              call CrlMakeTrMatToLocal(XCentr,Veta,LocMolSystAx(l,imp),
     1                    LocMolSystSt(1,l,imp),LocMolSystX(1,1,l,imp),
     2                    ' ',trp,trpi,ii,ich)
              if(ich.ne.0) go to 1800
              if(l.eq.1) then
                call CopyMat(trp,TrMol(1,imp),3)
                call CopyMat(trpi,TriMol(1,imp),3)
              else
                call CopyMat(trpi,TriMol(1,imp),3)
              endif
            endif
          enddo
          call MatInv(TrMol(1,imp),trpi,pom,3)
          call MatInv(TriMol(1,imp),trp,pom,3)
          call multm(rot,trpi,rotp,3,3,3)
          call multm(trp,rotp,rot,3,3,3)
          call EM40GetAngles(rot,irot(KPhase),euler(1,imp))
          do l=1,3
            euler(l,imp)=anint(euler(l,imp)*1000.)*.001
          enddo
          call CrlSetRotMat(imp)
          imo=0
          impo=0
          go to 1200
        else
          go to 1800
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPGInter)
     1  then
        call SelOneAtom('Select point group',SmbPGI,ipg,nSmbPG,ich)
        if(ich.eq.0) then
          SmbPGMol(im)=SmbPGI(ipg)
          call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
          go to 1900
        else
          go to 1800
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPGSchoen)
     1  then
        call SelOneAtom('Select point group',SmbPGO,ipg,nSmbPG,ich)
        if(ich.eq.0) then
          SmbPGMol(im)=SmbPGO(ipg)
          call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
          go to 1900
        else
          go to 1800
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwPG) then
        Veta=EdwStringQuest(nEdwPG)
        ipg=LocateInStringArray(SmbPGI,nSmbPG,Veta,.true.)
        if(ipg.le.0) then
          ipg=LocateInStringArray(SmbPGO,nSmbPG,Veta,.true.)
          if(ipg.gt.0) Veta=SmbPGO(ipg)
        else
          Veta=SmbPGI(ipg)
        endif
        if(ipg.le.0) then
          call FeChybne(-1.,-1.,'the point group isn''t on the list',
     1                  ' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwPG
          ipg=1
          go to 1500
        else
          SmbPGMol(im)=Veta
          call FeQuestStringEdwOpen(nEdwPG,Veta)
          go to 1900
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwImprop) then
        if(CrwLogicQuest(nCrwImprop)) then
          RotSign(imp)=-1
        else
          RotSign(imp)= 1
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditTLS)
     1  then
        call EM40EditTLS(imp)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSymmetry)
     1  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        neq=0
        neqs=0
        call MolSpec(imo,imo)
        do i=1,neq
          KiMol(lnp(i),imp)=0
          lnp(i)=lnp(i)+pocder(ktatmol(lat(i)))
          do j=1,npa(i)
            pnp(j,i)=pnp(j,i)+pocder(ktatmol(pat(j,i)))
          enddo
        enddo
        call RefAppEq(0,0,0)
        imo=0
        impo=0
        go to 1200
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCrenel) then
        call FeQuestIntFromEdw(nEdwModOcc,n)
        call FeQuestIntFromEdw(nEdwModPos,m)
        if(CrwLogicQuest(nCrwCrenel)) then
          call FeQuestCrwOff(nCrwSawTooth)
          call FeQuestCrwOff(nCrwZigZag)
          if(KFM(2,imp).gt.0) then
            KFM(2,imp)=0
            m=m-1
            call FeQuestIntEdwOpen(nEdwModPos,m,.false.)
          endif
          n=n+1
          KFM(1,imp)=1
        else
          n=n-1
          KFM(1,imp)=0
        endif
        call FeQuestIntEdwOpen(nEdwModOcc,n,.false.)
        call FeQuestEudOpen(nEdwModOcc,0,mxw,1,0.,0.,0.)
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        EventType=EventEdw
        EventNumber=nEdwModOcc
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.eq.nCrwSawTooth.or.CheckNumber.eq.nCrwZigZag))
     2  then
        call FeQuestIntFromEdw(nEdwModPos,n)
        call FeQuestIntFromEdw(nEdwModOcc,m)
        if(CheckNumber.eq.nCrwSawTooth) then
          call FeQuestCrwOff(nCrwZigZag)
          nCrw=nCrwSawTooth
          i=1
        else
          call FeQuestCrwOff(nCrwSawTooth)
          nCrw=nCrwZigZag
          i=2
        endif
        if(CrwLogicQuest(nCrw)) then
          if(CrwLogicQuest(nCrwCrenel)) then
            call FeQuestCrwOff(nCrwCrenel)
            if(KFM(1,imp).gt.0) then
              KFM(1,imp)=0
              m=m-1
              call FeQuestIntEdwOpen(nEdwModOcc,m,.false.)
            endif
          endif
          if(KFM(2,imp).le.0) n=n+1
          KFM(2,imp)=i
        else
          n=n-1
          KFM(2,imp)=0
        endif
        call FeQuestIntEdwOpen(nEdwModPos,n,.false.)
        call FeQuestEudOpen(nEdwModPos,0,mxw,1,0.,0.,0.)
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        EventType=EventEdw
        EventNumber=nEdwModPos
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.ge.nCrwHarm.and.CheckNumber.le.nCrwXHarm))
     2  then
        TypeModFunNew=CheckNumber-nCrwHarm
        if(TypeModFunMol(imp).eq.1)
     1    call FeQuestRealFromEdw(nEdwHarmLimit,OrthoEpsMol(imp))
        call EM40ChangeMolTypeMod(imp,TypeModFunNew,OrthoEpsMol(imp))
        nEdw=nEdwPrvPar+1
        do i=1,3
          call FeQuestRealEdwOpen(nEdw,Euler(i,imp),.false.,.false.)
          nEdw=nEdw+1
        enddo
!        nEdw=nEdwPrvPar+4
        do i=1,3
          call FeQuestRealEdwOpen(nEdw,trans(i,imp),.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditMod)
     1  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        j=0
        do i=1,6
          if(i.eq.1) then
            if(KModM(1,impo).le.0) cycle
          else if(i.eq.2.or.i.eq.3) then
            if(KModM(2,impo).le.0) cycle
          else
            if(KModM(3,impo).le.0) cycle
          endif
          j=j+1
          menp(j)=men(i)
          Ktera(j)=i
        enddo
        i=ButtonFr+nButtEditMod-1
        i=FeMenu(ButtonXMax(i),ButtonYMax(i),menp,1,j,1,1)
        if(i.gt.0) call EM40EditMolModPar(impo,Ktera(i))
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 1890
1800  imo=0
      impo=0
      go to 1200
1890  Konec=.true.
      go to 2000
1900  if(.not.EqIgCase(SmbPGMol(im),SmbPGMolOld)) then
        call EM40DefPGSyst(im,ich)
        call CrlMakeTrMatToLocal(xm(1,im),molname(im),
     1                    LocPGSystAx(im),LocPGSystSt(1,im),' ',
     2                    LocPGSystX(1,1,im),
     3                    TrPG(1,im),TriPG(1,im),im,ich)
      endif
2000  if(Konec.and.ich.ne.0) go to 3000
      if(iabs(impo).gt.0) then
        call EM40MolNewBasic(imo,iabs(impo),PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        if(impo.lt.0) impo=0
      endif
      i=iabs(imo)
      if(i.gt.0.and..not.EqIgCase(SmbPGMol(i),SmbPGMolOld)) then
        KPointOld=KPoint(i)
        call GenPg(SmbPGMol(i),rpoint(1,1,i),npoint(i),ich)
        do l=1,NPoint(i)
          call multm(TriPG(1,i),rpoint(1,l,i),px,3,3,3)
          call multm(px,TrPG(1,i),rpoint(1,l,i),3,3,3)
        enddo
        ji=(i-1)*mxp+1
        call AddVek(xm(1,i),trans(1,ji),px,3)
        call SpecPg(px,rpgp,isw,n)
        call SetIntArrayTo(ipoint(1,i),npoint(i),1)
        nexp=npoint(i)
        do k=2,n
          call multm(rpgp(1,k),RotMol(1,ji),px,3,3,3)
          call multm(RotIMol(1,ji),px,rpgp(1,k),3,3,3)
          do l=1,NPoint(i)
            if(ipoint(l,i).le.0) cycle
            call multm(rpgp(1,k),rpoint(1,l,i),px,3,3,3)
            do m=2,npoint(i)
              if(ipoint(m,i).le.0) cycle
              if(eqrv(px,rpoint(1,m,i),9,.0001)) then
                ipoint(m,i)=0
                nexp=nexp-1
                exit
              endif
            enddo
          enddo
        enddo
        NAtCalcIn=NAtCalc
        n=iam(i)*(nexp-1)
        if(NAtAll+n.ge.MxAtAll) call ReallocateAtoms(n)
        NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+n
        if(i.lt.NMolec) then
          nap=NAtMolFr(1,1)
          do j=1,i
            nap=nap+iam(j)
          enddo
          call AtSun(nap,NAtAll,nap+n)
        else
          nap=NAtAll+1
        endif
c        iam(i)=iam(i)/KPointOld+n
c        iam(i)=iam(i)+n*KPoint(i)
        m=PrvniKiAtomu(nap-1)+DelkaKiAtomu(nap-1)+1
        do k=nap,nap+n-1
          call SetBasicKeysForAtom(k)
          PrvniKiAtomu(k)=m
          DelkaKiAtomu(k)=0
        enddo
        call EM40UpdateAtomLimits
        NAtCalc=NAtCalcIn
      else

      endif
      if(i.gt.0) then
        k=0
        do l=1,npoint(i)
          if(ipoint(l,i).gt.0) k=k+1
          call srotb(rpoint(1,l,i),rpoint(1,l,i),tpoint(1,l,i))
          call multm(rpoint(1,l,i),xm(1,i),spoint(1,l,i),3,3,1)
          do m=1,3
            spoint(m,l,i)=xm(m,i)-spoint(m,l,i)
          enddo
        enddo
        kpoint(i)=k
      endif
      SmbPGMolOld=SmbPGMol(im)
      if(.not.Konec) then
        imo=0
        impo=0
        go to 1200
      endif
3000  call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      do i=1,NMolec
        if(EqIgCase(SmbPGMol(i),SmbPGMolIn(i))) cycle
        nap=NAtMolFr(1,1)
        do j=1,i-1
          nap=nap+iam(j)
        enddo
        nak=nap+iam(i)-1
        Allocate(NAtMult(nak))
        call SetIntArrayTo(NAtMult,nak,1)
        do j=nap,nak-1
          do l=j+1,nak
            if(isf(j).ne.isf(l)) cycle
            do m=1,3
              xpoma(m)=x(m,j)-x(m,l)
            enddo
            call Multm(MetTens(1,1,KPhase),xpoma,tpoma,3,3,1)
            if(sqrt(scalmul(xpoma,tpoma)).lt..1) then
              ai(j)=ai(j)+ai(l)
              ai(l)=0.
              NAtMult(l)=0
            endif
          enddo
        enddo
        do j=nap,nak
          if(NAtMult(j).eq.0) cycle
          do k=2,NPoint(i)
            if(IPoint(k,i).le.0) cycle
            do m=1,3
              px(m)=x(m,j)-xm(m,i)
              py(m)=xm(m,i)
            enddo
            call cultm(rpoint(1,k,i),px,py,3,3,1)
            do l=j,nak
              do m=1,3
                xpoma(m)=py(m)-x(m,l)
              enddo
              call Multm(MetTens(1,1,KPhase),xpoma,tpoma,3,3,1)
              if(sqrt(scalmul(xpoma,tpoma)).lt..1.and.NAtMult(l).ne.0)
     1          then
                if(l.eq.j) then
                  NAtMult(j)=NAtMult(j)+1
                else
                  NAtMult(l)=0
                endif
                exit
              endif
            enddo
          enddo
        enddo
        do j=nap,nak
          if(NAtMult(j).le.0)
     1      call CrlAtomNamesSave(Atom(j),'#delete#',1)
        enddo
        n=nap-1
        nd=0
        do j=nap,nak
          if(NAtMult(j).le.0) then
            call AtSun(n+2,NAtAll,n+1)
            NAtAll=NAtAll-1
            NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)-1
            nd=nd+1
          else
            n=n+1
            ai(n)=ai(n)/float(NAtMult(j))
          endif
        enddo
        if(nd.gt.0) then
          m=(iam(i)-nd)*mam(i)*KPoint(i)-NAtPosLen(isw,KPhase)
          iam(i)=(iam(i)-nd)*KPoint(i)
          if(m.gt.0) call ReallocateAtoms(m)
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+m)
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+m
          call EM40UpdateAtomLimits
        endif
        deallocate(NAtMult)
        call CrlAtomNamesApply
      enddo
9999  if(allocated(SmbPGMolIn)) deallocate(SmbPGMolIn)
      return
100   format(3f10.6)
      end
