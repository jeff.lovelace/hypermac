      subroutine EM40SetNewH(n)
      use Atoms_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'editm40.cmn'
      dimension u(3),ug(3),v(3),vg(3),dt(3),nd(3),nt(3),tt(3),xp(3),
     1          x4(3),
     2          xb(3),uxb(3,mxw),uyb(3,mxw),x40b(3),RMiib(9),xbm(3),
     3          xh(3,3),
     4          xc(3,0:5),uxc(3,mxw,0:5),uyc(3,mxw,0:5),x40c(3,0:5),
     5          xcm(3,0:5),
     6          RMiic(9,0:5),ic(0:5),
     7          rx((mxw+1)*(2*mxw+1)),px(2*mxw+1,3,3),dr(2*mxw+1),
     8          tt0(3),FPolA(2*mxw+1)
      logical   EqIgCase
      equivalence (xb,xc(1,0)),(uxb,uxc(1,1,0)),(uyb,uyc(1,1,0)),
     1            (x40b,x40c(1,0)),(RMiib,RMiic(1,0)),(ib,ic(0)),
     2            (xbm,xcm(1,0))
      KPhaseIn=KPhase
      call EM40GetXFromAtName(KeepAtCentr(n),ib,xb,uxb,uyb,x40b,RMiib,
     1                        ich)
      if(ich.ne.0) go to 9999
      ksw=kswa(ib)
      KPhase=kswa(ib)
      isw=iswa(ib)
      if(KeepType(n).eq.IdKeepHTetraHed) then
        NAtMax=4
      else if(KeepType(n).eq.IdKeepHTriangl) then
        NAtMax=3
      else if(KeepType(n).eq.IdKeepHApical) then
        NAtMax=6
      endif
      if((KeepType(n).eq.IdKeepHTetrahed.and.KeepNH(n).eq.3).or.
     1   (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.2)) then
        call EM40GetXFromAtName(KeepAtNeigh(n,1),ic(1),xc(1,1),
     1               uxc(1,1,1),uyc(1,1,1),x40c(1,1),RMiic(1,1),ich)
        if(ich.ne.0) go to 9999
        if(KeepNNeigh(n)+KeepNH(n).gt.NAtMax) then
          call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                  uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
          if(ich.ne.0) go to 9999
          natc=2
        else
          if(EqIgCase(KeepAtAnchor(n),'#unknown#')) then
            dpom=2.
1100        call DistFromAtom(KeepAtNeigh(n,1),dpom,isw)
            do i=1,NDist
              k=ipord(i)
              if(.not.EqIgCase(adist(k),KeepAtCentr(n))) then
                j=idel(SymCodeJanaDist(k))
                KeepAtAnchor(n)=adist(k)
                if(j.gt.0)
     1            KeepAtAnchor(n)=
     2              KeepAtAnchor(n)(:idel(KeepAtAnchor(n)))//'#'//
     3              SymCodeJanaDist(k)(:idel(SymCodeJanaDist(k)))
                call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                  uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
                if(ich.ne.0) go to 9999
                if(isf(ic(2)).eq.isfhp) cycle
                do j=1,3
                  u(j)=xc(j,1)-xb(j)
                enddo
                call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
                uu=scalmul(u,ug)
                do j=1,3
                  v(j)=xc(j,2)-xc(j,1)
                enddo
                call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
                vv=scalmul(v,vg)
                uv=scalmul(u,vg)
                pom=abs(uv/sqrt(uu*vv))
                if(pom.lt..01.or.abs(pom-1.).lt..01) go to 1120
                natc=2
                go to 1130
              endif
            enddo
            if(dpom.lt.3.) then
              dpom=dpom+.2
              go to 1100
            endif
            do i=1,3
              u(i)=xc(i,1)-xb(i)
            enddo
            call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
            uu=scalmul(u,ug)
1120        pomm=2.
            do i=1,3
              call SetRealArrayTo(v,3,0.)
              v(i)=1.
              call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
              uv=scalmul(u,vg)
              vv=scalmul(v,vg)
              pom=abs(uv/sqrt(uu*vv))
              if(pom.lt.pomm) then
                pomm=pom
                call AddVek(v,xc(1,1),xc(1,2),3)
                KeepAtAnchor(n)='#000#'
                KeepAtAnchor(n)(i+1:i+1)='1'
              endif
            enddo
            natc=2
          else
            if(KeepAtAnchor(n).eq.'#asitis') then
              KeepAngleH(n)=0.
              call EM40GetXFromAtName(KeepAtH(n,1),ic(3),xc(1,3),
     1                  uxc(1,1,3),uyc(1,1,3),x40c(1,3),RMiic(1,3),ich)
              if(ich.ne.0) go to 9999
              do i=1,3
                xc(i,2)=xc(i,3)-xb(i)+xc(i,1)
              enddo
              natc=2
              ic(2)=0
            else if(KeepAtAnchor(n)(1:1).eq.'#') then
              j=2
              do i=2,4
                if(KeepAtAnchor(n)(i:i).eq.'0') then
                  v(i-1)=0.
                else
                  v(i-1)=1.
                endif
              enddo
              call AddVek(v,xc(1,1),xc(1,2),3)
              natc=1
            else
              call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
              if(ich.ne.0) go to 9999
              natc=2
            endif
          endif
        endif
1130    if(NDimI(KPhase).le.0) then
          call EM40AllButOneH(xb,xc(1,1),xh,KeepNH(n),KeepDistH(n),
     1                        KeepAngleH(n),isw)
          do j=1,KeepNH(n)
            call CopyVek(xh(1,j),x(1,KeepNAtH(n,j)),3)
          enddo
        endif
      else if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.1).or.
     1        (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.1).or.
     2        (KeepType(n).eq.IdKeepHApical)) then
        do j=1,KeepNNeigh(n)
          call EM40GetXFromAtName(KeepAtNeigh(n,j),ic(j),xc(1,j),
     1                  uxc(1,1,j),uyc(1,1,j),x40c(1,j),RMiic(1,j),ich)
          if(ich.ne.0) go to 9999
        enddo
        call EM40AddApicalH(xb,xc(1,1),KeepNNeigh(n),x(1,KeepNAtH(n,1)),
     1                      KeepDistH(n),isw)
        natc=KeepNNeigh(n)
      else if(KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.2) then
        do j=1,2
          call EM40GetXFromAtName(KeepAtNeigh(n,j),ic(j),xc(1,j),
     1                  uxc(1,1,j),uyc(1,1,j),x40c(1,j),RMiic(1,j),ich)
          if(ich.ne.0) go to 9999
        enddo
        call EM40TetraAdd2H(xb,xc(1,1),xh,KeepDistH(n),isw)
        do j=1,2
          call CopyVek(xh(1,j),x(1,KeepNAtH(n,j)),3)
        enddo
        natc=2
      endif
      if(NDimI(KPhase).le.0) go to 9000
      kmodmn=KModA(2,ic(0))
      kmodmx=KModA(2,ic(0))
      do i=1,natc
        if(ic(i).ne.0) then
          kmodmn=min(KModA(2,ic(i)),kmodmn)
          kmodmx=max(KModA(2,ic(i)),kmodmx)
        endif
      enddo
      do i=1,KeepNH(n)
        kmodmn=min(KModA(2,KeepNAtH(n,i)),kmodmn)
        kmodmx=max(KModA(2,KeepNAtH(n,i)),kmodmx)
      enddo
      ntmx=1
      do i=1,3
        if(i.le.NDimI(KPhase)) then
          if(kcommen(KPhase).le.0) then
            nt(i)=(6*kmodmx+3)*10
          else
            nt(i)=(ngc(KPhase)+1)*NCommQProduct(isw,KPhase)
            nt(i)=(6*kmodmx+3)*10
          endif
          dt(i)=1./float(nt(i))
        else
          nt(i)=1
          dt(i)=0
        endif
        ntmx=ntmx*nt(i)
      enddo
      nx=2*kmodmx+1
      if(KCommen(KPhase).gt.0) nx=min(nx,ntmx)
      call SetRealArrayTo(rx,(nx*(nx+1))/2,0.)
      call SetRealArrayTo(px,(2*mxw+1)*9,0.)
      if(KCommen(KPhase).gt.0) then
        call CopyVek(trez(1,1,KPhase),tt0,NDimI(KPhase))
      else
        call SetRealArrayto(tt0,NDimI(KPhase),0.)
      endif
      do 4000it=1,ntmx
        call RecUnpack(it,nd,nt,NDimI(KPhase))
        do i=1,NDimI(KPhase)
          tt(i)=(nd(i)-1)*dt(i)+tt0(i)
        enddo
        do j=0,natc
          if(KeepAtAnchor(n).eq.'#asitis'.and.j.eq.natc) then
            jj=j+1
          else
            jj=j
          endif
          ia=ic(jj)
          if(ia.le.0) cycle
          call CopyVek(xc(1,jj),xcm(1,jj),3)
          call CopyVek(x40c(1,jj),x4,NDimI(KPhase))
          call cultm(Rmiic(1,jj),tt,x4,NDimI(KPhase),NDimI(KPhase),1)
          if(KModA(1,ia).gt.0) then
            if(KFA(1,ia).eq.0) then
              occ=a0(ia)
              do kk=1,KModA(1,ia)
                arg=0.
                do i=1,NDimI(KPhase)
                  arg=arg+x4(i)*float(kw(i,kk,ksw))
                enddo
                arg=pi2*arg
                occ=occ+ax(kk,ia)*sin(arg)+ay(kk,ia)*cos(arg)
              enddo
            else
              kk=KModA(1,ia)-NDimI(KPhase)
              do k=1,NDimI(KPhase)
                kk=kk+1
                x4p=x4(k)-ax(kk,ia)
                ix4p=x4p
                if(x4p.lt.0.) ix4p=ix4p-1
                x4p=x4p-float(ix4p)
                if(x4p.gt..5) x4p=x4p-1.
                if(NDimI(KPhase).eq.1) then
                  delta=a0(ia)*.5
                else
                  delta=ay(kk,ia)
                endif
                if(x4p.ge.-delta.and.x4p.le.delta) then
                  occ=1.
                else
                  occ=0.
                  go to 4000
                endif
              enddo
            endif
            if(occ.lt..01) go to 4000
          endif
          if(jj.eq.0) dr(1)=1.
          k=1
          do kk=1,KModA(2,ia)
            if(kk.lt.KModA(2,ia).or.KFA(2,ia).eq.0) then
              if(TypeModFun(ia).le.1) then
                arg=0.
                do i=1,NDimI(KPhase)
                  arg=arg+x4(i)*float(kw(i,kk,ksw))
                enddo
                arg=pi2*arg
                sna=sin(arg)
                csa=cos(arg)
              else
                if(kk.eq.1) then
                  x4p=x4(k)-ax(kk,ia)
                  ix4p=x4p
                  if(x4p.lt.0.) ix4p=ix4p-1
                  x4p=x4p-float(ix4p)
                  if(x4p.gt..5) x4p=x4p-1.
                  pom=x4p/delta
                  call GetFPol(pom,FPolA,2*kmodmx+1,TypeModFun(ia))
                endif
                sna=FPolA(k+1)
                csa=FPolA(k+2)
              endif
              do i=1,3
                xcm(i,jj)=xcm(i,jj)+Uxc(i,kk,jj)*sna+Uyc(i,kk,jj)*csa
              enddo
              if(jj.eq.0) then
                k=k+1
                dr(k)=sna
                k=k+1
                dr(k)=csa
              endif
            else
              x4p=x4(1)-Uyc(1,kk,jj)
              ix4p=x4p
              if(x4p.lt.0.) ix4p=ix4p-1
              x4p=x4p-float(ix4p)
              if(x4p.gt..5) x4p=x4p-1.
              znak=2.*x4p/Uyc(2,kk,jj)
              do i=1,3
                xcm(i,jj)=xcm(i,jj)+znak*Uxc(i,kk,jj)
              enddo
            endif
          enddo
        enddo
        if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.3).or.
     1     (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.2)) then
          if(KeepNNeigh(n)+KeepNH(n).le.NAtMax) then
            if(EqIgCase(KeepAtAnchor(n),'#unknown#')) then
c              go to 9999
            else
              if(KeepAtAnchor(n).eq.'#asitis') then
                do i=1,3
                  xcm(i,2)=xcm(i,3)-xbm(i)+xcm(i,1)
                enddo
              else if(KeepAtAnchor(n)(1:1).eq.'#') then
              else
              endif
            endif
          endif
          call EM40AllButOneH(xbm,xcm(1,1),xh,KeepNH(n),KeepDistH(n),
     1                        KeepAngleH(n),isw)
        else if(KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.2) then
          call EM40TetraAdd2H(xbm,xcm(1,1),xh,KeepDistH(n),isw)
        else if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.1).or.
     1          (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.1).or.
     2          (KeepType(n).eq.IdKeepHApical)) then
          call EM40AddApicalH(xbm,xcm(1,1),KeepNNeigh(n),xh,
     1                        KeepDistH(n),isw)
        endif
        l=0
        do i=1,nx
          do k=1,KeepNH(n)
            do j=1,3
              px(i,j,k)=px(i,j,k)+xh(j,k)*dr(i)
            enddo
          enddo
          do j=1,i
            l=l+1
            rx(l)=rx(l)+dr(i)*dr(j)
          enddo
        enddo
4000  continue
      call smi(rx,nx,ising)
      if(ising.eq.0) then
        do k=1,KeepNH(n)
          nh=KeepNAtH(n,k)
          do j=1,3
            call nasob(rx,px(1,j,k),dr,nx)
            x(j,nh)=dr(1)
            l=1
            do kk=1,KModA(2,nh)
              l=l+1
              if(l.le.nx) then
                ux(j,kk,nh)=dr(l)
              else
                ux(j,kk,nh)=0.
              endif
              l=l+1
              if(l.le.nx) then
                uy(j,kk,nh)=dr(l)
              else
                uy(j,kk,nh)=0.
              endif
            enddo
          enddo
          call qbyx(x(1,nh),xp,isw)
          do kk=1,KModA(2,nh)
            if(kk.eq.KModA(2,nh).and.KFA(2,nh).ne.0) then
              uy(1,kk,nh)=uy(1,kk,nh)+xp(1)-qcnt(1,ib)
            else
              if(TypeModFun(ia).le.1) then
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(xp(m)-qcnt(m,ib))*float(kw(m,kk,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,3
                  xpom=ux(l,kk,nh)
                  ypom=uy(l,kk,nh)
                  ux(l,kk,nh)= xpom*cosfik+ypom*sinfik
                  uy(l,kk,nh)=-xpom*sinfik+ypom*cosfik
                enddo
              endif
            endif
          enddo
        enddo
      else
        go to 9000
      endif
9000  KPhase=KPhaseIn
9999  return
      end
