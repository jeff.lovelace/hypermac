      subroutine MolModi(jii,imi,KModNew)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension kmodanp(3),kmn(3),KModNew(*),kiold(:),xp(3)
      integer PrvKi,PrvKiOld,PrvKiMod
      logical BratMol(mxpm)
      allocatable kiold
      im=imi
      ji=jii
      isw=iswmol(im)
      i1=1
      i2=1
      BratMol(1)=.true.
      call CopyVekI(KModNew,kmodanp,3)
      call ReallocateMoleculeParams(NMolec,mxp,KTLS(im),KModNew)
1200  nalloc=0
      do i=i1,i2
        PrvKi=1
        if(ktls(im).gt.0) then
          PrvKiMod=PrvKi+28
          k=28
        else
          PrvKiMod=PrvKi+7
          k=7
        endif
        PrvKiOld=1
        n=DelkaKiMolekuly(ji)-k
        if(n.gt.nalloc) then
          if(allocated(kiold)) deallocate(kiold)
          allocate(kiold(n))
          nalloc=n
        endif
        if(n.gt.0) call CopyVekI(KiMol(PrvKiMod,ji),kiold,n)
        do j=1,3
          if(kmodanp(j).le.mxw) then
            kmn(j)=kmodanp(j)
          else
            kmn(j)=KModM(j,ji)
          endif
        enddo
        call ShiftKiMol(ji,ktls(im),kmn(1),kmn(2),kmn(3),.false.)
        do j=1,3
          if(KModM(j,ji).gt.0) then
            KiMol(DelkaKiMolekuly(ji),ji)=kiold(n)
            go to 1450
          endif
        enddo
        do j=1,3
          if(kmn(j).gt.0) then
            phfm(ji)=0.
            sphfm(ji)=0.
            KiMol(DelkaKiMolekuly(ji),ji)=0
            go to 1450
          endif
        enddo
1450    j1=KModM(1,ji)
        j2=kmn(1)
        if(j2.gt.0) then
          if(j1.eq.0) then
            call SpecPos(xm(1,i),xp,0,isw,.05,n1)
            n2=9999
            do ia=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
              if(kswa(ia).ne.KPhase.or.iswa(ia).ne.isw.or.
     1           kmol(ia).ne.ji) cycle
              call SpecPos(x(1,ia),xp,0,isw,.05,n2)
              exit
            enddo
            pom=1./float(min(n1,n2))
            a0m(ji)=aimol(ji)/pom
            aimol(ji)=pom
            sa0m(ji)=0.
            KiMol(PrvKiMod+1,ji)=0
            KiMol(PrvKi,ji)=0
          endif
          l=PrvKiMod+2+2*j1
          do j=j1+1,j2
            KiMol(l  ,ji)=1
            KiMol(l+1,ji)=1
            axm(j,ji)=.01
            aym(j,ji)=.01
            saxm(j,ji)=.0
            saym(j,ji)=.0
            l=l+2
          enddo
        endif
        if(kmn(2).ne.KModM(2,ji)) then
          j1=KModM(2,ji)
          j2=kmn(2)
          l=PrvKiMod
          lold=PrvKiOld
          if(kmn(1).gt.0) l=l+1+2*kmn(1)
          if(KModM(1,ji).gt.0) lold=lold+1+2*KModM(1,ji)
          if(KFM(2,ji).ne.0.and.j2.gt.0.and.j1.gt.0) then
            l1=lold+6*(j1-1)+1
            l2=l+6*(j2-1)+1
            l3=l1+6*j1
            l4=l2+6*j2
            call CopyVekI(kiOld(l1),KiMol(l2,ji),6)
            call CopyVekI(kiOld(l3),KiMol(l4,ji),6)
            call CopyVek( utx(1,j1,ji), utx(1,j2,ji),3)
            call CopyVek( uty(1,j1,ji), uty(1,j2,ji),3)
            call CopyVek(sutx(1,j1,ji),sutx(1,j2,ji),3)
            call CopyVek(suty(1,j1,ji),suty(1,j2,ji),3)
            call CopyVek( urx(1,j1,ji), urx(1,j2,ji),3)
            call CopyVek( ury(1,j1,ji), ury(1,j2,ji),3)
            call CopyVek(surx(1,j1,ji),surx(1,j2,ji),3)
            call CopyVek(sury(1,j1,ji),sury(1,j2,ji),3)
            j2=j2-1
            j1=j1-1
          endif
          if(kmn(2).gt.KModM(2,ji)) then
            n=3*(j2-j1)
            call SetRealArrayTo( utx(1,j1+1,ji),n,.0001)
            call SetRealArrayTo( uty(1,j1+1,ji),n,.0001)
            call SetRealArrayTo(sutx(1,j1+1,ji),n,.0)
            call SetRealArrayTo(suty(1,j1+1,ji),n,.0)
            call SetRealArrayTo( urx(1,j1+1,ji),n,.0001)
            call SetRealArrayTo( ury(1,j1+1,ji),n,.0001)
            call SetRealArrayTo(surx(1,j1+1,ji),n,.0)
            call SetRealArrayTo(sury(1,j1+1,ji),n,.0)
            n=2*n
            l1=l+6*j1+1
            l3=l1+6*kmn(2)
            call SetIntArrayTo(KiMol(l1,ji),n,1)
            call SetIntArrayTo(KiMol(l3,ji),n,1)
          endif
        endif
2301    if(kmn(3).gt.KModM(3,ji)) then
          j1=KModM(3,ji)
          j2=kmn(3)
          l=PrvKiMod
          if(kmn(1).gt.0) l=l+1+2*kmn(1)
          l=l+12*kmn(2)
          if(KFM(3,ji).ne.0) then
            l1=l+12*(j1-1)
            l2=l+12*(j2-1)
            l3=l1+12*j1
            l4=l2+12*j2
            do m=1,6
              KiMol(l2+m  ,ji)=KiMol(l1+m  ,ji)
              KiMol(l2+m+3,ji)=KiMol(l1+m+3,ji)
              KiMol(l4+m  ,ji)=KiMol(l3+m  ,ji)
              KiMol(l4+m+3,ji)=KiMol(l3+m+3,ji)
              ttx(m,j2,ji)=ttx(m,j1,ji)
              tty(m,j2,ji)=tty(m,j1,ji)
              sttx(m,j2,ji)=sttx(m,j1,ji)
              stty(m,j2,ji)=stty(m,j1,ji)
              tlx(m,j2,ji)=tlx(m,j1,ji)
              tly(m,j2,ji)=tly(m,j1,ji)
              stlx(m,j2,ji)=stlx(m,j1,ji)
              stly(m,j2,ji)=stly(m,j1,ji)
            enddo
            l1=l+24*j1+18*(j1-1)
            l2=l+24*j2+18*(j2-1)
            do m=1,9
              KiMol(l2+m  ,ji)=KiMol(l1+m  ,ji)
              KiMol(l2+m+3,ji)=KiMol(l1+m+3,ji)
              tsx(m,j2,ji)=tsx(m,j1,ji)
              tsy(m,j2,ji)=tsy(m,j1,ji)
              stsx(m,j2,ji)=stsx(m,j1,ji)
              stsy(m,j2,ji)=stsy(m,j1,ji)
            enddo
            j1=j1-1
            j2=j2-1
          endif
          l1=l+12*j1
          l2=l1+12*j2
          do j=j1+1,j2
            do m=1,6
              KiMol(l1+m  ,ji)=1
              KiMol(l1+m+6,ji)=1
              KiMol(l2+m  ,ji)=1
              KiMol(l2+m+6,ji)=1
              ttx(m,j,ji)=.00001
              tty(m,j,ji)=.00001
              sttx(m,j,ji)=.0
              stty(m,j,ji)=.0
              tlx(m,j,ji)=.00001
              tly(m,j,ji)=.00001
              stlx(m,j,ji)=.0
              stly(m,j,ji)=.0
            enddo
            l1=l1+12
            l2=l2+12
          enddo
          l1=l+24*kmn(3)+18*j1
          do j=j1+1,j2
            do m=1,9
              KiMol(l1+m  ,ji)=1
              KiMol(l1+m+9,ji)=1
              tsx(m,j,ji)=.00001
              tsy(m,j,ji)=.00001
              stsx(m,j,ji)=.0
              stsy(m,j,ji)=.0
            enddo
            l1=l1+18
          enddo
        endif
        do j=1,3
          KModM(j,ji)=kmn(j)
        enddo
      enddo
9999  return
      end
