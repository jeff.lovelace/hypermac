      subroutine EM40AtomTypeSave
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*7 AtTypeOld(:),AtP,AtPOld
      integer Order(:)
      real AtNumOld(:)
      logical EqIgCase
      allocatable AtTypeOld,Order,AtNumOld
      save AtTypeOld,NAtFOld,AtNumOld,Order
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      if(allocated(AtTypeOld)) deallocate(AtTypeOld,Order,
     1                                       AtNumOld)
      NAtFOld=NAtFormula(KPhase)
      allocate(AtTypeOld(NAtFOld),Order(NAtFOld),AtNumOld(NAtFOld))
      do i=1,NAtFOld
        AtTypeOld(i)=AtTypeMenu(i)
        AtNumOld(i)=AtNum(i,KPhase)
      enddo
      go to 9999
      entry  EM40AtomTypeModify
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      call SetIntArrayTo(Order,NAtFOld,0)
      do i=1,NAtFOld
        do j=1,NAtFormula(KPhase)
          AtP=AtTypeMenu(j)
          AtPOld=AtTypeOld(i)
          ii=index(AtTypeOld(i),'#')
          jj=index(AtTypeMenu(j),'#')
          if(ii.le.0) then
            if(jj.gt.0) AtP=AtTypeMenu(j)(:jj-1)
          else
            if(jj.le.0) AtPOld=AtTypeOld(i)(:ii-1)
          endif
          if(EqIgCase(AtP,AtPOld)) then
            Order(i)=j
            exit
          endif
        enddo
      enddo
      do i=1,NAtFOld
        if(Order(i).ne.0) cycle
        pom=999999.
        k=0
        do j=1,NAtFormula(KPhase)
          ppp=abs(AtNumOld(i)-AtNum(j,KPhase))
          if(ppp.lt.pom) then
            k=j
            pom=ppp
          endif
        enddo
        Order(i)=k
      enddo
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.le.NAtCalcBasic) cycle
        if(kswa(i).ne.KPhase) cycle
        j=isf(i)
        isf(i)=Order(j)
      enddo
      if(allocated(AtTypeOld)) deallocate(AtTypeOld,Order,AtNumOld)
9999  return
      end
