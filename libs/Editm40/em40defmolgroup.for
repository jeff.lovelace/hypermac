      subroutine EM40DefMolGroup(BratMol,ich)
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      logical BratMol(mxpm)
      dimension isfm(:)
      allocatable isfm
      allocate(isfm(mxpm))
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      call SetIntArrayTo(isfm,mxpm,0)
      if(NMolecLenAll(KPhase).gt.0) then
        call EM40SetMolName
        call SelAtoms('Select molecules',MolMenu,BratMol,isfm,MaxMolPos,
     1                ich)
      endif
      deallocate(MolMenu,isfm)
      return
      end
