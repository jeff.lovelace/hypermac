      subroutine EM40NewAt(ich)
      use Basic_mod
      use Editm40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension xp(3),xo(3)
      character*80 Veta
      character*8  At
      integer SbwItemPointerQuest,UseTabsIn,RadiationUsed
      logical FeYesNo,CrwLogicQuest,PointAlreadyPresent
      external EM40NewAtCheck,FeVoid
      data biso,dmez,dmin/2*3.,.5/
      Klic=0
      go to 1050
      entry EM40NewAtFromFourier(ich)
      Klic=1
1050  if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      pom=FeTxLengthSpace('XX.XXXXXX ')
      xpom=2.*PropFontWidthInPixels
      do i=1,3
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+pom
      enddo
      xpom=xpom+20.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      ich=0
      iz=0
      l48=.false.
      if(WizardMode) go to 1210
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 1150
1100  read(m40,FormA80,end=1110) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 1100
      read(m40,102,end=1110) iswn,KPhasePeaks,RadiationUsed
      l48=.true.
      go to 1150
1110  rewind m40
1120  read(m40,FormA80,end=1150) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier minima',.false.,.true.).le.0)
     2  go to 1120
      read(m40,102,end=1150) iswn,KPhasePeaks,RadiationUsed
      l48=RadiationUsed.eq.NeutronRadiation
1150  if(l48) then
        if(Klic.eq.0) then
          il=6
        else
          il=4
        endif
      else
        il=1
        call CloseIfOpened(m40)
      endif
      if(NComp(KPhase).gt.1) il=il+1
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Inserting/replacing of '//
     1                   'atoms',0,LightGray,0,0)
      il=0
      if(l48.and.Klic.eq.0) then
        xpom=5.
        tpom=xpom+CrwgXd+10.
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      'Peaks from the last Fourier calculation',
     2                      'L',CrwgXd,CrwgYd,1,1)
        nCrwFromFourier=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,'Coordinates from '//
     1                      '%keyboard','L',CrwgXd,CrwgYd,1,1)
        nCrwFromKeyboard=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
        il=il+1
        call FeQuestLinkaMake(id,il)
      else
        nCrwFromFourier=0
        nCrwFromKeyboard=0
      endif
      tpom=5.
      dpom=50.
      if(l48) then
        xpom=5.
        tpom=xpom+CrwgXd+3.
        il=il+1
        Veta='Ski%p peaks being too close to existing atoms'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSkipShort=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
        tpom=5.
        il=il+1
        Veta='M%inimal distance'
        xpom=tpom+FeTxLengthUnder(Veta)+43.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSkipShort=EdwLastMade
        call FeQuestLblMake(id,xpom+dpom+10.,il,'Angs.','L','N')
        nLblSkipShort=LblLastMade
        call FeQuestLblOff(LblLastMade)
      else
        nCrwSkipShort=0
        nEdwSkipShort=0
        nLblSkipShort=0
      endif
      il=il+1
      Veta='%Show distances up to'
      if(.not.l48) xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxDistance=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,dmez,.false.,.false.)
      call FeQuestLblMake(id,xpom+dpom+10.,il,'Angs.','L','N')
      if(NComp(KPhase).gt.1) then
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,'%Composite part','L',
     1                      dpom,EdwYd,0)
        nEdwIsw=EdwLastMade
        if(.not.l48) call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      else
        nEdwIsw=0
        iswn=1
      endif
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwFromFourier.or.
     2    CheckNumber.eq.nCrwFromKeyboard)) then
        if(CheckNumber.eq.nCrwFromFourier) then
          if(nEdwIsw.gt.0) call FeQuestEdwClose(nEdwIsw)
          call FeQuestCrwOpen(nCrwSkipShort,.false.)
        else
          if(nEdwIsw.gt.0) call FeQuestIntEdwOpen(nEdwIsw,1,.false.)
          call FeQuestCrwClose(nCrwSkipShort)
          call FeQuestEdwClose(nEdwSkipShort)
          call FeQuestLblOff(nLblSkipShort)
        endif
        go to 1200
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSkipShort)
     1  then
        if(CrwLogicQuest(CheckNumber)) then
          call FeQuestRealEdwOpen(nEdwSkipShort,dmin,.false.,.false.)
          call FeQuestLblOn(nLblSkipShort)
        else
          call FeQuestEdwClose(nEdwSkipShort)
          call FeQuestLblOff(nLblSkipShort)
        endif
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      if(ich.eq.0) then
        if(l48.and.Klic.eq.0) l48=CrwLogicQuest(nCrwFromFourier)
        call FeQuestRealFromEdw(nEdwMaxDistance,dmez)
        if(.not.l48.and.NComp(KPhase).gt.1)
     1    call FeQuestIntFromEdw(nEdwIsw,iswn)
        if(nCrwSkipShort.gt.0) then
          if(CrwLogicQuest(nCrwSkipShort)) then
            call FeQuestRealFromEdw(nEdwSkipShort,DSkip)
            dmin=DSkip
          else
            DSkip=-1.
          endif
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        call CloseIfOpened(m40)
        go to 9999
      endif
1210  DMezN=dmez
      if(l48) then
        nan=0
        KMxMod=0
        if(RadiationUsed.eq.NeutronRadiation) then
          NPeakMax=2
        else
          NPeakMax=1
        endif
        do 1260NPeak=1,NPeakMax
          rewind m40
1220      read(m40,FormA80,end=1260) Veta
          if(Veta(1:10).ne.'----------') go to 1220
          if(NPeak.eq.1) then
            if(LocateSubstring(Veta,'Fourier maxima',.false.,.true.)
     1         .le.0) go to 1220
          else
            if(LocateSubstring(Veta,'Fourier minima',.false.,.true.)
     1         .le.0) go to 1220
          endif
          read(m40,FormA,end=1260)
1240      nan=nan+1
          read(m40,FormA80,end=1250) Veta
          if(Veta(1:10).eq.'----------') then
            if(NPeak.ne.NPeakMax) backspace m40
            go to 1250
          endif
          read(Veta,101) At,xp,k
          do i=1,k+1
            read(m40,FormA80,end=1250) Veta
          enddo
          if(k.gt.0) read(m40,100,end=1250)
          KMxMod=max(k,KMxMod)
          if(DSkip.gt.0.) then
            if(PointAlreadyPresent(xp,xo,x,NAtCalc,DSkip,.false.,dpom,i,
     1         i,iswn)) nan=nan-1
          endif
          go to 1240
1250      if(NPeak.ne.NPeakMax) nan=nan-1
1260    continue
        if(nan.gt.0) then
          allocate(atomn(nan),kmodxn(nan),xn(3,nan),Rho(nan))
          if(Klic.ne.0) then
            go to 1270
          else
            if(nan.gt.ubound(isfn,1)) go to 1270
          endif
          go to 1280
1270      if(allocated(isfn)) deallocate(isfn)
          allocate(isfn(nan))
1280      n=max(KMxMod,1)
          allocate(uxn(3,n,nan),uyn(3,n,nan))
        endif
        nan=nan-1
        i=0
        do 1360NPeak=1,NPeakMax
          rewind m40
1320      read(m40,FormA80,end=1360) Veta
          if(Veta(1:10).ne.'----------') go to 1320
          if(NPeak.eq.1) then
            if(LocateSubstring(Veta,'Fourier maxima',.false.,.true.)
     1         .le.0) go to 1320
          else
            if(LocateSubstring(Veta,'Fourier minima',.false.,.true.)
     1         .le.0) go to 1320
          endif
          read(m40,FormA,end=1360)
1340      i=i+1
          read(m40,FormA80,end=1350) Veta
          if(Veta(1:10).eq.'----------') then
            if(NPeak.ne.NPeakMax) backspace m40
            go to 1350
          endif
          read(Veta,'(a8,19x,3f9.6,12x,i3)')
     1      atomn(i),(xn(j,i),j=1,3),kmodxn(i)
          isfn(i)=0
          read(m40,'(2f9.6)',end=1350) pom,Rho(i)
          call zhusti(atomn(i))
          call uprat(atomn(i))
          do j=1,kmodxn(i)
            read(m40,'(6f9.6)',end=1350)
     1        (uxn(k,j,i),k=1,3),(uyn(k,j,i),k=1,3)
          enddo
          if(kmodxn(i).gt.0) read(m40,FormA,end=1350)
          if(DSkip.gt.0.) then
            if(PointAlreadyPresent(xn(1,i),xo,x,NAtCalc,DSkip,.false.,
     1                             dpom,j,j,iswn)) i=i-1
          endif
          go to 1340
1350      i=i-1
1360    continue
      else
        allocate(atomn(1),kmodxn(1),xn(3,1),uxn(3,1,1),uyn(3,1,1),
     1           Rho(1))
        kmodxn(1)=0
      endif
      call CloseIfOpened(m40)
      if(nan.le.0.and.l48) then
        call FeChybne(-1.,-1.,'there are no Fourier peak within '//
     1                'defined region.',' ',Warning)
        go to 9999
      endif
      isfp=1
      biso=3.
      NNewAt=0
      NModAt=0
      id=NextQuestId()
      if(WizardMode) then
        xqd=WizardLength
        il=WizardLines
        Veta='Define atomic positions:'
        call FeQuestTitleMake(id,Veta)
        iswn=1
      else
        xqd=650.
        il=17
        Veta=' '
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      endif
      ild=11
      if(l48) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do i=1,nan
          write(ln,FormA) Atomn(i)(:idel(Atomn(i)))
        enddo
        call CloseIfOpened(ln)
        xpom=5.
        dpom=xqd*.5-50.-SbwPruhXd
        il=ild+1
        call FeQuestSbwMake(id,xpom,il,dpom,ild,3,CutTextFromRight,
     1                      SbwHorizontal)
        nSbwSel=SbwLastMade
        call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'_list.tmp')
        il=ild+3
        Veta=' '
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        nLblRho=LblLastMade
        il=1
        xpom=xpom+dpom*.5
        Veta='List of peaks'
        call FeQuestLblMake(id,xpom,il,Veta,'C','N')
        nEdwNewCoordinates=0
        nButtNewCoordinates=0
      else
        il=ild/2+1
        Veta='%Type new atom coordinates'
        xpom=5.
        dpom=xqd*.5-60.
        tpom=xpom+dpom*.5
        call FeQuestEdwMake(id,tpom,il,xpom,il+1,Veta,'C',
     1                      dpom,EdwYd,0)
        nEdwNewCoordinates=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,xn,3,.true.,.false.)
        Veta='Use new coordinates ->'
        il=il+2
        pom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom+(dpom-pom)*.5,il,pom,ButYd,Veta)
        nButtNewCoordinates=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nSbwSel=0
        nLblRho=0
        nan=0
      endif
      xpom=xqd*.5-40.
      dpom=xqd-xpom-5.
      il=ild+1
      call FeQuestSbwMake(id,xpom,il,dpom,ild,1,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      nSbwList=SbwLastMade
      call EM40NewAtCheckReset
      call EM40NewAtCheck
      il=1
      Veta='Equivalent coordinates'
      tpom=xpom+40.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+50.*EnlargeFactor
      Veta='Distance'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+25.*EnlargeFactor
      Veta='Atom'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=ild+3
      Veta='Include selected peak'
      pom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom+(dpom-pom)*.5,il,pom,ButYd,Veta)
      nButtInclude=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ilp=-10*il-12
      Veta='No peaks included'
      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Veta,'C','N')
      nLblIncuded=LblLastMade
      ilp=-10*il-18
      Veta='No atom modified'
      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Veta,'C','N')
      nLblModified=LblLastMade
      il=il+2
      if(.not.WizardMode) then
        il=il+1
        Veta='%Finish'
        dpom=FeTxLengthUnder(Veta)+20.
        call FeQuestButtonMake(id,(xqd-dpom)*.5,il,dpom,ButYd,Veta)
        nButtFinish=ButtonLastMade
      else
        nButtFinish=0
      endif
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  if(l48) then
        call FeQuestEventWithCheck(id,ich,EM40NewAtCheck,FeVoid)
      else
        call FeQuestEvent(id,ich)
      endif
      if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtInclude)
     1   .or.(CheckType.eq.EventSbwDoubleClick.and.
     2        mod(CheckNumber,100).eq.nSbwList)) then
        if(l48) then
          j=SbwItemPointerQuest(nSbwSel)
        else
          j=1
        endif
        i=SbwItemPointerQuest(nSbwList)
        if(i.le.0) go to 1500
        i=MaxOrder(i)
        if(i.eq.0) then
          call EM40NewAtomComplete(xn(1,j),uxn(1,1,j),
     1                             uyn(1,1,j),kmodxn(j),iswn,ich)
        else
          call EM40NewAtomComplete(xdist(1,i),uxdist(1,1,i),
     1                             uydist(1,1,i),kmodxn(j),iswn,ich)
        endif
        if(ich.le.0) then
          if(ich.eq.0) then
            NNewAt=NNewAt+1
            if(NNewAt.eq.1) then
              Veta='One new atom has'
            else
              write(Cislo,102) NNewAt
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' new atoms have'
            endif
            Veta=Veta(:idel(Veta)+1)//'been already included'
            call FeQuestLblChange(nLblIncuded,Veta)
          else if(ich.lt.0) then
            NModAt=NModAt+1
            if(NModAt.eq.1) then
              Veta='One atom has'
            else
              write(Cislo,102) NModAt
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' atoms have'
            endif
            Veta=Veta(:idel(Veta)+1)//'been modified'
            call FeQuestLblChange(nLblModified,Veta)
          endif
          if(l48) then
            call FeQuestSbwItemSelDel(nSbwSel)
            do i=j+1,nan
              call CopyVek(xn(1,i),xn(1,i-1),3)
              do k=1,kmodxn(i)
                call CopyVek(uxn(1,k,i),uxn(1,k,i-1),3)
                call CopyVek(uyn(1,k,i),uyn(1,k,i-1),3)
              enddo
              kmodxn(i-1)=kmodxn(i)
              Atomn(i-1)=Atomn(i)
              Rho(i-1)=Rho(i)
            enddo
            nan=nan-1
          else
            nan=0
            call FeQuestRealAEdwOpen(nEdwNewCoordinates,xn,3,.true.,
     1                               .false.)
          endif
        endif
        call EM40NewAtCheckReset
        call EM40NewAtCheck
        go to 1500
      else if(CheckType.eq.EventSbwDoubleClick.and.
     1        mod(CheckNumber,100).eq.nSbwSel) then
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtNewCoordinates) then
        call FeQuestRealAFromEdw(nEdwNewCoordinates,xn)
        nan=1
        call EM40NewAtCheck
        go to 1500
      else if((CheckType.eq.EventButton.and.
     1         CheckNumber.eq.nButtFinish).or.
     2        (CheckType.eq.EventKey.and.
     3         CheckNumber.eq.JeEscape)) then
        if(NNewAt.le.0.and.NModAt.le.0) then
          Veta='Do you really want to leave the form?'
          if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) go to 1500
        else
          if(NNewAt.gt.0) then
            Veta='Do you want to include new atom'
            if(NNewAt.gt.1) Veta=Veta(:idel(Veta))//'s'
          endif
          if(NModAt.gt.0) then
            if(NNewAt.gt.0) then
              Veta=Veta(:idel(Veta))//' and modify the selected one'
              if(NModAt.gt.1) Veta=Veta(:idel(Veta))//'s'
            else
              Veta='Do you want to modify the selected atom'
              if(NModAt.gt.1) Veta=Veta(:idel(Veta))//'s'
            endif
          endif
          Veta=Veta(:idel(Veta))//'?'
          call FeYesNoCancel(-1.,YBottomMessage,Veta,1,i)
          if(i.eq.1) then
            ich=0
          else if(i.eq.2) then
            ich=1
          else
            go to 1500
          endif
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(.not.WizardMode) call FeQuestRemove(id)
      call EM40UpdateAtomLimits
9999  if(allocated(kmodxn)) deallocate(Atomn,kmodxn,xn,Rho)
      if(allocated(uxn)) deallocate(uxn,uyn)
      if(Klic.ne.0) then
        if(allocated(isfn)) deallocate(isfn)
      endif
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(f8.4)
101   format(a8,19x,3f9.6,12x,i3)
102   format(3i5)
      end
