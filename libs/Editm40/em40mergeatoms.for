      subroutine EM40MergeAtoms(ich)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*80 Veta
      data dmez/.1/
      Klic=0
      id=NextQuestId()
      Veta='Merging distance [A]'
      call FeBoldFont
      xqd=FeTxLength(Veta)+10.
      call FeNormalFont
      call FeQuestCreate(id,-1.,-1.,xqd,1,Veta,0,LightGray,0,0)
      dpom=50.
      xpom=(xqd-dpom)*.5
      call FeQuestEdwMake(id,0.,1,xpom,1,' ','C',dpom,EdwYd,0)
      nEdwDMez=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,dmez,.false.,.false.)
1000  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1000
      endif
      if(ich.eq.0) call FeQuestRealFromEdw(nEdwDMez,dmez)
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      go to 1050
      entry EM40MergeAtomsRun(DMezI,ich)
      Klic=1
      dmez=dmezi
1050  do im=0,NMolec
        if(im.gt.0) then
          if(kswmol(im).ne.KPhase) cycle
        endif
        if(im.eq.0) then
          NaFrom=1
          NaTo=NAtInd
        else
          if(im.eq.1) then
            NaFrom=NAtMolFr(1,1)
            NaTo=NaFrom+iam(im)/KPoint(im)-1
          else
            NaFrom=NaFrom+iam(im-1)
            NaTo=NaFrom+iam(im)/KPoint(im)-1
          endif
        endif
        NAll=0
        NMerge=0
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(ia.lt.NaFrom.or.ia.ge.NaTo.or.isf(ia).eq.0) cycle
          NAll=NAll+1
          isw=iswa(ia)
          j=ia+1
1100      j=KoincAtoms(ia,j,NaTo,dmez,dst)
          if(j.gt.0) then
            if(iswa(j).ne.isw.or.kswa(j).ne.KPhase) go to 1300
            if(isf(ia).eq.isf(j)) then
              do k=1,NAtActive
                if(.not.LAtActive(k)) cycle
                ja=IAtActive(k)
                if(j.eq.ja) then
                  ai(ia)=ai(ia)+ai(ja)
                  isf(ja)=0
                  NMerge=NMerge+1
                  go to 1300
                endif
              enddo
            endif
1300        j=j+1
            go to 1100
          else
            cycle
          endif
        enddo
      enddo
      call EM40CleanAtoms
      if(Klic.eq.0.and.NMerge.gt.0) then
        if(NMerge.eq.1) then
          TextInfo(1)='           One atom has'
        else
          write(Cislo,FormI15) NMerge
          call Zhusti(Cislo)
          TextInfo(1)='           '//Cislo(:idel(Cislo))//
     1                ' atoms have'
        endif
        TextInfo(1)(idel(TextInfo(1))+1:)=' been merged.'
        NInfo=1
        WaitTime=10000
        call FeInfoOut(-1.,-1.,'INFORMATION:','L')
      endif
9999  return
      end
