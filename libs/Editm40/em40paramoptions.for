      subroutine EM40ParamOptions(ich)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest,FileName,CurrentDirO,Directory
      integer FeChdir
      logical CrwLogicQuest
      dimension px(9),py(9),pz(9)
      id=NextQuestId()
      xqd=500.
      il=9
      if(NDimI(KPhase).gt.0) il=il+2
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Options',0,LightGray,
     1                   0,OKForBasicFiles)
      il=1
      xpom=5.
      Veta='Atomic displacemt parameters:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      tpom=xpom+CrwgXd+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='use %U'
        else
          Veta='use %beta'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwUseU=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.lite(KPhase))
      enddo
      il=1
      xpom=xpom+xqd*.5
      Veta='Rotation angles for molecules:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      tpom=xpom+CrwgXd+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='use a%xial angles'
        else
          Veta='use %Eulerian angles'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      2)
        if(i.eq.1) nCrwAxial=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i.eq.2-irot(KPhase))
      enddo
      il=il+1
      xpom=5.
      tpom=xpom+CrwgXd+3
      Veta='Rounding procedure:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='s.u.''s 2-19 for %Acta Cryst.'
        else if(i.eq.2) then
          nCrwRound=CrwLastMade
          Veta='s.u.''s 2-15 for some other crystallographic journals'
        else
          Veta='%one s.u. digit'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      3)
        call FeQuestCrwOpen(CrwLastMade,i.eq.RoundMethod)
      enddo
      il=il+1
      xpom=5.
      Veta='CIF options:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      il=il+1
      Veta='%CIF specific file:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=330.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCIFSpecFile=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,CIFSpecificFile)
      xpom=xpom+dpom+10.
      Veta='Bro%wse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='De%fine wave vectors'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtDefWaves=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtDefWaves=0
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefWaves) then
        call EM40DefWaves(ich)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        CurrentDirO=CurrentDir
        Veta=EdwStringQuest(nEdwCIFSpecFile)
        call ExtractDirectory(Veta,Directory)
        call ExtractFileName(Veta,FileName)
        i=FeChdir(Directory)
        call FeGetCurrentDir
        call FeFileManager('Define specific CIF file',FileName,'*.dat',
     2                     0,.true.,ich)
        if(ich.eq.0) then
          if(index(FileName,DirectoryDelimitor).le.0)
     1      FileName=CurrentDir(:idel(CurrentDir))//
     1           FileName(:idel(FileName))
          call FeQuestStringEdwOpen(nEdwCIFSpecFile,FileName)
        endif
        i=FeChdir(CurrentDirO)
        call FeGetCurrentDir
        EventType=EventEdw
        EventNumber=nEdwCIFSpecFile
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        iroto=irot(KPhase)
        if(CrwLogicQuest(nCrwAxial)) then
          irot(KPhase)=1
        else
          irot(KPhase)=0
        endif
        if(iroto.ne.irot(KPhase)) then
          do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
            do j=1,mam(i)
              ji=j+(i-1)*mxp
              call matinv(TriMol(1,ji),py,pom,3)
              call matinv(TrMol(1,ji),pz,pom,3)
              call multm(py,RotMol(1,ji),px,3,3,3)
              call multm(px,pz,py,3,3,3)
              if(RotSign(ji).lt.0) call RealMatrixToOpposite(py,py,3)
              call EM40GetAngles(py,irot(KPhase),euler(1,ji))
            enddo
          enddo
        endif
        if(CrwLogicQuest(nCrwUseU)) then
          lite(KPhase)=0
        else
          lite(KPhase)=1
        endif
        nCrw=nCrwRound
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            RoundMethod=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        CIFSpecificFile=EdwStringQuest(nEdwCIFSpecFile)
        if(index(CIFSpecificFile,DirectoryDelimitor).le.0)
     1    CIFSpecificFile=CurrentDir(:idel(CurrentDir))//
     2                    CIFSpecificFile(:idel(CIFSpecificFile))
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
      call FeQuestRemove(id)
9999  return
      end
