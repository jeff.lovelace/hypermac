      subroutine SetMatBlock(R,n,ip,jp,A,Phi,m)
      complex R(n,n),A(m,m),Factor
      Factor=exp(cmplx(0.,Phi))
      ii=0
      do i=ip,ip+m-1
        ii=ii+1
        jj=0
        do j=jp,jp+m-1
          jj=jj+1
          R(i,j)=Factor*A(ii,jj)
        enddo
      enddo
      return
      end
