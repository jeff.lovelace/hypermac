      subroutine CopyFile(File1,File2)
      include 'fepc.cmn'
      character*256 t256
      character*(*) File1,File2
      t256='"'//File1(:idel(File1))//'" "'//File2(:idel(File2))//
     1            '"'
      i=idel(t256)
      if(OpSystem.le.0) then
        call FeCopyFile(File1,File2)
      else if(OpSystem.eq.1) then
        t256=ObrLom//'cp '//t256(:i)
        call FeSystem(t256)
      else if(OpSystem.eq.2) then
        t256='copy '//t256(:i)
        call FeSystem(t256)
      else
        ln1=NextLogicNumber()
        ln2=0
        call OpenFile(ln1,File1,'formatted','old')
        if(ErrFlag.ne.0) go to 2000
        ln2=NextLogicNumber()
        call OpenFile(ln1,File1,'formatted','unknown')
        if(ErrFlag.ne.0) go to 2000
1000    read(ln1,FormA128,end=2000) t256
        write(ln2,FormA1)(t256(i:i),i=1,idel(t256))
        go to 1000
2000    call CloseIfOpened(ln1)
        call CloseIfOpened(ln2)
      endif
      return
      end
