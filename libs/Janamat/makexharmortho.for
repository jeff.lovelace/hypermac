      subroutine MakeXHarmOrtho(XMat,nd)
      dimension XMat(nd,nd),GMat(:,:)
      allocatable GMat
      allocate(GMat(nd,nd))
      do i=1,nd
        do j=1,i
          GMat(i,j)=ScProdXHarm(i,j)
          if(i.ne.j) GMat(j,i)=GMat(i,j)
        enddo
      enddo
      do n=1,nd
        do i=1,nd
          XMat(n,i)=0.
        enddo
        do j=1,n-1
          aj=0.
          do k=1,j
            aj=aj-XMat(j,k)*GMat(k,n)
          enddo
          do k=1,j
            XMat(n,k)=XMat(n,k)+aj*XMat(j,k)
          enddo
        enddo
        XMat(n,n)=1.
        pom=0.
        do i=1,n
          do j=1,n
            pom=pom+XMat(n,i)*XMat(n,j)*GMat(i,j)
          enddo
        enddo
        if(pom.gt.0.) then
          pom=sqrt(1./pom)
        else
          pom=0.
        endif
        do i=1,n
          if(i.ne.n.or.pom.ne.0.) then
            XMat(n,i)=XMat(n,i)*pom
          else
            XMat(n,i)=1.
          endif
        enddo
      enddo
      deallocate(GMat)
      return
      end
