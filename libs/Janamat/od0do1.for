      subroutine od0do1(x,y,n)
      include 'fepc.cmn'
      dimension x(n),y(n)
      do i=1,n
        y(i)=x(i)-aint(x(i))
        if(y(i).lt.0.) y(i)=y(i)+1.
        if(y(i).gt..9999.or.y(i).lt..0001) y(i)=0.
      enddo
      return
      end
