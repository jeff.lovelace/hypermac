      logical function MatRealEqMinusUnitMat(RM,n,Toll)
      dimension RM(n,n)
      MatRealEqMinusUnitMat=.true.
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            p=-1.
          else
            p=0.
          endif
          if(abs(RM(i,j)-p).gt.Toll) then
            MatRealEqMinusUnitMat=.false.
            go to 9999
          endif
        enddo
      enddo
9999  return
      end
