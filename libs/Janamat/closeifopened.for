      subroutine CloseIfOpened(n)
      logical Opened
      if(n.gt.0) then
        inquire(n,opened=opened)
        if(opened) close(n)
      endif
      return
      end
