      subroutine bessj(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      double precision bj,bjm,bjp
      dimension besp(*)
      if(n.lt.0) go to 9999
      if(x.eq.0.) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        bjm=bessj0(x)
        bj =bessj1(x)
        besp(1)=bjm
        if(n.gt.0) then
          besp(2)=bj
        else
          besp1=bj
          go to 9999
        endif
        ax=abs(x)
        tox=2./ax
        if(x.lt.0.) bj=-bj
        mez=min(ifix(ax),n+1)
        if(mez.lt.2) go to 2000
        do j=1,mez-1
          bjp=j*tox*bj-bjm
          bjm=bj
          bj=bjp
          if(j.lt.n) then
            besp(j+2)=bj
          else
            besp1=bj
          endif
        enddo
        if(mez.ge.n+1) go to 3000
2000     m=2*((n+1+int(sqrt(float(iacc*(n+1)))))/2)
        mez=max(mez,1)
        do i=mez+2,n+1
          besp(i)=0.
        enddo
        besp1=0.
        jsum=0
        sum=0.
        bjp=0.
        bj=1.
        do j=m,1,-1
          bjm=j*tox*bj-bjp
          bjp=bj
          bj=bjm
          if(abs(bj).gt.bigno) then
            bj=bj*bigni
            bjp=bjp*bigni
            do i=mez+2,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
            sum=sum*bigni
          endif
          if(jsum.ne.0) sum=sum+bj
          jsum=1-jsum
          if(j.le.n.and.j.ge.mez+1) then
            besp(j+1)=bjp
          else if(j.eq.n+1) then
            besp1=bjp
          endif
        enddo
        sum=2.*sum-bj
        sum=1./sum
        do i=mez+2,n+1
          besp(i)=besp(i)*sum
        enddo
        besp1=besp1*sum
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
