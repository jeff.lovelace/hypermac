      subroutine OpenIfExist(Ln,File,Formatted,Message)
      include 'fepc.cmn'
      character*80 ch80
      character*(*) File,Formatted,Message
      logical   ExistFile
      ErrFlag=0
      if(ExistFile(File)) then
        open(Ln,file=File,form=Formatted)
      else
        if(Message.ne.' ') then
          ch80='the file : '//File(:idel(File))//' doesn''t exist'
          call FeChybne(-1.,-1.,ch80(:idel(ch80)),Message,SeriousError)
        endif
        ErrFlag=1
      endif
      return
      end
