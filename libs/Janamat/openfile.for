      subroutine OpenFile(ln,FileName,form,status)
      include 'fepc.cmn'
      character*(*) FileName,form,status
      logical ExistFile,ef,opened
      character*80 t80
      character*40 t40
      character*10 formatted
      ErrFlag=0
C      write (*,*) "Opening: ",FileName
      inquire(file=FileName,opened=opened,err=8000)
      if(opened) then
        t80='attempt to open already opened file.'
        goto 9100
      else
        ef=ExistFile(FileName)
      endif
      if(ef) then
        if(OpSystem.le.0) then
          inquire(File=FileName,formatted=formatted,err=8000)
          call mala(formatted)
        else
          formatted='unknown'
        endif
      else
        formatted='unknown'
      endif
      if(status.eq.'new') then
        if(ef) then
          t80='cannot be opened as a new file, it already exists.'
          go to 9000
        endif
      else if(status.eq.'old') then
        if(.not.ef) then
          t80='cannot be opened as an old file, it doesn''t exist.'
          go to 9000
        endif
      else if(status.eq.'unknown') then
        go to 1000
      else
        t80='the incorrect open parameter "status" : '//
     1      status(:idel(status))//'.'
        go to 9000
      endif
1000  if(form.eq.'formatted') then
        if(ef.and.formatted.eq.'no') then
          t80='attempt to open unformatted file as formatted.'
          go to 9000
        endif
      else if(form.eq.'unformatted') then
        if(ef.and.formatted.eq.'yes') then
          t80='attempt to open formatted file as unformatted.'
          go to 9000
        endif
      else
        t80='the incorrect open parameter "form" : '//
     1      form(:idel(form))//','
        go to 9000
      endif
      open(ln,file=FileName,status=status,form=form,err=8000)
      go to 9999
8000  t80='couldn''t be opened.'
9000  ErrFlag=1
9100  id=idel(FileName)
      call FeCutName(FileName,t40,40,CutTextFromLeft)
      if(ErrFlag.eq.0) then
        i=Warning
      else
        i=SeriousError
      endif
      call FeChybne(-1.,-1.,'the file "'//t40(:idel(t40))//'"',
     1              t80,i)
9999  return
      end
