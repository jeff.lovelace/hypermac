      integer function LocateSubstring(String,SubString,CaseSensitive,
     1                                 FromLeft)
      character*(*) String,SubString
      logical CaseSensitive,FromLeft,EqIgCase
      idl =idel(String)
      idls=len(SubString)
      LocateSubstring=0
      if(idls.gt.idl) go to 9999
      if(FromLeft) then
        ip=1
        ik=idl-idls+1
        is=1
      else
        ip=idl-idls+1
        ik=1
        is=-1
      endif
      do i=ip,ik,is
        if(CaseSensitive) then
          if(String(i:i+idls-1).eq.SubString) go to 2000
        else
          if(EqIgCase(String(i:i+idls-1),SubString)) go to 2000
        endif
      enddo
      i=0
2000  LocateSubstring=i
9999  return
      end
