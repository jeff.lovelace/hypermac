      subroutine Round2Closest(R)
      pom=abs(R)
      if(pom.lt..001) then
        R=0.
        go to 9999
      else if(abs(pom-.866).lt..001) then
        pom=sqrt(.75)
      else if(abs(pom-.5).lt..001) then
        pom=.5
      else if(abs(pom-1.).lt..001) then
        pom=1.
      endif
      R=sign(pom,R)
9999  return
      end
