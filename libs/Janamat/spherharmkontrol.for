      subroutine SpherHarmKontrol(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      real h(3),f((n+1)**2)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.le.0.) then
        f=0.
        go to 9999
      endif
      costh=h(3)/r
      costh=min(costh, 1.)
      costh=max(costh,-1.)
      fi=atan2(h(1)/r,h(2)/r)
      i=0
      do l=1,n+1
        do m=1,l
          pom=(-1)**(m-1)/sqrt(2.*pi)*plgndr(l-1,m-1,costh)
          if(m.eq.1) then
            i=i+1
            f(i)=pom
          else
            csp=cos(float(m-1)*fi)
            i=i+1
            f(i)=pom*csp
            snp=sin(float(m-1)*fi)
            i=i+1
            f(i)=pom*snp
          endif
        enddo
      enddo
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          f(i)=f(i)*Clm(i)
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          f(i)=f(i)*Mlm(i)
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          f(i)=f(i)*Llm(i)
        enddo
      endif
9999  return
      end
