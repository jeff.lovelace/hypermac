      subroutine E1Z(Z,CE1)
      implicit none
      integer k
      complex*16 Z,CE1,CR,CT,CT0
      double precision Pi,El,A0,x,fk
      data Pi,El/3.141592653589793D0,0.5772156649015328D0/
      x=real(Z)
      A0=cdabs(Z)
      if(A0.EQ.0.0D0) then
        CE1=(1.d+300,0.)
      else if(A0.le.10..or.(x.lt.0..and.A0.lt.20.)) then
        CE1=(1.0,0.0)
        CR =(1.0,0.0)
        do k=1,150
           fk=dble(float(k))
           CR=-CR*fk*Z/(fk+1.0D0)**2
           CE1=CE1+CR
           if(cdabs(CR).le.cdabs(CE1)*1.0D-15) go to 15
        enddo
15      CE1=-EL-cdlog(Z)+Z*CE1
      else
        CT0=(0.,0.)
        do k=120,1,-1
          fk=dble(float(k))
          CT0=fk/(1.0D0+fk/(Z+CT0))
        enddo
        CT=1.0D0/(Z+CT0)
        CE1=CDEXP(-Z)*CT
        if(x.le.0..and.dimag(Z).EQ.0.) CE1=CE1-pi*(0.,1.)
      endif
      return
      end
