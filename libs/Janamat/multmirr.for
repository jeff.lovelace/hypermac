      subroutine MultMIRR(a,b,c,n1,n2,n3)
      dimension b(*),c(*)
      integer a(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+float(a(ij))*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
