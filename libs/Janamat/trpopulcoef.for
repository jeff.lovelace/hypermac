      subroutine TrPopulCoef(Pin,Rot,lmax,Pout)
      include 'fepc.cmn'
      include 'basic.cmn'
      real KroneckerDelta
      dimension Pin(*),Pout(*),rot(3,3),R(225)
      call matinv(Rot,R,det,3)
      det=sign(1.,det)
      r33=rot(3,3)*det
      if(abs(r33).le..99999) then
        beta=acos(r33)
        alfa=atan2(rot(3,2),rot(3,1))
        gama=atan2(rot(2,3),-rot(1,3))
      else
        alfa=atan2(rot(1,2),rot(1,1))
        gama=0.
        if(r33.gt.0.) then
          beta=0.
        else
          beta=pi
        endif
      endif
      if(det.lt.0.) then
        alfa=alfa-pi
        gama=gama-pi
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      ip=1
      Pout(1)=Pin(1)
      do l=1,lmax
        mq=0
        n=2*l+1
        k=0
        call SetRealArrayTo(R,225,0.)
        do i=1,n
          m=iabs(mq)
          fm=float(m)
          f1=Llm(i+ip)/Clm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          j=1
          do mp=0,l
            fmp=float(mp)
            f2=Clm(j+ip)/Llm(j+ip)
            cp=0.
            kp=max(0,m-mp)
            A=(-1)**kp
            pom=chbeta**(2*l-2*(kp-1)+m-mp)*shbeta**(2*(kp-1)+mp-m)
            pomd=(shbeta/chbeta)**2
            do kk=kp,min(l-mp,l+m)
              pom=pom*pomd
              cp=cp+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l-mp-kk)*pom
              A=-A
            enddo
            cm=0.
            kp=max(0,m+mp)
            A=(-1)**(mp+kp)
            pom=chbeta**(2*l-2*(kp-1)+m+mp)*shbeta**(2*(kp-1)-mp-m)
            do kk=kp,min(l+mp,l+m)
              pom=pom*pomd
              cm=cm+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l+mp-kk)*pom
              A=-A
            enddo
            A=factorial(l-mp)/factorial(l-m)*f1*f2*
     1                        0.5*(2.-KroneckerDelta(0,mp))
            angp=fm*gama+fmp*alfa
            angm=fm*gama-fmp*alfa
            csp=cos(angp)*cp
            csm=cos(angm)*cm
            snp=sin(angp)*cp
            snm=sin(angm)*cm
            if(mq.ge.0) then
              k=k+1
              R(k)=A*(csp+csm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(snp-snm)
              endif
            else
              k=k+1
              R(k)=-A*(snp+snm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(csp-csm)
              endif
            endif
            j=j+2
          enddo
          mq=-mq
          if(mod(i,2).eq.1) mq=mq+1
        enddo
        call multm(R,Pin(ip+1),Pout(ip+1),n,n,1)
        ip=ip+n
      enddo
      return
      end
