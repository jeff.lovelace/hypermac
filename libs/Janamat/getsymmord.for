      integer function GetSymmOrd(rm)
      dimension rm(3,3),rs(3,3),rp(3,3)
      logical MatRealEqUnitMat
      rs=rm
      GetSymmOrd=0
1100  GetSymmOrd=GetSymmOrd+1
      if(MatRealEqUnitMat(rs,3,.001)) go to 9999
      call Multm(rs,rm,rp,3,3,3)
      rs=rp
      go to 1100
9999  return
      end
