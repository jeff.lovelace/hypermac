      function SlaterFBOld(nl,l,sinthl,z,DerZ,DerS,ich)
      include 'fepc.cmn'
c      include 'basic.cmn'
      dimension fact(0:8)
      data fact/1.,1.,2.,6.,24.,120.,720.,5040.,40320./
      SlaterFBOld=0.
      DerZ=0.
      ich=0
      n=nl+2
      s=pi4*sinthl
      den=s**2+z**2
      if(den.le.0.) go to 9999
      fn=z**(n+1)/(fact(n)*den**n)
      if(z.ne.0.) then
        fnz=float(n+1)*fn/z
      else
        if(n.gt.1) then
          fnz=0.
        else
          fnz=1./den
        endif
      endif
      fnz=fnz-2.*z*float(n)*fn/den
      fns=-2.*s*float(n)*fn/den
      s2=s**2
      z2=z**2
      sz=s*z
      if(n.eq.1) then
        fnn=1.
        SlaterFBOld=1.
        DerZ=0.
        DerS=0.
      else if(n.eq.2) then
        fnn=2.
        if(l.eq.0) then
          SlaterFBOld=z
          DerZ=1.
          DerS=0.
        else
          SlaterFBOld=s
          DerZ=0.
          DerS=1.
        endif
      else if(n.eq.3) then
        if(l.eq.0) then
          fnn=2.
          SlaterFBOld=3.*z2-s2
          DerZ=6.*z
          DerS=-2.*s
        else if(l.eq.1) then
          fnn=8.
          SlaterFBOld=sz
          DerZ=s
          DerS=z
        else
          fnn=8.
          SlaterFBOld=s2
          DerZ=0.
          DerS=2.*s
        endif
      else if(n.eq.4) then
        if(l.eq.0) then
          fnn=24.
          SlaterFBOld=z*z2-z*s2
          DerZ=3.*z2-s2
          DerS=-2.*sz
        else if(l.eq.1) then
          fnn=8.
          SlaterFBOld=5.*s*z2-s*s2
          DerZ=10.*sz
          DerS=5.*z2-3.*s2
        else if(l.eq.2) then
          fnn=48.
          SlaterFBOld=s2*z
          DerZ=s2
          DerS=2.*sz
        else
          fnn=48.
          SlaterFBOld=s*s2
          DerZ=0.
          DerS=3.*s2
        endif
      else if(n.eq.5) then
        if(l.eq.0) then
          fnn=24.
          SlaterFBOld=5.*z2*z2-10.*s2*z2+s2*s2
          DerZ=20.*(z*z2-z*s2)
          DerS=-20.*s*z2+4.*s*s2
        else if(l.eq.1) then
          fnn=48.
          pom1=5.*z2-3.*s2
          SlaterFBOld=sz*pom1
          DerZ=s*(pom1+10.*z2)
          DerS=z*(pom1-6.*s2)
        else if(l.eq.2) then
          fnn=48.
          pom1=7.*z2-s2
          SlaterFBOld=s2*pom1
          DerZ=14.*s2*z
          DerS=(s+s)*(pom1-s2)
        else if(l.eq.3) then
          fnn=384.
          SlaterFBOld=s2*sz
          DerZ=s2*s
          DerS=3.*s2*z
        else
          fnn=384.
          SlaterFBOld=s2*s2
          DerZ=0.
          DerS=4.*s*s2
        endif
      else if(n.eq.6) then
        if(l.eq.0) then
          fnn=240.
          pom1=3.*z2**2-10.*s2*z2+3.*s2**2
          SlaterFBOld=z*pom1
          DerZ=pom1+z2*(12.*z2-20.*s2)
          DerS=sz*(-20.*z2+12.*s2)
        else if(l.eq.1) then
          fnn=48.
          pom1=35.*z2**2-42.*s2*z2+3.*s2**2
          SlaterFBOld=s*pom1
          DerZ=sz*(140.*z2-84.*s2)
          DerS=pom1+s2*(-84.*z2+12.*s2)
        else if(l.eq.2) then
          fnn=384.
          pom1=s2*z
          pom2=7.*z2-3.*s2
          SlaterFBOld=pom1*pom2
          DerZ=s2*pom2+14.*z*pom1
          DerS=(sz+sz)*pom2-pom1*6.*s
        else if(l.eq.3) then
          fnn=384.
          pom1=s*s2
          pom2=9.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*18.*z
          DerS=3.*s2*pom2-(s+s)*pom1
        else if(l.eq.4) then
          fnn=3840.
          DerZ=s2**2
          SlaterFBOld=z*DerZ
          DerS=4.*s2*sz
        else
          fnn=3840.
          SlaterFBOld=s*s2**2
          DerZ=0.
          DerS=5.*s2**2
        endif
      else if(n.eq.7) then
        if(l.eq.0) then
          fnn=720.
          pom1=z2**2
          pom2=7.*z2-35.*s2
          pom3=s2**2
          pom4=21.*z2-s2
          SlaterFBOld=pom1*pom2+pom3*pom4
          DerZ=z*(4.*z2*pom2+14.*pom1+42.*pom3)
          DerS=s*(-70.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.1) then
          fnn=1920.
          pom1=7.*z2**2-14.*s2*z2+3.*s2**2
          SlaterFBOld=sz*pom1
          DerZ=s*pom1+sz*28.*z*(z2-s2)
          DerS=z*pom1+sz*s*(-28.*z2+12.*s2)
        else if(l.eq.2) then
          fnn=1152.
          pom1=21.*z2**2-18.*s2*z2+s2**2
          SlaterFBOld=s2*pom1
          DerZ=s2*z*(84.*z2-36.*s2)
          DerS=(s+s)*pom1+s*s2*(-36.*z2+4.*s2)
        else if(l.eq.3) then
          fnn=11520.
          pom1=s2*sz
          pom2=3.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=s*s2*pom2+6.*z*pom1
          DerS=3.*s2*z*pom2-pom1*(s+s)
        else if(l.eq.4) then
          fnn=3840.
          pom1=s2**2
          pom2=11.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*22*z
          DerS=s*(4.*s2*pom2-pom1-pom1)
        else if(l.eq.5) then
          fnn=46080.
          DerZ=s*s2**2
          SlaterFBOld=z*DerZ
          DerS=5.*SlaterFBOld/s
        else
          fnn=46080.
          SlaterFBOld=s2**3
          DerZ=0.
          DerS=6.*SlaterFBOld/s
        endif
      else if(n.eq.8) then
        if(l.eq.0) then
          fnn=40320.
          pom1=z2**2
          pom2=z2-7.*s2
          pom3=s2**2
          pom4=7.*z2-s2
          SlaterFBOld=z*(pom1*pom2+pom3*pom4)
          DerZ=SlaterFBOld/z+z2*(4.*z2*pom2+pom1+pom1+14.*pom3)
          DerS=sz*(-14.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.1) then
          fnn=5760.
          pom1=z2**2
          pom2=21.*z2-63.*s2
          pom3=s2**2
          pom4=27.*z2-s2
          SlaterFBOld=s*(pom1*pom2+pom3*pom4)
          DerZ=sz*(4.*z2*pom2+42.*pom1+54.*pom3)
          DerS=SlaterFBOld/s+s2*(-126.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.2) then
          fnn=11520.
          pom1=s2*z
          pom2=21.*z2**2-30.*s2*z2+5.*s2**2
          SlaterFBOld=pom1*pom2
          DerZ=s2*pom2+pom1*z*(84.*z2-60.*s2)
          DerS=(sz+sz)*pom2+pom1*s*(-60.*z2+20.*s2)
        else if(l.eq.3) then
          fnn=11520.
          pom1=s*s2
          pom2=33.*z2**2-22.*s2*z2+s2**2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*z*(132.*z2-44.*s2)
          DerS=3.*s2*pom2+pom1*s*(-44.*z2+4.*s2)
        else if(l.eq.4) then
          fnn=46080.
          pom1=s2**2*z
          pom2=11.*z2-3.*s2
          SlaterFBOld=pom1*pom2
          DerZ=SlaterFBOld/z+pom1*22.*z
          DerS=4.*SlaterFBOld/s-pom1*6.*s
        else if(l.eq.5) then
          fnn=46080.
          pom1=s*s2**2
          pom2=13.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*26.*z
          DerS=5.*SlaterFBOld/s-pom1*(s+s)
        else if(l.eq.6) then
          fnn=645120.
          DerZ=s2**3
          SlaterFBOld=DerZ*z
          DerS=6.*SlaterFBOld/s
        else
          fnn=645120.
          pom1=s2**3
          SlaterFBOld=s*pom1
          DerZ=0.
          DerS=7.*pom1
        endif
      else
        ich=1
        go to 9999
      endif
      DerZ=pi4*(SlaterFBOld*fnz+DerZ*fn)*fnn
      DerS=pi4**2*(SlaterFBOld*fns+DerS*fn)*fnn
      SlaterFBOld=pi4*SlaterFBOld*fn*fnn
      if(SlaterFBOld.ne.0.) then
        DerZ=DerZ/SlaterFBOld
        DerS=DerS/SlaterFBOld
      else
        DerZ=0.
        DerS=0.
      endif
9999  return
      end
