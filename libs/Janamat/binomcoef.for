      function BinomCoef(n,m)
      include 'fepc.cmn'
      BinomCoef=factorial(n)/(factorial(m)*factorial(n-m))
      return
      end
