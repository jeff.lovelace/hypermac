      subroutine SetPopulTrans(Rot,lmax,R)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rot(3,3),roti(3,3),R(*)
      real KroneckerDelta
      call matinv(rot,roti,det,3)
      det=sign(1.,det)
      do i=1,3
        do j=1,3
          roti(i,j)=det*rot(i,j)
        enddo
      enddo
      if(abs(roti(3,3)).le..99999) then
        beta=acos(roti(3,3))
        alfa=atan2(roti(3,2), roti(3,1))
        gama=atan2(roti(2,3),-roti(1,3))
      else
        gama=0.
        if(roti(3,3).gt.0.) then
          beta=0.
          alfa=atan2(roti(1,2),roti(1,1))
        else
          beta=pi
          alfa=atan2(-roti(1,2),-roti(1,1))
        endif
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      if(abs(chbeta).lt..0001) chbeta=0.
      if(abs(shbeta).lt..0001) shbeta=0.
      if(chbeta.gt. .9999) chbeta= 1.
      if(shbeta.gt. .9999) shbeta= 1.
      if(chbeta.lt.-.9999) chbeta=-1.
      if(shbeta.lt.-.9999) shbeta=-1.
      ip=1
      k=1
      do l=1,lmax
        mz=0
        nl=2*l+1
        do i=1,nl
          m=iabs(mz)
          znm=isign(1,mz)
          f1=Llm(i+ip)/Mlm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          mpz=0
          do j=1,nl
            mp=iabs(mpz)
            znn=isign(1,mpz)
            f2=1.
            f2=Mlm(j+ip)/Llm(j+ip)
            A=f1*f2*float((-1)**(m+mp))/
     1        sqrt((1.+KroneckerDelta(0,m))*(1.+KroneckerDelta(0,mp)))
            argp=m*gama+mp*alfa
            argn=m*gama-mp*alfa
            k=k+1
            if(mpz*mz.gt.0.or.(mz*mpz.eq.0.and.mz+mpz.ge.0)) then
              R(k)=A*(DTrCoef(l, mp,m,shbeta,chbeta)*cos(argp)+
     1                DTrCoef(l,-mp,m,shbeta,chbeta)*cos(argn)*znn*
     2                                                         (-1)**mp)
            else
              R(k)=A*(DTrCoef(l, mp,m,shbeta,chbeta)*sin(argp)*znm-
     1                DTrCoef(l,-mp,m,shbeta,chbeta)*sin(argn)*(-1)**mp)
            endif
            mpz=-mpz
            if(mod(j,2).eq.1) mpz=mpz+1
          enddo
          mz=-mz
          if(mod(i,2).eq.1) mz=mz+1
        enddo
        ip=ip+nl
      enddo
      return
      end
