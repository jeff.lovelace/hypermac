      subroutine smiold(a,h,n,ising)
      dimension a(*),h(*)
      ising=0
      ij=0
      do 500i=1,n
        ij=ij+i
        p=a(ij)
        if(p.gt.0.) go to 500
        ising=i
        go to 9999
500   continue
      if(n.le.1) then
        a(1)=1./a(1)
        go to 9999
      endif
      do k=n,1,-1
        p=a(1)
        if(p.le.0.) then
          ising=n+1-k
          go to 9999
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            iji=ij-i
            ijm=ij-m
            a(iji)=a(ij)+q*h(ijm)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          ijm=m+i
          a(ijm)=h(i)
        enddo
      enddo
9999  return
      end
