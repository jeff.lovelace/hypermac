      subroutine Gausz(a,m,n,r,q)
      integer r,s,q(*),z(:)
      double precision eps,g,h,b(:)
      complex a(m,n),hc
      allocatable z,b
      allocate(z(m),b(m))
      eps=0.1e-5
      do i=1,m
        g=0.
        do j=1,n
          h=max(g,dble(cabs(a(i,j))))
        enddo
        if(g.ne.0) then
          b(i)=1.0/g
        else
          b(i)=1.
        endif
      enddo
      r=0
      do s=1,n
        h=0.
        do ii=r+1,m
          g=abs(a(ii,s))*b(ii)
          if(g.gt.h) then
            h=g
            k=ii
          endif
        enddo
        if(h.lt.eps) cycle
        r=r+1
        q(r)=s
        if(k.ne.r) then
          h=b(r)
          b(r)=b(k)
          b(k)=h
          do j=s,n
            hc=a(r,j)
            a(r,j)=a(k,j)
            a(k,j)=hc
          enddo
        endif
        hc=(1.0,0.0)/a(r,s)
        do j=s,n
          a(r,j)=hc*a(r,j)
        enddo
        do i=r+1,m
          do j=n,s,-1
            a(i,j)=a(i,j)-a(i,s)*a(r,j)
            p1=real(a(i,j))
            p2=aimag(a(i,j))
            if(abs(p1).lt.1.e-5) p1=0.
            if(abs(p2).lt.1.e-5) p2=0.
            a(i,j)=cmplx(p1,p2)
          enddo
        enddo
      enddo
      call SetIntArrayTo(z,n,0)
      do i=1,r
        j=q(i)
        z(j)=1
      enddo
      do i=r,2,-1
        s=q(i)
        do l=s+1,n
          if(z(l).ne.0) cycle
          do k=i-1,1,-1
            a(k,l)=a(k,l)-a(k,s)*a(i,l)
            p1=real(a(k,l))
            p2=aimag(a(k,l))
            if(abs(p1).lt.1.e-5) p1=0.0
            if(abs(p2).lt.1.e-5) p2=0.0
            a(k,l)=cmplx(p1,p2)
          enddo
        enddo
        do k=i-1,1,-1
          a(k,s)=(0.0,0.0)
        enddo
      enddo
      deallocate(z,b)
      return
      end
