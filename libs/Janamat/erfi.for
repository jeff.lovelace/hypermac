      real function erfi(x)
      real :: pi = 3.141593
      double precision :: a = .140012, TwoDivPia = 4.546894,
     1                    pom,poml,dx
      a=0.147
      TwoDivPia=2./(pi*a)
      if(x.ge.1.) then
        erfi= 1.
      else if(x.le.-1.) then
        erfi=-1.
      else
        dx=x
        poml=dlog(1.-dx**2)
        pom=TwoDivPia+.5*poml
        erfi=dsqrt(dsqrt(pom**2-poml/a)-pom)
        if(x.lt.0.) erfi=-erfi
      endif
      return
      end
