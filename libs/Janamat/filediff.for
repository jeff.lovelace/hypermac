      logical function FileDiff(File1,File2)
      include 'fepc.cmn'
      character*(*) File1,File2
      character*256 Veta1,Veta2
      FileDiff=.true.
      ln1=0
      ln2=0
      ln1=NextLogicNumber()
      call OpenFile(ln1,File1,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      ln2=NextLogicNumber()
      call OpenFile(ln2,File2,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1000  read(ln1,FormA,end=2000) Veta1
      read(ln2,FormA,end=3000) Veta2
      if(Veta1.eq.Veta2) go to 1000
      go to 9999
2000  ln=ln2
      go to 4000
3000  ln=ln1
      go to 4010
4000  read(ln,FormA,end=9000) Veta1
4010  if(Veta1.ne.' ') go to 9999
      go to 4000
9000  FileDiff=.false.
9999  call CloseIfOpened(ln1)
      call CloseIfOpened(ln2)
      ErrFlag=0
      return
      end
