      subroutine FeFileBufferOpen(FileName,OpSystem)
      integer OpSystem
      character*(*) FileName
      character*1 ch,String(16384)
      character*16384 Buffer
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,
     1        BuffLen
      equivalence (Buffer,String)
      save n,nmax,ln,BuffLen,Buffer
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      nmax=len(Buffer)
      BuffLen=nmax
      n=nmax
      go to 9999
      entry FeFileBufferGetOneCharacter(ch,ich)
      ich=0
      if(n.ge.nmax) then
        if(nmax.ge.Bufflen) then
          nmax=FeReadBinaryFile(ln,String,BuffLen)
          n=0
        else
          i=FeCloseBinaryFile(ln)
          ich=1
          go to 9999
        endif
      endif
      n=n+1
      ch=Buffer(n:n)
      go to 9999
      entry FeFileBufferClose
      i=FeCloseBinaryFile(ln)
9999  return
      end
