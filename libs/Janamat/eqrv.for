      logical function eqrv(a,b,n,dif)
      dimension a(n),b(n)
      eqrv=.false.
      do i=1,n
        if(abs(a(i)-b(i)).ge.dif) go to 9999
      enddo
      eqrv=.true.
9999  return
      end
