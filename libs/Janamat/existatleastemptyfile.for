      logical function ExistAtLeastEmptyFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      integer FeFileSize
      logical Exist
      ExistAtLeastEmptyFile=.false.
      if(FileName.eq.' ') go to 9999
      if(FileName(1:1).eq.'.'.and.FileName(2:2).ne.'/'.and.
     1   FileName(2:2).ne.'.') go to 9999
      inquire(file=FileName,exist=Exist,err=9000)
      ExistAtLeastEmptyFile=Exist
      if(ExistAtLeastEmptyFile) then
        ExistAtLeastEmptyFile=.not.FeFileSize(FileName).eq.0
      endif
      go to 9999
9000  ExistAtLeastEmptyFile=.false.
      ErrFlag=1
9999  return
      end
