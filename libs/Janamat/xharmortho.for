      function XHarmOrtho(x,n,XMat,nd)
      include 'fepc.cmn'
c      include 'basic.cmn'
      dimension XMat(nd,nd)
      XHarmOrtho=0.
      do i=1,n
        if(abs(XMat(n,i)).lt..000001) cycle
        if(i.eq.1) then
          XHarmOrtho=XHarmOrtho+XMat(n,i)*x
        else
          j=i/2
          if(mod(i,2).eq.1) then
            XHarmOrtho=XHarmOrtho+XMat(n,i)*sin(pi*float(j)*x)
          else
            XHarmOrtho=XHarmOrtho+XMat(n,i)*cos(pi*float(j)*x)
          endif
        endif
      enddo
      return
      end
