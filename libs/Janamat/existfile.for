      logical function ExistFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      integer FeFileSize
      logical Exist
      ExistFile=.false.
      if(FileName.eq.' ') go to 9999
      if(FileName(1:1).eq.'.'.and.FileName(2:2).ne.'/'.and.
     1   FileName(2:2).ne.'.') go to 9999
      inquire(file=FileName,exist=Exist,err=9000)
      ExistFile=Exist
      if(ExistFile) then
        ExistFile=.not.FeFileSize(FileName).eq.0
        if(.not.ExistFile) call DeleteFile(FileName)
      endif
      go to 9999
9000  ExistFile=.false.
      ErrFlag=1
9999  return
      end
