      subroutine SetGenRot(fi,u,isgn,r)
      dimension r(3,3),u(3)
      sgn=isgn
      cs=sgn*cos(fi)
      sn=sgn*sin(fi)
      call UnitMat(r,3)
      do i=1,3
        r(i,i)=r(i,i)*cs
      enddo
      do i=1,3
        do j=1,3
          if(i.ne.j) then
            k=6-i-j
            pom=sn*u(k)
            if(mod(2*i+j,3).eq.1) pom=-pom
            r(i,j)=r(i,j)+pom
          endif
        enddo
      enddo
      cs=sgn*(1.-sgn*cs)
      do i=1,3
        do j=1,3
          r(i,j)=r(i,j)+u(i)*u(j)*cs
        enddo
      enddo
      return
      end
