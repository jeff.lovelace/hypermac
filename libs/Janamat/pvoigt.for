      subroutine PVoigt(dt,func,dfdtp,dfds,dfdg)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      data amlp,bmlp/.819449,1.656854/
      dtq=dt**2
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        bunbo=(.5*fwhm)**2+dtq
        tl=fwhm/(pi2*bunbo)
        if(CalcDer) then
          dtldt=-2.*etaPwd*dt*tl/bunbo
          tv=etaPwd*(tl/fwhm-pi*tl**2)
        endif
      else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
        aml=amlp/fwhm
        bml=bmlp/fwhm**2
        den=1.+bml*dtq
        tl=aml/den**2
        if(CalcDer) then
          dtldt=-4.*aml*bml*dt/den**3
          tv=4.*tl/den*bml/fwhm*dtq-tl/fwhm
        endif
      else
        tl=0.
        if(CalcDer) then
          dtldt=0.
          tv=0.
        endif
      endif
      if((KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt).and.sigp.ne.0.)
     2  then
        ex=-.5*dtq/sigp
        tg=.9394373*ExpJana(ex,ich)/fwhm
      else
        ex=0.
        tg=0.
      endif
      func=etaPwd*tl+(1.-etaPwd)*tg
      if(CalcDer) then
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          ts=-2.*(1.-etaPwd)*tg*(ex+.5)/fwhm
          if(sigp.ne.0.) then
            pomg=-(1.-etaPwd)*tg*dt/sigp
          else
            pomg=0.
          endif
        else
          ts=0.
          pomg=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          poml=dtldt
        else
          poml=0.
        endif
        dfdtp=poml+pomg
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dfds=dedffg*(tl-tg)+(tv+ts)*dfwdg
          if(sqsg.ne.0.) then
            dfds=1.17741*dfds/sqsg
          else
            dfds=0.
          endif
        else
          dfds=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dfdg=dedffl*(tl-tg)+(tv+ts)*dfwdl
        else
          dfdg=0.
        endif
      endif
      return
      end
