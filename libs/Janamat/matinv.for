      subroutine matinv(a,b,det,n)
      dimension a(n,n),b(n,n),ipiv(:),indxr(:),indxc(:)
      allocatable ipiv,indxr,indxc
      allocate(ipiv(n),indxr(n),indxc(n))
      det=1.
      do i=1,n
        do j=1,n
          b(i,j)=a(i,j)
        enddo
      enddo
      do j=1,n
        ipiv(j)=0
      enddo
      do i=1,n
        big=0.
        do j=1,n
          if(ipiv(j).ne.1) then
            do k=1,n
              if(ipiv(k).eq.0) then
                if(abs(B(j,k)).ge.big) then
                  big=abs(b(J,K))
                  irow=j
                  icol=k
                endif
              else if(ipiv(k).gt.1) then
                det=0.
                go to 9999
              endif
            enddo
          endif
        enddo
        ipiv(icol)=ipiv(icol)+1
        if(irow.ne.icol) then
          do l=1,n
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
          enddo
          det=-det
        endif
        indxr(i)=irow
        indxc(i)=icol
        if(b(icol,icol).eq.0.) then
          det=0.
          go to 9999
        endif
        det=det*b(icol,icol)
        pivinv=1./b(icol,icol)
        b(icol,icol)=1.
        do l=1,n
          b(icol,l)=b(icol,l)*pivinv
        enddo
        do ll=1,n
          if(ll.ne.icol) then
            dum=b(ll,icol)
            b(ll,icol)=0.
            do l=1,n
              b(ll,l)=b(ll,l)-b(icol,l)*dum
            enddo
          endif
        enddo
      enddo
      do l=n,1,-1
        if(indxr(l).ne.indxc(l)) then
          do k=1,n
            dum=b(k,indxr(l))
            b(k,indxr(l))=b(k,indxc(l))
            b(k,indxc(l))=dum
          enddo
        endif
      enddo
9999  deallocate(ipiv,indxr,indxc)
      return
      end
