      subroutine ctireal(text,x,dx,a,da,n,iw,id,veta,klic)
      include 'fepc.cmn'
      dimension a(n),da(n)
      character*(*) text
      character*120 format
      character*80 veta
      character*6  fwd
      character*2  nty
      if(n.le.0) return
      np=1
      if(iw.gt.0) then
        if(id.gt.0) then
          write(fwd,'(''f'',i2,''.'',i2)') iw,id
        else
          write(fwd,'(''i'',i2)') iw
        endif
      endif
      format=text(1:idel(text))
      call uprap(format)
      ifk=idel(format)
1000  if(iw.gt.0) then
        format=format(1:ifk)//' ['','
        do i=np,n-1
          format=format(1:idel(format))//fwd//','','','
        enddo
        format=format(1:idel(format))//fwd//',''] : '
        if(id.gt.0) then
          if(n.eq.1) then
            write(out,'('' '//format(1:idel(format)+1)//''',$)') dx
          else
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1            (da(i),i=np,n)
          endif
        else
          if(n.eq.1) then
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1            nint(dx)
          else
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1           (nint(da(i)),i=np,n)
          endif
        endif
      else
        format=format(1:ifk)//' : '
        write(out,'('' '//format(1:idel(format)+1)//''',$)')
      endif
      read(dta,FormA) veta
      idelv=idel(veta)
      if(idelv.le.0.and.iw.ne.0) then
        if(n.eq.1) then
          x=dx
        else
          do i=np,n
            a(i)=da(i)
          enddo
        endif
      else
        if(idelv.eq.0) then
          write(out,'('' The string is empty, try again'')')
          go to 1000
        endif
        k=0
        if(n.eq.1) then
          call kus(veta,k,cislo)
          x=fract(cislo,ich)
          if(ich.eq.0) return
          if(klic.eq.1) then
            klic=-1
            return
          endif
          write(out,102)
          go to 1000
        else
          do i=np,n
            call kus(veta,k,cislo)
            a(i)=fract(cislo,ich)
            if(ich.ne.0) go to 1250
            if(k.eq.80) go to 1260
          enddo
          return
1250      if(klic.eq.1) then
            klic=-1
            return
          endif
          np=i
          write(out,'('' Error - non-numerical character in the '',i1,
     1                a2,''item of this line'')') np,nty(np)
          go to 1000
1260      np=i+1
          if(np.gt.n) return
          write(out,'('' Response not complete continue with the '',i1,
     1                a2,'' item of this line'')') np,nty(np)
          go to 1000
        endif
      endif
102   format(' String contains non-numerical character, try again')
      end
