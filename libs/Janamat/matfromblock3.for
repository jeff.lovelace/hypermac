      subroutine MatFromBlock3(r3,rm,n)
      include 'fepc.cmn'
      dimension rm(*),r3(3,3)
      call UnitMat(rm,n)
      do j=1,3
        do i=1,3
          rm(i+(j-1)*n)=r3(i,j)
        enddo
      enddo
      return
      end
