      subroutine CheckEOLOnFile(FileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      character*80  Message
      character*256 t256
      character*1   ch1,String(256)
      integer FileType,CR,FeOpenBinaryFile,FeReadBinaryFile,
     1        FeCloseBinaryFile
      logical FeYesNoHeader,EOL
      equivalence (t256,String)
      data CR,LF/13,10/
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      lno=0
      if(ln.le.0) then
        call FeCutName(FileName,Message,70-26,CutTextFromLeft)
        Message='cannot open file "'//
     1              Message(:idel(Message))//'"'
        call FeChybne(-1.,-1.,Message,' ',SeriousError)
        ErrFlag=1
        go to 9000
      endif
      n=FeReadBinaryFile(ln,String,256)
      do i=1,255
        ich1=ichar(t256(i:i))
        if(ich1.eq.CR) then
          ich2=ichar(t256(i+1:i+1))
          if(ich2.eq.LF) then
            FileType=-1
          else if(ich2.eq.CR) then
            ich3=ichar(t256(i+2:i+2))
            if(ich3.eq.LF) then
              FileType=-2
            else
              FileType=2
            endif
          else
            FileType=2
          endif
          go to 1200
        else if(ich1.eq.LF) then
          FileType=1
          go to 1200
        endif
      enddo
      go to 9000
1200  if(OpSystem.eq.FileType) go to 9000
      if(Klic.ge.1) then
        NInfo=3
        if(FileType.eq.-1) then
          Cislo='Windows CR/LF'
        else if(FileType.eq.1) then
          Cislo='UNIX LF'
        else if(FileType.eq.2) then
          Cislo='MAC CR'
        endif
        t256='Do you want to continue?'
        if(Klic.eq.1) then
          TextInfo(1)='End of line characters of one or more basic '//
     1                'JANA files are not'
          TextInfo(2)='compatible with currently used system. Jana '//
     1                'must convert them.'
          t256=t256(:idel(t256)-1)//' with this structure?'
        else
          call FeCutName(FileName,TextInfo(1),len(TextInfo(1))-35,
     1                   CutTextFromLeft)
          TextInfo(1)='End of line characters of "'//
     1                TextInfo(1)(:idel(TextInfo(1)))//'" is not'
          TextInfo(2)='compatible with currently used system. Jana '//
     1                'must convert it.'
          t256='Do you want to continue?'
        endif
        TextInfo(3)='Jana converts between Windows, MacIntosh and Unix.'
        if(FeYesNoHeader(-1.,-1.,t256,1)) then
          go to 2000
        else
          ErrFlag=1
        endif
      else
        go to 2000
      endif
      go to 9000
2000  i=FeCloseBinaryFile(ln)
      call FeFileBufferOpen(FileName,OpSystem)
      lno=NextLogicNumber()
      open(lno,file='WorkFile.tmp')
      n=0
2100  id=0
      t256=' '
2200  call FeFileBufferGetOneCharacter(ch1,ich)
      if(ich.ne.0) go to 5000
      if(FileType.eq.1) then
        EOL=ichar(ch1).eq.LF
      else
        EOL=ichar(ch1).eq.CR
        if(EOL.and.FileType.le.0) then
          call FeFileBufferGetOneCharacter(ch1,ich)
          if(ich.ne.0) go to 5000
          if(FileType.eq.-2) then
            call FeFileBufferGetOneCharacter(ch1,ich)
            if(ich.ne.0) go to 5000
          endif
        endif
      endif
      if(EOL) then
        n=n+1
        if(n.eq.1000) then
          Message='     0 records of "'//FileName(:idel(FileName))//
     1            '" already converted'
          write(Message(1:6),100) n
          call FeTxOut(-1.,-1.,Message)
        else if(n.gt.5000.and.mod(n,5000).eq.0) then
          write(Message(1:6),100) n
          call FeTxOutCont(Message)
        endif
        if(id.gt.0) then
          write(lno,FormA1)(t256(i:i),i=1,id)
        else
          write(lno,FormA1)
        endif
        go to 2100
      else
        id=min(id+1,256)
        t256(id:id)=ch1
        go to 2200
      endif
5000  if(n.ge.1000) then
        write(Message(1:6),100) n
        call FeTxOutCont(Message)
        call FeTxOutEnd
        call FeReleaseOutput
      endif
      i=FeCloseBinaryFile(ln)
      call CloseIfOpened(lno)
      call MoveFile('WorkFile.tmp',FileName)
      go to 9999
9000  i=FeCloseBinaryFile(ln)
      call CloseIfOpened(lno)
      call DeleteFile('WorkFile.tmp')
9999  return
100   format(i6)
      end
