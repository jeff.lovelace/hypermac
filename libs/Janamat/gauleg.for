      subroutine gauleg(x1,x2,x,w,n)
      dimension x(n),w(n)
      double precision eps
      parameter(eps=3.d-14)
      double precision p1,p2,p3,xl,xm,z,z1,dj
      xl=0.5d0*(x2-x1)
      xm=0.5d0*(x2+x1)
      m=(n+1)/2
      do i=1,m
        z=dcos(3.1415926536d0*(float(i)-.25d0)/(float(n)+.5d0))
1000    p1=1.d0
        p2=0.d0
        do j=1,n
          dj=float(j)
          p3=p2
          p2=p1
          p1=((2.d0*dj-1.d0)*z*p2-(dj-1.d0)*p3)/dj
        enddo
        pp=float(n)*(z*p1-p2)/(z**2-1.d0)
        z1=z
        z=z1-p1/pp
        if(dabs(z-z1).gt.eps) go to 1000
        x(i)=xm-xl*z
        x(n-i+1)=xm+xl*z
        w(i)=2.*xl/((1.-z**2)*pp**2)
        w(n-i+1)=w(i)
      enddo
      return
      end
