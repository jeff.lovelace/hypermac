      double precision function QHesterOriginal(x,y)
      double precision :: pom1,pom2,snx,sny,Mez1=.999999999d0
      real x,y
      sny=dsin(dble(y))
      snx=dsin(dble(x))
      pom1=min((snx-sny**2)/((snx-1.d0)*sny),Mez1)
      pom1=max(pom1,-Mez1)
      pom2=min((snx+sny**2)/((snx+1.d0)*sny),Mez1)
      pom2=max(pom2,-Mez1)
      QHesterOriginal=.5d0*(-dasin(pom1)+dasin(pom2))
      return
      end
