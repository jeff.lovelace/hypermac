      subroutine IndUnpack(m,ih,ihd,ihm,n)
      dimension ih(n),ihd(n),ihm(n)
      j=m
      do i=n,2,-1
        k=mod(j,ihd(i))
        ih(i)=k-ihm(i)
        j=j/ihd(i)
      enddo
      ih(1)=j-ihm(1)
      return
      end
