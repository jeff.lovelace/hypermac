      subroutine vecnor(u,ug)
      dimension u(3),ug(3)
      pom=scalmul(u,ug)
      if(pom.gt.0.) then
        pom=1./sqrt(pom)
        do i=1,3
          ug(i)=ug(i)*pom
          u(i)=u(i)*pom
        enddo
      endif
      return
      end
