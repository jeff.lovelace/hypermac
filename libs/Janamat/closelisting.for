      subroutine CloseListing
      include 'fepc.cmn'
      logical Opened
      if(LstOpened) then
        inquire(lst,opened=opened)
        if(Opened) then
          do i=1,mxline-line
            write(lst,FormA1)
          enddo
          close(lst)
        endif
        LstOpened=.false.
      endif
      return
      end
