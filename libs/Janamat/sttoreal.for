      subroutine StToReal(Veta,k,X,n,Message,ErrFlg)
      include 'fepc.cmn'
      dimension X(n)
      character*(*) Veta
      character*80  Polozka
      character*7 FormR
      character*5 FormI
      character*1 t1,tm1
      logical   Message
      integer ErrFlg
      data FormR,FormI/'(f15.0)','(i10)'/
      kin=k
      ErrFlg=0
      call SetRealArrayTo(X,n,0.)
      id=idel(Veta)
      if(id.le.0) go to 9900
      nn=0
      i=k
1000  idl=0
      Polozka=' '
      t1=' '
1100  if(idl.eq.0) then
        tm1=' '
      else
        tm1=t1
      endif
      i=i+1
      if(i.gt.id) go to 2000
      t1=Veta(i:i)
      call Mala(t1)
      if(ichar(t1).eq.9) t1=' '
      if(t1.eq.' ') then
        if(idl.eq.0) then
          go to 1100
        else
          go to 2000
        endif
      else if(index(Cifry(12:),t1).gt.0.and.idl.gt.0) then
        if(tm1.eq.'e'.or.tm1.eq.'d') then
          go to 1500
        else
          i=i-1
          go to 2000
        endif
      endif
1500  idl=idl+1
      Polozka(idl:idl)=t1
      go to 1100
2000  if(idl.le.0) go to 5000
      nn=nn+1
      j=index(Polozka,'/')
      if(j.le.0) then
        if(index(Polozka,'e').ne.0) then
          FormR(2:2)='e'
        else if(index(Polozka,'d').ne.0) then
          FormR(2:2)='d'
        else
          FormR(2:2)='f'
          if(index(Polozka,'.').le.0) then
            idl=idl+1
            Polozka(idl:idl)='.'
          endif
        endif
        write(FormR(3:4),100) idl
        read(Polozka,FormR,err=9000) X(nn)
      else
        if(j.eq.1.or.j.eq.idl) then
          X(nn)=0.
          go to 3000
        endif
        write(FormI(3:4),100) j-1
        read(Polozka(1:j-1),FormI,err=9000) i1
        write(FormI(3:4),100) idl-j
        read(Polozka(j+1:idl),FormI,err=9000) i2
        X(nn)=float(i1)/float(i2)
      endif
3000  if(nn.lt.n) go to 1000
5000  k=i
      if(nn.lt.n) go to 9100
      go to 9900
9000  errflg=1
      k=kin
      if(Message) then
        call FeChybne(-1.,-1.,'illegal real number - try again',Polozka,
     1                SeriousError)
      endif
      go to 9900
9100  errflg=2+nn
      if(Message) then
        call FeChybne(-1.,-1.,'the string doesn''t contains all '//
     1                'requested real numbers',Veta(1:idel(Veta)),
     2                SeriousError)
      endif
9900  k=min(len(Veta),k)
      if(k.ge.len(Veta)) go to 9999
9910  if(Veta(k+1:k+1).eq.' ') then
        k=k+1
        if(k.lt.len(Veta)) go to 9910
      endif
9999  return
100   format(i2)
      end
