      logical function EqIgCase(St1,St2)
      character*(*) St1,St2
      character*1   CharMal
      id1=idel(St1)
      id2=idel(St2)
      EqIgCase=.false.
      if(id1.ne.id2) go to 9999
      do i=1,id1
        if(CharMal(St1(i:i)).ne.CharMal(St2(i:i))) go to 9999
      enddo
      EqIgCase=.true.
9999  return
      end
