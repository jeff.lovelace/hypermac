      function ExpJanaOld(x,ich)
      double precision dx,fdx
      dx=x
      ich=0.
      if(dx.gt.700.) then
        ExpJanaOld=1.e+30
        ich=1
        go to 9999
      else if(dx.lt.-700.) then
        ExpJanaOld=0.
        ich=2
        go to 9999
      endif
      fdx=dexp(dx)
      if(fdx.gt.1.e+30) then
        fdx=1.e+30
        ich=1
      else if(fdx.lt.1.e-30) then
        fdx=0.
        ich=2
      endif
      ExpJanaOld=fdx
9999  return
      end
