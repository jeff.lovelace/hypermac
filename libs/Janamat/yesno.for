      logical function yesno(text,idflt)
      include 'fepc.cmn'
      character*(*) text
      character*120 format
      character*80 t80
      logical EqIgCase
      if(idflt.eq.1) then
        format=text(1:idel(text))//'? ([y]/n) : '
      else
        format=text(1:idel(text))//'? (y/[n]) : '
      endif
      call uprap(format)
1000  write(out,'('' '//format(1:idel(format)+1)//''',$)')
      read(dta,FormA) t80
      if(idel(t80).eq.0) then
        i=idflt
      else
        call zhusti(t80)
        if(EqIgCase(t80(1:1),'y')) then
          i=1
        else if(EqIgCase(t80(1:1),'n')) then
          i=0
        else
          write(out,'('' Your answer is not correct, try again'')')
          go to 1000
        endif
      endif
      yesno=i.eq.1
      return
      end
