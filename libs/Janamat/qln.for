      subroutine qln(a,rot,diag,n)
      dimension a(n,n),rot(n,n),diag(n)
      dimension e(:)
      allocatable e
      allocate(e(n))
      call CopyMat(a,rot,n)
      call tred2(rot,n,n,diag,e)
      call tqli(diag,e,n,n,rot,ich)
      if(ich.ne.0) then
        do i=1,n
          diag(i)=a(i,i)
        enddo
        call UnitMat(rot,n)
      endif
      deallocate(e)
      end
