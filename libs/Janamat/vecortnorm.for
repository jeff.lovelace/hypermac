      subroutine VecOrtNorm(v,n)
      dimension v(n)
      pom=VecOrtLen(v,n)
      if(pom.le.0.) go to 9999
      pom=1./pom
      do i=1,n
        v(i)=v(i)*pom
      enddo
9999  return
      end
