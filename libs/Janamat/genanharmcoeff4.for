      subroutine GenAnharmCoeff4
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(:),im(:),iap(:,:),iar(:,:),iapp(:,:),iapr(:,:),
     1          idlcn(:,:),ipocn(:,:)
      integer Order(:,:),Used(:),Exponent10,FeChDir,TRankP
      character*80  Veta,Frmt
      character*256 CurrentDirO
      allocatable ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn
      allocate(ia(6),im(6),iap(6,256),iar(6,256),iapp(256,4),
     1         iapr(256,4),Order(256,4),Used(256),idlcn(256,4),
     2         ipocn(256,4))
      CurrentDirO=CurrentDir
      i=FeChdir('C:\Jana2006\testy')
      call SetIntArrayTo(im,6,4)
      do n=4,4
        n4=4**n
        do 1200m=1,n4
          call RecUnpack(m,ia,im,n)
          do i=1,n
            iap(n-i+1,m)=ia(i)
          enddo
          k=0
          do i=1,4
            do j=1,n
              if(iap(j,m).eq.i) then
                k=k+1
                iar(k,m)=i
                if(k.eq.n) go to 1200
              endif
            enddo
          enddo
1150      pause
1200    continue
        do i=1,n4
          ip =0
          ipr=0
          do j=1,n
            ip =ip *4+iap(j,i)
            ipr=ipr*4+iar(j,i)
          enddo
          iapp(i,n-2)=ip
          iapr(i,n-2)=ipr
          Order(i,n-2)=0
          Used(i)=0
        enddo
        ip=0
        do 1500i=1,n4
          if(Used(i).ne.0) go to 1500
          ip=ip+1
          Order(ip,n-2)=i
          Used(i)=1
          do 1400j=i+1,n4
            if(Used(j).ne.0.or.iapr(i,n-2).ne.iapr(j,n-2)) go to 1400
            ip=ip+1
            Order(ip,n-2)=j
            Used(j)=1
1400      continue
1500    continue
      enddo
      ln=NextLogicNumber()
      open(ln,file='Anh_common')
      Veta='      data ipec4/'
      m=0
      do n=4,4
        mx=0
        n4=4**n
        do i=1,n4
          mx=max(mx,iapp(i,n-2))
        enddo
        i=Exponent10(float(mx))+1
        if(i.eq.2) then
          Frmt='(18(i2,'',''))'
          k=18
        else if(i.eq.3) then
          Frmt='(13(i3,'',''))'
          k=13
        else if(i.eq.4) then
          Frmt='(11(i4,'',''))'
          k=11
        endif
        do j=1,n4,k
          kk=min(k,n4-j+1)
          write(Veta(18:),Frmt)(iapp(Order(j+i-1,n-2),n-2),i=1,kk)
          if(j+kk-1.ge.n4.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data idlc4/'
      Frmt='(18(i2,'',''))'
      k=18
      m=0
      do n=4,4
        n4=4**n
        nn=1
        idlcn(nn,n-2)=0
        ipocn(nn,n-2)=1
        do j=1,n4
          i=Order(j,n-2)
          if(idlcn(nn,n-2).eq.0) then
            iv=iapr(i,n-2)
            idlcn(nn,n-2)=1
          else if(iapr(i,n-2).eq.iv) then
            idlcn(nn,n-2)=idlcn(nn,n-2)+1
          else
            nn=nn+1
            ipocn(nn,n-2)=ipocn(nn-1,n-2)+idlcn(nn-1,n-2)
            idlcn(nn,n-2)=1
            iv=iapr(i,n-2)
          endif
        enddo
        TRankP=nn
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(idlcn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data cmlt4/'
      Frmt='(13(f3.0,'',''))'
      k=13
      m=0
      do n=4,4
        nn=TRankP
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(float(idlcn(j+i-1,n-2)),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data ipoc4/'
      m=0
      do n=4,4
        nn=TRankP
        Frmt='(13(i3,'',''))'
        k=13
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(ipocn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      do i=1,35
        call NToStrainString4(i,Veta)
        write(Cislo,'(i5)') i
        call FeWinMessage(Cislo,Veta)
      enddo
      call CloseIfOpened(ln)
      i=FeChdir(CurrentDirO)
      deallocate(ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn)
      return
      end
