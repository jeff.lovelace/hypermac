      subroutine CheckFileIfNotReadOnly(FileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      logical FeReadOnly,FeYesNoHeader
      ErrFlag=0
      if(FeReadOnly(FileName(:idel(FileName)))) then
        if(Klic.eq.1) then
          NInfo=4
          TextInfo(1)='The file "'//FileName(:idel(FileName))//
     1                '" has attribute READONLY.'
          TextInfo(2)='In case that you will ask to continue with '//
     1                'the structure'
          TextInfo(3)='the program will reset this attribute. '//
     1                'Otherwise program will'
          TextInfo(4)='continue without specified structure.'
          if(FeYesNoHeader(-1.,-1.,'Do you want to continue with the '//
     1                     'structure?',1)) then
           go to 1000
          else
           ErrFlag=1
          endif
        else
          go to 1000
        endif
      endif
      go to 9999
1000  call FeResetReadOnly(FileName(:idel(FileName)))
9999  return
      end
