      function Chebev(a,da,x,n)
      dimension a(n),da(n)
      if(n.le.0) then
        Chebev=0.
        go to 9999
      endif
      d=0.
      dd=0.
      x2=2.*x
      do i=n,2,-1
        sv=d
        d=x2*d-dd+a(i)
        dd=sv
      enddo
      chebev=x*d-dd+a(1)
      do i=n,2,-1
        d=1.
        dd=0.
        do j=i-1,2,-1
          sv=d
          d=x2*d-dd
          dd=sv
        enddo
        da(i)=x*d-dd
      enddo
      da(1)=1.
9999  return
      end
