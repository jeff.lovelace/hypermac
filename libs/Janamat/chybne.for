      subroutine Chybne(text1,text2,tisk,konec)
      include 'fepc.cmn'
      character*128 ven
      character*(*) text1,text2
      integer tisk
      if(konec.eq.1) then
        ven='Fatal -'
      else
        ven='Error -'
      endif
      ven=ven(1:idel(ven))//' '//text1(1:idel(text1))
      n=idel(ven)
      if(tisk.ne.-1.or.konec.eq.1) write(out,FormA1)(ven(i:i),
     1                                            i=1,min(79,n))
      if(tisk.eq.1) then
        call newln(1)
        write(lst,FormA1)(ven(i:i),i=1,n)
      endif
      n=idel(text2)
      if(n.ne.0) then
        if(konec.eq.1) then
          ven='        '//text2(1:idel(text2))
        else
          ven='          '//text2(1:idel(text2))
        endif
        n=idel(ven)
        if(tisk.ne.-1.or.konec.eq.1) write(out,FormA1)(ven(i:i),
     1                                              i=1,min(79,n))
        if(tisk.eq.1) then
          call newln(1)
          write(lst,FormA1)(ven(i:i),i=1,n)
        endif
      endif
      if(konec.eq.1) stop
9999  return
      end
