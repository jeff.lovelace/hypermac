      subroutine GenAnharmCoeff
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(:),im(:),iap(:,:),iar(:,:),iapp(:,:),iapr(:,:),
     1          idlcn(:,:),ipocn(:,:),invn(:),ioffmn(:),ioffvn(:)
      integer Order(:,:),Used(:),Exponent10,FeChDir
      character*80  Veta,Frmt
      character*256 CurrentDirO
      allocatable ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn,invn,
     1            ioffmn,ioffvn
      allocate(ia(6),im(6),iap(6,729),iar(6,729),iapp(729,4),
     1         iapr(729,4),Order(729,4),Used(729),idlcn(28,4),
     2         ipocn(28,4),invn(4),ioffmn(4),ioffvn(4))
      CurrentDirO=CurrentDir
      i=FeChdir('C:\Jana2006\testy')
      call SetIntArrayTo(im,6,3)
      ioffvn(1)=0
      do n=3,6
        n3=3**n
        invn(n-2)=n3
        if(n.lt.6) ioffvn(n-1)=ioffvn(n-2)+n3
        do 1200m=1,n3
          call RecUnpack(m,ia,im,n)
          do i=1,n
            iap(n-i+1,m)=ia(i)
          enddo
          k=0
          do i=1,3
            do j=1,n
              if(iap(j,m).eq.i) then
                k=k+1
                iar(k,m)=i
                if(k.eq.n) go to 1200
              endif
            enddo
          enddo
1150      pause
1200    continue
        do i=1,n3
          ip =0
          ipr=0
          do j=1,n
            ip =ip *4+iap(j,i)
            ipr=ipr*4+iar(j,i)
          enddo
          iapp(i,n-2)=ip
          iapr(i,n-2)=ipr
          Order(i,n-2)=0
          Used(i)=0
        enddo
        ip=0
        do 1500i=1,n3
          if(Used(i).ne.0) go to 1500
          ip=ip+1
          Order(ip,n-2)=i
          Used(i)=1
          do 1400j=i+1,n3
            if(Used(j).ne.0.or.iapr(i,n-2).ne.iapr(j,n-2)) go to 1400
            ip=ip+1
            Order(ip,n-2)=j
            Used(j)=1
1400      continue
1500    continue
      enddo
      ln=NextLogicNumber()
      open(ln,file='Anh_common')
      Veta='      data ipec /'
      m=0
      do n=3,6
        mx=0
        n3=3**n
        do i=1,n3
          mx=max(mx,iapp(i,n-2))
        enddo
        i=Exponent10(float(mx))+1
        if(i.eq.2) then
          Frmt='(18(i2,'',''))'
          k=18
        else if(i.eq.3) then
          Frmt='(13(i3,'',''))'
          k=13
        else if(i.eq.4) then
          Frmt='(11(i4,'',''))'
          k=11
        endif
        do j=1,n3,k
          kk=min(k,n3-j+1)
          write(Veta(18:),Frmt)(iapp(Order(j+i-1,n-2),n-2),i=1,kk)
          if(j+kk-1.ge.n3.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data idlc /'
      Frmt='(18(i2,'',''))'
      k=18
      m=0
      do n=3,6
        n3=3**n
        nn=1
        idlcn(nn,n-2)=0
        ipocn(nn,n-2)=1
        do j=1,n3
          i=Order(j,n-2)
          if(idlcn(nn,n-2).eq.0) then
            iv=iapr(i,n-2)
            idlcn(nn,n-2)=1
          else if(iapr(i,n-2).eq.iv) then
            idlcn(nn,n-2)=idlcn(nn,n-2)+1
          else
            nn=nn+1
            ipocn(nn,n-2)=ipocn(nn-1,n-2)+idlcn(nn-1,n-2)
            idlcn(nn,n-2)=1
            iv=iapr(i,n-2)
          endif
        enddo
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(idlcn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data cmlt /'
      Frmt='(13(f3.0,'',''))'
      k=13
      m=0
      ioffmn(1)=0
      do n=3,6
        nn=TRank(n)
        if(n.lt.6) ioffmn(n-1)=ioffmn(n-2)+nn
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(float(idlcn(j+i-1,n-2)),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data ipoc /'
      m=0
      do n=3,6
        nn=TRank(n)
        if(n.le.4) then
          Frmt='(18(i2,'',''))'
          k=18
        else
          Frmt='(13(i3,'',''))'
          k=13
        endif
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(ipocn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') invn
      call Zhusti(Frmt)
      Veta='      data inv'//Frmt(:idel(Frmt))
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') ioffmn
      call Zhusti(Frmt)
      Veta=Veta(:idel(Veta))//',ioffm'//Frmt(:idel(Frmt))
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') ioffvn
      call Zhusti(Frmt)
      Veta=Veta(:idel(Veta))//',ioffv'//Frmt(:idel(Frmt))//','
      write(ln,FormA) Veta(:idel(Veta))
      call CloseIfOpened(ln)
      i=FeChdir(CurrentDirO)
      deallocate(ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn,invn,
     1           ioffmn,ioffvn)
      return
      end
