      subroutine TestTOF
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension XTOF(-500:500)
      real ITOF(-500:500,10)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      xomn= 9999.
      xomx=-9999.
      yomn= 0.
      yomx=-100.
      be=1.
      al=5.
      sig=.25
      sum1=0.
      sum2=0.
      etaPwd=0.
      sigp=sig**2
      fwhm=sig*sqrt(5.545177)
      do i=-500,500
        XTOF(i)=float(i)*.002
        ITOF(i,1)=ConvTOFWithGauss(XTOF(i),al,be,fwhm,dx,dal,dbe,dsig)

c        ITOF(i,2)=dx
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i)+.01,al,be,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.

c        ITOF(i,2)=dal
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al+.1,be,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*10.

c        ITOF(i,2)=dbe
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al,be+.01,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.

        ITOF(i,2)=dsig
        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al,be,fwhm+.01,dap,dbp,dxp,
     1                              dgp)-ITOF(i,1))*100.


c        ITOF(i,4)=ConvTOFWithGauss(XTOF(i),al,be,sig,dx,dal,dbe,dsig)
c        ITOF(i,2)=ConvTOFWithGauss(XTOF(i),al,be,sig,dx,dal,dbe,dsig)
c        ITOF(i,2)=(ConvTOFWithGauss(XTOF(i),al,be,sig+.01,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.
c        ITOF(i,1)=dsig
        xomn=min(xomn,XTOF(i))
        xomx=max(xomx,XTOF(i))
c        yomn=min(yomn,ITOF(i,1),ITOF(i,2))
c        yomx=max(yomx,ITOF(i,1),ITOF(i,2))
        yomn=min(yomn,ITOF(i,1),ITOF(i,2),ITOF(i,3))
        yomx=max(yomx,ITOF(i,1),ITOF(i,2),ITOF(i,3))
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'TOF')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      do i=1,3
        if(i.eq.1) then
          j=Red
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Magenta
        else if(i.eq.4) then
          j=Khaki
        else if(i.eq.5) then
          j=Blue
        endif
        call FeXYPlot(XTOF(-500),ITOF(-500,i),1001,NormalLine,
     1                  NormalPlotMode,j)
      enddo
      call FeReleaseOutput
      call FeDeferOutput
6000  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 6000
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
