      function FLegen(a,da,x,n)
      include 'fepc.cmn'
      real a(*),da(*)
      if(n.le.0) then
        FLegen=0.
        go to 9999
      endif
      da(1)=1.
      da(2)=x
      FLegen=a(1)+a(2)*x
      do i=3,n
        da(i)=(2.-1./float(i-1))*(da(i-1)*x-da(i-2))+da(i-2)
        FLegen=FLegen+a(i)*da(i)
      enddo
9999  return
      end
