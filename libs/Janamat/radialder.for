      subroutine RadialDer(x,y,z,r,DSto,Type,STOArr)
      dimension STOArr(*),DSto(0:2)
      integer Type
      STOArr(1)=DSto(0)
      if(Type.gt.0) then
        if(r.gt..001) then
          rr=1./r
          V1=rr*DSto(1)
          STOArr(2)=x*V1
          STOArr(3)=y*V1
          STOArr(4)=z*V1
          if(Type.gt.1) then
            V2=(-V1+DSto(2))*rr**2
            STOArr(5)=V1+x**2*V2
            STOArr(6)=V1+y**2*V2
            STOArr(7)=V1+z**2*V2
            STOArr( 8)=x*y*V2
            STOArr( 9)=x*z*V2
            STOArr(10)=y*z*V2
          endif
        else
          call SetRealArrayTo(STOArr(2),3,DSto(1))
          call SetRealArrayTo(STOArr(5),6,DSto(2))
        endif
      endif
      return
      end
