      subroutine smi(a,n,ising)
      dimension a(*),h(:)
      allocatable h
      data diff/.000001/
      allocate(h(n))
      ising=0
      if(n.eq.0) then
        go to 9999
      else if(n.eq.1) then
        if(a(1).ne.0.) then
          a(1)=1./a(1)
        else
          ising=1
        endif
        go to 9999
      endif
      do k=n,1,-1
        p=a(1)
        if(p.lt.diff) then
          ising=n+1-k
          exit
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            a(ij-i)=a(ij)+q*h(ij-m)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          a(m+i)=h(i)
        enddo
      enddo
9999  deallocate(h)
      return
      end
