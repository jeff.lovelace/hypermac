      function SlaterFB(nl,l,sinthl,z,DerZ,DerS,ich)
      include 'fepc.cmn'
      dimension Sla(210),DerZA(190),DerSA(190)
      data sold,zold,nold/2*-9999.,-9999/
      save Sla,DerZA,DerSA
      SlaterFB=0.
      DerZ=0.
      ich=0
      n=nl+2
      s=pi4*sinthl
      if(sold.ne.s.or.z.ne.zold.or.n.gt.nold) then
        call Slater(n,s,z,sla,derza,dersa,ich)
        if(ich.ne.0) go to 9999
      endif
      if(l.lt.0) go to 9000
      i=(n*(n-1))/2+l+1
      SlaterFB=sla(i)
      DerZ=derza(i)
      DerS=dersa(i)
9000  nold=max(n,nold)
      zold=z
      sold=s
9999  return
      end
