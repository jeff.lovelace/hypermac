      subroutine ql(a,z,d,n)
      dimension e(20),d(n),a(*),z(*)
      data tol,eps/.0000001,.0000001/
      imd=1-2*mod(n,2)
      nb=n*(n-1)+1
      k=0
      do i=1,n
        do j=1,n
          k=k+1
          if(i.eq.j) then
            z(k)=1.
          else
            z(k)=0.
          endif
        enddo
      enddo
      iz=n*(n+1)/2
      l=n
      do ip=3,n
        i=l
        l=l-1
        h=0.
        iz=iz-i
        do k=1,l
          ind=iz+k
          f=a(ind)
          d(k)=f
          h=h+f**2
        enddo
        if(h.le.tol) then
          e(i)=0.
          h=0.
          go to 890
        endif
        g=-sqrt(h)
        if(f.lt.0.) g=-g
        e(i)=g
        h=h-f*g
        rh=1./h
        dl=f-g
        d(l)=dl
        ind=iz+l
        a(ind)=dl
        f=0.
        ind=1
        do j=1,l
          g=0.
          jk=ind
          do k=1,l
            g=g+a(jk)*d(k)
            id=1
            if(j.le.k) id=k
            jk=jk+id
          enddo
          g=g*rh
          e(j)=g
          f=f+g*d(j)
          ind=ind+j
        enddo
        hh=f*.5*rh
        jk=0
        do j=1,l
          f=d(j)
          g=e(j)-hh*f
          e(j)=g
          do k=1,j
            jk=jk+1
            a(jk)=a(jk)-f*e(k)-g*d(k)
          enddo
        enddo
890     jk=iz+i
        d(i)=a(jk)
        a(jk)=h
      enddo
      d(1)=a(1)
      e(2)=a(2)
      d(2)=a(3)
      b=0.
      f=0.
      do l=1,n
        ln=l*n-n
        if(l.eq.n) go to 1800
        l1=l+1
        h=eps*(abs(d(l))+abs(e(l1)))
        if(b.lt.h) b=h
        do m=l1,n
          if(abs(e(m)).le.b) go to 1200
        enddo
        m=n+1
1200    m=m-1
        if(m.eq.l) go to 1800
        ml=m-l
        mn=m*n
1250    g=d(l)
        el=e(l1)
        dl=(d(l1)-g)/(2.*el)
        h=abs(dl)+sqrt(dl**2+1.)
        if(dl.lt.0.) h=-h
        dl=el/h
        d(l)=dl
        h=g-dl
        do i=l1,n
          d(i)=d(i)-h
        enddo
        f=f+h
        dl=d(m)
        c=1.
        s=0.
        ind=mn
        do jk=1,ml
          ind=ind-n
          i=m-jk
          i1=i+1
          el=e(i1)
          g=c*el
          h=c*dl
          if(abs(dl).ge.abs(el)) then
            c=el/dl
            r=sqrt(c**2+1.)
            e(i+2)=s*dl*r
            s=c/r
            c=1./r
          else
            c=dl/el
            r=sqrt(c**2+1.)
            e(i+2)=s*el*r
            s=1./r
            c=c*s
          endif
          dl=c*d(i)-s*g
          d(i1 )=h+s*(c*g+s*d(i))
          do k=1,n
            iz=k+ind
            izn=iz-n
            h=z(izn)
            g=z(iz)
            z(izn)=c*h-s*g
            z(iz) =s*h+c*g
          enddo
        enddo
        el=s*dl
        e(l1)=el
        d(l)=c*dl
        if(abs(el).gt.b) go to 1250
1800    dl=d(l)+f
        jk=l-1
        iz=ln
        do k=1,jk
          i=l-k
          iz=iz-n
          if(dl.ge.d(i)) go to 1860
          imd=-imd
          do j=1,n
            ind=iz+j
            inz=ind+n
            p=z(ind)
            z(ind)=z(inz)
            z(inz)=p
          enddo
          d(i+1)=d(i)
        enddo
        i=0
1860    d(i+1)=dl
      enddo
      if(imd.lt.0) then
        do i=1,n
          z(i)=-z(i)
        enddo
      endif
      iz=1
      do i=3,n
        l=i-1
        iz=iz+l
        ind=iz+i
        h=a(ind)
        if(h.ne.0.) then
          do j=1,nb,n
            jk=j-1
            s=0.
            do k=1,l
              m=iz+k
              ind=jk+k
              s=s+a(m)*z(ind)
            enddo
            s=s/h
            do k=1,l
              ind=jk+k
              m=iz+k
              z(ind)=z(ind)-s*a(m)
            enddo
          enddo
        endif
      enddo
      return
      end
