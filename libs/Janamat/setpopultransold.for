      subroutine SetPopulTransOld(Rot,lmax,R)
      include 'fepc.cmn'
      include 'basic.cmn'
      real KroneckerDelta
      dimension rot(3,3),roti(3,3),R(*)
      call matinv(rot,roti,det,3)
      det=sign(1.,det)
      do i=1,3
        do j=1,3
          roti(i,j)=det*rot(i,j)
        enddo
      enddo
      if(abs(roti(3,3)).le..99999) then
        beta=acos(roti(3,3))
        alfa=atan2(roti(3,2), roti(3,1))
        gama=atan2(roti(2,3),-roti(1,3))
      else
        alfa=atan2(roti(1,2),roti(1,1))
        gama=0.
        if(roti(3,3).gt.0.) then
          beta=0.
        else
          beta=pi
        endif
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      ip=1
      k=1
      do l=1,lmax
        mq=0
        n=2*l+1
        do i=1,n
          m=iabs(mq)
          fm=float(m)
          f1=Llm(i+ip)/Clm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          j=1
          do mp=0,l
            fmp=float(mp)
            f2=Clm(j+ip)/Llm(j+ip)
            cp=0.
            kp=max(0,m-mp)
            kk=min(l-mp,l+m)
            if(shbeta.ne.0.) then
              if(chbeta.ne.0.) then
                pom=chbeta**(2*l-2*kp+m-mp)*shbeta**(2*kp+mp-m)
                pomd=(shbeta/chbeta)**2
              else
                if(2*l-2*kk+m-mp.eq.0) then
                  pom=shbeta**(2*kk+mp-m)
                else
                  pom=0.
                endif
                kp=kk
                pomd=0.
              endif
            else
              if(2*kp+mp-m.eq.0) then
                pom=chbeta**(2*l-2*kp+m-mp)
              else
                pom=0.
              endif
              kk=kp
              pomd=0.
            endif
            A=(-1)**kp
            do kk=kp,min(l-mp,l+m)
              cp=cp+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l-mp-kk)*pom
              A=-A
              pom=pom*pomd
            enddo
            cm=0.
            kp=max(0,m+mp)
            A=(-1)**(mp+kp)
            if(shbeta.ne.0.) then
              pom=chbeta**(2*l-2*kp+m+mp)*shbeta**(2*kp-mp-m)
            else
              if(2*kp-mp-m.eq.0) then
                pom=chbeta**(2*l-2*kp+m+mp)
              else
                pom=0.
              endif
              pomd=0.
            endif
            do kk=kp,min(l+mp,l+m)
              cm=cm+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l+mp-kk)*pom
              A=-A
              pom=pom*pomd
            enddo
            A=factorial(l-mp)/factorial(l-m)*f1*f2*
     1                        0.5*(2.-KroneckerDelta(0,mp))
            angp=fm*gama+fmp*alfa
            angm=fm*gama-fmp*alfa
            csp=cos(angp)*cp
            csm=cos(angm)*cm
            snp=sin(angp)*cp
            snm=sin(angm)*cm
            if(mq.ge.0) then
              k=k+1
              R(k)=A*(csp+csm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(snp-snm)
              endif
            else
              k=k+1
              R(k)=-A*(snp+snm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(csp-csm)
              endif
            endif
            j=j+2
          enddo
          mq=-mq
          if(mod(i,2).eq.1) mq=mq+1
        enddo
        ip=ip+n
      enddo
      return
      end
