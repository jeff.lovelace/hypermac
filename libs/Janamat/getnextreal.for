      subroutine GetNextReal(radka,k,array,n,istat)
C istat=0 ... OK; istat=n ... malo slov; -n ... read error
C n je cislo slova, u nehoz nastala chyba
      dimension array(*),irray(*)
      character*10 fmt
      character*80 slovo
      character*(*) radka
      ifl=1
      go to 400
      entry GetNextInteger(radka,k,irray,n,istat)
      fmt='(i80)'
      ifl=0
400   call zhusti(fmt)
      id=idel(radka)
      do i=1,n
        if(k.ge.id) then
          istat=i
          go to 9999
        endif
        call kus(radka,k,slovo)
        call posun(slovo,ifl)
        if(ifl.eq.1) then
          ipom=idel(slovo)-index(slovo,'.')
          if(ipom.lt.10) then
            write(fmt,'(''(f80.'',i1,'')'')') ipom
          else
            write(fmt,'(''(f80.'',i2,'')'')') ipom
          endif
          read(slovo,fmt,err=9100) array(i)
        else
          read(slovo,fmt,err=9100) irray(i)
        endif
      enddo
      istat=0
      go to 9999
9100  istat=-i
9999  return
      end
