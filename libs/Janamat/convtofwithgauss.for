      function ConvTOFWithGauss(x,al,be,fwhm,dx,dal,dbe,dsig)
      include 'fepc.cmn'
      data TwoOverSqrtPi/1.128379/
      sig=fwhm/2.35482
      GaussNorm=1.
      fn=al*be/(2.*(al+be))
      SigOverSqrt2=rsqrt2/sig
      GaussNorm=1./(sqrt(2.*pi)*sig)
      poma=al*sig**2+x
      u=al*.5*(poma+x)
      ConvTOFWithGauss=0.
      if(u.lt.30.) then
        expu=ExpJana(u,ich)
        y=poma*SigOverSqrt2
        erfcny=erfc(y)
        expy=ExpJana(-y**2,ich)
        ConvTOFWithGauss=fn*expu*erfcny
        dx=fn*expu*(erfcny*al-expy*TwoOverSqrtPi*SigOverSqrt2)
        dal=fn*expu*(erfcny*(al*sig**2+x)-
     1                TwoOverSqrtPi*expy*sig*rsqrt2)
     2      +ConvTOFWithGauss*(1./al-1./(al+be))
        dala=ConvTOFWithGauss*(1./al-1./(al+be))
        dbe=ConvTOFWithGauss*(1./be-1./(al+be))
        pomp=x*SigOverSqrt2/sig
        dsig=fn*expu*(erfcny*al**2*sig-
     1                TwoOverSqrtPi*expy*(al*rsqrt2-pomp))
      else
        ConvTOFWithGauss=.5*GaussNorm*ExpJana(-x**2/(2.*sig**2),ich)
        dx=-ConvTOFWithGauss*x/sig**2
        dal=0.
        dbe=0.
        dsig=ConvTOFWithGauss*x**2/sig**3
      endif
      pomb=be*sig**2-x
      v=be*.5*(pomb-x)
      if(v.lt.30.) then
        expv=ExpJana(v,ich)
        z=pomb*SigOverSqrt2
        erfcnz=erfc(z)
        expz=ExpJana(-z**2,ich)
        ConvTOFWithGaussP=fn*expv*erfcnz
        ConvTOFWithGauss=ConvTOFWithGauss+ConvTOFWithGaussP
        dx=dx-fn*expv*(erfcnz*be-expz*TwoOverSqrtPi*SigOverSqrt2)
        dal=dal+ConvTOFWithGaussP*(1./al-1./(al+be))
        dbe=dbe+fn*expv*(erfcnz*(be*sig**2-x)-
     1              TwoOverSqrtPi*expz*sig*rsqrt2)
     2         +ConvTOFWithGaussP*(1./be-1./(al+be))
        pomp=x*SigOverSqrt2/sig
        dsig=dsig+
     1       fn*expv*(erfcnz*be**2*sig-
     3                 TwoOverSqrtPi*expz*(be*rsqrt2+pomp))
      else
        pom=.5*GaussNorm*ExpJana(-x**2/(2.*sig**2),ich)
        ConvTOFWithGauss=ConvTOFWithGauss+pom
        dx=dx-pom*x/sig**2
        dsig=dsig+ConvTOFWithGauss*x**2/sig**3
      endif
      dsig=dsig/2.35482
      return
      end
