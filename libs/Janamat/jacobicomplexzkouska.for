      subroutine JacobiComplexZkouska(AIn,n,D,V,NRot)
      include 'fepc.cmn'
c      include 'basic.cmn'
      complex AIn(n,n),D(n),V(n,n)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,aip,aiq,apj,aqj,
     1        A(:,:),Upp,Uqq,Upq,Uqp,Vip,Viq,
     2        UPom(:,:),UPomC(:,:),ANew(:,:),APom(:,:)
      allocatable A,UPom,UPomC,ANew,APom
      allocate(A(n,n),UPom(n,n),UPomC(n,n),ANew(n,n),APom(n,n))
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
        enddo
        V(p,p)=1.
      enddo
      NRot=0
      do m=1,50
        sm=(0.,0.)
        do p=1,n-1
          do q=p+1,n
            sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..000001) go to 9999
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.0000001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).le.tresh) cycle
            ap=A(q,p)
            bp=A(p,p)-A(q,q)
            cp=-A(p,q)
            BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
            if(abs(BetaK).lt.tresh) cycle

            AlphaK=atan2(aimag(BetaK),real(BetaK))
            ThetaK=atan(abs(BetaK))
            call UnitMatC(UPom,n)
c            write(6,'('' To ma byt jednotkova matice'')')
c            write(6,'(8f10.6)')((UPom(ii,jj),jj=1,n),ii=1,n)
c            write(6,'(2i5,2f10.6)') p,q,ThetaK,AlphaK
            UPom(p,p)=cmplx(cos(ThetaK))
            UPom(q,q)=cmplx(cos(ThetaK))
            UPom(p,q)= cexp(cmplx(0., AlphaK))*cmplx(sin(ThetaK),0.)
            UPom(q,p)=-cexp(cmplx(0.,-AlphaK))*cmplx(sin(ThetaK),0.)
            call MatTransConjg(UPom,UPomC,n)
c            write(6,'('' To ma byt konjugovana matice'')')
cc            write(6,'(8f10.6)')((UPomC(ii,jj),jj=1,n),ii=1,n)
            call MultmC(UPom,A,ANew,n,n,n)
            call MultmC(ANew,UPomC,Apom,n,n,n)
c            write(6,'('' Vysledek1:'')')
c            write(6,'(8f10.6)')((APom(ii,jj),jj=1,n),ii=1,n)
c
c            write(6,'('' To ma byt transformacni matice'')')
c            write(6,'(8f10.6)')((UPom(ii,jj),jj=1,n),ii=1,n)


            app=A(p,p)
            aqq=A(q,q)
            apq=A(p,q)
            aqp=A(q,p)
c            ThetaK=atan(abs(BetaK))
c            BetaK=exp(cmplx(0.,AlphaK))*tan(ThetaK)
c                  write(6,'(2i5,2f10.6)') p,q,ThetaK,AlphaK
            CosThetaK=cos(ThetaK)
            SinThetaK=sin(ThetaK)
            CosTheta2K=CosThetaK**2
            LK=(aqq-app)*cmplx(SinThetaK**2,0.)+
     1         (apq*cexp(-cmplx(0.,AlphaK))+
     2          aqp*cexp( cmplx(0.,AlphaK)))*
     3          cmplx(SinThetaK*CosThetaK,0.)
            A(p,p)=app+LK
            A(q,q)=aqq-LK
            A(p,q)=(apq+(aqq-app)*BetaK
     1             -aqp*BetaK**2)*CosTheta2K
            A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1             -apq*conjg(BetaK)**2)*CosTheta2K
            if(abs(A(p,p)-APom(p,p)).gt..0001) then
              write(6,'('' Rozdil pp:'',4i5,4f10.6)')
     1          p,q,p,p,A(p,p),APom(p,p)
              pause
            endif
            if(abs(A(q,q)-APom(q,q)).gt..0001) then
              write(6,'('' Rozdil qq:'',4i5,4f10.6)')
     1          p,q,q,q,A(q,q),APom(q,q)
              pause
            endif
            if(abs(A(p,q)-APom(p,q)).gt..0001) then
              write(6,'('' Rozdil pq:'',4i5,4f10.6)')
     1          p,q,p,q,A(p,q),APom(p,q)
              pause
            endif
            if(abs(A(q,p)-APom(q,p)).gt..0001) then
              write(6,'('' Rozdil qp:'',4i5,4f10.6)')
     1          p,q,q,p,A(q,p),APom(q,p)
              pause
            endif

c            write(6,'(2f10.6)') A(p,q)
c            write(6,'(2f10.6)') A(q,p)
            do i=1,n
              if(i.eq.p.or.i.eq.q) cycle
              aip=A(i,p)
              aiq=A(i,q)
              A(i,p)=aip*CosThetaK+
     1               aiq*SinThetaK*cexp(cmplx(0.,-AlphaK))
              if(abs(A(i,p)-APom(i,p)).gt..0001) then
                write(6,'('' Rozdil ip:'',4i5,4f10.6)')
     1            p,q,i,p,A(i,p),APom(i,p)
                pause
              endif
              A(i,q)=aiq*CosThetaK-
     1               aip*SinThetaK*cexp(cmplx(0., AlphaK))
              if(abs(A(i,q)-APom(i,q)).gt..0001) then
                write(6,'('' Rozdil iq:'',4i5,4f10.6)')
     1            p,q,i,q,A(i,q),APom(i,q)
                pause
              endif


            enddo
            do j=1,n
              if(j.eq.p.or.j.eq.q) cycle
              apj=A(p,j)
              aqj=A(q,j)
              A(p,j)=apj*CosThetaK+
     1               aqj*SinThetaK*cexp(cmplx(0., AlphaK))
              if(abs(A(p,j)-APom(p,j)).gt..0001) then
                write(6,'('' Rozdil pj:'',4i5,8f10.6)')
     1            p,q,p,j,apj,aqj,A(p,j),APom(p,j)
                pause
              endif
              A(q,j)=aqj*CosThetaK-
     1               apj*SinThetaK*cexp(cmplx(0.,-AlphaK))
              if(abs(A(q,j)-APom(q,j)).gt..0001) then
                write(6,'('' Rozdil qj:'',4i5,8f10.6)')
     1            p,q,q,j,apj,aqj,A(q,j),APom(q,j)
                pause
              endif
            enddo
c            write(6,'('' Vysledek2:'')')
c            write(6,'(8f10.6)')((A(ii,jj),jj=1,n),ii=1,n)
c            pause
            Upp=CosThetaK
            Uqq=CosThetaK
            Upq=-SinThetaK*cexp(cmplx(0., AlphaK))
            Uqp= SinThetaK*cexp(cmplx(0.,-AlphaK))
            do i=1,n
              Vip=V(i,p)
              Viq=V(i,q)
              V(i,p)=Vip*Upp+Viq*Uqp
              V(i,q)=Vip*Upq+Viq*Uqq
            enddo


            NRot=NRot+1
          enddo
        enddo
      enddo
9999  do p=1,n
        D(p)=A(p,p)
      enddo
      deallocate(A,UPom,UPomC,ANew,APom)
      return
      end
