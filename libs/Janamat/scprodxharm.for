      function ScProdXHarm(n,m)
      include 'fepc.cmn'
c      include 'basic.cmn'
      if(n.eq.1.or.m.eq.1) then
        if(n.eq.m) then
          ScProdXHarm=2./3.
        else
          if(n.eq.1) then
            mp=m
            np=m/2
          else
            mp=n
            np=n/2
          endif
          if(mod(mp,2).eq.1) then
            ScProdXHarm=2./(float(np)*pi)
            if(mod(np,2).eq.0) ScProdXHarm=-ScProdXHarm
          else
            ScProdXHarm=0.
          endif
        endif
      else
        if(n.eq.m) then
          ScProdXHarm=1.
        else
          ScProdXHarm=0.
        endif
      endif
      return
      end
