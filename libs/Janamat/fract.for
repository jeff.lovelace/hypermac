      function fract(veta,ich)
      character*(*) veta
      character*15 pomc
      character*5 format
      ich=0
      pomc=veta
      idl=idel(pomc)
      i=index(pomc,'/')
      if(i.le.0) then
        call posun(pomc,1)
        read(pomc,'(f15.0)',err=9000) fract
      else
        format='(i  )'
        if(i.eq.1.or.i.eq.idel(pomc)) then
          fract=0.
          go to 9999
        endif
        write(format(3:4),100) i-1
        read(pomc(1:i-1),format,err=9000) i1
        write(format(3:4),100) idl-i
        read(pomc(i+1:idl),format,err=9000) i2
        fract=float(i1)/float(i2)
      endif
      go to 9999
9000  ich=1
9999  return
100   format(i2)
      end
