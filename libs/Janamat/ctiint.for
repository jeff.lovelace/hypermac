      subroutine ctiint(text,x,dx,a,da,n,iw)
      include 'fepc.cmn'
      character*(*) text
      character*120 format
      character*80 t80
      character*3  fwd
      character*2  nty
      integer x,dx,a(n),da(n)
      if(n.le.0) return
      np=1
      if(iw.gt.0) write(fwd,'(''i'',i2)') iw
      format=text(1:idel(text))
      call uprap(format)
      ifk=idel(format)
1000  if(iw.gt.0) then
        format=format(1:ifk)//' ['','
        do i=np,n-1
          format=format(1:idel(format))//fwd//','','','
        enddo
        format=format(1:idel(format))//fwd//',''] : '
        if(n.eq.1) then
          write(out,'('' '//format(1:idel(format)+1)//''',$)') dx
        else
          write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1          (da(i),i=np,n)
        endif
      else
        format=format(1:ifk)//' : '
        write(out,'('' '//format(1:idel(format)+1)//''',$)')
      endif
      read(dta,FormA) t80
      idelt80=idel(t80)
      if(idelt80.le.0.and.iw.ne.0) then
        if(n.eq.1) then
          x=dx
        else
          do i=np,n
            a(i)=da(i)
          enddo
        endif
      else
        if(idelt80.eq.0) then
          if(n.eq.1) then
            x=999
          else
            do i=np,n
              a(i)=999
            enddo
          endif
          return
        endif
        k=0
        if(n.eq.1) then
          call kus(t80,k,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=1150) x
          return
1150      write(out,102)
          go to 1000
        else
          do i=np,n
            call kus(t80,k,cislo)
            call posun(cislo,0)
            read(cislo,FormI15,err=1250) a(i)
            if(k.eq.80) go to 1260
          enddo
          return
1250      np=i
          write(out,'('' Error : non-numerical character in the '',i1,
     1                a2,''item of this line'')') np,nty(np)
          go to 1000
1260      np=i+1
          if(np.gt.n) return
          write(out,'('' Response not complete continue with the '',i1,
     1                a2,'' item of this line'')') np,nty(np)
          go to 1000
        endif
      endif
102   format(' String contains non-numerical character, try again')
      end
