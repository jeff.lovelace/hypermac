      subroutine MultMRRI(a,b,c,n1,n2,n3,ich)
      dimension a(*),b(*)
      integer c(*)
      ich=0
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          ip=nint(p)
          if(abs(p-float(ip)).gt..001) then
            ich=1
            go to 9999
          else
            c(ik)=ip
          endif
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
9999  return
      end
