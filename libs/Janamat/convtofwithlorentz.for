      function ConvTOFWithLorentz(x,al,be,ga,dx,dal,dbe,dga)
      include 'fepc.cmn'
      complex*16 aa,bb,aap,bbp,e1a,e1b,poma,pomb,expaa,expbb
      fn=-al*be/(pi*(al+be))
      aap= x*(1.,0.)+ga*.5*(0.,1.)
      bbp=-x*(1.,0.)+ga*.5*(0.,1.)
      aa=al*aap
      bb=be*bbp
      if(abs(aa).gt.700..or.abs(bb).gt.700.) then
        ConvTOFWithLorentz=0.
        dx=0.
        dga=0.
        dal=0.
        dbe=0.
        go to 9999
      endif
      call e1z(aa,e1a)
      call e1z(bb,e1b)
      if(al*x.le.0.) then
        expaa=exp(aa)
      else
        expaa=1./exp(-aa)
      endif
      if(be*x.ge.0.) then
        expbb=exp(bb)
      else
        expbb=1./exp(-bb)
      endif
      ConvTOFWithLorentz=fn*aimag(expaa*e1a+expbb*e1b)
      poma=expaa*e1a-1./aa
      pomb=expbb*e1b-1./bb
      dx=fn*(al*aimag(poma)-be*aimag(pomb))
      dga=fn*.5*(al*aimag(poma*(0.,1.))+be*aimag(pomb*(0.,1.)))
      dal=fn*aimag(poma*aap)+ConvTOFWithLorentz*(1./al-1./(al+be))
      dbe=fn*aimag(pomb*bbp)+ConvTOFWithLorentz*(1./be-1./(al+be))
9999  return
      end
