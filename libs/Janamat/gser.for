      subroutine gser(gamser,a,x,gln)
      parameter (itmax=100,eps=3.e-7)
      gln=gammln(a)
      if(x.le.0.) then
        if(x.lt.0.) pause 'x < 0 in gser'
        gamser=0.
        return
      endif
      ap=a
      sum=1./a
      del=sum
      do n=1,itmax
        ap=ap+1.
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*eps) go to 1
      enddo
      pause 'a too large, itmax too small in gser'
1     gamser=sum*exp(-x+a*log(x)-gln)
      return
      end
