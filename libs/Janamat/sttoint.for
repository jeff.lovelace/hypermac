      subroutine StToInt(Veta,k,X,n,Message,ErrFlg)
      include 'fepc.cmn'
      integer X(n)
      character*(*) Veta
      character*80 Polozka
      character*5 FormI
      character*1 t1,tm1
      logical Message
      integer ErrFlg
      data FormI/'(i10)'/
      kin=k
      ErrFlg=0
      call SetIntArrayTo(X,n,0)
      id=idel(Veta)
      if(id.le.0) go to 9900
      nn=0
      i=k
1000  idl=0
      Polozka=' '
      t1=' '
1100  if(idl.eq.0) then
        tm1=' '
      else
        tm1=t1
      endif
      i=i+1
      if(i.gt.id) go to 2000
      t1=Veta(i:i)
      if(ichar(t1).eq.9) t1=' '
      if(t1.eq.' ') then
        if(idl.eq.0) then
          go to 1100
        else
          go to 2000
        endif
      else if(index(Cifry(12:),t1).gt.0.and.idl.gt.0) then
        i=i-1
        go to 2000
      endif
1500  idl=idl+1
      Polozka(idl:idl)=t1
      go to 1100
2000  if(idl.le.0) go to 5000
      nn=nn+1
      write(FormI(3:4),100) idl
      read(Polozka,FormI,err=9000) X(nn)
3000  if(nn.lt.n) go to 1000
5000  k=i
      if(nn.lt.n) go to 9100
      go to 9900
9000  errflg=1
      if(Message) then
        call zhusti(Cislo)
        call FeChybne(-1.,-1.,'illegal integer number - try again',
     1                Cislo,SeriousError)
      endif
      go to 9900
9100  errflg=2+nn
      k=kin
      if(Message) then
        call FeChybne(-1.,-1.,'the string doesn''t contains all '//
     1                'requested integers',Veta(1:idel(Veta)),
     2                SeriousError)
      endif
9900  k=min(len(Veta),k)
      if(k.ge.len(Veta)) go to 9999
9910  if(Veta(k+1:k+1).eq.' ') then
        k=k+1
        if(k.lt.len(Veta)) go to 9910
      endif
9999  return
100   format(i2)
      end
