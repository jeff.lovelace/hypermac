      subroutine vrchol(xnula,xkrok,yjed,ydva,ytri,xpik,ypik)
      a=.5*(ytri-yjed)
      b=2.*ydva-ytri-yjed
      if(b.eq.0.) then
        b=1.
        a=0.
      endif
      b=a/b
      xpik=xnula+xkrok*b
      ypik=ydva+.5*a*b
      return
      end
