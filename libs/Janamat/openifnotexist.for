      subroutine OpenIfNotExist(Ln,File,Formatted)
      include 'fepc.cmn'
      character*80 ch80
      character*(*) File,Formatted
      logical   ExistFile,FeYesNoLong
      if(.not.ExistFile(File)) then
        open(Ln,file=File,form=Formatted)
      else
        open(Ln,file=File,form=Formatted)
        ch80=Formatted
        call mala(ch80)
        if(ch80.eq.'formatted') then
          read(Ln,'(a)',end=500) ch80(1:1)
        else
          read(Ln,end=500) ch80(1:1)
        endif
        rewind Ln
        TextInfo(1)='The file '//File(1:idel(File))//' already exists.'
        TextInfo(2)='Do you want to overwrite it?'
        NInfo=2
        if(.not.FeYesNoLong(-1.,-1.,'Warning',0)) then
          call CloseIfOpened(ln)
          ErrFlag=1
        endif
      endif
 500  return
      end
