      subroutine SmiHandleSing(a,n,NSingArr,NSing)
      dimension a(*),h(:),NSingArr(*)
      allocatable h
      data diff/.000001/
      call SetIntArrayTo(NSingArr,n,0)
      if(n.eq.0) then
        go to 9999
      else if(n.eq.1) then
        if(a(1).ne.0.) then
          a(1)=1./a(1)
        else
          NSing=1
          NSingArr(1)=1
        endif
        go to 9999
      endif
      NSing=0
      allocate(h(n))
      do k=n,1,-1
        p=a(1)
        if(p.lt.diff) then
          NSing=NSing+1
          NSingArr(NSing)=n+1-k
          a(1)=1.
          ij=1
          do i=1,n-1
            ij=ij+i
            a(ij)=0.
          enddo
          p=1.
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            a(ij-i)=a(ij)+q*h(ij-m)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          a(m+i)=h(i)
        enddo
      enddo
      deallocate(h)
9999  return
      end
