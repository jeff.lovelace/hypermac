      subroutine SlaterDensityDer(r,zeta,n,DSto)
      dimension DSto(0:2)
      E=ExpJana(-zeta*r,ich)
      if(n.ne.0) then
        rn=r**n
      else
        rn=1.
      endif
      DSto0=rn*E
      DSto(0)=DSto0
      if(r.gt.0.) then
        fn=float(n)
        rr=1./r
        fnrr=fn*rr
        V=fnrr-zeta
        DSto1=V*DSto0
        DSto(1)=DSto1
        DSto(2)=-fnrr*rr*DSto0+V*DSto1
      else
        if(n.eq.1) then
          DSto(1)=1.
        else if(n.eq.0) then
          DSto(1)=-zeta
        else
          DSto(1)=0.
        endif
        if(n.eq.2) then
          DSto(2)=2.
        else if(n.eq.1) then
          DSto(2)=-2.*zeta
        else if(n.eq.0) then
          DSto(2)=zeta**2
        else
          DSto(2)=0.
        endif
      endif
      return
      end
