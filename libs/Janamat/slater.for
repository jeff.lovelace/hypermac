      subroutine Slater(nmax,s,z,sla,derz,ders,ich)
      include 'fepc.cmn'
      dimension sla(*),derz(*),ders(*)
      ich=0
      if(nmax.lt.1.or.nmax.gt.20) then
        ich=1
        go to 9999
      endif
      nmxp1=nmax+1
      zq=z**2
      dinv=1./(s**2+zq)
      pom1=2.*s*z*dinv
      dinv=zq*dinv
      pom2=2.*dinv
      pom3=pi4*dinv
      sla(1)=pom3
      nn=1
      do 1500n=2,nmxp1
        nn=nn+n
        fnm1 =float(n-1)
        rnm1=1./fnm1
        rn=1./float(n)
        pom=fnm1*pom3*rn
        pom3=pom1*pom
        sla(nn)=pom3
        pom4=pom2*pom
        nnp=nn-1
        sla(nnp)=pom4
        if(n.lt.3) go to 1500
        n1=nn-n
        n2=n1-n+2
        n3=2*n-3
        n4=0
        do l=n-3,0,-1
          nnp=nnp-1
          n1=n1-1
          n2=n2-1
          n3=n3-1
          n4=n4+1
          sla(nnp)=dinv*rn*
     1             (2.*fnm1*sla(n1)-float(n3*n4)*sla(n2)*rnm1)
        enddo
1500  continue
      nn=0
      do n=1,nmax
        pom2=float(n+1)/z
        pom3=pom2*pi4
        nnz=nn+n
        do 2000l=0,n-1
          nn=nn+1
          nnz=nnz+1
          pom=sla(nn)
          if(pom.ne.0.) then
            pom1=1./pom
          else
            ders(nn)=0.
            derz(nn)=0.
            go to 2000
          endif
          pom=-float(l+1)*sla(nnz+1)
          if(n.ne.0) pom=pom+float(l)*sla(nnz-1)
          ders(nn)=pom3*pom1*pom/float(2*l+1)
          derz(nn)=pom2*(1.-sla(nnz)*pom1)
2000    continue
      enddo
9999  return
      end
