      double precision function QHester(x,y)
      double precision :: pom1,csy,sny,csx,snx,
     1                    dpid4=0.78539816339744825d0
      csy=dcos(dble(y))
      csx=dcos(dble(x))
      snx=dsin(dble(x))
      pom1=(csx/csy)**2
      if(pom1.lt.1.d0) pom1=1.d0
      pom1=dsqrt(pom1-1.)
      QHester=.5d0*datan2((-pom1**2+snx**2),2.*pom1*snx)+dpid4
      return
      end
