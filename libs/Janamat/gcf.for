      subroutine gcf(gammcf,a,x,gln)
      parameter (itmax=100,eps=3.e-7,fpmin=1.e-30)
      gln=gammln(a)
      b=x+1.-a
      c=1./fpmin
      d=1./b
      h=d
      do  i=1,itmax
        an=-i*(i-a)
        b=b+2.
        d=an*d+b
        if(abs(d).lt.fpmin) d=fpmin
        c=b+an/c
        if(abs(c).lt.fpmin) c=fpmin
        d=1./d
        del=d*c
        h=h*del
        if(abs(del-1.).lt.eps) go to 1
      enddo
      pause 'a too large, itmax too small in gcf'
1     gammcf=exp(-x+a*log(x)-gln)*h
      return
      end
