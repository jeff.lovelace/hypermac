      subroutine SlaterNorm(nx,z,xn)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension nx(8),z(8),xn(8)
      do l=1,8
        n=nx(l)+2
        zl=z(l)/BohrRad
        rn=zl
        do k=1,n
          rn=rn*zl/float(k)
        enddo
        xn(l)=rn
        z(l)=zl
      enddo
      return
      end
