      function csprod(n,m,x1,x2,natp)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension fc(3),xs(6)
      if(KCommen(KPhase).ge.0.or.natp.le.0) then
        if(n.le.m) then
          nl=n
          ml=m
        else
          nl=m
          ml=n
        endif
        if(nl.gt.1) then
          i=isign(iabs(nl)/2,nl)
          a=pi2*float(i)
        endif
        if(ml.gt.1) then
          i=isign(iabs(ml)/2,nl)
          b=pi2*float(i)
        endif
        i=isign(iabs(nl)/2,nl)
        knl=mod(nl,2)
        kml=mod(ml,2)
        if(nl.eq.1) then
          if(ml.eq.1) then
            csprod=x2-x1
          else if(kml.eq.0) then
            csprod=-1./b*(cosnas(b*x2)-cosnas(b*x1))
          else
            csprod=1./b*(sinnas(b*x2)-sinnas(b*x1))
          endif
        else if(knl.eq.0.and.kml.eq.0) then
          if(a.eq.b) then
            csprod=.5*(x2-x1)-.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else if(a.eq.-b) then
            csprod=.5*(x1-x2)+.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else
            amb=a-b
            apb=a+b
            csprod=.5/amb*(sinnas(amb*x2)-sinnas(amb*x1))-
     1             .5/apb*(sinnas(apb*x2)-sinnas(apb*x1))
          endif
        else if(knl.eq.1.and.kml.eq.1) then
          if(abs(a).eq.abs(b)) then
            csprod=.5*(x2-x1)+.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else
            amb=a-b
            apb=a+b
            csprod=.5/amb*(sinnas(amb*x2)-sinnas(amb*x1))+
     1             .5/apb*(sinnas(apb*x2)-sinnas(apb*x1))
          endif
        else
          if(knl.eq.1) then
            pom=a
            a=b
            b=pom
          endif
          if(abs(a).eq.abs(b)) then
            csprod=.5/a*(sinnas(a*x2)**2-sinnas(a*x1)**2)
          else
            amb=a-b
            apb=a+b
            csprod=-.5/amb*(cosnas(amb*x2)-cosnas(amb*x1))-
     1              .5/apb*(cosnas(apb*x2)-cosnas(apb*x1))
          endif
        endif
      else
        isw=iswa(natp)
        if(n.gt.1) a=pi2*float(n/2)
        if(m.gt.1) b=pi2*float(m/2)
        kn=mod(n,2)
        km=mod(m,2)
        suma=0.
        do ic3=0,NCommQ(3,isw,KPhase)-1
          fc(3)=ic3
          do ic2=0,NCommQ(2,isw,KPhase)-1
            fc(2)=ic2
            do 4100ic1=0,NCommQ(1,isw,KPhase)-1
              fc(1)=ic1
              pom=ic1
              do j=1,3
                fc(j)=VCommQ(j,1,isw,KPhase)*pom
              enddo
              do j=1,3
                xs(j)=x(j,natp)+fc(j)
              enddo
              call qbyx(xs,xs(4),isw)
              xs(4)=trez(1,isw,KPhase)+xs(4)
              argl=xs(4)
2010          if(argl.le.x2) go to 2020
              argl=argl-1.
              go to 2010
2020          if(argl.ge.x1) go to 2030
              argl=argl+1.
              go to 2020
2030          if(argl.gt.x2) go to 4100
              if(n.ne.1) then
                if(kn.eq.1) then
                  pom=cosnas(a*argl)
                else
                  pom=sinnas(a*argl)
                endif
              else
                pom=1.
              endif
              if(m.ne.1) then
                if(km.eq.1) then
                  pom=pom*cosnas(b*argl)
                else
                  pom=pom*sinnas(b*argl)
                endif
              endif
              suma=suma+pom
4100        continue
          enddo
        enddo
        csprod=suma
      endif
      return
      end
