      subroutine MatTransConjg(A,B,n)
      complex A(n,n),B(n,n)
      do i=1,n
        do j=1,n
          B(j,i)=conjg(A(i,j))
        enddo
      enddo
      return
      end
