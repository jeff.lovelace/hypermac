      subroutine HeapI(n,ra,sgn)
      integer ra(n),rra,sgn
      if(n.le.1) go to 9999
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(1)=rra
          go to 9999
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if((sgn.lt.0.and.ra(j).gt.ra(j+1)).or.
     1       (sgn.gt.0.and.ra(j).lt.ra(j+1))) j=j+1
        endif
        if((sgn.lt.0.and.rra.gt.ra(j)).or.
     1     (sgn.gt.0.and.rra.lt.ra(j))) then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      ra(i)=rra
      go to 10
9999  return
      end
