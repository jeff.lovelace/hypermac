      logical function eqrvlt0(a,n)
      dimension a(n)
      n0=0
      do i=1,n
        if(a(i).gt.0.) then
          eqrvlt0=.false.
          go to 9999
        else if(a(i).eq.0.) then
          n0=n0+1
        endif
      enddo
      eqrvlt0=n0.lt.n
9999  return
      end
