      subroutine JacobiComplexOld(AIn,n,D,V,NRot)
      complex AIn(n,n),D(n),V(n,n)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,apj,aip,aqj,aiq,
     1        A(:,:),U(:,:),VP(:,:)
      allocatable A,U,VP
      allocate(A(n,n),U(n,n),VP(n,n))
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
        enddo
        V(p,p)=1.
      enddo
      NRot=0
      do m=1,50
        sm=(0.,0.)
        do p=1,n-1
          do q=p+1,n
            sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..00001) go to 9999
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.00001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).gt.tresh) then
              ap=A(q,p)
              bp=A(p,p)-A(q,q)
              cp=-A(p,q)
              BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
              ThetaK=atan(abs(BetaK))
              AlphaK=atan2(aimag(BetaK),real(BetaK))
              CosThetaK=cos(ThetaK)
              SinThetaK=sin(ThetaK)
              CosTheta2K=CosThetaK**2
              LK=(A(q,q)-A(p,p))*cmplx(SinThetaK**2,0.)+
     1           (A(p,q)*exp(-cmplx(0.,AlphaK))+
     2            A(q,p)*exp( cmplx(0.,AlphaK)))*
     3            cmplx(SinThetaK*CosThetaK,0.)
              app=A(p,p)
              aqq=A(p,p)
              apq=A(p,q)
              aqp=A(q,p)
              do i=1,n
                aip=A(i,p)
                aiq=A(i,q)
                do j=1,n
                  apj=A(p,j)
                  aqj=A(q,j)
                  if(i.eq.p.and.j.eq.p) then
                    A(p,p)=app+LK
                  else if(i.eq.q.and.j.eq.q) then
                    A(q,q)=aqq-LK
                  else if(i.eq.p.and.j.eq.q) then
                    A(p,q)=(apq+(aqq-app)*BetaK
     1                         -aqp*BetaK**2)*CosTheta2K
                  else if(i.eq.q.and.j.eq.p) then
                    A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1                         -apq*conjg(BetaK)**2)*CosTheta2K
                  else if(i.eq.p) then
                    A(p,j)=apj*CosThetaK+
     1                     aqj*SinThetaK*exp(cmplx(0., AlphaK))
                  else if(j.eq.p) then
                    A(i,p)=aip*CosThetaK+
     1                     aiq*SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(i.eq.q) then
                    A(q,j)=aqj*CosThetaK-
     1                     apj*SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(j.eq.q) then
                    A(i,q)=aiq*CosThetaK-
     1                     aip*SinThetaK*exp(cmplx(0., AlphaK))
                  endif
                enddo
              enddo
              do i=1,n
                do j=1,n
                  if((i.eq.p.and.j.eq.p).or.(i.eq.q.and.j.eq.q))
     1              then
                    U(i,j)=CosThetaK
                  else if(i.eq.p.and.j.eq.q) then
                    U(i,j)=-SinThetaK*exp(cmplx(0., AlphaK))
                  else if(i.eq.q.and.j.eq.p) then
                    U(i,j)= SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(i.eq.j) then
                    U(i,j)=1.
                  else
                    U(i,j)=0.
                  endif
                enddo
              enddo
              call CopyVekC(V,VP,n**2)
              call MultMC(VP,U,V,n,n,n)
              NRot=NRot+1
            endif
          enddo
        enddo
      enddo
9999  do p=1,n
        D(p)=A(p,p)
      enddo
      deallocate(A,U,VP)
      return
      end
