      logical function EqWild(Veta,Hledany,CaseSensitive)
      character*(*) Veta,Hledany
      logical Hvezda,CaseSensitive
      idlh=idel(Hledany)
      idlv=idel(Veta)
      EqWild=.true.
      ik=0
      jk=0
1000  Hvezda=.false.
      ip=ik+1
      jp=jk+1
1100  if(Hledany(ip:ip).eq.'*') then
        Hvezda=.true.
      else if(Hledany(ip:ip).eq.'?') then
        if(jp.lt.idlv) then
          jp=jp+1
        else if(jp.eq.idlv.and.ip.eq.idlh) then
          go to 9000
        else
          go to 8000
        endif
      else
        go to 1200
      endif
      if(ip.ge.idlh) then
        if(Hvezda) then
          go to 9000
        else
          go to 8000
        endif
      else
        ip=ip+1
        go to 1100
      endif
1200  ik=ip
1500  if(Hledany(ik:ik).ne.'*'.and.Hledany(ik:ik).ne.'?') then
        if(ik.lt.idlh) then
          ik=ik+1
          go to 1500
        else
          go to 2000
        endif
      endif
      ik=ik-1
2000  idlp=ik-ip
      if(Hvezda) then
        idlpp=idlp
        do i=ik+1,idlh
          if(Hledany(i:i).ne.'*') then
            idlpp=idlpp+1
          else
            go to 2200
          endif
        enddo
        jp=idlv-idlpp
      endif
2200  if(jp.le.0) go to 8000
      i=LocateSubstring(Veta(jp:),Hledany(ip:ik),CaseSensitive,.true.)-1
      if(i.lt.0.or.(.not.Hvezda.and.i.gt.0)) go to 8000
      jk=jp+idlp+i
      if(ik.lt.idlh) then
        go to 1000
      else
        if(jk.lt.idlv) then
          go to 8000
        else
          go to 9000
        endif
      endif
8000  EqWild=.false.
9000  return
      end
