      subroutine bessi(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      dimension besp(*)
      double precision bi,bim,bip
      if(n.lt.0) go to 9999
      if(abs(x).lt.bigni) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        besp(1)=bessi0(x)
        bi=bessi1(x)
        if(n.ge.1) then
          besp(2)=bi
        else
          besp1=bi
          go to 9999
        endif
        tox=2./abs(x)
        m=2*(n+1+int(sqrt(float(iacc*(n+1)))))
        do i=3,n+1
          besp(i)=0.
        enddo
        besp1=0.
        bip=0.
        bi=1.
        do j=m,1,-1
          bim=bip+j*tox*bi
          bip=bi
          bi=bim
          if(abs(bi).gt.bigno) then
            bi=bi*bigni
            bip=bip*bigni
            do i=3,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
          endif
          if(j.le.n.and.j.ge.2) then
            besp(j+1)=bip
          else if(j.eq.n+1) then
            besp1=bip
          endif
        enddo
        bi=besp(1)/bi
        do i=3,n+1
          besp(i)=besp(i)*bi
        enddo
        besp1=besp1*bi
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
