      subroutine GetFPol(x,FPol,NPol,Type)
      integer Type
      real x,FPol(NPol),XMat(:,:)
      integer :: NAlloc = 0
      allocatable XMat
      save XMat,NAlloc
      if(Type.eq.2) then
        call FLeg(x,FPol,NPol)
      else if(Type.eq.3) then
        FPol(1)=1.
        if(NPol-1.gt.NAlloc) then
          if(allocated(XMat)) deallocate(XMat)
          allocate(XMat(NPol-1,NPol-1))
          NAlloc=NPol-1
          call MakeXHarmOrtho(XMat,NPol-1)
        endif
        do i=2,NPol
          FPol(i)=XHarmOrtho(x,i-1,XMat,NAlloc)
        enddo
      endif
      return
      end
