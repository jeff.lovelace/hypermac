      subroutine SpherHarm(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),f((n+1)*(n+1)),xyz(3)
      equivalence (xyz(1),x),(xyz(2),y),(xyz(3),z)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.lt..001) then
        call SetRealArrayTo(f,(n+1)*(n+1),0.)
        go to 9999
      endif
      if(n.ge.0) f(1)=1.
      if(n.ge.1) then
        call CopyVek(h,xyz,3)
        call VecOrtNorm(xyz,3)
        f(2)=z
        f(3)=x
        f(4)=y
      endif
      if(n.ge.2) then
        x2=x**2
        y2=y**2
        z2=z**2
        xy=x*y
        xz=x*z
        yz=y*z
        f(5)=3.*z2-1.
        f(6)=xz
        f(7)=yz
        f(8)=(x2-y2)*.5
        f(9)=xy
      endif
      if(n.ge.3) then
        x3=x2*x
        y3=y2*y
        z3=z2*z
        f(10)=5.*z3-3.*z
        pom=5.*z2-1.
        f(11)=pom*x
        f(12)=pom*y
        f(13)=2.*z*f(8)
        f(14)=2.*z*f(9)
        f(15)= x3-3.*xy*y
        f(16)=-y3+3.*xy*x
      endif
      if(n.ge.4) then
        x4=x3*x
        y4=y3*y
        z4=z3*z
        f(17)=35.*z4-30.*z2+3.
        pom=7.*z3-3.*z
        f(18)=pom*x
        f(19)=pom*y
        pom=14.*z2-2.
        f(20)=pom*f(8)
        f(21)=pom*f(9)
        f(22)=z*f(15)
        f(23)=z*f(16)
        f(24)=x4-6.*x2*y2+y4
        f(25)=4.*(x3*y-x*y3)
      endif
      if(n.ge.5) then
        x5=x4*x
        y5=y4*y
        z5=z4*z
        f(26)=63.*z5-70.*z3+15.*z
        pom=(21.*z4-14.*z2+1.)
        f(27)=pom*x
        f(28)=pom*y
        pom=6.*z3-2.*z
        f(29)=pom*f(8)
        f(30)=pom*f(9)
        pom=(9.*z2-1)
        f(31)=pom*f(15)
        f(32)=pom*f(16)
        f(33)=z*f(24)
        f(34)=z*f(25)
        f(35)=x5-10.*x3*y2+5.*x*y4
        f(36)=5.*x4*y-10.*x2*y3+y5
      endif
      if(n.ge.6) then
        x6=x5*x
        y6=y5*y
        z6=z5*z
        f(37)=231.*z6-315.*z4+105.*z2-5
        pom=33.*z5-30.*z3+5.*z
        f(38)=pom*x
        f(39)=pom*y
        pom=66.*z4-36.*z2+2.
        f(40)=pom*f(8)
        f(41)=pom*f(9)
        pom=11.*z3-3.*z
        f(42)=pom*f(15)
        f(43)=pom*f(16)
        pom=11.*z2-1.
        f(44)=pom*f(24)
        f(45)=pom*f(25)
        f(46)=z*f(35)
        f(47)=z*f(36)
        f(48)=x6-15.*x4*y2+15.*x2*y4-y6
        f(49)=6.*x5*y-20.*x3*y3+6.*x*y5
      endif
      if(n.ge.7) then
        x7=x6*x
        y7=y6*y
        z7=z6*z
        f(50)=429.*z7-693.*z5+315.*z3-35.*z
        pom=429.*z6-495.*z4+135.*z2-5.
        f(51)=pom*x
        f(52)=pom*y
        pom=286.*z5-220.*z3+30.*z
        f(53)=pom*f(8)
        f(54)=pom*f(9)
        pom=143.*z4-66.*z2+3.
        f(55)=pom*f(15)
        f(56)=pom*f(16)
        pom=13.*z3-3.*z
        f(57)=pom*f(24)
        f(58)=pom*f(25)
        pom=13.*z2-1.
        f(59)=pom*f(35)
        f(60)=pom*f(36)
        f(61)=z*f(48)
        f(62)=z*f(49)
        f(63)=x7-21.*x5*y2+35.*x3*y4-7.*x*y6
        f(64)=7.*x6*y-35.*x4*y3+21.*x2*y5-y7
      endif
      if(n.ge.8) then
        x8=x7*x
        y8=y7*y
        z8=z7*z
        f(65)=6435.*z8-12012.*z6+6930.*z4-1260.*z2+35.
        pom=715.*z7-1001.*z5+385.*z3-35.*z
        f(66)=pom*x
        f(67)=pom*y
        pom=286.*z6-286.*z4+66.*z2-2.
        f(68)=pom*f(8)
        f(69)=pom*f(9)
        pom=39.*z5-26.*z3+3.*z
        f(70)=pom*f(15)
        f(71)=pom*f(16)
        pom=65.*z4-26.*z2+1.
        f(72)=pom*f(24)
        f(73)=pom*f(25)
        pom=5.*z3-z
        f(74)=pom*f(35)
        f(75)=pom*f(36)
        pom=15.*z2-1.
        f(76)=pom*f(48)
        f(77)=pom*f(49)
        pom=z
        f(78)=pom*f(63)
        f(79)=pom*f(64)
        f(80)=x8-28.*x6*y2+70.*x4*y4-28.*x2*y6+y8
        f(81)=8.*x7*y-56.*x5*y3+56.*x3*y5-8.*x*y7
      endif
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          f(i)=f(i)*Clm(i)
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          f(i)=f(i)*Mlm(i)
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          f(i)=f(i)*Llm(i)
        enddo
      endif
9999  return
      end
