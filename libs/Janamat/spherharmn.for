      subroutine SpherHarmN(h,f,fd1,fd2,lmax,Type,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Type
      real h(3),f(*),fd1(3,*),fd2(6,*)
      dimension fm(-7:7),fmd1(2,-7:7),fmd2(3,-7:7)
      XX=h(1)
      YY=h(2)
      ZZ=h(3)
      r=XX**2+YY**2+ZZ**2
      lmax1q=(lmax+1)**2
      if(r.lt..00001) then
        call SetRealArrayTo(f,lmax1q,0.)
        if(Type.gt.0) then
          call SetRealArrayTo(fd1,3*lmax1q,0.)
          if(Type.gt.0) then
             call SetRealArrayTo(fd2,6*lmax1q,0.)
          endif
        endif
        go to 9999
      endif
      r=1./r
      pom=sqrt(r)
      x=XX*pom
      y=YY*pom
      z=ZZ*pom
      if(Type.gt.0) then
        pom3=pom*r
        XXYYQ=XX**2+YY**2
        XXZZQ=XX**2+ZZ**2
        YYZZQ=YY**2+ZZ**2
        XXpom3=XX*pom3
        YYpom3=YY*pom3
        ZZpom3=ZZ*pom3
        DxDh=YYZZQ*pom3
        DxDk=-XX*YYpom3
        DxDl=-XX*ZZpom3
        DyDh=DxDk
        DyDk=XXZZQ*pom3
        DyDl=-YY*ZZpom3
        DzDh=DxDl
        DzDk=DyDl
        DzDl=XXYYQ*pom3
        if(Type.gt.1) then
          pom5=pom3*r
          XXpom5=XX*pom5
          YYpom5=YY*pom5
          ZZpom5=ZZ*pom5
          D2xDh2=-3.*YYZZQ*XXpom5
          D2xDk2=-XXpom3*(1.-3.*YY**2*r)
          D2xDl2=-XXpom3*(1.-3.*ZZ**2*R)
          D2xDhDk=YYpom3*(2.-3.*YYZZQ*r)
          D2xDhDl=ZZpom3*(2.-3.*YYZZQ*r)
          D2xDkDl=3.*XX*YY*ZZpom5
          D2yDh2=-YYpom3*(1.-3.*XX**2*r)
          D2yDk2=-3.*XXZZQ*YYpom5
          D2yDl2=-YYpom3*(1.-3.*ZZ**2*r)
          D2yDhDk=D2xDk2
          D2yDhDl=D2xDkDl
          D2yDkDl=ZZpom3*(2.-3.*XXZZQ*r)
          D2zDh2=-ZZpom3*(1.-3.*XX**2*r)
          D2zDk2=-ZZpom3*(1.-3.*YY**2*r)
          D2zDl2=-3.*XXYYQ*ZZpom5
          D2zDhDk=D2xDkDl
          D2zDhDl=D2xDl2
          D2zDkDl=D2yDl2
        endif
      endif
      Pmm=1.
      f(1)=Pmm
      Fmm=0.
      fd1(3,1)=0.
      Smm=0.
      fd2(3,1)=0.
      pom=max((1.-z)*(1.+z),.000001)
      arg=sqrt(pom)
      sarg1=-1./pom
      farg=z*sarg1
      sarg2=farg**2
      mm=0
      n=-1
      do m=0,lmax
        n=n+2
        mm=mm+n
        if(m.eq.1) mm=mm-1
        if(m.eq.0) then
          Pmmp=1.
          if(Type.gt.0) then
            Fmmp=0.
            if(Type.gt.1) Smmp=0.
          endif
          fact1=1.
        else
          Pmmp=-Pmmp*fact1*arg
          Pmm=Pmmp
          f(mm)=Pmm
          if(Type.gt.0) then
            Fmmp=Pmmp*float(m)*farg
            Fmm=Fmmp
            fd1(3,mm)=Fmmp
            if(Type.gt.1) then
              Smmp=Pmmp*float(m)*(sarg1+float(m-2)*sarg2)
              Smm=Smmp
              fd2(3,mm)=Smmp
            endif
          endif
          fact1=fact1+2.
        endif
        np=n
        mp=mm
        if(m.le.lmax-1) then
          mp=mp+np
          pom=z*fact1
          Pmm1=pom*Pmm
          f(mp)=Pmm1
          if(Type.gt.0) then
            Fmm1=pom*Fmm+fact1*Pmm
            fd1(3,mp)=Fmm1
            if(Type.gt.1) then
              Smm1=pom*Smm+2.*fact1*Fmm
              fd2(3,mp)=Smm1
            endif
          endif
          np=np+2
        endif
        fact2=fact1+2.
        fact3=fact1
        fact4=2.
        do m2=m+2,lmax
          mp=mp+np
          rfact4=1./fact4
          pom=z*fact2
          Pmm2=(pom*Pmm1-fact3*Pmm)*rfact4
          f(mp)=Pmm2
          if(Type.gt.0) then
            Fmm2=(pom*Fmm1+fact2*Pmm1-fact3*Fmm)*rfact4
            fd1(3,mp)=Fmm2
            if(Type.gt.1) then
              Smm2=(pom*Smm1+2.*fact2*Fmm1-fact3*Smm)*rfact4
              fd2(3,mp)=Smm2
            endif
          endif
          Pmm=Pmm1
          Pmm1=Pmm2
          if(Type.gt.0) then
            Fmm=Fmm1
            Fmm1=Fmm2
            if(Type.gt.1) then
              Smm=Smm1
              Smm1=Smm2
            endif
          endif
          fact2=fact2+2.
          fact3=fact3+1.
          fact4=fact4+1.
          np=np+2
        enddo
      enddo
      fm(0)=1.
      fmd1(1,0)=0.
      fmd1(2,0)=0.
      fmd2(1,0)=0.
      fmd2(2,0)=0.
      fmd2(3,0)=0.
      r=x**2+y**2
      if(r.lt..00001) then
        do l=-lmax,lmax
          fm(l)=0.
          fmd1(1,l)=0.
          fmd1(2,l)=0.
          fmd2(1,l)=0.
          fmd2(2,l)=0.
          fmd2(3,l)=0.
        enddo
        fm(0)=1.
        go to 3500
      endif
      r=1./r
      pom=sqrt(r)
      pom3=pom*r
      sfi=y*pom
      cfi=x*pom
      if(Type.gt.0) then
        DsfiDx=-x*y*pom3
        DsfiDy= x**2*pom3
        DcfiDx= y**2*pom3
        DcfiDy=DsfiDx
        if(Type.gt.1) then
          pom5=pom3*r
          D2sfiDx2=-y*pom3*(1.-3.*x**2*r)
          D2sfiDy2=-3.*x**2*y*pom5
          D2sfiDxDy=-x*pom3*(1.-3.*y**2*r)
          D2cfiDx2=-3.*x*y**2*pom5
          D2cfiDy2=D2sfiDxDy
          D2cfiDxDy=D2sfiDx2
        endif
      endif
      cfin1=1.
      sfin1=0.
      Dcfin1Dcfi=0.
      Dcfin1Dsfi=0.
      Dsfin1Dcfi=0.
      Dsfin1Dsfi=0.
      D2cfin1Dcfi2=0.
      D2cfin1Dsfi2=0.
      D2cfin1DsfiDcfi=0.
      D2sfin1Dcfi2=0.
      D2sfin1Dsfi2=0.
      D2sfin1DsfiDcfi=0.
      do i=1,lmax
        cfin=cfin1*cfi-sfin1*sfi
        sfin=sfin1*cfi+cfin1*sfi
        fm( i)= cfin
        fm(-i)= sfin
        if(Type.gt.0) then
          DcfinDcfi=Dcfin1Dcfi*cfi-Dsfin1Dcfi*sfi+cfin1
          DcfinDsfi=Dcfin1Dsfi*cfi-Dsfin1Dsfi*sfi-sfin1
          DsfinDcfi=Dsfin1Dcfi*cfi+Dcfin1Dcfi*sfi+sfin1
          DsfinDsfi=Dsfin1Dsfi*cfi+Dcfin1Dsfi*sfi+cfin1
          fmd1(1, i)= DcfinDcfi*DcfiDx+DcfinDsfi*DsfiDx
          fmd1(1,-i)= DsfinDcfi*DcfiDx+DsfinDsfi*DsfiDx
          fmd1(2, i)= DcfinDcfi*DcfiDy+DcfinDsfi*DsfiDy
          fmd1(2,-i)= DsfinDcfi*DcfiDy+DsfinDsfi*DsfiDy
          if(Type.gt.1) then
            D2cfinDcfi2=D2cfin1Dcfi2*cfi+2.*Dcfin1Dcfi-D2sfin1Dcfi2*sfi
            D2cfinDsfi2=D2cfin1Dsfi2*cfi-2.*Dsfin1Dsfi-D2sfin1Dsfi2*sfi
            D2cfinDsfiDcfi=D2cfin1DsfiDcfi*cfi-D2sfin1DsfiDcfi*sfi
     1                    -Dsfin1Dcfi+Dcfin1Dsfi
            D2sfinDcfi2=D2sfin1Dcfi2*cfi+2.*Dsfin1Dcfi+D2cfin1Dcfi2*sfi
            D2sfinDsfi2=D2sfin1Dsfi2*cfi+2.*Dcfin1Dsfi+D2cfin1Dsfi2*sfi
            D2sfinDsfiDcfi=D2sfin1DsfiDcfi*cfi+D2cfin1DsfiDcfi*sfi
     1                    +Dcfin1Dcfi+Dsfin1Dsfi
            fmd2(1, i)= DcfinDcfi*D2cfiDx2+DcfinDsfi*D2sfiDx2
     1                 +D2cfinDcfi2*DcfiDx**2+D2cfinDsfi2*DsfiDx**2
     2                 +2.*D2cfinDsfiDcfi*DcfiDx*DsfiDx
            fmd2(1,-i)= DsfinDcfi*D2cfiDx2+DsfinDsfi*D2sfiDx2
     1                 +D2sfinDcfi2*DcfiDx**2+D2sfinDsfi2*DsfiDx**2
     2                 +2.*D2sfinDsfiDcfi*DcfiDx*DsfiDx
            fmd2(2, i)= DcfinDcfi*D2cfiDy2+DcfinDsfi*D2sfiDy2
     1                 +D2cfinDcfi2*DcfiDy**2+D2cfinDsfi2*DsfiDy**2
     2                 +2.*D2cfinDsfiDcfi*DcfiDy*DsfiDy
            fmd2(2,-i)= DsfinDcfi*D2cfiDy2+DsfinDsfi*D2sfiDy2
     1                 +D2sfinDcfi2*DcfiDy**2+D2sfinDsfi2*DsfiDy**2
     2                 +2.*D2sfinDsfiDcfi*DcfiDy*DsfiDy
            fmd2(3, i)= DcfinDcfi*D2cfiDxDy+DcfinDsfi*D2sfiDxDy
     1                 +D2cfinDcfi2*DcfiDx*DcfiDy
     2                 +D2cfinDsfi2*DsfiDx*DsfiDy
     3                 +D2cfinDsfiDcfi*(DcfiDx*DsfiDy+DcfiDy*DsfiDx)
            fmd2(3,-i)= DsfinDcfi*D2cfiDxDy+DsfinDsfi*D2sfiDxDy
     1                 +D2sfinDcfi2*DcfiDx*DcfiDy
     2                 +D2sfinDsfi2*DsfiDx*DsfiDy
     3                 +D2sfinDsfiDcfi*(DcfiDx*DsfiDy+DcfiDy*DsfiDx)
          endif
        endif
        if(mod(i,2).eq.1) then
          fm( i)=-fm( i)
          fm(-i)=-fm(-i)
          if(Type.gt.0) then
            fmd1(1, i)=-fmd1(1, i)
            fmd1(1,-i)=-fmd1(1,-i)
            fmd1(2, i)=-fmd1(2, i)
            fmd1(2,-i)=-fmd1(2,-i)
            if(Type.gt.1) then
              fmd2(1, i)=-fmd2(1, i)
              fmd2(1,-i)=-fmd2(1,-i)
              fmd2(2, i)=-fmd2(2, i)
              fmd2(2,-i)=-fmd2(2,-i)
              fmd2(3, i)=-fmd2(3, i)
              fmd2(3,-i)=-fmd2(3,-i)
            endif
          endif
        endif
        sfin1=sfin
        cfin1=cfin
        if(Type.gt.0) then
          Dsfin1Dcfi=DsfinDcfi
          Dsfin1Dsfi=DsfinDsfi
          Dcfin1Dcfi=DcfinDcfi
          Dcfin1Dsfi=DcfinDsfi
          if(Type.gt.1) then
            D2sfin1Dcfi2=D2sfinDcfi2
            D2sfin1Dsfi2=D2sfinDsfi2
            D2sfin1DsfiDcfi=D2sfinDsfiDcfi
            D2cfin1Dcfi2=D2cfinDcfi2
            D2cfin1Dsfi2=D2cfinDsfi2
            D2cfin1DsfiDcfi=D2cfinDsfiDcfi
          endif
        endif
      enddo
3500  k=0
      do l=0,lmax
        do n=1,2*l+1
          k=k+1
          if(n.eq.1.or.mod(n,2).eq.0) then
            fk=f(k)
            dfk=fd1(3,k)
            d2fk=fd2(3,k)
            m=n/2
          else
            m=-m
          endif
          fmm=fm(m)
          dfmdx=fmd1(1,m)
          dfmdy=fmd1(2,m)
          d2fmdx2=fmd2(1,m)
          d2fmdy2=fmd2(2,m)
          d2fmdxdy=fmd2(3,m)
          f(k)=fk*fmm
          DfmmDh=dfmdx*DxDh+dfmdy*DyDh
          DfkDh=dfk*DzDh
          DfmmDk=dfmdx*DxDk+dfmdy*DyDk
          DfkDk=dfk*DzDk
          DfmmDl=dfmdx*DxDl+dfmdy*DyDl
          DfkDl=dfk*DzDl
          if(Type.gt.0) then
            fd1(1,k)=fk*DfmmDh+fmm*DfkDh
            fd1(2,k)=fk*DfmmDk+fmm*DfkDk
            fd1(3,k)=fk*DfmmDl+fmm*DfkDl
            if(Type.gt.1) then
              fd2(1,k)=fk*(dfmdx*D2xDh2+dfmdy*D2yDh2+
     1                     d2fmdx2*DxDh**2+d2fmdy2*DyDh**2+
     2                     2.*d2fmdxdy*DxDh*DyDh)+
     3                 fmm*(dfk*D2zDh2+d2fk*DzDh**2)+2.*DfmmDh*DfkDh
              fd2(2,k)=fk*(dfmdx*D2xDk2+dfmdy*D2yDk2+
     1                     d2fmdx2*DxDk**2+d2fmdy2*DyDk**2+
     2                     2.*d2fmdxdy*DxDk*DyDk)+
     3                 fmm*(dfk*D2zDk2+d2fk*DzDk**2)+2.*DfmmDk*DfkDk
              fd2(3,k)=fk*(dfmdx*D2xDl2+dfmdy*D2yDl2+
     1                     d2fmdx2*DxDl**2+d2fmdy2*DyDl**2+
     2                     2.*d2fmdxdy*DxDl*DyDl)+
     3                 fmm*(dfk*D2zDl2+d2fk*DzDl**2)+2.*DfmmDl*DfkDl
              fd2(4,k)=fk*(dfmdx*D2xDhDk+dfmdy*D2yDhDk+
     1                     d2fmdx2*DxDh*DxDk+d2fmdy2*DyDh*DyDk+
     2                     d2fmdxdy*(DxDh*DyDk+DxDk*DyDh))+
     3                 fmm*(dfk*D2zDhDk+d2fk*DzDh*DzDk)+
     4                 DfmmDh*DfkDk+DfmmDk*DfkDh
              fd2(5,k)=fk*(dfmdx*D2xDhDl+dfmdy*D2yDhDl+
     1                     d2fmdx2*DxDh*DxDl+d2fmdy2*DyDh*DyDl+
     2                     d2fmdxdy*(DxDh*DyDl+DxDl*DyDh))+
     3                 fmm*(dfk*D2zDhDl+d2fk*DzDh*DzDl)+
     4                 DfmmDh*DfkDl+DfmmDl*DfkDh
              fd2(6,k)=fk*(dfmdx*D2xDkDl+dfmdy*D2yDkDl+
     1                     d2fmdx2*DxDk*DxDl+d2fmdy2*DyDk*DyDl+
     2                     d2fmdxdy*(DxDk*DyDl+DxDl*DyDk))+
     3                 fmm*(dfk*D2zDkDl+d2fk*DzDk*DzDl)+
     4                 DfmmDk*DfkDl+DfmmDl*DfkDk
            endif
          endif
        enddo
      enddo
6000  if(Klic.eq.2) then
        do i=1,lmax1q
          pom=MlmC(i)
          f(i)=f(i)*pom
          if(Type.gt.0) then
            do j=1,3
              fd1(j,i)=fd1(j,i)*pom
            enddo
            if(Type.gt.1) then
              do j=1,6
                fd2(j,i)=fd2(j,i)*pom
              enddo
            endif
          endif
        enddo
      else if(Klic.eq.3) then
        do i=1,lmax1q
          pom=LlmC(i)
          f(i)=f(i)*pom
          if(Type.gt.0) then
            do j=1,3
              fd1(j,i)=fd1(j,i)*pom
            enddo
            if(Type.gt.1) then
              do j=1,6
                fd2(j,i)=fd2(j,i)*pom
              enddo
            endif
          endif
        enddo
      endif
9999  return
      end
