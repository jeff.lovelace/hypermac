      function Finter(x,fx,dx,n)
      dimension fx(n)
      pt=x/dx+1.
      ipt=ifix(pt)-1
      Finter=0.
      do k=1,4
        pom=1.
        do l=1,4
          if(k.ne.l) pom=pom*(pt-float(ipt+l))/float(k-l)
        enddo
        l=min(k+ipt,n)
        l=max(l,1)
        Finter=Finter+pom*fx(l)
      enddo
      return
      end
