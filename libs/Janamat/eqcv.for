      logical function eqcv(a,b,n,dif)
      complex a(n),b(n)
      eqcv=.false.
      do i=1,n
        if(abs(a(i)-b(i)).ge.dif) go to 9999
      enddo
      eqcv=.true.
9999  return
      end
