      subroutine SetPermutMat(RMat,n,iord,ich)
      dimension Rmat(n,n)
      call SetRealArrayTo(RMat,n**2,0.)
      ich=0
      k=iord
      m=10**(n-1)
      do i=1,n
        j=k/m
        if(j.lt.1.or.j.gt.3) go to 9000
        RMat(i,j)=1.
        k=k-j*m
        m=m/10
      enddo
      do i=1,n
        sum=0.
        do j=1,n
          sum=sum+RMat(j,i)
          if(sum.gt.1.1) go to 9000
        enddo
        if(sum.lt..1) go to 9000
      enddo
      go to 9999
9000  ich=1
9999  return
      end
