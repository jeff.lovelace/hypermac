      subroutine TrianglC(a,b,n,m,nx)
      complex a(n,m),b(n),p
      jp=1
      do 2000i=1,m
        do j=jp,n
          if(abs(a(j,i)).gt..001) go to 1100
          a(j,i)=(0.,0.)
        enddo
        go to 2000
1100    if(j.ne.jp) then
          do k=i,m
            p=a(j,k)
            a(j,k)=a(jp,k)
            a(jp,k)=p
          enddo
          p=b(j)
          b(j)=b(jp)
          b(jp)=p
        endif
        p=1./a(jp,i)
        if(p.ne.1.) then
          a(jp,i)=1.
          do k=i+1,m
            a(jp,k)=p*a(jp,k)
          enddo
          b(jp)=p*b(jp)
        endif
        do l=jp+1,n
          p=a(l,i)
          a(l,i)=(0.,0.)
          if(p.ne.0.) then
            do k=i+1,m
              a(l,k)=a(l,k)-a(jp,k)*p
            enddo
            b(l)=b(l)-b(jp)*p
          endif
        enddo
        jp=jp+1
        if(jp.gt.m) go to 2500
2000  continue
2500  do i=n,2,-1
        do jp=1,m
          if(abs(a(i,jp)).gt..001) go to 3100
          a(i,jp)=(0.,0.)
        enddo
        cycle
3100    do l=i-1,1,-1
          p=a(l,jp)
          a(l,jp)=(0.,0.)
          if(p.ne.(0.,0.)) then
            do k=jp+1,m
              a(l,k)=a(l,k)-a(i,k)*p
            enddo
            b(l)=b(l)-b(i)*p
          endif
        enddo
      enddo
      nx=0
      do i=1,n
        do j=1,m
          if(abs(a(i,j)).gt..001) then
            nx=nx+1
            exit
          endif
        enddo
      enddo
9999  return
      end
