      subroutine Extrapol(t,dx,a,b,c,d)
      parameter (nn=10)
      dimension xp(3),dx(*),t(0:2,0:2,0:2),b(3),c(6),d(10),
     1          dr(20,0:2,0:2,0:2),px(20),rx(210),sol(20)
      character*80 t80
      logical :: First = .true.
      save dr
      if(First) then
        do i1=0,2
          xp(1)=i1-1
          do i2=0,2
            xp(2)=i2-1
            do i3=0,2
              xp(3)=i3-1
              n=1
              dr(1,i1,i2,i3)=1.
              do j1=1,3
                n=n+1
                dr(n,i1,i2,i3)=xp(j1)
              enddo
              do j1=1,3
                do j2=j1,3
                  n=n+1
                  dr(n,i1,i2,i3)=xp(j1)*xp(j2)
                enddo
              enddo
            enddo
          enddo
        enddo
        First=.false.
      endif
      call SetRealArrayTo(px,nn,0.)
      call SetRealArrayTo(rx,(nn*(nn+1))/2,0.)
      do i1=0,2
        xp(1)=i1-1
        do i2=0,2
          xp(2)=i2-1
          do i3=0,2
            xp(3)=i3-1
            tp=t(i1,i2,i3)
            n=0
            do i=1,nn
              px(i)=px(i)+dr(i,i1,i2,i3)*tp
              do j=1,i
                n=n+1
                rx(n)=rx(n)+dr(i,i1,i2,i3)*dr(j,i1,i2,i3)
              enddo
            enddo
          enddo
        enddo
      enddo



      n=0
      do i=1,nn
        n=n+i
        sol(i)=1./sqrt(rx(n))
      enddo
      call znorm(rx,sol,nn)
      call smi(rx,nn,ising)
      call znorm(rx,sol,nn)
      if(ising.eq.0) then
        call nasob(rx,px,sol,nn)
        do i1=0,2
          xp(1)=i1-1
          do i2=0,2
            xp(2)=i2-1
            do i3=0,2
              xp(3)=i3-1
              n=1
            px(1)=1.
              do j1=1,3
                n=n+1
                px(n)=xp(j1)
              enddo
              do j1=1,3
                do j2=j1,3
                  n=n+1
                  px(n)=xp(j1)*xp(j2)
                enddo
              enddo
              pom=VecOrtScal(sol,px,nn)
            enddo
          enddo
        enddo
      else
        write(t80,'(i5)') ising
        call FeWinMessage('Je to singular',t80)
      endif
      return
      end
