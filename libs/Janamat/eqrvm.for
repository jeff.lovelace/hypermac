      logical function eqrvm(a,b,n,dif)
      dimension a(n),b(n)
      eqrvm=.false.
      do i=1,n
        if(abs(a(i)+b(i)).ge.dif) go to 9999
      enddo
      eqrvm=.true.
9999  return
      end
