      function DTrCoef(l,mp,m,shbeta,chbeta)
      include 'fepc.cmn'
      DTrCoef=0.
      kp=max(0,m-mp)
      kk=min(l+m,l-mp)
      kkk=kp+mp-m
      A=(-1)**kkk*sqrt(factorial(l+mp)*factorial(l-mp)/
     1                (factorial(l+m)*factorial(l-m)))
      do k=kp,kk
        DTrCoef=DTrCoef+A*chbeta**(2*l+m-mp-2*k)*shbeta**(2*k+mp-m)*
     1                  Binomcoef(l+m,k)*Binomcoef(l-m,l-mp-k)
        A=-A
      enddo
      return
      end
