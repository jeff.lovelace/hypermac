      subroutine ScalDivMatC(AMat,BMat,n,m,ScalDiv,ich)
      complex AMat(n,m),ScalDiv
      dimension BMat(n,m)
      logical EqCV0,EqRV0,UzHoMame
      ich=0
      ScalDiv=0.
      if(EqCV0(AMat,n*m,.001)) then
        go to 9999
      else if(EqRV0(BMat,n*m,.001)) then
        go to 9000
      else
        UzHoMame=.false.
        do i=1,m
          do j=1,n
            if(abs(AMat(i,j)).lt..001) then
              if(abs(BMat(i,j)).lt..001) then
                cycle
              else
                go to 9000
              endif
            else
              if(abs(BMat(i,j)).lt..001) then
                go to 9000
              else
                if(UzHoMame) then
                  if(abs(AMat(i,j)-ScalDiv*BMat(i,j)).gt..001)
     1              go to 9000
                else
                  ScalDiv=AMat(i,j)/cmplx(BMat(i,j))
                  UzHoMame=.true.
                endif
              endif
            endif
          enddo
        enddo
      endif
      go to 9999
9000  ich=1
9999  return
      end
