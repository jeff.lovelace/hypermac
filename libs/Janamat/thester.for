      double precision function THester(fi2)
      real fi2
      double precision :: dfi2,dpid4=0.78539816339744825d0
      dfi2=fi2
      THester=dlog(dabs(dtan(dfi2*.5+dpid4)))
      return
      end
