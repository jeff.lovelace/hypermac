      subroutine FileChanges
      include 'fepc.cmn'
      character*(*) FileName
      character*(*) Lines(*)
      character*256 t256
      entry AppendFile(FileName,Lines,n)
      ln1=NextLogicNumber()
      call OpenFile(ln1,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      nold=0
1000  read(ln1,FormA,end=1100) t256
      if(t256.eq.' ') go to 1100
      nold=nold+1
      go to 1000
1100  call CloseIfOpened(ln1)
      call OpenFile(ln1,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      do i=1,nold
        read(ln1,FormA) t256
      enddo
      do i=1,n
        write(ln1,FormA1)(Lines(i)(j:j),j=1,idel(Lines(i)))
      enddo
      call CloseIfOpened(ln1)
      go to 9999
      entry DeleteLinesFromFile(FileName,n,m)
      id=1
      go to 1500
      entry RewriteLinesOnFile(FileName,n,m,Lines,nl)
      id=0
1500  ln1=NextLogicNumber()
      call OpenFile(ln1,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9000
      ln2=NextLogicNumber()
      call OpenFile(ln2,'WorkFile.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      i=0
2000  read(ln1,FormA,end=2100) t256
      i=i+1
      if(i.lt.n.or.i.gt.m) then
        write(ln2,FormA1)(t256(j:j),j=1,idel(t256))
      else if(id.eq.0.and.i.eq.n) then
        do k=1,nl
          write(ln2,FormA1)(lines(k)(j:j),j=1,idel(Lines(k)))
        enddo
      endif
      go to 2000
2100  call CloseIfOpened(ln1)
      call CloseIfOpened(ln2)
      call MoveFile('WorkFile.tmp',FileName)
      go to 9999
9000  call CloseIfOpened(ln1)
9999  return
      end
