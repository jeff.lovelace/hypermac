      function VecOrtLen(v,n)
      dimension v(n)
      VecOrtLen=0.
      do i=1,n
        VecOrtLen=VecOrtLen+v(i)**2
      enddo
      VecOrtLen=sqrt(VecOrtLen)
      return
      end
