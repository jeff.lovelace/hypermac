      character*2 function nty(i)
      include 'fepc.cmn'
      character*2 th(4)
      data th/'st','nd','rd','th'/
      if(i.lt.10.or.i.gt.19) then
        j=min(mod(i-1,10)+1,4)
      else
        j=4
      endif
      nty=th(j)
      return
      end
