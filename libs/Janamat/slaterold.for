      subroutine SlaterOld(nmax,s,z,sla,derz,ders,ich)
      include 'fepc.cmn'
      dimension sla(*),derz(*),ders(*)
      real ninv
      ich=0
      if(nmax.lt.1.or.nmax.gt.20) then
        ich=1
        go to 9999
      endif
      nmxp1=nmax+1
      pom=z**2
      dinv=1./(s**2+pom)
      pom1=2.*s*dinv
      pom2=2.*z*dinv
      pom3=pi4*dinv*pom
      sinv=z/s
      sla(1)=pom3
      nn=3
      do n=2,nmxp1
        pom=z*float(n-1)*pom3/float(n)
        pom3=pom1*pom
        sla(nn)=pom3
        pom4=pom2*pom
        nnp=nn
        nn=nn-1
        sla(nn)=pom4
        if(n.lt.3) go to 1400
        ninv=1./float(n)
        do l=n-3,0,-1
          nn=nn-1
          nnz=nnz-1
          pom4=sinv*(pom4-float(n-l-2)*sla(nnz)*ninv)
          sla(nn)=pom4
        enddo
1400    nnz=nnp+1
        nn=nnz+n
      enddo
      nn=0
      do n=1,nmax
        pom2=float(n+1)/z
        pom3=pom2*pi4
        nnz=nn+n
        do l=0,n-1
          nn=nn+1
          nnz=nnz+1
          pom1=1./sla(nn)
          pom=-float(l+1)*sla(nnz+1)
          if(n.ne.0) pom=pom+float(l)*sla(nnz-1)
          ders(nn)=pom3*pom1*pom/float(2*l+1)
          derz(nn)=pom2*(1.-sla(nnz)*pom1)
        enddo
      enddo
9999  return
      end
