      subroutine PVoigtTOF(dt,R,dRdAl,dRdBe,dRdT,dRdS,dRdG)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      etaPwdComp=(1.-etaPwd)
      dtq=dt**2
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        RLorentz=ConvTOFWithLorentz(dt,Alpha12,Beta12,fwhm,dRLdT,dRLdAl,
     1                              dRLdBe,dRLdG)
        if(CalcDer) then
          dRLdG=etaPwd*dRLdG
          dRLdT=etaPwd*dRLdT
          dRLdAl=etaPwd*dRLdAl
          dRLdBe=etaPwd*dRLdBe
        endif
      else
        RLorentz=0.
        if(CalcDer) then
          dRLdT=0.
          dRLdG=0.
          dRLdAl=0.
          dRLdBe=0.
          dRLdG=0.
        endif
      endif
      if((KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt).and.sigp.ne.0.)
     2  then
        RGauss=ConvTOFWithGauss(dt,Alpha12,Beta12,fwhm,dRGdT,dRGdAl,
     1                          dRGdBe,dRGdS)
        if(CalcDer) then
          dRGdS=etaPwdComp*dRGdS
          dRGdT=etaPwdComp*dRGdT
          dRGdAl=etaPwdComp*dRGdAl
          dRGdBe=etaPwdComp*dRGdBe
        endif
      else
        RGauss=0.
        if(CalcDer) then
          dRGdT=0.
          dRGdS=0.
          dRGdAl=0.
          dRGdBe=0.
          dRGdS=0.
        endif
      endif
      R=etaPwd*RLorentz+etaPwdComp*RGauss
      if(CalcDer) then
        dRdT=dRLdT+dRGdT
        dRdAl=dRLdAl+dRGdAl
        dRdBe=dRLdBe+dRGdBe
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dRdS=dedffg*(RLorentz-RGauss)+(dRLdG+dRGdS)*dfwdg
          if(sqsg.ne.0.) then
            dRdS=1.17741*dRdS/sqsg
          else
            dRdS=0.
          endif
        else
          dRdS=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dRdG=dedffl*(RLorentz-RGauss)+(dRLdG+dRGdS)*dfwdl
        else
          dRdG=0.
        endif
      endif
      return
      end
