      subroutine RomanNumber(n,String)
      character*(*) String
      if(n.gt.20) then
        String='???'
        go to 9999
      endif
      j=mod(n-1,10)+1
      if(j.eq.1) then
        String='i'
      else if(j.eq.2) then
        String='ii'
      else if(j.eq.3) then
        String='iii'
      else if(j.eq.4) then
        String='iv'
      else if(j.eq.5) then
        String='v'
      else if(j.eq.6) then
        String='vi'
      else if(j.eq.7) then
        String='vii'
      else if(j.eq.8) then
        String='viii'
      else if(j.eq.9) then
        String='ix'
      else if(j.eq.10) then
        String='x'
      endif
      if(n.gt.10) String='x'//String(:idel(String))
9999  return
      end
