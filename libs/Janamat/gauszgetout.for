      subroutine GauszGetOut(a,m,n,r,q,dim)
      integer dim,p(:),q(n),r
      complex a(m,n),b(:,:)
      real lambda
      allocatable b,p
      allocate(b(n,n),p(n))
      call SetComplexArrayTo(b,n*n,(0.,0.))
      call SetIntArrayTo(q(r+1),n-r,0)
      k=1
      l=0
      do j=1,n
        if(q(k).eq.j) then
          k=k+1
        else
          l=l+1
          p(l)=j
        endif
      enddo
      do j=1,n-r
        j2=p(j)
        b(j2,j)=(-1.0,0.0)
        do i=1,r
          i2=q(i)
          b(i2,j)=a(i,j2)
        enddo
      enddo
      lambda = 0.
      do i=1,dim
        lambda=lambda+abs(b(i,1))**2
      enddo
      lambda=sqrt(lambda)
      do i=1,dim
        do j=1,dim
          a(i,j)=b((i-1)*dim+j,1)/lambda
        enddo
      enddo
      deallocate(b,p)
      return
      end
