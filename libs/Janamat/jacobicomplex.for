      subroutine JacobiComplex(AIn,n,D,V,NRot)
      include 'fepc.cmn'
      complex AIn(n,n),D(n),V(n,n),B(:,:)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,aip,aiq,apj,aqj,
     1        A(:,:),Vpom(:,:),VpomT(:,:),Upp,Uqq,Upq,Uqp,Vip,Viq,
     2        Apom(:,:)
      allocatable A,Vpom,VpomT,Apom,B
      allocate(A(n,n),B(n,n),Apom(n,n),Vpom(n,n),VPomT(n,n))
      pom=0.
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
          if(p.eq.q) pom=pom+abs(A(p,q))
        enddo
        V(p,p)=1.
      enddo
c
c   Pokus o jakesi pootoceni v pripade, ze na diagonle jsou same nuly
c
c      if(pom.le.0.) then
c        if(n.eq.3) then
c          Vpom (1,1)= 1./sqrt(3.)
c          VpomT(1,1)= 1./sqrt(3.)
c          Vpom (2,1)= 1./sqrt(3.)
c          VpomT(1,2)= 1./sqrt(3.)
c          Vpom (3,1)= 1./sqrt(3.)
c          VpomT(1,3)= 1./sqrt(3.)
c          Vpom (1,2)= 1./sqrt(2.)
c          VpomT(2,1)= 1./sqrt(2.)
c          Vpom (2,2)=-1./sqrt(2.)
c          VpomT(2,2)=-1./sqrt(2.)
c          Vpom (3,2)= 0.
c          VpomT(2,3)= 0.
c          Vpom (1,3)= 1./sqrt(6.)
c          VpomT(3,1)= 1./sqrt(6.)
c          Vpom (2,3)= 1./sqrt(6.)
c          VpomT(3,2)= 1./sqrt(6.)
c          Vpom (3,3)=-2./sqrt(6.)
c          VpomT(3,3)=-2./sqrt(6.)
c          call MultMC(Vpom,A,Apom,3,3,3)
c          call MultMC(Apom,VPomT,A,3,3,3)
c          write(6,'('' Kontrola matic'')')
c          write(6,'(6f10.3)') A
c          write(6,'(1x,60(''-''))')
c          V=VpomT
c        endif
c      endif
      NRot=0
      do m=1,200
        sm=(0.,0.)
        do p=1,n
          do q=1,n
            if(p.ne.q) sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..000001) go to 9000
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.0000001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).le.tresh) cycle
            ap=A(q,p)
            bp=A(p,p)-A(q,q)
            cp=-A(p,q)
            if(abs(ap).gt.0.) then
              BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
              go to 1100
            else if(abs(bp).gt.0.) then
              BetaK=-cp/bp
              go to 1100
            else
              ap=A(p,q)
              bp=A(p,p)-A(q,q)
              cp=-A(q,p)
              BetaK=A(p,q)
            endif
1100        if(abs(BetaK).lt.tresh) cycle
            AlphaK=atan2(aimag(BetaK),real(BetaK))
            ThetaK=atan(abs(BetaK))
1200        app=A(p,p)
            aqq=A(q,q)
            apq=A(p,q)
            aqp=A(q,p)
            CosThetaK=cos(ThetaK)
            SinThetaK=sin(ThetaK)
            CosTheta2K=CosThetaK**2
            LK=(aqq-app)*cmplx(SinThetaK**2,0.)+
     1         (apq*cexp(-cmplx(0.,AlphaK))+
     2          aqp*cexp( cmplx(0.,AlphaK)))*
     3          cmplx(SinThetaK*CosThetaK,0.)
            A(p,p)=app+LK
            A(q,q)=aqq-LK
            A(p,q)=(apq+(aqq-app)*BetaK
     1             -aqp*BetaK**2)*CosTheta2K
            A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1             -apq*conjg(BetaK)**2)*CosTheta2K
            do i=1,n
              if(i.eq.p.or.i.eq.q) cycle
              aip=A(i,p)
              aiq=A(i,q)
              A(i,p)=aip*CosThetaK+
     1               aiq*SinThetaK*cexp(cmplx(0.,-AlphaK))
              A(i,q)=aiq*CosThetaK-
     1               aip*SinThetaK*cexp(cmplx(0., AlphaK))
            enddo
            do j=1,n
              if(j.eq.p.or.j.eq.q) cycle
              apj=A(p,j)
              aqj=A(q,j)
              A(p,j)=apj*CosThetaK+
     1               aqj*SinThetaK*cexp(cmplx(0., AlphaK))
              A(q,j)=aqj*CosThetaK-
     1               apj*SinThetaK*cexp(cmplx(0.,-AlphaK))
            enddo
            Upp=CosThetaK
            Uqq=CosThetaK
            Upq=-SinThetaK*cexp(cmplx(0., AlphaK))
            Uqp= SinThetaK*cexp(cmplx(0.,-AlphaK))
            do i=1,n
              Vip=V(i,p)
              Viq=V(i,q)
              V(i,p)=Vip*Upp+Viq*Uqp
              V(i,q)=Vip*Upq+Viq*Uqq
            enddo
            NRot=NRot+1
          enddo
        enddo
      enddo
9000  do p=1,n
        D(p)=A(p,p)
      enddo
      go to 9999
c
c   Kontrola diagonalizace
c
c      Vpom=V
c      call MatTransConjg(Vpom,VpomT,n)
c      call MultMC(VpomT,AIn,Apom,n,n,n)
c      call MultMC(Apom,Vpom,B,n,n,n)
c      do p=1,n
c        do q=1,n
c          if(abs(B(p,q)-A(p,q)).gt..1) go to 9100
c        enddo
c      enddo
c      go to 9999
c9100  write(6,'('' Kontrola'')')
c      do p=1,n
c        write(6,'(20f10.5)')(A(p,q),q=1,n)
c      enddo
c      write(6,'(1x,60(''-''))')
c      do p=1,n
c        write(6,'(20f10.5)')(B(p,q),q=1,n)
c      enddo
c      write(6,'(1x,60(''=''))')
9999  deallocate(A,B,Apom,Vpom,VpomT)
      return
      end
