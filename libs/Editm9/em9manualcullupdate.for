      subroutine EM9ManualCullUpdate(Klic)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      logical WizardModeIn
      dimension RefDatCorrespondOld(:)
      allocatable RefDatCorrespondOld
      WizardModeIn=WizardMode
      SilentRun=.true.
      allocate(RefDatCorrespondOld(NRefBlock))
      do i=1,NRefBlock
        RefDatCorrespondOld(i)=RefDatCorrespond(i)
      enddo
      call iom95(0,fln(:ifln)//'.m95')
      do i=1,NRefBlock
        RefDatCorrespond(i)=RefDatCorrespondOld(i)
      enddo
      deallocate(RefDatCorrespondOld)
      n=0
      do IRefBlock=1,MaxRefBlockAr
        call OpenRefBlockM95(95,IRefBlock,fln(:ifln)//'.m95')
        write(Cislo,'(''.l'',i2)') IRefBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        call OpenFile(97,RefBlockFileName,'formatted','unknown')
        if(ErrFlag.ne.0) then
          call CloseIfOpened(95)
          call CloseIfOpened(97)
          go to 9999
        endif
1100    call DRGetReflectionFromM95(95,iend,ich)
        if(iend.ne.0.or.ich.ne.0) go to 1200
        if(no.gt.0.and.iflg(1).ge.0.and.iflg(2).eq.1) then
          n=n+1
          if(n.gt.ubound(RefBlockAr,1)) then
            n=n-1
            go to 1150
          endif
          if(mod(RefBlockAr(n),1000).eq.IRefBlock.and.
     1           RefBlockAr(n)/1000.eq.no) then
            iflg(3)=ICullAr(n)
          else
            n=n-1
          endif
        endif
1150    call DRPutReflectionToM95(97,nl)
        go to 1100
1200    call CloseIfOpened(95)
        call CloseIfOpened(97)
      enddo
      call CompleteM95(1)
      if(Klic.eq.0) then
        if(allocated(riar))
     1    deallocate(riar,rsar,ICullAr,RefBlockAr,HCondAr,RunCCDAr)
        call CompleteM90
        call EM9CreateM90(1,ich)
      endif
9999  SilentRun=.false.
      WizardMode=WizardModeIn
      return
      end
