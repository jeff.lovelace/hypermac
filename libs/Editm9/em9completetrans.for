      subroutine EM9CompleteTrans(StavVolani,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension PomMat(36),TrRefBlockI(36),CellPom(6),QuPom(3,3),
     1          TrMatP(3,3),TrMatPI(3,3),xp(3)
      character*256 EdwStringQuest
      character*80  Veta
      character*8 :: HKLInput(6)=(/'h(inp)','k(inp)','l(inp)',
     1                             'm(inp)','n(inp)','p(inp)'/)
      character*8 :: HKLFloat(6)=(/'H(float)','K(float)','L(float)',
     1                             'M(float)','N(float)','P(float)'/)
      character*21  LabelT
      character*2   nty
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      integer EdwStateQuest,StavVolani,UzTuByl,UseTabsIn,UseTabsP
      logical CrwLogicQuest,EqRV,MatRealEqUnitMat,FeYesNoHeader,
     1        UpdateRefCell,UzSePtal,EqRVM,VolatHide,Poprve
      data LabelT/'%Number of domains'/
      save NDimLim
      UseTabsIn=UseTabs
      UzTuByl=mod(StavVolani/10,10)
      UpdateRefCell=.false.
      UzSePtal=.false.
      VolatHide=.false.
      Poprve=.true.
      if(ReimportRefBlock) then
        call CopyVek(CellRefBlock(1,0),CellPom,6)
        call CopyVek(QuRefBlock(1,1,0),QuPom,3*(maxNDim-3))
        call MatInv(TrRefBlock(1,KRefBlock),TrRefBlockI,pom,maxNDim)
        call CrlCellQTrans(TrRefBlockI,CellPom,QuPom,maxNDim,ich)
        if(ich.ne.0) go to 1100
        if(.not.EqRV(CellRefBlock(1,KRefBlock),CellPom,3,.001).or.
     1     .not.EqRV(CellRefBlock(4,KRefBlock),CellPom(4),3,.01)) then
          call FindCellTrans(CellRefBlock(1,KRefBlock),CellPom,
     1                       TrMatP,ich)
          if(ich.eq.0) then
            call UnitMat(PomMat,maxNDim)
            do i=1,3
              do j=1,3
                k=j+(i-1)*maxNDim
                PomMat(k)=TrMatP(j,i)
              enddo
            enddo
            do i=4,maxNDim
              call MultM(QuPom(1,i-3),TrMatP,xp,1,3,3)
              if(EqRVM(xp,QuRefBlock(1,1,KRefBlock),3,.01)) then
                j=i+(i-1)*maxNDim
                PomMat(j)=-1.
              endif
            enddo
            if(MatRealEqUnitMat(PomMat,maxNDim,.001)) go to 1100
            call MultM(PomMat,TrRefBlock(1,KRefBlock),TrRefBlockI,
     1                 maxNDim,maxNDim,maxNDim)
            call CopyMat(TrRefBlockI,TrRefBlock(1,KRefBlock),maxNDim)
            UseTrRefBlock(KRefBlock)=.not.
     1        MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001)
            if(KRefBlock.eq.1) then
              call CopyVek(CellRefBlock(1,KRefBlock),CellPom,6)
              call CopyVek(QuRefBlock(1,1,KRefBlock),QuPom,
     1                     3*(maxNDim-3))
              call CrlCellQTrans(TrRefBlock(1,KRefBlock),CellPom,QuPom,
     1                           maxNDim,ich)
              UpdateRefCell=.not.EqRV(CellRefBlock(1,0),CellPom,3,.001)
     1                .or..not.EqRV(CellRefBlock(4,0),CellPom(4),3,.01)
            endif
          else
            UseTabs=NextTabs()
            xpom=20.
            do i=1,6
              call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
              xpom=xpom+50.
            enddo
            NInfo=6
            TextInfo(1)='relationship between the reimported cell '//
     1                  'parameters:'
            write(TextInfo(2),102)(Tabulator,CellRefBlock(i,KRefBlock),
     1                             i=1,6)
            TextInfo(3)='and the original ones:'
            write(TextInfo(4),102)(Tabulator,CellPom(i),i=1,6)
            TextInfo(5)='could not be estabilished and therefore the '//
     1                  'reimport will not'
            TextInfo(6)='be accomplished.'
            call FeInfoOut(-1.,-1.,'ERROR','L')
            ich=-1
            go to 9999
          endif
        endif
      endif
1100  id=NextQuestId()
      CheckKeyboard=.true.
      if(KRefBlock.eq.1.and.CreateNewRefBlock) then
        Veta='Define'
      else
        Veta='Relationship to'
      endif
      Veta=Veta(:idel(Veta))//
     1     ' the reference cell/split by twinning'
      call FeQuestTitleMake(id,Veta)
      il=0
      tpom=5.
      xpom=50.
      il=il+1
      write(Veta,100)(CellRefBlock(i,0),i=1,6)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblCell=LblLastMade
      il=il+1
      if(KRefBlock.gt.1.or..not.CreateNewRefBlock) then
        write(Veta,'(''Target dimension:'',i2)') maxNDim
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        NDimLim=maxNDim
        nEdwNDim=0
      else
        Veta='Tar%get dimension'
        dpom=45.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        if(UzTuByl.eq.0) then
          NDimLim=3
          do i=1,3
            if(QuRefBlock(1,i,0).gt.-300.) NDimLim=NDimLim+1
          enddo
        endif
        NDim(KPhase)=NDimLim
        NDimI(KPhase)=NDimLim-3
        NDimQ(KPhase)=NDimLim**2
        maxNDim=max(maxNDim,NDimLim)
        call UnitMat(TrRefBlock(1,KRefBlock),maxNDim)
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNDim=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,maxNDim,.false.)
        call FeQuestEudOpen(EdwLastMade,NDimLim,6,1,0.,0.,0.)
      endif
      nEdwModFr=0
      nEdwModTo=0
      nLblModFr=0
      nLblModTo=0
      do i=1,3
        il=il+1
        write(Veta,'(''%'',i1,a2,'' modulation vector'')') i,nty(i)
        if(i+3.le.NDimLim+1) xpom=tpom+FeTxLengthUnder(Veta)+10.
        if(i+3.le.NDimLim) then
          write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          if(i.eq.1) nLblModFr=LblLastMade
          nLblModTo=LblLastMade
        else if(KRefBlock.eq.1) then
          dpom=WizardLength*.5-xpom+10.
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i+2.eq.NDimLim) then
            nEdwModFr=EdwLastMade
          else if(i.eq.3) then
            nEdwModTo=EdwLastMade
          endif
          if(i.le.maxNDim-3) then
            call FeQuestRealAEdwOpen(EdwLastMade,
     1        QuRefBlock(1,i,0),3,QuRefBlock(1,i,0).lt.-300.,.false.)
          else
            call FeQuestEdwDisable(EdwLastMade)
          endif
        endif
      enddo
      il=il+1
      Veta='%Max. satellite index'
      dpom=60.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestEdwDisable(EdwLastMade)
      nEdwMMax=EdwLastMade
      Veta='A%ccuracy'
      il=il+1
      dpom=120.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestEdwDisable(EdwLastMade)
      nEdwDiffSat=EdwLastMade
      il=il+1
      tpom=50.
      Veta='%Define transformation matrix'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtDefineTrans=ButtonLastMade
      il=il+1
      tpom=20.
      Veta='Transformation matrix applied to input indices:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblLabelMatrix=LblLastMade
      call FeQuestLblOff(LblLastMade)
      YShow=QuestYPosition(id,il+7)+QuestYMin(id)
      XShow=QuestXMin(id)
      il=2
      Veta='%Twinning'
      tpom=WizardLength*.5+50.
      xpom=tpom+CrwXd+10.
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwTwin=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NTwin.gt.1)
      xpom=xpom+FeTxLengthUnder(Veta)+20.
      Veta='T%winning matrices'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonDisable(ButtonLastMade)
      nButtTwinMat=ButtonLastMade
      il=il+1
      dpom=60.
      xpom=WizardLength-dpom-EdwYd-30.
      call FeQuestEudMake(id,tpom,il,xpom,il,LabelT,'L',dpom,EdwYd,1)
      call FeQuestIntEdwOpen(EdwLastMade,NTwin,.false.)
      call FeQuestEudOpen(EdwLastMade,2,mxsc,1,0.,0.,0.)
      nEdwNTwin=EdwLastMade
      il=il+1
      Veta='Data %related to domain#'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,ITwRead(KRefBlock),.false.)
      call FeQuestEudOpen(EdwLastMade,1,NTwin,1,0.,0.,0.)
      if(HKLF5RefBlock(KRefBlock).ne.0)
     1  call FeQuestEdwDisable(EdwLastMade)
      nEdwITwin=EdwLastMade
      NTwinOld=NTwin
      NDimOld=maxNDim
      il=il+1
      dpom=dpom+15.
      Veta='Multipl%y input F(hkl)/I(hkl) by'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScale=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ScaleRefBlock(KRefBlock),
     1                        .false.,.false.)
      if(HKLF5RefBlock(KRefBlock).ne.0) then
        il=il+1
        Veta='%HKLF5 file by Jana tools'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwHKLF5=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,HKLF5RefBlock(KRefBlock).eq.2)
        if(Ntwin.le.1) then
          NTwin=1
1200      read(71,FormatRefBlock(KRefBlock),err=1250,end=1250)
     1      (k,i=1,maxNDim),pom,pom,k
          NTwin=max(NTwin,iabs(k))
          go to 1200
1250      rewind 71
          do i=2,NTwin
            call UnitMat(rtw (1,i),3)
            call UnitMat(rtwi(1,i),3)
          enddo
          if(NTwin.gt.1) call FeQuestCrwOn(nCrwTwin)
        endif
      else
        nCrwHKLF5=0
      endif
      if(maxNDim.gt.3) then
        il=il+1
        Veta='Import %only satellites'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwImpOnlySat=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,ImportOnlySatellites(KRefBlock))
      else
        nCrwImpOnlySat=0
      endif
1300  if(NTwin.gt.1) then
        call FeQuestButtonOpen(nButtTwinMat,ButtonOff)
        call FeQuestIntEdwOpen(nEdwNTwin,NTwin,.false.)
        call FeQuestEudOpen(nEdwNTwin,2,mxsc,1,0.,0.,0.)
        call FeQuestIntEdwOpen(nEdwITwin,ITwRead(KRefBlock),.false.)
        call FeQuestEudOpen(nEdwITwin,1,NTwin,1,0.,0.,0.)
        if(HKLF5RefBlock(KRefBlock).ne.0)
     1    call FeQuestEdwDisable(nEdwITwin)
      else
        call FeQuestButtonDisable(nButtTwinMat)
        call FeQuestEdwDisable(nEdwNTwin)
        call FeQuestEdwDisable(nEdwITwin)
      endif
      if(NDim95(KRefBlock).lt.maxNDim) then
        call FeQuestIntEdwOpen(nEdwMMax,MMaxRefBlock(KRefBlock),
     1                         .false.)
        call FeQuestEudOpen(nEdwMMax,0,111,1,0.,0.,0.)
        call FeQuestRealAEdwOpen(nEdwDiffSat,
     1                           DiffSatRefBlock(1,KRefBlock),3,
     2                           .false.,.false.)
      else
        call FeQuestEdwDisable(nEdwMMax)
        call FeQuestEdwDisable(nEdwDiffSat)
      endif
      if(maxNDim.ne.NDimOld) then
        k=0
        call CopyMat(TrRefBlock(1,KRefBlock),PomMat,NDimOld)
        call UnitMat(TrRefBlock(1,KRefBlock),maxNDim)
        do i=1,NDimOld
          do j=1,6
            if(j.le.NDimOld) then
              k=k+1
              if(i.le.maxNDim.and.j.le.maxNDim) then
                l=j+(i-1)*maxNDim
                TrRefBlock(l,KRefBlock)=PomMat(j+(i-1)*NDimOld)
              endif
            endif
          enddo
        enddo
      endif
      if(UpdateRefCell) then
        UseTabsP=UseTabs
        UseTabs=NextTabs()
        xpom=80.
        do i=1,6
          call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
          xpom=xpom+50.
        enddo
        NInfo=3
        TextInfo(1)='Reimported data file leads to different '//
     1              'reference cell parameters'
        write(Veta,102)(Tabulator,CellRefBlock(i,0),i=1,6)
        TextInfo(2)='Old ones:'//Veta(:idel(Veta))
        write(Veta,102)(Tabulator,CellPom(i),i=1,6)
        TextInfo(3)='New ones:'//Veta(:idel(Veta))
        Veta='Do you want to update them?'
        if(FeYesNoHeader(-1.,-1.,Veta,1)) then
          call CopyVek(CellPom,CellRefBlock(1,0),6)
          write(Veta,100)(CellRefBlock(i,0),i=1,6)
          call FeQuestLblChange(nLblCell,Veta)
          nLbl=nLblModFr
          do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
            call CopyVek(QuPom(1,i),QuRefBlock(1,i,0),3)
            write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
        endif
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsP
        UpdateRefCell=.false.
      endif
      if(MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001))
     1  then
        call FeQuestLblOff(nLblLabelMatrix)
        if(VolatHide) then
          call CrlMatEqHide
          VolatHide=.false.
        endif
      else
        if(VolatHide) call CrlMatEqHide
        if(NDim95(KRefBlock).eq.3.and.maxNDim.gt.3) then
          call MatBlock3(TrRefBlock(1,KRefBlock),TrMatP,maxNDim)
          call CrlMatEqShow(XShow,YShow,6,TrMatP,0,HKLFloat,HKLInput,3)
        else
          call CrlMatEqShow(XShow,YShow,6,TrRefBlock(1,KRefBlock),0,
     1                      IndicesCapital,Indices,maxNDim)
        endif
        VolatHide=.true.
        call FeQuestLblOn(nLblLabelMatrix)
      endif
      if(Poprve) then
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)

        Poprve=.false.
      endif
      NDimOld=maxNDim
1500  call FeQuestEvent(id,ich)
      if(nEdwNTwin.gt.0) then
        if(EdwStateQuest(nEdwNTwin).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwNTwin,Ntwin)
          do i=NTwinOld+1,NTwin
            call UnitMat(rtw(1,i),3)
          enddo
        endif
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNDim) then
        call FeQuestIntFromEdw(nEdwNDim,maxNDim)
        NDim(KPhaseDR)=maxNDim
        NDimI(KPhaseDR)=maxNDim-3
        NDimQ(KPhaseDR)=maxNDim**2
        nEdw=nEdwModFr
        do i=1,3
          if(i+3.gt.NDimLim) then
            if(i+3.le.maxNDim) then
              if(EdwStateQuest(nEdw).ne.EdwOpened)
     1          call FeQuestRealAEdwOpen(nEdw,QuRefBlock(1,i,KRefBlock),
     2                        3,QuRefBlock(1,i,KRefBlock).lt.0.,.false.)
            else
              call FeQuestEdwDisable(nEdw)
            endif
            nEdw=nEdw+1
          endif
        enddo
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNTwin) then
        call FeQuestIntEdwOpen(nEdwITwin,
     1                         min(ITwRead(KRefBlock),NTwin),.false.)
        call FeQuestEudOpen(nEdwITwin,1,NTwin,1,0.,0.,0.)
        if(HKLF5RefBlock(KRefBlock).ne.0)
     1    call FeQuestEdwDisable(nEdwITwin)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwTwin) then
        if(CrwLogicQuest(CheckNumber)) then
          NTwin=2
          EventType=EventEdw
          EventNumber=nEdwITwin
        else
          NTwin=1
        endif
        go to 1300
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDefineTrans) then
        WizardMode=.false.
        Veta='Define the transformation matrix'
        if(NDim95(KRefBlock).eq.3.and.maxNDim.gt.3) then
          if(UzSePtal) then
            call MatBlock3(TrRefBlock(1,KRefBlock),TrMatP,maxNDim)
          else
            call FindCellTrans(CellRefBlock(1,KRefBlock),
     1                         CellRefBlock(1,0),
     2                         TrMatP,ichp)
            if(ichp.ne.0) call UnitMat(TrMatP,3)
          endif
          call FeReadRealMat(-1.,-1.,Veta,HKLInput,IdUseNew,HKLFloat,
     1                     TrMatP,TrMatPI,3,.true.,.false.,ichp)
          call MatFromBlock3(TrMatP,TrRefBlock(1,KRefBlock),maxNDim)
          call MatFromBlock3(TrMatPI,TrRefBlockI,maxNDim)
        else
          call FeReadRealMat(-1.,-1.,Veta,Indices,IdChangeCase,Indices,
     1                       TrRefBlock(1,KRefBlock),TrRefBlockI,
     1                       maxNDim,.true.,.false.,ichp)
        endif
        if(ichp.ne.0) go to 1600
        UzSePtal=.true.
        if(KRefBlock.le.1.and.CreateNewRefBlock.and.
     1     .not.ParentStructure) then
          call EM9TrMatApply(nLblCell,nLblModFr,1,PomMat,ichp)
        else
          call EM9TrMatApply(nLblCell,nLblModFr,0,
     1                       TrDirCos(1,KRefBlock),ichp)
        endif
1600    WizardMode=.true.
        WizardTitle=.true.
        if(ichp.eq.0) then
          go to 1300
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtTwinMat) then
        WizardMode=.false.
        if(.not.MatRealEqUnitMat(TrMP,MaxNDim,.001)) then
          NInfo=2
          TextInfo(1)='The repository reference cell parameters are '//
     1                'different from those used in the refinement'
          TextInfo(2)='as the cell has been transformed. The twinning'//
     1                ' matri'
          if(NTwin.gt.2) then
            Veta='ces are'
          else
            Veta='x is'
          endif
          TextInfo(2)=TextInfo(2)(:idel(TextInfo(2)))//
     1                Veta(:idel(Veta))//' related to the final cell '//
     2                'parameters.'
          call FeInfoOut(-1.,-1.,'WARNING','L')
        endif
        call ReadTw(Rtw,Rtwi,NTwin,CellRefBlock(1,0),ichp)
        if(ichp.eq.0) NTwinOld=NTwin
        WizardMode=.true.
        WizardTitle=.true.
        go to 1500
      else if(CheckType.eq.EventASCII) then
        go to 1500
      else if(CheckType.eq.EventKey) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(.not.UzSePtal) then
          call EM9TrMatApply(nLblCell,nLblModFr,0,
     1                       TrDirCos(1,KRefBlock),ichp)
          if(ichp.ne.0) then
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1300
          endif
        endif
        if(nCrwHKLF5.gt.0) then
          if(CrwLogicQuest(nCrwHKLF5)) then
            HKLF5RefBlock(KRefBlock)=2
          else
            HKLF5RefBlock(KRefBlock)=1
          endif
        else
          HKLF5RefBlock(KRefBlock)=0
        endif
        if(nCrwImpOnlySat.gt.0) then
          ImportOnlySatellites(KRefBlock)=CrwLogicQuest(nCrwImpOnlySat)
        endif
        UseTrRefBlock(KRefBlock)=.not.
     1    MatRealEqUnitMat(TrRefBlock(1,KRefBlock),maxNDim,.001)
        if(EdwStateQuest(nEdwITwin).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwITwin,ITwRead(KRefBlock))
        else
          ITwRead(KRefBlock)=1
        endif
        if(nEdwModFr.gt.0.and.CreateNewRefBlock) then
          nEdw=nEdwModFr
          do i=1,maxNDim-3
            if(i.gt.NDimLim-3) then
              Veta=EdwStringQuest(nEdw)
              if(Veta.eq.' ') then
                if(ich.eq.0.and.KRefBlock.eq.1) then
                  write(Veta,'(i1,a2,
     1                  '' modulation vector not defined'')') i,nty(i)
                  go to 1800
                endif
              endif
              call FeQuestRealAFromEdw(nEdw,QuRefBlock(1,i,0))
              nEdw=nEdw+1
            endif
          enddo
        endif
        if(NDim95(KRefBlock).lt.maxNDim) then
          call FeQuestIntFromEdw(nEdwMMax,MMaxRefBlock(KRefBlock))
          call FeQuestRealAFromEdw(nEdwDiffSat,
     1                             DiffSatRefBlock(1,KRefBlock))
        endif
        call FeQuestRealFromEdw(nEdwScale,ScaleRefBlock(KRefBlock))
        if(UzTuByl.eq.0) StavVolani=StavVolani+10
        go to 9999
1800    call FeChybne(-1.,-1.,Veta,'Please complete the form.',
     1                SeriousError)
      endif
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(VolatHide) then
        call CrlMatEqHide
        VolatHide=.false.
      endif
      return
100   format('Cell parameters:',3f8.4,3f8.3)
101   format(i1,a2,' modulation vector',5x,3f7.4)
102   format(3(a1,f10.4),3(a1,f10.3))
      end
