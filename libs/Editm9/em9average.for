      subroutine EM9Average(KItwIn,KIqIn,KlicIn,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ihpp(6),ihmx(6),ihd(6),itr(6),icull(:),kpor(:),idiff(:),
     1          kpoint(:),hp(3),ihov(6),iporp(:),tp(:),dircosa(:,:,:),
     2          sj(3),soj(3),RIArO(:),RSArO(:)
      character*80 Veta,ShForm
      character*30 LabelMult
      character*25 LabelSlow,LabelFast
      character*23 LabelRef
      character*20 FormRefAve
      character*19 LabelSigma
      character*42 iven(mxl4)
      character*4  iven1
      character*1  Znak
      integer EM9SortIndex,EdwStateQuest,CrlCentroSymm,FromWhere,
     1        UseTabsIn
      logical :: CrwLogicQuest,lpom,Psal,lcull,tisk,ExistFile,
     1           FeYesNoHeader
      real RunCCDa(:)
      allocatable iporp,tp,icull,kpor,idiff,kpoint,DirCosA,RIArO,RSArO,
     1            RunCCDa
      save mxl34,ncsp,izp,Psal,FormRefAve,flimp,fcullp,ihov,ihov4,jcomm,
     1     RIArO,RSArO
      data iven/mxl4*' '/
      data LabelSlow,LabelFast/'The slowest varying index',
     1                         'The fastest varying index'/
      data LabelRef/'Reflections |I-I(ave)|>'/
      data LabelSigma/'Sigma(I(ave)) from:'/
      data LabelMult/'Multiply measured reflections:'/
      KItw=KItwIn
      KIq=KIqIn
      Klic=KlicIn
      FromWhere=AveFromEditM9
      go to 1100
      entry DRAverage(Key,KItwIn,ich)
      FromWhere=Key
      Klic=0
      KItw=KItwIn
      KIq=1
1100  if(Klic.eq.0) then
        LnInstab=0
        ncsp=2
        do i=1,NPhase
          KPhase=i
          if(CrlCentroSymm().le.0) then
            ncsp=1
            exit
          endif
        enddo
        if(FromWhere.ne.AveFromEditM9) then
          if(FromWhere.eq.AveFromManualCull.or.
     1       FromWhere.eq.AveFromRSViewer.or.
     2       FromWhere.eq.AveFromXShape.or.
     3       FromWhere.eq.AveForSemiEmpir) then
            if(FromWhere.eq.AveFromXShape.or.
     1         FromWhere.eq.AveForSemiEmpir) then
              if(FromWhere.eq.AveFromXShape) then
                Veta='Creating "eqv" file for X-shape'
                NDimUse=3
              else
                Veta='Creating "eqv" file for frame scale and/or '//
     1               'empirical absorption corrections'
                NDimUse=maxNDim
              endif
              n=2*nref95(KRefBlock)
              if(ExistFile(RefBlockFileName)) then
                call OpenFile(95,RefBlockFileName,'formatted','old')
              else
                call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
              endif
              if(ErrFlag.ne.0) go to 9999
              ShForm=FormatShelx
              write(ShForm(2:2),'(i1)') NDimUse
              i=idel(ShForm)
              ShForm(i:)=',f15.3)'
              RIMax=0.
              nobsa=0
            else
              Veta='Averaging reflections'
              n=2*NRefRead
            endif
            call FeFlowChartOpen(-1.,-1.,max(nint(float(n)*.01),10),n,
     1                           Veta,' ',' ')
          endif
          call SetRealArrayTo(rnum ,11,0.)
          call SetRealArrayTo(rden ,11,0.)
          call SetRealArrayTo(ronum,11,0.)
          call SetRealArrayTo(roden,11,0.)
          call SetRealArrayTo(rexpnum ,11,0.)
          call SetRealArrayTo(rexpden ,11,0.)
          call SetRealArrayTo(roexpnum,11,0.)
          call SetRealArrayTo(roexpden,11,0.)
          call SetIntArrayTo(nrefi ,11,0)
          call SetIntArrayTo(norefi,11,0)
          call SetIntArrayTo(nrefa ,11,0)
          call SetIntArrayTo(norefa,11,0)
          IndFastest(KDatBlock)=3
          IndSlowest(KDatBlock)=1
          go to 1600
        endif
        if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1     DifCode(KRefBlock).eq.IdSXD.or.
     2     DifCode(KRefBlock).eq.IdTopaz.or.
     3     DifCode(KRefBlock).eq.IdSCDLANL.or.
     4     DifCode(KRefBlock).eq.IdSENJU) then
          DataAve(KDatBlock)=0
          go to 9999
        endif
        if(.not.SilentRun) then
          DrawInstabGraph=.true.
          id=NextQuestId()
          if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1       TitleSingleCrystal(:idel(TitleSingleCrystal)))
          il=0
          Veta='Perform a%veraging'
          xpom=(WizardLength-FeTxLengthUnder(Veta)-CrwgXd-6.)*.5
          tpom=xpom+CrwgXd+6.
          do i=1,2
            il=il-9
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='Use %non-averaged data'
              nCrwAverage=CrwLastMade
            else
              nCrwNoAverage=CrwLastMade
            endif
            if((i.eq.1.and.DataAve(KDatBlock).gt.0).or.
     1         (i.eq.2.and.DataAve(KDatBlock).le.0)) then
              lpom=.true.
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
          enddo
          il=il-9
          call FeQuestLinkaMake(id,il)
          xpom=FeTxLength(LabelSlow)+20.
          pom=xpom
          il=il-9
          do i=1,3
            call FeQuestCrwMake(id,xpom,il,xpom-4.,il-9,indices(i),'C',
     1                          CrwgXd,CrwgYd,1,2)
            if(i.eq.1) then
              nCrwSlowFrom=CrwLastMade
            else if(i.eq.3) then
              nCrwSlowTo=CrwLastMade
            endif
            xpom=xpom+20.
          enddo
          xpom=5.
          il=il-9
          call FeQuestLblMake(id,xpom,il,LabelSlow,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblSlow=LblLastMade
          il=il-9
          call FeQuestLblMake(id,xpom,il,LabelFast,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblFast=LblLastMade
          xpom=pom
          do i=1,3
            call FeQuestCrwMake(id,xpom,il,xpom-4.,il,' ','C',CrwgXd,
     1                          CrwgYd,1,3)
            if(i.eq.1) then
              nCrwFastFrom=CrwLastMade
            else if(i.eq.3) then
              nCrwFastTo=CrwLastMade
            endif
            xpom=xpom+20.
          enddo
          il=il-9
          call FeQuestLinkaMake(id,il)
          nLine1=LinkaLastMade
          il=il-9
          tpom=5.
          Veta='%Full print'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwPrint=CrwLastMade
          Veta='Apply c%ulling'
          tpom=xpom+CrwXd+20.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwCull=CrwLastMade
          Veta='A%dd center of symmetry'
          tpom=xpom+CrwXd+20.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          nCrwCSym=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,AddCentrSymm(KDatBlock).gt.0)
          if(ncsp.ne.1) call FeQuestCrwDisable(CrwLastMade)
          il=il-9
          tpom=5.
          Veta='Apply 1/sig(I) %weights in averaging'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          call FeQuestCrwOpen(CrwLastMade,AveSigWt(KDatBlock).gt.0)
          nCrwSigWt=CrwLastMade
          il=il-9
          xpom=5.
          call FeQuestLblMake(id,xpom,il,LabelRef,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblPrint=LblLastMade
          xpom=FeTxLength(LabelRef)+xpom+5.
          dpom=40.
          tpom=xpom+dpom+5.
          Veta='*sig(I(ave)) will be %printed'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwPrint=EdwLastMade
          il=il-9
          xpomp=5.
          call FeQuestLblMake(id,xpomp,il,LabelRef,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblCull=LblLastMade
          Veta='*sig(I(ave)) will be %culled'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwCull=EdwLastMade
          il=il-9
          tpom=5.
          Veta='Displa%y graph sig(Icount)/sig(Istat)'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          call FeQuestCrwOpen(CrwLastMade,DrawInstabGraph)
          nCrwShowSigGraph=CrwLastMade
          il=il-9
          call FeQuestLinkaMake(id,il)
          nLine2=LinkaLastMade
          il=il-9
          tpom=5.
          ilb5=il
          call FeQuestLblMake(id,tpom,il,LabelSigma,'L','N')
          call FeQuestLblDisable(LblLastMade)
          nLblSigma=LblLastMade
          xpom=tpom+60.
          do i=1,3
            il=il-9
            if(i.eq.1) then
              Veta='P%oisson'
            else if(i.eq.2) then
              Veta='%Equivalents'
            else
              Veta='%Maximum'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,4)
            if(i.eq.1) then
              nCrwPoisson=CrwLastMade
            else if(i.eq.2) then
              nCrwEquivalent=CrwLastMade
            else
              nCrwMaximum=CrwLastMade
            endif
          enddo
          if(DifCode(KRefBlock).eq.IdKumaPD) then
            tpomm=tpom+WizardLength*.5
            call FeQuestLblMake(id,tpomm,ilb5,LabelMult,'L','N')
            call FeQuestLblDisable(LblLastMade)
            nLblMult=LblLastMade
            xpom=xpom+WizardLength*.5
            il=ilb5
            do i=1,3
              il=il-9
              if(i.eq.1) then
                Veta='use %all'
              else if(i.eq.2) then
                Veta='use f%irst'
              else
                Veta='use %last'
              endif
              call FeQuestCrwMake(id,tpomm,il,xpom,il,Veta,'L',CrwgXd,
     1                            CrwgYd,0,5)
              if(i.eq.1) then
                nCrwUseAll=CrwLastMade
              else if(i.eq.2) then
                nCrwUseFirst=CrwLastMade
              else
                nCrwUseLast=CrwLastMade
              endif
            enddo
          endif
          fcullp=20.
          flimp=5.
1400      xpom=5.
          if(CrwLogicQuest(nCrwAverage)) then
            nCrw=nCrwSlowFrom
            do i=1,3
              call FeQuestCrwOpen(nCrw,i.eq.IndSlowest(KDatBlock))
              nCrw=nCrw+1
            enddo
            call FeQuestLblOn(nLblSlow)
            call FeQuestLblOn(nLblFast)
            nCrw=nCrwFastFrom
            do i=1,3
              if(i.ne.IndSlowest(KDatBlock))
     1          call FeQuestCrwOpen(nCrw,i.eq.IndFastest(KDatBlock))
              nCrw=nCrw+1
            enddo
            call FeQuestCrwOpen(nCrwPrint,FLimPrint(KDatBlock).lt.0.)
            call FeQuestCrwOpen(nCrwCull,FLimCull(KDatBlock).gt.0.)
            if(ncsp.eq.1)
     1        call FeQuestCrwOpen(nCrwCSym,AddCentrSymm(KDatBlock).gt.0)
            if(FLimPrint(KDatBlock).ge.0.) then
              call FeQuestRealEdwOpen(nEdwPrint,FLimPrint(KDatBlock),
     1                                .false.,.false.)
              call FeQuestLblOn(nLblPrint)
            endif
            if(FLimCull(KDatBlock).ge.0.) then
              call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                                .false.,.false.)
              call FeQuestLblOn(nLblCull)
            endif
            call FeQuestLblOn(nLblSigma)
            nCrw=nCrwPoisson
            do i=1,3
              call FeQuestCrwOpen(nCrw,i.eq.SigIMethod(KDatBlock))
              nCrw=nCrw+1
            enddo
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              call FeQuestLblOn(nLblMult)
              nCrw=nCrwUseAll
              do i=1,3
                call FeQuestCrwOpen(nCrw,i.eq.MultAve(KDatBlock))
                nCrw=nCrw+1
              enddo
            endif
            call FeQuestCrwOpen(nCrwSigWt,AveSigWt(KDatBlock).gt.0)
            call FeQuestCrwOpen(nCrwShowSigGraph,DrawInstabGraph)
          else
            nCrw=nCrwSlowFrom
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            call FeQuestLblDisable(nLblSlow)
            call FeQuestLblDisable(nLblFast)
            nCrw=nCrwFastFrom
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            call FeQuestCrwDisable(nCrwPrint)
            call FeQuestCrwDisable(nCrwCull)
            if(ncsp.eq.1) call FeQuestCrwDisable(nCrwCSym)
            call FeQuestLblDisable(nLblPrint)
            call FeQuestEdwDisable(nEdwPrint)
            call FeQuestLblDisable(nLblCull)
            call FeQuestEdwDisable(nEdwCull)
            call FeQuestLblDisable(nLblSigma)
            call FeQuestCrwDisable(nCrwSigWt)
            call FeQuestCrwDisable(nCrwShowSigGraph)
            nCrw=nCrwPoisson
            do i=1,3
              call FeQuestCrwDisable(nCrw)
              nCrw=nCrw+1
            enddo
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              call FeQuestLblDisable(nLblMult)
              nCrw=nCrwUseAll
              do i=1,3
                call FeQuestCrwDisable(nCrw)
                nCrw=nCrw+1
              enddo
            endif
          endif
1500      call FeQuestEvent(id,ich)
          if(CheckType.eq.EventCrw) then
            if(CheckNumber.eq.nCrwAverage.or.
     1         CheckNumber.eq.nCrwNoAverage) then
              go to 1400
            else if(CheckNumber.ge.nCrwSlowFrom.and.
     1              CheckNumber.le.nCrwSlowTo)  then
              izn=CheckNumber-nCrwSlowFrom+1
              if(IndSlowest(KDatBlock).ne.izn) then
                call FeQuestCrwDisable(izn+nCrwFastFrom-1)
                call FeQuestCrwOpen(
     1            IndSlowest(KDatBlock)-1+nCrwFastFrom,
     2            IndFastest(KDatBlock).eq.izn)
                if(IndFastest(KDatBlock).eq.izn)
     1            IndFastest(KDatBlock)=IndSlowest(KDatBlock)
                IndSlowest(KDatBlock)=izn
              endif
            else if(CheckNumber.ge.nCrwFastFrom.and.
     1              CheckNumber.le.nCrwFastTo)  then
              IndFastest(KDatBlock)=CheckNumber-nCrwFastFrom+1
            else if(CheckNumber.eq.nCrwPrint) then
              if(CrwLogicQuest(CheckNumber)) then
                if(EdwStateQuest(nEdwPrint).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdwPrint,flimp)
                  call FeQuestEdwDisable(nEdwPrint)
                endif
                call FeQuestLblDisable(nLblPrint)
                FLimPrint(KDatBlock)=-1.
              else
                FLimPrint(KDatBlock)=flimp
                call FeQuestLblOn(nLblPrint)
                call FeQuestRealEdwOpen(nEdwPrint,FLimPrint(KDatBlock),
     1                                  .false.,.false.)
              endif
            else if(CheckNumber.eq.nCrwCull) then
              if(CrwLogicQuest(CheckNumber)) then
                FLimCull(KDatBlock)=fcullp
                call FeQuestLblOn(nLblCull)
                call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                                  .false.,.false.)
              else
                if(EdwStateQuest(nEdwCull).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdwCull,fcullp)
                  call FeQuestEdwDisable(nEdwCull)
                endif
                call FeQuestLblDisable(nLblCull)
                FLimCull(KDatBlock)=-1.
              endif
            endif
            go to 1500
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1500
          endif
          if(ich.eq.0) then
            if(CrwLogicQuest(nCrwAverage)) then
              DataAve(KDatBlock)=1
            else
              DataAve(KDatBlock)=0
              go to 9999
            endif
            if(CrwLogicQuest(nCrwPrint)) then
              FLimPrint(KDatBlock)=-1.
            else
              call FeQuestRealFromEdw(nEdwPrint,FLimPrint(KDatBlock))
            endif
            if(CrwLogicQuest(nCrwCull)) then
              call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
            else
              FLimCull(KDatBlock)=-1.
            endif
            if(CrwLogicQuest(nCrwSigWt)) then
              AveSigWt(KDatBlock)=1
            else
              AveSigWt(KDatBlock)=0
            endif
            if(CrwLogicQuest(nCrwPoisson)) then
              SigIMethod(KDatBlock)=1
            else if(CrwLogicQuest(nCrwEquivalent)) then
              SigIMethod(KDatBlock)=2
            else if(CrwLogicQuest(nCrwMaximum)) then
              SigIMethod(KDatBlock)=3
            endif
            if(DifCode(KRefBlock).eq.IdKumaPD) then
              if(CrwLogicQuest(nCrwUseAll)) then
                MultAve(KDatBlock)=1
              else if(CrwLogicQuest(nCrwUseFirst)) then
                MultAve(KDatBlock)=2
              else if(CrwLogicQuest(nCrwUseLast)) then
                MultAve(KDatBlock)=3
              endif
            endif
            if(ncsp.eq.1) then
              if(CrwLogicQuest(nCrwCSym)) then
                AddCentrSymm(KDatBlock)=1
                ncsp=2
              else
                AddCentrSymm(KDatBlock)=0
              endif
            endif
            DrawInstabGraph=CrwLogicQuest(nCrwShowSigGraph)
          endif
        endif
        if(ich.ne.0) go to 9999
1600    if(FromWhere.eq.AveFromRSViewer.or.
     1     FromWhere.eq.AveFromXShape.or.
     2     FromWhere.eq.AveForSemiEmpir) then
          ncull=0
          jcomm=1
          if(FromWhere.eq.AveFromRSViewer) then
            call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
          else
            call OpenFile(90,fln(:ifln)//'.eqv','formatted','unknown')
          endif
          go to 2500
        endif
        if(maxNDim.gt.3) then
          mxl34=3*(mxline-5)
        else
          mxl34=4*(mxline-5)
        endif
        FormRefAve='(3i4,f 9.1,f 7.1,a4)'
        if(maxNDim.gt.3) then
          n=20-2*maxNDim
          write(FormRefAve(2:2),102) maxNDim
          write(FormRefAve(7:8),106) n
          write(FormRefAve(13:14),106) n-2
          FormLabAve='(3(''   h   k   l'
          if(maxNDim.gt.3)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   m'
          if(maxNDim.gt.4)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   n'
          if(maxNDim.gt.5)
     1      FormLabAve=FormLabAve(:idel(FormLabAve))//'   p'
          Veta=''',  x,''I '',  x,''sig(I)'',3x)/)'
          write(Veta(3:4),106) n-2
          write(Veta(12:13),106) n-7
          FormLabAve=FormLabAve(:idel(FormLabAve))//Veta(:idel(Veta))
        else
          FormLabAve='(4(''   h   k   l      I    sig(I)   '')/)'
        endif
        npom=2
        do i=1,NPhase
          KPhase=i
          if(CrlCentroSymm().le.0) then
            npom=1
            go to 2120
          endif
        enddo
2120    if(AddCentrSymm(KDatBlock).gt.0.and.ncsp.eq.1) then
          call NewLn(2)
          write(lst,'(''Center of symmetry will be applied even if '',
     1                ''the space group is acentric''/)')
        endif
        if(AveSigWt(KDatBlock).gt.0) then
          call NewLn(2)
          write(lst,'(''Weighting by 1/sig(I) will be used in '',
     1                ''averaging''/)')
        endif
        call NewLn(2)
        if(FLimPrint(KDatBlock).ne.0.) then
          if(FLimPrint(KDatBlock).gt.0.) then
            write(lst,'(''Only reflections |I-I(ave)|>'',f5.1,
     1                  ''*sig(I(ave)) will be printed'')')
     2        FLimPrint(KDatBlock)
            write(lst,FormA1)
          else
            write(lst,'(''Full print averaged reflections'')')
            write(lst,FormA1)
          endif
          call newln(4)
          write(lst,'(
     1      ''Symbol +    means that for the relevant reflection '',
     2      '' 3*sig(I(ave)) < I(i)-I(ave) <  5*sig(I(ave))''/
     3      ''Symbol ++               -- " --                    '',
     4      '' 5*sig(I(ave)) < I(i)-I(ave) < 10*sig(I(ave))''/
     5      ''Symbol +++              -- " --                    '',
     6      ''10*sig(I(ave)) < I(i)-I(ave) < 20*sig(I(ave))''/
     7      ''Symbol ++++             -- " --                    '',
     8      ''20*sig(I(ave)) < I(i)-I(ave)'')')
          call newln(5)
          write(lst,'(
     1      ''Symbol -                -- " --                    '',
     2      '' 3*sig(I(ave)) < I(ave)-I(i) <  5*sig(I(ave))''/
     3      ''Symbol --               -- " --                    '',
     4      '' 5*sig(I(ave)) < I(ave)-I(i) < 10*sig(I(ave))''/
     5      ''Symbol ---              -- " --                    '',
     6      ''10*sig(I(ave)) < I(ave)-I(i) < 20*sig(I(ave))''/
     7      ''Symbol ----             -- " --                    '',
     8      ''20*sig(I(ave)) < I(ave)-I(i)'')')
          write(lst,FormA1)
        else
          write(lst,'(''Print averaged reflections suppressed'')')
          write(lst,FormA1)
        endif
        if(FLimCull(KDatBlock).gt.0.) then
          call NewLn(2)
          write(lst,'(''Reflections |I-I(ave)|>'',f5.1,
     1                ''*sig(I(ave)) will be culled'')')
     2      FLimCull(KDatBlock)
          write(lst,FormA1)
        endif
        if(FLimCull(KDatBlock).gt.0.) then
          ncull=0
        else
          ncull=-1
        endif
        if(NDimI(KPhase).eq.1.and.KCommen(KPhase).ne.0) then
          pom=NCommQProduct(1,KPhase)
          ihov4=NCommQProduct(1,KPhase)
          do j=1,3
            ihov(j)=nint(qu(j,1,1,KPhase)*pom)
          enddo
!          jcomm=2
          jcomm=1
        else
          jcomm=1
        endif
2500    itr(3)=IndFastest(KDatBlock)
        itr(1)=IndSlowest(KDatBlock)
        itr(2)=6-itr(1)-itr(3)
        do i=4,maxNDim
          itr(i)=i
        enddo
        izp=0
        if(FromWhere.eq.AveFromEditM9) then
          if(allocated(RIAr)) then
            NRefReadO=NRefRead
            allocate(RIArO(NRefReadO),RSArO(NRefReadO))
            call CopyVek(RIAr,RIArO,NRefReadO)
            call CopyVek(RSAr,RSArO,NRefReadO)
            deallocate(RIAr,RSAr)
          endif
          LnInstab=NextLogicNumber()
          call OpenFile(LnInstab,fln(:ifln)//'.m89','formatted',
     1                  'unknown')
          go to 9999
        endif
      endif
      n=0
      nn=0
      call SetIntArrayTo(ihmax,6,0)
      call SetIntArrayTo(ihmx,6,0)
      if(FromWhere.eq.AveFromEditM9) rewind 91
      KPhase=1
      ntw=0
3000  n=n+1
3030  izpet=0
      nn=nn+1
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromSGTest.or.
     2   FromWhere.eq.AveFromRSViewer) then
        if(nn.gt.NRefRead) go to 3200
        itw=0
        do i=1,NTwin
          if(HCondAr(i,nn).gt.0) then
            itw=i
            exit
          endif
        enddo
        if(itw.eq.0) go to 3030
        call IndUnPack(HCondAr(itw,nn),ihp,HCondLn,HCondMx,NDim(KPhase))
        if(FromWhere.eq.AveFromSGTest) then
          call indtr(ihp,TrMP,ih,NDim(KPhase))
        else
          call CopyVekI(ihp,ih,NDim(KPhase))
        endif
!        itw=1
        iq=kiq
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call DRGetReflectionFromM95(95,iend,ichp)
        if(ichp.ne.0) go to 9000
        if(iend.ne.0) go to 3200
        if(no.le.0.or.iflg(1).lt.0.or.iflg(2).ne.1) then
          izpet=1
          go to 3040
        endif
        if(FromWhere.eq.AveFromXShape) then
          do i=4,NDim(KPhase)
            if(ih(i).ne.0) then
              izpet=1
              go to 3040
            endif
          enddo
        endif
        ri=ri*corrf(1)
        RIMax=max(RIMax,ri)
        itw=1
        iq=kiq
      else
        read(91,format91,end=3200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar
        if(nxx.gt.1) nxx=0
      endif
3040  if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) then
        call FeFlowChartEvent(izp,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9000
        endif
        if(itw.lt.0) then
          ntw=ntw+1
          go to 3030
        else if(ntw.gt.0) then
          ntw=0
          go to 3030
        endif
        if(iq.ne.kiq.or.itw.ne.KItw.or.izpet.ne.0) go to 3030
      endif
      if(FromWhere.ne.AveFromEditM9.and.FromWhere.ne.AveFromXShape.and.
     1   FromWhere.ne.AveForSemiEmpir) then
        RIAr(n)=RIAr(nn)
        RSAr(n)=RSAr(nn)
        ICullAr(n)=ICullAr(nn)
        RefBlockAr(n)=RefBlockAr(nn)
        RunCCDAr(n)=RunCCDAr(nn)
      endif
      do i=1,maxNDim
        ihmax(i)=max(ihmax(i),iabs(ih(i)))
      enddo
      do jj=1,jcomm
        if(jj.eq.2) then
          if(iabs(ih(4)).eq.ihov4/2) then
            izn=isign(1,ih(4))
            do i=1,3
              ih(i)=ih(i)+izn*ihov(i)
            enddo
            ih(4)=ih(4)-izn*ihov4
          else
            cycle
          endif
        endif
        do 3100i=1,NSymmN(KPhase)
          do j=1,ncsp
            if(j.eq.1) then
              ii=i
            else
              ii=-i
            endif
            call GetSymIndRespectTwin(ih,ihp,itr,KItw,ii,ncsp,ichp)
            if(ichp.ne.0) cycle
            if(jcomm.gt.1) then
              if(iabs(ihp(4)).gt.ihov4/2) then
                izn=isign(1,ihp(4))
                do ii=1,3
                  ihp(ii)=ihp(ii)+izn*ihov(itr(ii))
                enddo
                ihp(4)=ihp(4)-izn*ihov4
              endif
            endif
            do k=1,maxNDim
              ihmx(k)=max(ihmx(k),iabs(ihp(k)))
            enddo
          enddo
3100    continue
      enddo
      go to 3000
3200  n=n-1
      if(n.le.0) then
        if(FromWhere.eq.AveFromRSViewer) then
          call CloseIfOpened(91)
        else
          call CloseIfOpened(90)
        endif
        go to 9999
      endif
      allocate(irec(n),irecp(n),ireca(n),ipor(n),iporp(n),tp(n))
      if(FromWhere.eq.AveFromEditM9.or.FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        allocate(RIAr(n),RSAr(n))
        if(FromWhere.eq.AveFromEditM9) then
          allocate(ICullAr(n),RefBlockAr(n))
          rewind 91
        else
          if(ExistFile(RefBlockFileName)) then
            rewind 95
          else
            call CloseIfOpened(95)
            call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
          endif
          allocate(ICullAr(n),RefBlockAr(n),inca(n),dircosa(3,2,n),
     1             RunCCDa(n))
        endif
      endif
      call SetIntArrayTo(ihd,6,1)
      j=1
      do i=1,NDim(KPhase)
        ihd(i)=2*ihmx(i)+1
        if(imax/j.lt.ihd(i)) go to 8000
        j=j*ihd(i)
      enddo
      ntw=0
      n=0
      nn=0
3300  n=n+1
3330  izpet=0
      nn=nn+1
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromSGTest.or.
     2   FromWhere.eq.AveFromRSViewer) then
        if(nn.gt.NRefRead) go to 3500
        itw=0
        do i=1,NTwin
          if(HCondAr(i,nn).gt.0) then
            itw=i
            exit
          endif
        enddo
        if(itw.eq.0) go to 3330
        call IndUnPack(HCondAr(itw,nn),ihp,HCondLn,HCondMx,NDim(KPhase))
        if(FromWhere.eq.AveFromSGTest) then
          call indtr(ihp,TrMP,ih,NDim(KPhase))
        else
          call CopyVekI(ihp,ih,NDim(KPhase))
        endif
!        itw=1
        iq=kiq
        if(ICullAr(n).le.1) then
          nxx=ICullAr(nn)
        else
          ICullAr(nn)=0
          nxx=0
        endif
        tbar=0.
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call DRGetReflectionFromM95(95,iend,ichp)
        if(ichp.ne.0) go to 9000
        if(iend.ne.0) go to 3500
        if(no.le.0.or.iflg(1).lt.0.or.iflg(2).ne.1) then
          izpet=1
          go to 3340
        endif
        if(FromWhere.eq.AveFromXShape) then
          do i=4,NDim(KPhase)
            if(ih(i).ne.0) then
              izpet=1
              go to 3340
            endif
          enddo
        endif
        ri=ri*corrf(1)
        rs=rs*corrf(1)
        itw=1
        iq=kiq
        tbar=0.
      else
        read(91,format91,end=3500)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar,(pom,i=1,8),NoRefBlock
        if(nxx.gt.1) nxx=0
      endif
3340  if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) then
        call FeFlowChartEvent(izp,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9000
        endif
        if(itw.lt.0) then
          ntw=ntw+1
          go to 3330
        else if(ntw.gt.0) then
          ntw=0
          go to 3330
        endif
        if(iq.ne.kiq.or.itw.ne.KItw.or.izpet.ne.0) go to 3330
        if(FromWhere.eq.AveFromEditM9) RefBlockAr(n)=NoRefBlock
      endif
      if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromXShape.or.
     2   FromWhere.eq.AveForSemiEmpir) then
        RIAr(n)=ri
        RSAr(n)=rs
        if(FromWhere.eq.AveFromXShape.or.
     1     FromWhere.eq.AveForSemiEmpir) then
          nxx=0
          do j=1,2
            do i=1,3
              dircosa(i,j,n)=dircos(i,j)*rcp(i,1,KPhase)
            enddo
            call multm(MetTens(1,1,KPhase),dircosa(1,j,n),sj,3,3,1)
            pom=sqrt(scalmul(dircosa(1,j,n),sj))
            if(pom.gt.0.) then
              pom=1./pom
            else
              pom=1.
            endif
            do i=1,3
              dircosa(i,j,n)=dircosa(i,j,n)*pom
            enddo
          enddo
          RunCCDa(n)=expos
        endif
      endif
      tp(n)=tbar
      do i=1,NDim(KPhase)
        ihp(i)=ih(itr(i))
      enddo
      mx=IndPack(ihp,ihd,ihmx,maxNDim)
      irec(n)=mx
      ICullAr(n)=nxx
      mx =0
      mxx=0
      kk=0
      do k=1,3-ncsp
        if(k.eq.2) call IntVectorToOpposite(ih,ih,maxNDim)
        do jj=1,jcomm
          if(jj.eq.2) then
            if(iabs(ih(4)).eq.ihov4/2) then
              izn=isign(1,ih(4))
              do i=1,3
                ih(i)=ih(i)+izn*ihov(i)
              enddo
              ih(4)=ih(4)-izn*ihov4
            else
              cycle
            endif
          endif
          do i=1,NSymmN(KPhase)
            do j=1,ncsp
              if(j.eq.1) then
                ii=i
              else
                ii=-i
               endif
              call GetSymIndRespectTwin(ih,ihp,itr,KItw,ii,ncsp,ichp)
              if(ichp.ne.0) cycle
              if(jcomm.gt.1) then
                if(iabs(ihp(4)).gt.ihov4/2) then
                  izn=isign(1,ihp(4))
                  do ii=1,3
                    ihp(ii)=ihp(ii)+izn*ihov(itr(ii))
                  enddo
                  ihp(4)=ihp(4)-izn*ihov4
                endif
              endif
              mxq=IndPack(ihp,ihd,ihmx,maxNDim)
              if(k.eq.1) then
                if(mxq.gt.mxx) then
                  mxx=mxq
                  kk=k
                endif
                if(mxq.gt.mx ) mx =mxq
              else
                if(mxq.gt.mxx) then
                  mxx=mxq
                  kk=k
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      irecp(n)=mx
      ireca(n)=2*mxx+(kk-1)
      go to 3300
3500  n=n-1
      if(FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        pom=99999./RIMax
        if(pom.lt.1.) then
          do i=1,n
            riar(i)=riar(i)*pom
            rsar(i)=rsar(i)*pom
          enddo
          RIMax=RIMax*pom
        endif
        RIMax=RIMax*.01
      endif
      call indexx(n,ireca,ipor)
      if(FromWhere.eq.AveFromEditM9.or.
     1   FromWhere.eq.AveFromManualCull.or.
     2   FromWhere.eq.AveFromSGTest) then
        lines=line
        if(maxNDim.eq.3) then
          mxln=mxl34-(line-3)*4
        else
          mxln=mxl34-(line-3)*3
        endif
        Psal=.false.
        line=0
      endif
      kk=0
      NCull=0
      if(FromWhere.ne.AveFromEditM9) then
        NAve=0
        NTotallyCulled=0
      endif
      mxg=0
      incmax=0
      nuni=0
4000  lcull=.false.
      kp=kk+1
      if(kp.gt.n) go to 5000
      kn=0
      j=ipor(kp)
      irecpHledany=irecp(j)
      call IndUnPack(irecpHledany,ihpp,ihd,ihmx,maxNDim)
      do i=1,maxNDim
        ihp(itr(i))=ihpp(i)
      enddo
4050  kk=kk+1
      if(kk.gt.n) go to 5000
      if(irecp(ipor(kk)).eq.irecpHledany) then
        kn=kn+1
        if(kk.lt.n) go to 4050
      else
        kk=kk-1
      endif
      if(kn.gt.mxg) then
        if(allocated(KPor)) deallocate(KPor,ICull,KPoint,IDiff)
        allocate(KPor(kn),ICull(kn),KPoint(kn),IDiff(kn))
        mxg=kn
      endif
      call SetIntArrayTo(ICull,kn,0)
      do i=1,kn
        KPoint(i)=IPor(kp+i-1)
        ICull(i)=ICullAr(KPoint(i))
        if(ICull(i).gt.0) NCull=NCull+1
      enddo
      if(MultAve(KDatBlock).gt.1) then
        do i=1,kn-1
          ki=KPoint(i)
          if(ki.le.0) cycle
          do j=i+1,kn
            kj=KPoint(j)
            if(kj.le.0) cycle
            if(irec(ki).eq.irec(kj)) then
              if(MultAve(KDatBlock).eq.2) then
                KPoint(j)=-kj
              else
                KPoint(i)=-ki
                exit
              endif
            endif
          enddo
        enddo
      endif
4100  s1=0.
      s2=0.
      s4=0.
      s5=0.
      npr=0
      DiffMax=0.
      jprv=0
      do i=1,kn
        j=KPoint(i)
        if(j.le.0) cycle
        if(s2.le.0.) jprv=j
        if(icull(i).ne.0) cycle
        if(AveSigWt(KDatBlock).gt.0) then
          if(RSAr(j).gt.0.) then
            wt=1./RSAr(j)
          else if(RIAr(j).gt.0.) then
            wt=1./sqrt(RIAr(j))
          else
            wt=0.
          endif
        else
          wt=1.
        endif
        s1=s1+wt*RIAr(j)
        s2=s2+wt
        s4=s4+(wt*RSAr(j))**2
        s5=s5+wt*tp(j)
        npr=npr+1
      enddo
      if(npr.gt.1) then
        s1=s1/s2
        s4=sqrt(s4)/s2
        s5=s5/s2
        prumer=.true.
        s2=0.
        s3=0.
        do i=1,kn
          j=KPoint(i)
          if(j.gt.0) then
            if(AveSigWt(KDatBlock).gt.0) then
              if(RSAr(j).gt.0.) then
                wt=1./RSAr(j)
              else if(RIAr(j).gt.0.) then
                wt=1./sqrt(RIAr(j))
              else
                wt=0.
              endif
            else
              wt=1.
            endif
            pom=RIAr(j)-s1
            Diff=abs(pom)
            IDiff(i)=nint(pom*1000.)
            if(icull(i).eq.0) then
              s2=s2+wt
              s3=s3+wt*Diff**2
            endif
          endif
        enddo
        call indexx(kn,IDiff,kpor)
        pom=sqrt(s3/(s2*float(npr-1)))
        if(LnInstab.gt.0) write(LnInstab,'(3i4,3f9.1,i5)')
     1    (ihp(i),i=1,3),s1,s4,pom,npr
        if(SigIMethod(KDatBlock).eq.1) then
          s3=s4
        else if(SigIMethod(KDatBlock).eq.2) then
          s4=s4*.5
          if(pom.lt.s4) then
            s3=s4
          else
            s3=pom
          endif
        else
          s3=max(pom,s4)
        endif
        if(s3.le.0.) then
          if(s1.gt.0.) then
            s3=FLimPrint(KDatBlock)*s1
          else
            s3=.1
          endif
          s4=s3
        endif
        tisk=.false.
        do i=1,kn
          if(icull(i).ne.0) cycle
          j=KPoint(i)
          if(j.gt.0) then
            Diff=abs(RIAr(j)-s1)
            tisk=tisk.or.(Diff.gt.FLimPrint(KDatBlock)*s3)
            Diff=Diff/s3
            if(Diff.gt.DiffMax) then
              DiffMax=Diff
              kmax=i
            endif
          else
            Tisk=.true.
          endif
        enddo
        if(FLimCull(KDatBlock).gt.0..and.
     1     DiffMax.gt.FLimCull(KDatBlock).and.npr.gt.2) then
          icull(kmax)=2
          if(FromWhere.eq.AveFromManualCull.or.
     1       FromWhere.eq.AveFromSGTest.or.
     2       FromWhere.eq.AveFromEditM9) ICullAr(KPoint(kmax))=2
          lcull=.true.
          ncull=ncull+1
          if(LnInstab.gt.0) backspace(LnInstab)
          go to 4100
        endif
        if((FromWhere.eq.AveFromEditM9.or.
     1      FromWhere.eq.AveFromManualCull.or.
     2      FromWhere.eq.AveFromSGTest).and.
     3     (tisk.or.lcull.or.FLimPrint(KDatBlock).lt.0.)) then
          line=line+1
          write(iven(line),FormRefAve)(ihp(k),k=1,maxNDim),s1,s3,
     1                                '    '
          do k=1,4
            if(iven(line)(k:k).eq.' ') iven(line)(k:k)='>'
          enddo
          if(line.eq.mxln) then
            call EM9OutSta(iven)
            call newpg(0)
            line=0
            Psal=.true.
            mxln=mxl34
          endif
          do i=1,kn
            j=KPoint(kpor(i))
            ja=iabs(j)
            pom=abs(RIAr(ja)-s1)/s3
            if(RIAr(ja).gt.s1) then
              Znak='+'
            else
              Znak='-'
            endif
            if(j.le.0) then
              iven1='Skip'
            else if(icull(kpor(i)).ne.0) then
              iven1='Cull'
            else
              iven1=' '
              if(pom.gt. 3.) iven1=Znak
              if(pom.gt. 5.) iven1=iven1(:idel(iven1))//Znak
              if(pom.gt.10.) iven1=iven1(:idel(iven1))//Znak
              if(pom.gt.20.) iven1=iven1(:idel(iven1))//Znak
            endif
            l=irec(ja)
            if(l.le.0) cycle
            call IndUnPack(l,ihpp,ihd,ihmx,maxNDim)
            do j=1,maxNDim
              ih(itr(j))=ihpp(j)
            enddo
            line=line+1
            write(iven(line),FormRefAve)(ih(l),l=1,maxNDim),
     1                                   RIAr(ja),RSAr(ja),iven1
            if(line.eq.mxln) then
              Psal=.true.
              call EM9OutSta(iven)
              call newpg(0)
              line=0
              mxln=mxl34
            endif
          enddo
          line=line+1
          iven(line)=' '
          if(line.eq.mxln) then
            Psal=.true.
            call EM9OutSta(iven)
            call newpg(0)
            line=0
            mxln=mxl34
          endif
        else if(FromWhere.eq.AveFromXShape.or.
     1          FromWhere.eq.AveForSemiEmpir) then
          do i=1,kn
            if(icull(kpor(i)).ne.0) cycle
            j=KPoint(kpor(i))
            ja=iabs(j)
            if(FromWhere.eq.AveFromXShape) then
              if(CutMaxXShape.gt.0..and.
     1           riar(ja).gt.CutMaxXShape*rimax) go to 4000
              if(CutMinXShape.gt.0..and.
     1           riar(ja).lt.CutMinXShape*rsar(ja)) go to 4000
            else
              if(CutMaxAve(KRefBlock).gt.0..and.
     1           riar(ja).gt.CutMaxAve(KRefBlock)*rimax) go to 4000
              if(CutMinAve(KRefBlock).gt.0..and.
     1           riar(ja).lt.CutMinAve(KRefBlock)*rsar(ja)) go to 4000
            endif
          enddo
          inc=0
          nuni=nuni+1
          do 4070i=1,kn
            if(icull(kpor(i)).ne.0) cycle
            j=KPoint(kpor(i))
            ja=iabs(j)
            if(FromWhere.eq.AveForSemiEmpir) then
              call IndUnPack(irec(ja),ihpp,ihd,ihmx,maxNDim)
              do k=1,maxNDim
                ihp(itr(k))=ihpp(k)
              enddo
            endif
            if(FromWhere.eq.AveFromXShape) then
              call multm(MetTens(1,1,KPhase),dircosa(1,1,ja),soj,3,3,1)
              call multm(MetTens(1,1,KPhase),dircosa(1,2,ja),sj ,3,3,1)
              do k=1,i-1
                if(icull(kpor(k)).ne.0) cycle
                m=KPoint(kpor(k))
                ma=iabs(m)
                if(scalmul(dircosa(1,1,ma),soj).gt..999.and.
     1             scalmul(dircosa(1,2,ma), sj).gt..999) go to 4070
              enddo
            endif
            write(90,ShForm)(ihp(l),l=1,NDimUse),riar(ja),rsar(ja),1,
     1                      (-dircosa(l,1,ja)/rcp(l,1,KPhase),
     2                        dircosa(l,2,ja)/rcp(l,1,KPhase),l=1,3),
     3                       RunCCDa(ja)
            nobsa=nobsa+1
            inc=inc+1
4070      continue
          if(inc.eq.1) then
            nobsa=nobsa-1
            nuni=nuni-1
            backspace 90
          else
            inca(nuni)=inc
            incmax=max(inc,incmax)
          endif
          go to 4000
        endif
      else if(npr.gt.0) then
       if(FromWhere.eq.AveFromXShape.or.
     1    FromWhere.eq.AveForSemiEmpir) go to 4000
        s1=RIAr(jprv)
        s3=RSAr(jprv)
        s5=tp(jprv)
        kpor(1)=1
      else
       if(FromWhere.eq.AveFromXShape.or.
     1    FromWhere.eq.AveForSemiEmpir) go to 4000
        s1=0.
        s3=1.
        kpor(1)=1
      endif
      mm=EM9SortIndex(ihp)
      call FromIndSinthl(ihp,hp,sinthl,sinthlq,1,0)
      if(FromWhere.ne.AveFromEditM9) then
        if(NAve+1.gt.NAveMax) call ReallocateDRAve(1000,0)
        if(kn.gt.NAveRedMax) call ReallocateDRAve(0,max(10,kn))
        NAve=NAve+1
        if(npr.lt.1) NTotallyCulled=NTotallyCulled+1
        do i=1,kn
          j=KPoint(i)
          if(j.le.0) cycle
          Ave2Org(i,NAve)=j
        enddo
        HCondAveAr(NAve)=IndPack(ihp,HCondLn,HCondMx,NDim(KPhase))
        RIAveAr(NAve)=s1
        RSAveAr(NAve)=s3
        NAveRedAr(NAve)=kn
        NAveRed=max(NAveRed,kn)
        DiffAveAr(NAve)=DiffMax
      endif
      if(npr.gt.1) then
        do i=1,kn
          j=KPoint(i)
          if(j.le.0) cycle
          if(icull(i).eq.0) then
            pom=RIAr(j)
            apom=abs(pom-s1)
            rnum(1)=rnum(1)+apom
            rden(1)=rden(1)+pom
            if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.))
     1        then
              ronum(1)=ronum(1)+apom
              roden(1)=roden(1)+pom
            endif
            if(maxNDim.gt.3) then
              mmp=min(mm,10)
              mmMax=max(mmp,mmMax)
              rnum(mmp)=rnum(mmp)+apom
              rden(mmp)=rden(mmp)+pom
              nrefi(mmp)=nrefi(mmp)+1
              if(i.eq.1) nrefa(mmp)=nrefa(mmp)+1
              if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.))
     1          then
                ronum(mmp)=ronum(mmp)+apom
                roden(mmp)=roden(mmp)+pom
                norefi(mmp)=norefi(mmp)+1
                if(i.eq.1) norefa(mmp)=norefa(mmp)+1
              endif
            endif
            call EM9AverageStatRInt(sinthl,mmp,s1,s3,apom,pom,i)
          endif
          if(FromWhere.ne.AveFromEditM9) Ave2Org(i,NAve)=j
        enddo
      else if(npr.gt.0) then
        if(maxNDim.gt.3) then
          mmp=min(mm,10)
          mmMax=max(mmp,mmMax)
          nrefa(mmp)=nrefa(mmp)+1
          nrefi(mmp)=nrefi(mmp)+1
          if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
            norefa(mmp)=norefa(mmp)+1
            norefi(mmp)=norefi(mmp)+1
          endif
          call EM9AverageStatRInt(sinthl,mmp,s1,s3,-1.,-1.,1)
        endif
      else
        go to 4000
      endif
      if(maxNDim.gt.3) then
        rexpnum(mmp)=rexpnum(mmp)+s3
        rexpden(mmp)=rexpden(mmp)+s1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          roexpnum(mmp)=roexpnum(mmp)+s3
          roexpden(mmp)=roexpden(mmp)+s1
        endif
      endif
      call EM9AverageStatrexp(sinthl,mmp,s1,s3)
      if(FromWhere.eq.AveFromEditM9) then
        write(92,format91)(ihp(k),k=1,maxNDim),s1,s3,kiq,0,KItw,s5
      else if(FromWhere.eq.AveFromRSViewer) then
        write(91,Format83e)(ihp(l),l=1,NDim(KPhase)),s1*SimScale,
     1                                               s1*SimScale,
     2                                               s3*SimScale
      endif
      go to 4000
5000  if(FromWhere.eq.AveFromRSViewer) then
        call CloseIfOpened(91)
      else if(FromWhere.eq.AveFromXShape.or.
     1        FromWhere.eq.AveForSemiEmpir) then
        call CloseIfOpened(95)
        if(FromWhere.eq.AveFromXShape) then
          npom=NRefMaxXShape
        else
          npom=NRefMaxAve(KRefBlock)
        endif
        if(npom.gt.0.and.nuni.gt.npom) then
          ln=NextLogicNumber()
          open(ln,file=fln(:ifln)//'.l90')
          inc=incmax
          n=0
5050      i=0
5100      i=i+1
          if(inca(i).eq.inc) then
            inca(i)=-inca(i)
            n=n+1
            if(n.eq.npom) go to 5200
          endif
          if(i.ge.nuni) then
            inc=inc-1
            go to 5050
          else
            go to 5100
          endif
5200      rewind 90
5300      nobsa=0
          do i=1,nuni
            iz=inca(i)
            do j=1,iabs(iz)
              read(90,ShForm,end=5500)(ih(l),l=1,NDimUse),riar(j),
     1                                 rsar(j),m,dircos,RunCCDP
              if(iz.lt.0) then
                write(ln,ShForm)(ih(l),l=1,NDimUse),riar(j),rsar(j),m,
     1                           dircos,RunCCDP
                nobsa=nobsa+1
              endif
            enddo
          enddo
          nuni=npom
5500      close(90)
          close(ln)
          call MoveFile(fln(:ifln)//'.l90',fln(:ifln)//'.eqv')
        endif
        call CloseIfOpened(90)
        if(NUni.lt.10) then
          NInfo=1
          TextInfo(1)='The number of unique reflections smaller then '//
     1                '10.'
          if(FeYesNoHeader(-1.,YBottomMessage,'Do you want to repeat '//
     1                     'the selection procedure?',0)) then
            go to 9990
          else
            go to 9000
          endif
        else
          if(CovTest.gt.0.and.FromWhere.eq.AveForSemiEmpir) then
            call DRTestFrScCoverage(n,m)
            if(n.gt.0) then
              NInfo=2
              TextInfo(1)='The number of frames for one scale does '//
     1                    'not allow refining of scale factors.'
              TextInfo(2)='Please enlarge this parametr and try again.'
              call FeInfoOut(-1.,-1.,'WARNING','L')
              go to 9990
            else if(CovTest.eq.1) then
              NInfo=1
              TextInfo(1)='The number of frames for one scale allows '//
     1                    'refining scale of factors.'
              call FeInfoOut(-1.,-1.,'INFORMATION','L')
              go to 9990
            endif
          else
            NUniXShape=NUni
            NAllXShape=NObsA
          endif
        endif
      else
        if(line.gt.0) then
          i=line
          call EM9OutSta(iven)
          if(maxNDim.eq.3) then
            if(mxln.eq.mxl34) then
              line=(i-1)/4+6
            else
              line=lines+(i-1)/4+3
            endif
          else
            if(mxln.eq.mxl34) then
              line=(i-1)/3+6
            else
              line=lines+(i-1)/3+3
            endif
          endif
        else
          if(Psal) then
            line=3
          else
            line=lines
          endif
        endif
      endif
      if(FromWhere.eq.AveFromEditM9.and..not.SilentRun.and.KITw.eq.1)
     1  then
        MaxRefBlockAr=0
        NRefBlockAr=0
        do IRefBlock=1,NRefBlock
          if(RefDatCorrespond(IRefBlock).eq.1) then
            MaxRefBlockAr=max(MaxRefBlockAr,IRefBlock)
            NRefBlockAr=NRefBlockAr+1
          endif
        enddo
        call EM9ManualCullUpdate(1)
      endif
      go to 9999
8000  call FeChybne(-1.,-1.,'diffraction indices are too large to be '//
     1              'sorted and averaged.',' ',SeriousError)
9000  ich=1
      ErrFlag=0
      go to 9999
9990  ich=-1
9999  if(allocated(irec)) deallocate(irec,irecp,ireca,ipor,iporp,tp)
      if(allocated(KPor)) deallocate(KPor,ICull,KPoint,IDiff)
      if(FromWhere.eq.AveFromEditM9.or.FromWhere.eq.AveFromXShape.or.
     1   FromWhere.eq.AveForSemiEmpir) then
        if(allocated(RIAr)) deallocate(RIAr,RSAr)
        if(allocated(inca)) deallocate(inca,dircosa,RunCCDa)
        if(allocated(ICullAr)) deallocate(ICullAr,RefBlockAr)
        if(allocated(RIArO).and.Klic.ne.0) then
          allocate(RIAr(NRefReadO),RSAr(NRefReadO))
          call CopyVek(RIArO,RIAr,NRefReadO)
          call CopyVek(RSArO,RSAr,NRefReadO)
          deallocate(RIArO,RSArO)
          NRefRead=NRefReadO
        endif
      endif
      if(FromWhere.eq.AveFromManualCull.or.
     1   FromWhere.eq.AveFromRSViewer.or.
     2   FromWhere.eq.AveFromXShape.or.
     3   FromWhere.eq.AveForSemiEmpir.or.
     4   FromWhere.eq.AveFromRSViewer) call FeFlowChartRemove
      return
100   format(3i4,3f9.1)
102   format(i1)
106   format(i2)
      end
