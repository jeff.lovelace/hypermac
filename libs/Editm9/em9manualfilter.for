      subroutine EM9ManualFilter(Key)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*80 Veta
      integer SatInd,SatIndMax,EM9SortIndex
      logical CrwLogicQuest,lpom
      if(Key.eq.0) then
        ReflUse=ReflUseAll
      else
        SatIndMax=0
        do i=1,NAve
          if(DisplaySelect.ne.DisplayAll) then
            if(DisplaySelect.eq.DisplayObserved) then
              if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
            else
              n=0
              do j=1,NAveRedAr(i)
                m=Ave2Org(j,i)
                if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1            then
                  go to 1120
                else if(ICullAr(m).eq.0) then
                  n=n+1
                endif
              enddo
              if(DisplaySelect.eq.DisplayCulled) then
                cycle
              else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1                 n.eq.2).or.
     2                (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
                go to 1120
              else
                cycle
              endif
            endif
          endif
1120      k=HCondAveAr(i)
          call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
          SatIndMax=max(SatIndMax,EM9SortIndex(ih)-2)
        enddo
        SatInd=max(ReflUse,1)
        SatInd=min(SatInd,SatIndMax)
        id=NextQuestId()
        xqd=300.
        il=4
        Veta='Reflections used in refinement:'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=0
        xpom=5.
        tpom=xpom+CrwXd+10.
        ichk=1
        igrp=1
        Veta='%All reflections'
        do i=-2,1
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          if(i.eq.-2) then
            nCrwSelRefFirst=CrwLastMade
            Veta='%Main reflections'
          else if(i.eq.-1) then
            Veta='All %satellites'
          else if(i.eq.0) then
            Veta='S%pecified satellites'
          else if(i.eq.1) then
            nCrwSelRefLast=CrwLastMade
          endif
          if(i.le.0) then
            lpom=i.eq.ReflUse
          else
            lpom=ReflUse.gt.0
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if((i.eq.0.and.SatIndMax.le.0).or.
     1       (i.eq.1.and.SatIndMax.le.1))
     2      call FeQuestCrwDisable(CrwLastMade)
        enddo
        tpom=tpom+FeTxLengthUnder(Veta)+5.
        Veta='=>'
        xpom=tpom+FeTxLengthUnder(Veta)+7.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSatInd=EdwLastMade
1400    if(CrwLogicQuest(nCrwSelRefLast)) then
          call FeQuestIntEdwOpen(nEdwSatInd,SatInd,.false.)
          call FeQuestEudOpen(nEdwSatInd,1,2,1,0.,0.,0.)
        else
          call FeQuestEdwClose(nEdwSatInd)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw) then
          go to 1400
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          do nCrw=nCrwSelRefFirst,nCrwSelRefLast
            if(CrwLogicQuest(nCrw)) then
              ReflUse=ReflUseAll+nCrw-nCrwSelRefFirst
              exit
            endif
          enddo
          if(ReflUse.eq.1) call FeQuestIntFromEdw(nEdwSatInd,ReflUse)
        endif
        call FeQuestRemove(id)
      endif
9999  return
      end
