      subroutine EM9SigCountSigStat
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(4) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot'/)
      integer ih(6)
      real RMP(4),PS(2),Par(2),der(2),diag(2),xpp(3),xo(3),xp(2),yp(2)
      real, allocatable :: SigCount(:),SigCountN(:),SigStat(:),RiSt(:)
      Tiskne=.false.
      if(LnInstab.le.0) go to 9000
      rewind LnInstab
      nave=2
      n=0
1100  read(LnInstab,100,end=1200) ih(1:3),pom,pomc,poms,npr
      if(npr.le.nave.or.pom.le.0.) go to 1100
      n=n+1
      go to 1100
1200  rewind LnInstab
      if(n.lt.10) go to 9000
      allocate(SigCount(n),SigCountN(n),SigStat(n),RiSt(n))
      j=0
      PMax=0.
      SMax=0.
1300  read(LnInstab,100,end=1400) ih(1:3),pom,pomc,poms,npr
      if(npr.le.nave.or.pom.le.0.) go to 1300
      if(j.gt.n) go to 1400
      j=j+1
      RiSt(j)=pom
      SigCount(j)=pomc
      SigStat(j)=poms
      if(poms.gt.SMax) SMax=poms
      PMax=max(SigCount(j),SigStat(j),PMax)
      go to 1300
1400  close(LnInstab)
      PS=0.
      RMP=0.
      nref=1
      do i=1,n
        wt=1.
        der(1)=RISt(i)**2
        der(2)=RISt(i)
        FI=SigStat(i)**2-SigCount(i)**2
        m=0
        do ii=1,nref
          PS(ii)=PS(ii)+wt*FI*der(ii)
          do jj=1,ii
            m=m+1
            RMP(m)=RMP(m)+wt*der(ii)*der(jj)
          enddo
        enddo
      enddo
      ij=0
      do i=1,nref
        ij=ij+i
        Diag(i)=1./sqrt(RMP(ij))
      enddo
      par=0.
      call znorm(RMP,Diag,nref)
      call smi(RMP,nref,ising)
      call znorm(RMP,Diag,nref)
      call Nasob(RMP,PS,Par,nref)
      EM9Instab(KDatBlock)=sign(sqrt(abs(Par(1))/2.),Par(1))
      if(.not.DrawInstabGraph) go to 9999
      do i=1,n
        SigCountN(i)=SigCount(i)**2+par(1)*RiSt(i)**2+
     1                              par(2)*RiSt(i)
        SigCountN(i)=sign(sqrt(abs(SigCountN(i))),SigCountN(i))
      enddo
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(100.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        endif
        if(i.le.4) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      PXMax=0.
      PYMax=0.
      do i=1,n
        PXMax=max(PXMax,RISt(i))
        PYMax=max(PYMax,abs(SigStat(i)**2-SigCount (i)**2),
     1                  abs(SigStat(i)**2-SigCountN(i)**2))
      enddo
      nButtBasTo=ButtonLastMade
      Klic=1
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        if(Klic.eq.0) then
          xomn=0.
          xomx=PXMax
          yomn=-1.
          yomx= 1.
        else if(Klic.eq.1) then
          xomn=0.
          xomx=PMax
          yomn=0.
          yomx=PMax
        endif
        call UnitMat(F2O,3)
        if(Klic.eq.0) then
          call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
          call FeMakeAcFrame
          call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Delta(rel)')
          call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
        else if(Klic.eq.1) then
          call FeSetTransXo2X(xomn,xomx,yomn,yomx,.true.)
          call FeMakeAcFrame
          call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'sig(count)')
          call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'sig(stat)')
        endif
        xpp(3)=0.
        do i=1,n
          if(Klic.eq.0) then
            xpp(1)=RISt(i)
            xpp(2)=(SigStat(i)**2-SigCount(i)**2)/PYMax
          else if(Klic.eq.1) then
            xpp(1)=SigCount(i)
            xpp(2)=SigStat(i)
          endif
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
          if(Klic.eq.0) then
            xpp(2)=(SigStat(i)**2-SigCountN(i)**2)/PYMax
          else if(Klic.eq.1) then
            xpp(1)=SigCountN(i)
            xpp(2)=SigStat(i)
          endif
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,Green)
        enddo
2300    xpp(1)=0.
        xpp(2)=0.
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=xomx
        if(Klic.eq.0) then
          xpp(2)=0.
        else
          xpp(2)=xomx
        endif
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEVent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          if(Klic.eq.0) then
            Klic=1
            go to 2100
          else
            go to 8000
          endif
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      go to 9999
9000  EM9Instab(KDatBlock)=-999.
9999  if(allocated(SigCount))
     1  deallocate(SigCount,SigCountN,SigStat,RiSt)
      return
100   format(3i4,3f9.1,i5)
      end
