      subroutine EM9CreateM90Single(n,ich)
      use Basic_mod
      use EDZones_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      dimension h(6),hp(6),hpp(6),sp(:),spo(:),ntwq(-mxsc:mxsc),
     1          ntwqo(-mxsc:mxsc),difi(3),ihi(6),mlim(3,MxPhases),
     2          mlimp(3,MxPhases),ihtw(6,50),itwa(50),IqOff(MxRefBlock)
      character*256 Veta
      character*128 Ven(4)
      character*80 t80,p80,FileNameAve,FileNameExp,FileNameLst
      character*2 nty
      logical FeYesNoHeader,SystExtRef,CrwLogicQuest,Nulova,RedukceDolu,
     1        HKLF5P,AlreadyAllocated,OnlyNext,MatRealEqUnitMat,lpom,
     2        UseMoreScales
      integer AcceptM91,YesNoSkip,UseTabsIn
      real LamAvePom,LamUsed
      allocatable sp,spo
      data difi/3*.01/
      UseTabsIn=UseTabs
      NoOfScales=1
      LnSum=0
      AlreadyAllocated=.false.
      HKLF5P=.false.
      nsp=10000
      allocate(sp(nsp))
      if(.not.SilentRun) then
        if(NDatBlock.gt.1) then
          TitleSingleCrystal='Create refinement reflection file for : '
     1       //DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
     2       //'->Single crystal'
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            Veta='X-rays'
          else if(Radiation(KDatBlock).eq.NeutronRadiation) then
            Veta='neutrons'
          else if(Radiation(KDatBlock).eq.ElectronRadiation) then
            Veta='electrons'
          else
            Veta=' '
          endif
          k=KLam(KDatBlock)
          if(Radiation(KDatBlock).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAve(KDatBlock).gt.0.) then
              write(Cislo,'(f10.6)') LamAve(KDatBlock)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          TitleSingleCrystal=
     1      TitleSingleCrystal(:idel(TitleSingleCrystal))//'/'//
     2      Veta(:idel(Veta))//'/'//Cislo(:idel(Cislo))
          WizardTitle=.true.
          WizardLength=550.
          WizardLines=16
        else
          TitleSingleCrystal=' '
          WizardTitle=.false.
          WizardLength=450.
          WizardLines=15
        endif
        WizardId=NextQuestId()
        WizardMode=.true.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     TitleSingleCrystal,0,LightGray,0,0)
      endif
      RealIndices=.false.
      OnlyNext=.true.
      if(allocated(CifKey)) then
        AlreadyAllocated=.true.
      else
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9900
      endif
1050  NRefP=0
      NoRefItems(KDatBlock)=0
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          NRefP=NRefP+nref95(i)
          NoRefItems(KDatBlock)=NoRefItems(KDatBlock)+1
        endif
      enddo
      if(NRefP.le.0) NRefP=10000
      if(SilentRun) then
        UseEFormat91=Radiation(KDatBlock).eq.NeutronRadiation.and.
     1               MagPolFlag(KDatBlock).ne.0
        if(UseEFormat91) then
          Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
        else
          Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
        endif
        write(Format91(2:2),'(i1)') maxNDim
      else
        id=NextQuestId()
        if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1     TitleSingleCrystal(:idel(TitleSingleCrystal)))
        il=1
        tpom=5.
        t80='Reflections I<'
        call FeQuestLblMake(id,tpom,il,t80,'L','N')
        xpom=tpom+FeTxLength(t80)+5.
        dpom=40.
        tpom=xpom+dpom+5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                      '*sig(I) will be sorted as unobserved','L',
     2                      dpom,EdwYd,0)
        nEdwFLim=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,EM9ObsLim(KDatBlock),
     1                          .false.,.false.)
        il=il+1
        xpom=5.
        t80='Note: this number is not interpreted by REFINE'
        call FeQuestLblMake(id,xpom,il,t80,'L','N')
        tpom=xpom+CrwgXd+5.
        il=il+1
        if(n.eq.1) then
          t80='%use in output file E-format (recommended for data '//
     1        'with large dynamical range)'
          call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,CrwgYd,
     1                        0,0)
          if(ExistM90) then
            lpom=UseEFormat91
          else
            if(Radiation(KDatBlock).eq.NeutronRadiation.and.
     1         MagPolFlag(KDatBlock).ne.0) then
              lpom=.true.
            else
              lpom=.false.
            endif
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if(Radiation(KDatBlock).eq.NeutronRadiation.and.
     1       MagPolFlag(KDatBlock).ne.0) then
            UseEFormat91=.true.
            call FeQuestCrwDisable(CrwLastMade)
          endif
          nCrwUseEFormat=CrwLastMade
        else
          if(UseEFormat91) then
            t80='e-format will be use in output file'
          else
            t80='f-format will be use in output file'
          endif
          call FeQuestLblMake(id,xpom,il,t80,'L','N')
          nCrwUseEFormat=0
        endif
        if(NoRefItems(KDatBlock).gt.1) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          write(Cislo,FormI15) KDatBlock
          call zhusti(Cislo)
          t80='Dat-block#'//Cislo(:idel(Cislo))//' consists of'
          write(Cislo,FormI15) NoRefItems(KDatBlock)
          call zhusti(Cislo)
          t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))//
     1        ' ref-blocks being:'
          il=il+1
          call FeQuestLblMake(id,tpom,il,t80,'L','N')
          xpom=5.
          tpom=xpom+CrwgXd+5.
          t80='on %different scales'
          do i=1,2
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,
     1                          CrwgYd,0,1)
            if(i.eq.1) then
              t80='on the %same scale'
              nCrwDiffScale=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.DiffScales(KDatBlock))
          enddo
        endif
        call FeQuestButtonDisable(ButtonEsc)
1150    call FeQuestEVent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        if(ich.eq.0) then
          call FeQuestRealFromEdw(nEdwFLim,EM9ObsLim(KDatBlock))
          if(NoRefItems(KDatBlock).gt.1) then
            nCrw=nCrwDiffScale
            do i=1,2
              if(CrwLogicQuest(nCrw)) then
                DiffScales(KDatBlock)=i
                exit
              endif
              nCrw=nCrw+1
            enddo
          else
            DiffScales(KDatBlock)=1
          endif
          if(Radiation(KDatBlock).ne.NeutronRadiation.or.
     1       MagPolFlag(KDatBlock).eq.0) then
            if(nCrwUseEFormat.gt.0) then
              UseEFormat91=CrwLogicQuest(nCrwUseEFormat)
            else
              UseEFormat91=.false.
            endif
          else
            UseEFormat91=.true.
          endif
          if(UseEFormat91) then
            Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
          else
            Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
          endif
          write(Format91(2:2),'(i1)') maxNDim
        endif
        if(ich.ne.0) go to 9900
      endif
      t80=fln(:ifln)//'.l91'
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      call OpenFile(91,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      if(LnSum.ne.0) call CloseIfOpened(LnSum)
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_MakeRefFile.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# MakeRefFile'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      EM9ScToM90(KDatBlock)=1.
      RedukceDolu=.false.
      iz=0
      izu=0
      call FeFlowChartOpen(-1.,-1.,max(nint(float(NRefP)*.005),10),
     1  NRefP,'Creating reflection file',' ',' ')
      if(ErrFlag.ne.0) go to 9900
      YesNoSkip=1
1400  StructureName=' '
      Uloha='Creating of the JANA reflection file'
      FileNameLst=fln(:ifln)//'_em9_1_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(1)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      AniNaListing=.true.
      call iom50(0,1,fln(:ifln)//'.m50')
      AniNaListing=.false.
      if(ErrFlag.ne.0) go to 9900
      call CloseListing
      FileNameLst=fln(:ifln)//'_em9_2_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(2)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call SetIntArrayTo(ihmin,maxNDim, 9999)
      call SetIntArrayTo(ihmax,maxNDim,-9999)
      call SetIntArrayTo(mlim,MxPhases*(maxNDim-3),0)
      SinThLMin= 9999.
      SinThLMax=-9999.
      NExt(1)=0
      NExtObs(1)=0
      NExt500(1)=0
      RIExtMax(1)=0.
      SumRelI(1)=0.
      NExt(2)=0
      NExtObs(2)=0
      NExt500(2)=0
      RIExtMax(2)=0.
      SumRelI(2)=0.
      rcc=0.
      rjc=0.
      rcn=0.
      rjn=0.
      NRef91=0
      NRef91Obs=0
      NRead=0
      NReadObs=0
      fomax=0.
      fsmax=0.
      do i=-mxsc,mxsc
        ntwq(i)=0
      enddo
      nq=0
      nqp=0
      itwmax=0
      itwmin=1
      ntw=0
      KRefBlockUsed=-1
      m=0
      if(kcommenMax.ne.0) call comsym(0,1,ich)
      do KRefBlock=1,NRefBlock
        if(RefDatCorrespond(KRefBlock).ne.KDatBlock) cycle
        m=m+1
        if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          IqOff(m)=nqp
        else
          if(DiffScales(KDatBlock).eq.1) then
            IqOff(m)=nq
          else if(DiffScales(KDatBlock).eq.2) then
            IqOff(m)=0
          else if(DiffScales(KDatBlock).eq.3) then
            if(ScNoScheme(m,KDatBlock).eq.m) then
              IqOff(m)=nq
            else
              IqOff(m)=IqOff(ScNoScheme(m,KDatBlock))
            endif
          endif
        endif
        if(KRefBlockUsed.lt.0) KRefBlockUsed=KRefBlock
        if(CorrLp(KRefBlock).eq.0) then
          call FeChybne(-1.,-1.,'LP correction has not been applied.',
     1                  ' ',SeriousError)
          go to 9900
        endif
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
        HKLF5P=HKLF5RefBlock(KRefBlock).eq.1
2100    call DRGetReflectionFromM95(95,iend,ich)
        if(ich.ne.0) go to 9000
        if(iend.ne.0) go to 2550
        iq=iflg(1)+IqOff(m)
        itw=iflg(2)
        ReadLam=DRLam
        call CopyVekI(ih,ihi,maxNDim)
        if(itw.eq.0) itw=1
        iatw=mod(iabs(itw),100)
        KPhase=KPhaseTwin(iatw)
        if(ReadLam.gt.0.) then
          LamAvePom=ReadLam
        else
          LamAvePom=LamAve(KDatBlock)
        endif
        if(iatw.gt.NTwin) then
          write(t80,format91)(ihi(i),i=1,maxNDim),ri,rs,iq,0,itw
          call FeChybne(-1.,-1.,'disagreement between twin flag and '//
     1                  'number of twin domains','for reflection : '//
     2                  t80(:idel(t80)),SeriousError)

          go to 9100
        endif
        if(iz.ge.izu) call FeFlowChartEVent(iz,ie)
        if(ie.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) go to 9100
        endif
        if(iq.lt.0) go to 2100
        if(no.gt.0) then
          pom1=corrf(1)*corrf(2)*EM9ScToM90(KDatBlock)
          if(DelMi(KRefBlock).ne.0.)
     1      pom1=pom1*exp(DelMi(KRefBlock)*tbar*10.)
          if(RunScN(KRefBlock).gt.0) then
            ikde=RunScGrN(ifix(expos),KRefBlock)
            if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
              pom=ScFrame(ikde,KRefBlock)
            else
              c1=(expos-float(RunScGrM0(ikde,KRefBlock)))/
     1            float(RunScGrM(ikde,KRefBlock)-1)
              pom=(1.-c1)*ScFrame(ikde,KRefBlock)+
     1            c1*ScFrame(ikde+1,KRefBlock)
            endif
            pom1=pom1*pom
          endif
          ri=ri*pom1
          rs=rs*pom1
          if(rs.le.0.05.and..not.UseEFormat91) then
            if(.not.RedukceDolu.and.rs.gt.0.) then
2200          if(rs.le..05) then
                EM9ScToM90(KDatBlock)=EM9ScToM90(KDatBlock)*10.
                rs=rs*10.
                go to 2200
              endif
              rewind 95
              rewind 91
              izu=iz
              call CloseIfOpened(95)
              go to 1400
            endif
            rs=0.1
          else if(rs.le.0.) then
            rs=max(sqrt(ri),.1)
          endif
        else
          go to 2100
        endif
        NRead=NRead+1
        Nulova=.true.
        if(anint(ri*10.).gt.EM9ObsLim(KDatBlock)*anint(rs*10.)) then
          Nulova=.false.
          NReadObs=NReadObs+1
        endif
        call SetIntArrayTo(mlimp,3*Mxphases,9999)
        if(HKLF5P) then
!          KPhase=KPhaseTwin(1)
          if(KPhase.eq.1) then
            call MultmIRI(ihi,trmp,ih,1,maxNDim,maxNDim)
          else
            do i=1,maxNDim
              ih(i)=ihi(i)
            enddo
          endif
          do i=1,maxNDim
            hp(i)=ih(i)
          enddo
          do i=1,3
            do j=1,NDimI(KPhase)
              hp(i)=hp(i)+qu(i,j,1,KPhase)*hp(j+3)
            enddo
          enddo
          do isw=1,NComp(KPhase)
            call MultmIRI(ih,zvi(1,isw,KPhase),ihp,1,NDim(KPhase),
     1                 NDim(KPhase))
            do i=1,NDimI(KPhase)
              mlimp(i,1)=min(mlimp(i,1),iabs(ihp(i+3)))
            enddo
          enddo
          do i=1,NDimI(KPhase)
            mlim(i,1)=max(mlim(i,1),mlimp(i,1))
          enddo
          if(SystExtRef(ih,.false.,1)) then
            if(itw.lt.0) then
              if(.not.Nulova) NReadObs=NReadObs-1
              NRead=NRead-1
              go to 2100
            else
              if(ntw.le.0) then
                iswp=0
              else
                itwa(ntw)=iabs(mod(itwa(ntw),100))
                iswp=1
              endif
            endif
          else
            ntw=ntw+1
            if(ntw.le.50) then
              call CopyVekI(ih,ihtw(1,ntw),maxNDim)
              itwa(ntw)=itw
            endif
            if(itw.lt.0) then
              if(.not.Nulova) NReadObs=NReadObs-1
              NRead=NRead-1
              go to 2100
            endif
            if(ntw.gt.50) then
              ntw=0
              go to 2100
            endif
            iswp=1
          endif
        else
          mmax=0

!   Toto byl pokus eliminovat slabe reflexe, ktere v superprostoru
!   byly zakazane, ale superbunce nejsou. To bych obnovil az se
!   zase vyskytne problem.

!          if(KCommen(KPhase).gt.0) then
!            call CrlRestoreSymmetry(ISymmSSG(KPhase))
!            do i=1,3
!              do j=1,NDimI(KPhase)
!                hp(i)=float(ihi(i))+qu(i,j,1,KPhase)*float(ihi(j+3))
!              enddo
!            enddo
!            call IndFromIndReal(hp,NCommQProduct(1,KPhase),difi,ih,itw,
!     1                          iswp,-1.,CheckExtRefYes)
!            do i=4,NDim(KPhase)
!              j=iabs(ih(i))
!              mmax=max(j,mmax)
!              mlimp(i-3,1)=min(mlimp(i-3,1),j)
!            enddo
!            call CrlRestoreSymmetry(ISymmCommen(KPhase))
!          endif
          do i=1,maxNDim
            hp(i)=ihi(i)
          enddo
          call Multm(hp,trmp,h,1,maxNDim,maxNDim)
          do isw=1,NComp(KPhase)
            call Multm(h,zvi(1,isw,KPhase),hpp,1,NDim(KPhase),
     1                 NDim(KPhase))
            do i=4,NDim(KPhase)
              j=iabs(nint(hpp(i)))
              mmax=max(j,mmax)
              mlimp(i-3,1)=min(mlimp(i-3,1),j)
            enddo
          enddo
          if(kcommenMax.ne.0.and.mmax.eq.0) mmax=1
          do i=1,NDimI(KPhase)
            mlim(i,1)=max(mlim(i,1),mlimp(i,1))
          enddo
          do i=1,NDim(KPhase)
            ihp(i)=nint(h(i))
          enddo
          do i=1,3
            do j=1,NDimI(KPhase)
              h(i)=h(i)+qu(i,j,1,KPhase)*h(j+3)
            enddo
          enddo
          call multm(h,rtwi(1,iatw),hp,1,3,3)
          call IndFromIndReal(hp,mmax,difi,ih,itw,iswp,-1.,
     1                        CheckExtRefNo)
          if(iswp.le.0) then
            call EM9NejdouPotvory(2,ihi,hp,ri,rs,maxNDim,RealIndices,
     1                            EM9ObsLim(KDatBlock))
            go to 2100
          endif
!          if(KCommen(KPhase).gt.0) then
!            call CrlRestoreSymmetry(ISymmSSG(KPhase))
!          endif
          call IndFromIndReal(hp,mmax,difi,ih,itw,iswp,-1.,
     1                        CheckExtRefYes)
!          if(KCommen(KPhase).gt.0) then
!            call CrlRestoreSymmetry(ISymmCommen(KPhase))
!            ihp(1:NDim(KPhase))=ih(1:NDim(KPhase))
!          endif
          if(iswp.gt.0.and.itw.eq.iatw)
     1      call CopyVekI(ih,ihp,NDim(KPhase))
        endif
        if(iswp.le.0) then
          call EM9NejdouPotvory(1,ih,hp,ri,rs,maxNDim,RealIndices,
     1                          EM9ObsLim(KDatBlock))
          go to 2100
        endif
        if(itw.eq.iatw) call CopyVekI(ihp,ih,NDim(KPhase))
        if(HKLF5P) then
          KPhase=KPhaseTwin(mod(itw,100))
        else
          KPhase=KPhaseTwin(1)
        endif
        call FromIndSinthl(ih,hp,sinthl,sinthlq,1,1)
        KPhase=KPhaseTwin(mod(itw,100))
        if(LamAvePom.gt.0.) then
          pom1=sinthl*LamAvePom
          if(abs(pom1).gt..999999.and.YesNoSkip.ne.0) then
            NInfo=2
            write(t80,'(''#'',i10,''...('',6(i5,'',''))')
     1        no,(ihi(i),i=1,maxNDim)
            call zhusti(t80)
            i=idel(t80)
            if(t80(i:i).eq.',') i=i-1
            write(t80(i+1:),'('')...sin(th)='',f6.3)') pom1
            i=idel(t80)
            TextInfo(1)='The reflection '//t80(:i)//
     1                  ' has unrealistic indices.'
            TextInfo(2)='It gives sin(theta)>1 and it will be skipped.'
            call FeMouseShape(0)
            if(FeYesNoHeader(-1.,YBottomMessage,
     1                       'Do you want to stop the whole '//
     2                       'procedure?',YesNoSkip)) then
              go to 9100
            else
              call FeMouseShape(3)
              YesNoSkip=0
              pom1=.999999
              go to 2100
            endif
          endif
        endif
        if(SinThL.gt.SinThLMax) then
          SinThLMax=SinThL
          LamUsed=LamAvePom
        endif
        SinThLMin=min(SinThLMin,SinThL)
        fomax=max(fomax,ri)
        fsmax=max(fsmax,rs)
        if(fomax.gt.999999.9.and..not.UseEFormat91) then
          RedukceDolu=.true.
2300      if(fomax.gt.999999.9) then
            EM9ScToM90(KDatBlock)=EM9ScToM90(KDatBlock)*.1
            fomax=fomax*.1
            fsmax=fsmax*.1
            go to 2300
          endif
2400      if(fsmax.gt.9999.9) then
            EM9ScToM90(KDatBlock)=EM9ScToM90(KDatBlock)*.1
            fsmax=fsmax*.1
            go to 2400
          endif
          rewind 95
          rewind 91
          call FeFlowChartRefresh
          call CloseIfOpened(95)
          go to 1400
        endif
        do i=1,6
          ihmax(i)=max(ihmax(i),ih(i))
          ihmin(i)=min(ihmin(i),ih(i))
        enddo
        rcc=rcc+rs
        rjc=rjc+ri
        NRef91=NRef91+1
        if(NRef91.gt.nsp) then
          allocate(spo(nsp))
          call CopyVek(sp,spo,nsp)
          deallocate(sp)
          nspo=nsp
          nsp=nsp+10000
          allocate(sp(nsp))
          call CopyVek(spo,sp,nspo)
          deallocate(spo)
        endif
        sp(NRef91)=sinthl
        if(.not.Nulova) then
          rcn=rcn+rs
          rjn=rjn+ri
          NRef91Obs=NRef91Obs+1
        endif
        if(iq.eq.0) iq=1
        ii=mod(iabs(itw),100)
        if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          if(CalcDyn) iq=RunCCD
          nqp=max(nqp,iq)
          if(DiffScales(KDatBlock).eq.1.and..not.CalcDyn) then
            ntwq(ii)=max(ntwq(ii),iq)
            nq=nqp
          else if(DiffScales(KDatBlock).eq.2) then
            nq=1
            ntwq(ii)=1
          endif
        else
          ntwq(ii)=max(ntwq(ii),iq)
          nq=max(nq,iq)
          nqp=nq
        endif
        itwmin=min(itwmin,ii)
        itwmax=max(itwmax,ii)
        nxx=iflg(3)
        if(HKLF5P) then
          do j=1,ntw
            write(91,format91)(ihtw(i,j),i=1,maxNDim),ri,rs,iq,nxx,
     1                         itwa(j),tbar,ReadLam,DirCos,0.,
     2                         1000*no+KRefBlock
          enddo
          ntw=0
        else
          write(91,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,
     1                       tbar,ReadLam,DirCos,0.,
     2                       1000*no+KRefBlock
        endif
        go to 2100
2550    call CloseIfOpened(95)
        ModifiedRefBlock(KRefBlock)=.false.
      enddo
      KRefBlock=KRefBlockUsed
      call FeFlowChartRemove
      NInfo=2
      UseTabs=NextTabs()
!      call FeTabsReset(UseTabs)
      write(t80,100) NReadObs,NRead
      call Zhusti(t80)
      Ven(1)=t80
      j=index(t80,'/')
      Cislo=' '
      if(j.lt.7) Ven(1)=Cislo(:7-j)//Ven(1)(:idel(Ven(1)))
      xpom=FeTxLength(t80(:j))
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'/')
      xpom=FeTxLength(t80(:idel(t80))//'X')
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      t80=Tabulator//t80(:idel(t80))//Tabulator
      TextInfo(1)=t80(:idel(t80))//' reflections read from input file'
      Ven(1)=Ven(1)(:idel(Ven(1)))//' reflections read from input file'
      write(t80,100) NRef91Obs,NRef91
      call Zhusti(t80)
      Ven(2)=t80
      j=index(t80,'/')
      Cislo=' '
      if(j.lt.7) Ven(2)=Cislo(:7-j)//Ven(2)(:idel(Ven(2)))
      t80=Tabulator//t80(:idel(t80))//Tabulator
      TextInfo(2)=t80(:idel(t80))//' reflections written to output file'
      Ven(2)=Ven(2)(:idel(Ven(2)))//
     1       ' reflections written to output file'
      if(NExt(2).gt.0) then
        write(t80,100) NExtObs(2),NExt(2)
        call Zhusti(t80)
        NInfo=NInfo+1
        Ven(NInfo)=t80
        j=index(t80,'/')
        Cislo=' '
        if(j.lt.10)
     1    Ven(NInfo)=Cislo(:7-j)//Ven(NInfo)(:idel(Ven(NInfo)))
        t80=Tabulator//t80(:idel(t80))//Tabulator
        TextInfo(NInfo)=t80(:idel(t80))//
     1    ' reflections gave unacceptable indices'
        Ven(NInfo)=Ven(NInfo)(:idel(Ven(NInfo)))//
     1    ' reflections gave unacceptable indices'
      endif
      NInfo=NInfo+1
      if(NExt(1).gt.0) then
        write(t80,100) NExtObs(1),NExt(1)
        call Zhusti(t80)
        Ven(NInfo)=t80
        j=index(t80,'/')
        Cislo=' '
        if(j.lt.7)
     1    Ven(NInfo)=Cislo(:7-j)//Ven(NInfo)(:idel(Ven(NInfo)))
        t80=Tabulator//t80(:idel(t80))//Tabulator
        TextInfo(NInfo)=t80(:idel(t80))//
     1    ' reflections rejected as systematically extinct'
        Ven(NInfo)=Ven(NInfo)(:idel(Ven(NInfo)))//
     1    ' reflections rejected as systematically extinct'
      else
        TextInfo(NInfo)='There were no rejections due to systematic '//
     1                  'extinctions'
        Ven(NInfo)='There were no rejections due to systematic '//
     1             'extinctions'
      endif
      call FeInfoOut(-1.,-1.,'Import statistics - obs/all','L')
      call FeTabsReset(UseTabs)
      if(NRef91.le.0) then
        call FeChybne(-1.,-1.,'All reflections were discarded.',' ',
     1                Warning)
        ich=-1
        go to 9900
      endif
      call CrlGetReflectionConditions
      if(NExtRefCond.gt.0) then
        call Newln(2)
        write(lst,'(''The following reflection conditions will be '',
     1              ''applied''/)')
        do i=1,NExtRefCond
          call Newln(1)
          k=ExtRefPor(i)
          call CrlMakeExtString(ExtRefGroup(1,k),ExtRefCond(1,k),t80,
     1                          p80)
          if(ExtRefFlag(k).ne.0) then
            Veta=' <= induced from above conditions'
          else
            Veta=' '
          endif
          write(lst,FormA1) ' ',(t80(j:j),j=1,idel(t80)),' ',':',' ',
     1                          (p80(j:j),j=1,idel(p80)),
     2                          (Veta(j:j),j=1,idel(Veta))
        enddo
      else
        call Newln(2)
        write(lst,'(''No reflection conditions will be applied''/)')
      endif
      if(.not.MatRealEqUnitMat(TrMP,maxNDim,.0001)) then
        call Newln(maxNDim+3)
        write(lst,'(/''The following transformation will be applied '',
     1               ''to the indices from the data collection''/)')
        do i=1,maxNDim
          write(lst,'(1x,6f8.4)')(trmp(i+(j-1)*maxNDim),
     1                                    j=1,maxNDim)
        enddo
      endif
      call Newln(3)
      write(lst,'(/''Reflections with I<'',f5.2,''*sig(I) will be '',
     1             ''classified as unobserved''/)') EM9ObsLim(KDatBlock)
      if(abs(EM9ScToM90(KDatBlock)-1.).gt..5) then
        call newln(1)
        if(EM9ScToM90(KDatBlock).gt.1.) then
          write(Cislo,'(f15.0)') EM9ScToM90(KDatBlock)
        else
          write(Cislo,'(f15.5)') EM9ScToM90(KDatBlock)
        endif
        call ZdrcniCisla(Cislo,1)
        write(lst,'(''Intesities from M95 were rescaled by the factor ''
     1              ,a)') Cislo(:idel(Cislo))
        call newln(1)
        write(lst,FormA)
      endif
      do i=1,NInfo
        call newln(1)
        write(lst,FormA) Ven(i)(:idel(Ven(i)))
      enddo
      call Newln(maxNDim+1)
      write(lst,FormA)
      Ninfo=maxNDim+3
      write(lst,105)(indices(i),ihmin(i),indices(i),ihmax(i),
     1                                           i=1,maxNDim)
      rn=rcn/rjn*100.
      rc=rcc/rjc*100.
      call Newln(2)
      write(lst,FormA)
      write(lst,104) rn,rc
      write(Cislo,FormI15) NRef91
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(93,10),Cislo(:idel(Cislo))
      if(LamUsed.gt.0.) then
        do i=1,2
          if(i.eq.1) then
            pom=asin(min(SinThLMin*LamUsed,.999999))/Torad
          else
            ThMax=asin(min(SinThLMax*LamUsed,.999999))/Torad
            pom=thmax
          endif
          write(Cislo,'(f8.2)') pom
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(98-i,10),Cislo
        enddo
      else
        ThMax=SinThLMax
      endif
      m=88
      do i=1,2*NDim(KPhase)
        j=(i-1)/2+1
        k=mod(i-1,2)+1
        if(mod(i,2).eq.1) then
          l=ihmin(j)
        else
          l=ihmax(j)
        endif
        write(Cislo,FormI15) l
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(m,10),Cislo(:idel(Cislo))
        if(i.eq.6) m=140
        if(mod(i,2).eq.1) then
          m=m-1
        else
          m=m+3
        endif
      enddo
      call EM9OutputOfRejected(2)
      call EM9OutputOfRejected(1)
      call HeapR(NRef91,sp,1)
      sinmez(8)=sp(NRef91)+.000001
      d=0.
      dd=float(NRef91)*.125
      do i=1,7
        d=d+dd
        j=d
        sinmez(i)=(sp(j)+sp(j+1))*.5
      enddo
      call CloseIfOpened(91)
      FileNameExp=fln(:ifln)//'.l91'
c      if(nq.gt.18) then
c        DataAve(KDatBlock)=0
c        FileNameAve=FileNameExp
c        go to 6000
c      endif
      nrefav=0
      nrefsc=NRef91*(nq+2)
      do i=itwmin,itwmax
        nrefav=nrefav+2*ntwq(i)*NRef91
      enddo
      UseMorescales=.false.
      if(nq.le.1.or.
     1   DifCode(KRefBlock).eq.IdVivaldi.or.
     2   DifCode(KRefBlock).eq.IdSXD.or.
     3   DifCode(KRefBlock).eq.IdTopaz.or.
     4   DifCode(KRefBlock).eq.IdSCDLANL.or.
     5   DifCode(KRefBlock).eq.IdSENJU) go to 5000
      do i=itwmin,itwmax
        ntwqo(i)=ntwq(i)
      enddo
4000  call EM9MakeScales(nq,0,ich)
      if(ich.gt.0) then
        go to 1050
      else if(ich.lt.0) then
        go to 9900
      endif
      if(EM9ScaleLim(KDatBlock).lt.0.) then
        FileNameExp=fln(:ifln)//'.l91'
        NRef90(KDatBlock)=NRef91
        nrefav=0
        do i=itwmin,itwmax
          ntwq(i)=ntwqo(i)
          nrefav=nrefav+2*ntwq(i)*NRef91
        enddo
        UseMorescales=.true.
        go to 5000
      else if(EM9ScaleLim(KDatBlock).eq.0.) then
        call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        FileNameExp=fln(:ifln)//'.l92'
        call OpenFile(92,FileNameExp,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
4100    read(91,format91,end=4200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar
        if(nxx.ne.0) go to 4100
        iq=1
        write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar
        go to 4100
4200    call CloseIfOpened(91)
        call CloseIfOpened(92)
        go to 5000
      endif
      t80=fln(:ifln)//'.l92'
      call CloseListing
      FileNameLst=fln(:ifln)//'_em9_3_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(3)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call OpenFile(92,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nrefsc)*.005),10),
     1                     nrefsc,
     2                     'Transformation to common scale',' ',
     3                     ' ')
      call EM9MakeScales(1,1,ich)
      if(ich.ne.0) go to 4500
      do j=2,nq
        call EM9MakeScales(j,2,ich)
        if(ich.ne.0) then
          call CloseIfOpened(91)
          call CloseIfOpened(92)
          call FeFlowChartRemove
          go to 4000
        endif
      enddo
      sckor(1)=1.
      sckors(1)=0.
      sccom(1)=0
      sccom1(1)=0
      scpairs(1)=0
      sckorm=0.
      do i=1,nq
        sckorm=max(sckorm,sckor(i))
      enddo
      do i=1,nq
        sckor (i)=sckor (i)/sckorm
        sckors(i)=sckors(i)/sckorm
      enddo
      call EM9MakeScales(1,3,ich)
4500  call FeFlowChartRemove
      call CloseIfOpened(91)
      call CloseIfOpened(92)
      if(ich.eq.1) then
        go to 5000
      else if(ich.eq.2) then
        go to 8000
      endif
      Ninfo=nq+3
      TextInfo(1)='           Reference set      Actual set'
      TextInfo(2)='         #common/#gt-ones  #common/#gt-ones    '//
     1            '#used pairs    Scale'
      TextInfo(3)='Set'
      do j=1,nq
        write(p80,106) sccom1(j),scref(1)
        call Zhusti(p80)
        if(15-idel(p80).gt.0) then
          n=index(p80,'/')-8
          if(n.gt.0) then
            p80=p80(n:)
          else if(n.lt.0) then
            do i=1,-n
              p80=' '//p80
            enddo
          endif
        endif
        write(t80,106) sccom(j),scref(j)
        call Zhusti(t80)
        if(15-idel(t80).gt.0) then
          n=index(t80,'/')-8
          if(n.gt.0) then
            t80=t80(n:)
          else if(n.lt.0) then
            do ii=1,-n
              t80=' '//t80
            enddo
          endif
        endif
        write(TextInfo(j+3),'(i2,7x,a15,3x,a15,3x,i10,5x,f8.4)') j,
     1    p80(:15),t80(:15),scpairs(j),sckor(j)
        if(j.gt.1) then
          write(t80,'(''('',i15,'')'')') nint(sckors(j)*10000.)
          call zhusti(t80)
          TextInfo(j+3)=TextInfo(j+3)(:idel(TextInfo(j+3)))//
     1                  t80(:idel(t80))
        else
          TextInfo(j+3)(10:25)='    ---/---    '
          TextInfo(j+3)(28:42)='    ---/---    '
          TextInfo(j+3)(46:55)='       ---'
        endif
      enddo
      t80='Summary from transformation to the common scale'
      call TitulekVRamecku(t80)
      call NewLn(Ninfo)
      do i=1,NInfo
        write(lst,FormA) TextInfo(i)(:idel(TextInfo(i)))
      enddo
      TextInfo(1)='              Reference set             Actual set'
      TextInfo(2)='         #common/#gt-ones  #common/#gt-ones    '//
     1            '#used pairs    Scale'
      call FeTabsAdd( 10.,UseTabs,IdLeftTab,' ')
      call FeTabsAdd( 75.,UseTabs,IdCharTab,'/')
      call FeTabsAdd(175.,UseTabs,IdCharTab,'/')
      call FeTabsAdd(275.,UseTabs,IdLeftTab,' ')
      call FeTabsAdd(310.,UseTabs,IdCharTab,'.')
      do j=1,nq
        if(j.eq.1) then
          p80='---/---'
          t80='---/---'
          Cislo='---'
        else
          write(p80,106) sccom1(j),scref(1)
          call Zhusti(p80)
          write(t80,106) sccom(j),scref(j)
          call Zhusti(t80)
          write(Cislo,'(i10)') scpairs(j)
        endif
        write(TextInfo(j+3),'(a1,i2,a1,a,a1,a,a1,a,a1,f8.4)')
     1    Tabulator,j,Tabulator,p80(:idel(p80)),Tabulator,
     2    t80(:idel(t80)),Tabulator,Cislo(:idel(Cislo)),Tabulator,
     3    sckor(j)
        if(j.gt.1) then
          write(t80,'(''('',i15,'')'')') nint(sckors(j)*10000.)
          call zhusti(t80)
          TextInfo(j+3)=TextInfo(j+3)(:idel(TextInfo(j+3)))//
     1                  t80(:idel(t80))
        endif
      enddo
      t80='Summary from transformation to the common scale'
      call FeInfoOut(-1.,-1.,t80,'L')
      do i=itwmin,itwmax
        ntwqo(i)=ntwq(i)
        ntwq (i)=1
      enddo
      nrefav=2*(itwmax-itwmin+1)*NRef91
      FileNameExp=fln(:ifln)//'.l92'
      iqmax=1
5000  call CloseListing
      if(DifCode(KRefBlock).eq.IdPets.and.CalcDyn) then
        DataAve(KDatBlock)=0
        FileNameAve=FileNameExp
        go to 6000
      endif
      FileNameLst=fln(:ifln)//'_em9_4_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      LstOpened=.true.
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      call NewPg(1)
      Veta=LabelsEditm9(4)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      call EM9Average(0,0,0,ich)
      if(ich.gt.0) then
        call CloseListing
        call DeleteFile(FileNameLst)
        if(nq.gt.1) then
          go to 4000
        else
          go to 1050
        endif
      else if(ich.lt.0) then
        go to 9900
      endif
      call EM9AverageStatReset
      if(DataAve(KDatBlock).le.0) then
        FileNameAve=FileNameExp
        go to 6000
      endif
      t80=fln(:ifln)//'.l93'
      call OpenFile(91,FileNameExp,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call OpenFile(92,t80,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      NRef92=0
      Prumer=.false.
      call SetRealArrayTo(rnum ,11,0.)
      call SetRealArrayTo(rden ,11,0.)
      call SetRealArrayTo(ronum,11,0.)
      call SetRealArrayTo(roden,11,0.)
      call SetRealArrayTo(rexpnum ,11,0.)
      call SetRealArrayTo(rexpden ,11,0.)
      call SetRealArrayTo(roexpnum,11,0.)
      call SetRealArrayTo(roexpden,11,0.)
      call SetIntArrayTo(nrefi ,11,0)
      call SetIntArrayTo(norefi,11,0)
      call SetIntArrayTo(nrefa ,11,0)
      call SetIntArrayTo(norefa,11,0)
      mmMax=1
      call TitulekVRamecku('Report from averaging reflections')
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nrefav)*.005),10),
     1  nrefav,'Averaging reflections from reflection file',' ',' ')
      do i=itwmin,itwmax
        if(i.eq.0) cycle
        do j=1,ntwq(i)
          if(line.gt.40) call NewPg(0)
          if(itwmax.gt.itwmin.or.ntwq(i).gt.1) then
            call newln(3)
            if(itwmax.gt.itwmin) then
              if(ntwq(i).le.1) then
                write(lst,'(/''Averaging from '',i2,a2,
     1                       '' domain fraction''/)') i,nty(i)
              else
                write(lst,'(/''Averaging of '',i2,a2,
     1                       '' domain fraction, scale #'',i2/)')
     2            i,nty(i),j
              endif
            else
              write(lst,'(/''Averaging date with scale #'',i2/)') j
            endif
          endif
          call EM9Average(i,j,1,ich)
          if(ich.ne.0) go to 9100
        enddo
      enddo
      if(LnInstab.gt.0) then
        call EM9SigCountSigStat
        close(LnInstab,status='delete')
        call newln(2)
        if(EM9Instab(KDatBlock).gt.-900.) then
          write(lst,'(/'' Refined instability factor : '',f8.5)')
     1    EM9Instab(KDatBlock)
        else
          write(lst,'(/'' Instability factor could not be refined, ''
     1                 '' there too few or no redundacy'')')
        endif
        LnInstab=0
      endif
      call FeFlowChartRemove
      if(HKLF5P) then
        rewind 91
5100    read(91,format91,end=5150)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                             itw,tbar,(pom,i=1,8),NoRefBlock
        if(itw.lt.0) then
          if(ntw.eq.0) nxxm=0
          nxxm=max(nxxm,nxx)
          ntw=ntw+1
          write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar,
     1                      (pom,i=1,8),NoRefBlock
          go to 5100
        else if(ntw.gt.0) then
          nxxm=max(nxxm,nxx)
          if(nxxm.gt.0) then
            do i=1,ntw
              backspace 92
            enddo
          else
            write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,itw,tbar,
     1                        (pom,i=1,8),NoRefBlock
          endif
          ntw=0
          go to 5100
        endif
        go to 5100
      endif
5150  close(91)
      call NewPg(0)
      call TitulekVRamecku('Summary after averaging')
      i=maxNDim+6
      if(ncull.gt.0) i=i+1
      call Newln(i)
      do i=1,maxNDim
        ihmin(i)= 9999
        ihmax(i)=-9999
      enddo
      rcc=0.
      rjc=0.
      rcn=0.
      rjn=0.
      NRef92=0
      NRef92Obs=0
      rewind 92
5200  read(92,format91,end=5300)(ih(k),k=1,maxNDim),ri,rs,iq,nxx,
     1                           itw,tbar
      if(HKLF5P.and.itw.lt.0) go to 5200
      KPhase=KPhaseTwin(mod(itw,100))
      if(KPhase.eq.1) then
        do i=1,6
          ihmax(i)=max(ihmax(i),ih(i))
          ihmin(i)=min(ihmin(i),ih(i))
        enddo
      endif
      do i=1,3
        h(i)=ih(i)
        do j=1,NDimI(KPhase)
          h(i)=h(i)+qu(i,j,1,KPhase)*float(ih(j+3))
        enddo
      enddo
      if(HKLF5P) then
        KPhase=KPhaseTwin(mod(itw,100))
        hp(1:3)=h(1:3)
      else
        call multm(h,rtwi(1,mod(iatw,100)),hp,1,3,3)
        KPhase=KPhaseTwin(1)
      endif
!      KPhase=KPhaseTwin(1)
      call FromIndSinthl(ih,hp,sinthl,sinthlq,1,1)
      rcc=rcc+rs
      rjc=rjc+ri
      NRef92=NRef92+1
      if(NRef92.gt.nsp) then
        allocate(spo(nsp))
        call CopyVek(sp,spo,nsp)
        deallocate(sp)
        nspo=nsp
        nsp=nsp+10000
        allocate(sp(nsp))
        call CopyVek(spo,sp,nspo)
        deallocate(spo)
      endif
      sp(NRef92)=sinthl
      if(anint(ri*10.).gt.EM9ObsLim(KDatBlock)*anint(rs*10.)) then
        rcn=rcn+rs
        rjn=rjn+ri
        NRef92Obs=NRef92Obs+1
      endif
      go to 5200
5300  Ninfo=maxNDim+4
      if(ncull.ge.0) Ninfo=Ninfo+1
      if(prumer) then
        rave=rnum(1)/rden(1)
        if(roden(1).gt.0.) then
          write(t80,'(f6.2,''/'',f6.2)') ronum(1)/roden(1)*100.,
     1                                   rave*100.
        else
          if(rden(1).gt.0.) then
            write(t80,'(''-----/'',f6.2)') rnum(1)/rden(1)*100.
          else
            write(t80,'(''-----/-----'')')
          endif
        endif
      else
        write(t80,'(''-----/-----'')')
      endif
      call Zhusti(t80)
      TextInfo(1)='Rint(obs/all) = '//t80(:idel(t80))
      write(t80,100) NRef92Obs,NRef92
      call Zhusti(t80)
      j=idel(TextInfo(1))
      TextInfo(1)=TextInfo(1)(:j)//' for '//t80(:idel(t80))//
     1            ' reflections'
      write(t80,100) NRef91Obs,NRef91
      call Zhusti(t80)
      TextInfo(2)=' '
      TextInfo(2)=TextInfo(2)(:j-9)//'averaged from '//t80(:idel(t80))
     1          //' reflections'
      if(NRef92.gt.0) then
        write(t80,'(f10.3)') float(NRef91)/float(NRef92)
      else
        t80='0.0'
      endif
      call ZdrcniCisla(t80,1)
      TextInfo(3)='Redundancy = '//t80(:idel(t80))
      do 5315k=1,3
        write(lst,FormA) TextInfo(k)(:idel(TextInfo(k)))
        if(k.le.2) then
          kk=0
          n=0
5310      kp=kk
          call kus(TextInfo(k),kk,Cislo)
          ic=index(Cislo,'/')
          if(ic.gt.0) then
            n=n+1
            if((k.eq.1.and.n.eq.3).or.
     1         (k.eq.2.and.n.eq.1)) then
              if(k.eq.1) then
                xpom=FeTxLengthSpace(TextInfo(k)(:kp+ic))
                tpom=FeTxLength(TextInfo(k)(:kp))
              endif
              TextInfo(k)=TextInfo(k)(:kp)//Tabulator//
     1                    TextInfo(k)(kp+1:)
              if(k.eq.2) TextInfo(k)=Tabulator//
     1                               TextInfo(k)(:idel(TextInfo(k)))
              go to 5315
            endif
          endif
          if(kk.lt.len(TextInfo(k))) go to 5310
        endif
5315  continue
      UseTabsInfo(1)=NextTabs()
      call FeTabsAdd(xpom,UseTabsInfo(1),IdCharTab,'/')
      UseTabsInfo(2)=NextTabs()
      call FeTabsAdd(tpom,UseTabsInfo(2),IdLeftTab,' ')
      call FeTabsAdd(xpom,UseTabsInfo(2),IdCharTab,'/')
      UseTabsInfo(3)=UseTabsInfo(2)
      UseTabsInfo(4)=NextTabs()
      xpom=FeTxLength('XXXXXXXXXXX')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdRightTab,' ')
      xpom=xpom+FeTxLengthSpace('h(min) ')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdRightTab,' ')
      xpom=xpom+FeTxLengthSpace('XXXXXX')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdLeftTab,' ')
      xpom=xpom+FeTxLengthSpace('X')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdRightTab,' ')
      xpom=xpom+FeTxLengthSpace('h(max) ')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdRightTab,' ')
      xpom=xpom+FeTxLengthSpace('XXXXXX')
      call FeTabsAdd(xpom,UseTabsInfo(4),IdLeftTab,' ')
      write(lst,FormA)
      do i=1,maxNDim
        write(TextInfo(i+3),105) indices(i),ihmin(i),
     1                           indices(i),ihmax(i)
        write(lst,FormA) TextInfo(i+3)(:idel(TextInfo(i+3)))
        p80=' '
        t80=TextInfo(i+3)
        k=0
5320    call kus(t80,k,Cislo)
        if(idel(p80).le.0) then
          p80=Tabulator//Cislo
        else
          p80=p80(:idel(p80))//Tabulator//Cislo
        endif
        if(k.lt.len(t80)) go to 5320
        TextInfo(i+3)=p80
        if(i.gt.1) UseTabsInfo(i+3)=UseTabsInfo(4)
      enddo
      rn=rcn/rjn*100.
      rc=rcc/rjc*100.
      write(lst,FormA)
      i=maxNDim+4
      write(TextInfo(i),104) rn,rc
      UseTabsInfo(i)=UseTabsInfo(4)
      write(lst,FormA ) TextInfo(i)(:idel(TextInfo(i)))
      if(ncull.ge.0) then
        if(ncull.gt.0) then
          write(t80,FormI15) ncull
          call zhusti(t80)
          t80=t80(:idel(t80))//' reflections were culled'
        else
          t80='no reflection was culled'
        endif
        i=i+1
        TextInfo(i)='Information from culling : '//t80(:idel(t80))
        UseTabsInfo(i)=UseTabsInfo(4)
        write(lst,FormA) TextInfo(i)(:idel(TextInfo(i)))
      endif
      call FeInfoOut(-1.,-1.,'Summary after averaging','L')
      do i=1,NInfo
        call FeTabsReset(UseTabsInfo(i))
      enddo
      if(maxNDim.gt.3) then
        call TitulekVRamecku('Rint and other characteritics as a '//
     1                       'function of satellite index')
        call Newln(11)
        if(maxNDim.eq.4) then
          Ven(1)='Satelite index'
        else
          Ven(1)='Satelite indices'
        endif
        l=30
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          if(i.eq.2) then
            Veta='('
          else
            Veta='+-('
          endif
          do j=1,NDimI(KPhase)
            write(Cislo,FormI15) HSatGroups(j,i-2)
            call zhusti(Cislo)
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
          enddo
          j=idel(Veta)
          Veta(j:j)=')'
          Ven(1)(l-j+1:l)=Veta(:j)
          l=l+11
        enddo
        write(lst,FormA) Ven(1)(:idel(Ven(1)))
        write(lst,FormA)
        Ven(1)='Obs  Rexp'
        Ven(2)='     Rint'
        Ven(3)='     Number'
        Ven(4)='     Averaged from'
        l=20
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          write(Ven(1)(l:),'(f11.2)') roexpnum(i)/roexpden(i)*100.
          if(roden(i).gt.0.) then
            write(Ven(2)(l:),'(f11.2)') ronum(i)/roden(i)*100.
          else
            Ven(2)(l:)='        ----'
          endif
          write(Ven(3)(l:),'(i11)') norefa(i)
          write(Ven(4)(l:),'(i11)') norefi(i)
          l=l+11
        enddo
        do i=1,4
          write(lst,FormA) Ven(i)(:idel(Ven(i)))
        enddo
        write(lst,FormA)
        Ven(1)='All  Rexp'
        l=20
        do i=2,mmMax
          if(roexpden(i).le.0.) cycle
          write(Ven(1)(l:),'(f11.2)') rexpnum(i)/rexpden(i)*100.
          if(rden(i).gt.0.) then
            write(Ven(2)(l:),'(f11.2)') rnum(i)/rden(i)*100.
          else
            Ven(2)(l:)='        ----'
          endif
          write(Ven(3)(l:),'(i11)') nrefa(i)
          write(Ven(4)(l:),'(i11)') nrefi(i)
          l=l+11
        enddo
        do i=1,4
          write(lst,FormA ) Ven(i)(:idel(Ven(i)))
        enddo
      endif
      call EM9AverageStatOutput
      if(NRef92.gt.7) then
        call HeapR(NRef92,sp,1)
        sinmez(8)=sp(NRef92)+.000001
        d=0.
        dd=float(NRef92)*.125
        do i=1,7
          d=d+dd
          j=d
          sinmez(i)=(sp(j)+sp(j+1))*.5
        enddo
      endif
      do i=85,86
        if(i.eq.85) then
          if(Prumer) then
            pom=rave
          else
            pom=0.
          endif
          ii=85
        else
          ii=130
          pom=rc*.01
        endif
        write(Cislo,'(f8.4)') pom
        call ZdrcniCisla(Cislo,1)
        write(LnSum,FormCIF) CifKey(ii,10),Cislo
      enddo
      FileNameAve=fln(:ifln)//'.l93'
      call CloseIfOpened(91)
      call CloseIfOpened(92)
      NRef90(KDatBlock)=NRef92
      go to 7000
6000  NRef90(KDatBlock)=NRef91
      write(Cislo,'(f8.4)') rc*.01
      call ZdrcniCisla(Cislo,1)
      write(LnSum,FormCIF) CifKey(130,10),Cislo(:idel(Cislo))
      call CloseIfOpened(91)
7000  do i=1,6
        ihmax(i)=max(iabs(ihmin(i)),iabs(ihmax(i)))
      enddo
8000  call CloseListing
      FileNameLst=fln(:ifln)//'_em9_5_'
      write(Cislo,FormI15) KDatBlock
      call Zhusti(Cislo)
      FileNameLst=FileNameLst(:idel(FileNameLst))//Cislo(:idel(Cislo))//
     1            '.tmp'
      call OpenFile(lst,FileNameLst,'formatted','unknown')
      LstOpened=.true.
      call NewPg(1)
      Veta=LabelsEditm9(5)
      if(NDatBlock.gt.1)
     1  Veta=Veta(:idel(Veta))//' - '//DatBlockName(KDatBlock)
      call TitulekVRamecku(Veta)
      if(.not.SilentRun) then
        id=NextQuestId()
        if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1     TitleSingleCrystal(:idel(TitleSingleCrystal)))
        Veta='Accept the new DatBlock and calculate coverage'
        xpom=5.
        tpom=xpom+CrwgXd+10.
        il=0
        do i=1,3
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,1)
          if(i.eq.1) then
            nCrwFirst=CrwLastMade
            Veta='Accept the new DatBlock'
          else if(i.eq.2) then
            Veta='Discard the new DatBlock'
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        enddo
        call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
8100    call FeQuestEVent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 8100
        endif
        AcceptM91=0
        if(ich.eq.0) then
          nCrw=nCrwFirst
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              AcceptM91=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
      else
        AcceptM91=1
      endif
      if(AcceptM91.eq.0) then
        if(.not.SilentRun)
     1    call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
        if(nq.gt.6.or.HKLF5P) then
          go to 1050
        else
          go to 5000
        endif
      else if(AcceptM91.le.2) then
        write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        DatBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        call MoveFile(FileNameAve,DatBlockFileName)
        if(AcceptM91.eq.1) call EM9CheckCompleteness(ThMax,LamUsed,mlim)
        M91Changed=.true.
        if(PerLimit.lt.101.) then
          write(Cislo,'(f8.2)') ThFull
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(95,10),Cislo
          write(Cislo,'(f8.2)') PerLimitMax*.01
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(6,10),Cislo
          write(Cislo,'(f8.2)') PerLimit*.01
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(5,10),Cislo
        endif
      else
        call DeleteFile(PreviousM50)
      endif
      if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1   DifCode(KRefBlock).eq.IdSXD.or.
     2   DifCode(KRefBlock).eq.IdTopaz.or.
     3   DifCode(KRefBlock).eq.IdSCDLANL.or.
     4   DifCode(KRefBlock).eq.IdSENJU.or.
     5   UseMoreScales) then
        if(nq.gt.mxscutw-NTwin+1) then
          mxscu=nq
          mxscutw=mxscu+NTwin-1
        endif
        do i=1,nq
          if(sc(i,KDatBlock).le.0.) sc(i,KDatBlock)=1.
        enddo
        sctw(1:NTwin,KDatBlock)=1./float(Ntwin)
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
      call DeleteFile(fln(:ifln)//'.l91')
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      go to 9900
9000  call FeReadError(95)
9100  call FeFlowChartRemove
9900  if(.not.SilentRun) call FeQuestRemove(WizardId)
      call CloseListing
      do i=89,91
        call CloseIfOpened(i)
      enddo
      call CloseIfOpened(95)
      call CloseListing
      if(allocated(sp)) deallocate(sp)
      call DeleteFile(fln(:ifln)//'.l91')
      call DeleteFile(fln(:ifln)//'.l92')
      call DeleteFile(fln(:ifln)//'.l93')
      if(UseTabs.ne.UseTabsIn) call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(allocated(CIFKey).and..not.AlreadyAllocated)
     1  deallocate(CifKey,CifKeyFlag)
      call CloseIfOpened(LnSum)
      return
100   format(i15,'/'i15)
101   format(6i4)
102   format(1x,3f9.1)
103   format('Exported ',i8,' reflections, ',i8,' observed ones')
104   format('R(obs/all) from e.s.d. of I : ',f5.2,'/',f5.2)
105   format(17x,a1,'(min) = ',i4, ', ',a1,'(max) = ',i4)
106   format(i15,'/',i15)
108   format('Rave:',2f8.4)
      end
