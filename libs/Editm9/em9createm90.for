      subroutine EM9CreateM90(Klic,ich)
      use Basic_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      logical ExistFile
      dimension ip(1)
      character*256 t256,Veta,Radka(:)
      character*80  FileNameLst,VetaPom
      character*20  RadString(MxDatBlock)
      integer SbwItemPointerQuest,Tisk,RefDatCorrespondOld(MxRefBlock),
     1        RefDatCorrespondPom(MxRefBlock),UseTabsIn
      logical CrwLogicQuest,Prazdno,WizardModeIn
      allocatable Radka
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      WizardModeIn=WizardMode
      ich=0
      KDatBlockIn=KDatBlock
      KRefBlockIn=KRefBlock
      KPhaseIn=KPhase
      if(StatusM50.gt.100) then
        call FeChybne(-1.,-1.,'M50 doesn''t contain either parameters '
     1              //'or symmetry information.',' ',SeriousError)
        ich=1
        go to 9900
      endif
      if(kcommenMax.ne.0) then
        if(LstOpened) then
          Tisk=1
        else
          Tisk=0
        endif
        call comsym(0,Tisk,ich)
        call iom50(0,0,fln(:ifln)//'.m50')
      endif
      if(ExistFile(fln(:ifln)//'.m95')) then
        ExistM95=.true.
      else
        call FeChybne(-1.,-1.,'M90 file cannot be created as file',
     1                fln(:ifln)//'.m95 doesn''t exist.',SeriousError)
        ich=1
        go to 9900
      endif
1000  call iom95(0,fln(:ifln)//'.m95')
      call CopyVekI(RefDatCorrespond,RefDatCorrespondOld,MxRefBlock)
      if(ExistM90) then
        call iom90(0,fln(:ifln)//'.m90')
      else if(ParentM90.ne.' ') then
        call iom90(0,ParentM90)
      else
        NDatBlock=0
        LamAve(1)=-1.
      endif
      NDatBlockOld=NDatBlock
      do i=1,NDatBlock
        if(LamAve(i).lt.0.) then
          call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          NDatBlock=0
          cycle
        endif
      enddo
      do i=1,NRefBlock
        k=RefDatCorrespond(i)
        if(k.gt.NDatBlock.or.k.le.0) then
          call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          NDatBlock=0
          exit
        endif
      enddo
      n=0
      if(ParentM90.ne.' ') call iom90(0,ParentM90)
      do 1120i=1,NRefBlock
        k=RefDatCorrespond(i)
        if(DifCode(i).lt.100.and.
     1     (RadiationRefBlock(i).ne.ElectronRadiation.or.
     2      .not.CalcDyn)) n=n+1
        if(k.le.0) then
          if(DifCode(i).lt.100.and.
     1       (RadiationRefBlock(i).ne.ElectronRadiation.or.
     2      .not.CalcDyn)) then
            do j=1,NDatBlock
              if(RadiationRefBlock(i).eq.Radiation(j))
     1          then
                if(Radiation(j).eq.NeutronRadiation.or.
     1             abs(LamAve(j)-
     2                 LamAveRefBlock(i)).lt..00001) then
                  RefDatCorrespond(i)=j
                  ModifyDatBlock(j)=.true.
                  if(ParentM90.eq.' ') then
                    call SetBasicM90(NDatBlock)
                    call SetBasicM90FromM95(i,NDatBlock)
                  endif
                  go to 1120
                endif
              endif
            enddo
          endif
          NDatBlock=NDatBlock+1
          ModifyDatBlock(NDatBlock)=.true.
          RefDatCorrespond(i)=NDatBlock
          k=NDatBlock
        else
          ModifyDatBlock(k)=ModifiedRefBlock(i).or.ModifyDatBlock(k)
        endif
        if(ParentM90.eq.' '.and.ModifyDatBlock(k)) then
          call SetBasicM90(NDatBlock)
          call SetBasicM90FromM95(i,NDatBlock)
        endif
1120  continue
      if(n.gt.1) then
        if(SilentRun) go to 1300
        Veta='Modify grouping of data collection blocks into '//
     1       'refinement blocks'
        id=NextQuestId()
        xqd=600.
        il=nint(float((NRefBlock+2)*8)*.1)
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
        il=-18
        xpom=5.
        Veta='Data collection block'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        tpom=0.
        do i=1,NRefBlock
          il=il-8
          call ExtractFileName(SourceFileRefBlock(i),Veta)
          if(DifCode(i).gt.200) then
            t256='->Powder TOF or ED'
          else if(DifCode(i).gt.100) then
            t256='->Powder CW'
          else
            t256='->Single crystal'
          endif
          Veta=Veta(:idel(Veta))//t256(:idel(t256))
          if(RadiationRefBlock(i).eq.XRayRadiation) then
            Cislo='/X-rays/'
          else if(RadiationRefBlock(i).eq.NeutronRadiation) then
            Cislo='/Neutrons/'
          else if(RadiationRefBlock(i).eq.ElectronRadiation) then
            Cislo='/Electrons/'
          else
            Cislo='/-/'
          endif
          Veta=Veta(:idel(Veta))//Cislo
          k=KLamRefBlock(i)
          if(RadiationRefBlock(i).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAveRefBlock(i).gt.0.) then
              write(Cislo,'(f10.6)') LamAveRefBlock(i)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          Veta=Veta(:idel(Veta))//Cislo
          call FeQuestLblMake(id,xpom,il,Veta,'L','N')
          tpom=max(tpom,FeTxLength(Veta)+20.)
        enddo
        il=-10
        xpom=tpom
        Veta='Data refinement block'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        il=il-8
        do i=1,NRefBlock
          write(Veta,'(''#'',i5)') i
          call Zhusti(Veta)
          call FeQuestLblMake(id,xpom,il,Veta,'L','N')
          xpom=xpom+30.
        enddo
        do i=1,NRefBlock
          il=il-8
          xpom=tpom+3.
          do j=1,i
            call FeQuestCrwMake(id,tpom,il,xpom,il,' ','L',CrwXd,CrwYd,
     1                          0,i)
            call FeQuestCrwOpen(CrwLastMade,j.eq.RefDatCorrespond(i))
            if(i.eq.1.and.j.eq.1) nCrwPrv=CrwLastMade
            xpom=xpom+30.
          enddo
        enddo
1200    call FeQuestEVent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
          n=0
          nCrw=nCrwPrv
          do i=1,NRefBlock
            do j=1,i
              if(CrwLogicQuest(nCrw)) then
                RefDatCorrespondPom(i)=j
                n=max(j,n)
              endif
              nCrw=nCrw+1
            enddo
          enddo
          Prazdno=.false.
          do j=1,n
            m=0
            mp=0
            mm=0
            mn=0
            mx=0
            me=0
            do i=1,NRefBlock
              if(RefDatCorrespondPom(i).eq.j) then
                m=m+1
                if(DifCode(i).gt.100) then
                  mp=mp+1
                else
                  if(RadiationRefBlock(i).eq.XRayRadiation) then
                    mx=mx+1
                  else if(RadiationRefBlock(i).eq.NeutronRadiation) then
                    mn=mn+1
                  else if(RadiationRefBlock(i).eq.ElectronRadiation)
     1              then
                    me=me+1
                  endif
                endif
              endif
            enddo
            if(m.eq.0) then
              Prazdno=.true.
              jp=j
            else if(m.gt.0.and.Prazdno) then
              write(Cislo,'(i5)') jp
              call Zhusti(Cislo)
              Veta='the reflection block #'//Cislo(:idel(Cislo))//
     1             ' is empty but higher block(s) are not.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
            write(Cislo,'(i5)') j
            call Zhusti(Cislo)
            if(mp.gt.1) then
              Veta='the reflection block #'//Cislo(:idel(Cislo))//
     1             ' contains more than one powder diffraction set.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
            if((mx.gt.0.and.(mn.gt.0.or.me.gt.0)).or.
     1         (mn.gt.0.and.(mx.gt.0.or.me.gt.0)).or.
     2         (me.gt.0.and.(mx.gt.0.or.mn.gt.0))) then
              Veta='the combined reflection block #'//
     1             Cislo(:idel(Cislo))//
     2             ' contains data collected by different type of '//
     3             'radiation.'
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1220
            endif
          enddo
          QuestCheck(id)=0
          go to 1200
1220      call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1200
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1200
        endif
        if(ich.eq.0) then
          nCrw=nCrwPrv
          do i=1,NRefBlock
            do j=1,i
              if(CrwLogicQuest(nCrw)) then
                if(j.ne.RefDatCorrespond(i)) then
                  ModifyDatBlock(j)=.true.
                  RefDatCorrespond(i)=j
                  if(j.gt.NDatBlock) then
                    NDatBlock=j
                    call SetBasicM90(j)
                    call SetBasicM90FromM95(j,i)
                  endif
                endif
              endif
              nCrw=nCrw+1
            enddo
          enddo
        endif
        call FeQuestRemove(id)
        go to 1500
1300    ModifyDatBlock=.true.
        RefDatCorrespondOld=0
      endif
1500  do i=1,NDatBlock
        if(ModifyDatBlock(i)) then
          do j=1,NRefBlock
            if(RefDatCorrespond(j).eq.i) then
              if(RefDatCorrespond(j).ne.RefDatCorrespondOld(j).or.
     1           RefDatCorrespondOld(j).eq.0)
     2          call SetBasicM90FromM95(j,i)
              ich=0
              go to 2500
            endif
          enddo
        endif
      enddo
      if(Klic.eq.0) go to 9900
      if(NDatBlock.eq.1.or.SilentRun) then
        ModifyDatBlock(KDatBlock)=.true.
        go to 2500
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_m90list.tmp','formatted',
     1              'unknown')
      n=0
      do i=1,NDatBlock
        if(NRef90(i).le.0) cycle
        if(KRefBlock.le.0) KRefBlock=i
        n=n+1
        k=1
        Veta=' '
        do j=1,NRefBlock
          if(RefDatCorrespond(j).eq.i) then
            write(Cislo,'(i2,'','')') j
            k=k+1
            call Zhusti(Cislo)
            idl=idel(Veta)
            Veta(idl+1:)=Cislo(:idel(Cislo))
          endif
        enddo
        j=idel(Veta)
        Veta(j:j)=' '
        Veta='#'//Veta(:j-1)
        j=j+1
        if(k.gt.1) Veta='s'//Veta(:j)
        write(Cislo,100) i
        call Zhusti(Cislo)
        Veta=Cislo(:idel(Cislo))//'-made of Refblock'//
     1       Veta(:idel(Veta))
        Veta=Veta(:idel(Veta))//Tabulator//'|'//Tabulator
        if(iabs(DataType(i)).eq.2) then
          VetaPom='Powder'
        else if(DataType(i).eq.1) then
          VetaPom='Single crystal'
        endif
        Veta=Veta(:idel(Veta))//VetaPom(:idel(VetaPom))//Tabulator//
     1       '|'//Tabulator
        if(Radiation(i).eq.XRayRadiation) then
          RadString(i)='X-rays'
        else if(Radiation(i).eq.NeutronRadiation) then
          RadString(i)='Neutrons'
        else
          RadString(i)=' '
        endif
        k=KLam(i)
        if(Radiation(i).eq.XRayRadiation.and.k.gt.0) then
          Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
        else
          if(LamAve(i).gt.0.) then
            write(Cislo,'(f10.6)') LamAve(i)
            call ZdrcniCisla(Cislo,1)
          else
            Cislo='TOF'
          endif
        endif
        RadString(i)=RadString(i)(:idel(RadString(i))+1)//
     1               Cislo(:idel(Cislo))
        Veta=Veta(:idel(Veta))//RadString(i)(:idel(RadString(i)))
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      id=NextQuestId()
      xqd=600.
      il=1
      Veta='Create refinement reflection file'
      WizardMode=.false.
      ilk=min(ifix((float(n)*MenuLineWidth+2.)/QuestLineWidth)+3,15)
      ilk=max(ilk,6)
      dpom=xqd-20.-SbwPruhXd
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,-1,0)
      call FeQuestLblMake(id,           20.,il,'File'     ,'L','N')
      call FeQuestLblMake(id,   dpom/2.+10.,il,'Type'     ,'L','N')
      call FeQuestLblMake(id,3.*dpom/4.+10.,il,'Radiation','L','N')
      il=ilk-1
      xpom=10.+EdwMarginSize
      call FeTabsReset(UseTabs)
      call FeTabsAdd(dpom/2.-xpom,UseTabs,IdRightTab,' ')
      call FeTabsAdd(dpom/2.-xpom+10.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(3.*dpom/4.-xpom,UseTabs,IdRightTab,' ')
      call FeTabsAdd(3.*dpom/4.-xpom+10.,UseTabs,IdRightTab,' ')
      xpom=10.
      il=ilk-1
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_m90list.tmp')
      dpom=xqd-10.
      xpom=5.
      il=ilk
      dpom=70.
      spom=15.
      tpom=dpom+spom
      xpom=xqd*.5-dpom*1.5-spom*.5
      do i=1,3
        if(i.eq.1) then
          Veta='%Info'
        else if(i.eq.2) then
          nButtInfo=ButtonLastMade
          Veta='%Rebuild'
        else if(i.eq.3) then
          nButtRebuild=ButtonLastMade
          Veta='%Delete'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(n.le.0) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+tpom
      enddo
      nButtDelete=ButtonLastMade
2150  call FeQuestEVent(id,ich)
      if(CheckType.eq.EVentButton) then
        k=SbwItemPointerQuest(nSbw)
2162    if(CheckNumber.eq.nButtInfo) then
          NInfo=1
          write(Cislo,100) k
          TextInfo(NInfo)='DatBlock'//Cislo(:idel(Cislo))//
     1                    ' is made from file(s):'
          do i=1,NRefBlock
            if(RefDatCorrespond(i).ne.k) cycle
            NInfo=NInfo+1
            TextInfo(NInfo)=SourceFileRefBlock(i)
          enddo
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          go to 2150
        else if(CheckNumber.eq.nButtDelete) then
          NRef90(k)=0
          call FeQuestRemove(id)
          call FeTabsReset(UseTabs)
          do i=1,NRefBlock
            if(RefDatCorrespond(i).eq.k) then
              nref95(i)=0
            else if(RefDatCorrespond(i).gt.k) then
              RefDatCorrespond(i)=RefDatCorrespond(i)-1
            endif
          enddo
          KDatBlock=1
          call UpdateDatBlocksM40
          call CompleteM95(0)
          call CompleteM90
          if(ExistM40) call iom40(1,0,fln(:ifln)//'.m40')
          if(ExistM90) go to 1000
        else if(CheckNumber.eq.nButtRebuild) then
          ModifyDatBlock(k)=.true.
          call FeQuestRemove(id)
          call DeleteFile(fln(:ifln)//'_m90list.tmp')
          go to 2500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2150
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_m90list.tmp')
      call FeTabsReset(UseTabs)
      go to 9900
2500  NDatBlockSingle=0
      NDatBlockPowder=0
      do KDatBlock=1,NDatBlock
        if(DataType(KDatBlock).eq.1) then
          NDatBlockSingle=NDatBlockSingle+1
        else
          NDatBlockPowder=NDatBlockPowder+1
        endif
      enddo
      call FeTabsReset(UseTabs)
      Veta=fln(:ifln)//'.rre'
      if(ExistFile(Veta)) then
        call OpenFile(lst,fln(:ifln)//'.rre','formatted','unknown')
        ln=NextLogicNumber()
        Konec=0
        allocate(Radka(mxline))
        FileNameLst=' '
2610    do i=1,mxline
          read(lst,FormA256,end=2630) Radka(i)
        enddo
        i=mxline
        go to 2635
2630    Konec=1
2635    do j=1,i
          do k=1,5
            l=LocateSubstring(Radka(j),
     1                        LabelsEditm9(k)(:idel(LabelsEditm9(k))),
     2                        .false.,.true.)
            if(l.gt.0) go to 2645
          enddo
          cycle
2645      l=LocateSubstring(Radka(j),'#',.false.,.true.)
          if(l.ne.0) then
            Cislo=Radka(j)(l+1:l+2)
            kk=0
            call StToInt(Cislo,kk,ip,1,.false.,ich)
            if(ich.eq.0) then
              l=ip(1)
            else
              l=1
            endif
          else
            l=1
          endif
          write(Cislo,'(i2,''_'',i2)') k,l
          call Zhusti(Cislo)
          FileNameLst=fln(:ifln)//'_em9_'//Cislo(:idel(Cislo))//'.tmp'
          call CloseIfOpened(ln)
          call OpenFile(ln,FileNameLst,'formatted','unknown')
          exit
        enddo
        if(FileNameLst.ne.' ') then
          do j=1,i
            write(ln,FormA) Radka(j)(:idel(Radka(j)))
          enddo
        endif
        if(Konec.eq.0) go to 2610
        call CloseIfOpened(ln)
        deallocate(Radka)
      endif
!      if(.not.ExistM90) then
!        ExistM90=.true.
!        call iom50(1,0,fln(:ifln)//'.m50')
!        ExistM90=.false.
!      endif
      n=0
      do KDatBlock=1,NDatBlock
        if(ModifyDatBlock(KDatBlock)) then
          if(DataType(KDatBlock).eq.1) then
            n=n+1
            isPowder=.false.
            KPh=0
            if(maxNDim.gt.3) then
              do KPhP=1,NPhase
                if(maxNDim.eq.NDim(KPhP)) then
                  KPh=KPhP
                endif
              enddo
            endif
            KPhase=max(KPh,1)
            call SetSatGroups
            call EM9CreateM90Single(n,ich)
          else
            isPowder=.true.
            call EM9CreateM90Powder(ich)
          endif
          if(ich.ne.0) then
            call CloseIfOpened(lst)
            go to 9900
          endif
          ModifyDatBlock(KDatBlock)=.false.
        endif
      enddo
      if(NDatBlockSingle.ge.1) then
        call OpenFile(lst,fln(:ifln)//'.rre','formatted','unknown')
        do KDatBlock=1,NDatBlock
          ln=NextLogicNumber()
          npg=0
          do i=1,5
            FileNameLst=fln(:ifln)//'_em9_'
            write(Cislo,FormI15) i
            call Zhusti(Cislo)
            FileNameLst=FileNameLst(:idel(FileNameLst))//
     1                  Cislo(:idel(Cislo))//'_'
            write(Cislo,FormI15) KDatBlock
            call Zhusti(Cislo)
            FileNameLst=FileNameLst(:idel(FileNameLst))//
     1                  Cislo(:idel(Cislo))//'.tmp'
            if(.not.ExistFile(FileNameLst)) cycle
            call OpenFile(ln,FileNameLst,'formatted','unknown')
3020        read(ln,FormA256,end=3040) t256
            j=index(t256,'page=')
            if(j.gt.0) then
              npg=npg+1
              write(Cislo,'(i3)') npg
              t256(j+5:)=Cislo(:3)
            endif
            write(lst,FormA) t256(:idel(t256))
            go to 3020
3040        call CloseIfOpened(ln)
            call DeleteFile(FileNameLst)
          enddo
        enddo
        call CloseIfOpened(lst)
      endif
      KDatBlock=1
      if(ParentStructure) then
        call UpdateDatBlocksM40
        call iom40Only(1,0,fln(:ifln)//'.m44')
        call SSG2QMag(fln)
        call UpdateDatBlocksM40
        call iom40Only(1,0,fln(:ifln)//'.m40')
        call QMag2SSG(fln,0)
        call iom40Only(0,0,fln(:ifln)//'.m44')
      else
        call UpdateDatBlocksM40
      endif
!      if(ParentStructure) then
!        call iom40Only(1,0,fln(:ifln)//'.m40')
!        call QMag2SSG(fln,0)
!        call iom40Only(0,0,fln(:ifln)//'.m44')
!      endif
      call CompleteM95(0)
      call CompleteM90
      if(UpdateAnomalous) then
        call ReallocFormF(NAtFormula(1),NPhase,NDatBlock)
        do KPh=1,NPhase
          do i=1,NAtFormula(KPh)
            do KDatB=NDatBlockOld+1,NDatBlock
              if(Radiation(KDatB).eq.XRayRadiation) then
                if(KAnRef(KDatB).ne.1) then
                  if(klam(KDatB).gt.0) then
                    call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f''',
     1                           ffra(i,KPh,KDatB),klam(KDatB),ich)
                    call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f"',
     1                           ffia(i,KPh,KDatB),klam(KDatB),ich)
                  else
                    call EM50ReadAnom(AtTypeFull(i,KPh),
     1                ffra(i,KPh,KDatB),ffia(i,KPh,KDatB),KDatB)
                  endif
                endif
              else if(Radiation(KDatB).eq.NeutronRadiation) then
                ffra(i,KPh,KDatB)=0.
                ffia(i,KPh,KDatB)=0.
              endif
            enddo
          enddo
        enddo
      endif
      if(ExistM40) then
        if(ParentStructure) then
          call iom40Only(1,0,fln(:ifln)//'.m44')
        else
          call iom40Only(1,0,fln(:ifln)//'.m40')
        endif
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(Klic.ne.0.and.NDatBlock.gt.1.and..not.SilentRun) go to 1500
9900  WizardMode=.false.
      KDatBlock=min(KDatBlockIn,NDatBlock)
      KRefBlock=KRefBlockIn
      KPhase=KPhaseIn
      call DeleteFile(PreviousM50)
      WizardMode=WizardModeIn
      UseTabs=UseTabsIn
      return
100   format('#',i2)
      end
