      subroutine EM9ImportMagnetic(WhatToDo,CoDal)
      use Atoms_mod
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(36),xp(3)
      character*80  Veta
      integer WhatToDo,CoDal
      logical StructureExists,UzTuByl
      equivalence (IdNumbers(0),IdMagneticManual),
     1            (IdNumbers(1),IdMagneticSHELX),
     2            (IdNumbers(2),IdMagneticCIF),
     3            (IdNumbers(3),IdMagneticJana)
      UzTuByl=.false.
1100  if(WhatToDo.eq.IdMagneticManual) then
        if(UzTuByl) then
          go to 1300
        else
          call FeFillTextInfo('em9importmagnetic1.txt',0)
          call FeWizardTextInfo('INFORMATION',1,1,1,ich)
          if(ich.ne.0) then
            CoDal=ich
            go to 9999
          endif
          call SetBasicM40(.true.)
        endif
1200    ExistM90=.true.
        call EditM50(ich)
        ExistM90=.false.
        call iom50(0,0,Fln(:iFln)//'.m50')
        if(ich.ne.0) then
          CoDal=ich
          go to 9999
        endif
        UzTuByl=.true.
        call CopyFile(Fln(:iFln)//'.m50',PreviousM50)
1300    if(NAtFormula(KPhase).gt.0) then
          call EM40NewAt(ich)
          if(ich.ne.0) then
            if(ich.gt.0) then
              UzTuByl=.false.
              go to 1200
            else
              CoDal=ich
              go to 9999
            endif
          endif
          call iom40Only(1,0,Fln(:iFln)//'.m40')
          call CopyFile(Fln(:iFln)//'.m40',PreviousM40)
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticSHELX) then
        call ReadSHELX(-1,ich)
        if(ErrFlag.ne.0) then
          ich=1
          go to 9900
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticCIF) then
        call DeleteFile(fln(:ifln)//'.m90')
        call DeleteFile(fln(:ifln)//'.m95')
        ExistM90=.false.
        ExistM95=.false.
        call ReadCIF(1,CoDal)
        if(ErrFlag.ne.0) then
          ich=1
          go to 9900
        endif
        if(isPowder) then
          call DeleteFile(fln(:ifln)//'.m41')
          isPowder=.false.
        endif
        Veta=Fln
      else if(WhatToDo.eq.IdMagneticJana) then
        Veta=' '
2100    call FeFileManager('Define input Jana structure',Veta,' ',1,
     1                     .false.,ich)
        if(ich.ne.0) go to 9900
        if(.not.StructureExists(Veta)) then
          call FeChybne(-1.,-1.,'The structure "'//
     1                  Veta(:idel(Veta))//'" doesn''t exist.',
     2                  ' ',SeriousError)
          go to 2100
        endif
        call iom50(0,0,Veta(:idel(Veta))//'.m50')
        call iom40Only(0,0,Veta(:idel(Veta))//'.m40')
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      NPhase=1
      KPhase=1
      NQMag(KPhase)=0
      call SetRealArrayTo(QMag(1,1,KPhase),3,0.)
      call DeleteFile(Veta(:idel(Veta))//'.l51')
      call SetRealArrayTo(sc ,MxSc,0.)
      call SetRealArrayTo(scs,MxSc,0.)
      sc(1,1)=1.
      call SetRealArrayTo(sctw ,MxSc,0.)
      call SetRealArrayTo(sctws,MxSc,0.)
      OverAllB (1)=0.
      OverAllBs(1)=0.
      call SetRealArrayTo(ec ,12,0.)
      call SetRealArrayTo(ecs,12,0.)
      call SetRealArrayTo(ecMag ,2,0.)
      call SetRealArrayTo(ecsMag,2,0.)
      call CrlAtomNamesIni
      if(NDimI(KPhase).gt.0) then
        do j=1,NComp(KPhase)
          do i=1,NSymm(KPhase)
            call MatBlock3(rm6(1,i,j,KPhase),rmp,NDim(KPhase))
            call CopyMat(rmp,rm6(1,i,j,KPhase),3)
            call SetRealArrayTo(s6(4,i,j,KPhase),NDimI(KPhase),0.)
            ZMag(i,j,KPhase)=1.
          enddo
          do i=1,NLattVec(KPhase)
            call SetRealArrayTo(vt6(4,i,j,KPhase),NDimI(KPhase),0.)
          enddo
        enddo
        NDim(KPhase)=3
        NDimI(KPhase)=0
        NDimQ(KPhase)=9
        call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      endif
      do i=1,NAtAll
        MagPar(i)=0
        call SetIntArrayTo(KModA(1,i),7,0)
        call SetIntArrayTo(KFA(1,i),7,0)
        sai(i)=0.
        if(itf(i).ge.2) then
          call SetRealArrayTo(sx(1,i),3,0.)
          call SetRealArrayTo(sbeta(1,i),6,0.)
          call ZmTF21(i)
        endif
      enddo
      MagneticType(KPhase)=1
      MaxMagneticType=1
      if(.not.allocated(ZMag)) then
        allocate(ZMag(NSymm(KPhase),NComp(KPhase),NPhase),
     1           RMag(9,NSymm(KPhase),NComp(KPhase),NPhase))
        call SetRealArrayTo(ZMag,NSymm(KPhase)*NComp(KPhase)*NPhase,1.)
        call SetRealArrayTo(RMag,9*NSymm(KPhase)*NComp(KPhase)*NPhase,
     1                      0.)
      endif
      n=NAtAll
      if(.not.allocated(NamePolar))
     1  allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
      call EM9ImportMagneticFF(CoDal)
      if(CoDal.eq.0) then
        Radiation(1)=NeutronRadiation
        LamAve(1)=1.
        LamA1(1)=1.
        LamA2(1)=1.
        LamRat(1)=0.
        LPFactor(1)=PolarizedLinear
        ParentStructure=.true.
        NCommQProduct(1,KPhase)=1
        if(NQMag(KPhase).gt.0) then
          do 3250j=1,2
            do i=1,3
              xp(i)=QMag(i,1,KPhase)*float(j)
              if(abs(anint(xp(i))-xp(i)).gt..001) go to 3250
            enddo
            KCommen(KPhase)=1
            ICommen(KPhase)=0
            KCommenMax=1
            do i=1,3
              if(xp(i).le.0.) then
                NCommen(i,1,KPhase)=1
              else
                NCommen(i,1,KPhase)=j
              endif
            enddo
            NCommQProduct(1,KPhase)=NCommQProduct(1,KPhase)*j
            trez(1,1,KPhase)=0.
            exit
3250      continue
        endif
        call QMag2SSG(fln,0)
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40Only(0,0,fln(:ifln)//'.m44')
      else if(WhatToDo.eq.IdMagneticManual) then
        if(CoDal.gt.0) go to 1100
      endif
      go to 9999
9900  if(ich.ne.0) then
        CoDal=-1
      endif
9999  return
      end
