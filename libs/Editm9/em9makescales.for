      subroutine EM9MakeScales(kiqIn,KlicIn,ich)
      use Basic_mod
      use EDZones_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*256 Veta
      character*14 :: Label = 'Reflections I>'
      integer, allocatable :: Used1(:)
      integer EdwStateQuest,ihmx(6),ihd(6)
      logical lpom,CrwLogicQuest
      real, allocatable :: fia(:),sigfia(:)
      save iz,N1Obs,flimp,fia,sigfia
      kiq=kiqIn
      Klic=KlicIn
      FromWhere=AveFromEditM9
      go to 1100
      entry DRMakeScales(kiqIn,KlicIn,ich)
      kiq=kiqIn
      Klic=KlicIn
      FromWhere=AveFromManualCull
      iz=0
1100  ich=0
      if(Klic.eq.0) then
        if(EM9ScaleLim(KDatBlock).le.0.) then
          flimp=10.
        else
          flimp=EM9ScaleLim(KDatBlock)
        endif
        if(.not.SilentRun) then
          id=NextQuestId()
          if(TitleSingleCrystal.ne.' ') call FeQuestTitleMake(id,
     1       TitleSingleCrystal(:idel(TitleSingleCrystal)))
          il=0
          Veta='%determine a unique scale'
          xpom=5.
          tpom=xpom+CrwgXd+10.
          do i=1,3
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='Use %multi-scale data'
              nCrwUniqueScale=CrwLastMade
            else if(i.eq.2) then
              Veta='%Ignore scale flags'
              nCrwMultiScale=CrwLastMade
            else
              nCrwIgnoreScale=CrwLastMade
            endif
            if((i.eq.1.and.EM9ScaleLim(KDatBlock).gt.0.).or.
     1         (i.eq.2.and.EM9ScaleLim(KDatBlock).lt.0.).or.
     2         (i.eq.3.and.EM9ScaleLim(KDatBlock).eq.0.)) then
              lpom=.true.
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
          enddo
          if(kiqIn.gt.18) then
            call FeQuestCrwOn(nCrwIgnoreScale)
            call FeQuestCrwDisable(nCrwMultiScale)
            call FeQuestCrwDisable(nCrwUniqueScale)
          endif
          il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          tpom=5.
          call FeQuestLblMake(id,tpom,il,Label,'L','N')
          nLblRefl=LblLastMade
          xpom=tpom+FeTxLength(Label)+10.
          dpom=40.
          tpom=xpom+dpom+10.
          call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                        '*sig(I) will be %used in the process',
     2                        'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,FLimP,.false.,.false.)
          nEdwFLim=EdwLastMade
1200      if(CrwLogicQuest(nCrwUniqueScale)) then
            call FeQuestLblOn(nLblRefl)
            call FeQuestRealEdwOpen(nEdwFLim,FLimP,.false.,.false.)
            EM9ScaleLim(KDatBlock)=FLimP
            NoOfScales=1
          else
            call FeQuestLblDisable(nLblRefl)
            if(EdwStateQuest(nEdwFLim).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdwFLim,FLimP)
              call FeQuestEdwDisable(nEdwFLim)
              NoOfScales(KDatBlock)=1
            endif
            if(CrwLogicQuest(nCrwMultiScale)) then
              EM9ScaleLim(KDatBlock)=-1.
              NoOfScales(KDatBlock)=kiq
            else
              EM9ScaleLim(KDatBlock)=0.
              NoOfScales(KDatBlock)=1
            endif
          endif
1500      call FeQuestEvent(id,ich)
          if(CheckType.eq.EventCrw) then
            go to 1200
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1500
          endif
          if(ich.eq.0) then
            if(CrwLogicQuest(nCrwUniqueScale))
     1        call FeQuestRealFromEdw(nEdwFLim,EM9ScaleLim(KDatBlock))
          endif
          if(ich.ne.0) go to 9999
        endif
        iz=0
      else if(Klic.eq.1) then
        N1Obs=0
        call SetIntArrayTo(ihmx,6,0)
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
3000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=3200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 3000
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 3200
          if(ICullAr(nr).ne.0) go to 3000
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
        do i=1,NSymmN(1)
          call IndTr(ih,rm6(1,i,1,1),ihp,maxNDim)
          do j=1,maxNDim
            ihmx(j)=max(ihmx(j),iabs(ihp(j)))
          enddo
        enddo
        if(iq.ne.1.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 3000
        N1Obs=N1Obs+1
        go to 3000
3200    if(allocated(fia)) deallocate(fia,sigfia,irec,irecp,Used1)
        allocate(fia(N1Obs),sigfia(N1Obs),irec(N1Obs),irecp(N1Obs),
     1           Used1(N1Obs))
        scref(1)=N1Obs
        if(N1Obs.lt.5) then
          call FeChybne(-1.,YBottomMessage,'the number of selected '//
     1                  'reflections for the first scale too small.',
     1                  ' ',SeriousError)
          ich=1
          go to 9900
        endif
        call SetIntArrayTo(ihd,6,1)
        j=1
        do i=1,NDim(KPhase)
          ihd(i)=2*ihmx(i)+1
          if(imax/j.lt.ihd(i)) go to 8000
          j=j*ihd(i)
        enddo
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
        n=0
3300    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=9999)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 3300
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 9999
          if(ICullAr(nr).ne.0) go to 3300
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          ri=RIAr(nr)
          rs=RSAr(nr)
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
        endif
        if(iq.ne.1.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 3300
        n=n+1
        mx=IndPack(ih,ihd,ihmx,maxNDim)
        fia(n)=ri
        sigfia(n)=rs
        irec(n)=mx
        irecp(n)=iatw
        go to 3300
      else if(Klic.eq.2) then
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
        N2Used=0
        N2Obs=0
        NPairs=0
        swt=0.
        srel=0.
        srelq=0.
        Used1=0
4000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=4200)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation)
     1      iq=KRefBlock
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
          if(nxx.ne.0) go to 4000
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 4200
          if(ICullAr(nr).ne.0) go to 4000
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
        if(iq.ne.kiq.or.ri.lt.EM9ScaleLim(KDatBlock)*rs) go to 4000
        N2Obs=N2Obs+1
        m=0
        do i=1,NSymmN(1)
          call IndTr(ih,rm6(1,i,1,1),ihp,maxNDim)
          mx=IndPack(ihp,ihd,ihmx,maxNDim)
          do j=1,N1Obs
            if(mx.eq.irec(j).and.iatw.eq.irecp(j).and.
     1         fia(j).ge.EM9ScaleLim(KDatBlock)*sigfia(j)) then
              Used1(j)=1
              if(m.eq.0) N2Used=N2Used+1
              m=m+1
              NPairs=NPairs+1
              rel=fia(j)/ri
              wt=1./(rel**2*((sigfia(j)/fia(j))**2+(rs/ri)**2))
              srel=srel+wt*rel
              srelq=srelq+(wt*rel)**2
              swt=swt+wt
            endif
          enddo
        enddo
        go to 4000
4200    if(N2Used.lt.5) then
          call FeChybne(-1.,YBottomMessage,'the number of common '//
     1                  'reflections too small.',
     2                  'Try another method or modify the minimal '//
     3                  'I/sig(I) ratio.',
     4                  SeriousError)
          ich=1
          go to 9900
        else
          sckor(kiq)=srel/swt
!          sckors(kiq)=sqrt((srelq-srel**2/swt)/(float(m-1)*swt))
          sckors(kiq)=sqrt(srelq)/swt
          N1Used=0
          do i=1,N1Obs
            if(Used1(i).gt.0) N1Used=N1Used+1
          enddo
          sccom1(kiq)=N1Used
          sccom(kiq)=N2Used
          scref(kiq)=N2Obs
          scpairs=NPairs
          go to 9999
        endif
      else if(Klic.eq.3) then
        if(FromWhere.eq.AveFromEditM9) then
          rewind 91
        else
          nr=0
        endif
5000    if(FromWhere.eq.AveFromEditM9) then
          read(91,format91,end=9900)(ih(i),i=1,maxNDim),ri,rs,iq,nxx,
     1                               itw,tbar,ReadLam,DirCos,pom,
     2                               NoRefBlock
          iqp=iq
          no=NoRefBlock/1000
          KRefBlock=mod(NoRefBlock,1000)
          if(RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
            iq=KRefBlock
            if(.not.CalcDyn) iqp=1
          endif
          iatw=iabs(itw)
          call FeFlowChartEvent(iz,ie)
          if(ie.ne.0) then
            call FeBudeBreak
            if(ErrFlag.ne.0) go to 9000
          endif
        else
          nr=nr+1
          if(nr.gt.NRefRead) go to 9900
          call IndUnPack(HCondAr(1,nr),ihp,HCondLn,HCondMx,NDim(KPhase))
          if(FromWhere.eq.AveFromSGTest) then
            call indtr(ihp,TrMP,ih,NDim(KPhase))
          else
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          iatw=1
          iq=mod(RefBlockAr(nr),1000)
          iqp=iq
          ri=RIAr(nr)
          rs=RSAr(nr)
        endif
!        if(iq.ne.1) then
          ri=ri*sckor(iq)
          rs=rs*sckor(iq)
          iqp=1
!        endif
        if(FromWhere.eq.AveFromEditM9) then
          write(92,format91)(ih(i),i=1,maxNDim),ri,rs,iqp,nxx,itw,
     1                       tbar,ReadLam,DirCos,0.,NoRefBlock
        else
          RIAr(nr)=ri
          RSAr(nr)=rs
        endif
        go to 5000
      endif
      go to 9999
8000  call FeChybne(-1.,YBottomMessage,'diffraction indices are too '//
     1              'large to be sorted.',' ',SeriousError)
      ich=2
      go to 9900
9000  ich=1
      ErrFlag=0
9900  if(allocated(fia)) deallocate(fia,sigfia,irec,irecp,Used1)
9999  return
      end
