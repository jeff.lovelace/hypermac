      subroutine EM9ImportDatRedData(Navrat)
      use Basic_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'profil.cmn'
      include 'powder.cmn'
      character*256 EdwStringQuest,Veta,p80,mene(25),FileKoala(2)
      character*1   ChNDim
      dimension QSymm(3,3),TrMP3(3,3),TrMP6(36),xp(3)
      logical ExistFile,FeYesNoHeader,EqIgCase,DelejBasic,
     1        CrwLogicQuest,EqIV0,ObnovCell
      integer Order(25),OrderSel,DatRedData,WhatToDo,EdwStateQuest,
     1        BratJineItw,StavVolani,FeCloseBinaryFile
      real CellParOld(6),CellParSUOld(6),QuOld(3,3)
      equivalence (DatRedData     ,IdNumbers(1)),
     1            (ImportSingle   ,IdNumbers(2)),
     2            (ImportCWPowder ,IdNumbers(3)),
     4            (ImportTOFPowder,IdNumbers(4))
      save NSkip
      Order(1:nDiffTypes)=OrderDiffTypes(1:nDiffTypes)
      call CopyStringArray(mene1,mene,nDiffTypes)
      nCrw=nDiffTypes
      WhatToDo=DatRedData
      go to 1010
      entry EM9ImportSingle(Navrat)
      Order(1:nSingleTypes)=OrderSingleTypes(1:nSingleTypes)
      call CopyStringArray(mene2,mene,nSingleTypes)
      WhatToDo=ImportSingle
      nCrw=nSingleTypes
      go to 1010
      entry EM9ImportCWPowder(Navrat)
      Order(1:nCWPowderTypes)=OrderCWPowderTypes(1:nCWPowderTypes)
      call CopyStringArray(mene3,mene,nCWPowderTypes)
      nCrw=nCWPowderTypes
      WhatToDo=ImportCWPowder
      go to 1010
      entry EM9ImportTOFPowder(Navrat)
      Order(1:nTOFPowderTypes)=OrderTOFPowderTypes(1:nTOFPowderTypes)
      call CopyStringArray(mene4,mene,nTOFPowderTypes)
      nCrw=nTOFPowderTypes
      WhatToDo=ImportTOFPowder
1010  if(Navrat.eq.1) then
        call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted',
     1                'old')
        if(ErrFlag.ne.0) go to 9999
        if(isPowder) then
          do i=1,NSkip
            read(71,FormA) Veta
          enddo
          if(DifCode(KRefBlock).eq.IdDataRiet7) then
            read(71,FormA) Veta
          endif
!   Nevim co pro jine
          go to 1600
        else
          go to 1700
        endif
      endif
1015  if(SilentRun) then
        if(CyclicRefMode)
     1    SourceFileRefBlock(KRefBlock)=CyclicRefFile(ICyclicRefFile)
        go to 1080
      endif
      if(nCrw.gt.8) then
         nCrwZalom=(nCrw+1)/2+1
      else
         nCrwZalom=nCrw+1
      endif
      if(WhatToDo.eq.DatRedData) then
        Veta='Data reduction file from:'
      else if(WhatToDo.eq.ImportSingle) then
        Veta='Single crystal data from:'
      else if(WhatToDo.eq.ImportCWPowder) then
        Veta='Powder data from:'
      else if(WhatToDo.eq.ImportTOFPowder) then
        nCrwZalom=7
        Veta='TOF/ED powder data from:'
      endif
      id=NextQuestId()
      call FeQuestTitleMake(id,Veta)
      QuestCheck(id)=1
      ilm=nCrwZalom+2
      il=1
      Veta='Bro%wse'
      dpom=FeTxLengthUnder(Veta)+10.
      xpomb=WizardLength-dpom-5.
      call FeQuestButtonMake(id,xpomb,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      Veta='Fi%le name'
      tpom=5.
      xpom=tpom+10.+FeTxLengthUnder(Veta)
      dpom=xpomb-xpom-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      if(WhatToDo.eq.DatRedData) then
        xpom=25.
        Veta='import merged file "'
        do i=1,2
          tpom=xpom+CrwgYd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,0,1)
          if(i.eq.1) then
            nCrwImportMerged=CrwLastMade
            Veta='import unmerged file "'
            xpom=xpom+WizardLength*.5
          else
            nCrwImportUnmerged=CrwLastMade
          endif
        enddo
      else
        nCrwImportMerged=0
        nCrwImportUnmerged=0
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=15.
      tpom=xpom+CrwgYd+10.
      do i=1,nCrw
        if(i.ne.nCrwZalom) then
          il=il+1
        else
          il=3
          xpom=xpom+WizardLength*.5
          tpom=tpom+WizardLength*.5
        endif
        if(WhatToDo.eq.DatRedData) then
          Veta=men1(i)
        else if(WhatToDo.eq.ImportSingle) then
          Veta=men2(i)
        else if(WhatToDo.eq.ImportCWPowder) then
          Veta=men3(i)
        else if(WhatToDo.eq.ImportTOFPowder) then
          Veta=men4(i)
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) nCrwTypeFr=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      Order(i).eq.DifCode(KRefBlock))
        if(Order(i).eq.DifCode(KRefBlock)) OrderSel=i
      enddo
      nCrwTypeTo=CrwLastMade
      if(WhatToDo.eq.ImportSingle) then
        Veta='Inp%ut format:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=WizardLength-xpom-5.
        call FeQuestEdwMake(id,tpom,ilm,xpom,ilm,Veta,'L',dpom,EdwYd,0)
        nEdwFormat=EdwLastMade
      else
        nEdwFormat=0
      endif
      if(WhatToDo.eq.ImportCWPowder) then
        Veta='Show de%tails about the selected format'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=(WizardLength-dpom)*.5
        call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
        nButtPowderDetails=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=15.
        tpom=xpom+CrwgYd+10.
        il=ilm
        do i=1,4
          il=il+1
          if(i.eq.1) then
            Veta='%Debye-Scherrer method'
          else if(i.eq.2) then
            nCrwMethod=CrwLastMade
            Veta='Bragg-Brentanno method - %Fixed Divergence Slit'
          else if(i.eq.3) then
            Veta='Bragg-Brentanno method - %Variable Divergence Slit'
          else if(i.eq.4) then
            Veta='%Another/unknown method'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,0,3)
          call FeQuestCrwOpen(CrwLastMade,
     1                        mod(i,4).eq.PwdMethodRefBlock(KRefBlock))
        enddo
      else
        nButtPowderDetails=0
        nCrwMethod=0
      endif
1020  if(SourceFileRefBlock(KRefBlock).eq.' ') then
        if(KRefBlock.gt.1.and.
     1     DifCode(KRefBlock).eq.DifCode(KRefBlock-1)) then
          SourceFileRefBlock(KRefBlock)=SourceFileRefBlock(KRefBlock-1)
        else
          k=0
          call Kus(mene(OrderSel),k,Cislo)
          SourceFileRefBlock(KRefBlock)=fln(:ifln)//Cislo(:idel(Cislo))
        endif
      endif
1030  if(DifCode(KRefBlock).eq.IdImportGeneralF.or.
     1   DifCode(KRefBlock).eq.IdImportGeneralI) then
        if(EdwStateQuest(nEdwFormat).ne.EdwOpened)
     1    call FeQuestStringEdwOpen(nEdwFormat,
     2                              FormatRefBlock(KRefBlock))
      else
        call FeQuestEdwDisable(nEdwFormat)
      endif
      if(DifCode(KRefBlock).eq.IdKoala) then
        Veta='Laue4_jana.out'
        if(ExistFile(Veta)) then
          ln=NextLogicNumber()
          call OpenFile(ln,'Laue4_jana.out','formatted','old')
          do i=1,3
            read(ln,FormA,err=1035,end=1035)
          enddo
          do i=1,2
            read(ln,FormA,err=1035,end=1035) FileKoala(i)
          enddo
          call CloseIfOpened(ln)
          call FeQuestEdwClose(nEdwFileName)
          call FeQuestButtonClose(nButtBrowse)
          call FeQuestCrwOpen(nCrwImportMerged,.false.)
          Veta='import merged file "'//
     1         FileKoala(1)(:idel(FileKoala(1)))//'"'
          call FeQuestCrwLabelChange(nCrwImportMerged,Veta)
          call FeQuestCrwOpen(nCrwImportUnmerged,.true.)
          Veta='import unmerged file "'//
     1         FileKoala(2)(:idel(FileKoala(2)))//'"'
          call FeQuestCrwLabelChange(nCrwImportUnmerged,Veta)
          go to 1050
1035      call CloseIfOpened(ln)
          go to 1050
        endif
      endif
      if(nCrwImportMerged.gt.0) then
        call FeQuestCrwClose(nCrwImportMerged)
        call FeQuestCrwClose(nCrwImportUnmerged)
      endif
      call FeQuestStringEdwOpen(nEdwFileName,
     1                          SourceFileRefBlock(KRefBlock))
      call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      if(.not.CreateNewRefBlock) call FeQuestButtonDisable(ButtonEsc)
1050  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        if(nCrwImportMerged.gt.0) then
          if(DifCode(KRefBlock).eq.IdKoala) then
            if(CrwLogicQuest(nCrwImportMerged)) then
              SourceFileRefBlock(KRefBlock)=FileKoala(1)
            else
              SourceFileRefBlock(KRefBlock)=FileKoala(2)
            endif
          else
            SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
          endif
        else
          SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
        endif
        if(ExistFile(SourceFileRefBlock(KRefBlock))) then
          QuestCheck(id)=0
        else
          call FeChybne(-1.,-1.,'the input file "'//
     1                  SourceFileRefBlock(KRefBlock)(:
     2                    idel(SourceFileRefBlock(KRefBlock)))//
     3                  '" does not exist',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFileName
        endif
        go to 1050
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwTypeFr.and.
     1        CheckNumber.le.nCrwTypeTo) then
        if(Order(OrderSel).eq.IdRigakuCCD) then
          Veta='shelx.hkl'
        else
          k=0
          call Kus(mene(OrderSel),k,Cislo)
          Veta=fln(:ifln)//Cislo(:idel(Cislo))
        endif
        OrderSel=CheckNumber-nCrwTypeFr+1
        DifCode(KRefBlock)=Order(OrderSel)
        if(EqIgCase(EdwStringQuest(nEdwFileName),Veta)) then
          if(Order(OrderSel).eq.IdRigakuCCD) then
            Veta='shelx.hkl'
          else
            k=0
            call Kus(mene(OrderSel),k,Cislo)
            Veta=fln(:ifln)//Cislo(:idel(Cislo))
          endif
          SourceFileRefBlock(KRefBlock)=Veta
        endif
        go to 1030
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        Veta='Browse for the data collection file'
        SourceFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileName)
        p80=' '
        k=0
1070    call Kus(mene(OrderSel),k,Cislo)
        p80=p80(:idel(p80))//' *'//Cislo(:idel(Cislo))
        if(k.lt.len(mene(OrderSel))) go to 1070
        call FeFileManager(Veta,SourceFileRefBlock(KRefBlock),p80,0,
     1                     .true.,ich)
        go to 1020
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPowderDetails) then
        i=Order(OrderSel)-100
        call FeFillTextInfo('pwddataformats.txt',i)
        if(NInfo.le.0) then
          call FeChybne(-1.,YBottomMessage,
     1                  'information not yet distributed.',' ',
     1                  WarningWithESC)
        else
          call FeInfoOut(-1.,-1.,' ','L')
        endif
        go to 1050
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1030
      endif
      if(ich.eq.0) then
        if(nEdwFormat.ne.0) then
          if(DifCode(KRefBlock).ne.IdImportGraindex.and.
     1       DifCode(KRefBlock).ne.IdImportCIF)
     2      FormatRefBlock(KRefBlock)=EdwStringQuest(nEdwFormat)
        endif
        if(nCrwMethod.ne.0) then
          nCrwP=nCrwMethod
          do i=1,4
            if(CrwLogicQuest(nCrwP)) then
              PwdMethodRefBlock(KRefBlock)=mod(i,4)
              exit
            endif
            nCrwP=nCrwP+1
          enddo
        endif
      endif
      if(ich.ne.0) then
        call EM9ImportRestorePar
        if(ich.gt.0) then
          ErrFlag=-1
        else
          ErrFlag= 1
        endif
        go to 9999
      endif
1080  call CheckEOLOnFile(SourceFileRefBlock(KRefBlock),2)
      if(ErrFlag.ne.0) go to 9000
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted','old')
      if(ErrFlag.ne.0) go to 9000
      RealIndices=.false.
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        if(DifCode(KRefBlock).ne.IdImportCIF.and.
     1     DifCode(KRefBlock).ne.IdPets.and.
!     2     DifCode(KRefBlock).ne.IdImportSHELXF.and.
!     3     DifCode(KRefBlock).ne.IdImportSHELXI.and.
     4     DifCode(KRefBlock).ne.IdImportHKLF5.and.
     5     DifCode(KRefBlock).ne.IdImportGraindex.and.
     6     DifCode(KRefBlock).ne.IdImportXD.and.
     7     DifCode(KRefBlock).ne.IdSENJU.and.
     8     DifCode(KRefBlock).ne.Id6T2LBB) then
          call DRTestNDim(NDim95(KRefBlock))
          if(NDim95(KRefBlock).lt.3) then
            NDim95(KRefBlock)=3
            RealIndices=.true.
          endif
        endif
      endif
      NBankActual=0
      DelejBasic=.not.CyclicRefMode
      FormulaRefBlock=' '
      if(.not.ExistM50) then
        NTwin=1
        NComp(KPhaseDR)=1
        call UnitMat(zv (1,1,KPhaseDR),NDim(KPhaseDR))
        call UnitMat(zvi(1,1,KPhaseDR),NDim(KPhaseDR))
        KPhaseTwin(1)=KPhaseDR
      endif
1090  if(WhatToDo.eq.DatRedData) then
        PocitatUhly=DifCode(KRefBlock).ne.IdSiemensP4.and.
     1              DifCode(KRefBlock).ne.IdCAD4.and.
     2              DifCode(KRefBlock).ne.IdD9ILL.and.
     3              DifCode(KRefBlock).ne.IdHasylabF1.and.
     4              DifCode(KRefBlock).ne.IdHasylabHuber.and.
     5              DifCode(KRefBlock).ne.IdKumaPD.and.
     6              DifCode(KRefBlock).ne.IdSXD.and.
     7              DifCode(KRefBlock).ne.IdTopaz.and.
     8              DifCode(KRefBlock).ne.IdVivaldi.and.
     9              DifCode(KRefBlock).ne.IdSENJU.and.
     a              DifCode(KRefBlock).ne.IdSCDLANL.and.
     1              DifCode(KRefBlock).ne.IdKoala
        Profil=.false.
        ThOmRatio=2.
        call SetRealArrayTo(SenseOfAngle,4,-1.)
        SenseOfAngle(2)=1.
        NDimP=NDim95(KRefBlock)
        if(DifCode(KRefBlock).eq.IdCAD4) then
1100      read(71,FormA) Veta
1110      if(idel(Veta).lt.4) go to 1100
          if(ichar(Veta(1:1)).lt.32) then
            Veta=Veta(2:)
            go to 1110
          endif
          if(Veta(1:4).eq.'    ') go to 1100
          jentri=Veta(4:4).eq.' '
          rewind 71
          call DRBasicCad
        else if(DifCode(KRefBlock).eq.IdSiemensP4) then
          call DRBasicP4
        else if(DifCode(KRefBlock).eq.IdIPDSStoe) then
          call DRBasicIpStoe
        else if(DifCode(KRefBlock).eq.IdD9ILL) then
          WizardMode=.false.
          call DRBasicMonstrum
          WizardMode=.true.
          NDimP=NDim95(KRefBlock)
        else if(DifCode(KRefBlock).eq.IdHasyLabF1) then
          call DRBasicMonstrum
        else if(DifCode(KRefBlock).eq.IdHasyLabHuber) then
          call DRBasicMonstrum
        else if(DifCode(KRefBlock).eq.IdKumaCCD) then
          call DRBasicKumaCCD
        else if(DifCode(KRefBlock).eq.IdKumaPD) then
          call DRBasicKumaPD
        else if(DifCode(KRefBlock).eq.IdNoniusCCD) then
          call DRBasicNoniusCCD
        else if(DifCode(KRefBlock).eq.IdBrukerCCD.or.
     1          DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
          call DRBasicBrukerCCD
          if(ExistM50.and.StatusM50.lt.10000)
     1      call iom50(0,0,fln(:ifln)//'.m50')
        else if(DifCode(KRefBlock).eq.IdSXD) then
          call DRBasicSXD
        else if(DifCode(KRefBlock).eq.IdTopaz) then
          call DRBasicTopaz
        else if(DifCode(KRefBlock).eq.IdKoala) then
          call DRBasicKoala
        else if(DifCode(KRefBlock).eq.IdSENJU) then
          call DRBasicSENJU
        else if(DifCode(KRefBlock).eq.IdVivaldi) then
          call DRBasicVivaldi
        else if(DifCode(KRefBlock).eq.IdSCDLANL) then
          call DRBasicSCDLANL
        else if(DifCode(KRefBlock).eq.IdXDS) then
          call DRBasicXDS
        else if(DifCode(KRefBlock).eq.IdRigakuCCD) then
          call DRBasicRigakuCCD
        else if(DifCode(KRefBlock).eq.IdPets) then
          call DRBasicPets
        else if(DifCode(KRefBlock).eq.Id6T2LBB) then
          call DRBasic6T2LBB
        else if(DifCode(KRefBlock).eq.IdSHELXINoAbsCorr) then
          call DRBasicSHELXINoAbsCorr
        else if(DifCode(KRefBlock).eq.IdPolNeutrons) then
          call DRPolNeutrons
        else
          FormatRefBlock(KRefBlock)='(.i4,2f8.4,i4,6f8.5)'
          write(FormatRefBlock(KRefBlock)(2:2),101) NDim95(KRefBlock)
        endif
        if(ReimportRefBlock) NDim95(KRefBlock)=NDimP
        if(ErrFlag.ne.0) then
          ErrFlag=0
          call CloseIfOpened(71)
          go to 1015
        endif
      else if(WhatToDo.eq.ImportSingle) then
        PocitatDirCos=.false.
        PocitatUhly=.true.
        if(DifCode(KRefBlock).eq.IdImportCIF) then
          call DRBasicRefCIF(71,0)
        else if(DifCode(KRefBlock).eq.IdImportXD) then
          call DRBasicRefXD
        else if(DifCode(KRefBlock).eq.IdImportGraindex) then
          call DRBasicRefGraindex
        else if(DifCode(KRefBlock).eq.IdImportFullProf) then
          call DRBasicFullProf
        else if(DifCode(KRefBlock).eq.IdImportDABEX) then
          call DRBasicDABEX
        else if(DifCode(KRefBlock).eq.IdImportJanaM90) then
          call DRBasicJanaM90
        else
          if(DifCode(KRefBlock).ne.IdImportGeneralF.and.
     1       DifCode(KRefBlock).ne.IdImportGeneralI) then
            FormatRefBlock(KRefBlock)='(.i4,2f8.2,i4,6f8.5)'
            write(FormatRefBlock(KRefBlock)(2:2),101) NDim95(KRefBlock)
            if(DifCode(KRefBlock).eq.IdImportHKLF5.and.
     1         (.not.ReimportRefBlock.or.HKLF5RefBlock(KRefBlock).eq.0))
     2        HKLF5RefBlock(KRefBlock)=1
          endif
          call Zhusti(FormatRefBlock(KRefBlock))
          call mala(FormatRefBlock(KRefBlock))
        endif
        if(ErrFlag.ne.0) then
          ErrFlag=0
          call CloseIfOpened(71)
          go to 1015
        endif
        NDimP=NDim95(KRefBlock)
      else if(WhatToDo.eq.ImportCWPowder.or.
     1        WhatToDo.eq.ImportTOFPowder) then
        NImpPwd=1
        if(DifCode(KRefBlock).eq.IdDataMAC.or.
     1     DifCode(KRefBlock).eq.IdDataCPI) then
          NSkip=1
        else if(DifCode(KRefBlock).eq.IdDataPSI.or.
     1          DifCode(KRefBlock).eq.IdDataG41) then
          NSkip=0
          PwdInputInt=.true.
        else if(DifCode(KRefBlock).eq.IdDataRiet7) then
          NSkip=0
1410      read(71,FormA) Veta
          k=0
          call StToReal(Veta,k,xp,3,.false.,ich)
          if(ich.ne.0) then
            NSkip=NSkip+1
            if(NSkip.lt.6) then
              go to 1410
            else
              NSkip=4
            endif
          endif
          rewind 71
        else
          NSkip=0
        endif
        do i=1,NSkip
          read(71,FormA,end=1510) Veta
        enddo
        go to 1520
1510    call FeChybne(-1.,-1.,'the file does not contain relevant '//
     1                'information',' ',SeriousError)
        go to 1010
1520    NBankActual=NBankActual+1
        if(NBankActual.eq.1) then
          call PwdBasic(ich)
          IncSpectFileOpened=.false.
          if(ich.ne.0) then
            ErrFlag=0
            go to 1015
          endif
          if(DifCode(KRefBlock).eq.IdTOFGSAS.and.
     1       IncSpectFileRefBlock(KRefBlock).ne.' ') then
            call OpenFile(72,IncSpectFileRefBlock(KRefBlock),
     1                    'formatted','old')
            IncSpectFileOpened=.true.
          endif
        endif
        call PwdSetBank(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 5000
        endif
        NDimP=NDim95(KRefBlock)
      endif
      RadiationDetails=DifCode(KRefBlock).ne.IdD9ILL.and.
     1                 DifCode(KRefBlock).ne.IdSXD.and.
     2                 DifCode(KRefBlock).ne.IdTopaz.and.
     3                 DifCode(KRefBlock).ne.IdSENJU.and.
     4                 DifCode(KRefBlock).ne.IdHasyLabF1.and.
     5                 DifCode(KRefBlock).ne.IdHasyLabHuber.and.
     6                 DifCode(KRefBlock).ne.IdDataD1AD2B.and.
     7                 DifCode(KRefBlock).ne.IdDataD1AD2BOld.and.
     8                 DifCode(KRefBlock).ne.IdDataSaclay.and.
     9                 DifCode(KRefBlock).ne.IdDataPSI.and.
     a                 DifCode(KRefBlock).ne.IdImportFullProf.and.
     1                 DifCode(KRefBlock).ne.IdVivaldi.and.
     2                 DifCode(KRefBlock).ne.IdSCDLANL.and.
     3                 DifCode(KRefBlock).ne.IdData11BM.and.
     4                 DifCode(KRefBlock).ne.IdKoala
      RadiationDetails=RadiationDetails.and.
     1  (RadiationRefBlock(KRefBlock).eq.XRayRadiation.or.
     2   DifCode(KRefBlock).gt.200)
      if(CreateNewRefBlock) then
        if(RadiationDetails.and.DifCode(KRefBlock).lt.200) then
          AngleMonRefBlock(KRefBlock)=
     1      CrlMonAngle(MonCell(1,3),MonH(1,3),
     2                  LamAveRefBlock(KRefBlock))
        else
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        endif
      endif
      if(.not.ReimportRefBlock.and.KRefBlock.eq.1) NDimP=0
      StavVolani=0
1600  if(DelejBasic) then
        NDimPom=NDim95(KRefBlock)
        call EM9CompleteBasic(StavVolani,ich)
        if(ich.ne.0) then
          if(ich.gt.0) then
            call CloseIfOpened(70)
            call CloseIfOpened(71)
            Navrat=0
            go to 1010
          else
            ErrFlag=-1
            go to 9000
          endif
        endif
        if(NDimPom.ne.NDim95(KRefBlock)) then
          write(ChNDim,'(i1)') NDim95(KRefBlock)
          if(DifCode(KRefBlock).ne.IdImportGeneralF.and.
     1       DifCode(KRefBlock).ne.IdImportGeneralI) then
            FormatRefBlock(KRefBlock)='('//ChNDim//'i4,'
            i=idel(FormatRefBlock(KRefBlock))
            FormatRefBlock(KRefBlock)=FormatRefBlock(KRefBlock)(:i)//
     1                                '2f8.2,i4,6f8.5)'
          endif
          call Zhusti(FormatRefBlock(KRefBlock))
          call mala(FormatRefBlock(KRefBlock))
        endif
      endif
      if(CellRefBlock(1,0).lt.0.) then
        call CopyVek(CellRefBlock(1,KRefBlock),CellRefBlock(1,0),6)
        call CopyVek(CellRefBlockSU(1,KRefBlock),CellRefBlockSU(1,0),6)
        call CopyVek(CellRefBlock(1,0),CellPar(1,1,KPhaseDR),6)
        call CopyVek(CellRefBlockSU(1,0),CellParSU(1,1,KPhaseDR),6)
      else if(isPowder.and.KRefBlock.le.1) then
        call CopyVek(CellRefBlock(1,0),CellPar(1,1,KPhaseDR),6)
      endif
      do i=1,3
        if(QuRefBlock(1,i,0).lt.-300.) then
          call CopyVek(QuRefBlock(1,i,KRefBlock),QuRefBlock(1,i,0),3)
          call CopyVek(QuRefBlock(1,i,0),Qu(1,i,1,KPhaseDR),3)
        else if(isPowder.and.KRefBlock.le.1) then
          call CopyVek(QuRefBlock(1,i,0),Qu(1,i,1,KPhaseDR),3)
        endif
      enddo
      if(maxNDim.ne.NDimP.and..not.ReimportRefBlock) then
        call UnitMat(TrMP ,maxNDim)
        call UnitMat(TrMPI,maxNDim)
        if(.not.ExistM50) then
          call UnitMat(zv (1,1,KPhaseDR),maxNDim)
          call UnitMat(zvi(1,1,KPhaseDR),maxNDim)
        endif
        NDimP=maxNDim
      endif
1700  ObnovCell=.false.
      maxNDimOld=maxNDim
      if(ExistM50.and..not.ParentStructure) then
        CellParOld(1:6)=CellPar(1:6,1,KPhaseDR)
        CellParSUOld(1:6)=CellParSU(1:6,1,KPhaseDR)
        CellParSU(1:6,1,KPhaseDR)=CellRefBlockSU(1:6,0)
        if(maxNDim.gt.3) then
          call CopyVek(Qu(1,1,1,KPhaseDR),QuOld,3*NDimI(KPhaseDR))
          call CopyVek(QuRefBlock(1,1,0),Qu(1,1,1,KPhaseDR),
     1                 3*NDimI(KPhaseDR))
        endif
        ObnovCell=.true.
      endif
      if(.not.isPowder) then
        if(SilentRun) then
          if(HKLReimport) then
            ObnovCell=.true.
            call CopyVek(CellRefBlock(1,KRefBlock),CellRefBlock(1,0),6)
            call CopyVek(CellRefBlockSU(1,KRefBlock),
     1                   CellRefBlockSU(1,0),6)
            do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
              call CopyVek(QuRefBlock(1,i,KRefBlock),QuRefBlock(1,i,0),
     1                     3)
            enddo
          endif
        else
          call EM9CompleteTrans(StavVolani,ich)
          if(ich.ne.0) then
            if(ich.gt.0) then
              go to 1600
            else
              ErrFlag=-1
              go to 9000
            endif
          endif
        endif
      endif
      if(maxNDim.ne.NDimP.and..not.ReimportRefBlock) then
        call UnitMat(TrMP ,maxNDim)
        call UnitMat(TrMPI,maxNDim)
        if(.not.ExistM50) then
          call UnitMat(zv (1,1,KPhaseDR),maxNDim)
          call UnitMat(zvi(1,1,KPhaseDR),maxNDim)
        endif
        NDimP=maxNDim
      endif
      ModifiedRefBlock(KRefBlock)=.true.
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      nread=0
      if(ReimportRefBlock) NRef95Old=NRef95(KRefBlock)
      NRef95(KRefBlock)=0
      NLines95(KRefBlock)=0
      Veta='00000000'
      UseTabs=NextTabs()
      call FeTabsAdd(FeTxLength(Veta),UseTabs,IdLeftTab,' ')
      call FeTabsAdd(FeTxLengthSpace(Veta(:8)//' '),UseTabs,IdRightTab,
     1               ' ')
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        CellPar(1:6,1,KPhaseDR)=CellRefBlock(1:6,0)
        call CopyVek(QuRefBlock(1,1,0),Qu(1,1,1,KPhaseDR),
     1               3*NDimI(KPhaseDR))
        call DRSetCell(0)
        CellVol(1,KPhaseDR)=TrToOrtho(1,1,KPhaseDR)*
     1                      TrToOrtho(5,1,KPhaseDR)*
     1                      TrToOrtho(9,1,KPhaseDR)
        write(Format95(5:5),'(i1)') maxNDim
        if(ub(1,1,KRefBlock).gt.-300.)
     1    call matinv(ub(1,1,KRefBlock),ubi(1,1,KRefBlock),pom,3)
        if(.not.CyclicRefMode) then
          Veta=Tabulator//'       0'//Tabulator//
     1         'reflections already read'
          call FeTxOut(-1.,-1.,Veta)
        endif
        expold=0.
        addtime=0.
        expos0=-1.
        call SetRealArrayTo(uhly,4,0.)
        call SetRealArrayTo(dircos,6,0.)
        call SetRealArrayTo(corrf,2,1.)
        call SetIntArrayTo(iflg,2,1)
        iflg(3)=0
        tbar=0.
        noa=0
        IntFromProfile=DifCode(KRefBlock).eq.IdSiemensP4.or.
     1                 DifCode(KRefBlock).eq.IdKumaPD
        KProf=0
        NProf=0
        NExt(1)=0
        NExtObs(1)=0
        NExt500(1)=0
        RIExtMax(1)=0.
        SumRelI(1)=0.
        ReadLam=0.
        RunCCDMax(KRefBlock)=0
        ScMaxRead(KRefBlock)=0
        BratJineItw=-1
        FreeFormat=index(FormatRefBlock(KRefBlock),'*').gt.0
2000    nread=nread+1
2010    call DRReadRef(ich)
        if(KProf.eq.1) call CopyVekI(IProf(1,1),IProf(1,2),NProf)
        Konec=0
        if(ich.eq.1) then
          Konec=1
          go to 2020
        else if(ich.eq.2) then
          go to 4000
        else if(ich.eq.3) then
          call FeTabsReset(UseTabs)
          go to 5000
        else if(ich.eq.4) then
          call FeTabsReset(UseTabs)
          go to 5000
        else if(ich.eq.5) then
          go to 2000
        endif
        if(maxNDim.gt.3) then
          if(ImportOnlySatellites(KRefBlock).and.
     1       EqIV0(ih(4),maxNDim-3)) go to 2000
        endif
        if(HKLF5RefBlock(KRefBlock).ne.1) then
          if(iflg(2).lt.ITwRead(KRefBlock)) then
            if(BratJineItw.lt.0) then
              call FeFillTextInfo('em9importdatreddata1.txt',0)
              if(FeYesNoHeader(-1.,-1.,'Do you want to suppress '//
     1                         'possibly duplicate reflections?',1))
     2          then
                BratJineItw=0
              else
                BratJineItw=1
              endif
            endif
            if(BratJineItw.eq.0) go to 2000
          endif
        endif
        if(rs.le.0.) go to 2010
        nref95(KRefBlock)=nref95(KRefBlock)+1
        if(WhatToDo.eq.ImportSingle.or.DifCode(KRefBlock).eq.IdPets)
     1    no=nref95(KRefBlock)
        ScMaxRead(KRefBlock)=max(ScMaxRead(KRefBlock),iflg(1))
        ri=ri*ScaleRefBlock(KRefBlock)
        rs=rs*ScaleRefBlock(KRefBlock)
        call DRPutReflectionToM95(95,nl)
        NLines95(KRefBlock)=NLines95(KRefBlock)+nl
2020    if(mod(nread,50).eq.0.and..not.CyclicRefMode) then
          write(Veta(2:9),100) nread
          call FeTxOutCont(Veta)
        endif
        if(Konec.eq.0) then
          go to 2000
        endif
      else
        if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        nalloc=10000
        allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
        if(IncSpectFileOpened) then
          NPnts=0
          NInel=0
          ipointA=0
          iorderA=0
          ipointI=0
          iorderI=0
          p80=TypeBank
          Veta=FormatBank
          TypeBank=TypeBankInc
          FormatBank=FormatBankInc
3050      nread=nread+1
          call PwdImport(72,Konec,nalloc,ich)
          if(ich.ne.0) then
            ErrFlag=1
            call FeTabsReset(UseTabs)
            go to 5000
          endif
          if(Konec.eq.0) go to 3050
          TypeBank=p80
          FormatBank=Veta
          nread=0
        endif
        if(.not.CyclicRefMode) then
          Veta=Tabulator//'       0'//Tabulator//'records already read'
          call FeTxOut(-1.,-1.,Veta)
        endif
        NPnts=0
        NInel=0
        ipointA=0
        iorderA=0
        ipointI=0
        iorderI=0
3100    nread=nread+1
        call PwdImport(71,Konec,nalloc,ich)
        if(ich.ne.0) then
          ErrFlag=1
          call FeTabsReset(UseTabs)
          go to 5000
        endif
        if(mod(nread,50).eq.0.and..not.CyclicRefMode) then
          write(Veta(2:9),100) nread
          call FeTxOutCont(Veta)
        endif
        if(Konec.eq.0) go to 3100
        if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1     DifCode(KRefBlock).eq.IdTOFISIST.or.
     2     (DifCode(KrefBlock).eq.IdTOFGSAS.and.LSLOG).or.
     3     (DifCode(KrefBlock).eq.IdTOFGSAS.and.LRALF)) then
          if(DifCode(KRefBlock).ne.IdTOFISISD) then
            do i=1,NPnts
              if(i.lt.NPnts) then
                pom=1./(XPwd(i+1)-XPwd(i))
              else
                pom=1./(XPwd(i)-XPwd(i-1))
              endif
              YoPwd(i)=YoPwd(i)*pom
              if(IncSpectFileOpened) YiPwd(i)=YiPwd(i)*pom
              YsPwd(i)=YsPwd(i)*pom
            enddo
          endif
          pom=0.
          pomi=0.
          do i=1,NPnts
            pom=max(pom,YoPwd(i))
            if(IncSpectFileOpened) pomi=max(pomi,YiPwd(i))
          enddo
          if(pom.lt.100..or.pom.gt.10000.) then
            pom=1000./pom
            do i=1,NPnts
              YoPwd(i)=YoPwd(i)*pom
              YsPwd(i)=YsPwd(i)*pom
            enddo
          endif
          if(IncSpectFileOpened) then
            do i=1,NPnts
              YiPwd(i)=YiPwd(i)/pomi
            enddo
          endif
        endif
        call PwdMakeFormat92
        IncSpectNorm=.false.
        call PwdPutRecordToM95(95)
        if(NBankActual.lt.NBanks) then
          nread=nread-1
          if(.not.CyclicRefMode) then
            write(Veta(2:9),100) nread
            call FeTxOutEnd
            call FeReleaseOutput
            call FeDeferOutput
          endif
          KRefBlock=KRefBlock+1
          NRefBlock=max(NRefBlock,KRefBlock)
          call SetBasicM95(KRefBlock)
          write(Cislo,'(''?bank#'',i5)') NBankActual+1
          call Zhusti(Cislo)
          SourceFileRefBlock(KRefBlock)=Cislo
          if(IncSpectFileOpened) IncSpectFileRefBlock(KRefBlock)=Cislo
          SourceFileDateRefBlock(KRefBlock)=
     1      SourceFileDateRefBlock(KRefBlock-1)
          SourceFileTimeRefBlock(KRefBlock)=
     1      SourceFileTimeRefBlock(KRefBlock-1)
          DifCode(KRefBlock)=DifCode(KRefBlock-1)
          PolarizationRefBlock(KRefBlock)=
     1      PolarizationRefBlock(KRefBlock-1)
          LamAveRefBlock(KRefBlock)=LamAveRefBlock(KRefBlock-1)
          LamA1RefBlock(KRefBlock)=LamA1RefBlock(KRefBlock-1)
          LamA2RefBlock(KRefBlock)=LamA2RefBlock(KRefBlock-1)
          LamRatRefBlock(KRefBlock)=LamRatRefBlock(KRefBlock-1)
          KLamRefBlock(KRefBlock)=KLamRefBlock(KRefBlock-1)
          NAlfaRefBlock(KRefBlock)=NAlfaRefBlock(KRefBlock-1)
          write(Cislo,'(''.l'',i2)') KRefBlock
          if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
          RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
          call DeleteFile(RefBlockFileName)
          DelejBasic=DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1               DifCode(KRefBlock).eq.IdTOFISIST
          go to 1090
        endif
      endif
4000  nread=nread-1
      if(.not.CyclicRefMode) then
        write(Veta(2:9),100) nread
        call FeTxOutCont(Veta)
        call FeTxOutEnd
      endif
      call FeTabsReset(UseTabs)
      if(WhatToDo.eq.DatRedData.or.WhatToDo.eq.ImportSingle) then
        if(NExt500(1).eq.0) then
          NInfo=1
          write(Cislo,100) nread
          call Zhusti(Cislo)
          TextInfo(1)='All '//Cislo(:idel(Cislo))//
     1                ' input reflections were properly handled'
          Veta='INFORMATION'
        else
          if(NExt(1).ne.0) then
            SumRelI(1)=SumRelI(1)/float(NExt(1))
          else
            SumRelI(1)=0.
          endif
          write(TextInfo(1),'(''n(all) :'',i8,'' n(obs) :'',i8)')
     1      NExt(1),NExtObs(1)
          write(TextInfo(2),'(''Average(I/Sig(I)) : '',f5.2)')
     1          SumRelI(1)
          if(NExt500(1).ne.500) call Indexx(NExt500(1),ria(1,1),
     1                                      OrderExtRef(1,1))
          TextInfo(3)='List of the strongest ones:'
          NInfo=4
          if(UseTrRefBlock(KRefBlock)) then
            TextInfo(NInfo)='Indices are related to the original cell'//
     1                      ' !!!'
            NInfo=NInfo+1
          endif
          if(RealIndices) then
            write(Veta,'(3(7x,a1))')(indices(i),i=1,3)
            xpom=FeTxLength('0000.000')
            n=6
          else
            write(Veta,EM9Form1)(indices(i),i=1,NDim95(KRefBlock))
            xpom=FeTxLength('0000')
            n=NDim95(KRefBlock)+3
          endif
          Veta(idel(Veta)+1:)='       I      sig(I)  I/sig(I)'
          k=0
          p80=' '
          do i=1,n
            call kus(Veta,k,Cislo)
            if(i.eq.1) then
              p80=Tabulator//Cislo
            else
              p80=p80(:idel(p80))//Tabulator//Cislo
            endif
          enddo
          TextInfo(NInfo)=p80
          UseTabs=NextTabs()
          xx=0.
          do i=1,NDim95(KRefBlock)
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          xpom=FeTxLength('0000000.0')
          do i=1,3
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          do i=1,min(NExt500(1),15)
            k=OrderExtRef(i,1)
            if(RealIndices) then
              write(Veta,'(3f8.3)')(HExt(j,k,1),j=1,NDim95(KRefBlock))
            else
              write(Veta,EM9Form2)(IHExt(j,k,1),j=1,NDim95(KRefBlock))
            endif
            pom1=-float(ria(k,1))*.0001
            pom2= float(rsa(k,1))*.01
            write(Veta(idel(Veta)+1:),'(1x,3f9.1)')
     1        pom1*pom2,pom2,pom1
            k=0
            p80=' '
            do j=1,n
              call kus(Veta,k,Cislo)
              if(j.eq.1) then
                p80=Tabulator//Cislo
              else
                p80=p80(:idel(p80))//Tabulator//Cislo
              endif
            enddo
            NInfo=NInfo+1
            TextInfo(NInfo)=p80
          enddo
          Veta='Summary of reflections which couldn''t be imported'
        endif
        call FeInfoOut(-1.,-1.,Veta,'L')
        call FeTabsReset(UseTabs)
      endif
      if(WhatToDo.eq.DatRedData) then
        if(DifCode(KRefBlock).eq.IdSXD.or.
     1     DifCode(KRefBlock).eq.IdTopaz.or.
     2     DifCode(KRefBlock).eq.IdSENJU.or.
     3     DifCode(KRefBlock).eq.IdSCDLANL.or.
     4     DifCode(KRefBlock).eq.IdVivaldi.or.
     5     DifCode(KRefBlock).eq.IdKoala) then
          CorrAbs(KRefBlock)=-1
        else
          CorrAbs(KRefBlock)=0
        endif
        if(DifCode(KRefBlock).eq.IdCAD4.or.
     1     DifCode(KRefBlock).eq.IdSiemensP4.or.
     2     DifCode(KRefBlock).eq.IdKumaPD) then
          CorrLp(KRefBlock)= 0
        else
          CorrLp(KRefBlock)=-1
        endif
      else
        CorrAbs(KRefBlock)=-1
        CorrLp(KRefBlock) =-1
      endif
      if(ExistM50) then
        if((ReimportRefBlock.or.ObnovCell.or.HKLReimport).and.
     1      KRefBlock.le.1) then
          call MatBlock3(TrMP,TrMP3,maxNDim)
          call UnitMat(TrMP6,maxNDim)
          call DRMatTrCellQ(TrMP3,CellPar(1,1,KPhaseDR),
     1                      qui(1,1,KPhaseDR),quir(1,1,KPhaseDR),TrMP6)
          call AddVek(Qui(1,1,KPhaseDR),Quir(1,1,KPhaseDR),
     1                Qu(1,1,1,KPhaseDR),3*NDimI(KPhaseDR))
          call SetMet(0)
          maxNDimI=max(maxNDimI,NDimI(KPhaseDR))
          if(ObnovCell.or.maxNDimOld.ne.maxNDim) then
            if(NDim(KPhaseDR).gt.3) then
              call SetIntArrayTo(kw(1,1,KPhaseDR),3*mxw,0)
              if(NDim(KPhaseDR).eq.4) then
                j=mxw
              else
                j=NDimI(KPhaseDR)
              endif
              do i=1,j
                kw(mod(i-1,NDimI(KPhaseDR))+1,i,KPhaseDR)=
     1                                           (i-1)/NDimI(KPhaseDR)+1
              enddo
            endif
            call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                       NLattVec(KPhase),NComp(KPhase),NPhase,1)
            call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
            call FindSmbSg(Veta,ChangeOrderYes,1)
            if(ExistM40) then
              call iom40(0,0,fln(:ifln)//'.m40')
              call iom40(1,0,fln(:ifln)//'.m40')
            endif
          endif
          if(ParentStructure) call SSG2QMag(fln)
          call EM50CellSymmTest(1,ich)
          if(ParentStructure) call QMag2SSG(fln,0)
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
      else
        if(StatusM50.ge.10000) then
          if(NDim(KPhaseDR).ne.4) then
            Grupa(KPhaseDR)='P1'
          else
            Grupa(KPhaseDR)='P1(abg)'
          endif
          call AllocateSymm(1,1,1,1)
          call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QSymm,ich)
          Radiation(1)=RadiationRefBlock(1)
          LamAve(1)=LamAveRefBlock(1)
          call iom50(1,0,fln(:ifln)//'.m50')
        else if(Formula(KPhaseDR).eq.' ') then
          Formula(KPhaseDR)=FormulaRefBlock
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(DifCode(KRefBlock).eq.IdPets) then
        call iom42(1,0,fln(:ifln)//'.m42')
        ExistM42=.true.
      endif
      ModifiedRefBlock(KRefBlock)=.true.
      go to 9999
5000  if(.not.CyclicRefMode) call FeTxOutEnd
      if(ich.eq.3) then
        call FeReadError(71)
        if(ReimportRefBlock) NRef95(KRefBlock)=NRef95Old
        ErrFlag=1
      else if(ich.eq.4) then
        if(ReimportRefBlock) NRef95(KRefBlock)=NRef95Old
        ErrFlag=1
      endif
      go to 9999
9000  call EM9ImportRestorePar
9999  if(DifCode(KRefBlock).eq.IdDataXRDML.or.
     1   DifCode(KRefBlock).eq.IdDataPwdRigaku) then
        close(71,status='delete')
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        i=FeCloseBinaryFile(LnStoe)
      else
        call CloseIfOpened(71)
        call CloseIfOpened(72)
      endif
      call CloseIfOpened(95)
      if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(TMapTOF)) deallocate(TMapTOF,ClckWdtTOF,
     1                                  NPointsTimeMaps)
      return
100   format(i8)
101   format(i1)
      end
