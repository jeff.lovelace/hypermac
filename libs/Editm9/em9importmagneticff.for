      subroutine EM9ImportMagneticFF(ich)
      use Atoms_mod
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80  Veta,FormulaOld
      character*7   At,AtTypeMagP
      character*5  MenuJ(10,5)
      integer RolMenuSelectedQuest,nCrwJType(0:5),nRolMenuJType(5),
     1        NMenuJ(5),IMenuJ(5),EdwStateQuest
      logical CrwLogicQuest,EqRV0,MagneticAtom,EqIgCase,ExistM90In
      real xp(3),xpp(3)
1100  ExistXRayData=.false.
      ExistNeutronData=.true.
      ExistM90In=ExistM90
      ExistM90=.true.
      id=NextQuestId()
      Veta='Define magnetic propagation vector and form factors'
      call FeQuestTitleMake(id,Veta)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+5.
      tpom=5.
      Veta='Number of non-zero %magnetic propagation vectors:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwNQMag=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NQMag(KPhase),.false.)
      call FeQuestEudOpen(EdwLastMade,0,3,1,0.,0.,0.)
      il=il+1
      Veta='This defines the number of independent modulation vectors'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      ilp=-10*il-5
      Veta='which is smaller or equal to the number of active ones.'
      call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
      il=il-1
      Veta='Propagation vector#'
      tpom=xpom+dpom+50.
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      dpom=WizardLength-xpom-10.
      do i=1,3
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                      Veta(:idel(Veta))//Cislo(1:1)//':','L',dpom,
     2                      EdwYd,0)
        call FeQuestRealAEdwOpen(EdwLastMade,QMag(1,i,KPhase),3,.false.,
     1                           .false.)
        if(i.gt.NQMag(KPhase)) call FeQuestEdwDisable(EdwLastMade)
        if(i.eq.1) nEdwQMag=EdwLastMade
        il=il+1
      enddo
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Formula %units'
      xpome=tpom+FeTxLengthUnder(Veta)+10.
      dpom=300.
      call FeQuestEdwMake(id,tpom,il,xpome,il,'%Formula','L',dpom,EdwYd,
     1                    1)
      nEdwFormula=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
      il=il+1
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwZ=EdwLastMade
      if(NUnits(KPhase).le.0) then
        if(NSymmN(KPhase).gt.0.and.NLattVec(KPhase).gt.0) then
          NUnits(KPhase)=NLattVec(KPhase)*NSymmN(KPhase)
        else
          NUnits(KPhase)=1
        endif
      endif
      call FeQuestIntEdwOpen(nEdwZ,NUnits(KPhase),.false.)
      Veta='Calculate %density'
      xpom=xpome+dpom+36.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      if(Formula(KPhase).ne.' ') call FeQuestButtonOff(ButtonLastMade)
      xpom=xpom+dpom+5.
      il=il+1
      Veta='%Atom type'
      dpomr=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpome,il,Veta,'L',dpomr,EdwYd,
     1                        1)
      nRolMenuAtType=RolMenuLastMade
      xpom=xpome+dpomr+20.
      Veta='Own scattering length for %neutrons'
      dpom=FeTxLengthUnder(Veta)+10
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefineFF=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=xpom+dpom+20.
      Veta='Use as a ma%gnetic atom'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwMagnetic=CrwLastMade
      tpom=15.
      Veta='%Own formfactors'
      xpom=tpom
      tpom=xpom+CrwXd+10.
      xpomp=tpom+FeTxLengthUnder(Veta)+100.
      dpom=80.
      MenuJ(1,1)=' '
      NMenuJ(1)=1
      do i=0,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                      1)
        if(i.eq.0) then
          Veta='%Edit'
          call FeQuestButtonMake(id,xpomp,il,dpom,ButYd,Veta)
          nButtOwn=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonDisable(ButtonLastMade)
        else
          call FeQuestRolMenuMake(id,tpom,il,xpomp,il,'  ','L',dpom,
     1                            EdwYd,1)
          call FeQuestRolMenuOpen(RolMenuLastMade,MenuJ(1,1),NMenuJ(1),
     1                            1)
          call FeQuestRolMenuDisable(RolMenuLastMade)
          nRolMenuJType(i)=RolMenuLastMade
        endif
        if(i.lt.4) then
          write(Veta,'(''Magnetic formfactor <j%'',i1,''>'')') 2*i
        else if(i.eq.4) then
          Veta='Magnetic formfactor <j0>%+c<j2>'
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.0)
        call FeQuestCrwDisable(CrwLastMade)
        nCrwJType(i)=CrwLastMade
      enddo
      LastAtomOld=-1
      LastAtom=1
      if(NAtFormula(KPhase).gt.0) then
        MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
      else
        MagneticAtom=.false.
      endif
1400  if(LastAtom.ne.LastAtomOld.and.NAtFormula(KPhase).gt.0) then
        call FeQuestRolMenuOpen(nRolMenuAtType,AtType(1,KPhase),
     1                          NAtFormula(KPhase),LastAtom)
        call FeQuestCrwOpen(nCrwMagnetic,MagneticAtom)
      endif
      if(MagneticAtom) then
        if(LastAtom.ne.LastAtomOld) then
          call EM50ReadMagneticFFLabels(AtType(LastAtom,KPhase),MenuJ,
     1                                  NMenuJ,ierr)
          if(AtTypeMag(LastAtom,KPhase).eq.' ') then
          if(NMenuJ(1).gt.0) then
              AtTypeMag(LastAtom,KPhase)=MenuJ(NMenuJ(1),1)
              AtTypeMagJ(LastAtom,KPhase)='j0'
              Veta='Magnetic_formfactor_<j0>'
              call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1                               AtTypeMag(LastAtom,KPhase),Veta,
     2                               FFMag(1,LastAtom,KPhase),ich)
            else
              AtTypeMag(LastAtom,KPhase)=AtType(LastAtom,KPhase)
              AtTypeMagJ(LastAtom,KPhase)='own'
              TypeFFMag(LastAtom,KPhase)=0
              call SetRealArrayTo(FFMag(1,LastAtom,KPhase),7,0.)
            endif
          endif
          if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'own')) then
            JAtP=0
          else if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'j0j2')) then
            JAtP=5
          else
            JAtP=1
            read(AtTypeMagJ(LastAtom,KPhase)(2:2),'(i1)',err=1450) i
            JAtP=i/2+1
          endif
1450      call FeQuestCrwOpen(nCrwJType(0),JAtP.eq.0)
          if(JAtP.eq.0) then
            call FeQuestButtonOpen(nButtOwn,ButtonOff)
            nRolMenuLast=0
          else
            call FeQuestButtonDisable(nButtOwn)
          endif
          do i=1,5
            if(NMenuJ(i).gt.0) then
              call FeQuestCrwOpen(nCrwJType(i),JAtP.eq.i)
            else
              call FeQuestCrwDisable(nCrwJType(i))
            endif
          enddo
          AtTypeMagP=AtTypeMag(LastAtom,KPhase)
          do i=1,5
            if(NMenuJ(i).gt.0) then
              IMenuJ(i)=
     1          max(LocateInStringArray(MenuJ(1,i),NMenuJ(i),
     2                                  AtTypeMagP,IgnoreCaseYes),1)
              if(i.eq.JAtP) then
                call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                  NMenuJ(i),IMenuJ(i))
                nRolMenuLast=nRolMenuJType(i)
              else
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            else
              IMenuJ(i)=0
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            endif
          enddo
        endif
      else
        call FeQuestButtonDisable(nButtOwn)
        call FeQuestCrwDisable(nCrwJType(0))
        do i=1,5
          call FeQuestCrwDisable(nCrwJType(i))
          call FeQuestRolMenuDisable(nRolMenuJType(i))
        enddo
      endif
      LastAtomOld=LastAtom
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNQMag) then
        call FeQuestIntFromEdw(nEdwNQMag,NQMag(KPhase))
        nEdw=nEdwQMag
        do i=1,3
          if(i.le.NQMag(KPhase)) then
            if(EdwStateQuest(nEdw).ne.EdwOpened)
     1        call FeQuestRealAEdwOpen(nEdw,QMag(1,i,KPhase),3,.false.,
     2                                 .false.)
          else
            if(EdwStateQuest(nEdw).eq.EdwOpened)
     1        call FeQuestRealAFromEdw(nEdw,QMag(1,i,KPhase))
            call FeQuestEdwDisable(nEdw)
          endif
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFormula) then
        FormulaOld=Formula(KPhase)
        NAtFormulaOld=NAtFormula(KPhase)
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        Veta=Formula(KPhase)
        call EM50SaveFormFactors
        do i=1,idel(Veta)
          if(index(Cifry,Veta(i:i)).gt.0) Veta(i:i)=' '
        enddo
        do i=1,idel(FormulaOld)
          if(index(Cifry,FormulaOld(i:i)).gt.0) FormulaOld(i:i)=' '
        enddo
        call PitFor(1,ichp)
        if(ichp.ne.0) go to 1520
        if(NAtFormula(KPhase).gt.0) then
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          if(NAtFormulaOld.le.0) FFType(KPhase)=-62
        else
          go to 1500
        endif
        call PitFor(0,ichp)
        if(ichp.ne.0) go to 1520
        if(NAtFormulaOld.gt.0) then
          call EM50SmartUpdateFormFactors
        else
          do i=1,NAtFormula(KPhase)
            FFBasic(1,i,KPhase)=-3333.
            AtRadius(i,KPhase)=-3333.
          enddo
        endif
        call FeQuestButtonOff(nButtCalculateDensity)
        do i=1,NAtFormula(KPhase)
          if(AtRadius(i,KPhase).lt.-3000.)
     1      call EM50ReadOneFormFactor(i)
          if(FFBasic(1,i,KPhase).lt.-3000.)
     1      call EM50OneFormFactorSet(i)
        enddo
        LastAtomOld=-1
        go to 1400
1520    EventType=CheckType
        EventNumber=CheckNumber
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtCalculateDensity) then
        ExistM90=ExistM90In
        call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        if(Formula(KPhase).ne.' ') call PitFor(0,ichp)
        NInfo=0
        call EM50CalcDenAbs(ichp)
        if(ichp.ne.0)
     1    TextInfo(1)='Error during calculation of density and '//
     2                'absorption coefficient.'
        NInfo=NInfo-1
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        ExistM90=.true.
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwJType(0).and.
     2         CheckNumber.le.nCrwJType(5))) then
        if(CheckNumber.eq.nCrwJType(0)) then
          call FeQuestButtonOff(nButtOwn)
          do i=1,5
            call FeQuestRolMenuDisable(nRolMenuJType(i))
          enddo
          RolMenuLast=0
          AtTypeMag(LastAtom,KPhase)=AtTypeMag(LastAtom,KPhase)
          AtTypeMagJ(LastAtom,KPhase)='own'
          call EM50ReadOwnFormFactorMag(LastAtom)
        else
          call FeQuestButtonDisable(nButtOwn)
          if(nRolMenuLast.gt.0)
     1      IMenuJ(nRolMenuLast-nRolMenuJType(1)+1)=
     2        RolMenuSelectedQuest(nRolMenuLast)
          do i=1,5
            if(CheckNumber.eq.nCrwJType(i)) then
              call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                NMenuJ(i),IMenuJ(i))
              nRolMenuLast=nRolMenuJType(i)
              AtTypeMag(LastAtom,KPhase)=MenuJ(IMenuJ(i),i)
              if(i.eq.5) then
                AtTypeMagJ(LastAtom,KPhase)='j0j2'
                Veta='Magnetic_formfactor_<j0>+c<j2>'
              else
                write(Cislo,'(i1)') 2*(i-1)
                AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
                Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
              endif
            else
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            endif
          enddo
          call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1      AtTypeMag(LastAtom,KPhase),Veta,
     2      FFMag(1,LastAtom,KPhase),ich)
          Klic=0
          go to 1400
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtType) then
        LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
        if(LastAtomOld.ne.LastAtom) then
          MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
          Klic=0
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.ge.nRolMenuJType(1).and.
     2         CheckNumber.le.nRolMenuJType(5))) then
        nRolMenuLast=CheckNumber
        i=nRolMenuLast-nRolMenuJType(1)+1
        AtTypeMag(LastAtom,KPhase)=
     1    MenuJ(RolMenuSelectedQuest(nRolMenuLast),i)
        if(i.eq.5) then
          AtTypeMagJ(LastAtom,KPhase)='j0j2'
          Veta='Magnetic_formfactor_<j0>+c<j2>'
        else
          write(Cislo,'(i1)') 2*(i-1)
          AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
          Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
        endif
        call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1    AtTypeMag(LastAtom,KPhase),Veta,
     2    FFMag(1,LastAtom,KPhase),ich)
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMagnetic)
     1  then
        MagneticAtom=CrwLogicQuest(nCrwMagnetic)
        if(.not.MagneticAtom) then
          AtTypeMag(LastAtom,KPhase)=' '
          AtTypeMagJ(LastAtom,KPhase)=' '
        endif
        LastAtomOld=-1
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOwn) then
        call EM50ReadOwnFormFactorMag(LastAtom)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefineFF)
     1  then
        if(NAtFormula(KPhase).gt.0) then
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
        endif
        call EM50ReadOwnFormFactor(LastAtom)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNQMag,NQMag(KPhase))
        nEdw=nEdwQMag
        do i=1,NQMag(KPhase)
          call FeQuestRealAFromEdw(nEdw,QMag(1,i,KPhase))
          nEdw=nEdw+1
        enddo
      else
        go to 9999
      endif
      ExistM90=ExistM90In
      call FeFillTextInfo('em9importmagnetic2.txt',0)
      call FeWizardTextInfo('INFORMATION',1,-1,1,ich)
      if(ich.gt.0) go to 1100
9999  return
      end
