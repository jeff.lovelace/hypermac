      subroutine EM9CheckCompleteness(ThMax,LamUsed,mmax)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension sp(:),ih(6),hn(6),har(:,:),h(6),hp(6),NGen(8),NMer(8),
     1          PerArr(8),mmax(3,*),rtw6(36,MxPhases),rtw6i(36,MxPhases)
      character*40 Veta
      integer flag(:),KPhA(:)
      logical EqRV
      real LamUsed
      allocatable sp,flag,har,KPhA
      do KPhase=1,NPhase
        if(NDim(KPhase).eq.MaxNDim) go to 1050
      enddo
      KPhase=1
1050  call iom50(0,0,fln(:ifln)//'.m50')
      if(kcommenMax.ne.0) call comsym(0,1,ich)
      call OpenFile(91,fln(:ifln)//'.l93','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(LamUsed.gt.0.) then
        snthlmx=sin(ThMax*ToRad)/LamUsed
      else
        snthlmx=ThMax
        LamUsed=.5/snthlmx
        ThMax=asin(.5)/ToRad
      endif
      call GenerRef('Calculating coverage statistics',snthlmx,mmax,
     1              .true.,0)
      if(ErrFlag.ne.0) go to 9999
      if(KCommen(KPhase).eq.0) then
        NDimP=NDim(KPhase)
      else
        NDimP=3
      endif
      rewind 91
      nref=0
1100  read(91,Format91,end=1200)(ih(i),i=1,maxNDim),f,f,i,i,KPh,s
      if(ih(1).gt.900) go to 1200
      nref=nref+1
      go to 1100
1200  allocate(har(NDim(KPhase),nref),sp(nref),Flag(nref),KPhA(nref))
      rewind 91
      k=0
1300  read(91,Format91,end=1500)(ih(i),i=1,maxNDim),f,f,i,i,KPh,s
      if(ih(1).gt.900) go to 1500
      k=k+1
      KPhase=KPh
      call FromIndSinthl(ih,h,sp(k),sinthlq,1,0)
      call CopyVek(h,har(1,k),3)
      KPhA(k)=KPh
      go to 1300
1500  close(91,status='delete')
      do KPh=1,NPhase
        do i=1,NTwin
          if(KPhaseTwin(i).eq.KPh) then
            call CopyMat(rtw (1,i),rtw6 (1,KPh),3)
            call matinv(rtw6(1,KPh),rtw6i(1,KPh),pom,3)
c            call CopyMat(rtwi(1,i),rtw6i(1,KPh),3)
            exit
          endif
        enddo
      enddo
      do KPh=1,NPhase
        KPhase=KPh
        if(Grupa(KPh).ne.' '.and.index(Grupa(KPh),'?').le.0) then
          Veta='Coverage statistics for '//Grupa(KPh)(:idel(Grupa(KPh)))
        else
          Veta='Coverage statistics for symmetry defined above'
        endif
        if(NPhase.gt.1)
     1    Veta=Veta(:idel(Veta))//' - '//PhaseName(KPh)
        call newln(3)
        call TitulekVRamecku(Veta)
        Veta='sin(theta)/lambda '
        write(lst,'(a39,8f10.6)') Veta,sinmez
        call OpenFile(91,DatBlockFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do k=1,2
          call SetIntArrayTo(Flag,nref,0)
          rewind 91
2100      read(91,Format91,end=3000)(ih(i),i=1,maxNDim),f,sf,i,i,
     1                              itw
          if(ih(1).gt.900) go to 3000
          if(itw.lt.0) then
            itw=mod(-itw,100)
          else if(itw.gt.100) then
            itw=mod(itw,100)
          endif
          KPhP=KPhaseTwin(itw)
          KPhase=KPhP
          call FromIndSinthl(ih,h,s,sinthlq,1,0)
          if(KPhp.ne.KPh) then
            call MultM(h,Rtw6I(1,KPhP),hp,1,3,3)
            call MultM(hp,RTw6(1,KPh),h,1,3,3)
          endif
          if(k.eq.1.and.f.lt.EM9ObsLim(KDatBlock)*sf) go to 2100
          ip=1
          ik=NRef
2150      if(ik.le.ip+1) go to 2200
          ipul=(ik+ip)/2
          if(s.lt.sp(ipul)) then
            ik=ipul
            go to 2150
          else if(s.gt.sp(ipul)) then
            ip=ipul
            go to 2150
          else
            ip=ipul
            ik=ipul
            go to 2200
          endif
2200      ip=max(ip-10,1)
          ik=min(ik+10,NRef)
          do j=1,NSymmN(KPh)
            call MultM(h,rm(1,j,1,KPh),hp,1,3,3)
            call RealVectorToOpposite(hp,hn,3)
            do i=ip,ik
              if(KPhA(i).ne.KPh) cycle
              if(eqrv(hp,har(1,i),3,.001).or.eqrv(hn,har(1,i),3,.001))
     1          then
                flag(i)=1
                go to 2100
              endif
            enddo
          enddo
          go to 2100
3000      call SetIntArrayTo(NMer,8,0)
          call SetIntArrayTo(NGen,8,0)
          do i=1,NRef
            if(KPhA(i).ne.KPh) cycle
            do j=1,8
              if(sp(i).le.sinmez(j)) then
                if(Flag(i).gt.0) NMer(j)=NMer(j)+1
                NGen(j)=NGen(j)+1
              endif
            enddo
          enddo
          do i=1,8
            PerArr(i)=float(NMer(i))/float(NGen(i))*100.
          enddo
          if(k.eq.1) then
            Veta='Coverage in  % for observed reflections'
          else
            Veta='Coverage in  % for all reflections'
          endif
          write(lst,'(a39,8f10.2)') Veta,PerArr
        enddo
        close(91)
        PerLimitMax=min(PerArr(8),100.)
        ppp=min(sinmez(8)*LamUsed,.999999)
        ThMax=asin(ppp)/ToRad
        PerLimit=111.
        call newln(3)
        Veta='Coverage        Theta'
        write(lst,'(/5x,a/)') Veta(:idel(Veta))
        if(PerLimitMax.ge.98.) then
          PerLimit=PerLimitMax
          ThFull=ThMax
          write(lst,100) PerLimit,ThFull
          go to 9999
        endif
        do n=0,100
          PerLimitP=float(ifix(PerLimitMax))+float(n)
          if(PerLimitP.gt.100.1) cycle
          PerLimitP=min(PerLimitP,99.96)
          do i=8,1,-1
            if(PerArr(i).gt.PerLimitP) go to 3540
          enddo
          go to 3590
3540      if(i.eq.8) then
            ThFullP=ThMax
            PerLimitP=PerArr(8)
            go to 3580
          endif
          SThP=sinmez(i)
          SThK=sinmez(i+1)
3550      SThPul=(SThK+SThP)*.5
          if(SThK-SthP.le..00001) then
            pom=min(SThPul*LamUsed,.999999)
            ThFullP=asin(pom)/ToRad
            go to 3580
          endif
          NMerP=0
          NGenP=0
          do i=1,NRef
            if(KPhA(i).ne.KPh) cycle
            if(sp(i).le.SThPul) then
              if(Flag(i).gt.0) NMerP=NMerP+1
              NGenP=NGenP+1
            endif
          enddo
          pom=float(NMerP)/float(NGenP)*100.
          if(pom.gt.PerLimitP) then
            SThP=SThPul
          else if(pom.lt.PerLimitP) then
            SThK=SThPul
          else
            SThP=SThPul
            SThK=SThPul
          endif
          go to 3550
3580      call newln(1)
          write(lst,100) PerLimitP,ThFullP
          if(PerLimitP.le.98.) then
            PerLimit=PerLimitP
            ThFull=ThFullP
          endif
          cycle
3590      call newln(1)
          write(lst,'(5x,f6.2,''%       ---------'')') PerLimitP
          exit
        enddo
        call newln(3)
        write(lst,FormA)
        write(lst,'('' Fraction '',f6.2,''% for theta(max) '',f8.2)')
     1    PerLimitMax*.01,ThMax
        write(lst,'('' Fraction '',f6.2,''% for theta(full)'',f8.2)')
     1    PerLimit*.01,ThFull
      enddo
9999  if(kcommenMax.ne.0) call iom50(0,0,fln(:ifln)//'.m50')
      if(allocated(Flag)) deallocate(flag,har,sp,KPhA)
      return
100   format(5x,f6.2,'%       ',f5.2,' deg')
      end
