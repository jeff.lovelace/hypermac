      subroutine EM9ImportWizard(KlicIn)
      use Atoms_mod
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'powder.cmn'
      common/EM9CheckCommon/ nSbw,nButtInfo,nButtReimport,nButtModify,
     1                       nButtDelete,nButtUndelete
      character*128 Veta,VetaPom
      character*80  FormatOut,FormatIn,FlnOld
      character*20  RadString(MxRefBlock)
      dimension NRef95Old(MxRefBlock),xp(3)
      integer WhatToDo,SbwItemPointerQuest,FirstPowder,FirstSingle,
     1        CoDal,DifCodePrev,UseTabsIn
      integer ::  WhatToDoLast = 0
      logical CrwLogicQuest,ExistFile,Skrt,First,Change,FeYesNo,
     1        OnlyModify,EqIgCase,ImportMagnetic,StructureExists,
     2        ChangeSGTest,FeYesNoHeader,RefineEnd
      real TrMP3I(3,3),TrMP6I(36)
      external EM9CheckRefBlock,FeVoid
      equivalence (IdNumbers( 1),IdImportSingleDatRed),
     1            (IdNumbers( 2),IdImportSingle),
     2            (IdNumbers( 3),IdImportCWPowder),
     3            (IdNumbers( 4),IdImportTOFPowder),
     4            (IdNumbers( 5),IdFullProfImport),
     5            (IdNumbers( 6),IdShelxImport),
     6            (IdNumbers( 7),IdCifImport),
     7            (IdNumbers( 8),IdXDImport),
     8            (IdNumbers( 9),IdJana2000Import),
     9            (IdNumbers(10),IdPDBImport),
     a            (IdNumbers(11),IdMagneticManual),
     1            (IdNumbers(12),IdMagneticSHELX),
     2            (IdNumbers(13),IdMagneticCIF),
     3            (IdNumbers(14),IdMagneticJana)
      save /EM9CheckCommon/
      UseTabsIn=UseTabs
      KCyclicRef=0
      Klic=KlicIn
      HKLReimport=.false.
      call CopyFile(fln(:ifln)//'.l51',fln(:ifln)//'.m51')
      call DeletePomFiles
      call ChangeUSDFile(fln,'opened','*')
      call MoveFile(fln(:ifln)//'.m51',fln(:ifln)//'.l51')
      if(ExistM95) call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
      if(ExistM90) call CopyFile(fln(:ifln)//'.m90',fln(:ifln)//'.z90')
      if(ExistM50) call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      if(ExistM40) call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      if(ExistM42) call CopyFile(fln(:ifln)//'.m42',fln(:ifln)//'.z42')
      First=.true.
      Change=.false.
      ImportMagnetic=.false.
      NChange=0
      KnownM50=ExistM50
      KPhaseDR=1
      if(KnownM50) then
        do i=1,NPhase
          if(maxNDim.eq.NDim(i)) then
            KPhaseDR=i
            exit
          endif
        enddo
      endif
      call iom95(0,fln(:ifln)//'.m95')
      call SetLogicalArrayTo(ModifiedRefBlock,MxRefBlock,.false.)
      NRefBlockIn=NRefBlock
1050  call iom95(1,fln(:ifln)//'.l95')
      KRefBlock=0
      n=0
      do i=1,NRefBlock
        if(NRef95(i).gt.0) then
          if(KRefBlock.le.0) KRefBlock=i
          n=n+1
        endif
      enddo
      if(First) then
        norg=n
        First=.false.
      endif
      k=1
      if(norg.gt.0.or.n.gt.0) then
        if(SilentRun) then
          if(CyclicRefMode) then
            if(KCyclicRef.le.0) then
              ReimportRefBlock=.true.
              OnlyModify=.false.
              CreateNewRefBlock=.false.
              KCyclicRef=1
              go to 1190
            else
              go to 1170
            endif
          else
            go to 1170
          endif
        endif
1055    id=NextQuestId()
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_m95list.tmp','formatted',
     1                'unknown')
        xqd=600.
        dpom=xqd-20.-SbwPruhXd
        n=0
        do i=1,NRefBlock
          if(KRefBlock.le.0) KRefBlock=i
          n=n+1
          Veta=SourceFileRefBlock(i)
          j=idel(Veta)
          if(Veta(1:1).eq.'?') then
            if(EqIgCase(Veta(2:5),'bank')) then
              Veta='    -"-    '//Veta(2:idel(Veta))
            else
              Veta='not specified'
            endif
          else
            call FeCutNameLength(SourceFileRefBlock(i),Veta,
     1        dpom*.5-2.*EdwMarginSize,CutTextFromLeft)
          endif
          if(NRef95(i).gt.0) then
            Veta=Veta(:idel(Veta))//Tabulator//'|'//Tabulator
          else
            Veta=Veta(:idel(Veta))//Tabulator//' --- Deleted ---'
            go to 1065
          endif
          if(DifCode(i).gt.200) then
            VetaPom='Powder TOF/ED'
          else if(DifCode(i).gt.100) then
            VetaPom='Powder CW'
          else if(DifCode(i).gt.0) then
            VetaPom='Single crystal'
          else
            if(DifCode(i).eq.IdImportSHELXF.or.
     1         DifCode(i).eq.IdImportGeneralF.or.
     2         DifCode(i).eq.IdImportDABEX) then
              VetaPom='F(hkl) imported'
            else
              VetaPom='I(hkl) imported'
            endif
          endif
          Veta=Veta(:idel(Veta))//VetaPom(:idel(VetaPom))//Tabulator//
     1         '|'//Tabulator
          if(RadiationRefBlock(i).eq.XRayRadiation) then
            RadString(i)='X-rays'
          else if(RadiationRefBlock(i).eq.NeutronRadiation) then
            RadString(i)='Neutrons'
          else if(RadiationRefBlock(i).eq.ElectronRadiation) then
            RadString(i)='Electrons'
          else
            RadString(i)=' '
          endif
          k=KLamRefBlock(i)
          if(RadiationRefBlock(i).eq.XRayRadiation.and.k.gt.0) then
            Cislo=LamTypeD(k)(:idel(LamTypeD(k)))//' K(alpha)'
          else
            if(LamAveRefBlock(i).gt.0.) then
              write(Cislo,'(f10.6)') LamAveRefBlock(i)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='TOF'
            endif
          endif
          RadString(i)=RadString(i)(:idel(RadString(i))+1)//
     1                 Cislo(:idel(Cislo))
          Veta=Veta(:idel(Veta))//RadString(i)(:idel(RadString(i)))
1065      write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        ilk=min(ifix((float(n)*MenuLineWidth+2.)/QuestLineWidth)+3,15)
        ilk=max(ilk,6)
        Veta='Data repository'
        WizardMode=.false.
        il=1
        call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,0,0)
        call FeQuestLblMake(id,           30.,il,'File'     ,'L','N')
        call FeQuestLblMake(id,   dpom/2.+20.,il,'Type'     ,'L','N')
        call FeQuestLblMake(id,3.*dpom/4.+20.,il,'Radiation','L','N')
        UseTabs=NextTabs()
        pom=dpom/2.-EdwMarginSize
        call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
        call FeTabsAdd(pom+EdwMarginSize,UseTabs,IdRightTab,' ')
        pom=3.*dpom/4.-EdwMarginSize
        call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
        call FeTabsAdd(pom+EdwMarginSize,UseTabs,IdRightTab,' ')
        xpom=10.
        il=ilk-1
        call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                      SbwVertical)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_m95list.tmp')
        dpom=xqd-10.
        xpom=5.
        il=ilk
        dpom=70.
        spom=15.
        tpom=dpom+spom
        xpom=xqd*.5-tpom*3.+spom*.5
        do i=1,6
          if(i.eq.1) then
            Veta='%Info'
          else if(i.eq.2) then
            nButtInfo=ButtonLastMade
            Veta='%Reimport'
          else if(i.eq.3) then
            nButtReimport=ButtonLastMade
            Veta='%Modify'
          else if(i.eq.4) then
            nButtModify=ButtonLastMade
            Veta='%Delete'
          else if(i.eq.5) then
            nButtDelete=ButtonLastMade
            Veta='%Undelete'
          else
            nButtUndelete=ButtonLastMade
            Veta='Im%port new'
          endif
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(n.le.0.and.i.lt.4) then
            j=ButtonDisabled
          else
            j=ButtonOff
          endif
          call FeQuestButtonOpen(ButtonLastMade,j)
          xpom=xpom+tpom
        enddo
        nButtNew=ButtonLastMade
1150    call FeQuestEventWithCheck(id,ich,EM9CheckRefBlock,FeVoid)
        KRefBlock=SbwItemPointerQuest(nSbw)
        if(CheckType.eq.EventButton) then
          if(CheckNumber.eq.nButtInfo) then
            NInfo=1
            if(SourceFileRefBlock(KRefBlock)(1:1).eq.'?') then
              TextInfo(NInfo)=SourceFileRefBlock(KRefBlock)(2:)
            else
              TextInfo(NInfo)='Imported file: '//
     1                        SourceFileRefBlock(KRefBlock)
              NInfo=NInfo+1
              TextInfo(NInfo)='Date/time    : '//
     1          SourceFileDateRefBlock(KRefBlock)//' '//
     2          SourceFileTimeRefBlock(KRefBlock)
            endif
            NInfo=NInfo+1
            j=DifCode(KRefBlock)
            if(DifCode(KRefBlock).gt.200) then
              Veta='???'
              do i=1,nTOFPowderTypes
                if(OrderTOFPowderTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men4(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Powder TOF data from: '//
     1                        Veta(:idel(Veta))
            else if(DifCode(KRefBlock).gt.100) then
              Veta='???'
              do i=1,nCWPowderTypes
                if(OrderCWPowderTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men3(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Powder data from: '//Veta(:idel(Veta))
            else if(DifCode(KRefBlock).gt.0) then
              Veta='???'
              do i=1,nDiffTypes
                if(OrderDiffTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men1(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Data collection from: '//
     1                        Veta(:idel(Veta))
            else
              Veta='???'
              do i=1,nSingleTypes
                if(OrderSingleTypes(i).eq.DifCode(KRefBlock)) then
                  Veta=men2(i)
                  exit
                endif
              enddo
              TextInfo(NInfo)='Imported from: '//Veta(:idel(Veta))
            endif
            call DelChar(TextInfo(NInfo),'%')
            NInfo=NInfo+1
            TextInfo(NInfo)='Radiation: '//RadString(KRefBlock)
            NInfo=NInfo+1
            j=0
            if(PolarizationRefBlock(KRefBlock).eq.PolarizedCircular)
     1        then
              Veta='Circular'
            else if(PolarizationRefBlock(KRefBlock).eq.PolarizedMonoPer)
     1        then
              Veta='by monochromator in perpendicular setting'
              j=1
            else if(PolarizationRefBlock(KRefBlock).eq.PolarizedMonoPar)
     1        then
              Veta='by monochromator in parallel setting'
              j=1
            else
              Veta='Linearly'
            endif
            TextInfo(NInfo)='Polarization: '//Veta(:idel(Veta))
            if(j.gt.0) then
              NInfo=NInfo+1
              write(TextInfo(NInfo),'(''Glancing angle of the '',
     1                                ''monochromator: '',f8.3)')
     2          AngleMonRefBlock(KRefBlock)
              NInfo=NInfo+1
              write(TextInfo(NInfo),'(''Fraction of the perfectness '',
     1                                ''of monochromator: '',f8.3)')
     2          FractPerfMonRefBlock(KRefBlock)
            endif
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            go to 1150
          else if(CheckNumber.eq.nButtDelete) then
            if(NRef95(KRefBlock).gt.0) then
              NRef95Old(KRefBlock)=NRef95(KRefBlock)
              NRef95(KRefBlock)=0
              KRefBlock=0
              call FeQuestRemove(id)
              call FeTabsReset(UseTabs)
              NChange=NChange+1
              go to 1055
            else
              go to 1150
            endif
          else if(CheckNumber.eq.nButtUndelete) then
            if(NRef95(KRefBlock).le.0) then
              NRef95(KRefBlock)=NRef95Old(KRefBlock)
              call FeQuestRemove(id)
              call FeTabsReset(UseTabs)
              NChange=NChange-1
              go to 1055
            else
              go to 1150
            endif
          else if(CheckNumber.eq.nButtReimport.or.
     1            CheckNumber.eq.nButtModify) then
            if(SourceFileRefBlock(KRefBlock)(1:1).eq.'?'.and.
     1         CheckNumber.eq.nButtReimport) then
              go to 1150
            else if(NRef95(KRefBlock).le.0) then
              go to 1150
            endif
            NChange=NChange+1
          endif
          CreateNewRefBlock=CheckNumber.eq.nButtNew
          ReimportRefBlock=CheckNumber.eq.nButtReimport
          if(CheckNumber.ne.nButtReimport.and.
     1       CheckNumber.ne.nButtModify) then
            NRefBlock=NRefBlock+1
            KRefBlock=NRefBlock
            NChange=NChange+1
          endif
          call FeQuestRemove(id)
          call DeleteFile(fln(:ifln)//'_m95list.tmp')
          OnlyModify=CheckNumber.eq.nButtModify
          WhatToDo=0
          call FeTabsReset(UseTabs)
          go to 1190
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1150
        endif
        call FeTabsReset(UseTabs)
        if(ich.eq.0) then
          if(NChange.gt.0) then
            if(.not.FeYesNo(-1.,-1.,
     1         'Do you want to accept made changes?',1)) go to 4000
          endif
        else
          if(NChange.gt.0) then
            if(FeYesNo(-1.,-1.,
     1         'Do you want to discard made changes?',0)) go to 4000
          endif
        endif
        call FeQuestRemove(id)
        call DeleteFile(fln(:ifln)//'_m95list.tmp')
1170    if(NChange.gt.0.or.SilentRun) then
          do j=1,NRefBlock
            if(NRef95(j).le.0) ModifiedRefBlock(j)=.true.
          enddo
          Skrt=.false.
          do 1175i=1,NDatBlock
            do j=1,NRefBlock
              if((RefDatCorrespond(j).eq.i.and.NRef95(j).gt.0).or.
     1            RefDatCorrespond(j).le.0) go to 1175
            enddo
            Skrt=.true.
            NRef90(i)=0
1175      continue
          if(Skrt) then
            if(ExistM40) call UpdateDatBlocksM40
            if(ExistM42) call UpdateRefBlocksM42
          endif
          call CompleteM95(0)
          call CompleteM90
          if(Skrt) then
            if(ExistM40) call iom40(1,0,fln(:ifln)//'.m40')
            if(ExistM42) call iom42(1,0,fln(:ifln)//'.m42')
            call SetIntArrayTo(RefDatCorrespond,MxRefBlock,0)
          endif
        endif
        go to 3000
      else
        KRefBlock=1
        NRefBlock=1
        CreateNewRefBlock=.true.
        OnlyModify=.false.
        ReimportRefBlock=.false.
        NChange=NChange+1
      endif
      go to 1190
      entry EM9HKLReimport
      call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
      UseTabsIn=UseTabs
      Klic=1
      First=.true.
      KPhaseDR=1
      HKLReimport=.true.
      CreateNewRefBlock=.false.
      ReimportRefBlock=.true.
      OnlyModify=.false.
      NChange=1
      ImportMagnetic=.false.
      NRefBlockIn=NRefBlock
      call DeleteFile(fln(:ifln)//'.m90')
      ExistM40=ExistFile(fln(:ifln)//'.m40')
      ExistM50=ExistFile(fln(:ifln)//'.m50')
      ExistM90=.false.
      WizardMode=.false.
      go to 1195
1190  if(.not.SilentRun) then
        WizardId=NextQuestId()
        WizardMode=.true.
        WizardTitle=.true.
        WizardLines=17
        WizardLength=600.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     'x',0,LightGray,0,0)
      endif
1195  if(OnlyModify) go to 1400
      if(.not.CreateNewRefBlock) then
        if(DifCode(KRefBlock).gt.200) then
          WhatToDo=IdImportTOFPowder
          TOFInD=DifCode(KRefBlock).eq.IdTOFISISD
        else if(DifCode(KRefBlock).gt.100) then
          WhatToDo=IdImportCWPowder
        else if(DifCode(KRefBlock).gt.0) then
          WhatToDo=IdImportSingleDatRed
          if(ReimportRefBlock) then
            call MatBlock3(TrMPI,TrMP3I,maxNDim)
            call UnitMat(TrMP6I,maxNDim)
            call DRMatTrCellQ(TrMP3I,CellPar(1,1,KPhaseDR),
     1                        qui(1,1,KPhaseDR),quir(1,1,KPhaseDR),
     2                        TrMP6I)
            call AddVek(Qui(1,1,KPhaseDR),Quir(1,1,KPhaseDR),
     1                  Qu(1,1,1,KPhaseDR),3*NDimI(KPhaseDR))
            call SetMet(0)
          else
            call DRSetCell(0)
          endif
        else
          WhatToDo=IdImportSingle
        endif
        go to 1400
      endif
1200  ErrFlag=0
      id=NextQuestId()
      call FeQuestTitleMake(id,'Specify type of the file to be '//
     1                         'imported')
      if(KRefBlock.gt.1.or.KnownM50) then
        n=4
        if(WhatToDoLast.gt.0.and.WhatToDoLast.le.n) then
          m=WhatToDoLast
        else if(KRefBlock.gt.1) then
          if(DifCode(KRefBlock-1).gt.200) then
            m=IdImportTOFPowder
          else if(DifCode(KRefBlock-1).gt.100) then
            m=IdImportCWPowder
          else if(DifCode(KRefBlock-1).gt.0) then
            m=IdImportSingleDatRed
          else
            m=IdImportSingle
          endif
        else
          m=IdImportCWPowder
        endif
      else
        n=14
        m=IdImportSingleDatRed
      endif
      il=0
      xpoml=5.
      Veta='Single crystal:'
      j=1
      call FeBoldFont
      xpom=xpoml+FeTxLength(Veta)+80.
      call FeNormalFont
      tpom=xpom+CrwgXd+10.
      do i=1,n
        il=il+1
        if(i.eq.3) then
          Veta='Powder data:'
          j=1
        else if(i.eq.6) then
          Veta='Structure:'
          j=1
        else if(i.eq.11) then
          Veta='Magnetic parent structure:'
          j=1
        endif
        if(j.ne.0) then
          call FeQuestLblMake(id,xpoml,il,Veta,'L','B')
          j=0
        endif
        if(i.eq.IdImportSingleDatRed) then
          Veta='%known diffractometer formats'
        else if(i.eq.IdImportSingle) then
          Veta='%reflection file corrected for LP and absorption'
        else if(i.eq.IdImportCWPowder) then
          Veta='various C%W formats'
        else if(i.eq.IdImportTOFPowder) then
          Veta='various %TOF/ED formats'
        else if(i.eq.IdFullProfImport) then
          Veta='from Full%Prof'
        else if(i.eq.IdShelxImport) then
          Veta='from S%HELX'
        else if(i.eq.IdCifImport) then
          Veta='from %CIF'
        else if(i.eq.IdXDImport) then
          Veta='from %XD'
        else if(i.eq.IdJana2000Import) then
          Veta='from %Jana2000'
        else if(i.eq.IdPDBImport) then
          Veta='from PD%B'
        else if(i.eq.IdMagneticManual) then
          Veta='nuclear model made i%nteractively'
        else if(i.eq.IdMagneticSHELX) then
          Veta='nuclear model from SHE%LX'
        else if(i.eq.IdMagneticCIF) then
          Veta='nuclear model from CI%F'
        else if(i.eq.IdMagneticJana) then
          Veta='nuclear model from Jana200%6'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i.eq.m)
        if(ImportMagnetic.and.KRefBlock.gt.1.and.
     1     ((DifCode(1).gt.100.and.i.le.2).or.
     2      (DifCode(1).lt.100.and.i.gt.2)))
     3    call FeQuestCrwDisable(CrwLastMade)
      enddo
      call FeQuestButtonDisable(ButtonEsc)
1250  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1250
      endif
      if(ich.eq.0) then
        nCrw=nCrwFirst
        do i=1,n
          if(CrwLogicQuest(nCrw)) then
            WhatToDo=i
            go to 1300
          endif
          nCrw=nCrw+1
        enddo
      endif
1300  if(ich.ne.0) then
        if(CreateNewRefBlock) then
          NRefBlock=NRefBlock-1
          NChange=NChange-1
        endif
        if(ExistM95.or.ReimportRefBlock) then
          call FeQuestRemove(WizardId)
          go to 1050
        else
          go to 9900
        endif
      endif
      WhatToDoLast=WhatToDo
1400  if(OnlyModify) then
        isPowder=DifCode(KRefBlock).gt.100
        isTOF=DifCode(KRefBlock).gt.200.and.DifCode(KRefBlock).le.300
        isED=DifCode(KRefBlock).gt.300
      else
        isPowder=WhatToDo.eq.IdImportCWPowder.or.
     1           WhatToDo.eq.IdImportTOFPowder
        isTOF=WhatToDo.eq.IdImportTOFPowder
      endif
      if(ParentStructure) then
        ExistSingle=.not.isPowder
        call SSG2QMag(fln)
        call QMag2SSG(fln,0)
      endif
      if(WhatToDo.eq.IdImportSingleDatRed.or.
     1   WhatToDo.eq.IdImportSingle.or.
     2   WhatToDo.eq.IdImportCWPowder.or.
     3   WhatToDo.eq.IdImportTOFPowder.or.
     4   OnlyModify) then
        if(CreateNewRefBlock) then
          if(KRefBlock.eq.1) then
            CellRefBlock(1,0)=-1.
            call SetRealArrayTo(QuRefBlock(1,1,0),9,-333.)
            if(KnownM50) then
              call CopyVek(CellPar(1,1,1),CellRefBlock(1,0),6)
              call CopyVek(CellParSU(1,1,1),CellRefBlockSU(1,0),6)
              call CopyVek(Qu(1,1,1,1),QuRefBlock(1,1,0),
     1                     3*NDimI(KPhaseDR))
              if(isPowder) call SetBasicM41(0)
            endif
          endif
          call SetBasicM95(KRefBlock)
          if(KRefBlock.gt.1) then
            DifCodePrev=DifCode(KRefBlock-1)
          else
            DifCodePrev=0
          endif
          if(KnownM50) then
             NDim95(KRefBlock)=NDim(KPhaseDR)
             call CopyVek(CellRefBlock(1,0),CellRefBlock(1,KRefBlock),6)
             call CopyVek(QuRefBlock(1,1,0),QuRefBlock(1,1,KRefBlock),
     1                    3*NDimI(KPhaseDR))
             if(MagneticType(KPhaseDR).ne.0) then
               RadiationRefBlock(KRefBlock)=NeutronRadiation
               if(KRefBlock.le.1.and..not.ReimportRefBlock) then
                 if(LamAveRefBlock(KRefBlock).le.0.and.
     1              .not.isTOF) then
                   LamAveRefBlock(KRefBlock)=1.
                   LamA1RefBlock(KRefBlock)=1.
                   LamA2RefBlock(KRefBlock)=1.
                   LamRatRefBlock(KRefBlock)=0.
                   TempRefBlock(KRefBlock)=10.
                 endif
               else if(KRefBlock.gt.1) then
                 LamAveRefBlock(KRefBlock)=LamAveRefBlock(KRefBlock-1)
                 LamA1RefBlock(KRefBlock)=LamA1RefBlock(KRefBlock-1)
                 LamA2RefBlock(KRefBlock)=LamA2RefBlock(KRefBlock-1)
                 LamRatRefBlock(KRefBlock)=LamRatRefBlock(KRefBlock-1)
                 TempRefBlock(KRefBlock)=TempRefBlock(KRefBlock-1)
               endif
             else
               if(KRefBlock.le.1.and..not.ReimportRefBlock.and.
     1            LamAveRefBlock(KRefBlock).le.0.) then
                 LamAveRefBlock(KRefBlock)=LamAve(1)
                 LamA1RefBlock(KRefBlock)=LamAve(1)
                 LamA2RefBlock(KRefBlock)=LamAve(1)
               endif
            endif
          endif
          if(WhatToDo.eq.IdImportSingle.or.
     1       WhatToDo.eq.IdImportSingleDatRed) then
            if(WhatToDo.eq.IdImportSingle) then
              if(DifCodePrev.gt.-100.and.DifCodePrev.lt.0) then
                DifCode(KRefBlock)=DifCodePrev
              else
                DifCode(KRefBlock)=IdImportSHELXI
              endif
            else
              if(DifCodePrev.gt.0.and.DifCodePrev.lt.100) then
                DifCode(KRefBlock)=DifCodePrev
              else
                DifCode(KRefBlock)=IdKumaCCD
              endif
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
            if(LamAveRefBlock(KRefBlock).lt.0.) then
              LamAveRefBlock(KRefBlock)=LamAveD(6)
              LamA1RefBlock(KRefBlock)=LamA1D(6)
              LamA2RefBlock(KRefBlock)=LamA2D(6)
              LamRatRefBlock(KRefBlock)=LamRatD(6)
            endif
          else if(WhatToDo.eq.IdImportCWPowder) then
            if(DifCodePrev.gt.100.and.DifCodePrev.lt.200) then
              DifCode(KRefBlock)=DifCodePrev
            else
              DifCode(KRefBlock)=IdDataFree
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
            if(LamAveRefBlock(KRefBlock).lt.0.) then
              LamAveRefBlock(KRefBlock)=LamAveD(5)
              LamA1RefBlock(KRefBlock)=LamA1D(5)
              LamA2RefBlock(KRefBlock)=LamA2D(5)
              LamRatRefBlock(KRefBlock)=LamRatD(5)
            endif
          else if(WhatToDo.eq.IdImportTOFPowder) then
            if(DifCodePrev.gt.200.and.DifCodePrev.lt.300) then
              DifCode(KRefBlock)=DifCodePrev
            else
              DifCode(KRefBlock)=IdTOFGSAS
            endif
            PolarizationRefBlock(KRefBlock)=PolarizedLinear
          endif
        endif
        call EM9ImportSavePar
        write(Cislo,'(''.l'',i2)') KRefBlock
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        if(OnlyModify) then
          ICorrZpet=0
          go to 1600
        endif
        if(.not.ReimportRefBlock) call DeleteFile(RefBlockFileName)
        Navrat=0
1500    ErrFlag=0
        if(WhatToDo.eq.IdImportSingleDatRed) then
          call EM9ImportDatRedData(Navrat)
        else if(WhatToDo.eq.IdImportSingle) then
          call EM9ImportSingle(Navrat)
        else if(WhatToDo.eq.IdImportCWPowder) then
          call EM9ImportCWPowder(Navrat)
        else if(WhatToDo.eq.IdImportTOFPowder) then
          call EM9ImportTOFPowder(Navrat)
        endif
        if(ErrFlag.gt.0) then
          if(CreateNewRefBlock) then
            NRefBlock=NRefBlock-1
            NChange=NChange-1
          endif
          if(ExistM95.or.ReimportRefBlock) then
            call FeQuestRemove(WizardId)
            go to 1050
          else
            go to 9900
          endif
        else if(ErrFlag.lt.0) then
          if(CreateNewRefBlock) then
            go to 1200
          else
            call FeQuestRemove(WizardId)
            go to 1050
          endif
        endif
        Navrat=1
        ICorrZpet=0
1600    ErrFlag=0
        if(isPowder) then
          if(OnlyModify) then
            i=-1
            RadiationDetails=.true.
            call EM9CompleteBasic(i,ich)
            ModifiedRefBlock(KRefBlock)=.true.
          endif
        else
          if(OnlyModify.and.DifCode(KRefBlock).lt.0) then
            i=-1
            RadiationDetails=.true.
            call EM9CompleteBasic(i,ich)
            ModifiedRefBlock(KRefBlock)=.true.
            go to 1700
          endif
          if(CorrLp(KRefBlock).ge.0.and.ICorrZpet.lt.2) then
            call DRCorrLPDecay(OnlyModify)
            if(ErrFlag.gt.0) then
              if(OnlyModify) then
                call FeQuestRemove(WizardId)
                go to 1050
              else
                go to 9900
              endif
            else if(ErrFlag.lt.0) then
              go to 1500
            endif
          endif
          ErrFlag=0
          if(CorrAbs(KRefBlock).ge.0) then
            call DRAbsCorr(OnlyModify.and.CorrLp(KRefBlock).lt.0)
            if(ErrFlag.gt.0) then
              if(.not.OnlyModify) NRef95(KRefBlock)=0
              call FeQuestRemove(WizardId)
              go to 1050
            else if(ErrFlag.lt.0) then
              if(CorrLp(KRefBlock).ge.0) then
                go to 1600
              else
                go to 1500
              endif
            endif
          endif
        endif
1700    if(SilentRun) then
          go to 1050
        endif
        call FeFillTextInfo('em9importdatreddata2.txt',0)
        call FeWizardTextInfo('INFORMATION',1,-1,1,ich)
        if(ich.ne.0) then
          if(ich.gt.0) then
            if(CorrAbs(KRefBlock).ge.0) then
              ICorrZpet=1
              go to 1600
            else if(CorrLp(KRefBlock).ge.0) then
              ICorrZpet=2
              go to 1600
            else
              go to 1500
            endif
          else
            go to 9900
          endif
        endif
1750    call FeQuestRemove(WizardId)
        go to 1050
      endif
      if(WhatToDo.ne.IdMagneticManual.and.
     1   WhatToDo.ne.IdMagneticSHELX.and.
     2   WhatToDo.ne.IdMagneticCIF.and.
     3   WhatToDo.ne.IdMagneticJana) then
        call FeQuestRemove(WizardId)
        WizardMode=.false.
      endif
2000  if(WhatToDo.eq.IdShelxImport) then
        call ReadSHELX(0,ich)
        go to 9999
      else if(WhatToDo.eq.IdCIFImport) then
        call ReadCIF(0,CoDal)
        if(ErrFlag.ne.0) go to 9999
        KnownM50=.true.
        if(CoDal.eq.1) then
          go to 1190
        else
          go to 9999
        endif
      else if(WhatToDo.eq.IdXDImport) then
        call ZXD
        go to 9999
      else if(WhatToDo.eq.IdPDBImport) then
        call ReadPDB
        go to 9999
      else if(WhatToDo.eq.IdMagneticManual.or.
     1        WhatToDo.eq.IdMagneticSHELX.or.
     2        WhatToDo.eq.IdMagneticCIF.or.
     3        WhatToDo.eq.IdMagneticJana) then
        call EM9ImportMagnetic(WhatToDo-IdMagneticManual,CoDal)
        if(CoDal.lt.0) then
          go to 9900
        else if(CoDal.gt.0) then
          go to 1200
        endif
        KnownM50=.true.
        ImportMagnetic=.true.
        call FeQuestRemove(WizardId)
        go to 1190
      endif
3000  FirstSingle=0
      FirstPowder=0
      FirstSingle=0
      FirstPowder=0
      do i=1,NRefBlock
        if(ModifiedRefBlock(i)) then
          if(DifCode(i).lt.100) then
            if(FirstSingle.eq.0) FirstSingle=i
          else
            if(FirstPowder.eq.0) FirstPowder=i
          endif
        endif
      enddo
      if(FirstSingle.gt.0) then
        KRefBlock=FirstSingle
        isPowder=.false.
      else if(FirstPowder.gt.0) then
        KRefBlock=FirstPowder
        isPowder=.true.
      else
        go to 9999
      endif
      if(ImportMagnetic) then
        if(NRefBlock.gt.0) then
!          if(NQMag(KPhase).gt.1.and..not.isPowder) then
!          if(.not.isPowder) then
!            ExistM90=.false.
!            go to 3020
!          endif
!          SilentRun=NRefBlock.le.1
          SilentRun=.false.
          call EM9CreateM90(0,ich)
          call iom90(0,fln(:ifln)//'.m90')
          if(isPowder) then
            do i=1,NDatBlock
              call SetBasicM41(i)
              if(Radiation(i).eq.NeutronRadiation) then
                j=0
                if(TOFSigma(1,i).gt.-3000.) then
                  j=j+1
                  call CopyVek(TOFSigma(1,i),GaussPwd(1,1,i),3)
                endif
                if(TOFGamma(1,i).gt.-3000.) then
                  j=j+2
                  call CopyVek(TOFGamma(1,i),LorentzPwd(1,1,i),3)
                endif
                if(j.gt.0) KProfPwd(1,i)=j
                TOFKey(i)=1
                if(TOFAlfbe(1,i).gt.-3000.) then
                  AsymPwd(1,i)=TOFAlfbe(1,i)
                  AsymPwd(2,i)=TOFAlfbe(3,i)
                  AsymPwd(3,i)=TOFAlfbe(2,i)
                  AsymPwd(4,i)=TOFAlfbe(4,i)
                  KAsym(i)=IdPwdAsymTOF1
                  NAsym(i)=4
                endif
                if(TOFAlfbt(1,i).gt.-3000.) then
                  AsymPwd(5,i)=TOFAlfbt(1,i)
                  AsymPwd(6,i)=TOFAlfbt(3,i)
                  AsymPwd(7,i)=TOFAlfbt(2,i)
                  AsymPwd(8,i)=TOFAlfbt(4,i)
                  KAsym(i)=IdPwdAsymTOF2
                  NAsym(i)=8
                endif
              endif
            enddo
            call iom41(1,0,fln(:ifln)//'.m41')
          endif
          ExistM90=.true.
3020      ExistM95=.true.
          SilentRun=.false.
        else
          ExistM90=.false.
          ExistM95=.false.
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        if(ExistM95.and.isPowder) then
          FlnOld=Fln
          iflnOld=ifln
          Fln=Fln(:ifln)//'_tmp'
          iFln=idel(Fln)
          i=0
3050      if(StructureExists(Fln)) then
            i=i+1
            write(Cislo,'(i5)') i
            call Zhusti(Cislo)
            Fln=Fln(:iFln)//'_'//Cislo(:idel(Cislo))
            go to 3050
          endif
          iFln=idel(Fln)
        endif
        WizardId=NextQuestId()
        WizardMode=.true.
        WizardTitle=.true.
        WizardLines=10
        WizardLength=570.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     'x',0,LightGray,0,0)
        if(isPowder) then
          if(ExistM90) then
            Veta='em9importsolvepowder.txt'
          else
            go to 3400
          endif
        else
          go to 3450
c          if(ExistM95) then
c            Veta='em9importsolvesinglecrystal.txt'
c          else
c            go to 3400
c          endif
        endif
        call FeFillTextInfo(Veta,0)
        call FeWizardTextInfo('INFORMATION',0,1,1,ich)
        call DeleteFile(Fln(:iFln)//'.m70')
        if(isPowder.and.ExistM90) then
          NTwin=1
          k=1
          call DeleteFile(fln(:ifln)//'.m70')
          NDimIP=NDimI(KPhaseDR)
3100      if(NDimI(KPhaseDR).gt.0) then
            do i=1,NSymm(KPhaseDR)
              if(k.eq.1) then
                call SetRealArrayTo(s6(1,i,1,KPhaseDR),NDim(KPhaseDR),
     1                              0.)
              else
                call CopyMat(rm(1,i,1,KPhaseDR),rm6(1,i,1,KPhaseDR),3)
              endif
            enddo
            if(k.ne.1) then
              NDimIP=NDimI(KPhaseDR)
              NDim(KPhaseDR)=3
              NDimq(KPhaseDR)=9
              NDimI(KPhaseDR)=0
            endif
          endif
          NQMagOld=NQMag(KPhaseDR)
          NQMag(KPhaseDR)=0
          ParentStructure=.false.
          call iom50(1,0,fln(:ifln)//'.m50')
          if(k.eq.1) then
            call SetBasicM40(.true.)
            call iom40(1,0,fln(:ifln)//'.m40')
          else
            call CopyFile(FlnOld(:iFlnOld)//'.m44',Fln(:ifln)//'.m40')
            call SetIntArrayTo(kiPwd,MxParPwd,0)
            call iom41(1,0,fln(:ifln)//'.m41')
          endif
          call CopyFile(FlnOld(:iFlnOld)//'.m90',Fln(:ifln)//'.m90')
          call CopyFile(FlnOld(:iFlnOld)//'.m95',Fln(:ifln)//'.m95')
          if(NDatBlock.gt.1) then
            do i=1,NDatBlock
              MenuDatBlock(i)=DatBlockName(i)
              if(DataType(i).eq.1) then
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     2            '->single crystal'
              else
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'->powder'
              endif
              if(Radiation(i).eq.NeutronRadiation) then
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'/neutrons'
              else
                MenuDatBlock(i)=
     1            MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'/X-ray'
              endif
            enddo
          endif
          if(k.eq.1) then
            call CopyFile(fln(:ifln)//'.m40',PreviousM40)
            call CopyFile(fln(:ifln)//'.m41',PreviousM41)
            call CopyFile(fln(:ifln)//'.m50',PreviousM50)
3110        call CrlLeBailMaster(ich)
            if(ich.ne.0) then
              if(ich.gt.0) then
                go to 3110
              else
                go to 3130
              endif
            else
              if(.not.FeYesNo(-1.,-1.,'Do you really want to leave '//
     1                        'the form for le Bail refinement?',
     2                        0)) go to 3110
            endif
            call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            if(NQMagOld.gt.0)
     1        call CopyVek(QuPwd(1,1,KPhaseDR),QMag(1,1,KPhaseDR),3)
            call CopyVek(CellPwd(1,KPhaseDR),CellPar(1,1,KPhaseDR),6)
            call iom50(1,0,flnOld(:iFlnOld)//'.m50')
            call iom41(1,0,flnOld(:iFlnOld)//'.m41')
            call CopyFile(Fln(:ifln)//'.m90',FlnOld(:iFlnOld)//'.m90')
            call CopyFile(Fln(:ifln)//'.m95',FlnOld(:iFlnOld)//'.m95')
            call MoveFile(fln(:ifln)//'.m70',flnOld(:iflnOld)//'.m70')
          else
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
3112        read(lni,FormA,end=3114) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 3112
3114        call CloseIfOpened(lni)
            Veta='fixed all *'
            write(lno,FormA) Veta(:idel(Veta))
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            call RefRewriteCommands(1)
            call iom50(0,0,fln(:ifln)//'.m50')
3120        call CrlRietveldMaster(ich)
            if(ich.ne.0) then
              if(ich.gt.0) then
                k=k-1
                call MoveFile(flnOld(:iflnOld)//'.m70',
     1                        fln(:ifln)//'.m70')
                go to 3100
              else
                go to 3130
              endif
            else
              if(.not.FeYesNo(-1.,-1.,'Do you really want to leave '//
     1                        'the form for Rietveld refinement?',
     2                        0)) go to 3120
            endif
            if(NDimIP.ne.NDimI(KPhaseDR)) then
              NDim(KPhaseDR)=3+NDimIP
              NDimq(KPhaseDR)=NDim(KPhaseDR)**2
              NDimI(KPhaseDR)=NDimIP
              if(allocated(qcnt)) deallocate(qcnt,phf,sphf,OrthoX40,
     1                                       OrthoDelta,OrthoEps)
              n=NAtAll
              allocate(qcnt(3,n),phf(n),sphf(n),
     1                 OrthoX40(n),OrthoDelta(n),OrthoEps(n))
              do ia=1,NAtAll
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                qcnt(1:3,ia)=0.
                 phf(ia)=0.
                sphf(ia)=0.
                OrthoX40(ia)=0.
                OrthoDelta(ia)=1.
                OrthoEps(ia)=.95
              enddo
              call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            endif
            call CopyFile(fln(:ifln)//'.m40',flnOld(:iFlnOld)//'.m43')
          endif
          go to 3140
3130      NInfo=2
          TextInfo(1)='You are about to leave the wizard for handling'//
     1                ' of magnetic structures from powder.'
          TextInfo(2)='Your work will not be lost but from now you '//
     1                'have to select your own way.'
          if(FeYesNoHeader(-1.,-1.,'Do you really want do leave the '//
     1                     'wizard?',0)) then
            call iom50(0,0,flnOld(:iFlnOld)//'.m50')
            if(NQMagOld.gt.0)
     1        call CopyVek(QuPwd(1,1,KPhaseDR),QMag(1,1,KPhaseDR),3)
            call CopyVek(CellPwd(1,KPhase),CellPar(1,1,KPhaseDR),6)
            call iom50(1,0,flnOld(:iFlnOld)//'.m50')
            call iom41(1,0,flnOld(:iFlnOld)//'.m41')
            call CopyFile(Fln(:ifln)//'.m90',FlnOld(:iFlnOld)//'.m90')
            call CopyFile(Fln(:ifln)//'.m95',FlnOld(:iFlnOld)//'.m95')
            call MoveFile(fln(:ifln)//'.m70',flnOld(:iflnOld)//'.m70')
          else
            go to 3100
          endif
3140      call DeletePomFiles
          call DeleteAllFiles(Fln(:ifln)//'*')
          call iom50(0,0,flnOld(:iflnOld)//'.m50')
          call iom40(0,0,flnOld(:iflnOld)//'.m40')
          call iom90(0,flnOld(:iflnOld)//'.m90')
          call iom95(0,flnOld(:iflnOld)//'.m95')
          if(ich.eq.0.and.k.eq.1) then
            k=k+1
            go to 3100
          endif
          if(ich.lt.0.or.k.eq.2) then
            fln=flnOld
            ifln=iFlnOld
          endif
          if(ich.lt.0) then
            call FeQuestRemove(WizardId)
            WizardMode=.false.
            go to 9999
          endif
        endif
        if(.not.isPowder.and.ExistM95) then
3300      call CopyFile(FlnOld(:iFlnOld)//'.m40',Fln(:ifln)//'.m40')
          NQMagOld=NQMag(KPhaseDR)
          NQMag(KPhaseDR)=0
          ParentStructure=.false.
          MagneticType(KPhaseDR)=0
          call SSG2QMag(fln)
          call iom50(1,0,fln(:ifln)//'.m50')
          FormatOut=Format95
          FormatIn =Format95
          if(NQMagOld.gt.0) then
            FormatIn(5:5)='4'
          else
            FormatIn(5:5)='3'
          endif
          FormatOut(5:5)='3'
          call iom95(0,flnOld(:iflnOld)//'.m95')
          NDimOld=4
          do KRefB=1,NRefBlock
            call OpenRefBlockM95(95,KRefB,flnOld(:iflnOld)//'.m95')
            if(ErrFlag.ne.0) go to 3350
            write(Cislo,'(''.l'',i2)') KRefB
            if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
            RefBlockFileName=Fln(:ifln)//Cislo(:idel(Cislo))
            call OpenFile(96,RefBlockFileName,'formatted','unknown')
            if(ErrFlag.ne.0) go to 3350
            NRef95(KRefB)=0
            NLines95(KRefB)=0
3320        NDim(KPhaseDR)=NDimOld
            MaxNDim=NDimOld
            Format95=FormatIn
            call DRGetReflectionFromM95(95,iend,ich)
            if(ich.ne.0) go to 3350
            if(iend.ne.0) go to 3350
            do i=4,NDim(KPhaseDR)
              if(ih(i).ne.0) go to 3320
            enddo
            pom1=corrf(1)*corrf(2)
            if(DelMi(KRefBlock).ne.0.)
     1        pom1=pom1*exp(DelMi(KRefBlock)*tbar*10.)
            if(RunScN(KRefBlock).gt.0) then
              ikde=RunScGrN(ifix(expos),KRefBlock)
              if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                pom=ScFrame(ikde,KRefBlock)
              else
                c1=(expos-float(RunScGrM0(ikde,KRefBlock)))/
     1              float(RunScGrM(ikde,KRefBlock)-1)
                pom=(1.-c1)*ScFrame(ikde,KRefBlock)+
     1              c1*ScFrame(ikde+1,KRefBlock)
              endif
              pom1=pom1*pom
            endif
            ri=ri*pom1
            rs=rs*pom1
            corrf(1)=1.
            corrf(2)=1.
            NProf=0
            NDim(KPhaseDR)=3
            maxNDim=3
            Format95=FormatOut
            call DRPutReflectionToM95(96,nl)
            NRef95(KRefB)=NRef95(KRefB)+1
            NLines95(KRefB)=NLines95(KRefB)+nl
            NDim(KPhaseDR)=NDimOld
            Format95=FormatIn
            go to 3320
3350        call CloseIfOpened(95)
            call CloseIfOpened(96)
            SourceFileRefBlock(KRefB)='?The strucure created from "'//
     1                                flnOld(:iflnOld)//'"'
            SourceFileDateRefBlock(KRefB)='?'
            SourceFileTimeRefBlock(KRefB)='?'
            UseTrRefBlock(KRefB)=.false.
            NDim95(KRefB)=3
            CellReadIn(KRefB)=.false.
            if(NLines95(KRefB).lt.0) NLines95(KRefB)=NRef95(KRefB)
          enddo
          call iom95(1,fln(:ifln)//'.m95')
          call FeQuestRemove(WizardId)
          WizardMode=.false.
          call CompleteM95(0)
          if(NQMag(KPhase).gt.1) go to 9999
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          UpdateAnomalous=.false.
          SilentRun=NRefBlock.le.1
          call EM9CreateM90(0,ich)
          SilentRun=.false.
          call iom90(0,fln(:ifln)//'.m90')
          WizardId=NextQuestId()
          WizardMode=.true.
          WizardTitle=.true.
          WizardLines=10
          WizardLength=570.
          call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                       'x',0,LightGray,0,0)
          call CrlSingleCrystalMaster(ich)
          if(ich.ne.0) then
            if(ich.gt.0) then
              go to 3300
            else
              go to 3400
            endif
          endif
          call iom50(0,0,flnOld(:iflnOld)//'.m50')
          NQMag(KPhaseDR)=NQMagOld
          ParentStructure=.true.
          MagneticType(KPhaseDR)=1
          call CopyFile(fln(:ifln)//'.l51',flnOld(:iflnOld)//'.l51')
          call iom50(1,0,flnOld(:iflnOld)//'.m50')
          call CopyFile(fln(:ifln)//'.m40',flnOld(:iflnOld)//'.m40')
          call DeletePomFiles
          call DeleteAllFiles(Fln(:ifln)//'*')
          fln=flnOld
          ifln=iFlnOld
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CrlSetMagMoments(NQMag(KPhaseDR))
          call iom40(1,0,fln(:ifln)//'.m40')
          ExistM90=.false.
          call iom95(0,fln(:ifln)//'.m95')
        endif
3400    call RefOpenCommands
        NacetlInt(nCmdncykl)=100
        NacetlReal(nCmdtlum)=.1
        call RefRewriteCommands(1)
3450    if(NQMag(KPhase).le.1) then
          id=NextQuestId()
          call FeFillTextInfo('em9importrepanalysis.txt',0)
          il=3
          call FeQuestLblMake(id,WizardLength*.5,il,'INFORMATION','C',
     1                        'B')
          il=il+1
          tpom=(WizardLength-FeTxLength(TextInfo(1)))*.5
          il=-10*il
          do i=1,NInfo
            il=il-5
            call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
          enddo
          call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
3500      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          if(ich.lt.0) then
            go to 9900
          endif
          call FeQuestRemove(WizardId)
          WizardMode=.false.
          call UseIrreps
          go to 9999
        else
          call FeQuestRemove(WizardId)
          WizardMode=.false.
          call RAIsodistort
        endif
      else
        ich=0
        if(ExistM40) then
          NDatBlockIn=NDatBlock
        else
          call SetBasicM40(.false.)
          call iom40(1,0,fln(:ifln)//'.m40')
          NDatBlockIn=0
        endif
        if(NRefBlockIn.gt.0.or.isPowder) then
          call FeTabsReset(UseTabs)
          call EM9CreateM90(0,ich)
          call iom90(0,fln(:ifln)//'.m90')
          ExistM90=.true.
        endif
        if(.not.ReimportRefBlock.and.isPowder) then
          call SetBasicM41(0)
          do i=1,NDatBlock
            if(Radiation(i).eq.NeutronRadiation) then
              j=0
              if(TOFSigma(1,i).gt.-3000.) then
                j=j+1
                call CopyVek(TOFSigma(1,i),GaussPwd(1,1,i),3)
              endif
              if(TOFGamma(1,i).gt.-3000.) then
                j=j+2
                call CopyVek(TOFGamma(1,i),LorentzPwd(1,1,i),3)
              endif
              if(j.gt.0) KProfPwd(1,i)=j
              TOFKey(i)=1
              if(TOFAlfbe(1,i).gt.-3000.) then
                AsymPwd(1,i)=TOFAlfbe(1,i)
                AsymPwd(2,i)=TOFAlfbe(3,i)
                AsymPwd(3,i)=TOFAlfbe(2,i)
                AsymPwd(4,i)=TOFAlfbe(4,i)
                KAsym(i)=IdPwdAsymTOF1
                NAsym(i)=4
              endif
              if(TOFAlfbt(1,i).gt.-3000.) then
                AsymPwd(5,i)=TOFAlfbt(1,i)
                AsymPwd(6,i)=TOFAlfbt(3,i)
                AsymPwd(7,i)=TOFAlfbt(2,i)
                AsymPwd(8,i)=TOFAlfbt(4,i)
                KAsym(i)=IdPwdAsymTOF2
                NAsym(i)=8
              endif
            endif
          enddo
          call iom41(1,0,fln(:ifln)//'.m41')
        endif
        if(NRefBlockIn.le.0) then
          if(NAtCalc.le.0) then
            if(isPowder) then
              if(CyclicWizardHotovo) go to 9999
              id=NextQuestId()
              il=8
              xqd=400.
              call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
              Veta='The program can now continue with a wizard to '//
     1             'refine profile parameters,'
              tpom=5.
              il=-10
              call FeQuestLblMake(id,tpom,il,Veta,'L','N')
              Veta='make a space group test and solve the structure:'
              tpom=5.
              il=il-6
              call FeQuestLblMake(id,tpom,il,Veta,'L','N')
              Veta='%Yes, I would like to continue with the wizard'
              xpom=5.
              tpom=xpom+CrwgXd+10.
              il=il-10
              do i=1,2
                call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,
     1                              CrwYd,1,1)
                if(i.eq.1) then
                  nCrwYes=CrwLastMade
                  Veta='%No, I shall go by my own way'
                endif
                call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                il=il-7
              enddo
              il=il-3
              call FeQuestLinkaMake(id,il)
              nLinka=LinkaLastMade
              call FeFillTextInfo('em9lebailbeforesgtest.txt',0)
              il=il-10
              call FeQuestLblMake(id,xqd*.5,il,'INFORMATION','C','N')
              nLblFirst=LblLastMade
              il=il-10
              tpom=5.
              do i=1,NInfo
                call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
                il=il-6
              enddo
              nLblLast=LblLastMade
3550          call FeQuestEvent(id,ich)
              if(CheckType.eq.EventCrw) then
                if(CrwLogicQuest(nCrwYes)) then
                  call FeQuestLinkaOn(nLinka)
                else
                  call FeQuestLinkaOff(nLinka)
                endif
                do nLbl=nLblFirst,nLblLast
                  if(CrwLogicQuest(nCrwYes)) then
                    call FeQuestLblOn(nLbl)
                  else
                    call FeQuestLblOff(nLbl)
                  endif
                enddo
                go to 3550
              else if(CheckType.ne.0) then
                call NebylOsetren
                go to 3550
              endif
              if(CrwLogicQuest(nCrwYes)) then
                ich=0
              else
                ich=1
              endif
              call FeQuestRemove(id)
              if(ich.ne.0) go to 9999
              if(NDatBlock.gt.1) then
                do i=1,NDatBlock
                  MenuDatBlock(i)=DatBlockName(i)
                  if(DataType(i).eq.1) then
                    MenuDatBlock(i)=
     1               MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     2                '->single crystal'
                  else
                    MenuDatBlock(i)=
     1                MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     2                '->powder'
                  endif
                  if(Radiation(i).eq.NeutronRadiation) then
                    MenuDatBlock(i)=
     1                MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     2                '/neutrons'
                  else
                    MenuDatBlock(i)=
     1                MenuDatBlock(i)(:idel(MenuDatBlock(i)))//'/X-ray'
                  endif
                enddo
              endif
              call CrlLeBailMaster(ich)
              if(ich.ne.0) go to 3700
            endif
            call DRMakeMenuRefBlock
            if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1         DifCode(KRefBlock).eq.IdSXD.or.
     2         DifCode(KRefBlock).eq.IdTopaz.or.
     3         DifCode(KRefBlock).eq.IdSCDLANL.or.
     4         DifCode(KRefBlock).eq.IdSENJU) then
              ChangeSGTest=.false.
            else
              call DRSGTest(10,ChangeSGTest)
            endif
          else
            ChangeSGTest=.false.
          endif
          if(isPowder.and.ChangeSGTest) then
            NInfo=1
            TextInfo(1)='The last le Bail refinement has used a lower'//
     1                  ' space group.'
            if(FeYesNoHeader(-1.,-1.,'Do you want refine profile '//
     1                       'parameters once more?',1)) then
              call iom50(0,0,fln(:ifln)//'.m50')
              call Refine(0,RefineEnd)
            endif
          else
            call EM9CreateM90(0,ich)
            call iom90(0,fln(:ifln)//'.m90')
          endif
        endif
3700    WizardMode=.false.
        if(ExistM40.and.NDatBlockIn.gt.0) then
          if(ParentStructure) then
            call SSG2QMag(fln)
            call iom40Only(0,0,fln(:ifln)//'.m40')
          endif
          do i=NDatBlockIn+1,NRefBlock
            if(isPowder.and.(Radiation(i).ne.NeutronRadiation.or.
     1         (TOFSigma(1,i).lt.-3000..and.TOFGamma(1,i).lt.-3000.)))
     2        call SetBasicM41(i)
            sc(1,i)=1.
          enddo
          call iom40(1,0,fln(:ifln)//'.m40')
          if(ParentStructure) call QMag2SSG(fln,0)
        endif
        if(ParentStructure) then
          call iom40Only(0,0,fln(:ifln)//'.m44')
        else
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        if(Klic.eq.0.and.NAtCalc.le.0.and.ich.eq.0.and..not.SilentRun)
     1    then
          if(isPowder.and.ChangeSGTest) then
          endif
          call FeFillTextInfo('em9runsolution.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          call RunSolution
        endif
      endif
      go to 9999
4000  call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_m95list.tmp')
      if(ExistFile(fln(:ifln)//'.z95')) then
        call MoveFile(fln(:ifln)//'.z95',fln(:ifln)//'.m95')
      else
        call DeleteFile(fln(:ifln)//'.m95')
      endif
      if(ExistFile(fln(:ifln)//'.z90')) then
        call CopyFile(fln(:ifln)//'.z90',fln(:ifln)//'.m90')
      else
        call DeleteFile(fln(:ifln)//'.m90')
      endif
      if(ExistM50) then
        call MoveFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
      endif
      if(ExistM40) then
        call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      endif
      if(ExistM42) then
        call MoveFile(fln(:ifln)//'.z42',fln(:ifln)//'.m42')
      endif
      go to 9999
9900  call FeQuestRemove(WizardId)
      WizardMode=.false.
9999  call DeleteFile(fln(:ifln)//'.l95')
      call DeleteFile(fln(:ifln)//'.z95')
      call DeleteFile(fln(:ifln)//'.z90')
      call DeleteFile(fln(:ifln)//'.z50')
      call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z42')
      UseTabs=UseTabsIn
      return
      end
