      subroutine EM9OutputOfRejected(n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*80 t80,p80,ta80(2),Veta
      integer UseTabsIn
      equivalence (t80,ta80(1)),(p80,ta80(2))
      UseTabsIn=UseTabs
      if(NExt(n).gt.0) then
        if(n.eq.1) then
          t80='Summary of reflections absent due to systematic '//
     1        'extinctions'
        else
          t80='Summary of reflections which could not be transformed '//
     1        'to integer indices'
        endif
        call TitulekVRamecku(t80)
        avis=SumRelI(n)/float(NExt(n))
        write(t80,'(''n(all) :'',i7,'', n(obs) :'',i7)')
     1    NExt(n),NExtObs(n)
        call newln(1)
        write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        NInfo=1
        TextInfo(NInfo)=t80
        write(t80,'(''Average(I/Sig(I)) : '',f5.2)') avis
        call newln(1)
        write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        NInfo=NInfo+1
        TextInfo(NInfo)=t80
        call newln(1)
        write(lst,FormA1)
        NInfo=NInfo+1
        if(NExt500(n).le.0) then
          if(N.eq.1) then
            t80='All absent'
          else
            t80='All rejected'
          endif
          t80=t80(:idel(t80))//' reflections classified as unobserved'
          TextInfo(NInfo)=t80
          call NewLn(1)
          write(lst,FormA1)(t80(i:i),i=1,idel(t80))
        else
          if(NExt500(n).ne.500) call Indexx(NExt500(n),ria(1,n),
     1                                      OrderExtRef(1,n))
          if(Line.gt.50) call NewPg(0)
          if(n.eq.1) then
            p80='List of the strongest absent reflections:'
          else
            p80='List of the strongest rejected reflections:'
          endif
          call NewLn(1)
          write(lst,FormA1)(p80(i:i),i=1,idel(p80))
          if(n.eq.2) then
            Veta='Indices are related to the original cell !!!'
            call NewLn(1)
            write(lst,FormA1)(Veta(i:i),i=1,idel(p80))
          endif
          call NewLn(1)
          write(lst,FormA1)
          TextInfo(NInfo)=p80
          if(n.eq.2) then
            NInfo=NInfo+1
            TextInfo(NInfo)=Veta
          endif
          xpom=FeTxLength('0000')
          UseTabs=NextTabs()
          xx=0.
          do i=1,maxNDim
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          xpom=FeTxLength('0000000.0')
          do i=1,3
            xx=xx+xpom
            call FeTabsAdd(xx,UseTabs,IdLeftTab,' ')
          enddo
          write(t80,EM9Form1)(indices(i),i=1,maxNDim)
          t80(idel(t80)+1:)='       I      sig(I)  I/sig(I)'
          idlp=idel(t80)
          call newln(1)
          write(lst,FormA1)(t80(i:i),i=1,idlp),(' ',i=1,10),
     1                     (t80(i:i),i=1,idlp)
          k=0
          p80=' '
          do i=1,maxNDim+3
            call kus(t80,k,Cislo)
            if(i.eq.1) then
              p80=Tabulator//Cislo
            else
              p80=p80(:idel(p80))//Tabulator//Cislo
            endif
          enddo
          NInfo=NInfo+1
          TextInfo(NInfo)=p80
          do i=1,min(18-NInfo,NExt500(n))
            kk=OrderExtRef(i,n)
            write(t80,EM9Form2)(IHExt(j,kk,n),j=1,maxNDim)
            pom1=-float(ria(kk,n))*.0001
            pom2= float(rsa(kk,n))*.01
            idl=idel(t80)+1
            write(t80(idl:),100) pom1*pom2,pom2,pom1
            k=0
            do j=1,maxNDim+3
              call kus(t80,k,Cislo)
              if(j.eq.1) then
                p80=Tabulator//Cislo
              else
                p80=p80(:idel(p80))//Tabulator//Cislo
              endif
            enddo
            Ninfo=Ninfo+1
            TextInfo(NInfo)=p80
          enddo
          m=min(2*(mxline-Line),NExt500(n))
          ks=0
2000      k=ks
          if(mod(m,2).eq.1) m=m+1
          do i=1,m
            ip=mod(i-1,2)+1
            ks=ks+1
            if(ks.gt.NExt500(n)) go to 2300
            if(ip.eq.1) then
              k=k+1
              kk=OrderExtRef(k,n)
            else
              kk=OrderExtRef(k+m/2,n)
            endif
            write(ta80(ip),EM9Form2)(IHExt(j,kk,n),j=1,maxNDim)
            pom1=-float(ria(kk,n))*.0001
            pom2= float(rsa(kk,n))*.01
            write(ta80(ip)(idl:),100) pom1*pom2,pom2,pom1
2300        if(ip.eq.2) then
              call newln(1)
              write(lst,FormA1)(ta80(1)(j:j),j=1,idlp),(' ',j=1,10),
     1                         (ta80(2)(j:j),j=1,idlp)
              ta80(2)=' '
            endif
          enddo
          if(ks.lt.NExt500(n)) then
            m=min(2*(mxline-3),NExt500(n)-ks)
            go to 2000
          endif
        endif
        if(n.eq.1) then
          t80='Summary of systematic extinctions'
        else
          t80='Summary of rejected reflections'
        endif
        call FeInfoOut(-1.,-1.,t80,'L')
      endif
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(1x,3f9.1)
      end
