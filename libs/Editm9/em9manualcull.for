      subroutine EM9ManualCull
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension xp(3),xo(3),xsel(5),ysel(5),RFac(5),xoc(3),xpc(3)
      character*256 Veta,SavedM40,EdwStringQuest
      character*80  SvFile,t80
      character*60  format83a
      character*17 :: Labels(20) =
     1              (/'%Quit            ',
     2                'X=I(Min)         ',
     3                'X=I(ave)         ',
     4                'X=I(max)         ',
     5                'X=theta          ',
     6                'X-uniformly      ',
     7                'Un%zoom          ',
     8                'Run %average     ',
     9                'A%verage report  ',
     a                '%Edit atoms      ',
     1                'Edit m%40 file   ',
     2                'R%un refine      ',
     3                'Refine re%port   ',
     4                'Refine %commands ',
     5                'Run %Fourier     ',
     6                'Fourier co%mmands',
     7                'Run Co%ntour     ',
     8                '%Reset culled    ',
     9                'Satellite fil%ter',
     a                '%Options         '/)
      character*50 :: Menu(6) =
     1           (/'%Escape                                        ',
     2             'Make %zoom                                     ',
     3             'Start culling %dialog                          ',
     4             'Cull %minimal unculled item for each reflection',
     5             'Cull ma%ximal unculled item for each reflection',
     6             'Cull all items - delete reflections            '/)
      integer CullQuest,Color,ColorGasket,ColorDiamond,FeRGBCompress,
     1        CoDal,FeMenu,XUseOld,YUseOld,EM9SortIndex,HSearch(6)
      integer, allocatable :: ICullArIn(:),ICullArOld(:),Ave2FoInd(:),
     1                        OrderAve(:),KrFlagAve(:)
      logical :: FeYesNo,EqIV,ExistFile,Diff,FileDiff,RefineEnd,
     1           XLinearOld
      real IoPom,IcPom,IsPom,ICalcP
      real, allocatable :: ThetaArr(:)
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          KRefBlock=i
          exit
        endif
      enddo
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      SavedM40='jm40'
      call CreateTmpFile(SavedM40,i,0)
      call FeTmpFilesAdd(SavedM40)
      call EM9ManualFilter(0)
      call EM9ManualCullOptions(0)
      call FeRGBUncompress(Magenta,IRed,IGreen,IBlue)
      IRed=IRed*.2
      IGreen=IGreen*.2
      IBlue=IBlue*.2
      ColorGasket=FeRGBCompress(IRed,IGreen,IBlue)
      call FeRGBUncompress(Cyan,IRed,IGreen,IBlue)
      IRed=IRed*.2
      IGreen=IGreen*.2
      IBlue=IBlue*.2
      ColorDiamond=FeRGBCompress(IRed,IGreen,IBlue)
      NAve=0
      NAveRed=0
      NAveMax=0
      NAveRedMax=0
      NAveRed=0
      NRefRead=0
      if(allocated(HCondAveAr))
     1  deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     2             Ave2Org,RunCCDAr)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      call DRSetRefArrays(1)
      if(NRefRead.le.0) then
        call FeChybne(-1.,-1.,'No reflection read from your '//
     1                'repository file.',
     2                'The file does not contain symmetry related '//
     3                'reflections.',SeriousError)
        go to 9999
      endif
      allocate(ICullArIn(NRefRead),ICullArOld(NRefRead))
      call CopyVekI(ICullAr,ICullArIn,NRefRead)
      FLimCullIn=FLimCull(KDatBlock)
      CullQuest=NextQuestId()
      CheckMouse=.true.
      call FeQuestAbsCreate(CullQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,120.,2.*YBottomMargin,24.)
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=100.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      k=1
      ik=20
      do i=1,ik
        call FeQuestAbsButtonMake(CullQuest,xpom,ypom,wpom,ButYd,
     1                            Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtIMin=ButtonLastMade
          nButtFobs=ButtonLastMade
        else if(i.eq.3) then
          nButtIAve=ButtonLastMade
        else if(i.eq.4) then
          nButtIMax=ButtonLastMade
        else if(i.eq.5) then
          nButtTheta=ButtonLastMade
        else if(i.eq.6) then
          nButtXScale=ButtonLastMade
          XLinear=.true.
          XLinearOld=.true.
        else if(i.eq.7) then
          nButtUnzoom=ButtonLastMade
        else if(i.eq.8) then
          nButtAverage=ButtonLastMade
        else if(i.eq.9) then
          nButtAverageReport=ButtonLastMade
        else if(i.eq.10) then
          nButtEditAtoms=ButtonLastMade
        else if(i.eq.11) then
          nButtEditM40=ButtonLastMade
        else if(i.eq.12) then
          nButtRefine=ButtonLastMade
        else if(i.eq.13) then
          nButtRefineReport=ButtonLastMade
        else if(i.eq.14) then
          nButtRefineCommands=ButtonLastMade
        else if(i.eq.15) then
          nButtFourier=ButtonLastMade
        else if(i.eq.16) then
          nButtFourierCommands=ButtonLastMade
        else if(i.eq.17) then
          nButtContour=ButtonLastMade
        else if(i.eq.18) then
          nButtResetCulled=ButtonLastMade
        else if(i.eq.19) then
          nButtFilter=ButtonLastMade
        else if(i.eq.20) then
          nButtOptions=ButtonLastMade
        endif
        if(i.eq.19.and.NDimI(KPhase).le.0) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.1.or.i.eq.7.or.i.eq.9.or.i.eq.17.or.i.eq.20) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          ypom=ypom+2.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+10.
      Veta='Search reflection'
      tpom=(XMaxBasWin+XMaxGrWin)*.5
      call FeQuestAbsLblMake(CullQuest,tpom,ypom,Veta,'C','N')
      ypom=ypom-25.
      call FeQuestAbsEdwMake(CullQuest,tpom,ypom,xpom,ypom,' ','C',wpom,
     1                       EdwYd,1)
      call FeQuestIntAEdwOpen(EdwLastMade,HSearch,NDim(KPhase),.true.)
      nEdwSearch=EdwLastMade
      Veta=' '
      do i=1,7
        if(i.eq.1.or.i.eq.5) then
          xpom=10.
          if(i.eq.1) then
            ypom=YBottomMargin+YBottomText-5.
          else
            ypom=YBottomText+5.
          endif
        else
          xpom=xpom+125.
          if(i.ne.4) xpom=xpom+65.
        endif
        call FeQuestAbsLblMake(CullQuest,xpom,ypom,Veta,'L','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) then
          nLblRint=LblLastMade
        else if(i.eq.2) then
          nLblPocet=LblLastMade
        else if(i.eq.3) then
          nLblRedundancy=LblLastMade
        else if(i.eq.4) then
          nLblCulled=LblLastMade
        else if(i.eq.5) then
          nLblRF=LblLastMade
        else if(i.eq.6) then
          nLblRwF=LblLastMade
        else if(i.eq.7) then
          nLblGOF=LblLastMade
        endif
      enddo
      XUseOld=-1
      YUseOld=-1
      ICullArOld(:)=-1
      FLimCullOld=FLimCull(KDatBlock)
1100  if(.not.EqIV(ICullAr,ICullArOld,NRefRead).or.
     1   FLimCull(KDatBlock).ne.FLimCullOld) then
        if(FLimCull(KDatBlock).ne.FLimCullOld) then
          call OpenFile(lst,fln(:ifln)//'.ave','formatted','unknown')
          LstOpened=.true.
          Uloha='From manual culling'
          call NewPg(1)
          call DRAverage(AveFromManualCull,1,ich)
          call CloseListing
        endif
        NInfo=2
        TextInfo(1)=' '
        TextInfo(2)=' '
        if(NRefBlockAr.gt.1.and.DiffScales(1).eq.1.and.
     1     EM9ScaleLim(KDatBlock).gt.0.) then
          call DRMakeScales(1,1,ich)
          do i=2,MaxRefBlockAr
            call DRMakeScales(i,2,ich)
            if(ich.ne.0) go to 1110
          enddo
          call DRMakeScales(1,3,ich)
        endif
1110    call OpenFile(lst,fln(:ifln)//'.ave','formatted','unknown')
        LstOpened=.true.
        Uloha='From manual culling'
        call NewPg(1)
        call DRAverage(AveFromManualCull,1,ich)
        call CloseListing
        call CopyVekI(ICullAr,ICullArOld,NRefRead)
        FLimCullOld=FLimCull(KDatBlock)
      endif
      if(YUse.eq.YUseIAve) NPocet=NAve
      RIMaxMin=-99999999.
      RIMaxMax=-99999999.
      RIMaxAve=-99999999.
      RIMinMin= 99999999.
      RIMinMax= 99999999.
      RIMinAve= 99999999.
      DMax=0.
      NAveKresli=0
      if(allocated(OrderAve)) deallocate(OrderAve,KrFlagAve)
      allocate(OrderAve(NAve),KrFlagAve(NAve))
      KrFlagAve=0
      do i=1,NAve
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(RIAveAr(i).le.ObsLevelCull*RSAveAr(i)) cycle
          else
            n=0
            do j=1,NAveRedAr(i)
              m=Ave2Org(j,i)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1120
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              cycle
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1120
            else
              cycle
            endif
          endif
        endif
1120    k=HCondAveAr(i)
        call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        RIMax=-99999999.
        RIMin= 99999999.
        nn=0
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).eq.1) cycle
          nn=nn+1
          RIMax=max(RIMax,RIAr(m))
          RIMin=min(RIMin,RIAr(m))
        enddo
        if(nn.le.0) cycle
        NAveKresli=NAveKresli+1
        KrFlagAve(i)=1
        RIMaxMin=max(RIMaxMin,RIMin)
        RIMaxMax=max(RIMaxMax,RIMax)
        RIMaxAve=max(RIMaxAve,RIAveAr(i))
        RIMinMin=min(RIMinMin,RIMin)
        RIMinMax=min(RIMinMax,RIMax)
        RIMinAve=min(RIMinAve,RIAveAr(i))
        DMax=max(DMax,DiffAveAr(i))
      enddo
      call Indexx(NAve,HCondAveAr,OrderAve)
      Veta=fln(:ifln)//'.m83'
      if(ExistFile(Veta)) then
        if(allocated(Ave2FoInd)) deallocate(Ave2FoInd)
        allocate(Ave2FoInd(NAve))
        Ave2FoInd=-999.
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 1170
        endif
        read(ln,FormA) Veta
        i=LocateSubstring(Veta,'e',.false.,.true.)
        if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
          format83a=format83e
        else
          format83a=format83
        endif
        rewind ln
        NRefWdF=0
        FoMax=0.
        FcMax=0.
        wdFMax=0.
        NRefWdFMax=0
        call ReallocateWdFAr(NRefRead)
        go to 1150
1140    NRefWdF=NRefWdF-1
1150    read(ln,Format83a,end=1170,err=1170)(ih(i),i=1,maxNDim),IoPom,
     1    IcPom,IsPom,Cislo,itw,wdy,FoPom,FcPom,FsPom
        if(itw.le.0) go to 1150
        if(NRefWdF.ge.NRefWdFMax)
     1    call ReallocateWdFAr(NRefWdFMax+1000)
        NRefWdF=NRefWdF+1
        HCondWdFAr(NRefWdF)=IndPack(ih,HCondLn,HCondMx,NDim(KPhase))
        FoAr(NRefWdF)=FoPom
        FcAr(NRefWdF)=FcPom
        FsAr(NRefWdF)=FsPom
        IcAr(NRefWdF)=FcPom**2
        WdFAr(NRefWdF)=wdy
        j=0
        do i=1,NSymm(KPhase)
          call MultMIRI(ih,rm6(1,i,1,KPhase),ihp,1,NDim(KPhase),
     1                  NDim(KPhase))
          k=IndPack(ihp,HCondLn,HCondMx,NDim(KPhase))
          j=LocateInIntArray(k,HCondAveAr,NAve)
          if(j.gt.0) exit
        enddo
        if(j.le.0) go to 1140
        FoInd2Ave(NRefWdF)=j
        Ave2FoInd(j)=NRefWdF
        i=j
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(FoAr(NRefWdF).le.2.*ObsLevelCull*FsAr(i)) go to 1140
          else
            n=0
            do j=1,NAveRedAr(i)
              m=Ave2Org(j,i)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1160
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              go to 1160
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3               n.eq.1)) then
              go to 1160
            else
              go to 1140
            endif
          endif
        endif
1160    k=HCondAveAr(i)
        call IndUnPack(k,ih,HCondLn,HCondMx,maxNDim)
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) go to 1140
        endif
        FoMax=max(FoMax,FoPom)
        FcMax=max(FcMax,FcPom)
        wdFMax=max(wdFMax,abs(wdy))
        go to 1150
1170    call CloseIfOpened(ln)
        if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) NPocet=NRefWdF
      endif
      ThetaMin=0.
      ThetaMax=0.
      if(allocated(ThetaArr)) deallocate(ThetaArr)
      allocate(ThetaArr(NPocet))
      do i=1,NPocet
        if(YUse.eq.YUseIAve) then
          IAve=i
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          IAve=FoInd2Ave(i)
          IFo=i
        endif
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(YUse.eq.YUseIAve) then
              if(RIAveAr(IAve).le.ObsLevelCull*RSAveAr(IAve)) cycle
            else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
              if(FoAr(IFo).le.2.*ObsLevelCull*FsAr(IFo)) cycle
            endif
          else
            n=0
            do j=1,NAveRedAr(IAve)
              m=Ave2Org(j,IAve)
              if(ICullAr(m).ne.0.and.DisplaySelect.eq.DisplayCulled)
     1          then
                go to 1180
              else if(ICullAr(m).eq.0) then
                n=n+1
              endif
            enddo
            if(DisplaySelect.eq.DisplayCulled) then
              cycle
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1180
            else
              cycle
            endif
          endif
        endif
1180    if(YUse.eq.YUseIAve) then
          k=HCondAveAr(IAve)
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          k=HCondWdFAr(IFo)
        endif
        call IndUnPack(k,ih,HCondLn,HCondMx,NDim(KPhase))
        call FromIndSinthl(ih,xo,sinthl,sinthlq,1,0)
        pom=min(sinthl*LamAveRefBlock(KRefBlock),.999999)
        ThetaArr(i)=asin(pom)/ToRad
        ThetaMax=max(ThetaMax,ThetaArr(i))
      enddo
      if(YUse.eq.YUseFcalc) then
        if(.not.XLinear) then
          Veta='X-linear scale'
          call FeQuestButtonLabelChange(nButtXScale,Veta)
          XLinear=.true.
          XLinearOld=XLinear
        endif
        call FeQuestButtonDisable(nButtXScale)
      else
        call FeQuestButtonOff(nButtXScale)
      endif
1200  if(XLinear.neqv.XLinearOld) then
        Zoomed=.false.
        if(XLinear) then
          Veta='X-uniformly'
        else
          Veta='X-linear scale'
        endif
        call FeQuestButtonLabelChange(nButtXScale,Veta)
        XLinearOld=XLinear
      endif
      if((YUse.eq.YUseIAve.and.NAveKresli.le.0).or.
     1   ((YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc).and.NRefWdF.le.0)) then
        call FeClearGrWin
        NInfo=1
        TextInfo(1)='No reflections fulfil the selection '//
     1              'criteria.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      endif
      if(XUse.ne.XUseOld.or.YUse.ne.YUseOld) Zoomed=.false.
      if(YUse.eq.YUseIAve) then
        yomn=0.
        yomx=DMax
        if(XLinear) then
          if(XUse.eq.XUseIMin) then
            xomn=RIMinMin
            xomx=RIMaxMin
            Veta='I(min)'
          else if(XUse.eq.XUseIAve) then
            xomn=RIMinAve
            xomx=RIMaxAve
            Veta='I(ave)'
          else if(XUse.eq.XUseIMax) then
            xomn=RIMinMax
            xomx=RIMaxMax
            Veta='I(max)'
          endif
        else
          xomn=0.
          xomx=1.
          Veta='n'
        endif
      else if(YUse.eq.YUseWdF) then
        yomn=-wdFMax
        yomx= wdFMax
        if(XLinear) then
          Veta='F(obs)'
          if(XUse.eq.XUseFobs) then
            xomn=0.
            xomx=FoMax
          endif
        else
          xomn=0.
          xomx=1.
          Veta='n'
        endif
      else if(YUse.eq.YUseFcalc) then
        yomn=0
        yomx=max(FoMax,FcMax)
        Veta='F(obs)'
        xomn=0.
        xomx=yomx
      endif
      if(XUse.eq.XUseTheta) then
        if(XLinear) then
          xomn=ThetaMin
          xomx=ThetaMax
          Veta='Theta'
        else
          xomn=0.
          xomx=1.
          Veta='n'
        endif
      endif
      if(yomx.le.0.) yomx=1.
      xomx=xomx*1.01
      yomn=yomn*1.01
      yomx=yomx*1.01
      if(Zoomed) then
        xomn=xomnz
        xomx=xomxz
        yomn=yomnz
        yomx=yomxz
      endif
      call FeClearGrWin
      call UnitMat(F2O,3)
      call UnitMat(O2F,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
      if(YUse.eq.YUseIAve) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Diff')
      else if(YUse.eq.YUseWdF) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'WdF')
      else if(YUse.eq.YUseFcalc) then
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'F(calc)')
      endif
      xp(3)=0.
      if(.not.Zoomed.and.YUse.eq.YUseFcalc.and.XUse.eq.XUseFobs) then
        xp(1)=xomn
        xp(2)=yomn
        call FeXf2X(xp,xo)
        xu(1)=xo(1)
        yu(1)=xo(2)
        xp(1)=xomx
        xp(2)=yomx
        call FeXf2X(xp,xo)
        xu(2)=xo(1)
        yu(2)=xo(2)
        call FePolyLine(2,xu,yu,Red)
      endif
      if(XUse.eq.XUseTheta) then
        if(DiamondUsed.gt.0) then
          do i=1,22
            if(DiamondTheta(i).le.0..or.DiamondTheta(i).gt.ThetaMax)
     1        exit
            xp(1)=max(DiamondTheta(i)-DiamondDeltaTheta,1.)
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)+1.
            xp(2)=yomx
            call FeXf2X(xp,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)-1.
            xp(1)=min(DiamondTheta(i)+DiamondDeltaTheta,ThetaMax-1.)
            call FeXf2X(xp,xo)
            xu(3)=xo(1)
            yu(3)=xo(2)-1.
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(4)=xo(1)
            yu(4)=xo(2)+1.
            call FeFillRectangle(xu(1),xu(4),yu(1),yu(2),4,0,0,
     1                           ColorDiamond)
            call FePolyLine(2,xu,yu,ColorDiamond)
          enddo
        endif
        if(GasketUsed.gt.0) then
          do i=1,22
            if(GasketTheta(i).le.0..or.GasketTheta(i).gt.ThetaMax) exit
            xp(1)=max(GasketTheta(i)-GasketDeltaTheta,1.)
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)+1.
            xp(2)=yomx
            call FeXf2X(xp,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)-1.
            xp(1)=min(GasketTheta(i)+GasketDeltaTheta,ThetaMax-1.)
            call FeXf2X(xp,xo)
            xu(3)=xo(1)
            yu(3)=xo(2)-1.
            xp(2)=yomn
            call FeXf2X(xp,xo)
            xu(4)=xo(1)
            yu(4)=xo(2)+1.
            call FeFillRectangle(xu(1),xu(4),yu(1),yu(2),4,0,0,
     1                           ColorGasket)
            call FePolyLine(2,xu,yu,ColorGasket)
          enddo
        endif
      endif
      xp(3)=0.
      NCullAuto=0
      NCullManual=0
      do i=1,NAve
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).ne.0) then
            if(ICullAr(m).eq.1) then
              NCullManual=NCullManual+1
            else if(ICullAr(m).eq.2) then
              NCullAuto=NCullAuto+1
            endif
          endif
        enddo
      enddo
      if(.not.XLinear) then
        if(YUse.eq.YUseIAve.or.YUse.eq.YUseWdF) then
          if(allocated(OrderX)) deallocate(OrderX)
          allocate(OrderX(NPocet))
          if(XUse.eq.XUseTheta) then
            call RIndexx(NPocet,ThetaArr,OrderX)
          else
            if(YUse.eq.YUseIAve) then
              call RIndexx(NPocet,RIAveAr,OrderX)
            else
              call RIndexx(NPocet,FoAr,OrderX)
            endif
          endif
        endif
      endif
      do i=1,NPocet
        if(YUse.eq.YUseIAve.or.YUse.eq.YUseWdF) then
          if(XLinear) then
            if(YUse.eq.YUseIAve) then
              IAve=i
              ix=i
            else
              IFo=i
              IAve=FoInd2Ave(i)
              ix=i
            endif
          else
            if(YUse.eq.YUseIAve) then
              IAve=OrderX(i)
              ix=i
            else
              IFo=OrderX(i)
              IAve=FoInd2Ave(IFo)
              ix=i
            endif
          endif
        else if(YUse.eq.YUseFcalc) then
          IAve=FoInd2Ave(i)
          IFo=i
          ix=i
        endif
        n=0
        nc=0
        if(IAve.ne.0) then
          do j=1,NAveRedAr(IAve)
            m=Ave2Org(j,IAve)
            if(ICullAr(m).ne.0) then
              nc=nc+1
            else if(ICullAr(m).eq.0) then
              n=n+1
            endif
          enddo
        endif
        if(DisplaySelect.ne.DisplayAll) then
          if(DisplaySelect.eq.DisplayObserved) then
            if(YUse.eq.YUseIAve) then
              if(RIAveAr(IAve).le.ObsLevelCull*RSAveAr(IAve)) cycle
            else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
              if(FoAr(IFo).le.2.*ObsLevelCull*FsAr(IFo)) cycle
            endif
          else if(IAve.gt.0) then
            if(DisplaySelect.eq.DisplayCulled) then
              if(nc.gt.0) then
                go to 1250
              else
                cycle
              endif
            else if((DisplaySelect.eq.DisplayMeasuredTwice.and.
     1               n.eq.2).or.
     2              (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                n.eq.1)) then
              go to 1250
            else
              cycle
            endif
          endif
        endif
1250    if(YUse.eq.YUseIAve) then
          k=HCondAveAr(IAve)
          Theta=ThetaArr(IAve)
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          k=HCondWdFAr(IFo)
          Theta=ThetaArr(IFo)
        endif
        call IndUnPack(k,ih,HCondLn,HCondMx,NDim(KPhase))
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        if(maxNDim.gt.3.and.ReflUse.ne.ReflUseAll) then
          mm=EM9SortIndex(ih)-2
          if((ReflUse.eq.ReflUseMain.and.mm.ne.0).or.
     1       (ReflUse.eq.ReflUseAllSat.and.mm.eq.0).or.
     2       (ReflUse.gt.0.and.mm.ne.ReflUse)) cycle
        endif
        Color=White
        if(XUse.eq.XUseTheta) then
          if(XLinear) then
            xp(1)=Theta
          else
            xp(1)=float(ix)/float(NPocet)
          endif
        else
          if(YUse.eq.YUseIAve) then
            if(XLinear) then
              RIMax=-99999999.
              RIMin= 99999999.
              do j=1,NAveRedAr(IAve)
                m=Ave2Org(j,IAve)
                if(ICullAr(m).eq.1) cycle
                RIMax=max(RIMax,RIAr(m))
                RIMin=min(RIMin,RIAr(m))
              enddo
              if(XUse.eq.XUseIMin) then
                xp(1)=RIMin
              else if(XUse.eq.XUseIAve) then
                xp(1)=RIAveAr(IAve)
              else if(XUse.eq.XUseIMax) then
                xp(1)=RIMax
              endif
            else
              xp(1)=float(ix)/Float(NPocet)
            endif
          else if(YUse.eq.YUseWdF) then
            if(XLinear) then
              xp(1)=FoAr(ix)
            else
              xp(1)=float(ix)/Float(NPocet)
            endif
          else if(YUse.eq.YUseFcalc) then
            xp(1)=FoAr(ix)
          endif
        endif
        if(YUse.eq.YUseIAve) then
          xp(2)=DiffAveAr(IAve)
        else if(YUse.eq.YUseWdF) then
          xp(2)=WdFAr(IFo)
        else
          xp(2)=FcAr(IFo)
        endif
        if(DiamondUsed.gt.0) then
          do j=1,22
            if(Theta.ge.DiamondTheta(j)-DiamondDeltaTheta.and.
     1         Theta.le.DiamondTheta(j)+DiamondDeltaTheta) then
              Color=Cyan
              exit
            endif
          enddo
        endif
        if(GasketUsed.gt.0) then
          do j=1,22
            if(Theta.ge.GasketTheta(j)-GasketDeltaTheta.and.
     1         Theta.le.GasketTheta(j)+GasketDeltaTheta) then
              Color=Magenta
              exit
             endif
          enddo
        endif
        if(xp(1).lt.xomn.or.xp(1).gt.xomx.or.
     1     xp(2).lt.yomn.or.xp(2).gt.yomx) cycle
        call FeXf2X(xp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,Color)
      enddo
1300  NAveObs=0
      do i=1,NAve
        if(RIAveAr(i).gt.EM9ObsLim(KDatBlock)*RSAveAr(i))
     1    NAveObs=NAveObs+1
      enddo
      if(rden(1).gt.0.) rave=rnum(1)/rden(1)*100.
      if(roden(1).gt.0.) then
        write(Cislo,'(f6.2,''/'',f6.2)') ronum(1)/roden(1)*100.,
     1                                   rave
      else
        if(rden(1).gt.0.) then
          write(Cislo,'(''-----/'',f6.2)') rave
        else
          write(Cislo,'(''-----/-----'')')
        endif
      endif
      call Zhusti(Cislo)
      write(Veta,'(''Rint(obs)/Rint(all) : '',a)') Cislo
      call FeQuestLblChange(nLblRint,Veta)
      write(t80,100) NAveObs,NAve-NTotallyCulled
      call Zhusti(t80)
      Veta='N(obs)/N(all) : '//t80(:idel(t80))
      call FeQuestLblChange(nLblPocet,Veta)
      write(Cislo,'(f10.3)') float(NRefRead)/float(NAve)
      call ZdrcniCisla(Cislo,1)
      Veta='Redundancy : '//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRedundancy,Veta)
      write(t80,100) NCullManual,NCullAuto
      call Zhusti(t80)
      Veta='Culled(man)/Culled(auto) : '//t80(:idel(t80))
      call FeQuestLblChange(nLblCulled,Veta)
      call SetRealArrayTo(RFac,5,0.)
      Veta=fln(:ifln)//'.m70'
      if(ExistFile(Veta)) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1400
1320    read(ln,FormA,end=1340) Veta
        m=23
        n=15
        do i=1,5
          j=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                      .false.,.true.)
          if(j.gt.0) then
            k=j+idel(CIFKey(m,n))
            call StToReal(Veta,k,RFac(i),1,.false.,ich)
            if(ich.ne.0) RFac(i)=99.99
            if(i.ne.5) RFac(i)=RFac(i)*100.
            go to 1320
          endif
          if(i.eq.1) then
            m=22
          else if(i.eq.2) then
            m=38
          else if(i.eq.3) then
            m=37
          else if(i.eq.4) then
            m=12
          endif
        enddo
        go to 1320
1340    call CloseIfOpened(ln)
      endif
1400  Veta='R(obs)/R(all) : '
      if(RFac(1).gt.0.) then
        write(Cislo,101) RFac(1)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta='R(obs)/R(all) : '//Cislo(:idel(Cislo))//'/'
      if(RFac(2).gt.0.) then
        write(Cislo,101) RFac(2)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRF,Veta)
      if(RFac(3).gt.0.) then
        write(Cislo,101) RFac(3)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta='Rw(obs)/Rw(all) : '//Cislo(:idel(Cislo))//'/'
      if(RFac(4).gt.0.) then
        write(Cislo,101) RFac(4)
      else
        Cislo='---'
      endif
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblRwF,Veta)
      write(Cislo,'(f15.3)') RFac(5)
      call Zhusti(Cislo)
      Veta='GOF : '//Cislo(:idel(Cislo))
      call FeQuestLblChange(nLblGOF,Veta)
      if(YUse.eq.YUseIAve) then
        Veta='versus |I(i)-I(ave)|/sig(I(ave))'
      else if(YUse.eq.YUseWdF) then
        Veta='versus w.|F(obs)-F(calc)|'
      else if(YUse.eq.YUseFcalc) then
        Veta='versus F(calc)'
      endif
      if(XUse.eq.XUseTheta) then
        Veta='Theta '//Veta(:idel(Veta))
      else
        if(YUse.eq.YUseIAve) then
          if(XUse.eq.XUseIMin) then
            Veta='I(min) '//Veta(:idel(Veta))
          else if(XUse.eq.XUseIAve) then
            Veta='I(ave) '//Veta(:idel(Veta))
          else if(XUse.eq.XUseIMax) then
            Veta='I(max) '//Veta(:idel(Veta))
          endif
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          Veta='F(obs) '//Veta(:idel(Veta))
        endif
      endif
      call FeHeaderInfo(Veta)
      AllowChangeMouse=.false.
      CheckMouse=.true.
1500  k=0
      CulledExist=.false.
      TwiceMeasuredExist=.false.
      OnceMeasuredExist=.false.
      CompletelyExcludedExist=.false.
      do i=1,NAve
        n=0
        do j=1,NAveRedAr(i)
          m=Ave2Org(j,i)
          if(ICullAr(m).ne.0.and..not.CulledExist) then
            CulledExist=.true.
            k=k+1
          endif
          if(ICullAr(m).eq.0) n=n+1
        enddo
        if(n.eq.0) then
          if(.not.CompletelyExcludedExist) then
            CompletelyExcludedExist=.true.
            k=k+1
          endif
        else if(n.eq.1) then
          if(.not.OnceMeasuredExist) then
            OnceMeasuredExist=.true.
            k=k+1
          endif
        else if(n.eq.2) then
          if(.not.TwiceMeasuredExist) then
            TwiceMeasuredExist=.true.
            k=k+1
          endif
        endif
        if(k.ge.4) exit
      enddo
      if(YUse.eq.YUseIAve) then
        call FeQuestButtonLabelChange(nButtIMin,Labels(2))
        call FeQuestButtonOff(nButtIAve)
        call FeQuestButtonOff(nButtIMax)
        call FeQuestButtonOff(nButtTheta)
      else
        call FeQuestButtonDisable(nButtIAve)
        call FeQuestButtonDisable(nButtIMax)
        if(YUse.eq.YUseFcalc) then
          call FeQuestButtonDisable(nButtTheta)
        else
          call FeQuestButtonOff(nButtTheta)
        endif
        call FeQuestButtonLabelChange(nButtIMin,'X=F(obs)')
      endif
      if(Zoomed) then
        call FeQuestButtonOff(nButtUnzoom)
      else
        call FeQuestButtonDisable(nButtUnzoom)
      endif
      XUseOld=XUse
      YUseOld=YUse
      call FeQuestEvent(CullQuest,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          call FeQuestRemove(CullQuest)
          go to 5000
        else if(CheckNumber.eq.nButtIMin) then
          XUse=XUseIMin
          go to 1200
        else if(CheckNumber.eq.nButtIAve) then
          XUse=XUseIAve
          go to 1200
        else if(CheckNumber.eq.nButtIMax) then
          XUse=XUseIMax
          go to 1200
        else if(CheckNumber.eq.nButtTheta) then
          XUse=XUseTheta
          go to 1200
        else if(CheckNumber.eq.nButtXScale) then
          XLinear=.not.XLinear
          go to 1200
        else if(CheckNumber.eq.nButtUnzoom) then
          Zoomed=.false.
          go to 1200
        else if(CheckNumber.eq.nButtAverage) then
          go to 1100
        else if(CheckNumber.eq.nButtAverageReport) then
          call FeListView(fln(:ifln)//'.ave',0)
          go to 1500
        else if(CheckNumber.eq.nButtEditAtoms) then
          call CopyFile(fln(:ifln)//'.m40',SavedM40)
          call EM40Atoms(ich)
          call iom40(1,0,fln(:ifln)//'.m40')
          Diff=FileDiff(fln(:ifln)//'.m40',SavedM40)
          if(ich.ne.0) then
            if(Diff) then
              if(FeYesNo(-1.,-1.,'Do you want to discard made '//
     1                   'changes?',0))
     2          call CopyFile(SavedM40,fln(:ifln)//'.m40')
            endif
          else
            if(Diff) then
              if(.not.FeYesNo(-1.,-1.,'Do you want to rewrite changed'//
     1                        ' files?',1))
     2          call CopyFile(SavedM40,fln(:ifln)//'.m40')
            endif
          endif
          call iom40(0,0,fln(:ifln)//'.m40')
          go to 1500
        else if(CheckNumber.eq.nButtEditM40) then
          call FeEdit(fln(:ifln)//'.m40')
          call iom40(0,0,fln(:ifln)//'.m40')
          call iom40(1,0,fln(:ifln)//'.m40')
          go to 1500
        else if(CheckNumber.eq.nButtRefine.or.
     1          CheckNumber.eq.nButtFourier.or.
     2          CheckNumber.eq.nButtContour) then
          Uloha='From manual culling'
          call NewPg(1)
          if(CheckNumber.eq.nButtRefine) then
            call EM9ManualCullUpdate(0)
            call NewPg(1)
            call Refine(0,RefineEnd)
          else if(CheckNumber.eq.nButtFourier) then
            call Fourier
          else if(CheckNumber.eq.nButtContour) then
            SvFile='jmcl'
            if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
            call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,SvFile)
            call Contour
            call FeMakeGrWin(0.,120.,2.*YBottomMargin,24.)
            call FeMakeAcWin(60.,20.,30.,30.)
            call FeBottomInfo('#prazdno#')
            call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,SvFile,0)
          endif
          call UpdateSummary
          if(CheckNumber.eq.nButtRefine) call DRSetRefArrays(1)
          if(YUse.eq.YUseIAve) then
            go to 1300
          else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
            go to 1100
          endif
        else if(CheckNumber.eq.nButtRefineReport) then
          call FeListView(fln(:ifln)//'.ref',0)
          go to 1500
        else if(CheckNumber.eq.nButtRefineCommands.or.
     1          CheckNumber.eq.nButtFourierCommands) then
          if(CheckNumber.eq.nButtRefineCommands) then
            call RefSetCommands
          else
            call FouSetCommands(ichp)
          endif
          if(StartProgram) then
            Uloha='From manual culling'
            if(CheckNumber.eq.nButtRefine) call EM9ManualCullUpdate(0)
            call NewPg(1)
            if(CheckNumber.eq.nButtRefineCommands) then
              call Refine(0,RefineEnd)
            else
              call Fourier
            endif
            call UpdateSummary
            if(CheckNumber.eq.nButtRefine) call DRSetRefArrays(1)
          endif
          if(YUse.eq.YUseIAve) then
            go to 1300
          else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
            go to 1100
          endif
        else if(CheckNumber.eq.nButtResetCulled) then
          do i=1,NRefRead
            if(ICullAr(i).ne.2) ICullAr(i)=0
          enddo
          go to 1100
        else if(CheckNumber.eq.nButtFilter) then
          call EM9ManualFilter(1)
          go to 1100
        else if(CheckNumber.eq.nButtOptions) then
          call EM9ManualCullOptions(1)
          go to 1100
        endif
      else if(CheckType.eq.EventMouse) then
        if(CheckNumber.eq.JeLeftDown) then
          xsel(1)=XPos
          ysel(1)=YPos
          do i=1,5
            xsel(i)=XPos
            ysel(i)=YPos
          enddo
          xo(1)=FeX2Xo(XPos)
          xo(2)=FeY2Yo(YPOs)
          xo(3)=0.
          call multm(O2F,xo,xp,3,3,1)
          XMinP=xp(1)
          YMinP=xp(2)
          call FePlotMode('E')
          ip=2
          call FePolyLine(2,xsel,ysel,Green)
          TakeMouseMove=.true.
          if(DeferredOutput) call FeReleaseOutput
2200      call FeEvent(1)
          if(EventType.eq.0) go to 2200
2250      if(EventType.eq.EventMouse) then
            if(EventNumber.eq.JeMove) then
              call FePolyLine(ip,xsel,ysel,Green)
              ysel(2)=YPos
              xsel(3)=XPos
              ysel(3)=YPos
              xsel(4)=XPos
              if(xsel(2).ne.xsel(3)) then
                ip=5
              else
                ip=2
              endif
              call FePolyLine(ip,xsel,ysel,Green)
              go to 2200
            else if(EventNumber.eq.JeLeftUp) then
              TakeMouseMove=.false.
              xo(1)=FeX2Xo(xsel(3))
              xo(2)=FeY2Yo(ysel(3))
              xo(3)=0.
              call multm(O2F,xo,xp,3,3,1)
              XMin=min(XMinP,xp(1))
              YMin=min(YMinP,xp(2))
              XMax=max(XMinP,xp(1))
              YMax=max(YMinP,xp(2))
              DMin=999999.
              do mm=1,2
                if(mm.eq.1) then
                  Kolik=0
                else
                  Mame=0
                endif
                do i=1,NPocet
                  if(YUse.eq.YUseIAve) then
                    if(XLinear) then
                      IAve=i
                      ix=i
                    else
                      IAve=OrderX(i)
                      ix=i
                    endif
                    if(allocated(Ave2FoInd)) then
                      IFo=Ave2FoInd(IAve)
                    else
                      IFo=-999
                    endif
                  else if(YUse.eq.YUseWdF) then
                    if(XLinear) then
                      ix=i
                      IFo=i
                      IAve=FoInd2Ave(IFo)
                    else
                      ix=i
                      IFo=OrderX(i)
                      IAve=FoInd2Ave(IFo)
                    endif
                  else if(YUse.eq.YUseFcalc) then
                    ix=i
                    IAve=FoInd2Ave(i)
                    IFo=i
                  endif
                  if(DisplaySelect.ne.DisplayAll) then
                    if(DisplaySelect.eq.DisplayObserved) then
                      if(YUse.eq.YUseIAve) then
                        if(RIAveAr(IAve).le.ObsLevelCull*RSAveAr(IAve))
     1                    cycle
                      else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                        if(FoAr(IFo).le.2.*ObsLevelCull*FsAr(IFo)) cycle
                      endif
                    else
                      n=0
                      do j=1,NAveRedAr(IAve)
                        m=Ave2Org(j,IAve)
                        if(ICullAr(m).ne.0.and.
     1                     DisplaySelect.eq.DisplayCulled) then
                          go to 2500
                        else if(ICullAr(m).eq.0) then
                          n=n+1
                        endif
                      enddo
                      if(DisplaySelect.eq.DisplayCulled) then
                        cycle
                      else if((DisplaySelect.eq.DisplayMeasuredTwice
     1                         .and.n.eq.2).or.
     2                        (DisplaySelect.eq.DisplayMeasuredOnce.and.
     3                          n.eq.1)) then
                        go to 2500
                      else
                        cycle
                      endif
                    endif
                  endif
2500              if(YUse.eq.YUseWdF) then
                    xp(2)=WdFAr(IFo)
                  else if(YUse.eq.YUseFcalc) then
                    xp(2)=FcAr(i)
                  else
                    xp(2)=DiffAveAr(IAve)
                  endif
                  if(XUse.eq.XUseTheta) then
                    if(YUse.eq.YUseIAve.or.YUse.eq.YUseWdF) then
                      if(XLinear) then
                        xp(1)=ThetaArr(ix)
                      else
                        xp(1)=float(ix)/float(NPocet)
                      endif
                    else
                      xp(1)=ThetaArr(i)
                    endif
                  else if(YUse.eq.YUseWdF) then
                    if(XLinear) then
                      xp(1)=FoAr(ix)
                    else
                      xp(1)=float(ix)/float(NPocet)
                    endif
                  else if(YUse.eq.YUseFcalc) then
                    xp(1)=FoAr(ix)
                  else
                    RIMax=-999999.
                    RIMin= 999999.
                    do j=1,NAveRedAr(IAve)
                      m=Ave2Org(j,IAve)
                      if(ICullAr(m).eq.1) cycle
                      RIMax=max(RIMax,RIAr(m))
                      RIMin=min(RIMin,RIAr(m))
                    enddo
                    if(XLinear) then
                      if(XUse.eq.XUseIMin) then
                        xp(1)=RIMin
                      else if(XUse.eq.XUseIAve) then
                        xp(1)=RIAveAr(IAve)
                      else if(XUse.eq.XUseIMax) then
                        xp(1)=RIMax
                      endif
                    else
                      xp(1)=float(ix)/float(NPocet)
                    endif
                  endif
                  if(xp(1).lt.xomn.or.xp(1).gt.xomx.or.
     1               xp(2).lt.yomn.or.xp(2).gt.yomx) cycle
                  call FeXf2X(xp,xo)
                  if(mm.eq.1) then
                    xu(1)=(XMin+XMax)*.5
                    xu(2)=(YMin+YMax)*.5
                    xu(3)=0.
                    call FeXf2X(xu,yu)
                    pom=sqrt((xo(1)-yu(1))**2+(xo(2)-yu(2))**2)
                    if(pom.lt.DMin) then
                      DMin=min(DMin,pom)
                      call CopyVek(xo,xoc,3)
                      call CopyVek(xp,xpc,2)
                      IAveC=IAve
                      IFoC=IFo
                    endif
                  endif
                  if(xp(1).lt.XMin.or.xp(1).ge.XMax.or.
     1               xp(2).lt.YMin.or.xp(2).ge.YMax) then
                    cycle
                  endif
                  if(mm.eq.1) then
                    Kolik=Kolik+1
                    cycle
                  else
                    Mame=Mame+1
                  endif
                  if(Mame.eq.1) then
3000                call FeEvent(1)
                    if(EventType.eq.0) go to 3000
                    if(EventType.eq.EventMouse) then
                      if(EventNumber.eq.JeLeftUp) then
                        CoDal=1
                        call FePolyLine(ip,xsel,ysel,Green)
                        call FePlotMode('N')
                      else if(EventNumber.eq.JeRightDown) then
                        call FePlotMode('N')
                        CoDal=max(FeMenu(-1.,-1.,Menu,1,6,1,0)-1,0)
                        call FePlotMode('E')
                        call FePolyLine(ip,xsel,ysel,Green)
                        call FePlotMode('N')
                        if(CoDal.le.0) go to 1500
                      else
                        go to 3000
                      endif
                      if(CoDal.eq.1) then
                        Zoomed=.true.
                        xomnz=XMin
                        xomxz=XMax
                        yomnz=YMin
                        yomxz=YMax
                        go to 1100
                      endif
                    else
                      go to 3000
                    endif
                  endif
                  if(YUse.eq.YUseIAve) then
                    if(allocated(Ave2FoInd)) then
                      if(IFo.gt.0) then
                        ICalcP=IcAr(IFo)*ScSgTest/EM9ScToM90(KDatBlock)
                      else
                        ICalcP=-999.
                      endif
                    else
                      ICalcP=-999.
                    endif
                  else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                    if(IFo.gt.0) then
                      ICalcP=IcAr(IFo)*ScSgTest/EM9ScToM90(KDatBlock)
                    else
                      ICalcP=-999.
                    endif
                  endif
                  if(CoDal.eq.2) then
                    if(IAve.gt.0) then
                      call EM9ManualCullQuest(xo,IAve,ICalcP,
     1                                        Mame.eq.Kolik,Konec)
                    else
                      call FeChybne(-1.,-1.,'Nenasel ji.',' ',
     1                              SeriousError)
                      Konec=0
                    endif
                    if(Konec.eq.1) go to 1100
                  else
                    MinInt=0
                    MaxInt=0
                    RIMax=-99999999.
                    RIMin= 99999999.
                    do j=1,NAveRedAr(IAve)
                      m=Ave2Org(j,IAve)
                      if(ICullAr(m).eq.1) cycle
                      if(CoDal.eq.5) then
                        ICullAr(m)=1
                      else
                        if(RIAr(m).ge.RIMax) then
                          RIMax=RIAr(m)
                          MaxInt=m
                        endif
                        if(RIAr(m).le.RIMin) then
                          RIMin=RIAr(m)
                          MinInt=m
                        endif
                      endif
                    enddo
                    if(MinInt.eq.0.and.MaxInt.eq.0) cycle
                    if(CoDal.eq.3) then
                      ICullAr(MinInt)=1
                    else if(CoDal.eq.4) then
                      ICullAr(MaxInt)=1
                    endif
                  endif
                enddo
                if(mm.eq.2.and.Mame.ne.0) then
                  call FePolyLine(ip,xsel,ysel,Green)
                  call FePlotMode('N')
                  go to 1100
                endif
              enddo
              if(Kolik.eq.0.and.Mame.eq.0.and.DMin.lt.5.) then
                call FePolyLine(ip,xsel,ysel,Green)
                call FePlotMode('N')
                if(YUse.eq.YUseIAve) then
                  if(allocated(Ave2FoInd)) then
                    if(IFoC.gt.0) then
                      ICalcP=IcAr(IFoC)*ScSgTest/EM9ScToM90(KDatBlock)
                    else
                      ICalcP=-999.
                    endif
                  else
                    ICalcP=-999.
                  endif
                else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
                  if(IFoC.gt.0) then
                    ICalcP=IcAr(IFoC)*ScSgTest/EM9ScToM90(KDatBlock)
                  else
                    ICalcP=-999.
                  endif
                endif
                if(IAve.gt.0) then
                  call EM9ManualCullQuest(xoc,IAveC,ICalcP,.true.,Konec)
                else
                  call FeChybne(-1.,-1.,'Nenasel ji.',' ',
     1                          SeriousError)
                endif
              else
                call FePolyLine(ip,xsel,ysel,Green)
                call FePlotMode('N')
                go to 1500
              endif
              go to 1100
            else
              go to 2200
            endif
          else
            go to 2200
          endif
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSearch) then
        if(EdwStringQuest(nEdwSearch).eq.' ') go to 1500
        ich=0
        call FeQuestIntAFromEdw(nEdwSearch,HSearch)
3200    k=IndPack(HSearch,HCondLn,HCondMx,NDim(KPhase))
3210    ip=1
        ik=NAve
3220    if(ip+2.le.ik) then
          do i=ip,ik
            if(k.eq.HCondAveAr(OrderAve(i))) then
              ip=i
              ik=i
              go to 3230
            endif
          enddo
          ich=1
          go to 3250
        endif
        ipul=(ik+ip)/2
        kk=HCondAveAr(OrderAve(ipul))
        if(k.lt.kk) then
          ik=ipul
          go to 3220
        else if(k.gt.kk) then
          ip=ipul
          go to 3220
        else
          ip=ipul
          ik=ipul
        endif
3230    IAve=OrderAve(ip)
        if(KrFlagAve(IAve).le.0) then
          ich=3
          go to 3250
        endif
        if(YUse.eq.YUseIAve) then
          if(XLinear) then
            ix=IAve
          else
            do ix=1,NPocet
              if(OrderX(ix).eq.IAve) exit
            enddo
          endif
          if(allocated(Ave2FoInd)) then
            IFo=Ave2FoInd(IAve)
          else
            IFo=-999
          endif
        else if(YUse.eq.YUseWdF) then
          IFo=Ave2FoInd(IAve)
          if(XLinear) then
            ix=IFo
          else
            do ix=1,NPocet
              if(OrderX(ix).eq.IFo) exit
            enddo
          endif
        else if(YUse.eq.YUseFcalc) then
          IFo=Ave2FoInd(IAve)
          ix=IFo
        endif
        if(XUse.eq.XUseTheta) then
          if(YUse.eq.YUseIAve.or.YUse.eq.YUseWdF) then
            if(XLinear) then
              xp(1)=ThetaArr(ix)
            else
              xp(1)=float(ix)/float(NPocet)
            endif
          else
            xp(1)=ThetaArr(ix)
          endif
        else if(YUse.eq.YUseWdF) then
          if(XLinear) then
            xp(1)=FoAr(IFo)
          else
            xp(1)=float(ix)/float(NPocet)
          endif
        else if(YUse.eq.YUseFcalc) then
          xp(1)=FoAr(ix)
        else
          RIMax=-999999.
          RIMin= 999999.
          do j=1,NAveRedAr(IAve)
            m=Ave2Org(j,IAve)
            if(ICullAr(m).eq.1) cycle
            RIMax=max(RIMax,RIAr(m))
            RIMin=min(RIMin,RIAr(m))
          enddo
          if(XLinear) then
            if(XUse.eq.XUseIMin) then
              xp(1)=RIMin
            else if(XUse.eq.XUseIAve) then
              xp(1)=RIAveAr(IAve)
            else if(XUse.eq.XUseIMax) then
              xp(1)=RIMax
            endif
          else
            xp(1)=float(ix)/float(NPocet)
          endif
        endif
        if(YUse.eq.YUseWdF) then
          xp(2)=WdFAr(IFo)
        else if(YUse.eq.YUseFcalc) then
          xp(2)=FcAr(IFo)
        else
          xp(2)=DiffAveAr(IAve)
        endif
        if(xp(1).lt.xomn.or.xp(1).gt.xomx.or.
     1     xp(2).lt.yomn.or.xp(2).gt.yomx) then
          ich=2
          go to 3250
        endif
        call FeXf2X(xp,xo)
        if(YUse.eq.YUseIAve) then
          if(allocated(Ave2FoInd)) then
            if(IFo.gt.0) then
              ICalcP=IcAr(IFo)*ScSgTest/EM9ScToM90(KDatBlock)
            else
              ICalcP=-999.
            endif
          else
            ICalcP=-999.
          endif
        else if(YUse.eq.YUseWdF.or.YUse.eq.YUseFcalc) then
          if(IFo.gt.0) then
            ICalcP=IcAr(IFo)*ScSgTest/EM9ScToM90(KDatBlock)
          else
            ICalc=-999.
          endif
        endif
        call EM9ManualCullQuest(xo,IAve,ICalcP,.true.,Konec)
        go to 3500
3250    Veta='('
        do i=1,NDim(KPhase)
          write(Cislo,'(i5,'','')') HSearch(I)
          Veta=Veta(:idel(Veta))//Cislo
        enddo
        call Zhusti(Veta)
        i=idel(Veta)
        Veta(i:i)=')'
        if(ich.eq.1) then
          Veta='Reflection '//Veta(:idel(Veta))//' not found, try '//
     1         'again.'
        else if(ich.eq.2) then
          Veta='Reflection '//Veta(:idel(Veta))//' outside of the '//
     1         'selected area, try again.'
        else if(ich.eq.3) then
          Veta='Reflection '//Veta(:idel(Veta))//' filtered out, '//
     1         'try again.'
        endif
        ich=0
        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        go to 1100
3500    call FeQuestIntAEdwOpen(nEdwSearch,HSearch,NDim(KPhase),.true.)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 1500
5000  if(.not.EqIV(ICullAr,ICullArIn,NRefRead).or.
     1   FLimCull(KDatBlock).ne.FLimCullIn) then
        if(FeYesNo(-1.,-1.,'Do you want to accept the changes?',1))
     1    call EM9ManualCullUpdate(0)
      endif
9999  if(allocated(HCondAveAr)) then
        deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     1             Ave2Org)
        NAveMax=0
        NAveRedMax=0
        NAve=0
        NAveRed=0
      endif
      call DeleteFile(fln(:ifln)//'.ave')
      if(allocated(ICullArIn)) deallocate(ICullArIn,ICullArOld)
      if(allocated(HCondAr)) deallocate(HCondAr,riar,rsar)
      if(allocated(ICullAr)) deallocate(ICullAr,RefBlockAr,RunCCDAr)
      if(allocated(FoAr))
     1  deallocate(FoAr,FcAr,FsAr,WdFAr,HCondWdFAr,FoInd2Ave,IcAr)
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      if(allocated(OrderX)) deallocate(OrderX)
      if(allocated(ThetaArr)) deallocate(ThetaArr)
      if(allocated(Ave2FoInd)) deallocate(Ave2FoInd)
      if(allocated(OrderAve)) deallocate(OrderAve,KrFlagAve)
      call FeTmpFilesClear(SavedM40)
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      return
100   format(i15,'/',i15)
101   format(f15.2)
      end
