      subroutine EM9ManualCullQuest(xo,IRefAve,ICalc,LastOne,Konec)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xo(3),xsel(5),ysel(5)
      integer UseTabsIn,UseTabsInt,UseTabsDef,SbwLnQuest,
     1        SbwItemSelQuest,SbwItemPointerQuest,Konec
      integer, allocatable :: ISel(:),IDiff(:),IPorP(:),ICull(:),
     1                        ICullOld(:)
      character*80 Veta,t80
      character*30 FormRefAve,FormRefAveDef
      character*8  Flags,FlagsR
      character*2  Znak
      logical LastOne,EqIV
      real ICalc
      UseTabsIn=UseTabs
      n=NAveRedAr(IRefAve)
      allocate(ISel(n),IPorP(n),IDiff(n),ICull(n),ICullOld(n))
      call SetIntArrayTo(ISel,NAveRedAr(IRefAve),0)
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        if(ICullAr(m).eq.1) then
          ICull(i)=1
        else
          ICull(i)=0
        endif
        ICullOld(i)=ICull(i)
      enddo
      FormRefAve='(a,3(a1,i4),2(a1,f10.1),a1,a)'
      FormRefAveDef='(a,3(a1,i4),2(a1,a))'
      write(FormRefAve   (4:4),100) NDim(KPhase)
      write(FormRefAveDef(4:4),100) NDim(KPhase)
      id=NextQuestId()
      ild=min(NAveRed+4,12)
      Veta='Define/Modify culling flags'
      xq=-1.
      yq=-1.
      if(ICalc.gt.-900.) then
        xqd=520.+float(NDimI(KPhase))*20.
      else
        xqd=420.+float(NDimI(KPhase))*20.
      endif
      call FeDeferOutput
      n=0
1100  call FeQuestCreate(id,xq,yq,xqd,ild,Veta,0,LightGray,-1,-1)
      XQMin=QuestXMin(id)
      XQMax=QuestXMax(id)
      YQMin=QuestYMin(id)
      YQMax=QuestYMax(id)
      if(xo(1).gt.XQMin-9.5.and.xo(1).lt.XQMax+9.5.and.
     1   xo(2).gt.YQMin-9.5.and.xo(2).lt.YQMax+9.5.and.n.lt.5) then
        if(xo(1)-(XQMin+XQMax)*.5.ge.0.) then
          xq=xo(1)+XQMin-XQMax-10.
        else
          xq=xo(1)+10.
        endif
        if(xo(2)-(YQMin+YQMax)*.5.ge.0.) then
          yq=xo(2)+YQMin-YQMax-10.
        else
          yq=xo(2)+10.
        endif
        xq=anint(xq)
        yq=anint(yq)
        call FeQuestRemove(id)
        n=n+1
        go to 1100
      endif
      xsel(1)=xo(1)-6.
      ysel(1)=xo(2)-6.
      xsel(2)=xsel(1)+12.
      ysel(2)=ysel(1)
      xsel(3)=xsel(1)+12.
      ysel(3)=ysel(1)+12.
      xsel(4)=xsel(1)
      ysel(4)=ysel(1)+12.
      xsel(5)=xsel(1)
      ysel(5)=ysel(1)
      call FePlotMode('E')
      call FePolyLine(5,xsel,ysel,Red)
      call FePlotMode('N')
      call IndUnPack(HCondAveAr(IRefAve),ih,HCondLn,HCondMx,
     1               NDim(KPhase))
      il=1
      UseTabsInt=NextTabs()
      xpom=120.
      do i=1,NDim(KPhase)
        call FeTabsAdd(xpom,UseTabsInt,IdLeftTab,' ')
        xpom=xpom+20.
      enddo
      xpom=xpom+30.
      do i=1,2
        call FeTabsAdd(xpom,UseTabsInt,IdCharTab,'.')
        xpom=xpom+50.
      enddo
      xpom=xpom-15.
      call FeTabsAdd(xpom,UseTabsInt,IdRightTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabsInt,IdRightTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabsInt,IdRightTab,' ')
      UseTabsDef=NextTabs()
      xpom=120.
      do i=1,NDim(KPhase)
        call FeTabsAdd(xpom,UseTabsDef,IdLeftTab,' ')
        xpom=xpom+20.
      enddo
      xpom=xpom+40.
      do i=1,2
        call FeTabsAdd(xpom,UseTabsDef,IdLeftTab,' ')
        xpom=xpom+50.
      enddo
      tpom=10.
      UseTabs=UseTabsInt
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblAveraged1=LblLastMade
      UseTabs=UseTabsDef
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblAveraged2=LblLastMade
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,ild-3,dpom,ild-4,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwSel=SbwLastMade
      il=ild-2
      dpom=80.
      spom=15.
      xpom=.5*xqd-dpom-spom*.5
      Veta='%Select all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+spom
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=.5*xqd-dpom-spom*.5
      Veta='%Uncull'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtUncull=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+spom
      Veta='%Cull'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCull=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=xqd*.5-2.*dpom-spom*1.5
      Veta='%Avoid'
      do i=1,4
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtAvoid=ButtonLastMade
          Veta='%Next'
        else if(i.eq.2) then
          nButtNext=ButtonLastMade
          Veta='%End'
        else if(i.eq.3) then
          nButtEnd=ButtonLastMade
          Veta='A%pply'
        else
          nButtApply=ButtonLastMade
        endif
        xpom=xpom+dpom+spom
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      if(LastOne) then
        call FeQuestButtonDisable(nButtNext)
        call FeQuestMouseToButton(nButtEnd)
      else
        call FeQuestMouseToButton(nButtNext)
      endif
      nLblLast=nLblAveraged2
1250  call CloseIfOpened(SbwLnQuest(nSbwSel))
      do i=1,NAveRedAr(IRefAve)
        if(ICull(i).eq.2) ICull(i)=0
      enddo
1350  s1=0.
      s2=0.
      s4=0.
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        if(ICull(i).gt.0) cycle
        s1=s1+RIAr(m)
        s2=s2+1.
        s4=s4+RSAr(m)**2
      enddo
      if(s2.gt.1.5) then
        s1=s1/s2
        s4=sqrt(s4)/s2
        s3=0.
        do i=1,NAveRedAr(IRefAve)
          m=Ave2Org(i,IRefAve)
          if(ICull(i).gt.0) cycle
          Diff=abs(RIAr(m)-s1)
          s3=s3+Diff**2
        enddo
        pom=sqrt(s3/(s2*(s2-1.)))
        if(SigIMethod(KDatBlock).eq.1) then
          s3=s4
        else if(SigIMethod(KDatBlock).eq.2) then
          s4=s4*.5
          if(pom.lt.s4) then
            s3=s4
          else
            s3=pom
          endif
        else
          s3=max(pom,s4)
        endif
      else
        s3=sqrt(s4)
      endif
      if(s2.gt..5) then
        RIAveAr(IRefAve)=s1
        RSAveAr(IRefAve)=s3
        DiffMax=0.
        do i=1,NAveRedAr(IRefAve)
          if(icull(i).ne.0) cycle
          m=Ave2Org(i,IRefAve)
          Diff=abs(RIAr(m)-s1)/s3
          if(Diff.gt.DiffMax) then
            DiffMax=Diff
            kmax=i
          endif
        enddo
        if(FLimCull(KDatBlock).gt.0..and.
     1     DiffMax.gt.FLimCull(KDatBlock).and.s2.gt.2.5) then
          icull(kmax)=2
          m=Ave2Org(kmax,IRefAve)
          ICullAr(m)=2
          go to 1350
        endif
        PomI=RIAveAr(IRefAve)
        PomS=RSAveAr(IRefAve)
        UseTabs=UseTabsInt
        write(Veta,FormRefAve) 'Averaged:',
     1                        (Tabulator,ih(j),j=1,NDim(KPhase)),
     2                         Tabulator,PomI,Tabulator,PomS
        if(ICalc.gt.-900.)
     1    write(Veta(idel(Veta)+1:),'(2(a1,a,a1,a,a1,f10.1))')
     2      Tabulator,' ',Tabulator,'Calculated:',Tabulator,ICalc
        if(nLblLast.ne.nLblAveraged1) then
          call FeQuestLblOff(nLblLast)
          nLblLast=nLblAveraged1
          call FeQuestLblOn(nLblLast)
        endif
        call FeQuestLblChange(nLblLast,Veta)
      else
        UseTabs=UseTabsDef
        write(Veta,FormRefAveDef) 'Averaged:',
     1                        (Tabulator,ih(j),j=1,NDim(KPhase)),
     2                        (Tabulator,'---------',i=1,2)
        if(nLblLast.ne.nLblAveraged2) then
          call FeQuestLblOff(nLblLast)
          nLblLast=nLblAveraged2
          call FeQuestLblOn(nLblLast)
        endif
        call FeQuestLblChange(nLblLast,Veta)
      endif
      do i=1,NAveRedAr(IRefAve)
        call FeQuestSetSbwItemSel(i,nSbwSel,ISel(i))
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_ref.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NAveRedAr(IRefAve)
        m=Ave2Org(i,IRefAve)
        IDiff(i)=nint(1000.*
     1                (RIAr(m)-RIAveAr(IRefAve))/RSAveAr(IRefAve))
      enddo
      call indexx(NAveRedAr(IRefAve),IDiff,IPorP)
      UseTabs=UseTabsInt
      do i=1,NAveRedAr(IRefAve)
        j=IPorP(i)
        m=Ave2Org(j,IRefAve)
        call IndUnPack(HCondAr(1,m),ih,HCondLn,HCondMx,NDim(KPhase))
        pom=abs(float(IDiff(j)))*.001
        PomI=RIAr(m)
        PomS=RSAr(m)
        if(IDiff(j).gt.0) then
          Znak='+'
        else
          Znak='--'
        endif
        if(ICull(j).eq.1) then
          Flags='Cull-m'
        else if(ICull(j).eq.2) then
          Flags='Cull-a'
        else
          Flags=' '
          if(pom.gt. 3.) Flags=Znak
          if(pom.gt. 5.) Flags=Flags(:idel(Flags))//Znak
          if(pom.gt.10.) Flags=Flags(:idel(Flags))//Znak
          if(pom.gt.20.) Flags=Flags(:idel(Flags))//Znak
        endif
        if(NRefBlock.gt.1) then
          write(Cislo,'(''%'',i10)') mod(RefBlockAr(m),1000)
        else
          Cislo=' '
        endif
        if(ICalc.gt.-900.) then
          pom=(PomI-ICalc)/RSAveAr(IRefAve)
          if(pom.gt.0) then
            Znak='+'
          else
            Znak='--'
          endif
          pom=abs(pom)
          FlagsR=' '
          if(pom.gt. 3.) FlagsR=Znak
          if(pom.gt. 5.) FlagsR=FlagsR(:idel(FlagsR))//Znak
          if(pom.gt.10.) FlagsR=FlagsR(:idel(FlagsR))//Znak
          if(pom.gt.20.) FlagsR=FlagsR(:idel(FlagsR))//Znak
        endif
        k=RefBlockAr(m)/1000
        write(t80,'(''#'',i10)') k
        t80=Cislo(:idel(Cislo))//t80(:idel(t80))
        if(RunCCDMax(KRefBlock).gt.1) then
          l=0
          do nr=1,NRuns(KRefBlock)
            if(l+RunNFrames(nr,KRefBlock).lt.RunCCDAr(m)) then
              l=l+RunNFrames(nr,KRefBlock)
            else
              exit
            endif
          enddo
          l=RunCCDAr(m)-l
          write(Cislo,'(''-'',i6,''/'',i6)') nr,l
          t80=t80(:idel(t80))//Cislo(:idel(Cislo))
        endif
        call Zhusti(t80)
        write(Veta,FormRefAve) t80(:idel(t80)),
     1                        (Tabulator,ih(k),k=1,NDim(KPhase)),
     1                         Tabulator,PomI,
     2                         Tabulator,PomS,
     3                         Tabulator,Flags
        if(ICalc.gt.-900.)
     1    write(Veta(idel(Veta)+1:),'(2(a1,a))') Tabulator,' ',
     2                                           Tabulator,FlagsR
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_ref.tmp')
1500  if(EqIV(ICull,ICullOld,NAveRedAr(IRefAve))) then
        call FeQuestButtonOff(nButtEnd)
        if(LastOne) then
          call FeQuestMouseToButton(nButtEnd)
        else
          call FeQuestButtonOff(nButtNext)
          call FeQuestMouseToButton(nButtNext)
        endif
        call FeQuestButtonDisable(nButtApply)
        call FeQuestButtonDisable(nButtAvoid)
      else
        call FeQuestButtonDisable(nButtEnd)
        if(.not.LastOne) call FeQuestButtonDisable(nButtNext)
        call FeQuestButtonOff(nButtApply)
        call FeQuestButtonOff(nButtAvoid)
        call FeQuestMouseToButton(nButtApply)
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAvoid) then
        do i=1,NAveRedAr(IRefAve)
          ICullAr(Ave2Org(i,IRefAve))=ICullOld(i)
        enddo
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtApply)
     1  then
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNext)
     1  then
        Konec=0
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEnd)
     1  then
        Konec=1
      else if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh)) then
        if(CheckNumber.eq.nButtAll) then
          j=1
        else
          j=0
        endif
        call SetIntArrayTo(ISel,NAveRedAr(IRefAve),j)
        go to 1250
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtCull.or.CheckNumber.eq.nButtUnCull))
     2  then
        if(CheckNumber.eq.nButtCull) then
          j=1
        else
          j=0
        endif
        n=0
        do i=1,NAveRedAr(IRefAve)
          if(SbwItemSelQuest(i,nSbwSel).eq.1) then
            n=n+1
            k=IPorP(i)
            m=Ave2Org(k,IRefAve)
            ICullAr(m)=j
            ICull(k)=j
          endif
        enddo
        if(n.le.0) then
          k=IPorP(SbwItemPointerQuest(nSbwSel))
          m=Ave2Org(k,IRefAve)
          ICullAr(m)=j
          ICull(k)=j
        endif
        go to 1250
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
9999  call FeTabsReset(UseTabsInt)
      call FeTabsReset(UseTabsDef)
      UseTabs=UseTabsIn
      call FePlotMode('E')
      call FePolyLine(5,xsel,ysel,Red)
      call FePlotMode('N')
      if(allocated(ISel)) deallocate(ISel,IPorP,IDiff,ICull,ICullOld)
      return
100   format(i1)
      end
