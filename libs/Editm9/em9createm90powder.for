      subroutine EM9CreateM90Powder(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*256 Veta
      logical EqIgCase,First
      ich=0
      EM9ScToM90(KDatBlock)=1.
      First=.true.
      do 3000i=1,NRefBlock
        if(RefDatCorrespond(i).ne.KDatBlock) go to 3000
        if(First) then
          write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
          if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
          DatBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
          call OpenFile(90,DatBlockFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          NRef90(KDatBlock)=0
          First=.false.
        endif
        call OpenRefBlockM95(95,i,fln(:ifln)//'.m95')
        if(ErrFlag.ne.0) go to 3000
2000    read(95,FormA,end=2500) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 2500
        write(90,FormA) Veta(:idel(Veta))
        NRef90(KDatBlock)=NRef90(KDatBlock)+1
        go to 2000
2500    call CloseIfOpened(95)
        call CloseIfOpened(90)
        ModifiedRefBlock(i)=.false.
3000  continue
9999  return
      end
