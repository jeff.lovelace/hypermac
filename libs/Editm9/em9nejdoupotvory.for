      subroutine EM9NejdouPotvory(n,ih,h,ri,rs,nd,RealIndices,ObsLim)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension ih(*),h(*)
      logical RealIndices
      pom=anint(ri*10.)/anint(rs*10.)
      SumRelI(n)=SumRelI(n)+pom
      NExt(n)=NExt(n)+1
      if(pom.gt.ObsLim) then
        NExtObs(n)=NExtObs(n)+1
        if(pom.gt.RIExtMax(n)) then
          if(NExt500(n).lt.500) then
           NExt500(n)=NExt500(n)+1
            k=NExt500(n)
          else
            k=OrderExtRef(500,n)
          endif
          ria(k,n)=-nint(pom*10000.)
          rsa(k,n)= nint( rs*100.)
          if(RealIndices) then
            call CopyVek(h,HExt(1,k,n),3)
          else
            call CopyVekI(ih,IHExt(1,k,n),nd)
          endif
          if(NExt500(n).ge.500) then
            call Indexx(500,ria(1,n),OrderExtRef(1,n))
            RIExtMax(n)=-float(ria(OrderExtRef(500,n),n))*.0001
          endif
        endif
      endif
      return
      end
