      subroutine EM9TrMatApply(nLblCell,nLblModFr,Key,DirCosTr,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension DirCosTr(*)
      character*80 Veta
      character*2  nty
      ich=0
      if(Key.eq.0) then
        KRefB=0
      else
        KRefB=KRefBlock
      endif
      call CopyVek(CellRefBlock(1,KRefB),CellPar(1,1,KPhaseDR),6)
      call CopyVek(CellRefBlockSU(1,KRefB),CellParSU(1,1,KPhaseDR),6)
      do i=1,maxNDim-3
        call CopyVek(QuRefBlock(1,i,KRefB),Qu(1,i,1,KPhaseDR),3)
      enddo
      call SetMet(0)
      call EM9SetTransform(DirCosTr,Key,ich)
      if(ich.lt.0) then
        ich=0
        write(Veta,100)(CellRefBlock(i,0),i=1,6)
        call FeQuestLblChange(nLblCell,Veta)
        if(nLblModFr.gt.0) then
          nLbl=nLblModFr
          do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
            write(Veta,101) i,nty(i),(QuRefBlock(j,i,0),j=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
        endif
      endif
      return
100   format('Cell parameters:',3f8.4,3f8.3)
101   format(i1,a2,' modulation vector  ',3f7.4)
      end
