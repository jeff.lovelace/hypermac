      subroutine EM9CheckRefBlock(Void)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer SbwItemPointerQuest
      common/EM9CheckCommon/ nSbw,nButtInfo,nButtReimport,nButtModify,
     1                       nButtDelete,nButtUndelete
      save /EM9CheckCommon/
      external Void
      i=SbwItemPointerQuest(nSbw)
      if(i.eq.0) go to 9999
      if(NRef95(i).gt.0) then
        call FeQuestButtonDisable(nButtUndelete)
        call FeQuestButtonOff(nButtDelete)
        call FeQuestButtonOff(nButtModify)
      else
        call FeQuestButtonDisable(nButtDelete)
        call FeQuestButtonOff(nButtUndelete)
        call FeQuestButtonDisable(nButtModify)
      endif
      if(SourceFileRefBlock(i)(1:1).eq.'?') then
        call FeQuestButtonDisable(nButtReimport)
      else
        call FeQuestButtonOff(nButtReimport)
      endif
9999  return
      end
