      subroutine EM9ImportSavePar
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      dimension TrIn(36),DiffSatIn(3),QuIn(3,3),UbIn(9),CellIn(6)
      character*256 SourceFileIn
      character*10  SourceFileDateIn
      character*8   SourceFileTimeIn
      integer       DifCodeIn,RadiationIn,PolarizationIn,HKLF5RefBlockIn
     1             ,PwdMethodRefBlockIn
      logical       UseTrRefBlockIn
      real          LamAveIn,LamA1In,LamA2In
      save SourceFileIn,SourceFileDateIn,SourceFileTimeIn,NDimIn,TrIn,
     1     NDim95In,DifCodeIn,ITwReadIn,UseTrRefBlockIn,
     2     MMaxIn,DiffSatIn,QuIn,UBIn,LamAveIn,CellIn,RadiationIn,
     3     AngleMonIn,FractPerfMonIn,PolarizationIn,
     4     LamA1In,LamA2In,TOFDifcRefBlockIn,TOFDifaRefBlockIn,
     5     TOFZeroRefBlockIn,TOFTThRefBlockIn,ScaleRefBlockIn,
     6     PwdMethodRefBlockIn,EDOffsetRefBlockIn,EDSlopeRefBlockIn,
     7     EDTThRefBlockIn,TempRefBlockIn,HKLF5RefBlockIn
      SourceFileIn=SourceFileRefBlock(KRefBlock)
      SourceFileDateIn=SourceFileDateRefBlock(KRefBlock)
      SourceFileTimeIn=SourceFileTimeRefBlock(KRefBlock)
      NDimIn=maxNDim
      NDim95In=NDim95(KRefBlock)
      DifCodeIn=DifCode(KRefBlock)
      ITwReadIn=ITwRead(KRefBlock)
      UseTrRefBlockIn=UseTrRefBlock(KRefBlock)
      RadiationIn=RadiationRefBlock(KRefBlock)
      PolarizationIn=PolarizationRefBlock(KRefBlock)
      AngleMonIn=AngleMonRefBlock(KRefBlock)
      FractPerfMonIn=FractPerfMonRefBlock(KRefBlock)
      TOFDifcRefBlockIn=TOFDifcRefBlock(KRefBlock)
      TOFDifaRefBlockIn=TOFDifaRefBlock(KRefBlock)
      TOFZeroRefBlockIn=TOFZeroRefBlock(KRefBlock)
      TOFTThRefBlockIn=TOFZeroRefBlock(KRefBlock)
      EDOffsetRefBlockIn=EDOffsetRefBlock(KRefBlock)
      EDSlopeRefBlockIn=EDSlopeRefBlock(KRefBlock)
      EDTThRefBlockIn=EDTThRefBlock(KRefBlock)
      ScaleRefBlockIn=ScaleRefBlock(KRefBlock)
      PwdMethodRefBlockIn=PwdMethodRefBlock(KRefBlock)
      TempRefBlockIn=TempRefBlock(KRefBlock)
      HKLF5RefBlockIn=HKLF5RefBlock(KRefBlock)
      if(UseTrRefBlockIn)
     1  call CopyVek(TrRefBlock(1,KRefBlock),TrIn,maxNDim**2)
      if(maxNDim.gt.3.and..not.isPowder) then
        call CopyVek(DiffSatRefBlock(1,KRefBlock),DiffSatIn,3)
        MMaxIn=MMaxRefBlock(KRefBlock)
      endif
      if(DifCode(KRefBlock).gt.0.and.DifCode(KRefBlock).le.100) then
        call CopyMat(UB(1,1,KRefBlock),UBIn,3)
        LamAveIn=LamAveRefBlock(KRefBlock)
        if(CellReadIn(KRefBlock)) then
          call CopyVek(CellRefBlock(1,KRefBlock),CellIn,6)
          call CopyVek(QuRefBlock(1,1,KRefBlock),QuIn,3*(maxNDim-3))
        else
          call SetRealArrayTo(CellIn,6,0.)
          call SetRealArrayTo(QuIn,3*(maxNDim-3),0.)
        endif
      else
        LamA1In=LamA1RefBlock(KRefBlock)
        LamA2In=LamA2RefBlock(KRefBlock)
      endif
      go to 9999
      entry EM9ImportRestorePar
      SourceFileRefBlock(KRefBlock)=SourceFileIn
      SourceFileDateRefBlock(KRefBlock)=SourceFileDateIn
      SourceFileTimeRefBlock(KRefBlock)=SourceFileTimeIn
      maxNDim=NDimIn
      NDim95(KRefBlock)=NDim95In
      DifCode(KRefBlock)=DifCodeIn
      ITwRead(KRefBlock)=ITwReadIn
      UseTrRefBlock(KRefBlock)=UseTrRefBlockIn
      RadiationRefBlock(KRefBlock)=RadiationIn
      PolarizationRefBlock(KRefBlock)=PolarizationIn
      AngleMonRefBlock(KRefBlock)=AngleMonIn
      FractPerfMonRefBlock(KRefBlock)=FractPerfMonIn
      TOFDifcRefBlock(KRefBlock)=TOFDifcRefBlockIn
      TOFDifaRefBlock(KRefBlock)=TOFDifaRefBlockIn
      TOFZeroRefBlock(KRefBlock)=TOFZeroRefBlockIn
      TOFZeroRefBlock(KRefBlock)=TOFTThRefBlockIn
      EDOffsetRefBlock(KRefBlock)=EDOffsetRefBlockIn
      EDSlopeRefBlock(KRefBlock)=EDSlopeRefBlockIn
      EDTThRefBlock(KRefBlock)=EDTThRefBlockIn
      ScaleRefBlock(KRefBlock)=ScaleRefBlockIn
      PwdMethodRefBlock(KRefBlock)=PwdMethodRefBlockIn
      TempRefBlock(KRefBlock)=TempRefBlockIn
      if(UseTrRefBlockIn)
     1  call CopyVek(TrIn,TrRefBlock(1,KRefBlock),maxNDim**2)
      if(maxNDim.gt.3.and..not.isPowder) then
        call CopyVek(DiffSatIn,DiffSatRefBlock(1,KRefBlock),3)
        MMaxRefBlock(KRefBlock)=MMaxIn
      endif
      if(DifCode(KRefBlock).gt.0.and.DifCode(KRefBlock).le.100) then
        call CopyMat(UBIn,UB(1,1,KRefBlock),3)
        LamAveRefBlock(KRefBlock)=LamAveIn
        if(CellReadIn(KRefBlock)) then
          call CopyVek(CellIn,CellRefBlock(1,KRefBlock),6)
          call CopyVek(QuIn,QuRefBlock(1,1,KRefBlock),3*(maxNDim-3))
        endif
      else
        LamA1RefBlock(KRefBlock)=LamA1In
        LamA2RefBlock(KRefBlock)=LamA2In
      endif
9999  return
      end
