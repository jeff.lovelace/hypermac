      subroutine EM9CompleteBasic(StavVolani,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*256 EdwStringQuest
      character*80  Veta
      character*22  LabelRatio
      character*21  LabelT
      character*11  LabelWaveLength
      character*14  LabelWaveLength1
      character*2   nty
      integer FeMenu,EdwStateQuest,LblStateQuest,Polarization,
     1        StavVolani,UzTuByl
      real xp(6)
      logical CrwLogicQuest,lpom,SelectRadiation,KeepNDim
      equivalence (nEdwZero,nEdwZeroT),(nEdwDIFC,nEdwDT),
     1            (nEdwDIFA,nEdwAT)
      data LabelRatio/'%I(#2)/I(#1)'/,
     1     LabelWaveLength /'Wave length'/,
     2     LabelWaveLength1/'Wave length #1'/
      data LabelT/'%Number of domains'/
      if(StavVolani.ge.0) then
        UzTuByl=mod(StavVolani,10)
      else
        UzTuByl=0
      endif
      SelectRadiation=(DifCode(KRefBlock).lt.0.and.
     1                 LamAveRefBlock(KRefBlock).gt.0).or.
     1                (DifCode(KRefBlock).gt.100.and.
     2                 DifCode(KRefBlock).ne.IdDataD1AD2B.and.
     3                 DifCode(KRefBlock).ne.IdDataD1AD2BOld.and.
     4                 DifCode(KRefBlock).ne.IdDataD1BD20.and.
     5                 DifCode(KRefBlock).ne.IdDataSaclay.and.
     6                 DifCode(KRefBlock).ne.IdDataPSI.and.
     7                 DifCode(KRefBlock).ne.IdData11BM.and.
     8                 DifCode(KRefBlock).le.200)
      KeepNDim=DifCode(KRefBlock).eq.IdBrukerCCD.or.
     1         DifCode(KRefBlock).eq.IdBrukerCCDRaw
      id=NextQuestId()
      if(NBanks.gt.1) then
        write(Cislo,'(i5)') NBankActual
        call Zhusti(Cislo)
        Cislo=' for bank#'//Cislo(:idel(Cislo))
      else
        Cislo=' '
      endif
      Veta='Complete/correct experimental parameters'
      if(Cislo.ne.' ') Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestTitleMake(id,Veta)
      if(SilentRun) then
        ich=0
        go to 9999
      endif
      il=1
      tpom=5.
      if(isPowder.and.KRefBlock.le.1) then
        CellReadIn(KRefBlock)=CellRefBlock(1,0).gt.0.
      else
        CellReadIn(KRefBlock)=CellRefBlock(1,KRefBlock).gt.0.
      endif
      if(isPowder.and.KRefBlock.gt.1) then
        write(Veta,'(''Cell parameters:'',3f8.4,3f8.3)')
     1    CellPar(1:6,1,1)
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        write(Veta,'(''Target dimension:'',i2)') maxNDim
        il=il+1
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        do i=1,maxNDim-3
          il=il+1
          write(Veta,'(i1,a2,'' modulation vector'',3f7.4)')
     1      i,nty(i),Qu(1:3,i,1,1)
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        enddo
        il=il+6-maxNDim
        nEdwCell=0
        nEdwModFr=0
        nEdwModTo=0
        nEdwNDim95=0
        xpom=75.
        dpom=WizardLength*.5+20.-xpom
      else
        Veta='Cell para%meters:'
        xpom=140.
        dpom=WizardLength-xpom-5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCell=EdwLastMade
        if(isPowder) then
          k=0
        else
          k=KRefBlock
        endif
        call FeQuestRealAEdwOpen(EdwLastMade,CellRefBlock(1,k),6,
     1                           .not.CellReadIn(KRefBlock),.false.)
        il=il+1
        tpom=5.
        dpom=45.
        if(isPowder) then
          Veta='Target %dimension:'
          if(.not.KnownM50) NDim95(KRefBlock)=3
        else
          Veta='%Number of input indices:'
        endif
        if(KeepNDim) then
          write(Cislo,FormI15) NDim95(KRefBlock)
          call Zhusti(Cislo)
          Veta=Veta(2:idel(Veta))//'       '//Cislo(:idel(Cislo))
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          nEdwNDim95=0
        else
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          nEdwNDim95=EdwLastMade
          call FeQuestIntEdwOpen(nEdwNDim95,NDim95(KRefBlock),.false.)
          call FeQuestEudOpen(nEdwNDim95,3,6,1,0.,0.,0.)
        endif
        tpom=xpom+dpom+50.
        Veta='Info about metrics parameters'
        dpom=FeTxLengthUnder(Veta)+5.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtInfoCell=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%1st modulation vector:'
        tpom=5.
        dpom=WizardLength*.5+20.-xpom
        do i=1,3
          il=il+1
          write(Veta(2:4),'(i1,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            nEdwModFr=EdwLastMade
          else if(i.eq.3) then
            nEdwModTo=EdwLastMade
          endif
          call FeQuestRealAEdwOpen(EdwLastMade,
     1      QuRefBlock(1,i,k),3,QuRefBlock(1,i,k).lt.-300..and.
     2      .not.isPowder,.false.)
          if(i.gt.NDim95(KRefBlock)-3)
     1      call FeQuestEdwDisable(EdwLastMade)
        enddo
      endif
      il=il+1
      ilp=il
      xpom=5.
      if(SelectRadiation) then
        tpom=xpom+CrwgXd+10.
        do i=1,3
          if(i.eq.1) then
            Veta='%X-rays'
            lpom=RadiationRefBlock(KRefBlock).eq.XRayRadiation
          else if(i.eq.2) then
            Veta='Ne%utrons'
            lpom=RadiationRefBlock(KRefBlock).eq.NeutronRadiation
          else if(i.eq.3) then
            Veta='%Electrons'
            lpom=RadiationRefBlock(KRefBlock).eq.ElectronRadiation
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) then
            nCrwRadFirst=CrwLastMade
            ilb=il
          else
            xpomb=tpom+FeTxLength(Veta)+10.
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          il=il+1
        enddo
        nCrwRadLast=CrwLastMade
      else
        if(RadiationRefBlock(KRefBlock).eq.XRayRadiation.and.
     1     DifCode(KRefBlock).ne.IdData11BM.and.
     2     DifCode(KRefBlock).ne.IdEDInel) then
          ilb=il
          xpomb=xpom
          il=il+1
        else
          ilb=0
        endif
        nCrwRadFirst=0
        nCrwRadLast=0
      endif
      if(ilb.gt.0) then
        Veta='X%-ray tube'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
        nButtTube=ButtonLastMade
        if(nCrwRadFirst.eq.0)
     1    call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtTube=0
      endif
      tpom=5.
      nCrwDouble=0
      nEdwWL2=0
      nEdwRat=0
      if(DifCode(KRefBlock).ne.IdSXD.and.
     1   DifCode(KRefBlock).ne.IdTopaz.and.
     2   DifCode(KRefBlock).ne.IdSENJU.and.
     3   DifCode(KRefBlock).ne.IdSCDLANL.and.
     4   DifCode(KRefBlock).ne.IdVivaldi.and.
     5   DifCode(KRefBlock).ne.IdKoala.and.
     6   LamAveRefBlock(KRefBlock).gt.0..and.
     7   DifCode(KRefBlock).le.200) then
        if(isPowder.and.RadiationRefBlock(KRefBlock).eq.XRayRadiation
     1     .and.DifCode(KRefBlock).ne.IdData11BM) then
          Veta='Kalpha1/Kalpha2 dou%blet'
          xpom=tpom+CrwXd+10.
          call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwDouble=CrwLastMade
          il=il+1
          call FeQuestCrwOpen(nCrwDouble,NAlfaRefBlock(KRefBlock).gt.1)
          Veta=LabelWaveLength1
          xpom=tpom+FeTxLength(LabelWaveLength1)+10.
        else
          Veta=LabelWaveLength
          xpom=tpom+FeTxLength(Veta)+10.
        endif
        dpom=90.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwWL1=EdwLastMade
        if(isPowder) then
          il=il+1
          i=idel(LabelWaveLength1)
          Veta(i:i)='2'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwWL2=EdwLastMade
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                        EdwYd,0)
          nEdwRat=EdwLastMade
          call FeQuestRealEdwOpen(nEdwWL1,LamA1RefBlock(KRefBlock),
     1                       LamA1RefBlock(KRefBlock).lt.0.,.false.)
        else
          call FeQuestRealEdwOpen(EdwLastMade,LamAveRefBlock(KRefBlock),
     1                       LamAveRefBlock(KRefBlock).lt.0.,.false.)
        endif
        il=il+1
      else
        nButtTube=0
        nEdwWL1=0
        tpom=5.
        dpom=90.
      endif
      il=il+1
      Veta='%Temperature'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwT=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,TempRefBlock(KRefBlock),
     1                        .false.,.false.)
      nLblPol=0
      nCrwPolarizationFirst=0
      nCrwPolarizationLast=0
      nLblMono=0
      nEdwMonAngle=0
      nButtSetMonAngle=0
      nEdwPerfMon=0
      nButtInfoPerpendicular=0
      nButtInfoParallel=0
      nEdwDIFC=0
      nEdwDIFA=0
      nEdwZERO=0
      nEdwTTh=0
      nEdwWCross=0
      nEdwTCross=0
      nEdwDE=0
      nEdwZeroE=0
      nCrwJason=0
      if(RadiationDetails) then
        il=ilp
        if(DifCode(KRefBlock).le.200) then
          xpom=xpom+dpom+80.
          tpom=xpom+CrwgXd+10.
          Veta='Polarization correction:'
          call FeQuestLblMake(id,xpom+30.,il,Veta,'L','B')
          nLblPol=LblLastMade
          do i=1,5
            il=il+1
            if(i.eq.1) then
              Veta='Circul%ar polarization'
              j=PolarizedCircular
            else if(i.eq.2) then
              Veta='%Perpendicular setting'
             j=PolarizedMonoPer
            else if(i.eq.3) then
              Veta='Pa%rallel setting'
              j=PolarizedMonoPar
            else if(i.eq.4) then
              Veta='Guinier %camera'
              j=PolarizedGuinier
            else
              Veta='%Linearly polarized beam'
              j=PolarizedLinear
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,2)
            call FeQuestCrwOpen(CrwLastMade,
     1                          PolarizationRefBlock(KRefBlock).eq.j)
            if(i.eq.1) then
              nCrwPolarizationFirst=CrwLastMade
            else if(i.eq.2.or.i.eq.3) then
              if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
              Veta='Info'
              dpomb=FeTxLengthUnder(Veta)+10.
              call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
              if(i.eq.2) then
                nButtInfoPerpendicular=ButtonLastMade
              else
                nButtInfoParallel=ButtonLastMade
              endif
              call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            endif
          enddo
          nCrwPolarizationLast=CrwLastMade
          il=il+1
          tpom=xpom
          Veta='Monochromator parameters:'
          call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
          call FeQuestLblOff(LblLastMade)
          nLblMono=LblLastMade
          il=il+1
          Veta='Per%fectness'
          xpom=tpom+FeTxLengthUnder(Veta)+80.
          dpom=90.
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwPerfMon=EdwLastMade
          il=il+1
          Veta='Glancing an%gle'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwMonAngle=EdwLastMade
          Veta='%Set glancing angle'
          dpomb=FeTxLengthUnder(Veta)+5.
          call FeQuestButtonMake(id,xpom+dpom+15.,il,dpomb,ButYd,Veta)
          nButtSetMonAngle=ButtonLastMade
          Veta='Alp%ha(Guinier)'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwGAlpha=EdwLastMade
          il=il+1
          Veta='Beta(Gui%nier)'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwGBeta=EdwLastMade
          Polarization=PolarizationRefBlock(KRefBlock)
        else if(DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1          DifCode(KRefBlock).eq.IdTOFISIST.or.
     2          DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     3          DifCode(KRefBlock).eq.IdTOFVEGA.or.
     4          DifCode(KRefBlock).eq.IdTOFFree) then
          dpom=90.
          tpom=xpom+dpom+35.
          Polarization=PolarizedLinear
          Veta='TOF instrument parameters:'
          call FeQuestLblMake(id,tpom+110.,il,Veta,'L','B')
          il=il+1
          Veta='Use formula developed by Jason %Hodges'
          xpom=tpom+CrwXd+86.
          call FeQuestCrwMake(id,xpom,il,tpom+76.,il,Veta,'L',CrwXd,
     1                        CrwYd,1,0)
          call FeQuestCrwOpen(CrwLastMade,
     1                        TOFJasonRefBlock(KRefBlock).ge.1)
          nCrwJason=CrwLastMade
          Veta='Detector an%gle'
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          do i=1,5
            il=il+1
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) then
              nEdwTTh=EdwLastMade
              Veta='%ZERO'
            else if(i.eq.2) then
              nEdwZERO=EdwLastMade
              Veta='DIF%C'
            else if(i.eq.3) then
              nEdwDIFC=EdwLastMade
              Veta='DIF%A'
            else if(i.eq.4) then
              nEdwDIFA=EdwLastMade
              Veta='%WCross'
            else
              nEdwWCross=EdwLastMade
            endif
            if(i.eq.1) then
              pom=TOFTThRefBlock(KRefBlock)
              call FeQuestRealEdwOpen(EdwLastMade,pom,pom.lt.-3000.,
     1                                .false.)
            endif
          enddo
          tpom=xpom+dpom+20.
          xpom=tpom+80.
          Veta='ZER%OE'
          il=il-4
          do i=1,3
            il=il+1
            if(i.eq.3) il=il+1
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) then
              nEdwZEROE=EdwLastMade
              Veta='C%E'
            else if(i.eq.2) then
              nEdwDE=EdwLastMade
              Veta='TCro%ss'
            else
              nEdwTCross=EdwLastMade
            endif
          enddo
        endif
      endif
      NDim95Old=NDim95(KRefBlock)
      if(StavVolani.lt.0) call FeQuestButtonDisable(ButtonEsc)
1300  if(nCrwRadFirst.gt.0) then
        if(CrwLogicQuest(nCrwRadFirst)) then
          call FeQuestButtonOff(nButtTube)
          if(isPowder) call FeQuestCrwOpen(nCrwDouble,
     1                                   NAlfaRefBlock(KRefBlock).gt.1)
          RadiationRefBlock(KRefBlock)=XRayRadiation
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(CrwLogicQuest(nCrwRadLast)) then
            RadiationRefBlock(KRefBlock)=ElectronRadiation
          else
            RadiationRefBlock(KRefBlock)=NeutronRadiation
          endif
        endif
      endif
1320  if(nCrwDouble.ne.0) then
        if(CrwLogicQuest(nCrwDouble)) then
          call FeQuestRealEdwOpen(nEdwWL2,LamA2RefBlock(KRefBlock),
     1                          LamA2RefBlock(KRefBlock).lt.0.,.false.)
          call FeQuestRealEdwOpen(nEdwRat,LamRatRefBlock(KRefBlock),
     1                          LamA2RefBlock(KRefBlock).lt.0.,.false.)
          call FeQuestEdwLabelChange(nEdwWL1,LabelWaveLength1)
        else
          call FeQuestEdwClose(nEdwWL2)
          call FeQuestEdwClose(nEdwRat)
          call FeQuestEdwLabelChange(nEdwWL1,LabelWaveLength)
        endif
      endif
1340  if(RadiationDetails.and.DifCode(KRefBlock).le.200) then
        if(RadiationRefBlock(KRefBlock).eq.NeutronRadiation.or.
     1     RadiationRefBlock(KRefBlock).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            Polarization=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            Polarization=PolarizationRefBlock(KRefBlock)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(Polarization.eq.PolarizedMonoPer.or.
     1     Polarization.eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,
     1        FractPerfMonRefBlock(KRefBlock),.false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,
     1        AngleMonRefBlock(KRefBlock),.false.,.false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(Polarization.eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,
     1        FractPerfMonRefBlock(KRefBlock),.false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,
     1        AlphaGMonRefBlock(KRefBlock),.false.,.false.)
            call FeQuestRealEdwOpen(nEdwGBeta,
     1        BetaGMonRefBlock(KRefBlock),.false.,.false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
1350  if(nEdwTTh.gt.0) then
        if(CrwLogicQuest(nCrwJason)) then
          pom=TOFWCrossRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwWCross,pom,pom.lt.-3000.,.false.)
          pom=TOFTCrossRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwTCross,pom,pom.lt.-3000.,.false.)
          pom=TOFZeroERefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwZEROE,pom,pom.lt.-3000.,.false.)
          pom=TOFCeRefBlock(KRefBlock)
          call FeQuestRealEdwOpen(nEdwDE,pom,pom.lt.-3000.,.false.)
          if(EdwStateQuest(nEdwZERO).ne.EdwOpened) then
            pom=TOFZeroTRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwZEROT,pom,pom.lt.-3000.,.false.)
            pom=TOFCtRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDT,pom,pom.lt.-3000.,.false.)
            pom=TOFAtRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwAT,pom,pom.lt.-3000.,.false.)
          endif
          call FeQuestEdwLabelChange(nEdwZEROT,'%ZEROT')
          call FeQuestEdwLabelChange(nEdwDT,'%CT')
          call FeQuestEdwLabelChange(nEdwAT,'%AT')
        else
          call FeQuestEdwClose(nEdwWCross)
          call FeQuestEdwClose(nEdwTCross)
          call FeQuestEdwClose(nEdwZEROE)
          call FeQuestEdwClose(nEdwDE)
          if(EdwStateQuest(nEdwZERO).ne.EdwOpened) then
            pom=TOFZeroRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwZERO,pom,pom.lt.-3000.,.false.)
            pom=TOFDifcRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDIFC,pom,pom.lt.-3000.,.false.)
            pom=TOFDifaRefBlock(KRefBlock)
            call FeQuestRealEdwOpen(nEdwDIFA,pom,pom.lt.-3000.,.false.)
          endif
          call FeQuestRealEdwOpen(nEdwDIFA,pom,pom.lt.-3000.,.false.)
          call FeQuestEdwLabelChange(nEdwZERO,'%ZERO')
          call FeQuestEdwLabelChange(nEdwDIFC,'DIF%C')
          call FeQuestEdwLabelChange(nEdwDIFA,'DIF%A')
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNDim95) then
        call FeQuestIntFromEdw(nEdwNDim95,NDim95(KRefBlock))
        nEdw=nEdwModFr
        do i=1,3
          if(i+3.le.NDim95(KRefBlock)) then
            if(EdwStateQuest(nEdw).ne.EdwOpened)
     1        call FeQuestRealAEdwOpen(nEdw,QuRefBlock(1,i,KRefBlock),3,
     2                         QuRefBlock(1,i,KRefBlock).lt.0.,.false.)
          else
            call FeQuestEdwDisable(nEdw)
          endif
          nEdw=nEdw+1
        enddo
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwRadFirst.and.
     2         CheckNumber.le.nCrwRadLast)) then
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        EventType=EventEdw
        EventNumber=nEdwWL1
        go to 1320
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          if(isPowder) then
            if(CrwLogicQuest(nCrwDouble)) then
              call FeQuestRealEdwOpen(nEdwWL1,LamA1D(k),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2D(k),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRatD(k),.false.,
     1                                .false.)
            else
              call FeQuestRealEdwOpen(nEdwWL1,LamA1D(k),.false.,
     1                                .false.)
              LamA2RefBlock (KRefBlock)=LamA2D (k)
              LamRatRefBlock(KRefBlock)=LamRatD(k)
            endif
          else
            call FeQuestRealEdwOpen(nEdwWL1,LamAveD(k),.false.,.false.)
          endif
        endif
        EventType=EventEdw
        EventNumber=nEdwWL1
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoCell.or.
     2         CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoCell) then
          if(isPowder) then
            call FeFillTextInfo('completebasic1.txt',0)
          else
            call FeFillTextInfo('completebasic2.txt',0)
          endif
        else if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(isPowder) then
          call FeQuestRealFromEdw(nEdwWL1,
     1                            LamA1RefBlock(KRefBlock))
          if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
            if(CrwLogicQuest(nCrwDouble)) then
              call FeQuestRealFromEdw(nEdwWL2,
     1                                LamA2RefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwRat,
     1                                LamRatRefBlock(KRefBlock))
              LamAveRefBlock(KRefBlock)=
     1          (LamA1RefBlock(KRefBlock)+
     2           LamA2RefBlock(KRefBlock)*LamRatRefBlock(KRefBlock))/
     3          (1.+LamRatRefBlock(KRefBlock))
            else
              LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
            endif
          else
            LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
          endif
        else
          call FeQuestRealFromEdw(nEdwWL1,
     1                            LamAveRefBlock(KRefBlock))
        endif
        AngleMonPom=AngleMonRefBlock(KRefBlock)
        call CrlCalcMonAngle(AngleMonPom,LamAveRefBlock(KRefBlock),ichp)
        if(ichp.eq.0) then
          call FeQuestRealEdwOpen(nEdwMonAngle,AngleMonPom,.false.,
     1                            .false.)
          AngleMonRefBlock(KRefBlock)=AngleMonPom
          EventType=EventEdw
          EventNumber=nEdwMonAngle
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              Polarization=PolarizedCircular
            else if(i.eq.2) then
              Polarization=PolarizedMonoPer
            else if(i.eq.3) then
              Polarization=PolarizedMonoPar
            else if(i.eq.4) then
              Polarization=PolarizedGuinier
            else
              Polarization=PolarizedLinear
            endif
          endif
          nCrw=nCrw+1
        enddo
        go to 1300
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwJason) then
        go to 1350
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.ge.0) then
        if(nEdwNDim95.gt.0) then
          call FeQuestIntFromEdw(nEdwNDim95,NDim95(KRefBlock))
          do i=NDim95(KRefBlock)-2,3
            call SetRealArrayTo(QuRefBlock(1,i,KRefBlock),3,-333.)
          enddo
        endif
        if(UzTuByl.eq.0) then
          if(KRefBlock.eq.1.and.CreateNewRefBlock) then
            if(maxNDim.le.3.or.maxNDim.gt.NDim95(KRefBlock)) then
              n=3
              do i=1,3
                if(QuRefBlock(1,i,0).gt.-300.) n=n+1
              enddo
              NDim(KPhaseDR)=max(NDim95(KRefBlock),n)
            endif
            NDimI(KPhaseDR)=NDim(KPhaseDR)-3
            NDimQ(KPhaseDR)=NDim(KPhaseDR)**2
            call UnitMat(TrRefBlock(1,KRefBlock),NDim(KPhaseDR))
            maxNDim=max(maxNDim,NDim(KPhaseDR))
          endif
        endif
        if(nEdwCell.gt.0) then
          Veta=EdwStringQuest(nEdwCell)
          if(Veta.eq.' ') then
            if(ich.eq.0.and.KRefBlock.eq.1) then
              Veta='cell parameters not defined'
              nEdw=nEdwCell
              go to 1800
            endif
          else
            if(isPowder) then
              k=0
            else
              k=KRefBlock
            endif
            call FeQuestRealAFromEdw(nEdwCell,CellRefBlock(1,k))
            CellReadIn(KRefBlock)=CellRefBlock(1,KRefBlock).gt.0.
          endif
          nEdw=nEdwModFr
          do i=1,NDim95(KRefBlock)-3
            Veta=EdwStringQuest(nEdw)
            if(Veta.eq.' ') cycle
            call FeQuestRealAFromEdw(nEdw,QuRefBlock(1,i,KRefBlock))
            pom=0.
            do j=1,3
              pom=pom+abs(QuRefBlock(j,i,KRefBlock))
            enddo
            if(pom.lt..00001) then
              write(Veta,'(i1,a2,'' modulation vector isn''''t '',
     1                     ''acceptable'')') i,nty(i)
              go to 1800
            endif
            nEdw=nEdw+1
          enddo
        endif
        if(nCrwRadFirst.gt.0) then
          nCrw=nCrwRadFirst
          do i=1,2
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.1) then
                RadiationRefBlock(KRefBlock)=XRayRadiation
              else if(i.eq.2) then
                RadiationRefBlock(KRefBlock)=NeutronRadiation
              endif
            endif
            nCrw=nCrw+1
          enddo
        endif
        if(nEdwWL1.gt.0) then
          Veta=EdwStringQuest(nEdwWL1)
          if(Veta.eq.' ') then
            if(ich.eq.0) then
              Veta='wavelength not defined'
              nEdw=nEdwWL1
              go to 1800
            endif
          else
            if(isPowder) then
              call FeQuestRealFromEdw(nEdwWL1,
     1                                LamA1RefBlock(KRefBlock))
              if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
                KLamRefBlock(KRefBlock)=
     1            LocateInArray(LamA1RefBlock(KRefBlock),LamA1D,7,
     2                          .0001)
                if(CrwLogicQuest(nCrwDouble)) then
                  NAlfaRefBlock(KRefBlock)=2
                  call FeQuestRealFromEdw(nEdwWL2,
     1                                    LamA2RefBlock(KRefBlock))
                  call FeQuestRealFromEdw(nEdwRat,
     1                                    LamRatRefBlock(KRefBlock))
                  if(KLamRefBlock(KRefBlock).ne.0)
     1              KLamRefBlock(KRefBlock)=
     2                LocateInArray(LamA2RefBlock(KRefBlock),LamA2D,7,
     3                              .0001)
                  LamAveRefBlock(KRefBlock)=
     1                        (2.*LamA1RefBlock(KRefBlock)+
     2                            LamA2RefBlock(KRefBlock))/3.
                else
                  NAlfaRefBlock(KRefBlock)=1
                  LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
                endif
              else
                LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
                KLamRefBlock(KRefBlock)=0
                NAlfaRefBlock(KRefBlock)=1
              endif
            else
              call FeQuestRealFromEdw(nEdwWL1,
     1                                LamAveRefBlock(KRefBlock))
              if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
                KLamRefBlock(KRefBlock)=
     1            LocateInArray(LamAveRefBlock(KRefBlock),LamAveD,7,
     2                          .0001)
              else
                KLamRefBlock(KRefBlock)=0
              endif
            endif
          endif
          if(RadiationDetails) then
            PolarizationRefBlock(KRefBlock)=Polarization
            if(Polarization.ne.PolarizedCircular.and.
     1         Polarization.ne.PolarizedLinear.and.
     2         Polarization.ne.PolarizedGuinier) then
              call FeQuestRealFromEdw(nEdwMonAngle,
     1                                AngleMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwPerfMon,
     1                                FractPerfMonRefBlock(KRefBlock))
            else if(Polarization.eq.PolarizedGuinier) then
              call FeQuestRealFromEdw(nEdwGAlpha,
     1                                AlphaGMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwGBeta,
     1                                BetaGMonRefBlock(KRefBlock))
              call FeQuestRealFromEdw(nEdwPerfMon,
     1                                FractPerfMonRefBlock(KRefBlock))

            endif
          endif
        endif
        if(nEdwTTh.gt.0) then
          if(CrwLogicQuest(nCrwJason)) then
            TOFJasonRefBlock(KRefBlock)=1
          else
            TOFJasonRefBlock(KRefBlock)=0
          endif
          nEdw=nEdwTTh
          do i=1,4
            Veta=EdwStringQuest(nEdw)
            if(Veta.eq.' ') then
              if(i.eq.1) then
                Veta='detector angle'
              else if(i.eq.2) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='%ZERO'
                else
                  Veta='ZERO%E'
                endif
              else if(i.eq.3) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='DIF%C'
                else
                  Veta='%CT'
                endif
              else if(i.eq.4) then
                if(TOFJasonRefBlock(KRefBlock).eq.0) then
                  Veta='DIF%A'
                else
                  Veta='%AT'
                endif
              endif
              Veta=Veta(:idel(Veta))//' not defined'
              go to 1800
            endif
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.eq.1) then
              TOFTThRefBlock(KRefBlock)=pom
            else if(i.eq.2) then
              TOFZeroRefBlock(KRefBlock)=pom
            else if(i.eq.3) then
              TOFDifcRefBlock(KRefBlock)=pom
            else if(i.eq.4) then
              TOFDifaRefBlock(KRefBlock)=pom
            endif
            nEdw=nEdw+1
          enddo
          if(TOFJasonRefBlock(KRefBlock).ne.0) then
            nEdw=nEdwWCross
            do i=1,4
              Veta=EdwStringQuest(nEdw)
              if(Veta.eq.' ') then
                if(i.eq.1) then
                  Veta='WCross'
                else if(i.eq.2) then
                  Veta='ZERO%E'
                else if(i.eq.3) then
                  Veta='CE'
                else if(i.eq.4) then
                  Veta='TCross'
                endif
                Veta=Veta(:idel(Veta))//' not defined'
                go to 1800
              endif
              call FeQuestRealFromEdw(nEdw,pom)
              if(i.eq.1) then
                TOFWCrossRefBlock(KRefBlock)=pom
              else if(i.eq.2) then
                TOFZeroERefBlock(KRefBlock)=pom
              else if(i.eq.3) then
                TOFCeRefBlock(KRefBlock)=pom
              else
                TOFTCrossRefBlock(KRefBlock)=pom
              endif
              nEdw=nEdw+1
            enddo
          endif
        endif
        call FeQuestRealFromEdw(nEdwT,TempRefBlock(KRefBlock))
        if(UzTuByl.eq.0.and.StavVolani.ge.0) StavVolani=StavVolani+1
        go to 9999
1800    call FeChybne(-1.,-1.,Veta,'Please complete the form',
     1                SeriousError)
1850    call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        if(nEdw.ne.0) then
          EventType=EventEdw
          EventNumber=nEdw
        endif
        go to 1500
      endif
9999  if(ich.ge.0.and..not.isPowder) then
        call CopyVek(CellRefBlock(1,KRefBlock),xp,6)
        do i=4,6
          xp(i)=cos(xp(i)*ToRad)
        enddo
        call Recip(xp,DRRcp,pom)
      endif
      return
      end
