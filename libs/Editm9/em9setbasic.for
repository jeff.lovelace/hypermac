      subroutine EM9SetBasic
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      nDiffTypes=23
      OrderDiffTypes( 1)=IdCAD4
      OrderDiffTypes( 2)=IdNoniusCCD
      OrderDiffTypes( 3)=IdSiemensP4
      OrderDiffTypes( 4)=IdBrukerCCD
      OrderDiffTypes( 5)=IdBrukerCCDRaw
      OrderDiffTypes( 6)=IdKumaCCD
      OrderDiffTypes( 7)=IdKumaPD
      OrderDiffTypes( 8)=IdRigakuCCD
      OrderDiffTypes( 9)=IdIPDSStoe
      OrderDiffTypes(10)=IdD9ILL
      OrderDiffTypes(11)=IdVivaldi
      OrderDiffTypes(12)=IdSXD
      OrderDiffTypes(13)=IdTopaz
      OrderDiffTypes(14)=IdKoala
      OrderDiffTypes(15)=IdSCDLANL
      OrderDiffTypes(16)=IdHasyLabF1
      OrderDiffTypes(17)=IdHasyLabHuber
      OrderDiffTypes(18)=IdXDS
      OrderDiffTypes(19)=Id6T2LBB
      OrderDiffTypes(20)=IdPets
      OrderDiffTypes(21)=IdSENJU
      OrderDiffTypes(22)=IdPolNeutrons
      OrderDiffTypes(23)=IdSHELXINoAbsCorr

      nSingleTypes=13
      OrderSingleTypes( 1)=IdImportSHELXF
      OrderSingleTypes( 2)=IdImportSHELXI
      OrderSingleTypes( 3)=IdImportHKLF5
      OrderSingleTypes( 4)=IdImportIPDS
      OrderSingleTypes( 5)=IdImportCCDBruker
      OrderSingleTypes( 6)=IdImportCIF
      OrderSingleTypes( 7)=IdImportGraindex
      OrderSingleTypes( 8)=IdImportFullProf
      OrderSingleTypes( 9)=IdImportXD
      OrderSingleTypes(10)=IdImportDABEX
      OrderSingleTypes(11)=IdImportJanaM90
      OrderSingleTypes(12)=IdImportGeneralF
      OrderSingleTypes(13)=IdImportGeneralI

      nCWPowderTypes=20
      OrderCWPowderTypes( 1)=IdDataMAC
      OrderCWPowderTypes( 2)=IdDataGSAS
      OrderCWPowderTypes( 3)=IdDataRiet7
      OrderCWPowderTypes( 4)=IdDataD1AD2BOld
      OrderCWPowderTypes( 5)=IdDataD1AD2B
      OrderCWPowderTypes( 6)=IdDataD1BD20
      OrderCWPowderTypes( 7)=IdDataG41
      OrderCWPowderTypes( 8)=IdDataSaclay
      OrderCWPowderTypes( 9)=IdDataPSI
      OrderCWPowderTypes(10)=IdData11BM
      OrderCWPowderTypes(11)=IdDataAPS
      OrderCWPowderTypes(12)=IdDataCPI
      OrderCWPowderTypes(13)=IdDataUXD
      OrderCWPowderTypes(14)=IdDataM92
      OrderCWPowderTypes(15)=IdDataXRDML
      OrderCWPowderTypes(16)=IdDataPwdRigaku
      OrderCWPowderTypes(17)=IdDataPwdHuber
      OrderCWPowderTypes(18)=IdDataPwdStoe
      OrderCWPowderTypes(19)=IdDataFreeOnlyI
      OrderCWPowderTypes(20)=IdDataFree

      nTOFPowderTypes=7
      OrderTOFPowderTypes( 1)=IdTOFGSAS
      OrderTOFPowderTypes( 2)=IdTOFISISD
      OrderTOFPowderTypes( 3)=IdTOFISIST
      OrderTOFPowderTypes( 4)=IdTOFISISFullProf
      OrderTOFPowderTypes( 5)=IdTOFVega
      OrderTOFPowderTypes( 6)=IdTOFFree
      OrderTOFPowderTypes( 7)=IdEDInel
      end
