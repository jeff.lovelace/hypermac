      subroutine ChangeOrderM90
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,RefSt(:)
      integer isina(:),ipor(:)
      allocatable RefSt,isina,ipor
      if(iabs(DataType(KDatBlock)).eq.2) go to 9999
      call OpenDatBlockM90(90,KDatBlock,fln(:ifln)//'.m90')
      write(Cislo,'(''.l'',i2)') MxRefBlock+KDatBlock
      allocate(RefSt(NRef90(KDatBlock)),isina(NRef90(KDatBlock)),
     1         ipor(NRef90(KDatBlock)))
1100  do i=1,NRef90(KDatBlock)
        read(90,FormA) RefSt(i)
        read(RefSt(i),'(6i4)')(ih(j),j=1,NDim(KPhase))
        call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
        isina(i)=nint(sinthl*1000000.)
      enddo
      close(90)
      call indexx(NRef90(KDatBlock),isina,ipor)
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      Veta=fln(:ifln)//Cislo(:idel(Cislo))
      call OpenFile(90,Veta,'formatted','unknown')
      do i=1,NRef90(KDatBlock)
        j=ipor(i)
        write(90,FormA) RefSt(j)(:idel(RefSt(j)))
      enddo
      close(90)
      call CompleteM90
9999  if(allocated(RefSt)) deallocate(RefSt,isina,ipor)
      return
      end
