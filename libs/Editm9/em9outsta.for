      subroutine EM9OutStA(iven)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*42 iven(*)
      if(line.gt.0) then
        write(lst,FormLabAve)
        if(maxNDim.gt.3) then
          n=(line-1)/3+1
        else
          n=(line-1)/4+1
        endif
        do i=1,n
          if(maxNDim.gt.3) then
            write(lst,'(3a42)')(iven(i+(j-1)*n),j=1,3)
          else
            write(lst,'(4a32)')(iven(i+(j-1)*n)(1:32),j=1,4)
          endif
        enddo
        do i=1,line
          iven(line)=' '
        enddo
        line=mxline
      endif
      return
      end
