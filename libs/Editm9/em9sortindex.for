      integer function EM9SortIndex(ihref)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ihref(6),hh(6),hhp(6),ihp(6),ihpp(6),mmabs(3)
      logical EqIV,EqIVM
      EM9SortIndex=9
      do i=1,NComp(KPhase)
        mmabs(i)=NSatGroups+1
        mm=0
        do j=1,NDim(KPhase)
          hh(j)=ihref(j)
        enddo
        call multm(hh,zvi(1,i,KPhase),hhp,1,NDim(KPhase),NDim(KPhase))
        do j=1,NDim(KPhase)
          ihp(j)=nint(hhp(j))
        enddo
        do j=1,NSymm(KPhase)
          call MultMIRI(ihp,Rm6(1,j,i,KPhase),ihpp,1,NDim(KPhase),
     1                  NDim(KPhase))
          do m=0,NSatGroups
            if(EqIV (ihpp(4:),HSatGroups(1,m),NDimI(KPhase)).or.
     1         EqIVM(ihpp(4:),HSatGroups(1,m),NDimI(KPhase))) then
              mm=1
              mmabs(i)=min(mmabs(i),m)
            endif
          enddo
          if(mm.eq.0) mmabs(i)=min(mmabs(i),NSatGroups+1)
        enddo
        EM9SortIndex=min(mmabs(i),EM9SortIndex)
      enddo
      EM9SortIndex=EM9SortIndex+2
      return
      end
