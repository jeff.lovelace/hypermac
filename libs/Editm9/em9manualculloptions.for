      subroutine EM9ManualCullOptions(Key)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*80 Veta
      character*2 :: Gasket(3) = (/'Fe','W ','Re'/)
      integer RolMenuSelectedQuest,EdwStateQuest,XUseOld,YUseOld
      logical CrwLogicQuest,UseAutomaticCulling,ExistFile
      real GasketD(22,3),DiamondD(22)
      data (GasketD(i,1),i=1,8) /2.027,1.433,1.170,1.013,0.906,0.827,
     1                           0.766,-1.000/
      data (GasketD(i,2),i=1,9) /2.238,1.582,1.292,1.119,1.001,0.914,
     1                           0.846,0.791,-1.000/
      data (GasketD(i,3),i=1,22)/2.392,2.228,2.108,1.630,1.381,1.262,
     1                           1.196,1.174,1.155,1.114,1.054,1.010,
     2                           0.932,0.904,0.886,0.867,0.838,0.835,
     3                           0.815,0.797,0.785,-1.000/
      data DiamondD/2.0595,1.2612,1.0755,1.0297,0.8918,0.8184,0.7281,
     1              0.6865,0.6306,0.6030,0.5945,0.5640,0.5440,0.5370,
     2              0.5149,0.4995,0.4767,0.4644,0.4360,0.4209,0.4119,
     3              0.4092/
      if(Key.eq.0) then
        GasketUsed=0
        GasketDeltaTheta=.2
        DiamondUsed=0
        DiamondDeltaTheta=.3
        DisplaySelect=DisplayAll
        XUse=XUseIAve
        YUse=YUseIAve
        ObsLevelCull=3.
        XUseOld=-1
        YUseOld=-1
      else
        XUseOld=XUse
        YUseOld=YUse
        id=NextQuestId()
        xqd=500.
        il=16
        Veta='Options'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=1
        tpomb=5.
        Veta='Draw:'
        call FeQuestLblMake(id,tpomb,il,Veta,'L','B')
        xpom=tpomb+FeTxLength(Veta)+25.
        Veta='|I(i)-I(ave)|/sig(I(ave)) from averaging'
        tpom=xpom+CrwgXd+10.
        do i=1,3
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                        1)
          call FeQuestCrwOpen(CrwLastMade,i.eq.YUse)
          if(i.eq.1) then
            nCrwDrawIAve=CrwLastMade
            Veta='w.|F(obs)-F(calc)| from %refinement'
          else if(i.eq.2) then
            nCrwDrawWdF=CrwLastMade
            Veta='F(obs) v.s. F(%calc) from refinement'
          else if(i.eq.3) then
            nCrwDrawFcalc=CrwLastMade
          endif
          if(i.gt.1.and..not.ExistFile(fln(:ifln)//'.m83'))
     1      call FeQuestCrwDisable(CrwLastMade)
          il=il+1
        enddo
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Display:'
        call FeQuestLblMake(id,tpomb,il,Veta,'L','B')
        Veta='%all reflections'
        do i=1,5
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        2)
          call FeQuestCrwOpen(CrwLastMade,i-1.eq.DisplaySelect)
          if(i.eq.1) then
            nCrwDisplayAll=CrwLastMade
            Veta='Display only o%bserved reflections'
          else if(i.eq.2) then
            nCrwDisplayObserved=CrwLastMade
            tpome=xpom+FeTxLength(Veta)+15.
            Veta='=> observabitity level I>'
            xpome=tpome+FeTxLength(Veta)+10.
            dpom=50.
            call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,
     1                          EdwYd,0)
            nEdwSigLevel=EdwLastMade
            tpome=xpome+dpom+5.
            Veta='*sig(I)'
            call FeQuestLblMake(id,tpome,il,Veta,'L','N')
            nLblSigLevel=LblLastMade
            call FeQuestLblOff(LblLastMade)
            Veta='Displa%y only reflections with culling'
          else if(i.eq.3) then
            if(.not.CulledExist) call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayCulled=CrwLastMade
            Veta='Display reflections averaged only from t%wo'
          else if(i.eq.4) then
            if(.not.TwiceMeasuredExist)
     1        call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayMeasuredTwice=CrwLastMade
            Veta='Display reflections measured only o%nce'
          else
            if(.not.OnceMeasuredExist)
     1        call FeQuestCrwDisable(CrwLastMade)
            nCrwDisplayMeasuredOnce=CrwLastMade
          endif
          il=il+1
        enddo
        call FeQuestLinkaMake(id,il)
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+15.
        Veta='Apply automatic c%ulling'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwCull=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,FLimCull(KDatBlock).gt.0.)
        il=il+1
        xpom=25.
        Veta='Reflections |I-I(ave)|>'
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        nLblCull=LblLastMade
        call FeQuestLblOff(LblLastMade)
        xpom=FeTxLength(Veta)+xpom+5.
        dpom=40.
        tpom=xpom+dpom+5.
        Veta='*sig(I(ave)) will be culle%d'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwCull=EdwLastMade
        if(FLimCull(KDatBlock).lt.0.) then
          UseAutomaticCulling=.false.
          FLimCull(KDatBlock)=20.
        else
          UseAutomaticCulling=.true.
        endif
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+15.
        Veta='%High-pressure data collection'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwHighPressure=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,DiamondUsed.gt.0)
        il=il+1
        tpom=25.
        Veta='Gasket %material:'
        xpom=tpom+150.
        dpom=60.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolMenuGasket=RolMenuLastMade
        il=il+1
        Veta='Diam%ond theta width in degs:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDiamondDelta=EdwLastMade
        il=il+1
        Veta='%Gasket theta width in degs:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGasketDelta=EdwLastMade
1100    if(UseAutomaticCulling) then
          call FeQuestLblOn(nLblCull)
          call FeQuestRealEdwOpen(nEdwCull,FLimCull(KDatBlock),
     1                            .false.,.false.)
        else
          if(EdwStateQuest(nEdwCull).eq.EdwOpened) then
            call FeQuestLblOff(nLblCull)
            call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
            call FeQuestEdwClose(nEdwCull)
          endif
        endif
1200    if(DiamondUsed.gt.0) then
          call FeQuestRolMenuOpen(nRolMenuGasket,Gasket,3,GasketUsed)
          call FeQuestRealEdwOpen(nEdwDiamondDelta,2.*DiamondDeltaTheta,
     1                            .false.,.false.)
          call FeQuestRealEdwOpen(nEdwGasketDelta,2.*GasketDeltaTheta,
     1                            .false.,.false.)
        else
          call FeQuestRolMenuClose(nRolMenuGasket)
          if(EdwStateQuest(nEdwDiamondDelta).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwDiamondDelta,pom)
            DiamondDeltaTheta=pom*.5
            call FeQuestEdwClose(nEdwDiamondDelta)
          endif
          if(EdwStateQuest(nEdwGasketDelta).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwGasketDelta,pom)
            GasketDeltaTheta=pom*.5
            call FeQuestEdwClose(nEdwGasketDelta)
          endif
        endif
1400    if(CrwLogicQuest(nCrwDisplayObserved)) then
          call FeQuestRealEdwOpen(nEdwSigLevel,3.,.false.,.false.)
          call FeQuestLblOn(nLblSigLevel)
        else
          call FeQuestRealFromEdw(nEdwSigLevel,ObsLevelCull)
          call FeQuestEdwClose(nEdwSigLevel)
          call FeQuestLblOff(nLblSigLevel)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwHighPressure)
     1    then
          if(CrwLogicQuest(nCrwHighPressure)) then
            DiamondUsed=1
            GasketUsed=1
          else
            DiamondUsed=0
          endif
          go to 1200
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.eq.nCrwDisplayAll.or.
     2           CheckNumber.eq.nCrwDisplayObserved.or.
     3           CheckNumber.eq.nCrwDisplayCulled.or.
     4           CheckNumber.eq.nCrwDisplayMeasuredTwice.or.
     5           CheckNumber.eq.nCrwDisplayMeasuredOnce)) then
          go to 1400
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCull) then
          UseAutomaticCulling=CrwLogicQuest(nCrwCull)
          go to 1100
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          GasketUsed=RolMenuSelectedQuest(nRolMenuGasket)
          call FeQuestRealFromEdw(nEdwDiamondDelta,pom)
          DiamondDeltaTheta=pom*.5
          call FeQuestRealFromEdw(nEdwGasketDelta,pom)
          GasketDeltaTheta=pom*.5
          if(.not.UseAutomaticCulling) then
            FLimCull(KDatBlock)=-1.
          else
            call FeQuestRealFromEdw(nEdwCull,FLimCull(KDatBlock))
          endif
          if(CrwLogicQuest(nCrwDisplayAll)) then
            DisplaySelect=DisplayAll
          else if(CrwLogicQuest(nCrwDisplayObserved)) then
            DisplaySelect=DisplayObserved
            call FeQuestRealFromEdw(nEdwSigLevel,ObsLevelCull)
          else if(CrwLogicQuest(nCrwDisplayCulled)) then
            DisplaySelect=DisplayCulled
          else if(CrwLogicQuest(nCrwDisplayMeasuredTwice)) then
            DisplaySelect=DisplayMeasuredTwice
          else if(CrwLogicQuest(nCrwDisplayMeasuredOnce)) then
            DisplaySelect=DisplayMeasuredOnce
          endif
          if(CrwLogicQuest(nCrwDrawIAve)) then
            if(XUse.ne.XUseTheta) XUse=XUseIAve
            YUse=YUseIAve
          else if(CrwLogicQuest(nCrwDrawWdF)) then
            if(XUse.ne.XUseTheta) XUse=XUseFobs
            YUse=YUseWdF
          else
            XUse=XUseFobs
            YUse=YUseFcalc
          endif
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) then
          XUse=XUseOld
          YUse=YUseOld
          go to 9999
        endif
      endif
      call SetRealArrayTo(GasketTheta,22,0.)
      if(GasketUsed.gt.0) then
        do i=1,22
          if(GasketD(i,GasketUsed).lt.0.) exit
          pom=.5/GasketD(i,GasketUsed)*LamAveRefBlock(KRefBlock)
          if(pom.gt..99) then
            GasketTheta(i)=-20.
          else
            GasketTheta(i)=asin(pom)/ToRad
          endif
        enddo
      endif
      if(DiamondUsed.gt.0) then
        do i=1,22
          pom=.5/DiamondD(i)*LamAveRefBlock(KRefBlock)
          if(pom.gt..99) then
            DiamondTheta(i)=-20.
          else
            DiamondTheta(i)=asin(pom)/ToRad
          endif
        enddo
      endif
      if(YUse.eq.YUseOld) then
        XUse=XUseOld
      else
        Zoomed=.false.
      endif
9999  return
      end
