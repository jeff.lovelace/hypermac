      subroutine EM9SetTransform(DirCosTr,Key,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ZVPom(36),ZVIPom(36),ZSigP(9),ZSigIP(9),PomMat1(9),
     1          PomMat2(9),CellOut(6),VSig(9),RcpOut(3),DirCosTr(9),
     2          QuOut(3,3),CellOutSU(6)
      integer CoDal,FeSelectOnePossibility
      logical Psat,EqRV,FeYesNoHeader,WizardModeOld
      real MTPom(9),MTIPom(9)
      character*80 Veta
      Psat=.false.
      NInfo=1
      if(Key.eq.0) then
        TextInfo(NInfo)='Cell parameters'
        idl=idel(TextInfo(NInfo))
        if(maxNDim.gt.3) then
          TextInfo(NInfo)(idl+2:)='and modulation vector'
          idl=idel(TextInfo(NInfo))
          if(maxNDim.gt.4) then
            TextInfo(NInfo)(idl+1:)='s'
            idl=idl+1
          endif
        endif
        j=idel(TextInfo(NInfo))
        TextInfo(NInfo+maxNDim-1)=TextInfo(NInfo)(:j)//
     1                    ' as found in data collection:'
        TextInfo(NInfo)=TextInfo(NInfo)(:j)//
     1              ' as follows from the reference ones:'
      endif
      ich=0
      if(Key.eq.0) then
        call CopyMat(TrRefBlock(1,KRefBlock),ZVPom,maxNDim)
        call MatInv(ZVPom,ZVIPom,pom,maxNDim)
      else
        call CopyMat(TrRefBlock(1,KRefBlock),ZVIPom,maxNDim)
        call MatInv(ZVIPom,ZVPom,pom,maxNDim)
      endif
      if(abs(pom).le.0.) then
        call FeChybne(-1.,-1.,'transformation matrix is singular.',
     1               ' ',SeriousError)
        ich=1
        go to 9999
      endif
      if(maxNDim.gt.3) then
        m=0
        do j=1,3
          do k=1,3
            m=m+1
            pom=ZVPom(k+(j-1)*maxNDim)
            do l=4,maxNDim
              if(Key.eq.0) then
                KRefB=0
              else
                KRefB=KRefBlock
              endif
              pom=pom+ZVPom(k+(l-1)*maxNDim)*QuRefBlock(j,l-3,KRefB)
            enddo
            ZSigP(m)=pom
          enddo
        enddo
        call matinv(ZSigP,ZSigIP,pom,3)
        if(abs(pom).le.0.) then
          call FeChybne(-1.,-1.,'3x3 upper block is singular.',' ',
     1                   SeriousError)
          ich=1
          go to 9999
        endif
      else
        call CopyMat(ZV Pom,ZSigP ,3)
        call CopyMat(ZVIPom,ZSigIP,3)
      endif
      call trmat(ZSigP,PomMat1,3,3)
      call multm(ZSigP,MetTensI(1,1,KPhaseDR),PomMat2,3,3,3)
      call multm(PomMat2,PomMat1,MTIPom,3,3,3)
      call matinv(MTIPom,MTPom,pom,3)
      m=1
      do j=1,3
        CellOut(j)=sqrt(MTPom(m))
        RcpOut(j)=sqrt(MTIPom(m))
        m=m+4
      enddo
      k=0
      do i=1,3
        do j=1,3
          k=k+1
          DirCosTr(k)=ZSigIP(k)/rcp(j,1,KPhaseDR)*RcpOut(i)
        enddo
      enddo
      do j=4,6
        call indext(10-j,l,k)
        m=l+(k-1)*3
        CellOut(j)=MTPom(m)/(CellOut(l)*CellOut(k))
        if(abs(CellOut(j)).le..000001) CellOut(j)=0.
        CellOut(j)=acos(CellOut(j))/torad
      enddo
      do j=1,9
        MetTensS(j,1,KPhaseDR)=sqrt(MetTensS(j,1,KPhaseDR))
      enddo
      call trmat(ZSigIP,PomMat1,3,3)
      call MultMQ(ZSigIP,MetTensS(1,1,KPhaseDR),PomMat2,3,3,3)
      call MultMQ(PomMat2,PomMat1,MTIPom,3,3,3)
      m=1
      do j=1,3
        CellOutSU(j)=MTIPom(m)*.5/CellOut(j)
        m=m+4
      enddo
      do j=1,2
        do k=j+1,3
          m=k+(j-1)*3
          l=6-j-k
          pom=MTPom(m)
          if(abs(pom).gt..0001) then
            pom=(MTIPom(m)**2-
     1           (pom/CellOut(j)*CellOutSU(j))**2-
     2           (pom/CellOut(k)*CellOutSU(k))**2)/
     3           (pom*tan(CellOut(l+3)*ToRad)*ToRad)**2
            if(pom.gt.0.) then
              CellOutSU(l+3)=sqrt(pom)
            else
              CellOutSU(l+3)=0.
            endif
          else
            CellOutSU(l+3)=0.
          endif
        enddo
      enddo
      if(maxNDim.gt.3) then
        m=0
        do j=1,3
          do k=4,maxNDim
            m=m+1
            pom=ZVPom(k+(j-1)*maxNDim)
            do l=4,maxNDim
              if(Key.eq.0) then
                KRefB=0
              else
                KRefB=KRefBlock
              endif
              pom=pom+ZVPom(k+(l-1)*maxNDim)*
     1                QuRefBlock(j,l-3,KRefB)
            enddo
            VSig(m)=pom
          enddo
        enddo
        call multm(VSig,ZSigIP,PomMat1,maxNDim-3,3,3)
        call trmat(PomMat1,QuOut,maxNDim-3,3)
      endif
      if(Key.eq.0) then
        NInfo=NInfo+1
        write(TextInfo(NInfo),100) CellOut
        do i=1,maxNDim-3
          NInfo=NInfo+1
          write(TextInfo(NInfo),101)(QuOut(j,i),j=1,3)
        enddo
        if(CellRefBlock(1,KRefBlock).gt.0.) then
          NInfo=NInfo+2
          write(TextInfo(NInfo),100)(CellRefBlock(i,KRefBlock),i=1,6)
          Psat=.not.EqRV(CellRefBlock(1,KRefBlock),CellOut,3,.001).or.
     1         .not.EqRV(CellRefBlock(4,KRefBlock),CellOut(4),3,.01)
          do i=1,maxNDim-3
            NInfo=NInfo+1
            if(QuRefBlock(1,i,KRefBlock).gt.-300.) then
              write(TextInfo(NInfo),101)(QuRefBlock(j,i,KRefBlock),
     1                                   j=1,3)
              if(.not.Psat)
     1          Psat=.not.EqRV(QuRefBlock(1,i,KRefBlock),QuOut(1,i),3,
     2                         .001)
            else
              write(TextInfo(NInfo),102)
            endif
          enddo
          if(Psat) then
            WizardModeOld=WizardMode
            WizardMode=.false.
            CoDal=1
            NInfo=NInfo+1
            NHeader=NInfo
            TextInfo(NInfo)='Difference seems to be too large'
            NInfo=NInfo+1
            TextInfo(NInfo)='Continue with the old reference cell '//
     1                      'parameters'
            NInfo=NInfo+1
            TextInfo(NInfo)='Use the new cell parameters as the '//
     1                      'reference ones'
            NInfo=NInfo+1
            TextInfo(NInfo)='Quit the import process'
            CoDal=FeSelectOnePossibility(-1.,-1.,NHeader,CoDal)
            if(CoDal.eq.2) then
              call CopyVek(CellRefBlock(1,KRefBlock),CellRefBlock(1,0),
     1                     6)
              call CopyVek(CellOutSU,CellRefBlockSU(1,0),6)
              do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
                call CopyVek(QuOut(1,i),QuRefBlock(1,i,0),3)
              enddo
              ich=-1
            else if(CoDal.eq.3) then
              ich=1
            endif
            WizardMode=WizardModeOld
          endif
        endif
      else
        call CopyVek(CellOut,CellRefBlock(1,0),6)
        call CopyVek(CellOutSU,CellRefBlockSU(1,0),6)
        do i=1,min(maxNDim-3,NDim95(KRefBlock)-3)
          call CopyVek(QuOut(1,i),QuRefBlock(1,i,0),3)
        enddo
        ich=-1
      endif
9999  return
100   format(3f9.4,3f9.3)
101   format(13x,3f9.4)
102   format(13x,3('   ------'))
      end
