      subroutine EM9AverageStatReset
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      dimension rnums(8,11,2),rdens(8,11,2),
     1          rexpdens(8,11,2),rexpnums(8,11,2),
     2          nrefis(8,11,2),nrefas(8,11,2)
      character*128 Ven
      save rnums,rdens,rexpdens,rexpnums,nrefis,nrefas
      call SetRealArrayTo(rnums,176,0.)
      call SetRealArrayTo(rdens,176,0.)
      call SetRealArrayTo(rexpnums,176,0.)
      call SetRealArrayTo(rexpdens,176,0.)
      call SetIntArrayTo(nrefis,176,0)
      call SetIntArrayTo(nrefas,176,0)
      go to 9999
      entry EM9AverageStatRInt(sinthl,mmp,s1,s3,apom,pom,iprv)
      do j=1,8
        if(sinthl.le.sinmez(j)) go to 1200
      enddo
      j=8
1200  if(apom.ge.0.) then
        rnums(j,1,1)=rnums(j,1,1)+apom
        rdens(j,1,1)=rdens(j,1,1)+pom
      endif
      nrefis(j,1,1)=nrefis(j,1,1)+1
      if(iprv.eq.1) nrefas(j,1,1)=nrefas(j,1,1)+1
      if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
        if(apom.ge.0.) then
          rnums(j,1,2)=rnums(j,1,2)+apom
          rdens(j,1,2)=rdens(j,1,2)+pom
        endif
        nrefis(j,1,2)=nrefis(j,1,2)+1
        if(iprv.eq.1) nrefas(j,1,2)=nrefas(j,1,2)+1
      endif
      if(maxNDim.gt.3) then
        if(apom.ge.0.) then
          rnums(j,mmp,1)=rnums(j,mmp,1)+apom
          rdens(j,mmp,1)=rdens(j,mmp,1)+pom
        endif
        nrefis(j,mmp,1)=nrefis(j,mmp,1)+1
        if(iprv.eq.1) nrefas(j,mmp,1)=nrefas(j,mmp,1)+1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          if(apom.ge.0.) then
            rnums(j,mmp,2)=rnums(j,mmp,2)+apom
            rdens(j,mmp,2)=rdens(j,mmp,2)+pom
          endif
          nrefis(j,mmp,2)=nrefis(j,mmp,2)+1
          if(iprv.eq.1) nrefas(j,mmp,2)=nrefas(j,mmp,2)+1
        endif
      endif
      go to 9999
      entry EM9AverageStatrexp(sinthl,mmp,s1,s3)
      do j=1,8
        if(sinthl.le.sinmez(j)) go to 2200
      enddo
      j=8
2200  rexpnums(j,1,1)=rexpnums(j,1,1)+s3
      rexpdens(j,1,1)=rexpdens(j,1,1)+s1
      if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
        rexpnums(j,1,2)=rexpnums(j,1,2)+s3
        rexpdens(j,1,2)=rexpdens(j,1,2)+s1
      endif
      if(maxNDim.gt.3) then
        rexpnums(j,mmp,1)=rexpnums(j,mmp,1)+s3
        rexpdens(j,mmp,1)=rexpdens(j,mmp,1)+s1
        if(anint(s1*10.).gt.EM9ObsLim(KDatBlock)*anint(s3*10.)) then
          rexpnums(j,mmp,2)=rexpnums(j,mmp,2)+s3
          rexpdens(j,mmp,2)=rexpdens(j,mmp,2)+s1
        endif
      endif
      go to 9999
      entry EM9AverageStatOutput
      call NewPg(0)
      call TitulekVRamecku('Rint and other characteritics as a '//
     1                     'function of sin(th)/lambda')
      do i=1,mmMax
        nn=0
        do j=1,8
          nn=nn+nrefis(j,i,2)
        enddo
        if(nn.le.0) cycle
        if(maxNDim.gt.3) then
          call NewLn(3+2*7)
          if(i.eq.1) then
            Ven='Main reflections + satellites'
          else if(i.eq.2) then
            Ven='Main reflections'
          else
            Ven='Satellites +-('
            do j=1,NDimI(KPhase)
              write(Cislo,FormI15) HSatGroups(j,i-2)
              call zhusti(Cislo)
              Ven=Ven(:idel(Ven))//Cislo(:idel(Cislo))//','
            enddo
            j=idel(Ven)
            Ven(j:j)=')'
          endif
          write(lst,FormA)
          write(lst,FormA) Ven(:idel(Ven))
          write(lst,FormA1)('-',j=1,idel(Ven))
        else
          call NewLn(2*7)
        endif
        do l=1,2
          if(l.eq.1) then
            write(lst,'(/''All reflections'')')
          else
            write(lst,'(/''Observed reflections'')')
          endif
          write(lst,'(''sin(theta)/lambda'',8f10.6)') sinmez
          Ven=' '
          k=1
          do j=1,8
            if(rexpdens(j,i,l).gt.0.) then
              write(Ven(k:),'(f10.2)')
     1          rexpnums(j,i,l)/rexpdens(j,i,l)*100.
            else
              Ven(k:)='    ------'
            endif
            k=k+10
          enddo
          Ven='Rexp             '//Ven(:idel(Ven))
          write(lst,FormA) Ven(:idel(Ven))
          Ven=' '
          k=1
          do j=1,8
            if(rdens(j,i,l).gt.0.) then
              write(Ven(k:),'(f10.2)') rnums(j,i,l)/rdens(j,i,l)*100.
            else
              Ven(k:)='    ------'
            endif
            k=k+10
          enddo
          Ven='Rint             '//Ven(:idel(Ven))
          write(lst,FormA) Ven(:idel(Ven))
          write(lst,'(''Averaged from    '',8i10)')(nrefis(j,i,l),j=1,8)
          write(lst,'(''Number           '',8i10)')(nrefas(j,i,l),j=1,8)
        enddo
      enddo
9999  return
      end
