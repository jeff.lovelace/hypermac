      function KteraSc(prom)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension iar(4)
      character*(*) prom
      logical navelka,EqIgCase
      equivalence (iar,i)
      id=idel(prom)
      navelka=.false.
      do i=1,6
        if(EqIgCase(prom,lcell(i))) then
          KteraSc=ndoffPwd+ICellPwd+i-1
          go to 9999
        endif
      enddo
      do i=1,6
        if(EqIgCase(prom(1:1),'q')) then
          if(id.gt.3) go to 9000
          read(prom(2:3),101,err=9000) j,k
          k=max(1,k)
          if(j.lt.1.or.j.gt.3.or.k.lt.1.or.k.gt.maxNDimI)
     1      go to 9000
          KteraSc=ndoffPwd+IQuPwd+j-1+(k-1)*3
          go to 9999
        endif
      enddo
      if(EqIgCase(prom(1:4),lLamPwd)) then
        if(EqIgCase(prom(5:5),'1')) then
          i=1
        else if(EqIgCase(prom(5:5),'2')) then
          i=2
        endif
        KteraSc=ndoffPwd+ILamPwd+i-1
        go to 9999
       endif
       do i=1,3
        if(EqIgCase(prom,lShiftPwd(i))) then
          KteraSc=ndoffPwd+IShiftPwd+i-1
          prom=lShiftPwd(i)
          go to 9999
        endif
      enddo
      do i=1,3
        if(EqIgCase(prom,lTOF1Pwd(i))) then
          KteraSc=ndoffPwd+IShiftPwd+i-1
          prom=lTOF1Pwd(i)
          go to 9999
        endif
      enddo
      do i=1,7
        if(EqIgCase(prom,lTOF2Pwd(i))) then
          KteraSc=ndoffPwd+IShiftPwd+i-1
          prom=lTOF2Pwd(i)
          go to 9999
        endif
      enddo
      do i=1,4
        if(EqIgCase(prom,lGaussPwd(i))) then
          KteraSc=ndoffPwd+IGaussPwd+i-1
          prom=lGaussPwd(i)
          go to 9999
        endif
      enddo
      do i=1,4
        if(EqIgCase(prom,lGaussPwdF(i))) then
          KteraSc=ndoffPwd+IGaussPwd+i-1
          prom=lGaussPwdF(i)
          go to 9999
        endif
      enddo
      do i=1,3
        if(EqIgCase(prom,lGaussPwdTOF(i))) then
          KteraSc=ndoffPwd+IGaussPwd+i-1
          prom=lGaussPwdTOF(i)
          go to 9999
        endif
      enddo
      do i=1,4
        if(EqIgCase(prom,lLorentzPwd(i))) then
          KteraSc=ndoffPwd+ILorentzPwd+i-1
          prom=lLorentzPwd(i)
          go to 9999
        endif
      enddo
      do i=1,4
        if(EqIgCase(prom,lLorentzPwdF(i))) then
          KteraSc=ndoffPwd+ILorentzPwd+i-1
          prom=lLorentzPwdF(i)
          go to 9999
        endif
      enddo
      do i=1,5
        if(EqIgCase(prom,lLorentzPwdTOF(i))) then
          KteraSc=ndoffPwd+ILorentzPwd+i-1
          prom=lLorentzPwdTOF(i)
          go to 9999
        endif
      enddo
      if(EqIgCase(prom,lZetaPwd)) then
        KteraSc=ndoffPwd+IZetaPwd
        prom=lZetaPwd
        go to 9999
      endif
      if(EqIgCase(prom,lTOFAbsPwd)) then
        KteraSc=ndoffPwd+ITOFAbsPwd
        prom=lTOFAbsPWD
        go to 9999
      endif
      do i=1,4
        if(EqIgCase(prom,lAsymPwdD(i))) then
          KteraSc=ndoffPwd+IAsymPwd+i-1
          if(i.gt.2) KteraSc=KteraSc-2
          prom=lAsymPwdD(i)
          go to 9999
        endif
      enddo
      do i=1,8
        if(EqIgCase(prom,lAsymPwdF(i))) then
          KteraSc=ndoffPwd+IAsymPwd+i-1
          if(i.eq.8) KteraSc=KteraSc-6
          prom=lAsymPwdF(i)
          go to 9999
        endif
      enddo
      do i=1,6
        if(EqIgCase(prom,lAsymPwdDI(i))) then
          KteraSc=ndoffPwd+IAsymPwd+i-1
          prom=lAsymPwdDI(i)
          go to 9999
        endif
      enddo
      do i=1,4
        if(EqIgCase(prom,lAsymTOF1Pwd(i))) then
          KteraSc=ndoffPwd+IAsymPwd+i-1
          prom=lAsymTOF1Pwd(i)
          go to 9999
        endif
      enddo
      do i=1,8
        if(EqIgCase(prom,lAsymTOF2Pwd(i))) then
          KteraSc=ndoffPwd+IAsymPwd+i-1
          prom=lAsymTOF2Pwd(i)
          go to 9999
        endif
      enddo
      do i=1,MxEDRef
        if(EqIgCase(prom,lEDVar(i))) then
          KteraSc=ndoffED+i
          prom=lEDVar(i)
          go to 9999
        endif
      enddo
      if(EqIgCase(prom(1:5),'scale'))  then
        cislo=prom(6:)
        call posun(cislo,0)
        read(cislo,FormI15,err=9000) i
        kterasc=i
        go to 5000
      else if(EqIgCase(prom(1:8),'PrfScale'))  then
        kterasc=1
        go to 5000
      else if(EqIgCase(prom(1:9),'BckgScale'))  then
        kterasc=2
        go to 5000
      else if(EqIgCase(prom(1:5),'twvol'))  then
        cislo=prom(6:)
        call posun(cislo,0)
        read(cislo,FormI15,err=9000) i
        if(i.eq.1) go to 9000
        kterasc=mxscu+i-1
        go to 5000
      else if(EqIgCase(prom(1:5),'phvol'))  then
        cislo=prom(6:)
        call posun(cislo,0)
        read(cislo,FormI15,err=9000) i
        if(i.eq.1) go to 9000
        kterasc=mxscu+i-1
        go to 5000
      else if(EqIgCase(Prom(1:2),'GU')) then
        KteraSc=ndoffPwd+40
        NaVelka=.true.
      else if(EqIgCase(Prom(1:2),'GV')) then
        KteraSc=ndoffPwd+41
        NaVelka=.true.
      else if(EqIgCase(Prom(1:2),'GW')) then
        KteraSc=ndoffPwd+42
        NaVelka=.true.
      else if(EqIgCase(prom(1:4),lBackgPwd(1:4))) then
        KteraSc=ndoffPwd+IBackgPwd
        k=4
        call StToInt(prom,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9000
        KteraSc=KteraSc+i-1
        go to 5000
      else if(EqIgCase(prom(1:2),lStPwd)) then
        Cislo=Prom(3:)
        if(NDimI(KPhase).eq.1) then
          n=4
          nn=35
        else
          n=3
          nn=15
        endif
        if(idel(Cislo).eq.n) then
          read(Cislo,'(4i1)',err=9000)(iar(j),j=1,n)
          do j=1,n
            if(iar(j).gt.n+1.or.iar(j).lt.0) go to 9000
          enddo
          if(n.eq.3) then
            call StrainIndToN(iar,j)
          else
            call StrainIndToN4(iar,j)
          endif
          if(j.le.0.or.j.gt.nn) go to 9000
        else
          if(idel(Cislo).ne.2) go to 9000
          if(EqIgCase(Cislo(2:2),'m')) then
            j=16
          else if(EqIgCase(Cislo(2:2),'n')) then
            j=17
          else if(EqIgCase(Cislo(2:2),'p')) then
            j=18
          else
            go to 9000
          endif
        endif
        KteraSc=ndoffPwd+IStPwd+j-1
        go to 5000
      else if(EqIgCase(prom(1:4),lPrefPwd(1:4))) then
        KteraSc=ndoffPwd+IPrefPwd
        k=4
        call StToInt(prom,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9000
        KteraSc=KteraSc+i-1
        go to 5000
      else if(EqIgCase(prom(1:8),lBroadHKL(1:8))) then
        KteraSc=ndoffPwd+IBroadHKLPwd
        k=8
        call StToInt(prom,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9000
        KteraSc=KteraSc+i-1
        go to 5000
      else if(EqIgCase(prom(1:4),lAsymPwd(1:4))) then
        KteraSc=ndoffPwd+IAsymPwd
        k=4
        call StToInt(prom,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9000
        KteraSc=KteraSc+i-1
        go to 5000
      else if(EqIgCase(prom(1:5),lRoughPwd(1:5))) then
        KteraSc=ndoffPwd+IRoughPwd
        k=5
        call StToInt(prom,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9000
        KteraSc=KteraSc+i-1
        go to 5000
      else
        i=LocateSubstring(Prom,'#',.false.,.true.)
        if(i.gt.0) then
          Cislo=prom(i+1:)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=9000) k
        else
          k=1
        endif
      endif
      if(LocateSubstring(Prom,'GIso',.false.,.true.).eq.1) then
        kterasc=mxsc+13
        go to 5000
      else if(LocateSubstring(Prom,'GMag',.false.,.true.).eq.1) then
        kterasc=mxsc+3
        go to 5000
      else if(LocateSubstring(Prom,'g',.false.,.true.).eq.1) then
        kterasc=mxsc+12
        i=2
        go to 3500
      else if(LocateSubstring(Prom,'RhoIso',.false.,.true.).eq.1) then
        kterasc=mxsc+7
        go to 5000
      else if(LocateSubstring(Prom,'RhoMag',.false.,.true.).eq.1) then
        kterasc=mxsc+2
        go to 5000
      else if(LocateSubstring(Prom,'Rho',.false.,.true.).eq.1) then
        kterasc=mxsc+6
        i=4
        go to 3500
      else if(LocateSubstring(Prom,'TOverall',.false.,.true.).eq.1) then
        kterasc=mxsc+1
        go to 5000
      else if(LocateSubstring(Prom,'sclam/2',.false.,.true.).eq.1) then
        kterasc=mxsc+4
        go to 5000
      endif
      i=LocateSubstring(Prom,'(',.false.,.true.)
      if(i.eq.0) go to 9000
      j=LocateSubstring(Prom,')',.false.,.true.)
      if(j.eq.0.or.j.lt.i+2) go to 9000
      Cislo=prom(i+1:j-1)
      call uprat(Cislo)
      do i=1,NAtFormula(KPhase)
        if(EqIgCase(AtType(i,KPhase),Cislo)) go to 3100
      enddo
      go to 9000
3100  if(LocateSubstring(Prom,'f''',.false.,.true.).eq.1) then
        kterasc=MxSc+18+i
      else if(LocateSubstring(Prom,'f"',.false.,.true.).eq.1) then
        kterasc=MxSc+18+i+MaxNAtFormula
      else
        go to 9000
      endif
      go to 5000
3500  read(prom(i:i+1),101,err=9000) i,j
      if(i.lt.1.or.i.gt.3.or.j.lt.1.or.j.gt.3) go to 9000
      if(i.eq.j) then
        kterasc=kterasc+i
      else
        kterasc=kterasc+i+j+1
      endif
5000  if(navelka) call velka(prom)
      go to 9999
9000  kterasc=0
9999  return
101   format(2i1)
      end
