      subroutine tisk
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*6 format
      data format/'(f6.0)'/
      if(.not.FinalInver) then
        ivenr(1)(InvDel+2:)=' ch/su'
        n=0
        pom=0.
        do i=1,InvDel/10-1
          if(abs(rxa(i)).gt.abs(pom).and.kxa(i).ne.0) then
            pom=rxa(i)
            n=i
           endif
        enddo
        if(n.gt.0) then
          n=n*10+11
          ivenr(2)(n:n)='*'
        endif
        p=abs(pom)
        if(p.lt.10.) then
          format(5:5)='2'
        else if(p.lt.100.) then
          format(5:5)='1'
        else
          format(5:5)='0'
        endif
        write(ivenr(2)(InvDel+2:),format) pom
      endif
      write(lst,FormA)(ivenr(i),i=1,3)
      InvDel=10
      do i=1,3
        ivenr(i)(11:)=' '
      enddo
      return
      end
