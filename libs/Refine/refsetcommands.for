      subroutine RefSetCommands
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      data LastListek/1/
      call RewriteTitle('Refine-commands')
      call RefOpenCommands
      XdQuestRef=650.
      call FeKartCreate(-1.,-1.,XdQuestRef,19,'Refine commands',0,0)
      call FeCreateListek('Basic',1)
      KartIdBasic=KartLastId
      call RefBasicCommandsMake(KartIdBasic)
      call FeCreateListek('Select/Listing',1)
      KartIdSelect=KartLastId
      call RefSelectCommandsMake(KartIdSelect)
      call FeCreateListek('Various',1)
      KartIdVarious=KartLastId
      call RefVariousCommandsMake(KartIdVarious)
      call FeCreateListek('Modulation',1)
      KartIdModulation=KartLastId
      call RefModulationCommandsMake(KartIdModulation)
      call FeCompleteKart(LastListek)
3000  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdBasic) then
          call RefBasicCommandsUpdate
        else if(KartId.eq.KartIdSelect) then
          call RefSelectCommandsUpdate
        else if(KartId.eq.KartIdVarious) then
          call RefVariousCommandsUpdate
        else if(KartId.eq.KartIdModulation) then
          call RefModulationCommandsUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdBasic) then
          call RefBasicCommandsCheck
        else if(KartId.eq.KartIdSelect) then
          call RefSelectCommandsCheck
        else if(KartId.eq.KartIdVarious) then
          call RefVariousCommandsCheck
        else if(KartId.eq.KartIdModulation) then
          call RefModulationCommandsCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdBasic) then
          call RefBasicCommandsUpdate
          LastListek=1
        else if(KartId.eq.KartIdSelect) then
          call RefSelectCommandsUpdate
          LastListek=2
        else if(KartId.eq.KartIdVarious) then
          call RefVariousCommandsUpDate
          LastListek=3
        else if(KartId.eq.KartIdModulation) then
          call RefModulationCommandsUpdate
          LastListek=4
        endif
      endif
      call FeDestroyKart
      if(ich.ne.0) then
        call DeleteAllFiles('"'//fln(:ifln)//'_*.tmp"')
        go to 9999
      endif
      if(StructureLocked) then
        close(55,status='delete')
      else
        call RefRewriteCommands(0)
      endif
      call RewriteTitle(' ')
9999  return
      end
