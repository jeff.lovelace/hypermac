      subroutine RefFixed
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/FixedQuest/ nEdwAtoms,nEdwSetTo,FixedType,AtomString,
     1                   ValueSetTo
      character*256 AtomString
      character*80  Veta
      character*25  :: CrwLabels(9) = (/'A%ll parameters           ',
     1                                  'Coo%rdinates              ',
     2                                  'ADP %harmonic parameters  ',
     3                                  '%Modulation parameters    ',
     4                                  'Ch%arge density parameters',
     4                                  'Ori%gin                   ',
     5                                  'x%4 axis                  ',
     6                                  'Indi%vidual               ',
     7                                  'S%et individual           '/)
      integer WhatHappened,FixedType,CrwStateQuest
      logical PolarAxis,PolarX4Axis
      external RefFixedReadCommand,RefFixedWriteCommand,FeVoid,
     1         PolarAxis,PolarX4Axis
      save /FixedQuest/
      xqd=500.
      i=8
      call RepeatCommandsProlog(id,fln(:ifln)//'_fixed.tmp',xqd,i,il,
     1                          OKForBasicFiles)
      xpom=10.
      n=0
      do i=1,9
        if(i.ne.IdFixOrigin) then
          il=il+1
        else
          xpom=xpom+250.
          il=ilp
        endif
        call FeQuestCrwMake(id,xpom+15.,il,xpom,il,CrwLabels(i),'L',
     1                      CrwgXd,CrwgYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.IdFixAllPar)
        if(i.eq.1) then
          nCrwFirst=CrwLastMade
          ilp=il
        endif
        if((i.eq.IdFixOrigin.and..not.PolarAxis()).or.
     1     ((i.eq.IdFixModul.or.i.eq.IdFixOriginX4).and.
     2       NDimI(KPhase).eq.0).or.
     3     (i.eq.IdFixOriginX4.and..not.PolarX4Axis()))
     4    call FeQuestCrwDisable(CrwLastMade)
      enddo
      xpom=5.
      tpom=xqd*.5
      dpom=xqd-xpom-5.
      il=ilp+5
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,'Atoms/parameters','C',
     1                    dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      il=il+2
      Veta='Set t%o'
      dpom=50.
      xpom=FeTxLengthUnder(Veta)+5.
      tpom=(xqd-xpom-dpom)*.5
      xpom=tpom+xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSetTo=EdwLastMade
      ValueSetTo=0.
1300  FixedType=IdFixAllPar
      AtomString=' '
1350  call FeQuestStringEdwOpen(nEdwAtoms,AtomString)
      nCrw=nCrwFirst
      do i=1,9
        if(CrwStateQuest(nCrw).ne.CrwDisabled)
     1    call FeQuestCrwOpen(nCrw,i.eq.FixedType)
        nCrw=nCrw+1
      enddo
1400  if(FixedType.eq.IdFixSetTo) then
        call FeQuestRealEdwOpen(nEdwSetTo,ValueSetTo,.false.,.false.)
      else
        call FeQuestEdwDisable(nEdwSetTo)
      endif
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  RefFixedReadCommand,RefFixedWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventCrw) then
        FixedType=CheckNumber
        if(FixedType.eq.IdFixSetTo) then
          EventNumber=nEdwSetTo
        else
          EventNumber=nEdwAtoms
        endif
        EventType=EventEdw
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
