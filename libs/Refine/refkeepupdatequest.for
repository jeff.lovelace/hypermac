      subroutine RefKeepUpdateQuest(Void)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun,nButtSelAtoms
      character*80 Veta
      integer ButtonStateQuest,AtomToBeFilled
      logical Recalculate,SaveKeepCommands,MapAlreadyUsed,
     1        TryAutomaticRun
      external Void
      save /KeepQuest/
      i=EdwActive-EdwFr+1
      if(i.eq.nEdwCentr) then
        Veta='Se%lect central'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        AtomToBeFilled=SelectedCentr
        go to 2000
      else if(i.ge.nEdwNeighFirst.and.i.le.nEdwNeighLast) then
        Veta='Se%lect neighbors'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        AtomToBeFilled=SelectedNeigh
        go to 2000
      else if(i.ge.nEdwHFirst.and.i.le.nEdwHLast) then
        Veta='Se%lect hydrogens'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        AtomToBeFilled=SelectedHydro
        go to 2000
      else if(i.eq.nEdwAnchor) then
        Veta='Se%lect anchor'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        AtomToBeFilled=SelectedAnchor
        go to 2000
      else
        if(ButtonStateQuest(nButtSelect).ne.ButtonClosed)
     1    call FeQuestButtonDisable(nButtSelect)
        AtomToBeFilled=SelectedNothing
        go to 9999
      endif
2000  if(ButtonStateQuest(nButtSelect).ne.ButtonOff)
     1   call FeQuestButtonOff(nButtSelect)
9999  return
      end
