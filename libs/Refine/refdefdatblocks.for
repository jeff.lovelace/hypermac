      subroutine RefDefDatBlocks
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*256 Veta
      logical CrwLogicQuest,lpom
      id=NextQuestId()
      xqd=300.
      il=nint(float(NDatBlock)*.8)+1
      Veta='Define datablocks to be used in the refinement:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwXd+10.
      do i=1,NDatBlock
        il=il-8
        Veta=MenuDatBlock(i)(:5)//'%'//MenuDatBlock(i)(6:)
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        if(i.eq.1) nCrwDatBlockFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,UseDatBlockInRefine(i))
      enddo
      il=il-10
      Veta='%Use all'
      dpom=FeTxLengthUnder(Veta)+15.
      xpom=xqd*.5-dpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAll=ButtonLastMade
      Veta='%Refresh'
      xpom=xqd*.5+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        lpom=CheckNumber.eq.nButtAll
        nCrw=nCrwDatBlockFirst
        do i=1,NDatBlock
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nCrw=nCrwDatBlockFirst
        do i=1,NDatBlock
          UseDatBlockInRefine(i)=CrwLogicQuest(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
