      subroutine RefRewriteCommands(klic)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 t80
      call WriteKeys('refine')
      j=NactiInt+NactiReal+21
      do KDatB=1,NDatBlock
        if(ExtTensor(KDatB).ne.0) then
          if(NDatBlock.gt.1) then
            write(t80,100) IdRefine(nCmdExtLabel),KDatB
            call ZdrcniCisla(t80,2)
            idl=idel(t80)
            n=10
          else
            n=8
            idl=-1
          endif
          write(t80(idl+2:),100)
     1      IdRefine(nCmdiext),ExtTensor(KDatB),
     2      IdRefine(nCmditypex),ExtType(KDatB),
     3      IdRefine(nCmdidistr),ExtDistr(KDatB),
     4      IdRefine(nCmdrrext),ExtRadius(KDatB)
          call ZdrcniCisla(t80,n)
          t80='  '//t80(:idel(t80))
          write(55,FormA) t80(:idel(t80))
        endif
      enddo
      do i=j,j+9
        call MergeFileToM50(NactiKeywords(i))
      enddo
      if(nxxn.gt.0) then
        write(t80,'(20i3)')(nxxp(i),i=1,nxxn)
        call ZdrcniCisla(t80,nxxn)
        t80='  skipflag '//t80(:idel(t80))
        write(55,FormA) t80(:idel(t80))
      endif
      if(RefRandomize) then
        write(t80,'(i5,f10.5)') RefRandomSeed,RefRandomDiff
        call ZdrcniCisla(t80,2)
        i=nCmdRandomize
        write(55,'(2x,a,1x,a)') IdRefine(i)(:idel(IdRefine(i))),
     1                          t80(:idel(t80))
      endif
      if(NDimI(KPhase).gt.0) then
        if(NDimI(KPhase).eq.1) then
          j=32
        else
          j=16
        endif
        do i=1,NDimI(KPhase)
          if(ngrid(i).ne.j) then
            write(t80,'(''grid '',3i5)')(ngrid(n),n=1,NDimI(KPhase))
            call ZdrcniCisla(t80,NDimI(KPhase)+1)
            write(55,FormA) '  '//t80(:idel(t80))
            go to 1150
          endif
        enddo
      endif
1150  if(NDatBlock.gt.1) then
        n=1
        t80=IdRefine(nCmdUseDatBl)
        do i=1,NDatBlock
          if(UseDatBlockInRefine(i)) then
            n=n+1
            write(t80(idel(t80)+1:),'(i3)') i
          endif
        enddo
        call ZdrcniCisla(t80,n)
        t80='  '//t80(:idel(t80))
        if(n.le.NDatBlock) write(55,FormA) t80(:idel(t80))
      endif
      call MergeFileToM50('rest')
      write(55,'(''end refine'')')
      call DopisKeys(Klic)
      if(VasekTest.ne.0.and.reddyn.le.0.) then
        NInfo=1
        TextInfo(1)='Podivnost: reddyn <=0'
        call FeInfoOut(-1.,-1.,'POZOR, POZOR !!!','L')
      endif
      if(allocated(ExtTensor))
     1  deallocate(ExtTensor,ExtType,ExtDistr,ExtRadius)
      return
100   format(3(a8,i3,1x),a8,f10.6)
      end
