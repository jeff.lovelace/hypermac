      subroutine Refine(Klic,RefineEnd)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use Bessel_mod
      use RefPowder_mod
      use Powder_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      integer ih(6),mmax(3),NSupPom(3)
      real, allocatable :: TrToOrthoCorr(:,:,:),snwp(:),cswp(:),FPolA(:)
      real TrMolCorr(9,mxm),TrPom(9),xp(3),xo(3),sm(3),ssm(3),fc(3),
     1     xs(6),x4center(3),delta(3)
      character*256 Veta
      character*128 ven
      character*80  t80,TmpFile
      character*8   timst,timen
      character*4   chlst
      integer UseTabsIn,TryAgain,ihitw(6,96)
      logical PwdCheckTOFInD,RefineEnd,ExistFile,EqIgCase,FeYesNo,
     1        FlowChartUsed,CIFAllocated,ResetUsedTabs,Ranover
      data Ranover/.false./
      AnnouncedTwinProblem=.false.
1100  TryAgain=0
      CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9900
      endif
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_Refine.l70','formatted',
     1              'unknown')
      KDatBlockIn=KDatBlock
      UseTabsIn=UseTabs
      ResetUsedTabs=.false.
      RefineEnd=.false.
      VisiMsgRf=.false.
      VisiMsgSc=.false.
      MakeItPermanent=.false.
      nFlowChart=0
      if(LamAve(1).lt.0.) then
        LamAve(1)=LamAveD(6)
        LamA1(1)=LamA1D(6)
        LamA2(1)=LamA1D(6)
        AngleMon(1)=CrlMonAngle(MonCell(1,3),MonH(1,3),LamAve(1))
        KLam(1)=6
      endif
      if(MaxNDimI.gt.0) then
        call RefUpdateOrtho
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
      if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1   ExistM42.and.CalcDyn) then
        if(.not.allocated(ScRes)) then
          allocate(ScRes(ubound(Sc,1),ubound(Sc,2)))
          ScRes=Sc
        endif
        do i=1,ubound(Sc,1)
          ScRes(i,KDatBlock)=Sc(i,KDatBlock)
        enddo
      endif
      call RefPrelim(Klic)
      SetMetAllowed=.false.
      if(ErrFlag.ne.0) go to 9900
      if(Klic.eq.1) go to 9990
      if(isimul.le.0.and..not.ExistM90) then
        call FeChybne(-1.,-1.,'the refinement cannot be executed as',
     1                'the reflection file M90 doesn''t exist.',
     2                SeriousError)
        go to 9990
      endif
      call DeleteFile(fln(:ifln)//'_refine.tmp')
      if(NAtCalc.gt.0.and.DoLeBail.eq.0) then
        LstRef=NextLogicNumber()
        open(LstRef,file=fln(:ifln)//'.l99')
      else
        LstRef=0
      endif
      LstSing=NextLogicNumber()
      open(LstSing,file=fln(:ifln)//'.l69')
      TimeStart=FeEtime()
      call FeDateAndTime(t80,timst)
      icykl=0
      NDynCycle=0
      nRLast=0
      TlumOrigin=Tlum
      JenSc=.false.
      lst0=lst
      LnPrf=0
      call DeleteFile(fln(:ifln)//'.m80')
      NPntsAll=0
      NRefAll=0
      NSing=0
      FlowChartUsed=.false.
      do KDatBlock=1,NDatBlock
        if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1     ExistM42.and.CalcDyn) then
          if(ActionED.ne.1.and.ActionDatBlock.ne.KDatBlock) cycle
        endif
        if(DatBlockDerTest.le.0) then
          if(.not.UseDatBlockActual(KDatBlock)) cycle
        else
          if(KDatBlock.ne.DatBlockDerTest) cycle
        endif
        if(MagPolFlag(KDatBlock).ne.0) call RefGetUBMatrix
        isPowder=iabs(DataType(KDatBlock)).eq.2
        CorrScPwd=isPowder
        call PwdSetTOF(KDatBlock)
        if(isPowder) then
          if(isTOF) then
            SinTOFThUse=sin(.5*TOFTTh(KDatBlock)*ToRad)
          else
            LPTypeCorrUse=LpFactor(KDatBlock)
            FractPerfMonUse=FractPerfMon(KDatBlock)
            if(LPTypeCorrUse.eq.PolarizedGuinier) then
              snm=sin(AlphaGMon(KDatBlock)*ToRad)
              BetaGMonUse=BetaGMon(KDatBlock)*ToRad
            else
              snm=sin(AngleMon(KDatBlock)*ToRad)
            endif
            Cos2ThMon =1.-2.*snm**2
            Cos2ThMonQ=Cos2ThMon**2
          endif
          if(isimul.le.0) call PwdM92Nacti
          if(MagneticType(KPhase).ne.0) call SetMag(0)
          call PwdLeBail(2,.true.)
          if(MagneticType(KPhase).ne.0) call SetMag(1)
          if(ErrFlag.ne.0) then
            call FeFlowChartRemove
            go to 9900
          endif
          FlowChartUsed=.true.
          call CloseIfOpened(91)
          NPntsAll=NPntsAll+NPnts
          NRefAll=NRefAll+NRef91(KDatBlock)
        else
          if(isimul.gt.0) then
            call GenerQuest(1,0)
            if(ErrFlag.ne.0) go to 9990
            if(NTwin.gt.1) then
              if(TwMax.lt.TwDiff) TwMax=TwDiff
              TmpFile='jgen'
              call CreateTmpFile(TmpFile,i,0)
              call FeTmpFilesAdd(TmpFile)
              call CloseIfOpened(91)
              call CopyFile(fln(:ifln)//'.l91',TmpFile)
              call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
              call OpenFile(92,TmpFile,'formatted','unknown')
1120          read(92,Format91,end=1140)(ih(i),i=1,MaxNDim),RIObs,RsObs,
     1          i1,i2,itwr,snl
              ihitw=0
              ihitw(1,1:NTwin)=999
              do itw=1,NTwin
                call settw(ih,ihitw,itw,Ranover)
                do jtw=1,itw-1
                  if(ihitw(1,jtw).lt.900) go to 1120
                enddo
                write(91,Format91)(ih(i),i=1,MaxNDim),RIObs,RsObs,i1,i2,
     1                            itw,snl
              enddo
              go to 1120
1140          call DeleteFile(TmpFile)
              call FeTmpFilesClear(TmpFile)
!              rewind(91)
              call CloseIfOpened(91)
            endif
          else if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1            ExistM42.and.CalcDyn) then
            SinThLMx=0.
            mmax=0
            call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
            if(ErrFlag.ne.0) go to 9990
1200        read(91,FormA,end=1300) Veta
            if(Veta.eq.' ') then
              go to 1200
            else
              k=0
              call kus(Veta,k,Cislo)
            endif
            if(EqIgCase(Cislo,'data')) go to 1300
            read(Veta,Format91,end=1300,err=9900)(IHRead(i),i=1,maxNDim)
            if(IHRead(1).gt.900) go to 1300
            KPhase=KPhaseTwin(1)
            call FromIndSinthl(ihread,xp,sinthl,sinthlq,1,0)
            SinThLMx=max(SinThL,SinThLMx)
            do j=4,NDim(KPhase)
              mmax(j-3)=max(mmax(j-3),ihread(j))
            enddo
            go to 1200
1300        call CloseIfOpened(91)
            SinThLMx=max(SinThLMx,GMaxEDZone(KDatBlock))
            ns =NSymm (KPhase)
            nsn=NSymmN(KPhase)
            NSymm (KPhase)=1
            NSymmN(KPhase)=1
            Veta=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
            call OpenFile(91,Veta,'formatted','unknown')
            if(ErrFlag.ne.0) go to 9900
            Veta='Generating of reflections'
            if(NDatBlock.gt.1) then
              write(Cislo,'(i5)') KDatBlock
              call Zhusti(Cislo)
              Veta=Veta(:idel(Veta))//' for block#'//Cislo
            endif
            call GenerRef(Veta,sinthlmx,mmax,.false.,0)
            call CloseIfOpened(91)
            NSymm (KPhase)=ns
            NSymmN(KPhase)=nsn
          endif
          NRefAll=NRefAll+NRef90(KDatBlock)
        endif
      enddo
      if(FlowChartUsed) call FeFlowChartRemove
      KDatBlock=KDatBlockIn
      UseTabs=NextTabs()
      xpom=0.
      do i=1,4
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+FeTxLength(StRFac(IStRFac(i)))+35.
        call FeTabsAdd(xpom,UseTabs,IdLeftTab,' ')
        xpom=xpom+20.
      enddo
      ResetUsedTabs=.true.
      nRFacProf=0
      RFacProf=0.
      cRFacProf=0.
      wRFacProf=0.
      wcRFacProf=0.
      eRFacProf=0.
2000  ICyklMod6=mod(icykl,6)+1
      CalcDer=icykl.ne.ncykl
      okraj=icykl.eq.0.or..not.CalcDer
      if(okraj) then
        Veta=fln(:ifln)//'.sflog'
        if(ExistFile(Veta)) call DeleteFile(Veta)
      endif
      DelejStatistiku=istat.eq.1.and.
     1                (.not.CalcDer.or.(okraj.and.iabs(nowr).eq.1))
      if(.not.CalcDer) then
        t80=fln(:ifln)//'.m80'
        call OpenFile(80,t80,'formatted','unknown')
        if(ErrFlag.ne.0) then
          call DeleteFile(t80)
          call OpenFile(80,t80,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9900
        endif
        call OpenFile(83,fln(:ifln)//'.m83','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        if(ExistPowder) then
          if(LnPrf.eq.0) LnPrf=NextLogicNumber()
          call OpenFile(LnPrf,fln(:ifln)//'.prf','formatted','unknown')
          if(ErrFlag.ne.0) go to 9900
        endif
      else
        LnPrf=0
      endif
      if(MagneticType(KPhase).ne.0) call SetMag(0)
      ich=0
      if(DatBlockDerTest.le.0) then
        rewind LnSum
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,'(''# Refine'')')
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,FormA)
        if(Radiation(1).eq.NeutronRadiation) then
          write(Cislo,'(f15.3)') F000(KPhase,2)
        else
           write(Cislo,'(f15.0)') F000(KPhase,1)
        endif
        call ZdrcniCisla(Cislo,1)
        write(LnSum,FormCIF) CifKey(14,11),Cislo(:idel(Cislo))
        t80='weight:'
        if(ExistPowder) then
          write(LnSum,FormCIF) CifKey(36,15),'sigma'
        else
          if(iwq.eq.0) then
            t80(10:)='sigma'
            if(MagPolFlag(KDatBlock).eq.0) then
              if(UseSigStat.eq.0) then
                blk=blkoef
                Znak=1.
              else
                blk=abs(EM9Instab(KDatBlock))
                if(EM9Instab(KDatBlock).gt.0) then
                  Znak=1.
                else
                  Znak=-1.
                endif
              endif
              if(blk.ne.0.) then
                if(ifsq.ne.1) then
                  write(Cislo,'(f14.10)') znak*blkoef**2
                  call ZdrcniCisla(Cislo,1)
                  if(Cislo(1:1).ne.'-') Cislo='+'//Cislo
                  t80(16:)='''w=1/('//ObrLom//'s^2^(F)'//
     1                      Cislo(:idel(Cislo))//'F^2^)'''
                else
                  write(Cislo,'(f14.10)') znak*(2.*blkoef)**2
                  call ZdrcniCisla(Cislo,1)
                  if(Cislo(1:1).ne.'-') Cislo='+'//Cislo
                  t80(16:)='''w=1/('//ObrLom//'s^2^(I)'//
     1                      Cislo(:idel(Cislo))//'I^2^)'''
                endif
              else
                if(ifsq.ne.1) then
                  t80(16:)='''w=1/'//ObrLom//'s^2^(F)'''
                else
                  t80(16:)='''w=1/'//ObrLom//'s^2^(I)'''
                endif
              endif
            else
              t80(16:)='''w=1/'//ObrLom//'s^2^(R)'''
            endif
          else if(iwq.eq.1) then
            t80(10:)='unit'
          else
            t80(10:)='calc'
          endif
          write(LnSum,FormCIF) CifKey(36,15),t80(10:15)
          if(t80(16:).ne.' ') then
            write(LnSum,FormCIF) CifKey(35,15),t80(16:idel(t80))
          else
            write(LnSum,FormCIF) CifKey(35,15),'?'
          endif
          t80='coef:'
          if(MagPolFlag(KDatBlock).eq.0) then
            if(ifsq.ne.1) then
              t80(8:)='F'
            else
              t80(8:)='Fsqd'
            endif
          else
            t80(8:)='K'
          endif
          write(LnSum,FormCIF) CifKey(34,15),t80(8:idel(t80))
          write(Cislo,'(f8.3)') slevel
          call ZdrcniCisla(Cislo,1)
          Cislo='''I>'//Cislo(:idel(Cislo))//ObrLom//'s(I)'''
          write(LnSum,FormCIF) CifKey(15,17),Cislo(:idel(Cislo))
          write(t80,'(''extinction:'',3i5)') ExtTensor(1),ExtType(1),
     1                                       ExtDistr(1)
          if(ExtTensor(1).eq.0) then
            Cislo='''none'''
          else if(ExtTensor(1).eq.1) then
            if(ExtType(1).eq.1) then
              t80=' ''B-C type 1'
              if(ExtDistr(1).eq.1) then
                t80=t80(1:12)//' Gaussian isotropic'
              else
                t80=t80(1:12)//' Lorentzian isotropic'
              endif
            else if(ExtType(1).eq.2) then
              t80=' ''B-C type 2'
            else
              t80=' ''B-C mixed type'
            endif
            t80=t80(:idel(t80))//' (Becker & Coppens, 1974)'''
            write(LnSum,FormCIF) CifKey(11,15)
            write(LnSum,FormA) t80(:idel(t80))
            Cislo=' '
          else if(ExtTensor(1).eq.2) then
            Cislo='?'
          endif
          if(Cislo.ne.' ')
     1      write(LnSum,FormCIF) CifKey(11,15),Cislo(:idel(Cislo))
        endif
        write(Cislo,FormI15) NConstrain
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(18,15),Cislo(:idel(Cislo))
        write(Cislo,FormI15) NRestrain
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(21,15),Cislo(:idel(Cislo))
      endif
3000  call RefLoopNuluj(NRefAll+NPntsAll)
      if(ErrFlag.ne.0) go to 9990
      if(ipisd.ne.0) then
        call CloseIfOpened(88)
        call OpenFile(88,fln(:ifln)//'.m88','formatted','unknown')
        if(ErrFlag.ne.0) go to 9990
      else
        call DeleteFile(fln(:ifln)//'.m88')
      endif
      RNumOverall=0.
      RDenOverall=0.
      RNumOverallObs=0.
      RDenOverallObs=0.
      wRNumOverall=0.
      wRDenOverall=0.
      wRNumOverallObs=0.
      wRDenOverallObs=0.
      nRFacOverall=0
      nRFacOverallObs=0
      cRDenOverall=0.
      wcRDenOverall=0.
      wdyOld=0.
      wdyqOld=0.
      BerarCorr=0.
      BerarS=0.
      itw=1
      NTwinIn=NTwin
      IOverIn=IOver
      do KDatBlock=1,NDatBlock
        if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1     ExistM42.and.CalcDyn) then
          if(ActionED.ne.1.and.ActionDatBlock.ne.KDatBlock) cycle
        endif
        if(DatBlockDerTest.le.0) then
          if(.not.UseDatBlockActual(KDatBlock)) cycle
        else
          if(KDatBlock.ne.DatBlockDerTest) cycle
        endif
        call RefRFacNuluj
        if(KDatBlock.gt.1.and.OrthoOrd.gt.0) call trortho(0)
        isPowder=iabs(DataType(KDatBlock)).eq.2
        call PwdSetTOF(KDatBlock)
        if(isTOF) then
          SinTOFThUse=sin(.5*TOFTTh(KDatBlock)*ToRad)
        else
          LPTypeCorrUse=LpFactor(KDatBlock)
          FractPerfMonUse=FractPerfMon(KDatBlock)
          if(LPTypeCorrUse.eq.PolarizedGuinier) then
            snm=sin(AlphaGMon(KDatBlock)*ToRad)
            BetaGMonUse=BetaGMon(KDatBlock)*ToRad
          else
            snm=sin(AngleMon(KDatBlock)*ToRad)
          endif
          Cos2ThMon =1.-2.*snm**2
          Cos2ThMonQ=Cos2ThMon**2
        endif
        if(.not.CalcDer) then
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            k=1
          else
            k=2
          endif
          if(NDatBlock.gt.1)
     1       write(80,100)
     2         DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     3         'begin'
          do KPh=1,NPhase
            write(80,format80)(0,i=1,maxNDim),KPh,(F000(KPh,k),i=1,3),
     1                        (F000(KPh,k),F000N(KPh,k),i=1,4),0.,0.
          enddo
        endif
        KDatBlockUsed=KDatBlock
        if(CalcDer) then
          call SetRealArrayTo(der,LastNp,0.)
          call SetRealArrayTo(dc ,LastNp,0.)
        endif
        KPhase=1
        if(isPowder) then
          NTwin=1
          iover=0
          if(isimul.le.0) then
            call PwdM92Nacti
          else
            scPwd(KDatBlock)=sc(1,KDatBlock)
          endif
          if(ErrFlag.ne.0) go to 9990
          if(icykl.ne.0.and.(NAtCalc.le.0.or.DoLeBail.ne.0).and.
     1       NleBail.gt.0.and.isimul.le.0) then
            if(mod(icykl,nlebail).eq.0) then
              call PwdLeBail(3,.true.)
              if(ErrFlag.ne.0) go to 9900
            endif
          endif
          if(DoLeBail.ne.0) then
            pom=sc(1,KDatBlock)
            sc(1,KDatBlock)=scPwd(KDatBlock)
            scPwd(KDatBlock)=pom
            j=0
            do KDatB=1,NDatBlock
              do k=1,MxScAll
                j=j+1
                if(k.gt.2) then
                  if(ki(j).ne.0) then
                    nLSRS=nLSRS-1
                    ki(j)=0
                  endif
                endif
              enddo
            enddo
            nLSMat=nLSRS*(nLSRS+1)/2
          endif
          call RefReadRefPwd(ich)
          if(ich.ne.0) go to 9900
          call RefLoopPwd(RefineEnd,lst0,ich)
        else if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1          ExistM42.and.CalcDyn) then
          Veta=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
          call OpenFile(91,Veta,'formatted','unknown')
          do ik=1,2
            NRefED=0
            rewind 91
1350        read(91,Format91,end=1400,err=9900)(IHRead(i),i=1,maxNDim)
            if(IHRead(1).gt.900) go to 1400
            NRefED=NRefED+1
            if(ik.eq.2) then
              call CopyVekI(IHRead,IHED(1,NRefED),maxNDim)
              call FromIndSinthl(ihread,xp,SinThLED(NRefED),sinthlq,1,
     1                           0)
            endif
            go to 1350
1400        if(ik.eq.1) then
              if(allocated(IHED)) deallocate(IHED,FlagED,ADerED,BDerED,
     1                                       IDerED,ACalcED,BCalcED,
     2                                       SinThLED,IObsED,SIObsED,
     3                                       FCalcED,ExcitErrED)
              allocate(IHED(maxNDim,NRefED),FlagED(NRefED),
     1                 ACalcED(NRefED),BCalcED(NRefED),
     2                 SinThLED(NRefED),IObsED(NRefED),
     3                 SIObsED(NRefED),FCalcED(NRefED),
     4                 ADerED(nLSRS,NRefED),BDerED(nLSRS,NRefED),
     5                 IDerED(nLSRS,NRefED),ExcitErrED(NRefED),stat=i)
              if(i.ne.0) then
                n=4*(8+3*maxNDim+4*nLSRS*NRefED)
                write(Veta,FormI15)
     1            nint(float(8+3*maxNDim+4*nLSRS*NRefED)/1000000.)
                call Zhusti(Veta)
                idl=idel(Veta)
                j=129
                ven=' '
                do i=idl,1,-1
                  if(mod(idl-i,3).eq.0.and.i.ne.idl) then
                    j=j-1
                    ven(j:j)='.'
                  endif
                  j=j-1
                  ven(j:j)=Veta(i:i)
                enddo
                call Zhusti(ven)
                call FeAllocErr('Error occur in "Refine" program needs '
     1                        //'about '//ven(:idel(ven))//
     2                          ' MB memory.',i)
                if(allocated(IHED)) deallocate(IHED)
                if(allocated(FlagED)) deallocate(FlagED)
                if(allocated(ACalcED)) deallocate(ACalcED)
                if(allocated(BCalcED)) deallocate(BCalcED)
                if(allocated(SinThLED)) deallocate(SinThLED)
                if(allocated(IObsED)) deallocate(IObsED)
                if(allocated(SIObsED)) deallocate(SIObsED)
                if(allocated(FCalcED)) deallocate(FCalcED)
                if(allocated(ADerED)) deallocate(ADerED)
                if(allocated(BDerED)) deallocate(BDerED)
                if(allocated(IDerED)) deallocate(IDerED)
                if(allocated(ExcitErrED)) deallocate(ExcitErrED)
                go to 9900
              endif
              ExcitErrED=0.
            endif
          enddo
          NTwin=1
          iover=0
          call RefLoopED(RefineEnd,lst0,ich)
        else
          NTwin=NTwinIn
          IOver=IOverIn
          if(isimul.gt.0) then
            call OpenFile(91,fln(:ifln)//'.l91','formatted','old')
            if(ErrFlag.ne.0) go to 9990
          endif
          call RefLoop(RefineEnd,lst0,ich)
          if(ich.eq.5) then
            TryAgain=1
            go to 9900
          endif
        endif
        if(ich.eq.3) then
          call RefApplyDynRed(lst0,chlst,ich)
          if(ich.ne.0) go to 9900
          NDynCycle=0
          AtDisableNNew=AtDisableNOld
          go to 6000
        else if(ich.ne.0) then
          go to 9900
        endif
        call CloseIfOpened(91)
        if(NDatBlock.gt.1.and..not.CalcDer) then
          write(80,100)
     1      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     2      'end'
        endif
        if(DatBlockDerTest.gt.0) go to 9990
        if(ich.eq.1) then
          ncykl=icykl-1
          call NewPg(0)
          call pisrfac
          ncykl=icykl
          go to 7000
        else if(ich.eq.2) then
          go to 8000
        endif
      enddo
      NTwin=NTwinIn
      IOver=IOverIn
      if(Allocated(ParSup))
     1   deallocate(ParSup,snw,csw,skfx1,skfx2,skfx3,PrvniParSup,
     2              DelkaParSup,x4low,x4length,
     3              snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     4              sncospsc,cscospsc)
      i=NRefAll+NPntsAll
      if(nFlowChart.gt.0) call FeFlowChartEvent(i,ie)
      KDatBlock=KDatBlockIn
      if(isimul.gt.0) go to 8000
      if(NUseDatBlockActual.gt.1) then
        GOFOverall=wRNumOverall
        if(nRFacOverall-nParRef.gt.1) then
          GOFOverall=sqrt(GOFOverall/float(nRFacOverall-nParRef-1))
          BerarS=sqrt((BerarS+BerarCorr**2)/
     1                 float(nRFacOverall-nParRef-1))
        else
          GOFOverall=9999.99
          BerarS=9999.99
        endif
        GOFOverallObs=wRNumOverallObs
        if(nRFacOverallObs-nParRef.gt.1) then
          GOFOverallObs=sqrt(GOFOverallObs/float(nRFacOverallObs-
     1                                           nParRef-1))
        else
          GOFOverallObs=9999.99
          BerarS=9999.99
        endif
        BerarCorr=BerarS/GOFOverall
        if(RDenOverall.ne.0.) then
          RFacOverall=RNumOverall/RDenOverall*100.
          wRNumOverall=sqrt(wRNumOverall)
          wRDenOverall=sqrt(wRDenOverall)
          wRFacOverall=wRNumOverall/wRDenOverall*100.
          eRFacOverall=sqrt(float(nRFacOverall-nParRef))/wRDenOverall*
     1                 100.
        else
          RFacOverall=0.
          wRFacOverall=0.
          eRFacOverall=0.
        endif
        if(RDenOverallObs.ne.0.) then
          RFacOverallObs=RNumOverallObs/RDenOverallObs*100.
          wRNumOverallObs=sqrt(wRNumOverallObs)
          wRDenOverallObs=sqrt(wRDenOverallObs)
          wRFacOverallObs=wRNumOverallObs/wRDenOverallObs*100.
        else
          RFacOverallObs=0.
          wRFacOverallObs=0.
          eRFacOverallObs=0.
        endif
      else
        if(NPowder.gt.0) then
           RFacOverall= RFacProf(ICyklMod6,KDatBlockUsed)
          wRFacOverall=wRFacProf(ICyklMod6,KDatBlockUsed)
          eRFacOverall=eRFacProf(KDatBlockUsed)
          GOFOverall=GOFProf(ICyklMod6,KDatBlockUsed)
        else
          GOFOverall=GOFAll(ICyklMod6,KDatBlockUsed)
        endif
      endif
      call RFactorWrite
      if(KeyDyn.ne.0.and.icykl.gt.0) then
        if(nPowder.eq.0.or.nSingle.eq.0) then
          wrz=wRFacOverall
        else
          wrz=GOFOverall
        endif
        if(RLast(nRLast-1)*(1.+TolDyn*.01).lt.wrz.and.
     1     RLast(nRLast-1)+.01.lt.wrz.and.
     2     AtDisableNOld.eq.AtDisableNNew) then
          call RefApplyDynRed(lst0,chlst,ich)
          if(ich.ne.0) go to 9900
          nRLast=nRlast-1
          NDynCycle=0
          go to 6000
        endif
      endif
      AtDisableNOld=AtDisableNNew
      NDynRed=0
      if(MagneticType(KPhase).ne.0) call SetMag(1)
      if(calcder) then
        if(KeyDyn.ne.0) then
          NDynCycle=NDynCycle+1
          if(NDynCycle.gt.NDynCycleMax.and.NDynCycleMax.gt.0) then
            if(Tlum/TlumOrigin.gt..9) go to 4000
            Tlum=Tlum*RedDyn
            t80='Dampi'
            do i=1,NInfo
              j=index(TextInfo(1),t80(1:5))
              if(j.gt.0) then
                write(TextInfo(1)(j+16:),'(f8.4)') Tlum
                go to 3160
              endif
            enddo
3160        NDynCycle=0
          endif
        endif
4000    call LoopDistAng(1)
        if(ChargeDensities.and.lasmaxm.gt.0.and.iaute.eq.2)
     1    call RefPopvSuma
        call RefFixOriginSuma
        write(chlst,'(''.l'',i2)') lst+ICyklMod6
        lst=lst+1
        call OpenFile(lst,fln(:ifln)//chlst,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        call Inver(lst0)
        lst0l=lst0
        call CloseIfOpened(lst)
        lst=lst0
        if(ErrFlag.ne.0) go to 9900
        if(jensc) then
          nRLast=nRLast-1
          VisiMsgSc=.true.
          call CloseIfOpened(89)
          if(kcommenMax.gt.0) NatCalc=NAtCalcBasic
          go to 2000
        endif
      else
        if(.not.isPowder.and.nRFacOverall.le.0) then
          call FeChybne(-1.,-1.,'no reflections fulfilling selection '//
     1                  'conditions.',' ',Warning)
          go to 8000
        endif
        if(ncykl.gt.0) Ninfo=Ninfo+1
      endif
6000  if(NSing.gt.0.and.NPisSing.gt.0) then
        write(Cislo,'(''-'',i5)') NSing
        call Zhusti(Cislo)
        t80=TextInfo(NPisSing)
        i=index(t80,']')
        j=index(t80,'-')
        if(j.le.0) j=i
        if(i.gt.0)
     1    TextInfo(NPisSing)=t80(:j-1)//Cislo(:idel(Cislo))//t80(i:)
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l88','formatted','unknown')
      write(ln,'(i5)') NInfo
      write(ln,FormA)(TextInfo(i)(:idel(TextInfo(i))),i=1,NInfo)
      call CloseIfOpened(ln)
      if(VisiMsgSc) then
        call FeMsgClear(2)
        VisiMsgSc=.false.
      endif
      if(ShowInfoOnScreen) then
        if(BatchMode.or.Console) call FeLSTBackspace
        call FeMsgShow(1,-1.,YMaxBasWin-250.+float(Ninfo*10),
     1                 .not.VisiMsgRf)
        VisiMsgRf=.true.
        if(BatchMode.or.Console) call FeLstWriteLine(' ',-1)
      endif
      if(icykl.lt.ncykl) then
        icykl=icykl+1
        go to 2000
      endif
      call pisrfac
7000  if(ncykl.gt.0) then
        call NewPg(0)
        call TitulekVRamecku('Changes overview')
        if(ExistPowder.and.NDatBlock.eq.1) then
          call newln(2)
          write(lst,'(''More realisitic s.u.''''s can be achieved '',
     1                ''by applying the Berar''''s factor : '',f8.3)')
     2                BerarCorr
          t80='The correction has'
          if(CorrESD.eq.0) t80=t80(:idel(t80))//'n''t'
          t80=t80(:idel(t80))//' been applied'
          write(lst,FormA) t80(:idel(t80))
        endif
        n=0
        Ven=' '
        j=1
        do i=1,NAtCalc
          if(AtDisable(i)) then
            if(n.eq.0) then
              call newln(1)
              write(lst,'(''The following atoms where disabled as '',
     1                    ''their Uiso was too large:'')')
            endif
            n=n+1
            id=idel(atom(i))
            if(j+id.gt.len(Ven)) then
              call newln(1)
              write(lst,FormA) Ven(:idel(Ven))
              j=1
              Ven=' '
            endif
            Ven(j:)=atom(i)(:id)
            j=j+id+1
          endif
        enddo
        if(Ven.ne.' ') then
          call newln(1)
          write(lst,FormA) Ven(:idel(Ven))
        endif
        lst=lst0+1
        write(chlst,'(''.l'',i2)') 60+mod(icykl,6)+1
        call OpenFile(lst,fln(:ifln)//chlst,'formatted','unknown')
        call InverFinal(lst0)
        call CloseIfOpened(lst)
        lst=lst0
        call PisZmeny(chlst,lst0)
        if(MaxMagneticType.ne.0) then
          call TitulekVRamecku('Interpretation of magnetic parameters')
          call iom50(0,0,fln(:ifln)//'.m50')
          if(MaxNSymmMol.gt.MaxNSymm) then
            call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                       NPhase,0)
            MaxNSymm=MaxNSymmMol
          endif
          call trortho(0)
          call comsym(0,0,ich)
          if(ngcMax.gt.0.and.NAtCalc.gt.0)
     1    call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
          allocate(snwp(MaxUsedKwAll),cswp(MaxUsedKwAll),
     1             FPolA(2*MaxUsedKwAll+1))
          do KPh=1,NPhase
            KPhase=KPh
            if(MagneticType(KPh).eq.0) cycle
            if(NPhase.gt.1) call TitulekPodtrzeny('Phase: '//
     1                                            PhaseName(KPh),'-')
            call NewLn(3)
            write(lst,'(38x,''Magnetic moment in Bohr magnetons'')')
            write(Ven,'(''Atom'',23x,''along a'',8x,''along b'',8x,
     1                  ''along c'',8x,''length'')')
            if(NDimI(KPh).gt.0) then
              if(KCommen(KPh).le.0) then
                Ven(14:20)='wave'
              else
                Ven(12:20)='t section'
              endif
            endif
            write(lst,FormA) Ven
            write(lst,FormA)
            do i=1,NAtCalc
              MagParI=MagPar(i)
              MxMod=0
              if(MagParI.le.0.or.kswa(i).ne.KPh) cycle
              isw=iswa(i)
              if(KUsePolar(i).gt.0) then
                call ShpCoor2Fract(sm0(1,i),TrPom)
                do k=1,MagParI-1
                  call ShpCoor2Fract(smx(1,k,i),TrPom)
                  call ShpCoor2Fract(smy(1,k,i),TrPom)
                enddo
              endif
              if(NDimI(KPh).le.0.or.KCommen(KPh).le.0) then
                do k=1,MagParI
                  do iw=1,2
                    if(iw.eq.1) then
                      if(k.eq.1) then
                        call CopyVek( sm0(1,i),xp,3)
                        call CopyVek(ssm0(1,i),xo,3)
                      else
                        call CopyVek( smx(1,k-1,i),xp,3)
                        call CopyVek(ssmx(1,k-1,i),xo,3)
                      endif
                    else
                      if(k.eq.1) then
                        cycle
                      else
                        call CopyVek( smy(1,k-1,i),xp,3)
                        call CopyVek(ssmy(1,k-1,i),xo,3)
                      endif
                    endif
                    call MultM(TrToOrtho(1,isw,KPh),xp,sm,3,3,1)
                    pom=VecOrtLen(sm,3)
                    call MultMQ(TrToOrtho(1,isw,KPh),xo,ssm,3,3,1)
                    spom=VecOrtLen(ssm,3)
                    ven=' '
                    kk=0
                    do j=1,3
                      write(Cislo,'(''('',i5,'')'')')
     1                  nint(xo(j)*CellPar(j,isw,KPhase)*1000.)
                      call Zhusti(Cislo)
                      write(t80,'(f8.3,a)')
     1                  xp(j)*CellPar(j,isw,KPhase),Cislo
                      ven=ven(:kk)//t80
                      kk=kk+15
                    enddo
                    write(Cislo,'(''('',i5,'')'')') nint(spom*1000.)
                    call Zhusti(Cislo)
                    write(t80,'(f8.3,a)') pom,Cislo
                    ven=Atom(i)//'               '//Ven(:kk)//t80
                    if(NDimI(KPh).gt.0) then
                      if(k.eq.1) then
                        Ven(15:15)='0'
                      else
                        Ven(1:8)=' '
                        if(iw.eq.1) then
                          write(Cislo,'(''sin'',i3)') k-1
                        else
                          write(Cislo,'(''cos'',i3)') k-1
                        endif
                        call Zhusti(Cislo)
                        ven(14:23)=Cislo(:idel(Cislo))
                      endif
                    endif
                    call NewLn(1)
                    write(lst,FormA) Ven(:idel(Ven))
                  enddo
                enddo
              else
                kmodsi=KModA(1,i)
                kmodxi=KModA(2,i)
                if(MagParI.gt.0) then
                  MagParP=1
                else
                  MagParP=0
                endif
                if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
                  kfsi=KFA(1,i)
                else
                  kfsi=0
                endif
                if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
                  kfxi=KFA(2,i)
                else
                  kfxi=0
                endif
                MxMod=MagParI-1
                if(kfsi.ne.0) then
                  k=kmodsi-NDimI(KPh)
                  do j=1,NDimI(KPh)
                    k=k+1
                    x4center(j)=ax(k,i)
                    if(NDimI(KPh).gt.1) then
                      delta(j)=ay(k,i)*.5
                    else
                      delta(j)=a0(i)*.5
                    endif
                  enddo
                else if(kfxi.ne.0) then
                  x4center(1)=uy(1,kmodxi,i)
                  delta(1)=uy(2,kmodxi,i)*.5
                endif
                call CopyVekI(NCommQ(1,isw,KPh),NSupPom,3)
                do ica=1,NCommAdd(1,isw,KPh)
                  do ic3=1,NSupPom(3)
                    if(NDimI(KPh).ne.1) fc(3)=ic3-1
                    do ic2=1,NSupPom(2)
                      if(NDimI(KPh).ne.1) fc(2)=ic2-1
                      do ic1=1,NSupPom(1)
                        if(NDimI(KPh).eq.1) then
                          pom=ic1-1
                          do j=1,3
                            fc(j)=VCommQ(j,1,isw,KPh)*pom+
     1                            VCommAdd(j,1,isw,KPh)*float(ica-1)
                          enddo
                        else
                          fc(1)=ic1-1
                        endif
                        call AddVek(x(1,i),fc,xs,3)
                        call qbyx(fc,xs(4),isw)
                        tact=xs(4)
                        call AddVek(qcnt(1,i),xs(4),xs(4),NDimI(KPh))
                        call AddVek(xs(4),trez(1,isw,KPh),xs(4),
     1                              NDimI(KPh))
                        kk=1
                        do k=1,mxmod
                          if(TypeModFun(i).le.1) then
                            arg=0.
                            do n=1,NDimI(KPh)
                              arg=arg+float(kw(n,k,KPh))*xs(n+3)
                            enddo
                            arg=arg*pi2
                            snwp(k)=sin(arg)
                            cswp(k)=cos(arg)
                          else
                            if(k.eq.1) then
                              pom=xs(4)-x4center(1)
                              j=pom
                              if(pom.lt.0.) j=j-1
                              pom=pom-float(j)
                              if(pom.gt..5) pom=pom-1.
                              pom=pom/delta(1)
                              call GetFPol(pom,FPolA,2*mxmod+1,
     1                                     TypeModFun(i))
                            endif
                            kk=kk+1
                            snwp(k)=FPolA(kk)
                            kk=kk+1
                            cswp(k)=FPolA(kk)
                          endif
                        enddo
                        call CopyVek( sm0(1,i),xp,3)
                        call CopyVek(ssm0(1,i),xo,3)
                        do j=1,3
                          xo(j)=xo(j)**2
                        enddo
                        do k=1,MagParI-1
                          do j=1,3
                            xp(j)=xp(j)+smx(j,k,i)*snwp(k)
     1                                 +smy(j,k,i)*cswp(k)
                            xo(j)=xo(j)+(ssmx(j,k,i)*snwp(k))**2
     1                                 +(ssmy(j,k,i)*cswp(k))**2
                          enddo
                        enddo
                        call MultM(TrToOrtho(1,isw,KPh),xp,sm,3,3,1)
                        pom=VecOrtLen(sm,3)
                        do j=1,3
                          xo(j)=sqrt(xo(j))
                        enddo
                        call MultMQ(TrToOrtho(1,isw,KPh),xo,ssm,3,3,1)
                        spom=VecOrtLen(ssm,3)
                        ven=' '
                        k=0
                        do j=1,3
                          write(Cislo,'(''('',i5,'')'')')
     1                      nint(ssm(j)*1000.)
                          call Zhusti(Cislo)
                          write(t80,'(f8.3,a)') sm(j),Cislo
                          ven=ven(:k)//t80
                          k=k+15
                        enddo
                        write(Cislo,'(''('',i5,'')'')') nint(spom*1000.)
                        call Zhusti(Cislo)
                        write(t80,'(f8.3,a)') pom,Cislo
                        ven=Atom(i)//'               '//Ven(:k)//t80
                        write(Ven(10:20),'(f9.3)') tact
                        call NewLn(1)
                        write(lst,FormA) Ven(:idel(Ven))
                      enddo
                    enddo
                  enddo
                enddo
              endif
              if(KUsePolar(i).gt.0) then
                call Fract2ShpCoor(sm0(1,i))
                do k=1,MagParI-1
                  call Fract2ShpCoor(smx(1,k,i))
                  call Fract2ShpCoor(smy(1,k,i))
                enddo
              endif
              if(NDimI(KPh).gt.0.and.KCommen(KPh).le.0) then
                call iom50(0,0,fln(:ifln)//'.m50')
                if(MaxNSymmMol.gt.MaxNSymm) then
                  call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,
     1                             MaxNComp,NPhase,0)
                  MaxNSymm=MaxNSymmMol
                endif
              endif
            enddo
          enddo
          if(allocated(snwp)) deallocate(snwp,cswp,FPolA)
        endif
      endif
8000  call CloseIfOpened(80)
      call CloseIfOpened(83)
      if(ExistPowder) call CloseIfOpened(LnPrf)
      call FeDateAndTime(t80,timen)
      call newln(3)
      write(lst,'(//''Program started at '',a8,'' ended   at '',a8,
     1              '' cpu time : '',f9.2)') timst,timen,
     2                                       FeEtime()-TimeStart
      call CloseListing
      LstOpened=.false.
      RefineCallFourier=CallFour.eq.1.and.ExistFile(fln(:ifln)//'.m80')
      if(DatBlockDerTest.le.0) then
        if(nFlowChart.gt.0) then
          call FeFlowChartRemove
          call FeReleaseOutput
          nFlowChart=0
        endif
        if(ich.eq.1.or.ich.eq.2) go to 8500
        write(LnSum,FormA)
        do i=1,6
          if(i.eq.1) then
            Veta=fln(:ifln)//'_fix_dist.tmp'
          else if(i.eq.2) then
            Veta=fln(:ifln)//'_fix_angle.tmp'
          else if(i.eq.3) then
            Veta=fln(:ifln)//'_fix_torsion.tmp'
          else if(i.eq.4) then
            Veta=fln(:ifln)//'_equal_dist.tmp'
          else if(i.eq.5) then
            Veta=fln(:ifln)//'_equal_angle.tmp'
          else if(i.eq.6) then
            Veta=fln(:ifln)//'_equal_torsion.tmp'
          endif
          if(ExistFile(Veta)) then
            write(LnSum,102)
            if(i.eq.1) then
              do j=1,nCIFRestDist
                k=CIFRestDist(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            else if(i.eq.2) then
              do j=1,nCIFRestAngle
                k=CIFRestAngle(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            else if(i.eq.3) then
              do j=1,nCIFRestTorsion
                k=CIFRestTorsion(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            else if(i.eq.4) then
              do j=1,nCIFRestEqDist
                k=CIFRestEqDist(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            else if(i.eq.5) then
              do j=1,nCIFRestEqAngle
                k=CIFRestEqAngle(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            else if(i.eq.6) then
              do j=1,nCIFRestEqTorsion
                k=CIFRestEqTorsion(j)
                write(LnSum,FormA) ' '//CIFKey(k,24)
              enddo
            endif
            ln=NextLogicNumber()
            call OpenFile(ln,Veta,'formatted','old')
2100        read(ln,FormA,end=2200) t80
            write(lnSum,FormA) t80(:idel(t80))
            go to 2100
2200        write(LnSum,FormA)
            close(ln,status='delete')
          endif
        enddo
        close(lnsum)
        if(RefineCallFourier) then
          call iom40(0,0,fln(:ifln)//'.m40')
          call iom50(0,0,fln(:ifln)//'.m50')
          call CloseIfOpened(LnSum)
          call Fourier
          call OpenFile(LnSum,fln(:ifln)//'_Refine.l70','formatted',
     1                 'unknown')
          RefineCallFourier=.false.
          ErrFlag=0
        endif
      endif
8500  if(isimul.le.0.and.ShowInfoOnScreen.and..not.BatchMode.and.
     1   .not.Console.and..not.CyclicRefMode) then
        if(RefSeriousWarnings) then
          NInfo=1
          TextInfo(1)='Serious warnings in the listing!'
          TextInfoFlags(1)='CN'
        else
          NInfo=0
        endif
        call FeShowListing(-1.,YBottomMessage,'REFINE program',
     1                     fln(:ifln)//'.ref',10000)
        if(OpSystem.eq.1) then
          VisiMsgRf=.false.
          VisiMsgSc=.false.
        endif
      endif
      if(ExistPowder) then
        if(allocated(TrToOrthoCorr)) deallocate(TrToOrthoCorr)
        allocate(TrToOrthoCorr(9,3,NPhase))
        do KPh=1,NPhase
          do isw=1,NComp(KPh)
            call CopyMat(TrToOrtho(1,isw,KPh),
     1                   TrToOrthoCorr(1,isw,KPh),3)
          enddo
        enddo
        do im=1,NMolec
          i=(im-1)*mxp+1
          call CopyMat(TrMol(1,i),TrMolCorr(1,im),3)
        enddo
        SetMetAllowed=.true.
        call SetMet(0)
        do im=1,NMolec
          i=(im-1)*mxp+1
          KPh=kswmol(im)
          isw=iswmol(im)
          if(LocMolSystType(i).gt.0) then
            do j=1,2
              call CrlGetXFromAtString(LocMolSystSt(j,1,i),im,xp,t80,
     1                                 ich)
              call multm(TrToOrthoCorr(1,isw,KPh),xp,xo,3,3,1)
              call multm(TrToOrthoI(1,isw,KPh),xo,LocMolSystX(1,j,1,i),
     1                   3,3,1)
              write(LocMolSystSt(j,1,i),'(3f9.6)')
     1          (LocMolSystX(k,j,1,i),k=1,3)
            enddo
          endif
        enddo
        do im=1,NMolec
          i=(im-1)*mxp+1
          call Multm(TrIMol(1,i),TrMolCorr(1,im),TrPom,3,3,3)
          call CopyMat(TrPom,TrMolCorr(1,im),3)
        enddo
        iak=NAtMolFr(1,1)-1
        do im=1,NMolec
          iap=iak+1
          iak=iak+iam(im)
          call CopyVek(xm(1,im),xo,3)
          call Multm(TrMolCorr(1,im),xo,xm(1,im),3,3,1)
          do ia=iap,iak
            call CopyVek(x(1,ia),xp,3)
            call Multm(TrMolCorr(1,im),xp,x(1,ia),3,3,1)
          enddo
        enddo
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
          enddo
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
          enddo
        endif
        if(allocated(TrToOrthoCorr)) deallocate(TrToOrthoCorr)
      endif
      if(MaxNDimI.gt.0) then
        call RefUpdateOrtho
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
          enddo
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
          enddo
        endif
      endif
9900  if(VisiMsgSc) call FeMsgClear(2)
      if(VisiMsgRf) call FeMsgClear(1)
      call FeReleaseOutput
      if(nFlowChart.gt.0) call FeFlowChartRemove
      call CloseIfOpened(LstRef)
      call CloseIfOpened(88)
      call DeleteFile(fln(:ifln)//'.l99')
      call DeleteFile(fln(:ifln)//'.l75')
      if(RefineEnd.and.ich.eq.1) ErrFlag=1
9990  KPhase=KPhaseBasic
      if(NMolec.gt.0) then
        call CrlRestoreSymmetry(ISymmBasic)
        call CrlCleanSymmetry
      endif
      call DeleteFile(fln(:ifln)//'.l40')
      call DeleteFile(fln(:ifln)//'.l50')
      if(ExistPowder) then
        call DeleteFile(fln(:ifln)//'.l41')
        do i=1,NDatBlock
          write(Cislo,'(''.l'',i2)') i
          if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
          call DeleteFile(fln(:ifln)//Cislo(:idel(Cislo)))
        enddo
      endif
      if(ExistElectronData.and.Existm42.and.CalcDyn)
     1  call DeleteFile(fln(:ifln)//'.l42')
      if(KCommenMax.gt.0) call DeleteFile(PreviousM50)
      call DeleteFile(PreviousM40)
      call DeleteFile(fln(:ifln)//'.l88')
      if(MakeItPermanent.or.RefRandomize) then
        call RefOpenCommands
        if(MakeItPermanent) then
          NacetlReal(nCmdtlum)=tlumNew
          NacetlInt (nCmdncykl)=NCyklNew
          NacetlInt(nCmdLSMethod)=LSMethodNew
          NacetlReal(nCmdMarqLam)=MarqLamNew
        endif
        RefRandomize=.false.
        call RefRewriteCommands(1)
      endif
      call FeReleaseOutput
      call CloseListing
      SetMetAllowed=.true.
      KDatBlock=KDatBlockIn
      call RefDeallocate(CIFAllocated,0)
      if(ResetUsedTabs) then
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
      endif
      call CloseIfOpened(LnSum)
      call CloseIfOpened(LstSing)
      if(isimul.gt.0.and.ich.eq.0) then
        call DeleteFile(fln(:ifln)//'_Refine.l70')
        if(isPowder.and.ErrFlag.eq.0) then
          if(FeYesNo(-1.,-1.,
     1               'Do you want to start "Profile viewer"?',0))
     2      call PwdPrf(0)
        endif
      endif
      if(TryAgain.ne.0) go to 1100
      if(ExistPowder) call CloseIfOpened(LnPrf)
      LnPrf=0
      return
100   format(a,1x,a)
102   format('loop_')
104   format(a40,1x,47a1)
      end
