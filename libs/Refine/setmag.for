      subroutine SetMag(Klic)
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical :: UzMaFract = .false.
      if(Klic.lt.0) then
        UzMaFract=.false.
        go to 9999
      endif
      n=0
      do i=1,NAtCalc
        if(KUsePolar(i).le.0) cycle
        n=n+1
        m=0
        do j=1,MagPar(i)
          if(j.eq.1) then
            if(Klic.eq.0) then
              if(UzMaFract) go to 9000
              m=m+1
              call ShpCoor2Fract(sm0(1,i),RMagDer(1,m,n))
            else
              if(.not.UzMaFract) go to 9000
              call Fract2ShpCoor(sm0(1,i))
            endif
          else
            if(Klic.eq.0) then
              m=m+1
              call ShpCoor2Fract(smx(1,j-1,i),RMagDer(1,m,n))
              m=m+1
              call ShpCoor2Fract(smy(1,j-1,i),RMagDer(1,m,n))
             else
              call Fract2ShpCoor(smx(1,j-1,i))
              call Fract2ShpCoor(smy(1,j-1,i))
            endif
          endif
        enddo
      enddo
      UzMaFract=Klic.eq.0
      go to 9999
9000  if(VasekTest.ne.0) then
        if(UzMaFract) then
          call FeChybne(-1.,-1.,'chce prepinat do fract, ale ono je '//
     1                  'jiz prepnuto',' ',Warning)
        else
          call FeChybne(-1.,-1.,'chce prepinat do polar, ale ono je '//
     1                  'jiz prepnuto',' ',Warning)
        endif

      endif
9999  return
      end
