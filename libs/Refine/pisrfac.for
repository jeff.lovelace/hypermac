      subroutine PisRfac
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension mar(2)
      character*128 ven,venp,Label
      character*80 s80
      character*8  t8
      integer FixTab(8)
      integer, allocatable :: MUse(:)
      logical PwdCheckTOFInD
      data FixTab/1,9,19,28,38,47,57,66/
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l88','formatted','unknown')
      read(ln,'(i5)',err=1070,end=1070) NInfo
      read(ln,FormA,err=1070,end=1070)(TextInfo(i),i=1,NInfo)
      call CloseIfOpened(ln)
      call NewLn(NInfo+5)
      write(lst,'(/''Last screen information window:''/)')
      idl=FixTab(8)+8
      write(lst,FormA1)('-',i=1,idl)
      do i=1,NInfo
        VenP='|'
        l=1
        k=0
        do j=1,idel(TextInfo(i))
          if(TextInfo(i)(j:j).eq.Tabulator) then
            k=k+1
            if(k.ne.0) then
              l=FixTab(k)
            endif
          else
            l=l+1
            VenP(l:l)=TextInfo(i)(j:j)
          endif
        enddo
        VenP(idl:idl)='|'
        if(Venp(2:11).eq.'==========') then
          do j=2,idl-1
            if(VenP(j:j).ne.'=') VenP(j:j)='='
          enddo
        endif
        write(lst,FormA) Venp(:idl)
      enddo
      write(lst,FormA1)('-',i=1,idl)
1070  if(NRFactors.gt.0) then
        call TitulekVRamecku('Partial R-factors')
        Ven='Reflection condition'
        Ven(31:)='   Robs  wRobs   Rall  wRall  nobs  nall'
        call NewLn(1)
        write(lst,FormA) Ven(:idel(Ven))
        do i=1,NRFactors
          Ven=StRFact(i)(:idel(StRFact(i)))//' : '//
     1        StRFactIncl(i)(:idel(StRFactIncl(i)))
          if(StRFactExcl(i).ne.' ')
     1      Ven=Ven(:idel(Ven)+1)//StRFactExcl(i)(:idel(StRFactExcl))

          if(nRPartAll(i).gt.0) then
             RFacAllPom=RNumPartAll(i)/RDenPartAll(i)*100.
            wRFacAllPom=sqrt(wRNumPartAll(i))/sqrt(wRDenPartAll(i))*100.
          else
             RFacAllPom=0.
            wRFacAllPom=0.
          endif
          if(nRPartObs(i).gt.0) then
             RFacObsPom=RNumPartObs(i)/RDenPartObs(i)*100.
            wRFacObsPom=sqrt(wRNumPartObs(i))/sqrt(wRDenPartObs(i))*100.
          else
             RFacObsPom=0.
            wRFacObsPom=0.
          endif
          call RefMakeRFacString(RFacObsPom,wRFacObsPom,
     1                           RFacAllPom,wRFacAllPom,Ven(31:))
          write(Ven(idel(Ven)+1:),'(2i6)') nRPartObs(i),nRPartAll(i)
          call NewLn(1)
          write(lst,FormA) Ven(:idel(Ven))
        enddo
        call NewLn(1)
        write(lst,FormA1)('-',i=1,idel(Ven))
        Ven='Overall'
        if(nRPartAllAll.gt.0) then
           RFacAllPom=RNumPartAllAll/RDenPartAllAll*100.
          wRFacAllPom=sqrt(wRNumPartAllAll)/sqrt(wRDenPartAllAll)*100.
        else
           RFacAllPom=0.
          wRFacAllPom=0.
        endif
        if(nRPartAllObs.gt.0) then
           RFacObsPom=RNumPartAllObs/RDenPartAllObs*100.
          wRFacObsPom=sqrt(wRNumPartAllObs)/sqrt(wRDenPartAllObs)*100.
        else
           RFacObsPom=0.
          wRFacObsPom=0.
        endif
        call RefMakeRFacString(RFacObsPom,wRFacObsPom,
     1                         RFacAllPom,wRFacAllPom,Ven(31:))
        write(Ven(idel(Ven)+1:),'(2i6)') nRPartAllObs,nRPartAllAll
        call NewLn(1)
        write(lst,FormA) Ven(:idel(Ven))
      endif
      if(ExistElectronData.and.NMaxEDZone.gt.1) then
        call TitulekVRamecku('R-factors for individual zones')
        Ven=' '
        Ven(16:)='   Robs  wRobs   Rall  wRall  nobs  nall'
        call NewLn(1)
        write(lst,FormA) Ven(:idel(Ven))
        do KDatB=1,NDatBlock
          do i=1,NEDZone(KDatB)
            if(nRZoneAll(i,KDatB).le.0) cycle
            write(Cislo,'(i5)') i
            call Zhusti(Cislo)
            Ven='Zone#'//Cislo
            if(NDatBlock.gt.1) then
              write(Cislo,'(i5)') KDatB
              call Zhusti(Cislo)
              Ven=Ven(:idel(Ven))//'%'//Cislo
            endif
            if(nRZoneAll(i,KDatB).gt.0) then
               RFacAllPom=RNumZoneAll(i,KDatB)/
     1                    RDenZoneAll(i,KDatB)*100.
              wRFacAllPom=sqrt(wRNumZoneAll(i,KDatB))/
     1                    sqrt(wRDenZoneAll(i,KDatB))*100.
            else
               RFacAllPom=0.
              wRFacAllPom=0.
            endif
            if(nRZoneObs(i,KDatB).gt.0) then
               RFacObsPom=RNumZoneObs(i,KDatB)/
     1                    RDenZoneObs(i,KDatB)*100.
              wRFacObsPom=sqrt(wRNumZoneObs(i,KDatB))/
     1                    sqrt(wRDenZoneObs(i,KDatB))*100.
            else
               RFacObsPom=0.
              wRFacObsPom=0.
            endif
            call RefMakeRFacString(RFacObsPom,wRFacObsPom,
     1                             RFacAllPom,wRFacAllPom,Ven(16:))
            write(Ven(idel(Ven)+1:),'(2i6)') nRZoneObs(i,KDatB),
     1                                       nRZoneAll(i,KDatB)
            call NewLn(1)
            write(lst,FormA) Ven(:idel(Ven))
          enddo
        enddo
      endif
      call TitulekVRamecku('R-factors overview')
      i1=max(0,ncykl-5)
      i2=ncykl
      icp=mod(i1,6)+1
      do 5000KDatB=1,NDatBlock
        if(.not.UseDatBlockActual(KDatB)) cycle
        if(NDatBlock.gt.1) then
          call NewLn(1)
          write(lst,FormA)
          call TitulekPodtrzeny(MenuDatBlock(KDatB),'-')
          call NewLn(1)
          write(lst,FormA)
        endif
        isPowder=iabs(DataType(KDatB)).eq.2
        call PwdSetTOF(KDatB)
        if(isPowder) then
          Ven='Cycle    Rp      wRp     cRp    cwRp   npnts   nppwd  '//
     1        '    np  npnts/np'
          if(NUseDatBlockActual.le.1) then
            Ven=Ven(:idel(Ven))//
     1          '    damp   gof av ch/su       max ch/su'
          endif
          call newln(i2-i1+2)
          write(lst,FormA) ven
          ic=icp
          do i=i1,i2
            n=NParStrAll(KDatB)+NParPwdAll(KDatB)
            if(n.eq.0) then
              Cislo='   -------'
            else
              write(Cislo,'(f10.3)') nRFacProf(KDatB)/float(n)
            endif
            write(Ven,100) i,RFacProf(ic,KDatB),wRFacProf(ic,KDatB),
     1                     cRFacProf(ic,KDatB),wcRFacProf(ic,KDatB),
     2                     nRFacProf(KDatB),NParPwdAll(KDatB),n,
     3                     Cislo(:10)
            if(NUseDatBlockActual.le.1.and.i.lt.i2) then
              write(Ven(idel(Ven)+1:),105) DampFac(ic),
     1          GOFProf(ic,KDatB),ChngSUAve(ic),ChngSUMax(ic)
              Ven(idel(Ven)+2:)=WhatHasMaxChange(ic)
            endif
            write(lst,FormA) ven
            ic=mod(ic,6)+1
          enddo
        endif
        if(isPowder.and.(DoLeBail.ne.0.or.NAtCalc.le.0)) go to 5000
        if(isPowder) then
          Label='Cycle  RFobs RFwobs  RFall RFwall  nobs  nall'
          call newln(1)
          write(lst,FormA)
        else
          Label='Cycle   Robs  wRobs   Rall  wRall  nobs  nall'
        endif
        iLabel=idel(Label)
        Label=Label(:iLabel)//'   np nall/np'
        if(isPowder) then
          NPh=NPhase
          s80='  RBobs RBwobs  RBall RBwall'
        else
          s80=' nskip nover  damp  gofall gofobs av ch/su       max '//
     1        'ch/su'
          NPh=1
        endif
        Label=Label(:idel(Label))//s80(:idel(s80))
        do 4000KPh=1,NPh
          KPhase=KPh
          if(NPh.gt.1) then
            call newln(1)
            write(lst,FormA)
            if(NPh.gt.1) call TitulekPodtrzeny(PhaseName(KPh),'=')
          endif
          call newln(i2-i1+2)
          write(lst,FormA) Label
          ic=icp
          do i=i1,i2
            nRFacAllP=nRFacAll(1,ic,KPh,KDatB)
            call RefMakeRFacString(RFacObs(1,ic,KPh,KDatB),
     1                            wRFacObs(1,ic,KPh,KDatB),
     2                             RFacAll(1,ic,KPh,KDatB),
     3                            wRFacAll(1,ic,KPh,KDatB),venp)
            if(isPowder) then
              if(nParStr(KPh).gt.0) then
                write(t8,'(f8.2)')
     1            float(nRFacAllP)/float(nParStr(KPh))
              else
                t8=' -----'
              endif
              write(ven,101) i,venp(:28),nRFacObs(1,ic,KPh,KDatB),
     1                                   nRFacAll(1,ic,KPh,KDatB),
     2                                   nParStr(KPh),t8
            else
              if(nParRef.gt.0) then
                write(t8,'(f8.2)')
     1            float(nRFacAllP)/float(nParRef)
              else
                t8=' -----'
              endif
              write(ven,101) i,venp(:28),nRFacObs(1,ic,KPh,KDatB),
     1                                   nRFacAll(1,ic,KPh,KDatB),
     2                                   nParRef,t8
            endif
            if(isPowder) then
              call RefMakeRFacString(RIFacObs(ic,KPh,KDatB),
     1                              wRIFacObs(ic,KPh,KDatB),
     2                               RIFacAll(ic,KPh,KDatB),
     3                              wRIFacAll(ic,KPh,KDatB),
     4                              ven(idel(ven)+1:))
            else if(.not.isPowder.and.i.lt.i2) then
              write(ven(idel(Ven)+1:),'(2i6)') NSkipRef(ic,KDatB),
     1                                         NBadOverRef(ic,KDatB)
              write(ven(idel(Ven)+1:),104)
     1          DampFac(ic),GOFObs(ic,KDatB),GOFAll(ic,KDatB),
     2          ChngSUAve(ic),ChngSUMax(ic)
              Ven(idel(Ven)+2:)=WhatHasMaxChange(ic)
            endif
            write(lst,FormA) Ven
            ic=mod(ic,6)+1
          enddo
          if(NDimI(KPhase).le.0.and.MagneticRFac(KPhase).le.0)
     1      go to 4000
          if(allocated(MUse)) deallocate(MUse)
          allocate(MUse(1:Maxmmabsm))
          NMuse=0
          NUse=0
          if(NComp(KPhase).gt.1) then
            km=2
          else
            km=1
          endif
          Label=Label(:iLabel)
          Label(66:)=Label(:iLabel)
          do k=1,km
            if(k.eq.1) then
              i1=max(0,ncykl-5)
              i2=ncykl
              m1=0
              do 1100m=2,Maxmmabsm
                ic=mod(i1,6)+1
                do i=i1,i2
                  if(nRFacAll(m,ic,KPh,KDatB).gt.0) then
                    NMuse=NMuse+1
                    if(m1.eq.0) m1=NMuse
                    m2=NMuse
                    MUse(NMuse)=m
                    go to 1100
                  endif
                  ic=mod(ic,6)+1
                enddo
1100          continue
!              m1=2
!              m2=Maxmmabsm
            else
              if(NComp(KPhase).eq.3) then
                m1=1
                m2=7
              else
                m1=1
                m2=3
              endif
            endif
1200        if(k.eq.1) IMUse=0
            do mp=m1,m2,2
              call newln(i2-i1+4)
              if(mp.ne.m2) then
                il=128
              else
                il=65
              endif
              if(k.eq.1) then
                IMUse=IMUse+1
                mar(1)=MUse(mp)
                if(mp.eq.m2) then
                  mar(2)=0
                else
                  mar(2)=MUse(mp+1)
                endif
              else
                if(NComp(KPhase).eq.3) then
                  mar(1)=mp+21
                  mar(2)=mar(1)+1
                  if(mar(2).gt.mar(1)) mar(2)=0
                else
                  if(mp.eq.1) then
                    mar(1)=mp+21
                    mar(2)=mp+22
                  else
                    mar(1)=mp+22
                    mar(2)=0
                  endif
                endif
              endif
              Ven=' '
              n=12
              do 1600m=1,2
                if(mar(m).eq.0) go to 1600
                if(k.eq.1) then
                  if(mar(m).eq.2) then
                    if(MagneticRFac(KPhase).gt.0) then
                      ven(n:)='        nuclear reflections'
                    else
                      ven(n:)='        main reflections'
                    endif
                  else if(mar(m).ge.21) then
                    ven(n:)=' unsorted satellites'
                  else if(mar(m).gt.2) then
                    if(MagneticRFac(KPhase).gt.0) then
                      write(ven(n:),113)
                    else
                      s80='+-('
                      do j=1,NDimI(KPhase)
                        write(Cislo,FormI15) HSatGroups(j,mar(m)-2)
                        call zhusti(Cislo)
                        s80=s80(:idel(s80))//Cislo(:idel(Cislo))//','
                      enddo
                      j=idel(s80)
                      s80(j:j)=')'
                      ven(n:)='       satellites '//s80
                    endif
                  endif
                else
                  marm=mar(m)-21
                  if(marm.le.3) then
                    write(ven(n:),'(''        composite part#'',i1)')
     1                marm
                  else if(marm.le.6) then
                    call indext(marm,i,j)
                    write(ven(n:),'(''      common part#'',i1,
     1                              '' and #'',i1)') i,j
                  else
                    write(ven(n:),'(''    common part#1,#2 and #3'')')
                  endif
                endif
                n=n+65
1600          continue
              write(lst,FormA)
              write(lst,FormA) Ven
              write(lst,'(a)') Label(:il)
              ic=icp
              do i=i1,i2
                Ven=' '
                l=1
                do 1700j=1,2
                  m=mar(j)
                  if(m.eq.0) cycle
                  call RefMakeRFacString(RFacObs(m,ic,KPh,KDatB),
     1                                  wRFacObs(m,ic,KPh,KDatB),
     2                                   RFacAll(m,ic,KPh,KDatB),
     3                                  wRFacAll(m,ic,KPh,KDatB),
     4                                   venp)
                  write(ven(l:),101) i,venp(:28),
     1                               nRFacObs(m,ic,KPh,KDatB),
     2                               nRFacAll(m,ic,KPh,KDatB)
                  l=l+65
1700            continue
                write(lst,FormA) Ven
                ic=mod(ic,6)+1
              enddo
            enddo
          enddo
4000    continue
5000  continue
      if(allocated(MUse)) deallocate(MUse)
      return
100   format(i3,1x,4f8.2,3i8,a10)
101   format(i4,1x,a28,2i6,i5,a8)
104   format(f7.4,2f7.2,f9.4,f11.4)
105   format(f7.4, f7.2,f9.4,f11.4)
113   format(5x,'magnetic reflections ')
      end
