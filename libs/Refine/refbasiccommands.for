      subroutine RefBasicCommands
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension nCrwBasic(8),nCmdBasic(8)
      character*256 EdwStringQuest
      character*80 Veta
      logical CrwLogicQuest,lpom
      integer ConvChkOld,WeightOld,CheckADP,CheckADPOld,DoLeBailOld,
     1        EdwStateQuest
      real :: UisoLimOld=.2
      save nEdwCycles,nEdwDamp,nEdwMarq,nCrwBasic,nCmdBasic,
     1     nLblDumpBack,nCrwMethod,LSMethodOld,ik,nCrwSigma,nCrwUnit,
     2     nCrwDyn,nEdwInstability,WeightOld,nEdwTolDyn,nEdwRedDyn,
     3     KeyDynOld,nEdwCyclesDyn,nCrwConvChk,nEdwConvLim,
     4     nEdwConvCycl,nLblIn,ConvChkOld,nCrwCheckADP,nEdwUisoLim,
     5     CheckADP,CheckADPOld,nCrwCorrESD,nEdwleBail,nCrwDoLeBail,
     6     DoLeBailOld,nEdwMmax,nEdwTwDiff,nEdwTwMax,nEdwRandomSeed,
     7     nEdwRandomDiff,nCrwOnlyMag,nCrwWilsonMod,nCrwInstabStat
      entry RefBasicCommandsMake(id)
      LSMethodOld=-1
      WeightOld=-1
      KeyDynOld=-1
      ConvChkOld=-1
      CheckADPOld=-1
      DoLeBailOld=-1
      xqd=XdQuestRef-2.*KartSidePruh
      il=1
      tpom=5.
      Veta='%Number of cycles'
      dpom=50.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCycles=EdwLastMade
      call FeQuestIntEdwOpen(nEdwCycles,NacetlInt(nCmdncykl),.false.)
      call FeQuestEudOpen(nEdwCycles,0,9999,1,dpom,dpom,dpom)
      il=il+1
      Veta='Damping factor'
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDamp=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdtlum),.false.,
     1                        .false.)
      xpom=xpom+dpom+40.
      tpom=xpom+CrwgXd+10.
      il=0
      ichk=1
      igrp=0
      il=il+1
      Veta='Use Mar%quart technique'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                    ichk,igrp)
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdLSMethod).eq.2)
      nCrwMethod=CrwLastMade
      il=il+1
      tpom=xpom+dpom+10.
      Veta='Fud%ge factor'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMarq=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdMarqLam),
     1                        .false.,.false.)
      xpom=tpom+FeTxLengthUnder(Veta)+50.
      tpom=xpom+CrwgXd+10.
      Veta='Sigma weig%ht'
      il=1
      igrp=1
      do i=0,1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      igrp)
        if(i.eq.0) then
          Veta='Unit %weight'
          nCrwSigma=CrwLastMade
        else if(i.eq.2) then
          nCrwUnit=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdiwq))
        if(i.eq.0) il=-34
      enddo
      il=il-2
      tpomp=tpom+FeTxLengthUnder(Veta)+30.
      Veta='Instability fact%or'
      xpomp=tpomp+FeTxLengthUnder(Veta)+10.
      il=-10
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwInstability=EdwLastMade
      il=il-8
      xpom=xpom+25.
      tpom=tpom+25.
      Veta='Instability factor from reflection statistics'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdUseSigStat).gt.0)
      if(EM9Instab(KDatBlock).lt.-900.) then
        call FeQuestCrwDisable(CrwLastMade)
        NacetlInt(nCmdUseSigStat)=0
      endif
      nCrwInstabStat=CrwLastMade
      il=il-8
      Veta='Use Wilson''s modification'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwWilsonMod=CrwLastMade
      il=4
      call FeQuestLinkaMake(id,il)
      il=il+1
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Use %dynamical LS method'
      ichk=1
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                    ichk,0)
      nCrwDyn=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdKeyDyn).eq.1)
      tpom=tpom+FeTxLengthUnder(Veta)+5.
      tpomp=tpom
      Veta=' => if Rw is in%creased by'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwTolDyn=EdwLastMade
      tpom=xpom+dpom+5.
      Veta='reduce the damping by a %factor'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRedDyn=EdwLastMade
      il=il+1
      tpom=tpomp+23.
      Veta='%After'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCyclesDyn=EdwLastMade
      tpom=xpom+dpom+EdwYd+10.
      Veta='cycles try to enlarge it back.'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblDumpBack=LblLastMade
      call FeQuestLblOff(LblLastMade)
      xpom=xpom+dpom+13.
      il=il+1
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Check for con%vergence'
      ichk=1
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,ichk,
     1                    0)
      nCrwConvChk=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdConvChk).eq.1)
      tpom=tpomp
      Veta=' => stop if max(change/s.u.)%<'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwConvLim=EdwLastMade
      xpom=xpom+dpom+13.
      Veta='in'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblIn=LblLastMade
      xpom=xpom+FeTxLengthUnder(Veta)+10.
      tpom=xpom+dpom+EdwYd+10.
      Veta='cons%ecutive cycles.'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwConvCycl=EdwLastMade
      il=il+1
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Disa%ble atoms having too large isotropic ADP parameter'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwCheckADP=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlReal(nCmdUisoLim).gt.0.)
      if(NacetlReal(nCmdUisoLim).gt.0.) then
        CheckADP=1
      else
        CheckADP=0
      endif
      tpom=tpom+FeTxLengthUnder(Veta)+10.
      Veta=' => AD%P(iso) limit for disabling'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwUisoLim=EdwLastMade
      UisoLimOld=NacetlReal(nCmdUisoLim)
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=7.
      tpom=xpom+CrwXd+10.
      ilp=il
      ik=8
      ichk=0
      il=-10*il
      Veta='Automatic refinement %keys'
      do i=1,ik
        il=il-8
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      ichk,0)
        nCrwBasic(i)=CrwLastMade
        if(i.eq.1) then
          nCmdBasic(i)=nCmdiautk
          Veta='Automatic %symmetry restrictions'
        else if(i.eq.2) then
          nCmdBasic(i)=nCmdiauts
          Veta='Refinements on F(obs)**%2'
        else if(i.eq.3) then
          nCmdBasic(i)=nCmdifsq
          Veta='Appl%y electroneutrality'
        else if(i.eq.4) then
          nCmdBasic(i)=nCmdiaute
          Veta='Simu%lation run'
        else if(i.eq.5) then
          nCmdBasic(i)=nCmdisimul
          Veta='After last cycle call Fou%rier'
        else if(i.eq.6) then
          nCmdBasic(i)=nCmdCallFour
          Veta='Correct for la%mbda/2 effect'
        else if(i.eq.7) then
          nCmdBasic(i)=nCmdLam2Corr
          Veta='Randomi%ze atomic coordinates'
          tpomp=tpom+FeTxLengthUnder(Veta)+15.
          ichk=1
        else if(i.eq.8) then
          Veta='Random seed'
          xpomp=tpomp+FeTxLengthUnder(Veta)+10.
          dpom=60.
          call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,
     1                        0)
          call FeQuestIntEdwOpen(EdwLastMade,RefRandomSeed,.false.)
          nEdwRandomSeed=EdwLastMade
          tpomp=xpomp+dpom+15.
          Veta='Maximal random displacement in Ang'
          xpomp=tpomp+FeTxLengthUnder(Veta)+10.
          call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,
     1                        0)
          call FeQuestRealEdwOpen(EdwLastMade,RefRandomDiff,.false.,
     1                            .false.)
          nEdwRandomDiff=EdwLastMade
          nCmdBasic(i)=nCmdRandomize
        endif
        if(i.le.7) then
          lpom=NacetlInt(nCmdBasic(i)).eq.1
        else
          lpom=RefRandomize
        endif
        call FeQuestCrwOpen(CrwLastMade,lpom)
        if((nCmdBasic(i).eq.nCmdisimul.and.NAtCalc.le.0).or.
     1     (nCmdBasic(i).eq.nCmdiaute.and..not.ChargeDensities).or.
     2     (nCmdBasic(i).eq.nCmdLam2Corr.and.
     3      (.not.ExistSingle.or..not.ExistXRayData))) then
          call FeQuestCrwDisable(CrwLastMade)
          nCmdBasic(i)=0
        endif
        if(i.eq.8) then
          Veta='Waring: the randomize procedure will apllied just '//
     1         'once during the next run'
          il=il-8
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        endif
      enddo
      il=-10*ilp
      nEdwMmax=0
      nEdwTwDiff=0
      nEdwTwMax=0
      if(ExistPowder) then
        il=il-8
        xpom=xqd*.5-20.
        tpom=xpom+CrwXd+10.
        Veta='Apply Berar%''s correction'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwCorrESD=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdCorrESD).eq.1)
        il=il-8
        Veta='Make only profile ma%tching (le Bail refinment)'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                      0)
        nCrwDoLeBail=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdDoLeBail).eq.1)
        if(NAtCalc.le.0) call FeQuestCrwDisable(CrwLastMade)
        il=il-8
        Veta='Freq%uency of le Bail decomposition'
        tpom=xpom+dpom+EdwYd+10.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwleBail=EdwLastMade
        call FeQuestIntEdwOpen(nEdwleBail,NacetlInt(nCmdnlebail),
     1                         .false.)
        call FeQuestEudOpen(nEdwleBail,0,9999,1,pom,pom,pom)
      endif
      if(ExistSingle.and.NTwin.gt.1) then
        xpom=xqd*.5-20.
        tpom=xpom+dpom+EdwYd+10.
        if(NDim(KPhase).gt.3) then
          il=il-8
          call FeQuestEdwMake(id,tpom,il,xpom,il,'Maximal satellite '//
     1                        'index for %twin check','L',dpom,EdwYd,0)
          nEdwMmax=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdmmaxtw),
     1                           .false.)
        endif
        il=il-9
        call FeQuestEdwMake(id,tpom,il,xpom,il,'Ma%ximal angular '//
     1                      'difference for twin overlap','L',dpom,
     2                      EdwYd,0)
        nEdwTwDiff=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdtwdiff),
     1                          .false.,.false.)
        il=il-9
        call FeQuestEdwMake(id,tpom,il,xpom,il,'M%inimal angular '//
     1                      'difference for full separation','L',dpom,
     2                      EdwYd,0)
        nEdwTwMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdTwMax),
     1                          .false.,.false.)
      endif
      if(MaxMagneticType.gt.0) then
        xpom=xqd*.5-20.
        tpom=xpom+CrwXd+10.
        il=il-8
        Veta='Calculate only magnetic scattering'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwOnlyMag=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdOnlyMag).eq.1)
      else
        nCrwOnlyMag=0
      endif
      go to 2500
      entry RefBasicCommandsCheck
      if(CrwLogicQuest(nCrwMethod)) then
        NacetlInt(nCmdLSMethod)=2
      else
        NacetlInt(nCmdLSMethod)=1
      endif
      if(CrwLogicQuest(nCrwDyn)) then
        NacetlInt(nCmdKeyDyn)=1
      else
        NacetlInt(nCmdKeyDyn)=0
      endif
      if(CrwLogicQuest(nCrwSigma)) then
        NacetlInt(nCmdiwq)=0
      else
        NacetlInt(nCmdiwq)=1
      endif
      if(CrwLogicQuest(nCrwConvChk)) then
        NacetlInt(nCmdConvChk)=1
      else
        NacetlInt(nCmdConvChk)=0
      endif
      if(CrwLogicQuest(nCrwCheckADP)) then
        CheckADP=1
      else
        CheckADP=0
      endif
      if(NAtCalc.gt.0.and.nCrwDoLeBail.gt.0) then
        if(CrwLogicQuest(nCrwDoLeBail)) then
          NacetlInt(nCmdDoLeBail)=1
        else
          NacetlInt(nCmdDoLeBail)=0
        endif
      endif
      RefRandomize=CrwLogicQuest(nCrwBasic(8))
2500  if(RefRandomize) then
        if(EdwStateQuest(nEdwRandomSeed).ne.EdwOpened) then
          call FeQuestIntEdwOpen(nEdwRandomSeed,RefRandomSeed,.false.)
          call FeQuestRealEdwOpen(nEdwRandomDiff,RefRandomDiff,.false.,
     1                            .false.)
        endif
      else
        if(EdwStateQuest(nEdwRandomSeed).eq.EdwOpened) then
          call FeQuestEdwDisable(nEdwRandomSeed)
          call FeQuestEdwDisable(nEdwRandomDiff)
        endif
      endif
      if(EdwStateQuest(nEdwInstability).eq.EdwOpened)
     1   call FeQuestRealFromEdw(nEdwInstability,
     2                           NacetlReal(nCmdblkoef))
      if(NacetlInt(nCmdiwq).ne.WeightOld) then
        if(NacetlInt(nCmdiwq).eq.0) then
          call FeQuestRealEdwOpen(nEdwInstability,
     1      NacetlReal(nCmdblkoef),.false.,.false.)
          if(EM9Instab(KDatBlock).gt.-900.)
     1      call FeQuestCrwOpen(nCrwInstabStat,
     2                          NacetlInt(nCmdUseSigStat).eq.1)
          call FeQuestCrwOpen(nCrwWilsonMod,
     1                        NacetlInt(nCmdWilsonMod).eq.1)
        else
          if(EdwStateQuest(nEdwInstability).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdwInstability,
     2                              NacetlReal(nEdwInstability))
          call FeQuestEdwDisable(nEdwInstability)
          if(EM9Instab(KDatBlock).gt.-900.)  then
            if(CrwLogicQuest(nCrwInstabStat)) then
              NacetlInt(nCmdUseSigStat)=1
            else
              NacetlInt(nCmdUseSigStat)=0
            endif
            call FeQuestCrwDisable(nCrwInstabStat)
          endif
          if(CrwLogicQuest(nCrwWilsonMod)) then
            NacetlInt(nCmdWilsonMod)=1
          else
            NacetlInt(nCmdWilsonMod)=0
          endif
          call FeQuestCrwDisable(nCrwWilsonMod)
        endif
        WeightOld=NacetlInt(nCmdiwq)
      endif
      if(NacetlInt(nCmdiwq).eq.0) then
        if(CrwLogicQuest(nCrwInstabStat)) then
          if(EdwStateQuest(nEdwInstability).eq.EdwOpened)
     1       call FeQuestRealFromEdw(nEdwInstability,
     2                               NacetlReal(nCmdblkoef))
          call FeQuestEdwDisable(nEdwInstability)
        else
          if(EdwStateQuest(nEdwInstability).ne.EdwOpened)
     1       call FeQuestRealEdwOpen(nEdwInstability,
     1         NacetlReal(nCmdblkoef),.false.,.false.)
        endif
      endif
      if(NacetlInt(nCmdKeyDyn).ne.KeyDynOld) then
        if(NacetlInt(nCmdKeyDyn).eq.1) then
          write(Cislo,'(f10.2)') NacetlReal(nCmdTolDyn)
          call ZdrcniCisla(Cislo,1)
          Cislo=Cislo(:idel(Cislo))//'%'
          call FeQuestStringEdwOpen(nEdwTolDyn,Cislo)
          call FeQuestRealEdwOpen(nEdwRedDyn,NacetlReal(nCmdRedDyn),
     1                            .false.,.false.)
          call FeQuestIntEdwOpen(nEdwCyclesDyn,
     1                           NacetlInt(nCmdNDynCycleMax),
     2                           .false.)
          call FeQuestEudOpen(nEdwCyclesDyn,0,20,1,0.,0.,0.)
          call FeQuestLblOn(nLblDumpBack)
        else
          call FeQuestEdwDisable(nEdwTolDyn)
          call FeQuestEdwDisable(nEdwRedDyn)
          call FeQuestEdwDisable(nEdwCyclesDyn)
          call FeQuestLblDisable(nLblDumpBack)
        endif
        KeyDynOld=NacetlInt(nCmdKeyDyn)
      endif
      if(CheckADP.ne.CheckADPOld) then
        if(CheckADP.eq.1) then
          if(UisoLimOld.le.0.) UisoLimOld=DefRealRefine(nCmdUisoLim)
          call FeQuestRealEdwOpen(nEdwUisoLim,UisoLimOld,.false.,
     1                            .false.)
        else
          call FeQuestRealFromEdw(nEdwUisoLim,UisoLimOld)
          call FeQuestEdwDisable(nEdwUisoLim)
        endif
      endif
      if(NacetlInt(nCmdConvChk).ne.ConvChkOld) then
        if(NacetlInt(nCmdConvChk).eq.1) then
          call FeQuestRealEdwOpen(nEdwConvLim,NacetlReal(nCmdConvLim),
     1                            .false.,.false.)
          call FeQuestLblOn(nLblIn)
          call FeQuestIntEdwOpen(nEdwConvCycl,NacetlInt(nCmdConvCycl),
     1                           .false.)
          call FeQuestEudOpen(nEdwConvCycl,0,9999,1,xpom,xpom,xpom)
        else
          call FeQuestEdwDisable(nEdwConvLim)
          call FeQuestLblDisable(nLblIn)
          call FeQuestEdwDisable(nEdwConvCycl)
        endif
        ConvChkOld=NacetlInt(nCmdConvChk)
      endif
      if(NacetlInt(nCmdLsMethod).eq.2) then
        call FeQuestRealEdwOpen(nEdwMarq,NacetlReal(nCmdMarqLam),
     1                          .false.,.false.)
      else
        call FeQuestRealFromEdw(nEdwMarq,NacetlReal(nCmdMarqLam))
        call FeQuestEdwDisable(nEdwMarq)
      endif
      if(ExistPowder) then
        if(NacetlInt(nCmdDoLeBail).ne.DoLeBailOld) then
          if(NAtCalc.le.0.or.NacetlInt(nCmdDoLeBail).ne.0) then
            if(EdwStateQuest(nEdwleBail).ne.EdwOpened) then
              call FeQuestIntEdwOpen(nEdwleBail,NacetlInt(nCmdnlebail),
     1                               .false.)
              call FeQuestEudOpen(nEdwleBail,0,9999,1,pom,pom,pom)
            endif
          else
            call FeQuestEdwDisable(nEdwleBail)
          endif
        endif
        DoLeBailOld=NacetlInt(nCmdDoLeBail)
      endif
      go to 9999
      entry RefBasicCommandsUpdate
      do i=1,7
        j=nCrwBasic(i)
        k=nCmdBasic(i)
        if(j.ne.0.and.k.ne.0) then
          if(CrwLogicQuest(j)) then
            NacetlInt(k)=1
          else
            NacetlInt(k)=0
          endif
        endif
      enddo
      if(CrwLogicQuest(nCrwBasic(8))) then
        RefRandomize=.true.
        call FeQuestIntFromEdw(nEdwRandomSeed,RefRandomSeed)
        call FeQuestRealFromEdw(nEdwRandomDiff,RefRandomDiff)
      endif
      if(ExistPowder) then
        if(CrwLogicQuest(nCrwCorrESD)) then
          NacetlInt(nCmdCorrESD)=1
        else
          NacetlInt(nCmdCorrESD)=0
        endif
        if(NAtCalc.gt.0) then
          if(CrwLogicQuest(nCrwDoLeBail)) then
            NacetlInt(nCmdDoLeBail)=1
          else
            NacetlInt(nCmdDoLeBail)=0
          endif
        endif
        if(EdwStateQuest(nEdwleBail).eq.EdwOpened)
     1    call FeQuestIntFromEdw(nEdwleBail,NacetlInt(nCmdnlebail))
      endif
      call FeQuestIntFromEdw(nEdwCycles,NacetlInt(nCmdncykl))
      call FeQuestRealFromEdw(nEdwDamp,NacetlReal(nCmdtlum))
      if(NacetlInt(nCmdLSMethod).eq.2)
     1  call FeQuestRealFromEdw(nEdwMarq,NacetlReal(nCmdMarqLam))
      if(NacetlInt(nCmdKeyDyn).eq.1) then
        Cislo=EdwStringQuest(nEdwTolDyn)
        i=index(Cislo,'%')
        if(i.ne.0) Cislo(i:i)=' '
        read(Cislo,'(f15.0)',err=3100) pom
        NacetlReal(nCmdTolDyn)=pom
3100    call FeQuestRealFromEdw(nEdwRedDyn,NacetlReal(nCmdRedDyn))
        call FeQuestIntFromEdw(nEdwCyclesDyn,
     1                         NacetlInt(nCmdNDynCycleMax))
      endif
      if(NacetlInt(nCmdConvChk).eq.1) then
        call FeQuestRealFromEdw(nEdwConvLim,NacetlReal(nCmdConvLim))
        call FeQuestIntFromEdw(nEdwConvCycl,NacetlInt(nCmdConvCycl))
      endif
      if(NacetlInt(nCmdiwq).eq.0) then
        call FeQuestRealFromEdw(nEdwInstability,NacetlReal(nCmdblkoef))
        if(CrwLogicQuest(nCrwWilsonMod)) then
          NacetlInt(nCmdWilsonMod)=1
        else
          NacetlInt(nCmdWilsonMod)=0
        endif
        if(CrwLogicQuest(nCrwInstabStat)) then
          NacetlInt(nCmdUseSigStat)=1
        else
          NacetlInt(nCmdUseSigStat)=0
        endif
      endif
      if(CheckADP.eq.1) then
        call FeQuestRealFromEdw(nEdwUisoLim,NacetlReal(nCmdUisoLim))
      else
        NacetlReal(nCmdUisoLim)=0.
      endif
      if(nEdwMmax.gt.0)
     1  call FeQuestIntFromEdw(nEdwMmax,NacetlInt(nCmdmmaxtw))
      if(nEdwTwDiff.gt.0)
     1  call FeQuestRealFromEdw(nEdwTwDiff,NacetlReal(nCmdtwdiff))
      if(nEdwTwMax.gt.0)
     1  call FeQuestRealFromEdw(nEdwTwMax,NacetlReal(nCmdTwMax))
      if(nCrwOnlyMag.gt.0) then
        if(CrwLogicQuest(nCrwOnlyMag)) then
          NacetlInt(nCmdOnlyMag)=1
        else
          NacetlInt(nCmdOnlyMag)=0
        endif
      endif
9999  return
      end
