      subroutine RefGetNParMol(m,k,MakeInOrtho,PridatQR,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical   MakeInOrtho,PridatQR
      integer TRankP
      ich=0
      PridatQR=.false.
      if(m.gt.mxdmm.or.m.eq.JeToDelta.or.m.eq.JeToX40.or.m.eq.JeToT40
     1   .or.m.eq.JeToXSlope.or.m.eq.JeToYSlope.or.m.eq.JeToZSlope) then
        if(MakeInOrtho) call TrOrtho(1)
        if(m.gt.mxdmm) then
          m=m-mxdmm
          do l=1,7
            if(l.le.2) then
              TRankP=1
              ll=1
            else if(l.le.4) then
              TRankP=3
              ll=2
            else if(l.le.6) then
              TRankP=6
              ll=3
            else if(l.le.7) then
              TRankP=9
              ll=3
            endif
            if(m.lt.CumulMol(l)) then
              m=m+2*TRankP*(KModM(ll,k)-1)
              go to 1100
            endif
          enddo
        else if(m.gt.JeToXSlope) then
          if(KFM(2,k).gt.0) then
            if(m.eq.JeToDelta) then
              m=5
            else
              PridatQR=m.eq.JeToT40
              m=4
            endif
            m=m+CumulMol(2)+2*TRank(1)*(KModM(2,k)-1)
          else if(KFM(1,k).gt.0) then
            if(m.eq.JeToDelta) then
              m=CumulMol(1)+1
            else
              PridatQR=m.eq.JeToT40
              m=CumulMol(1)+2*(KModM(1,k)-1)+2
            endif
          else
            ich=1
          endif
        else
          m=CumulMol(2)+2*TRank(1)*(KModM(2,k)-1)-m+JeToXSlope+1
        endif
1100    if(MakeInOrtho) call TrOrtho(0)
      endif
      return
      end
