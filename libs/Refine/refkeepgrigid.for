      subroutine RefKeepGRigid
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 AtName(*)
      integer IAt(*),KiAtX(*),KiAtXMod(*),Type
      integer, allocatable :: TypeRigid(:),TypeRigidO(:),ISave(:,:),
     1                        ISaveO(:,:),NSave(:),NSaveO(:),BSave(:,:),
     2                        BSaveO(:,:),MultAt(:)
      real RMAt(9,*),RM6At(36,*),RMiiAt(9,*),xi(3),xj(3),xq(3),
     1     dsym(6,*),px(6),rx(21),dr(6),xf(3,3),V21Actual(3),
     2     V31Actual(3),V21Save(3),V31Save(3),PA(9),PB(9),PC(9),
     3     RotMat(3,3),DRotMat(3,3,3)
      real, allocatable :: DSave(:,:,:),DSaveO(:,:,:),XSave(:,:,:),
     1                     XSaveO(:,:,:),RSave(:,:,:),RSaveO(:,:,:),
     2                     RISave(:,:,:),RISaveO(:,:,:),SSave(:,:,:),
     3                     SSaveO(:,:,:),XActual(:,:,:),Angles(:,:),
     4                     TrVec(:,:)
      save sfix,NMax,DSave,XSave,RSave,RISave,SSave,ISave,NSave,
     1     BSave,TypeRigid
      entry RefKeepGRigidReset
      NKeepRigidAll=0
      NMax=0
      sfix=0.001
      if(allocated(NSave))
     1  deallocate(TypeRigid,NSave,ISave,BSave,XSave,RSave,RISave,SSave,
     2             DSave)
      if(allocated(NKeepRigid)) deallocate(NKeepRigid)
      allocate(NKeepRigid(NPhase))
      NKeepRigid=0
      go to 9999
      entry RefKeepGRigidSet(Type,AtName,IAt,RMAt,RM6At,dsym,RMiiAt,
     1                       KiAtX,KiAtXMod,nn)
      n=NKeepRigidAll
      if(n.gt.0) then
        allocate(TypeRigidO(n),NSaveO(n),ISaveO(NMax,n),BSaveO(3,n),
     1           XSaveO(3,NMax,n),RSaveO(9,NMax,n),RISaveO(9,NMax,n),
     2           SSaveO(3,NMax,n),DSaveO(NMax,NMax,n))
        do i=1,n
          NSaveO(i)=NSave(i)
          TypeRigidO(i)=TypeRigid(i)
          call CopyVekI(BSave(1,i),BSaveO(1,i),3)
          do j=1,NSave(i)
            ISaveO(j,i)=ISave(j,i)
            call CopyVek(XSave(1,j,i),XSaveO(1,j,i),3)
            call CopyVek(SSave(1,j,i),SSaveO(1,j,i),3)
            call CopyVek(RSave(1,j,i),RSaveO(1,j,i),9)
            call CopyVek(RISave(1,j,i),RISaveO(1,j,i),9)
            do k=1,NSave(i)
              DSaveO(k,j,i)=DSave(k,j,i)
            enddo
          enddo
        enddo
        deallocate(TypeRigid,NSave,ISave,BSave,XSave,RSave,RISave,SSave,
     1             DSave)
      endif
      n=NKeepRigidAll+1
      m=max(NMax,nn)
      NMax=m
      allocate(TypeRigid(n),NSave(n),ISave(m,n),BSave(3,n),XSave(3,m,n),
     1         RSave(9,m,n),RISave(9,m,n),SSave(3,m,n),DSave(m,m,n))
      if(NKeepRigidAll.gt.0) then
        do i=1,NKeepRigidAll
          NSave(i)=NSaveO(i)
          TypeRigid(i)=TypeRigidO(i)
          call CopyVekI(BSaveO(1,i),BSave(1,i),3)
          do j=1,NSaveO(i)
            ISave(j,i)=ISaveO(j,i)
            call CopyVek(XSaveO(1,j,i),XSave(1,j,i),3)
            call CopyVek(SSaveO(1,j,i),SSave(1,j,i),3)
            call CopyVek(RSaveO(1,j,i),RSave(1,j,i),9)
            call CopyVek(RISaveO(1,j,i),RISave(1,j,i),9)
            do k=1,NSave(i)
              DSave(k,j,i)=DSaveO(k,j,i)
            enddo
          enddo
        enddo
        deallocate(TypeRigidO,NSaveO,ISaveO,BSaveO,XSaveO,RSaveO,
     1             RISaveO,SSaveO,DSaveO)
      endif
      NKeepRigidAll=n
      NSave(NKeepRigidAll)=nn
      TypeRigid=Type
      call SetRealArrayTo(DSave(1,1,NKeepRigidAll),NMax**2,0.)
      DMax=0.
      nnh=0
      do i=1,nn
        ia=IAt(i)
        if(i.eq.1) then
          isw=iswa(ia)
          ksw=kswa(ia)
        endif
        if(isf(ia).eq.isfh(ksw)) nnh=nnh+1
      enddo
      do i=1,nn
        ia=IAt(i)
        ISave(i,NKeepRigidAll)=ia
        do j=2,4
          if(ai(ia).le.0.) then
            KiA(j,ia)=0
          else if(KiA(j,ia).gt.0) then
            KiA(j,ia)=-1
          endif
        enddo
        NConstrain=NConstrain+3
        call CopyVek(RMAt(1,i),RSave(1,i,NKeepRigidAll),9)
        call MatInv(RMAt(1,i),RISave(1,i,NKeepRigidAll),Det,3)
        call CopyVek(dsym(1,i),SSave(1,i,NKeepRigidAll),3)
        call CopyVek(x(1,ia),xq,3)
        call CopyVek(dsym(1,i),XSave(1,i,NKeepRigidAll),3)
        call CultM(RMAt(1,i),xq,XSave(1,i,NKeepRigidAll),3,3,1)
        do j=1,i-1
          ja=IAt(j)
          do k=1,3
            xq(k)=XSave(k,j,NKeepRigidAll)-XSave(k,i,NKeepRigidAll)
          enddo
          call Multm(MetTens(1,isw,ksw),xq,xj,3,3,1)
          DSave(i,j,NKeepRigidAll)=sqrt(scalmul(xj,xq))
          DSave(j,i,NKeepRigidAll)=DSave(i,j,NKeepRigidAll)
        enddo
      enddo
      NKeepRigid(ksw)=NKeepRigid(ksw)+1
      DMax=0.
      do i=1,nn
        do j=1,i-1
          do k=1,j-1
            pom=DSave(i,j,NKeepRigidAll)+DSave(i,k,NKeepRigidAll)+
     1          DSave(j,k,NKeepRigidAll)
            if(pom.gt.DMax) then
              DMax=pom
              BSave(1,NKeepRigidAll)=i
              BSave(2,NKeepRigidAll)=j
              BSave(3,NKeepRigidAll)=k
            endif
          enddo
        enddo
      enddo
      if(lnda.eq.0) then
        lnda=NextLogicNumber()
        call OpenFile(lnda,fln(:ifln)//'.l95','unfoRMatted','unknown')
      endif
      do i=1,nn-1
        do j=i+1,nn
          if(Type.eq.IdKeepGRigid) then
            pom=DSave(i,j,NKeepRigidAll)
          else
            pom=-999.
          endif
          write(lnda) -IdFixDistAngTors,2,pom,
     1                sfix,AtName(i),AtName(j),
     2                IAt(i),(RMAt(k,i),k=1,9),
     3                (RM6At(k,i),k=1,NDimQ(ksw)),
     4                (dsym(k,i),k=1,NDim(ksw)),
     5                (RMiiAt(k,i),k=1,9),KiAtX(i),KiAtXMod(i),
     6                IAt(j),(RMAt(k,j),k=1,9),
     7                (RM6At(k,j),k=1,NDimQ(ksw)),
     8                (dsym(k,j),k=1,NDim(ksw)),
     9                (RMiiAt(k,j),k=1,9),KiAtX(j),KiAtXMod(j)
        enddo
      enddo
      go to 9999
      entry RefKeepGRigidSetKi
      do im=1,NKeepRigidAll
        do i=1,NSave(im)
          ia=ISave(i,im)
          do j=2,4
            if(ai(ia).le.0.) then
              KiA(j,ia)=0
            else if(KiA(j,ia).gt.0) then
              KiA(j,ia)=-1
            endif
          enddo
        enddo
      enddo
      go to 9999
      entry RefKeepGRigidUpdate
      allocate(XActual(3,NMax,NKeepRigidAll),Angles(3,NKeepRigidAll),
     1         TrVec(3,NKeepRigidAll),MultAt(NAtAll))
      MultAt=0
      do im=1,NKeepRigidAll
        if(TypeRigid(im).ne.IdKeepGRigid) cycle
        i1=BSave(1,im)
        i2=BSave(2,im)
        i3=BSave(3,im)
        do i=1,NSave(im)
          ia=ISave(i,im)
          if(i.eq.1) then
            isw=iswa(ia)
            ksw=kswa(ia)
          endif
          MultAt(ia)=MultAt(ia)+1
          call CopyVek(x(1,ISave(i,im)),xq,3)
          call CopyVek(SSave(1,i,im),XActual(1,i,im),3)
          call CultM(RSave(1,i,im),xq,XActual(1,i,im),3,3,1)
        enddo
        do i=1,3
          V21Actual(i)=XActual(i,i2,im)-XActual(i,i1,im)
          V31Actual(i)=XActual(i,i3,im)-XActual(i,i1,im)
          V21Save(i)=XSave(i,i2,im)-XSave(i,i1,im)
          V31Save(i)=XSave(i,i3,im)-XSave(i,i1,im)
        enddo
        call MultM(TrToOrtho(1,1,ksw),V21Actual,xq,3,3,1)
        call CopyVek(xq,V21Actual,3)
        call MultM(TrToOrtho(1,1,ksw),V31Actual,xq,3,3,1)
        call CopyVek(xq,V31Actual,3)
        call MultM(TrToOrtho(1,1,ksw),V21Save,xq,3,3,1)
        call CopyVek(xq,V21Save,3)
        call MultM(TrToOrtho(1,1,ksw),V31Save,xq,3,3,1)
        call CopyVek(xq,V31Save,3)
        call VecOrtNorm(V21Actual ,3)
        call VecOrtNorm(V21Save,3)
        call CopyVek(V21Actual,PA,3)
        call CopyVek(V21Save,PB,3)
        fs=-scalmul(V21Actual,V31Actual)
        fsv=-scalmul(V21Save,V31Save)
        do i=1,3
          V31Actual(i)=V31Actual(i)+fs *V21Actual(i)
          V31Save(i)  =V31Save(i)  +fsv*V21Save(i)
        enddo
        call VecOrtNorm(V31Actual,3)
        call VecOrtNorm(V31Save  ,3)
        call CopyVek(V31Actual,PA(4),3)
        call CopyVek(V31Save  ,PB(4),3)
        call VecMul(V21Actual,V31Actual,PA(7))
        call VecOrtNorm(PA(7),3)
        call VecMul(V21Save,V31Save,PB(7))
        call VecOrtNorm(PB(7),3)
        call MatInv(PB,PC,det,3)
        call MultM(PA,PC,RotMat,3,3,3)
        call VecOrtNorm(RotMat(1,1),3)
        call VecOrtNorm(RotMat(1,2),3)
        call VecOrtNorm(RotMat(1,3),3)
        call EM40GetAngles(RotMat,1,Angles(1,im))
        call MultM(TrToOrthoI(1,isw,ksw),RotMat,PA,3,3,3)
        call MultM(PA,TrToOrtho(1,isw,ksw),RotMat,3,3,3)
        do i=1,3
          TrVec(i,im)=XActual(i,i1,im)-XSave(i,i1,im)
        enddo
      enddo
      nx=6
      do ir=1,5
        SumaD=0.
        do im=1,NKeepRigidAll
          if(NSave(im).le.3.or.TypeRigid(im).ne.IdKeepGRigid) cycle
          ia=ISave(1,im)
          isw=iswa(ia)
          ksw=iswa(ia)
          call SetRealArrayTo(rx,(nx*(nx+1))/2,0.)
          call SetRealArrayTo(px,nx,0.)
          call SetRotMat(Angles(1,im),RotMat,DRotMat,ksw,isw)
          do i=1,NSave(im)
            ia=ISave(i,im)
            if(MultAt(ia).gt.1) then
              wt=100.
            else
              wt=1.
            endif
            call CopyVek(TrVec(1,im),xq,3)
            call CultM(RotMat,XSave(1,i,im),xq,3,3,1)
            do k=1,3
              call Multm(DRotMat(1,1,k),XSave(1,i,im),xf(1,k),3,3,1)
            enddo
            do j=1,3
              pom=xq(j)-XActual(j,i,im)
              SumaD=SumaD+pom**2
              dr=0.
              do k=1,3
                dr(k)=xf(j,k)
              enddo
              dr(j+3)=1.
              n=0
              do k=1,nx
                px(k)=px(k)-wt*pom*dr(k)
                do l=1,k
                  n=n+1
                  rx(n)=rx(n)+wt*dr(k)*dr(l)
                enddo
              enddo
            enddo
          enddo
          call smi(rx,nx,ising)
          if(ising.ne.0) cycle
          call nasob(rx,px,dr,nx)
          do i=1,3
            Angles(i,im)=Angles(i,im)+dr(i)
          enddo
          do i=1,3
            TrVec(i,im)=TrVec(i,im)+dr(i+3)
          enddo
        enddo
      enddo
      MultAt=0
      do im=1,NKeepRigidAll
        if(TypeRigid(im).ne.IdKeepGRigid) cycle
        call SetRotMat(Angles(1,im),RotMat,DRotMat,ksw,isw)
        do i=1,NSave(im)
          call CopyVek(TrVec(1,im),xq,3)
          call CultM(RotMat,XSave(1,i,im),xq,3,3,1)
          do j=1,3
            xq(j)=xq(j)-SSave(j,i,im)
          enddo
          call MultM(RISave(1,i,im),xq,xi,3,3,1)
          ia=ISave(i,im)
          if(MultAt(ia).le.0) call SetRealArrayTo(x(1,ia),3,0.)
          MultAt(ia)=MultAt(ia)+1
          call AddVek(xi,x(1,ia),x(1,ia),3)
        enddo
      enddo
      xq=0.
      do i=1,NAtAll
        KPhase=kswa(i)
        if(MultAt(i).gt.1) then
          pom=1./float(MultAt(i))
          do j=1,3
            x(j,i)=x(j,i)*pom
          enddo
          kk=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,i).ne.0.and.KModA(1,i).ne.0) then
              xq(1)=ax(KModA(1,i),i)
              kk=1
            else if(KFA(2,i).ne.0.and.KModA(2,i).ne.0) then
              xq(1)=uy(1,KModA(2,i),i)
              kk=1
            endif
          endif
          call SpecPos(x(1,i),xq,kk,iswa(i),.1,nocc)
        endif
      enddo
      deallocate(XActual,Angles,TrVec,MultAt)
9999  return
      end
