      subroutine RefGetNParAt(m,k,MakeInOrtho,PridatQR,ich)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical MakeInOrtho,PridatQR
      ich=0
      PridatQR=.false.
      if(m.gt.mxdam.or.m.eq.JeToDelta.or.m.eq.JeToX40.or.
     1   m.eq.JeToT40.or.m.eq.JeToXSlope.or.m.eq.JeToYSlope.or.
     2   m.eq.JeToZSlope) then
        if(MakeInOrtho) call TrOrtho(1)
        if(m.gt.mxdam) then
          m=m-mxdam
          do j=1,9
            if(m.lt.CumulAt(j)) then
              m=m+2*TRank(j-3)*(KModA(j-2,k)-1)
              go to 1100
            endif
          enddo
        else if(m.gt.JeToXSlope) then
          if(KFA(2,k).gt.0) then
            if(m.eq.JeToDelta) then
              m=5
            else
              PridatQR=m.eq.JeToT40
              m=4
            endif
            m=m+CumulAt(3)+2*TRank(1)*(KModA(2,k)-1)
          else if(KFA(1,k).gt.0) then
            if(m.eq.JeToDelta) then
              m=CumulAt(2)+1
            else
              PridatQR=m.eq.JeToT40
              m=CumulAt(2)+2*TRank(0)*(KModA(1,k)-1)+2
            endif
          else
            ich=1
          endif
        else
          m=CumulAt(3)+2*TRank(1)*(KModA(2,k)-1)-m+JeToXSlope+1
        endif
1100    if(MakeInOrtho) call TrOrtho(0)
      endif
      return
      end
