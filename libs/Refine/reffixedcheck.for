      subroutine RefFixedCheck(AtomString,FixedType,ErrString)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*(*) AtomString,ErrString
      integer FixedType
      logical EqIgCase
      ErrString=' '
      if(FixedType.lt.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildYes,IdAtMolNo,IdMolNo,
     1                      IdSymmNo,IdAtMolMixNo,ErrString)
      else if(FixedType.eq.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildNo,IdAtMolNo,IdMolYes,
     1                      IdSymmNo,IdAtMolMixNo,ErrString)
      else
        if(FixedType.eq.IdFixOriginX4) then
          KeyWild=IdWildNo
          KeyAtMolMix=IdAtMolMixNo
        else
          KeyWild=IdWildYes
          KeyAtMolMix=IdAtMolMixYes
        endif
        call TestParamString(AtomString,KeyWild,KeyAtMolMix,ErrString)
        if(EqIgCase(ErrString,'Mixed').and.KeyAtMolMix.eq.IdAtMolMixYes)
     1    ErrString=' '
      endif
      return
      end
