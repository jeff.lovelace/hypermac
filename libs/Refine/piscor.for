      subroutine piscor(i,j,corrl)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      integer RefGetKiInd
      character*128 ven
      character*20  at
      character*12  pn
      k=RefGetKiInd(i)
      if(ki(k).lt.0) go to 9999
      call kdoco(k,at,pn,0,pom,pom)
      write(ven,'(f6.3,'' correlation  : '')') corrl
      ven=ven(:idel(ven))//' '//pn(1:idel(pn))
      k=idel(at)
      if(k.gt.0) ven=ven(:idel(ven))//'['//at(:k)//']'
      k=RefGetKiInd(j)
      if(ki(k).lt.0) go to 9999
      call kdoco(k,at,pn,0,pom,pom)
      ven=ven(:idel(ven))//'/'//pn(:idel(pn))
      k=idel(at)
      if(k.gt.0) ven=ven(:idel(ven))//'['//at(:k)//']'
      write(lst,FormA) ven
9999  return
      end
