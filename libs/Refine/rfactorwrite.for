      subroutine RFactorWrite
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*128 t128
      character*80 ForSingle1,ForSingle2,Veta
      character*32 Form89
      character*1  t1
      logical PsatNaDisplay,PwdCheckTOFInD
      data Form89/'(a1,''factors'',i1,'': '',4f8.4,3i7)'/
      NInfo=0
      NPisSing=0
      KDatBP=0
      do KDatB=1,NDatBlock
        if(.not.UseDatBlockActual(KDatB)) cycle
        KDatBP=KDatB
        isPowder=iabs(DataType(KDatB)).eq.2
        call PwdSetTOF(KDatB)
        if(isPowder) then
          n=NPhase
        else
          n=1
        endif
        do KPh=1,n
          if(NAtCalc.gt.0.and.DoLeBail.eq.0)
     1      call RefCalcRval(KPh,KDatB,ich)
          if(isPowder.and.NUseDatBlockActual.le.1) then
            if(NDimI(KPh).gt.0.or.MagneticRFac(KPh).gt.0) then
              if(KPh.eq.KPhaseBasic) Ninfo=0
            endif
          endif
          Form89(16:16)='1'
          do i=1,28
            if(i.eq.10) then
              Form89(16:16)='2'
            else if(i.eq.22) then
              Form89(16:16)='1'
            endif
            if(i.le.21) then
              t1='r'
            else
              t1='p'
            endif
            r=0.
            wr=0.
            rz=0.
            wrz=0.
            if(isPowder.and.i.eq.1) then
              RIFacObs(ICyklMod6,KPh,KDatB)=0.
              RIFacAll(ICyklMod6,KPh,KDatB)=0.
              wRIFacObs(ICyklMod6,KPh,KDatB)=0.
              wRIFacAll(ICyklMod6,KPh,KDatB)=0.
            endif
            if(nRAll(i).gt.0.or.
     1         (i.eq.1.and.NAtCalc.gt.0.and.DoLeBail.eq.0)) then
              if(RDenObs(i).gt.0.) then
                r=RNumObs(i)/RDenObs(i)*100.
                wRNumObs(i)=sqrt(wRNumObs(i))
                wRDenObs(i)=sqrt(wRDenObs(i))
                wr=wRNumObs(i)/wRDenObs(i)*100.
              endif
              if(RDenAll(i).gt.0.) then
                rz=RNumAll(i)/RDenAll(i)*100.
                wRNumAll(i)=sqrt(wRNumAll(i))
                wRDenAll(i)=sqrt(wRDenAll(i))
                wrz=wRNumAll(i)/wRDenAll(i)*100.
                if(i.eq.1.and..not.isPowder.and.
     1             NUseDatBlockActual.le.1) wRFacOverall=wrz
              endif
              if(isPowder) then
                PsatNaDisplay=
     1            (((NDimI(KPhase).gt.0.or.MagneticRFac(KPhase).gt.0)
     2               .and.KPhase.eq.KPhaseBasic).or.
     3             (NDimI(KPhaseBasic).le.0.and.i.eq.1)).and.i.le.21
              else
                PsatNaDisplay=i.le.21
                if(i.eq.1) write(ForSingle1,111)
     1            GOFObs(ICyklMod6,KDatB),GOFAll(ICyklMod6,KDatB)
              endif
              if(PsatNaDisplay.and.NUseDatBlockActual.le.1) then
                if(i.eq.1) then
                  if(isPowder) then
                    write(t128,114) nRAll(i),nRObs(i),nRAll(i)-nRObs(i),
     1                              nParStr(KPh)+nParData(KDatB)
                  else
                    write(t128,114) nRAll(i),nRObs(i),nRAll(i)-nRObs(i),
     1                              nParRef
                  endif
                else
                  write(t128,113) nRAll(i),nRObs(i),nRAll(i)-nRObs(i)
                endif
                call zhusti(t128)
                if(i.eq.1) then
                  if(.not.isPowder) write(t128(idel(t128)+1:),103) tlum
                  Ninfo=Ninfo+1
                  if(NPhase.gt.1.and.NDimI(KPhaseBasic).le.0.and.
     1              isPowder) then
                    TextInfo(Ninfo)='R factors for '//
     1                PhaseName(KPh)(:idel(PhaseName(KPh)))//' : '//
     2                t128(:idel(t128))
                  else
                    NPisSing=NInfo
                    TextInfo(Ninfo)='R factors : '//
     1                              t128(:idel(t128))
                  endif
                  if(.not.isPowder) then
                    Ninfo=Ninfo+1
                    TextInfo(Ninfo)=ForSingle1
                    if(iskip.ne.0.or.NTwin.gt.1) then
                      Ninfo=Ninfo+1
                      write(Cislo,'(i6,''+'',i6)')
     1                  NSkipRef(ICyklMod6,KDatB),
     2                  NBadOverRef(ICyklMod6,KDatB)
                      call Zhusti(Cislo)
                      TextInfo(NInfo)='Number of reflections excluded'//
     1                                ' due to refinement options: '//
     2                                Cislo(:idel(Cislo))
                    endif
                  endif
                else if(i.eq.2) then
                  Ninfo=Ninfo+1
                  if(MagneticRFac(KPh).gt.0) then
                    TextInfo(Ninfo)='R factors for nuclear '//
     1                            'reflections : '//t128(:idel(t128))
                  else
                    TextInfo(Ninfo)='R factors for main '//
     1                            'reflections : '//t128(:idel(t128))
                  endif
                else if(i.lt.20) then
                  if(NInfo.le.13) then
                    Ninfo=Ninfo+1
                    if(MagneticRFac(KPh).gt.0) then
                      TextInfo(Ninfo)='R factors for magnetic '//
     1                              'reflections : '//t128(:idel(t128))
                    else
                      Veta='+-('
                      do j=1,NDimI(KPhase)
                        write(Cislo,FormI15) HSatGroups(j,i-2)
                        call zhusti(Cislo)
                        Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
                      enddo
                      j=idel(Veta)
                      Veta(j:j)=')'
                      TextInfo(Ninfo)='R factors for satellites '//
     1                  Veta(:idel(Veta))//' : '//t128(:idel(t128))
                    endif
                  endif
                else
                  if(NInfo.le.13) then
                    Ninfo=Ninfo+1
                    TextInfo(Ninfo)=
     1                'R factors for unsorted satellites : '//
     2                 t128(:idel(t128))
                  endif
                endif
              endif
              if(nRObs(i).gt.0) then
                if(i.eq.1) then
                  if(NAtCalc.gt.0.and.DoLeBail.eq.0) then
                    do j=1,4
                      if(j.eq.1) then
                        m=23
                        pom=r*.01
                      else if(j.eq.2) then
                        m=38
                        pom=wr*.01
                      else if(j.eq.3) then
                        m=22
                        pom=rz*.01
                      else if(j.eq.4) then
                        m=37
                        pom=wrz*.01
                      endif
                      write(Cislo,'(f8.4)') pom
                      call Zhusti(Cislo)
                      write(LnSum,FormCIF) CifKey(m,15),Cislo
                      if(j.eq.4)
     1                  write(LnSum,FormCIF) CifKey(40,15),Cislo
                    enddo
                  endif
                  if(.not.isPowder) then
                    do j=1,2
                      if(j.eq.1) then
                        write(Cislo,FormI15) nRAll(i)
                      else if(j.eq.2) then
                        write(Cislo,FormI15) nParRef
                      endif
                      call Zhusti(Cislo)
                      write(LnSum,FormCIF)
     1                  CifKey(21-j,15),Cislo(:idel(Cislo))
                    enddo
                  endif
                  write(ForSingle2,110)
     1              Tabulator//StRFac(IStRFac(1))//Tabulator,r,
     2              Tabulator//StRFac(IStRFac(2))//Tabulator,wr,
     3              Tabulator//StRFac(IStRFac(3))//Tabulator,rz,
     4              Tabulator//StRFac(IStRFac(4))//Tabulator,wrz
                endif
                if(PsatNaDisplay.and.NInfo.le.13.and.
     1             NUseDatBlockActual.le.1) then
                  Ninfo=Ninfo+1
                  write(TextInfo(Ninfo),110)
     1              Tabulator//StRFac(IStRFac(1))//Tabulator,r,
     2              Tabulator//StRFac(IStRFac(2))//Tabulator,wr,
     3              Tabulator//StRFac(IStRFac(3))//Tabulator,rz,
     4              Tabulator//StRFac(IStRFac(4))//Tabulator,wrz
                endif
              else
                if(PsatNaDisplay.and.NInfo.le.13.and.
     1             NUseDatBlockActual.le.1) then
                  Ninfo=Ninfo+1
                  if(RDenAll(i).gt.0.) then
                    write(TextInfo(Ninfo),112)
     1                Tabulator//StRFac(IStRFac(1))//Tabulator,
     2                Tabulator//StRFac(IStRFac(2))//Tabulator,
     3                Tabulator//StRFac(IStRFac(3))//Tabulator,rz,
     4                Tabulator//StRFac(IStRFac(4))//Tabulator,wrz
                  else
                    write(TextInfo(Ninfo),109)
     1                Tabulator//StRFac(IStRFac(1))//Tabulator,
     2                Tabulator//StRFac(IStRFac(2))//Tabulator,
     3                Tabulator//StRFac(IStRFac(3))//Tabulator,
     4                Tabulator//StRFac(IStRFac(4))//Tabulator
                  endif
                endif
              endif
            endif
             RFacObs(i,ICyklMod6,KPh,KDatB)=R
             RFacAll(i,ICyklMod6,KPh,KDatB)=Rz
            wRFacObs(i,ICyklMod6,KPh,KDatB)=wR
            wRFacAll(i,ICyklMod6,KPh,KDatB)=wRz
            nRFacObs(i,ICyklMod6,KPh,KDatB)=nRObs(i)
            nRFacAll(i,ICyklMod6,KPh,KDatB)=nRAll(i)
            if(isPowder.and.i.eq.1) then
              if(RIDenObs.gt.0.) then
                 RIFacObs(ICyklMod6,KPh,KDatB)=RINumObs/RIDenObs*100.
                wRINumObs=sqrt(wRINumObs)
                wRIDenObs=sqrt(wRIDenObs)
                wRIFacObs(ICyklMod6,KPh,KDatB)=wRINumObs/wRIDenObs*100.
              endif
              if(RIDenAll.gt.0.) then
                 RIFacAll(ICyklMod6,KPh,KDatB)=RINumAll/RIDenAll*100.
                wRINumAll=sqrt(wRINumAll)
                wRIDenAll=sqrt(wRIDenAll)
                wRIFacAll(ICyklMod6,KPh,KDatB)=wRINumAll/wRIDenAll*100.
              endif
            endif
          enddo
        enddo
        if(NUseDatBlockActual.gt.1.and.NInfo.lt.16) then
          if(KDatB.gt.KDatBP) then
            Ninfo=Ninfo+1
            TextInfo(Ninfo)=' '
            do i=1,90
              TextInfo(Ninfo)(i:i)='-'
            enddo
          endif
          Ninfo=Ninfo+1
          if(isPowder) then
            Ninfo=Ninfo+1
            write(TextInfo(Ninfo),'(''GOF   ='',f6.2,''  Rp    ='',f6.2,
     1                              ''  wRp    ='',f6.2)')
     2        GOFProf(ICyklMod6,KDatB),RFacProf(ICyklMod6,KDatB),
     3        wRFacProf(ICyklMod6,KDatB)
            write(t128,'('' ['',i6,''/'',i6,''+'',i6,'']'')')
     1          nRFacProf(KDatB),NParPwdAll(KDatB),NParStrAll(KDatB)
            k=1
          else
            Ninfo=Ninfo+1
            TextInfo(NInfo)=ForSingle1
            Ninfo=Ninfo+1
            TextInfo(NInfo)=ForSingle2
            write(t128,113) nRAll(1),nRObs(1),nRAll(1)-nRObs(1)
            k=2
          endif
          call zhusti(t128)
          TextInfo(Ninfo-k)=
     1      MenuDatBlock(KDatB)(:idel(MenuDatBlock(KDatB)))//' : '//
     2      t128(:idel(t128))
        endif
      enddo
      if((NUseDatBlockActual.gt.1.and.
     1    NUseDatBlockActual.ne.NSingle).or.NPowder.eq.1) then
        if(NInfo.gt.0) then
          Ninfo=Ninfo+1
          TextInfo(Ninfo)=' '
          do i=1,45
            TextInfo(Ninfo)(i:i)='='
          enddo
        endif
        Ninfo=Ninfo+1
        NPar=0
        NParS=0
        do j=0,NDatBlock
          do i=0,NPhase
            NPar=NPar+NParPwd(i,j)
          enddo
        enddo
        do i=1,NDatBlock
          NParS=NParS+NParData(i)
        enddo
        do i=1,NPhase
          NParS=NParS+NParStr(i)
        enddo
        write(t128,'('' ['',i6,''/'',i6,''+'',i6,'']'')')
     1    nRFacOverall,NPar,NParS
        call zhusti(t128)
        NPisSing=NInfo
        if(NUseDatBlockActual.gt.1) then
          TextInfo(Ninfo)='Over all used blocks :'//t128(:idel(t128))
        else
          write(t128(idel(t128)+1:),103) tlum
          TextInfo(Ninfo)='Profile R factors :'//t128(:idel(t128))
        endif
      endif
      if(nPowder.eq.NUseDatBlockActual) then
        Ninfo=Ninfo+1
        write(TextInfo(Ninfo),'(''GOF   ='',f6.2,'' Rp    ='',f6.2,
     1                          '' wRp    ='',f6.2)')
     2                 GOFOverall,RFacOverall,wRFacOverall
        if(nPowder.gt.1) then
          write(t128,103) tlum
          TextInfo(Ninfo)=TextInfo(Ninfo)(:idel(TextInfo(Ninfo)))//
     1                    t128(:idel(t128))
        endif
        do j=1,3
          if(j.eq.1) then
            pom=RFacOverall*.01
          else if(j.eq.2) then
            pom=wRFacOverall*.01
          else
            pom=eRFacProf(KDatBlock)*.01
          endif
          write(Cislo,'(f8.4)') pom
          call Zhusti(Cislo)
          write(LnSum,FormCIF) CifKey(j+141,20),Cislo
        enddo
        write(Cislo,FormI15) nParRef
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(19,15),Cislo
      else if(nSingle.eq.NUseDatBlockActual) then
        if(NUseDatBlockActual.gt.1) then
          if(NInfo.gt.0) then
            Ninfo=Ninfo+1
            TextInfo(Ninfo)=' '
            do i=1,45
              TextInfo(Ninfo)(i:i)='='
            enddo
          endif
          Ninfo=Ninfo+1
          write(t128,114) nRFacOverall,nRFacOverallObs,
     1                   nRFacOverall-nRFacOverallObs,nParRef
          call Zhusti(t128)
          NPisSing=NInfo
          write(t128(idel(t128)+1:),103) tlum
          TextInfo(Ninfo)='Overall R factors : '//t128(:idel(t128))
          Ninfo=Ninfo+1
          write(TextInfo(Ninfo),111) GOFOverallObs,GOFOverall
          Ninfo=Ninfo+1
          if(nRFacOverallObs.gt.0) then
            write(TextInfo(Ninfo),110)
     1        Tabulator//StRFac(IStRFac(1))//Tabulator,RFacOverAllObs,
     2        Tabulator//StRFac(IStRFac(2))//Tabulator,wRFacOverAllObs,
     3        Tabulator//StRFac(IStRFac(3))//Tabulator,RFacOverAll,
     4        Tabulator//StRFac(IStRFac(4))//Tabulator,wRFacOverAll
          else
            if(nRFacOverall.gt.0.) then
              write(TextInfo(Ninfo),112)
     1          Tabulator//StRFac(IStRFac(1))//Tabulator,
     2          Tabulator//StRFac(IStRFac(2))//Tabulator,
     3          Tabulator//StRFac(IStRFac(3))//Tabulator,RFacOverAll,
     4          Tabulator//StRFac(IStRFac(4))//Tabulator,wRFacOverAll
            else
              write(TextInfo(Ninfo),109)
     1          Tabulator//StRFac(IStRFac(1))//Tabulator,
     2          Tabulator//StRFac(IStRFac(2))//Tabulator,
     3          Tabulator//StRFac(IStRFac(3))//Tabulator,
     4          Tabulator//StRFac(IStRFac(4))//Tabulator
            endif
          endif
        endif
      else
        write(t128,'(''GOF   ='',f6.2)') GOFOverall
        write(t128(idel(t128)+1:),103) tlum
        Ninfo=Ninfo+1
        TextInfo(Ninfo)=t128(:idel(t128))
      endif
      Ninfo=Ninfo+1
      if(nPowder.eq.NUseDatBlockActual) then
        TextInfo(Ninfo)='Last wRp:'
      else if(nSingle.eq.NUseDatBlockActual) then
        if(ifsq.ne.1) then
          TextInfo(Ninfo)='Last wR(all):'
        else
          TextInfo(Ninfo)='Last wR2(all):'
        endif
      else
        TextInfo(Ninfo)='Last GOF:'
      endif
      idl=idel(TextInfo(Ninfo))+1
      if(nRLast.gt.0) then
        write(TextInfo(NInfo)(idl:),'(8f6.2)')(RLast(j),j=1,nRLast)
      endif
      if(nRLast.lt.8) then
        nRLast=nRLast+1
        if(nPowder.eq.0.or.nSingle.eq.0) then
          RLast(nRLast)=wRFacOverall
        else
          RLast(nRLast)=GOFOverall
        endif
      else
        do j=1,7
          RLast(j)=RLast(j+1)
        enddo
        if(nPowder.eq.0.or.nSingle.eq.0) then
          RLast(8)=wRFacOverall
        else
          RLast(8)=GOFOverall
        endif
      endif
      if(ncykl.le.0) then
        rxm=0.
        rxs=0.
      endif
      pom2=0.
      if(NUseDatBlockActual.gt.1) then
        pom1=GOFOverall
      else
        if(nPowder.gt.0) then
          pom1=GOFProf(ICyklMod6,KDatBlockUsed)
        else
          pom1=GOFAll(ICyklMod6,KDatBlockUsed)
          pom2=GOFObs(ICyklMod6,KDatBlockUsed)
        endif
      endif
      if(nPowder.gt.0) then
        j=12
      else
        j=13
      endif
      do i=12,j
        if(i.eq.12) then
          pom=pom1
        else
          pom=pom2
        endif
        write(Cislo,'(f7.2)') pom
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(i,15),Cislo
        if(i.eq.12)
     1    write(LnSum,FormCIF) CifKey(15,15),Cislo
      enddo
      do i=32,33
        if(i.eq.32) then
          pom=abs(rxm)
        else
          pom=rxs
        endif
        write(Cislo,'(f9.4)') pom
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(i,15),Cislo
      enddo
      return
102   format(a1,'factorsp: ',5f8.4,3i8,f10.3)
103   format(',    Damping factor: ',f8.4)
109   format(2(a,'  ----'),2(a,'  ----'))
110   format(4(a,f6.2))
111   format('GOF(obs)=',f6.2,'    GOF(all)=',f6.2)
112   format(2(a,'  ----'),2(a,f6.2))
113   format('  [',i6,'=',i6,'+',i6,']')
114   format('  [',i6,'=',i6,'+',i6,'/',i6,']')
      end
