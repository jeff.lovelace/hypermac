      subroutine MultSpec(iap,iak)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PrvKi
      dimension RPop(680),rp1(6,3),rp2(10,5),rp3(14,7),
     1          rp4(18,9),rp5(22,11),rp6(26,13),rp7(30,15),px(30),
     2          rp(1358),nx(7),RPom(9)
      equivalence (rp,rp1),(rp( 19),rp2),(rp( 69),rp3),(rp(167),rp4),
     1                     (rp(329),rp5),(rp(571),rp6),(rp(909),rp7)
      call SetRealArrayTo(px,30,0.)
      do ia=iap,iak
        lmax=lasmax(ia)-2
        if(lmax.le.0) cycle
        call SetRealArrayTo(rp,1358,0.)
        call SetIntArrayTo(nx,7,0)
        if(SmbPGAt(ia).ne.'1'.and.SmbPGAt(ia).ne.'C1') then
          call GenPg(SmbPGAt(ia),RPGAt(1,1,ia),NPGAt(ia),ich)
          if(ich.ne.0) then
            call FeChybne(-1.,-1.,'wrong point group symbol for atom "'
     1                  //atom(ia)(:idel(atom(ia)))//'" - '//
     2                    SmbPGAt(ia)(:idel(SmbPGAt(ia))),' ',
     3                    SeriousError)
            cycle
          endif
        else
          do i=1,NPGAt(ia)
            call multm(TrAt(1,ia),RPGAt(1,i,ia),RPom,3,3,3)
            call multm(RPom,TriAt(1,ia),RPGAt(1,i,ia),3,3,3)
          enddo
        endif
        do j=1,NPGAt(ia)
          call SetPopulTrans(RPGAt(1,j,ia),lmax,RPop)
          kp=2
          lp=1
          do l=1,lmax
            n=2*l+1
            call AveSpec(rp(lp),px,nx(l),0,RPop(kp),n)
            n=n**2
            kp=kp+n
            lp=lp+2*n
          enddo
        enddo
        PrvKi=PrvniKiAtomu(ia)+DelkaKiAtomu(ia)-(lmax+1)**2
        lp=1
        do l=1,lmax
          n=2*l+1
          call speceq(rp(lp),px,nx(l),n,PrvKi)
          PrvKi=PrvKi+n
          lp=lp+2*n**2
        enddo
      enddo
      return
      end
