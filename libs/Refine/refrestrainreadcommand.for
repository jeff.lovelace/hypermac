      subroutine RefRestrainReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/RestrainQuest/ RestrainType,nEdwValue,nEdwESD,nEdwAtoms,
     1     FixValue,FixValueESD,StringOfAtoms,TypeOfRestrain,FixString
      character*(*) Command
      character*256 StringOfAtoms
      character*80  t80
      character*8   FixString
      integer RestrainType,TypeOfRestrain
      dimension xp(1)
      save /RestrainQuest/
      ich=0
      TypeOfRestrains=1
      if(RestrainType.eq.IdDistFix) then
        FixValue   =1.5
        FixValueESD=0.001
      else if(RestrainType.eq.IdAngleFix) then
        FixValue   =109.47
        FixValueESD=  0.01
      else if(RestrainType.eq.IdTorsFix) then
        FixValue   =180.00
        FixValueESD=  0.01
      endif
      StringOfAtoms=' '
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.FixString.and.t80.ne.'!'//FixString) go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(command,k,t80)
        if(t80.eq.'=') then
          TypeOfRestrain=2
          cycle
        else if(t80.eq.'*') then
          TypeOfRestrain=3
          cycle
        endif
        kk=0
        call StToReal(t80,kk,xp,1,.false.,ich)
        if(ich.ne.0) go to 8040
        if(i.eq.1) then
          FixValue=xp(1)
        else
          FixValueESD=xp(1)
        endif
      enddo
      call RefRestrainCheckAtomString(Command(k+1:),t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
