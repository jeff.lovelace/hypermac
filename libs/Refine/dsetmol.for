      subroutine DSetMol
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension px(12),xp(3)
      integer PrvKiMol
      call SetRealArrayTo(der(PrvniKiAtMol),
     1                    PosledniKiAtMol-PrvniKiAtMol+1,0.)
      do KPh=1,NPhase
        KPhase=KPh
        ibk=NAtMolFrAll(KPh)-1
        ia=NAtPosFrAll(KPh)-1
        do i=NMolecFrAll(KPh),NMolecToAll(KPh)
          ibp=ibk+1
          ibk=ibk+iam(i)
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            PrvKiMol=PrvniKiMolekuly(ji)
            call SetRealArrayTo(der(PrvKiMol),DelkaKiMolekuly(ji),0.)
            do ib=ibp,ibk
              ia=ia+1
              if(ai(ib).le.0.) cycle
              mb=ib-NAtMolFr(1,1)+1
              kipa=PrvniKiAtomu(ia)
              kipb=PrvniKiAtomu(ib)
              if(TypeModFun(ia).eq.1) then
                kmodxia=kmodao(2,ia)
                kmodbia=kmodao(3,ia)
              else
                kmodxia=KModA(2,ia)
                kmodbia=KModA(3,ia)
              endif
              idf=kipb-kipa
              kipm=PrvKiMol
              der(kipa+idf)=der(kipa+idf)+der(kipa)*aimol(ji)
              der(kipm)=der(kipm)+der(kipa)*ai(ib)
              do l=1,3
                xp(l)=x(l,ib)-xm(l,i)
              enddo
              kipa=kipa+1
              kipm=kipm+1
              call multm(drotf(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              kipm=kipm+1
              call multm(drotc(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              kipm=kipm+1
              call multm(drotp(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              call cultm(der(kipa),RotMol(1,ji),der(kipa+idf),1,3,3)
              kipm=kipm+1
              do l=1,3
                der(kipm)=der(kipm)+der(kipa)
                kipa=kipa+1
                kipm=kipm+1
              enddo
              if(itf(ib).ne.0) then
                if(itf(ia).eq.2) then
                  if(itf(ib).ge.2) then
                    kipm=kipm-6
                    call cultm(der(kipa),rotb(1,ji),der(kipa+idf),1,6,6)
                    call multm(drotbf(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+1
                    call multm(drotbc(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+1
                    call multm(drotbp(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+4
                  else if(itf(ib).eq.1) then
                    call cultm(der(kipa),prcp(1,iswa(ia),KPhase),
     1                         der(kipa+idf),1,6,1)
                  endif
                else
                  der(kipa+idf)=der(kipa+idf)+der(kipa)
                endif
              else
                call multm(der(kipa),rotb(1,ji),px,1,6,6)
                do l=1,6
                  der(kipm)=der(kipm)+px(l)
                  kipm=kipm+1
                enddo
                call cultm(px,tztl(1,mb),der(kipm),1,6,6)
                kipm=kipm+6
                call cultm(px,tzts(1,mb),der(kipm),1,6,9)
              endif
              if(ktls(i).le.0) then
                kipm=PrvKiMol+7
              else
                kipm=PrvKiMol+28
              endif
              kipa=kipa+6
              kipb=kipa+idf
              kmodib=1+2*KModA(1,ib)
              if(kmodib.eq.1) kmodib=kmodib-1
              kmodp=1+2*KModM(1,ji)
              if(KModA(1,ia).gt.0) then
                do l=1,1+2*KModA(1,ia)
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+der(kipa)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipm)=der(kipm)+der(kipa)
                    kipm=kipm+1
                  endif
                  kipa=kipa+1
                enddo
              endif
              kmodp=KModM(2,ji)
              if(kmodp.gt.0) then
                kx=PrvniKiAtomu(ib)+1
                kipt=kipm
                kipr=kipt+6*kmodp
              else
                kipr=kipm
              endif
              kmodib=KModA(2,ib)
              do l=1,kmodxia
                call multm(der(kipa),RotMol(1,ji),px,1,3,3)
                kipa=kipa+3
                call multm(der(kipa),RotMol(1,ji),px(4),1,3,3)
                kipa=kipa+3
                do m=1,6
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+px(m)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipt)=der(kipt)+px(m)
                    kipt=kipt+1
                  endif
                enddo
                if(kmodp.ge.l) then
                  call cultm(px,durdr(1,ia),der(kipr),1,3,3)
                  kipr=kipr+3
                  call cultm(px(4),durdr(1,ia),der(kipr),1,3,3)
                  kipr=kipr+3
                  call cultm(px,durdx(1,l,ji),der(kx),1,6,3)
                endif
              enddo
              kipm=kipr
              kmodp=KModM(3,ji)
              kmodib=KModA(3,ib)
              if(kmodp.gt.0) then
                kipt=kipm
                kipl=kipt+12*kmodp
                kips=kipl+12*kmodp
                kipss=kips
              else
                kips=kipm
              endif
              do l=1,kmodbia
                call multm(der(kipa),rotb(1,ji),px,1,6,6)
                kipa=kipa+6
                call multm(der(kipa),rotb(1,ji),px(7),1,6,6)
                kipa=kipa+6
                do m=1,12
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+px(m)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipt)=der(kipt)+px(m)
                    kipt=kipt+1
                  endif
                enddo
                if(kmodp.ge.l) then
                  call cultm(px,   tztl(1,mb),der(kipl),1,6,6)
                  kipl=kipl+6
                  call cultm(px(7),tztl(1,mb),der(kipl),1,6,6)
                  kipl=kipl+6
                  call cultm(px,   tzts(1,mb),der(kips),1,6,9)
                  kips=kips+9
                  call cultm(px(7),tzts(1,mb),der(kips),1,6,9)
                  kips=kips+9
                endif
              enddo
              kipm=kips
              if(KModA(1,ib).gt.0.or.KModA(2,ib).gt.0.or.
     1           KModA(3,ib).gt.0) der(kipb)=der(kipb)+der(kipa)
              if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1           KModM(3,ji).gt.0) der(kipm)=der(kipm)+der(kipa)
            enddo
          enddo
          if(kpoint(i).le.1) go to 2850
          iami=iam(i)/KPoint(i)
          do j=ibp,ibp+iami-1
            ic=j
            kbp=PrvniKiAtomu(j)-1
            do m=2,npoint(i)
              if(ipoint(m,i).le.0) cycle
              ic=ic+iami
              kcp=PrvniKiAtomu(ic)-1
              kc=kcp+1
              kb=kbp+1
              der(kb)=der(kb)+der(kc)
              kc=kc+1
              kb=kb+1
              call cultm(der(kc),rpoint(1,m,i),der(kb),1,3,3)
              kc=kc+3
              kb=kb+3
              if(itf(ic).gt.1) then
                call cultm(der(kc),tpoint(1,m,i),der(kb),1,6,6)
              else
                der(kb)=der(kb)+der(kc)
              endif
            enddo
          enddo
2850      ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            kimp=PrvniKiMolekuly(ji)+4
            if(AtTrans(ji).ne.' ') then
              k=ktat(atom,NAtInd,AtTrans(ji))
              if(k.le.0) cycle
              kipa=PrvniKiAtomu(k)+1
              call AddVek(der(kimp),der(kipa),der(kipa),3)
            endif
          enddo
        enddo
      enddo
      return
      end
