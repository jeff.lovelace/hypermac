      subroutine SetSpec
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call AtSpec(NAtIndFrAll(KPhase),NAtIndToAll(KPhase),0,0,0)
      if(ErrFlag.ne.0) go to 9999
      if(NMolecLenAll(KPhase).gt.0)
     1  call molspec(NMolecFrAll(KPhase),NMolecToAll(KPhase))
9999  return
      end
