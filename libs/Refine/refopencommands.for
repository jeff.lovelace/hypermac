      subroutine RefOpenCommands
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      j=NactiInt+NactiReal+21
      do i=j,j+9
        call MakeKeyWordFile(NactiKeywords(i)(:idel(NactiKeywords(i))),
     1                       'refine',NactiActive(i),NactiPassive(i))
      enddo
      call MakeZbytekFile
9999  if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      return
      end
