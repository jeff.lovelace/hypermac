      subroutine RefDerCompress(DerIn,DerOut,NCompr1,NCompr2,NCompr3,
     1                          Factor)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension DerIn(*),DerOut(*)
      j=0
      do i=1,ndoffPwd
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      NCompr1=j
      do i=ndoffED+1,ndoff
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      NCompr2=j
      do i=ndoff+1,PosledniKiAtInd
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      NCompr3=j
      do i=PrvniKiAtMol,PosledniKiAtMol
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      do i=PrvniKiMol,PosledniKiMol
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      do i=PrvniKiAtXYZMode,PosledniKiAtXYZMode
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      do i=PrvniKiAtMagMode,PosledniKiAtMagMode
        if(ki(i).ne.0) then
          j=j+1
          DerOut(j)=DerIn(i)*Factor
        endif
      enddo
      NCompress=j
      go to 9999
      entry RefDerUncompress(DerIn,DerOut,Factor)
      j=0
      do i=1,ndoffPwd
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=ndoffED+1,ndoff
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=ndoff+1,PosledniKiAtInd
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=PrvniKiAtMol,PosledniKiAtMol
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=PrvniKiMol,PosledniKiMol
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=PrvniKiAtXYZMode,PosledniKiAtXYZMode
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
      do i=PrvniKiAtMagMode,PosledniKiAtMagMode
        if(ki(i).ne.0) then
          j=j+1
          DerOut(i)=DerIn(j)*Factor
        else
          DerOut(i)=0.
        endif
      enddo
9999  return
      end
