      subroutine AveSpec(rx,px,nx,ji,smp,nrank)
      include 'fepc.cmn'
      dimension rx(2*nrank,nrank),px(2*nrank),smp(nrank,nrank)
      ll=nx+nrank
      do l=1,nrank
        mm=nrank
        do m=1,nrank
          if(m.eq.l) then
            rx(ll,mm)=smp(l,m)-1.
          else
            rx(ll,mm)=smp(l,m)
          endif
          mm=mm-1
        enddo
        ll=ll-1
      enddo
      call uprspec(rx,px,nrank,nx,ji)
      return
      end
