      subroutine SpecEq(rx,px,nx,n,kip)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rx(2*n,n),px(2*n)
      character*20 at
      character*12 pn
      logical psat
      psat=.false.
      do i=nx,1,-1
        l=0
        do j=n,1,-1
          if(abs(rx(i,j)).gt..00001) l=l+1
        enddo
        if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
        neq=neq+1
        npa(neq)=l-1
        pab(neq)=px(i)
        l=0
        do j=1,n
          if(abs(rx(i,j)).gt..00001) then
            l=l+1
            k=n+1-j
            call kdoco(kip+k,at,pn,0,pom,pom)
            ia=ktatmol(at)
            call mala(pn)
            if(ia.ne.0) k=Ktera(pn)
            if(l.eq.1) then
              lat(neq)=at
              lpa(neq)=pn
              lnpo(neq)=0
              if(ia.gt.0) then
                call ChangeAtCompress(k,lnp(neq),ia,0,ich)
              else if(ia.lt.0) then
                call ChangeMolCompress(k,lnp(neq),-ia,0,ich)
              else
                lnp(neq)=-kip-k
              endif
            else
              pat(l-1,neq)=at
              ppa(l-1,neq)=pn
              if(ia.gt.0) then
                call ChangeAtCompress(k,pnp(l-1,neq),ia,0,ich)
              else if(ia.lt.0) then
                call ChangeMolCompress(k,pnp(l-1,neq),-ia,0,ich)
              else
                pnp(l-1,neq)=-kip-k
              endif
              pko(l-1,neq)=-rx(i,j)
            endif
          endif
        enddo
        call SetEq(neq,neq)
        if(npa(neq).le.0) neq=neq-1
        if(ErrFlag.ne.0) go to 9999
      enddo
9999  return
      end
