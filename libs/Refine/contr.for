      subroutine contr(icont)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ssqwds(8),ssqwdf(9),nlsp(8),nlsn(8),nls(8),nlfp(9),
     1          nlfn(9),nlf(9),rnumsp(8),rnumsn(8),rnums(8),rdens(8),
     2          rfaks(8),rnumfp(9),rnumfn(9),rnumf(9),rdenf(9),rfakf(9)
      character*1 CharIF
      equivalence (flim,fmez(8)),(sinlim,sinmez(8))
      save rnumfp,rnumfn,rdenf,ssqwdf,nlfp,nlfn,rnumsp,rnumsn,rdens,
     1     ssqwds,nlsp,nlsn
      if(icont.eq.1) then
        do i=1,9
          rnumfp(i)=0.0
          rnumfn(i)=0.0
          rdenf(i)=0.0
          ssqwdf(i)=0.0
          nlfp(i)=0
          nlfn(i)=0
          if(i.ne.9) then
            rnumsp(i)=0.0
            rnumsn(i)=0.0
            rdens(i)=0.0
            ssqwds(i)=0.0
            nlsp(i)=0
            nlsn(i)=0
          endif
        enddo
      else if(icont.eq.2) then
        ls=0
        if(nulova) then
          lf=9
        else
2000      ls=ls+1
          if(sinthl.gt.sinmez(ls).and.ls.lt.8) go to 2000
          lf=0
2010      lf=lf+1
          if(Fobs.gt.fmez(lf).and.lf.lt.8) go to 2010
          ssqwds(ls)=ssqwds(ls)+wdyq
        endif
        ssqwdf(lf)=ssqwdf(lf)+wdyq
        if(dy.lt.0) then
          rnumfn(lf)=rnumfn(lf)+dyp
          nlfn(lf)=nlfn(lf)+1
          if(.not.nulova) then
            rnumsn(ls)=rnumsn(ls)+dyp
            nlsn(ls)=nlsn(ls)+1
          endif
        else
          rnumfp(lf)=rnumfp(lf)+dyp
          nlfp(lf)=nlfp(lf)+1
          if(.not.nulova) then
            rnumsp(ls)=rnumsp(ls)+dyp
            nlsp(ls)=nlsp(ls)+1
          endif
        endif
        rdenf(lf)=rdenf(lf)+Fobs
        if(.not.nulova) rdens(ls)=rdens(ls)+Fobs
      else
        if(ifsq.eq.1) then
          CharIF='Iq'
        else
          CharIF='Fq'
        endif
        snump=0.0
        snumn=0.0
        fnump=0.0
        fnumn=0.0
        sden=0.0
        fden=0.0
        lsp=0
        lsn=0
        lfp=0
        lfn=0
        do i=1,9
          rnumf(i)=rnumfp(i)-rnumfn(i)
          if(rdenf(i).ne.0.0) then
            rfakf(i)=100.0*rnumf(i)/rdenf(i)
          else
            rfakf(i)=0.
          endif
          nlf(i)=nlfp(i)+nlfn(i)
          if(nlf(i).ne.0) ssqwdf(i)=ssqwdf(i)/float(nlf(i))
          if(i.ne.9) then
            rnums(i)=rnumsp(i)-rnumsn(i)
            if(rdens(i).ne.0.0) then
              rfaks(i)=100.0*rnums(i)/rdens(i)
            else
              rfaks(i)=0.
            endif
            snump=snump+rnumsp(i)
            snumn=snumn+rnumsn(i)
            fnump=fnump+rnumfp(i)
            fnumn=fnumn+rnumfn(i)
            sden=sden+rdens(i)
            fden=fden+rdenf(i)
            lsp=lsp+nlsp(i)
            lsn=lsn+nlsn(i)
            lfp=lfp+nlfp(i)
            lfn=lfn+nlfn(i)
            nls(i)=nlsp(i)+nlsn(i)
            if(nls(i).ne.0) ssqwds(i)=ssqwds(i)/float(nls(i))
          endif
        enddo
        call newln(34)
        write(lst,100)
        write(lst,101)
        write(lst,102) sinmez,nlsp,nlsn,nls,CharIF,ssqwds,rnumsp,rnumsn,
     1                 rnums,rdens,rfaks
        write(lst,103)
        write(lst,104) fmez,nlfp,nlfn,nlf,CharIF,ssqwdf,rnumfp,rnumfn,
     1                 rnumf,rdenf,rfakf
        snum=snump-snumn
        fnum=fnump-fnumn
        rfs=100.0*snum/sden
        rff=100.0*fnum/fden
        ls=lsp+lsn
        lf=lfp+lfn
        write(lst,105) lsp,lfp,lsn,lfn,ls,lf,snump,fnump,snumn,fnumn,
     1                 snum,fnum,sden,fden,rfs,rff
      endif
      return
100   format(/'Statistics as a function of sin(th)/lambda and structure'
     1      ,' factors')
101   format('sin(th)/lambda')
102   format(18x,'limits',5x,8f10.6/18x,'number +',3x,8i10/25x,'-',3x,
     1       8i10/18x,'together',3x,8i10/18x,'av. wd',a2,3x,8f10.4/18x,
     2       'numerator +',8f10.1/28x,'-',8f10.1/18x,'together',3x,
     3       8f10.1/18x,'denominator',8f10.1/18x,'R factor',3x,8f10.2)
103   format('struct. factors')
104   format(18x,'limits',5x,8f10.1,6x,'unobs'/18x,'number +',3x,9i10/
     1       25x,'-',3x,9i10/18x,'together',3x,9i10/18x,'av. wd',a2,3x,
     2       9f10.4/18x,'numerator +',9f10.1/28x,'-',9f10.1/18x,
     3       'together',3x,9f10.1/18x,'denominator',9f10.1/18x,
     4       'R factor',3x,9f10.2)
105   format(/33x,'final check',20x,'sin(th)/lambda',5x,
     1       'structure factors'/47x,'number +',9x,i9,13x,i9/54x,1h-,
     2       9x,i9,13x,i9/47x,'together',9x,i9,13x,i9/47x,'numerator +',
     3       5x,f10.1,9x,f13.1/57x,'-',5x,f10.1,9x,f13.1/47x,'together',
     4       8x,f10.1,9x,f13.1/47x,'denominator',5x,f10.1,9x,f13.1/47x,
     5       'R-factor',8x,f10.2,9x,f13.2)
      end
