      subroutine TestGInd(String,ErrString)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String,ErrString
      integer Matrix(36),Vector(6)
      ErrString=' '
      call EqStringToIntMatrix(String,hklmnp(:maxNDim),maxNDim,Matrix,
     1                         Vector,ich)
      if(ich.eq.0) go to 9999
      ErrString='inacceptable group of reflections : '//
     1          String(:idel(String))
9999  return
      end
