      subroutine RefRestrainCheckAtomString(AtString,ErrString)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/RestrainQuest/ RestrainType,nEdwValue,nEdwESD,nEdwAtoms,
     1     FixValue,FixValueESD,StringOfAtoms,TypeOfRestrain,FixString
      dimension xp(3),it(3)
      character*(*) AtString,ErrString
      character*256 StringOfAtoms
      character*80  t80,At1,At2
      character*8   FixString
      integer RestrainType
      save /RestrainQuest/
      ErrString=' '
      StringOfAtoms=' '
      i=0
      k=0
      kk=0
      lena=len(AtString)
2100  call kus(AtString,k,t80)
      i=i+1
      j=idel(t80)
      if(i.eq.RestrainType) then
        if(t80(j:j).eq.';') then
          t80=t80(:j-1)
          j=j-1
        else if(k.lt.lena) then
          call kus(AtString,k,Cislo)
          if(Cislo.ne.';') then
            ErrString='semicolon is missing'
            go to 9999
          endif
        endif
      endif
      call UprAt(t80)
      call AtomSymCode(t80,ia,ISym,ICentr,xp,it,ich)
      if(ich.eq.1.and.NMolec.gt.0) then
        ia=ktatmol(t80)
        if(ia.ge.NAtMolFr(1,1)) ich=0
      endif
      if(ich.ne.0) then
        if(ich.eq.1) then
          ErrString='Atom "'//t80(:idel(t80))//'" doesn''t exist'
        else if(ich.eq.2) then
          t80='The symmetry part isn''t correct : '//t80(:idel(t80))
        else
          Errstring='??? unpredictable error ???'
        endif
        go to 9999
      endif
      if(i.eq.1) then
        At1=t80
        isw1=iswa(ia)
        ksw1=kswa(ia)
        if(ia.ge.NAtMolFr(1,1)) then
          kmol1=kmol(ia)
        else
          kmol1=0
        endif
      else
        At2=t80
        if(iswa(ia).ne.isw1) then
          ErrString=' are not from the same composite part'
          go to 2120
        else if(kmol(ia).ne.kmol1.and.kmol1.ne.0.and.kmol(ia).ne.0)
     1    then
          ErrString=' are not from the same molecule'
          go to 2120
        else if(kswa(ia).ne.ksw1) then
          ErrString=' are not from the same phase'
          go to 2120
        endif
      endif
      go to 2150
2120  ErrString='"'//At1(:idel(At1))//'" "'//At2(:idel(At2))//'" '//
     1          ErrString(:idel(ErrString))
      go to 9999
2150  if(kk.eq.0) then
        StringOfAtoms=t80
        kk=j
      else
        StringOfAtoms=StringOfAtoms(:kk+1)//t80(:j)
        kk=kk+j+1
        if(i.eq.RestrainType) then
          StringOfAtoms=StringOfAtoms(:kk)//';'
          kk=kk+1
          i=0
       endif
      endif
      if(k.lt.lena) go to 2100
9999  return
      end
