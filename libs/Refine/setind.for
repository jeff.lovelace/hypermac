      subroutine SetInd(Klic,ih)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ih(6,3),ihkl(6),hhp(6),hcp(3),so(3,2),xp(3)
      real kp,lp
      do i=1,NComp(KPhase)
        if(i.eq.1.and.Klic.eq.0.and.ExtTensor(KDatBlock).eq.2) then
          do j=1,2
            pom=0.
            do k=1,3
              so(k,j)=RefDirCos(k,j)*rcp(k,1,KPhase)
              pom=pom+abs(RefDirCos(k,j))
            enddo
            if(pom.lt..001) ErrFlag=1
            call Multm(MetTens(1,1,KPhase),so,xp,3,3,1)
            if(abs(sqrt(VecOrtScal(so,xp,3))-1.).gt..1) ErrFlag=2
          enddo
          if(XDDirCos(KDatBlock).eq.1) then
            do j=1,3
              efpip(j)  =RefDirCos(j,1)/CellPar(j,1,KPhase)
              efpip(j+3)=RefDirCos(j,2)/CellPar(j,1,KPhase)
            enddo
          else
            call VecMul(so(1,1),so(1,2),xp)
            call multm(MetTensI(1,1,KPhase),xp,efpip,3,3,1)
            call vecnor(efpip,xp)
            call VecMul(so(1,1),efpip,xp)
            call multm(MetTensI(1,1,KPhase),xp,efpip(4),3,3,1)
            call vecnor(efpip(4),xp)
          endif
        endif
        do j=1,NSymmRef(KPhase)
          call IndTr(ih(1,i),rm6(1,j,i,KPhase),ihkl,NDim(KPhase))
          call IndRealFromInd(ihkl,hhp,i)
          if(j.eq.1.and.MagneticType(KPhase).ne.0) then
            call CopyVek(hhp,HMagR(1,i),3)
            call MultM(HMagR(1,i),MetTensI(1,i,KPhase),HMag(1,i),1,3,3)
            call VecNor(HMag(1,i),HMagR(1,i))
          endif
          ath(j,i)=pi2*float(ihkl(1))
          atk(j,i)=pi2*float(ihkl(2))
          atl(j,i)=pi2*float(ihkl(3))
          if(NDim(KPhase).gt.3) then
            imnp(j,i,1)=ihkl(4)
            if(NDim(KPhase).gt.4) then
              imnp(j,i,2)=ihkl(5)
              if(NDim(KPhase).gt.5) then
                imnp(j,i,3)=ihkl(6)
              endif
            endif
          endif
          if(KCommen(KPhase).gt.0) then
            trezm(j,i)=0.
            do m=4,NDim(KPhase)
              trezm(j,i)=trezm(j,i)+pi2*trez(m-3,i,KPhase)*
     1                                       float(ihkl(m))
            enddo
          endif
          hp=hhp(1)
          kp=hhp(2)
          lp=hhp(3)
          rh(j,i)=hp*pi2
          rk(j,i)=kp*pi2
          rl(j,i)=lp*pi2
          if(i.eq.1) then
            pom=0.
            do m=1,NDim(KPhase)
              pom=pom+s6(m,j,i,KPhase)*float(ih(m,i))
            enddo
            t(j)=pom*pi2
          endif
          HCoef( 5,j,i)=hp**2
          HCoef( 6,j,i)=kp**2
          HCoef( 7,j,i)=lp**2
          HCoef( 8,j,i)=2.*hp*kp
          HCoef( 9,j,i)=2.*hp*lp
          HCoef(10,j,i)=2.*kp*lp
          if(itfmax.gt.2) then
            hcp(1)=rh(j,i)
            hcp(2)=rk(j,i)
            hcp(3)=rl(j,i)
            m3=ioffm(1)
            m4=ioffm(2)
            m5=ioffm(3)
            m6=ioffm(4)
            do i1=1,3
              pom1=hcp(i1)
              do i2=i1,3
                pom2=pom1*hcp(i2)
                do i3=i2,3
                  pom3=pom2*hcp(i3)
                  m3=m3+1
                  HCoef(m3+10,j,i)=pom3/cfact(1)*cmlt(m3)
                  if(itfmax.lt.4) cycle
                  do i4=i3,3
                    pom4=pom3*hcp(i4)
                    m4=m4+1
                    HCoef(m4+10,j,i)=pom4/cfact(2)*cmlt(m4)
                    if(itfmax.lt.5) cycle
                    do i5=i4,3
                      pom5=pom4*hcp(i5)
                      m5=m5+1
                      HCoef(m5+10,j,i)=pom5/cfact(3)*cmlt(m5)
                      if(itfmax.lt.6) cycle
                      do i6=i5,3
                        m6=m6+1
                        HCoef(m6+10,j,i)=pom5*hcp(i6)/cfact(4)*cmlt(m6)
                      enddo
                    enddo
                  enddo
                enddo
              enddo
            enddo
          endif
        enddo
      enddo
9999  return
      end
