      subroutine RefDensity(CellVolPwd,CellVolPwds,Density,Densitys)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrlMagInver
      Density =0.
      Densitys=0.
      do i=1,NAtFormula(KPhase)
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atweight',pomw,0,
     1                        ich)
        if(NAtCalc.gt.0) then
          do j=1,NAtCalc
            if(kswa(j).ne.KPhase) cycle
            if(isf(j).eq.i) then
              pom=ai(j)
              if(KModA(1,j).gt.0) pom=pom*a0(j)
              if(KFA(2,j).eq.1.and.KModA(2,j).gt.0) then
                k=KModA(2,j)
                pom=pom*uy(2,k,j)
              endif
              if(iswa(j).ne.1)
     1          pom=CellVol(1,KPhase)/CellVol(iswa(j),KPhase)*pom
              Density =Density+pomw*pom
              if(ai(j).ne.0.)
     1          Densitys=Densitys+(pomw*pom/ai(j)*sai(j))**2
              if(KModA(1,j).gt.0.and.a0(j).ne.0.)
     1          Densitys=Densitys+(pomw*pom/a0(j)*sa0(j))**2
              if(KFA(2,j).eq.1.and.KModA(2,j).gt.0.and.uy(2,k,j).ne.0.)
     1          Densitys=Densitys+(pomw*pom/uy(2,k,j)*suy(2,k,j))**2
            endif
          enddo
        else
          Density=Density+pomw*AtMult(i,KPhase)
        endif
      enddo
      if(NAtCalc.gt.0) then
        pomw=float(NSymm(KPhase)*NLattVec(KPhase))
        if(CrlMagInver().ne.0) pomw=pomw*.5
      else
        pomw=float(NUnits(KPhase))
      endif
      pomw=pomw*1.e24/(CellVolPwd*AvogadroNumber)
      Density=pomw*Density
      Densitys=Density*sqrt(pomw**2*Densitys+
     1                      (CellVolPwds/CellVolPwd)**2)
      return
      end
