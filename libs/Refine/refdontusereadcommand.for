      subroutine RefDontuseReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/DontuseQuest/ nEdwGroup,nEdwDontuse,nEdwExcept,nEdwScale,
     1                     nCrwDontUse,GroupString,DontuseString,
     2                     ExceptString,Dontuse,NScale,Klic
      character*80 t80
      character*40 GroupString,DontuseString,ExceptString
      character*(*) Command
      integer ic(1)
      logical Dontuse,EqIgCase
      save /DontuseQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      if(Klic.eq.0) then
        if(EqIgCase(t80,'dontuse').or.EqIgCase(t80,'!dontuse')) then
          Dontuse=.true.
        else if(EqIgCase(t80,'useonly').or.EqIgCase(t80,'!useonly'))
     1    then
          Dontuse=.false.
        else
          go to 8010
        endif
      else if(Klic.eq.1) then
        if(EqIgCase(t80,'scale').or.EqIgCase(t80,'!scale')) then
        else
          go to 8010
        endif
        call kus(Command,k,t80)
        kk=0
        call StToInt(t80,kk,ic,1,.false.,ich)
        if(ich.ne.0) go to 8030
        NScale=ic(1)
        call kus(Command,k,t80)
        if(t80.ne.'for') go to 8010
      else if(Klic.eq.2) then
        if(EqIgCase(t80,'rfactors').or.EqIgCase(t80,'!rfactors')) then
        else
          go to 8010
        endif
      endif
      if(k.ge.lenc) go to 8000
      call kus(Command,k,GroupString)
      call TestGInd(GroupString,t80)
      if(t80.ne.' ') go to 8100
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      if(t80.ne.':') go to 8010
      call kus(Command,k,DontuseString)
      call TestCInd(DontuseString,t80)
      if(t80.ne.' ') go to 8100
      if(k.ge.lenc) go to 9999
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'except') go to 8010
      call kus(Command,k,ExceptString)
      call TestCInd(ExceptString,t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command',
     1              SeriousError)
9999  return
      end
