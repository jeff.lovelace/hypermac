      subroutine RefLoopNuluj(NMaxFlow)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      use EDZones_mod
      parameter (MxRecFac=8*28)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 t80
      character*4  Lab1
      character*12 Lab2
      write(t80,'(i5,''/'',i5)') icykl,ncykl
      call zhusti(t80)
      t80='Refining procedure - cycle '//t80(:idel(t80))
      if(Radiation(KDatBlock).eq.ElectronRadiation.and.ExistM42.and.
     1   CalcDyn) then
        if(ActionED.eq.2) then
          t80='Intensity calculation for optimizing of scale'
        else if(ActionED.eq.3) then
          t80='Intensity calculation for optimizing of orientation'
        else if(ActionED.eq.4) then
          t80='Intensity calculation for optimizing of thickness'
        else if(ActionED.eq.5) then
          t80='Intensity calculation for optimizing of orientation '//
     1        'and thickness'
        endif
      endif
      if(icykl.eq.0.and..not.jensc) then
        if(DatBlockDerTest.eq.0) then
          if(isimul.le.0) then
            pom=120.
            Lab1='%End'
            Lab2='%Parameters'
          else
            if(isPowder) then
              t80='Simulation run - powder'
            else
              t80='Simulation run - single crystal'
            endif
            pom=-1.
            Lab1=' '
            Lab2=' '
          endif
          call FeFlowChartOpen(-1.,pom,
     1      max(nint(float(NMaxFlow)*.005),10),NMaxFlow,t80,Lab1,Lab2)
          if(BatchMode.or.Console) then
            nFlowChart=1
          else
            nFlowChart=LastQuest
          endif
        else
          nFlowChart=0
        endif
      else
        if(nFlowChart.ne.0) call FeFlowChartReopen(t80)
      endif
      if(MaxNDimI.gt.0) then
        if(OrthoOrd.gt.0) call trortho(0)
        if(metoda.ne.0) then
          call calcm2(i,i,isw,-1)
          if(ErrFlag.ne.0) go to 9999
        endif
      endif
      do i=1,NDatBlock
        NSkipRef(ICyklMod6,i)=0
        NBadOverRef(ICyklMod6,i)=0
      enddo
      call RefRFacNuluj
      call SetRealArrayTo(LSRS,nLSRS,0.)
      call SetRealArrayTo(LSMat,nLSMat,0.)
      SumaYcPwd=0.
      SumaYoPwd=0.
      call SetIntArrayTo(ihmin,6, 9999)
      call SetIntArrayTo(ihmax,6,-9999)
      a=0.
      b=0.
      noread=0
      nzz=0
9999  return
      end
