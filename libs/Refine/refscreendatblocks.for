      subroutine RefScreenDatBlocks
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      do 2000i=1,NDatBlock
        if(UseDatBlockActual(i)) go to 2000
        call SetIntArrayTo(KiS(1,i),mxscAll,0)
        if(iabs(DataType(i)).eq.2) then
          call SetIntArrayTo(kipwd(IShiftPwd+(i-1)*NParRecPwd),
     1                       NParRecPwd,0)
          IZdvih=0
          do j=1,NPhase
            k=IGaussPwd+IZdvih+(i-1)*NParProfPwd
            call SetIntArrayTo(kipwd(k),NParProfPwd,0)
            IZdvih=IZdvih+NParCellProfPwd
          enddo
        endif
2000  continue
      return
      end
