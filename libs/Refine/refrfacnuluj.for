      subroutine RefRFacNuluj
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
       RNumObs=0.
       RNumAll=0.
      wRNumObs=0.
      wRNumAll=0.
       RDenObs=0.
       RDenAll=0.
      wRDenObs=0.
      wRDenAll=0.
      SumaDyQAll=0.
      SumaDyQQAll=0.
      SumaSigQAll=0.
      SumaDyQObs=0.
      SumaDyQQObs=0.
      SumaSigQObs=0.
      nRObs=0.
      nRAll=0.
      if(NRFactors.gt.0) then
         RNumPartObs=0.
         RNumPartAll=0.
         RDenPartObs=0.
         RDenPartAll=0.
        wRNumPartObs=0.
        wRNumPartAll=0.
        wRDenPartObs=0.
        wRDenPartAll=0.
        nRPartAll=0
        nRPartObs=0
         RNumPartAllObs=0.
         RNumPartAllAll=0.
         RDenPartAllObs=0.
         RDenPartAllAll=0.
        wRNumPartAllObs=0.
        wRNumPartAllAll=0.
        wRDenPartAllObs=0.
        wRDenPartAllAll=0.
        nRPartAllAll=0
        nRPartAllObs=0
      endif
      if(ExistElectronData.and.NMaxEDZone.gt.1) then
         RNumZoneObs(1:NMaxEDZone,KDatBlock)=0.
         RNumZoneAll(1:NMaxEDZone,KDatBlock)=0.
         RDenZoneObs(1:NMaxEDZone,KDatBlock)=0.
         RDenZoneAll(1:NMaxEDZone,KDatBlock)=0.
        wRNumZoneObs(1:NMaxEDZone,KDatBlock)=0.
        wRNumZoneAll(1:NMaxEDZone,KDatBlock)=0.
        wRDenZoneObs(1:NMaxEDZone,KDatBlock)=0.
        wRDenZoneAll(1:NMaxEDZone,KDatBlock)=0.
        nRZoneAll(1:NMaxEDZone,KDatBlock)=0
        nRZoneObs(1:NMaxEDZone,KDatBlock)=0
      endif
      return
      end
