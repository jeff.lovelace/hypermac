      subroutine DSetResPowder
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      logical EqIgCase
      do i=nvai,1,-1
        if(mai(i).le.0.or..not.EqIgCase(RestType(i),'profile')) cycle
        do KDatB=1,NDatBlock
          KPh1=nai(1,i)
          IZdvih1=(KPh1-1)*NParCellProfPwd+(KDatB-1)*NParProfPwd+
     1             ndoffPwd
          do k=2,mai(i),2
            KPh2=nai(k,i)
            IZdvih2=(KPh2-1)*NParCellProfPwd+(KDatB-1)*NParProfPwd+
     1              ndoffPwd
            if(KProfPwd(KPh1,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
              j1=IGaussPwd+IZdvih1
              j2=IGaussPwd+IZdvih2
              do j=1,4
                der(j1)=der(j1)+der(j2)
                der(j2)=0.
                j1=j1+1
                j2=j2+1
              enddo
            endif
            if(KProfPwd(KPh1,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh1,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
              if(KStrain(KPh1,KDatB).eq.IdPwdStrainTensor.and.
     1           KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
                ii=5
              else
                ii=4
              endif
              j1=ILorentzPwd+IZdvih1
              j2=ILorentzPwd+IZdvih2
              do j=1,ii
                der(j1)=der(j1)+der(j2)
                der(j2)=0.
                j1=j1+1
                j2=j2+1
              enddo
            endif
            j1=IZetaPwd+IZdvih1
            j2=IZetaPwd+IZdvih2
            der(j1)=der(j1)+der(j2)
            der(j2)=0.
            if(NDimI(KPh1).eq.1) then
              ii=35
            else
              ii=15+NDimI(KPhase)
            endif
            if(NDimI(KPh2).eq.NDimI(KPh1)) then
              j1=IStPwd+IZdvih1
              j2=IStPwd+IZdvih2
              do j=1,ii
                der(j1)=der(j1)+der(j2)
                der(j2)=0.
                j1=j1+1
                j2=j2+1
              enddo
            endif
          enddo
        enddo
      enddo
      return
      end
