      subroutine RefSpecAt
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension px(36),xp(6),x0(6),x1(6),x2(6),x3(6),x4(6),
     1          rm6p(36),s6pi(6),s6pj(6),vt6p(:,:)
      allocatable vt6p
      KPhaseIn=KPhase
      allocate(vt6p(MaxNDim,MaxNLattVec))
      do i=1,NAtCalc
        isw=iswa(i)
        KPhase=kswa(i)
        ismRef(i)=1
        do j=1,NSymmRef(KPhase)*NCSymmRef(KPhase)
          isaRef(j,i)=j
        enddo
        call CopyVek(x(1,i),xp,3)
        NDimp=NDim(KPhase)
        if(KFA(1,i).eq.1.and.KModA(1,i)-NDim(KPhase)+4.gt.0) then
          call CopyVek(ax(KModA(1,i)-NDim(KPhase)+4,i),xp(4),
     1                 NDimI(KPhase))
        else if(KFA(2,i).eq.1.and.KModA(2,i).gt.0) then
          xp(4)=uy(1,KModA(2,i),i)
        else
          call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
          NDimp=3
        endif
        is=0
        do isym=1,NSymmRef(KPhase)
          iswi=IswSymm(isym,isw,KPhase)
          if(iswi.eq.isw) then
            call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,isym,isw,KPhase), s6pi,NDim(KPhase))
          else
            call Multm(ZVi(1,isw,KPhase),rm6(1,isym,isw,KPhase),px,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call Multm(ZV(1,iswi,KPhase),px,rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MultM(ZVi(1,isw,KPhase),s6(1,isym,isw,KPhase),px,
     1                 NDim(KPhase),NDim(KPhase),1)
            call MultM(ZV(1,iswi,KPhase),px,s6pi,NDim(KPhase),
     1                 NDim(KPhase),1)
          endif
          call multm(rm6p,xp,x0,NDim(KPhase),NDim(KPhase),1)
          do 2900icntrsm=1,3-2*NCSymmRef(KPhase),-2
            is=is+1
            if(isaRef(is,i).lt.0) go to 2900
            do m=1,NDim(KPhase)
              x1(m)=float(icntrsm)*x0(m)+s6pi(m)
            enddo
            do 2800jsym=isym,NSymmRef(KPhase)
              js=(jsym-1)*NCSymmRef(KPhase)
              iswj=IswSymm(jsym,isw,KPhase)
              if(iswi.ne.iswj) go to 2800
              if(iswj.eq.isw) then
                call CopyMat(rm6(1,jsym,isw,KPhase),rm6p,NDim(KPhase))
                call CopyVek( s6(1,jsym,isw,KPhase),s6pj,NDim(KPhase))
                do ivt=1,NLattVec(KPhase)
                  call CopyVek(vt6(1,ivt,isw,KPhase),vt6p(1,ivt),
     1                         NDim(KPhase))
                enddo
              else
                call Multm(ZVi(1,isw,KPhase),rm6(1,jsym,isw,KPhase),px,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call Multm(ZV(1,iswj,KPhase),px,rm6p,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call MultM(ZVi(1,isw,KPhase),s6(1,jsym,isw,KPhase),px,
     1                     NDim(KPhase),NDim(KPhase),1)
                call MultM(ZV(1,iswj,KPhase),px,s6pj,NDim(KPhase),
     1                     NDim(KPhase),1)
                do ivt=1,NLattVec(KPhase)
                  call MultM(ZVi(1,isw,KPhase),vt6(1,ivt,isw,KPhase),px,
     1                       NDim(KPhase),NDim(KPhase),1)
                  call MultM(ZV(1,iswj,KPhase),px,vt6p(1,ivt),
     1                       NDim(KPhase),NDim(KPhase),1)
                enddo
              endif
2060          call multm(rm6p,xp,x2,NDim(KPhase),NDim(KPhase),1)
              do 2700jcntrsm=1,3-2*NCSymmRef(KPhase),-2
                js=js+1
                if(js.le.is.or.isaRef(js,i).lt.0) go to 2700
                do m=1,NDim(KPhase)
                  x3(m)=float(jcntrsm)*x2(m)+s6pj(m)
                enddo
                do 2080jvt=1,NLattVec(KPhase)
                  call addvek(x3,vt6p,x4,ndim(KPhase))
                  do m=1,NDimp
                    x4m=x3(m)+vt6p(m,jvt)
                    pom=x1(m)-x4m
                    apom=anint(pom)
                    if(abs(pom-apom).gt..00001) then
                      go to 2080
                    endif
                  enddo
                  isaRef(js,i)=-is
                  if(is.eq.1) ismRef(i)=ismRef(i)+1
                  go to 2700
2080            continue
2700          continue
2800        continue
2900      continue
        enddo
        if(NCSymmRef(KPhase).eq.2.and.isaRef(2,i).eq.-1)
     1    ismRef(i)=ismRef(i)/2
      enddo
      KPhase=KPhaseIn
      if(allocated(vt6p)) deallocate(vt6p)
      return
      end
