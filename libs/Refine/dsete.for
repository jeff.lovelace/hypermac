      subroutine dsete(Klic)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      do ii=1,2
        if(ii.eq.1) then
          i1=neqs+1
          i2=neq
        else
          i1=1
          i2=neqs
        endif
        do 2000i=i2,i1,-1
          lnpi=lnp(i)
          if((Klic.eq.0.and.eqhar(i)).or.
     1       (Klic.eq.1.and..not.eqhar(i)).or.
     2       (Klic.ne.2.and.lnpi.gt.ndoffPwd.and.lnpi.le.ndoff).or.
     3        npa(i).le.0) go to 2000
          pom=der(lnpi)
          do j=1,npa(i)
            k=pnp(j,i)
            if(k.gt.0) der(k)=der(k)+pko(j,i)*pom
          enddo
2000    continue
      enddo
      return
      end
