      logical function PolarX4Axis()
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      integer CrlCentroSymm
      PolarX4Axis=.false.
      if(CrlCentroSymm().gt.0.or.NDimI(KPhase).le.0) go to 9999
      do i=1,NSymmN(KPhase)
        if(rm6(16,i,1,KPhase).lt.0.) go to 9999
      enddo
      PolarX4Axis=.true.
9999  return
      end
