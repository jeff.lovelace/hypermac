      subroutine RefAppEq(ik,jk,tisk)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*12  ln
      character*20  at
      character*30  t30
      character*256 Veta
      integer tisk,Exponent10
      if(neq.le.0) go to 9999
      call BetaToU(0)
      call MagParToBohrMag(0)
      do i=1,2
        if(ik.eq.0) then
          if(i.eq.1) then
            j1=neqs+1
            j2=neq
          else
            j1=1
            j2=neqs
          endif
        else
          if(i.ne.ik) cycle
          j1=jk
          j2=jk
        endif
        if(OrthoOrd.gt.0) then
          iok=2
        else
          iok=1
        endif
        do io=1,iok
          if(io.eq.2) call trortho(0)
          do j=j1,j2
            if(eqhar(j).neqv.io.eq.2) cycle
            if(lnp(j).lt.0) then
              if(tisk.eq.0.or..not.LstOpened) cycle
              k=-lnp(j)
              if(k.eq.1) then
                at=lat(j)
                ln=lpa(j)
              else
                k=k-1
                at=pat(k,j)
                ln=ppa(k,j)
              endif
              t30=at(:idel(at))//'['//ln(:idel(ln))//']'
              call FeChybne(-1.,-1.,'parameter "'//t30(:idel(t30))//
     1                      '" not applicable.',
     2                      'The relevant equation will be omitted.',
     3                       WarningWithESC)
              if(ErrFlag.ne.0) then
                go to 9999
              else
                cycle
              endif
            endif
            if(npa(j).ge.0) then
              pom=pab(j)
              poms=0.
              do k=1,npa(j)
                if(pnp(k,j).gt.0) then
                  call kdoco(pnp(k,j),at,ln,1,p,sp)
                else
                  sp=0.
                  im=-pnp(k,j)
                  ix=mod(im,10)
                  im=im/10
                  im=(im-1)/mxp+1
                  p=xm(ix,im)
                endif
                pom=pom+p*pko(k,j)
                if(poms.ge.0.) then
                  if(sp.ge.0.) then
                    poms=poms+(sp*pko(k,j))**2
                  else
                    poms=-.1
                  endif
                endif
              enddo
              if(poms.gt.0.) poms=sqrt(poms)
              if(tisk.eq.1.and.LstOpened) then
                call kdoco(lnp(j),at,ln,1,p,sp)
                dif=max(abs(p*.000005),.000005)
                if(abs(pom-p).gt.dif) then
                  call newln(1)
                  ii=Exponent10(dif)
                  at='f15.0'
                  if(ii.lt.0) write(at(5:6),'(i2)') abs(ii)
                  t30='('//at(:idel(at))//',''$=>$'','//
     1                at(:idel(at))//')'
                  write(Veta,t30) p,pom
                  call Zhusti(Veta)
                  do ii=1,idel(Veta)
                    if(Veta(ii:ii).eq.'$') Veta(ii:ii)=' '
                  enddo
                  Veta='  ... the equation not fulfilled, the initial'//
     1                 ' value has been changed : '//Veta(:idel(Veta))
                  write(lst,FormA) Veta(:idel(Veta))
                endif
              endif
              call kdoco(lnp(j),at,ln,-1,pom,poms)
            endif
          enddo
          if(io.eq.2) call trortho(1)
        enddo
      enddo
      call UToBeta(0)
      call MagParToCell(0)
9999  return
      end
