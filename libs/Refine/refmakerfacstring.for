      subroutine RefMakeRFacString(RFacObs,wRFacObs,RFacAll,wRFacAll,
     1                             RFacString)
      include 'fepc.cmn'
      character*(*) RFacString
      if(RFacObs.le.0.) then
        RFacString=' ------ ------'
      else
        write(RFacString,100) RFacObs,wRFacObs
      endif
      if(RFacAll.le.0.) then
        RFacString(15:)=' ------ ------'
      else
        write(RFacString(15:),100) RFacAll,wRFacAll
      endif
      return
100   format(2f7.2)
      end
