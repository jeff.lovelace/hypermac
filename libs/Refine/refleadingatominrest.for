      logical function RefLeadingAtomInRest(ia)
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical EqIgCase
      RefLeadingAtomInRest=.false.
      do i=1,nvai
        if(EqIgCase(AtvAi(1,i),Atom(ia))) then
          RefLeadingAtomInRest=.true.
          go to 9999
        endif
      enddo
9999  return
      end
