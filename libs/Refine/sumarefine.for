      subroutine SumaRefine
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      i=0
      do ip=1,PosledniKiAtInd
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      do ip=PrvniKiAtMol,PosledniKiAtMol
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      do ip=PrvniKiMol,PosledniKiMol
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      do ip=PrvniKiAtMol,PosledniKiMol
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      do ip=PrvniKiAtXYZMode,PosledniKiAtXYZMode
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      do ip=PrvniKiAtMagMode,PosledniKiAtMagMode
        if(ki(ip).ne.0) then
          i=i+1
          dc(i)=der(ip)
        endif
      enddo
      im=0
      do ip=1,nLSRS
        deri=dc(ip)*wt
        do jp=1,ip
          im=im+1
          LSMat(im)=LSMat(im)+deri*dc(jp)
        enddo
        LSRS(ip)=LSRS(ip)+deri*dy
      enddo
9999  return
      end
