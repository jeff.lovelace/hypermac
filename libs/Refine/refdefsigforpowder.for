      subroutine RefDefSigForPowder
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*256 Veta
      logical CrwLogicQuest
      id=NextQuestId()
      xqd=250.
      il=3
      Veta='For powder sig(I(hkl)) calculate:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=0
      xpom=15.
      tpom=xpom+CrwXd+10.
      Veta='from error propa%gation'
      ichk=0
      igrp=1
      do i=1,4
        il=il-8
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdSigMethod))
        if(i.eq.1) then
          nCrwSigMethod=CrwLastMade
          Veta='from %profile fit'
        else if(i.eq.2) then
          Veta='ma%ximum of both'
        else if(i.eq.3) then
          Veta='mi%nimum of both'
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nCrw=nCrwSigMethod
        do i=1,4
          if(CrwLogicQuest(nCrw)) then
            NacetlInt(nCmdSigMethod)=i
            exit
          endif
          nCrw=nCrw+1
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
