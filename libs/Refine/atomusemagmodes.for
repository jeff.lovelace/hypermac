      integer function AtomUseMagModes(At)
      use Atoms_mod
      character*(*) At
      logical EqIgCase
      do i=1,NAtMagMode
        do j=1,MAtMagMode(i)
          if(EqIgCase(At,Atom(IAtMagMode(j,i)))) then
            AtomUseMagModes=i
            go to 9999
          endif
        enddo
      enddo
      AtomUseMagModes=0
9999  return
      end
