      subroutine RefInfoKolaps(lst0,*)
      use Refine_mod
      use Bessel_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      if(kolaps) then
        lst=lst0
        call newln(2)
        write(lst,'('' the last (conflict reflection) is'')')
        if(isPowder) then
          write(lst,format3)(ihread(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b
        else
          write(lst,format3)(ihread(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b
        endif
        if(DatBlockDerTest.eq.0) call FeFlowChartRemove
        call Fechybne(-1.,-1.,'desired accuracy cannot be achieved.',
     1                ' ',SeriousError)
        return1
      endif
      return
      end
