      subroutine AtSpec(iap,iak,ji,im,nap)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      integer, allocatable :: UseLS(:),HowToUseLS(:),nms(:),nmx(:),
     1                        nmb(:),nmc3(:),nmc4(:),nmc5(:),nmc6(:),
     2                        nmmg(:),OrthoSelI(:),KwSymP(:,:,:,:)
      integer TypeModFunI,GammaInt(9),kwp(3),kwz(3),nxa(7),kmezai(7),
     1        kmodai(7),PrvKi,OrdX(3),OrdU(6)
      real, allocatable :: rm6p(:,:,:,:),s6p(:,:,:,:),snm(:),csm(:),
     1                     eps(:),pb(:),pbm(:),rms(:,:,:),rms56(:),
     2                     rx(:,:),rmx(:,:,:),rmx56(:),
     3                     rb(:,:),rmb(:,:,:),rmb56(:),
     4                     rc3(:,:),rmc3(:,:,:),rmc356(:),
     5                     rc4(:,:),rmc4(:,:,:),rmc456(:),
     6                     rc5(:,:),rmc5(:,:,:),
     7                     rc6(:,:),rmc6(:,:,:),
     8                     rmg(:,:),rmmg(:,:,:),rmmg56(:)
      real GammaIntP(9),GammaIntPI(9),x0(6),x1(6),x2(6),smp(3,3),pxx(3),
     1     pxp(3),smpt(6,6),smpp(3,3),smc(784),phi(3),px(6),x0p(6),
     2     x1p(6),x2p(6)
      logical eqrv,EqIgCase,eqiv
      equivalence (nxa(2),nx),(nxa(3),nb),(nxa(4),nc3),(nxa(5),nc4),
     1            (nxa(6),nc5),(nxa(7),nc6)
      if(InvMag(KPhase).gt.0) then
        kmg=2
      else
        kmg=1
      endif
      allocate(rx(6,3),pb(56))
      call SetRealArrayTo(pb,56,0.)
      nl=NSymmL(KPhase)
      ns=NSymm(KPhase)
      nc=NComp(KPhase)
      nd=NDim(KPhase)
      ndq=NDim(KPhase)**2
      if(allocated(UseLS)) deallocate(UseLS,HowToUseLS,rm6p,s6p,KwSymP)
      allocate(UseLS(0:nl),HowToUseLS(0:nl),rm6p(ndq,ns,0:nl,nc),
     1         s6p(nd,ns,0:nl,nc),KwSymP(MaxUsedKwAll,ns,0:nl,nc))
      UseLS(0)=0
      HowToUseLS(0)=IdRestXModADP
      do isw=1,NComp(KPhase)
        do ls=0,NSymmL(KPhase)
          do jsym=1,NSymm(KPhase)
            if(ls.eq.0) then
              rm6p(1:ndq,jsym,ls,isw)=rm6(1:ndq,jsym,isw,KPhase)
               s6p(1:nd ,jsym,ls,isw)= s6(1:nd ,jsym,isw,KPhase)
               if(NDimI(KPhase).gt.0)
     1           KwSymP(1:MaxUsedKwAll,jsym,ls,isw)=
     2             KwSym(1:MaxUsedKwAll,jsym,isw,KPhase)
            else
              call multm(rm6l(1,ls,isw,KPhase),rm6(1,jsym,isw,KPhase),
     2                   rm6p(1,jsym,ls,isw),nd,nd,nd)
              s6p(1:nd,jsym,ls,isw)=s6l(1:nd,ls,isw,KPhase)
              call cultm(rm6l(1,ls,isw,KPhase),s6(1,jsym,isw,KPhase),
     1                   s6p(1,jsym,ls,isw),nd,nd,1)
              do j=4,nd
                do k=4,nd
                  GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1              rm6p(k+(j-1)*nd,jsym,ls,isw)
                enddo
              enddo
              call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
              do j=1,NDimI(KPhase)**2
                GammaInt(j)=nint(GammaIntPI(j))
              enddo
              do 1700j=1,MaxUsedKwAll
                call multmi(kw(1,j,KPhase),GammaInt,kwp,1,NDimI(KPhase),
     1                      NDimI(KPhase))
                do k=1,NDimI(KPhase)
                  kwz(k)=-kwp(k)
                enddo
                do k=1,MaxUsedKwAll
                  if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                    KwSymP(j,jsym,ls,isw)=k
                    go to 1700
                  else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                    KwSymP(j,jsym,ls,isw)=-k
                    go to 1700
                  endif
                enddo
                KwSymP(j,jsym,ls,isw)=0
1700          continue
            endif
          enddo
        enddo
      enddo
      if(NDimI(KPhase).gt.0) then
        n=4*TRank(itfMax)
        if(MagParMax.gt.1) n=max(n,12)
        if(OrthoOrd.gt.0) then
          m=OrthoOrd
        else
          m=1
        endif
        allocate(snm(MaxUsedKwAll),csm(MaxUsedKwAll),
     1           eps(MaxUsedKwAll),pbm(n*MaxUsedKwAll),OrthoSelI(m))
        call SetRealArrayTo(pbm,n*MaxUsedKwAll,0.)
        m=1
        do i=iap,iak
          if(TypeModFun(i).ne.1) cycle
          do n=2,itf(i)+1
            if(KFA(n,i).eq.0.or.KModA(n,i).eq.0.or.NDimI(KPhase).gt.1)
     1        then
              k=KModA(n,i)
            else
              k=KModA(n,i)-1
            endif
            m=max(m,(OrthoSel(2*k+1,i)+1)/2)
          enddo
        enddo
        n=max(m,MaxUsedKwAll)
        if(allocated(nms)) deallocate(nms,nmx,nmb)
        allocate(nms(n),nmx(n),nmb(n))
        if(NDimI(KPhase).eq.1) then
          allocate(rms(4,2,n),rmx(12,6,n))
        else if(NDimI(KPhase).gt.1) then
          allocate(rms56(8*n**2),rmx56(72*n**2))
        endif
      endif
      if(MagneticType(KPhase).gt.0) then
        allocate(rmg(6,3))
        if(NDimI(KPhase).ge.1) allocate(nmmg(n))
        if(NDimI(KPhase).eq.1) then
          allocate(rmmg(12,6,n))
        else if(NDimI(KPhase).gt.1) then
          allocate(rmmg56(72*n**2))
        endif
      endif
      if(MaxUsedItfAll.gt.1) then
        allocate(rb(12,6))
        if(NDimI(KPhase).eq.1) then
          allocate(rmb(24,12,n))
        else if(NDimI(KPhase).gt.1) then
          allocate(rmb56(288*n**2))
        endif
        if(MaxUsedItfAll.gt.2) then
          allocate(rc3(20,10))
          if(NDimI(KPhase).eq.1) then
            allocate(rmc3(40,20,n),nmc3(n))
          else if(NDimI(KPhase).gt.1) then
            allocate(rmc356(800*n**2),nmc3(n))
          endif
          if(MaxUsedItfAll.gt.3) then
            allocate(rc4(30,15))
            if(NDimI(KPhase).eq.1) then
              allocate(rmc4(60,30,n),nmc4(n))
            else if(NDimI(KPhase).gt.1) then
              allocate(rmc456(2700*n**2),nmc4(n))
            endif
            if(MaxUsedItfAll.gt.4) then
              allocate(rc5(42,21))
              if(NDimI(KPhase).eq.1) then
                allocate(rmc5(84,42,n),nmc5(n))
              else if(NDimI(KPhase).gt.1) then

              endif
              if(MaxUsedItfAll.gt.5) then
                allocate(rc6(56,28))
                if(NDimI(KPhase).eq.1) then
                  allocate(rmc6(112,56,n),nmc6(n))
                else if(NDimI(KPhase).gt.1) then

                endif
              endif
            endif
          endif
        endif
      endif
      ia=nap
      do i=iap,iak
        if(ChargeDensities) then
          if(SmbPGAt(i).eq.'1'.or.SmbPGAt(i).eq.'C1') NPGAt(i)=0
        endif
        isw=iswa(i)
        itfi=itf(i)
        if(itfi.eq.0) itfi=2
        if(KModA(1,i).gt.0) then
          kfsi=KFA(1,i)
        else
          kfsi=0
        endif
        if(KModA(2,i).gt.0) then
          kfxi=KFA(2,i)
        else
          kfxi=0.
        endif
        TypeModFunI=TypeModFun(i)
        kmodmx=0
        call SetRealArrayTo(px,6,0.)
        call SetIntArrayTo(kmezai,7,0)
        MagPari=MagPar(i)
        if(NDimI(KPhase).gt.0) then
          if(OrthoOrd.gt.0) then
            call CopyVekI(OrthoSel(1,i),OrthoSelI,OrthoOrd)
          else
            OrthoSelI(1)=1
          endif
        endif
        do n=1,itfi+1
          nxa(n)=0
          nrank=TRank(n-1)
          nrank2q=2*nrank**2
          if(n.eq.1) then
            kmodai(n)=KModA(n,i)
          else
            if(KFA(n,i).eq.0.or.KModA(n,i).eq.0.or.NDimI(KPhase).gt.1)
     1        then
              k=KModA(n,i)
            else
              k=KModA(n,i)-1
            endif
            if(TypeModFunI.ne.1) then
              kmodai(n)=KModA(n,i)
            else
              kmodai(n)=(OrthoSelI(2*k+1)+1)/2
            endif
          endif
          if(KFA(n,i).eq.0.or.KModA(n,i).eq.0) then
            kmezai(n)=kmodai(n)
          else
            kmezai(n)=kmodai(n)-NDimI(KPhase)
          endif
          kmodmx=max(kmodmx,kmezai(n))
          if(n.eq.2) then
            call SetRealArrayTo(rx,nrank2q,0.)
          else if(n.eq.3) then
            call SetRealArrayTo(rb,nrank2q,0.)
          else if(n.eq.4) then
            call SetRealArrayTo(rc3,nrank2q,0.)
          else if(n.eq.5) then
            call SetRealArrayTo(rc4,nrank2q,0.)
          else if(n.eq.6) then
            call SetRealArrayTo(rc5,nrank2q,0.)
          else if(n.eq.7) then
            call SetRealArrayTo(rc6,nrank2q,0.)
          endif
          if(NDimI(KPhase).eq.1) then
            nrank=nrank*2
            nrank2q=2*nrank**2
            kmez=kmezai(n)
            kmezm=kmezai(n)*nrank2q
            if(n.eq.1) then
              call SetIntArrayTo(nms,kmez,0)
              call SetRealArrayTo(rms,kmezm,0.)
            else if(n.eq.2) then
              call SetIntArrayTo(nmx,kmez,0)
              call SetRealArrayTo(rmx,kmezm,0.)
            else if(n.eq.3) then
              call SetIntArrayTo(nmb,kmez,0)
              call SetRealArrayTo(rmb,kmezm,0.)
            else if(n.eq.4) then
              call SetIntArrayTo(nmc3,kmez,0)
              call SetRealArrayTo(rmc3,kmezm,0.)
            else if(n.eq.5) then
              call SetIntArrayTo(nmc4,kmez,0)
              call SetRealArrayTo(rmc4,kmezm,0.)
            else if(n.eq.6) then
              call SetIntArrayTo(nmc5,kmez,0)
              call SetRealArrayTo(rmc5,kmezm,0.)
            else if(n.eq.7) then
              call SetIntArrayTo(nmc6,kmez,0)
              call SetRealArrayTo(rmc6,kmezm,0.)
            endif
          else if(NDimI(KPhase).gt.1) then
            if(n.eq.1) then
              nms(1)=0
            else if(n.eq.2) then
              nmx(1)=0
            else if(n.eq.3) then
              nmb(1)=0
            else if(n.eq.4) then
              nmc3(1)=0
            else if(n.eq.5) then
              nmc4(1)=0
            else if(n.eq.6) then
              nmc5(1)=0
            else if(n.eq.7) then
              nmc6(1)=0
            endif
            do j=1,8*(nrank*kmezai(n))**2
              if(n.eq.1) then
                rms56(j)=0.
              else if(n.eq.2) then
                rmx56(j)=0.
              else if(n.eq.3) then
                rmb56(j)=0.
              else if(n.eq.4) then
                rmc356(j)=0.
              else if(n.eq.5) then
                rmc456(j)=0.
              endif
            enddo
          endif
        enddo
        kmodmx=max(kmodmx,MagPari-1)
        if(MagPari.ne.0) then
          nmg=0
          call SetRealArrayTo(rmg,18,0.)
          if(NDimI(KPhase).eq.1) then
            call SetIntArrayTo(nmmg,MagPari-1,0)
            n=(MagParI-1)*72
            call SetRealArrayTo(rmmg,n,0.)
          else if(NDimI(KPhase).gt.1) then
            nmmg(1)=0
            do j=1,8*(3*(MagParI-1))**2
              rmmg56(j)=0.
            enddo
          endif
        endif
        if(ji.eq.0) then
          call CopyVek(x(1,i),x0,3)
        else
          call CopyVek(x(1,ia),x0,3)
        endif
        NDimp=NDim(KPhase)
        if(kfsi.gt.0) then
          call CopyVek(ax(KModA(1,i)-NDimI(KPhase)+1,i),x0(4),
     1                 NDimI(KPhase))
        else if(kfxi.eq.1) then
          x0(4)=uy(1,KModA(2,i),i)
        else
          call SetRealArrayTo(x0(4),NDimI(KPhase),0.)
          NDimp=3
        endif
        if(NDimp.gt.3) then
          call CopyVek(x0,x0p,3)
          call SetRealArrayTo(x0p(4),NDimI(KPhase),0.)
        endif
        NUseLS=0
        do j=1,nvai
          if(EqIgCase(atvai(1,j),Atom(i)).and.mai(j).eq.1.and.
     1       nails(j).gt.0) then
            NUseLS=NUseLS+1
            UseLS(NUseLS)=nails(j)
            HowToUseLS(NUseLS)=mod(iabs(naixb(j)),10)
          endif
        enddo
        do lssym=0,NUseLS
          ls=UseLS(lssym)
          if(lssym.eq.0) then
            Diff=.001
          else
            Diff=.1
          endif
          do jsym=1,NSymm(KPhase)
            call multm(rm6p(1,jsym,ls,isw),x0,x1,NDim(KPhase),
     1                 NDim(KPhase),1)
            if(NDimp.gt.3)
     1        call multm(rm6p(1,jsym,ls,isw),x0p,x1p,NDim(KPhase),
     2                   NDim(KPhase),1)
            do j=1,NDim(KPhase)
                x2(j)=x1(j)+s6p(j,jsym,ls,isw)
              if(NDimp.gt.3) x2p(j)=x1p(j)+s6p(j,jsym,ls,isw)
            enddo
            do 2980ivt=1,NLattVec(KPhase)
              call SetRealArrayTo(px(nx+1),6-nx,0.)
              do j=1,NDimp
                x3j=x2(j)+vt6(j,ivt,isw,KPhase)
                pom=x0(j)-x3j
                if(j.le.3) then
                  DiffP=Diff/CellPar(j,isw,KPhase)
                else
                  DiffP=Diff/10.
                  if(InvMag(KPhase).gt.0) pom=pom-s6p(j,jsym,ls,isw)
                endif
                apom=anint(pom)
                if(abs(pom-apom).gt.DiffP) go to 2980
                if(j.le.3) px(nx+4-j)=-s6p(j,jsym,ls,isw)
     1                                -vt6(j,ivt,isw,KPhase)-apom
              enddo
              if(ChargeDensities) then
                if(SmbPGAt(i).eq.'1'.or.SmbPGAt(i).eq.'C1') then
                  NPGAt(i)=NPGAt(i)+1
                  call MatBlock3(rm6p(1,jsym,ls,isw),
     1                           RPGAt(1,NPGAt(i),i),NDim(KPhase))
                endif
              endif
              if(NDimI(KPhase).gt.0) then
                do j=1,NDimI(KPhase)
                  if(NDimp.gt.3) then
                    phi(j)=-x2p(j+3)-vt6(j+3,ivt,isw,KPhase)
                  else
                    phi(j)=-x2 (j+3)-vt6(j+3,ivt,isw,KPhase)
                  endif
                enddo
                if(ji.ne.0) then
                  do j=1,3
                    pxx(j)=xm(j,im)-x(j,i)
                  enddo
                  call multm(RotMol(1,ji),pxx,pxp,3,3,1)
                  do j=1,NDimI(KPhase)
                    do k=1,3
                      phi(j)=phi(j)-
     1                  rm6p(j+3+NDim(KPhase)*(k-1),jsym,ls,isw)*pxp(k)
                    enddo
                  enddo
                endif
                do j=1,NDimI(KPhase)
                  phi(j)=pi2*phi(j)
                enddo
                do j=1,kmodmx
                  l=KwSymP(j,jsym,ls,isw)
                  if(l.eq.0) then
                    call FeChybne(-1.,-1.,'the set of modulation waves '
     1                          //'isn''t closed.','Select another set '
     2                          //'and try again.',SeriousError)
                    ErrFlag=1
                    go to 9000
                  endif
                  eps(j)=isign(1,l)
                  if(TypeModFun(i).le.1.or.
     1               (kmg.eq.2.and.mod(j,kmg).eq.1)) then
                    l=iabs(l)
                    pom=0.
                    do k=1,NDimI(KPhase)
                      pom=pom+float(kw(k,l,KPhase))*phi(k)
                    enddo
                    snm(j)=sin(pom)
                    csm(j)=cos(pom)
                  else
                    snm(j)=0.
                    csm(j)=1.
                  endif
                enddo
              endif
              do n=1,itfi+1
                nrank=TRank(n-1)
                kmez=kmezai(n)
                if(n.gt.3) call srotc(smp,n-1,smc)
                if(n.eq.1) then
                  smp(1,1)=1.
                  do j=1,kmez
                    if(NDimI(KPhase).eq.1) then
                      call modspec(rms(1,1,j),pbm,nms(j),nrank,1,smp,1,
     1                             1,snm(j),csm(j),eps(j),ji)
                    else
                      l=iabs(KwSymP(j,jsym,ls,isw))
                      call modspec(rms56,pbm,nms(1),nrank,kmezai(1),smp,
     1                             l,j,snm(j),csm(j),eps(j),ji)
                    endif
                  enddo
                else if(n.eq.2) then
                  do l=1,3
                    do m=1,3
                      smp(l,m)=rm6p(l+NDim(KPhase)*(m-1),jsym,ls,isw)
                    enddo
                  enddo
                  if(ji.gt.0) then
                    call multm(RotMol(1,ji),xm(1,im),pxp,3,3,1)
                    do l=1,3
                      pxp(l)=pxp(l)-trans(l,ji)-xm(l,im)
                      pxx(l)=-pxp(l)+px(nx+4-l)
                    enddo
                    call cultm(smp,pxp,pxx,3,3,1)
                    call multm(RotiMol(1,ji),pxx,pxp,3,3,1)
                    do l=1,3
                      px(nx+4-l)=pxp(l)
                    enddo
                    call multm(RotiMol(1,ji),smp,smpp,3,3,3)
                    call multm(smpp,RotMol(1,ji),smp,3,3,3)
                  endif
                  if(HowToUseLS(lssym).eq.IdRestXModADP)
     1              call AveSpec(rx,px,nx,ji,smp,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      if(NDimI(KPhase).eq.1) then
                        call modspec(rmx(1,1,j),pbm,nmx(j),nrank,1,smp,
     1                               1,1,snm(j),csm(j),eps(j),ji)
                      else
                        l=iabs(KwSymP(j,jsym,ls,isw))
                        call modspec(rmx56,pbm,nmx(1),nrank,kmez,smp,l,
     1                               j,snm(j),csm(j),eps(j),ji)
                      endif
                    enddo
                  endif
                else if(n.eq.3) then
                  call srotb(smp,smp,smpt)
                  do l=1,6
                    do m=1,6
                      if(m.ne.l) smpt(l,m)=smpt(l,m)*urcp(m,isw,KPhase)/
     1                                               urcp(l,isw,KPhase)
                    enddo
                  enddo
                  if(HowToUseLS(lssym).le.IdRestADP)
     1              call AveSpec(rb,pb,nb,ji,smpt,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      if(NDimI(KPhase).eq.1) then
                        call modspec(rmb(1,1,j),pbm,nmb(j),nrank,1,smpt,
     1                               1,1,snm(j),csm(j),eps(j),ji)
                      else
                        l=iabs(KwSymP(j,jsym,ls,isw))
                        call modspec(rmb56,pbm,nmb(1),nrank,kmez,smpt,l,
     1                               j,snm(j),csm(j),eps(j),ji)
                      endif
                    enddo
                  endif
                else if(n.eq.4) then
                  if(HowToUseLS(lssym).le.IdRestADP)
     1              call AveSpec(rc3,pb,nc3,ji,smc,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      if(NDimI(KPhase).eq.1) then
                        call modspec(rmc3(1,1,j),pbm,nmc3(j),nrank,1,
     1                               smc,1,1,snm(j),csm(j),eps(j),ji)
                      else
                        l=iabs(KwSymP(j,jsym,ls,isw))
                        call modspec(rmc356,pbm,nmc3(1),nrank,kmez,smc,
     1                               l,j,snm(j),csm(j),eps(j),ji)
                      endif
                    enddo
                  endif
                else if(n.eq.5) then
                  if(HowToUseLS(lssym).le.IdRestADP)
     1              call AveSpec(rc4,pb,nc4,ji,smc,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      if(NDimI(KPhase).eq.1) then
                        call modspec(rmc4(1,1,j),pbm,nmc4(j),nrank,1,
     1                               smc,1,1,snm(j),csm(j),eps(j),ji)
                      else
                        l=iabs(KwSymP(j,jsym,ls,isw))
                        call modspec(rmc456,pbm,nmc4(1),nrank,kmez,smc,
     1                               l,j,snm(j),csm(j),eps(j),ji)
                      endif
                    enddo
                  endif
                else if(n.eq.6) then
                  if(HowToUseLS(lssym).le.IdRestADP)
     1              call AveSpec(rc5,pb,nc5,ji,smc,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      call modspec(rmc5(1,1,j),pbm,nmc5(j),nrank,1,smc,
     1                             1,1,snm(j),csm(j),eps(j),ji)
                    enddo
                  endif
                else
                  if(HowToUseLS(lssym).le.IdRestADP)
     1              call AveSpec(rc6,pb,nc6,ji,smc,nrank)
                  if(HowToUseLS(lssym).le.IdRestModADP) then
                    do j=1,kmez
                      call modspec(rmc6(1,1,j),pbm,nmc6(j),nrank,1,smc,
     1                             1,1,snm(j),csm(j),eps(j),ji)
                    enddo
                  endif
                endif
              enddo
              if(MagPari.ne.0.and.lssym.eq.0.and.KUsePolar(i).eq.0) then
                call AveSpec(rmg,pb,nmg,0,RMag(1,jsym,isw,KPhase),3)
                do j=1,MagPari-1
                  if(NDimI(KPhase).eq.1) then
                    call modspec(rmmg(1,1,j),pbm,nmmg(j),3,1,
     1                           RMag(1,jsym,isw,KPhase),1,1,snm(j),
     2                           csm(j),eps(j),ji)
                  else
                    l=iabs(KwSymP(j,jsym,ls,isw))
                    call modspec(rmmg56,pbm,nmmg(1),3,MagPari-1,
     1                           RMag(1,jsym,isw,KPhase),l,
     2                           j,snm(j),csm(j),eps(j),ji)
                  endif
                enddo
              endif
2980        continue
          enddo
        enddo
        if(ji.ne.0.) then
          if(npoint(im).gt.1) then
            do m=1,6
              if(m.le.3) OrdX(m)=4-m
              OrdU(m)=7-m
            enddo
            do jsym=2,npoint(im)
              call SetRealArrayTo(px(nx+1),6-nx,0.)
              call multm(rpoint(1,jsym,im),x(1,i),x2,3,3,1)
              call AddVek(x2,spoint(1,jsym,im),x2,3)
              if(.not.eqrv(x(1,i),x2,3,.001)) cycle
              k=0
              do m=1,3
                do l=1,3
                  k=k+1
                  if(m.eq.l) then
                    pom=rpoint(k,jsym,im)-1.
                  else
                    pom=rpoint(k,jsym,im)
                  endif
                  rx(nx+4-l,OrdX(m))=pom
                enddo
                px(nx+4-m)=-spoint(m,jsym,im)
              enddo
              call UprSpecOrd(rx,px,3,nx,OrdX)
              if(itfi.ge.2) then
                k=0
                do m=1,6
                  do l=1,6
                    k=k+1
                    if(m.eq.l) then
                      rb(nb+7-l,OrdU(m))=tpoint(k,jsym,im)-1.
                    else
                      if(lite(KPhase).eq.0) then
                        pom=urcp(m,isw,KPhase)/urcp(l,isw,KPhase)
                      else
                        pom=1.
                      endif
                      rb(nb+7-l,OrdU(m))=tpoint(k,jsym,im)*pom
                    endif
                  enddo
                enddo
                call UprSpecOrd(rb,pb,6,nb,OrdU)
              endif
            enddo
          endif
        endif
        PrvKi=PrvniKiAtomu(i)
        do n=1,itfi
          nrank=TRank(n)
          if(n.eq.1) then
            if(ji.ne.0) then
              if(npoint(im).gt.1)
     1          call speceqOrd(rx,px,nx,nrank,PrvKi,OrdX)
            else
              call speceq(rx,px,nx,nrank,PrvKi)
            endif
          else if(n.eq.2) then
            if(ji.ne.0) then
              if(npoint(im).gt.1)
     1          call SpeceqOrd(rb,pb,nb,nrank,PrvKi,OrdU)
            else
              call speceq(rb,pb,nb,nrank,PrvKi)
            endif
          else if(n.eq.3) then
            call speceq(rc3,pb,nc3,nrank,PrvKi)
          else if(n.eq.4) then
            call speceq(rc4,pb,nc4,nrank,PrvKi)
          else if(n.eq.5) then
            call speceq(rc5,pb,nc5,nrank,PrvKi)
          else
            call speceq(rc6,pb,nc6,nrank,PrvKi)
          endif
          if(ErrFlag.ne.0) go to 9000
          PrvKi=PrvKi+nrank
        enddo
        if(itfi.le.1) PrvKi=PrvKi+6
        kmodmxp=0
        if(NDimI(KPhase).eq.1) then
          do n=1,itfi+1
            nrank=TRank(n-1)
            nrank2=2*nrank
            kmez=kmezai(n)
            kmod=kmodai(n)
            kmodmxp=max(kmodmxp,KModA(n,i))
            if(kmez.le.0.and.n.ne.1) go to 4170
            kwo=2*KModA(n,i)
            if(kwo.gt.0.and.KFA(n,i).ne.0) kwo=kwo-2*NDimI(KPhase)
            if(n.eq.1) then
              if(kmod.gt.0) PrvKi=PrvKi+1
              do j=1,kmez
                call speceq(rms(1,1,j),pbm,nms(j),nrank2,PrvKi)
                if(ErrFlag.ne.0) go to 9000
                PrvKi=PrvKi+nrank2
              enddo
            else if(n.eq.2) then
              call SpecEqM(rmx,pbm,nmx,nrank,kmez,TypeModFunI,OrthoSelI,
     1                     kwo,PrvKi)
            else if(n.eq.3) then
              call SpecEqM(rmb,pbm,nmb,nrank,kmez,TypeModFunI,OrthoSelI,
     1                     kwo,PrvKi)
            else if(n.eq.4) then
              call SpecEqM(rmc3,pbm,nmc3,nrank,kmez,TypeModFunI,
     1                     OrthoSelI,kwo,PrvKi)
            else if(n.eq.5) then
              call SpecEqM(rmc4,pbm,nmc4,nrank,kmez,TypeModFunI,
     1                     OrthoSelI,kwo,PrvKi)
            else if(n.eq.6) then
              call SpecEqM(rmc5,pbm,nmc5,nrank,kmez,TypeModFunI,
     1                     OrthoSelI,kwo,PrvKi)
            else
              call SpecEqM(rmc6,pbm,nmc6,nrank,kmez,TypeModFunI,
     1                     OrthoSelI,kwo,PrvKi)
            endif
            if(ErrFlag.ne.0) go to 9000
4170        if(kmez.ne.kmod) PrvKi=PrvKi+nrank2*(kmod-kmez)
          enddo
          if(kmodmxp.gt.0) PrvKi=PrvKi+1
        else if(NDimI(KPhase).gt.1) then
          if(kmodai(1).gt.0) then
            kmodmxp=max(kmodmxp,KModA(1,i))
            PrvKi=PrvKi+1
            if(kmezai(1).gt.0) call speceq(rms56,pbm,nms(1),2*kmezai(1),
     1                                     PrvKi)
            if(ErrFlag.ne.0) go to 9000
            PrvKi=PrvKi+2*kmodai(1)
          endif
          call speceq(rmx56,pbm,nmx(1),6*kmezai(2),PrvKi)
          kmodmxp=max(kmodmxp,KModA(2,i))
          PrvKi=PrvKi+6*kmodai(2)
          if(ErrFlag.ne.0) go to 9000
          if(itfi.ge.2) then
            call speceq(rmb56,pbm,nmb(1),12*kmezai(3),PrvKi)
            if(ErrFlag.ne.0) go to 9000
            kmodmxp=max(kmodmxp,KModA(3,i))
            if(itfi.ge.3) then
              PrvKi=PrvKi+12*kmodai(3)
              call speceq(rmc356,pbm,nmc3(1),20*kmezai(4),PrvKi)
            endif
            if(itfi.ge.4) then
              PrvKi=PrvKi+20*kmodai(4)
              call speceq(rmc456,pbm,nmc4(1),30*kmezai(5),PrvKi)
            endif
          endif
          if(kmodmxp.gt.0) PrvKi=PrvKi+1
        endif
        if(MagPari.gt.0.and.KUsePolar(i).eq.0) then
          call speceq(rmg,pb,nmg,3,PrvKi)
          if(MagPari.gt.1) then
            if(NDimI(KPhase).eq.1) then
              PrvKi=PrvKi+3
              if(MagPari.gt.1)
     1          call SpecEqM(rmmg,pbm,nmmg,3,MagPari-1,0,OrthoSelI,0,
     2                       PrvKi)
            else
              PrvKi=PrvKi+3
              if(MagPari.gt.1)
     1          call speceq(rmmg56,pbm,nmmg(1),6*(MagPari-1),PrvKi)
            endif
          endif
        endif
        ia=ia+1
      enddo
      if(ChargeDensities) call MultSpec(iap,iak)
9000  if(allocated(rx)) deallocate(rx,pb)
      if(allocated(nms)) deallocate(nms,nmx,nmb)
      if(allocated(snm)) deallocate(snm,csm,eps,pbm,OrthoSelI)
      if(allocated(rms)) deallocate(rms,rmx)
      if(allocated(rms56)) deallocate(rms56,rmx56)
      if(allocated(rb)) deallocate(rb)
      if(allocated(rmb)) deallocate(rmb)
      if(allocated(rmb56)) deallocate(rmb56)
      if(allocated(rc3)) deallocate(rc3)
      if(allocated(rmc3)) deallocate(rmc3,nmc3)
      if(allocated(rmc356)) deallocate(rmc356,nmc3)
      if(allocated(rc4)) deallocate(rc4)
      if(allocated(rmc4)) deallocate(rmc4,nmc4)
      if(allocated(rmc456)) deallocate(rmc456,nmc4)
      if(allocated(rc5)) deallocate(rc5)
      if(allocated(rmg)) deallocate(rmg)
      if(allocated(rmmg)) deallocate(rmmg,nmmg)
      if(allocated(rmmg56)) deallocate(rmmg56,nmmg)
      if(allocated(UseLS)) deallocate(UseLS,HowToUseLS,rm6p,s6p,KwSymP)
      return
      end
