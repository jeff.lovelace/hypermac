      subroutine DSetMagMode
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      kip=PrvniKiAtMagMode
      do i=1,NAtMagMode
        do j=1,NMAtMagMode(i)
          der(kip)=0.
          kt=KAtMagMode(j,i)
          pom=FAtMagMode(j,i)
          m=1
          do k=1,MAtMagMode(i)
            ia=IAtMagMode(k,i)
            kiia=PrvniKiAtomu(ia)+max(TRankCumul(itf(ia)),10)-1
            do l=1,3
              der(kip)=der(kip)+pom*MagAMode(m,kt)*der(kiia+l)
              m=m+1
            enddo
          enddo
          kip=kip+1
        enddo
      enddo
      return
      end
