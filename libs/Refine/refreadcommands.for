      subroutine RefReadCommands
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension xp(:,:),IAtom(:),RmAt(:,:),Rm6At(:,:),KiAtX(:),
     1          KiAtXMod(:),RtwiTr(:),RmiiAt(:,:),
     2          IAtom1(:),RmAt1(:,:),Rm6At1(:,:),KiAtX1(:),
     3          xp1(:,:),KiAtXMod1(:),RtwiTr1(:),RmiiAt1(:,:),
     4          xpp(18),ic(3),x1(3),x2(3),x3(3),u(3),ug(3),v(3),vg(3)
      character*256 Veta
      character*80 t80,AtName(:),AtName1(:)
      character*20 at
      character*12 AtFix,PnFix
      character*8  atv(10)
      logical PrvniDistFix,PrvniAngFix,PrvniTorsFix,Prvni,EqIgCase
      allocatable xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     1            AtName,xp1,IAtom1,RmAt1,Rm6At1,KiAtX1,KiAtXMod1,
     2            RtwiTr1,RmiiAt1,AtName1
      data sdfix,safix/0.001,0.01/
      NFixDist=0
      lnfd=NextLogicNumber()
      call OpenFile(lnfd,fln(:ifln)//'_fix_dist.tmp','formatted',
     1              'unknown')
      NFixAngle=0
      lnfa=NextLogicNumber()
      call OpenFile(lnfa,fln(:ifln)//'_fix_angle.tmp','formatted',
     1              'unknown')
      NFixTorsion=0
      lnft=NextLogicNumber()
      call OpenFile(lnft,fln(:ifln)//'_fix_torsion.tmp','formatted',
     1              'unknown')
      NEqDist=0
      lned=NextLogicNumber()
      call OpenFile(lned,fln(:ifln)//'_equal_dist.tmp','formatted',
     1              'unknown')
      NEqAngle=0
      lnea=NextLogicNumber()
      call OpenFile(lnea,fln(:ifln)//'_equal_angle.tmp','formatted',
     1              'unknown')
      NEqTorsion=0
      lnet=NextLogicNumber()
      call OpenFile(lnet,fln(:ifln)//'_equal_torsion.tmp','formatted',
     1              'unknown')
      lnda=0
      lnfix=0
      i1=1
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('refine',i)
      if(i.ne.1) go to 9999
      i=NactiInt+NactiReal+20
      call SetIntArrayTo(NactiRepeat,i,0)
      call SetIntArrayTo(NactiRepeat(i+1),NactiKeys-i,-1)
      call SetIntArrayTo(NactiRepeat(nCmdiext),5,-1)
      PreskocVykricnik=.true.
      PrvniDistFix=.true.
      PrvniAngFix=.true.
      PrvniTorsFix=.true.
      ie=1
      call DeleteFile(fln(:ifln)//'.l95')
      NFixOrigin =0
      NFixX4=0
      NUseDatBlockInRefine=0
      izpet=0
      call RefKeepGRigidReset
      go to 1100
1050  izpet=-1
1100  call NactiCommon(m50,izpet)
      if(ErrFlag.ne.0) go to 9999
      if(izpet.ne.nCmddistfix) PrvniDistFix=.true.
      if(izpet.ne.nCmdanglefix) PrvniAngFix=.true.
1110  if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntRefine ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealRefine,NactiReal)
        go to 9000
      else if(izpet.eq.nCmdExtLabel) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) ie
        go to 1050
      else if(izpet.eq.nCmdiext) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) ExtTensor(ie)
        go to 1050
      else if(izpet.eq.nCmditypex) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) ExtType(ie)
        go to 1050
      else if(izpet.eq.nCmdidistr) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) ExtDistr(ie)
        go to 1050
      else if(izpet.eq.nCmdrrext) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,1)
        read(cislo,102,err=8500) ExtRadius(ie)
        go to 1050
      else if(izpet.eq.nCmdomdif.or.izpet.eq.nCmdthdif.or.
     1        izpet.eq.nCmdchidif) then
        call StToReal(NactiVeta,PosledniPozice,xpp,2,.false.,ich)
        if(ich.ne.0) go to 8500
        if(izpet.eq.nCmdomdif) then
          call CopyVek(xpp,omdif,2)
        else if(izpet.eq.nCmdthdif) then
          call CopyVek(xpp,thdif,2)
        else
          call CopyVek(xpp,chidif,2)
        endif
      else if(izpet.eq.nCmdabsorb) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) j
        iab=j
        if(iab.eq.0) go to 1100
        read(m50,*,err=2250)(aa(j),j=1,10)
        read(m50,*,err=2250)(ba(j),j=1,10)
        read(m50,*,err=2250)(dadmi(j),j=1,19)
        go to 1100
2250    iab=0
        go to 8500
      else if(izpet.eq.nCmddfoftw) then
        call StToReal(NactiVeta,PosledniPozice,difh,NDim(KPhase),
     1                .false.,ich)
        if(ich.eq.0) go to 1100
        call SetRealArrayTo(difh,NDim(KPhase),.001)
        go to 8500
      else if(izpet.eq.nCmdcheckran) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=2350) nchkr
        read(m50,*,err=2350)(((ormat(i,j,k),j=1,3),i=1,3),k=1,nchkr)
        if(nchkr.eq.1) then
          do i=2,NTwin
            call trmat(Rtwi(1,i),RtwiTr,3,3)
            call multm(ormat(1,1,1),RtwiTr,ormat(1,1,i),3,3,3)
          enddo
        endif
        go to 1100
2350    nchkr=0
        go to 8500
      else if(izpet.eq.nCmdskipflag) then
2420    if(PosledniPozice.ge.len(NactiVeta)) go to 1100
        if(nxxn.lt.20) then
          nxxn=nxxn+1
          call kus(NactiVeta,PosledniPozice,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=2430) nxxp(nxxn)
          go to 2420
2430      nxxn=nxxn-1
          go to 8500
        endif
      else if(izpet.eq.nCmdngrid) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) ngrid(1)
        k=PosledniPozice
        do i=2,NDimI(KPhase)
          call kus(NactiVeta,k,cislo)
          call posun(cislo,0)
          read(Cislo,FormI15,err=2450) ngrid(i)
        enddo
        PosledniPozice=k
        go to 2460
2450    ngrid(i)=ngrid(1)
        if(NDimI(KPhase).eq.3) ngrid(3)=ngrid(1)
2460    go to 1050
      else if(izpet.eq.nCmdrestric) then
        if(NAtCalc.le.0) go to 1100
        if(nvai.ge.nvaiMax) call ReallocRestricCommands(2*(nvai+1))
        nvai=nvai+1
        call kus(NactiVeta,PosledniPozice,atvai(1,nvai))
        call Kus(NactiVeta,PosledniPozice,Cislo)
        if(EqIgCase(Cislo,'profile')) then
          RestType(nvai)=Cislo
          sumai(nvai)=1.
          nails(nvai)=0
          i=1
          if(PosledniPozice.ge.len(NactiVeta)) then
            go to 2550
          else
            go to 2500
          endif
        else
          RestType(nvai)=' '
        endif
        call uprat(atvai(1,nvai))
        k=0
        call StToInt(Cislo,k,naixb(nvai),1,.false.,ich)
        if(ich.ne.0) go to 2560
        sumai(nvai)=1.
        nails(nvai)=0
        if(PosledniPozice.ge.len(NactiVeta)) then
          i=1
          go to 2550
        endif
        k=PosledniPozice
        call kus(NactiVeta,PosledniPozice,Cislo)
        j=index(Cislo,'ls#')
        if(j.le.0) then
          PosledniPozice=k
        else
          Cislo=Cislo(4:)
          call Posun(Cislo,0)
          read(Cislo,FormI15,err=2560) nails(nvai)
        endif
        i=1
        if(PosledniPozice.ge.len(NactiVeta)) go to 2550
2500    i=i+1
        call kus(NactiVeta,PosledniPozice,atvai(i,nvai))
        call uprat(atvai(i,nvai))
        if(PosledniPozice.lt.len(NactiVeta)) go to 2500
2550    mai(nvai)=i
        call SetIntArrayTo(nai(1,nvai),mai(nvai),0)
        go to 1100
2560    nvai=nvai-1
        go to 8500
      else if(izpet.eq.nCmddontuse.or.izpet.eq.nCmduseonly) then
        if(nskrt.ge.nskrtMax) call ReallocExclHKLCommands(nskrt+1)
        nskrt=nskrt+1
        call kus(NactiVeta,PosledniPozice,skupina(nskrt))
        if(PosledniPozice.eq.len(NactiVeta)) go to 2690
        call EqStringToIntMatrix(Skupina(nskrt),hklmnp(:maxNDim),
     1                           maxNDim,ihm(1,nskrt),ihma(1,nskrt),ich)
        if(ich.ne.0) go to 2690
        call kus(NactiVeta,PosledniPozice,nevzit(nskrt))
        if(nevzit(nskrt).eq.':') then
          if(nevzit(nskrt).eq.':')
     1    call kus(NactiVeta,PosledniPozice,nevzit(nskrt))
        endif
        call EqStringToModEq(nevzit(nskrt),hklmnp(:maxNDim),maxNDim,
     1                       ihsn(1,nskrt),ieqn(nskrt),imdn(nskrt),ich)
        if(ich.ne.0) go to 2690
        if(PosledniPozice.eq.len(NactiVeta)) then
          vzit(nskrt)=' '
          ihsv(1:6,nskrt)=0
          imdv(nskrt)=0
          ieqv(nskrt)=0
        else
          call kus(NactiVeta,PosledniPozice,Cislo)
          if(EqIgCase(Cislo,'except')) then
            if(PosledniPozice.eq.len(NactiVeta)) go to 2690
            call kus(NactiVeta,PosledniPozice,vzit(nskrt))
            call EqStringToModEq(vzit(nskrt),hklmnp(:maxNDim),maxNDim,
     1                           ihsv(1,nskrt),ieqv(nskrt),imdv(nskrt),
     2                           ich)
            if(ich.ne.0) go to 2690
          else
            go to 2690
          endif
        endif
        DontUse(nskrt)=izpet.eq.nCmddontuse
        go to 1100
2690    nskrt=nskrt-1
        go to 8500
      else if(izpet.eq.nCmdequation) then
        if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
        call kus(NactiVeta,PosledniPozice,cislo)
        n=0
        atv(1)=' '
        if(cislo.ne.'for') then
          if(cislo.ne.':'.and.cislo.ne.'&'.and.cislo.ne.'%') go to 8500
        else
2700      call kus(NactiVeta,PosledniPozice,Cislo)
          if(PosledniPozice.ge.len(NactiVeta)) then
            t80='delimiter ":" or "%" not found'
            go to 2790
          endif
          if(Cislo.eq.':'.or.Cislo.eq.'&'.or.Cislo.eq.'%') then
            go to 2710
          else
            if(Cislo.eq.'atoms'.or.Cislo.eq.'atom'.or.
     1         Cislo.eq.'molecules'.or.Cislo.eq.'molecule') then
              if(n.eq.0) then
                go to 2700
              else
                t80=' '
                go to 2790
              endif
            endif
          endif
          if(n.ge.10) then
            t80='number of atoms in equation larger than 10'
            go to 2790
          endif
          n=n+1
          atv(n)=Cislo
          call uprat(atv(n))
          go to 2700
        endif
2710    eqhar(neq+1)=NactiVeta(PosledniPozice-1:PosledniPozice-1).eq.'&'
     1           .or.NactiVeta(PosledniPozice-1:PosledniPozice-1).eq.'%'
        if(PosledniPozice.eq.len(NactiVeta)) then
          read(m50,FormA80) NactiVeta
          PosledniPozice=0
        else
          NactiVeta(:PosledniPozice)=' '
        endif
        neqp=neq+1
        call vazba(NactiVeta)
        if(neqp.gt.neq) go to 8500
        if(atv(1).ne.' ') then
          neq=neq-1
          do i=1,n
2725        j=ktatmv(atv(i),NAtInd,0)
            if(j.gt.0) then
              at=atom(j)
            else if(j.lt.0) then
              j=-j
              k=j/mxp+1
              l=mod(j,mxp)
              write(at,'(a8,''#'',i2)') molname(k),l
              call zhusti(at)
            else
              cycle
            endif
            neq=neq+1
            lat(neq)=lat(neqp)
            lpa(neq)=lpa(neqp)
            lnp(neq)=lnp(neqp)
            lnpo(neq)=0
            pab(neq)=pab(neqp)
            npa(neq)=npa(neqp)
            lat(neq)=at
            do j=1,npa(neq)
              pat(j,neq)=pat(j,neqp)
              ppa(j,neq)=ppa(j,neqp)
              pnp(j,neq)=pnp(j,neqp)
              pko(j,neq)=pko(j,neqp)
              pat(j,neq)=at
            enddo
            go to 2725
          enddo
        endif
        go to 1100
2790    call FeChybne(-1.,-1.,'Syntax error in equation : '//NactiVeta,
     1                t80,WarningWithESC)
        if(ErrFlag.ne.0) then
          go to 9999
        else
          go to 1100
        endif
      else if(izpet.eq.nCmdfixed) then
        call kus(NactiVeta,PosledniPozice,t80)
        i=islovo(t80,cfix,10)
        if(i.gt.3) i=i-1
        if(PosledniPozice.ge.len(NactiVeta).and.i.gt.0) go to 8500
        nfixt=0
        if(i.le.0) then
          go to 2850
        else if(i.eq.IdFixIndividual.or.i.eq.IdFixSetTo) then
          nfixt=i
          if(i.eq.IdFixSetTo) then
            call kus(NactiVeta,PosledniPozice,Cislo)
            call posun(Cislo,1)
            read(Cislo,102,err=8500) ValFixPom
          endif
          call kus(NactiVeta,PosledniPozice,t80)
          go to 2850
        else if(i.eq.IdFixOrigin) then
          NFixOrigin=NFixOrigin+1
          if(NFixOrigin.gt.NPhase) go to 1100
          call kus(NactiVeta,PosledniPozice,AtFixOrigin(NFixOrigin))
          call uprat(AtFixOrigin(NFixOrigin))
          KFixOrigin(NFixOrigin)=1
          go to 1100
        endif
        if(i.eq.IdFixOriginX4) then
          NFixX4=NFixX4+1
          if(NFixX4.gt.NPhase) go to 1100
          call kus(NactiVeta,PosledniPozice,t80)
          j=index(t80,'[')
          if(j.gt.0) then
            PromAtFixX4(NFixX4)=t80(:j-1)
            k=index(t80,']')
            if(k.le.0) k=idel(t80)+1
            AtFixX4(NFixX4)=t80(j+1:k-1)
          else
            AtFixX4(NFixX4)=t80
            call kus(NactiVeta,PosledniPozice,PromAtFixX4(NFixX4))
          endif
          call uprat(AtFixX4(NFixX4))
          KFixX4(NFixX4)=ktera(PromAtFixX4(NFixX4))
          go to 1100
        endif
2800    if(lnfix.eq.0) then
          lnfix=NextLogicNumber()
          call OpenFile(lnfix,fln(:ifln)//'_fixed.tmp','unformatted',
     1                  'unknown')
          if(ErrFlag.ne.0) go to 9999
        endif
        kfix=i
        call kus(NactiVeta,PosledniPozice,atfix)
        call uprat(atfix)
        pnfix=' '
        ValFix=0.
        write(lnfix) AtFix,PnFix,KFix,ValFix
        if(PosledniPozice.ge.len(NactiVeta)) go to 1100
        go to 2800
2850    if(lnfix.eq.0) then
          lnfix=NextLogicNumber()
          call OpenFile(lnfix,fln(:ifln)//'_fixed.tmp','unformatted',
     1                  'unknown')
          if(ErrFlag.ne.0) go to 9999
        endif
        ValFix=0.
        i=index(t80,'[')
        if(i.gt.0) then
          j=index(t80,']')
          if(j.lt.0.or.i.ge.j-1.or.j.ne.idel(t80)) go to 2870
          pnfix=t80(:i-1)
          kfix=-1
          atfix=t80(i+1:j-1)
          call uprat(atfix)
        else
          kfix=-1
          atfix=' '
          pnfix=t80
        endif
        if(KFix.ne.0) then
          if(nfixt.eq.IdFixSetTo) then
            KFix=-2
            ValFix=ValFixPom
          endif
          write(lnfix) AtFix,PnFix,KFix,ValFix
          go to 2880
        endif
2870    call FeChybne(-1.,-1.,'syntax error in the "fixed" keyword',
     1                'wrong parameter name : '//t80(:idel(t80)),
     2                SeriousError)
2880    if(PosledniPozice.ge.len(NactiVeta)) go to 1100
        call kus(NactiVeta,PosledniPozice,t80)
        go to 2850
      else if(izpet.eq.nCmdscale) then
        call kus(NactiVeta,PosledniPozice,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8500) i
        if(i.lt.2.or.i.gt.mxsc) go to 8500
        if(NScales.ge.NScalesMax) call ReallocScaleCommands(NScales+1)
        NScales=NScales+1
        iqs(NScales)=i
        call kus(NactiVeta,PosledniPozice,cislo)
        if(cislo.ne.'for'.or.PosledniPozice.eq.len(NactiVeta))
     1    go to 2990
        call kus(NactiVeta,PosledniPozice,scsk(NScales))
        if(PosledniPozice.eq.len(NactiVeta)) go to 2990
        call EqStringToIntMatrix(scsk(NScales),hklmnp(:maxNDim),
     1                           maxNDim,ihms(1,NScales),
     2                           ihmas(1,NScales),ich)
        if(ich.ne.0) go to 2990
        call kus(NactiVeta,PosledniPozice,scvzit(NScales))
        if(scvzit(NScales).eq.':') call kus(NactiVeta,PosledniPozice,
     1                                  scvzit(NScales))
        call EqStringToModEq(scvzit(NScales),hklmnp(:maxNDim),maxNDim,
     1                       ihssn(1,NScales),ieqsn(NScales),
     2                       imdsn(NScales),ich)
        if(PosledniPozice.eq.len(NactiVeta)) then
          scnevzit(NScales)=' '
          ihssv(1:6,NScales)=0
          imdsv(NScales)=0
          ieqsv(NScales)=0
        else
          call kus(NactiVeta,PosledniPozice,Cislo)
          if(EqIgCase(Cislo,'except')) then
            if(PosledniPozice.eq.len(NactiVeta)) go to 2990
            call kus(NactiVeta,PosledniPozice,scnevzit(NScales))
            call EqStringToModEq(scnevzit(NScales),hklmnp(:maxNDim),
     1                           maxNDim,ihssv(1,NScales),
     2                           ieqsv(NScales),imdsv(NScales),ich)
        if(ich.ne.0) go to 2990
            go to 1100
          else
            go to 2990
          endif
        endif
        go to 1100
2990    NScales=NScales-1
        go to 8500
      else if(izpet.eq.nCmdRFactors) then
        if(NRFactors.ge.NRFactorsMax)
     1    call ReallocRFactorsCommands(NRFactors+1)
        NRFactors=NRFactors+1
        call kus(NactiVeta,PosledniPozice,StRFact(NRFactors))
        if(PosledniPozice.eq.len(NactiVeta)) go to 3090
        call EqStringToIntMatrix(StRFact(NRFactors),hklmnp(:maxNDim),
     1                           maxNDim,RFactMatrix(1,NRFactors),
     2                           RFactVector(1,NRFactors),ich)
        if(ich.ne.0) go to 3090
        call kus(NactiVeta,PosledniPozice,StRFactIncl(NRFactors))
        if(StRFactIncl(NRFactors).eq.':') then
          call kus(NactiVeta,PosledniPozice,StRFactIncl(NRFactors))
        endif
        call EqStringToModEq(StRFactIncl(NRFactors),hklmnp(:maxNDim),
     1                       maxNDim,CondRFactIncl(1,NRFactors),
     2                       AbsRFactIncl(NRFactors),
     3                       ModRFactIncl(NRFactors),ich)
        if(ich.ne.0) go to 3090
        if(PosledniPozice.eq.len(NactiVeta)) then
          StRFactExcl(NRFactors)=' '
          CondRFactExcl(1:6,NRFactors)=0
          AbsRFactExcl(NRFactors)=0
          ModRFactExcl(NRFactors)=0
        else
          call kus(NactiVeta,PosledniPozice,Cislo)
          if(EqIgCase(Cislo,'except')) then
            if(PosledniPozice.eq.len(NactiVeta)) go to 3090
            call kus(NactiVeta,PosledniPozice,StRFactExcl(NRFactors))
            call EqStringToModEq(StRFactExcl(NRFactors),
     1                           hklmnp(:maxNDim),maxNDim,
     2                           CondRFactExcl(1,NRFactors),
     3                           AbsRFactExcl(NRFactors),
     4                           ModRFactExcl(NRFactors),ich)
            if(ich.ne.0) go to 3090
          endif
        endif
        go to 1100
3090    NRFactors=NRFactors-1
        go to 8500
      else if(izpet.eq.nCmdDistfix.or.izpet.eq.nCmdanglefix.or.
     1        izpet.eq.nCmdTorsfix) then
        mxkp=max(NKeepAtMax,8)
        if(allocated(IAtom))
     1    deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     2               AtName,xp1,IAtom1,RmAt1,Rm6At1,KiAtX1,KiAtXMod1,
     3               RtwiTr1,RmiiAt1,AtName1)
        allocate(xp(6,mxkp),IAtom(mxkp),RmAt(9,mxkp),Rm6At(36,mxkp),
     1           KiAtX(mxkp),KiAtXMod(mxkp),RtwiTr(mxkp),RmiiAt(9,mxkp),
     2           AtName(mxkp),xp1(6,mxkp),IAtom1(mxkp),RmAt1(9,mxkp),
     3           Rm6At1(36,mxkp),KiAtX1(mxkp),KiAtXMod1(mxkp),
     3           RtwiTr1(mxkp),RmiiAt1(9,mxkp),AtName1(mxkp))
        call SetRealArrayTo(RmiiAt,9*mxkp,0.)
        lnpom=0
        if(izpet.eq.nCmdDistfix) then
          Prvni=PrvniDistFix
          n=2
          lnpom=lnfd
        else if(izpet.eq.nCmdAnglefix) then
          Prvni=PrvniAngFix
          n=3
          lnpom=lnfa
        else
          Prvni=PrvniTorsFix
          n=4
          lnpom=lnft
        endif
        if(lnda.eq.0) then
          lnda=NextLogicNumber()
          call OpenFile(lnda,fln(:ifln)//'.l95','unformatted','unknown')
        endif
        j=PosledniPozice
        do i=1,2
          call kus(NactiVeta,PosledniPozice,Cislo)
          if(i.eq.1) then
            if(Cislo.eq.'*') then
              if(NDimI(KPhase).gt.0) then
                dfix=-999.
                cycle
              else
                go to 8500
              endif
            else if(Cislo.eq.'=') then
              if(izpet.eq.nCmdDistfix) then
                n=-4
                lnpom=lned
              else if(izpet.eq.nCmdAnglefix) then
                n=-6
                lnpom=lnea
              else if(izpet.eq.nCmdTorsfix) then
                n=-8
                lnpom=lnet
              endif
              dfix=-999.
              cycle
            endif
          endif
          call posun(cislo,1)
          read(cislo,102,err=8500) p
          if(i.eq.1) then
            dfix=p
          else
            sfix=p
          endif
        enddo
        if(n.gt.0) then
          na=n
        else
          na=-n/2
        endif
!        if(n.lt.0.and.NDimI(KPhase).gt.0) then
!          call FeChybne(-1.,-1.,'distfix, anglefix and torsfix, '//
!     1                  'commnands keeping identical values, ',
!     2                  'not yet generized for modulated structures',
!     3                  Warning)
!          go to 1100
!        endif
        nn=0
3120    nn=nn+1
        do i=1,na
          if(i.eq.1) ich=0
          if(PosledniPozice.ge.len(NactiVeta)) go to 8500
          call kus(NactiVeta,PosledniPozice,t80)
          if(i.eq.na) then
            j=idel(t80)
            if(t80(j:j).eq.';') t80(j:j)=' '
          endif
          if(ich.ne.0) cycle
          AtName(i)=t80
          call UprAt(AtName(i))
          call AtomSymCode(AtName(i),ia,ISym,ICentr,xp(1,i),ic,ich)
          if(ich.eq.1.and.NMolec.gt.0) then
            ia=ktatmol(AtName(i))
            if(ia.ge.NAtMolFr(1,1)) then
              ich=0
              ISym=1
              call SetRealArrayTo(xp(1,i),NDim(KPhase),0.)
            endif
          endif
          IAtom(i)=ia
          if(ich.eq.0) then
            call RefSetRestrainPar(ia,ISym,isw,ksw,KiAtX(i),KiAtXMod(i),
     1                             RmAt(1,i),Rm6At(1,i),RmiiAt(1,i))
            if(i.eq.1) then
              i1=1
              isw1=isw
              ksw1=ksw
              if(ia.ge.NAtMolFr(1,1)) then
                kmol1=kmol(ia)
              else
                kmol1=0
              endif
            else
              if(ia.ge.NAtMolFr(1,1)) then
                kmol2=kmol(IAtom(i))
              else
                kmol2=0
              endif
              if(isw.ne.isw1) then
                go to 8040
              else if(kmol2.ne.kmol1) then
                go to 8041
              else if(kswa(ia).ne.ksw1) then
                go to 8042
              endif
            endif
          endif
          if(ich.eq.0) cycle
          go to 8000
        enddo
        if(ich.eq.0) then
          if(n.gt.0.or.nn.gt.1) then
            NRestrain=NRestrain+1
            if(n.gt.0) then
              if(n.eq.2) then
                NFixDist=NFixDist+1
              else if(n.eq.3) then
                NFixAngle=NFixAngle+1
              else if(n.eq.4) then
                NFixTorsion=NFixTorsion+1
              endif
              write(lnda) IdFixDistAngTors,n,dfix,sfix,
     1          (AtName(i),i=1,n),
     2          (IAtom(i),(RmAt(j,i),j=1,9),
     3          (Rm6At(j,i),j=1,NDimQ(KPhase)),
     4          (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     5           KiAtX(i),KiAtXMod(i),i=1,n)
            else
              write(lnda) IdFixDistAngTors,n,dfix,sfix,
     1          (AtName(i),i=1,na),(AtName1(i),i=1,na),
     2          (IAtom(i),(RmAt(j,i),j=1,9),
     3          (Rm6At(j,i),j=1,NDimQ(KPhase)),
     4          (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     5           KiAtX(i),KiAtXMod(i),i=1,na),
     6          (IAtom1(i),(RmAt(j,i),j=1,9),
     7          (Rm6At1(j,i),j=1,NDimQ(KPhase)),
     8          (xp1(j,i),j=1,NDim(KPhase)),(RmiiAt1(j,i),j=1,9),
     9           KiAtX1(i),KiAtXMod1(i),i=1,na)
            endif
          else
            if(n.eq.-4) then
              NEqDist=NEqDist+1
            else if(n.eq.-6) then
              NEqAngle=NEqAngle+1
            else if(n.eq.-8) then
              NEqTorsion=NEqTorsion+1
            endif
            call CopyStringArray(AtName,AtName1,na)
            call CopyVekI(IAtom,IAtom1,na)
            call CopyVekI(KiAtX,KiAtX1,na)
            call CopyVekI(KiAtXMod,KiAtXMod1,na)
            call CopyVek(RmAt,RmAt1,9*na)
            call CopyVek(RmiiAt,RmiiAt1,9*na)
            do i=1,na
              do j=1,NDimQ(KPhase)
                Rm6At1(j,i)=Rm6At(j,i)
              enddo
              do j=1,NDim(KPhase)
                xp1(j,i)=xp(j,i)
              enddo
            enddo
          endif
          call AtomSymCode(AtName(1),ia,ISym,ICenter,xpp,ic,ich)
          call ScodeCIF(isym,icenter,ic,iswa(ia),Cislo)
          Veta=Atom(ia)(:idel(Atom(ia))+1)//Cislo
          do i=2,na
            call AtomSymCode(AtName(i),ia,ISym,ICenter,xpp,ic,ich)
            call ScodeCIF(ISym,ICenter,ic,iswa(ia),Cislo)
            j=IAtom(i)
            Veta=Veta(:idel(Veta)+1)//Atom(ia)(:idel(Atom(ia))+1)//Cislo
          enddo
          if(n.gt.0) then
            write(Cislo,102) dfix
          else
            if(n.eq.-4) then
              write(Cislo,101) NEqDist
            else if(n.eq.-6) then
              write(Cislo,101) NEqAngle
            else if(n.eq.-8) then
              write(Cislo,101) NEqTorsion
            endif
          endif
          call ZdrcniCisla(Cislo,1)
          Veta=Veta(:idel(Veta)+1)//Cislo
          write(Cislo,102) sfix
          call ZdrcniCisla(Cislo,1)
          Veta=Veta(:idel(Veta)+1)//Cislo
          write(lnpom,FormA) Veta(:idel(Veta))
          if(PosledniPozice.lt.len(NactiVeta)) go to 3120
        endif
        deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     1             AtName,xp1,IAtom1,RmAt1,Rm6At1,KiAtX1,KiAtXMod1,
     2             RtwiTr1,RmiiAt1,AtName1)
      else if(izpet.eq.nCmdKeep) then
        n=NKeep+1
        if(n.gt.NKeepMax)
     1    call ReallocKeepCommands(2*n,max(NKeepAtMax,8))
        call EM40SetDefaultKeepH(n)
        mxkp=NKeepAtMax
        if(allocated(IAtom))
     1    deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     2               AtName)
        allocate(xp(6,mxkp),IAtom(mxkp),RmAt(9,mxkp),Rm6At(36,mxkp),
     1           KiAtX(mxkp),KiAtXMod(mxkp),RtwiTr(mxkp),RmiiAt(9,mxkp),
     2           AtName(mxkp))
        call SetRealArrayTo(RmiiAt,9*mxkp,0.)
        if(PosledniPozice.ge.len(NactiVeta)) then
          t80=NactiVeta
          go to 8010
        endif
        call kus(NactiVeta,PosledniPozice,t80)
        call mala(t80)
        i=islovo(t80,CKeepType,3)
        if(i.le.0) then
          ich=1
          t80='Incorrect type "'//t80(:idel(t80))//'"'
          go to 8100
        else
          call kus(NactiVeta,PosledniPozice,t80)
          call mala(t80)
        endif
        KeepTypeMain=i
        if(i.eq.IdKeepHydro) then
          i=islovo(t80,CKeepHydro,3)
        else if(i.eq.IdKeepGeom) then
          i=islovo(t80,CKeepGeom,3)
        else if(i.eq.IdKeepADP) then
          i=islovo(t80,CKeepADP,2)
        endif
        if(i.le.0) then
          ich=1
          t80='Incorrect type "'//t80(:idel(t80))//'"'
          go to 8100
        endif
        KeepType(n)=KeepTypeMain*10+i
        if(KeepTypeMain.eq.IdKeepHydro) then
          if(KeepType(n).eq.IdKeepHApical) then
            NAtMax=6
          else if(KeepType(n).eq.IdKeepHTetraHed) then
            NAtMax=4
          else if(KeepType(n).eq.IdKeepHTriangl) then
            NAtMax=3
          endif
          if(PosledniPozice.ge.len(NactiVeta)) go to 8010
          call kus(NactiVeta,PosledniPozice,t80)
          call UprAt(t80)
          ia=ktatmol(t80)
          if(ia.le.0) then
            ich=1
            go to 8000
          endif
          KeepNAtCentr(n)=ia
          KeepAtCentr(n)=t80
          IAtom(2)=ia
          AtName(2)=t80
          ISym=1
          call SetRealArrayTo(xp(1,2),NDim(KPhase),0.)
          if(ia.ge.NAtMolFr(1,1)) then
            kmol1=kmol(ia)
            call CrlRestoreSymmetry(ISymmMolec((kmol1-1)/mxp+1))
          else
            kmol1=0
            if(NMolec.gt.0) call CrlRestoreSymmetry(ISymmBasic)
          endif
          call RefSetRestrainPar(ia,ISym,isw1,ksw1,KiAtX(2),KiAtXMod(2),
     1                           RmAt(1,2),Rm6At(1,2),RmiiAt(1,2))
          i1=2
          if(PosledniPozice.ge.len(NactiVeta)) go to 8010
          call kus(NactiVeta,PosledniPozice,t80)
          k=0
          call StToInt(t80,k,ic,1,.false.,ich)
          if(ich.ne.0) go to 8020
          KeepNNeigh(n)=ic(1)
          if(KeepNNeigh(n).gt.NAtMax) then
            write(Cislo,FormI15) KeepNNeigh(n)
            call Zhusti(Cislo)
            t80='The number of '//Cislo(:idel(Cislo))//
     1          ' neighbors atoms exceeds the limit'
            go to 8100
          else if(KeepType(n).eq.IdKeepHApical.and.KeepNNeigh(n).lt.1)
     1      then
            ich=1
            t80='The number of neighbor atoms is too small'
            go to 8100
          endif
          if(KeepType(n).ne.IdKeepHApical) then
            call kus(NactiVeta,PosledniPozice,t80)
            k=0
            call StToInt(t80,k,ic,1,.false.,ich)
            if(ich.ne.0) go to 8020
            KeepNH(n)=ic(1)
          else
            KeepNH(n)=1
          endif
          if(KeepNNeigh(n)+KeepNH(n).gt.NAtMax+1) then
            write(Cislo,FormI15) KeepNNeigh(n)+KeepNH(n)
            call Zhusti(Cislo)
            t80='The number of '//Cislo(:idel(Cislo))//
     1          ' Neighbor+H atoms exceeds the limit'
            go to 8100
          endif
          i=2
          do j=1,KeepNNeigh(n)
            if(j+KeepNH(n).eq.NAtMax+1.and.KeepType(n).ne.IdKeepHApical)
     1        then
              if(PosledniPozice.ge.len(NactiVeta)) go to 8010
              call StToReal(NactiVeta,PosledniPozice,xpp,1,.false.,ich)
              if(ich.ne.0) go to 8030
              KeepAngleH(n)=xpp(1)
            endif
            i=i+1
            if(PosledniPozice.ge.len(NactiVeta)) go to 8010
            call kus(NactiVeta,PosledniPozice,t80)
            call UprAt(t80)
            call AtomSymCode(t80,ia,ISym,ICentr,xp(1,i),ic,ich)
            if(ich.eq.1.and.NMolec.gt.0) then
              ia=ktatmol(t80)
              if(ia.ge.NAtMolFr(1,1)) then
                ich=0
                ISym=1
                call SetRealArrayTo(xp(1,i),NDim(KPhase),0.)
              endif
            endif
            if(ich.ne.0) go to 8000
            AtName(i)=t80
            IAtom(i)=ia
            call RefSetRestrainPar(ia,ISym,isw,ksw,KiAtX(i),KiAtXMod(i),
     1                             RmAt(1,i),Rm6At(1,i),RmiiAt(1,i))
            if(j+KeepNH(n).lt.NAtMax+1) then
              KeepNAtNeigh(n,j)=ia
              KeepAtNeigh(n,j)=AtName(i)
            else
              KeepNAtAnchor(n)=ia
              KeepAtAnchor(n)=AtName(i)
            endif
            if(ia.ge.NAtMolFr(1,1)) then
              kmol2=kmol(ia)
            else
              kmol2=0
            endif
            if(isw.ne.isw1) then
              go to 8040
            else if(kmol2.ne.kmol1) then
              go to 8041
            else if(kswa(ia).ne.ksw1) then
              go to 8042
            endif
          enddo
          if(PosledniPozice.ge.len(NactiVeta)) go to 8010
          call StToReal(NactiVeta,PosledniPozice,xpp,1,.false.,ich)
          if(ich.ne.0) go to 8030
          KeepDistH(n)=xpp(1)
          do k=1,KeepNH(n)
            if(PosledniPozice.ge.len(NactiVeta)) go to 8010
            call kus(NactiVeta,PosledniPozice,t80)
            call UprAt(t80)
            ia=ktatmol(t80)
            if(ia.le.0) then
              ich=1
              go to 8000
            endif
            KeepNAtH(n,k)=ia
            KeepAtH(n,k)=t80
            i=1
            AtName(1)=t80
            if(ia.ge.NAtMolFr(1,1)) then
              kmol2=kmol(ia)
            else
              kmol2=0
            endif
            if(iswa(ia).ne.isw1) then
              go to 8040
            else if(kmol2.ne.kmol1) then
              go to 8041
            else if(kswa(ia).ne.ksw1) then
              go to 8042
            endif
          enddo
          j=2
          if(KeepType(n).eq.IdKeepHTetraHed) then
            DTors=120.
          else
            DTors=180.
          endif
          do 4500k=1,KeepNH(n)
            IAtom(1)=KeepNAtH(n,k)
            AtName(1)=KeepAtH(n,k)
            ISym=1
            call SetRealArrayTo(xp(1,1),NDim(KPhase),0.)
            call RefSetRestrainPar(IAtom(1),ISym,isw,ksw,KiAtX(1),
     1                             KiAtXMod(1),RmAt(1,1),Rm6At(1,1),
     2                             RmiiAt(1,1))
            if(lnda.eq.0) then
              lnda=NextLogicNumber()
              call OpenFile(lnda,fln(:ifln)//'.l95','unformatted',
     1                      'unknown')
            endif
            if(KeepType(n).eq.IdKeepHApical.or.
     1         ((KeepType(n).eq.IdKeepHTetraHed.or.
     2           KeepType(n).eq.IdKeepHTriangl).and.KeepNH(n).eq.1))
     3        then
              dfix=KeepDistH(n)
              sfix=sdfix
              nn=KeepNNeigh(n)+2
              write(lnda) IdFixApical,nn,dfix,sfix,(AtName(i),i=1,nn),
     1          (IAtom(i),(RmAt(j,i),j=1,9),
     2          (Rm6At(j,i),j=1,NDimQ(KPhase)),
     2          (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     3          KiAtX(i),KiAtXMod(i),i=1,nn)
            else
              dfix=KeepDistH(n)
              sfix=sdfix
              nn=2
              write(lnda) -IdFixDistAngTors,nn,dfix,sfix,
     1          (AtName(i),i=1,nn),(IAtom(i),
     2         (RmAt(j,i),j=1,9),(Rm6At(j,i),j=1,NDimQ(KPhase)),
     3         (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     4          KiAtX(i),KiAtXMod(i),i=1,nn)
              iv=1
              if(KeepType(n).eq.IdKeepHTetraHed) then
                if(NAtMax-KeepNH(n).eq.2) then
                  call MultM(RmAt(1,1),x(1,IAtom(1)),x1,3,3,1)
                  call MultM(RmAt(1,2),x(1,IAtom(2)),x2,3,3,1)
                  call MultM(RmAt(1,3),x(1,IAtom(3)),x3,3,3,1)
                  do i=1,3
                    x1(i)=x1(i)+xp(i,1)
                    x2(i)=x2(i)+xp(i,2)
                    x3(i)=x3(i)+xp(i,3)
                  enddo
                  do i=1,3
                    u(i)=x1(i)-x2(i)
                    v(i)=x3(i)-x2(i)
                  enddo
                  call multm(MetTens(1,isw,ksw),u,ug,3,3,1)
                  call multm(MetTens(1,isw,ksw),v,vg,3,3,1)
                  pom=scalmul(u,vg)/sqrt(scalmul(u,ug)*scalmul(v,vg))
                  if(abs(pom).lt..999) then
                    dfix=acos(pom)/ToRad
                  else
                    dfix=109.4712
                  endif
                else
                  dfix=109.4712
                endif
              else
                dfix=120.
              endif
              sfix=safix
              nn=3
              do l=1,NAtMax-KeepNH(n)
                m=l+2
                write(lnda) -IdFixDistAngTors,nn,dfix,sfix,
     1            (AtName(i),i=1,2),AtName(m),
     2            (IAtom(i),(RmAt(j,i),j=1,9),
     3            (Rm6At(j,i),j=1,NDimQ(KPhase)),
     4            (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     5            KiAtX(i),KiAtXMod(i),i=1,2),
     6            IAtom(m),(RmAt(j,m),j=1,9),
     7            (Rm6At(j,m),j=1,NDimQ(KPhase)),
     8            (xp(j,m),j=1,NDim(KPhase)),(RmiiAt(j,m),j=1,9),
     9            KiAtX(m),KiAtXMod(m)
                iv=iv+1
                if(iv.ge.3) go to 4500
              enddo
              if(KeepNNeigh(n)+KeepNH(n).gt.NAtMax) then
                dfix=KeepAngleH(n)+float(k-1)*DTors
4225            if(dfix.gt.180.) then
                  dfix=dfix-360.
                  go to 4225
                endif
4226            if(dfix.le.-180.) then
                  dfix=dfix+360.
                  go to 4226
                endif
                sfix=safix
                nn=4
                write(lnda) -IdFixDistAngTors,nn,dfix,sfix,
     1            (AtName(i),i=1,nn),
     2            (IAtom(i),(RmAt(j,i),j=1,9),
     3            (Rm6At(j,i),j=1,NDimQ(KPhase)),
     4            (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     5            KiAtX(i),KiAtXMod(i),i=1,nn)
              else
                if(k.gt.1) then
                  nn=3
                  if(KeepType(n).eq.IdKeepHTetraHed) then
                    dfix=109.4712
                  else
                    dfix=120.
                  endif
                  sfix=safix
                  t80=atom(nho)
                  write(lnda) -IdFixDistAngTors,nn,dfix,sfix,
     1             (AtName(i),i=1,2),t80,
     2             (IAtom(i),(RmAt(j,i),j=1,9),
     3             (Rm6At(j,i),j=1,NDimQ(KPhase)),
     3             (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     4              KiAtX(i),KiAtXMod(i),i=1,2),
     5              nho,(RmAt(j,1),j=1,9),
     6              (Rm6At(j,1),j=1,NDimQ(KPhase)),
     7              (xp(j,1),j=1,NDim(KPhase)),(RmiiAt(j,1),j=1,9),
     8              KiAtXo,KiAtXModo
                endif
                nho=IAtom(1)
                KiAtXo=KiAtX(i)
                KiAtXModo=KiAtXMod(i)
              endif
            endif
4500      continue
        else if(KeepTypeMain.eq.IdKeepGeom) then
          KeepNAtCentr(n)=0
          dfix=0.
          sfix=0.001
          do ii=1,2
            if(ii.eq.1) then
              ipp=PosledniPozice
            else
              PosledniPozice=ipp
              if(nn.gt.NKeepAtMax) then
                NKeep=n
                call ReallocKeepCommands(NKeepMax,2*nn)
                mxkp=2*nn
                if(allocated(IAtom))
     1            deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,
     2                       RmiiAt,AtName)
                allocate(xp(6,mxkp),IAtom(mxkp),RmAt(9,mxkp),
     1                   Rm6At(36,mxkp),KiAtX(mxkp),KiAtXMod(mxkp),
     2                   RtwiTr(mxkp),RmiiAt(9,mxkp),AtName(mxkp))
              endif
              call SetRealArrayTo(RmiiAt,9*mxkp,0.)
            endif
            nn=0
5000        if(PosledniPozice.ge.len(NactiVeta)) go to 5100
            call kus(NactiVeta,PosledniPozice,t80)
5050        iap=ktatmv(t80,NAtInd,0)
            if(iap.gt.0) then
              nn=nn+1
            else if(iap.lt.0) then
              go to 5050
            else
              go to 5000
            endif
            if(ii.eq.1) go to 5050
            At=atom(iap)
            KeepAt(n,nn)=At
            call AtomSymCode(At,ia,ISym,ICentr,xp(1,nn),ic,ich)
            KeepNAt(n,nn)=ia
            if(ich.eq.1.and.NMolec.gt.0) then
              ia=ktatmol(t80)
              if(ia.ge.NAtMolFr(1,1)) then
                ich=0
                ISym=1
                call SetRealArrayTo(xp(1,nn),NDim(KPhase),0.)
              endif
            endif
            if(ich.ne.0) go to 8000
            AtName(nn)=At
            IAtom(nn)=ia
            call RefSetRestrainPar(ia,ISym,isw,ksw,KiAtX(nn),
     1                             KiAtXMod(nn),RmAt(1,nn),Rm6At(1,nn),
     2                             RmiiAt(1,nn))
            KeepKiX(n,nn)=KiAtX(nn)
            if(nn.eq.1) then
              isw1=isw
              ksw1=ksw
              i1=1
              if(ia.ge.NAtMolFr(1,1)) then
                kmol1=kmol(ia)
              else
                kmol1=0
              endif
            endif
            i=nn
            if(ia.ge.NAtMolFr(1,1)) then
              kmol2=kmol(ia)
            else
              kmol2=0
            endif
            if(isw.ne.isw1) then
              go to 8040
            else if(kmol2.ne.kmol1) then
              go to 8041
            else if(kswa(ia).ne.ksw1) then
              go to 8042
            endif
            go to 5050
5100        if(ii.eq.1) cycle
            if(lnda.eq.0) then
              lnda=NextLogicNumber()
              call OpenFile(lnda,fln(:ifln)//'.l95','unformatted',
     1                      'unknown')
            endif
            KeepN(n)=nn
            if(KeepType(n).eq.IdKeepGPlane) then
              write(lnda) IdFixPlane,nn,dfix,sfix,(AtName(i),i=1,nn),
     1          (IAtom(i),(RmAt(j,i),j=1,9),
     2          (Rm6At(j,i),j=1,NDimQ(KPhase)),
     2          (xp(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),
     3          KiAtX(i),KiAtXMod(i),i=1,nn)
            else
              call RefKeepGRigidSet(KeepType(n),AtName,IAtom,RmAt,Rm6At,
     1                              xp,RMiiAt,KiAtX,KiAtXMod,nn)
            endif
          enddo
        else if(KeepTypeMain.eq.IdKeepADP) then
          if(PosledniPozice.ge.len(NactiVeta)) go to 8010
          call kus(NactiVeta,PosledniPozice,t80)
          call UprAt(t80)
          ia=ktatmol(t80)
          if(ia.le.0) then
            ich=1
            go to 8000
          endif
          KeepNAtCentr(n)=ia
          isw1=iswa(ia)
          ksw1=kswa(ia)
          AtName(mxkp)=t80
          i1=mxkp
          if(ia.ge.NAtMolFr(1,1)) then
            kmol1=kmol(ia)
          else
            kmol1=0
          endif
          KeepAtCentr(n)=t80
          if(PosledniPozice.ge.len(NactiVeta)) go to 8010
          call StToReal(NactiVeta,PosledniPozice,xpp,1,.false.,ich)
          if(ich.ne.0) go to 8030
          KeepADPExtFac(n)=xpp(1)
          i=0
6000      if(PosledniPozice.ge.len(NactiVeta)) go to 6100
          i=i+1
          call kus(NactiVeta,PosledniPozice,t80)
          call UprAt(t80)
          ia=ktatmol(t80)
          if(ia.le.0) then
            ich=1
            go to 8000
          endif
          KeepNAtH(n,i)=ia
          KeepAtH (n,i)=t80
          AtName(i)=t80
          IAtom(i)=ia
          if(ia.ge.NAtMolFr(1,1)) then
            kmol2=kmol(ia)
          else
            kmol2=0
          endif
          if(iswa(ia).ne.isw1) then
            go to 8040
          else if(kmol2.ne.kmol1) then
            go to 8041
          else if(kswa(ia).ne.ksw1) then
            go to 8042
          endif
          go to 6000
6100      KeepNH(n)=i
        endif
        NKeep=n
        deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     1             AtName)
      else if(izpet.eq.nCmdUseDatBl) then
        if(NactiVeta(PosledniPozice+1:).eq.'*') go to 1100
6200    call StToInt(NactiVeta,PosledniPozice,ic,1,.false.,ich)
        if(ich.ne.0) then
          t80=NactiVeta(PosledniPozice+1:)
          go to 8020
        endif
        NUseDatBlockInRefine=NUseDatBlockInRefine+1
        if(NUseDatBlockInRefine.eq.1)
     1    call SetLogicalArrayTo(UseDatBlockInRefine,NDatBlock,.false.)
        UseDatBlockInRefine(ic(1))=.true.
        if(PosledniPozice.lt.len(NactiVeta)) go to 6200
      else if(izpet.eq.nCmdDerTest) then
        i=index(NactiVeta,'#')
        if(i.gt.0) then
          k=0
          Cislo=' '
          do j=i+1,idel(NactiVeta)
            Cislo(j-i:j-i)=NactiVeta(j:j)
          enddo
          read(Cislo,FormI15,err=8500) DatBlockDerTest
        else
          DatBlockDerTest=1
        endif

        if(iabs(DataType(DatBlockDerTest)).eq.2) then
          call StToReal(NactiVeta,PosledniPozice,xpp,1,.false.,ich)
          if(ich.ne.0) go to 8500
          XPwdDerTest=xpp(1)
        else
          HDerTest=0
          call StToInt(NactiVeta,PosledniPozice,HDerTest,maxNDim,
     1                 .false.,ich)
          if(ich.ne.0) go to 8500
        endif
      else if(izpet.eq.nCmdRandomize) then
        call StToInt(NactiVeta,PosledniPozice,ic,1,
     1               .false.,ich)
        if(ich.ne.0) go to 8500
        RefRandomize=.true.
        RefRandomSeed=ic(1)
        call StToReal(NactiVeta,PosledniPozice,xpp,1,.false.,ich)
        RefRandomDiff=xpp(1)
      endif
      if(izpet.ne.0) go to 1100
8000  if(ich.eq.1) then
        t80='Atom "'//t80(:idel(t80))//'" doesn''t exist'
      else if(ich.eq.2) then
        t80='The symmetry part isn''t correct : '//t80
      endif
      go to 8100
8010  ich=1
      i=idel(NactiVeta)
      j=min(i,25)
      t80='the command "'//NactiVeta(:j)
      if(i.gt.j) t80=t80(:idel(t80))//'...'
      t80=t80(:idel(t80))//'" is too short'
      go to 8100
8020  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='are not from the same composite part'
      go to 8045
8041  t80='are not from the same molecule'
      go to 8045
8042  t80='are not from the same phase'
8045  t80='"'//AtName(i1)(:idel(AtName(i1)))//'" "'//
     1    AtName(i)(:idel(AtName(i)))//'" '//t80
      go to 8100
8100  call FeChybne(-1.,-1.,'problem in "'//
     1              IdRefine(izpet)(:idel(IdRefine(izpet)))//
     2              '" command.',t80,WarningWithESC)
      if(ErrFlag.ne.0) then
        go to 9999
      else
        go to 1100
      endif
8500  call ChybaNacteni(izpet)
      go to 1100
9000  continue
9999  call CloseIfOpened(m50)
      call CloseIfOpened(lnfix)
      if(lnda.gt.0) call CloseIfOpened(lnda)
      if(.not.ExistSingle) then
        NacetlInt(nCmdmmaxtw)=DefIntRefine(nCmdmmaxtw)
        NacetlReal(nCmdTwMax)=DefRealRefine(nCmdTwMax)
        NacetlReal(nCmdTwDiff)=DefRealRefine(nCmdTwDiff)
      endif
      if(NUseDatBlockInRefine.le.0) then
        NUseDatBlockInRefine=0
        do i=1,NDatBlock
          if(UseDatBlockInRefine(i))
     1      NUseDatBlockInRefine=NUseDatBlockInRefine+1
        enddo
      endif
      if(blkoef.gt..5) then
        Blkoef=Blkoef*.01
        NacetlReal(nCmdblkoef)=NacetlReal(nCmdblkoef)*.01
      endif
      if(allocated(IAtom))
     1    deallocate(xp,IAtom,RmAt,Rm6At,KiAtX,KiAtXMod,RtwiTr,RmiiAt,
     2               AtName)
      if(allocated(IAtom1))
     1    deallocate(xp1,IAtom1,RmAt1,Rm6At1,KiAtX1,KiAtXMod1,
     2               RtwiTr1,RmiiAt1,AtName1)
      if(NMolec.gt.0.and.ISymmBasic.ne.0)
     1  call CrlRestoreSymmetry(ISymmBasic)
      if(NEqDist.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lned,status=Veta)
      if(NEqAngle.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lnea,status=Veta)
      if(NEqTorsion.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lnet,status=Veta)
      if(NFixDist.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lnfd,status=Veta)
      if(NFixAngle.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lnfa,status=Veta)
      if(NFixTorsion.le.0) then
        Veta='delete'
      else
        Veta='keep'
      endif
      close(lnft,status=Veta)
      return
101   format(i5)
102   format(f15.5)
      end
