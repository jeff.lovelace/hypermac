      subroutine Calc(iov)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension hp(3),h(3),SpH(64),smp(3),AMagM(3),BMagM(3),AMagMR(3),
     1          BMagMR(3),AMagMP(3),BMagMP(3),DerAMagM(3),DerBMagM(3),
     2          DerAMagMR(3),DerBMagMR(3),DerAMagMP(3),DerBMagMP(3)
      character*80 ch80,t80
      logical izo,main
      real kapa1i,kapa2i,NormNuclear,NormMagnetic
      a=0.
      b=0.
      call SetRealArrayTo(AMag,3,0.)
      call SetRealArrayTo(BMag,3,0.)
      af=0.
      bf=0.
      if(itw.eq.1) then
        afst=0.
        bfst=0.
        affree=0.
        bffree=0.
        affreest=0.
        bffreest=0.
      endif
      aanom=0.
      banom=0.
      if(Nicova) then
        FCalc=0.
        FCalcMag=0.
        FCalcMagPol=0.
        fyr=0.
        call SetRealArrayTo(der,LastNp,0.)
        call SetRealArrayTo(dc, LastNp,0.)
        go to 6100
      endif
      if(kanref(KDatBlock).ne.0) then
        i=(KDatBlock-1)*MxScAll+MxSc+18
        call SetRealArrayTo(der(i+1),2*MaxNAtFormula*NDatBlock,0.)
        call SetRealArrayTo(dc (i+1),2*MaxNAtFormula*NDatBlock,0.)
        ianomr=i+2*(KPhase-1)*MaxNAtFormula
        ianomi=ianomr+MaxNAtFormula
      endif
      do i=1,NAtCalc
        m=PrvniKiAtomu(i)
        NDerAll=DelkaKiAtomu(i)
        aii=ai(i)
        isw=iswa(i)
        if(aii.eq.0..or.kswa(i).ne.KPhase.or..not.okr(isw)) then
          if(CalcDer) then
            call SetRealArrayTo(der(m),NDerAll,0.)
            call SetRealArrayTo(dc(m), NDerAll,0.)
          endif
          fyr(i)=0.
          cycle
        endif
        if(NDimI(KPhase).gt.0) then
          if(metoda.eq.0) then
            call calcm1(i,0)
          else
            call calcm2(i,i,isw,0)
          endif
        else
          nemod=.true.
        endif
        if(Radiation(KDatBlock).eq.NeutronRadiation) then
          lasmaxi=0
        else
          lasmaxi=lasmax(i)
        endif
        itfi=itf(i)
        ifri=ifr(i)
        MagParI=MagPar(i)
        NDerNemodFirst=1
        NDerNemodLast=max(TRankCumul(itfi),10)
        NCoef=NDerNemodLast-4
        if(ifri.ne.0) NDerNemodLast=NDerNemodLast+1
        if(lasmaxi.gt.0) then
          NDerChd=NDerAll-2
          NDerNemodLast=NDerNemodLast+3
          if(lasmaxi.gt.1) then
            j=(lasmaxi-1)**2
            NDerChd=NDerChd-1-j
            NDerNemodLast=NDerNemodLast+1+j
          endif
        else
          NDerChd=NDerAll+1
        endif
        NDerModFirst=NDerNemodLast+1
        if(Nemod) then
          NDerModLast=NDerNemodLast
        else
          NDerModLast=NDerAll
          if(MagParI.gt.0) NDerModLast=NDerModLast-3-6*(MagParI-1)
        endif
        NDerMagFirst=NDerModLast+1
        NDerMagLast=NDerAll
        izo=itfi.eq.1
        pysvej=CellVol(1,1)/CellVol(isw,KPhase)
        atomic=kmol(i).eq.0
        l=0
        do n=0,max(itfi,2)
          nrank=TRank(n)
          do j=1,nrank
            l=l+1
            if(n.eq.0) then
              par(l)=aii
            else if(n.eq.1) then
              par(l)=x(j,i)
            else if(n.eq.2) then
              if(izo.and.j.ge.2) then
                par(l)=0.
              else
                par(l)=beta(j,i)
              endif
            else if(n.eq.3) then
              par(l)=c3(j,i)
            else if(n.eq.4) then
              par(l)=c4(j,i)
            else if(n.eq.5) then
              par(l)=c5(j,i)
            else
              par(l)=c6(j,i)
            endif
          enddo
        enddo
        if(ifri.gt.0) then
          l=l+1
          xfri=xfr(i)
          par(l)=xfri
          idfr=l
        endif
        if(NDimI(KPhase).gt.0) then
          mm=0
          lx=l
          do n=1,itfi+1
            nrank=TRank(n-1)
            kmod=KModA(n,i)
            if(kmod.le.0) cycle
            mm=mm+1
            do k=1,kmod
              do j=1,nrank
                lx=lx+1
                ly=lx+nrank
                if(n.eq.1) then
                  if(k.eq.1) then
                    par(lx)=a0(i)
                    lx=lx+1
                    ly=lx+nrank
                  endif
                  par(lx)=ax(k,i)
                  par(ly)=ay(k,i)
                else if(n.eq.2) then
                  par(lx)=ux(j,k,i)
                  par(ly)=uy(j,k,i)
                else if(n.eq.3) then
                  par(lx)=bx(j,k,i)
                  par(ly)=by(j,k,i)
                else if(n.eq.4) then
                  par(lx)=c3x(j,k,i)
                  par(ly)=c3y(j,k,i)
                else if(n.eq.5) then
                  par(lx)=c4x(j,k,i)
                  par(ly)=c4y(j,k,i)
                else if(n.eq.6) then
                  par(lx)=c5x(j,k,i)
                  par(ly)=c5y(j,k,i)
                else
                  par(lx)=c6x(j,k,i)
                  par(ly)=c6y(j,k,i)
                endif
              enddo
              lx=ly
            enddo
          enddo
          if(mm.gt.0) then
            lx=lx+1
            par(lx)=phf(i)
          endif
        endif
        if(CalcDer) then
          n=NDerAll
        else
          n=1
        endif
        call SetRealArrayTo(ader,n,0.)
        call SetRealArrayTo(bder,n,0.)
        if(MagneticType(KPhase).ne.0) then
          call SetRealArrayTo(aderm,3*n,0.)
          call SetRealArrayTo(bderm,3*n,0.)
        endif
        ader1=0.
        bder1=0.
        ader10=0.
        bder10=0.
        aderst=0.
        bderst=0.
        aderfreest=0.
        bderfreest=0.
        scosijb=0.
        ssinijb=0.
        expij=1.
        isfi=isf(i)
        fxi=fx(isfi)
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          fyri=ffia(isfi,KPhase,KDatBlock)
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
          fyri=FFNi(isfi,KPhase)
        else if(Radiation(KDatBlock).eq.ElectronRadiation) then
          fyri=ffia(isfi,KPhase,KDatBlock)
        endif
        fxrfree=fxi
        if(lasmaxi.le.0) then
          fxr0=fxi
        else
          kapa1i=kapa1(i)
          if(kapa1i.le.0.) go to 9600
          kapa1i=1./kapa1i
          if(lasmaxi.gt.1) then
            kapa2i=kapa2(i)
            if(kapa2i.le.0.) go to 9600
            kapa2i=1./kapa2i
          endif
          call GetDFormF(ffcore(1,isfi,KPhase),121,sinthl,fxp,dfxp)
          l=NDerChd
          if(ffcore(1,isfi,KPhase).gt..00001) then
            ader(l)=fxp/ffcore(1,isfi,KPhase)
          else
            ader(l)=0.
          endif
          call GetDFormF(ffval(1,isfi,KPhase),121,sinthl*kapa1i,fxp,
     1                   dfxp)
          fxr0=ader(l)*Popc(i)+fxp*Popv(i)
          ader(l+1)=fxp
          ader(l+2)=-sinthl*Popv(i)*dfxp*kapa1i**2
        endif
        fxr=fxr0+ffra(isfi,KPhase,KDatBlock)
        fxrfree=fxrfree+ffra(isfi,KPhase,KDatBlock)
        if(kanref(KDatBlock).ne.0) kanomr=isfi+ianomr
        if(MagParI.ne.0) then
          fxm=fm(isfi)*BohrMagToScatt
        else
          fxm=0.
        endif
        js=1
        do j=1,NSymmRef(KPhase)
          if(isaRef(js,i).lt.0) go to 4950
          if(NCoef.gt.0) call CopyVek(HCoef(5,j,isw),HCoefJ(5),NCoef)
          hj(1)=rh(j,isw)
          hj(2)=rk(j,isw)
          hj(3)=rl(j,isw)
          if(NDimI(KPhase).gt.0) then
            mj=imnp(j,isw,1)
            main=mj.eq.0
            if(NDimI(KPhase).gt.1) then
              nj=imnp(j,isw,2)
              main=main.and.nj.eq.0
              if(NDimI(KPhase).gt.2) then
                pj=imnp(j,isw,3)
                main=main.and.pj.eq.0
              endif
            endif
            if(atomic.and.KCommen(KPhase).le.0) then
              athj=ath(j,isw)
              atkj=atk(j,isw)
              atlj=atl(j,isw)
            else
              athj=hj(1)
              atkj=hj(2)
              atlj=hj(3)
            endif
          else
            athj=hj(1)
            atkj=hj(2)
            atlj=hj(3)
          endif
          HCoefj(2)=athj
          HCoefj(3)=atkj
          HCoefj(4)=atlj
          PhiAng=t(j)
          do l=2,4
            PhiAng=PhiAng+HCoefj(l)*par(l)
          enddo
          PhiAng=PhiAng-anint(PhiAng/pi2)*pi2
          if(.not.izo) then
            pom=0.
            do l=5,10
              pom=pom-HCoefj(l)*par(l)
            enddo
            expij=ExpJana(pom,ich)
            if(expij.gt.1000..or.ich.ne.0) cycle
          endif
          Creal=1.
          Cimag=0.
          if(itfi.gt.2) then
            l=10
            imag=.true.
            MinusReal=.true.
            do n=3,itfi
              nrank=TRank(n)
              if(mod(n,2).eq.0) MinusReal=.not.MinusReal
              imag=.not.imag
              do k=1,nrank
                l=l+1
                pom=HCoefj(l)*par(l)
                if(MinusReal) pom=-pom
                if(imag) then
                  Creal=Creal+pom
                else
                  Cimag=Cimag+pom
                endif
              enddo
            enddo
          endif
          if(NDimI(KPhase).le.0) then
            sinijst=sin(PhiAng)
            cosijst=cos(PhiAng)
            sinij=sinijst*expij
            cosij=cosijst*expij
            if(itfi.gt.2) then
              cosijc=cosij
              sinijc=sinij
              cosij=cosijc*Creal-sinijc*Cimag
              sinij=sinijc*Creal+cosijc*Cimag
            endif
          else
            sinijst=0.
            cosijst=0.
            if(metoda.eq.0) then
              call calcm1(i,1)
              if(itfi.gt.2) then
                cosijc=cosij
                sinijc=sinij
                cosij=cosijc*Creal-sinijc*Cimag
                sinij=sinijc*Creal+cosijc*Cimag
              endif
            else
              if(KCommen(KPhase).gt.0) PhiAng=PhiAng+trezm(j,isw)
              call calcm2(i,j,isw,1)
              if(ErrFlag.ne.0) cycle
            endif
          endif
          sinija=sinij
          cosija=cosij
          sinij0=sinij
          cosij0=cosij
          sinijb=sinij
          cosijb=cosij
          if(itfi.gt.2) then
            cosijca=cosijc
            sinijca=sinijc
          endif
          sinijast=sinijst
          cosijast=cosijst
          if(lasmaxi.gt.1) then
            hp(1)=rh(j,1)
            hp(2)=rk(j,1)
            hp(3)=rl(j,1)
            kk=NderChd+3
            call multm(hp,triat(1,i),h,1,3,3)
            call SpherHarm(h,SpH,lasmaxi-2,3)
            Creal=fxr
            Cimag=0.
            Dreal=0.
            Dimag=0.
            k=0
            l=lasmaxi-1
            Slat=SlaterFB(NSlater(l,isfi,KPhase),-1,sinthl*kapa2i,
     1                    ZSlater(l,isfi,KPhase)/BohrRad,DerZ,DerS,ich)
            if(ich.ne.0) go to 9700
            do l=0,lasmaxi-2
              pomk=ZSlater(l+1,isfi,KPhase)/BohrRad
              Slat=SlaterFB(NSlater(l+1,isfi,KPhase),l,sinthl*kapa2i,
     1                      pomk,DerZ,DerS,ich)
              if(ich.ne.0) go to 9700
              pom=0
              do n=1,2*l+1
                k=k+1
                pomr=Slat*SpH(k)
                pom=pom+popas(k,i)*pomr
                SpH(k)=pomr
              enddo
              pomr=-pom*DerS*sinthl*kapa2i**2
              n=mod(l,4)
              if(n.eq.0) then
                Creal=Creal+pom
                Dreal=Dreal+pomr
              else if(n.eq.1) then
                Cimag=Cimag+pom
                Dimag=Dimag+pomr
              else if(n.eq.2) then
                Creal=Creal-pom
                Dreal=Dreal-pomr
              else
                Cimag=Cimag-pom
                Dimag=Dimag-pomr
              endif
            enddo
            cosij=cosija*Creal-sinija*Cimag
            sinij=sinija*Creal+cosija*Cimag
            CReal0=Creal-fxr+fxr0
            cosij0=cosija*Creal0-sinija*Cimag
            sinij0=sinija*Creal0+cosija*Cimag
            if(itfi.gt.2) then
              cosijc=cosijca*Creal-sinijca*Cimag
              sinijc=sinijca*Creal+cosijca*Cimag
            endif
            cosijst=cosijast*Creal-sinijast*Cimag
            sinijst=sinijast*Creal+cosijast*Cimag
            if(CalcDer) then
              ader(kk)=ader(kk)+cosija*Dreal-sinija*Dimag
              bder(kk)=bder(kk)+sinija*Dreal+cosija*Dimag
            endif
          else
            if(MagParI.gt.0.and.NDimI(KPhase).le.0) then
              cosijmp=cosij
              sinijmp=sinij
            endif
            cosij=cosij*fxr
            sinij=sinij*fxr
            cosij0=cosij*fxr0
            sinij0=sinij*fxr0
            cosijst=cosijst*fxr
            sinijst=sinijst*fxr
            if(itfi.gt.2) then
              cosijc=cosijc*fxr
              sinijc=sinijc*fxr
            endif
          endif
          ader(1)=ader(1)+cosij
          ader10=ader10+cosij0
          ssinijb=ssinijb+sinijb
          if(lasmaxi.gt.0) then
            ader1=ader1+cosija
            aderst=aderst+cosijst
            aderfreest=aderfreest+fxrfree*cosijast
          else if(MagParI.ne.0) then
            if(NDimI(KPhase).le.0) then
              call Multm(RMag(1,j,1,KPhase),sm0(1,i),smp,3,3,1)
              do l=1,3
                cosijm(l)=smp(l)*cosijmp
                sinijm(l)=smp(l)*sinijmp
                if(CalcDer) then
                  ind=l
                  do n=NDerMagFirst,NDerMagFirst+2
                    pom=RMag(ind,j,isw,KPhase)
                    if(NCSymmRef(KPhase).eq.1) then
                      aderm(l,n)=aderm(l,n)+cosijmp*pom
                      bderm(l,n)=bderm(l,n)+sinijmp*pom
                    else
                      if(ZCSymmRef(KPhase).gt.0.) then
                        aderm(l,n)=aderm(l,n)+cosijmp*pom
                      else
                        bderm(l,n)=bderm(l,n)+sinijmp*pom
                      endif
                    endif
                    ind=ind+3
                  enddo
                endif
              enddo
            endif
            do l=1,3
              cosijm(l)=cosijm(l)*fxm
              sinijm(l)=sinijm(l)*fxm
              if(NCSymmRef(KPhase).eq.1) then
                aderm(l,1)=aderm(l,1)+cosijm(l)
                bderm(l,1)=bderm(l,1)+sinijm(l)
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  aderm(l,1)=aderm(l,1)+cosijm(l)
                else
                  bderm(l,1)=bderm(l,1)+sinijm(l)
                endif
              endif
            enddo
          endif
          if(NCSymmRef(KPhase).eq.1) then
            bder(1)=bder(1)+sinij
            bder10=bder10+sinij0
            scosijb=scosijb+cosijb
            if(lasmaxi.gt.0) then
              bder1=bder1+sinija
              bderst=bderst+sinijst
              bderfreest=bderfreest+fxrfree*sinijast
            endif
          endif
          if(.not.CalcDer) go to 4950
          do l=2,4
            pom=HCoefj(l)
            ader(l)=ader(l)-pom*sinij
            if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*cosij
            if(MagParI.ne.0) then
              do n=1,3
                if(NCSymmRef(KPhase).eq.1) then
                  aderm(n,l)=aderm(n,l)-pom*sinijm(n)
                  bderm(n,l)=bderm(n,l)+pom*cosijm(n)
                else
                  if(ZCSymmRef(KPhase).gt.0.) then
                    aderm(n,l)=aderm(n,l)-pom*sinijm(n)
                  else
                    bderm(n,l)=bderm(n,l)+pom*cosijm(n)
                  endif
                endif
              enddo
            endif
          enddo
          if(.not.izo) then
            do l=5,10
              pom=-HCoefj(l)
              ader(l)=ader(l)+pom*cosij
              if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*sinij
              if(MagParI.ne.0) then
                do n=1,3
                  if(NCSymmRef(KPhase).eq.1) then
                    aderm(n,l)=aderm(n,l)+pom*cosijm(n)
                    bderm(n,l)=bderm(n,l)+pom*sinijm(n)
                  else
                    if(ZCSymmRef(KPhase).gt.0.) then
                      aderm(n,l)=aderm(n,l)+pom*cosijm(n)
                    else
                      bderm(n,l)=bderm(n,l)+pom*sinijm(n)
                    endif
                  endif
                enddo
              endif
            enddo
            l=10
            imag=.false.
            MinusReal=.true.
            MinusImag=.true.
            do n=3,itfi
              nrank=TRank(n)
              if(mod(n,2).eq.1) then
                MinusReal=.not.MinusReal
              else
                MinusImag=.not.MinusImag
              endif
              imag=.not.imag
              if(imag) then
                pomi=cosijc
                pomr=sinijc
              else
                pomi=sinijc
                pomr=cosijc
              endif
              if(MinusReal) pomr=-pomr
              if(MinusImag) pomi=-pomi
              do k=1,nrank
                l=l+1
                pom=HCoefj(l)
                ader(l)=ader(l)+pom*pomr
                if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*pomi
              enddo
            enddo
          else
            l=10
          endif
          if(lasmaxi.gt.1) then
            kk=l+4
            k=0
            do l=0,lasmaxi-2
              md=mod(l,4)
              do n=1,2*l+1
                k=k+1
                kk=kk+1
                if(md.eq.0) then
                  pomr= SpH(k)*cosija
                  pomi= SpH(k)*sinija
                else if(md.eq.1) then
                  pomr=-SpH(k)*sinija
                  pomi= SpH(k)*cosija
                else if(md.eq.2) then
                  pomr=-SpH(k)*cosija
                  pomi=-SpH(k)*sinija
                else
                  pomr= SpH(k)*sinija
                  pomi=-SpH(k)*cosija
                endif
                ader(kk)=ader(kk)+pomr
                if(NCSymmRef(KPhase).eq.1) bder(kk)=bder(kk)+pomi
              enddo
            enddo
          endif
          if(metoda.eq.0) call calcm1(i,2)
4950      js=js+NCSymmRef(KPhase)
        enddo
        fta=aii*pysvej
        fta=fta*float(ismRef(i))
        if(phfp(isw).ne.0..and.phf(i).ne.0..and..not.nemod)
     1    fta=exp(phfp(isw)*phf(i))
        if(ifri.eq.1) then
          pom=pi4*sinthl*xfri
          fj0=sin(pom)/pom
          dj0=pi4*sinthl/tan(pom)-1./xfri
        else if(ifri.eq.2) then
!          pom=2.*sqrt(hj**2*prcp(1,isw,KPhase)+kj**2*prcp(2,isw,KPhase)+
!     1                hj*kj*prcp(4,isw,KPhase))
          pom=pom*xfri
          fj0= bessj0(pom)
          dj0=-bessj1(pom)*pom/fj0
        endif
        if(ifri.ne.0) fta=fta*fj0
        ftast=fta
        if(izo) then
          tfi=ExpJana(-sinthlq*par(5),ich)
          if(tfi.gt.1000..or.ich.ne.0) cycle
          fta=fta*tfi
        endif
        if(lasmaxi.gt.0) then
          do l=NderChd,NderChd+2
            pom=ader(l)
            ader(l)=ader1*pom
            if(NCSymmRef(KPhase).eq.1) bder(l)=bder1*pom
          enddo
        else if(MagParI.ne.0) then
          do l=1,3
            if(NCSymmRef(KPhase).eq.1) then
              AMag(l)=AMag(l)+aderm(l,1)*fta
              BMag(l)=BMag(l)+bderm(l,1)*fta
            else
              if(ZCSymmRef(KPhase).gt.0.) then
                AMag(l)=AMag(l)+aderm(l,1)*fta
              else
                BMag(l)=BMag(l)+bderm(l,1)*fta
              endif
            endif
          enddo
        endif
        ftacos =fta*ader(1)
        ftacos0=fta*ader10
        if(NCSymmRef(KPhase).eq.1) then
          ftasin =fta*bder(1)
          ftasin0=fta*bder10
        endif
        a =a+ ftacos
        af=af+ftacos0
        if(itw.eq.1.and.lasmaxi.gt.0) then
          afst=afst+ftast*aderst
          affree=affree+fta*fxrfree*ader1*fxr0
          affreest=affreest+ftast*aderfreest
        endif
        aanom=aanom+fyri*fta*scosijb
        if(NCSymmRef(KPhase).eq.1) then
          b =b +ftasin
          bf=bf+ftasin0
          if(itw.eq.1.and.lasmaxi.gt.0) then
            bfst=bfst+ftast*bderst
            bffree=bffree+fta*fxrfree*bder1*fxr0
            bffreest=bffreest+ftast*bderfreest
          endif
          banom=banom+fyri*fta*ssinijb
        endif
        if(abs(fxr).gt..001) then
          fyr(i)=fyri/fxr
        else
          fyr(i)=0.
        endif
        if(.not.CalcDer) cycle
        if(izo) then
          ader(5)=-sinthlq*ader(1)
          if(NCSymmRef(KPhase).eq.1) bder(5)=-sinthlq*bder(1)
          if(MagParI.ne.0) then
            do j=1,3
              if(NCSymmRef(KPhase).eq.1) then
                aderm(j,5)=-sinthlq*aderm(j,1)
                bderm(j,5)=-sinthlq*bderm(j,1)
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  aderm(j,5)=-sinthlq*aderm(j,1)
                else
                  bderm(j,5)=-sinthlq*bderm(j,1)
                endif
              endif
            enddo
          endif
        endif
        if(kanref(KDatBlock).ne.0) then
          der(kanomr)=der(kanomr)+ftacos/fxr
          if(NCSymmRef(KPhase).eq.1) dc(kanomr)=dc(kanomr)+ftasin/fxr
        endif
        if(NDimI(KPhase).gt.0) then
          if(metoda.eq.0) call calcm1(i,3)
          if(.not.nemod) then
            l=m+DelkaKiAtomu(i)-1
            der(l)=phfp(isw)*ftacos
            if(NCSymmRef(KPhase).eq.1) dc(l)=phfp(isw)*ftasin
          endif
        endif
        ader(1)=ader(1)/aii
        bder(1)=bder(1)/aii
        if(MagParI.ne.0) then
          do j=1,3
            if(NCSymmRef(KPhase).eq.1) then
              aderm(j,1)=aderm(j,1)/aii
              bderm(j,1)=bderm(j,1)/aii
            else
              if(ZCSymmRef(KPhase).gt.0.) then
                aderm(j,1)=aderm(j,1)/aii
              else
                bderm(j,1)=bderm(j,1)/aii
              endif
            endif
          enddo
        endif
        l=m
        ftap=fta
        do n=1,NDerAll
          if(n.ge.NDerModFirst.and.n.le.NDerModLast) then
            fta=ftap*fxr
            ftam=ftap*fxm
          else
            fta=ftap
            if(MagParI.gt.0.and.n.lt.NDerModFirst) then
              ftam=ftap
            else
              ftam=ftap*fxm
            endif
          endif
          der(l)=ader(n)*fta
          if(NCSymmRef(KPhase).eq.1) then
            dc(l)=bder(n)*fta
          else
            dc(l)=0.
          endif
          if(MagParI.ne.0) then
            do j=1,3
              if(NCSymmRef(KPhase).eq.1) then
                derm(j,l)=aderm(j,n)*ftam
                dcm (j,l)=bderm(j,n)*ftam
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  derm(j,l)=aderm(j,n)*ftam
                  dcm (j,l)=0.
                else
                  derm(j,l)=0.
                  dcm (j,l)=bderm(j,n)*ftam
                endif
              endif
            enddo
          else if(MagneticType(KPhase).ne.0) then
            do j=1,3
              derm(j,l)=0.
              dcm (j,l)=0.
            enddo
          endif
          l=l+1
        enddo
        if(ifri.ne.0) then
          k=m+idfr-1
          der(k)=dj0*ftacos
          if(NCSymmRef(KPhase).eq.1) dc(k)=dj0*ftasin
        endif
      enddo
      a=a*CentrRef(KPhase)
      b=b*CentrRef(KPhase)
      af=af*CentrRef(KPhase)
      bf=bf*CentrRef(KPhase)
      if(NDimI(KPhase).eq.0.and.itw.eq.1) then
        affree=affree*CentrRef(KPhase)
        bffree=bffree*CentrRef(KPhase)
        afst=afst*CentrRef(KPhase)
        bfst=bfst*CentrRef(KPhase)
        affreest=affreest*CentrRef(KPhase)
        bffreest=bffreest*CentrRef(KPhase)
      endif
      a=a-banom*CentrRef(KPhase)
      b=b+aanom*CentrRef(KPhase)
      sqrtab=sqrt(a**2+b**2)
      IZdvih=(KDatBlock-1)*MxScAll
      der(iq+IZdvih)=sqrtab
      sciq=sc(iq,KDatBlock)
      if(isPowder) then
        sciq=(sciq*.01)**2
        if(NPhase.gt.1) sciq=sciq*abs(sctw(KPhase,KDatBlock))
        sciq=sqrt(sciq)
      endif
      if(OverAllB(KDatBlock).ne.0.) then
        TFOverAll=ExpJana(-sinthlq*episq*OverAllB(KDatBlock),ich)
      else
        TFOverAll=1.
      endif
      FCalc=sqrtab*sciq*TFOverAll
      ACalc=a*sciq*TFOverAll
      BCalc=b*sciq*TFOverAll
      if(ExtTensor(KDatBlock).gt.0) then
        call philip(0,extinc,extink)
        extkor=sqrt(extinc)
        FCalc=FCalc*extkor
        ACalc=ACalc*extkor
        BCalc=BCalc*extkor
        if(CalcDer) then
          der(iq+IZdvih)=der(iq+IZdvih)*extkor
          if(extinc.gt.0.) then
            PhiPom=extkor*extink/extinc
          else
            PhiPom=0.
          endif
        endif
      else
        PhiPom=1.
      endif
      if(MagneticType(KPhase).ne.0) then
        PomSc=CentrRef(KPhase)*sciq*TFOverAll
        call Multm(MetTens(1,1,KPhase),AMag,AMagR,3,3,1)
        call Multm(MetTens(1,1,KPhase),BMag,BMagR,3,3,1)
        FCalcNucl=FCalc
        FCalcMag=sqrt(max(
     1             VecOrtScal(AMag,AMagR,3)+VecOrtScal(BMag,BMagR,3)
     1             -VecOrtScal(AMag,HMagR(1,1),3)**2
     2             -VecOrtScal(BMag,HMagR(1,1),3)**2,0.))*PomSc
        if(MagPolFlag(KDatBlock).ne.0) then
          do i=1,3
            AMagM(i)=AMag(i)-VecOrtScal(AMag,HMagR(1,1),3)*HMag(i,1)
            BMagM(i)=BMag(i)-VecOrtScal(BMag,HMagR(1,1),3)*HMag(i,1)
          enddo
          call Multm(MetTens(1,1,KPhase),AMagM,AMagMR,3,3,1)
          call Multm(MetTens(1,1,KPhase),BMagM,BMagMR,3,3,1)
          call MultM(UBRef,AMagMR,AMagMP,3,3,1)
          call MultM(UBRef,BMagMR,BMagMP,3,3,1)
          FCalcMagPol=(ACalc*AMagMP(3)+BCalc*BMagMP(3))*PomSc
        endif
        derm(1,iq+IZdvih)=FCalcMag
        if(CalcDer.and.FCalcMag.gt.0) then
          pom=PomSc**2/FCalcMag
          do i=1,NAtCalc
            if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder).or.
     1         MagPar(i).eq.0) cycle
            l=PrvniKiAtomu(i)
            do m=l,l+DelkaKiAtomu(i)-1
              if(MagPolFlag(KDatBlock).ne.0) then
                do j=1,3
                  DerAMagM(j)=derm(j,m)
     1              -VecOrtScal(derm(1,m),HMagR(1,1),3)*HMag(j,1)
                  DerBMagM(j)=dcm(j,m)
     1              -VecOrtScal(dcm(1,m),HMagR(1,1),3)*HMag(j,1)
                enddo
                call MultM(MetTens(1,1,KPhase),DerAMagM,DerAMagMR,3,3,1)
                call MultM(MetTens(1,1,KPhase),DerBMagM,DerBMagMR,3,3,1)
                call MultM(UBRef,DerAMagMR,DerAMagMP,3,3,1)
                call MultM(UBRef,DerBMagMR,DerBMagMP,3,3,1)
                DerPol(1,m)=(ACalc*DerAMagMP(3)+AMagMP(3)*der(m)+
     1                       BCalc*DerBMagMP(3)+BMagMP(3)*dc(m))*PomSc
              endif
              derm(1,m)=pom*(VecOrtScal(AMagR,derm(1,m),3)+
     1                       VecOrtScal(BMagR,dcm(1,m),3)
     2                      -VecOrtScal(AMag,HMagR(1,1),3)*
     3                       VecOrtScal(derm(1,m),HMagR(1,1),3)
     4                      -VecOrtScal(BMag,HMagR(1,1),3)*
     5                       VecOrtScal(dcm(1,m),HMagR(1,1),3))
            enddo
          enddo
        endif
        if(ExtTensor(KDatBlock).gt.0) then
          call philip(1,extincm,extinkm)
          extkorm=sqrt(extincm)
          FCalcMag=FCalcMag*extkorm
          if(CalcDer) then
            derm(1,iq+IZdvih)=derm(1,iq+IZdvih)*extkorm
            if(extincm.gt.0.) then
              PhiPomM=extkorM*extinkM/extincM
            else
              PhiPomM=0.
            endif
          endif
        else
          PhiPomM=1.
        endif
      endif
6100  if(ExtTensor(KDatBlock).gt.0) ie=MxScAll*(KDatBlock-1)+MxSc+7
      yct(itw,iov)=FCalc
      ycq(itw)=FCalc**2
      if(MagneticType(KPhase).ne.0) then
        yctm(itw,iov)=FCalcMag
        ycqm(itw)=FCalcMag**2
        yctmp(itw,iov)=FCalcMagPol
      endif
      afour(itw,iov)=af
      bfour(itw,iov)=bf
      act(itw,iov)=a
      bct(itw,iov)=b
      if(NTwin.gt.1) then
        FCalcScTw=FCalc*sctw(itw,KDatBlock)
        yctw=yctw+FCalcScTw*FCalc
        if(MagneticType(KPhase).ne.0) then
          FCalcMagScTw=FCalcMag*sctw(itw,KDatBlock)
          yctwm=yctwm+FCalcMagScTw*FCalcMag
          yctwmp=yctwmp+FCalcMagPol*sctw(itw,KDatBlock)
        endif
        if(itw.eq.NTwin) then
          if(yctw.ge.0.) then
            FCalc=sqrt(yctw)
          else
            FCalc=0.
          endif
          if(MagneticType(KPhase).ne.0.and.yctw.ge.0.) then
            FCalcMag=sqrt(yctwm)
            FCalcMagPol=yctwmp
          endif
        endif
        if(ExtTensor(KDatBlock).gt.0.and.CalcDer) then
          j=0
          do i=ie,ie+11
            j=j+1
            if(itw.eq.1) dertw(i)=0.
            dertw(i)=dertw(i)+FCalcScTw*der(i)
            if(MagneticType(KPhase).ne.0) then
              if(itw.eq.1) dertwm(i)=0.
              dertwm(i)=dertwm(i)+FCalcMagScTw*derme(j)
            endif
          enddo
        endif
      endif
      if(CalcDer) then
        if(nicova) then
          c1=0.
          c2=0.
        else
          if(sqrtab.ne.0.) sqrtab=sciq*TFOverAll/sqrtab
          c1=a*sqrtab*CentrRef(KPhase)*PhiPom
          c2=b*sqrtab*CentrRef(KPhase)*PhiPom
          if(NTwin.gt.1) then
            c1=c1*FCalcScTw
            c2=c2*FCalcScTw
          else
            if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
              pc=2.*Fcalc
              c1=c1*pc
              c2=c2*pc
              if(ExtTensor(KDatBlock).gt.0) then
                do i=ie,ie+11
                  der(i)=der(i)*pc
                enddo
                if(MagneticType(KPhase).ne.0) then
                  pc=2.*FCalcMag
                  do i=1,12
                    derme(i)=derme(i)*pc
                  enddo
                endif
              endif
            endif
          endif
        endif
        if(NTwin.gt.1.and.itw.eq.NTwin) then
          if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
            NormNuclear=2.
          else
            if(FCalc.gt.0.) then
              NormNuclear=1./FCalc
            else
              NormNuclear=0.
            endif
          endif
          if(ExtTensor(KDatBlock).gt.0) then
            do i=ie,ie+11
              der(i)=dertw(i)*NormNuclear
            enddo
          endif
          if(MagneticType(KPhase).ne.0) then
            if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
              NormMagnetic=2.
            else
              if(FCalcMag.gt.0.) then
                NormMagnetic=1./FCalcMag
              else
                NormMagnetic=0.
              endif
            endif
            if(ExtTensor(KDatBlock).gt.0) then
              j=0
              do i=ie,ie+11
                j=j+1
                derme(j)=dertwm(i)*NormMagnetic
              enddo
            endif
          endif
        endif
        if(kanref(KDatBlock).ne.0) then
          j=ianomr
          l=ianomi
          do i=1,NAtFormula(KPhase)
            j=j+1
            l=l+1
            der(l)=-c1* dc(j)+c2*der(j)
            der(j)= c1*der(j)+c2* dc(j)
            if(NTwin.gt.1) then
              if(itw.eq.1) then
                dertw(l)=0.
                dertw(j)=0.
              endif
              dertw(l)=dertw(l)+der(l)
              dertw(j)=dertw(j)+der(j)
              if(itw.eq.NTwin) then
                der(l)=dertw(l)*NormNuclear
                der(j)=dertw(j)*NormNuclear
              endif
            endif
          enddo
        endif
        m=ndoff
        do i=1,NAtCalc
          if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder)) cycle
          ca=c1+fyr(i)*c2
          cb=c2-fyr(i)*c1
          j=PrvniKiAtomu(i)
          do m=j,j+DelkaKiAtomu(i)-1
            if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1         ExistM42.and.CalcDyn) then
              dcp(m)=CentrRef(KPhase)*PhiPom*(der(m)+fyr(i)*dc (m))
              dc (m)=CentrRef(KPhase)*PhiPom*(dc(m) -fyr(i)*der(m))
            else
              der(m)=ca*der(m)+cb*dc(m)
            endif
            if(MagneticType(KPhase).ne.0) derm(1,m)=PhiPomM*derm(1,m)
            if(NTwin.gt.1) then
              if(itw.eq.1) dertw(m)=0.
              dertw(m)=dertw(m)+der(m)
              if(itw.eq.NTwin) der(m)=dertw(m)*NormNuclear
              if(MagneticType(KPhase).ne.0) then
                if(itw.eq.1) dertwm(m)=0.
                dertwm(m)=dertwm(m)+derm(1,m)*FCalcMagScTw
                if(itw.eq.NTwin) derm(1,m)=dertwm(m)*NormMagnetic
              endif
            endif
          enddo
        enddo
      endif
      if(MagneticType(KPhase).ne.0.and.itw.eq.NTwin) then
        if(OnlyMag.gt.0) then
          FCalcB=0.
          FCalc=FCalcMag
        else
          FCalcB=FCalc
          if(MagPolFlag(KDatBlock).ne.0) then
           F1=FCalc**2+FCalcMag**2+2.*FCalcMagPol*MagPolPlus(KDatBlock)
           F2=FCalc**2+FCalcMag**2+2.*FCalcMagPol*MagPolMinus(KDatBlock)
           FCalc=F1/F2
          else
            FCalc=sqrt(FCalc**2+FCalcMag**2)
          endif
        endif
        if(FCalc.gt.0.) then
          FCalcR=1./FCalc
        else
          FCalcR=0.
        endif
        if(CalcDer.and.MagPolFlag(KDatBlock).eq.0) then
          if(ExtTensor(KDatBlock).gt.0.and.CalcDer) then
            j=0
            do i=ie,ie+11
              j=j+1
              if(j.eq.1) then
                pom=1./ecMag(1,KDatBlock)
                k=ie-5
                der(k)=0.
              else if(j.eq.7) then
                pom=1./ecMag(2,KDatBlock)
                k=ie-4
                der(k)=0.
              endif
              if(OnlyMag.gt.0) der(i)=0.
              if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
                der(i)=der(i)+pom*derme(j)
                der(k)=der(k)+derme(j)*ec(j,KDatBlock)
              else
                der(i)=FCalcR*(FCalcB*der(i)+
     1                         pom*FCalcMag*derme(j))
                der(k)=der(k)+FCalcR*FCalcMag*derme(j)*ec(j,KDatBlock)
              endif
            enddo
          endif
        endif
        do i=1,NAtCalc
          if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder)) cycle
          l=PrvniKiAtomu(i)
          if(MagPolFlag(KDatBlock).gt.0) then
            if(F2.gt.0.) then
              RF2=1./F2**2
              F2mF1=2.*(F2-F1)
              F2mF1Pol=2.*(F2*MagPolPlus(KDatBlock)-
     1                     F1*MagPolMinus(KDatBlock))
            else
              RF2=0.
            endif
          endif
          do m=l,l+DelkaKiAtomu(i)-1
            if(MagPolFlag(KDatBlock).eq.0) then
              if(ifsq.eq.1) then
                der(m)=der(m)+2.*FCalcMag*derm(1,m)
              else
                der(m)=FCalcR*(FCalcB*der(m)+FCalcMag*derm(1,m))
              endif
            else
              if(F2.gt.0.) then
                der(m)=RF2*(F2mF1*(der(m)*FCalc+derm(1,m)*FCalcMag)+
     1                      F2mF1Pol*derpol(1,m))
              else
              endif
            endif
          enddo
        enddo
      endif
      go to 9999
9600  ch80='kappa or kappa'' of the atom "'//atom(i)(:idel(atom(i)))//
     1     '" is not positive'
      go to 9900
9700  ch80='Slater function for the atom "'//atom(i)(:idel(atom(i)))//
     1     '" cannot be calculated'
      go to 9900
9900  if(KeyDyn.ne.0.and.icykl.gt.0) then
        ErrFlag=2
      else
        t80='('
        do i=1,NDim(KPhase)
          write(Cislo,'(i5,'','')') IHRead(i)
          call Zhusti(Cislo)
          t80=t80(:idel(t80))//Cislo
        enddo
        i=idel(t80)
        t80(i:i)=')'
        call FeChybne(-1.,YBottomMessage,'numerical problem for '//
     1                'reflection '//t80(:i)//' in CALC.',
     1                ch80,SeriousError)
        ErrFlag=1
      endif
9999  return
      end
