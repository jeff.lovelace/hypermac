      subroutine RefDSetKeep
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      KPhaseIn=KPhase
      do n=1,NKeep
        if(KeepType(n).eq.IdKeepARiding) then
          nc=KeepNAtCentr(n)
          KPhase=kswa(nc)
          itfc=itf(nc)
          isw=iswa(nc)
          kicp=PrvniKiAtomu(nc)+4
          pom=KeepADPExtFac(n)
          do i=1,KeepNH(n)
            ia=KeepNAtH(n,i)
            itfh=itf(ia)
            kih=PrvniKiAtomu(ia)+4
            kica=kicp
            if(itfh.ge.2) then
              if(itfc.ge.2) then
                do j=1,6
                  der(kica)=der(kica)+der(kih)*pom
                  kih=kih+1
                  kica=kica+1
                enddo
              else
                do j=1,6
                  der(kica)=der(kica)+der(kih)*pom*prcp(j,isw,KPhase)
                  kih=kih+1
                enddo
              endif
            else
              if(itfc.ge.2) then
                pom=pom*1.3333333
                do j=1,6
                  call indext(j,k,l)
                  k=k+(l-1)*3
                  der(kica)=der(kica)+der(kih)*pom*MetTens(k,isw,KPhase)
                  kica=kica+1
                enddo
              else
                der(kica)=der(kica)+der(kih)*pom
              endif
            endif
          enddo
        endif
      enddo
      KPhase=KPhaseIn
      return
      end
