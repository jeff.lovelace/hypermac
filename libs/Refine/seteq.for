      subroutine SetEq(i1,i2)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*128 ven
      character*80  t80,s80
      character*20  at
      integer pocder,chybna,pnpji
      logical PsatLabel
      data PsatLabel/.true./
      go to 1000
      entry ResetSetEq
      PsatLabel=LstOpened
      go to 9999
1000  if(i1.gt.i2) go to 9999
      if(PsatLabel) call newln(3)
      if(i2.le.neqs) then
        n=2
        if(LstOpened) write(lst,'(/''Equations defined by user :''/)')
      else
        n=1
        if(PsatLabel) then
          write(lst,'(/''Equations induced by symmetry :''/)')
          PsatLabel=.false.
        endif
      endif
      do i=i1,i2
        if(n.eq.1.or.OrthoOrd.le.0) eqhar(i)=.false.
        chybna=0
        ven=lpa(i)(:idel(lpa(i)))
        at=lat(i)
        if(at.eq.' ') then
          ven=ven(:idel(ven))//'='
        else
          ven=ven(:idel(ven))//'['//at(:idel(at))//']='
        endif
        if(RizikoX4.and.n.eq.2) then
          do j=1,3
            do jj=1,2
              if(jj.eq.1) then
                Cislo=SmbX(j)//'sin'
              else
                Cislo=SmbX(j)//'cos'
              endif
              if(LocateSubString(lpa(i),Cislo(:idel(Cislo)),
     1                           .false.,.true.).gt.0) then
                RizikoX4=.false.
                go to 1050
              endif
            enddo
          enddo
          if(MagneticType(KPhase).gt.0.and.ktatmol(at).gt.0) then
            do j=1,3
              do jj=1,2
                if(jj.eq.1) then
                  Cislo=SmbP(j)//'sin'
                else
                  Cislo=SmbP(j)//'cos'
                endif
                if(LocateSubString(lpa(i),Cislo(:idel(Cislo)),
     1                             .false.,.true.).gt.0) then
                  RizikoX4=.false.
                  go to 1050
                endif
              enddo
            enddo
          endif
        endif
1050    if(lnp(i).ge.0) then
          if(lnp(i).lt.PrvniKiAtXYZMode) then
            k=pocder(ktatmol(at))
            if(k.eq.0) then
              chybna=1
              go to 1300
            else
              if(lnp(i).gt.0) then
                lnp(i)=lnp(i)+k
                if(EqHar(i)) then
                  lnpo(i)=lnpo(i)+k
                else
                  lnpo(i)=0
                endif
              else if(lnp(i).eq.0) then
                chybna=2
                t80=ven(:idel(ven)-1)
              endif
            endif
          endif
        else
          if(lnp(i).eq.-ndoff-1) then
            chybna=3
            go to 1300
          else if(lnp(i).le.-ndoff-2) then
            chybna=4
            go to 1300
          endif
          lnp(i)=-lnp(i)
          if(EqHar(i)) lnpo(i)=-lnpo(i)
        endif
        if(chybna.eq.0) then
          if(eqhar(i)) then
            call RefSetTrueKi(lnpo(i),0)
          else
            call RefSetTrueKi(lnp(i),0)
          endif
          if(n.eq.2) NConstrain=NConstrain+1
        endif
        if(pab(i).ne.0..or.npa(i).eq.0) then
          call Real2String(pab(i),s80,4)
          call zhusti(s80)
          call RealToFreeForm(s80)
          if(idel(s80).eq.0) go to 1100
          ven=ven(:idel(ven))//s80(:idel(s80))
        endif
1100    do j=1,npa(i)
          pom=pko(j,i)
          call Real2String(pom,s80,4)
          call RealToFreeForm(s80)
          k=idel(ven)
          if(ven(k:k).ne.'='.and.pom.ge.0.) s80(1:1)='+'
          call RealToFreeForm(s80)
          call zhusti(s80)
          if(s80.eq.'0') then
            pko(j,i)=0.
            pom=0.
          endif
          if(s80.eq.'+1') then
            ven=ven(:k)//'+'
          else if(s80.eq.'-1') then
            ven=ven(:k)//'-'
          else if(s80.ne.'1') then
            ven=ven(:k)//s80(:idel(s80))//'*'
          endif
          k=idel(ven)
          ven=ven(:k)//ppa(j,i)(:idel(ppa(j,i)))
          kp=k+1
          at=pat(j,i)
          if(at.ne.' ')
     1      ven=ven(:idel(ven))//'['//at(:idel(at))//']'
          pnpji=pnp(j,i)
          if(pnpji.ge.0) then
            if(pnpji.lt.PrvniKiAtXYZMode) then
              if(chybna.eq.0) then
                k=pocder(ktatmol(at))
                if(k.eq.0) then
                  chybna=1
                else
                  if(pnpji.gt.0) then
                    if(pnpji.le.2*mxdmm) then
                      pnp(j,i)=pnpji+k
                    else
                      pnp(j,i)=10*ktatmol(at)-pnpji+2*mxdmm
                    endif
                  else if(pnpji.eq.0) then
                    chybna=2
                    t80=ven(kp:)
                  endif
                endif
              endif
            endif
          else
            if(pnpji.eq.-ndoff-1) then
              chybna=3
              go to 1300
            else if(pnpji.le.-ndoff-2) then
              chybna=4
              go to 1300
            endif
            pnp(j,i)=-pnpji
          endif
        enddo
1300    if(Chybna.eq.1.or.Chybna.eq.3) then
          j=idel(ven)
          ven=ven(:idel(ven))//'['//at(:idel(at))//']'//' ...'
        endif
        if(LstOpened) then
          call newln(1)
          write(lst,FormA) ven(:idel(ven))
        endif
        if(chybna.ne.0) then
          npa(i)=-1
          if(chybna.eq.1) then
            t80='atom "'//at(:idel(at))//'" isn''t on the file M40'
          else if(chybna.eq.2) then
            call SetIgnoreETo(.true.)
            t80='the parameter "'//t80(:idel(t80))//'" isn''t '//
     1          'applicable.'
          else if(chybna.eq.3) then
            t80='phase "'//at(:idel(at))//'" isn''t defined.'
          else if(chybna.eq.4) then
            t80='DatBlock "'//at(:idel(at))//'" isn''t defined.'
          else
            t80=' '
          endif
          j=index(ven,'=')
          if(j.le.0) j=min(idel(ven),15)
          if(j.gt.1) then
            ven='Equation "'//ven(:j)//'..."'
          else
            ven='The relevant equation'
          endif
          ven=ven(:idel(ven))//' will be omitted.'
          call FeChybne(-1.,-1.,t80,ven,WarningWithESC)
          call SetIgnoreETo(.false.)
        else
          call RefAppEq(n,i,1)
        endif
        if(ErrFlag.ne.0) go to 9999
      enddo
100   format(f15.4)
9999  return
      end
