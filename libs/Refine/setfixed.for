      subroutine SetFixed
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*12 at,pn,AtFix,PnFix
      character*80 Hleda,Nasel
      logical EqWild,JeToMolekula,PridatQR
      if(NComp(KPhase).gt.1.and.(kic.gt.0.and.kic.lt.4)) then
        iap=NAtIndFr(kic,KPhase)
        iak=NAtIndTo(kic,KPhase)
      else
        iap=NAtIndFrAll(KPhase)
        iak=NAtIndToAll(KPhase)
      endif
      if(NComp(KPhase).gt.1.and.(kic.gt.0.and.kic.lt.4)) then
        imp=NMolecFr(kic,KPhase)
        imk=NMolecTo(kic,KPhase)
      else
        imp=NMolecFrAll(KPhase)
        imk=NMolecToAll(KPhase)
      endif
      if(NCSymmRef(KPhase).eq.1.and.NAtCalc.gt.0.and.DoLeBail.eq.0) then
        do i=1,NFixOrigin
          if(KFixOrigin(i).le.0) cycle
          At=AtFixOrigin(i)
          ktery=ktatmol(At)
          if(ktery.lt.0) then
            im=(-ktery-1)/mxp+1
            isw=iswmol(im)
            ksw=kswmol(im)
          else if(ktery.gt.0.and.ktery.lt.NAtMolFr(1,1)) then
            isw=iswa(ktery)
            ksw=kswa(ktery)
          endif
          if(ksw.eq.KPhase) go to 2300
        enddo
        isw=1
        ktery=0
2300    if(ktery.ne.0) then
          if(NComp(KPhase).gt.1.and.kic.gt.0.and.kic.lt.4.and.
     1       isw.ne.kic) then
            call FeChybne(-1.,-1.,'atom/molecule'//At(:idel(At))//
     2                    ' isn''t from the relevant composite part',
     3                    'The origin will be fixed according to '//
     4                    'default procedure',Warning)
            if(ErrFlag.ne.0) go to 9999
            ktery=0
          endif
        endif
      else
        isw=1
        Ktery=0
      endif
      call RefFixOriginProlog(Ktery,isw)
3000  if(NDim(KPhase).ne.4.or.NonModulated(KPhase)) go to 3500
      do i=1,NSymmN(KPhase)
        if(rm6(16,i,1,KPhase).lt.0) go to 3500
      enddo
      if(NCSymmRef(KPhase).eq.1) then
        do i=1,NFixX4
          m=KFixX4(i)
          At=AtFixX4(i)
          ktery=ktatmol(At)
          if(ktery.lt.0) then
            im=(-ktery-1)/mxp+1
            isw=iswmol(im)
            ksw=kswmol(im)
            call RefGetNParMol(m,-Ktery,.false.,PridatQR,ich)
            if(ich.ne.0) go to 3150
          else if(ktery.gt.0) then
            isw=iswa(ktery)
            ksw=kswa(ktery)
            call RefGetNParAt(m,Ktery,.false.,PridatQR,ich)
            if(ich.ne.0) go to 3150
          else
            go to 3150
          endif
          if(ksw.eq.KPhase) then
            ipol=i
            ktera=m
            go to 3200
          endif
        enddo
3150    ktery=0
3200    if(ktery.ne.0) then
          if(ktery.lt.0) then
            if(isw.eq.1) then
              if(ktera.le.28) go to 3350
            else
              if(ktera.lt.5.or.ktera.gt.7) go to 3350
            endif
            call ChangeMolCompress(ktera,j,-ktery,0,ich)
            if(ich.ne.0) go to 3350
            if(KiMol(j,-Ktery).ne.0) then
              NConstrain=NConstrain+1
              KiMol(j,-Ktery)=0
            endif
          else
           if(isw.eq.1) then
              if(ktera.le.10) go to 3350
            else
              if(ktera.eq.1.or.ktera.gt.5) go to 3350
            endif
            call ChangeAtCompress(ktera,j,ktery,0,ich)
            if(ich.ne.0) go to 3350
            if(KiA(j,Ktery).ne.0) then
              NConstrain=NConstrain+1
              KiA(j,Ktery)=0
            endif
          endif
          go to 3500
        else
          go to 3350
        endif
        go to 4000
3350    RizikoX4=.true.
      endif
      go to 4000
3500  RizikoX4=.false.
4000  if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_fixed.tmp','unformatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 6000
4100  read(ln,err=6000,end=6000) AtFix,PnFix,KFix,ValFix
      if(KFix.gt.0) then
4200    ia=ktatmv(AtFix,NAtInd,0)
        if(ia.gt.0) then
          if(kswa(ia).ne.KPhase) go to 4200
          if(KFix.lt.IdFixADP) then
            k=2
          else if(KFix.lt.IdFixModul) then
            k=5
          else
            k=max(TRankCumul(itf(ia)),10)+1
          endif
          if(KFix.eq.IdFixAllPar.or.KFix.eq.IdFixModul.or.
     1       KFix.eq.IdFixChden) then
            l=DelkaKiAtomu(ia)
          else if(KFix.eq.IdFixCoord) then
            l=4
          else
            l=max(TRankCumul(itf(ia)),10)
          endif
          ish=PrvniKiAtomu(ia)-1
          JeToMolekula=.false.
        else if(ia.lt.0) then
          ia=-ia
          im=(ia-1)/mxp+1
          if(kswmol(im).ne.KPhase) go to 4200
          if(KFix.lt.IdFixADP) then
            k=2
          else if(KFix.lt.IdFixModul.or.ktls(im).le.0) then
            k=8
          else
            k=29
          endif
          if(KFix.eq.IdFixAllPar.or.KFix.eq.IdFixModul) then
            l=DelkaKiMolekuly(ia)
          else if(KFix.eq.IdFixCoord.or.ktls(im).le.0) then
            l=7
          else
            l=28
          endif
          ish=PrvniKiMolekuly(ia)-1
          JeToMolekula=.true.
        else if(ia.eq.0) then
          go to 4100
        endif
        do ii=k,l
          if(RizikoX4) then
            call KdoCo(ii+ish,at,pn,0,pom,spom)
            do j=1,3
              do jj=1,2
                if(jj.eq.1) then
                  Cislo=SmbX(j)//'sin'
                else
                  Cislo=SmbX(j)//'cos'
                endif
                if(LocateSubString(pn,Cislo(:idel(Cislo)),
     1                             .false.,.true.).gt.0) then
                  RizikoX4=.false.
                  go to 5400
                endif
              enddo
            enddo
            if(MagneticType(KPhase).gt.0) then
              if(ia.gt.0) then
                do j=1,3
                  do jj=1,2
                    if(jj.eq.1) then
                      Cislo=SmbP(j)//'sin'
                    else
                      Cislo=SmbP(j)//'cos'
                    endif
                    if(LocateSubString(pn,Cislo(:idel(Cislo)),
     1                                 .false.,.true.).gt.0) then
                      RizikoX4=.false.
                      go to 5400
                    endif
                  enddo
                enddo
              endif
            endif
          endif
5400      if(JeToMolekula) then
            if(KiMol(ii,ia).ne.0) then
              NConstrain=NConstrain+1
              KiMol(ii,ia)=0
            endif
          else
            if(KiA(ii,ia).ne.0) then
              NConstrain=NConstrain+1
              KiA(ii,ia)=0
            endif
          endif
        enddo
        go to 4200
      else if(KFix.lt.0) then
        pom=0.
        spom=0.
        Hleda=PnFix
        if(AtFix.ne.' ') Hleda=Hleda(:idel(Hleda))//'['//
     1                         AtFix(:idel(AtFix))//']'
        do i=1,PosledniKiAtMagMode
          if(i.gt.PosledniKiAtMol.and.i.lt.PrvniKiMol) cycle
          call KdoCo(i,at,pn,0,pom,spom)
          Nasel=pn(:idel(pn))
          if(at.ne.' ')
     1      Nasel=Nasel(:idel(Nasel))//'['//at(:idel(at))//']'
          if(EqWild(Nasel,Hleda,.false.)) then
            call RefSetTrueKi(i,0)
            if(RizikoX4.and.i.gt.NdOff) then
              do j=1,3
                do jj=1,2
                  if(jj.eq.1) then
                    Cislo=SmbX(j)//'sin'
                  else
                    Cislo=SmbX(j)//'cos'
                  endif
                  if(LocateSubString(pn,Cislo(:idel(Cislo)),
     1                               .false.,.true.).gt.0) then
                    RizikoX4=.false.
                    go to 5500
                  endif
                enddo
              enddo
              if(MagneticType(KPhase).gt.0) then
                if(ia.gt.0) then
                  do j=1,3
                    do jj=1,2
                      if(jj.eq.1) then
                        Cislo=SmbP(j)//'sin'
                      else
                        Cislo=SmbP(j)//'cos'
                      endif
                      if(LocateSubString(pn,Cislo(:idel(Cislo)),
     1                                   .false.,.true.).gt.0) then
                        RizikoX4=.false.
                        go to 5500
                      endif
                    enddo
                  enddo
                endif
              endif
            endif
5500        if(KFix.eq.-2) call KdoCo(i,at,pn,-1,ValFix,spom)
          endif
        enddo
      endif
      go to 4100
6000  call CloseIfOpened(ln)
      if(RizikoX4.and.ncykl.gt.0) then
        call FeChybne(-1.,-1.,'origin in x4 axis may not be fixed.',
     1    'This can induce some instability in the refinement.',
     2    WarningWithEsc)
      endif
      if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
      go to 9999
9000  ErrFlag=1
9999  if(KPhase.ge.NPhase) call DeleteFile(fln(:ifln)//'_fixed.tmp')
      return
      end
