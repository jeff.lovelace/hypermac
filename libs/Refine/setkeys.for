      subroutine SetKeys
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      if(kim.eq.0.or.kim.eq.-2) then
        do KDatB=1,NDatBlock
          isPowder=iabs(DataType(KDatBlock)).eq.2
          if(UseDatBlockActual(KDatB)) then
            if(isPowder) then
              m=1
              call SetIntArrayTo(KiS(3,KDatB),MxScU-2,0)
            else
              m=mxscu
              call SetIntArrayTo(KiS(1,KDatB),MxScU,0)
            endif
            if(Radiation(KDatB).eq.ElectronRadiation.and.
     1         ExistM42.and.CalcDyn) then
              do i=1,m
                if(i.eq.1) then
                  sc(i,KDatB)=1.
                else
                  sc(i,KDatB)=0.
                endif
                KiS(i,KDatB)=0
              enddo
              do i=1,NEDZone(KDatB)
                if(UseEDZone(i,KDatB)) then
                  KiED(1,i,KDatB)=1
                  KiED(2,i,KDatB)=1
                else
                  KiED(1,i,KDatB)=0
                  KiED(2,i,KDatB)=0
                endif
              enddo
            else if(MagPolFlag(KDatB).eq.0) then
              do i=1,m
                if(sc(i,KDatB).ne.0.) KiS(i,KDatB)=1
              enddo
            endif
            if(.not.isPowder) then
              ip=mxsc+7
              call SetIntArrayTo(KiS(ip,KDatB),12,0)
              if(ExtTensor(KDatB).gt.0) then
                if(ExtTensor(KDatB).eq.1) then
                  n=1
                else
                  n=6
                endif
                if(ExtType(KDatB).gt.1) then
                  call SetIntArrayTo(KiS(ip  ,KDatB),n,1)
                else
                  call SetIntArrayTo(KiS(ip+6,KDatB),n,1)
                endif
                if(ExtType(KDatB).eq.3) then
                  KiS(ip+6,KDatB)=1
                  if(ec(1,KDatB).gt.99.*ec(7,KDatB)) then
                    kis(ip,KDatB)=0
                    ecs(1,KDatB)=0.
                  else if(ec(7,KDatB).gt.99.*ec(1,KDatB)) then
                    kis(ip+6,KDatB)=0
                    ecs(7,KDatB)=0.
                  endif
                endif
              endif
              if(Radiation(KDatB).eq.XRayRadiation.and.Lam2Corr.eq.1)
     1          then
                KiS(MxSc+4,KDatB)=1
              endif
            endif
          else
            call SetIntArrayTo(KiS(1,KDatB),MxScAll,0)
          endif
        enddo
      endif
      if(kim.ne.0) then
        kkk=1
      else
        kkk=0
      endif
      if(ExistPowder) then
        j=IBackgPwd
        do KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).eq.2) then
            if(UseDatBlockActual(KDatB)) then
              k=1
            else
              k=0
            endif
          else
            k=0
          endif
          call SetIntArrayTo(KiPwd(j),NBackg(KDatB),k)
          j=j+NParRecPwd
        enddo
      endif
      do KPh=1,NPhase
        KPhase=KPh
        if(NAtIndLenAll(KPh).le.0.and.NMolecLenAll(KPh).le.0)
     1    cycle
        if(NComp(KPh).gt.1.and.kic.gt.0.and.kic.lt.4) then
          iap=NAtIndFr(kic,KPh)
          iak=NAtIndTo(kic,KPh)
          imp=NMolecFr(kic,KPh)
          imk=NMolecTo(kic,KPh)
        else
          iap=NAtIndFrAll(KPh)
          iak=NAtIndToAll(KPh)
          imp=NMolecFrAll(KPh)
          imk=NMolecToAll(KPh)
        endif
        call SetAtKeys(NAtIndFrAll(KPh),NAtIndToAll(KPh),iap,iak,kkk)
        call SetMolKeys(NMolecFrAll(KPh),NMolecToAll(KPh),imp,imk,kkk)
      enddo
      do i=1,NAtXYZMode
        call SetIntArrayTo(KiAtXYZMode(1,i),NMAtXYZMode(i),1)
      enddo
      do i=1,NAtMagMode
        call SetIntArrayTo(KiAtMagMode(1,i),NMAtMagMode(i),1)
      enddo
      return
      end
