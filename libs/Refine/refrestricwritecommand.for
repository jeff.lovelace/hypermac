      subroutine RefRestricWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/RestricQuest/ nEdwAtoms,nEdwPhases,nCrwRestricAtMol,
     1                     nCrwRestricProfile,RestricType(6),OccType,
     2                     AtomString,LocalSymm,nRolMenuLocalSymm,
     3                     PhaseString,RestricProfile
      integer RestricType,OccType,RolMenuSelectedQuest
      character*(*) Command
      character*256 EdwStringQuest,AtomString,PhaseString
      character*80  t80
      logical  RestricProfile,EqIgCase
      save /RestricQuest/
      ich=0
      if(RestricProfile) then
        PhaseString=EdwStringQuest(nEdwPhases)
        k=0
        call kus(PhaseString,k,t80)
        Command='restric '//t80(:idel(t80))//' profile'
        if(k.lt.256) Command=Command(:idel(Command)+1)//
     1                       PhaseString(k+1:idel(PhaseString))
      else
        AtomString=EdwStringQuest(nEdwAtoms)
        if(AtomString.eq.' ') go to 9100
        call TestAtomString(AtomString,IdWildYes,IdAtMolYes,IdMolYes,
     1                      IdSymmNo,IdAtMolMixYes,t80)
        if(EqIgCase(t80,'Mixed')) then
          do i=1,3
            if(RestricType(i).ne.0) then
              call FeChybne(-1.,-1.,'For the mixed atom/molecule '//
     1                      'command only occupancies can be '//
     2                      'restriced.',' ',Warning)
              RestricType=0
              exit
            endif
          enddo
          t80=' '
        endif
        if(t80.ne.' ') then
          if(t80.ne.'Jiz oznameno')
     1      call FeChybne(-1.,-1.,t80,' ',SeriousError)
          ich=1
          go to 9999
        endif
        do i=1,2
          if(RestricType(i).gt.0) then
            RestricType(i+1:3)=1
            exit
          endif
        enddo
        if(NDimI(KPhase).le.0) RestricType(2)=0
        ii=0
        do i=1,3
          if(RestricType(i).gt.0) then
            ii=i
            exit
          endif
        enddo
        if(NDimI(KPhase).le.0) ii=min(ii,2)
        if(ii.eq.0) ii=IdRestNone
        jj=0
        do icd=6,4,-1
          jj=jj*2
          if(RestricType(icd).gt.0) jj=jj+1
        enddo
        ii=ii+100*jj
        if(OccType.eq.1) then
          i=-ii-10
        else if(OccType.eq.2) then
          i=-ii
        else if(OccType.eq.4) then
          i=10+ii
        else
          i=ii
        endif
        k=0
        call kus(AtomString,k,t80)
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        Command='restric '//t80(:idel(t80)+1)//Cislo(:idel(Cislo))
        if(nRolMenuLocalSymm.gt.0) then
          LocalSymm=RolMenuSelectedQuest(nRolMenuLocalSymm)
          if(LocalSymm.gt.1) then
            write(Cislo,'(i1)') LocalSymm-1
            Cislo='ls#'//Cislo(:idel(Cislo))
            Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
          endif
        endif
        if(k.lt.256) Command=Command(:idel(Command)+1)//
     1                       AtomString(k+1:idel(AtomString))
      endif
      go to 9999
9100  ich=1
9999  return
      end
