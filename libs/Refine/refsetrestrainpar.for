      subroutine RefSetRestrainPar(ia,ISym,isw,ksw,KiAtX,KiAtXMod,RmAt,
     1                             Rm6At,RmiiAt)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RmAt(*),Rm6At(*),RmiiAt(*)
      isw=iswa(ia)
      ksw=kswa(ia)
      KiAtX=PrvniKiAtomu(ia)+1
      if(NDim(KPhase).gt.3) then
        KiAtXMod=PrvniKiAtomu(ia)
        itfi=itf(ia)
        if(itfi.gt.0) then
          KiAtXMod=KiAtXMod+max(TRankCumul(itfi),10)
        else
          KiAtXMod=KiAtXMod+10
        endif
        if(KModA(1,ia).gt.0) KiAtXMod=KiAtXMod+1+2*KModA(1,ia)
      else
        KiAtXMod=0
      endif
      j=iabs(ISym)
      call CopyMat(rm(1,j,isw,ksw),RmAt,3)
      call CopyMat(rm6(1,j,isw,ksw),Rm6At,NDim(KPhase))
      if(ISym.lt.0) then
        call RealMatrixToOpposite(RmAt,RmAt,3)
        call RealMatrixToOpposite(Rm6At,Rm6At,NDim(KPhase))
      endif
      if(NDim(KPhase).gt.3)
     1  call GetGammaIntInv(rm6(1,j,isw,ksw),RmiiAt)
      return
      end
