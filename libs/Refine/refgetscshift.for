      subroutine RefGetScShift(NPar,StSc,OffSet)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      character*(*) StSc
      character*80 Veta
      integer OffSet
      logical EqIgCase
      KPh=1
      KDatB=1
      if(StSc.eq.' ') go to 1500
      i=index(StSc,'%')
      if(i.gt.0) then
        Veta=StSc(:i-1)
        KPh=ktat(PhaseName,NPhase,Veta)
        if(KPh.le.0) then
          OffSet=-1
          go to 9999
        endif
        Veta=StSc(i+1:)
        go to 1200
      endif
      Veta=StSc
      KPh=ktat(PhaseName,NPhase,Veta)
      if(KPh.gt.0) go to 1500
      KPh=1
1200  call mala(Veta)
      if(EqIgCase(Veta(:4),'zone')) then
        i=Index(Veta,'%')
        if(i.le.0) then
          Cislo=Veta(6:)
        else
          Cislo=Veta(6:i-1)
        endif
        call posun(Cislo,0)
        read(Cislo,FormI15) NZone
        OffSet=(NZone-1)*MxEDRef
        if(i.gt.0) then
          Cislo=Veta(i+1:)
          call posun(Cislo,0)
          read(Cislo,FormI15) KDatB
          OffSet=OffSet+MxEDRef*NDatBlock*(KDatB-1)
        endif
        go to 9999
      endif
      if(.not.EqIgCase(Veta(:5),'block')) then
        OffSet=-2
        go to 9999
      else
        Cislo=Veta(6:)
        call posun(Cislo,0)
        read(Cislo,FormI15) KDatB
      endif
1500  if(NPar.le.NdOffPwd) then
        OffSet=(KDatB-1)*MxScAll
      else if(NPar.le.NdOffPwd+NParRecPwd*MxDatBlock) then
        OffSet=(KDatB-1)*NParRecPwd
      else if(NPar.le.NdOffED) then
        OffSet=NParCellProfPwd*(KPh-1)+NParProfPwd*(KDatB-1)
      else
        OffSet=-3
      endif
9999  return
      end
