      subroutine RefPrelim(klic)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use Bessel_mod
      use Powder_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      character*256 EdwStringQuest
      character*128 ven
      character*80  Veta,p80
      character*80, allocatable :: AtName(:)
      character*30  FormPom1
      character*21  FormPom2
      character*12  Nazev
      character*12 :: IdGener(7) =
     1               (/'DMin    ','DMax    ','2ThMin  ','2ThMax  ',
     2                 '2ThStep ','MMax    ','SatFrMod'/)
      logical Prvni,PridatQR,EqRVM,EqRV,FeYesNoHeader,CrwLogicQuest,
     1        ExistFile,EqIV0
      integer :: CrlCentroSymm,CrlMagCenter,Seed(1),ihp(6),LimType=1
      real, allocatable :: F000R(:),F000I(:)
      data FormPom1/'(f7.4,'' with s.u.'',f7.4)'/,
     1     FormPom2/'('' with s.u.'',f7.4)'/
      call SetMag(-1)
      kolaps=.false.
      call OpenFile(lst,fln(:ifln)//'.ref','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      uloha='Refinement program'
      if(allocated(RNumObs))
     1  deallocate(yct,ycq,act,bct,afour,bfour,snt,yctm,ycqm,F000,F000N,
     2             yctmp,ycqmp)
      allocate(yct(NTwin,4),ycq(NTwin),act(NTwin,4),bct(NTwin,4),
     1         afour(NTwin,4),bfour(NTwin,4),snt(NTwin),yctm(NTwin,4),
     2         ycqm(NTwin),yctmp(NTwin,4),ycqmp(NTwin),F000(NPhase,3),
     3         F000N(NPhase,3))
      if(allocated(RNumObs))
     1  deallocate(RNumObs, RNumAll, RDenObs, RDenAll,
     2            wRNumObs,wRNumAll,wRDenObs,wRDenAll,
     3             RFacObs,RFacAll,wRFacObs,wRFacAll,
     4             RIFacObs,RIFacAll,wRIFacObs,wRIFacAll,
     5             RFacProf,wRFacProf,cRFacProf,wcRFacProf,
     6            eRFacProf,GOFProf,GOFObs,GOFAll,nRObs,nRAll,
     7            nRFacObs,nRFacAll,nRIFacObs,nRIFacAll,nRFacProf,
     8            NSkipRef,NBadOverRef,UseDatBlockActual)
      allocate(RNumObs(28), RNumAll(28), RDenObs(28), RDenAll(28),
     2        wRNumObs(28),wRNumAll(28),wRDenObs(28),wRDenAll(28),
     3         RFacObs(28,6,NPhase,NDatBlock),
     4         RFacAll(28,6,NPhase,NDatBlock),
     5        wRFacObs(28,6,NPhase,NDatBlock),
     6        wRFacAll(28,6,NPhase,NDatBlock),
     7        RIFacObs(6,NPhase,NDatBlock),
     8        RIFacAll(6,NPhase,NDatBlock),
     9       wRIFacObs(6,NPhase,NDatBlock),
     a       wRIFacAll(6,NPhase,NDatBlock),
     1        RFacProf(6,NDatBlock), wRFacProf(6,NDatBlock),
     2       cRFacProf(6,NDatBlock),wcRFacProf(6,NDatBlock),
     3       eRFacProf(NDatBlock),GOFProf(6,NDatBlock),
     4        GOFObs(6,NDatBlock),GOFAll(6,NDatBlock),
     5       nRObs(28),nRAll(28),
     6       nRFacObs(28,6,NPhase,NDatBlock),
     7       nRFacAll(28,6,NPhase,NDatBlock),
     8       nRIFacObs(6,NPhase,NDatBlock),
     9       nRIFacAll(6,NPhase,NDatBlock),nRFacProf(NDatBlock),
     a       NSkipRef(6,NDatBlock),NBadOverRef(6,NDatBlock),
     1       UseDatBlockActual(NDatBlock))
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(Allocated(NParStr))
     1  deallocate(NParStr,NParData,NParPwd,NParPwdAll,NParStrAll)
      allocate(NParStr(NPhase),NParData(NDatBlock),
     1         NParPwd(0:NPhase,0:NDatBlock),
     2         NParPwdAll(NDatBlock),NParStrAll(NDatBlock))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      if(allocated(CentrRef))
     1  deallocate(CentrRef,NSymmRef,NCSymmRef,ZCSymmRef)
      allocate(CentrRef(NPhase),NSymmRef(NPhase),NCSymmRef(NPhase),
     1         ZCSymmRef(NPhase))
      if(allocated(MagneticRFac)) deallocate(MagneticRFac)
      allocate(MagneticRFac(NPhase))
      call RefDefault
      n=0
      MaxNDimI=0
      NConstrain=0
      NRestrain=0
      RizikoX4=.true.
      call ComSym(0,0,ich)
      if(ich.ne.0) then
        ErrFlag=1
        go to 9999
      endif
      ExistMagnetic=.false.
      do 1230KPh=1,NPhase
        n=max(n,NSymm(KPh))
        MaxNDimI=max(MaxNDimI,NDimI(KPh))
        NCSymmRef(KPh)=1
        CentrRef(KPh)=Centr(KPh)
        if(MagneticType(KPhase).gt.0) then
          if(NDimI(KPh).gt.0) then
            MagneticRFac(KPh)=0
          else
            MagneticRFac(KPh)=1
          endif
        else
          MagneticRFac(KPh)=0
        endif
        if(CenterMag(KPh).gt.0) then
          NSymmRef(KPh)=NSymm(KPh)
          MagneticRFac(KPh)=1
          go to 1230
        else
          NSymmRef(KPh)=NSymmN(KPh)
        endif
        if(mod(NSymmN(KPh),2).ne.0) go to 1230
        do 1220i=1,NSymmN(KPh)/2
          do j=NSymmN(KPh)/2+1,NSymmN(KPh)
            if(EqRVM(RM6(1,i,1,KPh),RM6(1,j,1,KPh),NDimQ(KPh),.0001)
     1         .and.EqRV(s6(1,i,1,KPh),s6(1,j,1,KPh),NDim(KPh),.0001))
     2        go to 1220
          enddo
          go to 1230
1220    continue
        NSymmRef(KPh)=NSymmN(KPh)/2
        NCSymmRef(KPh)=2
        CentrRef(KPh)=Centr(KPh)*2.
        if(MagneticType(KPh).ne.0) then
          j=CrlCentroSymm()
          ZCSymmRef(KPh)=ZMag(j,1,KPhase)
          ExistMagnetic=.true.
        endif
1230  continue
      if(Allocated(LSMat))
     1  deallocate(LSMat,LSRS,LSSol,par,ader,bder,der,dc,dertw)
      if(Allocated(aderm))
     1  deallocate(aderm,bderm,derm,dcm,dertwm,derpol)
      if(allocated(isaRef)) deallocate(isaRef,ismRef,fyr)
      if(KCommenMax.gt.0.and.ngcMax.gt.0) then
        if(allocated(sngc)) deallocate(sngc,csgc)
        allocate(sngc(MaxUsedKwAll,ngcMax,NAtCalc),
     1           csgc(MaxUsedKwAll,ngcMax,NAtCalc))
        call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
      endif
      if(MaxNSymmL.gt.0) then
        if(allocated(snls)) deallocate(snls,csls)
        allocate(snls(MaxUsedKwAll,MaxNSymmL,NAtCalc),
     1           csls(MaxUsedKwAll,MaxNSymmL,NAtCalc))
      endif
      allocate(isaRef(MaxNSymm,NAtCalc),ismRef(NAtCalc),fyr(NAtCalc))
      allocate(AtDisable(NAtAll),AtDisableFact(NAtAll))
      AtDisableNOld=0
      AtDisableNNew=0
      call SetLogicalArrayTo(AtDisable,NAtAll,.false.)
      call SetRealArrayTo(AtDisableFact,NAtAll,1.)
      call RefSpecAt
      if(allocated(rh)) deallocate(rh,rk,rl,ath,atk,atl,t,imnp,HCoef,
     1                             trezm)
      allocate(rh(n,3),rk(n,3),rl(n,3),ath(n,3),atk(n,3),atl(n,3),t(n),
     1         imnp(n,3,3),HCoef(84,n,3),trezm(n,3))
      if(ISymmBasic.gt.0) call CrlRestoreSymmetry(ISymmBasic)
      call RefReadCommands
      if(ErrFlag.ne.0) go to 9999
      if(DoleBail.gt.0) then
        do i=1,NDatBlock
          if(iabs(DataType(i)).eq.2) go to 1240
        enddo
        DoleBail=0
      endif
1240  NUseDatBlockActual=0
      do i=1,NDatBlock
        UseDatBlockActual(i)=UseDatBlockInRefine(i)
        if(iabs(DataType(i)).eq.1.and.
     1     (DoLeBail.ne.0.or.(NAtCalc.le.0.and.ExistPowder)))
     2    UseDatBlockActual(i)=.false.
        if(UseDatBlockActual(i))NUseDatBlockActual=NUseDatBlockActual+1
      enddo
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      do KDatB=1,max(NDatBlock,1)
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      if(VasekTest.ne.0.and.reddyn.le.0) then
        NInfo=1
        TextInfo(1)='Podivnost: reddyn <=0'
        call FeInfoOut(-1.,-1.,'POZOR, POZOR !!!','L')
      endif
      if(ExistPowder.and.DoLeBail.eq.0.and.NAtCalc.ne.0) then
        do 1250KPh=1,NPhase
          do i=1,NAtCalc
            if(kswa(i).eq.KPh) go to 1250
          enddo
          call FeChybne(-1.,-1.,'the phase "'//
     1                  PhaseName(KPh)(:idel(PhaseName(KPh)))//
     2                 '" does not contain any atom.',
     3                 'You can make only a le Bail fit.',
     4                 SeriousError)
          ErrFlag=1
          go to 9999
1250    continue
      endif
      if(isimul.gt.0.or.StructureLocked) ncykl=0
      neqs=neq
      call RefInform
      if(iautk.eq.1) then
        call SetKeys
        if(ErrFlag.ne.0) go to 9999
      endif
      call RefKeepGRigidSetKi
      if(MaxNDimI.gt.0) then
        if(metoda.ne.0) then
          n=0
          do i=1,MaxNDimI
            n=max(n,ngrid(i))
          enddo
          if(Allocated(XGauss)) deallocate(XGauss,WGauss)
          allocate(XGauss(n,MaxNDimI,NPhase),
     1             WGauss(n,MaxNDimI,NPhase))
        else if(metoda.eq.0) then
        if(allocated(besp))
     1    deallocate(besp,dbfdu1,dbfdu2,dchidu1,dchidu2,dsdu1,dsdu2,chi,
     2               dadu1,dadu2,dbdu1,dbdu2,dsdchi,sb,dsbdax,dsbday,
     3               ksi,dksidax,dksiday,dadax,daday,dbdax,dbday,besb,
     4               dbfdb1,dbfdb2,detadb1,detadb2,dsdb1,dsdb2,eta,
     5               dadb1,dadb2,dbdb1,dbdb2,dsdeta,dsdb3,dsdu3,dbfdb3,
     6               dbfdu3,daxddlt,dayddlt,daxdx40,daydx40,daxdfct,
     7               daydfct,mx,mxs,mxkk,mb,mbs,mbk,mxdkw1,mxdkw2,
     8               mxdkw3,mbdkw1,mbdkw2,mbdkw3,accur)
          n=max(MaxUsedKwAll,1)
          allocate(besp(2*mxb+1,n),dbfdu1(2*mxb+1,n),
     1          dbfdu2(2*mxb+1,n),dchidu1(n),dchidu2(n),dsdu1(n),
     2          dsdu2(n),chi(n),dadu1(n),dadu2(n),dbdu1(n),dbdu2(n),
     3          dsdchi(n),sb(2*mxb+1),dsbdax(n),dsbday(n),ksi(n),
     4          dksidax(n),dksiday(n),dadax(n),daday(n),dbdax(n),
     5          dbday(n),besb(mxb+1,n),dbfdb1(mxb+1,n),dbfdb2(mxb+1,n),
     6          detadb1(n),detadb2(n),dsdb1(n),dsdb2(n),eta(n),dadb1(n),
     7          dadb2(n),dbdb1(n),dbdb2(n),dsdeta(n),dsdb3(n),dsdu3(n),
     8          dbfdb3(2*mxb+1,n),dbfdu3(2*mxb+1,n),daxddlt(n),
     9          dayddlt(n),daxdx40(n,n),daydx40(n,n),daxdfct(n,n),
     a          daydfct(n,n),mx(n),mxs(n),mxkk(n),mb(n),mbs(n),mbk(n),
     1          mxdkw1(n),mxdkw2(n),mxdkw3(n),mbdkw1(n),mbdkw2(n),
     2          mbdkw3(n),accur(mxb))
        endif
        if(OrthoOrd.gt.0) then
          n=max(MaxUsedKwAll,1)
          allocate(dori(2*n+1,28))
        endif
      endif
      if(Allocated(KWSym)) deallocate(KwSym)
      if(MaxNDimI.gt.0) then
        m=1
        do i=1,NAtAll
          if(TypeModFun(i).ne.1) cycle
          do n=2,itf(i)+1
            if(KFA(n,i).eq.0.or.KModA(n,i).eq.0.or.NDimI(KPhase).gt.1)
     1        then
              k=KModA(n,i)
            else
              k=KModA(n,i)-1
            endif
            m=max(m,(OrthoSel(2*k+1,i)+1)/2)
          enddo
        enddo
        n=max(m,MaxUsedKwAll)
        allocate(KWSym(n,MaxNSymm,MaxNComp,NPhase))
      endif
      do KDatB=1,max(NDatBlock,1)
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      allocate(F000R(NDatBlock),F000I(NDatBlock))
      if(isimul.gt.0) then
        ExistXRayData=Radiation(1).eq.XRayRadiation
        ExistNeutronData=Radiation(1).eq.NeutronRadiation
      endif
      do KPh=1,NPhase
        KPhase=KPh
        call inform50
        call newln(1)
        write(lst,FormA)
        poms=float(NSymmRef(KPh))*CentrRef(KPh)
        do iz=1,2
          F000 (KPh,iz)=0.
          F000N(KPh,iz)=0.
          F000R=0.
          F000I=0.
          nnn=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh) cycle
            j=isf(i)
            if(j.gt.NAtFormula(KPh)) then
              Veta='form factor for atom "'//Atom(i)(:idel(Atom(i)))//
     1            '" not defined.'
              call FeChybne(-1.,-1.,Veta,' ',SeriousError)
              ErrFlag=1
              go to 9999
            endif
            pom=ai(i)*CellVol(1,KPh)/CellVol(iswa(i),KPh)
            if(NDimI(KPh).gt.0) then
              pom=pom*a0(i)
              if(KFA(2,i).eq.1.and.KModA(2,i).ne.0)
     1          pom=pom*uy(2,KModA(2,i),i)
            endif
            if(iz.eq.1) then
              fpom=AtNum(j,KPh)
              fpomn=0.
            else
              fpom=ffn(j,KPh)
              if(ffn(j,KPh).lt.0.) then
                fpomn=-fpom
              else
                fpomn=0.
              endif
            endif
            F000 (KPh,iz)=F000 (KPh,iz)+fpom *pom
            F000N(KPh,iz)=F000N(KPh,iz)+fpomn*pom
            if(iz.eq.1) then
              do KDatB=1,NDatBlock
                if(Radiation(KDatB).ne.XRayRadiation) cycle
                fpomr=AtNum(j,KPh)+FFra(j,KPh,KDatB)
                fpomi=FFia(j,KPh,KDatB)
                F000R(KDatB)=F000R(KDatB)+fpomr*pom
                F000I(KDatB)=F000I(KDatB)+fpomi*pom
              enddo
            endif
          enddo
          F000 (KPh,iz)=F000 (KPh,iz)*poms
          F000N(KPh,iz)=F000N(KPh,iz)*poms
          n=1
          if(iz.eq.1) then
            if(ExistElectronData) then
              Veta=' for electrons :'
            else
              Veta=' for X-rays    :'
              n=0
              do KDatB=1,NDatBlock
                if(Radiation(KDatB).ne.XRayRadiation) cycle
                n=n+1
                F000R(KDatB)=F000R(KDatB)*poms
                F000I(KDatB)=F000I(KDatB)*poms
              enddo
            endif
          else
            Veta=' for neutrons  :'
          endif
          call newln(n)
          if(iz.eq.1) then
            do KDatB=1,KDatBlock
              if(Radiation(KDatB).eq.XRayRadiation) then
                write(lst,100) Veta(:idel(Veta)),
     1                F000(KPh,iz),F000R(KDatB),F000I(KDatB)
              else if(Radiation(KDatB).eq.ElectronRadiation) then
                write(lst,100) Veta(:idel(Veta)),F000(KPh,iz)
              endif
            enddo
          else
            write(lst,100) Veta(:idel(Veta)),F000(KPh,iz)
          endif
        enddo
        call newln(1)
        write(lst,FormA)
        if(NDimI(KPhase).gt.0) then
          call SetSymmWaves
          call SetSatGroups
          if(KCommen(KPh).gt.0) then
            metoda=1
            do i=1,NComp(KPh)
              scsup(i,KPh)=1./float(NCommQ(1,i,KPh)*NCommQ(2,i,KPh)*
     1                              NCommQ(3,i,KPh))
            enddo
          else
            call SetIntArrayTo(NSuper(1,1,KPh),3,1)
            call CopyVekI(ngrid,NSuper(1,1,KPh),NDimI(KPhase))
            metoda=min(metoda,1)
            if(metoda.eq.0) then
              do i=1,NAtCalc
                do j=4,7
                  if(KModA(j,i).gt.0) then
                    call FeChybne(-1.,-1.,'Bessel function method '//
     1                'doesn''t allow to modulate',
     2                'ADP anharmonic tensors, not implemented.',
     3                SeriousError)
                    ErrFlag=1
                    go to 9999
                  endif
                enddo
              enddo
              pom=1.
              do i=1,mxb
                pom=pom*float(i)*.125
                accur(i)=16.*(DifBess*pom)**(1./float(i))
              enddo
            else
              do i=1,NDimI(KPhase)
                call gauleg(0.,1.,XGauss(1,i,KPh),WGauss(1,i,KPh),
     1                      NSuper(i,1,KPh))
              enddo
              do j=2,NComp(KPh)
                call CopyVekI(NSuper(1,1,KPh),NSuper(1,j,KPh),3)
              enddo
              call SetRealArrayTo(scsup(1,KPh),NComp(KPh),1.)
              ngc(KPh)=0
            endif
          endif
          call newln(2)
          nw=0
          do i=1,mxw
            if(EqIV0(kw(1,i,KPh),NDimI(KPh))) then
              exit
            else
              nw=nw+1
            endif
          enddo
          write(lst,'(''The following harmonic waves can be used'',
     1                '' to describe modulation''/)')
          k=0
          ven=' '
          do i=1,nw
            kk=k
            write(ven(k+1:k+9),'(''wave#'',i2,'': '')') i
            if(i.lt.10) then
              ven(k+6:k+6)=ven(k+7:k+7)
              ven(k+7:k+7)=' '
            endif
            k=k+9
            prvni=.true.
            do j=1,NDimI(KPhase)
              if(kw(j,i,KPh).lt.0) then
                ven=ven(:k)//'-'
                k=k+1
              else if(kw(j,i,KPh).gt.0) then
                if(.not.prvni) ven=ven(:k)//'+'
                k=k+1
              else
                cycle
              endif
              kwij=iabs(kw(j,i,KPh))
              if(kwij.ne.1) then
                k=k+1
                if(kwij.le.9) then
                  write(ven(k:k),105) kwij
                else
                  write(ven(k:k+1),107) kwij
                  k=k+1
                endif
              endif
              write(ven(k+1:k+4),'(''q('',i1,'')'')') j
              k=k+4
              prvni=.false.
            enddo
            k=kk+27
            if(k.gt.100) then
              call newln(1)
              write(lst,FormA) ven
              ven=' '
              k=0
            endif
          enddo
          if(k.ne.0) then
            call newln(1)
            write(lst,FormA) ven
          endif
          do ia=1,NAtInd
            if(TypeModFun(ia).ne.1.or.kswa(ia).ne.KPh) cycle
            call newln(3)
            write(lst,FormA)
            write(ven,'(''Orthogonalized set functions based on x40='',
     1            f9.6,'' and delta='',f9.6)')
     2        OrthoX40(ia),OrthoDelta(ia)
            ven=ven(:idel(ven))//' for modulations of atom : '//Atom(ia)
            write(lst,FormA) ven(:idel(ven))
            do j=1,OrthoOrd
              ii=0
              ven='ortho#'
              if(j.lt.10) then
                write(ven(7:7),105) j-1
                ven(8:8)=' '
              else
                write(ven(7:8),107) j-1
              endif
              ven(9:9)='='
              kk=10
              do l=1,j
                i1=mod(OrthoSel(l,ia)-1,2)
                i2=(OrthoSel(l,ia)+1)/2
                pom=OrthoMat(j+OrthoOrd*(l-1),ia)
                write(ven(kk:kk+8),'(f8.3)') pom
                kk=kk+8
                if(l.gt.1) then
                  if(pom.ge.-0.0005) then
                    do m=kk-1,kk-8,-1
                      if(ven(m:m).eq.' ') go to 1510
                    enddo
1510                ven(m:m)='+'
                  endif
                  if(i1.eq.1) then
                    ven=ven(:kk-1)//'*cos('
                  else
                    ven=ven(:kk-1)//'*sin('
                  endif
                  kk=kk+5
                  write(ven(kk:kk+1),107) kw(1,i2,KPh)*2
                  kk=kk+2
                  ven=ven(:kk-1)//'*pi*x4)'
                  kk=kk+7
                endif
                if(kk.gt.120) then
                  call newln(1)
                  write(lst,FormA) ven
                  ven=' '
                  kk=18
                endif
              enddo
              if(kk.gt.18.or.j.eq.1) then
                if(j.ne.1) call newln(1)
                write(lst,FormA) ven(:idel(ven))
              endif
            enddo
          enddo
          iak=NAtPosFr(1,1)-1
          do im=1,NMolec
            if(kswmol(im).ne.KPh) cycle
            do ip=1,mam(im)
              ji=ip+(im-1)*mxp
              iap=iak+1
              iak=iak+iam(im)
              if(TypeModFunMol(ji).ne.1) cycle
              call newln(3)
              write(lst,FormA)
              write(ven,'(''Orthogonalized set functions based on x40='',
     1              f9.6,'' and delta='',f9.6)')
     2          OrthoX40Mol(ji),OrthoDeltaMol(ji)
                write(Cislo,FormI15) ji
                call Zhusti(Cislo)
                ven=ven(:idel(ven))//' for modulations of molecule : '//
     1              Molname(im)(:idel(Molname(im)))//'#'//
     2              Cislo(:idel(Cislo))
                write(lst,FormA) ven(:idel(ven))
                do j=1,OrthoOrd
                  ii=0
                  ven='ortho#'
                  if(j.lt.10) then
                    write(ven(7:7),105) j-1
                    ven(8:8)=' '
                  else
                    write(ven(7:8),107) j-1
                  endif
                  ven(9:9)='='
                  kk=10
                  do l=1,j
                  i1=mod(OrthoSel(l,iap)-1,2)
                  i2=(OrthoSel(l,iap)+1)/2
                  pom=OrthoMat(j+OrthoOrd*(l-1),iap)
                  write(ven(kk:kk+8),'(f8.3)') pom
                  kk=kk+8
                  if(l.gt.1) then
                    if(pom.ge.-0.0005) then
                      do m=kk-1,kk-8,-1
                        if(ven(m:m).eq.' ') go to 1520
                      enddo
1520                  ven(m:m)='+'
                    endif
                    if(i1.eq.1) then
                      ven=ven(:kk-1)//'*cos('
                    else
                      ven=ven(:kk-1)//'*sin('
                    endif
                    kk=kk+5
                    write(ven(kk:kk+1),107) kw(1,i2,KPh)*2
                    kk=kk+2
                    ven=ven(:kk-1)//'*pi*x4)'
                    kk=kk+7
                  endif
                  if(kk.gt.120) then
                    call newln(1)
                    write(lst,FormA) ven
                    ven=' '
                    kk=18
                  endif
                enddo
                if(kk.gt.18.or.j.eq.1) then
                  if(j.ne.1) call newln(1)
                  write(lst,FormA) ven(:idel(ven))
                endif
              enddo
            enddo
          enddo
        else
          NSatGroups=0
        endif
        if(NAtIndLenAll(KPhase).gt.0.or.NMolecLenAll(KPhase).gt.0.or.
     1     ExistPowder) then
          if(iauts.eq.1) then
            call SetSpec
            if(ErrFlag.ne.0) go to 9999
            if(ExistPowder) then
              call RefSetPwd
              if(ErrFlag.ne.0) go to 9999
            endif
          endif
          if(iautk.ne.0) then
            iap=NAtMolFrAll(KPh)
            do i=NMolecFrAll(KPh),NMolecToAll(KPh)
              ji=1+mxp*(i-1)
              nm=4
              m=1
              k=4
              jap=0
              do j=iap,iap+iam(i)/KPoint(i)-1
                if(ai(j).eq.0.) cycle
                is=0
                do l=1,3
                  if(KiA(m+l,j).ne.0) is=is+1
                enddo
                if(is.lt.nm) then
                  jap=j
                  nm=is
                endif
              enddo
              do l=1,3
                if(KiMol(k+l,ji).gt.0.or.KiMol(k+l,ji).eq.-2.and.
     1             jap.ne.0) KiA(m+l,jap)=0
                if(KiMol(k+l,ji).eq.-2) KiMol(k+l,ji)=0
              enddo
              iap=iap+iam(i)
            enddo
          endif
          if(iauts.eq.1) call ResetSetEq
          if(ExistPowder.and.NDim(KPhase).gt.3.and.DoLeBail.eq.0.and.
     1       (NAtIndLenAll(KPhase).gt.0.or.NMolecLenAll(KPhase).gt.0)
     2       .and.NComp(KPh).le.1) then
            if(MaxUsedKw(KPh).le.0) then
C   Bude slozitejsi
              l=IQuPwd+(KPh-1)*NParProfPwd
              call SetIntArrayTo(KiPwd(l),3*NDimI(KPhase),0)
            endif
          endif
        endif
        if(NDim(KPhase).gt.3) then
          call newln(2)
          if(KCommen(KPh).gt.0) then
            if(ICommen(KPh).eq.0) then
              write(lst,'(/''Commensurate modification in the '',
     1          ''supercell '',i2,''a '',i2,''b '',i2,
     2          ''c section t='',3f10.6)')
     3              (NCommen(i,1,KPh),i=1,3),(trez(i,1,KPh),
     4                                       i=1,NDimI(KPhase))
            else
              call newln(6)
              write(lst,'(/''Commensurate modification in supercell '',
     1                     ''defined by the matrix:'')')
              write(lst,FormA)
              do i=1,3
                k=3*(i-1)
                Veta='            '//SmbABC(i)//' ='
                call Velka(Veta)
                do j=1,3
                  k=k+1
                  pom=RCommen(k,1,KPh)
                  write(Cislo,'(f7.3,''*'',a1)') pom,SmbABC(j)
                  if(j.ne.1.and.pom.ge.0.) then
                    if(Cislo(2:2).eq.' ') then
                      m=2
                    else
                      m=1
                    endif
                    Cislo(m:m)='+'
                  endif
                  Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
                enddo
                write(lst,FormA) Veta(:idel(Veta))
              enddo
              write(lst,'(/''Section t='',3f10.6)')
     1          (trez(i,1,KPh),i=1,NDimI(KPhase))
            endif
            call ComSym(KPh,1,ich)
          else
            if(metoda.eq.0) then
              write(lst,'(/''Structure factors calculated by Bessel '',
     1                    ''function expansion; accuracy : '',f10.8)')
     2                    DifBess
            else if(metoda.eq.1) then
              write(lst,'(/''Structure factors calculated by Gaussian ''
     1                   ,''integration; number of grids : '',3i3)')
     2          (ngrid(i),i=1,NDimI(KPhase))
            endif
          endif
        endif
        do i=NMolecFrAll(KPh),NMolecToAll(KPh)
          if(kpoint(i).le.1) cycle
          call newln(4)
          k=idel(SmbPGMol(i))
          if(SmbPGMol(i)(k:k).eq.'*') k=k-1
          write(lst,FormA)
          ven='The molecule '//molname(i)(:idel(Molname(i)))//
     1        ' has the local point group '//SmbPGMol(i)(:k)
          write(lst,FormA) ven(:idel(ven))
          write(lst,FormA)
          write(lst,'(''The following additional operators will be '',
     1                ''applied on each atoms of the molecule :'')')
          k=1
          do j=1,npoint(i)
            if(ipoint(j,i).le.0) go to 1650
            do m=1,3
              if(k.eq.1) ivenr(m)=' '
              write(ivenr(m)(k:),'(''|'',3f8.5,''|'')')
     1             (rpoint(m+(n-1)*3,j,i),n=1,3)
            enddo
            k=k+28
1650        if(k.gt.99.or.(j.eq.npoint(i).and.k.ne.1)) then
              k=k-1
              call newln(4)
              write(lst,FormA)
              do m=1,3
                write(lst,FormA) ivenr(m)(:k-1)
              enddo
              k=1
            endif
          enddo
        enddo
      enddo
      if(isimul.gt.0) then
        ExistXRayData=.false.
        ExistNeutronData=.false.
      endif
      if(ExistPowder) then
        Format91(23:23)='3'
      else
        CorrESD=0
      endif
      if(nSingle.gt.0) then
        if(NTwin.gt.1) then
          call newln(2)
          write(lst,'(/''Crystal is supposed to be a twin composed '',
     1                ''from '',i1,'' twin fractions'')') NTwin
          call newln(3)
          write(lst,'(/''The twinning matrices are : ''/)')
          do k=2,NTwin
            call newln(4)
            do i=1,3
              write(lst,'(''|'',3f7.3,''|'')')(rtw(i+(j-1)*3,k),j=1,3)
            enddo
            write(lst,FormA)
          enddo
          if(HKLF5File) then
            call newln(1)
            write(lst,'(''The reflection file of type HKLF5 will be '',
     1                  ''used'')')
          else
            if(nchkr.le.0) then
              call newln(1)
              write(lst,'(''Reflections are supposed to be overlapped'',
     1                    '' if difference h1-h2 is smaller then '',
     2                    f8.2,'' deg.'')') TwDiff
              if(TwMax.lt.TwDiff) then
                TwMax=TwDiff
              else
                call newln(2)
                write(lst,'(''Reflections are supposed to be '',
     1                      ''separated if difference h1-h2 is larger'',
     2                      '' then '',f8.2,'' deg.'')') TwMax
                write(lst,'(''Note: All reflections which are neither'',
     1                      '' separated nor overlapped will be '',
     2                      ''discarded from the refinement.'')')
              endif
              if(NDim(KPhase).gt.3) then
                call newln(1)
                write(Cislo,'(''('',i3,'','',i3,'')'')') -mmaxtw,mmaxtw
                call zhusti(Cislo)
                write(lst,'(''Twin overlap is checked for reflections'',
     1                      '' with satellite indices from interval'',
     2                      a)') Cislo(:idel(Cislo))
              endif
            endif
          endif
        endif
        if(iover.gt.0.and.maxNDim.gt.3.and.KCommen(KPhase).le.0) then
          if(OverDif.le.0.) then
            if(maxNDim.eq.4) then
              pom=iover
              do i=1,3
                ihov(i)=nint(qu(i,1,1,KPhase)*pom)
              enddo
              ihov(4)=-iover
              write(ven,'(''('',3(i3,'',''),i3,'')'')')(ihov(i),i=1,4)
              call zhusti(ven)
              ven='Reflection (h,k,l,m) is supposed to be overlapped '//
     1            'with (h,k,l,m) + or - '//ven(:idel(ven))
              call newln(2)
              write(lst,FormA)
              write(lst,FormA) ven
            else
              call FeChybne(-1.,-1.,'this overlap option is applicable '
     1                    //'just 4d case',
     2                      'Try to use "overdif" command',
     3                      SeriousError)
              ErrFlag=1
              go to 9999
            endif
          else
            call newln(2)
            write(lst,'(''Reflections will be checked for possible '',
     1                  ''overlap within sphere delta(theta)<'',f5.3)')
     2        OverDif
            write(Cislo,'(''('',i3,'','',i3,'')'')') -iover,iover
            call zhusti(Cislo)
            write(lst,'(''Overlap is checked for reflections '',
     1                  ''with satellite indices from interval'',a)')
     2          Cislo(:idel(Cislo))
          endif
        else
          iover=0
        endif
      endif
      format83 (2:2)=format91(2:2)
      format83e(2:2)=format91(2:2)
      format83p(2:2)=format91(2:2)
      RefHeader='   h   k   l'
      if(maxNDim.gt.3) RefHeader=RefHeader(:idel(RefHeader))//'   m'
      if(maxNDim.gt.4) RefHeader=RefHeader(:idel(RefHeader))//'   n'
      if(maxNDim.gt.5) RefHeader=RefHeader(:idel(RefHeader))//'   p'
      RefHeader=RefHeader(:idel(RefHeader))
     1        //'     Fo        Fc        '
     2        //'A         B        Fo-Fc    sig(Fo) sqr(1/wt) sqr(wd'
      if(ifsq.eq.1) then
        Cislo='Iq)'
        IStRFac(1)=1
        IStRFac(2)=3
        IStRFac(3)=4
        IStRFac(4)=6
      else
        Cislo='Fq)'
        IStRFac(1)=1
        IStRFac(2)=2
        IStRFac(3)=4
        IStRFac(4)=5
      endif
      if(MagneticType(KPhase).ne.0) then
        RefHeader=RefHeader(:idel(RefHeader))//Cislo(:idel(Cislo))//
     1            '   nref    sinthl   ext   extm sc'
      else
        RefHeader=RefHeader(:idel(RefHeader))//Cislo(:idel(Cislo))//
     1            '   nref    sinthl   ext  sc'
      endif
      if(NTwin.gt.1) RefHeader=RefHeader(:idel(RefHeader))//' tw'
      if(ExistMagPol) then
        i=4*maxNDim
        RefHeaderMagPol=RefHeader(:i)
        if(ExistMagnetic) then
          RefHeaderMagPol=RefHeaderMagPol(:idel(RefHeaderMagPol))
     1        //'     Ro        Rc       Ro-Rc    '
     2        //'Inucl      Imag      Ipol    sqr(1/wt) sqr(wdRq)'
     3        //'  nref    sinthl   ext  extm  sc'
        else
          RefHeaderMagPol=RefHeaderMagPol(:idel(RefHeaderMagPol))
     1        //'     Ro        Rc       Ro-Rc    '
     2        //'Inucl      Imag      Ipol    sqr(1/wt) sqr(wdRq)'
     3        //'  nref    sinthl   ext  sc'
        endif
      endif
      write(format3,'(''('',i1)') maxNDim
      if(ExistMagnetic) then
        format3p=format3(:idel(format3))//
     1          'i4,8e15.6,i7,2a1,f9.6,2f6.3,3i3)'
        format3 =format3(:idel(format3))//
     1           'i4,8f10.4,i7,2a1,f9.6,2f6.3,2i3)'
      else
        format3p=format3(:idel(format3))//
     1          'i4,8e15.6,i7,2a1,f9.6,f6.3,3i3)'
        format3 =format3(:idel(format3))//
     1           'i4,8f10.4,i7,2a1,f9.6,f6.3,2i3)'
      endif
      do KDatB=1,NDatBlock
        if(ExtTensor(KDatB).eq.0) then
          call SetRealArrayTo(ec( 1,KDatB),12,0.)
          call SetRealArrayTo(ecs(1,KDatB),12,0.)
          call SetIntArrayTo(KiS(mxsc+7,KDatB),12,0)
        else if(ExtTensor(KDatB).eq.1) then
          do i=1,12
            if(ExtType(KDatB).ne.3) then
              if(i.ne.13-ExtType(KDatB)*6) ec(i,KDatB)=0.
            else
              if(i.ne.1.and.i.ne.7) ec(i,KDatB)=0.
            endif
          enddo
        else
          k=13-ExtType(KDatB)*6
          m=8-k
          call SetRealArrayTo(ec (m,KDatB),6,0.)
          call SetRealArrayTo(ecs(m,KDatB),6,0.)
          do i=k+1,k+5
            if(ec(i,KDatB).ne.0.) go to 2000
          enddo
          if(ec(k,KDatB).le.0.) then
            pom=0.01
          else
            pom=ec(k,KDatB)
            if(k.eq.1) pom=1./pom
          endif
          do i=k,k+2
            ec(i,KDatB)=pom**2
          enddo
        endif
        if(Lam2Corr.ne.1) then
          ScLam2(KDatB)=0.
          KiS(MxSc+4,KDatB)=0
        endif
      enddo
2000  do i=1,neqs
        m=lnp(i)
        if(m.eq.JeToDelta.or.m.eq.JeToX40.or.m.eq.JeToT40.or.
     1     m.eq.JeToXSlope.or.m.eq.JeToYSlope.or.m.eq.JeToZSlope)
     2    EqHar(i)=.true.
        if(EqHar(i)) call TrOrtho(0)
        if(m.gt.0.or.m.eq.JeToDelta.or.m.eq.JeToX40.or.m.eq.JeToT40.or.
     1     m.eq.JeToXSlope.or.m.eq.JeToYSlope.or.m.eq.JeToZSlope) then
          k=ktatmol(lat(i))
          mm=m
          ich=0
          if(k.gt.0) then
            isw=iswa(k)
            KPhase=kswa(k)
            call RefGetNParAt(m,k,.false.,PridatQR,ich)
            if(ich.ne.0) go to 3100
            call ChangeAtCompress(m,lnp(i),k,0,ich)
            if(ich.ne.0) go to 3100
            if(PridatQR) then
              lpa(i)='x40'
              npai=npa(i)
              do jj=1,3
                pom=qu(jj,1,isw,KPhase)
                if(pom.ne.0.) then
                  if(npai.ge.mxep) then
                    npa(i)=npai
                    call ReallocEquations(mxe,mxep+10)
                  endif
                  npai=npai+1
                  pat(npai,i)=lat(i)
                  pnp(npai,i)=jj+1
                  pko(npai,i)=pom
                  ppa(npai,i)=smbx(jj)
                endif
              enddo
              npa(i)=npai
            endif
            if(EqHar(i)) then
              call RefGetNParAt(mm,k,.true.,PridatQR,ich)
              if(ich.ne.0) go to 3100
              call ChangeAtCompress(mm,lnpo(i),k,0,ich)
              if(ich.ne.0) go to 3100
              KiA(lnpo(i),k)=0
            else
              KiA(lnp(i),k)=0
            endif
          else if(k.lt.0) then
            mm=m
            im=(-k-1)/mxp+1
            if(im.gt.NMolec) then
              ich=1
              go to 3100
            endif
            isw=iswmol(im)
            KPhase=kswmol(im)
            call RefGetNParMol(m,-k,.false.,PridatQR,ich)
            if(ich.ne.0) go to 3100
            call ChangeMolCompress(m,lnp(i),-k,0,ich)
            if(ich.ne.0) go to 3100
            if(PridatQR) then
              lpa(i)='x40'
              npai=npa(i)
              do jj=1,3
                pom=qu(jj,1,isw,KPhase)
                if(pom.ne.0.) then
                  if(npai.ge.mxep) then
                    npa(i)=npai
                    call ReallocEquations(mxe,mxep+10)
                  endif
                  npai=npai+1
                  pat(npai,i)=lat(i)
                  pnp(npai,i)=2*mxdmm+jj
                  pko(npai,i)=pom
                  ppa(npai,i)=smbx(jj)//'m'
                  if(npai.ge.mxep) then
                    npa(i)=npai
                    call ReallocEquations(mxe,mxep+10)
                  endif
                  npai=npai+1
                  pat(npai,i)=lat(i)
                  pnp(npai,i)=jj+4
                  pko(npai,i)=pom
                  ppa(npai,i)=smbx(jj)//'trans'
                endif
              enddo
              npa(i)=npai
            endif
            if(EqHar(i)) then
              call RefGetNParMol(mm,-k,.true.,PridatQR,ich)
              if(ich.ne.0) go to 3100
              call ChangeMolCompress(mm,lnpo(i),-k,0,ich)
              if(ich.ne.0) go to 3100
              KiMol(lnpo(i),-k)=0
            else
              KiMol(lnp(i),-k)=0
            endif
          endif
3100      if(ich.ne.0.or.k.eq.0) then
            lnp(i)=0
            go to 3490
          endif
        else if(m.eq.JeToXYZMode) then
          kip=KteraXYZMode(lpa(i),lat(i))
          lnp(i)=kip
          lnpo(i)=kip
          if(kip.gt.0) then
            go to 3150
          else
            lnp(i)=0
            go to 3490
          endif
        else if(m.eq.JeToMagMode) then
          kip=KteraMagMode(lpa(i),lat(i))
          lnp(i)=kip
          lnpo(i)=kip
          if(kip.gt.0) then
            go to 3150
          else
            lnp(i)=0
            go to 3490
          endif
        else
          lnp(i)=lnp(i)+100
          call RefGetScShift(iabs(lnp(i)),lat(i),k)
          if(k.ge.0) then
            lnp(i)=lnp(i)-k
            k=iabs(lnp(i))
            if(k.le.NdOffPwd) then
              j=mod(k-1,MxScAll)+1
              k=(k-1)/MxScAll+1
              KiS(j,k)=0
            else if(k.le.ndoffED) then
              k=k-NdOffPwd
              KiPwd(k)=0
            else if(NMaxEDZone.gt.0) then
              k=k-ndoffED
              l=mod(k-1,MxEDRef*NMaxEDZone)+1
              KDatB=(k-1)/(MxEDRef*NMaxEDZone)+1
              j=mod(l-1,MxEDRef)+1
              k=(l-1)/MxEDRef+1
              KiED(j,k,KDatB)=0
            endif
          else
            lnp(i)=-ndoff+k
          endif
        endif
3150    j=0
3420    j=j+1
        if(j.le.npa(i)) then
          m=pnp(j,i)
          if(m.gt.0.or.m.eq.JeToDelta.or.m.eq.JeToX40.or.m.eq.JeToT40
     1       .or.m.eq.JeToXSlope.or.m.eq.JeToYSlope.or.m.eq.JeToZSlope)
     2      then
            k=ktatmol(pat(j,i))
            if(k.gt.0) then
              call RefGetNParAt(m,k,.false.,PridatQR,ich)
              if(ich.ne.0) go to 3440
              call ChangeAtCompress(m,pnp(j,i),k,0,ich)
              if(ich.ne.0) go to 3440
              if(PridatQR) then
                ppa(j,i)='x40'
                npai=npa(i)
                do jj=1,3
                  pom=qu(jj,1,isw,KPhase)
                  if(pom.ne.0.) then
                    if(npai.ge.mxep) then
                      npa(i)=npai
                      call ReallocEquations(mxe,mxep+10)
                    endif
                    npai=npai+1
                    pat(npai,i)=pat(j,i)
                    pnp(npai,i)=jj+1
                    pko(npai,i)=-pko(j,i)*pom
                    ppa(npai,i)=smbx(jj)
                  endif
                enddo
                npa(i)=npai
              endif
            else if(k.lt.0) then
              im=(-k-1)/mxp+1
              isw=iswmol(im)
              if(m.gt.2*mxdmm) then
                pnp(j,i)=m
              else
                call RefGetNParMol(m,-k,.false.,PridatQR,ich)
                if(ich.ne.0) go to 3440
                call ChangeMolCompress(m,pnp(j,i),-k,0,ich)
                if(ich.ne.0) go to 3440
                if(PridatQR) then
                  ppa(j,i)='x40'
                  npai=npa(i)
                  do jj=1,3
                    pom=qu(jj,1,isw,KPhase)
                    if(pom.ne.0.) then
                      if(npai.ge.mxep) then
                        npa(i)=npai
                        call ReallocEquations(mxe,mxep+10)
                      endif
                      npai=npai+1
                      pat(npai,i)=pat(j,i)
                      pnp(npai,i)=2*mxdmm+jj
                      pko(npai,i)=-pko(j,i)*pom
                      ppa(npai,i)=smbx(jj)//'m'
                      if(npai.ge.mxep) then
                        npa(i)=npai
                        call ReallocEquations(mxe,mxep+10)
                      endif
                      npai=npai+1
                      pat(npai,i)=pat(j,i)
                      pnp(npai,i)=jj+4
                      pko(npai,i)=-pom
                      ppa(npai,i)=smbx(jj)//'trans'
                    endif
                  enddo
                  npa(i)=npai
                endif
              endif
            endif
3440        if(ich.ne.0.or.k.eq.0) then
              pnp(j,i)=0
              go to 3490
            endif
          else if(m.eq.JeToXYZMode) then
            kip=KteraXYZMode(ppa(j,i),pat(j,i))
            pnp(j,i)=kip
            if(kip.gt.0) then
              go to 3420
            else
              lnp(i)=0
              go to 3490
            endif
          else if(m.eq.JeToMagMode) then
            kip=KteraMagMode(ppa(j,i),pat(j,i))
            pnp(j,i)=kip
            if(kip.gt.0) then
              go to 3420
            else
              lnp(i)=0
              go to 3490
            endif
          else
            pnp(j,i)=pnp(j,i)+100
            call RefGetScShift(iabs(pnp(j,i)),pat(j,i),k)
            if(k.ge.0) then
              pnp(j,i)=pnp(j,i)-k
            else
              pnp(j,i)=-ndoff+k
            endif
          endif
          go to 3420
        endif
3490    if(EqHar(i)) call TrOrtho(1)
      enddo
      call TitulekVRamecku('User defined constrains/restrains')
      if(neqs.le.0.and.nvai.le.0.and.NRestDistAng.le.0) then
        call newln(1)
        write(lst,'(''None'')')
      endif
      if(NAtInd.gt.0.or.NMolec.gt.0.or.ExistPowder) then
        call SetEq(1,neqs)
        if(ErrFlag.ne.0) go to 9999
        if(nvai.gt.0) call SetRes(1)
        if(nKeep.gt.0) call RefSetKeep
        if(ErrFlag.ne.0) go to 9999
      endif
      if(ExistElectronData.and.ExistM42.and.CalcDyn) call SetElDyn(0,1)
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.s40')
      if(ExistPowder) then
        call CopyFile(fln(:ifln)//'.m41',fln(:ifln)//'.s41')
      else if(ExistElectronData.and.ExistM42) then
        call CopyFile(fln(:ifln)//'.m42',fln(:ifln)//'.s42')
        call iom42(1,0,fln(:ifln)//'.m42')
      endif
      if(MaxNDimI.gt.0) then
        call RefUpdateOrtho
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
         enddo
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
        if(allocated(ScRes)) then
          do j=1,NDatBlock
            if(Radiation(j).ne.ElectronRadiation.or.
     1         .not.ExistM42.or..not.CalcDyn) cycle
            do i=1,mxsc
              pom=Sc(i,j)
              Sc(i,j)=ScRes(i,j)
              ScRes(i,j)=pom
            enddo
          enddo
        endif
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(MaxNSymmMol.gt.MaxNSymm) then
        call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                   NPhase,0)
        MaxNSymm=MaxNSymmMol
      endif
      call RefFixOriginReset
      do KPh=1,NPhase
        KPhase=KPh
        call SetFixed
        if(ErrFlag.ne.0) go to 9999
        iap=NAtMolFrAll(KPh)
        do i=NMolecFrAll(KPh),NMolecToAll(KPh)
          if(iam(i).le.iam(i)/KPoint(i)) then
            k=0
            do j=iap,iap+iam(i)/KPoint(i)-1
              if(ai(j).eq.0..or..not.EqIV0(KiA(2,j),3)) cycle
              k=k+1
              if(k.ge.3) go to 3790
            enddo
            ji=1+mxp*(i-1)
            call SetIntArrayTo(KiMol(2,ji),3,0)
          endif
3790      iap=iap+iam(i)
        enddo
      enddo
      call ComSym(0,0,ich)
      if((NAtInd.gt.0.or.NMolec.gt.0).and.nKeep.gt.0) call RefSetKeep
      call iom40(1,0,fln(:ifln)//'.m40')
      call LoopDistAng(0)
      if(ErrFlag.ne.0) go to 9999
      if(NRestDistAng.ne.0) then
        mxkp=max(NKeepAtMax,8)
        allocate(AtName(mxkp))
        call newln(3)
        write(lst,'(/''The following restrains will be used :''/)')
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.l95','unformatted','old')
        if(ErrFlag.ne.0) go to 5200
        m=2
        FormPom1( 3: 3)='7'
        FormPom1( 5: 5)='4'
        FormPom1(21:21)='7'
        FormPom1(23:23)='4'
        FormPom2(16:16)='7'
        FormPom2(18:18)='4'
5100    read(ln,end=5120) Lbl,n,pom,spom,(AtName(i),i=1,n)
        if(n.eq.m.and.Lbl.eq.IdFixDistAngTors) then
          call newln(1)
          if(n.eq.2) then
            ivenr(1)='Distance'
          else if(n.eq.3) then
            ivenr(1)='Angle'
          else if(n.eq.4) then
            ivenr(1)='Torsion_angle'
          endif
          if(pom.gt.-900.) then
            write(Veta,FormPom1) pom,spom
          else
            write(Veta,FormPom2) spom
          endif
          call zhusti(ivenr(1))
          j=index(ivenr(1),'_')
          if(j.gt.0) ivenr(1)(j:j)=' '
          j=idel(ivenr(1))+1
          do i=1,n
            k=idel(AtName(i))
            ivenr(1)=ivenr(1)(:j)//AtName(i)(:k)
            j=j+k
            if(i.ne.n) then
              ivenr(1)=ivenr(1)(:j)//'-'
              j=j+1
            endif
          enddo
          if(pom.gt.-900.) then
            ivenr(1)=ivenr(1)(:j)//' restrained to '//Veta
          else
            ivenr(1)=ivenr(1)(:j)//' restrained to not be modulated'//
     1               Veta
          endif
          write(lst,FormA) ivenr(1)(:idel(ivenr(1)))
        endif
        go to 5100
5120    if(m.ge.4) then
          close(ln)
        else
          FormPom1( 3: 3)='8'
          FormPom1( 5: 5)='3'
          FormPom1(21:21)='6'
          FormPom1(23:23)='3'
          FormPom2(16:16)='6'
          FormPom2(18:18)='3'
          m=m+1
          rewind ln
          go to 5100
        endif
        if(allocated(AtName)) deallocate(AtName)
      endif
5200  call TitulekVRamecku('User defined keep commands')
      LinkaNavic=0
      if(NKeep.le.0) then
        call newln(1)
        write(lst,'(''None'')')
      else
        do i=IdKeepHydro,IdKeepADP
          if(i.eq.IdKeepHydro) then
            jp=IdKeepHTetraHed
            jk=IdKeepHApical
          else if(i.eq.IdKeepGeom) then
            jp=IdKeepGPlane
            jk=IdKeepGRigidMod
            Prvni=.true.
          else if(i.eq.IdKeepADP) then
            jp=IdKeepARiding
            jk=IdKeepARiding
          endif
          do j=jp,jk
            if(i.eq.IdKeepHydro) then
              ivenr(3)='The following hydrogens will be'
            else if(i.eq.IdKeepGeom) then
              ivenr(3)='The following geometry commands will be applied'
            endif
            if(j.eq.IdKeepHTetraHed) then
              ivenr(3)=ivenr(3)(:idel(ivenr(3))+1)//
     1                'kept to make a regular tetrahedron with the '//
     2                'central and neighbor atoms'
              NAtMax=4
            else if(j.eq.IdKeepHTriangl) then
              ivenr(3)=ivenr(3)(:idel(ivenr(3))+1)//
     1                'kept to make a regular triangle with the central'
     2              //' and neighbor atoms'
              NAtMax=3
            else if(j.eq.IdKeepHApical) then
              ivenr(3)=ivenr(3)(:idel(ivenr(3))+1)//
     1                'kept to be in an apical position with respect '//
     2                'to the central and neighbor atoms'
            else if(j.eq.IdKeepARiding) then
              ivenr(3)='ADP parameter of hydrogen atoms'
            endif
            if(i.ne.IdKeepGeom) Prvni=.true.
            do k=1,nKeep
              if(KeepType(k).ne.j) cycle
              if(i.eq.IdKeepHydro) then
                if(KeepType(k).eq.IdKeepHApical) NAtMax=KeepNNeigh(k)+1
                ivenr(1)='The central atom '//
     1                  KeepAtCentr(k)(:idel(KeepAtCentr(k)))//
     2                  ',neighbor atom'
                n=NAtMax-KeepNH(k)
                if(n.gt.1) ivenr(1)=ivenr(1)(:idel(ivenr(1)))//'s'
                idv=idel(ivenr(1))
                do l=1,n
                  ida=idel(KeepAtNeigh(k,l))
                  if(idv+ida.gt.len(ivenr(1))) cycle
                  ivenr(1)=ivenr(1)(:idv+1)//KeepAtNeigh(k,l)(:ida)
                  idv=idv+ida+1
                enddo
                ivenr(1)=ivenr(1)(:idel(ivenr(1)))//' and hydrogen'
                idv=idel(ivenr(1))
                if(KeepNH(k).gt.1) then
                  ivenr(1)=ivenr(1)(:idel(ivenr(1)))//'s'
                  idv=idv+1
                endif
                do l=1,KeepNH(k)
                  ida=idel(KeepAtH(k,l))
                  if(idv+ida.gt.len(ivenr(1))) cycle
                  ivenr(1)=ivenr(1)(:idv+1)//KeepAtH(k,l)(:ida)
                  idv=idv+ida+1
                enddo
              else if(i.eq.IdKeepGeom) then
                if(j.eq.IdKeepGPlane) then
                  ivenr(1)='Plane:'
                else if(j.eq.IdKeepGRigid) then
                  ivenr(1)='Rigid:'
                else
                  ivenr(1)='All distances constant in modulation:'
                endif
                idlk=idel(ivenr(1))
                idlp=idlk
                do l=1,KeepN(k)
                  m=idel(KeepAt(k,l))
                  if(m+idlk+1.gt.len(ivenr(1))) then
                    if(Prvni) then
                      call NewLn(2+LinkaNavic)
                      if(LinkaNavic.ne.0) write(lst,FormA)
                      write(lst,FormA) ivenr(3)(:idel(ivenr(3)))
                      write(lst,FormA1)('=',n=1,idel(ivenr(3)))
                    endif
                    Prvni=.false.
                    LinkaNavic=1
                    write(lst,FormA) ivenr(1)(:idel(ivenr(1)))
                    ivenr(1)=' '
                    idlk=idlp
                  endif
                  ivenr(1)=ivenr(1)(:idlk+1)//KeepAt(k,l)(:m)
                  idlk=idlk+m+1
                enddo
              else if(j.eq.IdKeepARiding) then
                ivenr(1)='ADP parameter of hydrogen'
                if(KeepNH(k).gt.1)
     1            ivenr(1)=ivenr(1)(:idel(ivenr(1)))//'s'
                if(itf(KeepNAtCentr(k)).eq.1) then
                  pom=KeepADPExtFac(k)
                else
                  pom=-1.
                endif
                do l=1,KeepNH(k)
                  ivenr(1)=ivenr(1)(:idel(ivenr(1))+1)//
     1                    KeepAtH(k,l)(:idel(KeepAtH(k,l)))
                  AtDisableFact(KeepNAtH(k,l))=pom
                enddo
                write(Cislo,'(f8.3)') KeepADPExtFac(k)
                call ZdrcniCisla(Cislo,1)
                ivenr(1)=ivenr(1)(:idel(ivenr(1)))//
     1                  ' derived from that of the central atom '//
     2                  KeepAtCentr(k)(:idel(KeepAtCentr(k)))//
     3                  ' by applying of riding factor '//
     4                  Cislo(:idel(Cislo))
              endif
              if(Prvni) then
                call NewLn(2+LinkaNavic)
                if(LinkaNavic.ne.0) write(lst,FormA)
                write(lst,FormA) ivenr(3)(:idel(ivenr(3)))
                write(lst,FormA1)('=',l=1,idel(ivenr(3)))
                LinkaNavic=1
              endif
              if(i.eq.IdKeepHydro) then
                mk=2
                write(Cislo,'(f8.3)') KeepDistH(k)
                call ZdrcniCisla(Cislo,1)
                ivenr(2)='Distance to central atom set to '//
     1                  Cislo(:idel(Cislo))//' Angstroems'
                if(n.lt.KeepNNeigh(k)) then
                  write(Cislo,'(f8.3)') KeepAngleH(k)
                  call ZdrcniCisla(Cislo,1)
                  ivenr(2)=ivenr(2)(:idel(ivenr(2)))//'; the atom '//
     1                    KeepAtAnchor(k)(:idel(KeepAtAnchor(k)))//
     2                    ' used as an anchor with the torsion angle '//
     3                    Cislo(:idel(Cislo))//' Degs'
                endif
              else
                mk=1
              endif
              call NewLn(mk)
              do m=1,mk
                write(lst,FormA) ivenr(m)(:idel(ivenr(m)))
              enddo
              Prvni=.false.
            enddo
          enddo
        enddo
      endif
      call RefScreenDatBlocks
      call RefSetMatrixLimits(1)
      if(ncykl.gt.0) then
        if(nLSRS.le.0) then
          ncykl=0
          Veta='nothing to refine, the number of cycles reduced to 0'
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        endif
      endif
      do KDatB=1,max(NDatBlock,1)
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      if(Klic.eq.1) go to 9999
      if(IsPowder) then
        if(isimul.gt.0) then
          if(BatchMode.or.Console) then
            if(ICDDBatch) then
              call ICDDGetGenerIKeys(TwoThetaLimits,
     1          SatFrMod(KPhase,KDatBlock),MMaxPwd(1,KPhase,KDatBlock))
            else
              TwoThetaLimits(1)=3.
              TwoThetaLimits(2)=177.
              TwoThetaLimits(3)=.01
              SatFrMod(KPhase,KDatBlock)=.true.
5500          read(BatchLN,FormA,end=5620,err=5630) Veta
              if(Veta(1:1).eq.'.') then
                backspace BatchLN
                go to 6000
              else if(Veta(1:1).eq.'#') then
                go to 5500
              endif
              k=0
5600          if(k.ge.len(Veta)) go to 5500
              call kus(Veta,k,Nazev)
              j=islovo(Nazev,IdGener,7)
              call kus(Veta,k,Cislo)
              call Posun(Cislo,1)
              read(Cislo,'(f15.5)',err=5620) Value
              IValue=nint(Value)
              if(j.eq.1) then
                pom=.5*LamAve(KDatBlock)/Value
                if(pom.gt.1.) then
                  TwoThetaLimits(2)=177.
                else
                  TwoThetaLimits(2)=2.*asin(pom)/ToRad
                endif
              else if(j.eq.2) then
                pom=.5*LamAve(KDatBlock)/Value
                if(pom.gt.1.) then
                  TwoThetaLimits(1)=3.
                else
                  TwoThetaLimits(1)=2.*asin(pom)/ToRad
                endif
              else if(j.eq.3) then
                TwoThetaLimits(1)=Value
              else if(j.eq.4) then
                TwoThetaLimits(2)=Value
              else if(j.eq.5) then
                TwoThetaLimits(3)=Value
              else if(j.eq.6) then
                MMaxPwd(1,KPhase,KDatBlock)=IValue
                do i=2,NDimI(KPhase)
                  call kus(Veta,k,Cislo)
                  call Posun(Cislo,o)
                  read(Cislo,FormI15,err=5620)
     1              MMaxPwd(i,KPhase,KDatBlock)
                enddo
              else if(j.eq.7) then
                SatFrMod(KPhase,KDatBlock)=IValue.eq.1
              endif
              pom=sin(TwoThetaLimits(2)*torad*.5)/LamAve(KDatBlock)
              if(pom.gt.1.) then
                TwoThetaLimits(2)=2.*asin(LamAve(KDatBlock))/torad
                pom=sin(TwoThetaLimits(1)*torad*.5)/LamAve(KDatBlock)
                if(pom.gt..05) TwoThetaLimits(1)=0.
5610            if((TwoThetaLimits(2)-TwoThetaLimits(1))/
     1              TwoThetaLimits(3).lt.1000.) then
                  TwoThetaLimits(3)=TwoThetaLimits(3)/10.
                  go to 5610
                endif
              endif
              go to 5600
5620          p80='end of file read from the batch file.'
              Veta=' '
              go to 5650
5630          p80='reading error occured on the batch file.'
5650          call FeChybne(-1.,-1.,p80,Veta,SeriousError)
              ErrFlag=1
              go to 9999
            endif
          else
            pom=sin(TwoThetaLimits(2)*torad*.5)/LamAve(KDatBlock)
            if(pom.gt.1.) then
              TwoThetaLimits(2)=2.*asin(LamAve(KDatBlock))/torad
              pom=sin(TwoThetaLimits(1)*torad*.5)/LamAve(KDatBlock)
              if(pom.gt..05) TwoThetaLimits(1)=0.
5660          if((TwoThetaLimits(2)-TwoThetaLimits(1))/
     1            TwoThetaLimits(3).lt.1000.) then
                TwoThetaLimits(3)=TwoThetaLimits(3)/10.
                go to 5660
              endif
            endif
            id=NextQuestId()
            p80='Define simulation parameters:'
            if(ExistM90) then
              call PwdM92Nacti
              TwoThetaLimits(1)=XPwd(1)
              TwoThetaLimits(2)=XPwd(NPnts)
              TwoThetaLimits(3)=DegStepPwd
            endif
            xqd=500.
            il=7
            if(NDimI(KPhase).gt.0) il=il+NDimI(KPhase)+4
            call FeQuestCreate(id,-1.,-1.,xqd,il,p80,1,LightGray,0,0)
            il=0
            xpom=5.
            tpom=xpom+CrwgXd+10.
            do i=1,3
              il=il+1
              if(i.eq.1) then
                Veta='%2theta'
              else if(i.eq.2) then
                Veta='%d'
              else
                Veta='%sin(theta)/lambda'
              endif
              call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                            CrwgYd,1,1)
              if(i.eq.1) then
                nCrwTheta=CrwLastMade
              else if(i.eq.2) then
                nCrwD=CrwLastMade
              else
                nCrwSinthl=CrwLastMade
              endif
              call FeQuestCrwOpen(CrwLastMade,i.eq.LimType)
            enddo
            tpom=tpom+FeTxLength(Veta)+15.
            tpoms=tpom
            il=0
            Veta='From'
            xpom=tpom+FeTxLength(Veta)+40.
            dpom=100.
            do i=1,3
              il=il+1
              if(i.eq.2) then
                Veta='To'
              else if(i.eq.3) then
                Veta='2theta-step'
              endif
              call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,0)
              if(i.eq.1) nEdwLim=EdwLastMade
            enddo
            nEdw2ThStep=EdwLastMade
            call FeQuestRealEdwOpen(EdwLastMade,TwoThetaLimits(3),
     1                              .false.,.false.)
            DegStepPwd=TwoThetaLimits(3)
            RadStepPwd=DegStepPwd*ToRad
            il=il+1
            call FeQuestLinkaMake(id,il)
            xpom=5.
            tpom=xpom+CrwgXd+10.
            Veta='%calculate profile from the model'
            if(NAtCalc.le.0) ksimul=2
            do i=1,2
              il=il+1
              call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',
     1                            CrwXd,CrwYd,1,2)
              call FeQuestCrwOpen(CrwLastMade,i.eq.ksimul)
              if(i.eq.1) then
                if(NAtCalc.le.0) call FeQuestCrwDisable(CrwLastMade)
                nCrwFCalc=CrwLastMade
                Veta='use I(%obs) values as measured from single '//
     1               'crystal'
              else
                nCrwFObs=CrwLastMade
              endif
            enddo
            il=il+1
            Veta='%Reflection file:'
            xpom=tpom+FeTxLengthUnder(Veta)+10.
            dpom=300.
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,0)
            call FeQuestStringEdwOpen(EdwLastMade,FileSimul)
            nEdwRefFile=EdwLastMade
            Veta='%Browse'
            xpom=xpom+dpom+10.
            dpom=xqd-xpom-10.
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
            nButtBrowse=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            if(NDimI(KPhase).gt.0) then
              il=il+1
              call FeQuestLinkaMake(id,il)
              il=il+1
              Veta='Use only satellites corresponding to'
              xpom=5.
              tpom=xpom+CrwXd+10.
              ilp=-10*il-5
              call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',
     1                            CrwXd,CrwYd,1,0)
              nCrwSatFrMod=CrwLastMade
              call FeQuestCrwOpen(CrwLastMade,
     1                            SatFrMod(KPhase,KDatBlock))
              il=il+1
              Veta='existing modulation waves'
              call FeQuestLblMake(id,tpom,il,Veta,'L','N')
              il=il+1
              Veta='%m(max)'
              cpom=FeTxLengthUnder(Veta)+10.
              tpom=5.
              dpom=40.
              xpom=tpom+cpom
              do i=1,NDimI(KPhase)
                if(i.eq.2) then
                  Veta(2:2)='p'
                else if(i.eq.3) then
                  Veta(2:2)='n'
                endif
                call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                if(i.eq.1) nEdwMMax=EdwLastMade
                tpom=xpom+dpom+20.
                xpom=tpom+cpom
                if(.not.SatFrMod(KPhase,KDatBlock))
     1            call FeQuestIntEdwOpen(EdwLastMade,
     2              MMaxPwd(i,KPhase,KDatBlock),.false.)
              enddo
            else
              nCrwSatFrMod=0
              nEdwMMax=0
            endif
5700        nEdw=nEdwLim
            do i=1,2
              if(LimType.eq.2) then
                j=3-i
              else
                j=i
              endif
              pom=TwoThetaLimits(j)
              if(LimType.eq.2) then
                if(pom.le.0.) then
                  pom=1000.
                else
                  pom=.5*LamAve(KDatBlock)/sin(pom*torad*.5)
                endif
              else if(LimType.eq.3) then
                pom=sin(pom*torad*.5)/LamAve(KDatBlock)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              nEdw=nEdw+1
            enddo
            LimTypeOld=LimType
5750        if(CrwLogicQuest(nCrwFCalc)) then
              FileSimul=EdwStringQuest(nEdwRefFile)
              call FeQuestEdwDisable(nEdwRefFile)
              call FeQuestButtonDisable(nButtBrowse)
            else
              call FeQuestStringEdwOpen(nEdwRefFile,FileSimul)
              call FeQuestButtonOff(nButtBrowse)
            endif
5800        call FeQuestEvent(id,ich)
            nEdw=nEdwLim
            do i=1,2
              j=i
              call FeQuestRealFromEdw(nEdw,pom)
              if(LimTypeOld.eq.2) then
                j=3-i
                if(pom.gt.0.) then
                  if(pom.ge.1000.) then
                    pom=0.
                  else
                    pom=.5*LamAve(KDatBlock)/pom
                  endif
                else
                  pom=0.
                endif
              else if(LimTypeOld.eq.3) then
                pom=LamAve(KDatBlock)*pom
              endif
              if(LimTypeOld.ne.1) then
                if(pom.ge.1.) then
                  pom=177.
                else
                  pom=2.*asin(pom)/torad
                endif
              endif
              TwoThetaLimits(j)=pom
              nEDw=nEdw+1
            enddo
            if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK)
     1        then
              if(CrwLogicQuest(nCrwFObs)) then
                FileSimul=EdwStringQuest(nEdwRefFile)
                if(.not.ExistFile(FileSimul)) then
                  Veta='file "'//FileSimul(:idel(FileSimul))//
     1                 '" doesn''t exist, try again.'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                  go to 5800
                else
                  ln=NextLogicNumber()
                  call OpenDatBlockM90(ln,1,FileSimul)
                  call NastavM90(ln)
5810              read(ln,format91,end=5820,err=5820)
     1              (ihp(i),i=1,maxNDim),pom,pom,iq,i,i
                  if(iq.ne.1) go to 5810
                  call CloseIfOpened(ln)
                  go to 5850
5820              call CloseIfOpened(ln)
                  Veta='file "'//FileSimul(:idel(FileSimul))//
     1                 '" gives reading error, try again.'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                   go to 5800
                endif
              endif
5850          QuestCheck(id)=0
              go to 5800
            else if(CheckType.eq.EventCrw.and.
     1              CheckNumber.ge.nCrwTheta.and.
     2              CheckNumber.le.nCrwSinthl) then
              LimType=CheckNumber-nCrwTheta+1
              go to 5700
            else if(CheckType.eq.EventCrw.and.
     1              CheckNumber.eq.nCrwSatFrMod) then
              SatFrMod(KPhase,KDatBlock)=CrwLogicQuest(CheckNumber)
              nEdw=nEdwMMax
              do i=1,NDimI(KPhase)
                if(SatFrMod(KPhase,KDatBlock)) then
                  call FeQuestEdwClose(nEdw)
                else
                  call FeQuestIntEdwOpen(nEdw,
     1              MMaxPwd(i,KPhase,KDatBlock),.false.)
                endif
                nEdw=nEdw+1
              enddo
              go to 5800
            else if(CheckType.eq.EventCrw.and.
     1              (CheckNumber.eq.nCrwFCalc.or.
     2               CheckNumber.eq.nCrwFObs)) then
              go to 5750
            else if(CheckType.eq.EventButton.and.
     1              CheckNumber.eq.nButtBrowse) then
              call FeFileManager('Select reflection file for '//
     1                           'simulation',FileSimul,'*.m90',0,
     2                           .true.,ichp)
              if(ichp.eq.0)
     1          call FeQuestStringEdwOpen(nEdwRefFile,FileSimul)
              go to 5800
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 5800
            endif
            if(ich.eq.0) then
              call FeQuestRealFromEdw(nEdw2ThStep,TwoThetaLimits(3))
              if(nEdwMMax.ne.0) then
                if(.not.SatFrMod(KPhase,KDatBlock)) then
                  nEdw=nEdwMMax
                  do i=1,NDimI(KPhase)
                    call FeQuestIntFromEdw(nEdw,
     1                MMaxPwd(i,KPhase,KDatBlock))
                    nEdw=nEdw+1
                  enddo
                endif
              endif
              if(CrwLogicQuest(nCrwFCalc)) then
                ksimul=1
              else
                ksimul=2
                FileSimul=EdwStringQuest(nEdwRefFile)
              endif
              call iom41(1,0,fln(:ifln)//'.m41')
              do KDatB=1,max(NDatBlock,1)
                if(KAnRef(KDatB).ne.0) then
                  do KPh=1,NPhase
                    call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                           NAtFormula(KPh))
                    call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                           NAtFormula(KPh))
                  enddo
                endif
              enddo
            endif
            call FeQuestRemove(id)
            if(ich.ne.0) then
              ErrFlag=1
              go to 9999
            endif
          endif
6000      NPnts=nint((TwoThetaLimits(2)-TwoThetaLimits(1))/
     1                TwoThetaLimits(3))
          if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YcPwd,YbPwd,
     1                                    YsPwd,YfPwd,YiPwd,YcPwdInd)
          allocate(XPwd(NPnts),YoPwd(NPnts),YcPwd(NPnts),YbPwd(NPnts),
     1             YsPwd(NPnts),YfPwd(NPnts),YiPwd(NPnts),
     2             YcPwdInd(NPnts,NPhase))
          do i=1,NPnts
            YoPwd(i)=2.
            YsPwd(i)=2.
            XPwd(i)=TwoThetaLimits(1)+(i-1)*TwoThetaLimits(3)
            YfPwd(i)=1
          enddo
          ncykl=0
        else
          do KDatB=1,NDatBlock
            call PwdSetTOF(KDatB)
            if(isTOF.or.isED.or.
     1         KAsym(KDatB).ne.IdPwdAsymFundamental) cycle
            IZdvihRec=(KDatB-1)*NParRecPwd
            if(iabs(DataType(KDatB)).eq.2) then
              j=0
              do i=IAsymPwd+IZdvihRec,IAsymPwd+IZdvihRec+6
                j=j+1
                AsymKi(j,KDatB)=KiPwd(i)
              enddo
              kip=IAsymPwd+IZdvihRec+ndoffPwd
              do i=1,neq
                j=lnp(i)-kip+1
                if(j.ge.1.and.j.le.7) then
                  AsymKi(j,KDatB)=1
                endif
              enddo
            endif
          enddo
        endif
      endif
      Maxmmabsm=0
      if(RefRandomize) then
        if(RefRandomSeed.gt.0) then
          Seed(1)=RefRandomSeed
        else
          Seed(1)=31459
        endif
        call Random_Seed(Seed(1))
        if(NAtXYZMode.le.0) then
          do i=1,NAtAll
            kip=PrvniKiAtomu(i)
            isw=iswa(i)
            ksw=kswa(i)
            do j=1,3
              kip=kip+1
              if(ki(kip).ne.1) cycle
              call Random_Number(pom)
              pom=(2.*pom-1.)/CellPar(j,isw,ksw)*RefRandomDiff
              x(j,i)=x(j,i)+pom
            enddo
          enddo
        else
          if(.not.isPowder.or.DoLeBail.eq.0) then
            if(NAtXYZMode.gt.0) call TrXYZMode(1)
            if(NAtMagMode.gt.0) call TrMagMode(1)
          endif
          do i=1,NAtXYZMode
            do j=1,MAtXYZMode(i)
              if(KiAtXYZMode(j,i).ne.1) cycle
              call Random_Number(pom)
              AtXYZMode(j,i)=AtXYZMode(j,i)+(2.*pom-1.)*RefRandomDiff
            enddo
          enddo
        endif
        call RefAppEq(0,0,0)
        if(nvai.gt.0) call SetRes(0)
        call RefKeepGRigidUpdate
        if(nKeep.gt.0) call RefSetKeep
        if(.not.isPowder.or.DoLeBail.eq.0) then
          if(NAtXYZMode.gt.0) call TrXYZMode(0)
          if(NAtMagMode.gt.0) call TrMagMode(0)
        endif
!        call RefKeepGRigidUpdate
        call RefFixOriginEpilog
        call RefFixOriginReset
        call RefFixOriginPrologCorrect(1)
      endif
9999  if(ExistElectronData.and.NMaxEDZone.gt.1)
     1   allocate( RNumZoneObs(NMaxEDZone,NDatBlock),
     2             RNumZoneAll(NMaxEDZone,NDatBlock),
     3             RDenZoneObs(NMaxEDZone,NDatBlock),
     4             RDenZoneAll(NMaxEDZone,NDatBlock),
     5            wRNumZoneObs(NMaxEDZone,NDatBlock),
     6            wRNumZoneAll(NMaxEDZone,NDatBlock),
     7            wRDenZoneObs(NMaxEDZone,NDatBlock),
     8            wRDenZoneAll(NMaxEDZone,NDatBlock),
     9               nRZoneAll(NMaxEDZone,NDatBlock),
     a               nRZoneObs(NMaxEDZone,NDatBlock))
      if(allocated(F000R)) deallocate(F000R,F000I)
      return
100   format('F(000) ',a,1x,3f12.3)
103   format(8f9.4)
105   format(i1)
107   format(i2)
      end
