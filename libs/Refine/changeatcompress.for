      subroutine ChangeAtCompress(PosIn,PosOut,N,Key,ich)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PosIn,PosOut,CumulCompress(11)
      ich=0
      if(itf(N).gt.0) then
        CumulCompress(1)=max(TRankCumul(itf(N)),10)
      else
        CumulCompress(1)=10
      endif
      if(ifr(N).gt.0) CumulCompress(1)=CumulCompress(1)+1
      if(lasmax(N).gt.0) then
        CumulCompress(2)=CumulCompress(1)+3
        if(lasmax(N).gt.1)
     1  CumulCompress(2)=CumulCompress(2)+1+(lasmax(N)-1)**2
      else
        CumulCompress(2)=CumulCompress(1)
      endif
      do i=3,9
        k=max(KModA(i-2,N),0)
        CumulCompress(i)=CumulCompress(i-1)+2*TRank(i-3)*k
        if(i.eq.3.and.k.gt.0) CumulCompress(i)=
     1                                   CumulCompress(i)+1
      enddo
      if(CumulCompress(9).gt.CumulCompress(1)) then
        CumulCompress(10)=CumulCompress(9)+1
      else
        CumulCompress(10)=CumulCompress(9)
      endif
      if(MagPar(N).gt.0) then
        CumulCompress(11)=CumulCompress(10)+3+6*(MagPar(N)-1)
      else
        CumulCompress(11)=CumulCompress(10)
      endif
      if(Key.eq.0) then
        do i=1,11
          if(PosIn.le.CumulAt(i)) then
            if(i.eq.1) then
              if(PosIn.lt.CumulAt(1)) then
                PosOut=PosIn
              else
                PosOut=CumulCompress(1)
              endif
              if(itf(n).eq.1.and.PosOut.gt.5) then
                go to 9000
              else
                go to 9999
              endif
            else
              go to 1500
            endif
          endif
        enddo
        go to 9000
1500    PosOut=CumulCompress(i-1)+PosIn-CumulAt(i-1)
        if(PosOut.gt.CumulCompress(i)) go to 9000
      else
        do i=1,11
          if(PosIn.le.CumulCompress(i)) then
            if(i.eq.1) then
              if(PosIn.lt.CumulCompress(i).or.ifr(N).eq.0) then
                PosOut=PosIn
              else
                PosOut=CumulAt(1)
              endif
              go to 9999
            else
              go to 2500
            endif
          endif
        enddo
        go to 9000
2500    PosOut=CumulAt(i-1)+PosIn-CumulCompress(i-1)
      endif
      go to 9999
9000  ich=1
9999  return
      end
