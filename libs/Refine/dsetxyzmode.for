      subroutine DSetXYZMode
      use Atoms_mod
      use Refine_mod
      kip=PrvniKiAtXYZMode
      do i=1,NAtXYZMode
        do j=1,NMAtXYZMode(i)
          der(kip)=0.
          kt=KAtXYZMode(j,i)
          pom=FAtXYZMode(j,i)
          m=1
          do k=1,MAtXYZMode(i)
            ia=IAtXYZMode(k,i)
            kiia=PrvniKiAtomu(ia)
            do l=1,3
              der(kip)=der(kip)+pom*XYZAMode(m,kt)*der(kiia+l)
              m=m+1
            enddo
          enddo
          kip=kip+1
        enddo
      enddo
      return
      end
