      subroutine SetRes(tisk)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      integer tisk,AtMolFlag
      dimension xp(6),xpp(6),Phi(3),px(28),py(28),spx(28),spy(28)
      character*128 Veta
      character*80 t80,p80
      logical EqIgCase,Wild,EqWild
      KPhaseIn=KPhase
      if(tisk.eq.1) then
        call newln(2)
        write(lst,'(/''Restrictions defined by user : '')')
      endif
      do i=1,nvai
        if(RestType(i).ne.' ') cycle
        KChd=naixb(i)/100
        if(KChd.gt.0.and..not.ChargeDensities) cycle
        naixbi=mod(naixb(i),100)
        nn=mod(iabs(naixbi),10)
        nsl=nails(i)
        if(tisk.eq.1) then
          n=0
          sumai(i)=0.
          do k=1,mai(i)
1000        l=KtAtMv(atvai(k,i),NAtInd,0)
            if(l.eq.0) then
              if(n.eq.0.and.index(atvai(k,i),'?').le.0.and.
     1                      index(atvai(k,i),'*').le.0) then
                if(NMolec.gt.0) then
                  t80='atom/molecule'
                else
                  t80='atom'
                endif
                Veta='The '//t80(:idl)
                t80=t80(:idl+1)//atvai(k,i)(:idel(atvai(k,i)))//
     1              ' isn''t on the file M40'
                Veta=Veta(:idel(Veta)+1)//'will not be restricted'
                call FeChybne(-1.,-1.,t80,Veta,WarningWithESC)
                if(ErrFlag.ne.0) then
                  go to 9999
                else
                  cycle
                endif
              endif
            else
              if(n.eq.0) then
                if(l.lt.0) then
                  AtMolFlag=-1
                  KPhase=kswmol((-l-1)/mxp+1)
                else
                  AtMolFlag=1
                  KPhase=kswa(l)
                endif
              else
                if((l.lt.0.and.AtMolFlag.gt.0).or.
     1             (l.gt.0.and.AtMolFlag.lt.0)) then
                  AtMolFlag=0
                endif
              endif
              n=n+1
              nai(n,i)=l
              if(naixbi.lt.11) then
                if(l.lt.0) then
                  if(naixbi.gt.0.or.KModM(1,-l).le.0) then
                    sumai(i)=sumai(i)+aimol(-l)
                  else
                    if(naixbi.le.-10) then
                      sumai(i)=sumai(i)+a0m(-l)
                    else
                      sumai(i)=sumai(i)+a0m(-l)*aimol(-l)
                    endif
                  endif
                else
                  if(naixbi.gt.0.or.KModA(1,l).le.0) then
                    sumai(i)=sumai(i)+ai(l)
                  else
                    if(naixbi.le.-10) then
                      sumai(i)=sumai(i)+a0(l)
                    else
                      sumai(i)=sumai(i)+a0(l)*ai(l)
                    endif
                  endif
                endif
              endif
              go to 1000
            endif
            cycle
            if(ErrFlag.ne.0) then
              go to 9999
            else
              n=0
            endif
          enddo
          if(mai(i).eq.1.and.nails(i).gt.0.and.NSymmL(KPhase).gt.0) then
            if(nn.gt.0) then
              if(nn.eq.IdRestXModADP) then
                Veta='Coordinates, ADP and modulations'
              else if(nn.eq.IdRestModADP) then
                Veta='ADP and modulations'
              else if(nn.eq.IdRestADP) then
                Veta='ADP of the atom'
              endif
              write(Cislo,100) nsl
              Veta=Veta(:idel(Veta))//' of the atom: '//
     1            atvai(1,i)(:idel(atvai(1,i)))//
     2            ' will be restricted by the local symmetry#'//
     3            Cislo(:idel(Cislo))
              call newln(3)
              write(lst,FormA)
              write(lst,FormA) Veta(:idel(Veta))
              write(lst,FormA)
            endif
          else
            j=(mai(i)-1)/10+1
            if(AtMolFlag.eq.0) then
              Veta='atoms/molecules'
            else if(AtMolFlag.gt.0) then
              Veta='atoms'
            else
              Veta='molecules'
            endif
            idl=idel(Veta)
            if(naixbi.le.-10) then
              call newln(j+2)
              write(lst,'(/''Occupational modulation of '',a,'' : '',
     1                    10a8/35x,10a8)')
     2          Veta(:idl),(atvai(j,i),j=1,mai(i))
              write(lst,'(''   ... will be kept identical'')')
            else if(naixbi.lt.0) then
              call newln(j+2)
              write(lst,'(/''Occupational modulation of '',a,'' : '',
     1                    10a8/35x,10a8)')
     2          Veta(:idl),(atvai(j,i),j=1,mai(i))
                write(lst,'(''   ... will be kept complementar'')')
            else if(naixbi.lt.11) then
              call newln(j+1)
              write(lst,'(/''Occupational restriction of '',a,'' : '',
     1                    10(a8,1x)/37x,10(a8,1x))') Veta(:idl),
     2                    (atvai(j,i),j=1,mai(i))
            else
              call newln(j+1)
              write(lst,'(/''Restriction of '',a,'' : '',10(a8,1x)/
     1                    25x,10(a8,1x))')
     2                    Veta(:idl),(atvai(j,i),j=1,mai(i))
            endif
            mai(i)=n
            if(naixbi.lt.11.and.naixbi.gt.0) then
              call newln(1)
              write(lst,'(''   ... sum of occupations will be kept to'',
     1                    f10.6)') sumai(i)
            endif
            if(nn.eq.IdRestXModADP) then
              t80='   ... coordinates, ADP and modulation'
            else if(nn.eq.IdRestModADP) then
              t80='   ... ADP and modulation'
            else if(nn.eq.IdRestADP) then
              t80='   ... ADP'
            else
              call newln(1)
              if(NDimI(KPhase).gt.0) then
                t80='   ... without restriction on coordinates, ADP '//
     1              'and modulation parameters'
              else
                t80='   ... without restriction on coordinates and ADP'
              endif
              write(lst,FormA) t80(:idel(t80))
            endif
            if(ChargeDensities.and.KChd.gt.0) then
              if(KChD.eq.1) then
                p80='   ... valence populations'
              else if(KChD.eq.2) then
                p80='   ... kappas'
              else if(KChD.eq.3) then
                p80='   ... valence populations and kappas'
              else if(KChD.eq.4) then
                p80='   ... kappas'''
              else if(KChD.eq.5) then
                p80='   ... valence populations and kappas'''
              else if(KChD.eq.6) then
                p80='   ... kappas and kappas'''
              else if(KChD.eq.5) then
                p80='   ... valence populations, kappas and kappas'''
              else
                p80=' '
              endif
            else
              p80=' '
            endif
            if(nn.le.IdRestADP.and.nn.gt.0) then
              if(nsl.gt.0) then
                Veta=' of second and futher atoms will be generated '//
     1              'from the first one'
                call newln(2)
              else
                Veta=' will be kept identical'
                call newln(1)
              endif
              write(lst,FormA) t80(:idel(t80))//Veta(:idel(Veta))
              if(nsl.gt.0) then
                Veta='       by the local symmetry#'
                write(Cislo,100) nsl
                Veta=Veta(:idel(Veta))//Cislo(1:1)
                write(lst,FormA) Veta(:idel(Veta))
              endif
            endif
            if(p80.ne.' ') then
              call newln(1)
              Veta=' will be kept identical'
              write(lst,FormA) p80(:idel(p80))//Veta(:idel(Veta))
            endif
          endif
        else
          do j=1,mai(i)
            l=nai(j,i)
            if(j.eq.1) then
              if(l.lt.0) then
                AtMolFlag=-1
                KPhase=kswmol((-l-1)/mxp+1)
              else
                AtMolFlag=1
                KPhase=kswa(l)
              endif
            else
              if((l.lt.0.and.AtMolFlag.gt.0).or.
     1           (l.gt.0.and.AtMolFlag.lt.0)) then
                AtMolFlag=0
              endif
            endif
          enddo
        endif
        if(mai(i).le.1) cycle
        if(naixbi.lt.11) then
          if(naixbi.le.-10) then
            k=nai(1,i)
          else
            k=nai(mai(i),i)
          endif
          if(k.lt.0) then
            kmodsk=KModM(1,-k)
            imk=(-k-1)/mxp+1
            iswk=iswmol(imk)
          else
            kmodsk=KModA(1,k)
            iswk=iswa(k)
          endif
          if(naixbi.gt.0.or.kmodsk.le.0) then
            if(k.lt.0) then
              aimol(-k)=sumai(i)
              saimol(-k)=0.
              if(KiMol(1,-k).ne.0) then
                if(Tisk.eq.1) NConstrain=NConstrain+1
                KiMol(1,-k)=0
              endif
            else
              ai(k)=sumai(i)
              sai(k)=0.
              if(KiA(1,k).ne.0) then
                if(Tisk.eq.1) NConstrain=NConstrain+1
                KiA(1,k)=0
              endif
            endif
          else
            if(k.lt.0) then
              if(aimol(-k).ne.0.) then
                if(naixbi.le.-10) then
!                  a0m(k)=sumai(i)
                else
                  a0m(-k)=sumai(i)/aimol(-k)
                endif
                sa0m(-k)=0.
              endif
              kip=8
              if(ktls(imk).gt.0) kip=kip+21
            else
              if(ai(k).ne.0.) then
                if(naixbi.le.-10) then
!                  a0(k)=sumai(i)
                else
                  a0(k)=sumai(i)/ai(k)
                endif
                sa0(k)=0.
              endif
              kip=max(TRankCumul(itf(k)),10)+1
            endif
            if(naixbi.gt.-10) then
              if(k.lt.0) then
                if(KiMol(kip,-k).ne.0) then
                  if(Tisk.eq.1) NConstrain=NConstrain+1
                  KiMol(kip,-k)=0
                endif
              else
                if(KiA(kip,k).ne.0) then
                  if(Tisk.eq.1) NConstrain=NConstrain+1
                  KiA(kip,k)=0
                endif
              endif
              do m=1,kmodsk
                if(k.lt.0) then
                  axm(m,-k)=0.
                  aym(m,-k)=0.
                  saxm(m,-k)=0.
                  saym(m,-k)=0.
                else
                  ax(m,k)=0.
                  ay(m,k)=0.
                  sax(m,k)=0.
                  say(m,k)=0.
                endif
                do ii=1,2
                  kip=kip+1
                  if(k.lt.0) then
                    if(KiMol(kip,-k).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiMol(kip,-k)=0
                    endif
                  else
                    if(KiA(kip,k).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiA(kip,k)=0
                    endif
                  endif
                enddo
              enddo
            endif
          endif
          do j=1,mai(i)
            if((naixbi.le.-10.and.j.eq.1).or.
     1         (naixbi.gt.-10.and.j.eq.mai(i))) cycle
            l=nai(j,i)
            if(l.lt.0) then
              iml=(-l-1)/mxp+1
              kmodsl=KModM(1,-l)
              if(k.lt.0) then
                if(aimol(-k).ne.0.) then
                  ratio=aimol(-l)/aimol(-k)
                else
                  ratio=0.
                endif
              else
                if(ai(k).ne.0.) then
                  ratio=aimol(-l)/ai(k)
                else
                  ratio=0.
                endif
              endif
            else
              kmodsl=KModA(1,l)
              if(k.lt.0) then
                if(aimol(-k).ne.0.) then
                  ratio=ai(l)/aimol(-k)
                else
                  ratio=0.
                endif
              else
                if(ai(k).ne.0.) then
                  ratio=ai(l)/ai(k)
                else
                  ratio=0.
                endif
              endif
            endif
            fip=0.
            if(NDimI(KPhase).eq.1) then
              do m=1,3
                if(l.lt.0) then
                  if(k.lt.0) then
                    pom= xm(m,iml)+trans(m,-l)
     1                  -xm(m,imk)-trans(m,-k)
                  else
                    pom= xm(m,iml)+trans(m,-l)
     1                  -x(m,k)
                  endif
                else
                  if(k.lt.0) then
                    pom= x(m,l)
     1                  -xm(m,imk)-trans(m,-k)
                  else
                    pom=x(m,l)-x(m,k)
                  endif
                endif
                fip=fip+pi2*qu(m,1,iswk,KPhase)*pom
              enddo
            endif
            if(naixbi.gt.0.or.kmodsl.le.0) then
              if(l.lt.0) then
                if(k.lt.0) then
                  aimol(-k)=aimol(-k)-aimol(-l)
                  saimol(-k)=saimol(-k)+saimol(-l)**2
                else
                  ai(k)=ai(k)-aimol(-l)
                  sai(k)=sai(k)+saimol(-l)**2
                endif
              else
                if(k.lt.0) then
                  aimol(-k)=aimol(-k)-ai(l)
                  saimol(-k)=saimol(-k)+sai(l)**2
                else
                  ai(k)=ai(k)-ai(l)
                  sai(k)=sai(k)+sai(l)**2
                endif
              endif
            else
              if(k.lt.0) then
                if(l.lt.0) then
                  if(naixbi.le.-10) then
!                    a0m(-l)=a0m(-k)
!                    sa0m(-l)=sa0m(-l)+sa0m(-k)**2
                    kip=8
                    if(ktls(iml).gt.0) kip=kip+21
                    if(KiMol(kip,-l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiMol(kip,-l)=0
                    endif
                  else
                    a0m(-k)=a0m(-k)-a0m(-l)*ratio
                    sa0m(-k)=sa0m(-k)+(sa0m(-l)*ratio)**2
                  endif
                else
                  if(naixbi.le.-10) then
!                    a0(l)=a0m(-k)
!                    sa0(l)=sa0(l)+sa0m(-k)**2
                    kip=max(TRankCumul(itf(l)),10)+1
                    if(KiA(kip,l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiA(kip,l)=0
                    endif
                  else
                    a0m(-k)=a0m(-k)-a0(l)*ratio
                    sa0m(-k)=sa0m(-k)+(sa0(l)*ratio)**2
                  endif
                endif
              else
                if(l.lt.0) then
                  if(naixbi.le.-10) then
!                    a0m(-l)=a0(k)
!                    sa0m(-l)=sa0m(-l)+sa0(k)**2
                    kip=8
                    if(ktls(iml).gt.0) kip=kip+21
                    if(KiMol(kip,-l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiMol(kip,-l)=0
                    endif
                  else
                    a0(k)=a0m(k)-a0m(-l)*ratio
                    sa0(k)=sa0m(k)+(sa0m(-l)*ratio)**2
                  endif
                else
                  if(naixbi.le.-10) then
!                    a0(l)=a0m(-k)
!                    sa0(l)=sa0(l)+sa0m(-k)**2
                    if(KiA(kip,l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      kip=max(TRankCumul(itf(l)),10)+1
                      KiA(kip,l)=0
                    endif
                  else
                  a0(k)=a0(k)-a0(l)*ratio
                  sa0(k)=sa0(k)+(sa0(l)*ratio)**2
                  endif
                endif
              endif
              do m=1,kmodsl
                pom=float(m)*fip
                snp=sin(pom)
                csp=cos(pom)
                if(l.lt.0) then
                  if(k.lt.0) then
                    if(naixbi.le.-10) then
                      axm(m,-l)=axm(m,-k)*csp+aym(m,-k)*snp
                      aym(m,-l)=aym(m,-k)*csp-axm(m,-k)*snp
                      saxm(m,-l)=(saxm(m,-k)*csp)**2+(saym(m,-k)*snp)**2
                      saym(m,-l)=(saym(m,-k)*csp)**2+(saxm(m,-k)*snp)**2
                    else
                      axm(m,-k)=axm(m,-k)-(axm(m,-l)*csp-aym(m,-l)*snp)
     1                                    *ratio
                      aym(m,-k)=aym(m,-k)-(aym(m,-l)*csp+axm(m,-l)*snp)
     1                                    *ratio
                      saxm(m,-k)=saxm(m,-k)+((saxm(m,-l)*csp)**2+
     1                                     (saym(m,-l)*snp)**2)*ratio**2
                      saym(m,-k)=saym(m,-k)+((saym(m,-l)*csp)**2+
     1                                     (saxm(m,-l)*snp)**2)*ratio**2
                    endif
                  else
                    if(naixbi.le.-10) then
                      axm(m,-l)=ax(m,k)*csp+ay(m,k)*snp
                      aym(m,-l)=ay(m,k)*csp-ax(m,k)*snp
                      saxm(m,-l)=(sax(m,k)*csp)**2+(say(m,k)*snp)**2
                      saym(m,-l)=(say(m,k)*csp)**2+(sax(m,k)*snp)**2
                    else
                      ax(m,k)=ax(m,k)-(axm(m,-l)*csp-aym(m,-l)*snp)
     1                                    *ratio
                      ay(m,k)=ay(m,k)-(aym(m,-l)*csp+axm(m,-l)*snp)
     1                                    *ratio
                      sax(m,k)=sax(m,k)+((saxm(m,-l)*csp)**2+
     1                                     (saym(m,-l)*snp)**2)*ratio**2
                      say(m,k)=say(m,k)+((saym(m,-l)*csp)**2+
     1                                     (saxm(m,-l)*snp)**2)*ratio**2
                    endif
                  endif
                else
                  if(k.lt.0) then
                    if(naixbi.le.-10) then
                      ax(m,l)=axm(m,-k)*csp+aym(m,-k)*snp
                      ay(m,l)=aym(m,-k)*csp-axm(m,-k)*snp
                      sax(m,l)=(saxm(m,-k)*csp)**2+(saym(m,-k)*snp)**2
                      say(m,l)=(saym(m,-k)*csp)**2+(saxm(m,-k)*snp)**2
                    else
                      axm(m,-k)=axm(m,-k)-(ax(m,l)*csp-ay(m,l)*snp)
     1                                    *ratio
                      aym(m,-k)=aym(m,-k)-(ay(m,l)*csp+ax(m,l)*snp)
     1                                    *ratio
                      saxm(m,-k)=saxm(m,-k)+((sax(m,l)*csp)**2+
     1                                     (say(m,l)*snp)**2)*ratio**2
                      saym(m,-k)=saym(m,-k)+((say(m,l)*csp)**2+
     1                                     (sax(m,l)*snp)**2)*ratio**2
                    endif
                  else
                    if(naixbi.le.-10) then
                      ax(m,l)=ax(m,k)*csp+ay(m,k)*snp
                      ay(m,l)=ay(m,k)*csp-ax(m,k)*snp
                      sax(m,l)=sqrt((sax(m,k)*csp)**2+(say(m,k)*snp)**2)
                      say(m,l)=sqrt((say(m,k)*csp)**2+(sax(m,k)*snp)**2)
                    else
                      ax(m,k)=ax(m,k)-(ax(m,l)*csp-ay(m,l)*snp)*ratio
                      ay(m,k)=ay(m,k)-(ay(m,l)*csp+ax(m,l)*snp)*ratio
                      sax(m,k)=sax(m,k)+((sax(m,l)*csp)**2+
     1                                   (say(m,l)*snp)**2)*ratio**2
                      say(m,k)=say(m,k)+((say(m,l)*csp)**2+
     1                                   (sax(m,l)*snp)**2)*ratio**2
                    endif
                  endif
                endif
                if(naixbi.le.-10) then
                  do ii=1,2
                    kip=kip+1
                    if(l.lt.0) then
                      if(KiMol(kip,-l).ne.0) then
                        if(Tisk.eq.1) NConstrain=NConstrain+1
                        KiMol(kip,-l)=0
                      endif
                    else
                      if(KiA(kip,l).ne.0) then
                        if(Tisk.eq.1) NConstrain=NConstrain+1
                        KiA(kip,l)=0
                      endif
                    endif
                  enddo
                endif
              enddo
            endif
          enddo
          if(naixbi.gt.0) then
            if(k.lt.0) then
              saimol(-k)=sqrt(saimol(-k))
            else
              sai(k)=sqrt(sai(k))
            endif
          else if(naixbi.gt.-10) then
            if(k.lt.0) then
              sa0m(-k)=sqrt(sa0m(-k))
            else
              sa0(k)=sqrt(sa0(k))
            endif
            do m=1,kmodsl
              if(k.lt.0) then
                saxm(m,-k)=sqrt(saxm(m,-k))
                saym(m,-k)=sqrt(saym(m,-k))
              else
                sax(m,k)=sqrt(sax(m,k))
                say(m,k)=sqrt(say(m,k))
              endif
            enddo
          endif
        endif
        if(AtMolFlag.eq.0) cycle
        k=nai(1,i)
        if(AtMolFlag.lt.0) then
          k=-k
          imk=(k-1)/mxp+1
          iswk=iswmol(imk)
          KPhase=kswmol(imk)
        else
          iswk=iswa(k)
          KPhase=kswa(k)
        endif
        do j=2,mai(i)
          l=nai(j,i)
          if(AtMolFlag.lt.0) then
            l=-l
            iml=(l-1)/mxp+1
            KPhase=kswmol(iml)
            if(nn.le.IdRestADP.and.ktls(iml).gt.0) then
              kip=8
              nrank=6
              do n=1,3
                if(n.eq.3) nrank=9
                do m=1,nrank
                  if(n.eq.1) then
                     tt(m,l)= tt(m,k)
                    stt(m,l)=stt(m,k)
                  else if(n.eq.2) then
                     tl(m,l)= tl(m,k)
                    stl(m,l)=stl(m,k)
                  else
                     ts(m,l)= ts(m,k)
                    sts(m,l)=sts(m,k)
                  endif
                  if(KiMol(kip,l).ne.0) then
                    if(Tisk.eq.1) NConstrain=NConstrain+1
                    KiMol(kip,l)=0
                  endif
                  kip=kip+1
                enddo
              enddo
            endif
            if(nn.le.IdRestModADP) then
              kip=8
              if(ktls(iml).gt.0) kip=kip+21
              if(KModM(1,l).gt.0) kip=kip+1+2*KModM(1,l)
              n=3*KModM(2,l)
              if(n.gt.0) then
                call CopyVek(utx(1,1,k),utx(1,1,l),n)
                call CopyVek(uty(1,1,k),uty(1,1,l),n)
                call CopyVek(urx(1,1,k),urx(1,1,l),n)
                call CopyVek(ury(1,1,k),ury(1,1,l),n)
                call CopyVek(sutx(1,1,k),sutx(1,1,l),n)
                call CopyVek(suty(1,1,k),suty(1,1,l),n)
                call CopyVek(surx(1,1,k),surx(1,1,l),n)
                call CopyVek(sury(1,1,k),sury(1,1,l),n)
                n=4*n
                do ii=1,n
                  if(KiMol(kip,l).ne.0) then
                    if(Tisk.eq.1) NConstrain=NConstrain+1
                    KiMol(kip,l)=0
                  endif
                  kip=kip+1
                enddo
              endif
              kmod=KModM(3,l)
              n=6*kmod
              if(n.gt.0) then
                call CopyVek(ttx(1,1,k),ttx(1,1,l),n)
                call CopyVek(tty(1,1,k),tty(1,1,l),n)
                call CopyVek(tlx(1,1,k),tlx(1,1,l),n)
                call CopyVek(tly(1,1,k),tly(1,1,l),n)
                call CopyVek(sttx(1,1,k),sttx(1,1,l),n)
                call CopyVek(stty(1,1,k),stty(1,1,l),n)
                call CopyVek(stlx(1,1,k),stlx(1,1,l),n)
                call CopyVek(stly(1,1,k),stly(1,1,l),n)
                n=(n*3)/2
                call CopyVek(tsx(1,1,k),tsx(1,1,l),n)
                call CopyVek(tsy(1,1,k),tsy(1,1,l),n)
                call CopyVek(stsx(1,1,k),stsx(1,1,l),n)
                call CopyVek(stsy(1,1,k),stsy(1,1,l),n)
                n=42*kmod
                do ii=1,n
                  if(KiMol(kip,l).ne.0) then
                    if(Tisk.eq.1) NConstrain=NConstrain+1
                    KiMol(kip,l)=0
                  endif
                  kip=kip+1
                enddo
              endif
            endif
            if(nn.eq.IdRestXModADP) then
              call CopyVek(Euler(1,k),Euler(1,l),3)
              call CopyVek(trans(1,k),trans(1,l),3)
              do ii=2,7
                if(KiMol(ii,l).ne.0) then
                  if(Tisk.eq.1) NConstrain=NConstrain+1
                  KiMol(ii,l)=0
                endif
              enddo
            endif
          else
            KPhase=kswa(l)
            if(nsl.gt.0) then
              call CopyVek(x(1,k),xp,3)
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              call multm(rm6l(1,nsl,iswk,KPhase),xp,xpp,NDim(KPhase),
     1                   NDim(KPhase),1)
              call AddVek(xpp,s6l(1,nsl,iswk,KPhase),xp,NDim(KPhase))
              do m=1,NDimI(KPhase)
                phi(m)=-xp(m+3)*pi2
              enddo
              kmodmx=0
              do m=1,7
                kmodmx=max(kmodmx,KModA(m,k))
              enddo
              do kk=1,kmodmx
                ll=iabs(KwSymL(kk,nsl,iswk,KPhase))
                pom=0.
                do m=1,NDimI(KPhase)
                  pom=pom+float(kw(m,ll,KPhase))*phi(m)
                enddo
                snls(kk,nsl,l)=sin(pom)
                csls(kk,nsl,l)=cos(pom)
              enddo
            endif
            itfl=itf(l)
            if(nn.le.IdRestADP.and.nn.gt.0) then
              kip=2
              do n=1,itfl
                nrank=TRank(n)
                if(n.eq.1.and.nn.eq.IdRestXModADP) then
                  if(nsl.gt.0) then
                    call CopyVek(xp,x(1,l),3)
                    call multmq(rml(1,nsl,iswk,KPhase),sx(1,k),
     1                          sx(1,l),3,3,1)
                  else
                    call CopyVek( x(1,k), x(1,l),3)
                    call CopyVek(sx(1,k),sx(1,l),3)
                  endif
                else if(n.eq.2) then
                  if(nsl.gt.0) then
                    call multm (rtl(1,nsl,iswk,KPhase), beta(1,k),
     1                           beta(1,l),nrank,nrank,1)
                    call multmq(rtl(1,nsl,iswk,KPhase),sbeta(1,k),
     1                          sbeta(1,l),nrank,nrank,1)
                  else
                    call CopyVek( beta(1,k), beta(1,l),nrank)
                    call CopyVek(sbeta(1,k),sbeta(1,l),nrank)
                  endif
                else if(n.eq.3) then
                  if(nsl.gt.0) then
                    call multm (rc3l(1,nsl,iswk,KPhase), c3(1,k),
     1                           c3(1,l),nrank,nrank,1)
                    call multmq(rc3l(1,nsl,iswk,KPhase),sc3(1,k),
     1                          sc3(1,l),nrank,nrank,1)
                  else
                    call CopyVek( c3(1,k), c3(1,l),nrank)
                    call CopyVek(sc3(1,k),sc3(1,l),nrank)
                  endif
                else if(n.eq.4) then
                  if(nsl.gt.0) then
                    call multm (rc4l(1,nsl,iswk,KPhase), c4(1,k),
     1                           c4(1,l),nrank,nrank,1)
                    call multmq(rc4l(1,nsl,iswk,KPhase),sc4(1,k),
     1                          sc4(1,l),nrank,nrank,1)
                  else
                    call CopyVek( c4(1,k), c4(1,l),nrank)
                    call CopyVek(sc4(1,k),sc4(1,l),nrank)
                  endif
                else if(n.eq.5) then
                  if(nsl.gt.0) then
                    call multm (rc5l(1,nsl,iswk,KPhase), c5(1,k),
     1                           c5(1,l),nrank,nrank,1)
                    call multmq(rc5l(1,nsl,iswk,KPhase),sc5(1,k),
     1                          sc5(1,l),nrank,nrank,1)
                  else
                    call CopyVek( c5(1,k), c5(1,l),nrank)
                    call CopyVek(sc5(1,k),sc5(1,l),nrank)
                  endif
                else if(n.eq.6) then
                  if(nsl.gt.0) then
                    call multm (rc6l(1,nsl,iswk,KPhase), c6(1,k),
     1                           c6(1,l),nrank,nrank,1)
                    call multmq(rc6l(1,nsl,iswk,KPhase),sc6(1,k),
     1                          sc6(1,l),nrank,nrank,1)
                  else
                    call CopyVek( c6(1,k), c6(1,l),nrank)
                    call CopyVek(sc6(1,k),sc6(1,l),nrank)
                  endif
                endif
                if(n.gt.1.or.(n.eq.1.and.nn.eq.IdRestXModADP)) then
                  do ii=1,nrank
                    if(KiA(kip,l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiA(kip,l)=0
                    endif
                    kip=kip+1
                  enddo
                else
                  kip=kip+nrank
                endif
              enddo
              if(itfl.le.1) then
                beta(1,l)=beta(1,k)
                sbeta(1,l)=sbeta(1,k)
                if(KiA(kip,l).ne.0) then
                  if(Tisk.eq.1) NConstrain=NConstrain+1
                  KiA(kip,l)=0
                endif
              endif
            endif
            if(nn.le.IdRestModADP.and.nn.gt.0) then
              kip=max(TRankCumul(itfl),10)+1
              do n=1,itfl+1
                nrank=TRank(n-1)
                kmod=KModA(n,l)
                kfap=KFA(n,l)
                if(n.eq.1.and.kmod.gt.0) then
                  kip=kip+1+2*kmod
                  cycle
                endif
                do m=1,kmod
                  if(nsl.gt.0) then
                    iw=iabs(KwSymL(m,nsl,iswk,KPhase))
                    snp=snls(m,nsl,l)
                    csp=csls(m,nsl,l)
                    epsp=isign(1,KwSymL(m,nsl,iswk,KPhase))
                  endif
                  if(n.eq.2) then
                    if(nsl.gt.0) then
                      call multm(rml(1,nsl,iswk,KPhase),ux(1,m,k),px,
     1                           nrank,nrank,1)
                      call multmq(rml(1,nsl,iswk,KPhase),sux(1,m,k),spx,
     1                            nrank,nrank,1)
                      if(kfap.eq.0.or.m.ne.kmod) then
                        call multm(rml(1,nsl,iswk,KPhase),uy(1,m,k),py,
     1                             nrank,nrank,1)
                        call multmq(rml(1,nsl,iswk,KPhase),suy(1,m,k),
     1                              spy,nrank,nrank,1)
                      endif
                    else
                      call CopyVek( ux(1,m,k), ux(1,m,l),nrank)
                      call CopyVek( uy(1,m,k), uy(1,m,l),nrank)
                      call CopyVek(sux(1,m,k),sux(1,m,l),nrank)
                      call CopyVek(suy(1,m,k),suy(1,m,l),nrank)
                    endif
                  else if(n.eq.3) then
                    if(nsl.gt.0) then
                      call multm(rtl(1,nsl,iswk,KPhase),bx(1,m,k),px,
     1                           nrank,nrank,1)
                      call multm(rtl(1,nsl,iswk,KPhase),by(1,m,k),py,
     1                           nrank,nrank,1)
                      call multmq(rtl(1,nsl,iswk,KPhase),sbx(1,m,k),spx,
     1                           nrank,nrank,1)
                      call multmq(rtl(1,nsl,iswk,KPhase),sby(1,m,k),spy,
     1                           nrank,nrank,1)
                    else
                      call CopyVek( bx(1,m,k), bx(1,m,l),nrank)
                      call CopyVek( by(1,m,k), by(1,m,l),nrank)
                      call CopyVek(sbx(1,m,k),sbx(1,m,l),nrank)
                      call CopyVek(sby(1,m,k),sby(1,m,l),nrank)
                    endif
                  else if(n.eq.4) then
                    if(nsl.gt.0) then
                      call multm(rc3l(1,nsl,iswk,KPhase),c3x(1,m,k),px,
     1                           nrank,nrank,1)
                      call multm(rc3l(1,nsl,iswk,KPhase),c3y(1,m,k),py,
     1                           nrank,nrank,1)
                      call multmq(rc3l(1,nsl,iswk,KPhase),sc3x(1,m,k),
     1                            spx,nrank,nrank,1)
                      call multmq(rc3l(1,nsl,iswk,KPhase),sc3y(1,m,k),
     1                            spy,nrank,nrank,1)
                    else
                      call CopyVek( c3x(1,m,k), c3x(1,m,l),nrank)
                      call CopyVek( c3y(1,m,k), c3y(1,m,l),nrank)
                      call CopyVek(sc3x(1,m,k),sc3x(1,m,l),nrank)
                      call CopyVek(sc3y(1,m,k),sc3y(1,m,l),nrank)
                    endif
                  else if(n.eq.5) then
                    if(nsl.gt.0) then
                      call multm(rc4l(1,nsl,iswk,KPhase),c4x(1,m,k),px,
     1                           nrank,nrank,1)
                      call multm(rc4l(1,nsl,iswk,KPhase),c4y(1,m,k),py,
     1                           nrank,nrank,1)
                      call multmq(rc4l(1,nsl,iswk,KPhase),sc4x(1,m,k),
     1                            spx,nrank,nrank,1)
                      call multmq(rc4l(1,nsl,iswk,KPhase),sc4y(1,m,k),
     1                            spy,nrank,nrank,1)
                    else
                      call CopyVek( c4x(1,m,k), c4x(1,m,l),nrank)
                      call CopyVek( c4y(1,m,k), c4y(1,m,l),nrank)
                      call CopyVek(sc4x(1,m,k),sc4x(1,m,l),nrank)
                      call CopyVek(sc4y(1,m,k),sc4y(1,m,l),nrank)
                    endif
                  else if(n.eq.6) then
                    if(nsl.gt.0) then
                      call multm(rc5l(1,nsl,iswk,KPhase),c5x(1,m,k),px,
     1                           nrank,nrank,1)
                      call multm(rc5l(1,nsl,iswk,KPhase),c5y(1,m,k),py,
     1                           nrank,nrank,1)
                      call multmq(rc5l(1,nsl,iswk,KPhase),sc5x(1,m,k),
     1                            spx,nrank,nrank,1)
                      call multmq(rc5l(1,nsl,iswk,KPhase),sc5y(1,m,k),
     1                            spy,nrank,nrank,1)
                    else
                      call CopyVek( c5x(1,m,k), c5x(1,m,l),nrank)
                      call CopyVek( c5y(1,m,k), c5y(1,m,l),nrank)
                      call CopyVek(sc5x(1,m,k),sc5x(1,m,l),nrank)
                      call CopyVek(sc5y(1,m,k),sc5y(1,m,l),nrank)
                    endif
                  else if(n.eq.7) then
                    if(nsl.gt.0) then
                      call multm(rc6l(1,nsl,iswk,KPhase),c6x(1,m,k),px,
     1                           nrank,nrank,1)
                      call multm(rc6l(1,nsl,iswk,KPhase),c6y(1,m,k),py,
     1                           nrank,nrank,1)
                      call multmq(rc6l(1,nsl,iswk,KPhase),sc6x(1,m,k),
     1                            spx,nrank,nrank,1)
                      call multmq(rc6l(1,nsl,iswk,KPhase),sc6y(1,m,k),
     1                            spy,nrank,nrank,1)
                    else
                      call CopyVek( c6x(1,m,k), c6x(1,m,l),nrank)
                      call CopyVek( c6y(1,m,k), c6y(1,m,l),nrank)
                      call CopyVek(sc6x(1,m,k),sc6x(1,m,l),nrank)
                      call CopyVek(sc6y(1,m,k),sc6y(1,m,l),nrank)
                    endif
                  endif
                  if(nsl.gt.0.and.(kfap.ne.1.or.m.ne.kmod)) then
                    do mm=1,nrank
                      ppx=epsp*csp*px(mm)-snp*py(mm)
                      ppy=epsp*snp*px(mm)+csp*py(mm)
                      sppx=sqrt((epsp*csp*spx(mm))**2+(snp*spy(mm))**2)
                      sppy=sqrt((epsp*snp*spx(mm))**2+(csp*spy(mm))**2)
                      if(n.eq.2) then
                        ux(mm,iw,l)=ppx
                        uy(mm,iw,l)=ppy
                        sux(mm,iw,l)=sppx
                        suy(mm,iw,l)=sppy
                      else if(n.eq.3) then
                        bx(mm,iw,l)=ppx
                        by(mm,iw,l)=ppy
                        sbx(mm,iw,l)=sppx
                        sby(mm,iw,l)=sppy
                      else if(n.eq.4) then
                        c3x(mm,iw,l)=ppx
                        c3y(mm,iw,l)=ppy
                        sc3x(mm,iw,l)=sppx
                        sc3y(mm,iw,l)=sppy
                      else if(n.eq.5) then
                        c4x(mm,iw,l)=ppx
                        c4y(mm,iw,l)=ppy
                        sc4x(mm,iw,l)=sppx
                        sc4y(mm,iw,l)=sppy
                      else if(n.eq.6) then
                        c5x(mm,iw,l)=ppx
                        c5y(mm,iw,l)=ppy
                        sc5x(mm,iw,l)=sppx
                        sc5y(mm,iw,l)=sppy
                      else
                        c6x(mm,iw,l)=ppx
                        c6y(mm,iw,l)=ppy
                        sc5x(mm,iw,l)=sppx
                        sc5y(mm,iw,l)=sppy
                      endif
                    enddo
                  endif
                  do ii=1,2*nrank
                    if(KiA(kip,l).ne.0) then
                      if(Tisk.eq.1) NConstrain=NConstrain+1
                      KiA(kip,l)=0
                    endif
                    kip=kip+1
                  enddo
                enddo
              enddo
              do n=1,itfl
                if(KModA(n,l).gt.0) then
                  KiA(kip,l)=0
                  phf(l)=phf(k)
                  sphf(l)=sphf(k)
                  exit
                endif
              enddo
            endif
            if(KChD.gt.0.and.lasmax(k).gt.0.and.lasmax(k).gt.0) then
              kk=KChD
              do icd=1,3
                if(mod(kk,2).eq.1) then
                  if(icd.eq.1) then
                    popv(l)=popv(k)
                  else if(icd.eq.2) then
                    kapa1(l)=kapa1(k)
                  else if(icd.eq.3) then
                    kapa2(l)=kapa2(k)
                  endif
                  kip=max(TRankCumul(itf(k)),10)+icd+1
                  KiA(kip,l)=0
                endif
                kk=kk/2
              enddo
            endif
          endif
        enddo
      enddo
3000  if(NPhase.le.1.or..not.ExistPowder) go to 9999
      do i=1,nvai
        if(RestType(i).eq.' ') cycle
        if(Tisk.eq.1) then
          n=0
          do k=1,mai(i)
            Wild=index(atvai(k,i),'*').gt.0.or.
     1           index(atvai(k,i),'?').gt.0
            l=0
            do KPh=1,NPhase
              if(Wild) then
                if(EqWild(PhaseName(KPh),atvai(k,i),.false.)) then
                  n=n+1
                  nai(n,i)=KPh
                  l=KPh
                endif
              else
                if(EqIgCase(PhaseName(KPh),atvai(k,i))) then
                  n=n+1
                  nai(n,i)=KPh
                  l=KPh
                  exit
                endif
              endif
            enddo
            if(l.le.0.and..not.Wild) then
              write(Cislo,'(3i5)') i,k,l
              Veta='The phase '//atvai(k,i)(:idel(atvai(k,i)))//
     1             'doesn''t exist and therefore it will not be '//
     2             'restricted.'
              call FeChybne(-1.,-1.,Veta,Cislo,WarningWithESC)
            endif
          enddo
          j=(mai(i)-1)/10+1
          call newln(j+2)
          write(lst,'(/''Profile parameters of : '',10a8/35x,10a8)')
     2          (atvai(j,i),j=1,mai(i))
          write(lst,'(''   ... will be kept identical'')')
          mai(i)=n
        endif
        do KDatB=1,NDatBlock
          KPh1=nai(1,i)
          do k=2,mai(i)
            KPh2=nai(k,i)
            KProfPwd(KPh2,KDatB)=KProfPwd(KPh1,KDatB)
            KStrain(KPh2,KDatB)=KStrain(KPh1,KDatB)
            PCutOff(KPh2,KDatB)=PCutOff(KPh1,KDatB)
            IZdvih=(KPh2-1)*NParCellProfPwd+(KDatB-1)*NParProfPwd
            j=IGaussPwd+IZdvih
            call SetIntArrayTo(KiPwd(j),4,0)
            j=ILorentzPwd+IZdvih
            call SetIntArrayTo(KiPwd(j),5,0)
            if(KProfPwd(KPh1,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
              call CopyVek(GaussPwd(1,KPh1,KDatB),
     1                     GaussPwd(1,KPh2,KDatB),4)
            endif
            if(KProfPwd(KPh1,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh1,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
              if(KStrain(KPh1,KDatB).eq.IdPwdStrainTensor.and.
     1           KProfPwd(KPh1,KDatB).eq.IdPwdProfVoigt) then
                ii=5
              else
                ii=4
              endif
              call CopyVek(LorentzPwd(1,KPh1,KDatB),
     1                     LorentzPwd(1,KPh2,KDatB),ii)
              ZetaPwd(KPh2,KDatB)=ZetaPwd(KPh1,KDatB)
            endif
            if(KStrain(KPh1,KDatB).eq.IdPwdStrainTensor.and.
     1         NDim(KPh1).eq.NDim(KPh2)) then
              if(NDimI(KPh1).eq.1) then
                 n=35
              else
                 n=15+NDimI(KPh1)
              endif
              call CopyVek(StPwd(1,KPh1,KDatB),StPwd(1,KPh2,KDatB),n)
              j=IStPwd+IZdvih
              call SetIntArrayTo(KiPwd(j),n,0)
            endif
          enddo
        enddo
      enddo
9999  KPhase=KPhaseIn
      return
100   format(i1)
      end
