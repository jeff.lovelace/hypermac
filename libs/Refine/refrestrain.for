      subroutine RefRestrain(WhatToRestrain)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/RestrainQuest/ RestrainType,nEdwValue,nEdwESD,nEdwAtoms,
     1     FixValue,FixValueESD,StringOfAtoms,TypeOfRestrain,FixString
      character*(*) WhatToRestrain
      character*256 StringOfAtoms,t256,EdwStringQuest
      character*80 Veta
      character*8  FixString
      integer RestrainType,WhatHappened,TypeOfRestrain
      logical   CrwLogicQuest
      external RefRestrainReadCommand,RefRestrainWriteCommand,FeVoid
      save /RestrainQuest/
      FixString=WhatToRestrain
      call mala(FixString)
      if(WhatToRestrain.eq.'distfix') then
        RestrainType=IdDistFix
      else if(WhatToRestrain.eq.'anglefix') then
        RestrainType=IdAngleFix
      else
        RestrainType=IdTorsFix
      endif
      xqd=500.
      i=4
      if(NDimI(KPhase).gt.0) i=i+1
      t256=fln(:ifln)//'_'//
     1            WhatToRestrain(:idel(WhatToRestrain))//'.tmp'
      call RepeatCommandsProlog(id,t256,xqd,i,il,OKForBasicFiles)
      il=il+1
      ilp=il
      xpom=5.
      tpom=xpom+CrwgXd+5.
      nTypeOfRestrain=2
      if(NDimI(KPhase).gt.0) nTypeOfRestrain=nTypeOfRestrain+1
      Veta='Fix to the s%pecified value'
      xpomc=0.
      do i=1,nTypeOfRestrain
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          Veta='Keep a value %equal for all items'
          nCrwValue=CrwLastMade
        else if(i.eq.2) then
          Veta='Fix to an optimal value'
          nCrwEqual=CrwLastMade
        else
          nCrwOptimal=CrwLastMade
        endif
        xpomc=max(xpomc,FeTxLengthUnder(Veta))
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        il=il+1
      enddo
      il=ilp
      tpom=tpom+xpomc+40.
      xpom=tpom+40.
      dpom=80.
      Veta='%Value'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwValue=EdwLastMade
      Veta='s%.u.'
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwESD=EdwLastMade
      if(NDimI(KPhase).gt.0) il=il+1
      Veta='%Atoms'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      if(RestrainType.eq.IdDistFix) then
        Veta='pair'
      else if(RestrainType.eq.IdAngleFix) then
        Veta='triplet'
      else if(RestrainType.eq.IdTorsFix) then
        Veta='quartet'
      endif
      il=il+1
      Veta='Each '//Veta(:idel(Veta))//
     1    ' of atoms should be separated by semicolon'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
1300  TypeOfRestrain=1
      if(RestrainType.eq.IdDistFix) then
        FixValue   =1.5
        FixValueESD=0.001
      else if(RestrainType.eq.IdAngleFix) then
        FixValue   =109.47
        FixValueESD=  0.01
      else if(RestrainType.eq.IdTorsFix) then
        FixValue   =180.00
        FixValueESD=  0.01
      endif
      StringOfAtoms=' '
1350  if(TypeOfRestrain.eq.1) then
        call FeQuestRealEdwOpen(nEdwValue,FixValue,.false.,.false.)
      else
        call FeQuestEdwDisable(nEdwValue)
      endif
      call FeQuestRealEdwOpen(nEdwESD,FixValueESD,.false.,.false.)
      call FeQuestStringEdwOpen(nEdwAtoms,StringOfAtoms)
      nCrw=nCrwValue
      do i=1,nTypeOfRestrain
        call FeQuestCrwOpen(nCrw,i.eq.TypeOfRestrain)
        nCrw=nCrw+1
      enddo
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  RefRestrainReadCommand,RefRestrainWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventCrw) then
        if(TypeOfRestrain.eq.1)
     1    call FeQuestRealFromEdw(nEdwValue,FixValue)
        nCrw=nCrwValue
        do i=1,nTypeOfRestrain
          if(CrwLogicQuest(nCrw)) then
            TypeOfRestrain=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        StringOfAtoms=EdwStringQuest(nEdwAtoms)
        go to 1350
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
