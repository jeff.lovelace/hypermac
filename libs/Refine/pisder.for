      subroutine PisDer(Ln,ihp,sigyo,wdy,no,nspec,kspec)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ihp(6),kmodai(7)
      character*1 nspec,kspec
      character*25  Formln
      data formln/'(''Extinction : '',10e15.7)'/
      if(.not.CalcDer) go to 9999
      if(isPowder) then
        write(Ln,'(i8,2f10.1)') no,sigyo,wdy
      else
        if(ExistMagnetic) then
          write(Ln,format3)(ihp(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b,dyp,
     1                      sigyo,1./sqrt(wt),wdy,no,nspec,kspec,sinthl,
     2                      extkor,extkorm,iq
        else
          write(Ln,format3)(ihp(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b,dyp,
     1                      sigyo,1./sqrt(wt),wdy,no,nspec,kspec,sinthl,
     2                      extkor,iq
        endif
      endif
      formln(3:13)='Scale'
      write(ln,formln)(der(i),i=1,6)
      formln(3:13)='Extinction'
      write(ln,formln)(der(i),i=19,30)
      write(ln,112)
      do i=1,NAtInd
        itfi=itf(i)
        kmodai(1)=KModA(1,i)
        if(TypeModFun(i).ne.1) then
          do j=2,itfi+1
            kmodai(j)=KModA(j,i)
          enddo
        else
          do j=2,itfi+1
            kmodai(j)=kmodao(j,i)
          enddo
        endif
        mp=PrvniKiAtomu(i)
        mk=mp+9
        write(ln,100) atom(i),(der(m),m=mp,mk)
        formln(3:11)='B'
        do j=3,itfi
          formln(3:3)=char(ichar(formln(3:3))+1)
          mp=mk+1
          mk=mk+TRank(j)
          write(ln,formln)(der(m),m=mp,mk)
        enddo
        if(NDim(KPhase).eq.3.and.lasmax(i).gt.0) then
          formln(3:13)='Dens1'
          mp=mk+1
          mk=mk+3
          if(lasmax(i).gt.1) mk=mk+1
          write(ln,formln)(der(m),m=mp,mk)
          if(lasmax(i).le.1) go to 1400
          mp=mk+1
          mk=mk+(lasmax(i)-1)**2
          if(mk.ge.mp) then
            formln(3:13)='Dens2'
            write(ln,formln)(der(m),m=mp,mk)
            go to 1400
          endif
        endif
        k=ichar('C')-4
        do j=1,itfi+1
          if(j.eq.1) then
            formln(3:13)='Occ.mod.'
          else if(j.eq.2) then
            formln(3:13)='Pos.mod.'
          else if(j.eq.3) then
            formln(3:13)='Temp.mod.'
          else
            write(formln(3:13),'(a1,''.mod.'')') char(k+j)
          endif
          mp=mk+1
          mk=mk+2*TRank(j-1)*kmodai(j)
          if(j.eq.1.and.kmodai(1).gt.0) mk=mk+1
          if(mk.ge.mp) write(ln,formln)(der(m),m=mp,mk)
        enddo
        do j=1,itfi+1
          if(kmodai(j).gt.0) then
            mp=mk+1
            mk=mk+1
            formln(3:13)='Phason'
            write(ln,formln) der(mp)
            go to 1400
          endif
        enddo
1400    if(MagPar(i).gt.0) then
          formln(3:13)='Mag0'
          mp=mk+1
          mk=mk+3
          write(ln,formln)(der(m),m=mp,mk)
          formln(3:13)='Mag.mod.'
          do j=1,MagPar(i)-1
            mp=mk+1
            mk=mk+6
            write(ln,formln)(der(m),m=mp,mk)
          enddo
        endif
        write(ln,112)
      enddo
      do i=NAtMolFr(1,1),NAtAll
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        m=PrvniKiAtomu(i)
        write(ln,100) atom(i),(der(j),j=m,m+9)
        m=m+10
        if(kmodsi.gt.0) then
          write(ln,110)(der(j),j=m,m+2*kmodsi)
          m=m+2*kmodsi+1
        endif
        if(kmodxi.gt.0) then
          write(ln,109)(der(j),j=m,m+6*kmodxi-1)
          m=m+6*kmodxi
        endif
        if(kmodbi.gt.0) then
          write(ln,106)(der(j),j=m,m+12*kmodbi-1)
          m=m+12*kmodbi
        endif
        if(kmodsi.gt.0.or.kmodxi.gt.0.or.kmodsi.gt.0) then
          write(ln,111) der(m)
          m=m+1
        endif
        write(ln,112)
      enddo
      do i=1,NMolec
        do j=1,mam(i)
          ji=j+(i-1)*mxp
          m=PrvniKiMolekuly(ji)
          write(ln,100) molname(i),(der(k),k=m,m+6)
          m=m+7
          if(ktls(i).gt.0) then
            write(ln,'(''T          : '',6e15.7)')(der(k),k=m,m+5)
            m=m+6
            write(ln,'(''L          : '',6e15.7)')(der(k),k=m,m+5)
            m=m+6
            write(ln,'(''S          : '',6e15.7)')(der(k),k=m,m+8)
            m=m+9
          endif
          if(KModM(1,ji).gt.0) then
            write(ln,110)(der(k),k=m,m+2*KModM(1,ji))
            m=m+2*KModM(1,ji)+1
          endif
          if(KModM(2,ji).gt.0) then
            write(ln,109)(der(k),k=m,m+6*KModM(2,ji)-1)
            m=m+6*KModM(2,ji)
            write(ln,109)(der(k),k=m,m+6*KModM(2,ji)-1)
            m=m+6*KModM(2,ji)
          endif
          if(KModM(3,ji).gt.0) then
            write(ln,106)(der(k),k=m,m+12*KModM(3,ji)-1)
            m=m+12*KModM(3,ji)
            write(ln,106)(der(k),k=m,m+12*KModM(3,ji)-1)
            m=m+12*KModM(3,ji)
            write(ln,106)(der(k),k=m,m+18*KModM(3,ji)-1)
            m=m+18*KModM(3,ji)
          endif
          if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1       KModM(3,ji).gt.0) write(ln,111) der(m)
          write(ln,112)
        enddo
      enddo
9999  return
100   format(a8,'   : ',10e15.7)
106   format('Temp.      : ',10e15.7)
109   format('Position   : ',10e15.7)
110   format('Occ.       : ',10e15.7)
111   format('Phason     : ',10e15.7)
112   format(163('-'))
      end
