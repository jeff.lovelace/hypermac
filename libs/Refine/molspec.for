      subroutine MolSpec(nmp,nmk)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x0(6),x1(6),x2(6),smp(3,3),smpp(3,3),smpt(6,6),
     1          smps(9,9),rx(:,:),rtt(:,:),rts(:,:),px(:),pt(:),pxx(3),
     2          snm(:),csm(:),eps(:),nmx(:),nmr(:),nms(:),nmtt(:),pb(:),
     3          nmts(:),rmx(:,:,:),rmr(:,:,:),rms(:,:,:),rmtt(:,:,:),
     4          rmts(:,:,:),rms56(:),rmx56(:),rmr56(:),rmtt56(:),
     5          rmts56(:),phi(3),x0p(6),x1p(6),x2p(6)
      character*11 molji
      integer TypeModFunI,OrthoSelI(:)
      allocatable rx,rtt,rts,px,pt,pb,snm,csm,eps,nmx,nmr,nms,
     1            nmtt,nmts,rmx,rmr,rms,rmtt,rmts,rms56,rmx56,rmr56,
     2            rmtt56,rmts56,OrthoSelI
      allocate(rx(6,3),rtt(12,6),rts(18,9),px(6),pt(18))
      call SetRealArrayTo(pt,18,0.)
      if(NDimI(KPhase).gt.0.and.MaxUsedKwAll.gt.0) then
        n=MaxUsedKwAll
        if(OrthoOrd.gt.0) then
          m=OrthoOrd
        else
          m=1
        endif
        allocate(pb(36*n),snm(n),csm(n),eps(n),nmx(n),nmr(n),nms(n),
     1           nmtt(n),nmts(n),OrthoSelI(m))
        if(NDimI(KPhase).eq.1) then
          allocate(rmx(12,6,n),rmr(12,6,n),rms(4,2,n),rmtt(24,12,n),
     1             rmts(36,18,n))
        else
          n=MaxUsedKwAll**2
          allocate(rms56(8*n),rmx56(72*n),rmr56(72*n),rmtt56(288*n),
     1             rmts56(648*n))
        endif
      endif
      iap=NAtMolFrAll(KPhase)
      nap=NAtPosFrAll(KPhase)
      do i=nmp,nmk
        isw=iswmol(i)
        if(MaxUsedKwAll.gt.0) call SetRealArrayTo(pb,36*MaxUsedKwAll,0.)
        do 4000jp=1,mam(i)
          write(molji,'(a8,''#'',i2)') molname(i),jp
          call zhusti(molji)
          ji=jp+(i-1)*mxp
          TypeModFunI=TypeModFunMol(ji)
          km=PrvniKiMolekuly(ji)
          call SetRealArrayTo(rx,18,0.)
          call SetRealArrayTo(px, 6,0.)
          nx=0
          call SetRealArrayTo(rtt,72,0.)
          ntt=0
          call SetRealArrayTo(rts,162,0.)
          call SetRealArrayTo(pt,  18,0.)
          nts=0
          kfsi=KFM(1,ji)
          kfxi=KFM(2,ji)
          kfbi=KFM(3,ji)
          if(NDimI(KPhase).gt.0.and.MaxUsedKwAll.gt.0) then
            kmodsi=KModM(1,ji)
            if(OrthoOrd.gt.0) then
              call CopyVekI(OrthoSel(1,nap),OrthoSelI,OrthoOrd)
            else
              OrthoSelI(1)=1
            endif
            if(kfxi.eq.0.or.KModM(2,i).eq.0.or.NDimI(KPhase).gt.1)
     1        then
              k=KModM(2,ji)
            else
              k=KModM(2,ji)-1
            endif
            if(TypeModFunI.ne.1) then
              kmodxi=KModM(2,ji)
            else
              kmodxi=(OrthoSelI(2*k+1)+1)/2
            endif
            if(kfbi.eq.0.or.KModM(3,i).eq.0.or.NDimI(KPhase).gt.1)
     1        then
              k=KModM(3,ji)
            else
              k=KModM(3,ji)-1
            endif
            if(TypeModFunI.ne.1) then
              kmodbi=KModM(3,ji)
            else
              kmodbi=(OrthoSelI(2*k+1)+1)/2
            endif
            if(kfsi.eq.0) then
              kmezsi=kmodsi
            else
              kmezsi=0
            endif
            if(kfxi.eq.0) then
              kmezxi=kmodxi
            else if(kfxi.eq.1.or.kfxi.eq.4) then
              kmezxi=kmodxi-1
            else if(kfxi.eq.2.or.kfxi.eq.5) then
              kmezxi=kmodxi-2
            else if(kfxi.eq.3.or.kfxi.eq.6) then
              kmezxi=kmodxi-3
            endif
            if(kfbi.eq.0) then
              kmezbi=kmodbi
            else
              kmezbi=kmodbi-1
            endif
            kmodmx=max(kmezsi,kmezxi,kmezbi)
            if(NDimI(KPhase).eq.1) then
              if(kmezxi.gt.0) then
                k=72*kmezxi
                call SetRealArrayTo(rmx,k,0.)
                call SetRealArrayTo(rmr,k,0.)
                call SetIntArrayTo(nmx,kmezxi,0)
                call SetIntArrayTo(nmr,kmezxi,0)
              endif
              if(kmezsi.gt.0) then
                call SetRealArrayTo(rms,8*kmezsi,0.)
                call SetIntArrayTo(nms,kmezsi,0)
              endif
              if(kmezbi.gt.0) then
                call SetRealArrayTo(rmtt,24*12*kmezbi,0.)
                call SetIntArrayTo(nmtt,kmezbi,0)
                call SetRealArrayTo(rmts,36*18*kmezbi,0.)
                call SetIntArrayTo(nmts,kmezbi,0)
              endif
            else if(NDimI(KPhase).gt.1) then
              if(kmezsi.gt.0) then
                call SetRealArrayTo(rms56,8*kmezsi*kmezsi,0.)
                nms(1)=0
              endif
              if(kmezxi.gt.0) then
                k=72*kmezxi*kmezxi
                call SetRealArrayTo(rmx56,k,0.)
                call SetRealArrayTo(rmr56,k,0.)
                nmx(1)=0
                nmr(1)=0
              endif
              if(kmezbi.gt.0) then
                k=kmezbi*kmezbi
                call SetRealArrayTo(rmtt56,288*k,0.)
                call SetRealArrayTo(rmts56,648*k,0.)
                nmtt(1)=0
                nmts(1)=0
              endif
            endif
          endif
          do k=1,3
            x0(k)=xm(k,i)+trans(k,ji)
          enddo
          NDimp=NDim(KPhase)
          if(kfsi.gt.0.and.KModM(1,ji)-NDim(KPhase)+4.gt.0) then
            call CopyVek(axm(KModM(1,ji)-NDim(KPhase)+4,ji),x0(4),
     1                   NDimI(KPhase))
          else if(kfxi.gt.0.and.KModM(2,ji).gt.0) then
            x0(4)=uty(1,KModM(2,ji),i)
          else
            call SetRealArrayTo(x0(4),NDimI(KPhase),0.)
            NDimp=3
          endif
          if(NDimp.gt.3) then
            call CopyVek(x0,x0p,3)
            call SetRealArrayTo(x0p(4),NDimI(KPhase),0.)
          endif
          do jsym=1,NSymmN(KPhase)
            call multm(rm6(1,jsym,isw,KPhase),x0,x1,NDim(KPhase),
     1                 NDim(KPhase),1)
            if(NDimp.gt.3)
     1        call multm(rm6(1,jsym,isw,KPhase),x0p,x1p,NDim(KPhase),
     2                   NDim(KPhase),1)
            do j=1,NDim(KPhase)
              x2(j)=x1(j)+s6(j,jsym,isw,KPhase)
              if(NDimp.gt.3)
     1          x2p(j)=x1p(j)+s6(j,jsym,isw,KPhase)
            enddo
            do 2980ivt=1,NLattVec(KPhase)
              call SetRealArrayTo(px(nx+1),6-nx,0.)
              do j=1,NDimp
                x3j=x2(j)+vt6(j,ivt,isw,KPhase)
                pom=x0(j)-x3j
                apom=anint(pom)
                if(abs(pom-apom).gt..00001) go to 2980
                if(j.le.3)
     1            px(nx+4-j)=-s6(j,jsym,isw,KPhase)
     2                       -vt6(j,ivt,isw,KPhase)-apom
              enddo
              if(NDimI(KPhase).gt.0.and.MaxUsedKwAll.gt.0) then
                do j=1,NDimI(KPhase)
                  if(NDimp.gt.3) then
                    phi(j)=-x2p(j+3)-vt6(j+3,ivt,isw,KPhase)
                  else
                    phi(j)=-x2 (j+3)-vt6(j+3,ivt,isw,KPhase)
                  endif
                enddo
                do j=1,kmodmx
                  l=KwSym(j,jsym,isw,KPhase)
                  if(l.eq.0) then
                    call FeChybne(-1.,-1.,'the set of modulation '//
     1                            'waves isn''t closed.',
     2                            'Select another set and try again.',
     3                            SeriousError)
                    ErrFlag=1
                    go to 9999
                  endif
                  eps(j)=isign(1,l)
                  l=iabs(l)
                  pom=0.
                  do k=1,NDimI(KPhase)
                    pom=pom+pi2*float(kw(k,l,KPhase))*phi(k)
                  enddo
                  snm(j)=sin(pom)
                  csm(j)=cos(pom)
                enddo
                smp(1,1)=1.
                do j=1,kmezsi
                  if(NDimI(KPhase).eq.1) then
                    call modspec(rms(1,1,j),pb,nms(j),1,1,smp,1,1,
     1                           snm(j),csm(j),eps(j),0)
                  else
                    l=iabs(KwSym(j,jsym,isw,KPhase))
                    call modspec(rms56,pb,nms(1),1,kmezsi,smp,l,j,
     1                           snm(j),csm(j),eps(j),ji)
                  endif
                enddo
              endif
              do l=1,3
                do m=1,3
                  smp(l,m)=rm6(l+NDim(KPhase)*(m-1),jsym,isw,KPhase)
                  if(m.eq.l) then
                    rx(nx+4-l,4-m)=smp(l,m)-1.
                  else
                    rx(nx+4-l,4-m)=smp(l,m)
                  endif
                enddo
              enddo
              call multm(smp,xm(1,i),pxx,3,3,1)
              do j=1,3
                px(nx+4-j)=px(nx+4-j)+xm(j,i)-pxx(j)
              enddo
              call uprspec(rx,px,3,nx,0)
              call multm(RotiMol(1,ji),smp,smpp,3,3,3)
              call multm(smpp,RotMol(1,ji),smp,3,3,3)
              call matinv(smp,smpp,detsmp,3)
              do l=1,3
                do m=1,3
                  smpp(l,m)=smp(l,m)*detsmp
                enddo
              enddo
              call srotb(smp,smp,smpt)
              call srotss(smp,smpp,smps)
              if(ktls(i).gt.0) then
                do l=1,6
                  do m=1,6
                    if(m.eq.l) then
                      rtt(ntt+7-l,7-m)=smpt(l,m)-1.
                    else
                      rtt(ntt+7-l,7-m)=smpt(l,m)
                    endif
                  enddo
                enddo
                call uprspec(rtt,pt,6,ntt,0)
                do l=1,9
                  do m=1,9
                    if(m.eq.l) then
                      rts(nts+10-l,10-m)=smps(l,m)-1.
                    else
                      rts(nts+10-l,10-m)=smps(l,m)
                    endif
                  enddo
                enddo
                call uprspec(rts,pt,9,nts,0)
              endif
              if(NDimI(KPhase).le.0.or.MaxUsedKwAll.le.0) go to 2980
              do j=1,kmezxi
                if(NDimI(KPhase).eq.1) then
                  call modspec(rmx(1,1,j),pb,nmx(j),3,1,smp,1,1,
     1                         snm(j),csm(j),eps(j),0)
                  call modspec(rmr(1,1,j),pb,nmr(j),3,1,smpp,1,1,
     1                         snm(j),csm(j),eps(j),0)
                else
                  l=iabs(KwSym(j,jsym,isw,KPhase))
                  call modspec(rmx56,pb,nmx(1),3,kmezxi,smp,l,j,snm(j),
     1                         csm(j),eps(j),0)
                  call modspec(rmr56,pb,nmr(1),3,kmezxi,smpp,l,j,snm(j),
     1                         csm(j),eps(j),0)
                endif
              enddo
2210          do j=1,kmezbi
                if(NDimI(KPhase).eq.1) then
                  call modspec(rmtt(1,1,j),pb,nmtt(j),6,1,smpt,1,1,
     1                         snm(j),csm(j),eps(j),0)
                else
                  l=iabs(KwSym(j,jsym,isw,KPhase))
                  call modspec(rmtt56,pb,nmx(1),6,kmezbi,smpt,l,j,
     1                         snm(j),csm(j),eps(j),0)
                endif
              enddo
              do j=1,kmezbi
                if(NDimI(KPhase).eq.1) then
                  call modspec(rmts(1,1,j),pb,nmts(j),9,1,smps,1,1,
     1                         snm(j),csm(j),eps(j),0)
                else
                  l=iabs(KwSym(j,jsym,isw,KPhase))
                  call modspec(rmts56,pb,nmx(1),9,kmezbi,smps,l,j,
     1                         snm(j),csm(j),eps(j),0)
                endif
              enddo
2980        continue
          enddo
          k=km+3
          call speceq(rx,px,nx,3,k)
          if(ErrFlag.ne.0) go to 9999
          k=k+3
          if(ktls(i).gt.0) then
            call speceq(rtt,pt,ntt,6,k)
            if(ErrFlag.ne.0) go to 9999
            k=k+6
            call speceq(rtt,pt,ntt,6,k)
            if(ErrFlag.ne.0) go to 9999
            k=k+6
            call speceq(rts,pt,nts,9,k)
            if(ErrFlag.ne.0) go to 9999
            k=k+9
          endif
          if(NDimI(KPhase).le.0.or.MaxUsedKwAll.le.0) go to 4000
          if(NDimI(KPhase).eq.1) then
            if(kmodsi.ne.0) then
              k=k+1
              do j=1,kmezsi
                call speceq(rms(1,1,j),pb,nms(j),2,k)
                if(ErrFlag.ne.0) go to 9999
                k=k+2
              enddo
              k=k+2*(kmodsi-kmezsi)
            endif
            if(kmezxi.gt.0) then
              kwo=2*KModM(2,ji)
              if(kwo.gt.0.and.KFM(2,ji).ne.0) kwo=kwo-2*NDimI(KPhase)
              call SpecEqM(rmx,pb,nmx,3,kmezxi,TypeModFunI,OrthoSelI,
     1                     kwo,k)
              if(ErrFlag.ne.0) go to 9999
              if(kmezxi.ne.kmodxi) k=k+6*(kmodxi-kmezxi)
              call SpecEqM(rmr,pb,nmr,3,kmezxi,TypeModFunI,OrthoSelI,
     1                     kwo,k)
              if(ErrFlag.ne.0) go to 9999
              if(kmezxi.ne.kmodxi) k=k+6*(kmodxi-kmezxi)
            else
              if(kmezxi.ne.kmodxi) k=k+12*(kmodxi-kmezxi)
            endif
            if(kmezbi.gt.0) then
              kwo=2*KModM(3,ji)
              if(kwo.gt.0.and.KFM(3,ji).ne.0) kwo=kwo-2*NDimI(KPhase)
              call SpecEqM(rmtt,pb,nmtt,6,kmezbi,TypeModFunI,OrthoSelI,
     1                     kwo,k)
              if(ErrFlag.ne.0) go to 9999
              if(kmezbi.ne.kmodbi) k=k+12*(kmodbi-kmezbi)
              call SpecEqM(rmtt,pb,nmtt,6,kmezbi,TypeModFunI,OrthoSelI,
     1                     kwo,k)
              if(ErrFlag.ne.0) go to 9999
              if(kmezbi.ne.kmodbi) k=k+12*(kmodbi-kmezbi)
              call SpecEqM(rmts,pb,nmts,9,kmezbi,TypeModFunI,OrthoSelI,
     1                     kwo,k)
              if(ErrFlag.ne.0) go to 9999
              if(kmezbi.ne.kmodbi) k=k+18*(kmodbi-kmezbi)
            endif
          else if(NDimI(KPhase).gt.1) then
            if(kmodsi.gt.0) then
              k=k+1
              if(kmezsi.gt.0) call speceq(rms56,pb,nms(1),2*kmezsi,k)
              if(ErrFlag.ne.0) go to 9999
              k=k+2*kmodsi
            endif
            if(kmezxi.gt.0) then
              call speceq(rmx56,pb,nmx(1),6*kmezxi,k)
              if(ErrFlag.ne.0) go to 9999
              k=k+6*kmodxi
              call speceq(rmr56,pb,nmr(1),6*kmezxi,k)
              if(ErrFlag.ne.0) go to 9999
              k=k+6*kmodxi
            endif
            if(kmezbi.gt.0) then
              call speceq(rmtt56,pb,nmtt(1),12*kmezbi,k)
              if(ErrFlag.ne.0) go to 9999
              k=k+12*kmodxi
              call speceq(rmts56,pb,nmts(1),18*kmezbi,k)
              if(ErrFlag.ne.0) go to 9999
            endif
          endif
4000    continue
        ji=1+(i-1)*mxp
        call atspec(iap,iap+iam(i)/KPoint(i)-1,ji,i,nap)
        if(ErrFlag.ne.0) return
        nap=nap+iam(i)*mam(i)
        iap=iap+iam(i)
      enddo
9999  if(allocated(rx)) deallocate(rx,rtt,rts,px,pt)
      if(NDimI(KPhase).gt.0.and.MaxUsedKwAll.gt.0) then
        if(allocated(pb))
     1    deallocate(pb,snm,csm,eps,nmx,nmr,nms,nmtt,nmts,OrthoSelI)
        if(NDimI(KPhase).eq.1) then
          if(allocated(rmx)) deallocate(rmx,rmr,rms,rmtt,rmts)
        else
          if(allocated(rms56))
     1      deallocate(rms56,rmx56,rmr56,rmtt56,rmts56)
        endif
      endif
      return
      end
