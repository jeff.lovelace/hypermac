      subroutine RefPopv
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension IAtUse(:),KiAtUse(:),OAtUse(:)
      allocatable IAtUse,KiAtUse,OAtUse
      save NAtUse,NAtUsePopv,IAtUse,KiAtUse,OAtUse,SumPopv,KiFix,NAtFix,
     1     Klic
      entry RefPopvPrologRestrain
      Klic=0
      go to 1100
      entry RefPopvPrologEquation
      Klic=1
      go to 1100
      entry RefPopvPrologContinue
      Klic=2
1100  if(allocated(IAtUse)) deallocate(IAtUse,KiAtUse,OAtUse)
      allocate(IAtUse(2*NAtCalc),KiAtUse(2*NAtCalc),OAtUse(2*NAtCalc))
      NAtUse=0
      SumPopv=0.
      SumPopc=0.
      SumProt=0.
      PopvMax=-99999.
      SymmRat=float(NSymm(KPhase)*NLattVec(KPhase))/
     1        float(NUnits(KPhase))
      do i=1,NAtCalc
        kip=PrvniKiAtomu(i)+max(TRankCumul(itf(i)),10)+1
        if(kswa(i).ne.KPhase.or.lasmax(i).le.0.or.ai(i).le.0.) cycle
        NAtUse=NAtUse+1
        IAtUse(NAtUse)=i
        popvp=popv(i)*ai(i)*SymmRat
        SumPopv=SumPopv+popvp
        SumPopc=SumPopc+popc(i)*ai(i)*SymmRat
        SumProt=SumProt+AtNum(isf(i),KPhase)*ai(i)*SymmRat
        OAtUse(NAtUse)=ai(i)*SymmRat
        KiAtUse(NAtUse)=kip
        if(popvp.gt.PopvMax.and.ki(kip).gt.0.and.Klic.lt.2) then
          KiFix=kip
          PopvMax=popvp
          NAtFix=NAtUse
        endif
      enddo
      NAtUsePopv=NAtUse
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or.lasmax(i).le.1.or.ai(i).le.0.) cycle
        kip=PrvniKiAtomu(i)+max(TRankCumul(itf(i)),10)+4
        NAtUse=NAtUse+1
        IAtUse(NAtUse)=i
        popvp=popas(1,i)*ai(i)*SymmRat
        SumPopv=SumPopv+popvp
        OAtUse(NAtUse)=ai(i)
        KiAtUse(NAtUse)=kip
        if(popvp.gt.PopvMax.and.ki(kip).gt.0.and.Klic.lt.2) then
          KiFix=kip
          PopvMax=popvp
          NAtFix=NAtUse
        endif
      enddo
      if(Klic.lt.2) then
        call newln(4)
        write(lst,'(/''Number of electrons in the structural unit:'',
     1        f10.3)') SumPopv+SumPopc
        write(lst,'(''Number of protons   in the structural unit:'',
     1        f10.3)') SumProt
        write(Cislo,'(f15.3)') SumPopv
        call ZdrcniCisla(Cislo,1)
        write(lst,'(''The sum of valence electrons will be fixed to '',
     1              a,'' electrons'')') Cislo(:idel(Cislo))
      endif
      if(Klic.gt.0.and.KiFix.gt.0) ki(KiFix)=0
      go to 9999
      entry RefPopvSuma
      der=0.
!   tato volba vychazi z toho, ze chceme mit SumPov urceno s jistou
!   presnosti
      if(SumPopv.gt.0.) then
        wt=(GOFOverall*100000./SumPopv)**2
      else
        wt=.01
      endif
      Fcalc=0.
      do i=1,NAtUsePopv
        Fcalc=Fcalc+popv(IAtUse(i))*OAtUse(i)
        der(KiAtUse(i))=OAtUse(i)
      enddo
      do i=NAtUsePopv+1,NAtUse
        Fcalc=Fcalc+popas(1,IAtUse(i))*OAtUse(i)
        der(KiAtUse(i))=OAtUse(i)
      enddo
      Fobs=SumPopv
      dy=Fobs-Fcalc
      call DSetAll
      call SumaRefine
      go to 9999
      entry RefPopvRenorm
      pom=0.
      n=0
      do i=1,NAtUsePopv
        n=n+1
        pom=pom+popv(IAtUse(i))*OAtUse(i)
      enddo
      do i=NAtUsePopv+1,NAtUse
        n=n+1
        pom=pom+popas(1,IAtUse(i))*OAtUse(i)
      enddo
      if(n.eq.0) go to 9999
      pom=SumPopv/pom
      do i=1,NAtUsePopv
        popv(IAtUse(i))=popv(IAtUse(i))*pom
      enddo
      do i=NAtUsePopv+1,NAtUse
        popas(1,IAtUse(i))=popas(1,IAtUse(i))*pom
      enddo
      SumPopvP=0.
      do i=1,NAtCalc
        kip=PrvniKiAtomu(i)+max(TRankCumul(itf(i)),10)+1
        if(kswa(i).ne.KPhase.or.lasmax(i).le.0.or.ai(i).le.0.) cycle
        SumPopvP=SumPopvP+popv(i)*ai(i)
      enddo
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or.lasmax(i).le.1.or.ai(i).le.0.) cycle
        kip=PrvniKiAtomu(i)+max(TRankCumul(itf(i)),10)+4
        SumPopvP=SumPopvP+popas(1,i)*ai(i)
      enddo
      go to 9999
      entry RefPopvSet
      if(KiFix.le.0) go to 9999
      pomo=1./OAtUse(NAtFix)
      pom=SumPopv*pomo
      spom=0.
      do i=1,NAtUsePopv
        if(KiAtUse(i).eq.KiFix) cycle
        pomp=OAtUse(i)*pomo
        pom=pom-pomp*popv(IAtUse(i))
        spom=spom+(pomp*spopv(IAtUse(i)))**2
      enddo
      do i=NAtUsePopv+1,NAtUse
        if(KiAtUse(i).eq.KiFix) cycle
        pomp=OAtUse(i)*pomo
        pom=pom-pomp*popas(1,IAtUse(i))
        spom=spom+(pomp*spopas(1,IAtUse(i)))**2
      enddo
      if(NAtFix.le.NAtUsePopv) then
        popv(IAtUse(NAtFix))=pom
        spopv(IAtUse(NAtFix))=sqrt(spom)
      else
        popas(1,IAtUse(NAtFix))=pom
        spopas(1,IAtUse(NAtFix))=sqrt(spom)
      endif
      SumPopvP=0.
      do i=1,NAtUsePopv
        SumPopvP=SumPopvP+OAtUse(i)*popv(IAtUse(i))
      enddo
      do i=NAtUsePopv+1,NAtUse
        SumPopvP=SumPopvP+OAtUse(i)*popas(1,IAtUse(i))
      enddo
      go to 9999
      entry RefPopvDSet
      if(KiFix.le.0) go to 9999
      do i=1,NAtUse
        k=KiAtUse(i)
        if(k.ne.KiFix) der(k)=der(k)-der(KiFix)*OAtUse(i)/OAtUse(NAtFix)
      enddo
      go to 9999
      entry RefPopvEpilog
      if(Klic.gt.0.and.allocated(ki).and.KiFix.gt.0) ki(KiFix)=1
      if(allocated(IAtUse)) deallocate(IAtUse,KiAtUse,OAtUse)
9999  return
      end
