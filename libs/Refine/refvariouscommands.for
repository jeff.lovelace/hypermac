      subroutine RefVariousCommands
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 Veta
      character*30 Label(10)
      data Label/'%Restrictions',
     1           'E%quations',
     2           'Fi%xed commands',
     3           '%Dontuse/Useonly command',
     4           'S%cale command',
     5           'Partial R%Factors',
     6           'D%istance restrains',
     7           '%Angles restrains',
     8           '%Torsion angle restrains',
     9           '%Keep commands'/
      equivalence (IdNumbers( 1),IdEditRestrict),
     1            (IdNumbers( 2),IdEditEquation),
     2            (IdNumbers( 3),IdEditFixed),
     3            (IdNumbers( 4),IdEditDontuse),
     4            (IdNumbers( 5),IdEditScale),
     5            (IdNumbers( 6),IdEditRFactors),
     6            (IdNumbers( 7),IdEditDistFix),
     7            (IdNumbers( 8),IdEditAngleFix),
     8            (IdNumbers( 9),IdEditTorsFix),
     9            (IdNumbers(10),IdEditKeep)
      save nButtFirst,jp
      entry RefVariousCommandsMake(id)
      xqd=XdQuestRef-2.*KartSidePruh
      dpom=220.
      xpom=(xqd-dpom)*.5
      il=3
      Veta='Press the button to edit/create:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      jp=NactiInt+NactiReal+20
      j=jp
      do i=1,10
        il=il+1
        j=j+1
        Veta=Label(i)(:idel(Label(i)))
        if(NactiActive(j)+NactiPassive(j).gt.0) then
          write(Cislo,'(''['',i5,''+'',i5,''!]'')') NactiActive(j),
     1                                              NactiPassive(j)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtRestrict=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtFirst=ButtonLastMade
      enddo
      go to 2500
      entry RefVariousCommandsCheck
      if(CheckType.eq.EventButton) then
        i=CheckNumber-nButtFirst+1
        if(i.eq.IdEditRestrict) then
          call RefRestric
        else if(i.eq.IdEditEquation) then
          call RefEquation
        else if(i.eq.IdEditFixed) then
          call RefFixed
        else if(i.eq.IdEditDontuse) then
          call RefDontuse
        else if(i.eq.IdEditScale) then
          call RefScale
        else if(i.eq.IdEditRFactors) then
          call RefRFactors
        else if(i.eq.IdEditDistFix) then
          call RefRestrain('distfix')
        else if(i.eq.IdEditAngleFix) then
          call RefRestrain('anglefix')
        else if(i.eq.IdEditTorsFix) then
          call RefRestrain('torsfix')
        else if(i.eq.IdEditKeep) then
          call RefKeep
        endif
        j=i+jp
        call UpdateNumberOfCommands(j)
        Veta=Label(i)(:idel(Label(i)))
        if(NactiActive(j)+NactiPassive(j).gt.0) then
          write(Cislo,'(''['',i5,''+'',i5,''!]'')') NactiActive(j),
     1                                              NactiPassive(j)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        call FeQuestButtonLabelChange(CheckNumber,Veta)
      else
        go to 9999
      endif
2500  continue
      go to 9999
      entry RefVariousCommandsUpdate
9999  return
      end
