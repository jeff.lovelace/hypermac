      subroutine RefEquationCheck(Command,ErrString)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*(*) Command,ErrString
      character*256 t256
      character*80  s80
      logical AtomPar
      ErrString=' '
      neq=0
      neqp=1
      t256=Command
      call Vazba(t256)
      if(neqp.gt.neq.or.idel(t256).le.0) then
        ErrString='Syntax error in the equation.'
        go to 9999
      else
        AtomPar=lnp(1).ge.1.or.lnp(1).eq.JeToDelta.or.lnp(1).eq.JeToT40
     1          .or.lnp(1).eq.JeToX40.or.lnp(1).eq.JeToXSlope.or.
     2          lnp(1).eq.JeToYSlope.or.lnp(1).eq.JeToZSlope.or.
     3          lnp(1).eq.JeToXYZMode.or.lnp(1).eq.JeToMagMode
      endif
1000  i1=index(t256,'[')
      if(i1.le.0) go to 9999
      i2=index(t256,']')
      s80=' '
      if(i2.gt.i1+1) s80=t256(i1+1:i2-1)
      if(idel(s80).le.0) then
        ErrString='Atom/Phase/DatBlock name missing.'
        go to 9999
      else
        if(AtomPar) then
          call TestAtomString(s80,IdWildNo,IdAtMolYes,IdMolYes,
     1                        IdSymmNo,IdAtMolMixNo,ErrString)
        else
          call TestScString(s80,ErrString)
        endif
        if(ErrString.ne.' ') go to 9999
      endif
      t256=t256(i2+1:)
      go to 1000
9999  return
      end
