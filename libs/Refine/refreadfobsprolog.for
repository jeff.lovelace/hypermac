      subroutine RefReadFobsProlog(ln)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(*),ihp(6),h(3)
      integer HArr(:,:),SinThLArr(:),Poradi(:)
      logical EqIV
      real IObs,FObsArr(:)
      allocatable HArr,FObsArr,SinThLArr,Poradi
      save nref,nfrom,HArr,FObsArr,SinThLArr,Poradi
      nref=0
      call NastavM90(ln)
1100  read(ln,format91,end=1200,err=9999)
     1  (ihp(i),i=1,maxNDim),Iobs,SigIobs,iq,nxx,itwr
      if(iq.ne.1) go to 1100
      nref=nref+1
      go to 1100
1200  allocate(HArr(maxNDim,nref),FObsArr(nref),SinThLArr(nref),
     1         Poradi(nref))
      nref=0
      call NastavM90(ln)
1300  read(ln,format91,end=1400,err=9999)
     1  (ihp(i),i=1,maxNDim),Iobs,SigIobs,iq,nxx,itwr
      if(iq.ne.1) go to 1300
      nref=nref+1
      FobsArr(nref)=sqrt(max(IObs,0.))
      call CopyVekI(ihp,HArr(1,nref),maxNDim)
      call FromIndSinthl(ihp,h,sinthl,sinthlq,1,0)
      SinThLArr(nref)=nint(100000.*sinthl)
      go to 1300
1400  call indexx(nref,SinThLArr,Poradi)
      nfrom=1
      call CloseIfOpened(ln)
      go to 9999
      entry RefReadFobs(ih,Fobs)
      do j=nfrom,nref
        n=Poradi(j)
        do i=1,NSymm(KPhase)
          call MultmIRI(HArr(1,n),rm6(1,i,1,KPhase),ihp,1,NDim(KPhase),
     1                  NDim(KPhase))
          if(EqIV(ihp,ih,NDim(KPhase))) then
            nfrom=j
            go to 2000
          endif
        enddo
      enddo
      go to 9000
      entry RefReadFobsEpilog
      if(allocated(HArr)) deallocate(HArr,FObsArr,SinThLArr,Poradi)
      go to 9999
2000  Fobs=FobsArr(n)
      go to 9999
9000  Fobs=0.
9999  return
      end
