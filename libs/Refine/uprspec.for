      subroutine uprspec(rx,px,n,nx,ji)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rx(2*n,n),px(2*n)
      dimension rxp(:),pxp(:),ipxx(:),ipor(:)
      allocatable rxp,pxp,ipxx,ipor
      m=2*n
      allocate(rxp(n*m),pxp(m),ipxx(m),ipor(m))
      if(ji.gt.0) then
        call CopyVek(rx,rxp,n*m)
        call CopyVek(px,pxp,m)
        k=nx
        do i=1,n
          k=k+1
          ipxx(i)=0.
          do j=1,n
            ipxx(i)=ipxx(i)-iabs(nint(100000.*rx(k,j)))
          enddo
        enddo
        call indexx(n,ipxx,ipor)
        id=nx
        do i=1,n
          k=ipor(i)+id
          l=i+id
          px(l)=pxp(k)
          do j=1,n
            rx(l,j)=rxp(k)
            k=k+m
          enddo
        enddo
      endif
      call triangl(rx,px,m,n,nx)
      deallocate(rxp,pxp,ipxx,ipor)
      return
      end
