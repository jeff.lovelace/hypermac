      integer function AtomUseXYZModes(At)
      use Atoms_mod
      character*(*) At
      logical EqIgCase
      do i=1,NAtXYZMode
        do j=1,MAtXYZMode(i)
          if(EqIgCase(At,Atom(IAtXYZMode(j,i)))) then
            AtomUseXYZModes=i
            go to 9999
          endif
        enddo
      enddo
      AtomUseXYZModes=0
9999  return
      end
