      function RefSmbConst(Const,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Const
      character*5 FormatPom
      logical   EqIgCase
      dimension ip(4)
      data FormatPom/'(4i1)'/
      ich=0
      RefSmbConst=1.
      if(EqIgCase(Const(1:2),'cq')) then
        id=idel(Const)-2
        if(id.ge.1.and.id.le.4) then
          write(FormatPom(2:2),'(i1)') id
          call SetIntArrayTo(ip,4,1)
          read(Const(3:id+3),FormatPom,err=9000)(ip(i),i=1,id)
          do i=1,4
            if(ip(i).lt.1) go to 9000
          enddo
          if(ip(1).gt.3.or.ip(2).gt.MaxNDimI.or.
     1       ip(3).gt.MaxNComp.or.ip(4).gt.NPhase) go to 9000
          RefSmbConst=qu(ip(1),ip(2),ip(3),ip(4))
        else
          go to 9000
        endif
      endif
      go to 9999
9000  ich=1
9999  return
100   format(3i1)
      end
