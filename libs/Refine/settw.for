      subroutine settw(ih,ihitw,itwri,ranover)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ih(6),ihitw(6,*),hf(6,96),c(3),difhp(3,96),ihp(6),
     1          hp(6),hpp(6),MMin(3),MMax(3)
      logical ranover
      ranover=.false.
      iatwr=iabs(itwri)
      MMax=0
      do i=1,NDimI(KPhase)
        MMax(i)=mmaxtw
      enddo
      MMin=-MMax
      call ChngInd(hf(1,iatwr),ih,1,difh,MMin,MMax,0,CheckExtRefYes)
      if(iatwr.ne.1) call multm(hf(1,iatwr),rtwi(1,iatwr),hf(1,1),1,3,3)
      do 1200j=1,NTwin
        KPhase=KPhaseTwin(j)
        call SetIntArrayTo(ihitw(1,j),maxNDim,0)
        call SetIntArrayTo(ihp,maxNDim,0)
        call multm(hf(1,1),rtw(1,j),hf(1,j),1,3,3)
        do isw=1,NComp(KPhase)
          call multm(hf(1,j),zsigi(1,isw,KPhase),hp,1,3,3)
          call ChngInd(hp,ihp,isw,difhp(1,j),MMin,MMax,1,CheckExtRefYes)
          if(nchkr.le.0) then
            if(itwri.lt.0.and.j.ne.iatwr) then
              ihp(1)=999
              go to 1100
            endif
            call FromIndSinthl(ih,difhp(1,j),pom,dpom,isw,1)
            pom=pom*LamAve(1)
            if(abs(pom).gt..999) then
              pom=2.*TwDiff
            else
              pom=asin(pom)/torad
            endif
            if(pom.gt.TwMax) then
              ihp(1)=999
            else if(pom.gt.TwDiff) then
              do i=1,NTwin
                ihitw(1,i)=999
              enddo
              go to 9999
            endif
          endif
1100      if(ihp(1).lt.900) then
            do i=1,NDim(KPhase)
              hp(i)=ihp(i)
            enddo
            call Multm(hp,zv(1,isw,KPhase),hpp,1,NDim(KPhase),
     1                 NDim(KPhase))
            if(j.eq.iatwr) then
              call CopyVekI(ih,ihitw(1,j),NDim(KPhase))
            else
              do i=1,NDim(KPhase)
                ihitw(i,j)=nint(hpp(i))
              enddo
            endif
            go to 1200
          else
            call CopyVekI(ihp,ihitw(1,j),NDim(KPhase))
          endif
        enddo
1200  continue
      if(nchkr.gt.0) then
        do j=1,NTwin
          do i=1,NDim(KPhase)
            hf(i,j)=ihitw(i,j)
          enddo
        enddo
        call multm(ormat(1,1,iatwr),hf(1,iatwr),c,3,3,1)
        pom=sqrt(c(1)**2+c(2)**2+c(3)**2)
        chi1=atan2(c(3),sqrt(c(1)**2+c(2)**2))/torad
        fi1=atan2(-c(1),c(2))
        sth1=.5*LamAve(1)*pom
        th1=asin(sth1)/torad
        sth1=sth1*5.75877
        do 1800j=1,NTwin
          if(j.eq.iatwr) go to 1800
          call multm(ormat(1,1,j),hf(1,j),c,3,3,1)
          pom=sqrt(c(1)**2+c(2)**2+c(3)**2)
          chi2=atan2(c(3),sqrt(c(1)**2+c(2)**2))/torad
          fi2=atan2(-c(1),c(2))
          th2=asin(.5*LamAve(1)*pom)/torad
          dom=abs(asin(-sin(fi1-fi2)*cos(chi2*torad))/torad)
          if(dom.gt.omdif(1).or.abs(th1-th2).gt.thdif(1).or.
     1       abs(chi1-chi2)*sth1.gt.chidif(1)) then
            ihitw(1,j)=999
            ranover=dom.lt.omdif(2).and.abs(th1-th2).lt.thdif(2).and.
     1              abs(chi1-chi2)*sth1.lt.chidif(2)
          endif
1800    continue
      endif
9999  return
      end
