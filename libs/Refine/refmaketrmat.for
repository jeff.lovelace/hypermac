      subroutine RefMakeTrMat(u,v,ip1,ip2,trp,trpi)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension trp(3,3),trpi(9),xp(3,3),u(3),v(3)
      call multm(TrToOrtho(1,1,KPhase),u,xp(1,ip1),3,3,1)
      call VecOrtNorm(xp(1,ip1),3)
      call multm(TrToOrtho(1,1,KPhase),v,xp(1,ip2),3,3,1)
      pom=VecOrtScal(xp(1,ip1),xp(1,ip2),3)
      do i=1,3
        xp(i,ip2)=xp(i,ip2)-xp(i,ip1)*pom
      enddo
      call VecOrtNorm(xp(1,ip2),3)
      ip3=6-ip1-ip2
      call VecMul(xp(1,ip1),xp(1,ip2),xp(1,ip3))
      call VecOrtNorm(xp(1,ip3),3)
      if((ip1.eq.1.and.ip2.eq.3).or.(ip1.eq.2.and.ip2.eq.1).or.
     1   (ip1.eq.3.and.ip2.eq.2))
     2   call RealVectorToOpposite(xp(1,ip3),xp(1,ip3),3)
      call MatInv(xp,trpi,pom,3)
      call multm(trpi,TrToOrtho(1,1,KPhase),trp,3,3,3)
      call MatInv(trp,trpi,pom,3)
      return
      end
