      subroutine smod(u,ax,ay,daxddlt,dayddlt,daxdfct,daydfct,daxdx40,
     1                daydx40,dlt,x40,fct,m,ityp)
      include 'fepc.cmn'
      include 'basic.cmn'
      real ksi
      if(ityp.eq.1) then
        arg2=pi2*float(m)
        arg=arg2*.5
        alfa=arg*dlt
        ksi= arg2*x40
        csa=2.*cos(alfa)
        sna=2.*sin(alfa)
        u=sna/arg
        csk=cos(ksi)
        snk=sin(ksi)
        ax=ax+u*snk
        ay=ay+u*csk
        daxdfct= csa*snk*dlt
        daydfct= csa*csk*dlt
        daxddlt=daxddlt+csa*snk
        dayddlt=dayddlt+csa*csk
        daxdx40= u*csk*arg2
        daydx40=-u*snk*arg2
      endif
      return
      end
