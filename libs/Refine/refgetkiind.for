      integer function RefGetKiInd(ii)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      i=0
      do k=1,PosledniKiAtInd
        if(ki(k).eq.0) cycle
        i=i+1
        if(i.eq.ii) go to 9999
      enddo
      do k=PrvniKiAtMol,PosledniKiAtMol
        if(ki(k).eq.0) cycle
        i=i+1
        if(i.eq.ii) go to 9999
      enddo
      do k=PrvniKiMol,PosledniKiMol
        if(ki(k).eq.0) cycle
        i=i+1
        if(i.eq.ii) go to 9999
      enddo
      do k=PrvniKiAtXYZMode,PosledniKiAtXYZMode
        if(ki(k).eq.0) cycle
        i=i+1
        if(i.eq.ii) go to 9999
      enddo
      do k=PrvniKiAtMagMode,PosledniKiAtMagMode
        if(ki(k).eq.0) cycle
        i=i+1
        if(i.eq.ii) go to 9999
      enddo
      k=0
9999  RefGetKiInd=k
      return
      end
