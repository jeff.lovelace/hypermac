      logical function PolarAxis()
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension rx(6,3),px(6)
      integer CrlCentroSymm
      call SetRealArrayTo(rx,18,0.)
      call SetRealArrayTo(px,6,0.)
      if(CrlCentroSymm().gt.0) then
        PolarAxis=.false.
      else
        nx=0
        do i=1,NSymmN(KPhase)
          do j=1,3
            do k=1,3
              if(k.eq.j) then
                rx(nx+4-j,4-k)=rm6(j+NDim(KPhase)*(k-1),i,1,KPhase)-1.
              else
                rx(nx+4-j,4-k)=rm6(j+NDim(KPhase)*(k-1),i,1,KPhase)
              endif
            enddo
          enddo
          call triangl(rx,px,6,3,nx)
          if(nx.ge.3) then
            PolarAxis=.false.
            go to 9999
          endif
        enddo
        PolarAxis=.true.
      endif
9999  return
      end
