      subroutine ChangeMolCompress(PosIn,PosOut,N,Key,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PosIn,PosOut,CumulCompress(8)
      ich=0
      if(ktls((N-1)/mxp+1).le.0) then
        CumulCompress(1)=7
      else
        CumulCompress(1)=28
      endif
      if(NDim(KPhase).gt.3) then
        if(KModM(1,N).gt.0) then
          CumulCompress(2)=CumulCompress(1)+1+2*KModM(1,N)
        else
          CumulCompress(2)=CumulCompress(1)
        endif
        CumulCompress(3)=CumulCompress(2)+ 6*KModM(2,N)
        CumulCompress(4)=CumulCompress(3)+ 6*KModM(2,N)
        CumulCompress(5)=CumulCompress(4)+12*KModM(3,N)
        CumulCompress(6)=CumulCompress(5)+12*KModM(3,N)
        CumulCompress(7)=CumulCompress(6)+18*KModM(3,N)
        if(CumulCompress(1).lt.CumulCompress(7)) then
          CumulCompress(8)=CumulCompress(7)+1
        else
          CumulCompress(8)=CumulCompress(7)
        endif
      endif
      if(Key.eq.0) then
        if(NDim(KPhase).eq.3) then
          if(PosIn.le.CumulCompress(1)) then
            PosOut=PosIn
          else
            go to 9000
          endif
        else
          do i=1,8
            if(PosIn.le.CumulMol(i)) then
              if(i.eq.1) then
                PosOut=PosIn
                go to 9999
              else
                go to 1500
              endif
            endif
          enddo
1500      PosOut=CumulCompress(i-1)+PosIn-CumulMol(i-1)
          if(PosOut.gt.CumulCompress(i)) go to 9000
        endif
      else
        if(NDim(KPhase).eq.3) then
          PosOut=PosIn
        else
          do i=1,8
            if(PosIn.le.CumulCompress(i)) then
              if(i.eq.1) then
                PosOut=PosIn
                go to 9999
              else
                go to 2500
              endif
            endif
          enddo
2500      PosOut=CumulMol(i-1)+PosIn-CumulCompress(i-1)
        endif
      endif
      go to 9999
9000  ich=1
9999  return
      end
