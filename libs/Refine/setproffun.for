      subroutine SetProfFun(ih,h3,th)
      use Refine_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension coft(6),cofn(3),ih(6),dtdgi(6),dtdrcp(6),dtdh3(3),
     1          Wx(-25:25),Wy(-25:25),D(25),DerLorentz(5),DerGauss(4),
     2          h3(3),h4(4),DerBroadHKLLorentz(10),DerBroadHKLGauss(10)
      logical Poprve,ReflFulfilsCond
      real :: ln2 = .693147
      data coft/1.,2.69269,2.42843,4.47163,0.07842,1./,
     1     cofn/1.36603,-.47719,.11116/
      data NBeta,FineStep/30,.1/
      BroadHKL=1.
      iBroadHKL=0
      do i=1,NBroadHKLPwd(KPhase,KDatBlock)
        if(ReflFulfilsCond(ih,HMBroadHKLPwd(1,i,KPhase,KDatBlock),
     1                     HMABroadHKLPwd(1,i,KPhase,KDatBlock),
     2                     HVBroadHKLPwd(1,i,KPhase,KDatBlock),
     4                     ModVBroadHKLPwd(i,KPhase,KDatBlock),
     3                     EqVBroadHKLPwd(i,KPhase,KDatBlock),
     5                     HNBroadHKLPwd(1,i,KPhase,KDatBlock),
     7                     ModNBroadHKLPwd(i,KPhase,KDatBlock),
     6                     EqNBroadHKLPwd(i,KPhase,KDatBlock))) then
          BroadHKL=BroadHKLPwd(i,KPhase,KDatBlock)
          iBroadHKL=i
          DerBroadHKLLorentz=0.
          DerBroadHKLGauss=0.
          exit
        endif
      enddo
      if(isTOF) then
        dd=.5/sinthl
        ddrcp=1./dd
        dd2rcp=ddrcp**2
        dd4rcp=ddrcp**4
        dd2=dd**2
        dd3=dd2*dd
        dd4=dd2**2
        dd6=dd3**2
        pomc=.001
        shift=0.
        if(KUseTOFJason(KDatBlock).gt.0) then
          CrossArg=ShiftPwd(6,KDatBlock)*
     1             (ShiftPwd(7,KDatBlock)-ddrcp)
          fn=.5*erfc(CrossArg)
          TE=ShiftPwd(4,KDatBlock)+ShiftPwd(5,KDatBlock)*dd
          TT=ShiftPwd(1,KDatBlock)+ShiftPwd(2,KDatBlock)*dd
     1      -ShiftPwd(3,KDatBlock)*ddrcp
        endif
      else if(isED) then
        tt2=th**2
        tt =th
        pomc=1.
        shift=pomc*shiftPwd(1,KDatBlock)
      else
        peak=th*2.
        if(abs(th/torad-.785398).lt..001) then
          cth=0.
        else
          cth=1./tan(peak)
        endif
        csth=cos(th)
        sinth=sin(th)
        sin2th=sin(peak)
        cos2th=cos(peak)
        cos2thq=cos2th**2
        tth=tan(th)
        cotgth=1./tth
        tthq=tth**2
        pomc=.01*torad
        shift=pomc*(shiftPwd(1,KDatBlock)+shiftPwd(2,KDatBlock)*csth+
     1              shiftPwd(3,KDatBlock)*sin2th)
      endif
      pomq=pomc**2
      pcosi=abs(cosHKL(h3,DirBroad(1,KPhase,KDatBlock)))
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        IZdvih=(KPhase-1)*NParCellProfPwd+(KDatBlock-1)*NParProfPwd
        if(isTOF.or.isED) then
          poms=.0001
        else
          poms=(.25/sinthlq)**2
        endif
        if(NDimI(KPhase).eq.1) then
          n=4
        else
          n=3
        endif
        pcosiqt=0.
        m4=0
        if(n.eq.4) then
          call CopyVek(h3,h4,3)
          h4(4)=ih(4)
        else
          call CopyVek(h3,h4,3)
        endif
        do i1=1,n
          pom1=h4(i1)*poms
          do i2=i1,n
            pom2=pom1*h4(i2)
            do i3=i2,n
              pom3=pom2*h4(i3)
              do i4=i3,n
                pom4=pom3*h4(i4)
                if(n.ne.4) then
                  pom=cmlt(m4+11)*pom4
                else
                  pom=cmlt4(m4+1)*pom4
                endif
                if(NLamPwd.ne.0)
     1            CoefP(m4+IStPwd+IZdvih,NLamPwd,NRefPwd)=pom
                m4=m4+1
                pcosiqt=pcosiqt+StPwd(m4,KPhase,KDatBlock)*pom
              enddo
            enddo
          enddo
        enddo
        if(n.eq.3) then
          do i=1,NDimI(KPhase)
            pom=poms*float(ih(3+i))**4
            CoefP(m4+IStPwd+IZdvih,NLamPwd,NRefPwd)=pom
            m4=m4+1
            pcosiqt=pcosiqt+StPwd(m4,KPhase,KDatBlock)*pom
          enddo
        endif
        if(pcosiqt.ge.0.) then
          ZnakStrain= 1.
        else
          ZnakStrain=-1.
!          ZnakStrain=0.
        endif
        pcosiqt=ZnakStrain*pcosiqt
        pcosit=sqrt(pcosiqt)
      else
        pcosit=0.
        pcosiqt=0.
      endif
      gam=0.
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        call SetRealArrayTo(DerLorentz,5,0.)
        if(isTOF) then
          DerLorentz(1)=1.
          DerLorentz(2)=dd
          DerLorentz(3)=dd2
          gam=0.
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
            ik=5
            DerLorentz(4)=dd *pcosi
            DerLorentz(5)=dd2*pcosi
          else
            ik=3
          endif
          do i=1,ik
            gam=gam+LorentzPwd(i,KPhase,KDatBlock)*DerLorentz(i)
          enddo
        else
          ik=4
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
            pom=LorentzPwd(1,KPhase,KDatBlock)
            if(LorentzPwd(2,KPhase,KDatBlock).ne.0.) then
              poma=LorentzPwd(2,KPhase,KDatBlock)
              pom=pom+pcosi*poma
            endif
            if(pom.ne.0.) then
              Clen=LorentzToCrySize(KDatBlock)/(pom*csth)
              gam=gam+Clen
              DerLorentz(1)=-Clen/pom
              DerLorentz(2)=-Clen/pom*pcosi
            endif
            pom=LorentzPwd(3,KPhase,KDatBlock)
            if(LorentzPwd(4,KPhase,KDatBlock).ne.0.) then
              poma=LorentzPwd(4,KPhase,KDatBlock)
              pom=pom+pcosi*poma
            endif
            Clen=tth/LorentzToStrain
            gam=gam+pom*Clen
            DerLorentz(3)=Clen
            DerLorentz(4)=Clen*pcosi
          else
!            DerLorentz(1)=1./csth
!            DerLorentz(2)=pcosi/csth
!            DerLorentz(3)=tth
!            DerLorentz(4)=pcosi*tth
            DerLorentz(1)=BroadHKL/csth
            DerLorentz(2)=BroadHKL*pcosi/csth
            DerLorentz(3)=BroadHKL*tth
            DerLorentz(4)=BroadHKL*pcosi*tth
            do i=1,4
              if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
                gam=gam+
     1              (LorentzPwd(i,KPhase,KDatBlock)*DerLorentz(i))**2
              else
                pom=LorentzPwd(i,KPhase,KDatBlock)*DerLorentz(i)
                gam=gam+pom
                if(IBroadHKL.gt.0.and.BroadHKL.ne.0.)
     1            DerBroadHKLLorentz(IBroadHKL)=
     2              DerBroadHKLLorentz(IBroadHKL)+pom/BroadHKL
              endif
            enddo
            if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
              gam1=gam
              if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
                gam2=(( AsymPwd(2,KDatBlock)**2+
     1                 (AsymPwd(3,KDatBlock)*cos2th)**2+
     2                 (AsymPwd(4,KDatBlock)*sin2th)**2)/
     3                  AsymPwd(6,KDatBlock)**2+
     4                (2.*AsymPwd(5,KDatBlock))**2+
     5                (2.*AsymPwd(1,KDatBlock)*tth)**2)*.1/ToRad
              else
                gam2=0.
              endif
              gam=sqrt(gam1+gam2)
              gam1=gam
            endif
          endif
        endif
      endif
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
          ZetaL=0.
          ZetaG=1.
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1          KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
          ZetaL=1.
          ZetaG=0.
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          ZetaL=ZetaPwd(KPhase,KDatBlock)
          ZetaG=(1.-ZetaL)**2
        endif
        if(isTOF) then
          gam=gam+pcosit*ZetaL*dd3
        else if(isED) then
          gam=gam
        else
          gam=gam+pcosit*ZetaL*tth
        endif
      endif
      if(gam.ge.0.) then
        ZnakGam= 1.
      else
        ZnakGam=-1.
      endif
      sig=0.
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        call SetRealArrayTo(DerGauss,4,0.)
        if(isTOF) then
          DerGauss(1)=1.
          DerGauss(2)=dd2
          DerGauss(3)=dd4
          sig=GaussPwd(1,KPhase,KDatBlock)+
     1        GaussPwd(2,KPhase,KDatBlock)*dd2+
     2        GaussPwd(3,KPhase,KDatBlock)*dd4
          sigpp=sig
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor)
     1      sig=sig+ZetaG*pcosiqt*dd6
        else if(isED) then
          DerGauss(1)=tt2
          DerGauss(2)=tt
          DerGauss(3)=1.
          sig=GaussPwd(1,KPhase,KDatBlock)*tt2+
     1        GaussPwd(2,KPhase,KDatBlock)*tt+
     2        GaussPwd(3,KPhase,KDatBlock)
        else
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
            pom=GaussPwd(1,KPhase,KDatBlock)
            if(pom.ne.0.) then
              Clen=(GaussToCrySize(KDatBlock)/(pom*csth))**2
              sig=sig+Clen
              DerGauss(1)=-2.*Clen/pom
            else
              DerGauss(1)=0.
            endif
            Clen=tth/GaussToStrain
            pom=GaussPwd(3,KPhase,KDatBlock)
            Clen=(pom*Clen)**2
            sig=sig+Clen
            if(pom.ne.0.) then
              DerGauss(3)=2.*Clen/pom
            else
              DerGauss(3)=0.
            endif
          else
!            DerGauss(1)=tthq
!            DerGauss(2)=tth
!            DerGauss(3)=1.
            pom=BroadHKL**2
            DerGauss(1)=pom*tthq
            DerGauss(2)=pom*tth
            DerGauss(3)=pom*1.
            DerGauss(4)=pom/csth**2
            sig=0.
            do i=1,3
              pom=GaussPwd(i,KPhase,KDatBlock)*DerGauss(i)
              sig=sig+pom
              if(IBroadHKL.gt.0.and.BroadHKL.ne.0.)
     1          DerBroadHKLGauss(IBroadHKL)=
     2            DerBroadHKLGauss(IBroadHKL)+2.*pom/BroadHKL
            enddo
            if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
              sig=sig+ZetaG*pcosiqt*tthq
            else
              sig=sig+GaussPwd(4,KPhase,KDatBlock)/csth**2
            endif
          endif
        endif
      endif
      if(Sig.ge.0.) then
        ZnakSig= 1.
      else
        ZnakSig=-1.
      endif
      gam=pomc*gam*ZnakGam
      sig=pomq*sig*ZnakSig
      sqsg=sqrt(sig)
      fwhg=2.35482*sqsg
      fwhmgl=fwhg+gam
      if(isTOF) then
        fnorm=1.
        if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          Alpha12=(AsymPwd(1,KDatBlock)+AsymPwd(2,KDatBlock)*ddrcp)*
     1             1000.
          Beta12=(AsymPwd(3,KDatBlock)+AsymPwd(4,KDatBlock)*dd4rcp)*
     1            1000.
        else if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
          AlphaE=AsymPwd(1,KDatBlock)+AsymPwd(2,KDatBlock)*dd
          if(AlphaE.ge.0.) then
            ZnAlphaE= 1.
          else
            ZnAlphaE=-1.
            AlphaE=-AlphaE
          endif
          BetaE=AsymPwd(3,KDatBlock)+AsymPwd(4,KDatBlock)*dd
          if(BetaE.ge.0.) then
            ZnBetaE= 1.
          else
            ZnBetaE=-1.
            BetaE=-BetaE
          endif
          AlphaT=AsymPwd(5,KDatBlock)-AsymPwd(6,KDatBlock)*ddrcp
          if(AlphaT.ge.0.) then
            ZnAlphaT= 1.
          else
            ZnAlphaT=-1.
            AlphaT=-AlphaT
          endif
          BetaT =AsymPwd(7,KDatBlock)-AsymPwd(8,KDatBlock)*ddrcp
          if(BetaT.ge.0.) then
            ZnBetaT= 1.
          else
            ZnBetaT=-1.
            BetaT=-BetaT
          endif
          Alpha12=1000./(fn*AlphaE+(1.-fn)*AlphaT)
          Beta12 =1000./(fn*BetaE +(1.-fn)*BetaT)
        endif
      else if(isED) then
        fnorm=1.
        ntsim=1
        fnorm=1.
        tntsim=1.
        pcot=0.
      else
        pcot=0.
        if(KAsym(KDatBlock).eq.IdPwdAsymNone.or.
     1     KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
          ntsim=1
          fnorm=1.
          tntsim=1.
        else if(KAsym(KDatBlock).eq.IdPwdAsymSimpson) then
          pcot=AsymPwd(1,KDatBlock)*cth*pomc
          kt=max(1,ifix(2.5*abs(pcot/fwhmgl)))
          ntsim=2*kt+1
          fnorm=1./float(6*kt)
          tntsim=float((ntsim-1)**2)
        else if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
          cotg2th=cth
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          fnorm=1.
          DelR6=RadStepPwd*.166667
          if(CalcDer) then
            lk=7
          else
            lk=0
          endif
          if(CalcDer)
     1      call SetRealArrayTo(DerAxDivProfA(-25,1,NLamPwd,NRefPwd),
     2                          357,0.)
          do l=0,lk
            if(l.eq.0.or.l.gt.2) then
              if(l.gt.0) then
                if(AsymKi(l,KDatBlock).eq.0) cycle
                Old=AsymPwd(l,KDatBlock)
                AsymPwd(l,KDatBlock)=AsymPwd(l,KDatBlock)*1.1
                deltar=1./(AsymPwd(l,KDatBlock)-Old)
              endif
              call PwdAxialDiv(Peak/ToRad,AsymPwd(3,KDatBlock),
     1          AsymPwd(4,KDatBlock),AsymPwd(5,KDatBlock),
     2          RadPrim(KDatBlock),AsymPwd(6,KDatBlock),
     3          KUsePSoll(KDatBLock).eq.1,
     4          AsymPwd(7,KDatBlock),
     6          KUsePSoll(KDatBLock).eq.1,
     7          NBeta,51,RadStepPwd*FineStep,DegStepPwd,Wy(-25))
              if(l.eq.0) then
                call CopyVek(Wy(-25),AxDivProfA(-25,NLamPwd,NRefPwd),51)
                call CopyVek(Wy(-25),Wx(-25),51)
              else
                AsymPwd(l,KDatBlock)=Old
                do i=-25,25
                  DerAxDivProfA(i,l,NLamPwd,NRefPwd)=(Wy(i)-Wx(i))*
     1                                               deltar
                enddo
              endif
            endif
            if(KUseRSW(KDatBlock).ne.0.and.(l.eq.0.or.l.gt.2)) then
              Poprve=.true.
1112          EpsRSW=AsymPwd(1,KDatBlock)/RadPrim(KDatBlock)
              limRSW=EpsRSW/(2.*RadStepPwd)
              pomRSW=RadStepPwd/EpsRSW
              pom=EpsRSW/(2.*RadStepPwd)-float(limRSW)
              gamma1=.5*(pom-1.)**2
              gamma2=.5*pom**2
              if(l.eq.0) then
                if(Poprve) call CopyVek(AxDivProfA(-25,NLamPwd,NRefPwd),
     1                                             Wy(-25),51)
              else
                call CopyVek(DerAxDivProfA(-25,l,NLamPwd,NRefPwd),
     1                                     Wy(-25),51)
              endif
              do i=-25,25
                jp=max(-25,i-limRSW)
                jk=min( 25,i+limRSW)
                Suma=0.
                do j=jp,jk
                  Suma=Suma+Wy(j)
                enddo
                k=i+limRSW
                if(k.le.25) then
                  Suma=Suma-gamma1*Wy(k)
                  k=k+1
                  if(k.le.25) Suma=Suma+gamma2*Wy(k)
                endif
                k=i-limRSW
                if(k.ge.-25) then
                  Suma=Suma-gamma1*Wy(k)
                  k=k-1
                  if(k.ge.-25) Suma=Suma+gamma2*Wy(k)
                endif
                Suma=pomRSW*Suma
                if(l.eq.0) then
                  if(Poprve) then
                    AxDivProfA(i,NLamPwd,NRefPwd)=Suma
                  else
                    DerAxDivProfA(i,1,NLamPwd,NRefPwd)=
     1                (Suma-AxDivProfA(i,NLamPwd,NRefPwd))*deltar
                  endif
                else
                  DerAxDivProfA(i,l,NLamPwd,NRefPwd)=Suma
                endif
              enddo
              if(l.eq.0) then
                if(Poprve) then
                  Old=AsymPwd(1,KDatBlock)
                  AsymPwd(1,KDatBlock)=AsymPwd(1,KDatBlock)*1.1
                  deltar=1./(AsymPwd(1,KDatBlock)-Old)
                  Poprve=.false.
                  go to 1112
                else
                  AsymPwd(1,KDatBlock)=Old
                endif
              endif
            endif
            if(KUseDS(KDatBlock).ne.0.and.(l.eq.0.or.l.gt.2)) then
              Poprve=.true.
1130          if(KUseDS(KDatBlock).eq.2) then
                EpsDS=abs(-sin2th*(AsymPwd(2,KDatBlock)/
     1                             (2.*ToRad*RadSec(KDatBlock)))**2)
                RelDS=abs(EpsDS)/RadStepPwd
              else
                EpsDS=abs(-.5*cotgth*AsymPwd(2,KDatBlock)**2)
                RelDS=abs(EpsDS)/RadStepPwd
              endif
              if(l.eq.0) then
                if(Poprve) call CopyVek(AxDivProfA(-25,NLamPwd,NRefPwd),
     1                                  Wy(-25),51)
              else
                call CopyVek(DerAxDivProfA(-25,l,NLamPwd,NRefPwd),
     1                                     Wy(-25),51)
              endif
              if(RelDS.gt.1.) then
                NDS=min(ifix(RelDS),24)
                pom=.5/sqrt(EpsDS*RadStepPwd)
                do i=1,NDS+1
                  D(i)=pom/sqrt(float(i))
                enddo
                q=.666667/sqrt(RelDS)
                pomy1=(RelDS-float(NDS))*RadStepPwd
                pomy2=pomy1**2
                eta3=pomy1**3*.333333
                eta1=pomy1-pomy2+eta3
                eta2=pomy2*.5-eta3
                do i=-25,25
                  if(i.le.24) then
                    Suma=q*(Wy(i)+.5*Wy(i+1))
                  else
                    Suma=q*Wy(i)
                  endif
                  if(i.eq.-25) then
                    Bim1=0.
                    Bi  =0.
                    m=i+1
                    do k=2,NDS
                      m=m+1
                      pom=D(k)
                      if(m.le.26) Bim1=Bim1+pom*Wy(m-1)
                      if(m.le.25) Bi  =Bi  +pom*Wy(m)
                    enddo
                  else
                    Bim1=Bi
                    Bi  =Bip1
                  endif
                  Bip1=0.
                  m=i+2
                  do k=2,min(NDS,25-i-1)
                    m=m+1
                    Bip1=Bip1+D(k)*Wy(m)
                  enddo
                  CorrMinus=0.
                  k=i+NDS
                  if(k.le.25) then
                    CorrMinus=CorrMinus+2.*Wy(k)
                    k=k+1
                    if(k.le.25) CorrMinus=CorrMinus+Wy(k)
                  endif
                  CorrPlus=0.
                  k=i+1
                  if(k.le.25) then
                    CorrPlus=CorrPlus+2.*Wy(k)
                    k=k+1
                    if(k.le.25) CorrPlus=CorrPlus+Wy(k)
                  endif
                  Suma=Suma+DelR6*(Bim1+Bip1+4.*Bi-D(NDS)*CorrMinus
     1                                            +D(1)  *CorrPlus)
                  k=i+NDS
                  if(k.le.25) then
                    Suma=Suma+Wy(k)*(eta1*D(NDS)+eta2*D(NDS+1))
                    k=k+1
                    if(k.le.25)
     1                Suma=Suma+Wy(k)*(eta2*D(NDS)+eta3*D(NDS+1))
                  endif
                  if(l.eq.0) then
                    if(Poprve) then
                      AxDivProfA(i,NLamPwd,NRefPwd)=Suma
                    else
                      DerAxDivProfA(i,2,NLamPwd,NRefPwd)=
     1                  (Suma-AxDivProfA(i,NLamPwd,NRefPwd))*deltar
                    endif
                  else
                    DerAxDivProfA(i,l,NLamPwd,NRefPwd)=Suma
                  endif
                enddo
              else
                pomDS=RelDS/3.
                do i=-25,25
                  if(i.le.24) then
                    Suma=(1.-pomDS)*Wy(i)+pomDS*Wy(i+1)
                  else
                    Suma=(1.-pomDS)*Wy(i)
                  endif
                  if(l.eq.0) then
                    if(Poprve) then
                      AxDivProfA(i,NLamPwd,NRefPwd)=Suma
                    else
                      DerAxDivProfA(i,2,NLamPwd,NRefPwd)=
     1                  (Suma-AxDivProfA(i,NLamPwd,NRefPwd))*deltar
                    endif
                  else
                    DerAxDivProfA(i,l,NLamPwd,NRefPwd)=Suma
                  endif
                enddo
              endif
              if(l.eq.0.and.CalcDer) then
                if(Poprve) then
                  Old=AsymPwd(2,KDatBlock)
                  AsymPwd(2,KDatBlock)=AsymPwd(2,KDatBlock)*1.1
                  deltar=1./(AsymPwd(2,KDatBlock)-Old)
                  Poprve=.false.
                  go to 1130
                else
                  AsymPwd(2,KDatBlock)=Old
                endif
              endif
            endif
          enddo
          if(KUseLamFile(KDatBlock).eq.1) then
            pom=Th-12.5*RadStepPwd
            PomLam0=LamPwd(NLamPwd,KDatBlock)/sinth
            do i=-25,25
              PomLam=(PomLam0*sin(pom)-LamPwd(NLamPwd,KDatBlock))*100000.
              Wx(i)=FInter(PomLam+5000.,
     1                     LamProfilePwd(-5000,NLamPwd,KDatBlock),1.,
     2                     10000)
              pom=pom+RadStepPwd*.5
            enddo
            do l=0,lk
              if(l.eq.0) then
                call CopyVek(AxDivProfA(-25,NLamPwd,NRefPwd),Wy(-25),51)
              else
                if(AsymKi(l,KDatBlock).eq.0) cycle
                call CopyVek(DerAxDivProfA(-25,l,NLamPwd,NRefPwd),
     1                                     Wy(-25),51)
              endif
              do i=-25,25
                pom=0.
                jp=max(-25,-25+i)
                jk=min( 25, 25+i)
                do j=jp,jk
                  Soucin=Wx(j)*Wy(i-j)
                  if(Wx(j).le.0.) then
                    if(pom.gt.0.) then
                      exit
                    else
                      cycle
                    endif
                  endif
                  pom=pom+Soucin
                enddo
                if(l.eq.0) then
                  AxDivProfA(i,NLamPwd,NRefPwd)=pom
                else
                  DerAxDivProfA(i,l,NLamPwd,NRefPwd)=pom
                endif
              enddo
            enddo
          endif
        endif
      endif
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
        etaPwd=0.
        fwhm=fwhg
        sigp=fwhm**2/5.545177
        dfwdg=ZnakSig
        dfwdl=0.
        dedffg=0.
        dedffl=0.
      else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1        KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     1        fwhg.eq.0.) then
        etaPwd=1.
        fwhm=gam
        sigp=0.
        dfwdg=0.
        dfwdl=ZnakGam
        dedffg=0.
        dedffl=0.
      else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        dsdl=0.
        dsdg=0.
        pgl=fwhg**5
        sumhm=pgl
        do i=1,5
          pgl=pgl/fwhg
          if(CalcDer) then
            dsdl=dsdl+float(i)*coft(i+1)*pgl
            dsdg=dsdg+float(6-i)*coft(i)*pgl
          endif
          pgl=pgl*gam
          sumhm=sumhm+coft(i+1)*pgl
        enddo
        fwhm=sumhm**.2
        frac=gam/fwhm
        sigp=fwhm**2/5.545177
        dedf=0.
        pf=1.
        etaPwd=0.
        do i=1,3
          if(CalcDer) dedf=dedf+float(i)*cofn(i)*pf
          pf=pf*frac
          etaPwd=etaPwd+cofn(i)*pf
        enddo
        dsdl=dsdl*ZnakGam
        dsdg=dsdg*ZnakSig
        dfwdg=.2*dsdg*fwhm/sumhm
        dfwdl=.2*dsdl*fwhm/sumhm
        dfrdg=-frac*dfwdg/fwhm
        dfrdl=(1.-frac*dfwdl)/fwhm
        dedffg=dedf*dfrdg
        dedffl=dedf*dfrdl
      endif
      rdeg(1)=peak-shift-PCutOff(KPhase,KDatBlock)*fwhm
      rdeg(2)=peak-shift+PCutOff(KPhase,KDatBlock)*fwhm
      if(isTOF) then
        if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          rdeg(1)=rdeg(1)-ln2/Alpha12*PCutOff(KPhase,KDatBlock)
          rdeg(2)=rdeg(2)+ln2/Beta12*PCutOff(KPhase,KDatBlock)
        endif
      else if(.not.isED) then
        if(pcot.ge.0.) then
          rdeg(1)=rdeg(1)-pcot
        else
          rdeg(2)=rdeg(2)-pcot
        endif
      endif
      if(CalcDer) then
        IZdvih=(KDatBlock-1)*NParRecPwd
        i=IShiftPwd+IZdvih
        if(isTOF) then
          if(KUseTOFJason(KDatBlock).le.0) then
            coef(i,NLamPwd,NRefPwd)=-pomc
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-pomc*dd
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-pomc*dd2
          else
            pom=pomc*(1.-fn)
            coef(i,NLamPwd,NRefPwd)=-pom
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-pom*dd
            i=i+1
            coef(i,NLamPwd,NRefPwd)=pom*ddrcp
            pom=.001*fn
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-pom
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-pom*dd
            i=i+1
            CrossDer=-1./sqrt(pi)*expJana(-CrossArg**2,ichp)
            pom=-pomc*CrossDer*(TE-TT)
            iw=i
            coef(i,NLamPwd,NRefPwd)=pom*(ShiftPwd(7,KDatBlock)-ddrcp)
            i=i+1
            it=i
            coef(i,NLamPwd,NRefPwd)=pom*ShiftPwd(6,KDatBlock)
          endif
        else if(isED) then
          coef(i,NLamPwd,NRefPwd)=1.
        else
          coef(i,NLamPwd,NRefPwd)=pomc
          i=i+1
          coef(i,NLamPwd,NRefPwd)=csth*pomc
          i=i+1
          coef(i,NLamPwd,NRefPwd)=sin2th*pomc
        endif
        i=ILamPwd+IZdvih
        if(isTOF.or.isED) then
          coef(i  ,NLamPwd,NRefPwd)=0.
          coef(i+1,NLamPwd,NRefPwd)=0.
        else
          pom=-sinthl/csth
          if(NLamPwd.eq.1) then
            coef(i  ,NLamPwd,NRefPwd)=pom
            coef(i+1,NLamPwd,NRefPwd)=0.
          else
            coef(i  ,NLamPwd,NRefPwd)=0.
            coef(i+1,NLamPwd,NRefPwd)=pom
          endif
        endif
        if(isTOF) then
          i=IAsymPwd+IZdvih
          if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
            coef(i,NLamPwd,NRefPwd)=1000.
            i=i+1
            coef(i,NLamPwd,NRefPwd)=1000.*ddrcp
            i=i+1
            coef(i,NLamPwd,NRefPwd)=1000.
            i=i+1
            coef(i,NLamPwd,NRefPwd)=1000.*dd4rcp
          else if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
            if(Alpha12.ne.0.) then
              dAlpha12Pom=-Alpha12**2*.001
              dAlpha12dAlphaT=dAlpha12Pom*(1.-fn)*ZnAlphaT
              dAlpha12dAlphaE=dAlpha12Pom*fn*ZnAlphaE
            else
              dAlpha12Pom=0.
              dAlpha12dAlphaT=0.
              dAlpha12dAlphaE=0.
            endif
            if(Beta12.ne.0.) then
              dBeta12Pom=-Beta12**2*.001
              dBeta12dBetaT=dBeta12Pom*(1.-fn)*ZnBetaT
              dBeta12dBetaE=dBeta12Pom*fn*ZnBetaE
            else
              dBeta12Pom=0.
              dBeta12dBetaT=0.
              dBeta12dBetaE=0.
            endif
            coef(i,NLamPwd,NRefPwd)=dAlpha12dAlphaE
            i=i+1
            coef(i,NLamPwd,NRefPwd)=dAlpha12dAlphaE*dd
            i=i+1
            coef(i,NLamPwd,NRefPwd)=dBeta12dBetaE
            i=i+1
            coef(i,NLamPwd,NRefPwd)=dBeta12dBetaE*dd
            i=i+1
            coef(i,NLamPwd,NRefPwd)= dAlpha12dAlphaT
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-dAlpha12dAlphaT*ddrcp
            i=i+1
            coef(i,NLamPwd,NRefPwd)= dBeta12dBetaT
            i=i+1
            coef(i,NLamPwd,NRefPwd)=-dBeta12dBetaT*ddrcp
            poma=CrossDer*(AlphaE-AlphaT)*dAlpha12Pom
            pomb=CrossDer*(BetaE-BetaT)*dBeta12Pom
            coefp(iw,NLamPwd,NRefPwd)=poma*(ShiftPwd(7,KDatBlock)-ddrcp)
            coefq( 1,NLamPwd,NRefPwd)=pomb*(ShiftPwd(7,KDatBlock)-ddrcp)
            coefp(it,NLamPwd,NRefPwd)=poma*ShiftPwd(6,KDatBlock)
            coefq( 2,NLamPwd,NRefPwd)=pomb*ShiftPwd(6,KDatBlock)
          endif
          if(TOFInD) then
            pom=dd3
          else
            if(KUseTOFJason(KDatBlock).le.0) then
              pom=dd3*(ShiftPwd(2,KDatBlock)+
     1                 2.*ShiftPwd(3,KDatBlock)*dd)*pomc
            else
              pom=dd3*((1.-fn)*(ShiftPwd(2,KDatBlock)+
     1                          ShiftPwd(3,KDatBlock)*dd2rcp)+
     2                     fn *ShiftPwd(5,KDatBlock)+
     3                 CrossDer*(TE-TT)*ShiftPwd(6,KDatBlock)*dd2rcp)
     4                 *pomc
            endif
          endif
        else if(isED) then
          pom=-EDEnergy2D(KDatBlock)**2/th
        else
          i=IAsymPwd+IZdvih
          coef(i,NLamPwd,NRefPwd)=cth*pomc
          pom=-tth/(2.*sinthlq)
        endif
        pom=pom*.5
        do i=1,6
          call indext(i,k,l)
          dtdgi(i)=pom*h3(k)*h3(l)
          if(k.ne.l) dtdgi(i)=2.*dtdgi(i)
        enddo
        do i=1,3
          dtdrcp(i)=2.*dtdgi(i)*rcp(i,1,KPhase)
          do j=1,3
            if(i.le.2) then
              k=j
            else
              k=j-1
            endif
            if(j.eq.4-i) cycle
            dtdrcp(i)=dtdrcp(i)+dtdgi(j+3)*rcp(7-j,1,KPhase)*
     1                                     rcp(  k,1,KPhase)
          enddo
          if(NDim(KPhase).gt.3) then
            call multm(MetTensI(1,1,KPhase),h3,dtdh3,3,3,1)
            do j=1,3
              dtdh3(j)=dtdh3(j)*pom
            enddo
          endif
        enddo
        do i=4,6
          call indext(i,k,l)
          dtdrcp(10-i)=dtdgi(i)*rcp(k,1,KPhase)*rcp(l,1,KPhase)
        enddo
        IZdvih=(KPhase-1)*NParCellProfPwd+(KDatBlock-1)*NParProfPwd
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          i=IGaussPwd+IZdvih
          do j=1,4
            coef(i,NLamPwd,NRefPwd)=pomq*DerGauss(j)
            i=i+1
          enddo
          if(IBroadHKL.gt.0) then
            i=IBroadHKLPwd+IZdvih+IBroadHKL-1
            coef(i,NLamPwd,NRefPwd)=pomq*DerBroadHKLGauss(IBroadHKL)
          endif
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          i=ILorentzPwd+IZdvih
          do j=1,5
            if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
              coef(i,NLamPwd,NRefPwd)=pomc/gam1*
     1          LorentzPwd(j,KPhase,KDatBlock)*DerLorentz(j)**2
            else
              coef(i,NLamPwd,NRefPwd)=pomc*DerLorentz(j)
            endif
            i=i+1
          enddo
          if(IBroadHKL.gt.0) then
            i=IBroadHKLPwd+IZdvih+IBroadHKL-1
            coefp(i,NLamPwd,NRefPwd)=pomc*DerBroadHKLLorentz(IBroadHKL)
          endif
        endif
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
          i=IZetaPwd+IZdvih
          if(isTOF.or.isED) then
            coef (i,NLamPwd,NRefPwd)=-2.*(1.-ZetaL)*pomq*pcosiqt*dd6
            coefp(i,NLamPwd,NRefPwd)=pomc*pcosit*dd3
          else
            coef (i,NLamPwd,NRefPwd)=-2.*(1.-ZetaL)*pomq*pcosiqt*tthq
            coefp(i,NLamPwd,NRefPwd)=pomc*pcosit*tth
          endif
          if(pcosit.ne.0.) then
            pom=.5/pcosit
          else
            pom=0.
          endif
          if(NDimI(KPhase).eq.1) then
            n=35
          else
            n=15+NDimI(KPhase)
          endif
          do i=IStPwd+IZdvih,IStPwd+IZdvih+n-1
            coe=coefp(i,NLamPwd,NRefPwd)*ZnakStrain
            if(isTOF.or.isED) then
              coef (i,NLamPwd,NRefPwd)=coe*pomq*dd6*ZetaG
              coefp(i,NLamPwd,NRefPwd)=coe*pom*pomc*dd3*ZetaL
            else
              coef (i,NLamPwd,NRefPwd)=coe*pomq*tthq*ZetaG
              coefp(i,NLamPwd,NRefPwd)=coe*pom*pomc*tth*ZetaL
            endif
          enddo
        endif
        IZdvih=(KPhase-1)*NParCellProfPwd
        j=ICellPwd+IZdvih
        coef(j,NLamPwd,NRefPwd)=-dtdrcp(1)*rcp(1,1,KPhase)/
     1                                     CellPwd(1,KPhase)
        j=j+1
        coef(j,NLamPwd,NRefPwd)=-dtdrcp(2)*rcp(2,1,KPhase)/
     1                                     CellPwd(2,KPhase)
        j=j+1
        coef(j,NLamPwd,NRefPwd)=-dtdrcp(3)*rcp(3,1,KPhase)/
     1                                     CellPwd(3,KPhase)
        j=j+1
        coef(j,NLamPwd,NRefPwd)=
     1    ((dtdrcp(1)/CellPwd(1,KPhase)*cotgbr(KPhase)*cotggr(KPhase)+
     2      dtdrcp(2)/CellPwd(2,KPhase)*cotgar(KPhase)/singr(KPhase)+
     3      dtdrcp(3)/CellPwd(3,KPhase)*cotgar(KPhase)/sinbr(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(4)*sina(KPhase)/(sinb(KPhase)*sing(KPhase))+
     6      dtdrcp(5)*cosgr(KPhase)*sinb(KPhase)/
     7      (sina(KPhase)*sing(KPhase))+
     8      dtdrcp(6)*cosbr(KPhase)*sing(KPhase)/
     9      (sina(KPhase)*sinb(KPhase)))*torad
        j=j+1
        coef(j,NLamPwd,NRefPwd)=
     1    ((dtdrcp(2)/CellPwd(2,KPhase)*cotggr(KPhase)*cotgar(KPhase)+
     2      dtdrcp(3)/CellPwd(3,KPhase)*cotgbr(KPhase)/sinar(KPhase)+
     3      dtdrcp(1)/CellPwd(1,KPhase)*cotgbr(KPhase)/singr(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(5)*sinb(KPhase)/(sina(KPhase)*sing(KPhase))+
     6      dtdrcp(4)*cosgr(KPhase)*sina(KPhase)/
     7      (sinb(KPhase)*sing(KPhase))+
     8      dtdrcp(6)*cosar(KPhase)*sing(KPhase)/
     9      (sinb(KPhase)*sina(KPhase)))*torad
        j=j+1
        coef(j,NLamPwd,NRefPwd)=
     1    ((dtdrcp(3)/CellPwd(3,KPhase)*cotgar(KPhase)*cotgbr(KPhase)+
     2      dtdrcp(1)/CellPwd(1,KPhase)*cotggr(KPhase)/sinbr(KPhase)+
     3      dtdrcp(2)/CellPwd(2,KPhase)*cotggr(KPhase)/sinar(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(6)*sing(KPhase)/(sina(KPhase)*sinb(KPhase))+
     6      dtdrcp(4)*cosbr(KPhase)*sina(KPhase)/
     7      (sing(KPhase)*sinb(KPhase))+
     8      dtdrcp(5)*cosar(KPhase)*sinb(KPhase)/
     9      (sing(KPhase)*sina(KPhase)))*torad
        if(NDimI(KPhase).ne.0) then
          do k=1,NDimI(KPhase)
            do i=1,3
              j=j+1
              coef(j,NLamPwd,NRefPwd)=float(ih(k+3))*dtdh3(i)
            enddo
          enddo
        endif
      endif
      if(NLamPwd.ne.0) then
        peaka(NLamPwd,NRefPwd)=peak
        shifta(NLamPwd,NRefPwd)=shift
        sqsga(NLamPwd,NRefPwd)=sqsg
        if(isTOF) then
          fnorma(NLamPwd,NRefPwd)=fnorm
          Alpha12a(NLamPwd,NRefPwd)=Alpha12
          Beta12a(NLamPwd,NRefPwd)=Beta12
        else if(.not.isED) then
          if(Kasym(KDatBlock).eq.IdPwdAsymSimpson) then
            ntsima(NLamPwd,NRefPwd)=ntsim
            fnorma(NLamPwd,NRefPwd)=fnorm
            tntsima(NLamPwd,NRefPwd)=tntsim
            pcota(NLamPwd,NRefPwd)=pcot
          else if(Kasym(KDatBlock).eq.IdPwdAsymBerar) then
            cotgtha(NLamPwd,NRefPwd)=cotgth
            cotg2tha(NLamPwd,NRefPwd)=cotg2th
          else if(Kasym(KDatBlock).eq.IdPwdAsymDivergence) then
            cos2tha(NLamPwd,NRefPwd)=cos2th
            cos2thqa(NLamPwd,NRefPwd)=cos2thq
          else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then

          endif
        endif
        do i=1,2
          rdega(i,NLamPwd,NRefPwd)=rdeg(i)
        enddo
        fwhma(NLamPwd,NRefPwd)=fwhm
        IBroadHKLa(NRefPwd)=IBroadHKL
        etaPwda(NLamPwd,NRefPwd)=etaPwd
        sigpa(NLamPwd,NRefPwd)=sigp
        dedffga(NLamPwd,NRefPwd)=dedffg
        dedffla(NLamPwd,NRefPwd)=dedffl
        dfwdga(NLamPwd,NRefPwd)=dfwdg
        dfwdla(NLamPwd,NRefPwd)=dfwdl
        if(.not.isTOF.and..not.isED) then
          if(NLamPwd.eq.1) then
            if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
              call CopyVek(AxDivProfA(-25,NLamPwd,NRefPwd),
     1                                AxDivProf(-25),51)
              do l=3,lk
                if(AsymKi(l,KDatBlock).eq.0) cycle
                call CopyVek(DerAxDivProfA(-25,l,NLamPwd,NRefPwd),
     1                     DerAxDivProf(-25,l),51)
              enddo
            endif
          endif
        endif
        Prof0(NRefPwd)=Prfl(-shift)
      endif
      return
      end
