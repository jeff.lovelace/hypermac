      subroutine RefFixedWriteCommand(Command,ich)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/FixedQuest/ nEdwAtoms,nEdwSetTo,FixedType,AtomString,
     1                   ValueSetTo
      character*(*) Command
      character*256 EdwStringQuest,AtomString
      character*80  ErrString
      integer FixedType
      save /FixedQuest/
      ich=0
      i=FixedType
      if(i.gt.3) i=i+1
      Command='fixed '//CFix(i)(:idel(CFix(i)))
      if(FixedType.eq.IdFixSetTo) then
        call FeQuestRealFromEdw(nEdwSetTo,pom)
        write(Cislo,'(f15.6)') pom
        call ZdrcniCisla(Cislo,1)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      endif
      AtomString=EdwStringQuest(nEdwAtoms)
      if(AtomString.eq.' ') go to 9100
      call RefFixedCheck(AtomString,FixedType,ErrString)
      if(ErrString.ne.' ') then
        call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
        ich=1
      else
        Command=Command(:idel(Command)+1)//AtomString(:idel(AtomString))
      endif
      go to 9999
9100  ich=1
9999  return
      end
