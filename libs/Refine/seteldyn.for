      subroutine SetElDyn(Klic,Tisk)
      use EDZones_mod
      use Refine_mod
      integer Tisk
      include 'fepc.cmn'
      include 'basic.cmn'
      character*120 Veta
      do KDatB=1,NDatBlock
        if(.not.ExistElectronData.or.ActionED.gt.1) cycle
        if(.not.UseDatBlockActual(KDatB)) then
          do j=1,NEDZone(KDatB)
            KiED(1:6,j,KDatB)=0
          enddo
          cycle
        endif
        do j=1,NEDZone(KDatB)
          if(.not.UseEDZone(j,KDatB)) KiED(1:6,j,KDatB)=0
          if(NEDZone(KDatB).le.1) cycle
          n=0
          pom=0.
          do i=1,NEDZone(KDatB)
            if(NThickEDZone(i,KDatB).eq.j.and.
     1         UseEDZone(i,KDatB)) then
              n=n+1
              pom=pom+ThickEDZone(i,KDatB)
              if(n.eq.1) then
                spom=ThickEDZoneS(i,KDatB)
                if(Klic.eq.1) exit
              endif
            endif
          enddo
          if(n.gt.0) pom=pom/float(n)
          n=0
          m=0
          do i=1,NEDZone(KDatB)
            if(NThickEDZone(i,KDatB).eq.j) then
              if(UseEDZone(i,KDatB)) n=n+1
              ThickEDZone (i,KDatB)=pom
              ThickEDZoneS(i,KDatB)=spom
              if(n.gt.1) KiED(2,i,KDatB)=0
            else if(NThickEDZone(i,KDatB).le.0) then
              m=m+1
            endif
          enddo
          if(Tisk.le.0) cycle
          if(n+m.eq.NEDZone(KDatB)) then
            if(n.eq.0) then
              if(j.ne.1) cycle
              call newln(2)
              write(lst,'(/''Sample thicknesses are not restricted'')')
            else
              call newln(2)
              write(lst,'(/''Sample thicknesses are supposed to be '',
     1                     ''identical for all zones'')')
            endif
          else if(n.gt.0) then
            call newln(3)
            Veta='The following zones'
            if(NDatBlock.gt.1) then
              write(Cislo,'(i5)') KDatB
              call Zhusti(Cislo)
              Veta=Veta(:idel(Veta))//' from the block#'//Cislo
            endif
            Veta=Veta(:idel(Veta))//' are supposed to have '//
     1           'identical thickness:'
            write(lst,FormA)
            write(lst,FormA) Veta(:idel(Veta))
            write(lst,FormA)
            Veta=' '
            m=0
            do i=1,NEDZone(KDatB)
              if(NThickEDZone(i,KDatB).eq.j) then
                write(Cislo,'(''Zone#'',i5)') i
                call Zhusti(Cislo)
                Veta=Veta(:m)//Cislo
                m=m+10
                if(m.ge.120) then
                  call NewLn(1)
                  write(lst,FormA) Veta(:idel(Veta))
                  Veta=' '
                  m=0
                endif
              endif
            enddo
            if(m.gt.0) then
              call NewLn(1)
              write(lst,FormA) Veta(:idel(Veta))
            endif
          endif
        enddo
      enddo
9999  return
      end
