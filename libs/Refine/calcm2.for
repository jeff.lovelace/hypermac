      subroutine Calcm2(i,js,isw,klic)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension x4center(3),fc(3),smp(3),FPolA(2*mxw+1),ParSupX4(3),
     1          argl(3),delta(3),NSupPom(3)
      save qcnt1,qcnt2,qcnt3,MxMod,MxModC,NSupPom,mcp,mck,mp,MxSupPar,
     1     MagParI,last,itfi,kfsi,kfxi
      if(klic.lt.0) then
        if(ngcMax.gt.0) then
          call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
          if(allocated(isaRef)) deallocate(isaRef,ismRef,fyr)
          allocate(isaRef(MaxNSymm,NAtCalc),ismRef(NAtCalc),
     1             fyr(NAtCalc))
          call RefSpecAt
          if(ErrFlag.ne.0) go to 9810
        endif
        if(allocated(PrvniParSup)) deallocate(PrvniParSup,DelkaParSup)
        allocate(PrvniParSup(NAtCalc),DelkaParSup(NAtCalc))
        MxSup=0
        MxSupPar=1
        MxMod=0
        do ii=1,NAtCalc
          KPhase=kswa(ii)
          PrvniParSup(ii)=MxSupPar
          if(ai(ii).eq.0..or.NDimI(KPhase).le.0) then
            DelkaParSup(ii)=0
            cycle
          endif
          isw=iswa(ii)
          itfi=itf(ii)
          MagParI=MagPar(ii)
          if(KFA(1,ii).ne.0.and.KModA(1,ii).ne.0) then
            kfsi=KFA(1,ii)
          else
            kfsi=0
          endif
          if(KFA(2,ii).ne.0.and.KModA(2,ii).ne.0) then
            kfxi=KFA(2,ii)
          else
            kfxi=0
          endif
          do j=1,itfi+1
            MxMod=max(MxMod,KModA(j,ii))
          enddo
          MxMod=max(MxMod,MagParI-1)
          if(KCommen(KPhase).eq.0) then
            call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
            if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
              do j=1,NDimI(KPhase)
                NSupPom(j)=NSupPom(j)+2
              enddo
            endif
          else
            do j=1,3
              NSupPom(j)=NCommQ(j,isw,KPhase)
            enddo
          endif
          NSupPomAll=NSupPom(1)*NSupPom(2)*NSupPom(3)
          MxSup=MxSup+NSupPomAll
          MxSupPar=MxSupPar+7*NSupPomAll
          DelkaParSup(ii)=7
          do j=3,itfi+1
            kmod=KModA(j,ii)
            if(kmod.le.0) cycle
            nrank=TRank(j-1)
            DelkaParSup(ii)=DelkaParSup(ii)+nrank
            MxSupPar=MxSupPar+nrank*NSupPomAll
          enddo
          if(MagParI.gt.0) then
            MxSupPar=MxSupPar+3*NSupPomAll
            DelkaParSup(ii)=DelkaParSup(ii)+3
          endif
        enddo
        if(allocated(ParSup))
     1    deallocate(ParSup,snw,csw,skfx1,skfx2,skfx3,x4low,x4length,
     2           snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     3           sncospsc,cscospsc)
        allocate(ParSup(MxSupPar),snw(MxMod,MxSup),csw(MxMod,MxSup),
     1           skfx1(MxSup),skfx2(MxSup),skfx3(MxSup),
     2           x4low(3,NAtCalc),x4length(3,NAtCalc),
     3           snsinps(mxmod),cssinps(mxmod),sncosps(mxmod),
     4           cscosps(mxmod),snsinpsc(mxmod),cssinpsc(mxmod),
     5           sncospsc(mxmod),cscospsc(mxmod))
        mc=0
        mp=1
        do ii=1,NAtCalc
          KPhase=kswa(ii)
          if(NDimI(KPhase).le.0) cycle
          isw=iswa(ii)
          kmodxi=KModA(2,ii)
          kmodsi=KModA(1,ii)
          MagParI=MagPar(ii)
          if(KFA(1,ii).ne.0.and.kmodsi.ne.0) then
            kfsi=KFA(1,ii)
          else
            kfsi=0
          endif
          if(KFA(2,ii).ne.0.and.kmodxi.ne.0) then
            kfxi=KFA(2,ii)
          else
            kfxi=0
          endif
          itfi=itf(ii)
          mxmod=0
          do j=1,itfi+1
            mxmod=max(mxmod,KModA(j,ii))
          enddo
          MxMod=max(MxMod,MagParI-1)
          if(ai(ii).eq.0.) cycle
          atomic=kmol(ii).eq.0
          if(kfsi.ne.0) then
            k=kmodsi-NDim(KPhase)+3
            do j=1,NDimI(KPhase)
              k=k+1
              if(NDimI(KPhase).gt.1) then
                delta(j)=ay(k,ii)*.5
              else
                delta(j)=a0(ii)*.5
              endif
              x4center(j)=ax(k,ii)
              x4low(j,ii)=x4center(j)-delta(j)
              x4length(j,ii)=delta(j)*2.
            enddo
          else if(kfxi.gt.0) then
            x4center(1)=uy(1,kmodxi,ii)
            delta(1)=uy(2,kmodxi,ii)*.5
            if(kfxi.eq.1) then
              x4low(1,ii)=x4center(1)-delta(1)
              x4length(1,ii)=delta(1)*2.
            else
              x4low(1,ii)=0.
              x4length(1,ii)=1.
            endif
          else
            call SetRealArrayTo(x4low   (1,ii),NDimI(KPhase),0.)
            call SetRealArrayTo(x4length(1,ii),NDimI(KPhase),1.)
          endif
          if(KCommen(KPhase).eq.0) then
            call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
            if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
              do j=1,NDimI(KPhase)
                NSupPom(j)=NSupPom(j)+2
              enddo
            endif
          else
            do j=1,3
              NSupPom(j)=NCommQ(j,isw,KPhase)
            enddo
          endif
          do ic3=1,NSupPom(3)
            if(KCommen(KPhase).eq.0.or.NDimI(KPhase).ne.1) fc(3)=ic3-1
            if(NDimI(KPhase).gt.2.and.KCommen(KPhase).le.0) then
              if(ic3.le.NSuper(3,isw,KPhase)) then
                ParSupX4(3)=x4low(3,ii)+x4length(3,ii)*
     1                                 XGauss(ic3,3,KPhase)
              else if(ic3.eq.NSuper(3,isw,KPhase)+1) then
                ParSupX4(3)=x4low(3,ii)+.0001
              else
                ParSupX4(3)=x4low(3,ii)+x4length(3,ii)-.0001
              endif
            endif
            do ic2=1,NSupPom(2)
              if(KCommen(KPhase).eq.0.or.NDimI(KPhase).ne.1) fc(2)=ic2-1
              if(NDimI(KPhase).gt.1.and.KCommen(KPhase).le.0) then
                if(ic2.le.NSuper(2,isw,KPhase)) then
                  ParSupX4(2)=x4low(2,ii)+x4length(2,ii)*
     1                                   XGauss(ic2,2,KPhase)
                else if(ic2.eq.NSuper(2,isw,KPhase)+1) then
                  ParSupX4(2)=x4low(2,ii)+.0001
                else
                  ParSupX4(2)=x4low(2,ii)+x4length(2,ii)-.0001
                endif
              endif
              do ic1=1,NSupPom(1)
                mc=mc+1
                call SetRealArrayTo(ParSup(mp),7,0.)
                if(KCommen(KPhase).gt.0) then
                  if(NDimI(KPhase).ne.1) then
                    fc(1)=ic1-1
                  else
                    pom=ic1-1
                    do j=1,3
                      fc(j)=VCommQ(j,1,isw,KPhase)*pom
                    enddo
                  endif
                  do j=1,3
                    ParSup(mp+j)=fc(j)
                  enddo
                  call qbyx(fc,ParSup(mp+4),isw)
                  do j=4,NDim(KPhase)
                    ParSup(mp+j)=ParSup(mp+j)+trez(j-3,isw,KPhase)+
     1                                        qcnt(j-3,ii)
                  enddo
                else
                  if(ic1.le.NSuper(1,isw,KPhase)) then
                    ParSupX4(1)=x4low(1,ii)+x4length(1,ii)*
     1                                     XGauss(ic1,1,KPhase)
                  else if(ic1.eq.NSuper(1,isw,KPhase)+1) then
                    ParSupX4(1)=x4low(1,ii)+.0001
                  else
                    ParSupX4(1)=x4low(1,ii)+x4length(1,ii)-.0001
                  endif
                  call CopyVek(ParSupX4,ParSup(mp+4),NDimI(KPhase))
                endif
                call CopyVek(ParSup(mp+4),argl,NDimI(KPhase))
                kk=1
                if(InvMag(KPhase).gt.0) then
                  kmg=2
                else
                  kmg=1
                endif
                do k=1,mxmod
                  if(TypeModFun(ii).le.1.or.
     1               (kmg.eq.2.and.mod(k,kmg).eq.1)) then
                    arg=0.
                    do n=1,NDimI(KPhase)
                      arg=arg+float(kw(n,k,KPhase))*ParSup(mp+n+3)
                    enddo
                    arg=arg*pi2
                    snw(k,mc)=sin(arg)
                    csw(k,mc)=cos(arg)
                  else
                    if(k.eq.kmg) then
                      pom=float(kmg)*argl(1)-x4center(1)
                      j=pom
                      if(pom.lt.0.) j=j-1
                      pom=pom-float(j)
                      if(pom.gt..5) pom=pom-1.
                      pom=pom/delta(1)
                      call GetFPol(pom,FPolA,2*mxmod+1,TypeModFun(ii))
                    endif
                    if(kmg.eq.1.or.mod(k,kmg).eq.0) then
                      kk=kk+1
                      snw(k,mc)=FPolA(kk)
                      kk=kk+1
                      csw(k,mc)=FPolA(kk)
                    else
                      snw(k,mc)=0.
                      csw(k,mc)=0.
                    endif
                  endif
                enddo
                occx=1.
                do k=1,kmodxi
                  if(kfxi.gt.0.and.k.eq.kmodxi) then
                    pom=argl(1)-x4center(1)
                    j=pom
                    if(pom.lt.0.) j=j-1
                    pom=pom-float(j)
                    if(pom.gt..5) pom=pom-1.
                    if(kfxi.eq.1) then
                      znak=pom/delta(1)
                      zn=1.
                      if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                        occx=1.
                      else
                        occx=0.
                      endif
                    else
                      if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                        zn=1.
                        if(kfxi.eq.2) then
                          znak=pom/delta(1)
                        else
                          znak=1.
                        endif
                      else
                        if(kfxi.eq.2) then
                          pom=pom+.5
                          if(pom.gt..5) then
                            pom=pom-1.
                          else if(pom.lt.-.5) then
                            pom=pom+1.
                          endif
!                          if(pom.ge.-.5+delta(1).and.pom.le..5-delta(1))
!     1                      then
!                            zn=-1.
!                            znak=-pom/(.5-delta(1))
!                            occx=1.
                          if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                            zn=-1.
                            znak=-pom/delta(1)
                            occx=1.
                           else
                            znak=0.
                            occx=0.
                          endif
                        else
                          znak=-1.
                        endif
                      endif
                    endif
                    do j=1,3
                      ParSup(mp+j)=ParSup(mp+j)+ux(j,k,ii)*znak
                      skfx1(mc)=znak
                      skfx2(mc)=-zn/delta(1)
                      skfx3(mc)=-.5*znak/delta(1)
                    enddo
                  else
                    sn=snw(k,mc)
                    cs=csw(k,mc)
                    do j=1,3
                      ParSup(mp+j)=ParSup(mp+j)+ux(j,k,ii)*sn
     1                                         +uy(j,k,ii)*cs
                    enddo
                  endif
                enddo
                occo=0.
                if(kmodsi.gt.0) then
                  if(kfsi.eq.0) occo=a0(ii)-occx
                  do k=1,kmodsi
                    if(kfsi.ne.0.and.k.ge.kmodsi-NDim(KPhase)+4)
     1                then
                      do l=1,NDimI(KPhase)
                        pom=argl(l)-x4center(l)
                        j=pom
                        if(pom.lt.0.) j=j-1
                        pom=pom-float(j)
                        if(pom.gt..5) pom=pom-1.
                        if(pom.ge.-delta(l)-.0001.and.
     1                     pom.le. delta(l)+.0001) then
                          occx=1.
                        else
                          occx=0.
                        endif
                        if(occx.lt..001) go to 1810
                      enddo
                    else
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      occo=occo+ax(k,ii)*sn+ay(k,ii)*cs
                    endif
                  enddo
                endif
1810            if(occx.gt.0.) then
                  ParSup(mp)=occo+occx
                else
                  ParSup(mp)=0.
                endif
                mp=mp+7
                do n=3,itfi+1
                  kmod=KModA(n,ii)
                  if(kmod.le.0) cycle
                  nrank=TRank(n-1)
                  do k=1,kmod
                    sn=snw(k,mc)
                    cs=csw(k,mc)
                    m=mp
                    do j=1,nrank
                      if(k.eq.1) ParSup(m)=0.
                      if(n.eq.3) then
                        ParSup(m)=ParSup(m)+bx(j,k,ii)*sn+by(j,k,ii)*cs
                      else if(n.eq.4) then
                        ParSup(m)=ParSup(m)+c3x(j,k,ii)*sn
     1                                     +c3y(j,k,ii)*cs
                      else if(n.eq.5) then
                        ParSup(m)=ParSup(m)+c4x(j,k,ii)*sn
     1                                     +c4y(j,k,ii)*cs
                      else if(n.eq.6) then
                        ParSup(m)=ParSup(m)+c5x(j,k,ii)*sn
     1                                     +c5y(j,k,ii)*cs
                      else
                        ParSup(m)=ParSup(m)+c6x(j,k,ii)*sn
     1                                     +c6y(j,k,ii)*cs
                      endif
                      m=m+1
                    enddo
                  enddo
                  mp=mp+nrank
                enddo
                if(MagParI.gt.0) then
                  m=mp
                  do j=1,3
                    ParSup(m)=sm0(j,ii)
                    do k=1,MagParI-1
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      ParSup(m)=ParSup(m)+smx(j,k,ii)*sn
     1                                   +smy(j,k,ii)*cs
                    enddo
                    m=m+1
                  enddo
                  mp=mp+3
                endif
              enddo
            enddo
          enddo
        enddo
        last=99999999
      else if(klic.eq.0) then
        mp=PrvniParSup(i)
        if(i.le.last) mck=0
        mcp=mck
        last=i
        itfi=itf(i)
        qcnt1=qcnt(1,i)
        if(NDim(KPhase).gt.4) then
          qcnt2=qcnt(2,i)
          if(NDim(KPhase).gt.5) qcnt3=qcnt(3,i)
        endif
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        MagParI=MagPar(i)
        if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
          kfsi=KFA(1,i)
        else
          kfsi=0
        endif
        if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
          kfxi=KFA(2,i)
        else
          kfxi=0
        endif
        if(kfxi.eq.0) then
          kmezx=kmodxi
        else
          kmezx=kmodxi-1
        endif
        if(kfsi.eq.0) then
          kmezs=kmodsi
        else if(kfsi.ne.0) then
          kmezs=kmodsi-NDim(KPhase)+3
        endif
        mxmod=0
        mxmodc=0
        do j=1,itfi+1
          mxmod=max(mxmod,KModA(j,i))
          if(j.gt.3) mxmodc=max(mxmodc,KModA(j,i))
        enddo
        MxMod=max(MxMod,MagParI-1)
        nemod=mxmod.le.0
      else if(klic.eq.1) then
        mp=PrvniParSup(i)
        mc=mcp
        if(.not.atomic.and.KCommen(KPhase).le.0) then
          PhiAng=PhiAng-pi2*float(mj)*qcnt1
          if(NDim(KPhase).gt.4) PhiAng=PhiAng-pi2*float(nj)*qcnt2
          if(NDim(KPhase).gt.5) PhiAng=PhiAng-pi2*float(pj)*qcnt3
        endif
        cosij=0.
        sinij=0.
        cosijc=0.
        sinijc=0.
        call SetRealArrayTo(cosijm,3,0.)
        call SetRealArrayTo(sinijm,3,0.)
        if(KCommen(KPhase).eq.0.) then
          call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
          if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
            do j=1,NDimI(KPhase)
              NSupPom(j)=NSupPom(j)+2
            enddo
          endif
        else
          do j=1,3
            NSupPom(j)=NCommQ(j,isw,KPhase)
          enddo
        endif
        call SetRealArrayTo(snsinps,mxmod,0.)
        call SetRealArrayTo(cssinps,mxmod,0.)
        call SetRealArrayTo(sncosps,mxmod,0.)
        call SetRealArrayTo(cscosps,mxmod,0.)
        call SetRealArrayTo(snsinpsc,mxmodc,0.)
        call SetRealArrayTo(cssinpsc,mxmodc,0.)
        call SetRealArrayTo(sncospsc,mxmodc,0.)
        call SetRealArrayTo(cscospsc,mxmodc,0.)
        if(kfxi.ne.0) then
          snsinpsx1=0.
          sncospsx1=0.
          snsinpsx2=0.
          sncospsx2=0.
          snsinpsx3=0.
          sncospsx3=0.
          huhu=hj(1)*ux(1,kmodxi,i)+hj(2)*ux(2,kmodxi,i)
     1        +hj(3)*ux(3,kmodxi,i)
        endif
        do ic3=1,NSupPom(3)
          do ic2=1,NSupPom(2)
            do ic1=1,NSupPom(1)
              mc=mc+1
              if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
                if((ic1.gt.NSuper(1,isw,KPhase).and.
     1              (ic2.gt.NSuper(2,isw,KPhase).or.
     2               ic3.gt.NSuper(3,isw,KPhase))).or.
     3             (ic2.gt.NSuper(2,isw,KPhase).and.
     4              ic3.gt.NSuper(3,isw,KPhase))) go to 2490
              endif
              if(ParSup(mp).eq.0.) go to 2490
              l=0
              m=mp
              es=expij*scsup(isw,KPhase)
              if(MagParI.gt.0) then
                mm=mp+DelkaParSup(i)-3
                call Multm(RMag(1,js,1,KPhase),ParSup(mm),smp,3,3,1)
              endif
              MinusReal=.false.
              Imag=.false.
              do n=2,itfi+1
                Imag=.not.Imag
                if(mod(n,2).eq.1) MinusReal=.not.MinusReal
                nrank=TRank(n-1)
                kmod=KModA(n,i)
                if(n.eq.2) then
                  eta=PhiAng
                else if(n.eq.3) then
                  pom=0.
                else if(n.eq.4) then
                  Crealm=Creal
                  Cimagm=Cimag
                endif
                if(kmod.le.0.and.n.ne.2) then
                  l=l+nrank
                  cycle
                endif
                do k=1,nrank
                  m=m+1
                  l=l+1
                  if(n.eq.2) then
                    Clen=hj(l)*ParSup(m)
                  else
                    Clen=HCoefj(l)*ParSup(m)
                  endif
                  if(MinusReal) Clen=-Clen
                  if(n.eq.2) then
                    eta=eta+Clen
                  else if(n.eq.3) then
                    pom=pom+Clen
                  else if(Imag) then
                    Cimagm=Cimagm+Clen
                  else
                    Crealm=Crealm+Clen
                  endif
                enddo
                if(n.eq.2) then
                  m=m+3
                  l=l+1
                else if(n.eq.3) then
                  pom=ExpJana(pom,ich)
                  es=pom*es
                endif
              enddo
              if(KCommen(KPhase).le.0) then
                eta=eta+pi2*float(mj)*ParSup(mp+4)
                if(ic1.le.NSuper(1,isw,KPhase))
     1            es=es*WGauss(ic1,1,KPhase)*x4length(1,i)
                if(NDim(KPhase).gt.4) then
                  eta=eta+pi2*float(nj)*ParSup(mp+5)
                  if(ic2.le.NSuper(2,isw,KPhase))
     1              es=es*WGauss(ic2,2,KPhase)*x4length(2,i)
                endif
                if(NDim(KPhase).gt.5) then
                  eta=eta+pi2*float(pj)*ParSup(mp+6)
                  if(ic3.le.NSuper(3,isw,KPhase))
     1              es=es*WGauss(ic3,3,KPhase)*x4length(3,i)
                endif
              endif
              sinpsc=es*sin(eta)
              cospsc=es*cos(eta)
              if(itfi.gt.2) then
                cosps=cospsc*Crealm-sinpsc*Cimagm
                sinps=sinpsc*Crealm+cospsc*Cimagm
                cospsc=ParSup(mp)*cospsc
                sinpsc=ParSup(mp)*sinpsc
              else
                cosps=cospsc
                sinps=sinpsc
              endif
              if(CalcDer.and.kmezs.gt.0) then
                jx=NDerModFirst
                if(kfsi.eq.0) then
                  ader(jx)=ader(jx)+cosps
                  if(NCSymmRef(KPhase).eq.1) bder(jx)=bder(jx)+sinps
                  if(MagParI.gt.0) then
                    do l=1,3
                      aderm(l,jx)=aderm(l,jx)+smp(l)*cosps
                      bderm(l,jx)=bderm(l,jx)+smp(l)*sinps
                    enddo
                  endif
                endif
                do k=1,kmezs
                  jx=jx+1
                  jy=jx+1
                  sn=snw(k,mc)
                  cs=csw(k,mc)
                  ader(jx)=ader(jx)+sn*cosps
                  ader(jy)=ader(jy)+cs*cosps
                  if(NCSymmRef(KPhase).eq.1) then
                    bder(jx)=bder(jx)+sn*sinps
                    bder(jy)=bder(jy)+cs*sinps
                  endif
                  if(MagParI.gt.0) then
                    do l=1,3
                      aderm(l,jx)=aderm(l,jx)+smp(l)*sn*cosps
                      bderm(l,jx)=bderm(l,jx)+smp(l)*sn*sinps
                      aderm(l,jy)=aderm(l,jy)+smp(l)*cs*cosps
                      bderm(l,jy)=bderm(l,jy)+smp(l)*cs*sinps
                    enddo
                  endif
                  jx=jy
                enddo
              endif
              if(KCommen(KPhase).le.0) then
                if(ic1.gt.NSuper(1,isw,KPhase).or.
     1             ic2.gt.NSuper(2,isw,KPhase).or.
     2             ic3.gt.NSuper(3,isw,KPhase)) then
                  if(.not.CalcDer) go to 2490
                  jx=NDerModFirst
                  if(ic1.gt.NSuper(1,isw,KPhase)) then
                    pom=2*(ic1-NSupPom(1))+1
                    if(kfxi.ne.0) then
                      if(kmodsi.gt.0) jx=jx+1+2*kmodsi
                      jx=jx+(kmodxi-1)*6+4
                      jy=jx-1
                    else
                      jy=jx+1+2*(kmodsi-NDim(KPhase)+3)
                      if(NDimI(KPhase).gt.1) jx=jy+1
                    endif
                  else if(ic2.gt.NSuper(2,isw,KPhase)) then
                    pom=2*(ic2-NSupPom(2))+1
                    jy=jx+1+2*(kmodsi-NDim(KPhase)+4)
                    jx=jy+1
                  else if(ic3.gt.NSuper(3,isw,KPhase)) then
                    pom=2*(ic3-NSupPom(3))+1
                    jy=jx+1+2*(kmodsi-NDim(KPhase)+5)
                    jx=jy+1
                  endif
                  if(kfxi.ne.2) then
                    ader(jx)=ader(jx)+cosps*.5
                    ader(jy)=ader(jy)+pom*cosps
                  endif
                  if(NCSymmRef(KPhase).eq.1) then
                    bder(jx)=bder(jx)+sinps*.5
                    bder(jy)=bder(jy)+pom*sinps
                  endif
                  go to 2490
                endif
              endif
              sinps=ParSup(mp)*sinps
              cosps=ParSup(mp)*cosps
              sinij=sinij+sinps
              cosij=cosij+cosps
              if(MagParI.gt.0) then
                do l=1,3
                  cosijm(l)=cosijm(l)+smp(l)*cosps
                  sinijm(l)=sinijm(l)+smp(l)*sinps
                  if(CalcDer) then
                    jx=NDerMagFirst
                    ind=l
                    do j=1,3
                      pom=RMag(ind,js,isw,KPhase)
                      if(NCSymmRef(KPhase).eq.1) then
                        aderm(l,jx)=aderm(l,jx)+cosps*pom
                        bderm(l,jx)=bderm(l,jx)+sinps*pom
                      else
                        if(ZCSymmRef(KPhase).gt.0.) then
                          aderm(l,jx)=aderm(l,jx)+cosps*pom
                        else
                          bderm(l,jx)=bderm(l,jx)+sinps*pom
                        endif
                      endif
                      ind=ind+3
                      jx=jx+1
                    enddo
                    jxp=jx
                    do k=1,MagParI-1
                      ind=l
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      jx=jxp
                      jy=jx+3
                      do j=1,3
                        pom=RMag(ind,js,isw,KPhase)
                        if(NCSymmRef(KPhase).eq.1) then
                          aderm(l,jx)=aderm(l,jx)+sn*cosps*pom
                          bderm(l,jx)=bderm(l,jx)+sn*sinps*pom
                          aderm(l,jy)=aderm(l,jy)+cs*cosps*pom
                          bderm(l,jy)=bderm(l,jy)+cs*sinps*pom
                        else
                          if(ZCSymmRef(KPhase).gt.0.) then
                            aderm(l,jx)=aderm(l,jx)+sn*cosps*pom
                            aderm(l,jy)=aderm(l,jy)+cs*cosps*pom
                          else
                            bderm(l,jx)=bderm(l,jx)+sn*sinps*pom
                            bderm(l,jy)=bderm(l,jy)+cs*sinps*pom
                          endif
                        endif
                        ind=ind+3
                        jx=jx+1
                        jy=jy+1
                      enddo
                      jxp=jxp+6
                    enddo
                  endif
                enddo
              endif
              if(itfi.gt.2) then
                sinijc=sinijc+sinpsc
                cosijc=cosijc+cospsc
                do k=1,mxmodc
                  sn=snw(k,mc)
                  cs=csw(k,mc)
                  snsinpsc(k)=snsinpsc(k)+sn*sinpsc
                  cssinpsc(k)=cssinpsc(k)+cs*sinpsc
                  sncospsc(k)=sncospsc(k)+sn*cospsc
                  cscospsc(k)=cscospsc(k)+cs*cospsc
                enddo
              endif
              if(.not.CalcDer) go to 2490
              do k=1,mxmod
                sn=snw(k,mc)
                cs=csw(k,mc)
                snsinps(k)=snsinps(k)+sn*sinps
                cssinps(k)=cssinps(k)+cs*sinps
                sncosps(k)=sncosps(k)+sn*cosps
                cscosps(k)=cscosps(k)+cs*cosps
              enddo
              if(kfxi.ne.0) then
                sn1=skfx1(mc)
                sn2=skfx2(mc)
                sn3=skfx3(mc)
                snsinpsx1=snsinpsx1+sn1*sinps
                sncospsx1=sncospsx1+sn1*cosps
                snsinpsx2=snsinpsx2+sn2*sinps
                sncospsx2=sncospsx2+sn2*cosps
                snsinpsx3=snsinpsx3+sn3*sinps
                sncospsx3=sncospsx3+sn3*cosps
              endif
2490          mp=mp+DelkaParSup(i)
            enddo
          enddo
        enddo
        mck=mc
        if(CalcDer) then
          lx=NDerNemodLast
          if(kmodsi.gt.0) lx=lx+1+2*kmodsi
          do k=1,kmodxi
            do l=1,3
              lx=lx+1
              ly=lx+3
              hjl=hj(l)
              if(k.le.kmezx) then
                ader(lx)=ader(lx)-hjl*snsinps(k)
                ader(ly)=ader(ly)-hjl*cssinps(k)
                if(NCSymmRef(KPhase).eq.1) then
                  bder(lx)=bder(lx)+hjl*sncosps(k)
                  bder(ly)=bder(ly)+hjl*cscosps(k)
                endif
              else
                if(kfxi.ne.0) then
                  ader(lx)=ader(lx)-hjl*snsinpsx1
                  if(NCSymmRef(KPhase).eq.1)
     1              bder(lx)=bder(lx)+hjl*sncospsx1
                  if(l.eq.1) then
                    ader(ly)=ader(ly)-huhu*snsinpsx2
                    lyy=ly
                    if(NCSymmRef(KPhase).eq.1)
     1                bder(ly)=bder(ly)+huhu*sncospsx2
                  else if(l.eq.2) then
                    ader(ly)=ader(ly)-huhu*snsinpsx3
                    if(NCSymmRef(KPhase).eq.1)
     1                bder(ly)=bder(ly)+huhu*sncospsx3
                  else
                    ader(ly)=0.
                    bder(ly)=0.
                  endif
                endif
              endif
            enddo
            lx=ly
          enddo
          do k=1,kmodbi
            do l=5,10
              lx=lx+1
              ly=lx+6
              HCoefjl=HCoefj(l)
              ader(lx)=ader(lx)-HCoefjl*sncosps(k)
              ader(ly)=ader(ly)-HCoefjl*cscosps(k)
              if(NCSymmRef(KPhase).eq.1) then
                bder(lx)=bder(lx)-HCoefjl*snsinps(k)
                bder(ly)=bder(ly)-HCoefjl*cssinps(k)
              endif
            enddo
            lx=ly
          enddo
          imag=.false.
          MinusReal=.true.
          MinusImag=.true.
          lp=10
          do n=3,itfi
            nrank=TRank(n)
            if(mod(n,2).eq.1) then
              MinusReal=.not.MinusReal
            else
              MinusImag=.not.MinusImag
            endif
            imag=.not.imag
            kmod=KModA(n+1,i)
            if(kmod.le.0) go to 3900
            do k=1,kmod
              if(imag) then
                pomrx=snsinpsc(k)
                pomry=cssinpsc(k)
                pomix=sncospsc(k)
                pomiy=cscospsc(k)
              else
                pomrx=sncospsc(k)
                pomry=cscospsc(k)
                pomix=snsinpsc(k)
                pomiy=cssinpsc(k)
              endif
              if(MinusReal) then
                pomrx=-pomrx
                pomry=-pomry
              endif
              if(MinusImag) then
                pomix=-pomix
                pomiy=-pomiy
              endif
              ll=lp
              do l=1,nrank
                ll=ll+1
                lx=lx+1
                ly=lx+nrank
                HCoefjl=HCoefj(ll)
                ader(lx)=ader(lx)+HCoefjl*pomrx
                ader(ly)=ader(ly)+HCoefjl*pomry
                if(NCSymmRef(KPhase).eq.1) then
                  bder(lx)=bder(lx)+HCoefjl*pomix
                  bder(ly)=bder(ly)+HCoefjl*pomiy
                endif
              enddo
              lx=ly
            enddo
3900        lp=lp+nrank
          enddo
        endif
      endif
      go to 9999
9800  ErrFlag=1
      go to 9999
9810  ErrFlag=2
      go to 9999
9999  return
      end
