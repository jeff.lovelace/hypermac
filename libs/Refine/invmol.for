      subroutine InvMol
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*12 at,lp
      iak=NAtMolFrAll(KPhase)-1
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        isw=iswmol(i)
        iap=iak+1
        iak=iak+iam(i)/KPoint(i)
        call invat(iap,iak,isw)
        do j=1,mam(i)
          write(at,'(a8,''#'',i2)') molname(i),j
          call zhusti(at)
          ivenr(1)=at
          ji=j+(i-1)*mxp
          ipm=PrvniKiMolekuly(ji)-1
          call zmena(lBasicPar(1)(1:2)//'mol',aimol(ji),saimol(ji),2,
     1               at,ipm,1.)
          do k=1,3
            call zmena(lBasicPar(k+4),euler(k,ji),seuler(k,ji),2,at,ipm,
     1                 1.)
          enddo
          do k=1,3
            lp=lBasicPar(k+1)(1:1)//'trans'
            call zmena(lp,trans(k,ji),strans(k,ji),2,at,ipm,1.)
          enddo
          call tisk
          if(ktls(i).gt.0) then
            do k=1,6
              call indext(k,l,m)
              write(lp,117) 'T',l,m
              call zmena(lp,tt(k,ji),stt(k,ji),2,at,ipm,1.)
            enddo
            call tisk
            do k=1,6
              call indext(k,l,m)
              write(lp,117) 'L',l,m
              call zmena(lp,tl(k,ji),stl(k,ji),2,at,ipm,1.)
            enddo
            call tisk
            do k=1,9
              call indexs(k,l,m)
              write(lp,117) 'S',l,m
              call zmena(lp,ts(k,ji),sts(k,ji),2,at,ipm,1.)
            enddo
            call CrlSetTraceS(ts(1,ji),isw)
            call tisk
          endif
          if(KModM(1,ji).gt.0) then
            if(InvDel.gt.10) call tisk
            if(KFM(1,ji).ne.0) then
              lp='delta'
            else
              lp='om'
            endif
            call zmena(lp,a0m(ji),sa0m(ji),2,at,ipm,1.)
            do k=1,KModM(1,ji)
              if(KFM(1,ji).ne.0.and.k.eq.KModM(1,ji)) then
                lp='x40'
              else
                write(lp,109) 'om','sin',k
              endif
              call zmena(lp,axm(k,ji),saxm(k,ji),2,at,ipm,1.)
              if(InvDel.gt.115) call tisk
              if(KFM(1,ji).ne.0.and.k.eq.KModM(1,ji)) then
                ipm=ipm+1
                cycle
              endif
              write(lp,109) 'om','cos',k
              call zmena(lp,aym(k,ji),saym(k,ji),2,at,ipm,1.)
              if(InvDel.gt.115) call tisk
            enddo
          endif
          do k=1,KModM(2,ji)
            if(InvDel.gt.10) call tisk
            do l=1,3
              if(TypeModFunMol(ji).eq.0) then
                write(lp,113) lBasicPar(l+1)(1:1),'t','sin',k
              else
                write(lp,113) lBasicPar(l+1)(1:1),'t','ort',k*2-1
              endif
              call zhusti(lp)
              call zmena(lp,utx(l,k,ji),sutx(l,k,ji),2,at,ipm,1.)
            enddo
            do l=1,3
              if(TypeModFunMol(ji).eq.0) then
                write(lp,113) lBasicPar(l+1)(1:1),'t','cos',k
              else
                write(lp,113) lBasicPar(l+1)(1:1),'t','ort',k*2
              endif
              call zhusti(lp)
              call zmena(lp,uty(l,k,ji),suty(l,k,ji),2,at,ipm,1.)
            enddo
          enddo
          do k=1,KModM(2,ji)
            if(InvDel.gt.10) call tisk
            do l=1,3
              if(TypeModFunMol(ji).eq.0) then
                write(lp,113) lBasicPar(l+1)(1:1),'r','sin',k
              else
                write(lp,113) lBasicPar(l+1)(1:1),'r','ort',k*2-1
              endif
              call zhusti(lp)
              call zmena(lp,urx(l,k,ji),surx(l,k,ji),2,at,ipm,1.)
            enddo
            do l=1,3
              if(TypeModFunMol(ji).eq.0) then
                write(lp,113) lBasicPar(l+1)(1:1),'r','cos',k
              else
                write(lp,113) lBasicPar(l+1)(1:1),'r','ort',k*2
              endif
              call zhusti(lp)
              call zmena(lp,ury(l,k,ji),sury(l,k,ji),2,at,ipm,1.)
            enddo
          enddo
          do k=1,KModM(3,ji)
            if(InvDel.gt.10) call tisk
            do l=1,6
              call indext(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'T',m,n,'sin',k
              else
                write(lp,114) 'T',m,n,'ort',2*k-1
              endif
              call zhusti(lp)
              call zmena(lp,ttx(l,k,ji),sttx(l,k,ji),2,at,ipm,1.)
            enddo
            call tisk
            do l=1,6
              call indext(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'T',m,n,'cos',k
              else
                write(lp,114) 'T',m,n,'ort',2*k
              endif
              call zhusti(lp)
              call zmena(lp,tty(l,k,ji),stty(l,k,ji),2,at,ipm,1.)
            enddo
          enddo
          do k=1,KModM(3,ji)
            if(InvDel.gt.10) call tisk
            do l=1,6
              call indext(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'L',m,n,'sin',k
              else
                write(lp,114) 'L',m,n,'ort',2*k-1
              endif
              call zhusti(lp)
              call zmena(lp,tlx(l,k,ji),stlx(l,k,ji),2,at,ipm,1.)
            enddo
            call tisk
            do l=1,6
              call indext(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'L',m,n,'cos',k
              else
                write(lp,114) 'L',m,n,'ort',2*k
              endif
              call zhusti(lp)
              call zmena(lp,tly(l,k,ji),stly(l,k,ji),2,at,ipm,1.)
            enddo
          enddo
          do k=1,KModM(3,ji)
            if(InvDel.gt.10) call tisk
            do l=1,9
              call indexs(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'S',m,n,'sin',k
              else
                write(lp,114) 'S',m,n,'ort',2*k-1
              endif
              call zhusti(lp)
              call zmena(lp,tsx(l,k,ji),stsx(l,k,ji),2,at,ipm,1.)
            enddo
            call CrlSetTraceS(tsx(1,k,ji),isw)
            call tisk
            do l=1,9
              call indexs(l,m,n)
              if(TypeModFunMol(ji).eq.0) then
                write(lp,114) 'S',m,n,'cos',k
              else
                write(lp,114) 'S',m,n,'ort',2*k
              endif
              call zhusti(lp)
              call zmena(lp,tsy(l,k,ji),stsy(l,k,ji),2,at,ipm,1.)
            enddo
            call CrlSetTraceS(tsy(1,k,ji),isw)
          enddo
          if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1       KModM(3,ji).gt.0) then
            lp=lBasicPar(11)(:idel(lBasicPar(11)))//'m'
            call zmena(lp,phfm(ji),sphfm(ji),2,at,ipm,1.)
            call tisk
          endif
        enddo
        if(kpoint(i).gt.1) iak=iak+iam(i)-iam(i)/KPoint(i)
      enddo
      return
109   format(a2,a3,i2)
113   format(2a1,a3,i2)
114   format(a1,2i1,a3,i2)
117   format(a1,2i1)
      end
