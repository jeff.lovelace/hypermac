      subroutine LoopDistAng(klic)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 AtName(:),t80
      character*10 ButtonLables(5)
      logical   Brat(:)
      logical   ExistFile,JeModul,PrepocetAtMol
      integer FeWhatToDo,WhatToDo,ButtDflt,KModMx(3),KModMxP(3)
      dimension IAt(:),XAt(:,:),dsym(:,:),RmAt(:,:),RmiiAt(:,:),
     1          Rm6At(:,:),x40(:,:),KiAtX(:),KiAtXMod(:),DerPom(:,:,:),
     2          UxAt(:,:,:),UyAt(:,:,:),SinArgN(:,:),CosArgN(:,:),
     4          dxf(:,:,:),dxg(:,:,:),dd(3,2),tt(3),xp(6),x1(3),x2(3),
     5          dt(3),nd(3),x4(3),nt(3),dau(3),dav(3),daw(3),dbu(3),
     6          dbv(3),dcv(3),dcw(3),dtu(3),dtv(3),dtw(3),u(3),v(3),
     7          w(3),ug(3),vg(3),wgg(3),Xd(3),XdG(3),FcalcA(3),xave(3),
     8          dwdx(3,3,3),dwndx(3,3,3),DerP(3),tt0(3),FPolA(:),
     9          FCalcP(2)
      allocatable AtName,Brat,IAt,XAt,dsym,RmAt,RmiiAt,Rm6At,x40,KiAtX,
     1            KiAtXMod,DerPom,UxAt,UyAt,SinArgN,CosArgN,dxf,dxg,
     2            FPolA
      data xp/6*0./,ButtonLables/'%Skip it','S%kip all','%Use it',
     1                           'U%se all','%Cancel'/
      allocate(FPolA(2*MaxUsedKwAll+1))
      if(DoLeBail.ne.0) go to 9999
      mxkp=max(NKeepAtMax,8)
      allocate(AtName(mxkp),Brat(mxkp),IAt(mxkp),XAt(6,mxkp),
     1         dsym(6,mxkp),RmAt(9,mxkp),RmiiAt(9,mxkp),Rm6At(36,mxkp),
     2         x40(3,mxkp),KiAtX(mxkp),KiAtXMod(mxkp),DerPom(3,3,mxkp),
     3         UxAt(3,mxw,mxkp),UyAt(3,mxw,mxkp),SinArgN(mxw,mxkp),
     4         CosArgN(mxw,mxkp),dxf(3,mxkp,2),dxg(3,mxkp,2))
      ButtDflt=1
      if(klic.eq.0) NRestDistAng=0
      if(.not.ExistFile(fln(:ifln)//'.l95')) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l95','unformatted','old')
      if(ErrFlag.ne.0) go to 9999
      if(NDimI(KPhase).gt.0.or.klic.eq.0) then
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'.l96','unformatted','unknown')
        WhatToDo=0
      else
        lno=0
      endif
      if(OrthoOrd.gt.0) call trortho(0)
1000  read(ln,err=9000,end=9000) Label,n,dfix,wtr,
     1    (AtName(i),i=1,iabs(n)),
     1    (IAt(i),(RmAt(j,i),j=1,9),(Rm6At(j,i),j=1,NDimQ(KPhase)),
     2     (dsym(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),KiAtX(i),
     3     KiAtXMod(i),i=1,iabs(n))
      if(iabs(Label).eq.IdFixDistAngTors) then

      endif
      if(klic.eq.1) call SetRealArrayTo(der,ubound(der,1),0.)
      if(n.lt.0) then
        na=-n/2
      else
        na=n
      endif
      KModMx=0
      do i=1,iabs(n)
        ia=IAt(i)
        ksw=kswa(ia)
        KModMxP=0
        do k=1,KModA(2,ia)
          do j=1,NDimI(KPhase)
            if(kw(j,k,ksw).ne.0) KModMxP(j)=KModMxP(j)+1
          enddo
        enddo
        do j=1,NDimI(KPhase)
          KModMx(j)=max(KModMx(j),KModMxP(j))
        enddo
      enddo
      ntmx=1
      do i=1,3
        if(i.le.NDimI(KPhase)) then
          if(KCommen(KPhase).le.0.or.NDimI(KPhase).gt.1) then
            nt(i)=6*KModMx(i)+3
          else
            nt(i)=(ngc(KPhase)+1)*NCommQProduct(1,KPhase)
          endif
          dt(i)=1./float(nt(i))
        else
          nt(i)=1
          dt(i)=0
        endif
        ntmx=ntmx*nt(i)
      enddo
!      do i=1,3
!        if(KModMx(i).gt.0) then
!          nt(i)=6*KModMx(i)+3
!          dt(i)=1./float(nt(i))
!        else
!          nt(i)=1
!          dt(i)=0
!        endif
!        ntmx=ntmx*nt(i)
!      enddo
      RedFac=1.
      izz=0
      if(klic.eq.1.and.NDimI(KPhase).gt.0)
     1  write(lno)(LSMat(i),i=1,nLSMat),(LSRS(i),i=1,nLSRS)
2002  wrznump=0.
      if(Label.eq.IdFixPlane) then
        natm=na
        ifm=1
        Fobs=0.
      else
        natm=1
        if(dfix.gt.-900.) then
          Fobs=dfix
          ifm=1
        else
          Fobs=0.
          if(NDimI(KPhase).gt.0) then
            ifm=2
            dds=0.
            nds=0
          else
            ifm=1
          endif
        endif
      endif
      PrepocetAtMol=.true.
      do i=1,iabs(n)-1
        if(IAt(i).lt.NAtMolFr(1,1).and.kmol(IAt(i)).gt.0) go to 2015
      enddo
      PrepocetAtMol=.false.
2015  if(klic.eq.1) wt=(GOFOverall*RedFac/wtr)**2
2020  isw=iswa(IAt(1))
      JeModul=.false.
      do i=1,iabs(n)
        ia=IAt(i)
        if(isw.ne.iswa(ia)) go to 1000
        call CopyVek(x(1,ia),xp,3)
        call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
        call multm(Rm6At(1,i),xp,XAt(1,i),NDim(KPhase),NDim(KPhase),1)
        if(NDimI(KPhase).gt.0) then
          call qbyx(dsym(1,i),xp,isw)
          do j=1,NDimI(KPhase)
            xp(j)=xp(j)-dsym(j+3,i)
          enddo
          call CopyVek(qcnt(1,ia),x40(1,i),NDimI(KPhase))
          call cultm(RMiiAt(1,i),xp,x40(1,i),NDimI(KPhase),
     1               NDimI(KPhase),1)
          do j=1,KModA(2,ia)
            JeModul=.true.
            call multm(RmAt(1,i),ux(1,j,ia),UxAt(1,j,i),3,3,1)
            call multm(RmAt(1,i),uy(1,j,ia),UyAt(1,j,i),3,3,1)
          enddo
        endif
      enddo
      if(iabs(Label).eq.IdFixDistAngTors) then
        jk=na-1
      else
        jk=na
      endif
      tt0=0.
      if(KCommen(KPhase).gt.0)
     1  tt0(1:NDimI(KPhase))=trez(1:NDimI(KPhase),isw,KPhase)
      if(n.gt.0) then
        iik=1
      else
        iik=2
      endif
      do if=1,ifm
        do 7000it=1,ntmx
          call RecUnpack(it,nd,nt,NDimI(KPhase))
          do i=1,NDimI(KPhase)
            tt(i)=(nd(i)-1)*dt(i)+tt0(i)
          enddo
          do ii=1,iik
            l=0
            do j=1,jk
              jj=j+(ii-1)*na
              ia=IAt(jj)
              ksw=kswa(ia)
              call CopyVek(XAt(1,jj),x1,3)
              if(JeModul) then
                call CopyVek(x40(1,jj),x4,NDimI(KPhase))
                call cultm(RmiiAt(1,jj),tt,x4,NDimI(KPhase),
     1                     NDimI(KPhase),1)
                if(KModA(1,ia).gt.0) then
                  if(KFA(1,ia).eq.0) then
                    occ=a0(ia)
                    do kk=1,KModA(1,ia)
                      arg=0.
                      do i=1,NDimI(KPhase)
                        arg=arg+x4(i)*float(kw(i,kk,ksw))
                      enddo
                      arg=pi2*arg
                      occ=occ+ax(kk,ia)*sin(arg)+ay(kk,ia)*cos(arg)
                    enddo
                    if(occ.lt..01) go to 7000
                  else
                    kk=KModA(1,ia)-NDimI(KPhase)
                    do i=1,NDimI(KPhase)
                      kk=kk+1
                      x4p=x4(i)-ax(kk,ia)
                      ix4p=x4p
                      if(x4p.lt.0.) ix4p=ix4p-1
                      x4p=x4p-float(ix4p)
                      if(x4p.gt..5) x4p=x4p-1.
                      if(NDim(KPhase).eq.4) then
                        delta=a0(ia)*.5
                      else
                        delta=ay(kk,ia)*.5
                      endif
                      if(x4p.ge.-delta.and.x4p.le.delta) then
                        occ=1.
                      else
                        occ=0.
                        go to 7000
                      endif
                    enddo
                    if(TypeModFun(ia).gt.1) then
                      pom=x4p/Delta
                      call GetFPol(pom,FPolA,2*KModA(2,ia)+1,
     1                             TypeModFun(ia))
                    endif
                  endif
                endif
                MPol=1
                do kk=1,KModA(2,ia)
                  if(kk.lt.KModA(2,ia).or.KFA(2,ia).eq.0) then
                    if(TypeModFun(ia).le.1) then
                      arg=0.
                      do i=1,NDimI(KPhase)
                        arg=arg+x4(i)*float(kw(i,kk,ksw))
                      enddo
                      arg=pi2*arg
                      sna=sin(arg)
                      csa=cos(arg)
                    else
                      MPol=MPol+1
                      sna=FPolA(MPol)
                      MPol=MPol+1
                      csa=FPolA(MPol)
                    endif
                    do i=1,3
                      x1(i)=x1(i)+UxAt(i,kk,jj)*sna+UyAt(i,kk,jj)*csa
                    enddo
                    SinArgN(kk,jj)=sna
                    CosArgN(kk,jj)=csa
                  else
                    x4p=x4(1)-UyAt(1,kk,jj)
                    ix4p=x4p
                    if(x4p.lt.0.) ix4p=ix4p-1
                    x4p=x4p-float(ix4p)
                    if(x4p.gt..5) x4p=x4p-1.
                    znak=2.*x4p/UyAt(2,kk,jj)
                    do i=1,3
                      x1(i)=x1(i)+znak*UxAt(i,kk,jj)
                    enddo
                    SinArgN(kk,jj)=znak
                    CosArgN(kk,jj)=0.
                  endif
                enddo
              endif
              if(iabs(Label).eq.IdFixDistAngTors) then
                k=jj+1
                ia=IAt(k)
                call CopyVek(XAt(1,k),x2,3)
                if(JeModul) then
                  call CopyVek(x40(1,k),x4,NDimI(KPhase))
                  call cultm(RmiiAt(1,k),tt,x4,NDimI(KPhase),
     1                       NDimI(KPhase),1)
                  if(KModA(1,ia).gt.0) then
                    if(KFA(1,ia).eq.0) then
                      occ=a0(ia)
                      do kk=1,KModA(1,ia)
                        arg=0.
                        do i=1,NDimI(KPhase)
                          arg=arg+x4(i)*float(kw(i,kk,ksw))
                        enddo
                        arg=pi2*arg
                        occ=occ+ax(kk,ia)*sin(arg)+ay(kk,ia)*cos(arg)
                      enddo
                      if(occ.lt..01) go to 7000
                    else
                      kk=KModA(1,ia)-NDimI(KPhase)
                      do i=1,NDimI(KPhase)
                        kk=kk+1
                        x4p=x4(i)-ax(kk,ia)
                        ix4p=x4p
                        if(x4p.lt.0.) ix4p=ix4p-1
                        x4p=x4p-float(ix4p)
                        if(x4p.gt..5) x4p=x4p-1.
                        if(NDim(KPhase).eq.4) then
                          delta=a0(ia)*.5
                        else
                          delta=ay(kk,ia)*.5
                        endif
                        if(x4p.ge.-delta.and.x4p.le.delta) then
                          occ=1.
                        else
                          occ=0.
                          go to 7000
                        endif
                      enddo
                      if(TypeModFun(ia).gt.1) then
                        pom=x4p/Delta
                        call GetFPol(pom,FPolA,2*KModA(2,ia)+1,
     1                               TypeModFun(ia))
                      endif
                    endif
                  endif
                  MPol=1
                  do kk=1,KModA(2,ia)
                    if(kk.lt.KModA(2,ia).or.KFA(2,ia).eq.0) then
                      if(TypeModFun(ia).le.1) then
                        arg=0.
                        do i=1,NDimI(KPhase)
                          arg=arg+x4(i)*float(kw(i,kk,ksw))
                        enddo
                        arg=pi2*arg
                        sna=sin(arg)
                        csa=cos(arg)
                      else
                        MPol=MPol+1
                        sna=FPolA(MPol)
                        MPol=MPol+1
                        csa=FPolA(MPol)
                      endif
                      do i=1,3
                        x2(i)=x2(i)+UxAt(i,kk,k)*sna+UyAt(i,kk,k)*csa
                      enddo
                      SinArgN(kk,k)=sna
                      CosArgN(kk,k)=csa
                    else
                      x4p=x4(1)-UyAt(1,kk,k)
                      ix4p=x4p
                      if(x4p.lt.0.) ix4p=ix4p-1
                      x4p=x4p-float(ix4p)
                      if(x4p.gt..5) x4p=x4p-1.
                      znak=2.*x4p/UyAt(2,kk,k)
                      do i=1,3
                        x2(i)=x2(i)+znak*UxAt(i,kk,k)
                      enddo
                      SinArgN(kk,k)=znak
                      CosArgN(kk,k)=0.
                    endif
                  enddo
                endif
                l=l+1
                do i=1,3
                  dxf(i,l,ii)= x2(i)+dsym(i,k)
     1                        -x1(i)-dsym(i,jj)
                enddo
                call multm(MetTens(1,isw,ksw),dxf(1,l,ii),dxg(1,l,ii),
     1                     3,3,1)
                dd(l,ii)=sqrt(scalmul(dxf(1,l,ii),dxg(1,l,ii)))
              else if(Label.eq.IdFixApical.or.Label.eq.IdFixPlane) then
                do i=1,3
                  dxf(i,j,ii)=x1(i)+dsym(i,j)
                enddo
              endif
            enddo
          enddo
          if(iabs(Label).eq.IdFixDistAngTors) then
            mult=1
            Fcalc=0.
            zn=1.
            jj=1
            if(na.eq.2) then
              do ii=1,iik
                FcalcP(ii)=dd(1,ii)
                Fcalc=Fcalc+zn*FcalcP(ii)
                do i=1,3
                  DerPom(i,1,jj  )=-zn*dxg(i,1,ii)/dd(1,ii)
                  DerPom(i,1,jj+1)=-DerPom(i,1,jj)
                enddo
                jj=jj+2
                zn=-1.
              enddo
            else if(na.eq.3) then
              do ii=1,iik
                dd12=-scalmul(dxf(1,1,ii),dxg(1,2,ii))
                a1=dd(1,ii)*dd(2,ii)
                pom=dd12/a1
                if(abs(pom).gt..999999) then
                  if(pom.gt.0) then
                    FcalcP(ii)=0.
                  else
                    FcalcP(ii)=180.
                  endif
                  coef=0.
                else
                  FcalcP(ii)=acos(pom)/torad
                  coef=-zn/(torad*sqrt(1.-pom**2)*a1**2)
                endif
                Fcalc=Fcalc+zn*FcalcP(ii)
                a2=dd12*dd(2,ii)/dd(1,ii)
                a3=dd12*dd(1,ii)/dd(2,ii)
                do i=1,3
                  DerPom(i,1,jj)  =coef*( dxg(i,2,ii)*a1+dxg(i,1,ii)*a2)
                  DerPom(i,1,jj+2)=coef*(-dxg(i,1,ii)*a1-dxg(i,2,ii)*a3)
                enddo
                do i=1,3
                  DerPom(i,1,jj+1)=coef*(-(-dxg(i,1,ii)+dxg(i,2,ii))*a1
     1                                   -dxg(i,1,ii)*a2+dxg(i,2,ii)*a3)
                enddo
                jj=jj+3
                zn=-1.
              enddo
            else
              do ii=1,iik
                call CopyVek(dxf(1,1,ii),u,3)
                call CopyVek(dxf(1,2,ii),v,3)
                call CopyVek(dxf(1,3,ii),w,3)
                call CopyVek(dxg(1,1,ii),ug,3)
                call CopyVek(dxg(1,2,ii),vg,3)
                call CopyVek(dxg(1,3,ii),wgg,3)
                uu=dd(1,ii)**2
                uv=scalmul(u,vg)
                uw=scalmul(u,wgg)
                vv=dd(2,ii)**2
                vw=scalmul(v,wgg)
                ww=dd(3,ii)**2
                if(u(1)*v(2)*w(3)+u(3)*v(1)*w(2)+u(2)*v(3)*w(1)-
     1             u(3)*v(2)*w(1)-u(1)*v(3)*w(2)-u(2)*v(1)*w(3).lt.0.)
     2            then
                  znp=-1.
                else
                  znp= 1.
                endif
                pa=uv*vw-uw*vv
                pb=uu*vv-uv**2
                pc=vv*ww-vw**2
                pom1=1./(pb*pc)
                pom1=sqrt(pom1)
                pom=pom1*pa
                if(abs(pom).gt..999999) then
                  if(pom.gt.0) then
                    FcalcP(ii)=0.
                    pom= .999999
                  else
                    FcalcP(ii)=180.
                    pom=-.999999
                  endif
                else
                  FcalcP(ii)=znp*acos(pom)/torad
                endif
                Fcalc=Fcalc+zn*FcalcP(ii)
                pom2=-znp*pom1/(sqrt((1.+pom)*(1-pom))*torad)
                do i=1,3
                  dau(i)=vg(i)*vw-wgg(i)*vv
                  dav(i)=ug(i)*vw+wgg(i)*uv-2.*vg(i)*uw
                  daw(i)=vg(i)*uv-ug(i)*vv
                  dbu(i)=2.*(ug(i)*vv-vg(i)*uv)
                  dbv(i)=2.*(vg(i)*uu-ug(i)*uv)
                  dcv(i)=2.*(vg(i)*ww-wgg(i)*vw)
                  dcw(i)=2.*(wgg(i)*vv-vg(i)*vw)
                enddo
                pom3=pa*.5/pb
                pom4=pa*.5/pc
                do i=1,3
                  dtu(i)=pom2*(dau(i)-pom3*dbu(i))
                  dtv(i)=pom2*(dav(i)-pom3*dbv(i)-pom4*dcv(i))
                  dtw(i)=pom2*(daw(i)-pom4*dcw(i))
                enddo
                do i=1,3
                  DerPom(i,1,jj  )=-zn*dtu(i)
                  DerPom(i,1,jj+1)=zn*(dtu(i)-dtv(i))
                  DerPom(i,1,jj+2)=zn*(dtv(i)-dtw(i))
                  DerPom(i,1,jj+3)=zn*dtw(i)
                enddo
                jj=jj+4
                zn=-1.
              enddo
            endif
          else if(Label.eq.IdFixApical) then
            call SetRealArrayTo(Xd,3,0.)
            do i=3,na
              do j=1,3
                v(j)=dxf(j,2,1)-dxf(j,i,1)
              enddo
              call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
              call vecnor(v,vg)
              call AddVek(Xd,v,Xd,3)
            enddo
            call multm(MetTens(1,isw,KPhase),Xd,XdG,3,3,1)
            pom=1./scalmul(Xd,XdG)
            Factor=dfix*sqrt(pom)
            DFactor=-Factor*pom
            do i=1,3
              FcalcA(i)=dxf(i,2,1)+Xd(i)*Factor-dxf(i,1,1)
            enddo
            mult=3
            pom=Factor+1.
            k=0.
            do j=1,3
              do i=1,3
                k=k+1
                if(i.eq.j) then
                  DPom=pom
                  DerPom(j,i,1)=-1.
                else
                  Dpom=0.
                  DerPom(j,i,1)=0.
                endif
                DerPom(j,i,2)=Dpom+Xd(i)*DFactor*XdG(j)
              enddo
            enddo
            rn=1./float(na-2)
             Factor=- Factor*rn
            DFactor=-DFactor*rn
            k=0
            do j=1,3
              do i=1,3
                k=k+1
                if(i.eq.j) then
                  DPom=Factor
                else
                  DPom=0.
                endif
                DerPom(j,i,3)=DPom+Xd(i)*DFactor*XdG(j)
              enddo
            enddo
            Fobs=0.
          else if(Label.eq.IdFixPlane) then
            mult=1
            do i=1,3
              u(i)=dxf(i,1,1)-dxf(i,2,1)
              v(i)=dxf(i,3,1)-dxf(i,2,1)
              xave(i)=(dxf(i,1,1)+dxf(i,2,1)+dxf(i,3,1))/3.
            enddo
            call VecMul(u,v,wgg)
            call multm(MetTensI(1,isw,KPhase),wgg,w,3,3,1)
            Factor=1./sqrt(scalmul(w,wgg))
            do i=1,3
              u(i)=u(i)*Factor
              v(i)=v(i)*Factor
              w(i)=w(i)*Factor
              wgg(i)=wgg(i)*Factor
            enddo
            dwdx(1,1,1)= 0.
            dwdx(1,2,1)= v(3)
            dwdx(1,3,1)=-v(2)
            dwdx(2,1,1)=-v(3)
            dwdx(2,2,1)= 0.
            dwdx(2,3,1)= v(1)
            dwdx(3,1,1)= v(2)
            dwdx(3,2,1)=-v(1)
            dwdx(3,3,1)= 0.
            dwdx(1,1,3)= 0.
            dwdx(1,2,3)=-u(3)
            dwdx(1,3,3)= u(2)
            dwdx(2,1,3)= u(3)
            dwdx(2,2,3)= 0.
            dwdx(2,3,3)=-u(1)
            dwdx(3,1,3)=-u(2)
            dwdx(3,2,3)= u(1)
            dwdx(3,3,3)= 0.
            call AddVek(dwdx(1,1,1),dwdx(1,1,3),dwdx(1,1,2),9)
            call RealMatrixToOpposite(dwdx(1,1,2),dwdx(1,1,2),3)
            do k=1,3
              call multm(w,dwdx(1,1,k),dtw,1,3,3)
              do j=1,3
                do i=1,3
                  dwndx(i,j,k)=dwdx(i,j,k)-dtw(j)*wgg(i)
                enddo
              enddo
            enddo
          endif
          if(if.ne.ifm) then
            if(na.gt.2.and.it.gt.1) then
              if(Fcalc-FcalcOld.gt.180.) then
                Fcalc=Fcalc-360.
              else if(FcalcOld-Fcalc.gt.180.) then
                Fcalc=Fcalc+360.
              endif
            endif
            dds=dds+Fcalc
            nds=nds+1
            FcalcOld=Fcalc
            go to 7000
          else
            if(na.gt.2) then
              if(Fcalc-Fobs.gt.180.) then
                Fcalc=Fcalc-360.
              else if(Fobs-Fcalc.gt.180.) then
                Fcalc=Fcalc+360.
              endif
            endif
          endif
          do nat=1,natm
            if(Label.eq.IdFixPlane) then
              call SetLogicalArrayTo(Brat(4),n-3,.false.)
              call SetLogicalArrayTo(Brat(1),3,.true.)
              Brat(nat)=.true.
              do i=1,3
                Xd(i)=dxf(i,nat,1)-xave(i)
              enddo
              Fcalc=ScalMul(Xd,wgg)
              call SetRealArrayTo(DerPom(1,1,1),3,0.)
              call SetRealArrayTo(DerPom(1,1,2),3,0.)
              call SetRealArrayTo(DerPom(1,1,3),3,0.)
              if(nat.gt.3) call SetRealArrayTo(DerPom(1,1,nat),3,0.)
              do i=1,3
                do j=1,3
                  DerPom(j,1,i)=-wgg(j)/3.
                enddo
                call cultm(Xd,dwndx(1,1,i),DerPom(1,1,i),1,3,3)
              enddo
              call AddVek(DerPom(1,1,nat),wgg,DerPom(1,1,nat),3)
            else
              call SetLogicalArrayTo(Brat,iabs(n),.true.)
            endif
            if(klic.eq.1) then
              do m=1,mult
                if(mult.gt.1) Fcalc=FcalcA(m)
                dy=Fobs-Fcalc
                do i=1,iabs(n)
                  ia=IAt(i)
                  if(.not.Brat(i)) cycle
                  kix=KiAtX(i)
                  if(Label.eq.IdFixApical) then
                    ip=min(i,3)
                  else
                    ip=i
                  endif
                  call multm(DerPom(1,m,ip),RmAt(1,i),derp,1,3,3)
                  call AddVek(derp,der(kix),der(kix),3)
                  kip=KiAtXMod(i)
                  if(kip.gt.0) then
                    do j=1,KModA(2,ia)
                      sna=SinArgN(j,i)
                      csa=CosArgN(j,i)
                      do k=0,2
                        der(kip)=der(kip)+derp(k+1)*sna
                        kip=kip+1
                      enddo
                      do k=0,2
                        der(kip)=der(kip)+derp(k+1)*csa
                        kip=kip+1
                      enddo
                    enddo
                  endif
                  if(dfix.lt.-900..and.n.gt.0)
     1              call SetRealArrayTo(der(kix),3,0.)
                enddo
                if(PrepocetAtMol) then
                  call DSetAll
                else
                  if(KCommen(KPhase).gt.0.and.ngcMax.gt.0) call dsetgc
                  if(neq.gt.0) call dsete(1)
                  if(OrthoOrd.gt.0) call dsetor
                  if(neq.gt.0) call dsete(0)
                endif
                call SumaRefine
                if(if.eq.ifm) wrznump=wrznump+wt*dy**2
                do i=1,iabs(n)
                  call SetRealArrayTo(der(KiAtX(i)),3,0.)
                  if(KModA(2,IAt(i)).gt.0) then
                    kip=KiAtXMod(i)
                    if(kip.gt.0)
     1                call SetRealArrayTo(der(kip),6*KModA(2,IAt(i)),0.)
                  endif
                enddo
              enddo
            else if(iabs(Label).eq.IdFixDistAngTors) then
              dy=Fobs-Fcalc
              if(abs(dy).gt.wtr*20..and..not.IgnoreW) then
                Ninfo=1
                if(na.eq.2) then
                  if(n.gt.0) then
                    write(t80,100) Fcalc,Fobs
                  else
                    write(t80,100) FcalcP
                  endif
                else
                  if(n.gt.0) then
                    write(t80,101) Fcalc,Fobs
                  else
                    write(t80,101) FcalcP
                  endif
                endif
                call zhusti(t80)
                if(n.gt.0) then
                  if(na.eq.2) then
                    TextInfo(1)='distance :'
                  else
                    TextInfo(1)='angle :'
                  endif
                  TextInfo(1)='Significant discrepancy '//
     1                        t80(:idel(t80))//
     2                        ' between starting and desired value of'//
     3                        ' the '//TextInfo(1)(:idel(TextInfo(1)))
     4
                else
                  if(na.eq.2) then
                    TextInfo(1)=' between distances:'
                  else
                    TextInfo(1)=' between angles:'
                  endif
                  TextInfo(1)='Significant difference '//
     1                        t80(:idel(t80))//
     2                        TextInfo(1)(:idel(TextInfo(1)))
                endif
                if(n.gt.0) then
                  jk=1
                else
                  jk=2
                endif
                ii=0
                t80=' '
                j=0
                do jj=1,jk
                  do i=1,na
                    ii=ii+1
                    k=idel(AtName(ii))
                    t80=t80(:j)//AtName(ii)(:k)
                    j=j+k
                    if(i.ne.na) then
                      t80=t80(:j)//'-'
                      j=j+1
                    endif
                  enddo
                  if(jj.lt.jk) then
                    t80=t80(:j)//' and '
                    j=j+5
                  endif
                enddo
                TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//' '//
     1                      t80(:idel(t80))
                if(WhatToDo.eq.0) then
                  j=FeWhatToDo(-1.,-1.,ButtonLables,5,ButtDflt)
                  ButtDflt=min(5,j)
                  ButtDflt=max(1,j)
                else
                  j=WhatToDo
                endif
                if(j.eq.1) then
                  go to 1000
                else if(j.eq.2) then
                  WhatToDo=1
                  go to 1000
                else if(j.eq.3) then
                  go to 7100
                else if(j.eq.4) then
                  WhatToDo=3
                  go to 7100
                else
                  ErrFlag=1
                  go to 9000
                endif
                go to 1000
              endif
              if(.not.JeModul) go to 7100
            endif
          enddo
c          pause
7000    continue
7100    if(if.ne.ifm) then
          Fobs=dds/float(nds)
          cycle
        else

        endif
        if(klic.eq.0) then
          if(Label.eq.IdFixDistAngTors) NRestDistAng=NRestDistAng+1

          write(lno) Label,n,dfix,wtr,(AtName(i),i=1,iabs(n)),
     1      (IAt(i),(RmAt(j,i),j=1,9),(Rm6At(j,i),j=1,NDimQ(KPhase)),
     2       (dsym(j,i),j=1,NDim(KPhase)),(RmiiAt(j,i),j=1,9),KiAtX(i),
     3       KiAtXMod(i),i=1,iabs(n))
        endif
      enddo
      go to 1000
9000  close(ln)
      if(klic.eq.0) then
        close(lno)
        call MoveFile(fln(:ifln)//'.l96',fln(:ifln)//'.l95')
      else
        if(lno.gt.0) close(lno,status='delete')
      endif
      if(OrthoOrd.gt.0) call trortho(1)
9999  if(allocated(AtName))
     1  deallocate(AtName,Brat,IAt,XAt,dsym,RmAt,RmiiAt,Rm6At,x40,KiAtX,
     2             KiAtXMod,DerPom,UxAt,UyAt,SinArgN,CosArgN,dxf,dxg)
      if(allocated(FPolA)) deallocate(FPolA)
      return
100   format(f10.3,'/',f10.3)
101   format(f10.2,'/',f10.2)
      end
