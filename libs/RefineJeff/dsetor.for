      subroutine dsetor
      use Refine_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      do ia=1,NAtCalc
        if(TypeModFun(ia).ne.1.or.kswa(ia).ne.KPhase) cycle
        idp=PrvniKiAtomu(ia)-1
        itfi=itf(ia)
        iskipn=max(TRankCumul(itfi),10)
        iskipo=iskipn
        nrank=TRank(0)
        kmod =KModA(1,ia)
        kmodo=kmod
        do n=1,itfi
          ids=idp+TRankCumul(n-1)
          id=ids
          iskipn=iskipn+nrank*(2*kmod -1)
          iskipo=iskipo+nrank*(2*kmodo-1)
          if(n.eq.1.and.kmod.gt.0) then
            iskipn=iskipn+1
            iskipo=iskipo+1
          endif
          kmod =kmoda (n+1,ia)
          kmodo=kmodao(n+1,ia)
          nrank=TRank(n)
          m=0
          if(KFA(n+1,ia).eq.0.or.kmod.eq.0) then
            kmez=2*kmod+2
          else
            kmez=2*kmod
          endif
          ii=1
          do j=1,2*kmod+1
            if(OrthoSel(ii,ia).eq.j-1) then
              m=m+1
              do k=1,nrank
                dori(m,k)=der(id+k)
              enddo
              if(ii.lt.2*kmodo+1) ii=ii+1
            endif
            do k=1,nrank
              der(id+k)=0.
            enddo
            if(j.eq.1) then
              id=id+iskipn
            else
              id=id+nrank
            endif
          enddo
          if(KFA(n+1,ia).eq.0.or.kmodo.eq.0) then
            kmez=2*kmodo+2
          else
            kmez=2*kmodo
          endif
          id=ids
          do j=1,2*kmodo+1
            do k=1,nrank
              if(j.lt.kmez) then
                pom=0.
                m=j
                do l=1,j
                  pom=pom+OrthoMat(m,ia)*dori(l,k)
                  m=m+OrthoOrd
                enddo
                der(id+k)=pom
              else
                der(id+k)=dori(j,k)
              endif
            enddo
            if(j.eq.1) then
              id=id+iskipo
            else
              id=id+nrank
            endif
          enddo
        enddo
        j=idp+max(TRankCumul(itf(ia)),10)+1
        if(KModA(1,ia).gt.0) j=j+1+2*KModA(1,ia)
        k=j
        do n=2,itfi+1
          nrank=2*TRank(n-1)
          k=k+nrank*kmodao(n,ia)
          j=j+nrank*kmoda (n,ia)
        enddo
        der(k)=der(j)
        der(j)=0.
      enddo
      return
      end
