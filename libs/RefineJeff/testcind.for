      subroutine TestCInd(String,ErrString)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*(*) String,ErrString
      integer ihp(6)
      call EqStringToModEq(String,hklmnp(:maxNDim),maxNDim,
     1                     ihp,ieqp,imn,ich)
      if(ich.ne.0) then
        ErrString='incorrect condition : '//String(:idel(String))
      else
        ErrString=' '
      endif
      return
      end
