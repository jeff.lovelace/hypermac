      subroutine Inver(lst0)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      real :: DxMod(3),GammaIntInv(9),xp(11),xpp(36),cpom(3,3),
     1        rpom(3,3),upom(3),tpisq=19.7392088
      real, allocatable :: ScPom(:,:),OccArr(:),BetaArr(:,:),sctwn(:),
     1                     DerSave(:)
      character*256 Veta
      character*80  ch80,t80
      character*20  at
      character*12  lp
      character*4   t4
      integer RefGetKiInd,ipor(3),NxMod(3)
      logical m85opened,PwdCheckTOFInD,UzMame,lpom,MameDPopul
      real :: Dmat(30) = (/ 0.200, 0.200, 0.200, 0.200, 0.200,
     1                      1.040, 0.520, 0.520,-1.040,-1.040,
     2                      0.000, 0.943,-0.943, 0.000, 0.000,
     3                      1.400,-0.931,-0.931, 0.233, 0.233,
     4                      0.000, 1.110,-1.110, 0.000, 0.000,
     5                      0.000, 0.000, 0.000, 1.570,-1.570/),
     a        DmatM(110) = (/ 1.090, 0.000, 0.000, 0.000, 0.000,
     1                        1.880, 0.000, 0.000, 1.880, 0.000,
     2                        0.000, 1.090, 0.000, 0.000, 0.000,
     3                        0.000, 1.880,-1.880, 0.000, 0.000,
     4                        0.000, 0.000,-2.180, 0.000, 0.000,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000,-1.850, 1.600,
     7                        0.000, 0.000, 0.000, 0.000, 0.000,
     8                        3.680, 0.000, 0.000, 0.000, 0.000,
     9                       -1.060, 0.000, 0.000,-1.060, 0.000,
     a                        0.000, 3.680, 0.000, 0.000, 0.000,
     1                        0.000,-1.060, 1.060, 0.000, 0.000,
     2                        0.000, 0.000, 1.920, 0.000, 0.000,
     3                        0.000, 0.000, 0.000, 0.000, 0.000,
     4                        0.000, 0.000, 0.000, 2.300, 1.880,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000, 0.000, 0.000,
     7                        2.100, 0.000, 0.000,-2.100, 0.000,
     8                        0.000, 0.000, 0.000, 0.000, 0.000,
     9                        0.000, 2.100, 2.100, 0.000, 0.000,
     a                        0.000, 0.000, 0.000, 0.000, 0.000,
     1                        0.000, 0.000, 0.000, 0.000, 3.140/),
     a        dpop(10),sdpop(10)
      save DerSave
      FinalInver=.false.
      if(nSingle.gt.0) then
        if(nRFacOverall.lt.3*nParRef) then
          lstp=lst
          lst=lst0
          ch80='Number of'
          write(Cislo,FormI15) nRFacOverall
          call Zhusti(Cislo)
          ch80=ch80(:idel(ch80)+1)//Cislo(:idel(Cislo))//
     1         ' reflections is smaller than'
          write(Cislo,FormI15) nParRef
          call Zhusti(Cislo)
          if(nRFacOverall.lt.nParRef) then
            Veta='number of '//Cislo(:idel(Cislo))//
     1           ' parameters to be refined.'
            call FeChybne(-1.,-1.,ch80,Veta,SeriousError)
            go to 9900
          else
            Veta='three times number of '//Cislo(:idel(Cislo))//
     1           ' parameters to be refined.'
            if(icykl.gt.0) call SetIgnoreWTo(.true.)
            call FeChybne(-1.,-1.,ch80,Veta,WarningWithESC)
            if(icykl.gt.0) call ResetIgnoreW
            if(ErrFlag.ne.0) go to 9999
          endif
          lst=lstp
        endif
      endif
      if(NDatBlock.gt.1.or.CorrESD.eq.0) then
        gofzp=GOFOverall
      else
        gofzp=GOFOverall*BerarCorr
      endif
      if(icykl.le.0) then
        jensc=.false.
        if(nParRef.le.1) go to 1055
        ij=0
        j=0
        allocate(ScPom(mxsc,NDatBlock))
        do KDatB=1,NDatBlock
          do i=1,MxScAll
            if(KiS(i,KDatB).ne.0) then
              j=j+1
              ij=ij+j
              if(i.le.MxScU) then
                pom=LSMat(ij)
                if(pom.gt.0.) then
                  pom=1./pom
                  ScPom(i,KDatB)=LSRS(j)*pom
                  pom=gofzp**2*pom
                else
                  ScPom(i,KDatB)=0.
                  exit
                endif
                jensc=jensc.or.abs(ScPom(i,KDatB)).gt.10.*sqrt(pom)
              endif
            endif
          enddo
        enddo
1055    if(jensc) then
          Ninfo=2
          TextInfo(1)='Change for scale factor(s) too large.'
          TextInfo(2)='The cycle will be repeated with refined one(s).'
          if(.not.VisiMsgSc) call FeMsgShow(2,-1.,-1.,.true.)
          do KDatB=1,NDatBlock
            do i=1,mxscu
              if(KiS(i,KDatB).ne.0) then
                if(CorrScPwd.and.i.eq.1.and.SumaYoPwd.gt.0..and.
     1             SumaYcPwd.gt.0.) then
                  sc(i,KDatB)=sc(i,KDatB)*sqrt(SumaYoPwd/SumaYcPwd)
                  CorrScPwd=.false.
                else
                  sc(i,KDatB)=sc(i,KDatB)+ScPom(i,KDatB)
                endif
              endif
              if(sc(i,KDatB).lt.0.) sc(i,KDatB)=0.
            enddo
          enddo
          if(NAtXYZMode.gt.0) call TrXYZMode(1)
          if(NAtMagMode.gt.0) call TrMagMode(1)
          call iom40(1,0,fln(:ifln)//'.m40')
          go to 9000
        endif
      endif
      call OpenFile(85,fln(1:ifln)//'.m85','unformatted','unknown')
      m85opened=ErrFlag.eq.0
      if(m85opened) then
        write(85) neq
        do i=1,neq
          write(85) lnp(i),pab(i),npa(i)
          do j=1,npa(i)
            write(85) pnp(j,i),pko(j,i)
          enddo
        enddo
      endif
      if(nLSRS.gt.0) then
        ij=0
        do i=1,nLSRS
          ij=ij+i
          if(LSMat(ij).gt.0.) then
            if(LSMethod.eq.2) LSMat(ij)=LSMat(ij)*(1.+MarqLam)
            der(i)=1./sqrt(LSMat(ij))
          endif
        enddo
        call znorm(LSMat,der,nLSRS)
        call SmiHandleSing(LSMat,nLSRS,NSingArr,NSing)
        if(NSing.ge.2*nLSRS/3.and.NSing.gt.0) then
          go to 9600
        else if(NSing.gt.0) then
          write(ch80,'(''#'',i5)') ICykl+1
          call Zhusti(ch80)
          ch80='In the cycle '//ch80(:idel(ch80))//' there'
          if(NSing.eq.1) then
            ch80=ch80(:idel(ch80))//' was one singularity:'
          else
            write(Cislo,FormI15) NSing
            call Zhusti(Cislo)
            ch80=ch80(:idel(ch80))//' were '//Cislo(:idel(Cislo))//
     1           ' singularities:'
          endif
          write(LstSing,FormA) ch80(:idel(ch80))
          do i=1,NSing
            j=NSingArr(i)
            LSRS(j)=0.
            der(j)=1.
            write(t4,'(i4)') j
            call Zhusti(t4)
            call kdoco(-j,at,lp,0,pom,pom)
            k=RefGetKiInd(j)
            if(ki(k).gt.0) then
              lpom=LstOpened
              LstOpened=.false.
              Ch80='Singularity - element #'//t4(:idel(t4))//' : '//
     1             lp(:idel(lp))
              if(at.ne.' ') Ch80=Ch80(:idel(Ch80))//'['//at(:idel(at))//
     1                           ']'
              call FeChybne(-1.,-1.,ch80,
     1                      'The parameter will be blocked.',
     2                      WarningWithESC)
              LstOpened=lpom
              if(ErrFlag.ne.0) then
                call CloseIfOpened(85)
                go to 9999
              endif
              ch80='   '//ch80(15:idel(ch80))
              write(LstSing,FormA) ch80(:idel(ch80))
              ki(k)=ki(k)+1000
            endif
          enddo
          write(LstSing,FormA)
        endif
        call znorm(LSMat,der,nLSRS)
        ij=0
        do i=1,nLSRS
          ij=ij+i
          der(i)=1./sqrt(LSMat(ij))
        enddo
        k=0
        do j=1,nLSRS
          do i=1,j
            k=k+1
            pom=LSMat(k)*der(i)*der(j)
            if(i.lt.j.and.abs(pom).gt.corr) call piscor(i,j,pom)
            dc(i)=LSMat(k)*gofzp**2
          enddo
          if(m85opened) write(85)(dc(i),i=1,j)
        enddo
        call CloseIfOpened(85)
        call nasob(LSMat,LSRS,LSSol,nLSRS)
        call SetRealArrayTo(dc,LastNp,0.)
        call SetRealArrayTo(der,LastNp,0.)
        k=0
        iamp=0
        do ip=1,PosledniKiAtInd
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtMol,PosledniKiAtMol
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiMol,PosledniKiMol
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtXYZMode,PosledniKiAtXYZMode
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtMagMode,PosledniKiAtMagMode
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
      endif
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.l40')
      if(ExistPowder)
     1  call CopyFile(fln(:ifln)//'.m41',fln(:ifln)//'.l41')
      if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1  call CopyFile(fln(:ifln)//'.m42',fln(:ifln)//'.l42')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.l50')
      call OpenFile(75,fln(:ifln)//'.l75','unformatted','unknown')
      write(75)(ki(i),i=1,PosledniKiAtMol),
     1         (dc(i),i=1,PosledniKiAtMol),
     2         (der(i),i=1,PosledniKiAtMol)
      if(NMolec.gt.0)
     1  write(75)(ki(i),i=PrvniKiMol,PosledniKiMol),
     2           (dc(i),i=PrvniKiMol,PosledniKiMol),
     3           (der(i),i=PrvniKiMol,PosledniKiMol)
      if(NAtXYZMode.gt.0)
     1  write(75)(ki(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     2           (dc(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     3           (der(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode)
      if(NAtMagMode.gt.0)
     1  write(75)(ki(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     2           (dc(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     3           (der(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode)
      close(75)
      if(allocated(DerSave)) deallocate(DerSave)
      allocate(DerSave(ubound(der,1)))
      DerSave(:)=Der(:)
      go to 1500
      entry InverFinal(lst0)
      FinalInver=.true.
      dc(:)=0.
      Der(:)=DerSave(:)
      go to 1500
      entry InverBackDamp(lst0)
1500  write(lst,'(''changes : '')')
      ivenr(1)=' '
      write(ivenr(2),'(i4)') icykl
      ivenr(3)='  su'
      InvDel=10
      call zmena(lBasicPar(1),pom,pom,1,' ',ip,1.)
      do KDatB=1,NDatBlock
        IZdvih=(KDatB-1)*MxScAll
        isPowder=iabs(DataType(KDatB)).eq.2
        call PwdSetTOF(KDatB)
        if(NDatBlock.gt.1) then
          write(at,'(''Block'',i2)') KDatB
          call Zhusti(at)
        else
          at=' '
        endif
        ivenr(1)=at
        ip=IZdvih
        if(isPowder) then
          n=1
          if(KManBackg(KDatB).gt.0) n=n+1
        else
          n=mxscu
        endif
        do i=1,n
          if(isPowder) then
            if(i.eq.1) then
              lp='PrfScale'
            else if(i.eq.2) then
              lp='BckgScale'
            endif
          else
            write(lp,100) i
            call zhusti(lp)
          endif
          call zmena(lp,sc(i,KDatB),scs(i,KDatB),2,at,ip,1.)
          if(mod(i,10).eq.0) call tisk
        enddo
        ip=ip+mxscu-n
        call tisk
        itwph=max(NTwin,NPhase)
        if(itwph.gt.1.and.DoLeBail.eq.0) then
          allocate(sctwn(itwph))
          pomo=sctw(1,KDatB)
          sctw(1,KDatB)=1.
          do i=2,itwph
            sctw(1,KDatB)=sctw(1,KDatB)-sctw(i,KDatB)
          enddo
          sctws(1,KDatB)=0.
          lp='nic'
          do k=1,2
            if(k.eq.1) then
              i1=2
            else
              i1=1
            endif
            do i=i1,itwph
              if(k.eq.1) pom=sctw(i,KDatB)
              write(Cislo,'(i2)') i
              if(NTwin.gt.1) then
                lp='twvol'
              else
                lp='phvol'
              endif
              lp=lp(:idel(lp))//Cislo(1:2)
              call zhusti(lp)
              if(i.eq.1) then
                ivenr(1)=ivenr(1)(1:InvDel)//'  '//lp(:idel(lp))
                sctwn(1)=1.
                do j=2,itwph
                  sctwn(1)=sctwn(1)-sctwn(j)
                enddo
                call pridej(pomo,sctws(i,KDatB),sctwn(i))
              else
                call zmena(lp,sctw(i,KDatB),sctws(i,KDatB),2,at,ip,1.)
              endif
              if(k.eq.1) then
                InvDel=10
                sctwn(i)=sctw(i,KDatB)
                sctw(i,KDatB)=pom
                sctws(1,KDatB)=sctws(1,KDatB)+sctws(i,KDatB)**2
              else
                if(mod(i,10).eq.0) call tisk
              endif
            enddo
            if(k.eq.1) then
              sctws(1,KDatB)=sqrt(sctws(1,KDatB))
              ip=ip-itwph+1
            endif
          enddo
          if(InvDel.gt.10) call tisk
          deallocate(sctwn)
        endif
        ip=IZdvih+MxSc
        lp='TOverall'
        call zmena(lp,OverAllB(KDatB),OverAllBS(KDatB),2,at,ip,1.)
        if(MaxMagneticType.gt.0) then
          do i=1,2
            if(i.eq.1) then
              lp='RhoMag'
            else
              lp='GMag'
            endif
            call zmena(lp,ecMag(i,KDatB),ecsMag(i,KDatB),2,at,ip,1.)
          enddo
        else
          ip=ip+2
        endif
        lp='sclam/2'
        call zmena(lp,ScLam2(KDatB),ScLam2S(KDatB),2,at,ip,1.)
        call tisk
        if(ScLam2(KDatB).lt.0.) ScLam2(KDatB)=0.
        ip=ip+2
        ipp=ip
        if(ExtTensor(KDatB).gt.0) then
          ip=ipp
          if(ExtType(KDatB).eq.2) then
            if(ExtTensor(KDatB).eq.1) then
              call zmena(lBasicPar(9),ec(1,KDatB),ecs(1,KDatB),
     1                   2,at,ip,1.)
            else
              do j=1,6
                call indext(j,k,l)
                write(lp,110) k,l
                call zmena(lp(:idel(lp)),ec(j,KDatB),ecs(j,KDatB),
     1                     2,at,ip,1.)
              enddo
            endif
          else
            if(ExtTensor(KDatB).eq.1) then
              if(ExtType(KDatB).eq.3) then
                call zmena(lBasicPar(9),ec(1,KDatB),ecs(1,KDatB),
     1                     2,at,ip,1.)
                call tisk
                ip=ip+5
              else
                ip=ip+6
              endif
              call zmena(lBasicPar(10),ec(7,KDatB),ecs(7,KDatB),2,
     1                   at,ip,1.)
            else
              ip=ip+6
              do j=1,6
                call indext(j,k,l)
                write(lp,102) 'g',k,l
                call zmena(lp(:idel(lp)),ec(j+6,KDatB),
     1                     ecs(j+6,KDatB),2,at,ip,1.)
              enddo
            endif
          endif
          call tisk
        endif
        ipp=mxsc+18+(KdatB-1)*MxScAll
        do KPh=1,NPhase
          KPhase=KPh
          if(kanref(KDatB).ne.0) then
            ip=ipp
            do j=1,NAtFormula(KPh)
              lp='f''('//
     1           AtType(j,KPh)(:idel(AtType(j,KPh)))//')'
              call zmena(lp,ffrRef(j,KPh,KDatB),sffrRef(j,KPh,KDatB),2,
     1                   at,ip,1.)
              if(mod(j,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
            ip=ipp+MaxNAtFormula
            do j=1,NAtFormula(KPh)
              lp='f"('//
     1        AtType(j,KPh)(:idel(AtType(j,KPh)))//')'
              call zmena(lp,ffiRef(j,KPh,KDatB),sffiRef(j,KPh,KDatB),2,
     1                   at,ip,1.)
              if(mod(j,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
          endif
          ipp=ipp+2*MaxNAtFormula
        enddo
      enddo
      if(ExistPowder) then
        IZdvih=0
        do KDatB=1,NDatBlock
          isPowder=iabs(DataType(KDatB)).eq.2
          if(.not.isPowder) go to 2240
          call PwdSetTOF(KDatB)
          if(NDatBlock.gt.1) then
            write(at,'(''Block'',i2)') KDatB
            call Zhusti(at)
          else
            at=' '
          endif
          ivenr(1)=at
          ip=IShiftPwd+IZdvih+ndoffPwd-1
          do i=1,3
            if(isTOF) then
              if(KUseTOFJason(KDatB).le.0) then
                lp=lTOF1Pwd(i)
              else
                lp=lTOF2Pwd(i)
              endif
            else
              lp=lShiftPwd(i)
            endif
            call zmena(lp,ShiftPwd(i,KDatB),ShiftPwds(i,KDatB),2,at,ip,
     1                 1.)
          enddo
          if(KUseTOFJason(KDatB).ge.1) then
            do i=4,7
              call zmena(lTOF2Pwd(i),ShiftPwd(i,KDatB),
     1                   ShiftPwds(i,KDatB),2,at,ip,1.)
            enddo
          endif
          call tisk
          lp=lBackgPwd
          ip=IBackgPwd+IZdvih+ndoffPwd-1
          do i=1,NBackg(KDatB)
            write(lp(5:),105) i
            call zhusti(lp)
            call zmena(lp,BackgPwd(i,KDatB),BackgPwds(i,KDatB),
     1                 2,at,ip,1.)
            if(mod(i,10).eq.0) call tisk
          enddo
          if(InvDel.gt.10) call tisk
          if(KRough(KDatB).ne.IdPwdRoughNone) then
            lp=lRoughPwd
            ip=IRoughPwd+IZdvih+ndoffPwd-1
            do i=1,2
              write(lp(6:),105) i
              call zhusti(lp)
              call zmena(lp,RoughPwd(i,KDatB),RoughPwds(i,KDatB),2,at,
     1                   ip,1.)
            enddo
            call tisk
          endif
          if(KRefLam(KDatB).eq.1) then
            ip=ILamPwd+IZdvih+ndoffPwd-1
            do i=1,NAlfa(KDatB)
              lp=lLamPwd
              write(lp(6:),105) i
              call zhusti(lp)
              call zmena(lp,LamPwd(i,KDatB),LamPwds(i,KDatB),2,at,ip,
     1                   1.)
            enddo
            call tisk
          endif
          if((KAsym(KDatB).ne.IdPwdAsymNone.and..not.isTOF).or.
     1       (KAsym(KDatB).ne.IdPwdAsymTOFNone.and.isTOF)) then
            ip=IAsymPwd+IZdvih+ndoffPwd-1
            do i=1,NAsym(KDatB)
              pom=1.
              if(isTOF) then
                if(KAsym(KDatB).eq.IdPwdAsymTOF1) then
                  lp=lAsymTOF1Pwd(i)
                else
                  lp=lAsymTOF2Pwd(i)
                endif
              else if(KAsym(KDatB).eq.IdPwdAsymDivergence) then
                if(KUseHS(KDatB).ge.1) then
                  ii=0
                else
                  ii=2
                endif
                lp=lAsymPwdD(i+ii)
              else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                if((i.eq.1.and.KUseRSW(KDatB).eq.0).or.
     1             (i.eq.2.and.
     2              PwdMethod(KDatB).ne.IdPwdMethodBBVDS.and.
     3              PwdMethod(KDatB).ne.IdPwdMethodBBFDS).or.
     4              (i.eq.6.and.KUsePSoll(KDatBLock).eq.0).or.
     5              (i.eq.7.and.KUseSSoll(KDatBLock).eq.0)) then
                  ip=ip+1
                  cycle
                endif
                if(i.eq.2.and.PwdMethod(KDatB).eq.IdPwdMethodBBVDS) then
                  lp=lAsymPwdF(8)
                else
                  lp=lAsymPwdF(i)
                endif
              else if(KAsym(KDatB).eq.IdPwdAsymDebyeInt) then
                lp=lAsymPwdDI(i)
              else
                lp=lAsymPwd
                call NToString(i,lp(5:))
              endif
              if(KAsym(KDatB).eq.IdPwdAsymFundamental.and..not.isTOF)
     1          then
                if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                  pom=1./ToRad
                  AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*pom
                endif
              endif
              call zmena(lp,AsymPwd(i,KDatB),AsymPwds(i,KDatB),2,at,ip,
     1                   pom)
              if(KAsym(KDatB).eq.IdPwdAsymFundamental.and..not.isTOF)
     1          then
                if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                  AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*ToRad
                  AsymPwdS(i,KDatB)=AsymPwdS(i,KDatB)*ToRad
                endif
              endif
              if(mod(i,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
          endif
          if(KUseTOFAbs(KDatB).gt.0) then
            lp=lTOFAbsPwd
            ip=ITOFAbsPwd+IZdvih+ndoffPwd-1
            call zmena(lp,TOFAbsPwd(KDatB),TOFAbsPwds(KDatB),2,at,ip,
     1                 pom)
            call tisk
          endif
2240      IZdvih=IZdvih+NParRecPwd
        enddo
        IZdvihP=0
        do KPh=1,NPhase
          KPhase=KPh
          ivenr(1)=PhaseName(KPh)
          call RefCellVolume
          CellVolPwdOld=CellVolPwd
          call RefDensity(CellVolPwd,CellVolPwds,DensityPwdOld,
     1                    DensityPwdS(KPh))
          ip=ICellPwd+ndoffPwd-1+IZdvihP
          do i=1,6
            call zmena(lcell(i),CellPwd(i,KPh),CellPwds(i,KPh),2,
     1                 PhaseName(KPh),ip,1.)
          enddo
          ivenr(1)=ivenr(1)(:InvDel)//'   Volume'
          call RefCellVolume
          call pridej(CellVolPwdOld,CellVolPwds,CellVolPwd)
          call RefDensity(CellVolPwd,CellVolPwds,DensityPwd(KPh),
     1                    DensityPwdS(KPh))
          ivenr(1)=ivenr(1)(:InvDel)//'   Density'
          call pridej(DensityPwdOld,DensityPwdS(KPh),DensityPwd(KPh))
          call tisk
          ip=IQuPwd+ndoffPwd-1+IZdvihP
          do j=1,NDimI(KPhase)
            do i=1,3
              write(lp,'(''q'',i1)') i
              if(NDim(KPhase).gt.4) write(lp(3:3),'(i1)') j
              call zmena(lp,QuPwd(i,j,KPh),QuPwds(i,j,KPh),2,
     1                   PhaseName(KPh),ip,1.)
            enddo
            call tisk
          enddo
          IZdvih=IZdvihP
          do KDatB=1,NDatBlock
            call PwdSetTOF(KDatB)
            KPhase=KPh
            if(NPhase.le.1) then
              if(NDatBlock.gt.1) then
                write(at,'(''Block'',i2)') KDatB
                call Zhusti(at)
              else
                at=' '
              endif
            else
              if(NDatBlock.gt.1) then
                write(at,'(''%Block'',i2)') KDatB
                call Zhusti(at)
                at=PhaseName(KPh)(:idel(PhaseName(KPh)))//at(:idel(at))
                call Zhusti(at)
              else
                at=PhaseName(KPh)
              endif
            endif
            ivenr(1)=at
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              ip=IGaussPwd+ndoffPwd-1+IZdvih
              if(isTOF) then
                ito=3
              else
                ito=4
              endif
              do i=1,ito
                if(isTOF) then
                  Veta=lGaussPwdTOF(i)
                else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  Veta=lGaussPwdF(i)
                  pomo=GaussPwd(i,KPh,KDatB)
                else
                  Veta=lGaussPwd(i)
                endif
                call zmena(Veta,GaussPwd(i,KPh,KDatB),
     1                     GaussPwds(i,KPh,KDatB),2,at,ip,1.)
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  if((i.eq.1.and.GaussPwd(i,KPh,KDatB).gt.99999.).or.
     1               (i.eq.3.and.GaussPwd(i,KPh,KDatB).lt.0.)) then
                    go to 9610
                  endif
                endif
                if(KAsym(KDatB).eq.IdPwdAsymFundamental.and.
     1             (i.eq.1.or.i.eq.3)) then
                  if(i.eq.1) then
                    Fact=sqrt8ln2/sqrt(2.*pi)
                    Veta='DV'
                  else if(i.eq.3) then
                    Fact=.25/Fact
                    Veta='Eps'
                  endif
                  pomo=pomo*Fact
                  pom =GaussPwd (i,KPh,KDatB)*Fact
                  poms=GaussPwds(i,KPh,KDatB)*Fact
                  ivenr(1)=ivenr(1)(1:InvDel)//'  '//Veta(:idel(Veta))
                  call pridej(pomo,poms,pom)
                endif
              enddo
              call tisk
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              ip=ILorentzPwd+ndoffPwd-1+IZdvih
              if(isTOF) then
                ik=3
                if(KStrain(KPh,KDatB).eq.IdPwdStrainAxial) ik=ik+2
              else
                ik=4
              endif
              do i=1,ik
                if(isTOF) then
                  Veta=lLorentzPwdTOF(i)
                else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  Veta=lLorentzPwdF(i)
                  pomo=LorentzPwd(i,KPh,KDatB)
                else
                  Veta=lLorentzPwd(i)
                endif
                call zmena(Veta,LorentzPwd(i,KPh,KDatB),
     1                     LorentzPwds(i,KPh,KDatB),2,at,ip,1.)
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  if((i.eq.1.and.LorentzPwd(i,KPh,KDatB).gt.99999.).or.
     1               (i.eq.3.and.LorentzPwd(i,KPh,KDatB).lt.0.)) then
                    go to 9610
                  endif
                endif
                if(KAsym(KDatB).eq.IdPwdAsymFundamental.and.i.le.4) then
                  if(i.le.2) then
                    Fact=2./Pi
                    Veta='DV'
                    if(i.eq.2) Veta=Veta(:idel(Veta))//'A'
                  else if(i.le.4) then
                    Fact=Pi/8.
                    Veta='Eps'
                    if(i.eq.4) Veta=Veta(:idel(Veta))//'A'
                  endif
                  pomo=pomo*Fact
                  pom =LorentzPwd (i,KPh,KDatB)*Fact
                  poms=LorentzPwds(i,KPh,KDatB)*Fact
                  ivenr(1)=ivenr(1)(1:InvDel)//'  '//Veta(:idel(Veta))
                  call pridej(pomo,poms,pom)
                endif
              enddo
              if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
                ip=IZetaPwd+ndoffPwd-1+IZdvih
                call zmena(lZetaPwd,ZetaPwd(KPh,KDatB),
     1                     ZetaPwds(KPh,KDatB),2,at,ip,1.)
              endif
              call tisk
            endif
            if(NBroadHKLPwd(KPh,KDatB).gt.0) then
              ip=IBroadHKLPwd+ndoffPwd-1+IZdvih
              do i=1,NBroadHKLPwd(KPh,KDatB)
                write(Cislo,'(i2)') i
                lp=lBroadHKL(:idel(lBroadHKL))//Cislo(1:2)
                call zmena(lp,BroadHKLPwd (i,KPh,KDatB),
     1                        BroadHKLPwdS(i,KPh,KDatB),2,at,ip,1.)
              enddo
              call tisk
            endif
            if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
              ip=IStPwd+ndoffPwd-1+IZdvih
              lp=lStPwd
              if(NDimI(KPhase).eq.1) then
                n=35
              else
                n=15+NDimI(KPhase)
              endif
              j=0
              do i=1,n
                if(n.eq.35) then
                  call NToStrainString4(i,lp(3:))
                  if(lp(6:6).eq.'3'.or.lp(6:6).eq.'4') then
                    ip=ip+1
                    cycle
                  endif
                else
                  call NToStrainString(i,lp(3:))
                endif
                j=j+1
                call zmena(lp,StPwd(i,KPh,KDatB),StPwds(i,KPh,KDatB),2,
     1                     at,ip,1.)
                if(mod(j,10).eq.0) call tisk
              enddo
              call tisk
            endif
            if(KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              ip=IPrefPwd+ndoffPwd-1+IZdvih
              lp=lPrefPwd
              do i=1,2
                call NToString(i,lp(5:))
                call zmena(lp,PrefPwd(i,KPh,KDatB),
     1                     PrefPwds(i,KPh,KDatB),2,at,ip,1.)
              enddo
              call tisk
            endif
            IZdvih=IZdvih+NParProfPwd
          enddo
          IZdvihP=IZdvihP+NParCellProfPwd
        enddo
      endif
      do KDatB=1,NDatBlock
        ip=ndoffED+MxEDRef*NMaxEDZone*(KDatB-1)
        if(Radiation(KDatB).ne.ElectronRadiation.or.
     1     .not.ExistM42.or..not.CalcDyn) cycle
        do i=1,NEDZone(KDatBlock)
          if(.not.UseEDZone(i,KDatB)) then
            ip=ip+6
            cycle
          endif
          write(at,'(''Zone#'',i5)') i
          call Zhusti(at)
          if(NDatBlock.gt.1) then
            write(Cislo,'(''%'',i5)') KDatB
            call Zhusti(Cislo)
            at=at(:idel(at))//Cislo(:idel(Cislo))
          endif
          ivenr(1)=at
          do j=1,6
            if(j.eq.1) then
              call Zmena(lEDVar(j),ScEDZone(i,KDatB),ScEDZoneS(i,KDatB),
     1                   2,at,ip,1.)
            else if(j.eq.2) then
              call Zmena(lEDVar(j),ThickEDZone(i,KDatB),
     1                   ThickEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.3) then
              call Zmena(lEDVar(j),XNormEDZone(i,KDatB),
     1                   XNormEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.4) then
              call Zmena(lEDVar(j),YNormEDZone(i,KDatB),
     1                   YNormEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.5) then
              call Zmena(lEDVar(j),PhiEDZone(i,KDatB),
     1                   PhiEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.6) then
              call Zmena(lEDVar(j),ThetaEDZone(i,KDatB),
     1                   ThetaEDZoneS(i,KDatB),2,at,ip,1.)
            endif
          enddo
          call tisk
        enddo
      enddo
      if(DoLeBail.le.0) then
        ip=ndoff
        if(NAtXYZMode.gt.0) call TrXYZMode(1)
        if(NAtMagMode.gt.0) call TrMagMode(1)
        do KPh=1,NPhase
          KPhase=KPh
          if(Nphase.gt.1) write(lst,'(''label : '',a8)') PhaseName(KPh)
          do isw=1,NComp(KPh)
            call invat(NAtIndFr(isw,KPh),NAtIndTo(isw,KPh),isw)
          enddo
          call invmol
        enddo
        ip=PrvniKiAtXYZMode-1
        do i=1,NAtXYZMode
          At=Atom(IAtXYZMode(1,i))
          ivenr(1)=at
          do j=1,NMAtXYZMode(i)
            call Zmena(LAtXYZMode(j,i),AtXYZMode(j,i),SAtXYZMode(j,i),2,
     1                 at,ip,1.)
            if(InvDel.gt.115) call tisk
          enddo
          if(InvDel.gt.10) call tisk
        enddo
        ip=PrvniKiAtMagMode-1
        do i=1,NAtMagMode
          At=Atom(IAtMagMode(1,i))
          ivenr(1)=at
          do j=1,NMAtMagMode(i)
            call Zmena(LAtMagMode(j,i),AtMagMode(j,i),SAtMagMode(j,i),2,
     1                 at,ip,1.)
            if(InvDel.gt.115) call tisk
          enddo
          if(InvDel.gt.10) call tisk
        enddo
      endif
      call zmena(lBasicPar(1),pom,pom,3,' ',ip,1.)
      call RefAppEq(0,0,0)
      if(nvai.gt.0) call SetRes(0)
      call RefKeepGRigidUpdate
      if(nKeep.gt.0) call RefSetKeep
      if(.not.isPowder.or.DoLeBail.eq.0) then
        if(NAtXYZMode.gt.0) call TrXYZMode(0)
        if(NAtMagMode.gt.0) call TrMagMode(0)
      endif
!      call RefKeepGRigidUpdate
      if(ExistElectronData.and.ExistM42.and.CalcDyn) call SetElDyn(1,0)
      if(ChargeDensities.and.lasmaxm.gt.0) then
        if(iaute.le.1) then
          if(iaute.eq.1) call RefPopvSet
          call RefPopvEpilog
        else if(iaute.eq.2) then
          call RefPopvRenorm
        endif
      endif
      call RefFixOriginRenorm
      call iom40(1,0,fln(:ifln)//'.m40')
      if(allocated(ScRes)) then
        do j=1,NDatBlock
          if(Radiation(j).ne.ElectronRadiation.or.
     1       .not.ExistM42.or..not.CalcDyn) cycle
          do i=1,mxsc
            pom=Sc(i,j)
            Sc(i,j)=ScRes(i,j)
            ScRes(i,j)=pom
          enddo
        enddo
      endif
      if(ErrFlag.ne.0) go to 9900
      if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1  call iom42(1,0,fln(:ifln)//'.m42')
      call RefScreenDatBlocks
      if(ExistPowder) call comsym(0,0,ich)
      call RefSetMatrixLimits(0)
      do KDatB=1,NDatBlock
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      n=0
      do KPh=1,NPhase
        do isw=1,NComp(KPh)
          do ik=1,2
            if(ik.eq.1) then
              ia1=NAtIndFr(isw,KPh)
              ia2=NAtIndTo(isw,KPh)
            else
              ia1=NAtPosFr(isw,KPh)
              ia2=NAtPosTo(isw,KPh)
            endif
            do ia=ia1,ia2
              if(itf(ia).eq.2) n=n+1
            enddo
          enddo
        enddo
      enddo
      if(n.le.0) go to 4100
      write(lst,'(''eigenvalues : '')')
      do KPh=1,NPhase
        KPhase=KPh
        if(NPhase.gt.1) then
          write(lst,FormA)
          write(lst,FormA) 'Phase: '//
     1                     PhaseName(KPh)(:idel(PhaseName(KPh)))
          write(lst,FormA)
        endif
        do isw=1,NComp(KPh)
          write(lst,FormA)
          Veta='Eigenvalues and eigenvectors are related to the '//
     1         'orthonormal system defined by the following '//
     2         'transformation matrix:'
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          do i=1,3
            write(lst,'(25x,3f12.4)')(TrToOrtho(j,isw,KPh),j=i,9,3)
          enddo
          write(lst,FormA)
          Veta='Atom           Eigenvalues               Eigenvectors'
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          call srotb(TrToOrtho(1,isw,KPh),TrToOrtho(1,isw,KPh),xpp)
          do ik=1,2
            if(ik.eq.1) then
              ia1=NAtIndFr(isw,KPh)
              ia2=NAtIndTo(isw,KPh)
            else
              ia1=NAtPosFr(isw,KPh)
              ia2=NAtPosTo(isw,KPh)
            endif
            do ia=ia1,ia2
              if(itf(ia).ne.2) cycle
              call MultM(xpp,beta(1,ia),xp,6,6,1)
              do j=1,6
                xp(j)=xp(j)/tpisq
              enddo
              do m=1,6
                call indext(m,j,k)
                cpom(j,k)=xp(m)
                if(j.ne.k) cpom(k,j)=xp(m)
              enddo
              call qln(cpom,rpom,upom,3)
              ipor(1)=1
              pommn=upom(1)
              ipor(3)=1
              pommx=upom(1)
              do i=2,3
                if(upom(i).gt.pommx) then
                  pommx=upom(i)
                  ipor(3)=i
                endif
                if(upom(i).le.pommn) then
                  pommn=upom(i)
                  ipor(1)=i
                endif
              enddo
              ipor(2)=6-ipor(1)-ipor(3)
              write(lst,'(a8,6x,f10.6,8x,3f10.6/
     1                   2(14x,f10.6,8x,3f10.6/))')
     2          atom(ia),(upom(ipor(i)),(rpom(j,ipor(i)),j=1,3),i=1,3)
            enddo
          enddo
        enddo
      enddo
4100  write(lst,'(''messages : '')')
      if(DoLeBail.ne.0) go to 4200
      do KDatB=1,NDatBlock
        if(.not.UseDatBlockActual(KDatB)) cycle
        if(ExtType(KDatB).eq.3) then
          if(ec(1,KDatB).gt.999.*ec(7,KDatB)) then
            ec(1,KDatB)=1000.*ec(7,KDatB)
            kis(MxSc+8,KDatB)=0
          else if(ec(7,KDatB).gt.999.*ec(1,KDatB)) then
            ec(7,KDatB)=1000.*ec(1,KDatB)
            kis(MxSc+14,KDatB)=0
          endif
        endif
        if(ExtType(KDatB).eq.1) then
          i=7
        else
          i=1
        endif
        if(ExtTensor(KDatB).le.0) then
          cycle
        else if(ExtTensor(1).eq.1) then
          if(ec(i,KDatB).lt.0.) write(lst,103)
        else if(ExtTensor(1).eq.2) then
          if(ec(i,KDatB).le.0..or.
     1       ec(i,KDatB)*ec(i+1,KDatB)-ec(i+3,KDatB)**2.le.0..or.
     2       ec(i,KDatB)*ec(i+1,KDatB)*ec(i+2,KDatB)+
     3       2.*ec(i+3,KDatB)*ec(i+4,KDatB)*ec(i+5,KDatB)
     4       -ec(i,KDatB)*ec(i+5,KDatB)**2
     5       -ec(i+1,KDatB)*ec(i+4,KDatB)**2
     6       -ec(i+2,KDatB)*ec(i+3,KDatB)**2.le.0.) write(lst,104)
        endif
      enddo
4200  do KDatB=1,NDatBlock
        if(UseDatBlockActual(KDatB)) then
          isPowder=iabs(DataType(KDatB)).eq.2
          call PwdSetTOF(KDatB)
          do i=1,mxscu
            if(sc(i,KDatB).lt.0.) sc(i,KDatB)=0.
          enddo
        endif
      enddo
      if(ChargeDensities.and.lasmaxm.ge.3) then
        write(lst,'(''dpopulations : '')')
        do KPh=1,NPhase
          KPhase=KPh
          MameDPopul=.false.
          do ia=1,NAtCalc
            if(kswa(ia).ne.KPh) cycle
            j=AtNum(isf(ia),KPh)
            if((j.ge.21.or.j.eq.15.or.j.eq.16).and.lasmax(ia).ge.3) then
              if(.not.MameDPopul) then
                if(NPhase.gt.1) then
                  write(lst,FormA)
                  write(lst,FormA) 'Phase: '//
     1                             PhaseName(KPh)(:idel(PhaseName(KPh)))
                  write(lst,FormA)
                endif
                MameDPopul=.true.
              endif
              xp(1)=popv(ia)+popas(1,ia)
              xp(2)=popas(5,ia)
              xp(3)=popas(8,ia)
              xp(4)=popas(17,ia)
              xp(5)=popas(20,ia)
              xp(6)=popas(24,ia)
              call MultM(DMat,xp,dpop,5,6,1)
              xp(1)=spopv(ia)+spopas(1,ia)
              xp(2)=spopas(5,ia)
              xp(3)=spopas(8,ia)
              xp(4)=spopas(17,ia)
              xp(5)=spopas(20,ia)
              xp(6)=spopas(24,ia)
              call MultMQ(DMat,xp,sdpop,5,6,1)
              SumDpop=0.
              do j=1,5
                SumDpop=SumDPop+DPop(j)
              enddo
              write(lst,111)
              write(lst,112) Atom(ia),dpop(1:5),SumDPop
              write(lst,113) sdpop(1:5)
              xp(1)=popas(6,i)
              xp(2)=popas(7,i)
              xp(3)=popas(8,i)
              xp(4)=popas(9,i)
              xp(5)=popas(18,i)
              xp(6)=popas(19,i)
              xp(7)=popas(20,i)
              xp(8)=popas(21,i)
              xp(9)=popas(22,i)
              xp(10)=popas(23,i)
              xp(11)=popas(25,i)
              call MultM(DMatM,xp,dpop,10,11,1)
              xp(1)=spopas(6,i)
              xp(2)=spopas(7,i)
              xp(3)=spopas(8,i)
              xp(4)=spopas(9,i)
              xp(5)=spopas(18,i)
              xp(6)=spopas(19,i)
              xp(7)=spopas(20,i)
              xp(8)=spopas(21,i)
              xp(9)=spopas(22,i)
              xp(10)=spopas(23,i)
              xp(11)=spopas(25,i)
              call MultMQ(DMatM,xp,sdpop,10,11,1)
              write(lst,FormA)
              write(lst,114)
              write(lst,112) Atom(ia),dpop(1:10)
              write(lst,113) sdpop(1:10)
              write(lst,FormA)
            endif
          enddo
        enddo
      endif
      AtDisableNNew=0
      UzMame=.false.
      do ia=1,NAtAll
        if(ia.ge.NAtPosFr(1,1).and.ia.lt.NAtMolFr(1,1)) cycle
        if(ai(ia).le.0..or.itf(ia).eq.0) cycle
        isw=iswa(ia)
        if(NDimI(KPhase).gt.0.and.KModA(3,ia).gt.0) then
          if(.not.UzMame) then
            n=1
            do i=1,NDimI(KPhase)
              DxMod(i)=.01
              NxMod(i)=100
              n=n*100
            enddo
            if(Allocated(OccArr)) deallocate(OccArr,BetaArr)
            allocate(OccArr(n),BetaArr(6,n))
            call UnitMat(GammaIntInv,NDimI(KPhase))
          endif
          call SetRealArrayTo(xpp,NDimI(KPhase),0.)
          call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(OccArr,qcnt(1,ia),xp,NxMod,DxMod,a0(ia),
     1                      ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                      GammaIntInv)
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1        call MakeOccModSawTooth(OccArr(1),qcnt(1,ia),xp,NxMod,
     2                                DxMod,uy(1,KModA(2,ia),ia),
     3                                uy(2,KModA(2,ia),ia),KFA(2,ia),
     4                                GammaIntInv)
          else
            call MakeOccModPol(OccArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv,TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakeBetaMod(BetaArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                       bx(1,1,ia),by(1,1,ia),KModA(3,ia),
     2                       KFA(3,ia),GammaIntInv)
          else
            call MakeBetaModPol(BetaArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                          bx(1,1,ia),by(1,1,ia),KModA(3,ia),
     2                          ax(1,ia),a0(ia)*.5,GammaIntInv,
     3                          TypeModFun(ia))
          endif
          do i=1,n
            if(OccArr(i).le.0.) cycle
            call AddVek(Beta(1,ia),BetaArr(1,i),xpp,6)
            if(xpp(1).lt.0..or.
     1         xpp(1)*xpp(2)-xpp(4)**2.le.0..or.
     2         xpp(1)*xpp(2)*xpp(3)-xpp(3)*xpp(4)**2-
     2         xpp(1)*xpp(6)**2-xpp(2)*xpp(5)**2+
     3          2.*xpp(4)*xpp(5)*xpp(6).le.0.) then
              write(lst,108) atom(ia)(:idel(atom(ia)))
              exit
            endif
          enddo
        else
          if(beta(1,ia).lt.0.) then
            write(lst,107) atom(ia)(:idel(atom(ia)))
          else if(itf(ia).eq.1) then
            if(AtDisableFact(ia).gt.0.) then
              if(UisoLim.gt.0..and.ai(ia).gt.0.and.
     1           beta(1,ia).gt.UisoLim*episq*AtDisableFact(ia)) then
                ai(ia)=0.
                call SetIntArrayTo(ki(PrvniKiAtomu(ia)),
     1                             DelkaKiAtomu(ia),0)
                call SetIntArrayTo(KiA(1,ia),DelkaKiAtomu(ia),0)
                AtDisable(ia)=.true.
              endif
              if(AtDisable(ia)) AtDisableNNew=AtDisableNNew+1
            endif
          else if(itf(ia).ne.1) then
           if(beta(1,ia)*beta(2,ia)-beta(4,ia)**2.le.0..or.
     1        beta(1,ia)*beta(2,ia)*beta(3,ia)-beta(3,ia)*beta(4,ia)**2-
     2        beta(1,ia)*beta(6,ia)**2-beta(2,ia)*beta(5,ia)**2+
     3        2.*beta(4,ia)*beta(5,ia)*beta(6,ia).le.0.)
     4        write(lst,107) atom(ia)(:idel(atom(ia)))
          endif
        endif
      enddo
      if(UisoLim.gt.0.) call RefSetMatrixLimits(0)
9000  if(DoLeBail.ne.0) then
        do KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).eq.2) then
            pom=sc(1,KDatB)
            sc(1,KDatB)=scPwd(KDatB)
            scPwd(KDatB)=pom
          endif
        enddo
      endif
      if(allocated(ScRes)) then
        do j=1,NDatBlock
          if(Radiation(j).ne.ElectronRadiation.or.
     1       .not.ExistM42.or..not.CalcDyn) cycle
          do i=1,mxsc
            pom=Sc(i,j)
            Sc(i,j)=ScRes(i,j)
            ScRes(i,j)=pom
          enddo
        enddo
      endif
      go to 9999
9600  lst=lst0
      write(Cislo,FormI15) NSing
      call Zhusti(Cislo)
      ch80='The number of '//Cislo(:idel(Cislo))//
     1     ' corrections for singularities too large.'
      go to 9800
9610  call kdoco(ip,at,lp,0,pom,pom)
      ch80='The parameter '//lp(:idel(lp))//'['//at(:idel(at))//']'//
     1     ' reached unreallistic value.'
9800  call FeChybne(-1.,-1.,'numerical problem in INVER.',ch80,
     1              SeriousError)
9900  ErrFlag=1
9999  if(allocated(ScPom)) deallocate(ScPom)
      return
100   format('scale',i2)
101   format('#',i2)
102   format(a1,2i1)
103   format(' extinction parameter has negative value')
104   format(' extinction tensor is not positive definite')
105   format(i2)
107   format('Harmonic ADP tensor of the atom "',a,
     1       '" is not positive definite')
108   format('Modulated harmonic ADP tensor of the atom "',a,
     1       '" is not positive in some regions'/
     2       '   For more details run "Grapht" and draw U(min) value')
110   format('rho',2i1)
111   format('Atom         z2        xz        yz       x2-y2      xy',
     1       '      tot d-pop')
112   format(a8,11f10.4)
113   format('s.u.    ',11f10.4)
114   format('Atom        z2/xz     z2/yz    z2/x2-y2   z2/xy     xz/xy'
     1      ,'    xz/x2-y2   xz/yz    yz/x2-y2   yz/xy    x2-y2/xy')
      end
