      subroutine KdoCo(ii,at,pn,iz,p,sp)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ifa(6)
      character*(*) at,pn
      character*3 cs
      character*1 cp
      integer RefGetKiInd
      at=' '
      pn=' '
      i=0
      if(iz.ge.0) sp=0.
      if(ii.lt.0) then
        k=RefGetKiInd(-ii)
        if(k.le.0) return
      else
        k=ii
      endif
1100  if(k.le.MxScAll*MxDatBlock) then
        KDatB=(k-1)/MxScAll+1
        k=mod(k-1,MxScAll)+1
        KPh=0
        if(KDatB.gt.NDatBlock) go to 8000
        if(k.le.MxScU) then
          if(iabs(DataType(KDatB)).eq.2.and.k.le.2) then
            if(k.eq.1) then
              pn='PrfScale'
            else if(k.eq.2) then
              pn='BckgScale'
            endif
          else
            write(pn,'(''scale'',i2)') k
          endif
          if(iz.gt.0) p=sc(k,KDatB)
          if(iz.lt.0) sc(k,KDatB)=p
        else if(k.le.MxScUTw) then
          k=k-mxscutw+itwph
          write(Cislo,'(i2)') k
          if(NTwin.gt.1) then
            pn='twvol'
          else
            pn='phvol'
          endif
          pn=pn(:idel(pn))//Cislo(1:2)
          if(iz.gt.0) p=sctw(k,KDatB)
          if(iz.lt.0) sctw(k,KDatB)=p
        else if(k.le.MxSc) then
          write(pn,'(''scale'',i2)') k
          if(iz.gt.0) p=sc(k,KDatB)
          if(iz.lt.0) sc(k,KDatB)=p
        else if(k.le.MxSc+1) then
          pn='TOverall'
          if(iz.gt.0) p=OverAllB(KDatB)
          if(iz.lt.0) OverAllB(KDatB)=p
        else if(k.le.MxSc+3) then
          k=k-MxSc-1
          if(k.eq.1) then
            pn='RhoMag'
          else
            pn='GMag'
          endif
          if(iz.gt.0) p=ecMag(k,KDatB)
          if(iz.lt.0) ecMag(k,KDatB)=p
        else if(k.le.MxSc+4) then
          pn='sclam/2'
          if(iz.gt.0) then
            p=ScLam2(KDatB)
            ap=ScLam2S(KDatB)
          endif
          if(iz.lt.0) then
            ScLam2(KDatB)=p
            ScLam2S(KDatB)=sp
          endif
        else if(k.le.MxSc+6) then
          pn='rezerva'
        else if(k.le.MxSc+18) then
          k=k-MxSc-6
          if(ExtTensor(KDatB).eq.1) then
            if(k.eq.1) then
              pn=lBasicPar(9)
            else
              pn=lBasicPar(10)
            endif
          else
            if(k.le.6) then
              call indext(k,i,j)
              write(pn,110) i,j
            else
              call indext(k-6,i,j)
              write(pn,114) 'g',i,j
            endif
          endif
          call zhusti(pn)
          if(iz.gt.0) p=ec(k,KDatB)
          if(iz.lt.0) ec(k,KDatB)=p
        else
          k=k-mxsc-18
          KPh=min((k-1)/(2*MaxNAtFormula)+1,NPhase)
          k=mod(k-1,2*MaxNAtFormula)+1
          j=(k-1)/MaxNAtFormula+1
          i=mod(k-1,MaxNAtFormula)+1
          if(j.eq.1) then
            pn='f''('
            if(iz.gt.0) p=ffrRef(i,KPh,KDatB)
            if(iz.lt.0) ffrRef(i,KPh,KDatB)=p
          else
            pn='f"('
            if(iz.gt.0) p=ffiRef(i,KPh,KDatB)
            if(iz.lt.0) ffiRef(i,KPh,KDatB)=p
          endif
          if(i.le.NAtFormula(KPh)) then
            Cislo=AtType(i,KPh)
          else
            Cislo='--'
          endif
          pn=pn(1:3)//Cislo(:idel(Cislo))//')'
        endif
        go to 8000
      else if(k.le.ndoffED) then
        k=k-ndoffPwd
        if(k.lt.ICellPwd) then
          KPh=0
          KDatB=(k-IShiftPwd)/NParRecPwd+1
          call PwdSetTOF(KDatB)
          if(KDatB.gt.NDatBlock) go to 8000
          k=mod(k-IShiftPwd,NParRecPwd)+IShiftPwd
          if(k.ge.IShiftPwd.and.k.le.IBackgPwd-1) then
            if(iz.gt.0) then
              p=ShiftPwd(k,KDatB)
              sp=ShiftPwds(k,KDatB)
            else if(iz.lt.0) then
              ShiftPwd(k,KDatB)=p
              ShiftPwds(k,KDatB)=sp
            endif
            if(isTOF) then
              if(KUseTOFJason(KDatB).le.0) then
                pn=lTOF1Pwd(k)
              else
                pn=lTOF2Pwd(k)
              endif
            else
              if(k.le.3) then
                pn=lShiftPwd(k)
              else
                pn=' '
              endif
            endif
            go to 8000
          else if(k.ge.IBackgPwd.and.k.le.IRoughPwd-1) then
            k=k-IBackgPwd+1
            if(iz.gt.0) then
              p=BackgPwd(k,KDatB)
              sp=BackgPwds(k,KDatB)
            else if(iz.lt.0) then
              BackgPwd(k,KDatB)=p
              BackgPwds(k,KDatB)=sp
            endif
            pn=lBackgPwd
          else if(k.ge.IRoughPwd.and.k.le.ILamPwd-1) then
            pn=lRoughPwd
            k=k-IRoughPwd+1
            if(iz.gt.0) then
              p=RoughPwd(k,KDatB)
              sp=RoughPwds(k,KDatB)
            else if(iz.lt.0) then
              RoughPwd(k,KDatB)=p
              RoughPwds(k,KDatB)=sp
            endif
          else if(k.ge.ILamPwd.and.k.le.IAsymPwd-1) then
            pn=lLamPwd
            k=k-ILamPwd+1
            if(iz.gt.0) then
              p=LamPwd(k,KDatB)
              sp=LamPwds(k,KDatB)
            else if(iz.lt.0) then
              LamPwd(k,KDatB)=p
              LamPwds(k,KDatB)=sp
            endif
          else if(k.ge.IAsymPwd.and.k.le.ITOFAbsPwd-1) then
            k=k-IAsymPwd+1
            if(iz.gt.0) then
              p=AsymPwd(k,KDatB)
              sp=AsymPwds(k,KDatB)
            else if(iz.lt.0) then
              AsymPwd(k,KDatB)=p
              AsymPwds(k,KDatB)=sp
            endif
            if(isTOF) then
              if(KAsym(KDatB).eq.IdPwdAsymTOF1) then
                if(k.le.4) then
                  pn=lAsymTOF1Pwd(k)
                else
                  pn=' '
                endif
              else
                if(k.le.8) then
                  pn=lAsymTOF2Pwd(k)
                else
                  pn=' '
                endif
              endif
            else if(KAsym(KDatB).eq.IdPwdAsymDivergence) then
              if(k.le.2) then
                if(KUseHS(KDatB).ge.1) then
                  kk=0
                else
                  kk=2
                endif
                pn=lAsymPwdD(k+kk)
              else
                pn=' '
              endif
            else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
              if(k.eq.2.and.PwdMethod(KDatB).eq.IdPwdMethodBBVDS) then
                pn=lAsymPwdF(8)
              else
                pn=lAsymPwdF(k)
              endif
            else if(KAsym(KDatB).eq.IdPwdAsymDebyeInt) then
              pn=lAsymPwdDI(k)
            else
              pn=lAsymPwd
              call NToString(k,pn(5:))
            endif
            go to 8000
          else if(k.ge.ITOFAbsPwd.and.k.le.ICellPwd-1) then
            pn=lTOFAbsPwd
            if(iz.gt.0) then
              p=TOFAbsPwd(KDatB)
              sp=TOFAbsPwds(KDatB)
            else if(iz.lt.0) then
              TOFAbsPwd(KDatB)=p
              TOFAbsPwds(KDatB)=sp
            endif
            go to 8000
          endif
          go to 1500
        endif
        KPh=(k-ICellPwd)/NParCellProfPwd+1
        k=mod(k-ICellPwd,NParCellProfPwd)+ICellPwd
        if(NPhase.gt.1) At=PhaseName(KPh)
        if(k.ge.ICellPwd.and.k.le.IGaussPwd-1) then
          KDatB=0
          k=k-ICellPwd+1
          if(k.le.6) then
            pn=lcell(k)
            if(iz.gt.0) then
              p=CellPwd(k,KPh)
              sp=CellPwds(k,KPh)
            else if(iz.lt.0) then
              CellPwd(k,KPh)=p
              CellPwds(k,KPh)=sp
            endif
          else
            k=k-6
            i=mod(k-1,3)+1
            j=(k-1)/3+1
            write(pn,114) 'q',i,j
            if(NDim(KPhase).eq.4.and.j.eq.1) pn(3:3)=' '
            if(iz.gt.0) then
              p=QuPwd(i,j,KPh)
              sp=QuPwds(i,j,KPh)
            else if(iz.lt.0) then
              QuPwd(i,j,KPh)=p
              QuPwds(i,j,KPh)=sp
            endif
          endif
          go to 8000
        endif
        KDatB=(k-IGaussPwd)/NParProfPwd+1
        k=mod(k-IGaussPwd,NParProfPwd)+IGaussPwd
        if(k.ge.IGaussPwd.and.k.le.ILorentzPwd-1) then
          k=k-IGaussPwd+1
          if(iz.gt.0) then
            p=GaussPwd(k,KPh,KDatB)
            sp=GaussPwds(k,KPh,KDatB)
          else if(iz.lt.0) then
            GaussPwd(k,KPh,KDatB)=p
            GaussPwds(k,KPh,KDatB)=sp
          endif
          if(isTOF) then
            if(k.le.3) then
              pn=lGaussPwdTOF(k)
            else
              pn=' '
            endif
          else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
            pn=lGaussPwdF(k)
          else
            pn=lGaussPwd(k)
          endif
        else if(k.ge.ILorentzPwd.and.k.le.IZetaPwd-1) then
          k=k-ILorentzPwd+1
          if(iz.gt.0) then
            p=LorentzPwd(k,KPh,KDatB)
            sp=LorentzPwds(k,KPh,KDatB)
          else if(iz.lt.0) then
            LorentzPwd(k,KPh,KDatB)=p
            LorentzPwds(k,KPh,KDatB)=sp
          endif
          if(isTOF) then
            pn=lLorentzPwdTOF(k)
          else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
            pn=lLorentzPwdF(k)
          else
            pn=lLorentzPwd(k)
          endif
        else if(k.ge.IZetaPwd.and.k.le.IStPwd-1) then
          k=k-IZetaPwd+1
          if(iz.gt.0) then
            p=ZetaPwd(KPh,KDatB)
            sp=ZetaPwds(KPh,KDatB)
          else if(iz.lt.0) then
            ZetaPwd(KPh,KDatB)=p
            ZetaPwds(KPh,KDatB)=sp
          endif
          pn=lZetaPwd
        else if(k.ge.IStPwd.and.k.le.IPrefPwd-1) then
          pn=lStPwd
          k=k-IstPwd+1
          if(NDimI(KPh).eq.1) then
            call NToStrainString4(k,pn(3:))
          else
            call NToStrainString(k,pn(3:))
          endif
          if(iz.gt.0) then
            p=StPwd(k,KPh,KDatB)
            sp=StPwds(k,KPh,KDatB)
          else if(iz.lt.0) then
            StPwd(k,KPh,KDatB)=p
            StPwds(k,KPh,KDatB)=sp
          endif
        else if(k.ge.IPrefPwd.and.k.le.IPrefPwd+1) then
          k=k-IPrefPwd+1
          if(iz.gt.0) then
            p=PrefPwd(k,KPh,KDatB)
            sp=PrefPwds(k,KPh,KDatB)
          else if(iz.lt.0) then
            PrefPwd(k,KPh,KDatB)=p
            PrefPwds(k,KPh,KDatB)=sp
          endif
          pn=lPrefPwd
          go to 1500
        else if(k.ge.IBroadHKLPwd.and.k.le.IBroadHKLPwd+9) then
          k=k-IBroadHKLPwd+1
          if(iz.gt.0) then
            p=BroadHKLPwd(k,KPh,KDatB)
            sp=BroadHKLPwds(k,KPh,KDatB)
          else if(iz.lt.0) then
            BroadHKLPwd(k,KPh,KDatB)=p
            BroadHKLPwds(k,KPh,KDatB)=sp
          endif
          pn=lBroadHKL
          go to 1500
        endif
        go to 8000
1500    ipn=idel(pn)+1
        write(pn(ipn:),'(i2)') k
        go to 8000
      else if(k.le.ndoff) then
        if(NMaxEDZone.le.0) go to 9999
        k=k-ndoffED
        l=mod(k-1,MxEDRef*NMaxEDZone)+1
        KDatB=(k-1)/(MxEDRef*NMaxEDZone)+1
        if(KDatB.gt.NDatBlock) go to 9999
        k=mod(l-1,MxEDRef)+1
        NZone=(l-1)/MxEDRef+1
        pn=lEdVar(k)
        write(at,'(''Zone#'',i5)') NZone
        call Zhusti(at)
        if(NDatBlock.gt.1) then
          write(Cislo,'(''%'',i5)') KDatB
          call Zhusti(Cislo)
          at=at(:idel(at))//Cislo(:idel(Cislo))
        endif
        if(k.eq.1) then
          if(iz.gt.0) then
            p=ScEDZone(NZone,KDatB)
            sp=ScEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            ScEDZone(NZone,KDatB)=p
            ScEDZoneS(NZone,KDatB)=sp
          endif
        else if(k.eq.2) then
          if(iz.gt.0) then
            p=ThickEDZone(NZone,KDatB)
            sp=ThickEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            ThickEDZone(NZone,KDatB)=p
            ThickEDZoneS(NZone,KDatB)=sp
          endif
        else if(k.eq.3) then
          if(iz.gt.0) then
            p=XNormEDZone(NZone,KDatB)
            sp=XNormEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            XNormEDZone(NZone,KDatB)=p
            XNormEDZoneS(NZone,KDatB)=sp
          endif
        else if(k.eq.4) then
          if(iz.gt.0) then
            p=YNormEDZone(NZone,KDatB)
            sp=YNormEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            YNormEDZone(NZone,KDatB)=p
            YNormEDZoneS(NZone,KDatB)=sp
          endif
        else if(k.eq.5) then
          if(iz.gt.0) then
            p=PhiEDZone(NZone,KDatB)
            sp=PhiEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            PhiEDZone(NZone,KDatB)=p
            PhiEDZoneS(NZone,KDatB)=sp
          endif
        else if(k.eq.5) then
          if(iz.gt.0) then
            p=ThetaEDZone(NZone,KDatB)
            sp=ThetaEDZoneS(NZone,KDatB)
          else if(iz.lt.0) then
            ThetaEDZone(NZone,KDatB)=p
            ThetaEDZoneS(NZone,KDatB)=sp
          endif
        endif
        go to 9999
      else if(k.lt.PrvniKiMol) then
        do ia=1,NAtAll
          if(k.lt.PrvniKiAtomu(ia)+DelkaKiAtomu(ia)) go to 2200
        enddo
c        do ia=NAtMolFr(1,1),NAtAll
c          if(k.lt.PrvniKiAtomu(ia)+DelkaKiAtomu(ia)) go to 2200
c        enddo
        call FeWinMessage('Co tady chces?','KdoCo-nenasel')
2200    k=k-PrvniKiAtomu(ia)+1
        call ChangeAtCompress(k,k,ia,1,ich)
        at=atom(ia)
        if(lite(kswa(ia)).eq.0) then
          lBasicPar(8)(1:1)='U'
        else
          lBasicPar(8)(1:1)='B'
        endif
        do i=1,11
          if(k.le.CumulAt(i)) go to 2320
        enddo
        i=9
2320    if(i.ne.1) k=k-CumulAt(i-1)
        if(i.eq.1) then
          if(k.lt.1) then
            go to 9999
          else if(k.eq.1) then
            pn=lBasicPar(1)
            if(iz.gt.0) then
              p=ai(ia)
              sp=sai(ia)
            else if(iz.lt.0) then
              ai(ia)=p
              sai(ia)=sp
            endif
          else if(k.le.4) then
            pn=lBasicPar(k)
            if(iz.gt.0) then
              p=x(k-1,ia)
              sp=sx(k-1,ia)
            else if(iz.lt.0) then
              x(k-1,ia)=p
              sx(k-1,ia)=sp
            endif
          else if(k.le.10) then
            if(itf(ia).eq.1) then
              if(k.eq.5) then
                pn=lBasicPar(8)
                if(iz.gt.0) then
                  p=beta(1,ia)
                  sp=sbeta(1,ia)
                else if(iz.lt.0) then
                  beta(1,ia)=p
                  sbeta(1,ia)=sp
                endif
              endif
            else
              call indext(k-4,i,j)
              if(iz.gt.0) then
                p=beta(k-4,ia)
                sp=sbeta(k-4,ia)
              else if(iz.lt.0) then
                beta(k-4,ia)=p
                sbeta(k-4,ia)=sp
              endif
              write(pn,114) lBasicPar(8)(1:1),i,j
            endif
          else if(k.le.20) then
            call indexc(k-10,3,ifa)
            write(pn,104) 'C',(ifa(j),j=1,3)
            if(iz.gt.0) then
              p=c3(k-10,ia)
              sp=sc3(k-10,ia)
            endif
            if(iz.lt.0) then
              c3(k-10,ia)=p
              sc3(k-10,ia)=sp
            endif
          else if(k.le.35) then
            call indexc(k-20,4,ifa)
            write(pn,104) 'D',(ifa(j),j=1,4)
            if(iz.gt.0) then
              p=c4(k-20,ia)
              sp=sc4(k-20,ia)
            endif
            if(iz.lt.0) then
              c4(k-20,ia)=p
              sc4(k-20,ia)=sp
            endif
          else if(k.le.56) then
            call indexc(k-35,5,ifa)
            write(pn,104) 'E',(ifa(j),j=1,5)
            if(iz.gt.0) then
              p=c5(k-35,ia)
              sp=sc5(k-35,ia)
            endif
            if(iz.lt.0) then
              c5(k-35,ia)=p
              sc5(k-35,ia)=sp
            endif
          else if(k.le.84) then
            call indexc(k-56,6,ifa)
            write(pn,104) 'F',(ifa(j),j=1,6)
            if(iz.gt.0) then
              p=c6(k-56,ia)
              sp=sc6(k-56,ia)
            endif
            if(iz.lt.0) then
              c6(k-56,ia)=p
              sc6(k-56,ia)=sp
            endif
          else
            pn='rfree'
            if(iz.gt.0) then
              p=xfr(ia)
              sp=sxfr(ia)
            endif
            if(iz.lt.0) then
              xfr(ia)=p
              sxfr(ia)=sp
            endif
          endif
        else if(i.eq.2) then
          if(k.eq.1) then
            pn='Pc'
            if(iz.gt.0) then
              p=popc(ia)
              sp=spopc(ia)
            else if(iz.lt.0) then
              popc(ia)=p
              spopc(ia)=sp
            endif
          else if(k.eq.2) then
            pn='Pv'
            if(iz.gt.0) then
              p=popv(ia)
              sp=spopv(ia)
            else if(iz.lt.0) then
              popv(ia)=p
              spopv(ia)=sp
            endif
          else if(k.eq.3) then
            pn=lk1
            if(iz.gt.0) then
              p=kapa1(ia)
              sp=skapa1(ia)
            else if(iz.lt.0) then
              kapa1(ia)=p
              skapa1(ia)=sp
            endif
          else if(k.eq.4) then
            pn=lk2
            if(iz.gt.0) then
              p=kapa2(ia)
              sp=skapa2(ia)
            else if(iz.lt.0) then
              kapa2(ia)=p
              skapa2(ia)=sp
            endif
          else
            k=k-4
            do i=0,7
              ip=(i+1)**2
              if(k.le.ip) then
                ip=k-i**2
                if(ip.eq.1) then
                  ip=-1
                  j=0
                else
                  j=ip/2
                  ip=mod(ip,2)
                endif
                go to 2210
              endif
            enddo
2210        write(pn,'(''P'',2i1)') i,j
            if(ip.eq.0) then
              pn(4:4)='+'
            else if(ip.eq.1) then
              pn(4:4)='-'
            endif
            if(iz.gt.0) then
              p=popas(k,ia)
              sp=spopas(k,ia)
            else if(iz.lt.0) then
              popas(k,ia)=p
              spopas(k,ia)=sp
            endif
          endif
        else if(i.le.9) then
          n=i-3
          if(n.eq.0) then
            if(k.eq.1) then
              if(KFA(1,ia).eq.0) then
                pn=lBasicPar(12)
              else
                pn='delta'
              endif
              if(iz.gt.0) then
                p=a0(ia)
                sp=sa0(ia)
              else if(iz.lt.0) then
                a0(ia)=p
                sa0(ia)=sp
              endif
              go to 9999
            else
              k=k-1
            endif
            pn='o'
          else if(n.eq.1) then
            pn='x'
          else if(n.eq.2) then
            pn=lBasicPar(8)(1:1)
          else
            pn=char(ichar('C')+n-3)
          endif
          idp=idel(pn)
          nrank=TRank(n)
          iw=(k-1)/(2*nrank)+1
          iwo=(k-1)/nrank+1
          ip=mod(k-1,nrank)+1
          im=mod(iwo,2)
          if(n.eq.1) then
            pn=smbx(ip)
          else if(n.ne.0) then
            if(n.eq.2) then
              call indext(ip,ifa(1),ifa(2))
            else
              call indexc(ip,n,ifa)
            endif
            write(pn(idp+1:),'(6i1)')(ifa(j),j=1,n)
          endif
          idp=idel(pn)
          if(TypeModFun(ia).ne.0.and.n.ne.0) then
            pn=pn(:idp)//'ort'
          else
            if(im.eq.1) then
              pn=pn(:idp)//'sin'
            else
              pn=pn(:idp)//'cos'
            endif
            iwo=iw
          endif
          write(pn(11:12),'(i2)') iwo
          if(im.eq.1) then
            if(n.eq.0.and.KFA(1,ia).ne.0.and.iw.eq.KModA(1,ia)) then
              pn='x40'
            else if(n.eq.1.and.KFA(2,ia).ne.0.and.iw.eq.KModA(2,ia))
     1        then
              pn=pn(1:1)//'-slope'
            endif
            if(iz.gt.0) then
              if(n.eq.0) then
                p=ax(iw,ia)
                sp=sax(iw,ia)
              else if(n.eq.1) then
                p=ux(ip,iw,ia)
                sp=sux(ip,iw,ia)
              else if(n.eq.2) then
                p=bx(ip,iw,ia)
                sp=sbx(ip,iw,ia)
              else if(n.eq.3) then
                p=c3x(ip,iw,ia)
                sp=sc3x(ip,iw,ia)
              else if(n.eq.4) then
                p=c4x(ip,iw,ia)
                sp=sc4x(ip,iw,ia)
              else if(n.eq.5) then
                p=c5x(ip,iw,ia)
                sp=sc5x(ip,iw,ia)
              else
                p=c6x(ip,iw,ia)
                sp=sc6x(ip,iw,ia)
              endif
            else if(iz.lt.0) then
              if(n.eq.0) then
                ax(iw,ia)=p
                sax(iw,ia)=sp
              else if(n.eq.1) then
                ux(ip,iw,ia)=p
                sux(ip,iw,ia)=sp
              else if(n.eq.2) then
                bx(ip,iw,ia)=p
                sbx(ip,iw,ia)=sp
              else if(n.eq.3) then
                c3x(ip,iw,ia)=p
                sc3x(ip,iw,ia)=sp
              else if(n.eq.4) then
                c4x(ip,iw,ia)=p
                sc4x(ip,iw,ia)=sp
              else if(n.eq.5) then
                c5x(ip,iw,ia)=p
                sc5x(ip,iw,ia)=sp
              else
                c6x(ip,iw,ia)=p
                sc6x(ip,iw,ia)=sp
              endif
            endif
          else
            if(n.eq.1.and.KFA(2,ia).ne.0.and.iw.eq.KModA(2,ia)) then
              if(ip.eq.1) then
                pn='x40'
              else if(ip.eq.2) then
                pn='delta'
              endif
            endif
            if(iz.gt.0) then
              if(n.eq.0) then
                p=ay(iw,ia)
                sp=say(iw,ia)
              else if(n.eq.1) then
                p=uy(ip,iw,ia)
                sp=suy(ip,iw,ia)
              else if(n.eq.2) then
                p=by(ip,iw,ia)
                sp=sby(ip,iw,ia)
              else if(n.eq.3) then
                p=c3y(ip,iw,ia)
                sp=sc3y(ip,iw,ia)
              else if(n.eq.4) then
                p=c4y(ip,iw,ia)
                sp=sc4y(ip,iw,ia)
              else if(n.eq.5) then
                p=c5y(ip,iw,ia)
                sp=sc5y(ip,iw,ia)
              else
                p=c6y(ip,iw,ia)
                sp=sc6y(ip,iw,ia)
              endif
            else if(iz.lt.0) then
              if(n.eq.0) then
                ay(iw,ia)=p
                say(iw,ia)=sp
              else if(n.eq.1) then
                uy(ip,iw,ia)=p
                suy(ip,iw,ia)=sp
              else if(n.eq.2) then
                by(ip,iw,ia)=p
                sby(ip,iw,ia)=sp
              else if(n.eq.3) then
                c3y(ip,iw,ia)=p
                sc3y(ip,iw,ia)=sp
              else if(n.eq.4) then
                c4y(ip,iw,ia)=p
                sc4y(ip,iw,ia)=sp
              else if(n.eq.5) then
                c5y(ip,iw,ia)=p
                sc5y(ip,iw,ia)=sp
              else
                c6y(ip,iw,ia)=p
                sc6y(ip,iw,ia)=sp
              endif
            endif
          endif
        else if(i.eq.10) then
          pn=lBasicPar(11)
          if(iz.gt.0) then
            p=phf(ia)
            sp=sphf(ia)
          else if(iz.lt.0) then
            phf(ia)=p
            sphf(ia)=sp
          endif
        else
          nrank=3
          if(k.gt.nrank) then
            k=k-nrank
            iw=(k-1)/(2*nrank)+1
            iwo=(k-1)/nrank+1
            ip=mod(k-1,nrank)+1
            im=mod(iwo,2)
          else
            iw=0
            ip=mod(k-1,nrank)+1
          endif
          if(KUsePolar(ia).eq.0) then
            pn='M'//smbx(ip)
          else
            pn='M'//smbp(ip)
          endif
          if(iw.gt.0) then
            if(im.eq.1) then
              pn=pn(:2)//'sin'
            else
              pn=pn(:2)//'cos'
            endif
          endif
          idlk=idel(pn)
          write(pn(idlk+1:),'(i2)') iw
          if(iw.eq.0) then
            if(iz.gt.0) then
              p=sm0(ip,ia)
              sp=ssm0(ip,ia)
            else if(iz.lt.0) then
              sm0(ip,ia)=p
              ssm0(ip,ia)=sp
            endif
          else
            if(im.eq.1) then
              if(iz.gt.0) then
                p=smx(ip,iw,ia)
                sp=ssmx(ip,iw,ia)
              else if(iz.lt.0) then
                smx(ip,iw,ia)=p
                ssmx(ip,iw,ia)=sp
              endif
            else
              if(iz.gt.0) then
                p=smy(ip,iw,ia)
                sp=ssmy(ip,iw,ia)
              else if(iz.lt.0) then
                smy(ip,iw,ia)=p
                ssmy(ip,iw,ia)=sp
              endif
            endif
          endif
        endif
      else if(k.lt.PrvniKiAtXYZMode) then
        do i=1,NMolec
          do j=1,mam(i)
            ia=j+(i-1)*mxp
            if(k.lt.PrvniKiMolekuly(ia)+DelkaKiMolekuly(ia)) go to 3200
          enddo
        enddo
3200    k=k-PrvniKiMolekuly(ia)+1
        call ChangeMolCompress(k,k,ia,1,ich)
        write(at,'(a8,''#'',i2)') molname(i),j
        call zhusti(at)
        if(k.eq.1) then
          pn=lBasicPar(1)(1:2)//'mol'
          if(iz.gt.0) then
            p=aimol(ia)
            sp=saimol(ia)
          else if(iz.lt.0) then
            aimol(ia)=p
            saimol(ia)=sp
          endif
        else if(k.le.4) then
          k=k-1
          pn=lBasicPar(k+4)
          if(iz.gt.0) then
            p=euler(k,ia)
            sp=seuler(k,ia)
          else if(iz.lt.0) then
            euler(k,ia)=p
            seuler(k,ia)=sp
          endif
        else if(k.le.7) then
          k=k-4
          pn=lBasicPar(k+1)(1:1)//'trans'
          if(iz.gt.0) then
            p=trans(k,ia)
            sp=strans(k,ia)
          else if(iz.lt.0) then
            trans(k,ia)=p
            strans(k,ia)=sp
          endif
        else if(k.le.13) then
          k=k-7
          call indext(k,l,m)
          write(pn,114) 'T',l,m
          if(iz.gt.0) then
            p=tt(k,ia)
            sp=stt(k,ia)
          else if(iz.lt.0) then
            tt(k,ia)=p
            stt(k,ia)=sp
          endif
        else if(k.le.19) then
          k=k-13
          call indext(k,l,m)
          write(pn,114) 'L',l,m
          if(iz.gt.0) then
            p=tl(k,ia)
            sp=stl(k,ia)
          else if(iz.lt.0) then
            tl(k,ia)=p
            stl(k,ia)=sp
          endif
        else if(k.le.28) then
          k=k-19
          call indexs(k,l,m)
          write(pn,114) 'S',l,m
          if(iz.gt.0) then
            p=ts(k,ia)
            sp=sts(k,ia)
          else if(iz.lt.0) then
            ts(k,ia)=p
            sts(k,ia)=sp
          endif
        else if(k.eq.29) then
          if(KFM(1,ia).eq.0) then
            pn=lBasicPar(12)(1:1)//'m'
          else
            pn='delta'
          endif
          if(iz.gt.0) then
            p=a0m(ia)
            sp=sa0m(ia)
          else if(iz.lt.0) then
            a0m(ia)=p
            sa0m(ia)=sp
          endif
        else if(k.le.29+2*mxw) then
          k=k-29
          i=(k-1)/2+1
          j=mod(k-1,2)+1
          if(j.eq.1) then
            if(KFM(1,ia).ne.0.and.i.eq.KModM(1,ia)) then
              pn='x40'
            else
              write(pn,109) 'om','sin',i
            endif
            if(iz.gt.0) then
              p=axm(i,ia)
              sp=saxm(i,ia)
            else if(iz.lt.0) then
              axm(i,ia)=p
              saxm(i,ia)=sp
            endif
          else
            write(pn,109) 'om','cos',i
            if(iz.gt.0) then
              p=aym(i,ia)
              sp=saym(i,ia)
            else if(iz.lt.0) then
              aym(i,ia)=p
              saym(i,ia)=sp
            endif
          endif
        else if(k.le.29+14*mxw) then
          k=k-29-2*mxw
          if(k.le.6*mxw) then
            cp='t'
          else
            k=k-6*mxw
            cp='r'
          endif
          i=(k-1)/6+1
          j=mod(k-1,6)+1
          if(j.le.3) then
            if(TypeModFunMol(ia).eq.0) then
              write(pn,113) lBasicPar(j+1)(1:1),cp,'sin',i
            else
              write(pn,113) lBasicPar(j+1)(1:1),cp,'ort',2*i-1
            endif
            if(cp.eq.'t') then
              if(iz.gt.0) then
                p=utx(j,i,ia)
                sp=sutx(j,i,ia)
              else if(iz.lt.0) then
                utx(j,i,ia)=p
                sutx(j,i,ia)=sp
              endif
            else
              if(iz.gt.0) then
                p=urx(j,i,ia)
                sp=surx(j,i,ia)
              else if(iz.lt.0) then
                urx(j,i,ia)=p
                surx(j,i,ia)=sp
              endif
            endif
          else
            j=j-3
            if(TypeModFunMol(ia).eq.0) then
              write(pn,113) lBasicPar(j+1)(1:1),cp,'cos',i
            else
              write(pn,113) lBasicPar(j+1)(1:1),cp,'ort',2*i
            endif
            if(cp.eq.'t') then
              if(iz.gt.0) then
                p=uty(j,i,ia)
                sp=suty(j,i,ia)
              else if(iz.lt.0) then
                uty(j,i,ia)=p
                suty(j,i,ia)=sp
              endif
            else
              if(iz.gt.0) then
                p=ury(j,i,ia)
                sp=sury(j,i,ia)
              else if(iz.lt.0) then
                ury(j,i,ia)=p
                sury(j,i,ia)=sp
              endif
            endif
          endif
        else if(k.le.29+26*mxw) then
          k=k-29-14*mxw
          i=(k-1)/12+1
          j=mod(k-1,12)+1
          if(j.le.6) then
            cs='sin'
          else
            j=j-6
            cs='cos'
          endif
          call indext(j,l,m)
          if(TypeModFunMol(ia).eq.0) then
            write(pn,114) 'T',l,m,cs,i
          else
            if(cs.eq.'sin') then
              write(pn,114) 'T',l,m,'ort',2*i-1
            else
              write(pn,114) 'T',l,m,'ort',2*i
            endif
          endif
          if(iz.gt.0) then
            if(cs.eq.'sin') then
              p=ttx(j,i,ia)
              sp=sttx(j,i,ia)
            else
              p=tty(j,i,ia)
              sp=stty(j,i,ia)
            endif
          else if(iz.lt.0) then
            if(cs.eq.'sin') then
              ttx(j,i,ia)=p
              sttx(j,i,ia)=sp
            else
              tty(j,i,ia)=p
              stty(j,i,ia)=sp
            endif
          endif
        else if(k.le.29+38*mxw) then
          k=k-29-26*mxw
          i=(k-1)/12+1
          j=mod(k-1,12)+1
          if(j.le.6) then
            cs='sin'
          else
            j=j-6
            cs='cos'
          endif
          call indext(j,l,m)
          if(TypeModFunMol(ia).eq.0) then
            write(pn,114) 'L',l,m,cs,i
          else
            if(cs.eq.'sin') then
              write(pn,114) 'L',l,m,'ort',2*i-1
            else
              write(pn,114) 'L',l,m,'ort',2*i
            endif
          endif
          if(iz.gt.0) then
            if(cs.eq.'sin') then
              p=tlx(j,i,ia)
              sp=stlx(j,i,ia)
            else
              p=tly(j,i,ia)
              sp=stly(j,i,ia)
            endif
          else if(iz.lt.0) then
            if(cs.eq.'sin') then
              tlx(j,i,ia)=p
              stlx(j,i,ia)=sp
            else
              tly(j,i,ia)=p
              stly(j,i,ia)=sp
            endif
          endif
        else if(k.le.29+56*mxw) then
          k=k-29-38*mxw
          i=(k-1)/18+1
          j=mod(k-1,18)+1
          if(j.le.9) then
            cs='sin'
          else
            j=j-9
            cs='cos'
          endif
          call indexs(j,l,m)
          if(TypeModFunMol(ia).eq.0) then
            write(pn,114) 'S',l,m,cs,i
          else
            if(cs.eq.'sin') then
              write(pn,114) 'S',l,m,'ort',2*i-1
            else
              write(pn,114) 'S',l,m,'ort',2*i
            endif
          endif
          if(iz.gt.0) then
            if(cs.eq.'sin') then
              p=tsx(j,i,ia)
              sp=stsx(j,i,ia)
            else
              p=tsy(j,i,ia)
              sp=stsy(j,i,ia)
            endif
          else if(iz.lt.0) then
            if(cs.eq.'sin') then
              tsx(j,i,ia)=p
              stsx(j,i,ia)=sp
            else
              tsy(j,i,ia)=p
              stsy(j,i,ia)=sp
            endif
          endif
        else
          pn=lBasicPar(11)(:idel(lBasicPar(11)))//'m'
          if(iz.gt.0) then
            p=phfm(ia)
            sp=sphfm(ia)
          else if(iz.lt.0) then
            phfm(ia)=p
            sphfm(ia)=sp
          endif
        endif
      else if(k.lt.PrvniKiAtMagMode) then
        k=k-PrvniKiAtXYZMode+1
        l=0
        do i=1,NAtXYZMode
          do j=1,NMAtXYZMode(i)
            l=l+1
            if(k.eq.l) then
              if(iz.gt.0) then
                 p= AtXYZMode(j,i)
                sp=sAtXYZMode(j,i)
              else if(iz.lt.0) then
                 AtXYZMode(j,i)= p
                sAtXYZMode(j,i)=sp
              endif
              pn=LAtXYZMode(j,i)
              at=Atom(IAtXYZMode(1,i))
              go to 9999
            endif
          enddo
        enddo
      else
        k=k-PrvniKiAtMagMode+1
        l=0
        do i=1,NAtMagMode
          do j=1,NMAtMagMode(i)
            l=l+1
            if(k.eq.l) then
              if(iz.gt.0) then
                 p= AtMagMode(j,i)
                sp=sAtMagMode(j,i)
              else if(iz.lt.0) then
                 AtMagMode(j,i)= p
                sAtMagMode(j,i)=sp
              endif
              pn=LAtMagMode(j,i)
              at=Atom(IAtMagMode(1,i))
              go to 9999
            endif
          enddo
        enddo
      endif
      go to 9999
8000  if(KPh.gt.0.and.NPhase.gt.1) then
        Cislo=PhaseName(KPh)
      else
        Cislo=' '
      endif
      if(KDatB.gt.0.and.NDatBlock.gt.1) then
        write(At,'(''Block'',i2)') KDatB
      else
        At=' '
      endif
      if(At.eq.' ') then
        if(Cislo.eq.' ') then
          go to 9999
        else
          At=Cislo
        endif
      else
        if(Cislo.ne.' ') then
          write(At,'(''Block'',i2)') KDatB
          At=Cislo(:idel(Cislo))//'%'//At(:idel(At))
        endif
      endif
9999  call Zhusti(at)
      call Zhusti(pn)
      return
104   format(a1,6i1)
105   format(6i1)
108   format(a1,a3,i2)
109   format(a2,a3,i2)
110   format('rho',2i1)
113   format(2a1,a3,i2)
114   format(a1,2i1,a3,i2)
115   format(a4,2i1,a3,i2)
      end
