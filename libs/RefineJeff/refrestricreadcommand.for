      subroutine RefRestricReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/RestricQuest/ nEdwAtoms,nEdwPhases,nCrwRestricAtMol,
     1                     nCrwRestricProfile,RestricType(6),OccType,
     2                     AtomString,LocalSymm,nRolMenuLocalSymm,
     3                     PhaseString,RestricProfile
      dimension ip(1)
      character*256 AtomString,PhaseString
      character*80  t80
      character*(*) Command
      integer RestricType,OccType
      logical RestricProfile,EqIgCase
      save /RestricQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'restric'.and.t80.ne.'!restric') go to 8010
      call kus(Command,k,AtomString)
      call kus(Command,k,t80)
      if(EqIgCase(t80,'profile')) then
        RestricProfile=.true.
        PhaseString=AtomString
        AtomString=' '
        if(k.lt.256)
     1    PhaseString=PhaseString(:idel(PhaseString)+1)//
     2                Command(k+1:idel(Command))
      else
        RestricProfile=.false.
        kk=0
        call StToInt(t80,kk,ip,1,.false.,ich)
        if(ich.ne.0) go to 8030
        if(ip(1).gt.100) then
          i=mod(ip(1),100)
        else
          i=ip(1)
        endif
        if(i.lt.-10) then
          OccType=1
        else if(i.lt.0) then
          OccType=2
        else if(i.ge.10) then
          OccType=4
        else
          OccType=3
        endif
        RestricType=0
        i=mod(iabs(ip(1)),10)
        if(i.eq.1) then
          RestricType(1:3)=1
        else if(i.eq.2) then
          RestricType(2:3)=1
        else if(i.eq.3) then
          RestricType(3:3)=1
        endif
        if(NDimI(KPhase).le.0) RestricType(2)=0
        i=ip(1)/100
        do icd=4,6
          if(mod(i,2).eq.1) RestricType(icd)=1
          i=i/2
        enddo
        kp=k
        call kus(Command,k,t80)
        i=index(t80,'ls#')
        if(i.gt.0) then
          kk=3
          call StToInt(t80,kk,ip,1,.false.,ich)
          if(ich.ne.0) go to 8030
          LocalSymm=ip(1)+1
        else
          k=kp
        endif
        if(k.lt.256)
     1    AtomString=AtomString(:idel(AtomString)+1)//
     2               Command(k+1:idel(Command))
        call TestAtomString(AtomString,IdWildYes,IdAtMolYes,IdMolYes,
     1                      IdSymmNo,IdAtMolMixYes,t80)
        if(EqIgCase(t80,'Mixed')) then
          RestricType=0
          t80=' '
        endif
        if(t80.ne.' ') then
          if(t80.ne.'Jiz oznameno')
     1      call FeChybne(-1.,-1.,t80,' ',SeriousError)
          ich=1
          go to 9999
        endif
      endif
      if(ExistPowder.and.NPhase.gt.1) then
        call FeQuestCrwOpen(nCrwRestricAtMol,.not.RestricProfile)
        call FeQuestCrwOpen(nCrwRestricProfile,RestricProfile)
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
