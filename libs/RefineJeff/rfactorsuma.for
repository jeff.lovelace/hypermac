      subroutine RFactorSuma
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical ReflFulfilsCond
      ady=abs(dyp)
      ayo=abs(Fobs)
       RNumAll(1)= RNumAll(1)+ady
       RDenAll(1)= RDenAll(1)+ayo
      wRNumAll(1)=wRNumAll(1)+wdyq
      wRDenAll(1)=wRDenAll(1)+wyoq
      nRAll(1)=nRAll(1)+1
      do i=1,NRFactors
        if(.not.ReflFulfilsCond(ihread,RFactMatrix(1,i),RFactVector(1,i)
     1                         ,CondRFactIncl(1,i),ModRFactIncl(i),
     2                                             AbsRFactIncl(i),
     3                          CondRFactExcl(1,i),ModRFactExcl(i),
     4                                             AbsRFactExcl(i)))
     5    cycle
        wRNumPartAll(i)=wRNumPartAll(i)+wdyq
        wRDenPartAll(i)=wRDenPartAll(i)+wyoq
         RNumPartAll(i)= RNumPartAll(i)+ady
         RDenPartAll(i)= RDenPartAll(i)+ayo
        nRPartAll(i)=nRPartAll(i)+1
        wRNumPartAllAll=wRNumPartAllAll+wdyq
        wRDenPartAllAll=wRDenPartAllAll+wyoq
         RNumPartAllAll= RNumPartAllAll+ady
         RDenPartAllAll= RDenPartAllAll+ayo
        nRPartAllAll=nRPartAllAll+1
      enddo
      if(ExistElectronData.and.ExistM42.and.CalcDyn) then
        if(NEDZone(KDatBlock).gt.1) then
          wRNumZoneAll(iq,KDatBlock)=wRNumZoneAll(iq,KDatBlock)+wdyq
          wRDenZoneAll(iq,KDatBlock)=wRDenZoneAll(iq,KDatBlock)+wyoq
           RNumZoneAll(iq,KDatBlock)=RNumZoneAll(iq,KDatBlock)+abs(dyp)
           RDenZoneAll(iq,KDatBlock)=RDenZoneAll(iq,KDatBlock)+abs(Fobs)
          nRZoneAll(iq,KDatBlock)=nRZoneAll(iq,KDatBlock)+1
        endif
      endif
      if(.not.nulova) then
         RNumObs(1)= RNumObs(1)+ady
         RDenObs(1)= RDenObs(1)+ayo
        wRNumObs(1)=wRNumObs(1)+wdyq
        wRDenObs(1)=wRDenObs(1)+wyoq
        nRObs(1)=nRObs(1)+1
        do i=1,NRFactors
          if(.not.ReflFulfilsCond(ihread,RFactMatrix(1,i),
     1                            RFactVector(1,i),
     2                            CondRFactIncl(1,i),ModRFactIncl(i),
     3                                               AbsRFactIncl(i),
     4                            CondRFactExcl(1,i),ModRFactExcl(i),
     5                                               AbsRFactexcl(i)))
     6      cycle
           RNumPartObs(i)= RNumPartObs(i)+ady
           RDenPartObs(i)= RDenPartObs(i)+ayo
          wRNumPartObs(i)=wRNumPartObs(i)+wdyq
          wRDenPartObs(i)=wRDenPartObs(i)+wyoq
          nRPartObs(i)=nRPartObs(i)+1
           RNumPartAllObs= RNumPartAllObs+ady
           RDenPartAllObs= RDenPartAllObs+ayo
          wRNumPartAllObs=wRNumPartAllObs+wdyq
          wRDenPartAllObs=wRDenPartAllObs+wyoq
          nRPartAllObs=nRPartAllObs+1
        enddo
        if(ExistElectronData.and.ExistM42.and.CalcDyn) then
          if(NEDZone(KDatBlock).gt.1) then
            wRNumZoneObs(iq,KDatBlock)=wRNumZoneObs(iq,KDatBlock)+wdyq
            wRDenZoneObs(iq,KDatBlock)=wRDenZoneObs(iq,KDatBlock)+wyoq
            RNumZoneObs(iq,KDatBlock)=RNumZoneObs(iq,KDatBlock)+abs(dyp)
            RDenZoneObs(iq,KDatBlock)=RDenZoneObs(iq,KDatBlock)+
     1                                abs(Fobs)
           nRZoneObs(iq,KDatBlock)=nRZoneObs(iq,KDatBlock)+1
          endif
        endif
      endif
      if(NDimI(KPhase).gt.0.or.MagneticRFac(KPhase).ne.0) then
        Maxmmabsm=max(Maxmmabsm,mmabsm)
        RNumAll(mmabsm)=RNumAll(mmabsm)+ady
        RDenAll(mmabsm)=RDenAll(mmabsm)+ayo
        wRNumAll(mmabsm)=wRNumAll(mmabsm)+wdyq
        wRDenAll(mmabsm)=wRDenAll(mmabsm)+wyoq
        nRAll(mmabsm)=nRAll(mmabsm)+1
        if(.not.nulova) then
          RNumObs(mmabsm)=RNumObs(mmabsm)+ady
          RDenObs(mmabsm)=RDenObs(mmabsm)+ayo
          wRNumObs(mmabsm)=wRNumObs(mmabsm)+wdyq
          wRDenObs(mmabsm)=wRDenObs(mmabsm)+wyoq
          nRObs(mmabsm)=nRObs(mmabsm)+1
        endif
        if(mic.gt.0) then
          m=mic+21
          RNumAll(m)=RNumAll(m)+ady
          RDenAll(m)=RDenAll(m)+ayo
          wRNumAll(m)=wRNumAll(m)+wdyq
          wRDenAll(m)=wRDenAll(m)+wyoq
          nRAll(m)=nRAll(m)+1
          if(.not.nulova) then
            RNumObs(m)=RNumObs(m)+ady
            RDenObs(m)=RDenObs(m)+ayo
            wRNumObs(m)=wRNumObs(m)+wdyq
            wRDenObs(m)=wRDenObs(m)+wyoq
            nRObs(m)=nRObs(m)+1
          endif
        endif
      endif
      return
      end
