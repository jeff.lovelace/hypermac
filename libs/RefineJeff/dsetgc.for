      subroutine DSetGC
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension px(28),py(28)
      jap=nagcOff(KPhase)
      do i=1,NAtCalcBasic
        if(kswa(i).ne.KPhase) cycle
        isw=iswa(i)
        itfi=itf(i)
        mip=PrvniKiAtomu(i)
        if(MagPar(i).gt.0) then
          mmag=DelkaKiAtomu(i)-3-6*(MagPar(i)-1)
        else
          mmag=0
        endif
        jap=jap+1
        ja=jap
        if(MagPar(i).le.0) then
          itfim=itfi+1
        else
          itfim=itfi+2
        endif
        do j=1,ngc(KPhase)
          if(ai(ja).le..000001) go to 3500
          mi=mip
          mj=PrvniKiAtomu(ja)
          do n=1,itfim
            if(n.gt.itfi+1) then
              nrank=3
            else
              nrank=TRank(n-1)
            endif
            if(n.gt.itfi+1) then
              mi=mip+mmag
              mjp=mj
              mj=PrvniKiAtomu(ja)+mmag
              call cultm(der(mj),RMagGC(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else if(n.eq.1) then
              der(mi)=der(mi)+der(mj)
            else if(n.eq.2) then
              call cultm(der(mj),rmgc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else if(n.eq.3) then
              call cultm(der(mj),rtgc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else if(n.eq.4) then
              call cultm(der(mj),rc3gc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else if(n.eq.5) then
              call cultm(der(mj),rc4gc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else if(n.eq.6) then
              call cultm(der(mj),rc5gc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            else
              call cultm(der(mj),rc6gc(1,j,isw,KPhase),der(mi),1,nrank,
     1                   nrank)
            endif
            mi=mi+nrank
            mj=mj+nrank
            if(itfi.le.1.and.n.eq.2) then
              der(mi)=der(mi)+der(mj)
              mi=mi+TRank(2)
              mj=mj+TRank(2)
            endif
          enddo
          if(MagPar(i).gt.0) then
            mj=mjp
            mmag=DelkaKiAtomu(i)-6*(MagPar(i)-1)
          else
            mmag=0
          endif
          do n=1,itfim
            if(n.gt.itfi+1) then
              kmod=MagPar(i)-1
              kfai=0
              nrank=3
              mjp=PrvniKiAtomu(ja)+mmag
              mi=PrvniKiAtomu(i)+mmag
            else
              kmod=KModA(n,i)
              kfai=KFA(n,i)
              nrank=TRank(n-1)
              mjp=mj
            endif
            if(kmod.le.0) cycle
            if(n.eq.1) then
              der(mi)=der(mi)+der(mj)
              mi=mi+1
              mj=mj+1
              mjp=mjp+1
            endif
            do k=1,kmod
              snp=sngc(k,j,i)
              csp=csgc(k,j,i)
              if(NDim(KPhase).eq.4) eps=rm6gc(16,j,isw,KPhase)
              epsp=isign(1,KwSymGC(k,j,isw,KPhase))
              if(kfai.eq.0.or.n.ne.2.or.k.ne.kmod) then
                iw=iabs(KwSymGC(k,j,isw,KPhase))
              else
                iw=k
              endif
              mj=mjp+2*(iw-1)*nrank
              if(n.gt.itfi+1) then
                call multm(der(mj      ),RMagGC(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),RMagGC(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              else if(n.eq.1) then
                px(1)=der(mj  )
                py(1)=der(mj+1)
              else if(n.eq.2) then
                call multm(der(mj       ),rmgc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                if(kfai.eq.0.or.k.ne.kmod)
     1            call multm(der(mj+nrank),rmgc(1,j,isw,KPhase),py,1,
     2                       nrank,nrank)
              else if(n.eq.3) then
                call multm(der(mj      ),rtgc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),rtgc(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              else if(n.eq.4) then
                call multm(der(mj      ),rc3gc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),rc3gc(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              else if(n.eq.5) then
                call multm(der(mj      ),rc4gc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),rc4gc(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              else if(n.eq.6) then
                call multm(der(mj      ),rc5gc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),rc5gc(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              else
                call multm(der(mj      ),rc6gc(1,j,isw,KPhase),px,1,
     1                     nrank,nrank)
                call multm(der(mj+nrank),rc6gc(1,j,isw,KPhase),py,1,
     1                     nrank,nrank)
              endif
              do l=1,nrank
                if(kfai.eq.0.or.n.ne.2.or.k.ne.kmod) then
                  der(mi)=der(mi)+epsp*(csp*px(l)+snp*py(l))
                  der(mi+nrank)=der(mi+nrank)-snp*px(l)+csp*py(l)
                else
                  der(mi)=der(mi)+eps*px(l)
                  if(l.eq.1) then
                    der(mi+nrank)=der(mi+nrank)+eps*der(mj+nrank)
                  else if(l.eq.2) then
                    der(mi+nrank)=der(mi+nrank)+der(mj+nrank)
                  endif
                endif
                mi=mi+1
                mj=mj+1
              enddo
              mi=mi+nrank
            enddo
            mj=mjp+2*nrank*kmod
          enddo
3500      ja=ja+nagcLen(KPhase)
        enddo
      enddo
9999  return
      end
