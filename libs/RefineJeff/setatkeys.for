      subroutine SetAtKeys(i1,i2,iap,iak,kkk)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical RefLeadingAtomInRest
      integer AtomUseXYZModes,AtomUseMagModes
      do i=i1,i2
        itfi=itf(i)
        if(i.lt.iap.or.i.gt.iak.or.
     1     (ai(i).eq.0..and..not.RefLeadingAtomInRest(i))) then
          call SetIntArrayTo(KiA(1,i),DelkaKiAtomu(i),0)
        else
          jp=TRankCumul(itfi)
          if(itfi.gt.1) then
            jp=jp-1
            jpp=jp
          else
            jpp=9
          endif
          call SetIntArrayTo(KiA(2,i),jpp,0)
          if(kim.eq.0.or.kim.eq.-2) then
            if(itfi.eq.0.or.(ExistMagPol.and.NDatBlock.eq.1)) then
              j=3
            else
              j=jp
            endif
            call SetIntArrayTo(KiA(2,i),j,1)
            if(AtomUseXYZModes(Atom(i)).gt.0)
     1        call SetIntArrayTo(KiA(2,i),3,0)
          else
            KiA(1,i)=0
          endif
          kip=max(TRankCumul(itfi),10)+1
          if(NDimI(KPhase).le.0) then
            if(ChargeDensities.and.lasmax(i).gt.0) then
              lasmaxi=lasmax(i)
              kip=DelkaKiAtomu(i)-1
              if(lasmaxi.gt.1) kip=kip-(lasmaxi-1)**2-1
              call SetIntArrayTo(KiA(kip,i),2,1)
              if(lasmaxi.gt.1) then
                kip=kip+2
                if(lasmaxi.gt.2) KiA(kip,i)=1
                kip=kip+2
                call SetIntArrayTo(KiA(kip,i),(lasmaxi-1)**2-1,1)
              endif
            endif
            go to 2000
          endif
          if(itfi.eq.0) itfi=2
          do n=1,itfi+1
            kmodp=KModA(n,i)
            if(kmodp.le.0) cycle
            if(n.eq.1) kip=kip+1
            if(KFA(n,i).ne.0) then
              if(kkk.ne.0) kmodp=kmodp-NDimI(KPhase)
              if(n.eq.1) KiA(kip+1,i)=0
            endif
            m=2*TRank(n-1)
            do k=1,m*kmodp
              KiA(kip,i)=kkk
              kip=kip+1
            enddo
            if(kmodp.ne.KModA(n,i)) kip=kip+m*(KModA(n,i)-kmodp)
          enddo
          do n=1,itfi+1
            if(KModA(n,i).gt.0) then
              if(phf(i).ne.0.) then
                KiA(kip,i)=kkk
              else
                KiA(kip,i)=0
              endif
              kip=kip+1
              go to 2000
            endif
          enddo
2000      if(MagPar(i).gt.0) then
            if(AtomUseMagModes(Atom(i)).le.0) then
              call SetIntArrayTo(KiA(kip,i),3,1)
              call SetIntArrayTo(KiA(kip+3,i),(MagPar(i)-1)*6,kkk)
            else
              call SetIntArrayTo(KiA(kip,i),3,0)
            endif
          endif
        endif
      enddo
      return
      end
