      subroutine RefFixedCheck(AtomString,FixedType,ErrString)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*(*) AtomString,ErrString
      integer FixedType
      ErrString=' '
      if(FixedType.lt.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildYes,IdAtMolNo,IdMolNo,
     1                      IdSymmNo,IdAtMolMixNo,ErrString)
      else if(FixedType.eq.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildNo,IdAtMolNo,IdMolYes,
     1                      IdSymmNo,IdAtMolMixNo,ErrString)
      else
        if(FixedType.eq.IdFixOriginX4) then
          j=0
        else
          j=1
        endif
        call TestParamString(AtomString,j,ErrString)
      endif
      return
      end
