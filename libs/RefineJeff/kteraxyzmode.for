      function KteraXYZMode(Param,At)
      use Atoms_mod
      character*(*) Param,At
      logical EqIgCase
      KteraXYZMode=PrvniKiAtXYZMode
      do 1200i=1,NAtXYZMode
        do j=1,MAtXYZMode(i)
          if(EqIgCase(At,Atom(IAtXYZMode(j,i)))) go to 1100
        enddo
        KteraXYZMode=KteraXYZMode+NMAtXYZMode(i)
        go to 1200
1100    do j=1,NMAtXYZMode(i)
          if(EqIgCase(Param,LAtXYZMode(j,i))) go to 9999
          KteraXYZMode=KteraXYZMode+1
        enddo
1200  continue
1500  KteraXYZMode=0
9999  return
      end
