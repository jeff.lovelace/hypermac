      subroutine RefSetPwd
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension smp(16),smc(:),rc(:,:),pb(:),QSymm(3)
      allocatable smc,rc,pb
      integer CrSystemP
      if(nPowder.le.1) then
        do KDatB=1,NDatBlock
          IZdvihP=nParRecPwd*(KDatB-1)
          if(DataType(KDatB).eq.2) then
            if(KiPwd(ILamPwd+IZdvihP).ne.0) then
              i=ICellPwd+(KPhase-1)*NParCellProfPwd
              call SetIntArrayTo(KiPwd(i),NDimI(KPhase)*3+6,0)
              go to 1100
            endif
          endif
        enddo
      endif
1100  CrSystemP=mod(CrSystem(KPhase),10)
      IZdvih=(KPhase-1)*NParCellProfPwd
      if(iauts.ne.0) then
        ICell=ICellPwd+IZdvih
        if(CrSystem(KPhase).eq.-CrSystemTrigonal) then
          do i=2,3
            if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
            neq=neq+1
            lat(neq)=PhaseName(KPhase)
            lpa(neq)=LCell(i)
            lnp(neq)=-ndoffPwd-ICell-i+1
            lnpo(neq)=0
            pab(neq)=0.
            pnp(1,neq)=-ndoffPwd-ICell
            pat(1,neq)=PhaseName(KPhase)
            ppa(1,neq)=LCell(1)
            pko(1,neq)=1.
            npa(neq)=1
            call SetEq(neq,neq)
            if(ErrFlag.ne.0) go to 9999
          enddo
          do i=5,6
            if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
            neq=neq+1
            lat(neq)=PhaseName(KPhase)
            lpa(neq)=LCell(i)
            lnp(neq)=-ndoffPwd-ICell-i+1
            lnpo(neq)=0
            pab(neq)=0.
            pnp(1,neq)=-ndoffPwd-ICell-3
            pat(1,neq)=PhaseName(KPhase)
            ppa(1,neq)=LCell(4)
            pko(1,neq)=1.
            npa(neq)=1
            call SetEq(neq,neq)
            if(ErrFlag.ne.0) go to 9999
          enddo
          go to 1500
        endif
        if(CrSystemP.eq.5) CrSystemP=6
        if(CrSystemP.ge.2) then
          do i=4,6
            if(CrSystemP.ne.2.or.Monoclinic(KPhase)+3.ne.i) then
              if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
              neq=neq+1
              lat(neq)=PhaseName(KPhase)
              lpa(neq)=LCell(i)
              lnp(neq)=-ndoffPwd-ICell-i+1
              lnpo(neq)=0
              npa(neq)=0
              if(CrSystemP.eq.6.and.i.eq.6) then
                pab(neq)=120.
              else
                pab(neq)=90.
              endif
              call SetEq(neq,neq)
              if(ErrFlag.ne.0) go to 9999
            endif
          enddo
          if(NDim(KPhase).eq.4) then
            j=ICell+6
            do i=1,3
              if(CrSystemP.eq.2) then
                if(abs(QuPwd(Monoclinic(KPhase),1,KPhase)).eq.0..or.
     1             abs(QuPwd(Monoclinic(KPhase),1,KPhase)).eq..5) then
                  if(i.eq.Monoclinic(KPhase)) KiPwd(j)=0
                else
                  if(i.ne.Monoclinic(KPhase)) KiPwd(j)=0
                endif
              else
                if(abs(QuPwd(i,1,KPhase)).eq.0.) KiPwd(j)=0
              endif
              j=j+1
            enddo
          endif
        endif
        if(CrSystemP.ge.4) then
          do i=2,2+CrSystemP/7
            if(neq.ge.mxe) call ReallocEquations(mxe+100,mxep)
            neq=neq+1
            lat(neq)=PhaseName(KPhase)
            lpa(neq)=LCell(i)
            lnp(neq)=-ndoffPwd-ICell-i+1
            lnpo(neq)=0
            pab(neq)=0.
            pnp(1,neq)=-ndoffPwd-ICell
            pat(1,neq)=PhaseName(KPhase)
            ppa(1,neq)=LCell(1)
            pko(1,neq)=1.
            npa(neq)=1
            call SetEq(neq,neq)
            if(ErrFlag.ne.0) go to 9999
          enddo
        endif
      endif
1500  if(CrSystemP.eq.1) go to 9999
      do KDatB=1,NDatBlock
        IZdvihP=IZdvih+(KDatB-1)*NParProfPwd
        if(KPref(KPhase,KDatB).eq.IdPwdPrefNone) SPref(KPhase,KDatB)=0
        if(KStrain(KPhase,KDatB).eq.IdPwdStrainTensor) then
          if(NDim(KPhase).eq.4) then
            i=index(grupa(KPhase),'(')
            j=index(grupa(KPhase),')')
            if(i.gt.0.and.j.gt.0) Cislo=grupa(KPhase)(i:j)
          endif
          if(CrSystemP.eq.2) then
            grupa(KPhase)='p2/m'
          else if(CrSystemP.eq.3) then
            grupa(KPhase)='pmmm'
          else if(CrSystemP.eq.4) then
            grupa(KPhase)='p4/mmm'
          else if(CrSystemP.eq.5) then
            grupa(KPhase)='p6/mmm'
          else if(CrSystemP.eq.6) then
            grupa(KPhase)='p6/mmm'
          else
            grupa(KPhase)='pm-3m'
          endif
          NDimO=0
          if(NDimI(KPhase).eq.1) then
            grupa(KPhase)=grupa(KPhase)(:idel(grupa(KPhase)))//
     1                    Cislo(:idel(Cislo))
          else if(NDimI(KPhase).gt.1) then
            NDimO=NDim(KPhase)
            NDim(KPhase)=3
            NDimI(KPhase)=0
            NDimQ(KPhase)=9
          endif
          call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
          nc=0
          if(NDimI(KPhase).eq.1) then
            n=35
            nd=4
          else
            n=15
            nd=3
          endif
          allocate(smc(n**2),rc(2*n,n),pb(2*n))
          call SetRealArrayTo(rc,2*n**2,0.)
          call SetRealArrayTo(pb,2*n,0.)
          do i=1,NSymmN(KPhase)
            if(n.eq.15) then
              call MatBlock3(rm6(1,i,1,KPhase),smp,NDim(KPhase))
            else
              call CopyMat(rm6(1,i,1,KPhase),smp,NDim(KPhase))
            endif
            do j=1,2
              if(j.eq.2) then
                do k=1,nd**2
                  smp(k)=-smp(k)
                enddo
              endif
              if(nd.eq.3) then
                call srotc(smp,4,smc)
              else
                call srotc4(smp,4,smc)
              endif
              call AveSpec(rc,pb,nc,0,smc,n)
            enddo
          enddo
          call speceq(rc,pb,nc,n,ndoffPwd+IStPwd+IZdvihP-1)
          if(ErrFlag.ne.0) cycle
          if(NDimI(KPhase).eq.1) then
            do i=1,4
              if(i.eq.1) then
                j=20
              else if(i.eq.2) then
                j=30
              else if(i.eq.3) then
                j=34
              else if(i.eq.4) then
                j=35
              endif
              k=IStPwd+IZdvihP+j-1
              StPwd(j,KPhase,KDatBlock)=0.
              KiPwd(k)=0
            enddo
          endif
          if(NDimO.gt.0) then
            NDim(KPhase)=NDimO
            NDimI(KPhase)=NDimO-3
            NDimQ(KPhase)=NDimO**2
          endif
          call iom50(0,0,fln(:ifln)//'.m50')
          if(MaxNSymmMol.gt.MaxNSymm) then
            call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                       NPhase,0)
            MaxNSymm=MaxNSymmMol
          endif
          deallocate(smc,rc,pb)
        endif
      enddo
9999  return
      end
