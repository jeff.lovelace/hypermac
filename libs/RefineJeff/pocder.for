      integer function pocder(k)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(k.gt.0) then
        pocder=PrvniKiAtomu(k)-1
      else if(k.lt.0) then
        pocder=PrvniKiMolekuly(-k)-1
      else
        pocder=0
      endif
      return
      end
