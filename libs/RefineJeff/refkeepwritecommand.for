      subroutine RefKeepWriteCommand(Command,ich)
      use Refine_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun,nButtSelAtoms
      integer AtomToBeFilled
      character*256 Command,EdwStringQuest,t256
      character*80  ErrString
      logical VolaRefine,Automatic,Recalculate,SaveKeepCommands,
     1        MapAlreadyUsed,TryAutomaticRun
      save /KeepQuest/
      VolaRefine=.true.
      Automatic=.false.
      go to 1000
      entry EM40KeepWriteCommand(Command,ich)
      VolaRefine=.false.
      Automatic=.false.
      go to 1000
      entry EM40KeepWriteCommandAut(Command,ich)
      VolaRefine=.false.
      Automatic=.true.
1000  ich=0
      KeepTypeMain=KeepType(1)/10
      KeepTypeAdd=mod(KeepType(1)-1,10)+1
      if(VolaRefine) then
        if(KeepTypeMain.eq.IdKeepHydro.or.KeepTypeMain.eq.IdKeepADP)
     1    then
          t256=EdwStringQuest(nEdwCentr)
        else
          t256=EdwStringQuest(nEdwList)
        endif
      else
        t256=KeepAtCentr(1)
      endif
      if(t256.eq.' ') go to 9200
      Command='keep '//CKeepType(KeepTypeMain)
      if(KeepTypeMain.eq.IdKeepHydro) then
        Cislo=CKeepHydro(KeepTypeAdd)
      else if(KeepTypeMain.eq.IdKeepGeom) then
        Cislo=CKeepGeom(KeepTypeAdd)
      else if(KeepTypeMain.eq.IdKeepADP) then
        Cislo='riding'
      endif
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      if(KeepTypeMain.eq.IdKeepHydro) then
        if(t256.eq.' ') then
          ErrString='the central atom has'
          ich=1
          go to 9000
        endif
        call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,IdSymmNo,
     1                      IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') then
          ich=1
          go to 9100
        endif
        Command=Command(:idel(Command)+1)//t256(:idel(t256))
        if(.not.Automatic)
     1     call FeQuestIntFromEdw(nEdwNNeigh,KeepNNeigh(1))
        n=KeepNNeigh(1)
        if(KeepType(1).ne.IdKeepHApical) then
          nh=NAtMax-n
        else
          nh=1
        endif
        if(UseAnchor) n=n+1
        write(Cislo,FormI15) n
        call Zhusti(Cislo)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
        if(KeepType(1).ne.IdKeepHApical) then
          write(Cislo,FormI15) nh
          call Zhusti(Cislo)
          Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
        endif
        if(.not.Automatic) nEdw=nEdwNeighFirst
        do i=1,KeepNNeigh(1)
          if(Automatic) then
            t256=KeepAtNeigh(1,i)
          else
            t256=EdwStringQuest(nEdw)
            if(t256.eq.' ') then
              if(KeepNNeigh(1).gt.1) then
               ErrString='some of neighbor atoms'
              else
                ErrString='the neighbor atom'
              endif
              ich=1
              go to 9000
            endif
          endif
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmYes,IdAtMolMixNo,ErrString)
          if(ErrString.ne.' ') then
            ich=1
            go to 9100
          endif
          Command=Command(:idel(Command)+1)//t256(:idel(t256))
          if(.not.Automatic) nEdw=nEdw+1
        enddo
        if(UseAnchor) then
          if(.not.Automatic)
     1      call FeQuestRealFromEdw(nEdwTorsAngle,KeepAngleH(1))
          write(Cislo,'(f15.3)') KeepAngleH(1)
          call ZdrcniCisla(Cislo,1)
          Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
          if(Automatic) then
            t256=KeepAtAnchor(1)
          else
            t256=EdwStringQuest(nEdwAnchor)
          endif
          if(t256.eq.' ') then
            ErrString='the anchor atom'
            ich=1
            go to 9000
          endif
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmYes,IdAtMolMixNo,ErrString)
          if(ErrString.ne.' ') then
            ich=1
            go to 9100
          endif
          Command=Command(:idel(Command)+1)//t256(:idel(t256))
        endif
        if(.not.Automatic)
     1    call FeQuestRealFromEdw(nEdwHDist,KeepDistH(1))

        write(Cislo,'(f15.3)') KeepDistH(1)
        call ZdrcniCisla(Cislo,1)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
        nEdw=nEdwHFirst
        do i=1,nh
          if(Automatic) then
            t256=KeepAtH(1,i)
          else
            t256=EdwStringQuest(nEdw)
          endif
          if(t256.eq.' ') then
            if(KeepNNeigh(1).gt.1) then
              ErrString='some of hydrogen atoms'
            else
              ErrString='the hydrogen atom'
            endif
            ich=1
            go to 9000
          endif
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmNo,IdAtMolMixNo,ErrString)
          if(ErrString.ne.' ') then
            ich=1
            go to 9100
          endif
          Command=Command(:idel(Command)+1)//t256(:idel(t256))
          if(.not.Automatic) nEdw=nEdw+1
        enddo
      else if(KeepTypeMain.eq.IdKeepGeom) then
        call TestAtomString(t256,IdWildYes,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') then
          ich=1
          go to 9100
        endif
        Command=Command(:idel(Command)+1)//t256(:idel(t256))
      else if(KeepTypeMain.eq.IdKeepADP) then
        if(t256.eq.' ') then
          ErrString='the central atom has'
          ich=1
          go to 9000
        endif
        call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,IdSymmNo,
     1                      IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') then
          ich=1
          go to 9100
        endif
        Command=Command(:idel(Command)+1)//t256(:idel(t256))
        if(VolaRefine)
     1    call FeQuestRealFromEdw(nEdwARiding,KeepADPExtFac(1))
        write(Cislo,'(f15.5)') KeepADPExtFac(1)
        call ZdrcniCisla(Cislo,1)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
        if(VolaRefine) then
          call FeQuestIntFromEdw(nEdwNH,nh)
        else
          nh=KeepNH(1)
        endif
        if(.not.Automatic) nEdw=nEdwHFirst
        do i=1,nh
          if(Automatic) then
            t256=KeepAtH(1,i)
          else
            t256=EdwStringQuest(nEdw)
          endif
          if(t256.eq.' ') then
            if(KeepNNeigh(1).gt.1) then
              ErrString='some of hydrogen atoms'
            else
              ErrString='the hydrogen atom'
            endif
            ich=1
            go to 9000
          endif
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmNo,IdAtMolMixNo,ErrString)
          if(ErrString.ne.' ') then
            ich=1
            go to 9100
          endif
          Command=Command(:idel(Command)+1)//t256(:idel(t256))
          if(.not.Automatic) nEdw=nEdw+1
        enddo
      endif
      go to 9999
9000  ErrString=ErrString(:idel(ErrString)+1)//'has not been defined.'
9100  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
      go to 9999
9200  ich=1
9999  return
      end
