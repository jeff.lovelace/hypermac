      subroutine RefKeep
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun,nButtSelAtoms
      character*256 EdwStringQuest,Veta
      character*80 AtFill(5)
      character*8  AtomNames(:)
      character*2  nty
      integer WhatHappened,AtomToBeFilled,isfp(:)
      external RefKeepReadCommand,RefKeepWriteCommand,RefKeepUpdateQuest
     1        ,FeVoid
      logical CrwLogicQuest,Recalculate,SaveKeepCommands,MapAlreadyUsed,
     1        TryAutomaticRun,Brat(:)
      allocatable AtomNames,Brat,isfp
      save /KeepQuest/
      if(NKeepMax.le.0) call ReallocKeepCommands(1,max(NKeepAtMax,5))
      KPhaseIn=KPhase
      NKeep=max(NKeep,1)
      NAtFill=0
      do i=1,NAtInd+NAtPos
        NAtFill=NAtFill+1
      enddo
      do i=NAtMolFr(1,1),NAtAll
        NAtFill=NAtFill+1
      enddo
      allocate(AtomNames(NAtFill),Brat(NAtFill),isfp(NAtFill))
      NAtFill=0
      do i=1,NAtInd+NAtPos
        NAtFill=NAtFill+1
        AtomNames(NAtFill)=Atom(i)
        isfp(NAtFill)=isf(i)
      enddo
      do i=NAtMolFr(1,1),NAtAll
        NAtFill=NAtFill+1
        AtomNames(NAtFill)=Atom(i)
        isfp(NAtFill)=isf(i)
      enddo
      xqd=600.
      i=7
      call RepeatCommandsProlog(id,fln(:ifln)//'_keep.tmp',xqd,i,il,
     1                          OKForBasicFiles)
      ilp=il
      xpom=5.
      tpom=xpom+10.+CrwgXd
      Veta='H%ydrogens'
      xpom1=tpom+FeTxLengthUnder(Veta)+30.
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          Veta='Geo%metry'
          nCrwTypeHydro=CrwLastMade
        else if(i.eq.2) then
          Veta='%ADP'
          nCrwTypeGeom=CrwLastMade
        else if(i.eq.3) then
          nCrwTypeADP=CrwLastMade
        endif
      enddo
      il=ilp
      xpom=xpom1
      tpom=xpom+10.+CrwgXd
      Veta='T%etrahedral'
      xpom1=tpom+FeTxLengthUnder(Veta)+30.
      nTypeHydro=3
      do i=1,nTypeHydro
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        if(i.eq.1) then
          Veta='T%rigonal'
          nCrwTetra=CrwLastMade
        else if(i.eq.2) then
          Veta='A%pical'
          nCrwTriangl=CrwLastMade
        else if(i.eq.3) then
          nCrwApical=CrwLastMade
        endif
      enddo
      il=ilp
      Veta='%Plane'
      nTypeGeom=3
      do i=1,nTypeGeom
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,3)
        if(i.eq.1) then
          Veta='%Rigid'
          nCrwPlane=CrwLastMade
        else if(i.eq.2) then
          Veta='Ri%gid modulation'
          nCrwRigid=CrwLastMade
        else
          nCrwRigidMod=CrwLastMade
        endif
      enddo
      il=ilp
      Veta='%Riding'
      nTypeADP=1
      do i=1,nTypeADP
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,4)
        if(i.eq.1) then
          nCrwADPRiding=CrwLastMade
        endif
      enddo
      il=ilp+4
      Veta='%List of atoms'
      tpom=xqd*.5
      xpom=5.
      dpom=xqd-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,Veta,'C',dpom,EdwYd,0)
      nEdwList=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      il=il+2
      Veta='Select at%oms'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtSelAtoms=ButtonLastMade
      il=ilp+1
      dpom=100.
      xpom=xpom1
      tpom=xpom+dpom+10.
      Veta='Ce%ntral'
      do i=1,2
        if(i.eq.1) then
          j=1
        else
          j=0
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,j)
        if(i.eq.1) then
          Veta='%H dist.'
          nEdwCentr=EdwLastMade
          xpom=tpom+FeTxLengthUnder(Veta)+30.
          tpom=xpom+dpom+10.
          xpom2=xpom
        else if(i.eq.2) then
          nEdwHDist=EdwLastMade
          Veta='E%xtension'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,j)
          nEdwARiding=EdwLastMade
        endif
      enddo
      il=il+1
      Veta='Nei%ghbor(s)'
      xpom=xpom1
      dpom1=30.
      tpom=xpom+dpom1+25.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom1,EdwYd,1)
      nEdwNNeigh=EdwLastMade
      xpom=xpom2+dpom*.5
      call FeQuestLblMake(id,xpom,il,'Hydrogen(s)','C','N')
      nLblH=LblLastMade
      call FeQuestLblOff(nLblH)
      Veta='%Hydrogen(s)'
      xpom=xpom2
      dpom1=30.
      tpom=xpom+dpom1+25.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom1,EdwYd,1)
      nEdwNH=EdwLastMade
      xpom=xpom1
      ilp=il
      do j=1,2
        tpom=xpom+dpom+10.
        do i=1,5
          il=il+1
          write(Veta,'(i1,a2)') i,nty(i)
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          if(j.eq.1) then
            if(i.eq.1) nEdwNeighFirst=EdwLastMade
          else
            if(i.eq.1) nEdwHFirst=EdwLastMade
          endif
        enddo
        if(j.eq.1) then
          nEdwNeighLast=EdwLastMade
          il=ilp
          xpom=xpom2
        else
          nEdwHLast=EdwLastMade
        endif
      enddo
      il=il-1
      Veta='Use anch%or atom =>'
      tpom=xpom1-FeTxLengthUnder(Veta)-10.
      xpom=tpom-10.-CrwXd
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseAnchor=CrwLastMade
      Veta=' '
      xpom=xpom1
      do i=1,2
        tpom=xpom+dpom+10.
        if(i.eq.1) then
          j=1
        else
          j=0
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,j)
        if(i.eq.1) then
          nEdwAnchor=EdwLastMade
          Veta='Tors%.angle'
          xpom=xpom2
        else if(i.eq.2) then
          nEdwTorsAngle=EdwLastMade
        endif
      enddo
      il=il+1
      xpom=xpom2
      Veta='Se%lect neighbors'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtSelect=ButtonLastMade
1300  call EM40SetDefaultKeepH(1)
      KeepType(1)=11
      KeepAtCentr(1)=' '
      KeepAtAnchor(1)=' '
      KeepNNeigh(1)=3
      NNeigh=3
      KeepNH(1)=1
      KeepDistH(1)=1.
      KeepADPExtFac(1)=1.2
      KeepAngleH(1)=180.
      UseAnchor=.false.
1350  KeepTypeOld=0
      KeepTypeMainOld=0
      KeepTypeAddOld=0
1400  KeepTypeMain=KeepType(1)/10
      KeepTypeAdd=mod(KeepType(1)-1,10)+1
      nCrw=nCrwTypeHydro
      do i=1,nTypeHydro
        call FeQuestCrwOpen(nCrw,i.eq.KeepTypeMain)
        nCrw=nCrw+1
      enddo
      if(KeepTypeMainOld.ne.KeepTypeMain) then
        if(KeepTypeMainOld.eq.IdKeepHydro.or.KeepTypeMainOld.le.0) then
          nCrw=nCrwTetra
          do i=1,nTypeHydro
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
        endif
        if(KeepTypeMainOld.eq.IdKeepGeom.or.KeepTypeMainOld.le.0) then
          nCrw=nCrwPlane
          do i=1,nTypeGeom
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
        endif
        if(KeepTypeMainOld.eq.IdKeepADP.or.KeepTypeMainOld.le.0) then
          nCrw=nCrwADPRiding
          do i=1,nTypeADP
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
        endif
      endif
      if(KeepType(1).ne.KeepTypeOld) then
        if(KeepTypeMain.eq.IdKeepHydro) then
          nCrw=nCrwTetra
          do i=1,nTypeHydro
            call FeQuestCrwOpen(nCrw,i.eq.KeepTypeAdd)
            nCrw=nCrw+1
          enddo
        else if(KeepTypeMain.eq.IdKeepGeom) then
          nCrw=nCrwPlane
          do i=1,nTypeGeom
            call FeQuestCrwOpen(nCrw,i.eq.KeepTypeAdd)
            if(i.eq.nTypeGeom.and.NDimI(KPhase).le.0)
     1        call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
        else if(KeepTypeMain.eq.IdKeepADP) then
          nCrw=nCrwADPRiding
          do i=1,nTypeADP
            call FeQuestCrwOpen(nCrw,i.eq.KeepTypeAdd)
            nCrw=nCrw+1
          enddo
        endif
      endif
      if(KeepTypeMain.eq.IdKeepHydro.or.KeepTypeMain.eq.IdKeepADP) then
        if(KeepType(1).eq.IdKeepHApical) then
          NAtMax=6
          KeepNH(1)=1
          UseAnchor=.false.
          if(KeepTypeOld.eq.IdKeepARiding) NNeigh=NAtMax-KeepNH(1)
        else if(KeepType(1).eq.IdKeepHTetraHed) then
          NAtMax=4
          if(KeepTypeOld.eq.IdKeepHTriangl) then
            NNeigh=NNeigh+1
          else if(KeepTypeOld.eq.IdKeepHApical) then
            NNeigh=min(NNeigh,NAtMax-1)
          else if(KeepTypeOld.eq.IdKeepARiding) then
            KeepNH(1)=min(KeepNH(1),3)
            NNeigh=NAtMax-KeepNH(1)
          endif
        else if(KeepType(1).eq.IdKeepHTriangl) then
          NAtMax=3
          if(KeepTypeOld.eq.IdKeepHTetraHed) then
            if(NNeigh.eq.1) then
              KeepNH(1)=KeepNH(1)-1
            else
              NNeigh=NNeigh-1
            endif
          else if(KeepTypeOld.eq.IdKeepHApical) then
            NNeigh=min(NNeigh,NAtMax-1)
          else if(KeepTypeOld.eq.IdKeepARiding) then
            KeepNH(1)=min(KeepNH(1),2)
            NNeigh=NAtMax-KeepNH(1)
          endif
        else if(KeepType(1).eq.IdKeepARiding) then
          NAtMax=4
          NNeigh=0
          UseAnchor=.false.
        endif
        KeepNNeigh(1)=NNeigh
        if(UseAnchor) KeepNNeigh(1)=KeepNNeigh(1)+1
      endif
1500  if(KeepTypeMain.eq.IdKeepHydro.or.KeepTypeMain.eq.IdKeepADP) then
        call FeQuestEdwClose(nEdwList)
        call FeQuestButtonClose(nButtSelAtoms)
        if(KeepTypeMain.eq.IdKeepHydro.and.KeepNH(1).eq.NAtMax-1) then
          UseAnchor=NAtMax.lt.KeepNNeigh(1)+KeepNH(1)
          call FeQuestCrwOpen(nCrwUseAnchor,UseAnchor)
        else
          call FeQuestCrwClose(nCrwUseAnchor)
          UseAnchor=.false.
        endif
        if(UseAnchor) then
          call FeQuestStringEdwOpen(nEdwAnchor,KeepAtAnchor(1))
          call FeQuestRealEdwOpen(nEdwTorsAngle,KeepAngleH(1),.false.,
     1                            .false.)
        else
          call FeQuestEdwClose(nEdwAnchor)
          call FeQuestEdwClose(nEdwTorsAngle)
        endif
        if(KeepTypeMain.eq.IdKeepHydro) then
          if(KeepTypeMainOld.ne.IdKeepHydro) then
            call FeQuestEdwClose(nEdwARiding)
            call FeQuestEdwClose(nEdwNH)
            call FeQuestLblOn(nLblH)
          endif
          call FeQuestIntEdwOpen(nEdwNNeigh,NNeigh,.false.)
          call FeQuestEudOpen(nEdwNNeigh,1,NAtMax-1,1,0.,0.,0.)
          call FeQuestRealEdwOpen(nEdwHDist,KeepDistH(1),.false.,
     1                            .false.)
        else
          if(KeepTypeMainOld.ne.IdKeepADP) then
            call FeQuestEdwClose(nEdwHDist)
            call FeQuestEdwClose(nEdwNNeigh)
            call FeQuestLblOff(nLblH)
          endif
          call FeQuestIntEdwOpen(nEdwNH,KeepNH(1),.false.)
          call FeQuestEudOpen(nEdwNH,1,4,1,0.,0.,0.)
          call FeQuestRealEdwOpen(nEdwARiding,KeepADPExtFac(1),.false.,
     1                            .false.)
        endif
        call FeQuestStringEdwOpen(nEdwCentr,KeepAtCentr(1))
        nEdwRepeatCheck=nEdwCentr
        do j=1,2
          if(j.eq.1) then
            nEdw=nEdwNeighFirst
            NMaxP=NNeigh
          else
            nEdw=nEdwHFirst
            NMaxP=KeepNH(1)
          endif
          do i=1,5
            if(i.le.NMaxP) then
              if(j.eq.1) then
                Veta=KeepAtNeigh(1,i)
              else
                Veta=KeepAtH(1,i)
              endif
              call FeQuestStringEdwOpen(nEdw,Veta)
            else
              call FeQuestEdwClose(nEdw)
            endif
            nEdw=nEdw+1
          enddo
        enddo
        call FeQuestButtonOpen(nButtSelect,ButtonOff)
      else
        if(KeepTypeMainOld.ne.IdKeepGeom) then
          do j=1,2
            if(j.eq.1) then
              nEdw=nEdwNeighFirst
            else
              nEdw=nEdwHFirst
            endif
            do i=1,5
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          enddo
          call FeQuestEdwClose(nEdwNNeigh)
          call FeQuestEdwClose(nEdwNH)
          call FeQuestEdwClose(nEdwCentr)
          call FeQuestEdwClose(nEdwHDist)
          call FeQuestEdwClose(nEdwARiding)
          call FeQuestButtonClose(nButtSelect)
          call FeQuestLblOff(nLblH)
          call FeQuestEdwClose(nEdwAnchor)
          call FeQuestEdwClose(nEdwTorsAngle)
          UseAnchor=.false.
          call FeQuestButtonOpen(nButtSelAtoms,ButtonOff)
        endif
        Veta=KeepAt(1,1)
        do i=2,KeepN(1)
          Veta=Veta(:idel(Veta)+1)//KeepAt(1,i)(:idel(KeepAt(1,i)))
        enddo
        call FeQuestStringEdwOpen(nEdwList,Veta)
        nEdwRepeatCheck=nEdwList
      endif
      KeepTypeOld=KeepType(1)
      KeepTypeMainOld=KeepTypeOld/10
      KeepTypeAddOld=mod(KeepTypeOld-1,10)+1
      KeepNHOld=KeepNH(1)
      KeepNNeighOld=KeepNNeigh(1)
2000  MakeExternalCheck=1
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1                     RefKeepReadCommand,RefKeepWriteCommand,
     2                     RefKeepUpdateQuest,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        KeepTypeMain=KeepType(1)/10
        if(KeepTypeMain.eq.IdKeepHydro) then
          NNeigh=KeepNNeigh(1)
          if(UseAnchor) then
            NNeigh=NNeigh-1
          else
            KeepAtAnchor(1)=' '
          endif
          do i=NNeigh+1,5
            KeepAtNeigh(1,i)=' '
          enddo
          do i=KeepNH(1)+1,4
            KeepAtH(1,i)=' '
          enddo
        endif
        go to 1350
      endif
      if(CheckType.eq.EventCrw) then
        if(CheckNumber.eq.nCrwTypeHydro) then
          KeepType(1)=IdKeepHydro*10+1
        else if(CheckNumber.eq.nCrwTypeGeom) then
          KeepType(1)=IdKeepGeom*10+1
        else if(CheckNumber.eq.nCrwTypeADP) then
          KeepType(1)=IdKeepADP*10+1
        else if(CheckNumber.eq.nCrwTetra) then
          KeepType(1)=IdKeepHTetraHed
        else if(CheckNumber.eq.nCrwTriangl) then
          KeepType(1)=IdKeepHTriangl
        else if(CheckNumber.eq.nCrwApical) then
          KeepType(1)=IdKeepHApical
        else if(CheckNumber.eq.nCrwPlane) then
          KeepType(1)=IdKeepGPlane
        else if(CheckNumber.eq.nCrwRigid) then
          KeepType(1)=IdKeepGRigid
        else if(CheckNumber.eq.nCrwRigidMod) then
          KeepType(1)=IdKeepGRigidMod
        else if(CheckNumber.eq.nCrwADPRiding) then
          KeepType(1)=IdKeepARiding
        else if(CheckNumber.eq.nCrwUseAnchor) then
          UseAnchor=CrwLogicQuest(nCrwUseAnchor)
          if(UseAnchor) then
            KeepNNeigh(1)=NNeigh+1
            KeepAtAnchor(1)=' '
          else
            KeepNNeigh(1)=NNeigh
          endif
          EventType=EventEdw
          EventNumber=nEdwAnchor
          go to 1500
        endif
        go to 1400
      else if(CheckType.eq.EventEdw) then
        if(CheckNumber.eq.nEdwNNeigh) then
          call FeQuestIntFromEdw(nEdwNNeigh,NNeigh)
          if(KeepType(1).eq.IdKeepHTetraHed.or.
     1       KeepType(1).eq.IdKeepHTriangl) then
            KeepNH(1)=NAtMax-NNeigh
            KeepNNeigh(1)=NNeigh
            if(UseAnchor.and.KeepNNeigh(1).eq.1)
     1        KeepNNeigh(1)=KeepNNeigh(1)+1
          else if(KeepType(1).eq.IdKeepHApical) then
            KeepNNeigh(1)=NNeigh
          endif
          if(KeepNH(1).ne.KeepNHOld.or.KeepNNeigh(1).ne.KeepNNeighOld)
     1      then
            EventType=EventEdw
            EventNumber=nEdwNNeigh
            go to 1500
          endif
        else if(CheckNumber.eq.nEdwNH) then
          call FeQuestIntFromEdw(nEdwNH,KeepNH(1))
          if(KeepNH(1).ne.KeepNHOld) then
            EventType=EventEdw
            EventNumber=nEdwNH
            go to 1500
          endif
        else if(CheckNumber.eq.nEdwCentr) then
          Veta=EdwStringQuest(CheckNumber)
          KeepAtCentr(1)=Veta
          if(Veta.ne.' ') then
            ia=ktat(AtomNames,NAtFill,Veta)
            if(ia.le.0) go to 2200
            isw=iswa(ia)
            KPhase=kswa(ia)
          endif
        else if(CheckNumber.ge.nEdwNeighFirst.and.
     1          CheckNumber.le.nEdwNeighLast) then
          i=CheckNumber-nEdwNeighFirst+1
          Veta=EdwStringQuest(CheckNumber)
          KeepAtNeigh(1,i)=Veta
          if(Veta.ne.' ') then
            i=index(Veta,'#')
            if(i.gt.0) Veta(i:)=' '
            ia=ktat(AtomNames,NAtFill,Veta)
            if(ia.le.0) go to 2200
            isw=iswa(ia)
            KPhase=kswa(ia)
          endif
        else if(CheckNumber.ge.nEdwHFirst.and.
     1          CheckNumber.le.nEdwHLast) then
          i=CheckNumber-nEdwHFirst+1
          Veta=EdwStringQuest(CheckNumber)
          KeepAtH(1,i)=Veta
          if(Veta.ne.' ') then
            ia=ktat(AtomNames,NAtFill,Veta)
            if(ia.gt.0) then
              isw=iswa(ia)
              KPhase=kswa(ia)
            endif
          endif
        else if(CheckNumber.eq.nEdwAnchor) then
          Veta=EdwStringQuest(CheckNumber)
          KeepAtAnchor(1)=Veta
          if(Veta.ne.' ') then
            ia=ktat(AtomNames,NAtFill,Veta)
            if(ia.le.0) go to 2200
            isw=iswa(ia)
            KPhase=kswa(ia)
          endif
        endif
        go to 2000
2200    Veta='the atom "'//Veta(:idel(Veta))//
     1       '" does not exist, try again.'
        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        EventType=EventEdw
        EventNumber=CheckNumber
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelect)
     1  then
        EventType=EventEdw
        if(AtomToBeFilled.eq.SelectedCentr) then
          EventNumber=nEdwCentr
          if(KeepAtCentr(1).ne.' ') then
            ia=ktat(AtomNames,NAtFill,KeepAtCentr(1))
          else
            ia=0
          endif
          call SelOneAtom('Select central atom',AtomNames,ia,NAtFill,
     1                    ich)
          n=1
          if(ich.ne.0.or.ia.le.0) go to 2000
          isw=iswa(ia)
          KPhase=kswa(ia)
          KeepNAtCentr(1)=ia
          KeepAtCentr(1)=AtomNames(ia)
          go to 1500
        else if(AtomToBeFilled.eq.SelectedAnchor) then
          EventNumber=nEdwAnchor
          Veta=EdwStringQuest(nEdwNeighFirst)
          if(KeepAtNeigh(1,1).ne.' ') then
            ia=ktat(AtomNames,NAtFill,KeepAtNeigh(1,1))
            if(ia.gt.0) then
              isw=iswa(ia)
              KPhase=kswa(ia)
            else
              call FeChybne(-1.,-1.,'the neighbor atom does not '//
     1                      'exist in the given subsystem/phase.',' ',
     2                      SeriousError)
            endif
            call SelNeighborAtoms('Select the anchor atom',3.,
     1                     KeepAtNeigh(1,1),AtFill,1,n,isw,ich)
            if(ich.ne.0) go to 2000
          else
            call FeChybne(-1.,-1.,'first you have to define the '//
     1                   'neighbor atom.',' ',SeriousError)
            EventNumber=nEdwNeighFirst
            go to 2000
          endif
          KeepAtAnchor(1)=AtFill(1)
        else
          if(AtomToBeFilled.eq.SelectedNeigh) then
            EventNumber=nEdwNeighFirst
            nmax=NNeigh
            Veta='Select the neighbor atoms'
          else if(AtomToBeFilled.eq.SelectedHydro) then
            EventNumber=nEdwHFirst
            nmax=KeepNH(1)
            Veta='Select the hydrogen atom'
            if(KeepNH(1).gt.1) Veta=Veta(:idel(Veta))//'s'
          endif
          if(KeepAtCentr(1).ne.' ') then
            ia=ktat(AtomNames,NAtFill,KeepAtCentr(1))
            if(ia.gt.0) then
              isw=iswa(ia)
              KPhase=kswa(ia)
            else
              call FeChybne(-1.,-1.,'the central atom does not '//
     1                      'exist in the given subsystem/phase.',' ',
     2                      SeriousError)
            endif
            call SelNeighborAtoms(Veta,3.,KeepAtCentr(1),AtFill,nmax,n,
     1                            isw,ich)
            if(ich.ne.0) go to 2000
          else
            call FeChybne(-1.,-1.,'first you have to define the '//
     1                   'central atom.',' ',SeriousError)
            EventNumber=nEdwCentr
            go to 2000
          endif
          if(AtomToBeFilled.eq.SelectedNeigh) then
            do i=1,NNeigh
              j=index(AtFill(i),'#')
              if(j.gt.0) AtFill(i)(j:)=' '
              if(i.le.n) then
                KeepAtNeigh(1,i)=AtFill(i)
              else
                KeepAtNeigh(1,i)=' '
              endif
            enddo
          else if(AtomToBeFilled.eq.SelectedHydro) then
            do i=1,KeepNH(1)
              if(i.le.n) then
                KeepAtH(1,i)=AtFill(i)
              else
                KeepAtH(1,i)=' '
              endif
            enddo
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelAtoms)
     1  then
        Veta='Select atoms to define'
        if(KeepType(1).eq.IdKeepGPlane) then
          Veta=Veta(:idel(Veta))//' plane'
        else
          Veta=Veta(:idel(Veta))//' rigid body unit'
        endif
        call SetLogicalArrayTo(Brat,NAtFill,.false.)
        call SelAtoms(Veta,AtomNames,Brat,isfp,NAtFill,ichp)
        if(ichp.eq.0) then
          n=0
          do i=1,NAtFill
            if(Brat(i)) then
              if(n.ge.NKeepAtMax) then
                KeepN(1)=n
                call ReallocKeepCommands(NKeepMax,2*n)
              endif
              n=n+1
              KeepAt(1,n)=AtomNames(i)
            endif
          enddo
          KeepN(1)=n
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      deallocate(AtomNames,Brat,isfp)
      KPhase=KPhaseIn
      return
      end
