      subroutine zmena(l,prm,sprm,k,at,ip,fct)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 t80
      character*40 atm
      character*20 at1
      character*12 lm,l1
      character*4  chlst
      character*(*) at,l
      logical   EqIgCase
      save atm,lm,iconv
      if(k.eq.1) then
        ip=0
        rxs=0.
        rxm=0.
        atm=' '
        lm='scale'
        if(icykl.eq.0) iconv=0
        call SetIntArrayTo(kxa,11,0)
      else if(k.eq.2) then
        ip=ip+1
        call zhusti(l)
        call KdoCo(ip,at1,l1,1,pom,spom)
        if(VasekTest.eq.1) then
          write(chlst,'(i4)') ip
          if(.not.EqIgCase(l,l1).or..not.EqIgCase(at,at1)) then
            call FeWinMessage(l(:idel(l))//'<=>'//l1//' ... '//Chlst,
     1                        at(:idel(at))//'<=>'//at1)
          endif
        endif
        i2=idel(l)
        i1=1+(11-i2)/2
        ivenr(1)(InvDel+i1:)=l
        ISing=LocateInIntArray(ip,NSingArr,NSing)
        if(ki(ip).ne.0.and.ISing.le.0) then
          dx=dc(ip)*fct
          if(ki(ip).gt.0) then
            if(ki(ip).gt.1000) then
              sprm=0.
              rxp=0.
              if(ki(ip).gt.1000) ki(ip)=ki(ip)-1000
            else
              sprm=der(ip)*fct
              if(sprm.gt.0.) then
                rxp=dx/sprm
              else
                rxp=0.
              endif
            endif
          else
            sprm=-.1
            rxp=0.
          endif
          if(abs(rxp).gt.abs(rxm).and..not.FinalInver) then
            if(.not.IsPowder.or.DoLeBail.eq.0.or.
     1         (.not.EqIgCase(l(1:8),'PrfScale').and.
     2          .not.EqIgCase(l(1:9),'BckgScale'))) then
              rxm=rxp
              atm=at
              lm=l
            endif
          endif
        else
          sprm=0.
          rxp=0.
          dx=0.
        endif
        rxs=rxs+abs(rxp)
        prmo=prm
        if(ki(ip).ne.0) prm=prm+tlum*dx
        call pridej(prmo,sprm,prm)
        rxa(InvDel/10-1)=rxp
        kxa(InvDel/10-1)=ki(ip)
      else
        if(FinalInver) go to 9999
        rxs=rxs/float(nLSRS)
        if(NDynRed.eq.0) then
          if(NInfo.lt.25) Ninfo=Ninfo+1
          t80=lm
          if(atm.ne.' ') t80=t80(:idel(t80))//'['//atm(:idel(atm))//']'
          write(TextInfo(Ninfo),'(''Maximum change/s.u. : '',f9.4,
     1                            '' for '',26a1)')
     2                            rxm,(t80(i:i),i=1,idel(t80))
        endif
        DampFac(ICyklMod6)=tlum
        ChngSUAve(ICyklMod6)=rxs
        ChngSUMax(ICyklMod6)=rxm
        WhatHasMaxChange(ICyklMod6)=t80
        if(ConvChk.eq.1) then
          if(abs(rxm).lt.ConvLim) then
            iconv=iconv+1
            if(iconv.ge.ConvCycl) ncykl=icykl+1
          else
            iconv=0
          endif
        endif
      endif
9999  return
      end
