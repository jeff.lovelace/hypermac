      subroutine RefInform
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      character*128 ven
      character*2 nty
      logical EqIV0
      if(ExistPowder) then
        if(NAtCalc.le.0) DoLeBail=1
        NLeBail=max(NLeBail,0)
      else
        DoLeBail=0
        NLeBail=1
      endif
      ExtDistr(1)=min(ExtDistr(1),2)
      ExtDistr(1)=max(ExtDistr(1),1)
      call TitulekVRamecku('Run parameters')
      if(NAtCalc.le.0.and..not.ExistPowder) ncykl=0
      nPowder=0
      nSingle=0
      do KDatB=1,NDatBlock
        if(UseDatBlockActual(KDatB)) then
          if(iabs(DataType(KDatB)).eq.2) then
            nPowder=nPowder+1
          else
            nSingle=nSingle+1
          endif
        endif
      enddo
      if(ncykl.gt.0) then
        call newln(1)
        if(DoLeBail.eq.0) then
          ven='Structure refinement'
        else
          ven='Profile refinement (le Bail)'
        endif
        write(Cislo,100) ncykl
        call zhusti(Cislo)
        ven=ven(:idel(ven))//' in '//Cislo(:idel(Cislo))//' cycles'
        if(KeyDyn.ge.2) KeyDyn=1
        if(tlum.le.0..or.tlum.gt.1.) tlum=1.
        if(tlum.ne.1.) then
          write(Cislo,102) tlum
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//' with damping '//Cislo(:idel(Cislo))
        endif
        ven=ven(:idel(ven))//' will be performed'
        write(lst,FormA) ven(:idel(ven))
        if(ConvChk.eq.1.and.ncykl.gt.0) then
          call newln(1)
          ven='The refinement will stop if max(change/s.u.)<'
          write(Cislo,102) ConvLim
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))
          if(ConvCycl.gt.1) then
            write(Cislo,100) ConvCycl
            call zhusti(Cislo)
            ven=ven(:idel(ven))//' in '//Cislo(:idel(Cislo))
          endif
          write(lst,FormA) ven(:idel(ven))
        endif
        if(KeyDyn.ne.0) then
          call newln(1)
          ven='The damping factor will be reduced by the factor'
          write(Cislo,102) RedDyn
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
          if(ExistPowder) then
            Cislo='if wRp'
          else
            if(ifsq.ne.1) then
              Cislo='if wR(all)'
            else
              Cislo='if wR2(all)'
            endif
          endif
          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
          ven(idel(ven)+2:)='is larger by more than'
          write(Cislo,102) TolDyn
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
          ven(idel(ven)+1:)='% from the previous value'
          write(lst,FormA) ven(:idel(ven))
          if(NDynCycleMax.gt.0) then
            call newln(1)
            Ven='In the case that in'
            write(Cislo,100) NDynCycleMax
            call ZdrcniCisla(Cislo,1)
            ven=ven(:idel(ven)+1)//Cislo(:idel(Cislo))
            ven(idel(ven)+2:)='consecutive cycles refinement converge'//
     1                        ', the damping factor will change back'
            write(lst,FormA) ven(:idel(ven))
          endif
        endif
        if(UisoLim.gt.0.) then
          call newln(1)
          write(Cislo,'(f15.6)') UisoLim
          call ZdrcniCisla(Cislo,1)
          ven='Atoms reaching Uiso>'//Cislo(:idel(Cislo))//
     1        ' will be disabled from the refinement'
          write(lst,FormA) ven(:idel(ven))
        endif
        if(DoLeBail.ne.0.and.NLeBail.gt.1) then
          call newln(1)
          ven='le Bail decomposition will be performed each'
          write(Cislo,100) NLeBail
          call zhusti(Cislo)
          ven=ven(:idel(ven))//' '//Cislo(:idel(Cislo))//
     1        nty(NLeBail)//' cycle'
          write(lst,FormA) ven(:idel(ven))
        endif
      else
        if(NAtCalc.le.0) then
          call newln(1)
          write(lst,'(''Number of atoms less or equal zero - program '',
     1                ''will prepare file m80 for Patterson '')')
        endif
      endif
      if(.not.ExistPowder) then
        call newln(1)
        if(ifsq.eq.0) then
          write(lst,'(''Refinement based on F'')')
        else
          write(lst,'(''Refinement based on F**2'')')
        endif
        iwq=max(iwq,0)
        iwq=min(iwq,2)
        call newln(1)
        if(iwq.eq.0) then
          ven='Weight 1/sig(Fo)**2 coefficient of unstability :'
          write(Cislo,102) blkoef
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//' '//Cislo(:idel(Cislo))
          if(ifsq.ne.0) ven(14:14)='I'
        else if(iwq.eq.1) then
          ven='Unit weight'
        else
          ven='Cruikshanck''s weight with yomin ='
          write(Cislo,102) yomin
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//' '//Cislo(:idel(Cislo))
          write(Cislo,102) yomax
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//', yomax = '//Cislo(:idel(Cislo))
        endif
        write(lst,FormA) ven(:idel(ven))
        if(UseUnobs.ne.1) then
          call newln(1)
          ven='Reflections with I<'
          write(Cislo,102) slevel
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//
     1        'sig(I) will be omitted'
          write(lst,FormA) ven(:idel(ven))
        endif
        if(snlmn.gt.0..or.snlmx.lt.10.) then
          call newln(1)
          ven='Reflections with sin(th)/lambda outside of interval <'
          write(Cislo,102) snlmn
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//','
          write(Cislo,102) snlmx
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//'> will be omitted'
          write(lst,FormA) ven(:idel(ven))
        endif
        if(iskip.eq.1) then
          call newln(1)
          ven='Reflections with |Fo-Fc|>'
          write(Cislo,102) vyh
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//
     1        'sig(F) will be omitted'
          write(lst,FormA) ven(:idel(ven))
        endif
        kim=max(kim,-2)
        if(kim.ne.-2) call newln(1)
        if(kim.eq.0) then
          write(lst,'(''Only main reflections will be accepted into '',
     1                ''calculation'')')
        else if(kim.eq.-1) then
          write(lst,'(''Only satellite reflections will be accepted '',
     1                ''into calculation'')')
        else if(kim.gt.0) then
          write(lst,'(''Only reflections with satellite index'',i2,
     1                ''or'',i2,'' will be accepted into calculation'')
     2                ') kim,-kim
        endif
        do KPh=1,KPhase
          KPhase=KPh
          if(NComp(KPh).gt.1.and.kic.ne.0) then
            call newln(1)
            write(lst,'(''Only reflections from composite part#'',i1,
     1                  '' will be used in calculation'')') kic
            go to 1150
          endif
        enddo
1150    do i=1,nskrt
          ven='reflections '
          ven=ven(:12)//skupina(i)(:NDim(KPhase))//' : '//
     1        nevzit(i)(:idel(nevzit(i)))
          if(.not.EqIV0(ihsn(1,i),maxNDim))
     1      ven=ven(:idel(ven))//' except : '//vzit(i)(:idel(vzit(i)))
          ven=ven(:idel(ven))//' will not be used in refinement'
          call newln(1)
          write(lst,FormA) ven
        enddo
        if(nxxn.gt.0) then
          call newln(1)
          write(ven,'(''Reflections with flag(s) : '',20i3)')
     1               (nxxp(i),i=1,nxxn)
          ven=ven(:idel(ven))//' will be omitted'
          write(lst,FormA) ven(:idel(ven))
        endif
      else
        if(SigMethod.eq.1) then
          ven='based on error propagation formula'
        else if(SigMethod.eq.2) then
          ven='derived from profile fit'
        else if(SigMethod.eq.3) then
          ven='as maximum from profile fit and error propagation '//
     1        'formula'
        else if(SigMethod.eq.4) then
          ven='as minimum from profile fit and error propagation '//
     1        'formula'
        endif
        ven='Calculation of sig(I) is '//ven(:idel(ven))
        call newln(1)
        write(lst,FormA) ven
      endif
      call newln(1)
      if(nowr.lt.0) then
        write(lst,'(''Only not-matching reflections flagged by # '',
     1              ''(see below) will be printed'')')
      else if(nowr.eq.0) then
        write(lst,'(''Print of reflections suppressed'')')
      else
        write(lst,'(''Full print of reflections'')')
      endif
      if(nowr.ne.0) then
        call newln(1)
        nowr=max(nowr,-2)
        nowr=min(nowr,2)
        if(iabs(nowr).lt.2) then
          write(lst,'(''Print of reflections before first and after '',
     1                ''last cycle of refinement'')')
        else
          write(lst,'(''Print of reflections after last cycle of '',
     1                ''refinement'')')
        endif
        if(UseUnobs.eq.1) then
          call newln(1)
          ven='Reflections with I<'
          write(Cislo,102) slevel
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//
     1        'sig(I) will be classified as unobserved - flag *'
          write(lst,FormA) ven(:idel(ven))
        endif
        if(iskip.ne.1) then
          call newln(1)
          ven='Reflections with |Fo-Fc|>'
          write(Cislo,102) vyh
          call ZdrcniCisla(Cislo,1)
          ven=ven(:idel(ven))//Cislo(:idel(Cislo))//
     1        'sig(I) will be flagged by #'
          write(lst,FormA) ven(:idel(ven))
        endif
        if(TwDetail.gt.0.and.(NTwin.gt.1.or.iover.gt.0)) then
          call newln(1)
          write(lst,'(''Printed reflecion will be followed by a list '',
     1                ''of contributions due to overlap'')')
        else
          TwDetail=0
        endif
      endif
      call newln(1)
      ven='Correlations larger than'
      write(Cislo,102) corr
      call ZdrcniCisla(Cislo,1)
      ven=ven(:idel(ven))//' '//Cislo(:idel(Cislo))//' will be printed'
      write(lst,FormA) ven(:idel(ven))
      if(Lam2Corr.eq.1) then
        call newln(1)
        ven='Correction for labmda/2 will be applied'
        write(lst,FormA) ven(:idel(ven))
      endif
      if(nSingle.gt.0) then
        call newln(1)
        write(lst,FormA)
        call TitulekPodtrzeny('Extinction correction:','-')
        do 1500KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).eq.2) go to 1500
          if(NDatBlock.gt.1) then
            call newln(2)
            write(lst,FormA)
            write(lst,'(''Extintion correction for:'',a)')
     1        DatBlockName(KDatB)(:idel(DatBlockName(KDatB)))
          endif
          if(ExtTensor(KDatB).gt.0) then
            if(ExtTensor(KDatB).eq.1) then
              write(lst,'(''Isotropic correction'')')
            else
              write(lst,'(''Anisotropic correction'')')
            endif
            call newln(1)
            if(ExtType(KDatB).eq.1) then
              write(lst,'(''Extinction type I'')')
            else if(ExtType(1).eq.2) then
              write(lst,'(''Extinction type II'')')
            else
              write(lst,'(''General type of extinction'')')
            endif
            if(ExtType(KDatB).ne.2) then
              call newln(1)
              if(ExtDistr(1).eq.1) then
                write(lst,'(''Gaussian distribution'')')
              else
                write(lst,'(''Lorentzian distribution'')')
              endif
            endif
            call newln(1)
            ven='Radius of spherical sample :'
            write(Cislo,102) ExtRadius(KDatB)
            call ZdrcniCisla(Cislo,1)
            ven=ven(:idel(ven))//' '//Cislo(:idel(Cislo))
            if(iab.ne.0) then
              call newln(5)
              write(lst,'(''Modification for absorption''/
     1              '' a(sin(th)) step 0.1 in sin(th)     : '',10f8.2/
     2              '' b(sin(th)) step 0.1 in sin(th)     : '',10f8.2/
     3              '' dadmi(th)  step 5 degrees in th    : '',10f8.4/
     4              38x,9f8.4)') aa,ba,dadmi
            endif
          else
            call newln(1)
            write(lst,'(''will not be applied'')')
          endif
1500    continue
        call newln(1)
        write(lst,FormA)
      endif
      if(itfmax.gt.2) then
        call newln(4)
        write(lst,'(''For convenience : '',
     1              ''tensors C(ijk)    multiplied by 10**3''/18x,
     2              ''tensors D(ijkl)   multiplied by 10**4''/18x,
     3              ''tensors E(ijklm)  multiplied by 10**5''/18x,
     4              ''tensors F(ijklmn) multiplied by 10**6'')')
      endif
      if(iauts.eq.1) then
        call newln(1)
        write(lst,'(''Automatic procedure for setting of refinement '',
     1              ''keys will be applied'')')
      endif
      if(iautk.eq.1) then
        call newln(1)
        write(lst,'(''Automatic procedure for setting of restrictions'',
     1              '' on atoms/molecules at special positions will '',
     2              ''applied'')')
      endif
      if(isPowder) then
        call PwdSetTOF(KDatBlock)
        call TitulekVRamecku('Powder parameters')
        if(NBackg(KDatBlock).gt.0) then
          if(KBackg(KDatBlock).eq.1) then
            ven='Legendre polynoms'
          else if(KBackg(KDatBlock).eq.2) then
            ven='Chebyshev polynoms'
          else if(KBackg(KDatBlock).eq.3) then
            ven='cos-ortho functions'
          else if(KBackg(KDatBlock).eq.4) then
            ven='cos-GSAS functions'
          else
            go to 2000
          endif
          write(Cislo,100) NBackg(KDatBlock)
          call zhusti(Cislo)
          ven=Cislo(:idel(Cislo))//' terms of '//ven(:idel(ven))
          if(KManBackg(KDatBlock).gt.0) then
            ven=ven(:idel(ven))//
     1          ' will be combined with the manual background'
          else
            ven=ven(:idel(ven))//
     1          ' will be used to describe background'
          endif
        else if(KManBackg(KDatBlock).gt.0) then
          ven='Manual backgrand will be used'
        else
          go to 2000
        endif
        call NewLn(1)
        write(lst,FormA) ven(:idel(ven))
2000    if(isTOF.or.isED) then
          if(KAsym(KDatBlock).eq.IdPwdAsymTOFNone) then
            go to 2200
          else if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
            ven='Profile fuction convoluted with exponential '//
     1          'rise/decay function'
          else if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
            ven='Profile fuction convoluted with exponential '//
     1          'rise/decay function - Jason Hodges'
          endif
        else
          if(KAbsor(KDatBlock).eq.1) then
            ven='Debye-Scherer/cylinder, mir : '
          else if(KAbsor(KDatBlock).eq.2) then
            ven='transmission/flat plate, mid : '
          else if(KAbsor(KDatBlock).eq.3) then
            ven='reflection/flat plate, mid : '
          else
            go to 2100
          endif
          write(Cislo,102) mirPwd(KDatBlock)
          call ZdrcniCisla(Cislo,1)
          ven='Absorption correction '//ven(:idel(ven))//' '//
     1        Cislo(:idel(Cislo))
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
2100      if(KAsym(KDatBlock).eq.IdPwdAsymSimpson) then
            ven='by Simpson method'
          else if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
            ven='by Berar & Baldinozzi method'
          else if(KAsym(KDatBlock).eq.IdPwdAsymDivergence) then
            ven='due to axial divergence - Finger, Cox & Jephcoat'
          else
            go to 2200
          endif
          ven='Asymmetry correction '//ven(:idel(ven))//
     1        ' will be applied'
        endif
        call NewLn(1)
        write(lst,FormA) ven(:idel(ven))
2200    if(NSkipPwd(KDatBlock).gt.0) then
          do 2300i=1,NSkipPwd(KDatBlock)
            if(SkipPwdFr(i,KDatBlock).lt.-200.) go to 2300
            call NewLn(1)
            write(lst,'(''2theta interval from : '',f7.2,'' to : '',
     1                  f7.2,'' will be skipped'')')
     2        SkipPwdFr(i,KDatBlock),SkipPwdTo(i,KDatBlock)
2300      continue
        endif
        do KPh=1,NPhase
          KPhase=KPh
          if(NPhase.gt.1) then
            call NewLn(1)
            write(lst,FormA)
            call TitulekPodtrzeny(PhaseName(KPh),'=')
          endif
          if(KProfPwd(KPh,KDatBlock).eq.IdPwdProfGauss) then
            ven='Gaussian'
          else if(KProfPwd(KPh,KDatBlock).eq.IdPwdProfLorentz) then
            ven='Lorentzian'
          else if(KProfPwd(KPh,KDatBlock).eq.IdPwdProfModLorentz) then
            ven='Modified Lorentzian'
          else if(KProfPwd(KPh,KDatBlock).eq.IdPwdProfVoigt) then
            ven='Pseudo-Voigt'
          endif
          ven=ven(:idel(ven))//' profile function'
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
          write(Cislo,102) PCutOff(KPh,KDatBlock)
          call ZdrcniCisla(Cislo,1)
          ven='The profile is cut outside '//Cislo(:idel(Cislo))//
     1        '*FWHM range'
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
          if(KPref(KPh,KDatBlock).eq.IdPwdPrefMarchDollase) then
            ven='March & Dollase'
          else if(KPref(KPh,KDatBlock).eq.IdPwdPrefSasaUda) then
            ven='Sasa & Uda'
          else
            go to 2500
          endif
          write(Cislo,103)(nint(DirPref(i,KPh,KDatBlock)),i=1,3)
          call Zhusti(Cislo)
          ven='Preference orientation with respect to the axis'//
     1        Cislo(:idel(Cislo))//' according to '//ven(:idel(ven))//
     2        ' will be applied'
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
          if(SPref(KPh,KDatBlock).ne.0) then
            ven='will'
          else
            ven='will not'
          endif
          ven='Summation over equivalents '//ven(:idel(ven))//
     1        ' performed'
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
2500      continue
          if(KStrain(KPh,KDatBlock).eq.IdPwdStrainAxial) then
            write(Cislo,103)(nint(DirPref(i,KPh,KDatBlock)),i=1,3)
            call Zhusti(Cislo)
            ven='Anisotropic strain broadering with respect to the '//
     1          'strain axis'//Cislo(:idel(Cislo))//' will be applied'
          else if(KStrain(KPh,KDatBlock).eq.IdPwdStrainTensor) then
            ven='Anisotropic strain broadering according to Stephens '//
     1          'will be applied'
          else
            go to 2700
          endif
          call NewLn(1)
          write(lst,FormA) ven(:idel(ven))
2700      if(KWleBail(KDatBlock).ne.0.and.DoLeBail.gt.0) then
            call NewLn(1)
            write(lst,'(''Weighting function will be used during '',
     1                  ''leBail decomposition'')')
          endif
        enddo
      endif
      return
100   format(i10)
102   format(f10.3)
103   format('(',2(i2,','),i2,')')
      end
