      subroutine UprSpecOrd(rx,px,n,nx,OrdX)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rx(2*n,n),px(2*n)
      real, allocatable :: rxp(:,:),pxp(:)
      integer OrdX(n)
      integer, allocatable :: OrdXNew(:),OrdXPom(:)
      logical, allocatable :: RowUsed(:),ColumnUsed(:)
      m=2*n
      allocate(rxp(m,n),pxp(m),RowUsed(m),ColumnUsed(n),OrdXNew(n),
     1         OrdXPom(n))
      call CopyVek(rx,rxp,n*m)
      call SetLogicalArrayTo(RowUsed,m,.false.)
      call SetLogicalArrayTo(ColumnUsed,n,.false.)
      call SetIntArrayTo(OrdXNew,n,0)
      do i=1,n
        RXMax=0.
        jm=0
        km=0.
        do j=1,m
          if(RowUsed(j)) cycle
          do k=1,n
            if(ColumnUsed(k)) cycle
            pom=abs(rxp(j,k))
            if(pom.gt.RXMax) then
              jm=j
              km=k
              RXMax=pom
            endif
          enddo
        enddo
        if(km.gt.0) then
          im=i
          OrdXNew(i)=km
          RowUsed(jm)=.true.
          ColumnUsed(km)=.true.
        endif
      enddo
      if(im.lt.n) then
        do i=1,n
          if(LocateInIntArray(i,OrdXNew,n).gt.0) cycle
          im=im+1
          OrdXNew(im)=i
          if(im.ge.n) exit
        enddo
      endif
      do i=1,m
        do j=1,n
          rx(i,j)=rxp(i,OrdXNew(j))
        enddo
      enddo
      do i=1,n
        OrdX(i)=LocateInIntArray(OrdX(i),OrdXNew,n)
      enddo
      call triangl(rx,px,m,n,nx)
      deallocate(rxp,pxp,RowUsed,ColumnUsed,OrdXNew,OrdXPom)
      return
      end
