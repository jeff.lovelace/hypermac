      subroutine RefWorstReflection
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Radka
      character*128 ReflString(10),Veta
      integer NRefl,Order(10),IWdFq(10)
      save NRefl,Order,IWdFq,ReflString
      entry RefWorstReflectionReset
      NRefl=0
      Order=0
      ReflString=' '
      go to 9999
      entry RefWorstReflectionSave(Radka,awdy,wdylim)
      if(NRefl.lt.10) then
        NRefl=NRefl+1
        kam=NRefl
      else
        kam=Order(10)
      endif
      ReflString(kam)=Radka
      IWdFq(kam)=-nint(awdy*1000.)
      if(NRefl.ge.10) then
        call indexx(10,IWdFq,Order)
        wdylim=-float(IWdFq(Order(10)))*.001
      endif
      go to 9999
      entry RefWorstReflectionPrint
      if(NRefl.lt.10) call indexx(NRefl,IWdFq,Order)
      call newpg(0)
      Veta='Fo/Fc list of worst fitted reflections'
      call TitulekVRamecku(Veta)
      call newln(1)
      ivp=NDim(KPhase)*4
      ivk=maxNDim*4
      if(ivk.le.ivp) then
        if(MagPolFlag(KDatBlock).eq.0) then
          write(lst,FormA) RefHeader(:idel(RefHeader))
        else
          write(lst,FormA) RefHeaderMagPol(:idel(RefHeaderMagPol))
        endif
      else
        if(MagPolFlag(KDatBlock).eq.0) then
          write(lst,FormA) RefHeader(:ivp)//
     1                     RefHeader(ivk+1:idel(RefHeader))
        else
          write(lst,FormA) RefHeaderMagPol(:ivp)//
     1                   RefHeaderMagPol(ivk+1:idel(RefHeaderMagPol))
        endif
      endif
      do i=1,NRefl
        call newln(1)
        write(lst,FormA) ReflString(Order(i))
      enddo
9999  return
      end
