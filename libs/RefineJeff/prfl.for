      function Prfl(dtth)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      integer arraysize
      parameter (numtab=14,arraysize=1883)
      dimension nterms(numtab),xp(arraysize),wp(arraysize),xpt(1000),
     1          wpt(1000),OddHermitCoef(21),dpomWdAxDifProf(7)
      integer fstterm(numtab)
      logical JesteNeni(numtab)
      double precision :: QHester,THester,dpom1,dpom2,dpom3,
     1                    QHesterOrginal,
     1                    dpi= 3.141592653589793d0,dpom1p,dpom2p
      data nterms /3,5,10,20,30,40,50,75,100,150,200,300,400,500/
      data fstterm/0,3,8,18,38,68,108,158,233,333,483,683,983,1383/
      data JesteNeni/numtab*.true./
      data OddHermitCoef/
     1       2.,
     2       8.,   -12.,
     3      32.,  -160.,   120.,
     5     128., -1344.,  3360.,   -1680.,
     6     512., -9216., 48384.,  -80640.,  30240.,
     7    2048.,-56320.,506880.,-1774080.,2217600.,-6652809./
      save xp,wp
      prfl=0.
      sigpart=0.
      gampart=0.
      dprdt=0.
      dprdc7=0.
      dFdAlpha12=0.
      dFdBeta12=0.
      dtt=dtth+shift
      if(isTOF) then
        if(KAsym(KDatBlock).eq.IdPwdAsymTOFNone) then
          call pvoigt(dtt,R,dRdT,dRdS,dRdG)
          prfl=prfl+R
          if(CalcDer) then
            sigpart=sigpart+dRdS
            gampart=gampart+dRdG
            dfdt=dRdT
            dprdt=dprdt+dfdt
          endif
        else if(KAsym(KDatBlock).eq.IdPwdAsymTOF1.or.
     1          KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
          call PVoigtTOF(dtt,R,dRdAl,dRdBe,dRdT,dRdS,dRdG)
          prfl=prfl+R
          if(CalcDer) then
            sigpart=sigpart+dRdS
            gampart=gampart+dRdG
            dfdt=dRdT
            dprdt=dprdt+dRdT
            dFdAlpha12=dFdAlpha12+dRdAl
            dFdBeta12=dFdBeta12+dRdBe
          endif
        endif
      else
        if(KAsym(KDatBlock).eq.IdPwdAsymSimpson.or.
     1     KAsym(KDatBlock).eq.IdPwdAsymNone.or.
     2     KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
          do it=1,ntsim
            if(mod(it,2) .eq. 0) then
              simk=4.
            else if(it.eq.1.or.it.eq.ntsim) then
              simk=1.
            else
              simk=2.
            endif
            simc=float((it-1)**2)/tntsim
            dt=dtt+pcot*simc
            call pvoigt(dt,R,dRdT,dRdS,dRdG)
            prfl=prfl+simk*R
            if(CalcDer) then
              sigpart=sigpart+simk*dRdS
              gampart=gampart+simk*dRdG
              dfdt=simk*dRdT
              dprdt=dprdt+dfdt
              dprdc7=dprdc7+simc*dfdt
            endif
          enddo
        else if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
          call pvoigt(dtt,prfl,dprdt,sigpart,gampart)
          z=dtt/fwhm
          z2=z**2
          pom1=-z*ExpJana(-z2,ich)
          k=1
          fnorm=1.
          do i=1,NAsym(KDatBlock)
            if(mod(i,2).eq.1) then
              j=(i+1)/2
              pol=PolByHorn(z2,OddHermitCoef(k),j)*pom1
              k=k+j
              dpdas(i)=cotgth *pol
            else
              dpdas(i)=cotg2th*pol
            endif
            if(i.gt.4) then
              pom2=.1**((i-3)/2)
            else
              pom2=1.
            endif
            dpdas(i)=dpdas(i)*pom2
            fnorm=fnorm+AsymPwd(i,KDatBlock)*dpdas(i)
          enddo
        else if(KAsym(KDatBlock).eq.IdPwdAsymDivergence) then
          if(KUseHS(KDatBlock).eq.1) then
            H=ASymPwd(1,KDatBlock)
            S=ASymPwd(2,KDatBlock)
            HpS=H+S
            HmS=H-S
          else
            HpS=ASymPwd(1,KDatBlock)
            HmS=ASymPwd(2,KDatBlock)
            H=(HpS+HmS)*.5
            S=(HpS-HmS)*.5
          endif
          HpS2=HpS**2
          cos2thq=cos2th**2
          if(H.eq.0..or.S.eq.0.) then
            Einfl=acos(cos2th)
          else
            pom=sqrt(1.+HmS**2)*cos2th
            if(pom.ge.1.) then
              Einfl=0.
            else if(pom.le.-1.) then
              Einfl=Pi
            else
              Einfl=acos(pom)
            endif
          endif
          tmp2=1.+HpS2
          tmp=sqrt(tmp2)*cos2th
          if(abs(tmp).le.1.) then
            Emin=acos(tmp)
            tmp1=tmp2*(1.0-tmp2*cos2thq)
          else
            tmp1=0.
            if(tmp.gt.0.) then
              Emin=0.
            else
              Emin=Pi
            endif
          endif
          if(tmp1.gt.0.and.abs(tmp).le.1.) then
            dEmindA=-HpS*cos2th/sqrt(tmp1)
          else
            dEmindA=0.
          endif
          tmp=100.*abs(peak-emin)/torad
          if(fwhm.le.0.) then
            ngt=max(tmp*4.,10.)
          else
            ngt=max(tmp*4.,10.,.2*tmp/(100.*fwhm/torad))
          endif
          do i=1,numtab
            if(ngt.lt.nterms(i)) go to 4010
          enddo
          i=numtab
4010      ngt=nterms(i)
          it=fstterm(i)
          if(JesteNeni(i)) then
            call gauleg(0.,1.,xp(it+1),wp(it+1),ngt)
            JesteNeni(i)=.false.
          endif
          sumWG=0.
          sumWRG=0.
          sumWdGdA=0.
          sumWRdGdA=0.
          sumWdGdB=0.
          sumWRdGdB=0.
          sumWGdRd2t=0.
          sumWGdRdsig=0.
          sumWGdRdgam=0.
          sumW=0.
          do i=1,ngt
            xpp=xp(i+it)
            wpp=wp(i+it)*(peak-EMin)
            sumW=sumW+wpp
            delta=emin+(peak-emin)*xpp
            sinDELTA=sin(Delta)
            cosDELTA=cos(Delta)
            if(abs(cosDELTA).lt.1.e-15) cosDELTA=1.e-15
            RcosDELTA=1./cosDELTA
            tanDELTA=tan(Delta)
            cosDELTA2=cosDELTA*cosDELTA
            tmp=cosDELTA2-cos2thq
            if(tmp.gt.0.) then
              tmp1=SQRT(tmp)
              F=abs(cos2th)/tmp1
            else
              F=0.
            endif
            if(abs(delta-emin).gt.abs(einfl-emin)) then
              if(H.ge.S) then
                gs=S/H*F*RcosDELTA
                dGdB=gs/S
                dGdA=-gs/H
              else
                gs=F*RcosDELTA
                dGdA=0.
                dGdB=0.
              endif
            else
              gs=.5*RcosDELTA*(-1.+HpS*F)/H
              dGdA=.5*RcosDELTA*(1.-S*F)/H**2
              dGdB=.5*RcosDELTA*F/H
            endif
            call pvoigt(dtt+peak-delta,R,dRdT,dRdS,dRdG)
            sumWG=sumWG+wpp*gs
            sumWRG=sumWRG+wpp*R*gs
            if(CalcDer) then
              sumWRdGdA=sumWRdGdA+wpp*R*dGdA
              sumWRdGdB=sumWRdGdB+wpp*R*dGdB
              sumWGdRd2t=sumWGdRd2t+wpp*gs*dRdT
              sumWGdRdsig=sumWGdRdsig+wpp*gs*dRdS
              sumWGdRdgam=sumWGdRdgam+wpp*gs*dRdG
            endif
          enddo
          dpom1=dpi*.5d0-QHester(einfl,peak)
          dpom2=QHester(einfl,peak)-QHester(emin,peak)
          dpom3=THester(einfl)-THester(emin)
!          sumWG=min(H,S)/H*dpom1+(H+S)/(2.*H)*dpom2-dpom3/(2.*H)
          if(S.le.H) then
            sumWdGdA=(dpom2*.5-sumWG)/H
            sumWdGdB=(pi-QHester(einfl,peak)-QHester(emin,peak))*.5/H
          else
            sumWdGdA=.5/H**2*(dpom3-S*dpom2)
            sumWdGdB=dpom2/(2.*H)
          endif
          if(sumWG.ne.0.) then
            prfl=sumWRG/sumWG
            if(CalcDer) then
              pom1=(-prfl*sumWdGdA+sumWRdGdA)/sumWG
              pom2=(-prfl*sumWdGdB+sumWRdGdB)/sumWG
              if(KUseHS(KDatBlock).eq.1) then
                dpdas(1)=pom1
                dpdas(2)=pom2
              else
                dpdas(1)=pom1+pom2
                dpdas(2)=pom1-pom2
              endif
              sigpart=sumWGdRdsig/sumWG
              gampart=sumWGdRdgam/sumWG
              dprdt=SumWGdRd2T/sumWG
            endif
          else
            call pvoigt(dtt,R,dRdT,dRdS,dRdG)
            prfl=R
            if(CalcDer) then
              sigpart=dRdS
              gampart=dRdG
              dfdt=dRdT
              dprdt=dfdt
              dpdas(1)=0.
              dpdas(2)=0.
            endif
          endif
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          ip=nint((rdeg(1)-peak-dtt)/RadStepPwd)
          ik=nint((rdeg(2)-peak-dtt)/RadStepPwd)
          ip=max(-25,ip)
          ik=min( 25,ik)
          pomR=0.
          pomdRdT=0.
          pomdRdS=0.
          pomdRdG=0.
          pomW=0.
          dt=dtt
          call SetRealArrayTo(dpdas,7,0.)
          call SetRealArrayTo(dpomWdAxDifProf,7,0.)
          do i=ip,ik
            dt=dtt-float(i)*RadStepPwd
            pom=AxDivProf(i)
            if(pom.le.0.) cycle
            call pvoigt(dt,R,dRdT,dRdS,dRdG)
            pomW   =pomW   +pom
            pomR   =pomR   +pom*R
            if(CalcDer) then
              pomdRdT=pomdRdT+pom*dRdT
              pomdRdS=pomdRdS+pom*dRdS
              pomdRdG=pomdRdG+pom*dRdG
              do j=1,7
                dpdas(j)=dpdas(j)+R*DerAxDivProf(i,j)
                dpomWdAxDifProf(j)=dpomWdAxDifProf(j)+DerAxDivProf(i,j)
              enddo
            endif
          enddo
          if(pomW.gt.0.) then
            pomW=1./pomW
            prfl   =pomR   *pomW
            if(CalcDer) then
              dprdt  =pomdRdT*pomW
              sigpart=pomdRdS*pomW
              gampart=pomdRdG*pomW
              do j=1,7
                dpdas(j)=dpdas(j)*pomW-pomR*dpomWdAxDifProf(j)*pomW**2
              enddo
            endif
          endif
        endif
        if(KAsym(KDatBlock).ne.IdPwdAsymDivergence.and.
     1     KAsym(KDatBlock).ne.IdPwdAsymFundamental) then
          if(CalcDer) then
            sigpart=fnorm*sigpart
            gampart=fnorm*gampart
            dprdt=fnorm*dprdt
            if(Kasym(KDatBlock).le.IdPwdAsymSimpson) then
              dprdc7=fnorm*dprdc7
            else if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
              do i=1,NAsym(KDatBlock)
                dpdas(i)=dpdas(i)*prfl
              enddo
            endif
          endif
          prfl=fnorm*prfl
        endif
      endif
9999  return
      end
