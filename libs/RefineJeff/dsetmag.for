      subroutine DSetMag
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension DerP(3)
      n=0
      do i=1,NAtCalc
        if(KUsePolar(i).le.0) cycle
        n=n+1
        m=0
        mip=PrvniKiAtomu(i)+DelkaKiAtomu(i)-3-6*(MagPar(i)-1)
        do j=1,MagPar(i)
          if(j.eq.1) then
            m=m+1
            call CopyVek(Der(mip),DerP,3)
            call MultM(DerP,RMagDer(1,m,n),Der(mip),1,3,3)
            mip=mip+3
          else
            m=m+1
            call CopyVek(Der(mip),DerP,3)
            call MultM(DerP,RMagDer(1,m,n),Der(mip),1,3,3)
            mip=mip+3
            m=m+1
            call CopyVek(Der(mip),DerP,3)
            call MultM(DerP,RMagDer(1,m,n),Der(mip),1,3,3)
            mip=mip+3
          endif
        enddo
      enddo
      return
      end
