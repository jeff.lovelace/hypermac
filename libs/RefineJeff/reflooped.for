      subroutine RefLoopED(RefineEnd,lst0,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ihp(6),hh(3),mmax(3),Ro(3,3),RoI(3,3),Ra(3,3),Rb(3,3),
     1          Rf(3,3),RfI(3,3),Rt(3,3),UZone(3,3),UFinal(3,3),Rq(3,3),
     2          Roab(3,3),GCart(3),RNorm(3),xp(6),Rp(3,3)
      character*256 Veta
      character*80  t80
      character*1 nspec,kspec,FlagObs
      logical RefineEnd,EqIgCase,EqIV,FeSystemCommandFinished,lpom,
     1        ReflFulfilsCond
      integer CrlCentroSymm,CentroSymm,UseTabsIn
      real :: Iobs,Lam,Kn,ERest=.5110123156e6,F2V=47.877630914194
      if(ActionED.ne.1) then
        if(KDatBlock.ne.ActionDatBlock) go to 9999
        LnDynOut=NextLogicNumber()
        Veta=fln(:ifln)//'.edout_all'
        call OpenFile(LnDynOut,Veta,'formatted','unknown')
      endif
      allocate(dcp(LastNp))
      Veta='000000'
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(FeTxLength(Veta),UseTabs,IdLeftTab,' ')
      call FeTabsAdd(FeTxLengthSpace(Veta(:6)//' '),UseTabs,IdRightTab,
     1               ' ')
      CentroSymm=CrlCentroSymm()
      dcp=0.
      LnDyn=NextLogicNumber()
      AR=0.97845e-6
      BR=1.
      CR=-(12.2643/LamAve(KDatBlock))**2
      E0ED=(-BR+sqrt(BR**2-4.*AR*CR))/(2.*AR)
      GammaED=1.+E0ED/ERest
      U0=F2V*F000(KPhase,1)*6.648405113324489e-3*GammaED/
     1   CellVol(1,KPhase)
      Vlna=sqrt(1./LamAve(KDatBlock)**2+U0)
      pom=0.
      n=0
      do i=1,NAtCalc
        if(itf(i).eq.1) then
          pomo=beta(1,i)/episq
        else if(itf(i).ge.2) then
          call boueq(beta(1,i),sbeta(1,i),lite(KPhase),pomo,spomo,
     1               iswa(i))
        endif
        if(pomo.gt.0.) then
          pom=pom+pomo
          n=n+1
        endif
      enddo
      if(n.gt.0) then
        UisoED=max(pom/float(n),.01)
      else
        UisoED=.01
      endif
      ich=0
      dcp=0.
      ACalcED=0.
      BCalcED=0.
      FCalcED=-1.
      IObsED=0.
      SIObsED=0.
      do ir=1,NRefED
        if(SinThLED(ir).gt.GMaxEDZone(KDatBlock)*.5) then
          NRefEDBasic=ir
          exit
        endif
      enddo
      if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     2  'begin'
      itstder=0
      if(isimul.le.0) then
        call CloseIfOpened(91)
        call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9100
      endif
      NZone=0
1100  NZone=NZone+1
      if(NZone.gt.NEDZone(KDatBlock)) then
        if(ActionED.ne.1) then
          call CloseIfOpened(LnDynOut)
          call CloseIfOpened(91)
          call CloseIfOpened(83)
          ich=1
          go to 9999
        else
          go to 4000
        endif
      endif
      if(.not.UseEDZone(NZone,KDatBlock)) then
        call NastavM90(91)
1150    read(91,FormA,end=1100) Veta
        if(Veta.eq.' ') then
          go to 1150
        else
          k=0
          call kus(Veta,k,Cislo)
        endif
        if(EqIgCase(Cislo,'data')) go to 1100
        read(Veta,Format91,err=9000)(IHRead(i),i=1,maxNDim),Iobs,
     1                             SigIobs,iq,nxx
        if(iq.eq.NZone) then
          if(nFlowChart.ne.0) then
            call FeFlowChartEvent(noread,ie)
            if(ie.ne.0) call EventRefine(ie,RefineEnd,*9000)
          else
            noread=noread+1
          endif
        endif
        go to 1150
      endif
      NdOffPom=ndoffED+MxEDRef*(NZone-1)+
     1                 MxEDRef*NMaxEDZone*(KDatBlock-1)
      scp=ScEDZone(NZone,KDatBlock)
      FlagED=0
      call OpenFile(LnDyn,fln(:ifln)//'.eldyn','formatted','unknown')
      call NastavM90(91)
1200  read(91,FormA,end=1250) Veta
      if(Veta.eq.' ') then
        go to 1200
      else
        k=0
        call kus(Veta,k,Cislo)
      endif
      if(EqIgCase(Cislo,'data')) go to 1250
      read(Veta,Format91,err=9000)(IHRead(i),i=1,maxNDim),Iobs,
     1                             SigIobs,iq,nxx
      if(iq.ne.NZone) go to 1200
      if(ActionED.ne.1) then
        if(nFlowChart.ne.0) then
          call FeFlowChartEvent(noread,ie)
          if(ie.ne.0) call EventRefine(ie,RefineEnd,*9000)
        else
          noread=noread+1
        endif
      endif
      call FromIndSinThL(ihread,hh,SinThL,SinThLQ,1,0)
1210  ip=1
      ik=NRefEDBasic
1220  if(ik.le.ip+1) go to 1230
      ipul=(ik+ip)/2
      if(SinThL.lt.SinThLED(ipul)) then
        ik=ipul
        go to 1220
      else if(SinThL.gt.SinThLED(ipul)) then
        ip=ipul
        go to 1220
      else
        ip=ipul
        ik=ipul
        go to 1230
      endif
1230  ipp=ip
      do i=ip,1,-1
        if(SinThL-.001.lt.SinThLED(i)) then
          ipp=i
        else
          exit
        endif
      enddo
      ip=ipp
      ipp=ik
      do i=ik,NRefEDBasic
        if(SinThL+.001.gt.SinThLED(i)) then
          ipp=i
        else
          exit
        endif
      enddo
      ik=ipp
      do i=ip,ik
        if(EqIV(IHED(1,i),IHRead,NDim(KPhase))) then
          FlagED(i)=5
          if(SinThL.lt.snlmn.or.SinThL.gt.snlmx) then
            FlagED(i)=1
            go to 1240
          endif
          if(FlagED(i).eq.5) then
            do ii=1,nxxn
              if(nxx.eq.nxxp(ii)) then
                FlagED(i)=1
                go to 1240
              endif
            enddo
          endif
          do j=1,nskrt
            lpom=ReflFulfilsCond(ihread,ihm(1,j),ihma(1,j),
     1                           ihsn(1,j),imdn(j),ieqn(j),
     2                           ihsv(1,j),imdv(j),ieqv(j))
            if(lpom.eqv.DontUse(i)) then
              FlagED(i)=1
              go to 1240
            endif
          enddo
          if(kim.ge.-1.and.NDimI(KPhase).gt.0) then
            mmm=999999
            do ii=1,NComp(KPhase)
              mm=0
              if(ii.eq.1) then
                call CopyVekI(ihread,ihp,NDim(KPhase))
              else
                call MultMIRI(ihread,zvi(1,ii,KPhase),ihp,1,
     1                        NDim(KPhase),NDim(KPhase))
              endif
              do j=4,NDim(KPhase)
                mm=mm+iabs(ihp(j))
                mmax(j-3)=max(mmax(j-3),ihp(j))
              enddo
              mmm=min(mm,mmm)
            enddo
            if((kim.eq.-1.and.mmm.eq.0).or.
     1         (kim.ge.0.and.mmm.ne.kim)) then
              FlagED(i)=1
              go to 1240
            endif
          endif
1240      if(Iobs.le.slevel*SigIobs.and.useunobs.ne.1) FlagED(i)=1
          if(ifsq.ne.1) then
            if(Iobs.gt.0.) then
              Iobs=sqrt(Iobs)
            else
              Iobs=0.
            endif
            if(Iobs.le..01*SigIobs) then
              SigIobs=sqrt(SigIobs)/(.2)
            else
              SigIobs=.5/Iobs*SigIobs
            endif
            SigIobs=sqrt(SigIobs**2+(blkoef*Iobs)**2)
          else
            SigIobs=sqrt(SigIobs**2+(2.*blkoef*Iobs)**2)
          endif
          IObsED(i)=IObs
          SIObsED(i)=SigIObs
          go to 1200
        endif
      enddo
      go to 1200
1250  if(isimul.le.0) then
        rewind 91
        call NastavM90(91)
        call SetRotMatAboutAxis( OmEDZone(KDatBlock),3,Ro)
        call SetRotMatAboutAxis(-OmEDZone(KDatBlock),3,RoI)
        call SetRotMatAboutAxis(AlphaEDZone(NZone,KDatBlock),1,Ra)
        call SetRotMatAboutAxis(-BetaEDZone(NZone,KDatBlock),2,Rb)
        call MultM(Ro,Rb ,Rp  ,3,3,3)
        call MultM(Rp,Ra ,Rq  ,3,3,3)
        call MultM(Rq,RoI,Roab,3,3,3)
        call MultM(Roab,OrMatEDZone(1,1,KDatBlock),UZone,3,3,3)
        call SetRotMatAboutAxis(  PhiEDZone(NZone,KDatBlock),3,Rf)
        call SetRotMatAboutAxis( -PhiEDZone(NZone,KDatBlock),3,RfI)
        call SetRotMatAboutAxis( ThetaEDZone(NZone,KDatBlock),2,Rt)
        call Multm(Rf,Rt ,Rp,3,3,3)
        call Multm(Rp,RfI,Rq,3,3,3)
        call MultM(Rq,UZone,UFinal,3,3,3)
        RNorm(1)=XNormEDZone(NZone,KDatBlock)
        RNorm(2)=YNormEDZone(NZone,KDatBlock)
        pom=RNorm(1)**2+RNorm(2)**2
        if(pom.lt.1.) then
          RNorm(3)=sqrt(1.-pom)
        else
          RNorm(3)=0.
        endif
      else
        rewind 91
      endif
      do ir=1,NRefEDBasic
        call MultMRIR(UFinal,IHED(1,ir),GCart,3,3,1)
        pom=4.*SinThLED(ir)*sin(PrAngEDZone(NZone,KDatBlock)*.5*ToRad)
        ExcitErrED(ir)=(Vlna**2-(gcart(1)**2+gcart(2)**2+
     1                  (gcart(3)-Vlna)**2))/(2.*Vlna)
        if(ExcitErrED(ir)-pom.le. SGMaxMEDZone(KDatBlock).and.
     1     ExcitErrED(ir)+pom.ge.-SGMaxMEDZone(KDatBlock)) then
          if(FlagED(ir).eq.0) FlagED(ir)=2
        else
          if(FlagED(ir).eq.1.or.FlagED(ir).eq.5) FlagED(ir)=0
        endif
        if(FlagED(ir).eq.5) then
          if(abs(ExcitErrED(ir)).gt.SGMaxREDZone(KDatBlock).or.
     1       (pom.gt.0..and.abs(ExcitErrED(ir)).gt.
     2        CSGMaxREDZone(KDatBlock)*pom)) FlagED(ir)=1
        endif
      enddo
      do ir1=1,NRefEdBasic-1
        if(FlagED(ir1).ne.1.and.FlagED(ir1).ne.2.and.FlagED(ir1).ne.5)
     1    cycle
        do ir2=ir1+1,NRefEdBasic
          if(FlagED(ir2).ne.1.and.FlagED(ir2).ne.2.and.FlagED(ir2).ne.5)
     1      cycle
          do i=1,NDim(KPhase)
            ihp(i)=IHED(i,ir1)-IHED(i,ir2)
          enddo
          call FromIndSinThL(ihp,hh,SinThL,SinThLQ,1,0)
          ip=1
          ik=NRefED
1320      if(ik.le.ip+1) go to 1340
          ipul=(ik+ip)/2
          if(SinThL.lt.SinThLED(ipul)) then
            ik=ipul
            go to 1320
          else if(SinThL.gt.SinThLED(ipul)) then
            ip=ipul
            go to 1320
          else
            ip=ipul
            ik=ipul
            go to 1340
          endif
1340      ipp=ip
          do i=ip,1,-1
            if(SinThL-.001.lt.SinThLED(i)) then
              ipp=i
            else
              exit
            endif
          enddo
          ip=ipp
          ipp=ik
          do i=ik,NRefED
            if(SinThL+.001.gt.SinThLED(i)) then
              ipp=i
            else
              exit
            endif
          enddo
          ik=ipp
          do i=ip,ik
            if(EqIV(IHED(1,i),ihp,NDim(KPhase))) then
              if(FlagED(i).eq.0) FlagED(i)=6
              exit
            endif
          enddo
        enddo
      enddo
      iq=1
      Veta=Tabulator//'     0'//Tabulator//'reflections already '//
     1                'precalculated'
      call FeTxOut(-1.,210.,Veta)
      NCompr=-1
      dcp=0
      dc =0
      do ir=1,NRefED
        if(mod(ir,1000).eq.0) then
          write(Veta(2:7),105) ir
          call FeTxOutCont(Veta)
        endif
        if(FlagED(ir).eq.0) cycle
        if(FCalcED(ir).lt.0.) then
          call CopyVekI(IHED(1,ir),IHRef(1,1),maxNDim)
          SinThL=SinThLED(ir)
          SinThLQ=SinThL**2
          mmabsm=-999
          nic=.false.
          nicova=.false.
          if(calcder) call SetRealArrayTo(der,ndoff,0.)
          call RefLoopBefore(0,1,ich)
          if(ich.eq.1) then
            ich=0
            cycle
          else if(ich.eq.2) then
            go to 9000
          endif
          extkor=1.
          extkorm=1.
          call calc(1)
          if(ErrFlag.eq.2) then
            ErrFlag=0
            go to 9300
          else if(ErrFlag.ne.0) then
            ErrFlag=0
            go to 9000
          endif
          call RefInfoKolaps(lst0,*9000)
          FCalcED(ir)=FCalc
          if(calcder) then
            der(1:LastNp)=dcp(1:LastNp)
            call DSetAll
            dcp(1:LastNp)=der(1:LastNp)
            der(1:LastNp)=dc(1:LastNp)
            call DSetAll
            dc(1:LastNp)=der(1:LastNp)
          endif
          call RefDerCompress(dcp,ADerED(1,ir),NCompr1,NCompr2,
     1                        NCompr3,1.)
          call RefDerCompress(dc ,BDerED(1,ir),NCompr1,NCompr2,
     1                        NCompr3,1.)
          ACalcED(ir)=act(1,1)
          BCalcED(ir)=bct(1,1)
        endif
        if(NCompr.lt.0) then
          NCompr=NCompr1+NCompr3-NCompr2
          if(ActionED.eq.1) then
            t80='int'
          else if(ActionED.eq.2) then
            t80='scale'
          else if(ActionED.eq.3) then
            t80='orient'
          else if(ActionED.eq.4) then
            t80='thick'
          else
            t80='thickorient'
          endif
          if(EDTiltCorr(KDatBlock).ne.1)
     1      t80=t80(:idel(t80))//' notiltcorr'
          write(Cislo,FormI15) EDThreads(KDatBlock)
          call Zhusti(Cislo)
          t80=t80(:idel(t80))//' thr '//Cislo(:idel(Cislo))
          write(LnDyn,FormA) t80(:idel(t80))
          write(LnDyn,FormA) EDCommands(:idel(EDCommands))
          write(LnDyn,'(''Zone#'',i3)') NZone
          if(CentroSymm.gt.0) then
            write(LnDyn,'(''Centrosymmetric'',i10)') NCompr
          else
            write(LnDyn,'(''Noncentrosymmetric'',i10)') NCompr
          endif
          if(ifsq.ne.1) then
            write(LnDyn,'(''Refinement F'')')
          else
            write(LnDyn,'(''Refinement I'')')
          endif
          do i=1,3
            write(LnDyn,101)(OrMatEDZone(i,j,KDatBlock),j=1,3)
          enddo
          write(LnDyn,104) LamAve(KDatBlock),F000(KPhase,1),
     1                     OmEDZone(KDatBlock),GMaxEDZone(KDatBlock),
     2                     SGMaxMEDZone(KDatBlock),
     3                     SGMaxREDZone(KDatBlock),
     4                     CSGMaxREDZone(KDatBlock),
     5                     EDIntSteps(KDatBlock)
          call CopyVek(HEDZone(1,NZone,KDatBlock),xp,3)
          xp(4)=AlphaEDZone(NZone,KDatBlock)
          xp(5)=BetaEDZone(NZone,KDatBlock)
          xp(6)=PrAngEDZone(NZone,KDatBlock)
          call iosts(LnDyn,xp,6,1)
          xp(1)=ScEDZone(NZone,KDatBlock)
          xp(2)=ThickEDZone(NZone,KDatBlock)
          xp(3)=XNormEDZone(NZone,KDatBlock)
          xp(4)=YNormEDZone(NZone,KDatBlock)
          call iost(LnDyn,xp,KiED(1,NZone,KDatBlock),4,1,0)
          xp(1)=PhiEDZone(NZone,KDatBlock)
          xp(2)=ThetaEDZone(NZone,KDatBlock)
          call iost(LnDyn,xp,KiED(5,NZone,KDatBlock),2,1,0)
        endif
        write(LnDyn,102)(IHED(i,ir),i=1,3),IObsED(ir),SIObsED(ir),
     1                   ACalcED(ir),BCalcED(ir),FlagED(ir),
     2                   ExcitErrED(ir)
        write(LnDyn,103)(ADerED(i,ir),i=1,NCompr1),
     1                  (ADerED(i,ir),i=NCompr2+1,NCompr3)
        write(LnDyn,103)(BDerED(i,ir),i=1,NCompr1),
     1                  (BDerED(i,ir),i=NCompr2+1,NCompr3)
      enddo
      write(Veta(2:7),105) NRefED
      call FeTxOutCont(Veta)
      call FeReleaseOutput
      call FeDeferOutput
      call FeTxOutEnd
      call CloseIfOpened(LnDyn)
      Veta='"'//HomeDir(:idel(HomeDir))//'Dyngo'//ObrLom//
     1     'Dyngo.exe" "'//fln(:ifln)//'.eldyn"'
      call FeSystemCommand(Veta,1)
1400  if(.not.FeSystemCommandFinished()) then
        call FeWait(.1)
        if(nFlowChart.ne.0) then
          call FeFlowChartEvent(noread,ie)
          if(ie.ne.0) call EventRefine(ie,RefineEnd,*9000)
        endif
        noread=noread-1
        go to 1400
      endif
      Veta=fln(:ifln)//'.edout'
      if(ActionED.ne.1) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
1450    read(ln,FormA,end=1460) t80
        write(LnDynOut,FormA) t80(:idel(t80))
        go to 1450
1460    call CloseIfOpened(ln)
        write(LnDynOut,'(36(''=''))')
        go to 1100
      endif
      call OpenFile(LnDyn,Veta,'formatted','unknown')
      read(LnDyn,FormA,end=9000) Veta
      k=0
      call Kus(Veta,k,Cislo)
      call StToReal(Veta,k,RFacEDZone(NZone,KDatBlock),1,.false.,ich)
      IDerED=0.
      do ir=1,NRefED
        if(FlagED(ir).ne.5) cycle
        read(LnDyn,102)(IHED(i,ir),i=1,3),FCalcED(ir)
        FCalcED(ir)=sqrt(FCalcED(ir))
        read(LnDyn,103)(dc(i),i=1,MxEDRef)
        read(LnDyn,103)(IDerED(i,ir),i=1,NCompr1),
     1                 (IDerED(i,ir),i=NCompr2+1,NCompr3)
        call RefDerUncompress(IDerED(1,ir),der,1.)
        do i=1,MxEDRef
          der(i+NdOffPom)=dc(i)
        enddo
        call DSetElDyn
        if(ifsq.le.0) then
          if(FCalcED(ir).eq.0.) then
            pom=0.
          else
            pom=.5/FCalcED(ir)
          endif
        else
          pom=1.
        endif
        call RefDerCompress(der,IDerED(1,ir),NCompr1,NCompr2,NCompr3,
     1                      pom)
      enddo
      call CloseIfOpened(LnDyn)
      call NastavM90(91)
1500  read(91,FormA,end=1100) Veta
      if(Veta.eq.' ') then
        go to 1500
      else
        k=0
        call kus(Veta,k,Cislo)
      endif
      if(EqIgCase(Cislo,'data')) go to 1100
      read(Veta,Format91,err=9000)(IHRead(i),i=1,maxNDim),Iobs,
     1                             SigIobs,iq,nxx
      if(iq.ne.NZone) go to 1500
      itwr=1
      iatwr=1
      iaov=1
      iovmax=1
      iov=1
      if(SigIobs.le.0.) SigIobs=.1
      KPhase=KPhaseTwin(iatwr)
      mmax=0
      call FromIndSinThL(ihread,hh,SinThL,SinThLQ,1,0)
      if(isimul.le.0) then
        nulova=Iobs.le.slevel*SigIobs
        if(Iobs.gt.0.) then
          Fobs=sqrt(Iobs)
        else
          Fobs=0.
        endif
        if(Iobs.le..01*SigIobs) then
          SigFobs=sqrt(SigIobs)/(.2)
        else
          SigFobs=.5/Fobs*SigIobs
        endif
        SigFobs=sqrt(SigFobs**2+(blkoef*Fobs)**2)
        SigIobs=sqrt(SigIobs**2+(2.*blkoef*Iobs)**2)
        if(ifsq.ne.1) then
          sigyo=SigFobs
        else
          sigyo=SigIobs
        endif
      else
        nulova=.false.
        sigyo=1.
        if(ifsq.ne.1) then
          SigFobs=0.
        else
          SigIobs=0.
        endif
      endif
      if(nFlowChart.ne.0) then
        call FeFlowChartEvent(noread,ie)
        if(ie.ne.0) call EventRefine(ie,RefineEnd,*9000)
      else
        noread=noread+1
      endif
      if(ipisd.eq.0.and.DatBlockDerTest.le.0) then
        if(nulova.and.useunobs.ne.1) go to 1500
        if(SinThL.lt.snlmn.or.SinThL.gt.snlmx) go to 1500
        do i=1,nxxn
          if(nxx.eq.nxxp(i)) go to 1500
        enddo
        do i=1,nskrt
          lpom=ReflFulfilsCond(ihread,ihm(1,i),ihma(1,i),
     1                         ihsn(1,i),imdn(i),ieqn(i),
     2                         ihsv(1,i),imdv(i),ieqv(i))
          if(lpom.eqv.DontUse(i)) go to 1500
        enddo
        if(kim.ge.-1.and.NDimI(KPhase).gt.0) then
          mmm=999999
          do i=1,NComp(KPhase)
            mm=0
            if(i.eq.1) then
              call CopyVekI(ihread,ihp,NDim(KPhase))
            else
              call MultMIRI(ihread,zvi(1,i,KPhase),ihp,1,NDim(KPhase),
     1                      NDim(KPhase))
            endif
            do j=4,NDim(KPhase)
              mm=mm+iabs(ihp(j))
              mmax(j-3)=max(mmax(j-3),ihp(j))
            enddo
            mmm=min(mm,mmm)
          enddo
          if((kim.eq.-1.and.mmm.eq.0).or.
     1       (kim.ge.0.and.mmm.ne.kim)) go to 1500
        endif
      endif
      if(nulova) then
        nspec='*'
      else
        nspec=' '
      endif
      nic=.false.
2020  extkor=1.
      extkorm=1.
2100  ip=1
      ik=NRefED
2150  if(ik.le.ip+1) go to 2200
      ipul=(ik+ip)/2
      if(SinThL.lt.SinThLED(ipul)) then
        ik=ipul
        go to 2150
      else if(SinThL.gt.SinThLED(ipul)) then
        ip=ipul
        go to 2150
      else
        ip=ipul
        ik=ipul
        go to 2200
      endif
2200  ipp=ip
      do i=ip,1,-1
        if(SinThL-.001.lt.SinThLED(i)) then
          ipp=i
        else
          exit
        endif
      enddo
      ip=ipp
      ipp=ik
      do i=ik,NRefED
        if(SinThL+.001.gt.SinThLED(i)) then
          ipp=i
        else
          exit
        endif
      enddo
      ik=ipp
      do i=ip,ik
        if(EqIV(IHED(1,i),IHRead,NDim(KPhase))) then
          if(FlagED(i).ne.5) go to 1500
          FCalc=FCalcED(i)
          pom=FCalc/sqrt(ACalcED(i)**2+BCalcED(i)**2)
          AFour(1,1)=ACalcED(i)*pom
          BFour(1,1)=BCalcED(i)*pom
!          act(1,1)=ACalcED(i)
!          bct(1,1)=BCalcED(i)
!          yct(1,1)=FCalc
          call RefDerUncompress(IDerED(1,i),der,1.)
          go to 2300
        endif
      enddo
      go to 1500
2300  yofour1=Fobs
      yofour2=Fobs
      ysfour1=SigFobs
      ysfour2=SigFobs
      ycpom=FCalc
      if(calcder) then
        IZdvih=(KDatBlock-1)*MxScAll
        pomp=der(NdOffPom+1)
        der(NdOffPom+1)=2.*der(NdOffPom+1)/scp
        if(ifsq.eq.1) then
          der(MxSc+1+IZdvih)=-SinThLQ*episq*2.*Fcalc**2
        else
          der(MxSc+1+IZdvih)=-SinThLQ*episq*Fcalc
        endif
      endif
      if(.not.CalcDer) then
        write(80,format80)(IHRead(i),i=1,maxNDim),KPhase,
     1    yofour1,yofour2,ycpom,afour(1,1),bfour(1,1),affree,
     2    bffree,afst,bfst,affreest,bffreest,ysfour1,ysfour2
      endif
      if(Nulova) then
        FlagObs='<'
      else
        FlagObs='o'
      endif
      if(DatBlockDerTest.gt.0) then
        call DerTest(ihread,Fobs,Fcalc,itstder)
        if(itstder.lt.0) then
          go to 9999
        else
          go to 2020
        endif
      endif
      dyp=Fobs-Fcalc
      dy=dyp
      if(iwq.eq.0) then
        wt=1./sigyo**2
      else if(iwq.eq.1) then
        wt=1.
      else
        wt=1./(2.*yomin+Fobs+2.*Fobs**2/yomax)
        if(ifsq.eq.1) then
          if(Iobs.gt.0.) then
            wt=wt/(4.*Iobs)
          else
            wt=0.
          endif
        endif
      endif
      if(wtsnthl.eq.1) wt=wt*exp(-SinThL**2)
      if(ifsq.eq.1) then
        dy=Iobs-Fcalc**2
        if(nulova) then
          sigyo=sqrt(.5*sigyo)
        else
          sigyo=.5/Fobs*sigyo
        endif
      endif
      wdy=sqrt(wt)*dy
      wdyq=wdy**2
      if(ifsq.ne.1) then
        wyoq=wt*Fobs**2
      else
        wyoq=wt*Iobs**2
      endif
      if(abs(wdy).gt.vyh) then
        kspec='#'
        if(iskip.eq.1) then
          nzz=nzz-1
          if(.not.CalcDer) backspace 80
          NSkipRef(ICyklMod6,KDatBlock)=NSkipRef(ICyklMod6,KDatBlock)+1
          go to 3150
        endif
      else
        kspec=' '
      endif
      wRNumAll(1)=wRNumAll(1)+wdyq
      wRDenAll(1)=wRDenAll(1)+wyoq
       RNumOverall=RNumOverall+abs(dyp)
       RDenOverall=RDenOverall+abs(Fobs)
      wRNumOverall=wRNumOverall+wdyq
      wRDenOverall=wRDenOverall+wyoq
      nRAll(1)=nRAll(1)+1
      nRFacOverall=nRFacOverall+1
      if(.not.nulova) then
        wRNumObs(1)=wRNumObs(1)+wdyq
        wRDenObs(1)=wRDenObs(1)+wyoq
        nRObs(1)=nRObs(1)+1
        nRFacOverallObs=nRFacOverallObs+1
         RNumOverallObs=RNumOverallObs+abs(dyp)
         RDenOverallObs=RDenOverallObs+abs(Fobs)
        wRNumOverallObs=wRNumOverallObs+wdyq
        wRDenOverallObs=wRDenOverallObs+wyoq
      endif
      do i=1,maxNDim
        ihmax(i)=max(ihmax(i),ihread(i))
        ihmin(i)=min(ihmin(i),ihread(i))
      enddo
      if(ipisd.ne.0) then
        call PisDer(88,ihread,sigyo,wdy,noread,nspec,kspec)
        if(ipisd.gt.0) go to 9200
      endif
      if(calcder) then
        call SumaRefine
      else
         pom=1./(scp*ExtKor)**2
         FObsQ =IObs    *pom
         FCalcQ=Fcalc**2*pom
        sFCalcQ=SigIobs**2-(2.*blkoef*Iobs)**2
        if(sFCalcQ.gt.0.) then
          sFCalcQ=sqrt(SigIobs**2-(2.*blkoef*Iobs)**2)*pom
        else
          sFCalcQ=SigIobs*pom
        endif
        write(83,format83e)(ihread(i),i=1,MaxNDim),FCalcQ,FObsQ,
     1                      sFCalcQ,FlagObs,itwr,wdy,Fobs,Fcalc,
     2                      SigFObs,pom
      endif
3150  if(ExistMagnetic) then
        write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,
     1    AFour(1,1),BFour(1,1),dyp,sigyo,wt,wdy,noread,nspec,kspec,
     2    SinThL,extkor,extkorm,iq,itwr,1
      else
        write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,
     1    AFour(1,1),BFour(1,1),dyp,sigyo,wt,wdy,noread,nspec,kspec,
     2    SinThL,extkor,iq,itwr,1
      endif
      go to 1500
4000  if(nFlowChart.gt.0) call FeFlowChartEvent(noread,ie)
      if(isimul.gt.0) go to 9999
      if(OrthoOrd.gt.0) call trortho(1)
      GOFAll(ICyklMod6,KDatBlock)=wRNumAll(1)
      nPar=nParData(KDatBlock)
      do i=1,NPhase
        nPar=nPar+NParStr(i)
      enddo
      if(nRAll(1)-nPar.gt.1) then
        GOFAll(ICyklMod6,KDatBlock)=
     1    sqrt(GOFAll(ICyklMod6,KDatBlock)/float(nRAll(1)-nPar-1))
      else
        GOFAll(ICyklMod6,KDatBlock)=-0.01
      endif
      GOFObs(ICyklMod6,KDatBlock)=wRNumObs(1)
      if(nRObs(1)-nPar.gt.1) then
        GOFObs(ICyklMod6,KDatBlock)=
     1    sqrt(GOFObs(ICyklMod6,KDatBlock)/float(nRObs(1)-nPar-1))
      else
        GOFObs(ICyklMod6,KDatBlock)=-0.01
      endif
      if(NAtCalc.le.0.or.DoLeBail.ne.0) go to 9200
      if(useunobs.eq.1) then
        n=nRAll(1)
      else
        n=NRef90(KDatBlock)
      endif
      write(Cislo,FormI15) n
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(12,17),Cislo
      write(Cislo,FormI15) nRObs(1)
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(10,17),Cislo
      go to 9999
9000  ErrFlag=1
9100  ich=1
      if(OrthoOrd.gt.0) call trortho(1)
      go to 9900
9200  ich=2
      go to 9900
9300  ich=3
9900  call FeTxOutEnd
9999  if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
      if(allocated(dcp)) deallocate(dcp)
      call CloseIfOpened(LnDyn)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(a,1x,a)
101   format(5f12.6)
102   format(3i4,4e15.5,i5,e15.5)
103   format(10e15.6)
104   format(7f12.6,i5)
105   format(i6)
      end
