      subroutine RefLoopBefore(Klic,iov,ich)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension hh(6),hhp(6)
      integer ihp(6)
      logical :: UzPsal=.false.,PocitatMMAbsM,SystExtRef,EqIV,EqIVM
      character*80 t80
      ich=0
      PocitatMMAbsM=mod(iov,2).eq.1.and.(KPhase.eq.1.or.mmabsm.lt.-900)
      if(PocitatMMAbsM) then
        mmabsm=19
      else if(NDimI(KPhase).le.1) then
        mmabsm=mmabsm-2
      else if(NDimI(KPhase).gt.1) then
        mmabsm=NSatGroups+1
      endif
      okr(1)=.true.
      do i=2,NComp(KPhase)
        okr(i)=.true.
        do j=1,NDim(KPhase)
          hh(j)=ihref(j,1)
        enddo
        if(NDimI(KPhase).eq.1.and.KCommen(KPhase).ne.0) then
          kkk=0
        else
          kkk=1
        endif
2200    call multm(hh,zvi(1,i,KPhase),hhp,1,NDim(KPhase),NDim(KPhase))
        do j=1,NDim(KPhase)
          n=nint(hhp(j))
          if(abs(float(n)-hhp(j)).gt..0001) then
            if(kkk.ne.0) go to 2290
            if(hh(4).ge.0.) then
              zn=-1.
            else
              zn= 1.
            endif
            pom=zn*float(NCommQ(1,1,KPhase))
            hh(4)=hh(4)+pom
            do k=1,3
              hh(k)=hh(k)-anint(pom*qu(k,1,1,KPhase))
            enddo
            kkk=1
            go to 2200
          endif
          ihref(j,i)=n
        enddo
        cycle
2290    okr(i)=.false.
      enddo
      mmabs=0
      phfp=0.
      if(NDimI(KPhase).gt.0) then
        do i=1,NComp(KPhase)
          mmabs(i)=NSatGroups+1
          mm=0
          do j=1,NSymm(KPhase)
            call MultMIRI(ihref(1,i),Rm6(1,j,i,KPhase),ihp,1,
     1                    NDim(KPhase),NDim(KPhase))
            do m=0,NSatGroups
              if(EqIV (ihp(4:),HSatGroups(1,m),NDimI(KPhase)).or.
     1           EqIVM(ihp(4:),HSatGroups(1,m),NDimI(KPhase))) then
                mm=1
                mmabs(i)=min(mmabs(i),m)
              endif
            enddo
            if(mm.eq.0) mmabs(i)=min(mmabs(i),NSatGroups+1)
            if(PocitatMMAbsM) mmabsm=min(mmabsm,mmabs(i))
          enddo
          phfp(i)=-mmabs(i)*(mmabs(i)-1)
        enddo
      else if(MagneticRFac(KPhase).gt.0) then
        MagneticType(KPhase)=0
        if(SystExtRef(ihref(1,1),.false.,1)) mmabs(1)=1
        if(PocitatMMAbsM) mmabsm=mmabs(1)
        MagneticType(KPhase)=1
      else
        if(PocitatMMAbsM) mmabsm=mmabs(1)
      endif
      if(mod(iov,2).ne.1) go to 2360
      mic=0
      if(NComp(KPhase).gt.1) then
        n=0
        do i=1,NComp(KPhase)
          if(mmabs(i).eq.0) n=n+1
        enddo
        if(n.eq.0) go to 2360
        if(n.eq.NComp(KPhase)) then
          mic=4+3*(NComp(KPhase)-2)
          go to 2340
        endif
        do i=1,NComp(KPhase)
          if(n.eq.1) then
            if(mmabs(i).eq.0) go to 2340
          else
            if(mmabs(i).ne.0) go to 2340
          endif
        enddo
2340    if(n.eq.1) then
          mic=i
        else
          mic=7-i
        endif
2350    if(kic.gt.0.and.mic.ne.kic) go to 9000
      endif
2360  if(NDimI(KPhase).gt.0.and.((kim.eq.-1.and.mmabsm.eq.0).or.
     1                           (kim.ge.0.and.mmabsm.ne.kim)))
     2  go to 9000
      if(nic) nzz=nzz+1
      nic=.false.
      call SetInd(Klic,ihref)
      if(ErrFlag.ne.0) then
        if(.not.UzPsal) then
          write(t80,'(''('',6(i4,'',''))')(ihref(i,1),i=1,NDim(KPhase))
          call Zhusti(t80)
          i=idel(t80)
          if(t80(i:i).ne.',') i=i+1
          t80(i:i)=')'
          t80='direction cosines for '//t80(:i)
          if(ErrFlag.eq.1) then
            t80=t80(:idel(t80))//' are missing.'
          else
            t80=t80(:idel(t80))//' are unrealistic.'
          endif
          call FeChybne(-1.,-1.,'anisotropic extinction cannot be '//
     1                  'performed.',t80,SeriousError)
          UzPsal=.true.
        endif
        ErrFlag=0
        go to 9100
      endif
      call SetFormF(sinthl)
      mmabsm=mmabsm+2
      go to 9999
9000  ich=1
      go to 9999
9100  ich=2
9999  return
      end
