      subroutine RefFixedReadCommand(Command,ich)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/FixedQuest/ nEdwAtoms,nEdwSetTo,FixedType,AtomString,
     1                   ValueSetTo
      character*(*) Command
      character*256 AtomString
      character*80  t80
      dimension xp(1)
      integer FixedType
      save /FixedQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'fixed'.and.t80.ne.'!fixed') go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      call mala(t80)
      FixedType=islovo(t80,cfix,10)
      if(FixedType.gt.3) FixedType=FixedType-1
      if(k.ge.lenc) go to 8000
      if(FixedType.eq.IdFixSetTo) then
        call kus(Command,k,t80)
        kk=0
        call StToReal(t80,kk,xp(1),1,.false.,ich)
        ValueSetTo=xp(1)
        if(ich.ne.0) go to 8040
      endif
      if(FixedType.eq.IdFixOrigin.or.FixedType.eq.IdFixOriginx4) then
        call kus(Command,k,AtomString)
      else
        AtomString=Command(k+1:)
      endif
      if(FixedType.lt.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildYes,IdAtMolNo,IdMolNo,
     1                      IdSymmNo,IdAtMolMixNo,t80)
        if(t80.ne.' ') go to 8100
      else if(FixedType.eq.IdFixOrigin) then
        call TestAtomString(AtomString,IdWildNo,IdAtMolNo,IdMolYes,
     1                      IdSymmNo,IdAtMolMixNo,t80)
        if(t80.ne.' ') go to 8100
      else
        if(FixedType.eq.IdFixOriginX4) then
          j=0
        else
          j=1
        endif
        call TestParamString(AtomString,j,t80)
        if(t80.ne.' ') go to 8100
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      if(t80.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.'
     2               ,SeriousError)
9999  return
      end
