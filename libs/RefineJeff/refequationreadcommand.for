      subroutine RefEquationReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/EquationQuest/ nCrwAtomic,nCrwApplyToOrtho,nEdwAtoms,
     1  nEdwEquation, AtomicEquation,ApplyToOrthoWaves,AtomString,
     2  EquationString
      character*256 AtomString,EquationString
      character*80 t80
      character*(*) Command
      logical   AtomicEquation,ApplyToOrthoWaves
      save /EquationQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'equation'.and.t80.ne.'!equation') go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      call mala(t80)
      AtomicEquation=t80.eq.'for'
      if(AtomicEquation) then
        i=index(Command(k+1:),':')
        if(i.le.0) i=index(Command(k+1:),'%')
        if(i.le.0) i=index(Command(k+1:),'&')
        if(i.le.0) go to 8010
        i=k+i
        AtomString=Command(k+1:i-1)
        call TestAtomString(AtomString,IdWildYes,IdAtMolYes,IdMolYes,
     1                      IdSymmNo,IdAtMolMixNo,t80)
        if(t80.ne.' ') go to 8010
        k=i-1
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        call mala(t80)
      else
        AtomString=' '
      endif
      if(t80.eq.':') then
        ApplyToOrthoWaves=.true.
      else if(t80.eq.'%'.or.t80.eq.'&') then
        ApplyToOrthoWaves=.false.
      else
         go to 8010
      endif
      if(k.ge.lenc) go to 8000
      EquationString=Command(k+1:)
      call RefEquationCheck(EquationString,t80)
      if(t80.ne.' ') go to 8100
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  if(t80.eq.'Jiz oznameno') then
        ich=1
        go to 9999
      endif
      t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
