      subroutine modspec(rmd,p,n,nd,nk,sm,iw,iws,sn,cs,eps,ji)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmd(4*nd*nk,2*nd*nk),sm(nd,nd),p(4*nd*nk)
      nd2=2*nd
      jk=nd2*(nk-iw)
      do i=1,nd2
        do j=1,nd2*nk
          jp=j-jk
          if(i.eq.jp) then
            rmd(n+i,j)=-1.
          else
            rmd(n+i,j)= 0.
          endif
        enddo
      enddo
      lm=nd2*(nk-iws)+nd+1
      lv=lm+nd
      km=n+nd+1
      kv=km+nd
      do j=1,nd
        do i=1,nd
          rmd(km-i,lm-j)=rmd(km-i,lm-j)+sm(i,j)*cs
        enddo
      enddo
      pom=sn*eps
      do j=1,nd
        do i=1,nd
          rmd(km-i,lv-j)=rmd(km-i,lv-j)+sm(i,j)*pom
        enddo
      enddo
      do j=1,nd
        do i=1,nd
          rmd(kv-i,lm-j)=rmd(kv-i,lm-j)-sm(i,j)*sn
        enddo
      enddo
      pom=cs*eps
      do j=1,nd
        do i=1,nd
          rmd(kv-i,lv-j)=rmd(kv-i,lv-j)+sm(i,j)*pom
        enddo
      enddo
      call uprspec(rmd,p,nd2*nk,n,ji)
      return
      end
