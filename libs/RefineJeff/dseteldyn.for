      subroutine DSetElDyn
      use EDZones_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      if(NEDZone(KDatBlock).le.1.or.ActionED.gt.1) go to 9999
      kip=2+ndoffED+MxEDRef*NMaxEDZone*(KDatBlock-1)
      do j=1,NEDZone(KDatBlock)
        n=0
        ki2=kip
        do i=1,NEDZone(KDatBlock)
          if(NThickEDZone(i,KDatBlock).eq.j.and.
     1       UseEDZone(i,KDatBlock)) then
            n=n+1
            if(n.eq.1) then
              ki1=ki2
            else
              der(ki1)=der(ki1)+der(ki2)
            endif
          endif
          ki2=ki2+MxEDRef
        enddo
      enddo
9999  return
      end
