      function Ktera(prom)
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*18 PrvniPar
      character*(*) prom
      character*3  PrvniVlna,JmenoVlny(3)
      logical JeToMolekula,EqIgCase
      data PrvniPar/'axyzubtlscdefoprkm'/,PrvniVlna/'sco'/,
     1     JmenoVlny/'sin','cos','ort'/
      integer ZacinaA,ZacinaX,ZacinaY,ZacinaZ,ZacinaU,ZacinaB,ZacinaT,
     1        ZacinaL,ZacinaS,ZacinaC,ZacinaD,ZacinaE,ZacinaF,ZacinaO,
     2        ZacinaP,ZacinaR,ZacinaK,ZacinaM
      equivalence (IdNumbers( 1),ZacinaA),
     1            (IdNumbers( 2),ZacinaX),
     2            (IdNumbers( 3),ZacinaY),
     3            (IdNumbers( 4),ZacinaZ),
     4            (IdNumbers( 5),ZacinaU),
     5            (IdNumbers( 6),ZacinaB),
     6            (IdNumbers( 7),ZacinaT),
     7            (IdNumbers( 8),ZacinaL),
     8            (IdNumbers( 9),ZacinaS),
     9            (IdNumbers(10),ZacinaC),
     a            (IdNumbers(11),ZacinaD),
     1            (IdNumbers(12),ZacinaE),
     2            (IdNumbers(13),ZacinaF),
     3            (IdNumbers(14),ZacinaO),
     4            (IdNumbers(15),ZacinaP),
     5            (IdNumbers(16),ZacinaR),
     6            (IdNumbers(17),ZacinaK),
     7            (IdNumbers(18),ZacinaM)
      call mala(prom)
      do k=5,7
        if(prom.eq.lBasicPar(k)) then
          Ktera=k-3
          go to 9999
        endif
      enddo
      if(prom.eq.'biso'.or.prom.eq.'uiso') then
        ktera=5
        go to 9999
      else if(prom.eq.'delta') then
        Ktera=JeToDelta
        go to 9999
      else if(prom.eq.'x40') then
        Ktera=JeToX40
        go to 9999
      else if(prom.eq.'t40') then
        Ktera=JeToT40
        go to 9999
      else if(NXYZAMode.gt.0.or.NMagAMode.gt.0) then
        if(KtAt(LXYZAMode,NXYZAMode,prom).gt.0) then
          Ktera=JeToXYZMode
          go to 9999
        endif
      else if(NMagAMode.gt.0) then
        if(KtAt(LMagAMode,NMagAMode,prom).gt.0) then
          Ktera=JeToMagMode
          go to 9999
        endif
      endif
      i=index(PrvniPar,Prom(1:1))
      id=idel(prom)
      KwPrv=0
      JeToMolekula=.false.
      if(i.le.0) then
        go to 9000
      else if(i.eq.ZacinaA) then
        if(prom.eq.'ai'.or.prom.eq.'aimol') then
          Ktera=1
          JeToMolekula=.true.
        else
          go to 9000
        endif
      else if(i.eq.ZacinaX.or.i.eq.ZacinaY.or.i.eq.ZacinaZ) then
        if(id.eq.1) then
          Ktera=i
        else if(prom(2:id).eq.'trans') then
          Ktera=i+3
          JeToMolekula=.true.
        else if(LocateSubstring(prom,'-slope',.false.,.true.).eq.2) then
          if(i.eq.2) then
            Ktera=JeToXSlope
          else if(i.eq.3) then
            Ktera=JeToYSlope
          else if(i.eq.4) then
            Ktera=JeToZSlope
          endif
          go to 9999
        else if(prom(2:2).eq.'t'.or.prom(2:2).eq.'r') then
          Ktera=i-1+CumulMol(2)
          if(prom(2:2).eq.'r') Ktera=Ktera+6*mxw
          KwPrv=3
          KwLen=6
          JeToMolekula=.true.
        else if(prom(2:2).eq.'m') then
          Ktera=2*mxdmm+i-1
          JeToMolekula=.true.
        else
          Ktera=i-1+CumulAt(3)
          KwPrv=2
          KwLen=6
        endif
      else if(i.eq.ZacinaU.or.i.eq.ZacinaB.or.i.eq.ZacinaT.or.
     1        i.eq.ZacinaL.or.i.eq.ZacinaS) then
        JeToMolekula=i.eq.ZacinaT.or.i.eq.ZacinaL.or.i.eq.ZacinaS
        if(i.eq.ZacinaB.and.prom(1:4).eq.'beta') then
          j=6
        else
          call velka(prom(1:1))
          j=3
        endif
        if(id.lt.j) go to 9000
        read(prom(j-1:j),100,err=9000) k,l
        if(k.lt.1.or.l.lt.1.or.k.gt.3.or.l.gt.3) go to 9000
        if(i.ne.9) then
          if(k.ne.l) k=k+l+1
        else
          k=k+(l-1)*3
        endif
        if(id.eq.j) then
          Ktera=k+4
          if(i.eq.7) then
            Ktera=Ktera+3
          else if(i.eq.8) then
            Ktera=Ktera+9
          else if(i.eq.9) then
            Ktera=Ktera+15
          endif
        else
          Ktera=k
          KwPrv=j+1
          if(i.eq.9) then
            KwLen=18
          else
            KwLen=12
          endif
          if(i.le.6) then
            Ktera=Ktera+CumulAt(4)
          else
            Ktera=Ktera+CumulMol(i-3)
          endif
        endif
      else if(i.eq.ZacinaC.or.i.eq.ZacinaD.or.i.eq.ZacinaE.or.
     1        i.eq.ZacinaF) then
        call velka(prom(1:1))
        n=i-7
        ip=0
        if(id.lt.n+1) go to 9000
        do i=2,n+1
          read(prom(i:i),100,err=9000) k
          if(k.lt.1.or.k.gt.3) go to 9000
          ip=ip*4+k
        enddo
        k=nindc(ip,n)
        if(k.le.0) go to 9000
        if(id.eq.n+1) then
          Ktera=k+TRankCumul(n-1)
        else
          Ktera=k+CumulAt(n+2)
          KwPrv=n+2
          KwLen=2*TRank(n)
        endif
      else if(i.eq.ZacinaO) then
        if(id.eq.1) then
          Ktera=1+CumulAt(2)
        else if(prom(2:2).eq.'m') then
          Ktera=1+CumulMol(1)
          if(id.eq.2) go to 9999
          Ktera=Ktera+1
          KwPrv=3
          KwLen=2
          JeToMolekula=.true.
        else
          KwPrv=2
          Ktera=2+CumulAt(2)
          KwLen=2
        endif
      else if(i.eq.ZacinaP) then
        if(prom.eq.'phason') then
          ktera=mxdam
        else if(prom.eq.'phasonm') then
          ktera=mxdmm
          JeToMolekula=.true.
        else if(prom.eq.'pc') then
          ktera=CumulAt(1)+1
          call velka(prom(1:1))
        else if(prom.eq.'pv') then
          ktera=CumulAt(1)+2
          call velka(prom(1:1))
        else
          if(id.lt.3.or.id.gt.5) go to 9000
          read(prom(2:3),100,err=9000) j,k
          ktera=CumulAt(1)+j**2+5
          if(k.eq.0) then
            if(id.ne.3) go to 9000
          else
            ktera=ktera+1+(k-1)*2
            if(prom(4:4).eq.'-') then
              ktera=ktera+1
            else if(prom(4:4).ne.'+') then
              go to 9000
            endif
          endif
          call velka(prom(1:1))
        endif
      else if(i.eq.ZacinaR) then
        if(prom.eq.'rfree') then
          ktera=CumulAt(1)
        else
          go to 9000
        endif
      else if(i.eq.ZacinaK) then
        if(prom.eq.lk1) then
          ktera=CumulAt(1)+3
        else if(prom.eq.lk2) then
          ktera=CumulAt(1)+4
        else
          go to 9000
        endif
      else if(i.eq.ZacinaM) then
        ix=ichar(prom(2:2))-ichar(smbx(1))+1
        if(ix.lt.1.or.ix.gt.4) then
          do i=1,3
            if(EqIgCase(prom(2:2),smbp(i))) then
              ix=i
              go to 4000
            endif
          enddo
          go to 9000
        endif
4000    Ktera=CumulAt(10)+ix
        if(prom(3:3).eq.'0') go to 9999
        Ktera=Ktera+3
        KwPrv=3
        KwLen=6
      endif
      if(KwPrv.eq.0) go to 9999
      if(KwPrv.gt.id) go to 9000
      i=index(PrvniVlna,Prom(KwPrv:KwPrv))
      if(i.le.0) go to 9000
      do j=KwPrv+1,id
        if(index('0123456789$',Prom(j:j)).gt.0) go to 5200
      enddo
      go to 9000
5200  if(index(JmenoVlny(i),Prom(KwPrv:j-1)).ne.1) go to 9000
      if(Prom(j:j).eq.'$') then
        k=1
      else
        Cislo=Prom(j:id)
        call Posun(Cislo,0)
        read(Cislo,FormI15,err=9000) k
      endif
      if(i.ne.3) then
        if(i.eq.2) Ktera=Ktera+KwLen/2
        Ktera=Ktera+KwLen*(k-1)
      else
        Ktera=Ktera+(k-1)*KwLen/2
      endif
      if(Prom(j:j).eq.'$') then
        if(JeToMolekula) then
          Ktera=Ktera+mxdmm
        else
          Ktera=Ktera+mxdam
        endif
      endif
      go to 9999
9000  Ktera=0
9999  return
100   format(2i1)
      end
