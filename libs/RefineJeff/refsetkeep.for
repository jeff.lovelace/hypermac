      subroutine RefSetKeep
      use Atoms_mod
      use Basic_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      KPhaseIn=KPhase
      do n=1,NKeep
        nc=KeepNAtCentr(n)
        if(NMolec.gt.0.and.nc.gt.0) then
          if(kmol(nc).gt.0) then
            call CrlRestoreSymmetry(ISymmMolec((kmol(nc)-1)/mxp+1))
          else
            call CrlRestoreSymmetry(ISymmBasic)
          endif
        endif
        if(KeepType(n)/10.eq.IdKeepHydro) then
          if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.3).or.
     1       (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.2)) then
            if(KeepType(n).eq.IdKeepHTetraHed) then
              NAtMax=4
            else if(KeepType(n).eq.IdKeepHTriangl) then
              NAtMax=3
            endif
            if(KeepNNeigh(n)+KeepNH(n).le.NAtMax)
     1        KeepAtAnchor(n)='#asitis'
          endif
          do i=1,KeepNH(n)
            ia=KeepNAtH(n,i)
            if(ai(ia).le.0.) then
              kiset=0
            else
              kiset=-1
            endif
            isw=iswa(ia)
            kip=2
            do ii=1,3
              if(KiA(kip,ia).gt.0) NConstrain=NConstrain+1
              KiA(kip,ia)=kiset
              kip=kip+1
            enddo
            if(NDimI(KPhase).le.0) cycle
            kip=kip+6
            if(KModA(1,ia).gt.0) kip=kip+1+2*KModA(1,ia)
            nn=2*KModA(2,ia)
            if(KCommen(KPhase).gt.0)
     1        nn=min(nn,(ngc(Kphase)+1)*NCommQProduct(isw,KPhase)-1)
            do ii=1,3*nn
              if(KiA(kip,ia).gt.0) NConstrain=NConstrain+1
              KiA(kip,ia)=kiset
              kip=kip+1
            enddo
          enddo
          call EM40SetNewH(n)
        else if(KeepType(n).eq.IdKeepARiding) then
          KPhase=kswa(nc)
          isw=iswa(nc)
          itfc=itf(nc)
          if(itfc.ge.2) then
            call boueq(beta(1,nc),sbeta(1,nc),1,bizo,sbizo,isw)
          else
            bizo=beta(1,nc)
          endif
          pom=KeepADPExtFac(n)
          do i=1,KeepNH(n)
            ia=KeepNAtH(n,i)
            itfh=itf(ia)
            kip=4
            do ii=1,6
              kip=kip+1
              if(KiA(kip,ia).gt.0) then
                KiA(kip,ia)=0
                NConstrain=NConstrain+1
              endif
            enddo
            if(itfh.ge.2) then
              if(itfc.ge.2) then
                do j=1,6
                  beta(j,ia)=pom*beta(j,nc)
                enddo
              else
                do j=1,6
                  beta(j,ia)=pom*Bizo*prcp(j,isw,KPhase)
                enddo
              endif
            else
              if(itfc.ge.2) then
                beta(1,ia)=pom*Bizo
              else
                beta(1,ia)=pom*beta(1,nc)
              endif
            endif
          enddo
        endif
      enddo
      KPhase=KPhaseIn
      if(NMolec.gt.0) call CrlRestoreSymmetry(ISymmBasic)
      return
      end
