      subroutine DSetResAtMol
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension px(28),py(28)
      integer AtMolFlag
      logical EqIgCase
      do i=nvai,1,-1
        if(mai(i).le.0.or.RestType(i).ne.' ') cycle
        do j=1,mai(i)
          if(j.eq.1) then
            if(nai(j,i).lt.0) then
              AtMolFlag=-1
            else
              AtMolFlag=1
            endif
          else
            if((nai(j,i).lt.0.and.AtMolFlag.gt.0).or.
     1         (nai(j,i).gt.0.and.AtMolFlag.lt.0)) then
              AtMolFlag=0
              exit
            endif
          endif
        enddo
        KChD=naixb(i)/100
        naixbi=mod(naixb(i),100)
        nn=mod(iabs(naixbi),10)
        nsl=nails(i)
        if(naixbi.lt.-10) then
          i2=nai(1,i)
        else
          i2=nai(mai(i),i)
        endif
        if(i2.lt.0) then
          j2p=PrvniKiMolekuly(-i2)
          im2=(-i2-1)/mxp+1
          isw=iswmol(im2)
        else
          isw=iswa(i2)
          j2p=PrvniKiAtomu(i2)
        endif
        if(naixbi.lt.11) then
          if(i2.lt.0) then
            if(naixbi.gt.0) then
              jp2=0
              n=-1
            else
              jp2=7
              if(ktls(im2).gt.0) jp2=jp2+21
              n=2*(KModM(1,-i2)-1)
            endif
          else
            if(naixbi.gt.0) then
              jp2=0
              n=-1
            else
              jp2=max(TRankCumul(itf(i2)),10)
              n=2*(KModA(1,i2)-1)
            endif
          endif
          j2=j2p+jp2
          do l=1,mai(i)
            if((naixbi.le.-10.and.l.eq.1).or.
     1         (naixbi.gt.-10.and.l.eq.mai(i))) cycle
            i1=nai(l,i)
            if(i1.lt.0) then
              if(i2.lt.0) then
                if(aimol(-i2).ne.0.) then
                  ratio=aimol(-i1)/aimol(-i2)
                else
                  ratio=0.
                endif
              else
                if(ai(i2).ne.0.) then
                  ratio=aimol(-i1)/ai(i2)
                else
                  ratio=0.
                endif
              endif
              j1=PrvniKiMolekuly(-i1)
              jp1=7
              im1=(-i1-1)/mxp+1
              if(ktls(im1).gt.0) jp1=jp1+21
              if(naixbi.gt.0) then
                jp1=0
              else
                jp1=7
                if(ktls(im1).gt.0) jp1=jp1+21
              endif
            else
              if(i2.lt.0) then
                if(aimol(-i2).ne.0.) then
                  ratio=ai(i1)/aimol(-i2)
                else
                  ratio=0.
                endif
              else
                if(ai(i2).ne.0.) then
                  ratio=ai(i1)/ai(i2)
                else
                  ratio=0.
                endif
              endif
              j1=PrvniKiAtomu(i1)
              if(naixbi.gt.0) then
                jp1=0
              else
                jp1=max(TRankCumul(itf(i1)),10)
              endif
            endif
            j1=j1+jp1
            if(naixbi.le.-10) then
!              der(j2)=der(j2)+der(j1)
            else if(naixbi.le.0) then
!              der(j1)=der(j1)-der(j2)*ratio
            else
              der(j1)=der(j1)-der(j2)
            endif
            if(n.ge.0) then
              fip=0.
              do j=1,3
                if(i1.lt.0) then
                  if(i2.lt.0) then
                    pom= xm(j,im1)+trans(j,-i1)
     1                  -xm(j,im2)-trans(j,-i2)
                  else
                    pom= xm(j,im1)+trans(j,-i1)
     1                  -x(j,i2)
                  endif
                else
                  if(i2.lt.0) then
                    pom= x(j,i1)
     1                  -xm(j,im2)-trans(j,-i2)
                  else
                    pom=x(j,i1)-x(j,i2)
                  endif
                endif
                fip=fip+pi2*qu(j,1,isw,KPhase)*pom
              enddo
              j11=j1-1
              j12=j1
              j1x=j1-jp1+3
              j21=j2-1
              j22=j2
              j2x=j2-jp2+3
              do j=0,n,2
                kk=j/2+1
                pom=fip*float(kk)
                csp=cos(pom)
                snp=sin(pom)
                j11=j11+2
                j12=j12+2
                j21=j21+2
                j22=j22+2
                if(naixbi.le.-10) then
                  der(j21)=der(j21)+der(j11)*csp-der(j12)*snp
                  der(j22)=der(j22)+der(j12)*csp+der(j11)*snp
                else
                  der(j11)=der(j11)-(der(j21)*csp+der(j22)*snp)*ratio
                  der(j12)=der(j12)-(der(j22)*csp-der(j21)*snp)*ratio
                endif
                if(NDimI(KPhase).eq.1) then
                  if(naixbi.le.-10) then
                    if(i1.lt.0) then
                      derp=
     1                  ((-axm(kk,-i1)*snp-aym(kk,-i1)*csp)*der(j21)+
     2                   ( axm(kk,-i1)*csp-aym(kk,-i1)*snp)*der(j22))
     3                     *pi2*float(kk)
                    else
                      derp=
     1                  ((-ax(kk,i1)*snp-ay(kk,i1)*csp)*der(j21)+
     2                   ( ax(kk,i1)*csp-ay(kk,i1)*snp)*der(j22))
     3                     *pi2*float(kk)
                    endif
                  else
                    if(i1.lt.0) then
                      derp=((-axm(kk,-i1)*snp+aym(kk,-i1)*csp)*der(j21)+
     1                      (-axm(kk,-i1)*csp-aym(kk,-i1)*snp)*der(j22))
     2                     *pi2*float(kk)*ratio
                    else
                      derp=((-ax(kk,i1)*snp+ay(kk,i1)*csp)*der(j21)+
     1                      (-ax(kk,i1)*csp-ay(kk,i1)*snp)*der(j22))*
     2                     pi2*float(kk)*ratio
                    endif
                  endif
                  do jx=1,3
                    qup=qu(jx,1,isw,KPhase)
                    der(j1x+jx)=der(j1x+jx)+derp*qup
                    der(j2x+jx)=der(j2x+jx)-derp*qup
                  enddo
                endif
              enddo
            endif
          enddo
        endif
        if(AtMolFlag.eq.0) cycle
        if(nn.le.IdRestADP) then
          if(AtMolFlag.lt.0) then
            if(ktls(im2).gt.0) then
              if(nn.eq.IdRestModADP.or.nn.eq.IdRestADP) then
                jpt=7
              else
                jpt=1
              endif
              jkt=27
            else
              jpt=7
              jkt=0
            endif
            if(nn.le.IdRestModADP) then
              if(KModM(1,i2).le.0) then
                jpz=7
              else
                jpz=8+2*KModM(1,i2)
              endif
              if(ktls(im2).gt.0) jpz=jpz+21
              jkz=DelkaKiMolekuly(i2)-1
            endif
            j1p=PrvniKiMolekuly(-nai(1,i))
            do l=2,mai(i)
              j1=j1p+jpt
              j2p=PrvniKiMolekuly(-nai(l,i))
              j2=j2p+jpt
              do j=jpt,jkt
                der(j1)=der(j1)+der(j2)
                der(j2)=0.
                j1=j1+1
                j2=j2+1
              enddo
              if(nn.le.IdRestModADP.and.NDim(KPhase).gt.3) then
                j1=j1p+jpz
                j2=j2p+jpz
                do j=jpz,jkz
                  der(j1)=der(j1)+der(j2)
                  der(j2)=0.
                  j1=j1+1
                  j2=j2+1
                enddo
              endif
            enddo
          else
            ia=nai(1,i)
            itfi=max(itf(ia),1)
            if(ia.gt.0) mip=PrvniKiAtomu(ia)
            do j=2,mai(i)
              mi=mip
              ja=nai(j,i)
              mj=PrvniKiAtomu(ja)
              mjpp=mj
              if(nn.eq.0) go to 3000
              do n=1,itfi+1
                nrank=TRank(n-1)
                if(nn.le.IdRestADP) then
                  if(nsl.gt.0) then
                    if(n.eq.2.and.nn.eq.IdRestXModADP) then
                      call cultm(der(mj),rml(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    else if(n.eq.3) then
                      call cultm(der(mj),rtl(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    else if(n.eq.4) then
                      call cultm(der(mj),rc3l(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    else if(n.eq.5) then
                      call cultm(der(mj),rc4l(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    else if(n.eq.6) then
                      call cultm(der(mj),rc5l(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    else if(n.eq.7) then
                      call cultm(der(mj),rc6l(1,nsl,isw,KPhase),der(mi),
     1                           1,nrank,nrank)
                    endif
                  else
                    if(n.gt.2.or.(n.eq.2.and.nn.eq.IdRestXModADP))
     1                call AddVek(der(mj),der(mi),der(mi),nrank)
                  endif
                endif
                mi=mi+nrank
                mj=mj+nrank
              enddo
              if(nn.le.IdRestADP) then
                if(itfi.eq.1) then
                  der(mi)=der(mi)+der(mj)
                  mi=mi+TRank(2)
                  mj=mj+TRank(2)
                endif
              endif
              if(nn.gt.IdRestModADP) cycle
              if(KModA(1,ia).gt.0) mi=mi+1+2*KModA(1,ia)
              if(KModA(1,ja).gt.0) mj=mj+1+2*KModA(1,ja)
              do n=2,itfi+1
                mjp=mj
                kmod=KModA(n,ia)
                kfai=KFA(n,ia)
                nrank=TRank(n-1)
                if(kmod.le.0) cycle
                do k=1,kmod
                  if(nsl.gt.0) then
                    snp=snls(k,nsl,ja)
                    csp=csls(k,nsl,ja)
                    if(NDim(KPhase).eq.4) eps=rm6l(16,nsl,isw,KPhase)
                    epsp=isign(1,KwSymL(k,nsl,isw,KPhase))
                    if(kfai.eq.0.or.n.ne.2.or.k.ne.kmod) then
                      iw=iabs(KwSymL(k,nsl,isw,KPhase))
                    else
                      iw=k
                    endif
                    mj=mjp+2*(iw-1)*nrank
                    if(n.eq.2) then
                      call multm(der(mj       ),rml(1,nsl,isw,KPhase),
     1                           px,1,nrank,nrank)
                      if(kfai.eq.0.or.k.ne.kmod)
     1                  call multm(der(mj+nrank),rml(1,nsl,isw,KPhase),
     2                             py,1,nrank,nrank)
                    else if(n.eq.3) then
                      call multm(der(mj      ),rtl(1,nsl,isw,KPhase),px,
     1                           1,nrank,nrank)
                      call multm(der(mj+nrank),rtl(1,nsl,isw,KPhase),py,
     1                           1,nrank,nrank)
                    else if(n.eq.4) then
                      call multm(der(mj      ),rc3l(1,nsl,isw,KPhase),
     1                           px,1,nrank,nrank)
                      call multm(der(mj+nrank),rc3l(1,nsl,isw,KPhase),
     1                           py,1,nrank,nrank)
                    else if(n.eq.5) then
                      call multm(der(mj      ),rc4l(1,nsl,isw,KPhase),
     1                           px,1,nrank,nrank)
                      call multm(der(mj+nrank),rc4l(1,nsl,isw,KPhase),
     1                           py,1,nrank,nrank)
                    else if(n.eq.6) then
                      call multm(der(mj      ),rc5l(1,nsl,isw,KPhase),
     1                           px,1,nrank,nrank)
                      call multm(der(mj+nrank),rc5l(1,nsl,isw,KPhase),
     1                           py,1,nrank,nrank)
                    else if(n.eq.7) then
                      call multm(der(mj      ),rc6l(1,nsl,isw,KPhase),
     1                           px,1,nrank,nrank)
                      call multm(der(mj+nrank),rc6l(1,nsl,isw,KPhase),
     1                           py,1,nrank,nrank)
                    endif
                    do l=1,nrank
                      if(kfai.eq.0.or.n.ne.2.or.k.ne.kmod) then
                        der(mi)=der(mi)+epsp*(csp*px(l)+snp*py(l))
                        der(mi+nrank)=der(mi+nrank)-snp*px(l)+csp*py(l)
                      else
                        der(mi)=der(mi)+eps*px(l)
                        if(l.eq.1) then
                          der(mi+nrank)=der(mi+nrank)+eps*der(mj+nrank)
                        else if(l.eq.2) then
                          der(mi+nrank)=der(mi+nrank)+der(mj+nrank)
                        endif
                      endif
                      mi=mi+1
                      mj=mj+1
                    enddo
                    mi=mi+nrank
                  else
                    call AddVek(der(mj),der(mi),der(mi),2*nrank)
                    mi=mi+2*nrank
                    mj=mj+2*nrank
                  endif
                enddo
                mj=mjp+2*nrank*kmod
              enddo
3000          if(.not.ChargeDensities) cycle
              if(lasmax(ia).le.0.or.lasmax(ja).le.0) cycle
              k=KChD
              mi=mip+max(TRankCumul(itfi),10)
              itfj=max(itf(ja),1)
              mj=mjpp+max(TRankCumul(itfj),10)
              do icd=1,3
                if(mod(k,2).eq.1) then
                  der(mi+icd)=der(mi+icd)+der(mj+icd)
                  der(mj+icd)=0.
                endif
                k=k/2
              enddo
            enddo
          endif
        endif
      enddo
9999  return
      end
