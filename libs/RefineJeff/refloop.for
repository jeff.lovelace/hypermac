      subroutine RefLoop(RefineEnd,lst0,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension hhp(6),ihfour(6),ihitw(6,18,4),IHReadO(6),difp(3),hh(3),
     1          ihp(6),DerOv(:),DerLam(:),MMin(3),MMax(3)
      character*256 Veta
      character*128 ven
      character*80 t80
      character*1 nspec,kspec,FlagObs
      logical ranover,RefineEnd,EqIgCase,FeYesNoHeader,EqIV,lpom,
     1        ReflFulfilsCond
      allocatable DerOv,DerLam
      real Iobs,LamAveOld
      data Ranover/.false./
      if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     2  'begin'
      LamAveOld=LamAve(KDatBlock)
      itstder=0
      wdylim=0.
      if(isimul.le.0) then
        call CloseIfOpened(91)
        call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9100
      endif
      n=0
      HKLF5File=HKLF5(KDatBlock).eq.1
      if(.not.HKLF5File) then
1100    read(91,Format91,err=1150,end=1150)(i,j=1,maxNDim),pomi,poms,
     1                                        i,i,i,pom
        n=n+1
        if(i.lt.0) then
          read(91,Format91,err=1150,end=1150)(i,m=1,maxNDim),pomn,
     1                                        poms,i,i,i,pom
          if(abs(pomn-pomi).lt..1) HKLF5File=.true.
        else
          if(n.lt.50) go to 1100
        endif
      endif
1150  if(isimul.le.0) then
        call NastavM90(91)
      else
        rewind 91
      endif
1300  do i=1,NTwin
        call SetIntArrayTo(ihitw(1,i,1),4*maxNDim,0)
        ihitw(1,i,1:4)=999
      enddo
1350  ntwp1=0
      ntwp2=0
1400  read(91,FormA,end=4000) Veta
      if(Veta.eq.' ') go to 1400
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'data')) go to 4000
      read(Veta,format91,err=9000)(ihread(i),i=1,maxNDim),Iobs,
     1                             SigIobs,iq,nxx,itwr,efpip(7),RefLam,
     2                             RefDirCos
      if(isimul.ne.0) efpip(7)=0.
      if(itwr.eq.0) itwr=1
      iatwr=iabs(mod(iabs(itwr),100))
      iaov=max(iabs(itwr)/100,1)
      iovmax=1
      if(SigIobs.le.0.) SigIobs=.1
      KPhase=KPhaseTwin(iatwr)
      if(HKLF5File) then
        if(iaov.eq.1) then
          ntwp1=ntwp1+1
          if(ntwp1.gt.50) then
            go to 1350
          else
            call CopyVekI(ihread,ihitw(1,iatwr,1),maxNDim)
            call FromIndSinthl(ihitw(1,iatwr,1),hh,snt(iatwr),sinthlq,1,
     1                         0)
            if(itwr.lt.0) go to 1400
          endif
        else
          iovmax=2
          ntwp2=ntwp2+1
          if(ntwp2.gt.50) then
            go to 1350
          else
            call CopyVekI(ihread,ihitw(1,iatwr,2),maxNDim)
            call FromIndSinthl(ihitw(1,iatwr,2),hh,snt(iatwr),sinthlq,1,
     1                         0)
            if(itwr.lt.0) go to 1400
          endif
        endif
      else if(itwr.gt.1.and.NTwin.le.1) then
        if(.not.AnnouncedTwinProblem) then
          AnnouncedTwinProblem=.true.
          call FeFillTextInfo('refine1.txt',0)
          t80='Do you want to continue with the twinning flag used as'//
     1        ' a scale indicator?'
          if(.not.FeYesNoHeader(-1.,-1.,t80,1)) go to 9200
          if(sc(itwr,KDatBlock).le.0.) then
            sc(itwr,KDatBlock)=sc(1,KDatBlock)
            kis(itwr,KDatBlock)=1
            if(MaxNDimI.gt.0) then
              call RefUpdateOrtho
              if(allocated(ScRes)) then
                do j=1,NDatBlock
                  if(Radiation(j).ne.ElectronRadiation.or.
     1               .not.ExistM42.or..not.CalcDyn) cycle
                  do i=1,mxsc
                    pom=Sc(i,j)
                    Sc(i,j)=ScRes(i,j)
                    ScRes(i,j)=pom
                  enddo
                enddo
              endif
              call iom40(1,0,fln(:ifln)//'.m40')
              if(allocated(ScRes)) then
                do j=1,NDatBlock
                  if(Radiation(j).ne.ElectronRadiation.or.
     1               .not.ExistM42.or..not.CalcDyn) cycle
                  do i=1,mxsc
                    pom=Sc(i,j)
                    Sc(i,j)=ScRes(i,j)
                    ScRes(i,j)=pom
                  enddo
                enddo
              endif
            endif
            ich=5
            go to 9999
          endif
        endif
        iq=itwr
        itwr=1
        iatwr=1
      endif
      ILam=1
      if(efpip(7).le.0.) efpip(7)=1.5*ExtRadius(KDatBlock)
1500  if(ILam.eq.1) then
        if(KAnRef(KDatBlock).eq.0.and.RefLam.gt.0.) then
          LamAve(KDatBlock)=RefLam
        else
          LamAve(KDatBlock)=LamAveOld
        endif
      else
        LamAve(KDatBlock)=LamAve(KDatBlock)*.5
        IHRead(1:maxNDim)=2*IHRead(1:maxNDim)
      endif
      call FromIndSinthl(ihread,hh,sinthl,sinthlq,1,0)
      if(ILam.eq.1) then
        if(isimul.le.0) then
          nulova=Iobs.le.slevel*SigIobs
          if(MagPolFlag(KDatBlock).ne.0) then
            Fobs=IObs
            SigFobs=SigIobs
            sigyo=SigFobs
            sigyos=sigyo
          else
            if(BayesFobs.eq.0) then
              if(Iobs.gt.0.) then
                Fobs=sqrt(Iobs)
              else
                Fobs=0.
              endif
              if(Iobs.le..01*SigIobs) then
                SigFobs=sqrt(SigIobs)/(.2)
              else
                SigFobs=.5/Fobs*SigIobs
              endif
            else
              Fobs=0.5*sqrt(2.*Iobs+sqrt(4.*Iobs**2+8.*SigIobs**2))
              SigFobs=1./
     1                sqrt(1./Fobs**2+2.*(3.*Fobs**2-Iobs)/SigIobs**2)
              Iobs=Fobs**2
              SigIobs=2.*Fobs*SigFobs
            endif
            if(ifsq.ne.1) then
              sigyos=SigFobs
            else
              sigyos=SigIobs
            endif
            SigFobs=sqrt(SigFobs**2+(blkoef*Fobs)**2)
            SigIobs=sqrt(SigIobs**2+(2.*blkoef*Iobs)**2)
            if(ifsq.ne.1) then
              sigyo=SigFobs
            else
              sigyo=SigIobs
            endif
          endif
        else
          nulova=.false.
          sigyo=1.
          sigyos=1.
          if(ifsq.ne.1) then
            SigIobs=0.
            SigFobs=0.
          else
            SigIobs=0.
            SigFobs=0.
          endif
        endif
      endif
      if(nFlowChart.ne.0) then
        call FeFlowChartEvent(noread,ie)
        if(ie.ne.0) call EventRefine(ie,RefineEnd,*9100)
      else
        noread=noread+1
      endif
      if(ILam.eq.2) NoRead=NoRead-1
      if(ipisd.eq.0.and.DatBlockDerTest.le.0) then
        if(ILam.eq.1) then
          if(nulova.and.useunobs.ne.1) go to 1300
          if(sinthl.lt.snlmn.or.sinthl.gt.snlmx) go to 1300
          do i=1,nxxn
            if(nxx.eq.nxxp(i)) go to 1300
          enddo
          NUse=0
          IUse=0
          do i=1,nskrt
            lpom=ReflFulfilsCond(ihread,ihm(1,i),ihma(1,i),
     1                           ihsn(1,i),imdn(i),ieqn(i),
     2                           ihsv(1,i),imdv(i),ieqv(i))
            if(.not.DontUse(i)) NUse=NUse+1
            if(lpom) then
              if(DontUse(i)) then
                go to 1300
              else
                IUse=1
              endif
            endif
          enddo
          if(NUse.gt.0.and.IUse.eq.0) go to 1300
        endif
        if(kim.ge.-1.and.NDimI(KPhase).gt.0) then
          mmm=999999
          do i=1,NComp(KPhase)
            mm=0
            if(i.eq.1) then
              call CopyVekI(ihread,ihp,NDim(KPhase))
            else
              call MultMIRI(ihread,zvi(1,i,KPhase),ihp,1,NDim(KPhase),
     1                      NDim(KPhase))
            endif
            do j=4,NDim(KPhase)
              mm=mm+iabs(ihp(j))
            enddo
            mmm=min(mm,mmm)
          enddo
          if((kim.eq.-1.and.mmm.eq.0).or.
     1       (kim.ge.0.and.mmm.ne.kim)) go to 1300
        endif
      else if(ipisd.ne.0) then
        if(noread.lt.ipisd.and.ipisd.gt.0) go to 1300
      else
        if(KDatBlock.ne.DatBlockDerTest.or.
     1     .not.EqIV(IHRead,HDerTest,maxNDim)) go to 1300
      endif
      ihp=0
      do i=1,NScales
        if(ReflFulfilsCond(ihread,ihms(1,i),ihmas(1,i),
     1                     ihssn(1,i),imdsn(i),ieqsn(i),
     2                     ihssv(1,i),imdsv(i),ieqsv(i)))
     3    iq=iqs(i)
      enddo
      if(nulova) then
        nspec='*'
      else
        nspec=' '
      endif
      nic=.true.
2020  extkor=1.
      extkorm=1.
      if(NAtCalc.le.0) then
        nzz=nzz+1
        write(80,format80)(ihread(i),i=1,maxNDim),KPhase,Fobs,Fobs,
     1                     Fobs,a,b
        go to 1300
      endif
      iov=(ILam-1)*2+1
2050  if(NTwin.gt.1) then
        yctw=0.
        yctwm=0.
        yctwmp=0.
        if(mod(iov,2).eq.1) then
          if(.not.HKLF5File) call settw(ihread,ihitw(1,1,iov),itwr,
     1                                  ranover)
          do j=1,NTwin
            if(ihitw(1,j,iov).lt.900) go to 2100
          enddo
          NBadOverRef(ICyklMod6,KDatBlock)=
     1      NBadOverRef(ICyklMod6,KDatBlock)+1
          Ranover=.true.
          Nic=.false.
          go to 3150
        else if(OverDif.le.0.) then
          do j=1,NTwin
            k=isign(1,ihitw(4,j,1))
            do i=1,4
              ihitw(i,j,iov)=ihitw(i,j,1)+ihov(i)*k
            enddo
          enddo
        endif
      else
        if(.not.HKLF5File) ihitw(1:maxNDim,1,iov)=IHRead(1:maxNDim)
        Ranover=.false.
      endif
2100  if(calcder) call SetRealArrayTo(der,ndoff,0.)
      if(mod(iov,2).eq.1) mmabsm=-999
      do itw=1,NTwin
        KPhase=KPhaseTwin(itw)
        if(NTwin.gt.1) then
          if(ranover) then
            if(kranpr.eq.1) then
              nic=.false.
              go to 3150
            else
              go to 1300
            endif
          endif
        endif
        Fcalc=0.
        yct(itw,iov)=0.
        ycq(itw)=0.
        yctm(itw,iov)=0.
        ycqm(itw)=0.
        yctmp(itw,iov)=0.
        ycqmp(itw)=0.
        do i=1,NDim(KPhase)
          hhp(i)=ihitw(i,itw,iov)
          ihref(i,1)=ihitw(i,itw,iov)
          if(itw.eq.1.and.iov.eq.1) ihfour(i)=ihref(i,1)
        enddo
        nicova=hhp(1).gt.900.
        if(nicova) go to 2800
        do i=2,NLattVec(KPhase)
          pom=0.
          do j=1,NDim(KPhase)
            pom=pom+hhp(j)*vt6(j,i,1,KPhase)
          enddo
          if(abs(pom-float(nint(pom))).gt..0001) then
            nicova=.true.
            go to 2800
          endif
        enddo
        call RefLoopBefore(0,iov,ich)
        if(ich.eq.1) then
          ich=0
          go to 1300
        else if(ich.eq.2) then
          go to 9100
        endif
2800    call calc(iov)
        if(ErrFlag.eq.2) then
          ErrFlag=0
          go to 9300
        else if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 9100
        endif
        if(nicova) cycle
        call RefInfoKolaps(lst0,*9100)
        if(NTwin.le.1) then
          yofour1=Fobs
          yofour2=Fobs
          ysfour1=SigFobs
          ysfour2=SigFobs
        endif
      enddo
      if(ILam.eq.1.and.isimul.le.0.and.MagPolFlag(KDatBlock).eq.0.and.
     1   WilsonMod.eq.1) then
        pom =(Fobs+2.*Fcalc)/3.
        pomq=(Iobs+2.*Fcalc**2)/3.
        SigFobs=sqrt(SigFobs**2+(blkoef*pom)**2-(blkoef*Fobs)**2)
        SigIobs=sqrt(SigIobs**2+(2.*blkoef*pomq)**2-(2.*blkoef*Iobs)**2)
        if(ifsq.ne.1) then
          sigyo=SigFobs
        else
          sigyo=SigIobs
        endif
      endif
      if(calcder) then
        IZdvih=(KDatBlock-1)*MxScAll
!   tady pro MagPol nevim ...
        if(ifsq.eq.1) then
          der(iq+IZdvih)=2.*Fcalc**2/sc(iq,KDatBlock)
          der(MxSc+1+IZdvih)=-sinthlq*episq*2.*Fcalc**2
          pom=1.
        else
          if(Fcalc.gt.0.) then
            der(iq+IZdvih)=Fcalc/sc(iq,KDatBlock)
            der(MxSc+1+IZdvih)=-sinthlq*episq*Fcalc
            pom=.5/Fcalc
          else
            pom=0.
          endif
        endif
        do i=2,NTwin
          der(mxscu+i-1+IZdvih)=(ycq(i)-ycq(1))*pom
          if(MagneticType(KPhase).ne.0)
     1      der(mxscu+i-1+IZdvih)=der(mxscu+i-1+IZdvih)+
     2                            (ycqm(i)-ycqm(1))*pom
        enddo
      endif
      if(iover.ne.0.or.(HKLF5File.and.iabs(ihitw(1,1,2)).lt.900)) then
        if(mod(iov,2).eq.1) then
          iov=iov+1
          IHReadO(1:maxNDim)=IHRead(1:maxNDim)
          if(OverDif.le.0.) then
            j=isign(1,ihread(4))
            do i=1,4
              IHRead(i)=IHRead(i)+ihov(i)*j
            enddo
          else
            pomm=999999.
            MMax=0
            do i=1,NDimI(KPhase)
              MMax(i)=iover
            enddo
            MMin=-MMax
            do i=1,NTwin
              KPhase=KPhaseTwin(i)
              if(NDimI(KPhase).gt.0.and.ihitw(1,i,1).lt.900) then
                call CopyVekI(ihitw(1,i,1),ihread,maxNDim)
                call FromIndSinthl(ihitw(1,i,1),hh,pom,pomq,1,0)
                call ChngInd(hh,ihread,1,difp,MMin,MMax,2,
     1                       CheckExtRefYes)
                call FromIndSinthl(ihread,difp,pom,dpom,1,1)
                pom=asin(pom*LamAve(1))/torad
              else
                pom=999999.
              endif
              pomm=min(pomm,pom)
              if(pom.gt.OverDif) then
                ihitw(:,i,2)=999
              else
                call CopyVekI(ihread,ihitw(1,i,2),maxNDim)
              endif
            enddo
            if(pomm.gt.OverDif) then
              call CopyVekI(ihreado,ihread,maxNDim)
              do itw=1,NTwin
                yct(itw,iov)=0.
                act(itw,iov)=0.
                bct(itw,iov)=0.
                yctm(itw,iov)=0.
                yctmp(itw,iov)=0.
              enddo
              go to 3000
            endif
          endif
          if(Calcder) then
            if(.not.allocated(DerOv)) allocate(DerOv(PosledniKiAtPos))
            call CopyVek(der,DerOv,PosledniKiAtPos)
          endif
          ycp=Fcalc
          go to 2050
        else
          ycpp=Fcalc
          Fcalc=sqrt(ycp**2+ycpp**2)
          if(Calcder) then
            if(Fcalc.gt.0.) then
              pom=1./Fcalc
            else
              pom=0.
            endif
            do i=1,PosledniKiAtPos
              der(i)=pom*(ycp*DerOv(i)+ycpp*der(i))
            enddo
          else
            if(NTwin.le.1) then
              if(Fcalc.gt.0.) then
                pom=ycpp/Fcalc
                yofour1=Fobs*pom
                ysfour1=SigFobs*pom
                ycpom=sqrt(afour(1,2)**2+bfour(1,2)**2)
                ycpoms=sqrt(act(1,2)**2+bct(1,2)**2)
                yofour2=Fobs**2-ycp**2
              else
                yofour1=0.
                ysfour1=0.
                yofour2=0.
                ycpom=0.
                ycpoms=0.
                afour(1,2)=0.
                bfour(1,2)=0.
              endif
              if(yofour2.gt.0.) then
                yofour2=sqrt(yofour2)
                ysfour2=SigFobs
              else
                yofour2=0.
              endif
              if(yct(1,2).gt.0.) then
                pom=ycpom/yct(1,2)
                poms=ycpoms/yct(1,2)
                yofour1=yofour1*pom
                ysfour1=ysfour1*poms
                yofour2=yofour2*pom
                ysfour2=ysfour2*poms
              endif
              write(80,format80)(ihitw(i,1,2),i=1,maxNDim),1,
     1          yofour1,yofour2,ycpom,afour(1,2),bfour(1,2),affree,
     2          bffree,afst,bfst,affreest,bffreest,ysfour1,ysfour2
              if(Fcalc.gt.0.) then
                pom=ycp/Fcalc
                yofour1=Fobs*pom
                ysfour1=SigFobs*pom
                yofour2=Fobs**2-ycpp**2
              else
                yofour1=0.
                ysfour1=0.
              endif
              if(yofour2.gt.0.) then
                yofour2=sqrt(yofour2)
                ysfour2=SigFobs
              else
                yofour2=0.
              endif
            endif
          endif
          IHRead(1:maxNDim)=IHReadO(1:maxNDim)
        endif
      endif
3000  if(Lam2Corr.eq.1) then
        if(ILam.eq.1) then
          ycpl=Fcalc
          al2=a
          bl2=b
          ExtKorl2=ExtKor
          ILam=2
          if(Calcder) then
            if(.not.allocated(DerLam)) allocate(DerLam(PosledniKiAtPos))
            call CopyVek(der,DerLam,PosledniKiAtPos)
          endif
          go to 1500
        else
          ycppl=Fcalc
          FCalc=sqrt(ScLam2(KDatBlock)*FCalc**2+ycpl**2)
          a=al2
          b=bl2
          ExtKor=ExtKorl2
          if(Fcalc.gt.0.) then
            pom=1./Fcalc
          else
            pom=0.
          endif
          if(Calcder) then
            do i=1,PosledniKiAtPos
              der(i)=pom*(ycpl*DerLam(i)+
     1                    ScLam2(KDatBlock)*ycppl*der(i))
            enddo
            Der(MxSc+(KDatBlock-1)*MxScAll+4)=.5*ycppl**2*pom
          endif
          ILam=1
          IHRead(1:maxNDim)=IHRead(1:maxNDim)/2
          call FromIndSinthl(ihread,hh,sinthl,sinthlq,1,0)
        endif
      endif
      if(nic) go to 3150
      if(.not.CalcDer) then
        do KPh=1,NPhase
          if(NTwin.gt.1) then
            do itw=1,NTwin
              if(KPh.eq.KPhaseTwin(itw)) go to 3100
            enddo
            cycle
          else
            itw=1
          endif
3100      if(ihitw(1,itw,1).gt.900) cycle
          if(NTwin.gt.1) then
            if(Fcalc.ne.0.) then
              pom=yct(itw,1)/Fcalc
              yofour1=Fobs*pom
              ysfour1=SigFobs*pom
            else
              yofour1=0.
            endif
            yofour2=Fobs**2
            do i=1,NTwin
              if(i.ne.itw)
     1          yofour2=yofour2-sctw(i,KDatBlock)*yct(i,1)**2
              if(iover.ne.0) then
                if(ihitw(1,itw,2).lt.900)
     1            yofour2=yofour2-sctw(i,KDatBlock)*yct(i,2)**2
              endif
            enddo
            if(yofour2.gt.0..and.sctw(itw,KDatBlock).gt.0.) then
              yofour2=sqrt(yofour2/sctw(itw,KDatBlock))
              ysfour2=SigFobs/sqrt(sctw(itw,KDatBlock))
            else
              yofour2=0.
              ysfour2=1.
            endif
          endif
          ycpom=sqrt(afour(itw,1)**2+bfour(itw,1)**2)
          ycpoms=sqrt(act(itw,1)**2+bct(itw,1)**2)
          if(isimul.le.0) then
            if(yct(itw,1).gt.0.) then
              pom=ycpom/yct(itw,1)
              poms=ycpoms/yct(itw,1)
              yofour1=yofour1*pom
              ysfour1=ysfour1*poms
              yofour2=yofour2*pom
              ysfour2=ysfour2*poms
            else
              yofour1=0.
              ysfour1=0.
              yofour2=0.
              ysfour2=0.
              ycpom=0.
              ycpoms=0.
              afour(itw,1)=0.
              bfour(itw,1)=0.
            endif
          else
            yofour1=ycpom
            yofour2=ycpom
            ysfour1=0.
            ysfour2=0.
          endif
          write(80,format80)(ihitw(i,itw,1),i=1,maxNDim),KPh,
     1      yofour1,yofour2,ycpom,afour(itw,1),bfour(itw,1),affree,
     2      bffree,afst,bfst,affreest,bffreest,ysfour1,ysfour2
        enddo
      else if(CalcDer) then
        call DSetAll
      endif
      if(Nulova) then
        FlagObs='<'
      else
        FlagObs='o'
      endif
      if(isimul.gt.0) then
        pom=1./sc(iq,KDatBlock)**2
         FCalcQ=Fcalc**2*pom
        sFCalcQ=SigIobs**2-(2.*blkoef*Iobs)**2
        if(sFCalcQ.gt.0.) then
          sFCalcQ=sqrt(SigIobs**2-(2.*blkoef*Iobs)**2)*pom
        else
          sFCalcQ=SigIobs*pom
        endif
        write(83,format83e)(ihread(i),i=1,MaxNDim),FcalcQ,FcalcQ,
     1                      sFCalcQ,FlagObs,itwr,0.,Fobs,Fcalc,SigFObs,
     2                      pom,sqrt(act(1,1)**2+bct(1,1)**2),
     3                      act(1,1),bct(1,1)
        go to 1300
      endif
      if(DatBlockDerTest.gt.0) then
        call DerTest(ihread,Fobs,Fcalc,itstder)
        if(itstder.lt.0) then
          go to 9999
        else
c          call RefLoopNuluj(NRef90(KDatBlock))
          go to 2020
        endif
      endif
      dyp=Fobs-Fcalc
      dy=dyp
      if(iwq.eq.0) then
        wt=1./sigyo**2
      else if(iwq.eq.1) then
        wt=1.
      else
        wt=1./(2.*yomin+Fobs+2.*Fobs**2/yomax)
        if(ifsq.eq.1) then
          if(Iobs.gt.0.) then
            wt=wt/(4.*Iobs)
          else
            wt=0.
          endif
        endif
      endif
      if(wtsnthl.eq.1) wt=wt*exp(-sinthl**2)
      if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
        dy=Iobs-Fcalc**2
        if(nulova) then
          sigyo=sqrt(.5*sigyo)
        else
          sigyo=.5/Fobs*sigyo
        endif
      endif
      wdy=sqrt(wt)*dy
      wdyq=wdy**2
      if(ifsq.ne.1.and.MagPolFlag(KDatBlock).eq.0) then
        wyoq=wt*Fobs**2
      else
        wyoq=wt*Iobs**2
      endif
      if(abs(wdy).gt.vyh) then
        kspec='#'
        if(iskip.eq.1) then
          nzz=nzz-1
          if(.not.CalcDer) backspace 80
          NSkipRef(ICyklMod6,KDatBlock)=NSkipRef(ICyklMod6,KDatBlock)+1
          go to 3150
        endif
      else
        kspec=' '
      endif
      wRNumAll(1)=wRNumAll(1)+wdyq
      wRDenAll(1)=wRDenAll(1)+wyoq
       RNumOverall=RNumOverall+abs(dyp)
       RDenOverall=RDenOverall+abs(Fobs)
      wRNumOverall=wRNumOverall+wdyq
      wRDenOverall=wRDenOverall+wyoq
      nRAll(1)=nRAll(1)+1
      nRFacOverall=nRFacOverall+1
      if(.not.nulova) then
        wRNumObs(1)=wRNumObs(1)+wdyq
        wRDenObs(1)=wRDenObs(1)+wyoq
        nRObs(1)=nRObs(1)+1
        nRFacOverallObs=nRFacOverallObs+1
         RNumOverallObs=RNumOverallObs+abs(dyp)
         RDenOverallObs=RDenOverallObs+abs(Fobs)
        wRNumOverallObs=wRNumOverallObs+wdyq
        wRDenOverallObs=wRDenOverallObs+wyoq
      endif
      do i=1,maxNDim
        ihmax(i)=max(ihmax(i),ihread(i))
        ihmin(i)=min(ihmin(i),ihread(i))
      enddo
      if(ipisd.ne.0) then
        call PisDer(88,ihread,sigyo,wdy,noread,nspec,kspec)
        if(ipisd.gt.0) go to 9200
      endif
      if(icykl.ne.ncykl) then
        call SumaRefine
      else
        pom=1./(sc(iq,KDatBlock)*ExtKor)**2
         FObsQ =IObs    *pom
         FCalcQ=Fcalc**2*pom
        sFCalcQ=SigIobs**2-(2.*blkoef*Iobs)**2
        if(sFCalcQ.gt.0.) then
          sFCalcQ=sqrt(SigIobs**2-(2.*blkoef*Iobs)**2)*pom
        else
          sFCalcQ=SigIobs*pom
        endif
        if(iover.ne.0) then
          if(iabs(ihitw(1,1,2)).lt.900) then
            j=-200-itwr
            write(83,format83e)(ihitw(i,1,2),i=1,MaxNDim),FCalcQ,FObsQ,
     1                          sFCalcQ,FlagObs,j,wdy,Fobs,Fcalc,
     2                          SigFObs,pom,
     3                          sqrt(act(1,1)**2+bct(1,1)**2),
     4                          act(1,1),bct(1,1)
            j=100+itwr
            write(83,format83e)(ihitw(i,1,1),i=1,MaxNDim),FCalcQ,FObsQ,
     1                          sFCalcQ,FlagObs,j,wdy,Fobs,Fcalc,
     2                          SigFObs,pom,
     3                          sqrt(act(1,1)**2+bct(1,1)**2),
     4                          act(1,1),bct(1,1)
          else
            write(83,format83e)(ihread(i),i=1,MaxNDim),FCalcQ,FObsQ,
     1                          sFCalcQ,FlagObs,itwr,wdy,Fobs,Fcalc,
     2                            SigFObs,pom,
     3                            sqrt(act(1,1)**2+bct(1,1)**2),
     4                            act(1,1),bct(1,1)
          endif
        else
          if(NTwin.gt.1) then
            do i=1,NTwin
              if(iabs(ihitw(1,i,1)).lt.900) then
                IFirst=i
                exit
              endif
            enddo
            do i=NTwin,IFirst,-1
              if(abs(ihitw(1,i,1)).gt.900) cycle
              if(i.eq.IFirst) then
                itwp= i
                pom1=sqrt(act(itwp,1)**2+bct(itwp,1)**2)
                pom2=act(itwp,1)
                pom3=bct(itwp,1)
              else
                itwp=-i
                pom1=0.
                pom2=0.
                pom3=0.
              endif
              write(83,format83e)(ihitw(j,i,1),j=1,MaxNDim),FCalcQ,
     1                            FObsQ,sFCalcQ,FlagObs,itwp,wdy,Fobs,
     2                            Fcalc,SigFObs,pom,pom1,pom2,pom3
            enddo
          else
            write(83,format83e)(ihread(i),i=1,MaxNDim),FCalcQ,FObsQ,
     1                          sFCalcQ,FlagObs,itwr,wdy,Fobs,Fcalc,
     2                          SigFObs,pom,
     3                          sqrt(act(1,1)**2+bct(1,1)**2),
     4                          act(1,1),bct(1,1)
          endif
        endif
      endif
3150  if(nic.or.ranover) then
        Fcalc=0.
        wdy=0.
        a=0.
        b=0.
        dyp=0.
        if(ranover) then
          kspec='r'
        else
          kspec='n'
        endif
      endif
      if(ExistMagnetic) then
        if(ExistMagPol) then
          write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,
     1      dyp,FCalcNucl**2,FCalcMag**2,FCalcMagPol,wt,wdy,noread,
     2      nspec,kspec,sinthl,extkor,extkorm,iq,itwr,1
        else
          write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,a,b,
     1      dyp,sigyos,wt,wdy,noread,nspec,kspec,sinthl,extkor,extkorm,
     2      iq,itwr,1
        endif
      else
        if(ExistMagPol) then
          write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,
     1      dyp,FCalcNucl**2,FCalcMag**2,FCalcMagPol,wt,wdy,noread,
     2      nspec,kspec,sinthl,extkor,iq,itwr,1
        else
          write(LstRef,format3p)(ihread(i),i=1,maxNDim),Fobs,Fcalc,a,b,
     1      dyp,sigyos,wt,wdy,noread,nspec,kspec,sinthl,extkor,iq,itwr,1
        endif
      endif
      if((Ntwin.gt.1.or.iover.gt.0).and.twdetail.gt.0.and.
     1    kspec.ne.'r'.and.kspec.ne.'n') then
        if(iover.le.0) then
          mk=1
        else
          mk=2
        endif
        do m=1,mk
          do i=1,NTwin
            write(t80,format3)(ihitw(k,i,m),k=1,maxNDim),
     1        sqrt(yct(i,m)**2+yctm(i,m)**2),act(i,m),bct(i,m)
            j=4*maxNDim
            ven=' '
            if(ihitw(1,i,m).gt.900) then
              do k=4,j,4
                ven(k:k)='-'
              enddo
            else
              ven(:j)=t80(:j)
            endif
            ven(j+11:)=t80(j+1:)
            ven='t'//ven(:idel(Ven))
            if(HKLF5File.and.ihitw(1,i,m).le.900) then
              write(Cislo,'(f9.6)') snt(i)
              Ven(idel(Ven)+40:)=Cislo(:idel(Cislo))
            endif
            write(LstRef,FormA) ven(:idel(Ven))
          enddo
        enddo
      endif
      go to 1300
4000  if(nFlowChart.gt.0) call FeFlowChartEvent(noread,ie)
      if(isimul.gt.0) go to 9999
      if(OrthoOrd.gt.0) call trortho(1)
      GOFAll(ICyklMod6,KDatBlock)=wRNumAll(1)
      nPar=nParData(KDatBlock)
      do i=1,NPhase
        nPar=nPar+NParStr(i)
      enddo
      if(nRAll(1)-nPar.gt.1) then
        GOFAll(ICyklMod6,KDatBlock)=
     1    sqrt(GOFAll(ICyklMod6,KDatBlock)/float(nRAll(1)-nPar-1))
      else
        GOFAll(ICyklMod6,KDatBlock)=-0.01
      endif
      GOFObs(ICyklMod6,KDatBlock)=wRNumObs(1)
      if(nRObs(1)-nPar.gt.1) then
        GOFObs(ICyklMod6,KDatBlock)=
     1    sqrt(GOFObs(ICyklMod6,KDatBlock)/float(nRObs(1)-nPar-1))
      else
        GOFObs(ICyklMod6,KDatBlock)=-0.01
      endif
      if(NAtCalc.le.0.or.DoLeBail.ne.0) go to 9200
      if(useunobs.eq.1) then
        n=nRAll(1)
      else
        n=NRef90(KDatBlock)
      endif
      write(Cislo,FormI15) n
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(12,17),Cislo
      write(Cislo,FormI15) nRObs(1)
      call Zhusti(Cislo)
      write(LnSum,FormCIF) CifKey(10,17),Cislo
      go to 9999
9000  ErrFlag=1
9100  ich=1
      if(OrthoOrd.gt.0) call trortho(1)
      go to 9999
9200  ich=2
      go to 9999
9300  ich=3
9999  if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
      if(allocated(DerOv)) deallocate(DerOv)
      if(allocated(DerLam)) deallocate(DerLam)
      LamAve(KDatBlock)=LamAveOld
      return
100   format(a,1x,a)
103   format(i5)
104   format(f8.3)
115   format(a1,'factorsp: ',3f8.4,3i8,f10.3)
      end
