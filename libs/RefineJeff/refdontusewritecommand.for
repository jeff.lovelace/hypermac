      subroutine RefDontuseWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/DontuseQuest/ nEdwGroup,nEdwDontuse,nEdwExcept,nEdwScale,
     1                     nCrwDontUse,GroupString,DontuseString,
     2                     ExceptString,Dontuse,NScale,Klic
      character*(*) Command
      character*256 EdwStringQuest
      character*80  ErrString
      character*40  GroupString,DontuseString,ExceptString
      logical Dontuse,CrwLogicQuest
      save /DontuseQuest/
      ich=0
      if(Klic.eq.0) then
        Dontuse=CrwLogicQuest(nCrwDontUse)
        if(Dontuse) then
          Command='dontuse'
        else
          Command='useonly'
        endif
      else if(Klic.eq.1) then
        Command='scale'
      else if(Klic.eq.2) then
        Command='rfactors'
      endif
      if(Klic.eq.1) then
        call FeQuestIntFromEdw(nEdwScale,NScale)
        write(Cislo,FormI15) NScale
        call zhusti(Cislo)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))//' for'
      endif
      GroupString=EdwStringQuest(nEdwGroup)
      if(GroupString.eq.' ') go to 9100
      call TestGInd(GroupString,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command)+1)//GroupString(:idel(GroupString))
      DontuseString=EdwStringQuest(nEdwDontuse)
      if(DontuseString.eq.' ') go to 9100
      call TestCInd(DontuseString,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command))//' : '//
     1        DontuseString(:idel(DontuseString))
      ExceptString=EdwStringQuest(nEdwExcept)
      if(ExceptString.eq.' ') go to 9999
      call TestCInd(ExceptString,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command))//' except '//
     1        ExceptString(:idel(ExceptString))
      go to 9999
9000  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
