      subroutine GetDFormF(ff,n,x,f,df)
      include 'fepc.cmn'
      dimension ff(n)
      dpt=20.
      pt=x*dpt+1.
      ipt=ifix(pt)-1
      f=0.
      df=0.
      do k=1,4
        pom=1.
        dpom=0.
        do l=1,4
          if(k.ne.l) then
            pomj=1./float(k-l)
            pomp=(pt-float(ipt+l))*pomj
            dpom=dpom*pomp+pom*dpt*pomj
            pom=pom*pomp
          endif
        enddo
        l=k+ipt
        if(l.gt.0) then
          if(l.le.n) then
            ffp=ff(l)
          else
            ffp=ff(n)
          endif
        else
          ffp=ff(1)
        endif
        f=f+pom*ffp
        df=df+dpom*ffp
      enddo
      return
      end
