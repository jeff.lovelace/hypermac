      subroutine vazba(veta)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension xp(1)
      character*(*) veta
      character*80  Clen
      character*20 ppap
      character*2  ch2
      character*1  zn1(2),zn2(1),zn3(14),zn4(5),Ch1
      data zn1/'[','='/,zn2/']'/,zn3/'+','-','.','0','1','2','3','4',
     1     '5','6','7','8','9','/'/,zn4/'[','+','-',' ','*'/
      lenVeta=len(Veta)
      idlVeta=idel(Veta)
      neq=neq+1
      kk=0
      call zhusti(Veta)
      call mala(Veta)
      call kusz(veta,kk,lpa(neq),zn1,2,n,.true.,0)
      if(kk.eq.lenVeta) go to 9000
      lnp(neq)=ktera(lpa(neq))
      lnpo(neq)=0
      if(lnp(neq).eq.0) then
        lnp(neq)=-kterasc(lpa(neq))-100
        if(lnp(neq).eq.-100) go to 9000
      endif
      if(n.eq.1) then
        kk=kk+1
        call kusz(veta,kk,lat(neq),zn2,1,n,.true.,0)
        call uprat(lat(neq))
        if(kk.eq.lenVeta) go to 9000
        kk=kk+1
      else
        lat(neq)=' '
      endif
      kk=kk+1
      if(veta(kk:kk).ne.'=') go to 9000
      pab(neq)=0.
      i=0
2000  if(i.ge.mxep) then
        npa(neq)=i
        call ReallocEquations(mxe,mxep+10)
      endif
      i=i+1
      if(kk.gt.lenVeta-1) go to 9100
      idv=idel(veta(kk+1:))
      if(idv.eq.0) go to 9100
      Clen=' '
      Cinitel=1.
      j=0
      izav=0
      do k=kk+1,idel(Veta)
        ch1=Veta(k:k)
        ch2=Veta(k:k+1)
        if(ch1.eq.zn1(1)) izav=izav+1
        if(ch1.eq.zn2(1)) izav=izav-1
        if(j.ne.0.and.izav.eq.0.and.
     1     ((ch1.eq.zn3(1).and.(ch2.ne.'+['.and.ch2.ne.'+#')).or.
     2      (ch1.eq.zn3(2).and.(ch2.ne.'-['.and.ch2.ne.'-#'.and.
     3       Veta(k:k+5).ne.'-slope')))) go to 3110
        if(ch1.ne.' ') then
          kk=k
          j=j+1
          Clen(j:j)=ch1
        endif
      enddo
      kk=lenVeta
3110  idClen=j
      k=0
      j=0
3200  if(j.ne.0.and.Clen(k+1:k+1).eq.'*') k=k+1
      if(k.ge.idClen) then
        if(j.lt.1000) then
          pab(neq)=pab(neq)+Cinitel
          i=i-1
        else
          pko(i,neq)=Cinitel
        endif
        go to 2000
      endif
      j=j+1
      call kusz(Clen,k,Cislo,zn3,14,n,.false.,3)
      id=idel(Cislo)
      if(id.eq.0) then
        if(i.ne.1.and.j.eq.1) then
          go to 9000
        else
          go to 3300
        endif
      else if(id.eq.1.and.
     1        (Cislo(1:1).eq.zn3(2).or.Cislo(1:1).eq.zn3(1))) then
        if(Cislo(1:1).eq.zn3(2)) Cinitel=-Cinitel
      else
        kkk=0
        call StToReal(Cislo,kkk,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        Cinitel=Cinitel*xp(1)
      endif
      go to 3200
3300  if(k.lt.idClen) then
        if(k.gt.0) then
          Ch1=Clen(k:k)
        else
          Ch1='n'
        endif
        if(Ch1.eq.'*'.or.(Clen(k+1:k+1).ne.zn3(1).and.
     1     Clen(k+1:k+1).ne.zn3(2).or.Clen(k+1:k+1).ne.' ')) then
          call kusz(Clen,k,ppap,zn4,5,n,.true.,0)
          if(Clen(k+1:k+2).eq.'+['.or.Clen(k+1:k+2).eq.'-[') then
            ppap=ppap(:idel(ppap))//Clen(k+1:k+1)
            k=k+1
            n=1
          else if(Clen(k+1:k+2).eq.'+#'.or.Clen(k+1:k+2).eq.'-#') then
            ppap=ppap(:idel(ppap))//Clen(k+1:k+2)
            k=k+2
            call kusz(Clen,k,Cislo,zn4,5,n,.true.,0)
            ppap=ppap(:idel(ppap))//Cislo(:idel(Cislo))
            n=1
          else if(Clen(k+1:k+6).eq.'-slope') then
            ppap=ppap(:idel(ppap))//Clen(k+1:k+6)
            k=k+6
            call kusz(Clen,k,Cislo,zn4,5,n,.true.,0)
            ppap=ppap(:idel(ppap))//Cislo(:idel(Cislo))
            n=1
          endif
          if(idel(ppap).le.0) go to 9000
          pnp(i,neq)=ktera(ppap)
          if(pnp(i,neq).eq.0) then
            pnp(i,neq)=-kterasc(ppap)-100
            if(pnp(i,neq).eq.-100) then
              pom=RefSmbConst(ppap,ich)
              Cinitel=Cinitel*pom
              if(ich.eq.0) then
                j=j+1
                go to 3200
              else
                go to 9000
              endif
            else
              ppa(i,neq)=ppap
            endif
          else
            ppa(i,neq)=ppap
          endif
          if(n.eq.1) then
            k=k+1
            call kusz(Clen,k,pat(i,neq),zn2,1,n,.true.,0)
            call uprat(pat(i,neq))
            k=k+1
          else
            pat(i,neq)=' '
          endif
          j=j+1000
        endif
      endif
      go to 3200
9000  neq=neq-1
      go to 9999
9100  npa(neq)=i-1
9999  return
      end
