      subroutine dftcor(w,delta,a,b,corfac,a0r,a1r,a2r,a3r,a0i,a1i,a2i,
     1                  a3i,ar)
      include 'fepc.cmn'
      double precision cth,ctth,spth2,sth,sth4i,stth,th,th2,th4,
     1                 tmth2,tth4i
      th=abs(w)*delta
      if(a.ge.b.or.th.lt.0.d0.or.th.gt.3.1416d0)
     1    pause 'bad arguments to dftcor'
      if(abs(th).lt.5.d-2) then
        th2=th**2
        th4=th2*th2
        th6=th4*th2
        corfac=1.-(11./720.)*th4+(23./15120.)*th6
        ar=1./3.+th2/45.-8./945.*th4+11./14175.*th6
        a0r=(-2./3.)+th2/45.+(103./15120.)*th4-(169./226800.)*th6
        a1r=(7./24.)-(7./180.)*th2+(5./3456.)*th4-(7./259200.)*th6
        a2r=(-1./6.)+th2/45.-(5./6048.)*th4+th6/64800.
        a3r=(1./24.)-th2/180.+(5./24192.)*th4-th6/259200.
        a0i=th*(2./45.+(2./105.)*th2-(8./2835.)*th4+(86./467775.)*th6)
        a1i=th*(7./72.-th2/168.+(11./72576.)*th4-(13./5987520.)*th6)
        a2i=th*(-7./90.+th2/210.-(11./90720.)*th4+(13./7484400.)*th6)
        a3i=th*(7./360.-th2/840.+(11./362880.)*th4-(13./29937600.)*th6)
      else
        cth=cos(th)
        sth=sin(th)
        ctth=cth**2-sth**2
        stth=2.d0*sth*cth
        th2=th**2
        th4=th2*th2
        tmth2=3.d0-th2
        spth2=6.d0+th2
        sth4i=1./(6.d0*th4)
        tth4i=2.d0*sth4i
        corfac=tth4i*spth2*(3.d0-4.d0*cth+ctth)
        ar= sth4i*(-6.d0+11.d0*th2+spth2*ctth)
        a0r=sth4i*(-42.d0+5.d0*th2+spth2*(8.d0*cth-ctth))
        a0i=sth4i*(th*(-12d0+6.d0*th2)+spth2*stth)
        a1r=sth4i*(14.d0*tmth2-7.d0*spth2*cth)
        a1i=sth4i*(30.d0*th-5.d0*spth2*sth)
        a2r=tth4i*(-4.d0*tmth2+2.d0*spth2*cth)
        a2i=tth4i*(-12.d0*th+2.d0*spth2*sth)
        a3r=sth4i*(2.d0*tmth2-spth2*cth)
        a3i=sth4i*(6.d0*th-spth2*sth)
      endif
      return
      end
