      subroutine DSetAll
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical BezMolekul
      BezMolekul=.false.
      go to 1100
      entry DSetAllBezMolekul
      BezMolekul=.true.
1100  KPhaseIn=KPhase
      if(NAtXYZMode.gt.0) call DSetXYZMode
      if(MagneticType(KPhase).ne.0) then
        if(NAtMagMode.gt.0) then
          call DSetMagMode
        else
          call DSetMag
        endif
      endif
      if(KCommenMax.gt.0.and.ngcMax.gt.0) call dsetgc
      if(neq.gt.0) call dsete(1)
      if(OrthoOrd.gt.0) call dsetor
      if(NMolec.gt.0.and..not.BezMolekul) call dsetmol
      if(nKeep.gt.0) call RefDSetKeep
      if(ChargeDensities.and.lasmaxm.gt.0.and.iaute.eq.1)
     1  call RefPopvDSet
      if(nvai.gt.0) call DSetResAtMol
      if(neq.gt.0) call dsete(0)
      KPhase=KPhaseIn
      return
      end
