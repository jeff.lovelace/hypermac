      subroutine RefDeallocate(CIFAllocated,Klic)
      use Basic_mod
      use Refine_mod
      use Atoms_mod
      use Bessel_mod
      use RefPowder_mod
      use Powder_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical CIFAllocated
      if(allocated(RNumObs))
     1  deallocate(yct,ycq,act,bct,afour,bfour,snt,yctm,ycqm,yctmp,
     2             ycqmp,F000,F000N)
      if(allocated(RNumObs))
     1  deallocate(RNumObs, RNumAll, RDenObs, RDenAll,
     2            wRNumObs,wRNumAll,wRDenObs,wRDenAll,
     3             RFacObs,RFacAll,wRFacObs,wRFacAll,
     4             RIFacObs,RIFacAll,wRIFacObs,wRIFacAll,
     5             RFacProf,wRFacProf,cRFacProf,wcRFacProf,
     6            eRFacProf,GOFProf,GOFObs,GOFAll,nRObs,nRAll,
     7            nRFacObs,nRFacAll,nRIFacObs,nRIFacAll,nRFacProf,
     8            NSkipRef,NBadOverRef,UseDatBlockActual)
      if(Allocated(ParSup))
     1  deallocate(ParSup,snw,csw,skfx1,skfx2,skfx3,PrvniParSup,
     2             DelkaParSup,x4low,x4length,
     3             snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     4             sncospsc,cscospsc)
      if(Allocated(XGauss)) deallocate(XGauss,WGauss)
      if(Allocated(LSMat))
     1  deallocate(LSMat,LSRS,LSSol,par,ader,bder,der,dc,NSingArr,dertw)
      if(Allocated(NParStr))
     1  deallocate(NParStr,NParData,NParPwd,NParPwdAll,NParStrAll)
      if(Allocated(aderm))
     1  deallocate(aderm,bderm,derm,dcm,dertwm,derpol)
      if(Allocated(rh))
     1  deallocate(rh,rk,rl,ath,atk,atl,t,imnp,HCoef,trezm)
      if(allocated(besp))
     1  deallocate(besp,dbfdu1,dbfdu2,dchidu1,dchidu2,dsdu1,dsdu2,chi,
     2             dadu1,dadu2,dbdu1,dbdu2,dsdchi,sb,dsbdax,dsbday,ksi,
     3             dksidax,dksiday,dadax,daday,dbdax,dbday,besb,dbfdb1,
     4             dbfdb2,detadb1,detadb2,dsdb1,dsdb2,eta,dadb1,dadb2,
     5             dbdb1,dbdb2,dsdeta,dsdb3,dsdu3,dbfdb3,dbfdu3,daxddlt,
     6             dayddlt,daxdx40,daydx40,daxdfct,daydfct,mx,mxs,mxkk,
     7             mb,mbs,mbk,mxdkw1,mxdkw2,mxdkw3,mbdkw1,mbdkw2,mbdkw3,
     8             accur)
      if(allocated(sngc)) deallocate(sngc,csgc)
      if(allocated(snls)) deallocate(snls,csls)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      if(allocated(YoPwd).and.Klic.eq.0)
     1  deallocate(XPwd,YoPwd,YcPwd,YbPwd,YsPwd,YfPwd,YiPwd,YcPwdInd)
      if(allocated(dori)) deallocate(dori)
      if(allocated(isaRef)) deallocate(isaRef,ismRef,fyr)
      if(allocated(AtDisable)) deallocate(AtDisable,AtDisableFact)
      if(allocated(RMagDer)) deallocate(RMagDer)
      if(.not.CIFAllocated.and.allocated(CifKey))
     1  deallocate(CifKey,CifKeyFlag)
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      if(allocated(ki)) deallocate(ki)
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      if(allocated(CentrRef))
     1  deallocate(CentrRef,NSymmRef,NCSymmRef,ZCSymmRef)
      if(allocated(ExtTensor))
     1  deallocate(ExtTensor,ExtType,ExtDistr,ExtRadius)
      if(ChargeDensities.and.iaute.gt.0.and.lasmaxm.gt.0)
     1  call RefPopvEpilog
      call RefFixOriginEpilog
      if(allocated(IHED)) deallocate(IHED,FlagED,ADerED,BDerED,
     1                               IDerED,ACalcED,BCalcED,
     2                               SinThLED,IObsED,SIObsED,
     3                               FCalcED,ExcitErrED)
      if(allocated(ScRes)) deallocate(ScRes)
      if(allocated(MagneticRFac)) deallocate(MagneticRFac)
      if(allocated(RNumZoneObs))
     1   deallocate( RNumZoneObs, RNumZoneAll,
     2               RDenZoneObs, RDenZoneAll,
     3              wRNumZoneObs,wRNumZoneAll,
     4              wRDenZoneObs,wRDenZoneAll,
     5                 nRZoneAll,   nRZoneObs)
      return
      end
