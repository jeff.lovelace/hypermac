      subroutine RefEquation
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/EquationQuest/ nCrwAtomic,nCrwApplyToOrtho,nEdwAtoms,
     1  nEdwEquation,AtomicEquation,ApplyToOrthoWaves,AtomString,
     2  EquationString
      character*256 AtomString,EquationString
      character*80 Veta
      integer WhatHappened
      logical   CrwLogicQuest,AtomicEquation,ApplyToOrthoWaves
      external RefEquationReadCommand,RefEquationWriteCommand,FeVoid
      save /EquationQuest/
      xqd=500.
      il=3
      call RepeatCommandsProlog(id,fln(:ifln)//'_equation.tmp',xqd,il,
     1                          ilp,OKForBasicFiles)
      il=ilp+1
      xpom=5.
      tpom=xpom+CrwXd+10.
      Veta='%Atom equation'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwAtomic=CrwLastMade
      tpom=tpom+FeTxLengthUnder(Veta)+10.
      Veta='%for ->'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      il=il+1
      tpom=5.
      Veta='%Equation'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'%Equation','L',dpom,EdwYd,
     1                    0)
      nEdwEquation=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
      if(OrthoOrd.gt.0) then
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+10.
        Veta='A%pply equation to orthogonalized waves, as they are on'//
     1       ' M40'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwApplyToOrtho=CrwLastMade
      else
        nCrwApplyToOrtho=0
      endif
1300  EquationString=' '
      AtomString=' '
      AtomicEquation=.false.
      ApplyToOrthoWaves=.true.
1350  if(AtomicEquation) then
        call FeQuestStringEdwOpen(nEdwAtoms,AtomString)
      else
        call FeQuestEdwClose(nEdwAtoms)
      endif
      call FeQuestStringEdwOpen(nEdwEquation,EquationString)
      call FeQuestCrwOpen(nCrwAtomic,AtomicEquation)
      if(OrthoOrd.gt.0)
     1  call FeQuestCrwOpen(nCrwApplyToOrtho,ApplyToOrthoWaves)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  RefEquationReadCommand,RefEquationWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAtomic) then
        AtomicEquation=CrwLogicQuest(nCrwAtomic)
        go to 1350
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
