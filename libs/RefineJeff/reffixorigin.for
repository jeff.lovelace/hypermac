      subroutine RefFixOrigin
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension KiUse(:,:,:),OUse(:,:,:),NUse(:,:),rx(:,:,:),px(3),
     1          XSumaUse(:,:,:),nx(:),NFix(:)
      double precision SumaUse(:,:),SSumaUse(:,:),WSumaUse(:,:)
      logical XRayUsed
      allocatable KiUse,OUse,NUse,SumaUse,SSumaUse,WSumaUse,XSumaUse,
     1            nx,rx,NFix
      save KiUse,OUse,NUse,SumaUse,SSumaUse,WSumaUse,XSumaUse,NFix,
     1     XRayUsed,Ktery,nx,rx
      entry RefFixOriginReset
      if(allocated(KiUse)) deallocate(KiUse,OUse,NUse,SumaUse,SSumaUse,
     1                                WSumaUse,XSumaUse,rx,nx,NFix)
      n=3*(NAtCalc+NMolec)
      allocate(KiUse(n,3,NPhase),OUse(n,3,NPhase),XSumaUse(n,3,NPhase),
     1         NUse(3,NPhase),SumaUse(3,NPhase),SSumaUse(3,NPhase),
     2         WSumaUse(3,NPhase),rx(3,3,NPhase),nx(NPhase),
     3         NFix(NPhase))
      NFix=0
      NUse=0
      XRayUsed=.false.
      do KDatB=1,NDatBlock
        if(Radiation(KDatB).eq.XRayRadiation) then
          XRayUsed=.true.
          exit
        endif
      enddo
      go to 9999
      entry RefFixOriginProlog(KteryIn,isw)
      Ktery=KteryIn
      go to 1050
      entry RefFixOriginPrologCorrect(isw)
1050  call SetRealArrayTo(px,3,0.)
      call SetRealArrayTo(rx(1,1,KPhase),9,0.)
      do is=1,NSymm(KPhase)
        k=0
        do j=1,3
          do i=1,3
            k=k+1
            rx(i,j,KPhase)=rx(i,j,KPhase)+rm(k,is,isw,KPhase)
          enddo
        enddo
      enddo
      call triangl(rx(1,1,KPhase),px,3,3,nx(KPhase))
      nfp=0
      do 1200kk=1,nx(KPhase)
        do k=1,3
          if(abs(rx(kk,k,KPhase)).gt..00001) exit
        enddo
        if(ktery.gt.0) then
          if(KiA(k+1,Ktery).ne.0) then
            NConstrain=NConstrain+1
            KiA(k+1,Ktery)=0
          endif
        else if(ktery.lt.0) then
          if(KiMol(k+4,-Ktery).ne.0) then
            NConstrain=NConstrain+1
            KiMol(k+4,-Ktery)=0
          endif
        else
          do i=1,NAtXYZMode
            do j=1,NMAtXYZMode(i)
              kt=KAtXYZMode(j,i)
              pom=0.
              do m=k,3*MAtXYZMode(i),3
                pom=pom+XYZAMode(m,kt)
              enddo
              if(abs(pom).gt..00001) then
                NConstrain=NConstrain+1
                KiAtXYZMode(j,i)=0
                go to 1200
              endif
            enddo
          enddo
          do i=1,NAtMagMode
            do j=1,NMAtMagMode(i)
              kt=KAtMagMode(j,i)
              pom=0.
              do m=k,3*MAtMagMode(i),3
                pom=pom+MagAMode(m,kt)
              enddo
              if(abs(pom).gt..00001) then
                NConstrain=NConstrain+1
                KiAtMagMode(j,i)=0
                go to 1200
              endif
            enddo
          enddo
          nfp=nfp+1
          NConstrain=NConstrain+1
           SumaUse(nfp,KPhase)=0.
          SSumaUse(nfp,KPhase)=0.
          WSumaUse(nfp,KPhase)=0.
          n=0
          do i=1,NAtInd
            if(iswa(i).ne.isw.or.kswa(i).ne.KPhase) cycle
            if(XRayUsed) then
              wt=AtNum(isf(i),KPhase)**2
            else
              wt=1.
            endif
            do k=1,3
              n=n+1
              OUse(n,nfp,KPhase)=wt*rx(kk,k,KPhase)
              KiUse(n,nfp,KPhase)=PrvniKiAtomu(i)+k
              XSumaUse(n,nfp,KPhase)=x(k,i)
               SumaUse(nfp,KPhase)= SumaUse(nfp,KPhase)+
     1                                OUse(n,nfp,KPhase)* x(k,i)
              SSumaUse(nfp,KPhase)=SSumaUse(nfp,KPhase)+
     1                               (OUse(n,nfp,KPhase)*sx(k,i))**2
              WSumaUse(nfp,KPhase)=WSumaUse(nfp,KPhase)+
     1                               (OUse(n,nfp,KPhase)*.0001)**2
            enddo
          enddo
          iak=NAtMolFrAll(KPhase)-1
          do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
            iap=iak+1
            iak=iak+iam(im)
            if(iswmol(im).ne.isw) cycle
            wt=0.
            do ia=iap,iak
              if(XRayUsed) then
                wt=wt+AtNum(isf(ia),KPhase)**2
              else
                wt=wt+1.
              endif
            enddo
            do ip=1,mam(im)
              ji=ip+mxp*(im-1)
              do k=1,3
                n=n+1
                OUse(n,nfp,KPhase)=wt*rx(kk,k,KPhase)
                KiUse(n,nfp,KPhase)=PrvniKiMolekuly(ji)+k+3
                XSumaUse(n,nfp,KPhase)=trans(k,ji)
                 SumaUse(nfp,KPhase)=SumaUse(nfp,KPhase)+
     1                                 OUse(n,nfp,KPhase)*trans(k,ji)
                SSumaUse(nfp,KPhase)=SSumaUse(nfp,KPhase)+
     1                         (OUse(n,nfp,KPhase)*strans(k,ji))**2
                WSumaUse(nfp,KPhase)=WSumaUse(nfp,KPhase)+
     1                                 (OUse(n,nfp,KPhase)*.0001)**2
              enddo
            enddo
          enddo
          NUse(nfp,KPhase)=n
        endif
        NFix(KPhase)=nfp
1200  continue

      go to 9999
      entry RefFixOriginSuma
      do KPh=1,NPhase
        do i=1,NFix(KPh)
          if(NUse(i,KPh).le.0) cycle
          call SetRealArrayTo(der,ubound(der,1),0.)
c          wt=(GOFOverall/.001)**2
c          wt=(GOFOverall/.01)**2
c          wt=GOFOverall**2
c   zkusenost - nesmi to byt prilis silne vazeno podminka - > numericke problemy
          if(SSumaUse(NFix(KPh),KPh).gt.0.) then
            wt=10000./SSumaUse(NFix(KPh),KPh)
          else if(WSumaUse(NFix(KPh),KPh).gt.0.) then
            wt=10000./WSumaUse(NFix(KPh),KPh)
          else
            cycle
          endif
          do j=1,NUse(i,KPh)
            der(KiUse(j,i,KPh))=OUse(j,i,KPh)
          enddo
          dy=0.
          call DSetAllBezMolekul
          call SumaRefine
        enddo
      enddo
      go to 9999
      entry RefFixOriginRenorm
      do KPh=1,NPhase
        ifix=0
        if(NFix(KPh).le.0) cycle
        do i=1,NFix(KPh)
          do j=1,NUse(i,KPh)
            if(NUse(i,KPh).gt.0) go to 2100
          enddo
        enddo
        cycle
2100    do kk=1,nx(KPh)
          do k=1,3
            if(abs(rx(kk,k,KPh)).gt..00001) exit
          enddo
          pom=0.
          pomd=0.
          n=0
          ifix=ifix+1
          do i=1,NAtInd
            if(iswa(i).ne.isw.or.kswa(i).ne.KPh) cycle
            do k=1,3
              n=n+1
              pom=pom+OUse(n,ifix,KPh)*(XSumaUse(n,ifix,KPh)-x(k,i))
              if(abs(OUse(n,ifix,KPh)).gt.0.) pomd=pomd+1.
            enddo
          enddo
          iak=NAtMolFrAll(KPh)-1
          do im=NMolecFrAll(KPh),NMolecToAll(KPh)
            iap=iak+1
            iak=iak+iam(im)
            if(iswmol(im).ne.isw) cycle
            wt=0.
            do ia=iap,iak
              if(XRayUsed) then
                wt=wt+AtNum(isf(ia),KPh)**2
              else
                wt=wt+1.
              endif
            enddo
            do ip=1,mam(im)
              ji=ip+mxp*(im-1)
              do k=1,3
                n=n+1
                pom=pom+OUse(n,ifix,KPh)*
     1                  (XSumaUse(n,ifix,KPh)-trans(k,ji))
                if(abs(OUse(n,ifix,KPh)).gt.0.) pomd=pomd+1.
              enddo
            enddo
          enddo
          if(pomd.ne.0.) then
            Diff=pom/pomd
          else
            Diff=0.
          endif
          n=0
          do i=1,NAtInd
            if(iswa(i).ne.isw.or.kswa(i).ne.KPh) cycle
            do k=1,3
              n=n+1
              if(abs(OUse(n,ifix,KPh)).gt.0.)
     1          x(k,i)=x(k,i)+Diff/OUse(n,ifix,KPh)
            enddo
          enddo
          iak=NAtMolFrAll(KPh)-1
          do im=NMolecFrAll(KPh),NMolecToAll(KPh)
            iap=iak+1
            iak=iak+iam(im)
            if(iswmol(im).ne.isw) cycle
            do ip=1,mam(im)
              ji=ip+mxp*(im-1)
              do k=1,3
                n=n+1
                if(abs(OUse(n,ifix,KPh)).gt.0.)
     1            trans(k,ji)=trans(k,ji)+Diff/OUse(n,ifix,KPh)
              enddo
            enddo
          enddo
        enddo
      enddo
      go to 9999
      entry RefFixOriginEpilog
      if(allocated(KiUse)) deallocate(KiUse,OUse,NUse,SumaUse,SSumaUse,
     1                                WSumaUse,XSumaUse,rx,nx,NFix)
9999  return
      end
