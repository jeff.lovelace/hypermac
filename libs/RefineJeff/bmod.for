      subroutine bmod(f,df1,df2,df3,db,d1,d2,m,ityp)
      include 'fepc.cmn'
      include 'basic.cmn'
      if(ityp.eq.1) then
        pom=exp(db)
        if(m.eq.0) then
          f=d1*pom+d2*(pom-1.)/db-d1-d2+1.
          if(f.ne.0.) then
            df1=(d1*pom+d2*(pom*db-pom+1.)/db**2)/f
            df2=(pom-1.)/f
            df3=((pom-1.)/db-1.)/f
          else
            df1=0.
            df2=0.
            df3=0.
          endif
        else
          arg=pi2*float(m)*.5
          alfa1=arg*d1
          cs1=cos(alfa1)
          sn1=sin(alfa1)
          if(d2.ne.0.) then
            alfa2=arg*d2
            aq=alfa2**2
            cs12=cos(alfa1+alfa2)
            sn12=sin(alfa1+alfa2)
            bq=db**2
            ab=alfa2*db
            fcit=ab*pom*cs1-ab*cs12+bq*pom*sn1-bq*sn12
            fjmen=alfa2*(aq+bq)
            fjq=fjmen**2
            z=fcit/fjmen
            f=d2*z
            if(f.ne.0) then
              df1=d2*((pom*cs1*(ab+alfa2)-alfa2*cs12
     1            +2.*db*(pom*sn1-sn12))*fjmen-2.*ab*fcit)/(fjq*f)
              df2=(-ab*alfa2*pom*sn1+ab*alfa2*sn12+bq*pom*alfa2*cs1
     1            -bq*alfa2*cs12)/(fjmen*f)
              df3=z+((ab*pom*cs1-ab*cs12+alfa2*ab*sn12
     1             -bq*alfa2*cs12)*fjmen-(3.*aq+bq)*alfa2*fcit)/(fjq*f)
            else
              df1=0.
              df2=0.
              df3=0.
            endif
          else
            fcit=sn1/arg
            f=(pom-1.)*fcit
            if(f.ne.0) then
              df1=pom*fcit/f
              df2=(pom-1.)*cs1/f
            else
              df1=0.
              df2=0.
              df3=0.
            endif
          endif
        endif
      else if(ityp.eq.2) then
        if(db.ne.0.) then
          d=sqrt(abs(db))
          pom=sin(d)/d
          pomd=-(cos(d)*d-sin(d))/d**3*.5
        else
          pom=1.
          pomd=0.
        endif
        if(m.eq.0) then
          f=d1*(pom-1.)+1.
          df1=d1*pomd
          df2=pom-1.
        else
          arg=pi2*float(m)*.5
          alfa1=arg*d1
          cs1=cos(alfa1)
          sn1=sin(alfa1)
          fcit=sn1/arg
          f=(pom-1.)*fcit
          df1=pomd*fcit
          df2=(pom-1.)*cs1
          df3=0.
        endif
      endif
      return
      end
