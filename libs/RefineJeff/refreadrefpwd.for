      subroutine RefReadRefPwd(ich)
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ih(6)
      ich=0
      call OpenFile(91,fln(:ifln)//'.l0'//
     1  Cifry(KDatBlock+1:KDatBlock+1),'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      MxRefPwd=0
1100  read(91,format91pow,err=9900,end=1150) ih
      if(ih(1).gt.900) go to 1150
      MxRefPwd=MxRefPwd+1
      go to 1100
1150  if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
      l=NAlfa(KDatBlock)
      call PwdSetTOF(KDatBlock)
      allocate(ihPwdArr(maxNDim,MxRefPwd),FCalcPwdArr(MxRefPwd),
     1         MultPwdArr(MxRefPwd),iqPwdArr(MxRefPwd),
     2         KPhPwdArr(MxRefPwd),ypeaka(l,MxRefPwd),peaka(l,MxRefPwd),
     3         pcota(l,MxRefPwd),rdega(2,l,MxRefPwd),shifta(l,MxRefPwd),
     4         fnorma(l,MxRefPwd),tntsima(l,MxRefPwd),
     5         ntsima(l,MxRefPwd),sqsga(l,MxRefPwd),fwhma(l,MxRefPwd),
     6         sigpa(l,MxRefPwd),etaPwda(l,MxRefPwd),
     7         dedffga(l,MxRefPwd),dedffla(l,MxRefPwd),
     8         dfwdga(l,MxRefPwd),dfwdla(l,MxRefPwd),
     9         coef(m,l,MxRefPwd),coefp(m,l,MxRefPwd),
     a         coefq(2,l,MxRefPwd),Prof0(MxRefPwd),cotg2tha(l,MxRefPwd),
     1         cotgtha(l,MxRefPwd),cos2tha(l,MxRefPwd),
     2         cos2thqa(l,MxRefPwd),Alpha12a(l,MxRefPwd),
     3         Beta12a(l,MxRefPwd),IBroadHKLa(MxRefPwd))
      if(.not.isTOF.and..not.isED.and.
     1   KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2  allocate(AxDivProfA(-25:25,l,MxRefPwd),
     3          DerAxDivProfA(-25:25,7,l,MxRefPwd),
     4          FDSProf(-25:25,l,MxRefPwd))
      rewind 91
      n=0
1200  read(91,format91pow,err=9900,end=1250) ih,ri,fmult,iq,i,KPh
      if(ih(1).gt.900) go to 1250
      n=n+1
      call CopyVekI(ih,ihPwdArr(1,n),maxNDim)
      FCalcPwdArr(n)=ri
      MultPwdArr(n)=fmult
      iqPwdArr(n)=iq
      KPhPwdArr(n)=KPh
      go to 1200
1250  close(91)
      go to 9999
9900  ich=1
9999  return
      end
