      function CosBackg(B,derp,X,NBackg)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension derp(*),B(*)
      if(NBackg.le.0) then
        CosBackg=0.
        go to 9999
      endif
      CosBackg=B(1)
      derp(1)=1.
      do i=2,NBackg
        pom=cos(X*float(i-1))
        CosBackg=CosBackg+B(i)*pom
        derp(i)=pom
      enddo
9999  return
      end
