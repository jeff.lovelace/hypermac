      subroutine RefDontuse
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/DontuseQuest/ nEdwGroup,nEdwDontuse,nEdwExcept,nEdwScale,
     1                     nCrwDontUse,GroupString,DontuseString,
     2                     ExceptString,Dontuse,NScale,Klic
      character*256 Veta
      character*40 GroupString,DontuseString,ExceptString
      integer WhatHappened
      external RefDontuseReadCommand,RefDontuseWriteCommand,FeVoid
      logical Dontuse
      save /DontuseQuest/
      Klic=0
      go to 1100
      entry RefScale
      Klic=1
      go to 1100
      entry RefRFactors
      Klic=2
1100  xqd=500.
      i=7
      if(Klic.eq.0) then
        Veta=fln(:ifln)//'_dontuse.tmp'
      else if(Klic.eq.1) then
        Veta=fln(:ifln)//'_scale.tmp'
      else if(Klic.eq.2) then
        Veta=fln(:ifln)//'_rfactors.tmp'
      endif
      call RepeatCommandsProlog(id,Veta,xqd,i,il,OKForBasicFiles)
      ilp=il+1
      Veta='%Group of reflection'
      tpom=5.
      xpom=120.
      dpom=150.
      do i=1,3
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          Veta='Co%ndition'
          nEdwGroup=EdwLastMade
        else if(i.eq.2) then
          Veta='E%xcept condition'
          nEdwDontuse=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        else
          nEdwExcept=EdwLastMade
        endif
      enddo
      il=-10*il-10
      call FeFillTextInfo('reflcondition.txt',0)
      do i=1,NInfo
        call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
        il=il-6
      enddo
      il=ilp
      nEdwScale=0
      nCrwDontUse=0
      if(Klic.eq.0) then
        xpom=xpom+dpom+20.
        tpom=xpom+CrwgXd+20.
        Veta='D%ontuse command'
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                        1)
          if(i.eq.1) then
            Veta='Us%eonly command'
            nCrwDontUse=CrwLastMade
          endif
          il=il+1
        enddo
      else if(Klic.eq.1) then
        tpom=xpom+dpom+20.
        Veta='Sc%ale number'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwScale=EdwLastMade
      endif
1300  GroupString=' '
      do i=1,NDim(KPhase)
        GroupString(i:i)=indices(i)
      enddo
      DontuseString=' '
      ExceptString=' '
      Dontuse=.true.
1350  call FeQuestStringEdwOpen(nEdwGroup,GroupString)
      call FeQuestStringEdwOpen(nEdwDontuse,DontuseString)
      call FeQuestStringEdwOpen(nEdwExcept,ExceptString)
      if(Klic.eq.0) then
        nCrw=nCrwDontUse
        do i=1,2
          call FeQuestCrwOpen(nCrw,i.eq.1.eqv.Dontuse)
          nCrw=nCrw+1
        enddo
      else if(Klic.eq.1) then
        call FeQuestIntEdwOpen(nEdwScale,NScale,.false.)
      endif
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  RefDontuseReadCommand,RefDontuseWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
