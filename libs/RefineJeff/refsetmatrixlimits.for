      subroutine RefSetMatrixLimits(Poprve)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      character*12 pn
      character*20 at
      integer Poprve
      logical   EqIgCase
      dimension ip(1)
      LastNp=PosledniKiAtPos
      if(ngcMax.gt.0.and.NAtCalc.gt.0) then
        call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
        if(Poprve.eq.1) then
          if(allocated(isaRef)) deallocate(isaRef,ismRef,fyr)
          allocate(isaRef(MaxNSymm,NAtCalc),ismRef(NAtCalc),
     1             fyr(NAtCalc))
          if(allocated(AtDisable)) deallocate(AtDisable,AtDisableFact)
          allocate(AtDisable(NAtAll),AtDisableFact(NAtAll))
          call SetLogicalArrayTo(AtDisable,NAtAll,.false.)
          call SetRealArrayTo(AtDisableFact,NAtAll,1.)
          call RefSpecAt
        endif
        NAtCalc=NAtCalcBasic
      endif
      LastNp=PosledniKiAtMagMode
      if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1  LastNP=LastNP+NEDZone(KDatBlock)*MxEDRef
      if(Poprve.eq.1) then
        if(allocated(ki)) deallocate(ki,der,NSingArr,dc,dertw)
        allocate(ki(LastNp),der(LastNp),NSingArr(LastNp),dc(LastNp),
     1           dertw(LastNp))
      endif
      call SetIntArrayTo(ki,LastNp,0)
      k=0
      do i=1,NDatBlock
        do j=1,MxScAll
          k=k+1
          ki(k)=KiS(j,i)
        enddo
      enddo
      if(ExistPowder) call CopyVekI(KiPwd,Ki(NdOffPwd+1),MxParPwd)
      if(ExistElectronData.and.ExistM42.and.CalcDyn) then
        do KDatB=1,NDatBlock
          kip=NdOffED+(KDatB-1)*MxEDRef*NMaxEDZone+1
          call CopyVekI(KiED(1,1,KDatB),Ki(kip),NEDZone(KDatB)*MxEDRef)
        enddo
      endif
      do i=1,NAtInd
        call CopyVekI(KiA(1,i),Ki(PrvniKiAtomu(i)),DelkaKiAtomu(i))
      enddo
      do i=NAtMolFr(1,1),NAtAll
        call CopyVekI(KiA(1,i),Ki(PrvniKiAtomu(i)),DelkaKiAtomu(i))
      enddo
      if(ChargeDensities.and.lasmaxm.gt.0) then
        if(iaute.le.1) then
          if(Poprve.eq.1) then
            call RefPopvPrologEquation
          else
            if(iaute.eq.1) call RefPopvPrologContinue
          endif
        else if(iaute.le.2.and.Poprve.eq.1) then
          call RefPopvPrologRestrain
        endif
      endif
      do i=1,NMolec
        do j=1,mam(i)
          ia=j+(i-1)*mxp
          call CopyVekI(KiMol(1,ia),Ki(PrvniKiMolekuly(ia)),
     1                  DelkaKiMolekuly(ia))
        enddo
      enddo
      if(NAtXYZMode.gt.0) then
        j=PrvniKiAtXYZMode
        do i=1,NAtXYZMode
          call CopyVekI(KiAtXYZMode(1,i),Ki(j),NMAtXYZMode(i))
          j=j+NMAtXYZMode(i)
        enddo
      endif
      if(NAtMagMode.gt.0) then
        j=PrvniKiAtMagMode
        do i=1,NAtMagMode
          call CopyVekI(KiAtMagMode(1,i),Ki(j),NMAtMagMode(i))
          j=j+NMAtMagMode(i)
        enddo
      endif
      call SetRealArrayTo(der,  LastNp,0.)
      call SetRealArrayTo(dc,   LastNp,0.)
      call SetRealArrayTo(dertw,LastNp,0.)
      do i=1,NAtCalc
        MaxDerAtom=max(MaxDerAtom,DelkaKiAtomu(i))
      enddo
      MagneticTypeMax=0
      do i=1,NPhase
        MagneticTypeMax=max(MagneticTypeMax,MagneticType(i))
      enddo
      if(Poprve.eq.1) then
        if(allocated(par)) deallocate(par,ader,bder)
        allocate(par(MaxDerAtom),ader(MaxDerAtom),bder(MaxDerAtom))
      endif
      if(MagneticTypeMax.ne.0) then
        if(Poprve.eq.1) then
          if(allocated(aderm)) deallocate(aderm,bderm,derm,dcm,dertwm,
     1                                    derpol)
          allocate(aderm(3,MaxDerAtom),bderm(3,MaxDerAtom),
     1             derm(3,LastNp),dcm(3,LastNp),dertwm(LastNp),
     2             derpol(3,LastNp))
          n=0
          m=0
          do i=1,NAtCalc
            if(MagPar(i).gt.0.and.KUsePolar(i).gt.0) then
              n=n+1
              m=max(m,2*MagPar(i)-1)
            endif
          enddo
          if(allocated(RMagDer)) deallocate(RMagDer)
          if(n.gt.0) allocate(RMagDer(9,m,n))
        endif
        call SetRealArrayTo(derm,  3*LastNp,0.)
        call SetRealArrayTo(dcm,   3*LastNp,0.)
        call SetRealArrayTo(derpol,3*LastNp,0.)
        call SetRealArrayTo(dertwm,LastNp,0.)
      endif
      if(ExistPowder.and.(NAtCalc.gt.0.and.DoLeBail.ne.0)) then
        call SetIntArrayTo(ki(ndoff+1),PosledniKiAtPos-ndoff,0)
        if(NMolec.gt.0) then
          call SetIntArrayTo(ki(PrvniKiAtMol),
     1                      PosledniKiAtMol-PrvniKiAtMol+1,0)
          call SetIntArrayTo(ki(PrvniKiMol),
     1                      PosledniKiMol-PrvniKiMol+1,0)
        endif
        if(NAtXYZMode.gt.0) then
          j=PrvniKiAtXYZMode
          do i=1,NAtXYZMode
            call SetIntArrayTo(Ki(j),NMAtXYZMode(i),0)
            j=j+NMAtXYZMode(i)
          enddo
        endif
        if(NAtMagMode.gt.0) then
          j=PrvniKiAtMagMode
          do i=1,NAtMagMode
            call SetIntArrayTo(Ki(j),NMAtMagMode(i),0)
            j=j+NMAtMagMode(i)
          enddo
        endif
      endif
      j=0
      nLSMat=0
      nLSRS=0
      nParRef=0
      call SetIntArrayTo(nParStr,NPhase,0)
      call SetIntArrayTo(nParData,NDatBlock,0)
      call SetIntArrayTo(nParPwd(0,0),(NDatBlock+1)*(NPhase+1),0)
      npsPwd=0
      do i=1,ndOffPwd
        if(ki(i).ne.0) then
          nLSRS=nLSRS+1
          if(ki(i).lt.0) cycle
          call KdoCo(i,at,pn,0,pom,spom)
          if(EqIgCase(at(:5),'block')) then
            k=0
            call StToInt(at(6:),k,ip,1,.false.,ich)
            if(ich.eq.0) then
              KDatB=ip(1)
            else
              KDatB=1
            endif
          else
            KDatB=1
          endif
          nParRef=nParRef+1
          nParData(KDatB)=nParData(KDatB)+1
        endif
      enddo
      do i=ndOffPwd+1,ndOff
        if(ki(i).ne.0) then
          nLSRS=nLSRS+1
          if(ki(i).lt.0) cycle
          call KdoCo(i,at,pn,0,pom,spom)
          if(at.eq.' ') then
            KDatB=0
            KPh=0
            go to 7050
          endif
          k=index(at,'%')
          if(k.ne.0) then
            Cislo=at(:k-1)
            at=at(k+1:)
          else
            Cislo=at
          endif
          if(EqIgCase(at(:5),'block')) then
            k=0
            call StToInt(at(6:),k,ip,1,.false.,ich)
            if(ich.eq.0) then
              KDatB=ip(1)
            else
              KDatB=1
            endif
          else
            KDatB=0
          endif
          KPh=max(ktat(PhaseName,NPhase,Cislo),1)
7050      nParRef=nParRef+1
          nParPwd(KPh,KDatB)=nParPwd(KPh,KDatB)+1
        endif
      enddo
      do it=1,5
        if(it.eq.1) then
          i1=ndOff+1
          i2=PosledniKiAtInd
        else if(it.eq.2) then
          i1=PrvniKiAtMol
          i2=PosledniKiAtMol
        else if(it.eq.3) then
          i1=PrvniKiMol
          i2=PosledniKiMol
        else if(it.eq.4) then
          i1=PrvniKiAtXYZMode
          i2=PosledniKiAtXYZMode
        else
          i1=PrvniKiAtMagMode
          i2=PosledniKiAtMagMode
        endif
        do i=i1,i2
          if(ki(i).ne.0) then
            nLSRS=nLSRS+1
            if(ki(i).lt.0) cycle
            if(it.le.3) then
              call KdoCo(i,at,pn,0,pom,spom)
              ia=KtAtMol(at)
              if(ia.gt.0) then
                KPh=kswa(ia)
              else if(ia.lt.0.and.it.eq.3) then
                ia=(-ia-1)/mxp+1
                KPh=kswmol(ia)
              else
                KPh=1
              endif
            else
              KPh=1
            endif
            KPh=max(KPh,1)
            nParRef=nParRef+1
            nParStr(KPh)=nParStr(KPh)+1
          endif
        enddo
      enddo
      do KPh=1,NPhase
        do i=1,NKeepRigid(KPh)
          NConstrain=NConstrain-6
          nParRef=nParRef+6
          nParStr(KPh)=nParStr(KPh)+6
        enddo
      enddo
      do KDatB=1,NDatBlock
        NParPwdAll(KDatB)=NParPwd(0,0)+NParPwd(0,KDatB)
        NParStrAll(KDatB)=NParData(KDatB)
        do KPh=1,NPhase
          NParPwdAll(KDatB)=NParPwdAll(KDatB)+NParPwd(KPh,KDatB)+
     1                      NParPwd(KPh,0)
          NParStrAll(KDatB)=NParStrAll(KDatB)+NParStr(KPh)
        enddo
      enddo
      nLSMat=nLSRS*(nLSRS+1)/2
      if(Allocated(LSMat)) deallocate(LSMat,LSRS,LSSol)
      allocate(LSMat(nLSMat),LSRS(nLSRS),LSSol(nLSRS))
      return
      end
