      subroutine RefCalcRval(KPh,KDatB,ich)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension fp(:),sp(:),fpo(:),spo(:)
      character*256 t256,ven
      character*1 nspec,kspec
      logical poprve,EqIgCase,Piseme
      allocatable fp,sp,fpo,spo
      call RefWorstReflectionReset
      wdylim=0.
      MaxRef=10000
      allocate(fp(MaxRef),sp(MaxRef))
      ich=0
      KPhase=KPh
      KDatBlock=KDatB
      rewind LstRef
      if((Ntwin.gt.1.or.iover.gt.0).and.twdetail.gt.0) then
        if(iover.le.0) then
          mkp=NTwin
        else
          mkp=NTwin*2
        endif
      else
        mkp=0
      endif
1100  read(LstRef,FormA,end=9999) t256
      k=0
      call kus(t256,k,Cislo)
      if(EqIgCase(Cislo,DatBlockName(KDatB))) then
        call kus(t256,k,Cislo)
        if(.not.EqIgCase(Cislo,'begin')) go to 4000
      else
        go to 1100
      endif
      n=0
      poprve=.true.
      call RefRFacNuluj
      RINumObs=0.
      RINumAll=0.
      RIDenObs=0.
      RIDenAll=0.
      wRINumObs=0.
      wRINumAll=0.
      wRIDenObs=0.
      wRIDenAll=0.
      call SetIntArrayTo(ihmin,6, 9999)
      call SetIntArrayTo(ihmax,6,-9999)
1500  read(LstRef,FormA,end=4000) t256
      k=0
      call kus(t256,k,Cislo)
      if(EqIgCase(Cislo,DatBlockName(KDatB))) then
        call kus(t256,k,Cislo)
        if(EqIgCase(Cislo,'end')) then
          go to 4000
        else if(EqIgCase(Cislo,'begin')) then
          backspace LstRef
          go to 4000
        endif
      endif
      if(ExistMagnetic) then
        if(ExistMagPol) then
          read(t256,format3p,err=4000)(ihref(i,1),i=1,maxNDim),Fobs,
     1      Fcalc,dyp,pom1,pom2,pom3,wt,wdy,nn,nspec,kspec,sinthl,
     2      extkor,extkorm,iq,itwr,k
        else
          read(t256,format3p,err=4000)(ihref(i,1),i=1,maxNDim),Fobs,
     1      Fcalc,pom1,pom2,dyp,sigyo,wt,wdy,nn,nspec,kspec,sinthl,
     2      extkor,extkorm,iq,itwr,k
        endif
      else
        if(ExistMagPol) then
          read(t256,format3p,err=4000)(ihref(i,1),i=1,maxNDim),Fobs,
     1      Fcalc,dyp,pom1,pom2,pom3,wt,wdy,nn,nspec,kspec,sinthl,
     2      extkor,iq,itwr,k
        else
          read(t256,format3p,err=4000)(ihref(i,1),i=1,maxNDim),Fobs,
     1      Fcalc,pom1,pom2,dyp,sigyo,wt,wdy,nn,nspec,kspec,sinthl,
     2      extkor,iq,itwr,k
        endif
      endif
      IHRead(1:maxNDim)=ihref(1:maxNDim,1)
      if(k.ne.KPhase) go to 1500
      mmabsm=-999
      call RefLoopBefore(1,1,ich)
      if(ich.eq.1) go to 1500
      nulova=nspec.eq.'*'
      if(kspec.eq.'n') then
        write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs
        ven=ven(:idel(ven))//'  ... no contribution to this reflection'
     1      //' was found'
        go to 2000
      else if(kspec.eq.'r') then
        write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs
        ven=ven(:idel(ven))//'  ... incomplete overlap detected'
        go to 2000
      else
        if(ExistMagnetic) then
          if(ExistMagPol) then
            write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs,Fcalc,dyp,
     1        pom1,pom2,pom3,1./sqrt(wt),wdy,nn,nspec,kspec,sinthl,
     2        extkor,extkorm,iq
          else
            write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs,Fcalc,pom1,
     1        pom2,dyp,sigyo,1./sqrt(wt),wdy,nn,nspec,kspec,sinthl,
     2        extkor,extkorm,iq
          endif
        else
          if(ExistMagPol) then
            write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs,Fcalc,dyp,
     1        pom1,pom2,pom3,1./sqrt(wt),wdy,nn,nspec,kspec,sinthl,
     2        extkor,iq
          else
            write(ven,Format3)(ihref(i,1),i=1,maxNDim),Fobs,Fcalc,pom1,
     1         pom2,dyp,sigyo,1./sqrt(wt),wdy,nn,nspec,kspec,sinthl,
     2         extkor,iq
          endif
        endif
      endif
      if(isPowder) then
        if(nulova) then
          wtp=1./(2.*sigyo**2)
        else
          wtp=1./(2.*Fobs*sigyo)**2
        endif
        DI=abs(Fobs**2-Fcalc**2)
        RINumAll=RINumAll+DI
        RIDenAll=RIDenAll+Fobs**2
        wRINumAll=wRINumAll+wtp*DI**2
        wRIDenAll=wRIDenAll+wtp*Fobs**4
        if(.not.nulova) then
          RINumObs=RINumObs+DI
          RIDenObs=RIDenObs+Fobs**2
          wRINumObs=wRINumObs+wtp*DI**2
          wRIDenObs=wRIDenObs+wtp*Fobs**4
        endif
      endif
      ady=abs(dyp)
      ayo=abs(Fobs)
      wdyq=wdy**2
      if(ifsq.ne.1.or.isPowder) then
        wyoq=wt*Fobs**2
      else
        wyoq=wt*Fobs**4
      endif
      awdy=abs(wdy)
      if(.not.nulova) then
        n=n+1
        if(n.gt.MaxRef) then
          allocate(fpo(MaxRef),spo(MaxRef))
          call CopyVek(fp,fpo,MaxRef)
          call CopyVek(sp,spo,MaxRef)
          MaxRefOld=MaxRef
          MaxRef=MaxRef+10000
          deallocate(fp,sp)
          allocate(fp(MaxRef),sp(MaxRef))
          call CopyVek(fpo,fp,MaxRefOld)
          call CopyVek(spo,sp,MaxRefOld)
          deallocate(fpo,spo)
        endif
        sp(n)=sinthl
        fp(n)=Fobs
      endif
      if(kspec.ne.'#'.or.iskip.ne.1) call RFactorSuma
2000  Piseme=.false.
      if((nowr.lt.0.and.kspec.ne.'#'.and.kspec.ne.'n'.and.kspec.ne.'r')
     1    .or..not.okraj.or.nowr.eq.0.or.(iabs(nowr).eq.2.and.calcder))
     2  go to 2100
      if(kspec.ne.'n'.and.kspec.ne.'r') then
        if(NTwin.gt.1.or.iover.gt.0) then
          if(NTwin.gt.1) write(ven(idel(ven)+1:),'(i3)') itwr
          i=NDim(KPhase)*4+21
          ven(i:i+19)='    ---       ---   '
        endif
        if(awdy.gt.wdylim.and.awdy.gt.0.)
     1    call RefWorstReflectionSave(ven,awdy,wdylim)
      endif
      if(twdetail.gt.0) then
        i=1
2060    if(ven(i:i).eq.' ') then
          ven(i:i)='>'
          i=i+1
          go to 2060
        endif
      endif
      if(poprve) then
        poprve=.false.
        call newpg(0)
        if(calcder) then
          t256='Fo/Fc list before first cycle'
        else
          t256='Fo/Fc list after last cycle'
        endif
        if(NPhase.gt.1) then
          t256=t256(:idel(t256))//' - '//PhaseName(KPhase)
          if(NDatBlock.gt.1) then
            t256=t256(:idel(t256))//'%'//DatBlockName(KDatB)
          endif
        else
          if(NDatBlock.gt.1)
     1      t256=t256(:idel(t256))//' - '//DatBlockName(KDatB)
        endif
        call TitulekVRamecku(t256)
        call newln(1)
        ivp=NDim(KPhase)*4
        ivk=maxNDim*4
        if(ivk.le.ivp) then
          if(MagPolFlag(KDatBlock).eq.0) then
            write(lst,FormA) RefHeader(:idel(RefHeader))
          else
            write(lst,FormA) RefHeaderMagPol(:idel(RefHeaderMagPol))
          endif
        else
          if(MagPolFlag(KDatBlock).eq.0) then
            write(lst,FormA) RefHeader(:ivp)//
     1                       RefHeader(ivk+1:idel(RefHeader))
          else
            write(lst,FormA) RefHeaderMagPol(:ivp)//
     1                     RefHeaderMagPol(ivk+1:idel(RefHeaderMagPol))
          endif
        endif
      endif
      if(ivk.gt.ivp) then
        id=idel(ven)
        ven=ven(:ivp)//ven(ivk:id)
      endif
      Piseme=.true.
      call newln(1)
      write(lst,FormA) ven(:idel(ven))
2100  if(kspec.ne.'n'.and.kspec.ne.'r') then
        mk=mkp
      else
        mk=0
      endif
      do m=1,mk
        read(LstRef,FormA,end=9999) t256
        if(Piseme) then
          call newln(1)
          write(lst,FormA) t256(2:idel(t256))
        endif
      enddo
      go to 1500
4000  if(.not.isPowder.and.KPh.eq.1.and.okraj.and.
     1   (iabs(nowr).ne.2.or..not.calcder)) call RefWorstReflectionPrint
      if(DelejStatistiku.and.n.gt.10.and.
     1   (iabs(nowr).eq.1.or..not.calcder)) then
        call HeapR(n,sp,1)
        call HeapR(n,fp,1)
        sinmez(8)=sp(n)+.000001
        fmez(8)=fp(n)+.1
        d=0.
        dd=float(n)*.125
        do i=1,7
          d=d+dd
          j=nint(d)
          sinmez(i)=(sp(j)+sp(j+1))*.5
          fmez(i)=(fp(j)+fp(j+1))*.5
        enddo
        call contr(1)
        if(NDim(KPhase).gt.3) call contrm(1,0)
        rewind LstRef
4150    read(LstRef,FormA,end=9999) t256
        k=0
        call kus(t256,k,Cislo)
        if(.not.EqIgCase(Cislo,DatBlockName(KDatB))) go to 4150
4200    read(LstRef,FormA,end=4000) t256
        k=0
        call kus(t256,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatB))) go to 4500
        if(t256(1:1).eq.'t') go to 4200
        if(ExistMagnetic) then
          if(ExistMagPol) then
            read(t256,format3p,err=9999)
     1        (ihref(i,1),i=1,maxNDim),Fobs,Fcalc,dyp,pom,pom,pom,wt,
     2        wdy,i,nspec,kspec,sinthl,extkor,extkorm,iq,itwr,k
              sigyo=1./sqrt(wt)
          else
            read(t256,format3p,err=9999)
     1        (ihref(i,1),i=1,maxNDim),Fobs,Fcalc,pom,pom,dyp,sigyo,wt,
     2        wdy,i,nspec,kspec,sinthl,extkor,extkorm,iq,itwr,k
          endif
        else
          if(ExistMagPol) then
            read(t256,format3p,err=9999)
     1        (ihref(i,1),i=1,maxNDim),Fobs,Fcalc,dyp,pom,pom,pom,wt,
     2        wdy,i,nspec,kspec,sinthl,extkor,iq,itwr,k
              sigyo=1./sqrt(wt)
          else
            read(t256,format3p,err=9999)
     1        (ihref(i,1),i=1,maxNDim),Fobs,Fcalc,pom,pom,dyp,sigyo,wt,
     2        wdy,i,nspec,kspec,sinthl,extkor,iq,itwr,k
          endif
        endif
        if(k.ne.KPhase) go to 4200
        if(kspec.eq.'n'.or.kspec.eq.'r') go to 4200
        if(kspec.eq.'#'.and.iskip.eq.1) go to 4200
        mmabsm=-999
        call RefLoopBefore(1,1,ich)
        if(ich.eq.1) go to 4200
        dy=dyp
        ady=abs(dyp)
        ayo=abs(Fobs)
        wdyq=wdy**2
        wyoq=(Fobs/sigyo)**2
        nulova=nspec.eq.'*'
        call contr(2)
        if(maxNDim.gt.3) call contrm(2,mmabsm-1)
        go to 4200
4500    call contr(3)
        if(maxNDim.gt.3) call contrm(3,0)
      endif
      go to 9999
9000  ich=1
9999  rewind LstRef
      if(Allocated(fp)) deallocate(fp,sp)
      return
      end
