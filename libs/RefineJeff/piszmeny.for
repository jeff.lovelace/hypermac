      subroutine PisZmeny(chlst,lst0)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      real, allocatable :: pomm(:),spomm(:)
      real poma(8),spoma(8)
      character*128 ven
      character*80  Veta
      character*63, allocatable :: CorrV(:)
      character*40 :: Labels(3) = (/'eigenvalues :  ',
     1                              'dpopulations : ',
     2                              'messages :     '/)
      character*8, allocatable :: ModeName(:)
      character*4   chlst
      integer, allocatable :: CorrA(:),ipor(:)
      logical EqIgCase,Messages,Prvne
      i1=max(0,ncykl-5)
      i2=ncykl
      do i=i1,i2
        lst=lst+1
        if(i.eq.i1) lstp=lst
        write(chlst,'(''.l'',i2)') lst0+mod(i,6)+1
        call OpenFile(lst,fln(1:ifln)//chlst,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9000
      enddo
      lstk=lst
      lst=lst0
      call newln(1)
      write(lst,FormA)
      do ln=lstp,lstk
        rewind ln
3050    read(ln,FormA) ven
        if(.not.EqIgCase(ven(1:10),'changes : ')) go to 3050
      enddo
3200  do ln=lstp,lstk
        read(ln,FormA) ven
      enddo
      if(LocateSubstring(ven,'label :',.false.,.true.).eq.1) then
        call TitulekPodtrzeny(ven(9:),'=')
        go to 3200
      endif
      do k=1,3
        if(LocateSubstring(Ven,Labels(k)(:idel(Labels(k))),.false.,
     1                     .true.).eq.1) go to 3500
      enddo
      call newln(i2-i1+4)
      write(lst,FormA) ven(:idel(ven))
      do ln=lstp,lstk
        read(ln,FormA) ven
        write(lst,FormA) ven(:idel(ven))
      enddo
      do ln=lstp,lstk-1
        read(ln,FormA) ven
      enddo
      write(lst,FormA) ven(:idel(ven))
      write(lst,'(128(''=''))')
      read(lstk,FormA) ven
      go to 3200
3500  ln=lstk-1
      RefSeriousWarnings=.false.
      do 3600k=1,3
        rewind ln
        Prvne=.true.
3510    read(ln,FormA,end=3600) ven
        if(LocateSubstring(Ven,Labels(k)(:idel(Labels(k))),.false.,
     1                     .true.).eq.1) then
          go to 3520
        else
          go to 3510
        endif
3520    read(ln,FormA,end=3600) ven
        do l=1,3
          if(l.eq.k) cycle
          if(LocateSubstring(Ven,Labels(l)(:idel(Labels(l))),.false.,
     1                       .true.).eq.1) go to 3600
        enddo
        if(Prvne) then
          if(k.eq.1) then
            call TitulekVRamecku('ADP eigenvalues and eigenvectors')
          else if(k.eq.2) then
            call TitulekVRamecku('d polulations from multipoles')
          else if(k.eq.3) then
            call TitulekVRamecku('List of serious warnings')
            RefSeriousWarnings=.true.
          endif
          Prvne=.false.
        endif
        call newln(1)
        write(lst,FormA) ven(:idel(ven))
        go to 3520
3600  continue
      rewind ln
      do 4000k=1,2
        n=0
        rewind ln
3700    read(ln,FormA,end=4000) ven
        if(LocateSubstring(Ven,'changes :',.false.,.true.).eq.1) then
          if(k.eq.1.and.n.gt.0) allocate(CorrV(n),CorrA(n),ipor(n))
          cycle
        endif
        if(LocateSubString(ven,'correlation',.false.,.true.).le.0)
     1    go to 3700
        n=n+1
        if(k.eq.2) then
          read(ven,'(f6.3)',err=3700) pom
          CorrA(n)=-abs(pom)*1000.
          CorrV(n)=ven
        endif
        go to 3700
4000  continue
      if(n.gt.0) then
        if(n.gt.1) then
          call indexx(n,CorrA,ipor)
        else
          ipor(1)=1
        endif
      endif
      do ln=lstp,lstk
        close(ln,status='delete')
      enddo
      call newln(1)
      write(lst,FormA)
      call newln(1)
      if(n.eq.0) then
        write(lst,'(''There were no correlations larger than '',f6.3,
     1              '' in the last refinement cycle'')') corr
        go to 5000
      else
        write(Cislo,FormI15) n
        call Zhusti(Cislo)
        write(lst,'(''There were '',a,'' correlations larger than '',
     1              f6.3,'' in last refinement cycle'')')
     2    Cislo(:idel(Cislo)),Corr
      endif
      call newln(1)
      write(lst,FormA)
      do j=1,n
        m=ipor(j)
        ven((1-mod(j,2))*63+1:)=CorrV(m)
        if(mod(j,2).eq.0.or.j.eq.n) then
          call newln(1)
          write(lst,FormA) ven(:idel(ven))
        endif
      enddo
      if(allocated(CorrV)) deallocate(CorrV,CorrA,ipor)
5000  if(isPowder.and.NPhase.gt.1.and.DoLeBail.eq.0) then
        do i=1,NPhase
          if(DensityPwd(i).le.0.) go to 9999
        enddo
        call newln(3)
        write(lst,'(/''Relative phase amounts in mass''/)')
        do KDatB=1,NDatBlock
          if(NDatBlock.gt.1) then
            call newln(1)
            write(lst,'(a/)')
     1        DatBlockName(KDatB)(:idel(DatBlockName(KDatB)))
          endif
          pom=0.
          do i=1,NPhase
            pom=pom+DensityPwd(i)*sctw(i,KDatB)
          enddo
          pom=1./pom
          do i=1,NPhase
            call newln(1)
            ScMass (i)=DensityPwd(i)*sctw(i,KDatB)*pom
            ScMassS(i)=pom*sqrt((DensityPwdS(i)*sctw(i,KDatB))**2+
     1                          (DensityPwd(i)*sctws(i,KDatB))**2)
            call RoundESD(Cislo,ScMass (i),ScMassS(i),6,0,0)
            write(lst,'(a8,3x,a15)') PhaseName(i),Cislo
          enddo
        enddo
      endif
      rewind LstSing
      call NewPg(0)
      n=0
5100  read(LstSing,FormA,end=5200,err=5200) Veta
      if(n.eq.0) then
        if(Veta.eq.' ') go to 5100
        call TitulekVRamecku('List of parameters blocked due to '//
     1                       'singularity')
        n=1
      endif
      call NewLn(1)
      write(lst,FormA) Veta(:idel(Veta))
      go to 5100
5200  close(LstSing,status='delete')
      if(NAtXYZMode.gt.0) then
        call NewPg(0)
        call TitulekVRamecku('Summary of refined amplitudes of '//
     1                       'symmetry modes')
        allocate(ModeName(NXYZAMode))
        n=0
        do 5300i=1,NXYZAMode
          j=index(LXYZAMode(i),'#')
          if(j.le.1) cycle
          Cislo=LXYZAMode(i)(:j-1)
          do j=1,n
            if(EqIgCase(ModeName(j),Cislo)) go to 5300
          enddo
          n=n+1
          ModeName(n)=Cislo
5300    continue
        allocate(pomm(n),spomm(n))
        call SetRealArrayTo( pomm,n,0.)
        call SetRealArrayTo(spomm,n,0.)
        ick=(NAtXYZMode-1)/8+1
        jk=0
        do ic=1,ick
          jp=jk+1
          jk=min(jk+8,NAtXYZMode)
          call SetRealArrayTo( poma,8,0.)
          call SetRealArrayTo(spoma,8,0.)
          do i=1,n
            if(i.eq.1) then
              Ven=' '
              k=14
              do j=jp,jk
                Ven(k:)=Atom(IAtXYZMode(1,j))
                k=k+12
              enddo
              if(jk.eq.NAtXYZMode) Ven(k:)='All'
              call NewLn(2)
              write(lst,FormA)
              write(lst,FormA) Ven(:idel(Ven))
            endif
            Ven=ModeName(i)
            idl=idel(Ven)
            id=0
            ia=0
            do j=jp,jk
              ia=ia+1
              id=id+12
               pom=0.
              spom=0.
              do k=1,NMAtXYZMode(j)
                if(LocateSubstring(LAtXYZMode(k,j),ModeName(i)(:idl),
     1                             .false.,.true.).le.0) cycle
                 pom= pom+ AtXYZMode(k,j)**2
                spom=spom+(AtXYZMode(k,j)*SAtXYZMode(k,j))**2
                 pomm(i)= pomm(i)+ AtXYZMode(k,j)**2
                spomm(i)=spomm(i)+(AtXYZMode(k,j)*SAtXYZMode(k,j))**2
                 poma(ia)= poma(ia)+ AtXYZMode(k,j)**2
                spoma(ia)=spoma(ia)+(AtXYZMode(k,j)*SAtXYZMode(k,j))**2
              enddo
              if(pom.gt.0.) then
                spom=sqrt(spom/pom)
                pom=sqrt(pom)
              endif
              call DistRoundESD(Cislo,pom,spom,3)
              Ven(id:)=Cislo
            enddo
            if(jk.eq.NAtXYZMode) then
              id=id+12
              if(pomm(i).gt.0.) then
                spomm(i)=sqrt(spomm(i)/pomm(i))
                pomm(i)=sqrt(pomm(i))
              endif
              call DistRoundESD(Cislo,pomm(i),spomm(i),3)
              Ven(id:)=Cislo
            endif
            call NewLn(1)
            write(lst,FormA) Ven(:idel(Ven))
          enddo
          ia=0
          Ven='All'
          id=0
          do j=jp,jk
            ia=ia+1
            id=id+12
            if(poma(ia).gt.0.) then
              spoma(ia)=sqrt(spoma(ia)/poma(ia))
              poma(ia)=sqrt(poma(ia))
            endif
            call DistRoundESD(Cislo,poma(ia),spoma(ia),3)
            Ven(id:)=Cislo
          enddo
          call NewLn(1)
          write(lst,FormA) Ven(:idel(Ven))
        enddo
        if(allocated(ModeName)) deallocate(ModeName)
        if(allocated(pomm)) deallocate(pomm,spomm)
      endif
      go to 9999
9000  lst=lst0
      do ln=lstp,lstk
        call CloseIfOpened(ln)
      enddo
9999  return
      end
