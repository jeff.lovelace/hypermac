      subroutine SpecEqM(rx,px,nx,n,kmod,TypeModFunI,OrSelI,kwo,k)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rx(4*n,2*n,kmod),px(4*n),nx(kmod)
      integer TypeModFunI,OrSelI(*)
      ii=2
      m=2*n
      kwp=0
      do j=1,kmod
        nd=0
        if(TypeModFunI.ne.1) then
          nd=n
          kwp=kwp+1
        else
          jj=2*j-1
          if(OrSelI(ii).eq.jj) then
            if(kwp.lt.kwo-1) then
              if(OrSelI(ii+1).eq.jj+1) then
                nd=n
                kwp=kwp+1
                ii=ii+1
              else
                do l=1,nx(j)
                  do i=1,n
                    rx(l,i,j)=0.
                  enddo
                enddo
              endif
            else
              do l=1,nx(j)
                do i=1,n
                  rx(l,i,j)=0.
                enddo
              enddo
              kwp=kwp+1
            endif
          else if(OrSelI(ii).eq.jj+1) then
            do l=1,nx(j)
              do i=1,n
                rx(l,n+i,j)=0.
              enddo
            enddo
            k=k-n
            nd=n
          else
            cycle
          endif
        endif
        call uprspec(rx(1,1,j),px,m,nx(j),0)
        call speceq(rx(1,1,j),px,nx(j),m,k)
        if(ErrFlag.ne.0) go to 9999
        k=k+n+nd
        if(TypeModFunI.eq.1) then
          ii=ii+1
          kwp=kwp+1
        endif
      enddo
9999  return
      end
