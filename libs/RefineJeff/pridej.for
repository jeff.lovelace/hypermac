      subroutine pridej(prmo,sprm,prm)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*7 FormatOut
      integer Exponent10
      data FormatOut/'(f10.6)'/
      if(abs(prm).gt.0.) then
        j=max(6-Exponent10(prm),0)
        j=min(j,6)
      else
        j=6
      endif
      write(formatout(6:6),'(i1)') j
      write(ivenr(2)(InvDel+1:),FormatOut) prmo
      write(ivenr(3)(InvDel+1:),FormatOut) sprm
      write(ivenr(4)(InvDel+1:),FormatOut) prm
      InvDel=InvDel+10
      return
      end
