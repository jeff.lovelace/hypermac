      subroutine xmod(f,df1,df2,df3,u,delta,m,ityp)
      include 'fepc.cmn'
      include 'basic.cmn'
      if(ityp.le.3) then
        pmd=pi*float(m)*delta
        arg=pmd+u
        if(arg.ne.0.) then
          sn=sin(arg)
          cs=cos(arg)
          v=sn/arg
          f=delta*v
          rf=1./f
          dv=(cs-v)/arg
          df1=delta*dv*rf
          df2=(v+dv*pmd)*rf
        else
          f=delta
          df1=0.
          df2=1./delta
        endif
        df3=0.
      else if(ityp.le.6) then
        pm=pi*float(m)
        pmd=pm*delta
        arg=pmd+u
        if(arg.ne.0.) then
          sn=sin(arg)
          cs=cos(arg)
          den=1./(arg*(arg-pm))
          v=sn*den
          f=v*u
          if(f.gt.0.) then
            rf=1./f
            dv=den*(cs-v*(2.*arg-pm))
            dvu=dv*u
            df1=(v+dvu)*rf
            df2=dvu*pm*rf
          else
            df1=0.
            df2=1.
          endif
        else
          f=1.
          df1=0.
          df2=1.
        endif
      endif
      return
      end
