      subroutine Philip(Klic,extinc,extink)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      data factor/1.e6/
      yy(a,b,c,fxx)=1./(1.+c*fxx+a*fxx**2/(1.+b*fxx))
      zz(a,b,c,fxx,fyy)=-0.5*fyy*(c+a*fxx*(2.+b*fxx)/(1.+b*fxx)**2)
      if(Klic.eq.0) then
        FCalcUse=FCalc
      else
        FCalcUse=FCalcMag
      endif
      sint=sinthl*LamAve(KDatBlock)
      if(abs(sint).lt..00001.and.FcalcUse.le.0.)
     1  then
        extinc=1.
        extink=1.
        go to 9999
      else if(abs(sint).lt.1.) then
        th=asin(sint)/torad
        cost=sqrt(1.-sint**2)
      else
        th=90.
        sint=.99
        cost=sqrt(1.-sint**2)
      endif
      scpom=sc(iq,KDatBlock)**2
      fsqr=FcalcUse**2/scpom
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        volume=CellVol(1,KPhase)**2*12.593e-4/LamAve(KDatBlock)**3
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        volume=CellVol(1,KPhase)**2*1.e-4/LamAve(KDatBlock)**3
      endif
      tbar=efpip(7)/volume
      gamma=1.5e-4*LamAve(KDatBlock)/volume
      if(Klic.eq.0) then
        mm0=MxScAll*(KDatBlock-1)+MxSc+6
      else
        mm0=0
      endif
      if(ExtType(KDatBlock).eq.1) then
        n=0
        m=6
        hroot=ec(7,KDatBlock)
        if(Klic.eq.1) hroot=hroot*ecMag(2,KDatBlock)
        mm=mm0+6
      else if(ExtType(KDatBlock).eq.2) then
        n=3
        m=0
        mm=mm0
        hroot=ec(1,KDatBlock)
        if(Klic.eq.1) hroot=hroot*ecMag(1,KDatBlock)
      endif
      if(ExtTensor(KDatBlock).ne.1) then
        hroot=efpip(n+1)**2*ec(m+1,KDatBlock)+
     1        efpip(n+2)**2*ec(m+2,KDatBlock)+
     2        efpip(n+3)**2*ec(m+3,KDatBlock)+
     3        2.*(efpip(n+1)*efpip(n+2)*ec(m+4,KDatBlock)+
     4            efpip(n+1)*efpip(n+3)*ec(m+5,KDatBlock)+
     5            efpip(n+2)*efpip(n+3)*ec(m+6,KDatBlock))
        if(hroot.lt.0.) then
          hroot=sqrt(-hroot*factor)
          zroot=-1.
        else
          hroot=sqrt(hroot*factor)
          zroot= 1.
        endif
        hroot=1./hroot
      endif
      if(iab.ne.0) then
        i=th*.2
        xi=th*.2-float(i)
        i=i+1
        cp=.6666667*(dadmi(i)+(dadmi(i+1)-dadmi(i))*xi)
        tbar=tbar*cp
        gamma=gamma*cp
      endif
      sin2t=2.*sint*cost
      cos2t=cost**2-sint**2
      ap=0.20+0.45*cos2t
      bp=0.22-0.12*(0.5-cos2t)**2
      cp=2.
      if(iab.ne.0) then
        i=sint*10.
        xi=sint*10.-float(i)
        i=i+1
        ag=aa(i)+(aa(i+1)-aa(i))*xi
        bg=ba(i)+(ba(i+1)-ba(i))*xi
        al=ag
        bl=bg
      else
        ag=0.58+0.48*cos2t+0.24*cos2t**2
        bg=0.020-0.025*cos2t
        al=0.025+0.285*cos2t
        bl=0.15-0.2*(0.75-cos2t)**2
        if(cos2t.lt.0.) bl=-0.45*cos2t
      endif
      if(ExtType(KDatBlock).eq.2) then
        as=ap
        bs=bp
        cs=2.
      else
        if(ExtDistr(KDatBlock).eq.1) then
          as=ag
          bs=bg
          cs=2.12
        else
          as=al
          bs=bl
          cs=2.
        endif
      endif
      if(ExtType(KDatBlock).eq.2) then
        rho=hroot
        bet=rho
        dbrho=1.
        dbg=0.
      else if(ExtType(KDatBlock).eq.1) then
        gp=hroot
        bet=gp/sin2t
        dbg=1./sin2t
        rho=0.
        dbrho=0.
      else
        rho=ec(1,KDatBlock)
        if(Klic.eq.1) rho=rho*ecMag(1,KDatBlock)
        gp=ec(7,KDatBlock)
        if(Klic.eq.1) gp=gp*ecMag(2,KDatBlock)
        if(ExtTensor(KDatBlock).ne.1) gp=hroot
        if(ExtDistr(KDatBlock).eq.1) then
          bet=rho/sqrt(1.+(rho*sin2t/gp)**2)
          dbrho=(bet/rho)**3
          dbg=(bet/gp)**3*sin2t**2
        else
          bet=rho/(1.+rho*sin2t/gp)
          dbrho=(bet/rho)**2
          dbg=(bet/gp)**2*sin2t
c          bet=rho*gp/(gp+rho*sin2t)
c          dbrho=(gp/(gp+rho*sin2t))**2
c          dbg=(rho/(gp+rho*sin2t))**2*sin2t
        endif
      endif
      x2=tbar*bet*fsqr
      x1=0.
      if(ExtType(KDatBlock).eq.3) x1=gamma*fsqr*rho**2
      xp=x1
      yp=yy(ap,bp,cp,xp)
      zp=zz(ap,bp,cp,xp,yp)
      yp=sqrt(yp)
      if(ExtType(KDatBlock).ne.3) zp=0.
      xsec=x2*yp
      ys=yy(as,bs,cs,xsec)
      zs=zz(as,bs,cs,xsec,ys)
      if(ys.gt.0.) then
        ys=sqrt(ys)
      else
        ys=1.
        zs=0.
      endif
      pol=cos2t**2
      fact=1./(Cos2ThMonQ+pol)
      xpp=x1*pol
      ypp=abs(yy(ap,bp,cp,xpp))
      if(ExtType(KDatBlock).ne.3) then
        zpp=0.
      else
        zpp=zz(ap,bp,cp,xpp,ypp)
      endif
      ypp=sqrt(ypp)
      xps=ypp*x2*pol
      yps=yy(as,bs,cs,xps)
      zps=zz(as,bs,cs,xps,yps)
      if(yps.gt.0.) then
        yps=sqrt(yps)
      else
        yps=1.
        zps=0.
      endif
      ypys=Cos2ThMonQ*yp*ys
      yppyps=pol*ypp*yps
      extinc=fact*(ypys+yppyps)
      om1=fact*(ypys*(1.+xp*zp+xsec*zs*(1.+xp*zp))+yppyps*
     1                            (1.+xpp*zpp+xps*zps*(1.+xpp*zpp)))
      om2=fact*(ypys*zs*yp+pol*yppyps*zps*ypp)
      om3=fact*(ypys*(2.*gamma*rho*zp+zs*yp*(tbar*dbrho +2.*x2*gamma*
     1    rho*zp))+pol*yppyps*(2.*gamma*rho*zpp+zps*ypp*(tbar*dbrho
     2    +2.*pol*x2*gamma*rho*zpp)))
      extink=om1
      if(FcalcUse.gt.0.) then
        tums=0.5*scpom*fsqr**2/(FcalcUse*sqrt(extinc))
      else
        tums=0.
      endif
      tums1=tums*om3
      tums2=tums*tbar*om2*dbg
      if(Klic.eq.0) then
        der(mm0+1)=tums1
      else
        derme(mm0+1)=tums1
      endif
      if(ExtTensor(KDatBlock).eq.1) then
        if(Klic.eq.0) then
          der(mm0+7)=tums2
        else
          derme(mm0+7)=tums2
        endif
      else
        divid=-0.5*hroot**3*factor
        if(ExtType(KDatBlock).eq.2) tums2=tums1
        if(Klic.eq.0) then
          der(mm+1)=zroot*tums2*efpip(n+1)**2*divid
          der(mm+2)=zroot*tums2*efpip(n+2)**2*divid
          der(mm+3)=zroot*tums2*efpip(n+3)**2*divid
          der(mm+4)=zroot*2.*tums2*efpip(n+1)*efpip(n+2)*divid
          der(mm+5)=zroot*2.*tums2*efpip(n+1)*efpip(n+3)*divid
          der(mm+6)=zroot*2.*tums2*efpip(n+2)*efpip(n+3)*divid
        else
          derme(mm+1)=zroot*tums2*efpip(n+1)**2*divid
          derme(mm+2)=zroot*tums2*efpip(n+2)**2*divid
          derme(mm+3)=zroot*tums2*efpip(n+3)**2*divid
          derme(mm+4)=zroot*2.*tums2*efpip(n+1)*efpip(n+2)*divid
          derme(mm+5)=zroot*2.*tums2*efpip(n+1)*efpip(n+3)*divid
          derme(mm+6)=zroot*2.*tums2*efpip(n+2)*efpip(n+3)*divid
        endif
      endif
9999  return
      end
