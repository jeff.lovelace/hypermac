      subroutine RefRestric
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/RestricQuest/ nEdwAtoms,nEdwPhases,nCrwRestricAtMol,
     1                     nCrwRestricProfile,RestricType(6),OccType,
     2                     AtomString,LocalSymm,nRolMenuLocalSymm,
     3                     PhaseString,RestricProfile
      character*256 AtomString,PhaseString,EdwStringQuest
      character*80  Veta,Menu(:)
      character*25 :: CrwLabels(10) = (/'Coo%rdinates            ',
     1                                  '%Modulations            ',
     2                                  'AD%P parameters         ',
     3                                  '%Valence population     ',
     4                                  '%Kappa                  ',
     5                                  'Kappa%''                 ',
     6                                  'modulated identica%lly  ',
     7                                  'modulated complementar%y',
     8                                  'keep %overall sum       ',
     9                                  'not r%estricted         '/)
      integer WhatHappened,RestricType,OccType,CrwGr,EdwStateQuest
      logical CrwLogicQuest,RestricProfile,Poprve
      external RefRestricReadCommand,RefRestricWriteCommand,FeVoid
      allocatable Menu
      save /RestricQuest/
      RestricProfile=.false.
      if(NsymmL(KPhase).gt.0) then
        xqd=650.
        allocate(Menu(NSymmL(KPhase)+1))
        Menu(1)='identical'
        Menu(2)='related by the local symmetry#'
        idl=idel(Menu(2))
        do i=1,NSymmL(KPhase)
          Menu(i+1)=Menu(2)
          write(Menu(i+1)(idl+1:idl+1),'(i1)') i
        enddo
      else
        xqd=500.
      endif
      if(ExistPowder.and.NPhase.gt.1) then
        i=8
      else
        i=6
      endif
      call RepeatCommandsProlog(id,fln(:ifln)//'_restric.tmp',xqd,i,il,
     1                          OKForBasicFiles)
      CrwGr=0
      il=il+1
      if(ExistPowder.and.NPhase.gt.1) then
        tpom=5.
        CrwGr=CrwGr+1
        Veta='For atoms/molecules'
        do i=1,2
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,CrwGr)
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(i.eq.1) then
            nCrwRestricAtMol=CrwLastMade
            tpom=tpom+xqd*.5
            Veta='For powder profiles:'
          else
            nCrwRestricProfile=CrwLastMade
          endif
        enddo
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='List of phases with identical profiles:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=xqd-xpom-5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwPhases=EdwLastMade
      else
        nCrwRestricAtMol=0
        nCrwRestricProfile=0
      endif
      xd=20.
      ilp=il
      if(NsymmL(KPhase).gt.0) then
        tpom=5.
        Veta='Make'
        call FeQuestLblMake(id,tpom,il,Veta,'L','B')
        nLblMake=LblLastMade
        call FeBoldFont
        xpom=tpom+FeTxLength(Veta)+15.
        call FeNormalFont
      else
        tpom=5.
        Veta='Make identical:'
        call FeQuestLblMake(id,tpom,il,Veta,'L','B')
        nLblIdentical=LblLastMade
        call FeBoldFont
        xpom=tpom+FeTxLength(Veta)+15.
        call FeNormalFont
      endif
      tpom=xpom+CrwXd+10.
      do i=1,6
        call FeQuestCrwMake(id,tpom,il,xpom,il,CrwLabels(i),'L',
     1                      CrwXd,CrwYd,1,0)
        if(i.eq.1) nCrwParamFirst=CrwLastMade
        if(i.eq.2.and.NDimI(KPhase).le.0) cycle
        if(i.gt.2.and..not.ChargeDensities) cycle
        il=il+1
      enddo
      ilk=il
      il=ilp
      tpom=tpom+FeTxLengthUnder(CrwLabels(3))+10.
      if(NsymmL(KPhase).gt.0) then
        dpom=FeTxLength(Menu(2))+2.*EdwMarginSize+EdwYd
        call FeQuestRolMenuMake(id,tpom,il,tpom,il,' ','L',dpom,
     1                          EdwYd,0)
        nRolMenuLocalSymm=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,Menu,NSymmL(KPhase)+1,1)
        tpom=tpom+dpom+60.
      else
        nRolMenuLocalSymm=0
      endif
      nCrwParamLast=CrwLastMade
      il=ilp
      Veta='Occupancies:'
      call FeQuestLblMake(id,tpom,il,'Occupancies:','L','B')
      nLblOccupancies=LblLastMade
      call FeBoldFont
      xpom=tpom+FeTxLength(Veta)+15.
      call FeNormalFont
      tpom=xpom+CrwgXd+10.
      CrwGr=CrwGr+1
      do i=1,4
        call FeQuestCrwMake(id,tpom,il,xpom,il,CrwLabels(i+6),'L',
     1                      CrwgXd,CrwgYd,1,CrwGr)
        if(i.eq.1) nCrwOccFirst=CrwLastMade
        if(i.le.2.and.NDimI(KPhase).le.0) cycle
        il=il+1
      enddo
      ilk=max(il,ilk)
      il=ilk
      if(NMolec.gt.0) then
        Veta='%Atoms/molecules'
      else
        Veta='%Atoms'
      endif
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwAtoms=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
1300  RestricType(1:6)=0
      OccType=4
      LocalSymm=1
      AtomString=' '
      PhaseString=' '
      Poprve=.true.
1400  nCrw=nCrwParamFirst-1
      if(RestricProfile) then
        if(EdwStateQuest(nEdwAtoms).eq.EdwOpened.or.Poprve) then
          if(EdwStateQuest(nEdwAtoms).eq.EdwOpened)
     1      AtomString=EdwStringQuest(nEdwAtoms)
          do i=1,6
            nCrw=nCrw+1
            if((i.eq.2.and.NDimI(KPhase).le.0).or.
     1         (i.gt.3.and..not.ChargeDensities)) cycle
            call FeQuestCrwClose(nCrw)
          enddo
          nCrw=nCrwOccFirst
          do i=1,4
            if(i.gt.2.or.NDimI(KPhase).gt.0)
     1        call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestLblOff(nLblIdentical)
          call FeQuestLblOff(nLblOccupancies)
          call FeQuestEdwClose(nEdwAtoms)
          if(NsymmL(KPhase).gt.0)
     1      call FeQuestRolMenuClose(nRolMenuLocalSymm)
          call FeQuestStringEdwOpen(nEdwPhases,PhaseString)
        endif
      else
        if(nEdwPhases.gt.0.or.Poprve) then
          if(nEdwPhases.gt.0) then
            if(EdwStateQuest(nEdwPhases).eq.EdwOpened) then
              PhaseString=EdwStringQuest(nEdwPhases)
              call FeQuestEdwClose(nEdwPhases)
            endif
          endif
          do i=1,6
            nCrw=nCrw+1
            if((i.eq.2.and.NDimI(KPhase).eq.0).or.
     1         (i.gt.3.and..not.ChargeDensities)) cycle
            call FeQuestCrwOpen(nCrw,RestricType(i).gt.0)
          enddo
          nCrw=nCrwOccFirst
          do i=1,4
            if(i.gt.2.or.NDimI(KPhase).gt.0)
     1        call FeQuestCrwOpen(nCrw,i.eq.OccType)
            nCrw=nCrw+1
          enddo
          call FeQuestLblOn(nLblIdentical)
          call FeQuestLblOn(nLblOccupancies)
          call FeQuestStringEdwOpen(nEdwAtoms,AtomString)
          if(NsymmL(KPhase).gt.0)
     1      call FeQuestRolMenuOpen(nRolMenuLocalSymm,Menu,
     2                              NSymmL(KPhase)+1,LocalSymm)
        endif
      endif
      Poprve=.false.
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  RefRestricReadCommand,RefRestricWriteCommand,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        Poprve=.true.
        go to 1400
      endif
      if(CheckType.eq.EventCrw) then
        if(CheckNumber.le.nCrwRestricProfile) then
          RestricProfile=CrwLogicQuest(nCrwRestricProfile)
          go to 1400
        else if(CheckNumber.ge.nCrwOccFirst) then
          OccType=CheckNumber-nCrwOccFirst+1
          go to 2000
        else
          i=CheckNumber-nCrwParamFirst+1
          if(CrwLogicQuest(CheckNumber)) then
            RestricType(i)=1
          else
            RestricType(i)=0
          endif
          go to 1400
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(allocated(Menu)) deallocate(Menu)
      return
      end
