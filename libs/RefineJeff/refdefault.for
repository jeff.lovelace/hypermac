      subroutine RefDefault
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      if(allocated(ExtTensor))
     1  deallocate(ExtTensor,ExtType,ExtDistr,ExtRadius)
      allocate(ExtTensor(NDatBlock),ExtType(NDatBlock),
     1         ExtDistr(NDatBlock),ExtRadius(NDatBlock))
      NactiInt=40
      NactiReal=40
      NactiComposed=40
      NactiKeys=NactiInt+NactiReal+NactiComposed
      call CopyVekI(DefIntRefine,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntRefine,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntRefine,CmdIntRefine,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealRefine(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealRefine(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealRefine(i),CmdRealRefine, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdRefine(i)
      enddo
      neq=0
      nvai=0
      if(allocated(nai)) then
        deallocate(nai,naixb,sumai,mai,atvai,nails,RestType)
        nvaiMax=0
      endif
      nskrt=0
      NScales=0
      NRFactors=0
      iab=0
      nfix=0
      nkeep=0
      NKeepAt=0
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      mxe=100
      mxep=5
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      call SetIntArrayTo(KFixOrigin,NPhase,0)
      call SetIntArrayTo(KFixX4,NPhase,0)
      nchkr=0
      call SetIntArrayTo(NSuper,9*NPhase,1)
      if(NDim(KPhase).gt.4) then
        i=16
      else
        i=32
      endif
      call SetIntArrayTo(ngrid,3,i)
      omdif(1)=.1
      omdif(2)=.5
      thdif(1)=.1
      thdif(2)=.5
      chidif(1)=.5
      chidif(2)=5.
      call SetIntArrayTo(ExtTensor,NDatBlock,0)
      call SetIntArrayTo(ExtType,NDatBlock,1)
      call SetIntArrayTo(ExtDistr,NDatBlock,1)
      call SetRealArrayTo(ExtRadius,NDatBlock,.01)
      call SetRealArrayTo(difh,6,.001)
      DatBlockDerTest=0
      nxxn=0
      call SetIntArrayTo(nxxp,20,0)
      call SetLogicalArrayTo(UseDatBlockInRefine,NDatBlock,.true.)
      RefRandomize=.false.
      RefRandomSeed=0
      RefRandomDiff=.1
      return
      end
