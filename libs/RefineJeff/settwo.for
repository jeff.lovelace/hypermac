      subroutine settwo(ih,ihitw,itwri,ranover)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(6),ihitw(6,18),hf(6,18),c(3),difhp(6,18)
      logical   ranover
      ranover=.false.
      do i=1,NDim(KPhase)
        hf(i,itwri)=ih(i)
      enddo
      if(itwri.ne.1) call multm(hf(1,itwri),rtwi(1,itwri),hf(1,1),1,
     1                          NDim(KPhase),NDim(KPhase))
      do j=2,NTwin
        if(j.ne.itwri) call multm(hf(1,1),rtw(1,j),hf(1,j),1,
     1                            NDim(KPhase),NDim(KPhase))
      enddo
      do j=1,NTwin
        do i=1,NDim(KPhase)
          ihitw(i,j)=nint(hf(i,j))
          difhp(i,j)=abs(hf(i,j)-float(ihitw(i,j)))
          hf(i,j)=ihitw(i,j)
        enddo
      enddo
      if(nchkr.le.0) then
        do 1700j=1,NTwin
          do i=1,NDim(KPhase)
            if(difhp(i,j).gt.difh(i)) then
              ihitw(1,j)=999
              go to 1700
            endif
          enddo
1700    continue
      else
        call multm(ormat(1,1,itwri),hf(1,itwri),c,3,3,1)
        pom=sqrt(c(1)**2+c(2)**2+c(3)**2)
        chi1=atan2(c(3),sqrt(c(1)**2+c(2)**2))/torad
        fi1=atan2(-c(1),c(2))
        sth1=.5*LamAve(1)*pom
        th1=asin(sth1)/torad
        sth1=sth1*5.75877
        do 1800j=1,NTwin
          if(j.eq.itwri) go to 1800
          call multm(ormat(1,1,j),hf(1,j),c,3,3,1)
          pom=sqrt(c(1)**2+c(2)**2+c(3)**2)
          chi2=atan2(c(3),sqrt(c(1)**2+c(2)**2))/torad
          fi2=atan2(-c(1),c(2))
          th2=asin(.5*LamAve(1)*pom)/torad
          dom=abs(asin(-sin(fi1-fi2)*cos(chi2*torad))/torad)
          if(dom.gt.omdif(1).or.abs(th1-th2).gt.thdif(1).or.
     1       abs(chi1-chi2)*sth1.gt.chidif(1)) then
            ihitw(1,j)=999
            ranover=dom.lt.omdif(2).and.abs(th1-th2).lt.thdif(2).and.
     1              abs(chi1-chi2)*sth1.lt.chidif(2)
          endif
1800    continue
      endif
      return
      end
