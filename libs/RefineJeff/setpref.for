      subroutine SetPref(ihp,pref)
      use Basic_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ihh(6),ihp(6),diff(3),hh(6),MMin(3),MMax(3)
      pref=1.
      dpref1=0.
      dpref2=0.
      if(KPref(KPhase,KDatBlock).eq.IdPwdPrefNone) go to 9999
      if(SPref(KPhase,KDatBlock).eq.1) then
        nsp=NSymmN(KPhase)
      else
        nsp=1
      endif
      pref=0.
      MMin=0
      MMax=0
      do i=1,nsp
        call IndTr(ihp,rm6(1,i,1,KPhase),ihh,NDim(KPhase))
        call ChngInd(hh,ihh,1,diff,MMin,MMax,0,CheckExtRefYes)
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefMarchDollase) then
          pom=cosHKL(DirPref(1,KPhase,KDatBlock),hh)**2
          qq=pom*PrefPwd(1,KPhase,KDatBlock)**2+(1.-pom)/
     1           PrefPwd(1,KPhase,KDatBlock)
          vv=1./qq**1.5
          dpref1=dpref1-(1.-PrefPwd(2,KPhase,KDatBlock))*
     1           1.5*vv/qq*(pom*2.*PrefPwd(1,KPhase,KDatBlock)-
     2           (1.-pom)/PrefPwd(1,KPhase,KDatBlock)**2)
        else if(KPref(KPhase,KDatBlock).eq.IdPwdPrefSasaUda) then
          pom=cosHKL(DirPref(1,KPhase,KDatBlock),hh)
          if(pom.ge.1.) then
            pom=0.
          else if(pom.le.-1.) then
            pom=pi
          else
            pom=acos(pom)
          endif
          pom=pom**2
          vv=ExpJana(PrefPwd(1,KPhase,KDatBlock)*pom,ich)
          dpref1=dpref1+(1.-PrefPwd(2,KPhase,KDatBlock))*vv*pom
        endif
        pref=pref+PrefPwd(2,KPhase,KDatBlock)+
     1            (1.-PrefPwd(2,KPhase,KDatBlock))*vv
        dpref2=dpref2+1.-vv
      enddo
      smult=1./nsp
      pref=pref*smult
      dpref1=dpref1*smult
      dpref2=dpref2*smult
      if(pref.ne.0.) then
        dpref1=dpref1/pref
        dpref2=dpref2/pref
      endif
      i=IPrefPwd+(KDatBlock-1)*NParProfPwd+(KPhase-1)*NParCellProfPwd
      NRefPwd=max(NRefPwd,1)
      pom=dpref1
      do j=i,i+1
        do k=1,NAlfa(KDatBlock)
          coef(j,k,NRefPwd)=pom
        enddo
        pom=dpref2
      enddo
9999  return
      end
