      subroutine RefKeepReadCommand(Command,ich)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension xp(6),it(3)
      character*(*) Command
      character*80 t80,At1,At2
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        call EM40SetDefaultKeepH(1)
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'keep'.and.t80.ne.'!keep') go to 8010
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      call mala(t80)
      i=islovo(t80,CKeepType,3)
      if(i.le.0) go to 8010
      KeepType(1)=i
      if(k.ge.lenc) go to 8000
      call kus(Command,k,t80)
      call mala(t80)
      if(i.eq.IdKeepHydro) then
        i=islovo(t80,CKeepHydro,3)
      else if(i.eq.IdKeepGeom) then
        i=islovo(t80,CKeepGeom,3)
      else if(i.eq.IdKeepADP) then
        i=1
      endif
      if(i.le.0) go to 8010
      KeepTypeMain=KeepType(1)
      KeepType(1)=KeepType(1)*10+i
      if(k.ge.lenc) go to 8000
      if(KeepTypeMain.eq.IdKeepHydro) then
        if(KeepType(1).eq.IdKeepHApical) then
          NAtMax=6
        else if(KeepType(1).eq.IdKeepHTetraHed) then
          NAtMax=4
        else if(KeepType(1).eq.IdKeepHTriangl) then
          NAtMax=3
        endif
        call kus(Command,k,t80)
        call UprAt(t80)
        ia=ktatmol(t80)
        if(ia.le.0) then
          ich=1
          call FeWinMessage('A je to tady',' ')
          go to 8060
        endif
        KeepNAtCentr(1)=ia
        KeepAtCentr(1)=t80
        kmol1=kmol(ia)
        isw1=iswa(ia)
        ksw1=kswa(ia)
        At1=t80
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        kk=0
        call StToInt(t80,kk,KeepNNeigh(1),1,.false.,ich)
        if(ich.ne.0) go to 8030
        if(KeepNNeigh(1).gt.NAtMax) then
          write(Cislo,FormI15) KeepNNeigh(1)
          call Zhusti(Cislo)
          t80='The number of '//Cislo(:idel(Cislo))//
     1        ' neighbors atoms exceeds the limit'
          go to 8100
        else if(KeepType(1).eq.IdKeepHApical.and.KeepNNeigh(1).lt.2)
     1    then
          t80='The number of neighbor atoms is too small'
          go to 8100
        endif
        if(k.ge.lenc) go to 8000
        if(KeepType(1).ne.IdKeepHApical) then
          call kus(Command,k,t80)
          kk=0
          call StToInt(t80,kk,KeepNH(1),1,.false.,ich)
          if(ich.ne.0) go to 8030
        else
          KeepNH(1)=1
        endif
        if(KeepNNeigh(1)+KeepNH(1).gt.NAtMax+1) then
          write(Cislo,FormI15) KeepNNeigh(n)+KeepNH(n)
          call Zhusti(Cislo)
          t80='The number of '//Cislo(:idel(Cislo))//
     1        ' Neighbor+H atoms exceeds the limit'
          go to 8100
        endif
        UseAnchor=.false.
        KeepNAtAnchor(1)=0
        KeepAtAnchor(1)=' '
        do i=1,KeepNNeigh(1)
          if(i+KeepNH(1).eq.NAtMax+1.and.KeepType(1).ne.IdKeepHApical)
     1      then
            if(k.ge.lenc) go to 8000
            call kus(Command,k,t80)
            kk=0
            call StToReal(t80,kk,KeepAngleH(1),1,.false.,ich)
            if(ich.ne.0) go to 8040
            UseAnchor=.true.
          endif
          if(k.ge.lenc) go to 8000
          call kus(Command,k,t80)
          call UprAt(t80)
          call AtomSymCode(t80,ia,ISym,ICentr,xp,it,ich)
          if(ich.eq.1.and.NMolec.gt.0) then
            ia=ktatmol(t80)
            if(ia.ge.NAtMolFr(1,1)) ich=0
          endif
          if(ich.ne.0) go to 8060
          if(i+KeepNH(1).lt.NAtMax+1) then
            KeepNAtNeigh(1,i)=ia
            KeepAtNeigh(1,i)=t80
          else
            KeepNAtAnchor(1)=ia
            KeepAtAnchor(1)=t80
          endif
        enddo
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        kk=0
        call StToReal(t80,kk,KeepDistH(1),1,.false.,ich)
        if(ich.ne.0) go to 8040
        do i=1,KeepNH(1)
          if(k.ge.lenc) go to 8000
          call kus(Command,k,t80)
          call UprAt(t80)
          ia=ktatmol(t80)
          if(ia.le.0) go to 8060
          KeepNAtH(1,i)=ia
          KeepAtH(1,i)=t80
          At2=t80
          if(iswa(ia).ne.isw1) then
            go to 8050
          else if(kmol(ia).ne.kmol1.and.kmol1.ne.0) then
            go to 8051
          else if(kswa(ia).ne.ksw1) then
            go to 8052
          endif
        enddo
      else if(KeepTypeMain.eq.IdKeepGeom) then
        n=0
5000    if(k.ge.lenc) go to 5100
        n=n+1
        call kus(Command,k,t80)
        call UprAt(t80)
        if(index(t80,'*').gt.0.or.index(t80,'?').gt.0) then
          ia=0
        else
          call AtomSymCode(t80,ia,ISym,ICentr,xp,it,ich)
          if(ich.eq.1.and.NMolec.gt.0) then
            ia=ktatmol(t80)
            if(ia.ge.NAtMolFr(1,1)) ich=0
          endif
          if(ich.ne.0) go to 8060
        endif
        KeepNAt(1,n)=ia
        KeepAt(1,n)=t80
        if(ia.le.0) go to 5000
        if(n.eq.1) then
          isw1=iswa(ia)
          ksw1=kswa(ia)
          kmol1=kmol(ia)
          At1=t80
        else
          At2=t80
        endif
        if(iswa(ia).ne.isw1) then
          go to 8050
        else if(kmol(ia).ne.kmol1.and.kmol1.ne.0) then
          go to 8051
        else if(kswa(ia).ne.ksw1) then
          go to 8052
        endif
        go to 5000
5100    KeepN(1)=n
      else if(KeepTypeMain.eq.IdKeepADP) then
        call kus(Command,k,t80)
        call UprAt(t80)
        ia=ktatmol(t80)
        if(ia.le.0) then
          ich=1
          go to 8060
        endif
        KeepNAtCentr(1)=ia
        KeepAtCentr(1)=t80
        kmol1=kmol(ia)
        isw1=iswa(ia)
        ksw1=kswa(ia)
        At1=t80
        call kus(Command,k,t80)
        kk=0
        call StToReal(t80,kk,KeepADPExtFac(1),1,.false.,ich)
        if(ich.ne.0) go to 8040
        n=0
6000    if(k.ge.lenc) go to 6100
        n=n+1
        call kus(Command,k,t80)
        call UprAt(t80)
        call AtomSymCode(t80,ia,ISym,ICentr,xp,it,ich)
        if(ich.eq.1.and.NMolec.gt.0) then
          ia=ktatmol(t80)
          if(ia.ge.NAtMolFr(1,1)) ich=0
        endif
        if(ich.ne.0) go to 8060
        KeepNAtH(1,n)=ia
        KeepAtH(1,n)=t80
        At2=t80
        if(iswa(ia).ne.isw1) then
          go to 8050
        else if(kmol(ia).ne.kmol1.and.kmol1.ne.0) then
          go to 8051
        else if(kswa(ia).ne.ksw1) then
          go to 8052
        endif
        go to 6000
6100    KeepNH(1)=n
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8020  t80='incorrect type "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8050  t80=' are not from the same composite part'
      go to 8055
8051  t80=' are not from the same molecule'
      go to 8055
8052  t80=' are not from the same phase'
8055  t80='"'//At1(:idel(At1))//'" "'//At2(:idel(At2))//'" '//
     1     t80(:idel(t80))
      go to 8100
8060  if(ich.eq.1) then
        t80='Atom "'//t80(:idel(t80))//'" doesn''t exist'
      else if(ich.eq.2) then
        t80='The symmetry part isn''t correct : '//t80(:idel(t80))
      endif
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
      KeepType(1)=11
9999  return
      end
