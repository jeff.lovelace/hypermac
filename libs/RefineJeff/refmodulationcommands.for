      subroutine RefModulationCommands
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 Veta,LabelOver
      logical CrwLogicQuest,lpom
      integer OvSwitch,OvSwitchOld
      save nCrwMetodaFirst,nEdwAccuracy,nEdwGrid,MetodaOld,
     1     nCrwSelRefFirst,nCrwSelRefLast,nCrwCompSelFirst,
     2     nCrwCompSelLast,nCrwOverlapFirst,nCrwOverlapLast,
     3     nEdwOverlapModulo,nEdwOverlapMMax,nEdwOverlapDif,
     4     OvSwitch,OvSwitchOld,nLblOverlapModulo,ModIndex,
     5     MaxIndex,OverDifP
      entry RefModulationCommandsMake(id)
      xqd=XdQuestRef-2.*KartSidePruh
      MetodaOld=-1
      OvSwitchOld=-1
      if(NDimI(KPhase).le.0) then
        Veta='Not applicable, the structure is not modulated.'
        call FeQuestLblMake(id,xqd*.5,7,Veta,'C','B')
        go to 9999
      endif
      il=0
      if(KCommenMax.le.0) then
        il=il+1
        Veta='Calculation of structure factors based on:'
        call FeQuestLblMake(id,5.,il,Veta,'L','B')
        xpom=7.
        tpom=xpom+CrwXd+10.
        ichk=1
        igrp=1
        do i=0,1
          if(i.eq.0) then
            Veta='%Bessel functions'
          else if(i.eq.1) then
            Veta='%Gaussian integration'
          endif
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          if(i.eq.0) nCrwMetodaFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdmetoda))
        enddo
        il=1
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=80.
        tpom=xpom+dpom+10.
        do i=1,2
          if(i.eq.1) then
            Veta='%Accuracy'
          else
            Veta='G%rid'
          endif
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            nEdwAccuracy=EdwLastMade
          else
            nEdwGrid=EdwLastMade
          endif
        enddo
      else
        nCrwMetodaFirst=0
        nEdwAccuracy=0
        nEdwGrid=0
        dpom=80.
      endif
      il=il+1
      call FeQuestLblMake(id,5.,il,'Reflections used in refinement:',
     1                    'L','B')
      xpom=7.
      tpom=xpom+CrwXd+10.
      ichk=0
      igrp=2
      Veta='A%ll'
      do i=-2,0
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        if(i.eq.-2) then
          nCrwSelRefFirst=CrwLastMade
          Veta='%Satellites'
        else if(i.eq.-1) then
          Veta='%Main'
        else if(i.eq.0) then
          nCrwSelRefLast=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdkim))
      enddo
      if(NComp(KPhase).gt.1) then
        il=il-3
        Veta='Bo%th subsytems'
        xpom=xpom+150.
        tpom=tpom+150
        igrp=3
        do 1300i=0,4
          if(i.eq.3) then
            Veta='C%ommon part'
            go to 1300
          endif
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          if(i.eq.0) then
            nCrwCompSelFirst=CrwLastMade
            Veta='%1 subsystem'
          else if(i.eq.1) then
            Veta='%2 subsystem'
          else if(i.eq.4) then
            nCrwCompSelLast=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdkic))
1300    continue
      else
        nCrwCompSelFirst=0
        nCrwCompSelLast=0
      endif
      if(KCommenMax.le.0) then
        il=1
        call FeQuestLblMake(id,xqd*.5,il,'Overlap option:','L','B')
        igrp=3
        ichk=1
        xpom=xqd*.5
        tpom=xpom+CrwXd+10.
        Veta='%None'
        do 1500i=1,3
          il=il+1
          if(i.ne.2.or.NDimI(KPhase).eq.1) then
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,ichk,igrp)
            call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdiover).le.0)
          endif
          if(i.eq.1) then
            nCrwOverlapFirst=CrwLastMade
            Veta='Defined by e%quation'
            lpom=NacetlInt(nCmdiover).le.0
          else if(i.eq.2) then
            Veta='%Closest reflections'
            xpom1=tpom+FeTxLengthUnder(Veta)+15.
            dpom=50.
            tpom1=xpom1+dpom+10.
            if(NDimI(KPhase).ne.1) then
              nCrwOverlapModulo=0
              nEdwOverlapModulo=0
              nLblOverlapModulo=0
              il=il-1
              go to 1500
            endif
            lpom=NacetlInt(nCmdiover).gt.0.and.
     1           NacetlReal(nCmdOverDif).le.0.
            call FeQuestEdwMake(id,tpom1,il,xpom1,il,'mo%dulo','L',dpom,
     1                          EdwYd,1)
            nEdwOverlapModulo=EdwLastMade
            il=il+1
            call FeQuestLblMake(id,tpom,il,' ','L','N')
            call FeQuestLblOff(LblLastMade)
            nLblOverlapModulo=LblLastMade
            if(lpom) then
              ModIndex=NacetlInt(nCmdiover)
            else
              pmx=9999999.
              do k=1,20
                p=0.
                flk=k
                do j=1,3
                  pom=abs(qu(j,1,1,KPhase)*flk)
                  pom=abs(pom-anint(pom))
                  p=p+pom
                enddo
                if(p.lt.pmx) then
                  pmx=p
                  ModIndex=k
                endif
              enddo
            endif
          else
            nCrwOverlapLast=CrwLastMade
            lpom=NacetlInt(nCmdiover).gt.0.and.
     1           NacetlReal(nCmdOverDif).gt.0.
            call FeQuestEdwMake(id,tpom1,il,xpom1,il,
     1                         'max. satellite %index','L',dpom,EdwYd,1)
            nEdwOverlapMMax=EdwLastMade
            il=il+1
            call FeQuestEdwMake(id,tpom1,il,xpom1,il,
     1                      'max. difference in %degs','L',dpom,EdwYd,1)
            nEdwOverlapDif=EdwLastMade
            if(lpom) then
              MaxIndex=NacetlInt(nCmdiover)
              OverDifP=NacetlReal(nCmdOverDif)
            else
              MaxIndex=4
              OverDifP=.5
            endif
          endif
          if(lpom) OvSwitch=i
          call FeQuestCrwOpen(CrwLastMade,lpom)
1500    continue
      else
        nCrwOverlapNone=0
        nCrwOverlapModulo=0
        nCrwOverlapClose=0
        nEdwOverlapModulo=0
        nEdwOverlapMMax=0
      endif
      go to 2500
      entry RefModulationCommandsCheck
      if(nCrwMetodaFirst.gt.0) then
        if(CrwLogicQuest(nCrwMetodaFirst)) then
          NacetlInt(nCmdmetoda)=0
        else
          NacetlInt(nCmdmetoda)=1
        endif
      endif
      if(KCommenMax.le.0) then
        nCrw=nCrwOverlapFirst
        do 2100i=1,3
          if(i.eq.2.and.NDimI(KPhase).ne.1) go to 2100
          if(CrwLogicQuest(nCrw)) then
            OvSwitch=i
            go to 2150
          endif
          nCrw=nCrw+1
2100    continue
        go to 2500
2150    if(CheckType.eq.EventEdw) then
          if(OvSwitch.eq.2) then
            i=ModIndex
            call FeQuestIntFromEdw(nEdwOverlapModulo,ModIndex)
            if(i.ne.ModIndex) OvSwitchOld=-1
          else if(OvSwitch.eq.3) then
            call FeQuestIntFromEdw(nEdwOverlapMMax,MaxIndex)
            call FeQuestRealFromEdw(nEdwOverlapDif,OverDifP)
          endif
        endif
      endif
2500  if(nCrwMetodaFirst.gt.0) then
        if(NacetlInt(nCmdmetoda).ne.MetodaOld) then
          if(NacetlInt(nCmdmetoda).eq.0) then
            call FeQuestRealEdwOpen(nEdwAccuracy,
     1        NacetlReal(nCmdDifBess),.false.,.false.)
            call FeQuestEdwClose(nEdwGrid)
          else
            call FeQuestEdwClose(nEdwAccuracy)
            call FeQuestIntAEdwOpen(nEdwGrid,ngrid,NDimI(KPhase),
     1                              .false.)
          endif
          MetodaOld=NacetlInt(nCmdmetoda)
        endif
      endif
      if(KCommenMax.le.0) then
        if(OvSwitch.ne.OvSwitchOld) then
          if(OvSwitch.eq.1) then
            if(nEdwOverlapModulo.ne.0) then
              call FeQuestEdwClose(nEdwOverlapModulo)
              call FeQuestLblOff(nLblOverlapModulo)
            endif
            call FeQuestEdwClose(nEdwOverlapMMax)
            call FeQuestEdwClose(nEdwOverlapDif)
          else if(OvSwitch.eq.2) then
            call FeQuestEdwClose(nEdwOverlapMMax)
            call FeQuestEdwClose(nEdwOverlapDif)
            LabelOver='(hklm) combined with (hklm)+-'
            pom=ModIndex
            do j=1,3
              ihov(j)=nint(qu(j,1,1,KPhase)*pom)
            enddo
            ihov(4)=-ModIndex
            write(LabelOver(30:),100)(ihov(j),j=1,4)
            call zhusti(LabelOver(30:))
            call FeQuestIntEdwOpen(nEdwOverlapModulo,ModIndex,.false.)
            call FeQuestLblChange(nLblOverlapModulo,LabelOver)
          else if(OvSwitch.eq.3) then
            if(nEdwOverlapModulo.ne.0) then
              call FeQuestEdwClose(nEdwOverlapModulo)
              call FeQuestLblOff(nLblOverlapModulo)
            endif
            call FeQuestIntEdwOpen(nEdwOverlapMMax,MaxIndex,.false.)
            call FeQuestRealEdwOpen(nEdwOverlapDif,OverDifP,.false.,
     1                              .false.)
          endif
        endif
        OvSwitchOld=OvSwitch
      endif
      go to 9999
      entry RefModulationCommandsUpdate
      if(KCommenMax.le.0) then
        if(NacetlInt(nCmdmetoda).eq.0) then
          call FeQuestRealFromEdw(nEdwAccuracy,NacetlReal(nCmdDifBess))
        else
          call FeQuestIntAFromEdw(nEdwGrid,ngrid)
        endif
      endif
      do i=nCrwSelRefFirst,nCrwSelRefLast
        if(CrwLogicQuest(i)) then
          NacetlInt(nCmdkim)=i-nCrwSelRefFirst-2
          go to 3150
        endif
      enddo
3150  NacetlInt(nCmdkic)=DefaultInt(nCmdkic)
      if(NComp(KPhase).gt.1) then
        do i=nCrwCompSelFirst,nCrwCompSelLast
          if(CrwLogicQuest(i)) then
            NacetlInt(nCmdkic)=i-nCrwCompSelFirst
            if(NacetlInt(nCmdkic).eq.3)
     1        NacetlInt(nCmdkic)=NacetlInt(nCmdkic)+1
            go to 3250
          endif
        enddo
      endif
3250  if(KCommenMax.le.0) then
        if(OvSwitch.eq.1) then
          NacetlInt(nCmdiover)=0
        else if(OvSwitch.eq.2) then
          NacetlInt(nCmdiover)=ModIndex
          NacetlReal(nCmdOverDif)=DefaultReal(nCmdOverDif)
        else if(OvSwitch.eq.3) then
          NacetlInt(nCmdiover)=MaxIndex
          NacetlReal(nCmdOverDif)=OverDifP
        endif
      endif
9999  return
100   format('(',3(i3,','),i3,')')
      end
