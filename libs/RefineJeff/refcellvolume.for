      subroutine RefCellVolume
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension dVdC(6),cosarr(3),sinarr(3),CorrP(6)
      CellVolPwd=1.
      pom1=1.
      pom2=2.
      do i=1,3
        pom=torad*CellPwd(i+3,KPhase)
        cosp=cos(pom)
        cosarr(i)=cosp
        sinarr(i)=sin(pom)
        CellVolPwd=CellVolPwd*CellPwd(i,KPhase)
        pom1=pom1-cosp**2
        pom2=pom2*cosp
      enddo
      pom1=pom1+pom2
      if(pom1.gt.0.) then
        CellVolPwd=CellPwd(1,KPhase)*CellPwd(2,KPhase)*
     1             CellPwd(3,KPhase)*sqrt(pom1)
      else
        CellVolPwd=0.
        CellVolPwds=0.
        go to 9999
      endif
      do i=1,3
        dVdC(i)=CellVolPwd/CellPwd(i,KPhase)
      enddo
      pom=CellVolPwd/pom1*torad
      do i=1,3
        j=i+3
        i1=mod(i  ,3)+1
        i2=mod(i+1,3)+1
        dVdC(j)=pom*sinarr(i)*(cosarr(i)-cosarr(i1)*cosarr(i2))
      enddo
      CellVolPwds=0.
      call OpenFile(85,fln(:ifln)//'.m85','unformatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      read(85) k
      do i=1,k
        read(85) j,pom,n
        do j=1,n
          read(85)
        enddo
      enddo
      ip=0
      ik=0
      do i=1,ICellPwd+ndoffPwd-1
        ik=ik+1
        if(ki(ik).eq.1) then
          ip=ip+1
          read(85) pom
        endif
      enddo
      ic=0
      k=0
      do 3300i=1,6
        if(ki(ik+i).eq.1) then
          ic=ic+1
          read(85)(pom,j=1,ip),(CorrP(j),j=1,ic)
        else
          k=k+i
          go to 3300
        endif
        jc=0
        do j=1,i
          k=k+1
          if(ki(ik+j).ne.0) then
            jc=jc+1
            if(i.eq.j) then
              pom=1.
            else
              pom=2.
            endif
            CellVolPwds=CellVolPwds+CorrP(jc)*dVdC(i)*dVdC(j)*pom
          endif
        enddo
3300  continue
      close(85)
      CellVolPwds=sqrt(CellVolPwds)
9999  return
      end
