      subroutine InvAt(i1,i2,isw)
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ia(6)
      character*15 t15
      character*12 lp
      character*5 lwx,lwy
      character*1 ln
      do i=i1,i2
        ip=PrvniKiAtomu(i)-1
        itfi=itf(i)
        if(itfi.le.0) itfi=2
        ivenr(1)=atom(i)
        do n=1,itfi+1
          nn=n-1
          nrank=TRank(nn)
          if(n.eq.1) then
            call zmena(lBasicPar(1),ai(i),sai(i),2,atom(i),ip,1.)
          else if(n.eq.2) then
            do j=1,nrank
              call zmena(lBasicPar(j+1),x(j,i),sx(j,i),2,atom(i),ip,1.)
            enddo
            if(itfi.eq.1) then
              if(lite(KPhase).eq.0) then
                pomt=1./episq
                beta(1,i)=beta(1,i)*pomt
                lBasicPar(8)(1:1)='U'
              else
                lBasicPar(8)(1:1)='B'
                pomt=1.
              endif
              call zmena(lBasicPar(8),beta(1,i),sbeta(1,i),2,atom(i),
     1                   ip,pomt)
              if(lite(KPhase).eq.0) then
                beta(1,i)=beta(1,i)*episq
                sbeta(1,i)=sbeta(1,i)*episq
              endif
              ip=ip+5
            endif
          else if(n.eq.3) then
            if(itfi.gt.0) then
              if(lite(KPhase).eq.0) then
                ln='U'
                lBasicPar(8)(1:1)='U'
              else
                ln='B'
                lBasicPar(8)(1:1)='B'
              endif
              call boueq(beta(1,i),sbeta(1,i),lite(KPhase),pomo,spomo,
     1                   isw)
              do j=1,nrank
                call indext(j,ia(1),ia(2))
                write(lp,100)(ia(m),m=1,nn)
                lp=ln//lp(:nn)
                if(lite(KPhase).eq.0) then
                  pomt=1./urcp(j,isw,KPhase)
                  beta(j,i)=beta(j,i)*pomt
                else
                  pomt=1.
                endif
                call zmena(lp,beta(j,i),sbeta(j,i),2,atom(i),ip,pomt)
                if(lite(KPhase).eq.0) then
                  pomt=1./pomt
                  beta(j,i)=beta(j,i)*pomt
                  sbeta(j,i)=sbeta(j,i)*pomt
                endif
              enddo
              ivenr(1)=ivenr(1)(1:InvDel)//'   '//lBasicPar(8)(1:1)//
     1                 'eq'
              call boueq(beta(1,i),sbeta(1,i),lite(KPhase),pomn,spom,
     1                   isw)
              call pridej(pomo,spom,pomn)
              rxa(11)=0.
              if(lite(KPhase).eq.0) ln='B'
            else
              ip=ip+6
            endif
          else
            ln=char(ichar(ln(1:1))+1)
            do j=1,nrank
              call indexc(j,nn,ia)
              write(lp,100)(ia(k),k=1,nn)
              lp=ln//lp(:nn)
              if(n.eq.4) then
                call zmena(lp,c3(j,i),sc3(j,i),2,atom(i),ip,1.)
              else if(n.eq.5) then
                call zmena(lp,c4(j,i),sc4(j,i),2,atom(i),ip,1.)
              else if(n.eq.6) then
                call zmena(lp,c5(j,i),sc5(j,i),2,atom(i),ip,1.)
              else
                call zmena(lp,c6(j,i),sc6(j,i),2,atom(i),ip,1.)
              endif
              if(mod(j,10).eq.0.or.j.eq.nrank) call tisk
            enddo
          endif
          if((n.eq.2.and.itfi.eq.1).or.n.eq.3) call tisk
        enddo
        if(ifr(i).gt.0) then
          lp='rfree'
          call zmena(lp,xfr(i),sxfr(i),2,atom(i),ip,1.)
          call tisk
        endif
        if(NDim(KPhase).le.3) then
          if(lasmax(i).gt.0) then
            lp='Pc'
            call zmena(lp,popc(i),spopc(i),2,atom(i),ip,1.)
            lp='Pv'
            call zmena(lp,popv(i),spopv(i),2,atom(i),ip,1.)
            lp=lk1
            call zmena(lp,kapa1(i),skapa1(i),2,atom(i),ip,1.)
            if(lasmax(i).gt.1) then
              lp=lk2
              call zmena(lp,kapa2(i),skapa2(i),2,atom(i),ip,1.)
              call tisk
              k=0
              do l=0,lasmax(i)-2
                do j=1,2*l+1
                  k=k+1
                  n=mod(j,2)
                  write(lp,'(''P'',2i1)') l,j/2
                  if(n.eq.0) then
                    lp(4:4)='+'
                  else if(j.ne.1) then
                    lp(4:4)='-'
                  endif
                  call zmena(lp,popas(k,i),spopas(k,i),2,atom(i),ip,1.)
                  if(InvDel.gt.115) call tisk
                enddo
              enddo
            endif
            if(InvDel.gt.10) call tisk
          endif
          go to 3150
        endif
        do 3000n=1,itfi+1
          nn=n-1
          nrank=TRank(nn)
          kmod=KModA(n,i)
          if(kmod.le.0) go to 3000
          if(InvDel.gt.10) call tisk
          if(n.eq.3) then
            if(lite(KPhase).eq.0) then
              lBasicPar(8)(1:1)='U'
            else
              lBasicPar(8)(1:1)='B'
            endif
            ln=lBasicPar(8)(1:1)
          else if(n.gt.3) then
            ln=char(ichar('C')+n-4)
          endif
          kwp=0
          do j=1,kmod
            if(n.gt.1.and.InvDel.gt.10) call tisk
            if(TypeModFun(i).eq.0) then
              write(lwx,'(''sin'',i2)') j
              write(lwy,'(''cos'',i2)') j
            else
              kwp=kwp+1
              write(lwx,'(''ort'',i2)') kwp
              kwp=kwp+1
              write(lwy,'(''ort'',i2)') kwp
            endif
            call zhusti(lwx)
            call zhusti(lwy)
            if(n.eq.1.and.j.eq.1) then
              if(KFA(1,i).eq.0) then
                lp=lBasicPar(12)
              else
                lp='delta'
              endif
              call zmena(lp,a0(i),sa0(i),2,atom(i),ip,1.)
            endif
            do k=1,nrank
              if(n.eq.1) then
                if(KFA(1,i).eq.0.or.j.lt.kmod) then
                  lp='o'//lwx
                else
                  lp='x40'
                endif
                call zmena(lp,ax(j,i),sax(j,i),2,atom(i),ip,1.)
              else if(n.eq.2) then
                if(KFA(2,i).eq.0.or.j.lt.kmod) then
                  lp=smbx(k)//lwx
                else
                  lp=smbx(k)//'-slope'
                endif
                call zmena(lp,ux(k,j,i),sux(k,j,i),2,atom(i),ip,1.)
              else if(n.eq.3) then
                if(lite(KPhase).eq.0) then
                  pomt=1./urcp(k,isw,KPhase)
                  bx(k,j,i)=bx(k,j,i)*pomt
                else
                  pomt=1.
                endif
                pom=bx(k,j,i)*pomt
                call indext(k,ia(1),ia(2))
                write(lp,100)(ia(m),m=1,nn)
                lp=ln//lp(:nn)//lwx
                call zmena(lp,bx(k,j,i),sbx(k,j,i),2,atom(i),ip,pomt)
                if(lite(KPhase).eq.0) then
                  pomt=1./pomt
                   bx(k,j,i)= bx(k,j,i)*pomt
                  sbx(k,j,i)=sbx(k,j,i)*pomt
                endif
              else
                call indexc(k,nn,ia)
                write(lp,100)(ia(m),m=1,nn)
                lp=ln//lp(:nn)//lwx
                if(n.eq.4) then
                  call zmena(lp,c3x(k,j,i),sc3x(k,j,i),2,atom(i),ip,1.)
                else if(n.eq.5) then
                  call zmena(lp,c4x(k,j,i),sc4x(k,j,i),2,atom(i),ip,1.)
                else if(n.eq.6) then
                  call zmena(lp,c5x(k,j,i),sc5x(k,j,i),2,atom(i),ip,1.)
                else
                  call zmena(lp,c6x(k,j,i),sc6x(k,j,i),2,atom(i),ip,1.)
                endif
              endif
              if(InvDel.gt.115) call tisk
            enddo
            if(n.ge.3.and.InvDel.gt.10) call tisk
            do k=1,nrank
              if(n.eq.1) then
                if(KFA(1,i).ne.0.and.j.eq.kmod) then
                  ip=ip+1
                  cycle
                endif
                call zmena('o'//lwy,ay(j,i),say(j,i),2,atom(i),ip,1.)
              else if(n.eq.2) then
                if(KFA(2,i).eq.0.or.j.lt.kmod) then
                  lp=smbx(k)//lwy
                else
                  if(k.eq.nrank-2) then
                    lp='x40'
                  else if(k.eq.nrank-1) then
                    lp='delta'
                  else
                    ip=ip+1
                    cycle
                  endif
                endif
                call zmena(lp,uy(k,j,i),suy(k,j,i),2,atom(i),ip,1.)
              else if(n.eq.3) then
                if(lite(KPhase).eq.0) then
                  pomt=1./urcp(k,isw,KPhase)
                  by(k,j,i)=by(k,j,i)*pomt
                else
                  pomt=1.
                endif
                call indext(k,ia(1),ia(2))
                write(lp,100)(ia(m),m=1,nn)
                lp=ln//lp(:nn)//lwy
                call zmena(lp,by(k,j,i),sby(k,j,i),2,atom(i),ip,pomt)
                if(lite(KPhase).eq.0) then
                  pomt=1./pomt
                   by(k,j,i)= by(k,j,i)*pomt
                  sby(k,j,i)=sby(k,j,i)*pomt
                endif
              else
                call indexc(k,nn,ia)
                write(lp,100)(ia(m),m=1,nn)
                lp=ln//lp(:nn)//lwy
                if(n.eq.4) then
                  call zmena(lp,c3y(k,j,i),sc3y(k,j,i),2,atom(i),ip,1.)
                else if(n.eq.5) then
                  call zmena(lp,c4y(k,j,i),sc4y(k,j,i),2,atom(i),ip,1.)
                else if(n.eq.6) then
                  call zmena(lp,c5y(k,j,i),sc5y(k,j,i),2,atom(i),ip,1.)
                else
                  call zmena(lp,c6y(k,j,i),sc6y(k,j,i),2,atom(i),ip,1.)
                endif
              endif
              if(InvDel.gt.115) call tisk
            enddo
          enddo
          if(n.eq.3.and.lite(KPhase).eq.0) ln='B'
3000    continue
        do j=1,itfi+1
          if(KModA(j,i).gt.0) then
            call zmena(lBasicPar(11),phf(i),sphf(i),2,atom(i),ip,1.)
            call tisk
            go to 3150
          endif
        enddo
3150    if(MagPar(i).gt.0) then
          do k=1,3
            if(KUsePolar(i).eq.0) then
              lp='M'//smbx(k)//'0'
              pom=CellPar(k,isw,KPhase)
              sm0(k,i)=sm0(k,i)*pom
            else
              lp='M'//smbp(k)//'0'
              pom=1.
            endif
            call zmena(lp,sm0(k,i),ssm0(k,i),2,atom(i),ip,pom)
            if(KUsePolar(i).eq.0) then
               sm0(k,i)= sm0(k,i)/pom
              ssm0(k,i)=ssm0(k,i)/pom
            endif
          enddo
          call tisk
          do j=2,MagPar(i)
            write(t15,'(i2)') j-1
            call zhusti(t15)
            do k=1,3
              if(KUsePolar(i).eq.0) then
                lp='M'//smbx(k)//'sin'//t15(:idel(t15))
                pom=CellPar(k,isw,KPhase)
                smx(k,j-1,i)=smx(k,j-1,i)*pom
              else
                lp='M'//smbp(k)//'sin'//t15(:idel(t15))
                pom=1.
              endif
              call zmena(lp,smx(k,j-1,i),ssmx(k,j-1,i),2,atom(i),ip,pom)
              if(KUsePolar(i).eq.0) then
                 smx(k,j-1,i)= smx(k,j-1,i)/pom
                ssmx(k,j-1,i)=ssmx(k,j-1,i)/pom
              endif
            enddo
            do k=1,3
              if(KUsePolar(i).eq.0) then
                lp='M'//smbx(k)//'cos'//t15(:idel(t15))
                pom=CellPar(k,isw,KPhase)
                smy(k,j-1,i)=smy(k,j-1,i)*pom
              else
                lp='M'//smbp(k)//'cos'//t15(:idel(t15))
                pom=1.
              endif
              call zmena(lp,smy(k,j-1,i),ssmy(k,j-1,i),2,atom(i),ip,pom)
              if(KUsePolar(i).eq.0) then
                 smy(k,j-1,i)= smy(k,j-1,i)/pom
                ssmy(k,j-1,i)=ssmy(k,j-1,i)/pom
              endif
            enddo
            call tisk
          enddo
        endif
      enddo
100   format(6i1)
      return
      end
