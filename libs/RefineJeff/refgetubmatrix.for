      subroutine RefGetUBMatrix
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      KRefBlockIn=KRefBlock
      KRefBlock=0
      do i=1,NRefBlock
        if(RefDatCorrespond(i).ne.KDatBlock) then
          KRefBlock=i
          exit
        endif
      enddo
      KRefBlock=max(KRefBlock,1)
      do i=1,3
        do j=1,3
          UBRef(i,j)=UB(i,j,KRefBlock)
        enddo
      enddo
      call MatInv(UBRef,UBRefI,pom,3)
      KRefBlock=KRefBlockIn
      return
      end
