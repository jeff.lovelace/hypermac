      subroutine RefRestrainWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/RestrainQuest/ RestrainType,nEdwValue,nEdwESD,nEdwAtoms,
     1     FixValue,FixValueESD,StringOfAtoms,TypeOfRestrain,FixString
      character*256 Command,StringOfAtoms,EdwStringQuest,t256
      character*80  t80
      character*8   FixString
      integer RestrainType,TypeOfRestrain
      save /RestrainQuest/
      ich=0
      Command=FixString
      if(TypeOfRestrain.eq.1) then
        call FeQuestRealFromEdw(nEdwValue,FixValue)
        write(Cislo,100) FixValue
        call ZdrcniCisla(Cislo,1)
      else if(TypeOfRestrain.eq.2) then
        Cislo='='
      else
        Cislo='*'
      endif
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwESD,FixValueESD)
      write(Cislo,100) FixValueESD
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      t256=EdwStringQuest(nEdwAtoms)
      if(t256.eq.' ') go to 9100
      call RefRestrainCheckAtomString(t256,t80)
      if(t80.ne.' ') then
        ich=1
        call FeChybne(-1.,-1.,t80,' ',SeriousError)
        go to 9999
      endif
      Command=Command(:idel(Command)+1)//
     1        StringOfAtoms(:idel(StringOfAtoms))
      go to 9999
9100  ich=1
9999  return
100   format(f15.4)
      end
