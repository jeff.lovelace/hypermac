      subroutine RefLoopPwd(RefineEnd,lst0,ich)
      use Refine_mod
      use RefPowder_mod
      use Powder_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension dera(:,:),afoura(:),bfoura(:),abasa(:),bbasa(:),
     1          affreea(:),bffreea(:),afsta(:),bfsta(:),Yssa(:),
     2          affreesta(:),bffreesta(:),ycqa(:),yoqa(:),ywa(:),
     3          ncprofa(:),ynia(:),ysqa(:),yna(:),sinthla(:),yka(:)
      dimension th12(2),hhh(6),DerRoughness(2)
      character*80 t80,t80p
      integer SkipFlag(MxPhases),SkipFlagSet(MxPhases),ReflPrispela(:)
      logical RefineEnd,EqIV0,ReflFulfilsCond
      real LpCorrection,LpArg
      double precision p1,p2,ratio
      allocatable dera,abasa,bbasa,afoura,bfoura,affreea,bffreea,afsta,
     1            bfsta,Yssa,affreesta,bffreesta,ycqa,yoqa,ywa,
     2            ncprofa,ynia,ysqa,yna,sinthla,yka,ReflPrispela
      allocate(dera(nLSRS,MxRefPwd),afoura(MxRefPwd),bfoura(MxRefPwd),
     1         abasa(MxRefPwd),bbasa(MxRefPwd),affreea(MxRefPwd),
     2         bffreea(MxRefPwd),afsta(MxRefPwd),bfsta(MxRefPwd),
     3         Yssa(MxRefPwd),affreesta(MxRefPwd),bffreesta(MxRefPwd),
     4         ycqa(MxRefPwd),yoqa(MxRefPwd),ywa(MxRefPwd),
     5         ncprofa(MxRefPwd),ynia(MxRefPwd),ysqa(MxRefPwd),
     6         yna(MxRefPwd),sinthla(MxRefPwd),yka(MxRefPwd),
     7         ReflPrispela(MxRefPwd))
      nRFacProf(KDatBlock)=0
      itstder=0
      ifsqo=ifsq
      ifsq=1
      if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     2  'begin'
      if(.not.CalcDer) then
        if(NDatBlock.gt.1)
     1    write(LnPrf,100)
     2      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     3      'begin'
        if(isTOF) then
          if(TOFInD) then
            i=2
          else
            i=1
          endif
        else if(isED) then
          i=3
        else
          i=0
        endif
        write(LnPrf,'(20i5)') 2,NAlfa(KDatBlock)-1,i,NPhase,
     1                        (NDim(i),i=1,NPhase)
      endif
      pom=0.
      do i=1,mxscu
        pom=pom+sc(i,KDatBlock)
      enddo
      if(pom.le.0.) then
        if(NAtCalc.le.0.or.DoLeBail.ne.0) then
          t80 ='model profile doesn''t fit to observed one at all.'
          t80p='Check background and Bragg peak positions.'
        else
          if(NDatBlock.gt.1) then
            Cislo=DatBlockName(KDatBlock)
            t80='scale factors for '//Cislo(:idel(Cislo))//
     1          ' went to zeros.'
          else
            t80 ='scale factors went to zeros.'
          endif
          t80p=' '
        endif
        call FeChybne(-1.,-1.,t80,t80p,SeriousError)
        go to 9100
      endif
      if(isimul.gt.0.and.ksimul.eq.2) then
        LnSim=NextLogicNumber()
        call OpenDatBlockM90(LnSim,KDatBlock,FileSimul)
        call RefReadFobsProlog(LnSim)
      else
        LnSimul=0
      endif
      RNumProf=0.
      RDenProf=0.
      wRNumProf=0.
      wRDenProf=0.
      cRDenProf=0.
      wcRDenProf=0.
1100  call PwdSetBackground
      SkipFlagSet=0
      call SetIntArrayTo(SkipFlagSet,NPhase,1)
      nrefout=0
      NFirst=1
      NRefPwd=0
      icm=0
      call SetIntArrayTo(ncprofa,MxRefPwd,0)
      if(NAtCalc.gt.0.and.DoLeBail.eq.0) then
        if(itstder.ne.0.and.LstRef.ne.0) rewind LstRef
      endif
1500  if(NRefPwd.ge.MxRefPwd) go to 3240
      NRefPwd=NRefPwd+1
      KPh=max(KPhPwdArr(NRefPwd),1)
      KPhase=KPh
      call CopyVekI(ihPwdArr(1,NRefPwd),ihread,maxNDim)
      call FromIndSinthl(ihread,hhh,sinthl,sinthlq,1,0)
      sinthla(NRefPwd)=sinthl
      iq=iqPwdArr(NRefPwd)
      if(.not.isTOF.and..not.isED) then
        if(sinthl*LamPwd(NAlfa(KDatBlock),KDatBlock).gt..99999) then
          iqPwdArr(NRefPwd)=0
          go to 1500
        endif
      endif
      if(FCalcPwdArr(NRefPwd).gt.0.) then
        Fcalc=sqrt(FCalcPwdArr(NRefPwd))*sc(iq,KDatBlock)*.01
      else
        Fcalc=0.
      endif
      af=Fcalc
      bf=0.
      if(nFlowChart.gt.0) then
        call FeFlowChartEvent(noread,ie)
        if(ie.ne.0) call EventRefine(ie,RefineEnd,*9100)
      endif
      nic=.true.
      if(calcder) call SetRealArrayTo(der,mxscutw,0.)
      if(NAtCalc.gt.0.and.DoLeBail.eq.0) then
        extkor=1.
        extkorm=1.
        call CopyVekI(ihread,ihref,NDim(KPhase))
        mmabsm=-999
        call RefLoopBefore(0,1,ich)
        if(ich.eq.1) then
          iqPwdArr(NRefPwd)=0
          ich=0
          go to 1500
        endif
        if(ksimul.eq.1.or.isimul.le.0) then
          KPhasePredCalc=KPhase
          call calc(1)
          KPhase=KPhasePredCalc
        else
          call RefReadFObs(ihread,Fcalc)
        endif
        if(ErrFlag.eq.2) then
          ErrFlag=0
          go to 9300
        else if(ErrFlag.ne.0) then
          ErrFlag=0
          go to 9100
        endif
        call RefInfoKolaps(lst0,*9100)
      endif
      IZdvih=(KDatBlock-1)*MxScAll
      if(calcder) then
        der(iq+IZdvih)=2.*Fcalc**2/sc(iq,KDatBlock)
        der(MxSc+1+IZdvih)=-2.*Fcalc**2*sinthlq*episq
        if(NPhase.gt.1) then
          pom=sctw(KPhase,KDatBlock)
          if(pom.ne.0.) then
            pom=Fcalc**2/pom
          else
            pom=0.
          endif
          if(KPhase.eq.1) then
            do k=mxscu+1,mxscutw
              der(k+IZdvih)=-pom
            enddo
          else
            der(mxscu+KPhase-1+IZdvih)=pom
          endif
        endif
      endif
      call SetPref(ihread,pref)
      pom=MultPwdArr(NRefPwd)*pref
      ypeakp=Fcalc**2*pom
      if(Fcalc.gt.0.) then
        pom=1./Fcalc**2
      else
        pom=0.
      endif
      if(CalcDer) then
        call Dsetall
        j=0
        do i=1,ndoffPwd
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        do i=ndoff,PosledniKiAtInd
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        do i=PrvniKiAtMol,PosledniKiAtMol
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        do i=PrvniKiMol,PosledniKiMol
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        do i=PrvniKiAtXYZMode,PosledniKiAtXYZMode
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        do i=PrvniKiAtMagMode,PosledniKiAtMagMode
          if(ki(i).ne.0) then
            j=j+1
            dera(j,NRefPwd)=der(i)*pom
          endif
        enddo
        npspp=j
      endif
      do NLamPwd=1,NAlfa(KDatBlock)
        DDActual=.5/sinthl
        if(isTOF) then
          LpArg=DDActual
          th=PwdD2TOF(LpArg)
          peak=th
        else if(isED) then
          LpArg=1.
          th=EDEnergy2D(KDatBlock)*2.*sinthl
          peak=th
        else
          pom=sinthl*LamPwd(NLamPwd,KDatBlock)
          if(pom.gt..99999) then
            iqPwdArr(NRefPwd)=0
            cycle
          endif
          th=asin(pom)
          LpArg=th
        endif
        ypeak=ypeakp/LpCorrection(LpArg)
        if(ypeak.lt.1.e-30) ypeak=0.
        call SetProfFun(ihread,hhh,th)
        if(isTOF.or.isED) then
          th12(NLamPwd)=th
        else
          th12(NLamPwd)=2.*th/torad
          if(NLamPwd.eq.2) ypeak=ypeak*LamRatPwd(KDatBlock)
        endif
        ypeaka(NLamPwd,NRefPwd)=ypeak
        if(NLamPwd.eq.1) then
          pom=Fcalc/(sc(1,KDatBlock)*.01)
          ycqa(NRefPwd)=pom**2
          yoqa(NRefPwd)=0.
          ysqa(NRefPwd)=0.
          yssa(NRefPwd)=0.
          ywa(NRefPwd)=0.
          yna(NRefPwd)=0.
          yka(NRefPwd)=0.
          ncprofa(NRefPwd)=0
          if(NAtCalc.gt.0.and.DoLeBail.eq.0) then
            abasa(NRefPwd)=a
            bbasa(NRefPwd)=b
            afoura(NRefPwd)=afour(1,1)
            bfoura(NRefPwd)=bfour(1,1)
            affreea(NRefPwd)=affree
            bffreea(NRefPwd)=bffree
            affreesta(NRefPwd)=affreest
            bffreesta(NRefPwd)=bffreest
            afsta(NRefPwd)=afst
            bfsta(NRefPwd)=bfst
          else
            abasa(NRefPwd)=pom
            bbasa(NRefPwd)=0.
            afoura(NRefPwd)=pom
            bfoura(NRefPwd)=0.
            affreea(NRefPwd)=pom
            bffreea(NRefPwd)=0.
            affreesta(NRefPwd)=pom
            bffreesta(NRefPwd)=0.
            afsta(NRefPwd)=pom
            bfsta(NRefPwd)=0.
          endif
        endif
      enddo
      if(.not.CalcDer) then
        if(Nalfa(KDatBlock).eq.2) then
          write(LnPrf,PrfFormat)(ihread(i),i=1,maxNDim),
     1      MultPwdArr(NRefPwd),KPhase,
     2      (th12(i),shifta(i,NRefPwd)/torad,fwhma(i,NRefPwd)/torad,
     3      ypeaka(i,NRefPwd),i=1,NAlfa(KDatBlock))
        else
          if(isTOF.or.isED) then
            ShiftA1=shifta(1,NRefPwd)
            FWHMA1=fwhma(1,NRefPwd)
          else
            ShiftA1=shifta(1,NRefPwd)/torad
            FWHMA1=fwhma(1,NRefPwd)/torad
          endif
          write(LnPrf,PrfFormat)(ihread(i),i=1,maxNDim),
     1      MultPwdArr(NRefPwd),KPhase,
     2      th12(1),shifta1,fwhma1,ypeaka(1,NRefPwd)
        endif
      endif
      go to 1500
3240  if(isTOF) then
        ifrom=NPnts
        ito=1
        istep=-1
      else
        ifrom=1
        ito=NPnts
        istep=1
      endif
      do 3800i=ifrom,ito,istep
        Ynia=0.
        if(nFlowChart.gt.0) then
          call FeFlowChartEvent(noread,ie)
          if(ie.ne.0) call EventRefine(ie,RefineEnd,*9100)
        endif
        if(isTOF.or.isED) then
          XPwdi=XPwd(i)
          RadPwdi=XPwdi
        else
          XPwdi=XPwd(i)
          RadPwdi=XPwdi*ToRad
        endif
        YoPwdi=YoPwd(i)
        if(YoPwdi.le.0.) then
          YcPwd(i)=-YoPwdi
          YcPwdInd(i,1:NPhase)=-YoPwdi
          go to 3800
        endif
        if(YfPwd(i).eq.0) then
          YcPwd(i)=YoPwdi
          YcPwdInd(i,1:NPhase)=0.
          YcPwdInd(i,1)=YoPwdi
          go to 3800
        endif
        do j=1,NskipPwd(KDatBlock)
          if(XPwdi.ge.SkipPwdFr(j,KDatBlock).and.
     1       XPwdi.le.SkipPwdTo(j,KDatBlock)) then
            YcPwd(i)=abs(YoPwdi)
            YcPwdInd(i,1:NPhase)=YcPwd(i)
            go to 3800
          endif
        enddo
        if(YsPwd(i).le.0.) YsPwd(i)=sqrt(YoPwdi)
        YsPwdi=YsPwd(i)
        if(CalcDer) then
          call SetRealArrayTo(der(ndoffPwd+1),MxParPwd,0.)
          call SetRealArrayTo(dc,nLSRS,0.)
        endif
        if(isTOF) then
          if(KUseTOFAbs(KDatBlock).gt.0) then
            Absorption=PwdTOFAbsorption(XPwdi,TOFAbsPwd(KDatBlock),
     1                                  DAbsorption)
          else
             Absorption=1.
            DAbsorption=0.
          endif
        else if(isED) then
          Absorption=1.
        else
          Absorption=1./PwdAbsorption(RadPwdi)
        endif
        call PwdRougness(RadPwdi,Roughness,DerRoughness)
        call PwdGetBackground(XPwdi,YcPwd(i),
     1                 der(IBackgPwd+ndoffPwd+(KDatBlock-1)*NParRecPwd),
     2                 DerBckgSc)
        YcPwdInd(i,1:NPhase)=0.
        YbPwd(i)=YcPwd(i)
3300    icp=0
        call SetIntArrayTo(ReflPrispela,MxRefPwd,0)
        SkipFlag=SkipFlagSet
        do 3600j=NFirst,MxRefPwd
          if(iqPwdArr(j).eq.0) go to 3600
          KPh=KPhPwdArr(j)
          KPhase=KPh
          call CopyVekI(ihPwdArr(1,j),ihref,NDim(KPhase))
          do k=1,NAlfa(KDatBlock)
            if((RadPwdi.gt.rdega(2,k,j).and..not.isTOF)
     1         .or.(RadPwdi.lt.rdega(1,k,j).and.isTOF)) then
              if(k.eq.NAlfa(KDatBlock)) then
                if(j.eq.NFirst) then
                  if(ncprofa(j).gt.0.and.Yna(j).gt.1.)
     1            call PwdWriteRefLst(LstRef,ihPwdArr(1,j),Yoqa(j),
     2              Ycqa(j),Ysqa(j),Yka(j),Ywa(j),Yssa(j),
     3              abasa(j),bbasa(j),afoura(j),bfoura(j),
     3              affreea(j),bffreea(j),afsta(j),bfsta(j),
     4              affreesta(j),bffreesta(j),nrefout,KPhPwdArr(j),
     5              sinthla(j),extkor,extkorm)
                  NFirst=NFirst+1
                else
                  go to 3600
                endif
              endif
            else
              if((RadPwdi.ge.rdega(1,k,j).and..not.isTOF)
     1           .or.(RadPwdi.le.rdega(2,k,j).and.isTOF)) then
                RDeg(1)=rdega(1,k,j)
                RDeg(2)=rdega(2,k,j)
                stred=(RDeg(1)+RDeg(2))*.5
                icp=icp+1
                shift=shifta(k,j)
                if(isTOF.or.isED) then
                  fnorm=1.
                  if(KAsym(KDatBlock).eq.IdPwdAsymTOF1.or.
     1               KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
                    Alpha12=Alpha12a(k,j)
                    Beta12=Beta12a(k,j)
                  endif
                else
                  if(KAsym(KDatBlock).eq.IdPwdAsymNone.or.
     1               KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
                    ntsim=1
                    fnorm=1.
                    tntsim=1.
                    pcot=0.
                  else if(KAsym(KDatBlock).eq.IdPwdAsymSimpson) then
                    fnorm=fnorma(k,j)
                    tntsim=tntsima(k,j)
                    ntsim=ntsima(k,j)
                    pcot=pcota(k,j)
                  else if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
                    cotgth=cotgtha(k,j)
                    cotg2th=cotg2tha(k,j)
                  else if(KAsym(KDatBlock).eq.IdPwdAsymDivergence) then
                    cos2th=cos2tha(k,j)
                    cos2thq=cos2thqa(k,j)
                  else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
                    call CopyVek(AxDivProfA(-25,k,j),AxDivProf(-25),51)
                    if(CalcDer) then
                      do l=1,7
                        call CopyVek(DerAxDivProfA(-25,l,k,j),
     1                               DerAxDivProf(-25,l),51)
                      enddo
                    endif
                  endif
                endif
                sqsg=sqsga(k,j)
                fwhm=fwhma(k,j)
                IBroadHKL=IBroadHKLa(j)
                sigp=sigpa(k,j)
                etaPwd=etaPwda(k,j)
                ypeakp=ypeaka(k,j)*Absorption
                dedffg=dedffga(k,j)
                dedffl=dedffla(k,j)
                dfwdg=dfwdga(k,j)
                dfwdl=dfwdla(k,j)
                peak=peaka(k,j)
                IBroadHKL=IBroadHKLa(j)
                prflp=prfl(RadPwdi-peak)
                ypeak=ypeakp*prflp
!                if(UseIncSpect(KDatBlock).eq.1) ypeak=ypeak*YiPwd(i)
                YcPwd(i)=YcPwd(i)+ypeak*RoughNess
                YcPwdInd(i,KPh)=YcPwdInd(i,KPh)+ypeak*RoughNess
                if(k.eq.1) then
                  ncprofa(j)=ncprofa(j)+1
                  if(RadPwdi.gt.Stred) then
                    ReflPrispela(j)=2
                  else
                    ReflPrispela(j)=1
                  endif
                  if(Prof0(j).ne.0) then
                    Ynia(j)=prflp/Prof0(j)
                  else
                    Ynia(j)=0.
                  endif
                endif
                dprdt=dprdt*ypeakp
                sigpart=sigpart*ypeakp
                gampart=gampart*ypeakp
                dprdc7=dprdc7*ypeakp
                dFdAlpha12=dFdAlpha12*ypeakp
                dFdBeta12=dFdBeta12*ypeakp
                if(calcder) then
                  do l=1,npspp
                    dc(l)=dc(l)+ypeak*dera(l,j)
                  enddo
                  IZdvih=(KDatBlock-1)*NParRecPwd
                  lp=IShiftPwd+IZdvih
                  if(KUseTOFJason(IShiftPwd).le.0) then
                    lk=lp+2
                  else
                    lk=lp+6
                  endif
                  do l=lp,lk
                    ll=l+ndoffPwd
                    der(ll)=der(ll)+coef(l,k,j)*dprdt
                  enddo
                  lp=ILamPwd+IZdvih
                  lk=lp+NAlfa(KDatBlock)-1
                  do l=lp,lk
                    ll=l+ndoffPwd
                    der(ll)=der(ll)+coef(l,k,j)*dprdt
                  enddo
                  lp=IAsymPwd+IZdvih
                  if(isTOF) then
                    if(KAsym(KDatBlock).eq.IdPwdAsymTOF1.or.
     1                 KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
                      lk=lp+1
                      do l=lp,lk
                        ll=l+ndoffPwd
                        der(ll)=der(ll)+coef(l,k,j)*dFdAlpha12
                      enddo
                      lp=lk+1
                      lk=lp+1
                      do l=lp,lk
                        ll=l+ndoffPwd
                        der(ll)=der(ll)+coef(l,k,j)*dFdBeta12
                      enddo
                      if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
                        lp=lk+1
                        lk=lp+1
                        do l=lp,lk
                          ll=l+ndoffPwd
                          der(ll)=der(ll)+coef(l,k,j)*dFdAlpha12
                        enddo
                        lp=lk+1
                        lk=lp+1
                        do l=lp,lk
                          ll=l+ndoffPwd
                          der(ll)=der(ll)+coef(l,k,j)*dFdBeta12
                        enddo
                        l=IShiftPwd+IZdvih+5
                        ll=l+ndoffPwd
                        der(ll)=der(ll)+coefp(l,k,j)*dFdAlpha12
     1                                 +coefq(1,k,j)*dFdBeta12
                        l=l+1
                        ll=ll+1
                        der(ll)=der(ll)+coefp(l,k,j)*dFdAlpha12
     1                                 +coefq(2,k,j)*dFdBeta12
                      endif
                    endif
                    if(KUseTOFAbs(KDatBlock).gt.0) then
                      lp=ITOFAbsPwd+IZdvih+ndoffPwd
                      der(lp)=der(lp)+ypeak*DAbsorption/Absorption
                    endif
                  else
                    if(KAsym(KDatBlock).eq.IdPwdAsymSimpson) then
                      ll=lp+ndoffPwd
                      der(ll)=der(ll)+coef(lp,k,j)*dprdc7
                    else if(KAsym(KDatBlock).eq.IdPwdAsymBerar.or.
     1                      KAsym(KDatBlock).eq.IdPwdAsymDivergence.or.
     2                      KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     3                then
                      ll=0
                      do l=lp,lp+NAsym(KDatBlock)-1
                        ll=ll+1
                        lll=l+ndoffPwd
                        der(lll)=der(lll)+dpdas(ll)*ypeakp
                      enddo
                    endif
                  endif
                  IZdvih=(KPh-1)*NParCellProfPwd
                  lp=ICellPwd+IZdvih
                  lk=lp+5
                  do l=lp,lk
                    ll=l+ndoffPwd
                    der(ll)=der(ll)+coef(l,k,j)*dprdt
                  enddo
                  lp=IQuPwd+IZdvih
                  lk=lp+3*NDimI(KPhase)-1
                  do l=lp,lk
                    ll=l+ndoffPwd
                    der(ll)=der(ll)+coef(l,k,j)*dprdt
                  enddo
                  IZdvih=IZdvih+(KDatBlock-1)*NParProfPwd
                  if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1               KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
                    lp=IGaussPwd+IZdvih
                    lk=lp+3
                    do l=lp,lk
                      ll=l+ndoffPwd
                      der(ll)=der(ll)+coef(l,k,j)*sigpart
                    enddo
                  endif
                  if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1               KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz
     2           .or.KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt)
     3              then
                    lp=ILorentzPwd+IZdvih
                    lk=lp+4
                    do l=lp,lk
                      ll=l+ndoffPwd
                      der(ll)=der(ll)+coef(l,k,j)*gampart
                    enddo
                  endif
                  if(IBroadHKL.gt.0) then
                    lp=IBroadHKLPwd+IZdvih+IBroadHKL-1
                    ll=lp+ndoffPwd
                    der(ll)=der(ll)+coefp(lp,k,j)*gampart+
     1                              coef (lp,k,j)*sigpart
                  endif
                  if(KStrain(KPh,KDatBlock).eq.IdPwdStrainTensor) then
                    lp=IZetaPwd+IZdvih
                    ll=lp+ndoffPwd
                    der(ll)=der(ll)+coefp(lp,k,j)*gampart+
     1                              coef (lp,k,j)*sigpart
                    lp=IStPwd+IZdvih
                    if(NDimI(KPhase).eq.1) then
                      n=35
                    else
                      n=15+NDimI(KPhase)
                    endif
                    lk=lp+n-1
                    do l=lp,lk
                      ll=l+ndoffPwd
                      der(ll)=der(ll)+coefp(l,k,j)*gampart+
     1                                coef (l,k,j)*sigpart
                    enddo
                  endif
                  if(KPref(KPh,KDatBlock).ne.IdPwdPrefNone) then
                    lp=IPrefPwd+IZdvih
                    lk=lp+1
                    do l=lp,lk
                      ll=l+ndoffPwd
                      der(ll)=der(ll)+coef(l,k,j)*ypeak
                    enddo
                  endif
                endif
              else
                if(k.eq.NAlfa(KDatBlock).and.SkipFlag(KPhase).gt.0)
     1            SkipFlag(KPhase)=0
                if(EqIV0(SkipFlag,NPhase)) go to 3700
              endif
            endif
          enddo
3600    continue
3700    if(CalcDer) then
          j=0
          do l=1,ndoffPwd
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          do l=ndoff,PosledniKiAtInd
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          do l=PrvniKiAtMol,PosledniKiAtMol
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          do l=PrvniKiMol,PosledniKiMol
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          do l=PrvniKiAtXYZMode,PosledniKiAtXYZMode
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          do l=PrvniKiAtMagMode,PosledniKiAtMagMode
            if(ki(l).ne.0) then
              j=j+1
              der(l)=dc(j)
            endif
          enddo
          if(KStrain(KPh,KDatBlock).eq.IdPwdStrainTensor) then
            l =IGaussPwd+3+ndoffPwd
            ll=ILorentzPwd+3+ndoffPwd
            der(ll)=der(ll)-2.*der(l)*LorentzPwd(3,1,KDatBlock)
          endif
          lp=IRoughPwd
          lk=lp+1
          do l=lp,lk
            ll=l+ndoffPwd
            der(ll)=(YcPwd(i)-YbPwd(i))/RoughNess*DerRoughness(l-lp+1)
          enddo
          der(2+IZdvih)=DerBckgSc
        endif
        icm=max(icp,icm)
        wt=1./YsPwdi**2
        if(isTOF.or.isED) then
          if(isimul.gt.0) go to 3800
        else
          snl=sin(XPwdi*ToRad*.5)/LamPwd(1,KDatBlock)
          if(isimul.gt.0.or.snl.lt.snlmn.or.snl.gt.snlmx) go to 3800
        endif
        if(icp.gt.0) then
          p1=max(YcPwd(i)-YbPwd(i),0.)
          p2=max(YoPwdi-YbPwd(i),0.)
          if(p1.gt.0.) then
            ratio=p2/p1
          else
            go to 3750
          endif
          y1=YcPwd(i)/YoPwdi
          y2=YbPwd(i)/YoPwdi
          wtp=wt/(y1*(ratio/p1)**2+1./p1**2+
     1            y2*((YoPwdi-YcPwd(i))/p1**2)**2)
          do j=NFirst,MxRefPwd
            if(ReflPrispela(j).le.0.or.ynia(j).lt..1) cycle
            if(ReflPrispela(j).eq.2.and.Yna(j).le.0.) Yna(j)=-Yna(j)
            if(Yna(j).gt.0.) then
              Yna(j)=Yna(j)+1.
            else
              Yna(j)=Yna(j)-1.
            endif
            Clen=Ycqa(j)*ratio
            Yoqa(j)=Yoqa(j)+Clen   *wtp
            Ysqa(j)=Ysqa(j)+Clen**2*wtp
            Ywa(j)=Ywa(j)+wtp
            Yka(j)=Yka(j)+1.
            Yssa(j)=Yssa(j)+Ycqa(j)**2*wtp
          enddo
        endif
3750    dy=YoPwdi-YcPwd(i)
        ady=abs(dy)
        ayo=abs(YoPwdi)
        wdy=sqrt(wt)*dy
        wdyq=wdy**2
        wyoq=wt*YoPwdi**2
        cwyoq=wt*(YoPwdi-YbPwd(i))**2
        if(NUseDatBlockActual.gt.1) then
          RNumOverall=RNumOverall+ady
          RDenOverall=RDenOverall+ayo
          cRDenOverall=cRDenOverall+abs(YoPwdi-YbPwd(i))
          wRNumOverall=wRNumOverall+wdyq
          wRDenOverall=wRDenOverall+wyoq
          wcRDenOverall=wcRDenOverall+cwyoq
        endif
        RNumProf=RNumProf+ady
        RDenProf=RDenProf+ayo
        cRDenProf=cRDenProf+abs(YoPwdi-YbPwd(i))
        wRNumProf=wRNumProf+wdyq
        wRDenProf=wRDenProf+wyoq
        wcRDenProf=wcRDenProf+cwyoq
        if(wdy*wdyOld.gt.0.) then
          z=sqrt(2.*pi*(wdyq+wdyqOld))
          z=z/(2.+z)
          BerarCorr=BerarCorr+z*abs(wdy)
          isw=0
        else
          z=0.
          isw=1
        endif
        BerarS=BerarS+(1.-z**2)*wdyq
        if(isw.eq.1) then
          BerarS=BerarS+BerarCorr**2
          BerarCorr=0.
        endif
        wdyOld=wdy
        wdyqOld=wdyq
        nRFacOverall=nRFacOverall+1
        nRFacProf(KDatBlock)=nRFacProf(KDatBlock)+1
        if(calcder) then
          SumaYcPwd=SumaYcPwd+(YcPwd(i)-YbPwd(i))*wt
          SumaYoPwd=SumaYoPwd+(YoPwdi  -YbPwd(i))*wt
          call DSetResPowder
          call Dsete(2)
          if(DatBlockDerTest.gt.0.and.
     1       abs(XPwd(i)-XPwdDerTest).lt..01) then
            call DerTest(ihPwdArr,yoPwdi,ycPwd(i),itstder)
            if(itstder.lt.0) go to 9999
            rewind 91
            go to 1100
          endif
          if(ipisd.ne.0.and.i.eq.ipisd) then
            call PisDer(88,ihPwdArr,yoPwdi,ycPwd(i),i,' ',' ')
            if(ipisd.gt.0) go to 9200
          endif
          call SumaRefine
        endif
3800  continue
3810  do j=NFirst,MxRefPwd
        if(ncprofa(j).gt.0.and.Yna(j).gt.1.)
     1    call PwdWriteRefLst(LstRef,ihPwdArr(1,j),Yoqa(j),Ycqa(j),
     2      Ysqa(j),Yka(j),Ywa(j),Yssa(j),abasa(j),bbasa(j),afoura(j),
     3      bfoura(j),affreea(j),bffreea(j),afsta(j),bfsta(j),
     4      affreesta(j),bffreesta(j),nrefout,KPhPwdArr(j),sinthla(j),
     5      extkor,extkorm)
      enddo
      if(isimul.gt.0) go to 5000
      GOFProf(ICyklMod6,KDatBlock)=wRNumProf
      NPar=NParPwdAll(KDatBlock)+NParStrAll(KDatBlock)
      if(nRFacProf(KDatBlock)-NPar.gt.1) then
        GOFProf(ICyklMod6,KDatBlock)=sqrt(GOFProf(ICyklMod6,KDatBlock)/
     1                         float(nRFacProf(KDatBlock)-NPar-1))
        BerarS=sqrt((BerarS+BerarCorr**2)/
     1          float(nRFacProf(KDatBlock)-NPar-1))
      else
        GOFProf(ICyklMod6,KDatBlock)=9999.99
        BerarS=9999.99
      endif
      BerarCorr=BerarS/GOFProf(ICyklMod6,KDatBlock)
      if(RDenProf.ne.0.) then
         RFacProf(ICyklMod6,KDatBlock)=RNumProf/RDenProf*100.
        cRFacProf(ICyklMod6,KDatBlock)=RNumProf/cRDenProf*100.
        wRNumProf=sqrt(wRNumProf)
        wRDenProf=sqrt(wRDenProf)
        wRFacProf(ICyklMod6,KDatBlock)=wRNumProf/wRDenProf*100.
        wcRDenProf=sqrt(wcRDenProf)
        wcRFacProf(ICyklMod6,KDatBlock)=wRNumProf/wcRDenProf*100.
        eRFacProf(KDatBlock)=
     1    sqrt(float(nRFacProf(KDatBlock)-NPar))/wRDenProf*100.
      else
         RFacProf(ICyklMod6,KDatBlock)=0.
        cRFacProf(ICyklMod6,KDatBlock)=0.
        wRFacProf(ICyklMod6,KDatBlock)=0.
        wcRDenProf=sqrt(wcRDenProf)
        wcRFacProf(ICyklMod6,KDatBlock)=0.
        eRFacProf(KDatBlock)=0.
      endif
5000  if(OrthoOrd.gt.0) call trortho(1)
      if(.not.CalcDer) then
        write(LnPrf,'('' 999'')')
        if(isimul.gt.0) then
          pom=0.
          do i=1,NPnts
            pom=max(YcPwd(i),pom)
            YoPwd(i)=YcPwd(i)
          enddo
          if(pom.gt.0.) then
            pom=10000./pom
            do i=1,NPnts
              YcPwd(i)=YcPwd(i)*pom
              YcPwdInd(i,1:NPhase)=YcPwdInd(i,1:NPhase)*pom
              YsPwd(i)=YsPwd(i)*pom
              YoPwd(i)=YcPwd(i)
              YiPwd(i)=0.
            enddo
          endif
          PrfPointsFormat='(f10.3,3e15.6,f10.3,i5,99e15.6)'
        endif
        do i=1,NPnts
          if(isTOF.or.isED) then
            pom=0.
          else
            th=XPwd(i)*ToRad
            csth=cos(th*.5)
            sin2th=sin(th)
            shift=(shiftPwd(1,KDatBlock)+shiftPwd(2,KDatBlock)*csth+
     1             shiftPwd(3,KDatBlock)*sin2th)*.01
            pom=th/ToRad+shift
          endif
          write(LnPrf,PrfPointsFormat)
     1      XPwd(i),YoPwd(i),YcPwd(i),YsPwd(i),pom,YfPwd(i),YiPwd(i),
     2      (YcPwdInd(i,j),j=1,NPhase),YbPwd(i)
        enddo
        write(LnPrf,'(''999.'')')
        if(isimul.gt.0) go to 9999
      endif
      go to 9999
9000  ErrFlag=1
9100  ich=1
      go to 9999
9200  ich=2
      go to 9999
9300  ich=3
9999  if(LstRef.ne.0) write(LstRef,100)
     1  DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
      if(.not.CalcDer.and.NDatBlock.gt.1) then
        write(LnPrf,100)
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
      endif
      deallocate(dera,abasa,bbasa,afoura,bfoura,affreea,bffreea,afsta,
     1           bfsta,Yssa,affreesta,bffreesta,ycqa,yoqa,ywa,ncprofa,
     2           ynia,ysqa,yna,sinthla,yka,ReflPrispela)
      ifsq=ifsqo
      if(isimul.gt.0.and.ksimul.eq.2) call RefReadFobsEpilog
      return
100   format(a,1x,a)
      end
