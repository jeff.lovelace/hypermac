      subroutine RefSetTrueKi(kip,Value)
      use Atoms_mod
      use Molec_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Value
      if(kip.le.NdOffPwd) then
        j=mod(kip-1,MxScAll)+1
        k=(kip-1)/MxScAll+1
        KiS(j,k)=Value
      else if(kip.le.NdOffED) then
        k=kip-NdOffPwd
        KiPwd(k)=0
      else if(kip.le.NdOff) then
        if(NMaxEDZone.gt.0) then
          k=kip-ndoffED
          l=mod(k-1,MxEDRef*NMaxEDZone)+1
          KDatB=(k-1)/(MxEDRef*NMaxEDZone)+1
          j=mod(l-1,MxEDRef)+1
          k=(l-1)/MxEDRef+1
          KiED(j,k,KDatB)=0
        endif
      else if(kip.lt.PrvniKiMol) then
        do ia=1,NAtAll
          if(kip.lt.PrvniKiAtomu(ia)+DelkaKiAtomu(ia)) then
            if(ia.gt.NAtInd.and.ia.lt.NAtMolFr(1,1)) then
              go to 9999
            else
              go to 1100
            endif
          endif
        enddo
        go to 9999
1100    KiA(kip-PrvniKiAtomu(ia)+1,ia)=Value
      else if(kip.lt.PrvniKiAtXYZMode) then
        do i=1,NMolec
          do j=1,mam(i)
            ia=j+(i-1)*mxp
            if(kip.lt.PrvniKiMolekuly(ia)+DelkaKiMolekuly(ia))
     1        go to 1200
          enddo
        enddo
1200    KiMol(kip-PrvniKiMolekuly(ia)+1,ia)=Value
      else if(kip.lt.PrvniKiAtMagMode) then
        kipp=PrvniKiAtXYZMode
        do i=1,NAtXYZMode
          if(kip.lt.kipp+NMAtXYZMode(i)) then
            KiAtXYZMode(kip-kipp+1,i)=Value
            exit
          else
            kipp=kipp+NMAtXYZMode(i)
          endif
        enddo
      else
        kipp=PrvniKiAtMagMode
        do i=1,NAtMagMode
          if(kip.lt.kipp+NMAtMagMode(i)) then
            KiAtMagMode(kip-kipp+1,i)=Value
            exit
          else
            kipp=kipp+NMAtMagMode(i)
          endif
        enddo
      endif
9999  return
      end
