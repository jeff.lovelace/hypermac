      subroutine RefUpdateOrtho
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      dimension xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1          FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),RSol(:)
      integer OrthoSelNew(:)
      allocatable xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1            YOld,RMat,RSide,RSol
      if(OrthoOrd.le.0) go to 9999
      allocate(xp(OrthoOrd),yp(OrthoOrd),OrthoSelNew(OrthoOrd),
     1         OrthoMatNew(OrthoOrd**2),OrthoMatNewI(OrthoOrd**2),
     2         FOld(OrthoOrd,NPoints),FNew(OrthoOrd,NPoints),
     3         YOld(NPoints),RMat(OrthoOrd*(OrthoOrd+1)),
     4         RSide(OrthoOrd),RSol(OrthoOrd))
      do ia=1,NAtInd
        if(TypeModFun(ia).ne.1) cycle
        X40=ax(1,ia)
        Delta=a0(ia)
        call UpdateOrtho(ia,X40,Delta,OrthoEps(ia),OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,ia)
        x4=X40-Delta*.5
        dx4=a0(ia)/float(NPoints-1)
        do m=1,NPoints
          do k=1,OrthoOrd
            kk=OrthoSel(k,ia)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMat(1,ia),yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
          x4=x4+dx4
        enddo
        RMat=0.
        do m=1,NPoints
          l=0
          do i=1,OrthoOrd
            do j=1,i
              l=l+1
              RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
            enddo
          enddo
        enddo
        call smi(RMat,OrthoOrd,ISing)
        do it=2,itf(ia)+1
          kmod=KModA(it,ia)
          if(kmod.le.0) cycle
          n=TRank(it-1)
          do i=1,n
            xp=0.
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
                if(it.eq.2) then
                  xp(k)=x(i,ia)
                else if(it.eq.3) then
                  xp(k)=beta(i,ia)
                else if(it.eq.4) then
                  xp(k)=c3(i,ia)
                else if(it.eq.5) then
                  xp(k)=c4(i,ia)
                else if(it.eq.6) then
                  xp(k)=c5(i,ia)
                else if(it.eq.7) then
                  xp(k)=c6(i,ia)
                endif
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  xp(k)=ux(i,km,ia)
                else if(it.eq.3) then
                  xp(k)=bx(i,km,ia)
                else if(it.eq.4) then
                  xp(k)=c3x(i,km,ia)
                else if(it.eq.5) then
                  xp(k)=c4x(i,km,ia)
                else if(it.eq.6) then
                  xp(k)=c5x(i,km,ia)
                else if(it.eq.7) then
                  xp(k)=c6x(i,km,ia)
                endif
              else
                if(it.eq.2) then
                  xp(k)=uy(i,km,ia)
                else if(it.eq.3) then
                  xp(k)=by(i,km,ia)
                else if(it.eq.4) then
                  xp(k)=c3y(i,km,ia)
                else if(it.eq.5) then
                  xp(k)=c4y(i,km,ia)
                else if(it.eq.6) then
                  xp(k)=c5y(i,km,ia)
                else if(it.eq.7) then
                  xp(k)=c6y(i,km,ia)
                endif
              endif
            enddo
            RSide=0.
            do m=1,NPoints
              call MultM(FOld(1,m),xp,YOld(m),1,OrthoOrd,1)
              do j=1,OrthoOrd
                RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
              enddo
            enddo
            call nasob(RMat,RSide,RSol,OrthoOrd)
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
                if(it.eq.2) then
                  x(i,ia)=RSol(k)
                else if(it.eq.3) then
                  beta(i,ia)=RSol(k)
                else if(it.eq.4) then
                  c3(i,ia)=RSol(k)
                else if(it.eq.5) then
                  c4(i,ia)=RSol(k)
                else if(it.eq.6) then
                  c5(i,ia)=RSol(k)
                else if(it.eq.7) then
                  c6(i,ia)=RSol(k)
                endif
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  ux(i,km,ia)=RSol(k)
                else if(it.eq.3) then
                  bx(i,km,ia)=RSol(k)
                else if(it.eq.4) then
                  c3x(i,km,ia)=RSol(k)
                else if(it.eq.5) then
                  c4x(i,km,ia)=RSol(k)
                else if(it.eq.6) then
                  c5x(i,km,ia)=RSol(k)
                else if(it.eq.7) then
                  c6x(i,km,ia)=RSol(k)
                endif
              else
                if(it.eq.2) then
                  uy(i,km,ia)=RSol(k)
                else if(it.eq.3) then
                  by(i,km,ia)=RSol(k)
                else if(it.eq.4) then
                  c3y(i,km,ia)=RSol(k)
                else if(it.eq.5) then
                  c4y(i,km,ia)=RSol(k)
                else if(it.eq.6) then
                  c5y(i,km,ia)=RSol(k)
                else if(it.eq.7) then
                  c6y(i,km,ia)=RSol(k)
                endif
              endif
            enddo
          enddo
        enddo
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
      enddo
      iak=NAtPosFr(1,1)-1
      do im=1,NMolec
        do ip=1,mam(im)
          ji=ip+(im-1)*mxp
          iap=iak+1
          iak=iak+iam(im)
          if(TypeModFunMol(ji).ne.1) cycle
          X40=axm(1,ji)
          Delta=a0m(ji)
          call UpdateOrtho(iap,X40,Delta,OrthoEpsMol(ji),OrthoSelNew,
     1                     OrthoOrd)
          call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1               OrthoMatNewI,iap)
          x4=X40-Delta*.5
          dx4=Delta/float(NPoints-1)
          do m=1,NPoints
            do k=1,OrthoOrd
              kk=OrthoSel(k,iap)
              km=(kk+1)/2
              if(kk.eq.0) then
                yp(k)=1.
              else if(mod(kk,2).eq.1) then
                yp(k)=sin(Pi2*km*x4)
              else
                yp(k)=cos(Pi2*km*x4)
              endif
            enddo
            call MultM(OrthoMat(1,iap),yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
            do k=1,OrthoOrd
              kk=OrthoSelNew(k)
              km=(kk+1)/2
              if(kk.eq.0) then
                yp(k)=1.
              else if(mod(kk,2).eq.1) then
                yp(k)=sin(Pi2*km*x4)
              else
                yp(k)=cos(Pi2*km*x4)
              endif
            enddo
            call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
            x4=x4+dx4
          enddo
          RMat=0.
          do m=1,NPoints
            l=0
            do i=1,OrthoOrd
              do j=1,i
                l=l+1
                RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
              enddo
            enddo
          enddo
          call smi(RMat,OrthoOrd,ISing)
          do it=1,5
            if(it.le.2) then
              kmod=KModM(2,ji)
              n=3
            else
              kmod=KModM(3,ji)
              if(it.eq.5) then
                n=9
              else
                n=6
              endif
            endif
            if(kmod.le.0) cycle
            do i=1,n
              xp=0.
              do k=1,2*kmod+1
                km=k/2
                if(k.eq.1) then
                  if(it.le.2) then
                    xp(k)=0.
                  else if(it.eq.3) then
                    xp(k)=tt(i,ji)
                  else if(it.eq.4) then
                    xp(k)=tl(i,ji)
                  else if(it.eq.6) then
                    xp(k)=ts(i,ji)
                  endif
                else if(mod(k,2).eq.0) then
                  if(it.eq.1) then
                    xp(k)=utx(i,km,ji)
                  else if(it.eq.2) then
                    xp(k)=urx(i,km,ji)
                  else if(it.eq.3) then
                    xp(k)=ttx(i,km,ji)
                  else if(it.eq.4) then
                    xp(k)=tlx(i,km,ji)
                  else if(it.eq.6) then
                    xp(k)=tsx(i,km,ji)
                  endif
                else
                  if(it.eq.1) then
                    xp(k)=uty(i,km,ji)
                  else if(it.eq.2) then
                    xp(k)=ury(i,km,ji)
                  else if(it.eq.3) then
                    xp(k)=tty(i,km,ji)
                  else if(it.eq.4) then
                    xp(k)=tly(i,km,ji)
                  else if(it.eq.6) then
                    xp(k)=tly(i,km,ji)
                  endif
                endif
              enddo
              RSide=0.
              do m=1,NPoints
                call MultM(FOld(1,m),xp,YOld(m),1,OrthoOrd,1)
                do j=1,OrthoOrd
                  RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
                enddo
              enddo
              call nasob(RMat,RSide,RSol,OrthoOrd)
              do k=1,2*kmod+1
                km=k/2
                if(k.eq.1) then
                  if(it.eq.3) then
                    tt(i,ji)=RSol(k)
                  else if(it.eq.4) then
                    tl(i,ji)=RSol(k)
                  else if(it.eq.6) then
                    tl(i,ji)=RSol(k)
                  endif
                else if(mod(k,2).eq.0) then
                  if(it.eq.1) then
                    utx(i,km,ji)=RSol(k)
                  else if(it.eq.2) then
                    urx(i,km,ji)=RSol(k)
                  else if(it.eq.3) then
                    ttx(i,km,ji)=RSol(k)
                  else if(it.eq.4) then
                    tlx(i,km,ji)=RSol(k)
                  else if(it.eq.6) then
                    tsx(i,km,ji)=RSol(k)
                  endif
                else
                  if(it.eq.1) then
                    uty(i,km,ji)=RSol(k)
                  else if(it.eq.2) then
                    ury(i,km,ji)=RSol(k)
                  else if(it.eq.3) then
                    tty(i,km,ji)=RSol(k)
                  else if(it.eq.4) then
                    tly(i,km,ji)=RSol(k)
                  else if(it.eq.6) then
                    tsy(i,km,ji)=RSol(k)
                  endif
                endif
              enddo
            enddo
          enddo
          OrthoX40Mol(ji)=X40
          OrthoDeltaMol(ji)=Delta
        enddo
      enddo
      deallocate(xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1           YOld,RMat,RSide,RSol)
9999  return
      end
