      subroutine Calcm1(i,klic)
      use Atoms_mod
      use Refine_mod
      use Bessel_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      save qcnt1,qcnt2,qcnt3,sumdlt,rstart,mss,msk,dadu3,dadu4,dbdu3,
     1     dbdu4,kmodsim,dada0,dbda0,itfi,kfsi,kfxi,kfbi,kmodsi,kmodxi,
     2     kmodbi,kmezx,kmezb
      if(klic.eq.0) then
        qcnt1=qcnt(1,i)
        qcnt2=qcnt(2,i)
        qcnt3=qcnt(3,i)
        itfi=itf(i)
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
          kfsi=KFA(1,i)
        else
          kfsi=0
        endif
        if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
          kfxi=KFA(2,i)
        else
          kfxi=0
        endif
        kfbi=KFA(3,i)
        if(kfxi.eq.0) then
          kmezx=kmodxi
        else if(kfxi.eq.1.or.kfxi.eq.4) then
          kmezx=kmodxi-1
        else if(kfxi.eq.2.or.kfxi.eq.5) then
          kmezx=kmodxi-2
        else if(kfxi.eq.3.or.kfxi.eq.6) then
          kmezx=kmodxi-3
        endif
        if(kfbi.eq.0) then
          kmezb=kmodbi
        else if(kfbi.eq.1) then
          kmezb=kmodbi-1
        endif
        if(kfsi.eq.0) then
          kmodsim=kmodsi
        else
          kmodsim=MaxUsedKw(KPhase)
        endif
        if(CalcDer.and.kmodsim.gt.0) then
          dada0=0.
          if(NCSymmRef(KPhase).eq.1) dbda0=0.
          do l=1,kmodsim
            dadax(l)=0.
            daday(l)=0.
            if(NCSymmRef(KPhase).eq.1) then
              dbdax(l)=0.
              dbday(l)=0.
            endif
          enddo
        endif
        nemod=kmodxi.le.0.and.kmodsim.le.0.and.kmodbi.le.0
        if(kfsi.eq.0) then
          sb(1)=a0(i)
        else
          sumdlt=1.
          do j=1,kmodsim
            daxddlt(j)=0.
            dayddlt(j)=0.
          enddo
          delta=a0(i)
          sb(1)=delta*sumdlt
        endif
        rstart=abs(sb(1))
        do j=1,kmodsim
          if(kfsi.eq.0) then
            axp=ax(j,i)
            ayp=ay(j,i)
          else
            axp=0.
            ayp=0.
            do k=1,kmodsi
              call smod(u,axp,ayp,daxddlt(j),dayddlt(j),daxdfct(k,j),
     1                  daydfct(k,j),daxdx40(k,j),daydx40(k,j),delta,
     2                  ax(k,i),ay(k,i),kw(1,j,1),kfsi)
            enddo
          endif
          uqr=axp**2+ayp**2
          u=sqrt(uqr)
          if(uqr.gt.0) then
            ksi(j)=atan2(-axp,ayp)
            uqr=1./uqr
            pom=u*.5
            sb(j+1)=pom
            if(CalcDer) then
              dsbdax(j)=axp/(u*pom)*.5
              dsbday(j)=ayp/(u*pom)*.5
              dksidax(j)=-ayp*uqr
              dksiday(j)= axp*uqr
            endif
          else
            sb(j+1)=0.
            ksi(j)=0.
            if(CalcDer) then
              dsbdax(j)=0.
              dsbday(j)=0.
              dksidax(j)=0.
              dksiday(j)=0.
            endif
          endif
          rstart=max(abs(sb(j+1)),rstart)
        enddo
        n=max(MaxUsedKw(KPhase),1)
        call SetIntArrayTo(mxs,n,0)
        call SetIntArrayTo(mxkk,n,0)
        call SetIntArrayTo(mbs,n,0)
        call SetIntArrayTo(mbk,n,0)
        mss=-kmodsim
        msk= kmodsim
      else if(klic.eq.1) then
        chi(1)=0.
        if(.not.nemod) redukce=rstart
        kk=NDerModFirst-1
        if(kmodsi.gt.0) kk=kk+1+2*kmodsi
        do k=1,kmodxi
          u1=0.
          do l=1,3
            kk=kk+1
            u1=u1+par(kk)*hj(l)
          enddo
          if(k.le.kmezx) then
            u2=0.
            do l=1,3
              kk=kk+1
              u2=u2+par(kk)*hj(l)
            enddo
            uqr=u1**2+u2**2
            if(uqr.le..00001) then
              mxs(k)=0
              mxkk(k)=0
              do l=0,mxkk(k)
                if(l.eq.0) then
                  besp(l+mxb+1,k)=1.
                  if(CalcDer) then
                    dbfdu1(l+mxb+1,k)=0.
                    dbfdu2(l+mxb+1,k)=0.
                  endif
                else
                  if(l.eq.1) then
                    besp( l+mxb+1,k)=0.
                    besp(-l+mxb+1,k)=0.
                    if(CalcDer) then
                      dbfdu1( l+mxb+1,k)= .5
                      dbfdu1(-l+mxb+1,k)=-.5
                      dbfdu2( l+mxb+1,k)= .5
                      dbfdu2(-l+mxb+1,k)=-.5
                    endif
                  else
                    besp( l+mxb+1,k)=0.
                    besp(-l+mxb+1,k)=0.
                    if(CalcDer) then
                      dbfdu1( l+mxb+1,k)=0.
                      dbfdu1(-l+mxb+1,k)=0.
                      dbfdu2( l+mxb+1,k)=0.
                      dbfdu2(-l+5,k)=0.
                    endif
                  endif
                endif
              enddo
              chi(k)=0.
              if(CalcDer) then
                dchidu1(k)=0.
                dchidu2(k)=0.
              endif
            else
              u=sqrt(uqr)
              do l=1,mxb
                if(u.lt.accur(l)/redukce) go to 2011
              enddo
              l=mxb
              kolaps=.true.
2011          mxs(k)=0
              mxkk(k)=l
              ru=1./u
              uqr=1./uqr
              chi(k)=atan2(u2,u1)
              call bessj(u,mxkk(k),besp(mxb+1,k),pomb)
              pomc=abs(besp(mxb+1,k))
              do l=1,mxkk(k)
                pom=besp(mxb+1+l,k)
                pomc=max(pomc,abs(pom))
                if(mod(l,2).eq.1) pom=-pom
                besp(-l+mxb+1,k)=pom
              enddo
              redukce=redukce*pomc
              if(CalcDer) then
                pomb=pomb*ru
                do l=mxkk(k),0,-1
                  pomc=besp(l+mxb+1,k)
                  poma=pomc*ru
                  if(pomc.ne.0.) pomc=1./pomc
                  pom=-pomb
                  if(l.ne.0) then
                    pom=pom+float(l)*ru*poma
                    pomu1=pom*u1*pomc
                    pomu2=pom*u2*pomc
                    dbfdu1(l+mxb+1,k)=pomu1
                    dbfdu2(l+mxb+1,k)=pomu2
                    dbfdu1(-l+mxb+1,k)=pomu1
                    dbfdu2(-l+mxb+1,k)=pomu2
                  else
                    dbfdu1(mxb+1,k)=pom*u1*pomc
                    dbfdu2(mxb+1,k)=pom*u2*pomc
                  endif
                  pomb=poma
                enddo
                dchidu1(k)=-uqr*u2
                dchidu2(k)= uqr*u1
              endif
            endif
          else
            kk=kk+1
            chi(k)=pi2*par(kk)
            kk=kk+1
            delta=par(kk)
            kk=kk+1
            if(kmodxi.eq.1.and.kmodsim.eq.0.and.kmodbi.eq.0) then
              lp=iabs(mj)
              lk=lp
            else
              lp=0
              lk=mxb
            endif
            isum3=0
            do l=lp,lk
              call xmod(besp(l+mxb+1,k),dbfdu1(l+mxb+1,k),
     1                  dbfdu2(l+mxb+1,k),dbfdu3(l+mxb+1,k),
     2                  u1,delta,-l,kfxi)
              call xmod(besp(-l+mxb+1,k),dbfdu1(-l+mxb+1,k),
     1                 dbfdu2(-l+mxb+1,k),dbfdu3(-l+mxb+1,k),
     2                 u1,delta,l,kfxi)
              if(abs(besp(-l+mxb+1,k)).lt.DifBess.and.
     1           abs(besp( l+mxb+1,k)).lt.DifBess) then
               isum3=isum3+1
              else
                isum3=0
              endif
              if(isum3.eq.3) go to 2041
            enddo
            mxs(k)=0
            mxs(k)=-lk
            mxkk(k)= lk
            go to 2042
2041        mxs(k)=0
            mxkk(k)= l-3
2042        dchidu1(k)=pi2
          endif
          mxdkw1(k)=kw(1,k,1)*(mxkk(k)-mxs(k))
          if(NDimI(KPhase).gt.1) mxdkw2(k)=kw(2,k,1)*(mxkk(k)-mxs(k))
          if(NDimI(KPhase).gt.2) mxdkw3(k)=kw(3,k,1)*(mxkk(k)-mxs(k))
        enddo
        do k=1,kmodbi
          bb1=0.
          do l=5,10
            kk=kk+1
            bb1=bb1-par(kk)*HCoefj(l)
          enddo
          if(k.le.kmezb) then
            bb2=0.
            do l=5,10
              kk=kk+1
              bb2=bb2-par(kk)*HCoefj(l)
            enddo
            uqr=bb1**2+bb2**2
            if(uqr.le.0.) then
              mbs(k)=0
              mbk(k)=0
              do l=0,mbk(k)
                eta(k)=0.
                if(CalcDer) then
                  detadb1(k)=0.
                  detadb2(k)=0.
                endif
                if(l.eq.0) then
                  besb(1,k)=1.
                  if(CalcDer) then
                    dbfdb1(1,k)=0.
                    dbfdb2(1,k)=0.
                  endif
                else
                  if(l.eq.1) then
                    besb(2,k)=0.
                    if(CalcDer) then
                      dbfdb1(2,k)=.5
                      dbfdb2(2,k)=.5
                    endif
                  else
                    besb(l+1,k)=0.
                    if(CalcDer) then
                      dbfdb1(l+1,k)=0.
                      dbfdb2(l+1,k)=0.
                    endif
                  endif
                endif
              enddo
            else
              u=sqrt(uqr)
              do l=1,mxb
                if(u.lt.accur(l)/redukce) go to 2071
              enddo
              l=mxb
              kolaps=.true.
2071          mbs(k)=0
              mbk(k)= l
              ru=1./u
              uqr=1./uqr
              eta(k)=atan2(bb2,bb1)
              call bessi(u,mbk(k),besb(1,k),pomb)
              pomc=0.
              do l=0,mbk(k)
                pomc=max(pomc,abs(besb(l+1,k)))
              enddo
              redukce=redukce*pomc
              if(CalcDer) then
                pomb=pomb*ru
                do l=mbk(k),0,-1
                  pomc=besb(l+1,k)
                  poma=pomc*ru
                  if(pomc.ne.0.) pomc=1./pomc
                  pom=pomb
                  if(l.ne.0) pom=pom+float(l)*ru*poma
                  dbfdb1(l+1,k)=pom*bb1*pomc
                  dbfdb2(l+1,k)=pom*bb2*pomc
                  pomb=poma
                enddo
                detadb1(k)=-uqr*bb2
                detadb2(k)= uqr*bb1
              endif
            endif
          else
            kk=kk+1
            eta(k)=pi2*par(kk)
            kk=kk+1
            delta=par(kk)
            kk=kk+4
            if(kmodbi.eq.1.and.kmodsim.eq.0.and.kmodxi.eq.0) then
              lp=mj
              lk=mj
            else
              lp=0
              lk=mxb
            endif
            isum3=0
            do l=lp,lk
              call bmod(besb(l+1,k),dbfdb1(l+1,k),dbfdb2(l+1,k),
     1                  dbfdb3(l+1,k),bb1,delta,delta,l,kfbi)
              if(abs(besb(l+1,k)).lt.DifBess) then
                isum3=isum3+1
              else
                isum3=0
              endif
              if(isum3.eq.3) go to 2091
            enddo
            mbs(k)=0
            mbk(k)= lk
            go to 2092
2091        mbs(k)=0
            mbk(k)= l-3
2092        detadb1(k)=pi2
          endif
          mbdkw1(k)=kw(1,k,1)*(mbk(k)-mbs(k))
          if(NDimI(KPhase).gt.1) mbdkw2(k)=kw(2,k,1)*(mbk(k)-mbs(k))
          if(NDimI(KPhase).gt.2) mbdkw3(k)=kw(3,k,1)*(mbk(k)-mbs(k))
        enddo
        if(atomic) then
          fip=PhiAng
        else
          fip=PhiAng-pi2*float(mj)*qcnt1
          if(NDimI(KPhase).gt.1) fip=fip-pi2*float(nj)*qcnt2
          if(NDimI(KPhase).gt.2) fip=fip-pi2*float(pj)*qcnt3
        endif
        cosij=0.
        sinij=0.
        if(CalcDer) then
          do k=1,kmodxi
            dadu1(k)=0.
            dadu2(k)=0.
            if(NCSymmRef(KPhase).eq.1) then
              dbdu1(k)=0.
              dbdu2(k)=0.
            endif
          enddo
          dadu3=0.
          dadu4=0.
          dbdu3=0.
          dbdu4=0.
          do k=1,kmodbi
            dadb1(k)=0.
            dadb2(k)=0.
            if(NCSymmRef(KPhase).eq.1) then
              dbdb1(k)=0.
              dbdb2(k)=0.
            endif
          enddo
          dadb3=0.
          dbdb3=0.
          dadb4=0.
          dbdb4=0.
        endif
        do 2500ms=mss,msk
          if(ms.eq.0) then
            kw171=0
            kw172=0
            kw173=0
          else
            k=iabs(ms)
            l=isign(1,ms)
            kw171=kw(1,k,1)*l
            if(NDimI(KPhase).gt.1) kw172=kw(2,k,1)*l
            if(NDimI(KPhase).gt.2) kw173=kw(3,k,1)*l
          endif
          do k=NDim(KPhase)-2,kmodxi
            mx(k)=mxs(k)
          enddo
          do k=1,kmodbi
            mb(k)=mbs(k)
          enddo
          mf=mj-kw171
          if(NDimI(KPhase).gt.1) mg=nj-kw172
          if(NDimI(KPhase).gt.2) mh=pj-kw173
          do k=NDim(KPhase)-2,kmodxi
            mf=mf-mx(k)*kw(1,k,1)
            if(NDimI(KPhase).gt.1) mg=mg-mx(k)*kw(2,k,1)
            if(NDimI(KPhase).gt.2) mh=mh-mx(k)*kw(3,k,1)
          enddo
          do k=1,kmodbi
            mf=mf-mb(k)*kw(1,k,1)
            if(NDimI(KPhase).gt.1) mg=mg-mb(k)*kw(2,k,1)
            if(NDimI(KPhase).gt.2) mh=mh-mb(k)*kw(3,k,1)
          enddo
2300      chis=0.
          s=expij
          if(kmodsim.gt.0) then
            mp=iabs(ms)
            if(mp.ne.0) then
              fmp=-sign(1.,float(ms))
              chis=chis+ksi(mp)*fmp
            endif
            poms=sb(mp+1)
            s=s*poms
            if(abs(s).lt.DifBess) go to 2500
            dsdksi=fmp
          endif
          do k=kmodbi,1,-1
            fmp=-mb(k)
            mp=iabs(mb(k))+1
            if(k.gt.kmezb) fmp=-fmp
            s=s*besb(mp,k)
            if(abs(s).lt.DifBess) then
              do l=NDim(KPhase)-2,kmodxi
                mxdif=mxkk(l)-mx(l)
                mf=mf-kw(1,l,1)*mxdif
                if(NDimI(KPhase).gt.1) mg=mg-kw(2,l,1)*mxdif
                if(NDimI(KPhase).gt.2) mh=mh-kw(3,l,1)*mxdif
                mx(l)=mxkk(l)
              enddo
              do l=1,k-1
                mbdif=mbk(l)-mb(l)
                mf=mf-kw(1,l,1)*mbdif
                if(NDimI(KPhase).gt.1) mg=mg-kw(2,l,1)*mbdif
                if(NDimI(KPhase).gt.2) mh=mh-kw(3,l,1)*mbdif
                mb(l)=mbk(l)
              enddo
              go to 2450
            endif
            chis=chis+fmp*eta(k)
            if(k.le.kmezb) chis=chis-.25*fmp*pi2
            if(CalcDer) dsdeta(k)=fmp
          enddo
          do k=kmodxi,NDim(KPhase)-2,-1
            fmp=-mx(k)
            mp=-mx(k)+mxb+1
            if(k.gt.kmezx) fmp=-fmp
            s=s*besp(mp,k)
            if(abs(s).lt.DifBess) then
              do l=NDim(KPhase)-2,k-1
                mxdif=mxkk(l)-mx(l)
                mf=mf-kw(1,l,1)*mxdif
                if(NDimI(KPhase).gt.1) mg=mg-kw(2,l,1)*mxdif
                if(NDimI(KPhase).gt.2) mh=mh-kw(3,l,1)*mxdif
                mx(l)=mxkk(l)
              enddo
              go to 2450
            endif
            chis=chis+fmp*chi(k)
            if(CalcDer) dsdchi(k)=fmp
          enddo
          chiss=chis
          ss=s
          mfs=mf
          if(NDimI(KPhase).gt.1) then
            mgs=mg
            if(NDimI(KPhase).gt.2) then
              mhs=mh
            endif
          endif
2315      s=ss
          chis=chiss
          mx(1)=mfs
          if(kfxi.eq.1) mx(1)=mx(1)+(kw(1,kmodxi,1)-1)*mx(kmodxi)
          if(NDimI(KPhase).gt.1) mx(2)=mgs
          if(NDimI(KPhase).gt.2) mx(3)=mhs
          if(mx(1).lt.-mxkk(1).or.mx(1).gt.mxkk(1)) go to 2440
          if(NDimI(KPhase).gt.1) then
            if(mx(2).lt.-mxkk(2).or.mx(2).gt.mxkk(2)) go to 2440
            if(NDimI(KPhase).gt.2) then
              if(mx(3).lt.-mxkk(3).or.mx(3).gt.mxkk(3)) go to 2440
            endif
          endif
          fmp=-mx(1)
          if(kmodxi.gt.0) then
            mp=-mx(1)+mxb+1
            if(kmezx.lt.1) fmp=-fmp
            s=s*besp(mp,1)
            if(abs(s).lt.DifBess) go to 2440
          endif
          chis=chis+fmp*chi(1)
          if(CalcDer) dsdchi(1)=fmp
          if(NDimI(KPhase).gt.1) then
            fmp=-mx(2)
            if(kmodxi.gt.0) then
              mp=-mx(2)+mxb+1
              s=s*besp(mp,2)
              if(abs(s).lt.DifBess) go to 2440
            endif
            chis=chis+fmp*chi(2)
            if(CalcDer) dsdchi(2)=fmp
            if(NDimI(KPhase).gt.2) then
              fmp=-mx(3)
              if(kmodxi.gt.0) then
                mp=-mx(3)+mxb+1
                s=s*besp(mp,3)
                if(abs(s).lt.DifBess) go to 2440
              endif
              chis=chis+fmp*chi(3)
              if(CalcDer) dsdchi(3)=fmp
            endif
          endif
          if(CalcDer) then
            do k=1,kmodxi
              mp=-mx(k)+mxb+1
              dsdu1(k)=s*dbfdu1(mp,k)
              dsdu2(k)=s*dbfdu2(mp,k)
              if(k.gt.kmezx) dsdu3(k)=s*dbfdu3(mp,k)
            enddo
            do k=1,kmodbi
              mp=iabs(mb(k))+1
              dsdb1(k)=s*dbfdb1(mp,k)
              dsdb2(k)=s*dbfdb2(mp,k)
              if(k.gt.kmezb) dsdb3(k)=s*dbfdb3(mp,k)
            enddo
            if(kmodsim.gt.0) then
              mp=iabs(ms)
              if(mp.eq.0) then
                if(CalcDer) dsda0=s/poms
              else
                dsdax=s*dsbdax(mp)
                dsday=s*dsbday(mp)
              endif
            endif
          endif
          pom=fip+chis
          cosp=cos(pom)
          sinp=sin(pom)
          cosps=cosp*s
          sinps=sinp*s
          cosij=cosij+cosps
          sinij=sinij+sinps
          if(.not.CalcDer) go to 2440
          do k=1,kmodxi
            poma=-dsdchi(k)*sinps
            pomb= dsdchi(k)*cosps
            if(k.le.kmezx) then
              dadu1(k)=dadu1(k)+poma*dchidu1(k)+cosp*dsdu1(k)
              dadu2(k)=dadu2(k)+poma*dchidu2(k)+cosp*dsdu2(k)
            else
              dadu1(k)=dadu1(k)+cosp*dsdu1(k)
              dadu2(k)=dadu2(k)+cosp*dsdu2(k)
              dadu3    =dadu3    +cosp*dsdu3(k)
              dadu4    =dadu4    +poma*dchidu1(k)
            endif
            if(NCSymmRef(KPhase).eq.1) then
              if(k.le.kmezx) then
                dbdu1(k)=dbdu1(k)+pomb*dchidu1(k)+sinp*dsdu1(k)
                dbdu2(k)=dbdu2(k)+pomb*dchidu2(k)+sinp*dsdu2(k)
              else
                dbdu1(k)=dbdu1(k)+sinp*dsdu1(k)
                dbdu2(k)=dbdu2(k)+sinp*dsdu2(k)
                dbdu3    =dbdu3    +sinp*dsdu3(k)
                dbdu4    =dbdu4    +pomb*dchidu1(k)
              endif
            endif
          enddo
          do k=1,kmodbi
            poma=-dsdeta(k)*sinps
            pomb= dsdeta(k)*cosps
            if(k.le.kmezb) then
              dadb1(k)=dadb1(k)+poma*detadb1(k)+cosp*dsdb1(k)
              dadb2(k)=dadb2(k)+poma*detadb2(k)+cosp*dsdb2(k)
            else
              dadb1(k)=dadb1(k)+cosp*dsdb1(k)
              dadb2(k)=dadb2(k)+cosp*dsdb2(k)
              dadb3    =dadb3    +cosp*dsdb3(k)
              dadb4    =dadb4    +poma*detadb1(k)
            endif
            if(NCSymmRef(KPhase).eq.1) then
              if(k.le.kmezb) then
                dbdb1(k)=dbdb1(k)+pomb*detadb1(k)+sinp*dsdb1(k)
                dbdb2(k)=dbdb2(k)+pomb*detadb2(k)+sinp*dsdb2(k)
              else
                dbdb1(k)=dbdb1(k)+sinp*dsdb1(k)
                dbdb2(k)=dbdb2(k)+sinp*dsdb2(k)
                dbdb3    =dbdb3    +sinp*dsdb3(k)
                dbdb4    =dbdb4    +pomb*detadb1(k)
              endif
            endif
          enddo
          if(kmodsi.gt.0) then
            if(mp.ne.0) then
              poma=-dsdksi*sinps
              pomb= dsdksi*cosps
              dadax(mp)=dadax(mp)+poma*dksidax(mp)+cosp*dsdax
              daday(mp)=daday(mp)+poma*dksiday(mp)+cosp*dsday
              if(NCSymmRef(KPhase).eq.1) then
                dbdax(mp)=dbdax(mp)+pomb*dksidax(mp)+sinp*dsdax
                dbday(mp)=dbday(mp)+pomb*dksiday(mp)+sinp*dsday
              endif
            else
              dada0=dada0+dsda0*cosp
              if(NCSymmRef(KPhase).eq.1) dbda0=dbda0+dsda0*sinp
            endif
          endif
2440      do 2441k=NDim(KPhase)-2,kmodxi
            if(k.gt.kmezx) go to 2441
            fmp=-mx(k)
            mx(k)=-mx(k)
            if(mod(iabs(mx(k)),2).eq.1) ss=-ss
            chiss=chiss-2.*fmp*chi(k)
            dsdchi(k)=-dsdchi(k)
            mfs=mfs-2*mx(k)*kw(1,k,1)
            if(NDimI(KPhase).gt.1) then
              mgs=mgs-2*mx(k)*kw(2,k,1)
              if(NDimI(KPhase).gt.2) then
                 mhs=mhs-2*mx(k)*kw(3,k,1)
               endif
             endif
            if(mx(k).lt.0) go to 2315
2441      continue
          do k=1,kmodbi
            fmp=-mb(k)
            mb(k)=-mb(k)
            chiss=chiss-2.*fmp*eta(k)+pi*fmp
            dsdeta(k)=-dsdeta(k)
            mfs=mfs-2*mb(k)*kw(1,k,1)
            if(NDimI(KPhase).gt.1) then
              mgs=mgs-2*mb(k)*kw(2,k,1)
              if(NDimI(KPhase).gt.2) then
                mhs=mhs-2*mb(k)*kw(3,k,1)
              endif
            endif
            if(mb(k).lt.0) go to 2315
          enddo
          if(mfs.ne.mf) then

          endif
2450      do k=NDim(KPhase)-2,kmodxi
            if(mx(k).lt.mxkk(k)) then
              mx(k)=mx(k)+1
              mf=mf-kw(1,k,1)
              if(NDimI(KPhase).gt.1) mg=mg-kw(2,k,1)
              if(NDimI(KPhase).gt.2) mh=mh-kw(3,k,1)
              go to 2300
            else
              mx(k)=mxs(k)
              mf=mf+mxdkw1(k)
              if(NDimI(KPhase).gt.1) mg=mg+mxdkw2(k)
              if(NDimI(KPhase).gt.2) mh=mh+mxdkw3(k)
            endif
          enddo
          do k=1,kmodbi
            if(mb(k).lt.mbk(k)) then
              mb(k)=mb(k)+1
              mf=mf-kw(1,k,1)
              if(NDimI(KPhase).gt.1) mg=mg-kw(2,k,1)
              if(NDimI(KPhase).gt.2) mh=mh-kw(3,k,1)
              go to 2300
            else
              mb(k)=mbs(k)
              mf=mf+mbdkw1(k)
              if(NDimI(KPhase).gt.1) mg=mg+mbdkw2(k)
              if(NDimI(KPhase).gt.2) mh=mh+mbdkw3(k)
            endif
          enddo
2500    continue
      else if(klic.eq.2) then
        if(.not.nemod) then
          lx=NDerModFirst-1
          if(kmodsi.gt.0) lx=lx+1+2*kmodsi
          do k=1,kmodxi
            apom1=dadu1(k)
            apom2=dadu2(k)
            if(NCSymmRef(KPhase).eq.1) then
              bpom1=dbdu1(k)
              bpom2=dbdu2(k)
            endif
            do l=1,3
              lx=lx+1
              ly=lx+3
              hjl=hj(l)
              ader(lx)=ader(lx)+hjl*apom1
              if(NCSymmRef(KPhase).eq.1) bder(lx)=bder(lx)+hjl*bpom1
              if(k.le.kmezx.or.l.eq.2) then
                ader(ly)=ader(ly)+hjl*apom2
                if(NCSymmRef(KPhase).eq.1) bder(ly)=bder(ly)+hjl*bpom2
              else
                if(l.eq.1) then
                  ader(ly)=ader(ly)+dadu4
                  if(NCSymmRef(KPhase).eq.1) bder(ly)=bder(ly)+dbdu4
                else
                  ader(ly)=ader(ly)+dadu3
                  if(NCSymmRef(KPhase).eq.1) bder(ly)=bder(ly)+dbdu3
                endif
              endif
            enddo
            lx=ly
          enddo
          do k=1,kmodbi
            apom1=dadb1(k)
            apom2=dadb2(k)
            if(NCSymmRef(KPhase).eq.1) then
              bpom1=dbdb1(k)
              bpom2=dbdb2(k)
            endif
            do l=5,10
              lx=lx+1
              ly=lx+6
              HCoefjl=HCoefj(l)
              ader(lx)=ader(lx)-HCoefjl*apom1
              if(NCSymmRef(KPhase).eq.1) bder(lx)=bder(lx)-HCoefjl*bpom1
              if(k.le.kmezb.or.l.eq.2) then
                ader(ly)=ader(ly)-HCoefjl*apom2
                if(NCSymmRef(KPhase).eq.1)
     1            bder(ly)=bder(ly)-HCoefjl*bpom2
              else
                if(l.eq.1) then
                  ader(ly)=ader(ly)+dadb4
                  if(NCSymmRef(KPhase).eq.1) bder(ly)=bder(ly)+dbdu4
                else if(l.eq.3) then
                  ader(ly)=ader(ly)+dadb3
                  if(NCSymmRef(KPhase).eq.1) bder(ly)=bder(ly)+dbdb3
                endif
              endif
            enddo
            lx=ly
          enddo
        endif
      else if(klic.eq.3) then
        if(kmodsi.gt.0) then
          l=max(TRankCumul(itfi),10)+1
          if(kfsi.eq.0) then
            ader(l)=dada0
            if(NCSymmRef(KPhase).eq.1) bder(l)=dbda0
            do k=1,kmodsim
              l=l+1
              ader(l)=dadax(k)
              if(NCSymmRef(KPhase).eq.1) bder(l)=dbdax(k)
              l=l+1
              ader(l)=daday(k)
              if(NCSymmRef(KPhase).eq.1) bder(l)=dbday(k)
            enddo
          else
            apom=dada0*sumdlt
            if(NCSymmRef(KPhase).eq.1) bpom=dbda0*sumdlt
            do k=1,MaxUsedKw(KPhase)
              xpom=daxddlt(k)
              ypom=dayddlt(k)
              apom=apom+dadax(k)*xpom+daday(k)*ypom
              if(NCSymmRef(KPhase).eq.1)
     1        bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
            enddo
            ader(l)=apom
            if(NCSymmRef(KPhase).eq.1) bder(l)=bpom
            l=l+1
            do j=1,kmodsi
              apom=0.
              bpom=0.
              do k=1,MaxUsedKw(KPhase)
                xpom=daxdx40(j,k)
                ypom=daydx40(j,k)
                apom=apom+dadax(k)*xpom+daday(k)*ypom
                if(NCSymmRef(KPhase).eq.1)
     1          bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
              enddo
              ader(l)=apom
              if(NCSymmRef(KPhase).eq.1) bder(l)=bpom
              l=l+1
              apom=dada0*delta
              bpom=dbda0*delta
              do k=1,MaxUsedKw(KPhase)
                xpom=daxdfct(j,k)
                ypom=daydfct(j,k)
                apom=apom+dadax(k)*xpom+daday(k)*ypom
                if(NCSymmRef(KPhase).eq.1)
     1          bpom=bpom+dbdax(k)*xpom+dbday(k)*ypom
              enddo
              ader(l)=apom
              if(NCSymmRef(KPhase).eq.1) bder(l)=bpom
              l=l+1
            enddo
          endif
        endif
      endif
      return
      end
