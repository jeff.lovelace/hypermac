      subroutine contrm(icont,m)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ssqwd(20),nlp(20),nln(20),nl(20),rnump(20),
     1          rnumn(20),rnumc(20),rdenc(20),rfak(20)
      character*128 venp(10),Veta
      character*7   IndLabel
      save ssqwd,rnump,rnumn,rdenc,nlp,nln,rfak
      if(icont.eq.1) then
        call SetRealArrayTo(ssqwd,20,0.)
        call SetRealArrayTo(rnump,20,0.)
        call SetRealArrayTo(rnumn,20,0.)
        call SetRealArrayTo(rdenc,20,0.)
        call SetIntArrayTo(nlp,20,0)
        call SetIntArrayTo(nln,20,0)
      else if(icont.eq.2) then
        if(nulova) return
        if(dyp.lt.0.) then
          rnumn(m)=rnumn(m)+dyp
          nln(m)=nln(m)+1
        else
          rnump(m)=rnump(m)+dyp
          nlp(m)=nlp(m)+1
        endif
        rdenc(m)=rdenc(m)+Fobs
        ssqwd(m)=ssqwd(m)+wdyq
      else
        if(NDimI(KPhase).eq.1) then
          IndLabel='index'
        else
          IndLabel='indices'
        endif
        call newln(13)
        write(lst,'(/''Statistics as a function of satellite '',a/)')
     1    IndLabel(:idel(IndLabel))
        k=0
        do i=1,20
          rnumc(i)=rnump(i)-rnumn(i)
          if(rdenc(i).ne.0.) rfak(i)=100.*rnumc(i)/rdenc(i)
          nl(i)=nln(i)+nlp(i)
          if(nl(i).ne.0) then
            ssqwd(i)=ssqwd(i)/float(nl(i))
          endif
          if(nl(i).eq.0) cycle
          k=k+1
          if(mod(k,5).eq.1) then
            call SetStringArrayTo(venp,10,' ')
            venp( 1)='Satellite '//IndLabel
            venp( 2)(19:)='number    +'
            venp( 3)(29:29)='-'
            venp( 4)(19:)='together'
            if(ifsq.eq.1) then
              venp( 5)(19:)='av. wdIq'
            else
              venp( 5)(19:)='av. wdFq'
            endif
            venp( 6)(19:)='numerator +'
            venp( 7)(29:29)='-'
            venp( 8)(19:)='together'
            venp( 9)(19:)='denominator'
            venp(10)(19:)='R factor'
            l=30
          endif
          Veta='+-('
          do j=1,NDimI(KPhase)
            write(Cislo,FormI15) HSatGroups(j,i-1)
            call zhusti(Cislo)
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
          enddo
          j=idel(Veta)
          Veta(j:j)=')'
          venp(1)(l+15-idel(Veta):)=Veta(:idel(Veta))
          write(venp( 2)(l:),FormI15) nlp(i)
          write(venp( 3)(l:),FormI15) nln(i)
          write(venp( 4)(l:),FormI15) nl (i)
          write(venp( 5)(l:),'(f15.1)') ssqwd(i)
          write(venp( 6)(l:),'(f15.1)') rnump(i)
          write(venp( 7)(l:),'(f15.1)') rnumn(i)
          write(venp( 8)(l:),'(f15.1)') rnumc(i)
          write(venp( 9)(l:),'(f15.1)') rdenc(i)
          write(venp(10)(l:),'(f15.2)') rfak(i)
          l=l+15
          if(mod(k,5).eq.0) then
            if(k.gt.5) then
              call newln(11)
              write(lst,FormA)
            endif
            do j=1,10
              write(lst,FormA) venp(j)(:idel(venp(j)))
            enddo
          endif
        enddo
        if(mod(k,5).ne.0) then
          if(k.gt.5) then
            call newln(11)
            write(lst,FormA)
          endif
          do j=1,10
            write(lst,FormA) venp(j)(:idel(venp(j)))
          enddo
        endif
      endif
      return
      end
