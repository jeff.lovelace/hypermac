      subroutine MakeZbytekFile
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*80  t80
      logical   Pokracovani
      t256=fln(:ifln)//'_rest.tmp'
      call DeleteFile(t256)
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 1050
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 1050
      call najdi('refine',j)
      if(j.ne.1) go to 1050
1000  Pokracovani=.false.
1010  read(m50,FormA80,end=1050) t80
      if(t80.eq.'end') go to 1050
      if(t80(1:1).eq.'*') go to 1000
      call vykric(t80)
      if(idel(t80).le.0) go to 1000
      call mala(t80)
      j=0
      call kus(t80,j,Cislo)
      j=islovo(Cislo,NactiKeywords,NactiKeys)
      if((j.ge.81.and.j.le.86).or.(Pokracovani.and.j.le.0)) then
        write(ln,FormA) t80(:idel(t80))
        Pokracovani=.true.
        go to 1010
      endif
      go to 1000
1050  call CloseIfOpened(ln)
      call CloseIfOpened(m50)
      return
      end
