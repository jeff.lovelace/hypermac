      subroutine RefEquationWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EquationQuest/ nCrwAtomic,nCrwApplyToOrtho,nEdwAtoms,
     1  nEdwEquation,AtomicEquation,ApplyToOrthoWaves,AtomString,
     2  EquationString
      character*(*) Command
      character*256 EdwStringQuest,AtomString,EquationString
      character*80  ErrString
      logical   CrwLogicQuest,AtomicEquation,ApplyToOrthoWaves
      save /EquationQuest/
      ich=0
      Command='equation'
      if(AtomicEquation) then
        AtomString=EdwStringQuest(nEdwAtoms)
        if(AtomString.eq.' ') go to 9100
        call TestAtomString(AtomString,IdWildYes,IdAtMolYes,IdMolYes,
     1                      IdSymmNo,IdAtMolMixNo,ErrString)
        if(ErrString.ne.' ') go to 9000
        Command=Command(:idel(Command))//' for '//
     1          AtomString(:idel(AtomString))
      endif
      if(nCrwApplyToOrtho.gt.0)
     1  ApplyToOrthoWaves=CrwLogicQuest(nCrwApplyToOrtho)
      if(ApplyToOrthoWaves) then
        ErrString=':'
      else
        ErrString='%'
      endif
      Command=Command(:idel(Command)+1)//ErrString(1:1)
      EquationString=EdwStringQuest(nEdwEquation)
      if(EquationString.eq.' ') go to 9100
      call RefEquationCheck(EquationString,ErrString)
      if(ErrString.ne.' ') go to 9000
      Command=Command(:idel(Command)+1)//
     1        EquationString(:idel(EquationString))
      go to 9999
9000  if(ErrString.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
      end
