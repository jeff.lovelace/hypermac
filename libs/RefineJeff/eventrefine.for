      subroutine EventRefine(ie,RefineEnd,*)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      logical FeYesNo,RefineEnd,CrwLogicQuest
      integer EdwStateQuest
      character*80 Veta
      if(ie.eq.1) then
        if(FeYesNo(-1.,250.,'Do you really want to end the refinement?',
     1             0)) then
          if(CalcDer) ncykl=icykl+1
          RefineEnd=.true.
        endif
      else if(ie.eq.3) then
        if(FeYesNo(-1.,250.,'Do you really want to cancel the '//
     1             'refinement?',0)) then
          call CloseIfOpened(80)
          call DeleteFile(fln(:ifln)//'.m80')
          RefineEnd=.true.
          return 1
        endif
      else if(ie.eq.2) then
        id=NextQuestId()
        il=6
        xqd=250.
        call FeQuestCreate(id,-1.,230.,xqd,il,'Modify refinement '//
     1                     'option:',0,LightGray,0,0)
        il=1
        tpom=5.
        dpom=60.
        xpom=100.
        Veta='%Damping factor'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwTlum=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,Tlum,.false.,.false.)
        il=il+1
        xpomp=5.
        tpomp=xpomp+CrwXd+10.
        Veta='%Use Marquart method'
        call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,
     1                      0)
        nCrwMarq=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,LSMethod.eq.2)
        il=il+1
        Veta='%Fudge factor'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwFudge=EdwLastMade
        il=il+1
        Veta='%Number of cycles'
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwNCykl=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,ncykl,.false.)
        if(icykl.ne.ncykl) then
          ic=9999
        else
          ic=ncykl
        endif
        call FeQuestEudOpen(EdwLastMade,min(icykl+1,ncykl),ic,1,pom,pom,
     1                      pom)
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Make these settings permanent'
        call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,
     1                      0)
        nCrwSave=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,MakeItPermanent)
        MarqLamNew=MarqLam
1400    if(CrwLogicQuest(nCrwMarq)) then
          call FeQuestRealEdwOpen(nEdwFudge,MarqLamNew,.false.,.false.)
        else
          if(EdwStateQuest(nEdwFudge).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdwFudge,MarqLamNew)
          call FeQuestEdwDisable(nEdwFudge)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMarq) then
          go to 1400
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          call FeQuestRealFromEdw(nEdwTlum,pom)
          pom=max(pom,0.)
          pom=min(pom,1.)
          if(pom.ne.tlum) then
            Tlum=pom
            TlumNew=pom
            TlumOrigin=pom
          endif
          call FeQuestIntFromEdw(nEdwNCykl,i)
          NCykl=i
          NCyklNew=i
          if(CrwLogicQuest(nCrwSave)) MakeItPermanent=.true.
          if(CrwLogicQuest(nCrwMarq)) then
            LSMethod=2
            LSMethodNew=2
            call FeQuestRealFromEdw(nEdwFudge,MarqLamNew)
            MarqLam=MarqLamNew
          else
            LSMethod=1
            LSMethodNew=1
            MarqLamNew=MarqLam
          endif
        endif
        call FeQuestRemove(id)
      endif
      return
      end
