      subroutine RefApplyDynRed(lst0,chlst,ich)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      character*(*) chlst
      ich=0
      NDynRed=NDynRed+1
      if(NDynRed.gt.10) then
        call FeChybne(-1.,-1.,'the dynamical method did not '//
     1    'converge in 10 steps.',' ',SeriousError)
        go to 9900
      endif
      call CopyFile(fln(:ifln)//'.l40',fln(:ifln)//'.m40')
      if(isPowder)
     1  call CopyFile(fln(:ifln)//'.l41',fln(:ifln)//'.m41')
      if(ExistElectronData.and.ExistM42)
     1  call CopyFile(fln(:ifln)//'.l42',fln(:ifln)//'.m42')
      call CopyFile(fln(:ifln)//'.l50',fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.ne.0) go to 9900
      if(ExistElectronData.and.ExistM42) then
        call iom42(0,0,fln(:ifln)//'.m42')
        if(ErrFlag.ne.0) go to 9900
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9900
      if(MaxNSymmMol.gt.MaxNSymm) then
        call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                   NPhase,0)
        MaxNSymm=MaxNSymmMol
      endif
      if(UisoLim.gt.0.) call RefSetMatrixLimits(0)
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9900
      if(MaxNSymmMol.gt.MaxNSymm) then
        call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                   NPhase,0)
        MaxNSymm=MaxNSymmMol
      endif
      call comsym(0,0,ich)
      if(ich.ne.0) go to 9900
      if(ngcMax.gt.0.and.NAtCalc.gt.0)
     1  call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
      if(MagneticType(KPhase).ne.0) then
        call SetMag(-1)
        call SetMag (0)
      endif
      call OpenFile(75,fln(:ifln)//'.l75','unformatted','unknown')
      read(75)(ki(i),i=1,PosledniKiAtMol),
     1        (dc(i),i=1,PosledniKiAtMol),
     2        (der(i),i=1,PosledniKiAtMol)
      if(NMolec.gt.0)
     1  read(75)(ki(i),i=PrvniKiMol,PosledniKiMol),
     2          (dc(i),i=PrvniKiMol,PosledniKiMol),
     3          (der(i),i=PrvniKiMol,PosledniKiMol)
      if(NAtXYZMode.gt.0)
     1  read(75)(ki(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     2          (dc(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     3          (der(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode)
      if(NAtMagMode.gt.0)
     1  read(75)(ki(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     2          (dc(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     3          (der(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode)
      close(75)
      tlum=tlum/RedDyn
      lst=lst+1
      call OpenFile(lst,fln(:ifln)//chlst,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      icykl=icykl-1
      if(MagneticType(KPhase).ne.0) call SetMag(1)
      call InverBackDamp(lst0)
      call CloseIfOpened(lst)
      if(ErrFlag.ne.0) go to 9900
      call CloseIfOpened(80)
      call CloseIfOpened(83)
      if(ExistPowder) call CloseIfOpened(LnPrf)
      lst=lst0
      do i=1,NInfo
        j=index(TextInfo(i),'Dampi')
        if(j.gt.0) then
          write(TextInfo(i)(j+16:),'(f8.4)') Tlum
          rewind LstRef
          go to 9999
        endif
      enddo
      rewind LstRef
      go to 9999
9900  ich=1
9999  return
      end
