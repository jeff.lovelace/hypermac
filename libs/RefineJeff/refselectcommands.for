      subroutine RefSelectCommands
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension nxxpp(20)
      character*256 EdwStringQuest
      character*80 Veta
      character*20 t20
      logical CrwLogicQuest,lpom
      save nEdwUnobs,nEdwSkip,nCrwUnobs,nCrwSkip,nCrwSinLim,nEdwSinMin,
     1     nEdwSinMax,ApplySinLimOld,nLblSinLim,nEdwFlags,
     2     nCrwRefListNone,nCrwRefListAfter,nCrwRefListBeforeAfter,
     3     nxxnp,nxxpp,nCrwTwDetails,nCrwIStat,nEdwCorr,nCrwSigMethod,
     4     nCrwDatBlockFirst,nButtAllDatBlocks,nowrOld,
     5     nCrwRefNotMatching
      entry RefSelectCommandsMake(id)
      nowrOld=-999
      xqd=XdQuestRef-2.*KartSidePruh
      il=1
      if(ExistSingle) then
        Veta='Indicate/Select reflections:'
      else
        Veta='Indicate reflections:'
      endif
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      ApplySinLimOld=-1
      tpom=5.
      dpom=50.
      xpom=230.
      Veta='Un%observed reflections:'
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          Veta='Not %matching reflections:'
          t20='I<'
          nEdwUnobs=EdwLastMade
          pom=NacetlReal(nCmdslevel)
        else if(i.eq.2) then
          nEdwSkip=EdwLastMade
          pom=NacetlReal(nCmdvyh)
          t20='|F(obs)-F(calc)|>'
        endif
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        spom=xpom-FeTxLength(t20)-7.
        call FeQuestLblMake(id,spom,il,t20,'L','N')
        spom=xpom+dpom+7.
        if(i.eq.1) then
          t20='*sig(I)'
        else
          t20='*sig(F(obs))'
          xpom=spom+FeTxLength(t20)+20.
        endif
        call FeQuestLblMake(id,spom,il,t20,'L','N')
      enddo
      if(ExistSingle) then
        il=1
        tpom=xpom+CrwXd+10.
        Veta='%Use unobserved reflections'
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                        0)
          if(i.eq.1) then
            nCrwUnobs=CrwLastMade
            lpom=NacetlInt(nCmduseunobs).eq.1
            Veta='%Skip not matching reflections'
          else
            nCrwSkip=CrwLastMade
            lpom=NacetlInt(nCmdiskip).eq.1
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
        enddo
        il=il+1
        xpom=7.
        tpom=xpom+CrwXd+10.
        dpom=80.
        Veta='Appl%y sin(th)/lambda limits'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSinLim=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdApplySinLim).gt.0)
        tpom=5.
        il=il+1
        Veta='sin(th)/lambda'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblSinLim=LblLastMade
        call FeQuestLblOff(LblLastMade)
        tpome=tpom+FeTxLength(Veta)+20.
        Veta='mi%n.'
        xpome=tpome+FeTxLength(Veta)+10.
        call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
        nEdwSinMin=EdwLastMade
        Veta='ma%x.'
        tpome=xpome+dpom+20.
        xpome=tpome+FeTxLengthUnder(Veta)+10.
        call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
        nEdwSinMax=EdwLastMade
        il=il+1
        tpom=5.
        Veta='Skip reflection having user''s %flag(s)'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=xqd-xpom-5.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        if(nxxn.gt.0) then
          write(Veta,'(20i4)')(nxxp(i),i=1,nxxn)
          call ZdrcniCisla(Veta,nxxn)
          call CopyVekI(nxxp,nxxpp,nxxn)
        else
          Veta=' '
        endif
        call FeQuestStringEdwOpen(EdwLastMade,Veta)
        nEdwFlags=EdwLastMade
      else
        nCrwUnobs=0
        nCrwSkip=0
        nCrwSinLim=0
        nLblSinLim=0
        nEdwSinMin=0
        nEdwSinMax=0
        nEdwFlags=0
      endif
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,'Listing commands:','C','B')
      xpom=7.
      tpom=xpom+CrwXd+10.
      Veta='Print of reflections supp%resed'
      ichk=1
      igrp=1
      do i=0,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        lpom=abs(NacetlInt(nCmdnowr)).eq.i
        call FeQuestCrwOpen(CrwLastMade,lpom)
        if(i.eq.0) then
          il=il+1
          Veta='Print of reflections allowed:'
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          Veta='before the first cycle and after the last cycle'
          pom=FeTxLengthUnder(Veta)
          nCrwRefListNone=CrwLastMade
        else if(i.eq.1) then
          Veta='after the last cycle'
          nCrwRefListBeforeAfter=CrwLastMade
        else
          nCrwRefListAfter=CrwLastMade
        endif
      enddo
      xpom=tpom+pom+50.
      tpom=xpom+CrwXd+10.
      Veta='Not ma%tching ones'
      il=il-2
      igrp=2
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        if(i.eq.1) then
          nCrwRefNotMatching=CrwLastMade
          Veta='A%ll ones'
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(Veta)+80.
      tpom=xpom+CrwXd+10.
      ilp=il
      il=il-2
      if(NTwin.gt.1.or.iover.gt.0) then
        Veta='Print t%win/overlap details'
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwTwDetails=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdTwDetail).eq.1)
      else
        nCrwTwDetails=0
      endif
      il=il+1
      Veta='Print stat%istics'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwIStat=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdistat).eq.1)
      il=ilp+1
      tpom=5.
      Veta='Print %correlation larger than'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCorr=EdwLastMade
      call FeQuestRealEdwOpen(nEdwCorr,NacetlReal(nCmdcorr),.false.,
     1                        .false.)
      ilp=il
      if(ExistPowder) then
        il=il+1
        xpom=7.
        Veta='For powder sig(I(hkl)) calculate:'
        call FeQuestLblMake(id,xpom,il,Veta,'L','B')
        tpom=xpom+CrwXd+10.
        Veta='from error propa%gation'
        ichk=0
        igrp=3
        do i=1,4
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          call FeQuestCrwOpen(CrwLastMade,i.eq.SigMethod)
          if(i.eq.1) then
            nCrwSigMethod=CrwLastMade
            Veta='from %profile fit'
          else if(i.eq.2) then
            Veta='ma%ximum of both'
          else if(i.eq.3) then
            Veta='mi%nimum of both'
          endif
        enddo
      endif
      if(NDatBlock.gt.1) then
        il=ilp
        xpom=330.
        Veta='Datablocks used in the refinement:'
        call FeQuestLblMake(id,xpom,il,Veta,'L','B')
        tpom=xpom+CrwXd+10.
        do i=1,NDatBlock
          il=il+1
          Veta=MenuDatBlock(i)(:5)//'%'//MenuDatBlock(i)(6:)
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          if(i.eq.1) nCrwDatBlockFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,UseDatBlockInRefine(i))
        enddo
        il=ilp+2
        Veta='Use %all ->'
        dpom=FeTxLengthUnder(Veta)+20.
        xpom=xpom-100.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAllDatBlocks=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nCrwDatBlockFirst=0
        nButtAllDatBlocks=0
      endif
      go to 2500
      entry RefSelectCommandsCheck
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFlags) then
        k=0
        nxxnp=0
        Veta=EdwStringQuest(nEdwFlags)
        if(Veta.ne.' ') then
2100      call kus(Veta,k,Cislo)
          call posun(Cislo,0)
          nxxnp=nxxnp+1
          if(nxxnp.gt.20) go to 2120
          read(Cislo,FormI15,err=2110) nxxpp(nxxnp)
          if(k.lt.len(Veta)) go to 2100
          go to 9999
2110      call Zhusti(Cislo)
          call FeChybne(-1.,-1.,'incorrect flag number : '//
     1                  Cislo(:idel(Cislo)),' ',SeriousError)
          go to 2150
2120      call FeChybne(-1.,-1.,'maximal number of 20 user flags '//
     1                  'exceeded',' ',SeriousError)
2150      EventType=EventEdw
          EventNumber=nEdwFlags
        endif
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtAllDatBlocks) then
        nCrw=nCrwDatBlockFirst
        do i=1,NDatBlock
          call FeQuestCrwOn(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      if(nCrwSinLim.gt.0) then
        if(CrwLogicQuest(nCrwSinLim)) then
          NacetlInt(nCmdApplySinLim)=1
        else
          NacetlInt(nCmdApplySinLim)=0
        endif
        if(NacetlInt(nCmdApplySinLim).ne.ApplySinLimOld) then
          if(NacetlInt(nCmdApplySinLim).eq.1) then
            call FeQuestLblOn(nLblSinLim)
            call FeQuestRealEdwOpen(nEdwSinMin,NacetlReal(nCmdsnlmn),
     1                              .false.,.false.)
            call FeQuestRealEdwOpen(nEdwSinMax,NacetlReal(nCmdsnlmx),
     1                              .false.,.false.)
          else
            call FeQuestLblOff(nLblSinLim)
            call FeQuestEdwClose(nEdwSinMin)
            call FeQuestEdwClose(nEdwSinMax)
          endif
          ApplySinLimOld=NacetlInt(nCmdApplySinLim)
        endif
      endif
      nCrw=nCrwRefListNone
      do i=0,2
        if(CrwLogicQuest(nCrw)) then
          NacetlInt(nCmdnowr)=i
          go to 2260
        endif
        nCrw=nCrw+1
      enddo
2260  if(CrwLogicQuest(nCrwRefNotMatching)) then
        NacetlInt(nCmdnowr)=-iabs(NacetlInt(nCmdnowr))
      else
        NacetlInt(nCmdnowr)= iabs(NacetlInt(nCmdnowr))
      endif
2500  if(nCrwSinLim.gt.0) then
        if(NacetlInt(nCmdApplySinLim).ne.ApplySinLimOld) then
          if(NacetlInt(nCmdApplySinLim).eq.1) then
            call FeQuestLblOn(nLblSinLim)
            call FeQuestRealEdwOpen(nEdwSinMin,NacetlReal(nCmdsnlmn),
     1                              .false.,.false.)
            call FeQuestRealEdwOpen(nEdwSinMax,NacetlReal(nCmdsnlmx),
     1                              .false.,.false.)
          else
            call FeQuestLblOff(nLblSinLim)
            call FeQuestEdwClose(nEdwSinMin)
            call FeQuestEdwClose(nEdwSinMax)
          endif
          ApplySinLimOld=NacetlInt(nCmdApplySinLim)
        endif
      endif
      if(CrwLogicQuest(nCrwRefListNone)) then
        if(NowrOld.ne.0) then
          call FeQuestCrwClose(nCrwRefNotMatching)
          call FeQuestCrwClose(nCrwRefNotMatching+1)
        endif
        NowrOld=0
      else
        if(NowrOld.ne.iabs(NacetlInt(nCmdnowr))) then
          call FeQuestCrwOpen(nCrwRefNotMatching,
     1                        NacetlInt(nCmdnowr).lt.0)
          call FeQuestCrwOpen(nCrwRefNotMatching+1,
     1                        NacetlInt(nCmdnowr).gt.0)
        endif
        NowrOld=iabs(NacetlInt(nCmdnowr))
      endif
      go to 9999
      entry RefSelectCommandsUpdate
      if(nCrwUnobs.gt.0) then
        if(CrwLogicQuest(nCrwUnobs)) then
          NacetlInt(nCmduseunobs)=1
        else
          NacetlInt(nCmduseunobs)=0
        endif
        if(CrwLogicQuest(nCrwSkip)) then
          NacetlInt(nCmdiskip)=1
        else
          NacetlInt(nCmdiskip)=0
        endif
      endif
      call FeQuestRealFromEdw(nEdwUnobs,NacetlReal(nCmdslevel))
      call FeQuestRealFromEdw(nEdwSkip,NacetlReal(nCmdvyh))
      call FeQuestRealFromEdw(nEdwCorr,NacetlReal(nCmdcorr))
      if(nEdwSinMin.gt.0) then
        if(NacetlInt(nCmdApplySinLim).eq.1) then
          call FeQuestRealFromEdw(nEdwSinMin,NacetlReal(nCmdsnlmn))
          call FeQuestRealFromEdw(nEdwSinMax,NacetlReal(nCmdsnlmx))
        else
          NacetlReal(nCmdsnlmn)=DefaultReal(nCmdsnlmn)
          NacetlReal(nCmdsnlmx)=DefaultReal(nCmdsnlmx)
        endif
      endif
      nxxn=nxxnp
      call CopyVekI(nxxpp,nxxp,nxxn)
      if(CrwLogicQuest(nCrwTwDetails)) then
        NacetlInt(nCmdTwDetail)=1
      else
        NacetlInt(nCmdTwDetail)=0
      endif
      if(CrwLogicQuest(nCrwistat)) then
        NacetlInt(nCmdistat)=1
      else
        NacetlInt(nCmdistat)=0
      endif
      if(ExistPowder) then
        nCrw=nCrwSigMethod
        do i=1,4
          if(CrwLogicQuest(nCrw)) then
            NacetlInt(nCmdSigMethod)=i
            go to 3550
          endif
          nCrw=nCrw+1
        enddo
      endif
3550  if(NDatBlock.gt.1) then
        nCrw=nCrwDatBlockFirst
        do i=1,NDatBlock
          UseDatBlockInRefine(i)=CrwLogicQuest(nCrw)
          nCrw=nCrw+1
        enddo
      endif
9999  return
      end
