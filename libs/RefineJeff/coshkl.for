      function cosHKL(h1,h2)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h1(3),h2(3),h1p(3),h2p(3)
      call multm(MetTensI(1,1,KPhase),h1,h1p,3,3,1)
      call multm(MetTensI(1,1,KPhase),h2,h2p,3,3,1)
      S1=scalmul(h1p,h1)
      S2=scalmul(h2p,h2)
      if(S1.le..00001.or.S2.le..00001) then
        cosHKL=0.
      else
        cosHKL=scalmul(h1p,h2)/sqrt(S1*S2)
      endif
      return
      end
