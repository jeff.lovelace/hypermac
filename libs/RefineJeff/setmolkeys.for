      subroutine SetMolKeys(i1,i2,imp,imk,kkk)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      nap=NAtMolFrAll(KPhase)
      do i=i1,i2
        if(i.lt.imp.or.i.gt.imk) then
          do j=1,mxp
            ji=j+mxp*(i-1)
            call SetIntArrayTo(KiMol(1,ji),DelkaKiMolekuly(ji),0)
          enddo
          do j=nap,nap+iam(i)/KPoint(i)-1
            call SetIntArrayTo(KiA(1,j),DelkaKiAtomu(j),0)
          enddo
        else
          nak=nap+iam(i)/KPoint(i)-1
          call SetAtKeys(nap,nak,nap,nak,kkk)
          do 3900j=1,mam(i)
            ji=j+mxp*(i-1)
            if(aimol(ji).le.0.) then
              call SetIntArrayTo(KiMol(1,ji),DelkaKiMolekuly(ji),0)
              go to 3900
            endif
            if(kim.eq.0.or.kim.eq.-2) then
              call SetIntArrayTo(KiMol(2,ji),6,0)
              if(AtTrans(ji).ne.' ') then
                m=3
              else
                m=6
              endif
              call SetIntArrayTo(KiMol(2,ji),m,1)
              if(ktls(i).gt.0) then
                call SetIntArrayTo(KiMol(8,ji),20,1)
                KiMol(28,ji)=0
              endif
            else
              KiMol(1,ji)=0
            endif
            if(NDimI(KPhase).le.0) go to 3900
            if(ktls(i).gt.0) then
              kip=29
            else
              kip=8
            endif
            kmodsi=KModM(1,ji)
            kmodxi=KModM(2,ji)
            kmodbi=KModM(3,ji)
            if(kmodsi.le.0.and.kmodxi.le.0.and.kmodbi.le.0) go to 3900
            if(KFM(1,ji).ne.0) then
              kmodsi=kmodsi-NDim(KPhase)+3
              KiMol(kip+2,ji)=0
            endif
            if(KFM(2,ji).eq.1.or.KFM(2,ji).eq.4) then
              kmodxi=kmodxi-1
            else if(KFM(2,ji).eq.2.or.KFM(2,ji).eq.5) then
              kmodxi=kmodxi-2
            else if(KFM(2,ji).eq.3.or.KFM(2,ji).eq.6) then
              kmodxi=kmodxi-3
            endif
            if(KFM(3,ji).ne.0) kmodbi=kmodbi-1
            if(KModM(1,ji).ne.0) then
              kip=kip+1
              call SetIntArrayTo(KiMol(kip,ji),2*kmodsi,kkk)
              kip=kip+2*KModM(1,ji)
            endif
            do l=1,2
              call SetIntArrayTo(KiMol(kip,ji),6*kmodxi,kkk)
              kip=kip+6*KModM(2,ji)
            enddo
            do l=1,2
              call SetIntArrayTo(KiMol(kip,ji),12*kmodbi,kkk)
              kip=kip+12*KModM(3,ji)
            enddo
            do k=1,18*kmodbi
              if(mod(k,9).eq.0) then
                KiMol(kip,ji)=0
              else
                KiMol(kip,ji)=kkk
              endif
              kip=kip+1
            enddo
            if(kmodbi.ne.KModM(3,ji)) kip=kip+18*(KModM(3,ji)-kmodbi)
            if(phfm(ji).ne.0.) then
              KiMol(kip,ji)=kkk
            else
              KiMol(kip,ji)=0
            endif
3900      continue
        endif
        nap=nap+iam(i)
      enddo
      return
      end
