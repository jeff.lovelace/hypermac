      subroutine DerTest(ihp,yop,ycp,itstder)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ihp(6),DerSave(:)
      character*256 EdwStringQuest
      character*80 t80,Message1,Message2
      character*20 at
      character*12 parp
      integer pocder,Exponent10
      logical FeYesNo,PridatQR
      save dlt,yc0,at,parp,Message1,Message2,pom0,sp,nButtCalc,
     1     nButtQuit,nEdwParameter,nEdwDelta,ktstdero,nLbl1,nLbl2,
     2     DerSave,kk
      allocatable DerSave
      if(.not.allocated(DerSave)) allocate(DerSave(PosledniKiAtMagMode))
      if(DoLeBail.ne.0) then
        pom=sc(1,KDatBlock)
        sc(1,KDatBlock)=scPwd(KDatBlock)
        scPwd(KDatBlock)=pom
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      if(kcommenMax.gt.0) then
        if(MaxNSymmMol.gt.MaxNSymm) then
          call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                     NPhase,0)
          MaxNSymm=MaxNSymmMol
        endif
        call ComSym(0,0,ich)
        if(ngc(KPhase).gt.0)
     1    call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),NAtCalc)
      endif
      if(DoLeBail.ne.0) then
        pom=sc(1,KDatBlock)
        sc(1,KDatBlock)=scPwd(KDatBlock)
        scPwd(KDatBlock)=pom
        if(NAtCalc.gt.0)
     1    call SetIntArrayTo(ki(ndoff+1),PosledniKiAtPos-ndoff,0)
      endif
      if(itstder.eq.0) then
        call CopyVek(Der,DerSave,PosledniKiAtMagMode)
        Message1=' '
        Message2=' '
        ktstdero=0
        id=NextQuestId()
        il=8
        xdq=350.
        if(isPowder) then
          write(Cislo,'(f10.3)') XPwdDerTest
          call Zhusti(Cislo)
          t80='Testing point 2theta/TOF: '//Cislo(:idel(Cislo))
          write(Cislo,'(f15.3)') ycp
          call Zhusti(Cislo)
          t80=t80(:idel(t80))//', I(calc) : '//Cislo(:idel(Cislo))
        else
          t80='Reflection : ('
          do i=1,NDim(KPhase)
            write(Cislo,'(i5,'','')') HDerTest(i)
            call Zhusti(Cislo)
            t80=t80(:idel(t80))//Cislo(:idel(Cislo))
          enddo
          write(Cislo,'(f10.4)') ycp
          call Zhusti(Cislo)
          t80=t80(:idel(t80)-1)//'), F(calc) : '//Cislo(:idel(Cislo))
        endif
        yc0=ycp
        call FeQuestCreate(id,-1.,-1.,xdq,il,t80,0,LightGray,-1,-1)
        il=1
        call FeQuestLinkaMake(id,il)
        tpom=5.
        t80='%Parameter to be tested'
        dpom=150.
        xpom=tpom+FeTxLengthUnder(t80)+5.
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
        nEdwParameter=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
        dlt=0.0001
        il=il+1
        t80='%Difference'
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
        nEdwDelta=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,dlt,.false.,.false.)
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        call FeQuestLblMake(id,5.,il,Message1,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLbl1=LblLastMade
        il=il+1
        call FeQuestLblMake(id,5.,il,Message2,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLbl2=LblLastMade
        il=8
        dpom=60.
        xpom=xdq*.5-dpom-10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Calculate')
        nButtCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Quit')
        nButtQuit=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        go to 1500
      endif
      if(itstder.eq.1) then
        if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode) then
          ppp=(ycp-yc0)/dlt*CellPar(kk,1,1)
        else
          ppp=(ycp-yc0)/dlt
        endif
        if(isPowder) then
          write(Message2,'(''New I(calc) :'',f10.3,
     1                     '', Numerical  derivation :'',e13.5)')
     2      ycp,ppp
        else
          write(Message2,'(''New F(calc) :'',f10.4,
     1                     '', Numerical derivation  :'',e13.5)')
     2      ycp,ppp
        endif
        call FeQuestLblChange(nLbl2,Message2)
        if(isPowder) then
          do i=1,NPhase
            KPhase=i
            call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
            call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
            if(CellPar(1,1,i).gt..1) call setmet(0)
          enddo
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtCalc) then
          t80=EdwStringQuest(nEdwParameter)
          i=index(t80,'[')
          j=index(t80,']')
          if(i.gt.0) then
            if(j.eq.0.or.i.ge.j.or.j.ne.idel(t80)) then
              call FeChybne(-1.,-1.,'Syntactic error - try again.',' ',0,
     1                      -1)
              go to 1500
            endif
            at=t80(i+1:j-1)
            call uprat(at)
            parp=t80(1:i-1)
            k=ktatmol(at)
            if(k.ne.0) then
              n=ktera(parp)
              if(n.eq.JeToXYZMode) then
                ktstder=KteraXYZMode(Parp,At)
              else if(n.eq.JeToMagMode) then
                ktstder=KteraMagMode(Parp,At)
              else
                m=pocder(k)
                if(m.le.0) go to 1600
                if(k.gt.0) then
                  call RefGetNParAt(n,k,.false.,PridatQR,ich)
                else
                  call RefGetNParMol(n,-k,.false.,PridatQR,ich)
                endif
                if(ich.ne.0) go to 1610
                if(k.gt.0) then
                  call ChangeAtCompress(n,n,k,0,ich)
                else if(k.lt.0) then
                  call ChangeMolCompress(n,n,-k,0,ich)
                endif
                ktstder=m+n
              endif
            else
              k=0
              ktstder=KteraSc(parp)
              call RefGetScShift(iabs(ktstder),at,i)
              if(i.ge.0) then
                ktstder=ktstder+i
              else
                go to 1610
              endif
            endif
          else
            k=0
            parp=t80
            ktstder=kterasc(parp)
            if(ktstder.le.0) go to 1610
          endif
          if(ki(ktstder).eq.0.and.isPowder) go to 1620
          call FeQuestRealFromEdw(nEdwDelta,dlt)
          if(ktstder.ne.ktstdero) then
            call kdoco(ktstder,at,parp,1,pom0,sp)
            if(k.gt.0) then
              kk=ktstder-PrvniKiAtomu(k)+1
              if(kk.ge.DelkaKiAtomu(k)-3-6*(MagPar(k)-1)) then
                kk=kk-DelkaKiAtomu(k)+3+6*(MagPar(k)-1)
                kk=mod(kk-1,3)+1
              else
                kk=0
              endif
            else
              kk=0
            endif
            if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode)
     1        pom0=pom0*CellPar(kk,1,1)
            pomd=DerSave(ktstder)
            if(ifsq.eq.1.and..not.isPowder.and.yc0.ne.0.)
     1        pomd=pomd*.5/yc0
c            if(isPowder.and.ktstder.le.mxscu) pomd=.0002*pom0*pomd
            t80='(''Parameter : '',f9.6,'', Analytical derivation : '''
     1          //',e13.5)'
            i=max(6-Exponent10(pom0),0)
            i=min(6-Exponent10(pom0),6)
            if(i.gt.1) write(t80(20:20),'(i1)') i
            write(Message1,t80) pom0,pomd
            call FeQuestLblChange(nLbl1,Message1)
            ktstdero=ktstder
          endif
          itstder=1
          if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode) then
            pom=(pom0+dlt)/CellPar(kk,1,1)
          else
            pom=pom0+dlt
          endif
          if(NAtXYZMode.gt.0) call TrXYZMode(1)
          if(NAtMagMode.gt.0) call TrMagMode(1)
          call kdoco(ktstdero,at,parp,-1,pom,sp)
          call kdoco(ktstdero,at,parp,1,pompp,sp)
          if(NDimI(KPhase).gt.0.and.k.gt.0) then
             if(kmol(k).le.0) call qbyx(x(1,k),qcnt(1,k),iswa(k))
          endif
          call RefAppEq(0,0,0)
          if(nvai.gt.0) call SetRes(0)
          if(nKeep.gt.0) call RefSetKeep
          if(NMolec.gt.0) then
            NAtCalc=NAtInd
            call SetMol(1,0)
          endif
          if(NAtXYZMode.gt.0) call TrXYZMode(0)
          if(NAtMagMode.gt.0) call TrMagMode(0)
          if(isPowder) then
            do i=1,NPhase
              KPhase=i
              call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
              call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
              if(CellPar(1,1,i).gt..1) call setmet(0)
            enddo
          endif
          go to 3000
        else if(CheckNumber.eq.nButtQuit) then
          if(FeYesNo(-1.,-1.,'Do you really want to quit testing?',0))
     1      then
            call FeQuestRemove(id)
            itstder=-1
            go to 9999
          endif
        endif
        go to 1500
1600    t80='atom "'//at(:idel(at))//'" doesn''t exist'
        go to 1650
1610    t80='unknown or incorrect parameter "'//parp(:idel(parp))//'"'
        go to 1650
1620    t80='not refined parameter "'//t80(:idel(t80))//
     1      '" cannot be tested for powder'
        go to 1650
1630    t80='phase "'//at(:idel(at))//'" doesn''t exist.'
1650    call FeChybne(-1.,-1.,t80,' ',-1)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(NMolec.gt.0) then
        NAtCalc=NAtInd
        call setmol(1,0)
      endif
9999  if(itstder.eq.-1) deallocate(DerSave)
      call iom50(0,0,fln(:ifln)//'.m50')
      if(MaxNSymmMol.gt.MaxNSymm) then
        call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                   NPhase,0)
        MaxNSymm=MaxNSymmMol
      endif
      if(kcommenMax.gt.0) then
        NAtCalc=NAtCalcBasic
        call ComSym(0,0,ich)
      endif
      do KDatB=1,NDatBlock
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      if(isPowder) then
        do i=1,NPhase
          KPhase=i
          call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
          call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
          if(CellPar(1,1,i).gt..1) call setmet(0)
        enddo
      endif
      if(maxNDimI.gt.0) call calcm2(i,i,i,-1)
      return
      end
