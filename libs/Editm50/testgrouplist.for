      subroutine TestGroupList
      use Basic_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iar(1),TrPom(9),TrPomI(9),TrShift(3),NGrpA(20),
     1          QSymm(3,3)
      integer PorGrpA(20)
      real ListGroupO(:,:)
      character*256 Veta
      character*80  ListGroup(:),t80,Grp,GrpA(20)
      character*8   PointGroup
      character*2   t2m
      integer ListGroupN(:),Choice,Sel(:,:)
      logical EqIgCase,IsNormal
      allocatable ListGroup,ListGroupN,ListGroupO,Sel
      allocate(ListGroup(230),ListGroupN(230),ListGroupO(3,230))
      call SetIntArrayTo(ListGroupN,230,1)
      call SetRealArrayTo(ListGroupO,3*230,0.)
      ln=0
      ListGroup(1)='P1'
      ListGroup(2)='P-1'
      ListGroup(3)='P2'
      ListGroup(4)='P21'
      ListGroup(5)='C2'
      ListGroup(6)='Pm'
      ListGroup(7)='Pc'
      ListGroup(8)='Cm'
      ListGroup(9)='Cc'
      ListGroup(10)='P2/m'
      ListGroup(11)='P21/m'
      ListGroup(12)='C2/m'
      ListGroup(13)='P2/c'
      ListGroup(14)='P21/c'
      ListGroup(15)='C2/c'
      ListGroup(16)='P222'
      ListGroup(17)='P2221'
      ListGroup(18)='P21212'
      ListGroup(19)='P212121'
      ListGroup(20)='C2221'
      ListGroup(21)='C222'
      ListGroup(22)='F222'
      ListGroup(23)='I222'
      ListGroup(24)='I212121'
      ListGroup(25)='Pmm2'
      ListGroup(26)='Pmc21'
      ListGroup(27)='Pcc2'
      ListGroup(28)='Pma2'
      ListGroup(29)='Pca21'
      ListGroup(30)='Pnc2'
      ListGroup(31)='Pmn21'
      ListGroup(32)='Pba2'
      ListGroup(33)='Pna21'
      ListGroup(34)='Pnn2'
      ListGroup(35)='Cmm2'
      ListGroup(36)='Cmc21'
      ListGroup(37)='Ccc2'
      ListGroup(38)='Amm2'
      ListGroup(39)='Aem2'
      ListGroup(40)='Ama2'
      ListGroup(41)='Aea2'
      ListGroup(42)='Fmm2'
      ListGroup(43)='Fdd2'
      ListGroup(44)='Imm2'
      ListGroup(45)='Iba2'
      ListGroup(46)='Ima2'
      ListGroup(47)='Pmmm'
      ListGroup(48)='Pnnn'
      ListGroupN(48)=2
      call SetRealArrayTo(ListGroupO(1,48),3,-.25)
      ListGroup(49)='Pccm'
      ListGroup(50)='Pban'
      ListGroupN(50)=2
      call SetRealArrayTo(ListGroupO(1,50),2,-.25)
      ListGroup(51)='Pmma'
      ListGroup(52)='Pnna'
      ListGroup(53)='Pmna'
      ListGroup(54)='Pcca'
      ListGroup(55)='Pbam'
      ListGroup(56)='Pccn'
      ListGroup(57)='Pbcm'
      ListGroup(58)='Pnnm'
      ListGroup(59)='Pmmn'
      ListGroupN(59)=2
      call SetRealArrayTo(ListGroupO(1,59),2,-.25)
      ListGroup(60)='Pbcn'
      ListGroup(61)='Pbca'
      ListGroup(62)='Pnma'
      ListGroup(63)='Cmcm'
      ListGroup(64)='Cmce'
      ListGroup(65)='Cmmm'
      ListGroup(66)='Cccm'
      ListGroup(67)='Cmme'
      ListGroup(68)='Ccce'
      ListGroupN(68)=2
      call SetRealArrayTo(ListGroupO(2,68),2,-.25)
      ListGroup(69)='Fmmm'
      ListGroup(70)='Fddd'
      ListGroupN(70)=2
      call SetRealArrayTo(ListGroupO(1,70),3,.125)
      ListGroup(71)='Immm'
      ListGroup(72)='Ibam'
      ListGroup(73)='Ibca'
      ListGroup(74)='Imma'
      ListGroup(75)='P4'
      ListGroup(76)='P41'
      ListGroup(77)='P42'
      ListGroup(78)='P43'
      ListGroup(79)='I4'
      ListGroup(80)='I41'
      ListGroup(81)='P-4'
      ListGroup(82)='I-4'
      ListGroup(83)='P4/m'
      ListGroup(84)='P42/m'
      ListGroup(85)='P4/n'
      ListGroupN(85)=2
      ListGroupO(1,85)= .25
      ListGroupO(2,85)=-.25
      ListGroup(86)='P42/n'
      ListGroupN(86)=2
      call SetRealArrayTo(ListGroupO(1,86),3,.25)
      ListGroup(87)='I4/m'
      ListGroup(88)='I41/a'
      ListGroupN(88)=2
      ListGroupO(2,88)=.25
      ListGroupO(3,88)=.125
      ListGroup(89)='P422'
      ListGroup(90)='P4212'
      ListGroup(91)='P4122'
      ListGroup(92)='P41212'
      ListGroup(93)='P4222'
      ListGroup(94)='P42212'
      ListGroup(95)='P4322'
      ListGroup(96)='P43212'
      ListGroup(97)='I422'
      ListGroup(98)='I4122'
      ListGroup(99)='P4mm'
      ListGroup(100)='P4bm'
      ListGroup(101)='P42cm'
      ListGroup(102)='P42nm'
      ListGroup(103)='P4cc'
      ListGroup(104)='P4nc'
      ListGroup(105)='P42mc'
      ListGroup(106)='P42bc'
      ListGroup(107)='I4mm'
      ListGroup(108)='I4cm'
      ListGroup(109)='I41md'
      ListGroup(110)='I41cd'
      ListGroup(111)='P-42m'
      ListGroup(112)='P-42c'
      ListGroup(113)='P-421m'
      ListGroup(114)='P-421c'
      ListGroup(115)='P-4m2'
      ListGroup(116)='P-4c2'
      ListGroup(117)='P-4b2'
      ListGroup(118)='P-4n2'
      ListGroup(119)='I-4m2'
      ListGroup(120)='I-4c2'
      ListGroup(121)='I-42m'
      ListGroup(122)='I-42d'
      ListGroup(123)='P4/mmm'
      ListGroup(124)='P4/mcc'
      ListGroup(125)='P4/nbm'
      ListGroupN(125)=2
      call SetRealArrayTo(ListGroupO(1,125),2,.25)
      ListGroup(126)='P4/nnc'
      ListGroupN(126)=2
      call SetRealArrayTo(ListGroupO(1,126),3,.25)
      ListGroup(127)='P4/mbm'
      ListGroup(128)='P4/mnc'
      ListGroup(129)='P4/nmm'
      ListGroupN(129)=2
      ListGroupO(1,129)= .25
      ListGroupO(2,129)=-.25
      ListGroup(130)='P4/ncc'
      ListGroupN(130)=2
      ListGroupO(1,130)= .25
      ListGroupO(2,130)=-.25
      ListGroup(131)='P42/mmc'
      ListGroup(132)='P42/mcm'
      ListGroup(133)='P42/nbc'
      ListGroupN(133)=2
      ListGroupO(1,133)= .25
      ListGroupO(2,133)=-.25
      ListGroupO(3,133)= .25
      ListGroup(134)='P42/nnm'
      ListGroupN(134)=2
      ListGroupO(1,134)= .25
      ListGroupO(2,134)=-.25
      ListGroupO(3,134)= .25
      ListGroup(135)='P42/mbc'
      ListGroup(136)='P42/mnm'
      ListGroup(137)='P42/nmc'
      ListGroupN(137)=2
      ListGroupO(1,137)= .25
      ListGroupO(2,137)=-.25
      ListGroupO(3,137)= .25
      ListGroup(138)='P42/ncm'
      ListGroupN(138)=2
      ListGroupO(1,138)= .25
      ListGroupO(2,138)=-.25
      ListGroupO(3,138)= .25
      ListGroup(139)='I4/mmm'
      ListGroup(140)='I4/mcm'
      ListGroup(141)='I41/amd'
      ListGroupN(141)=2
      ListGroupO(2,141)=-.25
      ListGroupO(3,141)= .125
      ListGroup(142)='I41/acd'
      ListGroupN(142)=2
      ListGroupO(2,142)=-.25
      ListGroupO(3,142)= .125
      ListGroup(143)='P3'
      ListGroup(144)='P31'
      ListGroup(145)='P32'
      ListGroup(146)='R3'
      ListGroup(147)='P-3'
      ListGroup(148)='R-3'
      ListGroup(149)='P312'
      ListGroup(150)='P321'
      ListGroup(151)='P3112'
      ListGroup(152)='P3121'
      ListGroup(153)='P3212'
      ListGroup(154)='P3221'
      ListGroup(155)='R32'
      ListGroup(156)='P3m1'
      ListGroup(157)='P31m'
      ListGroup(158)='P3c1'
      ListGroup(159)='P31c'
      ListGroup(160)='R3m'
      ListGroup(161)='R3c'
      ListGroup(162)='P-31m'
      ListGroup(163)='P-31c'
      ListGroup(164)='P-3m1'
      ListGroup(165)='P-3c1'
      ListGroup(166)='R-3m'
      ListGroup(167)='R-3c'
      ListGroup(168)='P6'
      ListGroup(169)='P61'
      ListGroup(170)='P65'
      ListGroup(171)='P62'
      ListGroup(172)='P64'
      ListGroup(173)='P63'
      ListGroup(174)='P-6'
      ListGroup(175)='P6/m'
      ListGroup(176)='P63/m'
      ListGroup(177)='P622'
      ListGroup(178)='P6122'
      ListGroup(179)='P6522'
      ListGroup(180)='P6222'
      ListGroup(181)='P6422'
      ListGroup(182)='P6322'
      ListGroup(183)='P6mm'
      ListGroup(184)='P6cc'
      ListGroup(185)='P63cm'
      ListGroup(186)='P63mc'
      ListGroup(187)='P-6m2'
      ListGroup(188)='P-6c2'
      ListGroup(189)='P-62m'
      ListGroup(190)='P-62c'
      ListGroup(191)='P6/mmm'
      ListGroup(192)='P6/mcc'
      ListGroup(193)='P63/mcm'
      ListGroup(194)='P63/mmc'
      ListGroup(195)='P23'
      ListGroup(196)='F23'
      ListGroup(197)='I23'
      ListGroup(198)='P213'
      ListGroup(199)='I213'
      ListGroup(200)='Pm-3'
      ListGroup(201)='Pn-3'
      ListGroupN(201)=2
      call SetRealArrayTo(ListGroupO(1,201),3,.25)
      ListGroup(202)='Fm-3'
      ListGroup(203)='Fd-3'
      ListGroupN(203)=2
      call SetRealArrayTo(ListGroupO(1,203),3,.125)
      ListGroup(204)='Im-3'
      ListGroup(205)='Pa-3'
      ListGroup(206)='Ia-3'
      ListGroup(207)='P432'
      ListGroup(208)='P4232'
      ListGroup(209)='F432'
      ListGroup(210)='F4132'
      ListGroup(211)='I432'
      ListGroup(212)='P4332'
      ListGroup(213)='P4132'
      ListGroup(214)='I4132'
      ListGroup(215)='P-43m'
      ListGroup(216)='F-43m'
      ListGroup(217)='I-43m'
      ListGroup(218)='P-43n'
      ListGroup(219)='F-43c'
      ListGroup(220)='I-43d'
      ListGroup(221)='Pm-3m'
      ListGroup(222)='Pn-3n'
      ListGroupN(222)=2
      call SetRealArrayTo(ListGroupO(1,222),3,.25)
      ListGroup(223)='Pm-3n'
      ListGroup(224)='Pn-3m'
      ListGroupN(224)=2
      call SetRealArrayTo(ListGroupO(1,224),3,.25)
      ListGroup(225)='Fm-3m'
      ListGroup(226)='Fm-3c'
      ListGroup(227)='Fd-3m'
      ListGroupN(227)=2
      call SetRealArrayTo(ListGroupO(1,227),3,.125)
      ListGroup(228)='Fd-3c'
      ListGroupN(228)=2
      call SetRealArrayTo(ListGroupO(1,228),3,.375)
      ListGroup(229)='Im-3m'
      ListGroup(230)='Ia-3d'
      MaxNSymm=1
      MaxNDim=3
      MaxNDimI=0
      MaxNComp=1
      MaxNLattVec=1
      call UnitMat(TrMP,3)
      call AllocateSymm(MaxNSymm,MaxNLattVec,MaxNComp,NPhase)
      KPhase=1
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      NLattVec(KPhase)=1
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      call AllocateSymm(1,1,1,1)
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      CellPar(1,1,KPhase)=5.
      CellPar(2,1,KPhase)=6.
      CellPar(3,1,KPhase)=7.
      CellPar(4,1,KPhase)=90.
      CellPar(5,1,KPhase)=95.
      CellPar(6,1,KPhase)=90.
      Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1     'spgr3test.txt'
      call OpenFile(46,Veta,'formatted','unknown')
      do n=1,230
c      do n=22,22
        if(n.gt.2.and.n.lt.16) then
          Monoclinic(KPhase)=2
          CrSystem(KPhase)=22
        else
          Monoclinic(KPhase)=0
        endif
        Grupa(KPhase)=ListGroup(n)
        Choice=1
        call SetRealArrayTo(ShSg,4,0.)
1100    Veta='Group : '//Grupa(KPhase)(:idel(Grupa(KPhase)))
        if(ListGroupN(n).gt.1) then
          write(Cislo,'('' - choice : '',i1)') Choice
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(46,FormA) Veta(:idel(Veta))
        write(46,FormA1)('=',i=1,idel(Veta))
        write(46,FormA1)
        call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
        call EM50MakeStandardOrder
        Veta='(0,0,0,0)+'
        do i=2,NLattVec(KPhase)
          t80='('
          do j=1,NDim(KPhase)
            call ToFract(vt6(j,i,1,KPhase),Cislo,15)
            call zhusti(Cislo)
            t80=t80(:idel(t80))//Cislo(:idel(Cislo))
            if(j.lt.NDim(KPhase)) then
              t80=t80(:idel(t80))//','
            else
              t80=t80(:idel(t80))//')+'
            endif
          enddo
          Veta=Veta(:idel(Veta)+1)//t80(:idel(t80))
        enddo
        write(46,FormA1)(Veta(i:i),i=1,idel(Veta))
        write(46,FormA1)
        nn=0
        do j=1,NSymm(KPhase)
          call codesymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                  symmc(1,j,1,KPhase),0)
          do k=1,NDim(KPhase)
            kk=idel(symmc(k,j,1,KPhase))+3
            if(symmc(k,j,1,KPhase)(1:1).ne.'-') kk=kk+1
            nn=max(nn,kk)
          enddo
        enddo
        if(nn.lt.5) nn=5
        do j=1,NSymm(KPhase)
          t80=' '
          write(t80,'(''('',i5,'')'')') j
          call Zhusti(t80)
          k=10
          do l=1,NDim(KPhase)
            if(symmc(l,j,1,KPhase)(1:1).eq.'-') then
              kk=k
            else
              kk=k+1
            endif
            t80(kk:)=symmc(l,j,1,KPhase)
            k=k+nn
          enddo
          call newln(1)
          write(46,FormA1)(t80(l:l),l=1,idel(t80))
        enddo
        write(46,FormA1)
        if(NGrupa(KPhase).le.1) go to 1800
        if(NGrupa(KPhase).eq.2) then
          ipg=2
        else if(NGrupa(KPhase).ge.3.and.NGrupa(KPhase).le.5) then
          ipg=3
        else if(NGrupa(KPhase).ge.6.and.NGrupa(KPhase).le.9) then
          ipg=4
        else if(NGrupa(KPhase).ge.10.and.NGrupa(KPhase).le.15) then
          ipg=5
        else if(NGrupa(KPhase).ge.16.and.NGrupa(KPhase).le.24) then
          CellPar(5,1,KPhase)=90.
          ipg=6
        else if(NGrupa(KPhase).ge.25.and.NGrupa(KPhase).le.46) then
          ipg=7
        else if(NGrupa(KPhase).ge.47.and.NGrupa(KPhase).le.74) then
          ipg=8
        else if(NGrupa(KPhase).ge.75.and.NGrupa(KPhase).le.80) then
          CellPar(1,1,KPhase)=5.
          CellPar(2,1,KPhase)=5.
          ipg=9
        else if(NGrupa(KPhase).ge.81.and.NGrupa(KPhase).le.82) then
          ipg=10
        else if(NGrupa(KPhase).ge.83.and.NGrupa(KPhase).le.88) then
          ipg=11
        else if(NGrupa(KPhase).ge.89.and.NGrupa(KPhase).le.98) then
          ipg=12
        else if(NGrupa(KPhase).ge.99.and.NGrupa(KPhase).le.110) then
          ipg=13
        else if(NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.122) then
          ipg=14
          if((NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.114).or.
     1       (NGrupa(KPhase).ge.121.and.NGrupa(KPhase).le.122)) then
            ipgp=14
          else
            ipgp=15
          endif
        else if(NGrupa(KPhase).ge.123.and.NGrupa(KPhase).le.142) then
          ipg=15
        else if(NGrupa(KPhase).ge.143.and.NGrupa(KPhase).le.146) then
          CellPar(6,1,KPhase)=120.
          ipg=16
        else if(NGrupa(KPhase).ge.147.and.NGrupa(KPhase).le.148) then
          ipg=17
        else if(NGrupa(KPhase).ge.149.and.NGrupa(KPhase).le.155) then
          CellPar(6,1,KPhase)=120.
          ipg=18
          if(NGrupa(KPhase).eq.155) then
            ipgp=25
C            32
          else if(mod(NGrupa(KPhase)-148,2).eq.1) then
            ipgp=19
C            312
          else
            ipgp=22
C            321
          endif
        else if(NGrupa(KPhase).ge.156.and.NGrupa(KPhase).le.161) then
          ipg=19
          if(NGrupa(KPhase).ge.160) then
            ipgp=26
C            3m
          else if(mod(NGrupa(KPhase)-155,2).eq.1) then
            ipgp=23
C            3m1
          else
            ipgp=20
C            31m
          endif
        else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.167) then
          ipg=20
          if(NGrupa(KPhase).ge.166) then
            ipgp=27
C            -3m
          else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.163) then
            ipgp=21
C            -31m
          else if(NGrupa(KPhase).ge.164.and.NGrupa(KPhase).le.165) then
            ipgp=24
C            -3m1
          endif
        else if(NGrupa(KPhase).ge.168.and.NGrupa(KPhase).le.173) then
          ipg=21
        else if(NGrupa(KPhase).ge.174.and.NGrupa(KPhase).le.174) then
          ipg=22
        else if(NGrupa(KPhase).ge.175.and.NGrupa(KPhase).le.176) then
          ipg=23
        else if(NGrupa(KPhase).ge.177.and.NGrupa(KPhase).le.182) then
          ipg=24
        else if(NGrupa(KPhase).ge.183.and.NGrupa(KPhase).le.186) then
          ipg=25
        else if(NGrupa(KPhase).ge.187.and.NGrupa(KPhase).le.190) then
          ipg=26
          if(NGrupa(KPhase).le.188) then
            ipgp=33
          else
            ipgp=34
          endif
        else if(NGrupa(KPhase).ge.191.and.NGrupa(KPhase).le.194) then
          ipg=27
        else if(NGrupa(KPhase).ge.195.and.NGrupa(KPhase).le.199) then
          ipg=28
        else if(NGrupa(KPhase).ge.200.and.NGrupa(KPhase).le.206) then
          ipg=29
        else if(NGrupa(KPhase).ge.207.and.NGrupa(KPhase).le.214) then
          ipg=30
        else if(NGrupa(KPhase).ge.215.and.NGrupa(KPhase).le.220) then
          ipg=31
        else if(NGrupa(KPhase).ge.221.and.NGrupa(KPhase).le.230) then
          ipg=32
        endif
        if(ipg.le.13) then
          ipgp=ipg
        else if(ipg.ge.15.and.ipg.le.17) then
          ipgp=ipg+1
        else if(ipg.ge.21.and.ipg.le.25) then
          ipgp=ipg+7
        else if(ipg.ge.27) then
          ipgp=ipg+8
        endif
        PointGroup=SmbPGI(ipgp)
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'pgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
1700    read(ln,FormA80,end=1800) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1    go to 1700
        call Kus(Veta,k,Cislo)
        call StToInt(Veta,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 1800
        m=iar(1)
        do i=1,m
          read(ln,FormA80,end=1800) Veta
        enddo
        read(ln,FormA80,end=1800) Veta
        k=0
        call StToInt(Veta,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 1800
        NSubGr=iar(1)
        if(NSubGr.le.0) go to 1800
        allocate(sel(NSymm(KPhase),NSubGr))
        call SetIntArrayTo(Sel,NSymm(KPhase)*NSubGr,0)
        do i=1,NSubGr
          read(ln,FormA80,end=1800) Veta
          k=0
          call StToInt(Veta,k,Sel(1,i),NSymm(KPhase),.false.,ich)
        enddo
        call CloseIfOpened(ln)
        m=NSymm(KPhase)*NLattVec(KPhase)
        allocate(rm6s(NDimQ(KPhase),m),s6s(NDim(KPhase),m),ipri(m),
     1           BratSymmSub(m),vt6s(NDim(KPhase),m),io(3,m),
     2           OpSmb(m),ios(3,m),OpSmbS(m),IswSymms(m),
     3           ZMagS(m),RMagS(9,m))
        do i=1,NSymm(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),
     1               t2m,io(1,i),m,det)
          ipri(i)=i
        enddo
        call ReallocSymm(NDim(KPhase),m,NLattVec(KPhase),NComp(KPhase),
     1                   NPhase,0)
        do i=1,NSubGr
          call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.false.)
          jk=NSymm(KPhase)
          do j=1,NSymm(KPhase)
            k=Sel(j,i)
            if(k.gt.0) then
              BratSymmSub(k)=.true.
            else
              jk=j-1
              go to 1750
            endif
          enddo
1750      call GoToSubGrGetGrSmb(nss,nvts,Grp,GrpA(i),NGrp,ncoset,
     1                           TrPom,TrPomI,TrShift,icrsys,IsNormal,0)
          NGrpA(i)=ind*100000*NLattVec(KPhase)/nvts+(300-NGrp)*100+i
        enddo
        call indexx(NSubGr,NGrpA,PorGrpA)
        do i=1,NSubGr
          m=PorGrpA(i)
          do j=1,NSymm(KPhase)
            k=Sel(j,m)
            if(k.le.0) then
              jk=j-1
              go to 1770
            endif
          enddo
1770      write(46,FormA)
          write(46,'(''Kombinace:'',60i3)')(Sel(j,m),j=1,jk)
          write(46,'(''Subgroup :  '',a)') GrpA(m)(:idel(GrpA(m)))
          write(46,FormA)
        enddo
        deallocate(rm6s,s6s,ipri,BratSymmSub,vt6s,io,OpSmb,ios,OpSmbS,
     1             IswSymms,ZMagS,RMagS)
        deallocate(Sel)
1800    call CloseIfOpened(ln)
        if(Choice.lt.ListGroupN(n)) then
          Choice=Choice+1
          call CopyVek(ListGroupO(1,n),ShSg,3)
          go to 1100
        endif
      enddo
      close (46)
      deallocate(ListGroup,ListGroupN,ListGroupO)
      call CloseIfOpened(ln)
      return
      end
