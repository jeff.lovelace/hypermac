      subroutine EditM50(ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      integer TypTextu
      real CellParOld(6)
      logical eqrv,ExistFile,FeYesNo,M50JesteNeexistovala,Zpet,EqIgCase,
     1        FeYesNoHeader,ChargeDensitiesIn,SymmetryNotChanged,
     2        PrepisM95
      external EM50UpdateListek
      character*256 Veta,t256
      dimension rmo(36),rmp(36),QrPom(3,3)
      ich=0
      KartIdZpet=1
1000  M50JesteNeexistovala=.not.ExistM50
      if(ExistM50) then
        CellParOld(1:6)=CellPar(1:6,1,KPhase)
        KCommenOld=KCommen(KPhase)
        if(StatusM50.lt.11000) call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
        IStSymmOrg=0
        call CrlStoreSymmetry(IStSymmOrg)
        IStSymmOrgComm=0
        if(KCommen(KPhase).gt.0) then
          call ComSym(0,0,ichp)
          call CrlStoreSymmetry(IStSymmOrgComm)
          if(StatusM50.lt.11000) call iom50(0,0,fln(:ifln)//'.m50')
          if(ParentStructure) call SSG2QMag(fln)
        endif
      else
        KCommenOld=0
        call SetBasicM50
        call AllocateSymm(1,1,1,1)
        NSymm(KPhase)=1
        MaxNSymm=1
        BratSymm(1,KPhase)=.true.
        do i=1,NSymm(KPhase)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                  symmc(1,i,1,KPhase),0)
        enddo
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=1
        m=121
        i=1
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4           AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7           FFra(n,NPhase,i),FFia(n,NPhase,i),
     8           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9           TypeFFMag(n,NPhase),
     a           FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1           PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2           ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3           HNSlater(n,NPhase),HZSlater(n,NPhase),
     4           ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5           NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6           CoreValSource(n,NPhase))
        FFCoreD=0.
        FFValD=0.
        CoreValSource='Default'
        FFType(KPhase)=-62
        do i=1,n
          AtType(i,KPhase)='Fe'
          AtTypeMag(i,KPhase)=' '
          AtTypeMagJ(i,KPhase)=' '
          AtMult(i,KPhase)=1.
          AtTypeFull(i,KPhase)=AtType(i,KPhase)
          AtNum(1,KPhase)=1.
          call EM50ReadOneFormFactor(i)
          call EM50OneFormFactorSet(i)
        enddo
      endif
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      if(ExistM40) call EM40AtomTypeSave
      MagneticTypeO=MagneticType(KPhase)
      NDimo=NDim(KPhase)
      maxNDimO=maxNDim
      if(WizardMode) then
        id=NextQuestId()
        QuestColorFill(id)=WhiteGray
        xdq=WizardLength
        il=WizardLines
        nButtOK=0
        nButtESC=0
      else
        xdq=600.
        il=17
        nButtOK=OKForBasicFiles
        nButtESC=0
      endif
      if(ExistM90) then
        AllowChargeDensities=ExistXRayData
      else
        AllowChargeDensities=Radiation(1).eq.XRayRadiation
      endif
      Veta='Define/modify basic structural parameters:'
      if(NPhase.gt.1) Veta=Veta(:idel(Veta))//' for phase : '//
     1                     PhaseName(KPhase)
      call FeKartCreate(-1.,-1.,xdq,il,Veta,nButtESC,nButtOK)
      xdqp=xdq-2.*KartSidePruh
      if(NPhase.gt.1) then
        ypom=35.
        dpom=0.
        do i=1,NPhase
          dpom=max(dpom,FeTxLength(PhaseName(i)))
        enddo
        dpom=dpom+2.*EdwMarginSize+EdwYd
        Veta=' '
        xpom=xdq*.5-dpom*.5
        tpom=xpom
        call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                             Veta,'L',dpom,EdwYd,1)
        nRolMenuPhase=RolMenuLastMade+RolMenuFr-1
        RolMenuAlways(nRolMenuPhase)=.true.
        call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                          KPhase)
      else
        nRolMenuPhase=0
      endif
      ChargeDensitiesIn=ChargeDensities
      call FeCreateListek('Cell',1)
      KartIdCell=KartLastId
      call EM50CellMake(KartIdCell)
      if(NDatBlock.le.1.and..not.ExistM90.and..not.ExistM95) then
        call FeCreateListek('Radiation',1)
        KartIdRadiation=KartLastId
        call EM50RadiationMake(KartIdRadiation)
      else
        KartIdRadiation=0
      endif
      call FeCreateListek('Symmetry',1)
      KartIdSymmetry=KartLastId
      call EM50SymmetryMake(KartIdSymmetry,0)
      call EM50SymmetryRefresh
      call FeCreateListek('Composition',1)
      KartIdComposition=KartLastId
      call EM50CompositionMake(KartIdComposition)
      call FeCreateListek('Multipole parameters',1)
      KartIdMultipols=KartLastId
      call EM50MultipolesMake(KartIdMultipols)
      call FeCreateListek('Magnetic parameters',1)
      KartIdMagnetic=KartLastId
      call EM50MagneticMake(KartIdMagnetic)
      call FeCompleteKart(KartIdZpet)
2500  ErrFlag=0
3000  Zpet=.false.
3010  call FeQuestEventWithKartUpdate(KartId,ich,
     1                                EM50UpdateListek)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(NComp(KPhase).gt.ncompold) then
          call FeChybne(-1.,-1.,'new composite matrices were not yet'//
     1                  ' defined.',' ',SeriousError)
          call FePrepniListek(KartIdCell-1)
          CheckType=EventButton
          CheckNumberAbs=ButtonOk
          CheckNumber=ButtonOk-ButtonFr+1
          EventType=EventButton
          EventNumber=nButtCompMat
          go to 3000
        endif
        if(KCommen(KPhase).gt.0) then
          if(NCommen(1,1,KPhase)*NCommen(2,1,KPhase)*
     1       NCommen(3,1,KPhase).le.0) then
            write(Veta,'(2(i5,''x''),i5)')(NCommen(i,1,KPhase),i=1,3)
            call Zhusti(Veta)
            call FeChybne(-1.,-1.,'unrealistic supercell: '//
     1                    Veta(:idel(Veta)),' ',SeriousError)
            call FePrepniListek(KartIdCell-1)
            CheckType=EventButton
            CheckNumberAbs=ButtonOk
            CheckNumber=ButtonOk-ButtonFr+1
            EventType=EventEdw
            EventNumber=nEdwSupCell
            go to 3000
          endif
        endif
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
          call EM50CellUpdate(ich)
          if(ich.ne.0) go to 3000
        else if(KartId.eq.KartIdRadiation) then
          call EM50RadiationUpDate
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryComplete
        else if(KartId.eq.KartIdComposition) then
          call EM50CompositionUpDate
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesUpDate
        endif
        call EM50CellSymmTest(0,ich)
        if(ich.ne.0) then
          EventType=EventKartSw
          EventNumber=KartIdCell+1-KartFirstId
          ich=0
          go to 3000
        endif
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        if(.not.Zpet) go to 3000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumberAbs.eq.nRolMenuPhase) then
        KPhaseNew=RolMenuSelected(nRolMenuPhase)
        if(KPhaseNew.gt.0.and.KPhaseNew.ne.KPhase) then
          ich=0
          Zpet=.true.
          EventType=EventButton
          EventNumber=ButtonOk-ButtonFr+1
          EventNumberAbs=ButtonOk
          KartIdZpet=KartId-1
          go to 3010
        else
          go to 3000
        endif
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
          call EM50CellUpdate(ich)
          if(ich.ne.0) QuestCheck(KartId)=1
        else if(KartId.eq.KartIdRadiation) then
          call EM50RadiationUpDate
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryComplete
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesUpDate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdCell) then
          call EM50CellCheck
        else if(KartId.eq.KartIdSymmetry) then
          call EM50SymmetryCheck
          if(ErrFlag.eq.0) then
            call EM50SymmetryRefresh
          else
            go to 2500
          endif
        else if(KartId.eq.KartIdComposition) then
          call EM50CompositionCheck
        else if(KartId.eq.KartIdMultipols) then
          call EM50MultipolesCheck
        else if(KartId.eq.KartIdMagnetic) then
          call EM50MagneticCheck
        else
          if(KartIdRadiation.gt.0) call EM50RadiationCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        MaxNSymm=max(MaxNSymm,NSymm(KPhase))
        if(allocated(isa)) deallocate(isa)
        allocate(isa(MaxNSymm,MxAtAll))
        if(NUnits(KPhase).le.0)
     1    NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        if(ParentStructure) call QMag2SSG(fln,0)
        if(Zpet) then
          call iom50(1,0,fln(:ifln)//'.m50')
        else
          call QuestionRewriteFile(50)
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
        if(isPowder.and.ExistFile(fln(:ifln)//'.m41')) then
          call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
          if(ParentStructure) then
            call CopyVek(QMag(1,1,KPhase),QuPwd(1,1,KPhase),3)
          else
            call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),
     1                   3*NDimI(KPhase))
          endif
        endif
        do i=1,MxAtAll
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        if(ExistM40) then
          if(NAtFormula(KPhase).gt.0) call EM40AtomTypeModify
        else
          call SetBasicM40(.false.)
        endif
        if(NDim(KPhase).ne.NDimo.or.m50JesteNeexistovala) then
          if(NDim(KPhase).gt.3) then
            do i=1,mxw
              call SetIntArrayTo(kw(1,i,KPhase),3,0)
              if(i.le.NDimI(KPhase).or.NDim(KPhase).eq.4)
     1          kw(mod(i-1,NDimI(KPhase))+1,i,KPhase)=
     2            (i-1)/NDimI(KPhase)+1
            enddo
          else
            NComp(KPhase)=1
            do i=2,3
              NAtIndFr(i,KPhase)=NAtIndTo(1,KPhase)+1
              NAtIndTo(i,KPhase)=NAtIndTo(1,KPhase)
              NAtIndLen(i,KPhase)=0
              NMolecFr(i,KPhase)=NMolecTo(1,KPhase)+1
              NMolecTo(i,KPhase)=NMolecTo(1,KPhase)
              NMolecLen(i,KPhase)=0
            enddo
          endif
        endif
        if(ChargeDensitiesIn.neqv.ChargeDensities) then
          if(ChargeDensitiesIn) then
            do i=1,NAtInd
              if(kswa(i).eq.KPhase) lasmax(i)=0
            enddo
          endif
        endif
        PrepisM95=.false.
        if(NDimO.ne.NDim(KPhase)) then
          MaxNDim=max(NDim(KPhase),MaxNDim)
          MaxNDimI=MaxNDim-3
          call iom40Only(0,0,fln(:ifln)//'.m40')
          if(ExistM95) then
            call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
            call MatBlock3(TrMPI,rmo,maxNDimO)
            do i=4,MaxNDim
              if(i.gt.MaxNDimO) then
                call MultM(Qu(1,i-3,1,KPhase),rmo,
     1                     QuRefBlock(1,i-3,0),1,3,3)
              endif
            enddo
            call UnitMat(rmo,maxNDim)
            n=min(maxNDimO,maxNDim)
            do i=1,n
              do j=1,n
                rmo(i+(j-1)*maxNDim)=TrMP(i+(j-1)*maxNDimO)
              enddo
            enddo
            call CopyMat(rmo,TrMP,maxNDim)
            call MatInv(TrMP,TrMPI,pom,maxNDim)
            PrepisM95=.true.
          endif
        endif
        if(ExistM95.and.KPhase.eq.1.and.
     1     (.not.EqRV(CellParOld(1),CellPar(1,1,KPhase),3,.0001).or.
     2      .not.EqRV(CellParOld(4),CellPar(4,1,KPhase),3,.001))) then
          if(.not.PrepisM95)
     1      call CopyFile(fln(:ifln)//'.m95',fln(:ifln)//'.z95')
          call MatBlock3(TrMPI,rmo,maxNDim)
          call UnitMat(rmp,maxNDim)
          CellRefBlock(1:6,0)=CellPar(1:6,1,KPhase)
          do i=1,NDimI(KPhase)
            QuRefBlock(1:3,i,0)=Qu(1:3,i,1,KPhase)
            QrPom(1:3,i)=0.
          enddo
          call DRMatTrCellQ(rmo,CellRefBlock(1,0),QuRefBlock(1:3,i,0),
     1                      QrPom,rmp)
          PrepisM95=.true.
        endif
        if(PrepisM95) then
          call iom95(1,fln(:ifln)//'.m95')
          lni=NextLogicNumber()
          call OpenFile(lni,fln(:ifln)//'.z95','formatted','old')
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'.m95','formatted','unknown')
4100      read(lni,FormA) Veta
          if(.not.EqIgCase(Veta,'end')) go to 4100
4200      read(lno,FormA) Veta
          if(.not.EqIgCase(Veta,'end')) go to 4200
4300      read(lni,FormA,end=4400) Veta
          write(lno,FormA) Veta(:idel(Veta))
          go to 4300
4400      call CloseIfOpened(lni)
          call CloseIfOpened(lno)
        endif
        if(NTwinin.ne.NTwin) then
          if(NTwin.gt.NTwinIn) mxscutw=mxscutw+NTwin-NTwinIn
          pom=1./float(NTwin)
          call SetRealArrayTo(sctw(1,KDatBlock),NTwin,pom)
        endif
        if(MagneticTypeO.gt.0.and.MagneticType(KPhase).eq.0) then
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase) cycle
            MagPar(i)=0
          enddo
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
        if(Zpet) then
          KPhase=KPhaseNew
          call FeDestroyKart
          go to 1000
        endif
      else
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ParentStructure) call SSG2QMag(fln)
      endif
      call FeDestroyKart
      KPhase=KPhaseBasic
      if(StatusM50.le.100.and.ich.eq.0.and.
     1   (.not.ExistM90.and.ExistM95)) then
        if(FeYesNo(-1.,-1.,'Do you want to create refinement '//
     1             'reflection file?',1)) then
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
        endif
        go to 9999
      endif
      if(isPowder.or..not.ExistM95.or.ich.ne.0) go to 9999
      if(.not.M50JesteNeexistovala) then
        TypTextu=1
        call CrlCompareSymmetry(IStSymmOrg,SymmetryNotChanged)
        if(.not.SymmetryNotChanged) go to 5000
        if(NTwinIn.ne.NTwin) go to 5000
        do 4500i=1,NTwin
          do j=1,NTwin
            if(EqRV(RTwIn(1,i),RTw(1,j),9,.001)) go to 4500
          enddo
          go to 5000
4500    continue
        if(StatusM50.lt.11000) then
          call iom50(0,0,fln(:ifln)//'.m50')
          if(ParentStructure) call SSG2QMag(fln)
        endif
        if(KCommen(KPhase).ne.KCommenOld) then
          TypTextu=2
          go to 5000
        else if(KCommen(KPhase).gt.0) then
          call ComSym(0,0,ich)
          call CrlCompareSymmetry(IStSymmOrgComm,SymmetryNotChanged)
          if(StatusM50.lt.11000) then
            call iom50(0,0,fln(:ifln)//'.m50')
            if(ParentStructure) call SSG2QMag(fln)
          endif
          if(.not.SymmetryNotChanged) go to 5000
        endif
      endif
      go to 9999
5000  if(ExistSingle) then
        NInfo=2
        if(TypTextu.eq.1) then
          TextInfo(1)='The program has detected a change of the '//
     1                'symmetry/twinning which call for'
          TextInfo(2)='re-creation of the reflection file.'
        else
          Veta='Description of the structure has been changed from'
          if(KCommen(KPhase).gt.0) then
            TextInfo(1)=Veta(:idel(Veta)+1)//
     1                  'incommensurate to commensurate'
          else
            TextInfo(1)=Veta(:idel(Veta)+1)//
     1                  'commensurate to incommensurate'
          endif
          TextInfo(2)='which call for a new averaging.'
        endif
        if(FeYesNoHeader(-1.,-1.,'Do you want to re-create '//
     1                   'refinement reflection file just now?',1)) then
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          if(NPhase.gt.1) then
            call EM9ImportWizard(1)
          else
            call EM9CreateM90(0,ich)
          endif
        endif
        Veta=fln(:ifln)//'.inflip'
        if(ExistFile(Veta)) call DeleteFile(Veta)
      endif
9999  call DeleteFile(fln(:ifln)//'.l52')
      call CrlCleanSymmetry
      if(WizardMode) QuestColorFill(id)=LightGray
      if(allocated(RTwIn)) deallocate(RTwIn)
      return
      end
