      subroutine EM50SymbolsForStokes
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*80  Veta
      character*1 :: smbs(6) = (/'x','y','z','t','u','v'/),String(32767)
      logical AbsYes,Prvni
      ln=NextLogicNumber()
      lno=0
      call OpenFile(ln,fln(:ifln)//'_stokes.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      call FeFillTextInfo('forstokes.txt',0)
      do i=1,NInfo
        write(ln,FormA) TextInfo(i)(:idel(TextInfo(i)))
      enddo
      write(ln,'(/''Centering vectors:''/)')
      do i=2,NLattVec(KPhase)
        Veta=' '
        do j=1,NDim(KPhase)
          call ToFract(vt6(j,i,1,KPhase),Cislo,6)
          if(Veta.eq.' ') then
            Veta=Cislo
          else
            Veta=Veta(:idel(Veta))//','//Cislo(:idel(Cislo))
          endif
        enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      write(ln,'(/''Symmetry operations:''/)')
      do is=2,NSymmN(KPhase)
        Veta=' '
        do i=1,NDim(KPhase)
          pom=s6(i,is,1,KPhase)
          if(pom.ne.0) then
            call ToFract(pom,Cislo,6)
            if(Veta.eq.' ') then
              Veta=Cislo
            else
              Veta=Veta(:idel(Veta))//','//Cislo(:idel(Cislo))
            endif
            AbsYes=.true.
          else
            if(Veta.ne.' ') Veta=Veta(:idel(Veta))//','
            AbsYes=.false.
          endif
          k=i-NDim(KPhase)
          Prvni=.true.
          do j=1,NDim(KPhase)
            k=k+NDim(KPhase)
            pom=rm6(k,is,1,KPhase)
            if(pom.eq.0.) then
              cycle
            else
              call ToFract(pom,Cislo,6)
            endif
            if(pom.gt.0.) then
              if(.not.Prvni.or.AbsYes) Veta=Veta(:idel(Veta))//'+'
              if(Cislo.ne.'1')
     1          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
            else
              if(Cislo(2:2).eq.'1') then
                Veta=Veta(:idel(Veta))//'-'
              else
                Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
              endif
            endif
            Prvni=.false.
            Veta=Veta(:idel(Veta))//smbs(j)
          enddo
          enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
!      if(CallWGet.ne.' ') then
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_stokes.txt','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 9999
        rewind ln
        k=0
        call FillStringArray(String,k,'centering=')
1500    read(ln,FormA,end=9999) Veta
        if(LocateSubstring(Veta,'Centering vectors:',.false.,.true.)
     1     .le.0) go to 1500
1600    read(ln,FormA,end=9999) Veta
        if(Veta.eq.' ') go to 1600
        if(LocateSubstring(Veta,'Symmetry operations:',.false.,.true.)
     1     .le.0) then
          Veta=Veta(:idel(Veta))//';'
          call TranslateToPostData(Veta,t256)
          call FillStringArray(String,k,t256)
          go to 1600
        endif
        call FillStringArray(String,k,'&ops=')
1700    read(ln,FormA,end=2000) Veta
        if(Veta.eq.' ') go to 1700
        Veta=Veta(:idel(Veta))//';'
        call TranslateToPostData(Veta,t256)
        call FillStringArray(String,k,t256)
        go to 1700
2000    write(lno,'(32767a1)') String(1:k)
        close(ln,status='delete')
        call CloseIfOpened(lno)
        t256=HomeDir(:idel(HomeDir))//'gnu'//ObrLom//'wget'//ObrLom//
     1       'wget.exe'
        t256='"'//t256(:idel(t256))//'" -q '//
     1       'http://stokes.byu.edu/iso/findssgform.php --post-file '//
     2       fln(:ifln)//'_stokes.txt -O '//fln(:ifln)//'_stokes.html'
        WaitTime=5000
        NInfo=3
        TextInfo(1)='Starting GNU Wget'
        TextInfo(2)='Free software for retrieving files using HTTP, '//
     1              'HTTPS and FTP.'
        TextInfo(3)='For details see http://www.gnu.org/software/wget'
        call FeInfoOut(-1.,-1.,'INFORMATION','C')
        call FeSystem(t256)
        Veta=fln(:ifln)//'_stokes.html'
        call FeShellExecute(Veta)
!      else
!        NInfo=4
!        TextInfo(1)='Important note : after downloading, installing '//
!     1              'WGet program'
!        TextInfo(2)='(e.g. http://gnuwin32.sourceforge.net/packages/'//
!     1              'wget.htm) and'
!        TextInfo(3)='defining WGet command in Jana2006 (Tools->'//
!     1              'Programs)'
!        TextInfo(4)='you can make SSG-check faster without pasting '//
!     1              'data.'
!        call FeInfoOut(-1.,-1.,'INFORMATION','L')
!        call CloseIfOpened(ln)
!        Veta='http://stokes.byu.edu/iso/findssg.html'
!        call FeShellExecute(Veta)
!        Veta=fln(:ifln)//'_stokes.tmp'
!        call FeEdit(Veta)
!        call DeleteFile(Veta)
!      endif
      go to 9999
9000  ErrFlag=0
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      return
      end
