      subroutine EM50SetGen(rm,nr,md,ndim)
      include 'fepc.cmn'
      dimension irm(9,7),rm(*)
      data irm/-1,0,0,0,-1,0,0,0,1,0,1,0,-1,-1,0,0,0,1,0,1,0,-1,0,0,
     1         0,0,1,1,1,0,-1,0,0,0,0,1,0,1,0,1,0,0,0,0,-1,0,-1,0,-1,0,0
     2        ,0,0,-1,0,1,0,0,0,1,1,0,0/
      id=md*3
      i1=-3
      mdd=md-1
      call SetRealArrayTo(rm,ndim**2,0.)
      do i=1,3
        i1=i1+3
        i1p=mod(i1+id,9)+1
        do j=1,3
          ij=i1+j
          ijp=i1p+mod(j+mdd,3)
          im=(ijp-1)/3+1
          jm=mod(ijp-1,3)+1
          rm(jm+(im-1)*ndim)=irm(ij,nr)
        enddo
      enddo
      return
      end
