      subroutine EM50FormFactorsSet
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ff(62),popc(7),popv(7),popp(7)
      character*80 Veta,VetaBasic
      logical EqIgCase,ExistWaveF
      i1=1
      i2=NAtFormula(KPhase)
      Klic=0
      go to 1000
      entry EM50OneFormFactorSet(n)
      i1=n
      i2=n
      Klic=0
      go to 1000
      entry EM50OneMultipoleSet(n)
      i1=n
      i2=n
      Klic=1
1000  if(FFType(KPhase).gt.0.or.FFType(KPhase).eq.-62.or.
     1   FFType(KPhase).eq.-56) then
        VetaBasic='Xray_form_factor_in_steps'
        nt=62
      else if(FFType(KPhase).eq.-9) then
        VetaBasic='Xray_form_factor_analytical'
        nt=9
      endif
      do 5000i=i1,i2
        if(Klic.eq.1) go to 2400
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1              'Neutron_scattering_length_real',ff,2,ich)
        ffn(i,KPhase)=ff(2)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1              'Neutron_scattering_length_imaginary',ff,2,ich)
        ffni(i,KPhase)=ff(2)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),
     1   'Electron_form_factor_in_steps',FFEl(1,i,KPhase),62,ich)
        call RealAFromAtomFile(AtTypeFull(i,KPhase),VetaBasic,ff,nt,ich)
        if(AtTypeMag(i,KPhase).ne.' ') then
          if(LocateSubstring(AtTypeMag(i,KPhase),'own',.false.,.true.)
     1       .le.0) then
            if(EqIgCase(AtTypeMagJ(i,KPhase),'j0j2')) then
              Veta='Magnetic_formfactor_<j0>+c<j2>'
            else
              Veta='Magnetic_formfactor_<j0>'
            endif
            call MagFFFromAtomFile(AtTypeFull(i,KPhase),
     1        AtTypeMag(i,KPhase),Veta,FFMag(1,i,KPhase),ich)
          endif
        endif
        if(FFType(KPhase).gt.0) then
          call TableFFToEquidistant(ff,FFBasic(1,i,KPhase),
     1                              AtTypeFull(i,KPhase))
        else if(FFType(KPhase).eq.-9) then
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        else if(FFType(KPhase).eq.-62.or.FFType(KPhase).eq.-56) then
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        else
          call CopyVek(ff,FFBasic(1,i,KPhase),nt)
        endif
        do KDatB=1,NDatBlock
          ffra(i,KPhase,KDatB)=0.
          ffia(i,KPhase,KDatB)=0.
        enddo
        do KDatB=1,NDatBlock
          if(Radiation(KDatB).eq.XRayRadiation) then
            if(KAnRef(KDatB).ne.1) then
              if(klam(KDatB).gt.0) then
                call RealFromAtomFile(AtTypeFull(i,KPhase),'Xray_f''',
     1                       ffra(i,KPhase,KDatB),klam(KDatB),ich)
                call RealFromAtomFile(AtType(i,KPhase),'Xray_f"',
     1                       ffia(i,KPhase,KDatB),klam(KDatB),ich)
              else
                call EM50ReadAnom(AtTypeFull(i,KPhase),
     1            ffra(i,KPhase,KDatB),ffia(i,KPhase,KDatB),KDatB)
              endif
            endif
          else if(Radiation(KDatB).eq.NeutronRadiation) then
            ffra(i,KPhase,KDatB)=0.
            ffia(i,KPhase,KDatB)=0.
          endif
        enddo
        j=LocateInStringArray(atn,98,AtType(i,KPhase),IgnoreCaseYes)
        if(j.gt.0) then
          AtNum(i,KPhase)=float(j)
        else
          AtNum(i,KPhase)=0.
        endif
2400    if(.not.ChargeDensities) go to 5000
        if(EqIgCase(CoreValSource(i,KPhase),'FF-table')) then
          go to 5000
        else if(EqIgCase(AtType(i,KPhase),'H').and.
     1          PopVal(1,i,KPhase).lt.0) then
          if(PopVal(1,i,KPhase).eq.-1) then
            call RealAFromAtomFile(AtType(i,KPhase),VetaBasic,ff,62,ich)
            call TableFFToEquidistant(ff,FFBasic(1,i,KPhase),
     1                                AtType(i,KPhase))
          else if(PopVal(1,i,KPhase).eq.-2) then
            s=0.
            pomk=HZSlater(i,KPhase)/.52918
            do j=1,FFType(KPhase)
              FFBasic(j,i,KPhase)=
     1          SlaterFB(HNSlater(i,KPhase),0,s,pomk,pom,pom,ich)/
     2          pi4
              s=s+.05
            enddo
          endif
          call CopyVek(FFBasic(1,i,KPhase),FFVal(1,i,KPhase),62)
          go to 5000
        else
          ExistWaveF=.true.
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          popp=0.
          call ReadWaveF(AtType(i,KPhase),i,OrbitName(1),popp,FF,121,0,
     1                   CoreValSource(i,KPhase),ich)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ich.ne.0) then
            ExistWaveF=.false.
            go to 5000
          endif
          call SetRealArrayTo(FFCore(1,i,KPhase),FFType(KPhase),0.)
          call SetRealArrayTo(FFVal (1,i,KPhase),FFType(KPhase),0.)
          kp=0
          fnormv=0.
          do l=1,7
            kp=kp+l
            k=kp
            spopc=0.
            spopv=0.
            do j=l,7
              jp=j-l+1
              popc(jp)=PopCore(k,i,KPhase)
              spopc=spopc+popc(jp)
              popv(jp)=PopVal(k,i,KPhase)
              spopv=spopv+popv(jp)
              fnormv=fnormv+popv(jp)
              k=k+j
            enddo
            if(spopc.gt.0..and.ExistWaveF) then
              call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),popc,
     1                       FFCore(1,i,KPhase),FFType(KPhase),0,
     2                       CoreValSource(i,KPhase),ich)
              if(ich.ne.0) then
                ExistWaveF=.false.
                go to 5000
              endif
            endif
            if(spopv.ne.0.) then
              ich=0
              if(ExistWaveF) then
                call ReadWaveF(AtType(i,KPhase),i,OrbitName(l),popv,
     1                         FFVal(1,i,KPhase),FFType(KPhase),0,
     2                         CoreValSource(i,KPhase),ich)
                if(ich.ne.0) then
                  call FeChybne(-1.,-1.,'STA wave function for "'//
     1                     AtType(i,KPhase)(:idel(AtType(i,KPhase)))//
     2                     '" is not on the database file "'//
     3                     NameOfWaveFile(:idel(NameOfWaveFile))//'".',
     4                     'Use another database or define own core '//
     5                     'and valence form factors',Warning)
                  go to 5000
                endif
              endif
            endif
          enddo
        endif
        do j=1,FFType(KPhase)
          FFBasic(j,i,KPhase)=FFCore(j,i,KPhase)+FFVal(j,i,KPhase)
        enddo
        if(fnormv.gt.0.) then
          fnormv=1./fnormv
          do j=1,FFType(KPhase)
            FFVal(j,i,KPhase)=FFVal(j,i,KPhase)*fnormv
          enddo
        endif
5000  continue
      return
      end
