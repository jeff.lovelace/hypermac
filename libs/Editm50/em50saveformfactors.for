      subroutine EM50SaveFormFactors
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension AtRadiusIn(:),HZSlaterIn(:),NSlaterIn(:,:),AtMultIn(:),
     1          ZSlaterIn(:,:),FFnIn(:),FFniIn(:),FFraIn(:,:),
     2          FFiaIn(:,:),FFBasicIn(:,:),FFCoreIn(:,:),FFValIn(:,:),
     3          FFMagIn(:,:),FFElIn(:,:)
      character*256, allocatable :: CoreValSourceIn(:)
      character*7  AtTypeFullIn(:),AtTypeMagIn(:),AtTypeMagJIn(:),
     1             AtTypeMenuIn(:),AtP,AtPIn
      character*2  AtTypeIn(:)
      integer HNSlaterIn(:),TypeCoreIn(:),TypeValIn(:),PopCoreIn(:,:),
     1        PopValIn(:,:),AtColorIn(:),UzHoMa(:),TypeFFMagIn(:)
      logical EqIgCase
      allocatable AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     1            AtRadiusIn,AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,
     2            FFValIn,FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,
     3            TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,ZSlaterIn,
     4            NSlaterIn,HNSlaterIn,HZSlaterIn,FFElIn,UzHoMa,
     5            AtTypeMenuIn,TypeFFMagIn
      save nf,ntab,ntaba,AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     1     AtRadiusIn,AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,
     2     FFValIn,FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,FFElIn,
     3     TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,ZSlaterIn,
     4     NSlaterIn,HNSlaterIn,HZSlaterIn,AtTypeMenuIn,TypeFFMagIn
      nf=NAtFormula(KPhase)
      if(allocated(AtTypeIn))
     1  deallocate(AtTypeIn,AtTypeFullIn,AtTypeMagIn,AtTypeMagJIn,
     2             AtRadiusIn,AtTypeMenuIn,
     3             AtColorIn,AtMultIn,FFBasicIn,FFCoreIn,FFValIn,
     4             FFraIn,FFiaIn,FFnIn,FFniIn,FFMagIn,TypeFFMagIn,
     5             TypeCoreIn,TypeValIn,PopCoreIn,PopValIn,
     6             ZSlaterIn,NSlaterIn,HNSlaterIn,HZSlaterIn,
     7             FFElIn,CoreValSourceIn)
      m=121
      allocate(AtTypeIn(nf),AtTypeFullIn(nf),AtTypeMagIn(nf),
     1         AtTypeMagJIn(nf),AtTypeMenuIn(nf),
     2         AtRadiusIn(nf),AtColorIn(nf),AtMultIn(nf),
     3         FFBasicIn(m,nf),FFCoreIn(m,nf),FFValIn(m,nf),
     4         FFraIn(nf,NDatBlock),FFiaIn(nf,NDatBlock),
     5         FFnIn(nf),FFniIn(nf),FFMagIn(7,nf),TypeFFMagIn(nf),
     6         TypeCoreIn(nf),TypeValIn(nf),PopCoreIn(28,nf),
     7         PopValIn(28,nf),ZSlaterIn(8,nf),NSlaterIn(8,nf),
     8         HNSlaterIn(nf),HZSlaterIn(nf),FFElIn(m,nf),
     9         CoreValSourceIn(nf))
      ntab=FFType(KPhase)
      ntaba=abs(FFType(KPhase))
      if(.not.allocated(AtTypeMenu)) then
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      do i=1,nf
        AtTypeIn(i)=AtType(i,KPhase)
        AtTypeMenuIn(i)=AtTypeMenu(i)
        AtTypeFullIn(i)=AtTypeFull(i,KPhase)
        AtTypeMagIn(i)=AtTypeMag(i,KPhase)
        AtTypeMagJIn(i)=AtTypeMagJ(i,KPhase)
        AtRadiusIn(i)=AtRadius(i,KPhase)
        AtColorIn(i)=AtColor(i,KPhase)
        AtMultIn(i)=AtMult(i,KPhase)
        call CopyVek(FFBasic(1,i,KPhase),FFBasicIn(1,i),ntaba)
        call CopyVek(FFEl(1,i,KPhase),FFElIn(1,i),62)
        do j=1,NDatBlock
          FFraIn(i,j)=FFra(i,KPhase,j)
          FFiaIn(i,j)=FFia(i,KPhase,j)
        enddo
        if(ChargeDensities) then
          call CopyVek(FFCore(1,i,KPhase),FFCoreIn(1,i),ntaba)
          call CopyVek(FFVal (1,i,KPhase),FFValIn (1,i),ntaba)
          TypeCoreIn(i)=TypeCore(i,KPhase)
          TypeValIn (i)=TypeVal (i,KPhase)
          call CopyVekI(PopCore(1,i,KPhase),PopCoreIn(1,i),28)
          call CopyVekI(PopVal(1,i,KPhase),PopValIn(1,i),28)
          call CopyVek (ZSlater(1,i,KPhase),ZSlaterIn(1,i),8)
          call CopyVekI(NSlater(1,i,KPhase),NSlaterIn(1,i),8)
          HNSlaterIn(i)=HNSlater(i,KPhase)
          HZSlaterIn(i)=HZSlater(i,KPhase)
          CoreValSourceIn(i)=CoreValSource(i,KPhase)
        endif
        FFnIn (i)=FFn (i,KPhase)
        FFniIn(i)=FFni(i,KPhase)
        if(AtTypeMagIn(i).ne.' ') then
          call CopyVek(FFMag(1,i,KPhase),FFMagIn(1,i),7)
          TypeFFMagIn(i)=TypeFFMag(i,KPhase)
        endif
      enddo
      go to 9999
      entry EM50SmartUpdateFormFactors
      allocate(UzHoMa(nf))
      call SetIntArrayTo(UzHoMa,nf,0)
      do 5000n=1,NAtFormula(KPhase)
        FFBasic(1,n,KPhase)=-3333.
        do i=1,nf
          AtP=AtTypeMenu(n)
          AtPIn=AtTypeMenuIn(i)
          j=index(AtTypeMenuIn(i),'#')
          k=index(AtTypeMenu(n),'#')
          if(j.le.0) then
            if(k.gt.0) AtP=AtTypeMenu(n)(:k-1)
          else
            if(k.le.0) AtPIn=AtTypeMenuIn(i)(:j-1)
          endif
          if(EqIgCase(AtPIn,AtP).and.UzHoMa(i).le.0) then
            AtTypeFull(n,KPhase)=AtTypeFullIn(i)
            AtTypeMenu(n)=AtTypeMenuIn(i)
            AtTypeMag(n,KPhase)=AtTypeMagIn(i)
            AtTypeMagJ(n,KPhase)=AtTypeMagJIn(i)
            AtRadius(n,KPhase)=AtRadiusIn(i)
            AtColor(n,KPhase)=AtColorIn(i)
            AtMult(n,KPhase)=AtMultIn(i)
            if(ntab.eq.FFType(KPhase))
     1        call CopyVek(FFBasicIn(1,i),FFBasic(1,n,KPhase),ntaba)
            call CopyVek(FFElIn(1,i),FFEl(1,n,KPhase),62)
            do j=1,NDatBlock
              FFra(n,KPhase,j)=FFraIn(i,j)
              FFia(n,KPhase,j)=FFiaIn(i,j)
            enddo
            if(ChargeDensities) then
              if(ntaba.eq.FFType(KPhase)) then
                call CopyVek(FFCoreIn(1,i),FFCore(1,n,KPhase),ntaba)
                call CopyVek(FFValIn (1,i),FFVal (1,n,KPhase),ntaba)
              endif
              TypeCore(n,KPhase)=TypeCoreIn(i)
              TypeVal (n,KPhase)=TypeValIn (i)
              call CopyVekI(PopCoreIn(1,i),PopCore(1,n,KPhase),28)
              call CopyVekI(PopValIn(1,i),PopVal(1,n,KPhase),28)
              call CopyVek (ZSlaterIn(1,i),ZSlater(1,n,KPhase),8)
              call CopyVekI(NSlaterIn(1,i),NSlater(1,n,KPhase),8)
              HNSlater(i,KPhase)=HNSlaterIn(i)
              HZSlater(i,KPhase)=HZSlaterIn(i)
              CoreValSource(i,KPhase)=CoreValSourceIn(i)
            endif
            FFn (n,KPhase)=FFnIn (i)
            FFni(n,KPhase)=FFniIn(i)
            if(AtTypeMagIn(i).ne.' ') then
              call CopyVek(FFMagIn(1,i),FFMag(1,n,KPhase),7)
              TypeFFMag(i,KPhase)=TypeFFMagIn(i)
            endif
            UzHoMa(i)=1
            go to 5000
          endif
        enddo
        AtRadius(n,KPhase)=-3333.
5000  continue
      deallocate(UzHoMa)
9999  return
      end
