      subroutine EM50ChngOrder(ie,ieps,tau,ipor,i1,i2)
      include 'fepc.cmn'
      dimension ie(4),ieps(4),tau(4),ipor(4)
      ipor(i1)=i2
      ipor(i2)=i1
      i=ieps(i1)
      ieps(i1)=ieps(i2)
      ieps(i2)=i
      i=ie(i1)
      ie(i1)=ie(i2)
      ie(i2)=i
      p=tau(i1)
      tau(i1)=tau(i2)
      tau(i2)=p
      return
      end
