      subroutine EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension ieps(5),tau(5),ie(5),rp(36),trm(36),ipor(5),xref(3),
     1          QuOld(3,3),QuiOld(3,3),QuSymm(3,3),MonoclinicA(3)
      character*256 Veta,StSymm(3)
      character*80 t80
      character*60 itxt,itxti,GrupaPom,GrupaHledej,GrupaRidka
      character*11 iqs
      character*8 GrupaI
      character*6 smbtau,SmbMagCentr
      character*5 smbq(3)
      character*2 nty
      logical StdSmb,SearchForPrimitive,FirstTime,AskForDelta,
     1        EqIgCase,EqRV,ItIsFd3c,EqRV0,Choice1,CrwLogicQuest,lpom,
     2        WizardModeIn,UplnySymbol
      integer EM50IrrType,CrlCentroSymm,UseTabsIn
      data iqs/'PABCLMNUVWR'/,smbtau/'01stqh'/,
     1     smbq/'alfa','beta','gamma'/
      call Zhusti(Grupa(KPhase))
      id=idel(Grupa(KPhase))
      i1=index(Grupa(KPhase),'(')
      UplnySymbol=i1.gt.0
      if(NDimI(KPhase).gt.0.and.UplnySymbol) then
        call CopyVek(Qu(1,1,1,KPhase),QuOld,3*NDimI(KPhase))
        call CopyVek(Qui(1,1,KPhase),QuiOld,3*NDimI(KPhase))
        i1=index(Grupa(KPhase),'(')
        if(i1.gt.0.and.i1.lt.id) then
          i2=index(Grupa(KPhase)(i1+1:),')')+i1
        else
          i2=0
        endif
        i3=idel(Grupa(KPhase))
        if(i1.le.0.or.i2.le.i1) go to 8150
        GrupaPom=Grupa(KPhase)(1:i1-1)
        t80=Grupa(KPhase)(i1+1:i2-1)
        call mala(t80)
        do i=1,3
          if(idel(t80).le.0) go to 8160
          k=1
1010      if(t80(k:k).eq.smbq(i)(k:k)) then
            k=k+1
            go to 1010
          endif
          k=k-1
          if(k.eq.0) then
            j=index(t80,'/')
            if(j.le.0.or.j.gt.3.or.(j.eq.3.and.t80(1:1).ne.'-')) then
              k=1
            else
              k=j+1
            endif
            quir(i,1,KPhase)=fract(t80(1:k),ich)
            qui(i,1,KPhase)=0.
            if(ich.ne.0) go to 8210
          else
            quir(i,1,KPhase)=0.
            qui(i,1,KPhase)=sqrt(float(i)*.1)
          endif
          t80=t80(k+1:)
        enddo
        n=0
        ic=0
        in=0
        do i=1,3
          if(abs(quir(i,1,KPhase)).lt..0001) then
            n=n+1
            if(in.eq.0) in=i
          else if(ic.eq.0) then
            ic=i
          endif
        enddo
        nq=0
        if(n.eq.3) then
          nq=1
        else if(n.eq.2) then
          if(abs(quir(ic,1,KPhase)-.5).lt..0001) then
            nq=ic+1
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001) then
            nq=ic+4
          endif
        else if(n.eq.1) then
          if(abs(quir(1,1,KPhase)-.333333).lt..0001.and.
     1       abs(quir(2,1,KPhase)-.333333).lt..0001) then
            nq=11
          else if(abs(quir(ic,1,KPhase)-.5).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-.5).lt..0001) then
            nq=7+in
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-1.).lt..0001) then
            nq=11+in
          endif
        endif
        if(nq.le.0) go to 8160
        call AddVek(Qui(1,1,KPhase),Quir(1,1,KPhase),Qu(1,1,1,KPhase),3)
        iqv=EM50IrrType(qui(1,1,KPhase))
      else
        do i=1,NDimI(KPhase)
          do j=1,3
            qui(j,i,KPhase)=qu(j,i,1,KPhase)
            quir(j,i,KPhase)=0.
          enddo
        enddo
        GrupaPom=Grupa(KPhase)
        iqv=0
      endif
1100  SmbMagCentr=' '
      if(GrupaPom(1:1).eq.'-') then
        call Uprat(GrupaPom(2:))
      else
        if(GrupaPom(2:2).eq.'[') then
          i=index(GrupaPom,']')
          if(i.gt.0) then
            SmbMagCentr=GrupaPom(2:i)
            GrupaPom=GrupaPom(1:1)//GrupaPom(i+1:)
            TauMagCenter=0.
          endif
        endif
        call Uprat(GrupaPom)
      endif
      i=index(GrupaPom,'.')
      idp=idel(GrupaPom)
      MagInv=0
      if(i.gt.0) then
        if(GrupaPom(i+1:i+2).eq.'1''') then
          i1p=index(GrupaPom,'[')
          i2p=index(GrupaPom,']')
          SmbMagCentr=' '
          if((i1p.eq.0.and.i2p.gt.0).or.(i1p.gt.0.and.i2p-i1p.ne.2))
     1      then
            go to 8190
          else if(i1p.gt.0) then
            SmbMagCentr=GrupaPom(i1p:i2p)
            i3=idel(Grupa(KPhase))
            if(EqIgCase(Grupa(KPhase)(i3:i3),'s')) then
              TauMagCenter=.5
            else if(EqIgCase(Grupa(KPhase)(i3:i3),'0')) then
              TauMagCenter=0.
            else
              go to 8105
            endif
            i3=idl-1
          else
            MagInv=1
          endif
          GrupaPom(i:)=' '
        else
          go to 8190
        endif
      else if(idp-2.gt.0) then
        if(NDimI(KPhase).eq.0) then
          MagInv=0
        else
          if(GrupaPom(idp-2:idp).eq. '.1'''.and.
     1       GrupaPom(idp-3:idp).ne.'.-1''') then
            GrupaPom(idp-1:)=' '
            MagInv=1
          else
            MagInv=0
          endif
        endif
      endif
      i=index(GrupaPom,';')
      j=index(GrupaPom(2:),'x')
      k=index(GrupaPom,'y')
      l=index(GrupaPom,'z')
      if(i.le.0.and.j.le.0.and.k.le.0.and.l.le.0.and.
     1   grupa(KPhase)(1:1).ne.'-') then
        StdSmb=.true.
        Lattice(KPhase)=GrupaPom(1:1)
        if(Lattice(KPhase).eq.'X') GrupaPom(1:1)='P'
        SearchForPrimitive=.false.
        j=0
        GrupaHledej=' '
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).ne.'''') then
            j=j+1
            GrupaHledej(j:j)=GrupaPom(i:i)
          endif
        enddo
        if(j.ne.idel(GrupaPom).and.MagneticType(KPhase).le.0) go to 8180
1200    imd=0
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1          'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 8700
        read(ln,FormA256) Veta
        if(Veta(1:1).ne.'#') rewind ln
        if(FirstTime) NInfo=0
2000    read(ln,FormA,err=8610,end=2010) Veta
        read(Veta,FormSG,err=8610) igi,ipgi,idli,GrupaI,itxti,
     1                             GrupaRidka,GrupaRidka,GrupaRidka
        if(imd.ne.0) imd=imd+1
        if((GrupaI.eq.'P2'.and.imd.eq.0).or.
     1      GrupaI.eq.'Pmm2') imd=1
        if(GrupaHledej.eq.GrupaI.or.
     1     (Lattice(KPhase).eq.'X'.and.GrupaHledej(2:).eq.GrupaI(2:)))
     2    then
          if(ipgi.ge.3.and.ipgi.le.5) then
            if(FirstTime.and.NInfo.lt.3) then
              NInfo=NInfo+1
              StSymm(NInfo)=Veta
              Veta=GrupaRidka
              if(EqIgCase(Lattice(KPhase),'X')) Veta(1:1)='X'
              write(TextInfo(NInfo),'(a,i3,2a)') Tabulator,ipgi,
     1                                 Tabulator,Veta(:idel(Veta))
              MonoclinicA(NInfo)=mod(imd-1,3)+1
            endif
            t80=GrupaRidka
            k=0
            call kus(t80,k,Cislo)
            GrupaRidka=Cislo
            do i=1,4
              call kus(t80,k,Cislo)
              if(Cislo.ne.'1')
     1          GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     2                     Cislo(:idel(Cislo))
            enddo
            if(.not.FirstTime.and.mod(imd-1,3)+1.eq.Monoclinic(KPhase))
     1        then
              go to 2100
            else
              go to 2000
            endif
          endif
          go to 2100
        endif
        go to 2000
2010    call CloseIfOpened(ln)
        if(NInfo.gt.1.and.FirstTime) then
          UseTabsIn=UseTabs
          UseTabs=NextTabs()
          WizardModeIn=WizardMode
          WizardMode=.false.
          call FeTabsAdd( 50.,UseTabs,IdRightTab,' ')
          call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
          id=NextQuestId()
          xdq=300.
          il=Ninfo+1
          Veta='Select relevant space group'
          call FeQuestCreate(id,-1.,-1.,xdq,il,Veta,0,LightGray,-1,0)
          il=1
          tpom=30.
          call FeQuestLblMake(id,tpom,il,'Space group number','L','N')
          tpom=150.
          call FeQuestLblMake(id,tpom,il,'Space group symbol','L','N')
          xpom=5.
          tpom=xpom+10.+CrwgXd
          n=0
          do i=1,NInfo
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                          TextInfo(i),'L',CrwgXd,CrwgYd,0,1)
            if(MonoclinicA(i).eq.Monoclinic(KPhase)) then
              lpom=.true.
              n=i
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
            if(i.eq.1) nCrwFirst=CrwLastMade
          enddo
          if(n.eq.0) call FeQuestCrwOn(nCrwFirst)
2050      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2050
          endif
          nCrw=nCrwFirst
          do i=1,NInfo
            if(CrwLogicQuest(nCrw)) then
              read(StSymm(i),FormSG,err=8610)
     1          igi,ipgi,idli,GrupaI,itxti,GrupaRidka,GrupaRidka,
     2          GrupaRidka
              t80=GrupaRidka
              k=0
              call kus(t80,k,Cislo)
              GrupaRidka=Cislo
              do j=1,3
                call kus(t80,k,Cislo)
                if(Cislo.ne.'1') then
                  GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     1                       Cislo(:idel(Cislo))
                  Monoclinic(KPhase)=j
                  exit
                endif
              enddo
            endif
            nCrw=nCrw+1
          enddo
          call FeQuestRemove(id)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsIn
          WizardMode=WizardModeIn
          go to 2100
        endif
        if(SearchForPrimitive.or.GrupaHledej(1:1).eq.'P') then
          go to 8170
        else
          SearchForPrimitive=.true.
          GrupaHledej(1:1)='P'
          go to 1200
        endif
2100    call CloseIfOpened(ln)
        k=index(itxti,'#')
        if(k.gt.0) then
          itxt=itxti(1:k-1)
          Veta=itxti(k+1:idel(itxti))
          do j=1,idel(Veta)
            if(Veta(j:j).eq.',') Veta(j:j)=' '
          enddo
          k=0
          call StToReal(Veta,k,ShiftSg,3,.false.,ich)
          call RealVectorToOpposite(ShiftSg,ShiftSg,3)
          call SetRealArrayTo(ShiftSg(4),NDimI(KPhase),0.)
        else
          itxt=itxti
          call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
        endif
        ipg=ipgi
!        if(Lattice(KPhase).eq.'X'.or.SearchForPrimitive) then
!          ngrupa(KPhase)=0
!        else
          ngrupa(KPhase)=igi
!        endif
        idl=idli
        if(MagInv.gt.0) idl=idl+1
        Poradi=.true.
        if(ipg.le.2) then
          CrSystem(KPhase)=CrSystemTriclinic
        else if(ipg.ge.3.and.ipg.le.5) then
          imd=mod(imd-1,3)+1
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.
     1       Monoclinic(KPhase).ne.iabs(iqv)) go to 8210
          CrSystem(KPhase)=CrSystemMonoclinic+Monoclinic(KPhase)*10
        else if(ipg.ge.6.and.ipg.le.8) then
          Poradi=.false.
          imd=mod(imd+1,3)+1
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.lt.1)
     1      go to 8210
          CrSystem(KPhase)=CrSystemOrthorhombic
        else if(ipg.ge.9.and.ipg.le.15) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.ne.3) go to 8210
          CrSystem(KPhase)=CrSystemTetragonal
        else if(ipg.ge.16.and.ipg.le.27) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.ne.3) go to 8210
          if(ipg.le.20) then
            CrSystem(KPhase)=CrSystemTrigonal
          else
            CrSystem(KPhase)=CrSystemHexagonal
          endif
        else if(ipg.ge.28.and.ipg.le.32) then
          CrSystem(KPhase)=CrSystemCubic
        endif
        if(mod(CrSystem(KPhase),10).ne.2) Monoclinic(KPhase)=0
        GrupaPom(1:1)=Lattice(KPhase)
        GrupaHledej(1:1)=Lattice(KPhase)
        if(itxt(1:1).eq.'-') then
          itxt(2:2)=Lattice(KPhase)
        else
          itxt(1:1)=Lattice(KPhase)
        endif
      else
        StdSmb=.false.
        itxt=GrupaPom
        idl=1
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).eq.';') idl=idl+1
        enddo
        call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
      endif
      if(NDimI(KPhase).eq.1.and.UplnySymbol) then
        Veta=Grupa(KPhase)(i2+1:i3)
        call mala(Veta)
        i=0
        n=0
        id=idel(Veta)
        Znak=1.
2200    i=i+1
        if(i.gt.id) go to 2300
        if(Veta(i:i).eq.'-') then
          Znak=-1.
          go to 2200
          i=i+1
          if(Veta(i:i).eq.'1') then
            n=n+1
            ieps(n)=-1
            tau(n)=0
            go to 2200
          else
            go to 8100
          endif
        else
          j=index(smbtau,Veta(i:i))
          if(j.le.0) go to 8100
          if(j.eq.1) then
            n=n+1
            ieps(n)=0
            tau(n)=0.
            go to 2200
          else if(j.eq.2) then
            go to 8100
          endif
          if(j.ne.6) j=j-1
          n=n+1
          ieps(n)=1
          if(j.ne.1) then
            if(Znak.gt.0.) then
              tau(n)= 1./float(j)
            else
              tau(n)=-1./float(j)
            endif
          else
            tau(n)=0.
          endif
          Znak=1.
          go to 2200
        endif
2300    if(n.ne.idl) then
          if(n.gt.idl) then
            Veta='"'//Veta(1:id)//'" too long.'
          else
            do i=n+1,idl
              ieps(i)=0
              tau(i)=0.
            enddo
            go to 2400
          endif
          id=idel(Veta)
          go to 8220
        endif
2400    if(StdSmb) then
          if(MagInv.gt.0) idl=idl-1
          call EM50SetEps(ie,ipor,ieps,tau,idl,ipg,ngrupa(KPhase),imd,
     1                    itxt,ich)
          if(ich.ne.0) go to 8162
          if(MagInv.gt.0) then
            idl=idl+1
            ie(idl)=1
          endif
          do i=1,idl
            if(ie(i).ne.ieps(i)) then
              if(ieps(i).eq.0) ieps(i)=ie(i)
            endif
          enddo
        endif
      endif
      call EM50GetMagFlags(GrupaPom,GrupaRidka,MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8190
      call EM50OrderMagFlags(ipg,NGrupa(KPhase),MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8200
      call EM50GenCentr(itxt,nc,FirstTime,*8110,*9900)
      call EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,UplnySymbol,ich)
      if(ich.ne.0) go to 9900
      do i=1,NLattVec(KPhase)
        do 2500j=1,NSymm(KPhase)
          call MultM(rm6(1,j,1,KPhase),vt6(1,i,1,KPhase),rp,
     1               NDim(KPhase),NDim(KPhase),1)
          call od0do1(rp,rp,NDim(KPhase))
          do k=1,NLattVec(KPhase)
            if(EqRV(vt6(1,k,1,KPhase),rp,NDim(KPhase),.001)) go to 2500
          enddo
          go to 8230
2500    continue
      enddo
      call EM50OriginShift(ShiftSg,0)
      i=CrlCentroSymm()
      call SetRealArrayTo(xref,3,0.)
      if(EqIgCase(Grupa(KPhase),'Fddd')) then
        call SetRealArrayTo(xref,3,.25)
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          do i=1,NSymm(KPhase)
            ipul=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.5).lt..001) ipul=ipul+1
            enddo
            if(ipul.eq.2) call SetRealArrayTo(s6(1,i,1,KPhase),3,0.)
          enddo
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          do 5020i=1,NSymm(KPhase)
            call MatInv(rm(1,i,1,KPhase),trm,Det,3)
            if(Det.gt.0.) then
              pom=.75
            else
              pom=.25
            endif
            do j=1,NLattVec(KPhase)
              call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
              call od0do1(rp,rp,3)
              nnul=0
              npom=0
              do k=1,3
                if(abs(rp(k)).lt..001) then
                  nnul=nnul+1
                else if(abs(rp(k)-pom).lt..001) then
                  npom=npom+1
                endif
              enddo
              if(nnul+npom.eq.3.and.(npom.eq.0.or.npom.eq.2)) then
                call CopyVek(rp,s6(1,i,1,KPhase),3)
                go to 5020
              endif
            enddo
5020      continue
        endif
        go to 5300

      else if(EqIgCase(Grupa(KPhase),'I41md').or.
     1        EqIgCase(Grupa(KPhase),'I41cd')) then
        k=1
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs( s6(1,i,1,KPhase)).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs( s6(1,i,1,KPhase)).lt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'I-42d')) then
        do 5040i=1,NSymm(KPhase)
          if(abs(s6(2,i,1,KPhase)).lt..001) go to 5040
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5040    continue
      else if(EqIgCase(Grupa(KPhase),'I41/a').or.
     1        EqIgCase(Grupa(KPhase),'I41/amd').or.
     2        EqIgCase(Grupa(KPhase),'I41/acd')) then
        xref(2)=.5
        xref(3)=.25
        k=2
        l=NDim(KPhase)+2
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          pomp1=.5
          pomm1=0.
          pomp2=0.
          pomm2=.5
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          if(EqIgCase(Grupa(KPhase),'I41/a')) then
            pomp1=.25
            pomm1=.75
          else
            pomp1=.75
            pomm1=.25
          endif
          pomp2=0.
          pomm2=0.
        else
          go to 5300
        endif
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp1).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm1).gt..001).or.
     4       (abs(RM6(l,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp2).gt..001).or.
     2       (abs(RM6(l,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm2).gt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3')) then
        call SetRealArrayTo(xref,3,.25)
        Choice1=EqRV(s6(1,i,1,KPhase),xref,3,.001)
        do 5050i=1,NSymm(KPhase)
          call MatInv(rm(1,i,1,KPhase),trm,Det,3)
          if(Det.gt.0.) then
            if(Choice1) then
              pom=0.
              nsum=3
            else
              pom=.25
              nsum=2
            endif
          else
            if(Choice1) then
              pom=.25
              nsum=3
            else
              pom=.75
              nsum=2
            endif
          endif
          do j=1,NLattVec(KPhase)
            call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
            call od0do1(rp,rp,3)
            npom=0
            do k=1,3
              if(abs(rp(k)-pom).lt..001) npom=npom+1
            enddo
            if(npom.eq.nsum) then
              call CopyVek(rp,s6(1,i,1,KPhase),3)
              go to 5050
            endif
          enddo
5050    continue
      else if(EqIgCase(Grupa(KPhase),'I-43d')) then
        do i=1,NSymm(KPhase)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.1.and.inul.eq.2).or.ipul.eq.3.or.
     1       (i34 .eq.1.and.i14 .eq.2).or.i34.eq.3) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3m').or.
     1        EqIgCase(Grupa(KPhase),'Fd-3c')) then
        Choice1=.not.EqRV(s6(1,i,1,KPhase),xref,3,.001)
        if(Choice1) then
          ItIsFd3c=EqIgCase(Grupa(KPhase),'Fd-3c')
          do i=1,NSymm(KPhase)
            if(ItIsFd3c) then
              call MatInv(RM(1,i,1,KPhase),rp,det,3)
            else
              det=1.
            endif
            i34=0
            i14=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
              if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
            enddo
            if(i34.gt.0.or.i14.gt.0) then
              Znak=-1.
            else
              Znak= 1.
            endif
            KteryZnak=0
            KdeZnak=0
            KdeSoused=0
            do j=1,3
              do k=1,3
                l=(k-1)*3+j
                if(abs(RM(l,i,1,KPhase)-Znak).lt..001) then
                  if(KdeZnak.le.0) then
                    KdeZnak=j
                    KteryZnak=k
                  else
                    KdeZnak=-1
                    go to 5090
                  endif
                endif
              enddo
            enddo
            k=mod(KteryZnak+1,3)+1
            k=(k-1)*3+1
            do j=1,3
              if(abs(RM(k,i,1,KPhase)).gt..001) then
                KdeSoused=j
                go to 5090
              endif
              k=k+1
            enddo
5090        if(Znak.lt.0.) then
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.75
              else
                pom=.25
              endif
            else
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.5
              else
                pom=0.
              endif
            endif
            call SetRealArrayTo(s6(1,i,1,KPhase),3,pom)
            if(KdeZnak.gt.0) then
              s6(KdeZnak,i,1,KPhase)=s6(KdeZnak,i,1,KPhase)+.5
              s6(KdeSoused,i,1,KPhase)=s6(KdeSoused,i,1,KPhase)+.5
            endif
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          enddo
        endif
      else if(EqIgCase(Grupa(KPhase),'Ia-3d')) then
        do 5120i=1,NSymm(KPhase)
          call MatInv(RM(1,i,1,KPhase),rp,det,3)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.2.and.inul.eq.1).or.inul.eq.3) go to 5120
          if(det.gt.0.) then
            if((i34.eq.1.and.i14.eq.2).or.i34.eq.3) go to 5120
          else
            if((i34.eq.2.and.i14.eq.1).or.i34.eq.0) go to 5120
          endif
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5120    continue
      endif
5300  call EM50OriginShift(ShSg(1,KPhase),0)
      do i=1,NSymm(KPhase)
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        do j=1,NDim(KPhase)
          if(index(symmc(j,i,1,KPhase),'?').gt.0) then
            NSymm(KPhase)=0
            go to 8140
          endif
        enddo
      enddo
      if(idel(SmbMagCentr).eq.3.and.SmbMagCentr(1:1).eq.'['.and.
     1                              SmbMagCentr(3:3).eq.']' ) then
        if(SmbMagCentr(2:2).eq.'I') then
          rp(1:3)=.5
          rp(4)=0.
          if(CrSystem(KPhase).eq.CrSystemTrigonal) then
            rp(1)=0.
            rp(2)=0.
          endif
          go to 5350
        else if(SmbMagCentr(2:2).eq.'c') then
          rp=0.
          rp(3)=.5
          go to 5350
        else
          i=ichar(SmbMagCentr(2:2))-ichar('a')+1
          rp=0.
          if(i.ge.1.and.i.le.3) then
            rp(i)=.5
            go to 5350
          endif
          i=ichar(SmbMagCentr(2:2))-ichar('A')+1
          rp=.5
          if(i.ge.1.and.i.le.3) then
            rp(i)=0.
            go to 5350
          endif
        endif
        go to 5400
5350    nss=NSymm(KPhase)
        call ReallocSymm(NDim(KPhase),2*nss,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,NSymm(KPhase)
          nss=nss+1
          call CopyVek(rm6(1,i,1,KPhase),rm6(1,nss,1,KPhase),
     1                 NDimQ(KPhase))
          call CopyVek(rm (1,i,1,KPhase),rm (1,nss,1,KPhase),9)
          call AddVek(s6(1,i,1,KPhase),rp,s6(1,nss,1,KPhase),
     1                 NDim(KPhase))
          if(NDimI(KPhase).gt.0)
     1      s6(4,nss,1,KPhase)=s6(4,nss,1,KPhase)+TauMagCenter
          call NormCentr(s6(1,nss,1,KPhase))
          call CodeSymm(rm6(1,nss,1,KPhase),s6(1,nss,1,KPhase),
     1                  symmc(1,nss,1,KPhase),0)
          ZMag(nss,1,KPhase)=-Zmag(i,1,KPhase)
        enddo
        NSymm(KPhase)=nss
      endif
5400  ich=0
      call GetQiQr(qu(1,1,1,KPhase),qui(1,1,KPhase),
     1             quir(1,1,KPhase),NDimI(KPhase),1)
      go to 9999
8100  write(t80,'(''Incorrect additional symbol of the '',i1,a2,
     1            '' generator.'')') n+1,nty(n+1)
      go to 8500
8105  write(t80,'(''Incorrect additional symbol for the time inverion'',
     1            '' operator.'')')
      go to 8500
8110  t80='The cell centring symbol is not correct.'
      go to 8500
8120  write(t80,'(''Opposite sign of additional symbol of the '',i1,a2,
     1            '' generator.'')') ipor(i),nty(ipor(i))
      go to 8500
8130  t80='The additional symbols are not consistent.'
      go to 8500
8140  t80='Origin shift is not acceptable.'
      go to 8500
8150  t80='Neither colon nor parenthesis section found.'
      go to 8500
8160  t80='The symbol for modulation vector is incorrrect.'
      go to 8500
8162  t80='The symbol for modulation vector isn''t consitent with the'//
     1    ' point group .'
      go to 8500
8170  t80='The symbol "'//Grupa(KPhase)(:idel(Grupa(KPhase)))//
     1    '" wasn''t found on the list.'
      go to 8500
8180  t80='The symbol contains time inversion flags for non-magnetic '//
     1    'structure.'
      go to 8500
8190  t80='During decoding time inversion flags.'
      go to 8500
8200  t80='The time inversion flags are inacceptable.'
      go to 8500
8210  t80='The modulation vector is inconsistent with the superspace '//
     1    'symbol.'
      go to 8500
8220  t80='The symbol for addition translation part '//Veta(:idel(Veta))
      go to 8500
8230  t80='The cell centring is not acceptable for the point group.'
      go to 8500
8500  if(NDimI(KPhase).eq.1) then
        Veta='incorrect superspace group symbol.'
      else
        Veta='incorrect space group symbol.'
      endif
      go to 9000
8610  call FeReadError(ln)
      call CloseIfOpened(ln)
8700  call DeletePomFiles
      call FeTmpFilesDelete
      call FeGrQuit
      stop
9000  call FeChybne(-1.,-1.,Veta,t80,SeriousError)
9900  ich=1
9999  if(NDimI(KPhase).gt.0.and.UplnySymbol) then
        call CopyVek(Qu(1,1,1,KPhase),QuSymm,3*NDimI(KPhase))
        call CopyVek(QuOld,Qu(1,1,1,KPhase),3*NDimI(KPhase))
        call CopyVek(QuiOld,Qui(1,1,KPhase),3*NDimI(KPhase))
      endif
      if(ich.eq.0.and.NDimI(KPhase).eq.1) then
        call EM90CorrectForHerold(ipg)
      endif
      return
      end
