      subroutine EM50ReadCellCentr(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension vt6p(:,:),xp(6)
      character*256 EdwStringQuest
      character*80 t80
      character*2 nty
      allocatable vt6p
      logical eqrv
      allocate(vt6p(NDim(KPhase),1000))
      nvtp=NLattVec(KPhase)
      if(nvtp.le.0) then
        nvtp=1
        call SetRealArrayTo(vt6p,NDim(KPhase),0.)
      else
        do i=1,nvtp
          call CopyVek(vt6(1,i,1,KPhase),vt6p(1,i),NDim(KPhase))
        enddo
      endif
      ik=0
      id=NextQuestId()
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,9,'Centring vectors',1,
     1                   LightGray,0,0)
      t80='%Complete the set'
      dpom=FeTxLengthUnder(t80)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,9,dpom,ButYd,t80)
      nButtComplete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      dpom=80.
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,9,dpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonMake(id,xpom,0,dpom,ButYd,'%Previous')
      nButtPrevious=ButtonLastMade
      tpom=5.
      dpom=120.
      xpom=tpom+FeTxLength('XXXX')+15.
      pom=xpom
      il=1
      do i=1,16
        write(cislo,100) i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il,Cislo,'L',dpom,EdwYd,
     1                      1)
        if(i.eq.1) nEdwCentrFirst=EdwLastMade
        if(mod(il,8).eq.0) then
          xpom=xpom+xqd*.5
          tpom=tpom+xqd*.5
          il=0
        endif
        il=il+1
      enddo
      m=1
1200  n1=(m-1)*16+1
      n2=min(n1+15,nvtp)
      nEdw=nEdwCentrFirst
      do i=n1,n1+15
        write(cislo,100) i,nty(i)
        if(i.ne.1) then
          call FeQuestRealAEdwOpen(nEdw,vt6p(1,i),NDim(KPhase),
     1                             i.gt.nvtp,.true.)
          write(cislo,100) i,nty(i)
          call FeQuestEdwLabelChange(nEdw,Cislo)
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestLblMake(id,tpom,1,Cislo,'L','N')
          t80='0 0 0'
          if(NDim(KPhase).gt.3) t80=t80(1:5)//' 0'
          if(NDim(KPhase).gt.4) t80=t80(1:7)//' 0'
          if(NDim(KPhase).gt.5) t80=t80(1:9)//' 0'
          call FeQuestLblMake(id,pom+EdwMarginSize,1,t80,'L','N')
        endif
        nEdw=nEdw+1
      enddo
1500  if(nvtp.gt.16) then
        if(n1.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
        if(n2.lt.nvtp) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        call EM50CompleteCentr(1,vt6p,ubound(vt6p,1),nvtp,1000,ich)
        if(ich.eq.0) then
          QuestCheck(id)=0
        else
          call FeChybne(-1.,-1.,'the set of vectors isn''t complete.',
     1                  ' ',SeriousError)
          EventType=EventEdw
          EventNumber=min(nvtp-n1+2,16)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw) then
        nEdw=CheckNumber
        il=nEdw+n1-1
        if(EdwStringQuest(nEdw).eq.' ') then
          if(il.le.nvtp) then
            do i=il+1,nvtp
              call CopyVek(vt6p(1,i),vt6p(1,i-1),NDim(KPhase))
            enddo
            ipp=il
            nvtp=nvtp-1
            go to 1200
          else
            go to 1500
          endif
        endif
        do i=1,nvtp
          call FeQuestRealAFromEdw(nEdw,xp)
          if(i.ne.il.and.eqrv(xp,vt6p(1,i),NDim(KPhase),.0001)) then
            call FeChybne(-1.,-1.,'the centring vector already present',
     1                    'try again.',SeriousError)
            EventType=EventEdw
            EventNumber=nEdw
            go to 1500
          endif
        enddo
        if(il.gt.nvtp) nvtp=nvtp+1
        call FeQuestRealAFromEdw(nEdw,vt6p(1,il))
        call FeQuestRealAEdwOpen(nEdw,vt6p(1,il),NDim(KPhase),.false.,
     1                           .true.)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtComplete)
     1  then
        call EM50CompleteCentr(0,vt6p,ubound(vt6p,1),nvtp,1000,ich)
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     1         CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          m=m+1
        else
          m=m-1
        endif
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),nvtp,
     1                   NComp(KPhase),NPhase,1)
        NLattVec(KPhase)=nvtp
        do i=1,nvtp
          call CopyVek(vt6p(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
        enddo
      endif
      call FeQuestRemove(id)
9999  deallocate(vt6p)
      return
100   format(i2,a2)
      end
