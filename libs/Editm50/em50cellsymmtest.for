      subroutine EM50CellSymmTest(Klic,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real ShSgS(6),gpp(36),gppi(36),CellNew(6),QuNew(3,3),trezS(3)
      real, allocatable :: RMagS(:,:),ZMagS(:)
      character*80 Veta
      character*4  LatticeS
      integer CrSystemSave,MonoclinicSave,NCommenOld(3)
      logical EqRV,FeYesNoHeader,EqIgCase
      real MetTens6NewI(36),MetTens6OldI(36),RCommenOld(9)
      ich=0
      KCommenOld=KCommen(KPhase)
      ICommenOld=ICommen(KPhase)
      if(KCommen(KPhase).ne.0) then
        call CopyVekI(NCommen(1,1,KPhase),NCommenOld,3)
        call CopyMat(RCommen(1,1,KPhase),RCommenOld,3)
        ISymmStore=0
        call CrlStoreSymmetry(ISymmStore)
        allocate(RMagS(9,NSymm(KPhase)),ZMagS(NSymm(KPhase)))
        nssn =NSymmN(KPhase)
        LatticeS=Lattice(KPhase)
        CrSystemSave=CrSystem(KPhase)
        MonoclinicSave=Monoclinic(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(RMag(1,i,1,KPhase),RMagS(1,i),3)
          ZMagS(i)=ZMag(i,1,KPhase)
        enddo
        call CopyVek(ShSg(1,KPhase),ShSgS,NDim(KPhase))
        call CopyVek(trez(1,1,1),trezS,NDimI(KPhase))
        call comsym(0,0,ich)
      endif
      if(ParentStructure.and.NQMag(KPhase).eq.1) then
!        Nevim proc to tady bylo ...
        call CopyMat(MetTensI(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call CopyMat(MetTensI(1,1,KPhase),MetTens6OldI,NDim(KPhase))
!        Tak asi uz vime. NDim(KPhase) je 3 i pro modulovane
!        call CopyMat(MetTens6I(1,1,KPhase),MetTens6NewI,NDim(KPhase))
!        call CopyMat(MetTens6I(1,1,KPhase),MetTens6OldI,NDim(KPhase))
      else
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6OldI,NDim(KPhase))
      endif
      call EM50MetTensSymm(MetTens6NewI)
      if(KCommenOld.ne.0) then
        call CrlRestoreSymmetry(ISymmStore)
        NDimI(KPhase)=NDim(KPhase)-3
        NDimQ(KPhase)=NDim(KPhase)**2
        NSymmN(KPhase)=nssn
        Lattice(KPhase)=LatticeS
        do i=1,NSymm(KPhase)
          call CopyMat(RMagS(1,i),RMag(1,i,1,KPhase),3)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                  symmc(1,i,1,KPhase),0)
          ZMag(i,1,KPhase)=ZMagS(i)
        enddo
        call CopyVek(ShSgS,ShSg(1,KPhase),NDim(KPhase))
        ngc(KPhase)=0
        KCommen(KPhase)=1
        ICommen(KPhase)=ICommenOld
        call CopyVekI(NCommenOld,NCommen(1,1,KPhase),3)
        call CopyMat(RCommenOld,RCommen(1,1,KPhase),3)
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        SwitchedToComm=.false.
        call CopyVek(trezS,trez(1,1,1),NDimI(KPhase))
        deallocate(RMagS,ZMagS)
      endif
      if(.not.eqrv(MetTens6OldI,MetTens6NewI,NDimQ(KPhase),1.e-6)) then
        call MatBlock3(MetTens6NewI,gppi,NDim(KPhase))
        call MatInv(gppi,gpp,det,3)
        CellNew(1)=sqrt(gpp(1))
        CellNew(2)=sqrt(gpp(5))
        CellNew(3)=sqrt(gpp(9))
        CellNew(4)=acos(gpp(6)/(CellNew(2)*CellNew(3)))/ToRad
        CellNew(5)=acos(gpp(3)/(CellNew(1)*CellNew(3)))/ToRad
        CellNew(6)=acos(gpp(2)/(CellNew(1)*CellNew(2)))/ToRad
        if(NDimI(KPhase).gt.0) then
          m=3*NDim(KPhase)+1
          do i=1,NDimI(KPhase)
            call CopyVek(MetTens6NewI(m),gppi,3)
            call multm(gpp,gppi,QuNew(1,i),3,3,1)
            m=m+NDim(KPhase)
          enddo
        endif
        if(ICDDBatch) then
          TextInfo(1)='Cell parameters'
          if(NDimI(KPhase).gt.0) then
            Veta=' and/or modulation vector'
            if(NDimI(KPhase).gt.1) Veta=Veta(:idel(Veta))//'s'
          endif
          TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                Veta(:idel(Veta))//
     2                'are inconsistent with symmetry.'
          call FeChybne(-1.,-1.,TextInfo(1),' ',SeriousError)
          TextInfo(1)='Original cell parameters:'
          if(NDimI(KPhase).gt.0) then
            Veta=' and modulation vector'
            if(NDimI(KPhase).eq.1) then
              Veta=Veta(:idel(Veta))//':'
            else
              Veta=Veta(:idel(Veta))//'s:'
            endif
            Veta=TextInfo(1)(:idel(TextInfo(1))-1)//
     1                  Veta(:idel(Veta))
          endif
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          write(Veta,100)(CellPar(i,1,KPhase),i=1,6)
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          do i=1,NDimI(KPhase)
            write(Veta,102)(Qu(j,i,1,KPhase),j=1,3)
            call FeLstWriteLine(Veta,-1)
            if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          enddo
          TextInfo(1)='New cell parameters:'
          if(NDimI(KPhase).gt.0) then
            Veta=' and modulation vector'
            if(NDimI(KPhase).eq.1) then
              Veta=Veta(:idel(Veta))//':'
            else
              Veta=Veta(:idel(Veta))//'s:'
            endif
            Veta=TextInfo(1)(:idel(TextInfo(1))-1)//
     1                  Veta(:idel(Veta))
          endif
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          write(Veta,100)(CellNew(i),i=1,6)
          call FeLstWriteLine(Veta,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          do i=1,NDimI(KPhase)
            write(Veta,102)(QuNew(j,i),j=1,3)
            call FeLstWriteLine(Veta,-1)
            if(LogLn.gt.0) write(LogLn,FormA) Veta(:idel(Veta))
          enddo
          call CopyVek(CellNew,CellPar(1,1,KPhase),6)
          do i=1,NDimI(KPhase)
            call CopyVek(QuNew(1,i),Qu(1,i,1,KPhase),3)
          enddo
        else
          if(Klic.eq.0) then
            k=1
            TextInfo(k)='Original cell parameters:'
            TextInfoFlags(k)='CB'
            if(NDimI(KPhase).gt.0) then
              Veta=' and modulation vector'
              if(NDimI(KPhase).eq.1) then
                Veta=Veta(:idel(Veta))//':'
              else
                Veta=Veta(:idel(Veta))//'s:'
              endif
              TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))-1)//
     1                    Veta(:idel(Veta))
            endif
            k=k+1
            write(TextInfo(k),100)(CellPar(i,1,KPhase),i=1,6)
            do i=1,NDimI(KPhase)
              k=k+1
              write(TextInfo(k),101)(Qu(j,i,1,KPhase),j=1,3)
            enddo
            k=k+1
            TextInfo(k)='New cell parameters:'
            TextInfoFlags(k)='CB'
            if(NDimI(KPhase).gt.0)
     1        TextInfo(k)=TextInfo(k)(:idel(TextInfo(k))-1)//
     2                    Veta(:idel(Veta))
            k=k+1
            write(TextInfo(k),100) CellNew
            do i=1,NDimI(KPhase)
              k=k+1
              write(TextInfo(k),101)(QuNew(j,i),j=1,3)
            enddo
            Ninfo=k
            do k=1,NInfo
              call DeleteFirstSpaces(TextInfo(k))
              if(.not.EqIgCase(TextInfoFlags(k),'CB'))
     1          TextInfoFlags(k)='CN'
            enddo
            if(NDimI(KPhase).le.0) then
              Veta='Do you want to accept the symmetrized cell '//
     1             'parameters?'
            else
              Veta='Do you want to accept the symmetrized cell '//
     1             'parameters'//Veta(:idel(Veta)-1)//'?'
            endif
            if(FeYesNoHeader(-1.,YBottomMessage,Veta,0)) then
              go to 4500
            else
              ich=1
            endif
          else
            go to 4500
          endif
        endif
      endif
      go to 5000
4500  call CopyVek(CellNew,CellPar(1,1,KPhase),6)
      do i=1,NDimI(KPhase)
         call CopyVek(QuNew(1,i),Qu(1,i,1,KPhase),3)
      enddo
      call GetQiQr(qu(1,1,1,KPhase),qui(1,1,KPhase),quir(1,1,KPhase),
     1             NDimI(KPhase),1)
5000  i=mod(CrSystem(KPhase),10)
      if(i.eq.CrSystemTriclinic) then
        go to 9999
      else if(i.eq.CrSystemMonoclinic) then
        j=Monoclinic(KPhase)
      else
        j=0
      endif
      do k=1,3
        if(k.ne.j) CellParSU(k+3,1,KPhase)=0.
      enddo
      if(i.gt.CrSystemOrthorhombic) then
        if(i.eq.CrSystemCubic) then
          kk=3
        else
          kk=2
        endif
        pom=0.
        do k=1,kk
          pom=pom+CellParSU(k,1,KPhase)
        enddo
        pom=pom/float(kk)
        do k=1,kk
          CellParSU(k,1,KPhase)=pom
        enddo
      endif
9999  return
100   format(3f9.4,3f9.3)
101   format(13x,3f9.4)
102   format(3f9.4)
      end
