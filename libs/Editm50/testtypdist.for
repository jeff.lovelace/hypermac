      subroutine TestTypDist
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*20 dist(2)
      character*2 At1(2),At2(2)
      logical EqIgCase
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1              'typical_distances.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'bondval/typical_distances.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      n=1
1000  rewind ln
      do i=1,n
        read(ln,FormA256,end=9999) t256
      enddo
1100  read(ln,FormA256) t256
      k=0
      do i=1,2
        call kus(t256,k,At1(i))
      enddo
      call kus(t256,k,dist(1))
      n=n+1
1200  read(ln,FormA256,end=2000) t256
      k=0
      do i=1,2
        call kus(t256,k,At2(i))
      enddo
      call kus(t256,k,dist(2))
      if((EqIgCase(At1(1),At2(1)).and.EqIgCase(At1(2),At2(2))).or.
     1   (EqIgCase(At1(1),At2(2)).and.EqIgCase(At1(2),At2(1)))) then
        pause
      else
        go to 1200
      endif
2000  go to 1000
9999  return
      end
