      subroutine EM50SetEps(ie,ipr,ieps,tau,idl,ipg,ig,imd,itxt,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension ie(*),ieps(*),tau(*),ipr(*)
      character*(*) itxt
      character*8 t8(3)
      ich=0
      do i=1,5
        ipr(i)=i
      enddo
      if(ipg.eq.1.or.ipg.eq.9.or.ipg.eq.21) then
        ie(1)=1
      else if(ipg.eq.2.or.ipg.eq.10.or.ipg.eq.22) then
        ie(1)=-1
      else if(ipg.eq.3.or.ipg.eq.4) then
        iz=7-2*ipg
        if(Monoclinic(KPhase).eq.iqv) then
          ie(1)= iz
        else
          ie(1)=-iz
        endif
      else if(ipg.eq.5) then
        if(Monoclinic(KPhase).eq.iqv) then
          ie(1)= 1
          ie(2)=-1
          tau(2)=tau(1)
        else
          ie(1)=-1
          tau(1)=tau(2)
          ie(2)= 1
        endif
      else if(ipg.ge.6.and.ipg.le.8) then
        if(iqv.gt.3) go to 9100
        iz=7-ipg
        if(iz.eq.0) iz=-1
        do i=1,3
          if(i.eq.iqv) then
            ie(i)= iz
          else
            ie(i)=-iz
          endif
        enddo
        if(ipg.eq.7) ie(imd)=-ie(imd)
        i1=0
        i2=0
        i3=0
        do i=1,3
          if(ie(i).eq.-1) cycle
          if(i1.eq.0) then
            i1=i
          else if(i2.eq.0) then
            i2=i
          else if(i3.eq.0) then
            i3=i
          endif
        enddo
        if(itxt(1:1).eq.'-') then
          kp=2
        else
          kp=1
        endif
        k=kp
        do i=1,idel(itxt)
          if(itxt(i:i).eq.';') itxt(i:i)=' '
        enddo
        do i=1,3
          call kus(itxt,k,t8(i))
        enddo
        if(i2.eq.0) then
          i2=i1+1
          if(i2.gt.3) i2=1
        endif
        itxt=itxt(1:kp)//t8(i1)(:idel(t8(i1)))//';'//
     1                   t8(i2)(:idel(t8(i2)))
      else if(ipg.eq.11.or.ipg.eq.23) then
        ie(1)=1
        ie(2)=-1
        tau(2)=tau(1)
      else if(ipg.eq.12.or.ipg.eq.24) then
        ie(1)=1
        ie(2)=-1
        ie(3)=-1
        tau(3)=tau(2)+tau(1)
        if(ig.ge.177) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.13.or.ipg.eq.25) then
        ie(1)=1
        ie(2)=1
        ie(3)=1
c        if(abs(tau(1)-.25).lt..0001.and.abs(tau(2)-.25).lt..0001) then
c          if(ig.eq.102) then
c            if(abs(tau(3)-.5).lt..0001) tau(2)=-.25
c          else
c            if(abs(tau(3))   .lt..0001) tau(2)=-.25
c          endif
c        endif
        if(ig.ge.183) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.14.or.ipg.eq.26) then
        ie(1)=-1
        if((ig.ge.115.and.ig.le.120).or.ig.eq.187.or.ig.eq.188) then
          ie(2)=1
          ie(3)=-1
          tau(3)=tau(2)
        else
          ie(2)=-1
          ie(3)=1
          tau(2)=tau(3)
        endif
        if(ig.ge.187) call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.15.or.ipg.eq.27) then
        ie(1)=1
        ie(2)=-1
        ie(3)=1
        ie(4)=1
c        if(abs(tau(1)-.25).lt..0001.and.abs(tau(3)-.25).lt..0001) then
c          if(ig.eq.133.or.ig.eq.134) then
c            if(abs(tau(4))   .lt..0001) tau(3)=-.25
c          else
c            if(abs(tau(4)-.5).lt..0001) tau(3)=-.25
c          endif
c        endif
        if(ig.ge.191) then
          j=4
        else
          j=3
        endif
        call EM50ChngOrder(ie,ieps,tau,ipr,2,j)
      else if(ipg.eq.16) then
        ie(1)=1
        ieps(1)=1
      else if(ipg.eq.17) then
        ie(1)=1
        ieps(1)=-ieps(1)
        if(ig.eq.162.or.ig.eq.163) call EM50ChngOrder(ie,ieps,tau,ipr,
     1                                                2,3)
      else if(ipg.eq.18) then
        ie(1)=1
        ie(2)=-1
        if(idl.ne.2) ie(3)=1
        if(ig.eq.149.or.ig.eq.151.or.ig.eq.153)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.19) then
        do i=1,idl
          ie(i)=1
        enddo
        if(ig.eq.157.or.ig.eq.159)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      else if(ipg.eq.20) then
        do i=1,idl
          ie(i)=1
        enddo
        ieps(1)=-ieps(1)
        if(ig.eq.162.or.ig.eq.163)
     1    call EM50ChngOrder(ie,ieps,tau,ipr,2,3)
      endif
      go to 9999
9100  ich=1
9999  return
      end
