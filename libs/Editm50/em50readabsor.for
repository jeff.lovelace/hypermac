      function EM50ReadAbsor(At,LamAveUse)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*256 t256
      character*80  Radka
      character*2   AtSearch
      logical ExistFile,EqIgCase
      real LamAveUse
      dimension xx(4),ff(4),xp(2)
      equivalence (t256,t80)
      EM50ReadAbsor=0.
      Energy=ToKeV/LamAveUse
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1              'absor.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/absor.dat'
      endif
      if(.not.ExistFile(t256)) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      read(ln,FormA80) Radka
      if(Radka(1:1).ne.'#') rewind ln
      if(ErrFlag.ne.0) go to 9999
      if(EqIgCase(At,'D')) then
        AtSearch='H'
      else
        AtSearch=At
      endif
      IAtSearch=KtAt(AtN,98,AtSearch)
      n=0
      if(IAtSearch.le.0) then
        EM50ReadAbsor=0.
        go to 9999
      endif
2100  read(ln,FormA80,end=2500) Radka
      k=0
      call kus(Radka,k,Cislo)
      if(.not.EqIgCase(Cislo,AtSearch)) go to 2100
2200  read(ln,FormA80) Radka
      j=1
2300  read(ln,*,err=9999,end=9999) xp
      jo=j
      if(xp(1).gt.Energy.or.j.lt.2) j=j+1
      if(jo.eq.j) then
        ff(j-1)=ff(j)
        xx(j-1)=xx(j)
      endif
      xx(j)=xp(1)
      ff(j)=xp(2)
      if(j.ge.4) then
        EM50ReadAbsor=anint(Finter4(ff,xx,Energy))
        go to 9999
      else
        go to 2300
      endif
2500  n=n-1
      AtSearch=AtN(IAtSearch+n)
      rewind ln
      go to 2100
9999  call CloseIfOpened(ln)
      return
      end
