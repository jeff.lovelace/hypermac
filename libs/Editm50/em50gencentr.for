      subroutine EM50GenCentr(itxt,nc,FirstTime,*,*)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) itxt
      logical FirstTime
      if(itxt(1:1).eq.'-') then
        NCSymm(KPhase)=2
        itxt=itxt(2:)
      else if(itxt(1:1).eq.'+') then
        NCSymm(KPhase)=1
        itxt=itxt(2:)
      else
        NCSymm(KPhase)=1
      endif
      nc=index(smbc,itxt(1:1))
      if(nc.gt.0.and.nc.le.8) then
        call EM50GenVecCentr(nc,FirstTime,ich)
        if(ich.ne.0) return2
      else
        return1
      endif
      Lattice(KPhase)=itxt(1:1)
      itxt=itxt(2:)
      return
      end
