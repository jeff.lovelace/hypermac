      subroutine EM50ReadMagneticFFLabels(AtomTypeToRead,Ionts,NIonts,
     1                                    ierr)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) AtomTypeToRead,Ionts(10,5)
      character*256 t256
      character*80  t80,Label
      character*40 :: LabelSearch(5) = (/
     1                'Magnetic_formfactor_<j0>      ',
     2                'Magnetic_formfactor_<j2>      ',
     3                'Magnetic_formfactor_<j4>      ',
     4                'Magnetic_formfactor_<j6>      ',
     5                'Magnetic_formfactor_<j0>+c<j2>'/)
      character*7 At
      integer NIonts(5)
      logical EqIgCase
      equivalence (t80,t256)
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//'atoms.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/data/atoms.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9200
      read(ln,FormA) t256
      if(t256(1:1).ne.'#') rewind ln
      ierr=0
      NIonts=0
      Ionts=' '
1000  read(ln,'(a7)',end=9000,err=9100) At
      if(EqIgCase(At,AtomTypeToRead)) then
2000    read(ln,FormA,end=9000,err=9100) t256
2100    if(LocateSubstring(t256,'Electron_form_factor',.false.,.true.)
     1     .gt.0.or.EqIgCase(t256,'end')) go to 9999
        k=0
        call kus(t256,k,Label)
        m=LocateInStringArray(LabelSearch,5,Label(:idel(Label)),
     1                        IgnoreCaseYes)
        if(m.gt.0) then
          NIonts(m)=0
2500      read(ln,FormA,end=9050,err=9100) t256
          if(Index(t256,'_').gt.0) go to 2100
          k=0
          NIonts(m)=NIonts(m)+1
          call kus(t256,k,Ionts(NIonts(m),m))
          go to 2500
        else
          go to 2000
        endif
      else
3000    read(ln,FormA,end=9000,err=9100) t256
        if(.not.EqIgCase(t256,'end')) go to 3000
      endif
      go to 1000
9000  iErr=1
      go to 9999
9050  iErr=2
      go to 9999
9100  call FeReadError(ln)
      iErr=3
      go to 9999
9200  iErr=4
9999  call CloseIfOpened(ln)
      return
      end
