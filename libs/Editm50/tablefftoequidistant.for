      subroutine TableFFToEquidistant(FFIn,FFOut,AtTypeIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension FFIn(*),FFOut(*)
      character*(*) AtTypeIn
      dimension am(4,4),ami(4,4),b(4),AFFIn(4,62)
      n=62
      do i=1,n
        if(i.eq.1) then
          jp=0
        else if(i.gt.n-2) then
          jp=n-4
        else
          jp=i-2
        endif
        if(AtTypeIn.eq.'H') then
          pom=1./(ffxh(jp+1)-ffxh(jp+2))
        else
          pom=1./(ffx(jp+1)-ffx(jp+2))
        endif
        do j=1,4
          b(j)=FFIn(jp+j)-FFIn(jp+2)
          if(AtTypeIn.eq.'H') then
            xp=(ffxh(jp+j)-ffxh(jp+2))*pom
          else
            xp=(ffx(jp+j)-ffx(jp+2))*pom
          endif
          xs=1.
          do k=1,4
            am(j,k)=xs
            xs=xs*xp
          enddo
        enddo
        call matinv(am,ami,xp,4)
        call multm(ami,b,AFFIn(1,i),4,4,1)
        AFFIn(1,i)=AFFIn(1,i)+FFIn(jp+2)
      enddo
      sinthl=0.
      do i=1,121
        do j=1,n
          if(AtTypeIn.eq.'H') then
            ffxj=ffxh(j)
          else
            ffxj=ffx(j)
          endif
          if(sinthl.lt.ffxj.or.j.eq.n) then
            jp=min(j,n-2)
            pom=AFFIn(4,jp)
            if(AtTypeIn.eq.'H') then
              xp=(sinthl-ffxh(jp))/(ffxh(jp-1)-ffxh(jp))
            else
              xp=(sinthl-ffx(jp))/(ffx(jp-1)-ffx(jp))
            endif
            do k=3,1,-1
              pom=pom*xp+AFFIn(k,jp)
            enddo
            go to 4500
          endif
        enddo
4500    FFOut(i)=pom
        sinthl=sinthl+.05
      enddo
      return
      end
