      subroutine EM50TypicalDist
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TypicalDistC/ nRolMenuFirst,nEdwDist,TypDist,AtNames(2)
      dimension xp(2)
      character*256 t256
      character*80  t80
      character*2   nty,AtNames,At(2)
      integer WhatHappened,RolMenuSelectedQuest
      logical EqIgCase
      external EM50TypicalDistReadCommand,EM50TypicalDistWriteCommand,
     1         FeVoid
      save /TypicalDistC/
      equivalence (t80,t256)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_typdist.tmp','formatted','unknown')
      if(NTypicalDistMax.gt.0) then
        do i=1,NTypicalDist(KPhase)
          write(Cislo,'(f10.3)') DTypicalDist(i,KPhase)
          t80='typdist '//AtTypicalDist(1,i,KPhase)//' '//
     1                    AtTypicalDist(2,i,KPhase)//' '//
     2        Cislo(:idel(Cislo))
          call ZdrcniCisla(t80,4)
          write(ln,FormA) t80(:idel(t80))
        enddo
      endif
      call CloseIfOpened(ln)
      xqd=400.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_typdist.tmp',xqd,
     1                          il,ilp,0)
      il=ilp+1
      xpom=5.
      dpom=40.
      spom=100.
      tpom=30.
      do i=1,3
        if(i.le.2) then
          write(t80,'(''%'',i1,a2,'' atom type'')') i,nty(i)
        else if(i.eq.3) then
          t80='Dist%ance'
          dpom=60.
          tpom=30.
        endif
        if(i.le.2) then
          call FeQuestRolMenuMake(id,xpom+tpom,il,xpom,il+1,t80,'C',
     1                            dpom+EdwYd,EdwYd,0)
          if(i.eq.1) nRolMenuFirst=RolMenuLastMade
        else
          call FeQuestEdwMake(id,xpom+tpom,il,xpom,il+1,t80,'C',dpom,
     1                        EdwYd,0)
        endif
        if(i.eq.3) then
          nEdwDist=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        endif
        xpom=xpom+spom
      enddo
      il=il+1
      dpom=60.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'F%rom file')
      nButtReadIn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1300  nRolMenu=nRolMenuFirst
      do i=1,2
        call FeQuestRolMenuOpen(nRolMenu,AtTypeMenu,
     1                          NAtFormula(KPhase),0)
        nRolMenu=nRolMenu+1
      enddo
      TypDist=-1.
1350  call FeQuestRealEdwOpen(nEdwDist,TypDist,TypDist.le.0.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  EM50TypicalDistReadCommand,EM50TypicalDistWriteCommand,FeVoid,
     2  FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        nRolMenu=nRolMenuFirst
        do i=1,2
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtNames(i),AtType(j,KPhase))) then
              call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                                NAtFormula(KPhase),j)
              go to 2025
            endif
          enddo
2025      nRolMenu=nRolMenu+1
        enddo
        go to 1350
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReadIn)
     1  then
        nRolMenu=nRolMenuFirst
        do i=1,2
          j=RolMenuSelectedQuest(nRolMenu)
          if(j.le.0) go to 2000
          AtNames(i)=AtType(j,KPhase)
          call Uprat(AtNames(i))
          nRolMenu=nRolMenu+1
        enddo
        if(OpSystem.le.0) then
          t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                'typical_distances.dat'
        else
          t256=HomeDir(:idel(HomeDir))//'bondval/typical_distances.dat'
        endif
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.ne.0) go to 2000
2120    read(ln,FormA80,end=2130) t80
        if(t80.eq.' ') go to 2120
        if(t80(1:1).eq.'#') go to 2120
        read(t80,'(2(a2,2x),f6.3)') At,pom
        if((EqIgCase(AtNames(1),At(1)).and.
     1      EqIgCase(AtNames(2),At(2))).or.
     2     (EqIgCase(AtNames(1),At(2)).and.
     3      EqIgCase(AtNames(2),At(1)))) then
          TypDist=pom
        else
          go to 2120
        endif
2130    call CloseIfOpened(ln)
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.eq.0) then
        call OpenFile(ln,fln(:ifln)//'_typdist.tmp','formatted',
     1                'unknown')
        n=0
3000    read(ln,FormA,end=3100) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,'typdist')) then
          n=n+1
          go to 3000
        endif
3100    call ReallocateTypicalDist(n)
        rewind ln
        n=0
3300    read(ln,FormA,end=3400) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,'typdist')) then
          n=n+1
          do i=1,2
            call Kus(t80,k,AtTypicalDist(i,n,KPhase))
          enddo
          call StToReal(t80,k,xp,1,.false.,ich)
          if(ich.ne.0) then
            n=n-1
          else
            DTypicalDist(n,KPhase)=xp(1)
          endif
          go to 3300
        endif
3400    close(ln,status='delete')
        NTypicalDist(KPhase)=n
      endif
      return
      end
