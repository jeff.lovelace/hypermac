      subroutine MakeSpGroupAlias
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      character*8   GrupaR,GrupaA(16)
      character*2   t2(3,2)
      character*1   abc(3)
      dimension ip(3,3),ic(3),ia(3),iz(3)
      logical EqIgCase,EqIV0
      data abc/'a','b','c'/
      lni=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      lno=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup_alias_new.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_alias_new.dat'
      endif
      call OpenFile(lno,Veta,'formatted','unknown')
      read(lni,FormA256) Veta
      if(Veta(1:1).ne.'#') rewind lni
1200  read(lni,FormSG,end=9000) ig,ipg,idl,GrupaR
      if(ig.lt.16.or.EqIgCase(GrupaR(1:1),'P').or.
     1               EqIgCase(GrupaR(1:1),'F')) go to 1200
      if(EqIgCase(GrupaR,'I222').or.
     1   EqIgCase(GrupaR,'I212121')) go to 1200
      if(ig.gt.74) go to 9000
      call SetIntArrayTo(ic,3,1)
      if(EqIgCase(GrupaR(1:1),'A')) then
        ic(1)=0
      else if(EqIgCase(GrupaR(1:1),'B')) then
        ic(2)=0
      else if(EqIgCase(GrupaR(1:1),'C')) then
        ic(3)=0
      endif
      i=2
      k=1
1300  call SetIntArrayTo(ip(1,k),3,0)
      if(GrupaR(i:i).eq.'2') then
        iz(k)=1
        call SetIntArrayTo(ip(1,k),3,0)
        if(GrupaR(i+1:i+1).eq.'1') then
          i=i+1
          ip(k,k)=1
        endif
      else if(EqIgCase(GrupaR(i:i),'m')) then
        iz(k)=-1
      else if(EqIgCase(GrupaR(i:i),'a')) then
        iz(k)=-1
        ip(1,k)=1
      else if(EqIgCase(GrupaR(i:i),'b')) then
        iz(k)=-1
        ip(2,k)=1
      else if(EqIgCase(GrupaR(i:i),'c')) then
        iz(k)=-1
        ip(3,k)=1
      else if(EqIgCase(GrupaR(i:i),'n')) then
        iz(k)=-1
        call SetIntArrayTo(ip(1,k),3,1)
        ip(k,k)=0
      else if(EqIgCase(GrupaR(i:i),'e')) then
        iz(k)=-1
        call SetIntArrayTo(ip(1,k),3,1)
      endif
      if(k.lt.3) then
        i=i+1
        k=k+1
        go to 1300
      endif
      do i=1,3
        do j=1,2
          n=0
          do k=1,3
            n=n+ip(k,i)
          enddo
          if(n.lt.3) then
            if(j.eq.1) then
              call CopyVekI(ip(1,i),ia,3)
            else
              call AddVekI(ip(1,i),ic,ia,3)
            endif
            do k=1,3
              ia(k)=mod(ia(k),2)
              if((k.eq.i.and.iz(i).lt.0).or.
     1           (k.ne.i.and.iz(i).gt.0)) ia(k)=0
            enddo
          else
            call CopyVekI(ip(1,i),ia,3)
          endif
          if(iz(i).gt.0) then
            if(EqIV0(ia,3)) then
              t2(i,j)='2'
            else
              t2(i,j)='21'
            endif
          else
            NNenul=0
            KdeNenul=0
            do k=1,3
              if(ia(k).ne.0) then
                NNenul=NNenul+1
                KdeNenul=k
              endif
            enddo
            if(NNenul.eq.0) then
              t2(i,j)='m'
            else if(NNenul.eq.1) then
              t2(i,j)=abc(KdeNenul)
            else if(NNenul.eq.2) then
              t2(i,j)='n'
            else if(NNenul.eq.3) then
              t2(i,j)='e'
            endif
          endif
        enddo
      enddo
      n=0
      do i1=1,2
        do i2=1,2
          do 2000i3=1,2
            Veta=GrupaR(1:1)//t2(1,i1)//t2(2,i2)//t2(3,i3)
            call Zhusti(Veta)
            if(EqIgCase(Veta,GrupaR)) go to 2000
            do k=1,n
              if(EqIgCase(Veta,GrupaA(k))) go to 2000
            enddo
            write(lno,'(a8,1x,a8)') Veta(1:8),GrupaR
            n=n+1
            GrupaA(n)=Veta
2000      continue
        enddo
      enddo
      go to 1200
9000  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      return
      end
