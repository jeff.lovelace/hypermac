      subroutine EM50Radiation
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*22  LabelRatio
      character*14  LabelWaveLength1
      character*11  LabelWaveLength
      character*80 Veta
      integer RadiationOld,EdwStateQuest,FeMenu,LblStateQuest
      real LamAveOld
      logical CrwLogicQuest,lpom
      save nCrwRadFirst,nCrwRadLast,nButtTube,nCrwDouble,nEdwWL,nEdwWL1,
     1     nEdwWL2,nEdwRat,RadiationOld,nLblMono,nEdwMonAngle,nLblPol,
     2     nCrwPolarizationFirst,nCrwPolarizationLast,nButtSetMonAngle,
     3     nEdwPerfMon,LPFactorOld,nButtInfoPerpendicular,nEdwGAlpha,
     4     nButtInfoParallel,nEdwGBeta,LamAveOld
      data LabelRatio/'%I(#2)/I(#1)'/,
     1     LabelWaveLength /'Wave length'/,
     2     LabelWaveLength1/'Wave length #1'/
      entry EM50RadiationMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      xpom=5.
      tpom=xpom+CrwgXd+10.
      ichk=1
      igrp=1
      il=1
      do i=1,3
        if(i.eq.1) then
          Veta='%X-rays'
          lpom=Radiation(1).eq.XRayRadiation
        else if(i.eq.2) then
          Veta='Ne%utrons'
          lpom=Radiation(1).eq.NeutronRadiation
        else if(i.eq.3) then
          Veta='%Electrons'
          lpom=Radiation(1).eq.ElectronRadiation
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      ichk,igrp)
        if(i.eq.1) then
          nCrwRadFirst=CrwLastMade
          ilb=il
        else
          xpomb=tpom+FeTxLength(Veta)+10.
        endif
        call FeQuestCrwOpen(CrwLastMade,lpom)
        il=il+1
      enddo
      nCrwRadLast=CrwLastMade
      Veta='X%-ray tube'
      dpomb=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
      nButtTube=ButtonLastMade
      tpom=5.
      Veta='Kalpha1/Kalpha2 dou%blet'
      xpom=tpom+CrwXd+10.
      ichk=1
      igrp=0
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,ichk,
     1                    igrp)
      nCrwDouble=CrwLastMade
      il=il+1
      xpom=tpom+FeTxLength(LabelWaveLength1)+10.
      dpom=100.
      Veta=LabelWaveLength
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwWL=EdwLastMade
      Veta=LabelWaveLength1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwWL1=EdwLastMade
      il=il+1
      i=idel(Veta)
      Veta(i:i)='2'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwWL2=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                    EdwYd,0)
      nEdwRat=EdwLastMade
      il=1
      xpom=xpom+dpom+45.
      tpom=xpom+CrwgXd+10.
      Veta='     Polarization correction:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      nLblPol=LblLastMade
      ichk=1
      igrp=2
      do i=1,5
        il=il+1
        if(i.eq.1) then
          Veta='Circul%ar polarization'
          j=PolarizedCircular
        else if(i.eq.2) then
          Veta='%Perpendicular setting'
         j=PolarizedMonoPer
        else if(i.eq.3) then
          Veta='Pa%rallel setting'
          j=PolarizedMonoPar
        else if(i.eq.4) then
          Veta='Guinier %camera'
          j=PolarizedGuinier
        else
          Veta='%Linearly polarized beam'
          j=PolarizedLinear
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,ichk,igrp)
        call FeQuestCrwOpen(CrwLastMade,LPFactor(1).eq.j)
        if(i.eq.1) then
          nCrwPolarizationFirst=CrwLastMade
        else if(i.eq.2.or.i.eq.3) then
          if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
          Veta='Info'
          dpomb=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
          if(i.eq.2) then
            nButtInfoPerpendicular=ButtonLastMade
          else
            nButtInfoParallel=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        endif
      enddo
      nCrwPolarizationLast=CrwLastMade
      il=il+1
      Veta='     Monochromator parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      call FeQuestLblOff(LblLastMade)
      nLblMono=LblLastMade
      il=il+1
      Veta='Per%fectness'
      xpom=tpom+FeTxLengthUnder(Veta)+35.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPerfMon=EdwLastMade
      il=il+1
      Veta='Glancing an%gle'
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMonAngle=EdwLastMade
      Veta='%Set glancing angle'
      dpomb=FeTxLengthUnder(Veta)+5.
      call FeQuestButtonMake(id,xpom+dpom+15.,il,dpomb,ButYd,Veta)
      nButtSetMonAngle=ButtonLastMade
      Veta='Alp%ha(Guinier)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwGAlpha=EdwLastMade
      il=il+1
      Veta='Beta(Gui%nier)'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwGBeta=EdwLastMade
      RadiationOld=Radiation(1)
      LPFactorOld=LPFactor(1)
      RadiationOld=-1
      LPFactorOld=-1
      LamAveOld=LamAve(1)
      go to 2500
      entry EM50RadiationCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwRadFirst.and.CheckNumber.le.nCrwRadLast)
     2  then
        AllowChargeDensities=.false.
        if(CrwLogicQuest(nCrwRadFirst)) then
          Radiation(1)=XRayRadiation
          AllowChargeDensities=.true.
        else if(CrwLogicQuest(nCrwRadFirst+1)) then
          Radiation(1)=NeutronRadiation
        else
          Radiation(1)=ElectronRadiation
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              LPFactor(1)=PolarizedCircular
            else if(i.eq.2) then
              LPFactor(1)=PolarizedMonoPer
            else if(i.eq.3) then
              LPFactor(1)=PolarizedMonoPar
            else if(i.eq.4) then
              LPFactor(1)=PolarizedGuinier
            else
              LPFactor(1)=PolarizedLinear
            endif
            exit
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        if(CrwLogicQuest(nCrwDouble)) then
          NAlfa(1)=2
        else
          NAlfa(1)=1
        endif
        RadiationOld=-1
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          LamA1(1)=LamA1D(k)
          LamA2(1)=LamA2D(k)
          LamAve(1)=LamAveD(k)
          LamRat(1)=LamRatD(k)
          if(CrwLogicQuest(nCrwDouble)) then
            call FeQuestRealEdwOpen(nEdwWL1,LamA1(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwWL2,LamA2(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwRat,LamRat(1),.false.,
     1                              .false.)
          else
            call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,
     1                              .false.)
          endif
        endif
!        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 9999
      else if(CheckType.eq.EventEdw.and.(CheckNumber.eq.nEdwWL1.or.
     1                                   CheckNumber.eq.nEdwWL2)) then
        continue
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(NAlfa(1).gt.1) then
          call FeQuestRealFromEdw(nEdwWL1,LamA1(1))
          call FeQuestRealFromEdw(nEdwWL2,LamA2(1))
          call FeQuestRealFromEdw(nEdwRat,LamRat(1))
          LamAve(1)=(LamA1(1)+
     1               LamA2(1)*LamRat(1))/(1.+LamRat(1))
        else
          call FeQuestRealFromEdw(nEdwWL,LamAve(1))
          LamA1(1)=LamAve(1)
          LamA2(1)=LamAve(2)
        endif
        AngleMonPom=AngleMon(1)
        call CrlCalcMonAngle(AngleMonPom,LamAve(1),ichp)
        if(ichp.ne.0) go to 9999
        AngleMon(1)=AngleMonPom
        call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(1),.false.,
     1                          .false.)
      endif
2500  if(Radiation(1).ne.RadiationOld) then
        if(Radiation(1).eq.XRayRadiation) then
          call FeQuestButtonOff(nButtTube)
          call FeQuestCrwOpen(nCrwDouble,NAlfa(1).gt.1)
          if(NAlfa(1).gt.1) then
            if(EdwStateQuest(nEdwWL1).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL)
              call FeQuestRealEdwOpen(nEdwWL1,LamA1(1),.false.,.false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2(1),.false.,.false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRat(1),.false.,.false.)
            endif
          else
            if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL1)
              call FeQuestEdwClose(nEdwWL2)
              call FeQuestEdwClose(nEdwRat)
              call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,.false.)
            endif
          endif
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
            call FeQuestEdwClose(nEdwWL1)
            call FeQuestEdwClose(nEdwWL2)
            call FeQuestEdwClose(nEdwRat)
            call FeQuestRealEdwOpen(nEdwWL,LamAve(1),.false.,.false.)
          endif
        endif
      endif
      if(Radiation(1).ne.RadiationOld.or.LPFactor(1).ne.LPFactorOld)
     1  then
        if(Radiation(1).eq.NeutronRadiation.or.
     1     Radiation(1).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            LPFactor(1)=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(LPFactor(1).eq.PolarizedMonoPer.or.
     1     LPFactor(1).eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(1),.false.,
     1                              .false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(1),.false.,
     1                              .false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(LPFactor(1).eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(1),.false.,
     1                              .false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,AlphaGMon(1),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwGBeta,BetaGMon(1),.false.,
     1                              .false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
      if(Radiation(1).eq.NeutronRadiation) NAlfa(1)=1
      if(NAlfa(1).gt.1) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(1))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(1))
        call FeQuestRealFromEdw(nEdwRat,LamRat(1))
        LamAve(1)=(LamA1(1)+LamRat(1)*LamA2(1))/(1.+LamRat(1))
      else
        call FeQuestRealFromEdw(nEdwWL,LamAve(1))
        LamA1(1)=LamAve(1)
        LamA2(1)=LamAve(1)
      endif
      RadiationOld=Radiation(1)
      LPFactorOld=LPFactor(1)
      entry EM50RadiationUpdate
      if(Radiation(1).eq.NeutronRadiation) NAlfa(1)=1
      if(NAlfa(1).gt.1) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(1))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(1))
        call FeQuestRealFromEdw(nEdwRat,LamRat(1))
        LamAve(1)=(LamA1(1)+LamRat(1)*LamA2(1))/(1.+LamRat(1))
      else
        call FeQuestRealFromEdw(nEdwWL,LamAve(1))
        LamA1(1)=LamAve(1)
        LamA2(1)=LamAve(1)
      endif
      klam(1)=LocateInArray(LamAve(1),LamAveD,7,.0001)
      if(Radiation(1).eq.XRayRadiation.and.
     1   (abs(LamAve(1)-LamAveOld).gt..0001.or.
     2    Radiation(1).ne.RadiationOld)) then
        do KPh=1,NPhase
          KPhase=KPh
          do i=1,NAtFormula(KPh)
            AtTypeFull(i,KPh)=AtType(i,KPh)
            AtTypeMag(i,KPh)=' '
            AtTypeMagJ(i,KPh)=' '
          enddo
          call EM50ReadAllFormFactors
          call EM50FormFactorsSet
        enddo
      endif
      LamAveOld=LamAve(1)
      if(LpFactor(1).ne.PolarizedCircular.and.
     1   LpFactor(1).ne.PolarizedLinear) then
        call FeQuestRealFromEdw(nEdwPerfMon,FractPerfMon(1))
        if(LpFactor(1).eq.PolarizedGuinier) then
          call FeQuestRealFromEdw(nEdwGAlpha,AlphaGMon(1))
          call FeQuestRealFromEdw(nEdwGBeta,BetaGMon(1))
        else
          call FeQuestRealFromEdw(nEdwMonAngle,AngleMon(1))
        endif
      endif
9999  return
      end
