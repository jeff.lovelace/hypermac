      subroutine EM50Magnetic
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*80 Veta
      character*7  At,AtTypeMagP
      character*5  MenuJ(10,5)
      integer RolMenuSelectedQuest,nCrwJType(0:5),nRolMenuJType(5),
     1        NMenuJ(5),IMenuJ(5)
      logical CrwLogicQuest,EqIgCase,MagneticAtom
      save nRolMenuAtType,nCrwJType,nButtOwn,nRolMenuLast,nCrwMagnetic,
     1     MagneticAtom,LastAtomOld,MenuJ,NMenuJ,IMenuJ,nLblNejde,
     2     nCrwActivateMagnetic,nRolMenuJType
      entry EM50MagneticMake(id)
      MenuJ(1,1)='Prvni'
      MenuJ(2,1)='Druhy'
      NMenuJ(1)=2
      Veta=' '
      call FeQuestLblMake(id,xdqp*.5,6,' ','C','B')
      nLblNejde=LblLastMade
      call FeQuestLblOff(nLblNejde)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+10.
      tpoms=tpom
      Veta='Activate the ma%gnetic option'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwActivateMagnetic=CrwLastMade
      il=il+1
      Veta='%Atom type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      tpom=xpom+dpom+30.
      Veta='%Use as a magnetic atom'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,.true.)
      call FeQuestCrwDisable(CrwLastMade)
      nCrwMagnetic=CrwLastMade
      Veta='%Own formfactors'
      xpom=tpom
      tpom=xpom+CrwXd+10.
      xpomp=tpom+FeTxLengthUnder(Veta)+100.
      dpom=80.
      do i=0,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                      1)
        if(i.eq.0) then
          Veta='%Edit'
          call FeQuestButtonMake(id,xpomp,il,dpom,ButYd,Veta)
          nButtOwn=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestButtonDisable(ButtonLastMade)
        else
          call FeQuestRolMenuMake(id,tpom,il,xpomp,il,'  ','L',dpom,
     1                            EdwYd,1)
          call FeQuestRolMenuOpen(RolMenuLastMade,MenuJ(1,1),NMenuJ(1),
     1                            1)
          call FeQuestRolMenuDisable(RolMenuLastMade)
          nRolMenuJType(i)=RolMenuLastMade
        endif
        if(i.lt.4) then
          write(Veta,'(''Magnetic formfactor <j%'',i1,''>'')') 2*i
        else if(i.eq.4) then
          Veta='Magnetic formfactor <j0>%+c<j2>'
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.0)
        call FeQuestCrwDisable(CrwLastMade)
        nCrwJType(i)=CrwLastMade
      enddo
      LastAtomOld=-1
      LastAtom=1
      KartId=id
      entry EM50MagneticRefresh
      Klic=0
      if(.not.ExistNeutronData.and.
     1   (ExistM90.or.Radiation(1).ne.NeutronRadiation)) then
        Veta='The magnetic option is not applicable.'
      else if(NAtFormula(KPhase).le.0) then
        Veta='The chemical formula has not been yet defined.'
      else
        Veta=' '
      endif
      if(Veta.ne.' ') then
        call FeQuestLblChange(nLblNejde,Veta)
        do i=0,5
          il=il+1
          call FeQuestCrwClose(nCrwJType(i))
          if(i.gt.0) call FeQuestRolMenuClose(nRolMenuJType(i))
        enddo
        call FeQuestCrwClose(nCrwMagnetic)
        call FeQuestButtonClose(nButtOwn)
      else
        call FeQuestLblOff(nLblNejde)
        call FeQuestCrwOpen(nCrwActivateMagnetic,
     1                      MagneticType(KPhase).ne.0)
        if(NAtFormula(KPhase).gt.0) then
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
          if(LastAtom.gt.NAtFormula(KPhase)) LastAtom=1
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),LastAtom)
          MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
        else
          MagneticAtom=.false.
        endif
      endif
      go to 2000
      entry EM50MagneticCheck
      Klic=1
2000  if(.not.ExistNeutronData.and.
     1   (ExistM90.or.Radiation(1).ne.NeutronRadiation)) go to 9999
2100  if(CrwLogicQuest(nCrwActivateMagnetic)) then
        MagneticType(KPhase)=1
        if(.not.allocated(rmag)) then
          allocate(rmag(9,MaxNSymm,MaxNComp,NPhase),
     1             zmag(MaxNSymm,MaxNComp,NPhase))
          do i=1,MaxNSymm
            ZMag(i,1,KPhase)=1.
            call CrlMakeRMag(i,1)
          enddo
        endif
      else
        MagneticType(KPhase)=0
      endif
      MaxMagenticType=0
      do i=1,NPhase
        MaxMagneticType=max(MaxMagneticType,MagneticType(i))
      enddo
2150  if(MagneticType(KPhase).ne.0) then
        if(LastAtom.ne.LastAtomOld) then
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),LastAtom)
          call FeQuestCrwOpen(nCrwMagnetic,MagneticAtom)
        endif
        if(MagneticAtom) then
          if(LastAtom.ne.LastAtomOld) then
            call EM50ReadMagneticFFLabels(AtType(LastAtom,KPhase),
     1        MenuJ,NMenuJ,ierr)
            if(AtTypeMag(LastAtom,KPhase).eq.' ') then
              if(NMenuJ(1).gt.0) then
                AtTypeMag(LastAtom,KPhase)=MenuJ(NMenuJ(1),1)
                AtTypeMagJ(LastAtom,KPhase)='j0'
                Veta='Magnetic_formfactor_<j0>'
                call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1                                 AtTypeMag(LastAtom,KPhase),Veta,
     2                                 FFMag(1,LastAtom,KPhase),ich)
              else
                AtTypeMag(LastAtom,KPhase)=AtType(LastAtom,KPhase)
                AtTypeMagJ(LastAtom,KPhase)='own'
                TypeFFMag(LastAtom,KPhase)=0
                call SetRealArrayTo(FFMag(1,LastAtom,KPhase),7,0.)
              endif
            endif
            if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'own')) then
              JAtP=0
            else if(EqIgCase(AtTypeMagJ(LastAtom,KPhase),'j0j2')) then
              JAtP=5
            else
              JAtP=1
              read(AtTypeMagJ(LastAtom,KPhase)(2:2),'(i1)',err=2200) i
              JAtP=i/2+1
            endif
2200        call FeQuestCrwOpen(nCrwJType(0),JAtP.eq.0)
            if(JAtP.eq.0) then
              call FeQuestButtonOpen(nButtOwn,ButtonOff)
              nRolMenuLast=0
            else
              call FeQuestButtonDisable(nButtOwn)
            endif
            do i=1,5
              if(NMenuJ(i).gt.0) then
                call FeQuestCrwOpen(nCrwJType(i),JAtP.eq.i)
              else
                call FeQuestCrwDisable(nCrwJType(i))
              endif
            enddo
            AtTypeMagP=AtTypeMag(LastAtom,KPhase)
            do i=1,5
              if(NMenuJ(i).gt.0) then
                IMenuJ(i)=
     1            max(LocateInStringArray(MenuJ(1,i),NMenuJ(i),
     2                                    AtTypeMagP,IgnoreCaseYes),1)
                if(i.eq.JAtP) then
                  call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                    NMenuJ(i),IMenuJ(i))
                  nRolMenuLast=nRolMenuJType(i)
                else
                  call FeQuestRolMenuDisable(nRolMenuJType(i))
                endif
              else
                IMenuJ(i)=0
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            enddo
          endif
        else
          call FeQuestButtonDisable(nButtOwn)
          call FeQuestCrwDisable(nCrwJType(0))
          do i=1,5
            call FeQuestCrwDisable(nCrwJType(i))
            call FeQuestRolMenuDisable(nRolMenuJType(i))
          enddo
        endif
        LastAtomOld=LastAtom
        if(Klic.eq.0) go to 9999
        if(CheckType.eq.EventCrw.and.
     1     CheckNumber.eq.nCrwActivateMagnetic) then
          Klic=0
          LastAtomOld=-1
          go to 2100
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.ge.nCrwJType(0).and.
     2           CheckNumber.le.nCrwJType(5))) then
          if(CheckNumber.eq.nCrwJType(0)) then
            call FeQuestButtonOff(nButtOwn)
            do i=1,5
              call FeQuestRolMenuDisable(nRolMenuJType(i))
            enddo
            RolMenuLast=0
            AtTypeMag(LastAtom,KPhase)=AtTypeMag(LastAtom,KPhase)
            AtTypeMagJ(LastAtom,KPhase)='own'
            call EM50ReadOwnFormFactorMag(LastAtom)
          else
            call FeQuestButtonDisable(nButtOwn)
            if(nRolMenuLast.gt.0)
     1        IMenuJ(nRolMenuLast-nRolMenuJType(1)+1)=
     2          RolMenuSelectedQuest(nRolMenuLast)
            do i=1,5
              if(CheckNumber.eq.nCrwJType(i)) then
                call FeQuestRolMenuOpen(nRolMenuJType(i),MenuJ(1,i),
     1                                  NMenuJ(i),IMenuJ(i))
                nRolMenuLast=nRolMenuJType(i)
                AtTypeMag(LastAtom,KPhase)=MenuJ(IMenuJ(i),i)
                if(i.eq.5) then
                  AtTypeMagJ(LastAtom,KPhase)='j0j2'
                  Veta='Magnetic_formfactor_<j0>+c<j2>'
                else
                  write(Cislo,'(i1)') 2*(i-1)
                  AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
                  Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
                endif
              else
                call FeQuestRolMenuDisable(nRolMenuJType(i))
              endif
            enddo
            call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1        AtTypeMag(LastAtom,KPhase),Veta,
     2        FFMag(1,LastAtom,KPhase),ich)
            Klic=0
            go to 2150
          endif
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
          if(LastAtomOld.ne.LastAtom) then
            MagneticAtom=AtTypeMag(LastAtom,KPhase).ne.' '
            Klic=0
            go to 2150
          endif
        else if(CheckType.eq.EventRolMenu.and.
     1          (CheckNumber.ge.nRolMenuJType(1).and.
     2           CheckNumber.le.nRolMenuJType(5))) then
          nRolMenuLast=CheckNumber
          i=nRolMenuLast-nRolMenuJType(1)+1
          AtTypeMag(LastAtom,KPhase)=
     1      MenuJ(RolMenuSelectedQuest(nRolMenuLast),i)
          if(i.eq.5) then
            AtTypeMagJ(LastAtom,KPhase)='j0j2'
            Veta='Magnetic_formfactor_<j0>+c<j2>'
          else
            write(Cislo,'(i1)') 2*(i-1)
            AtTypeMagJ(LastAtom,KPhase)='j'//Cislo(1:1)
            Veta='Magnetic_formfactor_<j'//Cislo(1:1)//'>'
          endif
          call MagFFFromAtomFile(AtType(LastAtom,KPhase),
     1      AtTypeMag(LastAtom,KPhase),Veta,
     2      FFMag(1,LastAtom,KPhase),ich)
          Klic=0
          go to 2150
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMagnetic)
     1    then
          MagneticAtom=CrwLogicQuest(nCrwMagnetic)
          if(.not.MagneticAtom) then
            AtTypeMag(LastAtom,KPhase)=' '
            AtTypeMagJ(LastAtom,KPhase)=' '
          endif
          LastAtomOld=-1
          Klic=0
          go to 2150
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOwn)
     1    then
          call EM50ReadOwnFormFactorMag(LastAtom)
        endif
      else
        call FeQuestCrwClose(nCrwMagnetic)
        call FeQuestRolMenuClose(nRolMenuAtType)
        call FeQuestCrwClose(nCrwJType(0))
        call FeQuestButtonClose(nButtOwn)
        do i=1,5
          call FeQuestRolMenuClose(nRolMenuJType(i))
          call FeQuestCrwClose(nCrwJType(i))
        enddo
      endif
      go to 9999
9999  return
      end
