      subroutine EM50ReadAllMultipoles
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,NAtFormula(KPhase)
        if(AllowChargeDensities.and.ChargeDensities) then
          call RealAFromAtomFile(AtType(i,KPhase),'ZSlater',
     1                           ZSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'NSlater',
     1                          NSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'PopCore',
     1                          PopCore(1,i,KPhase),28,ich)
          call IntAFromAtomFile(AtType(i,KPhase),'PopVal',
     1                          PopVal(1,i,KPhase),28,ich)
        endif
      enddo
      return
      end
