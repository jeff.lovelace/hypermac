      subroutine EM50ReadAllFormFactors
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeRGBCompress,npom(3)
      i1=1
      i2=NAtFormula(KPhase)
      go to 1000
      entry EM50ReadOneFormFactor(n)
      i1=n
      i2=n
1000  do i=i1,i2
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atweight',
     1                        AtWeight(i,KPhase),0,ich)
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atradius',
     1                        AtRadius(i,KPhase),0,ich)
        call IntAFromAtomFile(AtTypeFull(i,KPhase),'color',npom,3,ich)
        AtColor(i,KPhase)=
     1    FeRGBCompress(npom(1),npom(2),npom(3))
        if(AllowChargeDensities.and.ChargeDensities) then
          call RealAFromAtomFile(AtTypeFull(i,KPhase),'ZSlater',
     1                           ZSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'NSlater',
     1                          NSlater(1,i,KPhase),8,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'PopCore',
     1                          PopCore(1,i,KPhase),28,ich)
          call IntAFromAtomFile(AtTypeFull(i,KPhase),'PopVal',
     1                          PopVal(1,i,KPhase),28,ich)
        endif
      enddo
      return
      end
