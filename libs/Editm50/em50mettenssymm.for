      subroutine EM50MetTensSymm(MetTens6NewI)
      use Basic_mod
      dimension GSuma(36),rmm(36),gpp(36)
      real MetTens6NewI(*)
      include 'fepc.cmn'
      include 'basic.cmn'
      call SetRealArrayTo(GSuma,36,0.)
      do i=1,NSymm(KPhase)
        call trmat(rm6(1,i,1,KPhase),rmm,NDim(KPhase),NDim(KPhase))
        call multm(rm6(1,i,1,KPhase),MetTens6NewI,gpp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call Cultm(gpp,rmm,GSuma,NDim(KPhase),NDim(KPhase),NDim(KPhase))
      enddo
      do j=1,NDimQ(KPhase)
        MetTens6NewI(j)=GSuma(j)/float(NSymm(KPhase))
      enddo
      return
      end
