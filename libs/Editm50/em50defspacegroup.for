      subroutine EM50DefSpaceGroup(Klic,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      ich=0
      id=NextQuestId()
      xqd=600.-2.*KartSidePruh
      il=14
      Veta='Define '
      k=idel(Veta)+2
      if(NDim(KPhase).gt.3) then
        Veta(k:)='super'
        k=idel(Veta)+1
      endif
      Veta(k:)='space group'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,0)
      call EM50SymmetryMake(id,1)
      call EM50SymmetryRefresh
      ErrFlag=0
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call EM50SymmetryCheck
        if(ErrFlag.eq.0) call EM50SymmetryRefresh
        go to 1500
      endif
      if(ich.eq.0) then
        if(NUnits(KPhase).le.0)
     1    NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        if(Klic.eq.0) then
          call QuestionRewriteFile(50)
          call iom50(0,0,fln(:ifln)//'.m50')
        endif
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'.l52')
      return
      end
