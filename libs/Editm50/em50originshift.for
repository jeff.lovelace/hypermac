      subroutine EM50OriginShift(Shift,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6),Shift(*)
      if(Klic.eq.0) then
        i1=1
        i2=NSymm(KPhase)
      else
        i1=Klic
        i2=Klic
      endif
      do i=i1,i2
        call multm(rm6(1,i,1,KPhase),Shift,xp,NDim(KPhase),
     1             NDim(KPhase),1)
        do j=1,NDim(KPhase)
          s6(j,i,1,KPhase)=s6(j,i,1,KPhase)-Shift(j)+xp(j)
        enddo
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      return
      end
