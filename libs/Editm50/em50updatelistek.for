      subroutine EM50UpdateListek(KartIdOld)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      logical lpom
      lpom=BlockedActiveObj
      BlockedActiveObj=.true.
      if(KartId.eq.KartIdSymmetry) then
        call Em50SymmetryRefresh
      else if(KartId.eq.KartIdComposition) then
        call EM50CompositionRefresh
      else if(KartId.eq.KartIdMultipols) then
        call EM50MultipoleRefresh
      else if(KartId.eq.KartIdMagnetic) then
        call EM50MagneticRefresh
      endif
      BlockedActiveObj=lpom
      return
      end
