      subroutine TRANSO(nn,l1,z,s,ff)
      include 'fepc.cmn'
      dimension ff(9),a(28)
      data a(1)/0.0/
      d=s**2+z**2
      a(2)=1./d
      n=nn-1
      tz=z+z
      ts=s+s
      do ll=1,l1
        l=ll-1
        if(ll.eq.1) go to 1000
        a(ll+1)=a(ll)*ts*float(l)/d
        a(ll)=0.0
1000    do nx=ll,n
          i1=nx
          i2=nx+1
          i3=i2+1
          a(i3)=(tz*float(nx)*a(i2)-float((nx+l)*(nx-ll))*a(i1))/d
        enddo
        ff(ll)=a(i3)
      enddo
      return
      end
