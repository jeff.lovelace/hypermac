      subroutine TestGrup2
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrM(3,3),RmP(3,3),s(6),ShSgO(3),TrMi(3,3),QuSymm(3),
     1          QuOld(3)
      character*256 Veta,Radka
      character*8 GrupaO,GrupaN
      NDim(KPhase)=3
      NDimI(1)=NDim(KPhase)-3
      NDimQ(1)=NDim(KPhase)**2
      KPhase=1
      call SetRealArrayTo(TrM,9,0.)
      call SetRealArrayTo(s,6,0.)

      TrM(1,1)= 1.
      TrM(1,2)=-1.
      TrM(2,1)= 1.
      TrM(3,3)=1.

      call MatInv(TrM,TrMi,pom,3)
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call FeLstMake(-1.,-1.,65,30,' ')
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      n=0
1100  n=n+1
      read(45,FormA256,end=5000) Radka
      read(Radka,FormSG) isg,ipg,i,Grupa(KPhase)
      if(isg.lt.143) go to 1100
      close(45)
      n=n-1
1200  n=n+1
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      do i=1,n
        read(45,FormA256,end=5000) Radka
        read(Radka,'(2i3,4x,a8)') isg,ipg,Grupa(KPhase)
      enddo
      if(isg.ge.195) go to 5000
      close(45)
      Monoclinic(KPhase)=mod(n,3)+1
      GrupaO=Grupa(KPhase)
      write(Radka,'('' Grupa : '',a)') GrupaO(:idel(GrupaO))
      call FeLstWriteLine(Radka,-1)
      write(Radka,'(80a1)') ' ',('=',i=1,8+idel(GrupaO))
      call FeLstWriteLine(Radka,-1)
      call SetRealArrayTo(ShSG,3,0.)
      call SetRealArrayTo(ShSGO,3,0.)
      call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,ich)
      do i=1,NSymm(1)
        call multm(trm,rm6(1,i,1,1),rmp,3,3,3)
        call multm(rmp,trmi,rm6(1,i,1,1),3,3,3)
        call multm(trm,s6(1,i,1,1),s,3,3,1)
        call od0do1(s,s,3)
        call CopyVek(s,s6(1,i,1,1),3)
      enddo
      do i=1,NLattVec(KPhase)
        call multm(trm,vt6(1,i,1,1),s,3,3,1)
        call od0do1(s,s,3)
        call CopyVek(s,vt6(1,i,1,1),3)
      enddo
      if(NDim(KPhase).eq.4) then
        call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
        call CopyVek(QuSymm,Qu(1,1,1,KPhase),3)
      endif
      call FindSmbSg(GrupaN,ChangeOrderNo,1)
      if(NDim(KPhase).eq.4)
     1  call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
      write(Radka,'('' Grupa nactena : '',a8,'' Shift:'',3f8.3)')
     1      GrupaO,(ShSgO(i),i=1,3)
      call FeLstWriteLine(Radka,-1)
      write(Radka,'('' Grupa zjistena: '',a8,'' Shift:'',3f8.3)')
     1      GrupaN,(ShSg(i,1),i=1,3)
      call FeLstWriteLine(Radka,-1)
      if(GrupaN.ne.GrupaO) pause

      go to 1200
5000  call FeLstRemove
      return
      end
