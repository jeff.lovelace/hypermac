      subroutine EM50Symmetry
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      include 'datred.cmn'
      dimension rmp(36),sp(6),nx(4),ni(4),QuSymm(3,3),QuOld(3),
     1          QuPom(3,3),gpp(9),gppi(9)
      dimension rm6o(:,:),s6o(:,:),vt6o(:,:)
      allocatable rm6o,s6o,vt6o
      character*256 EdwStringQuest,SbwStringQuest
      character*80 Veta,VetaArr(2)
      character*60 GrupaOld,GrupaNew,Grp,GrpOut
      character*43 ::
     1  Text1='The operators derived from the group symbol',
     2  Text2='The group symbol derived from the operators'
      character*20 GrFull,GrOrigin,StQ(20)
      character*15 SymmCodePom(6)
      character*10 GrPerm
      character*8  GrShort
      character*6  GrPoint
      character*5  GrNum
      character*4  GrX4
      character*3  StQA(3,20),StP
      character*1  SmbL(8),SmbQ(3)
      integer ZeSymbolu,Zvenku,RolMenuSelectedQuest,UseTabsIn,
     1        SbwItemPointerQuest,SbwLnQuest
      logical eqrv,NicTamNeni,CrwLogicQuest,FirstTime,EqIgCase,
     1        AskForDelta,FeYesNoHeader,Change,FromEditM50,GrupaZListu,
     2        QCompatible,lpom
      real MetTens6NewI(36)
      save nEdwGrupa,nEdwShift,nEdwOperator,nButtSGList,
     1     nRolMenuCellCentr,nButtComplete,nButtDelete,nButtClean,
     2     nButtAdd,nButtRewrite,nButtRunTest,nButtLoad,nSbwSymmCodes,
     3     ZeSymbolu,ItemSel,nLblText,FromEditM50,IdSave,NDimShift,
     4     nButtLocalSymm,nButtForStokes
      data SmbL/'P','A','B','C','I','R','F','X'/,SmbQ/'a','b','g'/,
     1     GrupaOld/' '/
      equivalence (Veta,VetaArr(1))
      entry EM50SymmetryMake(id,Key)
      FromEditM50=Key.eq.0
      if(.not.FromEditM50) IdSave=id
      GrupaOld=Grupa(KPhase)
      if(NDim(KPhase).ne.4) then
        Veta='Space %group'
      else
        Veta='Superspace %group'
      endif
      il=1
      VetaArr(2)='%Select from list'
      dpom=FeTxLengthUnder(VetaArr(2))+10.
      tpomp=5.
      xpomp=FeTxLengthUnder(Veta)+15.
      dpomp=QuestXLen(id)-xpomp-dpom-15.
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpomp,EdwYd,1)
      nEdwGrupa=EdwLastMade
      NicTamNeni=index(Grupa(KPhase),'?').gt.0
      Veta=VetaArr(2)
      tpom=xpomp+dpomp+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtSGList=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      tpom=tpomp
      Veta='%Origin shift'
      xpom=xpomp
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwShift=EdwLastMade
      NDimShift=NDim(KPhase)
      if(NDimShift.gt.4) NDimShift=3
      call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                         NicTamNeni,.true.)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,QuestXLen(id)*.5,il,' ','C','N')
      nLblText=LblLastMade
      if(NicTamNeni) then
        ZeSymbolu=0
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
        if(Grupa(KPhase).eq.' ') then
          ZeSymbolu=0
        else
          call FeQuestLblChange(nLblText,Text1)
          ZeSymbolu=1
        endif
      endif
      xpom=5.
      dpom=300.
      call FeQuestSbwMake(id,xpom,15,dpom,11,1,CutTextFromRight,
     1                    SbwVertical)
      nSbwSymmCodes=SbwLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l52','formatted','unknown')
      do i=1,NSymm(KPhase)
        call MakeSymmSt(Veta,symmc(1,i,1,KPhase))
        if(MagneticType(KPhase).gt.0.and..not.ParentStructure) then
          if(ZMag(i,1,KPhase).gt.0) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(Cislo,100) i
        call Zhusti(Cislo)
        write(ln,FormA1)(Cislo(j:j),j=1,idel(Cislo)),' ',
     1                  (Veta(j:j),j=1,idel(Veta))
      enddo
      close(ln)
      call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'.l52')
      tpom=xpom+dpom+30.
      tpomp=tpom
      il=5
      Veta='%Load ->'
      dpom=FeTxLengthUnder(Veta)+5.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtLoad=ButtonLastMade
      xpom=tpom+dpom+15.
      dpom=QuestXLen(id)-xpom-5.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwOperator=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      Veta='<- %Add'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdd=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='<- %Rewrite'
      xpom=xpom+dpom+20.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRewrite=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      tpom=tpom+15.
      Veta='%Delete operator'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='%Clean out'
      xpom=tpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtClean=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
      il=il+1
      Veta='C%ell centering'
      dpom=30.+EdwYd
      xpom=tpomp+FeTxLength(Veta)+10.
      call FeQuestRolMenuMake(id,tpomp,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuCellCentr=RolMenuLastMade
      i=LocateInStringArray(SmbL,8,Lattice(KPhase),IgnoreCaseYes)
      call FeQuestRolMenuOpen(RolMenuLastMade,SmbL,8,i)
      xpom=xpom+dpom+5.
      il=il+1
      call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
      il=il+1
      Veta='Com%plete the set'
      dpom=220.
      xpom=(tpomp+QuestXLen(id)-5.-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtComplete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRunTest=0
      nButtForStokes=0
      if(.not.FromEditM50.or.NRefBlock.eq.1) then
        if(DifCode(KRefBlock).lt.100) then
          il=il+1
          Veta='%Make test'
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          nButtRunTest=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestMouseToButton(ButtonLastMade)
          if(.not.ExistM95) call FeQuestButtonDisable(ButtonLastMade)
        endif
      endif
      if(NDimI(KPhase).gt.0) then
        il=il+1
        Veta='Run Stokes & Camp%bell SSG-test'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtForStokes=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestMouseToButton(ButtonLastMade)
      endif
      if(FromEditM50) then
        il=il+1
        call FeQuestLinkaFromToMake(id,tpomp,QuestXLen(id)-5.,il)
        il=il+1
        Veta='De%fine local symmetry operators'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtLocalSymm=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtLocSymm=0
      endif
      go to 9999
      entry EM50SymmetryRefresh
      i=nSbwSymmCodes+SbwFr-1
      ItemFromOld=SbwItemFrom(i)
      ItemSelOld=SbwItemPointer(i)
      call CloseIfOpened(SbwLn(i))
      ln=NextLogicNumber()
      call OpenFile(ln,SbwFile(i),'formatted','unknown')
      do j=1,NSymm(KPhase)
        call MakeSymmSt(Veta,symmc(1,j,1,KPhase))
        if(MagneticType(KPhase).gt.0.and..not.ParentStructure) then
          if(ZMag(j,1,KPhase).gt.0) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(Cislo,100) j
        call Zhusti(Cislo)
        write(ln,FormA1)(Cislo(k:k),k=1,idel(Cislo)),' ',
     1                  (Veta(k:k),k=1,idel(Veta))
      enddo
      close(ln)
      call FeQuestSbwMenuOpen(nSbwSymmCodes,SbwFile(i))
      call FeQuestSbwShow(nSbwSymmCodes,ItemFromOld)
      call FeQuestSbwItemOff(nSbwSymmCodes,SbwItemFrom(i))
      call FeQuestSbwItemOn(nSbwSymmCodes,ItemSelOld)
      n=RolMenuSelectedQuest(nRolMenuCellCentr)
      i=LocateInStringArray(SmbL,8,Lattice(KPhase),IgnoreCaseYes)
      if(i.ne.n) call FeQuestRolMenuOpen(nRolMenuCellCentr,SmbL,8,i)
      if(Grupa(KPhase).eq.' '.or.index(Grupa(KPhase),'?').gt.0) then
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      if(NSymm(KPhase).gt.0) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtLoad,i)
      go to 9999
      entry EM50SymmetryCheck
      if(FromEditM50) then
        id=KartId
      else
        id=IdSave
      endif
      GrupaZListu=.false.
2000  if((CheckType.eq.EventEdw.and.
     1    (CheckNumber.eq.nEdwGrupa.or.CheckNumber.eq.nEdwShift)).or.
     3   GrupaZListu) then
        Grupa(KPhase)=EdwStringQuest(nEdwGrupa)
        if(Grupa(KPhase).eq.' ') go to 3250
        if(CheckNumber.ne.nEdwGrupa.or.
     1     EventTypeSave.eq.EventCrw.or.
     2     EventTypeSave.eq.EventRolMenu.or.
     3     (EventTypeSave.eq.EventButton.and.
     4      EventNumberSave.eq.nButtClean)) then
          AskForDelta=.false.
        else
          AskForDelta=.true.
        endif
        if(GrupaZListu) AskForDelta=.true.
        if(EdwStringQuest(nEdwShift).eq.' ') then
          call SetRealArrayTo(shsg(1,KPhase),NDim(KPhase),0.)
          call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                             .false.,.true.)
        endif
        call FeQuestRealAFromEdw(nEdwShift,shsg(1,KPhase))
        StdSg(KPhase)=1
        do i=1,NDim(KPhase)
          if(abs(shsg(i,KPhase)).gt..0001) then
            StdSg(KPhase)=0
            go to 2015
          endif
        enddo
2015    Grupa(KPhase)=EdwStringQuest(nEdwGrupa)
        FirstTime=.not.EqIgCase(Grupa(KPhase),GrupaOld).and.
     1            .not.GrupaZListu
        if(FromEditM50.and.(EventType.eq.EventKartSw.and.
     1      EventNumber.eq.KartIdCell+1-KartFirstId))
     2    QuestCheck(id)=-1
        GrupaOld=Grupa(KPhase)
        call EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
        if(ich.eq.0) then
          call EM50MakeStandardOrder
          call CrlOrderMagSymmetry
          if(Grupa(KPhase).ne.' '.and.ZeSymbolu.ne.1) then
            ZeSymbolu=1
            call FeQuestLblChange(nLblText,Text1)
          endif
        else
          EdwLastCheck=1
          if(QuestCheck(Id).ne.-1) then
            if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwGrupa.or.CheckNumber.eq.nEdwShift))
     2        then
              EventType=EventEdw
              EventNumber=nEdwGrupa
            endif
            ErrFlag=1
          endif
          GrupaOld=' '
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwOperator)
     1  then
        Veta=EdwStringQuest(CheckNumber)
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuCellCentr) then
        n=RolMenuSelectedQuest(nRolMenuCellCentr)
        go to 3100
      else if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtSGList) then
          UseTabsIn=UseTabs
          UseTabs=NextTabs()
          xpom=0.
          do i=1,4
            if(i.eq.1) then
              pom=4.
            else if(i.eq.2) then
              pom=8.
            else if(i.eq.3) then
              pom=12.
            else if(i.eq.4) then
              pom=7.
            endif
            xpom=xpom+pom*PropFontWidthInPixels
            call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
            if(i.eq.4) then
              xpom=xpom+5.*PropFontWidthInPixels
            else
              xpom=xpom+PropFontWidthInPixels
            endif
            call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
          enddo
          idp=NextQuestId()
          xqdp=xpom+100.
          Veta='Select space group'
          il=20
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
          il=1
          tpom=15.
          call FeQuestLblMake(idp,tpom,il,'#','L','N')
          tpom=tpom+4.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Short symbol','L','N')
          tpom=tpom+9.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Full symbol','L','N')
          tpom=tpom+14.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Permutation','L','N')
          tpom=tpom+10.*PropFontWidthInPixels
          call FeQuestLblMake(idp,tpom,il,'Origin at','L','N')
          xpom=5.
          dpom=xqdp-10.-SbwPruhXd
          il=20
          call FeQuestSbwMake(idp,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                        SbwVertical)
          nSbw=SbwLastMade
          call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
          lni=NextLogicNumber()
          if(OpSystem.le.0) then
            Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1           'spgroup_list.dat'
          else
            Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_list.dat'
          endif
          call OpenFile(lni,Veta,'formatted','old')
          if(ErrFlag.ne.0) go to 9999
          read(lni,FormA) Veta
          if(Veta(1:1).ne.'#') rewind lni
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'_list.tmp','formatted',
     1                  'unknown')
2100      read(lni,101,end=2120) GrNum,GrShort,GrFull,GrPerm,GrPoint,
     1                           GrOrigin
          Cislo=GrNum
          i=index(Cislo,'/')
          if(i.gt.0) Cislo(i:)=' '
          call Posun(Cislo,0)
          read(Cislo,FormI15) isg
          if(NDimI(KPhase).eq.1.and.isg.ge.195) go to 2120
          write(lno,'(17(a))')
     1      GrNum(:idel(GrNum)),Tabulator,'|',Tabulator,
     2      GrShort(:idel(GrShort)),Tabulator,'|',Tabulator,
     3      GrFull(:idel(GrFull)),Tabulator,'|',Tabulator,
     4      GrPerm(:max(idel(GrPerm),1)),Tabulator,'|',Tabulator,
     5      GrPoint(:max(idel(GrPoint),1))
          go to 2100
2120      call CloseIfOpened(lno)
          call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_list.tmp')
2200      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventSbwDoubleClick) then
            ich=0
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2200
          endif
          if(ich.eq.0) then
            n=SbwItemPointerQuest(nSbw)
            rewind lni
            read(lni,FormA) Veta
            if(Veta(1:1).ne.'#') rewind lni
            do i=1,n
              read(lni,101,end=2120) GrNum,GrShort,GrFull,GrPerm,
     1                               GrPoint,GrOrigin
            enddo
            Cislo=GrNum
            i=index(Cislo,'/')
            if(i.gt.0) Cislo(i:)=' '
            call Posun(Cislo,0)
            read(Cislo,FormI15) isg
            if(isg.ge.3.and.isg.le.15) then
              k=0
              call Kus(GrFull,k,Cislo)
              do i=1,3
                call Kus(GrFull,k,Cislo)
                if(.not.EqIgCase(Cislo,'1')) then
                  Monoclinic(KPhase)=i
                  exit
                endif
              enddo
            endif
            if(NDimI(KPhase).eq.1) then
              i=index(GrNum,'/')
              if(i.gt.0) GrNum(i:)=' '
              call Posun(GrNum,0)
              read(GrNum,102) isg
              if(isg.le.2) then
                Veta=GrShort(:idel(GrShort))//'(abg)0'
                go to 2250
              else if(isg.le.74) then
                nq=0
                if(EqIgCase(GrShort(1:1),'P')) then
                  StP='1/2'
                else
                  StP='1'
                endif
                if(isg.le.15) then
                  nq=nq+1
                  call CopyStringArray(SmbQ,StQA(1,nq),3)
                  StQA(Monoclinic(KPhase),nq)='0'
                  nq=nq+1
                  call CopyStringArray(StQA(1,1),StQA(1,nq),3)
                  StQA(Monoclinic(KPhase),nq)=StP
                  ip=Monoclinic(KPhase)
                  ik=Monoclinic(KPhase)
                else
                  ip=1
                  ik=3
                endif
                do i=ip,ik
                  i1=mod(i,3)+1
                  i2=6-i1-i
                  nq=nq+1
                  call SetStringArrayTo(StQA(1,nq),3,'0')
                  StQA(i,nq)=SmbQ(i)
                  nqp=nq
                  do j=1,3
                    nq=nq+1
                    call CopyStringArray(StQA(1,nqp),StQA(1,nq),3)
                    if(j.eq.1) then
                      StQA(i1,nq)=StP
                    else if(j.eq.2) then
                      StQA(i2,nq)=StP
                    else if(j.eq.3) then
                      StQA(i1,nq)=StP
                      StQA(i2,nq)=StP
                    endif
                  enddo
                enddo
                do j=1,nq
                  StQ(j)='('
                  do i=1,3
                    StQ(j)=StQ(j)(:idel(StQ(j)))//
     1                     StQA(i,j)(:idel(StQA(i,j)))
                  enddo
                  StQ(j)=StQ(j)(:idel(StQ(j)))//')'
                enddo
              else if(isg.le.142) then
                StQ(1)='(00g)'
                StQ(2)='(1/21/2g)'
                nq=2
              else if(isg.le.167) then
                StQ(1)='(00g)'
                StQ(2)='(1/31/3g)'
                nq=2
              else
                StQ(1)='(00g)'
                nq=1
              endif
              if(isg.le. 9.or.
     1          (isg.ge. 75.and.isg.le. 82).or.
     2          (isg.ge.143.and.isg.le.148).or.
     3          (isg.ge.168.and.isg.le.174)) then
                nn=1
              else if((isg.ge. 10.and.isg.le. 15).or.
     1                (isg.ge. 83.and.isg.le. 88).or.
     2                (isg.ge.175.and.isg.le.176).or.
     2                 GrShort(1:1).eq.'R') then
                nn=2
              else if((isg.ge. 16.and.isg.le. 74).or.
     1                (isg.ge. 89.and.isg.le.122).or.
     2                (isg.ge.149.and.isg.le.167).or.
     3                (isg.ge.177.and.isg.le.190)) then
                nn=3
              else
                nn=4
              endif
              if(iPGSel.le.15) then
                m=3
              else
                m=5
              endif
              if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
              allocate(rm6o(NDimQ(KPhase),NSymm(KPhase)),
     1                 s6o(NDim(KPhase),NSymm(KPhase)),
     2                 vt6o(NDim(KPhase),NLattVec(KPhase)))
              nso=NSymm(KPhase)
              nvto=NLattVec(KPhase)
              do i=1,NLattVec(KPhase)
                call CopyVek(vt6(1,i,1,KPhase),vt6o(1,i),NDim(KPhase))
              enddo
              do i=1,NSymm(KPhase)
                call CopyMat(rm6(1,i,1,KPhase),rm6o(1,i),NDim(KPhase))
                call CopyVek(s6(1,i,1,KPhase),s6o(1,i),NDim(KPhase))
              enddo
              lno=NextLogicNumber()
              call OpenFile(lno,fln(:ifln)//'_list_all.tmp','formatted',
     1                      'unknown')
              if(ErrFlag.ne.0) go to 9999
              lnq=NextLogicNumber()
              call OpenFile(lnq,fln(:ifln)//'_list_q.tmp','formatted',
     1                      'unknown')
              if(ErrFlag.ne.0) go to 9999
              call SetIntArrayTo(nx,nn,m)
              n=m**nn
              call SetIgnoreWTo(.true.)
              call SetIgnoreETo(.true.)
              ns4=0
              ns4q=0
              do iq=1,nq
                i=idel(StQ(iq))
                Grp=GrShort(:idel(GrShort))//StQ(iq)(:i)
                idl=idel(Grp)
                QCompatible=.false.
                Cislo=StQ(iq)(2:i-1)
                call mala(Cislo)
                do i=1,3
                  if(idel(Cislo).le.0) go to 2240
                  k=1
                  if(Cislo(k:k).eq.SmbQ(i)) k=k+1
                  k=k-1
                  if(k.eq.0) then
                    j=index(Cislo,'/')
                    if(j.le.0.or.j.gt.3.or.
     1                 (j.eq.3.and.Cislo(1:1).ne.'-')) then
                      k=1
                    else
                      k=j+1
                    endif
                    pom=fract(Cislo(:k),ich)
                    if(ich.ne.0) go to 2240
                    if(abs(pom-Qu(i,1,1,KPhase)).gt..001) go to 2240
                  endif
                  Cislo=Cislo(k+1:)
                enddo
                QCompatible=.true.
2240            do i=1,n
                  call RecUnPack(i,ni,nx,nn)
                  GrX4=' '
                  do j=1,nn
                    k=ni(j)
                    GrX4(j:j)=SmbSymmT(k)
                  enddo
                  Grupa(KPhase)=Grp(:idl)//GrX4(:nn)
                  call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,
     1                            QuSymm,ichp)
                  if(ichp.ne.0) cycle
                  if(NDim(KPhase).eq.4) then
                    call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
                    call CopyVek(QuSymm,Qu(1,1,1,KPhase),
     1                           3*NDimI(KPhase))
                  endif
                  call FindSmbSgOrgShiftNo(GrpOut,.false.,1)
                  if(NDim(KPhase).eq.4)
     1              call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
                  do j=1,idel(GrpOut)
                    if(EqIgCase(GrpOut(j:j),'e'))
     1                GrpOut(j:j)=Grupa(KPhase)(j:j)
                  enddo
                  if(EqIgCase(GrpOut,Grupa(KPhase))) then
                    ns4=ns4+1
                    if(ns4.eq.1) Veta=Grupa(KPhase)
                    write(lno,'(a)') Grupa(KPhase)(:idel(Grupa(KPhase)))
                    if(QCompatible) then
                      ns4q=ns4q+1
                      write(lnq,'(a)')
     1                  Grupa(KPhase)(:idel(Grupa(KPhase)))
                    endif
                  endif
                enddo
              enddo
              NSymm(KPhase)=nso
              NLattVec(KPhase)=nvto
              do i=1,NLattVec(KPhase)
                call CopyVek(vt6o(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
              enddo
              do i=1,NSymm(KPhase)
                call CopyMat(rm6o(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
                call CopyVek(s6o(1,i),s6(1,i,1,KPhase),NDim(KPhase))
                call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                        symmc(1,i,1,KPhase),0)
              enddo
              Grupa(KPhase)=GrupaOld
              Lattice(KPhase)=GrupaOld(1:1)
              if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
            else
              lnq=0
              Veta=GrShort
            endif
          endif
2250      call FeQuestRemove(idp)
          call CloseIfOpened(lni)
          call CloseIfOpened(lno)
          call CloseIfOpened(lnq)
          call SetIgnoreWTo(.false.)
          call SetIgnoreETo(.false.)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsIn
          if(ich.eq.0) then
            if(NDimI(KPhase).eq.1.and.isg.ge.3) then
              if(ns4.gt.1) then
                idp=NextQuestId()
                xqdp=250.
                Veta='Select superspace group'
                ilk=min(ifix((float(ns4)*MenuLineWidth+2.)/
     1                        QuestLineWidth)+1,14)
                if(ns4q.gt.0) ilk=ilk+2
                call FeQuestCreate(idp,-1.,-1.,xqdp,ilk,Veta,0,
     1                             LightGray,0,0)
                xpom=5.
                dpom=xqdp-10.-SbwPruhXd
                il=ilk
                if(ns4q.gt.0) il=il-2
                call FeQuestSbwMake(idp,xpom,il,dpom,il,1,
     1                              CutTextFromLeft,SbwVertical)
                nSbw=SbwLastMade
                call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
                call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                  '_list_all.tmp')
                if(ns4q.gt.0) then
                  xpom=5.
                  tpom=xpom+CrwgXd+5.
                  do i=1,2
                    il=il+1
                    if(i.eq.1) then
                      Veta='Show %relevant superspace groups'
                    else
                      nCrwFirst=CrwLastMade
                      Veta='Show q-%compatible superspace groups'
                    endif
                    call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',
     1                                  CrwgXd,CrwgYd,1,1)
                    call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                  enddo
                else
                  nCrwFirst=0
                endif
2300            call FeQuestEvent(idp,ichp)
                if(CheckType.eq.EventCrw) then
                  call CloseIfOpened(SbwLnQuest(nSbw))
                  if(CheckNumber.eq.nCrwFirst) then
                    call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                      '_list_all.tmp')
                  else
                    call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//
     1                                      '_list_q.tmp')
                  endif
                  go to 2300
                else if(CheckType.eq.EventSbwDoubleClick) then
                  ichp=0
                else if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 2300
                endif
                if(ichp.eq.0) then
                  n=SbwItemPointerQuest(nSbw)
                  if(ns4q.gt.0) lpom=CrwLogicQuest(nCrwFirst)
                endif
                call FeQuestRemove(idp)
                if(ichp.eq.0) then
                  if(ns4q.gt.0) then
                    if(lpom) then
                      Veta=fln(:ifln)//'_list_all.tmp'
                    else
                      Veta=fln(:ifln)//'_list_q.tmp'
                    endif
                  else
                    Veta=fln(:ifln)//'_list_all.tmp'
                  endif
                  call OpenFile(lno,Veta,'formatted','unknown')
                  do i=1,n
                    read(lno,FormA) Veta
                  enddo
                  call CloseIfOpened(lno)
                else
                  go to 9999
                endif
              else if(ns4.le.0) then
                call FeUnforeseenError('No corresponding superspace '//
     1                                 'group found.')
                go to 9999
              endif
            endif
            call DeleteFile(fln(:ifln)//'_list_all.tmp')
            call DeleteFile(fln(:ifln)//'_list_q.tmp')
            GrupaZListu=.true.
            call FeQuestStringEdwOpen(nEdwGrupa,Veta)
            call SetRealArrayTo(shsg(1,KPhase),NDim(KPhase),0.)
            n=idel(GrOrigin)
            if(n.gt.0) then
              do i=1,n
                if(GrOrigin(i:i).eq.',') GrOrigin(i:i)=' '
              enddo
              k=0
              call StToReal(GrOrigin,k,shsg(1,KPhase),3,.false.,ich)
            endif
            call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                               .false.,.true.)
            go to 2000
          else
            go to 9999
          endif
        else if(CheckNumber.eq.nButtComplete) then
          Zvenku=0
          go to 3000
        else if(CheckNumber.eq.nButtDelete) then
          i=nSbwSymmCodes+SbwFr-1
          ItemSel=SbwItemPointer(i)
          if(ItemSel.ne.1) then
            do i=ItemSel+1,NSymm(KPhase)
              call CopySymmOperator(i,i-1,1)
            enddo
            NSymm(KPhase)=NSymm(KPhase)-1
          endif
          go to 3250
        else if(CheckNumber.eq.nButtClean) then
          NSymm(KPhase)=1
          go to 3250
        else if(CheckNumber.eq.nButtAdd.or.CheckNumber.eq.nButtRewrite)
     1    then
          Veta=EdwStringQuest(nEdwOperator)
          call mala(Veta)
          if(Veta.ne.' ') then
            call ReadSymm(Veta,rmp,sp,SymmCodePom,ZMagP,0)
            if(ErrFlag.ne.0) go to 2550
            call od0do1(sp,sp,NDim(KPhase))
            do i=1,NSymm(KPhase)
              if(eqrv(sp,s6(1,i,1,KPhase),NDim(KPhase),.0001).and.
     1           eqrv(rmp,rm6(1,i,1,KPhase),NDimQ(KPhase),.001)) then
                if(abs(ZMagP-ZMag(i,1,KPhase)).lt..001) go to 2500
              endif
            enddo
            i=nSbwSymmCodes+SbwFr-1
            ItemFromOld=SbwItemFrom(i)
            ItemSelOld=SbwItemPointer(i)
            call CloseIfOpened(SbwLn(i))
            if(CheckNumber.eq.nButtRewrite) then
              ItemSel=ItemSelOld
              if(ItemSel.eq.1) go to 2600
            else
              NSymm(KPhase)=NSymm(KPhase)+1
              call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1          NLattVec(KPhase),NComp(KPhase),NPhase,1)
              ISwSymm(NSymm(KPhase),1,KPhase)=1
              ItemSel=NSymm(KPhase)
              write(Cislo,100) ItemSel
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' '//Veta(:idel(Veta))
              call AppendFile(SbwFile(i),VetaArr,1)
            endif
            call FeQuestStringEdwOpen(nEdwOperator,' ')
            write(Cislo,100) ItemSel
            call Zhusti(Cislo)
            Veta=Cislo(:idel(Cislo))//' '//Veta(:idel(Veta))
            if(CheckNumber.eq.nButtRewrite) then
              call RewriteLinesOnFile(SbwFile(i),ItemSelOld,
     1                                ItemSelOld,VetaArr,1)
            else
              call AppendFile(SbwFile(i),VetaArr,1)
            endif
            call CopyMat(rmp,rm6(1,ItemSel,1,KPhase),NDim(KPhase))
            call CopyVek(sp,s6(1,ItemSel,1,KPhase),NDim(KPhase))
            call CopyStringArray(SymmCodePom,symmc(1,ItemSel,1,KPhase),
     1                           NDim(KPhase))
            ZMag(ItemSel,1,KPhase)=ZMagP
            call FeQuestSbwMenuOpen(nSbwSymmCodes,SbwFile(i))
            call FeQuestSbwShow(nSbwSymmCodes,ItemFromOld)
            call FeQuestSbwItemOff(nSbwSymmCodes,SbwItemFrom(i))
            call FeQuestSbwItemOn(nSbwSymmCodes,ItemSelOld)
            go to 2600
          endif
          go to 2550
2500      call FeChybne(-1.,-1.,'the symmetry operator already present',
     1                  'try again.',SeriousError)
2550      EventNumber=nEdwOperator
          EventType=EventEdw
          go to 9999
        else if(CheckNumber.eq.nButtLoad) then
          i=nSbwSymmCodes+SbwFr-1
          ItemSel=SbwItemPointer(i)
          Veta=SbwStringQuest(ItemSel,i)
          k=0
          call kus(Veta,k,Cislo)
          Veta=Veta(k+1:)
          call FeQuestStringEdwOpen(nEdwOperator,Veta)
          EventNumber=nEdwOperator
          EventType=EventEdw
          go to 9999
        else if(CheckNumber.eq.nButtRunTest) then
          if(FromEditM50) then
            k=1
          else
            k=0
          endif
          if(NRefBlock.gt.1.and.k.eq.0) then
            call SetMenuRefBlock
            TextInfo(1)='Select refblock to be used for the space '//
     1                  'group test'
            call CopyStringArray(MenuRefBlock,TextInfo(2),NRefBlock)
            NInfo=NRefBlock+1
            allocate(UseInSGTest(NRefBlock))
            call FeSelectFromOffer(-1.,-1.,1,UseInSGTest,ich)
            if(ich.ne.0) go to 9999
          endif
          call DRSGTest(k,Change)
          if(Change) then
            call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
            call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),NDimShift,
     1                               .false.,.true.)
            GrupaOld=Grupa(KPhase)
          endif
          if(allocated(UseInSGTest)) deallocate(UseInSGTest)
          go to 9999
        else if(CheckNumber.eq.nButtForStokes) then
          call EM50SymbolsForStokes
          go to 9999
        else if(CheckNumber.eq.nButtLocalSymm) then
          call EM50DefineLocalSymm
        endif
2600    EventNumber=nEdwOperator
        EventType=EventEdw
        go to 3250
      else if(CheckType.eq.EventCrw) then
        go to 3200
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 9999
      entry EM50SymmetryComplete
      if(ZeSymbolu.eq.1.and.Grupa(KPhase).ne.' ') go to 9999
      Zvenku=1
3000  call CompleteSymm(0,ich)
      if(ich.eq.0) then
        go to 3010
      else
        go to 3050
      endif
3010  GrupaNew='???'
      if(NDimI(KPhase).gt.0) then
        call CopyVek(Qu(1,1,1,KPhase),QuPom,3*NDimI(KPhase))
        call CopyMat(MetTens6I(1,1,KPhase),MetTens6NewI,NDim(KPhase))
        call EM50MetTensSymm(MetTens6NewI)
        call MatBlock3(MetTens6NewI,gppi,NDim(KPhase))
        call MatInv(gppi,gpp,det,3)
        m=3*NDim(KPhase)+1
        do i=1,NDimI(KPhase)
          call CopyVek(MetTens6NewI(m),gppi,3)
          call multm(gpp,MetTens6NewI(m),Qu(1,i,1,KPhase),3,3,1)
          m=m+NDim(KPhase)
        enddo
      endif
      call FindSmbSg(GrupaNew,ChangeOrderYes,1)
      if(NDimI(KPhase).gt.0)
     1  call CopyVek(QuPom,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      if(index(GrupaNew,'?').gt.0) go to 3040
      if(Zvenku.eq.0.and..not.EqIgCase(GrupaNew,Grupa(KPhase)).and.
     1  Grupa(KPhase).ne.' '.and.Grupa(KPhase)(1:1).ne.'?') then
        NInfo=2
        TextInfo(1)=Grupa(KPhase)(:idel(Grupa(KPhase)))//'->'//
     1              GrupaNew(:idel(GrupaNew))
        TextInfo(2)='The program is offering you the standard space '//
     1              'group symbol'
        if(FeYesNoHeader(-1.,-1.,
     1                   'Do you want to accept the new symbol?',0))
     2    then
          Grupa(KPhase)=GrupaNew
        else
          go to 3040
        endif
      else
        Grupa(KPhase)=GrupaNew
      endif
      if(ZeSymbolu.eq.1) call FeQuestLblOff(nLblText)
      if(Grupa(KPhase).eq.' '.or.index(Grupa(KPhase),'?').gt.0) then
        call FeQuestStringEdwOpen(nEdwGrupa,' ')
      else
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      n=NDim(KPhase)
      if(n.gt.4) n=3
      call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),n,.false.,
     1                         .true.)
      if(ZeSymbolu.ne.-1) then
        ZeSymbolu=-1
        call FeQuestLblChange(nLblText,Text2)
      endif
3040  if(Zvenku.eq.1) go to 9999
3050  EventType=EventEdw
      EventNumber=nEdwOperator
      go to 9999
3100  if(Lattice(KPhase).ne.SmbL(n).or.Lattice(KPhase).eq.'X') then
        call EM50GenVecCentr(n,.true.,ich)
        Lattice(KPhase)=SmbL(n)
        Grupa(KPhase)=' '
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
        Zvenku=0
        go to 3000
      else
        go to 9999
      endif
3200  EventType=EventEdw
      EventNumber=nEdwOperator
3250  if(EdwStringQuest(nEdwGrupa).ne.' ') then
        if(ZeSymbolu.eq.1) then
          call FeQuestLblOff(nLblText)
        else if(ZeSymbolu.eq.-1) then
          call FeQuestLblOff(nLblText)
        endif
        ZeSymbolu=0
        Grupa(KPhase)=' '
        call FeQuestStringEdwOpen(nEdwGrupa,Grupa(KPhase))
      endif
      if(EdwStringQuest(nEdwShift).ne.' ') then
        n=NDim(KPhase)
        if(n.gt.4) n=3
        call FeQuestRealAEdwOpen(nEdwShift,shsg(1,KPhase),n,.true.,
     1                           .true.)
      endif
9999  return
100   format('(',i5,')')
101   format(a5,1x,a8,1x,a20,1x,a10,1x,a6,1x,a20)
102   format(i5)
      end
