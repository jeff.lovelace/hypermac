      subroutine EM50GenVecCentr(nc,FirstTime,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical FirstTime
      ich=0
      if(nc.lt.8) then
        NLattVec(KPhase)=1
        call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
      endif
      if(nc.gt.1.and.nc.le.5) then
        NLattVec(KPhase)=2
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,3
          vt6(i,2,1,KPhase)=.5
        enddo
        if(nc.ne.5) vt6(nc-1,2,1,KPhase)=0.
      else if(nc.eq.6) then
        NLattVec(KPhase)=3
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        vt6(1,2,1,KPhase)=.666666667
        vt6(2,2,1,KPhase)=.333333333
        vt6(3,2,1,KPhase)=.333333333
        vt6(1,3,1,KPhase)=.333333333
        vt6(2,3,1,KPhase)=.666666667
        vt6(3,3,1,KPhase)=.666666667
      else if(nc.eq.7) then
        NLattVec(KPhase)=4
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do j=2,4
          do i=1,3
            if(i.ne.j-1) then
              vt6(i,j,1,KPhase)=.5
            else
              vt6(i,j,1,KPhase)=0.
            endif
          enddo
        enddo
      else if(nc.eq.8) then
        if(FirstTime) then
          call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                     NComp(KPhase),NPhase,1)
          call EM50ReadCellCentr(ich)
        endif
      endif
      if(nc.lt.7.and.NDimI(KPhase).gt.0) then
        do i=2,NLattVec(KPhase)
          call SetRealArrayTo(vt6(4,i,1,KPhase),NDimI(KPhase),0.)
        enddo
      endif
      do i=1,NLattVec(KPhase)
        do j=NDim(KPhase)+1,MaxNDim
          vt6(j,i,1,KPhase)=0.
        enddo
      enddo
      return
      end
