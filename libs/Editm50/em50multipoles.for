      subroutine EM50Multipoles
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*256 :: InputFile=' ',EdwStringQuest
      character*80 Veta,NotApplicable,MenuWFAct(6)
      character*30 Label(3)
      integer TypVodiku,RolMenuSelectedQuest,PopPom,PopCoreSum,AtNumP,
     1        RolMenuStateQuest,KMenuWFAct(6)
      logical EqIgCase,CrwLogicQuest,MameVodik,Cation,STOKnown,FFKnown,
     1        ExistFile
      real SingleZeta(28)
      save nButtMore,nCrwSDS,nCrwClementi,nLblFirst,nLblNotApplicable,
     1     nRolMenuAtType,nEdwPopFirst,nEdwNSlater,nEdwZetaSlater,
     2     nEdwHN,nEdwHZeta,TypVodiku,nCrwFromFile,nLblDeform,nLinka,
     3     nLblZetaSlater,nLblNSlater,nButtZetaRecalc,MenuWFAct,
     4     nRolMenuWaveFSource,KMenuWFAct,NButtImport,nButtEdit,
     5     STOKnown,FFKnown,LastAtomOld
      data Label/'Core','Valence','<-   Orbital populations   ->'/,
     1     NotApplicable/' '/
      entry EM50MultipolesMake(id)
      MenuWFAct(1)='STO-Default'
      MenuWFAct(2)='STO-JANA'
      MenuWFAct(3)='STO-MOLLY'
      MenuWFAct(4)='STO-Coppens WEB page'
      MenuWFAct(5)='STO-table'
      MenuWFAct(6)='FF-table'
      il=1
      tpom=5.
      call FeQuestLblMake(id,xdqp*.5,6,' ','C','B')
      call FeQuestLblOff(LblLastMade)
      nLblNotApplicable=LblLastMade
      Veta='%Atom type'
      xpomp=tpom+FeTxLengthUnder(Veta)+35.
      dpom=40.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      xpom=xpomp+dpom+110.
      do i=1,3
        tpom=xpom+CrwXd+10.
        if(i.eq.1) then
          Veta='S%DS FF'
        else if(i.eq.2) then
          Veta='Simple S%later'
          xpom0=xpom
        else
          Veta='S%TO or FF'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      2)
        if(i.eq.1) then
          nCrwSDS=CrwLastMade
        else if(i.eq.3) then
          nCrwClementi=CrwLastMade
        endif
        xpom=xpom+110.
      enddo
      il=il+1
      ilp=il
      tpom=5.
      Veta='%STO/FF source'
      dpom=FeTxLength(MenuWFAct(4))+25.
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuWaveFSource=RolMenuLastMade
      xpom=xpomp+dpom+20.
      Veta='Import STO-table'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtImport=ButtonLastMade
      xpom=xpom+dpom+20.
      Veta='Edit STO-table'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtEdit=ButtonLastMade
      tpom=xpom0+5.
      xpom=tpom+45.
      dpom=50.
      do i=1,2
        if(i.eq.1) then
          Veta='%H-N'
        else
          Veta='H-z%eta'
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwHN=EdwLastMade
        else
          nEdwHZeta=EdwLastMade
        endif
        il=il+1
      enddo
      il=il+1
      tpom=13.
      xpom=30.
      tpom0=13.
      xpom0=30.
      dpom=30.
      do 1250i=1,2
        il=ilp
        il=il+1
        call FeQuestLblMake(id,xdqp*(.25+float(i-1)*.5),il,Label(i),
     1                      'C','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) then
          nLblFirst=LblLastMade
          Veta='%Edit higher orbitals'
          dpomb=FeTxLength(Veta)+20.
          call FeQuestButtonMake(id,(xdq-dpomb)*.5,il+5,dpomb,ButYd,
     1                           Veta)
          nButtMore=ButtonLastMade
        endif
        if(i.eq.1) then
          call FeQuestLblMake(id,xdqp*.5,il,Label(3),'C','B')
          call FeQuestLblOff(LblLastMade)
        endif
        do j=1,4
          il=ilp+1
          do k=1,j
            il=il+1
            write(Veta,'(i1,a1)') j,OrbitName(k)
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,0)
            if(i.eq.1.and.j.eq.1.and.k.eq.1) nEdwPopFirst=EdwLastMade
          enddo
          tpom=tpom+dpom+30.
          xpom=xpom+dpom+30.
        enddo
        if(i.eq.2) go to 1250
        tpom=tpom0+xdqp*.5+10.
        xpom=xpom0+xdqp*.5+10.
1250  continue
      il=il+2
      call FeQuestLinkaMake(id,il)
      nLinka=LinkaLastMade
      call FeQuestLinkaOff(LinkaLastMade)
      il=il+1
      Veta='Deformation basis'
      call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
      nLblDeform=LblLastMade
      call FeQuestLblOff(LblLastMade)
      il=il+2
      Veta='Zeta-Slater:'
      xpom0=FeTxLength(Veta)+10.
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblZetaSlater=LblLastMade
      Veta='N-Slater:'
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblNSlater=LblLastMade
      dpom=45.
      xpom=xpom0
      tpom=xpom+dpom*.5
      sh=55.
      do i=1,8
        write(Veta,'(''l='',i1)') i-1
        call FeQuestEdwMake(id,tpom,il-1,xpom,il,Veta,'C',dpom,EdwYd,0)
        if(i.eq.1) nEdwZetaSlater=EdwLastMade
        xpom=xpom+sh
        tpom=tpom+sh
      enddo
      il=il+1
      xpom=xpom0
      tpom=xpom+dpom*.5
      do i=1,8
        call FeQuestEdwMake(id,tpom,il-1,xpom,il,' ','C',dpom,EdwYd,0)
        if(i.eq.1) nEdwNSlater=EdwLastMade
        xpom=xpom+sh
        tpom=tpom+sh
      enddo
      il=il+1
      Veta='%Recalculate zeta''s from single zeta exponents'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xdqp-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtZetaRecalc=ButtonLastMade
      LastAtom=1
      LastAtomOld=-1
      KartId=id
      go to 2000
      entry EM50MultipoleRefresh
2000  if(ChargeDensities) then
        NotApplicable=' '
        if(LastAtomOld.ne.LastAtom) then
          STOKnown=.false.
          FFKnown=.false.
        endif
        call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
        call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                          NAtFormula(KPhase),LastAtom)
        TypVodiku=3
        MameVodik=EqIgCase(AtType(LastAtom,KPhase),'H')
        if(MameVodik) then
          if(PopVal(1,LastAtom,KPhase).eq.-1) then
            TypVodiku=1
          else if(PopVal(1,LastAtom,KPhase).eq.-2) then
            TypVodiku=2
          endif
        endif
        if(.not.MameVodik.or.TypVodiku.eq.3) then
          if(EqIgCase(CoreValSource(LastAtom,KPhase),'Default').or.
     1       EqIgCase(CoreValSource(LastAtom,KPhase),NameOfWaveFile))
     2      then
            CoreValSource(LastAtom,KPhase)='Default'
            n=1
            call FeQuestButtonClose(nButtImport)
            call FeQuestButtonClose(nButtEdit)
          else
            n=1
            do i=1,4
              if(EqIgCase(CoreValSource(LastAtom,KPhase),MenuWF(i)))
     1          then
                n=i+1
                exit
              endif
            enddo
            if(n.eq.1) then
              if(EqIgCase(CoreValSource(LastAtom,KPhase),'FF-table'))
     1          then
                n=6
                call FeQuestButtonLabelChange(nButtImport,
     1                                        'Import FF-table')
                call FeQuestButtonLabelChange(nButtEdit,
     1                                        'Edit FF-table')
              else if(EqIgCase(CoreValSource(LastAtom,KPhase),
     1                         'STO-table')) then
                n=5
                call FeQuestButtonLabelChange(nButtImport,
     1                                        'Import STO-table')
                call FeQuestButtonLabelChange(nButtEdit,
     1                                        'Edit STO-table')
              endif
              call FeQuestButtonOpen(nButtImport,ButtonOff)
              call FeQuestButtonOpen(nButtEdit,ButtonOff)
            else
              call FeQuestButtonClose(nButtImport)
              call FeQuestButtonClose(nButtEdit)
            endif
          endif
          KMenuWFAct=1
          do i=1,3
            if(EqIgCase(NameOfWaveFile,MenuWF(i))) then
              KMenuWFAct(i+1)=0
              exit
            endif
          enddo
          call FeQuestRolMenuWithEnableOpen(nRolMenuWaveFSource,
     1                                      MenuWFAct,KMenuWFAct,6,n)
        else
          call FeQuestRolMenuDisable(nRolMenuWaveFSource)
        endif
        do i=1,8
          call FeQuestRealEdwOpen(nEdwZetaSlater+i-1,
     1                            ZSlater(i,LastAtom,KPhase),
     2                            .false.,.false.)
          call FeQuestIntEdwOpen(nEdwNSlater+i-1,
     1                           NSlater(i,LastAtom,KPhase),.false.)
        enddo
        call FeQuestButtonOff(nButtZetaRecalc)
        if(TypVodiku.ne.2.and.MameVodik.or..not.MameVodik) then
          call FeQuestEdwClose(nEdwHN)
          call FeQuestEdwClose(nEdwHZeta)
        endif
        nEdw=nEdwPopFirst
        nLbl=nLblFirst
        do i=1,2
          if(TypVodiku.ne.3.and.MameVodik) then
            call FeQuestLblOff(nLbl)
            if(i.eq.1) call FeQuestLblOff(nLbl+2)
            call FeQuestButtonClose(nButtMore)
          else
            call FeQuestLblOn(nLbl)
            if(i.eq.1) call FeQuestLblOn(nLbl+2)
            call FeQuestButtonOpen(nButtMore,ButtonOff)
          endif
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              if(TypVodiku.ne.3) then
                call FeQuestEdwClose(nEdw)
              else
                if(i.eq.1) then
                  PopPom=PopCore(nn,LastAtom,KPhase)
                else
                  PopPom=PopVal(nn,LastAtom,KPhase)
                endif
                call FeQuestIntEdwOpen(nEdw,PopPom,.false.)
              endif
              nEdw=nEdw+1
            enddo
          enddo
          nLbl=nLbl+1
        enddo
        nCrw=nCrwSDS
        do i=1,3
          if(MameVodik) then
            call FeQuestCrwOpen(nCrw,TypVodiku.eq.i)
          else
            call FeQuestCrwClose(nCrw)
          endif
          nCrw=nCrw+1
        enddo
        if(MameVodik) then
          if(TypVodiku.eq.2) then
            call FeQuestIntEdwOpen(nEdwHN,HNSlater(LastAtom,KPhase),
     1                             .false.)
            call FeQuestRealEdwOpen(nEdwHZeta,
     1                              HZSlater(LastAtom,KPhase),.false.,
     2                              .false.)
          endif
        endif
        call FeQuestLinkaOn(nLinka)
        call FeQuestLblOn(nLblDeform)
        call FeQuestLblOn(nLblZetaSlater)
        call FeQuestLblOn(nLblNSlater)
      else
        call FeQuestCrwClose(nCrwFromFile)
        do i=1,8
          call FeQuestEdwClose(nEdwNSlater+i-1)
          call FeQuestEdwClose(nEdwZetaSlater+i-1)
        enddo
        call FeQuestButtonClose(nButtZetaRecalc)
        if(RolMenuStateQuest(nRolMenuAtType).eq.RolMenuOpened)
     1    call FeQuestRolMenuClose(nRolMenuAtType)
        call FeQuestEdwClose(nEdwHN)
        call FeQuestEdwClose(nEdwHZeta)
        nEdw=nEdwPopFirst
        nLbl=nLblFirst
        do i=1,2
          call FeQuestLblOff(nLbl)
          if(i.eq.1) call FeQuestLblOff(nLbl+2)
          call FeQuestButtonClose(nButtMore)
          do j=1,4
            do k=1,j
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          enddo
          nLbl=nLbl+1
        enddo
        il=1
        if(NDim(KPhase).gt.3.or..not.AllowChargeDensities) then
          NotApplicable='The multipole option is not applicable.'
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        else if(.not.ChargeDensities) then
          NotApplicable='The multipole option not yet activated.'
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        else
          NotApplicable=' '
          call FeQuestLblChange(nLblNotApplicable,NotApplicable)
        endif
        call FeQuestLinkaOff(nLinka)
        call FeQuestLblOff(nLblDeform)
        call FeQuestLblOff(nLblZetaSlater)
        call FeQuestLblOff(nLblNSlater)
      endif
      go to 9999
      entry EM50MultipolesCheck
      Klic=0
      go to 2100
      entry EM50MultipolesUpDate
      Klic=1
2100  if(NDim(KPhase).gt.3.or..not.ChargeDensities) go to 9999
      LastAtomOld=LastAtom
      if(TypVodiku.eq.2) then
        call FeQuestIntFromEdw(nEdwHN,HNSlater(LastAtom,KPhase))
        call FeQuestRealFromEdw(nEdwHZeta,
     1                          HZSlater(LastAtom,KPhase))
      else if(TypVodiku.eq.3) then
        nEdw=nEdwPopFirst
        do i=1,2
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
      endif
      do i=1,8
        call FeQuestRealFromEdw(nEdwZetaSlater+i-1,
     1                          ZSlater(i,LastAtom,KPhase))
        call FeQuestIntFromEdw(nEdwNSlater+i-1,
     1                         NSlater(i,LastAtom,KPhase))
      enddo
      if(Klic.eq.1) go to 9900
      if(CheckType.eq.EventRolMenu) then
        if(CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(CheckNumber)
          go to 9900
        else if(CheckNumber.eq.nRolMenuWaveFSource) then
          i=RolMenuSelectedQuest(CheckNumber)
          Veta=CoreValSource(LastAtom,KPhase)
          if(i.le.1) then
            CoreValSource(LastAtom,KPhase)='Default'
          else if(i.le.4) then
            CoreValSource(LastAtom,KPhase)=MenuWF(i-1)
          else if(i.eq.5) then
            if(.not.STOKnown) then
              if(EqIgCase(Veta,'Default').or.EqIgCase(Veta,'FF-table'))
     1          then
                Veta=HomeDir(:idel(HomeDir))//'FORMFAC'//
     1               DirectoryDelimitor//
     2               NameOfWaveFile(:idel(NameOfWaveFile))
              else
                Veta=CoreValSource(LastAtom,KPhase)
              endif
              call ReadAndSaveWaveFunction(AtType(LastAtom,KPhase),
     1                                     LastAtom,Veta,ich)
              STOKnown=.true.
            endif
            CoreValSource(LastAtom,KPhase)='STO-table'
          else if(i.eq.6) then
            if(.not.EqIgCase(Veta,'FF-table').and..not.FFKnown) then
              call SetCoreVal(LastAtom)
              FFKnown=.true.
            endif
            CoreValSource(LastAtom,KPhase)='FF-table'
          endif
          go to 9900
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMore)
     1  then
        call EM50PopOrbMore
        go to 9900
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtImport)
     1  then
        xqdp=300.
        Veta='Specify input file:'
        idp=NextQuestId()
        call FeQuestCreate(idp,-1.,-1.,xqdp,1,Veta,0,LightGray,0,0)
        il=1
        bpom=50.
        xpom=5.
        tpom=5.
        dpom=xqdp-bpom-20.
        call FeQuestEdwMake(idp,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        nEdwName=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,InputFile)
        Veta='%Browse'
        xpom=tpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,bpom,ButYd,Veta)
        nButtBrowse=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
          InputFile=EdwStringQuest(nEdwName)
          call FeFileManager('Select input file',InputFile,'*.*',
     1                        0,.true.,ichp)
          if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,InputFile)
          go to 1500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          InputFile=EdwStringQuest(nEdwName)
          if(.not.ExistFile(InputFile)) then
            Veta='The file "'//InputFile(:idel(InputFile))//
     1           '" doesn''t exist, try again.'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
          call ReadAndSaveWaveFunction(AtType(LastAtom,KPhase),
     1                                 LastAtom,InputFile,ich)
        endif
        call FeQuestRemove(idp)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEdit)
     1  then
        Klic=0
        if(EqIgCase(CoreValSource(LastAtom,KPhase),'FF-table')) then
          Klic=1
        else if(EqIgCase(CoreValSource(LastAtom,KPhase),'STO-table'))
     1    then
          Klic=2
        endif
        if(Klic.gt.0) call EM50ReadOwnFormFactor(LastAtom,Klic)
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtZetaRecalc) then
        nEdw=nEdwPopFirst
        do i=1,2
          nn=0
          do j=1,4
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
        call RealAFromAtomFile(AtTypeFull(LastAtom,KPhase),
     1                         'Single_zeta_exponents',SingleZeta,28,
     2                         ich)
        if(ich.eq.0) then
          SumZ=0.
          SumN=0.
          do i=1,28
            pom=PopVal(i,LastAtom,KPhase)
            SumZ=SumZ+pom*SingleZeta(i)
            SumN=SumN+pom
          enddo
          if(SumN.gt.0.) then
            write(Cislo,100) 2.*SumZ/SumN
          else
            PopCoreSum=0
            do i=1,28
              PopCoreSum=PopCoreSum+PopCore(i,LastAtom,KPhase)
            enddo
            NumAt=nint(AtNum(LastAtom,KPhase))
            Cation=PopCoreSum.lt.NumAt
            if(PopCoreSum.eq.2) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(2)
                else
                  pom=(SingleZeta(2)+SingleZeta(3))/2.
                endif
              else
                pom=SingleZeta(1)
              endif
            else if(PopCoreSum.eq.10) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(4)
                else
                  pom=(SingleZeta(4)+SingleZeta(5))/2.
                endif
              else
                pom=SingleZeta(3)
              endif
            else if(PopCoreSum.eq.18) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(7)
                else
                  if(NumAt.ge.31.and.NumAt.le.35) then
                    pom=(SingleZeta(7)+SingleZeta(8))/2.
                  else if(NumAt.ge.21.and.NumAt.le.30) then
                    pom=SingleZeta(6)
                  endif
                endif
              else
                pom=SingleZeta(5)
              endif
            else if(PopCoreSum.eq.36) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(11)
                else if(NumAt.ge.49.and.NumAt.le.53) then
                  pom=(SingleZeta(11)+SingleZeta(12))/2.
                else if(NumAt.ge.39.and.NumAt.le.48) then
                  pom=SingleZeta(9)
                endif
              else
                pom=SingleZeta(8)
              endif
            else if(PopCoreSum.eq.54) then
              if(Cation) then
                if(NumAt-PopCoreSum.le.2) then
                  pom=SingleZeta(16)
                else if(NumAt.ge.81.and.NumAt.le.85) then
                  pom=(SingleZeta(16)+SingleZeta(17))/2.
                else if(NumAt.ge.71.and.NumAt.le.80) then
                  pom=SingleZeta(13)
                endif
              else
                pom=SingleZeta(12)
              endif
            else if(PopCoreSum.eq.86) then
              pom=SingleZeta(17)
            endif
            pom=2.*pom
          endif
          read(Cislo,100) pom
          ZSlater(1:8,LastAtom,KPhase)=pom
          do i=1,8
            call FeQuestRealEdwOpen(nEdwZetaSlater+i-1,
     1                              ZSlater(i,LastAtom,KPhase),
     2                              .false.,.false.)
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwSDS.and.
     1        CheckNumber.le.nCrwClementi) then
        nCrw=nCrwSDS
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            if(i.le.2) then
              PopVal(1,LastAtom,KPhase)=-i
              if(i.eq.2) then
                HNSlater(LastAtom,KPhase)=0
                HZSlater(LastAtom,KPhase)=2.3
              endif
            else
              call SetIntArrayTo(PopCore(1,LastAtom,KPhase),28,0)
              call SetIntArrayTo(PopVal(1,LastAtom,KPhase),28,0)
              PopVal(1,LastAtom,KPhase)=1
            endif
            go to 9900
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.ne.0) then
        call NebylOsetren
      endif
      go to 9999
9900  call EM50OneMultipoleSet(LastAtomOld)
      go to 2000
9999  return
100   format(f10.3)
      end
