      subroutine EM50PopOrbMore
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      integer PopPom
      character*8 Veta
      character*7  Label(2)
      data Label/'Core','Valence'/
      id=NextQuestId()
      xdqq=400.
      call FeQuestCreate(id,-1.,-1.,xdqq,8,
     1                   'Define additional orbital populations',0,
     2                   LightGray,0,0)
      tpom=13.
      xpom=30.
      tpom0=13.
      xpom0=30.
      dpom=30.
      ilp=0
      do 1250i=1,2
        nn=10
        il=ilp
        il=il+1
        call FeQuestLblMake(id,xdqq*(.25+float(i-1)*.5),il,Label(i),'C',
     1                      'B')
        do j=5,7
          il=ilp+1
          do k=1,j
            nn=nn+1
            il=il+1
            write(Veta,'(i1,a1)') j,OrbitName(k)
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,0)
            if(i.eq.1) then
              PopPom=PopCore(nn,LastAtom,KPhase)
            else
              PopPom=PopVal(nn,LastAtom,KPhase)
            endif
            call FeQuestIntEdwOpen(EdwLastMade,PopPom,.false.)
            if(nn.eq.11.and.i.eq.1) nEdwFirst=EdwLastMade
          enddo
          tpom=tpom+dpom+30.
          xpom=xpom+dpom+30.
        enddo
        if(i.eq.2) go to 1250
        tpom=tpom0+xdqq*.5+10.
        xpom=xpom0+xdqq*.5+10.
1250  continue
      call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
      endif
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=1,2
          nn=10
          do j=5,7
            do k=1,j
              nn=nn+1
              call FeQuestIntFromEdw(nEdw,PopPom)
              if(i.eq.1) then
                PopCore(nn,LastAtom,KPhase)=PopPom
              else
                PopVal(nn,LastAtom,KPhase)=PopPom
              endif
              nEdw=nEdw+1
            enddo
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
