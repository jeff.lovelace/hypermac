      subroutine EM50Cell
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension tzero(3),rm6p(36),pp(6),pq(6),mpp(3),qup(3,3),
     1          RCommPom(9),RCommPomI(9)
      character*256 EdwStringQuest
      character*80 Veta,t80
      character*2 nty
      integer EdwStateQuest,CrwStateQuest,ButtonStateQuest,CrlCentroSymm
      integer :: NDimO=0
      logical CrwLogicQuest,AlreadyApplied,EqRV0
      save nCrwTwin,nEdwTwin,nEdwDim,nEdwComp,nCrwCommen,nButtShowSG,
     1     nButtSelectSG,nButtOriginSG,nEdw,nCrwSimpleSupercell,
     2     nButtSupercellMatrix
      data AlreadyApplied/.true./
      entry EM50CellMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      ncompold=NComp(KPhase)
      NTwinOld=NTwin
      NTwinIn=NTwin
      NDimO=NDim(KPhase)
      if(allocated(RTwIn)) deallocate(RTwIn)
      allocate(RTwIn(9,NTwin))
      call CopyVek(RTw,RTwIn,9*NTwin)
      il=1
      Veta='Cell %parameters'
      tpom=5.
      xpom=FeTxLengthUnder(Veta)+15.
      dpom=400.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'S%tructure','L',dpom,
     1                    EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(nEdwName,StructureName)
      if(NPhase.gt.1) then
        il=il+1
        dpomp=120.
        call FeQuestEdwMake(id,tpom,il,xpom,il,'P%hase label','L',dpomp,
     1                      EdwYd,0)
        nEdwPhase=EdwLastMade
        call FeQuestStringEdwOpen(nEdwPhase,PhaseName(KPhase))
      else
        nEdwPhase=0
      endif
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCell=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwCell,CellPar(1,1,KPhase),6,
     1                         StatusM50.ge.10000,.false.)
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,'%E.s.d.''s','L',dpom,
     1                    EdwYd,0)
      nEdwEsd=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwEsd,CellParSU(1,1,KPhase),6,
     1                         StatusM50.ge.10000,.false.)
      if((ExistSingle.or..not.Existm90).and..not.ParentStructure) then
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,'T%winning','L',CrwXd,
     1                      CrwYd,1,0)
        nCrwTwin=CrwLastMade
        call FeQuestCrwOpen(nCrwTwin,NTwin.gt.1)
      else
        nCrwTwin=0
      endif
      tpomp=xqd*.5
      xpom=tpomp+FeTxLengthUnder(Veta)+15.
      dpomp=50.
      tpomb=xpom+dpomp+2.*EdwYd
      dpomb=xqd-15.-tpomb
      if(ExistSingle.or..not.Existm90) then
        call FeQuestEudMake(id,tpomp,il,xpom,il,'#twi%n domains','L',
     1                      dpomp,EdwYd,1)
        nEdwTwin=EdwLastMade
        call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'%Matrices')
        nButtTwMat=ButtonLastMade
      else
        nEdwTwin=0
        nButtTwMat=0
      endif
      il=il+1
      Veta='#%composite parts'
      call FeQuestEudMake(id,tpomp,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
      nEdwComp=EdwLastMade
      call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'M%atrices')
      nButtCompMat=ButtonLastMade
      if(nEdwTwin.gt.0.and.NTwin.gt.1) then
        call FeQuestIntEdwOpen(nEdwTwin,NTwin,.false.)
        call FeQuestEudOpen(nEdwTwin,2,mxscutw,1,0.,0.,0.)
        call FeQuestButtonOpen(nButtTwMat,ButtonOff)
      endif
      if(NDimI(KPhase).gt.0) then
        call FeQuestIntEdwOpen(nEdwComp,NComp(KPhase),.false.)
        call FeQuestEudOpen(nEdwComp,1,3,1,0.,0.,0.)
        if(NComp(KPhase).gt.1)
     1    call FeQuestButtonOpen(nButtCompMat,ButtonOff)
      endif
      if(ParentStructure) then
        n=NQMag(KPhase)+3
      else
        n=NDim(KPhase)
      endif
      if((.not.ExistSingle.or.(.not.ExistM90.and..not.ExistM95).or.
     1    StatusM50.gt.10000).and..not.WizardMode) then
        Veta='%Dimension'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpomp=50.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
        nEdwDim=EdwLastMade
        call FeQuestIntEdwOpen(nEdwDim,n,.false.)
        if(NPhase.gt.1.and.ExistSingle.and.StatusM50.gt.10000) then
          i=maxNDim
        else
          i=6
        endif
        call FeQuestEudOpen(nEdwDim,3,i,1,0.,0.,0.)
      else
        nEdwDim=0
        write(Veta,'(i1)') n
        Veta='Dimension = '//Veta(1:1)
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      endif
      if(ParentStructure) then
        write(Veta,'(''%'',i1,a2,'' magnetic propagation vector'')')
     1    1,nty(1)
      else
        write(Veta,'(''%'',i1,a2,'' modulation vector'')') 1,nty(1)
      endif
      xpom=FeTxLength(Veta)+10.
      dpomp=200.
      if(ParentStructure) then
        n=NQMag(KPhase)
      else
        n=NDimI(KPhase)
      endif
      do i=1,3
        il=il+1
        write(Veta(2:4),'(i1,a2)') i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,0)
        if(i.eq.1) nEdwModVek=EdwLastMade
        if(i.le.n) then
          if(ParentStructure) then
            call FeQuestRealAEdwOpen(nEdwModVek+i-1,QMag(1,i,KPhase),
     1                               3,StatusM50.ge.10000,.true.)
          else
            call FeQuestRealAEdwOpen(nEdwModVek+i-1,Qu(1,i,1,KPhase),
     1                               3,StatusM50.ge.10000,.true.)
          endif
        endif
      enddo
      if(ParentStructure) then
        nCrwCommen=0
        nCrwSimpleSupercell=0
        nEdwSupCell=0
        nButtSupercellMatrix=0
        nEdwTzero=0
        nButtShowSG=0
        nButtSelectSG=0
        nButtOriginSG=0
      else
        il=il+1
        tpom=5.
        Veta='Commens%urate case'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwCommen=CrwLastMade
        if(NDim(KPhase).gt.3)
     1    call FeQuestCrwOpen(CrwLastMade,KCommen(KPhase).ne.0)
        il=il+1
        Veta='Use s%imple supercell'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSimpleSupercell=CrwLastMade
        if(KCommen(KPhase).gt.0.and.NDim(KPhase).eq.4)
     1    call FeQuestCrwOpen(CrwLastMade,ICommen(KPhase).eq.0)
        il=il-1
        tpom=xpom+CrwXd+50.
        Veta='%Supercell'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpomp=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpomp,EdwYd,1)
        nEdwSupCell=EdwLastMade
        Veta='Define %supercell matrix'
        call FeQuestButtonMake(id,tpom,il,dpomp+55.,ButYd,Veta)
        nButtSupercellMatrix=ButtonLastMade
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,'T%zero','L',dpomp,EdwYd,
     1                      0)
        nEdwTzero=EdwLastMade
        il=il-1
        Veta='Show supercell %group'
        tpom=(xdqp+xpom+dpomp)*.5
        xpom=FeTxLengthUnder(Veta)+10.
        tpom=tpom-xpom*.5
        call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
        nButtShowSG=ButtonLastMade
        il=il+1
        Veta='Se%lect supercell group'
        call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
        nButtSelectSG=ButtonLastMade
        il=il+1
        Veta='Select its %origin'
        call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
        nButtOriginSG=ButtonLastMade
        if(KCommen(KPhase).ne.0) then
          if(ICommen(KPhase).eq.0) then
            call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                              .false.)
          else
            call FeQuestButtonOff(nButtSupercellMatrix)
          endif
          call FeQuestRealAEdwOpen(nEdwTzero,trez(1,1,KPhase),
     1                           NDimI(KPhase),.false.,.true.)
          if(NSymm(KPhase).gt.0) then
            i=ButtonOff
          else
            i=ButtonDisabled
          endif
          call FeQuestButtonOpen(nButtShowSG,i)
          if(NDim(KPhase).lt.5) then
            call FeQuestButtonOpen(nButtSelectSG,i)
            call FeQuestButtonOpen(nButtOriginSG,i)
          endif
        endif
      endif
      go to 9999
      entry EM50CellCheck
      if(nEdwDim.gt.0) then
        call FeQuestIntFromEdw(nEdwDim,n)
        if(ParentStructure) then
          NQMag(KPhase)=n-3
        else
          NDim(KPhase)=n
          NDimI(KPhase)=NDim(KPhase)-3
          NDimQ(KPhase)=NDim(KPhase)**2
        endif
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDim) then
        if(ParentStructure) then
          n=NQMag(KPhase)
        else
          n=NDimI(KPhase)
        endif
        nEdw=nEdwModVek-1
        do 1100i=1,3
          nEdw=nEdw+1
          if(i.le.n) then
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              go to 1100
            else
              if(ParentStructure) then
                call FeQuestRealAEdwOpen(nEdw,QMag(1,i,KPhase),3,.true.,
     1                                   .false.)
              else
                call FeQuestRealAEdwOpen(nEdw,qu(1,i,1,KPhase),3,.true.,
     1                                   .false.)
              endif
            endif
          else
            call FeQuestEdwClose(nEdw)
          endif
1100    continue
        if(.not.ParentStructure) then
          if(NDim(KPhase).gt.3) then
            if(EdwStateQuest(nEdwComp).ne.EdwOpened) then
              NComp(KPhase)=1
              call FeQuestIntEdwOpen(nEdwComp,NComp(KPhase),.false.)
              call FeQuestEudOpen(nEdwComp,1,3,1,0.,0.,0.)
            endif
            if(CrwStateQuest(nCrwCommen).eq.CrwClosed) then
              KCommen(KPhase)=0
              call FeQuestCrwOpen(nCrwCommen,.false.)
            endif
          else
            call FeQuestEdwClose(nEdwComp)
            call FeQuestCrwClose(nCrwCommen)
            call FeQuestCrwClose(nCrwSimpleSupercell)
            call FeQuestEdwClose(nEdwSupCell)
            call FeQuestButtonClose(nButtSupercellMatrix)
            call FeQuestEdwClose(nEdwTzero)
            call FeQuestButtonClose(nButtShowSG)
            call FeQuestButtonClose(nButtSelectSG)
            call FeQuestButtonClose(nButtOriginSG)
          endif
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwComp) then
        call FeQuestIntFromEdw(nEdwComp,NComp(KPhase))
        do i=ncompOld+1,NComp(KPhase)
          call UnitMat(zv(1,i,KPhase),NDim(KPhase))
        enddo
        if(NComp(KPhase).gt.1) then
          if(ButtonStateQuest(nButtCompMat).eq.ButtonClosed)
     1      call FeQuestButtonOpen(nButtCompMat,ButtonOff)
        else
          call FeQuestButtonClose(nButtCompMat)
        endif
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call FinSym(0)
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSupCell) then
        call FeQuestIntAFromEdw(nEdwSupCell,NCommen(1,1,KPhase))
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTwin) then
        call FeQuestIntFromEdw(nEdwTwin,NTwin)
        mxscu=mxscutw-NTwin+1
        call FeDeferOutput
        if(NTwin.le.1) then
          call FeQuestCrwOff(nCrwTwin)
          call FeQuestEdwClose(nEdwTwin)
          call FeQuestButtonClose(nButtTwMat)
          EventType=0
          EventNumber=0
          EventTypeSave=0
          EventNumberSave=0
        else
          do i=NTwinold+1,NTwin
            call UnitMat(rtw(1,i),3)
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwSimpleSupercell) then
        ICommen(KPhase)=1-ICommen(KPhase)
        if(ICommen(KPhase).eq.0) then
          call FeQuestButtonClose(nButtSupercellMatrix)
          call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                            .false.)
        else
          call FeQuestEdwClose(nEdwSupCell)
          call FeQuestButtonOff(nButtSupercellMatrix)
          do j=1,NDimI(KPhase)
            call MultM(qu(1,j,1,KPhase),RCommen(1,1,KPhase),pp,1,3,3)
            do k=1,3
              if(abs(anint(pp(k))-pp(k)).gt..0001) then
                call UnitMat(RCommen (1,1,KPhase),3)
                call UnitMat(RCommenI(1,1,KPhase),3)
                go to 1800
              endif
            enddo
          enddo
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwTwin) then
        if(CrwLogicQuest(nCrwTwin)) then
          if(EdwState(nEdwTwin).eq.EdwClosed) then
            NTwin=2
            mxscu=mxscutw-NTwin+1
            call UnitMat(rtw(1,2),3)
            if(CrlCentroSymm().le.0)
     1        call RealMatrixToOpposite(rtw(1,2),rtw(1,2),3)
            call FeQuestIntEdwOpen(nEdwTwin,NTwin,.false.)
            call FeQuestEudOpen(nEdwTwin,1,mxscutw,1,0.,0.,0.)
            call FeQuestButtonOpen(nButtTwMat,ButtonOff)
          endif
          EventType=EventEdw
          EventNumber=nEdwTwin
        else
          NTwin=1
          mxscu=mxscutw-NTwin+1
          call FeQuestEdwClose(nEdwTwin)
          call FeQuestButtonClose(nButtTwMat)
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCommen) then
        if(CrwLogicQuest(nCrwCommen)) then
          if(ICommen(KPhase).eq.0) then
            call FeQuestIntAEdwOpen(nEdwSupCell,NCommen(1,1,KPhase),3,
     1                              .false.)
          else
            call FeQuestButtonOff(nButtSupercellMatrix)
          endif
          call SetRealArrayTo(trez(1,1,KPhase),NDimI(KPhase),0.)
          call FeQuestRealAEdwOpen(nEdwTzero,trez(1,1,KPhase),
     1                             NDimI(KPhase),.false.,.true.)
          if(NDim(KPhase).eq.4)
     1      call FeQuestCrwOpen(nCrwSimpleSupercell,
     2                          ICommen(KPhase).eq.0)
          if(NSymm(KPhase).gt.0) then
            i=ButtonOff
          else
            i=ButtonDisabled
          endif
          call FeQuestButtonOpen(nButtShowSG,i)
          if(NDim(KPhase).lt.5) then
            call FeQuestButtonOpen(nButtSelectSG,i)
            call FeQuestButtonOpen(nButtOriginSG,i)
          endif
          EventType=EventEdw
          EventNumber=nEdwSupCell
          KCommen(KPhase)=1
          KCommenMax=1
          if(NComp(KPhase).gt.1)
     1      call FeQuestButtonOpen(nButtCompMat,ButtonOff)
        else
          call FeQuestCrwClose(nCrwSimpleSupercell)
          call FeQuestEdwClose(nEdwSupCell)
          call FeQuestEdwClose(nEdwTzero)
          call FeQuestButtonClose(nButtShowSG)
          call FeQuestButtonClose(nButtSelectSG)
          call FeQuestButtonClose(nButtOriginSG)
          call FeQuestButtonClose(nButtSupercellMatrix)
          KCommen(KPhase)=0
        endif
      else if(CheckType.eq.EventButton) then
        call EM50CellUpdate(ich)
        if(CheckNumber.gt.0) then
          if(CheckNumber.eq.nButtTwMat) then
            call ReadTw(Rtw,Rtwi,NTwin,CellPar(1,1,1),ich)
            if(ich.eq.0) NTwinold=NTwin
          else if(CheckNumber.eq.nButtCompMat) then
            call ReadCompMat(ich)
            if(ich.eq.0) ncompold=NComp(KPhase)
          else if(CheckNumber.eq.nButtShowSG) then
            if(ich.eq.0) call EM50ShowSuperSG(0,Veta,pq,t80,rm6p,pp)
          else if(CheckNumber.eq.nButtSelectSG.or.
     1            CheckNumber.eq.nButtOriginSG) then
            if(ich.eq.0) then
              if(CheckNumber.eq.nButtSelectSG) then
                Key=0
              else
                Key=1
              endif
              call CopyVek(trez(1,1,KPhase),tzero,3)
              call EM50SelectSuperSG(Key,tzero,ichp)
              if(ichp.eq.0)
     1          call FeQuestRealAEdwOpen(nEdwTzero,tzero,NDimI(KPhase),
     2                                   .false.,.true.)
            endif
          else if(CheckNumber.eq.nButtSupercellMatrix) then
            Veta='Define the supercell matrix'
            call CopyMat(RCommen (1,1,KPhase),RCommPom ,3)
            call CopyMat(RCommenI(1,1,KPhase),RCommPomI,3)
1500        call FeReadRealMat(-1.,-1.,Veta,SmbABC,IdChangeCase,SmbABC,
     1        RCommen(1,1,KPhase),RCommenI(1,1,KPhase),3,CheckSingYes,
     2        CheckPosDefYes,ich)
            if(ich.ne.0) then
              call CopyMat(RCommPom ,RCommen (1,1,KPhase),3)
              call CopyMat(RCommPomI,RCommenI(1,1,KPhase),3)
              go to 1800
            endif
            do j=1,NDimI(KPhase)
              call MultM(qu(1,j,1,KPhase),RCommen(1,1,KPhase),pp,1,3,3)
              do k=1,3
                if(abs(anint(pp(k))-pp(k)).gt..0001) then
                  call FeChybne(-1.,-1.,'the matrix isn''t consistent'//
     1                          ' with modulation vector(s), try again.'
     2                         ,' ',SeriousError)
                  go to 1500
                endif
              enddo
            enddo
          endif
        endif
      endif
1800  if(nEdwDim.eq.0) go to 9999
      if((NDim(KPhase).ne.NDimo.or..not.AlreadyApplied).and.
     1   .not.ParentStructure) then
        AlreadyApplied=.false.
        if(ParentStructure) then
          n=NQMag(KPhase)-3
        else
          n=NDim(KPhase)-3
        endif
        if(n.gt.0) then
          nEdw=nEdwModVek
          do i=1,n
            if(EdwStringQuest(nEdw).ne.' ') then
              if(ParentStructure) then
                call FeQuestRealAFromEdw(nEdw,QMag(1,i,KPhase))
              else
                call FeQuestRealAFromEdw(nEdw,qu(1,i,1,KPhase))
              endif
            else
              go to 9999
            endif
            nEdw=nEdw+1
          enddo
        endif
2025    nso=NSymm(KPhase)
        nvto=NLattVec(KPhase)
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        if(NDimO.lt.NDim(KPhase)) then
          do i=1,nvto
            call SetRealArrayTo(vt6(NDimo+1,i,1,KPhase),
     1                          NDim(KPhase)-NDimo,0.)
          enddo
        endif
        NSymm(KPhase)=0
        do 2080i=1,nso
          call UnitMat(rm6p,NDim(KPhase))
          if(NDimO.lt.NDim(KPhase)) then
            call SetRealArrayTo(s6(NDimo+1,i,1,KPhase),
     1                          NDim(KPhase)-NDimo,0.)
          endif
          m=0
          do j=1,NDimo
            do k=1,NDimo
              m=m+1
              rm6p(k+(j-1)*NDim(KPhase))=rm6(m,i,1,KPhase)
            enddo
          enddo
          do j=1,NDimI(KPhase)
            call CopyVek(qu(1,j,1,KPhase),qup(1,j),3)
            nnul=0
            KdeNenul=0
            do k=1,3
              if(abs(qup(k,j)).lt..0001) then
                nnul=nnul+1
              else
                if(KdeNenul.le.0) KdeNenul=k
              endif
            enddo
            if(abs(qup(KdeNenul,j)-1.).gt..001)
     1        qup(KdeNenul,j)=1./sqrt(3.)
          enddo
           do j=4,NDim(KPhase)
            do k=1,3
              pom=0.
              do l=1,3
                pom=pom+qup(l,j-3)*rm6p(l+(k-1)*NDim(KPhase))
              enddo
              pp(k)=pom
            enddo
            do k=-1,1
              do l=-1,1
                do 2060m=-1,1
                  do n=1,3
                    pq(n)=float(m)*qup(n,1)-pp(n)
                    if(NDim(KPhase).gt.4)
     1                pq(n)=pq(n)+float(l)*qup(n,2)
                    if(NDim(KPhase).gt.5)
     1                pq(n)=pq(n)+float(k)*qup(n,3)
                    mpp(n)=nint(pq(n))
                    if(abs(pq(n)-float(mpp(n))).gt..0005) go to 2060
                  enddo
                  do n=1,NLattVec(KPhase)
                    pom=ScalMul(vt6(1,n,1,KPhase),pq)
                    if(abs(pom-anint(pom)).gt..0005) go to 2060
                  enddo
                  do n=1,3
                    rm6p(j+NDim(KPhase)*(n-1))=-mpp(n)
                  enddo
                  rm6p(j+NDim(KPhase)*3)=m
                  if(NDim(KPhase).gt.4) rm6p(j+NDim(KPhase)*4)=l
                  if(NDim(KPhase).gt.5) rm6p(j+NDim(KPhase)*5)=k
                  go to 2075
2060            continue
              enddo
            enddo
          enddo
          if(NDim(KPhase).gt.3) go to 2080
2075      NSymm(KPhase)=NSymm(KPhase)+1
          BratSymm(NSymm(KPhase),KPhase)=.true.
          call CopyMat(rm6p,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
          call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                  s6(1,NSymm(KPhase),1,KPhase),
     1                  symmc(1,NSymm(KPhase),1,KPhase),0)
2080    continue
        NDimo=NDim(KPhase)
        AlreadyApplied=.true.
        Veta='???'
        call FindSmbSg(Veta,.true.,1)
        Grupa(KPhase)=Veta
      endif
9999  return
      end
