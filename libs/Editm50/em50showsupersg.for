      subroutine EM50ShowSuperSG(Key,Grp,ShSgOrg,GrpTr,TrMatOut,ShSgOut)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrSystemSave,MonoclinicSave,CrSystemNew,CrSystemNewTr
      integer, allocatable :: IswSymmS(:,:)
      logical, allocatable :: BratSymmS(:)
      real, allocatable :: rm6s(:,:,:),rms(:,:,:),s6s(:,:,:),
     1                     vt6s(:,:,:),RMagS(:,:,:),ZMagS(:,:)
      real ShSgOrg(*),TrMatOut(*),ShSGOut(*),MetTensO(9,3),rmp(9),
     1     rmq(9),ShSgS(6)
      character*(*) GrpTr,Grp
      character*4  LatticeS
      allocate(rm6s(NDimQ(KPhase),NSymm(KPhase),NComp(KPhase)),
     1         rms(9,NSymm(KPhase),NComp(KPhase)),
     2         IswSymms(NSymm(KPhase),NComp(KPhase)),
     3         s6s(NDim(KPhase),NSymm(KPhase),NComp(KPhase)),
     4         BratSymmS(NSymm(KPhase)),
     5         RMagS(9,NSymm(KPhase),NComp(KPhase)),
     6         ZMagS(NSymm(KPhase),NComp(KPhase)),
     4         vt6s(NDim(KPhase),NLattVec(KPhase),NComp(KPhase)))
      nss =NSymm(KPhase)
      nvts=NLattVec(KPhase)
      LatticeS=Lattice(KPhase)
      NDims=NDim(KPhase)
      CrSystemSave=CrSystem(KPhase)
      MonoclinicSave=Monoclinic(KPhase)
      NGrupaSave=NGrupa(KPhase)
      do isw=1,NComp(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,isw,KPhase),rm6s(1,i,isw),NDim(KPhase))
          call CopyMat(rm(1,i,isw,KPhase),rms(1,i,isw),3)
          call CopyMat(RMag(1,i,isw,KPhase),RMagS(1,i,isw),3)
          ZMagS(i,isw)=ZMag(i,isw,KPhase)
          call CopyVek(s6(1,i,isw,KPhase),s6s(1,i,isw),NDim(KPhase))
          IswSymmS(i,isw)=IswSymm(i,isw,KPhase)
          BratSymmS(i)=BratSymm(i,KPhase)
        enddo
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6(1,i,1,KPhase),vt6s(1,i,isw),NDim(KPhase))
        enddo
        call CopyMat(MetTens(1,isw,KPhase),MetTensO(1,isw),3)
      enddo
      call CopyVek(ShSg(1,KPhase),ShSgS,NDim(KPhase))
      call ComSym(0,0,ich)
      call TrMat(RCommen(1,1,KPhase),rmq,3,3)
      call MultM(rmq,MetTens(1,1,KPhase),rmp,3,3,3)
      call MultM(rmp,RCommen(1,1,KPhase),MetTens(1,1,KPhase),3,3,3)
      call SuperSGToSuperCellSG(ich)
      if(ich.eq.0) then
        NDim(KPhase)=3
        NDimI(KPhase)=0
        NDimQ(KPhase)=NDim(KPhase)**2
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        call CrlOrderMagSymmetry
        call SetIgnoreWTo(.true.)
        call SetIgnoreETo(.true.)
        call FindSmbSgTr(Grp,ShSgOrg,GrpTr,n,TrMatOut,ShSgOut,
     1                   CrSystemNew,CrSystemNewTr)
        MonoclinicNewTr=CrSystemNewTr/10
        if(mod(CrSystemNewTr,10).eq.CrSystemMonoclinic)
     1    GrpTr=GrpTr(:idel(GrpTr))//' - '//SmbABC(MonoclinicNewTr)//
     2        ' setting'
        MonoclinicNew=CrSystemNew/10
        if(mod(CrSystemNew,10).eq.CrSystemMonoclinic)
     1    Grp=Grp(:idel(Grp))//' - '//SmbABC(MonoclinicNew)//
     2        ' setting'
        call SetIgnoreWTo(.false.)
        call SetIgnoreETo(.false.)
        if(Key.eq.0) then
          NInfo=6
          TextInfo(1)='Space group symbol (original setting) : '//Grp
          call OriginShift2String(ShSgOrg,3,TextInfo(2))
          TextInfo(2)='Origin shift: '//TextInfo(2)
          TextInfo(3)=' '
          TextInfo(4)='Space group symbol (standard setting) : '//GrpTr
          call TrMat2String(TrMatOut,3,TextInfo(6))
          TextInfo(5)='Transformation matrix: '//TextInfo(6)
          call OriginShift2String(ShSgOut,3,TextInfo(6))
          TextInfo(6)='Origin shift: '//TextInfo(6)
          del=0.
          do i=1,6
            del=max(del,FeTxLength(TextInfo(i)))
          enddo
          do while(FeTxLength(TextInfo(3)).lt.del)
            TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//'-'
          enddo
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
        endif
      else
        if(Key.eq.0) call FeUnforeseenError(' ')
      endif
      NDim(KPhase)=NDims
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      NSymm(KPhase)=nss
      NLattVec(KPhase)=nvts
      Lattice(KPhase)=LatticeS
      do isw=1,NComp(KPhase)
        do i=1,NSymm(KPhase)
          call CopyMat(rm6s(1,i,isw),rm6(1,i,isw,KPhase),NDim(KPhase))
          call CopyMat(rms(1,i,isw),rm(1,i,isw,KPhase),3)
          call CopyVek(s6s(1,i,isw),s6(1,i,isw,KPhase),NDim(KPhase))
          call CopyMat(RMagS(1,i,isw),RMag(1,i,isw,KPhase),3)
          ZMag(i,isw,KPhase)=ZMagS(i,isw)
          IswSymm(i,isw,KPhase)=IswSymmS(i,isw)
          BratSymm(i,KPhase)=BratSymmS(i)
          call CodeSymm(rm6(1,i,isw,KPhase),s6(1,i,isw,KPhase),
     1                  symmc(1,i,isw,KPhase),0)
        enddo
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6s(1,i,isw),vt6(1,i,isw,KPhase),NDim(KPhase))
        enddo
        call CopyMat(MetTensO(1,isw),MetTens(1,isw,KPhase),3)
      enddo
      call CopyVek(ShSgS,ShSg(1,KPhase),NDim(KPhase))
      ngc(KPhase)=0
      KCommen(KPhase)=1
      deallocate(rm6s,rms,s6s,vt6s,RMagS,ZMagS,IswSymmS,BratSymmS)
      CrSystem(KPhase)=CrSystemSave
      Monoclinic(KPhase)=MonoclinicSave
      NGrupa(KPhase)=NGrupaSave
      SwitchedToComm=.false.
      return
      end
