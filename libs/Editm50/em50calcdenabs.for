      subroutine EM50CalcDenAbs(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*60 Veta
      logical Poprve
      call PwdSetTOF(KDatBlock)
      ich=0
      wmol=0.
      ami=0.
      Poprve=.true.
      do i=1,NAtFormula(KPhase)
        call RealFromAtomFile(AtTypeFull(i,KPhase),'atweight',pomw,
     1                        0,ich)
        if(ich.ne.0) go to 9999
        pomm=AtMult(i,KPhase)
        if(.not.AtAbsCoeffOwn(KPhase)) then
          if(Poprve.and..not.isED.and..not.isTOF) then
            call CrlReadAbsCoeff(1,AtType(i,KPhase),
     1                           AtAbsCoeff(i,KPhase),ich)
            if(ich.ne.0) Poprve=.false.
          else
            AtAbsCoeff(i,KPhase)=0.
          endif
        endif
        wmol=wmol+pomw*pomm
        ami=ami+AtAbsCoeff(i,KPhase)*pomm*.1
      enddo
      NInfo=NInfo+1
      write(TextInfo(NInfo),'(''Molecular weight = '',f8.2)') wmol
      dx=float(NUnits(KPhase))*wmol*1.e24/
     1   (CellVol(1,KPhase)*AvogadroNumber)
      NInfo=NInfo+1
      write(TextInfo(NInfo),'(''Calculated density ='',f8.4,
     1                     '' g.cm**(-3)'')') dx
      ami=ami*float(NUnits(KPhase))/CellVol(1,KPhase)
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        if(.not.isED) then
          if(klam(KDatBlock).gt.0) then
            write(Veta,'(''('',a2,''-Kalfa) ='',f9.3,'' mm**-1'')')
     1        LamTypeD(klam(KDatBlock)),ami
          else
            write(Veta,'('' ='',f9.3,'' mm**-1'')') ami
          endif
        endif
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        write(Veta,'('' ='',f9.6,'' mm**-1'')') ami
      endif
      if(.not.isED.and.Radiation(KDatBlock).ne.ElectronRadiation) then
        NInfo=NInfo+1
        TextInfo(NInfo)='Absorption coefficient mi'//Veta(:idel(Veta))
      endif
9999  return
      end
