      subroutine EM50ReadOwnFormFactorMag(ia)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ff(7)
      character*256 FileName,Veta,Slovo
      character*10 :: id(2) =(/'ffmag    ',
     1                         'ffmagtype'/)
      logical EqIgCase,WizardModeIn
      equivalence (IdNumbers(1),IdFFMag),
     1            (IdNumbers(2),IdFFMagType)
      WizardModeIn=WizardMode
      WizardMode=.false.
      ln=NextLogicNumber()
      FileName=fln(:ifln)//'_ffedit.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      Veta='# Here you can supply and edit magnetic form factors for '//
     1     'atom type: '//
     1     AtTypeMag(ia,KPhase)(:idel(AtTypeMag(ia,KPhase)))//' #'
      idl=idel(Veta)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA) Veta(:idl)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA)
      if(ExistNeutronData.or..not.ExistM90) then
        write(ln,FormA)
        Veta='# seven coefficients of magnetic form factor as in IT'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFMag)(:idel(id(IdFFMag)))
        write(Veta,'(8f8.3)')(FFMag(i,ia,KPhase),i=1,7)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFMag)(:idel(id(IdFFMag)))
        write(ln,FormA)
        Veta='# FFMagType = 0  <j0> type - as it is'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='#           = 1  non <j0> type - individual values '//
     1       'will be multiplied by (sin(th)/Lambda)^2'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFMagType)(:idel(id(IdFFMagType)))
        if(TypeFFMag(ia,KPhase).eq.0) then
          Veta='   0'
        else
          Veta='   1'
        endif
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFMagType)(:idel(id(IdFFMagType)))
      endif
      call CloseIfOpened(ln)
      call FeEdit(FileName)
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3000  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3000
      k=0
      call kus(Veta,k,Slovo)
      if(.not.EqIgCase(Slovo,'begin')) go to 3000
      call kus(Veta,k,Slovo)
      i=LocateInStringArray(id,2,Slovo,IgnoreCaseYes)
      if(i.eq.IdFFMag) then
        nt=7
        nr=nt
      else if(i.eq.IdFFMagType) then
        nt=1
        nr=1
      endif
      ip=1
3100  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3100
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) go to 4000
      call vykric(Veta)
      k=0
      if(i.eq.IdFFMag) then
        call StToReal(Veta,k,ff(ip),nr,.false.,ich)
        if(ich.gt.1) then
          n=ich-2
          ip=ip+n
          nr=nr-n
        else if(ich.ne.0) then
          go to 3000
        endif
      else if(i.eq.IdFFMagType) then
        call StToInt(Veta,k,TypeFFMag(ia,KPhase),nt,.false.,ich)
        if(TypeFFMag(ia,KPhase).ne.0) TypeFFMag(ia,KPhase)=1
      endif
      go to 3100
4000  if(i.eq.IdFFMag) then
        call CopyVek(ff,FFMag(1,ia,KPhase),nt)
      endif
      go to 3000
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      WizardMode=WizardModeIn
      return
100   format('begin ',a)
101   format('end ',a)
      end
