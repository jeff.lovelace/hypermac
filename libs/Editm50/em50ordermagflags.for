      subroutine EM50OrderMagFlags(ipg,isg,MagFlag,NMagFlag,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension MagFlag(*)
      ich=0
      if(ipg.eq.5.or.ipg.eq.11.or.ipg.eq.23) then
        NMagFlag=NMagFlag+1
        MagFlag(NMagFlag)=MagFlag(1)*MagFlag(2)
      else if(ipg.eq.8) then
        i=1
        do n=1,NMagFlag
          i=i*MagFlag(n)
        enddo
        NMagFlag=NMagFlag+1
        MagFlag(NMagFlag)=i
      else if(ipg.eq.15.or.ipg.eq.27) then
        if(ipg.eq.27) then
          i=MagFlag(1)*MagFlag(2)
        else
          i=MagFlag(2)
        endif
        do n=3,NMagFlag
          MagFlag(n-1)=MagFlag(n)
        enddo
        MagFlag(NMagFlag)=i
      else if(ipg.ge.16.and.ipg.le.20) then
        if(ipg.ne.17.and.ipg.ne.20.and.MagFlag(1).lt.0) go to 9000
        if((mod(isg-148,2).eq.1.and.ipg.eq.18).or.
     1     (mod(isg-155,2).eq.1.and.ipg.eq.19).or.
     2     isg.eq.162.or.isg.eq.163) then
          if(NMagFlag.gt.2) then
            do n=3,NMagFlag
              MagFlag(n-1)=MagFlag(n)
            enddo
            NMagFlag=NMagFlag-1
          endif
        endif
        if(ipg.eq.17.or.ipg.eq.20) then
          NMagFlag=NMagFlag+1
          MagFlag(NMagFlag)=MagFlag(1)
          MagFlag(1)=1
        endif
      else if(ipg.eq.28) then
        if(MagFlag(1).lt.0.or.MagFlag(2).lt.0) go to 9000
        NMagFlag=3
        MagFlag(3)=1
        MagFlag(2)=MagFlag(1)
      else if(ipg.eq.29) then
        NMagFlag=4
        MagFlag(4)=MagFlag(2)
        MagFlag(3)=1
        MagFlag(2)=MagFlag(1)*MagFlag(4)
        MagFlag(1)=MagFlag(1)*MagFlag(4)
      else if(ipg.eq.30.or.ipg.eq.31.or.ipg.eq.32) then
        i=MagFlag(3)
        MagFlag(3)=MagFlag(2)
        MagFlag(2)=i
        if(ipg.eq.30.or.ipg.eq.31) then
          if(MagFlag(1).ne.MagFlag(2)) go to 9000
          MagFlag(2)=1
        else if(ipg.eq.32) then
          if(MagFlag(1)*MagFlag(3).lt.0) go to 9000
          NMagFlag=4
          MagFlag(4)=MagFlag(3)
          MagFlag(3)=1
          i=MagFlag(1)
          MagFlag(1)=i*MagFlag(2)
          MagFlag(2)=i*MagFlag(4)
        endif
        if(ipg.ne.32.and.MagFlag(3).lt.0) go to 9000
      endif
      if(ipg.ge.24.and.ipg.le.27) then
        i=MagFlag(3)
        MagFlag(3)=MagFlag(2)
        MagFlag(2)=i
      endif
      go to 9999
9000  ich=1
9999  return
      end
