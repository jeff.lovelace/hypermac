      subroutine EM50Composition
      use Basic_mod
      use Atoms_mod
      use EditM50_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension fmult(:),ip(3)
      character*256 EdwStringQuest,Veta
      character*80  FormulaNew,FormulaOld
      character*7   MenuIon(8)
      character*2   LastType
      integer RolMenuSelectedQuest,FFTypeOld,iar(1),FeRGBCompress,
     1        AtNumMax,CrwStateQuest
      integer :: WFMaxAtNum(3) = (/86,35,54/)
      integer :: WFChybi(4,3) = reshape
     1                          ((/ 0, 0, 0, 0,
     2                             10,18,21, 0,
     3                             57,58,60,62/),shape(WFChybi))
      logical FeYesNo,eqrv,EqIgCase,CrwLogicQuest,ChargeDensitiesOld,
     1        ExistFile,Chybi
      allocatable fmult
      save nEdwFormula,nEdwZ,nRolMenuAtType,nRolMenuIon,MenuIon,ip,
     1     nButtCalculateDensity,nButtCalculateFormula,nCrwFFTypeFirst,
     2     LastType,nCrwFFTypeLast,nButtAllDefault,nEdwAtRadius,
     3     ChargeDensitiesOld,nButtDefault,nButtDefine,nEdwRGB,
     4     XminColor,XmaxColor,YminColor,YmaxColor,nLblColor,
     5     nButtTypicalDist,nCrwJana,nCrwMolly,nCrwCoppens,nCrwEDAbs,
     6     nLblCharge1,nLblCharge2
      entry EM50CompositionMake(id)
      nnn=0
      ChargeDensitiesOld=ChargeDensities
      il=1
      tpom=5.
      Veta='Formula %units'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=300.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'%Formula','L',dpom,EdwYd,
     1                    1)
      nEdwFormula=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
      il=il+1
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwZ=EdwLastMade
      if(NUnits(KPhase).le.0) then
        if(NSymm(KPhase).gt.0.and.NLattVec(KPhase).gt.0) then
          NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
        else
          NUnits(KPhase)=1
        endif
      endif
      call FeQuestIntEdwOpen(nEdwZ,NUnits(KPhase),.false.)
      Veta='For%mula from M40'
      xpom=xpom+dpom+50.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateFormula=ButtonLastMade
      il=il+1
      Veta='Calculate %density'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      xpom=xpom+dpom+5.
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='%Atom type:'
      xpom=tpom+FeTxLengthUnder(Veta)+30.
      dpom=60.+EdwYd
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      Veta='%Ion (for X-xay):'
      tpom=xpom+dpom+25.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuIon=RolMenuLastMade
      Veta='Atom %radius:'
      tpom=xpom+dpom+25.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwAtRadius=EdwLastMade
      il=il+1
      Veta='Drawing color:'
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblColor=LblLastMade
      dpom=40.
      tpom=tpom+FeTxLength(Veta)+dpom+20.
      Veta='R:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      do i=1,3
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        xpom=xpom+80.
        tpom=xpom-15.
        if(i.eq.1) then
          nEdwRGB=EdwLastMade
          Veta='G:'
        else if(i.eq.2) then
          Veta='B:'
        endif
      enddo
      XMinColor=EdwXMinQuest(nEdwRGB+2)-230.
      XMaxColor=XMinColor+dpom
      YMinColor=EdwYMinQuest(nEdwRGB+2)
      YMaxColor=EdwYMaxQuest(nEdwRGB+2)
      xpom=xpom+10.
      Veta='D%efine/modify typical distances'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtTypicalDist=ButtonLastMade
      il=il+1
      Veta='%Set to default'
      dpom=FeTxLengthUnder(Veta)+10.
      xsh=20.
      xpom=xdqp*.5-dpom*1.5-xsh
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefault=ButtonLastMade
      Veta='A%ll to default'
      xpom=xpom+xsh+dpom
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAllDefault=ButtonLastMade
      Veta='Define o%wn'
      xpom=xpom+xsh+dpom
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDefine=ButtonLastMade
      if(AllowChargeDensities) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Form factors from:'
        call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
        il=il+1
        xpom=15.
        tpom=xpom+CrwgXd+5.
!        ilp=-10*il
        ilp=il
!        il=ilp
        xsh=xdqp*.333333
        do i=1,6
          if(i.eq.1) then
            Veta='IT Vol.C 6.1.1.1-6.1.1.3'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='s%teps as in IT'
          else if(i.eq.2) then
            nCrwFFTypeFirst=CrwLastMade
            Veta='e%quidistant step 0.05'
          else if(i.eq.3) then
            il=ilp
            xpom=xpom+xsh
            tpom=tpom+xsh
            Veta='IT Vol.C 6.1.1.4'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='anal%ytical form'
          else if(i.eq.4) then
            xpom=xpom+xsh
            tpom=tpom+xsh
            il=ilp
            Veta='STO wave functions'
            call FeQuestLblMake(id,xpom,il,Veta,'L','B')
            Veta='JA%NA'
          else if(i.eq.5) then
            Veta='M%OLLY'
          else if(i.eq.6) then
            Veta='Co%ppens WEB page'
          else if(i.eq.7) then
            Veta='XD-CR'
          else if(i.eq.8) then
            Veta='XD-BBB'
          else if(i.eq.9) then
            Veta='XD-SCM'
          else if(i.eq.10) then
            Veta='XD-VM'
          endif
!          il=il-7
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.4) then
            nCrwJana=CrwLastMade
          else if(i.eq.5) then
            nCrwMolly=CrwLastMade
          else if(i.eq.6) then
            nCrwCoppens=CrwLastMade
          else if(i.eq.7) then
            nCrwXDCR=CrwLastMade
          else if(i.eq.8) then
            nCrwXDBBB=CrwLastMade
          else if(i.eq.9) then
            nCrwXDSCM=CrwLastMade
          else if(i.eq.10) then
            nCrwXDVM=CrwLastMade
          endif
        enddo
        nCrwFFTypeLast=CrwLastMade
!        il=il-7
        il=il+1
        Veta='INFORMATION: STO wave functions are taken by default '//
     1       'from the selected source.'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        tpom=tpom+FeTxLength(Veta(1:12))+3.
        Veta='However the source can modified in "Multipole '//
     1       'parameters" individually for each atomic type.'
        nLblCharge1=LblLastMade
        ilp=-10*il-7
!        il=il-7
        call FeQuestLblMake(id,tpom,ilp,Veta,'L','N')
        nLblCharge2=LblLastMade
        nCrwEDAbs=0
      else if(Radiation(KDatBlock).eq.ElectronRadiation) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Form factors:'
        call FeQuestLblMake(id,xdqp*.5,il,Veta,'C','B')
        xpom=15.
        tpom=xpom+CrwgXd+5.
        Veta='from IT Vol.C %4.3.1.1'
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          nCrw=CrwLastMade
          if(i.eq.1) then
            nCrwFFTypeFirst=CrwLastMade
            Veta='%calculated by Weickenmeier and Kohl procedure'
          else if(i.eq.2) then
            nCrwFFTypeLast=CrwLastMade
            il=-il*10-8
            tpom=tpom+40.
            xpom=xpom+40.
            Veta='a%pply absorption effects'
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,0)
            nCrwEDAbs=CrwLastMade
            call FeQuestCrwOpen(CrwLastMade,AbsFlagED.eq.3)
          endif
          call FeQuestCrwOpen(nCrw,i.eq.2.eqv.UseWKS)
        enddo
      else
        nCrwFFTypeFirst=0
        nCrwFFTypeLast =0
        nCrwEDAbs=0
      endif
      LastAtom=1
      go to 2000
      entry EM50CompositionRefresh
2000  if(Formula(KPhase).eq.' ') then
        i=ButtonDisabled
      else
        i=ButtonOff
      endif
      call FeQuestButtonOpen(nButtCalculateDensity,i)
      if(i.eq.ButtonOff.and.NAtCalc.le.0) i=ButtonDisabled
      call FeQuestButtonOpen(nButtCalculateFormula,i)
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
        AtNumMax=0
        do i=1,NAtFormula(KPhase)
          AtNumMax=max(AtNumMax,nint(AtNum(i,KPhase)))
        enddo
      endif
      if(LastAtom.gt.NAtFormula(KPhase)) then
        LastType=' '
      else
        LastType=AtTypeMenu(LastAtom)
      endif
      if(LastType.ne.' ') then
        call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                          NAtFormula(KPhase),LastAtom)
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1                'ions.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'formfac/ions.dat'
        endif
        if(.not.ExistFile(Veta)) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        read(ln,FormA) Veta
        if(Veta(1:1).ne.'#') rewind ln
2100    read(ln,FormA,end=2120) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,AtType(LastAtom,KPhase))) go to 2100
        call CloseIfOpened(ln)
        call StToInt(Veta,k,iar,1,.false.,ichp)
        if(ichp.ne.0) go to 2120
        n=iar(1)
        do i=1,n
          call Kus(Veta,k,MenuIon(i))
          if(EqIgCase(MenuIon(i),AtTypeFull(LastAtom,KPhase))) m=i
        enddo
        go to 2140
2120    n=1
        m=1
        MenuIon(1)=AtType(LastAtom,KPhase)
2140    call FeQuestRolMenuOpen(nRolMenuIon,MenuIon,n,m)
        call FeQuestRealEdwOpen(nEdwAtRadius,AtRadius(LastAtom,KPhase),
     1                          .false.,.false.)
        call FeQuestLblOn(nLblColor)
        nEdw=nEdwRGB
        call FeRGBUncompress(AtColor(LastAtom,KPhase),ip(1),ip(2),ip(3))
        do i=1,3
          call FeQuestIntEdwOpen(nEdw,ip(i),.false.)
          call FeQuestEudOpen(nEdw,0,255,1,0.,0.,0.)
          nEdw=nEdw+1
        enddo
        call FeDrawFrame(XminColor,YminColor,
     1                   XmaxColor-XminColor,YmaxColor-YminColor,
     2                   2.,White,Gray,Black,.false.)
        call FeFillRectangle(XminColor,XmaxColor,YminColor,YmaxColor,
     1                       4,0,0,AtColor(LastAtom,KPhase))
        call FeQuestButtonOpen(nButtDefault,ButtonOff)
        call FeQuestButtonOpen(nButtAllDefault,ButtonOff)
        call FeQuestButtonOpen(nButtDefine,ButtonOff)
        call FeQuestButtonOpen(nButtTypicalDist,ButtonOff)
      else
        call FeQuestLblOff(nLblColor)
      endif
      if(AllowChargeDensities) then
        if(NDim(KPhase).gt.3) then
          i2=3
          ChargeDensities=.false.
        else
          i2=6
        endif
        if(ChargeDensities) then
          do i=1,3
            if(EqIgCase(NameOfWaveFile,MenuWF(i))) then
              ntp=i+3
              exit
            endif
          enddo
          call FeQuestLblOn(nLblCharge1)
          call FeQuestLblOn(nLblCharge2)
        else
          if(FFType(KPhase).eq.-62) then
            ntp=1
          else if(FFType(KPhase).eq.-9) then
            ntp=3
          else
            ntp=2
          endif
          call FeQuestLblOff(nLblCharge1)
          call FeQuestLblOff(nLblCharge2)
        endif
        nCrw=nCrwFFTypeFirst
        do i=1,6
!          if(i.le.i2.and.(i.le.6.or.XDBnkDir.ne.' ')) then
          if(i.le.i2) then
            call FeQuestCrwOpen(nCrw,i.eq.ntp)
          else
            call FeQuestCrwDisable(nCrw)
          endif
          nCrw=nCrw+1
        enddo
        if(ChargeDensities) then
          nCrw=nCrwJana
          nCrwMaHo=0
          ntpp=0
          do i=1,3
            Chybi=.false.
!            do j=1,4
!              if(WFChybi(j,i).le.0) cycle
!              do k=1,NAtFormula(KPhase)
!                if(nint(AtNum(k,KPhase)).eq.WFChybi(j,i)) then
!                  Chybi=.true.
!                  go to 2150
!                endif
!              enddo
!            enddo
!2150        if(AtNumMax.gt.WFMaxAtNum(i).or.Chybi) then
!              call FeQuestCrwOff(nCrw)
!              call FeQuestCrwDisable(nCrw)
!              if(ntp.eq.i+3) ntp=0
!            else if(ntpp.eq.0) then
!              nCrwMaHo=nCrw
!              ntpp=i+3
!            endif
!            nCrw=nCrw+1
          enddo
          if(ntp.eq.0) then
            if(ntpp.ne.0) then
              call FeQuestCrwOn(nCrwMaHo)
              NameOfWaveFile=MenuWF(ntpp-3)
            endif
          endif
        endif
      endif
      go to 9999
      entry EM50CompositionCheck
      Klic=0
      go to 2200
      entry EM50CompositionUpdate
      Klic=1
2200  call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
      call EM50SaveFormFactors
      if(Klic.eq.1) go to 9999
      if(CheckType.eq.EventEdw) then
        if(CheckNumber.eq.nEdwFormula) then
          FormulaOld=Formula(KPhase)
          NAtFormulaOld=NAtFormula(KPhase)
          Formula(KPhase)=EdwStringQuest(nEdwFormula)
          Veta=Formula(KPhase)
          do i=1,idel(Veta)
            if(index(Cifry,Veta(i:i)).gt.0) Veta(i:i)=' '
          enddo
          do i=1,idel(FormulaOld)
            if(index(Cifry,FormulaOld(i:i)).gt.0) FormulaOld(i:i)=' '
          enddo
          call PitFor(1,ich)
          if(ich.ne.0) go to 2290
          if(NAtFormula(KPhase).gt.0) then
            call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
            if(NAtFormulaOld.le.0) FFType(KPhase)=-62
          else
            go to 9999
          endif
          call PitFor(0,ich)
          if(ich.ne.0) go to 2290
          if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
          allocate(AtTypeMenu(NAtFormula(KPhase)))
          call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                           NAtFormula(KPhase))
          if(NAtFormulaOld.gt.0) then
            call EM50SmartUpdateFormFactors
          else
            do i=1,NAtFormula(KPhase)
              FFBasic(1,i,KPhase)=-3333.
              AtRadius(i,KPhase)=-3333.
            enddo
          endif
          do i=1,NAtFormula(KPhase)
            if(AtRadius(i,KPhase).lt.-3000.)
     1        call EM50ReadOneFormFactor(i)
            if(FFBasic(1,i,KPhase).lt.-3000.) then
              if(ChargeDensities) then
                CoreValSource(i,KPhase)='Default'
                TypeCore(i,KPhase)=0
                TypeVal(i,KPhase)=0
                HNSlater(i,KPhase)=0
                HZSlater(i,KPhase)=0.
              endif
              call EM50OneFormFactorSet(i)
            endif
          enddo
          call FeQuestButtonOff(nButtCalculateDensity)
          if(NAtCalc.gt.0) call FeQuestButtonOff(nButtCalculateFormula)
          go to 2000
        else if(CheckNumber.eq.nEdwAtRadius) then
          call FeQuestRealAFromEdw(CheckNumber,
     1                             AtRadius(LastAtom,KPhase))
          go to 9999
        else if(CheckNumber.ge.nEdwRGB.and.CheckNumber.le.nEdwRGB+2)
     1    then
          i=CheckNumber-nEdwRGB+1
          call FeQuestIntFromEdw(CheckNumber,ip(i))
          AtColor(LastAtom,KPhase)=FeRGBCompress(ip(1),ip(2),ip(3))
          call FeFillRectangle(XminColor,XmaxColor,YminColor,YmaxColor,
     1                         4,0,0,AtColor(LastAtom,KPhase))
          go to 9999
        endif
2290    EVentType=CheckType
        EVentNumber=CheckNumber
      else if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtCalculateDensity.or.
     1          CheckNumber.eq.nButtCalculateFormula) then
          if(allocated(fmult)) deallocate(fmult)
          allocate(fmult(NAtFormula(KPhase)))
          call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
          if(CheckNumber.eq.nButtCalculateDensity) then
            Formula(KPhase)=EdwStringQuest(nEdwFormula)
            if(Formula(KPhase).ne.' ') call PitFor(0,ich)
            NInfo=0
          else
            pom=float(NLattVec(KPhase)*NSymm(KPhase))/
     1          float(NUnits(KPhase))
            call CopyVek(AtMult(1,KPhase),fmult,NAtFormula(KPhase))
            call SetRealArrayTo(AtMult(1,KPhase),NAtFormula(KPhase),0.)
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              j=isf(i)
              pomm=ai(i)*pom*CellVol(1,KPhase)/CellVol(iswa(i),KPhase)
              if(KModA(1,i).gt.0) then
                pomm=pomm*a0(i)
              else if(KFA(2,i).gt.0) then
                pomm=pomm*uy(2,KModA(2,i),i)
              endif
              AtMult(j,KPhase)=AtMult(j,KPhase)+pomm
            enddo
            do i=1,NAtFormula(KPhase)
              if(i.eq.1) then
                FormulaNew=AtType(i,KPhase)
              else
                FormulaNew=FormulaNew(:idel(FormulaNew)+1)//
     1               AtType(i,KPhase)(:idel(AtType(i,KPhase)))
              endif
              if(abs(AtMult(i,KPhase)-1.).lt..0001) cycle
              write(Cislo,'(f15.3)') AtMult(i,KPhase)
              call ZdrcniCisla(Cislo,1)
              FormulaNew=FormulaNew(:idel(FormulaNew))//
     1                   Cislo(:idel(Cislo))
            enddo
            NInfo=1
            write(TextInfo(NInfo),'(''Formula from M40 : '',a)')
     1        FormulaNew(:idel(FormulaNew))
          endif
          call EM50CalcDenAbs(ich)
          if(ich.ne.0) then
            TextInfo(1)='Error during calculation of density and '//
     1                  'absorption coefficient.'
            ich=0
          endif
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          if(CheckNumber.eq.nButtCalculateFormula.and.
     1       .not.eqrv(AtMult(1,KPhase),fmult,NAtFormula(KPhase),.001))
     2      then
            if(FeYesNo(-1.,-1.,'Do you want to update the formula '//
     1                      'correspondingly?',0)) then
              Formula(KPhase)=FormulaNew
              call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
            else
              call CopyVek(fmult,AtMult(1,KPhase),NAtFormula(KPhase))
            endif
          endif
          deallocate(fmult)
          go to 9999
        else if(CheckNumber.eq.nButtDefault) then
          call EM50ReadOneFormFactor(LastAtom)
          call EM50OneFormFactorSet(LastAtom)
        else if(CheckNumber.eq.nButtAllDefault) then
          call EM50ReadAllFormFactors
          call EM50FormFactorsSet
        else if(CheckNumber.eq.nButtDefine) then
          call EM50ReadOwnFormFactor(LastAtom,0)
        else if(CheckNumber.eq.nButtTypicalDist) then
          call EM50TypicalDist
        endif
        go to 2000
      else if(CheckType.eq.EVentRolMenu) then
        if(CheckNumber.eq.nRolMenuAtType) then
          LastAtom=RolMenuSelectedQuest(nRolMenuAtType)
        else if(CheckNumber.eq.nRolMenuIon) then
          Veta=AtTypeFull(LastAtom,KPhase)
          AtTypeFull(LastAtom,KPhase)=
     1      MenuIon(RolMenuSelectedQuest(nRolMenuIon))
          if(.not.EqIgCase(Veta,AtTypeFull(LastAtom,KPhase))) then
            call EM50OneFormFactorSet(LastAtom)
            call EM50ReadOneFormFactor(LastAtom)
          endif
        endif
        go to 2000
      else if(CheckType.eq.EventCrw) then
        if(Radiation(KDatBlock).ne.ElectronRadiation) then
          nCrw=nCrwFFTypeFirst
          ChargeDensities=.false.
          FFTypeOld=FFType(KPhase)
          do i=1,6
            if(CrwLogicQuest(nCrw)) then
              ChargeDensities=.false.
              if(i.eq.1) then
                FFType(KPhase)=-62
              else if(i.eq.2) then
                FFType(KPhase)=121
              else if(i.eq.3) then
                FFType(KPhase)=-9
              else if(i.ge.4) then
                ChargeDensities=.true.
                if(.not.ChargeDensitiesOld) then
                  call EM50ReadAllMultipoles
                  ChargeDensitiesOld=.true.
                endif
                FFType(KPhase)=121
                NameOfWaveFile=MenuWF(i-3)
              endif
            endif
            nCrw=nCrw+1
          enddo
          if(FFType(KPhase).ne.FFTypeOld) call EM50FormFactorsSet
        else
          if(CheckNumber.ge.nCrwFFTypeFirst.and.
     1       CheckNumber.le.nCrwFFTypeLast) then
            UseWKS=CrwLogicQuest(nCrwFFTypeLast)
            if(UseWKS) then
              if(CrwStateQuest(nCrwEDAbs).eq.CrwDisabled)
     1          call FeQuestCrwOpen(nCrwEDAbs,AbsFlagED.eq.3)
            else
              if(CrwStateQuest(nCrwEDAbs).ne.CrwDisabled) then
                if(CrwLogicQuest(nCrwEDAbs)) then
                  AbsFlagED=3
                else
                  AbsFlagED=0
                endif
                call FeQuestCrwDisable(nCrwEDAbs)
              endif
            endif
          else if(CheckNumber.eq.nCrwEDAbs) then
            if(CrwLogicQuest(nCrwEDAbs)) then
              AbsFlagED=3
            else
              AbsFlagED=0
            endif
          endif
        endif
        go to 2000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
      endif
9999  return
      end
