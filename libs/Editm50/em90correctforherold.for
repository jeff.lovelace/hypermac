      subroutine EM90CorrectForHerold(ipg)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real RMPom(9)
      logical EqRV
      if(ipg.eq.24) then
        RMPom=0
        RMPom(1)=-1.
        RMPom(4)= 1.
        RMPom(5)= 1.
        RMPom(9)=-1.
        do i=1,NSymm(KPhase)
          if(EqRV(RM(1,i,1,KPhase),RMPom,9,.001)) then
            pom=s6(4,i,1,KPhase)
            do j=1,NSymm(KPhase)
              if(RM6(16,j,1,KPhase).lt.0.) then
                s6(4,j,1,KPhase)=s6(4,j,1,KPhase)-pom
                call od0do1(s6(4,j,1,KPhase),s6(4,j,1,KPhase),1)
              endif
            enddo
            exit
          endif
        enddo
      endif
      return
      end
