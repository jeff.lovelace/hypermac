      subroutine GenerPGFile
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension io(3),QuSymm(3,3)
      character*256 Veta
      character*8 SPG(39)
      character*3 t3
      character*2 t2m
      logical FirstTime
      data Spg/'P1','P-1',
     1         'P2','Pm','P2/m',
     2         'P222','Pmm2','Pmmm',
     3         'P4','P-4','P4/m','P422','P4mm','P-4m2','P4/mmm',
     4         'P3','P-3','P312','P321','R32','P3m1','P31m','R3m',
     5         'P-3m1','P-31m','R32',
     7         'P6','P-6','P6/m','P622','P6mm','P-6m2','P-62m','P6/mmm',
     8         'P23','Pm-3','P432','P-43m','Pm-3m'/
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      KPhase=1
      FirstTime=.true.
      if(.not.allocated(rm)) then
        n=1
        call AllocateSymm(1,1,1,NPhase)
      endif
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'pgroup-pom.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup-pom.dat'
      endif
      call OpenFile(46,Veta,'formatted','unknown')
      call SetRealArrayTo(ShSg(1,KPhase),6,0.)
      do i=1,39
        Grupa(KPhase)=SPG(i)
        Monoclinic(KPhase)=2
        call EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
        write(46,'(a7,1x,a,i4)') SPG(i)(2:8),SmbPGO(i),NSymm(KPhase)
        do j=1,NSymm(KPhase)
          call SmbOp(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),1,1.,t3,t2m,io,
     1               n,det)
          write(Veta,'(''('',2(i2,'',''),i2,'')'')') io
          call zhusti(Veta)
          write(46,'(9i3,5x,a2,1x,a)')(nint(RM6(k,j,1,KPhase)),k=1,9),
     1                                t3,Veta(:idel(Veta))
        enddo
      enddo
      go to 9999
9000  call FeChybne(-1.,-1.,'Konex souboru.',' ',SeriousError)
9999  return
      end
