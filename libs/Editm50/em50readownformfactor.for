      subroutine EM50ReadOwnFormFactor(ia,Klic)
      use Basic_mod
      use EditM50_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real ff(121),CSTOPom(7,7,40),ZSTOPom(7,7,40)
      integer NCoefSTOPom(7,7),idt(MxDatBlock),NSTOPom(7,7,40)
      character*256 FileName,Veta,Slovo
      character*12 id(6)
      logical EqIgCase,WizardModeIn
      equivalence (IdNumbers(1) ,IdFFBasic),
     1            (IdNumbers(2) ,IdFFCore),
     2            (IdNumbers(3) ,IdFFVal),
     3            (IdNumbers(4) ,IdFFAnom),
     4            (IdNumbers(5) ,IdFFNeutron),
     5            (IdNumbers(6) ,IdSTO)
      data id/'ffbasic','ffcore','ffval','f''f"','ffneutron',
     1        'STO-function'/
      WizardModeIn=WizardMode
      WizardMode=.false.
      ln=NextLogicNumber()
      FileName=fln(:ifln)//'_ffedit.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(Klic.le.1) then
        Veta='# Here you can supply and edit form factors for atom '//
     1       'type: '//AtTypeMenu(ia)(:idel(AtTypeMenu(ia)))//' #'
      else
        Veta='# Here you can supply and edit STO wave function for '//
     1       'atom type: '//AtTypeMenu(ia)(:idel(AtTypeMenu(ia)))//' #'
      endif
      idl=idel(Veta)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA) Veta(:idl)
      write(ln,FormA1)('#',i=1,idl)
      write(ln,FormA)
      if(AllowChargeDensities) then
        if(ChargeDensities.eqv.Klic.eq.1) then
          Veta='# Form factors are to be typed as'
          write(ln,FormA) Veta(:idel(Veta))
          if(FFType(KPhase).eq.-9) then
            Veta='# coefficients for analytical approximation '//
     1           'according to IT vol.C 6.1.1.4'
          else
            if(FFType(KPhase).gt.0) then
              write(Cislo,'(i5)') FFType(KPhase)
              call zhusti(Cislo)
            else
              Cislo='62'
            endif
            Veta='# a table defined in '//Cislo(:idel(Cislo))//
     1           ' points of sin(theta)/lambda distributed'
          endif
          write(ln,FormA) Veta(:idel(Veta))
          if(FFType(KPhase).ne.-9) then
            if(FFType(KPhase).gt.0) then
              write(Cislo,'(f10.2)') float(FFType(KPhase)-1)*.05
              call Zhusti(Cislo)
              Veta='# equidistanly '
            else
              Veta='# according to IT vol.C table 6.1.1.1'
              Cislo='6.00'
            endif
            Veta=Veta(:idel(Veta)+1)//'over the interval from 0.00 '//
     1           'to '//Cislo(:idel(Cislo))
            write(ln,FormA) Veta(:idel(Veta))
          endif
          write(ln,FormA)
          if(ChargeDensities) then
            ik=3
          else
            ik=1
          endif
          nt=iabs(FFType(KPhase))
          do i=1,ik
            if(i.eq.1) then
              call CopyVek(FFBasic(1,ia,KPhase),ff,nt)
              k=IdFFBasic
            else if(i.eq.2) then
              call CopyVek(FFCore(1,ia,KPhase),ff,nt)
              k=IdFFCore
            else
              call CopyVek(FFVal(1,ia,KPhase),ff,nt)
              k=IdFFVal
            endif
            write(ln,100) id(k)(:idel(id(k)))
            if(FFType(KPhase).eq.-9) then
              write(ln,'(7f11.6)')(ff(j),j=1,nt)
            else
              write(ln,'(8f9.4)')(ff(j),j=1,nt)
            endif
            write(ln,101) id(k)(:idel(id(k)))
          enddo
        else if(ChargeDensities.eqv.Klic.eq.2) then
          Veta='# STO wave function'
          write(ln,FormA) Veta(:idel(Veta))
          write(ln,100) id(6)(:idel(id(6)))
          do j=1,7
            do i=1,7
              if(NCoefSTOA(i,j,ia,KPhase).le.0) cycle
              write(ln,102) i,OrbitName(j),NCoefSTOA(i,j,ia,KPhase)
              write(ln,103)(CSTOA(i,j,k,ia,KPhase),
     1                      ZSTOA(i,j,k,ia,KPhase),
     2                      NSTOA(i,j,k,ia,KPhase),
     3                      k=1,NCoefSTOA(i,j,ia,Kphase))
            enddo
          enddo
          write(ln,101) id(6)(:idel(id(6)))
        endif
        NFPrime=0
        do i=1,NDatBlock
          if(Radiation(i).eq.XRayRadiation.and.KAnRef(i).eq.0)
     1      NFPrime=NFPrime+1
        enddo
        if(Klic.le.0.and.NFPrime.gt.0) then
          write(ln,FormA)
          write(ln,100) id(IdFFAnom)(:idel(id(IdFFAnom)))
            ndt=0
            do i=1,NDatBlock
              if(Radiation(i).eq.XRayRadiation.and.KAnRef(i).eq.0) then
                ndt=ndt+1
                idt(ndt)=i
                write(Veta,'(2f8.4)') FFra(ia,KPhase,i),
     1                                FFia(ia,KPhase,i)
                write(Cislo,'(f15.5)') LamAve(i)
                call zhusti(Cislo)
                Veta(21:)='! the wave length '//Cislo(:idel(Cislo))
                write(ln,FormA) Veta(:idel(Veta))
              endif
            enddo
            write(ln,101) id(IdFFAnom)(:idel(id(IdFFAnom)))
        endif
      endif
      if(ExistNeutronData.or..not.ExistM90) then
        write(ln,FormA)
        Veta='# scattering lengths for neutrons'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='# the first number defines the real component'
        write(ln,FormA) Veta(:idel(Veta))
        Veta='# the second one the imaginary component'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA)
        write(ln,100) id(IdFFNeutron)(:idel(id(IdFFNeutron)))
        write(Veta,'(2f8.4)') FFn(ia,KPhase),FFni(ia,KPhase)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,101) id(IdFFNeutron)(:idel(id(IdFFNeutron)))
      endif
      call CloseIfOpened(ln)
      call FeEdit(FileName)
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
3000  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3000
      k=0
      call kus(Veta,k,Slovo)
      if(.not.EqIgCase(Slovo,'begin')) go to 3000
      call kus(Veta,k,Slovo)
      i=LocateInStringArray(id,6,Slovo,IgnoreCaseYes)
      if(i.eq.IdFFBasic.or.i.eq.IdFFCore.or.i.eq.IdFFVal) then
        no=1
        nr=nt
      else if(i.eq.IdFFAnom) then
        no=ndt
        nr=2
      else if(i.eq.IdFFNeutron) then
        no=1
        nr=2
      else if(i.eq.IdSTO) then
        NCoefSTOPom=0
        go to 3200
      endif
      ip=1
3100  read(ln,FormA,end=9999) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3100
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) go to 4000
      call vykric(Veta)
      k=0
      call StToReal(Veta,k,ff(ip),nr,.false.,ich)
      if(ich.gt.1) then
        n=ich-2
        ip=ip+n
        nr=nr-n
      else if(ich.ne.0) then
        go to 3000
      else if(i.eq.IdFFAnom) then
        j=idt(ndt-no+1)
        FFra(ia,KPhase,j)=ff(1)
        FFia(ia,KPhase,j)=ff(2)
        no=no-1
      endif
      go to 3100
3200  read(ln,FormA,end=3000) Veta
      if(Veta.eq.' '.or.Veta(1:1).eq.'#') go to 3200
      k=0
      call kus(Veta,k,Slovo)
      if(EqIgCase(Slovo,'end')) then
        do j=1,7
          do i=1,7
            NCoefSTOA(i,j,ia,KPhase)=NCoefSTOPom(i,j)
            if(NCoefSTOPom(i,j).eq.0) cycle
            do ii=1,NCoefSTOPom(i,j)
              CSTOA(i,j,ii,ia,KPhase)=CSTOPom(i,j,ii)
              ZSTOA(i,j,ii,ia,KPhase)=ZSTOPom(i,j,ii)
              NSTOA(i,j,ii,ia,KPhase)=NSTOPom(i,j,ii)
            enddo
          enddo
        enddo
        go to 4000
      endif
      k=0
      call Kus(Veta,k,Cislo)
      read(Cislo,'(i1)',err=3000) i
      j=0
      do ii=1,7
        if(EqIgCase(OrbitName(ii),Cislo(2:2))) then
          j=ii
          exit
        endif
      enddo
      if(j.eq.0) go to 3000
      call StToInt(Veta,k,NCoefSTOPom(i,j),1,.false.,ich)
      if(ich.ne.0) go to 3000
      do ii=1,NCoefSTOPom(i,j)
        if(mod(ii-1,4)+1.eq.1) then
          read(ln,FormA,end=3000) Veta
          k=0
        endif
        call StToReal(Veta,k,ff,3,.false.,ich)
        if(ich.ne.0) go to 3000
        CSTOPom(i,j,ii)=ff(1)
        ZSTOPom(i,j,ii)=ff(2)
        NSTOPom(i,j,ii)=nint(ff(3))
      enddo
      go to 3200
4000  if(i.eq.IdFFBasic) then
        call CopyVek(ff,FFBasic(1,ia,KPhase),nt)
      else if(i.eq.IdFFCore) then
        call CopyVek(ff,FFCore(1,ia,KPhase),nt)
      else if(i.eq.IdFFVal) then
        call CopyVek(ff,FFVal(1,ia,KPhase),nt)
      else if(i.eq.IdFFNeutron) then
        FFn(ia,KPhase)=ff(1)
        FFni(ia,KPhase)=ff(2)
      endif
      no=no-1
      if(no.le.0) then
        go to 3000
      else
        go to 3100
      endif
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      WizardMode=WizardModeIn
      return
100   format('begin ',a)
101   format('end ',a)
102   format(i2,a1,i3)
103   format(4(f12.8,f12.4,i2))
      end
