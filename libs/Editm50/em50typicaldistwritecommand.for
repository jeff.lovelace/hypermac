      subroutine EM50TypicalDistWriteCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/TypicalDistC/ nRolMenuFirst,nEdwDist,TypDist,AtNames(2)
      character*(*) Command
      character*80  ErrString
      character*2   AtNames
      integer RolMenuSelectedQuest
      save /TypicalDistC/
      ich=0
      Command='typdist'
      nRolMenu=nRolMenuFirst
      do i=1,2
        AtNames(i)=AtType(RolMenuSelectedQuest(nRolMenu),KPhase)
        call Uprat(AtNames(i))
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nRolMenu=nRolMenu+1
      enddo
      call FeQuestRealFromEdw(nEdwDist,TypDist)
      write(Cislo,100) TypDist
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      go to 9999
9000  call FeChybne(-1.,-1.,ErrString,' ',SeriousError)
9100  ich=1
9999  return
100   format(f15.4)
      end
