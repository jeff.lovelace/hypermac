      subroutine EM50ReadAnom(At,ffrOut,ffiOut,KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*256 t256
      character*80  Radka
      character*2   Atp
      logical ExistFile,EqIgCase
      dimension xx(4),ff(4,2),xp(3)
      equivalence (t256,t80)
      ln=0
      if(LamAve(KDatB).le.0.) then
        ffrOut=-1.
        ffiOut=-1.
        go to 9999
      else
        ffrOut=0.
        ffiOut=0.
      endif
      if(EqIgCase(At,'D')) then
        AtP='H'
      else
        AtP=At
      endif
      Energy=ToKeV/LamAve(KDatB)
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//
     1              'anom.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/anom.dat'
      endif
      if(.not.ExistFile(t256)) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA80) Radka
      if(Radka(1:1).ne.'#') rewind ln
2100  read(ln,FormA80,end=9999) Radka
      k=0
      call kus(Radka,k,Cislo)
      if(.not.EqIgCase(Cislo,AtP)) go to 2100
2200  read(ln,FormA80) Radka
      j=1
2300  read(ln,*,err=9999,end=9999) xp
      jo=j
      if(xp(1).gt.Energy.or.j.lt.2) j=j+1
      if(jo.eq.j) then
        ff(j-1,1)=ff(j,1)
        ff(j-1,2)=ff(j,2)
        xx(j-1)  =xx(j)
      endif
      xx(j)  =xp(1)
      ff(j,1)=xp(2)
      ff(j,2)=xp(3)
      if(j.ge.4) then
        ffrOut=anint(Finter4(ff(1,1),xx,Energy)*10000.)*.0001
        ffiOut=anint(Finter4(ff(1,2),xx,Energy)*10000.)*.0001
        go to 9999
      else
        go to 2300
      endif
9999  call CloseIfOpened(ln)
      return
      end
