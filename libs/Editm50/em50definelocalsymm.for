      subroutine EM50DefineLocalSymm
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*25 :: Label=('Local symmetry operator #')
      dimension RMat(:,:,:),SVec(:,:),RMatO(:,:,:),SVecO(:,:),
     1          rm6lo(:,:,:),rmlo(:,:,:),s6lo(:,:,:)
      allocatable RMat,SVec,RMatO,SVecO,rm6lo,rmlo,s6lo
      NMatMax=max(16,MaxNSymmL)
      n=NDim(KPhase)
      allocate(RMat(n,n,NMatMax),SVec(n,NMatMax),
     1         rm6lo(n**2,NMatMax,NPhase),rmlo(9,NMatMax,NPhase),
     2         s6lo(n,NMatMax,NPhase))
      do KPh=1,NPhase
        do i=1,NSymmL(KPh)
          call CopyMat(rm6l(1,i,1,KPh),rm6lo(1,i,KPh),n)
          call CopyMat(rml (1,i,1,KPh),rmlo (1,i,KPh),3)
          call CopyVek(s6l(1,i,1,KPh),s6lo(1,i,KPh),n)
        enddo
      enddo
      do i=1,NSymmL(KPhase)
        call CopyMat(rm6l(1,i,1,KPhase),RMat(1,1,i),n)
        call CopyVek(s6l(1,i,1,KPhase),SVec(1,i),n)
      enddo
      ns=NSymmL(KPhase)
      ich=0
      id=NextQuestId()
      Veta=' '
      xqd=40.*float(n+3)
      if(n.eq.3) then
        xqd=xqd+PropFontWidthInPixels*float(3+4*n)
      else
        xqd=xqd+PropFontWidthInPixels*float(4+5*n)
      endif
      il=n+3
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,Label,'C','B')
      nLblLabel=LblLastMade
      call FeQuestLblOff(LblLastMade)
      dpom=40.
      do i=1,n
        tpom=20.
        il=il+1
        if(n.le.3) then
          Veta=SmbX(i)//'''='
        else
          Veta=SmbX6(i)//'''='
        endif
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) dll=FeTxLength(Veta)+5.
        xpom=tpom+dll
        tpom=xpom+dpom+5.
        if(i.eq.1) nLblFirst=LblLastMade
        do j=1,n+1
          tpom=xpom+dpom+5.
          if(j.le.n) then
            if(n.le.3) then
              Veta='* '//SmbX(j)//'+'
            else
              Veta='* '//SmbX6(j)//'+'
            endif
          else
            Veta=' '
            dpom=dpom*2.
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                        0)
          if(j.eq.n+1) dpom=dpom*.5
          if(i.eq.1.and.j.eq.1) then
            nEdwMatFirst=EdwLastMade
            dlp=FeTxLength(Veta)+5.
          endif
          xpom=tpom+dlp
        enddo
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      dpom=60.
      Veta='%New'
      tpom=xqd*.5-dpom*1.5-10.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtNew=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+10.
      Veta='%Delete'
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+10.
      Veta='%Go to #'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwGoTo=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,0,ns,1,0.,0.,0.)
      NActOld=-1
      if(ns.gt.0) then
        NAct=1
      else
        NAct=0
      endif
1400  if(NAct.gt.0.and.NAct.ne.NActOld) then
        write(Cislo,FormI15) NAct
        call Zhusti(Cislo)
        call FeQuestLblChange(nLblLabel,Label//Cislo(:idel(Cislo)))
        nEdw=nEdwMatFirst
        nLbl=nLblFirst
        do i=1,n
          call FeQuestLblOn(nLbl)
          do j=1,n+1
            if(j.le.n) then
              pom=RMat(j,i,NAct)
            else
              pom=SVec(i,NAct)
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.true.)
            nEdw=nEdw+1
          enddo
          nLbl=nLbl+1
        enddo
      else if(NAct.le.0) then
        call FeQuestLblOff(nLblLabel)
        nEdw=nEdwMatFirst
        nLbl=nLblFirst
        do i=1,n
          call FeQuestLblOff(nLbl)
          do j=1,n+1
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
          nLbl=nLbl+1
        enddo
      endif
      NActOld=NAct
      if(ns.le.0) then
        call FeQuestButtonDisable(nButtDelete)
        call FeQuestEdwDisable(nEdwGoTo)
      else
        call FeQuestButtonOff(nButtDelete)
        call FeQuestIntEdwOpen(nEdwGoTo,NAct,.false.)
        call FeQuestEudOpen(nEdwGoTo,0,ns,1,0.,0.,0.)
      endif
1500  call FeQuestEvent(id,ich)
      if(NAct.gt.0.and.
     1   (CheckType.ne.EventButton.or.CheckNumber.ne.nButtDelete)) then
        nEdw=nEdwMatFirst
        do i=1,n
          do j=1,n+1
            call FeQuestRealFromEdw(nEdw,pom)
            if(j.le.n) then
              RMat(j,i,NAct)=pom
            else
              SVec(i,NAct)=pom
            endif
            nEdw=nEdw+1
          enddo
        enddo
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwGoTo) then
        call FeQuestIntFromEdw(nEdwGoTo,NAct)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew) then
        if(ns.ge.NMatMax) then
          allocate(RMatO(n,n,ns),SVecO(n,ns))
          do i=1,ns
            call CopyMat(RMat(1,1,i),RMatO(1,1,i),n)
            call CopyVek(SVec(1,i),SVecO(1,i),n)
          enddo
          deallocate(RMat,SVec)
          NMatMax=2*NMatMax
          allocate(RMat(n,n,NMatMax),SVec(n,NMatMax))
          do i=1,ns
            call CopyMat(RMatO(1,1,i),RMat(1,1,i),n)
            call CopyVek(SVecO(1,i),SVec(1,i),n)
          enddo
          deallocate(RMatO,SVecO)
        endif
        ns=ns+1
        NAct=ns
        call UnitMat(RMat(1,1,ns),n)
        call SetRealArrayTo(SVec(1,ns),n,0.)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDelete)
     1  then
        do i=NAct+1,ns
          call CopyMat(RMat(1,1,i),RMat(1,1,i-1),n)
          call CopyVek(SVec(1,i),SVec(1,i-1),n)
        enddo
        ns=ns-1
        if(ns.gt.0) then
          NAct=max(NAct-1,1)
          NActOld=-1
        else
          NAct=0
        endif
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(allocated(rm6l)) deallocate(rm6l,rml,s6l,ISwSymmL,KwSymL,rtl,
     1                               rc3l,rc4l,rc5l,rc6l,GammaIntL,
     2                               LXYZMode,XYZMode,NXYZMode,
     3                               MXYZMode)
      MaxNSymmL=max(MaxNSymmL,ns)
      if(MaxNSymmL.gt.0) then
        allocate(rm6l     (MaxNDim**2,MaxNSymmL  ,MaxNComp,NPhase),
     1           rml      (9         ,MaxNSymmL  ,MaxNComp,NPhase),
     2           s6l      (MaxNDim   ,MaxNSymmL  ,MaxNComp,NPhase),
     3           rtl      (36        ,MaxNSymmL  ,MaxNComp,NPhase),
     4           rc3l     (100       ,MaxNSymmL  ,MaxNComp,NPhase),
     5           rc4l     (225       ,MaxNSymmL  ,MaxNComp,NPhase),
     6           rc5l     (441       ,MaxNSymmL  ,MaxNComp,NPhase),
     7           rc6l     (784       ,MaxNSymmL  ,MaxNComp,NPhase),
     8           GammaIntL(9         ,MaxNSymmL  ,MaxNComp,NPhase),
     9           KwSymL  (mxw       ,MaxNSymmL   ,MaxNComp,NPhase),
     a           ISwSymmL           (MaxNSymmL   ,MaxNComp,NPhase),
     1           LXYZMode(3*(MaxNSymmL+1),NPhase),
     2           XYZMode (3*(MaxNSymmL+1),3*(MaxNSymmL+1),NPhase),
     3           NXYZMode(NPhase),MXYZMode(NPhase))
        call SetIntArrayTo(NXYZMode,NPhase,0)
        call SetIntArrayTo(MXYZMode,NPhase,0)
        do KPh=1,NPhase
          if(KPh.eq.KPhase) cycle
          do i=1,NSymmL(KPh)
            call CopyMat(rm6lo(1,i,KPh),rm6l(1,i,1,KPh),NDim(KPh))
            call CopyMat(rmlo (1,i,KPh),rml (1,i,1,KPh),3)
            call CopyVek(s6lo(1,i,KPh),s6l(1,i,1,KPh),NDim(KPh))
          enddo
        enddo
        do i=1,ns
          call CopyMat(RMat(1,1,i),rm6l(1,i,1,KPhase),n)
          call MatBlock3(RMat(1,1,i),rml(1,i,1,KPhase),n)
          call CopyVek(SVec(1,i),s6l(1,i,1,KPhase),n)
        enddo
        NSymmL(KPhase)=ns
      endif
9999  if(allocated(RMat)) deallocate(RMat,SVec,rm6lo,rmlo,s6lo)
      return
      end
