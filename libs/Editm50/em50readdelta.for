      subroutine EM50ReadDelta(rmp,s,SymmString)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(NDim(KPhase),NDim(KPhase)),s(NDim(KPhase))
      character*(*) SymmString(NDim(KPhase))
      character*80 Veta,Radka
      call FeBoldFont
      Radka='Complete translational part for the operator:'
      xqd=FeTxLength(Radka)+10.
      call FeNormalFont
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,NDim(KPhase)+2,Radka,0,
     1                   LightGray,-1,0)
      dpom=40.
      xpom=float(NDim(KPhase))*FeTxLength('XXX')+dpom+10.
      tpom=(xqd-xpom)*.5
      xpom=(xqd+xpom)*.5-dpom
      il=1
      call CodeSymm(rmp,s,SymmString,0)
      call MakeSymmSt(Radka,SymmString)
      call FeQuestLblMake(id,xqd*.5,il,Radka,'C','B')
      il=il+1
      call FeQuestLblMake(id,tpom+10.,il,'Rotation','L','N')
      call FeQuestLblMake(id,xpom-15.,il,'Translation','L','N')
      il=il+1
      do i=1,NDim(KPhase)
        write(Veta,'(6i3)')(nint(rmp(i,j)),j=1,NDim(KPhase))
        if(i.le.3) then
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          call ToFract(s(i),Cislo,15)
          call FeQuestLblMake(id,xpom+EdwMarginSize,il,Cislo,'L','N')
        else
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.4) nEdwFirst=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,s(i),.false.,.true.)
        endif
        il=il+1
      enddo
      call FeQuestEvent(id,ich)
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=4,NDim(KPhase)
          call FeQuestRealFromEdw(nEdw,s(i))
          nEdw=nEdw+1
        enddo
      endif
      call FeQuestRemove(id)
      return
      end
