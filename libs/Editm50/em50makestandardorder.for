      subroutine EM50MakeStandardOrder
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iar(1),RM6Pom(:,:),s6Pom(:,:),RMPom(9),Tr(9),TrI(9),
     1          RMPomIn(9),ZMagPom(:),RMagPom(:,:),io(3)
      character*80 Veta
      character*8  PointGroup
      character*3  t3
      character*2  t2m
      allocatable RM6Pom,s6Pom,ZMagPom,RMagPom
      logical EqIgCase,EqRV
      if(NGrupa(KPhase).le.1) go to 9999
      if(mod(iabs(CrSystem(KPhase)),10).eq.CrSystemMonoclinic) then
        i=iabs(CrSystem(KPhase))/10
        if(i.eq.1) then
          i=312
        else if(i.eq.2) then
          i=123
        else if(i.eq.3) then
          i=231
        endif
        call SetPermutMat(Tr,3,i,ich)
        call MatInv(Tr,TrI,pom,3)
      else
        call UnitMat(Tr ,3)
        call UnitMat(Tri,3)
      endif
      if(NGrupa(KPhase).eq.2) then
        ipg=2
      else if(NGrupa(KPhase).ge.3.and.NGrupa(KPhase).le.5) then
        ipg=3
      else if(NGrupa(KPhase).ge.6.and.NGrupa(KPhase).le.9) then
        ipg=4
      else if(NGrupa(KPhase).ge.10.and.NGrupa(KPhase).le.15) then
        ipg=5
      else if(NGrupa(KPhase).ge.16.and.NGrupa(KPhase).le.24) then
        ipg=6
      else if(NGrupa(KPhase).ge.25.and.NGrupa(KPhase).le.46) then
        k=0
        do i=1,NSymmN(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,t3,t2m,
     1               io,n,det)
          if(t3(1:1).eq.'2') then
            do j=1,3
              if(io(j).ne.0) then
                if(j.eq.1) then
                  k=231
                else if(j.eq.2) then
                  k=312
                else if(j.eq.3) then
                  k=123
                endif
                go to 1100
              endif
            enddo
          endif
        enddo
1100    ipg=7
        if(k.ne.0) then
          call SetPermutMat(Tr,3,k,ich)
          call MatInv(Tr,TrI,pom,3)
        endif
      else if(NGrupa(KPhase).ge.47.and.NGrupa(KPhase).le.74) then
        ipg=8
      else if(NGrupa(KPhase).ge.75.and.NGrupa(KPhase).le.80) then
        ipg=9
      else if(NGrupa(KPhase).ge.81.and.NGrupa(KPhase).le.82) then
        ipg=10
      else if(NGrupa(KPhase).ge.83.and.NGrupa(KPhase).le.88) then
        ipg=11
      else if(NGrupa(KPhase).ge.89.and.NGrupa(KPhase).le.98) then
        ipg=12
      else if(NGrupa(KPhase).ge.99.and.NGrupa(KPhase).le.110) then
        ipg=13
      else if(NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.122) then
        ipg=14
        if((NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.114).or.
     1     (NGrupa(KPhase).ge.121.and.NGrupa(KPhase).le.122)) then
          ipgp=14
        else
          ipgp=15
        endif
      else if(NGrupa(KPhase).ge.123.and.NGrupa(KPhase).le.142) then
        ipg=15
      else if(NGrupa(KPhase).ge.143.and.NGrupa(KPhase).le.146) then
        ipg=16
      else if(NGrupa(KPhase).ge.147.and.NGrupa(KPhase).le.148) then
        ipg=17
      else if(NGrupa(KPhase).ge.149.and.NGrupa(KPhase).le.155) then
        ipg=18
        if(NGrupa(KPhase).eq.155) then
          ipgp=25
C          32
        else if(mod(NGrupa(KPhase)-148,2).eq.1) then
          ipgp=19
C          312
        else
          ipgp=22
C          321
        endif
      else if(NGrupa(KPhase).ge.156.and.NGrupa(KPhase).le.161) then
        ipg=19
        if(NGrupa(KPhase).ge.160) then
          ipgp=26
C          3m
        else if(mod(NGrupa(KPhase)-155,2).eq.1) then
          ipgp=23
C          3m1
        else
          ipgp=20
C          31m
        endif
      else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.167) then
        ipg=20
        if(NGrupa(KPhase).ge.166) then
          ipgp=27
C          -3m
        else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.163) then
          ipgp=21
C          -31m
        else if(NGrupa(KPhase).ge.164.and.NGrupa(KPhase).le.165) then
          ipgp=24
C          -3m1
        endif
      else if(NGrupa(KPhase).ge.168.and.NGrupa(KPhase).le.173) then
        ipg=21
      else if(NGrupa(KPhase).ge.174.and.NGrupa(KPhase).le.174) then
        ipg=22
      else if(NGrupa(KPhase).ge.175.and.NGrupa(KPhase).le.176) then
        ipg=23
      else if(NGrupa(KPhase).ge.177.and.NGrupa(KPhase).le.182) then
        ipg=24
      else if(NGrupa(KPhase).ge.183.and.NGrupa(KPhase).le.186) then
        ipg=25
      else if(NGrupa(KPhase).ge.187.and.NGrupa(KPhase).le.190) then
        ipg=26
        if(NGrupa(KPhase).le.188) then
          ipgp=33
        else
          ipgp=34
        endif
      else if(NGrupa(KPhase).ge.191.and.NGrupa(KPhase).le.194) then
        ipg=27
      else if(NGrupa(KPhase).ge.195.and.NGrupa(KPhase).le.199) then
        ipg=28
      else if(NGrupa(KPhase).ge.200.and.NGrupa(KPhase).le.206) then
        ipg=29
      else if(NGrupa(KPhase).ge.207.and.NGrupa(KPhase).le.214) then
        ipg=30
      else if(NGrupa(KPhase).ge.215.and.NGrupa(KPhase).le.220) then
        ipg=31
      else if(NGrupa(KPhase).ge.221.and.NGrupa(KPhase).le.230) then
        ipg=32
      endif
      if(ipg.le.13) then
        ipgp=ipg
      else if(ipg.ge.15.and.ipg.le.17) then
        ipgp=ipg+1
      else if(ipg.ge.21.and.ipg.le.25) then
        ipgp=ipg+7
      else if(ipg.ge.27) then
        ipgp=ipg+8
      endif
      PointGroup=SmbPGI(ipgp)
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'pgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9900
1200  read(ln,FormA80,end=9900) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1   go to 1200
      call Kus(Veta,k,Cislo)
      call StToInt(Veta,k,iar,1,.false.,ich)
      if(ich.ne.0) go to 9900
      n=iar(1)
      allocate(RM6Pom(NDimQ(KPhase),n),s6Pom(NDim(KPhase),n))
      allocate(ZMagPom(n),RMagPom(9,n))
      do 1500i=1,n
        read(ln,FormA80,end=9900) Veta
        k=0
        call StToReal(Veta,k,RMPom,9,.false.,ich)
        if(ich.ne.0) go to 9900
        call Multm(TrI,RMPom,RMPomIn,3,3,3)
        call Multm(RMPomIn,Tr,RMPom,3,3,3)
        do j=1,n
          if(EqRV(RM(1,j,1,KPhase),RMPom,9,.001)) then
            call CopyMat(RM6(1,j,1,KPhase),RM6Pom(1,i),NDim(KPhase))
            call CopyVek(s6 (1,j,1,KPhase),s6Pom (1,i),NDim(KPhase))
            ZMagPom(i)=ZMag(j,1,KPhase)
            call CopyMat(RMag(1,j,1,KPhase),RMagPom(1,i),3)
            go to 1500
          endif
        enddo
        go to 9900
1500  continue
      do i=1,n
        call CopyMat(RM6Pom(1,i),RM6(1,i,1,KPhase),NDim(KPhase))
        call MatBlock3(RM6(1,i,1,KPhase),RM(1,i,1,KPhase),NDim(KPhase))
        call CopyVek(s6Pom (1,i),s6 (1,i,1,KPhase),NDim(KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        ZMag(i,1,KPhase)=ZMagPom(i)
        call CopyMat(RMagPom(1,i),RMag(1,i,1,KPhase),3)
      enddo
9900  call CloseIfOpened(ln)
      if(allocated(RM6Pom)) deallocate(RM6Pom,s6Pom)
      if(allocated(ZMagPom)) deallocate(ZMagPom,RMagPom)
9999  return
      end
