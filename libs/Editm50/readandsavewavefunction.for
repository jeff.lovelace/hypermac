      subroutine ReadAndSaveWaveFunction(Atom,iat,AtWaveFile,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xp(3),ZSTOAO(7,7,40),CSTOAO(7,7,40)
      integer ip(2),NSTOAO(7,7,40),NCoefSTOAO(7,7)
      character*(*) AtWaveFile,Atom
      character*256 t256,STOString
      character*1   Orbit
      logical EqIgCase
      do i1=1,7
        do i2=1,7
          NCoefSTOAO(i1,i2)=NCoefSTOA(i1,i2,iat,KPhase)
          NCoefSTOA(i1,i2,iat,KPhase)=0
          do k=1,40
            ZSTOAO(i1,i2,k)=ZSTOA(i1,i2,k,iat,KPhase)
            ZSTOA(i1,i2,k,iat,KPhase)=0.
            CSTOAO(i1,i2,k)=CSTOA(i1,i2,k,iat,KPhase)
            CSTOA(i1,i2,k,iat,KPhase)=0.
            NSTOAO(i1,i2,k)=NSTOA(i1,i2,k,iat,KPhase)
            NSTOA(i1,i2,k,iat,KPhase)=0
          enddo
        enddo
      enddo
      ich=0
      ln=NextLogicNumber()
      n=0
      do i=1,3
        if(EqIgCase(AtWaveFile,MenuWF(i))) then
          n=i
          exit
        endif
      enddo
      call OpenFile(ln,AtWaveFile,'formatted','old')
      if(ErrFlag.ne.0) go to 9100
1100  read(ln,FormA,end=9100) t256
      k=0
      call Kus(t256,k,Cislo)
      if(EqIgCase(Cislo,Atom)) then
1250    read(ln,FormA,end=9100) t256
        if(t256.eq.' ') go to 1250
        if(t256(1:1).eq.' ') then
          read(t256,'(i2,a1,i3)',end=9100,err=9100) i,Orbit,nsto
          j=0
          do ii=1,7
            if(EqIgCase(OrbitName(ii),Orbit)) then
              j=ii
              exit
            endif
          enddo
          if(i.le.0) go to 9100
          NCoefSTOA(i,j,iat,Kphase)=nsto
          read(ln,'(4(f12.8,f12.4,i2))')(CSTOA(i,j,k,iat,KPhase),
     1                                   ZSTOA(i,j,k,iat,KPhase),
     2                                   NSTOA(i,j,k,iat,KPhase),
     1                                   k=1,nsto)
          go to 1250
        endif
      else if(EqIgCase(Cislo,':'//Atom(:idel(Atom)))) then
        nn=0
1300    read(ln,FormA,end=9100) STOString
        k=0
        call kus(STOString,k,Cislo)
        nn=nn+1
        if(nn.gt.9) go to 9100
        if(.not.EqIgCase(Cislo,'STO')) go to 1300
        idl=idel(STOString)
        if(STOString(idl:idl).eq.'=') then
          read(ln,FormA,end=9100) t256
          STOString=STOString(:idl-1)//t256(:idel(t256))
        endif
        k=0
        call kus(STOString,k,Cislo)
        if(.not.EqIgCase(Cislo,'STO')) go to 9100
1320    call kus(STOString,k,Cislo)
        Orbit=Cislo(2:2)
        j=0
        do i=1,7
          if(EqIgCase(OrbitName(i),Orbit)) then
            j=i
            exit
          endif
        enddo
        if(j.le.0) go to 9100
        read(Cislo(1:1),'(i1)',err=9100) i
        call StToInt(STOString,k,ip,2,.false.,ich)
        if(ich.ne.0) go to 9100
        NCoefSTOA(i,j,iat,KPhase)=ip(2)
        ii=0
1350    if(mod(ii,4).eq.0) then
          read(ln,FormA,end=9100) t256
          kk=0
        endif
        ii=ii+1
        call StToReal(t256,kk,xp,3,.false.,ich)
        NSTOA(i,j,ii,iat,KPhase)=nint(xp(3))
        CSTOA(i,j,ii,iat,KPhase)=xp(1)
        ZSTOA(i,j,ii,iat,KPhase)=xp(2)
        if(ii.lt.NCoefSTOA(i,j,iat,KPhase)) go to 1350
        if(k.lt.len(STOString)) go to 1320
      else
        go to 1100
      endif
      go to 9999
9100  ich=1
      do i1=1,7
        do i2=1,7
          NCoefSTOA(i1,i2,iat,KPhase)=NCoefSTOAO(i1,i2)
          do k=1,40
            ZSTOA(i1,i2,k,iat,KPhase)=ZSTOAO(i1,i2,k)
            CSTOA(i1,i2,k,iat,KPhase)=CSTOAO(i1,i2,k)
            NSTOA(i1,i2,k,iat,KPhase)=NSTOAO(i1,i2,k)
          enddo
        enddo
      enddo
9999  call CloseIfOpened(ln)
      return
      end
