      subroutine TestComp
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Z3(9),Zd(9),PomMat(9),RelMat(3,3),RelMatI(3,3),xx(3),
     1          xf(3),xg(3)
      character*80 Veta
      call MatBlock3(zv(1,2,KPhase),Z3,NDim(KPhase))
      i=NDim(KPhase)*3+1
      do j=1,3*NDimI(KPhase),3
        do l=0,2
          call CopyVek(zv(i,2,KPhase),Zd(j),3)
        enddo
        i=i+NDim(KPhase)
      enddo
      call CopyMat(Z3,RelMat,3)
      call TrMat(Qu(1,1,1,KPhase),PomMat,3,NDimI(KPhase))
      call Cultm(Zd,PomMat,RelMat,3,NDimI(KPhase),3)
      call MatInv(RelMat,RelMatI,pom,3)
      call FeLstMake(0.,-1.,60,20,' ')
      do i=1,3
        write(Veta,'(3f10.4)')(RelMat(i,j),j=1,3)
        call FeLstWriteLine(Veta,-1)
      enddo
      do i=1,3
        call SetRealArrayTo(xx,3,0.)
        xx(i)=1.
        call Multm(MetTens(1,1,KPhase),xx,xg,3,3,1)
        do j=1,3
          call SetRealArrayTo(xx,3,0.)
          xx(j)=1.
          call Multm(RelMatI,xx,xf,3,3,1)
          pom=ScalMul(xf,xg)
          uhel=acos(pom/(CellPar(i,1,KPhase)*CellPar(j,2,KPhase)))/ToRad
          write(Veta,'(''uhel'',i1,''-'',i1,'':'',5f10.4)') i,j,uhel
          call FeLstWriteLine(Veta,-1)
        enddo
      enddo
5000  call FeEvent(0)
      if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1   (EventType.eq.EventMouse.and.
     2    (EventNumber.eq.JeLeftDown.or.EventNumber.eq.JeRightDown)))
     3  then

      else
        go to 5000
      endif
9999  return
      end
