      subroutine EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,
     1                        UplnySymbol,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension tau(idl),vtp(6),tv(3,8),tvo(3,6),pp(6),pq(3),mpp(3),
     1          rmp(36),sp(6),spp(6),QuPom(3),QuSum(3),RM6Old(:,:),
     2          S6Old(:,:)
      character*(*) itxt
      character*2 nty
      character*5 ir
      character*20 ito
      character*60 HallSymbol
      character*80 t80,errtxt
      real NulVek(6)
      logical AskForDelta,EQRV0,EQRV,UplnySymbol
      data ir/'12346'/,ito/'xyz"''*12345abcnuvwd;'/
      data tv/.5,.0,.0,.0,.5,.0,.0,.0,.5,.5,.5,.5,.25,.0,.0,
     1        .0,.25,.0,.0,.0,.25,.25,.25,.25/
      data tvo/1.,0.,0.,0.,1.,0.,0.,0.,1.,1.,1.,0.,1.,-1.,0.,1.,1.,1./
      data NulVek/6*0./
      allocatable RM6Old,S6Old
      ich=0
      Tetra=.false.
      if(NDimI(KPhase).gt.1) then
        NSymmOld=NSymm(KPhase)
        allocate(RM6Old(NDimQ(KPhase),NSymmOld),
     1            S6Old(NDim(KPhase) ,NSymmOld))
        do i=1,NSymmOld
          call CopyMat(RM6(1,i,1,KPhase),RM6Old(1,i),NDim(KPhase))
          call CopyVek( S6(1,i,1,KPhase), S6Old(1,i),NDim(KPhase))
        enddo
      endif
      NSymm(KPhase)=1
      call SetRealArrayTo(rm6(1,1,1,KPhase),maxNDim**2,0.)
      call SetRealArrayTo(s6 (1,1,1,KPhase),maxNDim,0.)
      call SetStringArrayTo(symmc(1,1,1,KPhase),maxNDim,' ')
      call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
      call UnitMat(rm(1,1,1,KPhase),3)
      call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
      call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1              symmc(1,1,1,KPhase),0)
      ISwSymm(NSymm(KPhase),1,KPhase)=1
      ZMag(1,1,KPhase)=1
      call UnitMat(RMag(1,1,1,KPhase),3)
      BratSymm(NSymm(KPhase),KPhase)=.true.
      HallSymbol=itxt
      id=idel(itxt)
1000  if(id.le.0) go to 9999
      if(itxt(1:1).eq.'-') then
        iz=-1
        itxt=itxt(2:)
        id=id-1
      else if(itxt(1:1).eq.'+') then
        iz= 1
        itxt=itxt(2:)
        id=id-1
      else
        iz= 1
      endif
      if(id.le.0) then
        errtxt='rotational information is missing.'
        go to 8000
      endif
      nr=index(ir,itxt(1:1))
      itxt=itxt(2:)
      if(nr.le.0) then
        errtxt='incorrect rotational part.'
        go to 8000
      else if(nr.eq.1) then
        if(idel(itxt).gt.0) then
          errtxt='incorrect rotational part.'
          go to 8000
        else
          go to 5000
        endif
      endif
      Tetra=Tetra.or.nr.eq.4
      ior=-1
      call SetRealArrayTo(vtp,NDim(KPhase),0.)
      pom=0.
1100  if(idel(itxt).gt.0) then
        i=index(ito,itxt(1:1))
        if(i.gt.0) then
          itxt=itxt(2:)
        else
          errtxt='incorrect orientation or translation part.'
          go to 8000
        endif
      else
        i=20
      endif
      if(i.le.6) then
        if(ior.eq.-1) then
          ior=i
          go to 1100
        else
          errtxt='orientation is doubled.'
          go to 8000
        endif
      else if(i.le.19) then
        nt=i-6
        if(nt.le.5) then
          p=nr
          if(nr.eq.5) then
            p=6.
          else if(nr.eq.6.or.nr.eq.7) then
            p=2.
          else if(nr.eq.8) then
            p=3.
          endif
          pom=pom+float(nt)/p
        else
          do i=1,3
            vtp(i)=vtp(i)+tv(i,nt-5)
          enddo
        endif
      else if(i.eq.20) then
        if(ior.lt.0) then
          if(NSymm(KPhase).eq.1) then
            ior=3
          else if(NSymm(KPhase).eq.2.or.NSymm(KPhase).eq.3) then
            if(nr.eq.2.and.(nrold.eq.2.or.nrold.eq.4)) ior=1
            if(nr.eq.2.and.(nrold.eq.3.or.nrold.eq.5)) ior=5
            if(nr.eq.3) ior=6
          endif
        endif
        if(ior.lt.0) then
          errtxt='orientation is not defined.'
          go to 8000
        endif
        if(ior.eq.6) then
          if(nr.eq.3) then
            nr=8
            ior=1
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        else if(ior.eq.4.or.ior.eq.5) then
          if(nr.eq.2) then
            nr=ior+2
            if(NSymm(KPhase).ne.1) then
              ior=iorold
            else
              ior=3
            endif
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        endif
        call EM50SetGen(rmp,nr-1,ior,NDim(KPhase))
        if(iz.eq.-1) call realMatrixToOpposite(rmp,rmp,NDim(KPhase))
        do i=1,3
          sp(i)=vtp(i)+pom*tvo(i,ior)
        enddo
        if(NDim(KPhase).gt.3) then
          k1=0
          k2=0
          n1=0
          n2=0
          if(NDim(KPhase).gt.4) then
            k1=-2
            k2= 2
            if(NDim(KPhase).gt.5) then
              n1=-2
              n2= 2
            endif
          endif
        endif
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)*sqrt(1.2)
            enddo
          enddo
        endif
        do 1600i=1,NDimI(KPhase)
          do j=1,3
            pom=0.
            do k=1,3
              pom=pom+(qui(k,i,KPhase)+quir(k,i,KPhase))*
     1                rmp(k+(j-1)*NDim(KPhase))
            enddo
            pp(j)=pom
          enddo
          jknMn=999999
          do n=n1,n2
            do k=k1,k2
              do 1560j=-4,4
                mmpa=0
                do l=1,3
                  pq(l)=float(j)*(qui(l,1,KPhase)+quir(l,1,KPhase))-
     1                            pp(l)
                  if(NDim(KPhase).gt.4) pq(l)=pq(l)+float(k)*
     1                                (qui(l,2,KPhase)+quir(l,2,KPhase))
                  if(NDim(KPhase).gt.5) pq(l)=pq(l)+float(n)*
     1                                (qui(l,3,KPhase)+quir(l,3,KPhase))
                  mpp(l)=nint(pq(l))
                  mmpa=mmpa+iabs(mpp(l))
                  if(abs(pq(l)-float(mpp(l))).gt..0005) go to 1560
                enddo
                do ivt=1,NLattVec(KPhase)
                  pom=ScalMul(vt6(1,ivt,1,KPhase),pq)
                  if(abs(pom-anint(pom)).gt..0005) go to 1560
                enddo
                jkn=iabs(j)+iabs(k)+iabs(n)+mmpa*10
                if(jkn.lt.jknMn) then
                  ip3=i+3
                  do l=1,3
                    rmp(ip3+NDim(KPhase)*(l-1))=-mpp(l)
                  enddo
                  rmp(ip3+NDim(KPhase)*3)=j
                  if(NDim(KPhase).gt.4)
     1              rmp(ip3+NDim(KPhase)*4)=k
                  if(NDim(KPhase).gt.5)
     1              rmp(ip3+NDim(KPhase)*5)=n
                  jknMn=jkn
                endif
1560          continue
            enddo
          enddo
          if(jknMn.ge.999990) then
            errtxt='generator inconsistent with the modulation vector.'
            go to 8000
          endif
1600    continue
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)/sqrt(1.2)
            enddo
          enddo
        endif
        call SetRealArrayTo(sp(4),NDimI(KPhase),0.)
        if(NDimI(KPhase).gt.1) then
          call od0do1(sp,sp,NDim(KPhase))
          do i=1,NSymmOld
            if(eqrv(rmp,rm6Old(1,i),NDimQ(KPhase),.001)) then
              call CopyVek(s6Old(1,i),spp,NDim(KPhase))
              call od0do1(spp,spp,NDim(KPhase))
              call CopyVek(spp(4),sp(4),NDimI(KPhase))
              exit
            endif
          enddo
        endif
2200    NSymm(KPhase)=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,NSymm(KPhase),1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,NSymm(KPhase),1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,NSymm(KPhase),1,KPhase),maxNDim,
     1                        ' ')
        call CopyMat(rmp,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call CopyVek(sp,s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1                 rm(1,NSymm(KPhase),1,KPhase),
     2                 NDim(KPhase))
        call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                s6(1,NSymm(KPhase),1,KPhase),
     2                symmc(1,NSymm(KPhase),1,KPhase),0)
        ISwSymm(NSymm(KPhase),1,KPhase)=1
        BratSymm(NSymm(KPhase),KPhase)=.true.
        if(Poradi) then
          j=NSymm(KPhase)-1
        else
          j=mod(ior-1,3)+1
        endif
        ZMag(NSymm(KPhase),1,KPhase)=MagFlag(j)
        call CrlMakeRMag(NSymm(KPhase),1)
        if(NDimI(KPhase).gt.0) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol) then
            call multm(rm6(1,NSymm(KPhase),1,KPhase),ShiftSg,sp,
     1                 NDim(KPhase),NDim(KPhase),1)
            do i=1,NDim(KPhase)
              sp(i)=s6(i,NSymm(KPhase),1,KPhase)+ShiftSg(i)-sp(i)
            enddo
            call od0do1(sp,sp,NDim(KPhase))
            s6(4,NSymm(KPhase),1,KPhase)=tau(j)+sp(4)+
     1                                   scalmul(quir(1,1,KPhase),sp)
          else
            if(NDimI(KPhase).gt.1) then
              l1=-2
              l2= 2
            else
              l1=0
              l2=0
            endif
            if(NDimI(KPhase).gt.2) then
              m1=-2
              m2= 2
            else
              m1=0
              m2=0
            endif
            i=NSymm(KPhase)
            do 2270j=1,NDimI(KPhase)
              call Multm(Qu(1,j,1,KPhase),rm(1,i,1,KPhase),QuPom,1,3,3)
              do k=-2,2
                fk=k
                do l=l1,l2
                  fl=l
                  do m=m1,m2
                    fmm=m
                    call CopyVek(QuPom,QuSum,3)
                    do n=1,3
                      QuSum(n)=QuSum(n)+Qu(n,1,1,KPhase)*fk
     1                                 +Qu(n,2,1,KPhase)*fl
                      if(NDimI(KPhase).eq.3)
     1                  QuSum(n)=QuSum(n)+Qu(n,3,1,KPhase)*fmm
                      QuSum(n)=QuSum(n)-anint(QuSum(n))
                    enddo
                    if(EqRV0(QuSum,3,.001)) go to 2270
                  enddo
                enddo
              enddo
              call FeChybne(-1.,-1.,'The set of modulation vectors '//
     1                      'is not complete with respect to the '//
     2                      'symmetry.',' ',SeriousError)
              go to 8100
2270        continue
            if(AskForDelta) then
              n=NSymm(KPhase)
              call CopyVek(s6(1,n,1,KPhase),sp,3)
              call EM50OriginShift(ShSg(1,KPhase),n)
              call EM50ReadDelta(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                           symmc(1,n,1,KPhase))
              call CopyVek(sp,s6(1,n,1,KPhase),3)
              call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                      symmc(1,n,1,KPhase),0)
            endif
          endif
        endif
        call od0do1(s6(1,NSymm(KPhase),1,KPhase),
     1              s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        iorold=ior
        nrold=nr
        if(idel(itxt).gt.0) then
          go to 1000
        else
          go to 5000
        endif
      endif
      go to 1100
5000  if(NCSymm(KPhase).eq.2) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call RealMatrixToOpposite(rm6(1,n,1,KPhase),rm6(1,n,1,KPhase),
     1                            NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call RealMatrixToOpposite(rm(1,n,1,KPhase),rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        BratSymm(n,KPhase)=.true.
        ZMag(n,1,KPhase)=MagFlag(NMagFlag)
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
        NCSymm(KPhase)=1
      endif
      if(MagInv.gt.0) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        if(NDimI(KPhase).eq.1) then
          s6(4,n,1,KPhase)=tau(idl)-
     1      scalmul(quir(1,1,KPhase),s6(1,n,1,KPhase))
        endif
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        ZMag(n,1,KPhase)=-1.
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
      endif
      call CompleteSymm(0,ich)
      go to 9999
8000  write(t80,'(''error in the '',i1,a2,'' Hall''''s generator: '',
     1      a)') NSymm(KPhase)+1,nty(NSymm(KPhase)+1),
     2           HallSymbol(:idel(HallSymbol))
      call FeChybne(-1.,-1.,t80,errtxt(1:idel(errtxt)),SeriousError)
8100  ich=1
9999  if(allocated(RM6Old)) deallocate(RM6Old,S6Old)
      return
      end
