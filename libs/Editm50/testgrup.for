      subroutine TestGrup
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ShSgIn(3),ShSgD(3),QuSymm(3,3),QuOld(3)
      character*256 Veta,Radka
      character*8 GrupaO,GrupaN
      logical Poprve,AskForDelta
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      KPhase=1
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      if(.not.allocated(rm)) call AllocateSymm(1,1,1,NPhase)
      TimeStart=FeEtime()
      call OpenFile(46,'grtest.txt','formatted','unknown')
      n=71
1200  n=n+1
      call OpenFile(45,Veta,'formatted','old')
      read(45,FormA256) Radka
      if(Radka(1:1).ne.'#') rewind 45
      do i=1,n
        read(45,FormA256,end=5000) Radka
        read(Radka,FormSG) ipg,j,j,Grupa(KPhase)
      enddo
      close(45)
      Monoclinic(KPhase)=mod(n,3)+1
      GrupaO=Grupa(KPhase)
      write(46,'(''Grupa : '',a)') GrupaO(:idel(GrupaO))
      write(46,'(80a1)')('=',i=1,8+idel(GrupaO))
      nn=7
      fnn=nn+1
      Poprve=.true.
      AskForDelta=.false.
      do i1=-nn,nn
        ShSgIn(1)=i1/fnn
        do i2=-nn,nn
          ShSgIn(2)=i2/fnn
          do i3=-nn,nn
            ShSgIn(3)=i3/fnn
            call CopyVek(ShSgIn,ShSg(1,KPhase),3)
            call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,ich)
            if(NDim(KPhase).eq.4) then
              call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
              call CopyVek(QuSymm,Qu(1,1,1,KPhase),3)
            endif
            call FindSmbSg(GrupaN,ChangeOrderNo,1)
            if(NDim(KPhase).eq.4)
     1        call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
            if(Poprve) then
              if(GrupaO.ne.GrupaN) then
                write(46,'(''Grupa nactena  : '',a)')
     1            GrupaO(:idel(GrupaO))
                write(46,'(''Grupa zjistena : '',a)')
     1            GrupaN(:idel(GrupaN))
              endif
            endif
            Poprve=.false.
            pom=0.
            do i=1,3
              ShSgD(i)=ShSg(i,KPhase)-ShSgIn(i)
              pom=pom+abs(ShSgD(i))
            enddo
            if(pom.gt..0001) then
              call CopyVek(ShSgD,ShSg(1,KPhase),3)
              call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QuSymm,
     1                        ich)
              if(NDim(KPhase).eq.4) then
                call CopyVek(Qu(1,1,1,KPhase),QuOld,3)
                call CopyVek(QuSymm,Qu(1,1,1,KPhase),3*NDimI(KPhase))
              endif
              call FindSmbSg(GrupaN,ChangeOrderNo,1)
              if(NDim(KPhase).eq.4)
     1          call CopyVek(QuOld,Qu(1,1,1,KPhase),3)
              pom=0.
              do i=1,3
                pom=pom+abs(ShSg(i,KPhase))
              enddo
              if(pom.gt..0001) then
                write(46,'('' Nesouhlas : '',3f8.3,'' <=>'',3f8.3)')
     1            ShSgIn,(ShSgIn(i)+ShSgD(i),i=1,3)
              endif
            endif
          enddo
        enddo
      enddo
      go to 1200
5000  write(46,'(''Cas: '',f8.2)') FeEtime()-TimeStart
      close(46)
      return
      end
