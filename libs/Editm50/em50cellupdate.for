      subroutine EM50CellUpdate(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      character*256 EdwStringQuest
      logical CrSystemDiff
      real QPom(3)
      ich=0
      StructureName=EdwStringQuest(nEdwName)
      if(nEdwPhase.gt.0) PhaseName(KPhase)=EdwStringQuest(nEdwPhase)
      if(EdwStringQuest(nEdwCell).ne.' ') then
        call FeQuestRealAFromEdw(nEdwCell,CellPar(1,1,KPhase))
        if(CellPar(1,1,KPhase).le.0..or.CellPar(2,1,KPhase).le.0..or.
     1     CellPar(3,1,KPhase).le.0.) then
          call FeChybne(-1.,-1.,'negative volume - try again.',' ',
     1                  SeriousError)
          go to 1200
        endif
        csa=cos(CellPar(4,1,KPhase)*torad)
        csb=cos(CellPar(5,1,KPhase)*torad)
        csg=cos(CellPar(6,1,KPhase)*torad)
        pom=1.-csa**2-csb**2-csg**2+2.*csa*csb*csg
        if(pom.le.0.) then
          call FeChybne(-1.,-1.,'angles are incompatible - try '//
     1                  'again.',' ',SeriousError)
          go to 1200
        endif
        CellVol(1,KPhase)=CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1                    CellPar(3,1,KPhase)*sqrt(pom)
        call CheckSystem(CellPar(1,1,KPhase),i,j)
        if(CrSystemDiff(j,CrSystem(KPhase))) then
          Monoclinic(KPhase)=i
          CrSystem(KPhase)=j
        endif
      else
        call FeChybne(-1.,-1.,'you must first define cell parameters.',
     1                ' ',SeriousError)
        go to 1200
      endif
      go to 1300
1200  EventType=EventEdw
      EventNumber=nEdwCell
      go to 9000
1300  call FeQuestRealAFromEdw(nEdwEsd,CellParSU(1,1,KPhase))
      nEdw=nEdwModVek-1
      if(ParentStructure) then
        n=NQMag(KPhase)
      else
        n=NDimI(KPhase)
      endif
      do i=1,n
        nEdw=nEdw+1
        if(EdwStringQuest(nEdw).ne.' ') then
          call FeQuestRealAFromEdw(nEdw,QPom)
          pom=0.
          do j=1,3
            pom=pom+abs(QPom(j))
          enddo
          if(pom.lt..0001) go to 1500
        else
          go to 1600
        endif
        if(ParentStructure) then
          QMag(1:3,i,KPhase)=QPom(1:3)
        else
          Qu(1:3,i,1,KPhase)=QPom(1:3)
        endif
        cycle
1500    call FeChybne(-1.,-1.,'the modulation vector cannot be a zero'//
     1                ' vector.',' ',SeriousError)
        go to 1900
1600    call FeChybne(-1.,-1.,'you must first define modulation '//
     1                'vector(s).',' ',SeriousError)
1900    EventType=EventEdw
        EventNumber=nEdwModVek
        go to 9000
      enddo
      if(KCommen(KPhase).ne.0) then
        if(ICommen(KPhase).eq.0) then
          if(EdwStringQuest(nEdwSupCell).ne.' ') then
            call FeQuestIntAFromEdw(nEdwSupCell,NCommen(1,1,KPhase))
          else
            go to 9999
          endif
        endif
        if(EdwStringQuest(nEdwTzero).ne.' ') then
          call FeQuestRealAFromEdw(nEdwTzero,trez(1,1,KPhase))
        else
          go to 9999
        endif
      endif
      call SetMet(0)
      go to 9999
9000  ich=1
9999  return
      end
