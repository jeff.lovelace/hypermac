      subroutine EM50CompleteCentr(klic,vt6p,nvt,nvtp,nlim,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension vt6p(nvt,*)
      dimension xp(6)
      logical eqrv
      ich=0
      nvp=nvtp
      i=1
1600  i=i+1
      if(i.gt.nvtp) go to 9999
      j=1
1620  j=j+1
      if(j.gt.nvtp) go to 1600
      do k=1,NDim(KPhase)
        xp(k)=vt6p(k,i)+vt6p(k,j)
      enddo
      call od0do1(xp,xp,NDim(KPhase))
      do k=1,nvtp
        if(eqrv(xp,vt6p(1,k),NDim(KPhase),.0001)) go to 1620
      enddo
      if(klic.eq.0) then
        nvtp=nvtp+1
        if(nvtp.gt.nlim) then
          nvtp=nvp
          write(Cislo,'(i5)') nlim
          call zhusti(Cislo)
          call FeChybne(-1.,-1.,'maximun number of '//
     1                  Cislo(:idel(Cislo))//
     2                  ' centring vectors exceeded.',' ',SeriousError)
          go to 9000
        endif
      else
        go to 9000
      endif
      call copyvek(xp,vt6p(1,nvtp),NDim(KPhase))
      go to 1620
9000  ich=1
9999  return
      end
