      subroutine EM50SelectSuperSG(Key,tzero,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ta(:),tap(:),tzero(*),xp(6),TrPom(9)
      character*80 Veta,GrP,t80
      character*20 General
      integer SbwItemPointerQuest,CrSystemSave,MonoclinicSave,UseTabsIn,
     1        SelNt
      logical EqIgCase
      allocatable ta,tap
      UseTabsIn=UseTabs
      ntm=100
      Allocate(ta(ntm))
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_ssg.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      nlim=NCommQ(1,1,KPhase)
      tlim=1./float(nlim)
      ta(1)=tzero(1)
      CrSystemSave=CrSystem(KPhase)
      MonoclinicSave=Monoclinic(KPhase)
      if(Key.eq.0) then
        itgrid=24
        nt=1
        ta(nt)=1./sqrt(2.)*tlim
        trez(1,1,KPhase)=ta(nt)
        call EM50ShowSuperSG(1,GrP,xp,t80,TrPom,xp)
        General=GrP
        Veta='General'//Tabulator//GrP
        write(ln,FormA) Veta(:idel(Veta))
      else
        nt=0
        itgrid=nlim
      endif
      do j=0,itgrid-1
        CrSystem(KPhase)=CrSystemSave
        Monoclinic(KPhase)=MonoclinicSave
        if(Key.eq.0) then
          do k=j,1,-1
            if(mod(itgrid*nlim,k).eq.0.and.mod(j,k).eq.0) then
              iz=(itgrid*nlim)/k
              jz=j/k
              go to 1540
            endif
          enddo
          iz=itgrid*nlim
          jz=j
1540      trez(1,1,KPhase)=float(jz)/float(iz)
        else
          pom=tzero(1)+float(j)*tlim
1550      if(pom.gt..9999) then
            pom=pom-1.
            go to 1550
          endif
          do k=1,24
            fk=k
            jz=nint(pom*fk)
            if(abs(pom-float(jz)/fk).lt..0001) then
              iz=k
              go to 1570
            endif
          enddo
          iz=0
          jz=0
1570      trez(1,1,KPhase)=pom
        endif
        call EM50ShowSuperSG(1,t80,xp,GrP,TrPom,xp)
        if(iz.gt.0) then
          if(jz.gt.0) then
            write(Veta,'(i4,''/'',i4)') jz,iz
            call zhusti(Veta)
          else
            Veta='0'
          endif
          if(Key.eq.0)  then
            write(Cislo,'(i4)') nlim
            call zhusti(Cislo)
            Veta=Veta(:idel(Veta))//' + n/'//Cislo(:idel(Cislo))
          endif
        else
          write(Veta,'(f10.4)') pom
          call zhusti(Veta)
        endif
        if(Key.eq.0) then
          if(EqIgCase(General,t80)) cycle
          Veta=Veta(:idel(Veta))//
     1         Tabulator//t80
        else
          write(t80,'(3f10.5)')(ShSg(i,KPhase),i=1,3)
          call ZdrcniCisla(t80,3)
          Veta=Veta(:idel(Veta))//
     1         Tabulator//GrP(:idel(GrP))//Tabulator//t80(:idel(t80))
        endif
        nt=nt+1
        if(nt.ge.ntm) then
           Allocate(tap(ntm))
           call CopyVek(ta,tap,ntm)
           ntm=ntm+100
           Deallocate(ta)
           Allocate(ta(ntm))
           call CopyVek(tap,tap,nt)
           Deallocate(tap)
        endif
        write(ln,FormA) Veta(:idel(Veta))
        ta(nt)=trez(1,1,KPhase)
        if(abs(ta(nt))-tzero(1).lt..0001) SelNt=Nt
      enddo
      call CloseIfOpened(ln)
1650  if(nt.gt.100) then
        call FeMsgOut(-1.,-1.,
     1              'Warning: Not enough space to complete the table')
      endif
      id=NextQuestId()
      if(Key.eq.0) then
        xqd=250.
      else
        xqd=400.
      endif
      nmax=min(nt,15)
      ilk=min(ifix((float(nmax+1)*MenuLineWidth+2.)/QuestLineWidth)+2,
     1        15)
      ilk=max(ilk,6)
      Veta='Select supercell spacegroup'
      call FeQuestCreate(id,-1.,-1.,xqd,ilk,Veta,0,LightGray,0,0)
      xpom=xqd*.5-50.
      il=1
      call FeQuestLblMake(id, 10.,il,'Tzero','L','N')
      call FeQuestLblMake(id,110.,il,'Space group','L','N')
      if(Key.ne.0)
     1  call FeQuestLblMake(id,210.,il,'Origin shift','L','N')
      UseTabs=NextTabs()
      xpom=100.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=200.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      il=ilk
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_ssg.tmp')
      call FeQuestSbwShow(nSbw,1)
      call FeQuestSbwItemOff(nSbw,1)
      call FeQuestSbwItemOn(nSbw,SelNt)
3500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 3500
      endif
      if(ich.eq.0) tzero(1)=ta(SbwItemPointerQuest(nSbw))
5200  call FeQuestRemove(id)
9999  Deallocate(ta)
      call DeleteFile(fln(:ifln)//'_ssg.tmp')
      CrSystem(KPhase)=CrSystemSave
      Monoclinic(KPhase)=MonoclinicSave
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
      end
