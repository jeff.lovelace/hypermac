      subroutine MatUnpack(m,ih,ihmn,ihd,n)
      include 'fepc.cmn'
      dimension ih(n),ihd(n),ihmn(n)
      j=m-1
      do i=1,n-1
        k=mod(j,ihd(i))
        ih(i)=k+ihmn(i)
        j=j/ihd(i)
      enddo
      ih(n)=j+ihmn(n)
      return
      end
