      subroutine Fourier
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      include 'refine.cmn'
      character*256 Veta,FlnOld
      integer nxn(6),iorienn(6),NPeaksO(2),PageO
      logical FeYesNo,ExistFile
      real xfmnn(6),xfmxn(6),xdfn(6)
      real, allocatable :: Table1(:),Table2(:)
      call FouPrelim
      if(ErrFlag.ne.0) go to 9000
      call FouSynthesis
      if(ErrFlag.ne.0) go to 9000
      if(mapa.eq.10) then
        lsto=lst
        NPeaksO=NPeaks
        LineO=Line
        PageO=Page
        call CopyFile(Fln(:iFln)//'.l51',Fln(:iFln)//'.z51')
        FlnOld=Fln
        Fln=FouRefStr
        iFln=idel(Fln)
        call iom50(0,0,Fln(:iFln)//'.m50')
        call CopyFile(Fln(:iFln)//'.m50',Fln(:iFln)//'.z50')
        LstOpened=.false.
        if(.not.ExistFile(Fln(:iFln)//'.m80')) then
          ln=NextLogicNumber()
          lst=ln
          call OpenFile(ln,Fln(:iFln)//'.l60','formatted','unknown')
          call RefOpenCommands
          NacetlInt(nCmdncykl)=0
          NacetlInt(nCmdCallFour)=0
          NacetlInt(nCmdDoLeBail)=0
          call RefRewriteCommands(1)
          ShowInfoOnScreen=.false.
          call Refine(0,RefineEnd)
          call CloseListing
          LstOpened=.false.
          close(lst,status='delete')
        endif
        call CopyFile(FlnOld(:idel(FlnOld))//'.z51',Fln(:iFln)//'.l51')
        call iom50(1,0,Fln(:iFln)//'.m50')
        call FouOpenCommands
        FouRefStr=' '
        NacetlInt(nCmdmapa)=4
        call FouRewriteCommands(1)
        if(allocated(SinTable)) deallocate(SinTable,CosTable)
        call NewPg(1)
        ln=NextLogicNumber()
        lst=ln
        call FouPrelim
        if(ErrFlag.ne.0) go to 5000
        call FouSynthesis
        if(ErrFlag.ne.0) go to 5000
        if(lpeaks.ne.0.and..not.ContourCallFourier) call FouPeaks(0,0)
        call MoveFile(Fln(:iFln)//'.z50',Fln(:iFln)//'.m50')
        call CloseListing
        Fln=FlnOld
        iFln=idel(Fln)
        call iom50(0,0,Fln(:iFln)//'.m50')
        call iom40(0,0,Fln(:iFln)//'.m40')
        call DefaultFourier
        call NactiFourier
        if(ErrFlag.ne.0) go to 5000
        lst=lsto
        NPeaks=NPeaksO
        Line=LineO
        Page=PageO
        LstOpened=.true.
        if(allocated(Table1)) deallocate(Table1,Table2)
        allocate(Table1(nxny),Table2(nxny))
        lni=NextLogicNumber()
        call OpenMaps(lni,fln(:ifln)//'.m81',nxny,0)
        if(ErrFlag.ne.0) go to 5000
        read(lni,rec=1,err=5000) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),
     1                           xdf,iorien,mapa,nsubs,MameSatelity,
     2                           nonModulated(KPhase)
        lnr=NextLogicNumber()
        Veta=FouRefStr(:idel(FouRefStr))//'.m81'
        call OpenMaps(lnr,Veta,nxnyn,0)
        if(ErrFlag.ne.0) go to 5000
        if(nxnyn.ne.nxny) go to 5100
        read(lnr,rec=1,err=5000) nxn,nxny,nmapn,
     1                          (xfmnn(i),xfmxn(i),i=1,6),
     2                           xdfn,iorienn,mapan,nsubsn
        do i=1,NDim(KPhase)
          if(nx(i).ne.nxn(i).or.iorien(i).ne.iorienn(i)) go to 5100
        enddo
        do i=1,NDim(KPhase)
          if(abs(xfmn(i)-xfmnn(i)).gt..0001.or.
     1       abs(xfmx(i)-xfmxn(i)).gt..0001.or.
     2       abs(xdf(i)-xdfn(i)).gt..0001) go to 5100
        enddo
        if(nmap.ne.nmapn.or.mapan.ne.4.or.nsubsn.ne.nsubs) go to 5100
        lno=NextLogicNumber()
        call OpenMaps(lno,fln(:ifln)//'.z81',nxnyn,1)
        if(ErrFlag.ne.0) go to 5000
        write(lno,rec=1,err=5000) nx,nxny,nmap,
     1                           (xfmn(i),xfmx(i),i=1,6),
     2                            xdf,iorien,mapa,nsubs,MameSatelity,
     3                            nonModulated(KPhase)
        rmax=0.
        rmin=0.
        irec=1
        do i=1,nmap
          irec=irec+1
          read(lni,rec=irec,err=5000) Table1
          read(lnr,rec=irec,err=5000) Table2
          do j=1,nxny
            Table1(j)=Table1(j)-Table2(j)
            rmax=max(rmax,Table1(j))
            rmin=min(rmin,Table1(j))
          enddo
          write(lno,rec=irec) Table1
        enddo
        irec=irec+1
        write(lno,rec=irec) rmax,rmin
        call newln(2)
        write(lst,FormA)
        write(Cislo,'(f15.2)') rmax
        call ZdrcniCisla(Cislo,1)
        Veta='Maximal density : '//Cislo(:idel(Cislo))
        write(Cislo,'(f15.2)') rmin
        call ZdrcniCisla(Cislo,1)
        Veta=Veta(:idel(Veta))//', minimal density : '//
     1    Cislo(:idel(Cislo))
        write(lst,FormA) Veta(:idel(Veta))
        go to 6000
        if(allocated(Table1)) deallocate(Table1,Table2)
5000    Veta='the file "'//Veta(:idel(Veta))//'" does not exist or '//
     1        'cannot be properly opened.'
        go to 5500
5100    Veta='There is some inconsistency between the new and '//
     1       'reference map.'
5500    call FeChybne(-1.,-1.,Veta,'The final map is caluclated only '//
     1                'as F(obs) Fourier map.',SeriousError)
6000    close(lni)
        close(lnr)
        close(lno)
        call MoveFile(fln(:ifln)//'.z81',fln(:ifln)//'.m81')
      endif
      if(lpeaks.ne.0.and..not.ContourCallFourier) then
        call FouPeaks(0,0)
        if(ErrFlag.ne.0) go to 9000
      endif
9000  call CloseIfOpened(80)
      call CloseListing
      if(lpeaks.ne.0.and.ErrFlag.eq.0.and..not.RefineCallFourier.and.
     1   .not.ContourCallFourier.and.Mapa.ne.10.and.
     2   .not.SolutionCallFourier.and..not.Patterson) then
        if(FeYesNo(-1.,-1.,'Do you want to start the procedure for '//
     1             'including of new atoms?',1)) then
          call EM40NewAtFromFourier(ich)
          if(ich.ne.0) then
            call CopyFile(PreviousM40,fln(:ifln)//'.m40')
            call DeleteFile(PreviousM40)
          else
            call iom40only(1,0,fln(:ifln)//'.m40')
          endif
          go to 9900
        endif
      endif
      if(.not.RefineCallFourier.and..not.ContourCallFourier.and.
     1   .not.SolutionCallFourier.and.ErrFlag.eq.0.and..not.BatchMode)
     2  then
        NInfo=0
        call FeShowListing(-1.,-1.,'FOURIER program',fln(:ifln)//'.fou',
     2                     10000)
      endif
9900  if(Patterson) then
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
      endif
      if(allocated(SinTable)) deallocate(SinTable,CosTable)
      if(allocated(Table1)) deallocate(Table1,Table2)
      return
      end
