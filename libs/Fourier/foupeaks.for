      subroutine FouPeaks(Klic,m81)
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension x456(3),xp(6),stred(3),xpp(6),xold(3),xnew(3),ix(3),
     1          xm(3),ix3(3)
      dimension kmod(:,:),RhoIntMod(:,:),RhoPointMod(:,:),nmod(:),
     1          xmd(:,:),xmod(:,:,:),am(:),ps(:,:),der(:),
     2          waven(:,:),waveo(:,:),x48(:,:),NRhoI(:),
     3          ux48(:,:,:),uy48(:,:,:),tbl(:,:,:),isw48(:)
      character*128 ven,vent
      character*80 t80,format1
      character*27 format2
      character*20 Distance
      character*8 znak
      integer RhoAve,RhoPoint
      logical PointAlreadyPresent,EqIgCase,ZapisByl,Spokojen,EqRV
      allocatable kmod,RhoIntMod,RhoPointMod,nmod,xmd,xmod,am,ps,
     1            der,waven,waveo,x48,NRhoi,ux48,uy48,tbl,isw48
      equivalence (ix3(1),i1),(ix3(2),i2),(ix3(3),i3)
      data DMez/0.8/
      TypeOfSearch=SearchCharge
      ich=0
      nzab=0
      ZapisByl=.false.
      if(m81.le.0) then
        lnr=NextLogicNumber()
        call OpenMaps(lnr,fln(:ifln)//'.m81',nxny,0)
        if(ErrFlag.ne.0) go to 9999
      else
        lnr=m81
      endif
      read(lnr,rec=1,err=9200) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,
     1                         iorien,mapa,nsubs,MameSatelity,
     2                         nonModulated(KPhase)
      do i=1,3
        Pridavat(i)=(xfmn(i).ge.0..and.xfmn(i)-xdf(i)-.001.le.0.).and.
     1              (xfmx(i).le.1..and.xfmx(i)+xdf(i)+.001.ge.1.).and.
     2              nx(i).gt.1
        if(Pridavat(i)) then
          xplus(i)=xdf(i)
        else
          xplus(i)=0.
        endif
      enddo
      do i=1,6
        if(i.le.NDim(KPhase)) then
          cx(i)=smbx6(iorien(i))
        else
          cx(i)=' '
        endif
      enddo
      if(mapa.ge.7.and.mapa.le.9) then
        npeaks(1)=10000
        npeaks(2)=0
      endif
      if(IntMethod.eq.1) then
        call FouPeakIntSimpleProlog(IntRad)
      else
        call FouPeakIntProlog(IntRad)
      endif
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_peaks.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1010  read(m40,FormA,end=1011) t80
      if(t80(1:10).ne.'----------'.or.
     1   (LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0.and.
     2    LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0))
     3  then
        do i=1,idel(t80)
          if(t80(i:i).ne.'*') then
            write(ln,FormA) t80(:idel(t80))
            go to 1010
          endif
        enddo
      endif
1011  call CloseIfOpened(m40)
      nsubs=mod(nsubs,10)
      ntu =nx(4)*nx(5)
      ntuv=ntu*nx(6)
      do i=1,3
        if(iorien(i).gt.3) then
          ErrFlag=1
          go to 9999
        endif
      enddo
      n=max(npeaks(1),npeaks(2))
      if(allocated(tbl)) deallocate(tbl,x48,RhoIntArr,RhoPointArr,NRho)
      allocate(tbl(nx(1),nx(2),nx(3)),x48(3,5*n),RhoIntArr(5*n),
     1         isw48(5*n),RhoPointArr(5*n),NRho(5*n))
      if(NDimI(KPhase).gt.0) then
        n=5*n
        if(allocated(kmod)) deallocate(kmod,RhoIntMod,RhoPointMod,nmod,
     1                                 xmd,xmod,NRhoI,ux48,uy48)
        allocate(kmod(n,ntuv),RhoIntMod(n,ntuv),RhoPointMod(n,ntuv),
     1           xmod(3,n,ntuv),nmod(n*ntuv),xmd(6,n*ntuv),
     2           NRhoi(n),ux48(3,kharm,n),uy48(3,kharm,n))
        call SetIntArrayTo(NRhoi,n,0)
        na48max=n
        n=n/5
      else
        na48max=n
      endif
      read(lnr,rec=nmap+2,err=9200) rmax,rmin
      rmax=max(rmax,-rmin)
      format2='(i3,''/'',i3,3f8.4,f10.2,''|'')'
      if(rmax.gt.999000.00) format2(18:22)='e10.3'
      pom=rmax*33.33/float(imax)
      redukce=1.
1025  if(pom.gt.1.) then
        redukce=redukce*.1
        pom=pom*.1
        go to 1025
      endif
      rmax=rmax*redukce
      GridVol=CellVol(nsubs,KPhase)*xdf(1)*xdf(2)*xdf(3)
      eq48=0.
      if(Klic.eq.0) then
        do i=1,3
          l=iorien(i)
          if(xfmx(i)-xfmn(i).gt.xdf(i))
     1      eq48=max(eq48,xdf(i)*CellPar(l,nsubs,KPhase))
        enddo
        eq48=max(.1,eq48)
      else
        do i=1,3
          if(xfmx(i)-xfmn(i).gt.xdf(i))
     1      eq48=max(eq48,xdf(i))
        enddo
      endif
      call OpenFile(46,fln(:ifln)//'.m46','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      nflmx=(nx(3)-2)*ntuv
      if(npeaks(2).ge.1) nflmx=2*nflmx
      if(nflmx.le.0) go to 9999
      call FeFlowChartOpen(-1.,YMinFlowChart,1,nflmx,
     1                     'Peak searching procedure',' ',' ')
      nfl=0
      RhoMax=0.
      do kzap=1,-1,-2
        call newpg(0)
        if(kzap.gt.0) then
          npik=npeaks(1)
          znak='positive'
          if(Patterson.or.NDim(KPhase).gt.4) kharm=0
        else
          npik=npeaks(2)
          znak='negative'
          kharm=0
        endif
        ScPom=float(-kzap)*.01
        if(npik.le.0) cycle
        call newln(2)
        write(lst,'(/''Searching for '',a8,'' peaks - maximum number '',
     1              ''of peaks to be found : '',i3)') znak,npik
        if(redukce.lt..9) then
          call newln(2)
          write(lst,'(/''Scaling factor '',f12.8,'' will be applied'')')
     1      redukce
        endif
        rewind 46
        read(lnr,rec=1,err=9200) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),
     1                           xdf,iorien,mapa,nsubs,MameSatelity,
     2                           nonModulated(KPhase)
        nsubs=mod(nsubs,10)
        rzap=kzap
        irec=1
        do it=1,ntuv
          call RecUnpack(it,ix,nx(4),3)
          do i=1,3
            x456(i)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
          enddo
          do k=1,nx(3)
            irec=irec+1
            read(lnr,rec=irec)((tbl(i,j,k),i=1,nx(1)),j=1,nx(2))
            if(kzap.lt.0)
     1        call RealVectorToOpposite(tbl(1,1,k),tbl(1,1,k),nxny)
          enddo
          RhoMez=0.
          ipik=0
          ix33o=-1
          do 1100ipack=1,nx(1)*nx(2)*nx(3)
            call RecUnpack(ipack,ix3,nx,3)
            do i=1,3
              xm(i)=xfmn(i)+float(ix3(i)-1)*xdf(i)
            enddo
            if(ix3(3).ne.ix33o) then
              call FeFlowChartEvent(nfl,ie)
              if(ie.ne.0) then
                ErrFlag=1
                go to 9100
              endif
              ix33o=ix3(3)
            endif
            t111=FouGetTbl(Tbl,i1,i2,i3)
            t011=FouGetTbl(Tbl,i1-1,i2,i3)
            t211=FouGetTbl(Tbl,i1+1,i2,i3)
            if(t011.gt.t111.or.t211.ge.t111) go to 1100
            t101=FouGetTbl(Tbl,i1,i2-1,i3)
            t121=FouGetTbl(Tbl,i1,i2+1,i3)
            if(t101.gt.t111.or.t121.ge.t111) go to 1100
            t001=FouGetTbl(Tbl,i1-1,i2-1,i3)
            t021=FouGetTbl(Tbl,i1-1,i2+1,i3)
            if(t001.gt.t111.or.t021.ge.t111) go to 1100
            t201=FouGetTbl(Tbl,i1+1,i2-1,i3)
            t221=FouGetTbl(Tbl,i1+1,i2+1,i3)
            if(t201.gt.t111.or.t221.ge.t111) go to 1100
            if(nx(3).le.2) then
              t000=t001
              t002=t001
              t010=t011
              t012=t011
              t020=t021
              t022=t021
              t100=t101
              t102=t101
              t110=t111
              t112=t111
              t120=t121
              t122=t121
              t200=t201
              t202=t201
              t210=t211
              t212=t211
              t220=t221
              t222=t221
            else
              t000=FouGetTbl(Tbl,i1-1,i2-1,i3-1)
              t002=FouGetTbl(Tbl,i1-1,i2-1,i3+1)
              if(t000.gt.t111.or.t002.ge.t111) go to 1100
              t010=FouGetTbl(Tbl,i1-1,i2,i3-1)
              t012=FouGetTbl(Tbl,i1-1,i2,i3+1)
              if(t010.gt.t111.or.t012.ge.t111) go to 1100
              t020=FouGetTbl(Tbl,i1-1,i2+1,i3-1)
              t022=FouGetTbl(Tbl,i1-1,i2+1,i3+1)
              if(t020.gt.t111.or.t022.ge.t111) go to 1100
              t100=FouGetTbl(Tbl,i1,i2-1,i3-1)
              t102=FouGetTbl(Tbl,i1,i2-1,i3+1)
              if(t100.gt.t111.or.t102.ge.t111) go to 1100
              t110=FouGetTbl(Tbl,i1,i2,i3-1)
              t112=FouGetTbl(Tbl,i1,i2,i3+1)
              if(t110.gt.t111.or.t112.ge.t111) go to 1100
              t120=FouGetTbl(Tbl,i1,i2+1,i3-1)
              t122=FouGetTbl(Tbl,i1,i2+1,i3+1)
              if(t120.gt.t111.or.t122.ge.t111) go to 1100
              t200=FouGetTbl(Tbl,i1+1,i2-1,i3-1)
              t202=FouGetTbl(Tbl,i1+1,i2-1,i3+1)
              if(t200.gt.t111.or.t202.ge.t111) go to 1100
              t210=FouGetTbl(Tbl,i1+1,i2,i3-1)
              t212=FouGetTbl(Tbl,i1+1,i2,i3+1)
              if(t210.gt.t111.or.t212.ge.t111) go to 1100
              t220=FouGetTbl(Tbl,i1+1,i2+1,i3-1)
              t222=FouGetTbl(Tbl,i1+1,i2+1,i3+1)
              if(t220.gt.t111.or.t222.ge.t111) go to 1100
            endif
            call vrchol(xm(1),xdf(1),t011,t111,
     1                  t211,xp(iorien(1)),r)
            call vrchol(xm(2),xdf(2),t101,t111,
     1                  t121,xp(iorien(2)),s)
            call vrchol(xm(3),xdf(3),t110,t111,t112,xp(iorien(3)),t)
            do k=1,3
              l=iorien(k)
              if(xp(l).lt.xfmn(k)-xplus(k).or.xp(l).gt.xfmx(k)+xplus(k))
     1          go to 1100
            enddo
            if(klic.eq.0) then
              if(KoincSimple(xp,x48,1,ipik,eq48,pom,nsubs).gt.0)
     1          go to 1100
            endif
            RhoPointR=(r+s+t)*redukce/3.
            if(nx(3).le.2.or.Patterson.or.Mapa.ge.7) then
              RhoMax=max(RhoMax,abs(RhoPointR))
              RhoAve=nint(-100.*RhoPointR)
              RhoPoint=RhoAve
            else
              if(IntMethod.eq.1) then
                call FouPeakIntSimpleMake(Tbl,xp,RhoPointR,RhoAveR)
              else
                call FouPeakIntMake(Tbl,xp,RhoAveR)
              endif
              RhoAveR=RhoAveR*GridVol
              RhoAve=nint(-100.*RhoAveR)
              RhoPoint=nint(-100.*RhoPointR)
              if(TypeOfSearch.eq.SearchCharge) then
                RhoMax=max(RhoMax,abs(RhoAveR))
              else
                RhoMax=max(RhoMax,abs(RhoPointR))
              endif
            endif
            if((TypeOfSearch.eq.SearchCharge.and.RhoAve.lt.RhoMez).or.
     1         (TypeOfSearch.eq.SearchDensity.and.RhoPoint.lt.RhoMez))
     2        call FouInclude(xp,RhoAve,RhoPoint,x48,ipik,npik)
1100      continue
          if(ipik.lt.npik.and.ipik.gt.0) then
            if(TypeOfSearch.eq.SearchCharge) then
              call indexx(ipik,RhoIntArr,NRho)
            else
              call indexx(ipik,RhoPointArr,NRho)
           endif
          endif
          write(46,101) ipik,x456
          na48=ipik
          do n=1,ipik
            j=NRho(n)
            write(46,102)(x48(i,j),i=1,3),
     1                    -float(RhoIntArr(j))/100.*rzap,
     2                    -float(RhoPointArr(j))/100.*rzap
          enddo
        enddo
        if(ntuv.eq.1) then
          kharm=0
          go to 5000
        endif
        rewind 46
        mxnmod=0
        do k=1,ntuv
          read(46,101) nmod(k)
          mxnmod=max(mxnmod,nmod(k))
          do j=1,nmod(k)
            read(46,102)(xmod(i,j,k),i=1,3),RhoIntMod(j,k),
     1                                      RhoPointMod(j,k)
          enddo
          call SetIntArrayTo(kmod(1,k),nmod(k),0)
        enddo
        if(NDim(KPhase).gt.4.or.kzap.eq.-1.or.Patterson) go to 4900
        nlsq=2*kharm+1
        nam=nlsq*(kharm+1)
        allocate(am(nam),ps(nlsq,3),der(nlsq),waven(nlsq,3),
     1           waveo(nlsq,3))
        der(1)=1.
        nm=0
        na48=0
        jp=1
        kp=1
2500    do k=kp,ntuv
          do j=jp,nmod(k)
            if(kmod(j,k).eq.0) go to 2530
          enddo
          jp=1
        enddo
        go to 4800
2530    jp=j
        kp=k
        nm=nm+1
        Spokojen=.false.
        call CopyVek(xmod(1,jp,kp),xnew,3)
        call od0do1(xnew,xnew,3)
        do im=1,5
          n=1
          kmod(jp,kp)=-1
          RhoInt=RhoIntMod(jp,kp)
          RhoPointR=RhoPointMod(jp,kp)
          RhoMin=RhoInt*.5
          call SetRealArrayTo(xmd(1,1),6,0.)
          call SetRealArrayTo(waveo,3*nlsq,0.)
          call CopyVek(xmod(1,jp,kp),xmd(1,1),3)
          call RecUnpack(kp,ix,nx(4),3)
          do i=1,3
            xmd(i+3,1)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
          enddo
          call od0do1(xmd(1,1),xmd(1,1),NDim(KPhase))
          call CopyVek(xmd(1,1),Stred,3)
          do k=kp,ntuv
            do j=1,nmod(k)
              if(kmod(j,k).ne.0.or.RhoIntMod(j,k).lt.RhoMin) cycle
              call CopyVek(xmod(1,j,k),xp,3)
              call RecUnpack(k,ix,nx(4),3)
              do i=1,3
                xp(i+3)=xfmn(i+3)+(ix(i)-1)*xdf(i+3)
              enddo
              if(PointAlreadyPresent(xp,xpp,xnew,1,dmax,.true.,dpom,
     1                               isym,icent,nsubs)) then
                n=n+1
                call CopyVek(xpp,xmd(1,n),NDim(KPhase))
                call AddVek(stred,xpp,stred,3)
                if(Spokojen) then
                  RhoInt=RhoInt+RhoIntMod(j,k)
                  RhoPointR=RhoPointR+RhoPointMod(j,k)
                  kmod(j,k)=-1
                endif
              endif
            enddo
          enddo
          u=1./float(n)
          RhoInt=RhoInt*u
          RhoPointR=RhoPointR*u
          do i=1,3
            Stred(i)=Stred(i)*u
          enddo
          if(Spokojen) exit
          call CopyVek(Stred,xnew,3)
          if(im.ne.1) Spokojen=EqRV(xnew,xold,3,.0001).or.im.eq.4
          call CopyVek(xnew,xold,3)
        enddo
        kmodmx=-2
        iter=0
3999    iter=iter+1
        call SetRealArrayTo(ps,3*nlsq,0.)
        call SetRealArrayTo(am,nam,0.)
        do k=1,n
          arg=pi2*(xmd(4,k)-qu(1,1,1,KPhase)*(xmd(1,k)-stred(1))
     1                     -qu(2,1,1,KPhase)*(xmd(2,k)-stred(2))
     2                     -qu(3,1,1,KPhase)*(xmd(3,k)-stred(3)))
          j=0
          do i=1,kharm
            pom=float(i)*arg
            j=j+2
            der(j)=sin(pom)
            der(j+1)=cos(pom)
          enddo
          call FouPeaksSuma(der,xmd(1,k),am,ps,nlsq)
        enddo
        call smi(am,nlsq,ising)
        if(ising.eq.1.or.n*2.lt.ntuv) then
          do i=1,3
            waven(1,i)=stred(i)
          enddo
          iter=0
          go to 4600
        endif
        do i=1,3
          call nasob(am,ps(1,i),waven(1,i),nlsq)
        enddo
        do i=1,3
          stred(i)=waven(1,i)
        enddo
        pom=0.
        do i=1,3
          do j=1,nlsq
            pom=pom+abs(waven(j,i)-waveo(j,i))
            waveo(j,i)=waven(j,i)
          enddo
        enddo
        if(pom.gt..001.and.iter.le.5) go to 3999
4600    if(iter.le.5.and.iter.gt.0) then
          do i=1,3
            xp(i)=waven(1,i)
          enddo
          j=koinc(xp,x48,1,na48,eq48,pom,nsubs,isw48)
          if(j.le.0.and.na48.lt.na48max) then
            if(na48.lt.na48max) then
              na48=na48+1
              do i=1,3
                k=2
                do j=1,kharm
                  ux48(i,j,na48)=waven(k  ,i)
                  uy48(i,j,na48)=waven(k+1,i)
                  k=k+2
                enddo
                x48(i,na48)=waven(1,i)
              enddo
              call od0do1(x48(1,na48),x48(1,na48),3)
              isw48(na48)=nsubs
              RhoIntArr(na48)=nint(-RhoInt*100.)
              RhoPointArr(na48)=nint(-RhoPointR*100.)
              kmodmx=na48
            endif
          endif
        endif
        do k=1,ntuv
          do j=1,nmod(k)
            if(kmod(j,k).eq.-1) kmod(j,k)=kmodmx
          enddo
        enddo
        go to 2500
4800    if(na48.gt.1) then
          if(TypeOfSearch.eq.SearchCharge) then
            call indexx(na48,RhoIntArr,NRho)
          else
            call indexx(na48,RhoPointArr,NRho)
          endif
        else
          NRho(1)=1
        endif
4900    do i=1,na48
          NRhoi(NRho(i))=i
        enddo
        iz=1
        m=(nx(4)-1)/3+1
        kk=mod(nx(4)-1,3)+1
        if(kzap.eq.-1.or.Patterson) then
          RhoMez=0
          ipik=0
        endif
        x456(3)=xfmn(6)
        do iv=1,nx(6)
          x456(2)=xfmn(5)
          do iu=1,nx(5)
            x456(1)=xfmn(4)
            do it=1,m
              if(it.ne.m) then
                n=2
              else
                n=kk-1
              endif
              if(line.gt.mxline-10) call newpg(0)
              call newln(3)
              write(lst,FormA128)
              if(NDim(KPhase).eq.4) then
                write(ven,104) cx(4),x456(1)
              else if(NDim(KPhase).eq.5) then
                write(ven,105) cx(4),x456(1),cx(5),x456(2)
              else
               write(ven,106) cx(4),x456(1),cx(5),x456(2),cx(6),x456(3)
              endif
              if(n.gt.0) then
                if(NDim(KPhase).eq.4) then
                  write(ven(43:),104) cx(4),x456(1)+xdf(4)
                else if(NDim(KPhase).eq.5) then
                  write(ven(43:),105) cx(4),x456(1)+xdf(4),cx(5),
     1                  x456(2)+xdf(5)
                else
                  write(ven(43:),106) cx(4),x456(1)+xdf(4),cx(5),
     1                  x456(2)+xdf(5),cx(6),x456(3)+xdf(6)
                endif
                if(n.gt.1) then
                  if(NDim(KPhase).eq.4) then
                    write(ven(85:),104) cx(4),x456(1)+2.*xdf(4)
                  else if(NDim(KPhase).eq.5) then
                    write(ven(85:),105) cx(4),x456(1)+2.*xdf(4),cx(5)
     1                   ,x456(2)+2.*xdf(5)
                  else
                    write(ven(85:),106) cx(4),x456(1)+2.*xdf(4),cx(5)
     1                   ,x456(2)+2.*xdf(5),cx(6),x456(3)+2.*xdf(6)
                  endif
                endif
              endif
              write(lst,FormA128) ven
              write(ven,108)
              if(n.gt.0) write(ven(43:),108)
              if(n.gt.1) write(ven(85:),108)
              write(lst,FormA128) ven
              do j=1,mxnmod
                call newln(1)
                ven=' '
                ii=1
                do k=iz,iz+n
                  if(j.gt.nmod(k)) go to 4940
                  if(NDim(KPhase).eq.4.and.kzap.eq.1.and..not.Patterson)
     1              then
                    if(kmod(j,k).le.0) then
                      i1=0
                    else
                      i1=NRhoi(kmod(j,k))
                    endif
                  else
                    i1=0
                    if(kzap.eq.-1.or.Patterson) then
                      ip=kzap*nint(-RhoIntMod(j,k)*100.)
                      ipp=kzap*nint(-RhoPointMod(j,k)*100.)
                      if(ip.lt.RhoMez)
     1                  call FouInclude(xmod(1,j,k),ip,ipp,x48,ipik,
     2                                  npik)
                    endif
                  endif
                  write(ven(ii:),format2) j,i1,(xmod(i,j,k),i=1,3),
     1                                    RhoIntMod(j,k)
4940              ii=ii+42
                enddo
                write(lst,FormA128) ven
              enddo
              iz=iz+n+1
              x456(1)=x456(1)+3.*xdf(4)
            enddo
            x456(2)=x456(2)+xdf(5)
          enddo
          x456(3)=x456(3)+xdf(6)
        enddo
5000    call newln(3)
        write(lst,'(/''The list of '',a8,'' peaks written to the m40 '',
     1              '' file''/)') znak
        if(NDim(KPhase).gt.3) then
          if(kzap.eq.1.and..not.Patterson) then
            if(kharm.ne.0) then
              call newln(1)
              write(lst,'(''These peaks were successfully interpreted''
     1                   ,'' with '',i1,'' harmonic wave(s)'')') kharm
            endif
          else
            if(ipik.lt.npik) then
              if(TypeOfSearch.eq.SearchCharge) then
                call indexx(ipik,RhoIntArr,NRho)
              else
                call indexx(ipik,RhoPointArr,NRho)
              endif
            endif
          endif
        endif
        if(Patterson) then
          if(.not.allocated(Atom).or.MxAtAll.le.1)
     1      call ReallocateAtoms(100)
          vent='        x      y      z          rho   rel'//
     1         '   distance to origin'
          format1='(i3,''.'',3f7.4,f12.1,i5)'
          NAtIndLen(1,KPhase)=2
          NAtCalc=2
          NAtAll=2
          call SetBasicKeysForAtom(1)
          call SetBasicKeysForAtom(2)
          Atom(1)='Origin'
          Atom(2)='Itself'
          call specat
        else
          vent='        x      y      z     charge      rho   rel'
          format1='(i3,''.'',3f7.4,f8.2,f10.2,i6)'
        endif
        vent=vent(1:65)//vent(1:65)
        n2=0
        if(line.gt.mxline-12) call newpg(0)
5100    n1=n2+1
        n2=min(n2+2*(mxline-line-2),na48)
        n=n2-n1+1
        nn=(n-1)/2+1
        call newln(nn+2)
        write(lst,FormA128) vent
        write(lst,FormA128)
        if(Patterson) call SetRealArrayTo(x,3,0.)
        do ip=0,nn-1
          ven=' '
          l=1
          ll=n1+ip
          do j=1,2
            if(ll.gt.n2) cycle
            ii=NRho(ll)
            if(TypeOfSearch.eq.SearchCharge) then
              RhoAveR=float(-RhoIntArr(ii)*kzap)*.01
            else
              RhoAveR=float(-RhoPointArr(ii)*kzap)*.01
            endif
            if(klic.eq.0) then
              if(Patterson) then
                call CopyVek(x48(1,ii),x(1,2),3)
                call DistForOneAtom(2,20.,1,0)
                do i=1,ndist
                  m=ipord(i)
                  if(.not.EqIgCase(adist(m),'Itself')) then
                    write(Distance,'(f12.3)') ddist(m)
                    go to 5220
                  endif
                enddo
                Distance='        >20'
              else
                i=KoincM40(x48(1,ii),eq48,pom,nsubs)
              endif
            else
              i=0
            endif
5220        if(Patterson) then
              write(ven(l:),format1) ll,(x48(m,ii),m=1,3),RhoAveR,
     1                               nint(RhoAveR*999./RhoMax)
              ven=ven(:idel(ven))//'   '//Distance(:idel(Distance))
            else

              write(ven(l:),format1) ll,(x48(m,ii),m=1,3),
     1                               ScPom*float(RhoIntArr(ii)),
     2                               ScPom*float(RhoPointArr(ii)),
     3                               nint(RhoAveR*999./RhoMax)
              if(i.gt.0) ven=ven(:idel(ven))//' ='//atom(i)
            endif
            l=l+65
            ll=ll+nn
          enddo
          write(lst,FormA128) ven
        enddo
        if(n2.lt.na48) then
          call newpg(0)
          go to 5100
        endif
        ZapisByl=.true.
        if(kzap.lt.0) then
          Cislo='minima'
        else
          Cislo='maxima'
        endif
        write(ln,109) Cislo(1:6)
        write(ln,'(3i5)') nsubs,KPhase,Radiation(KDatBlock)
        do i=1,na48
          if(kzap.eq.1) then
            znak='max'
          else
            znak='min'
          endif
          write(znak(4:6),'(i3)') i
          call zhusti(znak)
          ii=NRho(i)
          write(ln,'(a8,2i3,4x,4f9.6,6x,3i1,3i3)') znak,1,1,1.,
     1         (x48(j,ii),j=1,3),0,0,0,0,kharm,0
          h=-float(RhoIntArr(ii))*.01
          if(h.gt.999000.00) then
            t80='(e9.3)'
          else
            t80='(3f9.2)'
          endif
          write(ln,t80) -float(RhoPointArr(ii))*.01,
     1                  -float(RhoIntArr(ii))*.01
          do j=1,kharm
            write(ln,103)(ux48(k,j,ii),k=1,3),(uy48(k,j,ii),k=1,3)
          enddo
          if(kharm.gt.0) write(ln,103) 0.
        enddo
      enddo
9100  call FeFlowChartRemove
      if(ZapisByl) write(ln,'(72(''-''))')
      call CloseIfOpened(ln)
      call MoveFile(fln(:ifln)//'_peaks.tmp',fln(:ifln)//'.m40')
      go to 9999
9200  call FeReadError(lnr)
      ErrFlag=1
9999  call DeleteFile(fln(:ifln)//'.m46')
      call CloseIfOpened(m40)
      if(allocated(tbl)) deallocate(tbl,x48,isw48,RhoIntArr,RhoPointArr,
     1                              NRho)
      if(allocated(kmod)) deallocate(kmod,RhoIntMod,RhoPointMod,nmod,
     1                               xmd,xmod,NRhoI,ux48,uy48)
      if(allocated(am)) deallocate(am,ps,der,waven,waveo)
      call CloseIfOpened(ln)
      if(m81.eq.0) call CloseIfOpened(lnr)
      if(IntMethod.ne.1) call FouPeakIntEpilog
      return
101   format(i5,3f9.6)
102   format(3f9.6,2e15.6)
103   format(6f9.6)
104   format(16x,a2,'=',f6.3,17x)
105   format(11x,2(a2,'=',f6.3,1x),11x)
106   format( 6x,3(a2,'=',f6.3,1x), 6x)
108   format('peak/atom    x       y       z       rho |')
109   format(26('-'),3x,'Fourier ',a6,3x,26('-'))
      end
