      subroutine FouOpenCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) return
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultFourier
      OrCode=-333
      call SetRealArrayTo(xrmn,6,-333.)
      call NactiFourier
      if(ptname.eq.'[neco]') then
        write(ptname,101) ptx
        call ZdrcniCisla(ptname,3)
      endif
      call FouMakeScopeType
9999  return
101   format(3f10.6)
      end
