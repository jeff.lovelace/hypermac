      subroutine FouPrelim
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*256 Veta
      integer CrlCentroSymm
      logical ExistM80,eqiv,EqIgCase,konec,ExistFile,AddBorder,EqIV0
      dimension ih(6),ihp(6),ihpp(6),mxh(6),mxd(6),hh(6),hhp(6),
     1          fr(:),fi(:),mlt(:)
      allocatable fr,fi,mlt
      call DeleteFile(fln(:ifln)//'_Fourier.tmp')
      call OpenFile(lst,fln(:ifln)//'.fou','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      uloha='Program for n-dimensional Fourier synthesis'
      OrCode=-333
      call SetRealArrayTo(xrmn,NDim(KPhase),-333.)
      ptstep=-1.0
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultFourier
      call NactiFourier
      if(ErrFlag.ne.0) go to 9999
      if(RefineCallFourier) then
        YMinFlowChart=120.
      else
        YMinFlowChart=-1.
      endif
      if(NDim(KPhase).le.3) then
        nsubs=1
      else
        nsubs=min(nsubs,NComp(KPhase))
      endif
      Patterson=mapa.le.3.or.Mapa.eq.9
      call iom50(0,1,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      ExistM80=ExistFile(fln(:ifln)//'.m80')
      if(.not.ExistM80.and.mapa.ne.1) then
        call FeChybne(-1.,-1.,'the M80 file doesn''t exist, first you'//
     1                ' have to','run REFINE to get phases and/or '//
     2                'Fcalc.',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      if(ptstep.lt.0.) ptstep=.25
      call FouDefLimits
      if(ExistM80) then
        call OpenFile(80,fln(:ifln)//'.m80','formatted','old')
        if(ErrFlag.ne.0) go to 9999
      else
        call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9999
      endif
      FileM82='jm82'
      call CreateTmpFile(FileM82,i,0)
      call FeTmpFilesAdd(FileM82)
      call OpenFile(82,FileM82,'unformatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      twov=2./CellVol(nsubs,1)
      if(Patterson) then
        srnat=0.
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          do i=1,NAtFormula(KPhase)
            srnat=srnat+AtMult(i,KPhase)
          enddo
        endif
        if(srnat.gt.0.) then
          do i=1,NAtFormula(KPhase)
            fx(i)=ffbasic(1,i,KPhase)+ffra(i,KPhase,KDatBlock)
          enddo
          pom0=0.
          do i=1,NAtFormula(KPhase)
            pom0=pom0+
     1           AtMult(i,KPhase)*sqrt((fx(i)**2+
     2                                  ffia(i,KPhase,KDatBlock)**2))
          enddo
        else
          pom0=1.
        endif
      endif
      if(lite(KPhase).eq.1) then
        rhom=1.
      else
        rhom=episq
      endif
      AddBorder=.false.
      if(ptname.ne.'[nic]') then
        if(ptname.ne.'[neco]') then
          kk=0
          call SetRealArrayTo(ptx,3,0.)
          pom=0.
1150      call Kus(ptname,kk,Veta)
          call atsym(Veta,i,hhp,hh,hh(4),j,k)
          if(i.le.0) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t on the '//'file M40',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(k.eq.3) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t correct',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(ErrFlag.ne.0) go to 9000
          pom=pom+1.
          call AddVek(ptx,hhp,ptx,3)
          if(kk.lt.idel(ptname)) go to 1150
          do i=1,3
            ptx(i)=ptx(i)/pom
          enddo
        endif
        do j=1,3
          nd=nint(.5*pts(j)/ptstep)
          if(nd.ne.0) then
            dd(j)=ptstep/CellPar(j,nsubs,KPhase)
          else
            dd(j)=1.
          endif
          xrmn(j)=ptx(j)-dd(j)*float(nd)
          xrmx(j)=ptx(j)+dd(j)*float(nd)
        enddo
      endif
      if(OrCode.eq.-333) OrCode=nop(nsubs)
      norien=OrCode
      n=norien
      i=10**(NDim(KPhase)-1)
      do j=1,NDim(KPhase)-1
        iorien(j)=n/i
        n=mod(n,i)
        i=i/10
      enddo
      iorien(NDim(KPhase))=n
      do j=1,6
        if(j.le.NDim(KPhase)) then
          if(xrmn(j).lt.-330.) then
            xrmn(j)=fourmn(j,nsubs)
            xrmx(j)=fourmx(j,nsubs)
            if(j.le.3) then
              n=max(nint((xrmx(j)-xrmn(j))*
     1              CellPar(j,nsubs,KPhase)/ptstep),1)
              dd(j)=(xrmx(j)-xrmn(j))/float(n)
            else
              dd(j)=.1
            endif
            if(IndUnit.eq.1) then
              AddBorder=.true.
            else
              xrmx(j)=xrmx(j)-dd(j)
            endif
          endif
        else
          xrmn(j)=0.
          xrmx(j)=0.
          dd(j)=1.
        endif
      enddo
      do i=1,NDim(KPhase)
        n=iorien(i)
        if(n.lt.1.or.n.gt.NDim(KPhase)) go to 1500
        do j=i+1,NDim(KPhase)
          if(n.eq.iorien(j)) go to 1500
        enddo
      enddo
      go to 1550
1500  Veta='wrong orientation'
      i=idel(Veta)+1
      write(Veta(i:i+6),'(i7)') norien
      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
      ErrFlag=1
      go to 9000
1550  do i=1,6
        if(i.le.NDim(KPhase)) then
          j=iorien(i)
          if(AddBorder.and.j.le.3) then
            xrmn(j)=xrmn(j)-dd(j)
            xrmx(j)=xrmx(j)+dd(j)
          endif
          xfmn(i)=xrmn(j)
          xfmx(i)=xrmx(j)
          xdf(i)=dd(j)
        else
          xfmn(i)=0.
          xfmx(i)=0.
          xdf(i)=1.
        endif
      enddo
      NDimSum=0
      do i=1,6
        pom=xfmx(i)-xfmn(i)
        nx(i)=nint(pom/xdf(i))+1
        if(i.le.NDim(KPhase)) then
          if(nx(i).gt.1) then
            dpom=pom/float(nx(i)-1)
            if(abs(xdf(i)-dpom).gt..00001) xdf(i)=dpom
          endif
          if(pom.lt.0.or.xdf(i).lt.0.) then
            write(Veta,'(3f8.3)') xfmn(i),xfmx(i),xdf(i)
            call ZdrcniCisla(Veta,3)
            j=idel(Veta)
            if(NDim(KPhase).eq.3) then
              Veta='Incorrect limits for '//smbx(i)//' : '//Veta(:j)
            else
              Veta='Incorrect limits for '//smbx6(i)//' : '//Veta(:j)
            endif
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            ErrFlag=1
            go to 9000
          endif
        endif
        if(nx(i).gt.1) NDimSum=NDimSum+1
      enddo
      NDimSum=min(NDimSum,NDim(KPhase))
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      if(nxny.lt.50) then
        write(Veta,'(i5,''x'',i5)') nx(1),nx(2)
        call Zhusti(Veta)
        Veta='2d sections would contain only '//Veta(:idel(Veta))//
     1      ' points.'
        call FeChybne(-1.,-1.,Veta,'Please enlarge the scope of maps.',
     1                SeriousError)
        ErrFlag=1
        go to 9000
      endif
1630  if(Patterson) then
        do i=1,NSymmN(KPhase)
          call SetRealArrayTo(s6(1,i,1,KPhase),NDim(KPhase),0.)
        enddo
        if(CrlCentroSymm().le.0) then
          call ReallocSymm(NDim(KPhase),2*NSymmN(KPhase),
     1                     NLattVec(KPhase),NComp(KPhase),NPhase,0)
          n=NSymmN(KPhase)
          do i=1,NSymmN(KPhase)
            call RealMatrixToOpposite(rm6(1,i,1,KPhase),
     1        rm6(1,i+n,1,KPhase),NDim(KPhase))
            call MatBlock3(rm6(1,i+n,1,KPhase),rm(1,i+n,1,KPhase),
     1                     NDim(KPhase))
            call CopyVek(s6(1,i,1,KPhase),s6(1,i+n,1,KPhase),
     1                   NDim(KPhase))
            ISwSymm(i+n,1,KPhase)=ISwSymm(i,1,KPhase)
          enddo
          NSymmN(KPhase)=2*n
          MaxNSymm=max(MaxNSymm,NSymm(KPhase))
          call ReallocateAtoms(0)
        endif
      endif
      do i=1,6
        if(i.le.NDim(KPhase)) then
          cx(i)=smbx6(iorien(i))
        else
          cx(i)=' '
        endif
      enddo
      if(Patterson) then
        npeaks(1)=50
      else
        if(npeaks(1).lt.0) then
          fa=0.
          fah=0.
          do i=1,NAtCalc
            if(ffbasic(1,isf(i),KPhase).lt.2.) then
              fah=fah+ai(i)
            else
              fa=fa+ai(i)
            endif
          enddo
          pom=0.
          pomh=0.
          do i=1,NAtFormula(KPhase)
            if(EqIgCase(AtType(i,KPhase),'H')) then
              pomh=pomh+AtMult(i,KPhase)
            else
              pom=pom+AtMult(i,KPhase)
            endif
          enddo
          pom=pom*float(NUnits(KPhase))/
     1        float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          pomh=pomh*float(NUnits(KPhase))/
     1      float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          nachp=nint(pomh)
          if(mapa.eq.4.or.mapa.eq.5) then
            npeaks(1)=nacp+10
          else
            npeaks(1)=max(nacp-nint(fa)+nachp-nint(fah)+10,10)
          endif
        endif
        if(npeaks(2).lt.0) then
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            npeaks(2)=10
          else
            NPeaks(2)=NPeaks(1)
          endif
        endif
        do i=1,2
          npeaks(i)=min(npeaks(i),3000)
        enddo
      endif
      if(mapa.eq.1) then
        Veta='F(obs)**2 - Patterson'
      else if(mapa.eq.2) then
        Veta='F(calc)**2 - checking Patterson'
      else if(mapa.eq.3) then
        Veta='F(obs)**2-F(calc)**2 - difference Patterson'
      else if(mapa.eq.4) then
        Veta='F(obs) - Fourier'
      else if(mapa.eq.5) then
        Veta='F(calc) - checking Fourier'
      else if(mapa.eq.6) then
        Veta='F(obs)-F(cal) - difference Fourier'
      else if(mapa.eq.7) then
        Veta='dynamic multipole deformation map'
      else if(mapa.eq.8) then
        Veta='static multipole deformation map'
      else if(mapa.eq.9) then
        Veta='1/0 - shape function'
      else if(mapa.eq.10) then
        call GetPureFileName(FouRefStr,Veta)
        Veta='Difference between Fourier maps of the actual and '//
     1       'reference structure "'//Veta(:idel(Veta))//'"'
      else
        call FeChybne(-1.,-1.,'wrong map type',' ',SeriousError)
        ErrFlag=1
        go to 9000
      endif
      Veta='Type of map : '//Veta(:idel(Veta))
      call newln(2)
      write(lst,FormA1)
      write(lst,FormA1)(Veta(i:i),i=1,idel(Veta))
      if(vyber.gt.0.) then
        call newln(2)
        write(lst,'(/''Reflections with  |Fo| > '',f10.2,'' * |Fc| '',
     1               ''will not be used in the synthesis'')') vyber
      endif
      call newln(2)
      if(UseWeight.gt.0) then
        write(lst,'(/''Weighting of reflection based on chi-square '',
     1               ''will be applied'')')
      else
        write(lst,'(/''No weighting of reflections will be applied'')')
      endif
      if(snlmxf.le.0.0) snlmxf=10.0
      call newln(2)
      write(lst,'(/''Limits of sin(th)/lambda for acceptance are : '',
     1            2f10.6)') snlmnf,snlmxf
      if(NComp(KPhase).gt.1) then
        call newln(2)
        write(lst,'(/''Fourier for subsystem #'',i1,'' will be '',
     1               ''calculated'')') nsubs
      endif
      rewind 82
      call FeMouseShape(3)
      call SetIntArrayTo(mxh,6,0)
      call FouNastavM80
      if(ErrFlag.ne.0) go to 9999
3000  if(ExistM80) then
        read(80,FormA80,end=3100) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 3100
        endif
        read(Veta,Format80,end=3100,err=8000)(ih(i),i=1,maxNDim),KPh,
     1                                       fo1,fc
      else
        read(91,format91,end=3100,err=8100)
     1    (ih(i),i=1,NDim(KPhase)),fo1,fo2
        KPh=KPhase
      endif
      if(ih(1).gt.900) go to 3100
      if(KPh.ne.KPhase) go to 3000
      if(nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        do j=1,NDim(KPhase)
          mxh(j)=max(mxh(j),iabs(ihpp(iorien(j))))
        enddo
      enddo
      go to 3000
3100  iz=0
      n=1
      do i=1,NDim(KPhase)
        mxd(i)=2*mxh(i)+1
        n=n*mxd(i)
        if(mxd(i).gt.nx(i).and.i.le.NDimSum.and.
     1     FMethod.eq.FFTMethod) then
          if(i.gt.2) then
            NDimSum=i-1
          else
            nxo=nx(i)
            nx(i)=mxd(i)
            xfmxo=xfmx(i)
            xfmx(i)=xfmx(i)-xdf(i)
            xfmn(i)=xfmn(i)+xdf(i)
            xdf(i)=(xfmx(i)-xfmn(i))/float(nx(i)-2)
3150        xfmn(i)=xfmn(i)-xdf(i)
            xfmx(i)=xfmx(i)+xdf(i)
            if(xfmxo.gt.xfmx(i)) then
              nx(i)=nx(i)+2
              go to 3150
            endif
          endif
        endif
      enddo
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      allocate(fr(n),fi(n),mlt(n))
      call SetRealArrayTo(fr,n,0.)
      call SetRealArrayTo(fi,n,0.)
      call SetIntArrayTo(mlt,n,0)
      if(ExistM80) then
        rewind 80
        call FouNastavM80
        if(ErrFlag.ne.0) go to 9999
      else
        call NastavM90(91)
      endif
      mmax=0
      kolik=0
      konec=.false.
3500  if(ExistM80) then
        read(80,FormA256,end=5000) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 5000
        endif
        read(Veta,Format80,err=8000,end=5000)
     1    (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2     acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
        if(ih(1).gt.900) go to 5000
        if(KPh.ne.KPhase) go to 3500
        if(EqIV0(ih,maxNDim)) then
          bc=0.
          bcfree=0.
          bcst=0.
          bcfreest=0.
        endif
      else
        if(isPowder) then
          read(91,format91Pow,err=8100,end=5000) ih,fo1,fo2,i,i,KPh
          if(KPh.ne.KPhase) go to 3500
        else
          read(91,format91,err=8100,end=5000)(ih(i),i=1,NDim(KPhase)),
     1                                        fo1,sigfo1
          sigfo2=sigfo1
          sigfo1=0.
          sigfo2=0.
          KPh=KPhase
        endif
        fc=0.
        if(ih(1).lt.900) then
          if(fo1.gt.0.) then
            fo1=sqrt(fo1)
          else
            fo1=0.
          endif
          fo2=fo1
        else
          go to 5000
        endif
      endif
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      if(TMethod.eq.RatioMethod) then
        fo=fo1
        sigfo=sigfo1
      else
        fo=fo2
        sigfo=sigfo2
      endif
      if(vyber.gt.0.and.abs(fo).gt.vyber*abs(fc)) go to 3500
      if(sinthl.gt.snlmxf.or.sinthl.lt.snlmnf) go to 3500
      if(fc.ne.0.) then
        cosaq=ac/fc
        sinaq=bc/fc
      else
        cosaq=0.
        sinaq=0.
      endif
      if(NDim(KPhase).gt.3.and.nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      if(mapa.eq.1) then
        frp=fo**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.2) then
        frp=fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.3) then
        frp=fo**2-fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.4.or.mapa.eq.10) then
        frp=fo*cosaq
        fip=fo*sinaq
        SigCoef=sigfo
        Coef=fo
      else if(mapa.eq.5) then
        frp=fc*cosaq
        fip=fc*sinaq
        SigCoef=sigfo
        Coef=fc
      else if(mapa.eq.6) then
        pom=fo-fc
        frp=pom*cosaq
        fip=pom*sinaq
        SigCoef=sigfo
        Coef=pom
      else if(mapa.eq.7) then
        frp=ac-acfree
        fip=bc-bcfree
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.8) then
        frp=acst-acfreest
        fip=bcst-bcfreest
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.9) then
        frp=1.
        fip=0.
        SigCoef=0.
        Coef=frp
      endif
      if(UseWeight.eq.1) then
        if(SigCoef.ne.0..and.Coef.ne.0.) then
          pom=1.-SigCoef**2/(Coef**2+SigCoef**2)
        else if(SigCoef.eq.0.) then
          pom=1.
        else if(Coef.eq.0.) then
          pom=0.
        endif
        frp=pom*frp
        fip=pom*fip
      endif
      if(ih(1).eq.0.and.ih(2).eq.0.and.ih(3).eq.0) then
        frp=frp/2.
        fip=fip/2.
      endif
      if(Patterson) then
        rho=sinthl**2*rhom
        if(srnat.gt.0.) then
          call SetFormF(sinthl)
          pom=0.
          do i=1,NAtFormula(KPhase)
            pom=pom+AtMult(i,KPhase)*fx(i)
          enddo
        else
          pom=1.
        endif
        pom=pom*exp(-OverAllB(KDatBlock)*rho)
        frp=frp*(pom0/pom)**2
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        arg=0.
        do j=1,NDim(KPhase)
          arg=arg-float(ih(j))*s6(j,i,nsubs,KPhase)
        enddo
        arg=arg*pi2
        cs=cos(arg)
        sn=sin(arg)
        pom1=twov*(frp*cs-fip*sn)
        pom2=twov*(frp*sn+fip*cs)
        do j=1,NDim(KPhase)
          if(ihpp(iorien(j)).lt.0) then
            pom2=-pom2
            call IntVectorToOpposite(ihpp,ihpp,NDim(KPhase))
            go to 4118
          else if(ihpp(iorien(j)).gt.0) then
            go to 4118
          endif
        enddo
4118    indp=0
        do j=NDim(KPhase),1,-1
          indp=indp*mxd(j)+ihpp(iorien(j))+mxh(j)
        enddo
        fr(indp)=fr(indp)+pom1
        fi(indp)=fi(indp)+pom2
        mlt(indp)=mlt(indp)+1
      enddo
      go to 3500
5000  call SetIntArrayTo(ihp,6,0)
      call SetIntArrayTo(hmin,6, 999999)
      call SetIntArrayTo(hmax,6,-999999)
      if(FMethod.eq.FFTMethod) then
        Redukce=.5
      else
        Redukce=1.
      endif
      do i=1,UBound(fr,1)
        indp=i
        if(mlt(indp).le.0) cycle
        pom=Redukce/float(mlt(indp))
        fr(indp)=fr(indp)*pom
        fi(indp)=fi(indp)*pom
        do j=1,NDim(KPhase)
          ihp(j)=mod(indp,mxd(j))-mxh(j)
          indp=indp/mxd(j)
          if(j.gt.3) mmax=max(mmax,iabs(ihp(j)))
        enddo
        do j=1,NDim(KPhase)
          hmax(j)=max(hmax(j),ihp(j))
          hmin(j)=min(hmin(j),ihp(j))
        enddo
        write(82) ihp,fr(i),fi(i)
        if(FMethod.eq.FFTMethod) then
          call IntVectorToOpposite(ihp,ihp,NDim(KPhase))
          do j=1,NDim(KPhase)
            hmax(j)=max(hmax(j),ihp(j))
            hmin(j)=min(hmin(j),ihp(j))
          enddo
          write(82) ihp,fr(i),-fi(i)
        endif
      enddo
      if(FMethod.eq.BLMethod) then
        n=0
        m=0
        do i=1,NDim(KPhase)
          n=max(n,hmax(i)-hmin(i)+1)
          m=max(m,nx(i))
        enddo
        allocate(SinTable(n,m,NDim(KPhase)),
     1          CosTable(n,m,NDim(KPhase)))
        do n=1,NDim(KPhase)
          do i=hmin(n),hmax(n)
            ii=i-hmin(n)+1
            fh=pi2*float(i)
            do j=1,nx(n)
              arg=fh*(xfmn(n)+(j-1)*xdf(n))
              SinTable(ii,j,n)=sin(arg)
              CosTable(ii,j,n)=cos(arg)
            enddo
          enddo
        enddo
      endif
      rewind 82
      MameSatelity=mmax.gt.0
      if(.not.MameSatelity) then
        do i=1,3
          if(iorien(i).gt.3) go to 9000
        enddo
        call SetIntArrayTo(nx(4),NDimI(KPhase),1)
        call SetRealArrayTo(xfmn(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xfmx(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xdf(4),NDimI(KPhase),0.1)
        nmap=nx(3)
      endif
      if(FMethod.eq.FFTMethod) then
        do i=1,NDimSum
          nx(i)=nint(1./xdf(i))
          n=2
6010      if(n.lt.nx(i)) then
            n=n*2
            go to 6010
          endif
          nx(i)=n
          xdf(i)=1./float(n)
          xfmn(i)=0.
          xfmx(i)=1.-xdf(i)
        enddo
        nxny=nx(1)*nx(2)
        nmap=nx(3)*nx(4)*nx(5)*nx(6)
      endif
      call newln(NDim(KPhase)+2)
      write(lst,FormA1)
      write(lst,'(''Scope of the map :'')')
      do i=1,NDim(KPhase)
        if(nx(i).gt.1) then
          write(lst,'(a2,'' from'',f8.4,'' to'',f8.4,'' step'',f7.4)')
     1          cx(i),xfmn(i),xfmx(i),xdf(i)
        else
          xfmn(i)=(xfmn(i)+xfmx(i))*.5
          xfmx(i)=xfmn(i)
          write(lst,'(a2,'' fixed to'',f8.4)') cx(i),xfmn(i)
        endif
      enddo
      call newln(2)
      write(lst,FormA1)
      write(Veta,'(''Orientation : '',6i1)')(iorien(i),i=1,NDim(KPhase))
      write(lst,FormA) Veta(:idel(Veta))
      call newln(2)
      write(lst,FormA1)
      Veta='The calculation will performed'
      if(FMethod.eq.FFTMethod) then
        Veta(idel(Veta)+2:)='by FFT method'
      else
        Veta(idel(Veta)+2:)=' by modified Beevers-Lipson algorithm'
      endif
      write(lst,FormA) Veta(:idel(Veta))
      if(NTwin.gt.1) then
        call newln(2)
        write(lst,FormA1)
        Veta='F(obs) will be corrrected for twinning'
        if(FMethod.eq.FFTMethod) then
          Veta(idel(Veta)+2:)='by substracting of F(calc)'
        else
          Veta(idel(Veta)+2:)='by in ratio of F(calc)'
        endif
        write(lst,FormA) Veta(:idel(Veta))
      endif
      go to 9999
8000  call FeReadError(80)
      go to 8900
8100  call FeReadError(91)
8900  ErrFlag=1
9000  call DeleteFile(fln(:ifln)//'.m81')
      call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
9999  if(allocated(fr)) deallocate(fr,fi,mlt)
      return
      end
