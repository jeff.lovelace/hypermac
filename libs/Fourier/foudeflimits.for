      subroutine FouDefLimits
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ias(:,:),bs(:,:)
      logical eqiv,eqrv
      allocatable ias,bs
      if(IndUnit.ne.1.or.FMethod.eq.FFTMethod) then
        call SetRealArrayTo(FourMn,6*NComp(KPhase),0.)
        call SetRealArrayTo(FourMx,6*NComp(KPhase),1.)
        nop(1)=0
        do i=1,NDim(KPhase)
          nop(1)=nop(1)*10+i
        enddo
        do i=2,NComp(KPhase)
          nop(i)=nop(1)
        enddo
        go to 9999
      endif
      n=2*NSymmN(KPhase)*NLattVec(KPhase)
      allocate(ias(3,n),bs(3,n))
      do isw=1,NComp(KPhase)
        n=1
        do 1300i=1,NSymmN(KPhase)
          k=0
          do j=1,3
            j2=j+3*mod(j,3)
            j3=j+3*mod(j+1,3)
            if(rm(k+j,i,isw,KPhase).eq.0..or.
     1         abs(rm(j2,i,isw,KPhase)).gt..01.or.
     1         abs(rm(j3,i,isw,KPhase)).gt..01) go to 1300
            ias(j,n)=nint(rm(k+j,i,isw,KPhase))
            bs(j,n)=s6(j,i,isw,KPhase)
            k=k+3
          enddo
          do j=1,n-1
            if(eqiv(ias(1,n),ias(1,j),3).and.
     1         eqrv(bs(1,n),bs(1,j),3,.0001)) go to 1300
          enddo
          n=n+1
1300    continue
        n=n-1
        if(Patterson) then
          call SetRealArrayTo(bs,3*n,0.)
          k=2
        else
          k=1
        endif
        call nascen(ias,bs,isw,n,m)
        call FouSetLimits(ias,bs,m,isw,k)
      enddo
      deallocate(ias,bs)
9999  return
      end
