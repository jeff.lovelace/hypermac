      function FouExMap(InputMap,xx,ich)
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension xx(*),xp(3),xpt(3),xd(3),n(3),x1(3)
      logical EqRV0
      real InputMap(*)
      nxnynz=nxny*nx(3)
      FouExMap=0.
      ich=0
      do i=1,NSymmN(KPhase)
        call multm(rm(1,i,nsubs,KPhase),xx,x1,3,3,1)
        call AddVek(x1,s6(1,i,nsubs,KPhase),x1,3)
        do 1500l=1,NLattVec(KPhase)
          call AddVek(x1,vt6(1,l,nsubs,KPhase),xp,3)
          do j=1,3
            xpt(j)=xp(iorien(j))
1100        if(xpt(j).ge.xfmn(j)-xplus(j)) go to 1200
            xpt(j)=xpt(j)+1.
            go to 1100
1200        if(xpt(j).gt.xfmx(j)+xplus(j)) then
              xpt(j)=xpt(j)-1.
              go to 1200
            endif
            if(xpt(j).lt.xfmn(j)-xplus(j)) go to 1500
          enddo
          do j=1,3
            xd(j)=(xpt(j)-xfmn(j))/xdf(j)+1.
            n(j)=xd(j)
            xd(j)=xd(j)-float(n(j))
            if(Pridavat(j)) then
              if(n(j).eq.0) then
                n(j)=nx(j)
              else if(n(j).eq.nx(j)+1) then
                n(j)=1
              endif
            endif
          enddo
          go to 2000
1500    continue
      enddo
      ich=1
      go to 9999
2000  i000=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
      if(EqRV0(xd,3,.001)) then
        FouExMap=InputMap(i000)
        go to 9999
      endif
      if(i000.gt.nxnynz) then
        call FouGetClosestPoint(i000,ich)
        if(ich.ne.0) go to 9999
      endif
      i100=i000+1
      if(i100.gt.nxnynz) then
        call FouGetClosestPoint(i100,ichp)
        if(ichp.ne.0) i100=i000
      endif
      i010=i000+nx(1)
      if(i010.gt.nxnynz) then
        call FouGetClosestPoint(i010,ichp)
        if(ichp.ne.0) i010=i000
      endif
      i110=i000+nx(1)+1
      if(i110.gt.nxnynz) then
        call FouGetClosestPoint(i110,ichp)
        if(ichp.ne.0) i110=i000
      endif
      i001=i000+nxny
      if(i001.gt.nxnynz) then
        call FouGetClosestPoint(i001,ichp)
        if(ichp.ne.0) i001=i000
      endif
      i101=i100+nxny
      if(i101.gt.nxnynz) then
        call FouGetClosestPoint(i101,ichp)
        if(ichp.ne.0) i101=i100
      endif
      i011=i010+nxny
      if(i011.gt.nxnynz) then
        call FouGetClosestPoint(i011,ichp)
        if(ichp.ne.0) i011=i010
      endif
      i111=i110+nxny
      if(i111.gt.nxnynz) then
        call FouGetClosestPoint(i111,ichp)
        if(ichp.ne.0) i111=i110
      endif
      f000=InputMap(i000)
      f100=InputMap(i100)
      f010=InputMap(i010)
      f110=InputMap(i110)
      f001=InputMap(i001)
      f101=InputMap(i101)
      f011=InputMap(i011)
      f111=InputMap(i111)
      f00=(f100-f000)*xd(1)+f000
      f01=(f101-f001)*xd(1)+f001
      f10=(f110-f010)*xd(1)+f010
      f11=(f111-f011)*xd(1)+f011
      f0=(f10-f00)*xd(2)+f00
      f1=(f11-f01)*xd(2)+f01
      FouExMap=(f1-f0)*xd(3)+f0
9999  return
      end
