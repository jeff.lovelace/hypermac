      function FouGetTbl(Tbl,i1,i2,i3)
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension Tbl(*),xp(3)
      i1p=i1
      if(Pridavat(1)) then
        if(i1.eq.nx(1)+1) then
          i1p=1
        else if(i1.eq.0) then
          i1p=nx(1)
        endif
      endif
      i2p=i2
      if(Pridavat(2)) then
        if(i2.eq.nx(2)+1) then
          i2p=1
        else if(i2.eq.0) then
          i2p=nx(2)
        endif
      endif
      i3p=i3
      if(Pridavat(3)) then
        if(i3.eq.nx(3)+1) then
          i3p=1
        else if(i3.eq.0) then
          i3p=nx(3)
        endif
      endif
      if(i1p.ge.1.and.i1p.le.nx(1).and.
     1   i2p.ge.1.and.i2p.le.nx(2).and.
     2   i3p.ge.1.and.i3p.le.nx(3)) then
        i=i1p+(i2p-1)*nx(1)+(i3p-1)*nx(1)*nx(2)
        FouGetTbl=Tbl(i)
      else
        xp(iorien(1))=xfmn(1)+(i1p-1)*xdf(1)
        xp(iorien(2))=xfmn(2)+(i2p-1)*xdf(2)
        xp(iorien(3))=xfmn(3)+(i3p-1)*xdf(3)
        FouGetTbl=FouExMap(Tbl,xp,ich)
      endif
      return
      end
