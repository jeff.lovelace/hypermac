      subroutine nascs(ias,bs,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ias(3,*),bs(3,*),p(6)
      logical eqiv,eqrv
      data p/6*0./
      m=n+1
      do 2000i=1,n
        do j=1,3
          ias(j,m)=-ias(j,i)
          p(j)=1.-bs(j,i)
        enddo
        call od0do1(p,p,3)
        do j=1,m-1
          if(eqiv(ias(1,m),ias(1,j),3).and.
     1       eqrv(p,bs(1,j),3,.0001)) go to 2000
        enddo
        do j=1,3
          bs(j,m)=p(j)
        enddo
        m=m+1
2000  continue
      n=m-1
      return
      end
