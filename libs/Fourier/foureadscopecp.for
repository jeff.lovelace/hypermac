      subroutine FouReadScopeCP(Klic,CPIndUnit,CPStep,DensityType,
     1                          CutOffDist,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ip(6),xp(3),nLblX(3),nLblMeze(3)
      character*256 EdwStringQuest,t256
      character*80 Veta
      character*25 :: MenDen(7) =
     1                (/'total density            ',
     2                  'valence density          ',
     3                  'deformation density      ',
     4                  'total density Laplacian  ',
     5                  'valence density Laplacian',
     6                  'inversed gradient vector ',
     7                  'gradient vector          '/)
      character*7 Label(3)
      integer ScopeTypeOld,CPIndUnit,DensityType,RolMenuSelectedQuest
      logical CrwLogicQuest
      data Label/'minimum','maximum','step'/
      call FouMakeScopeType
      xqd=500.
      id=NextQuestId()
      il=8
      Veta='Define scope:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      xpom=xqd*.166667
      ichk=1
      do i=1,3
        if(i.eq.1) then
          Veta='%automatically'
        else if(i.eq.2) then
          Veta='%explicitly'
        else
          Veta='by a central %point'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-4.,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,ichk)
        if(i.eq.1) then
          nCrwAuto=CrwLastMade
        else if(i.eq.1) then
          nCrwExpl=CrwLastMade
        else if(i.eq.3) then
          nCrwPoint=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.ScopeType)
        xpom=xpom+xqd*.333333
      enddo
      xpomp=xqd*.5-80.
      il=il+2
      ilp=il
      dpom=50.
      do i=1,3
        il=il+1
        Veta=smbx(i)
        call FeQuestLblMake(id,xpomp-20.,il,Veta,'L','N')
        nLblX(i)=LblLastMade
        call FeQuestLblOff(LblLastMade)
        xpom=xpomp
        do j=1,3
          if(i.eq.1) then
            call FeQuestLblMake(id,xpom+dpom*.5,ilp,Label(j),'C','N')
            call FeQuestLblOff(LblLastMade)
            nLblMeze(j)=LblLastMade
          endif
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwIntervalFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      il=3
      xpom=5.
      tpom=xpom+xpom+CrwgXd+10.
      Veta='%Independent parallelepiped'
      ichk=ichk+1
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      ichk)
        if(i.eq.1) then
          nCrwIndependent=CrwLastMade
          Veta='%Whole cell'
        else
          nCrwWholeCell=CrwLastMade
        endif
      enddo
      xpom=xqd*.5+50.
      Veta='%Step [A]'
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwStep=EdwLastMade
      il=il+1
      tpom=5.
      Veta='Ce%nter'
      xpomp=tpom+FeTxLengthUnder(Veta)+20.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,1)
      nEdwCenter=EdwLastMade
      Veta='Sc%ope [A]'
      xpom=xqd*.5+50.
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScope=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='C%utoff distance for atoms'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCutOffDist=EdwLastMade
      call FeQuestLblMake(id,xpom+dpom+10.,il,'[Ang]','L','N')
      call FeQuestRealEdwOpen(nEdwCutOffDist,CutOffDist,.false.,
     1                          .false.)
      if(Klic.ne.0) then
        tpom=xpom+dpom+70.
        Veta='%Calculate'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=150.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
        nRolDensityType=RolMenuLastMade
        call FeQuestRolMenuOpen(nRolDensityType,MenDen,7,DensityType+1)
      else
        nRolDensityType=0
      endif
      ScopeTypeOld=-1
1200  if(ScopeTypeOld.eq.ScopeType) go to 1500
      if(ScopeTypeOld.eq.ScopeAuto) then
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        call FeQuestEdwClose(nEdwStep)
      else if(ScopeTypeOld.eq.ScopeExplicit) then
        nEdw=nEdwIntervalFirst
        do i=1,3
          call FeQuestLblOff(nLblX(i))
          do j=1,3
            if(i.eq.1) call FeQuestLblOff(nLblMeze(j))
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
        enddo
      else
        ptname='[nic]'
        call SetRealArrayTo(ptx,3,0.)
        call SetRealArrayTo(pts,3,.5)
        call FeQuestEdwClose(nEdwCenter)
        call FeQuestEdwClose(nEdwScope)
        call FeQuestEdwClose(nEdwStep)
      endif
      if(ScopeType.eq.ScopeAuto) then
        call FeQuestCrwOpen(nCrwIndependent,CPIndUnit.eq.1)
        call FeQuestCrwOpen(nCrwWholeCell,CPIndUnit.ne.1)
        call FeQuestRealEdwOpen(nEdwStep,CPStep,.false.,.false.)
      else if(ScopeType.eq.ScopeExplicit) then
        nEdw=nEdwIntervalFirst
        do i=1,3
          call FeQuestLblOn(nLblX(i))
          do j=1,3
            if(i.eq.1) call FeQuestLblOn(nLblMeze(j))
            if(j.eq.1) then
              if(xrmn(i).lt.-330.) then
                pom=0.
              else
                pom=xrmn(i)
              endif
            else if(j.eq.2) then
              if(xrmn(i).lt.-330.) then
                pom=1.
              else
                pom=xrmx(i)
              endif
            else
              if(xrmn(i).lt.-330.) then
                if(i.le.3) then
                  if(CellPar(i,1,KPhase).gt.20.) then
                    pom=0.01
                  else if(CellPar(i,1,KPhase).gt.10.) then
                    pom=0.02
                  else
                    pom=0.05
                  endif
                else
                   pom=0.1
                endif
              else
                pom=dd(i)
              endif
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            nEdw=nEdw+1
          enddo
        enddo
      else
        if(ptname.eq.'[nic]') then
          Veta=' '
        else
          Veta=ptname
        endif
        call FeQuestStringEdwOpen(nEdwCenter,Veta)
        call FeQuestRealAEdwOpen(nEdwScope,pts,3,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwStep,CPStep,.false.,.false.)
      endif
      ScopeTypeOld=ScopeType
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwAuto.and.CheckNumber.le.nCrwPoint) then
        ScopeType=CheckNumber-nCrwAuto+1
        go to 1200
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCenter) then
        if(EventType.eq.EventCrw.and.EventNumber.le.nCrwPoint)
     1    go to 1500
        t256=EdwStringQuest(nEdwCenter)
        k=0
        call StToReal(t256,k,xp,3,.false.,ich)
        call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,IdSymmYes,
     1                      IdAtMolMixNo,Veta)
        if(Veta.ne.' ') then
          if(Veta.ne.'Jiz oznameno')
     1      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          ich=1
          go to 1520
        endif
        go to 1500
1520    EventType=EventEdw
        EventNumber=nEdwCenter
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(CrwLogicQuest(nCrwIndependent).and.ScopeType.eq.1) then
          CPIndUnit=1
        else
          CPIndUnit=0
        endif
        if(ScopeType.eq.1.or.ScopeType.eq.3) then
          i1=4
        else
          i1=1
        endif
        nEdw=nEdwIntervalFirst+(i1-1)*3
        do i=i1,NDim(KPhase)
          do j=1,3
            call FeQuestRealFromEdw(nEdw,pom)
            if(j.eq.1) then
              xrmn(i)=pom
            else if(j.eq.2) then
              xrmx(i)=pom
            else
              dd(i)=pom
            endif
            nEdw=nEdw+1
          enddo
        enddo
        if(ScopeType.eq.3) then
          ptname=EdwStringQuest(nEdwCenter)
          call FeQuestRealAFromEdw(nEdwScope,pts)
        else
          ptname='[nic]'
        endif
        if(ScopeType.ne.2)
     1    call FeQuestRealFromEdw(nEdwStep,CPStep)
        if(Klic.ne.0)
     1    DensityType=RolMenuSelectedQuest(nRolDensityType)-1
        call FeQuestRealFromEdw(nEdwCutOffDist,CutOffDist)
      endif
      call FeQuestRemove(id)
      return
100   format(6i1)
101   format(i6)
      end
