      subroutine FouNastavM80
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      logical EqIgCase
      if(NDatBlock.gt.1) then
1000    read(80,FormA80,end=1020,err=8000) t80
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(t80,k,Cislo)
          if(EqIgCase(Cislo,'begin')) go to 9999
        endif
        go to 1000
1020    call FeChybne(-1.,-1.,'the m80 file for '//
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))//
     2                ' has not been prepared.',' ',SeriousError)
        go to 8000
      endif
      go to 9999
8000  ErrFlag=1
9999  return
      end
