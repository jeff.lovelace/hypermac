      subroutine FouGetClosestPoint(imp,ich)
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer ix(3),n(3)
      real x(3),xm(3),xp(3),xq(3)
      call RecUnpack(imp,ix,nx,3)
      do i=1,3
        x(iorien(i))=xfmn(i)+float(ix(i)-1)*xdf(i)
      enddo
      do i=1,NSymmN(KPhase)
        call multm(rm(1,i,nsubs,KPhase),x,xp,3,3,1)
        call AddVek(xp,s6(1,i,nsubs,KPhase),xp,3)
        do 1500l=1,NLattVec(KPhase)
          call AddVek(xp,vt6(1,l,nsubs,KPhase),xq,3)
          do j=1,3
            xm(j)=xq(iorien(j))
1100        if(xm(j).ge.xfmn(j)-xplus(j)) go to 1200
            xm(j)=xm(j)+1.
            go to 1100
1200        if(xm(j).gt.xfmx(j)+xplus(j)) then
              xm(j)=xm(j)-1.
              go to 1200
            endif
            if(xm(j).lt.xfmn(j)-xplus(j)) go to 1500
          enddo
          do j=1,3
            xm(j)=(xm(j)-xfmn(j))/xdf(j)+1.
            n(j)=nint(xm(j))
            if(Pridavat(j)) then
              if(n(j).eq.0) then
                n(j)=nx(j)
              else if(n(j).eq.nx(j)+1) then
                n(j)=1
              endif
            endif
          enddo
          go to 2000
1500    continue
      enddo
      go to 9000
2000  imp=n(1)+nx(1)*(n(2)-1)+nxny*(n(3)-1)
9000  ich=0
9999  return
      end
