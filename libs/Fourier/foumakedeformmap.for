      subroutine FouMakeDeformMap
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      dimension ih(6)
      dimension F1Arr(:),F2Arr(:),AArr(:),BArr(:),sig(:)
      integer HArr(:,:)
      logical EqIgCase,EqIV
      allocatable HArr,F1Arr,F2Arr,AArr,BArr,sig
      call OpenFile(80,fln(:ifln)//'.m80','formatted','old')
      call FouNastavM80
      NRef=0
1100  read(80,FormA,end=1200) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 1200
      endif
      read(Veta,Format80,err=8000,end=1200)
     1  (ih(i),i=1,MaxNDim),KPh
      if(ih(1).gt.900) go to 1200
      if(KPh.eq.KPhase) NRef=NRef+1
      go to 1100
1200  allocate(HArr(MaxNDim,NRef),F1Arr(NRef),F2Arr(NRef),
     1         AArr(NRef),BArr(NRef),sig(NRef))
      rewind 80
      n=0
1300  read(80,FormA,end=1500) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 1500
      endif
      read(Veta,Format80,err=8000,end=1500)
     1  (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2   acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
      if(ih(1).gt.900) go to 1300
      if(KPh.eq.KPhase) then
        n=n+1
        call CopyVekI(ih,HArr(1,n),MaxNDim)
        F1Arr(n)=fo1
        F2Arr(n)=-1.
        sig(n)=sigfo1
      endif
      go to 1300
1500  call CloseIfOpened(80)
      Veta='D:\Structures\Jana2006\Work\Dominik Schaniel\Rupy\Final\'//
     1     'GS\Rupy-GS-final.m80'
      call OpenFile(80,Veta,'formatted','old')
      call FouNastavM80
      ist=0
1600  read(80,FormA,end=2000) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 2000
      endif
      read(Veta,Format80,err=8000,end=2000)
     1  (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2   acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
      if(ih(1).gt.900) go to 2000
      if(KPh.eq.KPhase) then
        do i=1,NRef
          j=mod(i+ist-1,NRef)+1
          if(EqIV(ih,HArr(1,j),MaxNDim)) then
            ist=j
            F2Arr(j)=fo1
            AArr(j)=ac
            BArr(j)=bc
            go to 1600
          endif
        enddo
      endif
      go to 1600
2000  call CloseIfOpened(80)
      suma1=0.
      suma2=0.
      do i=1,NRef
        if(F2Arr(i).gt.0.) then
          if(sig(i).gt.0) then
            w=1./sig(i)**2
          else
            w=0.
          endif
          suma1=suma1+w*F1Arr(i)*F2Arr(i)
          suma2=suma2+w*F2Arr(i)**2
        endif
      enddo
      scpom=suma1/suma2
      do i=1,NRef
        F2Arr(i)=F2Arr(i)*scpom
      enddo
      call CopyFile(fln(:ifln)//'.m80',fln(:ifln)//'.z80')
      call OpenFile(80,fln(:ifln)//'.z80','formatted','old')
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m80','formatted','old')
      if(NDatBlock.gt.1) then
2100    read(80,FormA,err=8000) Veta
        write(ln,FormA) Veta(:idel(Veta))
        k=0
        call Kus(t80,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(t80,k,Cislo)
          if(EqIgCase(Cislo,'begin')) go to 9999
        endif
        go to 2100
      endif
      do i=1,NRef
        if(F2Arr(i).gt.0.) then
          write(ln,Format80)
     1      (HArr(j,i),j=1,MaxNDim),KPhase,F1Arr(i),F1Arr(i),F2Arr(i),
     2      AArr(i),BArr(i),(0.,j=1,6),sig(i),sig(i)
        endif
      enddo
      call CloseIfOpened(80)
      call CloseIfOpened(ln)
      go to 9999
8000  call FeReadError(80)
9999  return
      end
