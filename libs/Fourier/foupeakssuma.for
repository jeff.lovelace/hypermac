      subroutine FouPeaksSuma(der,x,am,ps,n)
      include 'fepc.cmn'
      dimension der(n),x(*),am(*),ps(n,*)
      im=0
      do i=1,n
        deri=der(i)
        if(deri.eq.0.) then
          im=im+i
          cycle
        endif
        do j=1,i
          im=im+1
          derj=der(j)
          if(derj.eq.0) cycle
          am(im)=am(im)+deri*derj
        enddo
        do j=1,3
          ps(i,j)=ps(i,j)+deri*x(j)
        enddo
      enddo
      return
      end
