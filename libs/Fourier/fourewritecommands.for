      subroutine FouRewriteCommands(Klic)
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      call WriteKeys('fourier')
      if(ScopeType.eq.1.or.ScopeType.eq.3) then
        ip=4
      else
        ip=1
      endif
      do i=ip,NDim(KPhase)
        if(xrmn(i).gt.-300.) then
          write(t80,101) xrmn(i),xrmx(i),dd(i)
          call ZdrcniCisla(t80,3)
        else
          t80='0 1 0.1'
        endif
        if(i.gt.3.and.t80.eq.'0 1 0.1') cycle
        if(NDim(KPhase).eq.3) then
          t80='  '//NactiKeywords(nCmdxlim+i-1)(1:4)//' '//
     1        t80(:idel(t80))
        else
          t80='  '//NactiKeywords(nCmdx1lim+i-1)(1:5)//' '//
     1        t80(:idel(t80))
        endif
        write(55,FormA) t80(:idel(t80))
      enddo
      if(ScopeType.eq.3) then
        t80='  '//
     1      NactiKeyWords(nCmdcenter)(:idel(NactiKeyWords(nCmdcenter)))
     2    //' '//ptname(:idel(ptname))
        write(55,FormA) t80(:idel(t80))
        write(t80,101) pts
        call ZdrcniCisla(t80,3)
        t80='  '//
     1      NactiKeywords(nCmdscope)(:idel(NactiKeyWords(nCmdscope)))//
     2      ' '//t80(:idel(t80))
        write(55,FormA) t80(:idel(t80))
      endif
      if(FouRefStr.ne.' ') then
        t80='  '//
     1    NactiKeywords(nCmdRefStr)(:idel(NactiKeyWords(nCmdRefStr)))//
     2    ' '//FouRefStr(:idel(FouRefStr))
        write(55,FormA) t80(:idel(t80))
      endif
      write(55,'(''end fourier'')')
      call DopisKeys(Klic)
      call RewriteTitle(' ')
      return
101   format(3f10.6)
      end
