      subroutine FouPeakInt
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer MezD(3)
      real DMaxPolar(:,:),Tbl(*),x(3),dx(3),dxo(3),xp(3)
      allocatable DMaxPolar
      save DMaxPolar,DMez,DMezStep,MezD,MxStep
      data delta/.01/
      entry FouPeakIntProlog(DMezIn)
      DMez=DMezIn
      DMezStep=DMez*Delta
      MxStep=nint(1./Delta)
      do i=1,3
        l=iorien(i)
        MezD(i)=DMez*rcp(l,nsubs,KPhase)/xdf(i)+1.
      enddo
      allocate(DMaxPolar(0:72,0:36))
      go to 9999
      entry FouPeakIntMake(Tbl,x,Rho)
      da=5.*ToRad
      chi=0.
      do ichi=0,36
        snc=sin(chi)
        csc=cos(chi)
        phi=0.
        do iphi=0,72
          snp=sin(phi)
          csp=cos(phi)
          dxo(1)=DMezStep*csp*snc
          dxo(2)=DMezStep*snp*snc
          dxo(3)=DMezStep*csc
          call MultM(TrToOrthoI(1,nsubs,KPhase),dxo,dx,3,3,1)
          xp=x
          den2=FouExMap(Tbl,xp,ich)
          xp=xp+dx
          den3=FouExMap(Tbl,xp,ich)
          xp=xp+dx
          den4=FouExMap(Tbl,xp,ich)
          do i=2,MxStep
            den1=den2
            den2=den3
            den3=den4
            xp=xp+dx
            den4=FouExMap(Tbl,xp,ich)
            b=.5*(den3-den1)
            c=den2
            a=-den2+.5*(den3+den1)
            if(den3.lt.0.) then
              det=b**2-4.*a*c
              if(det.gt.0.) then
                if(a.eq.0.) then
                  DMaxPolar(iphi,ichi)=-c/b
                else
                  bb=.5*(-b+sqrt(Det))/a
                  aa=.5*(-b-sqrt(Det))/a
                  DMaxPolar(iphi,ichi)=aa
                endif
              else
                DMaxPolar(iphi,ichi)=0.
              endif
              go to 1500
c            else if(den3.gt.den2*1.002.and.den4.gt.den3*1.002) then
            else if(den3.gt.den2.and.den4.gt.den3) then
              if(a.ne.0.) then
                DMaxPolar(iphi,ichi)=-.5*b/a
              else
                DMaxPolar(iphi,ichi)=0.
              endif
              go to 1500
            endif
          enddo
          DMaxPolar(iphi,ichi)=DMez
          go to 1600
1500      DMaxPolar(iphi,ichi)=(float(i)+DMaxPolar(iphi,ichi))*
     1                         DMezStep
1600      phi=phi+da
        enddo
        chi=chi+da
      enddo
      Rho=0.
      do id1=-MezD(1),MezD(1)
        io1=iorien(1)
        dx(iorien(1))=float(id1)*xdf(1)
        do id2=-MezD(2),MezD(2)
          dx(iorien(2))=float(id2)*xdf(2)
          do id3=-MezD(3),MezD(3)
            dx(iorien(3))=float(id3)*xdf(3)
            call MultM(TrToOrtho(1,nsubs,KPhase),dx,dxo,3,3,1)
            D=sqrt(scalmul(dxo,dxo))
            if(D.gt..001) then
              pom=dxo(3)/D
              if(abs(pom).lt.1.) then
                chi=acos(pom)/ToRad
                pom=1./sqrt(1.-pom**2)
                if(abs(dxo(1)).gt..001.and.abs(dxo(2)).gt..001.and.
     1            pom.gt..001) then
                  phi=atan2(dxo(2),dxo(1))/ToRad
                  if(phi.lt.0.) phi=phi+360.
                else
                  phi=0.
                endif
              else
                if(pom.gt.0.) then
                  chi=0.
                  phi=0.
                else
                  chi=180.
                  phi=0.
                endif
              endif
              iphi=nint(phi/5.)
              ichi=nint(chi/5.)
              if(D.gt.DMaxPolar(iphi,ichi)) cycle
            endif
            xp=x+dx
            pom=FouExMap(tbl,xp,ich)
            if(ich.eq.0) Rho=Rho+pom
          enddo
        enddo
      enddo
      go to 9999
      entry FouPeakIntEpilog
      if(allocated(DMaxPolar)) deallocate(DMaxPolar)
9999  return
      end
