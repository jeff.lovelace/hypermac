      subroutine FouSetLimits(ias,bs,m,isw,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ias(3,*),bs(3,*),idal(:)
      allocatable idal
      allocate(idal(m))
      call SetIntArrayTo(idal,m,0)
      dx=9999.
      do i=1,3
        n1=0
        do j=1,m
          if(iabs(ias(i,j)).ne.1.or.bs(i,j).ne.0.) cycle
          n1=n1+1
        enddo
        pom=float(n1)*CellPar(i,isw,KPhase)/(float(m)*ptstep)
        if(pom.lt.dx) then
          dx=pom
          ismer=i
        endif
      enddo
      nop(isw)=231
      if(ismer.eq.2) nop(isw)=312
      if(ismer.eq.3) nop(isw)=123
      if(NDim(KPhase).gt.3) then
        nd=NDimI(KPhase)
        i=10**nd
        nop(isw)=nop(isw)*i+ifix(float(i)*.456)
        do i=4,NDim(KPhase)
          fourmn(i,isw)=0.
          fourmx(i,isw)=1.
        enddo
      endif
      do i=ismer,ismer+2
        k=mod(i-1,3)+1
        n1=0
        n2=0
        bsmin=9999.
        do j=1,m
          if(idal(j).ne.0) cycle
          n2=n2+1
          if(ias(k,j).ne.1) go to 4100
          if(bs(k,j).ne.0.) go to 4200
          n1=n1+1
          cycle
4100      bsmin=min(bsmin,bs(k,j))
4200      idal(j)=1
        enddo
        if(bsmin.gt.10.) bsmin=0.
        fourmn(k,isw)=bsmin*.5
        fourmx(k,isw)=fourmn(k,isw)+float(n1)/float(n2)
      enddo
      deallocate(idal)
      return
      end
