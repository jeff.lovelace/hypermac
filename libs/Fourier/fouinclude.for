      subroutine FouInclude(u,RhoInt,RhoPoint,XRho,n,npik)
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension u(*),XRho(3,*)
      integer RhoInt,RhoPoint
      if(n.lt.npik) then
        n=n+1
        NRho(n)=n
        kam=n
      else
        if(TypeOfSearch.eq.SearchCharge) then
          do i=1,n
            if(RhoInt.lt.RhoIntArr(NRho(i))) go to 1200
          enddo
        else
          do i=1,n
            if(RhoPoint.lt.RhoPointArr(NRho(i))) go to 1200
          enddo
        endif
        go to 9999
1200    kam=NRho(n)
        do j=n,i+1,-1
          NRho(j)=NRho(j-1)
        enddo
        NRho(i)=kam
      endif
      RhoPointArr(kam)=RhoPoint
      RhoIntArr(kam)=RhoInt
      call CopyVek(u,XRho(1,kam),3)
      if(n.ge.npik) then
        if(TypeOfSearch.eq.SearchCharge) then
          call indexx(n,RhoIntArr,NRho)
          RhoMez=RhoIntArr(NRho(npik))
        else
          call indexx(n,RhoPointArr,NRho)
          RhoMez=RhoPointArr(NRho(npik))
        endif
      endif
9999  return
      end
