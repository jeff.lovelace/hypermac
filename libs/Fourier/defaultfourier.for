      subroutine DefaultFourier
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      NactiInt=20
      NactiReal=20
      NactiComposed=12
      NactiKeys=NactiInt+NactiReal+NactiComposed
      call CopyVekI(DefIntFour,NacetlInt   ,NactiInt)
      call CopyVekI(DefIntFour,DefaultInt  ,NactiInt)
      call CopyVekI(DefIntFour,CmdIntFour,NactiInt)
      i=NactiInt+1
      call CopyVek(DefRealFour(i),NacetlReal(i) ,NactiReal)
      call CopyVek(DefRealFour(i),DefaultReal(i),NactiReal)
      call CopyVek(DefRealFour(i),CmdRealFour, NactiReal)
      do i=1,NactiKeys
        NactiKeywords(i)=IdFour(i)
      enddo
      ptname='[nic]'
      ptx(1)=0.
      ptx(2)=0.
      ptx(3)=0.
      pts(1)=.5
      pts(2)=.5
      pts(3)=.5
      FouRefStr=' '
      return
      end
