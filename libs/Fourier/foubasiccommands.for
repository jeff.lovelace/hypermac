      subroutine FouBasicCommands
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension MapTypeEnable(10)
      character*256 EdwStringQuest
      character*80  Veta
      integer RolMenuSelectedQuest,EdwStateQuest
      logical CrwLogicQuest
      save nEdwMin,nEdwMax,nEdwOmit,nEdwComp,nEdwUiso,nCrwOmit,nCrwDiff,
     1     nCrwUseWeight,nCrwFract,nCrwSinLim,nLblOmit,nRolMenuMapType,
     2     nEdwRefStr,nButtBrowse,niso,UIso
      entry FouBasicCommandsMake(id)
      il=1
      tpom=5.
      Veta='%Map type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=0.
      do i=1,10
        dpom=max(dpom,FeTxLength(MapType(i)))
        if(.not.ChargeDensities.and.i.ge.7.and.i.le.8) then
          MapTypeEnable(i)=0
        else
          MapTypeEnable(i)=1
        endif
      enddo
      dpom=dpom+EdwYd+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuMapType=RolMenuLastMade
      call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,MapType,
     1                            MapTypeEnable,10,NacetlInt(nCmdmapa))
      if(lite(KPhase).eq.0) then
        Veta='U(iso)'
        niso=nCmdUOverAll
      else
        Veta='B(iso)'
        niso=nCmdBOverAll
      endif
      Uiso=NacetlReal(niso)
      tpom=xpom+dpom+20.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwUiso=EdwLastMade
      call FeQuestRealEdwOpen(nEdwUiso,Uiso,.false.,.false.)
      if(NacetlInt(nCmdmapa).gt.3) call FeQuestEdwDisable(nEdwUiso)
      il=il+1
      Veta='%Omit not-matching reflections'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwOmit=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlReal(nCmdvyber).gt.0.)
      il=il+1
      Veta='%Reflections with F(obs)>'
      xpome=tpom+FeTxLengthUnder(Veta)+10.
      dpom=70.
      call FeQuestEdwMake(id,xpom-10.,il,xpom,il,Veta,'R',dpom,EdwYd,0)
      nEdwOmit=EdwLastMade
      tpome=xpom+dpom+10.
      Veta='*F(calc) will be omitted'
      call FeQuestLblMake(id,tpome,il,Veta,'L','N')
      nLblOmit=LblLastMade
      call FeQuestLblDisable(LblLastMade)
      call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmdvyber),
     1                        .false.,.false.)
      call FeQuestLblOn(nLblOmit)
      if(NacetlReal(nCmdvyber).le.0.) then
        call FeQuestEdwDisable(nEdwOmit)
        call FeQuestLblDisable(nLblOmit)
      endif
      il=il+1
      Veta='%Use weighting of reflections'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwUseWeight=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdUseWeight).gt.0)
      il=il+1
      Veta='A%pply sin(th)/lambda limits'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwSinLim=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdUseSinLim).gt.0)
      il=il+1
      tpome=tpom+FeTxLength(Veta)+20.
      Veta='sin(th)/lambda      mi%n.'
      xpome=tpome+FeTxLength(Veta)+10.
      call FeQuestEdwMake(id,xpom-10.,il,xpom,il,Veta,'R',dpom,EdwYd,0)
      nEdwMin=EdwLastMade
      Veta='ma%x.'
      tpome=xpom+dpom+20.
      xpome=tpome+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwMax=EdwLastMade
      call FeQuestRealEdwOpen(nEdwMin,NacetlReal(nCmdsnlmnf),.false.,
     1                        .false.)
      call FeQuestRealEdwOpen(nEdwMax,NacetlReal(nCmdsnlmxf),.false.,
     1                        .false.)
      if(NacetlInt(nCmdUseSinLim).le.0) then
        call FeQuestEdwDisable(nEdwMin)
        call FeQuestEdwDisable(nEdwMax)
      endif
      il=il+1
      Veta='%Browse'
      dpomb=FeTxLengthUnder(Veta)+10.
      dpom=XdQuestFou-xpom-2.*KartSidePruh-dpomb-20.
      call FeQuestEdwMake(id,tpom,il,xpom,il,
     1                    'D%efine reference structure','L',dpom,
     2                    EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,FouRefStr)
      nEdwRefStr=EdwLastMade
      xpomb=xpom+dpom+10.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtBrowse=ButtonLastMade
      if(NacetlInt(nCmdmapa).ne.10) then
        call FeQuestEdwDisable(nEdwRefStr)
        call FeQuestButtonDisable(nButtBrowse)
      endif
      if(NTwin.gt.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        call FeQuestLblMake(id,XdQuestFou*.5,il,'Correction of '//
     1                      'F(obs) for twinning','C','B')
        il=il+1
        pom=50.
        Veta='%Difference'
        tpom=XdQuestFou*.5-pom-FeTxLengthUnder(Veta)
        xpom=tpom-CrwgXd-10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,0,1)
        xpom=XdQuestFou*.5+pom
        tpom=xpom+CrwgXd+10.
        nCrwDiff=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      NacetlInt(nCmdTMethod).eq.SubstMethod)
        call FeQuestCrwMake(id,tpom,il,xpom,il,'Fr%action','L',CrwgXd,
     1                      CrwgYd,0,1)
        nCrwFract=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,
     1                      NacetlInt(nCmdTMethod).ne.SubstMethod)
      endif
      if(NComp(KPhase).gt.1) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Composite part no.'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwComp=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdnsubs),.false.)
      endif
      go to 9999
      entry FouBasicCommandsCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwOmit) then
        if(CrwLogicQuest(nCrwOmit)) then
          call FeQuestRealEdwOpen(nEdwOmit,NacetlReal(nCmdvyber),
     1                            .false.,.false.)
          call FeQuestLblOn(nLblOmit)
        else
          call FeQuestEdwDisable(nEdwOmit)
          call FeQuestLblDisable(nLblOmit)
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSinLim) then
        if(CrwLogicQuest(nCrwSinLim)) then
          call FeQuestRealEdwOpen(nEdwMin,NacetlReal(nCmdsnlmnf),
     1                            .false.,.false.)
          call FeQuestRealEdwOpen(nEdwMax,NacetlReal(nCmdsnlmxf),
     1                            .false.,.false.)
        else
          call FeQuestEdwDisable(nEdwMin)
          call FeQuestEdwDisable(nEdwMax)
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuMapType) then
        NacetlInt(nCmdMapa)=RolMenuSelectedQuest(nRolMenuMapType)
        if(NacetlInt(nCmdmapa).ge.1.and.NacetlInt(nCmdmapa).le.3) then
          call FeQuestRealEdwOpen(nEdwUiso,Uiso,.false.,.false.)
        else
          Uiso=DefRealFour(niso)
          call FeQuestEdwDisable(nEdwUiso)
        endif
        if(NacetlInt(nCmdmapa).eq.10) then
          if(EdwStateQuest(nEdwRefStr).ne.EdwOpened) then
            call FeQuestStringEdwOpen(nEdwRefStr,FouRefStr)
            call FeQuestButtonOff(nButtBrowse)
          endif
        else
          if(EdwStateQuest(nEdwRefStr).eq.EdwOpened) then
            FouRefStr=EdwStringQuest(nEdwRefStr)
            call FeQuestEdwDisable(nEdwRefStr)
            call FeQuestButtonDisable(nButtBrowse)
          endif
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        Veta=EdwStringQuest(nEdwRefStr)
        call FeFileManager('Define the reference structure',Veta,' ',1,
     1                     .false.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwRefStr,Veta)
      endif
      go to 9999
      entry FouBasicCommandsUpdate
      NacetlInt(nCmdMapa)=RolMenuSelectedQuest(nRolMenuMapType)
      if(NacetlInt(nCmdmapa).ge.1.and.NacetlInt(nCmdmapa).le.3) then
        call FeQuestRealFromEdw(nEdwUiso,NacetlReal(niso))
      else
        NacetlReal(niso)=DefRealFour(niso)
      endif
      if(EdwStateQuest(nEdwRefStr).eq.EdwOpened) then
        FouRefStr=EdwStringQuest(nEdwRefStr)
      else
        FouRefStr=' '
      endif
      if(CrwLogicQuest(nCrwUseWeight)) then
        NacetlInt(nCmdUseWeight)=1
      else
        NacetlInt(nCmdUseWeight)=0
      endif
      call FeQuestRealFromEdw(nEdwMin,NacetlReal(nCmdsnlmnf))
      call FeQuestRealFromEdw(nEdwMax,NacetlReal(nCmdsnlmxf))
      if(CrwLogicQuest(nCrwOmit)) then
        call FeQuestRealFromEdw(nEdwOmit,NacetlReal(nCmdvyber))
      else
        NacetlReal(nCmdvyber)=0
      endif
      if(CrwLogicQuest(nCrwSinLim)) then
        NacetlInt(nCmdUseSinLim)=1
        call FeQuestRealFromEdw(nEdwMin,NacetlReal(nCmdsnlmnf))
        call FeQuestRealFromEdw(nEdwMax,NacetlReal(nCmdsnlmxf))
      else
        NacetlInt(nCmdUseSinLim)=0
        NacetlReal(nCmdsnlmnf)=DefRealFour(nCmdsnlmnf)
        NacetlReal(nCmdsnlmxf)=DefRealFour(nCmdsnlmxf)
      endif
      if(NTwin.gt.1) then
        if(CrwLogicQuest(nCrwDiff)) then
          NacetlInt(nCmdTMethod)=0
        else
          NacetlInt(nCmdTMethod)=1
        endif
      endif
      if(NComp(KPhase).gt.1) then
        call FeQuestIntFromEdw(nEdwComp,NacetlInt(nCmdnsubs))
      else
        NacetlInt(nCmdnsubs)=1
      endif
9999  return
      end
