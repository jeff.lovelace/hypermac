      subroutine FouPeaksCommands
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 Veta
      integer LblStateQuest,EdwStateQuest
      logical CrwLogicQuest,lpom
      save nCrwSearchForPeaks,nCrwDefault,nCrwExplicitly,nEdwPositive,
     1     nEdwNegative,nEdwNoOfHarm,nEdwDMax,nLblMaxPeaks,nLblMod,
     2     nCrwIntSphere,nEdwIntSphereRad,nCrwIntAdjust,Radius,
     3     nEdwIntAdjustRad,nLinka
      entry FouPeaksCommandsMake(id)
      il=1
      xpom=8.
      tpom=xpom+CrwXd+10.
      Veta='%search for peaks in the calculated map'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,0)
      nCrwSearchForPeaks=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,NacetlInt(nCmdLPeaks).gt.0)
      il=il+1
      tpom=30.
      call FeQuestLblMake(id,tpom,il,'Maximum number of peaks:','L','N')
      nLblMaxPeaks=LblLastMade
      il=il+1
      tpom=xpom+CrwgXd+10.
      Veta='%Default'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      nCrwDefault=CrwLastMade
      lpom=NacetlInt(nCmdPPeaks).lt.0.and.NacetlInt(nCmdNPeaks).lt.0
      call FeQuestCrwOpen(CrwLastMade,lpom)
      il=il+1
      Veta='%Explicitly'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,1)
      nCrwExplicitly=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,.not.lpom)
      Veta='%Positive'
      tpom=xpom+100.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPositive=EdwLastMade
      tpom=xpom+dpom+30.
      Veta='%Negative'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNegative=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Charge integration:'
      xpom=XdQuestFou*.5
      call FeQuestLblMake(id,xpom,il,Veta,'C','B')
      Veta='in a %fixed sphere of radius'
      xpom=5.
      tpom=xpom+CrwgXd+10.
      dpom=60.
      Radius=NacetlReal(nCmdIntRad)
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.NacetlInt(nCmdIntMethod))
        tpome=tpom+FeTxLengthUnder(Veta)+5.
        xpome=tpome+25.
        call FeQuestEdwMake(id,tpome,il,xpome,il,'=>','L',dpom,EdwYd,0)
        if(i.eq.1) then
          nCrwIntSphere=CrwLastMade
          nEdwIntSphereRad=EdwLastMade
          Veta='in an %adjusted shape within maximal radius'
        else
          nCrwIntAdjust=CrwLastMade
          nEdwIntAdjustRad=EdwLastMade
        endif
      enddo
      il=il+1
      Veta='The second method is more precise but slower'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        nLinka=LinkaLastMade
        il=il+1
        xpom=XdQuestFou*.5
        Veta='Interpretation of displacement waves:'
        call FeQuestLblMake(id,xpom,il,Veta,'C','B')
        nLblMod=LblLastMade
        il=il+1
        tpom=5.
        xpom=200.
        dpom=30.
        Veta='No. of %harmonics'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwNoOfHarm=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NacetlInt(nCmdkharm),.false.)
        il=il+1
        dpom=50.
        Veta='Ma%ximal modulation displacement'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDMax=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,NacetlReal(nCmddmax),
     1                          .false.,.false.)
      else
        nLblMod=0
        nEdwNoOfHarm=0
        nEdwDMax=0
      endif
1400  if(CrwLogicQuest(nCrwSearchForPeaks)) then
        if(LblStateQuest(nLblMaxPeaks).eq.LblOff) then
          call FeQuestLblOn(nLblMaxPeaks)
          lpom=NacetlInt(nCmdPPeaks).lt.0.and.
     1         NacetlInt(nCmdNPeaks).lt.0
          call FeQuestCrwOpen(nCrwDefault,lpom)
          call FeQuestCrwOpen(nCrwExplicitly,.not.lpom)
          if(NDimI(KPhase).gt.0) then
            call FeQuestLinkaOn(nLinka)
            call FeQuestLblOn(nLblMod)
            call FeQuestIntEdwOpen(nEdwNoOfHarm,NacetlInt(nCmdkharm),
     1                             .false.)
            call FeQuestRealEdwOpen(nEdwDMax,NacetlReal(nCmddmax),
     1                              .false.,.false.)
          endif
        endif
        if(CrwLogicQuest(nCrwExplicitly)) then
          k=nCmdPPeaks
          nEdw=nEdwPositive
          do i=1,2
            if(NacetlInt(k).gt.0) then
              j=NacetlInt(k)
            else
              if(i.eq.1) then
                j=50
              else
                j=5
              endif
              NacetlInt(k)=j
            endif
            call FeQuestIntEdwOpen(nEdw,j,.false.)
            k=k+1
            nEdw=nEdw+1
          enddo
        else
          NacetlInt(nCmdPPeaks)=-333
          NacetlInt(nCmdNPeaks)=-333
          call FeQuestEdwClose(nEdwPositive)
          call FeQuestEdwClose(nEdwNegative)
        endif
      else
        call FeQuestLblOff(nLblMaxPeaks)
        call FeQuestCrwClose(nCrwDefault)
        call FeQuestCrwClose(nCrwExplicitly)
        call FeQuestEdwClose(nEdwPositive)
        call FeQuestEdwClose(nEdwNegative)
        if(NDimI(KPhase).gt.0) then
          call FeQuestLinkaOff(nLinka)
          call FeQuestLblOff(nLblMod)
          call FeQuestEdwClose(nEdwNoOfHarm)
          call FeQuestEdwClose(nEdwDMax)
        endif
      endif
      if(CrwLogicQuest(nCrwIntSphere)) then
        if(EdwStateQuest(nEdwIntAdjustRad).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwIntAdjustRad,Radius)
          Radius=Radius*.5
        endif
        call FeQuestEdwClose(nEdwIntAdjustRad)
        call FeQuestRealEdwOpen(nEdwIntSphereRad,Radius,.false.,.false.)
      else
        if(EdwStateQuest(nEdwIntSphereRad).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwIntSphereRad,Radius)
          Radius=Radius*2.
        endif
        call FeQuestEdwClose(nEdwIntSphereRad)
        call FeQuestRealEdwOpen(nEdwIntAdjustRad,Radius,.false.,.false.)
      endif
      go to 9999
      entry FouPeaksCommandsCheck
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwExplicitly.or.CheckNumber.eq.nCrwDefault))
     2  then
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwIntSphere.or.CheckNumber.eq.nCrwIntAdjust))
     2  then
        go to 1400
      else if(CheckType.eq.
     1        EventCrw.and.CheckNumber.eq.nCrwSearchForPeaks) then
        go to 1400
      endif
      go to 9999
      entry FouPeaksCommandsUpdate
      if(CrwLogicQuest(nCrwSearchForPeaks)) then
        NacetlInt(nCmdLPeaks)=1
        if(CrwLogicQuest(nCrwDefault)) then
          NacetlInt(nCmdPPeaks)=-333
          NacetlInt(nCmdNPeaks)=-333
        else
          call FeQuestIntFromEdw(nEdwPositive,NacetlInt(nCmdPPeaks))
          call FeQuestIntFromEdw(nEdwNegative,NacetlInt(nCmdNPeaks))
        endif
        if(NDim(KPhase).gt.3) then
          call FeQuestIntFromEdw(nEdwNoOfHarm,NacetlInt(nCmdKHarm))
          call FeQuestRealFromEdw(nEdwDMax,NacetlReal(nCmdDMax))
        endif
      else
        NacetlInt(nCmdLPeaks)=0
        NacetlInt(nCmdPPeaks)=-333
        NacetlInt(nCmdNPeaks)=-333
        if(NDim(KPhase).gt.3) then
          NacetlInt(nCmdKHarm)=DefIntFour(nCmdKHarm)
          NacetlReal(nCmdDMax)=DefRealFour(nCmdDMax)
        endif
      endif
      if(CrwLogicQuest(nCrwIntSphere)) then
        call FeQuestRealFromEdw(nEdwIntSphereRad,NacetlReal(nCmdIntRad))
        NacetlInt(nCmdIntMethod)=1
      else
        call FeQuestRealFromEdw(nEdwIntAdjustRad,NacetlReal(nCmdIntRad))
        NacetlInt(nCmdIntMethod)=2
      endif
9999  return
      end
