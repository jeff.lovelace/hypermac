      subroutine FouPeakIntSimple
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      integer MezD(3)
      real x(*),xp(3),xd(3),xdp(3),Tbl(*)
      save MezD,DMez
      entry FouPeakIntSimpleProlog(DMezIn)
      DMez=DMezIn
      do i=1,3
        l=iorien(i)
        MezD(i)=DMez*rcp(l,nsubs,KPhase)/xdf(i)+1.
      enddo
      go to 9999
      entry FouPeakIntSimpleMake(Tbl,x,RhoPointR,Rho)
      Rho=0.
      do id1=-MezD(1),MezD(1)
        io1=iorien(1)
        xp(iorien(1))=x(iorien(1))+float(id1)*xdf(1)
        do id2=-MezD(2),MezD(2)
          xp(iorien(2))=x(iorien(2))+float(id2)*xdf(2)
          do id3=-MezD(3),MezD(3)
            xp(iorien(3))=x(iorien(3))+float(id3)*xdf(3)
            do k=1,3
              xd(k)=xp(k)-x(k)
            enddo
            call Multm(MetTens(1,nsubs,KPhase),xd,xdp,3,3,1)
            D=sqrt(scalmul(xd,xdp))
            if(D.le.DMez) then
              pom=FouExMap(tbl,xp,ich)
              if(D.gt..3.and.pom.gt.RhoPointR*.8.or.
     1           pom.lt.0.) cycle
              if(ich.eq.0) Rho=Rho+pom
            endif
          enddo
        enddo
      enddo
9999  return
      end
