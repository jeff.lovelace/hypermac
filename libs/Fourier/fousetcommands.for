      subroutine FouSetCommands(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      data LastListek/0/
      call RewriteTitle('Fourier-commands')
      call FouOpenCommands
      if(ErrFlag.ne.0) go to 9999
      XdQuestFou=500.
      call FeKartCreate(-1.,-1.,XdQuestFou,16,'Fourier commands',0,0)
      call FeCreateListek('Basic',1)
      KartIdBasic=KartLastId
      call FouBasicCommandsMake(KartIdBasic)
      call FeCreateListek('Scope',1)
      KartIdScope=KartLastId
      call FouReadScopeMake(KartIdScope)
      call FeCreateListek('Peaks',1)
      KartIdPeaks=KartLastId
      call FouPeaksCommandsMake(KartIdPeaks)
      if(ContourCallFourier) then
        if(LastListek.eq.0) LastListek=1
      else
        LastListek=1
      endif
      call FeCompleteKart(LastListek)
2500  ErrFlag=0
3000  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsUpdate
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeUpdate
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsCheck
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeCheck
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsCheck
        endif
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdBasic) then
          call FouBasicCommandsUpdate
          LastListek=1
        else if(KartId.eq.KartIdScope) then
          call FouReadScopeUpdate
          LastListek=2
        else if(KartId.eq.KartIdPeaks) then
          call FouPeaksCommandsUpdate
          LastListek=3
        endif
      endif
      call FeDestroyKart
      if(ich.ne.0) go to 9999
      if(ContourCallFourier) then
        Klic=1
      else
        Klic=0
      endif
      call FouRewriteCommands(Klic)
9999  return
      end
