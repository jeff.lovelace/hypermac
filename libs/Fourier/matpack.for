      function MatPack(ih,ihmn,ihd,n)
      include 'fepc.cmn'
      dimension ih(n),ihd(n),ihmn(n)
      MatPack=ih(n)-ihmn(n)
      do i=n-1,1,-1
        MatPack=MatPack*ihd(i)+ih(i)-ihmn(i)
      enddo
      MatPack=MatPack+1
      return
      end
