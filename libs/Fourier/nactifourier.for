      subroutine NactiFourier
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension xp(3)
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('fourier',i)
      if(i.ne.1) go to 9999
      call SetIntArrayTo(NactiRepeat,NactiKeys,0)
      PreskocVykricnik=.true.
      izpet=0
1100  call NactiCommon(M50,izpet)
      if(ErrFlag.ne.0) go to 9999
      if(izpet.eq.0) then
        call CopyVekI(NacetlInt ,CmdIntFour ,NactiInt)
        call CopyVek (NacetlReal(NactiInt+1),CmdRealFour,NactiReal)
        if(lite(KPhase).eq.0) then
          OverAllB(KDatBlock)=NacetlReal(nCmdUOverAll)
        else
          OverAllB(KDatBlock)=NacetlReal(nCmdBOverAll)
        endif
        go to 9999
      else if(izpet.ge.nCmdxlim.and.izpet.le.nCmdx6lim) then
        if(izpet.le.nCmdzlim) then
          j=izpet-nCmdxlim+1
        else
          j=izpet-nCmdx1lim+1
        endif
        call StToReal(NactiVeta,PosledniPozice,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        xrmn(j)=xp(1)
        xrmx(j)=xp(2)
        if(xp(3).le.0.) xp(3)=1.
        dd(j)=xp(3)
      else if(izpet.eq.nCmdcenter) then
        call StToReal(NactiVeta,PosledniPozice,ptx,3,.false.,ich)
        if(ich.eq.0) then
          ptname='[neco]'
          go to 1100
        endif
        ptname=NactiVeta(PosledniPozice+1:)
        call UprAt(ptname)
      else if(izpet.eq.nCmdscope) then
        call StToReal(NactiVeta,PosledniPozice,pts,3,.false.,ich)
        if(ich.ne.0) go to 9000
      else if(izpet.eq.nCmdRefStr) then
        call kus(NactiVeta,PosledniPozice,FouRefStr)
        PosledniPozice=len(NactiVeta)+1
      endif
      if(izpet.ne.0) go to 1100
9000  call ChybaNacteni(izpet)
      go to 1100
9999  call CloseIfOpened(m50)
      return
102   format(f15.5)
      end
