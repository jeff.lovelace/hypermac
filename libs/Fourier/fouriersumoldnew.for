      subroutine FourierSumOldNew
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      dimension xp(6),ix(6),ihp(6),Table(:),TableP(:),mxi(6),mni(6),
     1          mdi(6),mxin(6),mnin(6),mdin(6),ixn(6),ih(6)
      logical DelaTable
      allocatable Table,TableP
      equivalence (ix(1),ix1)
      call OpenMaps(81,fln(:ifln)//'.m81',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      rmax=0.
      rmin=0.
      write(81,rec=1) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,iorien,
     1                mapa,nsubs+(KPhase-1)*10,MameSatelity,
     2                nonModulated(KPhase)
      irec=1
      nn=0
      nd=NDimSum
      ncykl=1
      do i=nd+1,6
        ncykl=ncykl*nx(i)
      enddo
      nmp=1
      do i=1,nd
        nmp=nmp*nx(i)
      enddo
      n=2
      do i=1,nd
        n=n*max(hmax(i)-hmin(i)+1,nx(i))
      enddo
      allocate(Table (n))
      allocate(TableP(n))
      do it=1,ncykl
        call RecUnpack(it,ix,nx(nd+1),6-nd)
        do i=1,6-nd
          j=i+nd
          xp(j)=xfmn(j)+(ix(i)-1)*xdf(j)
        enddo
        j=nd
        do i=1,nd
          mni(j)=hmin(i)
          mxi(j)=hmax(i)
          j=j-1
        enddo
        mxc=1
        do i=1,nd
          mdi(i)=mxi(i)-mni(i)+1
          mxc=mxc*mdi(i)
        enddo
        call SetRealArrayTo(TableP,2*mxc,0.)
1200    read(82,end=1240) ihp,ai,bi
        j=nd
        do i=1,nd
          ih(j)=ihp(i)
          j=j-1
        enddo
        locc=MatPack(ih,mni,mdi,nd)
        locd=locc+mxc
        if(nd.ne.NDim(KPhase)) then
          xlz=0.
          do i=nd+1,NDim(KPhase)
            xlz=xlz+float(ihp(i))*xp(i)
          enddo
          xlz=pi2*xlz
          sinlz=sin(xlz)
          coslz=cos(xlz)
          TableP(locc)=TableP(locc)+ai*coslz+bi*sinlz
          TableP(locd)=TableP(locd)-ai*sinlz+bi*coslz
        else
          TableP(locc)=ai
          TableP(locd)=bi
        endif
        go to 1200
1240    rewind 82
        do nh=nd,1,-1
          DelaTable=mod(nh,2).eq.mod(nd,2)
          nxnh=nx(nh)
          do i=1,nh-1
            mxin(i)=mxi(i+1)
            mnin(i)=mni(i+1)
          enddo
          mnin(nh)=1
          mxin(nh)=nxnh
          do i=nh+1,nd
            mxin(i)=mxi(i)
            mnin(i)=mni(i)
          enddo
          mxcn=1
          do i=1,nd
            mdin(i)=mxin(i)-mnin(i)+1
            mxcn=mxcn*mdin(i)
          enddo
          if(DelaTable) then
            call SetRealArrayTo(Table,2*mxcn,0.)
          else
            call SetRealArrayTo(TableP,2*mxcn,0.)
          endif
          loccd=1
          do i=1,nh-1
            loccd=loccd*mdin(i)
          enddo
          call CopyVekI(mni,ix,nd)
          ix1=ix1-1
          do n=1,mxc
            if(DelaTable) then
              chy=TableP(n)
              dhy=TableP(n+mxc)
            else
              chy=Table(n)
              dhy=Table(n+mxc)
            endif
            do i=1,nd
              last=i
              ix(i)=ix(i)+1
              if(ix(i).gt.mxi(i).and.i.le.nd) then
                ix(i)=mni(i)
              else
                go to 1620
              endif
            enddo
1620        if(last.ne.1.or.n.eq.1) then
              do i=1,nh-1
                ixn(i)=ix(i+1)
              enddo
              do i=nh+1,nd
                ixn(i)=ix(i)
              enddo
              ixn(nh)=1
              loccp=MatPack(ixn,mnin,mdin,nd)
            endif
            locc=loccp
            ii=ix1-hmin(nh)+1
            do i=1,nxnh
              sinxh=SinTable(ii,i,nh)
              cosxh=CosTable(ii,i,nh)
              pom=chy*cosxh+dhy*sinxh
              if(DelaTable) then
                Table(locc)=Table(locc)+pom
              else
                TableP(locc)=TableP(locc)+pom
              endif
              if(nh.ne.1) then
                locd=locc+mxcn
                pom=-chy*sinxh+dhy*cosxh
                if(DelaTable) then
                  Table(locd)=Table(locd)+pom
                else
                  TableP(locd)=TableP(locd)+pom
                endif
              endif
              locc=locc+loccd
            enddo
          enddo
          if(nh.ne.1) then
            call CopyVekI(mxin,mxi,nd)
            call CopyVekI(mnin,mni,nd)
            call CopyVekI(mdin,mdi,nd)
            mxc=mxcn
          endif
        enddo
        if(.not.DelaTable) call CopyVek(TableP,Table,nmp)
        do i=1,nmp
          rmax=max(rmax,Table(i))
          rmin=min(rmin,Table(i))
        enddo
        IZdih=0
        do i=1,nmp/nxny
          irec=irec+1
          write(81,rec=irec)(table(IZdih+j),j=1,nxny)
          IZdih=IZdih+nxny
        enddo
      enddo
8000  irec=irec+1
      write(81,rec=irec) rmax,rmin
      call newln(1)
      write(Cislo,'(f15.2)') rmax
      call ZdrcniCisla(Cislo,1)
      t80='Maximal density : '//Cislo(:idel(Cislo))
      write(Cislo,'(f15.2)') rmin
      call ZdrcniCisla(Cislo,1)
      t80=t80(:idel(t80))//', minimal density : '//Cislo(:idel(Cislo))
      write(lst,FormA1)(t80(i:i),i=1,idel(t80))
      if(Mapa.eq.6) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.eq.0) then
          LnSum=NextLogicNumber()
          call OpenFile(LnSum,fln(:ifln)//'_Fourier.l70','formatted',
     1                  'unknown')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,'(''# Fourier'')')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,FormA)
          do i=1,2
            if(i.eq.1) then
              pom=rmax
            else
              pom=rmin
            endif
            write(Cislo,'(f8.2)') pom
            call Zhusti(Cislo)
            write(LnSum,FormCIF) CifKey(i,15),Cislo
          enddo
          call CloseIfOpened(LnSum)
        endif
        if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      endif
      call FeFlowChartRemove
9999  call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
      if(allocated(Table)) deallocate(Table)
      if(allocated(TableP)) deallocate(TableP)
      call CloseIfOpened(81)
      return
      end
