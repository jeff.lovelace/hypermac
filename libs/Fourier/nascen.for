      subroutine nascen(ias,bs,isw,n,m)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ias(3,*),bs(3,*),p(6)
      logical   eqiv,eqrv
      data p/6*0./
      m=n+1
      do i=2,NLattVec(KPhase)
        do 2000j=1,n
          do k=1,3
            ias(k,m)=ias(k,j)
            p(k)=bs(k,j)+vt6(k,i,isw,KPhase)
          enddo
          call od0do1(p,p,3)
          do k=1,m-1
            if(eqiv(ias(1,m),ias(1,k),3).and.
     1         eqrv(p,bs(1,k),3,.0001)) go to 2000
          enddo
          do k=1,3
            bs(k,m)=p(k)
          enddo
          m=m+1
2000    continue
      enddo
      m=m-1
      return
      end
