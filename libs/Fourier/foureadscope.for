      subroutine FouReadScope
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      dimension ip(6),prefr(3),xp(3)
      character*256 EdwStringQuest,t256
      character*80 Veta
      character*7 Label(3)
      character*2 nty
      integer EdwStateQuest,CrwStateQuest
      logical CrwLogicQuest
      logical, allocatable :: AtBrat(:)
      save nCrwAuto,nCrwExpl,nCrwPoint,nCrwDefaultOrient,nButtReset,
     1     nCrwOrientFirst,nCrwOrientLast,nCrwIndependent,
     2     nCrwRefreshScope,nLblMapTitle,nLblMapFirst,nLblMapLast,
     3     nEdwIntervalFirst,nEdwCenter,nEdwScope,nEdwStep,ip,
     4     nCrwWholeCell,nButtAtom
      data Label/'minimum','maximum','step'/
      entry FouReadScopeMake(id)
      if(NacetlInt(nCmdOrCode).lt.0) then
        call SetIntArrayTo(ip,6,-1)
      else
        write(Veta,101) NacetlInt(nCmdOrCode)
        call zhusti(Veta)
        read(Veta,100) ip
      endif
      call FouMakeScopeType
      xpom=XdQuestFou*.166667
      il=1
      ichk=1
      do i=1,3
        if(i.eq.1) then
          Veta='%automatically'
        else if(i.eq.2) then
          Veta='%explicitly'
        else
          Veta='by a central %point'
        endif
        call FeQuestCrwMake(id,xpom,il,xpom-CrwgXd*.5,il+1,Veta,'C',
     1                      CrwgXd,CrwgYd,1,ichk)
        if(i.eq.1) then
          nCrwAuto=CrwLastMade
        else if(i.eq.1) then
          nCrwExpl=CrwLastMade
        else if(i.eq.3) then
          nCrwPoint=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.ScopeType)
        xpom=xpom+XdQuestFou*.333333
      enddo
      il=il+2
      xpomm=18.
      Veta='Use %default map orientation'
      tpom=xpomm+CrwXd+3.
      call FeQuestCrwMake(id,tpom,il,xpomm,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwDefaultOrient=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ip(1).lt.0)
      il=il+1
      Veta='Map axes:'
      Veta=Veta(:idel(Veta)+2)//
     1     ' 1st=horizontal, 2nd=vertical, 3rd=section, ...'
      call FeQuestLblMake(id,xpomm,il,Veta,'L','B')
      nLblMapTitle=LblLastMade
      xpomp=XdQuestFou-230.
      il=il+1
      ilp=il
      dpom=50.
      do i=1,NDim(KPhase)
        xpom=5.
        il=il+1
        if(NDim(KPhase).eq.3) then
          Veta=smbx(i)
        else
          Veta=smbx6(i)
        endif
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        xpom=xpomp
        do j=1,3
          if(i.eq.1)
     1      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Label(j),'C','N')
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwIntervalFirst=EdwLastMade
          xpom=xpom+dpom+10.
        enddo
      enddo
      xpom=30.
      do i=1,NDim(KPhase)
        ichk=ichk+1
        il=ilp
        write(Veta,'(i1,a2)') i,nty(i)
        call FeQuestLblMake(id,xpom+10.,il,Veta,'C','N')
        if(i.eq.1) nLblMapFirst=LblLastMade
        do j=1,NDim(KPhase)
          il=il+1
          call FeQuestCrwMake(id,xpom,il,xpom,il,' ','C',CrwgXd,
     1                        CrwgYd,1,ichk)
          if(i.eq.1.and.j.eq.1) nCrwOrientFirst=CrwLastMade
        enddo
        xpom=xpom+30.
      enddo
      nLblMapLast=LblLastMade
      nCrwOrientLast=CrwLastMade
      il=il+1
      tpom=5.
      Veta='%Center'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwCenter=EdwLastMade
      Veta='Sc%ope [A]'
      xpom=xpomp
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwScope=EdwLastMade
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='%Independent parallelepiped'
      ichk=ichk+1
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      ichk+1)
        if(i.eq.1) then
          nCrwIndependent=CrwLastMade
          Veta='%Whole cell'
        else
          nCrwWholeCell=CrwLastMade
        endif
      enddo
      il=il-1
      xpom=90.
      Veta='Select ato%m'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAtom=ButtonLastMade
      tpom=120.
      xpom=xpomp
      Veta='%Step [A]'
      tpom=xpom-FeTxLengthUnder(Veta)-10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwStep=EdwLastMade
      xpom=xpomp
      il=il+1
      Veta='%Refresh scope'
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                    0,0)
      nCrwRefreshScope=CrwLastMade
      il=il+1
      Veta='Reset to de%fault'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(XdQuestFou-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtReset=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1200  nEdw=nEdwIntervalFirst
      do i=1,NDim(KPhase)
        if(xrmn(i).lt.-330.) then
          if(i.le.3) then
            if(CellPar(i,1,KPhase).gt.20.) then
              pomd=0.01
            else if(CellPar(i,1,KPhase).gt.10.) then
              pomd=0.02
            else
              pomd=0.05
            endif
          else
            pomd=0.1
          endif
        else
          pomd=dd(i)
        endif
        do j=1,3
          if((ScopeType.eq.ScopeExplicit.or.i.gt.3).and.
     1       EdwStateQuest(nEdw).ne.EdwOpened) then
            if(j.eq.1) then
              if(xrmn(i).lt.-330.) then
                pom=0.
              else
                pom=xrmn(i)
              endif
            else if(j.eq.2) then
              if(xrmn(i).lt.-330.) then
                pom=1.-pomd
              else
                pom=xrmx(i)
              endif
            else
              pom=pomd
            endif
            call FeQuestCrwClose(nCrwRefreshScope)
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          else if(ScopeType.ne.ScopeExplicit.and.
     1            EdwStateQuest(nEdw).eq.EdwOpened.and.i.le.3) then
            if(j.eq.1) xrmn(i)=-333.
            call FeQuestEdwClose(nEdw)
          endif
          nEdw=nEdw+1
        enddo
      enddo
      if(ScopeType.eq.ScopeCenter.and.
     1   EdwStateQuest(nEdwCenter).ne.EdwOpened) then
        if(ptname.eq.'[nic]') then
          Veta=' '
        else
          Veta=ptname
        endif
        call FeQuestStringEdwOpen(nEdwCenter,Veta)
        call FeQuestRealAEdwOpen(nEdwScope,pts,3,.false.,.false.)
      else if(ScopeType.ne.ScopeCenter.and.
     1        EdwStateQuest(nEdwCenter).eq.EdwOpened) then
        ptname='[nic]'
        call SetRealArrayTo(ptx,3,0.)
        call SetRealArrayTo(pts,3,.5)
        call FeQuestEdwClose(nEdwCenter)
        call FeQuestEdwClose(nEdwScope)
        call FeQuestCrwClose(nCrwRefreshScope)
        call FeQuestButtonClose(nButtAtom)
      endif
      if((ScopeType.eq.ScopeAuto.or.ScopeType.eq.ScopeCenter).and.
     1   EdwStateQuest(nEdwStep).ne.EdwOpened) then
        call FeQuestRealEdwOpen(nEdwStep,NacetlReal(nCmdptstep),.false.,
     1                          .false.)
      else if(ScopeType.eq.ScopeExplicit) then
        NacetlReal(nCmdptstep)=DefaultReal(nCmdptstep)
        call FeQuestEdwClose(nEdwStep)
      endif
1300  nCrw=nCrwOrientFirst-1
      do i=1,NDim(KPhase)
        do 1320j=1,NDim(KPhase)
          nCrw=nCrw+1
          do k=1,i-1
            if(ip(k).eq.j) then
              call FeQuestCrwClose(nCrw)
              go to 1320
            endif
          enddo
          if(ip(1).gt.0) call FeQuestCrwOpen(nCrw,j.eq.ip(i))
1320    continue
      enddo
      if(ScopeType.eq.ScopeCenter.and.ip(1).le.3.and.ip(2).gt.3) then
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        if(CrwStateQuest(nCrwRefreshScope).eq.CrwClosed) then
          call FeQuestCrwOpen(nCrwRefreshScope,.true.)
          call FeQuestButtonOpen(nButtAtom,ButtonOff)
        endif
      else
        call FeQuestCrwClose(nCrwRefreshScope)
        call FeQuestButtonClose(nButtAtom)
        if(ScopeType.eq.ScopeAuto) then
          call FeQuestCrwOpen(nCrwIndependent,
     1                        NacetlInt(nCmdIndUnit).eq.1)
          call FeQuestCrwOpen(nCrwWholeCell,
     1                        NacetlInt(nCmdIndUnit).ne.1)
        else
          NacetlInt(nCmdIndUnit)=DefaultInt(nCmdIndUnit)
          call FeQuestCrwClose(nCrwIndependent)
          call FeQuestCrwClose(nCrwWholeCell)
        endif
      endif
      go to 9999
      entry FouReadScopeCheck
1500  if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwAuto.and.CheckNumber.le.nCrwPoint) then
        ScopeType=CheckNumber-nCrwAuto+1
        go to 1600
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwDefaultOrient) then
        if(CrwLogicQuest(nCrwDefaultOrient)) then
          do i=nCrwOrientFirst,nCrwOrientLast
            call FeQuestCrwClose(i)
          enddo
          ip(1)=-1
        else
          do i=1,NDim(KPhase)
            ip(i)=i
          enddo
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwOrientFirst.and.
     2        CheckNumber.le.nCrwOrientLast) then
        irefr=ip(1)
        j=CheckNumber-nCrwOrientFirst
        i=j/NDim(KPhase)+1
        j=mod(j,NDim(KPhase))+1
        ipp=ip(i)
        ip(i)=j
        do k=i+1,NDim(KPhase)
          if(ip(k).eq.j) then
            ip(k)=ipp
            go to 1512
          endif
        enddo
1512    if(CrwLogicQuest(nCrwRefreshScope)) then
          call FeQuestRealAFromEdw(nEdwScope,prefr)
          if(ip(1).ne.irefr.and.ip(1).le.3) then
            i=mod(irefr,3)+1
            j=6-i-irefr
            pom=prefr(irefr)
            prefr(irefr)=prefr(ip(1))
            prefr(ip(1))=pom
            call FeQuestRealAEdwOpen(nEdwScope,prefr,3,.false.,.false.)
          endif
        endif
        go to 1300
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCenter) then
        if(EventTypeSave.eq.EventCrw.and.
     1     (EventNumberSave.eq.nCrwAuto.or.
     2      EventNumberSave.eq.nCrwExpl)) then
          go to 9999
        endif
        t256=EdwStringQuest(nEdwCenter)
        k=0
        call StToReal(t256,k,xp,3,.false.,ich)
        if(ich.ne.0) then
          call TestAtomString(t256,IdWildNo,IdAtMolYes,IdMolNo,
     1                        IdSymmYes,IdAtMolMixNo,Veta)
          if(Veta.ne.' ') then
            if(Veta.ne.'Jiz oznameno')
     1        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            go to 1520
          endif
        endif
        go to 9999
1520    EventType=EventEdw
        EventNumber=nEdwCenter
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtom)
     1  then
        nap=NAtIndFrAll(KPhase)
        nak=NAtPosToAll(KPhase)
        call SelOneAtom('Select atom as a central point',Atom(nap),ia,
     1                   nak-nap+1,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwCenter,Atom(ia))
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        go to 1600
      endif
      go to 9999
1600  call FeQuestEdwClose(nEdwStep)
      if(ScopeType.eq.ScopeAuto) then
        NacetlReal(nCmdptstep)=.1
         pom=.9
        dpom=.1
        call FeQuestCrwOn(nCrwDefaultOrient)
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
        ip(1)=-1
      else if(ScopeType.eq.ScopeExplicit) then
        call FeQuestCrwOn(nCrwDefaultOrient)
         pom=.9
        dpom=.1
        ip(1)=-1
      else if(ScopeType.eq.ScopeCenter) then
        NacetlReal(nCmdptstep)=.02
         pom=2.
        dpom=.02
        call FeQuestCrwOff(nCrwDefaultOrient)
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
        call FeQuestCrwClose(nCrwIndependent)
        call FeQuestCrwClose(nCrwWholeCell)
        if(NDimI(KPhase).gt.0) then
          ip(1)=1
          ip(2)=4
          j=1
          do i=3,NDim(KPhase)
            j=j+1
            if(j.eq.4) j=j+1
            ip(i)=j
          enddo
        else
          do i=1,3
            ip(i)=i
          enddo
        endif
        call FeQuestCrwOpen(nCrwRefreshScope,.true.)
        pts(1)=2.
        pts(2)=0.
        pts(3)=0.
        call FeQuestButtonOpen(nButtAtom,ButtonOff)
      endif
1620  if(ip(1).lt.0) then
        do i=nCrwOrientFirst,nCrwOrientLast
          call FeQuestCrwClose(i)
        enddo
      endif
      nEdw=nEdwIntervalFirst+9
      do i=4,NDim(KPhase)
        xrmn(i)=0.0
        xrmx(i)=pom
        dd(i)  =dpom
        do j=1,3
          call FeQuestEdwClose(nEdw)
          nEdw=nEdw+1
        enddo
      enddo
      go to 1200
      entry FouReadScopeUpdate
      if(CrwLogicQuest(nCrwIndependent).and.ScopeType.eq.ScopeAuto) then
        NacetlInt(nCmdIndUnit)=1
      else
        NacetlInt(nCmdIndUnit)=0
      endif
      if(ScopeType.eq.ScopeAuto.or.ScopeType.eq.ScopeCenter) then
        i1=4
      else
        i1=1
      endif
      nEdw=nEdwIntervalFirst+(i1-1)*3
      do i=i1,NDim(KPhase)
        do j=1,3
          call FeQuestRealFromEdw(nEdw,pom)
          if(j.eq.1) then
            xrmn(i)=pom
          else if(j.eq.2) then
            xrmx(i)=pom
          else
            dd(i)=pom
          endif
          nEdw=nEdw+1
        enddo
      enddo
      if(ScopeType.eq.ScopeCenter) then
        ptname=EdwStringQuest(nEdwCenter)
        call FeQuestRealAFromEdw(nEdwScope,pts)
      else
        ptname='[nic]'
      endif
      if(ScopeType.ne.ScopeExplicit)
     1  call FeQuestRealFromEdw(nEdwStep,NacetlReal(nCmdptstep))
      if(ip(1).le.0) then
        j=-333
      else
        j=ip(1)
        do i=2,NDim(KPhase)
          j=j*10+ip(i)
        enddo
      endif
      NacetlInt(nCmdOrCode)=j
9999  return
100   format(6i1)
101   format(i6)
      end
