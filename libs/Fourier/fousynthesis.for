      subroutine FouSynthesis
      use Basic_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*80 t80
      dimension xp(6),ix(6),ihp(6),Table(:),TableA(:),mxi(6),mni(6),
     1          mdi(6)
      integer SmallStep,SmallStep2,Delta,ss
      logical AlreadyAllocated
      allocatable Table,TableA
      call OpenMaps(81,fln(:ifln)//'.m81',nxny,1)
      if(ErrFlag.ne.0) go to 9999
      rmax=0.
      rmin=0.
      write(81,rec=1) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,iorien,
     1                mapa,nsubs+(KPhase-1)*10,MameSatelity,
     2                nonModulated(KPhase)
      irec=1
      nn=0
      nd=NDimSum
      fn=25000000.
      do i=1,nd
        fn=fn/float(nx(i))
        if(fn.lt.1.) then
          nd=i-1
          go to 1060
        endif
      enddo
1060  ncykl=1
      do i=nd+1,6
        ncykl=ncykl*nx(i)
      enddo
      mxc=1
      do i=1,nd
        mni(i)=1
        if(FMethod.eq.FFTMethod) then
          mxi(i)=nx(i)
        else
          mxi(i)=max(nx(i),hmax(i)-hmin(i)+1)
        endif
        mdi(i)=mxi(i)-mni(i)+1
        mxc=mxc*mdi(i)
      enddo
      allocate(Table(2*mxc))
      NMaxFlow=0
      do i=nd,1,-1
        n=1
        do j=1,i
          if(FMethod.eq.FFTMethod) then
            n=n*mdi(j)
          else
            n=n*(hmax(j)-hmin(j)+1)
          endif
        enddo
        do j=i+1,nd
          n=n*mdi(j)
        enddo
        NMaxFlow=NMaxFlow+n
      enddo
      NMaxFlow=NMaxFlow*ncykl
      call FeFlowChartOpen(-1.,YMinFlowChart,nint(float(NMaxFlow)*.005),
     1                     NMaxFlow,'Fourier summation',' ',' ')
      n=0
      do i=1,nd
        n=max(n,mdi(i))
      enddo
      allocate(TableA(2*n))
      do it=1,ncykl
        call RecUnpack(it,ix,nx(nd+1),6-nd)
        do i=1,6-nd
          j=i+nd
          xp(j)=xfmn(j)+(ix(i)-1)*xdf(j)
        enddo
        call SetRealArrayTo(Table,2*mxc,0.)
1200    read(82,end=1240) ihp,ai,bi
        indp=0
        do j=nd,1,-1
          indp=indp*mdi(j)+ihp(j)-hmin(j)
        enddo
        locc=2*indp+1
        locd=locc+1
        if(nd.ne.NDim(KPhase)) then
          xlz=0.
          do j=nd+1,NDim(KPhase)
            xlz=xlz+float(ihp(j))*xp(j)
          enddo
          xlz=pi2*xlz
          sinlz=sin(xlz)
          coslz=cos(xlz)
          Table(locc)=Table(locc)+ai*coslz+bi*sinlz
          Table(locd)=Table(locd)-ai*sinlz+bi*coslz
        else
          Table(locc)=ai
          Table(locd)=bi
        endif
        go to 1200
1240    rewind 82
        do nh=nd,1,-1
          LargeStep=1.
          SmallStep=1.
          do i=1,nh-1
            SmallStep=SmallStep*mdi(i)
          enddo
          do i=nh+1,nd
            LargeStep=LargeStep*mdi(i)
          enddo
          SmallStep2=2*SmallStep
          mdinh=mdi(nh)
          mdinh2=2*mdinh
          nxnh=nx(nh)
          Delta=SmallStep*mdinh
          if(FMethod.eq.FFTMethod) then
            ms=1
            mk=mdinh2
            nxsum=mdinh
          else
            if(nxnh.ge.mdi(nh)) then
              ms=1
              mk=2*(hmax(nh)-hmin(nh)+1)
              nxsum=mdinh
            else
              ms=1
              mk=mdinh2
              nxsum=nxnh
            endif
          endif
          ksp=0
          do ls=1,LargeStep
            ks=ksp
            call SetIntArrayTo(ix,nh-1,1)
            do ss=1,SmallStep
              ks=ks+1
              if(FMethod.eq.BLMethod) then
                do i=1,nh-1
                  if(ix(i).gt.hmax(i)-hmin(i)+1) go to 1610
                enddo
                call SetRealArrayTo(TableA,mdinh2,0.)
                ih=0
              endif
              k=2*ks-1
              do m=ms,mk,2
                if(FMethod.eq.BLMethod) ih=ih+1
                call FeFlowChartEvent(nn,ie)
                if(ie.ne.0) then
                  call FouBreak
                  if(ErrFlag.ne.0) go to 9999
                endif
                chy=Table(k  )
                dhy=Table(k+1)
                if(FMethod.eq.FFTMethod) then
                  TableA(m  )=chy
                  TableA(m+1)=dhy
                else
                  if(chy.eq.0..and.dhy.eq.0.) go to 1450
                  loc1=-1
                  do i=1,nxsum
                    loc1=loc1+2
                    loc2=loc1+1
                    sinxh=SinTable(ih,i,nh)
                    cosxh=CosTable(ih,i,nh)
                    TableA(loc1)=TableA(loc1)+chy*cosxh+dhy*sinxh
                    TableA(loc2)=TableA(loc2)-chy*sinxh+dhy*cosxh
                  enddo
                endif
1450            k=k+SmallStep2
              enddo
              if(FMethod.eq.FFTMethod) then
                call four1(TableA,mdi(nh),-1)
                m=1
                do i=1,mdi(nh)
                  if(mod(i-1,2).eq.1) then
                    TableA(m  )=-TableA(m  )
                    TableA(m+1)=-TableA(m+1)
                  endif
                  m=m+2
                enddo
              endif
              k=2*ks-1
              do m=1,mdinh2,2
                Table(k  )=TableA(m  )
                Table(k+1)=TableA(m+1)
                k=k+SmallStep2
              enddo
1610          if(FMethod.eq.BLMethod) then
                do i=1,nh-1
                  ix(i)=ix(i)+1
                  if(ix(i).gt.mxi(i)) then
                    ix(i)=1
                  else
                    exit
                  endif
                enddo
              endif
            enddo
            ksp=ksp+Delta
          enddo
        enddo
        k=0
        m=-1
        do 2900i=1,mxc
          m=m+2
          call RecUnpack(i,ix,mdi,nd)
          do j=1,nd
            if(ix(j).gt.nx(j)) go to 2900
          enddo
          k=k+1
          Table(k)=Table(m)
2900    continue
        do i=1,k
          rmax=max(rmax,Table(i))
          rmin=min(rmin,Table(i))
        enddo
        IZdih=0
        do i=1,k/nxny
          irec=irec+1
          write(81,rec=irec)(table(IZdih+j),j=1,nxny)
          IZdih=IZdih+nxny
        enddo
      enddo
8000  irec=irec+1
      write(81,rec=irec) rmax,rmin
      if(Mapa.ne.10) then
        call newln(2)
        write(lst,FormA)
        write(Cislo,'(f15.2)') rmax
        call ZdrcniCisla(Cislo,1)
        t80='Maximal density : '//Cislo(:idel(Cislo))
        write(Cislo,'(f15.2)') rmin
        call ZdrcniCisla(Cislo,1)
        t80=t80(:idel(t80))//', minimal density : '//Cislo(:idel(Cislo))
        write(lst,FormA) t80(:idel(t80))
      endif
      if(Mapa.eq.6) then
        if(allocated(CifKey)) then
          AlreadyAllocated=.true.
        else
          AlreadyAllocated=.false.
          allocate(CifKey(400,40),CifKeyFlag(400,40))
        endif
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.eq.0) then
          LnSum=NextLogicNumber()
          call OpenFile(LnSum,fln(:ifln)//'_Fourier.l70','formatted',
     1                  'unknown')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,'(''# Fourier'')')
          write(LnSum,'(''#'',71(''=''))')
          write(LnSum,FormA)
          do i=1,2
            if(i.eq.1) then
              pom=rmax
            else
              pom=rmin
            endif
            write(Cislo,'(f8.2)') pom
            call Zhusti(Cislo)
            write(LnSum,FormCIF) CifKey(i,15),Cislo
          enddo
          call CloseIfOpened(LnSum)
        endif
        if(allocated(CifKey).and..not.AlreadyAllocated)
     1     deallocate(CifKey,CifKeyFlag)
      endif
      call FeFlowChartRemove
9999  call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
      if(allocated(Table)) deallocate(Table)
      if(allocated(TableA)) deallocate(TableA)
      close(81)
      return
      end
