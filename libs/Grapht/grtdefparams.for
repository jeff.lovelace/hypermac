      subroutine GrtDefParams(ich)
      use Atoms_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension xsym(3),x4dif(3),tdif(3),gxo(3,3),gyo(2)
      character*256 EdwStringQuest,Veta,GrtStPom(3)
      character*80  Header,HeaderOld,t80
      character*3 :: smbm(4) = (/'Mx ','My ','Mz ','|M|'/)
      character*2   nty
      integer ColorOrder,WhatToDrawOld,EdwStateQuest,CrwStateQuest,
     1        RolMenuSelectedQuest,PocetParTypeAct
      logical CrwLogicQuest,BratAtom(:)
      allocatable BratAtom
      data icolor/-1/
      if(allocated(BratAtom)) deallocate(BratAtom)
      allocate(BratAtom(NAtCalc))
      BratAtom=.false.
      call GrtSaveDrawAtom
      HardCopy=0
      WhatToDrawOld=-1
      NParOld=-1
      NAtomsOld=0
      id=NextQuestId()
      xdq=520.
      xdqp=xdq*.5
      if(NDimI(KPhase).gt.1) then
        il=14+NDimI(KPhase)
      else
        il=14
      endif
      call FeQuestCreate(id,-1.,-1.,xdq,il,' ',1,LightGray,0,0)
      il=1
      Veta='%Parameter to draw'
      tpom=120.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=0.
      if(MagneticType(KPhase).gt.0) then
        PocetParTypeAct=PocetParType
      else
        PocetParTypeAct=PocetParType-1
      endif
      do i=1,PocetParTypeAct
        dpom=max(dpom,FeTxLength(MenuParType(i)))
      enddo
      dpom=dpom+EdwYd+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      xpom1=xpom+dpom+5.
      nRolMenuParType=RolMenuLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,xdqp,il,' ','C','B')
      nLblType=LblLastMade
      HeaderOld=' '
      dpom=60.
      xpom=xdqp-dpom-90.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'P%revious')
      nButtPrevious=ButtonLastMade
      xpom=xdqp+90.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Ne%xt')
      nButtNext=ButtonLastMade
      nCrwGroup=0
      if(NDimI(KPhase).gt.1) then
        nCrwGroup=nCrwGroup+1
        il=il+1
        tpom=5.
        Veta='Type of graph :'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblTypeGr=LblLastMade
        call FeQuestLblOn(nLblTypeGr)
        tpom=FeTxLengthUnder(Veta)+20.
        do i=0,1
          if(i.eq.0) then
            Veta='1d %graph'
            dpom=FeTxLengthUnder(Veta)+10.
          else
            Veta='2d %map'
          endif
          xpom=tpom+dpom
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,nCrwGroup)
          call FeQuestCrwOpen(CrwLastMade,WhatToDraw.eq.i)
          if(i.eq.0) nCrwTypeFirst=CrwLastMade
          tpom=xpom+30.
        enddo
        nCrwTypeLast=CrwLastMade
      else
        nCrwTypeFirst=0
        nCrwTypeLast=0
        WhatToDraw=0
      endif
      il=il+1
      dpom=50.
      posun=dpom+20.
      tpom=5.
      xpom=tpom+FeTxLengthUnder('XX')+5.+dpom*.5
      do i=1,3
        if(i.eq.1) then
          Veta='Minimum'
        else if(i.eq.2) then
          Veta='Maximum'
        else if(i.eq.3) then
          Veta='Step'
        endif
        call FeQuestLblMake(id,xpom,il,Veta,'C','N')
        call FeQuestLblOn(LblLastMade)
        xpom=xpom+posun
      enddo
      call CopyVek(gx,gxo,9)
      ilp=il
      do i=1,NDimI(KPhase)+1
        il=il+1
        tpom=5.
        xpom=tpom+FeTxLengthUnder('XX')+10.
        if(i.le.NDimI(KPhase)) then
          k=3
          if(GrtDrawX4) then
            Veta=smbx6(i+3)
          else
            Veta=smbt(i)
          endif
        else
          k=2
          Veta='p'
        endif
        do j=1,3
          if(j.le.k) then
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1.and.j.eq.1) then
              nEdwTLim=EdwLastMade
            else if(i.eq.NDimI(KPhase)+1.and.j.eq.1) then
              nEdwPLim=EdwLastMade
            endif
            if(i.le.NDimI(KPhase)) then
              pom=gx(j,i)
            else
              pom=gy(j)
            endif
            call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
          endif
          tpom=tpom+posun
          xpom=xpom+posun
          Veta=' '
        enddo
      enddo
      if(NDimI(KPhase).gt.1) then
        xpom=xpom+5.
        do i=1,NDimI(KPhase)
          nCrwGroup=nCrwGroup+1
          il=ilp
          write(Veta,'(i1,a2)') i,nty(i)
          call FeQuestLblMake(id,xpom+10.,il,Veta,'C','N')
          call FeQuestLblOn(LblLastMade)
          do j=1,NDimI(KPhase)
            il=il+1
            call FeQuestCrwMake(id,xpom,il,xpom,il,' ','C',CrwgXd,
     1                          CrwgYd,1,nCrwGroup)
            if(i.eq.1.and.j.eq.1) nCrwOrientFirst=CrwLastMade
          enddo
          xpom=xpom+25.
        enddo
        nCrwOrientLast=CrwLastMade
        il=il+1
        Veta='%Reference level explicitly'
        xpom=8.
        tpom=xpom+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwRefLevel=CrwLastMade
        tpom=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwRefLevel=EdwLastMade
      else
        nCrwOrientFirst=0
        nCrwOrientLast=0
        nCrwRefLevel=0
        nEdwRefLevel=0
      endif
      xpom=xdq*.5+90.
      tpom=xpom+CrwXd+5.
      Veta='%Use x4,x5,x6 instead of t,u,v'
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,
     1                    1,0)
      nCrwDrawX4=CrwLastMade
      dpom=0.
      do i=1,PocetBarev
        dpom=max(dpom,FeTxLength(ColorNames(i)))
      enddo
      dpom=dpom+EdwYd+10.
      Veta='%Color'
      tpom=xpom+dpom+10.
      call FeQuestRolMenuMake(id,tpom,ilp+1,xpom,ilp+1,Veta,'L',dpom,
     1                        EdwYd,1)
      nRolMenuColor=RolMenuLastMade
      Veta='Draw n%on-modulated curves'
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwNonMod=CrwLastMade
      il=il+1
      nCrwGroup=nCrwGroup+1
      xpom=5.
      Veta='Diagonal:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblDiagonal=LblLastMade
      xpomp=xpom+FeTxLength(Veta)+10.
      xpom=xpomp
      Veta=' '
      dpom=70.
      do i=1,4
        tpom=xpom+CrwgXd+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      nCrwGroup)
        if(i.eq.1) nCrwParFirst=CrwLastMade
        xpom=xpom+dpom
      enddo
      il=il+1
      xpom=5.
      Veta='Principal:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblPrincipal=LblLastMade
      xpom=xpomp
      Veta=' '
      do i=1,3
        tpom=xpom+CrwgXd+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      nCrwGroup)
        xpom=xpom+dpom
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      do i=1,4
        il=il+1
        write(Veta,'(''%'',i1,a2,'' atom'')') i,nty(i)
        tpom=5.
        if(i.eq.1) xpoma=tpom+FeTxLengthUnder(Veta)+55.
        xpom=xpoma
        dpom=150.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        if(i.eq.1) nEdwName=EdwLastMade
        xpom=xpom+dpom+5.
        Veta='List'
        dpomp=FeTxLengthUnder(Veta)+20.
        xpom=xpom+10.
        call FeQuestButtonMake(id,xpom,il,dpomp,ButYd,Veta)
        if(i.eq.1) then
          nButtAtomList=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          ilp=il
        endif
      enddo
      il=il-3
      Veta='Pl%ane #1'
      xpom=tpom+100.
      dpom=320.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane1=EdwLastMade
      xpomp=xpom+dpom+10.
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane1=ButtonLastMade
      il=il+1
      Veta='Plane #2/directi%on'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane2=EdwLastMade
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane2=ButtonLastMade
      il=il+1
      Veta='Plane #3/directi%on'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane3=EdwLastMade
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane3=ButtonLastMade
      il=il+1
      Veta='D%efine neighbour atoms'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xdq-dpom-5.
      call FeQuestButtonMake(id,xpom,ilp,dpom,ButYd,Veta)
      nButtNeighbours=ButtonLastMade
      Veta='De%fine bond valence coefficients'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xdq-dpom-5.
      ilp=ilp+1
      call FeQuestButtonMake(id,xpom,ilp,dpom,ButYd,Veta)
      nButtBondVal=ButtonLastMade
      tpom=5.
      Veta='Ma%ximal distance'
      dpom=80.
      call FeQuestEdwMake(id,tpom,ilp,xpoma,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
      ilp=ilp+1
      call FeQuestEdwMake(id,tpom,ilp,xpoma,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwDMaxIndDist=EdwLastMade
      il=il+1
      dpom=80.
      xpom=xdqp-20.-dpom
      Veta='Ne%w item'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtNew=ButtonLastMade
      xpom=xdqp+20.
      Veta='%Delete item'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      ia=1
      iao=0
      if(DrawN.gt.0) go to 1220
      call ReallocateGrtDraw(10)
      DrawN=1
1200  IColor=ColorOrder('White')
      DrawColor(ia)=IColor
      if(ia.eq.1) then
        call SetStringArrayTo(DrawAtom(1,ia),4,' ')
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
        DrawParam(ia)=31
      else
        DrawParam(ia)=NPar*10+1
        DrawAtom(1:4,ia)=DrawAtom(1:4,ia-1)
        DrawSymSt(ia)=DrawSymSt(ia-1)
        DrawSymStU(ia)=DrawSymStU(ia-1)
      endif
1220  NPar=DrawParam(ia)/10
      IPar=max(mod(DrawParam(ia),10),1)
      if(NPar.eq.IdDrawTorsion) then
        NAtoms=4
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        NAtoms=2
      else if(NPar.eq.IdDrawPlane) then
        NAtoms=-1
      else
        NAtoms=1
      endif
      if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1   NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawADP.or.
     2   NPar.eq.IdMagMoment) then
        call FeQuestCrwOpen(nCrwDrawX4,GrtDrawX4)
      else
        call FeQuestCrwClose(nCrwDrawX4)
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdDrawVal.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        if(WhatToDraw.ne.WhatToDrawOld) then
          if(NDimI(KPhase).gt.1) then
            call FeQuestEdwClose(nEdwRefLevel)
            call FeQuestCrwClose(nCrwRefLevel)
          endif
          call FeQuestCrwClose(nCrwNonMod)
        endif
      else
        if(WhatToDraw.ne.WhatToDrawOld) then
          if((NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1        NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane).and.
     2        WhatToDraw.eq.0) then
            if(NDimI(KPhase).gt.1.and.NPar.ne.IdDrawVal) then
              call FeQuestCrwClose(nCrwRefLevel)
              call FeQuestEdwClose(nEdwRefLevel)
            endif
            if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane)
     1        call FeQuestCrwClose(nCrwNonMod)
          else
            call FeQuestCrwClose(nCrwNonMod)
            call FeQuestEdwClose(nEdwPLim)
            if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)
     1        call FeQuestRealFromEdw(nEdwPLim+1,DrawDMax)
            call FeQuestEdwClose(nEdwPLim+1)
          endif
        endif
      endif
      if(NPar.ne.NParOld) then
        if(NDimI(KPhase).gt.1) then
          tpom=5.
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     2       NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            nCrw=nCrwTypeFirst
            if(NPar.ne.IdDrawVal) then
              call FeQuestLblOff(nLblTypeGr)
              do i=0,1
                call FeQuestCrwClose(nCrw)
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        if(NAtoms.ne.NAtomsOld) then
          if(NAtomsOld.le.0.and.NAtoms.gt.0) then
            call FeQuestEdwClose(nEdwPlane1)
            call FeQuestEdwClose(nEdwPlane2)
            call FeQuestEdwClose(nEdwPlane3)
            call FeQuestButtonClose(nButtPlane1)
            call FeQuestButtonClose(nButtPlane2)
            call FeQuestButtonClose(nButtPlane3)
          endif
          if(NAtoms.lt.NAtomsOld) then
            nEdw=nEdwName
            if(NAtoms.gt.0) then
              nn=NAtoms+1
              nEdw=nEdw+NAtoms
            else
              nn=1
            endif
            nButt=nButtAtomList
            if(NAtoms.gt.0) nButt=nButt+NAtoms
            do i=nn,NAtomsOld
              call FeQuestButtonClose(nButt)
              nButt=nButt+1
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          endif
        endif
        if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawVal.and.
     1     NPar.ne.IdDrawAngles)
     2    call FeQuestButtonClose(nButtNeighbours)
      endif
      if(WhatToDraw.ne.0.or.
     1   (NPar.ne.IdDrawOcc.and.NPar.ne.IdDrawXYZ.and.
     2    NPar.ne.IdDrawDeltaXYZ.and.NPar.ne.IdDrawTorsion.and.
     3    NPar.ne.IdDrawADP.and.NPar.ne.IdMagMoment.and.
     4    NPar.ne.IdDrawIndDist.or.NPar.ne.IdDrawIndAng)) then
        call FeQuestButtonClose(nButtNext)
        call FeQuestButtonClose(nButtPrevious)
      endif
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else if(NPar.eq.IdDrawADP) then
        ik=7
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=ik+1,7
        if(i.le.ik) then

        else
          call FeQuestCrwClose(nCrw)
        endif
        nCrw=nCrw+1
      enddo
      if(ik.ne.7) then
        call FeQuestLblOff(nLblDiagonal)
        call FeQuestLblOff(nLblPrincipal)
      endif
      if((NParOld.eq.IdDrawVal.or.NParOld.eq.IdDrawAngles).and.
     1   NPar.ne.NParOld) then
        call FeQuestEdwClose(nEdwDMax)
        if(NParOld.eq.IdDrawVal) call FeQuestButtonClose(nButtBondVal)
      else if(NParOld.eq.IdDrawIndDist.and.NParOld.eq.IdDrawIndAng.and.
     1        NPar.ne.NParOld) then
        call FeQuestEdwClose(nEdwDMaxIndDist)
      endif
      if(NDimI(KPhase).gt.1) then
        nCrw=nCrwOrientFirst-1
        do i=1,NDimI(KPhase)
         do 1370j=1,NDimI(KPhase)
            nCrw=nCrw+1
            do k=1,i-1
              if(ior(k).eq.j) then
                call FeQuestCrwClose(nCrw)
                go to 1370
              endif
            enddo
1370      continue
        enddo
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        write(Header,'(i2,''/'',i2)') ia,DrawN
        call zhusti(Header)
        Header=Header(:idel(Header))//' item to be drawn'
        if(WhatToDraw.ne.WhatToDrawOld) then
          call FeQuestButtonOpen(nButtNew,ButtonOff)
          call FeQuestButtonOpen(nButtDelete,ButtonOff)
          call FeQuestRolMenuOpen(nRolMenuColor,ColorNames,PocetBarev,
     1                            ColorOrder('White'))
          if(nEdwPLim.gt.0) call GrtSetParLimits(nEdwPLim,NPar)
          call CopyVek(gy,gyo,2)
        endif
      else
          call FeQuestButtonClose(nButtNew)
          call FeQuestButtonClose(nButtDelete)
          call FeQuestRolMenuClose(nRolMenuColor)
        if(NPar.eq.IdDrawAngles) then
          Header='Bond angles related to the central atom'
        else if(NPar.eq.IdDrawDist) then
          Header='Distances related to the central atom'
        else if(NPar.eq.IdDrawVal) then
          Header='Bond valence sum for the central atom'
        else if(NPar.eq.IdDrawPlane) then
          Header='Angle between two planes or between plane and '//
     1           'selected direction'
        endif
        if(WhatToDraw.ne.WhatToDrawOld) then
          if((NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal).and.
     1        WhatToDraw.eq.0) then
            call FeQuestCrwOpen(nCrwNonMod,DrawNonMod)
          endif
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1       WhatToDraw.eq.0) then
            call GrtSetParLimits(nEdwPLim,NPar)
            call CopyVek(gy,gyo,2)
          else if(WhatToDraw.ne.0) then
            call FeQuestCrwOpen(nCrwRefLevel,UseRefLevel)
            if(UseReflevel)
     1        call FeQuestRealEdwOpen(nEdwRefLevel,RefLevel,.false.,
     2                                .false.)
          endif
        endif
      endif
      if(NPar.ne.NParOld) then
        if(NDimI(KPhase).gt.1) then
          tpom=5.
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     2       NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            if(NPar.ne.IdDrawVal) then
               call FeQuestLblOff(nLblTypeGr)
            endif
          endif
          if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles) then
            nCrw=nCrwTypeFirst
            call FeQuestLblOn(nLblTypeGr)
            do i=0,1
              call FeQuestCrwOpen(nCrw,WhatToDraw.eq.i)
              nCrw=nCrw+1
            enddo
          endif
        endif
        if(NAtoms.ne.NAtomsOld.or.NPar.eq.IdDrawIndDist.or.
     1     NPar.eq.IdDrawIndAng) then
          if(NAtoms.gt.NAtomsOld) then
            nEdw=nEdwName
            if(NAtomsOld.gt.0) then
              nn=NAtomsOld+1
              nEdw=nEdw+NAtomsOld
            else
              nn=1
            endif
            nButt=nButtAtomList
            if(NAtomsOld.gt.0) nButt=nButt+NAtomsOld
            do i=nn,NAtoms
              call FeQuestButtonOpen(nButt,ButtonOff)
              nButt=nButt+1
              call FeQuestStringEdwOpen(nEdw,' ')
              nEdw=nEdw+1
            enddo
          endif
          if(NAtoms.gt.0) then
            if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
              if(EdwStateQuest(nEdwName).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwName)
                call FeQuestButtonClose(nButtAtomList)
              endif
              if(EdwStateQuest(nEdwName+1).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwName+1)
                call FeQuestButtonClose(nButtAtomList+1)
              endif
              if(EdwStateQuest(nEdwPlane1).ne.EdwOpened) then
                call FeQuestStringEdwOpen(nEdwPlane1,DrawAtom(1,1))
                call FeQuestStringEdwOpen(nEdwPlane2,DrawAtom(2,1))
                if(NPar.eq.IdDrawIndAng)
     1             call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,1))
              else if(NParOld.eq.IdDrawIndDist.and.
     1                NPar.eq.IdDrawIndAng) then
                call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,1))
              else if(NParOld.eq.IdDrawIndAng.and.
     1                NPar.eq.IdDrawIndDist) then
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane3)
              endif
              call FeQuestEdwLabelChange(nEdwPlane1,'Center/Atom#1')
              call FeQuestEdwLabelChange(nEdwPlane2,'Center/Atom#2')
              if(NPar.eq.IdDrawIndAng)
     1          call FeQuestEdwLabelChange(nEdwPlane3,'Center/Atom#3')
              call FeQuestButtonOff(nButtPlane1)
              call FeQuestButtonOff(nButtPlane2)
              if(NPar.eq.IdDrawIndAng)
     1          call FeQuestButtonOff(nButtPlane3)
            else
              if(EdwStateQuest(nEdwPlane1).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwPlane1)
                call FeQuestEdwClose(nEdwPlane2)
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane1)
                call FeQuestButtonClose(nButtPlane2)
                call FeQuestButtonClose(nButtPlane3)
              endif
              if(EdwStateQuest(nEdwName).ne.EdwOpened) then
                call FeQuestStringEdwOpen(nEdwName,DrawAtom(1,1))
                call FeQuestButtonOff(nButtAtomList)
              endif
              if(NAtoms.le.2) then
                Veta='Central %atom'
              else
                Veta='%1st atom'
              endif
              call FeQuestEdwLabelChange(nEdwName,Veta)
              if(NAtoms.eq.2) then
                Veta='%Neighbor atom'
              else
                Veta='%2nd atom'
              endif
              call FeQuestEdwLabelChange(nEdwName+1,Veta)
            endif
          else
            if(NAtoms.eq.-1) then
              call FeQuestStringEdwOpen(nEdwPlane1,GrtPlane(1))
              call FeQuestStringEdwOpen(nEdwPlane2,GrtPlane(2))
              call FeQuestEdwLabelChange(nEdwPlane1,'Pl%ane #1')
              call FeQuestEdwLabelChange(nEdwPlane2,
     1                                   'Plane #2/directi%on')
              if(EdwStateQuest(nEdwPlane3).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane3)
              endif
            endif
            call FeQuestButtonOff(nButtPlane1)
            call FeQuestButtonOff(nButtPlane2)
          endif
        endif
        if(NAtoms.eq.1.and.NAtoms.ne.NAtomsOld)
     1    call FeQuestEdwLabelChange(nEdwName,'Central %atom')
        if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1     NPar.eq.IdDrawAngles) then
          call FeQuestButtonOff(nButtNeighbours)
        else
          call FeQuestButtonClose(nButtNeighbours)
        endif
      endif
      if(Header.ne.HeaderOld) then
        call FeQuestLblChange(nLblType,Header)
        HeaderOld=Header
      endif
      if(ia.gt.DrawN) then
        IColor=ColorOrder('White')
      else
        IColor=DrawColor(ia)
      endif
      if(ia.ne.iao) then
        if(NPar.ne.IdDrawIndDist.and.NPar.ne.IdDrawIndAng) then
          nEdw=nEdwName
          do i=1,NAtoms
            call FeQuestStringEdwOpen(nEdw,DrawAtom(i,ia))
            nEdw=nEdw+1
          enddo
        else
          call FeQuestStringEdwOpen(nEdwPlane1,DrawAtom(1,ia))
          call FeQuestStringEdwOpen(nEdwPlane2,DrawAtom(2,ia))
          if(NPar.eq.IdDrawIndAng)
     1      call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,ia))
        endif
        if(ia.gt.DrawN) then
          icolor=ColorOrder('White')
        else
          icolor=DrawColor(ia)
        endif
        NPar=DrawParam(ia)/10
        if(NPar.eq.IdDrawTorsion) then
          NAtoms=4
        else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
          NAtoms=2
        else if(NPar.eq.IdDrawPlane) then
          NAtoms=-1
        else
          NAtoms=1
        endif
        IPar=max(mod(DrawParam(ia),10),1)
        if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1     NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2     NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
          if(WhatToDraw.eq.0)
     1      call FeQuestRolMenuOpen(nRolMenuColor,ColorNames,PocetBarev,
     2                              icolor)
        endif
        call FeQuestRolMenuOpen(nRolMenuParType,MenuParType,
     1                          PocetParTypeAct,NPar)
      endif
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdDrawADP) then
        ik=7
        call FeQuestLblOn(nLblDiagonal)
        call FeQuestLblOn(nLblPrincipal)
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=1,7
        if(i.le.ik) then
          if(NPar.eq.IdDrawXYZ) then
            call FeQuestCrwLabelChange(nCrw,smbx(i))
          else if(NPar.eq.IdDrawDeltaXYZ) then
            call FeQuestCrwLabelChange(nCrw,'d'//smbx(i))
          else if(NPar.eq.IdDrawADP) then
            call FeQuestCrwLabelChange(nCrw,smbu(i))
          else if(NPar.eq.IdMagMoment) then
            call FeQuestCrwLabelChange(nCrw,smbm(i))
          endif
          call FeQuestCrwOpen(nCrw,i.eq.IPar)
        endif
        nCrw=nCrw+1
      enddo
      if((NPar.eq.IdDrawVal.or.NPar.eq.IdDrawAngles).and.
     1   NPar.ne.NParOld) then
        call FeQuestRealEdwOpen(nEdwDMax,DrawDMax,.false.,.false.)
        if(NPar.eq.IdDrawVal) call FeQuestButtonOff(nButtBondVal)
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndDist) then
        if(WhatToDraw.eq.0) then
          call FeQuestEdwClose(nEdwDMaxIndDist)
        else
          call FeQuestRealEdwOpen(nEdwDMaxIndDist,DrawDMax,.false.,
     1                            .false.)
        endif
      endif
      if(NDimI(KPhase).gt.1) then
        nCrw=nCrwOrientFirst-1
        do i=1,NDimI(KPhase)
         do 1380j=1,NDimI(KPhase)
            nCrw=nCrw+1
            do k=1,i-1
              if(ior(k).eq.j) go to 1380
            enddo
            if(ior(1).gt.0) call FeQuestCrwOpen(nCrw,j.eq.ior(i))
1380      continue
        enddo
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        if(ia.lt.DrawN) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(ia.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      else
        call FeQuestButtonClose(nButtNext)
        call FeQuestButtonClose(nButtPrevious)
      endif
      iao=ia
      WhatToDrawOld=WhatToDraw
      NParOld=NPar
      NAtomsOld=NAtoms
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndDist.or.
     1     NPar.eq.IdDrawIndAng) then
          Veta=' '
          GrtStPom(1)=EdwStringQuest(nEdwPlane1)
          if(GrtStPom(1).eq.' ') then
            if(NPar.eq.IdDrawPlane) then
              Veta='the Plane#1 not defined.'
            else
              Veta='the Atom/Center#1 not defined.'
            endif
          endif
          if(Veta.eq.' ') then
            GrtStPom(2)=EdwStringQuest(nEdwPlane2)
            if(GrtStPom(2).eq.' ') then
              if(NPar.eq.IdDrawPlane) then
                Veta='the Plane#2/Dirtection not defined.'
              else
                Veta='the Atom/Center#2 not defined.'
              endif
            endif
          endif
          if(NPar.eq.IdDrawIndAng.and.Veta.eq.' ') then
            GrtStPom(3)=EdwStringQuest(nEdwPlane3)
            if(GrtStPom(3).eq.' ')
     1        Veta='the Atom/Center#3 not defined.'
          endif
          if(Veta.ne.' ') then
            EventType=EventEdw
            call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
            if(GrtStPom(1).eq.' ') then
              EventNumber=nEdwPlane1
            else
              EventNumber=nEdwPlane2
            endif
            if(NPar.eq.IdDrawPlane) then
              GrtPlane(1:2)=GrtStPom(1:2)
            else
              DrawAtom(1:2,ia)=GrtStPom(1:2)
            endif
            go to 1500
          endif
        else
          nEdw=nEdwName
          do i=1,NAtoms
            if(EdwStringQuest(nEdw).eq.' ') then
              if(NAtoms.eq.1) then
                Veta='no atom defined, try again'
              else
                write(Veta,'(''Atom #'',i1,'' not defined, try again'')
     1                ') i
              endif
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1500
            endif
            nEdw=nEdw+1
          enddo
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles) then
            do i=1,NAtCalc
              if(BratDruhyATreti(i)) go to 1540
            enddo
            call FeChybne(-1.,YBottomMessage,'no neighbour atom '//
     1                    'defined',' ',SeriousError)
            go to 1500
          endif
        endif
1540    if(WhatToDraw.eq.0) then
          ik=NDimI(KPhase)+1
          RefLevel=0.
          UseRefLevel=.false.
        else
          ik=NDimI(KPhase)
          if(UseRefLevel)
     1      call FeQuestRealFromEdw(nEdwRefLevel,RefLevel)
        endif
        if(NPar.eq.IdDrawVal.or.NPar.eq.IdDrawAngles) then
          call FeQuestRealFromEdw(nEdwDMax,DrawDMax)
        else if(NPar.eq.IdDrawIndDist) then
          call FeQuestRealFromEdw(nEdwDMaxIndDist,DrawDMax)
        endif
        nEdw=nEdwTLim
        do i=1,ik
          if(i.le.NDimI(KPhase)) then
            k=3
          else
            k=2
          endif
          do j=1,k
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.le.NDimI(KPhase)) then
              gx(j,i)=pom
              if(j.eq.2) then
                if(gx(1,i).ge.gx(2,i)) then
                  Veta=smbt(i)//'(min)>='//smbt(i)//'(max), try again'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                  go to 1500
                endif
              endif
            else
              gy(j)=pom
              if(j.eq.2) then
                if(gy(1).ge.gy(2)) then
                  Veta='p(min)>=p(max), try again'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                  go to 1500
                endif
              endif
            endif
            nEdw=nEdw+1
          enddo
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwName.and.
     1        CheckNumber.le.nEdwName+NAtoms-1) then
        nEdw=CheckNumber
        ip=CheckNumber-nEdwName+1
        call GrtUpdateDrawAtom(ia)
        if(DrawAtom(ip,ia).ne.' '.and.
     1     (EventType.ne.EventButton.or.EventNumber.lt.nButtAtomList.or.
     2      EventNumber.gt.nButtAtomList+NAtoms-1))
     3   call atsymi(DrawAtom(ip,ia),i,xsym,x4dif,tdif,isym,k,*1560)
        go to 1500
1560    EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtAtomList
     1        .and.CheckNumber.le.nButtAtomList+NAtoms-1) then
        ip=CheckNumber-nButtAtomList+1
        nEdw=nEdwName+ip-1
        if(DrawAtom(ip,ia).eq.' ') then
          i=1
        else
          i=ktat(atom,NAtCalc,DrawAtom(ip,ia))
        endif
        call SelOneAtomFromPhase('Select atom from the list',Atom,kswa,
     1                           i,NAtCalc,ich)
        if(ich.eq.0) then
          call FeQuestStringEdwOpen(nEdw,Atom(i))
          call GrtUpdateDrawAtom(ia)
        endif
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwPlane1.or.CheckNumber.eq.nEdwPlane2
     2         .or.CheckNumber.eq.nEdwPlane3)) then
        ik=CheckNumber-nEdwPlane1+1
        Veta=EdwStringQuest(CheckNumber)
        if(Veta.eq.' ') go to 1500
        if(ik.eq.2.and.NPar.eq.IdDrawPlane) then
          do i=1,idel(Veta)
            if(Veta(i:i).eq.' ') cycle
            if(index(Cifry,Veta(i:i)).le.0) then
              GrtPlaneDir=.false.
              go to 1572
            endif
          enddo
          GrtPlaneDir=.true.
          n=0
          k=0
1570      if(k.lt.len(Veta)) then
            call Kus(Veta,k,Cislo)
            n=n+1
            go to 1570
          endif
          if(n.ne.3) then
            call FeChybne(-1.,YBottomMessage,
     1                    'the direction should be defined by three'//
     2                    ' coordinates.',' ',SeriousError)
            go to 1580
          endif
          go to 1575
        endif
1572    if(NAtomArr.le.0) then
          allocate(AtomArr(1))
          NAtomArr=1
        endif
        call SplitToAtomStrings(Veta,AtomArr,n,0,t80)
        if(t80.ne.' ') then
          call FeChybne(-1.,YBottomMessage,t80,' ',SeriousError)
          go to 1580
        endif
1575    if(NPar.eq.IdDrawPlane) then
          NAtPlane=max(NAtPlane,n)
          if(ik.eq.1.or..not.GrtPlaneDir) then
            if(n.le.2) then
              call FeChybne(-1.,YBottomMessage,
     1                      'the number of atoms defining plane have '//
     2                      'to larger than 2.',' ',SeriousError)
              go to 1580
            endif
          endif
          GrtPlane(ik)=Veta
        else
          DrawAtom(ik,ia)=Veta
        endif
        go to 1500
1580    EventType=EventEdw
        EventNumber=CheckNumber
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew) then
        call GrtUpdateDrawAtom(ia)
        call ReallocateGrtDraw(10)
        DrawN=DrawN+1
        ia=DrawN
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDelete)
     1  then
        do i=ia,DrawN-1
          DrawAtom(1,i)=DrawAtom(1,i+1)
          DrawSymSt(i)=DrawSymSt(i+1)
          DrawSymStU(i)=DrawSymStU(i+1)
          DrawParam(i)=DrawParam(i+1)
          DrawColor(i)=DrawColor(i+1)
        enddo
        DrawN=max(0,DrawN-1)
        ia=min(ia,DrawN)
        ia=max(ia,1)
        iao=0
        go to 1220
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtNeighbours) then
        call SelAtomsFromPhase('Select atoms for distance calculation',
     1                         Atom,BratDruhyATreti,kswa,isf,NAtCalc,
     2                         ich)
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBondVal) then
        call DistSetCommandsFromGrapht
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColor) then
        i=RolMenuSelectedQuest(nRolMenuColor)
        if(i.le.0) i=ColorOrder('White')
        if(icolor.ne.i) then
          DrawColor(ia)=i
          IColor=i
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuParType) then
        NPar=RolMenuSelectedQuest(nRolMenuParType)
        if(NPar.le.0) NPar=IdDrawXYZ
        if(NPar.ne.NParOld) then
          DrawParam(ia)=NPar*10+1
          iao=0
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) WhatToDraw=0
          WhatToDrawOld=-1
          go to 1220
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNext)
     1  then
        call GrtUpdateDrawAtom(ia)
        ia=min(ia+1,DrawN)
        go to 1220
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPrevious) then
        call GrtUpdateDrawAtom(ia)
        ia=max(ia-1,1)
        go to 1220
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwTypeFirst.and.
     2        CheckNumber.le.nCrwTypeLast) then
        WhatToDraw=CheckNumber-nCrwTypeFirst
        if(WhatToDraw.eq.0) then
          ia=1
        else
          UseRefLevel=.false.
        endif
        go to 1220
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwParFirst.and.
     2        CheckNumber.le.nCrwParFirst+ik-1) then
        IPar=CheckNumber-nCrwParFirst+1
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwOrientFirst.and.
     2        CheckNumber.le.nCrwOrientLast) then
        j=CheckNumber-nCrwOrientFirst
        i=j/NDimI(KPhase)+1
        j=mod(j,NDimI(KPhase))+1
        ipp=ior(i)
        ior(i)=j
        do k=i+1,NDimI(KPhase)
          if(ior(k).eq.j) then
            ior(k)=ipp
            go to 1220
          endif
        enddo
        go to 1220
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRefLevel)
     1  then
        if(CrwLogicQuest(nCrwRefLevel)) then
          if(EdwStateQuest(nEdwRefLevel).ne.EdwOpened) then
            RefLevel=GrtSetRefLevel(ia,isw,NPar,IPar)
            call FeQuestRealEdwOpen(nEdwRefLevel,RefLevel,.false.,
     1                              .false.)
            UseRefLevel=.true.
            EventType=EventEdw
            EventNumber=nEdwRefLevel
          endif
        else
          call FeQuestEdwClose(nEdwRefLevel)
          UseRefLevel=.false.
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawX4) then
        nEdw=nEdwTLim
        do i=1,NDimI(KPhase)
          if(CrwLogicQuest(nCrwDrawX4)) then
            Veta=smbx6(i+3)
          else
            Veta=smbt(i)
          endif
          call FeQuestEdwLabelChange(nEdw,Veta)
          nEdw=nEdw+3
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPlane1.or.
     2         CheckNumber.eq.nButtPlane2)) then
        call SelAtomsFromPhase('Select atoms for distance calculation',
     1                         Atom,BratAtom,kswa,isf,NAtCalc,ich)
        if(ich.ne.0) go to 1500
        Veta=' '
        do i=1,NAtCalc
          if(BratAtom(i)) then
            Veta=Veta(:idel(Veta))//' '//Atom(i)(:idel(Atom(i)))
          endif
        enddo
        if(CheckNumber.eq.nButtPlane1) then
          nEdw=nEdwPlane1
        else
          nEdw=nEdwPlane2
        endif
        call FeQuestStringEdwOpen(nEdw,Veta(2:))
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call ReallocateGrtDraw(10)
        DrawN=max(ia,DrawN)
        if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ.or.
     1     NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
          if(NPar.eq.IdDrawADP) then
            ik=7
          else if(NPar.eq.IdMagMoment) then
            ik=4
          else
            ik=3
          endif
          nCrw=nCrwParFirst
          do i=1,ik
            if(CrwLogicQuest(nCrw)) go to 1660
            nCrw=nCrw+1
          enddo
          i=1
        else
          i=0
        endif
1660    DrawParam(ia)=10*NPar+i
        DrawColor(ia)=icolor
        if(CrwStateQuest(nCrwNonMod).ne.CrwClosed)
     1    DrawNonMod=CrwLogicQuest(nCrwNonMod)
        if(CrwStateQuest(nCrwDrawX4).ne.CrwClosed) then
          GrtDrawX4=CrwLogicQuest(nCrwDrawX4)
        else
          GrtDrawX4=.false.
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        call CopyVek(gxo,gx,9)
        call CopyVek(gyo,gy,2)
        call GrtRestoreDrawAtom
        go to 9999
      endif
      call SetRealArrayTo(DxMod,3,0.)
      call SetIntArrayTo(NxMod,3,1)
      call SetIntArrayTo(NxMap,2,1)
      NxMapMx=1
      if(WhatToDraw.eq.0) then
        ndGrt=1
      else
        ndGrt=2
      endif
      DrawPoints=1
      do i=1,NDimI(KPhase)
        j=ior(i)
        n=nint((gx(2,j)-gx(1,j))/gx(3,j))+1
        if(i.le.ndGrt) then
          DrawPoints=DrawPoints*n
          DxMod(j)=gx(3,j)
          NxMod(j)=n
          if(WhatToDraw.eq.0) then
            xomn=gx(1,j)
            xomx=gx(2,j)
            yomn=gy(1)
            yomx=gy(2)
          endif
        else
          NxMapMx=NxMapMx*n
          NxMap(i-ndGrt)=n
        endif
      enddo
      if(Allocated(DrawT)) deallocate(DrawT,DrawValue,DrawOcc,DrawOccP,
     1                                XMod)
      if(allocated(BratAtom)) deallocate(BratAtom)
      allocate(DrawT(DrawPoints),DrawValue(DrawPoints,10),
     1         DrawOcc(DrawPoints,10),DrawOccP(DrawPoints,10),
     2         XMod(6*DrawPoints))
      NMapGrt=1
9999  return
      end
