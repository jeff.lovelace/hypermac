      subroutine GrtMakeLabel(n,tfirstg)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension nd(3),tfirstg(3)
      call RecUnpack(n,nd,NxMap,NDimI(KPhase)-ndGrt)
      do i=1,NDimI(KPhase)
        j=ior(i)
        if(i.le.ndGrt) then
          tfirstg(j)=gx(1,j)
        else
          tfirstg(j)=gx(1,j)+(nd(i-ndGrt)-1)*gx(3,j)
        endif
      enddo
      if(NDimI(KPhase).gt.ndGrt) then
        write(GraphtLabel,'(3(a1,''='',f10.3,'',''))')
     1    (smbt(ior(j)),tfirstg(ior(j)),j=ndGrt+1,NDimI(KPhase))
        call Zhusti(GraphtLabel)
        j=idel(GraphtLabel)
        GraphtLabel(j:j)=' '
      else
        GraphtLabel=' '
      endif
      return
      end
