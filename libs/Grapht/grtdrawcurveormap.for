      subroutine GrtDrawCurveOrMap(Klic,NovaMapa,GraphExists)
      use Basic_mod
      use Atoms_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      real, allocatable :: fna(:,:,:)
      dimension xsym(3),x4dif(3),tdif(3),nd(3),uxi(3,mxw),it(3),xx(3),
     1          uyi(3,mxw),bxi(6,mxw),byi(6,mxw),xpp(3),xp(6),rmb(36),
     2          GammaInt(9),GammaIntInv(9),bi(6),tfirstg(3),XDstSym(6),
     3          TrPom(36),upom(9),cpom(3,3),rpom(3,3),qcnta(3),sm(3)
      character*80 t80,u80,DruhySymmString
      character*8 atp1,atp2,at1,at2
      character*2 Label
      integer ColorOrder,RecModToRecMap,DrawNP,HardCopyIn,CrlCentroSymm,
     1        n123(3)
      logical GraphExists,EqIgCase
      data tpisq/19.7392088/
      HardCopyIn=HardCopy
      GraphExists=.false.
      if(NDimI(KPhase).gt.1) then
        if(Klic.gt.0) then
          NMapGrt=min(NMapGrt+1,NxMapMx)
        else if(Klic.lt.0) then
          NMapGrt=max(NMapGrt-1,1)
        endif
        if(WhatToDraw.ne.0.and.NovaMapa.eq.0) go to 6000
      else
        tfirstg(1)=gx(1,1)
      endif
      NPar=DrawParam(1)/10
      if(WhatToDraw.ne.0) then
        nmap=NxMapMx
        rewind l80
        if(nmap.gt.0)
     1    call FeFlowChartOpen(-1.,-1.,1,nmap,'Calculation of maps',' ',
     2                         ' ')
      else
        if(HardCopy.gt.100) then
          nmap=NxMapMx
        else
          nmap=1
        endif
      endif
      if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1   NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2   NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     3   NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        if(WhatToDraw.eq.0) then
          DrawNP=DrawN
        else
          DrawNP=1
        endif
        if(NPar.eq.IdDrawTorsion) then
          NAtoms=4
        else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
          NAtoms=2
        else
          NAtoms=1
        endif
      else
        DrawNP=1
      endif
      do nm=1,nmap
        if(WhatToDraw.eq.0.and.HardCopy.lt.100) then
          jmap=NMapGrt
        else
          jmap=nm
        endif
        call RecUnpack(jmap,nd,NxMap,NDimI(KPhase)-ndGrt)
        do k=1,NDimI(KPhase)
          j=ior(k)
          if(k.le.ndGrt) then
            tfirstg(j)=gx(1,j)
          else
            tfirstg(j)=gx(1,j)+(nd(k-ndGrt)-1)*gx(3,j)
          endif
        enddo
        call GrtSetDistKeys(NPar,tfirstg)
        CrenelX40=-33333.
        CrenelDelta=-33333.
        CrenelQcnt=-33333.
        do 5000i=1,DrawNP
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            IPar=max(mod(DrawParam(i),10),1)
            do j=1,NAtoms
              call AtomSymCode(DrawAtom(j,i),ia,isym,l,XDstSym,it,ich)
              if(ich.ne.0) go to 5000
              call atsymi(DrawAtom(j,i),ia,xsym,x4dif,tdif,isym,k,*5000)
              if(j.eq.1) then
                isw=ISwSymm(isym,iswa(ia),KPhase)
                call multm(rm(1,isym,isw,KPhase),XDst(1,ia),xp,3,3,1)
                if(isym.lt.0) call RealVectorToOpposite(xp,xp,3)
                call AddVek(xp,XDstSym,XDstSym,3)
                call srotb(TrToOrtho(1,isw,KPhase),
     1                     TrToOrtho(1,isw,KPhase),TrPom)
              else
                isw=iswa(ia)
                if(isw.ne.iswa(ia)) then


                endif
              endif
            enddo
            if(NPar.ne.IdDrawTorsion) then
              call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv)
              call MatInv(GammaIntInv,GammaInt,det,NDimI(KPhase))
              if(NPar.eq.IdDrawXYZ.or.
     1           NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawOcc) then
                do l=1,KModA(2,ia)
                  call multm(rm(1,isym,isw,KPhase),ux(1,l,ia),uxi(1,l),
     1                       3,3,1)
                  if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
                     call multm(rm(1,isym,isw,KPhase),uy(1,l,ia),
     1                          uyi(1,l),3,3,1)
                  else
                     call CopyVek(uy(1,l,ia),uyi(1,l),3)
                  endif
                  if(isym.lt.0) then
                    do j=1,3
                      uxi(j,l)=-uxi(j,l)
                      if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0)
     1                  uyi(j,l)=-uyi(j,l)
                    enddo
                  endif
                enddo
              else if(NPar.eq.IdMagMoment) then
                call multm(rmag(1,isym,isw,KPhase),sm0(1,ia),sm,3,3,1)
                if(isym.lt.0.and.
     1            ZMag(CrlCentroSymm(),isw,KPhase).lt.0) then
                  do j=1,3
                    sm(j)=-sm(j)
                  enddo
                endif
                do l=1,MagPar(ia)-1
                  call multm(rmag(1,isym,isw,KPhase),smx(1,l,ia),
     1                       uxi(1,l),3,3,1)
                  call multm(rmag(1,isym,isw,KPhase),smy(1,l,ia),
     1                          uyi(1,l),3,3,1)
                  if(isym.lt.0.and.
     1              ZMag(CrlCentroSymm(),isw,KPhase).lt.0) then
                    do j=1,3
                      uxi(j,l)=-uxi(j,l)
                      uyi(j,l)=-uyi(j,l)
                    enddo
                  endif
                enddo
              else if(NPar.eq.IdDrawADP) then
                call srotb(rm(1,isym,isw,KPhase),rm(1,isym,isw,KPhase),
     1                     rmb)
                call multm(rmb,beta(1,ia),bi,6,6,1)
                do j=1,KModA(3,ia)
                  call multm(rmb,bx(1,j,ia),bxi(1,j),6,6,1)
                  call multm(rmb,by(1,j,ia),byi(1,j),6,6,1)
                enddo
              endif
            endif
          else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1            NPar.eq.IdDrawVal) then
            if(WhatToDraw.eq.0) then
              do j=1,10
                DrawColor(j)=ColorOrder('White')
                if(DrawNonMod.and.mod(j,2).eq.0)
     1            DrawColor(j)=-DrawColor(j)
              enddo
            endif
            j=index(DrawAtom(1,1),'#')
            if(j.gt.0.and.EqIgCase(DrawAtom(1,1)(j+1:),'s1')) then
              DrawAtom(1,1)=DrawAtom(1,1)(:j-1)
              j=0
            endif
            if(j.gt.0) then
              PrvniSymmString=DrawAtom(1,1)
              atp1=DrawAtom(1,1)(:j-1)
            else
              PrvniSymmString=' '
              atp1=DrawAtom(1,1)
            endif
            if(NPar.le.IdDrawVal.and.DrawNonMod)
     1        DrawAtom(1,2)=DrawAtom(1,1)
            call atsymi(DrawAtom(1,1),ia,xsym,x4dif,tdif,isym,k,*5000)
            IPar=max(mod(DrawParam(1),10),1)
            isw=ISwSymm(isym,iswa(ia),KPhase)
            call SetLogicalArrayTo(BratPrvni,NAtCalc,.false.)
            BratPrvni(ia)=.true.
          endif
          if(GrtDrawX4) then
            tdif=x4dif
            qcnta=0.
          else
            if(NPar.ne.IdDrawPlane.and.NPar.ne.IdDrawIndDist.and.
     1         NPar.ne.IdDrawIndAng)
     2        call CopyVek(qcnt(1,ia),qcnta,NDimI(KPhase))
          endif
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            if(NPar.ne.IdDrawTorsion) then
              call AddVek(tdif,tfirstg,xpp,NDimI(KPhase))
              call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),
     1                   1)
              if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
                call MakeOccMod(DrawOcc(1,i),qcnta,xp,NxMod,DxMod,
     1                          a0(ia),ax(1,ia),ay(1,ia),KModA(1,ia),
     2                          KFA(1,ia),GammaIntInv)
              else
                call MakeOccModPol(DrawOcc(1,i),qcnta,xp,NxMod,DxMod,
     1                             ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                             ax(KModA(1,ia),ia),a0(ia)*.5,
     3                             GammaIntInv,TypeModFun(ia))
              endif
              call SetRealArrayTo(xx,3,0.)
              kk=0
              if(KFA(1,ia).ne.0) then
                xx(1)=GammaIntInv(1)*ax(1,ia)+s6(4,isym,isw,KPhase)
                if(CrenelDelta.lt.0.) then
                  CrenelDelta=a0(ia)
                  CrenelQcnt=qcnta(1)
                  CrenelX40=xx(1)
                endif
                kk=1
              endif
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(DrawOccP(1,i),qcnta,xp,
     1            NxMod,DxMod,uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),
     2            KFA(2,ia),GammaIntInv)
                do k=1,NxMod(1)*NxMod(2)*NxMod(3)
                  if(DrawOccP(k,i).gt.0) then
                    DrawOcc(k,i)=DrawOccP(k,i)
                  else
                    DrawOcc(k,i)=0.
                  endif
                enddo
                xx(1)=uyi(1,KModA(2,ia))
                kk=1
              endif
              call SpecPos(x(1,ia),xx,kk,iswa(ia),.05,nocc)
              if(EqIgCase(Atom(ia)(1:3),'Dum')) then
                pom=float(nocc)
              else
                pom=ai(ia)*float(nocc)
              endif
              do k=1,NxMod(1)*NxMod(2)*NxMod(3)
                DrawOcc(k,i)=DrawOcc(k,i)*pom
              enddo
              if(NPar.eq.IdDrawOcc) then
                NRank=1
              else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
                if(TypeModFun(ia).le.1) then
                  call MakePosMod(XMod,qcnta,xp,NxMod,DxMod,uxi,
     1                            uyi,KModA(2,ia),KFA(2,ia),GammaIntInv)
                else
                  call MakePosModPol(XMod,qcnta,xp,NxMod,DxMod,uxi,
     1                               uyi,KModA(2,ia),ax(KModA(1,ia),ia),
     2                               a0(ia)*.5,GammaIntInv,
     3                               TypeModFun(ia))
                endif
                NRank=3
              else if(NPar.eq.IdMagMoment) then
                call MakeMagMod(XMod,qcnta,xp,NxMod,DxMod,sm,uxi,uyi,
     1                          MagPar(ia)-1,GammaIntInv)
                NRank=3
              else if(NPar.eq.IdDrawADP) then
                if(TypeModFun(ia).le.1) then
                  call MakeBetaMod(XMod,qcnta,xp,NxMod,DxMod,bxi,
     1                             byi,KModA(3,ia),KFA(3,ia),
     2                             GammaIntInv)
                else
                  call MakeBetaModPol(XMod,qcnta,xp,NxMod,DxMod,
     1                                bxi,byi,KModA(3,ia),
     2                                ax(KModA(1,ia),ia),a0(ia)*.5,
     3                                GammaIntInv,TypeModFun(ia))
                endif
                NRank=6
              endif
            else
              call DistTorsionAngle(DrawAtom(1,i),' ')
              DrawSymSt(i)=' '
              DrawSymStU(i)=' '
              rewind 78
              if(WhatToDraw.eq.0) t=xomn
1900          read(78,end=2000) k,pom,Occ
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
                t=t+DxMod(ior(1))
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              DrawValue(kk,i)=pom
              DrawOcc(kk,i)=Occ
              go to 1900
2000          IPar=1
              NRank=1
              close(78,status='delete')
              go to 5000
            endif
            if(WhatToDraw.eq.0) t=xomn
            if(IPar.le.3) then
              j=IPar
            else
              j=1
            endif
            do k=1,DrawPoints
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              if(NPar.eq.IdDrawOcc) then
                DrawValue(kk,i)=DrawOcc(k,i)
                if(WhatToDraw.eq.0) DrawOcc(k,i)=1.
              else if(NPar.eq.IdDrawXYZ) then
                DrawValue(kk,i)=XMod(j)+xsym(IPar)
              else if(NPar.eq.IdDrawDeltaXYZ) then
                DrawValue(kk,i)=(XMod(j)+XSym(IPar)-XDstSym(IPar))*
     1                          CellPar(IPar,isw,KPhase)
              else if(NPar.eq.IdDrawADP) then
                if(IPar.le.3) then
                  DrawValue(kk,i)=(XMod(j)+bi(IPar))/
     1                            urcp(IPar,isw,KPhase)
                else if(IPar.eq.4) then
                  call AddVek(XMod(j),bi,XMod(j),6)
                  call boueq(XMod(j),sbeta(1,ia),0,DrawValue(kk,i),pom,
     1                       isw)
                else
                  call AddVek(XMod(j),bi,XMod(j),6)
                  call MultM(trpom,XMod(j),upom,6,6,1)
                  do m=1,6
                    upom(m)=upom(m)/tpisq
                  enddo
                  do m=1,6
                    call indext(m,ii,jj)
                    cpom(ii,jj)=upom(m)
                    if(ii.ne.jj) cpom(jj,ii)=upom(m)
                  enddo
                  call qln(cpom,rpom,upom,3)
                  pmin=upom(1)
                  pmax=upom(1)
                  mmin=1
                  mmax=1
                  do m=2,3
                    if(upom(m).gt.pmax) then
                      pmax=max(pmax,upom(m))
                      mmax=m
                    endif
                    if(upom(m).lt.pmin) then
                      pmin=min(pmin,upom(m))
                      mmin=m
                    endif
                  enddo
                  mmean=6-mmax-mmin
                  if(IPar.eq.5) then
                    DrawValue(kk,i)=upom(mmax)
                  else if(IPar.eq.6) then
                    DrawValue(kk,i)=upom(mmean)
                  else if(IPar.eq.7) then
                    DrawValue(kk,i)=upom(mmin)
                  endif
                endif
              else if(NPar.eq.IdMagMoment) then
                if(IPar.le.3) then
                  DrawValue(kk,i)=XMod(j)*CellPar(IPar,isw,KPhase)
                else
                  call MultM(TrToOrtho(1,isw,KPhase),XMod(j),sm,3,3,1)
                  DrawValue(kk,i)=VecOrtLen(sm,3)
                endif
              endif
              DrawValue(kk,i)=DrawValue(kk,i)-RefLevel
              if(WhatToDraw.eq.0) t=t+DxMod(ior(1))
              j=j+NRank
            enddo
          else if(NPar.eq.IdDrawPlane) then
            allocate(fna(3,DrawPoints,2))
            if(GrtPlaneDir) then
              idm=1
            else
              idm=2
            endif
            DrawOcc=0.
            if(NAtPlane.gt.NAtomArr) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(NAtPlane))
              NAtomArr=NAtPlane
            endif
            do 2130id=1,idm
              call CloseIfOpened(78)
              call SplitToAtomStrings(GrtPlane(id),AtomArr,n,1,u80)
              if(id.eq.1) then
                Cislo=AtomArr(1)
                k=index(Cislo,'#')
                if(k.gt.0) Cislo(k:)=' '
                k=ktat(Atom,NAtCalc,Cislo)
                isw=iswa(k)
              endif
              nr=0
              call DistBestPlane(AtomArr,n,n,GrtESD,isw,nr,cpom,rpom)
              rewind 78
              if(WhatToDraw.eq.0) t=xomn
2120          read(78,end=2130) k,xpp,Occ
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
                t=t+DxMod(ior(1))
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              call CopyVek(xpp,fna(1,kk,id),3)
              DrawOcc(kk,i)=max(Occ,DrawOcc(kk,i))
              go to 2120
2130        continue
            close(78,status='delete')
            if(GrtPlaneDir) then
              k=0
              call StToReal(GrtPlane(2),k,xpp,3,.false.,ich)
              if(ich.ne.0) then
                call FeChybne(-1.,-1.,'numerical problem in '//
     1                        'the direction string',GrtPlane(2),
     2                        SeriousError)
                go to 5000
              endif
              call multm(MetTensI(1,isw,KPhase),xpp,xp,3,3,1)
              call VecNor(xpp,xp)
            endif
            xp=0.
            do kt=1,ntall(isw)
              if(DrawOcc(kt,i).le.ocut) cycle
              if(.not.GrtPlaneDir) call CopyVek(fna(1,kt,2),xpp,3)
              call DistDihedralAngle(fna(1,kt,1),xpp,xp,xp,isw,
     1                               DrawValue(kt,i),pom)
            enddo
            close(78)
            deallocate(fna)
          else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            if(NAtomArr.le.0) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(1))
              NAtomArr=1
            endif
            if(NPar.eq.IdDrawIndDist) then
              idm=2
            else
              idm=3
            endif
            n=0
            do id=1,idm
              call SplitToAtomStrings(DrawAtom(id,i),AtomArr,n123(id),
     1                                0,u80)
              n=n+n123(id)
            enddo
            n1=n123(1)
            n2=n123(1)+n123(2)
            n3=n
            if(n.gt.NAtomArr) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(n))
              NAtomArr=n
            endif
            ip=1
            do id=1,idm
              call SplitToAtomStrings(DrawAtom(id,i),AtomArr(ip),
     1                                n123(id),1,u80)
              ip=ip+n123(id)
            enddo
            Cislo=AtomArr(1)
            k=index(Cislo,'#')
            if(k.gt.0) Cislo(k:)=' '
            k=ktat(Atom,NAtCalc,Cislo)
            isw=iswa(k)
            if(NPar.eq.IdDrawIndDist) then
              call DistCenter(AtomArr,n1,n2,isw)
            else
              call AngleCenter(AtomArr,n1,n2,n3,isw)
            endif
            rewind 78
            if(WhatToDraw.eq.0) t=xomn
2220        read(78,end=2230) k,pom,Occ
            if(WhatToDraw.eq.0) then
              DrawT(k)=t
              kk=k
              t=t+DxMod(ior(1))
            else
              kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
            endif
            DrawOcc(kk,i)=Occ
            DrawValue(kk,i)=pom
            go to 2220
2230        close(78)
          else
            IColor=ColorOrder('White')
            ocut=-1111.
            call DistAt
            rewind 78
            DrawN=0
            iprv=0
            if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1         NPar.eq.IdDrawVal) call GrtMakeLabel(jmap,tfirstg)
            NPar10=NPar*10
3300        read(78,end=4500) Label,At1,isfi,j,(pom,j=1,7),t80
            if(Label.ne.'#1'.or..not.EqIgCase(At1,atp1)) go to 3300
!            if(NPar.eq.IdDrawIndDist) then
!              j=index(t80,'#')
!              if(j.lt.idel(t80)) then
!                u80=At1(:idel(At1))//t80(j:)
!              else
!                u80=' '
!              endif
!              if(.not.EqIgCase(PrvniSymmString,u80)) go to 3300
!            endif
3310        read(78,end=4500) Label,At2,isfj,j,aij,(pom,j=1,6),t80
3315        if(Label.eq.'#2'.or.Label.eq.'#3') then
!              if(NPar.eq.IdDrawIndDist) then
!                if(.not.EqIgCase(At2,atp2)) go to 3310
!                j=index(t80,'#')
!                if(j.lt.idel(t80)) then
!                  u80=At2(:idel(At2))//t80(j:)
!                else
!                  u80=' '
!                endif
!              endif
              t80=At2(:idel(At2))//'#'//t80(:idel(t80))
            endif
            if(Label.eq.'#2') then
              if(NPar.eq.IdDrawAngles) then
                u80=t80
                go to 3310
!              else if(NPar.eq.IdDrawIndDist) then
!                if(.not.EqIgCase(DruhySymmString,u80)) go to 3310
              else
                PomRho=dBondVal(isfi,isfj)
                PomB=cBondVal(isfi,isfj)
                go to 3320
              endif
            else if(Label.eq.'#3') then
              if(NPar.eq.IdDrawAngles) then
                go to 3320
              else
                go to 3310
              endif
            else
              go to 3310
            endif
3320        if(NPar.ne.IdDrawVal.or.DrawN.eq.0) then
              call ReallocateGrtDraw(10)
              DrawN=DrawN+1
              if(UseRefLevel) then
                pom=-RefLevel
              else
                pom=0.
              endif
              call SetRealArrayTo(DrawOcc(1,DrawN),DrawPoints,0.)
              call SetRealArrayTo(DrawValue(1,DrawN),DrawPoints,pom)
              if(DrawNonMod) then
                call SetRealArrayTo(DrawOcc(1,DrawN+1),DrawPoints,0.)
                call SetRealArrayTo(DrawValue(1,DrawN+1),DrawPoints,pom)
              endif
            endif
            if(Label.eq.'#2') then
              DrawSymSt(DrawN)=t80
              DrawSymStU(DrawN)=' '
              DrawParam(DrawN)=NPar10
              DrawColor(DrawN)=IColor
              if(DrawNonMod) then
                DrawSymSt(DrawN+1)=t80
                DrawSymStU(DrawN+1)=' '
                DrawParam(DrawN+1)=NPar10
                DrawColor(DrawN+1)=-IColor
              endif
            else if(Label.eq.'#3') then
              DrawSymSt(DrawN)=u80
              DrawSymStU(DrawN)=t80
              DrawParam(DrawN)=NPar10
              DrawColor(DrawN)=IColor
              if(DrawNonMod) then
                DrawSymSt(DrawN+1)=u80
                DrawSymStU(DrawN+1)=t80
                DrawParam(DrawN+1)=NPar10
                DrawColor(DrawN+1)=-IColor
              endif
            endif
3325        read(78,end=4500) Label,At2,k,j,pomn,pom,spom,occi,occj,
     1                        occk,pp,t80
            if(Label.ne.' ') then
              isfj=k
              aij=pomn
              if(DrawNonMod.and.
     1           (NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles))
     2          DrawN=DrawN+1
              if(DrawN.ge.10) then
                call GrtDrawCurves(iprv,jmap,isw)
                iprv=1
                DrawN=0
                GraphExists=.true.
              endif
              go to 3315
            endif
            call RecUnpack(k,nd,nt(1,1),NDimI(KPhase))
            if(WhatToDraw.eq.0) then
              kk=k
            else
              kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
            endif
            if(WhatToDraw.eq.0) then
              DrawT(kk)=tfirst(ior(1),1)+(nd(ior(1))-1)*dt(ior(1),1)
              DrawOcc(kk,DrawN)=1.
              if(DrawNonMod) DrawOcc(kk,DrawN+1)=1.
            endif
            if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
              if(occi.le.OccLimit.or.occj.le.OccLimit.or.
     1           (NPar.eq.IdDrawAngles.and.occk.le.OccLimit)) then
                DrawOcc(kk,DrawN)=0.
                go to 3325
              endif
              if(UseReflevel) then
                DrawValue(kk,DrawN)=pom-RefLevel
              else
                DrawValue(kk,DrawN)=pom
              endif
              if(DrawNonMod) DrawValue(kk,DrawN+1)=pomn
            else
              if(occi.le.OccLimit) then
                DrawOcc(kk,DrawN)=0.
                go to 3325
              endif
              if(PomRho.le.0) go to 3325
              DrawValue(kk,DrawN)=DrawValue(kk,DrawN)+
     1                            exp((PomRho-pom)/PomB)*aij*occj
              if(DrawNonMod)
     1          DrawValue(kk,DrawN+1)=DrawValue(kk,DrawN+1)+
     2                                exp((PomRho-pomn)/PomB)*aij*occj
            endif
            go to 3325
4500        close(78,status='delete')
            if(WhatToDraw.eq.0) then
              if(DrawNonMod.and.DrawN.gt.0.and.
     1           NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles)
     2          DrawN=DrawN+1
              if(DrawN.gt.0) then
                call GrtDrawCurves(iprv,jmap,isw)
                GraphExists=.true.
              endif
            endif
          endif
          if(WhatToDraw.ne.0) then
            write(l80)(DrawValue(k,1),k=1,DrawPoints)
            nmp=nm
            call FeFlowChartEvent(nmp,ie)
            if(ie.ne.0) then
              call FeBudeBreak
              if(ErrFlag.ne.0) go to 9999
            endif
          endif
          call FeReleaseOutput
          call FeDeferOutput
5000    continue
        if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles.and.
     1     NPar.ne.IdDrawVal) then
          if(WhatToDraw.eq.0) then
            call GrtMakeLabel(jmap,tfirstg)
            call GrtDrawCurves(0,jmap,isw)
            call FeReleaseOutput
            call FeDeferOutput
            GraphExists=DrawN.gt.0
          endif
        else
          DrawParam(1)=NPar10
          NPar=NPar10/10
          call ReallocateGrtDraw(10)
          DrawN=1
        endif
      enddo
6000  if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles.and.
     1   WhatToDraw.ne.0) then
        call FeFlowChartRemove
        call GrtSetTr
        call GrtDrawMap(1)
        GraphExists=.true.
      else
        call FeFlowChartRemove
      endif
      if(CrenelDelta.gt.0.and.GrtShowCrenel) then
       xp(1)=CrenelX40-.5*CrenelDelta-CrenelQcnt
        call FeLineType(DottedLine)
        write(85,'(''# Crenel'')')
        do ii=1,2
          xp(2)=yomn
          xp(3)=0.
          write(85,'(2f15.6)') xp(1),xp(2)
          call FeXf2X(xp,xx)
          xu(1)=xx(1)
          yu(1)=xx(2)
          xp(2)=yomx
          write(85,'(2f15.6)') xp(1),xp(2)
          call FeXf2X(xp,xx)
          xu(2)=xx(1)
          yu(2)=xx(2)
          call FePolyLine(2,xu,yu,White)
          xp(1)=xp(1)+CrenelDelta
          write(85,'(''# end of curve'')')
        enddo
        call FeLineType(NormalLine)
      endif
      if(WhatToDraw.eq.0.and.(HardCopy.eq.HardCopyNum.or.
     1   HardCopy.eq.HardCopyNum+100)) write(85,'(''# end of file'')')
      call FeHardCopy(HardCopy,'close')
      if(JedeMovie) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  HardCopy=HardCopyIn
      return
133   format(a2,1x,a8,i6,3f10.4,1x,a80)
      end
