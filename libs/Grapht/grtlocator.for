      subroutine GrtLocator
      use Grapht_mod
      parameter (mxcurves=5)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      dimension xcur(:,:),ycur(:,:),n(mxcurves+1),xp(2)
      integer Color,ColorA(mxcurves+1),LineTypeDrawA(mxcurves+1)
      character*80 Veta,PrvniAtom,DruhyAtom
      character*8  SvFile
      logical Skip,JeUvnitr
      allocatable xcur,ycur
      allocate(xcur(DrawPoints,mxcurves+1),ycur(DrawPoints,mxcurves+1))
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile)
      XPosO=FeX2Xo(XPos)
      YPosO=FeY2Yo(YPos)
      XPosO=max(xomn,XPosO)
      XPosO=min(xomx,XPosO)
      YPosO=max(yomn,YPosO)
      YPosO=min(yomx,YPosO)
      XPosP=FeXo2X(XPosO)
      YPosP=FeYo2Y(YPosO)
      JeUvnitr=abs(Xposp-Xpos).le..5.and.abs(Yposp-Ypos).le..5
      if(JeUvnitr) then
        i=1
        write(Cislo,100) XPosO,YPosO
        call FeWInfWrite(1,Cislo)
      else
        i=0
      endif
      call FeMouseShape(i)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
      NPar=DrawParam(1)/10
1000  call FeQuestEvent(GraphtQuest,ich)
      m=1
      if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeLeftDown) then
        if(NPar.eq.IdDrawPlane) go to 1000
        ln=NextLogicNumber()
        open(ln,file=fln(:ifln)//'.l66')
        PrvniAtom=' '
1100    read(ln,FormA,end=1500) Veta
        if(Veta(1:1).ne.'#') go to 1100
        TextInfo(m)=' '
1120    if(index(Veta,'# distance-t plot for').eq.1) then
          k=22
          call kus(Veta,k,PrvniAtom)
          call DeleteXYZFromAtomName(PrvniAtom)
          LabelType=IdDrawDist
        else if(index(Veta,'# angle-t plot for').eq.1) then
          k=19
          call kus(Veta,k,PrvniAtom)
          call DeleteXYZFromAtomName(PrvniAtom)
          LabelType=IdDrawAngles
        else if(index(Veta,'# to').eq.1.and.PrvniAtom.ne.' ') then
          Veta=Veta(6:)
          if(LabelType.eq.IdDrawDist) then
            call DeleteXYZFromAtomName(Veta)
            TextInfo(m)='Distance '//PrvniAtom(:idel(PrvniAtom))//
     1                  ' - '//Veta(:idel(Veta))
          else if(LabelType.eq.IdDrawAngles) then
            i=index(Veta,'and')
            if(i.le.0) i=idel(Veta)+1
            DruhyAtom=Veta(:i-2)
            call DeleteXYZFromAtomName(DruhyAtom)
            TextInfo(m)='Angle '//DruhyAtom(:idel(DruhyAtom))//' - '//
     1                  PrvniAtom(:idel(PrvniAtom))//' - '
            Veta=Veta(i+3:)
            call DeleteXYZFromAtomName(Veta)
            TextInfo(m)=TextInfo(m)(:idel(TextInfo(m)))//
     1                  Veta(:idel(Veta))
          endif
        else if(index(Veta,'# Color : ').eq.1) then
          k=10
          call StToReal(Veta,k,xp,2,.false.,ich)
          if(ich.eq.0) then
            Color=nint(xp(1))
            LineTypeDraw=nint(xp(2))
          endif
        else if(index(Veta,'plot for').gt.0) then
          TextInfo(m)=Veta(3:)
        endif
        read(ln,FormA,end=1500) Veta
        if(Veta(1:1).eq.'#') go to 1120
1130    n(m)=1
        skip=.false.
1140    k=0
        call StToReal(Veta,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 1000
        xcur(n(m),m)=xp(1)
        ycur(n(m),m)=xp(2)
        read(ln,FormA,end=1500) Veta
        if(Veta(1:1).ne.'#') then
          n(m)=n(m)+1
          go to 1140
        else if(Veta.eq.'# skip') then
          skip=.true.
          go to 1200
        endif
1160    if(Veta.eq.'# end of curve') go to 1200
        read(ln,FormA,end=1500) Veta
        go to 1160
1200    if(n(m).le.1) go to 1245
        XPosO=FeXo2X(xcur(1,m))
        YPosO=FeYo2Y(ycur(1,m))
        do 1240i=1,n(m)
          XPosP=FeXo2X(xcur(i,m))
          YPosP=FeYo2Y(ycur(i,m))
          if(XPos.lt.XPosO.or.XPos.gt.XPosP) then
            XPosO=XPosP
            YPosO=YPosP
            go to 1240
          endif
          YPosP=(XPos-XPosO)/(XPosP-XPosO)*(YPosP-YPosO)+YPosO
          if(abs(YPos-YPosP).lt.3.) then
            call FeDeferOutput
            call FeXYPlot(xcur(1,m),ycur(1,m),n(m),LineTypeDraw,
     1                    NormalPlotMode,Black)
            call FeXYPlot(xcur(1,m),ycur(1,m),n(m),DottedLine,
     1                    NormalPlotMode,Color)
            ColorA(m)=Color
            LineTypeDrawA(m)=LineTypeDraw
            call FeReleaseOutput
            if(m.gt.mxcurves) then
              XPosP=XPos
              YPosP=YPos
              write(Veta,'(i5)') MxCurves
              call Zhusti(Veta)
              call FeChybne(-1.,-1.,'the maximum number of '//
     1                      Veta(:idel(Veta))//' curves reached.',
     2                      'The list will not be complete.',Warning)
              XPos=XposP
              YPos=YPosP
              go to 1500
            endif
            m=m+1
            go to 1100
          else
            go to 1245
          endif
1240    continue
1245    if(Skip) then
1250      read(ln,FormA,end=1500) Veta
          if(Veta(1:1).ne.'#') then
            go to 1130
          else
            if(Veta.eq.'# end of curve') then
              Skip=.false.
              go to 1200
            else
              go to 1250
            endif
          endif
        else
          go to 1100
        endif
1500    close(ln)
        m=m-1
        if(m.gt.0) then
          NInfo=m
          call FeInfoWin(-1.,-1.)
1520      call FeQuestEvent(GraphtQuest,ich)
          if(CheckType.eq.EventMouse) then
            if(CheckNumber.eq.JeMove) then
              XPosO=FeX2Xo(XPos)
              YPosO=FeY2Yo(YPos)
              XPosO=max(xomn,XPosO)
              XPosO=min(xomx,XPosO)
              YPosO=max(yomn,YPosO)
              YPosO=min(yomx,YPosO)
              XPosP=FeXo2X(XPosO)
              YPosP=FeYo2Y(YPosO)
              JeUvnitr=abs(Xposp-Xpos).le..5.and.abs(Yposp-Ypos).le..5
              if(JeUvnitr) then
                i=1
              else
                i=0
              endif
              call FeMouseShape(i)
              go to 1520
            else if(CheckNumber.eq.JeLeftUp.or.
     1              CheckNumber.eq.JeRightUp) then
              go to 1520
            endif
          else if(CheckType.eq.EventKey) then
            if(CheckNumber.ne.JeEscape) go to 1520
          else if(CheckType.eq.EventButton) then
            go to 1540
          else
            go to 1520
          endif
1540      call FeKillInfoWin
          do j=1,m
            call FeXYPlot(xcur(1,j),ycur(1,j),n(j),DottedLine,
     1                    NormalPlotMode,Black)
          enddo
          do j=1,m
            call FeXYPlot(xcur(1,j),ycur(1,j),n(j),LineTypeDrawA(j),
     1                    NormalPlotMode,ColorA(j))
          enddo
        endif
        go to 1000
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeMove) then
        XPosO=FeX2Xo(XPos)
        YPosO=FeY2Yo(YPos)
        XPosO=max(xomn,XPosO)
        XPosO=min(xomx,XPosO)
        YPosO=max(yomn,YPosO)
        YPosO=min(yomx,YPosO)
        XPosP=FeXo2X(XPosO)
        YPosP=FeYo2Y(YPosO)
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          if(JeUvnitr) then
            call FeMouseShape(0)
            JeUvnitr=.false.
            call FeWinfClose(1)
          endif
        else
          if(.not.JeUvnitr) then
            call FeMouseShape(1)
            JeUvnitr=.true.
          endif
          write(Cislo,100) XPosO,YPosO
          call FeWInfWrite(1,Cislo)
        endif
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown)
     1  then
        go to 5000
      else if(CheckType.eq.EventButton) then
        go to 5000
      endif
      go to 1000
5000  TakeMouseMove=.false.
      call FeMouseShape(0)
      call FePlotMode('N')
      call FeDeferOutput
      call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile,0)
      deallocate(xcur,ycur)
      return
100   format(f7.3,f8.3)
      end
