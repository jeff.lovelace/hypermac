      subroutine GrtUpdateDrawAtom(ia)
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*256 EdwStringQuest,AtPom
      logical CrwLogicQuest
      NPar=DrawParam(ia)/10
      if(NPar.eq.IdDrawTorsion) then
        NAtoms=4
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        AtPom=EdwStringQuest(nEdwPlane1)
        DrawAtom(1,ia)=AtPom
        AtPom=EdwStringQuest(nEdwPlane2)
        DrawAtom(2,ia)=AtPom
        if(NPar.eq.IdDrawIndAng) then
          AtPom=EdwStringQuest(nEdwPlane3)
          DrawAtom(3,ia)=AtPom
        endif
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
        NAtoms=0
      else
        NAtoms=1
      endif
      nEdw=nEdwName
      do i=1,NAtoms
        AtPom=EdwStringQuest(nEdw)
        nEdw=nEdw+1
        j=index(AtPom,'#')
        if(j.le.0) then
          j=idel(AtPom)
        else
          j=j-1
        endif
        if(AtPom(:j).ne.' ') call UprAt(AtPom(:j))
        DrawAtom(i,ia)=AtPom
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
      enddo
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else if(NPar.eq.IdDrawADP) then
        ik=7
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=1,ik
        if(CrwLogicQuest(nCrw)) go to 2000
        nCrw=nCrw+1
      enddo
      i=1
2000  DrawParam(ia)=10*NPar+i
      return
      end
