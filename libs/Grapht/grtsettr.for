      subroutine GrtSetTr
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'grapht.cmn'
      c=1.
      alfa=90.
      bett=90.
      i=ior(1)
      j=ior(2)
      k=1
      NSubs=1
      call ConSetCellParCon
      a=sqrt(MetTens6(i+(i-1)*NDim(KPhase),NSubs,KPhase))
      b=sqrt(MetTens6(j+(j-1)*NDim(KPhase),NSubs,KPhase))
      c=sqrt(MetTens6(k+(k-1)*NDim(KPhase),NSubs,KPhase))
      pom=MetTens6(i+(j-1)*NDim(KPhase),NSubs,KPhase)/(a*b)
      gama=acos(pom)
      if(i.gt.3) then
        gaman=atan(a/ee(i-3)*tan(gama))
        a=ee(i-3)
        b=b*sin(gama)/sin(gaman)
        gama=gaman
      endif
      if(j.gt.3) then
        gaman=atan(b/ee(j-3)*tan(gama))
        b=ee(j-3)
        a=a*sin(gama)/sin(gaman)
        gama=gaman
      endif
      gama=gama/ToRad
      csa=cos(torad*alfa)
      csb=cos(torad*bett)
      csg=cos(torad*gama)
      sng=sin(torad*gama)
      volume=sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      F2O(1)=a*sng
      F2O(4)=0.
      F2O(2)=a*csg
      F2O(5)=b
      F2O(3)=0.
      F2O(6)=0.
      F2O(7)=c*csb
      F2O(8)=c*(csa-csb*csg)/sng
      F2O(9)=c*volume/sng
      call matinv(F2O,O2F,pom,3)
      do i=1,NDimI(KPhase)
        j=ior(i)
        xmin(i)=gx(1,j)
        xmax(i)=gx(2,j)
        dx(i)=gx(3,j)
      enddo
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
      return
      end
