      subroutine GrtReadOptions(ich)
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*80 Veta
      logical CrwLogicQuest
      ich=0
      id=NextQuestId()
      xdq=200.
      xdqp=xdq*.5
      il=3
      call FeQuestCreate(id,-1.,-1.,xdq,il,' ',0,LightGray,0,0)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+5.
      Veta='Indicate %commensurate t sections'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,DrawTCommen)
      nCrwTSections=CrwLastMade
      if(kcommen(KPhase).le.0) call FeQuestCrwDisable(CrwLastMade)
      il=il+1
      Veta='%Show crenel limits'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,GrtShowCrenel)
      nCrwShowCrenel=CrwLastMade
      il=il+1
      tpom=5.
      Veta='%Occupancy limit'
      dpom=60.
      xpom=xdq-dpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOccLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,OccLimit,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(kcommen(KPhase).gt.0)
     1    DrawTCommen=CrwLogicQuest(nCrwTSections)
        GrtShowCrenel=CrwLogicQuest(nCrwShowCrenel)
        call FeQuestRealFromEdw(nEdwOccLimit,OccLimit)
      endif
      call FeQuestRemove(id)
      return
      end
