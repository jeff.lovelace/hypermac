      subroutine GrtSaveDrawAtom
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension iorSave(3)
      character*80, allocatable :: DrawSymStSave(:),DrawSymStUSave(:)
      character*256, allocatable :: DrawAtomSave(:,:)
      integer, allocatable :: DrawColorSave(:),DrawParamSave(:)
      save DrawNSave,DrawAtomSave,DrawColorSave,DrawParamSave,
     1     DrawSymStSave,DrawSymStUSave,iorSave
      DrawNSave=DrawN
      if(allocated(DrawAtomSave))
     1  deallocate(DrawAtomSave,DrawSymStSave,DrawSymStUSave,
     2             DrawColorSave,DrawParamSave)
      allocate(DrawAtomSave(4,DrawN),DrawSymStSave(DrawN),
     1         DrawSymStUSave(DrawN),DrawColorSave(DrawN),
     2         DrawParamSave(DrawN))
      do i=1,DrawN
        DrawAtomSave(1:4,i)=DrawAtom(1:4,i)
        DrawParamSave(i)=DrawParam(i)
        DrawColorSave(i)=DrawColor(i)
        DrawSymStSave(i)=DrawSymSt(i)
        DrawSymStUSave(i)=DrawSymStU(i)
      enddo
      do i=1,NDimI(KPhase)
        iorSave(i)=ior(i)
      enddo
      go to 9999
      entry GrtRestoreDrawAtom
      DrawN=DrawNSave
      do i=1,DrawN
        DrawAtom(1:4,i)=DrawAtomSave(1:4,i)
        DrawParam(i)=DrawParamSave(i)
        DrawColor(i)=DrawColorSave(i)
        DrawSymSt(i)=DrawSymStSave(i)
        DrawSymStU(i)=DrawSymStUSave(i)
      enddo
      do i=1,NDimI(KPhase)
        ior(i)=iorSave(i)
      enddo
      if(allocated(DrawAtomSave))
     1  deallocate(DrawAtomSave,DrawSymStSave,DrawSymStUSave,
     2             DrawColorSave,DrawParamSave)
9999  return
      end
