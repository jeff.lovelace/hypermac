      subroutine GrtDrawMap(Klic)
      use Grapht_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      character*80 Veta
      character*8  LabelP
      real tfirstg(3)
      if(Klic.eq.0) go to 2000
      VolaToContour=.false.
      if(UseRefLevel) then
        ContourRefLevel=RefLevel
      else
        ContourRefLevel=0.
      endif
      DensityType=2
      do i=1,NDimI(KPhase)
        if(GrtDrawX4) then
          cx(i)=smbx6(ior(i)+3)
        else
          cx(i)=smbt(ior(i))
        endif
        iorien(i)=ior(i)
        ee(i)=1.
      enddo
      iorien(3)=6-iorien(1)-iorien(2)
      zmap(1)=0.
      do i=4,NDim(KPhase)
        iorien(i)=i
        zmap(i-2)=0.
      enddo
      call GrtSetTr
      DMin= 999.
      DMax=-999.
      rewind l80
      do j=1,NxMapMx
        read(l80)(DrawValue(i,1),i=1,DrawPoints)
        do i=1,DrawPoints
          pom=DrawValue(i,1)
          DMin=min(pom,DMin)
          DMax=max(pom,DMax)
        enddo
      enddo
      do i=1,2
        nx(i)=NxMod(ior(i))
      enddo
      nxny=DrawPoints
      call SetContours(0)
      Obecny=.false.
      call UnitMat(tm3,3)
      do i=1,NDimI(KPhase)
        do j=1,NDimI(KPhase)
          if(j.eq.ior(i)) then
            tm3(i,j)=1.
          else
            tm3(i,j)=0.
          endif
        enddo
      enddo
      call MatInv(tm3,tm3i,pom,3)
2000  call FeHardCopy(HardCopy,'open')
      call GrtMakeLabel(NMapGrt,tfirstg)
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(DrawPoints))
      call CopyVek(DrawValue,ActualMap,DrawPoints)
      NActualMap=DrawPoints
      if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100) then
        NPar=DrawParam(1)/10
        IPar=max(mod(DrawParam(1),10),1)
        if(NPar.eq.IdDrawOcc) then
          LabelP='occ'
        else if(NPar.eq.IdDrawXYZ) then
          LabelP=smbx(IPar)
        else if(NPar.eq.IdDrawDeltaXYZ) then
          LabelP='d'//smbx(IPar)
        else if(NPar.eq.IdDrawADP) then
          LabelP=SmbU(IPar)
        else if(NPar.eq.IdDrawTorsion) then
          LabelP='Tors'
        else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
          LabelP='Dist'
        else if(NPar.eq.IdDrawVal) then
          LabelP='Valence'
        else if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     1          NPar.eq.IdDrawIndAng) then
          LabelP='Angle'
        else if(NPar.eq.IdMagMoment) then
          if(IPar.le.3) then
            LabelP='M'//smbx(IPar)
          else
            LabelP='|M|'
          endif
        endif
        Veta=cx(1)(:idel(cx(1)))//'-'//cx(2)(:idel(cx(2)))//' map'
        Veta='# '//LabelP(:idel(LabelP))//' '//Veta(:idel(Veta))//
     1       ' for '//DrawAtom(1,1)(:idel(DrawAtom(1,1)))
        write(85,FormA) Veta(:idel(Veta))
        Veta='# columns: '//cx(1)(:idel(cx(1)))//','//
     1                      cx(2)(:idel(cx(2)))//','
        if(NDim(KPhase).gt.5) Veta=Veta(:idel(Veta))//
     1                             cx(3)(:idel(cx(3)))//','
        Veta=Veta(:idel(Veta))//LabelP
        write(85,FormA) Veta(:idel(Veta))
        if(HardCopy.eq.HardCopyNum+100) then
          rewind l80
          pom3=xmin(3)
          do i3=1,NxMapMx
            read(l80)(DrawValue(i,1),i=1,DrawPoints)
            k=0
            pom2=xmin(2)
            do i1=1,nx(2)
              pom1=xmin(1)
              do i2=1,nx(1)
                k=k+1
                if(NDimI(KPhase).gt.5) then
                  write(85,100) pom1,pom2,pom3,DrawValue(k,1)
                else
                  write(85,100) pom1,pom2,DrawValue(k,1)
                endif
                pom1=pom1+dx(1)
              enddo
              pom2=pom2+dx(2)
            enddo
            pom3=pom3+dx(3)
          enddo
        endif
      endif
      rewind l80
      do i=1,NMapGrt
        read(l80)(DrawValue(k,1),k=1,DrawPoints)
      enddo
      if(HardCopy.eq.HardCopyNum) then
        k=0
        pom2=xmin(2)
        do i1=1,nx(2)
          pom1=xmin(1)
          do i2=1,nx(1)
            k=k+1
            write(85,'(3f10.5)') pom1,pom2,DrawValue(k,1)
            pom1=pom1+dx(1)
          enddo
          pom2=pom2+dx(2)
        enddo
        go to 9000
      endif
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeMakeAcFrame
        pom=min(FeTxLength(GraphtLabel),
     1          abs(XCornAcWin(1,2)-XCornAcWin(1,3)))*.5
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1               (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2               pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3               (XCornAcWin(1,2)-XCornAcWin(1,3)),GraphtLabel,'C',
     4               White)
        call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
        call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
        call CopyVek(DrawValue,ActualMap,DrawPoints)
        if(DrawPos.gt.0) then
          call Vrstvy( 1)
          if(ErrFlag.ne.0) go to 9999
        endif
        if(DrawNeg.gt.0) then
          call Vrstvy(-1)
          if(ErrFlag.ne.0) go to 9999
        endif
        if(HardCopy.eq.HardCopyNum) then

        endif
      endif
9000  call FeHardCopy(HardCopy,'close')
9999  return
100   format(4f10.5)
      end
