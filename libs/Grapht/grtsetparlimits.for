      subroutine GrtSetParLimits(nEdwPLim,NPar)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      data NParSave/-1/
      nEdw=nEdwPLim
      if(NPar.eq.NParSave) then
        pom=EdwRealQuest(1,nEdw)
      else if(NPar.eq.IdDrawOcc) then
        pom=-.1
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        pom=-.5
      else if(NPar.eq.IdDrawADP) then
        pom=-.05
      else if(NPar.eq.IdDrawTorsion) then
        pom=-180.
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        pom=1.
      else if(NPar.eq.IdDrawVal) then
        pom=1.
      else if(NPar.eq.IdDrawAngles) then
        pom=90.
      else if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndAng) then
        pom=-90.
      else if(NPar.eq.IdMagMoment) then
        pom=-3.
      endif
      call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
      gy(1)=pom
      nEdw=nEdw+1
      if(NPar.eq.NParSave) then
        pom=EdwRealQuest(1,nEdw)
      else if(NPar.eq.IdDrawOcc) then
        pom=1.1
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        pom=.5
      else if(NPar.eq.IdDrawADP) then
        pom=.05
      else if(NPar.eq.IdDrawTorsion) then
        pom=180.
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        pom=4.
      else if(NPar.eq.IdDrawVal) then
        pom=3.
      else if(NPar.eq.IdDrawAngles) then
        pom=180.
      else if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndAng) then
        pom=90.
      else if(NPar.eq.IdMagMoment) then
        pom=3.
      endif
      call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
      gy(2)=pom
      NParSave=NPar
      return
      end
