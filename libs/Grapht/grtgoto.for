      subroutine GrtGoTo(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension nd(2)
      character*5 t5
      il=NDimI(KPhase)-ndGrt
      call RecUnpack(NMapGrt,nd,NxMap,il)
      id=NextQuestId()
      xqd=150.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Next graph to be drawn',0,
     1                   LightGray,0,0)
      il=0
      do i=ndGrt+1,NDimI(KPhase)
        il=il+1
        j=ior(i)
        t5='%'//smbt(j)//' ='
        call FeQuestEudMake(id,5.,il,30.,il,t5,'L',90.,EdwYd,0)
        pom=gx(1,j)+(nd(i-ndGrt)-1)*gx(3,j)
        if(i.eq.NDimI(KPhase)) nEdwLast=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
      enddo
      nEdwLast=EdwLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwLast
        NMapGrt=0
        do i=NDimI(KPhase),ndGrt+1,-1
          j=ior(i)
          call FeQuestRealFromEdw(nEdw,pom)
          if(gx(3,j).ne.0.) then
            n=nint((pom-gx(1,j))/gx(3,j))+1
          else
            n=1
          endif
          NMapGrt=NMapGrt*NxMap(i-ndGrt)+n-1
          nEdw=nEdw-1
        enddo
        NMapGrt=NMapGrt+1
      endif
      call FeQuestRemove(id)
      return
      end
