      function GrtSetRefLevel(ia,isw,NPar,IPar)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      if(NPar.eq.IdDrawOcc) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawADP) then
        if(IPar.le.3) then
          GrtSetRefLevel=beta(IPar,ia)/urcp(IPar,isw,KPhase)
        else
          call boueq(beta(1,ia),sbeta(1,ia),0,pom,spom,isw)
          GrtSetRefLevel=spom
        endif
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        if(WhatToDraw.eq.0) then
          GrtSetRefLevel=0.
        else
          GrtSetRefLevel=2.
        endif
      else if(NPar.eq.IdDrawVal) then
        if(WhatToDraw.eq.0) then
          GrtSetRefLevel=0.
        else
          GrtSetRefLevel=2.
        endif
      else if(NPar.eq.IdDrawTorsion) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawAngles) then
        GrtSetRefLevel=0.
      endif
      return
      end
