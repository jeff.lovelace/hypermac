      subroutine GrtDrawPerPartes(x,y,o,nn,LineTypeDraw,PlotMode,Color)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension x(nn),y(nn),o(nn)
      integer Color,PlotMode
      n=0
      np=1
      do i=1,nn
        if(o(i).gt.OccLimit) then
          n=n+1
        else
          if(n.gt.0) then
            call FeXYPlot(x(np),y(np),n,LineTypeDraw,PlotMode,Color)
            if(HardCopy.eq.HardCopyNum.or.
     1         HardCopy.eq.HardCopyNum+100) write(85,'(''# skip'')')
            np=i
            n=0
          endif
          np=np+1
        endif
      enddo
      if(n.gt.0) call FeXYPlot(x(np),y(np),n,LineTypeDraw,PlotMode,
     1                         Color)
      return
      end
