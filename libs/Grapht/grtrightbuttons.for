      subroutine GrtRightButtons
      use Contour_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      character*80 Veta
      character*17 :: Labels(12) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot',
     5                '%New/Edit        ',
     6                'G%-              ',
     7                'G%+              ',
     8                '%Go to           ',
     9                '%Movie           ',
     a                '%Contours        ',
     1                'Cur%ves          ',
     2                '%Options         '/)
      integer ButtonStateQuest,RecPack,FeGetSystemTime,HardCopyOld
      logical GraphExists,FeYesNo
      Tiskne=.false.
      nLblMovie=0
      VolaToDist=.false.
      DrawPos=1
      DrawNeg=1
      call SetLogicalArrayTo(BratDruhyATreti,NAtCalc,.true.)
      GraphExists=.false.
      if(NDimI(KPhase).gt.1) then
        l80=NextLogicNumber()
        call OpenFile(l80,fln(:ifln)//'.l80','unformatted','unknown')
      endif
      GraphtQuest=NextQuestId()
      ContourQuest=GraphtQuest
      CheckMouse=.true.
      call FeQuestAbsCreate(GraphtQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      ContourYSep(1)=pom
      k=1
      ik=12
      do i=1,ik
        ActiveButtBas(i)=.false.
        call FeQuestAbsButtonMake(GraphtQuest,xpom,ypom,
     1                            wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        else if(i.eq.5) then
          nButtNewGraph=ButtonLastMade
          wpom=wpom/2.-5.
          nButtBasFr=ButtonLastMade
        else if(i.eq.6) then
          nButtMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.7) then
          nButtPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.8) then
          nButtGoTo=ButtonLastMade
        else if(i.eq.9) then
          nButtMovie=ButtonLastMade
        else if(i.eq.10) then
          nButtContours=ButtonLastMade
        else if(i.eq.11) then
          nButtCurves=ButtonLastMade
        else if(i.eq.12) then
          nButtOptions=ButtonLastMade
        endif
        if(i.le.5.or.NDimI(KPhase).gt.1.or.i.eq.12) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        ActiveButtBas(i)=.true.
        call FeQuestButtonOpen(ButtonLastMade,j)
1050    if(i.eq.3.or.i.eq.7.or.i.eq.9.or.i.eq.10) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
          k=k+1
          ContourYSep(k)=ypom
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      fpom=FeTxLength('XXX.XXX')
      dhl1=FeTxLength(ConWinfLabel(1))
      do i=1,3
        if(i.eq.1) then
          dpom=2.*fpom+5.
          xpom=XCenGrWin-.5*dhl1-dpom-20.
          ypom=YMaxBasWin-20.
        else if(i.eq.2) then
          dpom=3.*fpom+5.
          xpom=XCenGrWin+.5*dhl1+20.
        else if(i.eq.3) then
          xpom=XMaxGrWin-100.
          ypom=YMinGrWin-25.
          dpom=FeTxLength('XXXXXXX.XX')+5.
        endif
        if(i.gt.2) then
          call FeQuestAbsLblMake(ContourQuest,xpom-10.,YBottomText,
     1                           'Value','R','N')
          call FeQuestLblOff(LblLastMade)
          nLblWinf(i)=LblLastMade
        endif
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      CheckNumber=nButtNewGraph
2000  if(GraphExists) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtSave,j)
      call FeQuestButtonOpen(nButtPrint,j)
      if(CallDPlot.ne.' ') then
        call FeQuestButtonOpen(nButtDPlot,j)
      else
        call FeQuestButtonOpen(nButtDPlot,ButtonDisabled)
      endif
      if(WhatToDraw.eq.1) then
        call FeQuestButtonOpen(nButtCurves,j)
        call FeQuestButtonOpen(nButtContours,j)
      else
        call FeQuestButtonOpen(nButtCurves,ButtonDisabled)
        call FeQuestButtonOpen(nButtContours,ButtonDisabled)
      endif
      if(NDimI(KPhase).gt.1) then
        if(NMapGrt.gt.1) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtMinus,j)
        if(NMapGrt.lt.NxMapMx) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtPlus,j)
      endif
      if(ButtonStateQuest(nButtPlus ).eq.ButtonOff.or.
     1   ButtonStateQuest(nButtMinus).eq.ButtonOff) then
        call FeQuestButtonOff(nButtGoto)
        call FeQuestButtonOff(nButtMovie)
      else
        call FeQuestButtonDisable(nButtGoto)
        call FeQuestButtonDisable(nButtMovie)
      endif
      call FeQuestMouseToButton(CheckNumber)
2090  if(GraphExists) then
        if(WhatToDraw.eq.0) then
          call GrtLocator
        else
          call PCurves(1)
        endif
      else
        call FeQuestEvent(GraphtQuest,ich)
      endif
2100  HardCopy=0
      ErrFlag=0
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          do i=1,3
            call FeWInfRemove(i)
          enddo
          call FeQuestRemove(GraphtQuest)
          go to 9999
        else if(CheckNumber.eq.nButtNewGraph) then
2200      call GrtDefParams(ich)
          if(ich.eq.0) then
            call GrtDrawCurveOrMap(0,1,GraphExists)
            if(ErrFlag.ne.0) then
              call FeClearGrWin
              GraphExists=.false.
              ErrFlag=0
            else
              if(.not.GraphExists) then
                call FeClearGrWin
                call FeChybne(-1.,-1.,'no curve to be drawn, please '//
     1                        'try again',' ',Warning)
                call ReallocateGrtDraw(10)
                DrawN=1
                go to 2200
              endif
            endif
          endif
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            call GrtDrawCurveOrMap(0,0,GraphExists)
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            HardCopy=0
          endif
        else if(CheckNumber.eq.nButtSave) then
          if(NDimI(KPhase).gt.1) then
            i=0
          else
            i=1
          endif
          call FeSavePicture('graph',7,i)
          if(HardCopy.lt.0) HardCopy=0
          if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                            HardCopy.ne.HardCopyPCX)) then
            call GrtDrawCurveOrMap(0,0,GraphExists)
          else
            call FeHardCopy(HardCopy,'open')
            call FeHardCopy(HardCopy,'close')
          endif
        else if(CheckNumber.eq.nButtDPlot) then
          call GrtDPlot
        else if(CheckNumber.eq.nButtMinus) then
          call GrtDrawCurveOrMap(-1,0,GraphExists)
        else if(CheckNumber.eq.nButtPlus) then
          call GrtDrawCurveOrMap( 1,0,GraphExists)
        else if(CheckNumber.eq.nButtGoto) then
          call GrtGoTo(ich)
          if(ich.eq.0) call GrtDrawCurveOrMap(0,0,GraphExists)
        else if(CheckNumber.eq.nButtMovie) then
          call GrtDefMovie(ich)
          if(ich.ne.0) go to 2000
          if(nLblMovie.le.0) then
            xpom=350.
            ypom=YBottomMargin*.5
            Veta='Press Esc to interrupt the movie'
            call FeQuestAbsLblMake(ContourQuest,xpom,ypom,Veta,
     1                             'L','B')
            nLblMovie=LblLastMade
          else
            call FeQuestLblOn(nLblMovie)
          endif
          JedeMovie=.true.
          irpt=0
          isv=0
          HardCopyOld=HardCopy
3000      irpt=irpt+1
          if(irpt.gt.MovieRepeat.and.MovieRepeat.ne.0) go to 3200
          call CopyVekI(nxMovieFr,nxdraw,NDimI(KPhase)-ndGrt)
3050      if(irpt.eq.1.and.MovieFileName.ne.' ') then
            isv=isv+1
            write(HCFileName,'(''_'',i4)') isv
            do i=1,idel(HCFileName)
              if(HCFileName(i:i).eq.' ') HCFileName(i:i)='0'
            enddo
            HCFileName=MovieFileName(:idel(MovieFileName))//
     1               HCFileName(:idel(HCFileName))//
     2               HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
          else
            HardCopy=0
          endif
          NMapGrt=RecPack(NxDraw,NxMap,NDimI(KPhase)-ndGrt)
          call GrtDrawCurveOrMap(0,0,GraphExists)
          if(ErrFlag.ne.0) go to 3200
          if(HardCopy.ne.0) then
            i=HardCopy
            HardCopy=0
            call KresliMapu( 0)
            if(ErrFlag.ne.0) go to 3200
            HardCopy=i
          endif
          TimeStart=FeGetSystemTime()
3100      call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            if(FeYesNo(-1.,-1.,
     1                  'Do you want really to interrupt it?',1))
     2        go to 3200
          else if(EventType.eq.EventAscii.and.
     1            char(EventNumber).eq.' ') then
3120        call FeEvent(1)
            if(EventType.ne.EventAscii.or.char(EventNumber).ne.' ')
     1        go to 3120
          else
            if(FeGetSystemTime()-TimeStart.le.
     1         nint(DelayTimeForMovie*1000.)) go to 3100
          endif
          do i=1,NDim(KPhase)-2
            if(nxdraw(i).lt.nxMovieTo(i)) go to 3150
          enddo
          go to 3000
3150      nxdraw(1)=nxdraw(1)+1
          if(nxdraw(1).gt.nxMovieTo(1)) then
            nxdraw(1)=nxMovieFr(1)
            nxdraw(2)=nxdraw(2)+1
            if(nxdraw(2).gt.nxMovieTo(2)) then
               call CopyVekI(nxMovieFr,nxdraw,2)
            endif
          endif
          go to 3050
3200      if(MovieFileName.ne.' ') then
            TextInfo(1)='The movie maps were recorded to files: '//
     1        MovieFileName(:idel(MovieFileName))//'_####'//
     2        HCExtension(HardCopyOld)(:idel(HCExtension(HardCopyOld)))
            NInfo=1
            WaitTime=10000
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeQuestLblOff(nLblMovie)
        else if(CheckNumber.eq.nButtContours) then
          call DefContour(ich)
          if(ich.eq.0) call GrtDrawMap(0)
        else if(CheckNumber.eq.nButtOptions) then
          call GrtReadOptions(ich)
          if(ich.eq.0.and.GraphExists)
     1      call GrtDrawCurveOrMap(0,1,GraphExists)
        else if(CheckNumber.eq.nButtCurves) then
          call PCurves(0)
        endif
        call FeQuestButtonOff(CheckNumber)
        go to 2000
      else
        go to 2090
      endif
      go to 2000
9999  if(NDim(KPhase).gt.5) close(l80,status='delete')
      return
      end
