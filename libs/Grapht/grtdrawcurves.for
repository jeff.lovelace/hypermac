      subroutine GrtDrawCurves(Klic,jmap,isw)
      use Basic_mod
      use Grapht_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension tp(3),xp(3),xo(3),Gamma(9)
      integer Color
      character*8 YLabel,LabelP
      character*80 Veta
      logical Prazdno,FileAlreadyOpened
      save Prazdno
      ich=0
      if(Klic.ne.0) go to 2000
      call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      if(HardCopy.ne.0) then
        if(HardCopy.lt.100.or.jmap.eq.1) then
          call FeHardCopy(HardCopy,'open')
          if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                            HardCopy.ne.HardCopyPCX)) then
            if(HardCopy.eq.HardCopyNum+100) then
              if(.not.FileAlreadyOpened(HCFileName)) then
                call OpenFile(85,HCFileName,'formatted','unknown')
                if(ErrFlag.ne.0) go to 9999
              endif
            endif
          endif
        endif
      else
        HardCopy=HardCopyNum
        open(85,file=fln(:ifln)//'.l66')
      endif
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeOutSt(0,XCenGrWin,(YMaxGrWin+YMaxAcWin)*.5,GraphtLabel,
     1               'C',White)
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    write(85,'(''# '',a)') GraphtLabel(:idel(GraphtLabel))
        call FeMakeAcFrame
        if(GrtDrawX4) then
          Veta=smbx6(ior(1)+3)
        else
          Veta=smbt(ior(1))
        endif
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
        if((HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1     .and.DrawN.gt.0) then
          NPar=DrawParam(1)/10
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
             if(NPar.eq.IdDrawDist) then
               Veta='distance'
             else
               Veta='angle'
             endif
             Veta='# '//Veta(:idel(Veta))//'-t plot for '//
     1            DrawAtom(1,1)(:idel(DrawAtom(1,1)))
             write(85,FormA) Veta(:idel(Veta))
          endif
        endif
        Prazdno=.true.
      endif
2000  YLabel=' '
      do 3000i=1,DrawN
        do j=1,DrawPoints
          if(DrawOcc(j,i).gt.OccLimit.and.DrawValue(j,i).ge.yomn
     1                               .and.DrawValue(j,i).lt.yomx)
     2      go to 2020
        enddo
        go to 3000
2020    NPar=DrawParam(i)/10
        IPar=max(mod(DrawParam(i),10),1)
        jc=DrawColor(i)
        Color=ColorNumbers(iabs(jc))
        if(jc.gt.0) then
          LineTypeDraw=NormalLine
        else
          LineTypeDraw=DashedLine
        endif
        if(NPar.eq.IdDrawOcc) then
          LabelP='occ'
        else if(NPar.eq.IdDrawXYZ) then
          LabelP=smbx(IPar)
        else if(NPar.eq.IdDrawDeltaXYZ) then
          LabelP='d'//smbx(IPar)
        else if(NPar.eq.IdDrawADP) then
          LabelP=SmbU(IPar)
        else if(NPar.eq.IdDrawTorsion) then
          LabelP='Tors'
        else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
          LabelP='Dist'
        else if(NPar.eq.IdDrawVal) then
          LabelP='Valence'
        else if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     1          NPar.eq.IdDrawIndAng) then
          LabelP='Angle'
        else if(NPar.eq.IdMagMoment) then
          if(IPar.le.3) then
            LabelP='M'//smbx(IPar)
          else
            LabelP='|M|'
          endif
        endif
        if(YLabel.eq.' ') then
          YLabel=LabelP
        else if(YLabel.ne.LabelP) then
          if(NPar.eq.IdDrawXYZ) then
            YLabel='xyz'
          else if(NPar.eq.IdDrawDeltaXYZ) then
            YLabel='dxyz'
          else if(NPar.eq.IdDrawADP) then
            YLabel='U'
          endif
        endif
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    then
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            Veta='# '//LabelP(:idel(LabelP))//'-'//
     1          smbt(ior(1))//' plot for '//
     2          DrawAtom(1,i)(:idel(DrawAtom(1,i)))
            write(85,FormA) Veta(:idel(Veta))
          else if(NPar.eq.IdDrawVal) then
            Veta='# '//LabelP(:idel(LabelP))//'-'//
     1          smbt(ior(1))//' plot for '//
     2          DrawAtom(1,i)(:idel(DrawAtom(1,i)))
            write(85,FormA) Veta(:idel(Veta))
            if(jc.gt.0) then
              write(85,'(''# curve includes modulation'')')
            else
              write(85,'(''# curve doesn''''t include modulation'')')
            endif
          else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
            Veta='# to '//DrawSymSt(i)(:idel(DrawSymSt(i)))
            if(NPar.eq.IdDrawAngles)
     1        Veta=Veta(:idel(Veta))//' and '//
     2             DrawSymStU(i)(:idel(DrawSymStU(i)))
            write(85,FormA) Veta(:idel(Veta))
            if(jc.gt.0) then
              write(85,'(''# curve includes modulation'')')
            else
              write(85,'(''# curve doesn''''t include modulation'')')
            endif
          endif
          write(85,'(''# Color : '',i15,i5)') Color,LineTypeDraw
        endif
        call GrtDrawPerPartes(DrawT,DrawValue(1,i),DrawOcc(1,i),
     1                        DrawPoints,LineTypeDraw,NormalPlotMode,
     2                        Color)
        Prazdno=.false.
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    write(85,'(''# end of curve'')')
3000  continue
      if(DrawTCommen) then
        n=NCommQ(1,isw,KPhase)*(ngc(KPhase)+1)
        fn=1./float(n)
        xp(3)=0.
        call FeLineType(DashedLine)
        do k=0,0
          if(k.eq.0) then
            call CopyVek(trez(1,isw,KPhase),tp,NDimI(KPhase))
          else
            do j=4,NDim(KPhase)
              call CopyVek(rm6gc(4+(j-1)*NDim(KPhase),k,1,KPhase),
     1                     Gamma(1+(j-4)*NDimI(KPhase)),NDimI(KPhase))
            enddo
            call Multm(Gamma,trez(1,1,KPhase),tp,NDimI(KPhase),
     1                 NDimI(KPhase),1)
            call AddVek(tp,s6gc(4,k,1,KPhase),tp,NDimI(KPhase))
            tp(1)=tp(1)-scalmul(s6gc(1,k,1,KPhase),qu(1,1,1,KPhase))
          endif
          pom=tp(1)
          do i=1,n
            xp(1)=pom
3100        if(xp(1).gt.xomn) then
              xp(1)=xp(1)-1.
              go to 3100
            endif
3200        if(xp(1).lt.xomx) then
              if(xp(1).ge.xomn) then
                xp(2)=yomn
                call multm(F2O,xp,xo,3,3,1)
                xu(1)=FeXo2X(xo(1))
                yu(1)=FeYo2Y(xo(2))
                xp(2)=yomx
                call multm(F2O,xp,xo,3,3,1)
                xu(2)=FeXo2X(xo(1))
                yu(2)=FeYo2Y(xo(2))
                call FePolyLine(2,xu,yu,White)
              endif
              xp(1)=xp(1)+1.
              go to 3200
            endif
            pom=pom+fn
          enddo
        enddo
        call FeLineType(NormalLine)
      endif
      if(Klic.eq.0) call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,YLabel)
9999  return
      end
