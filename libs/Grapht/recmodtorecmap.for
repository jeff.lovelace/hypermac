      integer function RecModToRecMap(k,nx,ior,n)
      dimension ior(n),nx(n),nd(3)
      call RecUnpack(k,nd,nx,n)
      RecModToRecMap=0
      do i=n,1,-1
        j=ior(i)
        RecModToRecMap=RecModToRecMap*nx(j)+nd(j)-1
      enddo
      RecModToRecMap=RecModToRecMap+1
      return
      end
