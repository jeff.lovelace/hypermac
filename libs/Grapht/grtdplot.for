      subroutine GrtDPlot
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*256 Veta
      character*80 t80
      real :: xp(2)
      real, allocatable :: xmn(:),xmx(:)
      integer, allocatable :: npt(:)
      logical EqIgCase
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l66','formatted','old')
      n=0
1100  read(ln,FormA,end=1150) Veta
      if(EqIgCase(Veta,'# end of curve')) n=n+1
      go to 1100
1150  allocate(xmn(n),xmx(n),npt(n))
      rewind ln
      i=1
      npt(1)=0
1200  read(ln,FormA,end=1250) Veta
      if(EqIgCase(Veta,'# end of curve')) then
        i=i+1
        if(i.gt.n) go to 1250
        npt(i)=0
      endif
      if(Veta(1:1).eq.'#') go to 1200
      npt(i)=npt(i)+1
      k=0
      call StToReal(Veta,k,xp,2,.false.,ich)
      if(ich.eq.0) then
        if(npt(i).eq.1) xmn(i)=xp(1)
        xmx(i)=xp(1)
      endif
      go to 1200
1250  rewind ln
      lni=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'DPlot'//ObrLom//
     1      'Grapht_template.grf'
      else
        Veta=HomeDir(:idel(HomeDir))//'dplot/Grapht_template.grf'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_tmp.grf','formatted','unknown')
1260  read(lni,FormA) Veta
      if(.not.EqIgCase(Veta,'#$%place to insert data#$%')) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1260
      endif
      write(Cislo,FormI15) n
      call Zhusti(Cislo)
      write(lno,FormA) Cislo(:idel(Cislo))
      i=1
1300  write(Cislo,FormI15) npt(i)
      call Zhusti(Cislo)
      write(lno,FormA) Cislo(:idel(Cislo))
      m=0
      t80=' '
1320  read(ln,FormA,end=1250) Veta
      if(EqIgCase(Veta,'# end of curve')) then
        if(m.ne.0) write(lno,FormA) t80(:idel(t80))
        write(t80,100) xmn(i),xmx(i)
        call Zhusti(t80)
        write(lno,FormA) t80(:idel(t80))
        write(lno,'(i2,i5)') 1,0
        write(lno,FormA)
        write(lno,FormA)
        i=i+1
        if(i.gt.n) go to 2000
        go to 1300
      endif
      if(Veta(1:1).eq.'#') go to 1320
      k=0
      call StToReal(Veta,k,xp,2,.false.,ich)
      write(Cislo,'(f15.6)') xp(2)
      call ZdrcniCisla(Cislo,1)
      m=m+1
      if(t80.eq.' ') then
        t80=Cislo
      else
        t80=t80(:idel(t80))//','//Cislo(:idel(Cislo))
      endif
      if(m.ge.5) then
        write(lno,FormA) t80(:idel(t80))
        m=0
        t80=' '
      endif
      go to 1320
2000  read(lni,FormA,end=2100) Veta
      if(Veta(1:3).eq.'###') then
        t80=Veta(4:idel(Veta)-3)
        idl=idel(t80)
        Veta=t80
        do i=2,n
          j=idel(Veta)
          if(j+idl.gt.110) then
            write(lno,FormA) Veta(:idel(Veta))
            Veta=t80
          else
            Veta=Veta(:j)//','//t80(:idl)
          endif
        enddo
      endif
      if(EqIgCase(Veta,'#$%x,y-min#$%')) then
        write(Veta,100) xomn,yomn
        call Zhusti(Veta)
      else if(EqIgCase(Veta,'#$%x,y-max#$%')) then
        write(Veta,100) xomx,yomx
        call Zhusti(Veta)
      endif
      write(lno,FormA) Veta(:idel(Veta))
      go to 2000
2100  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call CloseIfOpened(ln)
      Veta='"'//CallDPlot(:idel(CallDPlot))//'" '//
     1     fln(:ifln)//'_tmp.grf'
      call FeSystem(Veta)
      if(allocated(xmn)) deallocate(xmn,xmx,npt)
      return
100   format(f15.4,',',f15.4)
      end
