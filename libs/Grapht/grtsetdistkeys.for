      subroutine GrtSetDistKeys(NPar,xpp)
      use Dist_mod
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension xpp(3)
      logical ExistFile
      call TrOrthoS(1)
      call DefaultDist
      call NactiDist
      call CloseIfOpened(55)
      if(ExistFile(fln(:ifln)//'_torpl.tmp'))
     1  call Deletefile(fln(:ifln)//'_torpl.tmp')
      call SetRealArrayTo(dt,9,0.)
      call SetRealArrayTo(tfirst,9,0.)
      call SetRealArrayTo(tlast,9,1.)
      call SetIntArrayTo(nt,9,1)
      call CopyVek(xpp,tfirst,NDimI(KPhase))
      call CopyVek(xpp,tlast,NDimI(KPhase))
      call CopyVekI(NxMod,nt,NDimI(KPhase))
      call CopyVek (DxMod,dt,NDimI(KPhase))
      if(NPar.eq.IdDrawDist) then
        dmdk=0.
        if(WhatToDraw.eq.0) then
          dmhk=yomx
        else
          dmhk=DrawDMax
        endif
        iuhl=0
      else if(NPar.eq.IdDrawVal) then
        dmdk=0.
        dmhk=DrawDMax
        iuhl=0
      else if(NPar.eq.IdDrawAngles) then
        dmdk=0.
        dmhk=DrawDMax
        iuhl=1
      endif
      call SetRealArrayTo(dmh,NAtFormula(KPhase),dmhk)
      do i=1,NDimI(KPhase)
        do j=2,NComp(KPhase)
          nt(i,j)=nt(i,1)
          tfirst(i,j)=tfirst(i,1)
          tlast(i,j)=tlast(i,1)
          dt(i,j)=dt(i,1)
        enddo
      enddo
      do j=1,NComp(KPhase)
        ntall(j)=1
        do i=1,NDimI(KPhase)
          ntall(j)=ntall(j)*nt(i,j)
        enddo
      enddo
      ieach=1
      fullcoo=1
      VolaToDist=.false.
      call inm40(GrtESD)
      return
      end
