      subroutine GraphMapa
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      integer Color,ColorOrder
      dimension xs(4),xp(4),xpoint(6),x2(6),x3(6),x4dif(3),tdif(3)
     1         ,GammaIntInv(9),uxi(3,mxw),uyi(3,mxw),xpp(4),x40(3),
     2          dx4(3),nx4(3),occ(1),smp(36),rmp(9),rm6p(36),occp(1)
      logical First,CrwLogicQuest,PopsatOsy,DrawPoints,ExistFile,
     1        DrawLattice,DrawTCuts
      character*80 CoorLabel,t80
      character*12 Labels(11)
      data First/.true./,ix/1/
      data Labels/'%Quit','%Print','%Save','New %map','%Atoms',
     1            'x4-%length','T%ips on','La%ttice on','Reverse x%4',
     2            't-c%uts on','P%oints %on'/
      data PopsatOsy,DrawLattice,DrawTCuts,DrawPoints/4*.false./
      Tiskne=.false.
      ln=0
      call TrOrtho(0)
      if(First) then
        ReverseX4=.true.
        ee(1)=10.
        nsubs=1
        First=.false.
        call SetRealArrayTo(xmin,4,0.)
        call SetRealArrayTo(xmax,4,1.)
        call SetRealArrayTo(dx,4,.1)
        call SetIntArrayTo(nx,4,0)
        obecny=.false.
        tmapy=.false.
        xymap=.false.
        cx(2)='x4'
        iorien(2)=4
      endif
      call DefaultContour
      call NactiContour
      call ConReadKeys
      MapExists=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      j=9
      if(kcommen(KPhase).gt.0) then
        j=j+2
      else
        nButtDrawTCuts=0
        nButtDrawPoints=0
      endif
      do i=1,j
        if(i.eq.7) then
          if(PopsatOsy) then
            t80='T%ips off'
          else
            t80='T%ips on'
          endif
        else if(i.eq.8) then
          if(DrawLattice) then
            t80='La%ttice off'
          else
            t80='La%ttice on'
          endif
        else if(i.eq.10) then
          if(DrawTCuts) then
            t80='t-c%uts off'
          else
            t80='t-c%uts on'
          endif
        else if(i.eq.11) then
          if(DrawPoints) then
            t80='P%oints off'
          else
            t80='P%oints on'
          endif
        else
          t80=Labels(i)
        endif
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,t80)
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
        else if(i.eq.5) then
          nButtAtoms=ButtonLastMade
        else if(i.eq.6) then
          nButtX4Length=ButtonLastMade
        else if(i.eq.7) then
          nButtPopsatOsy=ButtonLastMade
        else if(i.eq.8) then
          nButtDrawLattice=ButtonLastMade
        else if(i.eq.9) then
          nButtReverseX4=ButtonLastMade
        else if(i.eq.10) then
          nButtDrawTCuts=ButtonLastMade
        else if(i.eq.11) then
          nButtDrawPoints=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.3.or.i.eq.5.or.i.eq.j) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      CheckType=EventButton
      CheckNumber=nButtNew
      go to 3010
1100  if(kcommen(KPhase).gt.0) then
        t0=trez(1,nsubs,KPhase)
        dtp=1./float(NCommQ(1,nsubs,KPhase)*NCommQ(2,nsubs,KPhase)*
     1               NCommQ(3,nsubs,KPhase))
      endif
      if(xmax(1)-xmin(1).gt.1.1.or.xmax(2)-xmin(2).gt.1.1) then
        call FeQuestButtonOff(nButtDrawLattice)
      else
        DrawLattice=.false.
        call FeQuestButtonLabelChange(nButtDrawLattice,'La%ttice on')
        call FeQuestButtonDisable(nButtDrawLattice)
      endif
      if(kcommen(KPhase).gt.0) then
        tmin=xmin(2)-max(xmin(1)*qu(ix,1,nsubs,KPhase),
     1                   xmax(1)*qu(ix,1,nsubs,KPhase))
        pom=(tmin-t0)/dtp
        ntmin=pom
        if(pom.gt.0.) ntmin=ntmin+1
        tmax=xmax(2)-min(xmin(1)*qu(ix,1,nsubs,KPhase),
     1                   xmax(1)*qu(ix,1,nsubs,KPhase))
        pom=(tmax-t0)/dtp
        ntmax=pom
        if(pom.lt.0.) ntmax=ntmax-1
      endif
      j=2
      do i=1,3
        if(i.eq.ix) then
          k=1
        else
          j=j+1
          k=j
        endif
        iorien(k)=i
        write(cx(k),'(''x'',i1)') i
      enddo
      call SetTr
1200  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeMakeAcFrame
        if(PopsatOsy) then
          call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
          call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
        endif
        if(DrawLattice) then
          ixp=xmin(1)
          ixk=xmax(1)
          do i=ixp,ixk
            xpom=i
            if(xpom.gt.xmin(1).and.xpom.lt.xmax(1)) then
              xp(1)=i
              xp(2)=xmin(2)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(1)=xo(1)
              yu(1)=xo(2)
              xp(2)=xmax(2)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(2)=xo(1)
              yu(2)=xo(2)
              call FePolyLine(2,xu,yu,White)
            endif
          enddo
          iyp=xmin(2)
          iyk=xmax(2)
          do i=iyp,iyk
            ypom=i
            if(ypom.gt.xmin(2).and.ypom.lt.xmax(2)) then
              xp(2)=i
              xp(1)=xmin(1)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(1)=xo(1)
              yu(1)=xo(2)
              xp(1)=xmax(1)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(2)=xo(1)
              yu(2)=xo(2)
              call FePolyLine(2,xu,yu,White)
            endif
          enddo
        endif
        if(DrawTCuts) then
          do i=ntmin,ntmax
            t=t0+float(i)*dtp
            x1(1)=xmin(1)
            x1(2)=t+xmin(1)*qu(ix,1,nsubs,KPhase)
            x1(3)=0.
            x2(1)=xmax(1)
            x2(2)=t+xmax(1)*qu(ix,1,nsubs,KPhase)
            x2(3)=0.
            if(x2(2).gt.xmax(2)) then
              x2(1)=x1(1)+(xmax(2)-x1(2))/(x2(2)-x1(2))*(x2(1)-x1(1))
              x2(2)=xmax(2)
            endif
            if(x1(2).lt.xmin(2)) then
              x1(1)=x2(1)+(xmin(2)-x2(2))/(x1(2)-x2(2))*(x1(1)-x2(1))
              x1(2)=xmin(2)
            endif
            call FeXf2X(x1,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)
            call FeXf2X(x2,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
          enddo
          call FeLineType(NormalLine)
        endif
        call SetRealArrayTo(xp,3,0.)
        call ConDrawAtoms(xp)
        if(DrawPoints) then
          if(ExistFile(fln(:ifln)//'.xyz')) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'.xyz','formatted','old')
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 1600
            endif
1400        read(ln,FormA,end=1600) t80
            if(t80.eq.' ') go to 1600
            if(t80(1:1).eq.'#') go to 1400
            k=0
            call Kus(t80,k,Cislo)
            call StToReal(t80,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 1600
            call Multm(RCommen(1,1,KPhase),xp,xpoint,3,3,1)
            call qbyx(xpoint,xpoint(4),1)
            do m=4,NDim(KPhase)
              xpoint(m)=xpoint(m)+trez(m-3,1,KPhase)
            enddo
            Radius=3.
            if(k.lt.80) then
              call StToReal(t80,k,xp,1,.false.,ich)
              if(ich.ne.0) go to 1500
              Radius=xp(1)
            endif
            if(k.lt.80) then
              call Kus(t80,k,Cislo)
              Color=ColorNumbers(ColorOrder(Cislo))
            endif
1500        do isym=1,NSymmN(KPhase)
              call multm(rm6(1,isym,1,KPhase),xpoint,x1,NDim(KPhase),
     1                   NDim(KPhase),1)
              do m=1,NDim(KPhase)
                x2(m)=x1(m)+s6(m,isym,1,KPhase)
              enddo
              do icenter=1,NLattVec(KPhase)
                do m=1,NDim(KPhase)
                  x3(m)=x2(m)+vt6(m,icenter,1,KPhase)
                enddo
                call od0do1(x3,x3,NDim(KPhase))
                xp(1)=x3(ix)
                xp(2)=x3(4)
                xp(3)=0.
                call FeXf2X(xp,xo)
                xm=xo(1)
                ym=xo(2)
                MarkerType=-1
                if(Radius.lt.1.2) MarkerType=-MarkerType
                call FeMarker(xm,ym,Radius,MarkerType,Color)
              enddo
            enddo
            go to 1400
1600        if(ln.ne.0) call CloseIfOpened(ln)
          else
            do 1800i=1,DrawAtN
              if(DrawAtSkip(i)) go to 1800
              call atsymi(DrawAtName(i),ia,xs,x4dif,tdif,isym,ich,*1800)
              if(DrawAtColor(i).eq.0) then
                Color=AtColor(isf(ia),KPhase)
              else
                Color=ColorNumbers(DrawAtColor(i))
              endif
              isw=ISwSymm(isym,iswa(ia),KPhase)
              call DistSetComp(isw,NSubs)
              if(isw.eq.iswa(ia)) then
                call CopyMat(rm (1,isym,isw,KPhase),rmp ,3)
                call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
              else
                call multm(zv(1,isw,KPhase),zvi(1,iswa(ia),KPhase),smp,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call multm(smp,rm6(1,isym,iswa(ia),KPhase),rm6p,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call MatBlock3(rm6p,rmp,NDim(KPhase))
              endif
              ksw=kswa(ia)
              call GetGammaIntInv(rm6p,GammaIntInv)
              do l=1,KModA(2,ia)
                call multm(rmp,ux(1,l,ia),uxi(1,l),3,3,1)
                if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
                  call multm(rmp,uy(1,l,ia),uyi(1,l),3,3,1)
                else
                  call CopyVek(uy(1,l,ia),uyi(1,l),3)
                endif
              enddo
              do ntp=-100,100
                t=t0+float(ntp)*dtp
                xpp(1)=TNaT(1)*t
                call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
                call AddVek(tdif,xpp,xpp,NDimI(KPhase))
                call multm(GammaIntInv,xpp,xp,NDimI(KPhase),
     1                     NDimI(KPhase),1)
                nx4(1)=1
                dx4(1)=0.
                if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
                  call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1                            ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                            GammaIntInv)
                  if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                    call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1                uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2                GammaIntInv)
                    if(occp(1).le.0.) occ(1)=0.
                  endif
                else
                  call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                               ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                               ax(KModA(1,ia),ia),a0(ia)*.5,
     3                               GammaIntInv,TypeModFun(ia))
                endif
                if(TypeModFun(ia).le.1) then
                  call MakePosMod(xpp,x40,xp,nx4,dx4,uxi,uyi,
     1                            KModA(2,ia),KFA(2,ia),GammaIntInv)
                else
                  call MakePosModPol(xpp,x40,xp,nx4,dx4,uxi,uyi,
     1                               KModA(2,ia),ax(KModA(1,ia),ia),
     2                               a0(ia)*.5,GammaIntInv,
     3                               TypeModFun(ia))
                endif
                if(occ(1).lt..1) cycle
                call AddVek(xs,xpp,xp,3)
                call qbyx(xp,xp(4),isw)
                xp(4)=xp(4)+TNaT(1)*t
                call multm(WPrI,xp,xpp,NDim(KPhase),NDim(KPhase),1)
                xpp(1)=xpp(ix)
                xpp(2)=xpp(4)
                xpp(3)=0.
                if(xpp(1).lt.xmin(1).or.xpp(1).gt.xmax(1).or.
     1             xpp(2).lt.xmin(2).or.xpp(2).gt.xmax(2)) cycle
                call FeXf2X(xpp,xo)
                call FeCircle(xo(1),xo(2),3.,Color)
              enddo
1800        continue
          endif
        endif
        MapExists=.true.
      endif
      call FeHardCopy(HardCopy,'close')
2900  HardCopy=0
3000  call FeQuestEvent(id,ich)
3010  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          call FeQuestRemove(id)
          go to 9999
        else if(CheckNumber.eq.nButtNew) then
          xpomt=120.
          xd=250.
          dpom=xd-xpomt-15.
          tpom=5.
          i=3
          if(NComp(KPhase).gt.1) i=i+1
          idp=NextQuestId()
          t80='Coordinate to draw'
          call FeQuestCreate(idp,-1.,-1.,xd,i,t80,0,LightGray,0,0)
          il=1
          xpom=(xd-CrwgXd)*.5-xd*.25
          do i=1,3
            call FeQuestCrwMake(idp,xpom,il,xpom+5.,il,'%'//smbx(i),'C',
     1                          CrwgXd,CrwgYd,1,1)
            if(i.eq.1) nCrwX=CrwLastMade
            call FeQuestCrwOpen(i,i.eq.ix)
            xpom=xpom+xd*.25
          enddo
          write(CoorLabel,100) ix,ix
          il=il+1
          call FeQuestEdwMake(idp,tpom,il,xpomt,il,CoorLabel,'L',dpom,
     1                        EdwYd,0)
          nedwLim3=EdwLastMade
          xp(1)=xmin(1)
          xp(2)=xmax(1)
          call FeQuestRealAEdwOpen(nedwLim3,xp,2,.false.,.false.)
          il=il+1
          call FeQuestEdwMake(idp,tpom,il,xpomt,il,'x%4min,x4max','L',
     1                        dpom,EdwYd,0)
          nedwLim4=EdwLastMade
          xp(1)=xmin(2)
          xp(2)=xmax(2)
          call FeQuestRealAEdwOpen(nedwLim4,xp,2,.false.,.false.)
          if(NComp(KPhase).gt.1) then
            il=il+1
            dpom=50.
            call FeQuestEdwMake(idp,tpom,il,xpomt,il,
     1                          '%Subsystem number','L',dpom,EdwYd,0)
            nedwSubs=EdwLastMade
            call FeQuestIntEdwOpen(nedwSubs,nsubs,.false.)
          endif
3500      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventCrw) then
            do i=nCrwX,nCrwX+2
              if(CrwLogicQuest(i)) then
                if(ix.ne.i) then
                  ix=i
                  write(CoorLabel,100) ix,ix
                  call FeQuestEdwLabelChange(nEdwLim3,CoorLabel)
                endif
                EventType=EventEdw
                EventNumber=nEdwLim3
                go to 3500
              endif
            enddo
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          if(ich.eq.0) then
            call FeQuestRealAFromEdw(nedwLim3,xp)
            xmin(1)=xp(1)
            xmax(1)=xp(2)
            call FeQuestRealAFromEdw(nedwLim4,xp)
            xmin(2)=xp(1)
            xmax(2)=xp(2)
            if(NComp(KPhase).gt.1)
     1        call FeQuestIntFromEdw(nedwSubs,nsubs)
          endif
          call FeQuestRemove(idp)
          if(ich.ne.0) then
            go to 3000
          else if(kcommen(KPhase).gt.0) then
            do i=1,3
              if(i.ne.ix) then
                if(qu(i,1,nsubs,KPhase).ne.0.) then
                  call FeQuestButtonDisable(nButtDrawTCuts)
                  go to 1100
                endif
              endif
            enddo
            call FeQuestButtonOff(nButtDrawTCuts)
          endif
          go to 1100
        else if(CheckNumber.eq.nButtAtoms) then
          call ConDrawAtomsEdit
          go to 1100
        else if(CheckNumber.eq.nButtX4Length) then
          idp=NextQuestId()
          call FeQuestCreate(idp,-1.,-1.,xd,1,' ',0,LightGray,0,0)
          tpom=15.
          dpom=150.
          t80='%Length of 4th vector'
          xpomt=tpom+FeTxLengthUnder(t80)+10.
          dpom=xd-xpomt-15.
          call FeQuestEdwMake(idp,tpom,1,xpomt,1,t80,'L',dpom,EdwYd,0)
          nedw4=EdwLastMade
          call FeQuestRealEdwOpen(nedw4,ee(1),.false.,.false.)
3700      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3700
          endif
          if(ich.eq.0) call FeQuestRealFromEdw(nedw4,ee(1))
          call FeQuestRemove(idp)
          go to 1100
        else if(CheckNumber.eq.nButtPopsatOsy) then
          PopsatOsy=.not.PopsatOsy
          if(PopsatOsy) then
            t80='T%ips off'
          else
            t80='T%ips on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtDrawLattice) then
          DrawLattice=.not.DrawLattice
          if(DrawLattice) then
            t80='La%ttice off'
          else
            t80='La%ttice on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtReverseX4) then
          ReverseX4=.not.ReverseX4
          go to 1100
        else if(CheckNumber.eq.nButtDrawTCuts) then
          DrawTCuts=.not.DrawTCuts
          if(DrawTCuts) then
            t80='t-c%uts off'
          else
            t80='t-c%uts on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtDrawPoints) then
          DrawPoints=.not.DrawPoints
          if(DrawPoints) then
            t80='P%oints off'
          else
            t80='P%oints on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtSave)
     1    then
          HardCopy=0
          if(CheckNumber.eq.nButtPrint) then
            call FePrintPicture(ich)
            if(ich.ne.0) HardCopy=0
          else
            call FeSavePicture('graph',6,0)
          endif
          if(HardCopy.eq.0) go to 3000
        endif
        go to 1200
3900    call FeQuestButtonLabelChange(CheckNumber,t80)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3000
      endif
9999  if(NDim(KPhase).gt.3) call TrOrtho(1)
      call ConWriteKeys
      return
100   format('x%',i1,'min,x',i1,'max')
      end
