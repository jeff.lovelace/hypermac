      subroutine GrtDefMovie(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      character*80 Veta
      character*10 Label
      integer EdwStateQuest
      logical CrwLogicQuest
      data Label/'Record'/
      MovieFileName=' '
      il=NDim(KPhase)+8
      id=NextQuestId()
      xqd=250.
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Define parameters for movie',0,LightGray,0,0)
      il=1
      tpom=50.
      call FeQuestLblMake(id,tpom,il,'Start at','C','N')
      tpom=tpom+120.
      call FeQuestLblMake(id,tpom,il,'End at','C','N')
      tpom=5.
      xpom=30.
      dpom=80.
      call SetIntArrayTo(nxMovieFr,2,1)
      call CopyVekI(NxMod(2),nxMovieTo,2)
      do i=ndGrt+1,NDimI(KPhase)
        j=ior(i)
        il=il+1
        call FeQuestEudMake(id,tpom,il,xpom,il,smbt(j),'L',dpom,EdwYd,0)
        if(i.eq.ndGrt+1) nEdwLimFr=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,gx(1,j),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
        call FeQuestEudMake(id,tpom,il,xpom+dpom+40.,il,' ','L',dpom,
     1                      EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,gx(2,j),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
      enddo
      nEdwLimTo=EdwLastMade
      il=il+1
      Veta='%Delay time in sec'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDelayTime=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelayTimeForMovie,.false.,
     1                        .false.)
      il=il+1
      Veta='%Repeat factor'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRepeat=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,MovieRepeat,.false.)
      nEdwRepeat=EdwLastMade
      call FeQuestEudOpen(EdwLastMade,0,9999,1,1.,1.,1.)
      il=il+1
      Veta='Record the sequence of %maps'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwRecord=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,MovieFileName.ne.' ')
      il=il+1
      xpom=tpom+3.+FeTxLengthUnder(Veta)
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      Veta='%Name radix'
      dpom=110.
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+10.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      il=il+1
      ilp=il
      tpom=FeTxLength(Label)+7.
      xpom=xqd-CrwgXd-100.
      do i=1,5
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      HCMenu(i)(:idel(HCMenu(i)))//'s','L',CrwgXd,
     2                      CrwgYd,0,1)
        if(i.eq.1) nCrwTypeFirst=CrwLastMade
        il=il+1
      enddo
      call FeQuestLblMake(id,5.,ilp,Label,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLbl=LblLastMade
1400  if(CrwLogicQuest(nCrwRecord)) then
        if(EdwStateQuest(nEdwFileName).ne.EdwOpened) then
          HardCopy=HardCopyPCX
          call FeQuestStringEdwOpen(nEdwFileName,MovieFileName)
          call FeQuestButtonOpen(nButtBrowse,ButtonOff)
          call FeQuestLblOn(nLbl)
          nCrw=nCrwTypeFirst
          do i=1,5
            call FeQuestCrwOpen(nCrw,i.eq.HardCopy)
            nCrw=nCrw+1
          enddo
          EventType=EventEdw
          EventNumber=nEdwFileName
        endif
      else
        HardCopy=0
        call FeQuestEdwClose(nEdwFileName)
        call FeQuestButtonClose(nButtBrowse)
        nCrw=nCrwTypeFirst
        do i=1,5
          call FeQuestCrwClose(nCrw)
          nCrw=nCrw+1
        enddo
        call FeQuestLblOff(nLbl)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRecord) then
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        call FeFileManager('Select file to derive the radix',Veta,'*.*',
     1                     0,.true.,ich)
        EventType=EventEdw
        EventNumber=nEdwFileName
        if(ich.eq.0.and.Veta.ne.' ') then
          i=idel(Veta)
1520      if(Veta(i:i).ne.'.') then
            i=i-1
            if(i.gt.1) then
              go to 1520
            else
              go to 1540
            endif
          endif
          Veta(i:)=' '
          i=i-1
1530      if(index(Cifry(:10),Veta(i:i)).gt.0) then
            i=i-1
            if(i.gt.1) then
              go to 1530
            else
              go to 1540
            endif
          endif
          if(Veta(i:i).eq.'_') Veta(i:)=' '
1540      call FeQuestStringEdwOpen(nEdwFileName,Veta)
        endif
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwDelayTime,DelayTimeForMovie)
        call FeQuestIntFromEdw(nEdwRepeat,MovieRepeat)
        i=1
        do nEdw=nEdwLimFr,nEdwLimTo
          k=(i-1)/2+1
          j=ior(k)
          call FeQuestRealFromEdw(nEdw,pom)
          if(gx(3,j).ne.0.) then
            n=nint((pom-gx(1,j))/gx(3,j))+1
          else
            n=1
          endif
          if(mod(i,2).eq.1) then
            nxMovieFr(k)=n
          else
            nxMovieTo(k)=n
          endif
          i=i+1
        enddo
        if(CrwLogicQuest(nCrwRecord)) then
          MovieFileName=EdwStringQuest(nEdwFileName)
          if(MovieFileName.eq.' ') then
            call FeChybne(-1.,-1.,'the file prefix not defined.',' ',
     1                    SeriousError)
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            go to 1400
          endif
          nCrw=nCrwTypeFirst
          do i=1,5
            if(CrwLogicQuest(nCrw)) then
              HardCopy=i
              go to 5000
            endif
            nCrw=nCrw+1
          enddo
        else
          MovieFileName=' '
          HardCopy=0
        endif
      endif
5000  call FeQuestRemove(id)
      return
      end
