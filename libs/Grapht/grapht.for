      subroutine Grapht
      use Contour_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension sngc(:,:,:),csgc(:,:,:)
      logical iesd
      allocatable sngc,csgc
      if(allocated(xdst)) deallocate(xdst,BetaDst,dxm,BratPrvni,
     1                 BratDruhyATreti,aselPrvni,aselDruhyATreti)
      n=NAtCalc+5
      allocate(xdst(3,n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call SetLogicalArrayTo(BratDruhyATreti,n,.true.)
      nt=0
      do i=1,NDimI(KPhase)
        do j=1,NComp(KPhase)
          nt(i,j)=1
        enddo
      enddo
      do j=1,NComp(KPhase)
        ntall(j)=1
        do i=1,NDimI(KPhase)
          ntall(j)=ntall(j)*nt(i,j)
        enddo
      enddo
      call inm40(iesd)
      call GrtRightButtons
      if(KCommenMax.gt.0) call DeleteFile(PreviousM50)
      call DeleteFile(PreviousM40)
      call CloseIfOpened(l80)
      call DeleteFile(fln(:ifln)//'.l80')
9999  if(Allocated(DrawT)) deallocate(DrawT,DrawValue,DrawOcc,DrawOccP,
     1                                XMod)
      if(allocated(ActualMap)) deallocate(ActualMap)
      if(allocated(xdst)) deallocate(xdst,BetaDst,dxm,BratPrvni,
     1                 BratDruhyATreti,aselPrvni,aselDruhyATreti)
      if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                             sxdk)
      if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
      if(allocated(AtomArr)) deallocate(AtomArr)
      NAtomArr=0
      return
      end
