      subroutine FeEdwCtrlCAction(id)
      include 'fepc.cmn'
      character*256 Veta
      Klic=0
      go to 1000
      entry FeEdwCtrlXAction(id)
      Klic=1
1000  n1=min(EdwSelFr,EdwSelTo)
      n2=max(EdwSelFr,EdwSelTo)
      if(n1.ge.n2) go to 9999
      call FeSetClipboardText(EdwString(id)(n1+1:n2))
      if(Klic.eq.0) go to 9999
      if(n1.gt.1) then
        Veta=EdwString(id)(:n1)
        idl=n1
      else
        Veta=' '
        idl=0
      endif
      call FeEdwSelClear(id)
      if(n2.lt.idel(EdwString(id))) then
        if(idl.gt.0) then
          EdwString(id)=Veta(:idl)//EdwString(id)(n2+1:)
        else
          EdwString(id)=EdwString(id)(n2+1:)
        endif
      else
        EdwString(id)=Veta
      endif
      EdwTextLen(id)=idel(EdwString(id))
      EdwKurzor(id)=min(EdwKurzor(id),EdwTextLen(id))
      if(Kurzor.gt.EdwTextMax(id)) then
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      else
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2    EdwTextMax(id))
      endif
9999  return
      end
