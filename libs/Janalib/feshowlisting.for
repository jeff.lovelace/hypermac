      subroutine FeShowListing(xm,ym,WhatEnds,FileLst,WaitTimeIn)
      include 'fepc.cmn'
      character*(*) WhatEnds,FileLst
      character*80 Radka
      integer WaitTimeIn
      logical FeYesNoHeader
      if(.not.AlwaysOpenListing) then
        NInfo=NInfo+1
        TextInfo(NInfo)=' '
        Radka='Regular end of '//WhatEnds(:idel(WhatEnds))
        WaitTime=WaitTimeIn
        if(WaitTime.gt.0) then
          i=max((45-idel(Radka))/2+1,1)
        else
          i=1
        endif
        TextInfo(NInfo)(i:)=Radka
        if(.not.FeYesNoHeader(xm,ym,'Open the listing?',0)) go to 9999
      endif
      call CloseIfOpened(lst)
      if(BuildInViewer) then
        call FeListView(FileLst,0)
      else
        if(OpSystem.le.0) then
          call FeEdit(FileLst)
        endif
      endif
9999  return
      end
