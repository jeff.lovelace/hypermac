      subroutine FeMarker(x,y,Size,MarkerType,Color)
      include 'fepc.cmn'
      integer Color
      if(iabs(MarkerType).eq.1) then
        call FeCircle(x,y,Size,Color)
        if(MarkerType.lt.0) call FeCircle(x,y,Size-1.,Black)
      else if(iabs(MarkerType).eq.2) then
        call FeSquare(x,y,2.*Size,Color)
        if(MarkerType.lt.0) call FeSquare(x,y,2.*(Size-1.),Black)
      else if(iabs(MarkerType).eq.3) then
        call FeTriangl(x,y,2.*Size,Color)
        if(MarkerType.lt.0) call FeTriangl(x,y,2.*(Size-1.),Black)
      endif
      return
      end
