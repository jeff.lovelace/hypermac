      subroutine FeGetVisibleSubstring(String,NStart,NDirection,Length,
     1                                 NFirst,NLast)
      include 'fepc.cmn'
      character*(*) String
      real Length
      if(FeTxLengthSpace(String).le.Length) then
        NFirst=1
        NLast=idel(String)
        go to 9999
      endif
      if(NDirection.gt.0) then
        KFirst=idel(String)
        KLast=NStart
        KDir=-1
      else
        KFirst=1
        KLast=NStart
        KDir=1
      endif
      do k=KFirst,KLast,KDir
        if(NDirection.gt.0) then
          k1=KLast
          k2=k
        else
          k1=k
          k2=KLast
        endif
        if(FeTxLengthSpace(String(k1:k2)).le.Length) then
          NFirst=k1
          NLast=k2
          go to 9999
        endif
      enddo
1200  if(NDirection.lt.0.and.NFirst.le.1) then
        do k=NFirst,idel(String)
          if(FeTxLengthSpace(String(NFirst:k)).le.Length) then
            NLast=k
          else
            go to 9999
          endif
        enddo
      endif
9999  return
      end
