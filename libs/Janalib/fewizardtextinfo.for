      subroutine FeWizardTextInfo(Title,Back,Next,Cancel,ich)
      include 'fepc.cmn'
      character*(*) Title
      integer Back,Next,Cancel
      id=NextQuestId()
      ypom=QuestYLen(id)*.5+float(NInfo)*6.
      if(Title.ne.' ') then
        ypom=ypom+12.
        call FeQuestAbsLblMake(id,WizardLength*.5,ypom,Title,'C','B')
        ypom=ypom-12.
      endif
      tpom=(WizardLength-FeTxLength(TextInfo(1)))*.5
      do i=1,NInfo
        ypom=ypom-12.
        call FeQuestAbsLblMake(id,tpom,ypom,TextInfo(i),'L','N')
      enddo
      if(Back.eq.0)
     1  call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
      if(Next.eq.0) then
        call FeQuestButtonDisable(ButtonOK-ButtonFr+1)
      else if(Next.lt.0) then
        call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
      endif
      if(Cancel.eq.0)
     1  call FeQuestButtonDisable(ButtonCancel-ButtonFr+1)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.gt.0) then
        if(Next.lt.0)
     1    call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
      endif
      return
      end
