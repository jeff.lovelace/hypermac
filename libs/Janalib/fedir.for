      subroutine FeDir(ListDir,ListFile,Filter,Key)
      include 'fepc.cmn'
      character*(*) ListDir,ListFile,Filter
      character*256 Nazev,NazevP
      character*128 WorkFile,WorkFileP
      character*80  t80,TmpFile,FilterArr(:)
      integer FeGetSystemTime,tf
      logical ExistFile,EqWild,Dlouho,EqIgCase
      allocatable FilterArr
      ich=0
      tf=FeGetSystemTime()
      Dlouho=.false.
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      TmpFile='jcmd'
      call CreateTmpFile(TmpFile,i,0)
      call FeTmpFilesAdd(TmpFile)
      if(OpSystem.le.0) then
        call FeDirList(TmpFile)
        if(ErrFlag.ne.0) go to 9999
      else if(OpSystem.eq.1) then
        call FeSystem(ObrLom//'ls -F -x -1 > '//TmpFile)
      endif
      ln1=NextLogicNumber()
      call OpenFile(ln1,TmpFile,'formatted','unknown')
      ln2=NextLogicNumber()
      call OpenFile(ln2,ListDir,'formatted','unknown')
      write(ln2,'(''.'')')
      if((OpSystem.le.0.and.CurrentDir(2:).ne.':'//ObrLom).or.
     1   (OpSystem.eq.1.and.CurrentDir.ne.'/')) write(ln2,'(''..'')')
      ln3=NextLogicNumber()
      if(Key.eq.0) then
        WorkFile=ListFile
        NFilter=0
        k=0
1100    call KusAp(Filter,k,t80)
        NFilter=NFilter+1
        if(k.lt.len(Filter)) go to 1100
        allocate(FilterArr(NFilter))
        i=0
        k=0
1200    i=i+1
        call KusAp(Filter,k,FilterArr(i))
        if(k.lt.len(Filter)) go to 1200
      else
        WorkFileP='jwork'
        ln4=ln3
        call CreateTmpFile(WorkFileP,i,0)
        call FeTmpFilesAdd(WorkFileP)
        call OpenFile(ln4,WorkFileP,'formatted','unknown')
        ln3=NextLogicNumber()
        WorkFile='jwork'
        call CreateTmpFile(WorkFile,i,0)
        call FeTmpFilesAdd(WorkFile)
      endif
      call OpenFile(ln3,WorkFile,'formatted','unknown')
2000  read(ln1,FormA,end=5000) Nazev
      call FeDirCheckTime(Dlouho,tf,ich)
      call Skrtni09(Nazev)
      if(Nazev.eq.' ') then
        if(ich.eq.0) then
          go to 2000
        else
          go to 5000
        endif
      endif
      i=idel(Nazev)
      if(OpSystem.eq.1) then
        do ip=i,1,-1
         ipom=ichar(Nazev(ip:ip))
         if(ipom.gt.32.and.ipom.lt.127) go to 2200
        enddo
2200    i=ip
        if(Nazev(ip:ip).eq.'*') then
          Nazev(ip:ip)=' '
          ip=ip-1
        endif
      endif
      if(OpSystem.le.0.and.Nazev(1:1).eq.'[') then
        i=i-1
        Nazev=Nazev(2:)
        if(Nazev(i:i).eq.']') Nazev=Nazev(:i-1)
        if(Nazev.eq.'.'.or.Nazev.eq.'..') then
          if(ich.eq.0) then
            go to 2000
          else
            go to 5000
          endif
        endif
        ln=ln2
      else if(OpSystem.eq.1.and.Nazev(i:i).eq.'/') then
        Nazev=Nazev(:i-1)
        ln=ln2
      else
        if(idel(Nazev).eq.0) go to 4000
        if(Key.ne.0) then
          ip=1
2500      i=index(Nazev(ip:),'.')
          if(i.gt.0) then
            ip=ip+i
            go to 2500
          endif
          if(OpSystem.le.0) then
            if(EqIgCase(Nazev(ip:),'tmp')) go to 4000
          else
            if(Nazev(ip:).eq.'tmp') go to 4000
          endif
          if(ip.gt.1) Nazev=Nazev(:ip-2)
          rewind ln3
          rewind ln4
3000      read(ln3,FormA,end=3100,err=3100) NazevP
          write(ln4,FormA) NazevP(:idel(NazevP))
          call FeDirCheckTime(Dlouho,tf,ich)
          if(OpSystem.eq.1) then
            if(Nazev.eq.NazevP) then
              go to 4000
            else
              go to 3000
            endif
          else
            if(EqIgCase(Nazev,NazevP)) then
              go to 4000
            else
              go to 3000
            endif
          endif
3100      i=ln4
          ln4=ln3
          ln3=i
        else
          do i=1,NFilter
            if(EqWild(Nazev,FilterArr(i),OpSystem.gt.0)) go to 3200
          enddo
          go to 4000
        endif
3200    ln=ln3
      endif
      write(ln,FormA) Nazev(:idel(Nazev))
4000  if(ich.eq.0) go to 2000
5000  call CloseIfOpened(ln1)
      call DeleteFile(TmpFile)
      call FeTmpFilesClear(TmpFile)
      if(Key.ne.0) then
        rewind ln3
        call OpenFile(ln1,ListFile,'formatted','unknown')
6000    read(ln3,FormA,end=6100) Nazev
        call FeDirCheckTime(Dlouho,tf,ich)
        if(ich.ne.0) go to 9999
        if(ExistFile(Nazev(:idel(Nazev))//'.m50')) then
          i=idel(Nazev)
          write(Nazev(i+2:),'(''#$color'',i10)') Red
          go to 6050
        endif
        if(ExistFile(Nazev(:idel(Nazev))//'.cad').or.
     1     ExistFile(Nazev(:idel(Nazev))//'.col').or.
     2     ExistFile(Nazev(:idel(Nazev))//'.hkl').or.
     3     ExistFile(Nazev(:idel(Nazev))//'.dat').or.
     4     ExistFile(Nazev(:idel(Nazev))//'.p4o').or.
     5     ExistFile(Nazev(:idel(Nazev))//'.raw').or.
     6     ExistFile(Nazev(:idel(Nazev))//'.psi')) then
          if(index(Nazev,'#$color').le.0) then
            i=idel(Nazev)
            write(Nazev(i+2:),'(''#$color'',i10)') Green
          endif
        endif
6050    write(ln1,FormA) Nazev(:idel(Nazev))
        go to 6000
6100    close(ln3,status='delete')
        close(ln4,status='delete')
        if(Key.ne.0) then
          call FeTmpFilesClear(WorkFile)
          call FeTmpFilesClear(WorkFileP)
        endif
        call CloseIfOpened(ln1)
      else
        call CloseIfOpened(ln3)
      endif
      call CloseIfOpened(ln2)
9999  if(Dlouho) then
        call FeMsgClear(1)
        call FeReleaseOutput
        call FeDeferOutput
        if(ich.ne.0) then
          NInfo=1
          TextInfo(1)='The list of files/structures isn''t complete.'
          call FeMsgShow(1,-1.,-1.,.true.)
          call FeWait(1.)
          call FeReleaseOutput
          call FeDeferOutput
          call FeMsgClear(1)
          call CloseIfOpened(ln1)
          call CloseIfOpened(ln2)
          call CloseIfOpened(ln3)
        endif
      endif
      if(allocated(FilterArr)) deallocate(FilterArr)
      return
      end
