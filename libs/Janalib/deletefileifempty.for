      subroutine DeleteFileIfEmpty(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      character*256 Veta
      logical opened
      inquire(file=FileName,opened=opened,number=ln)
      if(opened) then
        rewind ln
      else
        ln=NextLogicNumber()
        open(ln,file=FileName,err=9999)
      endif
1100  read(ln,FormA,end=9000,err=9999) Veta
      if(idel(Veta).gt.0) then
        if(.not.opened) call CloseIfOpened(ln)
        go to 9999
      else
        go to 1100
      endif
9000  close(ln,status='delete',err=9999)
9999  return
      end
