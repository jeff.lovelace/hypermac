      integer function FeGetEdwStringPosition(id,XPosition)
      include 'fepc.cmn'
      character*256 Veta
      Veta=EdwString(id)
      k1=EdwTextMin(id)
      k2=EdwTextMax(id)
      XLength=FeTxLengthSpace(Veta(k1:k2))
      XPom=XPosition-EdwTextXMin(id)
      if(XPom.lt.0.) then
        FeGetEdwStringPosition=0
        go to 9999
      else if(XPom.gt.XLength) then
        FeGetEdwStringPosition=k2
        go to 9999
      endif
      DPom1=0.
      do k=k1,k2
        DPom2=FeTxLengthSpace(Veta(k1:k))
        if(XPom.ge.DPom1.and.XPom.lt.DPom2) then
          if(abs(XPom-DPom1).lt.abs(XPom-DPom2)) then
            FeGetEdwStringPosition=k-1
          else
            FeGetEdwStringPosition=k
          endif
          go to 9999
        endif
        DPom1=DPom2
      enddo
      FeGetEdwStringPosition=k2
9999  return
      end
