      subroutine DeleteFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      logical opened
      i1=1
      i2=idel(FileName)
      if(FileName(i1:i1).eq.'#'.and.FileName(i2:i2).eq.'#') then
        i1=i1+1
        i2=i2-1
      endif
      inquire(file=FileName(i1:i2),opened=opened,number=i)
      if(.not.opened) then
        i=NextLogicNumber()
        open(i,file=FileName(i1:i2),err=9999)
      endif
      close(i,status='delete',err=9999)
9999  return
      end
