      subroutine FeXf2Pixels(xp,ix,iy)
      include 'fepc.cmn'
      dimension xp(3),xo(3)
      call FeXf2X(xp,xo)
      ix=nint(xo(1)*EnlargeFactor)
      iy=nint(xo(2)*EnlargeFactor)
      return
      end
