      subroutine FeKartRefresh(klic)
      include 'fepc.cmn'
      integer ButtonStateSave(mxdbutt)
      save NKartSave,KartNDolSave,ButtonStateSave
      if(klic.eq.0) then
        if(KartOn) then
          NKartSave=NKart
          KartNDolSave=KartNDol
          do i=1,KartNDol
            ButtonStateSave(i)=ButtonState(KartDolButt(i))
          enddo
          KartNDol=0
          NKart=0
        endif
      else
        NKart=NKartSave
        KartNDol=KartNDolSave
        do i=1,KartNDol
          ButtonState(KartDolButt(i))=ButtonStateSave(i)
        enddo
      endif
      KartOn=klic.ne.0
      return
      end
