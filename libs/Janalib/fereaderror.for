      subroutine FeReadError(ln)
      include 'fepc.cmn'
      character*20  Formatted
      character*256 t256,p256
      logical   EqIgCase
      if(IgnoreE) go to 9000
      inquire(ln,name=p256,form=Formatted)
      il=len(TextInfo(1))
      if(EqIgCase(formatted,'formatted')) then
        backspace ln
        read(ln,FormA,end=1000,err=1100) t256
        go to 1500
1000    t256='### EOF detected ###'
        go to 1500
1100    t256='### not readable by A-conversion ###'
1500    TextInfo(3)='Record     : '//t256
        il=len(TextInfo(3))
        if(idel(t256).gt.il-13) TextInfo(3)(il-1:il)='..'
      else
        TextInfo(3)='Unformatted file'
      endif
      call FeCutName(p256,t256,il-13,CutTextFromLeft)
      TextInfo(1)='File name : '//p256(:idel(p256))
      write(TextInfo(2),'(i2)') ln
      TextInfo(2)='Logical number : '//TextInfo(2)
      Ninfo=3
      call FeInfoOut(-1.,YBottomMessage,'READING ERROR','L')
      if(VasekTest.eq.999) call FeWinMessage('Cesta?',' ')
9000  call CloseIfOpened(ln)
      return
      end
