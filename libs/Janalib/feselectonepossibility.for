      integer function FeSelectOnePossibility(xmi,ymi,NHeader,idflt)
      include 'fepc.cmn'
      logical CrwLogicQuest,EqIgCase
      integer QuestLineWidthIn
      QuestLineWidthIn=QuestLineWidth
      QuestLineWidth=14.
      FeSelectOnePossibility=idflt
      xd=0.
      if(WizardMode) then
        yd=QuestLineWidth*float(WizardLines)+40.
        if(WizardTitle) yd=yd+QuestLineWidth
      else
        xd=0.
        do i=1,NHeader
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
          xd=max(FeTxLength(TextInfo(i))+10.,xd)
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
        enddo
        do i=NHeader+1,NInfo
          xd=max(FeTxLength(TextInfo(i))+CrwgXd+30.,xd)
        enddo
        il=NInfo+1
!        yd=20.+float(NInfo+1)*20.
      endif
      id=NextQuestid()
      if(.not.WizardMode)
     1  call FeQuestCreate(id,xmi,ymi,xd,il,' ',0,LightGray,-1,0)
      il=0
      do i=1,NHeader
        il=il+1
        if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
          xpom=xd*.5
        else
          xpom=5.
        endif
        call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                      TextInfoFlags(i)(1:1),
     2                      TextInfoFlags(i)(2:2))
        TextInfoFlags(i)='LN'
      enddo
      if(NHeader.gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
      endif
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=NHeader+1,NInfo
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,TextInfo(i),
     1                         'L',CrwgXd,CrwgYd,0,1)
        call FeQuestCrwOpen(CrwLastMade,i-NHeader.eq.idflt)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        do i=1,NInfo-NHeader
          if(CrwLogicQuest(i)) then
            FeSelectOnePossibility=i
            go to 3000
          endif
        enddo
      else if(ich.gt.0) then
        FeSelectOnePossibility= 0
      else
        FeSelectOnePossibility=-1
      endif
3000  if(.not.WizardMode) call FeQuestRemove(id)
      QuestLineWidth=QuestLineWidthIn
      return
      end
