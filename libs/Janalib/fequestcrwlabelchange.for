      subroutine FeQuestCrwLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer UseTabsOld
      UseTabsOld=UseTabs
      iw=iwp+CrwFr-1
      UseTabs=CrwLabelTabs(iw)
      if(CrwLabel(iw).ne.' ')
     1  call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),
     2                   CrwLabel(iw),CrwJustify(iw),LightGray)
      if(CrwState(iw).ne.CrwClosed) then
        if(Text.ne.' ') then
          call FeGetTextRectangle(CrwLabelX(iw),CrwLabelY(iw),
     1      Text,CrwJustify(iw),1,x1,x2,y1,y2,refx,refy,
     2      conx,cony)
          CrwLabelXMin(iw)=x1
          CrwLabelXMax(iw)=x2
          CrwLabelYMin(iw)=y1
          CrwLabelYMax(iw)=y2
          call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),
     1                     Text,CrwJustify(iw),LightGray)
          call FeOutStUnder(0,CrwLabelX(iw),CrwLabelY(iw),
     1                      Text,CrwJustify(iw),Black,Black,
     2                      CrwZ(iw:iw))
        else
          CrwZ(iw:iw)=' '
        endif
      endif
      CrwLabel(iw)=Text
      UseTabs=UseTabsOld
      return
      end
