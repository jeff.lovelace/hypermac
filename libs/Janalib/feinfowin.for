      subroutine FeInfoWin(xmi,ymi)
      include 'fepc.cmn'
      logical WizardModeIn,InfoWinOpened
      character*80 NapTmp
      data InfoWinOpened/.false./
      save xm,ym,xd,yd,NapTmp
      if(xmi.le.0) then
        xm=XPos
      else
        xm=xmi
      endif
      if(ymi.le.0) then
        ym=YPos
      else
        ym=Ymi
      endif
      xd=0.
      do i=1,NInfo
        xd=max(FeTxLength(TextInfo(i))+8.,xd)
      enddo
      yd=float(NInfo)*PropFontHeightInPixels
      if(xm+xd.gt.XMaxBasWin) xm=xm-xd
      NapTmp='jnap'
      if(OpSystem.le.0) call CreateTmpFile(NapTmp,i,1)
      call FeSaveImage(xm,xm+xd,ym,ym+yd,NapTmp)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,LightYellow)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,0,0,0,Black)
      xp=xm+5.
      yp=ym+yd-PropFontHeightInPixels*.5
      do i=1,NInfo
        call FeOutSt(0,xp,yp,TextInfo(i),'L',Black)
        yp=yp-PropFontHeightInPixels
      enddo
      InfoWinOpened=.true.
      go to 9999
      entry FeKillInfoWin
      if(InfoWinOpened) call FeLoadImage(xm,xm+xd,ym,ym+yd,NapTmp,0)
      InfoWinOpened=.false.
9999  return
      end
