      subroutine FeEdwBasic(id,ColorBckg)
      include 'fepc.cmn'
      integer ColorBckg
      call FeDrawFrame(EdwXMin(id),EdwYMin(id),
     1                 EdwXMax(id)-EdwXMin(id),
     2                 EdwYMax(id)-EdwYMin(id),2.,White,Gray,
     3                 Black,.false.)
      call FeFillRectangle(EdwXMin(id),EdwXMax(id),EdwYMin(id),
     1                     EdwYMax(id),4,0,0,ColorBckg)
      xu(1)=anint(EdwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(EdwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=anint(EdwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(2)=anint(EdwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(EdwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(3)=anint(EdwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=anint(EdwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(4)=anint(EdwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      return
      end
