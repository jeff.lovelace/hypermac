      subroutine FeSelectFiles(Header,ListOfFiles,FilterIn,ich)
      include 'fepc.cmn'
      character*(*) Header,ListOfFiles,FilterIn
      character*2 MenD(26)
      character*80 FileNameO,Filter,RolMenuTextQuest
      character*256 EdwStringQuest,SbwStringQuest,CurrentDirIn,
     1              CurrentDirO,Veta,Radka
      character*128 ListDir,ListFile
      integer FeChdir,ErrChdir,RolMenuSelectedQuest,ActualDrive,
     1        RolMenuNumberQuest,SbwItemPointerQuest,SbwLnQuest,
     2        SbwItemSelQuest,SbwItemFromQuest
      logical KeepOrgDir,Protected,EqIgCase,CrwLogicQuest,FeYesNoHeader,
     1        WizardModeIn,Finish
      ich=0
      call FeDeferOutput
      WizardModeIn=WizardMode
      WizardMode=.false.
      CurrentDirIn=CurrentDir
      id=NextQuestId()
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      il=17
      xqd=600.
      xqdp=xqd*.5
      call FeQuestCreate(id,-1.,-1.,xqd,il,Header,0,LightGray,0,0)
      xpom=15.
      dpom=xqdp-50.
      tpom=xpom+dpom*.5
      if(Language.eq.1) then
        Radka='Adres��'
      else
        Radka='Directory'
      endif
      call FeQuestLblMake(id,tpom,1,Radka,'C','N')
      ilp=14
      call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                    SbwVertical)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      nSbwDir=SbwLastMade
      dpome=dpom+SbwXMaxPruh(1,SbwLastMade)-SbwXMinPruh(1,SbwLastMade)
      il=ilp+1
      call FeQuestButtonMake(id,tpom-ButYd*.5,il,ButYd,ButYd,'#')
      nButtDir=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      i=ButtonLastMade+ButtonFr-1
      ButtonZ(i:i)='v'
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpome,EdwYd,1)
      nEdwDir=EdwLastMade
      xpom=xpom+xqdp
      xpomf=xpom
      tpom=tpom+xqdp
      if(Language.eq.1) then
        Radka='Soubory'
      else
        Radka='Files'
      endif
      call FeQuestLblMake(id,tpom,1,Radka,'C','N')
      call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                    SbwVertical)
      nSbwFile=SbwLastMade
      il=ilp+1
      il=il+1
      if(OpSystem.le.0) then
        tpom=15.
        if(Language.eq.1) then
          Radka='%Jednotka'
        else
          Radka='%Drive'
        endif
        xpom=tpom+FeTxLengthUnder(Radka)+12.
        dpom=40.+EdwYd
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,
     1                          1)
        call GetActiveDrives(MenD,NMenD)
        do i=1,NMenD
          if(EqIgCase(CurrentDir(1:2),MenD(i))) then
            KMenD=i
            go to 1110
          endif
        enddo
        KMenD=1
1110    nRolMenuDrive=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
        xpom=xpom+dpom+20.
      else
        xpom=15.
        nRolMenuDrive=0
      endif
      xpom=xpomf
      tpom=xpom+CrwXd+10.
      if(Language.eq.1) then
        Radka='Pou�ij %filtr'
      else
        Radka='Use %filter'
      endif
      call FeQuestCrwMake(id,tpom,il,xpom,il,Radka,'L',CrwXd,CrwYd,1,0)
      nCrwFilter=CrwLastMade
      if(FilterIn.eq.'*') then
        i=CrwOff
      else
        i=CrwOn
      endif
      call FeQuestCrwOpen(CrwLastMade,i.eq.CrwOn)
      tpom=tpom+FeTxLengthUnder(Radka)+5.
      Radka='=>'
      xpom=tpom+FeTxLengthUnder(Radka)+7.
      dpom=xqd-xpom-19.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,1)
      nEdwFilter=EdwLastMade
      Filter=FilterIn
      if(i.eq.CrwOn) call FeQuestStringEdwOpen(EdwLastMade,Filter)
      CurrentDirO=CurrentDir
      il=il-1
      Radka='%Select all'
      dpom=FeTxLengthUnder(Radka)+30.
      xpom=xpomf+60.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Radka)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAll=ButtonLastMade
      xpom=xpom+dpom+20.
      Radka='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Radka)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
1500  do i=1,5
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.le.0) go to 1530
      enddo
1530  Protected=ErrChdir.lt.0
      if(ErrChdir.gt.0) then
        ErrChdir=FeChdir(CurrentDirO)
        NInfo=1
        TextInfo(1)='The directory doesn''t exist'
        if(FeYesNoHeader(-1.,-1.,'Do you want to create it?',0)) then
          if(OpSystem.le.0) then
            call FeMkDir(CurrentDir,ichp)
          else
            call FeSystem('mkdir '//CurrentDir)
          endif
          ErrChdir=FeChdir(CurrentDir)
          if(ErrChdir.ne.0) then
            TextInfo(1)='Sorry, the action has not been successful.'
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          else
            go to 1560
          endif
        endif
        ErrChdir=FeChdir(CurrentDirO)
        CurrentDir=CurrentDirO
1560    EventType=EventEdw
        EventNumber=nEdwDir
      endif
      call FeGetCurrentDir
      call FeDir(ListDir,ListFile,Filter,0)
      call FeQuestSbwMenuOpen(nSbwDir,ListDir)
      call FeQuestStringEdwOpen(nEdwDir,CurrentDir)
      call FeQuestSbwSelectOpen(nSbwFile,ListFile)
2000  call GetActiveDrives(MenD,NMenD)
      if(NMenD.eq.RolMenuNumberQuest(nRolMenuDrive)) then
        do i=1,NMenD
          if(MenD(i).ne.RolMenuTextQuest(i,nRolMenuDrive)) go to 2020
        enddo
        go to 2060
      endif
2020  do i=1,NMenD
        if(EqIgCase(CurrentDir(1:2),MenD(i))) then
          KMenD=i
          go to 2040
        endif
      enddo
      KMenD=1
2040  call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
2060  ActualDrive=RolMenuSelectedQuest(nRolMenuDrive)
      Finish=.false.
      call FeQuestEvent(id,ich)
2100  if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDir) then
          CurrentDirO=CurrentDir
          i=SbwItemPointerQuest(nSbwDir)
          CurrentDir=SbwStringQuest(i,nSbwDir)
          go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        if(CheckNumber.eq.nButtAll) then
          j=1
        else
          j=0
        endif
        lni=SbwLnQuest(nSbwFile)
        i=0
        rewind lni
        if(EqIgCase(CurrentDirIn,CurrentDir)) then
          Radka=' '
        else
          Radka=CurrentDir
        endif
2200    read(lni,FormA,end=2220) Veta
        i=i+1
        call FeQuestSetSbwItemSel(i,nSbwFile,j)
        go to 2200
2220    call CloseIfOpened(SbwLnQuest(nSbwFile))
        ItemSelOld=SbwItemPointer(nSbwFile)
        ItemFromOld=SbwItemFromQuest(nSbwFile)
        call FeQuestSbwSelectOpen(nSbwFile,ListFile)
        call FeQuestSbwShow(nSbwFile,ItemFromOld)
        call FeQuestSbwItemOff(nSbwFile,SbwItemPointerQuest(nSbwFile))
        call FeQuestSbwItemOn(nSbwFile,ItemSelOld)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDrive) then
        i=RolMenuSelectedQuest(nRolMenuDrive)
        CurrentDirO=CurrentDir
        CurrentDir=MenD(i)
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.ne.0) then
          call FeChybne(-1.,-1.,'The drive isn''t accessible',' ',
     1                  SeriousError)
          CurrentDir=CurrentDirO
          ErrChdir=FeChdir(CurrentDir)
          call FeQuestRolMenuOpen(nRolMenuDrive,MenD,NMenD,ActualDrive)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFilter) then
        if((OpSystem.le.0.and.
     1      EqIgCase(Filter,EdwStringQuest(nEdwFilter))).or.
     2     (OpSystem.eq.1.and.
     3      Filter.eq.EdwStringQuest(nEdwFilter))) then
          go to 2000
        else
          Filter=EdwStringQuest(nEdwFilter)
          go to 1500
        endif
      else if(CheckType.eq.EventSbwDoubleClick) then
        CheckType=EventButton
        i=mod(CheckNumber,100)
        CheckNumber=nButtDir
        go to 2100
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDir) then
        if(EdwStringQuest(nEdwDir).ne.CurrentDir) then
          CurrentDir=EdwStringQuest(nEdwDir)
          go to 1500
        endif
        go to 2000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFilter) then
        if(CrwLogicQuest(nCrwFilter)) then
          call FeQuestStringEdwOpen(nEdwFilter,Filter)
        else
          Filter='*'
          call FeQuestEdwClose(nEdwFilter)
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0) then
        lni=SbwLnQuest(nSbwFile)
        lno=NextLogicNumber()
        Veta=CurrentDirIn(:idel(CurrentDirIn))//ListOfFiles
        call OpenFile(lno,Veta,'formatted','unknown')
        i=0
        rewind lni
        if(EqIgCase(CurrentDirIn,CurrentDir)) then
          Radka=' '
        else
          Radka=CurrentDir
        endif
3000    read(lni,FormA,end=3100) Veta
        i=i+1
        if(SbwItemSelQuest(i,nSbwFile).eq.1)
     1    write(lno,FormA) Radka(:idel(Radka))//Veta(:idel(Veta))
        go to 3000
3100    call CloseIfOpened(lno)
      endif
      call FeQuestRemove(id)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
9000  CurrentDir=CurrentDirIn
      i=FeChdir(CurrentDir(:idel(CurrentDir)))
9999  WizardMode=WizardModeIn
      return
      end
