      subroutine NToStrainString(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String
      dimension ia(3),ifa(4)
      if(n.le.15) then
        call indexc(n,4,ifa)
        do i=1,3
          ia(i)=0
        enddo
        do i=1,4
          j=ifa(i)
          ia(j)=ia(j)+1
        enddo
        write(String,'(3i1)') ia
      else
        if(n.eq.16) then
          String=' m'
        else if(n.eq.17) then
          String=' n'
        else if(n.eq.18) then
          String=' p'
        endif
        write(String(1:1),'(i1)') NDim(KPhase)
      endif
      return
      end
