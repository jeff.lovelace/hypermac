      subroutine FeOpenParEdwCrw(nEdw,nCrw,ParName,ParValue,ParRefKey,
     1                           ParToFraction)
      include 'fepc.cmn'
      character*(*) ParName
      integer ParRefKey
      logical ParToFraction,EqIgCase
      if(.not.EqIgCase(ParName,'#keep#'))
     1  call FeQuestEdwLabelChange(nEdw,ParName)
      call FeQuestRealEdwOpen(nEdw,ParValue,.false.,ParToFraction)
      call FeQuestCrwOpen(nCrw,mod(ParRefKey,10).ne.0)
      if(ParRefKey.ge.100) call FeQuestEdwLock(nEdw)
      if(mod(ParRefKey,100).ge.10) call FeQuestCrwLock(nCrw)
      return
      end
