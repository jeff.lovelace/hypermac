      logical function ProgramPresent(Name,CallString)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Name,CallString
      character*256 Veta
      logical ExistFile
      idl=idel(CallString)
      if(OpSystem.le.0) then
        Veta=' '
        do i=1,idl
          if(CallString(i:i).eq.'/') exit
          Veta(i:i)=CallString(i:i)
        enddo
        idl=idel(Veta)
      else
        Veta=CallString
      endif
      if(Veta(idl:idl).eq.'&') idl=idl-1
      if(ExistFile(Veta(:idl))) then
        ProgramPresent=.true.
      else
        ProgramPresent=.false.
        call FeChybne(-1.,-1.,
     1    'the '//Name(:idel(Name))//' as defined is not present. '//
     2    'Please correct the temporary defintion in "Tools->'//
     3    'Programs."',
     4    'The temporary defintion: "'//
     5    CallString(:idel(CallString))//'"',SeriousError)
      endif
      return
      end
