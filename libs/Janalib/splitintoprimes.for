      subroutine SplitIntoPrimes(ix,PrimeExp)
      include 'fepc.cmn'
      integer PrimeExp(30)
      call SetIntArrayTo(PrimeExp,30,0)
      iy=ix
      do i=1,30
        ip=PrimeNumbers(i)
        if(ip.gt.iy) go to 9999
        n=0
1000    if(mod(iy,ip).eq.0) then
          iy=iy/ip
          n=n+1
          go to 1000
        endif
        PrimeExp(i)=n
      enddo
9999  return
      end
