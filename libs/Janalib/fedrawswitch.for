      subroutine FeDrawSwitch(xmns,xmxs,ymns,ymxs,npix)
      include 'fepc.cmn'
      dimension xp(8),yp(8),xpp(8),ypp(8),xc(8),yc(8)
      fpix=npix
      dpix=2.*fpix
      xp(1)=xmns
      yp(1)=ymns
      xp(2)=xp(1)
      yp(2)=ymxs-dpix
      xp(3)=xmns+dpix
      yp(3)=ymxs
      xp(4)=xmxs-dpix
      yp(4)=ymxs
      xp(5)=xp(4)-fpix+1.
      yp(5)=ymxs-fpix+1.
      xp(6)=xmns+(dpix+fpix)-1.
      yp(6)=yp(5)
      xp(7)=xmns+fpix-1.
      yp(7)=ymxs-(dpix+fpix)+1.
      xp(8)=xp(7)
      yp(8)=yp(1)
      xpp(1)=xmxs
      ypp(1)=ymns
      xpp(2)=xpp(1)
      ypp(2)=ymxs-dpix
      xpp(3)=xp(4)
      ypp(3)=yp(4)
      xpp(4)=xp(5)
      ypp(4)=yp(5)
      xpp(5)=xmxs-fpix+1.
      ypp(5)=ymxs-(dpix+fpix)+1.
      xpp(6)=xpp(5)
      ypp(6)=ymns
      xc(1)=xpp(6)
      yc(1)=ypp(6)
      xc(2)=xpp(5)
      yc(2)=ypp(5)
      xc(3)=xpp(4)
      yc(3)=ypp(4)
      xc(4)=xp(6)
      yc(4)=yp(6)
      xc(5)=xp(7)
      yc(5)=yp(7)
      xc(6)=xp(8)
      yc(6)=yp(8)
      if(OpSystem.eq.1) then
        do i=1,8
          xp(i)=xp(i)
          yp(i)=yp(i)
        enddo
        do i=1,6
          xpp(i)=xpp(i)
          ypp(i)=ypp(i)
          xc(i)=xc(i)
          yc(i)=yc(i)
        enddo
      endif
      call FePolygon(xc,yc,6,4,0,0,LightGray)
      call FePolygon(xp,yp,8,4,0,0,White)
      call FePolygon(xpp,ypp,6,4,0,0,Gray)
      return
      end
