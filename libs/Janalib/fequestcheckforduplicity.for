      subroutine FeQuestCheckForDuplicity
      include 'fepc.cmn'
      character*128 TextInfoOld(50)
      character*2   TextInfoFlagsOld(50)
      integer UsedLetter(26)
      call SetIntArrayTo(UsedLetter,26,0)
      if(NInfo.gt.0) then
        do i=1,NInfo
          TextInfoOld(i)=TextInfo(i)
          TextInfoFlagsOld(i)=TextInfoFlags(i)
          TextInfoFlags(i)='LN'
        enddo
        NInfoOld=NInfo
      else
        NInfoOld=0
      endif
      TextInfo(1)=' '
      do i=ButtonFr,ButtonTo
        if(ButtonState(i).eq.ButtonClosed.or.ButtonZ(i:i).eq.' ') cycle
        j=ichar(ButtonZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,ButtonTo
          if(ButtonState(j).eq.ButtonClosed) cycle
          if(ButtonZ(i:i).eq.ButtonZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Button : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=ButtonText(j)
          endif
        enddo
        do j=CrwFr,CrwTo
          if(CrwState(j).eq.CrwClosed) cycle
          if(ButtonZ(i:i).eq.CrwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Crw : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=CrwLabel(j)
          endif
        enddo
        do j=EdwFr,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(ButtonZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/Edw : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(ButtonZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Button/RolMenu : '',2i3)') i,j
            TextInfo(2)=ButtonText(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwClosed.or.CrwZ(i:i).eq.' ') cycle
        j=ichar(CrwZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,CrwTo
          if(CrwState(j).eq.CrwClosed) cycle
          if(CrwZ(i:i).eq.CrwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/Crw : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=CrwLabel(j)
          endif
        enddo
        do j=EdwFr,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(CrwZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/Edw : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(CrwZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Crw/RolMenu : '',2i3)') i,j
            TextInfo(2)=CrwLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).eq.EdwClosed.or.EdwZ(i:i).eq.' ') cycle
        j=ichar(EdwZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,EdwTo
          if(EdwState(j).eq.EdwClosed) cycle
          if(EdwZ(i:i).eq.EdwZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''Edw/Edw : '',2i3)') i,j
            TextInfo(2)=EdwLabel(i)
            TextInfo(3)=EdwLabel(j)
          endif
        enddo
        do j=RolMenuFr,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(EdwZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''EdwZ/RolMenu : '',2i3)') i,j
            TextInfo(2)=EdwLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      do i=RolMenuFr,RolMenuTo
        if(RolMenuState(i).eq.RolMenuClosed.or.RolMenuZ(i:i).eq.' ')
     1    cycle
        j=ichar(RolMenuZ(i:i))-ichar('a')+1
        if(j.ge.1.and.j.le.26) UsedLetter(j)=1
        do j=i+1,RolMenuTo
          if(RolMenuState(j).eq.RolMenuClosed) cycle
          if(RolMenuZ(i:i).eq.RolMenuZ(j:j).and.TextInfo(1).eq.' ') then
            write(TextInfo(1),'(''RolMenu/RolMenu : '',2i3)') i,j
            TextInfo(2)=RolMenuLabel(i)
            TextInfo(3)=RolMenuLabel(j)
          endif
        enddo
      enddo
      if(TextInfo(1).eq.' ') go to 9999
      Ninfo=8
      TextInfo(4)=ButtonZ(ButtonFr:ButtonTo)
      TextInfo(5)=CrwZ(CrwFr:CrwTo)
      TextInfo(6)=EdwZ(EdwFr:EdwTo)
      TextInfo(7)=RolMenuZ(RolMenuFr:RolMenuTo)
      j=0
      TextInfo(8)=' '
      do i=1,26
        if(UsedLetter(i).eq.0) then
          j=j+1
          TextInfo(8)(j:j)=char(i+ichar('a')-1)
        endif
      enddo
      i=idel(TextInfo(8))
      TextInfo(8)='Free letters: '//TextInfo(8)(:i)
      call FeInfoOut(-1.,-1.,'Duplicate identification :','L')
9999  if(NInfoOld.gt.0) then
        do i=1,NInfoOld
          TextInfo(i)=TextInfoOld(i)
          TextInfoFlags(i)=TextInfoFlagsOld(i)
        enddo
        NInfo=NInfoOld
      endif
      return
      end
