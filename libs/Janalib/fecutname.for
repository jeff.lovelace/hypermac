      subroutine FeCutName(LongText,ShortText,is,CutText)
      include 'fepc.cmn'
      character*(*) LongText,ShortText
      integer CutText
      id=idel(LongText)
      if(id.le.is) then
        ShortText=LongText
      else
        if(CutText.eq.CutTextFromLeft) then
          ShortText='...'//LongText(id-is+4:id)
        else if(CutText.eq.CutTextFromRight) then
          ShortText=LongText(:is-3)//'...'
        else if(CutText.eq.CutTextNone) then
          ShortText=LongText(:is)
        endif
      endif
      return
      end
