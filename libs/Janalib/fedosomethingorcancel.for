      integer function FeDoSomethingOrCancel(Text,Label,n)
      include 'fepc.cmn'
      character*(*) Text,Label(n)
      character*7   Cancel
      data Cancel/'%Cancel'/
      id=NextQuestId()
      dpom=FeTxLengthUnder(Cancel)+10.
      do i=1,n
        dpom=max(dpom,FeTxLengthUnder(Label(n))+10.)
      enddo
      xd=dpom*float(n+1)+5.*float(n)
      xqd=max(xd,FeTxLength(Text))+10.
      xqd=min(xqd,260.)
      il=0
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,-1,-1)
      xpom=(xqd-xd)*.5
      il=1
      do i=1,n
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Label(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtFirst=ButtonLastMade
        xpom=xpom+dpom+5.
      enddo
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Cancel)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtCancel=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtCancel) then
          FeDoSomethingOrCancel=0
        else
          FeDoSomethingOrCancel=CheckNumber-nButtFirst+1
        endif
        call FeQuestRemove(id)
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      return
      end
