      subroutine FeEditParameterField(Par,SPar,ParDef,KiPar,n,lPar,xqd,
     1                                Text,tpoma,xpoma,NToInd)
      include 'fepc.cmn'
      dimension Par(n),SPar(n),ParDef(n),KiPar(n),tpoma(3),xpoma(3)
      character*(*) lPar,Text
      character*80 Veta
      logical :: CrwLogicQuest,InSig = .false.
      external NToInd
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,(n-1)/3+2,Text,0,LightGray,0,0)
      Veta=lPar
      idv=idel(Veta)+1
      do k=1,2
        do i=1,n
          im=mod(i-1,3)+1
          il=(i-1)/3+1
          call NToInd(i,Veta(idv:))
          if(k.eq.1) then
            call FeQuestLblMake(id,tpoma(im),il,Veta,'R','N')
            if(i.eq.1) nLblPrv=LblLastMade
            call FeQuestLblOff(LblLastMade)
            call FeQuestLblMake(id,xpoma(im)+6.,il,' ','L','N')
            call FeQuestLblOff(LblLastMade)
          else
            call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,
     1                           nCrw)
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',Par(i),kiPar(i),
     1                           .false.)
            if(i.eq.1) then
              nEdwPrv=nEdw
              nCrwPrv=nCrw
            endif
          endif
        enddo
      enddo
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      go to 2000
1400  nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblPrv
      if(InSig) then
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,Par,kiPar,n)
        do i=1,n
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
          call FeQuestLblOn(nLbl)
          if(SPar(i).gt.0.) then
            pom=abs(Par(i)/SPar(i))
          else
            pom=-1.
          endif
          if(pom.lt.0.) then
            Cislo='fixed'
          else if(pom.lt.3.) then
            Cislo='< 3*su'
          else
            write(Cislo,'(f15.1)') pom
            call ZdrcniCisla(Cislo,1)
            Cislo=Cislo(:idel(Cislo))//'*su'
          endif
          call FeQuestLblChange(nLbl+1,Cislo)
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        do i=1,n
          call FeQuestLblOff(nLbl)
          call FeQuestLblOff(nLbl+1)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',Par(i),kiPar(i),
     1                         .false.)
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtRefineAll) then
          i=CrwOn
        else if(CheckNumber.eq.nButtFixAll) then
          i=CrwOff
        else if(CheckNumber.eq.nButtReset) then
          i=0
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do j=1,n
            if(CrwLogicQuest(nCrw)) then
              pom=ParDef(j)
            else
              pom=0.
            endif
            call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else if(CheckNumber.eq.nButtInSig) then
          InSig=.not.InSig
          if(InSig) then
            Veta='%Edit mode'
          else
            Veta='Show %p/sig(p)'
          endif
          call FeQuestButtonLabelChange(nButtInSig,Veta)
          go to 1400
        endif
        if(i.ne.0) then
          do nCrw=nCrwPrv,nCrwPrv+n-1
            call FeQuestCrwOpen(nCrw,i.eq.CrwOn)
          enddo
        endif
        go to 2000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0)
     1  call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,Par,kiPar,n)
      call FeQuestRemove(id)
      return
      end
