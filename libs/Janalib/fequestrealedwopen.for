      subroutine FeQuestRealEdwOpen(iw,X,Prazdny,Zlomek)
      include 'fepc.cmn'
      dimension xp(1)
      integer Exponent10
      logical   Prazdny,Zlomek
      call FeQuestEdwOpenInic(iw,iwp,1,Prazdny)
      if(Prazdny) then
        EdwReal(1,iwp)=0.
      else
        if(Zlomek) then
          call ToFract(X,Cislo,10)
        else
          Cislo(1:1)='?'
        endif
        if(Cislo(1:1).eq.'?') then
          if(abs(X).gt.0.) then
            j=max(6-Exponent10(X),0)
            j=min(j,6)
          else
            j=6
          endif
          write(RealFormat(6:7),'(i2)') j
          write(Cislo,RealFormat) X
        endif
        call ZdrcniCisla(Cislo,1)
        k=0
        call StToReal(Cislo,k,xp,1,.false.,ich)
        EdwReal(1,iwp)=xp(1)
        EdwString(iwp)=Cislo
      endif
      call FeEdwOpen(iwp)
      return
      end
