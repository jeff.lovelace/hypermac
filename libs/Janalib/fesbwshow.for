      subroutine FeSbwShow(id,From)
      include 'fepc.cmn'
      integer From,BackgroundColor,TextColor
      logical KresliPlovak,KresliStrelku,Nacitat
      SbwItemFrom(id)=From
      SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
      Nacitat=.true.
      go to 2000
      entry FeSbwShowPohniUpDown(id,From)
      SbwItemFrom(id)=From
      SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
      KresliPlovak= .false.
      KresliStrelku=.false.
      Nacitat=.true.
      go to 2010
      entry FeSbwShowPohniLeftRight(id,From)
      if(SbwStripes(id).gt.1) then
        SbwItemFrom(id)=From
        SbwItemTo  (id)=SbwItemFrom(id)+SbwLines(id)*SbwStripes(id)-1
        Nacitat=.true.
      else
        SbwColumnFrom(id)=From
        SbwColumnTo  (id)=From+SbwColumns(id)-1
        Nacitat=.false.
      endif
      KresliPlovak= .false.
      KresliStrelku=.false.
      go to 2010
      entry FeSbwLineDown(id)
      SbwItemFrom(id)=SbwItemFrom(id)+1
      SbwItemTo  (id)=SbwItemTo  (id)+1
      Nacitat=.true.
      go to 2000
      entry FeSbwLineUp(id)
      SbwItemFrom(id)=SbwItemFrom(id)-1
      SbwItemTo  (id)=SbwItemTo  (id)-1
      Nacitat=.true.
      go to 2000
      entry FeSbwPageDown(id)
      SbwItemFrom(id)=SbwItemFrom(id)+SbwLines(id)
      SbwItemTo  (id)=SbwItemTo  (id)+SbwLines(id)
      Nacitat=.true.
      go to 2000
      entry FeSbwPageUp(id)
      SbwItemFrom(id)=SbwItemFrom(id)-SbwLines(id)
      SbwItemTo  (id)=SbwItemTo  (id)-SbwLines(id)
      Nacitat=.true.
      go to 2000
      entry FeSbwColumnLeft(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)-SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)-SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)-1
        SbwColumnTo  (id)=SbwColumnTo  (id)-1
      endif
      go to 2000
      entry FeSbwColumnRight(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)+SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)+SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)+1
        SbwColumnTo  (id)=SbwColumnTo  (id)+1
      endif
      go to 2000
      entry FeSbwColumnStrongLeft(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)-SbwStripes(id)*SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)-SbwStripes(id)*SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)-SbwColumns(id)
        SbwColumnTo  (id)=SbwColumnTo  (id)-SbwColumns(id)
      endif
      go to 2000
      entry FeSbwColumnStrongRight(id)
      Nacitat=SbwStripes(id).gt.1
      if(Nacitat) then
        SbwItemFrom(id)=SbwItemFrom(id)+SbwStripes(id)*SbwLines(id)
        SbwItemTo  (id)=SbwItemTo  (id)+SbwStripes(id)*SbwLines(id)
      else
        SbwColumnFrom(id)=SbwColumnFrom(id)+SbwColumns(id)
        SbwColumnTo  (id)=SbwColumnTo  (id)+SbwColumns(id)
      endif
      go to 2000
2000  KresliPlovak =SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth
      KresliStrelku=SbwBars(id).eq.SbwHorizontal.or.
     1              SbwBars(id).eq.SbwBoth
2010  if(SbwColumnFrom(id).lt.1) then
        SbwColumnFrom(id)=1
        SbwColumnTo  (id)=SbwColumns(id)
      endif
      if(SbwColumnTo(id).gt.SbwColumnsMax(id)) then
        SbwColumnTo  (id)=SbwColumnsMax(id)
        SbwColumnFrom(id)=max(SbwColumnsMax(id)-SbwColumns(id)+1,1)
      endif
      if(SbwItemFrom(id).lt.1) then
        SbwItemFrom(id)=1
        SbwItemTo  (id)=SbwLines(id)*SbwStripes(id)
      endif
      if(SbwItemTo(id).gt.SbwItemNMax(id)) then
        if(SbwStripes(id).gt.1) then
          SbwItemFrom(id)=max(SbwLines(id)*(SbwStripesMax(id)-
     1                                      SbwStripes(id))+1,1)
          SbwItemTo(id)=SbwItemNMax(id)
        else
          SbwItemTo  (id)=SbwItemNMax(id)
          SbwItemFrom(id)=max(SbwItemNMax(id)-SbwLines(id)+1,1)
        endif
      endif
      if(SbwStripes(id).gt.1) then
        SbwStripeFrom(id)=max(nint(float(SbwItemFrom(id)-1)/
     1                             float(SbwLines(id)))+1,1)
        SbwStripeTo  (id)=min(nint(float(SbwItemTo(id))/
     1                             float(SbwLines(id))),
     2                        SbwStripesMax(id)-SbwStripes(id))
      endif
      call FeSbwAction(id,Nacitat,ich)
      if(ich.ne.0) go to 9999
      if(KresliPlovak) call FeSbwKresliPlovak(id)
      if(KresliStrelku) call FeSbwKresliStrelku(id)
      go to 9999
      entry FeSbwItemOn(id,ik)
      i=ik+SbwItemFrom(id)-1
      if(SbwType(id).eq.SbwMenuType) then
        BackGroundColor=DarkBlue
        if(ik.ge.1) then
          if(SbwItemColor(ik,id).eq.Black) then
            TextColor=White
          else
            TextColor=SbwItemColor(ik,id)
          endif
          SbwItemPointer(id)=i
        endif
        go to 4000
      else if(SbwType(id).eq.SbwSelectType) then
        BackGroundColor=SnowGray
        if(SbwItemSel(i,id).eq.1) then
          TextColor=Red
        else
          if(SbwItemSel(i,id).eq.0) then
            TextColor=SbwItemColor(ik,id)
          else
            TextColor=Gray
          endif
        endif
        SbwItemPointer(id)=i
        go to 4000
      else
        SbwItemPointer(id)=0
        go to 9999
      endif
      entry FeSbwItemOff(id,ik)
      i=ik+SbwItemFrom(id)-1
      BackgroundColor=White
      SbwItemPointer(id)=0
      if(SbwType(id).eq.SbwMenuType) then
        TextColor=SbwItemColor(ik,id)
        go to 4000
      else if(SbwType(id).eq.SbwSelectType) then
        if(SbwItemSel(i,id).eq.1) then
          TextColor=Red
        else
          if(SbwItemSel(i,id).eq.0) then
            TextColor=SbwItemColor(ik,id)
          else
            TextColor=Gray
          endif
        endif
        go to 4000
      else
        go to 9999
      endif
4000  if(ik.ge.1.and.ik.le.SbwStripes(id)*SbwLines(id)) then
        ix=(ik-1)/SbwLines(id)+1
        iy=mod(ik-1,SbwLines(id))+1
        xpom=SbwXStripe(ix,id)
        ypom=SbwYMax(id)-float(iy-1)*SbwYStep(id)
        call FeWrLine(xpom,ypom,SbwXStripe(ix+1,id)-SbwXStripe(ix,id),
     1                SbwItemShort(ik,id),TextColor,
     2                BackgroundColor)
        do i=2,SbwStripes(id)
          xu(1)=SbwXStripe(i,id)
          xu(2)=xu(1)
          yu(1)=SbwYMin(id)
          yu(2)=SbwYMax(id)
          call FePolyLine(2,xu,yu,Black)
        enddo
      endif
9999  return
      end
