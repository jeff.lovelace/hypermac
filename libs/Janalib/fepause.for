      subroutine FePause
      include 'fepc.cmn'
      itype=EventType
      inumber=EventNumber
500   call FeEvent(0)
      if(EventType.ne.EventMouse.or.EventNumber.ne.JeLeftDown) go to 500
      EventType=itype
      EventNumber=inumber
      if(EventType.eq.EventEdw) then
        EventNumberAbs=EventNumber+EdwFr-1
      else if(EventType.eq.EventButton) then
        EventNumberAbs=EventNumber+ButtonFr-1
      else if(EventType.eq.EventCrw) then
        EventNumberAbs=EventNumber+CrwFr-1
      endif
      return
      end
