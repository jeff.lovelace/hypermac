      subroutine FeInOutIni(klic,FileNameIni)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*(*) FileNameIni
      character*256 t256,Radka
      character*80 KeyWord(90),KeyWordRead
      character*10 SizeType(4)
      dimension iap(1),poma(1)
      integer PixelWindowWidthS,PixelWindowHeightS,
     1        PixelWindowXPosS,PixelWindowYPosS
      logical EqIgCase,ExistFile
      equivalence (iap(1),i),(poma,pom)
      equivalence
     1           (IdNumbers( 1),IdWindowWidth),
     2           (IdNumbers( 2),IdWindowHeight),
     3           (IdNumbers( 3),IdWindowColors),
     4           (IdNumbers( 4),IdWindowX),
     5           (IdNumbers( 5),IdWindowY),
     6           (IdNumbers( 6),IdWindowSizeType),
     7           (IdNumbers( 7),IdFontSize),
     8           (IdNumbers( 8),IdUseViewer),
     9           (IdNumbers( 9),IdICDDKey),
     a           (IdNumbers(10),IdICDDDir),
     1           (IdNumbers(11),IdWineKey),
     2           (IdNumbers(12),IdTmpDir),
     3           (IdNumbers(13),IdCIFSpecFile),
     4           (IdNumbers(14),IdPSPrinter),
     1           (IdNumbers(21),IdEditorCommand),
     2           (IdNumbers(22),IdDosShellCommand),
     3           (IdNumbers(23),IdXShapeCommand),
     4           (IdNumbers(24),IdGraphicCommand),
     5           (IdNumbers(25),IdGnuplotCommand),
     6           (IdNumbers(26),IdGSCommand),
     7           (IdNumbers(27),Id3dMapsCommand),
     8           (IdNumbers(28),IdDPlotCommand),
     9           (IdNumbers(29),IdPathToELMAM),
     1           (IdNumbers(41),IdSIRCommand),
     2           (IdNumbers(42),IdSIR97Command),
     3           (IdNumbers(43),IdSIR2002Command),
     4           (IdNumbers(44),IdSIR2004Command),
     5           (IdNumbers(45),IdSIR2008Command),
     6           (IdNumbers(46),IdSIR2011Command),
     7           (IdNumbers(47),IdSIR2014Command),
     1           (IdNumbers(61),IdExpoCommand),
     2           (IdNumbers(62),IdExpo2004Command),
     3           (IdNumbers(63),IdExpo2009Command),
     4           (IdNumbers(64),IdExpo2013Command),
     5           (IdNumbers(65),IdExpo2014Command),
     6           (IdNumbers(71),IdShelxtCommand),
     7           (IdNumbers(72),IdGeminographyCommand),
     1           (IdNumbers(81),IdDelejKontroly),
     2           (IdNumbers(82),IdVasekTest),
     3           (IdNumbers(83),IdVasekAbs)
      save PixelWindowWidthS,PixelWindowHeightS,
     1     PixelWindowXPosS,PixelWindowYPosS
      data KeyWord/'WindowWidth','WindowHeight','WindowColors',
     1             'WindowX','WindowY','WindowSizeType','FontSize',
     2             'UseViewer','ICDDKey','ICDDDir','WineKey','TmpDir',
     3             'CIFSpecFile','PSPrinter','#15',
     4             '#16','#17','#18','#19','#20',
     5             'EditorCommand','DosShellCommand','XShapeCommand',
     6             'GraphicCommand','GnuplotCommand','GSCommand',
     7             '3dMapsCommand','DPlotCommand',
     8             'PathToELMAM','#30','#31','#32','#33','#34','#35',
     9             '#36','#37','#38','#39','#40',
     a             'SIRCommand','SIR97Command','SIR2002Command',
     1             'SIR2004Command','SIR2008Command','SIR2011Command',
     2             'SIR2014Command',
     3             '#48','#49','#50','#51','#52','#53','#54','#55',
     3             '#56','#57','#58','#59','#60',
     4             'ExpoCommand','Expo2004Command','Expo2009Command',
     5             'Expo2013Command','Expo2014Command',
     6             '#66','#67','#68','#69','#70',
     7             'ShelxtCommand','GeminographyCommand','#73',
     7             '#74','#75','#76','#77','#78','#79','#80',
     8             'DelejKontroly','VasekTest','VasekAbs',
     9             '#84','#85','#86','#87','#88','#89','#90'/
      data SizeType/'Normal','Minimal','Fullscreen','Exactly'/
      k=klic
1000  if(k.eq.0) then
        PropFontSize=8
        if(OpSystem.eq.0) then
          mon14=.true.
        else
          mon14=.false.
        endif
        DelejKontroly=0
        VasekTest=0
        BuildInViewer=.true.
        EditorName='c:\WINDOWS\notepad.exe'
        VisualClass='PCcol'
        FirstCommand='command.com'
        CallXShape=' '
        CallGeminography=' '
        CallGraphic=' '
        CallSIR=' '
        CallExpo=' '
        DirSIR=' '
        DirExpo=' '
        Call3dMaps=' '
        CallDPlot=' '
        PathToELMAM=' '
        CallGnuplot=' '
        CallGS=' '
        PSPrinter=' '
        ICDDDir=' '
        ICDDKey=0
        TmpDir=HomeDir(:idel(HomeDir))//'tmp'
        WineKey=0
        VasekAbs=0
        PixelWindowHeight=3*nint(.8*float(PixelScreenHeight)/3.)
        PixelWindowWidth=(4*PixelWindowHeight)/3
        WindowSizeType=WindowExactly
        PixelWindowXPos=(PixelScreenWidth-PixelWindowWidth)/2
        PixelWindowYPos=(PixelScreenHeight-PixelWindowHeight)/2
        PixelWindowWidthE=PixelWindowWidth
        PixelWindowHeightE=PixelWindowHeight
        PixelWindowXPosE=PixelWindowXPos
        PixelWindowYPosE=PixelWindowYPos
        if(ExistFile(FileNameIni)) then
          ln=NextLogicNumber()
          if(VasekDebug.ne.0) write(44,'(''PARAMS - as read from "'',a,
     1                        ''"'')') FileNameIni(:idel(FileNameIni))
          open(ln,file=FileNameIni,err=9000)
2000      read(ln,FormA,end=3000,err=9000) t256
          if(idel(t256).le.0.or.t256(1:1).eq.'#') go to 2000
          Radka=t256
          i=LocateSubstring(Radka,MasterName(:idel(MasterName))//'.',
     1                      .false.,.true.)
          if(i.le.0) go to 2000
          i=index(Radka,'.')+1
          j=index(Radka,':')
          if(j.le.i) go to 2000
          KeyWordRead=Radka(i:j-1)
          j=j+1
          do i=1,90
            if(EqIgCase(KeyWordRead,KeyWord(i))) go to 2600
          enddo
          go to 2000
2600      if(i.eq.IdWindowWidth) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowWidthE=i
              PixelWindowWidth=i
            endif
          else if(i.eq.IdWindowHeight) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowHeightE=i
              PixelWindowHeight=i
            endif
          else if(i.eq.IdWindowColors) then
            call StToInt(t256,j,iap,1,.false.,ich)
          else if(i.eq.IdWindowX) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowXPosE=i
              PixelWindowXPos=i
            endif
          else if(i.eq.IdWindowY) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PixelWindowYPosE=i
              PixelWindowYPos=i
            endif
          else if(i.eq.WindowSizeType) then
            t256=t256(j:)
            call zhusti(t256)
            do i=1,4
              if(EqIgCase(t256,SizeType(i))) then
                WindowSizeType=i
                go to 2000
              endif
            enddo
          else if(i.eq.IdFontSize) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) then
              PropFontSize=i
              if(PropFontMeasured.eq.FontInPixels) then
                if(PropFontSize.lt.15) PropFontSize=15
              else if(PropFontMeasured.eq.FontInPoints) then
                if(PropFontSize.ge.15) PropFontSize=8
              endif
            endif
          else if(i.eq.IdUseViewer) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) BuildInViewer=iap(1).eq.1
          else if(i.eq.IdICDDKey) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) ICDDKey=i
          else if(i.eq.IdICDDDir) then
            ICDDDir=t256(j+1:)
            i=idel(ICDDDir)
            if(i.gt.0) then
              call SkrtniPrvniMezery(ICDDDir)
3200          if(ICDDDir(i:i).eq.'\') then
                ICDDDir(i:i)=' '
                i=i-1
                go to 3200
              endif
              i=i+1
              ICDDDir(i:i)='\'
            endif
          else if(i.eq.IdWineKey) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) WineKey=i
          else if(i.eq.IdTmpDir) then
            TmpDir=t256(j+1:)
            call SkrtniPrvniMezery(TmpDir)
            i=idel(TmpDir)
3100        if(TmpDir(i:i).eq.'\') then
              TmpDir(i:i)=' '
              i=i-1
              go to 3100
            endif
            i=i+1
            TmpDir(i:i)='\'
          else if(i.eq.IdCIFSpecFile) then
            CIFSpecificFile=t256(j+1:)
            call SkrtniPrvniMezery(CIFSpecificFile)
          else if(i.eq.IdPSPrinter) then
            PSPrinter=Radka(j+1:)
            call SkrtniPrvniMezery(PSPrinter)
          else if(i.eq.IdEditorCommand) then
            EditorName=t256(j+1:)
            call SkrtniPrvniMezery(EditorName)
          else if(i.eq.IdDosShellCommand) then
            FirstCommand=t256(j+1:)
            call SkrtniPrvniMezery(FirstCommand)
          else if(i.eq.IdXShapeCommand) then
            CallXShape=t256(j+1:)
            call SkrtniPrvniMezery(CallXShape)
          else if(i.eq.IdGraphicCommand) then
            CallGraphic=t256(j+1:)
            call SkrtniPrvniMezery(CallGraphic)
          else if(i.eq.IdGnuplotCommand) then
            CallGnuplot=t256(j+1:)
            call SkrtniPrvniMezery(CallGnuplot)
          else if(i.eq.IdGSCommand) then
            CallGS=Radka(j+1:)
            call SkrtniPrvniMezery(CallGS)
          else if(i.eq.Id3dMapsCommand) then
            Call3dMaps=t256(j+1:)
            call SkrtniPrvniMezery(Call3dMaps)
          else if(i.eq.IdDPlotCommand) then
            CallDPlot=t256(j+1:)
            call SkrtniPrvniMezery(Call3dMaps)
          else if(i.eq.IdPathToELMAM) then
            PathToELMAM=t256(j+1:)
            call SkrtniPrvniMezery(PathToELMAM)
          else if(i.ge.IdSIRCommand.and.i.le.IdSIR2014Command) then
            t256=t256(j+1:)
            call SkrtniPrvniMezery(t256)
            k=max(i-IdSIRCommand,1)
            call ExtractFileName(t256,CallSIR(k))
            call ExtractDirectory(t256,DirSIR(k))
          else if(i.ge.IdExpoCommand.and.i.le.IdExpo2014Command) then
            t256=t256(j+1:)
            call SkrtniPrvniMezery(t256)
            call ExtractFileName(t256,CallExpo(i-IdExpoCommand+1))
            call ExtractDirectory(t256,DirExpo(i-IdExpoCommand+1))
          else if(i.eq.IdShelxtCommand) then
            CallShelxt=t256(j+1:)
            call SkrtniPrvniMezery(CallShelxt)
          else if(i.eq.IdGeminographyCommand) then
            CallGeminography=t256(j+1:)
            call SkrtniPrvniMezery(CallGeminography)
          else if(i.eq.IdDelejKontroly) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) DelejKontroly=i
          else if(i.eq.IdVasekTest) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) VasekTest=i
          else if(i.eq.IdVasekAbs) then
            call StToInt(t256,j,iap,1,.false.,ich)
            if(ich.eq.0) VasekAbs=i
          endif
          go to 2000
        else
          k=1
          call GetPathTo('x-shape.exe','xshape',CallXShape)
          call GetPathTo('diamond.exe','Diamond 3',CallGraphic)
          if(CallGraphic.eq.' ') then
            call GetPathTo('diamond.exe','Diamond2',CallGraphic)
            if(CallGraphic.eq.' ') call GetPathTo('atoms.exe','atoms*',
     1                                            CallGraphic)
          endif
          call GetPathTo('SIR97.exe','SIR97',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(1))
            call ExtractDirectory(t256,DirSIR(1))
          endif
          call GetPathTo('SIR2002.exe','SIR2002',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(2))
            call ExtractDirectory(t256,DirSIR(2))
          endif
          call GetPathTo('SIR2004.exe','SIR2004',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallSIR(3))
            call ExtractDirectory(t256,DirSIR(3))
          endif
          call GetPathTo('expo.exe','expo',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallExpo(1))
            call ExtractDirectory(t256,DirExpo(1))
          endif
          call GetPathTo('expo2004.exe','expo2004',t256)
          if(t256.ne.' ') then
            call ExtractFileName(t256,CallExpo(2))
            call ExtractDirectory(t256,DirExpo(2))
          endif
          ErrFlag=0
          go to 1000
        endif
      else
        go to 4000
      endif
3000  call CloseIfOpened(ln)
4000  if(k.eq.1) then
        ln=NextLogicNumber()
        if(VasekDebug.ne.0) write(44,'(''PARAMS - as written to "'',a,
     1                          ''"'')') FileNameIni(:idel(FileNameIni))
        open(ln,file=FileNameIni,err=9100)
      else
        ln=0
      endif
      do i=1,90
        if(KeyWord(i)(1:1).eq.'#') cycle
        if(i.eq.IdWindowWidth) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowWidthS
          else
            j=PixelWindowWidthE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowHeight) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowHeightS
          else
            j=PixelWindowHeightE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowColors) then
          cycle
        else if(i.eq.IdWindowX) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowXPosS
          else
            j=PixelWindowXPosE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowY) then
          if(PixelWindowXPosE.lt.-30000.or.PixelWindowYPosE.lt.-30000)
     1      then
            j=PixelWindowYPosS
          else
            j=PixelWindowYPosE
          endif
          write(t256,FormI15) j
        else if(i.eq.IdWindowSizeType) then
          t256=SizeType(WindowSizeType)
        else if(i.eq.IdFontSize) then
          write(t256,FormI15) PropFontSize
        else if(i.eq.IdUseViewer) then
          if(BuildInViewer) then
            t256='1'
          else
            t256='0'
          endif
        else if(i.eq.IdICDDKey) then
          write(t256,FormI15) ICDDKey
        else if(i.eq.IdICDDDir) then
          t256=ICDDDir
          go to 4100
        else if(i.eq.IdWineKey) then
          if(WineKey.eq.0) cycle
          write(t256,FormI15) WineKey
        else if(i.eq.IdTmpDir) then
          t256=TmpDir
          go to 4100
        else if(i.eq.IdCIFSpecFile) then
          t256=CIFSpecificFile
          go to 4100
        else if(i.eq.IdPSPrinter) then
          t256=PSPrinter
          go to 4100
        else if(i.eq.IdEditorCommand) then
          t256=EditorName
          go to 4100
        else if(i.eq.IdDosShellCommand) then
          t256=FirstCommand
          go to 4100
        else if(i.eq.IdXShapeCommand) then
          t256=CallXShape
          go to 4100
        else if(i.eq.IdGraphicCommand) then
          t256=CallGraphic
          go to 4100
        else if(i.eq.IdGnuplotCommand) then
          t256=CallGnuplot
          go to 4100
        else if(i.eq.IdGSCommand) then
          t256=CallGS
          go to 4100
        else if(i.eq.Id3dMapsCommand) then
          t256=Call3dMaps
          go to 4100
        else if(i.eq.IdDPlotCommand) then
          t256=CallDPlot
          go to 4100
        else if(i.eq.IdPathToELMAM) then
          t256=PathToELMAM
          go to 4100
        else if(i.ge.IdSIRCommand.and.i.le.IdSIR2014Command) then
          k=max(i-IdSIRCommand,1)
          t256=DirSIR(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallSIR(k)(:idel(CallSIR(k)))
          endif
          go to 4100
        else if(i.ge.45.and.i.le.50) then
          k=max(i-45,1)
          t256=DirSIR(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallSIR(k)(:idel(CallSIR(k)))
          endif
          go to 4100
        else if(i.ge.IdExpoCommand.and.i.le.IdExpo2014Command) then
          k=i-IdExpoCommand+1
          t256=DirExpo(k)
          j=idel(t256)
          if(j.gt.0) then
            if(t256(j:j).ne.'\') then
              j=j+1
              t256(j:j)='\'
            endif
            t256=t256(:j)//CallExpo(k)(:idel(CallExpo(k)))
          endif
          go to 4100
        else if(i.eq.IdShelxtCommand) then
          t256=CallShelxt
          go to 4100
        else if(i.eq.IdGeminographyCommand) then
          t256=CallGeminography
          go to 4100
        else if(i.eq.IdDelejKontroly) then
          if(DelejKontroly.ne.1) cycle
          write(t256,FormI15) DelejKontroly
        else if(i.eq.IdVasekTest) then
          if(VasekTest.eq.0) cycle
          write(t256,FormI15) VasekTest
        else if(i.eq.IdVasekAbs) then
          if(VasekAbs.eq.0) cycle
          write(t256,FormI15) VasekAbs
        endif
        call ZdrcniCisla(t256,1)
4100    t256=MasterName(:idel(MasterName))//'.'//
     1       KeyWord(i)(:idel(KeyWord(i)))//': '//t256(:idel(t256))
        if(ln.ne.0)
     1    write(ln,FormA1,err=9010)(t256(j:j),j=1,idel(t256))
        if(VasekDebug.ne.0) write(44,FormA1)(t256(j:j),j=1,idel(t256))
      enddo
      if(VasekDebug.ne.0) write(44,'(''PARAMS - end'')')
      call CloseIfOpened(ln)
      go to 9999
9000  TextInfo(1)=MasterName(:idel(MasterName))//'.ini : reading error.'
      t256='Please check if the file exist and has proper attributes.'
      go to 9090
9010  TextInfo(1)=MasterName(:idel(MasterName))//'.ini : writing error.'
      t256='Please check if the file is not protected or "read only".'
9090  call CloseIfOpened(ln)
      go to 9150
9100  TextInfo(1)=MasterName(:idel(MasterName))//
     1            'DIR defined but directory isn''t accessible.'
      t256='Please correct the set command (SET '//
     1     MasterName(:idel(MasterName))//'DIR=....) '//
     1     'or make the directory "'//HomeDir(:idel(HomeDir))//
     2     '" accessible.'
9150  call FeChybne(-1.,-1.,TextInfo(1),t256,FatalError)
9999  if(PixelWindowXPosE.gt.-30000.and.PixelWindowYPosE.gt.-30000)
     1  then
        PixelWindowWidthS=PixelWindowWidthE
        PixelWindowHeightS=PixelWindowHeightE
        PixelWindowXPosS=PixelWindowXPosE
        PixelWindowYPosS=PixelWindowYPosE
      endif
      return
101   format(f15.6)
      end
