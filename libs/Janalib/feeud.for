      subroutine FeEud
      include 'fepc.cmn'
      integer ColorArrow,ColorUpDown,ColorRD,ColorLU,ColorObtah
      entry FeEudMake(id)
      ydw=EdwYMax(id)-EdwYMin(id)
      ydwp=anint(ydw*.5*EnlargeFactor-3.)/EnlargeFactor
      EdwUpDownXMin(id)=anint(EdwXMax(id)*EnlargeFactor+5.)/
     1                  EnlargeFactor
      EdwUpDownXMax(id)=Anint((EdwUpDownXMin(id)+ydw*.6)*EnlargeFactor)/
     1                  EnlargeFactor
      EdwUpDownYMin(1,id)=anint((EdwYMin(id)+ydwp)*EnlargeFactor+5.)/
     1                    EnlargeFactor
      EdwUpDownYMax(1,id)=EdwUpDownYMin(1,id)+ydwp
      EdwUpDownYMin(2,id)=EdwYMin(id)
      EdwUpDownYMax(2,id)=EdwUpDownYMin(2,id)+ydwp
      go to 9999
      entry FeEudOff(id,iud)
      ColorUpDown=LightGray
      ColorArrow=Black
      ColorLU=White
      ColorRD=Gray
      ColorObtah=Gray
      PressShift=0.
      go to 2000
      entry FeEudOn(id,iud)
      ColorUpDown=LightGray
      ColorArrow=Black
      ColorLU=Gray
      ColorRD=White
      ColorObtah=Gray
      PressShift=1.
      go to 2000
      entry FeEudClose(id,iud)
      entry FeEudRemove(id,iud)
      ColorUpDown=LightGray
      ColorArrow=LightGray
      ColorLU=LightGray
      ColorRD=LightGray
      ColorObtah=LightGray
      PressShift=0.
2000  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(EdwUpDownXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(EdwUpDownXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(EdwUpDownYMin(iud,id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(EdwUpDownYMax(iud,id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorUpDown)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
      pomx=anint((EdwUpDownXMax(id)+EdwUpDownXMin(id))*.5*
     1           EnlargeFactor)
      pomy=anint((EdwUpDownYMax(iud,id)+EdwUpDownYMin(iud,id))*.5*
     1           EnlargeFactor)
      fnx=4.
      fny=2.
      xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
      xu(2)=anint(pomx+PressShift)/EnlargeFactor
      xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
      if(iud.eq.1) then
        yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
      else
        yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
      endif
      yu(3)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,ColorArrow)
9999  return
      end
