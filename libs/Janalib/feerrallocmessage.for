      subroutine FeErrAllocMessage(String1,String2)
      include 'fepc.cmn'
      character*(*) String1,String2
      NInfo=2
      TextInfo(1)='It has happened during allocation of '//
     1            String1(:idel(String1))
      TextInfo(2)=String2
      call FeInfoOut(-1.,-1.,'ALLOCATION ERROR','L')
      return
      end
