      subroutine FeOutStUnder(id,xmi,ymi,Text,Justify,Color,
     1                        UnderLineColor,StFlag)
      include 'fepc.cmn'
      character*1 Justify
      character*(*) text
      character*256 Text1,Veta
      character*1 StFlag
      integer Color,UnderLineColor
      xm=xmi
      ym=ymi
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xm+QuestXMin(id)
          ym=ym+QuestYMin(id)
        endif
      endif
      StFlag=' '
      Veta=Text
      n=index(Veta,'%')
      idlt=idel(Veta)
      if(n.gt.1) then
        if(Veta(n-1:n-1).eq.ObrLom) then
          Text1=Veta(:n-1)//Veta(n+1:)
          n=0
        endif
      else if(n.le.0.or.n.ge.idlt) then
        Text1=Veta
        n=0
      endif
      if(n.ne.0) then
        StFlag=Veta(n+1:n+1)
        Text1=Veta(:n-1)//Veta(n+1:)
        if(n.eq.1) then
          xu(1)=xm
        else
          xu(1)=xm+FeTxLengthSpace(Text1(:n-1))+1.
        endif
        xu(2)=xm+FeTxLength(Text1(:n))
        yu(1)=ym-6.
        yu(2)=ym-6.
        pom=FeTxLength(Text1)
        if(Justify.ne.'L') then
          if(Justify.eq.'C') pom=pom*.5
          xu(1)=xu(1)-pom
          xu(2)=xu(2)-pom
        endif
        call FePolyline(2,xu,yu,Color)
      else
        StFlag=' '
      endif
      call FeOutSt(id,xm,ym,Text1,Justify,Color)
      call mala(StFlag)
      return
      end
