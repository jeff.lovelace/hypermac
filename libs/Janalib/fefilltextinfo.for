      subroutine FeFillTextInfo(FileTxt,Label)
      include 'fepc.cmn'
      character*(*) FileTxt
      character*128  Veta
      logical EqIgCase
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        WorkString=HomeDir(:idel(HomeDir))//'txt'//ObrLom//
     1             FileTxt(:idel(FileTxt))
      else
        WorkString=HomeDir(:idel(HomeDir))//'txt/'//
     1             FileTxt(:idel(FileTxt))
      endif
      call OpenFile(ln,WorkString,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      NInfo=0
      if(Label.ne.0) then
        write(Cislo,'(''block#'',i2)') Label
        call Zhusti(Cislo)
1000    read(ln,FormA,end=9999) Veta
        if(.not.EqIgCase(Veta,Cislo)) go to 1000
        Cislo='end '//Cislo(:idel(Cislo))
      endif
1500  NInfo=NInfo+1
      read(ln,FormA,end=2000) Veta
      if(Label.ne.0) then
        if(EqIgCase(Veta,Cislo)) go to 2000
      endif
      TextInfo(NInfo)=Veta
      TextInfoFlags(NInfo)='LN'
      go to 1500
2000  NInfo=NInfo-1
      if(TextInfo(NInfo).eq.' ') go to 2000
9999  call CloseIfOpened(ln)
      return
      end
