      subroutine FeUpdateParamAndKeys(nEdwFirst,nCrwFirst,Param,KiParam,
     1                                n)
      include 'fepc.cmn'
      dimension Param(n),KiParam(n)
      integer EdwStateQuest,CrwStateQuest
      logical CrwLogicQuest
      if(nEdwFirst.eq.0.or.nCrwFirst.eq.0) go to 9999
      nEdw=nEdwFirst
      nCrw=nCrwFirst
      do i=1,n
        if(EdwStateQuest(nEdw).eq.EdwOpened)
     1    call FeQuestRealFromEdw(nEdw,Param(i))
        if(CrwStateQuest(nCrw).eq.CrwOff.or.
     1     CrwStateQuest(nCrw).eq.CrwOn) then
          if(CrwLogicQuest(nCrw)) then
            KiParam(i)=1
          else
            KiParam(i)=0
          endif
        else
          KiParam(i)=0
        endif
        if(CrwStateQuest(nCrw).eq.CrwLocked) KiParam(i)=KiParam(i)+10
        if(EdwStateQuest(nEdw).eq.EdwLocked) KiParam(i)=KiParam(i)+100
        nEdw=nEdw+1
        nCrw=nCrw+1
      enddo
9999  return
      end
