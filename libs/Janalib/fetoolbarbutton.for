      subroutine FeToolBarButton(xp,xk,yp,yk,Text,State,klic)
      include 'fepc.cmn'
      character*(*) Text
      character*1 StFlag
      integer ColorRD,ColorLU,State
      if(State.eq.klic) go to 9999
      xpX=xp+2.
      xkX=xk-2.
      ypX=yp+2.
      ykX=yk-2.
      wp=xkX-xpX
      hp=ykX-ypX
      if(klic.eq.0) then
        ColorRD=LightGray
        ColorLU=LightGray
        xt=0.
        yt=0.
      else if(klic.lt.0) then
        ColorLU=White
        ColorRD=Gray
        xt=0.
        yt=0.
      else
        ColorRD=White
        ColorLU=Gray
        xt= 1.
        yt=-1.
      endif
      call FeDeferOutput
      call FeDrawFrame(xpX,ypX,wp,hp,2.,ColorRD,ColorLU,Black,.false.)
      call FeFillRectangle(xpX,xkX,ypX,ykX,4,0,0,LightGray)
      call FeOutStUnder(0,xp+10.+xt,(yp+yk)*.5+yt,Text,'L',Black,Black,
     1                  StFlag)
      State=klic
9999  return
      end
