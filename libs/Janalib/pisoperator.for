      subroutine PisOperator(ln,rm,s,zn,n)
      include 'fepc.cmn'
      dimension rm(n,n),s(n)
      character*28 FormOut
      data FormOut/'(1x,'' |'',3i3,'' |'',f6.3,'' |'')'/
      write(FormOut(10:10),'(i1)') n
      write(ln,FormOut)((nint(zn*rm(i,j)),j=1,n),s(i),i=1,n)
      return
      end
