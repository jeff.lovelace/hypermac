      subroutine FeSbwAction(id,Nacitat,ich)
      include 'fepc.cmn'
      character*256 t256
      character*1 Zn
      integer BackgroundColor,TextColor
      logical Konec,Nacitat
      ich=0
      if(SbwType(id).ne.SbwPureType) then
        n=0
        if(Nacitat) then
          ln=SbwLn(id)
          rewind ln
          Konec=.false.
          do i=1,SbwItemFrom(id)-1
            n=n+1
            read(ln,FormA1,end=9999) Zn
          enddo
        endif
        if(SbwType(id).eq.SbwTextType) then
          xd=0.
          xpom=SbwXStripe(1,id)
          ypom=SbwYMax(id)-SbwYStep(id)*.5
          xpom=xpom+1.
        else
          xpom=SbwXStripe(1,id)
          ypom=SbwYMax(id)
          xd=SbwXStripe(2,id)-SbwXStripe(1,id)
        endif
        call FeFillRectangle(SbwXMin(id),SbwXMax(id),SbwYMin(id),
     1                       SbwYMax(id),4,0,0,White)
        do i=1,min(SbwLines(id)*SbwStripes(id),SbwItemNMax(id))
          n=n+1
          if(SbwType(id).eq.SbwTextType) then
            call FeSetFixFont
            call FeWrLine(xpom,ypom,xd,SbwItemShort(i,id),White,White)
            call FeSetPropFont
          endif
          if(.not.Konec.and.Nacitat) then
            read(ln,FormA,end=2200) SbwItem(i,id)
            t256=SbwItem(i,id)
            j=index(t256,'#$color')
            if(j.gt.0) then
              Cislo=t256(j+7:)
              call posun(Cislo,0)
              read(Cislo,FormI15) SbwItemColor(i,id)
              t256=t256(:j-1)
              SbwItem(i,id)=t256
            else
              SbwItemColor(i,id)=Black
            endif
            call FeCutNameLength(t256,SbwItemShort(i,id),
     1        SbwColumnsXLen(id),SbwCutText(id))
          endif
          go to 2500
2200      Konec=.true.
2500      if(SbwType(id).eq.SbwTextType) call FeSetFixFont
          call FeWrLine(xpom,ypom,xd,' ',Black,White)
          if(SbwType(id).eq.SbwTextType) call FeSetPropFont
          if(.not.Konec) then
            if(n.eq.SbwItemPointer(id)) then
              if(SbwType(id).eq.SbwMenuType) then
                BackGroundColor=DarkBlue
                if(SbwItemColor(i,id).eq.Black) then
                  TextColor=White
                else
                  TextColor=SbwItemColor(i,id)
                endif
              else if(SbwType(id).eq.SbwSelectType) then
                BackGroundColor=SnowGray
                if(SbwItemSel(n,id).eq.1) then
                  TextColor=Red
                else
                  if(SbwItemSel(n,id).eq.0) then
                    TextColor=SbwItemColor(i,id)
                  else
                    TextColor=Gray
                  endif
                endif
              endif
            else
              BackgroundColor=White
              if(SbwType(id).eq.SbwMenuType) then
                TextColor=SbwItemColor(i,id)
              else if(SbwType(id).eq.SbwSelectType) then
                if(SbwItemSel(n,id).eq.1) then
                  TextColor=Red
                else
                  if(SbwItemSel(n,id).eq.0) then
                    TextColor=SbwItemColor(i,id)
                  else
                    TextColor=Gray
                  endif
                endif
              else
                TextColor=Black
              endif
            endif
            if(SbwType(id).ne.SbwPureType) then
              if((SbwBars(id).eq.SbwHorizontal.or.
     1            SbwBars(id).eq.SbwBoth).and.SbwStripes(id).le.1) then
                t256=SbwItem(i,id)
              else
                t256=SbwItemShort(i,id)
              endif
              if(SbwType(id).eq.SbwTextType) call FeSetFixFont
              call FeWrLine(xpom,ypom,xd,t256,TextColor,BackgroundColor)
              if(SbwType(id).eq.SbwTextType) call FeSetPropFont
            endif
          endif
          if(mod(i,SbwLines(id)).le.0) then
            j=i/SbwLines(id)+1
            xpom=SbwXStripe(j,id)
            if(SbwType(id).eq.SbwTextType) then
              xpom=xpom+1.
              ypom=SbwYMax(id)-SbwYStep(id)*.5
              xd=0.
            else
              ypom=SbwYMax(id)
              xd=SbwXStripe(j+1,id)-xpom
            endif
          else
            ypom=ypom-SbwYStep(id)
          endif
        enddo
        do i=2,SbwStripes(id)
          xu(1)=SbwXStripe(i,id)
          xu(2)=xu(1)
          yu(1)=SbwYMin(id)
          yu(2)=SbwYMax(id)
          call FePolyLine(2,xu,yu,Black)
        enddo
      else
        call FeSbwActionSpecial(id)
      endif
      go to 9999
9900  ich=1
9999  return
      end
