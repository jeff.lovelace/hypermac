      subroutine FeQuestActiveUpdate
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      if(ActiveObj.le.0) then
        if(SbwTo.ge.SbwFr) then
          ActiveObj=10000*ActSbw+SbwTo
          go to 1200
        else if(EdwTo.ge.EdwFr) then
          do nEdw=EdwFr,EdwTo
            if(EdwState(nEdw).eq.EdwOpened) then
              ActiveObj=10000*ActEdw+nEdw
              go to 1200
            endif
          enddo
        endif
        if(ButtonOK.ne.0) then
          i=ButtonOK
        else if(ButtonESC.ne.0) then
          i=ButtonESC
        else
          go to 9999
        endif
        ActiveObj=10000*ActButton+i
1200    call FeQuestActiveObjOn
        go to 9999
      endif
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        if(ButtonState(j).eq.ButtonClosed.or.
     1     ButtonState(j).eq.ButtonDisabled) go to 1500
      else if(i.eq.ActEdw) then
        if(EdwState(j).eq.EdwClosed.or.
     1     EdwState(j).eq.EdwDisabled) go to 1500
      else if(i.eq.ActCrw) then
        if(CrwState(j).eq.CrwClosed.or.
     1     CrwState(j).eq.CrwDisabled) go to 1500
      else if(i.eq.ActRolMenu) then
        if(RolMenuState(j).eq.RolMenuClosed.or.
     1     RolMenuState(j).eq.RolMenuDisabled) go to 1500
      else if(i.eq.ActSbw) then
        if(SbwState(j).eq.SbwClosed) go to 1500
      else if(ActiveObj.eq.0) then
        go to 1500
      endif
      go to 9999
1500  i=LocateInIntArray(ActiveObj,ActiveObjList(ActiveObjFr),
     1                   ActiveObjTo-ActiveObjFr+1)
2200  if(i.gt.0) then
        j=i+ActiveObjFr
        if(j.gt.ActiveObjTo) j=ActiveObjFr
      else
        j=ActiveObjFr
      endif
      ActiveObj=ActiveObjList(j)
      call FeQuestActiveObjOn
      if(ActiveObj.lt.0) then
        i=mod(i,ActiveObjTo-ActiveObjFr+1)+1
        go to 2200
      endif
      go to 9999
9000  ActiveObj=-1
9999  return
      end
