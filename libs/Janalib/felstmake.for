      subroutine FeLstMake(xmi,ymi,idx,idy,Type,Header)
      include 'fepc.cmn'
      character*128  LstText(100),LstTextOld(100)
      character*1 FirstChar
      character*(*) Text,Header
      logical :: Overwrite=.false.
      integer Type
      save xd,yd,xm,ym,LstLine,LstLineMax,LstText,LstTextOld,LstFirst,
     1     MaxLength
      LstLine=0
      LstFirst=1
      if(Console) go to 9999
      if(.not.DeferredOutput) call FeDeferOutput
      call SetStringArrayTo(LstText,100,' ')
      LstLineMax=idy
      xd=float(idx)*FixFontWidthInPixels
      MaxLength=idx
      yd=float(idy)*FixFontHeightInPixels+FixFontHeightInPixels*.5
      if(xmi.lt.0.) then
        xm=XCenGrWin-xd*.5
      else
        xm=xmi
      endif
      if(ymi.lt.0.) then
        ym=YCenGrWin-yd*.5
      else
        ym=ymi-yd
      endif
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,White)
      if(Type.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,LightGray,Black,
     1                   LastQuest.ne.0)
      endif
      call SetStringArrayTo(LstTextOld,100,' ')
      if(Header.ne.' ') then
        LstText(1)=Header
        LstText(1)(MaxLength+1:)=' '
        LstLine=1
        LstFirst=2
      endif
      go to 9000
      entry FeLstBackSpace
      LstLine=LstLine-1
      if(Console) Overwrite=.true.
      go to 9000
      entry FeLstWriteLine(Text,il)
      if(Console) then
        if(il.eq.0.or.Overwrite) then
          FirstChar='+'
        else
          FirstChar=' '
        endif
        LstText(1)=' '
        do i=1,idel(Text)
          if(Text(i:i).eq.Tabulator) then
            LstText(1)(i:i)=' '
          else
            LstText(1)(i:i)=Text(i:i)
          endif
        enddo
        if(OpSystem.le.0) then
          write(6,FormA) FirstChar//LstText(1)(:idel(LstText(1)))
        else
          if(FirstChar.eq.'+') then
            write(6,FormA,advance='no') LstText(1)(:idel(LstText(1)))
          else
            write(6,FormA) LstText(1)(:idel(LstText(1)))
          endif
        endif
        Overwrite=.false.
      else
        if(il.eq.0) then
          ila=max(LstLine,0)
          if(ila.le.0) ila=1
        else if(il.lt.0) then
          ila=LstLine+1
        else
          ila=il
        endif
        if(ila.gt.LstLineMax) then
          do i=LstFirst+1,LstLineMax
            LstText(i-1)=LstText(i)
          enddo
          ila=LstLineMax
        endif
        LstText(ila)=' '
        do i=1,idel(Text)
          if(Text(i:i).eq.Tabulator) then
            LstText(ila)(i:i)=' '
          else
            LstText(ila)(i:i)=Text(i:i)
          endif
        enddo
        LstText(ila)(MaxLength+1:)=' '
        LstLine=max(LstLine,ila)
        ypom=ym+yd-.6667*FixFontHeightInPixels
        xpom=xm+3.
        do i=1,LstLine
          call FeRewritePureString(0,xpom,ypom,LstTextOld(i),LstText(i),
     1                             'L',White,Black)
          LstTextOld(i)=LstText(i)
          LstTextOld(i)(MaxLength+1:)=' '
          ypom=ypom-FixFontHeightInPixels
        enddo
      endif
      go to 9000
      entry FeLstRemove
9000  if(Console) go to 9999
      if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  return
      end
