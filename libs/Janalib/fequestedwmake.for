      subroutine FeQuestEdwMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                          Check)
      include 'fepc.cmn'
      character*(*) Text
      character*1   Justify
      integer Check,Color
      logical Erase
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 1000
      entry FeQuestAbsEdwMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check)
      yt=yti
      yw=ywi
1000  ient=0
      go to 2000
      entry FeQuestEudMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,Check)
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 1100
      entry FeQuestAbsEudMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check)
      yt=yti
      yw=ywi
1100  ient=1
2000  EdwTo=EdwTo+1
      EdwLastMade=EdwTo-EdwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActEdw+EdwTo
      QuestEdwTo(id)=EdwTo
      EdwCheck(EdwTo)=Check
      EdwLabel(EdwTo)=Text
      EdwJustify(EdwTo)=Justify
      EdwLabelX(EdwTo)=xt+QuestXMin(LastQuest)
      EdwLabelY(EdwTo)=yt+QuestYMin(LastQuest)
      ym=yw
      call FeEdwMake(EdwTo,id,xw,ym,xdw,ydw)
      if(ient.eq.0) then
        EdwUpDown(EdwTo)=0
      else
        EdwUpDown(EdwTo)=1
        call FeEudMake(EdwTo)
      endif
      go to 9999
      entry FeQuestEudOpen(iwa,imn,imx,istep,rmn,rmx,rstep)
      iwp=iwa+EdwFr-1
      if(EdwType(iwp).lt.0) then
        EdwInt(2,iwp)=imn
        EdwInt(3,iwp)=imx
        EdwInt(4,iwp)=istep
        EdwInt(5,iwp)=nint(float(imx-imn)/float(istep))
      else if(EdwType(iwp).gt.0) then
        EdwReal(2,iwp)=rmn
        EdwReal(3,iwp)=rmx
        EdwReal(4,iwp)=abs(rstep)
      endif
      call FeCheckEud(iwp,0)
      go to 9999
      entry FeQuestEdwLock(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwLocked) call FeEdwLock(iwp)
      go to 9999
      entry FeQuestEdwDisable(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwDisabled) call FeEdwDisable(iwp)
      Color=White
      go to 6000
      entry FeQuestEdwClose(iwa)
      if(iwa.le.0) go to 9999
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwClosed.and.
     1   EdwState(iwp).ne.EdwRemoved) then
        Erase=.true.
        call FeEdwClose(iwp)
      else
        Erase=.false.
      endif
      go to 4000
      entry FeQuestEdwRemove(iwa)
      iwp=iwa+EdwFr-1
      if(EdwState(iwp).ne.EdwRemoved) then
        Erase=EdwState(iwp).ne.EdwClosed
        call FeEdwRemove(iwp)
      else
        Erase=.false.
      endif
      go to 4000
      entry FeQuestEdwSelect(iwa)
      call FeEdwSelect(iwa+EdwFr-1)
      go to 9999
      entry FeQuestEdwDeselect(iwa)
      call FeEdwDeselect(iwa+EdwFr-1)
      go to 9999
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActEdw.and.j.eq.iwp) then
        call FeEdwDeactivate(iwp)
        ActiveObj=0
      endif
5000  if(EdwLabel(iwp).ne.' '.and.Erase) then
        call FeTextErase(0,EdwLabelX(iwp),EdwLabelY(iwp),
     1                   EdwLabel(iwp),EdwJustify(iwp),LightGray)
      endif
      go to 9999
6000  if(EdwLabel(iwp).ne.' ') then
        call FeTextErase(0,EdwLabelX(iwp),EdwLabelY(iwp),EdwLabel(iwp),
     1                   EdwJustify(iwp),LightGray)
        call FeOutStUnder(0,EdwLabelX(iwp),EdwLabelY(iwp),EdwLabel(iwp),
     1                    EdwJustify(iwp),Color,Color,EdwZ(iwp:iwp))
      endif
9999  return
      end
