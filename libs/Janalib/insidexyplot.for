      logical function InsideXYPlot(xx,yy)
      include 'fepc.cmn'
      dimension x1(3),x2(3),x3(3),x4(3)
      InsideXYPlot=.true.
      x1(1)=xx
      x1(2)=yy
      x1(3)=0.
      do i=1,4
        if(i.lt.4) then
          k=i+1
        else
          k=1
        endif
        do j=1,3
          x2(j)=x1(j)-XCornAcWin(j,i)
          x3(j)=XCornAcWin(j,k)-XCornAcWin(j,i)
        enddo
        call VecMul(x2,x3,x4)
        if(x4(3).le.-.5) then
          InsideXYPlot=.false.
          exit
        endif
      enddo
      return
      end
