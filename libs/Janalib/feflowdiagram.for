      subroutine FeFlowDiagram(xd,nd,n)
      include 'fepc.cmn'
      dimension xd(n),nd(n)
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        yomx=0.
        do i=1,n
          yomx=max(yomx,float(nd(i)))
        enddo
        yomx=yomx*1.1
        xomn=xd(1)
        xomx=xd(n)
        yomn=0.
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,' ')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'freq')
        do i=1,n-1
          if(nd(i).gt.0) then
            xu(1)=xd(i)
            xu(2)=xd(i)
            xu(3)=xd(i+1)
            xu(4)=xd(i+1)
            yu(1)=0.
            yu(2)=nd(i)
            yu(3)=yu(2)
            yu(4)=0.
            do j=1,4
              xu(j)=FeXo2X(xu(j))
              yu(j)=FeYo2Y(yu(j))
            enddo
            call FePolyLine(4,xu,yu,White)
          endif
        enddo
      endif
      call FeHardCopy(HardCopy,'close')
      return
      end
