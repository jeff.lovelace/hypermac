      subroutine srotc(a,n,b)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(3,3),b(*),ia(6),ja(6)
      m=inm(n-2)
      ioffmn=ioffm(n-2)
      ioffvn=ioffv(n-2)
      idenp=4**(n-1)
      do i=1,m
        k=ipoc(i+ioffmn)+ioffvn
        ip=ipec(k)
        iden=idenp
        do k=1,n
          ia(k)=ip/iden
          ip=ip-ia(k)*iden
          iden=iden/4
        enddo
        ij=i
        do j=1,m
          bij=0.
          idlcj=idlc(j+ioffmn)
          ipocj=ipoc(j+ioffmn)
          kp=ipocj
          kk=kp+idlcj-1
          do 1400k=kp,kk
            ip=ipec(k+ioffvn)
            iden=idenp
            do l=1,n
              ja(l)=ip/iden
              ip=ip-ja(l)*iden
              iden=iden/4
            enddo
            pom=1.
            do l=1,n
              p=a(ia(l),ja(l))
              if(p.eq.0.) go to 1400
              pom=pom*p
            enddo
            bij=bij+pom
1400      continue
          b(ij)=bij
          ij=ij+m
        enddo
      enddo
      return
      end
