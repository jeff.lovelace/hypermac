      subroutine SetCommands(KteryProgram)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeMenu
      character*8  men(3)
      data men/'%Fourier','%Refine','%Dist'/
      StartProgram=.false.
      if(KteryProgram.le.0) then
        KteryProgram=FeMenu(-1.,-1.,men,1,3,1,0)
        if(KteryProgram.le.0) go to 9999
      endif
      if(KteryProgram.eq.1) then
        call FouSetCommands(ich)
      else if(KteryProgram.eq.2) then
        call RefSetCommands
      else if(KteryProgram.eq.3) then
        call DistSetCommands
      endif
9999  return
      end
