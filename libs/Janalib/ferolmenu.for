      subroutine FeRolMenu
      include 'fepc.cmn'
      integer ColorArrow,ColorButt,ColorRD,ColorLU,ColorObtah,ColorSel
      logical Opening
      go to 9999
      entry FeRolMenuMake(id,idc,xm,ym,xd,yd)
      RolMenuTextXMin(id)=xm
      RolMenuTextXMax(id)=xm+xd
      RolMenuTextYMin(id)=ym
      RolMenuTextYMax(id)=ym+yd
      if(idc.gt.0) then
        RolMenuTextXMin(id)=RolMenuTextXMin(id)+QuestXMin(idc)
        RolMenuTextXMax(id)=RolMenuTextXMax(id)+QuestXMin(idc)
        RolMenuTextYMin(id)=RolMenuTextYMin(id)+QuestYMin(idc)
        RolMenuTextYMax(id)=RolMenuTextYMax(id)+QuestYMin(idc)
      endif
      RolMenuButtYMin(id)=RolMenuTextYMin(id)+2.
      RolMenuButtYMax(id)=RolMenuTextYMax(id)-2.
      RolMenuButtXMax(id)=RolMenuTextXMax(id)-2.
      RolMenuButtXMin(id)=RolMenuButtXMax(id)-yd+4.
      RolMenuState(id)=RolMenuClosed
      if(RolMenuButtYMax(id).gt.YCenGrWin) then
        RolMenuType(id)=RolMenuDown
      else
        RolMenuType(id)=RolMenuUp
      endif
      RolMenuAlways(id)=.false.
      go to 9999
      entry FeRolMenuOpen(id)
      Opening=.true.
      call FeDrawFrame(RolMenuTextXMin(id),RolMenuTextYMin(id),
     1                 RolMenuTextXMax(id)-RolMenuTextXMin(id),
     2                 RolMenuTextYMax(id)-RolMenuTextYMin(id),
     3                 2.,White,Gray,Black,.false.)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,White)
      xu(1)=RolMenuTextXMin(id)-1.
      yu(1)=RolMenuTextYMin(id)-1.
      xu(2)=RolMenuTextXMax(id)+1.
      yu(2)=RolMenuTextYMin(id)-1.
      xu(3)=RolMenuTextXMax(id)+1.
      yu(3)=RolMenuTextYMax(id)+1.
      xu(4)=RolMenuTextXMin(id)-1.
      yu(4)=RolMenuTextYMax(id)+1.
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      RolMenuState(id)=RolMenuOpened
      go to 2000
      entry FeRolMenuLock(id)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,LightYellow)
      RolMenuState(id)=RolMenuLocked
      Opening=.false.
      go to 2000
      entry FeRolMenuDisable(id)
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,WhiteGray)
      RolMenuState(id)=RolMenuDisabled
      Opening=.false.
      go to 2000
      entry FeRolMenuOff(id)
      if(RolMenuState(id).ne.RolMenuOpened) go to 9999
      Opening=.false.
2000  ColorButt=LightGray
      ColorArrow=Black
      ColorLU=White
      ColorRD=Gray
      ColorObtah=Gray
      PressShift=0.
      go to 2200
      entry FeRolMenuOn(id)
      if(RolMenuState(id).ne.RolMenuOpened) go to 9999
      Opening=.false.
      ColorButt=LightGray
      ColorArrow=Black
      ColorLU=Gray
      ColorRD=White
      ColorObtah=Gray
      PressShift=1.
2200  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(RolMenuButtXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(RolMenuButtXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(RolMenuButtYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(RolMenuButtYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButt)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
      if(RolMenuState(id).eq.RolMenuRemoved) go to 9999
      pomx=anint((RolMenuButtXMin(id)+RolMenuButtXMax(id))*.5*
     1           EnlargeFactor)
      pomy=anint((RolMenuButtYMin(id)+RolMenuButtYMax(id))*.5*
     1           EnlargeFactor)
      fnx=4.
      fny=3.
      xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
      xu(2)=anint(pomx+PressShift)/EnlargeFactor
      xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
      if(RolMenuType(id).eq.RolMenuUp) then
        yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
      else
        yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
        yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
      endif
      yu(3)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,ColorArrow)
      if(Opening.and.RolMenuSelected(id).gt.0) then
        go to 3000
      else
        go to 9999
      endif
      entry FeRolMenuWriteText(id,NSel)
      RolMenuSelected(id)=NSel
      call FeFillRectangle(RolMenuTextXMin(id),RolMenuTextXMax(id),
     1                     RolMenuTextYMin(id),RolMenuTextYMax(id),
     2                     4,0,0,White)
3000  xp=RolMenuTextXMin(id)+EdwMarginSize
      yp=(RolMenuTextYMin(id)+RolMenuTextYMax(id))*.5
      call FeOutSt(0,xp,yp,RolMenuText(RolMenuSelected(id),id),'L',
     1             Black)
      go to 9999
      entry FeRolMenuClose(id)
      i=RolMenuClosed
      go to 4000
      entry FeRolMenuRemove(id)
      i=RolMenuRemoved
4000  if(RolMenuState(id).eq.i) go to 9999
      dx=2.
      pom1=anint(RolMenuTextXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(RolMenuTextXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(RolMenuTextYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(RolMenuTextYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      RolMenuState(id)=i
      RolMenuSelected(id)=0
      go to 9999
      entry FeRolMenuActivate(id)
      ColorSel=Black
      go to 8000
      entry FeRolMenuDeactivate(id)
      ColorSel=LightGray
8000  x1=RolMenuLabelXMin(id)
      x2=RolMenuLabelXMax(id)
      y1=RolMenuLabelYMin(id)
      y2=RolMenuLabelYMax(id)
      xu(1)=x1-1.
      yu(1)=y1-1.
      xu(2)=xu(1)
      yu(2)=y2+1.
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorSel)
      xu(3)=x2+1.
      xu(4)=xu(3)
      call FePolyLine(2,xu(3),yu,ColorSel)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorSel)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorSel)
      call FeLineType(NormalLine)
9999  return
      end
