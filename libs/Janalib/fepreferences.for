      subroutine FePreferences(IGrOn)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 IniFileSave,IniFilePrev
      character*80  Veta
      integer EdwIntQuest,PixelWindowXPosIn,PixelWindowYPosIn,
     1        PropFontSizeIn
      logical ChangeWindowSize,ChangeWindowPosition,CheckSet,
     1        CrwLogicQuest,Prvne,FeYesNo,ChangeFont,FileDiff,
     2        ExistFile,ReturnPrev
      IGrOn=1
      PixelWindowXPosIn=PixelWindowXPos
      PixelWindowYPosIn=PixelWindowYPos
      PropFontSizeIn=PropFontSize
      IniFileSave='jins'
      call CreateTmpFile(IniFileSave,i,0)
      call FeTmpFilesAdd(IniFileSave)
      IniFilePrev='jinp'
      call CreateTmpFile(IniFilePrev,i,0)
      call FeTmpFilesAdd(IniFilePrev)
1000  call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini',IniFileSave)
      Prvne=.true.
      MinHeight=PixelScreenHeight/2
      MinHeight=MinHeight-mod(MinHeight,3)
      MinWidth=(MinHeight*3)/4
      id=NextQuestId()
      xqd=600.
      il=8
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Preferences',0,LightGray,
     1                   0,0)
      xpom=10.+CrwgXd
      tpom=5.
      Veta='%Normal window'
      il=0
      do i=1,4
        il=il+1
        call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        if(i.eq.1) then
          nCrwWndNormal=CrwLastMade
          Veta='%Minimal window'
        else if(i.eq.2) then
          nCrwWndMinimal=CrwLastMade
          Veta='F%ull screen'
        else if(i.eq.3) then
          nCrwWndMaximal=CrwLastMade
          Veta='Exactl%y'
        else if(i.eq.4) then
          nCrwWndExactly=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.WindowSizeType)
      enddo
      xpom=xpom+FeTxLength('Exactly')+10.
      call FeQuestLblMake(id,xpom,4,'=>','L','N')
      xpomt=xpom+FeTxLength('XX')+5.
      xpom=xpomt+FeTxLength('Height')+2.+CrwgXd
      dpom=100.
      call FeQuestEudMake(id,xpomt,il,xpom,il,'%Height','L',dpom,EdwYd,
     1                    1)
      nEdwHeight=EdwLastMade
      il=il+1
      call FeQuestEudMake(id,xpomt,il,xpom,il,'%Width','L',dpom,EdwYd,
     1                    1)
      nEdwWidth=EdwLastMade
      tpom=xpomt+220.
      if(PropFontMeasured.eq.FontInPixels) then
        Veta='%Font height in pixels:'
      else
        Veta='%Font height in points:'
      endif
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPropFontSize=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,PropFontSize,.false.)
      if(PropFontMeasured.eq.FontInPixels) then
        call FeQuestEudOpen(EdwLastMade,15,25,1,0.,0.,0.)
      else
        call FeQuestEudOpen(EdwLastMade,8,14,1,0.,0.,0.)
      endif
      xpomw=xqd*.5-50.
      ilk=il
      il=1
      Veta='Window position'
      call FeQuestLblMake(id,xpomw,il,Veta,'L','N')
      xpomt=xpomw+FeTxLength(Veta)+6.
      Veta='X:'
      xpom=xpomt+FeTxLength(Veta)+6.
      dpomb=50.
      call FeQuestEudMake(id,xpomt,il,xpom,il,Veta,'L',dpomb,EdwYd,0)
      nEdwXpos=EdwLastMade
      il=il+1
      tpom=EdwXMinQuest(nEdwXpos)-QuestXMin(id)
      call FeQuestButtonMake(id,tpom,il,dpomb,ButYd,'Center X')
      nButXcen=ButtonLastMade
      xpomt=EdwUpDownXMaxQuest(nEdwXpos)-QuestXMin(id)+15.
      Veta='Y:'
      xpom=xpomt+FeTxLength(Veta)+5.
      il=il-1
      call FeQuestEudMake(id,xpomt,il,xpom,il,Veta,'L',dpomb,EdwYd,0)
      nEdwYpos=EdwLastMade
      il=il+1
      tpom=EdwXMinQuest(nEdwYpos)-QuestXMin(id)
      call FeQuestButtonMake(id,tpom,2,dpomb,ButYd,'Center Y')
      nButYcen=ButtonLastMade
      il=ilk+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,'For listings use '//
     1                        '%build-in viewer','L',CrwXd,CrwYd,0,0)
      nCrwViewer=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,BuildInViewer)
      dpomb=100.
      xpom=xqd*.5-dpomb-10.
      il=il+1
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,'Che%ck setting')
      nButCheckSet=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpomb+20.
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,
     1                       'Return to pre%vious')
      nButReturnPrev=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      NewHeight=PixelWindowHeight
      NewWidth=PixelWindowWidth
      NewWindowSizeType=WindowSizeType
      CheckSet=.false.
      ReturnPrev=.false.
1400  if(CrwLogicQuest(nCrwWndExactly)) then
        call FeQuestIntEdwOpen(nEdwHeight,NewHeight,.false.)
        call FeQuestIntEdwOpen(nEdwWidth,NewWidth,.false.)
        call FeQuestEudOpen(nEdwHeight,MinHeight,PixelScreenHeight,1,
     1                      0.,0.,0.)
        call FeQuestEudOpen(nEdwWidth,MinWidth,PixelScreenWidth,1,
     1                      0.,0.,0.)
      else
        call FeQuestEdwDisable(nEdwHeight)
        call FeQuestEdwDisable(nEdwWidth)
      endif
      if(CrwLogicQuest(nCrwWndMaximal)) then
        PixelWindowXPos=EdwIntQuest(1,nEdwXpos)
        PixelWindowYPos=EdwIntQuest(1,nEdwYpos)
        call FeQuestEdwDisable(nEdwXpos)
        call FeQuestEdwDisable(nEdwYpos)
        call FeQuestButtonDisable(nButXcen)
        call FeQuestButtonDisable(nButYcen)
      else
        call FeQuestIntEdwOpen(nEdwXpos,PixelWindowXPos,.false.)
        call FeQuestEudOpen(nEdwXpos,0,PixelScreenWidth,1,0.,0.,0.)
        call FeQuestIntEdwOpen(nEdwYpos,PixelWindowYPos,.false.)
        call FeQuestEudOpen(nEdwYpos,0,PixelScreenHeight,1,0.,0.,0.)
        call FeQuestButtonOpen(nButXcen,ButtonOff)
        call FeQuestButtonOpen(nButYcen,ButtonOff)
      endif
      if(ExistFile(IniFilePrev)) then
        call FeQuestButtonOff(nButReturnPrev)
      else
        call FeQuestButtonDisable(nButReturnPrev)
      endif
1500  call FeQuestEvent(id,ich)
      Prvne=.false.
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHeight) then
        call FeQuestIntFromEdw(nEdwHeight,NewHeight)
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwWidth) then
        call FeQuestIntFromEdw(nEdwWidth,NewWidth)
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwWndNormal.and.
     2         CheckNumber.le.nCrwWndExactly)) then
        if(CheckNumber.eq.nCrwWndNormal) then
          NewHeight=nint(float(PixelScreenHeight)*.8)
        else if(CheckNumber.eq.nCrwWndMinimal) then
          NewHeight=MinHeight
        endif
        NewWindowSizeType=CheckNumber-nCrwWndNormal+1
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButXcen) then
        i=(PixelScreenWidth-NewWidth)/2
        call FeQuestIntEdwOpen(nEdwXpos,i,.false.)
        call FeQuestButtonOff(nButXcen)
        EventType=EventEdw
        EventNumber=nEdwXpos
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButYcen) then
        i=(PixelScreenHeight-NewHeight)/2
        call FeQuestIntEdwOpen(nEdwYpos,i,.false.)
        call FeQuestButtonOff(nButYcen)
        EventType=EventEdw
        EventNumber=nEdwYpos
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButCheckSet)
     1  then
        ich=0
        CheckSet=.true.
        call CopyFile(HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini',
     1                IniFilePrev)
        go to 2000
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButReturnPrev) then
        ich=0
        ReturnPrev=.true.
        call MoveFile(IniFilePrev,
     1                HomeDir(:idel(HomeDir))//
     2                MasterName(:idel(MasterName))//'.ini')
        call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  ChangeWindowSize=.false.
      ChangeWindowPosition=.false.
      if(ich.eq.0) then
        BuildInViewer=CrwLogicQuest(nCrwViewer)
        if(NewHeight.ne.PixelWindowHeight.or.
     1     NewWidth.ne.PixelWindowWidth.or.
     2     WindowSizeType.ne.NewWindowSizeType) then
          ChangeWindowSize=.true.
        endif
        if(NewWindowSizeType.ne.WindowMaximal) then
          call FeQuestIntFromEdw(nEdwXPos,PixelWindowXPos)
          call FeQuestIntFromEdw(nEdwYPos,PixelWindowYPos)
          ChangeWindowPosition=PixelWindowXPos.ne.PixelWindowXPosIn.or.
     1                         PixelWindowYPos.ne.PixelWindowYPosIn
        else
          PixelWindowXpos=0
          PixelWindowYPos=0
          ChangeWindowPosition=ChangeWindowSize
        endif
        WindowSizeType=NewWindowSizeType
        if(NewWindowSizeType.eq.WindowNormal.or.
     1     NewWindowSizeType.eq.WindowMinimal) then
          PixelWindowHeight=NewHeight-mod(NewHeight,3)
          PixelWindowWidth=(PixelWindowHeight*4)/3
        else if(NewWindowSizeType.eq.WindowExactly) then
          PixelWindowHeight=NewHeight
          PixelWindowWidth=NewWidth
        endif
        call FeQuestIntFromEdw(nEdwPropFontSize,PropFontSize)
        ChangeFont=PropFontSize.ne.PropFontSizeIn
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
2500  call FeQuestRemove(id)
      if(ich.eq.0) then
        if(CheckSet.or.ReturnPrev) then
          call DeletePomFiles
          call FeGrQuit
          call OpenWorkSpace
          call FeMakeGrWin(0.,0.,YBottomMargin,0.)
          if(CheckSet.and.
     1       .not.FileDiff(IniFilePrev,HomeDir(:idel(HomeDir))//
     2                     MasterName(:idel(MasterName))//'.ini'))
     3      call DeleteFile(IniFilePrev)
          go to 1000
        endif
        if(FileDiff(IniFileSave,
     1              HomeDir(:idel(HomeDir))//
     2              MasterName(:idel(MasterName))//'.ini')) then
          if(.not.FeYesNo(-1.,-1.,
     1       'Do you want to save the made changes?',1)) go to 9000
          if(ChangeWindowSize.or.ChangeWindowPosition.or.ChangeFont)
     1      then
            call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                      MasterName(:idel(MasterName))//'.ini')
            call DeletePomFiles
            call FeGrQuit
            IGrOn=0
          endif
        endif
      endif
      go to 9999
9000  call CopyFile(IniFileSave,HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini')
9999  call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      call DeleteFile(IniFileSave)
      call FeTmpFilesClear(IniFileSave)
      call DeleteFile(IniFilePrev)
      call FeTmpFilesClear(IniFilePrev)
      call SetBasicConstants
      call SetFePCConstants
      return
      end
