      subroutine normtr(x,isw,klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*)
      do i=1,NDim(KPhase)
        if(x(i).ge..0001.and.x(i).lt..9999) cycle
        xp=ifix(x(i))
        if(x(i).lt.0.) xp=xp-1.
        x(i)=x(i)-xp
        if(1.-x(i).lt..0001.or.x(i).lt..0001) x(i)=0.
        if(abs(x(i)-.5).lt..0001) x(i)=.5
      enddo
      if(klic.eq.0) return
      do i=1,NDim(KPhase)
        xp=100.
        n=0
        do 3000j=1,NLattVec(KPhase)
          do k=1,i-1
            if(vt6(k,j,isw,KPhase).ne.0.) go to 3000
          enddo
          if(vt6(i,j,isw,KPhase).eq.0.) go to 3000
          if(vt6(i,j,isw,KPhase).lt.xp) then
            n=j
            xp=vt6(i,j,isw,KPhase)
          endif
3000    continue
        if(n.eq.0) cycle
3100    if(x(i).lt.xp-.0001) go to 3200
        do k=i,NDim(KPhase)
          x(k)=x(k)-vt6(k,n,isw,KPhase)
          if(x(k).lt.-.0001.and.k.ne.i) x(k)=x(k)+1.
        enddo
        go to 3100
3200    if(x(i).ge.-.0001) go to 3300
        do k=i,NDim(KPhase)
          x(k)=x(k)+vt6(k,n,isw,KPhase)
          if(x(k).ge.1..and.k.ne.i) x(k)=x(k)-1.
        enddo
        go to 3200
3300    if(abs(x(i)).lt..0001) x(i)=0.
      enddo
      return
      end
