      subroutine FeTxOut(xmi,ymi,text)
      include 'fepc.cmn'
      character*(*) text
      character*80 TextOld,TxoutTmp
      save xm,ym,xd,yd,TextOld,TxOutTmp
      xd=max(50.,FeTxLength(text)+30.)
      yd=40.
      xm=xmi
      ym=ymi
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      TxOutTmp='jtxo'
      if(OpSystem.le.0) call CreateTmpFile(TxOutTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,
     1                 ym-FrameWidth,ym+yd+FrameWidth,TxOutTmp)
      call FeFillRectangle(xm,xm+xd,ym,ym+yd,4,0,0,LightGray)
      call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                 LastQuest.ne.0)
      call FeOutSt(0,xm+10.,ym+yd-20.,Text(:idel(text)),'L',Black)
      TextOld=Text
      go to 9999
      entry FeTxOutCont(Text)
      call FeEvent(1)
      call FeRewriteString(0,xm+10.,ym+yd-20.,TextOld,Text,'L',
     1                     LightGray,Black)
c      if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
c      else
c        call FeFlush
c      endif
      TextOld=Text
      go to 9999
      entry FeTxOutEnd
      if(TxOutTmp.eq.' ') go to 9999
      call FeWait(.5)
      call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,
     1                 ym-FrameWidth,ym+yd+FrameWidth,TxOutTmp,0)
      TxOutTmp=' '
9999  return
      end
