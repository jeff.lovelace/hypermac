      subroutine FeHeaderInfo(Text)
      include 'fepc.cmn'
      character*(*) Text
      character*80 TextOld
      data TextOld/' '/
      call FeBoldFont
      call FeTextErase(0,XCenGrWin,(YMaxGrWin+YMaxBasWin)*.5,
     1                 TextOld,'C',LightGray)
      call FeOutSt(0,XCenGrWin,(YMaxGrWin+YMaxBasWin)*.5,Text,'C',
     1             Black)
      TextOld=Text
      call FeNormalFont
      return
      end
