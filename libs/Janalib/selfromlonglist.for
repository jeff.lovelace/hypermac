      integer function SelFromLongList(Text,Menu,NMenu)
      include 'fepc.cmn'
      character*(*) Menu(*),Text
      character*256 FileName
      integer SbwItemPointerQuest
      SelFromLongList=0
      ln=NextLogicNumber()
      FileName=TmpDir(:idel(TmpDir))//'vyber.tmp'
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      do i=1,NMenu
        write(ln,FormA) Menu(i)(:idel(Menu(i)))
      enddo
      call CloseIfOpened(ln)
      nrow=NMenu/2
      ncol=2
      il=nint(float(nrow)*.9)
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=il
      ild=il
      call FeQuestSbwMake(id,xpom,il,dpom,ild,ncol,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,FileName)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) SelFromLongList=SbwItemPointerQuest(nSbw)
      call FeQuestRemove(id)
      go to 9999
9000  SelFromLongList=-1
9999  call CloseIfOpened(ln)
      call DeleteFile(FileName)
      return
      end
