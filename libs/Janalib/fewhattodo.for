      integer function FeWhatToDo(xmi,ymi,ButtonLables,n,ist)
      include 'fepc.cmn'
      logical EqIgCase
      character*(*) ButtonLables(n)
      xd=0.
      do i=1,Ninfo
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
        xd=max(FeTxLength(TextInfo(i)),xd)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
      enddo
      gap=10.
      xb=0.
      do i=1,n
        xb=xb+FeTxLength(ButtonLables(i))+20.
      enddo
      xb=xb+float(n-1)*gap
      if(xb.gt.xd) then
        XButPoc=10.
        xd=xb+20.
      else
        XButPoc=(xd-xb)*.5+10.
        xd=xd+20.
      endif
      XTextPoc=10.
      yd=45.+float(Ninfo)*16.
      id=NextQuestid()
      call FeQuestAbsCreate(id,xmi,ymi,xd,yd,' ',0,LightGray,-1,-1)
      ypom=yd-10.
      do i=1,Ninfo
        if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
          xpom=xd*.5
        else
          xpom=XTextPoc
        endif
        call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(i),
     1                         TextInfoFlags(i)(1:1),
     2                         TextInfoFlags(i)(2:2))
        TextInfoFlags(i)='LN'
        ypom=ypom-16.
      enddo
      xpom=XButPoc
      ypom=15.
      do i=1,n
        dpom=FeTxLength(ButtonLables(i))+20.
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,
     1                            ButtonLables(i))
        if(i.eq.1) nButtFirst=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+gap
      enddo
      call FeQuestMouseToButton(ist)
2000  call FeEvent(0)
      if(EventType.eq.EventButton) then
        call FeQuestButtonOn(EventNumber)
        FeWhatToDo=EventNumber
        call FeQuestRemove(id)
        return
      endif
      go to 2000
      end
