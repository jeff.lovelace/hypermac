      subroutine FeSetLabelStep(xmin,xmax,div,irad)
      include 'fepc.cmn'
      div=abs((xmax-xmin)/10.)
      if(div.le.0.) go to 9999
      pom=1.
      irad=0
1000  if(div.ge.1.) go to 2000
      div=div*10.
      pom=pom*.1
      irad=irad-1
      if(irad.lt.-10) go to 9000
      go to 1000
2000  if(div.lt.10.) go to 3000
      div=div*.1
      pom=pom*10.
      irad=irad+1
      if(irad.gt.10) go to 9000
      go to 2000
3000  if(div.gt.1..and.div.le.2.) then
        div=2.
      else if(div.gt.2..and.div.le.5.) then
        div=5.
      else if(div.gt.5..and.div.le.10.) then
        div=10.
      endif
      div=div*pom
      go to 9999
9000  div=0.
9999  return
      end
