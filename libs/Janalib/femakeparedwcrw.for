      subroutine FeMakeParEdwCrw(IdQuest,NamePos,ParPos,QuestLine,
     1                           ParName,nEdw,nCrw)
      include 'fepc.cmn'
      character*(*) ParName
      integer QuestLine
      real NamePos
      call FeQuestEdwMake(IdQuest,NamePos,QuestLine,ParPos,QuestLine,
     1                    ParName,'R',70.,EdwYd,0)
      nEdw=EdwLastMade
      call FeQuestCrwMake(IdQuest,5.,QuestLine,ParPos+75.,QuestLine,
     1                    ' ','L',CrwXd,CrwYd,0,0)
      nCrw=CrwLastMade
      return
      end
