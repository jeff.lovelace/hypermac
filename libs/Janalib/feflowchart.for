      subroutine FeFlowChart
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) text,TextBut1,TextBut2
      character*80  TextOld,t80
      integer FeGetSystemTime
      logical ButtonActive(3),AlreadyOpened,WizardModeIn
      save ButtonActive,xm,ym,xmnp,xmxp,xd,yd,ymnp,ymxp,id,
     1     NFlowModSave,NFlowMaxSave,FlowRatio,istart,xold,Procenta,
     2     t80
      data AlreadyOpened,WizardModeIn/2*.false./
      go to 9999
      entry FeFlowChartOpen(xmi,ymi,NFlowMod,NFlowMax,Text,TextBut1,
     1                       TextBut2)
      if(SilentRun) go to 9999
      t80='0%'
      WizardModeIn=WizardMode
      WizardMode=.false.
      if(AlreadyOpened) go to 9999
      if(NFlowMax.le.0) then
        NFlowMaxSave=1000
      else
        NFlowMaxSave=NFlowMax
      endif
      if(NFlowMod.le.0) then
        NFlowModSave=10
      else
        NFlowModSave=NFlowMod
      endif
      if(BatchMode.or.Console) then
        m=0
        call FeLstWriteLine(Text,-1)
        ilb=-1
      else
        istart=-1
        xold=-1.
        call FeBoldFont
        xd=max(FeTxLength(Text)+20.,200.)
        call FeNormalFont
        TextOld='%Cancel'
        xdt=max(FeTxLengthUnder(TextOld),FeTxLengthUnder(TextBut1),
     1          FeTxLengthUnder(TextBut2))+10.
        nBut=1
        if(TextBut1.ne.' ') nBut=nBut+1
        if(TextBut2.ne.' ') nBut=nBut+1
        xd=max(xd,xdt*float(nBut)+10.*float(nBut-1)+20.)
        yd=80.
        id=NextQuestid()
        call FeQuestAbsCreate(id,xmi,ymi,xd,yd,Text,0,LightGray,-1,-1)
        xm=QuestXMin(id)
        ym=QuestYMin(id)
        xmnp=anint(xm*EnlargeFactor+20.)/EnlargeFactor
        xmxp=anint((xm+xd)*EnlargeFactor-20.)/EnlargeFactor
        ymxp=anint((ym+yd)*EnlargeFactor-25.)/EnlargeFactor
        ymnp=anint((ym+yd)*EnlargeFactor-41.)/EnlargeFactor

        xmnp=anint((xm+20.)*EnlargeFactor)/EnlargeFactor
        xmxp=anint((xm+xd-20.)*EnlargeFactor)/EnlargeFactor
        ymxp=anint((ym+yd-25.)*EnlargeFactor)/EnlargeFactor
        ymnp=anint((ym+yd-41.)*EnlargeFactor)/EnlargeFactor
        call FeDrawFrame(xmnp,ymnp,xmxp-xmnp,ymxp-ymnp,2.,White,
     1                   Gray,Black,.false.)
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xu(1)=anint((xmnp-1.)*EnlargeFactor)/EnlargeFactor
        yu(1)=anint((ymnp-1.)*EnlargeFactor)/EnlargeFactor
        xu(2)=anint((xmxp+1.)*EnlargeFactor)/EnlargeFactor
        yu(2)=yu(1)
        xu(3)=xu(2)
        yu(3)=anint((ymxp+1.)*EnlargeFactor)/EnlargeFactor
        xu(4)=xu(1)
        yu(4)=yu(3)
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,Gray)
        xpom=xd*.5-xdt*float(nBut)*.5-10.*float((nBut-1))*.5
        ypom=10.
        do i=1,3
          if(i.eq.1) then
            t80=TextBut1
          else if(i.eq.2) then
            t80=TextBut2
          else if(i.eq.3) then
            t80=TextOld
          endif
          ButtonActive(i)=t80.ne.' '
          call FeQuestAbsButtonMake(id,xpom,ypom,xdt,ButYd,t80)
          if(ButtonActive(i)) xpom=xpom+xdt+10.
          if(i.eq.1) nButPrvni=ButtonLastMade
        enddo
        nBut=nButPrvni
        j=0
        do i=1,3
          if(ButtonActive(i)) then
            call FeQuestButtonOpen(nBut,ButtonOff)
            if(j.eq.0) j=nBut+ButtonFr-1
          endif
          nBut=nBut+1
        enddo
        call FeMoveMouseTo((ButtonXMin(j)+ButtonXMax(j))*.5,
     1                     (ButtonYMin(j)+ButtonYMax(j))*.5)
        FlowRatio=(xmxp-xmnp)/float(NFlowMaxSave)
        TakeMouseMove=.true.
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      AlreadyOpened=.true.
      m=0
      go to 3000
      entry FeFlowChartReopen(Text)
      if(SilentRun) go to 9999
      Procenta=0.
      if(BatchMode.or.Console) then
        call FeLstWriteLine(Text,-1)
        ilb=-1
      else
        call FeQuestTitleRemove(id)
        call FeQuestTitleMake(id,Text)
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      m=0
      go to 3000
      entry FeFlowChartRefresh
      if(SilentRun) go to 9999
      if(BatchMode.or.Console) then
        Procenta=0.
        ilb=0
      else
        call FeFillRectangle(xmnp,xmxp,ymnp,ymxp,4,0,0,White)
        xold=-1.
      endif
      m=0
      go to 3000
      entry FeFlowChartEvent(n,ie)
      ie=0
      if(SilentRun) go to 9999
      if(BatchMode.or.Console) then
        if(BatchMode) then
          call FeEvent(1)
          if(EventType.eq.EventCtrl) then
            if(char(EventNumber).eq.'C') then
              ie=3
            endif
          endif
        endif
      else
        ipom=FeGetSystemTime()
        if(istart.eq.-1) then
          istart=ipom
          icas=99999
        else
          icas=ipom-istart
          if(icas.ge.50) istart=ipom
        endif
        if(icas.ge.50) then
          call FeFlush
          call FeEvent(1)
          if(XPos.ge.xm.and.XPos.le.xm+xd.and.
     1       YPos.ge.ym.and.YPos.le.ym+yd) then
            call FeMouseShape(0)
          else
            call FeMouseShape(3)
          endif
          if(EventType.eq.EventButton.and.EventNumber.ge.1.and.
     1       EventNumber.le.3) then
            ie=EventNumber
          else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape)
     1      then
            ie=3
          else
            ie=0
          endif
        endif
      endif
      if(n.lt.imax) then
        n=n+1
      else
        go to 9999
      endif
      if(mod(n,NFlowModSave).ne.0.and.n.ne.NFlowMaxSave) go to 9999
      m=min(n,NFlowMaxSave)
      ilb=0
3000  if(BatchMode.or.Console) then
        i=nint(float(m)/float(NFlowMaxSave)*100.)
        if(mod(i,10).eq.0) then
          write(t80,'(i4,''%'')') i
          call FeLstWriteLine(t80,ilb)
        endif
      else
        pom=xmnp+float(m)*FlowRatio
        if(pom-xold.ge.1..or.pom.ge.xmxp-.1) then
          xold=pom
          call FeFillRectangle(xmnp,pom,ymnp,ymxp,4,0,0,Gray)
          if(pom.lt.xmxp-.1)
     1      call FeFillRectangle(pom,xmxp,ymnp,ymxp,4,0,0,White)
          if(DeferredOutput) then
            call FeReleaseOutput
            call FeDeferOutput
          else
            call FeFlush
          endif
        endif
      endif
      go to 9999
      entry FeFlowChartRemove
      if(SilentRun.or.Console) go to 9000
      if((id.gt.0.or.BatchMode).and.AlreadyOpened) then
        TakeMouseMove=.false.
        call FeMouseShape(0)
        if(BatchMode) then
          call FeLstBackSpace
          call FeLstBackSpace
        else
          call FeQuestRemove(id)
        endif
        id=0
        WizardMode=WizardModeIn
      endif
9000  AlreadyOpened=.false.
9999  return
      end
