      subroutine FeQuestSbwItemSelDel(iwa)
      include 'fepc.cmn'
      i=iwa+SbwFr-1
      if(SbwType(i).ne.SbwMenuType.and.
     1   SbwType(i).ne.SbwSelectType) go to 9999
      ItemFromOld=SbwItemFrom(i)
      ItemSelOld=SbwItemPointer(i)
      call CloseIfOpened(SbwLn(i))
      if(SbwType(i).eq.SbwMenuType) then
        call DeleteLinesFromFile(SbwFile(i),ItemSelOld,ItemSelOld)
        call FeSbwMenuOpen(i)
      else if(SbwType(i).eq.SbwSelectType) then
        if(SbwItemSelN(i).gt.0) then
          do j=SbwItemNMax(i),1,-1
            if(SbwItemSel(j,i).ne.0) then
              call DeleteLinesFromFile(SbwFile(i),j,j)
              SbwItemSel(j,i)=0
              if(j.le.ItemSelOld) ItemSelOld=ItemSelOld-1
            endif
          enddo
        else
          call DeleteLinesFromFile(SbwFile(i),ItemSelOld,ItemSelOld)
        endif
        call FeSbwSelectOpen(i)
      endif
      call FeSbwShow(i,ItemFromOld)
      call FeQuestSbwItemOff(iwa,SbwItemFrom(i))
9999  call FeQuestSbwItemOn(iwa,ItemSelOld)
      return
      end
