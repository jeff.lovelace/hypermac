      subroutine FeWrMenuItemO(x,y,xd,Veta,StFlag,SubM,SubZ,SubA,SubN,
     1                         TextColor,BackGroundColor,klic)
      include 'fepc.cmn'
      common/subrol/ SubState,xsub,ysub,xdsub,ydsub
      integer SubState,SubN
      character*(*) Veta,SubM(SubN),SubZ
      character*1 StFlag
      integer TextColor,BackGroundColor
      logical SubA(SubN)
      if(SubN.gt.0.and.klic.eq.1) then
        xsub=x+xd-1.+2.
        ysub=y
        ydsub=float(SubN)*MenuLineWidth
        xdsub=0.
        do i=1,SubN
          xdsub=max(FeTxLengthUnder(SubM(i)),xdsub)
        enddo
        xdsub=xdsub+2.*EdwMarginSize
        xsub=anint(xsub)
        xdsub=anint(xdsub)
        ysub=anint(ysub)
        ydsub=anint(ydsub)
        call FeSaveImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                   'subrol.bmp')
      endif
      call FeFillRectangle(x,x+xd,y,y-MenuLineWidth,4,0,0,
     1                     BackGroundcolor)
      xpom=x+EdwMarginSize
      ypom=y-MenuLineWidth*.5
      call FeOutStUnder(0,xpom,ypom,Veta,'L',TextColor,TextColor,StFlag)
      if(SubN.gt.0) then
        yu(1)=ypom
        yu(2)=ypom-3.
        yu(3)=ypom+3.
        xu(1)=x+xd-3.
        xu(2)=xu(1)-4.
        xu(3)=xu(2)
        call FePolygon(xu,yu,3,4,0,0,TextColor)
        if(klic.eq.-1.and.SubState.eq.1) then
          call FeLoadImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                     'subrol.bmp',0)
          SubState=0
        else if(klic.eq.0) then
          SubState=0
        else if(klic.eq.1) then
          call FeFillRectangle(xsub-1.,xsub+xdsub+1.,ysub+1.,
     1                         ysub-ydsub-1.,4,0,0,LightGray)
          call FeDrawFrame(xsub,ysub-ydsub,xdsub,ydsub,2.,Gray,
     1                     White,Black,.false.)
          xp=xsub
          yp=ysub
          do i=1,SubN
            if(SubA(i)) then
              call FeWrMenuItem(xp,yp,xdsub,SubM(i),SubZ(i:i),Black,
     1                          LightGray)
            else
              call FeWrMenuItem(xp,yp,xdsub,SubM(i),SubZ(i:i),WhiteGray,
     1                          LightGray)
            endif
            yp=yp-MenuLineWidth
          enddo
          SubState=1
        endif
      else
        xsub =0.
        xdsub=0.
        ysub =0.
        ydsub=0.
        SubState=0
      endif
      return
      end
