      subroutine FeDirCheckTime(Dlouho,tf,ich)
      include 'fepc.cmn'
      logical Dlouho,FeYesNo
      integer FeGetSystemTime,tf
      if(Dlouho) then
        call FeEvent(1)
        if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
          if(FeYesNo(-1.,60.,'Do you really want to cancel the '//
     1               'action?',0)) ich=1
          call FeReleaseOutput
          call FeDeferOutput
        endif
      else
        if(FeGetSystemTime()-tf.gt.3000) then
          Dlouho=.true.
          NInfo=1
          TextInfo(1)='Reading file list, press the ESC key to '//
     1                'cancel...'
          call FeMsgShow(1,-1.,-1.,.true.)
        endif
      endif
      return
      end
