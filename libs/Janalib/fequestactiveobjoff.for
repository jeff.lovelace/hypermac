      subroutine FeQuestActiveObjOff
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        call FeButtonDeactivate(j)
      else if(i.eq.ActEdw) then
        call FeEdwHighlight(j)
        EdwSelFr=0
        EdwSelTo=0
        call FeEdwDeactivate(j)
        EdwActive=0
      else if(i.eq.ActCrw) then
        call FeCrwDeactivate(j)
      else if(i.eq.ActRolMenu) then
        call FeRolMenuDeactivate(j)
      else if(i.eq.ActSbw) then
        call FeSbwDeactivate(j)
        SbwActive=0
      else
        go to 9999
      endif
      ActiveObj=0
9999  return
      end
