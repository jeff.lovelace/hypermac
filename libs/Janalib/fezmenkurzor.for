      subroutine FeZmenKurzor(id)
      include 'fepc.cmn'
      character*256 Veta
      if(KurzorEdw.le.0.or.id.le.0) go to 9999
      xp=EdwTextXmin(KurzorEdw)+1.
      if(EdwTextMin(id).ge.1.and.Kurzor.ge.0) then
        Veta=EdwString(id)
        if(Kurzor.eq.EdwTextMax(id)) then
          xp=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor))
          xk=xp+anint(.5*PropFontWidthInPixels)/EnlargeFactor
        else if(Kurzor.eq.EdwTextMin(id)-1) then
          xk=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor+1))
        else
          xk=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor+1))
          xp=xp+FeTxLengthSpace(Veta(EdwTextMin(id):Kurzor))
        endif
      endif
      yp=(EdwYmin(KurzorEdw)+EdwYmax(KurzorEdw))*.5
      if(InsertMode) then
        KurzorX(1)=xp
        KurzorX(2)=xp
        KurzorY(1)=yp-.5*PropFontHeightInPixels/EnlargeFactor
        KurzorY(2)=yp+.5*PropFontHeightInPixels/EnlargeFactor
        KurzorStyl=4
      else
        KurzorX(1)=xp
        KurzorX(2)=xk
        KurzorY(1)=yp-.5*PropFontHeightInPixels/EnlargeFactor
        KurzorY(2)=KurzorY(1)
        KurzorStyl=4
      endif
      call FePlotMode('E')
      call FePolyline(2,KurzorX,KurzorY,KurzorColor)
      if(InsertMode) then
        KurzorX(1)=xp-1./EnlargeFactor
        KurzorX(2)=xp-1./EnlargeFactor
      else
        KurzorY(1)=KurzorY(1)+1./EnlargeFactor
        KurzorY(2)=KurzorY(1)
      endif
      call FePolyline(2,KurzorX,KurzorY,KurzorColor)
      call FePlotMode('N')
9999  return
      end
