      subroutine FeHistory(NewFln,Focus)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) NewFln
      character*256 DirHist(mxhst),FileHist(mxhst),NameHist(mxhst),t256
      integer FeChdir,SbwLnQuest,SbwItemPointerQuest
      logical Focus,StructureOpened,GetStructureLocked,
     1        GetStructureParent
      ln=NextLogicNumber()
      call OpenFile(ln,HistoryFile,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_history.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9999
      n=0
1000  n=n+1
      read(ln,FormA,end=2000,err=2000) DirHist(n)
      read(ln,FormA,end=2000,err=2000) FileHist(n)
      NameHist(n)=DirHist (n)(:idel(DirHist (n)))//
     1            FileHist(n)(:idel(FileHist(n)))
      t256=NameHist(n)
      if(StructureOpened(t256))
     1  NameHist(n)=NameHist(n)(:idel(NameHist(n)))//'(opened)'
      if(GetStructureLocked(t256))
     1  NameHist(n)=NameHist(n)(:idel(NameHist(n)))//'(locked)'
      if(GetStructureParent(t256))
     1  NameHist(n)=NameHist(n)(:idel(NameHist(n)))//'(parent)'
      write(lno,FormA) NameHist(n)(:idel(NameHist(n)))
      if(n.ge.mxhst) then
        go to 2001
      else
        go to 1000
      endif
2000  n=n-1
2001  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      nold=n
      xqd=600.
      id=NextQuestId()
      il=max(n-1,5)
      call FeQuestCreate(id,-1.,-1.,xqd,il,'History',0,LightGray,0,0)
      Focus=.false.
      xpom=15.
      dpom=xqd-30.-SbwPruhXd
      il=max(n-2,4)
      call FeQuestSbwMake(id,xpom,il,dpom,il,1,CutTextFromLeft,
     1                    SbwVertical)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      il=il+1
      nSbw=SbwLastMade
      dpom=50.
      xpom=xqd*.5-dpom*1.5-20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Delete')
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Focus')
      nButtFocus=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Clear')
      nButtClear=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2400  call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_history.tmp')
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtClear) then
          call CloseIfOpened(SbwLnQuest(nSbw))
          call DeleteFile(fln(:ifln)//'_history.tmp')
          n=0
          go to 2400
        else if(CheckNumber.eq.nButtDelete) then
          j=SbwItemPointerQuest(nSbw)
          call FeQuestSbwItemSelDel(nSbw)
          do i=j,n-1
            DirHist(i)=DirHist(i+1)
            FileHist(i)=FileHist(i+1)
          enddo
          n=n-1
          go to 2500
        else if(CheckNumber.eq.nButtFocus) then
          Focus=.true.
          ich=0
          go to 2800
        endif
        go to 2500
      else if(CheckType.eq.EventSbwDoubleClick) then
        ich=0
        go to 2800
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
2800  if(ich.eq.0) then
        if(n.gt.0) then
          i=SbwItemPointerQuest(nSbw)
          NewFln=FileHist(i)
          call DeletePomFiles
          m=FeChDir(DirHist(i))
          call FeGetCurrentDir
          if(n.ne.nold) then
            ln=NextLogicNumber()
            call OpenFile(ln,HistoryFile,'formatted','old')
            if(ErrFlag.ne.0) go to 9999
            do i=1,n
              write(ln,FormA) DirHist(i) (:idel(DirHist (i)))
              write(ln,FormA) FileHist(i)(:idel(FileHist(i)))
            enddo
          endif
        else
          call DeleteFile(HistoryFile)
        endif
      endif
5000  ErrFlag=ich
      call FeQuestRemove(id)
9999  call DeleteFile(fln(:ifln)//'_history.tmp')
      call CloseIfOpened(ln)
      return
      end
