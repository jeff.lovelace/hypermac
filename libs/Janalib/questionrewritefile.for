      subroutine QuestionRewriteFile(m)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*2 lf
      character*80 FileL,FileM,File42,Veta
      logical FeYesNo,FileDiff,Diff,ExistFile,ExistL,Jina
      write(lf,'(i2)') m
      if(m.ne.40.and.m.ne.41.and.m.ne.50) then
        call FeChybne(-1.,-1.,'wrong argument in RewriteFile : '//lf,
     1                ' ',SeriousError)
        go to 9999
      endif
      Jina=.false.
      if(m.eq.40) then
        FileL=PreviousM40
      else if(m.eq.50) then
        FileL=PreviousM50
        if(ExistElectronData.and.ExistM42.and.CalcDyn) then
          call iom42(1,0,fln(:ifln)//'.m42')
          File42=PreviousM42
        endif
      else
        FileL=fln(:ifln)//'.l'//lf
        Jina=.true.
      endif
      FileM=fln(:ifln)//'.m'//lf
      if(ExistFile(FileM)) then
        if(Jina) call MoveFile(FileM,FileL)
      else
        call DeleteFile(FileL)
        if(m.eq.41) call DeleteFile(PreviousM50)
      endif
      ExistL=ExistFile(FileL)
      if(m.eq.50.or.m.eq.41) call iom50(1,0,fln(:ifln)//'.m50')
      if(m.eq.40.or.m.eq.41) call iom40(1,0,fln(:ifln)//'.m40')
      if(ExistFile(FileM)) then
        if(ExistL) then
          Diff=FileDiff(FileM,FileL)
          if(m.eq.41.and..not.Diff)
     1      Diff=FileDiff(fln(:ifln)//'.m50',PreviousM50)
            if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1        Diff=FileDiff(fln(:ifln)//'.m42',PreviousM42)
        else
          Diff=.true.
        endif
      else
        call CopyFile(FileL,FileM)
        go to 9999
      endif
      if(Diff) then
        if(m.eq.50.and.ExistElectronData.and.ExistM42.and.CalcDyn) then
          Veta='Do you want to rewrite M42/M50 files?'
        else
          Veta='Do you want to rewrite M'//lf//' file?'
        endif
        if(FeYesNo(-1.,-1.,Veta,1))
     1    then
          go to 9999
        else
          call CopyFile(FileL,FileM)
          if(m.eq.50.and.ExistElectronData.and.ExistM42.and.CalcDyn)
     1      call CopyFile(PreviousM42,fln(:ifln)//'.m42')
        endif
      endif
9999  if(Jina) call DeleteFile(FileL)
      return
      end
