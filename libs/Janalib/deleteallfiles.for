      subroutine DeleteAllFiles(Filter)
      include 'fepc.cmn'
      character*(*) Filter
      character*256 t256,ListDir,ListFile,CurrentDirO,CurrentDirN,
     1              FilterPure
      integer FeChdir
      CurrentDirO=CurrentDir
      j=0
      idlf=idel(Filter)
      t256=' '
      do i=1,idlf
        if(j.eq.0.or.i.eq.idlf) then
          if(Filter(i:i).eq.' '.or.Filter(i:i).eq.'"') cycle
        endif
        j=j+1
        t256(j:j)=Filter(i:i)
      enddo
      call ExtractDirectory(t256,CurrentDirN)
      if(CurrentDirN(1:1).ne.'.'.and.CurrentDirN.ne.' ') then
        i=FeChdir(CurrentDirN)
        CurrentDir=CurrentDirN
       call ExtractFileName(t256,FilterPure)
      else
        FilterPure=t256
      endif
      ListDir='jdir'
      ListFile='jfile'
      call CreateTmpFile(ListDir,id1,0)
      call CreateTmpFile(ListFile,id2,0)
      call FeTmpFilesAdd(ListDir)
      call FeTmpFilesAdd(ListFile)
      call FeDir(ListDir,ListFile,FilterPure,0)
      call DeleteFile(ListDir)
      call FeTmpFilesClear(ListDir)
      ln=NextLogicNumber()
      open(ln,file=ListFile)
1000  read(ln,FormA,end=2000) t256
      call DeleteFile(t256)
      go to 1000
2000  close(ln,status='delete')
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      call FeTmpFilesClear(ListFile)
      return
      end
