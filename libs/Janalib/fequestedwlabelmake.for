      subroutine FeQuestEdwLabelMake(id,iw)
      include 'fepc.cmn'
      if(EdwLabel(iw).ne.' ') then
        call FeGetTextRectangle(EdwLabelX(iw),EdwLabelY(iw),
     1    EdwLabel(iw),EdwJustify(iw),1,x1,x2,y1,y2,refx,refy,conx,
     2    cony)
        EdwLabelXMin(iw)=x1
        EdwLabelXMax(iw)=x2
        EdwLabelYMin(iw)=y1
        EdwLabelYMax(iw)=y2
        call FeTextErase(0,EdwLabelX(iw),EdwLabelY(iw),EdwLabel(iw),
     1                   EdwJustify(iw),LightGray)
        call FeOutStUnder(0,EdwLabelX(iw),EdwLabelY(iw),EdwLabel(iw),
     1                    EdwJustify(iw),Black,Black,EdwZ(iw:iw))
      else
        EdwLabelXMin(iw)=0.
        EdwLabelXMax(iw)=0.
        EdwLabelYMin(iw)=0.
        EdwLabelYMax(iw)=0.
        EdwZ(iw:iw)=' '
      endif
      return
      end
