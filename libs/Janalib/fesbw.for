      subroutine FeSbw
      include 'fepc.cmn'
      integer ColorLU,ColorRD,ColorButton,ColorObtah,ColorSel
      character*256 t256
      logical Show
      entry FeSbwMake(id,idc,xm,ym,xd,yd,nstrip)
      SbwXMin(id)=xm
      SbwXMax(id)=xm+xd
      SbwYMin(id)=ym
      SbwYMax(id)=ym+yd
      if(idc.gt.0) then
        SbwXMin(id)=SbwXMin(id)+QuestXMin(idc)
        SbwXMax(id)=SbwXMax(id)+QuestXMin(idc)
        SbwYMin(id)=SbwYMin(id)+QuestYMin(idc)
        SbwYMax(id)=SbwYMax(id)+QuestYMin(idc)
      endif
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        SbwXMinPruh(1,id)=anint(SbwXMax(id)*EnlargeFactor+2.)/
     1                    EnlargeFactor
        SbwXMaxPruh(1,id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+
     1                          SbwPruhXd+1.)/EnlargeFactor
        SbwYMinPruh(1,id)=anint(SbwYMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwYMaxPruh(1,id)=(SbwYMax(id)*EnlargeFactor+2.)/EnlargeFactor
        SbwXMinPosun(1,id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(1,id)=anint(SbwXMaxPruh(1,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(1,id)=anint(SbwYMaxPruh(1,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwYMinPosun(1,id)=anint(SbwYMaxPosun(1,id)*EnlargeFactor-
     1                           SbwPruhXd+4.)/EnlargeFactor
        SbwXMinPosun(2,id)=SbwXMinPosun(1,id)
        SbwXMaxPosun(2,id)=SbwXMaxPosun(1,id)
        SbwYMinPosun(2,id)=anint(SbwYMinPruh(1,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(2,id)=anint(SbwYMinPosun(2,id)*EnlargeFactor+
     1                           SbwPruhYd-4.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        SbwXMinPruh(2,id)=anint(SbwXMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwXMaxPruh(2,id)=anint(SbwXMax(id)*EnlargeFactor+2.)/
     1                    EnlargeFactor
        SbwYMaxPruh(2,id)=anint(SbwYMin(id)*EnlargeFactor-2.)/
     1                    EnlargeFactor
        SbwYMinPruh(2,id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-
     1                          SbwPruhYd-2.)/EnlargeFactor
        SbwXMinPosun(3,id)=anint(SbwXMinPruh(2,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(3,id)=anint(SbwXMinPosun(3,id)*EnlargeFactor+
     1                           SbwPruhXd-4.)/EnlargeFactor
        SbwYMinPosun(3,id)=anint(SbwYMinPruh(2,id)*EnlargeFactor+2.)/
     1                     EnlargeFactor
        SbwYMaxPosun(3,id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwXMaxPosun(4,id)=anint(SbwXMaxPruh(2,id)*EnlargeFactor-2.)/
     1                     EnlargeFactor
        SbwXMinPosun(4,id)=anint(SbwXMaxPosun(4,id)*EnlargeFactor-
     1                           SbwPruhXd+4.)/EnlargeFactor
        SbwYMinPosun(4,id)=SbwYMinPosun(3,id)
        SbwYMaxPosun(4,id)=SbwYMaxPosun(3,id)
      endif
      SbwLn(id)=0
      pom=SbwXMin(id)
      pomd=(SbwXMax(id)-SbwXMin(id))/float(nstrip)
      do i=1,nstrip+1
        SbwXStripe(i,id)=pom
        pom=pom+pomd
      enddo
      go to 9999
      entry FeSbwMenuOpen(id)
      SbwYStep(id)=MenuLineWidth
      SbwLines(id)=nint((SbwYMax(id)-SbwYMin(id))/MenuLineWidth)
      SbwType(id)=SbwMenuType
      go to 1500
      entry FeSbwSelectOpen(id)
      SbwYStep(id)=MenuLineWidth
      SbwLines(id)=nint((SbwYMax(id)-SbwYMin(id))/MenuLineWidth)
      SbwType(id)=SbwSelectType
      go to 1500
      entry FeSbwTextOpen(id,ly)
      SbwType(id)=SbwTextType
      SbwLines(id)=ly
      SbwYStep(id)=(SbwYMax(id)-SbwYMin(id)-3.)/float(ly)
      go to 1500
      entry FeSbwPureOpen(id,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      SbwType(id)=SbwPureType
1500  if(SbwType(id).ne.SbwPureType) then
        if(SbwType(id).eq.SbwMenuType.or.SbwType(id).eq.SbwSelectType)
     1    then
          pom=EdwMarginSize
        else if(SbwType(id).eq.SbwTextType) then
          pom=1.
        endif
        SbwColumnsXMin(id)=(SbwXMin(id)*EnlargeFactor+pom)/EnlargeFactor
        SbwColumnsXMax(id)=(SbwXMax(id)*EnlargeFactor-pom)/EnlargeFactor
        SbwColumnsXLen(id)=SbwColumnsXMax(id)-SbwColumnsXMin(id)
        SbwColumns(id)=SbwColumnsXLen(id)/FeTxLength('H')
        SbwLn(id)=NextLogicNumber()
        call FeDrawFrame(SbwXMin(id),SbwYMin(id),
     1                   SbwXMax(id)-SbwXMin(id),
     2                   SbwYMax(id)-SbwYMin(id),
     3                   2./EnlargeFactor,White,Gray,Black,.false.)
        call FeFillRectangle(SbwXMin(id),SbwXMax(id),SbwYMin(id),
     1                       SbwYMax(id),4,0,0,White)
        xu(1)=anint(SbwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
        yu(1)=anint(SbwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
        xu(2)=anint(SbwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
        yu(2)=anint(SbwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
        xu(3)=anint(SbwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
        yu(3)=anint(SbwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
        xu(4)=anint(SbwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
        yu(4)=anint(SbwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,Black)
        call OpenFile(Sbwln(id),SbwFile(id),'formatted','unknown')
        n=0
        m=0
        call SetStringArrayTo(SbwItem(1,id),60,' ')
        call SetStringArrayTo(SbwItemShort(1,id),60,' ')
2000    read(Sbwln(id),FormA,end=2010) t256
        n=n+1
        m=max(m,idel(t256))
        go to 2000
2010    SbwItemNMax(id)=n
        SbwItemFrom(id)=1
        SbwItemTo(id)=SbwLines(id)
        SbwColumnsMax(id)=m
        SbwColumnFrom(id)=1
        SbwColumnTo(id)=SbwColumns(id)
        SbwStripesMax(id)=(n-1)/SbwLines(id)+1
        SbwStripeFrom(id)=1
        SbwStripeTo(id)=max(nstrip,1)
        SbwItemPointer(id)=0
        if((SbwType(id).eq.SbwMenuType.or.
     1      SbwType(id).eq.SbwSelectType).and.n.gt.0)
     2    SbwItemPointer(id)=1
      else
        SbwItemNMax(id)=NYAll
        SbwItemFrom(id)=NYFr
        SbwItemTo(id)=NYTo
        SbwLines(id)=NYTo-NYFr+1
        n=NYAll
        SbwColumnsMax(id)=NXAll
        SbwColumnFrom(id)=NXFr
        SbwColumnTo(id)=NXTo
        SbwColumns(id)=NXTo-NXFr+1
        m=NXAll
      endif
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        call FeFillRectangle(SbwXMinPruh(1,id),
     1                       SbwXMaxPruh(1,id),
     2                       SbwYMinPruh(1,id),
     3                       SbwYMaxPruh(1,id),4,0,0,WhiteGray)
        xu(1)=anint(SbwXMaxPruh(1,id)*EnlargeFactor+1.)/EnlargeFactor
        yu(1)=SbwYMinPruh(1,id)
        xu(2)=anint(SbwXMaxPruh(1,id)*EnlargeFactor+1.)/EnlargeFactor
        yu(2)=SbwYMaxPruh(1,id)
        call FePolyLine(2,xu,yu,Black)
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        call FeFillRectangle(SbwXMinPruh(2,id),SbwXMaxPruh(2,id),
     1                       SbwYMinPruh(2,id),SbwYMaxPruh(2,id),
     2                       4,0,0,WhiteGray)
        xu(1)=SbwXMinPruh(2,id)
        yu(1)=anint(SbwYMinPruh(2,id)*EnlargeFactor-1.)/EnlargeFactor
        xu(2)=SbwXMaxPruh(2,id)
        yu(2)=anint(SbwYMinPruh(2,id)*EnlargeFactor-1.)/EnlargeFactor
        call FePolyLine(2,xu,yu,Black)
      endif
      PressShift=0.
      ip=0
      ik=0
      if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        if(n.gt.SbwLines(id)) then
          fn=float(n)
          SbwDelPlovak(id)=max(nint(float(SbwLines(id))/fn*
     1                         ((SbwYMinPosun(1,id)-SbwYMaxPosun(2,id))*
     2                         EnlargeFactor-2.)/EnlargeFactor),10)
        else
          SbwDelPlovak(id)=0
        endif
        ip=1
        ik=2
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        if(SbwStripes(id).le.1) then
          if(m.gt.SbwColumns(id)) then
            fm=float(m)
            SbwDelStrelka(id)=max(nint(float(SbwColumns(id))/fm*
     1                            ((SbwXMinPosun(4,id)-
     2                              SbwXMaxPosun(3,id))*EnlargeFactor-
     3                              2.)/EnlargeFactor),10)
          else
            SbwDelStrelka(id)=0.
          endif
        else
          if(SbwStripesMax(id).gt.SbwStripes(id)) then
            SbwDelStrelka(id)=max(nint(float(SbwStripes(id))/
     1                                 float(SbwStripesMax(id))*
     2                            ((SbwXMinPosun(4,id)-
     3                              SbwXMaxPosun(3,id))*EnlargeFactor-
     4                              2.)/EnlargeFactor),10)
          else
            SbwDelStrelka(id)=0.
          endif
        endif
      endif
      if(ip.eq.0) then
        ip=3
        ik=4
      endif
      Show=.true.
      SbwState(id)=SbwOpened
      go to 5000
      entry FeSbwClose(id)
      if(SbwState(id).eq.SbwClosed.or.
     1   SbwState(id).eq.SbwRemoved) go to 9999
      ir=0
      go to 2200
      entry FeSbwRemove(id)
      if(SbwState(id).eq.SbwRemoved) go to 9999
      ir=1
2200  if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        xpom=SbwXMaxPruh(1,id)
      else
        xpom=anint(SbwXMax(id)*EnlargeFactor+2.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        ypom=SbwYMinPruh(2,id)
      else
        ypom=anint(SbwYMin(id)*EnlargeFactor-2.)/EnlargeFactor
      endif
      call FeFillRectangle(anint(SbwXMin(id)*EnlargeFactor-2.)/
     1                     EnlargeFactor,
     2                     xpom,ypom,
     3                     anint(SbwYMax(id)*EnlargeFactor+2.)/
     4                     EnlargeFactor,4,0,0,LightGray)
      if(ir.eq.1) then
        SbwState(id)=SbwRemoved
      else
        SbwState(id)=SbwClosed
      endif
      go to 9999
      entry FeSbwPosunOff(id,it)
      ip=it
      ik=it
      Show=.false.
5000  PressShift=0.
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      go to 5100
      entry FeSbwPosunOn(id,it)
      PressShift=1.
      ip=it
      ik=it
      PressShift=1.
      ColorRD=Gray
      ColorLU=Gray
      ColorButton=LightGray
      ColorObtah=Gray
      Show=.false.
5100  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      do i=ip,ik
        xLD=anint(SbwXMinPosun(i,id)*EnlargeFactor-dxx)/EnlargeFactor
        xRU=anint(SbwXMaxPosun(i,id)*EnlargeFactor+dxx)/EnlargeFactor
        wp=xRU-xLD
        yLD=anint(SbwYMinPosun(i,id)*EnlargeFactor-dxx)/EnlargeFactor
        yRU=anint(SbwYMaxPosun(i,id)*EnlargeFactor+dxx)/EnlargeFactor
        hp=yRU-yLD
        call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
        call FeDrawFrame(xLD,yLD,wp,hp,dx/EnlargeFactor,ColorRD,ColorLU,
     1                   ColorObtah,.false.)
        pomx=anint((SbwXMinPosun(i,id)+SbwXMaxPosun(i,id))*.5*
     1             EnlargeFactor)
        pomy=anint((SbwYMinPosun(i,id)+SbwYMaxPosun(i,id))*.5*
     1             EnlargeFactor)
        if(i.le.2) then
          fnx=4.
          fny=3.
          xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
          xu(2)=anint(pomx+PressShift)/EnlargeFactor
          xu(3)=anint(pomx-fnx+PressShift)/EnlargeFactor
          if(i.eq.1) then
            yu(1)=anint(pomy-fny-PressShift)/EnlargeFactor
            yu(2)=anint(pomy+fny-PressShift)/EnlargeFactor
          else
            yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
            yu(2)=anint(pomy-fny-PressShift)/EnlargeFactor
          endif
          yu(3)=yu(1)
        else
          fny=4.
          fnx=3.
          yu(1)=anint(pomy+fny-PressShift)/EnlargeFactor
          yu(2)=anint(pomy-PressShift)/EnlargeFactor
          yu(3)=anint(pomy-fny-PressShift)/EnlargeFactor
          if(i.eq.3) then
            xu(1)=anint(pomx+fnx+PressShift)/EnlargeFactor
            xu(2)=anint(pomx-fnx+PressShift)/EnlargeFactor
          else
            xu(1)=anint(pomx-fnx+PressShift)/EnlargeFactor
            xu(2)=anint(pomx+fnx+PressShift)/EnlargeFactor
          endif
          xu(3)=xu(1)
        endif
        call FePolygon(xu,yu,3,4,0,0,Black)
      enddo
      if(Show) call FeSbwShow(id,1)
      go to 9999
      entry FeSbwSkokOn(id,it)
      ColorButton=Black
      go to 7000
      entry FeSbwSkokOff(id,it)
      ColorButton=WhiteGray
7000  if(it.le.2) then
        xLD=SbwXminPruh(1,id)
        xRU=SbwXmaxPruh(1,id)
        if(it.eq.1) then
          yLD=anint(SbwYmaxPlovak(id)*EnlargeFactor+3.)/EnlargeFactor
          yRU=anint(SbwYminPosun(1,id)*EnlargeFactor-3.)/EnlargeFactor
        else if(it.eq.2) then
          yLD=anint(SbwYmaxPosun(2,id)*EnlargeFactor+3.)/EnlargeFactor
          yRU=anint(SbwYminPlovak(id)*EnlargeFactor-3.)/EnlargeFactor
        endif
      else
        yLD=SbwYminPruh(2,id)
        yRU=SbwYmaxPruh(2,id)
        if(it.eq.3) then
          xLD=anint(SbwXmaxPosun(3,id)*EnlargeFactor+3.)/EnlargeFactor
          xRU=anint(SbwXminStrelka(id)*EnlargeFactor-3.)/EnlargeFactor
        else
          xLD=anint(SbwXmaxStrelka(id)*EnlargeFactor+3.)/EnlargeFactor
          xRU=anint(SbwXminPosun(4,id)*EnlargeFactor-3.)/EnlargeFactor
        endif
      endif
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
      go to 9999
      entry FeSbwActivate(id)
      ColorSel=Black
      go to 8000
      entry FeSbwDeactivate(id)
      ColorSel=LightGray
8000  if(SbwBars(id).eq.SbwVertical.or.SbwBars(id).eq.SbwBoth) then
        xpmax=anint(SbwXMaxPruh(1,id)*EnlargeFactor+2.)/EnlargeFactor
      else
        xpmax=anint(SbwXMax(id)*EnlargeFactor+3.)/EnlargeFactor
      endif
      if(SbwBars(id).eq.SbwHorizontal.or.SbwBars(id).eq.SbwBoth) then
        ypmin=anint(SbwYMinPruh(2,id)*EnlargeFactor-2.)/EnlargeFactor
      else
        ypmin=anint(SbwYMin(id)*EnlargeFactor-3.)/EnlargeFactor
      endif
      xpmin=anint(SbwXMin(id)*EnlargeFactor-3.)/EnlargeFactor
      ypmax=anint(SbwYMax(id)*EnlargeFactor+3.)/EnlargeFactor
      xu(1)=xpmin
      yu(1)=ypmin
      xu(2)=xu(1)
      yu(2)=ypmax
      xu(3)=xpmax
      xu(4)=xu(3)
9999  return
      end
