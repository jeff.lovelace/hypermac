      subroutine FeQuestEventEdw(izpet)
      include 'fepc.cmn'
      integer EdwClicked
      character*256 Veta
      data EdwClicked,KurzorClicked,NClicked/0,0,0/
      izpet=0
      if(EventType.eq.EventEdw.or.EventType.eq.EventEdwUp.or.
     1   EventType.eq.EventEdwDown) then
        if(EdwActive.eq.0.or.EventNumberAbs.ne.EdwActive) then
          iwnew=EventNumberAbs
          if(iwnew.eq.0) go to 9999
          if(EdwState(iwnew).ne.EdwOpened) go to 9999
          call FeQuestActiveObjOff
          EdwActive=iwnew
          ActiveObj=ActEdw*10000+EdwActive
          call FeQuestActiveObjOn
        endif
        if(EventType.eq.EventEdw.and.KurzorClick.gt.0) then
          if(KurzorClick.eq.KurzorClicked.and.NClicked.ne.0.and.
     1       EdwActive.eq.EdwClicked.and.FeTimeDiff().lt..5) then
            Veta=EdwString(EdwActive)
            if(NClicked.eq.1) then
              if(Veta(KurzorClicked:KurzorClicked).ne.' ') then
                k=KurzorClicked
1100            if(k.gt.1) then
                  if(Veta(k-1:k-1).ne.' ') then
                    k=k-1
                    go to 1100
                  endif
                endif
                EdwSelFr=k-1
                k=KurzorClicked
1200            if(k.lt.idel(Veta)) then
                  if(Veta(k+1:k+1).ne.' ') then
                    k=k+1
                    go to 1200
                  endif
                endif
                EdwSelTo=k
                NClicked=NClicked+1
              else
                EdwSelFr=0
                EdwSelTo=idel(Veta)
                NClicked=0
              endif
              KurzorClick=-1
            else if(NClicked.eq.2) then
              EdwSelFr=0
              EdwSelTo=idel(Veta)
              NClicked=0
              KurzorClick=-1
            endif
          else
            i=FeTimeDiff()
            NClicked=1
            EdwClicked=EdwActive
            KurzorClicked=KurzorClick
          endif
        endif
      endif
      if(EdwActive.ne.0) then
        if(EventType.eq.EventKey) then
          if(EventNumber.ne.JeEscape.and.EventNumber.ne.JeReturn)
     1      then
            call FeEdwMakeAction(EdwActive)
            if(CheckKeyboard.and.EventNumber.ne.0) then
              go to 9000
            else
              go to 99999
            endif
          endif
        else if(EventType.eq.EventCtrl) then
          if(char(EventNumber).eq.'V') then
            call FeEdwCtrlVAction(EdwActive)
            EventNumber=0
          else if(char(EventNumber).eq.'X') then
            call FeEdwCtrlXAction(EdwActive)
            EventNumber=0
          else if(char(EventNumber).eq.'C') then
            call FeEdwCtrlCAction(EdwActive)
            EventNumber=0
          endif
        else if(EventType.eq.EventEdwUp.or.EventType.eq.EventEdwDown)
     1    then
          if(EventType.eq.EventEdwUp) then
            iz= 1
            i=1
          else
            iz=-1
            i=2
          endif
          call FeEudOn(EdwActive,i)
          call FeReleaseOutput
          call FeDeferOutput
          call FeWait(.1)
          call FeCheckEud(EdwActive,iz)
          EventType=EventEdw
          if(EdwCheck(EdwActive).ne.0) go to 9000
        else if(EventType.eq.EventEdwSel) then
          if((Kurzor.eq.EdwSelFr.and.EdwSelTo.eq.EventNumber).or.
     1       (Kurzor.eq.EdwSelTo.and.EdwSelFr.eq.EventNumber)) then
            go to 2000
          else
            EdwSelFr=min(Kurzor,EventNumber)
            EdwSelTo=max(Kurzor,EventNumber)
          endif
        endif
      endif
      call FeEdwMakeAction(EdwActive)
      if(CheckKeyboard.and.
     1   (EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     2    EventType.eq.EventCtrl).and.EventNumber.ne.0) go to 9000
      go to 99999
2000  izpet=2000
      go to 99999
2100  izpet=2100
      go to 99999
9000  izpet=9000
      go to 99999
9999  izpet=9999
99999 return
      end
