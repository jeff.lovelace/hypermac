      logical function FeTestIn()
      include 'fepc.cmn'
      integer CrwLastClick,RolMenuLastClick,EdwLastClick,
     1        FeGetEdwStringPosition
      data CrwLastClick,RolMenuLastClick,EdwLastClick/3*0/
      EventType=0
      EventNumber=0
      EventNumberAbs=0
      if(.not.DelejTestIn) go to 9000
      do i=1,ButtonMax
        if(((i.lt.ButtonFr.or.i.gt.ButtonTo).and..not.ButtonAlways(i))
     1     .or.ButtonState(i).ne.ButtonOff) cycle
        if(xpos.ge.ButtonXmin(i).and.xpos.le.ButtonXmax(i).and.
     1     ypos.ge.ButtonYmin(i).and.ypos.le.ButtonYmax(i)) then
          EventType=EventButton
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do ii=1,KartNDol
        i=KartDolButt(ii)
        if(i.eq.0) cycle
        if(ButtonState(i).ne.ButtonOff) cycle
        if(xpos.ge.ButtonXmin(i).and.xpos.le.ButtonXmax(i).and.
     1     ypos.ge.ButtonYmin(i).and.ypos.le.ButtonYmax(i)) then
          if(i.eq.KartESC.or.i.eq.KartOK.or.i.eq.KartCancel) then
            EventType=EventButton
          else
            EventType=EventKartButt
          endif
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).ne.EdwOpened) cycle
        if((xpos.ge.EdwXmin(i).and.xpos.le.EdwXmax(i).and.
     1      ypos.ge.EdwYmin(i).and.ypos.le.EdwYmax(i)).or.
     2     (xpos.ge.EdwLabelXmin(i).and.xpos.le.EdwLabelXmax(i).and.
     3      ypos.ge.EdwLabelYmin(i).and.ypos.le.EdwLabelYmax(i))) then
          EventType=EventEdw
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          if(xpos.ge.EdwXmin(i).and.xpos.le.EdwXmax(i).and.
     1       ypos.ge.EdwYmin(i).and.ypos.le.EdwYmax(i)) then
            KurzorClick=FeGetEdwStringPosition(i,XPos)
          else
            KurzorClick=EdwKurzor(i)
          endif
          go to 9000
        else
          if(EdwUpDown(i).ne.0) then
            if(xpos.ge.EdwUpDownXmin(i).and.xpos.le.EdwUpDownXmax(i))
     1        then
              if(ypos.ge.EdwUpDownYmin(1,i).and.
     1           ypos.le.EdwUpDownYmax(1,i)) then
                EventType=EventEdwUp
                EventNumber=i-EdwFr+1
                EventNumberAbs=i
                go to 9000
              else if(ypos.ge.EdwUpDownYmin(2,i).and.
     1                ypos.le.EdwUpDownYmax(2,i)) then
                EventType=EventEdwDown
                EventNumber=i-EdwFr+1
                EventNumberAbs=i
                go to 9000
              endif
            endif
          endif
        endif
      enddo
      do i=EdwFr,EdwTo
        if(EdwState(i).eq.EdwRemoved.or.EdwState(i).eq.EdwDisabled.or.
     1     EdwState(i).eq.EdwClosed) cycle
        if(xpos.ge.EdwXMin(i).and.xpos.le.EdwXMax(i).and.
     1     ypos.ge.EdwYMin(i).and.ypos.le.EdwYMax(i)) then
          if(EdwState(i).eq.EdwLocked) then
            if(EdwLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                EdwLastClick=0
                call FeEdwOpen(i)
                go to 1550
              endif
            else
              EdwLastClick=FeTimeDiff()
              EdwLastClick=i
            endif
          endif
          go to 9000
1550      EventType=EventEdwUnlock
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1     CrwState(i).eq.CrwLocked.or.CrwState(i).eq.CrwDisabled) cycle
        if((xpos.ge.CrwXmin(i).and.xpos.le.CrwXmax(i).and.
     1      ypos.ge.CrwYmin(i).and.ypos.le.CrwYmax(i)).or.
     2     (xpos.ge.CrwLabelXMin(i).and.xpos.le.CrwLabelXMax(i).and.
     1      ypos.ge.CrwLabelYMin(i).and.ypos.le.CrwLabelYMax(i))) then
          EventType=EventCrw
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=CrwFr,CrwTo
        if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1     CrwState(i).eq.CrwDisabled) cycle
        if(xpos.ge.CrwXmin(i).and.xpos.le.CrwXmax(i).and.
     1     ypos.ge.CrwYmin(i).and.ypos.le.CrwYmax(i)) then
          if(CrwState(i).eq.CrwLocked) then
            if(CrwLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                j=CrwExGr(i)
                CrwLastClick=0
                if(j.le.0) then
                  if(CrwLogic(i)) then
                    call FeCrwOn(i)
                  else
                    call FeCrwOff(i)
                  endif
                  go to 2040
                endif
                do k=CrwFr,CrwTo
                  if(CrwState(k).eq.CrwRemoved.or.
     1               CrwState(k).eq.CrwDisabled.or.
     1               CrwState(k).eq.CrwClosed .or.CrwExGr(k).ne.j)
     2              cycle
                  call FeCrwOff(k)
                enddo
                call FeCrwOn(i)
                go to 2040
              endif
            else
              CrwLastClick=FeTimeDiff()
              CrwLastClick=i
            endif
          endif
          go to 9000
2040      EventType=EventCrwUnlock
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=1,RolMenuMax
        if(((i.lt.RolMenuFr.or.i.gt.RolMenuTo).and.
     1      .not.RolMenuAlways(i)).or.
     2     RolMenuState(i).eq.RolMenuRemoved.or.
     3     RolMenuState(i).eq.RolMenuLocked.or.
     4     RolMenuState(i).eq.RolMenuDisabled.or.
     5     RolMenuState(i).eq.RolMenuClosed) cycle
        if((xpos.ge.RolMenuButtXMin(i).and.
     1      xpos.le.RolMenuButtXMax(i).and.
     2      ypos.ge.RolMenuButtYMin(i).and.
     3      ypos.le.RolMenuButtYMax(i)).or.
     4     (xpos.ge.RolMenuLabelXMin(i).and.
     5      xpos.le.RolMenuLabelXMax(i).and.
     6      ypos.ge.RolMenuLabelYMin(i).and.
     7      ypos.le.RolMenuLabelYMax(i))) then
          EventType=EventRolMenu
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=RolMenuFr,RolMenuTo
        if(((i.lt.RolMenuFr.or.i.gt.RolMenuTo).and.
     1      .not.RolMenuAlways(i)).or.
     2     RolMenuState(i).eq.RolMenuRemoved.or.
     3     RolMenuState(i).eq.RolMenuClosed) cycle
        if(xpos.ge.RolMenuTextXMin(i).and.
     1     xpos.le.RolMenuTextXMax(i).and.
     2     ypos.ge.RolMenuTextYMin(i).and.
     3     ypos.le.RolMenuTextYMax(i)) then
          if(RolMenuState(i).eq.RolMenuLocked) then
            if(RolMenuLastClick.eq.i) then
              if(FeTimeDiff().lt..3) then
                RolMenuLastClick=0
                call FeRolMenuOpen(i)
                go to 3250
              endif
            else
              RolMenuLastClick=FeTimeDiff()
              RolMenuLastClick=i
            endif
          endif
          go to 9000
3250      EventType=EventRolMenuUnlock
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=IconFr,IconTo
        if(IconState(i).eq.IconRemoved.or.IconState(i).eq.IconDisabled)
     1     cycle
        if(xpos.ge.IconXmin(i).and.xpos.le.IconXmax(i).and.
     1     ypos.ge.IconYmin(i).and.ypos.le.IconYmax(i)) then
          EventType=EventIcon
          EventNumber=i
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=1,NKart
        if(xpos.ge.KartSwXmin(i).and.xpos.le.KartSwXMax(i).and.
     1     ypos.ge.KartSwYMin(i).and.ypos.le.KartSwYMax(i)) then
          EventType=EventKartSw
          EventNumber=i
          EventNumberAbs=i
          go to 9000
        endif
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened.or.SbwBars(i).eq.SbwHorizontal.or.
     1     SbwDelPlovak(i).le.0.) cycle
        do j=1,2
          if(xpos.ge.SbwXminPosun(j,i).and.
     1       xpos.le.SbwXmaxPosun(j,i).and.
     2       ypos.ge.SbwYminPosun(j,i).and.
     3       ypos.le.SbwYmaxPosun(j,i)) then
            if(j.eq.1) then
              EventType=EventSbwLineUp
            else
              EventType=EventSbwLineDown
            endif
            go to 6080
          endif
        enddo
        if(xpos.ge.SbwXminPruh(1,i).and.
     1     xpos.le.SbwXmaxPruh(1,i)) then
          if(ypos.ge.SbwYmaxPlovak(i)+3..and.
     1       ypos.le.SbwYminPosun(1,i)-3.) then
            EventType=EventSbwPageUp
            go to 6080
          endif
          if(ypos.ge.SbwYmaxPosun(2,i)+3..and.
     1       ypos.le.SbwYminPlovak(i)-3.) then
            EventType=EventSbwPageDown
            go to 6080
          endif
        endif
        if(xpos.ge.SbwXminPlovak(i).and.
     1     xpos.le.SbwXmaxPlovak(i).and.
     2     ypos.ge.SbwYminPlovak(i).and.
     3     ypos.le.SbwYmaxPlovak(i)) then
          EventType=EventSbwPlovak
          go to 6080
        endif
        cycle
6080    EventNumber=i-SbwFr+1
        EventNumberAbs=i
        go to 9000
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened.or.SbwBars(i).eq.SbwVertical.or.
     1     SbwDelStrelka(i).le.0.) cycle
        do j=3,4
          if(xpos.ge.SbwXminPosun(j,i).and.
     1       xpos.le.SbwXmaxPosun(j,i).and.
     2       ypos.ge.SbwYminPosun(j,i).and.
     3       ypos.le.SbwYmaxPosun(j,i)) then
            if(j.eq.3) then
              EventType=EventSbwColumnLeft
            else
              EventType=EventSbwColumnRight
            endif
            go to 6180
          endif
        enddo
        if(ypos.ge.SbwYminPruh(2,i).and.
     1     ypos.le.SbwYmaxPruh(2,i)) then
          if(xpos.ge.SbwXmaxPosun(3,i)+3..and.
     1       xpos.le.SbwXminStrelka(i)-3.) then
            EventType=EventSbwColumnStrongLeft
            go to 6180
          endif
          if(xpos.ge.SbwXmaxStrelka(i)+3..and.
     1       xpos.le.SbwXminPosun(4,i)-3.) then
            EventType=EventSbwColumnStrongRight
            go to 6180
          endif
        endif
        if(xpos.ge.SbwXminStrelka(i).and.
     1     xpos.le.SbwXmaxStrelka(i).and.
     2     ypos.ge.SbwYminStrelka(i).and.
     3     ypos.le.SbwYmaxStrelka(i)) then
          EventType=EventSbwStrelka
          go to 6180
        endif
        cycle
6180    EventNumber=i-SbwFr+1
        EventNumberAbs=i
        go to 9000
      enddo
      do i=SbwFr,SbwTo
        if(SbwState(i).ne.SbwOpened) cycle
        if(xpos.ge.SbwXmin(i).and.xpos.le.SbwXmax(i).and.
     1     ypos.ge.SbwYmin(i).and.ypos.le.SbwYmax(i).and.
     2     (SbwType(i).eq.SbwMenuType.or.SbwType(i).eq.SbwSelectType))
     3    then
          iy=ifix((SbwYmax(i)-ypos)/SbwYStep(i))+1
          do ix=1,SbwStripes(i)
            if(xpos.ge.SbwXStripe(ix,i).and.xpos.le.SbwXStripe(ix+1,i))
     1        go to 6260
          enddo
          ix=1
6260      j=(ix-1)*SbwLines(i)+iy
          EventType=EventSbwMenu
          j=i+100*j
          EventNumber=j-SbwFr+1
          EventNumberAbs=j
        endif
      enddo
9000  FeTestIn=EventType.ne.0
      return
      end
