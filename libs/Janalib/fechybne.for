      subroutine FeChybne(xmi,ymi,text1,text2,ErrType)
      include 'fepc.cmn'
      character*128 Ven1,Ven2,Veta
      character*(*) text1,text2
      integer ErrType,QuestLineWidthIn,UseTabsIn
      logical CrwLogicQuest,WizardModeIn,PropFontIn
      if(.not.ICDDBatch.and..not.Console) then
        PropFontIn=PropFont
        if(.not.PropFont) call FeSetPropFont
        WizardModeIn=WizardMode
        WizardMode=.false.
        QuestLineWidthIn=QuestLineWidth
        QuestLineWidth=16.
        if(DelejResize) go to 9999
      endif
      if(Text2.eq.' ') then
        Ven2=' '
      else
        Ven2=' '//Tabulator//' '//text2(:idel(text2))
      endif
      if(ErrType.eq.FatalError) then
        if(Language.eq.1) then
          Ven1='Z�sadn� chyba -'
        else
          Ven1='Fatal -'
        endif
      else if(ErrType.eq.SeriousError) then
        if(Language.eq.1) then
          Ven1='Chyba -'
        else
          Ven1='Error -'
        endif
      else
        if(Language.eq.1) then
          Ven1='Varov�n� - '
        else
          Ven1='Warning -'
        endif
      endif
      if(.not.ICDDBatch.and..not.Console) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        call FeTabsAdd(FeTxLength(Ven1),UseTabs,IdRightTab,' ')
      endif
      Ven1=Ven1(:idel(Ven1))//Tabulator//' '//text1(:idel(text1))
      idv1=idel(Ven1)
      idv2=idel(Ven2)
      if(.not.ICDDBatch.and..not.Console) then
        if((ErrType.eq.SeriousError.and.IgnoreE).or.
     1     ((ErrType.eq.Warning.or.
     2       ErrType.eq.WarningWithESC).and.IgnoreW)) then
          if(AniNaListing) then
            go to 9000
          else
            go to 2000
          endif
        endif
        xqd=max(FeTxLength(Ven1),FeTxLength(Ven2))
        il=1
        if(idv2.gt.0) il=il+1
        if(ErrType.eq.WarningWithESC) il=il+4
        xqd=xqd+10.
        if(ErrType.eq.WarningWithESC.or.ErrType.eq.Warning) then
          WaitTime=max(nint(30.*float(idv1+idv2)/160.),5)*1000
          xqd=max(xqd,FeTxLength(ItRemains)+10.)
        endif
        id=NextQuestid()
        call FeQuestCreate(id,xmi,ymi,xqd,il,' ',0,LightGray,-1,0)
        tpom=5.
        il=1
        call FeQuestLblMake(id,tpom,il,Ven1,'L','N')
        if(idv2.gt.0) then
          il=il+1
          call FeQuestLblMake(id,tpom,il,Ven2,'L','N')
        endif
        if(ErrType.eq.WarningWithESC) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          j=idel(RunningProgram)
          Veta='%Continue '//RunningProgram(:j)
          xdl=FeTxLengthUnder(Veta)+CrwgXd+15.
          xpom=(xqd-xdl)*.5
          tpom=xpom+CrwgXd+15.
          do i=1,3
            il=il+1
            if(i.eq.2) then
              nCrwFirst=CrwLastMade
              Veta='%Skip warnings'
            else if(i.eq.3) then
              Veta='%Break'
            endif
            if(i.gt.2.and.j.ne.0)
     1        Veta=Veta(:idel(Veta)+1)//RunningProgram(:j)
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,1)
            call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          enddo
        else
          nCrwFirst=0
        endif
        if(TakeMouseMove) call FeMouseShape(0)
1000    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
          go to 1100
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1000
        endif
        if(ErrType.eq.WarningWithESC) then
          nCrw=nCrwFirst
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.2) then
                IgnoreW=.true.
              else if(i.eq.3) then
                ErrFlag=1
              endif
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
1100    call FeQuestRemove(id)
        call FeReleaseOutput
        call FeDeferOutput
      endif
2000  if(LstOpened.or.ICDDBatch.or.Console) then
        call DelChar(Ven1,Tabulator)
        idv1=idv1-1
        if(idv2.gt.0) then
          call DelChar(Ven2,Tabulator)
          call DeleteFirstSpaces(Ven2)
          j=index(Ven1,'-')+1
          do i=1,j
            Ven2=' '//Ven2(:idel(Ven2))
          enddo
          idv2=idel(Ven2)
        endif
        if(LstOpened) then
          if(idv2.ne.0) then
            call newln(2)
          else
            call newln(1)
          endif
          write(lst,FormA) Ven1(:idv1)
          if(idv2.gt.0) write(lst,FormA) Ven2(:idv2)
        endif
        if(ICDDBatch) then
          call FeLstWriteLine(Ven1,-1)
          if(idv2.gt.0) call FeLstWriteLine(Ven2,-1)
          if(LogLn.ne.0) then
            write(LogLn,FormA) Ven1(:idv1)
            if(idv2.gt.0) write(LogLn,FormA) Ven2(:idv2)
          endif
        else if(Console) then
          if(OpSystem.le.0) then
            write(6,FormA) ' '//Ven1(:idv1)
          else
            write(6,FormA) Ven1(:idv1)
          endif
          if(idv2.gt.0) then
            if(OpSystem.le.0) then
              write(6,FormA) ' '//Ven2(:idv2)
            else
              write(6,FormA) Ven2(:idv2)
            endif
          endif
        endif
      endif
      if(Console) go to 9999
9000  if(ErrType.eq.FatalError) then
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        stop
      endif
      if(.not.ICDDBatch) then
        WizardMode=WizardModeIn
        QuestLineWidth=QuestLineWidthIn
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
        if(.not.PropFontIn) call FeSetFixFont
      endif
      if(VasekTest.eq.999.and..not.IgnoreW.and..not.IgnoreE)
     1  call FeWinMessage('Cesta?',' ')
9999  return
      end
