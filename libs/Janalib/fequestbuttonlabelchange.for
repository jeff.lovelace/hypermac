      subroutine FeQuestButtonLabelChange(iw,Text)
      include 'fepc.cmn'
      character*(*) Text
      iwa=iw+ButtonFr-1
      ButtonText(iwa)=Text
      i=ButtonState(iwa)
      if(i.ne.ButtonClosed) then
        ButtonState(iwa)=ButtonClosed
        call FeQuestButtonOpen(iw,i)
      endif
      return
      end
