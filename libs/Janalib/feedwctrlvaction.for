      subroutine FeEdwCtrlVAction(id)
      include 'fepc.cmn'
      character*256 Veta
      call FeGetClipboardText(Veta,n)
      KurzorOld=Kurzor
      if(EdwSelFr.ne.EdwSelTo) then
        n1=min(EdwSelFr,EdwSelTo)+1
        n2=max(EdwSelFr,EdwSelTo)
      else
        n1=Kurzor+1
        n2=n1-1
      endif
      if(n1.gt.1) then
        Veta=EdwString(id)(:n1-1)//Veta(:n)
        idl=n1+n-1
      else
        idl=n
      endif
      if(n2.lt.idel(EdwString(id))) then
        EdwString(id)=Veta(:idl)//EdwString(id)(n2+1:)
      else
        EdwString(id)=Veta(:idl)
      endif
      if(EdwSelFr.ne.EdwSelTo) call FeEdwSelClear(id)
      call FeEdwOpen(id)
      Kurzor=KurzorOld
      if(Kurzor.gt.EdwTextMax(id)) then
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      else
        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1    EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2    EdwTextMax(id))
      endif
      call FeEdwBasic(id,White)
      return
      end
