      subroutine FeYesNoAll(xmi,ymi,text,idf,iout)
      include 'fepc.cmn'
      character*12  t12(4)
      character*(*) text
      logical WizardModeIn
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='Yes to %all'
      t12(4)='No to a%ll'
      n=4
      go to 1000
      entry FeYesNoStart(xmi,ymi,text,idf,iout)
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='Yes+%start'
      t12(4)='No+s%tart'
      n=4
      go to 1000
      entry FeYesNoCancel(xmi,ymi,text,idf,iout)
      t12(1)='%Yes'
      t12(2)='%No'
      t12(3)='%Cancel'
      n=3
1000  WizardModeIn=WizardMode
      WizardMode=.false.
      gap=20.
      dbutt=0.
      do i=1,n
        dbutt=dbutt+FeTxLength(t12(i))+20.
      enddo
      dbutt=dbutt+3.*gap
      call FeBoldFont
      xd=max(dbutt+50.,FeTxLength(Text)+25.)
      call FeNormalFont
      id=NextQuestid()
      call FeQuestCreate(id,xmi,ymi,xd,1,Text,0,LightGray,-1,-1)
      CheckKeyboard=.true.
      pomx=(xd-dbutt)*.5
      do i=1,n
        w=FeTxLength(t12(i))+20.
        call FeQuestButtonMake(id,pomx,1,w,ButYd,t12(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtFirst=ButtonLastMade
        pomx=pomx+w+gap
      enddo
      iout=idf
2000  BlockedActiveObj=.false.
      call FeQuestActiveObjOff
      ActiveObj=10000*ActButton+iout+ButtonFr-1
      call FeQuestActiveObjOn
      call FeQuestMouseToButton(iout-nButtFirst+1)
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventKey.and.CheckNumber.eq.JeReturn) then
        go to 3000
      else if(CheckType.eq.EventButton) then
        iout=CheckNumber-nButtFirst+1
        go to 3000
      else if(CheckType.eq.EventKey) then
        if(CheckNumber.eq.JeRight) then
          iout=mod(iout,4)+1
        else if(CheckNumber.eq.JeLeft) then
          iout=mod(iout+2,4)+1
        endif
      endif
      go to 2000
3000  call FeWait(.2)
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      return
      end
