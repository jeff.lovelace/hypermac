      subroutine FeQuestCrwMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                          Check,ExGr)
      include 'fepc.cmn'
      character*(*) Text
      character*1   Justify
      integer Check,ExGr,Color
      logical   YesNo
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-xdw*.5
      go to 1000
      entry FeQuestAbsCrwMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                        Check,ExGr)
      yw=ywi
      yt=yti
1000  CrwTo=CrwTo+1
      CrwLastMade=CrwTo-CrwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActCrw+CrwTo
      QuestCrwTo(id)=CrwTo
      CrwCheck(CrwTo)=Check
      CrwLabel(CrwTo)=Text
      CrwJustify(CrwTo)=Justify
      CrwLabelX(CrwTo)=xt+QuestXMin(LastQuest)
      CrwLabelY(CrwTo)=yt+QuestYMin(LastQuest)
      CrwExGr(CrwTo)=ExGr
      CrwType(CrwTo)=CrwTypeRadio
      if(idel(Text).ge.2) then
        if(Text(1:2).eq.'##') then
          CrwType(CrwTo)=CrwTypeLetter
        else if(Text(1:2).eq.'$$') then
          CrwType(CrwTo)=CrwTypeBMP
          CrwBMPName(CrwTo)=Text(3:)
        endif
      endif
      call FeCrwMake(CrwTo,id,xw,yw,xdw,ydw)
      go to 9999
      entry FeQuestCrwOpen(iwa,YesNo)
      i=iwa+CrwFr-1
      if(CrwLabel(i).ne.' '.and.CrwType(i).eq.CrwTypeRadio) then
        call FeGetTextRectangle(CrwLabelX(i),CrwLabelY(i),
     1    CrwLabel(i),CrwJustify(i),1,x1,x2,y1,y2,refx,refy,conx,cony)
        CrwLabelXMin(i)=x1
        CrwLabelXMax(i)=x2
        CrwLabelYMin(i)=y1
        CrwLabelYMax(i)=y2
      else
        CrwZ(i:i)=' '
        CrwLabelXMax(i)=0.
        CrwLabelYMax(i)=0.
      endif
      call FeCrwOpen(i,YesNo)
      Color=Black
      go to 6000
      entry FeQuestCrwOn(iwa)
      i=iwa+CrwFr-1
      call FeCrwOn(i)
      Color=Black
      go to 6000
      entry FeQuestCrwOff(iwa)
      iw=iwa+CrwFr-1
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      call FeCrwOff(iw)
      if(i.eq.ActCrw.and.iw.eq.j) call FeCrwActivate(iw)
      Color=Black
      i=iw
      go to 6000
      entry FeQuestCrwClose(iwa)
      if(iwa.le.0) go to 9999
      iw=iwa+CrwFr-1
      if(CrwState(iw).eq.CrwClosed.or.
     1   CrwState(iw).eq.CrwRemoved) go to 5100
      call FeCrwClose(iw)
      go to 4000
      entry FeQuestCrwRemove(iwa)
      iw=iwa+CrwFr-1
      if(CrwState(iw).eq.CrwRemoved) go to 5100
      call FeCrwRemove(iw)
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActCrw.and.iw.eq.j) then
        call FeCrwActivate(iw)
        ActiveObj=0
      endif
5000  if(CrwLabel(iw).ne.' '.and.CrwType(iw).eq.CrwTypeRadio)
     1  call FeTextErase(0,CrwLabelX(iw),CrwLabelY(iw),CrwLabel(iw),
     2                   CrwJustify(iw),LightGray)
5100  CrwZ(iw:iw)=' '
      go to 9999
      entry FeQuestCrwLock(iwa)
      i=iwa+CrwFr-1
      call FeCrwLock(i)
      Color=Black
      go to 6000
      entry FeQuestCrwDisable(iwa)
      i=iwa+CrwFr-1
      call FeCrwDisable(i)
      Color=White
      go to 6000
      entry FeQuestCrwActivate(iwa)
      i=iwa+CrwFr-1
      call FeCrwActivate(i)
      go to 9999
      entry FeQuestCrwDeactivate(iwa)
      i=iwa+CrwFr-1
      call FeCrwDeactivate(i)
      go to 9999
6000  if(CrwLabel(i).ne.' '.and.CrwType(i).ne.CrwTypeLetter.and.
     1   CrwType(i).ne.CrwTypeBMP) then
        call FeTextErase(0,CrwLabelX(i),CrwLabelY(i),CrwLabel(i),
     1                   CrwJustify(i),LightGray)
        call FeOutStUnder(0,CrwLabelX(i),CrwLabelY(i),CrwLabel(i),
     1                    CrwJustify(i),Color,Color,CrwZ(i:i))
      endif
9999  return
      end
