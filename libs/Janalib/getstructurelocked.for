      logical function GetStructureLocked(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName
      character*80  t80
      character*10  Slovo
      character*1   String(80)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,CR
      logical ExistFile,EqIgCase
      equivalence (t80,String)
      data CR,LF/13,10/
      GetStructureLocked=.false.
      WorkString=StrName(:idel(StrName))//'.usd'
      if(ExistFile(WorkString)) then
        ln=FeOpenBinaryFile(WorkString(:idel(WorkString)))
        if(ln.le.0) go to 5000
        i=FeReadBinaryFile(ln,String,80)
        i=FeCloseBinaryFile(ln)
        do i=1,80
          j=ichar(t80(i:i))
          if(j.eq.CR.or.j.eq.LF) then
            t80(i:)=' '
            go to 1500
          endif
        enddo
        go to 5000
1500    k=0
2000    call kus(t80,k,Slovo)
        if(EqIgCase(Slovo,'locked')) then
          GetStructureLocked=.true.
          go to 5000
        else
          if(k.lt.len(t80)) go to 2000
        endif
      endif
5000  return
      end
