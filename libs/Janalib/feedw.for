      subroutine FeEdw
      include 'fepc.cmn'
      integer ColorSel
      logical PrepisEdw,PrepisKurzoru
      character*1 ChKey
      go to 9999
      entry FeEdwMake(id,idc,xm,ym,xd,yd)
      EdwXMin(id)=xm
      EdwXMax(id)=xm+xd
      EdwYMin(id)=ym
      EdwYMax(id)=ym+yd
      if(idc.gt.0) then
        EdwXMin(id)=EdwXMin(id)+QuestXMin(idc)
        EdwXMax(id)=EdwXMax(id)+QuestXMin(idc)
        EdwYMin(id)=EdwYMin(id)+QuestYMin(idc)
        EdwYMax(id)=EdwYMax(id)+QuestYMin(idc)
      endif
      EdwTextXMin(id)=EdwXMin(id)+EdwMarginSize
      EdwTextXMax(id)=EdwXMax(id)-EdwMarginSize
      EdwTextXLen(id)=EdwTextXMax(id)-EdwTextXMin(id)
      EdwState(id)=EdwClosed
      LastEdw=0
      EdwSelected(id)=0
      go to 9999
      entry FeEdwActivate(id)
      PrepisKurzoru=.true.
      ColorSel=Black
      go to 1500
      entry FeEdwDeactivate(id)
      PrepisKurzoru=.false.
      ColorSel=LightGray
1500  if(EdwLabel(id).ne.' ') then
        x1=EdwLabelXMin(id)
        x2=EdwLabelXMax(id)
        y1=EdwLabelYMin(id)
        y2=EdwLabelYMax(id)
        xu(1)=x1-1.
        yu(1)=y1-1.
        xu(2)=xu(1)
        yu(2)=y2+1.
        call FeLineType(DenseDottedLine)
        call FePolyLine(2,xu,yu,ColorSel)
        xu(3)=x2+1.
        xu(4)=xu(3)
        call FePolyLine(2,xu(3),yu,ColorSel)
        xu(2)=xu(3)
        yu(3)=yu(1)
        yu(4)=yu(1)
        call FePolyLine(2,xu,yu(3),ColorSel)
        yu(1)=yu(2)
        call FePolyLine(2,xu,yu,ColorSel)
        call FeLineType(NormalLine)
      endif
      if(EdwState(id).ne.EdwClosed.and.EdwState(id).ne.EdwRemoved) then
        PrepisEdw=.true.
        KurzorEdw=0
        Kurzor=EdwKurzor(id)
        go to 4000
      else
        go to 9999
      endif
      entry FeEdwOpen(id)
      if(id.ne.LastEdw) then
        if(LastEdw.ne.0) call FeEdwHighlight(LastEdw)
        EdwSelFr=0
        EdwSelTo=0
      endif
      call FeEdwBasic(id,White)
      EdwState(id)=EdwOpened
      LastEdw=0
      if(id.eq.KurzorEdw) KurzorEdw=0
      PrepisKurzoru=id.eq.EdwActive
      PrepisEdw=.true.
      EdwTextLen(id)=idel(EdwString(id))
      call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),1,1,
     1  EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
      if(EdwTextLen(id).gt.EdwTextMax(id))
     1  call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2    EdwTextLen(id),-1,EdwTextXLen(id),EdwTextMin(id),
     3    EdwTextMax(id))
      Kurzor=EdwTextMax(id)
      EdwKurzor(id)=Kurzor
      go to 4000
      entry FeEdwMakeAction(id)
      if(id.le.0) go to 9999
      Kurzor=EdwKurzor(id)
      if(KurzorClick.ge.0.or.id.ne.LastEdw) then
        if(LastEdw.ne.0) call FeEdwHighlight(LastEdw)
        EdwSelFr=0
        EdwSelTo=0
      endif
      PrepisKurzoru=.true.
      PrepisEdw=.true.
      if(EventType.eq.EventEdw) then
        if(KurzorClick.ge.0) then
          if(EventNumberAbs.eq.id) then
            Kurzor=min(KurzorClick,EdwTextLen(id))
            KurzorClick=-1
          else
            go to 2500
          endif
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
        go to 2500
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        go to 9999
      else if(EventType.eq.EventASCII.and.
     1        (EdwTextLen(id).lt.256.or..not.InsertMode)) then
        if(EdwSelTo.ne.EdwSelFr) call FeEdwSelDelete(id)
        Key=EventNumber
        ChKey=char(Key)
        if(Kurzor.lt.EdwTextLen(id)) then
          if(InsertMode) then
            EdwString(id)=EdwString(id)(1:Kurzor)//ChKey//
     1                    EdwString(id)(Kurzor+1:EdwTextLen(id))
          else
            EdwString(id)(Kurzor+1:Kurzor+1)=ChKey
          endif
        else
          EdwString(id)=EdwString(id)(:Kurzor)//ChKey
        endif
        if(InsertMode.or.Kurzor.ge.EdwTextLen(id))
     1    EdwTextLen(id)=EdwTextLen(id)+1
        Kurzor=Kurzor+1
        EventNumber=0
        if(Kurzor.gt.EdwTextMax(id)) then
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
        else
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2      EdwTextMax(id))
        endif
      else if(EventType.eq.EventKey) then
        if(EventNumber.eq.JeRight) then
          EventNumber=0
          if(Kurzor.lt.EdwTextLen(id)) then
            if(ShiftPressed) then
              if(EdwSelTo.ne.EdwSelFr) then
                EdwSelTo=EdwSelTo+1
              else
                EdwSelFr=Kurzor
                EdwSelTo=Kurzor+1
              endif
            else
              call FeEdwSelClear(id)
            endif
            Kurzor=Kurzor+1
            if(Kurzor.gt.EdwTextMax(id))
     1        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2          Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          else
            go to 2500
          endif
        else if(EventNumber.eq.JeLeft) then
          EventNumber=0
          if(Kurzor.gt.0) then
            if(ShiftPressed) then
              if(EdwSelTo.ne.EdwSelFr) then
                EdwSelTo=EdwSelTo-1
              else
                EdwSelFr=Kurzor
                EdwSelTo=Kurzor-1
              endif
            else
              call FeEdwSelClear(id)
            endif
            Kurzor=Kurzor-1
            if(Kurzor.lt.EdwTextMin(id).and.Kurzor.gt.0)
     1        call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     2          Kurzor,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          else
            go to 2500
          endif
        else if(EventNumber.eq.JeInsert) then
          EventNumber=0
          InsertMode=.not.InsertMode
        else if(EventNumber.eq.JeHome) then
          EventNumber=0
          if(ShiftPressed) then
            if(EdwSelTo.ne.EdwSelFr) then
              EdwSelTo=0
            else
              EdwSelFr=Kurzor
              EdwSelTo=0
            endif
          else
            call FeEdwSelClear(id)
          endif
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      1,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
          Kurzor=0
        else if(EventNumber.eq.JeEnd) then
          EventNumber=0
          if(ShiftPressed) then
            if(EdwSelTo.ne.EdwSelFr) then
              EdwSelTo=EdwTextLen(id)
            else
              EdwSelFr=Kurzor
              EdwSelTo=EdwTextLen(id)
            endif
          else
            call FeEdwSelClear(id)
          endif
          Kurzor=EdwTextLen(id)
          call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1      Kurzor,-1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
        else if(EventNumber.eq.JeBackspace) then
          EventNumber=0
          if(EdwSelTo.eq.EdwSelFr) then
            if(Kurzor.gt.0) then
              if(Kurzor.lt.EdwTextLen(id)) then
                EdwString(id)=EdwString(id)(1:Kurzor-1)//
     1                        EdwString(id)(Kurzor+1:EdwTextLen(id))
              else if(Kurzor.eq.EdwTextLen(id)) then
                EdwString(id)(EdwTextLen(id):)=' '
              endif
              if(Kurzor.le.EdwTextLen(id))
     1          EdwTextLen(id)=EdwTextLen(id)-1
              Kurzor=Kurzor-1
              k=min(Kurzor,EdwTextMin(id))
              call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1          k,1,EdwTextXLen(id),EdwTextMin(id),EdwTextMax(id))
            endif
          else
            call FeEdwSelDelete(id)
          endif
        else if(EventNumber.eq.JeDeleteUnder) then
          EventNumber=0
          if(EdwSelTo.eq.EdwSelFr) then
            if(Kurzor.lt.EdwTextLen(id)) then
              if(Kurzor.lt.EdwTextLen(id)-1) then
                EdwString(id)=EdwString(id)(:Kurzor)//
     1                        EdwString(id)(Kurzor+2:EdwTextLen(id))
              else
                EdwString(id)=EdwString(id)(:Kurzor)
              endif
              EdwTextLen(id)=EdwTextLen(id)-1
            endif
            call FeGetVisibleSubstring(EdwString(id)(:EdwTextLen(id)),
     1        EdwTextMin(id),1,EdwTextXLen(id),EdwTextMin(id),
     2        EdwTextMax(id))
          else
            call FeEdwSelDelete(id)
          endif
        else
          go to 2500
        endif
      endif
      go to 4000
2500  LastEdw=id
      EdwKurzor(id)=Kurzor
      go to 9999
      entry FeEdwLock(id)
      if(EdwState(id).eq.EdwClosed.or.EdwState(id).eq.EdwRemoved.or.
     1   EdwState(id).eq.EdwLocked) go to 9999
      call FeEdwBasic(id,LightYellow)
      EdwState(id)=EdwLocked
      go to 9999
      entry FeEdwDisable(id)
      if(EdwState(id).eq.EdwDisabled) go to 9999
      call FeEdwBasic(id,WhiteGray)
      EdwState(id)=EdwDisabled
      go to 9999
      entry FeEdwClose(id)
      if(EdwState(id).eq.EdwClosed.or.EdwState(id).eq.EdwRemoved)
     1   go to 9999
      i=EdwClosed
      go to 2900
      entry FeEdwRemove(id)
      if(EdwState(id).eq.EdwRemoved) go to 9999
      i=EdwRemoved
2900  LastEdw=0
      if(EdwUpDown(id).ne.0) then
        call FeEudClose(id,1)
        call FeEudClose(id,2)
      endif
      if(KurzorEdw.eq.id) KurzorEdw=0
      dx=2.
      call FeFillRectangle(EdwXmin(id)-dx,EdwXmax(id)+dx,EdwYmin(id)-dx,
     1                     EdwYmax(id)+dx,4,0,0,LightGray)
      EdwState(id)=i
      go to 9999
4000  xp=EdwTextXMin(id)
      yp=(EdwYmin(id)+EdwYmax(id))*.5
      call FeEdwBasic(id,White)
      if(EdwTextMin(id).ge.1.and.EdwTextMax(id).ge.1)
     1  call FeOutSt(0,xp,yp,
     2  EdwString(id)(EdwTextMin(id):EdwTextMax(id)),'L',Black)
      EdwKurzor(id)=Kurzor
      if(PrepisKurzoru) then
        KurzorEdw=id
        call FeZmenKurzor(id)
      endif
      call FeEdwHighlight(id)
      LastEdw=id
      if(EdwSelected(id).ne.1) then
        go to 9999
      else
        ColorSel=Black
        go to 5000
      endif
      go to 9999
      entry FeEdwSelect(id)
      ColorSel=Black
      EdwSelected(id)=1
      go to 5000
      entry FeEdwDeselect(id)
      ColorSel=White
      EdwSelected(id)=0
5000  xu(1)=EdwXMin(id)
      yu(1)=EdwYMin(id)
      xu(2)=EdwXMin(id)+2.
      yu(2)=EdwYMin(id)
      xu(3)=EdwXMin(id)
      yu(3)=EdwYMin(id)+2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMax(id)
      yu(1)=EdwYMax(id)
      xu(2)=EdwXMax(id)-2.
      yu(2)=EdwYMax(id)
      xu(3)=EdwXMax(id)
      yu(3)=EdwYMax(id)-2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMax(id)
      yu(1)=EdwYMin(id)
      xu(2)=EdwXMax(id)-2.
      yu(2)=EdwYMin(id)
      xu(3)=EdwXMax(id)
      yu(3)=EdwYMin(id)+2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
      xu(1)=EdwXMin(id)
      yu(1)=EdwYMax(id)
      xu(2)=EdwXMin(id)+2.
      yu(2)=EdwYMax(id)
      xu(3)=EdwXMin(id)
      yu(3)=EdwYMax(id)-2.
      call FePolygon(xu,yu,3,4,0,0,ColorSel)
9999  return
      end
