      subroutine FeOutSt(id,xmi,ymi,Text,Justify,Color)
      include 'fepc.cmn'
      character*(*) text
      character*1 Justify
      character*256 Veta
      character*1 ChTabActual
      integer Color,TTabActual
      Veta=Text
      i=index(Veta,Tabulator)
      if(Justify.eq.'C'.or.Justify.eq.'R'.or.NTabs(UseTabs).le.0.or.
     1   i.le.0) then
        call FeCharOut(id,xmi,ymi,Veta,Justify,Color)
      else
        xm=xmi
        ym=ymi
        if(id.gt.0) then
          if(QuestState(id).ne.0) then
            xm=xm+QuestXMin(id)
            ym=ym+QuestYMin(id)
          endif
        endif
        kk=0
        it=0
        xmp=xm
        TTabActual=0
        ChTabActual=' '
1100    kp=kk+1
        i=index(Veta(kp:),Tabulator)
        if(i.le.0) then
          kk=idel(Veta)
        else
          kk=kp+i-2
        endif
        if(kk.ge.kp) then
          if(TTabActual.eq.IdRightTab) then
            xmk=xmp+FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdLeftTab) then
            xmk=xmp
            xmp=xmp-FeTxLength(Veta(kp:kk))
          else if(TTabActual.eq.IdCharTab) then
            i=index(Veta(kp:kk),ChTabActual)+kp-1
            xmk=xmp+FeTxLength(Veta(i:kk))
            if(i.ge.kp) xmp=xmp-FeTxLength(Veta(kp:i-1))
          else if(TTabActual.eq.IdCenterTab) then
            xmk=xmp+.5*FeTxLength(Veta(kp:kk))
            xmp=xmp-.5*FeTxLength(Veta(kp:kk))
          endif
          call FeCharOut(0,xmp,ym,Veta(kp:kk),Justify,Color)
          if(kk.ge.idel(Veta).or.(kk.eq.idel(Veta)-1.and.
     1       Veta(kk+1:kk+1).eq.Tabulator)) go to 9999
        endif
        it=it+1
        if(it.le.NTabs(UseTabs)) then
          TTabActual=TTabs(it,UseTabs)
          ChTabActual=ChTabs(it,UseTabs)
          xmp=xm+XTabs(it,UseTabs)
          kk=kk+1
          go to 1100
        else
          kp=kk+2
          kk=idel(Veta)
          if(kp.le.kk)
     1      call FeCharOut(0,xmk+5.,ym,Veta(kp:kk),Justify,Color)
        endif
      endif
9999  return
      end
