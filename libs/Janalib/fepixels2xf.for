      subroutine FePixels2Xf(ix,iy,xp)
      include 'fepc.cmn'
      dimension xp(3),xo(3)
      xo(1)=nint(float(ix)/EnlargeFactor)
      xo(2)=nint(float(iy)/EnlargeFactor)
      xo(3)=0.
      call FeX2Xf(xo,xp)
      return
      end
