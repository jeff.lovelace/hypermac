      function LineNumberInQuest(id,y,klic)
      include 'fepc.cmn'
      LineNumberInQuest=0
      corr=0
      if(klic.eq.1) corr=4.
      n=nint((QuestLength(id)-y-corr)/QuestLineWidth)
      if(n.le.0) then
        n=1
        goto 600
      endif
500   pom=QuestYPosition(id,n)
      if(pom.lt.y) then
        n=n-1
        go to 500
      endif
600   ipom=-n*10
      dif=abs(QuestYPosition(id,ipom)-y-corr+QuestYMin(id))
1000  ipom=ipom-1
      pom=abs(QuestYPosition(id,ipom)-y-corr+QuestYMin(id))
      if(pom.lt.dif) then
        dif=pom
        go to 1000
      endif
      LineNumberInQuest=ipom+1
      return
      end
