      logical function StructureExists(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      character*(*) StrName
      logical   ExistFile
      i=idel(StrName)
      if(i.le.0) then
        StructureExists=.false.
      else
        t256=StrName
        StructureExists=ExistFile(t256(:i)//'.m40').or.
     1                  ExistFile(t256(:i)//'.m50').or.
     2                  ExistFile(t256(:i)//'.m91')
      endif
      return
      end
