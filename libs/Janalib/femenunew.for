      integer function FeMenuNew(xmi,ymi,xdi,men,menl,i1,i2,id,jak)
      include 'fepc.cmn'
      character*(*) men(i1:i2)
      character*80 MenuTmp
      character*30 MenZ
      character*1 Znak
      dimension xup(5),yup(5),menl(i1:i2)
      call FeKartRefresh(0)
      KartIdOld=KartId
      KartId=0
      xm=xmi
      ym=ymi
      MenZ=' '
      if(xdi.lt.0.) then
        xd=20.
        do i=i1,i2
          xd=max(xd,FeTxLengthUnder(Men(i))+2.*EdwMarginSize)
        enddo
      else
        xd=xdi
      endif
      yd=float(i2-i1+1)*MenuLineWidth
      if(xm.lt.0.) then
        xm=XCenGrWin-xd*.5
      endif
      if(ym.lt.0.) then
        ym=YCenGrWin-yd*.5
      endif
      xm=anint(xm)
      xd=anint(xd)
      ym=anint(ym)
      yd=anint(yd)
      MenuTmp='jmnu'
      if(OpSystem.le.0) call CreateTmpFile(MenuTmp,i,1)
      call FeSaveImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp)
      if(jak.eq.0) then
        ip=LightGray
      else
        ip=White
      endif
      call FeFillRectangle(xm-1.,xm+xd+1.,ym-1.,ym+yd+1.,4,0,0,ip)
      if(Jak.eq.0) then
        call FeDrawFrame(xm,ym,xd,yd,FrameWidth,Gray,White,Black,
     1                   LastQuest.ne.0)
      else
        xup(1)=xm
        yup(1)=ym
        xup(2)=xm+xd
        yup(2)=ym
        xup(3)=xup(2)
        yup(3)=ym+yd
        xup(4)=xm
        yup(4)=yup(3)
        xup(5)=xup(1)
        yup(5)=yup(1)
      endif
      idef=i1
      yp=ym+yd
      do i=i1,i2
        j=i-i1+1
        if(i.eq.idef) then
          if(menl(i).gt.0) then
            ipp=White
            icc=DarkBlue
          else
            ipp=Gray
            icc=DarkBlue
          endif
          call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                       -4.)
        else
          if(menl(i).gt.0) then
            ipp=Black
          else
            ipp=WhiteGray
          endif
          icc=ip
        endif
        call FeWrMenuItem(xm,yp,xd,Men(i),MenZ(j:j),ipp,icc)
        yp=yp-MenuLineWidth
      enddo
      if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      call mala(MenZ)
      if(VasekTest.ne.0) then
        idefo=idel(MenZ)
        do 1600i=1,idefo-1
          if(MenZ(i:i).eq.' ') go to 1600
          do j=i+1,idefo
            if(MenZ(i:i).eq.MenZ(j:j)) go to 1610
          enddo
1600    continue
        go to 1900
1610    TextInfo(1)=MenZ
        Ninfo=2
        write(TextInfo(2),'(2i3)') i,j
        call FeInfoOut(-1.,-1.,'Nejednoznacna rychla klavesa:','L')
      endif
1900  call FeFrToChange(LastActiveQuest,0)
      call FeFrToRefresh(-1)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      call FeMouseShape(0)
2000  call FeEvent(0)
      idefo=idef
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(Xpos.ge.xm.and.Xpos.le.xm+xd.and.
     1     Ypos.ge.ym.and.Ypos.le.ym+yd) then
          idef=nint((ym+yd-Ypos)/MenuLineWidth-.5)+i1
        else
          idef=i1-1
        endif
        idef=min(idef,i2)
      else if(EventType.eq.EventKey.and.(EventNumber.eq.JeUp.or.
     1                                   EventNumber.eq.JeDown)) then
        if(idef.ge.i1) then
          if(EventNumber.eq.JeUp) then
            idef=idef-1
            if(idef.lt.i1) idef=i2
          else
            idef=idef+1
            if(idef.gt.i2) idef=i1
          endif
        endif
        call FeMoveMouseTo(xm+xd*.5,ym+yd-float(idef-i1)*MenuLineWidth
     1                     -4.)
      else if(EventType.eq.EventASCII) then
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(MenZ,Znak)
        if(i.ne.0) then
          idef=i+i1-1
          go to 3000
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        if(idef.ge.i1.and.menl(idef).gt.0) go to 3000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
        idef=i1-1
        go to 3000
      endif
      if(idefo.ne.idef) then
        if(idefo.ge.i1) then
          if(menl(idefo).gt.0) then
            ipp=Black
            icc=ip
          else
            ipp=WhiteGray
            icc=ip
          endif
          call FeWrMenuItem(xm,ym+yd-float(idefo-i1)*MenuLineWidth,
     1                      xd,Men(idefo),Znak,ipp,icc)
        endif
        if(idef. ge.i1) then
          if(menl(idef).gt.0) then
            ipp=White
            icc=DarkBlue
          else
            ipp=Gray
            icc=DarkBlue
          endif
          call FeWrMenuItem(xm,ym+yd-float(idef -i1)*MenuLineWidth,
     1                     xd,Men(idef),Znak,ipp,icc)
        endif
        if(Jak.ne.0) call FePolyLine(5,xup,yup,Black)
      endif
      go to 2000
3000  if(idef.gt.0) then
        if(menl(idef).le.0) go to 2000
      endif
      call FeLoadImage(xm-FrameWidth,xm+xd+FrameWidth,ym-FrameWidth,
     1                 ym+yd+FrameWidth,MenuTmp,0)
      FeMenuNew=idef
      TakeMouseMove=.false.
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      if(KartIdOld.ne.0) then
        call FeKartRefresh(1)
        KartId=KartIdOld
      endif
      call FeFrToRefresh(LastActiveQuest)
      call FeDeferOutput
      return
      end
