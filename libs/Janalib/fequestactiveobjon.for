      subroutine FeQuestActiveObjOn
      include 'fepc.cmn'
      if(BlockedActiveObj) go to 9999
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton) then
        if(ButtonState(j).ne.ButtonClosed.and.
     1     ButtonState(j).ne.ButtonDisabled) then
          call FeButtonActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActEdw) then
        if(EdwState(j).ne.EdwClosed.and.
     1     EdwState(j).ne.EdwDisabled.and.
     2     EdwState(j).ne.EdwLocked) then
          EdwActive=j
          call FeEdwActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActCrw) then
        if(CrwState(j).ne.CrwClosed.and.
     1     CrwState(j).ne.CrwDisabled.and.
     2     CrwState(j).ne.CrwLocked) then
          call FeCrwActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActRolMenu) then
        if(RolMenuState(j).ne.RolMenuClosed) then
          call FeRolMenuActivate(j)
        else
          go to 9000
        endif
      else if(i.eq.ActSbw) then
        SbwActive=j
        call FeSbwActivate(j)
      else
        ActiveObj=0
      endif
      go to 9999
9000  ActiveObj=-1
9999  return
      end
