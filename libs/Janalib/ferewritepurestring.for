      subroutine FeRewritePureString(id,xp,yp,StringOld,StringNew,
     1                               Justify,EraseColor,WriteColor)
      include 'fepc.cmn'
      character*(*) StringOld,StringNew,Justify
      integer EraseColor,WriteColor
      logical Deferred
      Deferred=DeferredOutput
      if(.not.Deferred) call FeDeferOutput
      call FeGetPureTextRectangle(xp,yp,StringOld,Justify,1,xpold,xkold,
     1                            ypold,ykold,refx,refy,conx,cony)
      ypold=ypold-2./EnlargeFactor
      if(id.gt.0) then
        xpold=xpold+QuestXMin(id)
        xkold=xkold+QuestXMin(id)
        ypold=ypold+QuestYMin(id)
        ykold=ykold+QuestYMin(id)
      endif
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,EraseColor)
      call FeOutSt(id,xp,yp,StringNew,Justify,WriteColor)
      if(.not.Deferred) call FeReleaseOutput
      return
      end
