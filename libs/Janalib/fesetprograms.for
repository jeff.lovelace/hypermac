      subroutine FeSetPrograms
      use Basic_mod
      parameter (NPrograms=13)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 EdwStringQuest,IniFileSave,Veta,CurrentDirO
      character*80 t80
      character*30 :: LabelsEdw(NPrograms) =
     1             (/'E%ditor name:             ',
     2               '%X-Shape:                 ',
     3               'G%raphic viewer:          ',
     4               '%3d visualization of maps:',
     5               'DPlo%t:                   ',
     6               'S%IR                      ',
     7               '%Expo                     ',
     8               'S%helxt:                  ',
     9               'Ger%minography            ',
     a               'Shell comm%and:           ',
     1               'G%nuplot command:         ',
     2               '%GS command:              ',
     3               '%Path to ELMAM2 database: '/)
      integer FeChdir,RolMenuSelectedQuest
      logical FeYesNo,FileDiff
500   AllowResizing=.true.
      NSIR=1
      NExpo=1
      IniFileSave='jins'
      call CreateTmpFile(IniFileSave,i,0)
      call FeTmpFilesAdd(IniFileSave)
1000  call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini',IniFileSave)
      CurrentDirO=CurrentDir
      id=NextQuestId()
      xqd=500.
      il=NPrograms
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Programs',0,LightGray,
     1                   0,0)
      il=0
      tpom=5.
      dpom=250.
      xpom=0.
      do i=1,NPrograms
        xpom=max(FeTxLengthUnder(LabelsEdw(i)),xpom)
      enddo
      xpom=tpom+xpom+10.
      do i=1,NPrograms
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,LabelsEdw(i),'L',dpom,
     1                      EdwYd,1)
        if(i.eq.1) then
          nEdwEdit=EdwLastMade
          Veta=EditorName
          dpomb=80.
          xpomb=(xqd+xpom+dpom-dpomb)*.5
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,'Brow%se')
          nButBrowse=ButtonLastMade
          call FeQuestButtonOpen(nButBrowse,ButtonOff)
        else if(i.eq.2) then
          nEdwXShape=EdwLastMade
          Veta=CallXShape
        else if(i.eq.3) then
          nEdwGrViewer=EdwLastMade
          Veta=CallGraphic
        else if(i.eq.4) then
          nEdw3dMaps=EdwLastMade
          Veta=Call3dMaps
        else if(i.eq.5) then
          nEdwDPlot=EdwLastMade
          Veta=CallDPlot
        else if(i.eq.6) then
          nEdwSIR=EdwLastMade
          xpomp=tpom+27.
          dpomp=50.
          call FeQuestRolMenuMake(id,xpomp,il,xpomp,il,' ','L',dpomp,
     1                            EdwYd,1)
          nRolMenuSIR=RolMenuLastMade
          call FeQuestRolMenuOpen(RolMenuLastMade,SIRName,6,NSIR)
          Veta=DirSIR(NSIR)(:idel(DirSIR(NSIR)))//
     1         CallSIR(NSIR)(:idel(CallSIR(NSIR)))
        else if(i.eq.7) then
          nEdwExpo=EdwLastMade
          call FeQuestRolMenuMake(id,xpomp,il,xpomp,il,' ','L',dpomp,
     1                            EdwYd,1)
          nRolMenuExpo=RolMenuLastMade
          call FeQuestRolMenuOpen(RolMenuLastMade,ExpoName,5,NExpo)

          Veta=DirExpo(NExpo)(:idel(DirExpo(NExpo)))//
     1      CallExpo(NExpo)(:idel(CallExpo(NExpo)))
        else if(i.eq.8) then
          nEdwShelxt=EdwLastMade
          Veta=CallShelxt
        else if(i.eq.9) then
          nEdwGeminography=EdwLastMade
          Veta=CallGeminography
        else if(i.eq.10) then
          nEdwShell=EdwLastMade
          Veta=FirstCommand
        else if(i.eq.11) then
          nEdwGnuplot=EdwLastMade
          Veta=CallGnuplot
        else if(i.eq.12) then
          nEdwGS=EdwLastMade
          Veta=CallGS
        else if(i.eq.13) then
          nEdwELMAM=EdwLastMade
          Veta=PathToELMAM
        endif
        call FeQuestStringEdwOpen(EdwLastMade,Veta)
      enddo
      MakeExternalCheck=1
1500  call FeQuestEvent(id,ich)
      izpet=0
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButBrowse)
     1  then
        Veta=EdwStringQuest(nEdwLast)
        call ExtractFileName(Veta,t80)
        call ExtractDirectory(Veta,Veta)
        i=FeChdir(Veta)
        call FeFileManager('Browser',t80,'*.*',0,.false.,ich)
        if(ich.ne.0) go to 1500
        Veta=CurrentDir(:idel(CurrentDir))//t80(:idel(t80))
        call FeQuestStringEdwOpen(nEdwLast,Veta)
        go to 1500
      else if(CheckType.eq.EventEdw) then
        nEdwLast=CheckNumber
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuSIR) then
        i=RolMenuSelectedQuest(nRolMenuSIR)
        if(i.ne.NSIR) then
          Veta=EdwStringQuest(nEdwSIR)
          call ExtractFileName(Veta,CallSIR(NSIR))
          call ExtractDirectory(Veta,DirSIR(NSIR))
          NSIR=i
          Veta=DirSIR(NSIR)(:idel(DirSIR(NSIR)))//
     1         CallSIR(NSIR)(:idel(CallSIR(NSIR)))
          call FeQuestStringEdwOpen(nEdwSIR,Veta)
          nEdwLast=nEdwSIR
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuExpo) then
        i=RolMenuSelectedQuest(nRolMenuExpo)
        if(i.ne.NExpo) then
          Veta=EdwStringQuest(nEdwExpo)
          call ExtractFileName(Veta,CallExpo(NExpo))
          call ExtractDirectory(Veta,DirExpo(NExpo))
          NExpo=i
          Veta=DirExpo(NExpo)(:idel(DirExpo(NExpo)))//
     1         CallExpo(NExpo)(:idel(CallExpo(NExpo)))
          call FeQuestStringEdwOpen(nEdwExpo,Veta)
          nEdwLast=nEdwExpo
        endif
        go to 1500
      else if(CheckType.eq.EventResize) then
        ich=0
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2000  if(ich.eq.0) then
        EditorName=EdwStringQuest(nEdwEdit)
        FirstCommand=EdwStringQuest(nEdwShell)
        CallXShape=EdwStringQuest(nEdwXShape)
        CallGraphic=EdwStringQuest(nEdwGrViewer)
        Call3dMaps=EdwStringQuest(nEdw3dMaps)
        CallDPlot=EdwStringQuest(nEdwDPlot)
        CallGnuplot=EdwStringQuest(nEdwGnuplot)
        CallGS=EdwStringQuest(nEdwGS)
        Veta=EdwStringQuest(nEdwSIR)
        call ExtractFileName(Veta,CallSIR(NSIR))
        call ExtractDirectory(Veta,DirSIR(NSIR))
        Veta=EdwStringQuest(nEdwExpo)
        call ExtractFileName(Veta,CallExpo(NExpo))
        call ExtractDirectory(Veta,DirExpo(NExpo))
        PathToELMAM=EdwStringQuest(nEdwELMAM)
        CallShelxt=EdwStringQuest(nEdwShelxt)
        CallGeminography=EdwStringQuest(nEdwGeminography)
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
2500  call FeQuestRemove(id)
      if(CheckType.eq.EventResize) then
        call FeMakeGrWin(0.,0.,YBottomMargin,0.)
        call FeBottomInfo(' ')
        go to 500
      endif
      MakeExternalCheck=0
      if(ich.eq.0) then
        if(FileDiff(IniFileSave,
     1              HomeDir(:idel(HomeDir))//
     2              MasterName(:idel(MasterName))//'.ini')) then
          if(.not.FeYesNo(-1.,-1.,
     1       'Do you want to save the made changes?',1)) go to 9000
        endif
      endif
      go to 9999
9000  call CopyFile(IniFileSave,HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini')
9999  call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      AllowResizing=.false.
      return
      end
