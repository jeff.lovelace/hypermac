      logical function GetStructureParent(StrName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName
      character*256 Veta,t256
      logical ExistFile,EqIgCase
      Veta=StrName(:idel(StrName))//'.m50'
      GetStructureParent=.false.
      ln=0
      if(.not.ExistFile(Veta)) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1500  read(ln,FormA,err=9999,end=9999) Veta
      k=0
      call Kus(Veta,k,t256)
      if(EqIgCase(t256,'end')) go to 9999
      if(EqIgCase(t256,'parentstr')) then
        GetStructureParent=.true.
        go to 9999
      endif
      go to 1500
9999  call CloseIfOpened(ln)
      return
      end
