      subroutine FeListPrint(FileName,lnlst,ipg,npg,kam)
      include 'fepc.cmn'
      integer intpg(2,100),EdwStateQuest,RolMenuSelectedQuest
      logical lpom
      logical FeYesNo,pis,nacteno,canceled,konec,CrwLogicQuest,
     1        PrintToFile,Strankovani
      character*(*) filename
      character*1 zn
      character*10 ch10
      character*80 ch80,whichPgs,PSPrinters(100)
      character*256 radka,ch256
      character*256 EdwStringQuest,tmpout,toFile
      kam=0
      pis=.true.
      nacteno=.false.
      canceled=.false.
      nintpg=0
      i=index(FileName,'.')-1
      if(i.le.0) i=idel(FileName)
      toFile=FileName(:i)//'.ps'
      call FeGetPSPrinters(PSPrinters,nPSPrinters)
      if(nPSPrinters.gt.0) then
        PrintToFile=.false.
      else
        PrintToFile=.true.
      endif
      il=5
      id=NextQuestId()
      QuestWidth=300.
      if(nPSPrinters.gt.0) then
        il=il+1
        tpom=5.
        ch80='Printers with PostScript driver:'
        xpom=tpom+FeTxLengthUnder(ch80)+10.
        dpom=20.
        do i=1,nPSPrinters
          dpom=max(dpom,FeTxLengthUnder(PSPrinters(i))+2.*EdwMarginSize)
        enddo
        dpom=dpom+EdwYd+20.
        xpom1=xpom+dpom+5.
        QuestWidth=max(xpom1+EdwYd+5.,QuestWidth)
      endif
      call FeQuestCreate(id,-1.,-1.,QuestWidth,il,'Print listing',0,
     1                   LightGray,0,0)
      il=0
      if(nPSPrinters.gt.0) then
        il=il+1
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,
     1                          0)
        nRolMenuPrinters=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,PSPrinters,NPSPrinters,
     1                          1)
      else
        nRolMenuPrinters=0
      endif
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=1,3
        if(i.eq.1) then
          if(npg.gt.1) then
            write(ch80,'(i10)') npg
            call Zhusti(ch80)
            ch80='All '//ch80(:idel(ch80))//' pages'
          else
            ch80='One page'
          endif
        else if(i.eq.2) then
          ch80='Current page'
        else
          ch80='Pages:'
        endif
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwAll=CrwLastMade
          nCrwPageSelected=CrwLastMade
        else if(i.eq.2) then
          nCrwCur=CrwLastMade
        else
          nCrwSelP=CrwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(ch80)+5.
      ch80='Example: 1,2,4-6,30-'
      dpom=QuestWidth-xpom-5.
      call FeQuestEdwMake(id,xpom,il+1,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwSelP=EdwLastMade
      il=il+2
      xpom=5.
      tpom=xpom+CrwgXd+5.
      ch80='Print to file:'
      if(PrintToFile) then
        call FeQuestLblMake(id,xpom,il,ch80,'L','N')
        tpom=xpom
        nCrwToFile=0
      else
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwXd,CrwYd,1,0)
        nCrwToFile=CrwLastMade
        call FeQuestCrwOpen(nCrwToFile,.false.)
      endif
      xpom=tpom+FeTxLengthUnder(ch80)+5.
      ch80= ' '
      dpom=QuestWidth-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwToFile=EdwLastMade
      if(PrintToFile) call FeQuestStringEdwOpen(nEdwToFile,toFile)
1050  if(CrwLogicQuest(nCrwSelP)) then
        if(EdwStateQuest(nEdwSelP).ne.EdwOpened)
     1    call FeQuestStringEdwOpen(nEdwSelP,' ')
      else
        call FeQuestEdwClose(nEdwSelP)
      endif
      if(nCrwToFile.gt.0) then
        if(CrwLogicQuest(nCrwToFile)) then
          if(EdwStateQuest(nEdwToFile).ne.EdwOpened)
     1      call FeQuestStringEdwOpen(nEdwToFile,toFile)
        else
          call FeQuestEdwClose(nEdwToFile)
        endif
      endif
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1050
      else if(CheckType.eq.EventGlobal.and.
     1        CheckNumber.eq.GlobalClose) then
        call FeQuestRemove(id)
        kam=8000
        go to 9999
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        do i=nCrwAll,nCrwSelP
          if(CrwLogicQuest(i)) then
            nCrwPageSelected=i
            if(i.eq.nCrwSelP) then
              whichPgs=EdwStringQuest(nEdwSelP)
            else
              whichPgs=' '
              if(i.eq.nCrwCur) then
                nintpg=1
                intpg(1,1)=ipg
                intpg(2,1)=ipg
              endif
            endif
            go to 1120
          endif
        enddo
1120    if(nCrwToFile.ne.0) then
          PrintToFile=CrwLogicQuest(nCrwToFile)
          if(PrintToFile) toFile=EdwStringQuest(nEdwToFile)
        endif
        if(nPSPrinters.gt.0) PSPrinters(1)=
     1    PSPrinters(RolMenuSelectedQuest(nRolMenuPrinters))
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(nCrwPageSelected.eq.nCrwSelP) then
        call zhusti(whichPgs)
        i1=1
        i11=0
        i2=0
        i22=0
        idash=0
        ien=idel(whichPgs)
        do i=1,ien
          zn=whichPgs(i:i)
          if(zn.eq.'-') then
            idash=i
            i11=i-1
            i2=i+1
          endif
          if(zn.eq.','.or.i.eq.ien) then
            if(idash.gt.0)then
              i22=i-1
              if(i.eq.ien) i22=i
            else
              i11=i-1
              if(i.eq.ien) i11=i
            endif
            if(i1.gt.0.and.i11.ge.i1) then
              nintpg=nintpg+1
              if(nintpg.gt.100) nintpg=100
              write(ch10,'(''(i'',i5,'')'')') i11-i1+1
              call zhusti(ch10)
              read(whichPgs(i1:i11),ch10,err=1250) intpg(1,nintpg)
              intpg(2,nintpg)=intpg(1,nintpg)
            endif
            if(idash.eq.i) then
              intpg(2,nintpg)=-1
            else
              if(i2.gt.0.and.i22.ge.i2) then
                write(ch10,'(''(i'',i5,'')'')') i22-i2+1
                call zhusti(ch10)
                read(whichPgs(i2:i22),ch10,err=1250) intpg(2,nintpg)
              endif
            endif
            i1=0
            if(i.lt.ien) i1=i+1
            i11=0
            i2=0
            i22=0
            idash=0
          endif
        enddo
        go to 1300
1250    call FeMsgOut(-1.,-1.,'The selection of pages is wrong.')
        go to 9999
      endif
1300  if(PrintToFile) then
        inquire(file=toFile,exist=lpom)
        if(lpom) then
          if(.not.FeYesNo(-1.,-1.,'File '//toFile(1:idel(toFile))
     1       //' already exists - overwrite it?',1)) go to 9999
        endif
      endif
      rewind lnlst
      Strankovani=.false.
      do i=1,50
        read(lnlst,FormA,end=1350) radka
        if(index(Radka,'page=').gt.120) then
          Strankovani=.true.
          exit
        endif
      enddo
1350  rewind lnlst
      lnps=NextLogicNumber()
      if(OpSystem.le.0) then
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//'a2ps'//
     1                ObrLom//'lstprolo.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8000
      else
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//
     1                'a2ps/lstprolo.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8000
      endif
      read(lnps,FormA) radka
      if(radka(1:1).ne.'#') rewind lnps
      tmpout='WorkFile_prnj.tmp'
      lnout=NextLogicNumber()
      call OpenFile(lnout,tmpout,'formatted','unknown')
1400  read(lnps,FormA,end=1450) radka
      write(lnout,FormA) radka(:idel(radka))
      go to 1400
1450  call CloseIfOpened(lnps)
      npage=0
      nsheet=0
      write(lnout,'('' '')')
      write(lnout,'(''/docsave save def'')')
      if(OpSystem.le.0) then
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//'a2ps'//
     1                ObrLom//'lstnewpg.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8100
      else
        call OpenFile(lnps,HomeDir(:idel(HomeDir))//
     1                'a2ps/lstnewpg.ps','formatted','old')
        if(ErrFlag.ne.0) go to 8100
      endif
      write(ch80,'(''Preparing PostScript file, sheet '',i5,
     1  '' ,press ESC to cancel.'')') 0
      call FeTxOut(-1.,-1.,ch80)
      nn=MxLine
1500  rewind lnps
      read(lnps,FormA) radka
      if(radka(1:1).ne.'#') rewind lnps
      nsheet=nsheet+1
      write(ch80,'(''Preparing PostScript file, sheet '',i5,
     1  '' ,press ESC to cancel.'')') nsheet
      call FeTxOutCont(ch80)
      write(lnout,'(''%%Page:'',2i5)') nsheet,nsheet
1600  read(lnps,FormA,end=1700) radka
      write(lnout,FormA) radka(:idel(radka))
      go to 1600
1700  nlocal=0
2000  if(nacteno) then
        radka=ch256
        nacteno=.false.
        nlocal=1
        go to 2500
      endif
      if(mod(nsheet,10).eq.0) then
        call FeEvent(1)
        if(EventType.eq.10.and.EventNumber.eq.14) then
          if(FeYesNo(-1.,-1.,'Do you want cancel the printing?',1)) then
            canceled=.true.
            go to 6000
          endif
        endif
      endif
      read(lnlst,FormA,end=5000) radka
      if(Strankovani) then
        idr=idel(radka)
        if(idr.lt.124) then
          if(nlocal.le.1) then
            go to 2000
          else
            go to 2500
          endif
        endif
        if(index(radka,'page=').le.0) then
          if(nlocal.le.0) then
            go to 2000
          else
            go to 2500
          endif
        endif
      else
        if(nn.lt.MxLine) go to 2500
      endif
      nn=0
      nlocal=nlocal+1
      npage=npage+1
      if(nCrwPageSelected.ne.nCrwAll) then
        konec=.true.
        do i=1,nintpg
          if(npage.le.intpg(2,i).or.intpg(2,i).eq.-1)then
            konec=.false.
            if(npage.ge.intpg(1,i)) then
              pis=.true.
              go to 2200
            endif
          endif
        enddo
        if(konec) go to 5000
        nlocal=nlocal-1
        pis=.false.
      endif
2200  if(nlocal.eq.1.or..not.pis) then
        go to 2500
      else if(nlocal.eq.2) then
        write(lnout,'(''/sd 1 def'')')
        write(lnout,'(''border'')')
        write(lnout,'(''/x0 x 1 get bm add def'')')
        write(lnout,'(''/y0 y 1 get bm bfs add 0 add sub def'')')
        write(lnout,'(''x0 y0 moveto'')')
        write(lnout,'(''bf setfont'')')
        go to 2500
      else
        write(lnout,'(''/sd 0 def'')')
        write(lnout,'(i5,'' sn'')') nsheet
        write(lnout,'(''pagesave restore'')')
        write(lnout,'(''showpage'')')
        nacteno=.true.
        ch256=radka
        go to 1500
      endif
2500  if(pis) write(lnout,'(''( '',a,'') s'')') radka(:idel(radka))
      nn=nn+1
      go to 2000
5000  write(lnout,'(''/sd 0 def'')')
      write(lnout,'(i5,'' sn'')')nsheet
      write(lnout,'(''pagesave restore'')')
      write(lnout,'(''showpage'')')
      write(lnout,'('' '')')
      write(lnout,'(''%%Trailer'')')
      write(lnout,'(''%%Pages: '',i5)')nsheet
      write(lnout,'(''docsave restore end'')')
6000  call FeTxOutEnd
      call CloseIfOpened(lnps)
      call CloseIfOpened(lnout)
      if(canceled) go to 9999
      if(.not.PrintToFile.and.nsheet.gt.15) then
        write(radka,'(''Total: '',i6,
     1    '' sheets of A4 - Do you want to start printing?'')')
     2    nsheet
        call FeDelTwoSpaces(radka)
        if(.not.FeYesNo(-1.,-1.,radka,1)) go to 9999
      endif
      if(PrintToFile) then
        call MoveFile(tmpout,tofile)
      else
        call FePrintFile(PSPrinters(1),tmpout,ich)
        call DeleteFile(tmpout)
      endif
      go to 9900
8000  ch80='The file lstprolog.ps couldn''t be opened'
      go to 9000
8100  ch80='The file lstnewpg.ps couldn''t be opened'
      go to 9000
8300  write(ch80,'(i3)') ich
      ch80='printing error no.:'//ch80(1:3)
9000  call FeChybne(-1.,-1.,ch80,' ',SeriousError)
9900  call CloseIfOpened(lnps)
9999  return
      end
