      subroutine indexc(k,n,ia)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(*)
      ioffvn=ioffv(n-2)
      ioffmn=ioffm(n-2)
      i=ipoc(k+ioffmn)+ioffvn
      ip=ipec(i)
      iden=4**(n-1)
      do i=1,n
        ia(i)=ip/iden
        ip=ip-ia(i)*iden
        iden=iden/4
      enddo
      return
      end
