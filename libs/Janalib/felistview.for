      subroutine FeListView(FileName,Kam)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*128 Veta,FindString,FindList(15)
      character*80  FindLabel(15)
      character*1   Znak
      character*(*) FileName
      integer FeMenuNew,FromSave,SbwItemNMaxQuest,SbwLinesQuest,
     1        SbwItemFromQuest,SbwLnQuest,JenKdyzPredchozi(15),
     2        FindLine(15)
      logical Forwards,CrwLogicQuest,WizardModeOld,PropFontIn
      PropFontIn=PropFont
      WizardModeOld=WizardMode
      WizardMode=.false.
      AskIfQuit=.true.
      FindString=' '
      idFindString=0
      Forwards=.true.
      FromSave=0
      ils=0
      idl=idel(FileName)
      call SetStringArrayTo(FindLabel,15,' ')
      call SetStringArrayTo(FindList ,15,' ')
      call SetIntArrayTo(JenKdyzPredchozi,15,0)
      call SetIntArrayTo(FindLine,15,0)
      FindList(1)='###zacatek###'
      FindLine(1)=1
      FindLabel(1)='TOP       <HOME>'
      if(index(FileName,'.ref').eq.idl-3) then
        FindList(2)='fo/fc list before'
        FindLabel(2)='Fo/Fc list before refinement'
        FindList(3)='statistics'
        JenKdyzPredchozi(3)=1
        FindLabel(3)='Statistics Fo,sin(th)/lambda before refinement'
        FindList(4)='fo/fc list after'
        FindLabel(4)='Fo/Fc list after refinement'
        FindList(4)='fo/fc list after'
        FindLabel(4)='Fo/Fc list after refinement'
        FindList(5)='fo/fc list of worst fitted reflections'
        FindLabel(5)='Fo/Fc list of worst fitted reflections'
        FindList(6)='statistics '
c        JenKdyzPredchozi(5)=1
        FindLabel(6)='Statistics Fo,sin(th)/lambda after refinement'
        FindList(7)='r-factors overview'
        FindLabel(7)='R-factors overview'
        FindList(8)='changes overview'
        FindLabel(8)='Changes overview'
        FindList(9)='ADP eigenvalues and eigenvectors'
        FindLabel(9)='ADP eigenvalues and eigenvectors'
        FindList(10)='List of serious warnings'
        FindLabel(10)='List of serious warnings'
        FindList(11)='blocked due to singularity'
        FindLabel(11)='Singularity report'
        if(MaxMagneticType.gt.0) then
          FindList(12)='Interpretation of magnetic parameters'
          FindLabel(12)='Interpretation of magnetic parameters'
        endif
        if(ChargeDensities.and.lasmaxm.gt.0) then
          FindList(12)='d polulations from multipoles'
          FindLabel(12)='d polulations from multipoles'
        endif
      else if(index(FileName,'.fou').eq.idl-3) then
        FindList(2)='searching for positive peaks'
        FindLabel(2)='Positive peaks'
        FindList(3)='searching for negative peaks'
        FindLabel(3)='Negative peaks'
      else if(index(FileName,'.dis').eq.idl-3) then
        FindList(2)='no  atom'
        FindLabel(2)='List of input atoms'
        FindList(3)='List of distances'
        FindLabel(3)='List of distances'
        FindList(4)='List of bond valence sums'
        FindLabel(4)='List of bond valence sums'
        FindList(5)='Hirshfeld test'
        FindLabel(5)='Hirshfeld test'
        FindList(6)='List of possible hydrogen bonds'
        FindLabel(6)='List of possible hydrogen bonds'
        FindList(7)='List of torsion angles'
        FindLabel(7)='List of torsion angles'
        FindList(8)='List of best planes'
        FindLabel(8)='List of best planes'
        FindList(9)='List dihedral angles'
        FindLabel(9)='List dihedral angles'
      else if(index(FileName,'.rre').eq.idl-3) then
        FindList(2)='Report from the import procedure'
        FindLabel(2)='Report from the import procedure'
        FindList(3)='Summary of collected reflections'
        FindLabel(3)='Summary of rejected reflections'
        FindList(4)='Summary of reflections absent'
        FindLabel(4)='Summary of systematic extinctions'
        FindList(5)='Summary from transformation to the common scale'
        FindLabel(5)='Transformation to the common scale'
        FindList(6)='Report from averaging reflections'
        FindLabel(6)='Report from averaging'
        FindList(7)='Summary after averaging'
        FindLabel(7)='Summary after averaging'
        FindList(8)='Coverage statistics'
        FindLabel(8)='Coverage statistics'
      else if(index(FileName,'.cp').eq.idl-2) then

      else if(index(FileName,'.inb').eq.idl-3) then

      endif
      nList=0
      do i=1,12
        if(FindLabel(i).ne.' ') then
          nList=i
          call Mala(FindList(i))
        endif
      enddo
      nList=min(nList+1,15)
      FindList(nList)='###konec###'
      FindLabel(nList)='BOTTOM     <END>'
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      il=0
      ilz=0
      ilp=0
      xd=max(FeTxLengthUnder(FindLabel(1)),
     1       FeTxLengthUnder(FindLabel(NList)))+2.*EdwMarginSize
      do 1500i=2,NList-1
        xd=max(xd,FeTxLengthUnder(FindLabel(i))+2.*EdwMarginSize)
        if(JenKdyzPredchozi(i).gt.0.and.FindLine(i-1).le.0) then
          FindLine(i)=0
          go to 1500
        else
1400      read(ln,FormA,end=1450,err=1450) Veta
          il=il+1
          call Mala(Veta)
          if(index(Veta,'page=').gt.0) then
            ilp=il
            go to 1400
          endif
          if(index(Veta,FindList(i)(:idel(FindList(i)))).le.0)
     1      go to 1400
          ilz=il
          FindLine(i)=ilp
          go to 1500
1450      rewind ln
          do j=1,ilz
            read(ln,FormA,end=1500,err=1500) Veta
            if(index(Veta,'page=').gt.0) ilp=j
          enddo
          il=ilz
        endif
1500  continue
      call CloseIfOpened(ln)
      call FeSetPropFont
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,
     1                      LightGray,-1,-1)
      CheckKeyboard=.true.
      xpom=XMaxBasWin-(SbwPruhXd+4.)/EnlargeFactor
      ypom=YMaxBasWin-30.
      call FeQuestAbsSbwMake(id,0.,YMinBasWin,xpom,ypom,1,
     1                       CutTextNone,SbwVertical)
      nSbwText=SbwLastMade
      call FeQuestSbwTextOpen(nSbwText,MxLine,FileName)
      FindLine(NList)=SbwItemNMaxQuest(nSbwText)
      yln=SbwYStepQuest(nSbwText)
      yln2=yln*.5
      ln=SbwLnQuest(nSbwText)
      xpom=15.
      ypom=SbwYMaxQuest(nSbwText)+8.
      dpom=35.
      do i=1,7
        if(i.eq.1) then
          Veta='%Find'
        else if(i.eq.2) then
          Veta='Find %next'
        else if(i.eq.3) then
          Veta='%Go to'
        else if(i.eq.4) then
          Veta='%Print'
        else if(i.eq.5) then
          Veta='Pg%Top'
        else if(i.eq.6) then
          Veta='Open in %editor'
        else if(i.eq.7) then
          Veta='%Close'
        endif
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtFind=ButtonLastMade
        else if(i.eq.2) then
          nButtFindNext=ButtonLastMade
        else if(i.eq.3) then
          nButtGoto=ButtonLastMade
          xgoto=xpom
        else if(i.eq.4) then
          nButtPrint=ButtonLastMade
        else if(i.eq.5) then
          nButtPgTop=ButtonLastMade
        else if(i.eq.6) then
          nButtOpenEdit=ButtonLastMade
        else if(i.eq.7) then
          nButtClose=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+15.
      enddo
      ygoto=ypom-13.-float(nList)*MenuLineWidth
      npg=(SbwItemNMaxQuest(nSbwText)-1)/SbwLinesQuest(nSbwText)+1
      if(kam.lt.0) then
        ilp=FindLine(NList)
      else if(kam.gt.0) then
        ilp=kam
      else
        ilp=0
      endif
      if(ilp.gt.0) call FeQuestSbwShow(nSbwText,ilp)
2000  BlockedActiveObj=.false.
      ActiveObj=ActSbw*10000+nSbwText+SbwFr-1
      call FeQuestActiveObjOn
      SbwActive=nSbwText+SbwFr-1
      call FeQuestEvent(id,ich)
2050  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtFind.or.CheckNumber.eq.nButtFindNext)
     1    then
          if(FromSave.ne.SbwItemFromQuest(nSbwText))
     1      ils=SbwItemFromQuest(nSbwText)
          if(CheckNumber.eq.nButtFindNext) go to 2150
          idp=NextQuestId()
          il=3
          xqd=300.
          call FeQuestCreate(idp,-1.,-1.,xqd,il,'Find',0,LightGray,0,0)
          il=1
          Veta='%Search for:'
          xpom=5.
          dpom=xqd-10.
          call FeQuestEdwMake(idp,xpom,il,xpom,il+1,Veta,'L',dpom,EdwYd,
     1                        0)
          nEdwFind=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,FindString)
          il=il+2
          xpom=5.
          tpom=CrwgXd+10.
          do i=1,2
            if(i.eq.1) then
              Veta='%Forwards'
            else
              Veta='%Backwards'
            endif
            call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,1)
            if(i.eq.1) nCrwForwards=CrwLastMade
            call FeQuestCrwOpen(CrwLastMade,Forwards.eqv.i.eq.1)
            xpom=xpom+xqd*.5
            tpom=tpom+xqd*.5
          enddo
2100      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2100
          endif
          if(ich.eq.0) then
            Forwards=CrwLogicQuest(nCrwForwards)
            FindString=EdwStringQuest(nEdwFind)
            call Mala(FindString)
          endif
          call FeQuestRemove(idp)
          if(ich.ne.0) go to 2000
          idFindString=idel(FindString)
2150      if(idFindString.le.0) go to 2000
          rewind(ln)
          il=0
          ilsn=0
2200      read(ln,FormA,end=2300,err=2300) Veta
          il=il+1
          if(Forwards.and.il.le.ils) go to 2200
          call Mala(Veta)
          KurzX=index(Veta,FindString(:idFindString))
          if(KurzX.le.0) go to 2200
          if(.not.Forwards) then
            if(il.lt.ils) then
              ilsn=il
              KurzXs=KurzX
              go to 2200
            else
              if(ilsn.gt.0) then
                il=ilsn
                KurzX=KurzXs
              else
                call FeChybne(-1.,-1.,'the top has been reached.',' ',
     1                        Warning)
                go to 2000
              endif
            endif
          endif
2250      call FeQuestSbwShow(nSbwText,il)
          FromSave=SbwItemFromQuest(nSbwText)
          ils=il
          KurzY=il-FromSave+1
          call FeSetFixFont
          XKurz=SbwXMinQuest(nSbwText)+FeTxLengthSpace(Veta(:KurzX-1))
          YKurz=SbwYMaxQuest(nSbwText)-yln*float(KurzY-1)-yln2
          if(OpSystem.le.0) call FePlotMode('E')
          call FeFillRectangle(xKurz,
     1      xKurz+FeTxLengthSpace(FindString(:idFindString)),
     2      yKurz-yln2,yKurz+yln2,4,0,0,KurzorColor)
          call FeSetPropFont
          if(OpSystem.le.0) call FePlotMode('N')
          go to 2000
2300      if(.not.Forwards) then
            il=ilsn
            KurzX=KurzXs
            go to 2250
          endif
          call FeChybne(-1.,-1.,'the bottom has been reached.',' ',
     1                  Warning)
          go to 2000
        else if(CheckNumber.eq.nButtGoto) then
          i=FeMenuNew(xgoto,ygoto,xd,FindLabel,FindLine,1,nList,1,0)
          if(i.lt.1.or.i.gt.nList) go to 2000
          ilp=FindLine(i)
          if(ilp.le.0) go to 2000
2450      call FeSetFixFont
          call FeQuestSbwShow(nSbwText,ilp)
          call FeSetPropFont
          go to 2000
        else if(CheckNumber.eq.nButtPrint) then
          ipg=(SbwItemFromQuest(nSbwText)-1)/SbwLinesQuest(nSbwText)+1
          call FeListPrint(FileName,ln,ipg,npg,kam)
          if(kam.eq.8000) go to 8000
        else if(CheckNumber.eq.nButtPgTop) then
          if(SbwItemFromQuest(nSbwText).ne.1) then
            il=((SbwItemFromQuest(nSbwText)-2)/
     1           SbwLinesQuest(nSbwText)+1)*SbwLinesQuest(nSbwText)+1
            call FeSetFixFont
            call FeQuestSbwShow(nSbwText,il)
            call FeSetPropFont
          endif
        else if(CheckNumber.eq.nButtOpenEdit) then
          call FeEdit(FileName)
        else if(CheckNumber.eq.nButtClose) then
          go to 8000
        endif
        go to 2000
      else if(CheckType.eq.EventCTRL) then
        Znak=char(CheckNumber)
        call mala (Znak)
        if(Znak.eq.'f') then
          CheckType=EventButton
          CheckNumber=nButtFind
          go to 2050
        else if(Znak.eq.'n') then
          CheckType=EventButton
          CheckNumber=nButtFindNext
          go to 2050
        else if(Znak.eq.'p') then
          CheckType=EventButton
          CheckNumber=nButtPrint
          go to 2050
        endif
        go to 2000
      else if(CheckType.eq.EventASCII) then
        go to 2000
      else if(CheckType.eq.EventAlt) then
        go to 2000
      else if(CheckType.eq.EventKey) then
        go to 2000
      else if((CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape).or.
     1    (CheckType.eq.EventGlobal.and.CheckNumber.eq.GlobalClose))
     2  then
        go to 8000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
8000  call FeQuestRemove(id)
      CheckKeyboard=.false.
      WizardMode=WizardModeOld
9999  AskIfQuit=.false.
      if(.not.PropFontIn) call FeSetFixFont
      return
      end
