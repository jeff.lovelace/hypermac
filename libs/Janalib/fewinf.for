      subroutine FeWInf
      include 'fepc.cmn'
      parameter (mxwinf=10)
      character*(*) Text
      character*80 WInfTmpFile(mxwinf)
      dimension    WInfXMin(mxwinf),WInfXMax(mxwinf),WInfYMin(mxwinf),
     1             WInfYMax(mxwinf)
      integer      WInfState(mxwinf),WInfRemoved,WInfClosed,WInfOpened
      equivalence (IdNumbers(0),WInfRemoved),
     1            (IdNumbers(1),WInfClosed),
     2            (IdNumbers(2),WInfOpened)
      save WInfXMin,WInfXMax,WInfYMin,WInfYMax,WInfState,WInfTmpFile
      data WInfState/mxwinf*0/
      entry FeWInfMake(id,idc,xm,ym,xd,yd)
      WInfXMin(id)=xm
      WInfXMax(id)=xm+xd
      WInfYMin(id)=ym
      WInfYMax(id)=ym+yd
      if(idc.gt.0) then
        WInfXMin(id)=WInfXMin(id)+QuestXMin(idc)
        WInfXMax(id)=WInfXMax(id)+QuestXMin(idc)
        WInfYMin(id)=WInfYMin(id)+QuestYMin(idc)
        WInfYMax(id)=WInfYMax(id)+QuestYMin(idc)
      endif
      WInfState(id)=WInfOpened
      WInfTmpFile(id)='jnap'
      if(OpSystem.le.0) then
        call CreateTmpFile(WInfTmpFile(id),i,1)
      else
        call CreateTmpFileName(WInfTmpFile(id),id)
      endif
      call FeSaveImage(WInfXMin(id),WInfXMax(id),
     1                 WInfYMin(id),WInfYMax(id),WInfTmpFile(id))
      go to 9999
      entry FeWInfWrite(id,Text)
      if(WInfState(id).eq.WInfRemoved) go to 9999
      call FeDeferOutput
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),4,0,0,LightYellow)
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),0,0,0,Black)
      call FeOutSt(0,WInfXMin(id)+3.,(WInfYMin(id)+WInfYMax(id))*.5,
     1             Text,'L',Black)
      call FeReleaseOutput
      WInfState(id)=WInfOpened
      go to 9999
      entry FeWInfClose(id)
      if(WInfState(id).ne.WInfOpened) go to 9999
      call FeFillRectangle(WInfXMin(id),WInfXMax(id),WInfYMin(id),
     1                     WInfYMax(id),4,0,0,LightGray)
      WInfState(id)=WInfClosed
      go to 9999
      entry FeWInfRemove(id)
      if(WInfState(id).eq.WInfRemoved) go to 9999
      call FeLoadImage(WInfXMin(id),WInfXMax(id),
     1                 WInfYMin(id),WInfYMax(id),WInfTmpFile(id),0)
      WInfState(id)=WInfRemoved
9999  return
      end
