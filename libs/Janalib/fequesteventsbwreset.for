      subroutine FeQuestEventSbwReset(id)
      include 'fepc.cmn'
      integer ButtonStateQuest
      if(SbwType(id).eq.SbwSelectType) then
        if(RepeatMode) then
          j1=0
          j2=0
          if(SbwItemSelN(id).gt.1) then
            if(ButtonStateQuest(nButtRepeatClone).eq.ButtonOff) then
              j1=ButtonOff
              j2=ButtonDisabled
            endif
          else
            if(ButtonStateQuest(nButtRepeatClone).eq.ButtonOff) then
              j1=ButtonDisabled
              j2=ButtonOff
            endif
          endif
          if(j2.gt.0) then
            call FeQuestButtonOpen(nButtRepeatRead,j2)
            call FeQuestButtonOpen(nButtRepeatWrite,j2)
          endif
        endif
      endif
      return
      end
