      subroutine SetBasicConstants
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      CumulAt(1)=85
      CumulAt(2)=CumulAt(1)+68
      do i=3,9
        j=i-1
        CumulAt(i)=CumulAt(j)+2*TRank(j-2)*mxw
        if(i.eq.3) CumulAt(i)=CumulAt(i)+1
      enddo
      CumulAt(10)=CumulAt(9)+1
      CumulAt(11)=CumulAt(10)+(2*mxw+1)*3
      CumulMol(1)=28
      CumulMol(2)=CumulMol(1)+1+2*mxw
      CumulMol(3)=CumulMol(2)+ 6*mxw
      CumulMol(4)=CumulMol(3)+ 6*mxw
      CumulMol(5)=CumulMol(4)+12*mxw
      CumulMol(6)=CumulMol(5)+12*mxw
      CumulMol(7)=CumulMol(6)+18*mxw
      CumulMol(8)=CumulMol(7)+1
      HistoryFile=RootDir(:idel(RootDir))//'jana2006.hst'
      sqrt8ln2=sqrt(8.*log(2.))
      rsqrt2=1./sqrt(2.)
      LorentzToStrain=ToRad
      GaussToStrain=sqrt8ln2*LorentzToStrain
      IShiftPwd=1
      IBackgPwd=IShiftPwd+8
      IRoughPwd=IBackgPwd+MxBackg
      ILamPwd=IRoughPwd+2
      IAsymPwd=ILamPwd+2
      ITOFAbsPwd=IAsymPwd+12
      ICellPwd=IShiftPwd+NParRecPwd*MxDatBlock
      IQuPwd=ICellPwd+6
      IGaussPwd=IQuPwd+9
      ILorentzPwd=IGaussPwd+4
      IZetaPwd=ILorentzPwd+5
      IStPwd=IZetaPwd+1
      IPrefPwd=IStPwd+35
      IBroadHKLPwd=IPrefPwd+10
      ColorSat=Green
      do i=1,MxDatBlock
        write(DatBlockName(i),'(''Block'',i2)') i
        call Zhusti(DatBlockName(i))
      enddo
      do i=1,MxRefBlock
        write(RefBlockName(i),'(''Block'',i2)') i
        call Zhusti(RefBlockName(i))
      enddo
      call SetIntArrayTo(SimColorMain,3,Yellow)
      SimColorSat  =Cyan
      SimColorPlus =Yellow
      SimColorMinus=Cyan
      SimColorUnobs=White
      SimColorTwin(1)=Yellow
      SimColorTwin(2)=Green
      SimColorTwin(3)=Red
      SimColorTwin(4)=Cyan
      SimColorTwin(5)=Magenta
      SimColorTwin(6)=Blue
      SimColorTwin(7:18)=White
      if(.not.Console) call CreateCIFBasicFiles
      do i=1,50
        TextInfo(i)=' '
        TextInfoFlags(i)='LN'
        UseTabsInfo(i)=0
      enddo
      call SetRealArrayTo(RMatCalc,216,0.)
      UseVestaForMagDraw=.true.
      YBottomMessage=YLenBasWin*.25
      maxNDim=3
      return
      end
