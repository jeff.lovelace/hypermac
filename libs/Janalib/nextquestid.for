      function NextQuestId()
      include 'fepc.cmn'
      if(WizardMode) then
        call FeQuestReset(WizardId)
        xm=QuestXMin(WizardId)
        xdp=QuestXLen(WizardId)
        ym=QuestYMin(WizardId)
        yd=QuestYLen(WizardId)
        if(KartOn) then
          call FeFillRectangle(xm,xm+xdp,ym,ym+yd,4,0,0,LightGray)
        else if(QuestColorFill(WizardId).gt.0) then
          call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                         QuestColorFill(WizardId))
        endif
        if(ButtonOk.gt.0) then
          if(ButtonState(ButtonOk).ne.ButtonClosed) then
            ButtonState(ButtonOk)=ButtonOn
            call FeButtonOff(ButtonOk)
          endif
        endif
        if(ButtonEsc.gt.0) then
          if(ButtonState(ButtonEsc).ne.ButtonClosed) then
            ButtonState(ButtonEsc)=ButtonOn
            call FeButtonOff(ButtonEsc)
          endif
        endif
        if(ButtonCancel.gt.0) then
          if(ButtonState(ButtonCancel).ne.ButtonClosed) then
            ButtonState(ButtonCancel)=ButtonOn
            call FeButtonOff(ButtonCancel)
          endif
        endif
        NextQuestId=WizardId
        QuestCheck(WizardId)=0
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)
        EventTypeSave=0
        EventNumberSave=0
      else
        do i=1,mxquest
          if(QuestState(i).eq.0) then
            NextQuestId=i
            go to 2000
          endif
        enddo
        NextQuestId=0
      endif
2000  return
      end
