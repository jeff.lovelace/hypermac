      subroutine FeX2Xf(x,xf)
      include 'fepc.cmn'
      dimension xf(3),x(3),xo(3)
      xo(1)=FeX2Xo(x(1))
      xo(2)=FeY2Yo(x(2))
      xo(3)=x(3)
      call multm(O2F,xo,xf,3,3,1)
      return
      end
