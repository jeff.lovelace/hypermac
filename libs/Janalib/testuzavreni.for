      subroutine TestUzavreni
      include 'fepc.cmn'
      character*80 Veta
      do i=1,mxquest
        if(QuestState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Quest '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          QuestState(i)=0
        endif
      enddo
      do i=1,mxbut
        if(ButtonState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Button '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          ButtonState(i)=0
        endif
      enddo
      do i=1,mxcrw
        if(CrwState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Crw '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          CrwState(i)=0
        endif
      enddo
      do i=1,mxedw
        if(EdwState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Edw '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          EdwState(i)=0
        endif
      enddo
      do i=1,mxIcon
        if(IconState(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''Icon '',i2,'' zustal otevren.'')') i
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          IconState(i)=0
        endif
      enddo
      do i=0,MxTabs
        if(NTabs(i).ne.0) then
          if(VasekTest.ne.0) then
            write(Veta,'(''TabSystem '',i2,'' zustal otevren:'',i4)')
     1        i,NTabs(i)
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          endif
          call FeTabsReset(i)
        endif
      enddo
      UseTabs=0
      return
      end
