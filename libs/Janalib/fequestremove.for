      subroutine FeQuestRemove(id)
      include 'fepc.cmn'
      Klic=0
      go to 1000
      entry FeQuestReset(id)
      Klic=1
1000  if(QuestState(id).le.0) go to 9999
      call FeQuestActiveObjOff
      call FeQuestTitleRemove(id)
      do i=1,EdwTo-EdwFr+1
        call FeQuestEdwRemove(i)
      enddo
      do i=1,CrwTo-CrwFr+1
        call FeQuestCrwRemove(i)
      enddo
      if(Klic.eq.1) then
        if(WizardMode) then
          IFrom=4
        else
          IFrom=3
        endif
      else
        IFrom=1
      endif
      do i=IFrom,ButtonTo-ButtonFr+1
        call FeQuestButtonRemove(i)
      enddo
      do i=1,IFrom-1
        call FeQuestButtonOff(i)
      enddo
      do i=1,KartNDol
        j=KartDolButt(i)
        call FeButtonRemove(j)
      enddo
      do i=1,SbwTo-SbwFr+1
        call FeQuestSbwRemove(i)
      enddo
      SbwActive=0
      do i=1,LblTo-LblFr+1
        call FeQuestLblRemove(i)
      enddo
      do i=1,LinkaTo-LinkaFr+1
        call FeQuestLinkaRemove(i)
      enddo
      do i=1,SvisliceTo-SvisliceFr+1
        call FeQuestSvisliceRemove(i)
      enddo
      do i=1,RolMenuTo-RolMenuFr+1
        call FeQuestRolMenuRemove(i)
      enddo
      if(Klic.ne.0) then
        EdwTo=EdwFr-1
        CrwTo=CrwFr-1
        ButtonTo=ButtonFr+IFrom-2
        ButtonMax=ButtonTo
        KartNDol=0
        SbwTo=SbwFr-1
        LblTo=LblFr-1
        LinkaTo=LinkaFr-1
        SvisliceTo=SvisliceFr-1
        RolMenuTo=RolMenuFr-1
        RolMenuMax=RolMenuTo
        ActiveObjTo=ActiveObjFr-1
        go to 9999
      endif
      ipom=0
      if(ButtonEsc.gt.0) call FeButtonRemove(ButtonEsc)
      if(ButtonOK .gt.0) call FeButtonRemove(ButtonOk)
      if(KartOn) then
        fw=0.
        QXMn=KartXMin
        QXMx=KartXMax
        QYMn=KartYMin
        QYMx=KartYMax
      else
        QXMn=QuestXMin(id)
        QXMx=QuestXMax(id)
        QYMn=QuestYMin(id)
        QYMx=QuestYMax(id)
        fw=FrameWidth
      endif
      call FeLoadImage(QXMn-fw,QXMx+fw,QYMn-fw,QYMx+fw,QuestTmpFile(id),
     1                 0)
      if(QuestKartIdPrev(id).gt.0) then
        KartId=QuestKartIdPrev(id)
        call FeKartRefresh(1)
      endif
      call FeFrToRefresh(QuestCalledFrom(id))
      call FeQuestActiveObjOn
      if(id.eq.LastQuest) then
        LastQuest=QuestCalledFrom(id)
        LastActiveQuest=LastQuest
      endif
      QuestState(id)=0
      QuestCheck(id)=0
      QuestCalledFrom(id)=0
      if(WizardMode.and.id.eq.WizardId) then
        WizardMode=.false.
        WizardId=0
      endif
      WaitTime=0
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      call FeDeferOutput
9999  return
      end
