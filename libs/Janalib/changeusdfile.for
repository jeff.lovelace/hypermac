      subroutine ChangeUSDFile(StrName,Lbl1,Lbl2)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StrName,Lbl1,Lbl2
      character*256  t256
      character*10  Slovo,Lbl1A(2),Lbl2A(2)
      character*1   String(256)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,CR
      logical ExistFile,EqIgCase,StructureExists
      equivalence (t256,String)
      data CR,LF/13,10/
      data Lbl1A/'opened','closed'/,
     1     Lbl2A/'unlocked','locked'/
      if(StrName.eq.' '.or..not.StructureExists(StrName)) go to 5000
      WorkString=StrName(:idel(StrName))//'.usd'
      if(ExistFile(WorkString)) then
        ln=FeOpenBinaryFile(WorkString(:idel(WorkString)))
        if(ln.le.0) go to 5000
        i=FeReadBinaryFile(ln,String,256)
        i=FeCloseBinaryFile(ln)
        do i=1,256
          j=ichar(t256(i:i))
          if(j.eq.CR.or.j.eq.LF) then
            t256(i:)=' '
            go to 1500
          endif
        enddo
        go to 5000
1500    k=0
        i1=0
        i2=0
2000    call kus(t256,k,Slovo)
        if(EqIgCase(Slovo,Lbl1A(1))) then
          i1=i1+1
        else if(EqIgCase(Slovo,Lbl1A(2))) then
          i1=i1+2
        else if(EqIgCase(Slovo,Lbl2A(1))) then
          i2=i2+1
        else if(EqIgCase(Slovo,Lbl2A(2))) then
          i2=i2+2
        endif
        if(k.lt.len(t256)) go to 2000
      else
        i1=1
        i2=1
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,WorkString,'formatted','unknown')
      if(EqIgCase(Lbl1,Lbl1A(1))) then
        i1=1
      else if(EqIgCase(Lbl1,Lbl1A(2))) then
        i1=2
      endif
      if(EqIgCase(Lbl2,Lbl2A(1))) then
        i2=1
      else if(EqIgCase(Lbl2,Lbl2A(2))) then
        i2=2
      endif
      write(t256,'(a10,1x,a10)') Lbl1A(i1),Lbl2A(i2)
      call ZdrcniCisla(t256,2)
      write(ln,FormA1)(t256(i:i),i=1,idel(t256))
      call CloseIfOpened(ln)
5000  return
      end
