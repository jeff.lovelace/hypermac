      subroutine FeMakeAxisLabels(kterou,xmin,xmax,ymin,ymax,text)
      include 'fepc.cmn'
      dimension xpole(4),ypole(4),xp(3),xo(3)
      character*(*) text
      character*10 t10
      character*8 format
      data xp(3)/0./
      ix=kterou
      iy=3-kterou
      i=1
      call FeSetLabelStep(xmin,xmax,div,irad)
      if(div.le.0.) go to 9999
      if(irad.ge.0) then
        format='(i10   )'
      else
        write(format,'(''(f10.'',i2,'')'')') iabs(irad)
      endif
      xs=float(ifix(xmin/div))*div
      if(xs.lt.xmin) xs=xs+div
      xp(iy)=ymin
1210  xp(ix)=xs
      call multm(F2O,xp,xo,3,3,1)
      xpole(1)=FeXo2X(xo(1))
      ypole(1)=FeYo2Y(xo(2))
      if(kterou.eq.1) then
        if(F2O(5).lt.0.) then
          Znak=1.
        else
          Znak=-1.
        endif
        xpole(2)=xpole(1)
        ypole(2)=ypole(1)+Znak*5.
      else
        if(F2O(1).lt.0.) then
          Znak=1.
        else
          Znak=-1.
        endif
        xpole(2)=xpole(1)+Znak*5.
        ypole(2)=ypole(1)
      endif
      call FePolyLine(2,xpole,ypole,White)
      if(mod(i,2).eq.1) then
        if(irad.ge.0) then
          write(t10,format) nint(xs)
        else
          write(t10,format) xs
        endif
        call zhusti(t10)
        if(kterou.eq.1) then
          call FeOutSt(0,xpole(2),ypole(2)+Znak*15.,t10,'C',White)
        else
          call FeOutSt(0,xpole(2)+Znak*10.,ypole(2),t10,'R',White)
        endif
      endif
      xs=xs+div
      i=i+1
      if(xs.le.xmax+.001) go to 1210
      if(mod(i,2).eq.0) xs=xs-div
      xs=xs-div
      xp(kterou)=xs
      call multm(F2O,xp,xo,3,3,1)
      xp(1)=FeXo2X(xo(1))
      xp(2)=FeYo2Y(xo(2))
      if(kterou.eq.1) then
        call FeOutSt(0,xp(1),xp(2)+Znak*15.,text,'C',White)
      else
        call FeOutSt(0,xp(1)+Znak*10.,xp(2),text,'R',White)
      endif
9999  return
      end
