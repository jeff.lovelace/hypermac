      subroutine FeReadRealMat(xmi,ymi,Label,SmbV,ChangeStyle,SmbVN,
     1                         RMat,RMatI,n,CheckSing,CheckPosDef,ich)
      include 'fepc.cmn'
      character*(*) SmbV(n),SmbVN(n),Label
      character*80 t80
      integer ChangeStyle
      logical CheckSing,CheckPosDef
      dimension RMat(n,n),RMatI(n,n),RMatTr(:,:),tpoma(:),xpoma(:)
      allocatable RMatTr,tpoma,xpoma
      allocate(RMatTr(n,n),tpoma(n),xpoma(n))
      ich=0
      id=NextQuestId()
      xqd=150.
      dpom=50.
      xqd=10.+dpom*float(n)+(FeTxLength(SmbV(1)//'XX')+12.)*
     1                      float(n+1)
      xqd=max(FeTxLength(Label)+20.,xqd)
      il=n+2
      call FeQuestCreate(id,xmi,ymi,xqd,il,Label,1,LightGray,0,0)
      il=0
      do i=1,n
        il=il+1
        tpom=5.
        t80=SmbV(i)
        if(ChangeStyle.eq.IdChangeCase) then
          call ChangeCase(t80)
        else if(ChangeStyle.eq.IdAddPrime) then
          t80=t80(:idel(t80))//''''
        else if(ChangeStyle.eq.IdUseNew) then
          t80=SmbVN(i)
        endif
        t80=t80(:idel(t80))//'='
        call FeQuestLblMake(id,tpom,il,t80,'L','N')
        do j=1,n
          if(i.eq.1) then
            if(j.eq.1) then
              xpoma(j)=tpom+FeTxLength(t80)+6.
            else
              xpoma(j)=tpoma(j-1)+FeTxLength(t80)+6.
            endif
          endif
          t80='*'//SmbV(j)(:idel(SmbV(j)))
          if(j.ne.n) t80=t80(:idel(t80))//'+'
          if(i.eq.1) tpoma(j)=xpoma(j)+dpom+6.
          call FeQuestEdwMake(id,tpoma(j),il,xpoma(j),il,t80,'L',dpom,
     1                        EdwYd,0)
          if(i.eq.1.and.j.eq.1) nEdwFirst=1
          call FeQuestRealEdwOpen(EdwLastMade,RMat(j,i),.false.,.false.)
        enddo
      enddo
      il=il+1
      t80='Set %unit matrix'
      tpom=FeTxLength(t80)+20.
      xpom=xqd*.5-tpom-10.
      call FeQuestButtonMake(id,xpom,-10*il-2,tpom,ButYd,t80)
      nButtUnit=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      t80='Set to %zeros'
      xpom=xpom+20.+tpom
      call FeQuestButtonMake(id,xpom,-10*il-2,tpom,ButYd,t80)
      nButtZeros=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      t80='Matrix %calculator'
      dpom=FeTxLengthUnder(t80)+10.
      tpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,tpom,-10*il-2,dpom,ButYd,t80)
      nButtMatCalc=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        nEdw=nEdwFirst
        Suma=0.
        do i=1,n
          do j=1,n
            call FeQuestRealFromEdw(nEdw,RMat(j,i))
            Suma=max(Suma,abs(RMat(j,i)))
            nEdw=nEdw+1
          enddo
        enddo
        if(Suma.le.0.) then
          Suma=.0001
          pom=0.
          go to 2150
        else
          Suma=Suma*.0001
        endif
        call matinv(RMat,RMati,pom,n)
2150    if(CheckSing.or.CheckPosDef) then
          if(abs(pom).lt.Suma.and.CheckSing) then
            call FeChybne(-1.,YBottomMessage,
     1                    'The matrix is singular - try again',
     2                    ' ',SeriousError)
            go to 1500
          else if(pom.lt.0..and.CheckPosDef) then
            call FeChybne(-1.,YBottomMessage,
     1                    'The matrix has negative determinant '//
     2                    '- try again',' ',SeriousError)
            go to 1500
          endif
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtUnit) then
          call UnitMat(RMat,n)
        else if(CheckNumber.eq.nButtZeros) then
          call SetRealArrayTo(RMat,n**2,0.)
        else
          NRow=n
          NCol=n
          nEdw=nEdwFirst
          do i=1,n
            do j=1,n
              call FeQuestRealFromEdw(nEdw,RMat(j,i))
              nEdw=nEdw+1
            enddo
          enddo
          call TrMat(RMat,RMatTr,n,n)
          call MatrixCalculatorInOut(RMatTr,NRow,NCol,ichp)
          if(ichp.eq.0) then
            call TrMat(RMatTr,RMat,n,n)
          else
            go to 1500
          endif
        endif
        nEdw=nEdwFirst
        do i=1,n
          do j=1,n
            call FeQuestRealEdwOpen(nEdw,RMat(j,i),.false.,.false.)
            nEdw=nEdw+1
          enddo
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      deallocate(RMatTr,tpoma,xpoma)
      return
      end
