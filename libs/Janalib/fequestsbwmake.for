      subroutine FeQuestSbwMake(id,xw,nn,xdw,nd,nstrip,CutText,BarType)
      include 'fepc.cmn'
      character*(*) FileName
      integer CutText,BarType
      logical lpom
      yw=QuestYPosition(id,nn)-QuestLineWidth*.5+8.
      pom=QuestLineWidth*float(nd)-6.
      i=max(ifix(pom/MenuLineWidth),1)
      ydw=float(i)*MenuLineWidth
      yw=yw+(pom-ydw)*.5
      go to 1000
      entry FeQuestAbsSbwMake(id,xw,ywi,xdw,ydwi,nstrip,CutText,BarType)
      yw=ywi
      ydw=ydwi
1000  SbwTo=SbwTo+1
      SbwLastMade=SbwTo-SbwFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActSbw+SbwTo
      QuestSbwTo(id)=SbwTo
      SbwCutText(SbwTo)=CutText
      SbwBars(SbwTo)=BarType
      SbwStripes(SbwTo)=max(nstrip,1)
      call SetIntArrayTo(SbwItemSel(1,SbwTo),1000,0)
      call FeSbwMake(SbwTo,id,xw,yw,xdw,ydw,nstrip)
      SbwDoubleClickAllowed(SbwTo)=.false.
      SbwRightClickAllowed=.false.
      go to 9999
      entry FeQuestSbwSetDoubleClick(iwa,lpom)
      i=iwa+SbwFr-1
      SbwDoubleClickAllowed(i)=lpom
      go to 9999
      entry FeQuestSbwMenuOpen(iwa,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwMenuOpen(i)
      go to 9999
      entry FeQuestSbwSelectOpen(iwa,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwSelectOpen(i)
      go to 9999
      entry FeQuestSbwTextOpen(iwa,ly,FileName)
      i=iwa+SbwFr-1
      SbwFile(i)=FileName
      call FeSbwTextOpen(i,ly)
      go to 9999
      entry FeQuestSbwPureOpen(iwa,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      i=iwa+SbwFr-1
      SbwFile(i)=' '
      call FeSbwPureOpen(i,NXFr,NXTo,NXAll,NYFr,NYTo,NYAll)
      go to 9999
      entry FeQuestSbwClose(iwa)
      if(iwa.le.0) go to 9999
      i=iwa+SbwFr-1
      call FeSbwClose(i)
      go to 9999
      entry FeQuestSbwShow(iwa,nn)
      i=iwa+SbwFr-1
      call FeSbwShow(i,nn)
      go to 9999
      entry FeQuestSbwItemOn(iwa,nn)
      i=iwa+SbwFr-1
      j=min(nn,SbwItemNMax(i))-SbwItemFrom(i)+1
      if(j.ge.1) call FeSbwItemOn(i,j)
      go to 9999
      entry FeQuestSbwItemOff(iwa,nn)
      i=iwa+SbwFr-1
      j=min(nn,SbwItemNMax(i))-SbwItemFrom(i)+1
      if(j.ge.1.and.nn.le.SbwItemNMax(i)) call FeSbwItemOff(i,j)
      go to 9999
      entry FeQuestSbwRemove(iwa)
      i=iwa+SbwFr-1
      call CloseIfOpened(SbwLn(i))
      SbwFile(i)=' '
      call FeSbwRemove(i)
      go to 9999
      entry FeQuestSbwActivate(iwa)
      i=iwa+SbwFr-1
      call FeSbwActivate(i)
      go to 9999
      entry FeQuestSbwDeactivate(iwa)
      i=iwa+SbwFr-1
      call FeSbwDeactivate(i)
9999  return
      end
