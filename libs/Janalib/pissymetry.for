      subroutine PisSymetry(ln,rm,s,n,nd,ndm)
      dimension rm(ndm**2,n),s(ndm,n)
      character*1 Znak
      do i=1,n
        write(ln,'('' Operation#'',i3)') i
        call PisOperator(ln,rm(1,i),s(1,i),1.,nd)
        if(i.eq.n) then
          Znak='='
        else
          Znak='-'
        endif
        write(ln,'(1x,333a1)')(znak,j=1,13+nd*3)
      enddo
      return
      end
