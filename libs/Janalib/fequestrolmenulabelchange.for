      subroutine FeQuestRolMenuLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer Color
      Klic=0
      go to 1000
      entry FeQuestRolMenuLabelRemove(iwp)
      Klic=1
1000  iw=iwp+RolMenuFr-1
      if(RolMenuLabel(iw).ne.' ') then
        call FeTextErase(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                   RolMenuLabel(iw),RolMenuJustify(iw),LightGray)
      endif
      if(Klic.eq.0) then
        Color=Black
        go to 2000
      else
        go to 9999
      endif
      entry FeQuestRolMenuLabelMake(iwp,Text)
      iw=iwp+RolMenuFr-1
      if(RolMenuState(iw).eq.RolMenuDisabled) then
        Color=White
      else
        Color=Black
      endif
2000  RolMenuLabel(iw)=Text
      if(Text.ne.' ') then
        call FeGetTextRectangle(RolMenuLabelX(iw),RolMenuLabelY(iw),
     1    RolMenuLabel(iw),RolMenuJustify(iw),1,x1,x2,y1,y2,refx,refy,
     2    conx,cony)
        RolMenuLabelXMin(iw)=x1
        RolMenuLabelXMax(iw)=x2
        RolMenuLabelYMin(iw)=y1
        RolMenuLabelYMax(iw)=y2
        call FeTextErase(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                   RolMenuLabel(iw),RolMenuJustify(iw),LightGray)
        call FeOutStUnder(0,RolMenuLabelX(iw),RolMenuLabelY(iw),
     1                    RolMenuLabel(iw),RolMenuJustify(iw),Color,
     2                    Color,RolMenuZ(iw:iw))
      else
        RolMenuLabelXMin(iw)=0.
        RolMenuLabelXMax(iw)=0.
        RolMenuLabelYMin(iw)=0.
        RolMenuLabelYMax(iw)=0.
        RolMenuZ(iw:iw)=' '
      endif
9999  return
      end
