      subroutine Latin2ToWindows(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      dimension ILat2(39),IWin(39)
      data IWin /z"8A",z"8D",z"8E",z"9A",z"9D",z"9E",z"C1",z"C8",z"C9",
     1           z"CC",z"CD",z"CF",z"D2",z"D3",z"D8",z"D9",z"DA",z"DD",
     2           z"E1",z"E8",z"E9",z"EC",z"D8",z"ED",z"EF",z"F2",z"F3",
     3           z"F8",z"F9",z"FA",z"FD",z"C4",z"CB",z"D6",z"DC",z"E4",
     4           z"EB",z"F6",z"FC"/
      data ILat2/z"E6",z"9B",z"A6",z"E7",z"9C",z"A7",z"B5",z"AC",z"90",
     1           z"B7",z"D6",z"D2",z"D5",z"E0",z"FC",z"DE",z"E9",z"ED",
     2           z"A0",z"9F",z"82",z"D8",z"FC",z"A1",z"D4",z"E5",z"A2",
     3           z"FD",z"85",z"A3",z"EC",z"8E",z"D3",z"99",z"9A",z"84",
     4           z"89",z"94",z"81"/
      Klic=0
      go to 1000
      entry WindowsToLatin2(Veta)
      Klic=1
1000  do i=1,idel(Veta)
        ichp=ichar(Veta(i:i))
        do j=1,39
          if(Klic.eq.0) then
            if(ILat2(j).eq.ichp) go to 1100
          else
            if(IWin(j).eq.ichp) go to 1100
          endif
        enddo
        j=0
1100    if(j.gt.0) then
          if(Klic.eq.0) then
            Veta(i:i)=char(IWin(j))
          else
            Veta(i:i)=char(ILat2(j))
          endif
        endif
      enddo
      return
      end
