      subroutine FeUnforeseenError(Text)
      include 'fepc.cmn'
      character*(*) Text
      NInfo=1
      if(Text.ne.' ') then
        TextInfo(1)=Text
        NInfo=NInfo+1
      endif
      TextInfo(NInfo)='Unforeseen error, please contact authors.'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      return
      end
