      subroutine indexc4(k,n,ia)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(*)
      i=ipoc4(k)
      ip=ipec4(i)
      do i=n,1,-1
        ia(i)=mod(ip-1,4)+1
        ip=(ip-1)/4
      enddo
      return
      end
