      subroutine FeDrawFrame(xm,ym,xd,yd,dfx,ColorRD,ColorLU,
     1                       ColorObtah,ObtahniFrame)
      include 'fepc.cmn'
      dimension obx(6),oby(6)
      integer ColorRD,ColorLU,ColorObtah
      logical ObtahniFrame
      obx(1)=anint(xm*EnlargeFactor-dfx)/EnlargeFactor
      oby(1)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
      obx(2)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
      oby(2)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
      obx(3)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
      oby(3)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
      obx(4)=anint((xm+xd)*EnlargeFactor+1.)/EnlargeFactor
      oby(4)=anint((ym+yd)*EnlargeFactor+1.)/EnlargeFactor
      obx(5)=anint((xm+xd)*EnlargeFactor+1.)/EnlargeFactor
      oby(5)=anint(ym*EnlargeFactor-1.)/EnlargeFactor
      obx(6)=anint(xm*EnlargeFactor-1.)/EnlargeFactor
      oby(6)=anint(ym*EnlargeFactor-1.)/EnlargeFactor
      if(dfx.gt.0.) call FePolygon(obx,oby,6,4,0,0,ColorRD)
      obx(2)=anint(xm*EnlargeFactor-dfx)/EnlargeFactor
      oby(2)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
      obx(5)=anint(xm*EnlargeFactor-1.)/EnlargeFactor
      oby(5)=anint((ym+yd)*EnlargeFactor+1.)/EnlargeFactor
      if(dfx.gt.0.) call FePolygon(obx,oby,6,4,0,0,ColorLU)
      if(ObtahniFrame) then
        obx(2)=anint((xm+xd)*EnlargeFactor+dfx)/EnlargeFactor
        oby(2)=anint(ym*EnlargeFactor-dfx)/EnlargeFactor
        obx(4)=obx(1)
        oby(4)=anint((ym+yd)*EnlargeFactor+dfx)/EnlargeFactor
        obx(5)=obx(1)
        oby(5)=oby(1)
        call FePolyline(5,obx,oby,ColorObtah)
      endif
      return
      end
