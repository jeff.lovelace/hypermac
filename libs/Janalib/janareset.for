      subroutine JanaReset(Change)
      use Atoms_mod
      use Basic_mod
      use Powder_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      character*256 t256
      character*80 PuvodniSoubor
      logical ExistFile,FileDiff,Change,PwdCheckTOFInD
      Change=.false.
      call CrlCleanSymmetry
      ISymmBasic=0
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
1000  if(ifln.gt.0) then
        do i=1,9
          ExistMFile(i)=ExistFile(fln(:ifln)//ExtMFile(i))
        enddo
      else
        ExistMFile=.false.
      endif
      do i=1,ubound(SpecMatrixName,1)
1100    j=index(SpecMatrixName(i),'#')
        if(j.gt.0) then
          SpecMatrixName(i)(j:j)=Tabulator
          go to 1100
        endif
      enddo
      j=1
      do 1200i=1,9
        if(ExistMFile(i)) then
          t256=fln(:ifln)//ExtMFile(i)
          call CheckFileIfNotReadOnly(t256,1)
          if(ErrFlag.ne.0) go to 1150
          call CheckEOLOnFile(t256,j)
          j=0
          if(ErrFlag.eq.0) go to 1200
1150      fln=' '
          ifln=0
          go to 1000
        endif
1200  continue
      SetMetAllowed=.true.
      if(ExistM50) then
        call CrlTestJana2000(Change)
        if(ExistFile(HistoryFileOld)) then
          if(Change) then
            call MoveFile(HistoryFileOld,HistoryFile)
          else
            call DeleteFile(HistoryFileOld)
          endif
        endif
        if(fln.eq.' ') go to 9999
      else
        call DeleteFile(HistoryFileOld)
      endif
      if(ExistM90) then
        if(ExistM95) call iom95(0,fln(:ifln)//'.m95')
        call iom90(0,fln(:ifln)//'.m90')
        isPowder=iabs(DataType(KDatBlock)).eq.2
        call PwdSetTOF(KDatBlock)
      else
        if(ExistM95) then
          NDatBlock=1
          KDatBlock=1
          call SetBasicM90(1)
          isPowder=ExistM41
          ExistPowder=isPowder
          call iom95(0,fln(:ifln)//'.m95')
          Radiation(KDatBlock)=RadiationRefBlock(1)
          if(isPowder) then
            DataType(KDatBlock)=2
          else
            DataType(KDatBlock)=1
          endif
          call PwdSetTOF(KDatBlock)
        endif
      endif
      if(ExistM50) then
        PuvodniSoubor=fln(:ifln)//'.m50'
        if(FileDiff(PuvodniSoubor,PreviousM50)) then
          call iom50(0,0,PuvodniSoubor)
          WrongM50=ErrFlag.ne.0
          call CopyFile(PuvodniSoubor,PreviousM50)
        endif
      else
        StructureName=' '
        call SetBasicM50
      endif
      if(ParentStructure) then
        PuvodniSoubor=fln(:ifln)//'.m44'
      else
        PuvodniSoubor=fln(:ifln)//'.m40'
      endif
      if(ExistM40) then
        if(FileDiff(PuvodniSoubor,PreviousM40)) go to 1330
        if(ExistPowder) then
          if(FileDiff(fln(:ifln)//'.m41',PreviousM41)) go to 1330
        endif
        go to 1350
1330    WrongM40=.false.
        WrongM41=.false.
        if(.not.WrongM50) call iom40(0,0,PuvodniSoubor)
        if(ErrFlag.eq.0) call CrlAtomNamesIni
      else
        call AllocateAtoms(1)
        call SetBasicM40(.false.)
        ExistM41=ExistPowder
        WrongM40=.false.
        WrongM41=.false.
        if(ExistM50) then
          call iom40(1,0,fln(:ifln)//'.m40')
          ExistM40=.true.
        endif
      endif
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
      else if(ErrFlag.ne.0) then
        if(ErrFlag.eq.2) then
          WrongM41=.true.
        else
          WrongM40=.true.
        endif
      endif
      call CopyFile(PuvodniSoubor,PreviousM40)
      if(ExistPowder) then
        call CopyFile(fln(:ifln)//'.m41',PreviousM41)
        if(ExistM90) then
          KDatBlockIn=KDatBlock
          do KDatBlock=1,NDatBlock
            call PwdSetTOF(KDatBlock)
            if(iabs(DataType(KDatBlock)).eq.2) call PwdM92Nacti
          enddo
          KDatBlock=KDatBlockIn
          call PwdSetTOF(KDatBlock)
        endif
        if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
      endif
1350  if(.not.WrongM40.and..not.WrongM50) call SpecAt
      if(ExistFile(fln(:ifln)//'.m42')) then
        call iom42(0,0,fln(:ifln)//'.m42')
        call CopyFile(fln(:ifln)//'.m42',PreviousM42)
      endif
      IgnoreE=.false.
      IgnoreW=.false.
      LstOpened=.false.
      MakeCIFForGraphicViewer=.false.
      ContourCallFourier=.false.
      RefineCallFourier=.false.
      if(NDatBlock.gt.1) then
        do i=1,NDatBlock
          MenuDatBlock(i)=DatBlockName(i)
          if(DataType(i).eq.1) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->single crystal'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '->powder'
          endif
          if(Radiation(i).eq.NeutronRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/neutrons'
          else if(Radiation(i).eq.XRayRadiation) then
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/X-ray'
          else
            MenuDatBlock(i)=MenuDatBlock(i)(:idel(MenuDatBlock(i)))//
     1                      '/electron'
          endif
        enddo
      endif
      call DRMakeMenuRefBlock
9999  call RewriteTitle(' ')
      call FeBottomInfo(' ')
      return
100   format(i1)
101   format(6i5)
      end
