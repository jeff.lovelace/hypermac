      subroutine FeXYPlot(x,y,n,LineTypeDraw,PlotMode,Color)
      include 'fepc.cmn'
      dimension x(n),y(n),xpole(2),ypole(2),xline(:),yline(:)
      integer Color,PlotMode
      logical kresli
      allocatable xline,yline
      if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1  write(85,'(2f15.6)')(x(i),y(i),i=1,n)
      nn=1000
      allocate(xline(nn),yline(nn))
      if(PlotMode.eq.NormalPlotMode) then
        call FePlotMode('N')
      else
        call FePlotMode('E')
      endif
      call FeLineType(LineTypeDraw)
      k=0
      jp=1
      xpole(1)=x(1)
      ypole(1)=y(1)
      do 5000i=2,n
        Kresli=.false.
        dx=x(i)-x(i-1)
        if(y(i).gt.YoMaxAcWin.and.y(i-1).le.YoMaxAcWin) then
          xpole(2)=x(i-1)+dx*(YoMaxAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(2)=YoMaxAcWin
          Kresli=.true.
        else if(y(i).le.YoMaxAcWin.and.y(i-1).gt.YoMaxAcWin) then
          xpole(1)=x(i-1)+dx*(YoMaxAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(1)=YoMaxAcWin
          xpole(2)=x(i)
          ypole(2)=y(i)
          jp=1
        else if(y(i).lt.YoMinAcWin.and.y(i-1).ge.YoMinAcWin) then
          xpole(2)=x(i-1)+dx*(YoMinAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(2)=YoMinAcWin
          Kresli=.true.
        else if(y(i).ge.YoMinAcWin.and.y(i-1).lt.YoMinAcWin) then
          xpole(1)=x(i-1)+dx*(YoMinAcWin-y(i-1))/(y(i)-y(i-1))
          ypole(1)=YoMinAcWin
          xpole(2)=x(i)
          ypole(2)=y(i)
          jp=1
        else if((y(i).le.YoMaxAcWin.and.y(i-1).le.YoMaxAcWin).and.
     1          (y(i).ge.YoMinAcWin.and.y(i-1).ge.YoMinAcWin)) then
          xpole(2)=x(i)
          ypole(2)=y(i)
        else
          go to 5000
        endif
        do j=jp,2
          k=k+1
          xline(k)=FeXo2X(xpole(j))
          yline(k)=FeYo2Y(ypole(j))
          if(k.eq.nn) then
            call FePolyLine(nn,xline,yline,Color)
            k=1
            xline(k)=xline(nn)
            yline(k)=yline(nn)
            jp=2
            go to 5000
          else
            jp=2
          endif
        enddo
        if(Kresli) then
          call FePolyLine(k,xline,yline,Color)
          xline(1)=xline(k)
          yline(1)=yline(k)
          k=1
          jp=1
          go to 5000
        else
          jp=2
        endif
5000  continue
      if(k.gt.1) call FePolyLine(k,xline,yline,Color)
      call FePlotMode('N')
      call FeLineType(NormalLine)
      deallocate(xline,yline)
      return
      end
