      subroutine FeQuestLinkaMake(id,n1)
      include 'fepc.cmn'
      integer Color1,Color2
      XFrom=QuestXMin(id)+2.5
      XTo=QuestXMax(id)-2.5
      go to 1000
      entry FeQuestLinkaFromToMake(id,XFromIn,XToIn,n1)
      XFrom=QuestXMin(id)+XFromIn
      XTo=QuestXMin(id)+XToIn
1000  ym=QuestYPosition(id,n1)
      go to 1200
      entry FeQuestAbsLinkaMake(id,ymi)
      XFrom=QuestXMin(id)+2.5
      XTo=QuestXMax(id)-2.5
      go to 1100
      entry FeQuestAbsLinkaFromToMake(id,XFromIn,XToIn,ymi)
      XFrom=QuestXMin(id)+XFromIn
      XTo=QuestXMin(id)+XToIn
1100  ym=ymi
1200  LinkaTo=LinkaTo+1
      LinkaLastMade=LinkaTo-LinkaFr+1
      QuestLinkaTo(id)=LinkaTo
      LinkaState(LinkaTo)=LinkaOn
      LinkaXMin(LinkaTo)=XFrom
      LinkaXMax(LinkaTo)=XTo
      LinkaYPos(LinkaTo)=QuestYMin(id)+ym
      i=LinkaTo
      go to 1300
      entry FeQuestLinkaOn(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaOn
1300  Klic=0
      go to 2000
      entry FeQuestLinkaRemove(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaRemoved
      go to 1400
      entry FeQuestLinkaOff(iwa)
      i=iwa+LinkaFr-1
      LinkaState(i)=LinkaOff
1400  Klic=1
2000  if(Klic.eq.0) then
        Color1=Gray
        Color2=White
      else
        Color1=LightGray
        Color2=LightGray
      endif
      call FeTwoPixLineHoriz(LinkaXMin(i),LinkaXMax(i),LinkaYPos(i),
     1                       Color1,Color2)
9999  return
      end
