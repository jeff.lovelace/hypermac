      subroutine FeMakeGrWin(XGrLeft,XGrRight,YGrDown,YGrUp)
      include 'fepc.cmn'
      if(Console) go to 9999
      if(BatchMode) then
        call FeFillRectangle(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,4,0,0,White)
        XMinGrWin=XMinBasWin
        XMaxGrWin=XMaxBasWin
        YMinGrWin=YMinBasWin
        YMaxGrWin=YMaxBasWin
      else
        call FeFillRectangle(XMinBasWin,XMaxBasWin,YMinBasWin,
     1                       YMaxBasWin,4,0,0,LightGray)
        XMinGrWin=XMinBasWin+XGrLeft
        XMaxGrWin=XMaxBasWin-XGrRight
        YMinGrWin=YMinBasWin+YGrDown
        YMaxGrWin=YMaxBasWin-YGrUp
        call FeFillRectangle(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       4,0,0,Black)
      endif
      XCenGrWin=(XMinGrWin+XMaxGrWin)*.5
      YCenGrWin=(YMinGrWin+YMaxGrWin)*.5
      XLenGrWin=XMaxGrWin-XMinGrWin
      YLenGrWin=YMaxGrWin-YMinGrWin
      call FeBottomInfo(' ')
9999  return
      end
