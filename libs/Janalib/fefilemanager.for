      subroutine FeFileManager(Header,FileName,FilterIn,Key,KeepOrgDir,
     1                         ich)
      include 'fepc.cmn'
      character*(*) Header,FileName,FilterIn
      character*2 MenD(26)
      character*80 FileNameO,Radka,Filter,RolMenuTextQuest
      character*256 EdwStringQuest,SbwStringQuest,CurrentDirIn,
     1              CurrentDirO
      character*128 ListDir,ListFile
      integer FeChdir,ErrChdir,RolMenuSelectedQuest,ActualDrive,
     1        RolMenuNumberQuest,SbwItemPointerQuest
      logical KeepOrgDir,Protected,EqIgCase,CrwLogicQuest,FeYesNoHeader,
     1        WizardModeIn,Finish
      ich=0
      call FeDeferOutput
      WizardModeIn=WizardMode
      WizardMode=.false.
      id=NextQuestId()
      CurrentDirIn=CurrentDir
      FileNameO=FileName
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      if(Key.eq.0.or.OpSystem.le.0) then
        il=17
      else
        il=16
      endif
      if(Key.ge.0) then
        xqd=600.
        xqdp=xqd*.5
      else
        xqd=300.
        xqdp=xqd
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Header,0,LightGray,0,0)
      xpom=15.
      dpom=xqdp-50.
      tpom=xpom+dpom*.5
      if(Language.eq.1) then
        Radka='Adres��'
      else
        Radka='Directory'
      endif
      call FeQuestLblMake(id,tpom,1,Radka,'C','N')
      ilp=14
      call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                    SbwVertical)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      nSbwDir=SbwLastMade
      dpome=dpom+SbwXMaxPruh(1,SbwLastMade)-SbwXMinPruh(1,SbwLastMade)
      il=ilp+1
      call FeQuestButtonMake(id,tpom-ButYd*.5,il,ButYd,ButYd,'#')
      nButtDir=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      i=ButtonLastMade+ButtonFr-1
      ButtonZ(i:i)='v'
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpome,EdwYd,1)
      nEdwDir=EdwLastMade
      if(Key.ge.0) then
        xpom=xpom+xqdp
        xpomf=xpom
        tpom=tpom+xqdp
        if(Language.eq.1) then
          Radka='Soubor'
        else
          if(Key.eq.0) then
            Radka='File'
          else
            Radka='Structure'
          endif
        endif
        call FeQuestLblMake(id,tpom,1,Radka,'C','N')
        call FeQuestSbwMake(id,xpom,ilp,dpom,ilp-1,1,CutTextFromRight,
     1                      SbwVertical)
        call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
        nSbwFile=SbwLastMade
        il=ilp+1
        call FeQuestButtonMake(id,tpom-ButYd*.5,il,ButYd,ButYd,'#')
        nButtFile=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        i=ButtonLastMade+ButtonFr-1
        ButtonZ(i:i)='w'
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpome,EdwYd,0)
        nEdwFile=EdwLastMade
      else
        nSbwFile=0
        nButtFile=0
        nEdwFile=0
      endif
      il=il+1
      if(OpSystem.le.0) then
        tpom=15.
        if(Language.eq.1) then
          Radka='%Jednotka'
        else
          Radka='%Drive'
        endif
        xpom=tpom+FeTxLengthUnder(Radka)+12.
        dpom=40.+EdwYd
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,
     1                          1)
        call GetActiveDrives(MenD,NMenD)
        do i=1,NMenD
          if(EqIgCase(CurrentDir(1:2),MenD(i))) then
            KMenD=i
            go to 1110
          endif
        enddo
        KMenD=1
1110    nRolMenuDrive=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
        xpom=xpom+dpom+20.
      else
        xpom=15.
        nRolMenuDrive=0
      endif
      Radka='%Make new (sub)directory'
      tpom=FeTxLengthUnder(Radka)+15.
      call FeQuestButtonMake(id,xpom,il,tpom,ButYd,Radka)
      nButtNewDir=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(Key.eq.0) then
        xpom=xpomf
        tpom=xpom+CrwXd+10.
        if(Language.eq.1) then
          Radka='Pou�ij %filtr'
        else
          Radka='Use %filter'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Radka,'L',CrwXd,CrwYd,1,
     1                      0)
        nCrwFilter=CrwLastMade
        if(FilterIn.eq.'*') then
          i=CrwOff
        else
          i=CrwOn
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.CrwOn)
        tpom=tpom+FeTxLengthUnder(Radka)+5.
        Radka='=>'
        xpom=tpom+FeTxLengthUnder(Radka)+7.
        dpom=xqd-xpom-19.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,1)
        nEdwFilter=EdwLastMade
        Filter=FilterIn
        if(i.eq.CrwOn) call FeQuestStringEdwOpen(EdwLastMade,Filter)
      else
        Filter=' '
        nCrwFilter=0
        nEdwFilter=0
      endif
      CurrentDirO=CurrentDir
      tpom=27.
      xpom=65.
      call FeQuestLblMake(id,tpom,ilp+1,'Write','L','N')
      nLblWrite=LblLastMade
      call FeQuestLblOff(LblLastMade)
      call FeQuestLblMake(id,xpom,ilp+1,'protected','L','N')
      nLblProtected=LblLastMade
      call FeQuestLblOff(LblLastMade)
1500  do i=1,5
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.le.0) go to 1530
      enddo
1530  Protected=ErrChdir.lt.0
      if(ErrChdir.gt.0) then
        ErrChdir=FeChdir(CurrentDirO)
        NInfo=1
        TextInfo(1)='The directory doesn''t exist'
        if(FeYesNoHeader(-1.,-1.,'Do you want to create it?',0)) then
          if(OpSystem.le.0) then
            call FeMkDir(CurrentDir,ichp)
          else
            call FeSystem('mkdir '//CurrentDir)
          endif
          ErrChdir=FeChdir(CurrentDir)
          if(ErrChdir.ne.0) then
            TextInfo(1)='Sorry, the action has not been successful.'
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          else
            go to 1560
          endif
        endif
        ErrChdir=FeChdir(CurrentDirO)
        CurrentDir=CurrentDirO
1560    EventType=EventEdw
        EventNumber=nEdwDir
      else if(ErrChdir.lt.0) then
        call FeQuestLblOn(nLblWrite)
        call FeQuestLblOn(nLblProtected)
      else
        call FeQuestLblOff(nLblWrite)
        call FeQuestLblOff(nLblProtected)
      endif
      call FeGetCurrentDir
      if(Key.ge.0) then
        call CloseIfOpened(SbwLn(nSbwFile))
        call CloseIfOpened(SbwLn(nSbwDir))
      endif
      call FeDir(ListDir,ListFile,Filter,Key)
      call FeQuestSbwMenuOpen(nSbwDir,ListDir)
      call FeQuestStringEdwOpen(nEdwDir,CurrentDir)
      if(Key.ge.0) then
        call FeQuestSbwMenuOpen(nSbwFile,ListFile)
        call FeQuestStringEdwOpen(nEdwFile,FileName)
      endif
2000  call GetActiveDrives(MenD,NMenD)
      if(NMenD.eq.RolMenuNumberQuest(nRolMenuDrive)) then
        do i=1,NMenD
          if(MenD(i).ne.RolMenuTextQuest(i,nRolMenuDrive)) go to 2020
        enddo
        go to 2060
      endif
2020  do i=1,NMenD
        if(EqIgCase(CurrentDir(1:2),MenD(i))) then
          KMenD=i
          go to 2040
        endif
      enddo
      KMenD=1
2040  call FeQuestRolMenuOpen(RolMenuLastMade,MenD,NMenD,KMenD)
2060  ActualDrive=RolMenuSelectedQuest(nRolMenuDrive)
      Finish=.false.
      call FeQuestEvent(id,ich)
2100  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtDir) then
          CurrentDirO=CurrentDir
          i=SbwItemPointerQuest(nSbwDir)
          CurrentDir=SbwStringQuest(i,nSbwDir)
          go to 1500
        else if(CheckNumber.eq.nButtFile) then
          i=SbwItemPointerQuest(nSbwFile)
          if(i.gt.0) then
            FileName=SbwStringQuest(i,nSbwFile)
            if(Key.eq.1) then
              i=index(FileName,'#$color')
              if(i.gt.0) FileName(i:)=' '
            endif
            call FeQuestStringEdwOpen(nEdwFile,FileName)
            EventType=EventEdw
            EventNumber=nEdwFile
          else
            go to 2060
          endif
        else if(CheckNumber.eq.nButtNewDir) then
          idp=NextQuestId()
          xqdp=400.
          il=1
          call FeQuestCreate(idp,-1.,-1.,xqdp,1,' ',0,LightGray,0,0)
          tpom=5.
          Radka='New subdirectory'
          xpom=tpom+FeTxLengthUnder(Radka)+10.
          dpom=xqdp-xpom-5.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,
     1                        0)
          nEdwNewDir=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,' ')
2200      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2200
          endif
          if(ich.eq.0) then
            Radka=EdwStringQuest(nEdwNewDir)
            if(OpSystem.le.0) then
              call FeMkDir(Radka,ichp)
            else
              call FeSystem('mkdir '//Radka)
            endif
            if(LocateSubstring(Radka,':',.false.,.true.).gt.0) then
              CurrentDir=Radka
            else
              CurrentDir=CurrentDir(:idel(CurrentDir))//
     1                   Radka(:idel(Radka))
            endif
          endif
          call FeQuestRemove(idp)
          EventType=EventEdw
          EventNumber=nEdwDir
          if(ich.eq.0) go to 1500
        endif
        if(.Not.Finish) go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDrive) then
        i=RolMenuSelectedQuest(nRolMenuDrive)
        CurrentDirO=CurrentDir
        CurrentDir=MenD(i)
        ErrChdir=FeChdir(CurrentDir)
        if(ErrChdir.ne.0) then
          call FeChybne(-1.,-1.,'The drive isn''t accessible',' ',
     1                  SeriousError)
          CurrentDir=CurrentDirO
          ErrChdir=FeChdir(CurrentDir)
          call FeQuestRolMenuOpen(nRolMenuDrive,MenD,NMenD,ActualDrive)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFilter) then
        if((OpSystem.le.0.and.
     1      EqIgCase(Filter,EdwStringQuest(nEdwFilter))).or.
     2     (OpSystem.eq.1.and.
     3      Filter.eq.EdwStringQuest(nEdwFilter))) then
          go to 2000
        else
          Filter=EdwStringQuest(nEdwFilter)
          go to 1500
        endif
      else if(CheckType.eq.EventSbwDoubleClick) then
        CheckType=EventButton
        i=mod(CheckNumber,100)
        if(i.eq.nSbwDir) then
          CheckNumber=nButtDir
        else
          CheckNumber=nButtFile
          Finish=.true.
        endif
        go to 2100
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDir) then
        if(EdwStringQuest(nEdwDir).ne.CurrentDir) then
          CurrentDir=EdwStringQuest(nEdwDir)
          go to 1500
        endif
        go to 2000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFilter) then
        if(CrwLogicQuest(nCrwFilter)) then
          call FeQuestStringEdwOpen(nEdwFilter,Filter)
        else
          Filter='*'
          call FeQuestEdwClose(nEdwFilter)
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0) then
        if(Key.ge.0) then
          FileName=EdwStringQuest(nEdwFile)
          if(FileName.eq.' ') then
            if(Key.eq.0) then
              Radka='file'
            else
              Radka='structure'
            endif
            call FeChybne(-1.,YBottomMessage,'The empty '//
     1                    Radka(:idel(Radka))//
     2                    ' name isn''t acceptable, try again',' ',
     3                    SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 2060
          endif
        else
          FileName=EdwStringQuest(nEdwDir)
          if(FileName.eq.' ') then
            Radka='Directory'
            call FeChybne(-1.,YBottomMessage,
     1                    'The empty '//Radka(:idel(Radka))//
     2                    ' name isn''t acceptable, try again',' ',
     3                    SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 2060
          endif
        endif
        if(Key.eq.1) then
          i=index(FileName,'#$color')
          if(i.gt.0) FileName(i:)=' '
        endif
      endif
      call FeQuestRemove(id)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      if(ich.ne.0) then
        FileName=FileNameO
        go to 9000
      endif
      if(KeepOrgDir) then
        if(FileName.ne.' '.and.CurrentDir.ne.CurrentDirIn)
     1    FileName=CurrentDir(:idel(CurrentDir))//
     2    FileName(:idel(FileName))
        go to 9000
      else
        go to 9999
      endif
9000  CurrentDir=CurrentDirIn
      i=FeChdir(CurrentDir(:idel(CurrentDir)))
9999  WizardMode=WizardModeIn
      return
      end
