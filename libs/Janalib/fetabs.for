      subroutine FeTabs
      include 'fepc.cmn'
      character*1 ChTabsNew
      integer TTabsNew,Which
      entry FeTabsAdd(XTabsNew,Which,TTabsNew,ChTabsNew)
      do i=1,NTabs(Which)
        if(XTabsNew.lt.XTabs(i,Which)) then
          n=i
          go to 1100
        endif
      enddo
      n=NTabs(Which)+1
1100  NTabs(Which)=min(NTabs(Which)+1,MxTabs)
      do i=n+1,NTabs(Which)
        XTabs(i,Which)=XTabs(i-1,Which)
        TTabs(i,Which)=TTabs(i-1,Which)
        ChTabs(i,Which)=ChTabs(i-1,Which)
      enddo
      XTabs(n,Which)=XTabsNew
      TTabs(n,Which)=TTabsNew
      ChTabs(n,Which)=ChTabsNew
      go to 9999
      entry FeTabsReset(Which)
      if(Which.lt.0) then
        IFrom=0
        ITo=20
      else
        IFrom=Which
        ITo=Which
      endif
      do i=IFrom,ITo
        NTabs(i)=0
        call SetRealArrayTo(XTabs(1,i),MxTabs,0.)
        call SetIntArrayTo(TTabs(1,i),MxTabs,0)
        call SetStringArrayTo(ChTabs(1,i),MxTabs,' ')
      enddo
9999  return
      end
