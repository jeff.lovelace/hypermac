      subroutine FeCrwBasic(id,Color)
      include 'fepc.cmn'
      integer Color
      call FeFillRectangle(CrwXMin(id),CrwXMax(id),CrwYMin(id),
     1                     CrwYMax(id),4,0,0,Color)
      call FeDrawFrame(CrwXMin(id),CrwYMin(id),
     1                 CrwXMax(id)-CrwXMin(id),
     2                 CrwYMax(id)-CrwYMin(id),2.,White,Gray,
     3                 Black,.false.)
      xu(1)=anint(CrwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(CrwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=anint(CrwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(2)=anint(CrwYmin(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(CrwXmax(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(3)=anint(CrwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=anint(CrwXmin(id)*EnlargeFactor-1.)/EnlargeFactor
      yu(4)=anint(CrwYmax(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,Gray)
      return
      end
