      subroutine FeExpandLineToActiveWindow(xu1,yu1,xu2,yu2,
     1                                      xe1,ye1,xe2,ye2,ich)
      include 'fepc.cmn'
      logical InsideXYPlot
      ich=0
      n=0
      x11=xu1
      y11=yu1
      x12=xu2
      y12=yu2
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,1),XCornAcWin(2,1),
     2                        XCornAcWin(1,2),XCornAcWin(2,2),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          xe1=xpom
          ye1=ypom
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,2),XCornAcWin(2,2),
     2                        XCornAcWin(1,3),XCornAcWin(2,3),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,3),XCornAcWin(2,3),
     2                        XCornAcWin(1,4),XCornAcWin(2,4),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      call FeLineIntersection(x11,y11,x12,y12,
     1                        XCornAcWin(1,4),XCornAcWin(2,4),
     2                        XCornAcWin(1,1),XCornAcWin(2,1),
     3                        xpom,ypom,
     4                        ich)
      if(ich.eq.0) then
        if(InsideXYPlot(xpom,ypom)) then
          n=n+1
          if(n.eq.1) then
            xe1=xpom
            ye1=ypom
          else
            xe2=xpom
            ye2=ypom
            go to 9999
          endif
        endif
      endif
      ich=1
9999  return
      end
