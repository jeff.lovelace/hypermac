      subroutine FeIcon
      include 'fepc.cmn'
      character*(*) Label,File
      character*80  Veta
      integer Color
      return
      entry FeIconOpen(id,xm,ym,xd,yd,Label,File)
      IconXMin(id)=xm
      IconXMax(id)=xm+xd-1./EnlargeFactor
      IconYMin(id)=ym
      IconYMax(id)=ym+yd-1./EnlargeFactor
      call FeOutStUnder(0,(IconXMin(id)+IconXMax(id))*.5,
     1                     IconYMin(id)-15./EnlargeFactor,
     2                     Label(:idel(Label)),'C',White,White,
     3                     IconZ(id:id))
      IconLabel(id)=Label
      IconFile(id)=File
      entry FeIconOff(id)
      IconState(id)=IconOff
      Veta=IconFile(id)(:idel(IconFile(id)))//'-off'
      go to 2000
      entry FeIconOn(id)
      IconState(id)=IconOn
      Veta=IconFile(id)(:idel(IconFile(id)))//'-on'
      go to 2000
      entry FeIconDisable(id)
      Veta=IconFile(id)(:idel(IconFile(id)))//'-disable'
      IconState(id)=IconDisabled
      go to 2000
      entry FeIconRemove(id)
      Color=Black
      IconState(id)=IconRemoved
      IconZ(id:id)=' '
      call FeFillRectangle(IconXMin(id),IconXMax(id),IconYMin(id),
     1                     IconYMax(id),4,0,0,Color)
      call FeOutStUnder(0,(IconXMin(id)+IconXMax(id))*.5,
     1                     IconYMin(id)-15./EnlargeFactor,
     2                     Label(:idel(Label)),'C',Black,Black,
     3                     IconZ(id:id))
      go to 9999
2000  if(OpSystem.lt.0) then
        call FeLoadImage(IconXMin(id),IconXMax(id),IconYMin(id),
     1                   IconYMax(id),Veta(:idel(Veta))//'.bmp',1)
      else
        call FeLoadImage(IconXMin(id),IconXMax(id),IconYMin(id),
     1                   IconYMax(id),Veta(:idel(Veta))//'.pcx',1)
      endif
9999  return
      end
