      subroutine FeReadFileName(FileName,Text,Filter,ich)
      include 'fepc.cmn'
      character*(*) FileName,Text,Filter
      character*256 EdwStringQuest
      character*80  Veta
      call FeBoldFont
      xd=max(FeTxLength(Text)+10.,350.)
      call FeNormalFont
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xd,1,Text,0,LightGray,0,0)
      if(Language.eq.0) then
        Veta='%File name'
      else
        Veta='%Jm�no souboru'
      endif
      bpom=80.
      tpom=5.
      xpom=FeTxLength(Veta)+10.
      dpom=xd-xpom-bpom-15.
      call FeQuestEdwMake(id,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      if(Language.eq.0) then
        Veta='%Browse'
      else
        Veta='%Hledej'
      endif
      call FeQuestButtonMake(id,xd-bpom-5.,1,bpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtBrowse=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        call FeFileManager(Text,FileName,Filter,0,.true.,ich)
        call FeQuestStringEdwOpen(nEdwName,FileName)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) FileName=EdwStringQuest(nEdwName)
      call FeQuestRemove(id)
      return
      end
