      subroutine FeSbwKresliStrelku(id)
      include 'fepc.cmn'
      klic=0
      go to 1000
      entry FeSbwPohniStrelku(id,xin,xout)
      klic=1
      xpom=xin
1000  if(SbwDelStrelka(id).le.0) go to 9999
      XMinSloupek=anint(SbwXMaxPosun(3,id)*EnlargeFactor+3.)/
     1            EnlargeFactor
      XMaxSloupek=anint(SbwXMinPosun(4,id)*EnlargeFactor-3.)/
     1            EnlargeFactor
      XDelSloupek=anint((XMaxSloupek-XMinSloupek)*EnlargeFactor-
     1                  float(SbwDelStrelka(id)))/EnlargeFactor
      YMinSloupek=SbwYMinPruh(2,id)
      YMaxSloupek=SbwYMaxPruh(2,id)
      call FeFillRectangle(XMinSloupek,XMaxSloupek,
     1                     YMinSloupek,YMaxSloupek,
     2                     4,0,0,WhiteGray)
      if(Klic.eq.0) then
        if(SbwStripes(id).gt.1) then
          xpom=XMinSloupek+XDelSloupek*float(SbwStripeFrom(id)-1)/
     1                     float(SbwStripesMax(id)-SbwStripes(id))
        else
          xpom=XMinSloupek+XDelSloupek*float(SbwColumnFrom(id)-1)/
     1                     float(SbwColumnsMax(id)-SbwColumns(id))
        endif
      else
        xpom=min(xpom,anint(XMaxSloupek*EnlargeFactor-
     1                float(SbwDelStrelka(id)))/EnlargeFactor)
        xpom=max(xpom,XMinSloupek)
        xout=xpom
        if(SbwStripes(id).gt.1) then
          n=(nint((xpom-XMinSloupek)/XDelSloupek*
     1       float(SbwStripesMax(id)-SbwStripes(id))))*SbwLines(id)+1
        else
          n=nint((xpom-XMinSloupek)/XDelSloupek*
     1           float(SbwColumnsMax(id)-SbwColumns(id)))+1
        endif
        call FeSbwShowPohniLeftRight(id,n)
      endif
      SbwYMinStrelka(id)=anint(SbwYMinPruh(2,id)*EnlargeFactor+2.)/
     1                   EnlargeFactor
      SbwYMaxStrelka(id)=anint(SbwYMaxPruh(2,id)*EnlargeFactor-2.)/
     1                   EnlargeFactor
      SbwXMinStrelka(id)=anint(xpom*EnlargeFactor+2.)/EnlargeFactor
      SbwXMaxStrelka(id)=anint(SbwXMinStrelka(id)*EnlargeFactor+
     1                         float(SbwDelStrelka(id))-4)/
     2                   EnlargeFactor
      xLD=SbwXMinStrelka(id)
      xRU=SbwXMaxStrelka(id)
      wp=xRU-xLD
      yLD=SbwYMinStrelka(id)
      yRU=SbwYMaxStrelka(id)
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,LightGray)
      call FeDrawFrame(xLD,yLD,wp,hp,2./EnlargeFactor,Gray,White,Gray,
     1                 .false.)
9999  return
      end
