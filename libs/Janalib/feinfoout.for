      subroutine FeInfoOut(xmi,ymi,Text,Justify)
      include 'fepc.cmn'
      character*(*) Text
      character*1 Justify,JustifyActual,JustifyFinal
      logical WizardModeIn,EqIgCase,PropFontIn
      integer UseTabsIn
      UseTabsIn=UseTabs
      if(SilentRun) go to 9999
      JustifyActual=Justify
      go to 1200
      entry FeMsgOut(xmi,ymi,Text)
      UseTabsIn=UseTabs
      if(SilentRun) go to 9999
      NInfo=0
      JustifyActual='?'
1200  if(Console) then
        write(6,FormA) ' '//Text(:idel(Text))
        do i=1,Ninfo
          write(6,FormA) ' '//TextInfo(i)(:idel(TextInfo(i)))
        enddo
      else
        PropFontIn=PropFont
        if(.not.PropFont) call FeSetPropFont
        WizardModeIn=WizardMode
        WizardMode=.false.
        call FeBoldFont
        xd=FeTxLength(Text)
        call FeNormalFont
        do i=1,Ninfo
          if(NTabs(UseTabsInfo(i)).gt.0) then
            UseTabs=UseTabsInfo(i)
          else
            UseTabs=0
          endif
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
          xd=max(FeTxLength(TextInfo(i)),xd)
          if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
        enddo
        xd=xd+10.
        if(WaitTime.gt.0) xd=max(FeTxLength(ItRemains)+10.,xd)
        id=NextQuestid()
        il=-float(NInfo)*6
        call FeQuestCreate(id,xmi,ymi,xd,il,Text,0,LightGray,-1,0)
        il=-2
        do i=1,Ninfo
          il=il-6
          if(NTabs(UseTabsInfo(i)).gt.0) then
            UseTabs=UseTabsInfo(i)
          else
            UseTabs=0
          endif
          if(EqIgCase(JustifyActual,'?')) then
            JustifyFinal=TextInfoFlags(i)(1:1)
          else
            JustifyFinal=JustifyActual
          endif
          if(EqIgCase(JustifyFinal,'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        JustifyFinal,TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
1500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          go to 1500
        endif
        call FeQuestRemove(id)
        WizardMode=WizardModeIn
        if(.not.PropFontIn) call FeSetFixFont
      endif
9999  UseTabs=UseTabsIn
      return
      end
