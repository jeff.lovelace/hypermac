      subroutine FeSetTransXo2X(XfMin,XfMax,YfMin,YfMax,Equal)
      include 'fepc.cmn'
      dimension xf(3)
      logical Equal
      xf(3)=0.
      xf(1)=XfMin
      xf(2)=YfMin
      call multm(F2O,xf,XoCornAcWin(1,1),3,3,1)
      xf(1)=XfMin
      xf(2)=YfMax
      call multm(F2O,xf,XoCornAcWin(1,2),3,3,1)
      xf(1)=XfMax
      xf(2)=YfMax
      call multm(F2O,xf,XoCornAcWin(1,3),3,3,1)
      xf(1)=XfMax
      xf(2)=YfMin
      call multm(F2O,xf,XoCornAcWin(1,4),3,3,1)
      XoMinAcWin=XoCornAcWin(1,1)
      XoMaxAcWin=XoCornAcWin(1,1)
      YoMinAcWin=XoCornAcWin(2,1)
      YoMaxAcWin=XoCornAcWin(2,1)
      do i=2,4
        XoMinAcWin=min(XoMinAcWin,XoCornAcWin(1,i))
        XoMaxAcWin=max(XoMaxAcWin,XoCornAcWin(1,i))
        YoMinAcWin=min(YoMinAcWin,XoCornAcWin(2,i))
        YoMaxAcWin=max(YoMaxAcWin,XoCornAcWin(2,i))
      enddo
      XoCenAcWin=(XoMinAcWin+XoMaxAcWin)*.5
      YoCenAcWin=(YoMinAcWin+YoMaxAcWin)*.5
      XoLenAcWin=XoMaxAcWin-XoMinAcWin
      YoLenAcWin=YoMaxAcWin-YoMinAcWin
      X2XoRatio=XLenAcWin/XoLenAcWin
      Y2YoRatio=YLenAcWin/YoLenAcWin
      if(Equal) then
        X2XoRatio=min(X2XoRatio,Y2YoRatio)
        Y2YoRatio=X2XoRatio
      endif
      XOrgAcWin=XCenAcWin-.5*XoLenAcWin*X2XoRatio
      YOrgAcWin=YCenAcWin-.5*YoLenAcWin*Y2YoRatio
      do j=1,3
        do i=1,4
          if(j.eq.1) then
            XCornAcWin(j,i)=FeXo2X(XoCornAcWin(j,i))
          else if(j.eq.2) then
            XCornAcWin(j,i)=FeYo2Y(XoCornAcWin(j,i))
          else
            XCornAcWin(j,i)=0.
          endif
        enddo
      enddo
      return
      end
