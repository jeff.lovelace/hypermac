      subroutine FeDestroyKart
      include 'fepc.cmn'
      if(WizardMode) then
        ifin=1
      else
        ifin=0
      endif
      do i=NKart,ifin,-1
        if(i.ne.0) then
          call FePrepniListek(i)
          KartUpdateListek(i)=0
        endif
        if(i.eq.ifin) KartOn=.false.
        call FeQuestRemove(KartFirstId+i-1)
      enddo
      NKart=0
      KartNDol=0
      KartId=0
      return
      end
