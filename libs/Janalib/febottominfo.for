      subroutine FeBottomInfo(TextIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) TextIn
      character*256 TextLong,TextOld,Text
      logical EqIgCase
      data TextOld/' '/
      if(BatchMode.or.Console.or.YBottomText.le.0.) go to 9999
      if(YBottomText.le.0.) go to 9999
      if(TextIn.eq.' ') then
        if(EqIgCase(MasterName,'Notarius')) then
          Text=' '
        else
          TextLong=CurrentDir
          if(OpSystem.eq.1) then
            i=idel(RootDir)
            if(index(TextLong,RootDir(:i)).eq.1)
     1        TextLong='~/'//TextLong(i+1:)
          endif
          if(ifln.gt.0) then
            TextLong(idel(TextLong)+1:)=fln(:ifln)
            if(ParentStructure) then
              TextLong=TextLong(:idel(TextLong))//
     1                 ' (parent structure !!!)'
            endif
          else
            TextLong(idel(TextLong)+1:)='---'
          endif
          call FeCutNameLength(TextLong,Text,XLenBasWin-100.,
     1                         CutTextFromLeft)
          Text='Structure: '//Text(:idel(Text))
        endif
      else if(EqIgCase(TextIn,'#prazdno#')) then
        Text=' '
      else
        Text=TextIn
      endif
      call FeTextErase(0,5.,YBottomText,TextOld,'L',LightGray)
      call FeOutSt(0,5.,YBottomText,Text,'L',Black)
      TextOld=Text
9999  return
      end
