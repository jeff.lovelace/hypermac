      subroutine srotc4(a,n,b)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(4,4),b(*),ia(6),ja(6)
      m=35
      idenp=4**(n-1)
      do i=1,m
        k=ipoc4(i)
        ip=ipec4(k)
        do k=n,1,-1
          ia(k)=mod(ip-1,4)+1
          ip=(ip-1)/4
        enddo
        ij=i
        do j=1,m
          bij=0.
          idlcj=idlc4(j)
          ipocj=ipoc4(j)
          kp=ipocj
          kk=kp+idlcj-1
          do 1400k=kp,kk
            ip=ipec4(k)
            do l=n,1,-1
              ja(l)=mod(ip-1,4)+1
              ip=(ip-1)/4
             enddo
            pom=1.
            do l=1,n
              p=a(ia(l),ja(l))
              if(p.eq.0.) go to 1400
              pom=pom*p
            enddo
            bij=bij+pom
1400      continue
          b(ij)=bij
          ij=ij+m
        enddo
      enddo
      return
      end
