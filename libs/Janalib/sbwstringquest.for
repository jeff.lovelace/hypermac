      function SbwStringQuest(n,m)
      include 'fepc.cmn'
      character*256 SbwStringQuest
      id=m+SbwFr-1
      if(n.gt.SbwItemNMax(id)) then
        SbwStringQuest=' '
        go to 9999
      endif
      rewind SbwLn(id)
      do i=1,n
        read(SbwLn(id),FormA) SbwStringQuest
      enddo
9999  return
      end
