      subroutine FeRewriteIni(KeyWord,Value)
      include 'fepc.cmn'
      character*(*) Keyword,Value
      character*256 t256,p256
      logical EqIgCase
      call CopyFile(HomeDir(:idel(HomeDir))//
     1              MasterName(:idel(MasterName))//'.ini','#ini#.tmp')
      t256=MasterName(:idel(MasterName))//'.'//KeyWord(:idel(KeyWord))//
     1     ':'
      idt=idel(t256)
      t256=t256(:idt)//' '//Value(:idel(Value))
      lni=NextLogicNumber()
      open(lni,file='#ini#.tmp')
      lno=NextLogicNumber()
      open(lno,file=HomeDir(:idel(HomeDir))//
     1     MasterName(:idel(MasterName))//'.ini')
1000  read(lni,FormA,end=2000) p256
      if(EqIgCase(t256(:idt),p256(:idt))) then
        p256=t256
        t256=' '
      endif
      write(lno,FormA1)(p256(i:i),i=1,idel(p256))
      go to 1000
2000  if(t256.ne.' ') write(lno,FormA1)(t256(i:i),i=1,idel(t256))
      call DeleteFile('#ini#.tmp')
      call CloseIfOpened(lno)
      return
      end
