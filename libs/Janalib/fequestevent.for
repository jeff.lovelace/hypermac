      subroutine FeQuestEvent(id,ich)
      include 'fepc.cmn'
      character*43 ItRemainsOld
      external FeQuestExternalCheck,FeQuestExternalMake,
     1         FeQuestUpdateListek
      integer FeGetSystemTime,FeMenuNew
      data ItRemainsOld/'It remains XX sec - press space for pause'/,
     1     imm/0/
      save xwt,ywt
      MakeExternalCheck=0
      KartUpdateListekFlag=0
      go to 1000
      entry FeQuestEventWithCheck(id,ich,FeQuestExternalCheck,
     1                            FeQuestExternalMake)
      MakeExternalCheck=1
      KartUpdateListekFlag=0
      go to 1000
      entry FeQuestEventWithKartUpdate(id,ich,FeQuestUpdateListek)
      MakeExternalCheck=0
      KartUpdateListekFlag=1
1000  ich=0
      BlockedActiveObj=.false.
      call FeQuestActiveUpdate
      SbwClicked=0
      imm=0
      LastActiveQuest=id
      if(EventTypeSave.ne.0) then
        if(CheckType.eq.EventButton) then
          CheckNumberAbs=CheckNumber+ButtonFr-1
          if(ButtonState(CheckNumberAbs).eq.ButtonOn) then
            if(QuestCheck(id).ne.QuestCheckSave(id).and.
     1         QuestCheck(id).eq.0) then
              EventType=EventButton
              EventNumberAbs=ButtonOK
              go to 2100
            endif
            ActiveObj=ActButton*10000+CheckNumberAbs
            EdwActive=0
            SbwActive=0
            call FeButtonOff(CheckNumberAbs)
            call FeButtonActivate(CheckNumberAbs)
          endif
        else if(CheckType.eq.EventCtrl.or.CheckType.eq.EventASCII.or.
     1          CheckType.eq.EventKey.or.CheckType.eq.EventCrw) then
          EventType=0
        else if(EdwActive.gt.0.and.EventType.ne.EventEdw.and.
     1          EventType.ne.EventEdwUp.and.EventType.ne.EventEdwDown)
     2    then
          if(EventType.eq.0) then
            EventType=EventTypeSave
            EventNumber=EventNumberSave
          endif
          if(EventType.ne.EventMouse.and.EventType.ne.EventKey.and.
     1       EventType.ne.EventASCII.and.EventType.ne.EventCTRL.and.
     2       EventType.ne.EventEdwUp.and.EventType.ne.EventEdwDown.and.
     3       EventType.ne.EventResize.and.EventType.ne.EventKartSw) then
            EdwActive=0
          endif
        else if(CheckType.eq.EventKartSw.and.EventType.eq.0) then
          EventType=EventTypeSave
          EventNumber=EventNumberSave
          go to 2100
        else if(CheckType.eq.EventKey) then
          if(SbwActive.eq.0.and.EdwActive.eq.0.and.
     1       EventNumber.ne.JeReturn) EventType=0
        endif
      endif
      if(EventType.ne.0) then
        if(EventType.eq.EventEdw) then
          call FeQuestActiveObjOff
          EventNumberAbs=EventNumber+EdwFr-1
          EdwActive=EventNumberAbs
          ActiveObj=ActEdw*10000+EventNumberAbs
          call FeQuestActiveObjOn
        else if(EventType.eq.EventButton) then
          EventNumberAbs=EventNumber+ButtonFr-1
        else if(EventType.eq.EventCrw) then
          EventNumberAbs=EventNumber+CrwFr-1
        else if(EventType.eq.EventRolMenu) then
          EventNumberAbs=EventNumber+RolMenuFr-1
        endif
        go to 2100
      endif
      if(VasekTest.ne.0) call FeQuestCheckForDuplicity
2000  if(MakeExternalCheck.gt.0)
     1  call FeQuestExternalCheck(FeQuestExternalMake)
      if(MakeExternalCheck.eq.-1) MakeExternalCheck=1
      if(WaitTime.gt.0) then
        if(imm.eq.0.and.RemainTime.ge.WaitTime) then
          imm=-1
          ItRemains   (33:)='for pause'
          ItRemainsOld(33:)='for pause'
        endif
      else
        imm=0
      endif
2050  call FeEvent(imm)
      if(WaitTime.gt.0) then
        if(EventType.eq.EventSystem.and.EventNumber.eq.JeTimeOut) then
          PlivejVyrez=0
          PripravVyrez=0
          if(ButtonOK.gt.0) then
            EventType=EventButton
            EventNumberAbs=ButtonOK
          else
            go to 9000
          endif
        else if(EventType.eq.0.and.EventNumber.eq.0) then
          imm=-2
          xwt=(QuestXMin(id)+QuestXMax(id))*.5
          ywt=QuestYMin(id)+10.
          write(ItRemains(12:13),'(i2)')
     1      nint(float(RemainTime)*.001+.5)
          PlivejVyrez=1
          PripravVyrez=1
          go to 2070
        else if(EventType.eq.EventASCII.and.
     1          char(EventNumber).eq.' ') then
          if(imm.lt.0) then
            imm=0
            ItRemains(33:)='to continue'
          else
            imm=-2
            ItRemains(33:)='for pause'
            StartTime=RemainTime-WaitTime+FeGetSystemTime()
          endif
          go to 2070
        endif
        PlivejVyrez=0
        PripravVyrez=0
        go to 2100
2070    if(ItRemainsOld.ne.ItRemains) then
          call FeDeferOutput
          call FeTextErase(0,xwt,ywt,ItRemainsOld,'C',LightGray)
          call FeOutSt(0,xwt,ywt,ItRemains,'C',Black)
          ItRemainsOld=ItRemains
          call FeReleaseOutput
          call FeDeferOutput
        endif
        go to 2050
      endif
2100  EdwLastCheck=EdwLastCheck+1
      if(EdwActive.gt.0) then
        EdwLastActive=EdwActive-EdwFr+1
        if((EventType.eq.EventEdw.and.EventNumberAbs.eq.EdwActive).or.
     1     (EventType.eq.EventKey.and.EventNumber.ne.JeTab.and.
     2      EventNumber.ne.JeShiftTab.and.EventNumber.ne.JeReturn).or.
     3     EventType.eq.EventEdwSel.or.
     4     EventType.eq.EventCtrl.or.EventType.eq.EventASCII.or.
     5     EventType.eq.EventMouse.or.
     6     EventType.eq.EventSystem.or.
     7     (EventType.eq.EventButton.and.
     8      EventNumberAbs.eq.ButtonEsc).or.
     9     (EventType.eq.EventButton.and.
     a      EventNumberAbs.eq.ButtonCancel).or.
     1     EdwLastCheck.ne.1) go to 2110
        if(EdwState(EdwActive).eq.EdwOpened.and.
     1     EventType.ne.EventResize) then
          call FeCheckStringEdw(EdwActive,ich)
          if(ich.eq.2) then
            ich=0
            CheckType=EventEdw
            CheckNumber=EdwActive-EdwFr+1
            CheckNumberAbs=EdwActive
            call FeReleaseOutput
            call FeDeferOutput
            go to 9999
          endif
        endif
      endif
      call FeReleaseOutput
      call FeDeferOutput
2110  if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        i=ActiveObj/10000
        if(i.eq.EventButton) then
          EventType=i
          EventNumberAbs=mod(ActiveObj,10000)
          EventNumber=mod(ActiveObj,10000)-ButtonFr+1
          go to 2110
        else if(i.eq.EventSbw) then
          EventType=0
          EventNumber=0
          go to 2100
        endif
      endif
      if((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonOk).or.
     1   ((EventType.eq.EventKey.and.EventNumber.eq.JeReturn).and.
     2     ButtonOK.gt.0)) then
        call FeButtonOnAction(ButtonOk)
        EventType=EventButton
        EventNumberAbs=ButtonOk
        EventNumber=ButtonOk-ButtonFr+1
        if(QuestCheck(id).ne.0) go to 9000
        ich=0
        go to 5000
      endif
2150  EdwLastCheck=0
      if(EventType.eq.EventASCII.and.EventNumber.eq.ichar(' ').and.
     1   EdwActive.eq.0) then
        EventType=ActiveObj/10000
        EventNumberAbs=mod(ActiveObj,10000)
        if(EventType.eq.EventButton) then
          EventNumber=EventNumberAbs-ButtonFr+1
        else if(EventType.eq.EventCrw) then
          EventNumber=EventNumberAbs-CrwFr+1
        else if(EventType.eq.EventRolMenu) then
          EventNumber=EventNumberAbs-RolMenuFr+1
        else if(EventType.eq.EventSbw) then
          EventNumber=EventNumberAbs-SbwFr+1
        endif
        go to 2100
      else if(((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonEsc).or.
     2         ((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).and.
     3          .not.WizardMode)).and.ButtonEsc.gt.0) then
        call FeButtonOnAction(ButtonEsc)
        ich=1
        go to 5000
      else if(((EventType.eq.EventButton.and.
     1         EventNumberAbs.eq.ButtonCancel).or.
     2         ((EventType.eq.EventKey.and.EventNumber.eq.JeEscape).and.
     3           WizardMode)).and.ButtonCancel.gt.0) then
        call FeButtonOnAction(ButtonCancel)
        ich=-1
        go to 5000
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
        go to 9000
      else if(EventType.eq.EventKartButt) then
        ib=EventNumberAbs
        call FeButtonOnAction(ib)
        go to 9000
      else if(EventType.eq.EventButton) then
        ib=EventNumberAbs
        call FeButtonOnAction(ib)
        go to 9000
      else if(EventType.eq.EventGlobal) then
        go to 9000
      else if(EventType.eq.EventKartSw) then
        if(QuestCheck(KartId).gt.0) then
          QuestCheck(KartId)=-1
          CheckType=EventKartSw
          CheckNumber=KartId-1
          CheckNumberAbs=KartId-1
          go to 9999
        else if(QuestCheck(KartId).lt.0) then
          QuestCheck(KartId)=1
        endif
        i=KartId
        call FePrepniListek(EventNumber)
        if(KartUpdateListekFlag.ne.0) call FeQuestUpdateListek(i)
        go to 2000
      else if(EventType.eq.EventKey.and.
     1        (EventNumber.eq.JeTab.or.EventNumber.eq.JeShiftTab)) then
        i=LocateInIntArray(ActiveObj,ActiveObjList(ActiveObjFr),
     1                     ActiveObjTo-ActiveObjFr+1)
        call FeQuestActiveObjOff
2200    if(i.gt.0) then
          if(EventNumber.eq.JeTab) then
            j=i+ActiveObjFr
            if(j.gt.ActiveObjTo) then
              j=ActiveObjFr
              i=1
            endif
          else
            j=i+ActiveObjFr-2
            if(j.lt.ActiveObjFr) then
              j=ActiveObjTo
              i=ActiveObjTo-ActiveObjFr+1
            endif
          endif
        else
          j=ActiveObjFr
        endif
        ActiveObj=ActiveObjList(j)
        call FeQuestActiveObjOn
        if(ActiveObj.lt.0) then
          if(EventNumber.eq.JeTab) then
            i=i+1
          else
            i=i-1
          endif
          go to 2200
        endif
        EventNumber=0
      else if(EventType.eq.EventCrw) then
        call FeQuestActiveObjOff
        i=EventNumberAbs
        if(CrwExGr(i).eq.0) then
          if(CrwState(i).eq.CrwOff) then
            call FeCrwOnAction(i)
          else
            call FeCrwOffAction(i)
          endif
        else
          do j=CrwFr,CrwTo
            if(CrwExGr(j).ne.CrwExGr(i).or.CrwState(j).eq.CrwClosed.or.
     1                                     CrwState(j).eq.CrwDisabled)
     2        cycle
            if(j.eq.i) then
              if(.not.CrwLogic(j)) then
                call FeCrwOnAction(j)
              endif
            else
              if(CrwLogic(j)) then
                call FeCrwOff(j)
              endif
            endif
          enddo
        endif
        if(CrwCheck(i).ne.0) go to 9000
      else if(EventType.eq.EventRolMenu) then
        i=EventNumberAbs
        call FeRolMenuOnAction(i)
        if(RolMenuType(i).eq.RolMenuUp) then
          ypom=RolMenuTextYMin(i)
        else
          ypom=RolMenuTextYMin(i)-
     1         float(RolMenuNumber(i)-1)*MenuLineWidth
        endif
        n=FeMenuNew(RolMenuTextXMin(i),ypom,
     1    RolMenuButtXMin(i)-RolMenuTextXMin(i)-3.,
     2    RolMenuText(1,i),RolMenuEnable(1,i),1,RolMenuNumber(i),1,1)
        if(n.gt.0) call FeRolMenuWriteText(i,n)
        call FeRolMenuOff(i)
        if(RolMenuCheck(i).ne.0) go to 9000
      else if(EventType.eq.EventRolMenuUnlock) then
        go to 9000
      else if(EventType.eq.EventCrwUnlock) then
        go to 9000
      else if(EventType.eq.EventEdwUnlock) then
        go to 9000
      else if(EventType.eq.EventResize) then
        go to 9000
      endif
      call FeQuestEventSbw(izpet)
      if(izpet.ne.0) go to 3000
      call FeQuestEventEdw(izpet)
      if(izpet.ne.0) go to 3000
      go to 3100
3000  if(izpet.eq.2000) then
        go to 2000
      else if(izpet.eq.2100) then
        go to 2100
      else if(izpet.eq.2110) then
        go to 2110
      else if(izpet.eq.9000) then
        go to 9000
      else if(izpet.eq.9999) then
        go to 9999
      endif
3100  if(EventType.eq.EventMouse) then
        if(CheckMouse) go to 9000
      else if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1        EventType.eq.EventAlt.or.EventType.eq.EventCtrl) then
        if(CheckKeyboard.and.EventNumber.ne.0.and.
     1     (EventType.ne.EventCtrl.or.(char(EventNumber).ne.'V'.and.
     2                                 char(EventNumber).ne.'C'.and.
     3                                 char(EventNumber).ne.'X'))) then
          if(EdwActive.gt.0) then
            if(EdwState(EdwActive).eq.EdwOpened)
     1        call FeCheckStringEdw(EdwActive,ich)
          endif
          go to 9000
        endif
      else if(EventType.eq.EventResize) then
        CheckType=EventResize
        CheckNumber=0
      endif
      go to 2000
5000  CheckType=0
      CheckNumber=0
      CheckNumberAbs=0
      go to 9999
9000  CheckType=EventType
      CheckNumber=EventNumber
      CheckNumberAbs=EventNumberAbs
      if(EdwActive.gt.0.and.CheckType.eq.EventEdw)
     1  EdwLastCheck=EdwLastCheck+1
9999  if(EventType.ne.EventMouse) then
        EventTypeSave  =EventType
        EventNumberSave=EventNumber
        EventNumberAbsSave=EventNumberAbs
      else
        EventTypeSave  =0
        EventNumberSave=0
        EventNumberAbsSave=0
      endif
      EventType=0
      EventNumber=0
      QuestCheckSave(id)=QuestCheck(id)
      BlockedActiveObj=.true.
      return
      end
