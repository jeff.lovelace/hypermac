      subroutine Roleta(x,y,n,
     1                  RolI,RolM,RolZ,RolA,RolN,
     2                  SubI,SubM,SubZ,SubA,SubN,
     3                  Action)
      include 'fepc.cmn'
      integer RolN,SubN(n),RolI,RolIo,SubI,SubIo,Color1,Color2
      character*(*) RolM(RolN),action,RolZ,SubM(n,RolN),SubZ(n)
      logical RolA(RolN),SubA(n,RolN)
      common/subrol/ SubState,xsub,ysub,xdsub,ydsub
      integer SubState
      save xrol,xdrol,yrol,ydrol,RolIo,SubIo
      data RolIo/0/
      if(Action.eq.'create') then
        call FeDeferOutput
        xrol=x+2.
        yrol=y-2.
        ydrol=float(RolN)*MenuLineWidth
        xdrol=0.
        xpom=5.
        do i=1,RolN
          if(SubN(i).gt.0) xpom=30.
          xdrol=max(FeTxLengthUnder(RolM(i)),xdrol)
        enddo
        xdrol=xdrol+xpom+2.*EdwMarginSize
        xrol=anint(xrol)
        xdrol=anint(xdrol)
        yrol=anint(yrol)
        ydrol=anint(ydrol)
        call FeSaveImage(xrol-2.,xrol+xdrol+2.,yrol-ydrol-2.,yrol+2.,
     1                   'roleta.bmp')
        call FeFillRectangle(xrol-1.,xrol+xdrol+1.,yrol+1.,
     1                       yrol-ydrol-1.,4,0,0,LightGray)
        call FeDrawFrame(xrol,yrol-ydrol,xdrol,ydrol,2.,Gray,
     1                   White,Black,.false.)
        ypom=yrol
        do i=1,RolN
          if(RolA(i)) then
            if(i.eq.RolI) then
              Color1=White
              Color2=DarkBlue
              k=1
            else
              Color1=Black
              Color2=LightGray
              k=0
            endif
          else
            if(i.eq.RolI) then
              Color1=Gray
              Color2=DarkBlue
              k=1
            else
              Color1=WhiteGray
              Color2=LightGray
              k=0
            endif
          endif
          call FeWrMenuItemO(xrol,ypom,xdrol,RolM(i),RolZ(i:i),
     1                       SubM(1,i),SubZ(i),SubA(1,i),SubN(i),
     2                       Color1,Color2,k)
          if(VasekTest.eq.1.and.index(RolZ(:i-1),RolZ(i:i)).gt.0) then
            EdwString(1)='Duplicate identification : '//RolZ
            call FeMsgOut(-1.,-1.,EdwString(1))
          endif
          ypom=ypom-MenuLineWidth
        enddo
      else if(Action.eq.'remove') then
        call FeDeferOutput
        if(SubState.eq.1) then
          call FeLoadImage(xsub-2.,xsub+xdsub+2.,ysub-ydsub-2.,ysub+2.,
     1                     'subrol.bmp',0)
          SubState=0
        endif
        call FeLoadImage(xrol-2.,xrol+xdrol+2.,yrol-ydrol-2.,yrol+2.,
     1                   'roleta.bmp',0)
        RolI=0
        SubI=0
      else if(Action.eq.'test') then
        if(RolI.gt.0.and.SubState.ne.0) then
          if(Xpos.ge.xsub.and.Xpos.le.xsub+xdsub.and.
     1       Ypos.ge.ysub-ydsub.and.Ypos.le.ysub) then
            l=min(ifix((ysub-Ypos)/MenuLineWidth)+1,SubN(RolI))
          else if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1            Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
            l=0
          else
            l=-1
          endif
          if(l.ne.SubI) then
            call FeDeferOutput
            if(SubI.gt.0) then
              if(SubA(SubI,RolI)) then
                Color1=Black
                Color2=LightGray
              else
                Color1=WhiteGray
                Color2=LightGray
              endif
              call FeWrMenuItem(xsub,ysub-float(SubI-1)*MenuLineWidth,
     1                          xdsub,SubM(SubI,RolI),
     2                          SubZ(RolI)(SubI:SubI),
     3                          Color1,Color2)
            endif
            if(l.gt.0) then
              if(SubA(l,RolI)) then
                Color1=White
                Color2=DarkBlue
              else
                Color1=Gray
                Color2=DarkBlue
              endif
              call FeWrMenuItem(xsub,ysub-float(l-1)*MenuLineWidth,
     1                          xdsub,SubM(l,RolI),SubZ(RolI)(l:l),
     2                          Color1,Color2)
            endif
          endif
          SubI=l
        endif
        if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1     Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
          i=min(ifix((yrol-Ypos)/MenuLineWidth)+1,RolN)
        else if(SubState.eq.0) then
          i=0
        else
          i=RolI
        endif
        if(i.ne.RolI.and.SubI.eq.0) then
          call FeDeferOutput
          if(RolI.gt.0) then
            if(RolA(RolI)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolI-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolI),RolZ(RolI:RolI),
     3                         SubM(1,RolI),SubZ(RolI),SubA(1,RolI),
     4                         SubN(RolI),Color1,Color2,-1)
          endif
          if(i.gt.0) then
            if(RolA(i)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItemO(xrol,yrol-float(i-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(i),RolZ(i:i),
     3                         SubM(1,i),SubZ(i),SubA(1,i),
     4                         SubN(i),Color1,Color2,1)
          endif
          RolI=i
        endif
      else if(Action.eq.'check') then
        if(Xpos.ge.xrol.and.Xpos.le.xrol+xdrol.and.
     1     Ypos.le.yrol.and.Ypos.ge.yrol-ydrol) then
          RolI=min(ifix((yrol-Ypos)/MenuLineWidth)+1,RolN)
        else
          RolI=0
        endif
        go to 9999
      else if(Action.eq.'switch') then
        if(RolIo.ne.RolI) then
          call FeDeferOutput
          if(RolIo.gt.0) then
            if(RolA(RolIo)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolIo-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolIo),RolZ(RolIo:RolIo),
     3                         SubM(1,RolIo),SubZ(RolIo),
     4                         SubA(1,RolIo),SubN(RolIo),Color1,Color2,
     5                         -1)
          endif
          if(RolI.gt.0) then
            if(RolA(RolI)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItemO(xrol,yrol-float(RolI-1)*MenuLineWidth,
     1                         xdrol,
     2                         RolM(RolI),RolZ(RolI:RolI),
     3                         SubM(1,RolI),SubZ(RolI),
     4                         SubA(1,RolI),SubN(RolI),Color1,Color2,1)
          endif
        else if(SubIo.ne.SubI) then
          call FeDeferOutput
          if(SubIo.gt.0) then
            if(SubA(SubIo,RolI)) then
              Color1=Black
              Color2=LightGray
            else
              Color1=WhiteGray
              Color2=LightGray
            endif
            call FeWrMenuItem(xsub,ysub-float(SubIo-1)*MenuLineWidth,
     1                        xdsub,
     2                        SubM(SubIo,RolI),SubZ(RolI)(SubIo:SubIo),
     3                        Color1,Color2)
          endif
          if(SubI.gt.0) then
            if(SubA(SubI,RolI)) then
              Color1=White
              Color2=DarkBlue
            else
              Color1=Gray
              Color2=DarkBlue
            endif
            call FeWrMenuItem(xsub,ysub-float(SubI-1)*MenuLineWidth,
     1                        xdsub,
     2                        SubM(SubI,RolI),SubZ(RolI)(SubI:SubI),
     3                        Color1,Color2)
          endif
        endif
      endif
      RolIo=RolI
      SubIo=SubI
9999  return
      end
