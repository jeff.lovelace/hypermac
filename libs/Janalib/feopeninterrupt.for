      subroutine FeOpenInterrupt(xmi,ymi,text)
      include 'fepc.cmn'
      character*(*) text
      logical Konec,WizardModeOld
      integer FeGetSystemTime
      save istart,nLbl,nButtInterrupt,id,WizardModeOld
      WizardModeOld=WizardMode
      WizardMode=.false.
      xd=max(50.,FeTxLength(text)+10.)
      id=NextQuestid()
      call FeQuestCreate(id,xmi,ymi,xd,2,' ',0,LightGray,-1,-1)
      call FeQuestLblMake(id,5.,1,Text,'L','N')
      nLbl=LblLastMade
      Cislo='%Interrupt'
      dpom=FeTxLength(Cislo)+20.
      call FeQuestButtonMake(id,(xd-dpom)*.5,2,dpom,ButYd,Cislo)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtInterrupt=ButtonLastMade
      go to 2000
      entry FeOutputInterrupt(Text)
      call FeQuestLblChange(nLbl,Text)
2000  istart=FeGetSystemTime()
      go to 9000
      entry FeEventInterrupt(Konec)
      call FeEvent(id)
      konec=(EventType.eq.EventButton.and.EventNumber.eq.1).or.
     1      (EventType.eq.EventKey.and.EventNumber.eq.JeEscape)
      ipom=FeGetSystemTime()
      icas=ipom-istart
      if(icas.ge.50) then
        call FeQuestLblOn(nLbl)
        istart=ipom
        go to 9000
      else
        go to 9999
      endif
      entry FeCloseInterrupt
      call FeQuestRemove(id)
9000  if(DeferredOutput) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  WizardMode=WizardModeOld
      return
      end
