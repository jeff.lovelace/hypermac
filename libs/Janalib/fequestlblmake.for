      subroutine FeQuestLblMake(id,xt,n1,Text,Justify,Font)
      include 'fepc.cmn'
      integer Color
      character*(*) Text
      character*1 Justify,Font
      integer UseTabsOld,ColorErase
      logical EqIgCase,Remove
      yt=QuestYPosition(id,n1)
      go to 1000
      entry FeQuestAbsLblMake(id,xt,yti,Text,Justify,Font)
      yt=yti
1000  LblTo=LblTo+1
      LblLastMade=LblTo-LblFr+1
      QuestLblTo(id)=LblTo
      LblText(LblTo)=Text
      LblJustify(LblTo)=Justify
      LblFont(LblTo)=Font
      LblState(LblTo)=LblOn
      LblXPos(LblTo)=xt
      LblYPos(LblTo)=yt
      LblTabs(LblTo)=UseTabs
      i=LblTo
      go to 2000
      entry FeQuestLblOn(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved.or.LblState(i).eq.LblOn) go to 9999
2000  LblState(i)=LblOn
      Color=Black
      go to 3000
      entry FeQuestLblOff(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved) go to 9999
      LblState(i)=LblOff
      Color=LightGray
      go to 3000
      entry FeQuestLblDisable(iwa)
      i=iwa+LblFr-1
      if(LblState(i).eq.LblRemoved) go to 9999
      LblState(i)=LblDisabled
      Color=White
      go to 3000
      entry FeQuestLblRemove(iwa)
      i=iwa+LblFr-1
      LblState(i)=LblRemoved
      Color=LightGray
      Remove=.true.
      go to 3000
      entry FeQuestLblChange(iwa,Text)
      i=iwa+LblFr-1
      UseTabsOld=UseTabs
      UseTabs=LblTabs(i)
      if(LblState(i).eq.LblOn) then
        if(EqIgCase(LblFont(i),'B')) call FeBoldFont
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),LightGray)
        if(EqIgCase(LblFont(i),'B')) call FeNormalFont
      endif
      LblState(i)=LblOn
      LblText(i)=Text
      Color=Black
3000  UseTabsOld=UseTabs
3100  if(EqIgCase(LblFont(i),'B')) call FeBoldFont
      UseTabs=LblTabs(i)
      ColorErase=QuestColorFill(LastActiveQuest)
      if(ColorErase.le.0) ColorErase=LightGray
      if(Color.ne.ColorErase) then
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),ColorErase)
        call FeOutSt(LastActiveQuest,LblXPos(i),LblYPos(i),LblText(i),
     1               LblJustify(i),Color)
      else
        call FePureTextErase(LastActiveQuest,LblXPos(i),LblYPos(i),
     1                       LblText(i),LblJustify(i),ColorErase)
      endif
      if(EqIgCase(LblFont(i),'B')) call FeNormalFont
      UseTabs=UseTabsOld
9999  return
      end
