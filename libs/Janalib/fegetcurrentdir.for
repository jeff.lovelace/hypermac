      subroutine FeGetCurrentDir
      include 'fepc.cmn'
      character*80 TmpFile
      character Znak
      if(OpSystem.le.0) then
        call FeDirName(CurrentDir)
        Znak=Obrlom
      else if(OpSystem.eq.1) then
        TmpFile='jcmd'
        call CreateTmpFile(TmpFile,i,0)
        call FeTmpFilesAdd(TmpFile)
        call FeSystem(ObrLom//'pwd > '//TmpFile)
        Znak='/'
        ln=NextLogicNumber()
        call OpenFile(ln,TmpFile,'formatted','old')
        read(ln,FormA128) CurrentDir
        call CloseIfOpened(ln)
        call DeleteFile(TmpFile)
        call FeTmpFilesClear(TmpFile)
      endif
      i=idel(CurrentDir)
      if(CurrentDir(i:i).ne.Znak) CurrentDir=CurrentDir(:i)//Znak
      return
      end
