      subroutine FeMsgShow(id,xmi,ymi,Poprve)
      include 'fepc.cmn'
      dimension xm(10),ym(10),xd(10),yd(10),NInfoMax(10)
      character*80 TextOld(20,10),MsgTmp(10),Ven
      logical Poprve,MsgUsed(10)
      integer UseTabsIn
      save xm,ym,xd,yd,TextOld,MsgTmp,NInfoMax
      equivalence (TextOld,Ven)
      data MsgUsed/10*.false./
      UseTabsIn=UseTabs
      MsgUsed(id)=.true.
      if(BatchMode.or.Console) then
        do i=1,Ninfo
          Ven=TextInfo(i)
          idl=idel(Ven)
          if(index(Ven,'^').gt.0) then
            do j=idel(Ven),1,-1
              if(Ven(j:j).eq.':'.or.Ven(j:j).eq.'^'.or.Ven(j:j).eq.' ')
     1          then
                Ven(j:j)=' '
              else
                exit
              endif
            enddo
          endif
          call FeLstWriteLine(Ven,-1)
          if(LogLn.gt.0) write(LogLn,FormA) Ven(:idel(Ven))
        enddo
      else
        if(SilentRun) go to 9999
        if(.not.DeferredOutput) call FeDeferOutput
        call FeEvent(1)
        if(Poprve) NInfoMax(id)=NInfo
        xdp=0.
        do i=1,Ninfo
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=UseTabsIn
          endif
          xdp=max(FeTxLength(TextInfo(i))+10.,xdp)
          if(Poprve) TextOld(i,id)=' '
        enddo
        if(Poprve.or.xdp.gt.xd(id)) then
          if(.not.Poprve)
     1      call FeLoadImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     2                       ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     3                       MsgTmp(id),0)
          xd(id)=xdp
          if(xmi.lt.0.) then
c            xm(id)=XCenGrWin-xd(id)*.5
            xm(id)=XCenBasWin-xd(id)*.5
          else
            xm(id)=xmi-xd(id)
          endif
          if(Poprve) then
            yd(id)=float(Ninfo)*16.+10.
            if(ymi.lt.0.) then
              ym(id)=YCenGrWin-yd(id)*.5
            else
              ym(id)=ymi-yd(id)
            endif
            MsgTmp(id)='jmsg'
            if(OpSystem.le.0) then
              call CreateTmpFile(MsgTmp(id),i,1)
            else
              call CreateTmpFileName(MsgTmp(id),id)
            endif
          endif
          call FeSaveImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     1                     ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     2                     MsgTmp(id))
          call FeFillRectangle(xm(id)-1.,xm(id)+xd(id)+1.,ym(id)-1.,
     1                         ym(id)+yd(id)+1.,4,0,0,LightGray)
          call FeDrawFrame(xm(id),ym(id),xd(id),yd(id),FrameWidth,Gray,
     1                     White,Black,LastQuest.ne.0)
        endif
        ypom=ym(id)+yd(id)-12.
        xpom=xm(id)+5.
        do i=1,NinfoMax(id)
          if(NTabs(i).gt.0) then
            UseTabs=i
          else
            UseTabs=UseTabsIn
          endif
          j=idel(TextInfo(i))
          if(index(TextInfo(i),'^').gt.0) then
            do j=idel(TextInfo(i)),1,-1
              if(TextInfo(i)(j:j).eq.':'.or.TextInfo(i)(j:j).eq.'^')
     1          then
                TextInfo(i)(j:j)=' '
              else
                exit
              endif
            enddo
          endif
          if(i.le.NInfo) then
            call FeRewritePureString(0,xpom,ypom,TextOld(i,id),
     1                               TextInfo(i),'L',LightGray,Black)
          else
            call FeRewritePureString(0,xpom,ypom,TextOld(i,id),
     1                               TextOld(i,id),'L',LightGray,Black)
          endif
          TextOld(i,id)=TextInfo(i)
          ypom=ypom-16.
        enddo
        if(DeferredOutput) then
          call FeReleaseOutput
          call FeDeferOutput
        endif
      endif
      UseTabs=UseTabsIn
      go to 9999
      entry FeMsgClear(id)
      if(SilentRun) go to 9999
      if(MsgUsed(id).and..not.BatchMode.and..not.Console)
     1  call FeLoadImage(xm(id)-FrameWidth,xm(id)+xd(id)+FrameWidth,
     2                   ym(id)-FrameWidth,ym(id)+yd(id)+FrameWidth,
     3                   MsgTmp(id),0)
      MsgUsed(id)=.false.
9999  return
      end
