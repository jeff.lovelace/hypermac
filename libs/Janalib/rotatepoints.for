      subroutine RotatePoints(px,py,n,xc,yc,phi)
      include 'fepc.cmn'
      real px(*),py(*),phi,sphi,cphi,pomx,pomy,xc,yc
      integer n,i
      if(phi.eq.0.or.n.le.0) go to 9900
      sphi=sin(phi*torad)
      cphi=cos(phi*torad)
      do i=1,n
        px(i)=px(i)-xc
        py(i)=py(i)-yc
        pomx=px(i)*cphi-py(i)*sphi
        pomy=px(i)*sphi+py(i)*cphi
        px(i)=pomx+xc
        py(i)=pomy+yc
      enddo
9900  continue
      end
