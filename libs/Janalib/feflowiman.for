      subroutine FeFlowIMan(xp,yp,yk,y1,y2,yshift)
      include 'fepc.cmn'
      integer BarvaNad,BarvaPod
      if(abs(yshift).lt.y2-y1) then
        if(yshift.gt.0.) then
          BarvaNad=White
          BarvaPod=LightGray
        else if(yshift.lt.0.) then
          BarvaPod=White
          BarvaNad=LightGray
        else
          go to 9999
        endif
        call FeFillRectangle(xp+1.,xp+7.,y1,y1+yshift,4,0,0,BarvaPod)
        call FeFillRectangle(xp+1.,xp+7.,y2,y2+yshift,4,0,0,BarvaNad)
        y1=y1+yshift
        y2=y2+yshift
      else
        call FeFillRectangle(xp+1.,xp+7.,y1,y2,4,0,0,LightGray)
        y1=y1+yshift
        y2=y2+yshift
        call FeFillRectangle(xp+1.,xp+7.,y1,y2,4,0,0,White)
      endif
9999  return
      end
