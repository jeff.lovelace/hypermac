      subroutine MinMultMaxFract(Ia,n,MinMult,MaxFract)
      include 'fepc.cmn'
      dimension Ia(n),MaxArr(30),MinArr(30)
      integer PrimeExp(30)
      logical Poprve
      Poprve=.true.
      do i=1,n
        if(Ia(i).ne.0) then
          call SplitIntoPrimes(iabs(Ia(i)),PrimeExp)
          if(Poprve) then
            call CopyVekI(PrimeExp,MaxArr,30)
            call CopyVekI(PrimeExp,MinArr,30)
            Poprve=.false.
          else
            do j=1,30
              MaxArr(j)=max(MaxArr(j),PrimeExp(j))
              MinArr(j)=min(MinArr(j),PrimeExp(j))
            enddo
          endif
        endif
      enddo
      MinMult=1
      MaxFract=1
      do i=1,30
        MinMult=MinMult*PrimeNumbers(i)**MaxArr(i)
        MaxFract=MaxFract*PrimeNumbers(i)**MinArr(i)
      enddo
      return
      end
