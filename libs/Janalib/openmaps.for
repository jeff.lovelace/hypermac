      subroutine OpenMaps(ln,FileName,lrec,Key)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileName
      character*80 t80
      character*40 t40
      logical   ExistFile,ef
      logical   opened
      ErrFlag=0
      inquire(file=FileName,opened=opened,err=8000)
      if(opened) then
        t80='attempt to open already opened file'
        goto 9100
      else
        ef=ExistFile(FileName)
      endif
      if(Key.eq.0) then
        if(.not.ef) then
          t80='cannot be opened as an old file, it doesn''t exist'
          go to 9000
        endif
        open(ln,file=FileName,form='unformatted',status='old',
     1       access='direct',recl=35*RecLenFacUnform,err=8000)
        read(ln,rec=1,err=8000)(lrec,i=1,7)
        call CloseIfOpened(ln)
        open(ln,file=FileName,form='unformatted',status='unknown',
     1       access='direct',recl=max(lrec,35)*RecLenFacUnform,err=8000)
      else
        call DeleteFile(FileName)
        open(ln,file=FileName,form='unformatted',status='unknown',
     1       access='direct',recl=max(lrec,35)*RecLenFacUnform,err=8100)
      endif
      go to 9999
8000  t80='the file exists but it is probably corrupted'
      go to 9000
8100  t80='undeterminable problem'
9000  ErrFlag=1
9100  call FeCutName(FileName,t40,40,CutTextFromLeft)
      if(ErrFlag.eq.0) then
        i=Warning
      else
        i=SeriousError
      endif
      call FeChybne(-1.,-1.,'the file "'//t40(:idel(t40))//'"',t80,i)
9999  return
      end
