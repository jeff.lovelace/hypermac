      subroutine FeCrw
      include 'fepc.cmn'
      integer ColorLU,ColorRD,ColorCrw,ColorObtah,ColorSel,
     1        ColorBkg,ColorFrg
      character*20 Veta
      logical YesNo
      entry FeCrwMake(id,idc,xm,ym,xd,yd)
      if(CrwType(id).eq.CrwTypeBMP) then
        pom=1./EnlargeFactor
      else
        pom=1.
      endif
      CrwXMin(id)=xm
      CrwXMax(id)=xm+xd*pom
      CrwYMin(id)=ym
      CrwYMax(id)=ym+yd*pom
      if(idc.gt.0) then
        CrwXMin(id)=CrwXMin(id)+QuestXMin(idc)
        CrwXMax(id)=CrwXMax(id)+QuestXMin(idc)
        CrwYMin(id)=CrwYMin(id)+QuestYMin(idc)
        CrwYMax(id)=CrwYMax(id)+QuestYMin(idc)
      endif
      CrwState(id)=CrwClosed
      CrwLabelTabs(id)=UseTabs
      go to 9999
      entry FeCrwOpen(id,YesNo)
      if(YesNo) then
        go to 2000
      else
        go to 1000
      endif
      entry FeCrwOff(id)
1000  if(CrwState(id).eq.CrwOff.or.CrwState(id).eq.CrwRemoved)
     1  go to 9999
      CrwState(id)=CrwOff
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,White)
      else
        ColorRD=Gray
        ColorLU=White
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=White
        ColorFrg=Black
        go to 6000
      endif
      go to 9999
      entry FeCrwOn(id)
2000  if(CrwState(id).eq.CrwOn.or.CrwState(id).eq.CrwRemoved) go to 9999
      CrwState(id)=CrwOn
      CrwLogic(id)=.true.
      if(CrwExGr(id).eq.0) then
        if(CrwState(id).ne.CrwOff) call FeCrwBasic(id,White)
        xdp=CrwXmax(id)-CrwXmin(id)
        ydp=CrwYmax(id)-CrwYmin(id)
        xu(1)=CrwXmin(id)+xdp*.1
        yu(1)=(CrwYmax(id)+CrwYmin(id))*.5
        xu(2)=CrwXmin(id)+xdp*.333333
        yu(2)=CrwYmin(id)+ydp*.1
        xu(3)=CrwXmax(id)-xdp*.1
        yu(3)=CrwYmax(id)-ydp*.1
        xu(4)=xu(2)
        yu(4)=CrwYmin(id)+ydp*.333333
        call FePolygon(xu,yu,4,4,0,0,Black)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=White
        ColorFrg=Black
        go to 6000
      endif
      go to 9999
      entry FeCrwLock(id)
      if(CrwState(id).eq.CrwClosed.or.CrwState(id).eq.CrwRemoved.or.
     1   CrwState(id).eq.CrwDisabled.or.CrwState(id).eq.CrwLocked)
     3  go to 9999
      CrwState(id)=CrwLocked
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,LightYellow)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=LightYellow
        ColorFrg=LightYellow
        go to 6000
      endif
      go to 9999
      entry FeCrwDisable(id)
      if(CrwState(id).eq.CrwDisabled) go to 9999
      CrwState(id)=CrwDisabled
      CrwLogic(id)=.false.
      if(CrwExGr(id).eq.0) then
        call FeCrwBasic(id,WhiteGray)
      else
        ColorRD=White
        ColorLU=Gray
        ColorCrw=LightGray
        ColorObtah=Gray
        ColorBkg=WhiteGray
        ColorFrg=WhiteGray
        go to 6000
      endif
      go to 9999
      entry FeCrwClose(id)
      if(CrwState(id).eq.CrwClosed.or.
     1   CrwState(id).eq.CrwRemoved) go to 9999
      ir=0
      go to 5000
      entry FeCrwRemove(id)
      if(CrwState(id).eq.CrwRemoved) go to 9999
      ir=1
5000  dx=3.
      pom1=anint(CrwXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(CrwXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(CrwYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(CrwYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      if(ir.eq.1) then
        CrwState(id)=CrwRemoved
      else
        CrwState(id)=CrwClosed
      endif
      CrwLogic(id)=.false.
      go to 9999
6000  xpom=(CrwXMin(id)+CrwXMax(id))*.5
      ypom=(CrwYMin(id)+CrwYMax(id))*.5
      if(CrwType(id).eq.CrwTypeLetter) then
        if(CrwLogic(id)) then
          dx =1.
          dxx=1.
        else
          dx=2.
          dxx=0.
        endif
        xLD=anint(CrwXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
        xRU=anint(CrwXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
        wp=xRU-xLD
        yLD=anint(CrwYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
        yRU=anint(CrwYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
        hp=yRU-yLD
        call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorCrw)
        call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                   .false.)
        xpom=(CrwXMin(id)+CrwXMax(id))*.5
        ypom=(CrwYMin(id)+CrwYMax(id))*.5
        if(CrwLabel(id)(3:3).eq.'P') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'D') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'S') then
          go to 9999
        else if(CrwLabel(id)(3:3).eq.'X') then
          go to 9999
        endif
      else if(CrwType(id).eq.CrwTypeBMP) then
        Veta=CrwBMPName(id)(:idel(CrwBMPName(id)))
        if(CrwState(id).eq.CrwOn) then
          Veta=Veta(:idel(Veta))//'-on.bmp'
        else if(CrwState(id).eq.CrwOff) then
          Veta=Veta(:idel(Veta))//'-off.bmp'
        else if(CrwState(id).eq.CrwDisabled) then
          Veta=Veta(:idel(Veta))//'-disable.bmp'
        else if(CrwState(id).eq.CrwLocked) then
          Veta=Veta(:idel(Veta))//'-disable.bmp'
        endif
        call FeLoadImage(CrwXMin(id)-2.,CrwXMax(id)+2.,
     1                   CrwYMin(id)-2.,CrwYMax(id)+2.,
     2                   Veta,1)
      else
        Rad=(CrwXMax(id)-CrwXMin(id))*.5
        call FeArc(xpom,ypom,Rad, 45.,180.,Gray)
        call FeArc(xpom,ypom,Rad,225.,180.,White)
        Rad=anint(Rad*EnlargeFactor-1.)/EnlargeFactor
        call FeCircle(xpom,ypom,Rad,Gray)
        Rad=anint(Rad*EnlargeFactor-1.)/EnlargeFactor
        call FeCircle(xpom,ypom,Rad,ColorBkg)
      endif
      if(CrwLogic(id)) then
        if(CrwType(id).eq.CrwTypeRadio) then
          call FeCircle(xpom,ypom,2.,ColorFrg)
        else if(CrwType(id).eq.CrwTypeLetter) then
          call FeOutSt(0,xpom,ypom,CrwLabel(id)(3:3),'C',White)
        endif
      else
        if(CrwType(id).eq.CrwTypeLetter) then
          call FeOutSt(0,xpom,ypom,CrwLabel(id)(3:3),'C',Black)
        endif
      endif
      go to 9999
      entry FeCrwActivate(id)
      ColorSel=Black
      go to 8000
      entry FeCrwDeactivate(id)
      ColorSel=LightGray
8000  if(CrwType(id).eq.CrwTypeLetter.or.CrwType(id).eq.CrwTypeBMP)
     1  go to 9999
      if(CrwLabel(id).ne.' ') then
        x1=CrwLabelXMin(id)
        x2=CrwLabelXMax(id)
        y1=CrwLabelYMin(id)
        y2=CrwLabelYMax(id)
      else if(CrwExGr(id).eq.0) then
        x1=anint(CrwXmin(id)*EnlargeFactor+1.)/EnlargeFactor
        x2=anint(CrwXmax(id)*EnlargeFactor-1.)/EnlargeFactor
        y1=anint(CrwYmin(id)*EnlargeFactor+1.)/EnlargeFactor
        y2=anint(CrwYmax(id)*EnlargeFactor-1.)/EnlargeFactor
        if(ColorSel.eq.LightGray) ColorSel=White
      else
        go to 9999
      endif
      xu(1)=anint(x1*EnlargeFactor-1.)/EnlargeFactor
      yu(1)=anint(y1*EnlargeFactor-1.)/EnlargeFactor
      xu(2)=xu(1)
      yu(2)=anint(y2*EnlargeFactor+1.)/EnlargeFactor
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorSel)
      xu(3)=anint(x2*EnlargeFactor+1.)/EnlargeFactor
      xu(4)=xu(3)
      call FePolyLine(2,xu(3),yu,ColorSel)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorSel)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorSel)
      call FeLineType(NormalLine)
9999  return
      end
