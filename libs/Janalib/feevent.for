      subroutine FeEvent(Imm)
      include 'fepc.cmn'
      integer FeGetSystemTime
      logical FeTestIn,LeftIsDown
      character*1 Znak
      integer FeGetEdwStringPosition
      real MoveX,MoveY
      data DoubleClickX,DoubleClickY/2*0./,LeftIsDown/.false./
      data MoveX,MoveY/2*-1111./
      if(Imm.eq.-1) StartTime=FeGetSystemTime()
1000  EventType=0
      EventNumber=0
      EventNumberAbs=0
      if(Imm.le.0) then
        if(Imm.ne.-2) call FeReleaseOutput
        if(.not.TakeMouseMove) call FeMouseShape(0)
        if(Imm.eq.0) then
          call FeMessage(0)
        else
          call FeMessage(1)
          if(EventType.eq.0) then
            if(WaitTime.gt.0) then
              i=FeGetSystemTime()-StartTime
              if(i.gt.WaitTime) then
                EventType=EventSystem
                EventNumber=JeTimeOut
              else
                RemainTime=WaitTime-i
              endif
              go to 9100
            endif
            go to 1000
          endif
        endif
      else
        call FeMessage(1)
        if(EventType.eq.0) go to 9100
      endif

      if(EventType.eq.EventResize.or.
     1   (EventType.eq.EventSystem.and.EventNumber.eq.JeMimoOkno)) then
        go to 9100
      endif
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown.and.
     1   DoubleClickCount.eq.1.and.FeTimeDiff().lt..5.and.
     2   abs(DoubleClickX-Xpos).lt.1..and.
     3   abs(DoubleClickY-Ypos).lt.1.) then
        EventType=EventMouse
        EventNumber=JeDoubleClick
        DoubleClickCount=0
        go to 9990
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown)
     1  then
        LeftIsDown=.true.
        if(FeTestIn()) then
          if(EventType.eq.EventButton.or.EventType.eq.EventEdw.or.
     1       EventType.eq.EventCrw.or.EventType.eq.EventRolMenu.or.
     2       EventType.eq.EventEdwUp) then
            if(EventType.ne.EventEdw) LeftIsDown=.false.
            go to 9100
          endif
        else
          EventType=EventMouse
          EventNumber=JeLeftDown
        endif
        go to 9000
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
        LeftIsDown=.false.
        go to 9990
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        if(FeTestIn()) then
          if(EventType.eq.EventSbwMenu.and.SbwRightClickAllowed) then
            EventType=EventSbwRightClick
            go to 9100
          endif
        endif
        EventType=EventMouse
        EventNumber=JeRightDown
        go to 9100
      else if(EventType.eq.EventMouse.and.
     1        (EventNumber.eq.JeRightUp.or.
     2         EventNumber.eq.JeKoleckoKSobe.or.
     3         EventNumber.eq.JeKoleckoOdSebe)) then
        go to 9100
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        if(abs(MoveX-Xpos).gt.0..or.abs(MoveY-Ypos).gt.0.) then
          MoveX=Xpos
          MoveY=Ypos
          if(LeftIsDown.and.EdwActive.gt.0) then
            if(xpos.ge.EdwXmin(EdwActive).and.
     1         xpos.le.EdwXmax(EdwActive).and.
     2         ypos.ge.EdwYmin(EdwActive).and.
     3         ypos.le.EdwYmax(EdwActive)) then
              EventType=EventEdwSel
              EventNumber=FeGetEdwStringPosition(EdwActive,xpos)
              go to 9100
            endif
          endif
          if(TakeMouseMove.and.DoubleClickCount.eq.0) then
            EventType=EventMouse
            EventNumber=JeMove
            go to 9100
          endif
        else
          EventType=0
          EventNumber=0
          go to 1000
        endif
      endif
      if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1   EventType.eq.EventCtrl) then
        go to 9100
      else if((EventType.eq.EventAlt.and.EventNumber.eq.JeF4).or.
     1        (EventType.eq.EventSystem.and.EventNumber.eq.JeWinClose))
     2  then
        if(AskIfQuit) then
          EventType=EventGlobal
          EventNumber=GlobalClose
          go to 9100
        endif
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        stop
      else if(EventType.eq.EventAlt) then
        Znak=char(EventNumber)
        do i=ButtonFr,ButtonTo
          if(ButtonState(i).ne.ButtonOff.or.Znak.ne.ButtonZ(i:i)) cycle
          EventType=EventButton
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=EdwFr,EdwTo
          if(EdwState(i).ne.EdwOpened.or.Znak.ne.EdwZ(i:i)) cycle
          EventType=EventEdw
          EventNumber=i-EdwFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=CrwFr,CrwTo
          if(CrwState(i).eq.CrwRemoved.or.CrwState(i).eq.CrwClosed.or.
     1       CrwState(i).eq.CrwDisabled.or.CrwState(i).eq.CrwLocked.or.
     2       Znak.ne.CrwZ(i:i)) cycle
          EventType=EventCrw
          EventNumber=i-CrwFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do ii=1,KartNDol
          i=KartDolButt(ii)
          if(i.eq.0) cycle
          if(ButtonState(i).ne.ButtonOff.or.Znak.ne.ButtonZ(i:i)) cycle
          if(i.eq.KartESC.or.i.eq.KartOK) then
            EventType=EventButton
          else
            EventType=EventKartButt
          endif
          EventNumber=i-ButtonFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        do i=RolMenuFr,RolMenuTo
          if(RolMenuState(i).eq.RolMenuRemoved.or.
     1       RolMenuState(i).eq.RolMenuClosed.or.
     2       RolMenuState(i).eq.RolMenuDisabled.or.
     3       Znak.ne.RolMenuZ(i:i)) cycle
          EventType=EventRolMenu
          EventNumber=i-RolMenuFr+1
          EventNumberAbs=i
          go to 9100
        enddo
        go to 9100
      endif
      EventType=0
      EventNumber=0
      go to 1000
9000  if(DoubleClickAllowed) then
        DoubleClickCount=FeTimeDiff()
        DoubleClickCount=1
        DoubleClickX=Xpos
        DoubleClickY=Ypos
      endif
      go to 9990
9100  DoubleClickCount=0
9990  if(Imm.eq.0) then
        if((EventType.ne.EventMouse.or.EventNumber.ne.JeMove)
     1     .and.AllowChangeMouse) call FeMouseShape(3)
        if(.not.TakeMouseMove) call FeDeferOutput()
      endif
      if(EventNumberAbs.eq.0) EventNumberAbs=EventNumber
9999  return
      end
