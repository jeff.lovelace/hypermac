      subroutine FeArrow(xfrom,yfrom,xto,yto,Color,Type)
      include 'fepc.cmn'
      dimension xp(6),yp(6)
      integer Color,Type
      Angle=30.
      cosfi=cos(angle*ToRad)
      sinfi=sin(angle*ToRad)
      xp(1)=xto
      yp(1)=yto
      xp(2)=xfrom
      yp(2)=yfrom
      call FePolyLine(2,xp,yp,color)
      d=sqrt((xto-xfrom)**2+(yto-yfrom)**2)
      if(d.gt.5.) then
        dl=6.
      else
        dl=4.
      endif
      d=dl/d
      dx=(xfrom-xto)*d
      dy=(yfrom-yto)*d
      if(Type.eq.ArrowFromTo.or.Type.eq.ArrowTo) then
        xp(2)=xto+dx*cosfi-dy*sinfi
        yp(2)=yto+dx*sinfi+dy*cosfi
        xp(3)=xto+dx*cosfi+dy*sinfi
        yp(3)=yto-dx*sinfi+dy*cosfi
        call FePolygon(xp,yp,3,4,0,0,Color)
      endif
      if(Type.eq.ArrowFromTo.or.Type.eq.ArrowFrom) then
        xp(1)=xfrom
        yp(1)=yfrom
        xp(2)=xfrom-dx*cosfi+dy*sinfi
        yp(2)=yfrom-dx*sinfi-dy*cosfi
        xp(3)=xfrom-dx*cosfi-dy*sinfi
        yp(3)=yfrom+dx*sinfi-dy*cosfi
        call FePolygon(xp,yp,3,4,0,0,Color)
      endif
      return
      end
