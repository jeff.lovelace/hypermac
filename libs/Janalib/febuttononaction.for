      subroutine FeButtonOnAction(ib)
      include 'fepc.cmn'
      integer FeGetSystemTime,EventTypeOld,EventNumberOld,
     1        EventNumberAbsOld
      call FeQuestActiveObjOff
      call FeButtonOn(ib)
      call FeButtonActivate(ib)
      call FeReleaseOutput
      call FeDeferOutput
      StartTime=FeGetSystemTime()
      EventTypeOld=EventType
      EventNumberOld=EventNumber
      EventNumberAbsOld=EventNumberAbs
      EventTypeLost=0
      EventNumberLost=0
1000  call FeEvent(1)
      if(EventType.ne.0) then
        EventTypeLost=EventType
        EventNumberLost=EventNumber
      endif
      if(FeGetSystemTime()-StartTime.lt.200) go to 1000
      ActiveObj=ActButton*10000+ib
      EventType=EventTypeOld
      EventNumber=EventNumberOld
      EventNumberAbs=EventNumberAbsOld
      return
      end
