      logical function FeYeNo(xmi,ymi,text,idf,n)
      include 'fepc.cmn'
      character*(*) text
      character*4 AnoYes
      logical WizardModeIn,EqIgCase,PropFontIn
      integer QuestLineWidthIn
      PropFontIn=PropFont
      if(.not.PropFont) call FeSetPropFont
      WizardModeIn=WizardMode
      WizardMode=.false.
      AllowChangeMouse=.false.
      QuestLineWidthIn=QuestLineWidth
      QuestLineWidth=14.
      xd=max(150.,FeTxLength(Text)+10.)
      do i=1,iabs(n)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeBoldFont
        xd=max(FeTxLength(TextInfo(i))+10.,xd)
        if(EqIgCase(TextInfoFlags(i)(2:2),'B')) call FeNormalFont
      enddo
      if(WaitTime.gt.0) xd=max(FeTxLength(ItRemains)+10.,xd)
      id=NextQuestid()
      il=3
      if(iabs(n).gt.0) il=il+iabs(n)+1
      call FeQuestCreate(id,xmi,ymi,xd,il,' ',il,LightGray,-1,-1)
      CheckKeyboard=.true.
      il=0
      if(n.lt.0) then
        do i=1,iabs(n)
          il=il+1
          if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        TextInfoFlags(i)(1:1),
     2                        TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
        il=il+1
        call FeQuestLinkaMake(id,il)
      endif
      il=il+1
      call FeQuestLblMake(id,xd*.5,il,text,'C','N')
      if(n.gt.0) then
        do i=1,n
          il=il+1
          if(EqIgCase(TextInfoFlags(i)(1:1),'C')) then
            xpom=xd*.5
          else
            xpom=5.
          endif
          call FeQuestLblMake(id,xpom,il,TextInfo(i),
     1                        TextInfoFlags(i)(1:1),
     2                        TextInfoFlags(i)(2:2))
          TextInfoFlags(i)='LN'
        enddo
      endif
      il=il+1
      dpom=40.
      i=-10*il-7
      if(Language.eq.0) then
        AnoYes='Yes'
      else
        AnoYes='Ano'
      endif
      call FeQuestButtonMake(id,xd*.5-dpom-10.,i,dpom,ButYd,AnoYes)
      nButtYes=ButtonLastMade
      if(Language.eq.0) then
        AnoYes='No'
      else
        AnoYes='Ne'
      endif
      call FeQuestButtonMake(id,xd*.5+10.,i,dpom,ButYd,AnoYes)
      nButtNo=ButtonLastMade
      if(idf.eq.1) then
        FeYeNo=.true.
      else
        FeYeNo=.false.
      endif
      call FeQuestButtonOpen(nButtYes,ButtonOff)
      call FeQuestButtonOpen(nButtNo ,ButtonOff)
      nButOld=0
2000  if(FeYeNo) then
        nButNew=nButtYes
      else
        nButNew=nButtNo
      endif
      BlockedActiveObj=.false.
      call FeQuestActiveObjOff
      ActiveObj=10000*ActButton+nButNew+ButtonFr-1
      call FeQuestActiveObjOn
      if(nButNew.ne.nButOld) then
        i=nButNew
        call FeQuestMouseToButton(nButNew)
        nButOld=nButNew
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventKey.and.CheckNumber.eq.JeReturn) then
        FeYeNo=.true.
      else if(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
        FeYeNo=.false.
      else if(CheckType.eq.EventSystem.and.CheckNumber.eq.JeTimeOut)
     1  then
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtYes.or.CheckNumber.eq.nButtNo)) then
        FeYeNo=CheckNumber.eq.nButtYes
      else if(CheckType.eq.EventKey) then
        FeYeNo=.not.FeYeNo
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      if(LastQuest.le.0) then
        AllowChangeMouse=.true.
        call FeMouseShape(3)
      endif
      QuestLineWidth=QuestLineWidthIn
      if(.not.PropFontIn) call FeSetFixFont
      return
      end
