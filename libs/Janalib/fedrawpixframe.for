      subroutine FeDrawPixFrame(xmi,xma,ymi,yma,klic)
C klic = 1 ... vnejsi ram s ostrymi rohy
C klic = 2 ... vnejsi ram s kulatymi rohy
C klic = 3 ... line
C klic = 4 ... ram uvnitr plochy
      include 'fepc.cmn'
      real px(5),py(5),xmi,xma,ymi,yma,ppx(2),ppy(2)
      px(1)=xmi
      py(1)=ymi
      px(2)=xmi
      py(2)=yma
      px(3)=xma
      py(3)=yma
      px(4)=xma
      py(4)=ymi
      px(5)=px(1)
      py(5)=py(1)
      if(klic.eq.1) then
        call FePolyLine(3,px(1),py(1),LightGray)
        call FePolyLine(3,px(3),py(3),Black)
        do i=1,5
          if(px(i).eq.xmi) then
            px(i)=px(i)+1.
          else
            px(i)=px(i)-1.
          endif
          if(py(i).eq.ymi) then
            py(i)=py(i)+1.
          else
            py(i)=py(i)-1.
          endif
        enddo
        call FePolyLine(3,px(1),py(1),White)
        call FePolyLine(3,px(3),py(3),Gray)
      else if(klic.eq.2) then
        ppx(1)=px(1)
        ppx(2)=ppx(1)
        ppy(1)=py(1)+2.
        ppy(2)=py(2)-2.
        call FePolyLine(2,ppx,ppy,White)
        ppx(1)=ppx(1)+1.
        ppx(2)=ppx(2)+1.
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(2)+1.
        call FePolyLine(2,ppx,ppy,White)
        ppy(1)=ppy(1)+1.
        ppy(2)=ppy(2)-1.
        call FePolyLine(2,ppx,ppy,LightGray)
        ppx(1)=px(4)
        ppx(2)=px(4)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(2)+1.
        ppx(1)=ppx(1)-1.
        ppx(2)=ppx(1)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(1)=ppy(1)+1.
        ppy(2)=ppy(2)-1.
        call FePolyLine(2,ppx,ppy,Gray)
        ppx(1)=px(1)+2.
        ppx(2)=px(4)-2.
        ppy(1)=py(2)
        ppy(2)=py(2)
        call FePolyLine(2,ppx,ppy,White)
        ppy(1)=ppy(1)-1.
        ppy(2)=ppy(1)
        call FePolyLine(2,ppx,ppy,LightGray)
        ppy(1)=py(1)
        ppy(2)=ppy(1)
        call FePolyLine(2,ppx,ppy,Black)
        ppy(2)=ppy(2)+1.
        ppy(1)=ppy(2)
        call FePolyLine(2,ppx,ppy,Gray)
      else if(klic.eq.3) then
        px(1)=xmi
        py(1)=ymi
        px(2)=xma
        py(2)=yma
        call FePolyLine(2,px,py,Gray)
        py(1)=py(1)-1.
        py(2)=py(2)-1.
        call FePolyLine(2,px,py,White)
      else
        call FeChybne(-1.,-1.,'Spatny klic',' ',SeriousError)
      endif
      return
      end
