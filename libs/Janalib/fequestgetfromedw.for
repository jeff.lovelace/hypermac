      subroutine FeQuestGetFromEdw(iwa,IntArray,TextOut,RealArray,
     1                             Change)
      include 'fepc.cmn'
      dimension IntArray(*),RealArray(*)
      character*(*) TextOut
      logical   Change
      iwp=iwa+EdwFr-1
      go to 3400
      entry FeGetFromEdw(iwa,IntArray,TextOut,RealArray,Change)
      iwp=iwa
3400  if(EdwType(iwp).eq.0) then
        Change=TextOut.ne.EdwString(iwp)
        TextOut=EdwString(iwp)
      else
        Change=.false.
        do i=1,mod(iabs(EdwType(iwp)),10)
          if(EdwType(iwp).lt.0) then
            Change=Change.or.IntArray(i).ne.EdwInt(i,iwp)
            IntArray(i)=EdwInt(i,iwp)
          else
            Change=Change.or.RealArray(i).ne.EdwReal(i,iwp)
            RealArray(i)=EdwReal(i,iwp)
          endif
        enddo
      endif
      return
      end
