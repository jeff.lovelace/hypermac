      subroutine FeMakeAcFrame
      include 'fepc.cmn'
      do i=1,4
        xu(i)=XCornAcWin(1,i)
        yu(i)=XCornAcWin(2,i)
      enddo
      xu(5)=xu(1)
      yu(5)=yu(1)
      call FePolyLine(5,xu,yu,White)
      do i=1,10
        xu(1)=xu(1)-.05
        yu(1)=yu(1)-.05
        xu(2)=xu(2)-.05
        yu(2)=yu(2)+.05
        xu(3)=xu(3)+.05
        yu(3)=yu(3)+.05
        xu(4)=xu(4)+.05
        yu(4)=yu(4)-.05
        xu(5)=xu(1)
        yu(5)=yu(1)
        call FePolyLine(5,xu,yu,White)
      enddo
      return
      end
