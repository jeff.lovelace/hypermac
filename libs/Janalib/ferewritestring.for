      subroutine FeRewriteString(id,xp,yp,StringOld,StringNew,Justify,
     1                           EraseColor,WriteColor)
      include 'fepc.cmn'
      character*(*) StringOld,StringNew,Justify
      integer EraseColor,WriteColor
      logical Deferred
      Deferred=DeferredOutput
      if(.not.Deferred) call FeDeferOutput
      call FeGetTextRectangle(xp,yp,StringOld,Justify,1,xpold,xkold,
     1                        ypold,ykold,refx,refy,conx,cony)
      if(id.gt.0) then
        xpold=xpold+QuestXMin(id)
        xkold=xkold+QuestXMin(id)
        ypold=ypold+QuestYMin(id)
        ykold=ykold+QuestYMin(id)
      endif
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,EraseColor)
      call FeOutSt(id,xp,yp,StringNew,Justify,WriteColor)
      if(.not.Deferred) call FeReleaseOutput
      return
      end
