      subroutine FeSbwKresliPlovak(id)
      include 'fepc.cmn'
      klic=0
      go to 1000
      entry FeSbwPohniPlovak(id,yin,yout)
      klic=1
      ypom=yin
1000  if(SbwDelPlovak(id).le.0) go to 9999
      XMinSloupek=SbwXMinPruh(1,id)
      XMaxSloupek=SbwXMaxPruh(1,id)
      YMinSloupek=anint(SbwYMaxPosun(2,id)*EnlargeFactor+3.)/
     1            EnlargeFactor
      YMaxSloupek=anint(SbwYMinPosun(1,id)*EnlargeFactor-3.)/
     1            EnlargeFactor
      YDelSloupek=anint((YMaxSloupek-YMinSloupek)*EnlargeFactor-
     1                  float(SbwDelPlovak(id)))/EnlargeFactor
      call FeFillRectangle(XMinSloupek,XMaxSloupek,
     1                     YMinSloupek,YMaxSloupek,4,0,0,WhiteGray)
      if(Klic.eq.0) then
        ypom=YMaxSloupek-YDelSloupek*float(SbwItemFrom(id)-1)/
     1                               float(SbwItemNMax(id)-SbwLines(id))
      else
        ypom=min(ypom,YMaxSloupek)
        ypom=max(ypom,anint(YMinSloupek*EnlargeFactor+
     1                      float(SbwDelPlovak(id)))/EnlargeFactor)
        yout=ypom
        n=nint((YMaxSloupek-ypom)/YDelSloupek*
     1         float(SbwItemNMax(id)-SbwLines(id)))+1
        call FeSbwShowPohniUpDown(id,n)
      endif
      SbwXMinPlovak(id)=anint(SbwXMinPruh(1,id)*EnlargeFactor+2.)/
     1                  EnlargeFactor
      SbwXMaxPlovak(id)=anint(SbwXMaxPruh(1,id)*EnlargeFactor-2.)/
     1                  EnlargeFactor
      SbwYMaxPlovak(id)=anint(ypom*EnlargeFactor-2.)/EnlargeFactor
      SbwYMinPlovak(id)=anint(SbwYMaxPlovak(id)*EnlargeFactor-
     1                   float(SbwDelPlovak(id)-4))/EnlargeFactor
      xLD=SbwXMinPlovak(id)
      xRU=SbwXMaxPlovak(id)
      wp=xRU-xLD
      yLD=SbwYMinPlovak(id)
      yRU=SbwYMaxPlovak(id)
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,LightGray)
      call FeDrawFrame(xLD,yLD,wp,hp,2./EnlargeFactor,Gray,White,Gray,
     1                 .false.)
9999  return
      end
