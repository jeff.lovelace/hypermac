      function QuestYPosition(id,n)
      include 'fepc.cmn'
      if(n.ge.0) then
        if(n.gt.QuestLines(id).and.QuestLines(id).gt.0) then
          QuestYPosition=20.
        else
          if(n.eq.0) then
            i=0
          else
            i=n-1
            if(QuestText(id).ne.' ') i=i+1
          endif
          QuestYPosition=-QuestLineWidth*(float(i)+.5)
        endif
      else
        i=n+10
        if(QuestText(id).ne.' ') i=i-10
        QuestYPosition=-QuestLineWidth*(-float(i)*.1+.5)
      endif
      QuestYPosition=QuestLength(id)+QuestYPosition
      if(QuestText(id).eq.' ') QuestYPosition=QuestYPosition-5.
      return
      end
