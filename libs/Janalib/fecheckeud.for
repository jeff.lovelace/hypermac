      subroutine FeCheckEud(iw,iz)
      include 'fepc.cmn'
      integer Exponent10
      if(EdwType(iw).lt.0) then
        n=nint(float(EdwInt(1,iw)-EdwInt(2,iw))/float(EdwInt(4,iw)))+iz
        EdwInt(5,iw)=nint(float(EdwInt(3,iw)-EdwInt(2,iw))/
     1                    float(EdwInt(4,iw)))
        n=min(n,EdwInt(5,iw))
        n=max(n,0)
        EdwInt(1,iw)=EdwInt(2,iw)+n*EdwInt(4,iw)
        write(EdwString(iw),FormI15) EdwInt(1,iw)
      else if(EdwType(iw).gt.0) then
        pom=max(abs(EdwReal(2,iw)),abs(EdwReal(3,iw)))
        if(pom.gt.0.) then
          j=max(6-Exponent10(pom),0)
          j=min(j,6)
        else
          j=6
        endif
        pom=EdwReal(1,iw)+float(iz)*EdwReal(4,iw)
1100    if(pom.gt.EdwReal(3,iw)+EdwReal(4,iw)*.5) then
          pom=pom-EdwReal(4,iw)
          go to 1100
        endif
1200    if(pom.lt.EdwReal(2,iw)-EdwReal(4,iw)*.5) then
          pom=pom+EdwReal(4,iw)
          go to 1200
        endif
        EdwReal(1,iw)=pom
        if(EventType.eq.EventEdwUp.or.EventType.eq.EventEdwDown.or.
     1     (EventType.eq.EventKey.and.
     2      (EventNumber.eq.JeUp.or.EventNumber.eq.JeDown))) then
          write(RealFormat(6:7),'(i2)') j
          write(EdwString(iw),RealFormat) EdwReal(1,iw)
        else
          Cislo=EdwString(iw)
          call posun(Cislo,1)
          read(Cislo,'(f15.0)') EdwReal(1,iw)
        endif
      endif
      call ZdrcniCisla(EdwString(iw),1)
      Kurzor=idel(EdwString(iw))
      EdwTextLen(iw)=Kurzor
      if(iz.eq.0) then
        j=0
      else
        j=1
      endif
      call FeEdwOpen(iw)
      if(.not.BlockedActiveObj) ActiveObj=ActEdw*10000+iw
      call FeEudOff(iw,1)
      call FeEudOff(iw,2)
      return
      end
