      subroutine FeTwoPixLineHoriz(xmi,xma,y,ColorUp,ColorDown)
      include 'fepc.cmn'
      integer ColorUp,ColorDown
      xu(1)=xmi
      xu(2)=xma
      yu(1)=y
      yu(2)=yu(1)
      call FePolyLine(2,xu,yu,ColorUp)
      yu(1)=yu(1)-1.
      yu(2)=yu(1)
      call FePolyLine(2,xu,yu,ColorDown)
      return
      end
