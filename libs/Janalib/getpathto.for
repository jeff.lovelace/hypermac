      subroutine GetPathTo(PrgName,Directory,Path)
      include 'fepc.cmn'
      character*(*) PrgName,Directory,Path
      character*256 Veta,Filter,ListDir,ListFile,CurrentDirO
      character*80  PathTested
      integer FeChDir
      logical EqWild,EqIgCase
      ln=0
      CurrentDirO=CurrentDir
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      Path=' '
      do 3000i=1,2
        if(i.eq.1) then
          PathTested='C:\Program files\'
        else
          PathTested='C:\'
        endif
        j=FeChDir(PathTested)
        call FeDir(ListDir,ListFile,'*.*',0)
        if(ErrFlag.ne.0) go to 9999
        ln=NextLogicNumber()
        open(ln,file=ListDir)
1000    read(ln,FormA,err=3000,end=3000) Veta
        if(EqWild(Veta,Directory,OpSystem.gt.0)) then
          call CloseIfOpened(ln)
          j=FeChDir(Veta)
          call FeGetCurrentDir
          call FeDir(ListDir,ListFile,'*.*',0)
          if(ErrFlag.ne.0) go to 9999
          open(ln,file=ListFile)
1200      read(ln,FormA,err=3000,end=3000) Veta
          if(EqIgCase(Veta,PrgName)) then
            j=idel(CurrentDir)
            Path=CurrentDir(:j)
            if(Path(j:j).ne.'\') then
              j=j+1
              Path(j:j)='\'
            endif
            Path=Path(:j)//PrgName(:idel(PrgName))
            go to 9999
          endif
          go to 1200
        else
          go to 1000
        endif
3000  continue
      Path=' '
9999  i=FeChDir(CurrentDirO)
      call CloseIfOpened(ln)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      return
      end
