      subroutine FeQuestRealAEdwOpen(iw,X,n,Prazdny,Zlomek)
      include 'fepc.cmn'
      dimension X(n),xp(1)
      integer Exponent10
      logical Prazdny,Zlomek
      call FeQuestEdwOpenInic(iw,iwp,n,Prazdny)
      do i=1,n
        if(Prazdny) then
          EdwReal(i,iwp)=0.
        else
          if(Zlomek) then
            call ToFract(X(i),Cislo,10)
          else
            Cislo(1:1)='?'
          endif
          if(Cislo(1:1).eq.'?') then
            if(abs(X(i)).gt.0.) then
              j=max(6-Exponent10(X(i)),0)
              j=min(j,6)
            else
              j=6
            endif
            write(RealFormat(6:7),'(i2)') j
            write(Cislo,RealFormat) X(i)
          endif
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          EdwReal(i,iwp)=xp(1)
          call ZdrcniCisla(Cislo,1)
          if(i.eq.1) then
            EdwString(iwp)=Cislo
          else
            EdwString(iwp)=EdwString(iwp)(:idel(EdwString(iwp)))//
     1                     ' '//Cislo
          endif
        endif
      enddo
      call FeEdwOpen(iwp)
      return
      end
