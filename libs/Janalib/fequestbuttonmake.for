      subroutine FeQuestButtonMake(id,xw,n1,xdw,ydw,Text)
      include 'fepc.cmn'
      character*(*) Text
      integer OpenState
      ym=QuestYPosition(id,n1)-ydw*.5
      go to 1000
      entry FeQuestAbsButtonMake(id,xw,yw,xdw,ydw,Text)
      ym=yw
1000  ButtonTo=ButtonTo+1
      ButtonMax=ButtonTo
      ButtonLastMade=ButtonTo-ButtonFr+1
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
      QuestButtonTo(id)=ButtonTo
      call FeButtonMake(ButtonTo,id,xw,ym,xdw,ydw,Text)
      go to 9999
      entry FeQuestButtonOpen(iwa,OpenState)
      call FeButtonOpen(iwa+ButtonFr-1,OpenState)
      go to 9999
      entry FeQuestButtonDisable(iwa)
      call FeButtonDisable(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonOff(iwa)
      iw=iwa+ButtonFr-1
      call FeButtonOff(iw)
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton.and.iw.eq.j) call FeButtonActivate(iw)
      go to 9999
      entry FeQuestButtonOn(iwa)
      call FeButtonOn(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonClose(iwa)
      if(iwa.le.0) go to 9999
      iw=iwa+ButtonFr-1
      call FeButtonClose(iw)
      go to 4000
      entry FeQuestButtonActivate(iwa)
      call FeButtonActivate(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonDeactivate(iwa)
      call FeButtonDeactivate(iwa+ButtonFr-1)
      go to 9999
      entry FeQuestButtonRemove(iwa)
      iw=iwa+ButtonFr-1
      call FeButtonRemove(iw)
4000  i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActButton.and.iw.eq.j) then
        call FeButtonDeactivate(iw)
        ActiveObj=0
      endif
9999  return
      end
