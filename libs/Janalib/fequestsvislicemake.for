      subroutine FeQuestSvisliceMake(id,xmi,Shape)
      include 'fepc.cmn'
      integer Color1,Color2,Shape
      YFrom=0.
      YTo=QuestYMax(id)-QuestYMin(id)
      go to 1200
      entry FeQuestSvisliceFromToMake(id,xmi,ilFrom,ilTo,Shape)
      YFrom=QuestYPosition(id,ilFrom)+.5*QuestLineWidth
      YTo=QuestYPosition(id,ilTo)-.5*QuestLineWidth
      go to 1200
      entry FeQuestAbsSvisliceFromToMake(id,xmi,YFromIn,YToIn,Shape)
      YFrom=YFromIn
      YTo=YToIn
1200  xm=xmi
      SvisliceTo=SvisliceTo+1
      SvisliceLastMade=SvisliceTo-SvisliceFr+1
      QuestSvisliceTo(id)=SvisliceTo
      SvisliceState(SvisliceTo)=SvisliceOn
      SvisliceYMin(SvisliceTo)=YFrom
      SvisliceYMax(SvisliceTo)=YTo
      SvisliceXPos(SvisliceTo)=xm
      SvisliceShape(SvisliceTo)=Shape
      i=SvisliceTo
      go to 1300
      entry FeQuestSvisliceOn(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceOn
1300  Klic=0
      go to 2000
      entry FeQuestSvisliceRemove(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceRemoved
      go to 1400
      entry FeQuestSvisliceOff(iwa)
      i=iwa+SvisliceFr-1
      SvisliceState(i)=SvisliceOff
1400  Klic=1
2000  if(Klic.eq.0) then
        if(SvisliceShape(i).eq.0) then
          Color1=Gray
          Color2=White
        else
          Color1=Black
          Color2=Black
        endif
      else
        Color1=LightGray
        Color2=LightGray
      endif
      if(SvisliceShape(i).eq.1) then
        call FeOnePixLineVertic(SvisliceXPos(i)+QuestXMin(id),
     1                          SvisliceYMin(i)+QuestYMin(id),
     2                          SvisliceYMax(i)+QuestYMin(id),
     3                          Color1,Color2)
      else
        call FeTwoPixLineVertic(SvisliceXPos(i)+QuestXMin(id),
     1                          SvisliceYMin(i)+QuestYMin(id),
     2                          SvisliceYMax(i)+QuestYMin(id),
     3                          Color1,Color2)
      endif
9999  return
      end
