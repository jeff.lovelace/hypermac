      subroutine FeTriangl(x,y,size,Color)
      include 'fepc.cmn'
      integer Color
      pom=Size*sqrt(3.)*.5
      xp=x
      yp=y
      xu(1)=xp-pom
      xu(2)=xp+pom
      xu(3)=xp
      pom=Size
      yu(3)=yp+pom*.66666667
      yu(1)=yu(3)-pom
      yu(2)=yu(1)
      call FePolygon(xu,yu,3,4,0,0,Color)
      return
      end
