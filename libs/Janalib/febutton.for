      subroutine FeButton
      include 'fepc.cmn'
      character*(*) text
      integer BackGroundColor,ForeGroundColor,ColorLU,ColorRD,
     1        UnderLineColor,ColorButton,ColorObtah,OpenState
      logical Plivnout
      go to 9999
      entry FeButtonMake(id,idc,xm,ym,xd,yd,text)
      ButtonXMin(id)=xm
      ButtonXMax(id)=xm+xd
      ButtonYMin(id)=ym
      ButtonYMax(id)=ym+yd
      if(idc.gt.0) then
        ButtonXMin(id)=ButtonXMin(id)+QuestXMin(idc)
        ButtonXMax(id)=ButtonXMax(id)+QuestXMin(idc)
        ButtonYMin(id)=ButtonYMin(id)+QuestYMin(idc)
        ButtonYMax(id)=ButtonYMax(id)+QuestYMin(idc)
      endif
      ButtonState(id)=ButtonClosed
      ButtonText(id)=text
      ButtonAlways(id)=.false.
      go to 9999
      entry FeButtonOpen(id,OpenState)
      Plivnout=.false.
      if(OpenState.eq.ButtonRemoved) then
        go to 5000
      else if(OpenState.eq.ButtonOff) then
        go to 1000
      else if(OpenState.eq.ButtonOn) then
        go to 2000
      else if(OpenState.eq.ButtonDisabled) then
        go to 3000
      else if(OpenState.eq.ButtonClosed) then
        go to 4000
      else
        go to 9999
      endif
      entry FeButtonOff(id)
      Plivnout=.true.
1000  if(ButtonState(id).eq.ButtonOff.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=Black
      ButtonState(id)=ButtonOff
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      PressShift=0.
      UnderLineColor=ForeGroundColor
      go to 6000
      entry FeButtonOn(id)
      Plivnout=.true.
2000  go to 2500
      entry FeButtonOnNW(id)
      Plivnout=.true.
2500  PressShift=1.
      if(ButtonState(id).eq.ButtonOn.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=Black
      ButtonState(id)=ButtonOn
      ColorRD=White
      ColorLU=Gray
      ColorButton=LightGray
      ColorObtah=Gray
      UnderLineColor=ForeGroundColor
      go to 6000
      entry FeButtonDisable(id)
      Plivnout=.false.
3000  if(ButtonState(id).eq.ButtonDisabled.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      BackGroundColor=LightGray
      ForeGroundColor=WhiteGray
      ButtonState(id)=ButtonDisabled
      ColorRD=Gray
      ColorLU=White
      ColorButton=LightGray
      ColorObtah=Gray
      PressShift=0.
      UnderLineColor=-1
      go to 6000
      entry FeButtonClose(id)
      Plivnout=.false.
4000  if(ButtonState(id).eq.ButtonClosed.or.
     1   ButtonState(id).eq.ButtonRemoved) go to 9999
      ButtonState(id)=ButtonClosed
      go to 5500
      entry FeButtonRemove(id)
5000  if(ButtonState(id).eq.ButtonRemoved) go to 9999
      ButtonState(id)=ButtonRemoved
      ButtonZ(id:id)=' '
5500  dx=2.
      pom1=anint(ButtonXMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom2=anint(ButtonXMax(id)*EnlargeFactor+dx)/EnlargeFactor
      pom3=anint(ButtonYMin(id)*EnlargeFactor-dx)/EnlargeFactor
      pom4=anint(ButtonYMax(id)*EnlargeFactor+dx)/EnlargeFactor
      call FeFillRectangle(pom1,pom2,pom3,pom4,4,0,0,LightGray)
      go to 9999
6000  if(PressShift.gt..01) then
        dx =1.
        dxx=1.
      else
        dx=2.
        dxx=0.
      endif
      xLD=anint(ButtonXMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      xRU=anint(ButtonXMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      wp=xRU-xLD
      yLD=anint(ButtonYMin(id)*EnlargeFactor-dxx)/EnlargeFactor
      yRU=anint(ButtonYMax(id)*EnlargeFactor+dxx)/EnlargeFactor
      hp=yRU-yLD
      call FeFillRectangle(xLD,xRU,yLD,yRU,4,0,0,ColorButton)
      call FeDrawFrame(xLD,yLD,wp,hp,dx,ColorRD,ColorLU,ColorObtah,
     1                 .false.)
7000  if(ButtonText(id).ne.'#'.and.ButtonText(id).ne.'^'.and.
     1   ButtonText(id).ne.'$') then
        xpom=anint((ButtonXMin(id)+ButtonXMax(id))*.5*EnlargeFactor+
     1             PressShift)/EnlargeFactor
        ypom=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1             PressShift)/EnlargeFactor
        call FeOutStUnder(0,xpom,ypom,
     1                    ButtonText(id)(:idel(ButtonText(id))),'C',
     2                    ForeGroundColor,UnderLineColor,ButtonZ(id:id))
      else
        if(ButtonText(id).eq.'#'.or.ButtonText(id).eq.'^') then
          ButtonZ(id:id)=' '
          xu(1)=anint((ButtonXMin(id)+ButtonXMax(id))*.5*EnlargeFactor+
     1                PressShift-5.)/EnlargeFactor
          xu(2)=anint(xu(1)*EnlargeFactor+5.)/EnlargeFactor
          xu(3)=anint(xu(1)*EnlargeFactor+10.)/EnlargeFactor
          xu(4)=xu(2)
          if(ButtonText(id).eq.'#') then
            zn= 1.
          else
            zn=-1.
          endif
          yu(1)=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1                PressShift+5.*zn)/EnlargeFactor
          yu(2)=anint(yu(1)*EnlargeFactor-3.*zn)/EnlargeFactor
          yu(3)=yu(1)
          yu(4)=anint(yu(1)*EnlargeFactor-10.*zn)/EnlargeFactor
          call FePolygon(xu,yu,4,4,0,0,ForeGroundColor)
        else
          ButtonZ(id:id)=' '
          xu(1)=anint((ButtonXMin(id)+ButtonXMax(id))*EnlargeFactor*.5
     1                +PressShift-5.)/EnlargeFactor
          xu(2)=anint(xu(1)*EnlargeFactor+5.)/EnlargeFactor
          xu(3)=anint(xu(1)*EnlargeFactor+10.)/EnlargeFactor
          zn=-1.
          yu(1)=anint((ButtonYMin(id)+ButtonYMax(id))*.5*EnlargeFactor-
     1                PressShift+4.*zn)/EnlargeFactor
          yu(2)=anint(yu(1)*EnlargeFactor-8.*zn)/EnlargeFactor
          yu(3)=yu(1)
          call FePolygon(xu,yu,3,4,0,0,ForeGroundColor)
        endif
      endif
      go to 9999
      entry FeButtonActivate(id)
      ColorButton=Black
      go to 8000
      entry FeButtonDeactivate(id)
      ColorButton=LightGray
8000  xu(1)=anint(ButtonXMin(id)*EnlargeFactor+1.)/EnlargeFactor
      yu(1)=anint(ButtonYMin(id)*EnlargeFactor+1.)/EnlargeFactor
      xu(2)=xu(1)
      yu(2)=anint(ButtonYMax(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(3)=anint(ButtonXMax(id)*EnlargeFactor-1.)/EnlargeFactor
      xu(4)=xu(3)
      call FeLineType(DenseDottedLine)
      call FePolyLine(2,xu,yu,ColorButton)
      call FePolyLine(2,xu(3),yu,ColorButton)
      xu(2)=xu(3)
      yu(3)=yu(1)
      yu(4)=yu(1)
      call FePolyLine(2,xu,yu(3),ColorButton)
      yu(1)=yu(2)
      call FePolyLine(2,xu,yu,ColorButton)
      call FeLineType(NormalLine)
9999  return
      end
