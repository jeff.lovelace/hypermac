      subroutine FeQuestEdwLabelChange(iwp,Text)
      include 'fepc.cmn'
      character*(*) Text
      iw=iwp+EdwFr-1
      i=ActiveObj/10000
      j=mod(ActiveObj,10000)
      if(i.eq.ActEdw.and.j.eq.iw) then
        j=1
      else
        j=0
      endif
      if(EdwState(iw).eq.EdwOpened.or.EdwState(iw).eq.EdwDisabled) then
        if(EdwLabel(iw).ne.' ')
     1    call FeTextErase(0,EdwLabelX(iw),EdwLabelY(iw),
     2                     EdwLabel(iw),EdwJustify(iw),LightGray)
        if(j.ne.0) call FeEdwDeactivate(iw)
      endif
      if(Text.ne.' ') then
        call FeGetTextRectangle(EdwLabelX(iw),EdwLabelY(iw),
     1    Text,EdwJustify(iw),1,x1,x2,y1,y2,refx,refy,conx,
     2    cony)
        EdwLabelXMin(iw)=x1
        EdwLabelXMax(iw)=x2
        EdwLabelYMin(iw)=y1
        EdwLabelYMax(iw)=y2
      else
        EdwZ(iw:iw)=' '
        EdwLabelXMin(iw)=0.
        EdwLabelXMax(iw)=0.
        EdwLabelYMin(iw)=0.
        EdwLabelYMax(iw)=0.
      endif
      if(EdwState(iw).eq.EdwOpened) then
        if(Text.ne.' ') then
          call FeOutStUnder(0,EdwLabelX(iw),EdwLabelY(iw),
     2                      Text,EdwJustify(iw),Black,Black,
     3                      EdwZ(iw:iw))
          if(j.ne.0) call FeEdwActivate(iw)
        endif
      endif
      EdwLabel(iw)=Text
      return
      end
