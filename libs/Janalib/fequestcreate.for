      subroutine FeQuestCreate(id,x,y,xd,n,Text,Check,ColorFill,
     1                         ExternalESC,ExternalOK)
      include 'fepc.cmn'
      character*(*) Text
      character*80  Veta
      integer Check,ColorFill,ExternalESC,ExternalOK,ButtonFrPlus,
     1        ExternalCancel,ExternalCancelIn
      logical YInLines,JeToKart
      JeToKart=.false.
      ExternalCancel=0
      go to 500
      entry FeKartQuestCreate(id,x,y,xd,n,Text,Check,ColorFill,
     1                        ExternalESC,ExternalOK,ExternalCancelIn)
      ExternalCancel=ExternalCancelIn
      JeToKart=.true.
500   YInLines=.true.
      go to 1000
      entry FeQuestAbsCreate(id,x,y,xd,ydi,Text,Check,ColorFill,
     1                       ExternalESC,ExternalOK)
      JeToKart=.false.
      ExternalCancel=0
      go to 600
      entry FeKartQuestAbsCreate(id,x,y,xd,ydi,Text,Check,ColorFill,
     1                           ExternalESC,ExternalOK)
      JeToKart=.true.
      ExternalCancel=0
600   yd=ydi
      YInLines=.false.
1000  EdwLastCheck=0
      ButtonFrPlus=0
      BlockedActiveObj=.false.
      if(JeToKart) then
        QuestKartIdPrev(id)=0
      else
        if(id.gt.1.and.KartId.ne.0) ButtonFrPlus=KartNDol-2
        call FeKartRefresh(0)
        QuestKartIdPrev(id)=KartId
        KartId=0
      endif
      if(KartOn) then
        xm=KartXMin+KartSidePruh
        ym=KartYMin+KartLowerPruh
        xdp=KartXMax-KartXMin-2.*KartSidePruh
        if(YInLines) then
          QuestLines(id)=KartLines
          QuestLength(id)=float(KartLines)*QuestLineWidth
        else
          QuestLines(id)=0
          QuestLength(id)=yd
        endif
      else
        xm=x
        ym=y
        xdp=xd
        if(YInLines) then
          QuestLines(id)=n
          if(ExternalESC.eq.0.or.ExternalOk.eq.0) then
            if(QuestLines(id).ge.0.) then
              QuestLines(id)=QuestLines(id)+1
            else
              QuestLines(id)=QuestLines(id)-10
            endif
          endif
          if(Text.ne.' ') then
            if(QuestLines(id).ge.0.) then
              QuestLines(id)=QuestLines(id)+1
            else
              QuestLines(id)=QuestLines(id)-10
            endif
          endif
          if(QuestLines(id).ge.0.) then
            QuestLength(id)=QuestLineWidth*float(QuestLines(id))
          else
            QuestLength(id)=QuestLineWidth*
     1                      (-float(QuestLines(id))*.1)
          endif
          if(ExternalESC.eq.0.or.ExternalOk.eq.0) then
            QuestLength(id)=QuestLength(id)+20.
          else
            QuestLength(id)=QuestLength(id)+10.
          endif
          if(Text.eq.' ') QuestLength(id)=QuestLength(id)+5.
        else
          QuestLines(id)=0
          QuestLength(id)=yd
        endif
      endif
      if(YInLines.and.WaitTime.gt.0) QuestLength(id)=QuestLength(id)+12.
      yd=QuestLength(id)
      if(.not.KartOn) then
        if(x.lt.0.) then
          xm=XCenBasWin-xdp*.5
        endif
        if(ym.lt.0.) then
          ym=YCenBasWin-yd*.5
        endif
      endif
      xm=anint(xm)
      xdp=anint(xdp)
      ym=anint(ym)
      yd=anint(yd)
      QuestXMin(id)=xm
      QuestXMax(id)=xm+xdp
      QuestXLen(id)=QuestXMax(id)-QuestXMin(id)
      QuestYMin(id)=ym
      QuestYMax(id)=ym+yd
      QuestYLen(id)=QuestYMax(id)-QuestYMin(id)
      QuestGetEdwActive(id)=.true.
      QuestState(id)=1
      LastQuest=id
      QuestColorFill(id)=ColorFill
      if(id.eq.1) then
        ButtonFr=1
        EdwFr=1
        CrwFr=1
        SbwFr=1
        LblFr=1
        LinkaFr=1
        SvisliceFr=1
        RolMenuFr=1
        ActiveObjFr=1
        QuestCalledFrom(id)=0
        QuestEdwNAll(id)=0
        QuestEdwActive(id)=0
        QuestSbwActive(id)=0
        QuestEventType(id)=0
        QuestEventNumber(id)=0
        QuestEventNumberAbs(id)=0
      else
        call FeFrToChange(LastActiveQuest,0)
        QuestCalledFrom(id)=LastActiveQuest
        if(EdwActive.gt.0) then
          call FeZmenKurzor(EdwActive)
          call FeEdwSelClear(EdwActive)
        endif
        ButtonFr=QuestButtonTo(id-1)+1+ButtonFrPlus
        EdwFr=QuestEdwTo(id-1)+1
        CrwFr=QuestCrwTo(id-1)+1
        SbwFr=QuestSbwTo(id-1)+1
        LblFr=QuestLblTo(id-1)+1
        LinkaFr=QuestLinkaTo(id-1)+1
        SvisliceFr=QuestSvisliceTo(id-1)+1
        RolMenuFr=QuestRolMenuTo(id-1)+1
        ActiveObjFr=QuestActiveObjTo(id-1)+1
        CheckType=0
        CheckNumber=0
      endif
      CheckMouse=.false.
      AllowChangeMouse=.false.
      CheckKeyBoard=.false.
      TakeMouseMove=.false.
      QuestButtonFr(id)=ButtonFr
      ButtonTo=ButtonFr-1
      ButtonMax=ButtonTo
      EdwTo=EdwFr-1
      CrwTo=CrwFr-1
      SbwTo=SbwFr-1
      LblTo=LblFr-1
      LinkaTo=LinkaFr-1
      SvisliceTo=SvisliceFr-1
      RolMenuTo=RolMenuFr-1
      RolMenuMax=RolMenuTo
      ActiveObjTo=ActiveObjFr-1
      QuestEdwFr(id)=EdwFr
      QuestEdwTo(id)=EdwTo
      QuestCrwFr(id)=CrwFr
      QuestCrwTo(id)=CrwTo
      QuestButtonFr(id)=ButtonFr
      QuestButtonTo(id)=ButtonTo
      QuestSbwFr(id)=SbwFr
      QuestSbwTo(id)=SbwTo
      QuestLblFr(id)=LblFr
      QuestLblTo(id)=LblTo
      QuestLinkaFr(id)=LinkaFr
      QuestLinkaTo(id)=LinkaTo
      QuestSvisliceFr(id)=SvisliceFr
      QuestSvisliceTo(id)=SvisliceTo
      QuestRolMenuFr(id)=RolMenuFr
      QuestRolMenuTo(id)=RolMenuTo
      QuestActiveObjFr(id)=ActiveObjFr
      QuestActiveObjTo(id)=ActiveObjTo
      QuestActiveObj(id)=ActiveObj
      KurzorEdw=0
      EdwActive=0
      SbwActive=0
      LastEdw=0
      ActiveObj=0
      InsertMode=.true.
      LastActiveQuest=id
      QuestTmpFile(id)='jqst'
      if(OpSystem.le.0) then
        call CreateTmpFile(QuestTmpFile(id),i,1)
      else
        call CreateTmpFileName(QuestTmpFile(id),id)
      endif
      if(KartOn) then
        call FeFillRectangle(xm,xm+xdp,ym,ym+yd,4,0,0,LightGray)
      else
        call FeSaveImage(xm-FrameWidth,xm+xdp+FrameWidth,
     1                   ym-FrameWidth,ym+yd+FrameWidth,
     2                   QuestTmpFile(id))
        if(ColorFill.gt.0) then
          call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                         ColorFill)
          call FeDrawFrame(xm,ym,xdp,yd,FrameWidth,Gray,White,Black,
     1                     id.gt.1)
        endif
      endif
      call FeQuestTitleMake(id,Text)
      i=0
      if(ExternalESC.eq.0) i=i+1
      if(ExternalOk .eq.0) i=i+1
      if(WaitTime.gt.0) then
        ysh=16.
      else
        ysh=10.
      endif
      if(WizardMode) then
        dpom=75.
      else
        dpom=45.
      endif
      if(YInLines) then
        if(QuestLines(id).ge.0) then
          j=QuestLines(id)-1
          if(QuestText(id).eq.' ') j=j+1
        else
          j=QuestLines(id)+10
          if(QuestText(id).eq.' ') j=j-10
        endif
        ypom=QuestYPosition(id,j)-ButYd*.5-10.
        idp=id
      else
        if(WaitTime.gt.0) then
          ypom=ym+16.
        else
          ypom=ym+10.
        endif
        idp=0
      endif
      if(ExternalESC.eq.0) then
        if(i.eq.1) then
          xmp=(xdp-dpom)*.5
        else
          xmp=xdp*.5-dpom-8.
        endif
        if(.not.YInLines) xmp=xm+xmp
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        QuestButtonTo(id)=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        ButtonEsc=ButtonTo
        if(WizardMode) then
          Veta='Back'
        else
          Veta='Esc'
        endif
        call FeButtonMake(ButtonEsc,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonEsc,ButtonOff)
        ButtonInfo(1,ButtonEsc)='Ignores changes and closes the form.'
        ButtonInfo(2,ButtonEsc)='Pressing Esc has the same effect.'
        ButtonInfo(3,ButtonEsc)='@'
      else if(ExternalESC.gt.0) then
        ButtonInfo(1,ButtonEsc)='@'
        if(KartOn) then
          ButtonESC=ExternalEsc
        else
          ButtonEsc=ExternalEsc+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonESC
      else
        ButtonESC=0
      endif
      QuestButtonEsc(id)=ButtonESC
      if(ExternalOK.eq.0) then
        if(i.eq.1) then
          xmp=(xdp-dpom)*.5
        else
          xmp=xdp*.5+8.
        endif
        if(.not.YInLines) xmp=xm+xmp
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        QuestButtonTo(id)=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        ButtonOk=ButtonTo
        if(WizardMode) then
          Veta='Next'
        else
          Veta='Ok'
        endif
        call FeButtonMake(ButtonOk,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonOk,ButtonOff)
        ButtonInfo(1,ButtonOk)='Accepts changes and closes the form.'
        ButtonInfo(2,ButtonOk)='Double-pressing  Enter has the same'
        ButtonInfo(3,ButtonOk)='effect.'
        ButtonInfo(4,ButtonOk)='@'
      else if(ExternalOK.gt.0) then
        ButtonInfo(1,ButtonOk)='@'
        if(KartOn) then
          ButtonOK=ExternalOK
        else
          ButtonOk=ExternalOK+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonOK
      else
        ButtonOK=0
      endif
      QuestButtonOk(id)=ButtonOk
      if(WizardMode) then
        Veta='Cancel'
        dpom=FeTxLength(Veta)+10.
        xmp=xdp-15.-dpom
        ButtonTo=ButtonTo+1
        ButtonMax=ButtonTo
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonTo
        QuestButtonTo(id)=ButtonTo
        ButtonCancel=ButtonTo
        call FeButtonMake(ButtonCancel,idp,xmp,ypom,dpom,ButYd,Veta)
        call FeButtonOpen(ButtonCancel,ButtonOff)
      else if(ExternalCancel.gt.0) then
        if(KartOn) then
          ButtonCancel=ExternalCancel
        else
          ButtonCancel=ExternalCancel+ButtonTo
        endif
        ActiveObjTo=ActiveObjTo+1
        ActiveObjList(ActiveObjTo)=10000*ActButton+ButtonCancel
      else
        ButtonCancel=0
      endif
      if(ButtonOK.gt.0) then
        call FeMoveMouseTo(
     1                   (ButtonXMin(ButtonOk)+ButtonXMax(ButtonOk))*.5,
     2                   (ButtonYMin(ButtonOk)+ButtonYMax(ButtonOk))*.5)
      else
        call FeMoveMouseTo((QuestXMin(id)+QuestXMax(id))*.5,
     1                     (QuestYMin(id)+QuestYMax(id))*.5)
      endif
      CrwZ(CrwFr:)=' '
      EdwZ(EdwFr:)=' '
      ButtonZ(ButtonFr:)=' '
      QuestCheck(id)=Check
      QuestVystraha=VasekTest.eq.0
      RemainTime=WaitTime
      call FeQuestActiveObjOn
      BlockedActiveObj=.true.
      EventTypeSave     =0
      EventNumberSave   =0
      EventNumberAbsSave=0
      EventType         =0
      EventNumber       =0
      return
      end
