      subroutine FeTmpFileName
      include 'fepc.cmn'
      character*(*) TmpFileName
      character*256 TmpFileNameArr(100)
      logical EqIgCase
      data TmpFileNameArr/100*' '/
      entry FeTmpFilesAdd(TmpFileName)
      do i=1,100
        if(EqIgCase(TmpFileNameArr(i),' ')) then
          TmpFileNameArr(i)=TmpFileName
          go to 9999
        endif
      enddo
      go to 9999
      entry FeTmpFilesClear(TmpFileName)
      do i=1,100
        if(EqIgCase(TmpFileNameArr(i),TmpFileName)) then
          TmpFileNameArr(i)=' '
          go to 9999
        endif
      enddo
      go to 9999
      entry FeTmpFilesDelete
      do i=1,100
        if(.not.EqIgCase(TmpFileNameArr(i),' ')) then
          if(index(TmpFileNameArr(i),'*').gt.0.or.
     1       index(TmpFileNameArr(i),'?').gt.0) then
            call DeleteAllFiles('"'//
     1        TmpFileNameArr(i)(:idel(TmpFileNameArr(i)))//'"')
          else
            call DeleteFile(TmpFileNameArr(i))
          endif
        endif
      enddo
9999  return
      end
