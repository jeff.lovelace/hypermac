      subroutine FeTextErase(id,xt,yt,Text,Justify,Color)
      include 'fepc.cmn'
      character*(*) Text,Justify
      integer Color
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xt+QuestXMin(id)
          ym=yt+QuestYMin(id)
        endif
      else if(id.eq.0) then
        xm=xt
        ym=yt
      endif
      call FeGetTextRectangle(xm,ym,Text,Justify,1,xp,xk,yp,yk,
     1                        refx,refy,conx,cony)
      call FeFillRectangle(xp,xk,yp,yk,4,0,0,Color)
      return
      end
