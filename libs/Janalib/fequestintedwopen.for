      subroutine FeQuestIntEdwOpen(iw,X,Prazdny)
      include 'fepc.cmn'
      integer X
      logical   Prazdny
      call FeQuestEdwOpenInic(iw,iwp,-1,Prazdny)
      if(Prazdny) then
        EdwInt(1,iwp)=0
      else
        EdwInt(1,iwp)=X
        write(Cislo,FormI15) X
        call ZdrcniCisla(Cislo,1)
        EdwString(iwp)=Cislo
      endif
      call FeEdwOpen(iwp)
      return
      end
