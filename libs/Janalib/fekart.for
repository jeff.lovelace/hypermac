      subroutine FeKart
      include 'fepc.cmn'
      integer ExtESC,ExtOK,ExtESCIn,ExtOKIn
      logical stejnaW,WizardModeOld
      character*80 ch80
      character*(*) Text
      entry FeKartCreate(xmi,ymi,xdpom,n1,Text,ExtESCIn,ExtOKIn)
      call FeDeferOutput
      ExtOK=ExtOKIn
      ExtESC=ExtESCIn
      KartSidePruh=16.
      KartLabelPruh=30.
      KartTabsPruh=24.
      KartLowerPruh=45.+ButYd
      xdp=xdpom
      KartId0=NextQuestId()
      if(WizardMode) then
        xm=QuestXMin(KartId0)
        ym=QuestYMin(KartId0)
        xdp=QuestXLen(KartId0)
        yd=QuestYLen(KartId0)
        KartLines=nint((yd-KartLabelPruh-KartTabsPruh-KartLowerPruh)/
     1            QuestLineWidth)
        KartLowerPruh=yd-KartLabelPruh-KartTabsPruh-
     1                QuestLineWidth*float(KartLines)
        call FeFillRectangle(xm-1.,xm+xdp+1.,ym-1.,ym+yd+1.,4,0,0,
     1                       WhiteGray)
        if(ButtonOk.gt.0) then
          if(ButtonState(ButtonOk).ne.ButtonClosed) then
            ButtonState(ButtonOk)=ButtonOn
            call FeButtonOff(ButtonOk)
          endif
        endif
        if(ButtonEsc.gt.0) then
          if(ButtonState(ButtonEsc).ne.ButtonClosed) then
            ButtonState(ButtonEsc)=ButtonOn
            call FeButtonOff(ButtonEsc)
          endif
        endif
        if(ButtonCancel.gt.0) then
          if(ButtonState(ButtonCancel).ne.ButtonClosed) then
            ButtonState(ButtonCancel)=ButtonOn
            call FeButtonOff(ButtonCancel)
          endif
        endif
        KartCancel=ButtonCancel
      else
        KartLines=n1
        yd=KartLabelPruh+KartTabsPruh+
     1     QuestLineWidth*float(KartLines)+KartLowerPruh
        call FeKartQuestAbsCreate(KartId0,xmi,ymi,xdp,yd,' ',0,
     1                            WhiteGray,ExtESC,ExtOK)
        KartCancel=0
      endif
      KartXMin=QuestXMin(KartId0)
      KartXMax=QuestXMax(KartId0)
      KartYMin=QuestYMin(KartId0)
      KartYMax=QuestYMax(KartId0)
      call FeQuestAbsLblMake(KartId0,(KartXMax-KartXMin)*.5,
     1                       KartYMax-KartYMin-.5*KartLabelPruh,
     2                       Text,'C','B')
      WizardModeOld=WizardMode
      WizardMode=.false.
      KartFirstId=NextQuestId()
      WizardMode=WizardModeOld
      if(KartFirstId.eq.0) then
        call FeMsgOut(-1.,-1.,
     1                'FeCreateKart: Neni volne id pro prvni listek')
        call FeSmytec
      endif
      KartLastId=KartFirstId-1
      IKart=0
      if(ExtOK.ge.0) then
        KartOK=ButtonOK
      else
        KartOK=-1
      endif
      KartOn=.true.
      KartESC=ButtonEsc
      KartDolButt(1)=KartESC
      KartDolButt(2)=KartOK
      KartNDol=2
      if(WizardMode) then
        KartNDol=KartNDol+1
        KartDolButt(KartNDol)=KartCancel
      endif
      go to 9999
      entry FeCreateListek(Text,icheck)
      if(KartOn) then
        KartLastId=KartLastId+1
        id=KartLastId
        if(id.gt.mxquest) then
          call FeMsgOut(-1.,-1.,'FeCreateListek: presazeno mxquest')
          call FeSmytec
        endif
        if(id.gt.KartFirstId) then
          if(EdwTo.ge.EdwFr) call FeZmenKurzor(EdwActive)
          call FeSaveImage(QuestXMin(id-1),QuestXMax(id-1),
     1                     QuestYMin(id-1),QuestYMax(id-1),
     2                     QuestTmpFile(id-1))
        endif
        NKart=NKart+1
        if(NKart.gt.mxKart) then
          call FeMsgOut(-1.,-1.,'FeCreateListek: Presazeno mxKart')
          call FeSmytec
        endif
        KartListekSaved(NKart)=.false.
        KartUpdateListek(NKart)=0
        KartSwText(NKart)=Text
        sw=0.
        call FeBoldFont
        do i=1,NKart
          KartSwWidth(i)=FeTxLength(KartSwText(i))+10.
          sw=sw+KartSwWidth(i)
        enddo
        call FeNormalFont
        scw=(KartXMax-KartXMin-2.*KartSidePruh)/sw
        do i=1,NKart
          KartSwWidth(i)=KartSwWidth(i)*scw
        enddo
      endif
      WizardModeOld=WizardMode
      WizardMode=.false.
      call FeKartQuestCreate(id,0.,0.,0.,0,' ',icheck,0,KartESC,
     1                       KartOK,KartCancel)
      WizardMode=WizardModeOld
      go to 9999
      entry FeCompleteKart(nactive)
      Id=NKart+KartFirstId-1
      KartIdOld=id
      call FeSaveImage(QuestXMin(Id),QuestXMax(Id),
     1                 QuestYMin(Id),QuestYMax(Id),
     2                 QuestTmpFile(Id))
      sw=KartXMax-KartXMin-2.*KartSidePruh+1.
      swmx=0.
      swmn=sw
      do i=1,NKart
        pom1=KartSwWidth(i)
        sw=sw-pom1
        if(pom1.gt.swmx) then
          mx=i
          swmx=pom1
        endif
        if(pom1.lt.swmn) then
          mn=i
          swmn=pom1
        endif
      enddo
      i=nint(sw)
      if(i.gt.0) then
        j=mn
      else if(i.le.0) then
        j=mx
      endif
      KartSwWidth(j)=KartSwWidth(j)+float(i)
      xm=QuestXMin(KartFirstId)
      pom1=anint(QuestYMax(Id)*EnlargeFactor+3.)/EnlargeFactor
      pom2=anint((pom1+KartTabsPruh)*EnlargeFactor)/EnlargeFactor
      do i=1,NKart
        KartSwYMin(i)=pom1
        KartSwYMax(i)=pom2
        KartSwXmin(i)=xm
        xm=xm+KartSwWidth(i)
        KartSwXMax(i)=xm-1.
      enddo
      space=20.
      xd=10.
      nobu=0
      stejnaW=.true.
      sumaw=0.
      if(WizardMode) then
        ifr=4
      else
        ifr=3
      endif
      do i=ifr,KartNDol
        n=KartDolButt(i)
        if(n.le.0.or.n.gt.mxbut) cycle
        pom1=FeTxLength(ButtonText(n)(:idel(ButtonText(n)))//'x')
        xd=max(xd,pom1)
        sumaw=sumaw+pom1
        nobu=nobu+1
      enddo
1120  xm=KartXMin+.5*(KartXMax-KartXMin-float(nobu)*xd-
     1   float(nobu-1)*space)
      if(xm.le.KartXMin) then
        if(space.eq.20.) then
          space=10.
          go to 1120
        endif
        stejnaW=.false.
        space=(KartXMax-KartXMin-sumaw)/float(nobu+1)
        if(space.le.0.) space = 2.
        addw=0.
        if(space.gt.10.) then
          space=10.
          addw=(KartXMax-KartXMin-float(nobu+1)*space-sumaw)/
     1         float(nobu)
        endif
        xm=KartXMin+space
      endif
      ym=KartYMin+0.5*(KartLowerPruh+ButYd)
      do i=ifr,KartNDol
        n=KartDolButt(i)
        if(n.le.0.or.n.gt.mxbut) cycle
        ch80=ButtonText(n)
        if(.not.stejnaW) xd=FeTxLength(ch80(1:idel(ch80))//'x')+addw
        call FeButtonMake(n,0,xm,ym,xd,ButYd,ch80)
        call FeButtonOpen(n,ButtonOff)
        xm=xm+xd+space
      enddo
      go to 5000
      entry FeMalujKart(NActive)
      KartIdOld=KartId
5000  call FeFillRectangle(KartSwXMin(1)-2.,
     1                     KartSwXMax(NKart)+2.,
     2                     KartSwYMin(1)-2.,
     3                     KartSwYMax(1)+2.,4,0,0,WhiteGray)
      call FeFillRectangle(QuestXMin(KartFirstId),
     1                     QuestXMax(KartFirstId),
     2                     QuestYMin(KartFirstId),
     3                     QuestYMax(KartFirstId),
     4                     4,0,0,LightGray)
      call FeDrawFrame(QuestXMin(KartFirstId),QuestYMin(KartFirstId),
     1                 QuestXMax(KartFirstId)-QuestXMin(KartFirstId),
     2                 QuestYMax(KartFirstId)-QuestYMin(KartFirstId),
     3                 2.,Gray,White,Black,.false.)
      do i=1,NKart
        if(i.eq.NActive) cycle
        call FeDrawSwitch(KartSwXMin(i),KartSwXMax(i),KartSwYMin(i),
     1                    KartSwYMax(i),2)
        call FeOutSt(0,KartSwXmin(i)+.5*(KartSwXMax(i)-KartSwXmin(i)),
     1               KartSwYMin(i)+.5*(KartSwYMax(i)-KartSwYMin(i)),
     2               KartSwText(i),'C',Black)
      enddo
      pom1=2.
      call FeDrawSwitch(KartSwXMin(NActive)-pom1,
     1                  KartSwXMax(NActive)+pom1,
     2                  KartSwYMin(NActive)-pom1,
     3                  KartSwYMax(NActive)+pom1,2)
      call FeBoldFont
      call FeOutSt(0,KartSwXmin(NActive)
     1               +.5*(KartSwXMax(NActive)-KartSwXmin(NActive)),
     2               KartSwYMin(NActive)
     3               +.5*(KartSwYMax(NActive)-KartSwYMin(NActive))+1.,
     4               KartSwText(NActive),'C',Black)
      call FeNormalFont
      IKart=nactive
      LastActiveQuest=IKart+KartFirstId-1
      KartId=LastActiveQuest
      call FeLoadImage(QuestXMin(KartId),QuestXMax(KartId),
     1                 QuestYMin(KartId),QuestYMax(KartId),
     2                 QuestTmpFile(KartId),0)
      call FeSaveImage(KartXMin,KartXMax,KartYMin,KartYMax,
     1                 QuestTmpFile(KartId))
      KartListekSaved(nactive)=.true.
      WizardModeOld=WizardMode
      call FeFrToChange(KartIdOld,KartId)
      WizardMode=WizardModeOld
      call FeQuestActiveUpdate
      call FeQuestActiveObjOn
      go to 9999
      entry FePrepniListek(nactive)
      if(NActive.eq.IKart) go to 9999
      call FeEdwSelClear(KartId)
      if(QuestState(KartId).gt.0)
     1   call FeSaveImage(KartXMin,KartXMax,KartYMin,KartYMax,
     2                    QuestTmpFile(KartId))
      KartIdOld=KartId
      if(KartListekSaved(NActive)) then
        IKart=NActive
        LastActiveQuest=IKart+KartFirstId-1
        KartId=LastActiveQuest
        call FeLoadImage(KartXMin,KartXMax,KartYMin,KartYMax,
     1                   QuestTmpFile(KartId),-1)
        call FeFrToChange(KartIdOld,KartId)
        call FeQuestActiveUpdate
        call FeQuestActiveObjOn
      else
        go to 5000
      endif
9999  return
      end
