      subroutine MakeGraph
      include 'fepc.cmn'
      dimension xd(1000),nd(1000)
      character*80 t80
      character*12 jmena(4)
      data jmena/'%Quit','%Print','%Save','New %graph'/
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,40.,YBottomMargin,14.)
      call FeMakeAcWin(20.,2.,10.,10.)
      pom=YMaxGrWin
      ypom=YMaxBasWin-24.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      j=1
      do i=1,4
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Jmena(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
          j=ButtonOff
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
          j=ButtonOff
        endif
        ypom=ypom-10.
        call FeQuestButtonOpen(ButtonLastMade,j)
      enddo
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit) then
        go to 9000
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSave.or.CheckNumber.eq.nButtPrint))
     2  then
        if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.ne.0) go to 2000
        else
          call FeSavePicture('graph',6,1)
          if(HardCopy.le.0) then
            HardCopy=0
            go to 2000
          endif
        endif
        call FeFlowDiagram(xd,nd,n)
        if(CheckNumber.eq.nButtPrint) then
          call FePrintFile(PSPrinter,HCFileName,ich)
          call DeleteFile(HCFileName)
          call FeTmpFilesClear(HCFileName)
        endif
        HardCopy=0
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew)
     1  then
        step=.002
        ln=NextLogicNumber()
        call OpenFile(ln,'melilit_final.m61','formatted','old')
        if(ErrFlag.ne.0) go to 2000
        dmin=999.
        dmax=0.
2300    read(ln,FormA,end=2320) t80
        if(t80.eq.' '.or.t80(1:4).eq.'----') go to 2300
        read(t80,'(2f8.3,f9.4)',err=2300,end=2320) pom,pom,pom
        dmin=min(dmin,pom)
        dmax=max(dmax,pom)
        dmin=1.90
        dmax=1.96
        go to 2300
2320    rewind ln
        dmin=aint(dmin/step-1.)*step
        dmax=aint(dmax/step+2.)*step
        n=int((dmax-dmin)/step+1.)
        do i=1,n
          nd(i)=0
          xd(i)=dmin+float(i-1)*step
        enddo
2340    read(ln,FormA,end=2360) t80
        if(t80.eq.' ') go to 2340
        read(t80,'(2f8.3,f9.4)',err=2340,end=2360) pom,pom,pom
        if(pom.ge.dmin.and.pom.le.dmax) then
          i=(pom-dmin)/step+1.
          nd(i)=nd(i)+1
        endif
        go to 2340
2360    call CloseIfOpened(ln)
        call FeFlowDiagram(xd,nd,n)
        call FeQuestButtonOpen(nButtSave,ButtonOff)
        call FeQuestButtonOpen(nButtPrint,ButtonOff)
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      go to 2000
9000  call FeQuestRemove(id)
      return
      end
