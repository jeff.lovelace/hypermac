      subroutine FeQuestEventSbw(izpet)
      include 'fepc.cmn'
      integer tf,SbwActiveOld,SbwActiveNew,FeGetSystemTime
      logical Drzi,TakeMouseMoveSave
      izpet=0
      if(SbwActive.ne.0) then
        if(EventType.eq.EventKey) then
          is=SbwItemPointer(SbwActive)
          if((SbwType(SbwActive).eq.SbwMenuType.or.
     1        SbwType(SbwActive).eq.SbwSelectType)) then
            if(SbwItemSel(is,SbwActive).ge.0) then
              it=SbwItemPointer(SbwActive)-SbwItemFrom(SbwActive)+1
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(SbwItemSelLast.eq.-1)
     1            SbwItemSelLast=1-SbwItemSel(is,SbwActive)
              else
                SbwItemSelLast=-1
              endif
            endif
          endif
          if(EventNumber.eq.JePageUp.or.EventNumber.eq.JePageDown) then
            if(((SbwItemPointer(SbwActive).eq.1).and.
     1          EventNumber.eq.JePageUp).or.
     2         ((SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive))
     3          .and.EventNumber.eq.JePageDown)) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JePageDown) then
                  isp=is
                else
                  isp=is-SbwLines(SbwActive)+1
                endif
                call FeSbwSetItemSel(isp,SbwLines(SbwActive))
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JePageUp) then
                n=-SbwLines(SbwActive)+1
                if(SbwStripes(SbwActive).gt.1) n=n-1
                i=is+n
                if(i.lt.1) then
                  n=1-is
                  ip=1
                else
                  if(SbwStripes(SbwActive).gt.1) then
                    ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
                  else
                    ip=i
                  endif
                endif
              else if(EventNumber.eq.JePageDown) then
                n=SbwLines(SbwActive)-1
                if(SbwStripes(SbwActive).gt.1) n=n+1
                i=is+n
                if(i.gt.SbwItemNMax(SbwActive)) then
                  n=SbwItemNMax(SbwActive)-is
                endif
                if(SbwStripes(SbwActive).gt.1) then
                  ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                   SbwItemNMax(SbwActive))
                else
                  ip=is-SbwLines(SbwActive)+1+n
                endif
              endif
              SbwItemPointer(SbwActive)=is+n
              it=it+n
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JePageUp) then
                call FeSbwPageUp(SbwActive)
              else if(EventNumber.eq.JePageDown) then
                call FeSbwPageDown(SbwActive)
              endif
            endif
            go to 2000
          else if(EventNumber.eq.JeHome.or.EventNumber.eq.JeEnd) then
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(((SbwItemPointer(SbwActive).eq.1).and.
     1            EventNumber.eq.JeHome).or.
     2           ((SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive))
     3             .and.EventNumber.eq.JeEnd)) go to 2000
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JeHome) then
                  isp=1
                  n=is
                else
                  isp=is
                  n=SbwItemNMax(SbwActive)-is+1
                endif
                call FeSbwSetItemSel(isp,n)
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeEnd) then
                is=SbwItemNMax(SbwActive)
                if(SbwStripes(SbwActive).gt.1) then
                  ip=SbwLines(SbwActive)*
     1               (SbwStripesMax(SbwActive)-SbwStripes(SbwActive))+1
                else
                  ip=is-SbwLines(SbwActive)+1
                endif
              else if(EventNumber.eq.JeHome) then
                ip=1
                is=1
              endif
              SbwItemPointer(SbwActive)=is
              it=is-SbwItemFrom(SbwActive)+1
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive),SbwItemNMax(SbwActive)))
     2          then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JeEnd) then
                ip=SbwItemNMax(SbwActive)
              else if(EventNumber.eq.JeHome) then
                ip=1
              endif
              call FeSbwShow(SbwActive,ip)
            endif
            go to 2000
          else if((EventNumber.eq.JeUp.or.EventNumber.eq.JeDown).and.
     1            .not.CtrlPressed) then
            if((SbwItemPointer(SbwActive).eq.1.and.EventNumber.eq.JeUp)
     1         .or.(SbwItemPointer(SbwActive).eq.SbwItemNMax(SbwActive)
     2         .and.EventNumber.eq.JeDown)) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          call FeSbwSetItemSel(is,1)
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeUp) then
                it=it-1
                SbwItemPointer(SbwActive)=is-1
                if(SbwStripes(SbwActive).gt.1) then
                  ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
                else
                  ip=is-1
                endif
              else if(EventNumber.eq.JeDown) then
                it=it+1
                SbwItemPointer(SbwActive)=is+1
                if(SbwStripes(SbwActive).gt.1) then
                  ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                   SbwItemNMax(SbwActive))
                else
                  ip=is-SbwLines(SbwActive)+2
                endif
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            else
              if(EventNumber.eq.JeUp) then
                call FeSbwLineUp(SbwActive)
              else if(EventNumber.eq.JeDown) then
                call FeSbwLineDown(SbwActive)
              endif
            endif
            go to 2000
          else if(EventNumber.eq.JeLeft.or.EventNumber.eq.JeRight) then
            if((SbwItemPointer(SbwActive).le.SbwLines(SbwActive).and.
     1          EventNumber.eq.JeLeft).or.
     2         (SbwItemPointer(SbwActive)+SbwLines(SbwActive).gt.
     3          SbwItemNMax(SbwActive).and.EventNumber.eq.JeRight).or.
     4         SbwStripes(SbwActive).le.1) go to 2000
            if(SbwType(SbwActive).eq.SbwMenuType.or.
     1         SbwType(SbwActive).eq.SbwSelectType) then
              if(SbwType(SbwActive).eq.SbwSelectType.and.ShiftPressed)
     1          then
                if(EventNumber.eq.JeRight) then
                  isp=is
                else
                  isp=is-SbwLines(SbwActive)+1
                endif
                call FeSbwSetItemSel(isp,SbwLines(SbwActive))
                call FeSbwShow(SbwActive,SbwItemFrom(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive)))
     3          call FeSbwItemOff(SbwActive,it)
              if(EventNumber.eq.JeLeft) then
                it=it-SbwLines(SbwActive)
                SbwItemPointer(SbwActive)=is-SbwLines(SbwActive)
                ip=max(SbwItemFrom(SbwActive)-SbwLines(SbwActive),1)
              else if(EventNumber.eq.JeRight) then
                it=it+SbwLines(SbwActive)
                SbwItemPointer(SbwActive)=is+SbwLines(SbwActive)
                ip=min(SbwItemFrom(SbwActive)+SbwLines(SbwActive),
     1                 SbwItemNMax(SbwActive))
              endif
              if(it.ge.1.and.
     1           it.le.min(SbwLines(SbwActive)*SbwStripes(SbwActive),
     2                     SbwItemNMax(SbwActive))) then
                call FeSbwItemOn(SbwActive,it)
              else
                call FeSbwShow(SbwActive,ip)
              endif
            endif
            go to 2000
          endif
        endif
      endif
      if(EventType.eq.EventMouse.and.SbwActive.ne.0) then
        if(XPos.gt.SbwXMin(SbwActive).and.
     1     XPos.lt.SbwXMax(SbwActive).and.
     2     YPos.gt.SbwYMin(SbwActive).and.
     3     YPos.lt.SbwYMax(SbwActive)) then
          if(EventNumber.eq.JeKoleckoKSobe) then
            call FeSbwLineDown(SbwActive)
          else if(EventNumber.eq.JeKoleckoOdSebe) then
            call FeSbwLineUp(SbwActive)
          endif
        else
          go to 9999
        endif
      endif
      SbwActiveOld=SbwActive
      SbwActiveNew=0
      if(EventType.eq.EventSbwLineUp.or.
     1   EventType.eq.EventSbwLineDown.or.
     2   EventType.eq.EventSbwColumnLeft.or.
     3   EventType.eq.EventSbwColumnRight) then
        if(EventType.eq.EventSbwLineUp) then
          itype=1
        else if(EventType.eq.EventSbwLineDown) then
          itype=2
        else if(EventType.eq.EventSbwColumnLeft) then
          itype=3
        else if(EventType.eq.EventSbwColumnRight) then
          itype=4
        endif
        SbwActiveNew=EventNumberAbs
        call FeSbwPosunOn(SbwActiveNew,itype)
        tf=FeGetSystemTime()
        if(itype.eq.1) then
          call FeSbwLineUp(SbwActiveNew)
        else if(itype.eq.2) then
          call FeSbwLineDown(SbwActiveNew)
        else if(itype.eq.3) then
          call FeSbwColumnLeft(SbwActiveNew)
        else if(itype.eq.4) then
          call FeSbwColumnRight(SbwActiveNew)
        endif
        call FeReleaseOutput
        call FeDeferOutput
        Drzi=.false.
2700    call FeEvent(1)
        if(Drzi) then
          if(FeGetSystemTime()-tf.lt.5) go to 2705
          i=SbwItemFrom(SbwActiveNew)
          j=SbwColumnFrom(SbwActiveNew)
          k=SbwStripeFrom(SbwActiveNew)
          if(itype.eq.1) then
            call FeSbwLineUp(SbwActiveNew)
          else if(itype.eq.2) then
            call FeSbwLineDown(SbwActiveNew)
          else if(itype.eq.3) then
            call FeSbwColumnLeft(SbwActiveNew)
          else if(itype.eq.4) then
            call FeSbwColumnRight(SbwActiveNew)
          endif
          tf=FeGetSystemTime()
          if(i.ne.SbwItemFrom(SbwActiveNew).or.
     1       j.ne.SbwColumnFrom(SbwActiveNew).or.
     2       k.ne.SbwStripeFrom(SbwActiveNew)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else
          Drzi=FeGetSystemTime()-tf.gt.500
        endif
2705    if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp)
     1    go to 2710
        go to 2700
2710    call FeSbwPosunOff(SbwActiveNew,itype)
      else if(EventType.eq.EventSbwPageUp.or.
     1        EventType.eq.EventSbwPageDown.or.
     2        EventType.eq.EventSbwColumnStrongRight.or.
     3        EventType.eq.EventSbwColumnStrongLeft) then
        if(EventType.eq.EventSbwPageUp) then
          itype=1
        else if(EventType.eq.EventSbwPageDown) then
          itype=2
        else if(EventType.eq.EventSbwColumnStrongLeft) then
          itype=3
        else if(EventType.eq.EventSbwColumnStrongRight) then
          itype=4
        endif
        SbwActiveNew=EventNumberAbs
        tf=FeGetSystemTime()
        if(itype.eq.1) then
          call FeSbwPageUp(SbwActiveNew)
        else if(itype.eq.2) then
          call FeSbwPageDown(SbwActiveNew)
        else if(itype.eq.3) then
          call FeSbwColumnStrongLeft(SbwActiveNew)
        else if(itype.eq.4) then
          call FeSbwColumnStrongRight(SbwActiveNew)
        endif
        call FeSbwSkokOn(SbwActiveNew,itype)
        call FeReleaseOutput
        call FeDeferOutput
        Drzi=.false.
2720    call FeEvent(1)
        if(Drzi) then
          if(SbwType(SbwActiveNew).ne.SbwPureType) then
            if(FeGetSystemTime()-tf.lt.5) go to 2725
          endif
          i=SbwItemFrom(SbwActiveNew)
          j=SbwcolumnFrom(SbwActiveNew)
          if(itype.eq.1) then
            call FeSbwPageUp(SbwActiveNew)
          else if(itype.eq.2) then
            call FeSbwPageDown(SbwActiveNew)
          else if(itype.eq.3) then
            call FeSbwColumnStrongLeft(SbwActiveNew)
          else if(itype.eq.4) then
            call FeSbwColumnStrongRight(SbwActiveNew)
          endif
          call FeSbwSkokOn(SbwActiveNew,itype)
          if(SbwType(SbwActiveNew).ne.SbwPureType) then
            tf=FeGetSystemTime()
          endif
          if(i.ne.SbwItemFrom(SbwActiveNew).or.
     1       j.ne.SbwcolumnFrom(SbwActiveNew)) then
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else
          Drzi=FeGetSystemTime()-tf.gt.500
        endif
2725    if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp)
     1    go to 2730
        if(itype.le.2) then
          if(xpos.ge.SbwXMinPlovak(SbwActiveNew).and.
     1       xpos.le.SbwXMaxPlovak(SbwActiveNew)) then
            if(itype.eq.1) then
              if(ypos.le.SbwYMaxPlovak(SbwActiveNew)) go to 2730
            else if(itype.eq.2) then
              if(ypos.ge.SbwYMinPlovak(SbwActiveNew)) go to 2730
            endif
          endif
        else
          if(ypos.ge.SbwYMinStrelka(SbwActiveNew).and.
     1       ypos.le.SbwYMaxStrelka(SbwActiveNew)) then
            if(itype.eq.3) then
              if(xpos.ge.SbwXMinStrelka(SbwActiveNew)) go to 2730
            else if(itype.eq.4) then
              if(xpos.le.SbwXMaxStrelka(SbwActiveNew)) go to 2730
            endif
          endif
        endif
        go to 2720
2730    call FeSbwSkokOff(SbwActiveNew,itype)
      else if(EventType.eq.EventSbwPlovak) then
        SbwActiveNew=EventNumberAbs
        ypoc=ypos
        TakeMouseMoveSave=TakeMouseMove
        TakeMouseMove=.true.
2750    call FeEvent(1)
        if(EventType.eq.EventMouse) then
          if(EventNumber.eq.JeLeftUp) then
            go to 2760
          else if(EventNumber.eq.JeMove) then
            if(abs(ypoc-ypos).gt..5) then
              yin=anint((ypos-ypoc+SbwYMaxPlovak(SbwActiveNew))*
     1                  EnlargeFactor+2.)/EnlargeFactor
              call FeSbwPohniPlovak(SbwActiveNew,yin,yout)
              ypoc=ypos+yout-yin
              call FeReleaseOutput
              call FeDeferOutput
            endif
          endif
        else if(EventType.eq.EventSystem.and.
     1          EventNumber.eq.JeMimoOkno) then
          if(OpSystem.ne.1) go to 2760
        else
          go to 2750
        endif
        if(OpSystem.ne.1) then
          pom=(SbwXminPlovak(SbwActiveNew)+
     1         SbwXmaxPlovak(SbwActiveNew))*.5
          if(xpos.lt.pom-3..or.xpos.gt.pom+3.) then
            call FeMoveMouseTo(pom,ypos)
            xpos=pom
            call FeReleaseOutput
            call FeDeferOutput
          endif
        endif
        go to 2750
2760    TakeMouseMove=TakeMouseMoveSave
        go to 2000
      else if(EventType.eq.EventSbwStrelka) then
        SbwActiveNew=EventNumberAbs
        xpoc=xpos
        TakeMouseMoveSave=TakeMouseMove
        TakeMouseMove=.true.
2770    call FeEvent(1)
        if(EventType.eq.EventMouse) then
          if(EventNumber.eq.JeLeftUp) then
            go to 2780
          else if(EventNumber.eq.JeMove) then
            xin=anint((xpos-xpoc+SbwXMinStrelka(SbwActiveNew))*
     1                EnlargeFactor-2.)/EnlargeFactor
            call FeSbwPohniStrelku(SbwActiveNew,xin,xout)
            xout=xin
            xpoc=xpos+xout-xin
            call FeReleaseOutput
            call FeDeferOutput
          endif
        else if(EventType.eq.EventSystem.and.
     1          EventNumber.eq.JeMimoOkno) then
          if(OpSystem.ne.1) go to 2780
        else
          go to 2770
        endif
        if(OpSystem.ne.1) then
          pom=(SbwYminStrelka(SbwActiveNew)+
     1                        SbwYmaxStrelka(SbwActiveNew))*.5
          if(ypos.lt.pom-1..or.ypos.gt.pom+1.) then
            call FeMoveMouseTo(xpos,pom)
            ypos=pom
            call FeReleaseOutput
            call FeDeferOutput
          endif
        endif
        go to 2770
2780    TakeMouseMove=TakeMouseMoveSave
        call FeWInfRemove(1)
        go to 2000
      else if(EventType.eq.EventSbwMenu) then
        SbwActiveNew=mod(EventNumberAbs,100)
        if(SbwType(SbwActiveNew).eq.SbwMenuType.or.
     1     SbwType(SbwActiveNew).eq.SbwSelectType) then
          if(SbwItemPointer(SbwActiveNew).gt.0) then
            ifr=SbwItemPointer(SbwActiveNew)
            it=SbwItemPointer(SbwActiveNew)-SbwItemFrom(SbwActiveNew)+1
            if(it.ge.1.and.
     1         it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew)
     2                  ,SbwItemNMax(SbwActiveNew)-
     3                   SbwItemFrom(SbwActiveNew)+1))
     4        call FeSbwItemOff(SbwActiveNew,it)
            itold=it
          else
            itold=0
          endif
          it=EventNumberAbs/100
          if(it.ge.1.and.
     1       it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew),
     2                 SbwItemNMax(SbwActiveNew)-
     3                 SbwItemFrom(SbwActiveNew)+1)) then
            if(SbwType(SbwActiveNew).eq.SbwSelectType) then
              if(CtrlPressed) then
                ip=it+SbwItemFrom(SbwActiveNew)-1
                SbwItemSel(ip,SbwActiveNew)=
     1            1-SbwItemSel(ip,SbwActiveNew)
              else if(ShiftPressed) then
                ito=it+SbwItemFrom(SbwActiveNew)-1
                j=1-SbwItemSel(ifr,SbwActiveNew)
                if(ito.gt.ifr) then
                  i1=ifr
                  i2=ito
                else
                  i1=ito
                  i2=ifr
                endif
                do i=i1,i2
                  SbwItemSel(i,SbwActiveNew)=j
                  if(i.ne.ito) then
                    k=i-SbwItemFrom(SbwActiveNew)+1
                    if(k.gt.0) call FeSbwItemOff(SbwActiveNew,k)
                  endif
                enddo
              endif
            endif
            call FeSbwItemOn(SbwActiveNew,it)
          else
            call FeSbwItemOn(SbwActiveNew,itold)
          endif
          if(SbwDoubleClickAllowed(SbwActiveNew)) then
            if(SbwActiveNew.eq.SbwClicked.and.it.eq.itold) then
              if(FeTimeDiff().lt..3) then
                EventType=EventSbwDoubleClick
                go to 9000
              endif
            else
              SbwClicked=FeTimeDiff()
              SbwClicked=SbwActiveNew
            endif
          else
            SbwClicked=0
          endif
        endif
      else if(EventType.eq.EventSbwRightClick) then
        SbwActiveNew=mod(EventNumberAbs,100)
        if(SbwType(SbwActiveNew).eq.SbwSelectType) then
          it=EventNumberAbs/100
          if(it.ge.1.and.
     1       it.le.min(SbwLines(SbwActiveNew)*SbwStripes(SbwActiveNew),
     2                 SbwItemNMax(SbwActiveNew))) then
            ip=it+SbwItemFrom(SbwActiveNew)-1
            if(SbwItemSel(ip,SbwActiveNew).eq.0) then
              do i=1,SbwItemNMax(SbwActiveNew)
                SbwItemSel(i,SbwActiveNew)=0
                k=i-SbwItemFrom(SbwActiveNew)+1
                if(k.gt.0) call FeSbwItemOff(SbwActiveNew,k)
              enddo
              call FeSbwItemOn(SbwActiveNew,it)
              SbwItemSel(ip,SbwActiveNew)=1
            endif
            go to 9000
          endif
        endif
      endif
      go to 9100
2000  izpet=2000
      SbwActiveOld=SbwActive
      SbwActiveNew=0
      go to 9100
9000  izpet=9000
9100  if(SbwActiveOld.ne.SbwActiveNew.and.SbwActiveNew.ne.0) then
        call FeQuestActiveObjOff
        SbwActive=SbwActiveNew
        if(SbwActive.ne.0) then
          ActiveObj=ActSbw*10000+SbwActive
          call FeSbwActivate(SbwActive)
        endif
      endif
9999  if(SbwActive.gt.0) then
        if(SbwType(SbwActive).eq.SbwSelectType) then
          SbwItemSelN(SbwActive)=0
          do i=1,SbwItemNMax(SbwActive)
            if(SbwItemSel(i,SbwActive).gt.0)
     1        SbwItemSelN(SbwActive)=SbwItemSelN(SbwActive)+1
          enddo
          call FeQuestEventSbwReset(SbwActive)
        endif
      endif
      return
      end
