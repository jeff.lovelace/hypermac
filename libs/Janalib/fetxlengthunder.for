      function FeTxLengthUnder(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaPom,VetaT
      VetaT=Veta
      i=index(VetaT,Tabulator)
      if(NTabs(UseTabs).le.0.or.i.le.0) then
        j=index(VetaT,'%')
        if(j.gt.0) then
          if(j.gt.1) then
            if(VetaT(j-1:j-1).ne.ObrLom) then
              if(j+1.le.idel(VetaT)) then
                VetaPom=VetaT(:j-1)//VetaT(j+1:)
                j=idel(VetaPom)+1
              else
                VetaPom=VetaT(:j-1)
              endif
            endif
          else
            VetaPom=VetaT(2:)
            j=idel(VetaPom)+1
          endif
        else
          VetaPom=VetaT
          j=idel(VetaPom)+1
        endif
        FeTxLengthUnder=FeTxLengthSpace(VetaPom(:j-1))
      else
        k=1
        it=0
        xm=0.
1100    i=index(VetaT(k:),Tabulator)
        if(i.le.0) go to 1500
        k=k+i
        it=it+1
        xm=xm+XTabs(it,UseTabs)
        if(it.gt.1) xm=xm-XTabs(it-1,UseTabs)
        go to 1100
1500    if(it.gt.0) then
          if(TTabs(it,UseTabs).eq.IdLeftTab) then
            FeTxLengthUnder=XTabs(it,UseTabs)
            go to 9999
          else if(TTabs(it,UseTabs).eq.IdCharTab) then
            i=index(VetaT(k:),ChTabs(it,UseTabs))
            if(i.gt.1) then
              xmp=xm-FeTxLength(VetaT(k:k+i-2))
            else
              xmp=xm
            endif
          else if(TTabs(it,UseTabs).eq.IdCenterTab) then
            xmp=xm-.5*FeTxLength(VetaT(k:))
          else
            xmp=xm
          endif
          FeTxLengthUnder=xmp+FeTxLength(VetaT(k:))
        else
          FeTxLengthUnder=FeTxLength(VetaT)
        endif
      endif
9999  return
      end
