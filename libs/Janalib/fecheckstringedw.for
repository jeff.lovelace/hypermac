      subroutine FeCheckStringEdw(iw,ich)
      include 'fepc.cmn'
      ich=0
      if(iw.eq.0) go to 9999
      if(EdwType(iw).gt.0) then
        k=0
        call StToReal(EdwString(iw),k,EdwReal(1,iw),
     1                mod(EdwType(iw),10),.true.,ich)
      else if(EdwType(iw).lt.0) then
        k=0
        call StToInt(EdwString(iw),k,EdwInt(1,iw),-EdwType(iw),.true.,
     1               ich)
      endif
      if(ich.eq.0) then
        if(EdwUpDown(iw).ne.0) call FeCheckEud(iw,0)
        if(EdwCheck(iw).ne.0.and.EventType.ne.EventMouse) then
          CheckType=EventEdw
          CheckNumber=iw-EdwFr+1
          ich=2
        endif
      else
        EventType=EventEdw
        EventNumber=iw-EdwFr+1
        EventNumberAbs=iw
        KurzorClick=-1
        ich=1
      endif
9999  return
      end
