      subroutine FeMakeAcWin(XAcLeft,XAcRight,YAcDown,YAcUp)
      include 'fepc.cmn'
      XMinAcWin=XMinGrWin+XAcLeft
      XMaxAcWin=XMaxGrWin-XAcRight
      YMinAcWin=YMinGrWin+YAcDown
      YMaxAcWin=YMaxGrWin-YAcUp
      XCenAcWin=(XMinAcWin+XMaxAcWin)*.5
      YCenAcWin=(YMinAcWin+YMaxAcWin)*.5
      XLenAcWin=XMaxAcWin-XMinAcWin
      YLenAcWin=YMaxAcWin-YMinAcWin
      return
      end
