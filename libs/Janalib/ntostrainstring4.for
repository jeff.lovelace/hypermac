      subroutine NToStrainString4(n,String)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String
      dimension ia(4),ifa(4)
      if(n.le.35) then
        call indexc4(n,4,ifa)
        call SetIntArrayTo(ia,4,0)
        do i=1,4
          j=ifa(i)
          ia(j)=ia(j)+1
        enddo
        write(String,'(4i1)') ia
      endif
      return
      end
