      subroutine FeQuestRolMenuMake(id,xt,n1,xw,n2,Text,Justify,xdw,ydw,
     1                              Check)
      include 'fepc.cmn'
      character*(*) Text,Menu(NMenu)
      character*1   Justify
      integer Check,MenuEnable(*)
      yt=QuestYPosition(id,n1)
      yw=QuestYPosition(id,n2)-ydw*.5
      go to 2000
      entry FeQuestAbsRolMenuMake(id,xt,yti,xw,ywi,Text,Justify,xdw,ydw,
     1                            Check)
      yt=yti
      yw=ywi
2000  RolMenuTo=RolMenuTo+1
      RolMenuMax=RolMenuTo
      QuestRolMenuTo(id)=RolMenuTo
      ActiveObjTo=ActiveObjTo+1
      ActiveObjList(ActiveObjTo)=10000*ActRolMenu+RolMenuTo
      RolMenuLastMade=RolMenuTo-RolMenuFr+1
      RolMenuCheck(RolMenuTo)=Check
      RolMenuLabel(RolMenuTo)=Text
      RolMenuJustify(RolMenuTo)=Justify
      RolMenuLabelX(RolMenuTo)=xt+QuestXMin(LastQuest)
      RolMenuLabelY(RolMenuTo)=yt+QuestYMin(LastQuest)
      ym=yw
      call FeRolMenuMake(RolMenuTo,id,xw,ym,xdw,ydw)
      go to 9999
      entry FeQuestRolMenuOpen(iwa,Menu,NMenu,KMenu)
      Klic=0
      go to 2050
      entry FeQuestRolMenuWithEnableOpen(iwa,Menu,MenuEnable,NMenu,
     1                                   KMenu)
      Klic=1
2050  iwp=iwa+RolMenuFr-1
      do i=1,NMenu
        RolMenuText(i,iwp)=Menu(i)
        if(Klic.eq.0) then
          RolMenuEnable(i,iwp)=1
        else
          RolMenuEnable(i,iwp)=MenuEnable(i)
        endif
      enddo
      RolMenuNumber  (iwp)=NMenu
      RolMenuSelected(iwp)=KMenu
      call FeRolMenuOpen(iwp)
      go to 9000
      entry FeQuestRolMenuLock(iwa)
      call FeRolMenuLock(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuDisable(iwa)
      call FeRolMenuDisable(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuClose(iwa)
      iwap=iwa+RolMenuFr-1
      if(RolMenuState(iwap).ne.RolMenuClosed)
     1  call FeQuestRolMenuLabelRemove(iwa)
      call FeRolMenuClose(iwap)
      go to 9999
      entry FeQuestRolMenuRemove(iwa)
      iwap=iwa+RolMenuFr-1
      if(RolMenuState(iwap).ne.RolMenuRemoved)
     1  call FeQuestRolMenuLabelRemove(iwa)
      call FeRolMenuRemove(iwap)
      go to 9000
      entry FeQuestRolMenuActivate(iwa)
      call FeRolMenuActivate(iwa+RolMenuFr-1)
      go to 9000
      entry FeQuestRolMenuDeactivate(iwa)
      call FeRolMenuDeactivate(iwa+RolMenuFr-1)
9000  call FeQuestRolMenuLabelMake(iwa,RolMenuLabel(iwa+RolMenuFr-1))
9999  return
      end
