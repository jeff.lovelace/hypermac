      subroutine FeQuestWaitBegin
      include 'fepc.cmn'
      call FeMouseShape(3)
      if(ButtonOK    .gt.0) call FeButtonDisable(ButtonOK)
      if(ButtonEsc   .gt.0) call FeButtonDisable(ButtonESC)
      if(ButtonCancel.gt.0) call FeButtonDisable(ButtonCancel)
      go to 9999
      entry FeQuestWaitEnd
      call FeMouseShape(3)
      if(ButtonOK    .gt.0) call FeButtonOff(ButtonOK)
      if(ButtonEsc   .gt.0) call FeButtonOff(ButtonESC)
      if(ButtonCancel.gt.0) call FeButtonOff(ButtonCancel)
9999  return
      end
