      subroutine FeEdwHighlight(id)
      include 'fepc.cmn'
      character*256 Veta
      Veta=EdwString(id)
      if(EdwSelFr.eq.EdwSelTo) go to 9999
      n1=max(min(EdwSelFr,EdwSelTo),EdwTextMin(id)-1)
      n2=min(max(EdwSelFr,EdwSelTo),EdwTextMax(id))
      xp=EdwTextXMin(id)+
     1   FeTxLengthSpace(Veta(EdwTextMin(id):n1))
      yp=(EdwYmin(id)+EdwYmax(id))*.5
      call FeGetTextRectangle(xp,yp,Veta(n1+1:n2),'L',1,xpold,
     1                        xkold,ypold,ykold,refx,refy,conx,cony)
      call FePlotMode('E')
      xpold=xp
      xkold=xp+FeTxLengthSpace(Veta(n1+1:n2))
      call FeFillRectangle(xpold,xkold,ypold,ykold,4,0,0,White)
      call FePlotMode('N')
9999  return
      end
