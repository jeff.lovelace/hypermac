      subroutine FeCutNameLength(LongText,ShortText,Length,CutText)
      include 'fepc.cmn'
      character*(*) LongText,ShortText
      integer CutText
      real Length
      if(FeTxLength(LongText).le.Length) then
        ShortText=LongText
      else
        idll=idel(LongText)
        if(CutText.eq.CutTextFromLeft) then
          do i=idll,1,-1
            ShortText='...'//LongText(i:idll)
            if(FeTxLength(ShortText).gt.Length) then
              ShortText='...'//LongText(i+1:idll)
              go to 9999
            endif
          enddo
        else if(CutText.eq.CutTextFromRight.or.CutText.eq.CutTextNone)
     1    then
          do i=idll,1,-1
            if(CutText.eq.CutTextFromRight) then
              ShortText=LongText(:i)//'...'
            else
              ShortText=LongText(:i)
            endif
            if(FeTxLength(ShortText).le.Length) go to 9999
          enddo
        endif
      endif
9999  return
      end
