      subroutine FeLineIntersection(x11,y11,x12,y12,
     1                              x21,y21,x22,y22,
     2                              xint,yint,ich)

      ich=0
      pomx=x12-x11
      pomy=y12-y11
      if(abs(pomx).gt.0.) then
        A1=-pomy/pomx
        B1=1.
        C1=-y11+pomy/pomx*x11
      else
        A1=1.
        B1=0.
        C1=-x11
      endif
      pomx=x22-x21
      pomy=y22-y21
      if(abs(pomx).gt.0.) then
        A2=-pomy/pomx
        B2=1.
        C2=-y21+pomy/pomx*x21
      else
        A2=1.
        B2=0.
        C2=-x21
      endif
      pomx=A1*B2-A2*B1
      if(pomx.ne.0.) then
        pomx=1./pomx
        xint=(-C1*B2+C2*B1)*pomx
        yint=(-A1*C2+A2*C1)*pomx
      else
        ich=1
      endif
      return
      end
