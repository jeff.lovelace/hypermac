      subroutine RAGenEpiKer(NIrr)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      complex Delta,poma,pbc(12),Block,xc(6),argc
      complex, allocatable :: EqA(:,:,:,:),pa(:,:),pb(:)
      integer, allocatable :: NSel(:),NEqA(:,:)
      logical Pridal,EqRV
      character*80 Veta,t80
      real :: pc(6)=(/0.,0.,0.,0.,0.,0./),xp(36),xm(36)
      real, allocatable :: X4Add(:,:),ZMagAdd(:,:),vt6p(:,:)
      integer MGen(3),KGenP(3)
      nd=NDimIrrep(NIrr)
      if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
        ndp=nd/2
      else
        ndp=nd
      endif
      allocate(NSel(NSymmS),NEqA(nd,NSymmS),X4Add(nd,NSymmS),
     1         ZMagAdd(nd,NSymmS),EqA(2*nd,nd,nd,NSymmS),
     2         pa(2*TriN,TriN),pb(2*TriN),vt6p(6,NLattVec(KPhase)))
      X4Add=0.
      ZMagAdd=1.
      EqA=(0.,0.)
      pbc=(0.,0.)
      NEqA=0
      do i=1,NSymmS
        NSel(i)=0
        if(KSymmLG(i).gt.0) then
          k=1
        else
          do k=1,ndp
            if(abs(EigenValIrrep(k,i,NIrr)-(1.,0.)).lt..001) go to 1200
          enddo
          cycle
        endif
1200    NSel(i)=NSel(i)+1
        Delta=EigenValIrrep(k,i,NIrr)
        if(NDimI(KPhase).le.0) then
          if(abs(Delta-(1.,0.)).gt..0001.and.
     1       abs(Delta-(1.,0.)).lt..01) then
            Delta=1.
          else if(abs(Delta+(1.,0.)).gt..0001.and.
     1            abs(Delta+(1.,0.)).lt..01) then
            Delta=-1.
          else if(abs(Delta).gt..0001.and.
     1            abs(Delta).lt..01) then
            Delta=0.
          endif
          if(abs(Delta-(1.,0.)).lt..001) then
            do j=1,NSel(i)-1
              if(abs(ZMagAdd(j,i)-1.).lt..001) then
                NSel(i)=NSel(i)-1
                go to 1300
              endif
            enddo
            ZMagAdd(NSel(i),i)= 1.
          else if(abs(Delta+(1.,0.)).lt..001) then
            do j=1,NSel(i)-1
              if(abs(ZMagAdd(j,i)+1.).lt..001) then
                NSel(i)=NSel(i)-1
                go to 1300
              endif
            enddo
            ZMagAdd(NSel(i),i)=-1.
          else
            NSel(i)=NSel(i)-1
            go to 1300
          endif
        else
          call qbyx(s6r(1,i),xp,1)
          xp(1)=atan2(aimag(Delta),real(Delta))/(2.*pi)-xp(1)
          call od0do1(xp,xp,1)
          xp(1)=anint(xp(1)*24.)/24.
          X4Add(NSel(i),i)=xp(1)
        endif
        do jj=1,nd
          do ii=1,nd
            EqA(ii,jj,NSel(i),i)=RIrrep(ii+(jj-1)*nd,i,NIrr)
            if(ii.eq.jj) then
              if(ii.le.ndp) then
                poma=Delta
              else
                poma=conjg(Delta)
              endif
              EqA(ii,jj,NSel(i),i)=EqA(ii,jj,NSel(i),i)-poma
            endif
          enddo
        enddo
        NEqA(NSel(i),i)=nd
1300    if(KSymmLG(i).gt.0) then
          if(k.lt.ndp) then
            k=k+1
            go to 1200
          endif
        endif
      enddo
      if(NDimI(KPhase).eq.1) then
        do i=1,NSymmS
          if(KSymmLG(i).gt.0) cycle
          NSel(i)=1
        enddo
      endif
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      do 3000ISub=NSub,1,-1
        ips=IPorSub(ISub)
        if(TakeSub(ips).ne.1) cycle
        do i=1,NSymmS
          if(BratSub(i,ips).le.0.and.BratSymmKer(i,NIrr)) go to 3000
        enddo
        MGenAll=1
        do i=1,NGenSub(ips)
          k=GenSub(i,ips)
          if(BratSymmKer(k,NIrr)) then
            MGen(i)=1
          else
            MGen(i)=NSel(k)
          endif
          MGenAll=MGenAll*MGen(i)
        enddo
        do 2000IGenP=1,MGenAll
          if(NEpi.ge.NEpiMax) then
            allocate(KIrrepEpiO(NEpi),BratSymmEpiO(NSymmS,NEpi),
     1               GrpEpiO(NEpi),NGrpEpiO(NEpi),
     2               TrMatEpiO(NDim(KPhase),NDim(KPhase),NEpi),
     3               ShSgEpiO(NDim(KPhase),NEpi),
     4               ZMagEpiO(NSymmS,NEpi),s6EpiO(6,NSymmS,NEpi),
     5               NSymmEpiO(NEpi),TakeEpiO(NEpi),
     6               NEqEpiO(NEpi),EqEpiO(72,NEpi),
     7               NEqAEpiO(NEpi),EqAEpiO(18,NEpi),
     8               NEqPEpiO(NEpi),EqPEpiO(18,NEpi),EqPPEpiO(6,NEpi),
     9               NCondEpiO(NEpi),CondEpiO(TriN,TriN,NEpi),
     a               NCondEpiAllO(NEpi),CondEpiAllO(TriN,TriN,NEpi))
            do i=1,NEpi
              nd=NDimIrrep(KIrrepEpi(i))
              if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
                ndp=nd/2
              else
                ndp=nd
              endif
              GrpEpiO(i)=GrpEpi(i)
              NGrpEpiO(i)=NGrpEpi(i)
              NEqEpiO(i)=NEqEpi(i)
              NEqAEpiO(i)=NEqAEpi(i)
              NEqPEpiO(i)=NEqPEpi(i)
              NSymmEpiO(i)=NSymmEpi(i)
              KIrrepEpiO(i)=KIrrepEpi(i)
              TakeEpiO(i)=TakeEpi(i)
              NCondEpiO(i)=NCondEpi(i)
              NCondEpiAllO(i)=NCondEpiAll(i)
              call CopyVek(TrMatEpi(1,1,i),TrMatEpiO(1,1,i),
     1                     NDimQ(KPhase))
              call CopyVek(ShSgEpi(1,i),ShSgEpiO(1,i),NDim(KPhase))
              do j=1,NSymmS
                BratSymmEpiO(j,i)=BratSymmEpi(j,i)
                ZMagEpiO(j,i)=ZMagEpi(j,i)
                call CopyVek(s6Epi(1,j,i),s6EpiO(1,j,i),NDim(KPhase))
              enddo
              EqEpiO(1:72,i)=(0.,0.)
              do ii=1,NEqEpi(i)
                do jj=1,nd
                  kk=ii+2*(jj-1)*nd
                  EqEpiO(kk,i)=EqEpi(kk,i)
                enddo
              enddo
              EqAEpiO(1:18,i)=(0.,0.)
              do ii=1,NEqAEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqAEpiO(kk,i)=EqAEpi(kk,i)
                enddo
              enddo
              EqPEpiO(1:18,i)=(0.,0.)
              do ii=1,NEqPEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqPEpiO(kk,i)=EqPEpi(kk,i)
                enddo
                EqPPEpiO(ii,i)=EqPPEpi(ii,i)
              enddo
              call CopyVekC(CondEpi,CondEpiO,TriN**2*NEpi)
              call CopyVekC(CondEpiAll,CondEpiAllO,TriN**2*NEpi)
            enddo
            if(allocated(KIrrepEpi))
     1        deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2                   ShSgEpi,ZMagEpi,s6Epi,NSymmEpi,TakeEpi,
     3                   NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4                   NEqPEpi,EqPEpi,EqPPEpi,
     5                   NCondEpi,CondEpi,NCondEpiAll,CondEpiAll)
            nn=2*NEpiMax
            allocate(KIrrepEpi(nn),BratSymmEpi(NSymmS,nn),GrpEpi(nn),
     1               NGrpEpi(nn),
     2               TrMatEpi(NDim(KPhase),NDim(KPhase),nn),
     3               ShSgEpi(NDim(KPhase),nn),ZMagEpi(NSymmS,nn),
     4               s6Epi(6,NSymmS,nn),NSymmEpi(nn),TakeEpi(nn),
     5               NEqEpi(nn),EqEpi(72,nn),
     6               NEqAEpi(nn),EqAEpi(18,nn),
     7               NEqPEpi(nn),EqPEpi(18,nn),EqPPEpi(6,nn),
     8               NCondEpi(nn),CondEpi(TriN,TriN,nn),
     9               NCondEpiAll(nn),CondEpiAll(TriN,TriN,nn))
            NEpiMax=nn
            do i=1,NEpi
              nd=NDimIrrep(KIrrepEpiO(i))
              if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
                ndp=nd/2
              else
                ndp=nd
              endif
              GrpEpi(i)=GrpEpiO(i)
              NGrpEpi(i)=NGrpEpiO(i)
              KIrrepEpi(i)=KIrrepEpiO(i)
              NEqEpi(i)=NEqEpiO(i)
              NEqAEpi(i)=NEqAEpiO(i)
              NEqPEpi(i)=NEqPEpiO(i)
              NSymmEpi(i)=NSymmEpiO(i)
              TakeEpi(i)=TakeEpiO(i)
              NCondEpi(i)=NCondEpiO(i)
              NCondEpiAll(i)=NCondEpiAllO(i)
              call CopyVek(TrMatEpiO(1,1,i),TrMatEpi(1,1,i),
     1                     NDimQ(KPhase))
              call CopyVek(ShSgEpiO(1,i),ShSgEpi(1,i),NDim(KPhase))
              do j=1,NSymmS
                BratSymmEpi(j,i)=BratSymmEpiO(j,i)
                ZMagEpi(j,i)=ZMagEpiO(j,i)
                call CopyVek(s6EpiO(1,j,i),s6Epi(1,j,i),NDim(KPhase))
              enddo
              EqEpi(1:72,i)=(0.,0.)
              do ii=1,NEqEpiO(i)
                do jj=1,nd
                  kk=ii+2*(jj-1)*nd
                  EqEpi(kk,i)=EqEpiO(kk,i)
                enddo
              enddo
              EqAEpi(1:18,i)=(0.,0.)
              do ii=1,NEqAEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqAEpi(kk,i)=EqAEpiO(kk,i)
                enddo
              enddo
              EqPEpi(1:18,i)=(0.,0.)
              do ii=1,NEqPEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqPEpi(kk,i)=EqPEpiO(kk,i)
                enddo
                EqPPEpi(ii,i)=EqPPEpiO(ii,i)
              enddo
            enddo
            call CopyVekC(CondEpiO,CondEpi,TriN**2*NEpi)
            call CopyVekC(CondEpiAllO,CondEpiAll,TriN**2*NEpi)
            deallocate(KIrrepEpiO,BratSymmEpiO,GrpEpiO,NGrpEpiO,
     1                 TrMatEpiO,ShSgEpiO,ZMagEpiO,s6EpiO,NSymmEpiO,
     2                 NEqEpiO,EqEpiO,NEqAEpiO,EqAEpiO,TakeEpiO,
     3                 NEqPEpiO,EqPEpiO,EqPPEpiO,
     4                 NCondEpiO,CondEpiO,NCondEpiAllO,CondEpiAllO)
            nd=NDimIrrep(NIrr)
            if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
              ndp=nd/2
            else
              ndp=nd
            endif
          endif
          NEpi=NEpi+1
          BratSymmEpi(1:NSymmS,NEpi)=.false.
          ZMagEpi(1:NSymmS,NEpi)=ZMagKer(1:NSymmS,NIrr)
          s6Epi(1:6,1:NSymmS,NEpi)=s6Ker(1:6,1:NSymmS,NIrr)
          KIrrepEpi(NEpi)=NIrr
          TakeEpi(NEpi)=TakeSub(ips)
          NEqEpi(NEpi)=0
          EqEpi(1:72,NEpi)=0.
          BratSymmEpi(1:NSymmS,NEpi)=BratSymmKer(1:NSymmS,NIrr)
          m=IGenP-1
          do i=NGenSub(ips),1,-1
            KGenP(i)=mod(m,MGen(i))+1
            m=m/MGen(i)
          enddo
          NEqAP=0
          EqEpi(1:2*nd**2,NEpi)=(0.,0.)
          do i=1,NGenSub(ips)
            k=GenSub(i,ips)
            if(.not.BratSymmKer(k,NIrr)) then
              if(NDimI(KPhase).eq.1) then
                s6Epi(4,k,NEpi)=X4Add(KGenP(i),k)
                ZMagEpi(k,NEpi)= 1.
              else
                ZMagEpi(k,NEpi)=ZMagAdd(KGenP(i),k)
              endif
              BratSymmEpi(k,NEpi)=.true.
              do ii=1,NEqA(KGenP(i),k)
                NEqAP=NEqAP+1
                do jj=1,nd
                  kk=NEqAP+2*(jj-1)*nd
                  EqEpi(kk,NEpi)=EqA(ii,jj,KGenP(i),k)
                enddo
              enddo
              call trianglC(EqEpi(1,NEpi),pbc,2*nd,nd,NEqAP)
              if(NEqAP.ge.nd) then
                NEpi=NEpi-1
                go to 2000
              endif
            endif
          enddo
          NEqEpi(NEpi)=NEqAP
          NEqAEpi(NEpi)=0
          EqAEpi(1:18,NEpi)=0.
          NEqPEpi(NEpi)=0
          EqPEpi(1:18,NEpi)=0.
          EqPPEpi(1:6,NEpi)=0.
          if(NDimI(KPhase).eq.1) then
            do i=1,NEqEpi(NEpi)
              xc=(0.,0.)
              poma=(0.,0.)
              do j=1,nd
                if(abs(EqEpi(i+2*(j-1)*nd,NEpi)).gt..001) then
                  poma=EqEpi(i+2*(j-1)*nd,NEpi)
                  jp=j
                  exit
                endif
              enddo
              if(abs(poma).gt..001) then
                do j=1,nd
                  xc(j)=EqEpi(i+2*(j-1)*nd,NEpi)/poma
                enddo
              else
                cycle
              endif
              NEqAEpi(NEpi)=NEqAEpi(NEpi)+1
              do j=1,ndp
                kk=NEqAEpi(NEpi)+2*ndp*(j-1)
                EqAEpi(kk,NEpi)=0.
              enddo
              do j=1,ndp
                kk=NEqAEpi(NEpi)+2*ndp*(j-1)
                EqAEpi(kk,NEpi)=0.
              enddo
              do j=1,nd
                kk=NEqAEpi(NEpi)+2*ndp*mod(j-1,ndp)
                if(j.eq.jp) then
                  EqAEpi(kk,NEpi)=1.
                else if(abs(xc(j)).gt..01) then
                  EqAEpi(kk,NEpi)=EqAEpi(kk,NEpi)-1.
                endif
              enddo
              call triangl(EqAEpi(1,NEpi),pc,2*ndp,ndp,NEqAEpi(NEpi))
              Pridal=.false.
              do j=jp+1,nd
                if(abs(xc(j)).gt..01) then
                  if(.not.Pridal) then
                    NEqPEpi(NEpi)=NEqPEpi(NEpi)+1
                    do k=1,ndp
                      kk=NEqPEpi(NEpi)+2*ndp*(k-1)
                      EqPEpi(kk,NEpi)=0.
                    enddo
                    EqPPEpi(NEqPEpi(NEpi),NEpi)=0.
                    kk=NEqPEpi(NEpi)+2*ndp*mod(jp-1,ndp)
                    if(jp.gt.ndp) then
                      pom=-1.
                    else
                      pom= 1.
                    endif
                    EqPEpi(kk,NEpi)=pom
                    Pridal=.true.
                  endif
                  kk=NEqPEpi(NEpi)+2*ndp*mod(j-1,ndp)
                  if(j.gt.ndp) then
                    pom=-1.
                  else
                    pom= 1.
                  endif
                  EqPEpi(kk,NEpi)=EqPEpi(kk,NEpi)-pom
                  EqPPEpi(NEqPEpi(NEpi),NEpi)=
     1              EqPPEpi(NEqPEpi(NEpi),NEpi)+
     2              atan2(-aimag(xc(j)),-real(xc(j)))/(2.*pi)
                endif
              enddo
              call triangl(EqPEpi(1,NEpi),EqPPEpi(1,NEpi),2*ndp,ndp,
     1                     NEqPEpi(NEpi))
            enddo
            if(ndp.eq.2) then
              if(NEqPEpi(NEpi).eq.2) then
                do j=1,ndp
                  kk=2*ndp*(j-1)+1
                  EqPEpi(kk,NEpi)=EqPEpi(kk,NEpi)-EqPEpi(kk+1,NEpi)
                  EqPEpi(kk+1,NEpi)=0.
                enddo
                EqPPEpi(1,NEpi)=EqPPEpi(1,NEpi)-EqPPEpi(2,NEpi)
                EqPPEpi(2,NEpi)=0.
                call od0do1(EqPPEpi(1,NEpi),EqPPEpi(1,NEpi),1)
                NEqPEpi(NEpi)=1
              else if(NEqPEpi(NEpi).eq.1) then
                pom=EqPEpi(1,NEpi)*EqPEpi(2*ndp+1,NEpi)
                if(pom.gt.-.01) then
                  EqPEpi(1:18,NEpi)=0.
                  EqPPEpi(1:6,NEpi)=0.
                  NEqPEpi(NEpi)=0
                else
                  call od0do1(EqPPEpi(1,NEpi),EqPPEpi(1,NEpi),1)
                endif
              endif
            endif
          endif
          NCondEpi(NEpi)=0
          NCondEpiAll(NEpi)=0
          call SetComplexArrayTo(CondEpiAll(1,1,NEpi),TriN**2,(0.,0.))
          call SetComplexArrayTo(CondEpi(1,1,NEpi),TriN**2,(0.,0.))
          nx=NCondKer(NIrr)
          if(nx.le.0) go to 1400
          call SetComplexArrayTo(pa,2*TriN**2,(0.,0.))
          call SetComplexArrayTo(pb,2*TriN,(0.,0.))
          do i=1,nx
            do j=1,TriN
              pa(i,j)=CondKer(i,j,NIrr)
c              CondEpiAll(i,j,NEpi)=CondKer(i,j,NIrr)
            enddo
          enddo
          do is=2,NSymmS
            if(.not.BratSymmEpi(is,NEpi).or.BratSymmKer(is,NIrr)) cycle
            call MatInv(rmr(1,is),xp,det,3)
            if(NDimI(KPhase).gt.0) then
              if(KSymmLG(is).eq.0) then
                eps=-1.
              else
                eps= 1.
              endif
              pom=det
              argc=exp(-cmplx(0.,Pi2*s6Epi(4,is,NEpi)))
            else
              eps=-1.
              pom=det*ZMagEpi(is,NEpi)
              argc=(1.,0.)
            endif
            if(pom.lt.0.) call RealMatrixToOpposite(xp,xp,3)
            do js=2,NSymmS
              if(.not.BratSymmEpi(js,NEpi)) cycle
              ks=PerTab(is,js)
              jp=(NSymmS+1-js)*NSymmBlock+1
              if(NDimI(KPhase).gt.0) jp=jp-3
              do j=1,3
                do i=1,3
                  if(i.eq.j) then
                    pa(nx+4-i,jp-j)=(1.,0.)
                  else
                    pa(nx+4-i,jp-j)=(0.,0.)
                  endif
                enddo
              enddo
              k=0
              kp=(NSymmS+1-ks)*NSymmBlock+1
              if(eps.gt.0.) kp=kp-3
              do j=1,3
                do i=1,3
                  k=k+1
                  pa(nx+4-i,kp-j)=-argc*cmplx(xp(k),0.)
                enddo
              enddo
              nx=nx+3
              if(NDimI(KPhase).gt.0) then
                jp=jp+3
                do j=1,3
                  do i=1,3
                    if(i.eq.j) then
                      pa(nx+4-i,jp-j)=(1.,0.)
                    else
                      pa(nx+4-i,jp-j)=(0.,0.)
                    endif
                  enddo
                enddo
                k=0
                kp=(NSymmS+1-ks)*NSymmBlock+1
                if(eps.lt.0.) kp=kp-3
                do j=1,3
                  do i=1,3
                    k=k+1
                    pa(nx+4-i,kp-j)=-conjg(argc)*cmplx(xp(k),0.)
                  enddo
                enddo
                nx=nx+3
              endif
              call TrianglC(pa,pb,2*TriN,TriN,nx)
              if(nx.ge.NSymmBlock*NSymmS) then
                NEpi=NEpi-1
                go to 2000
              endif
            enddo
          enddo
          do i=1,nx
            do j=1,TriN
              CondEpiAll(i,j,NEpi)=pa(i,j)
            enddo
          enddo
          NCondEpiAll(NEpi)=nx
          nn=0
          do i=1,nx
            m=0
            do j=1,TriN,NSymmBlock
              do k=j,j+NSymmBlock-1
                if(abs(pa(i,k)).gt..01) then
                  m=m+1
                  exit
                endif
              enddo
            enddo
            if(m.gt.2) then
              nn=nn+1
              do j=1,TriN
                CondEpi(nn,j,NEpi)=pa(i,j)
              enddo
            endif
          enddo
          NCondEpi(NEpi)=nn
1400      iz=0
1450      ns=0
          do i=1,NSymmS
            if(BratSymmEpi(i,NEpi)) then
              ns=ns+1
              call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
              call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
              call CopyVek(s6Epi(1,i,NEpi),s6(1,ns,1,KPhase),
     1                     NDim(KPhase))
              zmag(ns,1,KPhase)=ZMagEpi(i,NEpi)
            endif
          enddo
          NSymm(KPhase)=ns
          call RACompleteSymmPlusMagInv(ns,ich)
          if(ich.ne.0) then
            NEpi=NEpi-1
            go to 2000
          endif
          call CrlOrderMagSymmetry
          if(NDimI(KPhase).eq.1.and.QPul.and.iz.eq.0) then
            ns=NSymm(KPhase)
            vt6p(1:NDim(KPhase),1:NLattVec(KPhase))=
     1        vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)
            nvt6p=NLattVec(KPhase)
            SwitchedToComm=.false.
            call ComSym(0,0,ichp)
            call SuperSGToSuperCellSG(ich)
            NSymm(KPhase)=ns
            NDim(KPhase)=4
            NDimI(KPhase)=1
            NDimQ(KPhase)=NDim(KPhase)**2
            NLattVec(KPhase)=nvt6p
            vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)=
     1        vt6p(1:NDim(KPhase),1:NLattVec(KPhase))
            if(ich.ne.0) then
              NEpi=NEpi-1
              go to 2000
            endif
            iz=1
            go to 1450
          endif
          qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
          call FindSmbSgTr(Veta,xp,GrpEpi(NEpi),NGrpEpi(NEpi),
     1                     TrMatEpi(1,1,NEpi),ShSgEpi(1,NEpi),i,i)
          qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
          NSymmEpi(NEpi)=NSymm(KPhase)
          MaxNSymm=NSymm(KPhase)
          do i=1,NSymmS
            do j=1,NSymmN(KPhase)
              if(EqRV(rmr(1,i),rm(1,j,1,KPhase),9,.01)) then
                BratSymmEpi(i,NEpi)=.true.
                call CopyVek(s6(1,j,1,KPhase),s6Epi(1,i,NEpi),
     1                       NDim(KPhase))
                ZMagEpi(i,NEpi)=ZMag(j,1,KPhase)
              endif
            enddo
          enddo
          do 1500i=1,NEpi-1
            if(KIrrepEpi(i).ne.NIrr) cycle
            if(NDimI(KPhase).eq.1) then
              if(NEqAEpi(i).ne.NEqAEpi(NEpi).or.
     1           NEqPEpi(i).ne.NEqPEpi(NEpi)) cycle
            else
              if(NEqEpi(i).ne.NEqEpi(NEpi)) cycle
            endif
            do j=1,NSymmS
              if(BratSymmEpi(j,NEpi)) then
                if(.not.BratSymmEpi(j,i)) go to 1500
                if(NDimI(KPhase).eq.1) then
                  if(abs(s6Epi(4,j,i)-s6Epi(4,j,NEpi)).gt..01)
     1              go to 1500
                else
                  if(abs(ZMagEpi(j,i)-ZMagEpi(j,NEpi)).gt..01)
     1              go to 1500
                endif
              endif
            enddo
            NEpi=NEpi-1
            go to 2000
1500      continue
2000    continue
3000  continue
      call SetIgnoreWTo(.false.)
      call SetIgnoreETo(.false.)
      if(allocated(NSel)) deallocate(NSel,NEqA,X4Add,ZMagAdd,EqA,pa,pb,
     1                               vt6p)
      return
      end
