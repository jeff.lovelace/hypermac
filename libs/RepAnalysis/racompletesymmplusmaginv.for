      subroutine RACompleteSymmPlusMagInv(ns,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrlMagInver
      logical MatRealEqUnitMat
      ich=0
      NSymmN(KPhase)=ns
      MaxNSymmN=NSymm(KPhase)
      NSymm(KPhase)=ns
      call CompleteSymm(0,ich)
      if(ich.ne.0) go to 9999
      if(NDimI(KPhase).eq.1) then
        do i=1,ns
          if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.01).and.
     1      abs(s6(4,i,1,KPhase)-.5).lt..01.and.
     2      abs(ZMag(i,1,KPhase)+1.).lt..01) go to 1500
        enddo
        ns=ns+1
        call ReallocSymm(NDim(KPhase),ns,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call UnitMat(rm6(1,ns,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,ns,1,KPhase),3)
        call SetRealArrayTo(s6(1,ns,1,KPhase),NDim(KPhase),0.)
        s6(4,ns,1,KPhase)=.5
        ZMag(ns,1,KPhase)=-1.
        call CrlMakeRMag(ns,1)
        IswSymm(ns,1,KPhase)=1
        NSymm(KPhase)=ns
1500    call CompleteSymm(0,ich)
        if(ich.ne.0) go to 9999
      else
        if(CrlMagInver().gt.0) then
          ich=1
          go to 9999
        endif
      endif
      MaxNSymm=NSymm(KPhase)
      MaxNSymmN=NSymmN(KPhase)
9999  return
      end
