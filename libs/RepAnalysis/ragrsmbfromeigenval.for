      subroutine RAGrSmbFromEigenVal(NIrr,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xp(4),xpp(4),TrMatP(36),Shift(6),CellParP(6)
      complex Delta
      character*80 Veta,t80
      integer CrSystemNew,CrlCentroSymm
      logical EqRV
      call SetLogicalArrayTo(BratSymmKer(1,NIrr),NSymmS,.false.)
      call SetRealArrayTo(s6Ker(1,1,NIrr),6*NSymmS,0.)
      call SetRealArrayTo(ZMagKer(1,NIrr),NSymmS,1.)
      nd=NDimIrrep(NIrr)
      if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
        ndp=nd/2
      else
        ndp=nd
      endif
      do i=1,NSymmS
        call CopyVek(s6r(1,i),s6Ker(1,i,NIrr),NDim(KPhase))
      enddo
      do 1100i=1,NSymmS
        if(KSymmLG(i).eq.0) cycle
        Delta=EigenValIrrep(1,i,NIrr)
        do j=2,ndp
          if(abs(EigenValIrrep(j,i,NIrr)-Delta).gt..001) go to 1100
        enddo
        BratSymmKer(i,NIrr)=.true.
        if(NDimI(KPhase).le.0) then
          if(abs(Delta-(1.,0.)).lt..001) then
            ZMagKer(i,NIrr)= 1.
          else if(abs(Delta+(1.,0.)).lt..001) then
            ZMagKer(i,NIrr)=-1.
          else
            BratSymmKer(i,NIrr)=.false.
            go to 1100
          endif
        else
          call qbyx(s6r(1,i),xp,1)
          xp(1)=atan2(aimag(Delta),real(Delta))/(2.*pi)-xp(1)
          call od0do1(xp,xp,1)
          Phi=xp(1)
          call CopyVek(s6r(1,i),s6Ker(1,i,NIrr),NDim(KPhase))
          s6Ker(4,i,NIrr)=Phi
        endif
1100  continue
      if(nd.le.2) then
        do i=1,NSymmS
          if(KSymmLG(i).eq.0) then
            BratSymmKer(i,NIrr)=.true.
            exit
          endif
        enddo
      endif
      ns=0
      do i=1,NSymmS
        if(BratSymmKer(i,NIrr)) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          call CopyVek(s6Ker(1,i,NIrr),s6(1,ns,1,KPhase),NDim(KPhase))
          ZMag(ns,1,KPhase)=ZMagKer(i,NIrr)
          IswSymm(ns,1,KPhase)=1
        endif
      enddo
      NSymm(KPhase)=ns
      call RACompleteSymmPlusMagInv(ns,ich)
      if(NDimI(KPhase).eq.1.and..not.QPul) then
        icnt=CrlCentroSymm()
        if(icnt.gt.0) then
          xp=0.
          xp(4)=-s6(4,icnt,1,KPhase)*.5
          do i=1,NSymm(KPhase)
            call MultM(rm6(1,i,1,KPhase),xp,xpp,
     1                 NDim(KPhase),NDim(KPhase),1)
            do j=1,NDim(KPhase)
              s6(j,i,1,KPhase)=s6(j,i,1,KPhase)+xpp(j)-xp(j)
            enddo
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
          enddo
        endif
      endif
      call SetRealArrayTo(ShSgKer(1,NIrr),6,0.)
      qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
      call FindSmbSgTr(Veta,Shift,GrpKer(NIrr),NGrpKer(NIrr),
     1                 TrMatKer(1,1,NIrr),ShSgKer(1,NIrr),i,CrSystemNew)
      qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
      if(NDimI(KPhase).eq.1.and.QPul) then
        CellParP(1:6)=CellPar(1:6,1,KPhase)
        call UnitMat(TrMatP,NDim(KPhase))
        call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),TrMatP)
        call EM50ShowSuperSG(1,t80,Shift,Veta,TrMatP,ShSgKer(1,NIrr))
        k=0
        call Kus(Veta,k,GrpKer(NIrr))
        call MatFromBlock3(TrMatP,TrMatKer(1,1,NIrr),4)
        CellPar(1:6,1,KPhase)=CellParP(1:6)
      endif
      CrSystem(KPhase)=CrSystemNew
      do i=1,NSymmS
        do j=1,NSymmN(KPhase)
          if(EqRV(rm6(1,j,1,KPhase),rm6r(1,i),NDimQ(KPhase),.001).and.
     1       (NDimI(KPhase).eq.0.or.ZMag(j,1,KPhase).gt..001)) then
            BratSymmKer(i,NIrr)=.true.
            call CopyVek(s6(1,j,1,KPhase),s6Ker(1,i,NIrr),
     1                   NDim(KPhase))
            ZMagKer(i,NIrr)=ZMag(j,1,KPhase)
            exit
          endif
        enddo
      enddo
      return
      end
