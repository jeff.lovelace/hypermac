      subroutine UseIrreps
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use RepAnal_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      external RASelEpiKer,FeVoid
      real, allocatable :: rtwo(:,:),ScTwP(:)
      real xr(9),TrMatP(36),MetTensO(9),rmp(9),rmq(9),CellParP(6)
      real, allocatable :: vt6p(:,:)
      character*128 Veta
      character*125 Ven(3)
      character*80  FlnOld,FlnPom,t80
      character*12, allocatable ::  Names(:)
      character*8   SvFile
      character*8, allocatable :: AtomOld(:)
      character*2 :: SmbABCPrime(3)=(/'a''','b''','c'''/)
      complex, allocatable :: pa(:,:),pb(:)
      integer, allocatable :: AddGen(:)
      integer UseTabsIn,SbwItemSelQuest,SbwItemPointerQuest,
     1        CrSystemNew,FeChdir
      logical EqRV,EqRV0,EqRVM,FeYesNo,ExistFile,StructureExists,
     1        ContinueWithOldStructure,Poprve,MatRealEqUnitMat,
     2        FeYesNoHeader,EqIgCase,CrlAcceptTransformation
      NDimOrg=NDim(KPhase)
      if(ExistM90) then
        ParentM90=fln(:ifln)//'.m90'
      else
        ParentM90=' '
      endif
      FromRepAnalysis=.true.
      FromGoToSub=.false.
      ContinueWithOldStructure=.true.
      FlnOld=Fln
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ParentStructure) then
        Veta=fln(:ifln)//'.m43'
        if(ExistFile(Veta)) call CopyFile(Veta,fln(:ifln)//'.m44')
      else
        call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.m44')
      endif
      call iom40Only(0,0,fln(:ifln)//'.m44')
1100  call RAGenIrrep(ich)
      if(QPul) call CopyMat(RCommen(1,1,KPhase),TrMatSuperCell,3)
      if(ich.ne.0) then
        if(ich.eq.2) then
          go to 9300
        else
          go to 9990
        endif
      endif
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      NIrrepSel=0
      call SetLogicalArrayTo(AddSymm,NSymmS,.false.)
      do IrrepSel=1,NIrrep
        if(NIrrepSel.le.0) NIrrepSel=IrrepSel
      enddo
      il=max(NIrrep+2,12)
      xqd=700.
      if(NDimI(KPhase).gt.0) il=max(il,15)
      id=NextQuestId()
      WizardId=id
      WizardMode=.true.
      WizardTitle=.true.
      WizardLength=xqd
      WizardLines=il
      call FeQuestCreate(id,-1.,-1.,xqd,il,'x',0,LightGray,0,0)
1350  id=NextQuestId()
      Veta='Representation analysis:'
      call FeQuestTitleMake(id,Veta)
      il=1
      tpom=5.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom1=tpom+5.
      tpom=tpom+FeTxLength(Veta)+15.
      Veta='Dimension'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom2=tpom+10.
      tpom=tpom+FeTxLength(Veta)+20.
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
      else
        Cislo='space'
      endif
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom3=tpom
      tpom=tpom+180.
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom4=tpom-40.
      tpom=tpom+200.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom5=tpom
      ilp=il
      il=WizardLines
      Veta='%Display representations'
      dpomb=FeTxLengthUnder(Veta)+20.
      xpomb=(xqd-dpomb)*.5
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtDisplayRep=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      LastAtom=1
      Poprve=.true.
1450  if(Poprve) then
        il=ilp
      else
        nLbl=nLblTauFirst
        nButt=nButtShowFirst
      endif
      Ven(1)='Details'
      dpomb=FeTxLength(Ven(1))+20.
      xpomb=640.
      do i=1,NIrrep
        il=il+1
        if(Poprve) then
          call FeQuestLblMake(id,tpom1,il,SmbIrrep(i),'L','N')
          if(i.eq.1) nLblTauFirst=LblLastMade
        endif
        write(Veta,106) NDimIrrep(i)
        if(Poprve) then
          call FeQuestLblMake(id,tpom2,il,Veta,'L','N')
        else
         nLbl=nLbl+1
         call FeQuestLblChange(nLbl,Veta)
         nLbl=nLbl+4
        endif
        if(Poprve) then
          if(NDimI(KPhase).eq.1.and..not.QPul) then
            nd=NDim(KPhase)
          else
            nd=3
          endif
          call FeQuestLblMake(id,tpom3,il,GrpKer(i),'L','N')
          if(QPul) then
            call MatBlock3(TrMatKer(1,1,i),xr,NDim(KPhase))
            call Multm(RCommen(1,1,KPhase),xr,TrMatP,3,3,3)
          else
            call MatBlock3(TrMatKer(1,1,i),TrMatP,NDim(KPhase))
          endif
          call TrMat2String(TrMatP,3,Veta)
          call FeQuestLblMake(id,tpom4,il,Veta,'L','N')
          call OriginShift2String(ShSgKer(1,i),nd,Veta)
          call FeQuestLblMake(id,tpom5,il,Veta,'L','N')
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Ven(1))
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          if(i.eq.1) nButtShowFirst=ButtonLastMade
        else
          call FeQuestButtonOpen(nButt,ButtonOff)
          nButt=nButt+1
        endif
      enddo
      if(Poprve) nButtShowLast=ButtonLastMade
      Poprve=.false.
      call FeQuestButtonDisable(ButtonEsc)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        CheckNumber.ge.nButtShowFirst.and.
     2        CheckNumber.le.nButtShowLast) then
        IrrepSel=CheckNumber-nButtShowFirst+1
        Veta=fln(:ifln)//'.pom'
        call OpenFile(lst,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9990
        LstOpened=.true.
        call NewPg(1)
        IEpi=0
        call RALstOfDetails(1,IrrepSel,IEpi)
        call CloseListing
        HCFileName=SvFile
        call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile)
        call FeListView(Veta,0)
        call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile,0)
        call DeleteFile(SvFile)
        call DeleteFile(Veta)
        HCFileName=' '
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDisplayRep) then
        call RAWriteIrr(SvFile)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.ne.0) go to 9900
      if(NQMag(KPhase).gt.0) then
        call iom50(0,0,fln(:ifln)//'.m50')
        if(QPul) call RASetCommen
        if(ParentStructure) then
          Veta=fln(:ifln)//'.m43'
          if(ExistFile(Veta)) call CopyFile(Veta,fln(:ifln)//'.m44')
        endif
      endif
      call SetLogicalArrayTo(BratSymm(1,KPhase),NSymmS,.true.)
      NSymmP=NSymmS
      go to 2000
      entry CrlMagGoToSubgroupFinish(ichf)
      id=WizardId
      EpiSelFr=1
      EpiSelTo=EpiSelFr+NEpi-1
      ISel=1
      FromRepAnalysis=.false.
      FromGoToSub=.true.
      FlnOld=Fln
      Klic=1
      if(NTwin.gt.1) then
        allocate(rtwo(9,NTwin))
        do i=1,NTwin
          call CopyMat(rtw(1,i),rtwo(1,i),3)
        enddo
      endif
2000  NTwinS=NTwin
      NTwinO=NTwin
      j=mxscu
2050  if(sc(j,1).le.0.) then
        j=j-1
        if(j.gt.1) go to 2050
      endif
      mxscutw=max(j+NTwin-1,6)
      mxscu=mxscutw-NTwin+1
      if(FromGoToSub) go to 3000
      if(NDimI(KPhase).eq.1.and.QPul) then
        nn=2*NSymmS*NLattVec(KPhase)
        if(allocated(NSymmEpiComm))
     1    deallocate(NSymmEpiComm,NLattVecEpiComm,rmEpiComm,s6EpiComm,
     2               vt6EpiComm,ZMagEpiComm)
        allocate(NSymmEpiComm(NEpi),NLattVecEpiComm(NEpi),
     1           rmEpiComm(9,nn,NEpi),s6EpiComm(3,nn,NEpi),
     2           vt6EpiComm(3,nn,NEpi),ZMagEpiComm(nn,NEpi))
        do i=1,NEpi
          ns=0
          do j=1,NSymmS
            if(BratSymmEpi(j,i)) then
              ns=ns+1
              call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
              call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
              call CopyVek(s6Epi(1,j,i),s6(1,ns,1,KPhase),
     1                     NDim(KPhase))
              zmag(ns,1,KPhase)=ZMagEpi(j,i)
              IswSymm(ns,1,KPhase)=1
            endif
          enddo
          allocate(vt6p(6,NLattVec(KPhase)))
          vt6p(1:NDim(KPhase),1:NLattVec(KPhase))=
     1      vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)
          nvt6p=NLattVec(KPhase)
          NSymm(KPhase)=ns
          call RACompleteSymmPlusMagInv(ns,ich)
          call SetRealArrayTo(ShSgEpi(1,i),NDimI(KPhase),0.)
          SwitchedToComm=.false.
          call ComSym(0,0,ichp)
          call CopyMat(MetTens(1,1,KPhase),MetTensO,3)
          call TrMat(RCommen(1,1,KPhase),rmq,3,3)
          call MultM(rmq,MetTens(1,1,KPhase),rmp,3,3,3)
          call MultM(rmp,RCommen(1,1,KPhase),MetTens(1,1,KPhase),3,3,3)
          call SuperSGToSuperCellSG(ich)
          if(ich.eq.0) then
            CellParP(1:6)=CellPar(1:6,1,KPhase)
            call UnitMat(TrMatP,NDim(KPhase))
            call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),
     1                       TrMatP)
            NDim(KPhase)=3
            NDimI(KPhase)=0
            NDimQ(KPhase)=NDim(KPhase)**2
            call CrlOrderMagSymmetry
            call SetIgnoreWTo(.true.)
            call SetIgnoreETo(.true.)
            call FindSmbSgTr(Veta,xr,GrpEpi(i),NGrpEpi(i),TrMatP,
     1                       ShSgEpi(1,i),ii,ii)
            if(NGrpEpi(i).eq.0)
     1        call CrlGetNGrupa(GrpEpi(i),NGrpEpi(i),ii)
            call ResetIgnoreW
            call ResetIgnoreE
            call MatFromBlock3(TrMatP,TrMatEpi(1,1,i),4)
            NSymmEpiComm(i)=NSymm(KPhase)
            NLattVecEpiComm(i)=NLattVec(KPhase)
            do j=1,NLattVec(KPhase)
              call CopyVek(vt6(1,j,1,KPhase),vt6EpiComm(1,j,i),3)
            enddo
            do j=1,NSymm(KPhase)
              call CopyMat(rm6(1,j,1,KPhase),rmEpiComm(1,j,i),3)
              call CopyVek( s6(1,j,1,KPhase),s6EpiComm(1,j,i),3)
              ZMagEpiComm(j,i)=ZMag(j,1,KPhase)
            enddo
            NDim(KPhase)=4
            NDimI(KPhase)=1
            NDimQ(KPhase)=NDim(KPhase)**2
            NLattVec(KPhase)=nvt6p
            vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)=
     1        vt6p(1:NDim(KPhase),1:NLattVec(KPhase))
            CellPar(1:6,1,KPhase)=CellParP(1:6)
          endif
          call CopyMat(MetTensO,MetTens(1,1,KPhase),3)
          deallocate(vt6p)
        enddo
        close(66)
        do i=1,NEpi-1
          do j=i+1,NEpi
            if(EqIgCase(GrpEpi(i),GrpEpi(j)).and.
     1         EqRV(TrMatEpi(1,1,i),TrMatEpi(1,1,j),9,.001).and.
     2         EqRV(ShSgEpi(1,i),ShSgEpi(1,j),3,.001)) then
              NLattVecEpiComm(i)=0
              exit
            endif
          enddo
        enddo
        j=0
        do i=1,NEpi
          if(NLattVecEpiComm(i).le.0.or.i.eq.j) cycle
          nd=NDimIrrep(KIrrepEpi(i))
          if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
            ndp=nd/2
          else
            ndp=nd
          endif
          j=j+1
          GrpEpi(j)=GrpEpi(i)
          NGrpEpi(j)=NGrpEpi(i)
          NEqEpi(j)=NEqEpi(i)
          NEqAEpi(j)=NEqAEpi(i)
          NEqPEpi(j)=NEqPEpi(i)
          NSymmEpi(j)=NSymmEpi(i)
          KIrrepEpi(j)=KIrrepEpi(i)
          TakeEpi(j)=TakeEpi(i)
          NCondEpi(j)=NCondEpi(i)
          NCondEpiAll(j)=NCondEpiAll(i)
          call CopyVek(TrMatEpi(1,1,i),TrMatEpi(1,1,j),NDimQ(KPhase))
          call CopyVek(ShSgEpi(1,i),ShSgEpi(1,j),NDim(KPhase))
          do k=1,NSymmS
            BratSymmEpi(k,j)=BratSymmEpi(k,i)
            ZMagEpi(k,j)=ZMagEpi(k,i)
            call CopyVek(s6Epi(1,k,i),s6Epi(1,k,j),NDim(KPhase))
          enddo
          EqEpi(1:72,j)=(0.,0.)
          do ii=1,NEqEpi(i)
            do jj=1,nd
              kk=ii+2*(jj-1)*nd
              EqEpi(kk,j)=EqEpi(kk,i)
            enddo
          enddo
          EqAEpi(1:18,j)=(0.,0.)
          do ii=1,NEqAEpi(i)
            do jj=1,ndp
              kk=ii+2*(jj-1)*ndp
              EqAEpi(kk,j)=EqAEpi(kk,i)
            enddo
          enddo
          EqPEpi(1:18,j)=(0.,0.)
          do ii=1,NEqPEpi(i)
            do jj=1,ndp
              kk=ii+2*(jj-1)*ndp
              EqPEpi(kk,j)=EqPEpi(kk,i)
            enddo
            EqPPEpi(ii,j)=EqPPEpi(ii,i)
          enddo
          call CopyVekC(CondEpi(1,1,i),CondEpi(1,1,j),TriN**2)
          call CopyVekC(CondEpiAll(1,1,i),CondEpiAll(1,1,j),TriN**2)
          NSymmEpiComm(j)=NSymmEpiComm(i)
          NLattVecEpiComm(j)=NLattVecEpiComm(i)
          do k=1,NSymmEpiComm(i)
            call CopyVek(rmEpiComm(1,k,i),rmEpiComm(1,k,j),9)
            call CopyVek(s6EpiComm(1,k,i),s6EpiComm(1,k,j),3)
            ZMagEpiComm(k,j)=ZMagEpiComm(k,i)
          enddo
          do k=1,NLattVecEpiComm(i)
            call CopyVek(vt6EpiComm(1,k,NEpi),vt6EpiComm(1,k,j),3)
          enddo
        enddo
        NEpi=j
      endif
      id=NextQuestId()
      Veta='List of kernels and epikernels:'
      call FeQuestTitleMake(id,Veta)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_EpiKernels.tmp','formatted',
     1              'unknown')
      NFam=0
      if(allocated(EpiFam)) deallocate(EpiFam,NFamEpi,FamEpi,
     1                                 NSymmFam,OrdFam,OrdEpi)
      allocate(EpiFam(NEpi),NFamEpi(NEpi),FamEpi(NEpi,NEpi),
     1         NSymmFam(NEpi),OrdFam(NEpi),OrdEpi(NEpi))
      call SetIntArrayTo(NFamEpi,NEpi,0)
      call SetIntArrayTo( EpiFam,NEpi,0)
      do i=1,NEpi
        if(EpiFam(i).gt.0) cycle
        NFam=NFam+1
        NFamEpi(NFam)=1
        FamEpi(1,NFam)=i
        EpiFam(i)=NFam
        n=0
        do j=1,NSymmS
          if(BratSymmEpi(j,i)) n=n+1
        enddo
        NSymmFam(NFam)=-NGrpEpi(i)
        do 2100j=i+1,NEpi
          if(EpiFam(j).gt.0) cycle
          do k=1,NSymmS
            if(BratSymmEpi(k,i).neqv.BratSymmEpi(k,j)) go to 2100
          enddo
          NFamEpi(NFam)=NFamEpi(NFam)+1
          FamEpi(NFamEpi(NFam),NFam)=j
          EpiFam(j)=NFam
2100    continue
      enddo
      call indexx(NFam,NSymmFam,OrdFam)
      n=0
      if(NDimI(KPhase).eq.1.and..not.QPul) then
        nd=NDim(KPhase)
      else
        nd=3
      endif
      do im=1,NFam
        ifm=OrdFam(im)
        do j=1,NEpi
          if(EpiFam(j).ne.ifm) cycle
          n=n+1
          OrdEpi(n)=j
          if(QPul) then
            call MatBlock3(TrMatEpi(1,1,j),xr,NDim(KPhase))
            call Multm(RCommen(1,1,KPhase),xr,TrMatP,3,3,3)
          else
            call MatBlock3(TrMatEpi(1,1,j),TrMatP,NDim(KPhase))
          endif
          call TrMat2String(TrMatP,3,t80)
          Veta=GrpEpi(j)(:idel(GrpEpi(j)))//Tabulator//
     1         t80(:idel(t80))
          call OriginShift2String(ShSgEpi(1,j),nd,t80)
          Veta=Veta(:idel(Veta))//Tabulator//t80(:idel(t80))
          Veta=Veta(:idel(Veta))//Tabulator//SmbIrrep(KIrrepEpi(j))
          write(ln,FormA) Veta(:idel(Veta))
          CrSystem(KPhase)=CrSystemS
        enddo
      enddo
      call CloseIfOpened(ln)
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(350.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(490.,UseTabs,IdRightTab,' ')
c      call FeTabsAdd(610.,UseTabs,IdRightTab,' ')
      xpom=15.
      dpom=WizardLength-30.-SbwPruhXd
      il=1
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
        Fract=3.
      else
        Cislo='space'
        Fract=3.
      endif
      tpom=xpom+5.
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+180
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+160.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+120.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+110.
c      Veta='# of conditions'
c      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=WizardLines-2
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwEpikernels=SbwLastMade
      call FeQuestSbwSelectOpen(SbwLastMade,fln(:ifln)//
     1                          '_EpiKernels.tmp')
      il=il+1
      Veta='%Show details'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtDetails=ButtonLastMade
      il=il+1
      tpom=10.
      Veta='Select from above kernels/epikernels one representative'//
     1     ' of a family of Shubnikov '//Cislo(:idel(Cislo))//
     2     ' groups for testing.'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
2500  call FeQuestEventWithCheck(id,ich,RASelEpiKer,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDetails)
     1 then
        i=OrdEpi(SbwItemPointerQuest(nSbwEpikernels))
        Veta=fln(:ifln)//'.pom'
        call OpenFile(lst,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        LstOpened=.true.
        call NewPg(1)
        call RALstOfDetails(-1,KIrrepEpi(i),i)
        call CloseListing
        HCFileName=SvFile
        call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile)
        call FeListView(Veta,0)
        call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile,0)
        call DeleteFile(SvFile)
        call DeleteFile(Veta)
        HCFileName=' '
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(ich.ne.0) then
        if(ich.eq.-1) then
          go to 9900
        else
          go to 1350
        endif
      else
        do i=1,NEpi
          if(SbwItemSelQuest(i,nSbwEpikernels).gt.0) then
            FamSel=EpiFam(OrdEpi(i))
            EpiSelFr=i
            go to 2600
          endif
        enddo
        j=SbwItemPointerQuest(nSbwEpikernels)
        FamSel=EpiFam(OrdEpi(j))
        do i=1,NEpi
          if(EpiFam(OrdEpi(i)).eq.FamSel) then
            EpiSelFr=i
            go to 2600
          endif
        enddo
        FamSel=EpiFam(OrdEpi(1))
        EpiSelFr=1
      endif
2600  EpiSelTo=EpiSelFr+NFamEpi(FamSel)-1
      ISel=OrdEpi(EpiSelFr)
3000  call SetLogicalArrayTo(BratSymm(1,KPhase),NSymmS,.true.)
      NSymmP=NSymmS
      if(allocated(AddGen)) deallocate(AddGen)
      allocate(AddGen(NSymmP))
      call SetLogicalArrayTo(AddSymmOrg,NSymmP,.false.)
      ii=0
      do i=1,NSymmP
        if(BratSymm(i,KPhase)) then
          ii=ii+1
          if(BratSymmEpi(ii,ISel)) then
            AddSymmOrg(i)=.true.
          endif
        endif
      enddo
      call SetIntArrayTo(AddGen,NSymmP,0)
      indc=1
      ii=0
      do i=1,NSymmP
        if(BratSymm(i,KPhase)) ii=ii+1
        if(AddSymmOrg(i)) cycle
        if(BratSymm(i,KPhase)) then
          if(BratSymmEpi(ii,ISel)) then
            cycle
          endif
        endif
        AddGen(indc)=i
        indc=indc+1
        jj=0
        do j=1,NSymmP
          if(BratSymm(j,KPhase)) then
            jj=jj+1
            if(.not.BratSymmEpi(jj,ISel)) cycle
          else
            cycle
          endif
          k=PerTabOrg(i,j)
          AddSymmOrg(k)=.true.
        enddo
      enddo
      ntrans=indc-1
      if(ParentStructure) then
        if(NQMag(KPhase).gt.0) then
          MaxUsedKw(KPhase)=1
        else
          MaxUsedKw(KPhase)=0
        endif
      endif
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      NSymmL(KPhase)=0
      if(allocated(ScTwP)) deallocate(ScTwP)
      allocate(ScTwP(NTwin*indc))
      ScTwP=0.
      ScTwP(1)=1.
      if(isPowder) then
        NTwin=1
        sctw(2:NTwinS,KDatBlock)=0.
        sctw(1,KDatBlock)=1.
        NTwinS=1
      else
        ScTwP(1:NTwinS)=sctw(1:NTwinS,KDatBlock)
      endif
      do i=1,indc-1
        j=AddGen(i)
        call CopyMat(rm6r(1,j),TransMat(1,i,1),NDim(KPhase))
        call CopyVek( s6r(1,j),TransVec(1,i,1),NDim(KPhase))
        if(MagneticType(KPhase).gt.0) TransZM(i)=1.
        call MatBlock3(TransMat(1,i,1),xr,NDim(KPhase))
        if(.not.isPowder) then
          do k=1,NTwinS
            NTwin=NTwin+1
            ScTwP(NTwin)=sctw(k,KDatBlock)
            call multm(rtw(1,k),xr,rtw(1,NTwin),3,3,3)
          enddo
        endif
      enddo
      if(.not.isPowder) then
        j=mxscu
3050    if(sc(j,1).le.0.) then
          j=j-1
          if(j.gt.1) go to 3050
        endif
        mxscutw=max(j+NTwin-1,6)
        mxscu=mxscutw-NTwin+1
      endif
      ns=0
      do i=1,NSymmS
        if(BratSymmEpi(i,ISel)) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          call CopyVek(s6Epi(1,i,ISel),s6(1,ns,1,KPhase),
     1                 NDim(KPhase))
          zmag(ns,1,KPhase)=ZMagEpi(i,ISel)
          IswSymm(ns,1,KPhase)=1
        endif
      enddo
      call RACompleteSymmPlusMagInv(ns,ich)
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      if(NDimI(KPhase).gt.0.and.NSymmN(KPhase).lt.NSymm(KPhase)) then
        if(allocated(isa)) deallocate(isa)
        allocate(isa(NSymm(KPhase),NAtAll))
        do i=1,NAtAll
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        do KPh=1,NPhase
          if(NDim(KPh).gt.3) then
            call SetIntArrayTo(kw(1,1,KPh),3*mxw,0)
            if(NDim(KPh).eq.4) then
              j=mxw
            else
              j=NDimI(KPh)
            endif
            do i=1,j
              kw(mod(i-1,NDimI(KPh))+1,i,KPh)=(i-1)/NDimI(KPh)+1
            enddo
          endif
        enddo
      endif
      NQMagOld=NQMag(KPhase)
      NQMag(KPhase)=0
      Veta=fln(:ifln)//'_test_mag'
      i=NTwin
      j=mxscu
      NTwin=1
      mxscu=6
      mxscutw=6
      call iom40Only(0,0,fln(:ifln)//'.m44')
      NTwin=i
3100  if(sc(j,1).le.0.) then
        j=j-1
        if(j.gt.1) go to 3100
      endif
      mxscutw=max(j+NTwin-1,6)
      mxscu=mxscutw-NTwin+1
      if(ParentStructure) then
        do i=1,NAtInd
          if(kswa(i).ne.KPhase.or.AtTypeMag(isf(i),KPhase).eq.' ') cycle
          if(NDimI(KPhase).eq.1) then
            MagPar(i)=2
          else
            MagPar(i)=1
          endif
          call ReallocateAtomParams(NAtInd,itfmax,IFrMax,KModAMax,
     1                              MagPar(i))
          do j=1,3
             sm0(j,i)=.1/CellPar(j,iswa(i),KPhase)
            ssm0(j,i)=0.
            if(NDimI(KPhase).eq.1) then
               smx(j,1,i)=sm0(j,i)
              ssmx(j,1,i)=0.
               smy(j,1,i)=sm0(j,i)
              ssmy(j,1,i)=0.
            endif
          enddo
        enddo
      endif
      call iom40Only(1,0,fln(:ifln)//'.m44')
      ParentStructure=.false.
      call GoToSubGrMag(Veta,indc,ScTwP,NTwinS)
      if(isPowder) then
        if(ExistFile(fln(:ifln)//'.m95')) then
          WizardMode=.false.
          ExistM90=.false.
          SilentRun=.true.
          call EM9CreateM90(0,ich)
          WizardMode=.true.
          SilentRun=.false.
          call iom90(0,fln(:ifln)//'.m90')
        endif
      endif
      if(QPul) then
        Veta=fln(:ifln)//'_pom'
        call TrSuperMag(0,Veta)
        idl=idel(Veta)
        call MoveFile(Veta(:idl)//'.m40',fln(:ifln)//'.m40')
        call MoveFile(Veta(:idl)//'.m50',fln(:ifln)//'.m50')
        call MoveFile(Veta(:idl)//'.m90',fln(:ifln)//'.m90')
        call MoveFile(Veta(:idl)//'.m95',fln(:ifln)//'.m95')
        if(isPowder)
     1    call MoveFile(Veta(:idl)//'.m41',fln(:ifln)//'.m41')
        call DeleteAllFiles(Veta(:idl)//'*')
        call iom50(0,0,fln(:ifln)//'.m50')
        if(isPowder) call iom41(0,0,fln(:ifln)//'.m41')
      endif
      call iom40(0,0,fln(:ifln)//'.m40')
      call RenameAtomsAccordingToAtomType(NAtCalc)
      call iom40(1,0,fln(:ifln)//'.m40')
      call CrlMagTester(ich)
      if(FromGoToSub) ichf=ich
      if(allocated(TransMat)) call DeallocateTrans
      if(ich.ne.0) then
        if(.not.FromGoToSub) then
          call DeleteAllFiles(Fln(:iFln)//'*')
          Fln=FlnOld
          IFln=idel(Fln)
          if(NQMag(KPhase).gt.0) call SSG2QMag(fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        if(ich.eq.-1) then
          go to 9900
        else
          call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
          if(FromGoToSub) then
            go to 9900
          else
            if(NQMag(KPhase).gt.0) then
              call iom50(0,0,fln(:ifln)//'.m50')
              if(QPul) call RASetCommen
              if(ParentStructure) then
                Veta=fln(:ifln)//'.m43'
                if(ExistFile(Veta))
     1            call CopyFile(Veta,fln(:ifln)//'.m44')
              endif
            endif
            go to 2000
          endif
        endif
      else
        if(FromGoToSub) then
          Veta='Do you want to continue with the last magnetic'
          if(NDimI(KPhase).gt.0) then
            t80=' superspace group?'
          else
            t80=' space group?'
          endif
          Veta=Veta(:idel(Veta))//t80(:idel(t80))
        else
          Veta='Do you want to create test structure for the last '//
     1         'epikernel?'
        endif
        call FeFillTextInfo('crlmagtester.txt',0)
        if(FeYesNoHeader(-1.,-1.,Veta,1)) then
          n=0
5000      n=n+1
          write(Cislo,'(i2)') n
          if(Cislo(1:1).eq.' ') Cislo(1:1)='0'
          FlnPom=FlnOld(:idel(FlnOld))//'_'//Cislo(1:2)
          if(StructureExists(FlnPom)) go to 5000
5100      call FeFileManager('Select structure name',FlnPom,' ',1,
     1                       .true.,ich)
          if(ich.le.0) then
            call CrlOrderMagSymmetry
            if(StructureExists(FlnPom)) then
              call ExtractFileName(FlnPom,Veta)
              if(.not.FeYesNo(-1.,-1.,'The structure "'//
     1           Veta(:idel(Veta))//'" already exists, rewrite it?',
     2           0)) go to 5100
            endif
            i=idel(FlnPom)
            call MoveFile(fln(:ifln)//'.m40',FlnPom(:i)//'.m40')
            call MoveFile(fln(:ifln)//'.m50',FlnPom(:i)//'.m50')
            if(ExistFile(fln(:ifln)//'.m41'))
     1        call MoveFile(fln(:ifln)//'.m41',FlnPom(:i)//'.m41')
            ExistM90=ExistFile(fln(:ifln)//'.m90')
            ExistM95=ExistFile(fln(:ifln)//'.m95')
            if(ExistM95)
     1        call MoveFile(fln(:ifln)//'.m95',FlnPom(:i)//'.m95')
            if(isPowder) then
              if(ExistM90)
     1          call MoveFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
            else
              if(ExistM90)
     1          call MoveFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
            endif
            call DeletePomFiles
            call DeleteAllFiles(fln(:ifln)//'.*')
            call ExtractDirectory(FlnPom,Veta)
            i=FeChdir(Veta)
            call FeGetCurrentDir
            call ExtractFileName(FlnPom,Fln)
            ifln=idel(fln)
            call FeBottomInfo(' ')
            call DeletePomFiles
            KPhase=1
            KPhaseBasic=1
            KDatBlock=1
            call iom40(0,0,fln(:ifln)//'.m40')
            do i=1,NAtCalc
              if(itf(i).gt.1) call ZmTF21(i)
            enddo
            call iom40(1,0,fln(:ifln)//'.m40')
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
5200        read(lni,FormA,end=5220) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 5200
5220        call CloseIfOpened(lni)
            Veta='fixed xyz *'
            write(lno,FormA) Veta(:idel(Veta))
            Veta='restric *'
            if(NDimI(KPhase).gt.0) then
              Cislo=' 13'
            else
              Cislo=' 12'
            endif
            write(lno,FormA) Veta(:idel(Veta))//Cislo(:3)
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            NacetlInt(nCmdncykl)=100
            NacetlReal(nCmdtlum)=.1
            NacetlInt(nCmdLSMethod)=2
            NacetlReal(nCmdMarqLam)=.001
            call RefRewriteCommands(1)
            ContinueWithOldStructure=.false.
            call iom50(0,0,fln(:ifln)//'.m50')
            call iom40(0,0,fln(:ifln)//'.m40')
          else
            go to 7000
          endif
        else
          go to 7000
        endif
        if(NDimOrg.eq.3) then
          call CopyMat(TrMatEpi(1,1,EpiSel),TrMatP,3)
        else
          call MatBlock3(TrMatEpi(1,1,EpiSel),TrMatP,NDimOrg)
        endif
        if(.not.MatRealEqUnitMat(TrMatP,3,.001).or.
     1     .not.EqRV0(ShSgEpi(1,EpiSel),3,.0001)) then
          Veta='The following transformation can bring the structure '//
     1         'to the standard setting:'
          if(.not.CrlAcceptTransformation(Veta,TrMatP,ShSgEpi(1,EpiSel),
     1                                    3,SmbABC,0)) go to 7100
          if(QPul) then
            call MatBlock3(TrMatEpi(1,1,EpiSel),TrMatP,4)
          else
            call CopyMat(TrMatEpi(1,1,EpiSel),TrMatP,NDim(KPhase))
          endif
          call DRCellDefinedTrans(TrMatP)
          if(.not.EqRV0(ShSgEpi(1,EpiSel),3,.0001))
     1      call OriginDefinedShift(ShSgEpi(1,EpiSel))
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase.or.MagPar(i).eq.0) cycle
            call CrlMagTesterSetAtom(i,.1,.false.)
          enddo
          call iom40(1,0,fln(:ifln)//'.m40')
        endif
      endif
      go to 7100
7000  call DeleteAllFiles(Fln(:iFln)//'*')
      ContinueWithOldStructure=.true.
7100  if(allocated(AddGen)) deallocate(AddGen)
      go to 9900
9300  call FeUnforeseenError('searching for physically irreducible '//
     1                       'representation faild.')
9900  call FeQuestRemove(id)
      WizardMode=.false.
      if(FromGoToSub) go to 9999
      if(allocated(KIrrepEpi))
     1  deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2             ShSgEpi,ZMagEpi,s6Epi,NCondEpi,CondEpi,NCondEpiAll,
     3             CondEpiAll,NSymmEpi,NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4             NEqPEpi,EqPEpi,EqPPEpi,TakeEpi)
9990  if(allocated(NDimIrrep))
     1  deallocate(NDimIrrep,RIrrep,EigenValIrrep,EigenVecIrrep,
     2             SmbIrrep,TraceIrrep,NBasFun,
     3             BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,ShSgKer,
     4             TrMatKer,AddSymm,NCondKer,CondKer)
      if(allocated(pa)) deallocate(pa,pb)
      if(allocated(BasFun)) deallocate(BasFun,BasFunOrg,MultIrrep)
      if(allocated(IGen)) deallocate(IGen,KGen,StGen,IoGen,
     1                               PerTab,rm6r,rmr,s6r,ZMagR)
      if(allocated(EpiFam)) deallocate(EpiFam,NFamEpi,FamEpi,
     1                                 NSymmFam,OrdFam,OrdEpi)
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(AtomOld)) deallocate(AtomOld)
      if(allocated(Names)) deallocate(Names)
      if(allocated(PerTabOrg)) deallocate(PerTabOrg,AddSymmOrg,
     1                                    SymmOrdOrg,SymmPorOrg,
     2                                    KSymmLG)
      if(allocated(rtwo)) deallocate(rtwo)
      if(allocated(BratSub)) deallocate(BratSub,NBratSub,IPorSub,GenSub,
     1                                  NGenSub,TakeSub)
      if(allocated(NSymmEpiComm))
     1  deallocate(NSymmEpiComm,NLattVecEpiComm,rmEpiComm,s6EpiComm,
     2             vt6EpiComm,ZMagEpiComm)
      if(allocated(ScTwP)) deallocate(ScTwP)
9999  if(ContinueWithOldStructure) then
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
        Fln=FlnOld
        IFln=idel(Fln)
      else
        if(ExistM95) then
          call DeleteFile(Fln(:ifln)//'.m90')
          ExistM90=.false.
          SilentRun=.true.
          call EM9CreateM90(1,ich)
          SilentRun=.false.
        endif
      endif
      if(allocated(vt6p)) deallocate(vt6p)
      ParentM90=' '
      return
103   format(6(' (',f10.6,',',f10.6,')'))
106   format(i5)
      end
