      subroutine RAGetIrrSymbol(SmbIrr,GrpPar,NGrpPar,Grp,NGrp,IPor,ich)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) SmbIrr,GrpPar,Grp
      character*10  StPom
      integer Type
      logical EqIgCase
      ich=0
      idlg=idel(Grp)
      Type=0
      if(Grp(1:1).eq.'?'.or.QPul) go to 9000
      if(NDimI(KPhase).eq.1) then
        i1=index(Grp,'(')
        i2=index(Grp,')')
        if(i1.le.0.or.i2.le.0.or.i2.lt.i1) go to 9000
        if(NGrpPar.eq.1) then
          SmbIrr='GP1GP1'
        else if(NGrpPar.eq.2) then
          SmbIrr='GP1'
        else if(NGrpPar.ge.3.and.NGrpPar.le.15) then
          if(EqIgCase(Grp(i1+1:i2-1),'a00').or.
     1       EqIgCase(Grp(i1+1:i2-1),'0b0').or.
     2       EqIgCase(Grp(i1+1:i2-1),'00g')) then
            SmbIrr='LD'
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/21/2').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/21/2g')) then
            SmbIrr='U'
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/20').or.
     1            EqIgCase(Grp(i1+1:i2-1),'0b1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='V'
            else
              SmbIrr='U'
            endif
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a01/2').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b0').or.
     2            EqIgCase(Grp(i1+1:i2-1),'01/2g')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='W'
            else
              SmbIrr='U'
            endif
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a0g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'ab0').or.
     2            EqIgCase(Grp(i1+1:i2-1),'0bg')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='F'
            else
              SmbIrr='B'
            endif
            Type=2
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/2g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'ab1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/2bg')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='G'
            else
              SmbIrr='B'
            endif
            Type=2
          else
            go to 9000
          endif
          if(NGrpPar.le.9) then
            ip=i2+1
          else
            if(Type.eq.1) then
              ip=i2+1
            else
              ip=i2+2
            endif
          endif
          if(EqIgCase(Grp(ip:ip),'0')) then
            SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
          else if(EqIgCase(Grp(ip:ip),'s')) then
            SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
          else
            go to 9000
          endif
          if((NGrpPar.ge.3.and.NGrpPar.le.5.and.Type.eq.1).or.
     1       (NGrpPar.ge.6.and.NGrpPar.le.8.and.Type.eq.2)) go to 3000
        else if(NGrpPar.ge.16.and.NGrpPar.le.74) then
          if(EqIgCase(Grp(1:1),'P')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a1/20')) then
              SmbIrr='C'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01/2')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a1/21/2')) then
              SmbIrr='E'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b0')) then
              SmbIrr='D'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1/2')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b1/2')) then
              SmbIrr='P'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
              SmbIrr='G'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'01/2g')) then
              SmbIrr='H'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/21/2g')) then
              SmbIrr='Q'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'C')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01/2')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1/2')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'10g').or.
     1              EqIgCase(Grp(i1+1:i2-1),'01g')) then
              SmbIrr='H'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'A')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='LD'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01').or.
     1              EqIgCase(Grp(i1+1:i2-1),'a10')) then
              SmbIrr='H'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b0')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='SM'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
              SmbIrr='A'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'F')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01').or.
     1              EqIgCase(Grp(i1+1:i2-1),'a10')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1').or.
     1              EqIgCase(Grp(i1+1:i2-1),'1b0')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'10g').or.
     1              EqIgCase(Grp(i1+1:i2-1),'01g')) then
              SmbIrr='H'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'I')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else
              go to 9000
            endif
          endif
          if(NGrpPar.ge.16.and.NGrpPar.le.24) then
            ip=i2+Type
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else
              go to 9000
            endif
          else if(NGrpPar.ge.25.and.NGrpPar.le.46) then
            if(Type.eq.3) then
              ip=i2+1
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1           EqIgCase(Grp(ip:ip+2),'qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1                EqIgCase(Grp(ip:ip+4),'-q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1                EqIgCase(Grp(ip:ip+3),'-qqs')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1                EqIgCase(Grp(ip:ip+3),'q-qs')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
              go to 3000
            else
              ip=i2+3-Type
              if(EqIgCase(Grp(ip:ip),'0').or.
     1           EqIgCase(Grp(ip:ip),'q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip),'s').or.
     1                EqIgCase(Grp(ip:ip+1),'-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else
                go to 9000
              endif
            endif
          else if(NGrpPar.ge.47.and.NGrpPar.le.74) then
            ip=i2+1
            if(Type.eq.3) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1                EqIgCase(Grp(ip:ip+2),'qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1           EqIgCase(Grp(ip:ip+4),'-q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1                EqIgCase(Grp(ip:ip+3),'q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s00').or.
     1                EqIgCase(Grp(ip:ip+3),'-qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            else if(Type.eq.2) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1           EqIgCase(Grp(ip:ip+2),'q0q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1                EqIgCase(Grp(ip:ip+4),'-q0-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1                EqIgCase(Grp(ip:ip+3),'-q0q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s00').or.
     1                EqIgCase(Grp(ip:ip+3),'q0-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            else if(Type.eq.1) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1                EqIgCase(Grp(ip:ip+2),'0qq')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1           EqIgCase(Grp(ip:ip+4),'0-q-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1                EqIgCase(Grp(ip:ip+3),'0q-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1                EqIgCase(Grp(ip:ip+3),'0-qq')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            endif
          else
            go to 9000
          endif
        else if(NGrpPar.ge.75.and.NGrpPar.le.142) then
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       EqIgCase(Grp(i1+1:i2-1),'0b0')) then
            SmbIrr='LD'
          else if(EqIgCase(Grp(i1+1:i2-1),'1/21/2g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b1/2')) then
            if(EqIgCase(GrpPar(1:1),'I')) then
              SmbIrr='W'
            else
              SmbIrr='V'
            endif
          else
            go to 9000
          endif
          if(NGrpPar.ge.75.and.NGrpPar.le.80) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+1),'-q')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip),'q')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.81.and.NGrpPar.le.82) then
            if(NGrp.lt.81) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              go to 3000
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            endif
          else if(NGrpPar.ge.83.and.NGrpPar.le.88) then
            ip=i2+1
            if(NGrp.lt.81) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1               SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.89.and.NGrpPar.le.98) then
            ip=i2+1
            if(NGrp.lt.89) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'//
     1                 SmbIrr(:idel(SmbIrr))//'2'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1                 SmbIrr(:idel(SmbIrr))//'4'
              endif
            else
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip),'s')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+1),'-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip),'q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            endif
          else if(NGrpPar.ge.99.and.NGrpPar.le.110) then
            ip=i2+1
            if(NGrp.lt.99) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+2),'000').or.
     1              EqIgCase(Grp(ip:ip+4),'-q-qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1              EqIgCase(Grp(ip:ip+3),'q-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1              EqIgCase(Grp(ip:ip+2),'qqs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1              EqIgCase(Grp(ip:ip+3),'-qq0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.111.and.NGrpPar.le.122) then
            ip=i2+1
            if(NGrp.lt.111) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1               SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+2),'000').or.
     1              EqIgCase(Grp(ip:ip+3),'0-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1              EqIgCase(Grp(ip:ip+2),'0q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.123.and.NGrpPar.le.142) then
            ip=i2+1
            if(NGrp.lt.123) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+3),'0000').or.
     1              EqIgCase(Grp(ip:ip+5),'-q0-qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'s00s').or.
     1              EqIgCase(Grp(ip:ip+4),'q0-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+3),'s0s0').or.
     1              EqIgCase(Grp(ip:ip+4),'-q0q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+3),'00ss').or.
     1              EqIgCase(Grp(ip:ip+3),'q0qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
          endif
        else if(NGrpPar.ge.143.and.NGrpPar.le.167) then
          n=0
          do i=1,3
            if(abs(qu(i,1,1,KPhase)).lt..001) n=n+1
          enddo
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       (EqIgCase(Grp(i1+1:i2-1),'abg').and.n.ge.2)) then
            if(EqIgCase(GrpPar(1:1),'R')) then
              SmbIrr='LD'
            else
              SmbIrr='DT'
            endif
          else if(EqIgCase(Grp(i1+1:i2-1),'1/31/3g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'abg')) then
            SmbIrr='P'
          else
            if(EqIgCase(GrpPar(1:1),'R')) then
              SmbIrr='LD'
            else
              SmbIrr='DT'
            endif
          endif
          if(NGrpPar.ge.143.and.NGrpPar.le.146) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'-t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip),'t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
            go to 3000
          else if(NGrpPar.ge.147.and.NGrpPar.le.148) then
            if(NGrp.lt.147) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'//
     1               SmbIrr(:idel(SmbIrr))//'3'
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              go to 3000
            endif
          else if(NGrpPar.ge.149.and.NGrpPar.le.154) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'-t00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'t00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
          else if(NGrpPar.ge.155.and.NGrpPar.le.155) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'-t0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+1),'t0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
          else if(NGrpPar.ge.156.and.NGrpPar.le.159) then
            ip=i2+1
            if(NGrp.lt.156) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1              EqIgCase(Grp(ip:ip+2),'00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
            go to 3000
          else if(NGrpPar.ge.160.and.NGrpPar.le.161) then
            ip=i2+1
            if(NGrp.lt.160) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
            go to 3000
          else if(NGrpPar.ge.162.and.NGrpPar.le.163) then
            ip=i2+1
            if(NGrp.lt.162) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.164.and.NGrpPar.le.165) then
            ip=i2+1
            if(NGrp.lt.164) then
              if(EqIgCase(SmbIrr(1:1),'P')) then
                SmbIrr='P2P3'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              endif
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.166.and.NGrpPar.le.167) then
            ip=i2+1
            if(NGrp.lt.166) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          endif
        else if(NGrpPar.ge.168.and.NGrpPar.le.194) then
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       EqIgCase(Grp(i1+1:i2-1),'abg').or.
     2       EqIgCase(Grp(i1+1:i2-1),'0b0')) then
            SmbIrr='DT'
          else
            go to 9000
          endif
          ip=i2+1
          if(NGrpPar.ge.168.and.NGrpPar.le.173.or.
     1       NGrpPar.ge.177.and.NGrpPar.le.182) then
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip),'t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip),'h')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+1),'-t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+1),'-h')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'6'
            else
              go to 9000
            endif
            if(NGrpPar.lt.177) go to 3000
          else if(NGrpPar.ge.174.and.NGrpPar.le.174) then
            if(NGrp.lt.174) then
              SmbIrr='DT2DT3'
            else if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            endif
          else if(NGrpPar.ge.175.and.NGrpPar.le.176) then
            if(NGrp.lt.175) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr='DT3DT5'
              else
                SmbIrr='DT4DT6'
              endif
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.183.and.NGrpPar.le.186) then
            if(NGrp.lt.183) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr='DT5'
              else
                SmbIrr='DT6'
              endif
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0ss')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'ss0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'s0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.187.and.NGrpPar.le.190) then
            if(NGrp.lt.187) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1              EqIgCase(Grp(ip:ip+2),'0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.191.and.NGrpPar.le.194) then
            if(NGrp.lt.191) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'6'
              endif
            else if(EqIgCase(Grp(ip:ip+3),'0000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'00ss')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+3),'s00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+3),'s0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
          endif
        else
          go to 9000
        endif
      endif
      go to 9900
3000  SmbIrr=SmbIrr(:idel(SmbIrr))//SmbIrr(:idel(SmbIrr))
      go to 9900
9000  ich=1
      go to 9999
9800  SmbIrr='GM'//SmbIrr(:idel(SmbIrr))
9900  SmbIrr='m'//SmbIrr(:idel(SmbIrr))
9999  return
      end
