      subroutine RAIsodistort
      use Atoms_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'refine.cmn'
      character*256 :: Veta,Filter='*.cif *.mcif *.magcif *.txt',
     1                 FileName
      character*256, allocatable :: FileNameArr(:),FileNameArrO(:)
      character*128 ListDir,ListFile
      character*80,  allocatable :: GrpArr(:),BasArr(:),OriginArr(:),
     1                              IrepArr(:),
     2                              GrpArrO(:),BasArrO(:),OriginArrO(:),
     3                              IrepArrO(:),
     4                              StMag(:,:,:)
      character*80 t80,FlnPom,FlnOld,FlnOrg
      character*8 SvFile
      character*8, allocatable :: AtMag(:)
      integer SbwLnQuest,ip(9),SbwItemPointerQuest,FeChdir
      integer, allocatable :: ipor(:),NGrp(:),iporO(:),NGrpO(:)
      logical StructureExists,FeYesNo,ExistFile,EqIgCase
      real QAct(3,3),QPom(3),TrMatP(9)
      real, allocatable :: TrMatArr(:,:,:),TrMatArrO(:,:,:)
      FromRepAnalysis=.true.
      FromGoToSub=.false.
      LstOpened=.false.
      FlnOrg=Fln
      call SSG2QMag(fln)
      NQMagIn=NQMag(KPhase)
      if(NQMagIn.gt.0) then
        ISymmFull=0
        call CrlStoreSymmetry(ISymmFull)
      endif
      call iom40Only(0,0,fln(:ifln)//'.m40')
      Veta=fln(:ifln)//'.cif'
      MagneticType(KPhase)=0
      call CIFMakeBasicTemplate(Veta,0)
      call CIFUpdate(Veta,ParentStructure,0)
      MagneticType(KPhase)=1
      call QMag2SSG(Fln,0)
      call iom40Only(0,0,fln(:ifln)//'.m44')
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      id=NextQuestId()
      Veta='ISODISTORT => Jana2006'
      Id=NextQuestId()
      xqd=600.
      ilm=17
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,Veta,0,LightGray,-1,-1)
      QPul=.true.
      do j=1,NQMagIn
        do i=1,3
          pom=QMag(i,j,KPhase)*2.
          if(abs(anint(pom)-pom).gt..00001) then
            QPul=.false.
            exit
          endif
        enddo
      enddo
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(350.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(490.,UseTabs,IdRightTab,' ')
      xpom=15.
      dpom=xqd-30.-SbwPruhXd
      il=1
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
        Fract=3.
      else
        Cislo='space'
        Fract=3.
      endif
      tpom=xpom+5.
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+180
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+160.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+120.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+110.
      il=ilm-4
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwSubgroups=SbwLastMade
      il=il+1
      Veta='Run %ISODISTORT'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtIsodistort=ButtonLastMade
      Veta='%Refresh the list'
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
      il=il+1
      Veta='%Start graphic simulation'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=xqd*.5-1.5*dpom-15.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtGraphic=ButtonLastMade
          Veta='S%tart profile simulation'
        else if(i.eq.2) then
          nButtPowder=ButtonLastMade
          Veta='Sho%w details'
        else if(i.eq.3) then
          nButtDetails=ButtonLastMade
        endif
        if(i.eq.2.and..not.isPowder)
     1    call FeQuestButtonDisable(ButtonLastMade)
        xpom=xpom+dpom+20.
      enddo
      il=il+1
      Veta='%Continue with the selected isotropic subgroup'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRun=ButtonLastMade
      xpom=xpom+dpom+15.
      Veta='%Quit the ISODISTORT tool'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtQuit=ButtonLastMade
      nMagCIFMax=10
      if(allocated(FileNameArr)) deallocate(FileNameArr,GrpArr,BasArr,
     1                                      OriginArr,IrepArr,NGrp,ipor,
     2                                      TrMatArr)
      allocate(FileNameArr(nMagCIFMax),GrpArr(nMagCIFMax),
     1         BasArr(nMagCIFMax),OriginArr(nMagCIFMax),
     2         IRepArr(nMagCIFMax),NGrp(nMagCIFMax),ipor(nMagCIFMax),
     3         TrMatArr(3,3,nMagCIFMax))
1100  nMagCIF=0
      call FeDir(ListDir,ListFile,Filter,0)
      ln=NextLogicNumber()
      call OpenFile(ln,ListFile,'formatted','old')
      if(ErrFlag.ne.0) go to 1500
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_magCIF.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 1500
      lni=NextLogicNumber()
1110  read(ln,FormA,end=1200,err=1200) FileName
      call OpenFile(lni,FileName,'formatted','unknown')
1120  read(lni,FormA,end=1160,err=1160) Veta
      if(LocateSubstring(Veta,'ISODISTORT',.false.,.true.).gt.0) then
        if(nMagCIF.ge.nMagCIFMax) then
          allocate(FileNameArrO(nMagCIF),GrpArrO(nMagCIF),
     1             BasArrO(nMagCIF),OriginArrO(nMagCIF),
     2             IRepArrO(nMagCIF),NGrpO(nMagCIF),iporO(nMagCIF),
     3             TrMatArrO(3,3,nMagCIF))
          FileNameArrO(1:nMagCIF)=FileNameArr(1:nMagCIF)
          GrpArrO(1:nMagCIF)=GrpArr(1:nMagCIF)
          BasArrO(1:nMagCIF)=BasArr(1:nMagCIF)
          OriginArrO(1:nMagCIF)=OriginArr(1:nMagCIF)
          IrepArrO(1:nMagCIF)=IRepArr(1:nMagCIF)
          NGrpO(1:nMagCIF)=NGrp(1:nMagCIF)
          iporO(1:nMagCIF)=ipor(1:nMagCIF)
          TrMatArrO(1:3,1:3,1:nMagCIF)=TrMatArr(1:3,1:3,1:nMagCIF)
          deallocate(FileNameArr,GrpArr,BasArr,
     1               OriginArr,IrepArr,NGrp,ipor,TrMatArr)
          n=nMagCIFMax+10
          allocate(FileNameArr(n),GrpArr(n),BasArr(n),OriginArr(n),
     1             IRepArr(n),NGrp(n),ipor(n),TrMatArr(3,3,n))
          FileNameArr(1:nMagCIF)=FileNameArrO(1:nMagCIF)
          GrpArr(1:nMagCIF)=GrpArrO(1:nMagCIF)
          BasArr(1:nMagCIF)=BasArrO(1:nMagCIF)
          OriginArr(1:nMagCIF)=OriginArrO(1:nMagCIF)
          IrepArr(1:nMagCIF)=IRepArrO(1:nMagCIF)
          NGrp(1:nMagCIF)=NGrpO(1:nMagCIF)
          ipor(1:nMagCIF)=iporO(1:nMagCIF)
          TrMatArr(1:3,1:3,1:nMagCIF)=TrMatArrO(1:3,1:3,1:nMagCIF)
          deallocate(FileNameArrO,GrpArrO,BasArrO,OriginArrO,IrepArrO,
     1               NGrpO,iporO,TrMatArrO)
          nMagCIFMax=nMagCIF+10
        endif
        nMagCIF=nMagCIF+1
        FileNameArr(nMagCIF)=FileName
        BasArr(nMagCIF)='(1,0,0|0,1,0|0,0,1)'
        GrpArr(nMagCIF)='???'
        OriginArr(nMagCIF)='(0,0,0)'
        IRepArr(nMagCIF)='???'
        NGrp(nMagCIF)=0
        ipor(nMagCIF)=0
        call UnitMat(TrMatArr(1,1,nMagCIF),3)
        n=0
1130    read(lni,FormA,end=1160,err=1160) Veta
        if(LocateSubstring(Veta,'IR:',.false.,.true.).gt.0) then
          n=n+1
          k=0
          do i=1,3
            call Kus(Veta,k,IRepArr(nMagCIF))
          enddo
          i=index(IRepArr(nMagCIF),',')
          if(i.gt.0) IRepArr(nMagCIF)(i:i)=' '
        endif
        if(LocateSubstring(Veta,'basis=',.false.,.true.).gt.0) then
          n=n+10
          k=0
          do i=1,4
            call Kus(Veta,k,t80)
          enddo
          nn=1
          do i=1,idel(t80)
            if(t80(i:i).eq.'.') then
              t80(i:i)=' '
              nn=nn+1
            endif
          enddo
          kk=0
          call StToInt(t80,kk,ip,nn,.false.,ich)
          if(ich.ne.0) then
            NGrp(nMagCIF)=0
          else
            m=0
            do i=1,nn
              m=m*1000+ip(i)
            enddo
            NGrp(nMagCIF)=-m
          endif
          call Kus(Veta,k,GrpArr(nMagCIF))
          if(GrpArr(nMagCIF)(2:2).eq.'_')
     1      GrpArr(nMagCIF)=GrpArr(nMagCIF)(1:1)//'['//
     2                      GrpArr(nMagCIF)(3:3)//']'//
     3                      GrpArr(nMagCIF)(4:idel(GrpArr(nMagCIF)))
          idl=idel(GrpArr(nMagCIF))
          if(GrpArr(nMagCIF)(idl:idl).eq.',') then
            GrpArr(nMagCIF)(idl:idl)=' '
            idl=idl-1
          endif
          t80=GrpArr(nMagCIF)
          GrpArr(nMagCIF)=' '
          j=0
          do i=1,idl
            if(t80(i:i).eq.'_') cycle
            j=j+1
            GrpArr(nMagCIF)(j:j)=t80(i:i)
          enddo
          call Kus(Veta,k,BasArr(nMagCIF))
          i1=index(BasArr(nMagCIF),'{')
          i1=max(i1+1,1)
          i2=index(BasArr(nMagCIF),'}')-1
          if(i2.le.0) i2=idel(BasArr(nMagCIF))
          BasArr(nMagCIF)=BasArr(nMagCIF)(i1:i2)
          t80=BasArr(nMagCIF)
          idl=idel(t80)
          BasArr(nMagCIF)='('
          j=1
          do i=2,idl-1
            if(t80(i:i).eq.')') then
              j=j+1
              BasArr(nMagCIF)(j:j)='|'
            else if(t80(i:i).eq.'(') then
              j=j-1
              cycle
            else
              j=j+1
              BasArr(nMagCIF)(j:j)=t80(i:i)
            endif
          enddo
          idl=idel(BasArr(nMagCIF))+1
          BasArr(nMagCIF)(idl:idl)=')'
          do i=1,idel(t80)
            if(t80(i:i).eq.')'.or.t80(i:i).eq.'('.or.
     1         t80(i:i).eq.',') then
              t80(i:i)=' '
            endif
          enddo
          kk=0
          call StToReal(t80,kk,TrMatP,9,.false.,ich)
          if(ich.eq.0) call CopyMat(TrMatP,TrMatArr(1,1,nMagCIF),3)
          call Kus(Veta,k,OriginArr(nMagCIF))
          i1=index(OriginArr(nMagCIF),'(')
          i1=max(i1,1)
          i2=index(OriginArr(nMagCIF),')')
          OriginArr(nMagCIF)=OriginArr(nMagCIF)(i1:i2)
          do i=1,4
            call Kus(Veta,k,t80)
          enddo
          nn=0
          do i=1,idel(t80)
            if(t80(i:i).eq.'('.or.t80(i:i).eq.',') then
              t80(i:i)=' '
            else if(t80(i:i).eq.')') then
              nn=nn+1
              t80(i:i)=' '
            endif
          enddo
          if(nn.lt.NQMagIn) go to 1160
          if(NQMagIn.gt.0) then
            i1p=-2
            i1k= 2
          else
            i1p= 0
            i1k= 0
          endif
          if(NQMagIn.gt.1) then
            i2p=-2
            i2k= 2
          else
            i2p= 0
            i2k= 0
          endif
          if(NQMagIn.gt.2) then
            i3p=-2
            i3k= 2
          else
            i3p= 0
            i3k= 0
          endif
          k=0
          do i=1,nn
            call StToReal(t80,k,QAct(1,i),3,.false.,ich)
            if(ich.ne.0) go to 1160
            do i3=i3p,i3k
              do i2=i2p,i2k
                do i1=i1p,i1k
                  do m=1,3
                    QPom(m)=float(i1)*QMag(m,1,KPhase)
                  enddo
                  if(NQMagIn.gt.1) then
                    do m=1,3
                      QPom(m)=QPom(m)+float(i2)*QMag(m,2,KPhase)
                    enddo
                    if(NQMagIn.gt.2) then
                      do m=1,3
                        QPom(m)=QPom(m)+float(i3)*QMag(m,3,KPhase)
                      enddo
                    endif
                  endif
                  do m=1,3
                    pom=QPom(m)-QAct(m,i)
                    if(abs(pom-anint(pom)).gt..001) go to 1140
                  enddo
                  go to 1150
1140            enddo
              enddo
            enddo
          enddo
          go to 1160
        endif
1150    if(n.lt.11) go to 1130
      endif
      do i=1,nMagCIF-1
        if(EqIgCase(GrpArr(i),GrpArr(nMagCIF)).and.
     1     EqIgCase(BasArr(i),BasArr(nMagCIF)).and.
     2     EqIgCase(OriginArr(i),OriginArr(nMagCIF)).and.
     3     EqIgCase(IRepArr(i),IRepArr(nMagCIF))) go to 1160
      enddo
      go to 1170
1160  nMagCIF=nMagCIF-1
1170  call CloseIfOpened(lni)
      go to 1110
1200  call Indexx(nMagCIF,NGrp,ipor)
      do j=1,nMagCIF
        i=ipor(j)
        write(lno,'(4(a,a1))') GrpArr(i)(:idel(GrpArr(i))),Tabulator,
     1                         BasArr(i)(:idel(BasArr(i))),Tabulator,
     2                   OriginArr(i)(:idel(OriginArr(i))),Tabulator,
     3                       IRepArr(i)(:idel(IRepArr(i))),Tabulator
      enddo
      call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      call FeQuestSbwSelectOpen(nSbwSubgroups,fln(:ifln)//'_magCIF.tmp')
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtIsodistort)
     1  then
        Veta='http://stokes.byu.edu/isodistort.html'
        call FeShellExecute(Veta)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRefresh)
     1  then
        call CloseIfOpened(SbwLnQuest(nSbwSubgroups))
        go to 1100
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtQuit.or.CheckNumber.eq.nButtRun.or.
     2         CheckNumber.eq.nButtPowder.or.
     3         CheckNumber.eq.nButtGraphic.or.
     4         CheckNumber.eq.nButtDetails))then
        if(CheckNumber.eq.nButtQuit) then
          Fln=FlnOrg
          iFln=idel(Fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        else
          FlnOld=Fln
          n=0
2000      n=n+1
          write(Cislo,'(i2)') n
          if(Cislo(1:1).eq.' ') Cislo(1:1)='0'
          FlnPom=Fln(:ifln)//'_'//Cislo(1:2)
          if(StructureExists(FlnPom)) go to 2000
          if(CheckNumber.ne.nButtRun) go to 2110
2100      call FeFileManager('Select structure name',FlnPom,' ',1,
     1                       .true.,ich)
          if(ich.ne.0) go to 1500
          if(StructureExists(FlnPom)) then
            call ExtractFileName(FlnPom,Veta)
            if(.not.FeYesNo(-1.,-1.,'The structure "'//
     1         Veta(:idel(Veta))//'" already exists, rewrite it?',
     2         0)) go to 2100
          endif
2110      if(ExistFile(fln(:ifln)//'.m41'))
     1      call CopyFile(fln(:ifln)//'.m41',
     2                    FlnPom(:idel(FlnPom))//'.m41')
          ISel=ipor(SbwItemPointerQuest(nSbwSubgroups))
          if(QPul.and.NQMagIn.gt.0) then
            call UnitMat(RCommen(1,1,KPhase),3)
            do i=1,3
              k=0
              do j=1,3
                if(abs(TrMatArr(i,j,ISel)).le.0) cycle
                k=k+1
                ip(k)=nint(TrMatArr(i,j,ISel))
              enddo
            enddo
            call MinMultMaxFract(ip,k,MinMult,MaxFract)
            do i=1,9,4
              RCommen(i,1,KPhase)=MinMult
            enddo
            trez(1:3,1,KPhase)=0.
            KCommen(KPhase)=1
            KCommenMax=1
            ICommen(KPhase)=1
            call SetCommen(1,NDimI(KPhase))
            NQMag(KPhase)=0
            ParentStructure=.false.
            ParentM90=fln(:ifln)//'.m90'
            call TrSuperMag(0,FlnPom)
          else
            i=idel(FlnPom)
            call CopyFile(fln(:ifln)//'.m40',FlnPom(:i)//'.m40')
            call CopyFile(fln(:ifln)//'.m50',FlnPom(:i)//'.m50')
            ExistM90=ExistFile(fln(:ifln)//'.m90')
            ExistM95=ExistFile(fln(:ifln)//'.m95')
            if(ExistM95)
     1        call CopyFile(fln(:ifln)//'.m95',FlnPom(:i)//'.m95')
            if(ExistM90)
     1        call CopyFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
          endif
          call DeletePomFiles
          call ExtractDirectory(FlnPom,Veta)
          i=FeChdir(Veta)
          call FeGetCurrentDir
          call ExtractFileName(FlnPom,Fln)
          ifln=idel(fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          if(QPul.and.NQMagIn.gt.0) then
            call CrlRestoreSymmetry(ISymmFull)
            call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
          endif
          call RAImportFromIsodistort(FileNameArr(ISel))
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CrlOrderMagSymmetry
          if(CheckNumber.eq.nButtGraphic) then
            call OpenFile(lst,fln(:ifln)//'.rep','formatted','unknown')
            LstOpened=.true.
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              if(MagPar(i).ne.0)
     1          call CrlMagTesterSetAtom(i,2.,.false.)
              neq=0
              call AtSpec(i,i,0,0,0)
            enddo
            close(lst,status='delete')
            LstOpened=.false.
            MakeCIFForGraphicViewer=.true.
            call iom50(1,0,fln(:ifln)//'.m50')
            call iom40Only(1,0,fln(:ifln)//'.m40')
            call CrlPlotStructure
            MakeCIFForGraphicViewer=.false.
            go to 3000
          else if(CheckNumber.eq.nButtDetails.or.
     1            CheckNumber.eq.nButtPowder) then
            if(OpSystem.ge.0) then
              SvFile='full.pcx'
            else
              SvFile='full.bmp'
            endif
            NAtMag=0
            do i=1,NAtAll
              if(MagPar(i).gt.0) NAtMag=NAtMag+1
            enddo
            if(allocated(AtMag)) deallocate(AtMag,StMag)
            allocate(AtMag(NAtMag),StMag(6,0:MagParMax-1,NAtMag))
            j=0
            do i=1,NAtAll
              if(MagPar(i).gt.0) then
                j=j+1
                AtMag(j)=Atom(i)
              endif
            enddo
            if(CheckNumber.eq.nButtDetails) then
              i=1
            else
              i=0
            endif
            call CrlMagTesterAction(i,SvFile,AtMag,StMag,NAtMag,
     1                              MagParMax-1)
            go to 3000
          else
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
2200        read(lni,FormA,end=2220) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 2200
2220        call CloseIfOpened(lni)
            Veta='fixed xyz *'
            write(lno,FormA) Veta(:idel(Veta))
            Veta='restric *'
            if(NDimI(KPhase).gt.0) then
              Cislo=' 13'
            else
              Cislo=' 12'
            endif
            write(lno,FormA) Veta(:idel(Veta))//Cislo(:3)
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            NacetlInt(nCmdncykl)=100
            NacetlReal(nCmdtlum)=.1
            NacetlInt(nCmdLSMethod)=2
            NacetlReal(nCmdMarqLam)=.001
            call RefRewriteCommands(1)
            call iom50(0,0,fln(:ifln)//'.m50')
            call iom40(0,0,fln(:ifln)//'.m40')
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              if(MagPar(i).ne.0)
     1          call CrlMagTesterSetAtom(i,.1,.false.)
            enddo
            call iom40(1,0,fln(:ifln)//'.m40')
            call iom40(0,0,fln(:ifln)//'.m40')
          endif
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 3100
3000  call DeleteAllFiles(Fln(:iFln)//'*')
      Fln=FlnOld
      iFln=idel(Fln)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      go to 1500
3100  call FeQuestRemove(id)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      if(allocated(FileNameArr)) deallocate(FileNameArr,GrpArr,BasArr,
     1                                      OriginArr,IrepArr,NGrp,ipor,
     2                                      TrMatArr)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(ISymmFull.gt.0) call CrlCleanSymmetry
      return
      end
