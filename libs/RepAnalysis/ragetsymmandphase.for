      subroutine RAGetSymmAndPhase(rms,ss,IFound,Phi)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rms(*),ss(*),sp(3),xp(1)
      logical EqRV
      IFound=0
      do i=1,NSymmS
        if(EqRV(rms,rmr(1,i),9,.001)) then
          IFound=i
          if(NDimI(KPhase).gt.0) then
            do j=1,3
              sp(j)=ss(j)-s6r(j,i)
            enddo
            call qbyx(sp,xp,1)
            phi=pi2*xp(1)
          else
            phi=0.
          endif
        endif
      enddo
      return
      end
