      subroutine RAGenIrrFromLG(Irr,rmq,sq,rmqi,sqi,SelSG,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmq(*),sq(*),rmqi(*),sqi(*),rmp(9),sp(3),rmt(9),st(3)
      real :: XNull(3) = (/0.,0.,0./)
      integer SelSG(*)
      ich=0
      do j=1,NSymmN(KPhase)
        if(SelSG(j).eq.0) then
          i1=2
          j1=1
          call MultSymmOp(rmqi,sqi,rm(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                    rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k1,phi1)
          if(k1.le.0) then
            go to 9100
          else
            if(SelSG(k1).le.0) then
              go to 9100
            endif
          endif
          i2=1
          j2=2
          call MultSymmOp(rm(1,j,1,KPhase),s6(1,j,1,KPhase),rmq,sq,
     1                    rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k2,phi2)
          if(k2.le.0) then
            go to 9100
          else
            if(SelSG(k2).le.0) then
              go to 9100
            endif
          endif
        else
          i1=1
          j1=1
          k1=j
          Phi1=0.
          i2=2
          j2=2
          call MultSymmOp(rmqi,sqi,rm(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                    rmt,st,3)
          call MultSymmOp(rmt,st,rmq,sq,rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k2,phi2)
          if(k2.le.0) then
            go to 9100
          else
            if(SelSG(k2).le.0) go to 9100
          endif
        endif
        n=NDimIrrep(Irr)
        call SetComplexArrayTo(RIrrep(1,j,Irr),4*n**2,(0.,0.))
        ip=(i1-1)*n+1
        jp=(j1-1)*n+1
        call SetMatBlock(RIrrep(1,j,Irr),2*n,ip,jp,RIrrepP(1,k1,Irr),
     1                   Phi1,n)
        ip=(i2-1)*n+1
        jp=(j2-1)*n+1
        call SetMatBlock(RIrrep(1,j,Irr),2*n,ip,jp,RIrrepP(1,k2,Irr),
     1                   Phi2,n)
      enddo
      go to 9999
9100  ich=1
9999  return
      end
