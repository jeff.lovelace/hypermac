      subroutine RASelEpiKer
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer SbwItemSelQuest,SbwItemPointerQuest,SbwItemFromQuest
      ItemFromOld=SbwItemFromQuest(nSbwEpikernels)
      m=SbwItemPointerQuest(nSbwEpikernels)
      if(SbwItemSelQuest(m,nSbwEpikernels).gt.0) then
        j=EpiFam(OrdEpi(m))
      else
        j=0
      endif
      do i=1,NEpi
        if(EpiFam(OrdEpi(i)).eq.j) then
          k=1
        else
          k=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwEpikernels,k)
      enddo
      call FeQuestSbwShow(nSbwEpikernels,ItemFromOld)
      return
      end
