      subroutine RAWriteIrr(SvFile)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) SvFile
      character*256 Veta
      character*80  t80
      character*3 t3
      character*2 t2m
      integer io(3)
      logical ComplexNumber,IntegerNumber
      call NewPg(1)
      call OpenFile(lst,fln(:ifln)//'.rep','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      Uloha='List of irreducible representations'
      do i=1,NSymmS
        call CopyMat(rm6r(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
        call CopyMat(rmr (1,i),rm (1,i,1,KPhase),3)
        call CopyVek(s6r(1,i),s6(1,i,1,KPhase),NDim(KPhase))
        zmag(i,1,KPhase)=ZMagR(i)
      enddo
      NSymm(KPhase)=NSymmS
      call inform50
      do i=1,NIrrep
        call NewLn(4)
        write(Veta,'(''  Dimension:'',i2)') NDimIrrep(i)
        Veta='Representation: '//SmbIrrep(i)(:idel(SmbIrrep(i)))//
     1       Veta(:idel(Veta))
        write(lst,FormA)
        write(lst,FormA) Veta(:idel(Veta))
        write(lst,FormA1)('-',j=1,idel(Veta))
        write(lst,FormA)
        ComplexNumber=.false.
        IntegerNumber=.true.
        do j=1,NSymmS
          do k=1,NDimIrrep(i)**2
            if(abs(aimag(RIrrep(k,j,i))).gt..001) ComplexNumber=.true.
            if(abs(anint(real(RIrrep(k,j,i)))-
     1                   real(RIrrep(k,j,i))).gt..001.or.
     2         abs(anint(aimag(RIrrep(k,j,i)))-
     3                   aimag(RIrrep(k,j,i))).gt..001)
     4        IntegerNumber=.false.
          enddo
        enddo
        nd=NDimIrrep(i)
        Veta='Symmetry operator      Symbol'
        if(ComplexNumber) then
          if(IntegerNumber) then
            idl1=9*nd
            idl2=9
          else
            idl1=15*nd
            idl2=15
          endif
        else
          if(IntegerNumber) then
            idl1=3*nd
            idl2=3
          else
            idl1=7*nd
            idl2=7
          endif
        endif
        if(nd.gt.1) idl1=idl1+2
        if(idl1.gt.6) then
          j=28+idl1/2+1
          ist1=31
        else
          j=31
          ist1=31+(1-idl1/2)
        endif
        ist2=ist1+max(idl1,6)+1
        Veta(j:)='Matrix'
        if(idl2.gt.9) then
          j=ist2-5+idl2/2
        else
          j=ist2
          ist2=ist2+max(3-idl2/2,0)
        endif
        Veta(j:)='Character'
        call NewLn(3)
        write(lst,FormA)
        write(lst,FormA) Veta(:idel(Veta))
        write(lst,FormA)
        do j=1,NSymmS
          call SmbOp(rm6r(1,j),s6r(1,j),1,1.,t3,t2m,io,nn,det)
          Veta=StSymmCard(j)
          Veta(26:)=t3
          call NewLn(nd+1)
          do k=1,nd
            if(ComplexNumber) then
              if(IntegerNumber) then
                write(t80,101)(nint(real(RIrrep(k+(l-1)*nd,j,i))),
     1                         nint(aimag(RIrrep(k+(l-1)*nd,j,i))),
     2                         l=1,nd)
              else
                write(t80,103)(real(RIrrep(k+(l-1)*nd,j,i)),
     1                         aimag(RIrrep(k+(l-1)*nd,j,i)),
     2                         l=1,nd)
              endif
              idl=idel(t80)
              if(t80(idl:idl).eq.'(') t80(idl:idl)=' '
            else
              if(IntegerNumber) then
                write(t80,102)
     1            (nint(real(RIrrep(k+(l-1)*nd,j,i))),l=1,nd)
              else
                write(t80,104)
     1            (real(RIrrep(k+(l-1)*nd,j,i)),l=1,nd)
              endif
            endif
            if(nd.gt.1) t80='|'//t80(:idel(t80))//'|'
            Veta(ist1:)=t80
            if(ComplexNumber) then
              if(IntegerNumber) then
                write(t80,101) nint(real(TraceIrrep(j,i))),
     1                         nint(aimag(TraceIrrep(j,i)))
              else
                write(t80,103) real(TraceIrrep(j,i)),
     1                         aimag(TraceIrrep(j,i))
              endif
              idl=idel(t80)
              if(t80(idl:idl).eq.'(') t80(idl:idl)=' '
            else
              if(IntegerNumber) then
                write(t80,102) nint(real(TraceIrrep(j,i)))
              else
                write(t80,104) real(TraceIrrep(j,i))
              endif
            endif
            Veta(ist2:)=t80
            write(lst,FormA) Veta(:idel(Veta))
            Veta=' '
          enddo
          write(lst,FormA)
        enddo


c        jp=1
c        do ifun=1,(NBasFun(i)-1)/6+1
c          jk=min(jp+5,NBasFun(i))
c          write(lst,FormA)
c          write(lst,'(''Basic function #:'',6(i10,8x))')
c     1               (j,j=jp,jk)
c          write(lst,FormA)
c          do k=1,TriN
c            write(Veta,'(''M'',a1,''#s'',i3)')
c     1        SmbX(mod(k-1,3)+1),(k-1)/NSymmBlock+1
c            call Zhusti(Veta)
c            if(NDimI(KPhase).le.0) then
c              idl=20
c            else
c              if(mod(k-1,NSymmBlock)/3+1.eq.1) then
c                Cislo='+'
c              else
c                Cislo='-'
c              endif
c              Veta=Veta(:idel(Veta))//'*exp('//Cislo(1:1)//'2pi.i.q*r)'
c              idl=25
c            endif
c            do j=jp,jk
c              write(Veta(idl:),103) real(BasFun(k,j,i)),
c     1                              aimag(BasFun(k,j,i))
c              ii=idel(Veta)
c              if(Veta(ii:ii).eq.'(') Veta(ii:ii)=' '
c              idl=idl+18
c            enddo
c            write(lst,FormA) Veta(:idel(Veta))
c          enddo
c          jp=jp+6
c        enddo
      enddo
      call CloseListing
      HCFileName=SvFile
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                 SvFile)
      call FeListView(fln(:ifln)//'.rep',0)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                 SvFile,0)
      call DeleteFile(SvFile)
      HCFileName=' '
9999  return
101   format(6('(',i3,',',i3,')'))
102   format(6i3)
103   format(6('(',f6.3,',',f6.3,')'))
104   format(6f7.3)
      end
