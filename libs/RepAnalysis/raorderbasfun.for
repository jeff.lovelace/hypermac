      subroutine RAOrderBasFun(Fun,io,n,m)
      complex Fun(n,m),FunP(:)
      dimension io(m)
      allocatable FunP
      allocate(FunP(n))
      Kam=0
      do i=1,m-1
        if(io(m).lt.io(i)) then
          Kam=i
          exit
        endif
      enddo
      if(Kam.gt.0) then
        iop=io(m)
        call CopyVekC(Fun(1,m),FunP,n)
        do i=m-1,Kam,-1
          call CopyVekC(Fun(1,i),Fun(1,i+1),n)
          io(i+1)=io(i)
        enddo
        io(Kam)=iop
        call CopyVekC(FunP,Fun(1,Kam),n)
      endif
      if(allocated(FunP)) deallocate(FunP)
      return
      end
