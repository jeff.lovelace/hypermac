      subroutine RAGenSubgroups
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: SelSub(:),is(:),SelPom(:),NGrpSub(:),
     1                        BratSubO(:,:),NBratSubO(:),GenSubO(:,:),
     2                        NGenSubO(:),NRev(:),NRevO(:)
      integer CrlCentroSymm,io(3)
      logical EqIgCase
      real rmi(36),xp(9),Shift(6)
      character*80 Veta,Grp,t80
      character*3  t3
      character*2  t2m
      allocate(SelSub(NSymmS),is(NSymmS),SelPom(NSymmS))
      NSubMax=10
      allocate(BratSub(NSymmS,NSubMax),NBratSub(NSubMax),
     1         GenSub(4,NSubMax),NGenSub(NSubMax),NRev(NSubMax))
      invc=CrlCentroSymm()
      SelPom=0
      nss=0
      do i=1,NSymmS
        if(i.ne.invc) then
          nss=nss+1
          SelPom(nss)=i
        endif
      enddo
      NSub=0
      do i1=1,nss
        i1a=SelPom(i1)
        do i2=1,i1
          if(i1.eq.i2.and.i2.ne.1) cycle
          i2a=SelPom(i2)
          do 5000i3=1,i2
            if((i1.eq.i3.or.i2.eq.i3).and.i3.ne.1) cycle
            i3a=SelPom(i3)
            is=0
            SelSub=0
            SelSub(1)=1
            ns=1
            if(i1.gt.1) then
              ns=ns+1
              SelSub(ns)=i1a
            endif
            if(i2.gt.1) then
              ns=ns+1
              SelSub(ns)=i2a
            endif
            if(i3.gt.1) then
              ns=ns+1
              SelSub(ns)=i3a
            endif
            ic=0
            nsg=ns
1400        is=0
1500        i=0
2000        if(i.ge.ns) go to 2500
            i=i+1
            j=is(i)
2100        if(j.ge.ns) go to 2000
            j=j+1
            is(i)=j
            k=PerTabOrg(SelSub(i),SelSub(j))
            do m=1,ns
              if(k.eq.SelSub(m)) go to 2100
            enddo
            ns=ns+1
            SelSub(ns)=k
            go to 1500
2500        if(NSub.ge.NSubMax) then
              allocate(BratSubO(NSymmS,NSub),NBratSubO(NSub),
     1                 GenSubO(4,NSub),NGenSubO(NSub),NRevO(NSub))
              NBratSubO=NBratSub
              NGenSubO=NGenSub
              NRevO=NRev
              do i=1,NSub
                BratSubO(1:NSymmS,i)=BratSub(1:NSymmS,i)
                GenSubO(1:NGenSub(i),i)=GenSub(1:NGenSub(i),i)
              enddo
              deallocate(BratSub,NBratSub,GenSub,NGenSub,NRev)
              n=NSubMax*2
              allocate(BratSub(NSymmS,n),NBratSub(n),GenSub(4,n),
     1                 NGenSub(n),NRev(n))
              NBratSub(1:NSub)=NBratSubO
              NGenSub(1:NSub)=NGenSubO
              NRev(1:NSub)=NRevO
              do i=1,NSub
                BratSub(1:NSymmS,i)=BratSubO(1:NSymmS,i)
                GenSub(1:NGenSub(i),i)=GenSubO(1:NGenSub(i),i)
              enddo
              NSubMax=n
              deallocate(BratSubO,NBratSubO,GenSubO,NGenSubO,NRevO)
            endif
            NSub=NSub+1
            BratSub(1:NSymmS,NSub)=0
            do i=1,ns
              BratSub(SelSub(i),NSub)=1
            enddo
            NRev(NSub)=0
            if(NDimI(KPhase).eq.1) then
              do i=2,nsg
                if(KSymmLG(SelSub(i)).gt.0) NRev(NSub)=NRev(NSub)+1
              enddo
            endif
            NBratSub(NSub)=ns
            if(NSub.eq.1) go to 3000
            if(invc.gt.0.and.ic.eq.0) then
              if(BratSub(invc,NSub).gt.0) then
                NSub=NSub-1
                go to 5000
              endif
            endif
            do 2600i=1,NSub-1
              do j=1,NSymmS
                if(BratSub(j,NSub).ne.BratSub(j,i)) go to 2600
              enddo
              if(nsg-1.lt.NGenSub(i).or.
     1           (nsg-1.eq.NGenSub(i).and.NRev(NSub).gt.NRev(i))) then
                NGenSub(i)=nsg-1
                do j=2,nsg
                  GenSub(j-1,i)=SelSub(j)
                enddo
                NRev(i)=NRev(NSub)
              endif
              NSub=NSub-1
              go to 3500
2600        continue
3000        NGensub(NSub)=nsg-1
            do i=2,nsg
              GenSub(i-1,NSub)=SelSub(i)
            enddo
3500        if(invc.gt.0.and.ic.eq.0) then
              do i=1,ns
                if(SelSub(i).eq.invc) go to 5000
              enddo
              nsg=nsg+1
              ns=nsg
              SelSub(ns)=invc
              ic=1
              go to 1400
            endif
5000      continue
        enddo
      enddo
      allocate(NGrpSub(NSub),IPorSub(NSub),TakeSub(NSub))
      do i=1,NSub
        ns=0
        do j=1,NSymmS
          if(BratSub(j,i).gt.0) then
            ns=ns+1
            call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,j),rm  (1,ns,1,KPhase),3)
            call CopyMat(rmr (1,j),rmag(1,ns,1,KPhase),3)
            call CopyVek(s6r(1,j),s6(1,ns,1,KPhase),
     1                   NDim(KPhase))
            zmag(ns,1,KPhase)=1.
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        NSymmN(KPhase)=ns
        NSymm(KPhase)=ns
        qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
        call FindSmbSgTr(Veta,Shift,Grp,NGrpSub(i),rmi,xp,kk,kk)
        qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
        NGrpSub(i)=NGrpSub(i)*1000000+30
        call MatBlock3(rmi,xp,NDim(KPhase))
        do j=1,9,4
          NGrpSub(i)=NGrpSub(i)-nint(xp(j))
        enddo
        i6m=0
        i3=0
        do 6000j=1,NSymmN(KPhase)
          call SmbOp(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),1,1.,
     1               t3,t2m,io,nn,pom)
          if(EqIgCase(t3,'-6')) then
            do k=1,NGenSub(i)
              if(GenSub(k,i).eq.j) i6m=j
            enddo
          else if(EqIgCase(t3,'3')) then
            do k=1,NGenSub(i)
              if(GenSub(k,i).eq.j) go to 6000
            enddo
            i3=j
          endif
          if(i6m.gt.0.and.i3.gt.0) then
            NGenSub(i)=NGenSub(i)+1
            GenSub(NGenSub(i),i)=i3
            exit
          endif
6000    continue
      enddo
      TakeSub=1
      do i=1,NSub
        if(TakeSub(i).lt.1) cycle
        NGrpSub(i)=NGrpSub(i)+1000*i
c        write(66,'(70(''=''))')
c        write(66,'('' Rad podgrupy:'',3i5)') NBratSub(i),i
c        write(66,'('' Generatory:'',10i5)')
c     1   (GenSub(j,i),j=1,NGenSub(i))
c        write(66,'(i15)') NGrpSub(i)
c        write(66,'(70(''=''))')
        do 7100j=i+1,NSub
          if(TakeSub(j).lt.1) cycle
          if(NGrpSub(i)/1000000.ne.NGrpSub(j)/1000000) cycle
          do 7000itw=2,NSymmS
            if(BratSub(itw,i).eq.1) go to 7000
            itwi=2
            do while(PerTabOrg(itw,itwi).ne.1)
              itwi=itwi+1
            enddo
            do k=2,NSymmS
              if(BratSub(k,i).ne.1) cycle
              m=PerTab(PerTabOrg(itw,k),itwi)
              if(BratSub(m,j).ne.1) go to 7000
            enddo
            NGrpSub(j)=NGrpSub(j)+1000*i
            TakeSub(j)=0
c            write(66,'('' Eqivalence:'',2i4)') i,j
c            write(66,'('' Rad podgrupy:'',3i5)') NBratSub(j),j
c            write(66,'('' Generatory:'',10i5)')
c     1       (GenSub(jj,j),jj=1,NGenSub(j))
c            write(66,'(i15)') NGrpSub(j)
c            write(66,'(70(''-''))')
            go to 7100
7000      continue
7100    continue
      enddo
      call indexx(NSub,NGrpSub,IPorSub)
      NGrpSubLast=0
      do i=1,NSub
        ii=IPorSub(i)
        k=NGrpSub(ii)/1000
        if(k.ne.NGrpSubLast) then
          TakeSub(ii)=1
          NGrpSubLast=k
        else
          TakeSub(ii)=0
        endif
c        write(66,'('' Rad podgrupy:'',4i5)') NBratSub(ii),i,ii,
c     1                                       TakeSub(ii)
c        write(66,'('' Generatory:'',10i5)')
c     1   (GenSub(j,ii),j=1,NGenSub(ii))
c        write(66,'('' KSymmLG:   '',10i5)')
c     1   (KSymmLG(GenSub(j,ii)),j=1,NGenSub(ii))
        ns=0
        do j=1,NSymmS
          if(BratSub(j,ii).gt.0) then
            ns=ns+1
            SelSub(ns)=j
            call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
            call CopyVek(s6r (1,j),s6(1,ns,1,KPhase),
     1                   NDim(KPhase))
            zmag(ns,1,KPhase)=1.
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        NSymmN(KPhase)=ns
        NSymm(KPhase)=ns
        call FindSmbSgTr(Veta,Shift,Grp,NGrTr,rmi,xp,kk,kk)
        call TrMat2String(rmi,NDim(KPhase),t80)
        Veta=Grp(:idel(Grp))//'  '//t80(:idel(t80))
        call OriginShift2String(xp,3,t80)
        Veta=Veta(:idel(Veta))//'   '//t80(:idel(t80))
      enddo
      ns=0
      do j=1,NSymmS
        ns=ns+1
        call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
        call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
        call CopyVek(s6r (1,j),s6(1,ns,1,KPhase),
     1               NDim(KPhase))
        zmag(ns,1,KPhase)=1.
        IswSymm(ns,1,KPhase)=1
      enddo
      NSymmN(KPhase)=ns
      NSymm(KPhase)=ns
      call FindSmbSgTr(Veta,Shift,Grp,NGrupa(KPhase),rmi,xp,kk,kk)
      deallocate(SelSub,is,SelPom,NGrpSub,NRev)
      return
      end
