      subroutine RASetCommen
      include 'fepc.cmn'
      include 'basic.cmn'
      logical EqIgCase
      call UnitMat(RCommen(1,1,KPhase),3)
      is=0
      ip=0
      in=0
      do i=1,3
        pom=QMag(i,1,KPhase)*2.
        if(abs(pom-anint(pom)).gt..0001) then
          k=0
        else
          k=mod(nint(QMag(i,1,KPhase)*2.),2)
        endif
        if(k.eq.1) then
          is=is+1
          ip=i
        else
          in=i
        endif
      enddo
      if(is.eq.1) then
        RCommen(ip+(ip-1)*3,1,KPhase)=2.
      else if(is.eq.2) then
        if(in.eq.0) in=3
        i1=mod(in,3)+1
        i2=mod(in+1,3)+1
        RCommen(i1+(i2-1)*3,1,KPhase)= 1.
        RCommen(i2+(i1-1)*3,1,KPhase)=-1.
      else if(is.eq.3) then
        if(EqIgCase(Lattice(KPhase),'P')) then
          RCommen(1,1,KPhase)= 1.
          RCommen(2,1,KPhase)=-1.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= 1.
          RCommen(6,1,KPhase)=-1.
          RCommen(7,1,KPhase)= 2.
          RCommen(8,1,KPhase)= 2.
          RCommen(9,1,KPhase)= 2.
        else if(EqIgCase(Lattice(KPhase),'F')) then
          RCommen(1,1,KPhase)= .5
          RCommen(2,1,KPhase)=-.5
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= .5
          RCommen(6,1,KPhase)=-.5
          RCommen(7,1,KPhase)= 2.
          RCommen(8,1,KPhase)= 2.
          RCommen(9,1,KPhase)= 2.
        else if(EqIgCase(Lattice(KPhase),'I')) then
          RCommen(1,1,KPhase)= 2.
          RCommen(2,1,KPhase)= 0.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= 2.
          RCommen(6,1,KPhase)= 0.
          RCommen(7,1,KPhase)= 0.
          RCommen(8,1,KPhase)= 0.
          RCommen(9,1,KPhase)= 2.
!        else if(EqIgCase(Lattice(KPhase),'C')) then
!          RCommen(1,1,KPhase)= 2.
!          RCommen(2,1,KPhase)= -1.
!          RCommen(3,1,KPhase)= 0.
!          RCommen(4,1,KPhase)= 0.
!          RCommen(5,1,KPhase)= 1.
!          RCommen(6,1,KPhase)= 0.
!          RCommen(7,1,KPhase)= 0.
!          RCommen(8,1,KPhase)= 0.
!          RCommen(9,1,KPhase)= 2.
        else
          RCommen(1,1,KPhase)= 2.
          RCommen(2,1,KPhase)= 0.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= -1.
          RCommen(5,1,KPhase)= 1.
          RCommen(6,1,KPhase)= 0.
          RCommen(7,1,KPhase)=-1.
          RCommen(8,1,KPhase)= 0.
          RCommen(9,1,KPhase)= 1.
        endif
      endif
      trez(1,1,KPhase)=0.
      KCommen(KPhase)=1
      KCommenMax=1
      ICommen(KPhase)=1
      call SetCommen(1,NDimI(KPhase))
      return
      end
