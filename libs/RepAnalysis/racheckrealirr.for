      subroutine RACheckRealIrr(NIrr,ns,NDimQIrr,NDimIrr,UseLG,RIrr,
     1                          Klic,ich)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer NDimIrr(*),UseLG(*)
      integer, allocatable :: ConjComb(:),NDimIrrP(:)
      complex U(36),UConj(36),V(26),RIrr(NDimQIrr,ns,NIrr),
     1        BlockUnit(36),Block(36),pomc
      complex, allocatable :: RIrrP(:,:,:),Herring(:)
      real s6p(3),xp(3)
      logical eqrvm
      allocate(Herring(NIrr),ConjComb(NIrr))
      ich=0
      do i=1,NIrr
        n=NDimIrr(i)
        Herring(i)=(0.,0.)
        do j=1,NSymmN(KPhase)
          if(NDimI(KPhase).eq.1) then
            call MultM(qu(1,1,1,KPhase),rmr(1,j),xp,1,3,3)
            if(.not.eqrvm(xp,qu(1,1,1,KPhase),3,.001)) cycle
          endif
!          if(UseLG(j).ne.0.and.NDimI(KPhase).eq.1) cycle
          is=PerTabOrg(j,j)
          if(NDimI(KPhase).eq.1) then
            s6p(1:3)=s6r(1:3,j)
            call CultM(rmr(1,j),s6r(1,j),s6p,3,3,1)
            pomc=exp(-pi2*cmplx(0.,ScalMul(s6p,qu(1,1,1,KPhase))))
          else
            pomc=(1.,0.)
          endif
          m=1
          do k=1,n
            Herring(i)=Herring(i)+RIrr(m,is,i)*pomc
            m=m+n+1
          enddo
        enddo
        Herring(i)=Herring(i)/cmplx(float(NSymmN(KPhase)),0.)
        if(NDimI(KPhase).eq.1) Herring(i)=2.*Herring(i)
      enddo
      if(NDimI(KPhase).eq.1.and.Klic.eq.1) then
        do i=1,NIrr
          n2=NDimIrr(i)*2
          n1=NDimIrr(i)
          do is=1,NSymmN(KPhase)
            call CopyMatC(RIrr(1,is,i),U,n2)
            k=0
            do jj=1,n1
              do ii=1,n1
                k=k+1
                RIrr(k,is,i)=U(ii+(jj-1)*n2)
              enddo
            enddo
          enddo
        enddo
      endif
      NConj=0
      do 1300i=1,NIrr-1
        n1=NDimIrr(i)
        if(abs(Herring(i)-(1.,0.)).lt..01) cycle
        if(abs(Herring(i)+(1.,0.)).lt..01) go to 9300
        do 1100j=i+1,NIrr
          n2=NDimIrr(j)
          if(abs(Herring(j)-(1.,0.)).lt..01.or.n1.ne.n2) cycle
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            do k=1,n1
              if(abs(RIrr(k,is,i)-conjg(RIrr(k,is,j))).gt..01)
     1          then
                go to 1100
              endif
            enddo
          enddo
          NConj=NConj+1
          ConjComb(NConj)=1000*i+j
          go to 1300
1100    continue
        do 1200j=i+1,NIrr
          n2=NDimIrr(j)
          if(abs(Herring(j)-(1.,0.)).lt..01.or.n1.ne.n2) cycle
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            do k=1,n1
              if(abs(RIrr(k,is,i)-RIrr(k,is,j)).gt..01)
     1          then
                go to 1150
              endif
            enddo
            go to 1175
1150        do k=1,n1
              if(abs(RIrr(k,is,i)+RIrr(k,is,j)).gt..01)
     1          then
                go to 1200
              endif
            enddo
          enddo
1175      NConj=NConj+1
          ConjComb(NConj)=1000*i+j
1200    continue
1300  continue
      if(NConj.eq.0.or.(NDimI(KPhase).eq.1.and.NDimIrr(i).gt.1))
     1  go to 9999
      do i=1,NConj
        ii=ConjComb(i)/1000
        jj=mod(ConjComb(i),1000)
        n=NDimIrr(ii)
        if(n.le.0.or.NDimIrr(jj).le.0) cycle
        n2=2*n
        nq=n**2
        call UnitMatC(BlockUnit,n)
        do j=1,nq
          Block(j)=cmplx(rsqrt2,0.)*BlockUnit(j)
        enddo
        call SetMatBlock(U,n2,1,1,Block,0.,n)
        call SetMatBlock(U,n2,n+1,n+1,Block,0.,n)
        do j=1,nq
          Block(j)=cmplx(0.,rsqrt2)*BlockUnit(j)
        enddo
        call SetMatBlock(U,n2,1,n+1,Block,0.,n)
        call SetMatBlock(U,n2,n+1,1,Block,0.,n)
        call MatTransConjg(U,UConj,n2)
        do is=1,NSymmN(KPhase)
          if(UseLG(is).eq.0) cycle
          call CopyMatC(RIrr(1,is,ii),V,n)
          RIrr(1:n2**2,is,ii)=(0.,0.)
          call SetMatBlock(RIrr(1,is,ii),n2,1,1,V,0.,n)
          call CopyMatC(RIrr(1,is,jj),V,n)
          call SetMatBlock(RIrr(1,is,ii),n2,n+1,n+1,V,0.,n)
          call MultMC(RIrr(1,is,ii),UConj,V,n2,n2,n2)
          call MultMC(U,V,RIrr(1,is,ii),n2,n2,n2)
        enddo
        NDimIrr(ii)=-n2
        NDimIrr(jj)=0
      enddo
      allocate(NDimIrrP(NIrr),RIrrP(36,NSymmN(KPhase),NIrr))
      mmx=0
      do i=1,NIrr
        NDimIrr(i)=abs(NDimIrr(i))
        mmx=max(NDimIrr(i),mmx)
        n=NDimIrr(i)
        NDimIrrP(i)=n
        do is=1,NSymmN(KPhase)
          if(UseLG(is).eq.0) cycle
          call CopyMatC(RIrr(1,is,i),RIrrP(1,is,i),n)
        enddo
      enddo
      j=0
      do m=1,mmx
        do i=1,NIrr
          n=NDimIrrP(i)
          if(n.ne.m) cycle
          j=j+1
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            call CopyMatC(RIrrP(1,is,i),RIrr(1,is,j),n)
          enddo
          NDimIrr(j)=NDimIrrP(i)
        enddo
      enddo
      NIrr=j
      deallocate(NDimIrrP,RIrrP)
      go to 9999
9300  ich=1
9999  if(allocated(Herring)) deallocate(Herring,ConjComb)
      return
      end
