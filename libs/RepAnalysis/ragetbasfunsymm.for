      subroutine RAGetBasFunSymm(IrrepSel,pa,pb,nx,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xr(9),Shift(6),TrMatP(36),CellParP(6)
      character*80 Veta,VetaP
      complex pa(2*TriN,TriN),pb(2*TriN),rmp(3,3,4),ScDiv(4),ScDivFinal,
     1        argc
      integer CrSystemNew,Znovu
      logical EqIgCase
      logical, allocatable :: BratSymmKerP(:)
      real xp(6),TrP(36),rmq(9)
      real, allocatable :: s6KerP(:,:),ZMagKerP(:)
      ich=0
      Znovu=0
      call RAEqFromBasFun(IrRepSel,TriN,pa,pb,nx)
      call SetComplexArrayTo(CondKer(1,1,IrrepSel),TriN**2,(0.,0.))
1100  if(nx.lt.TriN) then
        if(.not.allocated(BratSymmKerP))
     1    allocate(BratSymmKerP(NSymmS),s6KerP(6,NSymmS),
     2             ZMagKerP(NSymmS))
        BratSymmKerP=.false.
        s6KerP=0.
        do i=1,NSymmS
          s6KerP(1:NDim(KPhase),i)=s6r(1:NDim(KPhase),i)
        enddo
        ZMagKerP(1)=1.
        BratSymmKerP(1)=.true.
        do i=1,nx,NSymmBlock
          io1=0
          io2=0
          do k=1,TriN
            if(abs(pa(i,k)).gt..001) then
              if(io1.le.0) then
                io1=k
              else if(io2.le.0) then
                io2=k-mod(k-1,NSymmBlock)
              else
                call SetRealArrayTo(xp,6,0.)
              endif
            endif
          enddo
          isp=(NSymmBlock*NSymmS-io1)/NSymmBlock+1
          if(io2.ge.TriN-NSymmBlock+1) then
            do j=0,2
              do k=0,2
                rmp(3-j,3-k,1)=-pa(i+j,io2+k)
                if(NDimI(KPhase).eq.0) then
                  if(abs(aimag(pa(i+j,io2+k))).gt..001) go to 1300
                endif
              enddo
            enddo
            call MatInv(rmr(1,isp),xr,det,3)
            call ScalDivMatC(rmp(1,1,1),rmr(1,isp),3,3,ScDiv(1),ich)
            ijk=1
            if(ich.ne.0) go to 1250
            if(NDimI(KPhase).eq.0) then
              if(abs(ScDiv(1)).gt..001) then
                call CopyVek(s6r(1,isp),s6KerP(1,isp),NDim(KPhase))
                ZMagKerP(isp)=real(ScDiv(1))*det
                go to 1260
              else
                ijk=2
                go to 1250
              endif
            else
              if(KSymmLG(isp).eq.0) then
                eps=-1.
              else
                eps= 1.
              endif
              if(eps.gt.0.) ScDivFinal=ScDiv(1)
              do j=0,2
                do k=3,5
                  rmp(3-j,6-k,2)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,2),rmr(1,isp),3,3,ScDiv(2),ich)
              ijk=3
              if(ich.ne.0) go to 1250
              if(eps.lt.0.) ScDivFinal=ScDiv(2)
              do j=3,5
                do k=0,2
                  rmp(6-j,3-k,3)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,3),rmr(1,isp),3,3,ScDiv(3),ich)
              ijk=4
              if(ich.ne.0) go to 1250
              do j=3,5
                do k=3,5
                  rmp(6-j,6-k,4)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,4),rmr(1,isp),3,3,ScDiv(4),ich)
              ijk=5
              if(ich.ne.0) go to 1250
              call CopyVek(s6r(1,isp),s6KerP(1,isp),NDim(KPhase))
              if(abs(ScDivFinal).gt..001) then
                ScDivFinal=ScDivFinal*det
                call qbyx(s6r(1,isp),xp,1)
                pom=atan2(aimag(ScDivFinal),real(ScDivFinal))/(2.*pi)
                if(abs(abs(pom)-.5).lt..001) then
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=.5
                else if(abs(pom).lt..001) then
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=0.
                else
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=pom
                  call od0do1(s6KerP(4,isp),s6KerP(4,isp),1)
                endif
                go to 1260
              else
                ijk=6
                go to 1250
              endif
            endif
1250        write(Cislo,'(2i5)') ijk
            if(VasekTest.eq.1) call FeWinMessage('Je zle',Cislo)
            ZMagKerP(isp)=1.
1260        BratSymmKerP(isp)=.true.
          endif
        enddo
        ns=0
        do i=1,NSymmS
          if(BratSymmKerP(i)) then
            ns=ns+1
            call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
            call CopyVek(s6KerP(1,i),s6(1,ns,1,KPhase),NDim(KPhase))
            ZMag(ns,1,KPhase)=ZMagKerP(i)
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        call RACompleteSymmPlusMagInv(ns,ich)
        xp=0.
        qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
        call FindSmbSgTr(Veta,Shift,VetaP,n,TrP,xp,i,CrSystemNew)
        qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
        if(NDimI(KPhase).eq.1.and.QPul) then
          CellParP(1:6)=CellPar(1:6,1,KPhase)
          call UnitMat(TrMatP,NDim(KPhase))
          call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),
     1                     TrMatP)
          call EM50ShowSuperSG(1,VetaP,Shift,Veta,TrP,xp)
          k=0
          call Kus(Veta,k,VetaP)
          CellPar(1:6,1,KPhase)=CellParP(1:6)
        endif
        do is=1,NSymmS
          if(BratSymmKerP(is)) then
            if(.not.BratSymmKer(is,IrrepSel)) then
              Veta='Operator ('//
     1             SymmCodes(is)(:idel(SymmCodes(is)))//
     2             ') was approved from basis functions, '
              call FeChybne(-1.,-1.,Veta,
     1                      'but not from order parameter analysis.'//
     2                      'Please contact authors.',SeriousError)
              go to 8000
            endif
          else
            if(BratSymmKer(is,IrrepSel)) then
              if(NDimI(KPhase).eq.0) then
                Veta='Operator ('//
     1               SymmCodes(is)(:idel(SymmCodes(is)))//
     2               ') was approved from order parameter analysis, '
                call FeChybne(-1.,-1.,Veta,
     1                        'but not from basis functions. Please '//
     2                        'contact authors.',SeriousError)
                go to 8000
              endif
              call MatInv(rmr(1,is),rmq,det,3)
              if(NDimI(KPhase).gt.0) then
                if(KSymmLG(is).eq.0) then
                  eps=-1.
                else
                  eps= 1.
                endif
                argc=exp(cmplx(0.,-Pi2*(s6Ker(4,is,IrrepSel))))
                pom=det
              else
                eps=-1.
                pom=det*ZMagKer(is,IrrepSel)
                argc=(1.,0.)
              endif
              if(pom.lt.0.) call RealMatrixToOpposite(rmq,rmq,3)
              do js=1,NSymmS
                if(.not.BratSymmKerP(js)) cycle
                ks=PerTab(is,js)
                jp=(NSymmS+1-js)*NSymmBlock+1
                if(NDimI(KPhase).gt.0) jp=jp-3
                do j=1,3
                  do i=1,3
                    if(i.eq.j) then
                      pa(nx+4-i,jp-j)=(1.,0.)
                    else
                      pa(nx+4-i,jp-j)=(0.,0.)
                    endif
                  enddo
                enddo
                k=0
                kp=(NSymmS+1-ks)*NSymmBlock+1
                if(eps.gt.0.) kp=kp-3
                do j=1,3
                  do i=1,3
                    k=k+1
                    pa(nx+4-i,kp-j)=-argc*cmplx(rmq(k),0.)
                  enddo
                enddo
                nx=nx+3
                if(NDimI(KPhase).gt.0) then
                  jp=jp+3
                  do j=1,3
                    do i=1,3
                      if(i.eq.j) then
                        pa(nx+4-i,jp-j)=(1.,0.)
                      else
                        pa(nx+4-i,jp-j)=(0.,0.)
                      endif
                    enddo
                  enddo
                  k=0
                  kp=(NSymmS+1-ks)*NSymmBlock+1
                  if(eps.lt.0.) kp=kp-3
                  do j=1,3
                    do i=1,3
                      k=k+1
                      pa(nx+4-i,kp-j)=-conjg(argc)*cmplx(rmq(k),0.)
                    enddo
                  enddo
                  nx=nx+3
                endif
                call TrianglC(pa,pb,2*TriN,TriN,nx)
              enddo
              Znovu=Znovu+1
              if(Znovu.le.3) go to 1100
            endif
          endif
        enddo
      endif
8000  if(.not.EqIgCase(VetaP,GrpKer(IrrepSel)).and.VasekTest.ne.0)
     1  call FeWinMessage('Nesouhlas: '//VetaP,GrpKer(IrrepSel))
      go to 9999
1300  ich=1
9999  if(allocated(BratSymmKerP)) deallocate(BratSymmKerP,s6KerP,
     1                                       ZMagKerP)
      return
      end
