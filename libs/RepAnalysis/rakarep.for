      subroutine RAKarep(ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*3 t3
      character*2 t2m
      complex U(36),UPom(36),CPhi,Chi(:,:,:),Block1(36),Block2(36),
     1        exsc,PhGenLG(:)
      dimension xp(3),io(3),rmRep(9,3),sRep(9,3),rmRepInv(9,3),
     1          sRepInv(9,3),NRep(2),rmp(9),sp(3),rmt(9),st(3),
     2          rmq(9),rmqi(9),sq(3),sqi(3)
      dimension rmLG(:,:),sLG(:,:),rmGenLG(:,:),sGenLG(:,:)
      integer   SelLG(:),SelGenLG(:),DRep(12,2),DRepQ,DRepN,
     1          con(3),CrlCentroSymm,TriNPom
      logical   LogOrderLG(:,:),EqRV,REqR,BratRep(12),SkipNext
      allocatable rmLG,sLG,SelLG,SelGenLG,LogOrderLG,rmGenLG,sGenLG,
     1            Chi,PhGenLG
      ich=0
      n=NSymmN(KPhase)
      allocate(rmLG(9,n),sLG(3,n),rmGenLG(9,n),sGenLG(3,n),PhGenLG(n),
     1         SelLG(n),SelGenLG(n),LogOrderLG(5,n),
     2         Chi(12,n,2),Rep(36,12,n,2))
      call SetComplexArrayTo(Rep,36*12*2*n,(0.,0.))
      if(NDimI(KPhase).gt.1) then
        call FeChybne(-1.,-1.,'the representative analysis for more '//
     1                'than one modulation vector',
     2                'has not yest been implemented.',WarningWithESC)
        go to 9900
      endif
      NSymmLG=0
      InvCenter=0
      NFirst=0
      do i=1,NSymmN(KPhase)
        if(NDimI(KPhase).eq.1) then
          call Multm(qu(1,1,1,KPhase),rm(1,i,1,KPhase),xp,1,3,3)
          do j=1,3
            xp(j)=xp(j)-qu(j,1,1,KPhase)
          enddo
          do j=1,NLattVec(KPhase)
            call AddVek(xp,vt6(1,j,1,KPhase),sp,3)
            do k=1,3
              if(abs(sp(k)-anint(sp(k))).gt..001) go to 1050
            enddo
            go to 1100
1050      enddo
          if(NFirst.le.0.or.i.eq.CrlCentroSymm()) NFirst=i
          cycle
        endif
1100    NSymmLG=NSymmLG+1
        call CopyMat(rm(1,i,1,KPhase),rmLG(1,NSymmLG),3)
        call CopyVek(s6(1,i,1,KPhase),sLG(1,NSymmLG),3)
        SelLG(NSymmLG)=i
        call SetLogicalArrayTo(LogOrderLG(1,NSymmLG),5,.true.)
        call SmbOp(rm6r(1,i),s6r(1,i),1,1.,t3,t2m,io,nn,det)
        if(t3.eq.'-1') InvCenter=NSymmLG
        l=0
        do j=1,3
          do k=1,3
            l=l+1
            if(j.ne.k.and.abs(rmLG(l,NSymmLG)).gt..01) then
              LogOrderLG(1,NSymmLG)=.false.
              go to 1150
            endif
          enddo
        enddo
1150    LogOrderLG(2,NSymmLG)=nn.eq.3
        LogOrderLG(3,NSymmLG)=nn.eq.2.and.det.gt.0.
        LogOrderLG(5,NSymmLG)=det.gt.0.
      enddo
      if(NFirst.gt.0) then
        call CopyMat(rm(1,NFirst,1,KPhase),rmq,3)
        call CopyVek(s6(1,NFirst,1,KPhase),sq,3)
        call MatInv(rmq,rmqi,det,3)
        call MultM(rmqi,sq,sqi,3,3,1)
        call RealVectorToOpposite(sqi,sqi,3)
      endif
      call UnitMat(rmGenLG,3)
      call SetRealArrayTo(sGenLG,3,0.)
      if(allocated(IGenLG)) deallocate(IGenLG)
      allocate(IGenLG(0:NSymmLG))
      n=1
      NGenLG=0
      IGenLG(0)=1
      do i=1,4
        do j=2,NSymmLG
          if(LogOrderLG(i,j).and.LogOrderLG(4,j).and.LogOrderLG(5,j))
     1      then
            np=n+1
            call RAAddGen(rmLG,sLG,j,rmGenLG,sGenLG,n)
            NGenLG=NGenLG+1
            IGenLG(NGenLG)=np
            do k=np,n
              do l=2,NSymmLG
                LogOrderLG(4,l)=LogOrderLG(4,l).and.
     1            .not.EqRV(rmGenLG(1,k),rmLG(1,l),9,.01)
              enddo
            enddo
            LogOrderLG(4,j)=.false.
          endif
        enddo
      enddo
      if(n.lt.NSymmLG) then
        k=InvCenter
        if(k.eq.0) then
          do i=1,4
            do j=2,NSymmLG
              if(LogOrderLG(i,j).and.LogOrderLG(4,j)) then
                k=j
                go to 1300
              endif
            enddo
          enddo
        endif
1300    if(k.gt.0) then
          np=n+1
          call RAAddGen(rmLG,sLG,k,rmGenLG,sGenLG,n)
          NGenLG=NGenLG+1
          IGenLG(NGenLG)=np
        endif
      endif
      do i=1,n
        call RAGetSymmAndPhase(rmGenLG(1,i),sGenLG(1,i),SelGenLG(i),Phi)
        PhGenLG(i)=cmplx(cos(Phi),sin(Phi))
      enddo
      NRep(1)=1
      DRep(1,1)=1
      Rep(1,1,1,1)=(1.,0)
      Chi(1,1,1)=(1.,0.)
      iz1=1
      iz2=2
      if(allocated(IGenLGConj)) deallocate(IGenLGConj,GenLGConjDiff)
      allocate(IGenLGConj(3,NSymmLG),GenLGConjDiff(3,NSymmLG))
      do n=1,NGenLG
        NRep(iz2)=0
        i=IGenLG(n)
        nb=i-1
        if(n.eq.NGenLG) then
          np=NSymmLG
        else
          np=IGenLG(n+1)-1
        endif
        ind=nint(float(np)/float(nb))
        call SetLogicalArrayTo(BratRep,NRep(iz1),.true.)
        call CopyMat(rmGenLG(1,i),rmRep(1,1),3)
        call CopyVek(sGenLG(1,i),sRep(1,1),3)
        call InvSymmOp(rmRep(1,1),sRep(1,1),
     1                 rmRepInv(1,1),sRepInv(1,1),3)
        if(ind.gt.1) then
          call MultSymmOp(rmRep(1,1),sRep(1,1),rmRep(1,1),sRep(1,1),
     1                    rmRep(1,2),sRep(1,2),3)
          call MultSymmOp(rmRepInv(1,1),sRepInv(1,1),
     1                    rmRepInv(1,1),sRepInv(1,1),
     1                    rmRepInv(1,2),sRepInv(1,2),3)
          if(ind.eq.3) then
             call MultSymmOp(rmRep(1,1),sRep(1,1),rmRep(1,2),sRep(1,2),
     1                       rmRep(1,3),sRep(1,3),3)
             call MultSymmOp(rmRepInv(1,1),sRepInv(1,1),
     1                       rmRepInv(1,2),sRepInv(1,2),
     2                       rmRepInv(1,3),sRepInv(1,3),3)
          endif
        endif
        do j=1,ind
          do k=1,nb
            if(j.eq.1) then
              IGenLGConj(j,k)=k
              GenLGConjDiff(j,k)=(1.,0.)
            else
              call MultSymmOp(rmGenLG(1,k),sGenLG(1,k),
     1                        rmRep(1,j-1),sRep(1,j-1),rmp,sp,
     2                        3)
              call MultSymmOp(rmRepInv(1,j-1),sRepInv(1,j-1),
     1                        rmp,sp,rmt,st,3)
              IGenLGConj(j,k)=0
              do l=1,nb
                if(EqRV(rmt,rmGenLG(1,l),9,.01)) then
                  IGenLGConj(j,k)=l
                  if(NDimI(KPhase).gt.0) then
                    pom=pi2*(ScalMul(qu(1,1,1,KPhase),st)-
     1                       ScalMul(qu(1,1,1,KPhase),sGenLG(1,l)))
                  else
                    pom=0.
                  endif
                  GenLGConjDiff(j,k)=cmplx(cos(pom),sin(pom))
                  exit
                endif
              enddo
              if(IGenLGConj(j,k).le.0) then
                if(VasekTest.eq.1)
     1            call FeWinMessage('Zle maticko zle!!!','V Karep')
              endif
            endif
          enddo
        enddo
        do i=1,NRep(iz1)
          if(.not.BratRep(i)) cycle
          con(1)=i
          do j=2,ind
            do 1500k=i,NRep(iz1)
              if(DRep(i,iz1).eq.DRep(k,iz1)) then
                REqR=.true.
                do l=1,nb
                  kk=IGenLGConj(j,l)
                  exsc=Chi(i,IGenLGConj(j,l),iz1)/
     1                 GenLGConjDiff(j,l)
                  REqR=REqR.and.abs(exsc-Chi(k,l,iz1)).lt..01
                  if(.not.REqR) go to 1500
                enddo
                BratRep(k)=.false.
                con(j)=k
                go to 1550
              endif
1500        continue
          enddo
1550      do j=1,nb
            if(EqRV(rmRep(1,ind),rmGenLG(1,j),9,.01)) then
              nq23=j
              if(NDimI(KPhase).gt.0) then
                ex1=pi2*(ScalMul(qu(1,1,1,KPhase),sRep(1,ind))-
     1                   ScalMul(qu(1,1,1,KPhase),sGenLG(1,j)))
              else
                ex1=0.
              endif
              exit
            endif
          enddo
          if(i.eq.con(2)) then
            if(DRep(i,iz1).eq.1) then
              U(1)=(1.,0.)
              Ph=0.
              ij=1
              ji=1
            else
              call RAUnitRN(n-1,Drep(i,iz1),i,iz1,U,ij,ji,ph,ind)
            endif
            xx=real(Rep((ji-1)*DRep(i,iz1)+ij,i,nq23,iz1))
            yy=aimag(Rep((ji-1)*DRep(i,iz1)+ij,i,nq23,iz1))
            PhaseShift=atan2(yy,xx)-ph+ex1
            Mlt=ind
          else
            Mlt=1
          endif
          do j=1,Mlt
            NRep(iz2)=NRep(iz2)+1
            DRep(NRep(iz2),iz2)=ind*DRep(i,iz1)/Mlt
            DRepN=DRep(NRep(iz2),iz2)
            DRepQ=DRepN**2
            if(i.eq.con(2)) then
              phi=(pi2*float(j-1)+PhaseShift)/float(ind)
              CPhi=cmplx(cos(phi),sin(phi))
              do l=1,DRepQ
                UPom(l)=U(l)*CPhi
              enddo
            else
              call SetComplexArrayTo(UPom,DRepQ,(0.,0.))
              do l=1,ind
                k=mod(l+ind-2,ind)+1
                kk=0
                do ll=1,DRep(i,iz1)
                  kkk=(k-1)*DRepN+(l-1)*DRep(i,iz1)
                  do nn=1,DRep(i,iz1)
                    kk=kk+1
                    kkk=kkk+1
                    if(l.eq.1) then
                      UPom(kkk)=Rep(kk,i,nq23,iz1)*
     1                          cmplx(cos(ex1),sin(ex1))
                    else
                      if(ll.eq.nn) UPom(kkk)=(1.,0.)
                    endif
                  enddo
                enddo
              enddo
            endif
            do k=1,nb
              do jj=1,ind
                if(jj.eq.1) then
                  if(i.eq.con(2)) then
                    call CopyVekC(Rep(1,i,k,iz1),Rep(1,NRep(iz2),k,iz2),
     1                            DRepQ)
                  else
                    call SetComplexArrayTo(Rep(1,NRep(iz2),k,iz2),DRepQ,
     1                                     (0.,0.))
                    do l=1,ind
                      kk=0
                      do ll=1,DRep(i,iz1)
                        kkk=(l-1)*(DRepN+DRep(i,iz1))
                        do nn=1,DRep(i,iz1)
                          kk=kk+1
                          kkk=kkk+1
                          Rep(kkk,NRep(iz2),k,iz2)=
     1                       Rep(kk,i,IGenLGConj(l,k),iz1)*
     2                       GenLGConjDiff(l,k)
                        enddo
                      enddo
                    enddo
                  endif
                else
                  call MultMC(UPom,
     1                        Rep(1,NRep(iz2),k+(jj-2)*nb,iz2),
     2                        Rep(1,Nrep(iz2),k+(jj-1)*nb,iz2),
     3                        DRepN,DRepN,DRepN)
                endif
              enddo
            enddo
            do k=1,np
              chi(Nrep(iz2),k,iz2)=cmplx(0.,0.)
              do l=1,DRepN**2,DRepN+1
                chi(Nrep(iz2),k,iz2)=chi(Nrep(iz2),k,iz2)+
     1                               Rep(l,Nrep(iz2),k,iz2)
              enddo
            enddo
          enddo
        enddo
        iz1=mod(iz1,2)+1
        iz2=mod(iz2,2)+1
      enddo
      NIrrep=NRep(iz1)
      n=6
      if(allocated(NDimIrrep))
     1  deallocate(NDimIrrep,RIrrep,EigenValIrrep,EigenVecIrrep,
     2             SmbIrrep,TraceIrrep,NBasFun,
     3             BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,ShSgKer,
     4             TrMatKer,AddSymm,NCondKer,CondKer)
       TriNPom=NSymmBlock*NSymmN(KPhase)
      allocate(NDimIrrep(NIrrep),RIrrep(n**2,NSymmS,NIrrep),
     1         EigenValIrrep(n,NSymmS,NIrrep),
     2         EigenVecIrrep(n**2,NSymmS,NIrrep),SmbIrrep(NIrrep),
     3         TraceIrrep(NSymmS,NIrrep),NBasFun(NIrrep),
     4         BratSymmKer(NSymmS,NIrrep),zmagKer(NSymmS,NIrrep),
     5         s6Ker(6,NSymmS,NIrrep),GrpKer(NIrrep),NGrpKer(NIrrep),
     6         ShSgKer(6,NIrrep),
     7         TrMatKer(NDim(KPhase),NDim(KPhase),NIrrep),
     8         AddSymm(NSymmS),NCondKer(NIrrep),
     9         CondKer(TriNPom,TriNPom,NIrrep))
      n=0
      nd=0
5000  nd=nd+1
      do i=1,NIrrep
        if(DRep(i,iz1).ne.nd) cycle
        n=n+1
        NDimIrrep(n)=nd
        do j=1,NSymmLG
          js=SelGenLG(j)
          k=0
          do ii=1,NDimIrrep(n)
            do jj=1,NDimIrrep(n)
              k=k+1
              pomr=real(Rep(k,i,j,iz1))
              pomi=aimag(Rep(k,i,j,iz1))
              call Round2Closest(pomr)
              call Round2Closest(pomi)
              RIrrep(k,js,n)=cmplx(pomr,pomi)/PhGenLG(j)
            enddo
          enddo
        enddo
      enddo
      if(n.lt.NIrrep.and.nd.le.3) go to 5000
      if(VasekTest.gt.0) call RAKontrola(0,SelGenLG)
      if(NDimI(KPhase).le.0) go to 9999
      allocate(RIrrepP(36,NSymmN(KPhase),NIrrep))
      if(NSymmLG.eq.NSymmN(KPhase)) then
        call RACheckRealIrr(NIrrep,NSymmN(KPhase),ubound(RIrrep,1),
     1                      NDimIrrep,KSymmLG,RIrrep,0,ich)
        if(ich.ne.0) go to 9300
        go to 9999
      else
        do i=1,NIrrep
          n=NDimIrrep(i)
          if(n.ne.2) cycle
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0) then
              if(abs(RIrrep(1,j,i)).gt..01.or.
     1           abs(RIrrep(4,j,i)).gt..01) cycle
              alpha=atan2(aimag(RIrrep(3,j,i)),
     1                     real(RIrrep(3,j,i)))
              beta=atan2(aimag(RIrrep(2,j,i)),
     1                    real(RIrrep(2,j,i)))
              delta=(beta-alpha)*.25
              U(1)=exp(cmplx(0., delta))
              U(2)=(0.,0.)
              U(3)=(0.,0.)
              U(4)=exp(cmplx(0.,-delta))
              Upom(1)=exp(cmplx(0.,-delta))
              Upom(2)=(0.,0.)
              Upom(3)=(0.,0.)
              Upom(4)=exp(cmplx(0.,delta))
              do k=1,NSymmN(KPhase)
                if(KSymmLG(k).gt.0) then
                  call MultMC(U,RIrrep(1,k,i),Block1,2,2,2)
                  call MultMC(Block1,Upom,RIrrep(1,k,i),2,2,2)
                endif
              enddo
              exit
            endif
          enddo
        enddo
        do i=1,NIrrep
          n=NDimIrrep(i)
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0)
     1        call CopyMatC(RIrrep(1,j,i),RIrrepP(1,j,i),n)
          enddo
        enddo
        do i=1,NIrrep
          call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
        enddo
        if(ich.ne.0) go to 9100
        call RACheckRealIrr(NIrrep,NSymmN(KPhase),ubound(RIrrep,1),
     1                      NDimIrrep,KSymmLG,RIrrep,1,ich)
        if(ich.ne.0) go to 9300
        do i=1,NIrrep
          n=NDimIrrep(i)
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0) then
              call CopyMatC(RIrrep(1,j,i),RIrrepP(1,j,i),n)
            endif
          enddo
        enddo
        do 5200i=1,NIrrep
          n=NDimIrrep(i)
          kok=0
          do 5100k=1,NSymmN(KPhase)
            if(KSymmLG(k).ne.0) cycle
            call CopyMat(rm(1,k,1,KPhase),rmq,3)
            call CopyVek(s6(1,k,1,KPhase),sq,3)
            call MatInv(rmq,rmqi,det,3)
            call MultM(rmqi,sq,sqi,3,3,1)
            call RealVectorToOpposite(sqi,sqi,3)
            call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
            if(ich.ne.0) go to 5100
            if(kok.eq.0) kok=k
            do j=1,NSymmN(KPhase)
              if(KSymmLG(j).eq.0) then
                i1=2
                j1=1
                i2=1
                j2=2
              else
                i1=1
                j1=1
                i2=2
                j2=2
              endif
              ip=(i1-1)*n+1
              jp=(j1-1)*n+1
              l=0
              do ii=ip,ip+n-1
                l=l+1
                m=0
                do jj=jp,jp+n-1
                  m=m+1
                  Block1(l+(m-1)*n)=RIrrep(ii+2*(jj-1)*n,j,i)
                enddo
              enddo
              l=0
              ip=(i2-1)*n+1
              jp=(j2-1)*n+1
              do ii=ip,ip+n-1
                l=l+1
                m=0
                do jj=jp,jp+n-1
                  m=m+1
                  U(l+(m-1)*n)=RIrrep(ii+2*(jj-1)*n,j,i)
                enddo
              enddo
              call MatConjg(U,Block2,n)
              do ii=1,n**2
                if(abs(Block1(ii)-Block2(ii)).gt..01) go to 5100
              enddo
            enddo
            go to 5200
5100      continue
          if(kok.gt.0) then
            call CopyMat(rm(1,kok,1,KPhase),rmq,3)
            call CopyVek(s6(1,kok,1,KPhase),sq,3)
            call MatInv(rmq,rmqi,det,3)
            call MultM(rmqi,sq,sqi,3,3,1)
            call RealVectorToOpposite(sqi,sqi,3)
            call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
            NDimIrrep(i)=-NDimIrrep(i)
          else
            go to 9200
          endif
          ich=0
5200    continue
      endif
      do i=1,NIrrep
        NDimIrrep(i)=2*NDimIrrep(i)
      enddo
      do 5400i=1,NIrrep
        n=NDimIrrep(i)
        if(n.ge.0) go to 5400
        n=-n
        nq=n**2
        do 5300j=i,NIrrep
          if(NDimIrrep(j).ge.0.or.n.ne.iabs(NDimIrrep(j))) go to 5300
          do k=1,NSymmN(KPhase)
            Block1(1:nq)=RIrrep(1:nq,k,i)
                 U(1:nq)=RIrrep(1:nq,k,j)
            call MatConjg(U,Block2,n)
            do ii=1,n**2
              if(abs(Block1(ii)-Block2(ii)).gt..01) go to 5300
            enddo
          enddo
          do k=1,NSymmN(KPhase)
            Block1(1:nq)= RIrrep(1:nq,k,i)
            Block2(1:nq)= RIrrep(1:nq,k,j)
            RIrrep(1:36,k,i)=(0.,0.)
            ip=1
            jp=1
            call SetMatBlock(RIrrep(1,k,i),2*n,ip,jp,Block1,0.,n)
            ip=(n-1)*n+1
            jp=(n-1)*n+1
            call SetMatBlock(RIrrep(1,k,i),2*n,ip,jp,Block2,0.,n)
          enddo
          NDimIrrep(i)=2*n
          if(i.ne.j) NDimIrrep(j)=0
          go to 5400
5300    continue
        go to 9200
5400  continue
      NIrr=0
      do i=1,NIrrep
        n=NDimIrrep(i)
        if(n.le.0) cycle
        nq=n**2
        NIrr=NIrr+1
        NDimIrrep(NIrr)=n
        if(NIrr.eq.i) cycle
        do k=1,NSymmN(KPhase)
          RIrrep(1:nq,k,NIrr)=RIrrep(1:nq,k,i)
        enddo
      enddo
      NIrrep=NIrr
      do i=1,NSymmN(KPhase)
        SelGenLG(i)=i
      enddo
      call RAKontrola(1,SelGenLG)
      go to 9999
9100  call FeUnforeseenError('Error happened during inducing of '//
     1                       'irreps from the little group.')
      go to 9900
9200  call FeUnforeseenError('Conjugated basis could not be find.')
      go to 9900
9300  call FeUnforeseenError('searching for physically irreducible '//
     1                       'representation faild.')
      go to 9900
9900  ich=1
9999  deallocate(rmLG,sLG,rmGenLG,sGenLG,PhGenLG,SelLG,SelGenLG,
     1           LogOrderLG,Chi,Rep)
      if(allocated(IGenLG)) deallocate(IGenLG)
      if(allocated(IGenLGConj)) deallocate(IGenLGConj,GenLGConjDiff)
      if(allocated(RIrrepP))deallocate(RIrrepP)
      return
101   format(3(' (',i3,',',i3,')'))
103   format(6(' (',f10.6,',',f10.6,')'))
      end
