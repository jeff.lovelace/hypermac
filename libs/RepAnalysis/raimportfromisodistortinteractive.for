      subroutine RAImportFromIsodistortInteractive
      use Basic_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*80 Veta,FileIn
      character*60 GrupaOrg,GrupaImp,GrupaTr
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      character*6 :: CellParOrgC(3) = (/'a(org)','b(org)','c(org)'/)
      character*6 :: CellParImpC(3) = (/'a(imp)','b(imp)','c(imp)'/)
      real CellTr(6),CellImp(6),QuImp(3,3),TrM3(9),TrM3I(9),TrM(36),
     1     TrP(36),xp(36),xpp(36)
      integer UseTabsIn,UseTabsCell,UseTabsSG,CrwStateQuest
      integer, allocatable :: ips(:),itrl(:)
      logical UseOriginalCell,CrwLogicQuest,IdenticalSymmetry,EqRV,
     1        MatRealEqUnitMat,ExistFile
      real, allocatable :: RmOld(:,:),RmNew(:,:)
      UseTabsIn=UseTabs
      UseTabsCell=-1
      UseTabsSG=-1
      NDimOrg=NDim(KPhase)
      GrupaOrg=Grupa(KPhase)
      call CopyVek(CellPar(1,1,KPhase),CellParOrg,6)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      allocate(RmOld(9,NSymm(KPhase)),ips(NSymm(KPhase)),
     1         itrl(NSymm(KPhase)))
      NSymmOld=0
      write(Cislo,'(i5)') NSymm(KPhase)
      call FeWinMessage(Cislo,' ')
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmOld
          if(EqRV(xp,RmOld(1,j),9,.001)) go to 1100
        enddo
        NSymmOld=NSymmOld+1
        RmOld(1:9,NSymmOld)=xp(1:9)
        call PisOperator(rmOld(1,NSymmOld),s6(1,1,1,1),1.,3)
        write(6,'('' --------------------------------------'')')
1100  enddo
      IStSymmOrg=0
      call CrlStoreSymmetry(IStSymmOrg)
      if(KCommen(KPhase).gt.0) call iom50(0,0,fln(:ifln)//'.m50')
      FileIn=fln(:ifln)//'.cif'
1120  call FeFileManager('Select input CIF file',FileIn,
     1                   '*.cif *.mcif',0,.true.,ich)
      if(ich.ne.0) go to 9000
      if(.not.ExistFile(FileIn)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                FileIn(:idel(FileIn))//
     1                '" does not exist, try again.',' ',SeriousError)
        go to 1120
      endif
      call ReadSpecifiedCIF(FileIn,5)
      if(ErrFlag.ne.0) go to 9000
      if(NDim(KPhase).ne.3.and.NDim(KPhase).ne.NDimOrg) then
        call FeChybne(-1.,-1.,'the model is neither regular nor '//
     1                'modulated with the same dimensionality.',' ',
     2                SeriousError)
        go to 9000
      endif
!   nacteni matice transformace, kdyz ne tak
      call FindCellTrans(CellPar(1,1,KPhase),CellParOrg,TrM3,ich)
      if(ich.ne.0) then
        call FeFillTextInfo('importmodel.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call UnitMat(TrM3,3)
      endif
      call CopyVek(CellPar(1,1,KPhase),CellImp,6)
      call CopyVek(Qu(1,1,1,KPhase),QuImp,3*NDimI(KPhase))
      call UnitMat(TrP,NDimOrg)
      IStSymmImp=0
      call CrlStoreSymmetry(IStSymmImp)
      call CrlRestoreSymmetry(IStSymmImp)
      if(allocated(RmNew)) deallocate(RmNew)
      allocate(RmNew(9,NSymm(KPhase)))
      NSymmNew=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmNew
          if(EqRV(xp,RmNew(1,j),9,.001)) go to 1200
        enddo
        NSymmNew=NSymmNew+1
        RmNew(1:9,NSymmNew)=xp(1:9)
1200  enddo
      GrupaImp=Grupa(KPhase)
      ich=0
      id=NextQuestId()
      xqd=500.
      il=15
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      UseTabsCell=NextTabs()
      UseTabs=UseTabsCell
      xpom=0.
      do i=1,6
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+50.
      enddo
      il=-10
      tpom=5.
      xpom=tpom+100.
      Veta='Cell parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      il=il-7
      Veta='Imported:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      write(Veta,100)(Tabulator,CellImp(i),i=1,6)
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=il-7
      Veta='Transformed:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=' '
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblCell=LblLastMade
      il=il-7
      Veta='Original:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      write(Veta,100)(Tabulator,CellParOrg(i),i=1,6)
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=il-12
      Veta='Space group:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      UseTabsSG=NextTabs()
      UseTabs=UseTabsSG
      call FeTabsAdd(100.,UseTabs,IdRightTab,' ')
      il=il-7
      Veta='Imported:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//GrupaImp
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-7
      Veta='Transformed:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//' '
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblGrupa=LblLastMade
      il=il-7
      Veta='Original:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//GrupaOrg
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-14
      Veta='Transformation imported cell -> original cell:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      XShow=70.+QuestXMin(id)
      YShow=QuestYPosition(id,nint(float(-il)*.1)+4)+QuestYMin(id)
      il=il-45
      Veta='Matrix %calculator'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtMatCalc=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il-10
      call FeQuestLinkaMake(id,il)
      Veta='Transform the model into the %original cell'
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=1,2
        il=il-10
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwTrans=CrwLastMade
          Veta='Transform reflections files into the i%mported cell'
        endif
      enddo
      UseOriginalCell=.true.
      go to 1400
1300  call CrlMatEqHide
1400  call CopyVek(CellImp,CellTr,6)
      call CopyVek(QuImp,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      call SetRealArrayTo(xp,9,0.)
      call UnitMat(xpp,NDim(KPhase))
      if(NDimI(KPhase).gt.0) then
        call DRMatTrCellQ(TrM3,CellTr,Qu(1,1,1,KPhase),xp,xpp)
      else
        call DRMatTrCell(TrM3,CellTr,xpp)
      endif
      call CopyVek(CellTr,CellPar(1,1,KPhase),6)
      write(Veta,100)(Tabulator,CellTr(i),i=1,6)
      call FeQuestLblChange(nLblCell,Veta)
      call CrlRestoreSymmetry(IStSymmImp)
      if(NDim(KPhase).gt.3) then
        call MatFromBlock3(TrM3,TrM,NDim(KPhase))
      else
        call CopyMat(TrM3,TrM,NDim(KPhase))
      endif
      call CrlTransSymm(TrM,ichp)
      if(ichp.ne.0) then
        ich=0
        go to 1600
      endif
      call FindSmbSg(GrupaTr,ChangeOrderYes,1)
      call CrlRestoreSymmetry(IStSymmImp)
      Veta=Tabulator//GrupaTr
      call FeQuestLblChange(nLblGrupa,Veta)
      call CrlMatEqShow(XShow,YShow,3,TrM3,3,CellParOrgC,CellParImpC,3)
      nCrw=nCrwTrans
      call MatInv(TrM3,TrP,det,3)
      do i=1,2
        if(MatRealEqUnitMat(TrM3,3,.001).or.abs(det-1.).gt..001) then
          call FeQuestCrwDisable(nCrw)
          if(i.eq.1) UseOriginalCell=abs(det-1.).le..001
        else
          call FeQuestCrwOpen(nCrw,i.eq.1.eqv.UseOriginalCell)
        endif
        nCrw=nCrw+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMatCalc) then
        call TrMat(TrM3,TrP,3,3)
        call MatrixCalculatorInOut(TrP,3,3,ichp)
        if(ichp.eq.0) then
          call TrMat(TrP,TrM3,3,3)
          go to 1300
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
1600  if(ich.eq.0) then
        if(CrwStateQuest(nCrwTrans).eq.CrwDisabled) then
          UseOriginalCell=abs(det-1.).le..001
        else
          UseOriginalCell=CrwLogicQuest(nCrwTrans)
        endif
      endif
      call CrlMatEqHide
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9000
      if(NDimOrg.gt.3) then
        call MatFromBlock3(TrM3,TrM,NDimOrg)
      else
        call CopyMat(TrM3,TrM,NDimOrg)
      endif
      call CopyVek(CellParOrg,CellPar(1,1,KPhase),6)
      call CopyVek(QuOrg,Qu(1,1,1,KPhase),3*(NDimOrg-3))
      MaxNLattVec=max(MaxNLattVec,NLattVec(KPhase))
      if(NDimOrg.gt.NDim(KPhase)) then
        call ReallocSymm(NDimOrg,NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call CrlCompareSymmetry(IStSymmOrg,IdenticalSymmetry)
        if(IdenticalSymmetry) then
          call CrlCompleteSymmetry(IStSymmOrg)
        else
          call CrlCompIntSymm(NDimOrg,ich)
          if(ich.ne.0) go to 9000
        endif
        NDim(KPhase)=NDimOrg
        NDimI(KPhase)=NDimOrg-3
        NDimQ(KPhase)=NDimOrg**2
        MaxNDim =NDim (KPhase)
        MaxNDimI=NDimI(KPhase)
      endif
      call iom40Only(0,0,fln(:ifln)//'.m40')
      if(UseOriginalCell) then
        call CrlTransSymm(TrM,ichp)
        if(ichp.ne.0) then
          ich=0
          go to 1600
        endif
        if(.not.MatRealEqUnitMat(TrM3,3,.001)) then
          if(allocated(TransMat)) call DeallocateTrans
          NTrans=1
          call AllocateTrans
          call MatInv(TrM,TransMat,det,NDim(KPhase))
          call SetRealArrayTo(TransVec,NDim(KPhase),0.)
          TransZM(1)=0
          call EM40SetTr(0)
          call EM40TransAtFromTo(1,NAtInd,ich)
          if(allocated(TransMat)) call DeallocateTrans
        endif
      else
        call CrlRestoreSymmetry(IStSymmImp)
        call MatInv(TrM3,TrP,det,3)
        call SetRealArrayTo(xp,9,0.)
        call UnitMat(xpp,NDim(KPhase))
        if(NDimOrg.gt.3) then
          call DRMatTrCellQ(TrP,CellPar(1,1,KPhase),Qu(1,1,1,KPhase),xp,
     1                      xpp)
        else
          call DRMatTrCell(TrP,CellPar(1,1,KPhase),xpp)
        endif
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      if(isPowder) then
        call iom90(0,fln(:ifln)//'.m90')
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
        call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
        call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),3*NDimI(KPhase))
        call iom41(1,0,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
      else
        if(NSymmOld.gt.NSymmNew) then
          call MatInv(TrM3,TrM3I,VolRatio,3)
          if(UseOriginalCell) then
            do i=1,NSymmNew
              call multm(TrM3I,RmNew(1,i),xp,3,3,3)
              call multm(xp,TrM3,RmNew(1,i),3,3,3)
            enddo
          else
            do i=1,NSymmOld
              call multm(TrM3,RmOld(1,i),xp,3,3,3)
              call multm(xp,TrM3I,RmOld(1,i),3,3,3)
            enddo
          endif
          ips=0
          n=0
          do i=1,NSymmOld
            do j=1,NSymmNew
              if(EqRV(RmOld(1,i),RmNew(1,j),9,.001)) then
                n=n+1
                ips(i)=1
                exit
              endif
            enddo
          enddo
          n=1
          do i=1,NSymmOld
            if(ips(i).ne.0) cycle
            n=n+1
            do 2650j=1,NSymmOld
              if(ips(j).eq.1) then
                call MultM(RmOld(1,i),RmOld(1,j),xp,3,3,3)
                do k=1,NSymmOld
                  if(ips(k).eq.0.and.eqrv(RmOld(1,k),xp,9,.001)) then
                    ips(k)=n
                    itrl(n)=k
                    go to 2650
                  endif
                enddo
              endif
2650        continue
          enddo
          NTwin=n
          pom=1./float(NTwin)
          do i=2,NTwin
            rtw(1:9,i)=RmOld(1:9,itrl(i))
            sctw(i,KDatBlock)=pom
          enddo
          mxscutw=max(mxscu+NTwin-1,6)
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call iom50(1,0,fln(:ifln)//'.m50')
          call iom50(0,0,fln(:ifln)//'.m50')
        else if(NSymmOld.eq.NSymmNew) then
          NTwin=1
        else
          call FeChybne(-1.,-1.,'inconsistency: the number of '//
     1                  'symmetry operations of the parent phase '//
     2                  'larger than that for the subgroup.',
     3                  SeriousError)
          go to 9000
        endif
      endif
      if(UseOriginalCell) then
        if(.not.MatRealEqUnitMat(TrM3,3,.001)) then
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
        endif
        if(ExistM95) then
          call CrlCompRefExtSymmetry(IStSymmOrg,IdenticalSymmetry)
          if(.not.IdenticalSymmetry) then
            call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
            call EM9CreateM90(0,ich)
          endif
        endif
      else
        if(ExistM95) then
          call MatInv(TrM,TrP,det,NDim(KPhase))
          call Multm(TrMP,TrP,TrM,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          call CopyMat(TrM,TrMP,NDim(KPhase))
          call CompleteM95(0)
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
        endif
      endif
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      go to 9999
9000  call MoveFile(PreviousM40,fln(:ifln)//'.m40')
      if(isPowder) call MoveFile(PreviousM41,fln(:ifln)//'.m41')
      call MoveFile(PreviousM50,fln(:ifln)//'.m50')
9999  if(UseTabsCell.ge.0) call FeTabsReset(UseTabsCell)
      if(UseTabsSG.ge.0) call FeTabsReset(UseTabsSG)
      UseTabs=UseTabsIn
      call CrlCleanSymmetry
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      if(allocated(RmNew)) deallocate(RmNew)
      return
100   format(3(a1,f10.4),3(a1,f10.3))
      end
