      subroutine RAAddGen(rmLG,sLG,n,rmGenLG,sGenLG,m)
      dimension rmLG(9,*),sLG(3,*),rmGenLG(9,*),sGenLG(3,*),rp(9),sp(3)
      logical EqRV
      call CopyMat(rmLG(1,n),rp,3)
      call CopyVek(sLG(1,n),sp,3)
      mold=m
1000  do i=1,m
        if(EqRV(rmGenLG(1,i),rp,9,.0001)) go to 9999
      enddo
      m=m+1
      mp=m
      call CopyMat(rp,rmGenLG(1,mp),3)
      call CopyVek(sp,sGenLG(1,mp),3)
      do i=2,mold
        m=m+1
        call MultSymmOp(rp,sp,rmGenLG(1,i),sGenLG(1,i),
     1                  rmGenLG(1,m),sGenLG(1,m),3)
      enddo
      call MultSymmOp(rmLG(1,n),sLG(1,n),rmGenLG(1,mp),sGenLG(1,mp),
     1                rp,sp,3)
      go to 1000
9999  return
      end
