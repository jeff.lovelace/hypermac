set(REPANALYSIS_SOURCES
  raaddgen.for
  racheckrealirr.for
  racompletesymmplusmaginv.for
  raeqfrombasfun.for
  ragenepiker.for
  ragenirrep.for
  ragenirrfromlg.for
  ragensubgroups.for
  ragetbasfun.for
  ragetbasfunsymm.for
  ragetirrsymbol.for
  ragetsymmandphase.for
  ragrsmbfromeigenval.for
  raimportfromisodistort.for
  raimportfromisodistortinteractive.for
  raisodistort.for
  rakarep.for
  rakontrola.for
  ralstofdetails.for
  raorderbasfun.for
  raselepiker.for
  rasetcommen.for
  raunitrn.for
  rawriteirr.for
  useirreps.for
)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ../../lib64)
add_library(RepAnalysis ${REPANALYSIS_SOURCES})

