      subroutine RAGenIrrep(ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xr(9),xi(9)
      complex xp(10),pomc
      character*80 Veta,PointGroup
      integer CrlCentroSymm,TriNPom,GetSymmOrd,iar(3)
      integer, allocatable :: IPorEpi(:),ShValEpi(:)
      logical EqRV,EqIgCase
      ich=0
      ln=0
      if(allocated(PerTabOrg)) deallocate(PerTabOrg,AddSymmOrg,
     1                                    SymmOrdOrg,SymmPorOrg,
     2                                    KSymmLG)
      allocate(PerTabOrg(NSymmN(KPhase),NSymmN(KPhase)),
     1         AddSymmOrg(NSymmN(KPhase)),SymmOrdOrg(NSymmN(KPhase)),
     2         SymmPorOrg(NSymmN(KPhase)),KSymmLG(NSymmN(KPhase)))
      if(.not.ParentStructure) then
        if(NDimI(KPhase).eq.1) then
          QMag(1:3,1,KPhase)=qu(1:3,1,1,KPhase)
          QMagIrr(1:3,1,KPhase)=qu(1:3,1,1,KPhase)
          do i=1,3
            if(abs(qui(i,1,KPhase)).gt..001)
     1        QMagIrr(i,1,KPhase)=1./sqrt(float(i+4))
          enddo
        else
          QMag(1:3,1,KPhase)=0.
          QMagIrr(1:3,1,KPhase)=0.
        endif
      endif
      do i=1,NSymmN(KPhase)
        SymmOrdOrg(i)=-GetSymmOrd(rm(1,i,1,KPhase))
        if(NDimI(KPhase).eq.1) then
          KSymmLG(i)=0
          call Multm(qu(1,1,1,KPhase),rm(1,i,1,KPhase),xr,1,3,3)
          do j=1,3
            xr(j)=xr(j)-qu(j,1,1,KPhase)
          enddo
          do j=1,3
            if(abs(xr(j)-anint(xr(j))).gt..001) go to 1050
          enddo
          do j=2,NLattVec(KPhase)
            pom=scalmul(xr,vt6(1,j,1,KPhase))
            if(abs(pom-anint(pom)).gt..001) go to 1050
          enddo
          KSymmLG(i)=1
        else
          KSymmLG(i)=1
        endif
1050    do j=1,NSymmN(KPhase)
          call MultM(rm(1,i,1,KPhase),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmN(KPhase)
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTabOrg(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      call Indexx(NSymmN(KPhase),SymmOrdOrg,SymmPorOrg)
      if(allocated(IGen)) deallocate(IGen,KGen,StGen,IoGen,PerTab,rm6r,
     1                               rmr,s6r,ZMagR)
      n=NSymmN(KPhase)
      if(NDimI(KPhase).gt.0.and.n.ge.NSymm(KPhase))
     1  call ReallocSymm(NDim(KPhase),2*n,NLattVec(KPhase),
     2                   NComp(KPhase),NPhase,0)
      invc=CrlCentroSymm()
      allocate(IGen(n),KGen(n),StGen(n),ioGen(3,n),
     1         PerTab(n,n),rm6r(36,n),rmr(9,n),s6r(6,n),ZMagR(n))
      do i=1,NSymmN(KPhase)
        call CopyMat(rm6(1,i,1,KPhase),rm6r(1,i),NDim(KPhase))
        call CopyMat(rm (1,i,1,KPhase),rmr (1,i),3)
        call CopyVek(s6 (1,i,1,KPhase),s6r (1,i),NDim(KPhase))
        ZMagR(i)=zmag(i,1,KPhase)
        do j=1,NSymmN(KPhase)
          call MultM(rmr(1,i),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmN(KPhase)
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTab(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      NSymmS=NSymmN(KPhase)
      NGrupaS=NGrupa(KPhase)
      GrupaS=Grupa(KPhase)
      call RAGenSubgroups
      NTwinS=1
      NTwin =1
      mxscu=6
      mxscutw=6
      CrSystemS=CrSystem(KPhase)
      if(NDimI(KPhase).le.0) then
        NSymmBlock=3
        NSymmLG=NSymmN(KPhase)
!        go to 1150
        ln=NextLogicNumber()
        call GetPointGroupFromSpaceGroup(PointGroup,i,j)
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'pgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        TriNPom=NSymmBlock*NSymmN(KPhase)
        do ik=0,1
          if(ik.eq.1) then
            n=6
            if(allocated(NDimIrrep))
     1        deallocate(NDimIrrep,RIrrep,EigenValIrrep,
     2                   SmbIrrep,TraceIrrep,NBasFun,
     3                   BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,
     4                   ShSgKer,TrMatKer,AddSymm,NCondKer,CondKer)
            TriNPom=NSymmBlock*NSymmN(KPhase)
            allocate(NDimIrrep(NIrrep),RIrrep(n**2,NSymmS,NIrrep),
     1               EigenValIrrep(n,NSymmS,NIrrep),
     2               EigenVecIrrep(n**2,NSymmS,NIrrep),SmbIrrep(NIrrep),
     3               TraceIrrep(NSymmS,NIrrep),NBasFun(NIrrep),
     4               BratSymmKer(NSymmS,NIrrep),zmagKer(NSymmS,NIrrep),
     5               s6Ker(6,NSymmS,NIrrep),GrpKer(NIrrep),
     6               NGrpKer(NIrrep),ShSgKer(6,NIrrep),
     7               TrMatKer(NDim(KPhase),NDim(KPhase),NIrrep),
     8               AddSymm(NSymmS),NCondKer(NIrrep),
     9               CondKer(TriNPom,TriNPom,NIrrep))
          endif
          rewind ln
1100      read(ln,FormA,end=1120) Veta
          k=0
          call Kus(Veta,k,Cislo)
          if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1       go to 1100
          call Kus(Veta,k,Cislo)
          call StToInt(Veta,k,iar,1,.false.,ich)
          if(ich.ne.0) go to 1120
          do i=1,iar(1)
            read(ln,FormA,end=1120) Veta
          enddo
          read(ln,FormA,end=1120) Veta
          k=0
          call StToInt(Veta,k,iar,1,.false.,ich)
          do i=1,iar(1)
            read(ln,FormA,end=1120) Veta
          enddo
          NIrrep=0
          NDimIrrepMax=0
1120      read(ln,FormA,end=9900) Veta
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:2),'GM')) then
            NIrrep=NIrrep+1
            i=NIrrep
            if(ik.gt.0) SmbIrrep(NIrrep)=Cislo
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,'Dimension:')) then
              call StToInt(Veta,k,iar,1,.false.,ich)
              NDimP=iar(1)
              if(ik.eq.0) then
                do j=1,NSymmN(KPhase)
                  read(ln,'(i5,9f8.4/5x,9f8.4)',err=9900) jj,
     1              ((pom,pom,l=1,NDimP),k=1,NDimP)
                enddo
              else
                NDimIrrep(i)=iar(1)
                do j=1,NSymmN(KPhase)
                  read(ln,'(i5,9f8.4/5x,9f8.4)',err=9900) jj,
     1              ((xr(k+(l-1)*NDimIrrep(i)),
     2                xi(k+(l-1)*NDimIrrep(i)),l=1,NDimP),k=1,NDimP)
                  do k=1,NDimIrrep(i)**2
                    if(abs(abs(xr(k))-.866).lt..01)
     1                xr(k)=sign(sqrt(.75),xr(k))
                    if(abs(abs(xi(k))-.866).lt..01)
     1                xi(k)=sign(sqrt(.75),xi(k))
                    RIrrep(k,j,i)=cmplx(xr(k),xi(k))
                  enddo
                enddo
              endif
              go to 1120
            else
              go to 9900
            endif
          endif
        enddo
        call CloseIfOpened(ln)
        go to 1160
1150    call RAKarep(ich)
1160    QPul=.false.
      else
        NSymmBlock=6
        call RAKarep(ich)
        if(ich.ne.0) go to 9900
        QPul=.true.
        do i=1,3
          pom=qu(i,1,1,KPhase)*2.
          if(abs(anint(pom)-pom).gt..00001) then
            QPul=.false.
            exit
          endif
        enddo
        if(QPul) call RASetCommen
      endif
      MaxBasFun=NSymmBlock*NSymmS+1
      TriN=NSymmBlock*NSymmS
      NDimIrrepMax=0
      do i=1,NIrrep
        nd=NDimIrrep(i)
        NDimIrrepMax=max(NDimIrrepMax,nd)
        do j=1,NSymmS
          do k=1,nd**2
            if(abs(abs(real(RIrrep(k,j,i)))-.866).lt..01)
     1        RIrrep(k,j,i)=cmplx(sign(sqrt(.75),real(RIrrep(k,j,i))),
     2                            aimag(RIrrep(k,j,i)))
            if(abs(abs(aimag(RIrrep(k,j,i)))-.866).lt..01)
     1        RIrrep(k,j,i)=cmplx(real(RIrrep(k,j,i)),
     2                            sign(sqrt(.75),aimag(RIrrep(k,j,i))))
          enddo
          pomc=(0.,0.)
          do k=1,nd
            pomc=pomc+RIrrep(k+(k-1)*NDimIrrep(i),j,i)
          enddo
          TraceIrrep(j,i)=pomc
          call JacobiComplex(RIrrep(1,j,i),nd,EigenValIrrep(1,j,i),
     1                       EigenVecIrrep(1,j,i),NRot)
        enddo
        call RAGrSmbFromEigenVal(i,ich)
        if(ich.ne.0) then
          SmbIrrep(i)='???'
!        else
!          SmbIrrep(i)=' '
        endif
        call RAGetIrrSymbol(SmbIrrep(i),GrupaS,NGrupaS,GrpKer(i),
     1                      NGrpKer(i),i,ich)
        if(ich.ne.0) then
          write(SmbIrrep(i),'(''Tau'',i3)') i
          call Zhusti(SmbIrrep(i))
          ich=0
        endif
      enddo
      call RAGetBasFun
      NEpi=0
      NEpiMax=NIrrep
      n=NIrrep
      if(allocated(KIrrepEpi))
     1  deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2             ShSgEpi,ZMagEpi,s6Epi,NCondEpi,CondEpi,NCondEpiAll,
     3             CondEpiAll,NSymmEpi,NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4             NEqPEpi,EqPEpi,EqPPEpi,TakeEpi)
      TriNPom=NSymmBlock*NSymmS
      allocate(KIrrepEpi(n),BratSymmEpi(NSymmS,n),GrpEpi(n),NGrpEpi(n),
     1         TrMatEpi(NDim(KPhase),NDim(KPhase),n),
     2         ShSgEpi(NDim(KPhase),n),TakeEpi(n),
     3         ZMagEpi(NSymmS,n),s6Epi(6,NSymmS,n),
     4         NCondEpi(n),CondEpi(TriNPom,TriNPom,n),NCondEpiAll(n),
     5         CondEpiAll(TriNPom,TriNPom,n),NSymmEpi(n),
     6         NEqEpi(n),EqEpi(72,n),NEqAEpi(n),EqAEpi(72,n),
     7         NEqPEpi(n),EqPEpi(18,n),EqPPEpi(6,n))
      do i=1,NIrrep
        if(GrpKer(i)(1:4).ne.'----') call RAGenEpiKer(i)
      enddo
      if(allocated(IPorEpi)) deallocate(IPorEpi,ShValEpi)
      allocate(IPorEpi(NEpi),ShValEpi(NEpi))
      do i=1,NEpi
        n=0
        m=0
        do j=1,NDim(KPhase)
          if(abs(ShSgEpi(j,i)).gt..01) then
            n=n+1
            if(n.eq.1) m=j
          endif
        enddo
        ShValEpi(i)=-m-100*(NDim(KPhase)-n+1)-
     1              10000*(NIrrep-KIrrepEpi(i)+1)
      enddo
      call indexx(NEpi,ShValEpi,IPorEpi)
      allocate(KIrrepEpiO(NEpi),BratSymmEpiO(NSymmS,NEpi),
     1         GrpEpiO(NEpi),NGrpEpiO(NEpi),
     2         TrMatEpiO(NDim(KPhase),NDim(KPhase),NEpi),
     3         ShSgEpiO(NDim(KPhase),NEpi),
     4         ZMagEpiO(NSymmS,NEpi),s6EpiO(6,NSymmS,NEpi),
     5         NSymmEpiO(NEpi),TakeEpiO(NEpi),
     6         NEqEpiO(NEpi),EqEpiO(72,NEpi),
     7         NEqAEpiO(NEpi),EqAEpiO(18,NEpi),
     8         NEqPEpiO(NEpi),EqPEpiO(18,NEpi),EqPPEpiO(6,NEpi),
     9         NCondEpiO(NEpi),CondEpiO(TriN,TriN,NEpi),
     a         NCondEpiAllO(NEpi),CondEpiAllO(TriN,TriN,NEpi))
      do i=1,NEpi
        nd=NDimIrrep(KIrrepEpi(i))
        if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
          ndp=nd/2
        else
          ndp=nd
        endif
        GrpEpiO(i)=GrpEpi(i)
        NGrpEpiO(i)=NGrpEpi(i)
        NEqEpiO(i)=NEqEpi(i)
        NEqAEpiO(i)=NEqAEpi(i)
        NEqPEpiO(i)=NEqPEpi(i)
        NSymmEpiO(i)=NSymmEpi(i)
        KIrrepEpiO(i)=KIrrepEpi(i)
        TakeEpiO(i)=TakeEpi(i)
        NCondEpiO(i)=NCondEpi(i)
        NCondEpiAllO(i)=NCondEpiAll(i)
        call CopyVek(TrMatEpi(1,1,i),TrMatEpiO(1,1,i),
     1               NDimQ(KPhase))
        call CopyVek(ShSgEpi(1,i),ShSgEpiO(1,i),NDim(KPhase))
        do j=1,NSymmS
          BratSymmEpiO(j,i)=BratSymmEpi(j,i)
          ZMagEpiO(j,i)=ZMagEpi(j,i)
          call CopyVek(s6Epi(1,j,i),s6EpiO(1,j,i),NDim(KPhase))
        enddo
        EqEpiO(1:72,i)=(0.,0.)
        do ii=1,NEqEpi(i)
          do jj=1,nd
            kk=ii+2*(jj-1)*nd
            EqEpiO(kk,i)=EqEpi(kk,i)
          enddo
        enddo
        EqAEpiO(1:18,i)=(0.,0.)
        do ii=1,NEqAEpi(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqAEpiO(kk,i)=EqAEpi(kk,i)
          enddo
        enddo
        EqPEpiO(1:18,i)=(0.,0.)
        do ii=1,NEqPEpi(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqPEpiO(kk,i)=EqPEpi(kk,i)
          enddo
          EqPPEpiO(ii,i)=EqPPEpi(ii,i)
        enddo
        call CopyVekC(CondEpi,CondEpiO,TriN**2*NEpi)
        call CopyVekC(CondEpiAll,CondEpiAllO,TriN**2*NEpi)
      enddo
      n=0
      do 3100ip=1,NEpi
        i=IPorEpi(ip)
        pomo=0.
        PrvniO=0.
        do k=1,NDim(KPhase)
          pomo=pomo+abs(ShSgEpiO(k,i))
          if(abs(PrvniO).lt..001.and.
     1       abs(ShSgEpiO(k,i)).gt..001) PrvniO=ShSgEpiO(k,i)
        enddo
        do j=1,n
          if(KIrrepEpiO(i).ne.KIrrepEpi(j).or.
     1       (NGrpEpiO(i).gt.0.and.NGrpEpi(j).gt.0.and.
     2        NGrpEpiO(i).ne.NGrpEpi(j)).or.
     3        NSymmEpiO(i).ne.NSymmEpi(j)) cycle
          if(EqIgCase(GrpEpiO(i),GrpEpi(j))) then
            pom=0.
            Prvni=0.
            do k=1,NDim(KPhase)
              pom=pom+abs(ShSgEpi(k,j))
              if(abs(Prvni).lt..001.and.
     1           abs(ShSgEpi(k,j)).gt..001) Prvni=ShSgEpi(k,j)
            enddo
            if(pomo.lt.pom-.001.or.(abs(pomo-pom).le..001.and.
     1         PrvniO-Prvni.gt..001)) then
              m=j
              go to 3050
            else
              go to 3100
            endif
          endif
          do k=1,NSymmS
            if(BratSymmEpiO(k,i).neqv.BratSymmEpi(k,j)) go to 2950
          enddo
2950      do 3000itw=2,NSymmS
            if(BratSymmEpiO(itw,i)) go to 3000
            itwi=2
            do while(PerTabOrg(itw,itwi).ne.1)
              itwi=itwi+1
            enddo
            do k=2,NSymmS
              if(.not.BratSymmEpiO(k,i)) cycle
              m=PerTab(PerTabOrg(itw,k),itwi)
              if(.not.BratSymmEpi(m,j)) go to 3000
              if(NDimI(KPhase).eq.1) then
                if(abs(s6EpiO(4,k,i)-s6Epi(4,m,j)).gt..01)
     1            go to 3000
              else
                if(abs(ZMagEpiO(k,i)-ZMagEpi(m,j)).gt..01)
     1            go to 3000
              endif
            enddo
            go to 3100
3000      continue
        enddo
        n=n+1
        m=n
3050    nd=NDimIrrep(KIrrepEpi(i))
        if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
          ndp=nd/2
        else
          ndp=nd
        endif
        GrpEpi(m)=GrpEpiO(i)
        NGrpEpi(m)=NGrpEpiO(i)
        NEqEpi(m)=NEqEpiO(i)
        NEqAEpi(m)=NEqAEpiO(i)
        NEqPEpi(m)=NEqPEpiO(i)
        NSymmEpi(m)=NSymmEpiO(i)
        KIrrepEpi(m)=KIrrepEpiO(i)
        NCondEpi(m)=NCondEpiO(i)
        NCondEpiAll(m)=NCondEpiAllO(i)
        call CopyVek(TrMatEpiO(1,1,i),TrMatEpi(1,1,m),NDimQ(KPhase))
        call CopyVek(ShSgEpiO(1,i),ShSgEpi(1,m),NDim(KPhase))
        do j=1,NSymmS
          BratSymmEpi(j,m)=BratSymmEpiO(j,i)
          ZMagEpi(j,m)=ZMagEpiO(j,i)
          call CopyVek(s6EpiO(1,j,i),s6Epi(1,j,m),NDim(KPhase))
        enddo
        do ii=1,NEqEpiO(i)
          do jj=1,nd
            kk=ii+2*(jj-1)*nd
            EqEpi(kk,m)=EqEpiO(kk,i)
            EqEpi(kk,m)=EqEpiO(kk,i)
          enddo
        enddo
        do ii=1,NEqAEpiO(i)
          do jj=1,ndp
            kk=ii+(jj-1)*ndp
            EqAEpi(kk,m)=EqAEpiO(kk,i)
            EqAEpi(kk,m)=EqAEpiO(kk,i)
          enddo
        enddo
        do ii=1,NEqPEpiO(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqPEpi(kk,m)=EqPEpiO(kk,i)
            EqPEpi(kk,m)=EqPEpiO(kk,i)
          enddo
          EqPPEpi(ii,m)=EqPPEpiO(ii,i)
        enddo
        call CopyVekC(CondEpiO(1,1,i),CondEpi(1,1,m),TriN**2)
        call CopyVekC(CondEpiAllO(1,1,i),CondEpiAll(1,1,m),TriN**2)
3100  continue
      NEpi=n
      deallocate(KIrrepEpiO,BratSymmEpiO,GrpEpiO,NGrpEpiO,
     1           TrMatEpiO,ShSgEpiO,ZMagEpiO,s6EpiO,NSymmEpiO,
     2           NEqEpiO,EqEpiO,NEqAEpiO,EqAEpiO,TakeEpiO,
     3           NEqPEpiO,EqPEpiO,EqPPEpiO,
     4           NCondEpiO,CondEpiO,NCondEpiAllO,CondEpiAllO)
      go to 9999
9300  ich=2
      go to 9999
9900  ich=1
9999  call CloseIfOpened(ln)
      if(allocated(IPorEpi)) deallocate(IPorEpi,ShValEpi)
      return
      end
