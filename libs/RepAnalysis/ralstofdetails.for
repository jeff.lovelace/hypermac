      subroutine RALstOfDetails(Klic,Irrep,IEpi)
      use Atoms_mod
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80  StMag(6),Veta,CisloC
      character*20  StPhi
      character*20, allocatable :: StC(:)
      character*5   t5
      character*4   t4
      character*1   t1,cp1,cp2
      complex pomc
      complex, allocatable :: EqC(:,:),EqPPC(:),pa(:,:),pb(:)
      logical Prvni,Brat,MatRealEqUnitMat,Standard,EqIgCase
      integer CrlCentroSymm
      real TrMatP(3,3),TrPom(3,3)
      Uloha='List of details for selected epikernel'
      nd=NDimIrrep(Irrep)
      if(NDimI(KPhase).le.0) then
        ndp=NDimIrrep(Irrep)
      else
        ndp=NDimIrrep(Irrep)/2
      endif
      if(NDimI(KPhase).gt.0) then
        Veta='superspace'
      else
        Veta='space'
      endif
      Veta='ernel magnetic '//Veta(:idel(Veta))//
     1     ' group for the represenation '//
     2     SmbIrrep(Irrep)(:idel(SmbIrrep(Irrep)))
      if(Klic.ge.0) then
        Veta='K'//Veta(:idel(Veta))
      else
        Veta='Epik'//Veta(:idel(Veta))
      endif
      call TitulekVRamecku(Veta)
      if(Klic.lt.0) then
        if(NDimI(KPhase).eq.1) then
          if(nd.eq.2) then
            Veta='S*exp{2*pi*i*phi}'
          else
            if(NEqPEpi(IEpi).gt.0) then
              if(abs(EqPPEpi(1,IEpi)).lt..001) then
                StPhi='phi}]'
              else
                call ToFract(abs(1.-EqPPEpi(1,IEpi)),Cislo,9)
                StPhi='(phi+'//Cislo(:idel(Cislo))//')}]'
              endif
            endif
            if(NEqAEpi(IEpi).eq.ndp) then
              Veta='[0,0]'
            else if(NEqAEpi(IEpi).gt.1) then
              Veta='???'
            else if(NEqAEpi(IEpi).eq.1) then
              if(abs(EqAEpi(1,IEpi)).lt..001) then
                Veta='[S*exp{2*pi*i*phi},0]'
              else if(abs(EqAEpi(1+2*ndp,IEpi)).lt..001) then
                Veta='[0,S*exp{2*pi*i*phi}]'
              else
                if(NEqPEpi(IEpi).gt.0) then
                  Veta='[S*exp{2*pi*i*phi},S*exp{2*pi*i*'//StPhi
                else
                  Veta='[S*exp{2*pi*i*phi1},S*exp{2*pi*i*phi2}]'
                endif
              endif
            else if(NEqAEpi(IEpi).eq.0) then
              if(NEqPEpi(IEpi).gt.0) then
                Veta='[S1*exp{2*pi*i*phi},S2*exp{2*pi*i*'//StPhi
              else
                Veta='[S1*exp{2*pi*i*phi1},S2*exp{2*pi*i*phi2}]'
              endif
            endif
          endif
        else
          allocate(EqC(nd,nd),EqPPC(nd),StC(nd))
          do i=1,nd
            StC(i)=char(ichar('a')-1+i)
          enddo
          if(NEqEpi(IEpi).lt.nd) then
            EqPPC=(0.,0.)
            EqC=(0.,0.)
            NEqC=NEqEpi(IEpi)
            do i=1,NEqC
              do j=1,nd
                EqC(i,nd+1-j)=EqEpi(i+2*(j-1)*nd,IEpi)
              enddo
            enddo
            call trianglC(EqC,EqPPC,nd,nd,NEqC)
            kk=1
            do i=1,NEqC
              jp=0
              do j=1,nd
                if(abs(EqC(i,j)).gt..01) then
                  jp=j
                  go to 1300
                endif
              enddo
              cycle
1300          k=nd+1-jp
              StC(k)=' '
              do j=nd,jp+1,-1
                if(abs(EqC(i,j)).gt..01) then
                  pom=real(EqC(i,j))
                  write(Cislo,'(f15.4)') -pom
                  call ZdrcniCisla(Cislo,1)
                  if(Cislo.eq.'1') then
                    Cislo='+'
                  else if(Cislo.eq.'-1') then
                    Cislo='-'
                  else if(pom.gt.0.) then
                    Cislo='+'//Cislo(:idel(Cislo))
                  endif
                  StC(k)=StC(k)(:idel(StC(k)))//Cislo(:idel(Cislo))//
     1                    char(ichar('a')+nd-j)
                endif
              enddo
              if(StC(k).eq.' ') then
                StC(k)='0'
              else if(StC(k)(1:1).eq.'+') then
                StC(k)=StC(k)(2:)
              endif
            enddo
          else
            StC='0'
          endif
          cp1='a'
          cp2='b'
          do i=1,nd
            if(.not.EqIgCase(StC(i),cp1)) then
              do j=i,nd
                do k=1,idel(StC(j))
                  if(EqIgCase(StC(j)(k:k),cp2)) StC(j)(k:k)=cp1
                enddo
              enddo
            endif
            cp1=char(ichar(cp1)+1)
            cp2=char(ichar(cp2)+1)
          enddo
          Veta='('
          do i=1,nd
            Veta=Veta(:idel(Veta))//StC(i)(:idel(StC(i)))//','
          enddo
          deallocate(EqC,EqPPC,StC)
          idl=idel(Veta)
          Veta(idl:idl)=')'
        endif
        call NewLn(2)
        write(lst,'(''Order parameter: '',a/)') Veta(:idel(Veta))
      endif
      allocate(pa(2*TriN,TriN),pb(2*TriN))
      if(Klic.ge.0) then
        nx=NCondKer(Irrep)
        do i=1,nx
          do j=1,TriN
            pa(i,j)=CondKer(i,j,Irrep)
          enddo
        enddo
      else
        nx=NCondEpiAll(IEpi)
        do i=1,nx
          do j=1,TriN
            pa(i,j)=CondEpiAll(i,j,IEpi)
          enddo
        enddo
      endif
      Veta='a general position'
      Veta='Magnetic moments for '//Veta(:idel(Veta))
      call TitulekVRamecku(Veta)
      if(Klic.eq.0) then
        call NewLn(2)
        write(lst,'(1x,''Number'',7x,''Symmetry code'',13x,
     1              ''Coordinates'',25x,''Magnetic moments''/)')
      else
        call NewLn(2)
        write(lst,'(1x,''Number'',7x,''Symmetry code'',30x,
     1              ''Magnetic moments''/)')
      endif
      l=TriN+1
      m=nx
      io=0
      do i=1,NSymmS
        write(t5,106) i
        call Zhusti(t5)
        call SetStringArrayTo(StMag,NSymmBlock,' ')
        idl=0
        do j=1,NSymmBlock
          l=l-1
          if(m.gt.0) then
            if(io.eq.0) then
              do k=1,TriN
                if(abs(pa(m,k)).gt..001) then
                  io=k
                  exit
                endif
              enddo
            endif
          else
            io=0
          endif
          if(io.eq.l) then
            if(NSymmBlock.eq.3) then
              StMag(j)='M'//smbx(j)//t5(:idel(t5))//'='
            else
              if(j.le.3) then
                jp=j
                t1='p'
              else
                jp=j-3
                t1='m'
              endif
              StMag(j)='M'//smbx(jp)//t5(:idel(t5))//t1//'='
            endif
            Prvni=.true.
            do k=TriN,io+1,-1
              pomc=-pa(m,k)
              if(abs(pomc).gt..001) then
                if(abs(aimag(pomc)).lt..001.or.
     1             abs( real(pomc)).lt..001) then
                  if(abs(aimag(pomc)).lt..001) then
                    write(CisloC,105) real(pomc)
                  else
                    write(CisloC,105) aimag(pomc)
                  endif
                  call ZdrcniCisla(CisloC,1)
                  if(.not.Prvni.and.
     1               (real(pomc).gt..001.or.aimag(pomc).gt..001))
     2              CisloC='+'//CisloC
                  if(abs(abs(pomc)-1.).lt..001) then
                    if(Prvni.and.
     1                 ( real(pomc).gt..001.or.
     2                  aimag(pomc).gt..001)) then
                      CisloC=' '
                    else
                      CisloC(2:)=' '
                    endif
                  endif
                  if(abs(real(pomc)).lt..001)
     1              CisloC=CisloC(:idel(CisloC))//'i'
                else
                  write(CisloC,105) real(pomc)
                  call ZdrcniCisla(CisloC,1)
                  write(Cislo ,105) aimag(pomc)
                  call ZdrcniCisla(Cislo ,1)
                  if(aimag(pomc).gt..001) Cislo='+'//Cislo
                  if(abs(abs(aimag(pomc))-1.).lt..001) then
                    if(aimag(pomc).gt..001) Cislo(2:)=' '
                  endif
                  Cislo=Cislo(:idel(Cislo))//'i'
                  CisloC='('//CisloC(:idel(CisloC))//
     1                   Cislo(:idel(Cislo))//')'
                  if(.not.Prvni) CisloC='+'//CisloC
                endif
                StMag(j)=StMag(j)(:idel(StMag(j)))//
     1                   CisloC(:idel(CisloC))
                ii=TriN+1-k
                jj=mod(ii-1,NSymmBlock)+1
                if(jj.le.3) then
                  jp=jj
                  t1='p'
                else
                  jp=jj-3
                  t1='m'
                endif
                write(Cislo,106) (ii-1)/NSymmBlock+1
                call Zhusti(Cislo)
                Cislo='M'//smbx(jp)//Cislo(:idel(Cislo))
                if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
                Prvni=.false.
                StMag(j)=StMag(j)(:idel(StMag(j)))//
     1                   Cislo(:idel(Cislo))
              endif
            enddo
            if(Prvni) StMag(j)=StMag(j)(:idel(StMag(j)))//'0'
            m=m-1
            io=0
          else
            if(j.le.3) then
              jp=j
              t1='p'
            else
              jp=j-3
              t1='m'
            endif
            Cislo='M'//smbx(jp)//t5(:idel(t5))
            if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
            StMag(j)=Cislo(:idel(Cislo))//' ... free'
          endif
          idl=max(idl,idel(StMag(j)))
        enddo
        if(idl.gt.20) then
          call NewLn(1)
          write(lst,'(i4,10x,a20,5x,a80)') i,SymmCodes(i),StMag(1)
          do j=2,NSymmBlock
            call NewLn(1)
            write(lst,'(39x,a80)') StMag(j)
          enddo
        else
          call NewLn(1)
          write(lst,108) i,SymmCodes(i),(StMag(j),j=1,3)
          if(NSymmBlock.gt.3) then
            call NewLn(1)
            write(lst,110)(StMag(j),j=4,6)
          endif
        endif
      enddo
      if(Klic.eq.0) go to 9999
      ns=0
      do i=1,NSymmS
        if(Klic.ge.0) then
          ISel=Irrep
          Brat=BratSymmKer(i,ISel)
        else
          ISel=IEpi
          Brat=BratSymmEpi(i,ISel)
        endif
        if(Brat) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          if(Klic.ge.0) then
            call CopyVek(s6Ker(1,i,ISel),s6(1,ns,1,KPhase),NDim(KPhase))
            zmag(ns,1,KPhase)=ZMagKer(i,ISel)
            IswSymm(ns,1,KPhase)=1
          else
            call CopyVek(s6Epi(1,i,ISel),s6(1,ns,1,KPhase),NDim(KPhase))
            zmag(ns,1,KPhase)=ZMagEpi(i,ISel)
            IswSymm(ns,1,KPhase)=1
          endif
        endif
      enddo
      NSymmN(KPhase)=ns
      call RACompleteSymmPlusMagInv(ns,ich)
      if(Klic.ge.0) then
        Veta=GrpKer(ISel)
      else
        Veta=GrpEpi(ISel)
      endif
      Veta='Centrosymmetric super-space group: '//Veta(:idel(Veta))
      if(NDimI(KPhase).eq.0) Veta(18:)=Veta(24:)
      if(CrlCentroSymm().le.0) Veta='Non-c'//Veta(2:)
      call newln(2)
      write(lst,FormA)
      write(lst,FormA) Veta(:idel(Veta))
      call newln(3)
      write(lst,'(/''List of centring vectors:''/)')
      do j=1,NLattVec(KPhase)
        call newln(1)
        write(lst,'(6f10.6)')(vt6(k,j,1,KPhase),k=1,NDim(KPhase))
      enddo
      call newln(3)
      write(lst,'(/''Symmetry operators:''/)')
      n=0
      do j=1,NSymmN(KPhase)
        call CodeSymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                symmc(1,j,1,KPhase),0)
        do k=1,NDim(KPhase)
          kk=idel(symmc(k,j,1,KPhase))+1
          if(symmc(k,j,1,KPhase)(1:1).ne.'-') kk=kk+1
          n=max(n,kk)
        enddo
      enddo
      if(n.lt.5) n=5
      do j=1,NSymmN(KPhase)
        Veta=' '
        k=5
        do l=1,NDim(KPhase)
          if(symmc(l,j,1,KPhase)(1:1).eq.'-') then
            kk=k
          else
            kk=k+1
          endif
          Veta(kk:)=symmc(l,j,1,KPhase)
           k=k+n
        enddo
        if(MagneticType(KPhase).ne.0) then
          if(zmag(j,1,KPhase).gt.0) then
            Veta(k+1:k+1)='m'
          else
            Veta(k:k+1)='-m'
          endif
        endif
        call newln(1)
        write(lst,FormA) Veta(:idel(Veta))
      enddo
      if(Klic.ge.0) then
        call MatBlock3(TrMatKer(1,1,ISel),TrPom,NDim(KPhase))
      else
        call MatBlock3(TrMatEpi(1,1,ISel),TrPom,NDim(KPhase))
      endif
      if(QPul) then
        call Multm(TrPom,RCommen(1,1,KPhase),TrMatP,3,3,3)
      else
        call CopyMat(TrPom,TrMatP,3)
      endif
      Standard=MatRealEqUnitMat(TrMatP,3,.001)
      if(.not.Standard) then
        call newln(3)
        write(lst,'(/''Symmetry operations are related to the '',
     1               ''original axes. The following transfomation '',
     2               ''brings it to the standard setting:''/)
     3               ')
        do i=1,3
          Veta='          '//SmbABC(i)//'''='
          idl=idel(Veta)
          Prvni=.true.
          do j=1,3
            if(Klic.ge.0) then
              pom=TrPom(j,i)
            else
              pom=TrPom(j,i)
            endif
            if(abs(pom).lt..001) cycle
            call ToFract(pom,t4,9)
            if(EqIgCase(t4,'1')) then
              if(Prvni) then
                t4=' '
              else
                t4=' +'
              endif
            else if(EqIgCase(t4,'-1')) then
              t4=' -'
            else if(pom.gt.0.) then
              if(.not.Prvni) then
                t4='+'//t4(:idel(t4))
              else
                t4=' '//t4(:idel(t4))
              endif
            endif
            if(index(t4,'/').le.0) t4='  '//t4(:idel(t4))
            Veta=Veta(:idl)//t4//SmbABC(j)
            Prvni=.false.
            idl=idl+5
          enddo
          call newln(1)
          write(lst,FormA) Veta(:idel(Veta))
        enddo
      endif
      call newln(3)
      write(lst,'(/''Additional restrictions of magnetic moments:''/)')
      if(Klic.gt.0) then
        call SetComplexArrayTo(pa,TriN**2,(0.,0.))
        mk=0
        do i=1,NCondKer(ISel)
          m=0
          do j=1,TriN,NSymmBlock
            do k=j,j+NSymmBlock-1
              if(abs(CondKer(i,k,Irrep)).gt..01) then
                m=m+1
                exit
              endif
            enddo
          enddo
          if(m.gt.2) then
            mk=mk+1
            do j=1,TriN
              pa(mk,j)=CondKer(i,j,Irrep)
            enddo
          endif
        enddo
      else
        mk=NCondEpi(ISel)
        do i=1,mk
          do j=1,TriN
            pa(i,j)=CondEpi(i,j,IEpi)
          enddo
        enddo
      endif
      mm=0
      idl=0
      do m=mk,1,-1
        io=0
        do k=1,TriN
          pomc=pa(m,k)
          if(abs(pomc).gt..001) then
            io=k
            exit
          endif
        enddo
        if(io.le.0) cycle
        mm=mm+1
        j=mod(TriN-io,NSymmBlock)+1
        write(t5,106) (TriN-io)/NSymmBlock+1
        call Zhusti(t5)
        if(NSymmBlock.eq.3) then
          StMag(mm)='M'//smbx(j)//t5(:idel(t5))//'='
        else
          if(j.le.3) then
            jp=j
            t1='p'
          else
            jp=j-3
            t1='m'
          endif
          StMag(mm)='M'//smbx(jp)//t5(:idel(t5))//t1//'='
        endif
        Prvni=.true.
        do k=TriN,io+1,-1
          pomc=-pa(m,k)
          if(abs(pomc).gt..001) then
            if(abs(aimag(pomc)).lt..001.or.
     1         abs( real(pomc)).lt..001) then
              if(abs(aimag(pomc)).lt..001) then
                write(CisloC,105) real(pomc)
              else
                write(CisloC,105) aimag(pomc)
              endif
              call ZdrcniCisla(CisloC,1)
              if(.not.Prvni.and.
     1            (real(pomc).gt..001.or.aimag(pomc).gt..001))
     2           CisloC='+'//CisloC
              if(abs(abs(pomc)-1.).lt..001) then
                if(Prvni.and.
     1             ( real(pomc).gt..001.or.
     2              aimag(pomc).gt..001)) then
                  CisloC=' '
                else
                  CisloC(2:)=' '
                endif
              endif
              if(abs(real(pomc)).lt..001)
     1          CisloC=CisloC(:idel(CisloC))//'i'
            else
              write(CisloC,105) real(pomc)
              call ZdrcniCisla(CisloC,1)
              write(Cislo ,105) aimag(pomc)
              call ZdrcniCisla(Cislo ,1)
              if(aimag(pomc).gt..001) Cislo='+'//Cislo
              if(abs(abs(aimag(pomc))-1.).lt..001) then
                if(aimag(pomc).gt..001) Cislo(2:)=' '
              endif
              Cislo=Cislo(:idel(Cislo))//'i'
              CisloC='('//CisloC(:idel(CisloC))//
     1               Cislo(:idel(Cislo))//')'
              if(.not.Prvni) CisloC='+'//CisloC
            endif
            StMag(mm)=StMag(mm)(:idel(StMag(mm)))//
     1               CisloC(:idel(CisloC))
            ii=TriN+1-k
            jj=mod(ii-1,NSymmBlock)+1
            if(jj.le.3) then
              jp=jj
              t1='p'
            else
              jp=jj-3
              t1='m'
            endif
            write(Cislo,106) (ii-1)/NSymmBlock+1
            call Zhusti(Cislo)
            Cislo='M'//smbx(jp)//Cislo(:idel(Cislo))
            if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
            Prvni=.false.
            StMag(mm)=StMag(mm)(:idel(StMag(mm)))//
     1               Cislo(:idel(Cislo))
          endif
        enddo
        idl=max(idl,idel(StMag(mm)))
        if(mm.ge.3) then
          if(idl.gt.40) then
            call NewLn(3)
            write(lst,'(a80)')(StMag(j),j=1,mm)
          else
            call NewLn(1)
            write(lst,'(3(a40,2x))')(StMag(j),j=1,mm)
          endif
          idl=0
          mm=0
        endif
      enddo
9999  if(allocated(pa)) deallocate(pa,pb)
      return
105   format(f8.3)
106   format(i5)
107   format(i4,10x,a20,3f8.3,5x,3(a20,1x))
108   format(i4,10x,a20,5x,3(a20,1x))
109   format(63x,3(a20,1x))
110   format(39x,3(a20,1x))
      end
