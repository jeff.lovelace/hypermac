      subroutine RAKontrola(Klic,SelGenLG)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer SelGenLG(*)
      real rmp(9),sp(3),xp(1)
      complex RIrrMult(64),pomc
      logical EqRV,EqCV
      do n=1,NIrrep
        nd=NDimIrrep(n)
        do i=1,NSymmLG
          is=SelGenLG(i)
          do 1100j=1,NSymmLG
            js=SelGenLG(j)
            call MultSymmOp(rmr(1,is),s6r(1,is),rmr(1,js),s6r(1,js),
     1                      rmp,sp,3)
            do k=1,NSymmLG
              ks=SelGenLG(k)
              if(EqRV(rmp,rmr(1,ks),9,.01)) then
                call MultmC(RIrrep(1,is,n),RIrrep(1,js,n),
     1                      RIrrMult,nd,nd,nd)
                do m=1,3
                  sp(m)=s6r(m,ks)-sp(m)
                enddo
                call qbyx(sp,xp,1)
                Delta=pi2*xp(1)
                pomc=cmplx(cos(Delta),sin(Delta))
                m=0
                do ii=1,nd
                  do jj=1,nd
                    m=m+1
                    if(Klic.eq.0.or.jj.le.nd/2) then
                      RIrrMult(m)=RIrrMult(m)*pomc
                    else
                      RIrrMult(m)=RIrrMult(m)*conjg(pomc)
                    endif
                  enddo
                enddo
                if(.not.EqCV(RIrrMult,RIrrep(1,ks,n),nd**2,.001)) then
                  call FeChybne(-1.,-1.,
     1                          'the set of irrep matrices does not '//
     2                          'follow group multiplication table.',
     3                          'Please contact authors.',SeriousError)
                  go to 9999
                endif
                go to 1100
              endif
            enddo
1100      continue
        enddo
      enddo
9999  return
      end
