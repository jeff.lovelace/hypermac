      subroutine RAEqFromBasFun(NIrr,nn,pa,pb,nx)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xr(3),xi(3),xrp(3),xip(3)
      complex pa(2*nn,nn),pb(2*nn),pomc
      call SetComplexArrayTo(pa,2*TriN**2,(0.,0.))
      call SetComplexArrayTo(pb,2*TriN,(0.,0.))
      do i=1,NBasFun(NIrr)
        j1=0
        do j=1,TriN
          if(abs(BasFun(j,i,NIrr)).gt..001) then
            j1=j
            pomc=BasFun(j,i,NIrr)
            exit
          endif
        enddo
        if(j1.le.0) cycle
        j2=TriN+1-j1
        do j=j1+1,TriN
          if(abs(BasFun(j,i,NIrr)).gt..001) then
            j3=TriN+1-j
            pa(j3,j2)=pa(j3,j2)-BasFun(j,i,NIrr)/pomc
          endif
        enddo
      enddo
      nx=TriN
      do i=1,TriN
        do j=1,TriN
          if(abs(pa(i,j)).gt..001) then
            pa(i,i)=(1.,0.)
            exit
          endif
        enddo
      enddo
      nx=TriN
      call TrianglC(pa,pb,2*nn,nn,nx)
      return
      end
