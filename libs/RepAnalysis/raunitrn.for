      subroutine RAUnitRN(ngb,DRep,NRep,z,U,ii,jj,ph,ind)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer DRep,z
      integer r,q(45)
      complex d1(3,3,5),d2(3,3,5),a(:,:),U(DRep,DRep)
      allocatable a
      do k=1,ngb
        m=0
        do i=1,DRep
          do j=1,DRep
            m=m+1
            b=real(Rep(m,NRep,IGenLGConj(2,IGenLG(k)),z))
            p=aimag(Rep(m,NRep,IGenLGConj(2,IGenLG(k)),z))
            if(abs(b).lt.1.e-6) b=0.
            if(abs(p).lt.1.e-6) p=0.
            d1(i,j,k)=cmplx(b,p)*GenLGConjDiff(2,IGenLG(k))
            b=real(Rep(m,NRep,IGenLG(k),z))
            p=aimag(Rep(m,NRep,IGenLG(k),z))
            if(abs(b).lt.1.e-6)  b=0.
            if(abs(p).lt.1.e-6)  p=0.
            d2(i,j,k)=cmplx(b,p)
          enddo
        enddo
      enddo
      mg=DRep*DRep*ngb
      ng=DRep*DRep
      allocate(a(mg,ng))
      call SetComplexArrayTo(a,mg*ng,(0.,0.))
      kng=0
      do k=1,ngb
        nn=0
        do n=1,DRep
          in=n
          ni=nn+1
          do i=1,DRep
            nj=nn+1
            jn=n
            do j=1,DRep
              a(ni+kng,nj)=a(ni+kng,nj)+d1(j,i,k)
              a(in+kng,jn)=a(in+kng,jn)-d2(i,j,k)
              nj=nj+1
              jn=jn+DRep
            enddo
            in=in+DRep
            ni=ni+1
          enddo
          nn=nn+DRep
        enddo
        kng=kng+ng
      enddo
      call Gausz(a,mg,ng,r,q)
      call GauszGetOut(a,mg,ng,r,q,DRep)
      do n=1,ind-1
        do i=1,DRep
          do j=1,DRep
            a(i+n*DRep,j+n*DRep)=0.
            do k=1,DRep
              a(i+n*DRep,j+n*DRep)=a(i+n*DRep,j+n*DRep)+
     1                             a(i,k)*a(k+(n-1)*DRep,j+(n-1)*DRep)
            enddo
          enddo
        enddo
      enddo
      do i=1,DRep
        do j=1,DRep
          if(abs(a(i+(ind-1)*DRep,j+(ind-1)*DRep)).ne.0.) then
            ii=i
            jj=j
            go to 2000
          endif
        enddo
      enddo
2000  xx=real (a(i+(ind-1)*DRep,j+(ind-1)*DRep))
      yy=aimag(a(i+(ind-1)*DRep,j+(ind-1)*DRep))
      ph=atan2(yy,xx)
      do i=1,DRep
        do j=1,DRep
          U(i,j)=A(i,j)
        enddo
      enddo
      deallocate(a)
      return
      end
