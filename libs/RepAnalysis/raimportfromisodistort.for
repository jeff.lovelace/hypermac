      subroutine RAImportFromIsodistort(FileNameIn)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*(*) FileNameIn
      character*80 Veta,FileIn
      character*60 GrupaOrg,GrupaImp,GrupaTr
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      character*6 :: CellParOrgC(3) = (/'a(org)','b(org)','c(org)'/)
      character*6 :: CellParImpC(3) = (/'a(imp)','b(imp)','c(imp)'/)
      character*7, allocatable :: AtTypeMagO(:),AtTypeMagJO(:)
      dimension CellTr(6),CellImp(6),QuImp(3,3),TrM3(9),TrM3I(9),
     1          TrM(36),TrP(36),xp(36),xpp(36)
      integer CrwStateQuest
      logical UseOriginalCell,CrwLogicQuest,IdenticalSymmetry,EqRV,
     1        MatRealEqUnitMat,ExistFile,WasPowder
      integer, allocatable :: ips(:),itrl(:),TypeFFMagO(:)
      real, allocatable :: RmOld(:,:),RmNew(:,:),FFnO(:),FFniO(:),
     1                     FFMagO(:,:)
      WasPowder=IsPowder
      NDimOrg=NDim(KPhase)
      GrupaOrg=Grupa(KPhase)
      call CopyVek(CellPar(1,1,KPhase),CellParOrg,6)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      allocate(RmOld(9,NSymm(KPhase)),ips(NSymm(KPhase)),
     1         itrl(NSymm(KPhase)))
      NSymmOld=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmOld
          if(EqRV(xp,RmOld(1,j),9,.001)) go to 1100
        enddo
        NSymmOld=NSymmOld+1
        RmOld(1:9,NSymmOld)=xp(1:9)
1100  enddo
      if(allocated(AtTypeMagO))
     1  deallocate(AtTypeMagO,AtTypeMagJO,TypeFFMagO,FFnO,FFniO,FFMagO)
      nAtType=NAtFormula(KPhase)
      allocate(AtTypeMagO(nAtType),AtTypeMagJO(nAtType),
     1         TypeFFMagO(nAtType),FFnO(nAtType),FFniO(nAtType),
     2         FFMagO(7,nAtType))
      do i=1,nAtType
        AtTypeMagO(i)=AtTypeMag(i,KPhase)
        AtTypeMagJO(i)=AtTypeMagJ(i,KPhase)
        TypeFFMagO(i)=TypeFFMag(i,KPhase)
        FFnO(i)=FFn(i,KPhase)
        FFniO(i)=FFni(i,KPhase)
        FFMagO(1:7,i)=FFMag(1:7,i,KPhase)
      enddo
1120  if(FileNameIn.eq.' ') then
        FileIn=fln(:ifln)//'.cif'
        call FeFileManager('Select input CIF file',FileIn,
     1                   '*.cif *.mcif',0,.true.,ich)
        if(ich.ne.0) go to 9000
      else
        FileIn=FileNameIn
      endif
      if(.not.ExistFile(FileIn)) then
        if(FileNameIn.eq.' ') then
          Veta='Try it again.'
        else
          Veta=' '
        endif
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                FileIn(:idel(FileIn))//'" does not exist.',Veta,
     2                SeriousError)
        if(FileNameIn.eq.' ') then
          go to 1120
        else
          go to 9000
        endif
      endif
      call ReadSpecifiedCIF(FileIn,5)
      if(ErrFlag.ne.0) go to 9000
      do i=1,nAtType
        AtTypeMag(i,KPhase)=AtTypeMagO(i)
        AtTypeMagJ(i,KPhase)=AtTypeMagJO(i)
        TypeFFMag(i,KPhase)=TypeFFMagO(i)
        FFn(i,KPhase)=FFnO(i)
        FFni(i,KPhase)=FFniO(i)
        FFMag(1:7,i,KPhase)=FFMagO(1:7,i)
      enddo
      IsPowder=WasPowder
      if(NDim(KPhase).ne.3.and.NDim(KPhase).ne.NDimOrg) then
        call FeChybne(-1.,-1.,'the model is neither regular nor '//
     1                'modulated with the same dimensionality.',' ',
     2                SeriousError)
        go to 9000
      endif
!   nacteni matice transformace, kdyz ne tak
      call FindCellTrans(CellPar(1,1,KPhase),CellParOrg,TrM3,ich)
      if(ich.ne.0) then
        call FeFillTextInfo('importmodel.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call UnitMat(TrM3,3)
      endif
      call CopyVek(CellPar(1,1,KPhase),CellImp,6)
      call CopyVek(Qu(1,1,1,KPhase),QuImp,3*NDimI(KPhase))
      call UnitMat(TrP,NDimOrg)
      if(allocated(RmNew)) deallocate(RmNew)
      allocate(RmNew(9,NSymm(KPhase)))
      NSymmNew=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmNew
          if(EqRV(xp,RmNew(1,j),9,.001)) go to 1200
        enddo
        NSymmNew=NSymmNew+1
        RmNew(1:9,NSymmNew)=xp(1:9)
1200  enddo
      if(NDimOrg.gt.3) then
        call MatFromBlock3(TrM3,TrM,NDimOrg)
      else
        call CopyMat(TrM3,TrM,NDimOrg)
      endif
      call CopyVek(CellParOrg,CellPar(1,1,KPhase),6)
      call CopyVek(QuOrg,Qu(1,1,1,KPhase),3*(NDimOrg-3))
      call MatInv(TrM3,TrP,det,3)
      call SetRealArrayTo(xp,9,0.)
      call UnitMat(xpp,NDim(KPhase))
      if(NDimOrg.gt.3) then
        call DRMatTrCellQ(TrP,CellPar(1,1,KPhase),Qu(1,1,1,KPhase),xp,
     1                    xpp)
      else
        call DRMatTrCell(TrP,CellPar(1,1,KPhase),xpp)
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call FeEdit(fln(:ifln)//'.m50')
      do ia=1,NAtInd
        if(kswa(ia).ne.KPhase) cycle
        if(AtTypeMag(isf(ia),NPhase).eq.' ') then
          MagParP=0
        else
          MagParP=1
        endif
        call ZmMag(ia,MagParP)
      enddo
      call iom40Only(1,0,fln(:ifln)//'.m40')
      call iom40Only(0,0,fln(:ifln)//'.m40')
      call FeEdit(fln(:ifln)//'.m40')
      call FeEdit(fln(:ifln)//'.m50')
      if(IsPowder) then
        call iom90(0,fln(:ifln)//'.m90')
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
        call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
        call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),3*NDimI(KPhase))
        call iom41(1,0,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
      else
        if(NSymmOld.gt.NSymmNew) then
          call MatInv(TrM3,TrM3I,VolRatio,3)
          do i=1,NSymmOld
            call multm(TrM3,RmOld(1,i),xp,3,3,3)
            call multm(xp,TrM3I,RmOld(1,i),3,3,3)
          enddo
          ips=0
          n=0
          do i=1,NSymmOld
            do j=1,NSymmNew
              if(EqRV(RmOld(1,i),RmNew(1,j),9,.001)) then
                n=n+1
                ips(i)=1
                exit
              endif
            enddo
          enddo
          n=1
          do i=1,NSymmOld
            if(ips(i).ne.0) cycle
            n=n+1
            do 2650j=1,NSymmOld
              if(ips(j).eq.1) then
                call MultM(RmOld(1,i),RmOld(1,j),xp,3,3,3)
                do k=1,NSymmOld
                  if(ips(k).eq.0.and.eqrv(RmOld(1,k),xp,9,.001)) then
                    ips(k)=n
                    itrl(n)=k
                    go to 2650
                  endif
                enddo
              endif
2650        continue
          enddo
          NTwin=n
          pom=1./float(NTwin)
          do i=2,NTwin
            rtw(1:9,i)=RmOld(1:9,itrl(i))
            sctw(i,KDatBlock)=pom
          enddo
          mxscutw=max(mxscu+NTwin-1,6)
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call iom50(1,0,fln(:ifln)//'.m50')
          call iom50(0,0,fln(:ifln)//'.m50')
        else if(NSymmOld.eq.NSymmNew) then
          NTwin=1
        else
          call FeChybne(-1.,-1.,'inconsistency: the number of '//
     1                  'symmetry operations of the parent phase '//
     2                  'larger than that for the subgroup.',' ',
     3                  SeriousError)
          go to 9000
        endif
      endif
      if(ExistM95) then
        SilentRun=.true.
        call MatInv(TrM,TrP,det,NDim(KPhase))
        call Multm(TrMP,TrP,TrM,NDim(KPhase),NDim(KPhase),NDim(KPhase))
        call CopyMat(TrM,TrMP,NDim(KPhase))
        call CompleteM95(0)
        ExistM90=.false.
        call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
        call EM9CreateM90(0,ich)
      endif
      call DeleteFile(PreviousM40)
      if(isPowder) call DeleteFile(PreviousM41)
      call DeleteFile(PreviousM50)
      go to 9999
9000  call MoveFile(PreviousM40,fln(:ifln)//'.m40')
      if(isPowder) call MoveFile(PreviousM41,fln(:ifln)//'.m41')
      call MoveFile(PreviousM50,fln(:ifln)//'.m50')
9999  if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      if(allocated(RmNew)) deallocate(RmNew)
      if(allocated(AtTypeMagO))
     1  deallocate(AtTypeMagO,AtTypeMagJO,TypeFFMagO,FFnO,FFniO,FFMagO)
      return
100   format(3(a1,f10.4),3(a1,f10.3))
      end
