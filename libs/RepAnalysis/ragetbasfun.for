      subroutine RAGetBasFun
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      complex, allocatable :: PerMat(:,:,:),Spin(:),SpinT(:),Trace(:),
     1                        pa(:,:),pb(:)
      complex cmp(3,3)
      integer, allocatable :: ioa(:)
      dimension xo(6),xp(6),rmp(9),xpp(6),xt(6),xr(3),xi(3),GammaM(3)
      character*256 Veta
      complex pomc
      logical EqIgCase,EpsMinus
      if(allocated(SymmCodes)) deallocate(SymmCodes,xyz)
      allocate(SymmCodes(NSymmS),xyz(3,NSymmS))
      if(allocated(pa)) deallocate(pa,pb)
      allocate(pa(2*TriN,TriN),pb(2*TriN))
      n=1
      SymmCodes(1)='x,y,z'
      do i=1,3
        xyz(i,1)=sqrt(float(i)*.1)
      enddo
      xp=0.
      xp(1:3)=xyz(1:3,1)
      do 3100i=1,NSymmS
        call Multm(rm6r(1,i),xp,xo,NDim(KPhase),NDim(KPhase),1)
        call AddVek(xo,s6r(1,i),xo,NDim(KPhase))
        call CodeSymm(rm6r(1,i),s6r(1,i),symmc(1,i,1,KPhase),0)
        do j=1,NLattVec(KPhase)
          call AddVek(xo,vt6(1,j,1,KPhase),xpp,NDim(KPhase))
          do 3050k=1,n
            do m=1,3
              pom=xpp(m)-xyz(m,k)
              if(abs(anint(pom)-pom).gt..0001) go to 3050
            enddo
            go to 3100
3050      continue
        enddo
        n=n+1
        call CopyVek(xo,xyz(1,n),3)
        call CodeSymm(rm6r(1,i),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        Veta=' '
        do j=1,3
          Veta=Veta(:idel(Veta))//symmc(j,i,1,KPhase)
          if(j.ne.3) Veta=Veta(:idel(Veta))//','
        enddo
        if(NDimI(KPhase).gt.0) then
          do j=1,3
            do k=1,idel(Veta)-1
              if(EqIgCase(Veta(k:k+1),'x1')) Veta(k:k+1)='x '
              if(EqIgCase(Veta(k:k+1),'x2')) Veta(k:k+1)='y '
              if(EqIgCase(Veta(k:k+1),'x3')) Veta(k:k+1)='z '
            enddo
          enddo
          call Zhusti(Veta)
        endif
        SymmCodes(n)=Veta
3100  continue
      if(allocated(PerMat)) deallocate(PerMat,Spin,SpinT,ioa)
      if(allocated(BasFun)) deallocate(BasFun,BasFunOrg,MultIrrep)
      allocate(PerMat(TriN,TriN,NSymmS),Spin(TriN),SpinT(TriN),
     1         BasFun(TriN,MaxBasFun,NIrrep),
     2         BasFunOrg(TriN,MaxBasFun,NIrrep),ioa(MaxBasFun),
     3         MultIrrep(NIrrep))
      if(allocated(Trace)) deallocate(Trace)
      allocate(Trace(NSymmS))
      EpsMinus=.false.
      do is=1,NSymmS
        call matinv(rmr(1,is),rmp,det,3)
        if(NDimI(KPhase).gt.0) then
          if(KSymmLG(is).eq.0) then
            eps=-1.
          else
            eps= 1.
          endif
          if(eps.lt.0.) EpsMinus=.true.
          call MultM(qu(1,1,1,KPhase),rmr(1,is),GammaM,1,3,3)
          do i=1,3
            GammaM(i)=GammaM(i)-eps*qu(i,1,1,KPhase)
          enddo
          GammaMR=ScalMul(GammaM,xyz(1,1))
        endif
        call SetComplexArrayTo(PerMat(1,1,is),TriN**2,(0.,0.))
        do i=1,n
          call SetRealArrayTo(xp,6,0.)
          call CopyVek(xyz(1,i),xp,3)
          call Multm(rm6r(1,is),xp,xo,NDim(KPhase),NDim(KPhase),1)
          call AddVek(xo,s6r(1,is),xo,NDim(KPhase))
          do k=1,NLattVec(KPhase)
            call AddVek(xo,vt6(1,k,1,KPhase),xp,NDim(KPhase))
            call AddVek(vt6(1,k,1,KPhase),s6r(1,is),xt,NDim(KPhase))
            do 3150j=1,n
              do m=1,3
                pom=xyz(m,j)-xp(m)
                if(abs(anint(pom)-pom).gt..0001) go to 3150
              enddo
              go to 3200
3150        continue
          enddo
3200      k=0
          do jj=1,3
            do ii=1,3
              k=k+1
              cmp(ii,jj)=det*rmr(k,is)
            enddo
          enddo
          if(NDimI(KPhase).le.0) then
            jj=(j-1)*3+1
            ii=(i-1)*3+1
            call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,0.,3)
          else
            call qbyx(xt,xpp,1)
            delta=Pi2*xpp(1)
            if(eps.gt.0.) then
              jj=(j-1)*6+1
              ii=(i-1)*6+1
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
              jj=jj+3
              ii=ii+3
              delta=-delta
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
            else
              jj=(j-1)*6+1
              ii=(i-1)*6+4
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
              ii=ii-3
              jj=jj+3
              delta=-delta
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
            endif
          endif
        enddo
        Trace(is)=(0.,0.)
        do i=1,TriN
          Trace(is)=Trace(is)+PerMat(i,i,is)
        enddo
      enddo
      m=0
      j=TriN*MaxBasFun*NIrrep
      call SetComplexArrayTo(BasFun,j,(0.,0.))
      do i=1,NIrrep
        NBasFun(i)=0
        pomc=0.
        do j=1,NSymmS
          pomc=pomc+Trace(j)*TraceIrrep(j,i)
        enddo
        MultIrrep(i)=nint(abs(pomc)/float(NSymmS))
        if(MultIrrep(i).gt.0) then
          m=m+1
          nf=0
          do kk=1,NSymmBlock
            call SetComplexArrayTo(Spin,TriN,(0.,0.))
            if(kk.le.TriN) Spin(kk)=(1.,0.)
            do ii=1,NDimIrrep(i)
              do 3500jj=1,NDimIrrep(i)
                nf=nf+1
                k=ii+(jj-1)*NDimIrrep(i)
                do j=1,NSymmS
                  call MultmC(PerMat(1,1,j),Spin,SpinT,TriN,TriN,1)
                  do l=1,TriN
                    if(abs(SpinT(l)).gt..001) then
c                      pomc=RIrrep(k,j,i)*SpinT(l)
                      pomc=conjg(RIrrep(k,j,i))*SpinT(l)
                    else
                      cycle
                    endif
                    if(NSymmBlock.eq.6) then
                      if(EpsMinus) then
                        if(mod(l-1,6).le.2) then
                          if(ii.le.NDimIrrep(i)/2.or.NDimIrrep(i).eq.1)
     1                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        else
                          if(ii.gt.NDimIrrep(i)/2)
     1                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        endif
                      else
                        if(mod(l-1,6).le.2) then
                          BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        else
                          BasFun(l,nf,i)=BasFun(l,nf,i)+
     1                                   RIrrep(k,j,i)*SpinT(l)
                        endif
                      endif
                    else
                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                    endif
                  enddo
                enddo
3300            io=0
                do l=1,TriN
                  if(abs(BasFun(l,nf,i)).gt..001) then
                    io=l
                    exit
                  endif
                enddo
                if(io.le.0) then
                  nf=nf-1
                  go to 3500
                endif
                pomc=1./BasFun(io,nf,i)
                do l=io,TriN
                  BasFun(l,nf,i)=BasFun(l,nf,i)*pomc
                enddo
                call CopyVekC(BasFun(1,nf,i),BasFunOrg(1,nf,i),TriN)
                do k=1,nf-1
                  if(ioa(k).eq.io) then
                    do l=1,TriN
                      BasFun(l,nf,i)=BasFun(l,nf,i)-BasFun(l,k,i)
                    enddo
                    go to 3300
                  endif
                enddo
                ioa(nf)=io
                call RAOrderBasFun(BasFun(1,1,i),ioa,TriN,nf)
3500          continue
            enddo
            NBasFun(i)=nf
          enddo
          do j=2,nf
            io=0
            do l=1,TriN
              if(abs(BasFun(l,j,i)).gt..001) then
                io=l
                exit
              endif
            enddo
            do k=j-1,1,-1
              if(abs(BasFun(io,k,i)).gt..001) then
                pomc=-BasFun(io,k,i)/BasFun(io,j,i)
                do l=1,TriN
                  BasFun(l,k,i)=BasFun(l,k,i)+pomc*BasFun(l,j,i)
                enddo
              endif
            enddo
          enddo
        endif
        call RAGetBasFunSymm(i,pa,pb,nx,ich)
        do j=1,nx
          do k=1,TriN
            CondKer(j,k,i)=pa(j,k)
          enddo
        enddo
        NCondKer(i)=nx
      enddo
      if(allocated(PerMat)) deallocate(PerMat,Spin,SpinT,ioa)
      if(allocated(Trace)) deallocate(Trace)
      if(allocated(pa)) deallocate(pa,pb)
      return
      end
