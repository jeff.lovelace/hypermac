      subroutine ReallocIndInt(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iharp(:,:),riarp(:),rsarp(:),ICullArP(:)
      allocatable iharp,riarp,rsarp,ICullArP
      allocate(iharp(NDim(KPhase),NRefAlloc),riarp(NRefAlloc),
     1         rsarp(NRefAlloc),ICullArP(NRefAlloc))
      call CopyVekI(ihar,iharp,NRefAlloc*NDim(KPhase))
      call CopyVek (riar,riarp,NRefAlloc)
      call CopyVek (rsar,rsarp,NRefAlloc)
      call CopyVekI(ICullAr,ICullArP,NRefAlloc)
      NRefAllocp=NRefAlloc
      NRefAlloc=NRefAlloc+n
      deallocate(ihar,riar,rsar,ICullAr)
      allocate(ihar(NDim(KPhase),NRefAlloc),riar(NRefAlloc),
     1         rsar(NRefAlloc),ICullAr(NRefAlloc))
      call CopyVekI(iharp,ihar,NRefAllocp*NDim(KPhase))
      call CopyVek (riarp,riar,NRefAllocp)
      call CopyVek (rsarp,rsar,NRefAllocp)
      call CopyVekI(ICullArP,ICullAr,NRefAllocp)
      deallocate(iharp,riarp,rsarp)
      return
      end
