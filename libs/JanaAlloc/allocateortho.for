      subroutine AllocateOrtho(n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(orx40)) deallocate(orx40,ordel,oreps,ora)
      allocate(orx40(n),ordel(n),oreps(n),ora(n))
      mxo=n
      end
