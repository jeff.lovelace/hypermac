      subroutine ReallocRFactorsCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: RFactMatrixO(:,:),RFactVectorO(:,:),
     1                        CondRFactInclO(:,:),AbsRFactInclO(:),
     2                        ModRFactInclO(:),CondRFactExclO(:,:),
     3                        AbsRFactExclO(:),ModRFactExclO(:)
      character*20, allocatable :: StRFactO(:),StRFactInclO(:),
     1                             StRFactExclO(:)
      if(NRFactors.gt.0) then
        allocate(RFactMatrixO(36,NRFactors),RFactVectorO(6,NRFactors),
     1           StRFactO(NRfactors),CondRFactInclO(6,NRfactors),
     2           AbsRFactInclO(NRfactors),ModRFactInclO(NRfactors),
     3           StRFactInclO(NRfactors),CondRFactExclO(6,NRfactors),
     4           AbsRFactExclO(NRfactors),ModRFactExclO(NRfactors),
     5           StRFactExclO(NRfactors))
        do i=1,NRfactors
          call CopyVekI(RFactMatrix(1,i),RFactMatrixO(1,i),MaxNDim**2)
          call CopyVekI(RFactVector(1,i),RFactVectorO(1,i),MaxNDim)
          StRFactO(i)=StRFact(i)
          call CopyVekI(CondRFactIncl(1,i),CondRFactInclO(1,i),MaxNDim)
          AbsRFactInclO(i)=AbsRFactIncl(i)
          ModRFactInclO(i)=ModRFactIncl(i)
          StRFactInclO(i)=StRFactIncl(i)
          call CopyVekI(CondRFactIncl(1,i),CondRFactInclO(1,i),MaxNDim)
          AbsRFactExclO(i)=AbsRFactExcl(i)
          ModRFactExclO(i)=ModRFactExcl(i)
          StRFactExclO(i)=StRFactExcl(i)
        enddo
      endif
      if(allocated(RFactMatrix))
     1  deallocate(RFactMatrix,RFactVector,StRFact,
     2             CondRFactIncl,AbsRFactIncl,
     3             ModRFactIncl,StRFactIncl,
     4             CondRFactExcl,AbsRFactExcl,
     5             ModRFactExcl,StRFactExcl)
      if(allocated(nRPartAll))
     1  deallocate(nRPartAll,nRPartObs,
     2              RDenPartAll, RNumPartAll,
     3              RDenPartObs, RNumPartObs,
     4             wRDenPartAll,wRNumPartAll,
     5             wRDenPartObs,wRNumPartObs)
      allocate(RFactMatrix(36,n),RFactVector(6,n),StRFact(n),
     1         CondRFactIncl(6,n),AbsRFactIncl(n),
     2         ModRFactIncl(n),StRFactIncl(n),
     3         CondRFactExcl(6,n),AbsRFactExcl(n),
     4         ModRFactExcl(n),StRFactExcl(n))
      allocate(nRPartAll(n),nRPartObs(n),
     1          RDenPartAll(n), RNumPartAll(n),
     2          RDenPartObs(n), RNumPartObs(n),
     3         wRDenPartAll(n),wRNumPartAll(n),
     4         wRDenPartObs(n),wRNumPartObs(n))
      if(NRfactors.gt.0) then
        do i=1,NRfactors
          call CopyVekI(RFactMatrixO(1,i),RFactMatrix(1,i),MaxNDim**2)
          call CopyVekI(RFactVectorO(1,i),RFactVector(1,i),MaxNDim)
          StRFact(i)=StRFactO(i)
          call CopyVekI(CondRFactInclO(1,i),CondRFactIncl(1,i),MaxNDim)
          AbsRFactIncl(i)=AbsRFactInclO(i)
          ModRFactIncl(i)=ModRFactInclO(i)
          StRFactIncl(i)=StRFactInclO(i)
          call CopyVekI(CondRFactInclO(1,i),CondRFactIncl(1,i),MaxNDim)
          AbsRFactExcl(i)=AbsRFactExclO(i)
          ModRFactExcl(i)=ModRFactExclO(i)
          StRFactExcl(i)=StRFactExclO(i)
        enddo
        deallocate(RFactMatrixO,RFactVectorO,StRFactO,
     1             CondRFactInclO,AbsRFactInclO,
     2             ModRFactInclO,StRFactInclO,
     3             CondRFactExclO,AbsRFactExclO,
     4             ModRFactExclO,StRFactExclO)
      endif
      NRfactorsMax=n
      return
      end
