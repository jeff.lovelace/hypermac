      subroutine ReallocManBackg(NManBackgNew)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension   XManBackgO(:,:),YManBackgO(:,:)
      allocatable XManBackgO,YManBackgO
      NManBackgOld=ubound(XManBackg,1)
      if(NManBackgOld.ge.NManBackgNew) go to 9999
      n=NManBackgOld
      allocate(XManBackgO(n,NDatBlock),YManBackgO(n,NDatBlock))
      do i=1,NDatBlock
        do j=1,NManBackg(i)
          XManBackgO(j,i)=XManBackg(j,i)
          YManBackgO(j,i)=YManBackg(j,i)
        enddo
      enddo
      deallocate(XManBackg,YManBackg)
      n=NManBackgNew+100
      allocate(XManBackg(n,NDatBlock),YManBackg(n,NDatBlock))
      do i=1,NDatBlock
        do j=1,NManBackg(i)
          XManBackg(j,i)=XManBackgO(j,i)
          YManBackg(j,i)=YManBackgO(j,i)
        enddo
      enddo
      deallocate(XManBackgO,YManBackgO)
9999  return
      end
