      subroutine ReallocateAtMagMode(NPlus)
      use Atoms_mod
      character*8 LAtMagModeO(:,:)
      integer MAtMagModeO(:),NMAtMagModeO(:),JAtMagModeO(:),
     1        IAtMagModeO(:,:),KiAtMagModeO(:,:)
      real AtMagModeO(:,:)
      allocatable LAtMagModeO,MAtMagModeO,NMAtMagModeO,IAtMagModeO,
     1            KiAtMagModeO,AtMagModeO,JAtMagModeO
      if(NAtMagMode.lt.NAtMagModeMax) go to 9999
      if(NAtMagModeMax.gt.0) then
        allocate(MAtMagModeO(NAtMagMode),NMAtMagModeO(NAtMagMode),
     1           IAtMagModeO(MMagAModeMax/3,NAtMagMode),
     2           LAtMagModeO(MMagAModeMax,NAtMagMode),
     3           KiAtMagModeO(MMagAModeMax,NAtMagMode),
     4           AtMagModeO(MMagAModeMax,NAtMagMode),
     5           JAtMagModeO(NAtMagMode))
        do i=1,NAtMagMode
          MAtMagModeO(i)=MAtMagMode(i)
          JAtMagModeO(i)=JAtMagMode(i)
          NMAtMagModeO(i)=NMAtMagMode(i)
          call CopyVekI(IAtMagMode(1,i),IAtMagModeO(1,i),
     1                  MAtMagMode(i))
          do j=1,NMAtMagMode(i)
            LAtMagModeO(j,i)=LAtMagMode(j,i)
          enddo
          call CopyVekI(KiAtMagMode(1,i),KiAtMagModeO(1,i),
     1                  NMAtMagMode(i))
          call CopyVek(AtMagMode(1,i),AtMagModeO(1,i),
     1                 NMAtMagMode(i))
        enddo
      endif
      if(allocated(MAtMagMode))
     1  deallocate(MAtMagMode,NMAtMagMode,IAtMagMode,LAtMagMode,
     2             KiAtMagMode,AtMagMode,SAtMagMode,FAtMagMode,
     3             KAtMagMode,JAtMagMode)
      n=NAtMagModeMax+NPlus
      allocate(MAtMagMode(n),NMAtMagMode(n),
     1         IAtMagMode(MMagAModeMax/3,n),
     2         LAtMagMode(MMagAModeMax,n),
     3         KiAtMagMode(MMagAModeMax,n),
     4         AtMagMode(MMagAModeMax,n),
     5         SAtMagMode(MMagAModeMax,n),
     6         FAtMagMode(MMagAModeMax,n),
     7         KAtMagMode(MMagAModeMax,n),
     8         JAtMagMode(n))
      if(NAtMagModeMax.gt.0) then
        do i=1,NAtMagMode
          MAtMagMode(i)=MAtMagModeO(i)
          JAtMagMode(i)=JAtMagModeO(i)
          NMAtMagMode(i)=NMAtMagModeO(i)
          call CopyVekI(IAtMagModeO(1,i),IAtMagMode(1,i),
     1                  MAtMagMode(i))
          do j=1,NMAtMagMode(i)
            LAtMagMode(j,i)=LAtMagModeO(j,i)
          enddo
          call CopyVekI(KiAtMagModeO(1,i),KiAtMagMode(1,i),
     1                  NMAtMagMode(i))
          call CopyVek(AtMagModeO(1,i),AtMagMode(1,i),
     1                 NMAtMagMode(i))
        enddo
        deallocate(MAtMagModeO,NMAtMagModeO,IAtMagModeO,KiAtMagModeO,
     1             AtMagModeO,JAtMagModeO)
      endif
      NAtMagModeMax=n
9999  return
      end
