      subroutine ReallocScaleCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: iqso(:),ihmso (:,:),ihssvo(:,:),ieqsvo(:),
     1                        imdsvo(:),ihmaso(:,:),ihssno(:,:),
     2                        ieqsno(:),imdsno(:)
      character*20, allocatable :: scsko(:),scvzito(:),scnevzito(:)
      if(NScales.gt.0) then
        allocate(iqso(NScales),ihmso(36,NScales),ihmaso(6,NScales),
     1           ihssvo(6,NScales),ieqsvo(NScales),imdsvo(NScales),
     2           ihssno(6,NScales),ieqsno(NScales),imdsno(NScales),
     2           scsko(NScales),scvzito(NScales),scnevzito(NScales))
        do i=1,NScales
          iqso(i)=iqs(i)
          scsko(i)=scsk(i)
          scvzito(i)=scvzit(i)
          scnevzito(i)=scnevzit(i)
          call CopyVekI(ihms(1,i),ihmso(1,i),MaxNDim**2)
          call CopyVekI(ihmas(1,i),ihmaso(1,i),MaxNDim)
          call CopyVekI(ihssv(1,i),ihssvo(1,i),MaxNDim)
          ieqsvo(i)=ieqsv(i)
          imdsvo(i)=imdsv(i)
          call CopyVekI(ihssn(1,i),ihssno(1,i),MaxNDim)
          ieqsno(i)=ieqsn(i)
          imdsno(i)=imdsn(i)
        enddo
      endif
      if(allocated(iqs)) deallocate(iqs,ihms,ihmas,
     1                              ihssv,ieqsv,imdsv,
     2                              ihssn,ieqsn,imdsn,
     3                              scsk,scvzit,scnevzit)
      allocate(iqs(n),ihms(36,n),ihmas(6,n),
     1         ihssv(6,n),ieqsv(n),imdsv(n),
     1         ihssn(6,n),ieqsn(n),imdsn(n),
     3         scsk(n),scvzit(n),scnevzit(n))
      if(NScales.gt.0) then
        do i=1,NScales
          iqs(i)=iqso(i)
          scsk(i)=scsko(i)
          scvzit(i)=scvzito(i)
          scnevzit(i)=scnevzito(i)
          call CopyVekI(ihmso(1,i),ihms(1,i),MaxNDim**2)
          call CopyVekI(ihmaso(1,i),ihmas(1,i),MaxNDim)
          call CopyVekI(ihssvo(1,i),ihssv(1,i),MaxNDim)
          ieqsv(i)=ieqsvo(i)
          imdsv(i)=imdsvo(i)
          call CopyVekI(ihssno(1,i),ihssn(1,i),MaxNDim)
          ieqsn(i)=ieqsno(i)
          imdsn(i)=imdsno(i)
        enddo
        deallocate(iqso,ihmso,ihmaso,
     1             ihssvo,ieqsvo,imdsvo,
     2             ihssno,ieqsno,imdsno,
     3             scsko,scvzito,scnevzito)
      endif
      NScalesMax=n
      return
      end
