      subroutine ReallocateMoleculeParams(NMolLast,NPosLast,KTLSN,KModN)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),KModN(*),
     1          rx(:,:,:),ry(:,:,:),srx(:,:,:),sry(:,:,:),
     2          qx(:,:,:),qy(:,:,:),sqx(:,:,:),sqy(:,:,:),
     3          KiP(:,:)
      allocatable px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy,KiP
      md=7
      if(KTLSN.gt.0) md=md+21
      mw=0
      do i=0,2
        kmod=max(KModN(i+1),KModMMax(i+1))
        n=TRank(i)
        mw=max(mw,kmod)
        md=md+2*n*kmod
        if(i.eq.0.and.kmod.ne.0) md=md+1
        if(i.gt.0) md=md+2*n*kmod
        if(i.gt.1) md=md+18*kmod
      enddo
      if(mw.gt.0) md=md+1
      if(md.le.mxdm) go to 9999
      nm=mxm*mxp
      if(KTLSN.gt.KTLSMax) then
        if(.not.allocated(tt))
     1    allocate( tt(6,nm), tl(6,nm), ts(9,nm),
     2             stt(6,nm),stl(6,nm),sts(9,nm))
        KTLSMax=KTLSN
      endif
      do it=0,2
        n=TRank(it)
        KModP=KModMMax(it+1)
        if(mw.le.KModP) cycle
        if(it.eq.0) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm),py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( axm(1,ji), px(1,1,ji),m)
                call CopyVek( aym(1,ji), py(1,1,ji),m)
                call CopyVek(saxm(1,ji),spx(1,1,ji),m)
                call CopyVek(saym(1,ji),spy(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
          if(mw.gt.0) then
            allocate( axm(mw,nm), aym(mw,nm))
            allocate(saxm(mw,nm),saym(mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), axm(1,ji),m)
                call CopyVek( py(1,1,ji), aym(1,ji),m)
                call CopyVek(spx(1,1,ji),saxm(1,ji),m)
                call CopyVek(spy(1,1,ji),saym(1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(it.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( utx(1,1,ji), px(1,1,ji),m)
                call CopyVek( uty(1,1,ji), py(1,1,ji),m)
                call CopyVek(sutx(1,1,ji),spx(1,1,ji),m)
                call CopyVek(suty(1,1,ji),spy(1,1,ji),m)
                call CopyVek( urx(1,1,ji), rx(1,1,ji),m)
                call CopyVek( ury(1,1,ji), ry(1,1,ji),m)
                call CopyVek(surx(1,1,ji),srx(1,1,ji),m)
                call CopyVek(sury(1,1,ji),sry(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(utx))
     1      deallocate(utx,uty,sutx,suty,urx,ury,surx,sury)
          if(mw.gt.0) then
            allocate( utx(n,mw,nm), uty(n,mw,nm))
            allocate(sutx(n,mw,nm),suty(n,mw,nm))
            allocate( urx(n,mw,nm), ury(n,mw,nm))
            allocate(surx(n,mw,nm),sury(n,mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), utx(1,1,ji),m)
                call CopyVek( py(1,1,ji), uty(1,1,ji),m)
                call CopyVek(spx(1,1,ji),sutx(1,1,ji),m)
                call CopyVek(spy(1,1,ji),suty(1,1,ji),m)
                call CopyVek( rx(1,1,ji), urx(1,1,ji),m)
                call CopyVek( ry(1,1,ji), ury(1,1,ji),m)
                call CopyVek(srx(1,1,ji),surx(1,1,ji),m)
                call CopyVek(sry(1,1,ji),sury(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry)
          endif
          if(allocated(durdx)) deallocate(durdx)
          allocate(durdx(18,mw,nm))
        else if(it.eq.2) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm),
     4                qx(9,KModP,nm), qy(9,KModP,nm),
     5               sqx(9,KModP,nm),sqy(9,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( ttx(1,1,ji), px(1,1,ji),m)
                call CopyVek( tty(1,1,ji), py(1,1,ji),m)
                call CopyVek(sttx(1,1,ji),spx(1,1,ji),m)
                call CopyVek(stty(1,1,ji),spy(1,1,ji),m)
                call CopyVek( tlx(1,1,ji), rx(1,1,ji),m)
                call CopyVek( tly(1,1,ji), ry(1,1,ji),m)
                call CopyVek(stlx(1,1,ji),srx(1,1,ji),m)
                call CopyVek(stly(1,1,ji),sry(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( tsx(1,1,ji), qx(1,1,ji),m)
                call CopyVek( tsy(1,1,ji), qy(1,1,ji),m)
                call CopyVek(stsx(1,1,ji),sqx(1,1,ji),m)
                call CopyVek(stsy(1,1,ji),sqy(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(ttx))
     1      deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,stly,tsx,tsy,stsx,
     2                 stsy)
          if(mw.gt.0) then
            allocate( ttx(n,mw,nm), tty(n,mw,nm))
            allocate(sttx(n,mw,nm),stty(n,mw,nm))
            allocate( tlx(n,mw,nm), tly(n,mw,nm))
            allocate(stlx(n,mw,nm),stly(n,mw,nm))
            allocate( tsx(9,mw,nm), tsy(9,mw,nm))
            allocate(stsx(9,mw,nm),stsy(9,mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), ttx(1,1,ji),m)
                call CopyVek( py(1,1,ji), tty(1,1,ji),m)
                call CopyVek(spx(1,1,ji),sttx(1,1,ji),m)
                call CopyVek(spy(1,1,ji),stty(1,1,ji),m)
                call CopyVek( rx(1,1,ji), tlx(1,1,ji),m)
                call CopyVek( ry(1,1,ji), tly(1,1,ji),m)
                call CopyVek(srx(1,1,ji),stlx(1,1,ji),m)
                call CopyVek(sry(1,1,ji),stly(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( qx(1,1,ji), tsx(1,1,ji),m)
                call CopyVek( qy(1,1,ji), tsy(1,1,ji),m)
                call CopyVek(sqx(1,1,ji),stsx(1,1,ji),m)
                call CopyVek(sqy(1,1,ji),stsy(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy)
          endif
        endif
      enddo
      allocate(KiP(mxdm,nm))
      do i=1,NMolLast
        ji=(i-1)*mxp
        if(i.eq.NMolLast) then
          jk=min(NPosLast,mam(i))
        else
          jk=mam(i)
        endif
        do j=1,jk
          ji=ji+1
          call CopyVekI(KiMol(1,ji),KiP(1,ji),mxdm)
        enddo
      enddo
      deallocate(KiMol)
      allocate(KiMol(md,nm))
      do i=1,NMolLast
        ji=(i-1)*mxp
        if(i.eq.NMolLast) then
          jk=min(NPosLast,mam(i))
        else
          jk=mam(i)
        endif
        do j=1,jk
          ji=ji+1
          call CopyVekI(KiP(1,ji),KiMol(1,ji),mxdm)
          call SetIntArrayTo(KiMol(mxdm+1,ji),md-mxdm,0)
        enddo
      enddo
      deallocate(KiP)
      mxdm=md
      do i=1,3
        KModMMax(i)=max(KModMMax(i),KModN(i))
      enddo
9999  return
      end
