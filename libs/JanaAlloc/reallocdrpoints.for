      subroutine ReallocDrPoints(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: HDrP(:,:),HDrTwP(:),ColDrP(:)
      real, allocatable :: RIDrP(:),RSDrP(:),XDrP(:),YDrP(:),RadDrP(:)
      allocate(HDrP(NDim(KPhase),NDrAlloc),HDrTwP(NDrAlloc),
     1         RIDrP(NDrAlloc),RSDrP(NDrAlloc),XDrP(NDrAlloc),
     2         YDrP(NDrAlloc),RadDrP(NDrAlloc),ColDrP(NDrAlloc))
      call CopyVekI(HDr,HDrP,NDrAlloc*NDim(KPhase))
      call CopyVekI(HDrTw,HDrTwP,NDrAlloc)
      call CopyVekI(HDr,HDrP,NDrAlloc*NDim(KPhase))
      call CopyVek (RIDr,RIDrP,NDrAlloc)
      call CopyVek (RSDr,RSDrP,NDrAlloc)
      call CopyVek (XDr,XDrP,NDrAlloc)
      call CopyVek (YDr,YDrP,NDrAlloc)
      call CopyVek (RadDr,RadDrP,NDrAlloc)
      call CopyVekI(ColDr,ColDrP,NDrAlloc)
      NDrAllocP=NDrAlloc
      NDrAlloc=NDrAlloc+n
      deallocate(HDr,HDrTw,RIDr,RSDr,XDr,YDr,RadDr,ColDr,PorDr,IXDr)
      allocate(HDr(NDim(NPhase),NDrAlloc),HDrTw(NDrAlloc),
     1         RIDr(NDrAlloc),RSDr(NDrAlloc),XDr(NDrAlloc),
     2         YDr(NDrAlloc),RadDr(NDrAlloc),ColDr(NDrAlloc),
     3         PorDr(NDrAlloc),IXDr(NDrAlloc))
      call CopyVekI(HDrP,HDr,NDrAllocP*NDim(KPhase))
      call CopyVekI(HDrTwP,HDrTw,NDrAllocP)
      call CopyVek (RIDrP,RIDr,NDrAllocP)
      call CopyVek (RSDrP,RSDr,NDrAllocP)
      call CopyVek (XDrP,XDr,NDrAllocP)
      call CopyVek (YDrP,YDr,NDrAllocP)
      call CopyVek (RadDrP,RadDr,NDrAllocP)
      call CopyVekI(ColDrP,ColDr,NDrAllocP)
      deallocate(HDrP,HDrTwP,RIDrP,RSDrP,XDrP,YDrP,RadDrP,ColDrP)
      return
      end
