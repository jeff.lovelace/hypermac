      subroutine ReallocSGTest(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: riarp(:),rsarp(:),RunCCDArP(:)
      integer, allocatable :: RefBlockArP(:),ICullArP(:)
      allocate(riarp(NRefAlloc),rsarp(NRefAlloc),ICullArP(NRefAlloc),
     1         RefBlockArP(NRefAlloc),RunCCDArP(NRefAlloc))
      call CopyVek (riar,riarp,NRefAlloc)
      call CopyVek (rsar,rsarp,NRefAlloc)
      call CopyVekI(ICullAr,ICullArP,NRefAlloc)
      call CopyVekI(RefBlockAr,RefBlockArP,NRefAlloc)
      call CopyVek(RunCCDAr,RunCCDArP,NRefAlloc)
      NRefAllocp=NRefAlloc
      NRefAlloc=NRefAlloc+n
      deallocate(riar,rsar,ICullAr,RefBlockAr,RunCCDAr)
      allocate(riar(NRefAlloc),rsar(NRefAlloc),ICullAr(NRefAlloc),
     1         RefBlockAr(NRefAlloc),RunCCDAr(NRefAlloc))
      call CopyVek (riarp,riar,NRefAllocp)
      call CopyVek (rsarp,rsar,NRefAllocp)
      call CopyVekI(ICullArP,ICullAr,NRefAllocp)
      call CopyVekI(RefBlockArP,RefBlockAr,NRefAllocp)
      call CopyVek(RunCCDArP,RunCCDAr,NRefAllocP)
      deallocate(riarp,rsarp,ICullArP,RefBlockArP,RunCCDArP)
      return
      end
