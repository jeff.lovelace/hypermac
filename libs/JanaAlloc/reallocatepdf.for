      subroutine ReallocatePDF(NPlus)
      use Contour_mod
      character*80, allocatable :: scpdfo(:)
      integer, allocatable :: iapdfo(:),ispdfo(:),idpdfo(:)
      real, allocatable :: xpdfo(:,:),popaspdfo(:,:)
      if(NPDF.lt.MaxPDF) go to 9999
      if(NPDF.gt.0) then
        allocate(iapdfo(NPDF),ispdfo(NPDF),idpdfo(NPDF),
     1           scpdfo(NPDF),xpdfo(3,NPDF),popaspdfo(64,NPDF))
        call CopyVekI(iapdf,iapdfo,NPDF)
        call CopyVekI(ispdf,ispdfo,NPDF)
        call CopyVekI(idpdf,idpdfo,NPDF)
        call CopyVek (xpdf,xpdfo,3*NPDF)
        call CopyVek (popaspdf,popaspdfo,64*NPDF)
        call CopyStringArray(scpdf,scpdfo,NPDF)
      endif
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      n=MaxPDF+NPlus
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      if(NPDF.gt.0) then
        call CopyVekI(iapdfo,iapdf,NPDF)
        call CopyVekI(ispdfo,ispdf,NPDF)
        call CopyVekI(idpdfo,idpdf,NPDF)
        call CopyVek (xpdfo,xpdf,3*NPDF)
        call CopyStringArray(scpdfo,scpdf,NPDF)
        call CopyVek (popaspdfo,popaspdf,64*NPDF)
        deallocate(iapdfo,ispdfo,idpdfo,scpdfo,xpdfo,popaspdfo)
      endif
      MaxNPDF=n
9999  return
      end
