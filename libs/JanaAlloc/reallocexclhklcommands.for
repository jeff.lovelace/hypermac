      subroutine ReallocExclHKLCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ihmo(:,:),ihmao(:,:),
     1        ihsno(:,:),ieqno(:),imdno(:),
     2        ihsvo(:,:),ieqvo(:),imdvo(:)
      logical DontUseO(:)
      character*20 SkupinaO(:),NevzitO(:),VzitO(:)
      allocatable ihmo,ihmao,ihsno,ihsvo,ieqno,imdno,ieqvo,
     1            imdvo,SkupinaO,NevzitO,VzitO,DontUseO
      if(NSkrt.gt.0) then
        allocate(ihmo(36,NSkrt),ihmao(6,NSkrt),ihsno(6,NSkrt),
     1           ihsvo(6,NSkrt),
     2           ieqno(NSkrt),imdno(NSkrt),ieqvo(NSkrt),
     3           imdvo(NSkrt),SkupinaO(NSkrt),NevzitO(NSkrt),
     4           VzitO(NSkrt),DontUseO(NSkrt))
        do i=1,NSkrt
          call CopyVekI(ihm (1,i),ihmo (1,i),MaxNDim**2)
          call CopyVekI(ihma(1,i),ihmao(1,i),MaxNDim)
          call CopyVekI(ihsn(1,i),ihsno(1,i),MaxNDim)
          call CopyVekI(ihsv(1,i),ihsvo(1,i),MaxNDim)
          ieqno(i)=ieqn(i)
          imdno(i)=imdn(i)
          ieqvo(i)=ieqv(i)
          imdvo(i)=imdv(i)
          SkupinaO(i)=Skupina(i)
          NevzitO(i)=Nevzit(i)
          VzitO(i)=Vzit(i)
          DontUseO(i)=DontUse(i)
        enddo
      endif
      if(allocated(ihm))
     1  deallocate(ihm,ihma,ihsn,ihsv,ieqn,imdn,ieqv,
     2             imdv,Skupina,Nevzit,Vzit,DontUse)
      allocate(ihm(36,n),ihma(6,n),ihsn(6,n),ihsv(6,n),
     1         ieqn(n),imdn(n),ieqv(n),imdv(n),Skupina(n),
     2         Nevzit(n),Vzit(n),DontUse(n))
      if(NSkrt.gt.0) then
        do i=1,NSkrt
          call CopyVekI(ihmo (1,i),ihm (1,i),MaxNDim**2)
          call CopyVekI(ihmao(1,i),ihma(1,i),MaxNDim)
          call CopyVekI(ihsno(1,i),ihsn(1,i),MaxNDim)
          call CopyVekI(ihsvo(1,i),ihsv(1,i),MaxNDim)
          ieqn(i)=ieqno(i)
          imdn(i)=imdno(i)
          ieqv(i)=ieqvo(i)
          imdv(i)=imdvo(i)
          Skupina(i)=SkupinaO(i)
          Nevzit(i)=NevzitO(I)
          Vzit(i)=VzitO(I)
          DontUse(i)=DontUseO(i)
        enddo
        deallocate(ihmo,ihmao,ihsno,ihsvo,ieqno,imdno,
     1             ieqvo,imdvo,SkupinaO,NevzitO,VzitO,DontUseO)
      endif
      NSkrtMax=n
      return
      end
