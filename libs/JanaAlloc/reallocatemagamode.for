      subroutine ReallocateMagAMode(NMagAModeNew,MMagAModeNew)
      use Atoms_mod
      character*8 LMagAModeO(:)
      dimension MMagAModeO(:)
      real MagAModeO(:,:)
      allocatable LMagAModeO,MMagAModeO,MagAModeO
      if(NMagAModeNew.le.NMagAModeMax.and.
     1   MMagAModeNew.le.MMagAModeMax) go to 9999
      if(NMagAModeMax.gt.0) then
        allocate(MMagAModeO(NMagAMode),LMagAModeO(NMagAMode),
     1           MagAModeO(MMagAModeMax,NMagAMode))
        do i=1,NMagAMode
          MMagAModeO(i)=MMagAMode(i)
          LMagAModeO(i)=LMagAMode(i)
          call CopyVek(MagAMode(1,i),MagAModeO(1,i),MMagAMode(i))
        enddo
      endif
      if(allocated(MMagAMode))
     1  deallocate(MMagAMode,LMagAMode,MagAMode)
      n=max(NMagAModeNew,NMagAModeMax)
      m=max(MMagAModeNew,MMagAModeMax)
      allocate(MMagAMode(n),LMagAMode(n),MagAMode(m,n))
      if(NMagAModeMax.gt.0) then
        do i=1,NMagAMode
          MMagAMode(i)=MMagAModeO(i)
          LMagAMode(i)=LMagAModeO(i)
          call CopyVek(MagAModeO(1,i),MagAMode(1,i),MMagAMode(i))
        enddo
        if(allocated(MMagAModeO))
     1    deallocate(MMagAModeO,LMagAModeO,MagAModeO)
      endif
      NMagAModeMax=n
      MMagAModeMax=m
9999  return
      end
