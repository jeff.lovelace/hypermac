      subroutine ReallocateXYZAMode(NXYZAModeNew,MXYZAModeNew)
      use Atoms_mod
      character*8 LXYZAModeO(:)
      dimension MXYZAModeO(:),XYZAModeO(:,:)
      allocatable LXYZAModeO,MXYZAModeO,XYZAModeO
      if(NXYZAModeNew.le.NXYZAModeMax.and.
     1   MXYZAModeNew.le.MXYZAModeMax) go to 9999
      if(NXYZAModeMax.gt.0) then
        allocate(MXYZAModeO(NXYZAMode),LXYZAModeO(NXYZAMode),
     1           XYZAModeO(MXYZAModeMax,NXYZAMode))
        do i=1,NXYZAMode
          MXYZAModeO(i)=MXYZAMode(i)
          LXYZAModeO(i)=LXYZAMode(i)
          call CopyVek(XYZAMode(1,i),XYZAModeO(1,i),MXYZAMode(i))
        enddo
      endif
      if(allocated(MXYZAMode))
     1  deallocate(MXYZAMode,LXYZAMode,XYZAMode)
      n=max(NXYZAModeNew,NXYZAModeMax)
      m=max(MXYZAModeNew,MXYZAModeMax)
      allocate(MXYZAMode(n),LXYZAMode(n),XYZAMode(m,n))
      if(NXYZAModeMax.gt.0) then
        do i=1,NXYZAMode
          MXYZAMode(i)=MXYZAModeO(i)
          LXYZAMode(i)=LXYZAModeO(i)
          call CopyVek(XYZAModeO(1,i),XYZAMode(1,i),MXYZAMode(i))
        enddo
        if(allocated(MXYZAModeO))
     1    deallocate(MXYZAModeO,LXYZAModeO,XYZAModeO)
      endif
      NXYZAModeMax=n
      MXYZAModeMax=m
9999  return
      end
