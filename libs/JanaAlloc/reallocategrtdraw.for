      subroutine ReallocateGrtDraw(NPlus)
      use Grapht_mod
      integer, allocatable  :: DrawColorO(:),DrawParamO(:)
      character*80, allocatable :: DrawSymStO(:),DrawSymStUO(:)
      character*256, allocatable ::  DrawAtomO(:,:)
      if(DrawN+NPlus.lt.DrawNMax) go to 9999
      if(DrawN.gt.0) then
        allocate(DrawAtomO(4,DrawN),DrawSymStO(DrawN),
     1           DrawSymStUO(DrawN),DrawColorO(DrawN),
     2           DrawParamO(DrawN))
        DrawAtomO(1:4,1:DrawN)=DrawAtom(1:4,1:DrawN)
        DrawSymStO(1:DrawN)=DrawSymSt(1:DrawN)
        DrawSymStUO(1:DrawN)=DrawSymStU(1:DrawN)
        DrawColorO(1:DrawN)=DrawColor(1:DrawN)
        DrawParamO(1:DrawN)=DrawParam(1:DrawN)
      endif
      if(allocated(DrawAtom))
     1  deallocate(DrawAtom,DrawSymSt,DrawSymStU,
     2             DrawColor,DrawParam)
      n=DrawN+NPlus
      allocate(DrawAtom(4,n),DrawSymSt(n),DrawSymStU(n),DrawColor(n),
     1         DrawParam(n))
      if(DrawN.gt.0) then
        DrawAtom(1:4,1:DrawN)=DrawAtomO(1:4,1:DrawN)
        DrawSymSt(1:DrawN)=DrawSymStO(1:DrawN)
        DrawSymStU(1:DrawN)=DrawSymStUO(1:DrawN)
        DrawColor(1:DrawN)=DrawColorO(1:DrawN)
        DrawParam(1:DrawN)=DrawParamO(1:DrawN)
        deallocate(DrawAtomO,DrawSymStO,DrawSymStUO,DrawColorO,
     2             DrawParamO)
      endif
      DrawNMax=n
9999  return
      end
