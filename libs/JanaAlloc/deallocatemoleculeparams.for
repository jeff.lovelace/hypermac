      subroutine DeallocateMoleculeParams
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(tt)) deallocate(tt,tl,ts,stt,stl,sts)
      do i=0,2
        if(i.eq.0) then
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
        else if(i.eq.1) then
          if(allocated(utx)) deallocate(utx,uty,sutx,suty,urx,ury,surx,
     1                                  sury)
        else if(i.eq.2) then
          if(allocated(ttx)) deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,
     1                                  stly,tsx,tsy,stsx,stsy)
        endif
      enddo
      return
      end
