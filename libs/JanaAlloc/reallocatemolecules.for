      subroutine ReallocateMolecules(NPlus,MPlus)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
c
c   Musi byt alokovany vzdy
c
      character*27 LocMolSystStOld(:,:,:),LocPGSystStOld(:,:)
      character*8  MolNameOld(:),StRefPointOld(:),AtTransOld(:),
     1             SmbPGMolOld(:)
      character*2  LocMolSystAxOld(:,:),LocPGSystAxOld(:)
      integer ISwMolOld(:),KSwMolOld(:),KTLSOld(:),
     1        RefPointOld(:),KModMOld(:,:),KFMOld(:,:),KiMolOld(:,:),
     2        LocMolSystTypeOld(:),PrvniKiMolekulyOld(:),
     3        DelkaKiMolekulyOld(:),iamOld(:),mamOld(:),
     4        RotSignOld(:),NPointOld(:),KPointOld(:),IPointOld(:,:),
     5        TypeModFunMolOld(:)
      real    RotMolOld(:,:),RotIMolOld(:,:),TrMolOld(:,:),
     1        TrIMolOld(:,:),DRotFOld(:,:),DRotCOld(:,:),DRotPOld(:,:),
     2        RotBOld(:,:),DRotBFOld(:,:),DRotBCOld(:,:),DRotBPOld(:,:),
     3        TrPGOld(:,:),TriPGOld(:,:),RPointOld(:,:,:),
     4        SPointOld(:,:,:),TPointOld(:,:,:),LocPGSystXOld(:,:,:),
     5        AiMolOld(:),SAiMolOld(:),XmOld(:,:),TransOld(:,:),
     6        STransOld(:,:),EulerOld(:,:),SEulerOld(:,:),a0mOld(:),
     7        sa0mOld(:),LocMolSystXOld(:,:,:,:)
      logical UsePGSystOld(:)
      allocatable LocMolSystStOld,LocPGSystStOld,MolNameOld,
     1            StRefPointOld,AtTransOld,SmbPGMolOld,LocPGSystXOld,
     2            LocMolSystAxOld,LocPGSystAxOld,ISwMolOld,KSwMolOld,
     3            KTLSOld,RefPointOld,KModMOld,KFMOld,KiMolOld,
     4            LocMolSystTypeOld,PrvniKiMolekulyOld,
     5            DelkaKiMolekulyOld,iamOld,mamOld,RotSignOld,NPointOld,
     6            KPointOld,IPointOld,RotMolOld,RotIMolOld,TrMolOld,
     1            TrIMolOld,DRotFOld,DRotCOld,DRotPOld,RotBOld,
     2            DRotBFOld,DRotBCOld,DRotBPOld,TrPGOld,TriPGOld,
     3            RPointOld,SPointOld,TPointOld,AiMolOld,SAiMolOld,
     4            XmOld,TransOld,STransOld,EulerOld,SEulerOld,a0mOld,
     5            sa0mOld,UsePGSystOld,LocMolSystXOld,TypeModFunMolOld
c
c  Musi byt alokovany pro modulovane struktury
c
      real phfmOld(:),sphfmOld(:),OrthoX40MolOld(:),OrthoDeltaMolOld(:),
     1     OrthoEpsMolOld(:)
      allocatable phfmOld,sphfmOld,OrthoX40MolOld,OrthoDeltaMolOld,
     1            OrthoEpsMolOld
c
c  Musi byt alokovany pro TLS
c
      real ttOld(:,:),tlOld(:,:),tsOld(:,:),sttOld(:,:),stlOld(:,:),
     1     stsOld(:,:)
      allocatable ttOld,tlOld,tsOld,sttOld,stlOld,stsOld
      real p0(:,:),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),
     1             rx(:,:,:),ry(:,:,:),srx(:,:,:),sry(:,:,:),
     1             qx(:,:,:),qy(:,:,:),sqx(:,:,:),sqy(:,:,:)
      allocatable p0,px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy
      n=mxm
      m=mxp
      nm=n*m
      mw=0
      do i=1,3
        mw=max(mw,KModMMax(i))
      enddo
      allocate(MolNameOld(n),ISwMolOld(n),KSwMolOld(n),KModMOld(3,nm),
     1         KFMOld(3,nm),KTLSOld(n),StRefPointOld(n),
     2         RefPointOld(n),iamOld(n),mamOld(n),
     3         LocMolSystStOld(2,2,nm),LocMolSystTypeOld(nm),
     4         LocMolSystAxOld(2,nm),LocMolSystXOld(3,2,2,nm),
     5         RotSignOld(nm),RotMolOld(9,nm),RotIMolOld(9,nm),
     6         TrMolOld(9,nm),TriMolOld(9,nm),TypeModFunMolOld(nm),
     7         DRotFOld(9,nm),DRotCOld(9,nm),DRotPOld(9,nm),
     8         RotBOld(36,nm),DRotBFOld(36,nm),DRotBCOld(36,nm),
     9         DRotBPOld(36,nm),PrvniKiMolekulyOld(nm),
     a         DelkaKiMolekulyOld(nm),KiMolOld(mxdm,nm),UsePGSystOld(n),
     1         SmbPGMolOld(n),LocPGSystStOld(2,n),LocPGSystAxOld(n),
     2         TrPGOld(9,n),TriPGOld(9,n),LocPGSystXOld(3,2,n),
     3         NPointOld(n),KPointOld(n),IPointOld(mxpg,n),
     4         RPointOld(9,mxpg,n),SPointOld(3,mxpg,n),
     5         TPointOld(36,mxpg,n),
     6         AiMolOld(nm),SAiMolOld(nm),XmOld(3,n),AtTransOld(nm),
     7         TransOld(3,nm),STransOld(3,nm),EulerOld(3,nm),
     8         SEulerOld(3,nm),a0mOld(nm),sa0mOld(nm))
      do i=1,NMolec
        MolNameOld(i)=MolName(i)
        ISwMolOld(i)=ISwMol(i)
        KSwMolOld(i)=KSwMol(i)
        KTLSOld(i)=KTLS(i)
        StRefPointOld(i)=StRefPoint(i)
        RefPointOld(i)=RefPoint(i)
        KPointOld(i)=KPoint(i)
        iamOld(i)=iam(i)
        mamOld(i)=mam(i)
        UsePGSystOld(i)=UsePGSyst(i)
        NPointOld(i)=NPoint(i)
        call CopyVek(Xm(1,i),XmOld(1,i),3)
        SmbPGMolOld(i)=SmbPGMol(i)
        do k=1,NPoint(i)
          IPointOld(k,i)=IPoint(k,i)
          call CopyVek(RPoint(1,k,i),RPointOld(1,k,i),9)
          call CopyVek(SPoint(1,k,i),SPointOld(1,k,i),3)
          call CopyVek(TPoint(1,k,i),TPointOld(1,k,i),36)
        enddo
        LocPGSystStOld(1,i)=LocPGSystSt(1,i)
        LocPGSystStOld(2,i)=LocPGSystSt(2,i)
        LocPGSystAxOld(i)=LocPGSystAx(i)
        call CopyVek(LocPGSystX(1,1,i),LocPGSystXOld(1,1,i),6)
        call CopyVek(TrPG(1,i),TrPGOld(1,i),9)
        call CopyVek(TrIPG(1,i),TrIPGOld(1,i),9)
        ji=(i-1)*mxp
        do j=1,mam(i)
          ji=ji+1
          call CopyVekI(KModM(1,ji),KModMOld(1,ji),3)
          TypeModFunMolOld(ji)=TypeModFunMol(ji)
          call CopyVekI(KFM(1,ji),KFMOld(1,ji),3)
          LocMolSystTypeOld(ji)=LocMolSystType(ji)
          RotSignOld(ji)=RotSign(ji)
          call CopyVek(RotMol(1,ji),RotMolOld(1,ji),9)
          call CopyVek(RotIMol(1,ji),RotIMolOld(1,ji),9)
          call CopyVek(TrMol(1,ji),TrMolOld(1,ji),9)
          call CopyVek(TrIMol(1,ji),TrIMolOld(1,ji),9)
          call CopyVek(DRotF(1,ji),DRotFOld(1,ji),9)
          call CopyVek(DRotC(1,ji),DRotCOld(1,ji),9)
          call CopyVek(DRotP(1,ji),DRotPOld(1,ji),9)
          call CopyVek(RotB(1,ji),RotBOld(1,ji),36)
          call CopyVek(DRotBF(1,ji),DRotBFOld(1,ji),36)
          call CopyVek(DRotBC(1,ji),DRotBCOld(1,ji),36)
          call CopyVek(DRotBP(1,ji),DRotBPOld(1,ji),36)
          PrvniKiMolekulyOld(ji)=PrvniKiMolekuly(ji)
          DelkaKiMolekulyOld(ji)=DelkaKiMolekuly(ji)
          call CopyVekI(KiMol(1,ji),KiMolOld(1,ji),mxdm)
          AiMolOld(ji)=AiMol(ji)
          SAiMolOld(ji)=SAiMol(ji)
          AtTransOld(ji)=AtTrans(ji)
          a0mOld(ji)=a0m(ji)
          sa0mOld(ji)=sa0m(ji)
          call CopyVek(Trans(1,ji),TransOld(1,ji),3)
          call CopyVek(STrans(1,ji),STransOld(1,ji),3)
          call CopyVek(Euler(1,ji),EulerOld(1,ji),3)
          call CopyVek(SEuler(1,ji),SEulerOld(1,ji),3)
          do k=1,2
            LocMolSystAxOld(k,ji)=LocMolSystAx(k,ji)
            do l=1,2
              LocMolSystStOld(l,k,ji)=LocMolSystSt(l,k,ji)
              call CopyVek(LocMolSystX(1,l,k,ji),
     1                     LocMolSystXOld(1,l,k,ji),3)
            enddo
          enddo
        enddo
      enddo
      deallocate(MolName,ISwMol,KSwMol,KModM,KFM,KTLS,StRefPoint,
     1           RefPoint,iam,mam,LocMolSystSt,LocMolSystType,
     2           LocMolSystX,LocMolSystAx,TypeModFunMol,
     3           RotSign,RotMol,RotIMol,TrMol,TriMol,
     4           DRotF,DRotC,DRotP,RotB,DRotBF,DRotBC,DRotBP,
     5           PrvniKiMolekuly,DelkaKiMolekuly,KiMol,UsePGSyst,
     6           SmbPGMol,LocPGSystSt,LocPGSystAx,TrPG,TriPG,LocPGSystX,
     7           NPoint,KPoint,IPoint,RPoint,SPoint,TPoint,AiMol,SAiMol,
     8           Xm,AtTrans,Trans,STrans,Euler,SEuler,a0m,sa0m)
      n=n+NPlus
      m=m+MPlus
      nm=n*m
      allocate(MolName(n),ISwMol(n),KSwMol(n),KModM(3,nm),KFM(3,nm),
     1         KTLS(n),StRefPoint(n),RefPoint(n),iam(n),mam(n),
     2         LocMolSystSt(2,2,nm),LocMolSystType(nm),
     3         LocMolSystAx(2,nm),LocMolSystX(3,2,2,nm),
     4         RotSign(nm),RotMol(9,nm),RotiMol(9,nm),TrMol(9,nm),
     5         TriMol(9,nm),TypeModFunMol(nm),
     6         DRotF(9,nm),DRotC(9,nm),DRotP(9,nm),RotB(36,nm),
     7         DRotBF(36,nm),DRotBC(36,nm),DRotBP(36,nm),
     8         PrvniKiMolekuly(nm),DelkaKiMolekuly(nm),KiMol(mxdm,nm),
     9         UsePGSyst(n),SmbPGMol(n),LocPGSystSt(2,n),LocPGSystAx(n),
     a         TrPG(9,n),TriPG(9,n),LocPGSystX(3,2,n),
     1         NPoint(n),KPoint(n),IPoint(mxpg,n),RPoint(9,mxpg,n),
     2         SPoint(3,mxpg,n),TPoint(36,mxpg,n),
     3         AiMol(nm),SAiMol(nm),Xm(3,n),AtTrans(nm),Trans(3,nm),
     4         STrans(3,nm),Euler(3,nm),SEuler(3,nm),a0m(nm),sa0m(nm))
      do i=1,NMolec
        MolName(i)=MolNameOld(i)
        ISwMol(i)=ISwMolOld(i)
        KSwMol(i)=KSwMolOld(i)
        KTLS(i)=KTLSOld(i)
        StRefPoint(i)=StRefPointOld(i)
        RefPoint(i)=RefPointOld(i)
        iam(i)=iamOld(i)
        mam(i)=mamOld(i)
        UsePGSyst(i)=UsePGSystOld(i)
        NPoint(i)=NPointOld(i)
        call CopyVek(XmOld(1,i),Xm(1,i),3)
        SmbPGMol(i)=SmbPGMolOld(i)
        KPoint(i)=KPointOld(i)
        LocPGSystSt(1,i)=LocPGSystStOld(1,i)
        LocPGSystSt(2,i)=LocPGSystStOld(2,i)
        LocPGSystAx(i)=LocPGSystAxOld(i)
        call CopyVek(LocPGSystXOld(1,1,i),LocPGSystX(1,1,i),6)
        call CopyVek(TrPGOld(1,i),TrPG(1,i),9)
        call CopyVek(TrIPGOld(1,i),TrIPG(1,i),9)
        do k=1,NPoint(i)
          IPoint(k,i)=IPointOld(k,i)
          call CopyVek(RPointOld(1,k,i),RPoint(1,k,i),9)
          call CopyVek(SPointOld(1,k,i),SPoint(1,k,i),3)
          call CopyVek(TPointOld(1,k,i),TPoint(1,k,i),36)
        enddo
        ji=(i-1)*(mxp+MPlus)
        jio=(i-1)*mxp
        do j=1,mam(i)
          ji=ji+1
          jio=jio+1
          TypeModFunMol(ji)=TypeModFunMolOld(jio)
          call CopyVekI(KModMOld(1,jio),KModM(1,ji),3)
          call CopyVekI(KFMOld(1,jio),KFM(1,ji),3)
          LocMolSystType(ji)=LocMolSystTypeOld(jio)
          RotSign(ji)=RotSignOld(jio)
          call CopyVek(RotMolOld(1,jio),RotMol(1,ji),9)
          call CopyVek(RotIMolOld(1,jio),RotIMol(1,ji),9)
          call CopyVek(TrMolOld(1,jio),TrMol(1,ji),9)
          call CopyVek(TrIMolOld(1,jio),TrIMol(1,ji),9)
          call CopyVek(DRotFOld(1,jio),DRotF(1,ji),9)
          call CopyVek(DRotCOld(1,jio),DRotC(1,ji),9)
          call CopyVek(DRotPOld(1,jio),DRotP(1,ji),9)
          call CopyVek(RotBOld(1,jio),RotB(1,ji),36)
          call CopyVek(DRotBFOld(1,jio),DRotBF(1,ji),36)
          call CopyVek(DRotBCOld(1,jio),DRotBC(1,ji),36)
          call CopyVek(DRotBPOld(1,jio),DRotBP(1,ji),36)
          PrvniKiMolekuly(ji)=PrvniKiMolekulyOld(jio)
          DelkaKiMolekuly(ji)=DelkaKiMolekulyOld(jio)
          call CopyVekI(KiMolOld(1,jio),KiMol(1,ji),mxdm)
          AiMol(ji)=AiMolOld(jio)
          SAiMol(ji)=SAiMolOld(jio)
          AtTrans(ji)=AtTransOld(jio)
          a0m(ji)=a0mOld(jio)
          sa0m(ji)=sa0mOld(jio)
          call CopyVek(TransOld(1,jio),Trans(1,ji),3)
          call CopyVek(STransOld(1,jio),STrans(1,ji),3)
          call CopyVek(EulerOld(1,jio),Euler(1,ji),3)
          call CopyVek(SEulerOld(1,jio),SEuler(1,ji),3)
          do k=1,2
            LocMolSystAx(k,ji)=LocMolSystAxOld(k,jio)
            do l=1,2
              LocMolSystSt(l,k,ji)=LocMolSystStOld(l,k,jio)
              call CopyVek(LocMolSystXOld(1,l,k,jio),
     1                     LocMolSystX(1,l,k,ji),3)

            enddo
          enddo
        enddo
      enddo
      deallocate(MolNameOld,ISwMolOld,KSwMolOld,KModMOld,KFMOld,KTLSOld,
     1           StRefPointOld,RefPointOld,iamOld,mamOld,
     2           LocMolSystStOld,LocMolSystTypeOld,LocMolSystAxOld,
     3           LocMolSystXOld,TypeModFunMolOld,
     4           RotSignOld,RotMolOld,RotIMolOld,TrMolOld,TriMolOld,
     5           DRotFOld,DRotCOld,DRotPOld,RotBOld,DRotBFOld,DRotBCOld,
     6           DRotBPOld,PrvniKiMolekulyOld,DelkaKiMolekulyOld,
     7           KiMolOld,UsePGSystOld,SmbPGMolOld,LocPGSystStOld,
     8           LocPGSystAxOld,TrPGOld,TriPGOld,LocPGSystXOld,
     9           NPointOld,KPointOld,
     a           IPointOld,RPointOld,SPointOld,TPointOld,AiMolOld,
     1           SAiMolOld,XmOld,AtTransOld,TransOld,STransOld,EulerOld,
     2           SEulerOld,a0mOld,sa0mOld)
      if(KTLSMax.gt.0) then
        n=mxm
        m=mxp
        nm=n*m
        allocate( ttOld(6,nm), tlOld(6,nm), tsOld(9,nm),
     1           sttOld(6,nm),stlOld(6,nm),stsOld(9,nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            if(KTLS(i).gt.0) then
              call CopyVek(tt(1,ji),ttOld(1,ji),6)
              call CopyVek(tl(1,ji),tlOld(1,ji),6)
              call CopyVek(ts(1,ji),tsOld(1,ji),6)
              call CopyVek(stt(1,ji),sttOld(1,ji),6)
              call CopyVek(stl(1,ji),stlOld(1,ji),6)
              call CopyVek(sts(1,ji),stsOld(1,ji),6)
            endif
          enddo
        enddo
        n=n+NPlus
        m=m+MPlus
        nmNew=n*m
        deallocate(tt,tl,ts,stt,stl,sts)
        allocate( tt(6,nmNew), tl(6,nmNew), ts(9,nmNew),
     1           stt(6,nmNew),stl(6,nmNew),sts(9,nmNew))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            if(KTLS(i).gt.0) then
              call CopyVek(ttOld(1,ji),tt(1,ji),6)
              call CopyVek(tlOld(1,ji),tl(1,ji),6)
              call CopyVek(tsOld(1,ji),ts(1,ji),6)
              call CopyVek(sttOld(1,ji),stt(1,ji),6)
              call CopyVek(stlOld(1,ji),stl(1,ji),6)
              call CopyVek(stsOld(1,ji),sts(1,ji),6)
            endif
          enddo
        enddo
        deallocate(ttOld,tlOld,tsOld,sttOld,stlOld,stsOld)
      endif
      if(MaxNDimI.gt.0) then
        n=mxm
        m=mxp
        nm=n*m
        allocate(phfmOld(nm),sphfmOld(nm),OrthoX40MolOld(nm),
     1           OrthoDeltaMolOld(nm),OrthoEpsMolOld(nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            phfmOld(ji)=phfm(ji)
            sphfmOld(ji)=sphfm(ji)
            OrthoX40MolOld(ji)=OrthoX40Mol(ji)
            OrthoDeltaMolOld(ji)=OrthoDeltaMol(ji)
            OrthoEpsMolOld(ji)=OrthoEpsMol(ji)
          enddo
        enddo
        deallocate(phfm,sphfm,OrthoX40Mol,OrthoDeltaMol,OrthoEpsMol)
        n=n+NPlus
        m=m+MPlus
        nm=n*m
        allocate(phfm(nm),sphfm(nm),OrthoX40Mol(nm),
     1           OrthoDeltaMol(nm),OrthoEpsMol(nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          jio=(i-1)*(mxp+MPlus)
          do j=1,mam(i)
            ji=ji+1
            jio=jio+1
            phfm(jio)=phfmOld(ji)
            sphfm(jio)=sphfmOld(ji)
            OrthoX40Mol(jio)=OrthoX40MolOld(ji)
            OrthoDeltaMol(jio)=OrthoDeltaMolOld(ji)
            OrthoEpsMol(jio)=OrthoEpsMolOld(ji)
          enddo
        enddo
        deallocate(phfmOld,sphfmOld,OrthoX40MolOld,OrthoDeltaMolOld,
     1             OrthoEpsMolOld)
      endif
      n=mxm
      m=mxp
      nm=n*m
      n=n+NPlus
      m=m+MPlus
      nmNew=n*m
      do it=0,2
        n=TRank(it)
        KModP=KModMMax(it+1)
        if(it.eq.0) then
          if(KModP.gt.0) then
            allocate(p0(1,nm),px(n,KModP,nm),py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( axm(1,jio), px(1,1,jio),m)
                call CopyVek( aym(1,jio), py(1,1,jio),m)
                call CopyVek(saxm(1,jio),spx(1,1,jio),m)
                call CopyVek(saym(1,jio),spy(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
          if(mw.gt.0) then
            allocate( axm(mw,nmNew), aym(mw,nmNew))
            allocate(saxm(mw,nmNew),saym(mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), axm(1,ji),m)
                call CopyVek( py(1,1,jio), aym(1,ji),m)
                call CopyVek(spx(1,1,jio),saxm(1,ji),m)
                call CopyVek(spy(1,1,jio),saym(1,ji),m)
              enddo
            enddo
            deallocate(p0,px,py,spx,spy)
          endif
        else if(it.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( utx(1,1,jio), px(1,1,jio),m)
                call CopyVek( uty(1,1,jio), py(1,1,jio),m)
                call CopyVek(sutx(1,1,jio),spx(1,1,jio),m)
                call CopyVek(suty(1,1,jio),spy(1,1,jio),m)
                call CopyVek( urx(1,1,jio), rx(1,1,jio),m)
                call CopyVek( ury(1,1,jio), ry(1,1,jio),m)
                call CopyVek(surx(1,1,jio),srx(1,1,jio),m)
                call CopyVek(sury(1,1,jio),sry(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(utx))
     1      deallocate(utx,uty,sutx,suty,urx,ury,surx,sury)
          if(mw.gt.0) then
            allocate( utx(n,mw,nmNew), uty(n,mw,nmNew))
            allocate(sutx(n,mw,nmNew),suty(n,mw,nmNew))
            allocate( urx(n,mw,nmNew), ury(n,mw,nmNew))
            allocate(surx(n,mw,nmNew),sury(n,mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), utx(1,1,ji),m)
                call CopyVek( py(1,1,jio), uty(1,1,ji),m)
                call CopyVek(spx(1,1,jio),sutx(1,1,ji),m)
                call CopyVek(spy(1,1,jio),suty(1,1,ji),m)
                call CopyVek( rx(1,1,jio), urx(1,1,ji),m)
                call CopyVek( ry(1,1,jio), ury(1,1,ji),m)
                call CopyVek(srx(1,1,jio),surx(1,1,ji),m)
                call CopyVek(sry(1,1,jio),sury(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry)
          endif
          if(allocated(durdx)) deallocate(durdx)
          allocate(durdx(18,mw,nmNew))
        else if(it.eq.2) then
           if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm),
     4                qx(9,KModP,nm), qy(9,KModP,nm),
     5               sqx(9,KModP,nm),sqy(9,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( ttx(1,1,jio), px(1,1,jio),m)
                call CopyVek( tty(1,1,jio), py(1,1,jio),m)
                call CopyVek(sttx(1,1,jio),spx(1,1,jio),m)
                call CopyVek(stty(1,1,jio),spy(1,1,jio),m)
                call CopyVek( tlx(1,1,jio), rx(1,1,jio),m)
                call CopyVek( tly(1,1,jio), ry(1,1,jio),m)
                call CopyVek(stlx(1,1,jio),srx(1,1,jio),m)
                call CopyVek(stly(1,1,jio),sry(1,1,jio),m)
                m=KModM(it+1,ji)*9
                call CopyVek( tsx(1,1,jio), qx(1,1,jio),m)
                call CopyVek( tsy(1,1,jio), qy(1,1,jio),m)
                call CopyVek(stsx(1,1,jio),sqx(1,1,jio),m)
                call CopyVek(stsy(1,1,jio),sqy(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(ttx))
     1      deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,stly,tsx,tsy,stsx,
     2                 stsy)
          if(mw.gt.0) then
            allocate( ttx(n,mw,nmNew), tty(n,mw,nmNew))
            allocate(sttx(n,mw,nmNew),stty(n,mw,nmNew))
            allocate( tlx(n,mw,nmNew), tly(n,mw,nmNew))
            allocate(stlx(n,mw,nmNew),stly(n,mw,nmNew))
            allocate( tsx(9,mw,nmNew), tsy(9,mw,nmNew))
            allocate(stsx(9,mw,nmNew),stsy(9,mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), ttx(1,1,ji),m)
                call CopyVek( py(1,1,jio), tty(1,1,ji),m)
                call CopyVek(spx(1,1,jio),sttx(1,1,ji),m)
                call CopyVek(spy(1,1,jio),stty(1,1,ji),m)
                call CopyVek( rx(1,1,jio), tlx(1,1,ji),m)
                call CopyVek( ry(1,1,jio), tly(1,1,ji),m)
                call CopyVek(srx(1,1,jio),stlx(1,1,ji),m)
                call CopyVek(sry(1,1,jio),stly(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( qx(1,1,jio), tsx(1,1,ji),m)
                call CopyVek( qy(1,1,jio), tsy(1,1,ji),m)
                call CopyVek(sqx(1,1,jio),stsx(1,1,ji),m)
                call CopyVek(sqy(1,1,jio),stsy(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy)
          endif
        endif
      enddo
      mxm=mxm+NPlus
      mxp=mxp+MPlus
      mxpm=mxm*mxp
      return
      end
