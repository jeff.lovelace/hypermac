      subroutine ReallocateDRAve(NAvePlus,NAveRedPlus)
      use Datred_mod
      dimension RIAveArO(:),RSAveArO(:),DiffAveArO(:)
      integer HCondAveArO(:),Ave2OrgO(:,:),NAveRedArO(:)
      allocatable RIAveArO,RSAveArO,HCondAveArO,Ave2OrgO,NAveRedArO,
     1            DiffAveArO
      if(NAvePlus.le.0.and.NAveRedPlus.le.0) go to 9999
      if(NAve.gt.0) then
        n=NAve
        m=NAveRed
        allocate(HCondAveArO(n),RIAveArO(n),RSAveArO(n),NAveRedArO(n),
     1           DiffAveArO(n),Ave2OrgO(m,n))
        call CopyVekI(HCondAveAr,HCondAveArO,n)
        call CopyVekI(NAveRedAr,NAveRedArO,n)
        call CopyVek(RIAveAr,RIAveArO,n)
        call CopyVek(RSAveAr,RSAveArO,n)
        call CopyVek(DiffAveAr,DiffAveArO,n)
        do i=1,n
          call CopyVekI(Ave2Org(1,i),Ave2OrgO(1,i),NAveRedAr(i))
        enddo
      endif
      if(allocated(HCondAveAr))
     1  deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     2             Ave2Org)
      n=NAveMax+NAvePlus
      m=NAveRedMax+NAveRedPlus
      allocate(HCondAveAr(n),RIAveAr(n),RSAveAr(n),NAveRedAr(n),
     1         DiffAveAr(n),Ave2Org(m,n))
      if(NAve.gt.0) then
        nn=NAve
        call CopyVekI(HCondAveArO,HCondAveAr,nn)
        call CopyVekI(NAveRedArO,NAveRedAr,nn)
        call CopyVek(RIAveArO,RIAveAr,nn)
        call CopyVek(RSAveArO,RSAveAr,nn)
        call CopyVek(DiffAveArO,DiffAveAr,nn)
        do i=1,nn
          call CopyVekI(Ave2OrgO(1,i),Ave2Org(1,i),NAveRedAr(i))
        enddo
        deallocate(HCondAveArO,RIAveArO,RSAveArO,NAveRedArO,DiffAveArO,
     1             Ave2OrgO)
      endif
      NAveMax=n
      NAveRedMax=m
9999  return
      end
