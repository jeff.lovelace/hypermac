      subroutine ReallocateSavedPoints(MaxSavedPointsNew)
      use Basic_mod
      dimension XSavedPointOld(:,:)
      character*80 StSavedPointOld(:)
      allocatable XSavedPointOld,StSavedPointOld
      if(MaxSavedPointsNew.le.MaxSavedPoints) go to 9999
      if(NSavedPoints.gt.0) then
        allocate(XSavedPointOld(3,NSavedPoints),
     1          StSavedPointOld(NSavedPoints))
        call CopyVek(XSavedPoint,XSavedPointOld,3*NSavedPoints)
        call CopyStringArray(StSavedPoint,StSavedPointOld,NSavedPoints)
      endif
      MaxSavedPoints=MaxSavedPointsNew
      if(allocated(XSavedPoint)) deallocate(XSavedPoint,StSavedPoint)
      allocate(XSavedPoint(3,MaxSavedPoints),
     1         StSavedPoint(MaxSavedPoints))
      if(NSavedPoints.gt.0) then
        call CopyVek(XSavedPointOld,XSavedPoint,3*NSavedPoints)
        call CopyStringArray(StSavedPointOld,StSavedPoint,NSavedPoints)
        deallocate(XSavedPointOld,StSavedPointOld)
      endif
9999  return
      end
