      subroutine ReallocateAtoms(NAtPlus)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
c
c   Musi byt alokovany vzdy
c
      character*8  AtomOld(:)
      allocatable AtomOld
      integer isfOld(:),itfOld(:),ifrOld(:),kmolOld(:),lasmaxOld(:),
     1        iswaOld(:),kswaOld(:),kfaOld(:,:),kmodaOld(:,:),
     2        kmodaoOld(:,:),MagParOld(:),TypeModFunOld(:),
     3        KUsePolarOld(:),isaOld(:,:),AtSiteMultOld(:),
     4        PrvniKiAtomuOld(:),DelkaKiAtomuOld(:),KiAOld(:,:),
     5        NPGAtOld(:)
      real    aiOld(:),saiOld(:),a0Old(:),sa0Old(:),xOld(:,:),
     1        sxOld(:,:),betaOld(:,:),sbetaOld(:,:),durdrOld(:,:),
     2        xfrOld(:),sxfrOld(:),RPGAtOld(:,:,:)
      allocatable isfOld,itfOld,lasmaxOld,ifrOld,kmolOld,iswaOld,
     1            kswaOld,kfaOld,kmodaOld,kmodaoOld,MagParOld,
     2            isaOld,AtSiteMultOld,PrvniKiAtomuOld,DelkaKiAtomuOld,
     3            KiAOld,aiOld,saiOld,a0Old,sa0Old,xOld,sxOld,betaOld,
     4            sbetaOld,TypeModFunOld,KUsePolarOld,durdrOld,
     5            xfrOld,sxfrOld,NPGAtOld,RPGAtOld
c
c  Musi byt alokovany pro multipolove upresnovani
c
      character*27 LocAtSystStOld(:,:)
      character*8  SmbPGAtOld(:)
      character*2  LocAtSystAxOld(:)
      character*1  LocAtSenseOld(:)
      real TrAtOld(:,:),TriAtOld(:,:),TroAtOld(:,:),TroiAtOld(:,:),
     1     kapa1Old(:),kapa2Old(:),skapa1Old(:),skapa2Old(:),popcOld(:),
     2     spopcOld(:),popvOld(:),spopvOld(:),popasOld(:,:),
     3     spopasOld(:,:),LocAtSystXOld(:,:,:)
      allocatable LocAtSystStOld,LocAtSystAxOld,LocAtSenseOld,
     1            SmbPGAtOld,TrAtOld,TriAtOld,TroAtOld,TroiAtOld,
     2            kapa1Old,kapa2Old,skapa1Old,skapa2Old,popcOld,
     3            spopcOld,popvOld,spopvOld,popasOld,spopasOld,
     4            LocAtSystXOld
c
c  Musi byt alokovany pro modulovane struktury
c
      real qcntOld(:,:),phfOld(:),sphfOld(:),OrthoX40Old(:),
     1     OrthoDeltaOld(:),OrthoEpsOld(:)
      allocatable qcntOld,phfOld,sphfOld,OrthoX40Old,OrthoDeltaOld,
     1            OrthoEpsOld
c
c  Musi byt alokovany pro magneticke struktury
c
      character*8 NamePolarOld(:)
      real sm0Old(:,:),ssm0Old(:,:)
      allocatable NamePolarOld,sm0Old,ssm0Old
      real p0(:,:),sp0(:,:),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:)
      allocatable p0,sp0,px,py,spx,spy
      n=NAtAll
      allocate(AtomOld(n),isfOld(n),itfOld(n),ifrOld(n),kmolOld(n),
     1         lasmaxOld(n),iswaOld(n),kswaOld(n),kfaOld(7,n),
     2         kmodaOld(7,n),kmodaoOld(7,n),MagParOld(n),
     3         KUsePolarOld(n),TypeModFunOld(n),
     3         isaOld(MaxNSymm,n),AtSiteMultOld(n),PrvniKiAtomuOld(n),
     4         DelkaKiAtomuOld(n),KiAOld(mxda,n),
     5         aiOld(n),saiOld(n),a0Old(n),sa0Old(n),xOld(3,n),
     6         sxOld(3,n),betaOld(6,n),sbetaOld(6,n),durdrOld(9,n),
     7         xfrOld(n),sxfrOld(n))
      mw=0
      do i=1,7
        mw=max(mw,KModAMax(i))
      enddo
      do ia=1,NAtAll
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        AtomOld(ia)=Atom(ia)
        isfOld(ia)=isf(ia)
        itfOld(ia)=itf(ia)
        ifrOld(ia)=ifr(ia)
        kmolOld(ia)=kmol(ia)
        lasmaxOld(ia)=lasmax(ia)
        iswaOld(ia)=iswa(ia)
        kswaOld(ia)=kswa(ia)
        MagParOld(ia)=MagPar(ia)
        KUsePolarOld(ia)=KUsePolar(ia)
        TypeModFunOld(ia)=TypeModFun(ia)
        AtSiteMultOld(ia)=AtSiteMult(ia)
        PrvniKiAtomuOld(ia)=PrvniKiAtomu(ia)
        DelkaKiAtomuOld(ia)=DelkaKiAtomu(ia)
        aiOld(ia)=ai(ia)
        saiOld(ia)=sai(ia)
        a0Old(ia)=a0(ia)
        sa0Old(ia)=sa0(ia)
        xfrOld(ia)=xfr(ia)
        sxfrOld(ia)=sxfr(ia)
        call CopyVekI(kfa(1,ia),kfaOld(1,ia),7)
        call CopyVekI(kmoda(1,ia),kmodaOld(1,ia),7)
        call CopyVekI(kmodao(1,ia),kmodaoOld(1,ia),7)
        call CopyVekI(isa(1,ia),isaOld(1,ia),MaxNSymm)
        call CopyVekI(KiA(1,ia),KiAOld(1,ia),mxda)
        call CopyVek( x(1,ia), xOld(1,ia),3)
        call CopyVek(sx(1,ia),sxOld(1,ia),3)
        call CopyVek( beta(1,ia), betaOld(1,ia),6)
        call CopyVek(sbeta(1,ia),sbetaOld(1,ia),6)
        if(NMolec.gt.0) then
          if(allocated(durdr)) then
            call CopyVek(durdr(1,ia),durdrOld(1,ia),9)
          else
            call SetRealArrayTo(durdrOld(1,ia),9,0.)
          endif
        endif
      enddo
      deallocate(Atom,isf,itf,lasmax,ifr,kmol,iswa,kswa,kfa,kmoda,
     1           kmodao,MagPar,TypeModFun,KUsePolar,
     2           isa,WyckoffMult,WyckoffSmb,PrvniKiAtomu,
     3           DelkaKiAtomu,KiA,ai,sai,a0,sa0,x,sx,beta,sbeta,
     4           AtSiteMult,xfr,sxfr)
      n=MxAtAll+NAtPlus
      allocate(Atom(n),isf(n),itf(n),lasmax(n),ifr(n),kmol(n),
     1         iswa(n),kswa(n),KFA(7,n),KModA(7,n),kmodao(7,n),
     2         MagPar(n),TypeModFun(n),KUsePolar(n),
     3         isa(MaxNSymm,n),WyckoffMult(n),WyckoffSmb(n),
     4         PrvniKiAtomu(n),DelkaKiAtomu(n),KiA(mxda,n),
     5         ai(n),sai(n),a0(n),sa0(n),x(3,n),sx(3,n),beta(6,n),
     6         sbeta(6,n),AtSiteMult(n),xfr(n),sxfr(n))
      do ia=1,NAtAll
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        Atom(ia)=AtomOld(ia)
        isf(ia)=isfOld(ia)
        itf(ia)=itfOld(ia)
        ifr(ia)=ifrOld(ia)
        kmol(ia)=kmolOld(ia)
        lasmax(ia)=lasmaxOld(ia)
        iswa(ia)=iswaOld(ia)
        kswa(ia)=kswaOld(ia)
        MagPar(ia)=MagParOld(ia)
        KUsePolar(ia)=KUsePolarOld(ia)
        TypeModFun(ia)=TypeModFunOld(ia)
        AtSiteMult(ia)=AtSiteMultOld(ia)
        PrvniKiAtomu(ia)=PrvniKiAtomuOld(ia)
        DelkaKiAtomu(ia)=DelkaKiAtomuOld(ia)
        ai(ia)=aiOld(ia)
        sai(ia)=saiOld(ia)
        a0(ia)=a0Old(ia)
        sa0(ia)=sa0Old(ia)
        xfr(ia)=xfrOld(ia)
        sxfr(ia)=sxfrOld(ia)
        call CopyVekI(kfaOld(1,ia),kfa(1,ia),7)
        call CopyVekI(kmodaOld(1,ia),kmoda(1,ia),7)
        call CopyVekI(kmodaoOld(1,ia),kmodao(1,ia),7)
        call CopyVekI(isaOld(1,ia),isa(1,ia),MaxNSymm)
        call CopyVekI(KiAOld(1,ia),KiA(1,ia),mxda)
        call CopyVek( xOld(1,ia), x(1,ia),3)
        call CopyVek(sxOld(1,ia),sx(1,ia),3)
        call CopyVek( betaOld(1,ia), beta(1,ia),6)
        call CopyVek(sbetaOld(1,ia),sbeta(1,ia),6)
      enddo
      if(NMolec.gt.0) then
        if(allocated(tztl)) deallocate(tztl,tzts,durdr)
        allocate(tztl(36,n),tzts(54,n),durdr(9,n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          call CopyVek(durdrOld(1,ia),durdr(1,ia),9)
        enddo
      endif
      deallocate(AtomOld,isfOld,itfOld,ifrOld,kmolOld,lasmaxOld,
     1           iswaOld,kswaOld,kfaOld,kmodaOld,kmodaoOld,
     2           MagParOld,KUsePolarOld,TypeModFunOld,isaOld,
     3           AtSiteMultOld,PrvniKiAtomuOld,DelkaKiAtomuOld,KiAOld,
     4           aiOld,saiOld,a0Old,sa0Old,xOld,sxOld,betaOld,sbetaOld,
     5           durdrOld,xfrOld,sxfrOld)
      if(ChargeDensities) then
        n=NAtAll
        allocate(LocAtSystStOld(2,n),SmbPGAtOld(n),TrAtOld(9,n),
     1           TriAtOld(9,n),TroAtOld(9,n),TroiAtOld(9,n),
     2           LocAtSystAxOld(n),LocAtSenseOld(n),
     3           kapa1Old(n),kapa2Old(n),skapa1Old(n),skapa2Old(n),
     4           popcOld(n),spopcOld(n),popvOld(n),spopvOld(n),
     5           popasOld(64,n),spopasOld(64,n),NPGAtOld(n),
     6           RPGAtOld(9,48,n),LocAtSystXOld(3,2,n))
        do ia=1,NAtInd
          NPGAtOld(ia)=NPGAt(ia)
          do j=1,NPGAt(ia)
            call CopyMat(RPGAt(1,j,ia),RPGAtOld(1,j,ia),3)
          enddo
          call CopyMat(TrAt(1,ia),TrAtOld(1,ia),3)
          call CopyMat(TriAt(1,ia),TriAtOld(1,ia),3)
          call CopyMat(TroAt(1,ia),TroAtOld(1,ia),3)
          call CopyMat(TroiAt(1,ia),TroiAtOld(1,ia),3)
          kk=lasmax(ia)
          if(kk.le.0) then
            cycle
          else
            LocAtSystStOld(1,ia)=LocAtSystSt(1,ia)
            LocAtSystStOld(2,ia)=LocAtSystSt(2,ia)
            LocAtSystAxOld(ia)=LocAtSystAx(ia)
            LocAtSenseOld(ia)=LocAtSense(ia)
            SmbPGAtOld(ia)=SmbPGAt(ia)
            call CopyVek(LocAtSystX(1,1,ia),LocAtSystXOld(1,1,ia),6)
            popcOld(ia)=popc(ia)
            spopcOld(ia)=spopc(ia)
            popvOld(ia)=popv(ia)
            spopvOld(ia)=spopv(ia)
            kapa1Old(ia)=kapa1(ia)
            skapa1Old(ia)=skapa1(ia)
            call CopyVek( popas(1,ia), popasOld(1,ia),(kk-1)**2)
            call CopyVek(spopas(1,ia),spopasOld(1,ia),(kk-1)**2)
            kapa2Old(ia)=kapa2(ia)
            skapa2Old(ia)=skapa2(ia)
          endif
        enddo
        deallocate(LocAtSystSt,SmbPGAt,TrAt,TriAt,TroAt,TroiAt,
     1             LocAtSystAx,LocAtSense,kapa1,kapa2,skapa1,skapa2,
     2             popc,spopc,popv,spopv,popas,spopas,NPGAt,RPGAt,
     3             LocAtSystX)
        n=NAtAll+NAtPlus
        allocate(LocAtSystSt(2,n),SmbPGAt(n),TrAt(9,n),TriAt(9,n),
     1           TroAt(9,n),TroiAt(9,n),
     2           LocAtSystAx(n),LocAtSense(n),kapa1(n),kapa2(n),
     3           skapa1(n),skapa2(n),popc(n),spopc(n),popv(n),spopv(n),
     4           popas(64,n),spopas(64,n),NPGAt(n),RPGAt(9,48,n),
     5           LocAtSystX(3,2,n))
        do ia=1,NAtInd
          NPGAt(ia)=NPGAtOld(ia)
          do j=1,NPGAt(ia)
            call CopyMat(RPGAtOld(1,j,ia),RPGAt(1,j,ia),3)
          enddo
          call CopyMat(TrAtOld(1,ia),TrAt(1,ia),3)
          call CopyMat(TriAtOld(1,ia),TriAt(1,ia),3)
          call CopyMat(TroAtOld(1,ia),TroAt(1,ia),3)
          call CopyMat(TroiAtOld(1,ia),TroiAt(1,ia),3)
          kk=lasmax(ia)
          if(kk.le.0) then
            cycle
          else
            popc(ia)=popcOld(ia)
            spopc(ia)=spopcOld(ia)
            popv(ia)=popvOld(ia)
            spopv(ia)=spopvOld(ia)
            kapa1(ia)=kapa1Old(ia)
            skapa1(ia)=skapa1Old(ia)
            call CopyVek( popasOld(1,ia), popas(1,ia),(kk-1)**2)
            call CopyVek(spopasOld(1,ia),spopas(1,ia),(kk-1)**2)
            kapa2(ia)=kapa2Old(ia)
            skapa2(ia)=skapa2Old(ia)
            LocAtSystSt(1,ia)=LocAtSystStOld(1,ia)
            LocAtSystSt(2,ia)=LocAtSystStOld(2,ia)
            LocAtSystAx(ia)=LocAtSystAxOld(ia)
            LocAtSense(ia)=LocAtSenseOld(ia)
            SmbPGAt(ia)=SmbPGAtOld(ia)
            call CopyVek(LocAtSystXOld(1,1,ia),LocAtSystX(1,1,ia),6)
          endif
        enddo
        deallocate(LocAtSystStOld,SmbPGAtOld,TrAtOld,TriAtOld,
     1             TroAtOld,TroiAtOld,
     2             LocAtSystAxOld,LocAtSenseOld,kapa1Old,kapa2Old,
     3             skapa1Old,skapa2Old,popcOld,spopcOld,popvOld,
     4             spopvOld,popasOld,spopasOld,NPGAtOld,RPGAtOld,
     5             LocAtSystXOld)
      endif
      if(MaxNDimI.gt.0) then
        n=NAtAll
        allocate(qcntOld(3,n),phfOld(n),sphfOld(n),
     1           OrthoX40Old(n),OrthoDeltaOld(n),OrthoEpsOld(n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          if(NDimI(kswa(ia)).gt.0) then
            call CopyVek(qcnt(1,ia),qcntOld(1,ia),NDimI(kswa(ia)))
             phfOld(ia)= phf(ia)
            sphfOld(ia)=sphf(ia)
            OrthoX40Old(ia)=OrthoX40(ia)
            OrthoDeltaOld(ia)=OrthoDelta(ia)
            OrthoEpsOld(ia)=OrthoEps(ia)
          endif
        enddo
        deallocate(qcnt,phf,sphf,OrthoX40,OrthoDelta,OrthoEps)
        n=NAtAll+NAtPlus
        allocate(qcnt(3,n),phf(n),sphf(n),
     1           OrthoX40(n),OrthoDelta(n),OrthoEps(n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          if(NDimI(kswa(ia)).gt.0) then
            call CopyVek(qcntOld(1,ia),qcnt(1,ia),NDimI(kswa(ia)))
              phf(ia)= phfOld(ia)
            sphf(ia)=sphfOld(ia)
            OrthoX40(ia)=OrthoX40Old(ia)
            OrthoDelta(ia)=OrthoDeltaOld(ia)
            OrthoEps(ia)=OrthoEpsOld(ia)
          endif
        enddo
        deallocate(qcntOld,phfOld,sphfOld,OrthoX40Old,OrthoDeltaOld,
     1             OrthoEpsOld)
      endif
      if(MaxMagneticType.gt.0) then
        n=NAtAll
        allocate(NamePolarOld(n),sm0Old(3,n),ssm0Old(3,n))
        do ia=1,NAtInd
          if(MagPar(ia).gt.0) then
            call CopyVek( sm0(1,ia), sm0Old(1,ia),3)
            call CopyVek(ssm0(1,ia),ssm0Old(1,ia),3)
          endif
        enddo
        do ia=1,NPolar
          NamePolarOld(ia)=NamePolar(ia)
        enddo
        deallocate(NamePolar,sm0,ssm0)
        n=NAtAll+NAtPlus
        allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
        do ia=1,NAtInd
          if(MagPar(ia).gt.0) then
            call CopyVek( sm0Old(1,ia), sm0(1,ia),3)
            call CopyVek(ssm0Old(1,ia),ssm0(1,ia),3)
          endif
        enddo
        do ia=1,NPolar
          NamePolar(ia)=NamePolarOld(ia)
        enddo
        deallocate(NamePolarOld,sm0Old,ssm0Old)
      endif
      nn=NAtAll+NAtPlus
      do i=0,itfmax
        n=TRank(i)
        KModP=KModAMax(i+1)
        if(i.eq.0) then
          if(KModP.gt.0) then
            allocate(p0(1,NAtAll),px(n,KModP,NAtAll),py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( ax(1,ia), px(1,1,ia),m)
              call CopyVek( ay(1,ia), py(1,1,ia),m)
              call CopyVek(sax(1,ia),spx(1,1,ia),m)
              call CopyVek(say(1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(ax)) deallocate(ax,ay,sax,say)
          if(mw.gt.0) then
            allocate( ax(mw,nn), ay(mw,nn))
            allocate(sax(mw,nn),say(mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), ax(1,ia),m)
              call CopyVek( py(1,1,ia), ay(1,ia),m)
              call CopyVek(spx(1,1,ia),sax(1,ia),m)
              call CopyVek(spy(1,1,ia),say(1,ia),m)
            enddo
            deallocate(p0,px,py,spx,spy)
          endif
        else if(i.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( ux(1,1,ia), px(1,1,ia),m)
              call CopyVek( uy(1,1,ia), py(1,1,ia),m)
              call CopyVek(sux(1,1,ia),spx(1,1,ia),m)
              call CopyVek(suy(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(ux)) deallocate(ux,uy,sux,suy)
          if(mw.gt.0) then
            allocate(ux(n,mw,nn),uy(n,mw,nn))
            allocate(sux(n,mw,nn),suy(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), ux(1,1,ia),m)
              call CopyVek( py(1,1,ia), uy(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sux(1,1,ia),m)
              call CopyVek(spy(1,1,ia),suy(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.2) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( bx(1,1,ia), px(1,1,ia),m)
              call CopyVek( by(1,1,ia), py(1,1,ia),m)
              call CopyVek(sbx(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sby(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(bx)) deallocate(bx,by,sbx,sby)
          if(mw.gt.0) then
            allocate(bx(n,mw,nn),by(n,mw,nn))
            allocate(sbx(n,mw,nn),sby(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), bx(1,1,ia),m)
              call CopyVek( py(1,1,ia), by(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sbx(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sby(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.3) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.2) then
              call CopyVek(c3(1,ia),p0(1,ia),n)
              call CopyVek(sc3(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c3,sc3)
          nn=NAtAll+NAtPlus
          allocate(c3(n,nn),sc3(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.2) then
              call CopyVek(p0(1,ia),c3(1,ia),n)
              call CopyVek(sp0(1,ia),sc3(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c3x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c3y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc3x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc3y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
          if(mw.gt.0) then
            allocate(c3x(n,mw,nn),c3y(n,mw,nn))
            allocate(sc3x(n,mw,nn),sc3y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c3x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c3y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc3x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc3y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.4) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.3) then
              call CopyVek(c4(1,ia),p0(1,ia),n)
              call CopyVek(sc4(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c4,sc4)
          nn=NAtAll+NAtPlus
          allocate(c4(n,nn),sc4(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.3) then
              call CopyVek(p0(1,ia),c4(1,ia),n)
              call CopyVek(sp0(1,ia),sc4(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c4x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c4y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc4x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc4y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
          if(mw.gt.0) then
            allocate(c4x(n,mw,nn),c4y(n,mw,nn))
            allocate(sc4x(n,mw,nn),sc4y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c4x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c4y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc4x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc4y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.5) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.4) then
              call CopyVek(c5(1,ia),p0(1,ia),n)
              call CopyVek(sc5(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c5,sc5)
          nn=NAtAll+NAtPlus
          allocate(c5(n,nn),sc5(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.4) then
              call CopyVek(p0(1,ia),c5(1,ia),n)
              call CopyVek(sp0(1,ia),sc5(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c5x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c5y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc5x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc5y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
          if(mw.gt.0) then
            allocate(c5x(n,mw,nn),c5y(n,mw,nn))
            allocate(sc5x(n,mw,nn),sc5y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c5x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c5y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc5x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc5y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.6) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.5) then
              call CopyVek(c6(1,ia),p0(1,ia),n)
              call CopyVek(sc6(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c6,sc6)
          nn=NAtAll+NAtPlus
          allocate(c6(n,nn),sc6(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.5) then
              call CopyVek(p0(1,ia),c6(1,ia),n)
              call CopyVek(sp0(1,ia),sc6(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c6x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c6y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc6x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc6y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
          if(mw.gt.0) then
            allocate(c6x(n,mw,nn),c6y(n,mw,nn))
            allocate(sc6x(n,mw,nn),sc6y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c6x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c6y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc6x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc6y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        endif
      enddo
      if(MagParMax.gt.1) then
        n=3
        nn=NAtAll+NAtPlus
        allocate( px(n,MagParMax-1,NAtAll), py(n,MagParMax-1,NAtAll),
     1           spx(n,MagParMax-1,NAtAll),spy(n,MagParMax-1,NAtAll))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          m=(MagPar(ia)-1)*n
          if(m.gt.0) then
            call CopyVek( smx(1,1,ia), px(1,1,ia),m)
            call CopyVek( smy(1,1,ia), py(1,1,ia),m)
            call CopyVek(ssmx(1,1,ia),spx(1,1,ia),m)
            call CopyVek(ssmy(1,1,ia),spy(1,1,ia),m)
          endif
        enddo
        if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
        allocate( smx(n,MagParMax-1,nn), smy(n,MagParMax-1,nn),
     1           ssmx(n,MagParMax-1,nn),ssmy(n,MagParMax-1,nn))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          m=(MagPar(ia)-1)*n
          if(m.gt.0) then
            call CopyVek( px(1,1,ia), smx(1,1,ia),m)
            call CopyVek( py(1,1,ia), smy(1,1,ia),m)
            call CopyVek(spx(1,1,ia),ssmx(1,1,ia),m)
            call CopyVek(spy(1,1,ia),ssmy(1,1,ia),m)
          endif
        enddo
        deallocate(px,py,spx,spy)
      endif
      MxAtAll=MxAtAll+NAtPlus
9999  return
      end
