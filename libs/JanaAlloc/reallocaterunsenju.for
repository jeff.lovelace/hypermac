      subroutine ReallocateRunSENJU(NPlus)
      use Datred_mod
      integer RunSENJUO(:)
      allocatable RunSENJUO
      if(NRunSENJU.lt.NRunSENJUMax) go to 9999
      if(NRunSENJU.gt.0) then
        allocate(RunSENJUO(NRunSENJU))
        call CopyVekI(RunSENJU,RunSENJUO,NRunSENJU)
      endif
      if(allocated(RunSENJU)) deallocate(RunSENJU)
      n=NRunSENJUMax+NPlus
      allocate(RunSENJU(n))
      if(NRunSENJU.gt.0) then
        call CopyVekI(RunSENJUO,RunSENJU,NRunSENJU)
        deallocate(RunSENJUO)
      endif
      NRunSENJUMax=n
9999  return
      end
