      subroutine DeallocateAtomParams
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=0,6
        if(i.eq.0) then
          if(allocated(ax)) deallocate(ax,ay,sax,say)
        else if(i.eq.1) then
          if(allocated(ux)) deallocate(ux,uy,sux,suy)
        else if(i.eq.2) then
          if(allocated(bx)) deallocate(bx,by,sbx,sby)
        else if(i.eq.3) then
          if(allocated(c3)) deallocate(c3,sc3)
          if(allocated(c3x)) deallocate(c3x,sc3x,c3y,sc3y)
        else if(i.eq.4) then
          if(allocated(c4)) deallocate(c4,sc4)
          if(allocated(c4x)) deallocate(c4x,sc4x,c4y,sc4y)
        else if(i.eq.5) then
          if(allocated(c5)) deallocate(c5,sc5)
          if(allocated(c5x)) deallocate(c5x,sc5x,c5y,sc5y)
        else if(i.eq.6) then
          if(allocated(c6)) deallocate(c6,sc6)
          if(allocated(c6x)) deallocate(c6x,sc6x,c6y,sc6y)
        endif
      enddo
      if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
      return
      end
