      subroutine ReallocateEDZones(NPlus)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer, allocatable ::  KiEDO(:,:,:),NThickEDZoneO(:,:)
      real, allocatable :: HEDZoneO(:,:,:),AlphaEDZoneO(:,:),
     1                     BetaEDZoneO(:,:),PrAngEDZoneO(:,:),
     2                     ScEDZoneO(:,:),ScEDZoneSO(:,:),
     2                     RFacEDZoneO(:,:),
     3                     ThickEDZoneO(:,:),ThickEDZoneSO(:,:),
     4                     XNormEDZoneO(:,:),XNormEDZoneSO(:,:),
     5                     YNormEDZoneO(:,:),YNormEDZoneSO(:,:),
     6                     PhiEDZoneO(:,:),PhiEDZoneSO(:,:),
     7                     ThetaEDZoneO(:,:),ThetaEDZoneSO(:,:)
      logical, allocatable :: UseEDZoneO(:,:)
      if(allocated(KiED)) then
        KRefBlockO=ubound(KiED,3)
      else
        KRefBlockO=0
      endif
      if(NPlus.le.0.and.KRefBlock.le.KRefBlockO) go to 9999
      NEDZoneAll=0
      do i=1,KRefBlockO
        NEDZoneAll=NEDZoneAll+NEDZone(i)
      enddo
      if(NEDZoneAll.gt.0) then
        n=NEDZoneAll
        m=KRefBlockO
        allocate(HEDZoneO(3,n,m),RFacEDZoneO(n,m),
     1           AlphaEDZoneO(n,m),NThickEDZoneO(n,m),
     2           BetaEDZoneO(n,m),PrAngEDZoneO(n,m),
     3           ScEDZoneO(n,m),ScEDZoneSO(n,m),
     4           ThickEDZoneO(n,m),ThickEDZoneSO(n,m),
     5           XNormEDZoneO(n,m),XNormEDZoneSO(n,m),
     6           YNormEDZoneO(n,m),YNormEDZoneSO(n,m),
     7           PhiEDZoneO(n,m),PhiEDZoneSO(n,m),
     8           ThetaEDZoneO(n,m),ThetaEDZoneSO(n,m),
     9           KiEDO(EDNPar,n,m),UseEDZoneO(n,m))
        do KRefB=1,KRefBlockO
          n=NEDZone(KRefB)
          call CopyVek(HEDZone(1,1,KRefB),HEDZoneO(1,1,KRefB),3*n)
          call CopyVek(RFacEDZone(1,KRefB),RFacEDZoneO(1,KRefB),n)
          call CopyVek(AlphaEDZone(1,KRefB),AlphaEDZoneO(1,KRefB),n)
          call CopyVek(BetaEDZone(1,KRefB),BetaEDZoneO(1,KRefB),n)
          call CopyVek(PrAngEDZone(1,KRefB),PrAngEDZoneO(1,KRefB),n)
          call CopyVek(ScEDZone(1,KRefB),ScEDZoneO(1,KRefB),n)
          call CopyVek(ScEDZoneS(1,KRefB),ScEDZoneSO(1,KRefB),n)
          call CopyVek(ThickEDZone(1,KRefB),ThickEDZoneO(1,KRefB),n)
          call CopyVek(ThickEDZoneS(1,KRefB),ThickEDZoneSO(1,KRefB),n)
          call CopyVek(XNormEDZone(1,KRefB),XNormEDZoneO(1,KRefB),n)
          call CopyVek(XNormEDZoneS(1,KRefB),XNormEDZoneSO(1,KRefB),n)
          call CopyVek(YNormEDZone(1,KRefB),YNormEDZoneO(1,KRefB),n)
          call CopyVek(YNormEDZoneS(1,KRefB),YNormEDZoneSO(1,KRefB),n)
          call CopyVek(PhiEDZone(1,KRefB),PhiEDZoneO(1,KRefB),n)
          call CopyVek(PhiEDZoneS(1,KRefB),PhiEDZoneSO(1,KRefB),n)
          call CopyVek(ThetaEDZone(1,KRefB),ThetaEDZoneO(1,KRefB),n)
          call CopyVek(ThetaEDZoneS(1,KRefB),ThetaEDZoneSO(1,KRefB),n)
          call CopyVekI(KiED(1,1,KRefB),KiEDO(1,1,KRefB),EDNPar*n)
          call CopyVekI(NThickEDZone(1,KRefB),NThickEDZoneO(1,KRefB),n)
          do i=1,n
            UseEDZoneO(i,KRefB)=UseEDZone(i,KRefB)
          enddo
        enddo
      endif
      if(allocated(KiED))
     1  deallocate(HEDZone,RFacEDZone,AlphaEDZone,
     2             BetaEDZone,PrAngEDZone,
     3             ScEDZone,ScEDZoneS,NThickEDZone,
     4             ThickEDZone,ThickEDZoneS,
     5             XNormEDZone,XNormEDZoneS,
     6             YNormEDZone,YNormEDZoneS,
     7             PhiEDZone,PhiEDZoneS,
     8             ThetaEDZone,ThetaEDZoneS,
     9             KiED,UseEdZone)
      n=NMaxEDZone+NPlus
      m=NRefBlock
      allocate(HEDZone(3,n,m),RFacEDZone(n,m),AlphaEDZone(n,m),
     1         BetaEDZone(n,m),PrAngEDZone(n,m),
     2         ScEDZone(n,m),ScEDZoneS(n,m),NThickEDZone(n,m),
     3         ThickEDZone(n,m),ThickEDZoneS(n,m),
     4         XNormEDZone(n,m),XNormEDZoneS(n,m),
     5         YNormEDZone(n,m),YNormEDZoneS(n,m),
     6         PhiEDZone(n,m),PhiEDZoneS(n,m),
     7         ThetaEDZone(n,m),ThetaEDZoneS(n,m),
     8         KiED(EDNPar,n,m),UseEdZone(n,m))
      NMaxEDZone=n
      if(NEDZoneAll.gt.0) then
        n=NEDZoneAll
        m=KRefBlockO
        do KRefB=1,KRefBlockO
          n=NEDZone(KRefB)
          call CopyVek(HEDZoneO(1,1,KRefB),HEDZone(1,1,KRefB),3*n)
          call CopyVek(RFacEDZoneO(1,KRefB),RFacEDZone(1,KRefB),n)
          call CopyVek(AlphaEDZoneO(1,KRefB),AlphaEDZone(1,KRefB),n)
          call CopyVek(BetaEDZoneO(1,KRefB),BetaEDZone(1,KRefB),n)
          call CopyVek(PrAngEDZoneO(1,KRefB),PrAngEDZone(1,KRefB),n)
          call CopyVek(ScEDZoneO(1,KRefB),ScEDZone(1,KRefB),n)
          call CopyVek(ScEDZoneSO(1,KRefB),ScEDZoneS(1,KRefB),n)
          call CopyVek(ThickEDZoneO(1,KRefB),ThickEDZone(1,KRefB),n)
          call CopyVek(ThickEDZoneSO(1,KRefB),ThickEDZoneS(1,KRefB),n)
          call CopyVek(XNormEDZoneO(1,KRefB),XNormEDZone(1,KRefB),n)
          call CopyVek(XNormEDZoneSO(1,KRefB),XNormEDZoneS(1,KRefB),n)
          call CopyVek(YNormEDZoneO(1,KRefB),YNormEDZone(1,KRefB),n)
          call CopyVek(YNormEDZoneSO(1,KRefB),YNormEDZoneS(1,KRefB),n)
          call CopyVek(PhiEDZoneO(1,KRefB),PhiEDZone(1,KRefB),n)
          call CopyVek(PhiEDZoneSO(1,KRefB),PhiEDZoneS(1,KRefB),n)
          call CopyVek(ThetaEDZoneO(1,KRefB),ThetaEDZone(1,KRefB),n)
          call CopyVek(ThetaEDZoneSO(1,KRefB),ThetaEDZoneS(1,KRefB),n)
          call CopyVekI(KiEDO(1,1,KRefB),KiED(1,1,KRefB),EDNPar*n)
          call CopyVekI(NThickEDZoneO(1,KRefB),NThickEDZone(1,KRefB),n)
          do i=1,n
            UseEDZone(i,KRefB)=UseEDZoneO(i,KRefB)
          enddo
        enddo
        deallocate(HEDZoneO,RFacEDZoneO,AlphaEDZoneO,
     1             BetaEDZoneO,PrAngEDZoneO,
     2             ScEDZoneO,ScEDZoneSO,NThickEDZoneO,
     3             ThickEDZoneO,ThickEDZoneSO,
     4             XNormEDZoneO,XNormEDZoneSO,
     5             YNormEDZoneO,YNormEDZoneSO,
     6             PhiEDZoneO,PhiEDZoneSO,
     7             ThetaEDZoneO,ThetaEDZoneSO,
     8             KiEDO,UseEDZoneO)
      endif
      go to 9999
      entry DeallocateEDZones
      if(allocated(KiED))
     1  deallocate(HEDZone,RFacEDZone,AlphaEDZone,
     2             BetaEDZone,PrAngEDZone,
     3             ScEDZone,ScEDZoneS,NThickEDZone,
     4             ThickEDZone,ThickEDZoneS,
     5             XNormEDZone,XNormEDZoneS,
     6             YNormEDZone,YNormEDZoneS,
     7             PhiEDZone,PhiEDZoneS,
     8             ThetaEDZone,ThetaEDZoneS,
     9             KiED,UseEDZone)
      NMaxEDZone=0
9999  return
      end
