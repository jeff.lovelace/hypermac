      subroutine ReallocEquations(mxeNew,mxepNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer      lnpOld(:),lnpoOld(:),pnpOld(:,:),npaOld(:)
      real         pkoOld(:,:),pabOld(:)
      character*20 latOld(:),patOld(:,:)
      character*12 lpaOld(:),ppaOld(:,:)
      logical      eqharOld(:)
      allocatable  lnpOld,lnpoOld,pnpOld,npaOld,pkoOld,pabOld,latOld,
     1             patOld,lpaOld,ppaOld,eqharOld
      if(allocated(pnp)) then
        mxeOld=ubound(pnp,2)
        mxepOld=ubound(pnp,1)
        if(mxeOld.ge.mxeNew.and.mxepOld.ge.mxepNew) go to 9999
        allocate(lnpOld(mxeOld),pnpOld(mxepOld,mxeOld),
     1           npaOld(mxeOld),pkoOld(mxepOld,mxeOld),
     2           latOld(mxeOld),patOld(mxepOld,mxeOld),
     3           lpaOld(mxeOld),ppaOld(mxepOld,mxeOld),
     4           lnpoOld(mxeOld),pabOld(mxeOld),eqharOld(mxeOld))
        do i=1,neq
          lnpOld(i)=lnp(i)
          npaOld(i)=npa(i)
          latOld(i)=lat(i)
          lpaOld(i)=lpa(i)
          pabOld(i)=pab(i)
          lnpoOld(i)=lnpo(i)
          eqharOld(i)=eqhar(i)
          do j=1,npa(i)
            pnpOld(j,i)=pnp(j,i)
            pkoOld(j,i)=pko(j,i)
            patOld(j,i)=pat(j,i)
            ppaOld(j,i)=ppa(j,i)
          enddo
        enddo
        deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,lnpo,pab,eqhar)
      else
        mxeOld=0
        mxepOld=0
      endif
      mxe=mxeNew
      mxep=mxepNew
      allocate(lnp(mxe),pnp(mxep,mxe),
     1         npa(mxe),pko(mxep,mxe),
     2         lat(mxe),pat(mxep,mxe),
     3         lpa(mxe),ppa(mxep,mxe),
     4         lnpo(mxe),pab(mxe),eqhar(mxe))
      if(mxeOld.gt.0) then
        do i=1,neq
          lnp(i)=lnpOld(i)
          npa(i)=npaOld(i)
          lat(i)=latOld(i)
          lpa(i)=lpaOld(i)
          pab(i)=pabOld(i)
          lnpo(i)=lnpoOld(i)
          eqhar(i)=eqharOld(i)
c        if(npa(i).gt.0) then
c          jk=npaOld(i)
c        else
c          jk=mxepOld
c        endif
          do j=1,npaOld(i)
            pnp(j,i)=pnpOld(j,i)
            pko(j,i)=pkoOld(j,i)
            pat(j,i)=patOld(j,i)
            ppa(j,i)=ppaOld(j,i)
          enddo
        enddo
        deallocate(lnpOld,pnpOld,npaOld,pkoOld,latOld,patOld,lpaOld,
     1             ppaOld,lnpoOld,pabOld,eqharOld)
      endif
9999  return
      end
