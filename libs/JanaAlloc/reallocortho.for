      subroutine ReallocOrtho(n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real orx40o(:),ordelo(:),orepso(:)
      character*12 orao(:)
      allocatable orx40o,ordelo,orepso,orao
      if(n.le.mxo) go to 9999
      if(nor.gt.0) then
        allocate(orx40o(nor),ordelo(nor),orepso(nor),orao(nor))
        call CopyVek(OrX40,OrX40O,nor)
        call CopyVek(OrDel,OrDelO,nor)
        call CopyVek(OrEps,OrEpsO,nor)
        do i=1,nor
          OrAO(i)=OrA(i)
        enddo
      endif
      if(allocated(OrX40)) deallocate(OrX40,OrDel,OrEps,OrA)
      allocate(OrX40(n),OrDel(n),OrEps(n),OrA(n))
      mxo=n
      if(nor.gt.0) then
        call CopyVek(OrX40O,OrX40,nor)
        call CopyVek(OrDelO,OrDel,nor)
        call CopyVek(OrEpsO,OrEps,nor)
        do i=1,nor
          OrA(i)=OrAO(i)
        enddo
        deallocate(OrX40O,OrDelO,OrEpsO,OrAO)
      endif
9999  return
      end
