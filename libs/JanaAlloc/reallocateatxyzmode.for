      subroutine ReallocateAtXYZMode(NPlus)
      use Atoms_mod
      character*8 LAtXYZModeO(:,:)
      integer MAtXYZModeO(:),NMAtXYZModeO(:),JAtXYZModeO(:),
     1        IAtXYZModeO(:,:),KiAtXYZModeO(:,:)
      real AtXYZModeO(:,:)
      allocatable LAtXYZModeO,MAtXYZModeO,NMAtXYZModeO,IAtXYZModeO,
     1            KiAtXYZModeO,AtXYZModeO,JAtXYZModeO
      if(NAtXYZMode.lt.NAtXYZModeMax) go to 9999
      if(NAtXYZModeMax.gt.0) then
        allocate(MAtXYZModeO(NAtXYZMode),NMAtXYZModeO(NAtXYZMode),
     1           IAtXYZModeO(MXYZAModeMax/3,NAtXYZMode),
     2           LAtXYZModeO(MXYZAModeMax,NAtXYZMode),
     3           KiAtXYZModeO(MXYZAModeMax,NAtXYZMode),
     4           AtXYZModeO(MXYZAModeMax,NAtXYZMode),
     5           JAtXYZModeO(NAtXYZMode))
        do i=1,NAtXYZMode
          MAtXYZModeO(i)=MAtXYZMode(i)
          JAtXYZModeO(i)=JAtXYZMode(i)
          NMAtXYZModeO(i)=NMAtXYZMode(i)
          call CopyVekI(IAtXYZMode(1,i),IAtXYZModeO(1,i),
     1                  MAtXYZMode(i))
          do j=1,NMAtXYZMode(i)
            LAtXYZModeO(j,i)=LAtXYZMode(j,i)
          enddo
          call CopyVekI(KiAtXYZMode(1,i),KiAtXYZModeO(1,i),
     1                  NMAtXYZMode(i))
          call CopyVek(AtXYZMode(1,i),AtXYZModeO(1,i),
     1                 NMAtXYZMode(i))
        enddo
      endif
      if(allocated(MAtXYZMode))
     1  deallocate(MAtXYZMode,NMAtXYZMode,IAtXYZMode,LAtXYZMode,
     2             KiAtXYZMode,AtXYZMode,SAtXYZMode,FAtXYZMode,
     3             KAtXYZMode,JAtXYZMode)
      n=NAtXYZModeMax+NPlus
      allocate(MAtXYZMode(n),NMAtXYZMode(n),
     1         IAtXYZMode(MXYZAModeMax/3,n),
     2         LAtXYZMode(MXYZAModeMax,n),
     3         KiAtXYZMode(MXYZAModeMax,n),
     4         AtXYZMode(MXYZAModeMax,n),
     5         SAtXYZMode(MXYZAModeMax,n),
     6         FAtXYZMode(MXYZAModeMax,n),
     7         KAtXYZMode(MXYZAModeMax,n),
     8         JAtXYZMode(n))
      if(NAtXYZModeMax.gt.0) then
        do i=1,NAtXYZMode
          MAtXYZMode(i)=MAtXYZModeO(i)
          JAtXYZMode(i)=JAtXYZModeO(i)
          NMAtXYZMode(i)=NMAtXYZModeO(i)
          call CopyVekI(IAtXYZModeO(1,i),IAtXYZMode(1,i),
     1                  MAtXYZMode(i))
          do j=1,NMAtXYZMode(i)
            LAtXYZMode(j,i)=LAtXYZModeO(j,i)
          enddo
          call CopyVekI(KiAtXYZModeO(1,i),KiAtXYZMode(1,i),
     1                  NMAtXYZMode(i))
          call CopyVek(AtXYZModeO(1,i),AtXYZMode(1,i),
     1                 NMAtXYZMode(i))
        enddo
        deallocate(MAtXYZModeO,NMAtXYZModeO,IAtXYZModeO,LAtXYZModeO,
     1             KiAtXYZModeO,AtXYZModeO,JAtXYZModeO)
      endif
      NAtXYZModeMax=n
9999  return
      end
