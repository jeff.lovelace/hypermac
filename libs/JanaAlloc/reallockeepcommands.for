      subroutine ReallocKeepCommands(n,m)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 KeepAtO(:,:),KeepAtCentrO(:),KeepAtNeighO(:,:),
     1             KeepAtHO(:,:),KeepAtAnchorO(:)
      integer KeepTypeO(:),KeepNO(:),KeepNAtO(:,:),KeepKiXO(:,:),
     1        KeepNAtCentrO(:),KeepKiXCentrO(:),
     3        KeepNNeighO(:),KeepNAtNeighO(:,:),KeepKiXNeighO(:,:),
     4        KeepNHO(:),KeepNAtHO(:,:),KeepKiXHO(:,:),
     5        KeepNAtAnchorO(:),KeepKiXAnchorO(:)
      real KeepDistHO(:),KeepAngleHO(:),KeepADPExtFacO(:)
      allocatable KeepAtO,KeepAtCentrO,KeepAtNeighO,KeepAtHO,
     1            KeepAtAnchorO,KeepTypeO,KeepNO,KeepNAtO,KeepKiXO,
     2            KeepNAtCentrO,KeepKiXCentrO,KeepNNeighO,KeepNAtNeighO,
     3            KeepKiXNeighO,KeepNHO,KeepNAtHO,KeepKiXHO,
     4            KeepNAtAnchorO,KeepKiXAnchorO,KeepDistHO,KeepAngleHO,
     5            KeepADPExtFacO
      if(NKeep.gt.0) then
        allocate(KeepTypeO(NKeep),KeepNO(NKeep),
     1           KeepNAtO(NKeep,NKeepAtMax),
     1           KeepAtO(NKeep,NKeepAtMax),KeepKiXO(NKeep,NKeepAtMax),
     2           KeepNAtCentrO(NKeep),KeepKiXCentrO(NKeep),
     3           KeepAtCentrO(NKeep),
     4           KeepNNeighO(NKeep),KeepNAtNeighO(NKeep,5),
     5           KeepAtNeighO(NKeep,5),
     6           KeepKiXNeighO(NKeep,5),
     7           KeepNHO(NKeep),KeepNAtHO(NKeep,4),
     8           KeepAtHO(NKeep,4),
     9           KeepKiXHO(NKeep,4),
     a           KeepNAtAnchorO(NKeep),KeepKiXAnchorO(NKeep),
     1           KeepAtAnchorO(NKeep),
     2           KeepDistHO(NKeep),KeepAngleHO(NKeep),
     9           KeepADPExtFacO(NKeep))
        do i=1,NKeep
          KeepTypeO(i)=KeepType(i)
          k=KeepType(i)/10
          KeepNAtCentrO(i)=KeepNAtCentr(i)
          if(k.eq.IdKeepHydro.or.k.eq.IdKeepADP) then
            KeepAtCentrO(i)=KeepAtCentr(i)
            if(k.eq.IdKeepHydro) then
              KeepKiXCentrO(i)=KeepKiXCentr(i)
              KeepNNeighO(i)=KeepNNeigh(i)
              do j=1,KeepNNeigh(i)
                KeepNAtNeighO(i,j)=KeepNAtNeigh(i,j)
                KeepKiXNeighO(i,j)=KeepKiXNeigh(i,j)
                KeepAtNeighO(i,j)=KeepAtNeigh(i,j)
              enddo
              KeepNAtAnchorO(i)=KeepNAtAnchor(i)
              KeepAtAnchorO(i)=KeepAtAnchor(i)
              KeepKiXAnchorO(i)=KeepKiXAnchor(i)
              KeepDistHO(i)=KeepDistH(i)
              KeepAngleHO(i)=KeepAngleH(i)
            else
              KeepADPExtFacO(i)=KeepADPExtFac(i)
            endif
            KeepNHO(i)=KeepNH(i)
            do j=1,KeepNH(i)
              KeepNAtHO(i,j)=KeepNAtH(i,j)
              KeepAtHO(i,j)=KeepAtH(i,j)
              if(k.eq.IdKeepHydro) KeepKiXHO(i,j)=KeepKiXH(i,j)
            enddo
          else
            KeepNO(i)=KeepN(i)
            do j=1,KeepN(i)
              KeepNAtO(i,j)=KeepNAt(i,j)
              KeepAtO(i,j)=KeepAt(i,j)
              KeepKiXO(i,j)=KeepKiX(i,j)
            enddo
          endif
        enddo
      endif
      if(allocated(KeepType))
     1  deallocate(KeepType,KeepN,KeepNAt,KeepAt,KeepKiX,KeepNAtCentr,
     2             KeepAtCentr,KeepKiXCentr,KeepNNeigh,KeepNAtNeigh,
     3             KeepAtNeigh,KeepKiXNeigh,KeepNH,KeepNAtH,KeepAtH,
     4             KeepKiXH,KeepNAtAnchor,KeepAtAnchor,KeepKiXAnchor,
     5             KeepDistH,KeepAngleH,KeepADPExtFac)
      allocate(KeepType(n),KeepN(n),KeepNAt(n,m),KeepAt(n,m),
     1         KeepKiX(n,m),KeepNAtCentr(n),KeepAtCentr(n),
     2         KeepKiXCentr(n),KeepNNeigh(n),KeepNAtNeigh(n,5),
     3         KeepAtNeigh(n,5),KeepKiXNeigh(n,5),KeepNH(n),
     4         KeepNAtH(n,4),KeepAtH(n,4),KeepKiXH(n,4),
     5         KeepNAtAnchor(n),KeepAtAnchor(n),KeepKiXAnchor(n),
     6         KeepDistH(n),KeepAngleH(n),KeepADPExtFac(n))
      if(NKeep.gt.0) then
        do i=1,NKeep
          KeepType(i)=KeepTypeO(i)
          k=KeepTypeO(i)/10
          KeepNAtCentr(i)=KeepNAtCentrO(i)
          if(k.eq.IdKeepHydro.or.k.eq.IdKeepADP) then
            KeepAtCentr(i)=KeepAtCentrO(i)
            if(k.eq.IdKeepHydro) then
              KeepKiXCentr(i)=KeepKiXCentrO(i)
              KeepNNeigh(i)=KeepNNeighO(i)
              do j=1,KeepNNeighO(i)
                KeepNAtNeigh(i,j)=KeepNAtNeighO(i,j)
                KeepAtNeigh(i,j)=KeepAtNeighO(i,j)
                KeepKiXNeigh(i,j)=KeepKiXNeighO(i,j)
              enddo
              KeepNAtAnchor(i)=KeepNAtAnchorO(i)
              KeepAtAnchor(i)=KeepAtAnchorO(i)
              KeepKiXAnchor(i)=KeepKiXAnchorO(i)
              KeepDistH(i)=KeepDistHO(i)
              KeepAngleH(i)=KeepAngleHO(i)
            else
              KeepADPExtFac(i)=KeepADPExtFacO(i)
            endif
            KeepNH(i)=KeepNHO(i)
            do j=1,KeepNHO(i)
              KeepNAtH(i,j)=KeepNAtHO(i,j)
              KeepAtH(i,j)=KeepAtHO(i,j)
              if(k.eq.IdKeepHydro) KeepKiXH(i,j)=KeepKiXHO(i,j)
            enddo
          else
            KeepN(i)=KeepNO(i)
            do j=1,KeepN(i)
              KeepNAt(i,j)=KeepNAtO(i,j)
              KeepAt(i,j)=KeepAtO(i,j)
              KeepKiX(i,j)=KeepKiXO(i,j)
            enddo
          endif
        enddo
        if(allocated(KeepType))
     1    deallocate(KeepTypeO,KeepNO,KeepNAtO,KeepAtO,KeepKiXO,
     2               KeepNAtCentrO,KeepAtCentrO,KeepKiXCentrO,
     3               KeepNNeighO,KeepNAtNeighO,KeepAtNeighO,
     4               KeepKiXNeighO,KeepNHO,KeepNAtHO,KeepAtHO,KeepKiXHO,
     5               KeepNAtAnchorO,KeepAtAnchorO,KeepKiXAnchorO,
     6               KeepDistHO,KeepAngleHO,KeepADPExtFacO)
      endif
      NKeepMax=n
      NKeepAtMax=m
      return
      end
