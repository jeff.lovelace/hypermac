      subroutine ReallocFormF(NAtFormulaNew,NPhaseNew,NDatBlockNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real, allocatable :: AtWeightO(:,:),AtRadiusO(:,:),AtNumO(:,:),
     1  AtMultO(:,:),FFBasicO(:,:,:),FFCoreO(:,:,:),FFCoreDO(:,:,:),
     2  FFValO(:,:,:),FFValDO(:,:,:),FFnO(:,:),FFniO(:,:),FFMagO(:,:,:),
     3  FFElO(:,:,:),FFraO(:,:,:),FFiaO(:,:,:), ZSTOAO(:,:,:,:,:),
     4  CSTOAO(:,:,:,:,:),ZSlaterO(:,:,:),HZSlaterO(:,:),FFaO(:,:,:,:),
     5  FFaeO(:,:,:,:),fxO(:),fmO(:),TypicalDistO(:,:,:),
     6  DTypicalDistO(:,:)
      integer, allocatable :: PopCoreO(:,:,:),PopValO(:,:,:),
     1                        HNSlaterO(:,:),NSlaterO(:,:,:),
     2                        AtColorO(:,:),TypeCoreO(:,:),
     3                        TypeValO(:,:),AtValO(:,:),
     4                        NSTOAO(:,:,:,:,:),NCoefSTOAO(:,:,:,:),
     5                        TypeFFMagO(:,:)
      character*256, allocatable :: CoreValSourceO(:,:)
      character*7, allocatable :: AtTypeFullO(:,:),AtTypeMagO(:,:),
     1                            AtTypeMagJO(:,:)
      character*2, allocatable :: AtTypeO(:,:)
      if(Allocated(AtType)) then
        NAtFormulaOld=ubound(AtType,1)
        NPhaseOld=ubound(AtType,2)
        NDatBlockOld=ubound(FFra,3)
      else
        NAtFormulaOld=0
        NPhaseOld=0
        NDatBlockOld=0
      endif
      if(NAtFormulaOld.ge.NAtFormulaNew.and.NPhaseOld.ge.NPhaseNew.and.
     1   NDatBlockOld.ge.NDatBlockNew) go to 9999
      if(NAtFormulaOld.le.0) go to 1800
      n=MaxNAtFormula
      m=121
      i=NDatBlock
      mm=NPhaseOld
      allocate(AtTypeO(n,mm),AtTypeFullO(n,mm),
     1         AtTypeMagO(n,mm),AtTypeMagJO(n,mm),AtWeightO(n,mm),
     2         AtRadiusO(n,mm),AtColorO(n,mm),
     3         AtNumO(n,mm),AtValO(n,mm),AtMultO(n,mm),
     4         FFBasicO(m,n,mm),
     5         FFCoreO(m,n,mm),FFCoreDO(m,n,mm),
     6         FFValO(m,n,mm),FFValDO(m,n,mm),
     7         FFraO(n,mm,i),FFiaO(n,mm,i),
     8         FFnO(n,mm),FFniO(n,mm),FFMagO(7,n,mm),
     9         TypeFFMagO(n,mm),
     a         FFaO(4,m,n,mm),FFaeO(4,m,n,mm),fxO(n),fmO(n),
     1         FFElO(m,n,mm),TypicalDistO(n,n,mm))
      allocate(TypeCoreO(n,mm),TypeValO(n,mm),
     1         PopCoreO(28,n,mm),PopValO(28,n,mm),
     2         ZSlaterO(8,n,mm),NSlaterO(8,n,mm),
     3         HNSlaterO(n,mm),HZSlaterO(n,mm),
     4         ZSTOAO(7,7,40,n,mm),CSTOAO(7,7,40,n,mm),
     5         NSTOAO(7,7,40,n,mm),NCoefSTOAO(7,7,n,mm),
     6         CoreValSourceO(n,mm))
      do i=1,NPhaseOld
        do j=1,min(NAtFormula(i),NAtFormulaOld)
          AtTypeO    (j,i)=AtType    (j,i)
          AtTypeFullO(j,i)=AtTypeFull(j,i)
          AtTypeMagO (j,i)=AtTypeMag (j,i)
          AtTypeMagJO(j,i)=AtTypeMagJ(j,i)
          AtWeightO  (j,i)=AtWeight  (j,i)
          AtRadiusO  (j,i)=AtRadius  (j,i)
          AtColorO   (j,i)=AtColor   (j,i)
          AtNumO     (j,i)=AtNum     (j,i)
          AtValO     (j,i)=AtVal     (j,i)
          AtMultO    (j,i)=AtMult    (j,i)
          FFnO       (j,i)=FFn       (j,i)
          FFniO      (j,i)=FFni      (j,i)
          do k=1,iabs(FFType(i))
            FFBasicO(k,j,i)=FFBasic(k,j,i)
            if(ChargeDensities) then
              FFCoreO (k,j,i)=FFCore (k,j,i)
              FFCoreDO(k,j,i)=FFCoreD(k,j,i)
              FFValO (k,j,i)=FFVal (k,j,i)
              FFValDO(k,j,i)=FFValD(k,j,i)
            endif
          enddo
          do k=1,62
            FFElO(k,j,i)=FFEl(k,j,i)
          enddo
          if(AtTypeMag(j,i).ne.' ') then
            do k=1,7
              FFMagO(k,j,i)=FFMag(k,j,i)
            enddo
            TypeFFMagO(j,i)=TypeFFMag(j,i)
          endif
          do k=1,NDatBlockOld
            FFraO(j,i,k)=FFra(j,i,k)
            FFiaO(j,i,k)=FFia(j,i,k)
          enddo
          do k=1,min(NAtFormula(i),NAtFormulaOld)
            TypicalDistO(j,k,i)=TypicalDist(j,k,i)
          enddo
          CoreValSourceO(j,i)=CoreValSource(j,i)
        enddo
      enddo
      if(ChargeDensities) then
        do i=1,NPhaseOld
          do j=1,min(NAtFormula(i),NAtFormulaOld)
            TypeCoreO (j,i)=TypeCore (j,i)
            TypeValO  (j,i)=TypeVal  (j,i)
            HNSlaterO (j,i)=HNSlater (j,i)
            HZSlaterO(j,i)=HZSlater(j,i)
            do k=1,28
              PopCoreO(k,j,i)=PopCore(k,j,i)
              PopValO (k,j,i)=PopVal (k,j,i)
            enddo
            do k=1,8
              ZSlaterO(k,j,i)=ZSlater(k,j,i)
              NSlaterO (k,j,i)=NSlater (k,j,i)
            enddo
            do i1=1,7
              do i2=1,7
                NCoefSTOAO(i1,i2,j,i)=NCoefSTOA(i1,i2,j,i)
                do k=1,40
                  ZSTOAO(i1,i2,k,j,i)=ZSTOA(i1,i2,k,j,i)
                  CSTOAO(i1,i2,k,j,i)=CSTOA(i1,i2,k,j,i)
                  NSTOAO(i1,i2,k,j,i)=NSTOA(i1,i2,k,j,i)
                enddo
              enddo
            enddo
          enddo
        enddo
      endif
1800  if(allocated(AtType))
     1  deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2             AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3             FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4             TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
      if(allocated(TypeCore))
     1  deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2             HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3             CoreValSource)
2000  n=max(NAtFormulaNew,NAtFormulaOld)
      mm=max(NPhaseNew,NPhaseOld)
      m=121
      i=max(NDatBlockNew,NDatBlockOld)
      allocate(AtType(n,mm),AtTypeFull(n,mm),
     1         AtTypeMag(n,mm),AtTypeMagJ(n,mm),AtWeight(n,mm),
     2         AtRadius(n,mm),AtColor(n,mm),
     3         AtNum(n,mm),AtVal(n,mm),AtMult(n,mm),
     4         FFBasic(m,n,mm),
     5         FFCore(m,n,mm),FFCoreD(m,n,mm),
     6         FFVal(m,n,mm),FFValD(m,n,mm),
     7         FFra(n,mm,i),FFia(n,mm,i),
     8         FFn(n,mm),FFni(n,mm),FFMag(7,n,mm),TypeFFMag(n,mm),
     9         FFa(4,m,n,mm),FFae(4,m,n,mm),fx(n),fm(n),
     a         FFEl(m,n,mm),TypicalDist(n,n,mm))
      allocate(TypeCore(n,mm),TypeVal(n,mm),
     1         PopCore(28,n,mm),PopVal(28,n,mm),
     2         ZSlater(8,n,mm),NSlater(8,n,mm),
     3         HNSlater(n,mm),HZSlater(n,mm),
     4         ZSTOA(7,7,40,n,mm),CSTOA(7,7,40,n,mm),
     5         NSTOA(7,7,40,n,mm),NCoefSTOA(7,7,n,mm),
     6         CoreValSource(n,mm))
      do i=1,min(NPhaseOld,NPhaseNew)
        do j=1,min(NAtFormula(i),NAtFormulaOld)
          AtType    (j,i)=AtTypeO    (j,i)
          AtTypeFull(j,i)=AtTypeFullO(j,i)
          AtTypeMag (j,i)=AtTypeMagO (j,i)
          AtTypeMagJ(j,i)=AtTypeMagJO(j,i)
          AtWeight  (j,i)=AtWeightO  (j,i)
          AtRadius  (j,i)=AtRadiusO  (j,i)
          AtColor   (j,i)=AtColorO   (j,i)
          AtNum     (j,i)=AtNumO     (j,i)
          AtVal     (j,i)=AtValO     (j,i)
          AtMult    (j,i)=AtMultO    (j,i)
          FFn       (j,i)=FFnO       (j,i)
          FFni      (j,i)=FFniO      (j,i)
          do k=1,iabs(FFType(i))
            FFBasic(k,j,i)=FFBasicO(k,j,i)
            if(ChargeDensities) then
              FFCore(k,j,i) =FFCoreO (k,j,i)
              FFCoreD(k,j,i)=FFCoreDO(k,j,i)
              FFVal  (k,j,i)=FFValO  (k,j,i)
              FFValD (k,j,i)=FFValDO (k,j,i)
              FFVal  (k,j,i)=FFValO  (k,j,i)
            endif
          enddo
          do k=1,62
            FFEl(k,j,i)=FFElO(k,j,i)
          enddo
          if(AtTypeMagO(j,i).ne.' ') then
            do k=1,7
              FFMag(k,j,i)=FFMagO(k,j,i)
            enddo
            TypeFFMag(j,i)=TypeFFMagO(j,i)
          endif
          do k=1,NDatBlockOld
            FFra(j,i,k)=FFraO(j,i,k)
            FFia(j,i,k)=FFiaO(j,i,k)
          enddo
          do k=1,min(NAtFormula(i),NAtFormulaOld)
            TypicalDist(j,k,i)=TypicalDistO(j,k,i)
          enddo
          CoreValSource(j,i)=CoreValSourceO(j,i)
        enddo
      enddo
      if(ChargeDensities) then
        do i=1,min(NPhaseOld,NPhaseNew)
          do j=1,min(NAtFormula(i),NAtFormulaOld)
            TypeCore (j,i)=TypeCoreO (j,i)
            TypeVal  (j,i)=TypeValO  (j,i)
            HNSlater (j,i)=HNSlaterO (j,i)
            HZSlater(j,i)=HZSlaterO(j,i)
            do k=1,28
              PopCore(k,j,i)=PopCoreO(k,j,i)
              PopVal (k,j,i)=PopValO (k,j,i)
            enddo
            do k=1,8
              ZSlater(k,j,i)=ZSlaterO(k,j,i)
              NSlater (k,j,i)=NSlaterO (k,j,i)
            enddo
            do i1=1,7
              do i2=1,7
                NCoefSTOA(i1,i2,j,i)=NCoefSTOAO(i1,i2,j,i)
                do k=1,40
                  ZSTOA(i1,i2,k,j,i)=ZSTOAO(i1,i2,k,j,i)
                  CSTOA(i1,i2,k,j,i)=CSTOAO(i1,i2,k,j,i)
                  NSTOA(i1,i2,k,j,i)=NSTOAO(i1,i2,k,j,i)
                enddo
              enddo
            enddo
          enddo
        enddo
      endif
      if(allocated(AtTypeO))
     1  deallocate(AtTypeO,AtTypeFullO,AtTypeMagO,AtTypeMagJO,AtWeightO,
     2             AtRadiusO,AtColorO,AtNumO,AtValO,AtMultO,FFBasicO,
     3             FFCoreO,FFCoreDO,FFValO,FFValDO,FFraO,FFiaO,FFnO,
     4             FFniO,FFMagO,TypeFFMagO,FFaO,FFaeO,fxO,fmO,FFElO,
     5             TypicalDistO)
      if(allocated(TypeCoreO))
     1  deallocate(TypeCoreO,TypeValO,PopCoreO,PopValO,ZSlaterO,
     2             NSlaterO,HNSlaterO,HZSlaterO,ZSTOAO,CSTOAO,NSTOAO,
     3             NCoefSTOAO,CoreValSourceO)
9999  return
      end
