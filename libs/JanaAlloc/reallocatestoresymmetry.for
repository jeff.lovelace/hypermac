      subroutine ReallocateStoreSymmetry(IStore,NSymmP,NLattVecP,NDimP,
     1                                   NExtRefCondP)
      use Basic_mod
      use MatStore_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(IStore.le.NStore.and.NDimP.le.NDimStoreMax.and.
     1   NLattVecP.le.NLattVecStoreMax.and.
     2     NSymmP.le.NSymmStoreMax.and.
     3     NExtRefCond.le.NExtRefCondStoreMax) go to 9000
      if(NStore.gt.0) then
        ns=NSymmStoreMax
        nvt=NLattVecStoreMax
        m=NExtRefCondStoreMax
        n=NStore
        allocate(NSymmStoreO(n),NLattVecStoreO(n),
     1           NDimStoreO(n),NExtRefCondStoreO(n),
     2           RM6StoreO(36,ns,n),S6StoreO(6,ns,n),
     3           RMStoreO(9,ns,n),ProjStoreO(6,ns,n),
     3           VT6StoreO(6,nvt,n),
     4           ExtRefGroupStoreO(36,m,n),
     5           ExtRefCondStoreO(7,m,n))
        do i=1,NStore
          do is=1,NSymmStore(i)
            call CopyMat(RM6Store(1,is,i),RM6StoreO(1,is,i),
     1                   NDimStore(i))
            call CopyMat(RMStore(1,is,i),RMStoreO(1,is,i),3)
            call CopyVek( S6Store(1,is,i), S6StoreO(1,is,i),
     1                   NDimStore(i))
            call CopyVek(ProjStore(1,is,i),ProjStoreO(1,is,i),
     1                   NDimStore(i))
          enddo
          do ivt=1,NLattVecStore(i)
            call CopyVek(VT6Store(1,ivt,i),VT6StoreO(1,ivt,i),
     1                   NDimStore(i))
          enddo
          do iext=1,NExtRefCondStore(i)
            call CopyMat(ExtRefGroupStore (1,iext,i),
     1                   ExtRefGroupStoreO(1,iext,i),NDimStore(i))
            call CopyVek(ExtRefCondStore (1,iext,i),
     1                   ExtRefCondStoreO(1,iext,i),NDimStore(i)+1)
          enddo
          NSymmStoreO(i)=NSymmStore(i)
          NLattVecStoreO(i)=NLattVecStore(i)
          NDimStoreO(i)=NDimStore(i)
          NExtRefCondStoreO(i)=NExtRefCondStore(i)
        enddo
      endif
      if(allocated(NSymmStore))
     1  deallocate(NSymmStore,NLattVecStore,NDimStore,NExtRefCondStore,
     2             RM6Store,RMStore,S6Store,ProjStore,VT6Store,
     3             ExtRefGroupStore,ExtRefCondStore)
      ns=max(NSymmStoreMax,NSymmP)
      nvt=max(NLattVecStoreMax,NLattVecP)
      nd=max(NDimStoreMax,NDimP)
      n=max(NStore,IStore)
      m=max(NExtRefCondStoreMax,NExtRefCond)
      allocate(NSymmStore(n),NLattVecStore(n),
     1         NDimStore(n),NExtRefCondStore(n),
     2         RM6Store(36,ns,n),RMStore(9,ns,n),S6Store(6,ns,n),
     3         ProjStore(6,ns,n),
     4         VT6Store(6,nvt,n),
     5         ExtRefGroupStore(36,m,n),
     6         ExtRefCondStore(7,m,n))
      if(NStore.gt.0) then
        do i=1,NStore
          if(i.eq.IStore) cycle
          do is=1,NSymmStoreO(i)
            call CopyMat(RM6StoreO(1,is,i),RM6Store(1,is,i),
     1                   NDimStoreO(i))
            call CopyMat(RMStoreO(1,is,i),RMStore(1,is,i),3)
            call CopyVek( S6StoreO(1,is,i), S6Store(1,is,i),
     1                   NDimStoreO(i))
            call CopyVek(ProjStoreO(1,is,i),ProjStore(1,is,i),
     1                   NDimStoreO(i))
          enddo
          do ivt=1,NLattVecStoreO(i)
            call CopyVek(VT6StoreO(1,ivt,i),VT6Store(1,ivt,i),
     1                   NDimStoreO(i))
          enddo
          do iext=1,NExtRefCondStoreO(i)
            call CopyMat(ExtRefGroupStoreO(1,iext,i),
     1                   ExtRefGroupStore (1,iext,i),NDimStoreO(i))
            call CopyVek(ExtRefCondStoreO(1,iext,i),
     1                   ExtRefCondStore (1,iext,i),NDimStoreO(i)+1)
          enddo
          NSymmStore(i)=NSymmStoreO(i)
          NLattVecStore(i)=NLattVecStoreO(i)
          NDimStore(i)=NDimStoreO(i)
          NExtRefCondStore(i)=NExtRefCondStoreO(i)
        enddo
        deallocate(NSymmStoreO,NLattVecStoreO,NDimStoreO,
     1             NExtRefCondStoreO,
     2             RM6StoreO,RMStoreO,S6StoreO,VT6StoreO,ProjStoreO,
     3             ExtRefGroupStoreO,ExtRefCondStoreO)
      endif
9000  NSymmStore(IStore)=NSymmP
      NLattVecStore(IStore)=NLattVecP
      NDimStore(IStore)=NDimP
      NSymmStoreMax=ns
      NLattVecStoreMax=nvt
      NDimStoreMax=nd
      NExtRefCondStoreMax=m
      NStore=max(NStore,IStore)
      return
      end
