      subroutine ReallocateWdFAr(NRefWdFNew)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: FoArO(:),FcArO(:),FsArO(:),WdFArO(:),
     1                     IcArO(:)
      integer, allocatable :: HCondWdFArO(:),FoInd2AveO(:)
      if(NRefWdFNew.le.NRefWdFMax) go to 9999
      if(NRefWdFMax.gt.0) then
        n=NRefWdFMax
        allocate(FoArO(n),FcArO(n),FsArO(n),WdFArO(n),HCondWdFArO(n),
     1           FoInd2AveO(n),IcArO(n))
        call CopyVek (FoAr,FoArO,NRefWdF)
        call CopyVek (FcAr,FcArO,NRefWdF)
        call CopyVek (FsAr,FsArO,NRefWdF)
        call CopyVek (IcAr,IcArO,NRefWdF)
        call CopyVek (WdFAr,WdFArO,NRefWdF)
        call CopyVekI(HCondWdFAr,HCondWdFArO,NRefWdF)
        call CopyVekI(FoInd2Ave,FoInd2AveO,NRefWdF)
      endif
      if(allocated(FoAr)) deallocate(FoAr,FcAr,FsAr,WdFAr,HCondWdFAr,
     1                               FoInd2Ave,IcAr)
      n=NRefWdFNew
      allocate(FoAr(n),FcAr(n),FsAr(n),WdFAr(n),HCondWdFAr(n),
     1         FoInd2Ave(n),IcAr(n))
      if(NRefWdFMax.gt.0) then
        call CopyVek (FoArO,FoAr,NRefWdF)
        call CopyVek (FcArO,FcAr,NRefWdF)
        call CopyVek (IcArO,IcAr,NRefWdF)
        call CopyVek (WdFArO,WdFAr,NRefWdF)
        call CopyVekI(HCondWdFArO,HCondWdFAr,NRefWdF)
        call CopyVekI(HCondWdFArO,HCondWdFAr,NRefWdF)
        call CopyVekI(FoInd2AveO,FoInd2Ave,NRefWdF)
        deallocate(FoArO,FcArO,FsArO,WdFArO,HCondWdFArO,FoInd2AveO,
     1             IcArO)
      endif
      NRefWdFMax=NRefWdFNew
9999  return
      end
