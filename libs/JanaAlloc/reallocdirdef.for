      subroutine ReallocDirDef(n)
      use Dist_mod
      real DirRefO(:,:)
      allocatable DirRefO
      if(NDirRef.gt.0) then
        allocate(DirRefO(3,NDirRef))
        call CopyVek(DirRef,DirRefO,3*NDirRef)
      endif
      if(allocated(DirRef)) deallocate(DirRef)
      NDirRefMax=NDirRefMax+n
      allocate(DirRef(3,NDirRefMax))
      if(NDirRef.gt.0) then
        call CopyVek(DirRefO,DirRef,3*NDirRef)
        deallocate(DirRefO)
      endif
      return
      end
