      subroutine ReallocateTypicalDist(NTypicalDistNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension DTypicalDistO(:,:),NTypicalDistO(:)
      character*2 AtTypicalDistO(:,:,:)
      allocatable DTypicalDistO,NTypicalDistO,AtTypicalDistO
      if(NTypicalDistNew.le.NTypicalDistMax) go to 9999
      if(NTypicalDistMax.gt.0) then
        n=NTypicalDistNew
        allocate(DTypicalDistO(n,NPhase),NTypicalDistO(NPhase),
     1           AtTypicalDistO(2,n,NPhase))
        do i=1,NPhase
          NTypicalDistO(i)=NTypicalDist(i)
          do j=1,NTypicalDist(i)
            DTypicalDistO(j,i)=DTypicalDist(j,i)
            do k=1,2
              AtTypicalDistO(k,j,NPhase)=AtTypicalDist(k,j,NPhase)
            enddo
          enddo
        enddo
      endif
      if(allocated(NTypicalDist))
     1  deallocate(DTypicalDist,NTypicalDist,AtTypicalDist)
      n=NTypicalDistNew
      allocate(DTypicalDist(n,NPhase),NTypicalDist(NPhase),
     1         AtTypicalDist(2,n,NPhase))
      if(NTypicalDistMax.gt.0) then
        do i=1,NPhase
          NTypicalDist(i)=NTypicalDistO(i)
          do j=1,NTypicalDist(i)
            DTypicalDist(j,i)=DTypicalDistO(j,i)
            do k=1,2
              AtTypicalDist(k,j,NPhase)=AtTypicalDistO(k,j,NPhase)
            enddo
          enddo
        enddo
        if(allocated(NTypicalDistO))
     1    deallocate(DTypicalDistO,NTypicalDistO,AtTypicalDistO)
      endif
      NTypicalDistMax=NTypicalDistNew
9999  return
      end
