      subroutine ReallocSymm(NDimNew,NSymmNew,NLattVecNew,NCompNew,
     1                       NPhaseNew,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: rm6o(:,:,:,:),rmo(:,:,:,:),s6o(:,:,:,:),
     1                     vt6o(:,:,:,:),rmago(:,:,:,:),zmago(:,:,:)
      integer, allocatable :: ISwSymmo(:,:,:),InvMagO(:),CenterMagO(:)
      character*30, allocatable :: symmco(:,:,:,:)
      logical, allocatable :: BratSymmO(:,:)
      NDimOld=ubound(s6,1)
      NSymmOld=ubound(s6,2)
      NCompOld=ubound(s6,3)
      NPhaseOld=ubound(s6,4)
      NLattVecOld=ubound(vt6,2)
      if(NDimOld.ge.NDimNew.and.NSymmOld.ge.NSymmNew.and.
     1   NCompOld.ge.NCompNew.and.NPhaseOld.ge.NPhaseNew.and.
     2   NLattVecOld.ge.NLattVecNew) go to 9999
      allocate(   rmo(9         ,NSymmOld,NCompOld,NPhaseOld),
     1           rm6o(NDimOld**2,NSymmOld,NCompOld,NPhaseOld),
     2            s6o(NDimOld   ,NSymmOld,NCompOld,NPhaseOld),
     3         symmco(NDimOld   ,NSymmOld,NCompOld,NPhaseOld),
     4           vt6o(NDimOld   ,NLattVecOld,NCompOld,NPhaseOld),
     5                  ISwSymmo(NSymmOld,NCompOld,NPhaseOld),
     6         BratSymmO(NSymmOld,NPhaseOld),
     7         rmago(9,NSymmOld,NCompOld,NPhaseOld),
     8         zmago(  NSymmOld,NCompOld,NPhaseOld),
     9         InvMagO(NPhaseOld),CenterMagO(NPhaseOld))
      do i=1,NPhaseOld
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,min(NSymm(i),NSymmOld)
            call CopyVek(rm (1,k,j,i),rmo (1,k,j,i),9)
            call CopyVek(rm6(1,k,j,i),rm6o(1,k,j,i),NDimOld**2)
            call CopyVek(s6 (1,k,j,i),s6o (1,k,j,i),NDimOld)
            do l=1,NDimOld
              symmco(l,k,j,i)=symmc(l,k,j,i)
            enddo
            ISwSymmO(k,j,i)=ISwSymm(k,j,i)
            call CopyVek(rmag(1,k,j,i),rmago(1,k,j,i),9)
            zmago(k,j,i)=zmag(k,j,i)
            if(j.eq.1) BratSymmO(k,i)=BratSymm(k,i)
          enddo
          do k=1,min(NLattVec(i),NLattVecOld)
            call CopyVek(vt6(1,k,j,i),vt6o(1,k,j,i),NDimOld)
          enddo
        enddo
      enddo
      InvMagO(1:NPhaseOld)=InvMag(1:NPhaseOld)
      CenterMagO(1:NPhaseOld)=CenterMag(1:NPhaseOld)
      deallocate(rm6,rm,s6,symmc,vt6,ISwSymm,BratSymm,rmag,zmag,InvMag,
     1           CenterMag)
      ns=max(NSymmNew,NSymmOld)
      nc=max(NCompNew,NCompOld)
      nvt=max(NLattVecNew,NLattVecOld)
      nd=max(NDimNew,NDimOld)
      nph=max(NPhaseNew,NPhaseOld)
      allocate(rm(9,ns,nc,nph),rm6(nd**2,ns,nc,nph),s6(nd,ns,nc,nph),
     1         symmc(nd,ns,nc,nph),vt6(nd,nvt,nc,nph),
     2         ISwSymm(ns,nc,nph),BratSymm(ns,nph),
     3         rmag(9,ns,nc,nph),zmag(ns,nc,nph),InvMag(nph),
     4         CenterMag(nph))
      do i=1,min(NPhaseOld,NPhaseNew)
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,min(NSymm(i),NSymmOld)
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            call SetStringArrayTo(symmc(1,k,j,i),nd,' ')
            call CopyVek(rmo (1,k,j,i),rm (1,k,j,i),9)
            call CopyVek(rm6o(1,k,j,i),rm6(1,k,j,i),NDimOld**2)
            call CopyVek(s6o (1,k,j,i),s6 (1,k,j,i),NDimOld)
            do l=1,NDimOld
              symmc(l,k,j,i)=symmco(l,k,j,i)
            enddo
            ISwSymm(k,j,i)=ISwSymmO(k,j,i)
            call CopyVek(rmago(1,k,j,i),rmag(1,k,j,i),9)
            zmag(k,j,i)=zmago(k,j,i)
            if(j.eq.1) BratSymm(k,i)=BratSymmO(k,i)
          enddo
          do k=NSymmOld+1,NSymmNew
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            call SetStringArrayTo(symmc(1,k,j,i),nd,' ')
            if(j.eq.1) BratSymm(k,i)=.true.
          enddo
          do k=1,min(NLattVec(i),NLattVecOld)
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
            call CopyVek(vt6o(1,k,j,i),vt6(1,k,j,i),NDimOld)
          enddo
          do k=NLattVecOld+1,NLattVecNew
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
          enddo
        enddo
        InvMag(i)=InvMagO(i)
        CenterMag(i)=CenterMagO(i)
      enddo
      deallocate(rm6o,rmo,s6o,symmco,vt6o,IswSymmo,BratSymmO,rmago,
     1           zmago,InvMagO,CenterMagO)
      do i=min(NPhaseOld,NPhaseNew)+1,NPhaseNew
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,NSymmNew
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            ISwSymm(k,j,i)=k
            call SetRealArrayTo(rmag(1,k,j,i),9,0.)
            zmag(k,j,i)=1.
            call SetStringArrayTo(symmc(1,k,1,i),nd,' ')
            if(j.eq.1) BratSymm(k,i)=.true.
          enddo
          do k=1,NLattVecNew
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
          enddo
        enddo
        InvMag(i)=0
        CenterMag(i)=0
      enddo
9999  return
      end
