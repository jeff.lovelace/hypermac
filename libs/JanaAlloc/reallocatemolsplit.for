      subroutine ReallocateMolSplit(NPlus,MMolSplitMaxNew)
      use Molec_mod
      integer, allocatable :: MMolSplitO(:),IMolSplitO(:,:)
      character*80, allocatable :: MolSplitO(:,:)
      if(NPlus.le.0.and.MMolSplitMaxNew.le.MMolSplitMax) go to 9999
      n=NMolSplit
      m=MMolSplitMax
      if(n.gt.0) then
        allocate(MMolSplitO(n),IMolSplitO(m,n),MolSplitO(m,n))
        MMolSplitO(1:n)=MMolSplit(1:n)
        do i=1,n
          do j=1,MMolSplit(i)
            IMolSplitO(j,i)=IMolSplit(j,i)
            MolSplitO(j,i)=MolSplit(j,i)
          enddo
        enddo
      endif
      if(allocated(MMolSplit)) deallocate(MMolSplit,IMolSplit,MolSplit)
      n=NMolSplitMax+NPlus
      m=MMolSplitMaxNew
      allocate(MMolSplit(n),IMolSplit(m,n),MolSplit(m,n))
      if(NMolSplit.gt.0) then
        MMolSplit(1:NMolSplit)=MMolSplitO(1:NMolSplit)
        do i=1,NMolSplit
          do j=1,MMolSplitO(i)
            IMolSplit(j,i)=IMolSplitO(j,i)
            MolSplit(j,i)=MolSplitO(j,i)
          enddo
        enddo
      endif
      NMolSplitMax=n
      MMolSplitMax=m
      if(allocated(MMolSplitO))
     1  deallocate(MMolSplitO,IMolSplitO,MolSplitO)
9999  return
      end
