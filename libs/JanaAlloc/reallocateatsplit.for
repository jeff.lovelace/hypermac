      subroutine ReallocateAtSplit(NPlus,MAtSplitMaxNew)
      use Atoms_mod
      integer, allocatable :: MAtSplitO(:),IAtSplitO(:,:)
      character*80, allocatable :: AtSplitO(:,:)
      if(NPlus.le.0.and.MAtSplitMaxNew.le.MAtSplitMax) go to 9999
      n=NAtSplit
      m=MAtSplitMax
      if(n.gt.0) then
        allocate(MAtSplitO(n),IAtSplitO(m,n),AtSplitO(m,n))
        MAtSplitO(1:n)=MAtSplit(1:n)
        do i=1,n
          do j=1,MAtSplit(i)
            IAtSplitO(j,i)=IAtSplit(j,i)
            AtSplitO(j,i)=AtSplit(j,i)
          enddo
        enddo
      endif
      if(allocated(MAtSplit)) deallocate(MAtSplit,IAtSplit,AtSplit)
      n=NAtSplitMax+NPlus
      m=MAtSplitMaxNew
      allocate(MAtSplit(n),IAtSplit(m,n),AtSplit(m,n))
      if(NAtSplit.gt.0) then
        MAtSplit(1:NAtSplit)=MAtSplitO(1:NAtSplit)
        do i=1,NAtSplit
          do j=1,MAtSplitO(i)
            IAtSplit(j,i)=IAtSplitO(j,i)
            AtSplit(j,i)=AtSplitO(j,i)
          enddo
        enddo
      endif
      NAtSplitMax=n
      MAtSplitMax=m
      if(allocated(MAtSplitO))
     1  deallocate(MAtSplitO,IAtSplitO,AtSplitO)
9999  return
      end
