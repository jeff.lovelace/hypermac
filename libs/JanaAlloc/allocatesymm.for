      subroutine AllocateSymm(NSymmNew,NLattVecNew,NCompNew,NPhaseNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(rm)) deallocate(rm,rm6,s6,symmc,vt6,ISwSymm,
     1                             BratSymm,rmag,zmag,InvMag,CenterMag)
      allocate(rm                 (9,NSymmNew,NCompNew,NPhaseNew),
     1         rm6  (NDim(KPhase)**2,NSymmNew,NCompNew,NPhaseNew),
     2         s6   (NDim (KPhase)  ,NSymmNew,NCompNew,NPhaseNew),
     3         symmc(NDim (KPhase),  NSymmNew,NCompNew,NPhaseNew),
     4         ISwSymm              (NSymmNew,NCompNew,NPhaseNew),
     5         RMag               (9,NSymmNew,NCompNew,NPhaseNew),
     6         ZMag                 (NSymmNew,NCompNew,NPhaseNew),
     7         BratSymm             (NSymmNew,         NPhaseNew),
     8         vt6  (NDim(KPhase),NLattVecNew,NCompNew,NPhaseNew),
     9         InvMag(NPhaseNew),CenterMag(NPhaseNew))
      do i=1,NSymmNew
        do j=1,NCompNew
          do k=1,NPhaseNew
            call UnitMat(rm  (1,i,j,k),3)
            call UnitMat(rmag(1,i,j,k),3)
            call UnitMat(rm6 (1,i,j,k),NDim(KPhase))
            call SetRealArrayTo(s6(1,i,j,k),NDim(KPhase),0.)
            call SetStringArrayTo(SymmC(1,i,j,k),NDim(KPhase),' ')
            zmag(i,j,k)=1.
            ISwSymm(i,j,k)=j
            BratSymm(i,k)=.true.
            ZMag(i,j,k)=1.
          enddo
        enddo
      enddo
      InvMag(1:NPhaseNew)=0
      CenterMag(1:NPhaseNew)=0
      do i=1,NLattVecNew
        do j=1,NCompNew
          do k=1,NPhaseNew
            call SetRealArrayTo(vt6(1,i,j,k),NDim(KPhase),0.)
          enddo
        enddo
      enddo
      return
      end
