      subroutine AllocateMolecules(n,m)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(MolName))
     1  deallocate(MolName,ISwMol,KSwMol,KModM,KFM,KTLS,
     2             StRefPoint,RefPoint,TypeModFunMol,
     3             LocMolSystType,LocMolSystSt,LocMolSystAx,
     4             LocMolSystX,
     5             RotSign,RotMol,RotIMol,TrMol,TrIMol,
     6             DRotF,DRotC,DRotP,RotB,DRotBF,DRotBC,DRotBP,
     7             PrvniKiMolekuly,DelkaKiMolekuly,KiMol,
     8             UsePGSyst,SmbPGMol,LocPGSystSt,LocPGSystAx,TrPG,
     9             TriPG,LocPGSystX,
     a             NPoint,KPoint,IPoint,RPoint,SPoint,TPoint,
     1             AiMol,SAiMol,Xm,AtTrans,Trans,STrans,Euler,SEuler,
     2             a0m,sa0m)
      mxm=n
      mxp=m
      mw=0
      mxdm=7
      nm=n*m
      mxpm=nm
      allocate(MolName(n),ISwMol(n),KSwMol(n),KModM(3,nm),KFM(3,nm),
     1         KTLS(n),StRefPoint(n),RefPoint(n),
     2         LocMolSystSt(2,2,nm),LocMolSystType(nm),
     3         LocMolSystAx(2,nm),LocMolSystX(3,2,2,nm),
     4         RotSign(nm),RotMol(9,nm),RotiMol(9,nm),TrMol(9,nm),
     5         TriMol(9,nm),TypeModFunMol(nm),
     6         DRotF(9,nm),DRotC(9,nm),DRotP(9,nm),RotB(36,nm),
     7         DRotBF(36,nm),DRotBC(36,nm),DRotBP(36,nm),
     8         PrvniKiMolekuly(nm),DelkaKiMolekuly(nm),KiMol(mxdm,nm),
     9         UsePGSyst(n),SmbPGMol(n),LocPGSystSt(2,n),LocPGSystAx(n),
     a         TrPG(9,n),TriPG(9,n),LocPGSystX(3,2,n),
     1         NPoint(n),KPoint(n),IPoint(mxpg,n),RPoint(9,mxpg,n),
     2         SPoint(3,mxpg,n),TPoint(36,mxpg,n),
     3         AiMol(nm),SAiMol(nm),Xm(3,n),AtTrans(nm),Trans(3,nm),
     4         STrans(3,nm),Euler(3,nm),SEuler(3,nm),a0m(nm),sa0m(nm))
      if(allocated(phfm)) deallocate(phfm,sphfm,OrthoX40Mol,
     1                               OrthoDeltaMol,OrthoEpsMol)
      if(MaxNDimI.gt.0) allocate(phfm(nm),sphfm(nm),OrthoX40Mol(nm),
     1                           OrthoDeltaMol(nm),OrthoEpsMol(nm))
      call DeallocateMoleculeParams
      return
      end
