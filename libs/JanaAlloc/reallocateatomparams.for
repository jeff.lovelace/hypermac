      subroutine ReallocateAtomParams(NAtLast,itfn,ifrn,kmodan,MagParN)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodan(*),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),
     1          KiP(:,:)
      allocatable px,py,spx,spy,KiP
      md=0
      mw=0
      do i=0,max(itfn,itfmax,2)
        kmod=max(kmodan(i+1),KModAMax(i+1))
        n=TRank(i)
        mw=max(mw,kmod)
        md=md+n*(2*kmod+1)
        if(i.eq.0.and.kmod.ne.0) md=md+1
        if(ifrn.ne.0) md=md+1
      enddo
      if(mw.gt.0) md=md+1
      kmod=max(MagParN,MagParMax)
      mw=max(mw,kmod-1)
      md=md+3*(2*kmod+1)
      if(ChargeDensities) md=md+68
      if(md.le.mxda.and.MxAtAll.le.NAtAllocMod) go to 9999
      do i=0,max(itfn,itfmax,2)
        n=TRank(i)
        KModP=KModAMax(i+1)
        if(i.eq.0) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( ax(1,ia), px(1,1,ia),m)
                call CopyVek( ay(1,ia), py(1,1,ia),m)
                call CopyVek(sax(1,ia),spx(1,1,ia),m)
                call CopyVek(say(1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(ax)) deallocate(ax,ay,sax,say)
            if(mw.gt.0) then
              allocate( ax(mw,MxAtAll), ay(mw,MxAtAll))
              ax=0.
              ay=0.
              allocate(sax(mw,MxAtAll),say(mw,MxAtAll))
              sax=0.
              say=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), ax(1,ia),m)
                call CopyVek( py(1,1,ia), ay(1,ia),m)
                call CopyVek(spx(1,1,ia),sax(1,ia),m)
                call CopyVek(spy(1,1,ia),say(1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.1) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( ux(1,1,ia), px(1,1,ia),m)
                call CopyVek( uy(1,1,ia), py(1,1,ia),m)
                call CopyVek(sux(1,1,ia),spx(1,1,ia),m)
                call CopyVek(suy(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(ux)) deallocate(ux,uy,sux,suy)
            if(mw.gt.0) then
              allocate(ux(n,mw,MxAtAll),uy(n,mw,MxAtAll))
              ux=0.
              uy=0.
              allocate(sux(n,mw,MxAtAll),suy(n,mw,MxAtAll))
              sux=0.
              suy=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), ux(1,1,ia),m)
                call CopyVek( py(1,1,ia), uy(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sux(1,1,ia),m)
                call CopyVek(spy(1,1,ia),suy(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.2) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( bx(1,1,ia), px(1,1,ia),m)
                call CopyVek( by(1,1,ia), py(1,1,ia),m)
                call CopyVek(sbx(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sby(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(bx)) deallocate(bx,by,sbx,sby)
            if(mw.gt.0) then
              allocate( bx(n,mw,MxAtAll), by(n,mw,MxAtAll))
              bx=0.
              by=0.
              allocate(sbx(n,mw,MxAtAll),sby(n,mw,MxAtAll))
              sbx=0.
              sby=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), bx(1,1,ia),m)
                call CopyVek( py(1,1,ia), by(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sbx(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sby(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.3) then
          if(.not.allocated(c3))
     1      allocate(c3(n,MxAtAll),sc3(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c3x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c3y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc3x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc3y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
            if(mw.gt.0) then
              allocate( c3x(n,mw,MxAtAll), c3y(n,mw,MxAtAll))
              c3x=0.
              c3y=0.
              allocate(sc3x(n,mw,MxAtAll),sc3y(n,mw,MxAtAll))
              sc3x=0.
              sc3y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c3x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c3y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc3x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc3y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.4) then
          if(.not.allocated(c4))
     1      allocate(c4(n,MxAtAll),sc4(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c4x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c4y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc4x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc4y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
            if(mw.gt.0) then
              allocate( c4x(n,mw,MxAtAll), c4y(n,mw,MxAtAll))
              c4x=0.
              c4y=0.
              allocate(sc4x(n,mw,MxAtAll),sc4y(n,mw,MxAtAll))
              sc4x=0.
              sc4y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c4x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c4y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc4x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc4y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.5) then
          if(.not.allocated(c5))
     1      allocate(c5(n,MxAtAll),sc5(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c5x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c5y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc5x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc5y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
            if(mw.gt.0) then
              allocate( c5x(n,mw,MxAtAll), c5y(n,mw,MxAtAll))
              c5x=0.
              c5y=0.
              allocate(sc5x(n,mw,MxAtAll),sc5y(n,mw,MxAtAll))
              sc5x=0.
              sc5y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c5x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c5y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc5x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc5y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.6) then
          if(.not.allocated(c6))
     1      allocate(c6(n,MxAtAll),sc6(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c6x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c6y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc6x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc6y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
            if(mw.gt.0) then
              allocate( c6x(n,mw,MxAtAll), c6y(n,mw,MxAtAll))
              c6x=0.
              c6y=0.
              allocate(sc6x(n,mw,MxAtAll),sc6y(n,mw,MxAtAll))
              sc6x=0.
              sc6y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c6x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c6y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc6x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc6y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        endif
      enddo
      if(MagParN.gt.MagParMax.or.MxAtAll.gt.NAtAllocMod) then
        n=3
        if(MagParMax.gt.1) then
          allocate( px(n,MagParMax-1,NAtLast),
     1              py(n,MagParMax-1,NAtLast),
     2             spx(n,MagParMax-1,NAtLast),
     3             spy(n,MagParMax-1,NAtLast))
          do ia=1,NAtLast
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            m=(MagPar(ia)-1)*n
            if(m.gt.0) then
              call CopyVek( smx(1,1,ia), px(1,1,ia),m)
              call CopyVek( smy(1,1,ia), py(1,1,ia),m)
              call CopyVek(ssmx(1,1,ia),spx(1,1,ia),m)
              call CopyVek(ssmy(1,1,ia),spy(1,1,ia),m)
            endif
          enddo
        endif
        if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
        allocate( smx(n,MagParN-1,MxAtAll),
     1            smy(n,MagParN-1,MxAtAll))
        allocate(ssmx(n,MagParN-1,MxAtAll),
     1           ssmy(n,MagParN-1,MxAtAll))
        if(MagParMax.gt.1) then
          do ia=1,NAtLast
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            m=(MagPar(ia)-1)*n
            if(m.gt.0) then
              call CopyVek( px(1,1,ia), smx(1,1,ia),m)
              call CopyVek( py(1,1,ia), smy(1,1,ia),m)
              call CopyVek(spx(1,1,ia),ssmx(1,1,ia),m)
              call CopyVek(spy(1,1,ia),ssmy(1,1,ia),m)
            endif
          enddo
          deallocate(px,py,spx,spy)
        endif
      endif
      allocate(KiP(mxda,NAtLast))
      do ia=1,NAtLast
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        call CopyVekI(KiA(1,ia),KiP(1,ia),mxda)
      enddo
      deallocate(KiA)
      allocate(KiA(md,MxAtAll))
      do ia=1,NAtLast
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        call CopyVekI(KiP(1,ia),KiA(1,ia),mxda)
        if(md.gt.mxda) call SetIntArrayTo(KiA(mxda+1,ia),md-mxda,0)
      enddo
      deallocate(KiP)
      mxda=md
      do i=1,7
        KModAMax(i)=max(KModAMax(i),KModAN(i))
      enddo
      MagParMax=max(MagParMax,MagParN)
      NAtAllocMod=MxAtAll
9999  return
      end
