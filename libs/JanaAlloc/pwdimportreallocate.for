      subroutine PwdImportReallocate(nalloc)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension XPwdP(:),YoPwdP(:),YsPwdP(:),YiPwdP(:)
      allocatable XPwdP,YoPwdP,YsPwdP,YiPwdP
      allocate(XPwdP(nalloc),YoPwdP(nalloc),YsPwdP(nalloc),
     1         YiPwdP(nalloc))
      call CopyVek(XPwd,XPwdP,NPnts)
      call CopyVek(YoPwd,YoPwdP,NPnts)
      call CopyVek(YsPwd,YsPwdP,NPnts)
      call CopyVek(YiPwd,YiPwdP,NPnts)
!      nallocp=nalloc
      nalloc=2*nalloc
      deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
      call CopyVek(XPwdP,XPwd,NPnts)
      call CopyVek(YoPwdP,YoPwd,NPnts)
      call CopyVek(YsPwdP,YsPwd,NPnts)
      call CopyVek(YiPwdP,YiPwd,NPnts)
      deallocate(XPwdP,YoPwdP,YsPwdP,YiPwdP)
      return
      end
