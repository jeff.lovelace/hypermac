      subroutine ReallocateED(KRefB)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer, allocatable :: NEDZoneO(:),EDIntStepsO(:),
     1                        EDThreadsO(:),EDTiltcorrO(:),
     2                        EDGeometryIEDTO(:)
      real, allocatable :: OrMatEDZoneO(:,:,:),GMaxEDZoneO(:),
     1                     SGMaxMEDZoneO(:),SGMaxREDZoneO(:),
     2                     CSGMaxREDZoneO(:),OmEDZoneO(:)
      if(KRefB.le.NRefBlockED) go to 9999
      if(NRefBlockED.gt.0) then
        allocate(NEDZoneO(NRefBlock),EDIntStepsO(NRefBlock),
     1           OrMatEDZoneO(3,3,NRefBlock),GMaxEDZoneO(NRefBlock),
     2           SGMaxMEDZoneO(NRefBlock),SGMaxREDZoneO(NRefBlock),
     3           CSGMaxREDZoneO(NRefBlock),OmEDZoneO(NRefBlock),
     4           EDThreadsO(NRefBlock),EDTiltcorrO(NRefBlock),
     5           EDGeometryIEDTO(NRefBlock))
        do i=1,NRefBlockED
          if(RadiationRefBlock(i).ne.ElectronRadiation) cycle
          NEDZoneO(i)=NEDZone(i)
          EDIntStepsO(i)=EDIntSteps(i)
          EDThreadsO(i)=EDThreads(i)
          EDTiltcorrO(i)=EDTiltcorr(i)
          EDGeometryIEDTO(i)=EDGeometryIEDT(i)
          OrMatEDZoneO(1:3,1:3,i)=OrMatEDZone(1:3,1:3,i)
          GMaxEDZoneO(i)=GMaxEDZone(i)
          SGMaxMEDZoneO(i)=SGMaxMEDZone(i)
          SGMaxREDZoneO(i)=SGMaxREDZone(i)
          CSGMaxREDZoneO(i)=CSGMaxREDZone(i)
          OmEDZoneO(i)=OmEDZone(i)
        enddo
      endif
      if(Allocated(NEDZone))
     1  deallocate(NEDZone,EDIntSteps,OrMatEDZone,GMaxEDZone,
     2             SGMaxMEDZone,SGMaxREDZone,CSGMaxREDZone,OmEDZone,
     3             EDThreads,EDTiltcorr,EDGeometryIEDT)
      allocate(NEDZone(KRefB),EDIntSteps(KRefB),
     1         OrMatEDZone(3,3,KRefB),GMaxEDZone(KRefB),
     2         SGMaxMEDZone(KRefB),SGMaxREDZone(KRefB),
     3         CSGMaxREDZone(KRefB),OmEDZone(KRefB),
     4         EDThreads(KRefB),EDTiltcorr(KRefB),EDGeometryIEDT(KRefB))
      if(NRefBlockED.gt.0) then
        do i=1,NRefBlockED
          if(RadiationRefBlock(i).ne.ElectronRadiation) cycle
          NEDZone(i)=NEDZoneO(i)
          EDIntSteps(i)=EDIntStepsO(i)
          EDThreads(i)=EDThreadsO(i)
          EDTiltcorr(i)=EDTiltcorrO(i)
          EDGeometryIEDT(i)=EDGeometryIEDTO(i)
          OrMatEDZone(1:3,1:3,i)=OrMatEDZoneO(1:3,1:3,i)
          GMaxEDZone(i)=GMaxEDZoneO(i)
          SGMaxMEDZone(i)=SGMaxMEDZoneO(i)
          SGMaxREDZone(i)=SGMaxREDZoneO(i)
          CSGMaxREDZone(i)=CSGMaxREDZoneO(i)
          OmEDZone(i)=OmEDZoneO(i)
        enddo
        deallocate(NEDZoneO,EDIntStepsO,OrMatEDZoneO,GMaxEDZoneO,
     1             SGMaxMEDZoneO,SGMaxREDZoneO,CSGMaxREDZoneO,OmEDZoneO,
     2             EDThreadsO,EDTiltcorrO,EDGeometryIEDTO)
      endif
9999  return
      end
