      subroutine AllocateAtoms(n)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(LocAtSystSt))
     1  deallocate(LocAtSystSt,SmbPGAt,TrAt,TriAt,TroAt,TroiAt,
     2             LocAtSystAx,LocAtSense,kapa1,kapa2,skapa1,skapa2,
     3             popc,spopc,popv,spopv,popas,spopas,NPGAt,RPGAt,
     4             LocAtSystX)
      if(ChargeDensities) then
        allocate(LocAtSystSt(2,n),SmbPGAt(n),TrAt(9,n),TriAt(9,n),
     1           TrOAt(9,n),TrOiAt(9,n),
     2           LocAtSystAx(n),LocAtSense(n),kapa1(n),kapa2(n),
     3           skapa1(n),skapa2(n),popc(n),spopc(n),popv(n),spopv(n),
     4           popas(64,n),spopas(64,n),NPGAt(n),RPGAt(9,48,n),
     5           LocAtSystX(3,2,n))
        mxda=78
      else
        mxda=10
      endif
      if(allocated(Atom))
     1  deallocate(Atom,isf,itf,lasmax,ifr,kmol,iswa,kswa,kfa,kmoda,
     2             kmodao,MagPar,TypeModFun,KUsePolar,
     3             isa,WyckoffMult,WyckoffSmb,PrvniKiAtomu,
     4             DelkaKiAtomu,KiA,ai,sai,a0,sa0,x,sx,beta,sbeta,
     5             AtSiteMult,xfr,sxfr)
      allocate(Atom(n),isf(n),itf(n),lasmax(n),ifr(n),kmol(n),
     1         iswa(n),kswa(n),KFA(7,n),KModA(7,n),kmodao(7,n),
     2         MagPar(n),TypeModFun(n),KUsePolar(n),
     3         isa(MaxNSymm,n),WyckoffMult(n),WyckoffSmb(n),
     4         PrvniKiAtomu(n),DelkaKiAtomu(n),KiA(mxda,n),
     5         ai(n),sai(n),a0(n),sa0(n),x(3,n),sx(3,n),beta(6,n),
     6         sbeta(6,n),AtSiteMult(n),xfr(n),sxfr(n))
      if(allocated(qcnt)) deallocate(qcnt,phf,sphf,OrthoX40,OrthoDelta,
     1                               OrthoEps)
      if(MaxNDimI.gt.0)
     1  allocate(qcnt(3,n),phf(n),sphf(n),OrthoX40(n),OrthoDelta(n),
     2           OrthoEps(n))
      if(allocated(NamePolar)) deallocate(NamePolar,sm0,ssm0)
      if(MaxMagneticType.gt.0) allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
      if(allocated(tztl)) deallocate(tztl,tzts,durdr)
      if(NMolec.gt.0) allocate(tztl(36,n),tzts(54,n),durdr(9,n))
      if(allocated(ax)) deallocate(ax,ay,sax,say)
      if(allocated(ux)) deallocate(ux,uy,sux,suy)
      if(allocated(bx)) deallocate(bx,by,sbx,sby)
      if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
      if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
      if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
      if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
      MxAtInd=n
      MxAtAll=n
      NAtAllocMod=0
      return
      end
