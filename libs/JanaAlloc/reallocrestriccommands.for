      subroutine ReallocRestricCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: naio(:,:),naixbo(:),maio(:),nailso(:)
      real, allocatable :: sumaio(:)
      character*12, allocatable :: atvaio(:,:),RestTypeO(:)
      if(nvai.gt.0) then
        allocate(naio(NAtCalc,nvai),naixbo(nvai),sumaio(nvai),
     1           maio(nvai),atvaio(NAtCalc,nvai),nailso(nvai),
     2           RestTypeO(nvai))
        call CopyVekI(naixb,naixbo,nvai)
        call CopyVekI(mai,maio,nvai)
        call CopyVekI(nails,nailso,nvai)
        call CopyVek(sumai,sumaio,nvai)
        do i=1,nvai
          RestTypeO(i)=RestType(i)
          do j=1,mai(i)
            naio(j,i)=nai(j,i)
            atvaio(j,i)=atvai(j,i)
          enddo
        enddo
      endif
      if(allocated(nai)) deallocate(nai,naixb,sumai,mai,atvai,nails,
     1                              RestType)
      m=max(NAtCalc,NPhase)
      allocate(nai(m,n),naixb(n),sumai(n),mai(n),atvai(m,n),nails(n),
     1         RestType(n))
      if(nvai.gt.0) then
        call CopyVekI(naixbo,naixb,nvai)
        call CopyVekI(maio,mai,nvai)
        call CopyVek(sumaio,sumai,nvai)
        call CopyVekI(nailso,nails,nvai)
        do i=1,nvai
          RestType(i)=RestTypeO(i)
          do j=1,maio(i)
            nai(j,i)=naio(j,i)
            atvai(j,i)=atvaio(j,i)
          enddo
        enddo
        deallocate(naio,naixbo,sumaio,maio,atvaio,nailso,RestTypeO)
      endif
      nvaiMax=n
      return
      end
