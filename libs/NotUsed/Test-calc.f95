      subroutine Calc(iov)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension hp(3),h(3),SpH(64),smp(3),AMagM(3),BMagM(3),AMagMR(3),
     1          BMagMR(3),AMagMP(3),BMagMP(3),DerAMagM(3),DerBMagM(3),
     2          DerAMagMR(3),DerBMagMR(3),DerAMagMP(3),DerBMagMP(3)
      character*80 ch80
      logical izo,main
      real kapa1i,kapa2i,NormNuclear,NormMagnetic
      open(66,file='Test-der.txt')
      a=0.
      b=0.
      call SetRealArrayTo(AMag,3,0.)
      call SetRealArrayTo(BMag,3,0.)
      af=0.
      bf=0.
      if(itw.eq.1) then
        afst=0.
        bfst=0.
        affree=0.
        bffree=0.
        affreest=0.
        bffreest=0.
      endif
      aanom=0.
      banom=0.
      if(Nicova) then
        FCalc=0.
        FCalcMag=0.
        FCalcMagPol=0.
        fyr=0.
        call SetRealArrayTo(der,LastNp,0.)
        call SetRealArrayTo(dc, LastNp,0.)
        go to 6100
      endif
      if(kanref(KDatBlock).ne.0) then
        i=(KDatBlock-1)*MxScAll+MxSc+18
        call SetRealArrayTo(der(i+1),2*MaxNAtFormula*NDatBlock,0.)
        call SetRealArrayTo(dc (i+1),2*MaxNAtFormula*NDatBlock,0.)
        ianomr=i+2*(KPhase-1)*MaxNAtFormula
        ianomi=ianomr+MaxNAtFormula
      endif
      do i=1,NAtCalc
        m=PrvniKiAtomu(i)
        NDerAll=DelkaKiAtomu(i)
        aii=ai(i)
        isw=iswa(i)
        if(aii.eq.0..or.kswa(i).ne.KPhase) then
          if(CalcDer) then
            call SetRealArrayTo(der(m),NDerAll,0.)
            call SetRealArrayTo(dc(m), NDerAll,0.)
          endif
          fyr(i)=0.
          cycle
        endif
        if(NDimI(KPhase).gt.0) then
          if(metoda.eq.0) then
            call calcm1(i,0)
          else
            call calcm2(i,i,isw,0)
          endif
        else
          nemod=.true.
        endif
        if(Radiation(KDatBlock).eq.NeutronRadiation) then
          lasmaxi=0
        else
          lasmaxi=lasmax(i)
        endif
        itfi=itf(i)
        ifri=ifr(i)
        MagParI=MagPar(i)
        NDerNemodFirst=1
        NDerNemodLast=max(TRankCumul(itfi),10)
        NCoef=NDerNemodLast-4
        if(ifri.ne.0) NDerNemodLast=NDerNemodLast+1
        if(lasmaxi.gt.0) then
          NDerChd=NDerAll-2
          NDerNemodLast=NDerNemodLast+3
          if(lasmaxi.gt.1) then
            j=(lasmaxi-1)**2
            NDerChd=NDerChd-1-j
            NDerNemodLast=NDerNemodLast+1+j
          endif
        else
          NDerChd=NDerAll+1
        endif
        NDerModFirst=NDerNemodLast+1
        if(Nemod) then
          NDerModLast=NDerNemodLast
        else
          NDerModLast=NDerAll
          if(MagParI.gt.0) NDerModLast=NDerModLast-3-6*(MagParI-1)
        endif
        NDerMagFirst=NDerModLast+1
        NDerMagLast=NDerAll
        izo=itfi.eq.1
        if(.not.okr(isw)) then
          fyr(i)=0.
          cycle
        endif
        pysvej=CellVol(1,1)/CellVol(isw,KPhase)
        atomic=kmol(i).eq.0
        l=0
        do n=0,max(itfi,2)
          nrank=TRank(n)
          do j=1,nrank
            l=l+1
            if(n.eq.0) then
              par(l)=aii
            else if(n.eq.1) then
              par(l)=x(j,i)
            else if(n.eq.2) then
              if(izo.and.j.ge.2) then
                par(l)=0.
              else
                par(l)=beta(j,i)
              endif
            else if(n.eq.3) then
              par(l)=c3(j,i)
            else if(n.eq.4) then
              par(l)=c4(j,i)
            else if(n.eq.5) then
              par(l)=c5(j,i)
            else
              par(l)=c6(j,i)
            endif
          enddo
        enddo
        if(ifri.gt.0) then
          l=l+1
          xfri=xfr(i)
          par(l)=xfri
          idfr=l
        endif
        if(NDimI(KPhase).gt.0) then
          mm=0
          lx=l
          do n=1,itfi+1
            nrank=TRank(n-1)
            kmod=KModA(n,i)
            if(kmod.le.0) cycle
            mm=mm+1
            do k=1,kmod
              do j=1,nrank
                lx=lx+1
                ly=lx+nrank
                if(n.eq.1) then
                  if(k.eq.1) then
                    par(lx)=a0(i)
                    lx=lx+1
                    ly=lx+nrank
                  endif
                  par(lx)=ax(k,i)
                  par(ly)=ay(k,i)
                else if(n.eq.2) then
                  par(lx)=ux(j,k,i)
                  par(ly)=uy(j,k,i)
                else if(n.eq.3) then
                  par(lx)=bx(j,k,i)
                  par(ly)=by(j,k,i)
                else if(n.eq.4) then
                  par(lx)=c3x(j,k,i)
                  par(ly)=c3y(j,k,i)
                else if(n.eq.5) then
                  par(lx)=c4x(j,k,i)
                  par(ly)=c4y(j,k,i)
                else if(n.eq.6) then
                  par(lx)=c5x(j,k,i)
                  par(ly)=c5y(j,k,i)
                else
                  par(lx)=c6x(j,k,i)
                  par(ly)=c6y(j,k,i)
                endif
              enddo
              lx=ly
            enddo
          enddo
          if(mm.gt.0) then
            lx=lx+1
            par(lx)=phf(i)
          endif
        endif
        if(CalcDer) then
          n=NDerAll
        else
          n=1
        endif
        call SetRealArrayTo(ader,n,0.)
        call SetRealArrayTo(bder,n,0.)
        if(MagneticType(KPhase).ne.0) then
          call SetRealArrayTo(aderm,3*n,0.)
          call SetRealArrayTo(bderm,3*n,0.)
        endif
        ader1=0.
        bder1=0.
        ader10=0.
        bder10=0.
        aderst=0.
        bderst=0.
        aderfreest=0.
        bderfreest=0.
        expij=1.
        isfi=isf(i)
        fxi=fx(isfi)
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          fyri=ffia(isfi,KPhase,KDatBlock)
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
          fyri=FFNi(isfi,KPhase)
        else if(Radiation(KDatBlock).eq.ElectronRadiation) then
          fyri=ffia(isfi,KPhase,KDatBlock)
        endif
        fxrfree=fxi
        if(lasmaxi.le.0) then
          fxrp=fxi
        else
          kapa1i=kapa1(i)
          if(kapa1i.le.0.) go to 9600
          kapa1i=1./kapa1i
          if(lasmaxi.gt.1) then
            kapa2i=kapa2(i)
            if(kapa2i.le.0.) go to 9600
            kapa2i=1./kapa2i
          endif
          call GetDFormF(ffcore(1,isfi,KPhase),121,sinthl,fxp,dfxp)
          l=NDerChd
          if(ffcore(1,isfi,KPhase).gt..00001) then
            ader(l)=fxp/ffcore(1,isfi,KPhase)
          else
            ader(l)=0.
          endif
          call GetDFormF(ffval(1,isfi,KPhase),121,sinthl*kapa1i,fxp,
     1                   dfxp)
          fxrp=ader(l)*Popc(i)+fxp*Popv(i)
          ader(l+1)=fxp
          ader(l+2)=-sinthl*Popv(i)*dfxp*kapa1i**2
        endif
        fxr=fxrp+ffra(isfi,KPhase,KDatBlock)
!        if(fxr.ne.0.) then
!          fxrp=fxrp/fxr
!        else
!          fxrp=0.
!        endif
        fxrfree=fxrfree+ffra(isfi,KPhase,KDatBlock)
        if(kanref(KDatBlock).ne.0) kanomr=isfi+ianomr
        if(MagParI.ne.0) then
          fxm=fm(isfi)*BohrMagToScatt
        else
          fxm=0.
        endif
        js=1
        do j=1,NSymmRef(KPhase)
          if(isaRef(js,i).lt.0) go to 4950
          if(NCoef.gt.0) call CopyVek(HCoef(5,j,isw),HCoefJ(5),NCoef)
          hj(1)=rh(j,isw)
          hj(2)=rk(j,isw)
          hj(3)=rl(j,isw)
          if(NDimI(KPhase).gt.0) then
            mj=imnp(j,isw,1)
            main=mj.eq.0
            if(NDimI(KPhase).gt.1) then
              nj=imnp(j,isw,2)
              main=main.and.nj.eq.0
              if(NDimI(KPhase).gt.2) then
                pj=imnp(j,isw,3)
                main=main.and.pj.eq.0
              endif
            endif
            if(atomic.and.KCommen(KPhase).le.0) then
              athj=ath(j,isw)
              atkj=atk(j,isw)
              atlj=atl(j,isw)
            else
              athj=hj(1)
              atkj=hj(2)
              atlj=hj(3)
            endif
          else
            athj=hj(1)
            atkj=hj(2)
            atlj=hj(3)
          endif
          HCoefj(2)=athj
          HCoefj(3)=atkj
          HCoefj(4)=atlj
          PhiAng=t(j)
          write(66,'(''PhiAng1:'',f10.6)') PhiAng
          do l=2,4
            PhiAng=PhiAng+HCoefj(l)*par(l)
          enddo
          PhiAng=PhiAng-anint(PhiAng/pi2)*pi2
          write(66,'(''PhiAng2:'',f10.6)') PhiAng
          if(.not.izo) then
            pom=0.
            do l=5,10
              pom=pom-HCoefj(l)*par(l)
            enddo
            expij=ExpJana(pom,ich)
            if(expij.gt.1000..or.ich.ne.0) go to 9800
          endif
          Creal=1.
          Cimag=0.
          if(itfi.gt.2) then
            l=10
            imag=.true.
            MinusReal=.true.
            do n=3,itfi
              nrank=TRank(n)
              if(mod(n,2).eq.0) MinusReal=.not.MinusReal
              imag=.not.imag
              do k=1,nrank
                l=l+1
                pom=HCoefj(l)*par(l)
                if(MinusReal) pom=-pom
                if(imag) then
                  Creal=Creal+pom
                else
                  Cimag=Cimag+pom
                endif
              enddo
            enddo
          endif
          if(NDimI(KPhase).le.0) then
            sinijst=sin(PhiAng)
            cosijst=cos(PhiAng)
            sinij=sinijst*expij
            cosij=cosijst*expij
            if(itfi.gt.2) then
              cosijc=cosij
              sinijc=sinij
              cosij=cosijc*Creal-sinijc*Cimag
              sinij=sinijc*Creal+cosijc*Cimag
            endif
          else
            sinijst=0.
            cosijst=0.
            if(metoda.eq.0) then
              call calcm1(i,1)
              if(itfi.gt.2) then
                cosijc=cosij
                sinijc=sinij
                cosij=cosijc*Creal-sinijc*Cimag
                sinij=sinijc*Creal+cosijc*Cimag
              endif
            else
              if(KCommen(KPhase).gt.0) PhiAng=PhiAng+trezm(j,isw)
              write(66,'(''PhiAng3:'',2f10.6)') PhiAng,trezm(j,isw)
              call calcm2(i,j,isw,1)
              if(ErrFlag.ne.0) go to 9800
            endif
          endif
          sinija=sinij
          cosija=cosij
          sinij0=sinij
          cosij0=cosij
          if(itfi.gt.2) then
            cosijca=cosijc
            sinijca=sinijc
          endif
          sinijast=sinijst
          cosijast=cosijst
          if(lasmaxi.gt.1) then
            hp(1)=rh(j,1)
            hp(2)=rk(j,1)
            hp(3)=rl(j,1)
            kk=NderChd+3
            call multm(hp,triat(1,i),h,1,3,3)
            call SpherHarm(h,SpH,lasmaxi-2,3)
            Creal=fxr
            Cimag=0.
            Dreal=0.
            Dimag=0.
            k=0
            l=lasmaxi-1
            Slat=SlaterFB(NSlater(l,isfi,KPhase),-1,sinthl*kapa2i,
     1                    ZSlater(l,isfi,KPhase)/BohrRad,DerZ,DerS,ich)
            if(ich.ne.0) go to 9700
            do l=0,lasmaxi-2
              pomk=ZSlater(l+1,isfi,KPhase)/BohrRad
              Slat=SlaterFB(NSlater(l+1,isfi,KPhase),l,sinthl*kapa2i,
     1                      pomk,DerZ,DerS,ich)
              if(ich.ne.0) go to 9700
              pom=0
              do n=1,2*l+1
                k=k+1
                pomr=Slat*SpH(k)
                pom=pom+popas(k,i)*pomr
                SpH(k)=pomr
              enddo
              pomr=-pom*DerS*sinthl*kapa2i**2
              n=mod(l,4)
              if(n.eq.0) then
                Creal=Creal+pom
                Dreal=Dreal+pomr
              else if(n.eq.1) then
                Cimag=Cimag+pom
                Dimag=Dimag+pomr
              else if(n.eq.2) then
                Creal=Creal-pom
                Dreal=Dreal-pomr
              else
                Cimag=Cimag-pom
                Dimag=Dimag-pomr
              endif
            enddo
            cosij=cosija*Creal-sinija*Cimag
            sinij=sinija*Creal+cosija*Cimag
            if(itfi.gt.2) then
              cosijc=cosijca*Creal-sinijca*Cimag
              sinijc=sinijca*Creal+cosijca*Cimag
            endif
            cosijst=cosijast*Creal-sinijast*Cimag
            sinijst=sinijast*Creal+cosijast*Cimag
            if(CalcDer) then
              ader(kk)=ader(kk)+cosija*Dreal-sinija*Dimag
              bder(kk)=bder(kk)+sinija*Dreal+cosija*Dimag
            endif
          else
            if(MagParI.gt.0.and.NDimI(KPhase).le.0) then
              cosijmp=cosij
              sinijmp=sinij
            endif
            cosij=cosij*fxr
            sinij=sinij*fxr
            cosijst=cosijst*fxr
            sinijst=sinijst*fxr
            if(itfi.gt.2) then
              cosijc=cosijc*fxr
              sinijc=sinijc*fxr
            endif
          endif
          ader(1)=ader(1)+cosij
          ader10=ader10+cosij0
          if(lasmaxi.gt.0) then
            ader1=ader1+cosija
            aderst=aderst+cosijst
            aderfreest=aderfreest+fxrfree*cosijast
          else if(MagParI.ne.0) then
            if(NDimI(KPhase).le.0) then
              call Multm(RMag(1,j,1,KPhase),sm0(1,i),smp,3,3,1)
              do l=1,3
                cosijm(l)=smp(l)*cosijmp
                sinijm(l)=smp(l)*sinijmp
                if(CalcDer) then
                  ind=l
                  do n=NDerMagFirst,NDerMagFirst+2
                    pom=RMag(ind,j,isw,KPhase)
                    if(NCSymmRef(KPhase).eq.1) then
                      aderm(l,n)=aderm(l,n)+cosijmp*pom
                      bderm(l,n)=bderm(l,n)+sinijmp*pom
                    else
                      if(ZCSymmRef(KPhase).gt.0.) then
                        aderm(l,n)=aderm(l,n)+cosijmp*pom
                      else
                        bderm(l,n)=bderm(l,n)+sinijmp*pom
                      endif
                    endif
                    ind=ind+3
                  enddo
                endif
              enddo
            endif
            do l=1,3
              cosijm(l)=cosijm(l)*fxm
              sinijm(l)=sinijm(l)*fxm
              if(NCSymmRef(KPhase).eq.1) then
                aderm(l,1)=aderm(l,1)+cosijm(l)
                bderm(l,1)=bderm(l,1)+sinijm(l)
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  aderm(l,1)=aderm(l,1)+cosijm(l)
                else
                  bderm(l,1)=bderm(l,1)+sinijm(l)
                endif
              endif
            enddo
          endif
          if(NCSymmRef(KPhase).eq.1) then
            bder(1)=bder(1)+sinij
            bder10=bder10+sinij0
            if(lasmaxi.gt.0) then
              bder1=bder1+sinija
              bderst=bderst+sinijst
              bderfreest=bderfreest+fxrfree*sinijast
            endif
          endif
          if(.not.CalcDer) go to 4950
          do l=2,4
            pom=HCoefj(l)
            ader(l)=ader(l)-pom*sinij
            if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*cosij
            if(MagParI.ne.0) then
              do n=1,3
                if(NCSymmRef(KPhase).eq.1) then
                  aderm(n,l)=aderm(n,l)-pom*sinijm(n)
                  bderm(n,l)=bderm(n,l)+pom*cosijm(n)
                else
                  if(ZCSymmRef(KPhase).gt.0.) then
                    aderm(n,l)=aderm(n,l)-pom*sinijm(n)
                  else
                    bderm(n,l)=bderm(n,l)+pom*cosijm(n)
                  endif
                endif
              enddo
            endif
          enddo
          if(.not.izo) then
            do l=5,10
              pom=-HCoefj(l)
              ader(l)=ader(l)+pom*cosij
              if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*sinij
              if(MagParI.ne.0) then
                do n=1,3
                  if(NCSymmRef(KPhase).eq.1) then
                    aderm(n,l)=aderm(n,l)+pom*cosijm(n)
                    bderm(n,l)=bderm(n,l)+pom*sinijm(n)
                  else
                    if(ZCSymmRef(KPhase).gt.0.) then
                      aderm(n,l)=aderm(n,l)+pom*cosijm(n)
                    else
                      bderm(n,l)=bderm(n,l)+pom*sinijm(n)
                    endif
                  endif
                enddo
              endif
            enddo
            l=10
            imag=.false.
            MinusReal=.true.
            MinusImag=.true.
            do n=3,itfi
              nrank=TRank(n)
              if(mod(n,2).eq.1) then
                MinusReal=.not.MinusReal
              else
                MinusImag=.not.MinusImag
              endif
              imag=.not.imag
              if(imag) then
                pomi=cosijc
                pomr=sinijc
              else
                pomi=sinijc
                pomr=cosijc
              endif
              if(MinusReal) pomr=-pomr
              if(MinusImag) pomi=-pomi
              do k=1,nrank
                l=l+1
                pom=HCoefj(l)
                ader(l)=ader(l)+pom*pomr
                if(NCSymmRef(KPhase).eq.1) bder(l)=bder(l)+pom*pomi
              enddo
            enddo
          else
            l=10
          endif
          if(lasmaxi.gt.1) then
            kk=l+4
            k=0
            do l=0,lasmaxi-2
              md=mod(l,4)
              do n=1,2*l+1
                k=k+1
                kk=kk+1
                if(md.eq.0) then
                  pomr= SpH(k)*cosija
                  pomi= SpH(k)*sinija
                else if(md.eq.1) then
                  pomr=-SpH(k)*sinija
                  pomi= SpH(k)*cosija
                else if(md.eq.2) then
                  pomr=-SpH(k)*cosija
                  pomi=-SpH(k)*sinija
                else
                  pomr= SpH(k)*sinija
                  pomi=-SpH(k)*cosija
                endif
                ader(kk)=ader(kk)+pomr
                if(NCSymmRef(KPhase).eq.1) bder(kk)=bder(kk)+pomi
              enddo
            enddo
          endif
          if(metoda.eq.0) call calcm1(i,2)
4950      js=js+NCSymmRef(KPhase)
        enddo
        fta=aii*pysvej
        fta=fta*float(ismRef(i))
        if(phfp(isw).ne.0..and.phf(i).ne.0..and..not.nemod)
     1    fta=exp(phfp(isw)*phf(i))
        if(ifri.eq.1) then
          pom=pi4*sinthl*xfri
          fj0=sin(pom)/pom
          dj0=pi4*sinthl/tan(pom)-1./xfri
        else if(ifri.eq.2) then
!          pom=2.*sqrt(hj**2*prcp(1,isw,KPhase)+kj**2*prcp(2,isw,KPhase)+
!     1                hj*kj*prcp(4,isw,KPhase))
          pom=pom*xfri
          fj0= bessj0(pom)
          dj0=-bessj1(pom)*pom/fj0
        endif
        if(ifri.ne.0) fta=fta*fj0
        ftast=fta
        if(izo) then
          tfi=ExpJana(-sinthlq*par(5),ich)
          if(tfi.gt.1000..or.ich.ne.0) go to 9800
          fta=fta*tfi
        endif
        if(lasmaxi.gt.0) then
          do l=NderChd,NderChd+2
            pom=ader(l)
            ader(l)=ader1*pom
            if(NCSymmRef(KPhase).eq.1) bder(l)=bder1*pom
          enddo
        else if(MagParI.ne.0) then
          do l=1,3
            if(NCSymmRef(KPhase).eq.1) then
              AMag(l)=AMag(l)+aderm(l,1)*fta
              BMag(l)=BMag(l)+bderm(l,1)*fta
            else
              if(ZCSymmRef(KPhase).gt.0.) then
                AMag(l)=AMag(l)+aderm(l,1)*fta
              else
                BMag(l)=BMag(l)+bderm(l,1)*fta
              endif
            endif
          enddo
        endif
        ftacos =fta*ader(1)
        ftacos0=fta*ader10
        if(NCSymmRef(KPhase).eq.1) then
          ftasin =fta*bder(1)
          ftasin0=fta*bder10
        endif
        a =a+ ftacos
!        af=af+ftacos*fxrp
        af=af+ftacos0*fxr
        if(itw.eq.1.and.lasmaxi.gt.0) then
          afst=afst+ftast*aderst
          affree=affree+fta*fxrfree*ader1*fxrp
          affreest=affreest+ftast*aderfreest
        endif
!        if(abs(fxr).gt..001) then
!          fyri=fyri/fxr
!        else
!          fyri=0.
!        endif
        aanom=aanom+fyri*ftacos0
        if(NCSymmRef(KPhase).eq.1) then
          b =b +ftasin
!          bf=bf+ftasin*fxrp
          bf=bf+ftasin0*fxr
          if(itw.eq.1.and.lasmaxi.gt.0) then
            bfst=bfst+ftast*bderst
            bffree=bffree+fta*fxrfree*bder1*fxrp
            bffreest=bffreest+ftast*bderfreest
          endif
          banom=banom+fyri*ftasin0
        endif
!        fyr(i)=fyri
        if(abs(fxr).gt..001) then
          fyr(i)=fyri/fxr
        else
          fyr(i)=0.
        endif
        if(.not.CalcDer) cycle
        if(izo) then
          ader(5)=-sinthlq*ader(1)
          if(NCSymmRef(KPhase).eq.1) bder(5)=-sinthlq*bder(1)
          if(MagParI.ne.0) then
            do j=1,3
              if(NCSymmRef(KPhase).eq.1) then
                aderm(j,5)=-sinthlq*aderm(j,1)
                bderm(j,5)=-sinthlq*bderm(j,1)
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  aderm(j,5)=-sinthlq*aderm(j,1)
                else
                  bderm(j,5)=-sinthlq*bderm(j,1)
                endif
              endif
            enddo
          endif
        endif
        if(kanref(KDatBlock).ne.0) then
          der(kanomr)=der(kanomr)+ftacos/fxr
          if(NCSymmRef(KPhase).eq.1) dc(kanomr)=dc(kanomr)+ftasin/fxr
        endif
        if(NDimI(KPhase).gt.0) then
          if(metoda.eq.0) call calcm1(i,3)
          if(.not.nemod) then
            l=m+DelkaKiAtomu(i)-1
            der(l)=phfp(isw)*ftacos
            if(NCSymmRef(KPhase).eq.1) dc(l)=phfp(isw)*ftasin
          endif
        endif
        ader(1)=ader(1)/aii
        bder(1)=bder(1)/aii
        if(MagParI.ne.0) then
          do j=1,3
            if(NCSymmRef(KPhase).eq.1) then
              aderm(j,1)=aderm(j,1)/aii
              bderm(j,1)=bderm(j,1)/aii
            else
              if(ZCSymmRef(KPhase).gt.0.) then
                aderm(j,1)=aderm(j,1)/aii
              else
                bderm(j,1)=bderm(j,1)/aii
              endif
            endif
          enddo
        endif
        l=m
        ftap=fta
        do n=1,NDerAll
          if(n.ge.NDerModFirst.and.n.le.NDerModLast) then
            fta=ftap*fxr
            ftam=ftap*fxm
          else
            fta=ftap
            if(MagParI.gt.0.and.n.lt.NDerModFirst) then
              ftam=ftap
            else
              ftam=ftap*fxm
            endif
          endif
          der(l)=ader(n)*fta
          if(NCSymmRef(KPhase).eq.1) then
            dc(l)=bder(n)*fta
          else
            dc(l)=0.
          endif
          if(MagParI.ne.0) then
            do j=1,3
              if(NCSymmRef(KPhase).eq.1) then
                derm(j,l)=aderm(j,n)*ftam
                dcm (j,l)=bderm(j,n)*ftam
              else
                if(ZCSymmRef(KPhase).gt.0.) then
                  derm(j,l)=aderm(j,n)*ftam
                  dcm (j,l)=0.
                else
                  derm(j,l)=0.
                  dcm (j,l)=bderm(j,n)*ftam
                endif
              endif
            enddo
          else if(MagneticType(KPhase).ne.0) then
            do j=1,3
              derm(j,l)=0.
              dcm (j,l)=0.
            enddo
          endif
          l=l+1
        enddo
        if(ifri.ne.0) then
          k=m+idfr-1
          der(k)=dj0*ftacos
          if(NCSymmRef(KPhase).eq.1) dc(k)=dj0*ftasin
        endif
      enddo
      a=a*CentrRef(KPhase)
      b=b*CentrRef(KPhase)
      af=af*CentrRef(KPhase)
      bf=bf*CentrRef(KPhase)
      if(NDimI(KPhase).eq.0.and.itw.eq.1) then
        affree=affree*CentrRef(KPhase)
        bffree=bffree*CentrRef(KPhase)
        afst=afst*CentrRef(KPhase)
        bfst=bfst*CentrRef(KPhase)
        affreest=affreest*CentrRef(KPhase)
        bffreest=bffreest*CentrRef(KPhase)
      endif
      a=a-banom*CentrRef(KPhase)
      b=b+aanom*CentrRef(KPhase)
      sqrtab=sqrt(a**2+b**2)
      IZdvih=(KDatBlock-1)*MxScAll
      der(iq+IZdvih)=sqrtab
      sciq=sc(iq,KDatBlock)
      if(isPowder) then
        sciq=(sciq*.01)**2
        if(NPhase.gt.1) sciq=sciq*abs(sctw(KPhase,KDatBlock))
        sciq=sqrt(sciq)
      endif
      if(OverAllB(KDatBlock).ne.0.) then
        TFOverAll=ExpJana(-sinthlq*episq*OverAllB(KDatBlock),ich)
      else
        TFOverAll=1.
      endif
      FCalc=sqrtab*sciq*TFOverAll
      ACalc=a*sciq*TFOverAll
      BCalc=b*sciq*TFOverAll
      if(ExtTensor(KDatBlock).gt.0) then
        call philip(0,extinc,extink)
        extkor=sqrt(extinc)
        FCalc=FCalc*extkor
        ACalc=ACalc*extkor
        BCalc=BCalc*extkor
        if(CalcDer) then
          der(iq+IZdvih)=der(iq+IZdvih)*extkor
          if(extinc.gt.0.) then
            PhiPom=extkor*extink/extinc
          else
            PhiPom=0.
          endif
        endif
      else
        PhiPom=1.
      endif
      if(MagneticType(KPhase).ne.0) then
        PomSc=CentrRef(KPhase)*sciq*TFOverAll
        call Multm(MetTens(1,1,KPhase),AMag,AMagR,3,3,1)
        call Multm(MetTens(1,1,KPhase),BMag,BMagR,3,3,1)
        FCalcNucl=FCalc
        FCalcMag=sqrt(max(
     1             VecOrtScal(AMag,AMagR,3)+VecOrtScal(BMag,BMagR,3)
     1             -VecOrtScal(AMag,HMagR(1,1),3)**2
     2             -VecOrtScal(BMag,HMagR(1,1),3)**2,0.))*PomSc
        if(MagPolFlag(KDatBlock).ne.0) then
          do i=1,3
            AMagM(i)=AMag(i)-VecOrtScal(AMag,HMagR(1,1),3)*HMag(i,1)
            BMagM(i)=BMag(i)-VecOrtScal(BMag,HMagR(1,1),3)*HMag(i,1)
          enddo
          call Multm(MetTens(1,1,KPhase),AMagM,AMagMR,3,3,1)
          call Multm(MetTens(1,1,KPhase),BMagM,BMagMR,3,3,1)
          call MultM(UBRef,AMagMR,AMagMP,3,3,1)
          call MultM(UBRef,BMagMR,BMagMP,3,3,1)
          FCalcMagPol=(ACalc*AMagMP(3)+BCalc*BMagMP(3))*PomSc
        endif
        derm(1,iq+IZdvih)=FCalcMag
        if(CalcDer.and.FCalcMag.gt.0) then
          pom=PomSc**2/FCalcMag
          do i=1,NAtCalc
            if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder).or.
     1         MagPar(i).eq.0) cycle
            l=PrvniKiAtomu(i)
            do m=l,l+DelkaKiAtomu(i)-1
              if(MagPolFlag(KDatBlock).ne.0) then
                do j=1,3
                  DerAMagM(j)=derm(j,m)
     1              -VecOrtScal(derm(1,m),HMagR(1,1),3)*HMag(j,1)
                  DerBMagM(j)=dcm(j,m)
     1              -VecOrtScal(dcm(1,m),HMagR(1,1),3)*HMag(j,1)
                enddo
                call MultM(MetTens(1,1,KPhase),DerAMagM,DerAMagMR,3,3,1)
                call MultM(MetTens(1,1,KPhase),DerBMagM,DerBMagMR,3,3,1)
                call MultM(UBRef,DerAMagMR,DerAMagMP,3,3,1)
                call MultM(UBRef,DerBMagMR,DerBMagMP,3,3,1)
                DerPol(1,m)=(ACalc*DerAMagMP(3)+AMagMP(3)*der(m)+
     1                       BCalc*DerBMagMP(3)+BMagMP(3)*dc(m))*PomSc
              endif
              derm(1,m)=pom*(VecOrtScal(AMagR,derm(1,m),3)+
     1                       VecOrtScal(BMagR,dcm(1,m),3)
     2                      -VecOrtScal(AMag,HMagR(1,1),3)*
     3                       VecOrtScal(derm(1,m),HMagR(1,1),3)
     4                      -VecOrtScal(BMag,HMagR(1,1),3)*
     5                       VecOrtScal(dcm(1,m),HMagR(1,1),3))
            enddo
          enddo
        endif
        if(ExtTensor(KDatBlock).gt.0) then
          call philip(1,extincm,extinkm)
          extkorm=sqrt(extincm)
          FCalcMag=FCalcMag*extkorm
          if(CalcDer) then
            derm(1,iq+IZdvih)=derm(1,iq+IZdvih)*extkorm
            if(extincm.gt.0.) then
              PhiPomM=extkorM*extinkM/extincM
            else
              PhiPomM=0.
            endif
          endif
        else
          PhiPomM=1.
        endif
      endif
6100  if(ExtTensor(KDatBlock).gt.0) ie=MxScAll*(KDatBlock-1)+MxSc+7
      yct(itw,iov)=FCalc
      ycq(itw)=FCalc**2
      if(MagneticType(KPhase).ne.0) then
        yctm(itw,iov)=FCalcMag
        ycqm(itw)=FCalcMag**2
        yctmp(itw,iov)=FCalcMagPol
      endif
      afour(itw,iov)=af
      bfour(itw,iov)=bf
      act(itw,iov)=a
      bct(itw,iov)=b
      if(NTwin.gt.1) then
        FCalcScTw=FCalc*sctw(itw,KDatBlock)
        yctw=yctw+FCalcScTw*FCalc
        if(MagneticType(KPhase).ne.0) then
          FCalcMagScTw=FCalcMag*sctw(itw,KDatBlock)
          yctwm=yctwm+FCalcMagScTw*FCalcMag
          yctwmp=yctwmp+FCalcMagPol*sctw(itw,KDatBlock)
        endif
        if(itw.eq.NTwin) then
          if(yctw.ge.0.) then
            FCalc=sqrt(yctw)
          else
            FCalc=0.
          endif
          if(MagneticType(KPhase).ne.0.and.yctw.ge.0.) then
            FCalcMag=sqrt(yctwm)
            FCalcMagPol=yctwmp
          endif
        endif
        if(ExtTensor(KDatBlock).gt.0.and.CalcDer) then
          j=0
          do i=ie,ie+11
            j=j+1
            if(itw.eq.1) dertw(i)=0.
            dertw(i)=dertw(i)+FCalcScTw*der(i)
            if(MagneticType(KPhase).ne.0) then
              if(itw.eq.1) dertwm(i)=0.
              dertwm(i)=dertwm(i)+FCalcMagScTw*derme(j)
            endif
          enddo
        endif
      endif
      if(CalcDer) then
        if(nicova) then
          c1=0.
          c2=0.
        else
          if(sqrtab.ne.0.) sqrtab=sciq*TFOverAll/sqrtab
          c1=a*sqrtab*CentrRef(KPhase)*PhiPom
          c2=b*sqrtab*CentrRef(KPhase)*PhiPom
          if(NTwin.gt.1) then
            c1=c1*FCalcScTw
            c2=c2*FCalcScTw
          else
            if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
              pc=2.*Fcalc
              c1=c1*pc
              c2=c2*pc
              if(ExtTensor(KDatBlock).gt.0) then
                do i=ie,ie+11
                  der(i)=der(i)*pc
                enddo
                if(MagneticType(KPhase).ne.0) then
                  pc=2.*FCalcMag
                  do i=1,12
                    derme(i)=derme(i)*pc
                  enddo
                endif
              endif
            endif
          endif
        endif
        if(NTwin.gt.1.and.itw.eq.NTwin) then
          if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
            NormNuclear=2.
          else
            if(FCalc.gt.0.) then
              NormNuclear=1./FCalc
            else
              NormNuclear=0.
            endif
          endif
          if(ExtTensor(KDatBlock).gt.0) then
            do i=ie,ie+11
              der(i)=dertw(i)*NormNuclear
            enddo
          endif
          if(MagneticType(KPhase).ne.0) then
            if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
              NormMagnetic=2.
            else
              if(FCalcMag.gt.0.) then
                NormMagnetic=1./FCalcMag
              else
                NormMagnetic=0.
              endif
            endif
            if(ExtTensor(KDatBlock).gt.0) then
              j=0
              do i=ie,ie+11
                j=j+1
                derme(j)=dertwm(i)*NormMagnetic
              enddo
            endif
          endif
        endif
        if(kanref(KDatBlock).ne.0) then
          j=ianomr
          l=ianomi
          do i=1,NAtFormula(KPhase)
            j=j+1
            l=l+1
            der(l)=-c1* dc(j)+c2*der(j)
            der(j)= c1*der(j)+c2* dc(j)
            if(NTwin.gt.1) then
              if(itw.eq.1) then
                dertw(l)=0.
                dertw(j)=0.
              endif
              dertw(l)=dertw(l)+der(l)
              dertw(j)=dertw(j)+der(j)
              if(itw.eq.NTwin) then
                der(l)=dertw(l)*NormNuclear
                der(j)=dertw(j)*NormNuclear
              endif
            endif
          enddo
        endif
        m=ndoff
        do i=1,NAtCalc
          if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder)) cycle
          ca=c1+fyr(i)*c2
          cb=c2-fyr(i)*c1
          j=PrvniKiAtomu(i)
          do m=j,j+DelkaKiAtomu(i)-1
            if(Radiation(KDatBlock).eq.ElectronRadiation.and.
     1         ExistM42.and.CalcDyn) then
              dcp(m)=CentrRef(KPhase)*PhiPom*(der(m)+fyr(i)*dc (m))
              dc (m)=CentrRef(KPhase)*PhiPom*(dc(m) -fyr(i)*der(m))
            else
              der(m)=ca*der(m)+cb*dc(m)
            endif
            if(MagneticType(KPhase).ne.0) derm(1,m)=PhiPomM*derm(1,m)
            if(NTwin.gt.1) then
              if(itw.eq.1) dertw(m)=0.
              dertw(m)=dertw(m)+der(m)
              if(itw.eq.NTwin) der(m)=dertw(m)*NormNuclear
              if(MagneticType(KPhase).ne.0) then
                if(itw.eq.1) dertwm(m)=0.
                dertwm(m)=dertwm(m)+derm(1,m)*FCalcMagScTw
                if(itw.eq.NTwin) derm(1,m)=dertwm(m)*NormMagnetic
              endif
            endif
          enddo
        enddo
      endif
      if(MagneticType(KPhase).ne.0.and.itw.eq.NTwin) then
        if(OnlyMag.gt.0) then
          FCalcB=0.
          FCalc=FCalcMag
        else
          FCalcB=FCalc
          if(MagPolFlag(KDatBlock).ne.0) then
           F1=FCalc**2+FCalcMag**2+2.*FCalcMagPol*MagPolPlus(KDatBlock)
           F2=FCalc**2+FCalcMag**2+2.*FCalcMagPol*MagPolMinus(KDatBlock)
           FCalc=F1/F2
          else
            FCalc=sqrt(FCalc**2+FCalcMag**2)
          endif
        endif
        if(FCalc.gt.0.) then
          FCalcR=1./FCalc
        else
          FCalcR=0.
        endif
        if(CalcDer.and.MagPolFlag(KDatBlock).eq.0) then
          if(ExtTensor(KDatBlock).gt.0.and.CalcDer) then
            j=0
            do i=ie,ie+11
              j=j+1
              if(j.eq.1) then
                pom=1./ecMag(1,KDatBlock)
                k=ie-5
                der(k)=0.
              else if(j.eq.7) then
                pom=1./ecMag(2,KDatBlock)
                k=ie-4
                der(k)=0.
              endif
              if(OnlyMag.gt.0) der(i)=0.
              if(ifsq.eq.1.and.MagPolFlag(KDatBlock).eq.0) then
                der(i)=der(i)+pom*derme(j)
                der(k)=der(k)+derme(j)*ec(j,KDatBlock)
              else
                der(i)=FCalcR*(FCalcB*der(i)+
     1                         pom*FCalcMag*derme(j))
                der(k)=der(k)+FCalcR*FCalcMag*derme(j)*ec(j,KDatBlock)
              endif
            enddo
          endif
        endif
        do i=1,NAtCalc
          if(ai(i).eq.0..or.(kswa(i).ne.KPhase.and.isPowder)) cycle
          l=PrvniKiAtomu(i)
          if(MagPolFlag(KDatBlock).gt.0) then
            if(F2.gt.0.) then
              RF2=1./F2**2
              F2mF1=2.*(F2-F1)
              F2mF1Pol=2.*(F2*MagPolPlus(KDatBlock)-
     1                     F1*MagPolMinus(KDatBlock))
            else
              RF2=0.
            endif
          endif
          do m=l,l+DelkaKiAtomu(i)-1
            if(MagPolFlag(KDatBlock).eq.0) then
              if(ifsq.eq.1) then
                der(m)=der(m)+2.*FCalcMag*derm(1,m)
              else
                der(m)=FCalcR*(FCalcB*der(m)+FCalcMag*derm(1,m))
              endif
            else
              if(F2.gt.0.) then
                der(m)=RF2*(F2mF1*(der(m)*FCalc+derm(1,m)*FCalcMag)+
     1                      F2mF1Pol*derpol(1,m))
              else
              endif
            endif
          enddo
        enddo
      endif
      go to 9999
9600  ch80='kappa or kappa'' of the atom "'//atom(i)(:idel(atom(i)))//
     1     '" is not positive'
      go to 9900
9700  ch80='Slater function for the atom "'//atom(i)(:idel(atom(i)))//
     1     '" cannot be calculated'
      go to 9900
9800  ch80='the atom "'//atom(i)(:idel(atom(i)))//
     1     '" has inacceptable ADP harmonic parameters'
9900  if(KeyDyn.ne.0.and.icykl.gt.0) then
        ErrFlag=2
      else
        call FeChybne(-1.,YBottomMessage,'numerical problem in CALC.',
     1                ch80,SeriousError)
        ErrFlag=1
      endif
9999  close(66)
      return
      end
      subroutine Calcm2(i,js,isw,klic)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension x4center(3),fc(3),smp(3),FPolA(2*mxw+1),ParSupX4(3),
     1          argl(3),delta(3),NSupPom(3)
      save qcnt1,qcnt2,qcnt3,MxMod,MxModC,NSupPom,mcp,mck,mp,MxSupPar,
     1     MagParI,last,itfi
      if(klic.lt.0) then
        if(ngcMax.gt.0) then
          call ComExp(sngc,csgc,MaxUsedKwAll,ngcMax,NAtCalc)
          if(allocated(isaRef)) deallocate(isaRef,ismRef,fyr)
          allocate(isaRef(MaxNSymm,NAtCalc),ismRef(NAtCalc),
     1             fyr(NAtCalc))
          call RefSpecAt
          if(ErrFlag.ne.0) go to 9810
        endif
        if(allocated(PrvniParSup)) deallocate(PrvniParSup,DelkaParSup)
        allocate(PrvniParSup(NAtCalc),DelkaParSup(NAtCalc))
        MxSup=0
        MxSupPar=1
        MxMod=0
        do ii=1,NAtCalc
          KPhase=kswa(ii)
          PrvniParSup(ii)=MxSupPar
          if(ai(ii).eq.0..or.NDimI(KPhase).le.0) then
            DelkaParSup(ii)=0
            cycle
          endif
          isw=iswa(ii)
          itfi=itf(ii)
          MagParI=MagPar(ii)
          if(KFA(1,ii).ne.0.and.KModA(1,ii).ne.0) then
            kfsi=KFA(1,ii)
          else
            kfsi=0
          endif
          if(KFA(2,ii).ne.0.and.KModA(2,ii).ne.0) then
            kfxi=KFA(2,ii)
          else
            kfxi=0
          endif
          do j=1,itfi+1
            MxMod=max(MxMod,KModA(j,ii))
          enddo
          MxMod=max(MxMod,MagParI-1)
          if(KCommen(KPhase).eq.0) then
            call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
            if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
              do j=1,NDimI(KPhase)
                NSupPom(j)=NSupPom(j)+2
              enddo
            endif
          else
            do j=1,3
              NSupPom(j)=NCommQ(j,isw,KPhase)
            enddo
          endif
          NSupPomAll=NSupPom(1)*NSupPom(2)*NSupPom(3)
          MxSup=MxSup+NSupPomAll
          MxSupPar=MxSupPar+7*NSupPomAll
          DelkaParSup(ii)=7
          do j=3,itfi+1
            kmod=KModA(j,ii)
            if(kmod.le.0) cycle
            nrank=TRank(j-1)
            DelkaParSup(ii)=DelkaParSup(ii)+nrank
            MxSupPar=MxSupPar+nrank*NSupPomAll
          enddo
          if(MagParI.gt.0) then
            MxSupPar=MxSupPar+3*NSupPomAll
            DelkaParSup(ii)=DelkaParSup(ii)+3
          endif
        enddo
        if(allocated(ParSup))
     1    deallocate(ParSup,snw,csw,skfx1,skfx2,skfx3,x4low,x4length,
     2           snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     3           sncospsc,cscospsc)
        allocate(ParSup(MxSupPar),snw(MxMod,MxSup),csw(MxMod,MxSup),
     1           skfx1(MxSup),skfx2(MxSup),skfx3(MxSup),
     2           x4low(3,NAtCalc),x4length(3,NAtCalc),
     3           snsinps(mxmod),cssinps(mxmod),sncosps(mxmod),
     4           cscosps(mxmod),snsinpsc(mxmod),cssinpsc(mxmod),
     5           sncospsc(mxmod),cscospsc(mxmod))
        mc=0
        mp=1
        do ii=1,NAtCalc
          KPhase=kswa(ii)
          if(NDimI(KPhase).le.0) cycle
          isw=iswa(ii)
          kmodxi=KModA(2,ii)
          kmodsi=KModA(1,ii)
          MagParI=MagPar(ii)
          if(KFA(1,ii).ne.0.and.kmodsi.ne.0) then
            kfsi=KFA(1,ii)
          else
            kfsi=0
          endif
          if(KFA(2,ii).ne.0.and.kmodxi.ne.0) then
            kfxi=KFA(2,ii)
          else
            kfxi=0
          endif
          itfi=itf(ii)
          mxmod=0
          do j=1,itfi+1
            mxmod=max(mxmod,KModA(j,ii))
          enddo
          MxMod=max(MxMod,MagParI-1)
          if(ai(ii).eq.0.) cycle
          atomic=kmol(ii).eq.0
          if(kfsi.ne.0) then
            k=kmodsi-NDim(KPhase)+3
            do j=1,NDimI(KPhase)
              k=k+1
              if(NDimI(KPhase).gt.1) then
                delta(j)=ay(k,ii)*.5
              else
                delta(j)=a0(ii)*.5
              endif
              x4center(j)=ax(k,ii)
              x4low(j,ii)=x4center(j)-delta(j)
              x4length(j,ii)=delta(j)*2.
            enddo
          else if(kfxi.gt.0) then
            x4center(1)=uy(1,kmodxi,ii)
            delta(1)=uy(2,kmodxi,ii)*.5
            if(kfxi.eq.1) then
              x4low(1,ii)=x4center(1)-delta(1)
              x4length(1,ii)=delta(1)*2.
            else
              x4low(1,ii)=0.
              x4length(1,ii)=1.
            endif
          else
            call SetRealArrayTo(x4low   (1,ii),NDimI(KPhase),0.)
            call SetRealArrayTo(x4length(1,ii),NDimI(KPhase),1.)
          endif
          if(KCommen(KPhase).eq.0) then
            call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
            if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
              do j=1,NDimI(KPhase)
                NSupPom(j)=NSupPom(j)+2
              enddo
            endif
          else
            do j=1,3
              NSupPom(j)=NCommQ(j,isw,KPhase)
            enddo
          endif
          do ic3=1,NSupPom(3)
            if(KCommen(KPhase).eq.0.or.NDimI(KPhase).ne.1) fc(3)=ic3-1
            if(NDimI(KPhase).gt.2.and.KCommen(KPhase).le.0) then
              if(ic3.le.NSuper(3,isw,KPhase)) then
                ParSupX4(3)=x4low(3,ii)+x4length(3,ii)*
     1                                 XGauss(ic3,3,KPhase)
              else if(ic3.eq.NSuper(3,isw,KPhase)+1) then
                ParSupX4(3)=x4low(3,ii)+.0001
              else
                ParSupX4(3)=x4low(3,ii)+x4length(3,ii)-.0001
              endif
            endif
            do ic2=1,NSupPom(2)
              if(KCommen(KPhase).eq.0.or.NDimI(KPhase).ne.1) fc(2)=ic2-1
              if(NDimI(KPhase).gt.1.and.KCommen(KPhase).le.0) then
                if(ic2.le.NSuper(2,isw,KPhase)) then
                  ParSupX4(2)=x4low(2,ii)+x4length(2,ii)*
     1                                   XGauss(ic2,2,KPhase)
                else if(ic2.eq.NSuper(2,isw,KPhase)+1) then
                  ParSupX4(2)=x4low(2,ii)+.0001
                else
                  ParSupX4(2)=x4low(2,ii)+x4length(2,ii)-.0001
                endif
              endif
              do ic1=1,NSupPom(1)
                mc=mc+1
                call SetRealArrayTo(ParSup(mp),7,0.)
                if(KCommen(KPhase).gt.0) then
                  if(NDimI(KPhase).ne.1) then
                    fc(1)=ic1-1
                  else
                    pom=ic1-1
                    do j=1,3
                      fc(j)=VCommQ(j,1,isw,KPhase)*pom
                    enddo
                  endif
                  do j=1,3
                    ParSup(mp+j)=fc(j)
                  enddo
                  call qbyx(fc,ParSup(mp+4),isw)
                  do j=4,NDim(KPhase)
                    ParSup(mp+j)=ParSup(mp+j)+trez(j-3,isw,KPhase)+
     1                                        qcnt(j-3,ii)
                  enddo
                else
                  if(ic1.le.NSuper(1,isw,KPhase)) then
                    ParSupX4(1)=x4low(1,ii)+x4length(1,ii)*
     1                                     XGauss(ic1,1,KPhase)
                  else if(ic1.eq.NSuper(1,isw,KPhase)+1) then
                    ParSupX4(1)=x4low(1,ii)+.0001
                  else
                    ParSupX4(1)=x4low(1,ii)+x4length(1,ii)-.0001
                  endif
                  call CopyVek(ParSupX4,ParSup(mp+4),NDimI(KPhase))
                endif
                call CopyVek(ParSup(mp+4),argl,NDimI(KPhase))
                kk=1
                if(InvMag(KPhase).gt.0) then
                  kmg=2
                else
                  kmg=1
                endif
                do k=1,mxmod
                  if(TypeModFun(ii).le.1.or.
     1               (kmg.eq.2.and.mod(k,kmg).eq.1)) then
                    arg=0.
                    do n=1,NDimI(KPhase)
                      arg=arg+float(kw(n,k,KPhase))*ParSup(mp+n+3)
                    enddo
                    arg=arg*pi2
                    snw(k,mc)=sin(arg)
                    csw(k,mc)=cos(arg)
                  else
                    if(k.eq.kmg) then
                      pom=float(kmg)*argl(1)-x4center(1)
                      j=pom
                      if(pom.lt.0.) j=j-1
                      pom=pom-float(j)
                      if(pom.gt..5) pom=pom-1.
                      pom=pom/delta(1)
                      call GetFPol(pom,FPolA,2*mxmod+1,TypeModFun(ii))
                    endif
                    if(kmg.eq.1.or.mod(k,kmg).eq.0) then
                      kk=kk+1
                      snw(k,mc)=FPolA(kk)
                      kk=kk+1
                      csw(k,mc)=FPolA(kk)
                    else
                      snw(k,mc)=0.
                      csw(k,mc)=0.
                    endif
                  endif
                enddo
                occx=1.
                do k=1,kmodxi
                  if(kfxi.gt.0.and.k.eq.kmodxi) then
                    pom=argl(1)-x4center(1)
                    j=pom
                    if(pom.lt.0.) j=j-1
                    pom=pom-float(j)
                    if(pom.gt..5) pom=pom-1.
                    if(kfxi.eq.1) then
                      znak=pom/delta(1)
                      zn=1.
                      if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                        occx=1.
                      else
                        occx=0.
                      endif
                    else
                      if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                        zn=1.
                        if(kfxi.eq.2) then
                          znak=pom/delta(1)
                        else
                          znak=1.
                        endif
                      else
                        if(kfxi.eq.2) then
                          pom=pom+.5
                          if(pom.gt..5) then
                            pom=pom-1.
                          else if(pom.lt.-.5) then
                            pom=pom+1.
                          endif
                          if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                            zn=-1.
                            znak=-pom/delta(1)
                            occx=1.
                          else
                            znak=0.
                            occx=0.
                          endif
                        else
                          znak=-1.
                        endif
                      endif
                    endif
                    do j=1,3
                      ParSup(mp+j)=ParSup(mp+j)+ux(j,k,ii)*znak
                      skfx1(mc)=znak
                      skfx2(mc)=-zn/delta(1)
                      skfx3(mc)=-.5*znak/delta(1)
                    enddo
                  else
                    sn=snw(k,mc)
                    cs=csw(k,mc)
                    do j=1,3
                      ParSup(mp+j)=ParSup(mp+j)+ux(j,k,ii)*sn
     1                                         +uy(j,k,ii)*cs
                    enddo
                  endif
                enddo
                occo=0.
                if(kmodsi.gt.0) then
                  if(kfsi.eq.0) occo=a0(ii)-occx
                  do k=1,kmodsi
                    if(kfsi.ne.0.and.k.ge.kmodsi-NDim(KPhase)+4)
     1                then
                      do l=1,NDimI(KPhase)
                        pom=argl(l)-x4center(l)
                        j=pom
                        if(pom.lt.0.) j=j-1
                        pom=pom-float(j)
                        if(pom.gt..5) pom=pom-1.
                        if(pom.ge.-delta(l)-.0001.and.
     1                     pom.le. delta(l)+.0001) then
                          occx=1.
                        else
                          occx=0.
                        endif
                        if(occx.lt..001) go to 1810
                      enddo
                    else
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      occo=occo+ax(k,ii)*sn+ay(k,ii)*cs
                    endif
                  enddo
                endif
1810            if(occx.gt.0.) then
                  ParSup(mp)=occo+occx
                else
                  ParSup(mp)=0.
                endif
                mp=mp+7
                do n=3,itfi+1
                  kmod=KModA(n,ii)
                  if(kmod.le.0) cycle
                  nrank=TRank(n-1)
                  do k=1,kmod
                    sn=snw(k,mc)
                    cs=csw(k,mc)
                    m=mp
                    do j=1,nrank
                      if(k.eq.1) ParSup(m)=0.
                      if(n.eq.3) then
                        ParSup(m)=ParSup(m)+bx(j,k,ii)*sn+by(j,k,ii)*cs
                      else if(n.eq.4) then
                        ParSup(m)=ParSup(m)+c3x(j,k,ii)*sn
     1                                     +c3y(j,k,ii)*cs
                      else if(n.eq.5) then
                        ParSup(m)=ParSup(m)+c4x(j,k,ii)*sn
     1                                     +c4y(j,k,ii)*cs
                      else if(n.eq.6) then
                        ParSup(m)=ParSup(m)+c5x(j,k,ii)*sn
     1                                     +c5y(j,k,ii)*cs
                      else
                        ParSup(m)=ParSup(m)+c6x(j,k,ii)*sn
     1                                     +c6y(j,k,ii)*cs
                      endif
                      m=m+1
                    enddo
                  enddo
                  mp=mp+nrank
                enddo
                if(MagParI.gt.0) then
                  m=mp
                  do j=1,3
                    ParSup(m)=sm0(j,ii)
                    do k=1,MagParI-1
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      ParSup(m)=ParSup(m)+smx(j,k,ii)*sn
     1                                   +smy(j,k,ii)*cs
                    enddo
                    m=m+1
                  enddo
                  mp=mp+3
                endif
              enddo
            enddo
          enddo
        enddo
        last=99999999
      else if(klic.eq.0) then
        mp=PrvniParSup(i)
        if(i.le.last) mck=0
        mcp=mck
        last=i
        itfi=itf(i)
        qcnt1=qcnt(1,i)
        if(NDim(KPhase).gt.4) then
          qcnt2=qcnt(2,i)
          if(NDim(KPhase).gt.5) qcnt3=qcnt(3,i)
        endif
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        MagParI=MagPar(i)
        if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
          kfsi=KFA(1,i)
        else
          kfsi=0
        endif
        if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
          kfxi=KFA(2,i)
        else
          kfxi=0
        endif
        if(kfxi.eq.0) then
          kmezx=kmodxi
        else
          kmezx=kmodxi-1
        endif
        if(kfsi.eq.0) then
          kmezs=kmodsi
        else if(kfsi.ne.0) then
          kmezs=kmodsi-NDim(KPhase)+3
        endif
        mxmod=0
        mxmodc=0
        do j=1,itfi+1
          mxmod=max(mxmod,KModA(j,i))
          if(j.gt.3) mxmodc=max(mxmodc,KModA(j,i))
        enddo
        MxMod=max(MxMod,MagParI-1)
        nemod=mxmod.le.0
      else if(klic.eq.1) then
        mp=PrvniParSup(i)
        mc=mcp
        if(.not.atomic.and.KCommen(KPhase).le.0) then
          PhiAng=PhiAng-pi2*float(mj)*qcnt1
          if(NDim(KPhase).gt.4) PhiAng=PhiAng-pi2*float(nj)*qcnt2
          if(NDim(KPhase).gt.5) PhiAng=PhiAng-pi2*float(pj)*qcnt3
        endif
        write(66,'(''PhiAng3:'',f10.6)') PhiAng
        cosij=0.
        sinij=0.
        cosijc=0.
        sinijc=0.
        call SetRealArrayTo(cosijm,3,0.)
        call SetRealArrayTo(sinijm,3,0.)
        if(KCommen(KPhase).eq.0.) then
          call CopyVekI(NSuper(1,isw,KPhase),NSupPom,3)
          if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
            do j=1,NDimI(KPhase)
              NSupPom(j)=NSupPom(j)+2
            enddo
          endif
        else
          do j=1,3
            NSupPom(j)=NCommQ(j,isw,KPhase)
          enddo
        endif
        write(66,'(''mxmod:'',i5)') mxmod
        call SetRealArrayTo(snsinps,mxmod,0.)
        call SetRealArrayTo(cssinps,mxmod,0.)
        call SetRealArrayTo(sncosps,mxmod,0.)
        call SetRealArrayTo(cscosps,mxmod,0.)
        call SetRealArrayTo(snsinpsc,mxmodc,0.)
        call SetRealArrayTo(cssinpsc,mxmodc,0.)
        call SetRealArrayTo(sncospsc,mxmodc,0.)
        call SetRealArrayTo(cscospsc,mxmodc,0.)
        if(kfxi.ne.0) then
          snsinpsx1=0.
          sncospsx1=0.
          snsinpsx2=0.
          sncospsx2=0.
          snsinpsx3=0.
          sncospsx3=0.
          huhu=hj(1)*ux(1,kmodxi,i)+hj(2)*ux(2,kmodxi,i)
     1        +hj(3)*ux(3,kmodxi,i)
        endif
        do ic3=1,NSupPom(3)
          do ic2=1,NSupPom(2)
            do ic1=1,NSupPom(1)
              mc=mc+1
              if(KCommen(KPhase).eq.0.and.(kfsi.gt.0.or.kfxi.gt.0)) then
                if((ic1.gt.NSuper(1,isw,KPhase).and.
     1              (ic2.gt.NSuper(2,isw,KPhase).or.
     2               ic3.gt.NSuper(3,isw,KPhase))).or.
     3             (ic2.gt.NSuper(2,isw,KPhase).and.
     4              ic3.gt.NSuper(3,isw,KPhase))) go to 2490
              endif
              if(ParSup(mp).eq.0.) go to 2490
              l=0
              m=mp
              es=expij*scsup(isw,KPhase)
              if(MagParI.gt.0) then
                mm=mp+DelkaParSup(i)-3
                call Multm(RMag(1,js,1,KPhase),ParSup(mm),smp,3,3,1)
              endif
              MinusReal=.false.
              Imag=.false.
              do n=2,itfi+1
                Imag=.not.Imag
                if(mod(n,2).eq.1) MinusReal=.not.MinusReal
                nrank=TRank(n-1)
                kmod=KModA(n,i)
                if(n.eq.2) then
                  eta=PhiAng
                else if(n.eq.3) then
                  pom=0.
                else if(n.eq.4) then
                  Crealm=Creal
                  Cimagm=Cimag
                endif
                if(kmod.le.0.and.n.ne.2) then
                  l=l+nrank
                  cycle
                endif
                do k=1,nrank
                  m=m+1
                  l=l+1
                  if(n.eq.2) then
                    Clen=hj(l)*ParSup(m)
                  else
                    Clen=HCoefj(l)*ParSup(m)
                  endif
                  if(MinusReal) Clen=-Clen
                  if(n.eq.2) then
                    eta=eta+Clen
                  else if(n.eq.3) then
                    pom=pom+Clen
                  else if(Imag) then
                    Cimagm=Cimagm+Clen
                  else
                    Crealm=Crealm+Clen
                  endif
                enddo
                if(n.eq.2) then
                  m=m+3
                  l=l+1
                else if(n.eq.3) then
                  pom=ExpJana(pom,ich)
                  es=pom*es
                endif
              enddo
              if(KCommen(KPhase).le.0) then
                ppp=eta
                eta=eta+pi2*float(mj)*ParSup(mp+4)
                write(66,'(''eta:'',6f10.6)') ppp,eta,pi2,float(mj),
     1                                        ParSup(mp+4)
                if(ic1.le.NSuper(1,isw,KPhase))
     1            es=es*WGauss(ic1,1,KPhase)*x4length(1,i)
                if(NDim(KPhase).gt.4) then
                  eta=eta+pi2*float(nj)*ParSup(mp+5)
                  if(ic2.le.NSuper(2,isw,KPhase))
     1              es=es*WGauss(ic2,2,KPhase)*x4length(2,i)
                endif
                if(NDim(KPhase).gt.5) then
                  eta=eta+pi2*float(pj)*ParSup(mp+6)
                  if(ic3.le.NSuper(3,isw,KPhase))
     1              es=es*WGauss(ic3,3,KPhase)*x4length(3,i)
                endif
              endif
              sinpsc=es*sin(eta)
              cospsc=es*cos(eta)
              write(66,'(''Sinps,Cosps:'',3f10.6)') es,sin(eta),cos(eta)
              if(itfi.gt.2) then
                cosps=cospsc*Crealm-sinpsc*Cimagm
                sinps=sinpsc*Crealm+cospsc*Cimagm
                cospsc=ParSup(mp)*cospsc
                sinpsc=ParSup(mp)*sinpsc
              else
                cosps=cospsc
                sinps=sinpsc
              endif
              if(CalcDer.and.kmezs.gt.0) then
                jx=NDerModFirst
                if(kfsi.eq.0) then
                  ader(jx)=ader(jx)+cosps
                  if(NCSymmRef(KPhase).eq.1) bder(jx)=bder(jx)+sinps
                  if(MagParI.gt.0) then
                    do l=1,3
                      aderm(l,jx)=aderm(l,jx)+smp(l)*cosps
                      bderm(l,jx)=bderm(l,jx)+smp(l)*sinps
                    enddo
                  endif
                endif
                do k=1,kmezs
                  jx=jx+1
                  jy=jx+1
                  sn=snw(k,mc)
                  cs=csw(k,mc)
                  ader(jx)=ader(jx)+sn*cosps
                  ader(jy)=ader(jy)+cs*cosps
                  if(NCSymmRef(KPhase).eq.1) then
                    bder(jx)=bder(jx)+sn*sinps
                    bder(jy)=bder(jy)+cs*sinps
                  endif
                  if(MagParI.gt.0) then
                    do l=1,3
                      aderm(l,jx)=aderm(l,jx)+smp(l)*sn*cosps
                      bderm(l,jx)=bderm(l,jx)+smp(l)*sn*sinps
                      aderm(l,jy)=aderm(l,jy)+smp(l)*cs*cosps
                      bderm(l,jy)=bderm(l,jy)+smp(l)*cs*sinps
                    enddo
                  endif
                  jx=jy
                enddo
              endif
              if(KCommen(KPhase).le.0) then
                if(ic1.gt.NSuper(1,isw,KPhase).or.
     1             ic2.gt.NSuper(2,isw,KPhase).or.
     2             ic3.gt.NSuper(3,isw,KPhase)) then
                  if(.not.CalcDer) go to 2490
                  jx=NDerModFirst
                  if(ic1.gt.NSuper(1,isw,KPhase)) then
                    pom=2*(ic1-NSupPom(1))+1
                    if(kfxi.ne.0) then
                      if(kmodsi.gt.0) jx=jx+1+2*kmodsi
                      jx=jx+(kmodxi-1)*6+4
                      jy=jx-1
                    else
                      jy=jx+1+2*(kmodsi-NDim(KPhase)+3)
                      if(NDimI(KPhase).gt.1) jx=jy+1
                    endif
                  else if(ic2.gt.NSuper(2,isw,KPhase)) then
                    pom=2*(ic2-NSupPom(2))+1
                    jy=jx+1+2*(kmodsi-NDim(KPhase)+4)
                    jx=jy+1
                  else if(ic3.gt.NSuper(3,isw,KPhase)) then
                    pom=2*(ic3-NSupPom(3))+1
                    jy=jx+1+2*(kmodsi-NDim(KPhase)+5)
                    jx=jy+1
                  endif
                  if(kfxi.ne.2) then
                    ader(jx)=ader(jx)+cosps*.5
                    ader(jy)=ader(jy)+pom*cosps
                  endif
                  if(NCSymmRef(KPhase).eq.1) then
                    bder(jx)=bder(jx)+sinps*.5
                    bder(jy)=bder(jy)+pom*sinps
                  endif
                  go to 2490
                endif
              endif
              write(66,'(''Sinps,Cosps:'',i10,3f10.6)') mp,ParSup(mp),
     1                                                  sinps,cosps
              sinps=ParSup(mp)*sinps
              cosps=ParSup(mp)*cosps
              sinij=sinij+sinps
              cosij=cosij+cosps
              write(66,'(''Sinij,Cosij:'',i10,2f10.6)') mp,sinij,cosij
              if(MagParI.gt.0) then
                do l=1,3
                  cosijm(l)=cosijm(l)+smp(l)*cosps
                  sinijm(l)=sinijm(l)+smp(l)*sinps
                  if(CalcDer) then
                    jx=NDerMagFirst
                    ind=l
                    do j=1,3
                      pom=RMag(ind,js,isw,KPhase)
                      if(NCSymmRef(KPhase).eq.1) then
                        aderm(l,jx)=aderm(l,jx)+cosps*pom
                        bderm(l,jx)=bderm(l,jx)+sinps*pom
                      else
                        if(ZCSymmRef(KPhase).gt.0.) then
                          aderm(l,jx)=aderm(l,jx)+cosps*pom
                        else
                          bderm(l,jx)=bderm(l,jx)+sinps*pom
                        endif
                      endif
                      ind=ind+3
                      jx=jx+1
                    enddo
                    jxp=jx
                    do k=1,MagParI-1
                      ind=l
                      sn=snw(k,mc)
                      cs=csw(k,mc)
                      jx=jxp
                      jy=jx+3
                      do j=1,3
                        pom=RMag(ind,js,isw,KPhase)
                        if(NCSymmRef(KPhase).eq.1) then
                          aderm(l,jx)=aderm(l,jx)+sn*cosps*pom
                          bderm(l,jx)=bderm(l,jx)+sn*sinps*pom
                          aderm(l,jy)=aderm(l,jy)+cs*cosps*pom
                          bderm(l,jy)=bderm(l,jy)+cs*sinps*pom
                        else
                          if(ZCSymmRef(KPhase).gt.0.) then
                            aderm(l,jx)=aderm(l,jx)+sn*cosps*pom
                            aderm(l,jy)=aderm(l,jy)+cs*cosps*pom
                          else
                            bderm(l,jx)=bderm(l,jx)+sn*sinps*pom
                            bderm(l,jy)=bderm(l,jy)+cs*sinps*pom
                          endif
                        endif
                        ind=ind+3
                        jx=jx+1
                        jy=jy+1
                      enddo
                      jxp=jxp+6
                    enddo
                  endif
                enddo
              endif
              if(itfi.gt.2) then
                sinijc=sinijc+sinpsc
                cosijc=cosijc+cospsc
                do k=1,mxmodc
                  sn=snw(k,mc)
                  cs=csw(k,mc)
                  snsinpsc(k)=snsinpsc(k)+sn*sinpsc
                  cssinpsc(k)=cssinpsc(k)+cs*sinpsc
                  sncospsc(k)=sncospsc(k)+sn*cospsc
                  cscospsc(k)=cscospsc(k)+cs*cospsc
                enddo
              endif
              if(.not.CalcDer) go to 2490
              do k=1,mxmod
                sn=snw(k,mc)
                cs=csw(k,mc)
                snsinps(k)=snsinps(k)+sn*sinps
                cssinps(k)=cssinps(k)+cs*sinps
                sncosps(k)=sncosps(k)+sn*cosps
                cscosps(k)=cscosps(k)+cs*cosps
              enddo
              if(kfxi.ne.0) then
                sn1=skfx1(mc)
                sn2=skfx2(mc)
                sn3=skfx3(mc)
                snsinpsx1=snsinpsx1+sn1*sinps
                sncospsx1=sncospsx1+sn1*cosps
                snsinpsx2=snsinpsx2+sn2*sinps
                sncospsx2=sncospsx2+sn2*cosps
                snsinpsx3=snsinpsx3+sn3*sinps
                sncospsx3=sncospsx3+sn3*cosps
              endif
2490          mp=mp+DelkaParSup(i)
            enddo
          enddo
        enddo
        mck=mc
        if(CalcDer) then
          lx=NDerNemodLast
          if(kmodsi.gt.0) lx=lx+1+2*kmodsi
          do k=1,kmodxi
            do l=1,3
              lx=lx+1
              ly=lx+3
              hjl=hj(l)
              if(k.le.kmezx) then
                ader(lx)=ader(lx)-hjl*snsinps(k)
                ader(ly)=ader(ly)-hjl*cssinps(k)
                if(NCSymmRef(KPhase).eq.1) then
                  bder(lx)=bder(lx)+hjl*sncosps(k)
                  bder(ly)=bder(ly)+hjl*cscosps(k)
                endif
              else
                if(kfxi.ne.0) then
                  ader(lx)=ader(lx)-hjl*snsinpsx1
                  if(NCSymmRef(KPhase).eq.1)
     1              bder(lx)=bder(lx)+hjl*sncospsx1
                  if(l.eq.1) then
                    ader(ly)=ader(ly)-huhu*snsinpsx2
                    lyy=ly
                    if(NCSymmRef(KPhase).eq.1)
     1                bder(ly)=bder(ly)+huhu*sncospsx2
                  else if(l.eq.2) then
                    ader(ly)=ader(ly)-huhu*snsinpsx3
                    if(NCSymmRef(KPhase).eq.1)
     1                bder(ly)=bder(ly)+huhu*sncospsx3
                  else
                    ader(ly)=0.
                    bder(ly)=0.
                  endif
                endif
              endif
            enddo
            lx=ly
          enddo
          do k=1,kmodbi
            do l=5,10
              lx=lx+1
              ly=lx+6
              HCoefjl=HCoefj(l)
              ader(lx)=ader(lx)-HCoefjl*sncosps(k)
              ader(ly)=ader(ly)-HCoefjl*cscosps(k)
              if(NCSymmRef(KPhase).eq.1) then
                bder(lx)=bder(lx)-HCoefjl*snsinps(k)
                bder(ly)=bder(ly)-HCoefjl*cssinps(k)
              endif
            enddo
            lx=ly
          enddo
          imag=.false.
          MinusReal=.true.
          MinusImag=.true.
          lp=10
          do n=3,itfi
            nrank=TRank(n)
            if(mod(n,2).eq.1) then
              MinusReal=.not.MinusReal
            else
              MinusImag=.not.MinusImag
            endif
            imag=.not.imag
            kmod=KModA(n+1,i)
            if(kmod.le.0) go to 3900
            do k=1,kmod
              if(imag) then
                pomrx=snsinpsc(k)
                pomry=cssinpsc(k)
                pomix=sncospsc(k)
                pomiy=cscospsc(k)
              else
                pomrx=sncospsc(k)
                pomry=cscospsc(k)
                pomix=snsinpsc(k)
                pomiy=cssinpsc(k)
              endif
              if(MinusReal) then
                pomrx=-pomrx
                pomry=-pomry
              endif
              if(MinusImag) then
                pomix=-pomix
                pomiy=-pomiy
              endif
              ll=lp
              do l=1,nrank
                ll=ll+1
                lx=lx+1
                ly=lx+nrank
                HCoefjl=HCoefj(ll)
                ader(lx)=ader(lx)+HCoefjl*pomrx
                ader(ly)=ader(ly)+HCoefjl*pomry
                if(NCSymmRef(KPhase).eq.1) then
                  bder(lx)=bder(lx)+HCoefjl*pomix
                  bder(ly)=bder(ly)+HCoefjl*pomiy
                endif
              enddo
              lx=ly
            enddo
3900        lp=lp+nrank
          enddo
        endif
      endif
      go to 9999
9800  ErrFlag=1
      go to 9999
9810  ErrFlag=2
      go to 9999
9999  write(66,'(''Sinij,Cosij:'',2f10.6)') sinij,cosij
      return
      end
