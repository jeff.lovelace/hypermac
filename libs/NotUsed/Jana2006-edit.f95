!      call JacobiComplex(PMat,3,EVal,EVec,NRot)
!      write(6,'(i5)') NRot
!      write(6,'(2f10.6,'' | '',6f10.6)')(EVal(i),
!     1                                   (EVec(j,i),j=1,3),i=1,3)
!      write(6,'(80(''-''))')
!      QMat=0.
!      QMat(1,1)=-1.
!      QMat(2,2)= 1.
!      QMat(3,3)=-1.
!      call JacobiComplex(QMat,3,EVal,EVec,NRot)
!      write(6,'(i5)') NRot
!      write(6,'(2f10.6,'' | '',6f10.6)')(EVal(i),
!     1                                   (EVec(j,i),j=1,3),i=1,3)
!      write(6,'(80(''-''))')
!      call MultmC(PMat,QMat,RMat,3,3,3)
!      call JacobiComplex(RMat,3,EVal,EVec,NRot)
!      write(6,'(6f10.3)')((RMat(i,j),j=1,3),i=1,3)
!      write(6,'(2f10.3,'' | '',6f10.6)')(EVal(i),
!     1                                   (EVec(j,i),j=1,3),i=1,3)
!      write(6,'(80(''-''))')
!      return
!      end
!      subroutine UpravStokes
!      include 'fepc.cmn'
!      include 'basic.cmn'
!      character*512 Veta,Radka
!      character*80  t80,p80
!      logical Cist
!      Veta='C:\Structures\Jana2006\Work\Magnetic\Test_of_symbols'
!      open(40,file=Veta(:idel(Veta))//'\Input.txt')
!      open(41,file=Veta(:idel(Veta))//'\Output.txt')
!      Cist=.false.
!      igrp=1
c1100  read(40,FormA,end=2000) Veta
!      do i=1,idel(Veta)
!        if(ichar(Veta(i:i)).eq.9) Veta(i:i)=' '
!      enddo
!      k=0
!      call Kus(Veta,k,t80)
!      if(.not.Cist) then
!        if(t80(1:1).eq.'1') Cist=.true.
!        p80=t80
!      endif
!      if(.not.Cist) go to 1100
!      if(t80.ne.p80) then
!        write(41,FormA)
!        p80=t80
!        Cislo=t80
!        call Posun(Cislo,0)
!        read(Cislo,FormI15) igrp
!      endif
!      Radka(1:)=t80(:idel(t80))
!      call Kus(Veta,k,t80)
!      Radka(6:)=t80(:idel(t80))
!      call Kus(Veta,k,t80)
!      Radka(21:)=t80(:idel(t80))
!      call Kus(Veta,k,t80)
!      Radka(31:)=t80(:idel(t80))
!      call Kus(Veta,k,t80)
!      call Kus(Veta,k,t80)
!      if(igrp.le.74) then
!        Cislo=t80
!        call Posun(Cislo,1)
!        read(Cislo,'(f15.1)') pom
!        if(pom.lt.float(igrp)) go to 1100
!      endif
!      Radka(46:)=t80(:idel(t80))
!      call Kus(Veta,k,t80)
!      Radka(56:)=t80(:idel(t80))
!      Radka(81:)=Veta(k+1:)
!      write(41,FormA) Radka(:idel(Radka))
!      go to 1100
c2000  close(40)
!      close(41)
!      return
!      end
!      subroutine TestRef
!      use Basic_mod
!      include 'fepc.cmn'
!      include 'basic.cmn'
!      integer iha(6,3000)
!      real RI(3000),RS(3000)
!      call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
!      n=0
c1100  n=n+1
!      read(91,Format91,err=1150,end=1150) iha(1:6,n),RI(n),RS(n)
c!      write(6,'(6i5,2f10.1)') iha(1:6,n),RI(n),RS(n)
!      go to 1100
c1150  call CloseIfOpened(91)
!      n=n-1
!      do i=1,n
c!        write(6,'(6i5,2f10.1)') iha(1:6,i),RI(i),RS(i)
!        if(iha(4,i).ne.0.or.iha(5,i).eq.0.or.
!     1     iabs(iha(4,i))+iabs(iha(5,i))+iabs(iha(6,i)).eq.0) cycle
c!          write(6,'(6i5,2f10.1)') iha(1:6,i),RI(i),RS(i)
!        do j=1,n
!          if(iha(4,j).ne.0.or.iha(6,j).eq.0.or.
!     1       iabs(iha(4,j))+iabs(iha(5,j))+iabs(iha(6,j)).eq.0) cycle
!          if(iha(1,i).eq.iha(1,j).and.iha(3,i).eq.iha(3,j)) then
!            k=iha(2,j)-iha(2,i)
!            if(iabs(k).ne.1.or.
!     1         iha(5,i)-k.ne.iha(5,j).or.iha(6,i)-k.ne.iha(6,j)) cycle
!            write(6,'(6i5,2f10.1)') iha(1:6,i),RI(i),RS(i)
!            write(6,'(6i5,2f10.1)') iha(1:6,j),RI(j),RS(j)
!            write(6,'(1x,49(''-''))')
!          endif
!        enddo
!      enddo
!      return
!      end
!      subroutine PrepisHKL
!      use Basic_mod
!      include 'fepc.cmn'
!      include 'basic.cmn'
!      character*256 Veta
!      integer ihp(6)
!      call OpenFile(44,
!     1  'c:\Test structures\Marein Rahn\15K_total_HoCu.hkl',
!     2  'formatted','unknown')
!      call OpenFile(45,
!     1  'c:\Test structures\Marein Rahn\15K_total_HoCu_1.hkl',
!     2  'formatted','unknown')
c1100  read(44,FormA,end=2000) Veta
!      read(Veta,100) ihp
!      ihp(1)=2*ihp(1)+ihp(4)+ihp(5)
!      ihp(2)=2*ihp(2)+ihp(4)+ihp(6)
!      ihp(3)=2*ihp(3)+ihp(5)+ihp(6)
!      write(Cislo,100) ihp(1:3)
!      write(45,FormA) Cislo(:12)//Veta(25:idel(Veta))
!      go to 1100
c2000  call CloseIfOpened(44)
!      call CloseIfOpened(45)
!      return
c100   format(6i4)
!      end
!      subroutine PrevodM90
!      character*80 Veta
!      dimension ih(4),ihp(4)
!      real h(3),hp(3)
!      open(44,file='c:\tmp\test.m90')
!      open(45,file='c:\tmp\test_new.m90')
c1100  read(44,'(a)',end=1500) Veta
!      read(Veta,'(4i4)') ih
!      ihp=ih
!      if(ih(4).eq.1) then
!        ih(4)=-2
!        ih(1)=ih(1)+1
!      else if(ih(4).eq.-1) then
!        ih(4)= 2
!        ih(1)=ih(1)-1
!      else if(ih(4).eq. 2) then
!        ih(4)= 1
!        ih(3)=ih(3)+2
!      else if(ih(4).eq.-2) then
!        ih(4)=-1
!        ih(3)=ih(3)-2
!      endif
!      write(Veta(1:16),'(4i4)') ih
!      write(45,'(a)') Veta(:idel(Veta))
!      do i=1,3
!        h(i)=float(ih(i))
!        hp(i)=float(ihp(i))
!      enddo
c!      hp(1)=hp(1)+float(ihp(4))*.2
c!      hp(3)=hp(3)+float(ihp(4))*.8
c!      h(1)=h(1)+float(ih(4))*.4
c!      h(3)=h(3)-float(ih(4))*.4
c!      write(6,'(4i4,3f10.3)') ih,h,ihp,hp
c!      write(6,'('' -----------------------------'')')
!      go to 1100
c1500  close(44)
!      close(45)
!      return
!      end
!      subroutine Posunt0
!      use Atoms_mod
!      use Basic_mod
!      use Molec_mod
!      include 'fepc.cmn'
!      include 'basic.cmn'
!      do i=1,NAtInd
!        if(kmoda(2,i).gt.0) then
!          do j=1,3
!            ux(j,1,i)=-ux(j,1,i)
!            uy(j,1,i)=-uy(j,1,i)
!          enddo
!        endif
!        if(kmoda(3,i).gt.0) then
!          do j=1,6
!            bx(j,1,i)=-bx(j,1,i)
!            by(j,1,i)=-by(j,1,i)
!          enddo
!        endif
!      enddo
!      do i=1,NMolec
!        do k=1,mam(i)
!          ji=k+mxp*(i-1)
!          if(kmodm(2,ji).gt.0) then
!            do j=1,3
!              utx(j,1,ji)=-utx(j,1,ji)
!              uty(j,1,ji)=-uty(j,1,ji)
!              urx(j,1,ji)=-urx(j,1,ji)
!              ury(j,1,ji)=-ury(j,1,ji)
!            enddo
!          endif
!          if(kmodm(3,ji).gt.0) then
!            do j=1,6
!              ttx(j,1,ji)=-ttx(j,1,ji)
!              tty(j,1,ji)=-tty(j,1,ji)
!              tlx(j,1,ji)=-tlx(j,1,ji)
!              tly(j,1,ji)=-tly(j,1,ji)
!            enddo
!            do j=1,9
!              tsx(j,1,ji)=-tsx(j,1,ji)
!              tsy(j,1,ji)=-tsy(j,1,ji)
!            enddo
!          endif
!        enddo
!      enddo
!      call iom40(1,0,fln(:ifln)//'.m40')
!      call iom40(0,0,fln(:ifln)//'.m40')
!      return
!      end

      subroutine PrevodHK6
      character*80 Veta
      dimension ih(6)
      open(44,file='c:\Test structures\Mn4Si7\Mn4Si7_2_0m.hk6')
      open(45,file='c:\Test structures\Mn4Si7\Real_indices.hkl')
1100  read(44,'(a)',end=1500) Veta
      read(Veta,'(6i4,2f8.0)') ih,f,sf
      pl=(float(ih(3))+float(ih(4))*.125)*1.6
      pl=(float(ih(3))+float(ih(4))*.125)*1.6
      d=4./8.
      do i=-4,4
        pom=pl+d*float(i)
        if(abs(pom-anint(pom)).lt..001) then
          write(6,'(4i5,'' | '',4i5,'' | '',2f12.3)')
     1      ih(1:4),ih(1:2),nint(pom),i,f,sf
            exit
        endif
      enddo
      write(45,'(3f8.3,2f10.2)') float(ih(1)),float(ih(2)),pl,f,sf
      go to 1100
1500  close(44)
      close(45)
      return
      end
      subroutine TLSTest
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real :: TrMat(81),ttp(6),tlp(6),tpisq=19.7392088,tsp(9)
      if(NMolec.le.0) go to 9999
      call srotb(TrToOrtho(1,1,1),TrToOrtho(1,1,1),TrMat)
      call MultM(Trmat,tt,ttp,6,6,1)
      do i=1,6
        ttp(i)=ttp(i)/tpisq
      enddo
      write(6,'(3f10.6)') tt(1,1),tt(4,1),tt(5,1)
      write(6,'(3f10.6)') tt(4,1),tt(2,1),tt(6,1)
      write(6,'(3f10.6)') tt(5,1),tt(6,1),tt(3,1)
      write(6,'('' -----------------------------------------------'')')
      write(6,'(3f10.6)') ttp(1),ttp(4),ttp(5)
      write(6,'(3f10.6)') ttp(4),ttp(2),ttp(6)
      write(6,'(3f10.6)') ttp(5),ttp(6),ttp(3)
      write(6,'('' ==============================================='')')
      call MultM(Trmat,tl,tlp,6,6,1)
      do i=1,6
        tlp(i)=tlp(i)/tpisq
      enddo
      write(6,'(3f10.6)') tl(1,1),tl(4,1),tl(5,1)
      write(6,'(3f10.6)') tl(4,1),tl(2,1),tl(6,1)
      write(6,'(3f10.6)') tl(5,1),tl(6,1),tl(3,1)
      write(6,'('' -----------------------------------------------'')')
      write(6,'(3f10.6)') tlp(1),tlp(4),tlp(5)
      write(6,'(3f10.6)') tlp(4),tlp(2),tlp(6)
      write(6,'(3f10.6)') tlp(5),tlp(6),tlp(3)
      write(6,'('' ==============================================='')')
      call srotss(TrToOrtho(1,1,1),TrToOrtho(1,1,1),TrMat)
      call MultM(Trmat,ts,tsp,9,9,1)
      do i=1,9
        tsp(i)=tsp(i)/tpisq
      enddo
      write(6,'(3f10.6)') ts(1,1),ts(4,1),ts(7,1)
      write(6,'(3f10.6)') ts(2,1),ts(5,1),ts(8,1)
      write(6,'(3f10.6)') ts(3,1),ts(6,1),ts(9,1)
      write(6,'('' -----------------------------------------------'')')
      write(6,'(3f10.6)') tsp(1),tsp(2),tsp(3)
      write(6,'(3f10.6)') tsp(4),tsp(5),tsp(6)
      write(6,'(3f10.6)') tsp(7),tsp(8),tsp(9)
      write(6,'('' ==============================================='')')
9999  return
      end
      subroutine CrlSigCountSigStat
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(2),ih(3)
      dimension SigCount(:),SigCountN(:),SigStat(:),Ri(:)
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(4) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot'/)
      allocatable SigCount,SigCountN,SigStat,Ri
      Tiskne=.false.
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m89','formatted','unknown')
      n=0
1100  read(ln,100,end=1200) ih
      n=n+1
      go to 1100
1200  rewind ln
      allocate(SigCount(n),SigCountN(n),SigStat(n),Ri(n))
      j=0
      PMax=0.
1300  read(ln,100,end=1400) ih,pom,pomc,poms
      j=j+1
      if(j.gt.n) go to 1300
      Ri(j)=pom
      SigCount(j)=pomc
      SigStat(j)=poms
      PMax=max(SigCount(j),SigStat(j),PMax)
      go to 1300
1400  close(ln)
      Suma1=0.
      Suma2=0.
      do i=1,n
        Suma1=Suma1+Ri(i)**4
        Suma2=Suma2+(SigStat(i)**2-SigCount(i)**2)*RI(i)**2
      enddo
!      write(Veta,'(2e15.6)') Suma1,Suma2
      pom=Suma2/Suma1
      write(Veta,'(2f15.8)') pom,sqrt(abs(pom))/2.
      call FeWinMessage('Vysledek:'//Veta,' ')
      do i=1,n
        SigCountN(i)=sqrt(SigCount(i)**2+pom*Ri(i)**2)
        PMax=max(SigCountN(i),PMax)
      enddo
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        endif
        if(i.le.4) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        rewind ln
        xomn=0.
        xomx=PMax*1.1
        yomn=0.
        yomx=PMax*1.1
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.true.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'sig(count)')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'sig(stat)')
        xpp(3)=0.
        do i=1,n
          xpp(1)=SigCount(i)
          xpp(2)=SigStat(i)
          call FeXf2X(xpp,xo)
!          call FeCircleOpen(xo(1),xo(2),3.,White)
          xpp(1)=SigCountN(i)
          xpp(2)=SigStat(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,Green)
        enddo
2300    xpp(1)=0.
        xpp(2)=0.
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=xomx
        xpp(2)=yomx
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEVent(id,ich)
      if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  if(allocated(SigCount))
     1  deallocate(SigCount,SigCountN,SigStat,Ri)
      return
100   format(3i4,3f9.1)
      end
      subroutine CrlIoSigIoPlots
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(3)
      real IObs,sIObs,IObsMax,sIObsMax
      character*256 t256
      character*17 :: Labels(4) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot'/)
      AllowResizing=.true.
      RatioMin=10.
      Tiskne=.false.
      ln=0
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        endif
        if(i.le.4) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ln=NextLogicNumber()
      call OpenDatBlockM90(ln,1,fln(:ifln)//'.m90')
      IObsMax=0.
      sIObsMax=0.
2000  read(ln,format91,end=2100)(j,i=1,maxNDim),IObs,sIObs
      if(IObs.gt.RatioMin*sIObs) then
!        IObsMax=max(IObsMax,1./sqrt(IObs))
        IObsMax=max(IObsMax,IObs)
        sIObsMax=max(sIObsMax,sIObs/IObs)
      endif
      go to 2000
2100  close(ln)
      call OpenDatBlockM90(ln,1,fln(:ifln)//'.m90')
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        xomn=0.
        xomx=1.01
        yomn=0.
        yomx=sIobsMax*1.01
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'I')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'sig(I)/I')
        xpp(3)=0.
2200    read(ln,format91,end=2300)(j,i=1,maxNDim),IObs,sIObs
        if(IObs.le.RatioMin*sIObs) go to 2200
!        xpp(1)=1./(sqrt(IObs)*IObsMax)
        xpp(1)=IObs/IObsMax
        xpp(2)=sIObs/IObs
        call FeXf2X(xpp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,White)
        go to 2200
2300    xpp(1)=0.
        xpp(2)=0.
!        call FeXf2X(xpp,xo)
!        xp(1)=xo(1)
!        yp(1)=xo(2)
!        xpp(1)=xomx
!        xpp(2)=yomx
!        call FeXf2X(xpp,xo)
!        xp(2)=xo(1)
!        yp(2)=xo(2)
!        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEVent(id,ich)
      if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('graph',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  return
      end

!      subroutine FeUnlockWindowResize(hwndp)
!      use Jana_windows
!      include 'fepc.cmn'
!      integer hwndp
!      logical lpom
!      i=WS_THICKFRAME
!      i=ior(WS_OVERLAPPED,
!     1    ior(WS_THICKFRAME,
!     2      ior(WS_CAPTION,
!     3        ior(WS_SYSMENU,
!     4          ior(WS_MINIMIZEBOX,WS_MAXIMIZEBOX)))))
!      go to 2000
!      entry FeLockWindowResize(hwndp)
!      i=0
!      i=ior(WS_OVERLAPPED,
!     2    ior(WS_CAPTION,
!     3        ior(WS_SYSMENU,WS_MINIMIZEBOX)))
!2000  i=SetWindowLongA(carg(hwndp),carg(GWL_STYLE),carg(i))
!      return
!      end
!      subroutine TestPolynomials
!      dimension fx(50),sfx(50),XGauss(1000),WGauss(1000)
!      character*80 Veta
!      call gauleg(-1.,1.,XGauss,WGauss,1000)
!      fx=1.
!      n=12
!      m=13
!      suma1=0.
!      suma2=0.
!      do i=1,1000
!        x=XGauss(i)
!        y=Chebev(fx,sfx,x,50)
!        suma1=suma1+WGauss(i)*sfx(n)*sfx(m)/sqrt(1.-x**2)
!        suma2=suma2+WGauss(i)*sfx(n)*sfx(m)
!      enddo
!      write(Veta,'(2f10.5)') Suma1,Suma2
!      call FeWinMessage(Veta,' ')
!      return
!      end
      subroutine RFacTest
      character*80 Veta
      real Io,Ic
      ln=NextLogicNumber()
      open(ln,file='ListFoFc.dat')
      Suma1=0.
      Suma2=0.
      Suma3=0.
      Suma4=0.
      n=0
1100  read(ln,'(12x,3f9.2)',end=2000) Ic,Io,sIo
      Fo=sqrt(max(Io,0.))
      Fc=sqrt(abs(Ic))
      if(Io.le..01*sIo) then
        sFo=sqrt(sIo)/(.2)
      else
        sFo=.5/Fo*sIo
      endif
      wt=1./(sFo**2+(.02*Fo)**2)
      wti=1./(sIo**2+.0016*Io**2)
      n=n+1
      Suma1=Suma1+abs(Fo-Fc)
      Suma2=Suma2+Fo
      Suma3=Suma3+wt*(Fo-Fc)**2
      Suma4=Suma4+wt*Fo**2
      Suma5=Suma5+wti*(Io-Ic)**2
      Suma6=Suma6+wti*Io**2
      go to 1100
2000  write(Veta,'(3f10.6,i10)') Suma1/Suma2,sqrt(Suma3/Suma4),
     1                           sqrt(Suma5/Suma6),n
      call FeWinMessage(Veta,' ')
      return
      end
      subroutine DSetMol
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension px(12),xp(3)
      integer PrvKiMol
      call SetRealArrayTo(der(PrvniKiAtMol),
     1                    PosledniKiAtMol-PrvniKiAtMol+1,0.)
      do KPh=1,NPhase
        KPhase=KPh
        ibk=NAtMolFrAll(KPh)-1
        ia=NAtPosFrAll(KPh)-1
        do i=NMolecFrAll(KPh),NMolecToAll(KPh)
          ibp=ibk+1
          ibk=ibk+iam(i)
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            PrvKiMol=PrvniKiMolekuly(ji)
            call SetRealArrayTo(der(PrvKiMol),DelkaKiMolekuly(ji),0.)
            do ib=ibp,ibk
              ia=ia+1
              if(ai(ib).le.0.) cycle
              mb=ib-NAtMolFr(1,1)+1
              kipa=PrvniKiAtomu(ia)
              kipb=PrvniKiAtomu(ib)
              if(TypeModFun(ia).eq.1) then
                kmodxia=kmodao(2,ia)
                kmodbia=kmodao(3,ia)
              else
                kmodxia=KModA(2,ia)
                kmodbia=KModA(3,ia)
              endif
              idf=kipb-kipa
              kipm=PrvKiMol
              der(kipa+idf)=der(kipa+idf)+der(kipa)*aimol(ji)
              der(kipm)=der(kipm)+der(kipa)*ai(ib)
              do l=1,3
                xp(l)=x(l,ib)-xm(l,i)
              enddo
              kipa=kipa+1
              kipm=kipm+1
              call multm(drotf(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              kipm=kipm+1
              call multm(drotc(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              kipm=kipm+1
              call multm(drotp(1,ji),xp,px,3,3,1)
              call cultm(px,der(kipa),der(kipm),1,3,1)
              call cultm(der(kipa),RotMol(1,ji),der(kipa+idf),1,3,3)
              kipm=kipm+1
              do l=1,3
                der(kipm)=der(kipm)+der(kipa)
                kipa=kipa+1
                kipm=kipm+1
              enddo
!              if(itf(ib).ne.0) then
                if(itf(ia).eq.2) then
                  if(itf(ib).ge.2.or.itf(ib).eq.0) then
                    kipm=kipm-6
                    call cultm(der(kipa),rotb(1,ji),der(kipa+idf),1,6,6)
                    call multm(drotbf(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+1
                    call multm(drotbc(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+1
                    call multm(drotbp(1,ji),beta(1,ib),px,6,6,1)
                    call cultm(px,der(kipa),der(kipm),1,6,1)
                    kipm=kipm+4
                  else if(itf(ib).eq.1) then
                    call cultm(der(kipa),prcp(1,iswa(ia),KPhase),
     1                         der(kipa+idf),1,6,1)
                  endif
                else
                  der(kipa+idf)=der(kipa+idf)+der(kipa)
                endif
!              else
              if(itf(ib).eq.0) then
                call multm(der(kipa),rotb(1,ji),px,1,6,6)
                do l=1,6
                  der(kipm)=der(kipm)+px(l)
                  kipm=kipm+1
                enddo
                call cultm(px,tztl(1,mb),der(kipm),1,6,6)
                kipm=kipm+6
                call cultm(px,tzts(1,mb),der(kipm),1,6,9)
              endif
              if(ktls(i).le.0) then
                kipm=PrvKiMol+7
              else
                kipm=PrvKiMol+28
              endif
              kipa=kipa+6
              kipb=kipa+idf
              kmodib=1+2*KModA(1,ib)
              if(kmodib.eq.1) kmodib=kmodib-1
              kmodp=1+2*KModM(1,ji)
              if(KModA(1,ia).gt.0) then
                do l=1,1+2*KModA(1,ia)
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+der(kipa)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipm)=der(kipm)+der(kipa)
                    kipm=kipm+1
                  endif
                  kipa=kipa+1
                enddo
              endif
              kmodp=KModM(2,ji)
              if(kmodp.gt.0) then
                kx=PrvniKiAtomu(ib)+1
                kipt=kipm
                kipr=kipt+6*kmodp
              else
                kipr=kipm
              endif
              kmodib=KModA(2,ib)
              do l=1,kmodxia
                call multm(der(kipa),RotMol(1,ji),px,1,3,3)
                kipa=kipa+3
                call multm(der(kipa),RotMol(1,ji),px(4),1,3,3)
                kipa=kipa+3
                do m=1,6
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+px(m)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipt)=der(kipt)+px(m)
                    kipt=kipt+1
                  endif
                enddo
                if(kmodp.ge.l) then
                  call cultm(px,durdr(1,ia),der(kipr),1,3,3)
                  kipr=kipr+3
                  call cultm(px(4),durdr(1,ia),der(kipr),1,3,3)
                  kipr=kipr+3
                  call cultm(px,durdx(1,l,ji),der(kx),1,6,3)
                endif
              enddo
              kipm=kipr
              kmodp=KModM(3,ji)
              kmodib=KModA(3,ib)
              if(kmodp.gt.0) then
                kipt=kipm
                kipl=kipt+12*kmodp
                kips=kipl+12*kmodp
                kipss=kips
              else
                kips=kipm
              endif
              do l=1,kmodbia
                call multm(der(kipa),rotb(1,ji),px,1,6,6)
                kipa=kipa+6
                call multm(der(kipa),rotb(1,ji),px(7),1,6,6)
                kipa=kipa+6
                do m=1,12
                  if(kmodib.ge.l) then
                    der(kipb)=der(kipb)+px(m)
                    kipb=kipb+1
                  endif
                  if(kmodp.ge.l) then
                    der(kipt)=der(kipt)+px(m)
                    kipt=kipt+1
                  endif
                enddo
                if(kmodp.ge.l) then
                  call cultm(px,   tztl(1,mb),der(kipl),1,6,6)
                  kipl=kipl+6
                  call cultm(px(7),tztl(1,mb),der(kipl),1,6,6)
                  kipl=kipl+6
                  call cultm(px,   tzts(1,mb),der(kips),1,6,9)
                  kips=kips+9
                  call cultm(px(7),tzts(1,mb),der(kips),1,6,9)
                  kips=kips+9
                endif
              enddo
              kipm=kips
              if(KModA(1,ib).gt.0.or.KModA(2,ib).gt.0.or.
     1           KModA(3,ib).gt.0) der(kipb)=der(kipb)+der(kipa)
              if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1           KModM(3,ji).gt.0) der(kipm)=der(kipm)+der(kipa)
            enddo
          enddo
          if(kpoint(i).le.1) go to 2850
          iami=iam(i)/KPoint(i)
          do j=ibp,ibp+iami-1
            ic=j
            kbp=PrvniKiAtomu(j)-1
            do m=2,npoint(i)
              if(ipoint(m,i).le.0) cycle
              ic=ic+iami
              kcp=PrvniKiAtomu(ic)-1
              kc=kcp+1
              kb=kbp+1
              der(kb)=der(kb)+der(kc)
              kc=kc+1
              kb=kb+1
              call cultm(der(kc),rpoint(1,m,i),der(kb),1,3,3)
              kc=kc+3
              kb=kb+3
              if(itf(ic).gt.1) then
                call cultm(der(kc),tpoint(1,m,i),der(kb),1,6,6)
              else
                der(kb)=der(kb)+der(kc)
              endif
            enddo
          enddo
2850      ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            kimp=PrvniKiMolekuly(ji)+4
            if(AtTrans(ji).ne.' ') then
              k=ktat(atom,NAtInd,AtTrans(ji))
              if(k.le.0) cycle
              kipa=PrvniKiAtomu(k)+1
              call AddVek(der(kimp),der(kipa),der(kipa),3)
            endif
          enddo
        enddo
      enddo
      return
      end
      subroutine CislujCIF
      character*256 Veta
      open(40,file='c:\Jana2006\cif\cif_restrain.dat')
      open(41,file='c:\Jana2006\cif\cif_restrain_ocislovany.dat')
      n=0
1100  read(40,'(a)',end=5000) Veta
      n=n+1
      write(41,'(2i5,2x,a)') n,24,Veta(:idel(Veta))
      go to 1100
5000  close(40)
      close(41)
      return
      end
      subroutine UdelejMatice
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta,StSymm,StMat
      character*80 :: StSymmA(24) = (/
     1     '(x,y,z,mx,my,mz)             ',
     2     '(-y,x-y,z,-my,mx-my,mz)      ',
     3     '(-x+y,-x,z,-mx+my,-mx,mz)    ',
     4     '(-x,-y,z+1/2,-mx,-my,mz)     ',
     5     '(y,-x+y,z+1/2,my,-mx+my,mz)  ',
     6     '(x-y,x,z+1/2,mx-my,mx,mz)    ',
     7     '(y,x,-z,my,mx,-mz)           ',
     8     '(x-y,-y,-z,mx-my,-my,-mz)    ',
     9     '(-x,-x+y,-z,-mx,-mx+my,-mz)  ',
     a     '(-y,-x,-z+1/2,-my,-mx,-mz)   ',
     1     '(-x+y,y,-z+1/2,-mx+my,my,-mz)',
     2     '(x,x-y,-z+1/2,mx,mx-my,-mz)  ',
     3     '(-x,-y,-z,mx,my,mz)          ',
     4     '(y,-x+y,-z,-my,mx-my,mz)     ',
     5     '(x-y,x,-z,-mx+my,-mx,mz)     ',
     6     '(x,y,-z+1/2,-mx,-my,mz)      ',
     7     '(-y,x-y,-z+1/2,my,-mx+my,mz) ',
     8     '(-x+y,-x,-z+1/2,mx-my,mx,mz) ',
     9     '(-y,-x,z,my,mx,-mz)          ',
     a     '(-x+y,y,z,mx-my,-my,-mz)     ',
     1     '(x,x-y,z,-mx,-mx+my,-mz)     ',
     2     '(y,x,z+1/2,-my,-mx,-mz)      ',
     3     '(x-y,-y,z+1/2,-mx+my,my,-mz) ',
     4     '(-x,-x+y,z+1/2,mx,mx-my,-mz) '/)
      real RM(2,2,24)
      logical EqIgCase
      open(40,file='c:\Jana2006\symmdat\GM.txt')
      open(41,file='c:\Jana2006\symmdat\GM.mat')
      n=0
      RM=0.
1100  read(40,FormA,end=5000) Veta
      if(Veta.eq.' ') go to 1100
      StSymm=' '
      StMat=' '
      m=0
      do i=1,idel(Veta)
1200    if(m.gt.0) then
          if(m.eq.1) then
            StSymm=StSymm(:idel(StSymm))//Veta(i:i)
          else
            StMat=StMat(:idel(StMat))//Veta(i:i)
          endif
          if(Veta(i:i).eq.')')  m=-m
        else if(Veta(i:i).eq.'(') then
          m=iabs(m)+1
          go to 1200
        endif
      enddo
      do n=1,24
        if(EqIgCase(StSymm,StSymmA(n))) go to 1300
      enddo
      n=1
1300  do i=1,idel(StMat)
        if(StMat(i:i).eq.'('.or.StMat(i:i).eq.')'.or.StMat(i:i).eq.','
     1                      .or.StMat(i:i).eq.'/') StMat(i:i)=' '
      enddo
      read(StMat,*)((RM(i,j,n),j=1,2),i=1,2)
      go to 1100
5000  do n=1,24
        write(41,'(i5,9f8.4/5x,9f8.4)') n,((RM(i,j,n),0.,j=1,2),i=1,2)
      enddo
      close(40)
      close(41)
      return
      end
      subroutine XDRFac
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      open(40,file='D:\Structures\Jana2006\Work\Jacob\XD\xd_lsm.out')
      RNum=0.
      RDen=0.
1100  read(40,FormA,end=9999) Veta
      if(Veta(1:18).eq.'   NO.   H   K   L') go to 1200
      go to 1100
1200  read(40,FormA,end=9999) Veta
      if(Veta(1:18).eq.'   NO.   H   K   L') go to 1200
      if(Veta.eq.' '.or.Veta(1:4).eq.'----') go to 1200
      if(Veta(1:10).eq.' Condition') go to 2000
      read(Veta,'(i6,3i4,f7.4,i3,3e12.5)') i,i,i,i,pom,i,Fobs,Fcalc,
     1                                      Fdel
      RNum=RNum+abs(Fdel)
      RDen=RDen+Fobs
      go to 1200
2000  write(6,'(f10.6)') RNum/RDen
9999  return
      end
      subroutine XDFF
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      real FF(8)
      open(40,file='D:\Structures\Jana2006\Work\Jacob\XD\xd_lsm.out')
      open(41,file='XDFF.dat')
1100  read(40,FormA,end=5000) Veta
      if(Veta(2:5).eq.'CONF') then
        do i=1,5
          read(40,'(8f10.5)') FF
          write(41,'(8f9.4)') FF
        enddo
        write(41,'(8f9.4)')
      endif
      go to 1100
5000  close(40)
      close(41)
9999  return
      end
      subroutine PrevodNotarius
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      open(40,file='c:\Notarius-eclipse\source\NotariusLib.f95')
      open(41,file=
     1 'c:\Notarius-eclipse\source\NotariusLib-new.f95')
1100  read(40,FormA,end=5000) Veta
      call Latin2ToWindows(Veta)
      write(41,FormA) Veta(:idel(Veta))
      go to 1100
5000  close(40)
      close(41)
      return
      end
      subroutine PrevodReprezentaci
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      real rmr(3,3,48)
      integer ip(1),rmp(3,3,48),rms(3,3,48)
      logical EqIV
      lni=NextLogicNumber()
      call OpenFile(lni,'c:\Jana2006\symmdat\pgroup.dat','formatted',
     1              'old')
      lnn=NextLogicNumber()
      call OpenFile(lnn,'c:\Jana2006\symmdat\pgroup.new','formatted',
     1              'unknown')
      lnr=NextLogicNumber()
      call OpenFile(lnr,'c:\Jana2006\symmdat\Reprezentace_pro_Janu.txt',
     1              'formatted','unknown')
      read(lni,FormA) Veta
      write(lnn,FormA) Veta(:idel(Veta))
      do i=1,40
        read(lni,FormA) Veta
        write(6,'('' Grupa: '',a)') Veta(:idel(Veta))
        write(lnn,FormA) Veta(:idel(Veta))
        k=0
        call Kus(Veta,k,Cislo)
        call Kus(Veta,k,Cislo)
        call StToInt(Veta,k,ip,1,.false.,ich)
        ns=ip(1)
        do j=1,ns
          read(lni,FormA) Veta
          write(lnn,FormA) Veta(:idel(Veta))
          read(Veta,'(9i3)')((rms(ii,jj,j),ii=1,3),jj=1,3)
          write(6,'(3i3)')((rms(ii,jj,j),jj=1,3),ii=1,3)
          write(6,'('' --------------------------'')')
        enddo
        read(lni,FormA) Veta
        write(lnn,FormA) Veta(:idel(Veta))
        k=0
        call StToInt(Veta,k,ip,1,.false.,ich)
        do j=1,ip(1)
          read(lni,FormA) Veta
          write(lnn,FormA) Veta(:idel(Veta))
        enddo
1100    read(lni,FormA) Veta
        if(Veta(1:6).ne.'------') go to 1100
        read(lnr,FormA) Veta
1200    read(lnr,FormA) Veta
        if(Veta(1:6).eq.'------') then
          write(lnn,'(80(''-''))')
          cycle
        else
          k=0
          call Kus(Veta,k,Cislo)
          call StToInt(Veta,k,ip,1,.false.,ich)
          nd=ip(1)
          write(lnn,'(2x,a10,''Dimension:'',i2)') Cislo(1:10),nd
        endif
        do j=1,ns
          do ii=1,3
            read(lnr,'(4i4)') rmp(ii,1:3,j)
          enddo
          read(lnr,FormA) Veta
          do ii=1,3
            read(lnr,'(3f10.6)') rmr(ii,1:3,j)
          enddo
        enddo
        do j=1,ns
          kkk=0
          do k=1,ns
            if(EqIV(rms(1,1,j),rmp(1,1,k),9)) then
              write(lnn,'(i5,9f8.4/5x,9f8.4)')
     1          j,((rmr(ii,jj,k),0.,jj=1,nd),ii=1,nd)
              kkk=1
              exit
            endif
          enddo
          if(kkk.ne.1) then
            write(6,'('' Nenasel:'',i5)') j
            write(6,'(3i3)')((rms(ii,jj,j),jj=1,3),ii=1,3)
          endif
        enddo
        go to 1200
      enddo
      call CloseIfOpened(lni)
      call CloseIfOpened(lnn)
      call CloseIfOpened(lnr)
      return
      end
      subroutine TestMatic
      dimension RA(3,5),RAi(3,5),RB(5,5),RBi(5,5),RC(5,3),RD(3,3),
     1          RDi(3,3),RP(5,5)
      RA(1,1)=2.
      RA(1,2)=-1.
      RA(1,3)=-2.
      RA(1,4)=1.
      RA(1,5)=4.
      RA(2,1)=3.
      RA(2,2)=-3.
      RA(2,3)= 0.
      RA(2,4)=-1.
      RA(2,5)=0.
      RA(3,1)=2.
      RA(3,2)=3.
      RA(3,3)=0.
      RA(3,4)=-1.
      RA(3,5)=2.
      call TrMat(RA,RC,3,5)
      do i=1,3
        write(6,'(10f8.3)') RA(i,1:5)
      enddo
      do i=1,5
        write(6,'(10f8.3)') RC(i,1:3)
      enddo
      call MultM(RA,RC,RD,3,5,3)
      call MatInv(RD,RDi,det1,3)
      write(6,'('' Determinant1:'',f10.1)') det1
      RB(1,1)=1.
      RB(1,2)=-1.
      RB(1,3)=-2.
      RB(1,4)=-2.
      RB(1,5)= 3.
!      RB(2,1)=-3.
      RB(2,1)=RB(1,2)
      RB(2,2)= 1.
      RB(2,3)= 2.
      RB(2,4)=-3.
      RB(2,5)= 0.
!      RB(3,1)= 3.
!      RB(3,2)= 3.
      RB(3,1)=RB(1,3)
      RB(3,2)=RB(2,3)
      RB(3,3)= 0.
      RB(3,4)=-3.
      RB(3,5)=-1.
!      RB(4,1)=-3.
!      RB(4,2)= 1.
!      RB(4,3)=-2.
      RB(4,1)=RB(1,4)
      RB(4,2)=RB(2,4)
      RB(4,3)=RB(3,4)
      RB(4,4)= 0.
      RB(4,5)= 1.
!      RB(5,1)= 2.
!      RB(5,2)= 2.
!      RB(5,3)= 2.
!      RB(5,4)= 3.
      RB(5,1)=RB(1,5)
      RB(5,2)=RB(2,5)
      RB(5,3)=RB(3,5)
      RB(5,4)=RB(4,5)
      RB(5,5)=-1.
      call MatInv(RB,RBi,det2,5)
      write(6,'('' Determinant2:'',f10.1)') det2
!      call MultM(RA,RB,RAi,3,5,5)
!      call MultM(RAi,RC,RD,3,5,3)
!      call MatInv(RD,RDi,det3,3)
!      write(6,'('' Determinant3:'',f10.1)') det3
      call MultM(RC,RA,RBi,5,3,5)
      call MatInv(RBi,RP,det3,5)
      write(6,'('' Determinant3:'',f10.1)') det3
      call MultM(RA,RB,RAi,3,5,5)
      call MultM(RAi,RC,RD,3,5,3)
      call MatInv(RD,RDi,det4,3)
      write(6,'('' Determinant3:'',f10.1)') det4


      return
      end

      subroutine TestMatic2
      real RA(5,5),RAi(5,5),RB(5,5),RBi(5,5),RC(5,5),RD(5,5),
     1     RDi(5,5)
      call UnitMat(RA,5)
      RA(1,1)=2.
      RA(1,2)=-1.
      RA(1,3)=-2.
      RA(1,4)=1.
      RA(1,5)=4.
      RA(2,1)=3.
      RA(2,2)=-3.
      RA(2,3)= 0.
      RA(2,4)=-1.
      RA(2,5)=0.
      RA(3,1)=2.
      RA(3,2)=3.
      RA(3,3)=0.
      RA(3,4)=-1.
      RA(3,5)=2.
      call TrMat(RA,RC,5,5)
      do i=1,5
        write(6,'(10f8.3)') RA(i,1:5)
      enddo
      do i=1,5
        write(6,'(10f8.3)') RC(i,1:5)
      enddo
      call MatInv(RA,RAi,det1,5)
      write(6,'('' Determinant1:'',f10.1)') det1
      call MatInv(RC,RAi,det2,5)
      write(6,'('' Determinant2:'',f10.1)') det2
      call MultM(RA,RC,RD,5,5,5)
      call MatInv(RD,RAi,det3,5)
      write(6,'('' Determinant3:'',f10.1)') det3
      RB(1,1)=1.
      RB(1,2)=-1.
      RB(1,3)=-2.
      RB(1,4)=-2.
      RB(1,5)= 3.
!      RB(2,1)=-3.
      RB(2,1)=RB(1,2)
      RB(2,2)= 1.
      RB(2,3)= 2.
      RB(2,4)=-3.
      RB(2,5)= 0.
!      RB(3,1)= 3.
!      RB(3,2)= 3.
      RB(3,1)=RB(1,3)
      RB(3,2)=RB(2,3)
      RB(3,3)= 0.
      RB(3,4)=-3.
      RB(3,5)=-1.
!      RB(4,1)=-3.
!      RB(4,2)= 1.
!      RB(4,3)=-2.
      RB(4,1)=RB(1,4)
      RB(4,2)=RB(2,4)
      RB(4,3)=RB(3,4)
      RB(4,4)= 0.
      RB(4,5)= 1.
!      RB(5,1)= 2.
!      RB(5,2)= 2.
!      RB(5,3)= 2.
!      RB(5,4)= 3.
      RB(5,1)=RB(1,5)
      RB(5,2)=RB(2,5)
      RB(5,3)=RB(3,5)
      RB(5,4)=RB(4,5)
      RB(5,5)=-1.
      call MatInv(RB,RAi,det4,5)
      write(6,'('' Determinant4:'',f10.1)') det4
      call MultM(RA,RB,RD,5,5,5)
      call MatInv(RD,RAi,det5,5)
      write(6,'('' Determinant5:'',f10.1)') det5
      call MultM(RD,RC,RAi,5,5,5)
      call MatInv(RAi,RDi,det6,5)
      write(6,'('' Determinant5:'',f10.1)') det6
      return
      end
      subroutine TestOrtho1
      include 'fepc.cmn'
      include 'basic.cmn'
      real x4(1000),d(1000),F(7,1000),der(7),RM(1000),PS(7),Sol(7),
     1     Diag(7)
      open(40,file='simul_x.asc')
      n=0
1100  n=n+1
      read(40,*,end=1200) x4(n),d(n)
      x4(n)=anint(x4(n)*100.)/100.
      go to 1100
1200  close(40)
      n=n-1
!      do i=1,n
!        arg=pi2*x4(i)
!        F(1,i)=1.
!        F(2,i)=sin(arg)
!        F(3,i)=cos(arg)
!        F(4,i)=sin(2.*arg)
!        F(5,i)=cos(2.*arg)
!        F(6,i)=sin(3.*arg)
!        F(7,i)=cos(3.*arg)
!      enddo
      x4s=.2
      delta=.6
      do i=1,n
        pom=x4(i)-x4s
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=pom/delta
        call GetFPol(pom,F(1,i),7,3)
      enddo
      PS=0.
      RM=0.
      do i=1,n
        m=0
        do ii=1,7
          PS(ii)=PS(ii)+d(i)*F(ii,i)
          do jj=1,ii
            m=m+1
            RM(m)=RM(m)+F(ii,i)*F(jj,i)
          enddo
        enddo
      enddo
      ij=0
      do i=1,7
        ij=ij+i
        Diag(i)=1./sqrt(RM(ij))
      enddo
      call znorm(RM,Diag,7)
      call smi(RM,7,ising)
      if(ising.ne.0) then
        call FeChybne(-1.,-1.,'the process cannot continue due to a '//
     1                'singularity problem.',
     2                'Modify parameters and try again.',SeriousError)
      else
        call znorm(RM,Diag,7)
        call nasob(RM,PS,Sol,7)
      endif
      ij=0
      do i=1,7
        ij=ij+i
        Diag(i)=sqrt(RM(ij))
      enddo
      k=0
      m=0
      pom=0.
      do ii=1,7
        do jj=1,ii
          m=m+1
          if(ii.eq.jj) cycle
          write(6,'(i5)') k
          k=k+1
          pom=pom+abs(RM(m)/(Diag(ii)*Diag(jj)))
        enddo
      enddo
      write(6,'('' Mira korelace:'',f10.4)') pom/float(k)
      return
      end
      subroutine TestOrtho2
      include 'fepc.cmn'
      include 'basic.cmn'
      real NMat(64,64),F(7,64),FT(64,7),PMat(7,64),TMat(7,7),Diag(64),
     1     QMat(64,64)
      real DP(1024),DPi(1024)
      x4s=.2
      delta=.3
      x4d=delta/16.
      x4=x4s-delta+x4d*.5
!      do i=1,64
!        arg=pi2*x4
!        F(1,i)=1.
!        F(2,i)=sin(arg)
!        F(3,i)=cos(arg)
!        F(4,i)=sin(2.*arg)
!        F(5,i)=cos(2.*arg)
!        F(6,i)=sin(3.*arg)
!        F(7,i)=cos(3.*arg)
!        x4=x4+x4d
!      enddo
      do i=1,64
        pom=x4-x4s
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=pom/delta
        call GetFPol(pom,F(1,i),7,2)
        x4=x4+x4d
      enddo
      call Random_Seed()
      call TrMat(F,FT,7,64)
      CMax=.6
      do k=1,10
1100    call UnitMat(NMat,64)
        suma=0.
        n=0
        do i=1,64
          do j=1,i-1
            call Random_Number(pom)
            pom=(2.*pom-1.)*CMax
            NMat(i,j)=pom
            NMat(j,i)=pom
            suma=suma+abs(NMat(i,j)/sqrt(NMat(i,i)*NMat(j,j)))
            n=n+1
          enddo
        enddo
!        write(6,'('' Mira korelace pred:'',f10.4)') suma/float(n)
        do i=1,64
          do j=1,64
!            NMat(i,j)=NMat(i,j)/sqrt(float(i*j))
          enddo
        enddo
!        suma=0.
!        n=0
!        do i=1,64
!          do j=1,i-1
!            suma=suma+abs(NMat(i,j)/sqrt(NMat(i,i)*NMat(j,j)))
!            n=n+1
!          enddo
!        enddo


!        write(6,'(64f10.4)')((NMat(i,j),j=1,64),i=1,64)
!        call QLN(NMat,QMat,Diag,64)
!        call FeWinMessage('Prosel',' ')
!        do i=1,64
!          if(Diag(i).lt.0.) then
!            call FeWinMessage('Zaporna1',' ')
!            go to 1100
!          endif
!        enddo
!        kk=0
!        do nn=1,64
!          m=0
!          do i=1,nn
!            do j=1,nn
!              m=m+1
!              DP(m)=NMat(i,j)
!            enddo
!          enddo
!          call MatInv(DP,DPi,det,nn)
!          if(det.lt.0.) kk=1
!        enddo
!        if(kk.eq.1) then
!          call FeWinMessage('Zaporna2',' ')
!        else
!          call FeWinMessage('Kladna',' ')
!        endif
!        call FeWinMessage('Ma ji',' ')
        write(6,'('' Mira korelace pred:'',f10.4)') suma/float(n)
        call MultM(F,NMat,PMat,7,64,64)
        call MultM(PMat,FT,TMat,7,64,7)

!        call MultM(F,FT,TMat,7,64,7)

        suma=0.
        n=0
!        write(6,'(7f10.4)')((TMat(i,j),j=1,7),i=1,7)
        do i=1,7
          do j=1,i-1
            suma=suma+abs(TMat(i,j)/sqrt(TMat(i,i)*TMat(j,j)))
            n=n+1
          enddo
        enddo
        write(6,'('' Mira korelace pote:'',f10.4)') suma/float(n)
      enddo
      return
      end

      subroutine TestOrtho3
      include 'fepc.cmn'
      include 'basic.cmn'
      real F(7,2000),FT(2000,7),TMat(7,7)
      n=2000
      x4s=.2
      delta=.2
      x4d=2.*delta/float(n)
      x4=x4s-delta+x4d*.5
!      do i=1,n
!        arg=pi2*x4
!        F(1,i)=1.
!        F(2,i)=sin(arg)
!        F(3,i)=cos(arg)
!        F(4,i)=sin(2.*arg)
!        F(5,i)=cos(2.*arg)
!        F(6,i)=sin(3.*arg)
!        F(7,i)=cos(3.*arg)
!        x4=x4+x4d
!      enddo
      do i=1,n
        pom=x4-x4s
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=pom/delta
        call GetFPol(pom,F(1,i),7,3)
!        do j=1,7
!          F(j,i)=F(j,i)/sqrt(float(i))
!        enddo
        x4=x4+x4d
      enddo
      write(6,'(f10.4)') suma
      call TrMat(F,FT,7,n)
      call MultM(F,FT,TMat,7,n,7)
      suma=0.
      n=0
!      write(6,'(7f10.4)')((TMat(i,j),j=1,7),i=1,7)
      do i=1,7
        do j=1,i-1
          suma=suma+abs(TMat(i,j)/sqrt(TMat(i,i)*TMat(j,j)))
          n=n+1
        enddo
      enddo
      write(6,'('' Mira korelace pote:'',f10.4)') suma/float(n)
      return
      end
      subroutine EM40ChangeAtTypeMod(ia,TypeModFunNew,OrthoEpsNew)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      real, allocatable :: xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1                     FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),
     2                     RSol(:),Y1Old(:),Y1New(:)
      real XPom(3),QPom(3)
      integer kmodp(7),TypeModFunNew
      integer, allocatable :: OrthoSelNew(:)
      character*256 Veta
      if(TypeModFunNew.eq.TypeModFun(ia).and.TypeModFunNew.ne.1)
     1   go to 9999
      MaxUsedKwP=0
      do j=1,7
        MaxUsedKwP=max(MaxUsedKwP,KModA(j,ia))
      enddo
      m=max(OrthoOrd,2*MaxUsedKwAll+1)
      allocate(xp(m),yp(m),OrthoSelNew(m),OrthoMatNew(m**2),
     1         OrthoMatNewI(m**2),FOld(m,NPoints),FNew(m,NPoints),
     2         YOld(NPoints),RMat(m*(m+1)),RSide(m),RSol(m),
     3         Y1Old(NPoints),Y1New(NPoints))
      X40=ax(1,ia)
      Delta=a0(ia)
      if(TypeModFunNew.eq.1) then
        if(OrthoOrd.le.0) OrthoOrd=m
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        call UpdateOrtho(ia,X40,Delta,OrthoEpsNew,OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,ia)

      endif
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
        n=2*NCommQProduct(iswa(ia),KPhase)
        dx4=1./float(n)
        NPointsUsed=n
      else
        NPointsUsed=NPoints
        x4=X40-Delta*.5
        dx4=Delta/float(NPointsUsed-1)
      endif
      nn=0
      x4p=x4
      XPom(1:3)=x(1:3,ia)
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        nn=nn+1
        if(TypeModFun(ia).eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FOld(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FOld(k,m)=sin(Pi2*km*x4)
            else
              FOld(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFun(ia).eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSel(k,ia)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMat(1,ia),yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FOld(1,m),2*MaxUsedKwAll+1,TypeModFun(ia))
!          call GetFPol(pom,FOld(1,m),2*MaxUsedKwP+1,TypeModFun(ia))
        endif
        if(TypeModFunNew.eq.0) then
          do k=1,2*MaxUsedKwAll+1
!          do k=1,2*MaxUsedKwP+1
            km=k/2
            if(k-1.eq.0) then
              FNew(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FNew(k,m)=sin(Pi2*km*x4)
            else
              FNew(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunNew.eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FNew(1,m),2*MaxUsedKwAll+1,TypeModFunNew)
        endif
        x4=x4p+float(m)*dx4
      enddo
      if(TypeModFun(ia).eq.1) then
        nn=min(OrthoOrd,nn)
      else
        nn=min(2*MaxUsedKwP+1,nn)
      endif
      RMat=0.
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
      else
        x4=X40-Delta*.5
      endif
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        l=0
        do i=1,nn
          do j=1,i
            l=l+1
            RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
          enddo
        enddo
        x4=x4p+float(m)*dx4
      enddo
      open(66,file='RMat.txt')
      call smi(RMat,nn,ISing)
      do it=2,itf(ia)+1
        kmod=KModA(it,ia)
        if(kmod.le.0) cycle
        n=TRank(it-1)
        do i=1,n
          xp=0.
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                xp(k)=x(i,ia)
              else if(it.eq.3) then
                xp(k)=beta(i,ia)
              else if(it.eq.4) then
                xp(k)=c3(i,ia)
              else if(it.eq.5) then
                xp(k)=c4(i,ia)
              else if(it.eq.6) then
                xp(k)=c5(i,ia)
              else if(it.eq.7) then
                xp(k)=c6(i,ia)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                xp(k)=ux(i,km,ia)
              else if(it.eq.3) then
                xp(k)=bx(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3x(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4x(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5x(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6x(i,km,ia)
              endif
            else
              if(it.eq.2) then
                xp(k)=uy(i,km,ia)
              else if(it.eq.3) then
                xp(k)=by(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3y(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4y(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5y(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6y(i,km,ia)
              endif
            endif
          enddo
          RSide=0.
          if(KCommen(KPhase).gt.0) then
            x4=trez(1,1,KPhase)+qcnt(1,ia)
          else
            x4=X40-Delta*.5
          endif
          x4p=x4
          do m=1,NPointsUsed
            pom=x4-X40
            j=pom
            if(pom.lt.0.) j=j-1
            pom=pom-float(j)
            if(pom.gt..5) pom=pom-1.
            pom=2.*pom/Delta
            if(pom.lt.-1.01.or.pom.gt.1.01) then
              x4=x4p+float(m)*dx4
              cycle
            endif
            call MultM(FOld(1,m),xp,YOld(m),1,nn,1)
            if(it.eq.2.and.i.eq.1) Y1Old(m)=YOld(m)
            do j=1,nn
              RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
            enddo
            x4=x4p+float(m)*dx4
          enddo
          RSol=0.
          call nasob(RMat,RSide,RSol,nn)
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                x(i,ia)=RSol(k)
              else if(it.eq.3) then
                beta(i,ia)=RSol(k)
              else if(it.eq.4) then
                c3(i,ia)=RSol(k)
              else if(it.eq.5) then
                c4(i,ia)=RSol(k)
              else if(it.eq.6) then
                c5(i,ia)=RSol(k)
              else if(it.eq.7) then
                c6(i,ia)=RSol(k)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                ux(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                bx(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3x(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4x(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5x(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6x(i,km,ia)=RSol(k)
              endif
            else
              if(it.eq.2) then
                uy(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                by(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3y(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4y(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5y(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6y(i,km,ia)=RSol(k)
              endif
            endif
          enddo
        enddo
      enddo
      TypeModFun(ia)=TypeModFunNew
      if(TypeModFunNew.eq.1) then
        if(.not.allocated(OrthoSel))
     1    allocate(OrthoSel(OrthoOrd,NAtAll+1))
        m=OrthoOrd**2
        if(.not.allocated(OrthoMat))
     1     allocate(OrthoMat(m,NAtAll+1),OrthoMatI(m,NAtAll+1))
        call CopyVekI(OrthoSelNew,OrthoSel(1,ia),OrthoOrd)
        call CopyMat(OrthoMatNew ,OrthoMat (1,ia),OrthoOrd)
        call CopyMat(OrthoMatNewI,OrthoMatI(1,ia),OrthoOrd)
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        OrthoEps(ia)=OrthoEpsNew
        call trortho(0)
!        x(1:3,ia)=XPom(1:3)
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          kmodp=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
              kmodp(j)=max(kmodp(j),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        call trortho(1)
        if(Allocated(KWSym)) deallocate(KwSym)
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      else
        do i=1,3
          XPom(i)=x(i,ia)-XPom(i)
        enddo
        call qbyx(XPom,QPom,iswa(ia))
        ax(1,ia)=ax(1,ia)+QPom(1)
        X40=ax(1,ia)
      endif
      call SetRealArrayTo(xp,3,0.)
      xp(1)=X40
      call SpecPos(x(1,ia),xp,1,iswa(ia),.2,nocc)
      Veta=fln(:ifln)//'_pom.tmp'
      KPhaseIn=KPhase
      nvai=0
      neq=0
      neqs=0
      LstOpened=.true.
      uloha='Symmetry restrictions'
      call OpenFile(lst,Veta,'formatted','unknown')
      call NewPg(1)
      call MagParToCell(0)
      if(kmol(ia).gt.0) then
        im=kmol(ia)
        ji=(im-1)/mxp+1
        nap=NAtPosFrAll(KPhase)
        do i=1,im-1
          nap=nap+iam(i)*mam(i)
        enddo
      else
        im=0
        ji=0
        nap=0
      endif
      call atspec(ia,ia,ji,im,nap)
      call RefAppEq(0,0,0)
      call MagParToBohrMag(0)
      call CloseListing
      LstOpened=.false.
      call DeleteFile(Veta)
      deallocate(xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1           YOld,RMat,RSide,RSol,Y1Old,Y1New)
9999  return
      end
      subroutine CrlExportXD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,OutputFile,EdwStringQuest
      integer Exponent10,ihk(3)
      logical FeYesNo,ExistFile,SystExtRef
      real so(3,2),xp(3),MT(3,3),MTI(3,3),e1(3),e2(3),e3(3),e4(3),
     1     XD(3,4),MTP(9)
      lni=0
      lno=0
      id=NextQuestId()
      il=1
      xqd=300.
      Veta='Specify output file:'
      OutputFile=' '
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      bpom=50.
      xpom=5.
      tpom=5.
      dpom=xqd-bpom-20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,OutputFile)
      Veta='%Browse'
      xpom=tpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEVent(id,ich)
      if(CheckType.eq.EVentButton.and.CheckNumber.eq.nButtBrowse) then
        OutputFile=EdwStringQuest(nEdwName)
        call FeFileManager('Select output structure',OutputFile,'*.hkl',
     1                      0,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,OutputFile)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        OutputFile=EdwStringQuest(nEdwName)
        if(ExistFile(OutputFile)) then
          if(.not.FeYesNo(-1.,YBottomMessage,'The file "'//
     1       OutputFile(:idel(OutputFile))//
     2       '" already exists, overwrite it?',0)) then
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)

            go to 1500
          endif
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      MT(1,1)=1.
      MT(2,2)=1.
      MT(3,3)=1.
      MT(1,2)=cos(CellPar(6,1,KPhase)*ToRad)
      MT(2,1)=MT(1,2)
      MT(1,3)=cos(CellPar(5,1,KPhase)*ToRad)
      MT(3,1)=MT(1,3)
      MT(2,3)=cos(CellPar(4,1,KPhase)*ToRad)
      MT(3,2)=MT(2,3)
      call MatInv(MT,MTi,det,3)
      MT(1,1)=1.301900
      MT(2,2)=1.387334
      MT(3,3)=1.000157
      MT(1,2)=-0.218003
      MT(2,1)=MT(1,2)
      MT(1,3)=-0.212749
      MT(3,1)=MT(1,3)
      MT(2,3)=-0.440100
      MT(3,2)=MT(2,3)
      call MatInv(MT,MTi,det,3)
      write(66,'(3f10.6)') MT
      write(66,'(3f10.4)') sqrt(MT(1,1)),sqrt(MT(2,2)),sqrt(MT(3,3)),
     1  acos(MT(1,2)/(sqrt(MT(1,1)*MT(2,2))))/ToRad,
     1  acos(MT(1,3)/(sqrt(MT(1,1)*MT(3,3))))/ToRad,
     1  acos(MT(2,3)/(sqrt(MT(2,2)*MT(3,3))))/ToRad
      write(66,'('' ========================================='')')
      write(66,'(3f10.6)') MTi
      write(66,'(3f10.4)') sqrt(MTi(1,1)),sqrt(MTi(2,2)),sqrt(MTi(3,3)),
     1  acos(MTi(1,2)/(sqrt(MTi(1,1)*MTi(2,2))))/ToRad,
     1  acos(MTi(1,3)/(sqrt(MTi(1,1)*MTi(3,3))))/ToRad,
     1  acos(MTi(2,3)/(sqrt(MTi(2,2)*MTi(3,3))))/ToRad
      write(66,'('' ========================================='')')
      write(66,'(3f10.3)') MetTens(1:9,1,1)
      MTP(1:9)=MetTens(1:9,1,1)
      write(66,'(3f10.4)') sqrt(MTP(1)),sqrt(MTP(5)),sqrt(MTP(9)),
     1  acos(MTP(2)/(sqrt(MTP(1)*MTP(5))))/ToRad,
     1  acos(MTP(3)/(sqrt(MTP(1)*MTP(9))))/ToRad,
     1  acos(MTP(6)/(sqrt(MTP(5)*MTP(9))))/ToRad
      write(66,'('' ========================================='')')
      write(66,'(3f12.8)') MetTensI(1:9,1,1)
      MTP(1:9)=MetTensI(1:9,1,1)
      write(66,'(3f10.4)') sqrt(MTP(1)),sqrt(MTP(5)),sqrt(MTP(9)),
     1  acos(MTP(2)/(sqrt(MTP(1)*MTP(5))))/ToRad,
     1  acos(MTP(3)/(sqrt(MTP(1)*MTP(9))))/ToRad,
     1  acos(MTP(6)/(sqrt(MTP(5)*MTP(9))))/ToRad
      write(66,'('' ========================================='')')
!      call FeWinMessage('Kontrola',' ')
! 1.301900 -0.218003 -0.212749
!-0.218003  1.387334 -0.440100
!-0.212749 -0.440100  1.000157
!      MTi(1,1)=1.
!      MTi(2,2)=1.
!      MTi(3,3)=1.
!      MTi(1,2)=Rcp(6,1,KPhase)
!      MTi(2,1)=MTi(1,2)
!      MTi(1,3)=Rcp(5,1,KPhase)
!      MTi(3,1)=MTi(1,3)
!      MTi(2,3)=Rcp(4,1,KPhase)
!      MTi(3,2)=MTi(2,3)
!      call MatInv(MTi,MT,det,3)

!      write(6,'(3f10.5)') MT
!      call FeWinMessage('Kontrola',' ')
      call iom95(0,fln(:ifln)//'.m95')
      KDatBlock=1
      lni=NextLogicNumber()
      call OpenRefBlockM95(lni,KDatBlock,fln(:ifln)//'.m95')
      lno=NextLogicNumber()
      call OpenFile(lno,OutputFile,'formatted','unknown')
      lnp=NextLogicNumber()
      call OpenFile(lnp,'original.hkl','formatted','unknown')
      RiMax=0.
      RsMax=0.
2000  call DRGetReflectionFromM95(lni,iend,ich)
      if(iend.ne.0.or.ich.ne.0) go to 2500
      if(SystExtRef(ih,.false.,1)) go to 2000
      pom=corrf(1)*corrf(2)
      Ri=Ri*pom
      Rs=Rs*pom
      RiMax=max(Ri,RiMax)
      RsMax=max(Rs,RsMax)
      go to 2000
2500  pom=max(RiMax/9999999.99,RsMax/99999.9)
      k=Exponent10(pom)+2
      if(k.gt.0) then
        scp=1./10**k
      else
        scp=1.
      endif
      call CloseIfOpened(lni)
      call OpenRefBlockM95(lni,KDatBlock,fln(:ifln)//'.m95')
      Veta=fln
      Veta(ifln+10:)='F^2        NDAT   13'
      write(lno,FormA) Veta(:idel(Veta))
3000  call DRGetReflectionFromM95(lni,iend,ich)
      if(SystExtRef(ih,.false.,1)) go to 3000
      read(lnp,'(3i4,2f8.0,173x,6f8.5,1x,6f8.5)') ihk,Fp,sfp,
     1  XD
      write(6,'(3i4,'' | '',3i4,'' | '')') ihk,ih(1:3)
      write(6,'(6f8.5)') XD
      call MultM(MT,XD(1,1),e1,3,3,1)
!      call VecNor(e1,XD(1,1))
      call MultM(MT,XD(1,2),e2,3,3,1)
!      call VecNor(e2,XD(1,2))
      call MultM(MT,XD(1,3),e3,3,3,1)
!      call VecNor(e3,XD(1,3))
      call MultM(MT,XD(1,4),e4,3,3,1)
!      call VecNor(e4,XD(1,4))
      write(6,'(4f8.4)') scalmul(XD(1,1),e1),scalmul(XD(1,2),e2),
     1                   scalmul(XD(1,3),e3),scalmul(XD(1,4),e4)
!      write(6,'(6f8.5)') XD
      pom=corrf(1)*corrf(2)*scp
      Ri=Ri*pom
      Rs=Rs*pom
      if(iend.ne.0.or.ich.ne.0) go to 9999
      do i=1,3
        xp(i)=CellPar(i,1,KPhase)*rcp(i,1,KPhase)
!        xp(i)=1./(rcp(i,1,KPhase)*CellPar(i,1,KPhase))
      enddo
      do j=1,2
        if(j.eq.1) then
          pom=-1.
        else
          pom=1.
        endif
        do k=1,3
          so(k,j)=pom*DirCos(k,j)*xp(k)
        enddo
      enddo
      call MultM(MT,so(1,1),e1,3,3,1)
      call MultM(MT,so(1,2),e2,3,3,1)
      write(6,'(4f8.4)') scalmul(so(1,1),e1),
     1                   scalmul(so(1,2),e2)
      call VecMul(so(1,1),so(1,2),xp)
      call MultM(MTi,xp,e1,3,3,1)
      call VecNor(e1,xp)
      call VecMul(e1,so(1,1),xp)
      call MultM(MTi,xp,e2,3,3,1)
      call VecNor(e2,xp)
      write(lno,'(3i4,i2,f10.2,f7.1,1x,f5.4,6(1x,f7.4))') ih(1:3),1,
     1  ri,rs,tbar,e1,e2
      write(6,'(6f8.5)') e1,e2
      call MultM(MT,e1,e3,3,3,1)
      call MultM(MT,e2,e4,3,3,1)
      write(6,'(4f8.4)') scalmul(e1,e3),scalmul(e2,e4)

      go to 3000
9999  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      return
      end
      subroutine NajdiTenzorOld
      include 'fepc.cmn'
      include 'basic.cmn'
      real XD(3,2),AM(81),PS(9),der(9),SOL(9),MT(3,3),MTi(3,3),e1(3),
     1     e2(3),e3(3),e4(3),DirCos(3,2,20000),xp(3)
      integer ihk(3),iha(3,20000)
      nref=0
      lnp=NextLogicNumber()
      do 1500m=1,3
        if(m.eq.1) then
          call OpenFile(lnp,'HOZ23.hkl','formatted','unknown')
        else if(m.eq.2) then
          call OpenFile(lnp,'IOZ23.hkl','formatted','unknown')
        else if(m.eq.3) then
          call OpenFile(lnp,'KOZ23.hkl','formatted','unknown')
        endif
1100    read(lnp,100,end=1200) ihk,f,sf,i,der(1:6)
        if(ihk(1).eq.0.and.ihk(2).eq.0.and.ihk(3).eq.0) go to 1200
        nref=nref+1
        iha(1:3,nref)=ihk(1:3)
        DirCos(1,1,NRef)=der(1)
        DirCos(1,2,NRef)=der(2)
        DirCos(2,1,NRef)=der(3)
        DirCos(2,2,NRef)=der(4)
        DirCos(3,1,NRef)=der(5)
        DirCos(3,2,NRef)=der(6)
!        write(6,'(i10,6f8.5)') NRef,DirCos(1:3,1,NRef)
        go to 1100
1200    call CloseIfOpened(lnp)
1500  continue
!      call FeWinMessage('Kontrola',' ')
      lnp=NextLogicNumber()
      call OpenFile(lnp,'xd.hkl','formatted','unknown')
      read(lnp,'(a)')
      lno=NextLogicNumber()
      call OpenFile(lno,'xd.comp','formatted','unknown')
      AM=0.
      PS=0.
      n=0
!3000  read(lnp,'(3i4,2f8.0,173x,6f8.5,1x,6f8.5)') ihk,Fp,sfp,
!     1  XD
3000  read(lnp,'(3i4,i2,f10.0,f7.0,f6.0,6f8.5)',end=3333) ihk,i,Fp,sfp,
     1  tt,XD
      der(1)=XD(1,1)**2
      der(2)=XD(2,1)**2
      der(3)=XD(3,1)**2
      der(4)=2.*XD(1,1)*XD(2,1)
      der(5)=2.*XD(1,1)*XD(3,1)
      der(6)=2.*XD(2,1)*XD(3,1)
      m=0
      do j=1,6
        derj=der(j)
        do k=1,j
          m=m+1
          AM(m)=AM(m)+derj*der(k)
        enddo
        PS(j)=PS(j)+derj
      enddo
      n=n+1
!      if(n.le.3333) go to 3000
      go to 3000
3333  call smi(AM,6,ising)
      if(ising.gt.0) then
        call FeWinMessage('Singular',' ')
        go to 9999
      endif
      call nasob(AM,PS,SOL,6)
      write(lno,'(3f10.6)') SOL(1),Sol(4),Sol(5)
      write(lno,'(3f10.6)') SOL(4),Sol(2),Sol(6)
      write(lno,'(3f10.6)') SOL(5),Sol(6),Sol(3)
      MT(1,1)=SOL(1)
      MT(2,2)=SOL(2)
      MT(3,3)=SOL(3)
      MT(1,2)=SOL(4)
      MT(2,1)=SOL(4)
      MT(1,3)=SOL(5)
      MT(3,1)=SOL(5)
      MT(2,3)=SOL(6)
      MT(3,2)=SOL(6)
      call MatInv(MT,MTi,det,3)
!      MTi(1,1)=SOL(1)
!      MTi(2,2)=SOL(2)
!      MTi(3,3)=SOL(3)
!      MTi(1,2)=SOL(4)
!      MTi(2,1)=SOL(4)
!      MTi(1,3)=SOL(5)
!      MTi(3,1)=SOL(5)
!      MTi(2,3)=SOL(6)
!      MTi(3,2)=SOL(6)
!      call MatInv(MTi,MT,det,3)
      write(lno,'(3f10.6)') MTi
      do i=1,3
        xp(i)=CellPar(i,1,KPhase)*rcp(i,1,KPhase)
      enddo
      do i=1,NRef
        do j=1,2
          do k=1,3
            DirCos(k,j,i)=DirCos(k,j,i)*xp(k)
          enddo
        enddo
      enddo
      rewind lnp
      read(lnp,'(a)')
      AM=0.
      PS=0.
4000  read(lnp,'(3i4,i2,f10.0,f7.0,f6.0,6f8.5)',end=5000) ihk,i,Fp,sfp,
     1  tt,XD
      write(lno,'('' Reflexe:'',3i4)') ihk
      call MultM(MT,XD(1,1),e1,3,3,1)
      call MultM(MT,XD(1,2),e2,3,3,1)
      write(lno,'(3f10.5)') scalmul(XD(1,1),e1),scalmul(XD(1,2),e2),
     1                      scalmul(XD(1,2),e1)
      m=0
      do i=1,NRef
        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
     1     ihk(3).eq.iha(3,i)) then
          m=m+1
          ip=i
          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
          write(lno,'(i5,3f10.5)') m,scalmul(DirCos(1,1,i),e3),
     1                             scalmul(DirCos(1,1,i),e1),
     1                             scalmul(DirCos(1,2,i),e1)
        endif
      enddo
      if(m.le.0) then
        write(lno,'('' Nenasel nic'')')
      else if(m.eq.1) then
        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
        call MultM(MTi,xp,e1,3,3,1)
        call VecNor(e1,xp)
        e3=xp
        call VecMul(e1,DirCos(1,1,ip),xp)
        call MultM(MTi,xp,e2,3,3,1)
        call VecNor(e2,xp)
        e4=xp
        write(lno,'(6f8.5)') XD
        write(lno,'(6f8.5)') e1,e2,e3,e4
        call MultM(MT,DirCos(1,1,ip),e3,3,3,1)
        write(lno,'(3f10.5)') scalmul(DirCos(1,1,ip),e3),
     1                        scalmul(e1,e3),
     1                        scalmul(e2,e3)
        do i=1,3
          der=0.
          do j=1,3
            der((i-1)*3+j)=e1(j)
          enddo
          m=0
          do j=1,9
            derj=der(j)
            do k=1,j
              m=m+1
              AM(m)=AM(m)+derj*der(k)
            enddo
            PS(j)=PS(j)+derj*XD(i,1)
          enddo
        enddo
      endif
      write(lno,'('' -------------------------------------------'')')
      go to 4000
5000  call smi(AM,9,ising)
      if(ising.gt.0) then
        call FeWinMessage('Singular2',' ')
        go to 9999
      endif
      call nasob(AM,PS,SOL,9)
      write(lno,'(3f10.6)') SOL(1),Sol(2),Sol(3)
      write(lno,'(3f10.6)') SOL(4),Sol(5),Sol(6)
      write(lno,'(3f10.6)') SOL(7),Sol(8),Sol(9)
      rewind lnp
      read(lnp,'(a)')
      write(lno,'(''Kontrola'')')
6000  read(lnp,'(3i4,i2,f10.0,f7.0,f6.0,6f8.5)',end=9999) ihk,i,Fp,sfp,
     1  tt,XD
      write(lno,'('' Reflexe:'',3i4)') ihk
      m=0
      do i=1,NRef
        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
     1     ihk(3).eq.iha(3,i)) then
          m=m+1
          ip=i
          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
        endif
      enddo
      if(m.le.0) then
        write(lno,'('' Nenasel nic'')')
      else if(m.eq.1) then
        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
        call MultM(MTi,xp,e1,3,3,1)
        call VecNor(e1,xp)
        call VecMul(e1,DirCos(1,1,ip),xp)
        call MultM(MTi,xp,e2,3,3,1)
        call VecNor(e2,xp)
        call MultM(e1,Sol,e3,1,3,3)
        write(lno,'(3f8.5)') e3
        write(lno,'(3f8.5)') XD(1:3,1)
      endif
      go to 6000
9999  call CloseIfOpened(lnp)
      call CloseIfOpened(lno)
      return
100   format(3i4,2f8.0,i4,6f8.5)
      end
      subroutine NajdiTenzor
      include 'fepc.cmn'
      include 'basic.cmn'
      real XD(3,2),AM(81),PS(9),der(9),SOL(9),MT(3,3),MTi(3,3),e1(3),
     1     e2(3),e3(3),e4(3),DirCos(3,2,100000),xp(3)
      integer ihk(3),iha(3,100000)
      nref=0
      lnp=NextLogicNumber()
      do 1500m=1,1
        if(m.eq.1) then
          call OpenFile(lnp,'CuActetat100K_15175.hkl','formatted',
     1                  'unknown')
        endif
1100    read(lnp,100,end=1200) ihk,f,sf,i,der(1:6)
        if(ihk(1).eq.0.and.ihk(2).eq.0.and.ihk(3).eq.0) go to 1200
        nref=nref+1
        iha(1:3,nref)=ihk(1:3)
        DirCos(1,1,NRef)=der(1)
        DirCos(1,2,NRef)=der(2)
        DirCos(2,1,NRef)=der(3)
        DirCos(2,2,NRef)=der(4)
        DirCos(3,1,NRef)=der(5)
        DirCos(3,2,NRef)=der(6)
!        write(6,'(i10,6f8.5)') NRef,DirCos(1:3,1,NRef)
        go to 1100
1200    call CloseIfOpened(lnp)
1500  continue
!      call FeWinMessage('Kontrola',' ')
      lnp=NextLogicNumber()
      call OpenFile(lnp,'XD_compatible_15175.hkl','formatted','unknown')
!      call OpenFile(lnp,'xd.hkl','formatted','unknown')
      read(lnp,'(a)')
      lno=NextLogicNumber()
      call OpenFile(lno,'xd.comp','formatted','unknown')
      AM=0.
      PS=0.
      n=0
3000  read(lnp,101,end=3333) ihk,i,Fp,sfp,tt,XD
      der(1)=XD(1,1)**2
      der(2)=XD(2,1)**2
      der(3)=XD(3,1)**2
      der(4)=2.*XD(1,1)*XD(2,1)
      der(5)=2.*XD(1,1)*XD(3,1)
      der(6)=2.*XD(2,1)*XD(3,1)
      m=0
      do j=1,6
        derj=der(j)
        do k=1,j
          m=m+1
          AM(m)=AM(m)+derj*der(k)
        enddo
        PS(j)=PS(j)+derj
      enddo
      n=n+1
!      if(n.le.3333) go to 3000
      go to 3000
3333  call smi(AM,6,ising)
      if(ising.gt.0) then
        call FeWinMessage('Singular',' ')
        go to 9999
      endif
      call nasob(AM,PS,SOL,6)
      write(lno,'(3f10.6)') SOL(1),Sol(4),Sol(5)
      write(lno,'(3f10.6)') SOL(4),Sol(2),Sol(6)
      write(lno,'(3f10.6)') SOL(5),Sol(6),Sol(3)
      MTi(1,1)=SOL(1)
      MTi(2,2)=SOL(2)
      MTi(3,3)=SOL(3)
      MTi(1,2)=SOL(4)
      MTi(2,1)=SOL(4)
      MTi(1,3)=SOL(5)
      MTi(3,1)=SOL(5)
      MTi(2,3)=SOL(6)
      MTi(3,2)=SOL(6)
      call MatInv(MTi,MT,det,3)
      write(lno,'(3f10.6)') MT
      do i=1,3
        xp(i)=CellPar(i,1,KPhase)*rcp(i,1,KPhase)
      enddo
      do i=1,NRef
        do j=1,2
          do k=1,3
            DirCos(k,j,i)=DirCos(k,j,i)*xp(k)
          enddo
        enddo
      enddo
      rewind lnp
      read(lnp,'(a)')
      AM=0.
      PS=0.
      ip=0
4000  read(lnp,101,end=5000) ihk,i,Fp,sfp,tt,XD
      write(lno,'('' Reflexe:'',3i4)') ihk
      call MultM(MTi,XD(1,1),e1,3,3,1)
      call MultM(MTi,XD(1,2),e2,3,3,1)
      write(lno,'(3f10.5)') scalmul(XD(1,1),e1),scalmul(XD(1,2),e2),
     1                      scalmul(XD(1,2),e1)
      ip=ip+1
!      m=0
!      do i=1,NRef
!        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
!     1     ihk(3).eq.iha(3,i)) then
!          m=m+1
!          ip=i
!          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
!          write(lno,'(i5,3f10.5)') m,scalmul(DirCos(1,1,i),e3),
!     1                             scalmul(DirCos(1,1,i),e1),
!     1                             scalmul(DirCos(1,2,i),e1)
!        endif
!      enddo
!      if(m.le.0) then
!        write(lno,'('' Nenasel nic'')')
!      else if(m.eq.1) then
        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
        call MultM(MTi,xp,e1,3,3,1)
        call VecNor(e1,xp)
        e3=xp
        call VecMul(e1,DirCos(1,1,ip),xp)
        call MultM(MTi,xp,e2,3,3,1)
        call VecNor(e2,xp)
        e4=xp
        write(lno,'(6f8.5)') XD
        write(lno,'(6f8.5)') e1,e2
        write(lno,'(6f8.5)') e3,e4
        call MultM(MT,DirCos(1,1,ip),e3,3,3,1)
        write(lno,'(3f10.5)') scalmul(DirCos(1,1,ip),e3),
     1                        scalmul(e1,e3),
     1                        scalmul(e2,e3)
        do i=1,3
          der=0.
          do j=1,3
            der((i-1)*3+j)=e1(j)
          enddo
          m=0
          do j=1,9
            derj=der(j)
            do k=1,j
              m=m+1
              AM(m)=AM(m)+derj*der(k)
            enddo
            PS(j)=PS(j)+derj*XD(i,1)
          enddo
        enddo
!      endif
      write(lno,'('' -------------------------------------------'')')
      go to 4000
5000  call smi(AM,9,ising)
      if(ising.gt.0) then
        call FeWinMessage('Singular2',' ')
        go to 9999
      endif
      call nasob(AM,PS,SOL,9)
      write(lno,'(3f10.6)') SOL(1),Sol(2),Sol(3)
      write(lno,'(3f10.6)') SOL(4),Sol(5),Sol(6)
      write(lno,'(3f10.6)') SOL(7),Sol(8),Sol(9)
      rewind lnp
      read(lnp,'(a)')
      write(lno,'(''Kontrola'')')
      ip=0
6000  read(lnp,101,end=9999) ihk,i,Fp,sfp,tt,XD
      write(lno,'('' Reflexe:'',3i4)') ihk
      ip=ip+1
!      m=0
!      do i=1,NRef
!        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
!     1     ihk(3).eq.iha(3,i)) then
!          m=m+1
!          ip=i
!          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
!        endif
!      enddo
!      if(m.le.0) then
!        write(lno,'('' Nenasel nic'')')
!      else if(m.eq.1) then
!        call MultM(MT,DirCos(1,1,ip),e3,3,3,1)
        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
        call MultM(MTi,xp,e1,3,3,1)
        call VecNor(e1,xp)
        call VecMul(e1,DirCos(1,1,ip),xp)
        call MultM(MTi,xp,e2,3,3,1)
        call VecNor(e2,xp)
        call MultM(e1,Sol,e3,1,3,3)
        write(lno,'(3f8.5)') e3
        write(lno,'(3f8.5)') XD(1:3,1)
!      endif
      go to 6000
9999  call CloseIfOpened(lnp)
      call CloseIfOpened(lno)
      return
100   format(3i4,2f8.0,i4,6f8.5)
101   format(3i4,i4,f8.0,f8.0,f9.0,6f9.5)
      end
      subroutine NajdiTenzorCrysalis
      include 'fepc.cmn'
      include 'basic.cmn'
      real XD(3,4),AM(81),PS(9),der(9),SOL(9),MT(3,3),MTi(3,3),e1(3),
     1     e2(3),e3(3),e4(3),DirCos(3,2),xp(3)
      integer ihk(3)
      lnp=NextLogicNumber()
      call OpenFile(lnp,'Test.hkl','formatted','unknown')
      lno=NextLogicNumber()
      call OpenFile(lno,'xd.comp','formatted','unknown')
      AM=0.
      PS=0.
      MT(1,1)=1.
      MT(2,2)=1.
      MT(3,3)=1.
      MT(1,2)=cos(CellPar(6,1,KPhase)*ToRad)
      MT(2,1)=MT(1,2)
      MT(1,3)=cos(CellPar(5,1,KPhase)*ToRad)
      MT(3,1)=MT(1,3)
      MT(2,3)=cos(CellPar(4,1,KPhase)*ToRad)
      MT(3,2)=MT(2,3)
      call MatInv(MT,MTi,det,3)
3000  read(lnp,'(3i4,2f8.0,i4,6f8.5,121x,6f8.5,1x,6f8.5)',end=3333)
     1  ihk,Fp,sfp,i,((DirCos(i,j),j=1,2),i=1,3),XD
      call MultM(MTi,XD(1,3),e1,3,3,1)
      call MultM(MTi,XD(1,4),e2,3,3,1)
!      write(lno,'(2f10.5)') scalmul(XD(1,3),e2)
      der(1)=XD(1,4)**2
      der(2)=XD(2,4)**2
      der(3)=XD(3,4)**2
      der(4)=2.*XD(1,4)*XD(2,4)
      der(5)=2.*XD(1,4)*XD(3,4)
      der(6)=2.*XD(2,4)*XD(3,4)
      m=0
      do j=1,6
        derj=der(j)
        do k=1,j
          m=m+1
          AM(m)=AM(m)+derj*der(k)
        enddo
        PS(j)=PS(j)+derj
      enddo
      go to 3000
3333  call smi(AM,6,ising)
      if(ising.gt.0) then
        call FeWinMessage('Singular',' ')
        go to 9999
      endif
      call nasob(AM,PS,SOL,6)
!      write(lno,'(3f10.6)') SOL(1),Sol(4),Sol(5)
!      write(lno,'(3f10.6)') SOL(4),Sol(2),Sol(6)
!      write(lno,'(3f10.6)') SOL(5),Sol(6),Sol(3)
      MT(1,1)=SOL(1)
      MT(2,2)=SOL(2)
      MT(3,3)=SOL(3)
      MT(1,2)=SOL(4)
      MT(2,1)=SOL(4)
      MT(1,3)=SOL(5)
      MT(3,1)=SOL(5)
      MT(2,3)=SOL(6)
      MT(3,2)=SOL(6)
      call MatInv(MT,MTi,det,3)
!      MTi(1,1)=SOL(1)
!      MTi(2,2)=SOL(2)
!      MTi(3,3)=SOL(3)
!      MTi(1,2)=SOL(4)
!      MTi(2,1)=SOL(4)
!      MTi(1,3)=SOL(5)
!      MTi(3,1)=SOL(5)
!      MTi(2,3)=SOL(6)
!      MTi(3,2)=SOL(6)
!      call MatInv(MTi,MT,det,3)
      write(lno,'(3f10.6)') MT
      write(lno,'(3f10.6)') MTi
!      rewind lnp
!4000  read(lnp,'(3i4,2f8.0,i4,6f8.5,121x,6f8.5,1x,6f8.5)',end=9999)
!     1  ihk,Fp,sfp,i,((DirCos(i,j),j=1,2),i=1,3),XD
!      call MultM(MT,XD(1,3),e1,3,3,1)
!      call MultM(MT,XD(1,4),e2,3,3,1)
!      write(lno,'(3i4)') ihk
!      write(lno,'(3f10.5)') scalmul(XD(1,3),e1),scalmul(XD(1,4),e2),
!     1                      scalmul(XD(1,3),e2)
!      go to 4000
!      do i=1,3
!        xp(i)=CellPar(i,1,KPhase)*rcp(i,1,KPhase)
!      enddo
!      do j=1,2
!        do k=1,3
!          DirCos(k,j)=DirCos(k,j)*xp(k)
!        enddo
!      enddo

!      rewind lnp
!      read(lnp,'(a)')
!      AM=0.
!      PS=0.
!4000  read(lnp,'(3i4,i2,f10.0,f7.0,f6.0,6f8.5)',end=5000) ihk,i,Fp,sfp,
!     1  tt,XD
!      write(lno,'('' Reflexe:'',3i4)') ihk
!      call MultM(MT,XD(1,1),e1,3,3,1)
!      call MultM(MT,XD(1,2),e2,3,3,1)
!      write(lno,'(3f10.5)') scalmul(XD(1,1),e1),scalmul(XD(1,2),e2),
!     1                      scalmul(XD(1,2),e1)
!      m=0
!      do i=1,NRef
!        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
!     1     ihk(3).eq.iha(3,i)) then
!          m=m+1
!          ip=i
!          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
!          write(lno,'(i5,3f10.5)') m,scalmul(DirCos(1,1,i),e3),
!     1                             scalmul(DirCos(1,1,i),e1),
!     1                             scalmul(DirCos(1,2,i),e1)
!        endif
!      enddo
!      if(m.le.0) then
!        write(lno,'('' Nenasel nic'')')
!      else if(m.eq.1) then
!        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
!        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
!        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
!        call MultM(MTi,xp,e1,3,3,1)
!        call VecNor(e1,xp)
!        e3=xp
!        call VecMul(e1,DirCos(1,1,ip),xp)
!        call MultM(MTi,xp,e2,3,3,1)
!        call VecNor(e2,xp)
!        e4=xp
!        write(lno,'(6f8.5)') XD
!        write(lno,'(6f8.5)') e1,e2,e3,e4
!        call MultM(MT,DirCos(1,1,ip),e3,3,3,1)
!        write(lno,'(3f10.5)') scalmul(DirCos(1,1,ip),e3),
!     1                        scalmul(e1,e3),
!     1                        scalmul(e2,e3)
!        do i=1,3
!          der=0.
!          do j=1,3
!            der((i-1)*3+j)=e1(j)
!          enddo
!          m=0
!          do j=1,9
!            derj=der(j)
!            do k=1,j
!              m=m+1
!              AM(m)=AM(m)+derj*der(k)
!            enddo
!            PS(j)=PS(j)+derj*XD(i,1)
!          enddo
!        enddo
!      endif
!      write(lno,'('' -------------------------------------------'')')
!      go to 4000
!5000  call smi(AM,9,ising)
!      if(ising.gt.0) then
!        call FeWinMessage('Singular2',' ')
!        go to 9999
!      endif
!      call nasob(AM,PS,SOL,9)
!      write(lno,'(3f10.6)') SOL(1),Sol(2),Sol(3)
!      write(lno,'(3f10.6)') SOL(4),Sol(5),Sol(6)
!      write(lno,'(3f10.6)') SOL(7),Sol(8),Sol(9)
!      rewind lnp
!      read(lnp,'(a)')
!      write(lno,'(''Kontrola'')')
!6000  read(lnp,'(3i4,i2,f10.0,f7.0,f6.0,6f8.5)',end=9999) ihk,i,Fp,sfp,
!     1  tt,XD
!      write(lno,'('' Reflexe:'',3i4)') ihk
!      m=0
!      do i=1,NRef
!        if(ihk(1).eq.iha(1,i).and.ihk(2).eq.iha(2,i).and.
!     1     ihk(3).eq.iha(3,i)) then
!          m=m+1
!          ip=i
!          call MultM(MT,DirCos(1,1,i),e3,3,3,1)
!        endif
!      enddo
!      if(m.le.0) then
!        write(lno,'('' Nenasel nic'')')
!      else if(m.eq.1) then
!        call MultM(MT,DirCos(1,1,ip),e1,3,3,1)
!        call MultM(MT,DirCos(1,2,ip),e2,3,3,1)
!        call VecMul(DirCos(1,1,ip),DirCos(1,2,ip),xp)
!        call MultM(MTi,xp,e1,3,3,1)
!        call VecNor(e1,xp)
!        call VecMul(e1,DirCos(1,1,ip),xp)
!        call MultM(MTi,xp,e2,3,3,1)
!        call VecNor(e2,xp)
!        call MultM(e1,Sol,e3,1,3,3)
!        write(lno,'(3f8.5)') e3
!        write(lno,'(3f8.5)') XD(1:3,1)
!      endif
!      go to 6000
9999  call CloseIfOpened(lnp)
      call CloseIfOpened(lno)
      return
100   format(3i4,2f8.0,i4,6f8.5)
      end
      subroutine ioat(i1,i2,isw,nm,Klic,tisk)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(6),kmodp(7)
      character*80 Veta
      integer tisk
      logical PolMag,EqIgCase
      sumaa=0.
      sumac=0.
      sumav=0.
      do i=i1,i2
        kip=1
        if(Klic.eq.0) then
          PrvniKiAtomu(i)=PrvniKi
          call SetBasicKeysForAtom(i)
          read(m40,100,err=9000,end=9000) atom(i),isf(i),itff,lasmax(i),
     1      ai(i),(x(j,i),j=1,3),(KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
          sai(i)=0.
          call SetRealArrayTo(sx(1,i),3,0.)
          if(isf(i).le.0.or.isf(i).gt.NAtFormula(KPhase)) then
            write(Cislo,FormI15) isf(i)
            call Zhusti(Cislo)
            call FeChybne(-1.,-1.,'incorrect flag for atomic type (='//
     1                    Cislo(:idel(Cislo))//
     2                    ') of the atom '//atom(i),' ',SeriousError)
            go to 9100
          else if(itff.le.0.and.nm.le.0) then
            write(Cislo,FormI15) itff
            call Zhusti(Cislo)
            call FeChybne(-1.,-1.,'incorrect ADP flag (='//
     1                    Cislo(:idel(Cislo))//
     2                    ') of the atom '//atom(i),' ',SeriousError)
            go to 9100
          endif
          ifr(i)=itff/10
          IFrMax=max(IFrMax,ifr(i))
          itfi=mod(itff,10)
          itf(i)=itfi
          if(NDim(KPhase).le.3) then
            if(ChargeDensities) then
              lasmaxm=max(lasmax(i),lasmaxm)
              MagPar(i)=0
            else
              MagPar(i)=lasmax(i)
              lasmax(i)=0
              lasmaxm=0
            endif
          else
            MagPar(i)=lasmax(i)
            lasmax(i)=0
            if(itfi.gt.2)
     1        read(m40,104,err=9000,end=9000) Cislo,(KFA(j,i),j=4,7),
     2                                        (KModA(j,i),j=4,7)
            if(NDimI(KPhase).gt.0.and.NonModulated(KPhase)) then
              do j=1,itfi+1
                NonModulated(KPhase)=NonModulated(KPhase).and.
     1                               KModA(j,i).le.0
              enddo
              NonModulated(KPhase)=NonModulated(KPhase).and.
     1                             MagPar(i).le.1
            endif
             phf(i)=0.
            sphf(i)=0.
          endif
          do j=1,7
            if(KModA(j,i).le.0) KFA(j,i)=0
            kmodp(j)=KModA(j,i)
          enddo
          call ReallocateAtomParams(i-1,itf(i),ifr(i),kmodp,MagPar(i))
          read(m40,105,err=9000,end=9000)(beta(j,i),j=1,6),
     1                                   (kia(j,i),j=1,10)
          call SetRealArrayTo(sbeta(1,i),6,0.)
          if(itf(i).eq.1) call SetRealArrayTo(beta(2,i),5,0.)
          iswa(i)=isw
          kswa(i)=KPhase
          if(NAtFormula(KPhase).gt.0) then
            if(EqIgCase(AtType(isf(i),KPhase),'H').or.
     1         EqIgCase(AtType(isf(i),KPhase),'D')) then
              NAtH(KPhase)=NAtH(KPhase)+1
            endif
          endif
          kmol(i)=nm
          if(tisk.eq.1) then
            call newln(2)
            if(NDim(KPhase).eq.3) then
              write(lst,102) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (beta(j,i),j=1,6),
     2                       (max(kia(j,i),0),j=1,10)
            else
              write(lst,101) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3),
     2                       (beta(j,i),j=1,6),
     3                       (max(kia(j,i),0),j=1,10)
            endif
          endif
        else
          kip=1
          itfi=itf(i)
          itff=itfi+ifr(i)*10
          if(lite(KPhase).eq.0) then
            if(itfi.ne.1) then
              do j=1,6
                beta(j,i)=beta(j,i)/urcp(j,isw,KPhase)
              enddo
            else
              beta(1,i)=beta(1,i)/episq
            endif
          else if(itfi.eq.1) then
            call SetRealArrayTo(beta(2,i),5,0.)
          endif
          if(NDim(KPhase).eq.3) then
            if(ChargeDensities) then
              write(m40,100) atom(i),isf(i),itff,lasmax(i),ai(i),
     1          (x(j,i),j=1,3)
            else if(MagPar(i).gt.0) then
              write(m40,100) atom(i),isf(i),itff,MagPar(i),ai(i),
     1          (x(j,i),j=1,3)
            else
              write(m40,103) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3)
            endif
          else
            do j=itfi+2,7
              KModA(j,i)=0
            enddo
            do j=1,7
              if(KModA(j,i).le.0) KFA(j,i)=0
            enddo
            if(MagPar(i).gt.0) then
              write(m40,100) atom(i),isf(i),itff,MagPar(i),ai(i),
     1                       (x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
            else
              write(m40,103) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
            endif
            if(itfi.gt.2) write(m40,104) atom(i),(KFA(j,i),j=4,7),
     1                                   (KModA(j,i),j=4,7)
          endif
          write(m40,105)(beta(j,i),j=1,6),
     1                  (max(kia(j,i),0),j=1,10)
        endif
        if(lite(KPhase).eq.0) then
          if(itfi.ne.1) then
            do j=1,6
              beta(j,i)=beta(j,i)*urcp(j,isw,KPhase)
            enddo
          else
            beta(1,i)=beta(1,i)*episq
          endif
        endif
        kip=kip+10
        call uprat(atom(i))
        if(NDimI(KPhase).gt.0) then
          call SetRealArrayTo(qcnt(1,i),3,0.)
          call qbyx(x(1,i),qcnt(1,i),isw)
        endif
        itfmax=max(itfmax,itfi)
        if(itfi.gt.2) then
          call iost(m40,c3(1,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3(1,i),10,0.)
          kip=kip+10
        endif
        if(itfi.gt.3) then
          call iost(m40,c4(1,i),kia(kip,i),15,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc4(1,i),15,0.)
          kip=kip+15
        endif
        if(itfi.gt.4) then
          call iost(m40,c5(1,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5(1,i),21,0.)
          kip=kip+21
        endif
        if(itfi.gt.5) then
          call iost(m40,c6(1,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6(1,i),28,0.)
          kip=kip+28
        endif
        if(ifr(i).gt.0) then
          call iost(m40,xfr(i),kia(kip,i),1,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          kip=kip+1
        endif
        if(NDimI(KPhase).le.0) then
          if(ChargeDensities.and.Klic.le.0) then
            SmbPGAt(i)='1'
            NPGAt(i)=1
            call UnitMat(RPGAt(1,1,i),3)
            LocAtSystSt(1,i)=' 1.000000 0.000000 0.000000'
            LocAtSystSt(2,i)=' 0.000000 1.000000 0.000000'
            call SetRealArrayTo(LocAtSystX(1,1,i),6,0.)
            LocAtSystX(1,1,i)=1.
            LocAtSystX(2,2,i)=1.
            LocAtSystAx(i)='xy'
            LocAtSense(i)=' '
          endif
          if(lasmax(i).gt.0) then
            if(lasmax(i).gt.1) then
              j=4
            else
              j=3
            endif
            if(Klic.gt.0) then
              px(1)=popc(i)
              px(2)=popv(i)
              px(3)=kapa1(i)
              if(j.eq.4) px(4)=kapa2(i)
            endif
            call iost(m40,px,kia(kip,i),j,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            kip=kip+j
            if(Klic.le.0) then
              popc(i)=px(1)
              popv(i)=px(2)
              sumaa=sumaa+(px(1)+px(2))*ai(i)
              sumac=sumac+px(1)*ai(i)
              sumav=sumav+px(2)*ai(i)
              kapa1(i)=px(3)
              if(j.eq.4) then
                kapa2(i)=px(4)
              else
                kapa2(i)=1.
              endif
            endif
            kk=lasmax(i)-1
            if(kk.le.0) go to 1700
            if(Klic.le.0) then
              read(m40,107,err=9000,end=9000)(LocAtSystSt(j,i),j=1,2),
     1          LocAtSystAx(i),LocAtSense(i),SmbPGAt(i)
              call mala(LocAtSystAx(i))
              call mala(SmbPGAt(i))
            else
              if(LocAtSense(i).ne.'-') LocAtSense(i)=' '
              do j=1,2
                if(LocAtSystSt(j,i)(1:1).ne.'-'.and.
     1             LocAtSystSt(j,i)(1:1).ne.' ')
     2            LocAtSystSt(j,i)=' '//
     3              LocAtSystSt(j,i)(:idel(LocAtSystSt(j,i)))
              enddo
              write(m40,107)(LocAtSystSt(j,i),j=1,2),LocAtSystAx(i),
     1                       LocAtSense(i),SmbPGAt(i)
            endif
            kk=kk**2
            call iost(m40,popas(1,i),kia(kip,i),kk,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            kip=kip+kk
          endif
        endif
1700    if(KModA(1,i).gt.0) then
          if(Klic.eq.0) then
            read(m40,FormA) Veta
            k=0
            call StToReal(Veta,k,a0(i),1,.false.,ich)
            if(ich.ne.0) go to 9000
            if(k.lt.60) then
              call Kus(Veta,k,Cislo)
              if(EqIgCase(Cislo,'Ortho')) then
                TypeModFun(i)=1
                call StToReal(Veta,k,OrthoEps(i),1,.false.,ich)
                if(ich.ne.0) go to 9100
                OrthoDelta(i)=a0(i)
              else if(EqIgCase(Cislo,'Legendre')) then
                TypeModFun(i)=2
              else if(EqIgCase(Cislo,'XHarm')) then
                TypeModFun(i)=3
              endif
            else
              TypeModFun(i)=0
            endif
            call StToInt(Veta,k,kia(kip,i),1,.false.,ich)
            sa0(i)=0.
          else
            if(TypeModFun(i).eq.1) then
              Cislo='Ortho'
            else if(TypeModFun(i).eq.2) then
             Cislo='Legendre'
            else if(TypeModFun(i).eq.3) then
              Cislo='XHarm'
            else
              Cislo=' '
            endif
            if(TypeModFun(i).eq.1) then
              write(Veta,109) a0(i),Cislo(1:8),OrthoEps(i),kia(kip,i)
            else
              write(Veta,108) a0(i),Cislo(1:8),kia(kip,i)
            endif
            write(m40,FormA) Veta(:idel(Veta))
          endif
          kip=kip+1
          if(KFA(1,i).ne.0) SoucinAy=1.
          do j=1,KModA(1,i)
            if(Klic.ne.0) then
              px(1)=ax(j,i)
              px(2)=ay(j,i)
            endif
            call iost(m40,px,kia(kip,i),2,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(TypeModFun(i).eq.1.and.j.eq.KModA(1,i).and.Klic.eq.0)
     1        OrthoX40(i)=px(1)
            kip=kip+2
            if(ErrFlag.ne.0) go to 9999
            if(Klic.eq.0) then
               ax(j,i)=px(1)
               ay(j,i)=px(2)
              sax(j,i)=0.
              say(j,i)=0.
            endif
            if(KFA(1,i).ne.0.and.j.le.NDimI(KPhase).and.
     1         NDimI(KPhase).gt.1) SoucinAy=SoucinAy*ay(j,i)
          enddo
          if(KFA(1,i).ne.0.and.KModA(1,i).ge.NDimI(KPhase)) then
            if(NDim(KPhase).gt.4) then
              if(abs(SoucinAy).lt..0001) then
                call SetRealArrayTo(ay(KModA(1,i)-NDim(KPhase)-2,i),
     1            NDimI(KPhase),a0(i)**(1./float(NDimI(KPhase))))
              else
                 a0(i)=SoucinAy
                sa0(i)=0.
              endif
            else if(NDim(KPhase).eq.4) then
              ay(KModA(1,i),i)=0.
            endif
          endif
        else
           a0(i)=1.
          sa0(i)=0.
        endif
        do j=1,KModA(2,i)
          if(Klic.ne.0) then
            call CopyVek(ux(1,j,i),px   ,3)
            call CopyVek(uy(1,j,i),px(4),3)
          endif
          call iost(m40,px,kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          kip=kip+6
          if(Klic.eq.0) then
            call CopyVek(px   ,ux(1,j,i),3)
            call CopyVek(px(4),uy(1,j,i),3)
            if(Klic.eq.0) then
              call SetRealArrayTo(sux(1,j,i),3,0.)
              call SetRealArrayTo(suy(1,j,i),3,0.)
            endif
          endif
        enddo
        do j=1,KModA(3,i)
          if(Klic.ne.0.and.lite(KPhase).eq.0) then
            do l=1,6
              bx(l,j,i)=bx(l,j,i)/urcp(l,isw,KPhase)
              by(l,j,i)=by(l,j,i)/urcp(l,isw,KPhase)
            enddo
          endif
          call iost(m40,bx(1,j,i),kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sbx(1,j,i),6,0.)
          kip=kip+6
          call iost(m40,by(1,j,i),kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sby(1,j,i),6,0.)
          kip=kip+6
          if(lite(KPhase).eq.0) then
            do l=1,6
              bx(l,j,i)=bx(l,j,i)*urcp(l,isw,KPhase)
              by(l,j,i)=by(l,j,i)*urcp(l,isw,KPhase)
            enddo
          endif
        enddo
        do j=1,KModA(4,i)
          call iost(m40,c3x(1,j,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3x(1,j,i),10,0.)
          kip=kip+10
          call iost(m40,c3y(1,j,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3y(1,j,i),10,0.)
          kip=kip+10
        enddo
        do j=1,KModA(5,i)
          call iost(m40,c4x(1,j,i),kia(kip,i),15,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc4x(1,j,i),15,0.)
          kip=kip+15
          call iost(m40,c4y(1,j,i),kia(kip,i),15,Klic,tisk)
          if(Klic.eq.0) call SetRealArrayTo(sc4y(1,j,i),15,0.)
          kip=kip+15
        enddo
        do j=1,KModA(6,i)
          call iost(m40,c5x(1,j,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5x(1,j,i),21,0.)
          kip=kip+21
          call iost(m40,c5y(1,j,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5y(1,j,i),21,0.)
          kip=kip+21
        enddo
        do j=1,KModA(7,i)
          call iost(m40,c6x(1,j,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6x(1,j,i),28,0.)
          kip=kip+28
          call iost(m40,c6y(1,j,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6y(1,j,i),28,0.)
          kip=kip+28
        enddo
        do j=1,7
          if(KModA(j,i).gt.0) then
            call iost(m40,phf(i),kia(kip,i),1,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(Klic.eq.0) sphf(i)=0.
            kip=kip+1
            exit
          endif
        enddo
        if(MagneticType(KPhase).gt.0) then
          PolMag=ktat(NamePolar,NPolar,Atom(i)).gt.0
          do j=1,MagPar(i)
            if(j.eq.1) then
              if(Klic.gt.0) then
                if(PolMag) then
                  call CopyVek(sm0(1,i),px,3)
                else
                  do k=1,3
                    px(k)=sm0(k,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
              n=3
            else
              if(Klic.gt.0) then
                if(PolMag) then
                  call CopyVek(smx(1,j-1,i),px(1),3)
                  call CopyVek(smy(1,j-1,i),px(4),3)
                else
                  do k=1,3
                    px(k  )=smx(k,j-1,i)*CellPar(k,isw,KPhase)
                    px(k+3)=smy(k,j-1,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
              n=6
            endif
            call iost(m40,px,kia(kip,i),n,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(Klic.le.0) then
              if(j.eq.1) then
                if(PolMag) then
                  call CopyVek(px,sm0(1,i),3)
                else
                  do k=1,3
                    sm0(k,i)=px(k)/CellPar(k,isw,KPhase)
                  enddo
                endif
              else
                if(PolMag) then
                  call CopyVek(px(1),smx(1,j-1,i),3)
                  call CopyVek(px(4),smy(1,j-1,i),3)
                else
                  do k=1,3
                    smx(k,j-1,i)=px(k  )/CellPar(k,isw,KPhase)
                    smy(k,j-1,i)=px(k+3)/CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
            endif
            kip=kip+n
          enddo
        endif
        if(Klic.eq.0) then
          if(kip.le.mxda) call SetIntArrayTo(KiA(kip,i),mxda-kip+1,0)
          PrvniKi=PrvniKi+kip-1
          DelkaKiAtomu(i)=kip-1
        endif
      enddo
      NAtCalc=NAtInd
      if(Klic.eq.0.and.ChargeDensities) then
        do i=i1,i2
          kk=lasmax(i)
          call CopyMat(TrToOrtho (1,1,KPhase),TrAt (1,i),3)
          call CopyMat(TrToOrthoI(1,1,KPhase),TriAt(1,i),3)
          call UnitMat(TroAt (1,i),3)
          call UnitMat(TroiAt(1,i),3)
          if(kk.le.1) cycle
          call CrlMakeTrMatToLocal(x(1,i),atom(i),LocAtSystAx(i),
     1      LocAtSystSt(1,i),LocAtSense(i),LocAtSystX(1,1,i),TrAt(1,i),
     2      TriAt(1,i),0,ich)
          if(ich.ne.0) go to 9100
          call Multm(TrAt(1,i),TrToOrthoI(1,1,KPhase),TroAt(1,i),3,3,3)
          call MatInv(TroAt(1,i),TroiAt(1,i),pom,3)
        enddo
      endif
!      write(6,'(3f10.6)') sumaa,sumac,sumav
      go to 9999
9000  ErrFlag=1
      go to 9999
9050  call FeChybne(-1.,-1.,'in the local coordinate system for : '//
     1              atom(i)(:idel(atom(i))),' ',SeriousError)
9100  ErrFlag=2
9999  return
100   format(a8,3i3,1x,4f9.6,6x,3i1,3i3)
101   format(1x,a8,2i3,4x,4f9.6,3x,'>>',3i1,3i3,'<<'/
     1       1x,6f9.6,3x,'=>',10i1,'<=')
102   format(1x,a8,2i3,4x,4f9.6/1x,6f9.6,3x,'=>',10i1,'<=')
103   format(a8,2i3,4x,4f9.6,6x,3i1,3i3)
104   format(a8,52x,4i1,4i3)
105   format(6f9.6,6x,10i1)
106   format(7(i2,f7.3))
107   format(2a27,2x,a2,a1,1x,a8)
108   format(f9.6,10x,a8,33x,i1)
109   format(f9.6,10x,a8,f9.6,24x,i1)
      end
      subroutine DPopul
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real :: Dmat(30) = (/ 0.200, 0.200, 0.200, 0.200, 0.200,
     1                      1.040, 0.520, 0.520,-1.040,-1.040,
     2                      0.000, 0.943,-0.943, 0.000, 0.000,
     3                      1.400,-0.931,-0.931, 0.233, 0.233,
     4                      0.000, 1.110,-1.110, 0.000, 0.000,
     5                      0.000, 0.000, 0.000, 1.570,-1.570/),
     a        DmatM(110) = (/ 1.090, 0.000, 0.000, 0.000, 0.000,
     1                        1.880, 0.000, 0.000, 1.880, 0.000,
     2                        0.000, 1.090, 0.000, 0.000, 0.000,
     3                        0.000, 1.880,-1.880, 0.000, 0.000,
     4                        0.000, 0.000,-2.180, 0.000, 0.000,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000,-1.850, 1.600,
     7                        0.000, 0.000, 0.000, 0.000, 0.000,
     8                        3.680, 0.000, 0.000, 0.000, 0.000,
     9                       -1.060, 0.000, 0.000,-1.060, 0.000,
     a                        0.000, 3.680, 0.000, 0.000, 0.000,
     1                        0.000,-1.060, 1.060, 0.000, 0.000,
     2                        0.000, 0.000, 1.920, 0.000, 0.000,
     3                        0.000, 0.000, 0.000, 0.000, 0.000,
     4                        0.000, 0.000, 0.000, 2.300, 1.880,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000, 0.000, 0.000,
     7                        2.100, 0.000, 0.000,-2.100, 0.000,
     8                        0.000, 0.000, 0.000, 0.000, 0.000,
     9                        0.000, 2.100, 2.100, 0.000, 0.000,
     a                        0.000, 0.000, 0.000, 0.000, 0.000,
     1                        0.000, 0.000, 0.000, 0.000, 3.140/),
     a        xp(11),dpop(10),sdpop(10)
      open(33,file=fln(:ifln)//'.dpop')
      do i=1,1
        write(33,'(''Atom:'',a)') Atom(i)
!        xp(1)=popv(i)+popas(1,i)
        xp(1)=popv(i)+popas(1,i)
        xp(2)=popas(5,i)
        xp(3)=popas(8,i)
        xp(4)=popas(17,i)
        xp(5)=popas(20,i)
        xp(6)=popas(24,i)
        write(33,'(7f10.4)') xp(1:6)
        call MultM(DMat,xp,dpop,5,6,1)
        xp(1)=spopv(i)+spopas(1,i)
        xp(2)=spopas(5,i)
        xp(3)=spopas(8,i)
        xp(4)=spopas(17,i)
        xp(5)=spopas(20,i)
        xp(6)=spopas(24,i)
        call MultMQ(DMat,xp,sdpop,5,6,1)
        SumDpop=0.
        do j=1,5
          SumDpop=SumDPop+DPop(j)
        enddo
        write(33,'(''     z2        xz        yz       x2-y2      xy'',
     1             ''        sum'')')
        write(33,'(7f10.4)') dpop(1:5),SumDPop
        write(33,'(6f10.4)') sdpop(1:5)
        xp=0.
        xp(1)=popas(6,i)
        xp(2)=popas(7,i)
        xp(3)=popas(8,i)
        xp(4)=popas(9,i)
        xp(5)=popas(18,i)
        xp(6)=popas(19,i)
        xp(7)=popas(20,i)
        xp(8)=popas(21,i)
        xp(9)=popas(22,i)
        xp(10)=popas(23,i)
        xp(11)=popas(25,i)
        ppp=xp(11)*DMatM(110)
        write(33,'(10f10.4)') xp(1:11)
        do ii=1,10
          write(33,'('' Radek:'',i3,20f8.3)') ii,
     1      (DMatM(ii+(jj-1)*10),jj=1,11)
        enddo
        call MultM(DMatM,xp,dpop,10,11,1)
        xp(1)=spopas(6,i)
        xp(2)=spopas(7,i)
        xp(3)=spopas(8,i)
        xp(4)=spopas(9,i)
        xp(5)=spopas(18,i)
        xp(6)=spopas(19,i)
        xp(7)=spopas(20,i)
        xp(8)=spopas(21,i)
        xp(9)=spopas(22,i)
        xp(10)=spopas(23,i)
        xp(11)=spopas(25,i)
        call MultMQ(DMatM,xp,sdpop,10,11,1)
        write(33,'(11f10.4)') dpop(1:10),ppp
        write(33,'(10f10.4)') sdpop(1:10)
      enddo
      close(33)
      return
      end
      subroutine TestHKL
      integer ih(6)
      open(33,file='305-full_tP.hkl')
      nall=0
      nobs=0
1100  read(33,100,end=2000) ih,ri,sri
      if(ih(4).ne.0.and.ih(5).ne.0) then
        nall=nall+1
        if(ri.gt.3.*sri) then
          write(6,100) ih,ri,sri
          nobs=nobs+1
        endif
      endif
      go to 1100
2000  write(6,'(2i6)') nall,nobs
      close(33)
      return
100   format(6i4,2f8.2)
      end
      subroutine PrevodHKL
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom95(0,fln(:ifln)//'.m95')
      open(33,file='kus1.hkl')
      open(34,file='kus2.hkl')
      call OpenRefBlockM95(95,1,fln(:ifln)//'.m95')
1100  call DRGetReflectionFromM95(95,iend,ich)
      if(ich.ne.0) go to 9000
      if(iend.ne.0) go to 2000
      pom1=corrf(1)*corrf(2)
      if(RunScN(KRefBlock).gt.0) then
        ikde=min((RunCCD-1)/RunScGr(KRefBlock)+1,
     1            RunScN(KRefBlock))
        pom1=pom1*ScFrame(ikde,KRefBlock)
      endif
      ri=ri*pom1
      rs=rs*pom1
      write(6,'(5i5,2f10.1)') ih(1:5),ri,rs
      if(ih(5).eq.0) then
        write(33,100) ih(1:4),ri,rs
      else if(ih(4).eq.0) then
        write(34,100) ih(2),ih(1),-ih(3),ih(5),ri,rs
      endif
      go to 1100
2000  close(33)
      close(34)
      go to 9999
9000  call FeChybne(-1.,-1.,'chyba pri cteni m95',' ',SeriousError)
      go to 9999
9999  return
100   format(4i4,2f9.1)
      end
      subroutine Preoprav
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      integer ihp(3),ihpp(3)
      real XDDirCosP(3,2),DirCosP(3,2),MT(3,3),MTi(3,3),TrDiag(3),
     1     so(3,2),xp(9)
      logical EqIV
      MT(1,1)=1.
      MT(2,2)=1.
      MT(3,3)=1.
      MT(1,2)=cos(CellPar(6,1,KPhase)*ToRad)
      MT(2,1)=MT(1,2)
      MT(1,3)=cos(CellPar(5,1,KPhase)*ToRad)
      MT(3,1)=MT(1,3)
      MT(2,3)=cos(CellPar(4,1,KPhase)*ToRad)
      MT(3,2)=MT(2,3)
      do i=1,3
        TrDiag(i)=rcp(i,1,KPhase)*CellPar(i,1,KPhase)
      enddo
      call MatInv(MT,MTi,det,3)
      lnp=NextLogicNumber()
      call OpenFile(lnp,'CuActetat100K_15175.hkl','formatted','unknown')
      lni=NextLogicNumber()
      call OpenFile(lni,'XD_compatible_15175.hkl','formatted','unknown')
      read(lni,FormA) Veta
      lno=NextLogicNumber()
      call OpenFile(lno,'xd.hkl','formatted','unknown')
      write(lno,FormA) Veta(:idel(Veta))
2100  read(lnp,formatshelx,end=3000) ihp,Fobs,SFobs,i,
     1                               ((DirCosP(i,j),j=1,2),i=1,3)
      do j=1,2
        do k=1,3
          so(k,j)=DirCosP(k,j)*TrDiag(k)
        enddo
      enddo
      call VecMul(so(1,1),so(1,2),xp)
      call MultM(MTi,xp,XDDirCosP(1,1),3,3,1)
      call VecNor(XDDirCosP(1,1),xp)
      call VecMul(XDDirCosP(1,1),so(1,1),xp)
      call MultM(MTi,xp,XDDirCosP(1,2),3,3,1)
      call VecNor(XDDirCosP(1,2),xp)
      read(lni,*) ihpp,i,Fobs,SFobs,tbar,DirCosP
      if(.not.EqIV(ihpp,ihp,3)) then
        write(Veta,'('' Nesoulad:'',6i4)') ihp,ihpp
        call FeWinMessage(Veta,' ')
      endif
      write(lno,'(4i4,2f8.2,7f9.4)') ihp,1,Fobs,SFobs,tbar,XDDirCosP
!      do j=1,2
!        do i=1,3
!          if(abs(XDDirCosP(i,j)).gt.1.) then
!            write(6,formatshelx) ihp,i,Fobs,SFobs,XDDirCosP
!            go to 2100
!          endif
!        enddo
!      enddo
      go to 2100
3000  close(lnp)
      close(lno)
      close(lni)
      return
      end
      subroutine Nasobky
      integer ix(3,100),ixp(3)
      logical EqIV
      ix=0
      ix(1,2)=2
      ix(2,2)=2
      ix(3,2)=0
      ix(1,3)=-2
      ix(2,3)=2
      ix(3,3)=0
      ix(1,4)=3
      ix(2,4)=0
      ix(3,4)=3
      n=4
      do i1=-3,3
        do i2=-3,3
          do 2000i3=-3,3
            do i=1,3
              ixp(i)=i1*ix(i,2)+i2*ix(i,3)+i3*ix(i,4)
1100          if(ixp(i).ge.6) then
                ixp(i)=ixp(i)-6
                go to 1100
              endif
1200          if(ixp(i).lt.0) then
                ixp(i)=ixp(i)+6
                go to 1200
              endif
            enddo
            do j=1,n
              if(EqIV(ix(1,j),ixp,3)) go to 2000
            enddo
            n=n+1
            ix(1:3,n)=ixp(1:3)
            if(n.eq.100) go to 3000
2000      continue
        enddo
      enddo
3000  write(6,'('' Pocet:'',i4)') n
      do i=1,n
        write(6,'(i5,3f10.6)') i,(float(ix(j,i))/6.,j=1,3)
      enddo
      return
      end
      subroutine OdJeffa1
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*3 At
      real xp(3)
      logical EqIgCase
!      KRefBlock=1
!      NRefBlock=1
!      SourceFileRefBlock(KRefBlock)='sc_refs_fixed.txt'
!      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
!     1                   SourceFileDateRefBlock(KRefBlock),
!     2                   SourceFileTimeRefBlock(KRefBlock))
!      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
!      do i=1,NDimI(KPhase)
!        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,KRefBlock),3)
!      enddo
!      call UnitMat(TrMP,NDim(KPhase))
!      CellReadIn(KRefBlock)=.true.
!      CorrLp(KRefBlock)=-1
!      CorrAbs(KRefBlock)=-1
!      LamAve(1)=1.5418
!      LamAveRefBlock(KRefBlock)=LamAve(1)
!      DifCode(KRefBlock)=IdImportCIF
!      RadiationRefBlock(KRefBlock)=XRayRadiation
!      AngleMonRefBlock(KRefBlock)=
!     1  CrlMonAngle(MonCell(1,3),MonH(1,3),LamAveRefBlock(KRefBlock))
!      AlphaGMonRefBlock(KRefBlock)=0.
!      BetaGMonRefBlock(KRefBlock)=0.
!      TempRefBlock(KRefBlock)=293
!      UseTrRefBlock(KRefBlock)=.false.
!      ITwRead(KRefBlock)=1
!      if(isPowder) then
!        PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
!        DifCode(KRefBlock)=IdDataCIF
!      else
!        if(Radiation(KDatBlock).eq.NeutronRadiation) then
!          PolarizationRefBlock(KRefBlock)=PolarizedLinear
!        else
!          PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
!        endif
!        DifCode(KRefBlock)=IdImportCIF
!      endif
!      RefBlockFileName=fln(:ifln)//'.l01'
!      ln=NextLogicNumber()
!      call OpenFile(ln,'sc_refs_fixed.txt','formatted','unknown')
!      call OpenFile(95,RefBlockFileName,'formatted','unknown')
!      tbar=0.
!      call SetRealArrayTo(uhly,4,0.)
!      call SetRealArrayTo(dircos,6,0.)
!      call SetRealArrayTo(corrf,2,1.)
!      iflg(1)=1
!      iflg(2)=1
!      KProf=0
!      NProf=0
!      DRlam=0.
!      NRef95(KRefBlock)=0
!      NLines95(KRefBlock)=0
!1100  read(ln,'(3i4,f8.2)',end=1500) ih(1:3),ri
!!      rs=ri
!!      ri=ri**2
!      rs=sqrt(ri)
!      NRef95(KRefBlock)=NRef95(KRefBlock)+1
!      no=NRef95(KRefBlock)
!      expos=float(NRef95(KRefBlock))*.1
!      call DRPutReflectionToM95(95,n)
!      NLines95(KRefBlock)=NLines95(KRefBlock)+2
!      go to 1100
!1500  call iom95(1,fln(:ifln)//'.m95')
!      call CloseIfOpened(ln)
!      call CloseIfOpened(95)
!      call CompleteM95(0)
!      ExistM95=.true.
      call ReallocateAtoms(500)
      KamMax=500
      Kam=0
      NAtIndLen(1,KPhase)=0
      KPhase=1
      call OpenFile(ln,'Super-ref-1.txt','formatted','unknown')
2100  read(ln,'(i6,a3,3f8.2,2f6.2)',end=2500) n,At,xp,aip,BisoP
      call Zhusti(At)
      Kam=Kam+1
      if(Kam.gt.KamMax) then
        call ReallocateAtoms(KamMax*2)
        KamMax=KamMax*2
      endif
      call SetBasicKeysForAtom(Kam)
      isf(Kam)=1
      itf(Kam)=1
      do i=1,NAtFormula(KPhase)
        if(EqIgCase(At,AtType(i,KPhase))) then
          isf(Kam)=i
          exit
        endif
      enddo
      if(Kam.gt.1) then
        PrvniKiAtomu(Kam)=PrvniKiAtomu(Kam-1)+mxda
      else
        PrvniKiAtomu(Kam)=ndoff
      endif
      DelkaKiAtomu(Kam)=10
      write(Cislo,'(''-'',i5,''-'',i5)') (Kam-1)/7+1,mod(Kam-1,7)+1
      call Zhusti(Cislo)
      Atom(Kam)=At(1:idel(At))//Cislo
      ai(Kam)=aip
      kswa(Kam)=KPhase
      iswa(Kam)=1
       Beta(1,Kam)=BisoP
      SBeta(1,Kam)=0.
      sx(1:3,Kam)=0.
      do i=1,3
        x(i,Kam)=xp(i)/CellPar(i,1,KPhase)
      enddo
      NAtIndLen(1,KPhase)=NAtIndLen(1,KPhase)+1
      call EM40UpdateAtomLimits
      go to 2100
2500  call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
      call CloseIfOpened(ln)
      return
      end
      subroutine OdJeffaMod
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real :: Stred(3),qp(3)=(/0.,.28571429,0./)
      real, allocatable :: XComm(:,:),XModO(:,:),XModN(:,:),PS(:,:),
     1                     AM(:),der(:)
      integer kmod(3)
      kharm=2
      nlsq=2*kharm+1
      nam=nlsq*(kharm+1)
      ncomm=7
      if(allocated(XComm)) deallocate(XComm,XModO,XModN,PS,AM,der)
      allocate(XComm(4,ncomm),XModO(nlsq,3),XModN(nlsq,3),PS(nlsq,3),
     1         AM(nam),der(nlsq))
      der(1)=1.
      call OpenFile(66,fln(:ifln)//'.txt','formatted','unknown')
      do ip=1,NAtCalc,7
        j=0
        Stred=0.
        do ia=ip,ip+6
          j=j+1
          XComm(1:3,j)=x(1:3,ia)
          pom=7.*XComm(2,j)
          XComm(4,j)=pom*qp(2)
          XComm(2,j)=pom-anint(pom)
          if(j.gt.1.and.abs(XComm(2,j)-XComm(2,1)).gt..5) then
            if(XComm(2,j)-XComm(2,1).gt..5) then
              XComm(2,j)=XComm(2,j)-1.
            else if(XComm(2,j)-XComm(2,1).lt..5) then
              XComm(2,j)=XComm(2,j)+1.
            endif
          endif
          do k=1,3
            Stred(k)=Stred(k)+XComm(k,j)
          enddo
!          write(66,'(a8,3f9.6)') Atom(ia),XComm(1:3,j)
        enddo
        do k=1,3
          Stred(k)=Stred(k)/7.
        enddo
        XModO=0.
        iter=0
3999    iter=iter+1
        PS=0.
        AM=0.
        do k=1,7
          arg=pi2*(XComm(4,k)-qp(1)*(XComm(1,k)-stred(1))
     1                       -qp(2)*(XComm(2,k)-stred(2))
     2                       -qp(3)*(XComm(3,k)-stred(3)))
          j=0
          do i=1,kharm
            pom=float(i)*arg
            j=j+2
            der(j)=sin(pom)
            der(j+1)=cos(pom)
          enddo
          call FouPeaksSuma(der,XComm(1,k),AM,PS,nlsq)
        enddo
        call smi(AM,nlsq,ising)
        if(ising.eq.1) then
          call FeWinMessage('Singularita',' ')
        endif
        do i=1,3
          call nasob(AM,PS(1,i),XModN(1,i),nlsq)
        enddo
        do i=1,3
          stred(i)=XModN(1,i)
        enddo
        pom=0.
        pomc=0.
        do i=1,3
          do j=1,nlsq
            pom=pom+abs(XModN(j,i)-XModO(j,i))
            XModO(j,i)=XModN(j,i)
            if(j.gt.1) pomc=pomc+abs(XModN(j,i))
          enddo
        enddo
        if(pom.gt..001.and.iter.le.5) go to 3999
        kmod=0
        if(pomc.gt..001) kmod(2)=kharm
        write(6,'(1x,a8,f10.6)') Atom(ip),pomc
        write(66,103) Atom(ip),isf(ip),itf(ip),1.,
     1                       (XModN(1,i),i=1,3),(0,i=1,3),kmod
        write(66,'(6f9.6)') beta(1:6,ip)
        if(kmod(2).gt.0)
     1    write(66,'(6f9.6)')((XModN(j,i),i=1,3),j=2,3)
        if(kmod(2).gt.1)
     1    write(66,'(6f9.6)')((XModN(j,i),i=1,3),j=4,5)
        if(kmod(2).gt.2)
     1    write(66,'(6f9.6)')((XModN(j,i),i=1,3),j=6,7)
        if(kmod(2).gt.0)
     1    write(66,'(6f9.6)') 0.
      enddo
      call CloseIfOpened(66)
      if(allocated(XComm)) deallocate(XComm,XModO,XModN,PS,AM,der)
      return
103   format(a8,2i3,4x,4f9.6,6x,3i1,3i3)
      end
      subroutine OdJeffa
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      character*256 Veta
      character*3 At
      real xp(3),xmod(3,2),phimod(3,2)
      logical EqIgCase
      call ReallocateAtoms(500)
      KamMax=500
      Kam=0
      NAtIndLen(1,KPhase)=0
      KPhase=1
      ln=NextLogicNumber()
      call OpenFile(ln,'toxd2015a_mod_atoms.csv','formatted','unknown')
2100  read(ln,FormA,end=2500) Veta
      do i=1,idel(Veta)
        if(Veta(i:i).eq.',') Veta(i:i)=' '
      enddo
      read(Veta,*,end=2500) n,At,xp,aip,BisoP,Klic,
     1                      ((xmod(i,j),phimod(i,j),j=1,2),i=1,3)
      call Zhusti(At)
      Kam=Kam+1
      if(Kam.gt.KamMax) then
        call ReallocateAtoms(KamMax*2)
        KamMax=KamMax*2
      endif
      call SetBasicKeysForAtom(Kam)
      isf(Kam)=1
      itf(Kam)=1
      do i=1,NAtFormula(KPhase)
        if(EqIgCase(At,AtType(i,KPhase))) then
          isf(Kam)=i
          exit
        endif
      enddo
      if(Kam.gt.1) then
        PrvniKiAtomu(Kam)=PrvniKiAtomu(Kam-1)+mxda
      else
        PrvniKiAtomu(Kam)=ndoff
      endif
      DelkaKiAtomu(Kam)=10
      write(Cislo,'(''-'',i5,''-'',i5)') Kam,1
      call Zhusti(Cislo)
      Atom(Kam)=At(1:idel(At))//Cislo
      ai(Kam)=aip
      kswa(Kam)=KPhase
      iswa(Kam)=1
       Beta(1,Kam)=BisoP
      SBeta(1,Kam)=0.
      sx(1:3,Kam)=0.
      do i=1,3
        x(i,Kam)=xp(i)/CellPar(i,1,KPhase)
      enddo
      kmoda(1:3,Kam)=0
      if(Klic.gt.0) kmoda(2,Kam)=2
      do k=1,2
        do i=1,3
           ux(i,k,Kam)=-xmod(i,k)*sin(phimod(i,k))/CellPar(i,1,KPhase)
           uy(i,k,Kam)= xmod(i,k)*cos(phimod(i,k))/CellPar(i,1,KPhase)
          sux(i,k,Kam)=0.
          suy(i,k,Kam)=0.
        enddo
      enddo
      NAtIndLen(1,KPhase)=NAtIndLen(1,KPhase)+1
      call EM40UpdateAtomLimits
      go to 2100
2500  call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
      call CloseIfOpened(ln)
      return
      end
      subroutine ProJeffa
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      real xp(3),phip(3)
      open(45,file='Back.cvs')
      do i=1,NAtCalc
        if(kmoda(2,i).le.0) cycle
        write(45,'(a8,3f10.4)')
     1    Atom(i),(x(j,i)*CellPar(j,1,KPhase),j=1,3)
        do k=1,KModA(2,i)
          do j=1,3
            xp(j)=sqrt(ux(j,k,i)**2+uy(j,k,i)**2)*CellPar(j,1,KPhase)
            phip(j)=atan2(uy(j,k,i),ux(j,k,i))
          enddo
          write(45,'(8x,6f10.4)')
     1      (xp(j),phip(j),j=1,3)
        enddo
      enddo
      call CloseIfOpened(45)
      return
      end
      subroutine JeffPhases
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ih(6)
      lni=NextLogicNumber()
      call OpenFile(lni,fln(:ifln)//'.m80','formatted','unknown')
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'.m84','formatted','unknown')
1100  read(lni,Format80,end=2000) ih(1:NDim(KPhase)),i,Fobs,Fobs,Fcalc,
     1                           Acalc,Bcalc
      write(lno,'(4i4,2f15.2,f10.3)') ih(1:NDim(KPhase)),FObs,Fcalc,
     1                                atan2(Bcalc,Acalc)/ToRad
      go to 1100
2000  close(lni)
      close(lno)
      return
      end




!  Update hkl



      subroutine CrlNormalProbabilityPlot
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(3),xnorm(:),FobsA(:),FcalcA(:),
     1          sigFA(:),wdyA(:),am(3),ps(2)
      integer ipor(:),iwdy(:)
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(6) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Change %b        ',
     5                'Change %c        ',
     6                '%Optimal         '/)
      allocatable ipor,iwdy,xnorm,FobsA,FcalcA,sigFA,wdyA
      Tiskne=.false.
      ln=0
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA) Veta
      i=LocateSubstring(Veta,'e',.false.,.true.)
      if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
        format83a=format83e
      else
        format83a=format83
      endif
      rewind ln
      n=0
1100  read(ln,format83a,end=1200)
      n=n+1
      go to 1100
1200  allocate(ipor(n),iwdy(n),xnorm(n),FobsA(n),FcalcA(n),sigFA(n),
     1         wdyA(n))
      rewind ln
      n=0
1300  read(ln,format83a,end=1400)(j,i=1,maxNDim),(pom,i=1,3),
     1  Cislo,itw,wdy,Fobs,Fcalc,sigF
      n=n+1
      wdyA(n)=wdy
      FobsA(n)=Fobs
      FcalcA(n)=Fcalc
      sigFA(n)=sigF
      go to 1300
1400  call CloseIfOpened(ln)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
c        else if(i.eq.4) then
c          nButtDPlot=ButtonLastMade
        else if(i.eq.4) then
          nButtChangeB=ButtonLastMade
        else if(i.eq.5) then
          nButtChangeC=ButtonLastMade
        else if(i.eq.6) then
          nButtOptimal=ButtonLastMade
        endif
        j=ButtonOff
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.6) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      KPlot=0
      wta=0
      wtb=1.
      wtc=0.
2000  FMax=0.
      do i=1,n
        if(KPlot.eq.0) then
          pom=wdyA(i)
        else
          wt=sqrt(wta+wtb*sigFA(i)**2+wtc*FobsA(i)**2)
          pom=(FobsA(i)-FcalcA(i))/wt
        endif
        iwdy(i)=nint(pom*1000.)
        FMax=max(abs(pom),FMax)
      enddo
      call indexx(n,iwdy,ipor)
      XMax=0.
      do i=1,n
        j=ipor(i)
        xnorm(j)=erfi(float(-n+2*i-1)/float(n))
        XMax=max(XMax,xnorm(j))
      enddo
      HardCopy=0
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        rewind ln
        yomn=-FMax*1.1
        yomx= FMax*1.1
        xomn=-XMax*1.1
        xomx= XMax*1.1
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Normal')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'wdy')
        xpp(3)=0.
        do i=1,n
          j=ipor(i)
          xpp(1)=xnorm(j)
          xpp(2)=float(iwdy(j))*.001
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
        enddo
        if(FMax.gt.XMax) then
          xpp(1)=xomx
          xpp(2)=xomx
        else
          xpp(1)=yomx
          xpp(2)=yomx
        endif
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=-xpp(1)
        xpp(2)=-xpp(2)
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEVent(id,ich)
      if(CheckNumber.eq.nButtQuit) then
        go to 8000
      if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtChangeB)
     1  then
        wtb=wtb*1.1
        KPlot=1
        go to 2000
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtChangeC)
     1  then
        wtc=wtc+.0001
        KPlot=1
        go to 2000
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtOptimal)
     1  then
        wtb=sqrt(FMax/3.)
        do iii=1,1
3000    call SetRealArrayTo(am,3,0.)
        call SetRealArrayTo(ps,2,0.)
        Suma=0.
c        if(wtc.le.0.) wtc=.01
        do i=1,n
          wt=sqrt(wta+wtb*sigFA(i)**2+wtc*FobsA(i)**2)
          delta=(FobsA(i)-FcalcA(i))/wt
          pom=-delta/wt**3
          xo(1)=.5*pom*sigFA(i)**2
          xo(2)=.5*pom*FobsA(i)**2
          m=0
          do j=1,2
            do k=1,j
              m=m+1
              am(m)=am(m)+xo(j)*xo(k)
            enddo
            ps(j)=ps(j)-xo(j)*(delta-xnorm(i))
          enddo
          Suma=Suma+(delta-xnorm(i))**2
        enddo
        ps(1)=0.
c        write(Veta,'(e15.5)') Suma
c        call FeWinMessage(Veta,' ')
        call smi(am,2,ising)
        if(ising.le.0) then
          call nasob(am,ps,xo,2)
c          wtb=sqrt(abs(wtb**2+xo(1)))
c          wtc=sqrt(abs(wtc**2+xo(2)))
          wtb=wtb+xo(1)
          wtc=wtc+xo(2)
c          pause
c          write(Veta,'(''Oprava:'',4f10.6)') xo(1),xo(2),wtb,wtc
c          call FeWinMessage(Veta,' ')
c          go to 3000
        else
          call FeWinMessage('Singular',' ')
        endif
        enddo
        KPlot=1
        go to 2000
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  if(allocated(ipor)) deallocate(ipor,iwdy,xnorm,FobsA,FcalcA,sigFA,
     1                               wdyA)
      return
      end
      subroutine ConDensityInCell(ich)
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real xp(6),DenSupl(3),xn(6)
      real, allocatable :: Table(:)
      character*80 Veta
      integer :: DensityTypeSave=2
      logical ExistFile
      do i=1,6
        iorien(i)=i
      enddo
      DensityType=DensityTypeSave
      call SetLogicalArrayTo(AtBrat,NAtCalc,.true.)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      allocate(DenCore(240,MaxNAtFormula),DenVal(240,MaxNAtFormula),
     1         DenCoreDer(240,2,MaxNAtFormula),
     2         DenValDer(240,2,MaxNAtFormula))
      call ConSetLocDensity
      call CPReadScope(1,xmin,xmax,dx,DensityType,CutOffDist,ich)
      if(ich.ne.0) go to 9999
      call ConDefAtForDenCalc(3,ich)
      if(ich.ne.0) go to 9999
      nxny=1
      do i=1,6
        if(i.le.3) then
          nx(i)=nint((xmax(i)-xmin(i))/dx(i))+1
          if(i.le.2) then
            nxny=nxny*nx(i)
          else
            nmap=nx(i)
          endif
        else
          nx(i)=1
          xmin(i)=0.
          xmax(i)=0.
          dx(i)=.1
        endif
      enddo
      if(allocated(Table)) deallocate(Table)
      allocate(Table(nxny))
      Veta=fln(:ifln)//'.d81'
      if(ExistFile(Veta)) call DeleteFile(Veta)
      m8=NextLogicNumber()
      call OpenMaps(m8,Veta,nxny,1)
      if(ErrFlag.ne.0) go to 9999
      write(m8,rec=1) nx,nxny,nmap,(xmin(i),xmax(i),i=1,6),dx,
     1                (iorien(i),i=1,6),-DensityType-1,nsubs,
     2                SatelityByly,nonModulated(KPhase)
      nvse=npdf*nxny*nx(3)
      DensityTypeSave=DensityType
      Veta=MenDensity(DensityType+1)
      Veta='Calculation of '//Veta(:idel(Veta))//' map'
      call FeFlowChartOpen(-1.,120.,max(nint(float(nvse)*.005),10),nvse,
     1                     Veta,' ',' ')
      m=0
      Dmax=-999999.
      Dmin= 999999.
      CutOffDistA=CutOffDist*rcp(1,1,KPhase)
      CutOffDistB=CutOffDist*rcp(2,1,KPhase)
      CutOffDistC=CutOffDist*rcp(3,1,KPhase)
      xp3=xmin(3)
      open(66,file='Vypis-000.txt')
      do iz=1,nx(3)
        Table=0.
        do 3500ia=1,npdf
          ip=ipor(ia)
          fpdfi=fpdf(ip)
          if(fpdfi.le.0.) then
            m=m+nxny
            go to 3500
          endif
          call ConDensity(pom,ia,0,DenSupl,ich)
          xp(3)=xp3-xpdf(3,ip)
          if(abs(xp(3)).gt.CutOffDistC) then
            m=m+nxny
            go to 3500
          endif
          xp(2)=xmin(2)-xpdf(2,ip)
          n=0
          do iy=1,nx(2)
            if(abs(xp(2)).gt.CutOffDistB) then
              m=m+nx(1)
              n=n+nx(1)
              go to 3350
            endif
            xp(1)=xmin(1)-xpdf(1,ip)
            do ix=1,nx(1)
              n=n+1
              if(abs(xp(1)).gt.CutOffDistA) then
                m=m+1
                go to 3250
              endif
              call FeFlowChartEVent(m,is)
              if(is.ne.0) then
                call FeBudeBreak
                if(ErrFlag.ne.0) then
                  ich=1
                  go to 9000
                endif
              endif
              call MultM(TrToOrtho,xp,xo,3,3,1)
              dd=VecOrtLen(xo,3)
              if(dd.gt.CutOffDist) go to 3250
              if(ix.eq.1.and.iy.eq.1.and.iz.eq.1) then
                write(66,'(a8,''  dd:'',f10.6,i4)')
     1            Atom(iapdf(ip))(1:8),dd,ispdf(ip)
                write(66,'('' TrAt matice pro tento atom:'')')
                write(66,'(3f10.6)')
     1            ((TrAt(ii+(jj-1)*3,iapdf(ip)),jj=1,3),ii=1,3)
                VasekTest=123
              endif
              call ConDensity(dd,ia,1,DenSupl,ich)
              Table(n)=Table(n)+DenSupl(1)*fpdfi
              if(ix.eq.1.and.iy.eq.1.and.iz.eq.1) then
                write(66,'(''Prispevek, celkem:'',2e15.6)')
     1            DenSupl(1)*fpdfi,Table(n)
                write(66,'(''---------------------------------------'',
     1                     ''-------------------------------------'')')
                VasekTest=1
              endif
3250          xp(1)=xp(1)+dx(1)
            enddo
3350        xp(2)=xp(2)+dx(2)
          enddo
3500    continue
        do i=1,nxny
          pom=Table(i)
          Dmin=min(Dmin,pom)
          Dmax=max(Dmax,pom)
        enddo
        write(m8,rec=iz+1)(Table(i),i=1,nxny)
        xp3=xp3+dx(3)
      enddo
      write(m8,rec=nx(3)+2) Dmax,Dmin
      close(m8)
      call FeFlowChartRemove
      go to 9999
9000  close(m8,status='delete')
9999  if(allocated(Table)) deallocate(Table)
      if(allocated(DenCore))
     1  deallocate(DenCore,DenVal,DenCoreDer,DenValDer)
      close(66)
      VasekTest=1
      return
      end
      subroutine ConDensity(dd,iat,Klic,DenSupl,ich)
      use Atoms_mod
      use Basic_mod
      use Contour_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real kapa1i,kapa2i
      logical Hydrogen
      dimension FractToLocal(9),px(9),xp(3),SpX(64),SpXDer1(3,64),
     1          SpXDer2(6,64),fnorm(8),zz(8),nn(8),STOA(10),DSto(0:2),
     2          DenDer(0:2),DenDerXYZ(10),DenSupl(*),OrthoToLocal(9),
     3          RPop(680),popasi(64)
      equivalence (xp,px,pom)
      data st/.0333333/
      save FractToLocal,ip,isfi,PopValDeform,ia,fnorm,OrthoToLocal,zz,
     1     nn,lasmaxi,kapa1i,kapa2i,Hydrogen,popasi,PopCor,RPop
      ich=0
      if(Klic.eq.0) then
        ip=ipor(iat)
        ia=iapdf(ip)
        lasmaxi=lasmax(ia)
        isw=iswa(ia)
        isfi=isf(ia)
        if(ispdf(ip).eq.0) ispdf(ip)=1
        ISym=ispdf(ip)
        ISymAbs=abs(ISym)
        if(IsymAbs.eq.1) then
          call CopyMat(TrAt(1,ia),FractToLocal,3)
        else
          call MatInv(rm(1,ISymAbs,isw,KPhase),px,STOA(1),3)
          call MultM(TrAt(1,ia),px,FractToLocal,3,3,3)
        endif
        if(ISym.lt.0) then
          do i=1,9
            FractToLocal(i)=-FractToLocal(i)
          enddo
        endif
        call multm(FractToLocal,TrToOrthoI,OrthoToLocal,3,3,3)
        call SetPopulTrans(OrthoToLocal,lasmaxi-2,RPop)
        jp=2
        kp=2
        if(lasmaxi.ge.1) then
          popasi(1)=popas(1,ia)
          do l=1,lasmaxi-2
            n=2*l+1
            call multm(RPop(kp),popas(jp,ia),popasi(jp),n,n,1)
            kp=kp+n**2
            jp=jp+n
          enddo
          kapa1i=kapa1(ia)
          kapa2i=kapa2(ia)
          PopValDeform=kapa1i**3*popv(ia)
        endif
        PopValFree=ffbasic(1,isfi,KPhase)-ffcore(1,isfi,KPhase)
        PopCor=popc(ia)
        call CopyVek(ZSlater(1,isfi,KPhase),zz,8)
        call CopyVekI(NSlater(1,isfi,KPhase),nn,8)
        do i=1,8
          zz(i)=zz(i)*kapa2i
        enddo
        call SlaterNorm(nn,zz,fnorm)
        Hydrogen=AtType(isfi,KPhase).eq.'H'.or.
     1           AtType(isfi,KPhase).eq.'D'
      else
        if(DensityType.le.2) then
          if(VasekTest.eq.123) then
            write(66,'(''FractToLocal'')')
            write(66,'(3f10.6)')((FractToLocal(ii+(jj-1)*3),jj=1,3),
     1                           ii=1,3)
            write(66,'(''TrToOrthoI'')')
            write(66,'(3f10.6)')((TrToOrthoI(ii+(jj-1)*3,1,1),jj=1,3),
     1                           ii=1,3)
            write(66,'(''OrthoToLocal'')')
            write(66,'(3f10.6)')((OrthoToLocal(ii+(jj-1)*3),jj=1,3),
     1                           ii=1,3)
            jp=2
            kp=2
            if(lasmaxi.ge.1) then
              do l=1,lasmaxi-2
                n=2*l+1
                write(66,'(''RPop pro l ='',i3)') l
                kk=kp
                do ii=1,n
                  write(66,'(50f10.6)')(RPop(kk+(jj-1)*n),jj=1,n)
                  kk=kk+1
                enddo
                kp=kp+n**2
                jp=jp+n
              enddo
            endif
          endif
          if(lasmaxi.ge.1) then
            Density=PopValDeform*Finter(dd*kapa1i,DenVal(1,isfi),st,240)
     1              /pi4
          else
            Density=0.
          endif
          if(VasekTest.eq.123) then
            write(66,'(''DenVal:'')')
            write(66,'(5e15.6)') DenVal(1:240,isfi)
            write(66,'(''PopValDeform,dd*kapa1i,PopValDeform*Finter:'',
     1            5e15.6)')
     2        PopValDeform,dd*kapa1i,Density
          endif
          if(DensityType.eq.2.and.lasmaxi.gt.1) then
            if(Hydrogen) then
              Density=Density-PopValFree*2.148028*exp(-3.77943*dd)
            else
              Density=Density-
     1                   PopValFree*Finter(dd,DenVal(1,isfi),st,240)/pi4
            endif
          endif
          if(DensityType.eq.0)
     1      Density=Density+PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
          if(VasekTest.eq.123) then
            write(66,'(''DenCore:'')')
            write(66,'(5e15.6)') DenCore(1:240,isfi)
            write(66,'(''PopCor,dd,PopCor*Finter:'',3e15.6)')
     1        PopCor,dd,PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
          endif
          if(VasekTest.eq.123) write(66,'(''xo:'',3f10.4)') xo(1:3)
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          if(VasekTest.eq.123) then
            write(66,'(''SpX:'')')
            write(66,'(5e15.6)') SpX(1:(lasmaxi-2)**2)
            write(66,'(''popasi:'')')
            write(66,'(5e15.6)') popasi(1:(lasmaxi-2)**2)
          endif
          k=0
          do l=1,lasmaxi-1
            pom=0.
            do n=1,2*l-1
              k=k+1
              pom=pom+popasi(k)*SpX(k)
              if(VasekTest.eq.123)
     1          write(66,'(''k,popasi(k),SpX(k),pom:'',
     2                   i5,3e15.6)') k,popasi(k),SpX(k),pom
            enddo
            Density=Density+
     1                 pom*fnorm(l)*SlaterDensity(dd,zz(l),nn(l))
            if(VasekTest.eq.123)
     1        write(66,'(''l,nn(l),zz(l),nn(l),dd,SlaterDensity,'',
     2                   ''fnorm(l),pom'',2i4,f10.4,f10.6,3e15.6)')
     3                 l,nn(l),zz(l),dd,SlaterDensity(dd,zz(l),nn(l)),
     4                 fnorm(l),pom
          enddo
        else if(DensityType.eq.3.or.DensityType.eq.4.or.
     1          DensityType.eq.5.or.DensityType.eq.6) then
          if(DensityType.eq.3.or.DensityType.eq.5.or.DensityType.eq.6)
     1      then
            DenDer(0)=PopCor*Finter(dd,DenCore(1,isfi),st,240)/pi4
            DenDer(1)=PopCor*Finter(dd,DenCoreDer(1,1,isfi),st,240)/pi4
            DenDer(2)=PopCor*Finter(dd,DenCoreDer(1,2,isfi),st,240)/pi4
            call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
            if(DensityType.eq.3) then
              Density=-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
            else
              call CopyVek(DenDerXYZ(2),DenSupl,3)
            endif
          else
            Density=0.
          endif
          DenDer(0)=PopValDeform*
     1      Finter(dd*kapa1i,DenVal(1,isfi),st,240)/pi4
          DenDer(1)=PopValDeform*kapa1i*
     1      Finter(dd*kapa1i,DenValDer(1,1,isfi),st,240)/pi4
          DenDer(2)=PopValDeform*kapa1i**2*
     1      Finter(dd*kapa1i,DenValDer(1,2,isfi),st,240)/pi4
          call RadialDer(xo(1),xo(2),xo(3),dd,DenDer,2,DenDerXYZ)
          if(DensityType.eq.3.or.DensityType.eq.4) then
            Density=Density-DenDerXYZ(5)-DenDerXYZ(6)-DenDerXYZ(7)
          else
            call AddVek(DenSupl,DenDerXYZ(2),DenSupl,3)
          endif
          call SpherHarmN(xo,SpX,SpXDer1,SpXDer2,lasmaxi-2,2,3)
          k=0
          do l=1,lasmaxi-1
            call SlaterDensityDer(dd,zz(l),nn(l),DSto)
            STO=DSto(0)
            call RadialDer(xo(1),xo(2),xo(3),dd,DSto,2,STOA)
            call SetRealArrayTo(px,3,0.)
            fnorml=fnorm(l)
            do n=1,2*l-1
              k=k+1
              SpXk=SpX(k)
              popask=popasi(k)
              if(DensityType.eq.3.or.DensityType.eq.4) then
                pom=pom+popask*(
     1            SpXk*STOA(5)+2.*SpXDer1(1,k)*STOA(2)+SpXDer2(1,k)*STO+
     2            SpXk*STOA(6)+2.*SpXDer1(2,k)*STOA(3)+SpXDer2(2,k)*STO+
     3            SpXk*STOA(7)+2.*SpXDer1(3,k)*STOA(4)+SpXDer2(3,k)*STO)
              else
                do m=1,3
                  px(m)=px(m)+popask*(SpXk*STOA(m+1)+SpXDer1(m,k)*STO)
                enddo
              endif
            enddo
            if(DensityType.eq.3.or.DensityType.eq.4) then
              Density=Density-pom*fnorml
            else
              do m=1,3
                DenSupl(m)=DenSupl(m)+px(m)*fnorml
              enddo
            endif
          enddo
        endif
        if(DensityType.ne.5.and.DensityType.ne.6) DenSupl(1)=Density
      endif
9999  return
      end
      subroutine CtiOutXD
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      character*5   ch5
      logical EqIgCase
      integer ih(3),ihv(3)
      open(66,file='xd_lsm.out')
      open(67,file='xd_ind_used.txt')
1100  read(66,FormA,end=2000) Veta
      write(6,'(1x,a)') Veta(:18)
      if(EqIgCase(Veta(:18),'   NO.   H   K   L')) then
        go to 1150
      else
      go to 1100
      endif
1150  nall=0
      nmin=0
1200  read(66,FormA,end=1300) Veta
      if(EqIgCase(Veta(:18),'   NO.   H   K   L').or.
     1   EqIgCase(Veta(:18),'------------------')) go to 1200
      if(EqIgCase(Veta(:18),' Condition(s) met:')) go to 1300
      read(Veta,'(i6,3i4,f7.4,i3,3e12.5,a5,f7.1,i3)') no,ih,sn,isc,Fobs,
     1  Fcalc,Fdel,ch5,scp,iflg
      nall=nall+1
      if(iflg.lt.0) then
        write(67,'(i6,3i4)') no,ih
        nmin=nmin+1
      endif
      go to 1200
1300  write(Veta,'(2i10)') nall,nmin
      call FeWinMessage(Veta,' ')
2000  close(66)
      open(66,file='xd_full.hkl')
      open(68,file='xd_red.hkl')
      rewind 67
      read(66,FormA) Veta
      write(68,FormA) Veta(:idel(Veta))
      n=0
      nVen=0
2100  read(67,'(i6,3i4)',end=2200) nVen,ihv
      go to 2300
2200  nVen=0
2300  read(66,FormA,end=5000) Veta
      read(Veta,'(3i4)') ih
      n=n+1
      if(n.ne.nVen) then
        write(68,FormA) Veta(:idel(Veta))
        go to 2300
      else
        go to 2100
      endif
5000  close(66)
      close(67)
      close(68)
      return
      end
      subroutine NasobkyTest
      integer Mame(0:7,0:7),Vector(2),ip(2)
      integer Base(2,4)
      Base(1,1)=0
      Base(2,1)=0
      Base(1,2)=7
      Base(2,2)=0
      Base(1,3)=0
      Base(2,3)=7
      Base(1,4)=7
      Base(2,4)=7
      Vector(1)=3
      Vector(2)=5
      Mame=0
      n=0
1100  do j=1,4
        do i=1,2
          ip(i)=mod(Vector(i)*n+Base(i,j),7)
        enddo
        write(6,'(2i5)') ip
!      if(ip(1).eq.0.and.ip(2).eq.0) go to 1200
        Mame(ip(1),ip(2))=1
        if(ip(1).eq.0) Mame(7,ip(2))=1
        if(ip(2).eq.0) Mame(ip(1),7)=1
        if(ip(1).eq.0.and.ip(2).eq.0) Mame(7,7)=1
      enddo
      n=n+1
      if(n.gt.33333) go to 1200
      go to 1100
1200  write(6,'('' n='',i5)') n
      write(6,'(15i3)')((Mame(i,7-j),i=0,7),(Mame(i,7-j),i=1,7),j=0,7)
      write(6,'(15i3)')((Mame(i,7-j),i=0,7),(Mame(i,7-j),i=1,7),j=1,7)
      return
      end
      subroutine Srovnani
      dimension ih(3,15000,2),Fobs(15000,2),n(2),ihp(3)
      character*256 Veta
      logical EqIV
      open(44,file='D:\Structures\Jana2006\Work\Michal Dusek\'//
     1             'Novy format Crysalis\Cobtc1HA_Jana_bezkorekci.hkl')
      open(45,file='D:\Structures\Jana2006\Work\Michal Dusek\'//
     1             'Novy format Crysalis\Cobtc1HA_Jana_korigovane.hkl')
      do i=1,2
        nn=0
1100    read(43+i,'(3i4,f8.3,98x,2f11.5)',end=1200) ihp,pom,sc1,sc2
        nn=nn+1
        ih(1:3,nn,i)=ihp(1:3)
        Fobs(nn,i)=pom/(sc1*sc2)
        go to 1100
1200    close(43+i)
        n(i)=nn
      enddo
      do 1500i1=1,n(1)
        do i2=1,n(2)
          if(EqIV(ih(1,i1,1),ih(1,i2,2),3)) then
            if(Fobs(i2,2).ne.0.) then
              pom=Fobs(i1,1)/Fobs(i2,2)
            else
              pom=0.
            endif
            write(6,'(6i4,2f12.2,f10.6)') ih(1:3,i1,1),ih(1:3,i2,2),
     1                                    Fobs(i1,1),
     1                                    Fobs(i2,2),pom
          endif
        enddo
        write(6,'('' ---------------------------------'')')
1500  continue
      return
      end
      subroutine PisDer(Ln,ihp,sigyo,wdy,no,nspec,kspec)
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ihp(6),kmodai(7)
      character*1 nspec,kspec
      character*25  Formln
      data formln/'(''Extinction : '',10e15.7)'/
      if(.not.CalcDer) go to 9999
      if(isPowder) then
        write(Ln,'(i8,2f10.1)') no,sigyo,wdy
      else
        if(ExistMagnetic) then
          write(Ln,format3)(ihp(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b,dyp,
     1                      sigyo,1./sqrt(wt),wdy,no,nspec,kspec,sinthl,
     2                      extkor,extkorm,iq
        else
          write(Ln,format3)(ihp(i),i=1,NDim(KPhase)),Fobs,Fcalc,a,b,dyp,
     1                      sigyo,1./sqrt(wt),wdy,no,nspec,kspec,sinthl,
     2                      extkor,iq
        endif
      endif
      formln(3:13)='Scale'
      write(ln,formln)(der(i),i=1,6)
      formln(3:13)='Extinction'
      write(ln,formln)(der(i),i=19,30)
      write(ln,112)
      do i=1,NAtInd
        itfi=itf(i)
        kmodai(1)=KModA(1,i)
        if(TypeModFun(i).ne.1) then
          do j=2,itfi+1
            kmodai(j)=KModA(j,i)
          enddo
        else
          do j=2,itfi+1
            kmodai(j)=kmodao(j,i)
          enddo
        endif
        mp=PrvniKiAtomu(i)
        mk=mp+9
        write(ln,100) atom(i),(der(m),m=mp,mk)
        write(6,'('' Pise: '',a)') Atom(i)
        formln(3:11)='B'
        do j=3,itfi
          formln(3:3)=char(ichar(formln(3:3))+1)
          mp=mk+1
          mk=mk+TRank(j)
          write(ln,formln)(der(m),m=mp,mk)
        enddo
        if(NDim(KPhase).eq.3) then
          formln(3:13)='Dens1'
          mp=mk+1
          mk=mk+3
          if(lasmax(i).gt.1) mk=mk+1
          write(ln,formln)(der(m),m=mp,mk)
          if(lasmax(i).le.1) go to 1400
          mp=mk+1
          mk=mk+(lasmax(i)-1)**2
          if(mk.ge.mp) then
            formln(3:13)='Dens2'
            write(ln,formln)(der(m),m=mp,mk)
            go to 1400
          endif
        endif
        k=ichar('C')-4
        do j=1,itfi+1
          if(j.eq.1) then
            formln(3:13)='Occ.mod.'
          else if(j.eq.2) then
            formln(3:13)='Pos.mod.'
          else if(j.eq.3) then
            formln(3:13)='Temp.mod.'
          else
            write(formln(3:13),'(a1,''.mod.'')') char(k+j)
          endif
          mp=mk+1
          mk=mk+2*TRank(j-1)*kmodai(j)
          if(j.eq.1.and.kmodai(1).gt.0) mk=mk+1
          if(mk.ge.mp) write(ln,formln)(der(m),m=mp,mk)
        enddo
        do j=1,itfi+1
          if(kmodai(j).gt.0) then
            mp=mk+1
            mk=mk+1
            formln(3:13)='Phason'
            write(ln,formln) der(mp)
            go to 1400
          endif
        enddo
1400    if(MagPar(i).gt.0) then
          formln(3:13)='Mag0'
          mp=mk+1
          mk=mk+3
          write(ln,formln)(der(m),m=mp,mk)
          formln(3:13)='Mag.mod.'
          do j=1,MagPar(i)-1
            mp=mk+1
            mk=mk+6
            write(ln,formln)(der(m),m=mp,mk)
          enddo
        endif
        write(ln,112)
      enddo
      do i=NAtMolFr(1,1),NAtAll
        kmodsi=KModA(1,i)
        kmodxi=KModA(2,i)
        kmodbi=KModA(3,i)
        m=PrvniKiAtomu(i)
        write(ln,100) atom(i),(der(j),j=m,m+9)
        m=m+10
        if(kmodsi.gt.0) then
          write(ln,110)(der(j),j=m,m+2*kmodsi)
          m=m+2*kmodsi+1
        endif
        if(kmodxi.gt.0) then
          write(ln,109)(der(j),j=m,m+6*kmodxi-1)
          m=m+6*kmodxi
        endif
        if(kmodbi.gt.0) then
          write(ln,106)(der(j),j=m,m+12*kmodbi-1)
          m=m+12*kmodbi
        endif
        if(kmodsi.gt.0.or.kmodxi.gt.0.or.kmodsi.gt.0) then
          write(ln,111) der(m)
          m=m+1
        endif
        write(ln,112)
      enddo
      do i=1,NMolec
        do j=1,mam(i)
          ji=j+(i-1)*mxp
          m=PrvniKiMolekuly(ji)
          write(ln,100) molname(i),(der(k),k=m,m+6)
          m=m+7
          if(ktls(i).gt.0) then
            write(ln,'(''T          : '',6e15.7)')(der(k),k=m,m+5)
            m=m+6
            write(ln,'(''L          : '',6e15.7)')(der(k),k=m,m+5)
            m=m+6
            write(ln,'(''S          : '',6e15.7)')(der(k),k=m,m+8)
            m=m+9
          endif
          if(KModM(1,ji).gt.0) then
            write(ln,110)(der(k),k=m,m+2*KModM(1,ji))
            m=m+2*KModM(1,ji)+1
          endif
          if(KModM(2,ji).gt.0) then
            write(ln,109)(der(k),k=m,m+6*KModM(2,ji)-1)
            m=m+6*KModM(2,ji)
            write(ln,109)(der(k),k=m,m+6*KModM(2,ji)-1)
            m=m+6*KModM(2,ji)
          endif
          if(KModM(3,ji).gt.0) then
            write(ln,106)(der(k),k=m,m+12*KModM(3,ji)-1)
            m=m+12*KModM(3,ji)
            write(ln,106)(der(k),k=m,m+12*KModM(3,ji)-1)
            m=m+12*KModM(3,ji)
            write(ln,106)(der(k),k=m,m+18*KModM(3,ji)-1)
            m=m+18*KModM(3,ji)
          endif
          if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1       KModM(3,ji).gt.0) write(ln,111) der(m)
          write(ln,112)
        enddo
      enddo
9999  return
100   format(a8,'   : ',10e15.7)
106   format('Temp.      : ',10e15.7)
109   format('Position   : ',10e15.7)
110   format('Occ.       : ',10e15.7)
111   format('Phason     : ',10e15.7)
112   format(163('-'))
      end
      subroutine DerTest(ihp,yop,ycp,itstder)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ihp(6),DerSave(:)
      character*256 EdwStringQuest
      character*80 t80,Message1,Message2
      character*20 at
      character*12 parp
      integer pocder,Exponent10
      logical FeYesNo,PridatQR
      save dlt,yc0,at,parp,Message1,Message2,pom0,sp,nButtCalc,
     1     nButtQuit,nEdwParameter,nEdwDelta,ktstdero,nLbl1,nLbl2,
     2     DerSave,kk
      allocatable DerSave
      if(.not.allocated(DerSave)) allocate(DerSave(PosledniKiAtMagMode))
      if(DoLeBail.ne.0) then
        pom=sc(1,KDatBlock)
        sc(1,KDatBlock)=scPwd(KDatBlock)
        scPwd(KDatBlock)=pom
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      if(kcommenMax.gt.0) then
        if(MaxNSymmMol.gt.MaxNSymm) then
          call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                     NPhase,0)
          MaxNSymm=MaxNSymmMol
        endif
        call ComSym(0,0,ich)
        if(ngc(KPhase).gt.0)
     1    call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),NAtCalc)
      endif
      if(DoLeBail.ne.0) then
        pom=sc(1,KDatBlock)
        sc(1,KDatBlock)=scPwd(KDatBlock)
        scPwd(KDatBlock)=pom
        if(NAtCalc.gt.0)
     1    call SetIntArrayTo(ki(ndoff+1),PosledniKiAtPos-ndoff,0)
      endif
      if(itstder.eq.0) then
        call CopyVek(Der,DerSave,PosledniKiAtMagMode)
        Message1=' '
        Message2=' '
        ktstdero=0
        id=NextQuestId()
        il=8
        xdq=350.
        if(isPowder) then
          write(Cislo,'(f10.3)') XPwdDerTest
          call Zhusti(Cislo)
          t80='Testing point 2theta/TOF: '//Cislo(:idel(Cislo))
          write(Cislo,'(f15.3)') ycp
          call Zhusti(Cislo)
          t80=t80(:idel(t80))//', I(calc) : '//Cislo(:idel(Cislo))
        else
          t80='Reflection : ('
          do i=1,NDim(KPhase)
            write(Cislo,'(i5,'','')') HDerTest(i)
            call Zhusti(Cislo)
            t80=t80(:idel(t80))//Cislo(:idel(Cislo))
          enddo
          write(Cislo,'(f10.4)') ycp
          call Zhusti(Cislo)
          t80=t80(:idel(t80)-1)//'), F(calc) : '//Cislo(:idel(Cislo))
        endif
        yc0=ycp
        call FeQuestCreate(id,-1.,-1.,xdq,il,t80,0,LightGray,-1,-1)
        il=1
        call FeQuestLinkaMake(id,il)
        tpom=5.
        t80='%Parameter to be tested'
        dpom=150.
        xpom=tpom+FeTxLengthUnder(t80)+5.
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
        nEdwParameter=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
        dlt=0.0001
        il=il+1
        t80='%Difference'
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
        nEdwDelta=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,dlt,.false.,.false.)
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        call FeQuestLblMake(id,5.,il,Message1,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLbl1=LblLastMade
        il=il+1
        call FeQuestLblMake(id,5.,il,Message2,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLbl2=LblLastMade
        il=8
        dpom=60.
        xpom=xdq*.5-dpom-10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Calculate')
        nButtCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Quit')
        nButtQuit=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        go to 1500
      endif
      if(itstder.eq.1) then
        if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode) then
          ppp=(ycp-yc0)/dlt*CellPar(kk,1,1)
        else
          ppp=(ycp-yc0)/dlt
        endif
        if(isPowder) then
          write(Message2,'(''New I(calc) :'',f10.3,
     1                     '', Numerical  derivation :'',e13.5)')
     2      ycp,ppp
        else
          write(Message2,'(''New F(calc) :'',f10.4,
     1                     '', Numerical derivation  :'',e13.5)')
     2      ycp,ppp
        endif
        call FeQuestLblChange(nLbl2,Message2)
        if(isPowder) then
          do i=1,NPhase
            KPhase=i
            call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
            call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
            if(CellPar(1,1,i).gt..1) call setmet(0)
          enddo
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EVentButton) then
        if(CheckNumber.eq.nButtCalc) then
          t80=EdwStringQuest(nEdwParameter)
          i=index(t80,'[')
          j=index(t80,']')
          if(i.gt.0) then
            if(j.eq.0.or.i.ge.j.or.j.ne.idel(t80)) then
              call FeChybne(-1.,-1.,'Syntactic error - try again.',' ',0,
     1                      -1)
              go to 1500
            endif
            at=t80(i+1:j-1)
            call uprat(at)
            parp=t80(1:i-1)
            k=ktatmol(at)
            if(k.ne.0) then
              n=ktera(parp)
              if(n.eq.JeToXYZMode) then
                ktstder=KteraXYZMode(Parp,At)
              else if(n.eq.JeToMagMode) then
                ktstder=KteraMagMode(Parp,At)
              else
                m=pocder(k)
                if(m.le.0) go to 1600
                if(k.gt.0) then
                  call RefGetNParAt(n,k,.false.,PridatQR,ich)
                else
                  call RefGetNParMol(n,-k,.false.,PridatQR,ich)
                endif
                if(ich.ne.0) go to 1610
                if(k.gt.0) then
                  call ChangeAtCompress(n,n,k,0,ich)
                else if(k.lt.0) then
                  call ChangeMolCompress(n,n,-k,0,ich)
                endif
                ktstder=m+n
              endif
            else
              k=0
              ktstder=KteraSc(parp)
              call RefGetScShift(iabs(ktstder),at,i)
              if(i.ge.0) then
                ktstder=ktstder+i
              else
                go to 1610
              endif
            endif
          else
            k=0
            parp=t80
            ktstder=kterasc(parp)
            if(ktstder.le.0) go to 1610
          endif
          if(ki(ktstder).eq.0.and.isPowder) go to 1620
          call FeQuestRealFromEdw(nEdwDelta,dlt)
          if(ktstder.ne.ktstdero) then
            call kdoco(ktstder,at,parp,1,pom0,sp)
            if(k.gt.0) then
              kk=ktstder-PrvniKiAtomu(k)+1
              if(kk.ge.DelkaKiAtomu(k)-3-6*(MagPar(k)-1)) then
                kk=kk-DelkaKiAtomu(k)+3+6*(MagPar(k)-1)
                kk=mod(kk-1,3)+1
              else
                kk=0
              endif
            else
              kk=0
            endif
            if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode)
     1        pom0=pom0*CellPar(kk,1,1)
            pomd=DerSave(ktstder)
            if(ifsq.eq.1.and..not.isPowder.and.yc0.ne.0.)
     1        pomd=pomd*.5/yc0
c            if(isPowder.and.ktstder.le.mxscu) pomd=.0002*pom0*pomd
            t80='(''Parameter : '',f9.6,'', Analytical derivation : '''
     1          //',e13.5)'
            i=max(6-Exponent10(pom0),0)
            i=min(6-Exponent10(pom0),6)
            if(i.gt.1) write(t80(20:20),'(i1)') i
            write(Message1,t80) pom0,pomd
            call FeQuestLblChange(nLbl1,Message1)
            ktstdero=ktstder
          endif
          itstder=1
          if(kk.ne.0.and.ktstder.lt.PrvniKiAtMagMode) then
            pom=(pom0+dlt)/CellPar(kk,1,1)
          else
            pom=pom0+dlt
          endif
          if(NAtXYZMode.gt.0) call TrXYZMode(1)
          if(NAtMagMode.gt.0) call TrMagMode(1)
          call kdoco(ktstdero,at,parp,-1,pom,sp)
          call kdoco(ktstdero,at,parp,1,pompp,sp)
          if(NDimI(KPhase).gt.0.and.k.gt.0) then
             if(kmol(k).le.0) call qbyx(x(1,k),qcnt(1,k),iswa(k))
          endif
          call RefAppEq(0,0,0)
          if(nvai.gt.0) call SetRes(0)
          if(nKeep.gt.0) call RefSetKeep
!          if(NMolec.gt.0) then
!            NAtCalc=NAtInd
!            call SetMol(1,0)
!          endif
          if(NAtXYZMode.gt.0) call TrXYZMode(0)
          if(NAtMagMode.gt.0) call TrMagMode(0)
          if(isPowder) then
            do i=1,NPhase
              KPhase=i
              call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
              call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
              if(CellPar(1,1,i).gt..1) call setmet(0)
            enddo
          endif
          go to 3000
        else if(CheckNumber.eq.nButtQuit) then
          if(FeYesNo(-1.,-1.,'Do you really want to quit testing?',0))
     1      then
            call FeQuestRemove(id)
            itstder=-1
            go to 9999
          endif
        endif
        go to 1500
1600    t80='atom "'//at(:idel(at))//'" doesn''t exist'
        go to 1650
1610    t80='unknown or incorrect parameter "'//parp(:idel(parp))//'"'
        go to 1650
1620    t80='not refined parameter "'//t80(:idel(t80))//
     1      '" cannot be tested for powder'
        go to 1650
1630    t80='phase "'//at(:idel(at))//'" doesn''t exist.'
1650    call FeChybne(-1.,-1.,t80,' ',-1)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(NMolec.gt.0) then
        NAtCalc=NAtInd
        call setmol(1,0)
      endif
9999  if(itstder.eq.-1) deallocate(DerSave)
      call iom50(0,0,fln(:ifln)//'.m50')
      if(MaxNSymmMol.gt.MaxNSymm) then
        call ReallocSymm(MaxNDim,MaxNSymmMol,MaxNLattVec,MaxNComp,
     1                   NPhase,0)
        MaxNSymm=MaxNSymmMol
      endif
      if(kcommenMax.gt.0) then
        NAtCalc=NAtCalcBasic
        call ComSym(0,0,ich)
      endif
      do KDatB=1,NDatBlock
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      if(isPowder) then
        do i=1,NPhase
          KPhase=i
          call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
          call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*NDimI(KPhase))
          if(CellPar(1,1,i).gt..1) call setmet(0)
        enddo
      endif
      if(maxNDimI.gt.0) call calcm2(i,i,i,-1)
      return
      end
      subroutine FouPrelim
      use Basic_mod
      use Atoms_mod
      use Fourier_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*256 Veta
      integer CrlCentroSymm
      logical ExistM80,eqiv,EqIgCase,konec,ExistFile,AddBorder,EqIV0
      dimension ih(6),ihp(6),ihpp(6),mxh(6),mxd(6),hh(6),hhp(6),
     1          fr(:),fi(:),mlt(:)
      allocatable fr,fi,mlt
      call DeleteFile(fln(:ifln)//'_Fourier.tmp')
      call OpenFile(lst,fln(:ifln)//'.fou','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      uloha='Program for n-dimensional Fourier synthesis'
      OrCode=-333
      call SetRealArrayTo(xrmn,NDim(KPhase),-333.)
      ptstep=-1.0
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      call DefaultFourier
      call NactiFourier
      if(ErrFlag.ne.0) go to 9999
      if(RefineCallFourier) then
        YMinFlowChart=120.
      else
        YMinFlowChart=-1.
      endif
      if(NDim(KPhase).le.3) then
        nsubs=1
      else
        nsubs=min(nsubs,NComp(KPhase))
      endif
      Patterson=mapa.le.3.or.Mapa.eq.9
      call iom50(0,1,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      ExistM80=ExistFile(fln(:ifln)//'.m80')
      if(.not.ExistM80.and.mapa.ne.1) then
        call FeChybne(-1.,-1.,'the M80 file doesn''t exist, first you'//
     1                ' have to','run REFINE to get phases and/or '//
     2                'Fcalc.',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      if(ptstep.lt.0.) ptstep=.25
      call FouDefLimits
      if(ExistM80) then
        call OpenFile(80,fln(:ifln)//'.m80','formatted','old')
        if(ErrFlag.ne.0) go to 9999
      else
        call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9999
      endif
      FileM82='jm82'
      call CreateTmpFile(FileM82,i,0)
      call FeTmpFilesAdd(FileM82)
      call OpenFile(82,FileM82,'unformatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      twov=2./CellVol(nsubs,1)
      if(Patterson) then
        srnat=0.
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          do i=1,NAtFormula(KPhase)
            srnat=srnat+AtMult(i,KPhase)
          enddo
        endif
        if(srnat.gt.0.) then
          do i=1,NAtFormula(KPhase)
            fx(i)=ffbasic(1,i,KPhase)+ffra(i,KPhase,KDatBlock)
          enddo
          pom0=0.
          do i=1,NAtFormula(KPhase)
            pom0=pom0+
     1           AtMult(i,KPhase)*sqrt((fx(i)**2+
     2                                  ffia(i,KPhase,KDatBlock)**2))
          enddo
        else
          pom0=1.
        endif
      endif
      if(lite(KPhase).eq.1) then
        rhom=1.
      else
        rhom=episq
      endif
      AddBorder=.false.
      if(ptname.ne.'[nic]') then
        if(ptname.ne.'[neco]') then
          kk=0
          call SetRealArrayTo(ptx,3,0.)
          pom=0.
1150      call Kus(ptname,kk,Veta)
          call atsym(Veta,i,hhp,hh,hh(4),j,k)
          if(i.le.0) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t on the '//'file M40',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(k.eq.3) then
            call FeChybne(-1.,-1.,'atom '//Veta(:idel(Veta))//
     1                    ' isn''t correct',
     2                    'The part of the volume to be mapped '//
     3                    'isn''t defined.',SeriousError)
            ErrFlag=1
          endif
          if(ErrFlag.ne.0) go to 9000
          pom=pom+1.
          call AddVek(ptx,hhp,ptx,3)
          if(kk.lt.idel(ptname)) go to 1150
          do i=1,3
            ptx(i)=ptx(i)/pom
          enddo
        endif
        do j=1,3
          nd=nint(.5*pts(j)/ptstep)
          if(nd.ne.0) then
            dd(j)=ptstep/CellPar(j,nsubs,KPhase)
          else
            dd(j)=1.
          endif
          xrmn(j)=ptx(j)-dd(j)*float(nd)
          xrmx(j)=ptx(j)+dd(j)*float(nd)
        enddo
      endif
      if(OrCode.eq.-333) OrCode=nop(nsubs)
      norien=OrCode
      n=norien
      i=10**(NDim(KPhase)-1)
      do j=1,NDim(KPhase)-1
        iorien(j)=n/i
        n=mod(n,i)
        i=i/10
      enddo
      iorien(NDim(KPhase))=n
      do j=1,6
        if(j.le.NDim(KPhase)) then
          if(xrmn(j).lt.-330.) then
            xrmn(j)=fourmn(j,nsubs)
            xrmx(j)=fourmx(j,nsubs)
            if(j.le.3) then
              n=max(nint((xrmx(j)-xrmn(j))*
     1              CellPar(j,nsubs,KPhase)/ptstep),1)
              dd(j)=(xrmx(j)-xrmn(j))/float(n)
            else
              dd(j)=.1
            endif
            if(IndUnit.eq.1) then
              AddBorder=.true.
            else
              xrmx(j)=xrmx(j)-dd(j)
            endif
          endif
        else
          xrmn(j)=0.
          xrmx(j)=0.
          dd(j)=1.
        endif
      enddo
      do i=1,NDim(KPhase)
        n=iorien(i)
        if(n.lt.1.or.n.gt.NDim(KPhase)) go to 1500
        do j=i+1,NDim(KPhase)
          if(n.eq.iorien(j)) go to 1500
        enddo
      enddo
      go to 1550
1500  Veta='wrong orientation'
      i=idel(Veta)+1
      write(Veta(i:i+6),'(i7)') norien
      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
      ErrFlag=1
      go to 9000
1550  do i=1,6
        if(i.le.NDim(KPhase)) then
          j=iorien(i)
          if(AddBorder.and.j.le.3) then
            xrmn(j)=xrmn(j)-dd(j)
            xrmx(j)=xrmx(j)+dd(j)
          endif
          xfmn(i)=xrmn(j)
          xfmx(i)=xrmx(j)
          xdf(i)=dd(j)
        else
          xfmn(i)=0.
          xfmx(i)=0.
          xdf(i)=1.
        endif
      enddo
      NDimSum=0
      do i=1,6
        pom=xfmx(i)-xfmn(i)
        nx(i)=nint(pom/xdf(i))+1
        if(i.le.NDim(KPhase)) then
          if(nx(i).gt.1) then
            dpom=pom/float(nx(i)-1)
            if(abs(xdf(i)-dpom).gt..00001) xdf(i)=dpom
          endif
          if(pom.lt.0.or.xdf(i).lt.0.) then
            write(Veta,'(3f8.3)') xfmn(i),xfmx(i),xdf(i)
            call ZdrcniCisla(Veta,3)
            j=idel(Veta)
            if(NDim(KPhase).eq.3) then
              Veta='Incorrect limits for '//smbx(i)//' : '//Veta(:j)
            else
              Veta='Incorrect limits for '//smbx6(i)//' : '//Veta(:j)
            endif
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            ErrFlag=1
            go to 9000
          endif
        endif
        if(nx(i).gt.1) NDimSum=NDimSum+1
      enddo
      NDimSum=min(NDimSum,NDim(KPhase))
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      if(nxny.lt.50) then
        write(Veta,'(i5,''x'',i5)') nx(1),nx(2)
        call Zhusti(Veta)
        Veta='2d sections would contain only '//Veta(:idel(Veta))//
     1      ' points.'
        call FeChybne(-1.,-1.,Veta,'Please enlarge the scope of maps.',
     1                SeriousError)
        ErrFlag=1
        go to 9000
      endif
1630  if(Patterson) then
        do i=1,NSymmN(KPhase)
          call SetRealArrayTo(s6(1,i,1,KPhase),NDim(KPhase),0.)
        enddo
        if(CrlCentroSymm().le.0) then
          call ReallocSymm(NDim(KPhase),2*NSymmN(KPhase),
     1                     NLattVec(KPhase),NComp(KPhase),NPhase,0)
          n=NSymmN(KPhase)
          do i=1,NSymmN(KPhase)
            call RealMatrixToOpposite(rm6(1,i,1,KPhase),
     1        rm6(1,i+n,1,KPhase),NDim(KPhase))
            call MatBlock3(rm6(1,i+n,1,KPhase),rm(1,i+n,1,KPhase),
     1                     NDim(KPhase))
            call CopyVek(s6(1,i,1,KPhase),s6(1,i+n,1,KPhase),
     1                   NDim(KPhase))
            ISwSymm(i+n,1,KPhase)=ISwSymm(i,1,KPhase)
          enddo
          NSymmN(KPhase)=2*n
          MaxNSymm=max(MaxNSymm,NSymm(KPhase))
          call ReallocateAtoms(0)
        endif
      endif
      do i=1,6
        if(i.le.NDim(KPhase)) then
          cx(i)=smbx6(iorien(i))
        else
          cx(i)=' '
        endif
      enddo
      if(Patterson) then
        npeaks(1)=50
      else
        if(npeaks(1).lt.0) then
          fa=0.
          fah=0.
          do i=1,NAtCalc
            if(ffbasic(1,isf(i),KPhase).lt.2.) then
              fah=fah+ai(i)
            else
              fa=fa+ai(i)
            endif
          enddo
          pom=0.
          pomh=0.
          do i=1,NAtFormula(KPhase)
            if(EqIgCase(AtType(i,KPhase),'H')) then
              pomh=pomh+AtMult(i,KPhase)
            else
              pom=pom+AtMult(i,KPhase)
            endif
          enddo
          pom=pom*float(NUnits(KPhase))/
     1        float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          pomh=pomh*float(NUnits(KPhase))/
     1      float(NAtFormula(KPhase)*NSymmN(KPhase))
          nacp=nint(pom)
          nachp=nint(pomh)
          if(mapa.eq.4.or.mapa.eq.5) then
            npeaks(1)=nacp+10
          else
            npeaks(1)=max(nacp-nint(fa)+nachp-nint(fah)+10,10)
          endif
        endif
        if(npeaks(2).lt.0) then
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            npeaks(2)=10
          else
            NPeaks(2)=NPeaks(1)
          endif
        endif
        do i=1,2
          npeaks(i)=min(npeaks(i),3000)
        enddo
      endif
      Veta='Type of map : '
      if(mapa.eq.1) then
        Veta(15:)='F(obs)**2 - Patterson'
      else if(mapa.eq.2) then
        Veta(15:)='F(calc)**2 - checking Patterson'
      else if(mapa.eq.3) then
        Veta(15:)='F(obs)**2-F(calc)**2 - difference Patterson'
      else if(mapa.eq.4) then
        Veta(15:)='F(obs) - Fourier'
      else if(mapa.eq.5) then
        Veta(15:)='F(calc) - checking Fourier'
      else if(mapa.eq.6) then
        Veta(15:)='F(obs)-F(cal) - difference Fourier'
      else if(mapa.eq.7) then
        Veta(15:)='dynamic multipole deformation map'
      else if(mapa.eq.8) then
        Veta(15:)='static multipole deformation map'
      else if(mapa.eq.9) then
        Veta(15:)='1/0 - shape function'
      else
        call FeChybne(-1.,-1.,'wrong map type',' ',SeriousError)
        ErrFlag=1
        go to 9000
      endif
      call newln(2)
      write(lst,FormA1)
      write(lst,FormA1)(Veta(i:i),i=1,idel(Veta))
      if(vyber.gt.0.) then
        call newln(2)
        write(lst,'(/''Reflections with  |Fo| > '',f10.2,'' * |Fc| '',
     1               ''will not be used in the synthesis'')') vyber
      endif
      call newln(2)
      if(UseWeight.gt.0) then
        write(lst,'(/''Weighting of reflection based on chi-square '',
     1               ''will be applied'')')
      else
        write(lst,'(/''No weighting of reflections will be applied'')')
      endif
      if(snlmx.le.0.0) snlmx=10.0
      call newln(2)
      write(lst,'(/''Limits of sin(th)/lambda for acceptance are : '',
     1            2f10.6)') snlmn,snlmx
      if(NComp(KPhase).gt.1) then
        call newln(2)
        write(lst,'(/''Fourier for subsystem #'',i1,'' will be '',
     1               ''calculated'')') nsubs
      endif
      rewind 82
      call FeMouseShape(3)
      call SetIntArrayTo(mxh,6,0)
      call FouNastavM80
      if(ErrFlag.ne.0) go to 9999
3000  if(ExistM80) then
        read(80,FormA,end=3100) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 3100
        endif
        read(Veta,Format80,end=3100,err=8000)(ih(i),i=1,maxNDim),KPh,
     1                                       fo1,fc
      else
        read(91,format91,end=3100,err=8100)
     1    (ih(i),i=1,NDim(KPhase)),fo1,fo2
        KPh=KPhase
      endif
      if(ih(1).gt.900) go to 3100
      if(KPh.ne.KPhase) go to 3000
      if(nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        do j=1,NDim(KPhase)
          mxh(j)=max(mxh(j),iabs(ihpp(iorien(j))))
        enddo
      enddo
      go to 3000
3100  iz=0
      n=1
      do i=1,NDim(KPhase)
        mxd(i)=2*mxh(i)+1
        n=n*mxd(i)
        if(mxd(i).gt.nx(i).and.i.le.NDimSum.and.
     1     FMethod.eq.FFTMethod) then
          if(i.gt.2) then
            NDimSum=i-1
          else
            nxo=nx(i)
            nx(i)=mxd(i)
            xfmxo=xfmx(i)
            xfmx(i)=xfmx(i)-xdf(i)
            xfmn(i)=xfmn(i)+xdf(i)
            xdf(i)=(xfmx(i)-xfmn(i))/float(nx(i)-2)
3150        xfmn(i)=xfmn(i)-xdf(i)
            xfmx(i)=xfmx(i)+xdf(i)
            if(xfmxo.gt.xfmx(i)) then
              nx(i)=nx(i)+2
              go to 3150
            endif
          endif
        endif
      enddo
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      allocate(fr(n),fi(n),mlt(n))
      call SetRealArrayTo(fr,n,0.)
      call SetRealArrayTo(fi,n,0.)
      call SetIntArrayTo(mlt,n,0)
      if(ExistM80) then
        rewind 80
        call FouNastavM80
        if(ErrFlag.ne.0) go to 9999
      else
        call NastavM90(91)
      endif
      mmax=0
      kolik=0
      konec=.false.
3500  if(ExistM80) then
        read(80,FormA256,end=5000) Veta
        if(NDatBlock.gt.1) then
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:5),'block')) go to 5000
        endif
        read(Veta,Format80,err=8000,end=5000)
     1    (ih(i),i=1,MaxNDim),KPh,fo1,fo2,fc,ac,bc,
     2     acfree,bcfree,acst,bcst,acfreest,bcfreest,sigfo1,sigfo2
        if(ih(1).gt.900) go to 5000
        if(KPh.ne.KPhase) go to 3500
        if(EqIV0(ih,maxNDim)) then
          bc=0.
          bcfree=0.
          bcst=0.
          bcfreest=0.
        endif
      else
        if(isPowder) then
          read(91,format91Pow,err=8100,end=5000) ih,fo1,fo2,i,i,KPh
          if(KPh.ne.KPhase) go to 3500
        else
          read(91,format91,err=8100,end=5000)(ih(i),i=1,NDim(KPhase)),
     1                                        fo1,sigfo1
          sigfo2=sigfo1
          sigfo1=0.
          sigfo2=0.
          KPh=KPhase
        endif
        fc=0.
        if(ih(1).lt.900) then
          if(fo1.gt.0.) then
            fo1=sqrt(fo1)
          else
            fo1=0.
          endif
          fo2=fo1
        else
          go to 5000
        endif
      endif
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      if(TMethod.eq.RatioMethod) then
        fo=fo1
        sigfo=sigfo1
      else
        fo=fo2
        sigfo=sigfo2
      endif
      if(vyber.gt.0.and.abs(fo).gt.vyber*abs(fc)) go to 3500
      if(sinthl.gt.snlmx.or.sinthl.lt.snlmn) go to 3500
      if(fc.ne.0.) then
        cosaq=ac/fc
        sinaq=bc/fc
      else
        cosaq=0.
        sinaq=0.
      endif
      if(NDim(KPhase).gt.3.and.nsubs.gt.1) then
        do i=1,NDim(KPhase)
          hh(i)=ih(i)
        enddo
        call multm(hh,zvi(1,nsubs,KPhase),hhp,1,NDim(KPhase),
     1             NDim(KPhase))
        do i=1,NDim(KPhase)
          ih(i)=nint(hhp(i))
        enddo
      endif
      if(mapa.eq.1) then
        frp=fo**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.2) then
        frp=fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.3) then
        frp=fo**2-fc**2
        fip=0.
        SigCoef=2.*fo*sigfo
        Coef=frp
      else if(mapa.eq.4) then
        frp=fo*cosaq
        fip=fo*sinaq
        SigCoef=sigfo
        Coef=fo
      else if(mapa.eq.5) then
        frp=fc*cosaq
        fip=fc*sinaq
        SigCoef=sigfo
        Coef=fc
      else if(mapa.eq.6) then
        pom=fo-fc
        frp=pom*cosaq
        fip=pom*sinaq
        SigCoef=sigfo
        Coef=pom
      else if(mapa.eq.7) then
        frp=ac-acfree
        fip=bc-bcfree
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.8) then
        frp=acst-acfreest
        fip=bcst-bcfreest
        SigCoef=0.
        Coef=frp
      else if(mapa.eq.9) then
        frp=1.
        fip=0.
        SigCoef=0.
        Coef=frp
      endif
!      if(mod(iabs(ih(2)+ih(3)),2).eq.1.or.
!     1   mod(iabs(ih(1)+ih(3)),2).eq.1.or.
!     2   mod(iabs(ih(1)+ih(2)),2).eq.1) then
!        frp=frp*12.
!        fip=fip*12.
!      endif
      if(mod(iabs(ih(2)+ih(3)),2).eq.0) then
        frp=frp*0.5
        fip=fip*0.5
        go to 451
      endif
      if(mod(iabs(ih(1)+ih(3)),2).eq.0) then
        frp=frp*0.5
        fip=fip*0.5
        go to 451
      endif
!      if(mod(iabs(ih(1)+ih(2)),2).eq.0) then
!        frp=frp*0.5
!        fip=fip*0.5
!      endif
451   if(SigCoef.gt.1.e-30.and.UseWeight.eq.1) then
        if(abs(Coef).gt.1.e-30) then
          pom=max(1.-(SigCoef/Coef)**2,0.)
        else
          pom=0.
        endif
        frp=pom*frp
        fip=pom*fip
      endif
      if(ih(1).eq.0.and.ih(2).eq.0.and.ih(3).eq.0) then
        frp=frp/2.
        fip=fip/2.
      endif
      if(Patterson) then
!        rho=sinthl**2*rhom
!        if(srnat.gt.0.) then
!          call SetFormF(sinthl)
!          pom=0.
!          do i=1,NAtFormula(KPhase)
!            pom=pom+AtMult(i,KPhase)*fx(i)
!          enddo
!        else
!          pom=1.
!        endif
!        pom=pom*exp(-OverAllB(KDatBlock)*rho)
!        frp=frp*(pom0/pom)**2
      endif
      do i=1,NSymmN(KPhase)
        call IndTr(ih,rm6(1,i,nsubs,KPhase),ihpp,NDim(KPhase))
        arg=0.
        do j=1,NDim(KPhase)
          arg=arg-float(ih(j))*s6(j,i,nsubs,KPhase)
        enddo
        arg=arg*pi2
        cs=cos(arg)
        sn=sin(arg)
        pom1=twov*(frp*cs-fip*sn)
        pom2=twov*(frp*sn+fip*cs)
        do j=1,NDim(KPhase)
          if(ihpp(iorien(j)).lt.0) then
            pom2=-pom2
            call IntVectorToOpposite(ihpp,ihpp,NDim(KPhase))
            go to 4118
          else if(ihpp(iorien(j)).gt.0) then
            go to 4118
          endif
        enddo
4118    indp=0
        do j=NDim(KPhase),1,-1
          indp=indp*mxd(j)+ihpp(iorien(j))+mxh(j)
        enddo
        fr(indp)=fr(indp)+pom1
        fi(indp)=fi(indp)+pom2
        mlt(indp)=mlt(indp)+1
      enddo
      go to 3500
5000  call SetIntArrayTo(ihp,6,0)
      call SetIntArrayTo(hmin,6, 999999)
      call SetIntArrayTo(hmax,6,-999999)
      if(FMethod.eq.FFTMethod) then
        Redukce=.5
      else
        Redukce=1.
      endif
      do i=1,UBound(fr,1)
        indp=i
        if(mlt(indp).le.0) cycle
        pom=Redukce/float(mlt(indp))
        fr(indp)=fr(indp)*pom
        fi(indp)=fi(indp)*pom
        do j=1,NDim(KPhase)
          ihp(j)=mod(indp,mxd(j))-mxh(j)
          indp=indp/mxd(j)
          if(j.gt.3) mmax=max(mmax,iabs(ihp(j)))
        enddo
        do j=1,NDim(KPhase)
          hmax(j)=max(hmax(j),ihp(j))
          hmin(j)=min(hmin(j),ihp(j))
        enddo
        write(82) ihp,fr(i),fi(i)
        if(FMethod.eq.FFTMethod) then
          call IntVectorToOpposite(ihp,ihp,NDim(KPhase))
          do j=1,NDim(KPhase)
            hmax(j)=max(hmax(j),ihp(j))
            hmin(j)=min(hmin(j),ihp(j))
          enddo
          write(82) ihp,fr(i),-fi(i)
        endif
      enddo
      if(FMethod.eq.BLMethod) then
        n=0
        m=0
        do i=1,NDim(KPhase)
          n=max(n,hmax(i)-hmin(i)+1)
          m=max(m,nx(i))
        enddo
        allocate(SinTable(n,m,NDim(KPhase)),
     1          CosTable(n,m,NDim(KPhase)))
        do n=1,NDim(KPhase)
          do i=hmin(n),hmax(n)
            ii=i-hmin(n)+1
            fh=pi2*float(i)
            do j=1,nx(n)
              arg=fh*(xfmn(n)+(j-1)*xdf(n))
              SinTable(ii,j,n)=sin(arg)
              CosTable(ii,j,n)=cos(arg)
            enddo
          enddo
        enddo
      endif
      rewind 82
      MameSatelity=mmax.gt.0
      if(.not.MameSatelity) then
        do i=1,3
          if(iorien(i).gt.3) go to 9000
        enddo
        call SetIntArrayTo(nx(4),NDimI(KPhase),1)
        call SetRealArrayTo(xfmn(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xfmx(4),NDimI(KPhase),0.)
        call SetRealArrayTo(xdf(4),NDimI(KPhase),0.1)
        nmap=nx(3)
      endif
      if(FMethod.eq.FFTMethod) then
        do i=1,NDimSum
          nx(i)=nint(1./xdf(i))
          n=2
6010      if(n.lt.nx(i)) then
            n=n*2
            go to 6010
          endif
          nx(i)=n
          xdf(i)=1./float(n)
          xfmn(i)=0.
          xfmx(i)=1.-xdf(i)
        enddo
        nxny=nx(1)*nx(2)
        nmap=nx(3)*nx(4)*nx(5)*nx(6)
      endif
      call newln(NDim(KPhase)+2)
      write(lst,FormA)
      write(lst,'(''Scope of the map :'')')
      do i=1,NDim(KPhase)
        if(nx(i).gt.1) then
          write(lst,'(a2,'' from'',f8.4,'' to'',f8.4,'' step'',f7.4)')
     1          cx(i),xfmn(i),xfmx(i),xdf(i)
        else
          xfmn(i)=(xfmn(i)+xfmx(i))*.5
          xfmx(i)=xfmn(i)
          write(lst,'(a2,'' fixed to'',f8.4)') cx(i),xfmn(i)
        endif
      enddo
      call newln(2)
      write(lst,FormA1)
      write(Veta,'(''Orientation : '',6i1)')(iorien(i),i=1,NDim(KPhase))
      write(lst,FormA) Veta(:idel(Veta))
      call newln(2)
      write(lst,FormA1)
      Veta='The calculation will performed'
      if(FMethod.eq.FFTMethod) then
        Veta(idel(Veta)+2:)='by FFT method'
      else
        Veta(idel(Veta)+2:)=' by modified Beevers-Lipson algorithm'
      endif
      write(lst,FormA) Veta(:idel(Veta))
      if(NTwin.gt.1) then
        call newln(2)
        write(lst,FormA1)
        Veta='F(obs) will be corrrected for twinning'
        if(FMethod.eq.FFTMethod) then
          Veta(idel(Veta)+2:)='by substracting of F(calc)'
        else
          Veta(idel(Veta)+2:)='by in ratio of F(calc)'
        endif
        write(lst,FormA) Veta(:idel(Veta))
      endif
      go to 9999
8000  call FeReadError(80)
      go to 8900
8100  call FeReadError(91)
8900  ErrFlag=1
9000  call DeleteFile(fln(:ifln)//'.m81')
      call DeleteFile(FileM82)
      call FeTmpFilesClear(FileM82)
9999  if(allocated(fr)) deallocate(fr,fi,mlt)
      return
      end
!  blender renders two spinning icosahedrons (red and green).
!  The blending factors for the two icosahedrons vary sinusoidally
!  and slightly out of phase.  blender also renders two lines of
!  text in a stroke font: one line antialiased, the other not.

      module blender_data

! with use here, we don't need them in each routine
      use opengl_gl
      use opengl_glu
      use opengl_glut

      implicit none

      real(glfloat) :: light0_ambient(4) = (/0.2, 0.2, 0.2, 1.0/)
      real(glfloat) :: light0_diffuse(4) = (/0.0, 0.0, 0.0, 1.0/)
      real(glfloat) :: light1_diffuse(4) = (/1.0, 0.0, 0.0, 1.0/)
      real(glfloat) :: light1_position(4) = (/1.0, 1.0, 1.0, 0.0/)
      real(glfloat) :: light2_diffuse(4) = (/0.0, 1.0, 0.0, 1.0/)
      real(glfloat) :: light2_position(4) = (/-1.0, -1.0, 1.0, 0.0/)
      real :: s = 0.0
      real(glfloat) :: angle1 = 0.0, angle2 = 0.0

      end module blender_data

      subroutine output(x, y, text)
      use blender_data

      implicit none

      real(glfloat), intent(in) :: x,y
      character(len=*), intent(in) :: text
      integer(glcint) :: p
      integer :: i

      call glPushMatrix()
      call glTranslatef(x, y, 0.0_glfloat)
      do i=1,len(text)
        p = ichar(text(i:i))
        call glutStrokeCharacter(GLUT_STROKE_ROMAN, p)
      end do
      call glPopMatrix()
      return
      end subroutine output

      subroutine display()
      use blender_data

      implicit none

      real(glfloat), save :: amb(4) = (/0.4, 0.4, 0.4, 0.0/)
      real(glfloat), save :: dif(4) = (/1.0, 1.0, 1.0, 0.0/)

      interface
        subroutine output(x, y, text)
        implicit none
        real, intent(in) :: x,y
        character(len=*), intent(in) :: text
        end subroutine output
      end interface

      call glClear(ior(GL_COLOR_BUFFER_BIT,GL_DEPTH_BUFFER_BIT))
      call glEnable(GL_LIGHT1)
      call glDisable(GL_LIGHT2)
      dif(4) = cos(s) / 2.0 + 0.5
      amb(4) = dif(4)
      call glMaterialfv(GL_FRONT, GL_AMBIENT, amb)
      call glMaterialfv(GL_FRONT, GL_DIFFUSE, dif)

      call glPushMatrix()
! let's take a chance that the default integer is the same kind as
! glint, and not bother with the _glint on constants
      call glTranslatef(-0.3, -0.3, 0.0)
      call glRotatef(angle1, 1.0, 5.0, 0.0)
      call glCallList(1)        ! render ico display list
      call glPopMatrix()

      call glClear(GL_DEPTH_BUFFER_BIT)
      call glEnable(GL_LIGHT2)
      call glDisable(GL_LIGHT1)
      dif(4) = 0.5 - cos(s * .95) / 2.0
      amb(4) = dif(4)
      call glMaterialfv(GL_FRONT, GL_AMBIENT, amb)
      call glMaterialfv(GL_FRONT, GL_DIFFUSE, dif)

      call glPushMatrix()
      call glTranslatef(0.3, 0.3, 0.0)
      call glRotatef(angle2, 1.0, 0.0, 5.0)
      call glCallList(1)        ! render ico display list
      call glPopMatrix()

      call glPushAttrib(GL_ENABLE_BIT)
      call glDisable(GL_DEPTH_TEST)
      call glDisable(GL_LIGHTING)
      call glMatrixMode(GL_PROJECTION)
      call glPushMatrix()
      call glLoadIdentity()
      call gluOrtho2D(0.0_gldouble, 1500.0_gldouble, 0.0_gldouble,
     1                1500.0_gldouble)
      call glMatrixMode(GL_MODELVIEW)
      call glPushMatrix()
      call glLoadIdentity()
!  Rotate text slightly to help show jaggies.
      call glRotatef(4.0, 0.0, 0.0, 1.0)
      call output(200., 225., "This is antialiased.")
      call glscalef(.5,.5,.5)
      call glDisable(GL_LINE_SMOOTH)
      call glDisable(GL_BLEND)
      call output(160., 100., "This text is not.")
      call glPopMatrix()
      call glMatrixMode(GL_PROJECTION)
      call glPopMatrix()
      call glPopAttrib()
      call glMatrixMode(GL_MODELVIEW)

      return
      end subroutine display

      subroutine idle()
      use blender_data

      implicit none

      angle1 = mod(angle1 + 0.8, 360.0)
      angle2 = mod(angle2 + 1.1, 360.0)
      s = s + 0.05
      call glutPostRedisplay()
      return
      end subroutine idle

      subroutine visible(vis)
      use blender_data

      implicit none

      integer, intent(in) :: vis

      interface
        subroutine idle()
        end subroutine idle
      end interface

      if (vis == GLUT_VISIBLE) then
        call glutIdleFunc(idle)
      else
        call glutIdleFunc(glutnullfunc)
      end if
      return
      end subroutine visible

      subroutine TestGL
      use blender_data
      use opengl_glut

      implicit none

      integer(glcint) :: i

      interface
        subroutine display()
        end subroutine display

        subroutine visible(vis)
          implicit none
          integer, intent(in) :: vis
        end subroutine visible
      end interface

      call glutInit()
      call glutInitDisplayMode(ior(ior(GLUT_DOUBLE,GLUT_RGB),
     1                         GLUT_DEPTH))
!      i = glutGetWindow()
!      write(6,'(i5)') i
!      call glutSetWindow(i)
      i = glutCreateWindow("blender")
      call glutDisplayFunc(display)
      call glutVisibilityFunc(visible)

      call glNewList(1, GL_COMPILE)  ! create ico display list
      call glutSolidIcosahedron()
      call glEndList()

      call glEnable(GL_LIGHTING)
      call glEnable(GL_LIGHT0)
      call glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient)
      call glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse)
      call glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse)
      call glLightfv(GL_LIGHT1, GL_POSITION, light1_position)
      call glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse)
      call glLightfv(GL_LIGHT2, GL_POSITION, light2_position)
      call glEnable(GL_DEPTH_TEST)
      call glEnable(GL_CULL_FACE)
      call glEnable(GL_BLEND)
      call glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
      call glEnable(GL_LINE_SMOOTH)
      call glLineWidth(2.0)

      call glMatrixMode(GL_PROJECTION)
      call gluPerspective( 40.0_gldouble,
     1                     1.0_gldouble,
     2                     1.0_gldouble,
     3                    10.0_gldouble)
      call glMatrixMode(GL_MODELVIEW)
      call gluLookAt(
     1  0.0_gldouble, 0.0_gldouble, 5.0_gldouble,! eye is at (0,0,5)
     2  0.0_gldouble, 0.0_gldouble, 0.0_gldouble, ! center is at (0,0,0)
     3  0.0_gldouble, 1.0_gldouble, 0.0_gldouble) ! up is in positive Y direction
      call glTranslatef(0.0, 0.6, -1.0)

      call glutMainLoop()
      return
      end
      subroutine Z6dNa4d
      integer ih(6)
      character*80 Veta
      Veta='D:\Structures\Jana2006\Work\Jakub Plasil\swamboit\6d-new\'
      open(40,Veta(:idel(Veta))//'pl127_4_modul_6d_bz.hkl')
      open(41,Veta(:idel(Veta))//'4d\4d.hkl')
1000  read(40,'(6i4,a)',end=5000) ih,Veta
      if(iabs(ih(4)).ne.0.or.iabs(ih(5)).ne.0) go to 1000
      write(41,'(4i4,a)') ih(1:3),ih(6),Veta(:idel(Veta))
      go to 1000
5000  close(40)
      close(41)
      return
      end
      subroutine Z6dNa6d
      integer ih(6)
      character*80 Veta
      Veta='D:\Structures\Jana2006\Work\Jakub Plasil\swamboit\'//
     1     'Nove pokusy\'
      open(40,Veta(:idel(Veta))//'6d\pl127_4_modul_6d_bz.hkl')
      open(41,Veta(:idel(Veta))//'6d\6d.hkl')
1000  read(40,'(6i4,a)',end=5000) ih,Veta
      if((iabs(ih(4)).ne.0.or.iabs(ih(5)).ne.0).and.iabs(ih(6)).ne.0)
     1  go to 1000
      write(41,'(6i4,a)') ih(1:6),Veta(:idel(Veta))
      go to 1000
5000  close(40)
      close(41)
      return
      end
      subroutine TestSSG
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest,SSGSymbol
      SSGSymbol=' '
      ich=0
1100  id=NextQuestId()
      xqd=450.
      il=1
      Veta='TestSSG'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='SSG symbol:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+3.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSSG=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,SSGSymbol)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        SSGSymbol=EdwStringQuest(nEdwSSG)
      endif
      call FeQuestRemove(id)
      if(ich.eq.0) then
        if(SSGSymbol.ne.' ') go to 1100
      endif
      return
      end
      subroutine PrevodAR
      include 'fepc.cmn'
      include 'basic.cmn'
      real TW(3,3,2),hin(3),h(3),htw(3),q(3,6),hp(3)
      character*256 Veta
      TW=0.
      TW(1,2,1)=-1.
      TW(2,1,1)= 1.
      TW(2,2,1)=-1.
      TW(3,3,1)= 1.
      TW(1,1,2)=-1.
      TW(1,2,2)= 1.
      TW(2,1,2)=-1.
      TW(3,3,2)= 1.
      Veta='D:\Structures\Jana2006\Work\Alexander Regnat\2k3m\Znovu\'//
     2 'Fullprof data\crb2_2k3m_all.int'
      open(43,file=Veta)
      Veta='D:\Structures\Jana2006\Work\Alexander Regnat\2k3m\Znovu\'//
     2 'Fullprof data\Twin1.hkl'
      open(44,file=Veta)
      Veta='D:\Structures\Jana2006\Work\Alexander Regnat\2k3m\Znovu\'//
     2 'Fullprof data\Twin2.hkl'
      open(45,file=Veta)
      Veta='D:\Structures\Jana2006\Work\Alexander Regnat\2k3m\Znovu\'//
     2 'Fullprof data\Twin3.hkl'
      open(46,file=Veta)
      do i=1,4
        read(43,FormA) Veta
      enddo
      do i=1,6
        read(43,'(i1,3f10.4)') k,q(1:3,k)
      enddo
1100  read(43,'(3f4.0,i5,2f14.4)',end=2000) hin,iq,RI,RS
      do i=1,3
        h(i)=hin(i)+q(i,iq)
      enddo
      do i=1,3
        hp(i)=h(i)+q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1200
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#1-'',i1)') nint(hp(1:3)),-1,iq
      write(44,'(4i4,2f8.4)') nint(hp(1:3)),-1,RI,RS
      go to 1100
1200  do i=1,3
        hp(i)=h(i)-q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1300
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#1-'',i1)') nint(hp(1:3)),1,iq
      write(44,'(4i4,2f8.4)') nint(hp(1:3)),1,RI,RS
      go to 1100
1300  call Multm(h,TW(1,1,1),htw,1,3,3)
      do i=1,3
        hp(i)=htw(i)+q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1400
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#2-'',i1)') nint(hp(1:3)),-1,iq
      write(45,'(4i4,2f8.4)') nint(hp(1:3)),-1,RI,RS
      go to 1100
1400  do i=1,3
        hp(i)=htw(i)-q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1500
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#2-'',i1)') nint(hp(1:3)),1,iq
      write(45,'(4i4,2f8.4)') nint(hp(1:3)),1,RI,RS
      go to 1100
1500  call Multm(h,TW(1,1,2),htw,1,3,3)
      do i=1,3
        hp(i)=htw(i)+q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1600
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#3-'',i1)') nint(hp(1:3)),-1,iq
      write(46,'(4i4,2f8.4)') nint(hp(1:3)),-1,RI,RS
      go to 1100
1600  do i=1,3
        hp(i)=htw(i)-q(i,1)
        if(abs(hp(i)-anint(hp(i))).gt..0001) go to 1700
      enddo
      write(6,'(3f10.4)') h
      write(6,'(4i4,'' Twin#3-'',i1)') nint(hp(1:3)),1,iq
      write(46,'(4i4,2f8.4)') nint(hp(1:3)),1,RI,RS
      go to 1100
1700  write(6,'(3f10.4)') h
      write(6,'('' Za boha nesla'')')
      go to 1100
2000  close(43)
      close(44)
      close(45)
      close(46)
      return
      end
      subroutine SrovnaniAR
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Cesta1,Cesta2
      integer ih1(3),ih2(4)
      logical EqIV
      Cesta1='D:\Structures\Jana2006\Work\Alexander Regnat\reduction\'//
     1       'magnetic\'
      Cesta2='D:\Structures\Jana2006\Work\Alexander Regnat\2k3m\'
      open(43,file=Cesta1(:idel(Cesta1))//'crb2_2k3m_5.hkl')
      open(44,file=Cesta1(:idel(Cesta1))//'crb2_2k3m_6.hkl')
      open(45,file=Cesta2(:idel(Cesta2))//'crb2_2k3m_chi.hkl')
1100  read(45,'(4i4,2f10.2)',end=5000) ih2,RI1,RS1
      rewind 43
1200  read(43,'(3i4,2f14.4)',end=1300) ih1,RI2,RS2
      if(EqIV(ih1,ih2,3).and.abs(RI1-RI2).lt..001) then
        write(6,'('' Nasel:'',4i4,'' | '',i4)') ih2,1
        go to 1100
      endif
      go to 1200
1300  rewind 44
1400  read(44,'(3i4,2f14.4)',end=1500) ih1,RI2,RS2
      if(EqIV(ih1,ih2,3).and.abs(RI1-RI2).lt..001) then
        write(6,'('' Nasel:'',4i4,'' | '',i4)') ih2,-1
        go to 1100
      endif
      go to 1400
1500  write(6,'('' Nenasel:'',4i4)') ih2
      go to 1100
5000  close(43)
      close(44)
      close(45)
      return
      end
      subroutine Prevod5dAR
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Cesta
      integer ih(5)
      logical EqIV
      Cesta='D:\Structures\Jana2006\Work\Alexander Regnat\Znovu\5d\'
      open(43,file=Cesta(:idel(Cesta))//'crb2_2k3n.hkl')
      open(44,file=Cesta(:idel(Cesta))//'crb2_5d.hkl')
1100  read(43,'(3i4,2f8.2)',end=1200) ih(1:3),RI,RS
      if(mod(abs(ih(1)),14).ne.0.or.mod(abs(ih(2)),14).ne.0) go to 1100
      ih(1)=ih(1)/14
      ih(2)=ih(2)/14
      ih(4)=0
      ih(5)=0
      write(44,'(5i4,2f10.2)') ih,RI,RS
      go to 1100
1200  close(43)
      open(43,file=Cesta(:idel(Cesta))//'crb2_2k3m_tau.hkl')
1300  read(43,'(4i4,2f10.2)',end=1400) ih(1:4),RI,RS
      if(ih(4).eq.1) then
        ih(5)= 0
      else if(ih(4).eq.-1) then
        ih(5)= 0
      else
        go to 1300
      endif
      write(44,'(5i4,2f10.2)') ih,RI,RS
      go to 1300
1400  close(43)
      open(43,file=Cesta(:idel(Cesta))//'crb2_2k3m_epsilon.hkl')
1500  read(43,'(4i4,2f10.2)',end=1600) ih(1:4),RI,RS
      if(ih(4).eq.1) then
        ih(4)=-1
        ih(5)=-1
      else if(ih(4).eq.-1) then
        ih(4)= 1
        ih(5)= 1
      else
        go to 1500
      endif
      write(44,'(5i4,2f10.2)') ih,RI,RS
      go to 1500
1600  close(43)
      open(43,file=Cesta(:idel(Cesta))//'crb2_2k3m_chi.hkl')
1700  read(43,'(4i4,2f10.2)',end=1800) ih(1:4),RI,RS
      if(ih(4).eq.1) then
        ih(4)= 0
        ih(5)= 1
      else if(ih(4).eq.-1) then
        ih(4)= 0
        ih(5)=-1
      else
        go to 1700
      endif
      write(44,'(5i4,2f10.2)') ih,RI,RS
      go to 1700
1800  close(43)
      close(44)
      return
      end
      subroutine Inver(lst0)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      real :: DxMod(3),GammaIntInv(9),xp(11),xpp(36),cpom(3,3),
     1        rpom(3,3),upom(3),tpisq=19.7392088,Mat(7,7),MatI(7,7)
      real, allocatable :: ScPom(:,:),OccArr(:),BetaArr(:,:),sctwn(:),
     1                     DerSave(:)
      character*256 Veta
      character*80  ch80,t80
      character*20  at
      character*12  lp
      character*4   t4
      integer RefGetKiInd,ipor(3),NxMod(3)
      logical m85opened,PwdCheckTOFInD,UzMame,lpom,MameDPopul
      real :: Dmat(30) = (/ 0.200, 0.200, 0.200, 0.200, 0.200,
     1                      1.040, 0.520, 0.520,-1.040,-1.040,
     2                      0.000, 0.943,-0.943, 0.000, 0.000,
     3                      1.400,-0.931,-0.931, 0.233, 0.233,
     4                      0.000, 1.110,-1.110, 0.000, 0.000,
     5                      0.000, 0.000, 0.000, 1.570,-1.570/),
     a        DmatM(110) = (/ 1.090, 0.000, 0.000, 0.000, 0.000,
     1                        1.880, 0.000, 0.000, 1.880, 0.000,
     2                        0.000, 1.090, 0.000, 0.000, 0.000,
     3                        0.000, 1.880,-1.880, 0.000, 0.000,
     4                        0.000, 0.000,-2.180, 0.000, 0.000,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000,-1.850, 1.600,
     7                        0.000, 0.000, 0.000, 0.000, 0.000,
     8                        3.680, 0.000, 0.000, 0.000, 0.000,
     9                       -1.060, 0.000, 0.000,-1.060, 0.000,
     a                        0.000, 3.680, 0.000, 0.000, 0.000,
     1                        0.000,-1.060, 1.060, 0.000, 0.000,
     2                        0.000, 0.000, 1.920, 0.000, 0.000,
     3                        0.000, 0.000, 0.000, 0.000, 0.000,
     4                        0.000, 0.000, 0.000, 2.300, 1.880,
     5                        0.000, 0.000, 0.000, 0.000, 0.000,
     6                        0.000, 0.000, 0.000, 0.000, 0.000,
     7                        2.100, 0.000, 0.000,-2.100, 0.000,
     8                        0.000, 0.000, 0.000, 0.000, 0.000,
     9                        0.000, 2.100, 2.100, 0.000, 0.000,
     a                        0.000, 0.000, 0.000, 0.000, 0.000,
     1                        0.000, 0.000, 0.000, 0.000, 3.140/),
     a        dpop(10),sdpop(10)
      save DerSave
      FinalInver=.false.
      if(nSingle.gt.0) then
        if(nRFacOverall.lt.3*nParRef) then
          lstp=lst
          lst=lst0
          ch80='Number of'
          write(Cislo,FormI15) nRFacOverall
          call Zhusti(Cislo)
          ch80=ch80(:idel(ch80)+1)//Cislo(:idel(Cislo))//
     1         ' reflections is smaller than'
          write(Cislo,FormI15) nParRef
          call Zhusti(Cislo)
          if(nRFacOverall.lt.nParRef) then
            Veta='number of '//Cislo(:idel(Cislo))//
     1           ' parameters to be refined.'
            call FeChybne(-1.,-1.,ch80,Veta,SeriousError)
            go to 9900
          else
            Veta='three times number of '//Cislo(:idel(Cislo))//
     1           ' parameters to be refined.'
            if(icykl.gt.0) call SetIgnoreWTo(.true.)
            call FeChybne(-1.,-1.,ch80,Veta,WarningWithESC)
            if(icykl.gt.0) call ResetIgnoreW
            if(ErrFlag.ne.0) go to 9999
          endif
          lst=lstp
        endif
      endif
      if(NDatBlock.gt.1.or.CorrESD.eq.0) then
        gofzp=GOFOverall
      else
        gofzp=GOFOverall*BerarCorr
      endif
      if(icykl.le.0) then
        jensc=.false.
        if(nParRef.le.1) go to 1055
        ij=0
        j=0
        allocate(ScPom(mxsc,NDatBlock))
        do KDatB=1,NDatBlock
          do i=1,MxScAll
            if(KiS(i,KDatB).ne.0) then
              j=j+1
              ij=ij+j
              if(i.le.MxScU) then
                pom=LSMat(ij)
                if(pom.gt.0.) then
                  pom=1./pom
                  ScPom(i,KDatB)=LSRS(j)*pom
                  pom=gofzp**2*pom
                else
                  ScPom(i,KDatB)=0.
                  exit
                endif
                jensc=jensc.or.abs(ScPom(i,KDatB)).gt.10.*sqrt(pom)
              endif
            endif
          enddo
        enddo
1055    if(jensc) then
          Ninfo=2
          TextInfo(1)='Change for scale factor(s) too large.'
          TextInfo(2)='The cycle will be repeated with refined one(s).'
          if(.not.VisiMsgSc) call FeMsgShow(2,-1.,-1.,.true.)
          do KDatB=1,NDatBlock
            do i=1,mxscu
              if(KiS(i,KDatB).ne.0) then
                if(CorrScPwd.and.i.eq.1.and.SumaYoPwd.gt.0..and.
     1             SumaYcPwd.gt.0.) then
                  sc(i,KDatB)=sc(i,KDatB)*sqrt(SumaYoPwd/SumaYcPwd)
                  CorrScPwd=.false.
                else
                  sc(i,KDatB)=sc(i,KDatB)+ScPom(i,KDatB)
                endif
              endif
              if(sc(i,KDatB).lt.0.) sc(i,KDatB)=0.
            enddo
          enddo
          if(NAtXYZMode.gt.0) call TrXYZMode(1)
          if(NAtMagMode.gt.0) call TrMagMode(1)
          go to 9000
        endif
      endif
      call OpenFile(85,fln(1:ifln)//'.m85','unformatted','unknown')
      m85opened=ErrFlag.eq.0
      if(m85opened) then
        write(85) neq
        do i=1,neq
          write(85) lnp(i),pab(i),npa(i)
          do j=1,npa(i)
            write(85) pnp(j,i),pko(j,i)
          enddo
        enddo
      endif
      if(nLSRS.gt.0) then
        ij=0
        do i=1,nLSRS
          ij=ij+i
          if(LSMat(ij).gt.0.) then
            if(LSMethod.eq.2) LSMat(ij)=LSMat(ij)*(1.+MarqLam)
            der(i)=1./sqrt(LSMat(ij))
          endif
        enddo
        open(73,file='Corr.mat')
        k=0
        do j=1,nLSRS
          do i=1,j
            k=k+1
            write(73,'(2i4,f10.6,e15.6)') i,j,
     1              LSMat(k)*der(i)*der(j),LSMat(k)
          enddo
        enddo
        close(73)
        call znorm(LSMat,der,nLSRS)
        call SmiHandleSing(LSMat,nLSRS,NSingArr,NSing)
        if(NSing.ge.2*nLSRS/3.and.NSing.gt.0) then
          go to 9600
        else if(NSing.gt.0) then
          write(ch80,'(''#'',i5)') ICykl+1
          call Zhusti(ch80)
          ch80='In the cycle '//ch80(:idel(ch80))//' there'
          if(NSing.eq.1) then
            ch80=ch80(:idel(ch80))//' was one singularity:'
          else
            write(Cislo,FormI15) NSing
            call Zhusti(Cislo)
            ch80=ch80(:idel(ch80))//' were '//Cislo(:idel(Cislo))//
     1           ' singularities:'
          endif
          write(LstSing,FormA) ch80(:idel(ch80))
          do i=1,NSing
            j=NSingArr(i)
            LSRS(j)=0.
            der(j)=1.
            write(t4,'(i4)') j
            call Zhusti(t4)
            call kdoco(-j,at,lp,0,pom,pom)
            k=RefGetKiInd(j)
            if(ki(k).gt.0) then
              lpom=LstOpened
              LstOpened=.false.
              Ch80='Singularity - element #'//t4(:idel(t4))//' : '//
     1             lp(:idel(lp))
              if(at.ne.' ') Ch80=Ch80(:idel(Ch80))//'['//at(:idel(at))//
     1                           ']'
              call FeChybne(-1.,-1.,ch80,
     1                      'The parameter will be blocked.',
     2                      WarningWithESC)
              LstOpened=lpom
              if(ErrFlag.ne.0) then
                call CloseIfOpened(85)
                go to 9999
              endif
              ch80='   '//ch80(15:idel(ch80))
              write(LstSing,FormA) ch80(:idel(ch80))
              ki(k)=ki(k)+1000
            endif
          enddo
          write(LstSing,FormA)
        endif
        call znorm(LSMat,der,nLSRS)
        ij=0
        do i=1,nLSRS
          ij=ij+i
          der(i)=1./sqrt(LSMat(ij))
        enddo




        k=0
        do j=1,nLSRS
          do i=1,j
            k=k+1
            pom=LSMat(k)*der(i)*der(j)
            if(i.lt.j.and.abs(pom).gt.corr) call piscor(i,j,pom)
            dc(i)=LSMat(k)*gofzp**2
          enddo
          if(m85opened) write(85)(dc(i),i=1,j)
        enddo
        call CloseIfOpened(85)
        call nasob(LSMat,LSRS,LSSol,nLSRS)
        call SetRealArrayTo(dc,LastNp,0.)
        call SetRealArrayTo(der,LastNp,0.)
        k=0
        iamp=0
        do ip=1,PosledniKiAtInd
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtMol,PosledniKiAtMol
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiMol,PosledniKiMol
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtXYZMode,PosledniKiAtXYZMode
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
        do ip=PrvniKiAtMagMode,PosledniKiAtMagMode
          if(ki(ip).ne.0) then
            k=k+1
            iamp=iamp+k
            dc(ip)=LSSol(k)
            der(ip)=gofzp*sqrt(LSMat(iamp))
          endif
        enddo
      endif
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.l40')
      if(ExistPowder)
     1  call CopyFile(fln(:ifln)//'.m41',fln(:ifln)//'.l41')
      if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1  call CopyFile(fln(:ifln)//'.m42',fln(:ifln)//'.l42')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.l50')
      call OpenFile(75,fln(:ifln)//'.l75','unformatted','unknown')
      write(75)(ki(i),i=1,PosledniKiAtMol),
     1         (dc(i),i=1,PosledniKiAtMol),
     2         (der(i),i=1,PosledniKiAtMol)
      if(NMolec.gt.0)
     1  write(75)(ki(i),i=PrvniKiMol,PosledniKiMol),
     2           (dc(i),i=PrvniKiMol,PosledniKiMol),
     3           (der(i),i=PrvniKiMol,PosledniKiMol)
      if(NAtXYZMode.gt.0)
     1  write(75)(ki(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     2           (dc(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode),
     3           (der(i),i=PrvniKiAtXYZMode,PosledniKiAtXYZMode)
      if(NAtMagMode.gt.0)
     1  write(75)(ki(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     2           (dc(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode),
     3           (der(i),i=PrvniKiAtMagMode,PosledniKiAtMagMode)
      close(75)
      if(allocated(DerSave)) deallocate(DerSave)
      allocate(DerSave(ubound(der,1)))
      DerSave(:)=Der(:)
      go to 1500
      entry InverFinal(lst0)
      FinalInver=.true.
      dc(:)=0.
      Der(:)=DerSave(:)
      go to 1500
      entry InverBackDamp(lst0)
1500  write(lst,'(''changes : '')')
      ivenr(1)=' '
      write(ivenr(2),'(i4)') icykl
      ivenr(3)='  su'
      InvDel=10
      call zmena(lBasicPar(1),pom,pom,1,' ',ip,1.)
      do KDatB=1,NDatBlock
        IZdvih=(KDatB-1)*MxScAll
        isPowder=iabs(DataType(KDatB)).eq.2
        call PwdSetTOF(KDatB)
        if(NDatBlock.gt.1) then
          write(at,'(''Block'',i2)') KDatB
          call Zhusti(at)
        else
          at=' '
        endif
        ivenr(1)=at
        ip=IZdvih
        if(isPowder) then
          n=1
          if(KManBackg(KDatB).gt.0) n=n+1
        else
          n=mxscu
        endif
        do i=1,n
          if(isPowder) then
            if(i.eq.1) then
              lp='PrfScale'
            else if(i.eq.2) then
              lp='BckgScale'
            endif
          else
            write(lp,100) i
            call zhusti(lp)
          endif
          call zmena(lp,sc(i,KDatB),scs(i,KDatB),2,at,ip,1.)
          if(mod(i,10).eq.0) call tisk
        enddo
        ip=ip+mxscu-n
        call tisk
        itwph=max(NTwin,NPhase)
        if(itwph.gt.1.and.DoLeBail.eq.0) then
          allocate(sctwn(itwph))
          pomo=sctw(1,KDatB)
          sctw(1,KDatB)=1.
          do i=2,itwph
            sctw(1,KDatB)=sctw(1,KDatB)-sctw(i,KDatB)
          enddo
          sctws(1,KDatB)=0.
          lp='nic'
          do k=1,2
            if(k.eq.1) then
              i1=2
            else
              i1=1
            endif
            do i=i1,itwph
              if(k.eq.1) pom=sctw(i,KDatB)
              write(Cislo,'(i2)') i
              if(NTwin.gt.1) then
                lp='twvol'
              else
                lp='phvol'
              endif
              lp=lp(:idel(lp))//Cislo(1:2)
              call zhusti(lp)
              if(i.eq.1) then
                ivenr(1)=ivenr(1)(1:InvDel)//'  '//lp(:idel(lp))
                sctwn(1)=1.
                do j=2,itwph
                  sctwn(1)=sctwn(1)-sctwn(j)
                enddo
                call pridej(pomo,sctws(i,KDatB),sctwn(i))
              else
                call zmena(lp,sctw(i,KDatB),sctws(i,KDatB),2,at,ip,1.)
              endif
              if(k.eq.1) then
                InvDel=10
                sctwn(i)=sctw(i,KDatB)
                sctw(i,KDatB)=pom
                sctws(1,KDatB)=sctws(1,KDatB)+sctws(i,KDatB)**2
              else
                if(mod(i,10).eq.0) call tisk
              endif
            enddo
            if(k.eq.1) then
              sctws(1,KDatB)=sqrt(sctws(1,KDatB))
              ip=ip-itwph+1
            endif
          enddo
          if(InvDel.gt.10) call tisk
          deallocate(sctwn)
        endif
        ip=IZdvih+MxSc
        lp='TOverall'
        call zmena(lp,OverAllB(KDatB),OverAllBS(KDatB),2,at,ip,1.)
        if(MaxMagneticType.gt.0) then
          do i=1,2
            if(i.eq.1) then
              lp='RhoMag'
            else
              lp='GMag'
            endif
            call zmena(lp,ecMag(i,KDatB),ecsMag(i,KDatB),2,at,ip,1.)
          enddo
        else
          ip=ip+2
        endif
        lp='sclam/2'
        call zmena(lp,ScLam2(KDatB),ScLam2S(KDatB),2,at,ip,1.)
        call tisk
        if(ScLam2(KDatB).lt.0.) ScLam2(KDatB)=0.
        ip=ip+2
        ipp=ip
        if(ExtTensor(KDatB).gt.0) then
          ip=ipp
          if(ExtType(KDatB).eq.2) then
            if(ExtTensor(KDatB).eq.1) then
              call zmena(lBasicPar(9),ec(1,KDatB),ecs(1,KDatB),
     1                   2,at,ip,1.)
            else
              do j=1,6
                call indext(j,k,l)
                write(lp,110) k,l
                call zmena(lp(:idel(lp)),ec(j,KDatB),ecs(j,KDatB),
     1                     2,at,ip,1.)
              enddo
            endif
          else
            if(ExtTensor(KDatB).eq.1) then
              if(ExtType(KDatB).eq.3) then
                call zmena(lBasicPar(9),ec(1,KDatB),ecs(1,KDatB),
     1                     2,at,ip,1.)
                call tisk
                ip=ip+5
              else
                ip=ip+6
              endif
              call zmena(lBasicPar(10),ec(7,KDatB),ecs(7,KDatB),2,
     1                   at,ip,1.)
            else
              ip=ip+6
              do j=1,6
                call indext(j,k,l)
                write(lp,102) 'g',k,l
                call zmena(lp(:idel(lp)),ec(j+6,KDatB),
     1                     ecs(j+6,KDatB),2,at,ip,1.)
              enddo
            endif
          endif
          call tisk
        endif
        ipp=mxsc+18+(KdatB-1)*MxScAll
        do KPh=1,NPhase
          KPhase=KPh
          if(kanref(KDatB).ne.0) then
            ip=ipp
            do j=1,NAtFormula(KPh)
              lp='f''('//
     1           AtType(j,KPh)(:idel(AtType(j,KPh)))//')'
              call zmena(lp,ffrRef(j,KPh,KDatB),sffrRef(j,KPh,KDatB),2,
     1                   at,ip,1.)
              if(mod(j,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
            ip=ipp+MaxNAtFormula
            do j=1,NAtFormula(KPh)
              lp='f"('//
     1        AtType(j,KPh)(:idel(AtType(j,KPh)))//')'
              call zmena(lp,ffiRef(j,KPh,KDatB),sffiRef(j,KPh,KDatB),2,
     1                   at,ip,1.)
              if(mod(j,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
          endif
          ipp=ipp+2*MaxNAtFormula
        enddo
      enddo
      if(ExistPowder) then
        IZdvih=0
        do KDatB=1,NDatBlock
          isPowder=iabs(DataType(KDatB)).eq.2
          if(.not.isPowder) go to 2240
          call PwdSetTOF(KDatB)
          if(NDatBlock.gt.1) then
            write(at,'(''Block'',i2)') KDatB
            call Zhusti(at)
          else
            at=' '
          endif
          ivenr(1)=at
          ip=IShiftPwd+IZdvih+ndoffPwd-1
          do i=1,3
            if(isTOF) then
              if(KUseTOFJason(KDatB).le.0) then
                lp=lTOF1Pwd(i)
              else
                lp=lTOF2Pwd(i)
              endif
            else
              lp=lShiftPwd(i)
            endif
            call zmena(lp,ShiftPwd(i,KDatB),ShiftPwds(i,KDatB),2,at,ip,
     1                 1.)
          enddo
          if(KUseTOFJason(KDatB).ge.1) then
            do i=4,7
              call zmena(lTOF2Pwd(i),ShiftPwd(i,KDatB),
     1                   ShiftPwds(i,KDatB),2,at,ip,1.)
            enddo
          endif
          call tisk
          lp=lBackgPwd
          ip=IBackgPwd+IZdvih+ndoffPwd-1
          do i=1,NBackg(KDatB)
            write(lp(5:),105) i
            call zhusti(lp)
            call zmena(lp,BackgPwd(i,KDatB),BackgPwds(i,KDatB),
     1                 2,at,ip,1.)
            if(mod(i,10).eq.0) call tisk
          enddo
          if(InvDel.gt.10) call tisk
          if(KRough(KDatB).ne.IdPwdRoughNone) then
            lp=lRoughPwd
            ip=IRoughPwd+IZdvih+ndoffPwd-1
            do i=1,2
              write(lp(6:),105) i
              call zhusti(lp)
              call zmena(lp,RoughPwd(i,KDatB),RoughPwds(i,KDatB),2,at,
     1                   ip,1.)
            enddo
            call tisk
          endif
          if(KRefLam(KDatB).eq.1) then
            ip=ILamPwd+IZdvih+ndoffPwd-1
            do i=1,NAlfa(KDatB)
              lp=lLamPwd
              write(lp(6:),105) i
              call zhusti(lp)
              call zmena(lp,LamPwd(i,KDatB),LamPwds(i,KDatB),2,at,ip,
     1                   1.)
            enddo
            call tisk
          endif
          if((KAsym(KDatB).ne.IdPwdAsymNone.and..not.isTOF).or.
     1       (KAsym(KDatB).ne.IdPwdAsymTOFNone.and.isTOF)) then
            ip=IAsymPwd+IZdvih+ndoffPwd-1
            do i=1,NAsym(KDatB)
              pom=1.
              if(isTOF) then
                if(KAsym(KDatB).eq.IdPwdAsymTOF1) then
                  lp=lAsymTOF1Pwd(i)
                else
                  lp=lAsymTOF2Pwd(i)
                endif
              else if(KAsym(KDatB).eq.IdPwdAsymDivergence) then
                if(KUseHS(KDatB).ge.1) then
                  ii=0
                else
                  ii=2
                endif
                lp=lAsymPwdD(i+ii)
              else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                if((i.eq.1.and.KUseRSW(KDatB).eq.0).or.
     1             (i.eq.2.and.
     2              PwdMethod(KDatB).ne.IdPwdMethodBBVDS.and.
     3              PwdMethod(KDatB).ne.IdPwdMethodBBFDS).or.
     4              (i.eq.6.and.KUsePSoll(KDatBLock).eq.0).or.
     5              (i.eq.7.and.KUseSSoll(KDatBLock).eq.0)) then
                  ip=ip+1
                  cycle
                endif
                if(i.eq.2.and.PwdMethod(KDatB).eq.IdPwdMethodBBVDS) then
                  lp=lAsymPwdF(8)
                else
                  lp=lAsymPwdF(i)
                endif
              else if(KAsym(KDatB).eq.IdPwdAsymDebyeInt) then
                lp=lAsymPwdDI(i)
              else
                lp=lAsymPwd
                call NToString(i,lp(5:))
              endif
              if(KAsym(KDatB).eq.IdPwdAsymFundamental.and..not.isTOF)
     1          then
                if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                  pom=1./ToRad
                  AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*pom
                endif
              endif
              call zmena(lp,AsymPwd(i,KDatB),AsymPwds(i,KDatB),2,at,ip,
     1                   pom)
              if(KAsym(KDatB).eq.IdPwdAsymFundamental.and..not.isTOF)
     1          then
                if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                  AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*ToRad
                  AsymPwdS(i,KDatB)=AsymPwdS(i,KDatB)*ToRad
                endif
              endif
              if(mod(i,10).eq.0) call tisk
            enddo
            if(InvDel.gt.10) call tisk
          endif
          if(KUseTOFAbs(KDatB).gt.0) then
            lp=lTOFAbsPwd
            ip=ITOFAbsPwd+IZdvih+ndoffPwd-1
            call zmena(lp,TOFAbsPwd(KDatB),TOFAbsPwds(KDatB),2,at,ip,
     1                 pom)
            call tisk
          endif
2240      IZdvih=IZdvih+NParRecPwd
        enddo
        IZdvihP=0
        do KPh=1,NPhase
          KPhase=KPh
          ivenr(1)=PhaseName(KPh)
          call RefCellVolume
          CellVolPwdOld=CellVolPwd
          call RefDensity(CellVolPwd,CellVolPwds,DensityPwdOld,
     1                    DensityPwdS(KPh))
          ip=ICellPwd+ndoffPwd-1+IZdvihP
          do i=1,6
            call zmena(lcell(i),CellPwd(i,KPh),CellPwds(i,KPh),2,
     1                 PhaseName(KPh),ip,1.)
          enddo
          ivenr(1)=ivenr(1)(:InvDel)//'   Volume'
          call RefCellVolume
          call pridej(CellVolPwdOld,CellVolPwds,CellVolPwd)
          call RefDensity(CellVolPwd,CellVolPwds,DensityPwd(KPh),
     1                    DensityPwdS(KPh))
          ivenr(1)=ivenr(1)(:InvDel)//'   Density'
          call pridej(DensityPwdOld,DensityPwdS(KPh),DensityPwd(KPh))
          call tisk
          ip=IQuPwd+ndoffPwd-1+IZdvihP
          do j=1,NDimI(KPhase)
            do i=1,3
              write(lp,'(''q'',i1)') i
              if(NDim(KPhase).gt.4) write(lp(3:3),'(i1)') j
              call zmena(lp,QuPwd(i,j,KPh),QuPwds(i,j,KPh),2,
     1                   PhaseName(KPh),ip,1.)
            enddo
            call tisk
          enddo
          IZdvih=IZdvihP
          do KDatB=1,NDatBlock
            call PwdSetTOF(KDatB)
            KPhase=KPh
            if(NPhase.le.1) then
              if(NDatBlock.gt.1) then
                write(at,'(''Block'',i2)') KDatB
                call Zhusti(at)
              else
                at=' '
              endif
            else
              if(NDatBlock.gt.1) then
                write(at,'(''%Block'',i2)') KDatB
                call Zhusti(at)
                at=PhaseName(KPh)(:idel(PhaseName(KPh)))//at(:idel(at))
                call Zhusti(at)
              else
                at=PhaseName(KPh)
              endif
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              ip=IGaussPwd+ndoffPwd-1+IZdvih
              if(isTOF) then
                ito=3
              else
                ito=4
              endif
              do i=1,ito
                if(isTOF) then
                  Veta=lGaussPwdTOF(i)
                else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  Veta=lGaussPwdF(i)
                  pomo=GaussPwd(i,KPh,KDatB)
                else
                  Veta=lGaussPwd(i)
                endif
                call zmena(Veta,GaussPwd(i,KPh,KDatB),
     1                     GaussPwds(i,KPh,KDatB),2,at,ip,1.)
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  if((i.eq.1.and.GaussPwd(i,KPh,KDatB).gt.99999.).or.
     1               (i.eq.3.and.GaussPwd(i,KPh,KDatB).lt.0.)) then
                    go to 9610
                  endif
                endif
                if(KAsym(KDatB).eq.IdPwdAsymFundamental.and.
     1             (i.eq.1.or.i.eq.3)) then
                  if(i.eq.1) then
                    Fact=sqrt8ln2/sqrt(2.*pi)
                    Veta='DV'
                  else if(i.eq.3) then
                    Fact=.25/Fact
                    Veta='Eps'
                  endif
                  pomo=pomo*Fact
                  pom =GaussPwd (i,KPh,KDatB)*Fact
                  poms=GaussPwds(i,KPh,KDatB)*Fact
                  ivenr(1)=ivenr(1)(1:InvDel)//'  '//Veta(:idel(Veta))
                  call pridej(pomo,poms,pom)
                endif
              enddo
              call tisk
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              ip=ILorentzPwd+ndoffPwd-1+IZdvih
              if(isTOF) then
                ik=3
                if(KStrain(KPh,KDatB).eq.IdPwdStrainAxial) ik=ik+2
              else
                ik=4
              endif
              do i=1,ik
                if(isTOF) then
                  Veta=lLorentzPwdTOF(i)
                else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  Veta=lLorentzPwdF(i)
                  pomo=LorentzPwd(i,KPh,KDatB)
                else
                  Veta=lLorentzPwd(i)
                endif
                call zmena(Veta,LorentzPwd(i,KPh,KDatB),
     1                     LorentzPwds(i,KPh,KDatB),2,at,ip,1.)
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  if((i.eq.1.and.LorentzPwd(i,KPh,KDatB).gt.99999.).or.
     1               (i.eq.3.and.LorentzPwd(i,KPh,KDatB).lt.0.)) then
                    go to 9610
                  endif
                endif
                if(KAsym(KDatB).eq.IdPwdAsymFundamental.and.i.le.4) then
                  if(i.le.2) then
                    Fact=2./Pi
                    Veta='DV'
                    if(i.eq.2) Veta=Veta(:idel(Veta))//'A'
                  else if(i.le.4) then
                    Fact=Pi/8.
                    Veta='Eps'
                    if(i.eq.4) Veta=Veta(:idel(Veta))//'A'
                  endif
                  pomo=pomo*Fact
                  pom =LorentzPwd (i,KPh,KDatB)*Fact
                  poms=LorentzPwds(i,KPh,KDatB)*Fact
                  ivenr(1)=ivenr(1)(1:InvDel)//'  '//Veta(:idel(Veta))
                  call pridej(pomo,poms,pom)
                endif
              enddo
              if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
                ip=IZetaPwd+ndoffPwd-1+IZdvih
                call zmena(lZetaPwd,ZetaPwd(KPh,KDatB),
     1                     ZetaPwds(KPh,KDatB),2,at,ip,1.)
              endif
              call tisk
            endif
            if(NBroadHKLPwd(KPh,KDatB).gt.0) then
              ip=IBroadHKLPwd+ndoffPwd-1+IZdvih
              do i=1,NBroadHKLPwd(KPh,KDatB)
                write(Cislo,'(i2)') i
                lp=lBroadHKL(:idel(lBroadHKL))//Cislo(1:2)
                call zmena(lp,BroadHKLPwd (i,KPh,KDatB),
     1                        BroadHKLPwdS(i,KPh,KDatB),2,at,ip,1.)
              enddo
              call tisk
            endif
            if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
              ip=IStPwd+ndoffPwd-1+IZdvih
              lp=lStPwd
              if(NDimI(KPhase).eq.1) then
                n=35
              else
                n=15+NDimI(KPhase)
              endif
              j=0
              do i=1,n
                if(n.eq.35) then
                  call NToStrainString4(i,lp(3:))
                  if(lp(6:6).eq.'3'.or.lp(6:6).eq.'4') then
                    ip=ip+1
                    cycle
                  endif
                else
                  call NToStrainString(i,lp(3:))
                endif
                j=j+1
                call zmena(lp,StPwd(i,KPh,KDatB),StPwds(i,KPh,KDatB),2,
     1                     at,ip,1.)
                if(mod(j,10).eq.0) call tisk
              enddo
              call tisk
            endif
            if(KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              ip=IPrefPwd+ndoffPwd-1+IZdvih
              lp=lPrefPwd
              do i=1,2
                call NToString(i,lp(5:))
                call zmena(lp,PrefPwd(i,KPh,KDatB),
     1                     PrefPwds(i,KPh,KDatB),2,at,ip,1.)
              enddo
              call tisk
            endif
            IZdvih=IZdvih+NParProfPwd
          enddo
          IZdvihP=IZdvihP+NParCellProfPwd
        enddo
      endif
      do KDatB=1,NDatBlock
        ip=ndoffED+MxEDRef*NMaxEDZone*(KDatB-1)
        if(Radiation(KDatB).ne.ElectronRadiation.or.
     1     .not.ExistM42.or..not.CalcDyn) cycle
        do i=1,NEDZone(KDatBlock)
          if(.not.UseEDZone(i,KDatB)) then
            ip=ip+6
            cycle
          endif
          write(at,'(''Zone#'',i5)') i
          call Zhusti(at)
          if(NDatBlock.gt.1) then
            write(Cislo,'(''%'',i5)') KDatB
            call Zhusti(Cislo)
            at=at(:idel(at))//Cislo(:idel(Cislo))
          endif
          ivenr(1)=at
          do j=1,6
            if(j.eq.1) then
              call Zmena(lEDVar(j),ScEDZone(i,KDatB),ScEDZoneS(i,KDatB),
     1                   2,at,ip,1.)
            else if(j.eq.2) then
              call Zmena(lEDVar(j),ThickEDZone(i,KDatB),
     1                   ThickEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.3) then
              call Zmena(lEDVar(j),XNormEDZone(i,KDatB),
     1                   XNormEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.4) then
              call Zmena(lEDVar(j),YNormEDZone(i,KDatB),
     1                   YNormEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.5) then
              call Zmena(lEDVar(j),PhiEDZone(i,KDatB),
     1                   PhiEDZoneS(i,KDatB),2,at,ip,1.)
            else if(j.eq.6) then
              call Zmena(lEDVar(j),ThetaEDZone(i,KDatB),
     1                   ThetaEDZoneS(i,KDatB),2,at,ip,1.)
            endif
          enddo
          call tisk
        enddo
      enddo
      if(DoLeBail.le.0) then
        ip=ndoff
        if(NAtXYZMode.gt.0) call TrXYZMode(1)
        if(NAtMagMode.gt.0) call TrMagMode(1)
        do KPh=1,NPhase
          KPhase=KPh
          if(Nphase.gt.1) write(lst,'(''label : '',a8)') PhaseName(KPh)
          do isw=1,NComp(KPh)
            call invat(NAtIndFr(isw,KPh),NAtIndTo(isw,KPh),isw)
          enddo
          call invmol
        enddo
        ip=PrvniKiAtXYZMode-1
        do i=1,NAtXYZMode
          At=Atom(IAtXYZMode(1,i))
          ivenr(1)=at
          do j=1,NMAtXYZMode(i)
            call Zmena(LAtXYZMode(j,i),AtXYZMode(j,i),SAtXYZMode(j,i),2,
     1                 at,ip,1.)
            if(InvDel.gt.115) call tisk
          enddo
          if(InvDel.gt.10) call tisk
        enddo
        ip=PrvniKiAtMagMode-1
        do i=1,NAtMagMode
          At=Atom(IAtMagMode(1,i))
          ivenr(1)=at
          do j=1,NMAtMagMode(i)
            call Zmena(LAtMagMode(j,i),AtMagMode(j,i),SAtMagMode(j,i),2,
     1                 at,ip,1.)
            if(InvDel.gt.115) call tisk
          enddo
          if(InvDel.gt.10) call tisk
        enddo
      endif
      call zmena(lBasicPar(1),pom,pom,3,' ',ip,1.)
      call RefAppEq(0,0,0)
      if(nvai.gt.0) call SetRes(0)
      call RefKeepGRigidUpdate
      if(nKeep.gt.0) call RefSetKeep
      if(.not.isPowder.or.DoLeBail.eq.0) then
        if(NAtXYZMode.gt.0) call TrXYZMode(0)
        if(NAtMagMode.gt.0) call TrMagMode(0)
      endif
!      call RefKeepGRigidUpdate
      if(ExistElectronData.and.ExistM42.and.CalcDyn) call SetElDyn(1,0)
      if(ChargeDensities.and.lasmaxm.gt.0) then
        if(iaute.le.1) then
          if(iaute.eq.1) call RefPopvSet
          call RefPopvEpilog
        else if(iaute.eq.2) then
          call RefPopvRenorm
        endif
      endif
      call RefFixOriginRenorm
      call iom40(1,0,fln(:ifln)//'.m40')
      if(allocated(ScRes)) then
        do j=1,NDatBlock
          if(Radiation(j).ne.ElectronRadiation.or.
     1       .not.ExistM42.or..not.CalcDyn) cycle
          do i=1,mxsc
            pom=Sc(i,j)
            Sc(i,j)=ScRes(i,j)
            ScRes(i,j)=pom
          enddo
        enddo
      endif
      if(ErrFlag.ne.0) go to 9900
      if(ExistElectronData.and.ExistM42.and.CalcDyn)
     1  call iom42(1,0,fln(:ifln)//'.m42')
      call RefScreenDatBlocks
      if(ExistPowder) call comsym(0,0,ich)
      call RefSetMatrixLimits(0)
      do KDatB=1,NDatBlock
        if(KAnRef(KDatB).ne.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatB),FFra(1,KPh,KDatB),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatB),FFia(1,KPh,KDatB),
     1                   NAtFormula(KPh))
          enddo
        endif
      enddo
      n=0
      do KPh=1,NPhase
        do isw=1,NComp(KPh)
          do ik=1,2
            if(ik.eq.1) then
              ia1=NAtIndFr(isw,KPh)
              ia2=NAtIndTo(isw,KPh)
            else
              ia1=NAtPosFr(isw,KPh)
              ia2=NAtPosTo(isw,KPh)
            endif
            do ia=ia1,ia2
              if(itf(ia).eq.2) n=n+1
            enddo
          enddo
        enddo
      enddo
      if(n.le.0) go to 4100
      write(lst,'(''eigenvalues : '')')
      do KPh=1,NPhase
        KPhase=KPh
        if(NPhase.gt.1) then
          write(lst,FormA)
          write(lst,FormA) 'Phase: '//
     1                     PhaseName(KPh)(:idel(PhaseName(KPh)))
          write(lst,FormA)
        endif
        do isw=1,NComp(KPh)
          write(lst,FormA)
          Veta='Eigenvalues and eigenvectors are related to the '//
     1         'orthonormal system defined by the following '//
     2         'transformation matrix:'
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          do i=1,3
            write(lst,'(25x,3f12.4)')(TrToOrtho(j,isw,KPh),j=i,9,3)
          enddo
          write(lst,FormA)
          Veta='Atom           Eigenvalues               Eigenvectors'
          write(lst,FormA) Veta(:idel(Veta))
          write(lst,FormA)
          call srotb(TrToOrtho(1,isw,KPh),TrToOrtho(1,isw,KPh),xpp)
          do ik=1,2
            if(ik.eq.1) then
              ia1=NAtIndFr(isw,KPh)
              ia2=NAtIndTo(isw,KPh)
            else
              ia1=NAtPosFr(isw,KPh)
              ia2=NAtPosTo(isw,KPh)
            endif
            do ia=ia1,ia2
              if(itf(ia).ne.2) cycle
              call MultM(xpp,beta(1,ia),xp,6,6,1)
              do j=1,6
                xp(j)=xp(j)/tpisq
              enddo
              do m=1,6
                call indext(m,j,k)
                cpom(j,k)=xp(m)
                if(j.ne.k) cpom(k,j)=xp(m)
              enddo
              call qln(cpom,rpom,upom,3)
              ipor(1)=1
              pommn=upom(1)
              ipor(3)=1
              pommx=upom(1)
              do i=2,3
                if(upom(i).gt.pommx) then
                  pommx=upom(i)
                  ipor(3)=i
                endif
                if(upom(i).lt.pommn) then
                  pommn=upom(i)
                  ipor(1)=i
                endif
              enddo
              ipor(2)=6-ipor(1)-ipor(3)
              write(lst,'(a8,6x,f10.6,8x,3f10.6/
     1                   2(14x,f10.6,8x,3f10.6/))')
     2          atom(ia),(upom(ipor(i)),(rpom(j,ipor(i)),j=1,3),i=1,3)
            enddo
          enddo
        enddo
      enddo
4100  write(lst,'(''messages : '')')
      if(DoLeBail.ne.0) go to 4200
      do KDatB=1,NDatBlock
        if(.not.UseDatBlockActual(KDatB)) cycle
        if(ExtType(KDatB).eq.3) then
          if(ec(1,KDatB).gt.999.*ec(7,KDatB)) then
            ec(1,KDatB)=1000.*ec(7,KDatB)
            kis(MxSc+8,KDatB)=0
          else if(ec(7,KDatB).gt.999.*ec(1,KDatB)) then
            ec(7,KDatB)=1000.*ec(1,KDatB)
            kis(MxSc+14,KDatB)=0
          endif
        endif
        if(ExtType(KDatB).eq.1) then
          i=7
        else
          i=1
        endif
        if(ExtTensor(KDatB).le.0) then
          cycle
        else if(ExtTensor(1).eq.1) then
          if(ec(i,KDatB).lt.0.) write(lst,103)
        else if(ExtTensor(1).eq.2) then
          if(ec(i,KDatB).le.0..or.
     1       ec(i,KDatB)*ec(i+1,KDatB)-ec(i+3,KDatB)**2.le.0..or.
     2       ec(i,KDatB)*ec(i+1,KDatB)*ec(i+2,KDatB)+
     3       2.*ec(i+3,KDatB)*ec(i+4,KDatB)*ec(i+5,KDatB)
     4       -ec(i,KDatB)*ec(i+5,KDatB)**2
     5       -ec(i+1,KDatB)*ec(i+4,KDatB)**2
     6       -ec(i+2,KDatB)*ec(i+3,KDatB)**2.le.0.) write(lst,104)
        endif
      enddo
4200  do KDatB=1,NDatBlock
        if(UseDatBlockActual(KDatB)) then
          isPowder=iabs(DataType(KDatB)).eq.2
          call PwdSetTOF(KDatB)
          do i=1,mxscu
            if(sc(i,KDatB).lt.0.) sc(i,KDatB)=0.
          enddo
        endif
      enddo
      if(ChargeDensities.and.lasmaxm.ge.3) then
        write(lst,'(''dpopulations : '')')
        do KPh=1,NPhase
          KPhase=KPh
          MameDPopul=.false.
          do ia=1,NAtCalc
            if(kswa(ia).ne.KPh) cycle
            j=AtNum(isf(ia),KPh)
            if((j.ge.21.or.j.eq.15.or.j.eq.16).and.lasmax(ia).ge.3) then
              if(.not.MameDPopul) then
                if(NPhase.gt.1) then
                  write(lst,FormA)
                  write(lst,FormA) 'Phase: '//
     1                             PhaseName(KPh)(:idel(PhaseName(KPh)))
                  write(lst,FormA)
                endif
                MameDPopul=.true.
              endif
              xp(1)=popv(ia)+popas(1,ia)
              xp(2)=popas(5,ia)
              xp(3)=popas(8,ia)
              xp(4)=popas(17,ia)
              xp(5)=popas(20,ia)
              xp(6)=popas(24,ia)
              call MultM(DMat,xp,dpop,5,6,1)
              xp(1)=spopv(ia)+spopas(1,ia)
              xp(2)=spopas(5,ia)
              xp(3)=spopas(8,ia)
              xp(4)=spopas(17,ia)
              xp(5)=spopas(20,ia)
              xp(6)=spopas(24,ia)
              call MultMQ(DMat,xp,sdpop,5,6,1)
              SumDpop=0.
              do j=1,5
                SumDpop=SumDPop+DPop(j)
              enddo
              write(lst,111)
              write(lst,112) Atom(ia),dpop(1:5),SumDPop
              write(lst,113) sdpop(1:5)
              xp(1)=popas(6,i)
              xp(2)=popas(7,i)
              xp(3)=popas(8,i)
              xp(4)=popas(9,i)
              xp(5)=popas(18,i)
              xp(6)=popas(19,i)
              xp(7)=popas(20,i)
              xp(8)=popas(21,i)
              xp(9)=popas(22,i)
              xp(10)=popas(23,i)
              xp(11)=popas(25,i)
              call MultM(DMatM,xp,dpop,10,11,1)
              xp(1)=spopas(6,i)
              xp(2)=spopas(7,i)
              xp(3)=spopas(8,i)
              xp(4)=spopas(9,i)
              xp(5)=spopas(18,i)
              xp(6)=spopas(19,i)
              xp(7)=spopas(20,i)
              xp(8)=spopas(21,i)
              xp(9)=spopas(22,i)
              xp(10)=spopas(23,i)
              xp(11)=spopas(25,i)
              call MultMQ(DMatM,xp,sdpop,10,11,1)
              write(lst,FormA)
              write(lst,114)
              write(lst,112) Atom(ia),dpop(1:10)
              write(lst,113) sdpop(1:10)
              write(lst,FormA)
            endif
          enddo
        enddo
      endif
      AtDisableNNew=0
      UzMame=.false.
      do ia=1,NAtAll
        if(ia.ge.NAtPosFr(1,1).and.ia.lt.NAtMolFr(1,1)) cycle
        if(ai(ia).le.0..or.itf(ia).eq.0) cycle
        isw=iswa(ia)
        if(NDimI(KPhase).gt.0.and.KModA(3,ia).gt.0) then
          if(.not.UzMame) then
            n=1
            do i=1,NDimI(KPhase)
              DxMod(i)=.01
              NxMod(i)=100
              n=n*100
            enddo
            if(Allocated(OccArr)) deallocate(OccArr,BetaArr)
            allocate(OccArr(n),BetaArr(6,n))
            call UnitMat(GammaIntInv,NDimI(KPhase))
          endif
          call SetRealArrayTo(xpp,NDimI(KPhase),0.)
          call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),1)
          if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
            call MakeOccMod(OccArr,qcnt(1,ia),xp,NxMod,DxMod,a0(ia),
     1                      ax(1,ia),ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                      GammaIntInv)
            if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0)
     1        call MakeOccModSawTooth(OccArr(1),qcnt(1,ia),xp,NxMod,
     2                                DxMod,uy(1,KModA(2,ia),ia),
     3                                uy(2,KModA(2,ia),ia),KFA(2,ia),
     4                                GammaIntInv)
          else
            call MakeOccModPol(OccArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                         ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                         ax(KModA(1,ia),ia),a0(ia)*.5,
     3                         GammaIntInv,TypeModFun(ia))
          endif
          if(TypeModFun(ia).le.1) then
            call MakeBetaMod(BetaArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                       bx(1,1,ia),by(1,1,ia),KModA(3,ia),
     2                       KFA(3,ia),GammaIntInv)
          else
            call MakeBetaModPol(BetaArr,qcnt(1,ia),xp,NxMod,DxMod,
     1                          bx(1,1,ia),by(1,1,ia),KModA(3,ia),
     2                          ax(1,ia),a0(ia)*.5,GammaIntInv,
     3                          TypeModFun(ia))
          endif
          do i=1,n
            if(OccArr(i).le.0.) cycle
            call AddVek(Beta(1,ia),BetaArr(1,i),xpp,6)
            if(xpp(1).lt.0..or.
     1         xpp(1)*xpp(2)-xpp(4)**2.le.0..or.
     2         xpp(1)*xpp(2)*xpp(3)-xpp(3)*xpp(4)**2-
     2         xpp(1)*xpp(6)**2-xpp(2)*xpp(5)**2+
     3          2.*xpp(4)*xpp(5)*xpp(6).le.0.) then
              write(lst,108) atom(ia)(:idel(atom(ia)))
              exit
            endif
          enddo
        else
          if(beta(1,ia).lt.0.) then
            write(lst,107) atom(ia)(:idel(atom(ia)))
          else if(itf(ia).eq.1) then
            if(AtDisableFact(ia).gt.0.) then
              if(UisoLim.gt.0..and.ai(ia).gt.0.and.
     1           beta(1,ia).gt.UisoLim*episq*AtDisableFact(ia)) then
                ai(ia)=0.
                call SetIntArrayTo(ki(PrvniKiAtomu(ia)),
     1                             DelkaKiAtomu(ia),0)
                call SetIntArrayTo(KiA(1,ia),DelkaKiAtomu(ia),0)
                AtDisable(ia)=.true.
              endif
              if(AtDisable(ia)) AtDisableNNew=AtDisableNNew+1
            endif
          else if(itf(ia).ne.1) then
           if(beta(1,ia)*beta(2,ia)-beta(4,ia)**2.le.0..or.
     1        beta(1,ia)*beta(2,ia)*beta(3,ia)-beta(3,ia)*beta(4,ia)**2-
     2        beta(1,ia)*beta(6,ia)**2-beta(2,ia)*beta(5,ia)**2+
     3        2.*beta(4,ia)*beta(5,ia)*beta(6,ia).le.0.)
     4        write(lst,107) atom(ia)(:idel(atom(ia)))
          endif
        endif
      enddo
      if(UisoLim.gt.0.) call RefSetMatrixLimits(0)
9000  if(DoLeBail.ne.0) then
        do KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).eq.2) then
            pom=sc(1,KDatB)
            sc(1,KDatB)=scPwd(KDatB)
            scPwd(KDatB)=pom
          endif
        enddo
      endif
      if(allocated(ScRes)) then
        do j=1,NDatBlock
          if(Radiation(j).ne.ElectronRadiation.or.
     1       .not.ExistM42.or..not.CalcDyn) cycle
          do i=1,mxsc
            pom=Sc(i,j)
            Sc(i,j)=ScRes(i,j)
            ScRes(i,j)=pom
          enddo
        enddo
      endif
      go to 9999
9600  lst=lst0
      write(Cislo,FormI15) NSing
      call Zhusti(Cislo)
      ch80='The number of '//Cislo(:idel(Cislo))//
     1     ' corrections for singularities too large.'
      go to 9800
9610  call kdoco(ip,at,lp,0,pom,pom)
      ch80='The parameter '//lp(:idel(lp))//'['//at(:idel(at))//']'//
     1     ' reached unreallistic value.'
9800  call FeChybne(-1.,-1.,'numerical problem in INVER.',ch80,
     1              SeriousError)
9900  ErrFlag=1
9999  if(allocated(ScPom)) deallocate(ScPom)
      return
100   format('scale',i2)
101   format('#',i2)
102   format(a1,2i1)
103   format(' extinction parameter has negative value')
104   format(' extinction tensor is not positive definite')
105   format(i2)
107   format('Harmonic ADP tensor of the atom "',a,
     1       '" is not positive definite')
108   format('Modulated harmonic ADP tensor of the atom "',a,
     1       '" is not positive in some regions'/
     2       '   For more details run "Grapht" and draw U(min) value')
110   format('rho',2i1)
111   format('Atom         z2        xz        yz       x2-y2      xy',
     1       '      tot d-pop')
112   format(a8,11f10.4)
113   format('s.u.    ',11f10.4)
114   format('Atom        z2/xz     z2/yz    z2/x2-y2   z2/xy     xz/xy'
     1      ,'    xz/x2-y2   xz/yz    yz/x2-y2   yz/xy    x2-y2/xy')
      end
      subroutine CompMatCrenel
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001,nn=7,nnn=(nn*(nn+1))/2)
      real Mat(nn,nn,2),MatCov(nn,nn,2),FOld(nn,NPoints),
     1     FNew(nn,NPoints),RMat(nnn),RSide(nn),RSol(nn),
     2     GG(7,7),GGi(7,7),FG(7,7),FGi(7,7),Tr(7,7),Trt(7,7),Tri(7,7),
     3     Trit(7,7),
     3     MPom(7,7),RPom(7,7),xp(7),xo(7)
      open(40,file='Corr-non-ortho.mat')
      do i=1,nn
        do j=1,i
          read(40,100) ii,jj,MatCov(i,j,1),Mat(i,j,1)
        enddo
      enddo
      do i=1,nn
        do j=1,i
          MatCov(j,i,1)=MatCov(i,j,1)
          pom=Mat(i,j,1)*10.e8
!          pom=Mat(i,j,1)
          Mat(j,i,1)=pom
          Mat(i,j,1)=pom
        enddo
      enddo
      close(40)
      open(40,file='Corr-Legendre.mat')
      do i=1,nn
        do j=1,i
          read(40,100) ii,jj,MatCov(i,j,2),Mat(i,j,2)
        enddo
      enddo
      do i=1,nn
        do j=1,i
          MatCov(j,i,2)=MatCov(i,j,2)
          pom=Mat(i,j,2)*10.e8
!          pom=Mat(i,j,2)
          Mat(j,i,2)=pom
          Mat(i,j,2)=pom
        enddo
      enddo
      close(40)
      open(40,file='Srovnani.txt')
      write(40,'(''Matice kovariaci non-ortho:'')')
      write(40,101)((MatCov(i,j,1),j=1,nn),i=1,nn)
      write(40,101)((Mat(i,j,1)/(sqrt(Mat(i,i,1))*sqrt(Mat(j,j,1))),
     1              j=1,nn),i=1,nn)
      write(40,'(''Matice inverzni non-ortho:'')')
      write(40,102)((Mat(i,j,1),j=1,nn),i=1,nn)
      write(40,'(''Matice kovariaci Legendre:'')')
      write(40,101)((MatCov(i,j,2),j=1,nn),i=1,nn)
      write(40,101)((Mat(i,j,2)/(sqrt(Mat(i,i,2))*sqrt(Mat(j,j,2))),
     1              j=1,nn),i=1,nn)
      write(40,'(''Matice inverzni Legendre:'')')
      write(40,102)((Mat(i,j,2),j=1,nn),i=1,nn)
      x40=0.2
      delta=0.6
      dx4=delta/float(NPoints-1)
      x4=x40-delta/2.
      do m=1,NPoints
        pom=x4-x40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4+dx4
          cycle
        endif
        do k=1,2*3+1
          km=k/2
          if(k-1.eq.0) then
            FOld(k,m)=1.
          else if(mod(k-1,2).eq.1) then
            FOld(k,m)=sin(Pi2*km*x4)
          else
            FOld(k,m)=cos(Pi2*km*x4)
          endif
        enddo
!        write(6,'(i5,f10.6)') m,pom
        call GetFPol(pom,FNew(1,m),2*3+1,2)
        x4=x4+dx4
      enddo
      GG=0.
      FG=0.
      do m=1,NPoints
        do i=1,nn
          do j=1,nn
            GG(i,j)=GG(i,j)+FNew(i,m)*FNew(j,m)*dx4/Delta
            FG(i,j)=FG(i,j)+FOld(j,m)*FNew(i,m)*dx4/Delta
          enddo
        enddo
      enddo
      write(40,'(''GG:'')')
      write(40,101) GG
      call MatInv(GG,GGi,det,7)
      call Multm(GGi,FG,Tr,7,7,7)
      xp(1)=0.249808
      xp(2)=0.050000
      xp(3)=-0.025000
      xp(4)=0.025000
      xp(5)=0.050000
      xp(6)=0.035000
      xp(7)=0.025000
      call MultM(Tr,xp,xo,7,7,1)
      write(40,'(''Tr-x'')')
      write(40,101) xo
      call TrMat(Tr,Trt,7,7)
      call MatInv(Tr,Tri,det,7)
      call MatInv(Trt,Trit,det,7)
      call MultM(Trit,Mat(1,1,1),MPom,7,7,7)
      call MultM(MPom,Tri,RPom,7,7,7)
      write(40,'(''Matice inverzni prevod:'')')
      write(40,102)((RPom(i,j),j=1,nn),i=1,nn)
      write(40,'(''Matice korelacni prevod:'')')
      write(40,101)((RPom(i,j)/sqrt(RPom(i,i)*RPom(j,j)),j=1,nn),i=1,nn)
      close(40)
100   format(2i4,f10.6,e15.6)
101   format(7f10.6)
102   format(7e15.6)
      end
      subroutine TestSSGSymbol
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest
      real QuSymm(3,3)
      ich=0
      id=NextQuestId()
      xqd=300.
      il=2
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,-1)
      il=1
      tpom=5.
      xpom=60.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,'Symbol:','L',dpom,EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      nEdwSymbol=EdwLastMade
      il=il+1
      dpom=60.
      tpom=xqd*.5-dpom-5.
      Veta='Test it'
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtTest=ButtonLastMade
      tpom=tpom+dpom+10.
      Veta='Quit'
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtQuit=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTest) then
        Grupa(KPhase)=EdwStringQuest(nEdwSymbol)
        call EM50GenSym(.true.,.true.,QuSymm,ich)
        write(Cislo,'(i5)') ich
        call FeWinMessage(Grupa(KPhase),Cislo)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1  then
        call FeQuestRemove(id)
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      return
      end
      subroutine PrevodFrSuper
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Cesta,Veta
      integer ih(4)
      logical EqIV,MatRealEqUnitMat
      Cesta='D:\Structures\Jana2006\Work\Margarida\CeRuSn\'
      open(43,file=Cesta(:idel(Cesta))//'super.fr')
      open(44,file=Cesta(:idel(Cesta))//'super_4d.fr')
1100  read(43,FormA,end=1800) Veta
      if(Veta(1:1).eq.'#'.or.Veta.eq.' ') go to 1100
      read(Veta,'(2i5,f8.3)') ih(1:2),h3
      read(Veta(19:),*) ri,si
      ih(3)=nint(h3)
      diff=h3-float(ih(3))
      if(abs(diff-.35).lt..001) then
        ih(4)=2
      else if(abs(diff+.35).lt..001) then
        ih(4)=-2
      else
        call FeWinMessage('Problem',' ')
      endif
      write(44,'(4i5,2f10.6)') ih(1:4),ri,si
      go to 1100
1800  close(43)
      close(44)
      return
      end
      subroutine CrlMakeDCRED
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      character*256 Veta
      integer ihitw(6,18)
      integer :: MNPUse(3,13) = reshape
     1    ((/0,0,0,1,0,0,0,1,0,-1,-1,0,
     2             2,0,0,0,2,0,-2,-2,0,
     3             3,0,0,0,3,0,-3,-3,0,
     4             4,0,0,0,4,0,-4,-4,0/),shape(MNPUse))
      logical Ranover,EqIV,EqIVM
      UseEFormat91=.false.
      if(UseEFormat91) then
        Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
      else
        Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
      endif
      write(Format91(2:2),'(i1)') NDim(KPhase)
      call GenerQuest(-1,0)
      call iom50(0,0,fln(:ifln)//'.m50')
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      if(TwMax.lt.TwDiff) TwMax=TwDiff
      Veta=fln(:ifln)//'.l91'
      call MoveFile(DCREDFile,Veta)
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      call OpenFile(lno,DCREDFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1500  read(ln,100,end=9999) ih(1:NDim(KPhase))
      if(ih(1).gt.900) go to 9999
      do i=1,13
        if(EqIV (ih(4:NDim(KPhase)),MNPUse(1,i),NDimI(KPhase)).or.
     1     EqIVM(ih(4:NDim(KPhase)),MNPUse(1,i),NDimI(KPhase)))
     2    go to 2000
      enddo
      go to 1500
2000  ihitw=0
      ihitw(1,1:NTwin)=999
      call settw(ih,ihitw,1,Ranover)
      do i=1,NTwin
        if(ihitw(1,i).lt.900) then
          write(lno,101) ih(1:NDim(KPhase))
          go to 1500
        endif
      enddo
      go to 1500
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      return
100   format(6i4)
101   format(6(i10,1x))
      end
      subroutine TestN
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta
      real ubt(3,3),h(3)
      KRefBlock=1
      ub(1,1,KRefBlock)=.058615
      ub(1,2,KRefBlock)=-.073445
      ub(1,3,KRefBlock)=.0
      ub(2,1,KRefBlock)=.118648
      ub(2,2,KRefBlock)=.110086
      ub(2,3,KRefBlock)=0.
      ub(3,1,KRefBlock)=0.
      ub(3,2,KRefBlock)=0.
      ub(3,3,KRefBlock)=0.105108
      ub(1,1,KRefBlock)=.125198
      ub(1,2,KRefBlock)=.062599
      ub(1,3,KRefBlock)=.034723
      ub(2,1,KRefBlock)=-.043787
      ub(2,2,KRefBlock)=-.021893
      ub(2,3,KRefBlock)=.099283
      ub(3,1,KRefBlock)=0.
      ub(3,2,KRefBlock)=-.114865
      ub(3,3,KRefBlock)=.0
!      call CopyMat(UB(1,1,KRefBlock),ubt,3)
!      call TrMat(ubt,UB(1,1,KRefBlock),3,3)
      call DRCellFromUB
      write(Veta,'(6f10.3)') CellRefBlock(1:6,KRefBlock)
      call FeWinMessage(Veta,' ')
      rcp(1,1,KPhase)=8.7522
      rcp(2,1,KPhase)=8.7522
      rcp(3,1,KPhase)=9.5075
      rcp(4,1,KPhase)=0.
      rcp(5,1,KPhase)=0.
      rcp(6,1,KPhase)=-.5
!      rcp(1,1,KPhase)=CellRefBlock(1,KRefBlock)
!      rcp(2,1,KPhase)=CellRefBlock(2,KRefBlock)
!      rcp(3,1,KPhase)=CellRefBlock(3,KRefBlock)
!      rcp(4,1,KPhase)=cos(CellRefBlock(4,KRefBlock)*ToRad)
!!      rcp(5,1,KPhase)=cos(61.*ToRad)
!      rcp(5,1,KPhase)=cos(CellRefBlock(5,KRefBlock)*ToRad)
!      rcp(6,1,KPhase)=cos(CellRefBlock(6,KRefBlock)*ToRad)
      KPhase=1
      call recip(rcp(1,1,KPhase),rcp(1,1,KPhase),CellVol(1,KPhase))
      i=1
      do j=1,3
        prcp(j,i,KPhase)=.25*rcp(j,i,KPhase)**2
      enddo
      prcp(4,i,KPhase)=.25*rcp(1,i,KPhase)*rcp(2,i,KPhase)*
     1                                     rcp(6,i,KPhase)
      prcp(5,i,KPhase)=.25*rcp(1,i,KPhase)*rcp(3,i,KPhase)*
     1                                     rcp(5,i,KPhase)
      prcp(6,i,KPhase)=.25*rcp(2,i,KPhase)*rcp(3,i,KPhase)*
     1                                     rcp(4,i,KPhase)
      ih(1)=2
      ih(2)=2
      ih(3)=0
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      write(Veta,'(3i4,f10.6,f10.3)')
     1  ih(1:3),sinthl,2.*asin(sinthl*2.44)/ToRad
      call FeWinMessage(Veta,' ')
      ih(1)=2
      ih(2)=-4
      ih(3)=0
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      write(Veta,'(3i4,f10.6,f10.3)')
     1  ih(1:3),sinthl,2.*asin(sinthl*2.44)/ToRad
      call FeWinMessage(Veta,' ')
      ih(1)=1
      ih(2)=0
      ih(3)=0
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      write(Veta,'(3i4,f10.6,f10.3)')
     1  ih(1:3),sinthl,2.*asin(sinthl*2.44)/ToRad
      call FeWinMessage(Veta,' ')
      ih(1)=2
      ih(2)=0
      ih(3)=0
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      write(Veta,'(3i4,f10.6,f10.3)')
     1  ih(1:3),sinthl,2.*asin(sinthl*2.44)/ToRad
      call FeWinMessage(Veta,' ')
      ih(1)=3
      ih(2)=0
      ih(3)=0
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      write(Veta,'(3i4,f10.6,f10.3)')
     1  ih(1:3),sinthl,2.*asin(sinthl*2.44)/ToRad
      call FeWinMessage(Veta,' ')
      return
      end
      subroutine EM50GenSym(FirstTime,AskForDelta,QuSymm,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension ieps(5),tau(5),ie(5),rp(36),trm(36),ipor(5),xref(3),
     1          QuOld(3,3),QuiOld(3,3),QuSymm(3,3),MonoclinicA(3)
      character*256 Veta,StSymm(3)
      character*80 t80
      character*60 itxt,itxti,GrupaPom,GrupaHledej,GrupaRidka
      character*11 iqs
      character*8 GrupaI
      character*6 smbtau,SmbMagCentr
      character*5 smbq(3)
      character*2 nty
      logical StdSmb,SearchForPrimitive,FirstTime,AskForDelta,
     1        EqIgCase,EqRV,ItIsFd3c,EqRV0,Choice1,CrwLogicQuest,lpom,
     2        WizardModeIn,UplnySymbol
      integer EM50IrrType,CrlCentroSymm,UseTabsIn
      data iqs/'PABCLMNUVWR'/,smbtau/'01stqh'/,
     1     smbq/'alfa','beta','gamma'/
      call Zhusti(Grupa(KPhase))
      id=idel(Grupa(KPhase))
      i1=index(Grupa(KPhase),'(')
      UplnySymbol=i1.gt.0
      if(NDimI(KPhase).gt.0.and.UplnySymbol) then
        call CopyVek(Qu(1,1,1,KPhase),QuOld,3*NDimI(KPhase))
        call CopyVek(Qui(1,1,KPhase),QuiOld,3*NDimI(KPhase))
        i1=index(Grupa(KPhase),'(')
        if(i1.gt.0.and.i1.lt.id) then
          i2=index(Grupa(KPhase)(i1+1:),')')+i1
        else
          i2=0
        endif
        i3=idel(Grupa(KPhase))
        if(i1.le.0.or.i2.le.i1) go to 8150
        GrupaPom=Grupa(KPhase)(1:i1-1)
        t80=Grupa(KPhase)(i1+1:i2-1)
        call mala(t80)
        do i=1,3
          if(idel(t80).le.0) go to 8160
          k=1
1010      if(t80(k:k).eq.smbq(i)(k:k)) then
            k=k+1
            go to 1010
          endif
          k=k-1
          if(k.eq.0) then
            j=index(t80,'/')
            if(j.le.0.or.j.gt.3.or.(j.eq.3.and.t80(1:1).ne.'-')) then
              k=1
            else
              k=j+1
            endif
            quir(i,1,KPhase)=fract(t80(1:k),ich)
            qui(i,1,KPhase)=0.
            if(ich.ne.0) go to 8210
          else
            quir(i,1,KPhase)=0.
            qui(i,1,KPhase)=sqrt(float(i)*.1)
          endif
          t80=t80(k+1:)
        enddo
        n=0
        ic=0
        in=0
        do i=1,3
          if(abs(quir(i,1,KPhase)).lt..0001) then
            n=n+1
            if(in.eq.0) in=i
          else if(ic.eq.0) then
            ic=i
          endif
        enddo
        nq=0
        if(n.eq.3) then
          nq=1
        else if(n.eq.2) then
          if(abs(quir(ic,1,KPhase)-.5).lt..0001) then
            nq=ic+1
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001) then
            nq=ic+4
          endif
        else if(n.eq.1) then
          if(abs(quir(1,1,KPhase)-.333333).lt..0001.and.
     1       abs(quir(2,1,KPhase)-.333333).lt..0001) then
            nq=11
          else if(abs(quir(ic,1,KPhase)-.5).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-.5).lt..0001) then
            nq=7+in
          else if(abs(quir(ic,1,KPhase)-1.).lt..0001.and.
     1            abs(quir(6-ic-in,1,KPhase)-1.).lt..0001) then
            nq=11+in
          endif
        endif
        if(nq.le.0) go to 8160
        call AddVek(Qui(1,1,KPhase),Quir(1,1,KPhase),Qu(1,1,1,KPhase),3)
        iqv=EM50IrrType(qui(1,1,KPhase))
      else
        do i=1,NDimI(KPhase)
          do j=1,3
            qui(j,i,KPhase)=qu(j,i,1,KPhase)
            quir(j,i,KPhase)=0.
          enddo
        enddo
        GrupaPom=Grupa(KPhase)
        iqv=0
      endif
1100  SmbMagCentr=' '
      if(GrupaPom(1:1).eq.'-') then
        call Uprat(GrupaPom(2:))
      else
        if(GrupaPom(2:2).eq.'[') then
          i=index(GrupaPom,']')
          if(i.gt.0) then
            SmbMagCentr=GrupaPom(2:i)
            GrupaPom=GrupaPom(1:1)//GrupaPom(i+1:)
            TauMagCenter=.5
          endif
        endif
        call Uprat(GrupaPom)
      endif
      i=index(GrupaPom,'.')
      idp=idel(GrupaPom)
      MagInv=0
      if(i.gt.0) then
        if(GrupaPom(i+1:i+2).eq.'1''') then
          i1p=index(GrupaPom,'[')
          i2p=index(GrupaPom,']')
          SmbMagCentr=' '
          if((i1p.eq.0.and.i2p.gt.0).or.(i1p.gt.0.and.i2p-i1p.ne.2))
     1      then
            go to 8190
          else if(i1p.gt.0) then
            SmbMagCentr=GrupaPom(i1p:i2p)
            i3=idel(Grupa(KPhase))
            if(EqIgCase(Grupa(KPhase)(i3:i3),'s')) then
              TauMagCenter=.5
            else if(EqIgCase(Grupa(KPhase)(i3:i3),'0')) then
              TauMagCenter=0.
            else
              go to 8105
            endif
            i3=idl-1
          else
            MagInv=1
          endif
          GrupaPom(i:)=' '
        else
          go to 8190
        endif
      else if(idp-2.gt.0) then
        if(NDimI(KPhase).eq.0) then
          MagInv=0
        else
          if(GrupaPom(idp-2:idp).eq. '.1'''.and.
     1       GrupaPom(idp-3:idp).ne.'.-1''') then
            GrupaPom(idp-1:)=' '
            MagInv=1
          endif
        endif
      endif
      i=index(GrupaPom,';')
      j=index(GrupaPom(2:),'x')
      k=index(GrupaPom,'y')
      l=index(GrupaPom,'z')
      if(i.le.0.and.j.le.0.and.k.le.0.and.l.le.0.and.
     1   grupa(KPhase)(1:1).ne.'-') then
        StdSmb=.true.
        Lattice(KPhase)=GrupaPom(1:1)
        if(Lattice(KPhase).eq.'X') GrupaPom(1:1)='P'
        SearchForPrimitive=.false.
        j=0
        GrupaHledej=' '
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).ne.'''') then
            j=j+1
            GrupaHledej(j:j)=GrupaPom(i:i)
          endif
        enddo
        if(j.ne.idel(GrupaPom).and.MagneticType(KPhase).le.0) go to 8180
1200    imd=0
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1          'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'source/data/spgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 8700
        read(ln,FormA256) Veta
        if(Veta(1:1).ne.'#') rewind ln
        if(FirstTime) NInfo=0
2000    read(ln,FormA,err=8610,end=2010) Veta
        read(Veta,FormSG,err=8610) igi,ipgi,idli,GrupaI,itxti,
     1                             GrupaRidka,GrupaRidka,GrupaRidka
        if(imd.ne.0) imd=imd+1
        if((GrupaI.eq.'P2'.and.imd.eq.0).or.
     1      GrupaI.eq.'Pmm2') imd=1
        if(GrupaHledej.eq.GrupaI.or.
     1     (Lattice(KPhase).eq.'X'.and.GrupaHledej(2:).eq.GrupaI(2:)))
     2    then
          if(ipgi.ge.3.and.ipgi.le.5) then
            if(FirstTime.and.NInfo.lt.3) then
              NInfo=NInfo+1
              StSymm(NInfo)=Veta
              Veta=GrupaRidka
              if(EqIgCase(Lattice(KPhase),'X')) Veta(1:1)='X'
              write(TextInfo(NInfo),'(a,i3,2a)') Tabulator,ipgi,
     1                                 Tabulator,Veta(:idel(Veta))
              MonoclinicA(NInfo)=mod(imd-1,3)+1
            endif
            t80=GrupaRidka
            k=0
            call kus(t80,k,Cislo)
            GrupaRidka=Cislo
            do i=1,4
              call kus(t80,k,Cislo)
              if(Cislo.ne.'1')
     1          GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     2                     Cislo(:idel(Cislo))
            enddo
            if(.not.FirstTime.and.mod(imd-1,3)+1.eq.Monoclinic(KPhase))
     1        then
              go to 2100
            else
              go to 2000
            endif
          endif
          go to 2100
        endif
        go to 2000
2010    call CloseIfOpened(ln)
        if(NInfo.gt.1.and.FirstTime) then
          UseTabsIn=UseTabs
          UseTabs=NextTabs()
          WizardModeIn=WizardMode
          WizardMode=.false.
          call FeTabsAdd( 50.,UseTabs,IdRightTab,' ')
          call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
          id=NextQuestId()
          xdq=300.
          il=Ninfo+1
          Veta='Select relevant space group'
          call FeQuestCreate(id,-1.,-1.,xdq,il,Veta,0,LightGray,-1,0)
          il=1
          tpom=30.
          call FeQuestLblMake(id,tpom,il,'Space group number','L','N')
          tpom=150.
          call FeQuestLblMake(id,tpom,il,'Space group symbol','L','N')
          xpom=5.
          tpom=xpom+10.+CrwgXd
          n=0
          do i=1,NInfo
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                          TextInfo(i),'L',CrwgXd,CrwgYd,0,1)
            if(MonoclinicA(i).eq.Monoclinic(KPhase)) then
              lpom=.true.
              n=i
            else
              lpom=.false.
            endif
            call FeQuestCrwOpen(CrwLastMade,lpom)
            if(i.eq.1) nCrwFirst=CrwLastMade
          enddo
          if(n.eq.0) call FeQuestCrwOn(nCrwFirst)
2050      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2050
          endif
          nCrw=nCrwFirst
          do i=1,NInfo
            if(CrwLogicQuest(nCrw)) then
              read(StSymm(i),FormSG,err=8610)
     1          igi,ipgi,idli,GrupaI,itxti,GrupaRidka,GrupaRidka,
     2          GrupaRidka
              t80=GrupaRidka
              k=0
              call kus(t80,k,Cislo)
              GrupaRidka=Cislo
              do j=1,3
                call kus(t80,k,Cislo)
                if(Cislo.ne.'1') then
                  GrupaRidka=GrupaRidka(:idel(GrupaRidka)+1)//
     1                       Cislo(:idel(Cislo))
                  Monoclinic(KPhase)=j
                  exit
                endif
              enddo
            endif
            nCrw=nCrw+1
          enddo
          call FeQuestRemove(id)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsIn
          WizardMode=WizardModeIn
          go to 2100
        endif
        if(SearchForPrimitive.or.GrupaHledej(1:1).eq.'P') then
          go to 8170
        else
          SearchForPrimitive=.true.
          GrupaHledej(1:1)='P'
          go to 1200
        endif
2100    call CloseIfOpened(ln)
        k=index(itxti,'#')
        if(k.gt.0) then
          itxt=itxti(1:k-1)
          Veta=itxti(k+1:idel(itxti))
          do j=1,idel(Veta)
            if(Veta(j:j).eq.',') Veta(j:j)=' '
          enddo
          k=0
          call StToReal(Veta,k,ShiftSg,3,.false.,ich)
          call RealVectorToOpposite(ShiftSg,ShiftSg,3)
          call SetRealArrayTo(ShiftSg(4),NDimI(KPhase),0.)
        else
          itxt=itxti
          call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
        endif
        ipg=ipgi
        if(Lattice(KPhase).eq.'X'.or.SearchForPrimitive) then
          ngrupa(KPhase)=0
        else
          ngrupa(KPhase)=igi
        endif
        idl=idli
        if(MagInv.gt.0) idl=idl+1
        Poradi=.true.
        if(ipg.le.2) then
          CrSystem(KPhase)=CrSystemTriclinic
        else if(ipg.ge.3.and.ipg.le.5) then
          imd=mod(imd-1,3)+1
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.
     1       Monoclinic(KPhase).ne.iabs(iqv)) go to 8210
          CrSystem(KPhase)=CrSystemMonoclinic+Monoclinic(KPhase)*10
        else if(ipg.ge.6.and.ipg.le.8) then
          Poradi=.false.
          imd=mod(imd+1,3)+1
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.lt.1)
     1      go to 8210
          CrSystem(KPhase)=CrSystemOrthorhombic
        else if(ipg.ge.9.and.ipg.le.15) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.ne.3) go to 8210
          CrSystem(KPhase)=CrSystemTetragonal
        else if(ipg.ge.16.and.ipg.le.27) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol.and.iqv.ne.3) go to 8210
          if(ipg.le.20) then
            CrSystem(KPhase)=CrSystemTrigonal
          else
            CrSystem(KPhase)=CrSystemHexagonal
          endif
        else if(ipg.ge.28.and.ipg.le.32) then
          CrSystem(KPhase)=CrSystemCubic
        endif
        if(mod(CrSystem(KPhase),10).ne.2) Monoclinic(KPhase)=0
        GrupaPom(1:1)=Lattice(KPhase)
        GrupaHledej(1:1)=Lattice(KPhase)
        if(itxt(1:1).eq.'-') then
          itxt(2:2)=Lattice(KPhase)
        else
          itxt(1:1)=Lattice(KPhase)
        endif
      else
        StdSmb=.false.
        itxt=GrupaPom
        idl=1
        do i=1,idel(GrupaPom)
          if(GrupaPom(i:i).eq.';') idl=idl+1
        enddo
        call SetRealArrayTo(ShiftSg,NDim(KPhase),0.)
      endif
      if(NDimI(KPhase).eq.1.and.UplnySymbol) then
        Veta=Grupa(KPhase)(i2+1:i3)
        call mala(Veta)
        i=0
        n=0
        id=idel(Veta)
        Znak=1.
2200    i=i+1
        if(i.gt.id) go to 2300
        if(Veta(i:i).eq.'-') then
          Znak=-1.
          go to 2200
          i=i+1
          if(Veta(i:i).eq.'1') then
            n=n+1
            ieps(n)=-1
            tau(n)=0
            go to 2200
          else
            go to 8100
          endif
        else
          j=index(smbtau,Veta(i:i))
          if(j.le.0) go to 8100
          if(j.eq.1) then
            n=n+1
            ieps(n)=0
            tau(n)=0.
            go to 2200
          else if(j.eq.2) then
            go to 8100
          endif
          if(j.ne.6) j=j-1
          n=n+1
          ieps(n)=1
          if(j.ne.1) then
            if(Znak.gt.0.) then
              tau(n)= 1./float(j)
            else
              tau(n)=-1./float(j)
            endif
          else
            tau(n)=0.
          endif
          Znak=1.
          go to 2200
        endif
2300    if(n.ne.idl) then
          if(n.gt.idl) then
            Veta='"'//Veta(1:id)//'" too long.'
          else
            do i=n+1,idl
              ieps(i)=0
              tau(i)=0.
            enddo
            go to 2400
          endif
          id=idel(Veta)
          go to 8220
        endif
2400    if(StdSmb) then
          if(MagInv.gt.0) idl=idl-1
          call EM50SetEps(ie,ipor,ieps,tau,idl,ipg,ngrupa(KPhase),imd,
     1                    itxt,ich)
          if(ich.ne.0) go to 8162
          if(MagInv.gt.0) then
            idl=idl+1
            ie(idl)=1
          endif
          do i=1,idl
            if(ie(i).ne.ieps(i)) then
              if(ieps(i).eq.0) ieps(i)=ie(i)
            endif
          enddo
        endif
      endif
      call EM50GetMagFlags(GrupaPom,GrupaRidka,MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8190
      call EM50OrderMagFlags(ipg,NGrupa(KPhase),MagFlag,NMagFlag,ich)
      if(ich.ne.0) go to 8200
      call EM50GenCentr(itxt,nc,FirstTime,*8110,*9900)
      call EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,UplnySymbol,ich)
      if(ich.ne.0) go to 9900
      do i=1,NLattVec(KPhase)
        do 2500j=1,NSymm(KPhase)
          call MultM(rm6(1,j,1,KPhase),vt6(1,i,1,KPhase),rp,
     1               NDim(KPhase),NDim(KPhase),1)
          call od0do1(rp,rp,NDim(KPhase))
          do k=1,NLattVec(KPhase)
            if(EqRV(vt6(1,k,1,KPhase),rp,NDim(KPhase),.001)) go to 2500
          enddo
          go to 8230
2500    continue
      enddo
      call EM50OriginShift(ShiftSg,0)
      i=CrlCentroSymm()
      call SetRealArrayTo(xref,3,0.)
      if(EqIgCase(Grupa(KPhase),'Fddd')) then
        call SetRealArrayTo(xref,3,.25)
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          do i=1,NSymm(KPhase)
            ipul=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.5).lt..001) ipul=ipul+1
            enddo
            if(ipul.eq.2) call SetRealArrayTo(s6(1,i,1,KPhase),3,0.)
          enddo
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          do 5020i=1,NSymm(KPhase)
            call MatInv(rm(1,i,1,KPhase),trm,Det,3)
            if(Det.gt.0.) then
              pom=.75
            else
              pom=.25
            endif
            do j=1,NLattVec(KPhase)
              call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
              call od0do1(rp,rp,3)
              nnul=0
              npom=0
              do k=1,3
                if(abs(rp(k)).lt..001) then
                  nnul=nnul+1
                else if(abs(rp(k)-pom).lt..001) then
                  npom=npom+1
                endif
              enddo
              if(nnul+npom.eq.3.and.(npom.eq.0.or.npom.eq.2)) then
                call CopyVek(rp,s6(1,i,1,KPhase),3)
                go to 5020
              endif
            enddo
5020      continue
        endif
        go to 5300

      else if(EqIgCase(Grupa(KPhase),'I41md').or.
     1        EqIgCase(Grupa(KPhase),'I41cd')) then
        k=1
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs( s6(1,i,1,KPhase)).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs( s6(1,i,1,KPhase)).lt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'I-42d')) then
        do 5040i=1,NSymm(KPhase)
          if(abs(s6(2,i,1,KPhase)).lt..001) go to 5040
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5040    continue
      else if(EqIgCase(Grupa(KPhase),'I41/a').or.
     1        EqIgCase(Grupa(KPhase),'I41/amd').or.
     2        EqIgCase(Grupa(KPhase),'I41/acd')) then
        xref(2)=.5
        xref(3)=.25
        k=2
        l=NDim(KPhase)+2
        if(EqRV(s6(1,i,1,KPhase),xref,3,.001)) then
          pomp1=.5
          pomm1=0.
          pomp2=0.
          pomm2=.5
        else if(EqRV0(s6(1,i,1,KPhase),3,.001)) then
          if(EqIgCase(Grupa(KPhase),'I41/a')) then
            pomp1=.25
            pomm1=.75
          else
            pomp1=.75
            pomm1=.25
          endif
          pomp2=0.
          pomm2=0.
        else
          go to 5300
        endif
        do i=1,NSymm(KPhase)
          if((abs(RM6(k,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp1).gt..001).or.
     2       (abs(RM6(k,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm1).gt..001).or.
     4       (abs(RM6(l,i,1,KPhase)-1.).lt..001.and.
     1        abs(s6(k,i,1,KPhase)-pomp2).gt..001).or.
     2       (abs(RM6(l,i,1,KPhase)+1.).lt..001.and.
     3        abs(s6(k,i,1,KPhase)-pomm2).gt..001)) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3')) then
        call SetRealArrayTo(xref,3,.25)
        Choice1=EqRV(s6(1,i,1,KPhase),xref,3,.001)
        do 5050i=1,NSymm(KPhase)
          call MatInv(rm(1,i,1,KPhase),trm,Det,3)
          if(Det.gt.0.) then
            if(Choice1) then
              pom=0.
              nsum=3
            else
              pom=.25
              nsum=2
            endif
          else
            if(Choice1) then
              pom=.25
              nsum=3
            else
              pom=.75
              nsum=2
            endif
          endif
          do j=1,NLattVec(KPhase)
            call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),rp,3)
            call od0do1(rp,rp,3)
            npom=0
            do k=1,3
              if(abs(rp(k)-pom).lt..001) npom=npom+1
            enddo
            if(npom.eq.nsum) then
              call CopyVek(rp,s6(1,i,1,KPhase),3)
              go to 5050
            endif
          enddo
5050    continue
      else if(EqIgCase(Grupa(KPhase),'I-43d')) then
        do i=1,NSymm(KPhase)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.1.and.inul.eq.2).or.ipul.eq.3.or.
     1       (i34 .eq.1.and.i14 .eq.2).or.i34.eq.3) then
            call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                  s6(1,i,1,KPhase),3)
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          endif
        enddo
      else if(EqIgCase(Grupa(KPhase),'Fd-3m').or.
     1        EqIgCase(Grupa(KPhase),'Fd-3c')) then
        Choice1=.not.EqRV(s6(1,i,1,KPhase),xref,3,.001)
        if(Choice1) then
          ItIsFd3c=EqIgCase(Grupa(KPhase),'Fd-3c')
          do i=1,NSymm(KPhase)
            if(ItIsFd3c) then
              call MatInv(RM(1,i,1,KPhase),rp,det,3)
            else
              det=1.
            endif
            i34=0
            i14=0
            do j=1,3
              if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
              if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
            enddo
            if(i34.gt.0.or.i14.gt.0) then
              Znak=-1.
            else
              Znak= 1.
            endif
            KteryZnak=0
            KdeZnak=0
            KdeSoused=0
            do j=1,3
              do k=1,3
                l=(k-1)*3+j
                if(abs(RM(l,i,1,KPhase)-Znak).lt..001) then
                  if(KdeZnak.le.0) then
                    KdeZnak=j
                    KteryZnak=k
                  else
                    KdeZnak=-1
                    go to 5090
                  endif
                endif
              enddo
            enddo
            k=mod(KteryZnak+1,3)+1
            k=(k-1)*3+1
            do j=1,3
              if(abs(RM(k,i,1,KPhase)).gt..001) then
                KdeSoused=j
                go to 5090
              endif
              k=k+1
            enddo
5090        if(Znak.lt.0.) then
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.75
              else
                pom=.25
              endif
            else
              if(ItIsFd3c.and.det.lt.0.) then
                pom=.5
              else
                pom=0.
              endif
            endif
            call SetRealArrayTo(s6(1,i,1,KPhase),3,pom)
            if(KdeZnak.gt.0) then
              s6(KdeZnak,i,1,KPhase)=s6(KdeZnak,i,1,KPhase)+.5
              s6(KdeSoused,i,1,KPhase)=s6(KdeSoused,i,1,KPhase)+.5
            endif
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
          enddo
        endif
      else if(EqIgCase(Grupa(KPhase),'Ia-3d')) then
        do 5120i=1,NSymm(KPhase)
          call MatInv(RM(1,i,1,KPhase),rp,det,3)
          ipul=0
          inul=0
          i34=0
          i14=0
          do j=1,3
            if(abs(s6(j,i,1,KPhase)-.5 ).lt..001) ipul=ipul+1
            if(abs(s6(j,i,1,KPhase)    ).lt..001) inul=inul+1
            if(abs(s6(j,i,1,KPhase)-.25).lt..001) i14=i14+1
            if(abs(s6(j,i,1,KPhase)-.75).lt..001) i34=i34+1
          enddo
          if((ipul.eq.2.and.inul.eq.1).or.inul.eq.3) go to 5120
          if(det.gt.0.) then
            if((i34.eq.1.and.i14.eq.2).or.i34.eq.3) go to 5120
          else
            if((i34.eq.2.and.i14.eq.1).or.i34.eq.0) go to 5120
          endif
          call AddVek(s6(1,i,1,KPhase),VT6(1,2,1,KPhase),
     1                s6(1,i,1,KPhase),3)
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),3)
5120    continue
      endif
5300  call EM50OriginShift(ShSg(1,KPhase),0)
      do i=1,NSymm(KPhase)
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        do j=1,NDim(KPhase)
          if(index(symmc(j,i,1,KPhase),'?').gt.0) then
            NSymm(KPhase)=0
            go to 8140
          endif
        enddo
      enddo
      if(idel(SmbMagCentr).eq.3.and.SmbMagCentr(1:1).eq.'['.and.
     1                              SmbMagCentr(3:3).eq.']' ) then
        if(SmbMagCentr(2:2).eq.'I') then
          rp=.5
          if(CrSystem(KPhase).eq.CrSystemTrigonal) then
            rp(1)=0.
            rp(2)=0.
          endif
          go to 5350
        else if(SmbMagCentr(2:2).eq.'c') then
          rp=0.
          rp(3)=.5
          go to 5350
        else
          i=ichar(SmbMagCentr(2:2))-ichar('a')+1
          rp=0.
          if(i.ge.1.and.i.le.3) then
            rp(i)=.5
            go to 5350
          endif
          i=ichar(SmbMagCentr(2:2))-ichar('A')+1
          rp=.5
          if(i.ge.1.and.i.le.3) then
            rp(i)=0.
            go to 5350
          endif
        endif
        go to 5400
5350    nss=NSymm(KPhase)
        call ReallocSymm(NDim(KPhase),2*nss,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        do i=1,NSymm(KPhase)
          nss=nss+1
          call CopyVek(rm6(1,i,1,KPhase),rm6(1,nss,1,KPhase),
     1                 NDimQ(KPhase))
          call CopyVek(rm (1,i,1,KPhase),rm (1,nss,1,KPhase),9)
          call AddVek(s6(1,i,1,KPhase),rp,s6(1,nss,1,KPhase),
     1                 NDim(KPhase))
          if(NDimI(KPhase).gt.0)
     1      s6(4,nss,1,KPhase)=s6(4,nss,1,KPhase)+TauMagCenter
          call NormCentr(s6(1,nss,1,KPhase))
          call CodeSymm(rm6(1,nss,1,KPhase),s6(1,nss,1,KPhase),
     1                  symmc(1,nss,1,KPhase),0)
          ZMag(nss,1,KPhase)=-Zmag(i,1,KPhase)
        enddo
        NSymm(KPhase)=nss
      endif
5400  ich=0
      call GetQiQr(qu(1,1,1,KPhase),qui(1,1,KPhase),
     1             quir(1,1,KPhase),NDimI(KPhase),1)
      go to 9999
8100  write(t80,'(''Incorrect additional symbol of the '',i1,a2,
     1            '' generator.'')') n+1,nty(n+1)
      go to 8500
8105  write(t80,'(''Incorrect additional symbol for the time inverion'',
     1            '' operator.'')')
      go to 8500
8110  t80='The cell centring symbol is not correct.'
      go to 8500
8120  write(t80,'(''Opposite sign of additional symbol of the '',i1,a2,
     1            '' generator.'')') ipor(i),nty(ipor(i))
      go to 8500
8130  t80='The additional symbols are not consistent.'
      go to 8500
8140  t80='Origin shift is not acceptable.'
      go to 8500
8150  t80='Neither colon nor parenthesis section found.'
      go to 8500
8160  t80='The symbol for modulation vector is incorrrect.'
      go to 8500
8162  t80='The symbol for modulation vector isn''t consitent with the'//
     1    ' point group .'
      go to 8500
8170  t80='The symbol "'//Grupa(KPhase)(:idel(Grupa(KPhase)))//
     1    '" wasn''t found on the list.'
      go to 8500
8180  t80='The symbol contains time inversion flags for non-magnetic '//
     1    'structure.'
      go to 8500
8190  t80='During decoding time inversion flags.'
      go to 8500
8200  t80='The time inversion flags are inacceptable.'
      go to 8500
8210  t80='The modulation vector is inconsistent with the superspace '//
     1    'symbol.'
      go to 8500
8220  t80='The symbol for addition translation part '//Veta(:idel(Veta))
      go to 8500
8230  t80='The cell centring is not acceptable for the point group.'
      go to 8500
8500  if(NDimI(KPhase).eq.1) then
        Veta='incorrect superspace group symbol.'
      else
        Veta='incorrect space group symbol.'
      endif
      go to 9000
8610  call FeReadError(ln)
      call CloseIfOpened(ln)
8700  call DeletePomFiles
      call FeTmpFilesDelete
      call FeGrQuit
      stop
9000  call FeChybne(-1.,-1.,Veta,t80,SeriousError)
9900  ich=1
9999  if(NDimI(KPhase).gt.0.and.UplnySymbol) then
        call CopyVek(Qu(1,1,1,KPhase),QuSymm,3*NDimI(KPhase))
        call CopyVek(QuOld,Qu(1,1,1,KPhase),3*NDimI(KPhase))
        call CopyVek(QuiOld,Qui(1,1,KPhase),3*NDimI(KPhase))
      endif
      return
      end
      subroutine EM50GenSymOp(itxt,tau,idl,MagInv,AskForDelta,
     1                        UplnySymbol,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm50.cmn'
      dimension tau(idl),vtp(6),tv(3,8),tvo(3,6),pp(6),pq(3),mpp(3),
     1          rmp(36),sp(6),spp(6),QuPom(3),QuSum(3),RM6Old(:,:),
     2          S6Old(:,:)
      character*(*) itxt
      character*2 nty
      character*5 ir
      character*20 ito
      character*60 HallSymbol
      character*80 t80,errtxt
      real NulVek(6)
      logical AskForDelta,EQRV0,EQRV,UplnySymbol
      data ir/'12346'/,ito/'xyz"''*12345abcnuvwd;'/
      data tv/.5,.0,.0,.0,.5,.0,.0,.0,.5,.5,.5,.5,.25,.0,.0,
     1        .0,.25,.0,.0,.0,.25,.25,.25,.25/
      data tvo/1.,0.,0.,0.,1.,0.,0.,0.,1.,1.,1.,0.,1.,-1.,0.,1.,1.,1./
      data NulVek/6*0./
      allocatable RM6Old,S6Old
      ich=0
      Tetra=.false.
      if(NDimI(KPhase).gt.1) then
        NSymmOld=NSymm(KPhase)
        allocate(RM6Old(NDimQ(KPhase),NSymmOld),
     1            S6Old(NDim(KPhase) ,NSymmOld))
        do i=1,NSymmOld
          call CopyMat(RM6(1,i,1,KPhase),RM6Old(1,i),NDim(KPhase))
          call CopyVek( S6(1,i,1,KPhase), S6Old(1,i),NDim(KPhase))
        enddo
      endif
      NSymm(KPhase)=1
      call SetRealArrayTo(rm6(1,1,1,KPhase),maxNDim**2,0.)
      call SetRealArrayTo(s6 (1,1,1,KPhase),maxNDim,0.)
      call SetStringArrayTo(symmc(1,1,1,KPhase),maxNDim,' ')
      call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
      call UnitMat(rm(1,1,1,KPhase),3)
      call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
      call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1              symmc(1,1,1,KPhase),0)
      ISwSymm(NSymm(KPhase),1,KPhase)=1
      ZMag(1,1,KPhase)=1
      call UnitMat(RMag(1,1,1,KPhase),3)
      BratSymm(NSymm(KPhase),KPhase)=.true.
      HallSymbol=itxt
      id=idel(itxt)
1000  if(id.le.0) go to 9999
      if(itxt(1:1).eq.'-') then
        iz=-1
        itxt=itxt(2:)
        id=id-1
      else if(itxt(1:1).eq.'+') then
        iz= 1
        itxt=itxt(2:)
        id=id-1
      else
        iz= 1
      endif
      if(id.le.0) then
        errtxt='rotational information is missing.'
        go to 8000
      endif
      nr=index(ir,itxt(1:1))
      itxt=itxt(2:)
      if(nr.le.0) then
        errtxt='incorrect rotational part.'
        go to 8000
      else if(nr.eq.1) then
        if(idel(itxt).gt.0) then
          errtxt='incorrect rotational part.'
          go to 8000
        else
          go to 5000
        endif
      endif
      Tetra=Tetra.or.nr.eq.4
      ior=-1
      call SetRealArrayTo(vtp,NDim(KPhase),0.)
      pom=0.
1100  if(idel(itxt).gt.0) then
        i=index(ito,itxt(1:1))
        if(i.gt.0) then
          itxt=itxt(2:)
        else
          errtxt='incorrect orientation or translation part.'
          go to 8000
        endif
      else
        i=20
      endif
      if(i.le.6) then
        if(ior.eq.-1) then
          ior=i
          go to 1100
        else
          errtxt='orientation is doubled.'
          go to 8000
        endif
      else if(i.le.19) then
        nt=i-6
        if(nt.le.5) then
          p=nr
          if(nr.eq.5) then
            p=6.
          else if(nr.eq.6.or.nr.eq.7) then
            p=2.
          else if(nr.eq.8) then
            p=3.
          endif
          pom=pom+float(nt)/p
        else
          do i=1,3
            vtp(i)=vtp(i)+tv(i,nt-5)
          enddo
        endif
      else if(i.eq.20) then
        if(ior.lt.0) then
          if(NSymm(KPhase).eq.1) then
            ior=3
          else if(NSymm(KPhase).eq.2.or.NSymm(KPhase).eq.3) then
            if(nr.eq.2.and.(nrold.eq.2.or.nrold.eq.4)) ior=1
            if(nr.eq.2.and.(nrold.eq.3.or.nrold.eq.5)) ior=5
            if(nr.eq.3) ior=6
          endif
        endif
        if(ior.lt.0) then
          errtxt='orientation is not defined.'
          go to 8000
        endif
        if(ior.eq.6) then
          if(nr.eq.3) then
            nr=8
            ior=1
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        else if(ior.eq.4.or.ior.eq.5) then
          if(nr.eq.2) then
            nr=ior+2
            if(NSymm(KPhase).ne.1) then
              ior=iorold
            else
              ior=3
            endif
          else
            errtxt='operator cannot have this orientation.'
            go to 8000
          endif
        endif
        call EM50SetGen(rmp,nr-1,ior,NDim(KPhase))
        if(iz.eq.-1) call realMatrixToOpposite(rmp,rmp,NDim(KPhase))
        do i=1,3
          sp(i)=vtp(i)+pom*tvo(i,ior)
        enddo
        if(NDim(KPhase).gt.3) then
          k1=0
          k2=0
          n1=0
          n2=0
          if(NDim(KPhase).gt.4) then
            k1=-2
            k2= 2
            if(NDim(KPhase).gt.5) then
              n1=-2
              n2= 2
            endif
          endif
        endif
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)*sqrt(1.2)
            enddo
          enddo
        endif
        do 1600i=1,NDimI(KPhase)
          do j=1,3
            pom=0.
            do k=1,3
              pom=pom+(qui(k,i,KPhase)+quir(k,i,KPhase))*
     1                rmp(k+(j-1)*NDim(KPhase))
            enddo
            pp(j)=pom
          enddo
          jknMn=999999
          do n=n1,n2
            do k=k1,k2
              do 1560j=-4,4
                mmpa=0
                do l=1,3
                  pq(l)=float(j)*(qui(l,1,KPhase)+quir(l,1,KPhase))-
     1                            pp(l)
                  if(NDim(KPhase).gt.4) pq(l)=pq(l)+float(k)*
     1                                (qui(l,2,KPhase)+quir(l,2,KPhase))
                  if(NDim(KPhase).gt.5) pq(l)=pq(l)+float(n)*
     1                                (qui(l,3,KPhase)+quir(l,3,KPhase))
                  mpp(l)=nint(pq(l))
                  mmpa=mmpa+iabs(mpp(l))
                  if(abs(pq(l)-float(mpp(l))).gt..0005) go to 1560
                enddo
                do ivt=1,NLattVec(KPhase)
                  pom=ScalMul(vt6(1,ivt,1,KPhase),pq)
                  if(abs(pom-anint(pom)).gt..0005) go to 1560
                enddo
                jkn=iabs(j)+iabs(k)+iabs(n)+mmpa*10
                if(jkn.lt.jknMn) then
                  ip3=i+3
                  do l=1,3
                    rmp(ip3+NDim(KPhase)*(l-1))=-mpp(l)
                  enddo
                  rmp(ip3+NDim(KPhase)*3)=j
                  if(NDim(KPhase).gt.4)
     1              rmp(ip3+NDim(KPhase)*4)=k
                  if(NDim(KPhase).gt.5)
     1              rmp(ip3+NDim(KPhase)*5)=n
                  jknMn=jkn
                endif
1560          continue
            enddo
          enddo
          if(jknMn.ge.999990) then
            errtxt='generator inconsistent with the modulation vector.'
            go to 8000
          endif
1600    continue
        if(NDimI(KPhase).eq.1) then
          do k=1,NDimI(KPhase)
            do j=1,3
              pom=abs(qui(j,k,KPhase)-anint(qui(j,k,KPhase)))
              if(pom.gt..00001.and.abs(pom-.5).gt..00001)
     1          qui(j,k,KPhase)=qui(j,k,KPhase)/sqrt(1.2)
            enddo
          enddo
        endif
        call SetRealArrayTo(sp(4),NDimI(KPhase),0.)
        if(NDimI(KPhase).gt.1) then
          call od0do1(sp,sp,NDim(KPhase))
          do i=1,NSymmOld
            if(eqrv(rmp,rm6Old(1,i),NDimQ(KPhase),.001)) then
              call CopyVek(s6Old(1,i),spp,NDim(KPhase))
              call od0do1(spp,spp,NDim(KPhase))
              call CopyVek(spp(4),sp(4),NDimI(KPhase))
              exit
            endif
          enddo
        endif
2200    NSymm(KPhase)=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,NSymm(KPhase),1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,NSymm(KPhase),1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,NSymm(KPhase),1,KPhase),maxNDim,
     1                        ' ')
        call CopyMat(rmp,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call CopyVek(sp,s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1                 rm(1,NSymm(KPhase),1,KPhase),
     2                 NDim(KPhase))
        call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                s6(1,NSymm(KPhase),1,KPhase),
     2                symmc(1,NSymm(KPhase),1,KPhase),0)
        ISwSymm(NSymm(KPhase),1,KPhase)=1
        BratSymm(NSymm(KPhase),KPhase)=.true.
        if(Poradi) then
          j=NSymm(KPhase)-1
        else
          j=mod(ior-1,3)+1
        endif
        ZMag(NSymm(KPhase),1,KPhase)=MagFlag(j)
        call CrlMakeRMag(NSymm(KPhase),1)
        if(NDimI(KPhase).gt.0) then
          if(NDimI(KPhase).eq.1.and.UplnySymbol) then
            call multm(rm6(1,NSymm(KPhase),1,KPhase),ShiftSg,sp,
     1                 NDim(KPhase),NDim(KPhase),1)
            do i=1,NDim(KPhase)
              sp(i)=s6(i,NSymm(KPhase),1,KPhase)+ShiftSg(i)-sp(i)
            enddo
            call od0do1(sp,sp,NDim(KPhase))
            s6(4,NSymm(KPhase),1,KPhase)=tau(j)+sp(4)+
     1                                   scalmul(quir(1,1,KPhase),sp)
          else
            if(NDimI(KPhase).gt.1) then
              l1=-2
              l2= 2
            else
              l1=0
              l2=0
            endif
            if(NDimI(KPhase).gt.2) then
              m1=-2
              m2= 2
            else
              m1=0
              m2=0
            endif
            i=NSymm(KPhase)
            do 2270j=1,NDimI(KPhase)
              call Multm(Qu(1,j,1,KPhase),rm(1,i,1,KPhase),QuPom,1,3,3)
              do k=-2,2
                fk=k
                do l=l1,l2
                  fl=l
                  do m=m1,m2
                    fmm=m
                    call CopyVek(QuPom,QuSum,3)
                    do n=1,3
                      QuSum(n)=QuSum(n)+Qu(n,1,1,KPhase)*fk
     1                                 +Qu(n,2,1,KPhase)*fl
                      if(NDimI(KPhase).eq.3)
     1                  QuSum(n)=QuSum(n)+Qu(n,3,1,KPhase)*fmm
                      QuSum(n)=QuSum(n)-anint(QuSum(n))
                    enddo
                    if(EqRV0(QuSum,3,.001)) go to 2270
                  enddo
                enddo
              enddo
              call FeChybne(-1.,-1.,'The set of modulation vectors '//
     1                      'is not complete with respect to the '//
     2                      'symmetry.',' ',SeriousError)
              go to 8100
2270        continue
            if(AskForDelta) then
              n=NSymm(KPhase)
              call CopyVek(s6(1,n,1,KPhase),sp,3)
              call EM50OriginShift(ShSg(1,KPhase),n)
              call EM50ReadDelta(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                           symmc(1,n,1,KPhase))
              call CopyVek(sp,s6(1,n,1,KPhase),3)
              call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                      symmc(1,n,1,KPhase),0)
            endif
          endif
        endif
        call od0do1(s6(1,NSymm(KPhase),1,KPhase),
     1              s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        iorold=ior
        nrold=nr
        if(idel(itxt).gt.0) then
          go to 1000
        else
          go to 5000
        endif
      endif
      go to 1100
5000  if(NCSymm(KPhase).eq.2) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call RealMatrixToOpposite(rm6(1,n,1,KPhase),rm6(1,n,1,KPhase),
     1                            NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call RealMatrixToOpposite(rm(1,n,1,KPhase),rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        BratSymm(n,KPhase)=.true.
        ZMag(n,1,KPhase)=MagFlag(NMagFlag)
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
        NCSymm(KPhase)=1
      endif
      if(MagInv.gt.0) then
        n=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        call SetRealArrayTo(rm6(1,n,1,KPhase),maxNDim**2,0.)
        call SetRealArrayTo(s6 (1,n,1,KPhase),maxNDim,0.)
        call SetStringArrayTo(symmc(1,n,1,KPhase),maxNDim,' ')
        call UnitMat(rm6(1,n,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,n,1,KPhase),3)
        call SetRealArrayTo(s6(1,n,1,KPhase),NDim(KPhase),0.)
        if(NDimI(KPhase).eq.1) then
          s6(4,n,1,KPhase)=tau(idl)-
     1      scalmul(quir(1,1,KPhase),s6(1,n,1,KPhase))
        endif
        call CodeSymm(rm6(1,n,1,KPhase),s6(1,n,1,KPhase),
     1                symmc(1,n,1,KPhase),0)
        ZMag(n,1,KPhase)=-1.
        call CrlMakeRMag(n,1)
        ISwSymm(n,1,KPhase)=1
        NSymm(KPhase)=n
      endif
      call CompleteSymm(0,ich)
      go to 9999
8000  write(t80,'(''error in the '',i1,a2,'' Hall''''s generator: '',
     1      a)') NSymm(KPhase)+1,nty(NSymm(KPhase)+1),
     2           HallSymbol(:idel(HallSymbol))
      call FeChybne(-1.,-1.,t80,errtxt(1:idel(errtxt)),SeriousError)
8100  ich=1
9999  if(allocated(RM6Old)) deallocate(RM6Old,S6Old)
      return
      end
      subroutine ReadCIF(Klic,CoDal)
      use Basic_mod
      use Powder_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      real qup(3,mxw),rmp(36),gpp(36),CellParPom(6),xp(6),xq(6),sp1(6),
     1     sp2(6)
      real, allocatable :: pa(:),spa(:),aip(:),saip(:),AtMultP(:),
     1                     OrX40P(:),OrDelP(:),OrEpsP(:),rm6o(:,:),
     2                     s6o(:,:),vt6o(:,:)
      integer imn(3),imx(3),il(10),mrt(3),kmodn(7),iwa(20),kwp(3,mxw),
     1        IOldKey(30),JOldKey(30),i90(3), FullMultiplicity,CoDal,
     2        DataPosition(50),UseTabsIn,CrSystemFromCell,ProHonzu,
     3        kwi(mxw),mrto(3)
      integer, allocatable :: ia(:),EqSymm(:)
      character*(*) FileCIFIn
      character*256 FileIn,t256,p256,s256,e256
      character*80  OldCifKey(30)
      character*80, allocatable :: StLbl(:),sta(:),stap(:),IdCode(:)
      character*30  SymmCP(6)
      character*16  DataLabel(50)
      character*5   MenuJ0(10),MenuJ2(10)
      character*3   smbu(6),t3,StPh
      character*2   nty
      character*1   Znak
      logical eqiv,eqrv,CellMissing,QMissing,EqIgCase,FeYesNo,EqRV0,
     1        FeYesNoHeader,StructureExists,ExistFile,lpom,Psat,
     2        MatRealEqUnitMat,ReadMagneticCIF,EqRVM,ZShelx,DelejOrtho,
     3        eqivm
      equivalence (qup,gpp),(ProHonzu,IdNumbers(2))
      data smbu/'u11','u22','u33','u12','u13','u23'/
      data (OldCifKey(i),i=1,28)
     1              /'_symmetry_ssg_name_IT_number',
     2               '_symmetry_ssg_name',
     3               '_symmetry_ssg_name_IT',
     4               '_symmetry_ssg_name_WJJ',
     5               '_symmetry_ssg_WJJ_code',
     6               '_symmetry_ssg_equiv_pos_as_xyz',
     7               '_symmetry_ssg_equiv_pos_seq_id',
     8               '_atom_site_occ_fourier_cos',
     9               '_atom_site_occ_fourier_id',
     a               '_atom_site_occ_fourier_modulus',
     1               '_atom_site_occ_fourier_phase',
     2               '_atom_site_occ_fourier_sin',
     3               '_atom_site_displace_fourier_cos',
     4               '_atom_site_displace_fourier_id',
     5               '_atom_site_displace_fourier_modulus',
     6               '_atom_site_displace_fourier_phase',
     7               '_atom_site_displace_fourier_sin',
     8               '_atom_site_rot_fourier_cos',
     9               '_atom_site_rot_fourier_id',
     a               '_atom_site_rot_fourier_modulus',
     1               '_atom_site_rot_fourier_phase',
     2               '_atom_site_rot_fourier_sin',
     3               '_atom_site_u_fourier_cos',
     4               '_atom_site_u_fourier_id',
     5               '_atom_site_u_fourier_modulus',
     6               '_atom_site_u_fourier_phase',
     7               '_atom_site_u_fourier_sin',
     8               '_chemical_formula'/
      data (IOldKey(i),JOldKey(i),i=1,28)/
     1                                     1, 21,
     2                                     2, 21,
     3                                     3, 21,
     4                                     4, 21,
     5                                     5, 21,
     6                                     6, 21,
     7                                     7, 21,
     8                                    71,  1,
     9                                    72,  1,
     a                                    73,  1,
     1                                    74,  1,
     2                                    75,  1,
     3                                    58,  1,
     4                                    59,  1,
     5                                    60,  1,
     6                                    61,  1,
     7                                    62,  1,
     8                                    83,  1,
     9                                    84,  1,
     a                                    85,  1,
     1                                    86,  1,
     2                                    87,  1,
     3                                    92,  1,
     4                                    93,  1,
     5                                    94,  1,
     6                                    95,  1,
     7                                    96,  1,
     8                                    22,  6/
      data NOldCifKeys/28/
      DelejOrtho=.false.
      OrthoOrd=0
      CoDal=0
      lni=0
      KlicUsed=Klic
      if(StructureExists(fln).and.KlicUsed.ne.5) then
        if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1                  '" already exists, rewrite it?',0)) go to 9999
        if(ExistM40)
     1    call MoveFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
        if(ExistM50)
     1    call MoveFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      endif
      FileIn=fln(:ifln)//'.cif'
      if(KlicUsed.ne.ProHonzu) then
1120    call FeFileManager('Select input CIF file',FileIn,
     1                     '*.cif *.mcif',0,.true.,ich)
        if(ich.ne.0) go to 9900
        if(.not.ExistFile(FileIn)) then
          call FeChybne(-1.,YBottomMessage,'the file "'//
     1                  FileIn(:idel(FileIn))//
     1                  '" does not exist, try again.',' ',SeriousError)
          go to 1120
        endif
      endif
      go to 1150
      entry ReadSpecifiedCIF(FileCIFIn)
      lni=0
      FileIn=FileCIFIn
      KlicUsed=-1
1150  call CheckEOLOnFile(FileIn,2)
      if(ErrFlag.ne.0) go to 9999
      if(KPhase.le.1) then
        StPh=' '
      else
        write(StPh,'(''_'',i2)') KPhase
        call Zhusti(STPh)
      endif
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      if(KlicUsed.ne.ProHonzu.and..not.ICDDBatch) then
        nmax=0
        nl=0
        n=0
        p256=' '
1200    read(lni,FormA,end=1300) t256
        call SkrtniPrvniMezery(t256)
        nl=nl+1
        if(EqIgCase(t256(1:5),'data_')) then
          n=nl
          p256=t256(6:)
          if(p256.eq.' ') then
            write(p256,'(''Data#'',i3)') nmax+1
            call Zhusti(p256)
          endif
          go to 1200
        endif
        if(LocateSubstring(t256,'_cell_length',.false.,.true.).eq.1
     1                     .and.p256.ne.' ') then
          if(nmax.lt.50) then
            nmax=nmax+1
            DataLabel(nmax)=p256
            DataPosition(nmax)=n
            p256=' '
          endif
        endif
        go to 1200
1300    rewind lni
        if(nmax.ge.1) then
          i=1
          if(nmax.gt.1) then
            call SelOneAtom('Select CIF part to be imported',DataLabel,
     1                      i,nmax,ich)
            if(ich.ne.0) go to 9999
          endif
          i=DataPosition(i)
          nl=0
1320      read(lni,FormA,end=1330) t256
          nl=nl+1
          if(nl.eq.i) then
            go to 1330
          endif
          call SkrtniPrvniMezery(t256)
          if(EqIgCase(t256(1:5),'data_')) then
            if(nl.eq.i) go to 1330
          endif
          go to 1320
        endif
      endif
1330  call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.ne.0) go to 9999
      if(KPhase.le.1) then
        call SetBasicM40(.true.)
        call SetBasicM50
        isPowder=.false.
        ExistPowder=.false.
      endif
      if(allocated(CIFArray)) deallocate(CIFArray)
      nCIFArray=1000
      allocate(CIFArray(nCIFArray))
      m=0
      Psat=.true.
      ReadMagneticCIF=.false.
1400  read(lni,FormA,end=1500) t256
      s256=t256
      call SkrtniPrvniMezery(s256)
      if(LocateSubstring(s256,'data_',.false.,.true.).eq.1) then
        if(ICDDBatch) then
          Psat=LocateSubstring(s256,'data_AUDIT',.false.,.true.)
     1         .ne.1.and.
     2         LocateSubstring(s256,'data_REFERENCE',.false.,.true.)
     3         .ne.1.and.
     4         LocateSubstring(s256,'data_AtomicCoordinates_REFERENCE',
     5                         .false.,.true.)
     6         .ne.1.and.
     7         LocateSubstring(s256,'data_DILIST_REFERENCE',.false.,
     8                         .true.)
     9         .ne.1.and.
     a         LocateSubstring(s256,'data_POWDERPATTERN_REFERENCE',
     1                         .false.,.true.)
     2         .ne.1
        else
          n=0
          do i=1,idel(t256)
            if(t256(i:i).eq.'_') then
              n=n+1
              if(n.gt.1) go to 1400
            endif
          enddo
          go to 1500
        endif
      endif
      if(.not.Psat) go to 1400
      if(LocateSubstring(s256,'###non-st#',.false.,.true.).eq.1) then
        i=11
        t256=s256(i:)
        s256=s256(i:)
      endif
      if(.not.ReadMagneticCIF.and.
     1  (LocateSubstring(s256,'_magn_',.false.,.true.).gt.1.or.
     2   LocateSubstring(s256,'.magn_',.false.,.true.).gt.1))
     3  ReadMagneticCIF=.true.
      if(LocateSubstring(s256,'_cell_length',.false.,.true.).eq.1.and.
     1   index(s256,'?').gt.0) go to 1400
      if(LocateSubstring(s256,'_cell_angle',.false.,.true.).eq.1..and.
     1   index(s256,'?').gt.0) go to 1400
      p256=t256
      k=0
      call Kus(t256,k,s256)
      do n=1,NOldCifKeys
        it=idel(OldCifKey(n))
        if(EqIgCase(s256,OldCifKey(n)(:it))) then
          i=LocateSubstring(t256,OldCifKey(n)(:it),.false.,.true.)
          if(i.gt.0) then
            j=i+it
            s256=CifKey(IOldKey(n),JOldKey(n))
            p256=' '
            p256(i:)=s256(:idel(s256))//t256(j:)
          endif
          exit
        endif
      enddo
      if(KPhase.le.1) then
        k=0
        call kus(p256,k,s256)
        if(EqIgCase(s256,CifKey(27,20))) then
          call kus(p256,k,Cislo)
          if(Cislo(1:1).ne.'?') then
            isPowder=.true.
            if(KPhase.le.1) call SetBasicM41(KDatBlock)
          endif
        endif
      endif
      m=m+1
      if(m.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
      j=index(p256,'_')
      if(j.gt.0) then
        i=index(p256,'#')
        if(i.gt.0) p256(i:)=' '
      endif
      CIFArray(m)=p256
      go to 1400
1500  nCIFUsed=m
      do j=1,40
        do i=1,400
          call mala(CifKey(i,j))
        enddo
      enddo
      call CIFGetMxLoop(MxLoop)
      MxLoop=max(MxLoop+100,1000)
      if(ReadMagneticCIF) then
        MagneticType(KPhase)=1
        MaxMagneticType=1
      endif
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta,stap)
      allocate(pa(MxLoop),spa(MxLoop),ia(MxLoop),StLbl(MxLoop),
     1         sta(MxLoop),stap(MxLoop))
      if(KlicUsed.eq.3) then
        write(out,'(''Spelling of used CIF keywords'')')
        write(out,'(''============================='')')
        rewind(lni)
1600    read(lni,FormA,end=1620) t256
        call SkrtniPrvniMezery(t256)
        call mala(t256)
        k=0
        call kus(t256,k,p256)
        if(p256(1:1).ne.'_') then
          go to 1600
        else
          do j=1,40
            do i=1,400
              k=idel(CifKey(i,j))
              if(k.le.0) cycle
              if(index(p256,CifKey(i,j)(:k)).gt.0) then
                CifKeyFlag(i,j)=0
                go to 1600
              endif
            enddo
          enddo
          call FeChybne(-1.,-1.,'unknown keyword:',p256,SeriousError)
          ErrFlag=1
          go to 1600
        endif
1620    call CloseIfOpened(lni)
        if(ErrFlag.ne.0) go to 9999
        write(out,'(''All used keywords OK'')')
        write(out,FormA)
        write(out,'(''Presence of crutial CIF keywords'')')
        write(out,'(''================================'')')
        do j=1,40
          do i=1,400
            if(CifKeyFlag(i,j).ne.0) then
              call FeChybne(-1.,-1.,'crutial keyword missing:',
     1                      CifKey(i,j),SeriousError)
              ErrFlag=1
            endif
          enddo
        enddo
        if(ErrFlag.ne.0) go to 9999
        write(out,'(''All crutial data present'')')
        write(out,FormA)
        write(out,'(''Other problems'')')
        write(out,'(''=============='')')
      else if(KlicUsed.eq.ProHonzu) then
        lnb=NextLogicNumber()
        call OpenFile(lnb,fln(:ifln)//'.bib','formatted','unknown')
        s256=CifKey(31,14)
        call CIFGetSt(s256,sta,mm,t256,ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          n=1
        else
          call CIFGetLoop(' ',StLbl,MxLoop,s256,ia,Sta,pa,spa,0,n,
     1                    t256,ich)
          if(ich.eq.-2) go to 9000
        endif
        if(ich.eq.0) then
          t256='0010 '//sta(1)(:idel(sta(1)))
          do i=2,n-1
            t256=t256(:idel(t256))//', '//sta(i)(:idel(sta(i)))
          enddo
          if(n.gt.1) t256=t256(:idel(t256))//' & '//
     1                    sta(n)(:idel(sta(n)))
          write(lnb,FormA) t256(:idel(t256))
        endif
        call CIFGetSt(CifKey(23,13),sta,mm,t256,ich)
        s256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256='0010 '//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        else
          write(lnb,'(''0010 Unknown journal'')')
        endif
        call CIFGetSt(CifKey(36,13),sta,mm,t256,ich)
        s256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.ne.0) then
          s256='?'
        endif
        call CIFGetSt(CifKey(37,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256))//' ('//p256(:idel(p256))//'),'
        endif
        call CIFGetSt(CifKey(24,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256)+1)//p256(:idel(p256))
        else
          s256=s256(:idel(s256))//' ?'
        endif
        call CIFGetSt(CifKey(25,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256))//'-'//p256(:idel(p256))
        else
          s256=s256(:idel(s256))//'-?'
        endif
        s256='0010 '//s256(:idel(s256))
        write(lnb,FormA) s256(:idel(s256))
        call CIFGetReal(CifKey(22,15),pom,spom,t256,ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          write(Cislo,'(f8.3)') pom
        else
          Cislo='?'
        endif
        call Zhusti(Cislo)
        write(lnb,'(''0020 '',a)') Cislo(:idel(Cislo))
        do i=1,4
          k=20+i
          if(i.eq.1) then
            j=7
          else if(i.eq.2) then
            j=22
          else if(i.eq.3) then
            j=23
          else
            j=5
            k=29
          endif
          write(Cislo,'(''00'',i2)') k
          call CIFGetSt(CifKey(j,6),sta,mm,t256,ich)
          s256=sta(1)
          if(ich.eq.-2) then
            go to 9000
          else if(ich.ne.0) then
            s256='?'
          endif
          s256=Cislo(1:5)//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        enddo
        do i=1,2
          k=29+i
          if(i.eq.1) then
            j=1
          else
            j=4
          endif
          write(Cislo,'(''00'',i2)') k
          call CIFGetSt(CifKey(j,18),sta,mm,t256,ich)
          s256=sta(1)
          if(ich.eq.-2) then
            go to 9000
          else if(ich.ne.0) then
            s256='?'
          endif
          s256=Cislo(1:5)//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        enddo
      endif
      CIFFlag=0
      NSymm(KPhase)=0
      NAtFormulaP=0
      isTOF=.false.
      isED=.false.
      TOFInD=.false.
      ChargeDensities=.false.
      CellMissing=.false.
         QMissing=.false.
      do i=1,3
        s256=CifKey(i+4,5)
        call CIFGetReal(s256,CellPar(i,1,KPhase),
     1                  CellParSU(i,1,KPhase),t256,ich)
        if(ich.lt.0) then
          CellPar(i,1,KPhase)=10.
          CellParSU(i,1,KPhase)=0.
          if(ich.eq.-1) then
            CellMissing=.true.
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      enddo
      do i=4,6
        s256=CifKey(i-3,5)
        call CIFGetReal(s256,CellPar(i,1,KPhase),
     1                  CellParSU(i,1,KPhase),t256,ich)
        if(ich.lt.0) then
          CellPar(i,1,KPhase)=90.
          CellParSU(i,1,KPhase)=0.
          if(ich.eq.-1) then
            CellMissing=.true.
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      enddo
      if(isPowder) call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
      s256=CifKey(21,5)
      call CIFGetInt(s256,n,t256,ich)
      if(ich.lt.0) then
        NComp(KPhase)=1
        n=0
        if(ich.eq.-1) then
          n=0
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      endif
      if(n.le.0) then
        s256=CifKey(147,5)
        call CIFGetReal(s256,qu(1,1,1,KPhase),pom,t256,ich)
        if(ich.gt.0) n=ich
      endif
      NDim(KPhase)=n+3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      MaxNDim=max(MaxNDim,NDim(KPhase))
      MaxNDimI=MaxNDim-3
      if(NDimI(KPhase).gt.0) then
        call SetIntArrayTo(kw(1,1,KPhase),3*mxw,0)
        if(NDimI(KPhase).eq.1) then
          j=mxw
        else
          j=NDimI(KPhase)
        endif
        do i=1,j
          kw(mod(i-1,NDimI(KPhase))+1,i,KPhase)=(i-1)/NDimI(KPhase)+1
        enddo
      endif
      call SetRealArrayTo(qu(1,1,1,KPhase),9,0.)
      if(KPhase.le.1) then
        m=22
        mm=23
        n=22
        call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,Sta,
     1                  pa,spa,1,nn,t256,ich)
        if(ich.eq.-1) then
          m=6
          mm=7
          n=23
          call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,Sta,
     1                    pa,spa,1,nn,t256,ich)
        endif
        NTwinOld=NTwin
        if(ich.lt.0) then
          NTwin=NTwinOld
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        else
          NTwin=nn
        endif
        if(nn.gt.1) then
          if(n.eq.22) then
            m=22
            m0=12
          else
            m=6
            m0=7
          endif
          allocate(IdCode(NTwin))
          do i=1,NTwin
            ScTw (i,KDatBlock)= pa(i)
            ScTwS(i,KDatBlock)=spa(i)
            IdCode(i)=StLbl(i)
          enddo
          k=0
          do i=1,3
            do j=1,3
              k=k+1
              mm=m0+i+(j-1)*3
              call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,
     1                        Sta,pa,spa,1,nn,t256,ich)
              do l=1,nn
                do kk=1,NTwin
                  if(EqIgCase(StLbl(l),IdCode(kk))) then
                    RTw(k,kk)=pa(l)
                    exit
                  endif
                enddo
              enddo
            enddo
          enddo
          deallocate(IdCode)
          mxscutw=max(NTwin,6)
          mxscu=mxscutw-NTwin+1
        endif
      endif
      if(NDimI(KPhase).gt.0) then
        s256=CifKey(146,5)
        call CIFGetInt(s256,NComp(KPhase),t256,ich)
        if(ich.lt.0) then
          NComp(KPhase)=1
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        endif
        do i=1,3
          s256=CifKey(146+i,5)
          call CIFGetReal(s256,qu(i,1,1,KPhase),pom,t256,ich)
          if(ich.le.0) then
            if(ich.ge.-1) then
              cycle
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          s256=CifKey(150,5)
          p256=CifKey(146+i,5)
          call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,
     1                    ich)
          if(ich.le.0) then
            if(ich.eq.-1) then
              cycle
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          jj=0
          do j=1,n
            if(StLbl(j).eq.'?') cycle
            jj=jj+1
            StLbl(jj)=StLbl(j)
            pa(jj)=pa(j)
          enddo
          n=jj
          do j=1,NDimI(KPhase)
            k=0
            call kus(StLbl(j),k,Cislo)
            call posun(Cislo,0)
            read(Cislo,FormI15) k
            qu(i,k,1,KPhase)=pa(k)
          enddo
        enddo
        do i=1,NDimI(KPhase)
          if(EqRV0(qu(1,i,1,KPhase),3,.0001)) then
            QMissing=.true.
            exit
          endif
        enddo
        if(NComp(KPhase).gt.1) then
          do i=1,NComp(KPhase)
            call SetRealArrayTo(zv(1,i,KPhase),NDimQ(KPhase),0.)
          enddo
          allocate(IdCode(NComp(KPhase)))
          p256=CifKey(25,5)
          ip=LocateSubstring(p256,'W',.false.,.true.)+1
          k=0
          do i=1,NDim(KPhase)
            do j=1,NDim(KPhase)
              k=k+1
              write(p256(ip:),'(2(''_'',i2))') j,i
              call Zhusti(p256)
              s256=CifKey(24,5)
              call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,
     1                        t256,ich)
              if(ich.eq.-2) then
                call CIFSyntaxError(CIFLastReadRecord)
                go to 2000
              else if(ich.eq.0) then
                do l=1,n
                  ZV(k,l,KPhase)=pa(l)
                enddo
              endif
            enddo
          enddo
          call CopyStringArray(StLbl,IdCode,n)
        else
          call UnitMat(zv(1,1,KPhase),NDim(KPhase))
        endif
        do i=1,NComp(KPhase)
          call MatInv(ZV(1,i,KPhase),ZVI(1,i,KPhase),Det,NDim(KPhase))
          if(abs(Det).lt..001) then
            write(Cislo,'(i1,a2)') i,nty(i)
            call FeChybne(-1.,-1.,'W matrix for '//Cislo(:3)//
     1                    ' subsystem is singular.',' ',SeriousError)
            CIFFlag=1
            call UnitMat(zv(1,i,KPhase),NDim(KPhase))
          endif
        enddo
        n=22
        do m=1,9
          call CIFGetReal(CifKey(m,n),rmp(m),spom,t256,ich)
          if(ich.ne.0) then
            if(ich.ge.-1) then
              KCommen(KPhase)=0
              exit
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          KCommen(KPhase)=1
        enddo
        if(KCommen(KPhase).gt.0) then
          pom=0.
          m=0
          do i=1,3
            do j=1,3
              m=m+1
              if(i.ne.j) pom=pom+abs(rmp(m))
            enddo
          enddo
          if(pom.gt..00001) then
            ICommen(KPhase)=1
            call TrMat(rmp,RCommen(1,1,KPhase),3,3)
          else
            ICommen(KPhase)=0
            m=0
            do i=1,3
              do j=1,3
                m=m+1
                if(i.eq.j) NCommen(i,1,KPhase)=nint(rmp(m))
              enddo
            enddo
          endif
          do i=1,NDimI(KPhase)
            m=i+9
            call CIFGetReal(CifKey(m,n),trez(i,1,KPhase),spom,t256,ich)
            if(ich.ne.0) then
              if(ich.ge.-1) then
                KCommen(KPhase)=0
                exit
              else if(ich.eq.-2) then
                call CIFSyntaxError(CIFLastReadRecord)
              endif
            endif
          enddo
        endif
      endif
      go to 2010
2000  do i=1,NComp(KPhase)
        call UnitMat(zv(1,i,KPhase),NDim(KPhase))
      enddo
2010  if(CellMissing) then
        call FeChybne(-1.,-1.,'Some of cell parameters are missing.',
     1                ' ',SeriousError)
        CIFFlag=1
      endif
      if(QMissing) then
        call FeChybne(-1.,-1.,'some components of modulation vector '//
     1                'are missing.',' ',SeriousError)
        CIFFlag=1
      endif
      CrSystem(KPhase)=CrSystemFromCell(CellPar(1,1,KPhase),.001,.05)
      Monoclinic(KPhase)=CrSystem(KPhase)/10
      MameSymbol=0
      if(ReadMagneticCIF) go to 2020
      if(NDimI(KPhase).eq.0) then
        call CIFGetSt(CifKey(4,18),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.0) then
          ShSg(1:NDim(KPhase),KPhase)=0.
          call Zhusti(p256)
          Grupa(KPhase)=p256
          if(NPhase.le.1) then
            n=1
            call AllocateSymm(n+1,n+1,1,1)
          endif
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,pa,ich)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ich.eq.0) then
            MameSymbol=1
            n=NSymm(KPhase)
          endif
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      else
        MameSymbol=0
        call CIFGetSt(CifKey(3,21),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          call CIFGetSt(CifKey(2,21),sta,mm,t256,ich)
          p256=sta(1)
        endif
        if(ich.eq.0) then
          call CIFITSSGSymbol(p256,ich)
          if(ich.eq.0) then
            MameSymbol=1
            n=NSymm(KPhase)
          endif
        else
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          call CIFGetSt(CifKey(4,21),sta,mm,t256,ich)
          p256=sta(1)
          if(ich.eq.0) then
            call CIFWJJSSGSymbol(p256,ich)
            if(ich.eq.0) then
              MameSymbol=2
              n=NSymm(KPhase)
            endif
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      endif
      if(MameSymbol.ne.0) then
        MaxNSymm=NSymm(KPhase)
        MaxNSymmN=NSymm(KPhase)
        NSymmN(KPhase)=NSymm(KPhase)
        Lattice(KPhase)=Grupa(KPhase)(1:1)
      endif
2020  MameOperatory=0
      e256=' '
      kk=0
2025  n=0
      if(NDimI(KPhase).le.0) then
        if(kk.eq.0) then
          s256=CifKey(8,21)
          p256=CifKey(9,21)
        else
          s256=CifKey(5,18)
          p256=CifKey(6,18)
        endif
        kk=kk+1
      else
        s256=CifKey(6,21)
        p256=CifKey(7,21)
      endif
2030  call CIFGetSt(s256,sta,mm,t256,ich)
      if(ich.gt.0) then
        n=mm
      else if(ich.eq.-2) then
        e256=CIFLastReadRecord
      else if(ich.eq.-1) then
        call CIFGetSt(p256,sta,mm,t256,ich)
        if(ich.gt.0) then
          n=-mm
        else if(ich.eq.-2) then
          e256=CIFLastReadRecord
        endif
      endif
      if(e256.ne.' ') then
        ich=-2
        CIFLastReadRecord=e256
        go to 9000
      endif
      if(ich.lt.0) then
        if(kk.eq.1.and.NDimI(KPhase).le.0) go to 2025
      else
        MameOperatory=1
        if(n.lt.0) then
          s256=p256
          kk=1
        else
          kk=0
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,
     1                  ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.-1) then
          MameOperatory=0
        endif
        if(kk.eq.1) then
          do i=1,n
            Sta(i)=StLbl(i)
            write(StLbl(i),'(i5)') i
            call Zhusti(StLbl(i))
          enddo
        endif
      endif
      if(MameOperatory.le.0.and.ReadMagneticCIF) then
        if(NDimI(KPhase).le.0) then
          s256=CifKey(39,21)
          p256=CifKey(41,21)
        else
          s256=CifKey(69,21)
          p256=CifKey(70,21)
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,
     1                  ich)
        if(ich.eq.-2) then
          n=0
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      endif
2040  if(n.le.0) then
        if(MameSymbol.gt.0) go to 2250
        n=1
        if(NDimI(KPhase).eq.0) then
          sta(1)='x,y,z'
        else
          sta(1)=' '
          do i=1,NDim(KPhase)
            write(t3,'(''x'',i1,'','')') i
            sta(1)=sta(1)(:idel(sta(1)))//t3
          enddo
          i=idel(sta(1))
          sta(1)(i:i)=' '
        endif
        if(MameSymbol.eq.0.and.MameOperatory.eq.0) then
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,'symmetry information is missing.',
     1                    'Trivial symmetry will be used.',Warning)
            NSymm(KPhase)=1
            NSymmN(KPhase)=1
            NLattVec(KPhase)=1
            if(NPhase.le.1) call AllocateSymm(24,4,1,1)
            call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
            call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
            call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
          else
            NInfo=1
            TextInfo(NInfo)='Symmetry information is missing. The '//
     1                      'program will use just trivial symmetry.'
            NInfo=NInfo+1
            TextInfo(NInfo)='You should either correct manually the'//
     1                      ' input CIF file and import it once more'
            NInfo=NInfo+1
            TextInfo(NInfo)='or correct the file M50 by Jana tools.'
            call FeInfoOut(-1.,-1.,'Warning','L')
          endif
        endif
      endif
2050  if(MameSymbol.ne.0) then
        if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
        allocate(rm6o(NDimQ(KPhase),NSymm(KPhase)),
     1           s6o(NDim(KPhase),NSymm(KPhase)),
     2           vt6o(NDim(KPhase),NLattVec(KPhase)))
        nso=NSymm(KPhase)
        nvto=NLattVec(KPhase)
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6(1,i,1,KPhase),vt6o(1,i),NDim(KPhase))
        enddo
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,1,KPhase),rm6o(1,i),NDim(KPhase))
          call CopyVek(s6(1,i,1,KPhase),s6o(1,i),NDim(KPhase))
        enddo
      endif
      if(NPhase.le.1) then
        call AllocateSymm(n+1,n+1,1,1)
      else
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),NComp(KPhase),
     1                   NPhase,1)
      endif
      NLattVec(KPhase)=1
      call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
      NSymm(KPhase)=n
      MaxNSymm=n
      NSymmN(KPhase)=n
      MaxNSymmN=n
      Identita=0
      do i=1,n
        if(KlicUsed.eq.ProHonzu) then
          t256='0032 '//sta(i)(:idel(sta(i)))
          write(lnb,FormA) t256(:idel(t256))
        endif
2060    call DeleteString(sta(i),'~',k)
        if(k.gt.0) go to 2060
2065    call DeleteString(sta(i),'''',k)
        if(k.gt.0) go to 2065
        do j=1,idel(sta(i))
          if(sta(i)(j:j).eq.',') sta(i)(j:j)=' '
        enddo
        k=0
        do j=1,NDim(KPhase)
          call kus(Sta(i),k,Cislo)
        enddo
        call SetIgnoreWTo(.true.)
        call SetIgnoreETo(.true.)
        call ReadSymm(sta(i)(:k),rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),pom,0)
        call MatBlock3(rm6(1,i,1,KPhase),rm(1,i,1,KPhase),NDim(KPhase))
        call MatInv(rm(1,i,1,KPhase),rmp,det,3)
        if(ReadMagneticCIF.and.k.lt.len(Sta(i))) then
          Cislo=Sta(i)(k:)
          call posun(Cislo,1)
          read(Cislo,'(f15.0)') ZMag(i,1,KPhase)
        else
          ZMag(i,1,KPhase)=1.
        endif
        rmag(1:3,i,1,KPhase)=ZMag(i,1,KPhase)*det*rm(1:3,i,1,KPhase)
        BratSymm(i,KPhase)=.true.
        ISwSymm(i,1,KPhase)=1
        call ResetIgnoreW
        call ResetIgnoreE
        if(ErrFlag.ne.0) then
          call CIFSyntaxError('symmetry card : '//sta(i)(:idel(sta(i))))
          n=0
          go to 2040
        endif
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      if(NDimI(KPhase).eq.0) then
        if(ReadMagneticCIF) then
          s256=CifKey(46,21)
          p256=CifKey(48,21)
        else
          s256=CifKey(43,21)
          p256=CifKey(45,21)
        endif
      else
        if(ReadMagneticCIF) then
          s256=CifKey(73,21)
          p256=CifKey(74,21)
        else
          s256=CifKey(71,21)
          p256=CifKey(72,21)
        endif
      endif
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,StaP,pa,spa,0,m,t256,
     1                ich)
      if(ich.eq.-2) then
        go to 9000
      else if(ich.ne.-1.and.m.gt.0) then
        call ReallocSymm(NDim(KPhase),NSymm(KPhase)+m,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        nold=n
        do i=1,m
2090      call DeleteString(StaP(i),'~',k)
          if(k.gt.0) go to 2090
2095      call DeleteString(StaP(i),'''',k)
          if(k.gt.0) go to 2095
          do j=1,idel(StaP(i))
            if(StaP(i)(j:j).eq.',') StaP(i)(j:j)=' '
          enddo
          k=0
          do j=1,NDim(KPhase)
            call kus(StaP(i),k,Cislo)
          enddo
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          call ReadSymm(StaP(i)(:k),rmp,xp,SymmCP,pom,0)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ErrFlag.ne.0.or.
     1       .not.MatRealEqUnitMat(rmp,NDim(KPhase),.001)) then
            call CIFSyntaxError('centering operation : '//
     1                          StaP(i)(:idel(StaP(i))))
            cycle
          endif
          call od0do1(xp,xp,NDim(KPhase))
          if(ReadMagneticCIF.and.k.lt.len(StaP(i))) then
            Cislo=StaP(i)(k:)
            call posun(Cislo,1)
            read(Cislo,'(f15.0)') ZMagP
          else
            ZMagP=1.
          endif
          if(EqRV0(xp,NDim(KPhase),.001).and.abs(ZMagP-1.).le..001 )
     1      cycle
          n=n+1
          BratSymm(n,KPhase)=.true.
          ISwSymm(n,1,KPhase)=1
          rm6(1:NDimQ(KPhase),n,1,KPhase)=rmp(1:NDimQ(KPhase))
          s6(1:NDim(KPhase),n,1,KPhase)=xp(1:NDim(KPhase))
          symmc(1:NDim(KPhase),n,1,KPhase)=symmcp(1:NDim(KPhase))
          ZMag(n,1,KPhase)=ZMagP
          call MatBlock3(rm6(1,n,1,KPhase),rm(1,n,1,KPhase),
     1                   NDim(KPhase))
          rmag(1:9,n,1,KPhase)=ZMagP*rm(1:9,n,1,KPhase)
        enddo
        if(n.gt.nold) then
          NSymm(KPhase)=n
          call CompleteSymm(1,ich)
          n=NSymm(KPhase)
        endif
      endif
      do i=1,n
        if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.001)) then
          if(EqRV0(s6(1,i,1,KPhase),NDim(KPhase),.001)) then
            identita=i
          else if(MagneticType(KPhase).le.0.or.ZMag(i,1,KPhase).gt.0)
     1      then
            BratSymm(i,KPhase)=.false.
            call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                       NLattVec(KPhase)+1,NComp(KPhase),NPhase,1)
            NLattVec(KPhase)=NLattVec(KPhase)+1
            call CopyVek(s6(1,i,1,KPhase),
     1                   vt6(1,NLattVec(KPhase),1,KPhase),NDim(KPhase))
            call od0do1(vt6(1,NLattVec(KPhase),1,KPhase),
     1                  vt6(1,NLattVec(KPhase),1,KPhase),NDim(KPhase))
          endif
        endif
      enddo
      if(Identita.ne.1) then
        call CopyMat(rm6(1,1,1,KPhase),rm6(1,Identita,1,KPhase),
     1               NDim(KPhase))
        call MatBlock3(rm6(1,1,1,KPhase),rm(1,Identita,1,KPhase),
     1                 NDim(KPhase))
        call CopyMat(rm(1,1,1,KPhase),rmag(1,Identita,1,KPhase),3)
        call CopyVek(s6(1,1,1,KPhase),s6(1,Identita,1,KPhase),
     1               NDim(KPhase))
        BratSymm(Identita,KPhase)=BratSymm(1,KPhase)
        call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,1,1,KPhase),3)
        call UnitMat(rmag(1,1,1,KPhase),3)
        call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
        BratSymm(1,KPhase)=.true.
      endif
      if(KlicUsed.eq.ProHonzu) close(lnb)
      do i=1,n
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      if(NLattVec(KPhase).gt.0) then
        do i=1,n-1
          if(.not.BratSymm(i,KPhase)) cycle
          rmp(1:NDimQ(KPhase))=rm6(1:NDimQ(KPhase),i,1,KPhase)
          sp1(1:NDim(KPhase))=s6(1:NDim(KPhase),i,1,KPhase)
          call NormCentr(sp1)
          do j=i+1,n
            if(EqRV(rmp,rm6(1,j,1,KPhase),NDimQ(KPhase),.001).and.
     1         BratSymm(j,KPhase)) then
              sp2(1:NDim(KPhase))=s6(1:NDim(KPhase),j,1,KPhase)
              call NormCentr(sp2)
              if(Eqrv(sp1,sp2,NDim(KPhase),.0001))
     1          BratSymm(j,KPhase)=.false.
            endif
          enddo
        enddo
        NSymm(KPhase)=0
        do i=1,n
          if(.not.BratSymm(i,KPhase)) cycle
          NSymm(KPhase)=NSymm(KPhase)+1
          call CopySymmOperator(i,NSymm(KPhase),1)
        enddo
      endif
      call CrlOrderMagSymmetry
      MaxNSymm=NSymm(KPhase)
      MaxNSymmN=NSymmN(KPhase)
      IgnoreE=.true.
      call CompleteSymm(1,ich)
      IgnoreE=.false.
      if(MaxNSymm.ne.NSymm(KPhase)) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        NSymm(KPhase)=MaxNSymm
        dlk=0.
        do i=1,NSymm(KPhase)
          do j=1,NDim(KPhase)
            dlk=max(dlk,FeTxLength(SymmC(j,i,1,KPhase)))
          enddo
        enddo
        dlk=dlk*2.
        pom=0.
        do i=1,NDim(KPhase)
          call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
          pom=pom+dlk
        enddo
        dls=0.
        NInfo=0
        do i=1,NSymm(KPhase)
          if(NInfo.gt.ubound(TextInfo,1)-4) cycle
          if(ICDDBatch) then
            TextInfo(i)=sta(i)
          else
            TextInfo(i)=' '
            do j=1,NDim(KPhase)
              TextInfo(i)=TextInfo(i)(:idel(TextInfo(i)))//Tabulator//
     1          SymmC(j,i,1,KPhase)(:idel(SymmC(j,i,1,KPhase)))
            enddo
            dls=max(dls,FeTxLength(TextInfo(i)))
          endif
          NInfo=NInfo+1
        enddo
        call FeBoldFont
        if(ICDDBatch) then
          NInfo=NInfo+1
          TextInfo(NInfo)='does not make a group.'
        else
          if(MameSymbol.gt.0) then
            dlp=dls
            NInfo=NInfo+1
            TextInfo(NInfo)='does not make a group. The program will '//
     1                      'try to use symmetry'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
            NInfo=NInfo+1
            TextInfo(NInfo)='as follows from the symbol.'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
          else
            NInfo=NInfo+1
            TextInfo(NInfo)='does not make a group. The program will '//
     1                      'reduce the symmetry'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
            NInfo=NInfo+1
            TextInfo(NInfo)='to trivial one. You should either '//
     1                      'correct manually the input CIF'
            TextInfoFlags(NInfo)='LB'
            dlp=max(dlp,FeTxLength(TextInfo(NInfo)))
            NInfo=NInfo+1
            TextInfo(NInfo)='file and import it once more or correct '//
     1                      'the file M50 by Jana tools.'
            TextInfoFlags(NInfo)='LB'
            dlp=max(dlp,FeTxLength(TextInfo(NInfo)))
            call FeNormalFont
          endif
        endif
        if(ICDDBatch) then
          call FeChybne(-1.,-1.,'the set of symmetry operations:',' ',
     1                  SeriousError)
          do i=1,NInfo
            if(LogLn.gt.0)
     1        write(LogLn,FormA) TextInfo(i)(:idel(TextInfo(i)))
            call FeLstWriteLine(TextInfo(i),-1)
          enddo
          CIFFFlag=1
        else
          if(dlp.gt.dls) then
            call FeTabsReset(UseTabs)
            pom=(dlp-dls)*.5
            do i=1,NDim(KPhase)
              call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
              pom=pom+dlk
            enddo
          endif
          call FeInfoOut(-1.,-1.,'The set of symmetry operations:','L')
        endif
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
        if(MameSymbol.gt.0) then
          NSymm(KPhase)=nso
          MaxNSymm=NSymm(KPhase)
          MaxNSymmN=NSymm(KPhase)
          NSymmN(KPhase)=NSymm(KPhase)
          NLattVec(KPhase)=nvto
          do i=1,NLattVec(KPhase)
            call CopyVek(vt6o(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
          enddo
          do i=1,NSymm(KPhase)
            call CopyMat(rm6o(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
            call CopyVek(s6o(1,i),s6(1,i,1,KPhase),NDim(KPhase))
            call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                    symmc(1,i,1,KPhase),0)
            call MatBlock3(rm6(1,i,1,KPhase),rm (1,i,1,KPhase),
     1                     NDim(KPhase))
          enddo
        else
          NSymm(KPhase)=1
          MaxNSymm=NSymm(KPhase)
          MaxNSymmN=NSymm(KPhase)
          NSymmN(KPhase)=NSymm(KPhase)
          NLattVec(KPhase)=1
          call UnitMat(rm (1,1,1,KPhase),3)
          call UnitMat(rm6(1,1,1,KPhase),3)
          call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1                  symmc(1,1,1,KPhase),0)
          ISwSymm(1,1,KPhase)=1
          ZMag(1,1,KPhase)=1.
          BratSymm(1,KPhase)=.true.
          call SetRealArrayTo( s6(1,1,1,KPhase),NDim(KPhase),0.)
          call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
        endif
        go to 2200
      endif
      if(MameSymbol.gt.0) then
        call FindSmbSg(t256,ChangeOrderNo,1)
        Nesouhlas=0
        nn=max(NSymm(KPhase),NLattVec(KPhase))
        allocate(EqSymm(nn))
        call SetIntArrayTo(EqSymm,nn,0)
        n=0
        do i=1,NLattVec(KPhase)
          do 2120j=1,nvto
            do m=1,NDim(KPhase)
              xp(m)=vt6(m,i,1,KPhase)-vt6o(m,j)
            enddo
            call od0do1(xp,xp,NDim(KPhase))
            do m=1,NDim(KPhase)
              if(abs(anint(xp(m))-xp(m)).gt..0001) go to 2120
            enddo
            n=n+1
            EqSymm(i)=j
2120      continue
        enddo
        if((n.ne.NLattVec(KPhase).or.nvto.gt.NLattVec(KPhase)).and.
     1     .not.EqIgCase(Lattice(KPhase),'X')) then
          Nesouhlas=1
          if(.not.ICDDBatch) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted',
     1                    'unknown')
          endif
          t256='some translation vectors following from the symbol '//
     1         'do not correspond'
          p256=' to those from operators or vice versa.'
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,t256,p256,SeriousError)
          else
            call Velka(t256(1:1))
            write(ln,FormA) t256(:idel(t256))
            write(ln,FormA) p256(:idel(p256))
          endif
          do i=1,NLattVec(KPhase)
            write(t256,'(''  #'',i2,'' from operators ('')') i
            do m=1,NDim(KPhase)
              call ToFract(vt6(m,i,1,KPhase),Cislo,6)
              t256=t256(:idel(t256))//Cislo(:idel(Cislo))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            j=EqSymm(i)
            if(j.gt.0) then
              write(Cislo,'(''#'',i2,'' from symbol'')') j
              t256=t256(:idel(t256))//' = '//Cislo(:idel(Cislo))
            endif
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLN.gt.0) write(LogLn,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
          enddo
          ii=0
          do 2130i=1,nvto
            do j=1,NLattVec(KPhase)
              if(EqSymm(j).eq.i) go to 2130
            enddo
            write(t256,'(''  #'',i2,'' from the symbol ('')') i
            do m=1,NDim(KPhase)
              call ToFract(vt6o(m,i),Cislo,6)
              t256=t256(:idel(t256))//Cislo(:idel(Cislo))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            if(ii.eq.0) then
              p256='  Translation vectors from the symbol not having '//
     1             'some correspondant:'
              if(ICDDBatch) then
                call FeLstWriteLine(p256,-1)
                if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
              else
                write(ln,FormA) p256(:idel(p256))
              endif
            endif
            ii=ii+1
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
2130      continue
        endif
        call SetIntArrayTo(EqSymm,nn,0)
        n=0
        do i=1,NSymm(KPhase)
          do j=1,nso
            if(.not.EqRV(rm6(1,i,1,KPhase),rm6o(1,j),NDimQ(KPhase),.01))
     1        cycle
            do 2140k=1,NLattVec(KPhase)
              call AddVek(s6(1,i,1,KPhase),vt6(1,k,1,KPhase),xp,
     1                    NDim(KPhase))
              call MultM(rm6(1,i,1,KPhase),ShSg(1,KPhase),xq,
     1                   NDim(KPhase),NDim(KPhase),1)
              do m=1,NDim(KPhase)
                xp(m)=xp(m)-s6o(m,j)-xq(m)+ShSg(m,KPhase)
              enddo
              call od0do1(xp,xp,NDim(KPhase))
              do m=1,NDim(KPhase)
                if(abs(anint(xp(m))-xp(m)).gt..0001) go to 2140
              enddo
              n=n+1
              EqSymm(i)=j
              exit
2140        continue
          enddo
        enddo
        if(n.ne.NSymm(KPhase).or.nso.gt.NSymm(KPhase)) then
          Nesouhlas=1
          if(.not.ICDDBatch) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted',
     1                    'unknown')
          endif
          t256='some symmetry operations following from the symbol '//
     1         'do not correspond'
          p256=' to those from operators or vice versa.'
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,t256,p256,SeriousError)
          else
            call Velka(t256(1:1))
            write(ln,FormA) t256(:idel(t256))
            write(ln,FormA) p256(:idel(p256))
          endif
          do i=1,NSymm(KPhase)
            write(t256,'(''  #'',i2,'' from operators ('')') i
            do m=1,NDim(KPhase)
              p256=symmc(m,i,1,KPhase)
              t256=t256(:idel(t256))//p256(:idel(p256))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            j=EqSymm(i)
            if(j.gt.0) then
              write(Cislo,'(''#'',i2,'' from symbol'')') j
              t256=t256(:idel(t256))//' = '//Cislo(:idel(Cislo))
            endif
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
          enddo
          ii=0
          do 2150i=1,nso
            do j=1,NSymm(KPhase)
              if(EqSymm(j).eq.i) go to 2150
            enddo
            write(t256,'(''  #'',i2,'' from the symbol ('')') i
            call CodeSymm(rm6o(1,i),s6o(1,i),SymmCP,0)
            do m=1,NDim(KPhase)
              t256=t256(:idel(t256))//SymmCP(m)(:idel(SymmCP(m)))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            if(ii.eq.0) then
              p256='  Symmetry operations from the symbol not having '//
     1             'some correspondant:'
              if(ICDDBatch) then
                call FeLstWriteLine(p256,-1)
                if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
              else
                write(ln,FormA) p256(:idel(p256))
              endif
            endif
            ii=ii+1
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
2150      continue
        endif
        deallocate(EqSymm)
        if(Nesouhlas.ne.0) then
          if(ICDDBatch) then
            CIFFlag=1
          else
            call CloseIfOpened(ln)
            NInfo=2
            TextInfo(1)='These was a disagreement between symmetry '//
     1                  'operations as'
            TextInfo(2)='derived from the symbol and those read in '//
     1                  'from the CIF file.'
            t256=fln(:ifln)//'_list.tmp'
            if(FeYesNoHeader(-1.,-1.,'Do you want to see more details?',
     1         1)) call FeListView(t256,0)
            call DeleteFile(t256)
          endif
        endif
      endif
2200  if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
2250  call CopyVek(CellPar(1,1,KPhase),CellParPom,6)
      if(CrSystem(KPhase).eq.CrSystemTriclinic.or.
     1   CrSystem(KPhase).eq.0) then
        call SetIntArrayTo(i90,3,0)
      else
        call SetIntArrayTo(i90,3,1)
      endif
      lpom=isPowder
      call FindSmbCentr(Lattice(KPhase),1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      isPowder=lpom
      call EM50CellSymmTest(0,ich)
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      call EM50MakeStandardOrder
      call CopyVek(CellParPom,CellPar(1,1,KPhase),6)
      call CIFGetReal(CifKey(51,10),LamAve(1),sa,t256,ich)
      if(ich.le.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        if(ReadMagneticCIF) then
          LamAve(1)=1.
        else
          LamAve(1)=LamAveD(6)
        endif
      endif
      nlam=1
      call CIFGetSt(CifKey(48,10),sta,mm,t256,ich)
      if(ich.eq.-1)  then
        if(ReadMagneticCIF) then
          p256='neutron'
        else
          p256=' '
        endif
      else
        p256=sta(1)
      endif
      if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
      call mala(p256)
      if(EqIgCase(p256,'neutron')) then
        Radiation(KDatBlock)=NeutronRadiation
      else
        Radiation(KDatBlock)=XRayRadiation
        j=LocateInArray(LamAve(1),LamAveD,7,.0001)
        KLam(1)=j
        if(j.eq.0) then
          LamA1(1)=LamAve(1)
          LamA2(1)=LamAve(1)
          LamRat(1)=0.
          NAlfa(1)=1
        else
          LamA1(1)=LamA1D(j)
          LamA2(1)=LamA2D(j)
          LamRat(1)=LamRatD(j)
          NAlfa(1)=2
        endif
      endif
      call CIFGetReal(CifKey(4,5),pom,spom,t256,ich)
      NUnits(KPhase)=nint(pom)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        NUnits(KPhase)=1
      endif
      FullMultiplicity=NSymmN(KPhase)*NLattVec(KPhase)
      pom=1./float(FullMultiplicity)
      if(allocated(AtMultP)) deallocate(AtMultP)
      allocate(AtMultP(333))
      call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(3,3),ia,Sta,
     1                AtMultP,spa,1,NAtFormulaP,t256,ich)
      if(ich.eq.-1) then
        p256=' '
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,p256,ia,Sta,
     1                  AtMultP,spa,1,NAtFormulaP,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
          if(ich.eq.-2)
     1      call CIFGetSt(CifKey(22,6),sta,mm,p256,ich)
          t256=sta(1)
          if(ich.eq.-1.or.t256.eq.'?') then
            call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
            t256=sta(1)
          endif
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.-1) then

          else
            call PitChemFormula(t256,StLbl,AtMultP,NAtFormulaP)
            if(NAtFormulaP.lt.0) go to 9000
            pom=1.
          endif
        else
          call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
          t256=sta(1)
          if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.eq.-1) then
            call CIFGetSt(CifKey(22,6),sta,mm,p256,ich)
            t256=sta(1)
          else
            if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
          endif
          if(ich.eq.0) then
            call PitChemFormula(t256,StLbl,AtMultP,NAtFormulaP)
            if(NAtFormulaP.lt.0) go to 9000
            pom=1.
          else
            do i=1,NAtFormulaP
              AtMultP(i)=FullMultiplicity
            enddo
          endif
        endif
      else if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      endif
      if(NPhase.le.1) then
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=NAtFormulaP
        if(n.le.0) n=15
        m=121
        i=max(1,NDatBlock)
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4           AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7           FFra(n,NPhase,i),FFia(n,NPhase,i),
     8           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9           TypeFFMag(n,NPhase),
     a           FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1           PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2           ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3           HNSlater(n,NPhase),HZSlater(n,NPhase),
     4           ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5           NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6           CoreValSource(n,NPhase))
      else
        call ReallocFormF(NAtFormulaP,NPhase,NDatBlock)
      endif
      FFCoreD=0.
      FFValD=0.
      CoreValSource='Default'
      ExistXRayData=.true.
      NAtFormula(KPhase)=NAtFormulaP
      call SetRealArrayTo(TypicalDist(1,1,KPhase),NAtFormulaP**2,-1.)
      do i=1,NAtFormula(KPhase)
        AtTypeFull(i,KPhase)=StLbl(i)
        AtTypeMag(i,KPhase)=' '
        AtTypeMagJ(i,KPhase)=' '
        CoreValSource(i,KPhase)='Default'
        call GetPureAtType(StLbl(i),AtType(i,KPhase))
        call uprat(AtTypeFull(i,KPhase))
        call uprat(AtType(i,KPhase))
        AtMult(i,KPhase)=AtMultP(i)*pom
        FFType(KPhase)=-62
      enddo
      call EM50ReadAllFormFactors
      call EM50FormFactorsSet
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(17,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffra(j,KPhase,KDatBlock)=pa(i)
          enddo
        endif
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(16,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffia(j,KPhase,KDatBlock)=pa(i)
          enddo
        endif
        k=1
        do i=1,9
          call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(i+6,3),ia,
     1                    Sta,pa,spa,1,n,t256,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.-1) then
            if(i.eq.1) then
              go to 2350
            else
              go to 9000
            endif
          endif
          do j=1,NAtFormula(KPhase)
            FFBasic(k,j,KPhase)=pa(j)
          enddo
          if(i.eq.1) FFType(KPhase)=-9
          if(i.ne.4) then
            k=k+2
          else
            k=2
          endif
          if(k.gt.9) k=9
        enddo
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(18,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffn(j,KPhase)=pa(i)
          enddo
        endif
        if(MagneticType(KPhase).ne.0) then
          call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(56,3),ia,
     1                    Sta,pa,spa,0,n,t256,ich)
          if(ich.le.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.0) then
            do i=1,n
              j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                              StLbl(i),IgnoreCaseYes)
              if(j.ne.0) then
                if(EqIgCase(sta(i),'.')) then
                  AtTypeMag(j,KPhase)=' '
                  AtTypeMagJ(j,KPhase)=' '
                else
                  AtTypeMag(j,KPhase)=sta(i)
                endif
              endif
            enddo
          endif
        endif
      endif
2350  call SetFormula(Formula(KPhase))
      lpom=isPowder
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      if(NPhase.gt.1) then
        deallocate(isa)
        allocate(isa(MaxNSymm,NAtAll))
        do i=1,NAtAll
          do j=1,MaxNSymm
            isa(j,i)=j
          enddo
        enddo
      endif
      isPowder=lpom
      ExistM50=.true.
      if(CIFFlag.ne.0.and.ICDDBatch) then
        p256='There were serious error in reading of that CIF file.'
        call FeLstWriteLine(p256,-1)
        if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
        p256='Please make correction and submit the task again.'
        call FeLstWriteLine(p256,-1)
        if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
        go to 9900
      endif
      if(KPhase.le.1) then
        ItfMax=0
        MagParMax=0
        IFrMax=0
      endif
      s256=CIFKey(33,1)
      do i=1,3
        p256=CIFKey(29+i,1)
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,nac,t256,
     1                  ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(i.eq.1) then
          if(NPhase.le.1) then
            call AllocateAtoms(nac)
          else
            call ReallocateAtoms(nac)
          endif
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+nac)
          call EM40UpdateAtomLimits
        endif
        if(nac.gt.0) then
          jj=NAtIndFrAll(KPhase)-1
          do j=1,nac
            if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
            jj=jj+1
            if(i.eq.1) call SetBasicKeysForAtom(jj)
            Atom(jj)=StLbl(j)
            call uprat(Atom(jj))
            if(KPhase.gt.1) Atom(jj)=Atom(jj)(:idel(Atom(jj)))//
     1                               StPh(:idel(StPh))
            do k=1,MaxNSymm
              isa(k,jj)=k
            enddo
            itf(jj)=1
            ifr(jj)=0
            itfMax=1
            lasmax(jj)=0
            x(i,jj)= pa(j)
            sx(i,jj)=spa(j)
            iswa(jj)=1
            if(jj.eq.1) then
              PrvniKiAtomu(jj)=ndoff
            else
              PrvniKiAtomu(jj)=PrvniKiAtomu(jj-1)+mxda
            endif
            DelkaKiAtomu(jj)=10
          enddo
          NAtIndLen(1,KPhase)=jj-NAtIndFrAll(KPhase)+1
        else
          go to 9999
        endif
        call EM40UpdateAtomLimits
      enddo
      if(NDimI(KPhase).gt.0) then
        do i=1,nac
          if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
          call qbyx(x(1,i),qcnt(1,i),1)
        enddo
      endif
      if(NComp(KPhase).gt.1) then
        call CIFGetLoop(CIFKey(33,1),StLbl,MxLoop,CIFKey(52,1),ia,
     1                  Sta,pa,spa,0,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do 2400i=1,n
            do j=1,NComp(KPhase)
              if(EqIgCase(Sta(i),IdCode(j))) then
                iswa(i)=j
                call qbyx(x(1,i),qcnt(1,i),j)
                go to 2400
              endif
            enddo
            t256='the item "_atom_site_subsystem_code" for '//
     1           'the atom "'//Atom(i)(:idel(Atom(i)))//'"'
            p256='doesn''t correspond to any of composite subsystems.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
2400      continue
        endif
      endif
      do i=1,NComp(KPhase)
        n=0
        do j=1,nac
          if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
          if(iswa(j).eq.i) n=n+1
        enddo
        NAtIndLen(i,KPhase)=n
      enddo
      do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        kswa(j)=KPhase
        call SetRealArrayTo(xp,3,0.)
        call SpecPos(x(1,j),xp,0,iswa(j),.01,nocc)
        ai(j)=1./float(nocc)
        sai(j)=0.
      enddo
      call EM40UpdateAtomLimits
      allocate(aip(nac),saip(nac))
      call SetRealArrayTo(aip,nac,1.)
      call SetRealArrayTo(saip,nac,0.)
      s256=CIFKey(33,1)
      p256=CIFKey(41,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        call SetRealArrayTo(aip,nac,1.)
        call SetRealArrayTo(saip,nac,0.)
      else
        call CopyVek(pa,aip,nac)
        call CopyVek(spa,saip,nac)
      endif
      p256=CIFKey(41,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.ne.-1) then
        jj=NAtIndFrAll(KPhase)-1
        do i=1,n
          jj=jj+1
          WyckoffSmb(jj)=Sta(i)
        enddo
      endif
      jj=NAtIndFrAll(KPhase)-1
      do i=1,nac
         jj=jj+1
         WyckoffMult(jj)=-1
         if(NDimI(KPhase).le.0) then
           ai(jj)=ai(jj)* aip(i)
          sai(jj)=ai(jj)*saip(i)
        endif
      enddo
      p256=CIFKey(3,4)
      ZShelx=.false.
      call CIFGetSt(p256,sta,mm,t256,ich)
      if(ich.eq.0)
     1 ZShelx=LocateSubstring(sta(1),'SHELX', .false.,.true.).gt.0
      jj=NAtIndFrAll(KPhase)-1
      do ii=1,3
        if(ii.eq.1) then
          p256=CIFKey(44,1)
        else if(ii.eq.2) then
          p256=CIFKey(106,1)
        else if(ii.eq.3) then
          p256=CIFKey(107,1)
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,
     1                  ich)
        if(ich.eq.-1) then
          cycle
        else if(ich.eq.2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else
          j=0
          jj=NAtIndFrAll(KPhase)-1
          if(NDimI(KPhase).eq.1) then
            pom=float(NSymmN(KPhase))/float(NSymm(KPhase))
          else
            pom=1.
          endif
          do iat=1,n
            if(StLbl(iat).eq.'?'.or.StLbl(iat).eq.'0') cycle
            jj=jj+1
            WyckoffMult(jj)=nint(pa(iat))
            if(NDimI(KPhase).gt.0) cycle
!            ai(jj)=ai(jj)*aip(iat)
            if(ii.eq.2.or.(ii.eq.1.and..not.ZShelx)) then
               aip(iat)= aip(iat)* pa(iat)/float(FullMultiplicity)
              saip(iat)=saip(iat)*spa(iat)/float(FullMultiplicity)
            else
               aip(iat)= aip(iat)/pa(iat)
              saip(iat)=saip(iat)/pa(iat)**2*spa(iat)
            endif
            if(MagneticType(KPhase).ne.0) then
              aipi=aip(iat)*pom
            else
              aipi=aip(iat)
            endif
            if(abs(ai(jj)-aipi).gt..0001) then
              j=j+1
              if(j.eq.1) then
                t256=Atom(i)
                t256='the item "_atom_site_symmetry_multiplicity" '//
     1               'for the atom "'//Atom(jj)(:idel(Atom(jj)))//'"'
                p256='is not in accordance with the derived site '//
     1               'symmetry.'
                call FeChybne(-1.,-1.,t256,p256,Warning)
              endif
            endif
          enddo
          if(j.gt.0) then
            if(j.gt.1) then
              write(Cislo,FormI15) j
              call Zhusti(Cislo)
              t256='the previous error has occured '//
     1             Cislo(:idel(Cislo))//' times.'
            else
              t256='the previous error has occured just once.'
            endif
            p256='It will not have an influence to subsequest work.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
          endif
          go to 2450
        endif
      enddo
2450  p256=CIFKey(46,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
      if(ich.eq.-1) then
        jj=NAtIndFrAll(KPhase)-1
        do i=1,nac
          if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
          Sta(i)=' '
          jj=jj+1
          do j=1,idel(Atom(jj))
            znak=Atom(jj)(j:j)
            if(index(Cifry(1:10),znak).gt.0.or.Znak.eq.'('.or.
     1         Znak.eq.'_') go to 2490
            if(j.le.2) then
              Sta(i)(j:j)=znak
            else if(j.eq.3.and.EqIgCase(Sta(i),'wat')) then
              Sta(i)(j:j)='O'
              go to 2490
            else
              Sta(i)=' '
              go to 2490
            endif
          enddo
2490      if(idel(Sta(i)).le.0) Sta(i)=AtType(1,KPhase)
        enddo
      endif
      nfo=NAtFormula(KPhase)
      jj=NAtIndFrAll(KPhase)-1
      do i=1,nac
        if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
        jj=jj+1
        call SetRealArrayTo( beta(1,jj),6,0.)
        call SetRealArrayTo(sbeta(1,jj),6,0.)
        sta(i)=sta(i)(1:2)
        call uprat(sta(i))
        if(index(cifry(1:10),sta(i)(2:2)).gt.0.or.sta(i)(2:2).eq.'+'.or.
     1           sta(i)(2:2).eq.'-') sta(i)(2:)=' '
        do j=1,NAtFormula(KPhase)
          if(EqIgCase(sta(i),AtType(j,KPhase))) go to 2530
        enddo
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
        call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
        AtTypeFull(NAtFormula(KPhase),KPhase)=sta(i)
        AtTypeMag(NAtFormula(KPhase),KPhase)=' '
        AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
        AtVal(NAtFormula(KPhase),KPhase)=0
        CoreValSource(NAtFormula(KPhase),KPhase)='Default'
!        write(Cislo,'(2i4)') NAtFormula(KPhase)
!        call FeWinMessage(CoreValSource(1,KPhase),
!     1                    Cislo)
        call SetRealArrayTo(TypicalDist(1,1,KPhase),
     1                      NAtFormula(KPhase)**2,-1.)
        call GetPureAtType(sta(i),AtType(NAtFormula(KPhase),KPhase))
        call uprat(AtTypeFull(NAtFormula(KPhase),KPhase))
        call uprat(AtType(NAtFormula(KPhase),KPhase))
        AtMult(NAtFormula(KPhase),KPhase)=1.
        call EM50ReadOneFormFactor(NAtFormula(KPhase))
        call EM50OneFormFactorSet(NAtFormula(KPhase))
        j=LocateInStringArray(atn,98,AtType(NAtFormula(KPhase),KPhase),
     1                        IgnoreCaseYes)
        if(j.gt.0) then
          AtNum(NAtFormula(KPhase),KPhase)=float(j)
        else
          AtNum(NAtFormula(KPhase),KPhase)=0.
        endif
        isf(jj)=NAtFormula(KPhase)
        cycle
2530    isf(jj)=j
      enddo
!      call FeWinMessage('Pred iom50',' ')
!      do i=1,NAtFormula(KPhase)
!        call FeWinMessage(CoreValSource(i,KPhase),AtTypeFull(i,KPhase))
!      enddo
      if(NAtFormula(KPhase).ne.nfo) then
        call SetFormula(Formula(KPhase))
        lpom=isPowder
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        isPowder=lpom
      endif
      MagPar(NAtIndFrAll(KPhase):NAtIndFrAll(KPhase))=0
      p256=CIFKey(21,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.ne.-1) then
        j=0
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          j=j+1
          if(EqIgCase(Sta(j),'dum')) then
            ai(i)=0.
            sai(i)=0.
          endif
        enddo
      endif
      do m=1,2
        if(m.eq.1) then
          s256=CIFKey(48,1)
          pom=episq
        else
          s256=CIFKey(19,1)
          pom=1.
        endif
        call CIFGetLoop(CIFKey(33,1),StLbl,MxLoop,s256,ia,Sta,pa,spa,
     1                  1,n,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        jj=NAtIndFrAll(KPhase)-1
        do i=1,n
          jj=jj+1
          if(ich.eq.0) then
             beta(1,jj)= pa(i)*pom
            sbeta(1,jj)=spa(i)*pom
          endif
        enddo
      enddo
      l=idel(CIFKey(11,1))-2
      PomMx=0.
      n=0
      do m=1,2
        if(m.eq.1) then
          s256=CIFKey(11,1)(:l)
        else
          s256=CIFKey(2,1)(:l)
        endif
        do i=1,6
          if(m.eq.2) then
            if(KlicUsed.eq.ProHonzu) then
              pom=.0001
            else
              pom=1.
            endif
          endif
          call indext(i,j,k)
          write(p256,'(2i1)') j,k
          p256=s256(:l)//p256(1:2)
          call CIFGetLoop(CIFKey(8,1),StLbl,MxLoop,p256,ia,Sta,pa,
     1                    spa,1,nac,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(m.eq.1) pom=urcp(i,iswa(j),KPhase)
            do k=1,nac
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle
              if(EqIgCase(StLbl(k),Atom(j))) then
                if(i.eq.1) then
                  itf(j)=2
                  itfMax=2
                endif
                 beta(i,j)= pa(k)*pom
                sbeta(i,j)=spa(k)*pom
                if(i.le.3) then
                  PomMx=PomMx+abs(beta(i,j)/urcp(i,iswa(j),KPhase))
                  n=n+1
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      if(n.gt.0) PomMx=PomMx/float(n)
      if(PomMx.gt..2) then
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          if(itf(i).gt.1) then
            itfMax=2
            do j=1,6
               beta(j,i)= beta(j,i)/episq
              sbeta(j,i)=sbeta(j,i)/episq
            enddo
          endif
        enddo
      endif
      call SetIntArrayTo(kmodn,7,0)
      call SetIntArrayTo(KModAMax,7,0)
      m=81
c      mm=82
      n=3
      do n=3,6
        mm=m+1
        m=mm+1
        do i=1,TRank(n)
          m=m+1
          call CIFGetLoop(CIFKey(mm,22),StLbl,MxLoop,CIFKey(m,22),ia,
     1                    Sta,pa,spa,1,nac,t256,ich)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            do k=1,nac
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle
              if(EqIgCase(StLbl(k),Atom(j))) then
                if(i.eq.1) then
                  itf(j)=n
                  itfMax=max(itfMax,n)
                  call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,
     1                                      0)
                endif
                if(n.eq.3) then
                   c3(i,j)= pa(k)
                  sc3(i,j)=spa(k)
                else if(n.eq.4) then
                   c4(i,j)= pa(k)
                  sc4(i,j)=spa(k)
                else if(n.eq.5) then
                   c5(i,j)= pa(k)
                  sc5(i,j)=spa(k)
                else if(n.eq.6) then
                   c6(i,j)= pa(k)
                  sc6(i,j)=spa(k)
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        ifr(i)=0
        TypeModFun(i)=0
        call SetIntArrayTo(KFA  (1,i),7,0)
        call SetIntArrayTo(KModA(1,i),7,0)
        call SetIntArrayTo(KModAO(1,i),7,0)
        call SetIntArrayTo(KiA(1,i),mxda,0)
      enddo
      call SetIntArrayTo(KModMMax,3,0)
      if(NDimI(KPhase).le.0) go to 5000
      call SetRealArrayTo(qup,3*mxw,0.)
      n=1
      do i=1,3
        call CIFGetLoop(CIFKey(64,n),StLbl,MxLoop,CIFKey(64+i,n),ia,
     1                  Sta,pa,spa,1,nn,t256,ich)
        if(ich.eq.-1) then
          if(i.eq.1) then
            go to 3150
          else
            cycle
          endif
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
        jj=0
        do j=1,nn
          if(StLbl(j).eq.'?') cycle
          jj=jj+1
          StLbl(jj)=StLbl(j)
          qup(i,jj)=pa(j)
        enddo
        nn=jj
      enddo
      kwi=0
      imn(:NDimI(KPhase))=-mxw
      imn(NDimI(KPhase)+1:)=0
      imx(:NDimI(KPhase))= mxw
      imx(NDimI(KPhase)+1:)=0
      m=NDimI(KPhase)
      do 3100i=1,nn
        mrtl=999999
        mrto=999999
        do isw=1,NComp(KPhase)
          do i1=imn(1),imx(1)
            mrt(1)=i1
            do i2=imn(2),imx(2)
              mrt(2)=i2
              do 3050i3=imn(3),imx(3)
                mrt(3)=i3
                do j=1,3
                  pa(j)=-qup(j,i)
                  do k=1,NDimI(KPhase)
                    pa(j)=pa(j)+float(mrt(k))*qu(j,k,isw,KPhase)
                  enddo
                  if(abs(anint(pa(j))-pa(j)).gt..0001) go to 3050
                enddo
                do j=2,NLattVec(KPhase)
                  pom=ScalMul(pa,vt6(1,j,isw,KPhase))
                  if(abs(anint(pom)-pom).gt..0001) go to 3050
                enddo
                j=iabs(i1)+iabs(i2)+iabs(i3)
                if(j.gt.mrtl) go to 3050
                mrtl=j
                call CopyVekI(mrt,mrto,3)
3050          continue
            enddo
          enddo
        enddo
        if(mrtl.lt.999999) then
          k=0
          call StToInt(StLbl(i),k,ia,1,.false.,ich)
          if(ich.ne.0) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 3100
          endif
          do k=1,m
            if(eqiv(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=k
              go to 3100
            endif
            if(eqiv(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=k
              go to 3100
            else if(eqivm(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=-k
              go to 3100
            endif
          enddo
          m=m+1
          call CopyVekI(mrto,kw(1,m,KPhase),3)
          kwi(ia(1))=k
          go to 3100
        endif
        write(s256,'(3f10.4)') qup(1:3,i)
        call ZdrcniCisla(s256,3)
        do j=1,idel(s256)
          if(s256(j:j).eq.' ') s256(j:j)=','
        enddo
        s256='The wave vector ('//s256(:idel(s256))//') is not an '//
     1       'integer combination of modulation vectors'
        call FeChybne(-1.,-1.,s256,' ',SeriousError)
        CIFFlag=1
3100  continue
      go to 3200
3150  call SetIntArrayTo(kwp,3*mxw,0)
      kwi=0
      kk=0
3160  if(kk.eq.0) then
        s256=CifKey(64,n)
        ik=52
        im=22
      else
        ik=144
        im=1
      endif
      do i=1,NDimI(KPhase)
        call CIFGetLoop(s256,StLbl,MxLoop,CIFKey(ik+i,im),ia,
     1                  Sta,pa,spa,-1,nn,t256,ich)
        if(kk.eq.0) then
          kk=kk+1
          go to 3160
        endif
        if(ich.ne.0) then
          do j=1,mxw
            kwi(j)=j
          enddo
          go to 3200
        endif
        jj=0
        do j=1,nn
          if(StLbl(j).eq.'?') cycle
          jj=jj+1
          StLbl(jj)=StLbl(j)
          kwp(i,jj)=ia(j)
        enddo
        nn=jj
      enddo
      ii=0
      do i=1,nn
        k=0
        call kus(StLbl(i),k,Cislo)
        call posun(Cislo,0)
        read(Cislo,FormI15) k
        if(k.eq.0) cycle
        ii=ii+1
        do j=1,NDimI(KPhase)
          kw(j,k,KPhase)=kwp(j,i)
        enddo
        kwi(ii)=k
      enddo
3200  n=22
      do i=1,3
        call CIFGetLoop(CIFKey(49,n),StLbl,MxLoop,CIFKey(49+i,n),ia,
     1                  Sta,pa,spa,1,nn,t256,ich)
        if(i.eq.1) then
          norp=nn
          allocate(OrX40P(norp),OrDelP(norp),OrEpsP(norp))
          call SetRealArrayTo(OrX40P,norp,0.)
          call SetRealArrayTo(OrDelP,norp,1.)
          call SetRealArrayTo(OrEpsP,norp,.95)
          call CopyVek(pa,OrX40P,nn)
        else if(i.eq.2) then
          call CopyVek(pa,OrDelP,min(nn,norp))
        else if(i.eq.3) then
          call CopyVek(pa,OrEpsP,min(nn,norp))
        endif
      enddo
      do j=1,2
        call CIFGetLoop(CIFKey(103,1),StLbl,MxLoop,CIFKey(103+j,1),
     1                  ia,Sta,pa,spa,1,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          go to 3320
        else
          do i=1,n
            if(KPhase.gt.1)
     1        StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,StLbl(i))
            if(iat.le.0) cycle
            if(j.eq.1) kmodn(1)=max(KModA(1,iat),1)+1
            call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
            KModP=KModA(1,iat)
            if(j.eq.1) then
              ax(KModP+1,iat)=pa(i)
              ay(KModP+1,iat)=0.
              sax(KModP+1,iat)=0.
              say(KModP+1,iat)=0.
            else if(j.eq.2) then
              KFA(1,iat)=1
              KModA(1,iat)=KModP+1
               a0(iat)=pa(i)
              sa0(iat)=0.
            endif
          enddo
        endif
      enddo
3320  n=0
      call SetIntArrayTo(il,10,0)
      do i=1,9
        call CIFGetSt(CifKey(i+53,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(54,1),p256,t256,ich)
      if(ich.eq.-1) go to 3500
      call SetIntArrayTo(iwa,20,0)
3350  k=0
      it=0
      iw=0
      do 3400i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3500
        j=il(i)
        if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.2) then
          call mala(t256)
          do ix=1,3
            if(EqIgCase(t256,Smbx(ix))) go to 3400
          enddo
          ix=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.5) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.7) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.8) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.9) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.4) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=3360) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
3360      iw=-999
        endif
3400  continue
      if(ix.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        kmodn(2)=max(KModA(2,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(2,iat)=kmodn(2)-1
        if(iwa(iwabs).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( ux(1,iwabs,i),3,0.)
            call SetRealArrayTo( uy(1,iwabs,i),3,0.)
            call SetRealArrayTo(sux(1,iwabs,i),3,0.)
            call SetRealArrayTo(suy(1,iwabs,i),3,0.)
          enddo
          iwa(iwabs)=1
        endif
        call CIFSetMod(ux(ix,iwabs,iat),uy(ix,iwabs,iat),
     1                 sux(ix,iwabs,iat),suy(ix,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) ux(ix,iw,iat)=-ux(ix,iw,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        if(KFA(1,iat).eq.1) then
          if(TypeModFun(iat).ne.1) then
            DelejOrtho=.true.
            ifun=0
            do i=1,norp
              if(abs(a0(iat)-OrDelP(i)).lt..0001.and.
     1           abs(ax(1,iat)-OrX40P(i)).lt..0001) then
                ifun=i
                TypeModFun(iat)=1
                OrthoX40(iat)=OrX40P(ifun)
                OrthoDelta(iat)=OrDelP(ifun)
                OrthoEps(iat)=OrEpsP(ifun)
                exit
              endif
            enddo
          endif
          if(TypeModFun(iat).eq.1) then
            x(ix,iat)=x(ix,iat)+pcos
          endif
        endif
      else if(iw.lt.-900.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ix.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 3350
3500  do 3600ityp=1,2
        if(ityp.eq.1) then
          ilm=4
          ilp=24
        else
          ilm=5
          ilp=35
        endif
        n=0
        call SetIntArrayTo(il,10,0)
        do i=1,ilm
          call CIFGetSt(CifKey(ilp+i-1,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
            if(ityp.eq.1.and.i.gt.2) il(ich)=il(ich)+1
          endif
        enddo
        call CIFBeginLoop(m,CIFKey(ilp,22),p256,t256,ich)
        if(ich.eq.-1) go to 3600
3550    k=0
        iwp=0
        ifun=0
        do 3590i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3600
          j=il(i)
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,t256)
          else if(j.eq.2) then
            do ix=1,3
              if(EqIgCase(t256,Smbx(ix))) go to 3590
            enddo
            ix=0
            if(t256(1:1).ne.'?') then
              ich=-2
              t256=p256
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          else if(j.eq.3) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3560) ifun
            cycle
3560        ifun=0
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3570) iwp
            cycle
3570        iwp=0
          else if(j.eq.5) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
3590    continue
        if(ix.gt.0.and.iwp.gt.0) then
          iw=(iwp-1)/2+1
          kmodn(2)=max(KModA(2,iat),iw)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(2,iat)=kmodn(2)-1
          if(ityp.eq.1) then
            TypeModFun(iat)=2
          else
            TypeModFun(iat)=1
            OrthoX40(iat)=OrX40P(ifun)
            OrthoDelta(iat)=OrDelP(ifun)
            OrthoEps(iat)=OrEpsP(ifun)
          endif
          if(iwa(iw).le.0) then
            do i=1,NAtCalc
              call SetRealArrayTo( ux(1,iw,i),3,0.)
              call SetRealArrayTo( uy(1,iw,i),3,0.)
              call SetRealArrayTo(sux(1,iw,i),3,0.)
              call SetRealArrayTo(suy(1,iw,i),3,0.)
            enddo
            iwa(iw)=1
          endif
          if(mod(iwp,2).eq.1) then
             ux(ix,iw,iat)= pom
            sux(ix,iw,iat)=spom
          else
             uy(ix,iw,iat)= pom
            suy(ix,iw,iat)=spom
          endif
        endif
        call CIFNewRecord(m,p256,*4300)
        go to 3550
3600  continue
      call SetIntArrayTo(il,10,0)
      n=0
      do i=1,9
        call CIFGetSt(CifKey(i+87,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(88,1),p256,t256,ich)
      if(ich.eq.-1) go to 3900
      call SetIntArrayTo(iwa,20,0)
3750  k=0
      it=0
      iw=0
      do 3800i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3900
        j=il(i)
        if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.5) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.7) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.8) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.9) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.3) then
          do ib=1,6
            if(EqIgCase(t256,Smbu(ib))) go to 3800
          enddo
          ib=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.4) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=3760) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
3760      iw=-999
        endif
3800  enddo
      if(ib.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        pom=urcp(ib,iswa(iat),KPhase)
        if(it.eq.0) then
          psin=psin*pom
          pcos=pcos*pom
          ssin=ssin*pom
          scos=scos*pom
        else
          pmod=pmod*pom
          smod=smod*pom
        endif
        kmodn(3)=max(KModA(3,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(3,iat)=kmodn(3)-1
        if(iwa(iwabs).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( bx(1,iwabs,iat),6,0.)
            call SetRealArrayTo( by(1,iwabs,iat),6,0.)
            call SetRealArrayTo(sbx(1,iwabs,iat),6,0.)
            call SetRealArrayTo(sby(1,iwabs,iat),6,0.)
          enddo
          iwa(iwabs)=1
        endif
        call CIFSetMod(bx(ib,iwabs,iat),by(ib,iwabs,iat),
     1                 sbx(ib,iwabs,iat),sby(ib,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) bx(ib,iwabs,iat)=-bx(ib,iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        if(KFA(1,iat).eq.1) then
          if(TypeModFun(iat).ne.1) then
            DelejOrtho=.true.
            ifun=0
            do i=1,norp
              if(abs(a0(iat)-OrDelP(i)).lt..0001.and.
     1           abs(ax(1,iat)-OrX40P(i)).lt..0001) then
                ifun=i
                TypeModFun(iat)=1
                OrthoX40(iat)=OrX40P(ifun)
                OrthoDelta(iat)=OrDelP(ifun)
                OrthoEps(iat)=OrEpsP(ifun)
                exit
              endif
            enddo
          endif
          if(TypeModFun(iat).eq.1) then
            write(t256,'(i5,3f10.6)')
             beta(ib,iat)=beta(ib,iat)+pcos*urcp(ib,iswa(iat),KPhase)
          endif
        endif
      else if(iw.lt.-900.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ib.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 3750
3900  do 4000ityp=1,2
        if(ityp.eq.1) then
          ilm=4
          ilp=31
        else
          ilm=5
          ilp=44
        endif
        n=0
        call SetIntArrayTo(il,10,0)
        do i=1,ilm
          call CIFGetSt(CifKey(ilp+i-1,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
            if(ityp.eq.1.and.i.gt.2) il(ich)=il(ich)+1
          endif
        enddo
        write(Cislo,'(i4)') n
        call CIFBeginLoop(m,CIFKey(ilp,22),p256,t256,ich)
        if(ich.eq.-1) go to 4100
3950    k=0
        iwp=0
        ifun=0
        do 3990i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4000
          j=il(i)
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(Atom,NAtCalc,t256)
          else if(j.eq.2) then
            do ib=1,6
              if(EqIgCase(t256,Smbu(ib))) go to 3990
            enddo
            ib=0
            if(t256(1:1).ne.'?') then
              ich=-2
              t256=p256
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          else if(j.eq.3) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3960) ifun
            cycle
3960        ifun=0
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3970) iwp
            cycle
3970        iwp=0
          else if(j.eq.5) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
3990    enddo
        if(ib.gt.0.and.iwp.gt.0) then
          iw=(iwp-1)/2+1
          kmodn(3)=max(KModA(3,iat),iw)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(3,iat)=kmodn(3)-1
          if(ityp.eq.1) then
            TypeModFun(iat)=2
          else
            TypeModFun(iat)=1
            OrthoX40(iat)=OrX40P(ifun)
            OrthoDelta(iat)=OrDelP(ifun)
            OrthoEps(iat)=OrEpsP(ifun)
          endif
          if(iwa(iw).le.0) then
            do i=1,NAtCalc
              call SetRealArrayTo( bx(1,iw,i),3,0.)
              call SetRealArrayTo( by(1,iw,i),3,0.)
              call SetRealArrayTo(sbx(1,iw,i),3,0.)
              call SetRealArrayTo(sby(1,iw,i),3,0.)
            enddo
            iwa(iw)=1
          endif
          pomp=urcp(ib,iswa(iat),KPhase)
          if(mod(iwp,2).eq.1) then
             bx(ib,iw,iat)= pom*pomp
            sbx(ib,iw,iat)=spom*pomp
          else
             by(ib,iw,iat)= pom*pomp
            sby(ib,iw,iat)=spom*pomp
          endif
        endif
        call CIFNewRecord(m,p256,*4300)
        go to 3950
4000  continue
4100  call CIFGetLoop(CIFKey(200,22),StLbl,MxLoop,CIFKey(201,22),
     1                  ia,Sta,pa,spa,1,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        a0=-3333.
        go to 4150
      else
        do i=1,n
          if(KPhase.gt.1)
     1      StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,StLbl(i))
          if(iat.le.0) cycle
            a0(iat)= pa(i)
           sa0(iat)=spa(i)
        enddo
      endif
4150  n=0
      call SetIntArrayTo(il,10,0)
      do i=1,8
        call CIFGetSt(CifKey(i+67,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(68,1),p256,t256,ich)
      if(ich.eq.-1) go to 4300
      call SetIntArrayTo(iwa,20,0)
4200  k=0
      it=0
      iw=0
      do i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4300
        j=il(i)
        if(j.eq.4.or.j.eq.6.or.j.eq.7.or.j.eq.8) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.4) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.6) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.7) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.8) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.3) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=4240) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
4240      iw=-999
        endif
      enddo
      if(iw.gt.-900) then
        iwabs=iabs(iw)
        kmodn(1)=max(KModA(1,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(1,iat)=kmodn(1)-1
        if(iwa(iw).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( ax(iwabs,i),1,0.)
            call SetRealArrayTo( ay(iwabs,i),1,0.)
            call SetRealArrayTo(sax(iwabs,i),1,0.)
            call SetRealArrayTo(say(iwabs,i),1,0.)
          enddo
          iwa(iw)=1
        endif
        call CIFSetMod(ax(iwabs,iat),ay(iwabs,iat),
     1                 sax(iwabs,iat),say(iwabs,iat),
     1                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) ax(iwabs,iat)=-ax(iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 4200
4300  do j=1,5
        call CIFGetLoop(CIFKey(97,1),StLbl,MxLoop,CIFKey(97+j,1),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          exit
        else
          do i=1,n
            if(KPhase.gt.1)
     1        StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,StLbl(i))
            if(iat.le.0) cycle
            if(j.le.3) then
               ux(j,KModA(2,iat)+1,iat)=pa(i)
              sux(j,KModA(2,iat)+1,iat)=0.
            else if(j.eq.4) then
               uy(1,KModA(2,iat)+1,iat)=pa(i)
              sux(1,KModA(2,iat)+1,iat)=0.
            else if(j.eq.5) then
              KFA(2,iat)=1
              KModA(2,iat)=KModA(2,iat)+1
               uy(2,KModA(2,iat),iat)=pa(i)
              suy(2,KModA(2,iat),iat)=0.
            endif
          enddo
        endif
      enddo
      j=0
      do i=1,NAtCalc
        call SetRealArrayTo(xp,3,0.)
        k=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,i).ne.0) then
            xp(1)=ax(KModA(1,i),i)
            k=1
          else if(KFA(2,i).ne.0) then
            xp(1)=uy(1,KModA(2,i),i)
            k=1
          endif
        endif
        call SpecPos(x(1,i),xp,k,iswa(i),.01,nocc)
        aipi=aip(i)
        ai(i)=1./float(nocc)*aip(i)
        sai(i)=1./float(nocc)*saip(i)
        if(WyckoffMult(i).le.0) WyckoffMult(i)=FullMultiplicity/nocc
        aip(i)=aip(i)*float(WyckoffMult(i))/float(FullMultiplicity)
        if(abs(ai(i)-aip(i)).gt..0001) then
          j=j+1
          if(j.eq.1) then
            t256=Atom(i)
            t256='the item "_atom_site_symmetry_multiplicity" for '//
     1           'the atom "'//Atom(i)(:idel(Atom(i)))//'"'
            p256='is not in accordance with the derived site '//
     1           'symmetry.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
          endif
        endif
      enddo
      if(j.gt.0) then
        if(j.gt.1) then
          write(Cislo,FormI15) j
          call Zhusti(Cislo)
          t256='the previous error has occured '//Cislo(:idel(Cislo))//
     1         ' times.'
        else
          t256='the previous error has occured just once.'
        endif
        p256='It will not have an influence to subsequest work.'
        call FeChybne(-1.,-1.,t256,p256,Warning)
      endif
      ik=163
      do nn=3,6
        Znak=char(ichar('C')+nn-3)
        call SetIntArrayTo(il,10,0)
        n=0
        do i=1,9
          call CIFGetSt(CifKey(i+ik,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
          endif
        enddo
        call CIFBeginLoop(m,CIFKey(ik+1,22),p256,t256,ich)
        if(ich.eq.-1) cycle
        call SetIntArrayTo(iwa,20,0)
        ik=ik+9
4420    k=0
        it=0
        do 4500i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4600
          j=il(i)
          if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,t256)
          else if(j.eq.5) then
            pcos=pom
            scos=spom
            it=0
          else if(j.eq.7) then
            pmod=pom
            smod=spom
            it=1
          else if(j.eq.8) then
            pfaz=pom
            sfaz=spom
            it=1
          else if(j.eq.9) then
            psin=pom
            ssin=spom
            it=0
          else if(j.eq.3) then
            if(.not.EqIgCase(t256(1:1),Znak)) then
              ib=0
              go to 4500
            endif
            ip=0
            do l=2,nn+1
              read(t256(l:l),102,err=4500) kk
              if(kk.lt.1.or.kk.gt.3) go to 4500
              ip=ip*4+kk
            enddo
            ib=nindc(ip,nn)
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=4440) iw
            if(iw.gt.0) then
              iw=kwi(iw)
            else
              iw=0
            endif
            cycle
4440        iw=-999
          endif
4500    continue
        if(ib.gt.0.and.iw.gt.-900) then
          iwabs=iabs(iw)
          kmodn(nn+1)=max(KModA(nn+1,iat),iwabs)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(nn+1,iat)=kmodn(nn+1)-1
          if(iwa(iwabs).le.0) then
            do i=1,NAtCalc
              if(nn.eq.3) then
                call SetRealArrayTo( c3x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c3y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc3x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc3y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.4) then
                call SetRealArrayTo( c4x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c4y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc4x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc4y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.5) then
                call SetRealArrayTo( c5x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c5y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc5x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc5y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.6) then
                call SetRealArrayTo( c6x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c6y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc6x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc6y(1,iwabs,i),TRank(nn),0.)
              endif
            enddo
            iwa(iw)=1
          endif
          if(nn.eq.3) then
            call CIFSetMod( c3x(ib,iwabs,iat), c3y(ib,iwabs,iat),
     1                     sc3x(ib,iwabs,iat),sc3y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c3x(1,iwabs,i)=-c3x(1,iwabs,i)
          else if(nn.eq.4) then
            call CIFSetMod( c4x(ib,iwabs,iat), c4y(ib,iwabs,iat),
     1                     sc4x(ib,iwabs,iat),sc4y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c4x(1,iwabs,i)=-c4x(1,iwabs,i)
          else if(nn.eq.5) then
            call CIFSetMod( c5x(ib,iwabs,iat), c5y(ib,iwabs,iat),
     1                     sc5x(ib,iwabs,iat),sc5y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c5x(1,iwabs,i)=-c5x(1,iwabs,i)
          else if(nn.eq.6) then
            call CIFSetMod( c6x(ib,iwabs,iat), c6y(ib,iwabs,iat),
     1                     sc6x(ib,iwabs,iat),sc6y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c6x(1,iwabs,i)=-c6x(1,iwabs,i)
          endif
        else if(iw.eq.0.and.iat.gt.0) then
          p256='The wave vector not properly defined'
          call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
        else if(ib.eq.0.and.iat.gt.0) then
          p256='Unknown modulation component'
          call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
        endif
        call CIFNewRecord(m,p256,*5000)
        go to 4420
4600  enddo
      ik=107
      im=1
      il=0
      n=0
      do i=1,6
        call CIFGetSt(CifKey(ik+i,im),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(ik+1,im),p256,t256,ich)
      if(ich.eq.-1) go to 5000
      iwa=0
      kmodp=0
4620  k=0
      it=0
      do 4700i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 5000
        j=il(i)
        if(j.eq.4.or.j.eq.5) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.2) then
          do ix=1,3
            if(EqIgCase(t256,Smbx(ix))) go to 4700
          enddo
          ix=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.3) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=4640) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
4640      iw=-999
        else if(j.eq.4) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.5) then
          psin=pom
          ssin=spom
          it=0
        endif
4700  continue
      if(ix.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        kmodp=max(kmodp,iwabs+1)
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,kmodp)
        MagPar(iat)=max(MagPar(iat),iwabs+1)
        if(iwa(iw).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( smx(1,iwabs,i),3,0.)
            call SetRealArrayTo( smy(1,iwabs,i),3,0.)
            call SetRealArrayTo(ssmx(1,iwabs,i),3,0.)
            call SetRealArrayTo(ssmy(1,iwabs,i),3,0.)
          enddo
          iwa(iw)=1
        endif
        call CIFSetMod( smx(ix,iwabs,iat), smy(ix,iwabs,iat),
     1                 ssmx(ix,iwabs,iat),ssmy(ix,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        pom=1./CellPar(ix,iswa(iat),KPhase)
         smx(ix,iwabs,iat)= smx(ix,iwabs,iat)*pom
         smy(ix,iwabs,iat)= smy(ix,iwabs,iat)*pom
        ssmx(ix,iwabs,iat)=ssmx(ix,iwabs,iat)*pom
        ssmy(ix,iwabs,iat)=ssmx(ix,iwabs,iat)*pom
        if(iw.lt.0) smx(ix,iwabs,iat)=-smx(ix,iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ix.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*5000)
      go to 4620
5000  if(DelejOrtho) then
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        OrthoOrd=2*MaxUsedKwAll+1
        if(allocated(OrthoSel)) deallocate(OrthoSel)
        allocate(OrthoSel(OrthoOrd,NAtCalc))
        if(allocated(OrthoMat)) deallocate(OrthoMat,OrthoMatI)
        m=OrthoOrd**2
        allocate(OrthoMat(m,NAtCalc),OrthoMatI(m,NAtCalc))
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or.NDim(KPhase).le.3.or.
     1       KFA(1,i).ne.1.or.TypeModFun(i).ne.1) cycle
          call UpdateOrtho(i,OrthoX40(i),OrthoDelta(i),OrthoEps(i),
     1                     OrthoSel(1,i),OrthoOrd)
          j=1
5100      if(OrthoSel(j,i).le.2*KModA(2,i)) then
            j=j+1
            go to 5100
          endif
          kmodao(2,i)=(j-1)/2
          j=1
5200      if(OrthoSel(j,i).le.2*KModA(3,i)) then
            j=j+1
            go to 5200
          endif
          kmodao(3,i)=(j-1)/2
          call MatOr(OrthoX40(i),OrthoDelta(i),OrthoSel(1,i),OrthoOrd,
     1               OrthoMat(1,i),OrthoMatI(1,i),i)
        enddo
        SwitchedToHarm=.true.
        call trortho(1)
      endif
      if(MagneticType(KPhase).gt.0) then
        s256=CIFKey(116,1)
        do i=1,3
          p256=CIFKey(i+122,1)
          call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,nac,
     1                    t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            isfj=isf(j)
            do k=1,nac
              isw=iswa(j)
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle
              if(EqIgCase(StLbl(k),Atom(j))) then
                if(MagPar(j).le.0) then
                  MagPar(j)=1
                  MagParMax=1
                  call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,
     1                                      MagParMax)
                endif
                if(AtTypeMag(isfj,KPhase).eq.' ') then
                  call EM50ReadMagneticFFLabels(AtType(isfj,KPhase),
     1              MenuJ0(2),NMenuJ0,MenuJ2(2),NMenuJ2,ierr)
                  if(ierr.eq.0) then
                    if(NMenuJ2.gt.0) then
                      AtTypeMag(isfj,KPhase)=
     1                  MenuJ2(2)(:idel(MenuJ2(2)))//'m'
                    else if(NMenuJ0.gt.0) then
                      AtTypeMag(isfj,KPhase)=MenuJ0(2)
                    endif
                  endif
                endif
                 sm0(i,j)= pa(k)/CellPar(i,isw,KPhase)
                ssm0(i,j)=spa(k)/CellPar(i,isw,KPhase)
              endif
            enddo
          enddo
        enddo
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      lite(KPhase)=0
      irot(KPhase)=1
      if(KPhase.eq.1) then
        call SetRealArrayTo(ec,12*MxDatBlock,0.)
        sc(1,1)=1.
      endif
      if(NDimI(KPhase).gt.0) then
        do i=1,NAtCalc
          do j=1,7
            KModAMax(j)=max(KModAMax(j),KModA(j,i))
          enddo
          if(KModA(1,i).gt.0) then
            if(KFA(1,i).ne.0) then
              xp(1)=ax(KModA(1,i),i)
              k=1
            else if(KFA(2,i).ne.0) then
              xp(1)=uy(1,KModA(2,i),i)
              k=1
            else
              xp(1)=0.
              k=0
            endif
            call SpecPos(x(1,i),xp,k,iswa(i),.01,nocc)
            pom=1./float(nocc)
            if(a0(i).gt.-3000.) then
              ai(i)=ai(i)/a0(i)
            else
              a0(i)=ai(i)/pom
              sa0(i)=sai(i)/pom
              ai(i)=pom
            endif
            sai(i)=0.
          endif
          phf(i)=0.
          sphf(i)=0.
        enddo
      endif
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
      call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,0),3)
        call CopyVek(Qu(1,i,1,KPhase),QuPwd(1,i,KPhase),3)
      enddo
      if(isPowder.and.KPhase.le.1) then
        call SetBasicM41(KDatBlock)
        ExistPowder=.true.
      endif
      if(NComp(KPhase).gt.1) then
        if(2*NAtCalc.gt.MxAtAll) call ReallocateAtoms(2*NAtCalc-MxAtAll)
        do i=1,NAtCalc
          call AtCopy(i,i+NAtCalc)
        enddo
        nac=0
        do i=1,NComp(KPhase)
           do j=NAtCalc+1,2*NAtCalc
             if(iswa(j).eq.i) then
               nac=nac+1
               call AtCopy(j,nac)
             endif
           enddo
        enddo
      endif
      call iom40(1,0,fln(:ifln)//'.m40')
      if(KlicUsed.ne.5) call DeleteFile(PreviousM40)
      if(KPhase.gt.1) go to 9999
      if((ExistM90.or.ExistM95).and.KlicUsed.ne.5) then
        WaitTime=10000
        Ninfo=2
        TextInfo(1)='Please note possible inconsistency between'
        TextInfo(2)='imported structure and old reflection files.'
        call FeInfoOut(-1.,-1.,'WARNING','L')
      endif
      if(KlicUsed.gt.0) go to 7000
      if(isPowder) then
        call PwdBasicCIF(0,KlicUsed)
      else
        if(KlicUsed.eq.0) then
          call DRBasicRefCIF(0,1)
        else
          go to 7000
        endif
      endif
      if(ErrFlag.ne.0) then
        if(.not.BatchMode) then
          NInfo=1
          TextInfo(1)='CIF file does not contain any reflection block.'
          if(FeYesNoHeader(-1.,-1.,'Do you want to import data from '//
     1                     'file?',1)) CoDal=1
        endif
        ErrFlag=0
        isPowder=.false.
        ExistPowder=.false.
        t256=fln(:ifln)//'.m41'
        if(ExistFile(t256)) then
          call DeleteFile(t256)
          ExistM41=.false.
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        go to 7000
      endif
      call CloseIfOpened(lni)
      call SetBasicM95(KRefBlock)
      SourceFileRefBlock(KRefBlock)=FileIn
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,KRefBlock),3)
      enddo
      call UnitMat(TrMP,NDim(KPhase))
      CellReadIn(KRefBlock)=.true.
      CorrLp(KRefBlock)=-1
      CorrAbs(KRefBlock)=-1
      LamAveRefBlock(KRefBlock)=LamAve(1)
      DifCode(KRefBlock)=IdImportCIF
      if(Radiation(KDatBlock).eq.NeutronRadiation) then
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else
        RadiationRefBlock(KRefBlock)=XRayRadiation
        AngleMonRefBlock(KRefBlock)=
     1   CrlMonAngle(MonCell(1,3),MonH(1,3),LamAveRefBlock(KRefBlock))
        AlphaGMonRefBlock(KRefBlock)=0.
        BetaGMonRefBlock(KRefBlock)=0.
      endif
      TempRefBlock(KRefBlock)=293
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      if(isPowder) then
        PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
        DifCode(KRefBlock)=IdDataCIF
      else
        if(Radiation(KDatBlock).eq.NeutronRadiation) then
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        else
          PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
        endif
        DifCode(KRefBlock)=IdImportCIF
      endif
      RefBlockFileName=fln(:ifln)//'.l01'
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) call CIFSyntaxError(CIFLastReadRecord)
      t256='000000'
      UseTabs=NextTabs()
      call FeTabsAdd(FeTxLength(t256),UseTabs,IdLeftTab,' ')
      call FeTabsAdd(FeTxLengthSpace(t256(:6)//' '),UseTabs,IdRightTab,
     1               ' ')
      if(isPowder) then
        t256=Tabulator//'     0'//Tabulator//'records already read'
        if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
        nalloc=10000
        allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
        NPnts=0
      else
        t256=Tabulator//'     0'//Tabulator//'reflections already read'
        write(t256(2:7),100) 0
        tbar=0.
        call SetRealArrayTo(uhly,4,0.)
        call SetRealArrayTo(dircos,6,0.)
        call SetRealArrayTo(corrf,2,1.)
        iq=1
        KProf=0
        NProf=0
        DRlam=0.
      endif
      call FeTxOut(-1.,-1.,t256)
6000  if(isPowder) then
        ipointA=0
        iorderA=0
        ipointI=0
        iorderI=0
        call PwdReadCIF(0,0,ik)
      else
        call DRReadRefCIF(0,0,ik)
      endif
      if(ErrFlag.ne.0) call CIFSyntaxError(CIFLastReadRecord)
      if(ik.ne.0) go to 6100
      if(isPowder) then
        NRef95(KRefBlock)=NRef95(KRefBlock)+1
        if(NPnts.ge.nalloc) call PwdImportReallocate(nalloc)
        NPnts=NPnts+1
        XPwd(NPnts)=float(ih(1))/1000000.
        YoPwd(NPnts)=ri
        YiPwd(NPnts)=0.
        YsPwd(NPnts)=rs
      else
        NRef95(KRefBlock)=NRef95(KRefBlock)+1
        no=NRef95(KRefBlock)
        expos=float(NRef95(KRefBlock))*.1
        iflg(1)=iq
        if(iflg(2).lt.0) HKLF5RefBlock(KRefBlock)=1
        call DRPutReflectionToM95(95,n)
        NLines95(KRefBlock)=NLines95(KRefBlock)+2
      endif
      if(mod(NRef95(KRefBlock),50).eq.0) then
        write(t256(2:7),100) NRef95(KRefBlock)
        call FeTxOutCont(t256)
      endif
      go to 6000
6100  write(t256(2:7),100) NRef95(KRefBlock)
      call FeTxOutCont(t256)
      call FeTxOutEnd
      if(isPowder) then
        call PwdMakeFormat92
        call PwdPutRecordToM95(95)
      endif
      call CloseIfOpened(95)
      call iom95(1,fln(:ifln)//'.m95')
      call CompleteM95(0)
      ExistM95=.true.
7000  if(KlicUsed.eq.5) then
        i=2
      else
        i=0
      endif
      call FinalStepOfImport(i,'CIF')
      go to 9999
9000  if(ich.eq.-1) then
        call FeChybne(-1.,-1.,'the crutial information is missing.',
     1                t256,SeriousError)
      else if(ich.eq.-2) then
        call FeChybne(-1.,-1.,'CIF syntax problem.',CIFLastReadRecord,
     1                SeriousError)
      else if(ich.eq.-3) then
        call FeChybne(-1.,-1.,'number of variables does not match.',
     1                t256,SeriousError)
      else
        call FeChybne(-1.,-1.,'neco navic.',t256,SeriousError)
      endif
9900  ErrFlag=1
9999  call CloseIfOpened(lni)
      call CloseIfOpened(91)
      call CloseIfOpened(95)
      call DeleteFile('cif.pom')
      if(KlicUsed.ne.5) then
        call DeleteFile(fln(:ifln)//'.z40')
        call DeleteFile(fln(:ifln)//'.z50')
      endif
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      if(allocated(AtMultP)) deallocate(AtMultP)
      if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(CIFArray)) deallocate(CIFArray)
      if(allocated(aip)) deallocate(aip,saip)
      if(allocated(IdCode)) deallocate(IdCode)
      if(allocated(OrX40P)) deallocate(OrX40P,OrDelP,OrEpsP)
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta,stap)
      call FeTabsReset(-1)
      return
100   format(i6)
101   format(f15.0)
102   format(i1)
103   format(i6)
104   format(3f9.4,3f9.2)
      end
