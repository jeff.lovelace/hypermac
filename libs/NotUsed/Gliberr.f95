

!          **************************
!          ***      Okna          ***
!          **************************


      subroutine inic
      use Jana_windows
      use FileDirList_mod
      include 'fepc.cmn'
      dimension x(10),y(10)
      integer FeRGBCompress
      logical lpom,FeReadOnly
      character*256 t256
      type(RECT) rctc
!      if(Language.eq.1) i=ActivateKeyboardLayout(carg(HKL_NEXT),carg(0))
      PropFontMeasured=FontInPoints
      OpSystem=-1
      DirectoryDelimitor=ObrLom
      if(HomeDir.eq.' ') then
        t256=MasterName(:idel(MasterName))//'DIR'
        call Velka(t256)
        call FeOsVariable(t256,HomeDir)
        if(HomeDir.eq.' ') then
          TextInfo(1)='The environment variable '//t256(:idel(t256))//
     1                ' is not defined.'//char(0)
          if(LocateSubstring(MasterName,'jana',.false.,.true.).gt.0)
     1      then
            t256='See www-xray.fzu.cz/jana/Jana2006/environ.html for '//
     1           'more information.'//char(0)
          else
            t256=' Volej Vaska.'//char(0)
          endif
          call FeMessageBox(TextInfo(1),t256)
          stop
        endif
      endif
      i=idel(HomeDir)
1000  if(HomeDir(i:i).eq.'\') then
        HomeDir(i:i)=' '
        i=i-1
        go to 1000
      endif
      i=i+1
      HomeDir(i:i)='\'
      RootDir=HomeDir
      open(44,file=HomeDir(:idel(HomeDir))//
     1             MasterName(:idel(MasterName))//'.log')
      write(44,'(''Command line: '',a)') CommandLine(:idel(CommandLine))
      if(VasekDebug.ne.0) write(44,'(''INIC - begin'')')
      if(VasekDebug.ne.0)
     1  write(44,'(''HomeDir : '',256a1)')(HomeDir(j:j),j=1,i)
      Black      =FeRGBCompress(  0,  0,  0)
      White      =FeRGBCompress(255,255,255)
      Red        =FeRGBCompress(255,  0,  0)
      Green      =FeRGBCompress(  0,255,  0)
      Blue       =FeRGBCompress(  0,  0,255)
      Cyan       =FeRGBCompress(  0,255,255)
      Magenta    =FeRGBCompress(255,  0,255)
      Yellow     =FeRGBCompress(255,255,  0)
      WhiteGray  =FeRGBCompress(227,227,227)
      SnowGray   =FeRGBCompress(215,215,215)
      LightGray  =FeRGBCompress(200,200,200)
      Gray       =FeRGBCompress(124,124,124)
      LightYellow=FeRGBCompress(255,255,200)
      DarkBlue   =FeRGBCompress(  0,  0,130)
      Khaki      =FeRGBCompress(155,155,  0)
      ColorNumbers(1) = Black
      ColorNames(1)   ='Black'
      ColorNumbers(2) = White
      ColorNames(2)   ='White'
      ColorNumbers(3) = Red
      ColorNames(3)   ='Red'
      ColorNumbers(4) = Green
      ColorNames(4)   ='Green'
      ColorNumbers(5) = Blue
      ColorNames(5)   ='Blue'
      ColorNumbers(6) = Cyan
      ColorNames(6)   ='Cyan'
      ColorNumbers(7) = Magenta
      ColorNames(7)   ='Magenta'
      ColorNumbers(8) = Yellow
      ColorNames(8)   ='Yellow'
      ColorNumbers(9) = WhiteGray
      ColorNames(9)   ='WhiteGray'
      ColorNumbers(10)= SnowGray
      ColorNames(10)  ='SnowGray'
      ColorNumbers(11)= LightGray
      ColorNames(11)  ='LightGray'
      ColorNumbers(12)= Gray
      ColorNames(12)  ='Gray'
      ColorNumbers(13)= LightYellow
      ColorNames(13)  ='LightYellow'
      ColorNumbers(14)= DarkBlue
      ColorNames(14)  ='DarkBlue'
      ColorNumbers(15)= Khaki
      ColorNames(15)  ='Khaki'
      PocetBarev=15
      call FeRegisterAndOpenBasicWindow(hwnd,0,0,100,100)
      hDC=GetDC(carg(hWnd))
      BitsPixelNew=GetDeviceCaps(carg(hDC),carg(BITSPIXEL))
      PixelScreenWidth=GetDeviceCaps(carg(hDC),carg(HORZRES))
      PixelScreenHeight=GetDeviceCaps(carg(hDC),carg(VERTRES))
      call DestroyWindow(carg(hWnd))
      call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      hDCMeta=0
      call FeInOutIni(0,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      iii=WindowSizeType
      n=0
      hDCMeta=0
1100  call FeOpenBasicWindow(hWnd,PixelWindowXpos,PixelWindowYPos,
     1                       PixelWindowWidth,PixelWindowHeight)
      lpom=ShowWindow(carg(hwnd),carg(SW_SHOW))
      if(IsZoomed(carg(hWnd)).gt.0) then
        jjj=WindowMaximal
      else
        jjj=WindowExactly
      endif
      if(iii.ne.jjj.and.n.le.3) then
        n=n+1
        call DestroyWindow(carg(hWnd))
        go to 1100
      endif
      lpom=UpdateWindow(carg(hwnd))
      call FeMouseShape(0)
      XMinAbs=0.
      YMinAbs=0.
      XMaxAbs=1.
      YMaxAbs=1.
      do i=1,MxBmp
        BmpName(i)=' '
      enddo
      EnlargeFactor=1.
      PropFont=.true.
      KurzorClick=-1
      KurzorColor=White
      IconXLength=50.
      IconYLength=50.
      DelejResize=.false.
      Resized=.true.
      DockalSeConf=.true.
      call GetClientRect(carg(hWnd),carg(offset(rctc)))
      PixelClientHeight=rctc.bottom-rctc.top+1
      PixelClientWidth=rctc.right-rctc.left+1
      if(VasekDebug.ne.0) write(44,'(''INIC - end'')')
      hDC=GetDC(carg(hWnd))
      BitsPixelNew=GetDeviceCaps(carg(hDC),carg(BITSPIXEL))
      PixelScreenWidth=GetDeviceCaps(carg(hDC),carg(HORZRES))
      PixelScreenHeight=GetDeviceCaps(carg(hDC),carg(VERTRES))
      hDCComp=CreateCompatibleDC(carg(hDC))
      hDCBitMap=CreateCompatibleBitmap(carg(hDC),
     1                                 carg(PixelClientWidth),
     2                                 carg(PixelClientHeight))
      PropFontName='Tahoma'
      i=-nint(8.*GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HPropFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_DEVICE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      call FeSetPropFont
      t256='12345678901234567890123456789012345678901234567890'//
     1     '12345678901234567890123456789012345678901234567890'//
     2     '12345678901234567890123456789012345678901234567890'//
     3     '12345678901234567890123456789012345678901234567890'
      idl1=FeTxLength(t256)
      i=-nint(float(PropFontSize)*
     1  GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HPropFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_DEVICE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      HPropFontB=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(700),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_OUTLINE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(PropFontName(:idel(PropFontName))))
      FixFontName='Courier New'
      i=-nint(float(PropFontSize)*
     1  GetDeviceCaps(carg(hDC),carg(LOGPIXELSY))/72.)
      HFixFontN=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(0),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_OUTLINE_PRECIS),
     4                       carg(CLIP_DEFAULT_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(FixFontName(:idel(FixFontName))))
      HFixFontB=CreateFontA(carg(i),carg(0),carg(0),carg(0),carg(700),
     1                       carg(0),carg(0),carg(0),
     2                       carg(DEFAULT_CHARSET),
     3                       carg(OUT_TT_PRECIS),
     3                       carg(OUT_OUTLINE_PRECIS),
     5                       carg(DEFAULT_QUALITY),carg(FF_DONTCARE),
     6                       carg(FixFontName(:idel(FixFontName))))
      call FeSetFixFont
      FixFontWidthInPixels=FeTxLength(char(0))
      FixFontHeightInPixels=FeTxHeight(char(0)//'gy_')
      call FeSetPropFont
      PropFontWidthInPixels=FeTxLength(char(0))
      PropFontHeightInPixels=FeTxHeight(char(0)//'gy_')
      idl2=FeTxLength(t256)
      EnlargeFactor=max(PropFontWidthInPixels/11.,
     1                  float(idl2)/float(idl1),
     2                  PropFontHeightInPixels/13.)
      XMinBasWin=0.
      XMaxBasWin=anint(float(PixelClientWidth)/EnlargeFactor)
      YMinBasWin=0.
      YMaxBasWin=anint(float(PixelClientHeight)/EnlargeFactor)
      XCenBasWin=anint((XMinBasWin+XMaxBasWin)*.5)
      YCenBasWin=anint((YMinBasWin+YMaxBasWin)*.5)
      XLenBasWin=anint(XMaxBasWin-XMinBasWin)
      YLenBasWin=anint(YMaxBasWin-YMinBasWin)
      DeferredOutput=.false.
      call FePlotMode('N')
      call FeDeferOutput
      return
      end
      subroutine FeRegisterAndOpenBasicWindow(hwndp,ixp,iyp,ixd,iyd)
      use Jana_windows
      include 'fepc.cmn'
      integer hwndp
      logical lpom
      character*256 t256
      type(WNDCLASS) :: wc
      save wc
      Klic=0
      go to 1000
      entry FeRegisterBasicWindow(hwndp,ixp,iyp,ixd,iyd)
      Klic=1
1000  ModuleHandle=GetModuleHandleA(carg(NULL))
      wc%style         = 0
      wc%lpfnWndProc   = offset(WndProc)
      wc%cbClsExtra    = 0
      wc%cbWndExtra    = 0
      wc%hInstance     = ModuleHandle
      wc%hCursor       = LoadCursorA(carg(NULL),carg(IDC_ARROW))
      t256='ICON1'//char(0)
      wc%hIcon         = LoadIconA(carg(ModuleHandle),
     1                             carg(offset(t256)))
      wc%hbrBackGround = COLOR_WINDOWTEXT+1
      wc%lpszMenuName  = NULL
      wc%lpszClassName = offset(szClassName)
      i=RegisterClassA(wc)
      if(Klic.eq.1) go to 9999
      entry FeOpenBasicWindow(hwndp,ixp,iyp,ixd,iyd)
      t256=MasterName(:idel(MasterName))//char(0)
      i=ior(WS_OVERLAPPED,
     1    ior(WS_THICKFRAME,
     2      ior(WS_CAPTION,
     3          ior(WS_SYSMENU,
     4            ior(WS_MINIMIZEBOX,WS_MAXIMIZEBOX)))))
      ixpp=ixp
      iypp=iyp
      ixdp=ixd
      iydp=iyd
      if(WindowSizeType.eq.WindowMaximal) i=ior(i,WS_MAXIMIZE)
      hwndp=CreateWindowExA(carg(NULL),carg(szClassName),
     1                     carg(t256),
     2                     carg(i),
     3                     carg(ixpp),carg(iypp),
     4                     carg(ixdp),carg(iydp),carg(NULL),
     5                     CARG(NULL),CARG(ModuleHandle),
     6                     carg(NULL))
9999  return
      end
      subroutine FeGrQuit
      use Jana_windows
      include 'fepc.cmn'
      call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                MasterName(:idel(MasterName))//'.ini')
      call DestroyWindow(carg(hWnd))
      call DestroyWindow(carg(hWndT))
      end
      subroutine FeWindowTitle(Text)
      use Jana_windows
      character*(*) Text
      call SetWindowTextA(carg(hWnd),carg(Text(:idel(Text))//char(0)))
      return
      end
      subroutine FeMessage(Imm)
      use Jana_windows
      include 'fepc.cmn'
      character*1 Znak
      integer XWin,YWin
      integer wParam,hwndp,FeGetSystemTime,StTime,StTimeNew
      logical lpom
      type(MSG) zprava
      type(RECT) rct
      data XWin,YWin/2*-1111/,XPosLast,YPosLast/0.,0./,
     1     StTime/-1111./
      Zprava%hwnd=0
      Zprava%Message=0
      Zprava%wParam=0
      Zprava%lParam=0
      hwndp  =Zprava%hwnd
      Message=Zprava%Message
      wParam =Zprava%wParam
      lParam =Zprava%lParam
      StTime=FeGetSystemTime()
      KurzorColor=White
1000  EventType=0
      EventNumber=0
      if(imm.eq.0.and.KurzorEdw.le.0) then
        lpom=GetMessageA(carg(offset(Zprava)),carg(hwnd),carg(0),
     1                   carg(0))
      else
        lpom=PeekMessageA(carg(offset(Zprava)),carg(hwnd),carg(0),
     1                    carg(0),carg(PM_REMOVE))
      endif
      if(lpom) then
        hwndp  =Zprava%hwnd
        Message=Zprava%Message
        wParam =Zprava%wParam
        lParam =Zprava%lParam
        if(Message.eq.WM_KEYDOWN.or.Message.eq.WM_SYSKEYDOWN) then
          if(Message.eq.WM_KEYDOWN) then
            EventType=EventKey
          else
            EventType=EventAlt
          endif
          if(wParam.eq.VK_BACK) then
            EventNumber=JeBackspace
          else if(wParam.eq.VK_TAB) then
            if(ShiftPressed) then
              EventNumber=JeShiftTab
            else
              EventNumber=JeTab
            endif
          else if(wParam.eq.VK_RETURN) then
            EventNumber=JeReturn
          else if(wParam.eq.VK_ESCAPE) then
            EventNumber=JeEscape
          else if(wParam.eq.VK_PRIOR) then
            EventNumber=JePageUp
          else if(wParam.eq.VK_NEXT) then
            EventNumber=JePageDown
          else if(wParam.eq.VK_END) then
            if(CtrlPressed) then
              EventNumber=JeDownExtreme
            else
              EventNumber=JeEnd
            endif
          else if(wParam.eq.VK_HOME) then
            if(CtrlPressed) then
              EventNumber=JeUpExtreme
            else
              EventNumber=JeHome
            endif
          else if(wParam.eq.VK_LEFT) then
            if(CtrlPressed) then
              EventNumber=JeHome
            else
              EventNumber=JeLeft
            endif
          else if(wParam.eq.VK_UP) then
            EventNumber=JeUp
          else if(wParam.eq.VK_RIGHT) then
            if(CtrlPressed) then
              EventNumber=JeEnd
            else
              EventNumber=JeRight
            endif
          else if(wParam.eq.VK_DOWN) then
            EventNumber=JeDown
          else if(wParam.eq.VK_INSERT) then
            EventNumber=JeInsert
          else if(wParam.eq.VK_DELETE) then
            EventNumber=JeDeleteUnder
          else if(wParam.eq.VK_F1) then
            EventNumber=JeF1
          else if(wParam.eq.VK_F4) then
            EventNumber=JeF4
          else if(wParam.eq.VK_SPACE) then
            EventType=EventASCII
            EventNumber=ichar(' ')
          else if(wParam.eq.VK_SHIFT) then
            if(EventType.ne.EventAlt) then
              ShiftPressed=.true.
            else
              i=ActivateKeyboardLayout(carg(HKL_NEXT),carg(0))
            endif
            go to 1000
          else if(wParam.eq.VK_CONTROL) then
            CtrlPressed=.true.
            go to 1000
          else if(wParam.eq.VK_menu) then
            CtrlPressed=.false.
            ShiftPressed=.false.
            go to 1000
          endif
          if(EventNumber.ne.0) go to 8000
        else if(Message.eq.WM_KEYUP) then
          if(wParam.eq.VK_SHIFT) then
            ShiftPressed=.false.
          else if(wParam.eq.VK_CONTROL) then
            CtrlPressed=.false.
          endif
          go to 1000
        else if(Message.eq.WM_DESTROY) then
          EventType=EventSystem
          EventNumber=JeWinClose
        endif
        call TranslateMessage(carg(offset(Zprava)))
        hwndp  =Zprava%hwnd
        Message=Zprava%Message
        wParam =Zprava%wParam
        lParam =Zprava%lParam
        if(Message.eq.WM_CHAR) then
          EventType=EventASCII
          EventNumber=wParam
          if(EventNumber.ge.1.and.EventNumber.le.26) then
            EventType=EventCtrl
            EventNumber=EventNumber+64
          else if(EventNumber.eq.VK_RETURN) then
            EventType=EventCtrl
            EventNumber=ichar('M')
          else if(EventNumber.eq.VK_BACK) then
            EventType=EventCtrl
            EventNumber=ichar('H')
          else if(EventNumber.eq.VK_TAB) then
            EventType=EventCtrl
            EventNumber=ichar('I')
          endif
        else if(Message.eq.WM_SYSCHAR) then
          EventType=EventAlt
          Znak=char(wParam)
          call mala(Znak)
          EventNumber=ichar(Znak)
        else if(Message.eq.WM_LBUTTONDOWN.or.
     1          Message.eq.WM_LBUTTONDBLCLK) then
          EventType=EventMouse
          EventNumber=JeLeftDown
        else if(Message.eq.WM_LBUTTONUP) then
          EventType=EventMouse
          EventNumber=JeLeftUp
        else if(Message.eq.WM_RBUTTONDOWN) then
          EventType=EventMouse
          EventNumber=JeRightDown
        else if(Message.eq.WM_RBUTTONUP) then
          EventType=EventMouse
          EventNumber=JeRightUp
        else if(Message.eq.WM_MOUSEMOVE) then
          EventType=EventMouse
          EventNumber=JeMove
        else if(Message.eq.WM_MOUSEWHEEL) then
          EventType=EventMouse
          if(wParam.gt.0) then
            EventNumber=JeKoleckoOdSebe
          else
            EventNumber=JeKoleckoKSobe
          endif
        else if(Message.eq.0) then
          go to 8000
        else if(Message.ne.WM_SYSKEYUP.and.
     1          Message.ne.WM_SYSKEYDOWN) then
          if((Message.eq.WM_NCLBUTTONDOWN.and.wParam.eq.20).or.
     1       (Message.eq.WM_SYSCOMMAND.and.wParam.eq.SC_Close)) then
            EventType=EventSystem
            EventNumber=JeWinClose
            go to 9900
          endif
          i=DefWindowProcA(carg(hwndp),carg(message),carg(wParam),
     1                     carg(lParam))
          if(Message.eq.WM_NCMOUSEMOVE) then
            EventType=EventSystem
            EventNumber=JeMimoOkno
            go to 9900
          else if(Message.eq.WM_PAINT) then
            call FeFlush
            go to 9999
          else if(Message.eq.WM_NCLBUTTONDOWN.and.wParam.eq.9) then
            if(WindowSizeType.eq.WindowMaximal) then
              WindowSizeType=WindowExactly
            else
              WindowSizeType=WindowMaximal
            endif
            go to 8000
          else if(Message.ge.WM_NCLBUTTONDOWN.and.
     1            Message.le.WM_NCLBUTTONDOWN) then
            go to 8000
          endif
          go to 8000
        endif
      endif
8000  if(imm.eq.0) then
        if(KurzorEdw.gt.0) then
          if(lpom.and.Message.ne.0.and.
     1       (EventType.ne.EventMouse.or.EventNumber.ne.JeMove)) then
            if(KurzorColor.eq.Black) then
              call FeZmenKurzor(EdwActive)
              KurzorColor=White
              call FeZmenKurzor(EdwActive)
              call FeUpdateDisplay(-1,-1,-1,-1)
            endif
          else
            StTimeNew=FeGetSystemTime()
            if(StTimeNew.gt.StTime+500) then
              call FeZmenKurzor(EdwActive)
              StTime=StTimeNew
              if(KurzorColor.eq.White) then
                KurzorColor=Black
              else
                KurzorColor=White
              endif
              call FeZmenKurzor(EdwActive)
              call FeUpdateDisplay(-1,-1,-1,-1)
            else if(.not.lpom.or.Message.eq.0) then
              if(StTimeNew-StTime.gt.100) call FeWait(.1)
            endif
            if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
              go to 9900
            else
              go to 1000
            endif
          endif
        else
          if(Message.eq.0) go to 1000
        endif
      endif
9900  call GetWindowRect(carg(hWnd),carg(offset(rct)))
!      if(rct.left.lt.-4) go to 9999
      PixelWindowXPos=rct.left
      PixelWindowYPos=rct.top
      if( WindowSizeType.eq.WindowExactly) then
        PixelWindowXPosE=rct.left
        PixelWindowYPosE=rct.top
      endif
      i=rct.right-rct.left
      j=rct.bottom-rct.top
      if((PixelWindowWidth.ne.i.or.PixelWindowHeight.ne.j).and.
     1    AllowResizing) then
        PixelWindowWidth=i
        PixelWindowHeight=j
        if( WindowSizeType.eq.WindowExactly) then
          PixelWindowWidthE=i
          PixelWindowHeightE=j
        endif
        call GetClientRect(carg(hWnd),carg(offset(rct)))
        PixelClientHeight=rct.bottom-rct.top+1
        PixelClientWidth=rct.right-rct.left+1
        hDCBitMap=CreateCompatibleBitmap(carg(hDC),
     1                                   carg(PixelClientWidth),
     2                                   carg(PixelClientHeight))
        XMinBasWin=0.
        XMaxBasWin=anint(float(PixelClientWidth)/EnlargeFactor)
        YMinBasWin=0.
        YMaxBasWin=anint(float(PixelClientHeight)/EnlargeFactor)
        XCenBasWin=anint((XMinBasWin+XMaxBasWin)*.5)
        YCenBasWin=anint((YMinBasWin+YMaxBasWin)*.5)
        XLenBasWin=anint(XMaxBasWin-XMinBasWin)
        YLenBasWin=anint(YMaxBasWin-YMinBasWin)
        EventType=EventResize
        EventNumber=0
        if(IsZoomed(carg(hWnd)).gt.0) then
          WindowSizeType=WindowMaximal
        else
          WindowSizeType=WindowExactly
        endif
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
      call FeGraphicXYFromMessage(lParam,XPos,YPos)
      if(EventType.ne.0) then
        if(EventType.eq.EventMouse.and.
     1     (EventNumber.eq.JeKoleckoOdSebe.or.
     2      EventNumber.eq.JeKoleckoKSobe)) then
          XPos=XPosLast
          YPos=YPosLast
        else if(EventType.eq.EventMouse) then
          XPosLast=XPos
          YPosLast=YPos
        endif
      endif
      call FeClientToScreen(hWnd,0,0,i,j)
      if(XWin.gt.-1000.or.XWin.gt.-1000) then
        if(i.ne.XWin.or.j.ne.YWin) then
          call FeFlush
          XWin=i
          YWin=j
        endif
      else
        XWin=i
        YWin=j
      endif
9999  return
      end
      subroutine FeMouseShape(n)
      use Jana_windows
      integer hCursor
      include 'fepc.cmn'
      if(n.eq.0) then
        m=IDC_ARROW
      else if(n.eq.1) then
        m=IDC_CROSS
      else if(n.eq.3) then
        m=IDC_WAIT
      endif
      hCursor=LoadCursorA(carg(NULL),carg(m))
      call SetCursor(carg(hCursor))
      return
      end



C          ****************************
C          ***      System          ***
C          ****************************


      subroutine FeZacatek
      use FileDirList_mod
      include 'fepc.cmn'
      character*128 FileToDelete
      character*5  ExtVen(20)
      integer DeleteReadOnly
      logical EqWild,FeYesNoHeader,FeReadOnly
      data ExtVen/'*.bmp','*.tmp','*.m40','*.m50','*.m80',
     1            '*.m81','*.m91','*.s40','*.s50','*.fou',
     2            '*.ref','*.m70','     ','     ','     ',
     3            '     ','     ','     ','     ','     '/
      call FeGetFileDirList(TmpDir)
      j=0
      do 1200i=1,NFiles
        do l=1,20
          if(ExtVen(l).eq.' ') go to 1200
          if(EqWild(FileList(i),ExtVen(l),.false.)) then
            j=j+1
            exit
          endif
        enddo
1200  continue
      DeleteReadOnly=1
      if(j.gt.30.and..not.BatchMode) then
        Ninfo=1
        write(TextInfo(1),'(i5)') j-1
        call zhusti(TextInfo(1))
        TextInfo(1)='There are '//TextInfo(1)(:idel(TextInfo(1)))//
     1              ' temporary files which are probably idle'
        if(FeYesNoHeader(-1.,-1.,'Do you want to delete them?',1)) then
          do 2000i=1,NFiles
            do j=1,20
              if(ExtVen(j).eq.' ') go to 2000
              if(EqWild(FileList(i),ExtVen(j),.false.)) then
                FileToDelete=TmpDir(:idel(TmpDir))//FileList(i)
                if(FeReadOnly(FileToDelete)) then
                  if(DeleteReadOnly.le.2) then
                    call FeYesNoAll(-1.,-1.,
     1                'File "'//FileToDelete(:idel(FileToDelete))//
     2                '" is READONLY. Delete it?',1,DeleteReadOnly)
                  endif
                  if(DeleteReadOnly.eq.1.or.DeleteReadOnly.eq.3) then
                    call FeResetReadOnly(FileToDelete)
                    call DeleteFile(FileToDelete)
                  endif
                else
                  call DeleteFile(FileToDelete)
                endif
                go to 2000
              endif
            enddo
2000      continue
        endif
      endif
c3000  call ndpexc()
3000  return
      end
      subroutine FeGetFileDirList(Directory)
      use Jana_windows
      use FileDirList_mod
      character*(*) Directory
      character*260 FileListO(:),DirListO(:)
      character*256 Veta
      character*1   Flag
      type (WIN32_FIND_DATA) FindData
      integer hSearchFile
      logical EqIgCase
      allocatable FileListO,DirListO
      if(.not.allocated(FileList)) then
        allocate(FileList(1),DirList(1))
        NFilesMax=1
        NDirsMax=1
      endif
      NFiles=0
      NDirs=0
      Veta=Directory(:idel(Directory))//'\*'
      hSearchFile=FindFirstFileA(carg(offset(Veta)),
     1                           carg(offset(FindData)))
      if(hSearchFile.eq.INVALID_HANDLE_VALUE) go to 9999
      do while(FindNextFileA(carg(hSearchFile),
     1                       carg(offset(FindData))))
        if(iand(FindData.dwFileAttributes,FILE_ATTRIBUTE_DIRECTORY).ne.
     1          FILE_ATTRIBUTE_DIRECTORY) then
          if(NFiles.ge.NFilesMax) then
            allocate(FileListO(NFilesMax))
            do i=1,NFiles
              FileListO(i)=FileList(i)
            enddo
            deallocate(FileList)
            NFilesMax=2*NFilesMax
            allocate(FileList(NFilesMax))
            do i=1,NFiles
              FileList(i)=FileListO(i)
            enddo
            deallocate(FileListO)
          endif
          NFiles=NFiles+1
          Veta=FindData.cFileName
          i=index(Veta,char(0))
          FileList(NFiles)=Veta(:i-1)
          cycle
        endif
        if(iand(FindData.dwFileAttributes,FILE_ATTRIBUTE_DIRECTORY).eq.
     1          FILE_ATTRIBUTE_DIRECTORY) then
          if(NDirs.ge.NDirsMax) then
            allocate(DirListO(NDirsMax))
            do i=1,NDirs
              DirListO(i)=DirList(i)
            enddo
            deallocate(DirList)
            NDirsMax=2*NDirsMax
            allocate(DirList(NDirsMax))
            do i=1,NDirs
              DirList(i)=DirListO(i)
            enddo
            deallocate(DirListO)
          endif
          NDirs=NDirs+1
          Veta=FindData.cFileName
          i=index(Veta,char(0))
          DirList(NDirs)=Veta(:i-1)
        endif
      enddo
      call FindClose(carg(hSearchFile))
9999  return
      end
      subroutine FeDirName(CurrentDir)
      use Jana_windows
      character*(*) CurrentDir
      n=GetCurrentDirectoryA(carg(len(CurrentDir)),
     1                       carg(offset(CurrentDir)))
      CurrentDir(n+1:)=' '
      return
      end
      integer function FeChDir(Directory)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Directory
      character*260 Veta
      character*4 Flt
      logical ExistFile,io3
      FeChDir=0
      i=idel(Directory)
      if(i.le.0) go to 9999
      Veta=Directory(:i)//char(0)
      call FeMouseShape(3)
      n=0
1000  io3=SetCurrentDirectoryA(carg(offset(Veta)))
      if(.not.io3.and.n.lt.50) then
        n=n+1
        go to 1000
      endif
      call FeGetCurrentDir
      call FeMouseShape(0)
      if(.not.io3) then
        FeChDir=1
      else
        ln=NextLogicNumber()
        Flt='xy00'
        i=0
1500    write(Flt(3:4),100) i
        if(Flt(3:3).eq.' ') Flt(3:3)='0'
        if(ExistFile(Flt)) then
          i=i+1
          if(i.gt.50) go to 2000
          go to 1500
        endif
        open(ln,file=Flt,err=2000)
        i=-1
        write(ln,100,err=2000) i
        go to 3000
2000    FeChDir=-1
      endif
3000  if(i.eq.-1) close(ln,status='delete')
9999  return
100   format(i2)
      end
      subroutine FeMkDir(Veta,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Veta
      character*256 t256,CurrentDirIn
      integer FeChdir
      CurrentDirIn=CurrentDir
      ich=0
      i=LocateSubstring(Veta,':',.false.,.true.)
      kk=0
      if(i.gt.0) then
        if(i.ne.2) then
          go to 9000
        else
          n=0
1100      i=FeChDir(Veta(1:3))
          if(i.ne.0) then
            if(n.lt.5) then
              n=n+1
              go to 1100
            endif
            go to 9000
          endif
        endif
        kk=3
      endif
      idlv=idel(Veta)
1500  if(kk.ge.idlv) go to 9999
      kp=kk+1
      kk=LocateSubstring(Veta(kp:),ObrLom,.false.,.true.)+kp-1
      if(kk.le.kp-1) kk=idlv
      if(kk.gt.kp) then
        call CreateDirectoryA(carg(Veta//char(0)),carg(NULL))
        i=FeChDir(Veta(:kk))
        if(i.ne.0) go to 9000
      endif
      go to 1500
9000  ich=1
9999  i=FeChDir(CurrentDirIn)
      return
      end
      subroutine FeDirList(List)
      use FileDirList_mod
      include 'fepc.cmn'
      character*(*) List
      character*256 t256
      integer ipor(:)
      allocatable ipor
      call FeGetFileDirList(CurrentDir)
      ln=NextLogicNumber()
      open(ln,file=List,err=9000)
      allocate(ipor(NDirs))
      call SortTextIgnoreCase(NDirs,DirList,ipor)
      do i=1,NDirs
        k=ipor(i)
        write(ln,FormA) '['//DirList(k)(:idel(DirList(k)))//']'
      enddo
      deallocate(ipor)
      allocate(ipor(NFiles))
      call SortTextIgnoreCase(NFiles,FileList,ipor)
      do i=1,NFiles
        k=ipor(i)
        write(ln,FormA) FileList(k)(:idel(FileList(k)))
      enddo
      deallocate(ipor)
      go to 9900
9000  ErrFlag=1
9900  call CloseIfOpened(ln)
      return
      end
      subroutine GetActiveDrives(Drives,n)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Drives(*)
      Mask=GetLogicalDrives()
      n=0
      do i=0,25
        if(ibits(Mask,i,1).eq.1) then
          n=n+1
          Drives(n)=char(ichar('A')+i)//':'
        endif
      enddo
      return
      end
      subroutine FeExceptionInfo()
      include 'fepc.cmn'
      character*80 t80
      character*25 ErrString(0:4)
      logical BTest
      data ErrString/'"Invalid Operation"','"Denormalized Number"',
     1               '"Divide by Zero"','"Overflow"','"Underflow"'/
c      j=ndperr(.true.)
      NInfo=0
c      do 1000i=0,4
c        if((i.eq.1.or.i.eq.4).and.DelejKontroly.le.1) go to 1000
c        if(BTest(j,i)) then
c          NInfo=Ninfo+1
c          TextInfo(Ninfo)=ErrString(i)
c        endif
c1000  continue
      if(NInfo.ne.0) then
        if(NInfo.eq.1) then
          t80=' has'
        else
          t80='s have'
        endif
        t80='The following exception'//t80(:idel(t80))//
     1      ' occured during the last action:'
        call FeInfoOut(-1.,-1.,t80,'L')
      endif
c      call ndpexc()
      return
      end
      subroutine GetFln
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 p256,s256
      integer FeChdir,Delka
      logical EqIgCase
      BatchMode=.false.
      HomeDir=' '
      call getcl(CommandLine)

c    absoft
c      call get_command(CommandLine,Delka,ich)
c      if(ich.ne.0) go to 9999
c    absoft
      k=0
      n=0
      VasekDebug=0
      HKLUpdate=.false.
      DirectoryDelimitor=ObrLom
1000  kp=k
      call kusap(CommandLine,k,p256)
      if(p256(1:1).eq.'@') then
        BatchMode=.true.
        BatchFile=p256(2:)
        open(BatchLN,file=BatchFile)
        call FeGetCurrentDir
        BatchDir=CurrentDir
        n=n+10000
      else if(EqIgCase(p256,'-debug')) then
        VasekDebug=1
        n=n+1000
      else if(EqIgCase(p256,'-autohklupdate')) then
        HKLUpdate=.true.
        n=n+100
      else if(EqIgCase(p256,'-HomeDir')) then
        call kusap(CommandLine,k,HomeDir)
        n=n+10
      else
        if(p256(1:1).ne.'"'.and.k.lt.256) then
1500      i=k
          call kusap(CommandLine,k,s256)
          if(s256(1:1).ne.'@'.and..not.EqIgCase(s256,'-debug').and.
     1       .not.EqIgCase(s256,'-autohklupdate').and.
     2       .not.EqIgCase(s256,'-HomeDir')) then
            if(k.lt.256) then
              go to 1500
            else
              i=k
            endif
          endif
          p256=CommandLine(kp+1:i)
          k=i
        endif
        ifln=idel(p256)
        if(ifln.le.0) then
          fln=' '
        else
          call ExtractDirectory(p256,s256)
          i=FeChdir(s256)
          call ExtractFileName(p256,s256)
          call GetPureFileName(s256,fln)
          ifln=idel(fln)
          n=n+1
        endif
      endif
      if(n.ne.11111.and.k.lt.256) go to 1000
9999  return
      end
      subroutine FeEdit(File)
      include 'fepc.cmn'
      character*(*) File
      character*256 t256
      logical ExistFile,WizardModeOld
      WizardModeOld=WizardMode
      WizardMode=.false.
      if(.not.ExistFile(EditorName)) then
        call FeChybne(-1.,-1.,'the editor "'//
     1                EditorName(:idel(EditorName))//
     2                '" could not be found.',
     3                'Please redefine the editor name in '//
     4                'Tools->Programs.',SeriousError)
        go to 9999
      endif
      t256=EditorName(:idel(EditorName))//' "'//
     1                file(:idel(file))//'"'
      if(WineKey.eq.0) then
        call FeSystemCommand(t256,0)
        call FeWaitInfo('end of your editing.')
      else
        call FeSystem(t256)
      endif
9999  WizardMode=WizardModeOld
      return
      end
      subroutine FeGraphicViewer(FileName,Navrat)
      include 'fepc.cmn'
      character*(*) FileName
      character*256 t256
      i=idel(CallGraphic)
      if(CallGraphic(i:i).eq.'&') i=i-1
      t256=CallGraphic
      if(LocateSubstring(t256,'atoms',.false.,.true.).gt.0) then
        NInfo=3
        TextInfo(1)='JANA2006 will now start the ATOMS program to '//
     1              'draw your structure.'
        TextInfo(2)='The pre-prepared file named "'//
     1               FileName(:idel(FileName))//'" can be read in '
        TextInfo(3)='by the internal import command.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        Klic=1
      else
        Klic=0
      endif
      if(Klic.eq.1) then
        t256=CallGraphic(:i)
      else
        t256=CallGraphic(:i)//' '//FileName(:idel(FileName))
      endif
      if(Navrat.eq.0) t256=t256(:idel(t256))//'&'
      call FeSystem(t256)
      return
      end
      subroutine FeSetGraphicProgram
      include 'fepc.cmn'
      include 'basic.cmn'
      character*4 String
      character*55 :: RegSubKey(2) = (/
     1   'Software\Crystal Impact\Diamond 3\Import Assistant\',
     2   'Software\Crystal Impact\Diamond 3\Picture\         '/)
      character*15 :: ValueName(2) = (/'Enable     ',
     1                                 'AutoStartTS'/)
      integer ::
     1        ValueOld(2),ValueNew(2)=(/0,1/),ValueDefault(2)=(/1,2/)
      logical FeGetStringFromReg
      save ValueOld
      RegSubKey(1)=RegSubKey(1)(:idel(RegSubKey(1)))//char(0)
      RegSubKey(2)=RegSubKey(2)(:idel(RegSubKey(2)))//char(0)
      ValueName(1)=ValueName(1)(:idel(ValueName(1)))//char(0)
      ValueName(2)=ValueName(2)(:idel(ValueName(2)))//char(0)
      if(LocateSubstring(CallGraphic,'Diamond.exe',
     1                   .false.,.true.).le.0) go to 9999
      do i=1,2
        if(FeGetStringFromReg(RegSubKey(i),ValueName(i),String)) then
          ValueOld(i)=0
          do k=4,1,-1
            ValueOld(i)=256*ValueOld(i)+ichar(String(k:k))
          enddo
        else
          ValueOld(i)=ValueDefault(i)
        endif
        if(ValueNew(i).ne.ValueOld(i)) then
          k=ValueNew(i)
          do j=1,4
            kk=mod(k,256)
            String(j:j)=char(kk)
            k=k/256
          enddo
          call FeSetRegDWORD(RegSubKey(i),ValueName(i),String)
        endif
      enddo
      go to 9999
      entry FeResetGraphicProgram
      if(LocateSubstring(CallGraphic,'Diamond.exe',
     1                   .false.,.true.).le.0) go to 9999
      do i=1,2
        if(ValueNew(i).ne.ValueOld(i)) then
          k=ValueOld(i)
          do j=1,4
            kk=mod(k,256)
            String(j:j)=char(kk)
            k=k/256
          enddo
          call FeSetRegDWORD(RegSubKey(i),ValueName(i),String)
        endif
      enddo
9999  return
      end
      logical function FeGetStringFromReg(RegSubKey,ValueName,String)
      use Jana_windows
      character*(*) RegSubKey,ValueName,String
      integer RegKey
      i=RegOpenKeyExA(carg(HKEY_CURRENT_USER),
     1                carg(offset(RegSubKey)),carg(NULL),
     2                carg(KEY_READ),carg(offset(RegKey)))
      if(i.eq.0) then
        idl=len(String)
        i=RegQueryValueExA(carg(RegKey),
     1                     carg(offset(ValueName)),carg(NULL),
     2                     carg(offset(j)),carg(offset(String)),
     3                     carg(offset(idl)))
        FeGetStringFromReg=i.eq.0
      else
        FeGetStringFromReg=.false.
      endif
      return
      end
      subroutine FeSetRegDWORD(RegSubKey,ValueName,String)
      use Jana_windows
      character*(*) RegSubKey,ValueName,String
      integer RegKey
      i=RegOpenKeyExA(carg(HKEY_CURRENT_USER),
     1                carg(offset(RegSubKey)),carg(NULL),
     2                carg(KEY_ALL_ACCESS),carg(offset(RegKey)))
      if(i.eq.0) then
        i=RegSetValueExA(carg(RegKey),carg(offset(ValueName)),
     1                   carg(NULL),carg(REG_DWORD),
     2                   carg(offset(String)),carg(4))
      endif
      return
      end
      subroutine FeWaitInfo(VetaIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) VetaIn
      character*256 Veta
c      integer StTime,FeGetSystemTime
      logical FeSystemCommandFinished
      id=NextQuestId()
      Veta='The Jana2006 program is waiting for '//VetaIn(:idel(VetaIn))
      xqd=FeTxLength(Veta)+20.
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,il,'INFORMATION:',0,
     1                   LightGray,-1,-1)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','N')
      call FeReleaseOutput
      call FeDeferOutput
c      StTime=FeGetSystemTime()
c      call FeMakeLowerPriority
1500  call FeEvent(1)
      call FeMouseShape(0)
      if(.not.FeSystemCommandFinished()) then
        call FeWait(.1)
        call FeReleaseOutput
        call FeDeferOutput
        go to 1500
      endif
      call FeReturnOriginalPriority
      call FeQuestRemove(id)
      return
      end
      subroutine FeOsVariable(VariableName,Variable)
      use Jana_windows
      character*(*) VariableName,Variable
      Variable=' '
      j=GetEnvironmentVariableA(carg(VariableName(:idel(VariableName))),
     1                          carg(offset(Variable)),
     2                          carg(len(Variable)))
      Variable(j+1:)=' '
      return
      end
      subroutine CreateTmpFile(FileName,LFileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      character*4 Frmt
      logical exist
      FileName=TmpDir(:idel(TmpDir))//FileName(:idel(FileName))
      if(klic.eq.0) then
        FileName=FileName(:idel(FileName))//'0000.tmp'
      else if(klic.eq.1) then
        FileName=FileName(:idel(FileName))//'0000.bmp'
      else if(klic.eq.2) then
        FileName=FileName(:idel(FileName))//'0000.ps'
      endif
      LFileName=idel(FileName)
      j=LFileName-4
      k=j
      i=0
      Frmt='(i1)'
1200  inquire(file=FileName,exist=exist)
      if(Exist) then
        i=i+1
        if(i.eq.10) then
          k=k-1
          Frmt(3:3)='2'
        else if(i.eq.100) then
          k=k-1
          Frmt(3:3)='3'
        else if(i.eq.1000) then
          k=k-1
          Frmt(3:3)='4'
        endif
        write(FileName(k:j),Frmt) i
        go to 1200
      endif
      return
      end
      subroutine FeDateAndTime(ch9,ch8)
      include 'fepc.cmn'
      character*10 ch10
      character*(*) ch8,ch9
      dimension it(8)
      call Date_and_Time(ch8,ch10,ch9,it)
      ch9=ch8(7:8)//'-'//ch8(5:6)//'-'//ch8(3:4)
      ch8=ch10(1:2)//':'//ch10(3:4)//':'//ch10(5:6)
      if(it(1).lt.0) ch9='???'
      return
      end
      function FeEtime()
      call CPU_Time(CPUTime)
      FeETime=CPUTime
      return
      end
      function FeTimeDiff()
      include 'fepc.cmn'
      logical First
      integer FeGetSystemTime
      save TimeOld
      data first/.true./
      Time=float(FeGetSystemTime())/1000.
      if(First) then
        TimeOld=Time
        First=.false.
      endif
      FeTimeDiff=Time-TimeOld
      TimeOld=Time
      return
      end
      subroutine FeWait(seconds)
      use Jana_windows
      call Sleep(carg(nint(seconds*1000.)))
      return
      end
      integer function FeGetSystemTime()
      include 'fepc.cmn'
      call timer(i)
      FeGetSystemTime=i*10
      return
      end
      subroutine FeMessageBox(Veta2,Veta1)
      use Jana_windows
      character*(*) Veta1,Veta2
      i=MessageBoxA(carg(0),carg(Veta1(:idel(Veta1))),
     1              carg(Veta2(:idel(Veta2))),carg(MB_OK))
      return
      end
      subroutine FeCopyFile(File1,File2)
      use Jana_windows
      logical lpom
      character*(*) File1,File2
      character*260 File1p,File2p
      File1p=File1(:idel(File1))//char(0)
      File2p=File2(:idel(File2))//char(0)
      lpom=CopyFileA(carg(offset(File1p)),carg(offset(File2p)),
     1               carg(FALSE))
      return
      end
      subroutine FeAllocErr(Text,i)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text
      NInfo=4
      call iostat_msg(i,TextInfo(1))
      TextInfo(2)=Text
      TextInfo(3)='Please contact Jana authors and send them this '//
     1            'message and'
      TextInfo(4)='files m40, [m41], m50, m90 and m95.'
      call FeInfoOut(-1.,-1.,'ALLOCATION ERROR','L')
      ErrFlag=1
      return
      end      
      subroutine FeWinMessage(St1,St2)
      include 'fepc.cmn'
      character*(*) St1,St2
      character*80 TextInfo1,TextInfo2
      logical FeYesNoHeader
      i=EventType
      j=EventNumber
      n=NInfo
      TextInfo1=TextInfo(1)
      TextInfo2=TextInfo(2)
      Ninfo=1
      TextInfo(1)=St1
      if(St2.ne.' ') then
        Ninfo=2
        TextInfo(2)=St2
      endif
      if(FeYesNoHeader(-1.,-1.,'Co Ty na to? Skoncit?',0)) then
        write(6,'(f10.5)') St1
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        stop
      endif
      NInfo=n
      TextInfo(1)=TextInfo1
      TextInfo(2)=TextInfo2
      EventType=i
      EventNumber=j
      return
      end
      integer function FeFileSize(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      inquire(file=FileName,flen=FeFileSize,err=9000)
      go to 9999
9000  FeFileSize=-1
9999  return
      end
      integer function FeOpenBinaryFile(FileName)
      use Jana_windows
      character*(*) FileName
      ln=NextLogicNumber()
      open(ln,file=FileName,access='TRANSPARENT',form='BINARY')
      FeOpenBinaryFile=ln
      return
      end
      integer function FeCloseBinaryFile(ln)
      include 'fepc.cmn'
      close(ln)
      FeCloseBinaryFile=0
      return
      end
      integer function FeReadBinaryFile(ln,String,Size)
      include 'fepc.cmn'
      character*1 String(*)
      integer Size
      do i=1,Size
        read(ln,err=9000) String(i)
      enddo
      FeReadBinaryFile=Size
      go to 9999
9000  FeReadBinaryFile=i
9999  return
      end
      integer function FeWriteBinaryFile(ln,String,Size)
      include 'fepc.cmn'
      character*1 String(*)
      integer Size
      write(ln)(String(i),i=1,Size)
      FeWriteBinaryFile=0
      return
      end
      subroutine FeNull()
      include 'fepc.cmn'
      return
      end
      logical function FeReadOnly(FileName)
      use Jana_windows
      character*(*) FileName
      i=GetFileAttributesA(carg(FileName))
      FeReadOnly=iand(i,FILE_ATTRIBUTE_READONLY).ne.0
      return
      end
      subroutine FeResetReadOnly(FileName)
      use Jana_windows
      character*(*) FileName
      call SetFileAttributesA(carg(FileName),
     1                        carg(FILE_ATTRIBUTE_ARCHIVE))
      return
      end
      subroutine FeSystem(Command)
      include 'fepc.cmn'
      character*(*) Command
      logical FeSystemCommandFinished,Neceka
      i=idel(Command)
      Neceka=Command(i:i).eq.'&'
      if(Neceka) i=i-1
      call FeSystemCommand(Command(:i),0)
      if(.not.Neceka) call FeWaitInfo('end.')
      return
      end
      subroutine FeSystemCommand(CommandLine,HowToStart)
      use Jana_windows
      character*(*) CommandLine
      character*260 VetaP
      integer HowToStart
      logical lpom
      type(STARTUPINFO) si
      type(PROCESS_INFORMATION) pi
      common/SysCommand/ pi
      si.cb=17*4
      si.lpReserved=0
      si.lpDesktop=0
      si.lpTitle=0
      si.dwX=0
      si.dwY=0
      si.dwXSize=0
      si.dwYSize=0
      si.dwXCountChars=0
      si.dwYCountChars=0
      si.dwFillAttribute=0
      if(HowToStart.eq.1) then
        si.dwFlags=1
        si.wShowWindow=7
      else
        si.dwFlags=0
        si.wShowWindow=0
      endif
      si.cbReserved2=0
      si.lpReserved2=0
      si.hStdInput=0
      si.hStdOutput=0
      si.hStdError=0
      pi.hProcess=0
      pi.hThread=0
      pi.dwProcessId=0
      pi.dwThreadId=0
      VetaP=CommandLine(:idel(CommandLine))//char(0)
      lpom=CreateProcessA(carg(NULL),carg(offset(VetaP)),
     1                   carg(NULL),carg(NULL),carg(FALSE),carg(0),
     2                   carg(NULL),carg(NULL),
     3                   carg(offset(si)),carg(offset(pi)))
9999  return
      end
      logical function FeSystemCommandFinished()
      use Jana_windows
      integer ExitCode
      type(PROCESS_INFORMATION) pi
      common/SysCommand/ pi
      ExitCode=0
      call GetExitCodeProcess(carg(pi.hProcess),
     1                        carg(offset(ExitCode)))
      FeSystemCommandFinished=ExitCode.ne.STILL_ACTIVE
      return
      end
      subroutine FeShell
      include 'fepc.cmn'
      character*256 t256
      i=index(FirstCommand,'%p')
      if(i.ne.0) then
        t256=FirstCommand(:i-1)//'"'//CurrentDir(:idel(CurrentDir))//'"'
        if(i+1.lt.idel(FirstCommand))
     1    t256=t256(:idel(t256))//FirstCommand(i+2:)
      else
        t256=FirstCommand
      endif
      call FeSystem(t256)
      return
      end
      subroutine FeShellExecute(Veta)
      use Jana_windows
      character*(*) Veta
      character*80 Action,Prazdna
      character*260 VetaP
      VetaP=Veta(:idel(Veta))//char(0)
      Action='open'//char(0)
      Prazdna=char(0)
      call ShellExecuteA(carg(offset(Prazdna)),carg(offset(Action)),
     1                   carg(offset(VetaP)),
     2                   carg(offset(Prazdna)),carg(offset(Prazdna)),
     3                   carg(SW_SHOWNORMAL))
      return
      end


C          ****************************
C          **       Kresleni        ***
C          ****************************


      subroutine FeDrawSegments(n,px1,py1,px2,py2,Color)
      include 'fepc.cmn'
      dimension px1(*),py1(*),px2(*),py2(*),x(2),y(2)
      integer Color(*)
      do i=1,n
        x(1)=px1(i)
        x(2)=px2(i)
        y(1)=py1(i)
        y(2)=py2(i)
        call FePolyLineSolid(x,y,2,Color(i))
      enddo
      return
      end
      subroutine FeCircle(x,y,r,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,xss,yss,xjj,yjj,rs,Style,RGB
      character*80 Veta
      Style=0
      go to 1100
      entry FeCircleOpen(x,y,r,Color)
      Style=1
1100  call JanaToClient(x,y,xs,ys)
      rs=nint(r*EnlargeFactor)
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do ix=-rs,rs
        do iy=-rs,rs
          pom=sqrt(float(ix)**2+float(iy)**2)-float(rs)
          if(pom.lt..5) then
            if(Style.eq.0.or.pom.ge.-.5) then
              xjj=xs+ix
              yjj=ys+iy
              call SetPixel(carg(hDCComp),carg(xjj),carg(yjj),
     1                      carg(RGB(IRed,IGreen,IBlue)))
              if(hDCMeta.ne.0)
     1          call SetPixel(carg(hDCMeta),carg(xjj),carg(yjj),
     2                        carg(RGB(IRed,IGreen,IBlue)))
              ix1=min(ix1,xjj)
              ix2=max(ix2,xjj)
              iy1=min(iy1,yjj)
              iy2=max(iy2,yjj)
              if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS)
     1            .and.LnPS.gt.0) then
                if(Color.ne.LastColorPS) then
                  write(LnPS,'(''s'')')
                  write(Veta,'(3f8.3,'' g'')')
     1              float(IRed)/255.,float(IGreen)/255.,
     2              float(IBlue)/255.
                  call ZdrcniCisla(Veta,4)
                  write(LnPS,FormA) Veta(:idel(Veta))
                  LastColorPS=Color
                endif
                call JanaPixelToClientReverse(xjj,yjj,xss,yss)
                xp=float(xss-XPocPS)*ScalePS
                yp=float(yss-YPocPS)*ScalePS
                write(Veta,'(2f10.2,'' m cb'')') xp,yp
                call ZdrcniCisla(Veta,4)
                write(LnPS,FormA) Veta(:idel(Veta))
              endif
            endif
          endif
        enddo
      enddo
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
      subroutine FePoint(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,xss,yss,xj,yj,xjj,yjj,RGB
      character*80 Veta
      call JanaToClient(x,y,xs,ys)
      xjj=nint(x*EnlargeFactor)
      yjj=nint(y*EnlargeFactor)
      go to 1100
      entry FePixelPoint(xj,yj,Color)
      xjj=xj
      yjj=yj
      call JanaPixelToClient(xj,yj,xs,ys)
1100  call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call SetPixel(carg(hDCComp),carg(xs),carg(ys),
     1              carg(RGB(IRed,IGreen,IBlue)))
      call FeUpdateDisplay(xs,xs,ys,ys)
      if(hDCMeta.ne.0)
     1  call SetPixel(carg(hDCMeta),carg(xs),carg(ys),
     2                carg(RGB(IRed,IGreen,IBlue)))
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaPixelToClientReverse(xjj,yjj,xss,yss)
        xp=float(xss-XPocPS)*ScalePS
        yp=float(yss-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m cb'')') xp,yp
        call ZdrcniCisla(Veta,4)
        write(LnPS,FormA) Veta(:idel(Veta))
      endif
      return
      end
      subroutine FeArc(x,y,r,sa,aa,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer Color,xs,ys,rs,RGB
      call JanaToClient(x,y,xs,ys)
      rs=nint(r*EnlargeFactor)
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do ix=-rs,rs
        do iy=-rs,rs
          if(ix.ne.0.or.iy.ne.0) then
            Uhel=atan2(float(-iy),float(ix))/ToRad
          else
            Uhel=(sa+aa)*.5
          endif
1100      if(Uhel.lt.sa) then
            Uhel=Uhel+360.
            go to 1100
          endif
1200      if(Uhel.gt.sa+aa) then
            Uhel=Uhel-360.
            go to 1200
          endif
          if(Uhel.lt.sa) cycle
          pom=sqrt(float(ix)**2+float(iy)**2)-float(rs)
          if(pom.lt..5.and.pom.ge.-.5) then
            call SetPixel(carg(hDCComp),carg(xs+ix),carg(ys+iy),
     1                    carg(RGB(IRed,IGreen,IBlue)))
            ix1=min(ix1,xs+ix)
            ix2=max(ix2,xs+ix)
            iy1=min(iy1,ys+iy)
            iy2=max(iy2,ys+iy)
          endif
        enddo
      enddo
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
      subroutine FePolyline(n,x,y,Color)
      include 'fepc.cmn'
      dimension x(n),y(n)
      integer Color
      logical DeferredOutputIn
      DeferredOutputIn=DeferredOutput
      if(.not.DeferredOutput) call FeDeferOutput
      if(LineType.eq.NormalLine) then
        call FeMoveTo(x(1),y(1),Color)
      else if(LineType.eq.DashedLine.or.
     1        LineType.eq.DenseDashedLine) then
        call Carka(x(1),y(1),0,Color)
      else
        call FeDottedLine(x(1),y(1),0,Color)
      endif
      do i=2,n
        if(LineType.eq.NormalLine) then
          call FeLineTo(x(i),y(i),Color)
        else if(LineType.eq.DashedLine.or.
     1          LineType.eq.DenseDashedLine) then
          call Carka(x(i),y(i),1,Color)
        else
          call FeDottedLine(x(i),y(i),1,Color)
        endif
      enddo
      if(.not.DeferredOutputIn) DeferredOutput=.false.
      call FeUpdateDisplay(-1,-1,-1,-1)
      return
      end
      subroutine FeDottedLine(x,y,ik,Color)
      include 'fepc.cmn'
      integer Color
      save xs,ys,ic
      if(ik.le.0) then
        xs=x
        ys=y
        call FePoint(xs,ys,Color)
        ic=1
      else
2100    difx=x-xs
        dify=y-ys
        delka=sqrt(difx**2+dify**2)
        if(ic.eq.0) then
          fus=0.
        else
          if(LineType.eq.DottedLine) then
            fus=3.
          else
            fus=2.
          endif
        endif
        if(delka.lt.fus.or.delka.le.0.) go to 9000
        ic=mod(ic+1,2)
        dx=fus*difx/delka
        dy=fus*dify/delka
        xs=anint((xs+dx)*EnlargeFactor)/EnlargeFactor
        ys=anint((ys+dy)*EnlargeFactor)/EnlargeFactor
        if(ic.eq.0) call FePoint(xs,ys,Color)
        go to 2100
      endif
9000  return
      end
      subroutine carka(x,y,ik,Color)
      include 'fepc.cmn'
      dimension dc(2)
      integer Color
      save xs,ys,ic,zbytek
      data dc/5.,5./
      if(ik.le.0) then
        xs=x
        ys=y
        call FeMoveTo(x,y,Color)
        ic=2
        zbytek=0.
      else
        difx=x-xs
        dify=y-ys
        delka=sqrt(difx**2+dify**2)
        if(delka.gt.0.) then
          dx=difx/delka
          dy=dify/delka
        else
          dx=0.
          dy=0.
          go to 9000
        endif
        di=delka
2100    if(zbytek.le.0.) then
          ic=mod(ic,2)+1
          if(LineType.eq.DenseDashedLine) then
            fus=dc(ic)
          else
            fus=2.*dc(ic)
          endif
        else
          fus=zbytek
        endif
        if(fus.le.di) then
          zbytek=0.
        else
          zbytek=fus-di
          fus=di
        endif
        xs=anint(xs+fus*dx)
        ys=anint(ys+fus*dy)
        if(ic.eq.1) then
          call FeLineTo(xs,ys,Color)
        else
          call FeMoveTo(xs,ys,Color)
        endif
        di=di-fus
        if(di.gt.0.) go to 2100
      endif
9000  return
      end
      subroutine FeMoveTo(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer xs,ys,Color
      character*40 Veta
      call JanaToClient(x,y,xs,ys)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call MoveToEx(carg(hDCComp),carg(xs),carg(ys),carg(NULL))
      if(hDCMeta.ne.0)
     1  call MoveToEx(carg(hDCMeta),carg(xs),carg(ys),carg(NULL))
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          call FeRGBUncompress(Color,IRed,IGreen,IBlue)
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x,y,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        if(abs(xp-LastXPS).gt..01.or.abs(yp-LastYPS).gt..01) then
          write(Veta,'(2f10.2,'' m'')') xp,yp
          call ZdrcniCisla(Veta,3)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastXPS=xp
          LastYPS=yp
        endif
      endif
      call FeUpdateDisplay(xs,xs,ys,ys)
      return
      end
      subroutine FeLineTo(x,y,Color)
      use Jana_windows
      include 'fepc.cmn'
      integer xs,ys,Color,RGB,hpen
      character*40 Veta
      call JanaToClient(x,y,xs,ys)
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      hpen=CreatePen(carg(PS_SOLID),carg(0),
     1               carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCComp),carg(hPen))
      call LineTo(carg(hDCComp),carg(xs),carg(ys))
      if(hDCMeta.ne.0) then
        call SelectObject(carg(hDCMeta),carg(hPen))
        call LineTo(carg(hDCMeta),carg(xs),carg(ys))
      endif
      call DeleteObject(carg(hpen))
      call FeUpdateDisplay(xs,xs,ys,ys)
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x,y,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        if(abs(xp-LastXPS).gt..01.or.abs(yp-LastYPS).gt..01) then
          write(Veta,'(2f10.2,'' l'')') xp,yp
          call ZdrcniCisla(Veta,3)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastXPS=xp
          LastYPS=yp
          LastColorPS=Color
        endif
      endif
      return
      end
      subroutine FeLineType(LineTypeIn)
      include 'fepc.cmn'
      LineType=LineTypeIn
      return
      end
      subroutine FeCharOut(id,xmi,ymi,Text,Justify,Color)
      use Jana_windows
      include 'fepc.cmn'
      character*1 Justify
      character*(*) Text
      character*256 TextP,Veta
      integer Color,xs,ys
      xm=xmi
      ym=ymi
      if(id.gt.0) then
        if(QuestState(id).ne.0) then
          xm=xm+QuestXMin(id)
          ym=ym+QuestYMin(id)
        endif
      endif
      if(Justify.eq.'C') then
        xm=xm-FeTxLength(Text)*.5
      else if(Justify.eq.'R') then
        xm=xm-FeTxLength(Text)
      endif
      yd=PropFontHeightInPixels/EnlargeFactor
      ymm=ym+.5*yd
      call JanaToClient(xm,ymm,ix1,iy1)
      call JanaToClient(xm+FeTxLength(Text),ymm-yd,ix2,iy2)
      TextP=Text
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call FeTextOut(hDCComp,ix1,ix2,iy1,iy2,Color,TextP)
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          call FeRGBUncompress(Color,IRed,IGreen,IBlue)
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(xm,ymm-yd,xs,ys)
        xp=float(xs-XPocPS)*ScalePS
        yp=float(ys-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m'')') xp,yp
        call ZdrcniCisla(Veta,3)
        write(LnPS,FormA) Veta(:idel(Veta))
        write(LnPS,FormA) '('//Text(:idel(Text))//') lsh'
      endif
      return
      end
      subroutine FeNormalFont
      use Jana_windows
      include 'fepc.cmn'
      if(PropFont) then
        i=hPropFontN
      else
        i=hFixFontN
      endif
      go to 2000
      entry FeBoldFont
      if(PropFont) then
        i=hPropFontB
      else
        i=hFixFontB
      endif
      go to 2000
      entry FeUnderlineFont
      go to 9999
2000  call SelectObject(carg(hDCComp),carg(i))
9999  return
      end
      subroutine FeSetFixFont
      use Jana_windows
      include 'fepc.cmn'
      call SelectObject(carg(hDCComp),carg(hFixFontN))
      PropFont=.false.
      return
      end
      subroutine FeSetPropFont
      use Jana_windows
      include 'fepc.cmn'
      call SelectObject(carg(hDCComp),carg(hPropFontN))
      PropFont=.true.
      return
      end
      subroutine FeGetTextRectangle(x,y,String,Justify,RecType,xmin,
     1                              xmax,ymin,ymax,refx,refy,conx,cony)
      include 'fepc.cmn'
      character*(*) Justify,String
      integer RecType
      xd=FeTxLengthUnder(String)
      if(Justify.eq.'L') then
        xmin=x
      else if(Justify.eq.'C') then
        xmin=x-xd*.5
      else if(Justify.eq.'R') then
        xmin=x-xd
      endif
      ys=y
      yd=FeTxHeight(String)*.5
      if(.not.PropFont) yd=yd-2.
      ymin=ys-yd-1.
      ymax=ys+yd+1.
      xmax=xmin+xd
      xmin=xmin-1.
      xmax=xmax+1.
      refx=x
      refy=y
      conx=xmax
      cony=y
      return
      end
      subroutine FeGetPureTextRectangle(x,y,String,Justify,RecType,xmin,
     1                                  xmax,ymin,ymax,refx,refy,conx,
     2                                  cony)
      include 'fepc.cmn'
      character*(*) Justify,String
      integer RecType
      xd=FeTxLength(String)
      if(Justify.eq.'L') then
        xmin=x
      else if(Justify.eq.'C') then
        xmin=x-xd*.5
      else if(Justify.eq.'R') then
        xmin=x-xd
      endif
      ys=y
      yd=FeTxHeight(String)*.5
      if(.not.PropFont) yd=yd-2.
      ymin=ys-yd
      ymax=ys+yd
      xmax=xmin+xd
      xmin=xmin-1.
      xmax=xmax+1.
      refx=x
      refy=y
      conx=xmax
      cony=y
      return
      end
      subroutine FeTextOut(hDCP,ix1,ix2,iy1,iy2,Color,String)
      use Jana_windows
      integer hDCP,Color,RGB
      character*(*) String
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      call SetBkMode(carg(hDCP),carg(TRANSPARENT))
      call SetTextColor(carg(hDCP),carg(RGB(IRed,IGreen,IBlue)))
      idl=idel(String)
      call TextOutA(carg(hDCP),carg(ix1),carg(iy1),carg(String(:idl)),
     1              carg(idl))
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      if(hDCMeta.ne.0) then
        call SetBkMode(carg(hDCMeta),carg(TRANSPARENT))
        call SetTextColor(carg(hDCMeta),carg(RGB(IRed,IGreen,IBlue)))
        call TextOutA(carg(hDCMeta),carg(ix1),carg(iy1),
     1                carg(String(:idl)),carg(idl))
      endif
      return
      end
      subroutine FeFillRectangle(xmin,xmax,ymin,ymax,istyle,idense,
     1                           iangle,color)
      include 'fepc.cmn'
      dimension xv(5),yv(5)
      integer color
      xv(1)=xmin
      yv(1)=ymin
      xv(2)=xmax
      yv(2)=ymin
      xv(3)=xmax
      yv(3)=ymax
      xv(4)=xmin
      yv(4)=ymax
      if(istyle.gt.0) then
        call FePolygon(xv,yv,4,istyle,idense,iangle,Color)
      else
        xv(5)=xmin
        yv(5)=ymin
        call FePolylineSolid(xv,yv,5,Color)
      endif
      return
      end
      subroutine FePolygon(x,y,n,istyle,idense,iangle,color)
      use Jana_windows
      include 'fepc.cmn'
      dimension x(n),y(n)
      integer xp(:),yp(:),xs,ys,Color
      character*80 Veta
      allocatable xp,yp
      Klic=0
      go to 1100
      entry FePolylineSolid(x,y,n,color)
      Klic=1
      go to 1100
      entry FePolylineDotted(x,y,n,color)
      Klic=2
1100  allocate(xp(n),yp(n))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do i=1,n
        call JanaToClient(x(i),y(i),xp(i),yp(i))
        ix1=min(ix1,xp(i))
        ix2=max(ix2,xp(i))
        iy1=min(iy1,yp(i))
        iy2=max(iy2,yp(i))
      enddo
      if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1    LnPS.gt.0) then
        call FeRGBUncompress(Color,IRed,IGreen,IBlue)
        if(Color.ne.LastColorPS) then
          write(LnPS,'(''s'')')
          write(Veta,'(3f8.3,'' g'')')
     1      float(IRed)/255.,float(IGreen)/255.,float(IBlue)/255.
          call ZdrcniCisla(Veta,4)
          write(LnPS,FormA) Veta(:idel(Veta))
          LastColorPS=Color
        endif
        call JanaToClientReverse(x(1),y(1),xs,ys)
        xpp=float(xs-XPocPS)*ScalePS
        ypp=float(ys-YPocPS)*ScalePS
        write(Veta,'(2f10.2,'' m'')') xpp,ypp
        call ZdrcniCisla(Veta,3)
        write(LnPS,FormA) Veta(:idel(Veta))
      endif
      if(Klic.eq.0) then
        call FeFillPolygon(hDCComp,xp,yp,n,Color)
        if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1      LnPS.gt.0) then
          do i=2,n
            call JanaToClientReverse(x(i),y(i),xs,ys)
            xpp=float(xs-XPocPS)*ScalePS
            ypp=float(ys-YPocPS)*ScalePS
            write(Veta,'(2f10.2,'' l'')') xpp,ypp
            call ZdrcniCisla(Veta,3)
            write(LnPS,FormA) Veta(:idel(Veta))
          enddo
          write(LnPS,FormA) 'fill'
        endif
      else
        call FePolylineVarious(hDCComp,xp,yp,n,Color,Klic)
        if((HardCopy.eq.HardCopyPS.or.HardCopy.eq.HardCopyEPS).and.
     1      LnPS.gt.0) then
          if(Klic.eq.2) write(LnPS,FormA) '[1 1] 0 setdash'
          do i=2,n
            call JanaToClientReverse(x(i),y(i),xs,ys)
            xpp=float(xs-XPocPS)*ScalePS
            ypp=float(ys-YPocPS)*ScalePS
            write(Veta,'(2f10.2,'' l'')') xpp,ypp
            call ZdrcniCisla(Veta,3)
            write(LnPS,FormA) Veta(:idel(Veta))
          enddo
          write(LnPS,FormA) 's'
          if(Klic.eq.2) write(LnPS,FormA) '[] 0 setdash'
        endif
      endif
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      deallocate(xp,yp)
      return
      end
      subroutine FeFillPolygon(hDCP,x,y,n,Color)
      use Jana_windows
      integer hDCP,Color,RGB,hPen,hBrush
      integer x(*),y(*)
      type(POINT) pnt(:)
      character*80 Veta
      allocatable pnt
      allocate(pnt(n))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do i=1,n
        pnt(i).x=x(i)
        pnt(i).y=y(i)
        ix1=min(ix1,x(i))
        ix2=max(ix2,x(i))
        iy1=min(iy1,y(i))
        iy2=max(iy2,y(i))
      enddo
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      hPen=CreatePen(carg(PS_SOLID),carg(0),
     1               carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCP),carg(hPen))
      hBrush=CreateSolidBrush(carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCP),carg(hBrush))
      call SelectObject(carg(hDCP),carg(hDCBitmap))
      call Polygon(carg(hDCP),carg(offset(Pnt)),carg(n))
      if(hDCMeta.ne.0) then
        call SelectObject(carg(hDCMeta),carg(hPen))
        call SelectObject(carg(hDCMeta),carg(hBrush))
        call Polygon(carg(hDCMeta),carg(offset(Pnt)),carg(n))
      endif
      call DeleteObject(carg(hBrush))
      call DeleteObject(carg(hPen))
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      deallocate(pnt)
      return
      end
      subroutine FePolylineVarious(hDCP,x,y,n,Color,Klic)
      use Jana_windows
      integer hDCP,Color,RGB,hPen
      integer x(*),y(*)
      type(POINT) pnt(:)
      character*80 Veta
      allocatable pnt
      allocate(pnt(n))
      ix1= 99999
      ix2=-99999
      iy1= 99999
      iy2=-99999
      do i=1,n
        pnt(i).x=x(i)
        pnt(i).y=y(i)
        ix1=min(ix1,x(i))
        ix2=max(ix2,x(i))
        iy1=min(iy1,y(i))
        iy2=max(iy2,y(i))
      enddo
      call FeRGBUncompress(Color,IRed,IGreen,IBlue)
      if(Klic.eq.1) then
        i=PS_SOLID
      else
        i=PS_DOT
      endif
      hPen=CreatePen(carg(i),carg(0),
     1               carg(RGB(IRed,IGreen,IBlue)))
      call SelectObject(carg(hDCP),carg(hPen))
      call SelectObject(carg(hDCP),carg(hDCBitmap))
      call Polyline(carg(hDCP),carg(offset(Pnt)),carg(n))
      if(hDCMeta.ne.0) then
       call SelectObject(carg(hDCMeta),carg(hPen))
       call Polyline(carg(hDCMeta),carg(offset(Pnt)),carg(n))
      endif
      call DeleteObject(carg(hPen))
      call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      deallocate(pnt)
      return
      end
      subroutine FeMoveMouseTo(X,Y)
      use Jana_windows
      integer xs,ys
      if(hWnd.eq.GetActiveWindow()) then
        call JanaToClient(x,y,xs,ys)
        call JanaToScreen(x,y,xs,ys)
        call SetCursorPos(carg(xs),carg(ys))
      endif
      return
      end
      subroutine FePlotMode(Style)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Style
      integer PlotMode
      logical EqIgCase
      if(EqIgCase(Style,'E')) then
        PlotMode=R2_XORPEN
      else
        PlotMode=R2_COPYPEN
      endif
      call SetROP2(carg(hDCComp),carg(PlotMode))
      return
      end


C          ****************************
C          ***    Pomocne funkce    ***
C          ****************************


      subroutine JanaToClient(xj,yj,xc,yc)
      use Jana_windows
      include 'fepc.cmn'
      real xj,yj
      integer xc,yc,xs,ys,x,y
      Klic=0
      go to 1000
      entry JanaToClientReverse(xj,yj,xc,yc)
      Klic=2
      go to 1000
      entry JanaToScreen(xj,yj,xs,ys)
      Klic=1
1000  x=nint(xj*EnlargeFactor)
      if(Klic.eq.2) then
        y=PixelClientHeight-1+nint(yj*EnlargeFactor)
      else
        y=PixelClientHeight-1-nint(yj*EnlargeFactor)
      endif
      if(Klic.eq.0.or.Klic.eq.2) then
        xc=x
        yc=y
      else if(Klic.eq.1) then
        call FeClientToScreen(hWnd,x,y,xs,ys)
      endif
9999  return
      end
      subroutine ClientToJana(xc,yc,xj,yj)
      use Jana_windows
      include 'fepc.cmn'
      real xj,yj
      integer xc,yc,xs,ys,x,y
      x=xc
      y=yc
      go to 1000
      entry ScreenToJana(xj,yj,xs,ys)
      call FeScreenToClient(hWnd,xs,ys,x,y)
1000  xj=float(x)/EnlargeFactor
      yj=(PixelClientHeight-1-y)/EnlargeFactor
      return
      end
      subroutine FeScreenToClient(hWndP,sx,sy,cx,cy)
      use Jana_windows
      integer hWndP,cx,cy,sx,sy
      type(POINT) pnt
      pnt.x=sx
      pnt.y=sy
      i=ScreenToClient(carg(hWndP),carg(offset(pnt)))
      cx=pnt.x
      cy=pnt.y
      return
      end
      subroutine FeClientToScreen(hWndP,cx,cy,sx,sy)
      use Jana_windows
      integer hWndP,cx,cy,sx,sy
      type(POINT) pnt
      pnt.x=cx
      pnt.y=cy
      i=ClientToScreen(carg(hWndP),carg(offset(pnt)))
      sx=pnt.x
      sy=pnt.y
      return
      end
      subroutine JanaPixelToClient(xj,yj,xc,yc)
      use Jana_windows
      include 'fepc.cmn'
      integer xj,yj,xc,yc,xs,ys,x,y
      xc=xj
      yc=PixelClientHeight-1-yj
      return
      end
      subroutine JanaPixelToClientReverse(xj,yj,xc,yc)
      use Jana_windows
      include 'fepc.cmn'
      integer xj,yj,xc,yc,xs,ys,x,y
      xc=xj
      yc=PixelClientHeight-1+yj
      return
      end
      function FeTxLength(Veta)
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      VetaP=Veta
      i=index(VetaP,Tabulator)
      if(NTabs(UseTabs).le.0.or.i.le.0) then
        FeTxLength=FeTxLengthSpace(Veta(:idel(Veta)))
      else
        k=1
        it=0
        xm=0.
1100    i=index(VetaP(k:),Tabulator)
        if(i.le.0) go to 1500
        k=k+i
        it=it+1
        xm=xm+XTabs(it,UseTabs)
        if(it.gt.1) xm=xm-XTabs(it-1,UseTabs)
        go to 1100
1500    if(it.gt.0) then
          if(TTabs(it,UseTabs).eq.IdLeftTab) then
            xmp=xm-FeTxLengthSpace(Veta(k:idel(Veta)))
          else if(TTabs(it,UseTabs).eq.IdCharTab) then
            i=index(VetaP(k:),ChTabs(it,UseTabs))
            if(i.gt.1) then
              xmp=xm-FeTxLengthSpace(Veta(k:k+i-2))
            else
              xmp=xm
            endif
          else if(TTabs(it,UseTabs).eq.IdCenterTab) then
            xmp=xm-.5*FeTxLengthSpace(Veta(k:idel(Veta)))
          else
            xmp=xm
          endif
          FeTxLength=xmp+FeTxLengthSpace(Veta(k:idel(Veta)))
        else
          FeTxLength=FeTxLengthSpace(Veta(:idel(Veta)))
        endif
      endif
      return
      end
      function FeTxLengthSpace(Veta)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      type(SIZE) :: Sz
      VetaP=Veta
      idl=len(Veta)
      call GetTextExtentPoint32A(carg(hDCComp),carg(VetaP(:idl)),
     1                           carg(idl),carg(offset(Sz)))
      FeTxLengthSpace=float(Sz.cx)/EnlargeFactor
      return
      end
      function FeTxHeight(Veta)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) Veta
      character*256 VetaP
      integer ip1,ip2
      type(SIZE) :: Sz
      VetaP=Veta
      idl=len(Veta)
      call GetTextExtentPoint32A(carg(hDCComp),carg(Veta(:idel(Veta))),
     1                          carg(idel(Veta)),carg(offset(Sz)))
      FeTxHeight=float(Sz.cy)/EnlargeFactor
      return
      end
      subroutine FeSaveImage(xlow,xhigh,ylow,yhigh,File)
      include 'fepc.cmn'
      character*(*) File
      integer FeOpenBinaryFile,FeCloseBinaryFile,FeWriteBinaryFile
      logical EqIgCase
      call JanaToClient(xlow,ylow,ix1,iy1)
      call JanaToClient(xhigh,yhigh,ix2,iy2)
      if(EqIgCase(File,HCFileName)) go to 2000
      iu=0
      ip=0
      do i=1,MxBmp
        if(EqIgCase(File,BmpName(i))) then
          iu=i
          go to 1500
        endif
        if(ip.eq.0.and.BmpName(i).eq.' ') ip=i
      enddo
      if(ip.eq.0) go to 2000
      iu=ip
1500  BmpName(iu)=File
      if(iu.eq.ip) then
        IBmp(iu)=0
      else
        call FeBitmapDestroy(IBmp(iu))
      endif
      call FeBitmapGet(IBmp(iu),ix1,iy1,ix2,iy2)
      ln=NextLogicNumber()
      open(ln,file=File)
      write(ln,'(i5)') iu
      close(ln)
      if(iu.eq.ip) then
        go to 9000
      else
        go to 9999
      endif
2000  call FeSaveBMPFile(File,ix1,ix2,iy1,iy2,ich)
      if(EqIgCase(File,HCFileName)) go to 9999
9000  call FeTmpFilesAdd(File)
9999  return
      end
      subroutine FeLoadImage(xlow,xhigh,ylow,yhigh,File,klic)
      include 'fepc.cmn'
      character*(*) File
      character*256  Veta
      logical EqIgCase,Load
      Load=.true.
      go to 1100
      entry FeCleanImage(xlow,xhigh,ylow,yhigh,File,klic)
      Load=.false.
1100  iu=0
      if(klic.eq.1) then
        Veta=HomeDir(:idel(HomeDir))//'bmp\'//File
      else
        Veta=File
      endif
      call JanaToClient(xlow,ylow,ix1,iy1)
      call JanaToClient(xhigh,yhigh,ix2,iy2)
      if(klic.ne.1) then
        do i=1,MxBmp
          if(EqIgCase(Veta,BmpName(i))) then
            if(Load) call FeBitmapPut(IBmp(i),ix1,iy1,ix2,iy2)
            iu=i
            go to 9000
          endif
        enddo
      endif
      if(Load) call FeLoadBMPFile(Veta,ix1,ix2,iy1,iy2,ich)
9000  if(klic.eq.0) then
        call DeleteFile(Veta)
        call FeTmpFilesClear(Veta)
        if(iu.ne.0) then
          BmpName(iu)=' '
          call FeBitmapDestroy(IBmp(iu))
          IBmp(iu)=0
        endif
      endif
      if(Load) call FeUpdateDisplay(ix1,ix2,iy1,iy2)
      return
      end
      integer function RGB(Red,Green,Blue)
      integer Red,Green,Blue
      RGB=(Blue*256+Green)*256+Red
      return
      end
      integer function LoWord(n)
      include 'fepc.cmn'
      parameter (i2na16=65536)
      LoWord=mod(n,i2na16)
      return
      end
      integer function HiWord(n)
      include 'fepc.cmn'
      parameter (i2na16=65536)
      HiWord=n/i2na16
      return
      end
      subroutine FeBitmapGet(hBitmap,ix1,iy1,ix2,iy2)
      use Jana_windows
      include 'fepc.cmn'
      integer hBitmap,Width,Height,hDCPom
      if(ix1.ne.0.or.ix2.ne.0.or.iy1.ne.0.or.iy2.ne.0) then
        Width=iabs(ix2-ix1)+1
        Height=iabs(iy2-iy1)+1
        ixp=min(ix1,ix2)
        iyp=min(iy1,iy2)
      else
        Width=PixelClientWidth
        Height=PixelClientHeight
        ixp=0
        iyp=0
      endif
      hDCPom=CreateCompatibleDC(carg(hdc))
      hBitmap=CreateCompatibleBitmap(carg(hDC),carg(Width),
     1                               carg(Height))
      call SelectObject(carg(hDCPom),carg(hBitmap))
      call SelectObject(carg(hDCComp),carg(hDCBitMap))
      call BitBlt(carg(hDCPom),carg(0),carg(0),
     1            carg(Width),carg(Height),carg(hDCComp),
     2            carg(ixp),carg(iyp),carg(SRCCOPY))
      call DeleteDC(carg(hDCPom))
      return
      end
      subroutine FeBitmapPut(hBitmap,ix1,iy1,ix2,iy2)
      use Jana_windows
      include 'fepc.cmn'
      integer hBitmap,Width,Height,hDCPom
      if(ix1.ne.0.or.ix2.ne.0.or.iy1.ne.0.or.iy2.ne.0) then
        Width=iabs(ix2-ix1)+1
        Height=iabs(iy2-iy1)+1
        ixp=min(ix1,ix2)
        iyp=min(iy1,iy2)
      else
        Width=PixelClientWidth
        Height=PixelClientHeight
        ixp=0
        iyp=0
      endif
      hDCPom=CreateCompatibleDC(carg(hdc))
      call SelectObject(carg(hDCPom),carg(hBitmap))
      call SelectObject(carg(hDCComp),carg(hDCBitMap))
      call BitBlt(carg(hDCComp),carg(ixp),carg(iyp),
     1            carg(Width),carg(Height),carg(hDCPom),
     2            carg(0),carg(0),carg(SRCCOPY))
      call DeleteDC(carg(hDCPom))
      return
      end
      subroutine FeBitmapDestroy(hBitmap)
      use Jana_windows
      integer hBitmap
      call DeleteObject(carg(hBitmap))
      return
      end
      subroutine FeReleaseOutput
      include 'fepc.cmn'
      if(DeferredOutput) then
        DeferredOutput=.false.
        call FeUpdateDisplay(-1,-1,-1,-1)
      endif
      return
      end
      subroutine FeDeferOutput
      include 'fepc.cmn'
      if(.not.DeferredOutput) DeferredOutput=.true.
      return
      end
      subroutine FeFlush
      use Jana_windows
      include 'fepc.cmn'
      call FeClientToScreen(hDC,0,0,ii,jj)
      call BitBlt(carg(hDC),carg(ii),carg(jj),
     1            carg(PixelClientWidth),carg(PixelClientHeight),
     2            carg(hDCComp),carg(0),carg(0),carg(SRCCOPY))
      return
      end
      subroutine FeUpdateDisplay(ix1,ix2,iy1,iy2)
      use Jana_windows
      include 'fepc.cmn'
      integer ixp,iyp,Width,Height
      save ixp,iyp,ixm,iym,Width,Height
      if(ix1.eq.0.and.ix2.eq.0.and.iy1.eq.0.and.iy2.eq.0) then
        Width=PixelClientWidth
        Height=PixelClientHeight
        ixp=0
        iyp=0
      else if(ix1.gt.0.or.ix2.gt.0.or.iy1.gt.0.or.iy2.gt.0) then
        Width=max(iabs(ix2-ix1)+1,Width)
        Height=max(iabs(iy2-iy1)+1,Height)
        ixp=min(ix1,ix2,ixp)
        iyp=min(iy1,iy2,iyp)
        ixm=max(ix1,ix2,ixm)
        iym=max(iy1,iy2,iym)
        Width=ixm-ixp+1
        Height=iym-iyp+1
      endif
      if(.not.DeferredOutput) then
        call SelectObject(carg(hDCComp),carg(hDCBitMap))
        call BitBlt(carg(hDC),carg(ixp),carg(iyp),
     1              carg(Width),carg(Height),
     2              carg(hDCComp),carg(ixp),carg(iyp),carg(SRCCOPY))
        ixp= 99999
        iyp= 99999
        ixm=-99999
        iym=-99999
        Width=0
        Height=0
      endif
      return
      end
      subroutine FeGraphicXYFromMessage(lParam,X,Y)
      include 'fepc.cmn'
      integer lParam,HiWord
      ix=LoWord(lParam)
      iy=HiWord(lParam)
      call ClientToJana(ix,iy,X,Y)
      return
      end
c      subroutine FeSetForegroundWindow
c      use Jana_windows
c      call ShowWindow(carg(hwnd),carg(SW_RESTORE))
c      call SetWindowPos(carg(hwnd),carg(HWND_TOPMOST),
c     1                  carg(0),carg(0),carg(0),carg(0),
c     2                  carg(ior(SWP_NOMOVE,SWP_NOSIZE)))
c      call SetWindowPos(carg(hwnd),carg(HWND_NOTOPMOST),
c     1                  carg(0),carg(0),carg(0),carg(0),
c     2                  carg(ior(SWP_NOMOVE,SWP_NOSIZE)))
c      call FeFlush
c      return
c      end
      subroutine FeGetClipboardText(Veta,n)
      use Jana_windows
      character*1024 String
      character*256 t256
      character(*) Veta
      character*1  t1
      integer :: CR=13,LF=10,hMem
      Klic=0
      go to 1000
      entry FeGetClipboardLongText(Veta,n)
      Klic=1
1000  Veta=' '
      call OpenClipboard(carg(hWnd))
      hMem=GetClipboardData(carg(CF_TEXT))
      lp=GlobalLock(carg(hMem))
      n=lstrlen(carg(lp))
      String=' '
      call lstrcpy(carg(offset(String)),carg(lp))
      call GlobalUnlock(hMem)
      do i=1,min(n,len(Veta))
        j=ichar(String(i:i))
        if(j.eq.CR.or.j.eq.LF.or.j.eq.0) then
          if(Klic.eq.0) then
            go to 1100
          else
            cycle
          endif
        else
          Veta(i:i)=String(i:i)
        endif
      enddo
      i=min(n,len(Veta))
1100  n=i
      call CloseClipboard()
      return
      end
      subroutine FeSetClipboardText(Veta)
      use Jana_windows
      character(*) Veta
      character*256 VetaP
      integer hMem
      call OpenClipboard(carg(hWnd))
      call EmptyClipboard()
      VetaP=Veta(:idel(Veta))//char(0)
      idl=idel(VetaP)+1
      hMem=GlobalAlloc(carg(GHND),carg(idl))
      lp=GlobalLock(carg(hMem))
      call lstrcpy(carg(lp),carg(offset(VetaP)))
      call SetClipboardData(carg(CF_TEXT),carg(hMem))
      call GlobalUnlock(hMem)
      call CloseClipboard()
      return
      end
      subroutine FeMakeLowerPriority
      use Jana_windows
      integer OldPriority
      data OldPriority,NewPriority/32,16384/
      i=GetCurrentProcess()
      OldPriority=GetPriorityClass(carg(i))
      call SetPriorityClass(carg(i),carg(NewPriority))
      go to 9999
      entry FeReturnOriginalPriority
      i=GetCurrentProcess()
      call SetPriorityClass(carg(i),carg(OldPriority))
9999  return
      end
      subroutine FeGetFileTime(FileName,FileDate,FileTimeP)
      use Jana_windows
      integer Date,Time,hFile
      character*(*) FileName,FileDate,FileTimeP
      character*256 Veta
      type(FILETIME) fTime
      Date=0
      Time=0
      FileDate=' '
      FileTimeP=' '
      Veta=FileName(:idel(FileName))//char(0)
      hFile=CreateFileA(carg(offset(Veta)),carg(GENERIC_READ),
     1                 carg(NULL),carg(NULL),carg(OPEN_ALWAYS),
     2                 carg(FILE_ATTRIBUTE_NORMAL),carg(NULL))
      if(hFile.le.0) go to 9999
      call GetFileTime(carg(hFile),carg(NULL),carg(NULL),
     1                 carg(offset(fTime)))
      call FileTimeToDosDateTime(carg(offset(fTime)),
     1                           carg(offset(Date)),
     2                           carg(offset(Time)))
      call CloseHandle(carg(hFile))
      write(FileDate,'(i2,''/'',i2,''/'',i4)')
     1  mod(Date,32),mod(Date/32,16),Date/512+1980
      do i=1,idel(FileDate)
        if(FileDate(i:i).eq.' ') FileDate(i:i)='0'
      enddo
      write(FileTimeP,'(i2,'':'',i2,'':'',i2)')
     1  Time/2048,mod(Time/32,64),2*mod(Time,32)
      do i=1,idel(FileTimeP)
        if(FileTimeP(i:i).eq.' ') FileTimeP(i:i)='0'
      enddo
9999  return
      end
      subroutine FeGetPSPrinters(PSPrinters,n)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) PSPrinters(20)
      character*256 Printer,Veta
      character*10 ::  CPom
      integer :: Reserved=1600,Flags,hDCP,Returned=0
      logical EqIgCase
      type(PRINTER_INFO_1) PrInfo(:)
      allocatable PrInfo
      n=0
      do i=1,20
        PSPrinters(i)=' '
      enddo
1100  allocate(PrInfo(Reserved/16))
      Flags=ior(PRINTER_ENUM_LOCAL,PRINTER_ENUM_CONNECTIONS)
      Printer=char(0)
      i=EnumPrintersA(carg(Flags),carg(offset(Printer)),carg(1),
     1                carg(offset(PrInfo)),
     2                carg(Reserved),carg(offset(Needed)),
     3                carg(offset(Returned)))
      if(i.eq.0) then
        deallocate(PrInfo)
        Reserved=Needed+16
        go to 1100
      else
        do i=1,Returned
          Printer=' '
          call lstrcpy(carg(offset(Printer)),carg(PrInfo(i).pName))
          CPom='WINSPOOL'//char(0)
          hDCP=CreateDCA(carg(offset(CPom)),carg(offset(Printer)),
     1                   carg(NULL),carg(NULL))
          Veta=' '
          j=ExtEscape(carg(hDCP),carg(QUERYESCSUPPORT),carg(n),
     1                carg(offset(Veta)),carg(NULL),carg(NULL))
          if(j.eq.0) then
            Veta=' '
            j=ExtEscape(carg(hDCP),carg(GETTECHNOLOGY),
     1                  carg(NULL),carg(NULL),carg(256),
     2                  carg(offset(Veta)))
            k=0
1500        call DeleteString(Veta,char(0),k)
            if(k.gt.0) go to 1500
            if(EqIgCase(Veta,'Postscript')) then
              n=n+1
              PSPrinters(n)=Printer
            endif
          endif
          call DeleteDC(carg(hDCP))
        enddo
      endif
      j=0
      do i=1,n
        k=0
1600    call DeleteString(PSPrinters(i),char(0),k)
        if(k.gt.0) go to 1600
        if(PSPrinters(i).eq.PSPrinter.and.i.ne.1) j=i
      enddo
      if(j.gt.0) then
        PSPrinters(j)=PSPrinters(1)
        PSPrinters(1)=PSPrinter
      endif
9999  if(allocated(PrInfo)) deallocate(PrInfo)
      return
      end
      subroutine FePrintFile(PrinterName,FileName,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) FileName,PrinterName
      character*1 Bytes(:)
      character*80 Veta1,Veta2
      integer FeFileSize,FeReadBinaryFile,FeOpenBinaryFile,
     1        FeCloseBinaryFile,hPrinter,dwBytesWritten,BytesWritten
      allocatable Bytes
      type(DOC_INFO_1) DocInfo
      nBytes=FeFileSize(FileName)
      if(allocated(Bytes)) deallocate(Bytes)
      allocate(Bytes(nBytes))
      ln=FeOpenBinaryFile(FileName)
      i=FeReadBinaryFile(ln,Bytes,nBytes)
      i=FeCloseBinaryFile(ln)
      Veta1=PrinterName(:idel(PrinterName))//char(0)
      i=OpenPrinterA(carg(offset(Veta1)),carg(offset(hPrinter)),
     1               carg(NULL))
      ich=0
      if(i.eq.0) then
        ich=11
        go to 9999
      endif
      Veta1='My Document'//char(0)
      Veta2='RAW'//char(0)
      DocInfo.pDocName=offset(Veta1)
      DocInfo.pOutputFile=NULL
      DocInfo.pDatatype=offset(Veta2)
      if(StartDocPrinterA(carg(hPrinter),carg(1),carg(offset(DocInfo)))
     1   .eq.0) then
        call ClosePrinter(carg(hPrinter))
        ich=2
        go to 9999
      endif
      if(StartPagePrinter(carg(hPrinter)).eq.0) then
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=3
        go to 9999
      endif
      if(WritePrinter(carg(hPrinter),carg(offset(Bytes)),carg(nBytes),
     1                carg(offset(BytesWritten))).eq.0) then
        i=EndPagePrinter(carg(hPrinter))
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=4
        go to 9999
      endif
      if(EndPagePrinter(carg(hPrinter)).eq.0) then
        i=EndDocPrinter(carg(hPrinter))
        call ClosePrinter(carg(hPrinter))
        ich=5
        go to 9999
      endif
      if(EndDocPrinter(carg(hPrinter)).eq.0) then
        call ClosePrinter(carg(hPrinter))
        ich=6
        go to 9999
      endif
      call ClosePrinter(carg(hPrinter))
      if(ich.eq.0.and.PrinterName.ne.PSPrinter) then
        call FeRewriteIni('PSPrinter',PrinterName)
        PSPrinter=PrinterName
      endif
9999  if(allocated(Bytes)) deallocate(Bytes)
      return
      end
      subroutine FeHardCopy(Type,action)
      include 'fepc.cmn'
      character*256 Veta,t256
      character*240 FePrikaz
      character*8  SvFile
      character*(*) Action
      integer Type,WhiteOld,BlackOld
      save WhiteOld,BlackOld
      if(Type.eq.0) return
      if(Action.eq.'open') then
        if(Type.lt.HardCopyNum) then
          call FeOpenHC(Type,HCFileName)
          if(InvertWhiteBlack) then
            WhiteOld=White
            BlackOld=Black
            White=BlackOld
            Black=WhiteOld
            ColorNumbers(1)=Black
            ColorNumbers(2)=White
            SvFile='pcxp.bmp'
            call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       SvFile)
          endif
        else
          call OpenFile(85,HCFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
        endif
      else if(Action.eq.'close') then
        if(Type.lt.HardCopyNum) then
          call FeCloseHC(Type,HCFileName)
          if(InvertWhiteBlack) then
            White=WhiteOld
            Black=BlackOld
            ColorNumbers(1)=Black
            ColorNumbers(2)=White
            call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                       SvFile,0)
          endif
          if(Tiskne) then
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            Tiskne=.false.
          endif
        else
          close(85)
        endif
      endif
9999  ErrFlag=0
      return
      end
      subroutine FeSavePicture(WhatToSave,NFormat,Key)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) WhatToSave
      character*256 EdwStringQuest
      character*128 FileName(:)
      character*80  Veta
      character*1   ch1,ch2
      integer HardCopyLast,CrwStateQuest
      logical :: ExistFile,ThisSection=.true.,CrwLogicQuest,EqIgCase
      data HardCopyLast/2/
      allocatable FileName
      allocate(FileName(12))
      if(NFormat.le.0) then
        nCrwMax=12
      else
        nCrwMax=NFormat
      endif
      if(HardCopy.eq.0.or.mod(HardCopy,100).gt.nCrwMax)
     1  HardCopy=HardCopyLast
      ThisSection=HardCopy.lt.100
      id=NextQuestId()
      NameStandard=1
      do j=1,nCrwMax
        k=0
1000    write(Cislo,'(''_'',i3)') k
        call Zhusti(Cislo)
        FileName(j)=fln(:ifln)//Cislo(:idel(Cislo))//HCExtension(j)
        if(ExistFile(FileName(j))) then
          k=k+1
          go to 1000
        endif
      enddo
      xqd=350.
      call FeQuestCreate(id,-1.,-1.,xqd,nCrwMax+2,' ',0,LightGray,0,0)
      il=1
      tpom=5.
      Veta='%File name'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+20.
      Veta='Br%owse'
      dpom=xqd-xpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      pom=0.
      do i=1,12
        pom=max(pom,FeTxLength(HCMenu(i)))
      enddo
      xpom=xqd-10.-CrwgYd
      tpom=xqd-CrwgYd-20.-pom
      j=0
      do i=1,12
        j=j+1
        if(i.gt.nCrwMax) exit
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,HCMenu(j),'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) nCrwTypeFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,j.eq.mod(HardCopy,100))
      enddo
      nCrwTypeLast=CrwLastMade
      idl=idel(WhatToSave)
      if(Key.eq.0) then
        il=2
        tpom=5.
        Veta='Save t%his '//WhatToSave(:idl)
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,2)
          if(i.eq.1) then
            Veta='Save %all '//WhatToSave(:idl)//'s'
            il=il+6
            nCrwThis=CrwLastMade
          else
            nCrwAll=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.ThisSection)
        enddo
      else
        nCrwThis=0
        nCrwAll=0
        call FeQuestLblMake(id,5.,2,'Save this '//WhatToSave(:idl),'L',
     1                      'N')
      endif
      HCFileName=FileName(mod(HardCopy,100))
      il=nCrwMax+2
      xpom=5.
      tpom=xpom+CrwXd+5.
      Veta='I%nvert White for Black and vice versa'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,InvertWhiteBlack)
      nCrwInvert=CrwLastMade
1300  call FeQuestStringEdwOpen(nEdwFileName,HCFileName)
      nCrw=nCrwTypeFirst
      if(ThisSection) then
        do i=1,nCrwMax
          call FeQuestCrwOpen(nCrw,i.eq.mod(HardCopy,100))
          nCrw=nCrw+1
        enddo
      else
        do i=1,HardCopyNum-1
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
      endif
      if(mod(HardCopy,100).ge.HardCopyNum) then
        if(CrwStateQuest(nCrwInvert).eq.CrwOn.or.
     1     CrwStateQuest(nCrwInvert).eq.CrwOff) then
          InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
          call FeQuestCrwDisable(nCrwInvert)
        endif
      else
        call FeQuestCrwOpen(nCrwInvert,InvertWhiteBlack)
      endif
1500  call FeQuestEvent(id,ich)
      HCFileName=EdwStringQuest(nEdwFileName)
      id1=idel(HCFileName)
      id2=idel(HCExtension(mod(HardCopy,100)))
      j=id1-id2
      do i=1,id2
        j=j+1
        ch1=HCExtension(mod(HardCopy,100))(i:i)
        ch2=HCFileName(j:j)
        if(OpSystem.le.0) then
          call mala(ch1)
          call mala(ch2)
        endif
        if(ch1.ne.ch2) then
          NameStandard=-1
          go to 1610
        endif
      enddo
      if(.not.EqIgCase(HCFileName,FileName(mod(HardCopy,100))))
     1  go to 1607
      NameStandard=1
      FileName(mod(HardCopy,100))=HCFileName
      go to 1610
1607  NameStandard=0
1610  if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        call FeFileManager('Select output file',HCFileName,
     1                     '*'//HCExtension(mod(HardCopy,100)),0,.true.,
     2                     ich)
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwTypeFirst.and.
     2        CheckNumber.le.nCrwTypeLast) then
        HardCopy=CheckNumber
        if(NameStandard.eq.1) then
          HCFileName=FileName(HardCopy)
        else if(NameStandard.eq.0) then
          i=LocateSubstring(HCFileName,'.',.false.,.false.)
          if(i.gt.0) HCFileName=HCFileName(:i-1)//
     1     HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
        endif
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwThis.or.CheckNumber.eq.nCrwAll)) then
        ThisSection=CrwLogicQuest(nCrwThis)
        if(CheckNumber.eq.nCrwAll.and.
     1     mod(HardCopy,100).le.HardCopyNum-1) then
          HardCopy=HardCopyNum
          HCFileName=FileName(HardCopy)
          nCrw=nCrwTypeFirst-1+HardCopyNum
          call FeQuestCrwOn(nCrw)
        endif
        go to 1300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
        if(nCrwAll.gt.0) then
          if(CrwLogicQuest(nCrwAll)) then
            HardCopy=HardCopy+100
          else
            HardCopy=mod(HardCopy,100)
          endif
        endif
        HardCopyLast=HardCopy
      else
        HardCopy=HardCopyLast
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) HardCopy=0
9999  deallocate(FileName)
      return
      end
      subroutine FeOpenHC(Type,FileName)
      use Jana_windows
      include 'fepc.cmn'
      integer Type
      character*(*) FileName
      character*256 Veta
      character*3   Ext
      save Ext
      Klic=0
      LnPS=0
      go to 1100
      entry FeCloseHC(Type,FileName)
      Klic=1
1100  ErrFlag=0
      if(Type.eq.HardCopyHPGL) then
        NInfo=1
        InvertWhiteBlack=.false.
        TextInfo='Sorry, the option is not yet implemented.'
        if(Klic.eq.1) call FeInfoOut(-1.,-1.,'INFORMATION','L')
      else if(Type.eq.HardCopyPS.or.Type.eq.HardCopyEPS) then
        if(Type.eq.HardCopyPS) then
          Ext='ps'
        else
          Ext='eps'
        endif
        if(Klic.eq.0) then
          ln=NextLogicNumber()
          call OpenFile(ln,HomeDir(:idel(HomeDir))//'a2ps'//
     1                  ObrLom//'GraphStart.'//Ext,'formatted','old')
          read(ln,FormA) Veta
          if(Veta(1:1).ne.'#') rewind ln
          LnPS=NextLogicNumber()
          call OpenFile(LnPS,FileName,'formatted','unknown')
1200      read(ln,FormA,end=1220) Veta
          write(LnPS,FormA) Veta(:idel(Veta))
          go to 1200
1220      call CloseIfOpened(ln)
          call JanaToClientReverse(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClientReverse(XMaxGrWin,YMaxGrWin,ix2,iy2)
          WidthPS=iabs(ix2-ix1)+1
          HeightPS=iabs(iy2-iy1)+1
          XPocPS=min(ix1,ix2)
          YPocPS=min(iy1,iy2)
          ScalePS=min(756./float(WidthPS),540./float(HeightPS))
          LastXPS=-9999.
          LastYPS=-9999.
          LastColorPS=-1
        else
          ln=NextLogicNumber()
          call OpenFile(ln,HomeDir(:idel(HomeDir))//'a2ps'//
     1                  ObrLom//'GraphEnd.'//Ext,'formatted','old')
          read(ln,FormA) Veta
          if(Veta(1:1).ne.'#') rewind ln
1240      read(ln,FormA,end=1260) Veta
          write(LnPS,FormA) Veta(:idel(Veta))
          go to 1240
1260      call CloseIfOpened(ln)
          call CloseIfOpened(LnPS)
          LnPS=0
        endif
      else if(Type.eq.HardCopyPCX) then
        if(Klic.eq.1.eqv.InvertWhiteBlack) then
          call JanaToClient(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClient(XMaxGrWin,YMaxGrWin,ix2,iy2)
          call FeSavePCXFile(FileName,ix1,ix2,iy1,iy2,ich)
        endif
      else if(Type.eq.HardCopyBMP) then
        if(Klic.eq.1.eqv.InvertWhiteBlack) then
          call JanaToClient(XMinGrWin,YMinGrWin,ix1,iy1)
          call JanaToClient(XMaxGrWin,YMaxGrWin,ix2,iy2)
          call FeSaveBMPFile(FileName,ix1,ix2,iy1,iy2,ich)
        endif
      else if(Type.eq.HardCopyWMF) then
        Veta=FileName(:idel(FileName))//char(0)
        if(Klic.eq.0) then
          hDCMeta=CreateEnhMetaFileA(carg(NULL),carg(offset(Veta)),
     1                               carg(NULL),carg(NULL))
        else
          call CloseEnhMetaFile(carg(hDCMeta))
          hCDMeta=0
        endif
      else if(Type.eq.10) then
        call FeWinMessage('Sorry, not yet implemented',' ')
      else
        go to 9999
      endif
9999  return
      end
      subroutine FePrintPicture(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer EdwStateQuest,RolMenuSelectedQuest
      logical CrwLogicQuest
      character*256 EdwStringQuest
      character*80  Veta,PSPrinters(100)
      call FeGetPSPrinters(PSPrinters,nPSPrinters)
      ich=0
      xqd=300.
      if(nPSPrinters.gt.0) then
        tpom=5.
        Veta='Printers with PostScript driver:'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=20.
        do i=1,nPSPrinters
          dpom=max(dpom,FeTxLengthUnder(PSPrinters(i))+2.*EdwMarginSize)
        enddo
        dpom=dpom+EdwYd+20.
        xpom1=xpom+dpom+5.
        xqd=max(xpom1+EdwYd+5.,xqd)
      else
        call FeChybne(-1.,-1.,'no postscipt printer found.',' ',
     1                SeriousError)
        ich=1
        go to 9999
      endif
      il=2
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Define printer',0,LightGray,
     1                   0,0)
      il=1
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nRolMenuPrinters=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,PSPrinters,NPSPrinters,1)
      Veta='I%nvert White for Black and vice versa'
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,InvertWhiteBlack)
      nCrwInvert=CrwLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        InvertWhiteBlack=CrwLogicQuest(nCrwInvert)
        PSPrinters(1)=PSPrinters(RolMenuSelectedQuest(nRolMenuPrinters))
        PSPrinter=PSPrinters(1)
        HCFileName='jtps'
        call CreateTmpFile(HCFileName,i,2)
        call FeTmpFilesAdd(HCFileName)
        HardCopy=HardCopyPS
        Tiskne=.true.
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
9999  return
      end

