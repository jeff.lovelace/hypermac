      module transport
      implicit none
      real omeABS(:),theABS(:),kapABS(:),phiABS(:),ubp(3,3),InvUBp(3,3),
     1     RrozX,BrozX,GrozX,RrozY,BrozY,GrozY,TransMat(3,3),
     2     TMatFin(3,3)

      integer iBitmapABS(:,:), BlueABS(:,:,:), GreenABS(:,:,:),
     1        RedABS(:,:,:),iXmax,iYmax,tag(:,:)
      character*256 BmpNameABS(:)
      character*1 T(:,:)
      allocatable iBitmapABS, omeABS, theABS, kapABS, phiABS,
     1        BlueABS, GreenABS, RedABS, BmpNameABS, tag, T
      end module

      module vypocetni
      implicit none
      integer, parameter :: n=10000, ns=1000
      integer X,Y,Ip,Is,Q,Lp(ns),Kp(ns),Jp(ns),Mp(ns),kXminX,kXminY,
     1        kXmaxX,kXmaxY,kYminX,kYminY,kYmaxX,kYmaxY,iMiller
      REAL    sol(ns,2),Rp(ns),vek(3,ns),nvek(3,ns),Cnvek(ns),D(ns),
     1        xpr,ypr,dist,Miller(3,ns),Xpi(n),Yp(n),Tp(n),
     2        X1p(n),Y1p(n)
      CHARACTER*2, DIMENSION(ns) :: osa
      LOGICAL :: change, poca
      end module


      subroutine PrvniAbsCorr
      use transport
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,EdwStringQuest,FileNameUB,FileNameJPR,backup
      character*2 nty
      integer nilFirstMat,nilFirstJpr,nFrames
      logical CrwLogicQuest,FeYesNoHeader,FeYesNo,JPGfine,AngleFine
      real xp(3)
      data FileNameUB/'B.cif_od'/,
     1     FileNameJPR/'c:\Jana2006\testslouc\movie\B.jpr'/
!      ubp(1:3,1:3)=ub(1:3,1:3,MxRefBlock)
      ubp(1,1)=0.12
      ubp(1,2)=0.13
      ubp(1,3)=0.15
      ubp(2,1)=0.08
      ubp(2,2)=0.05
      ubp(2,3)=0.07
      ubp(3,1)=0.28
      ubp(3,2)=0.25
      ubp(3,3)=0.27
      ich=0
      nFrames=0
      nEwdClr=0

      id=NextQuestId()
      xqd=600.
      il=10
      Veta='Zkusebni nacteni matice'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      tpom=xqd*.5
      Veta='Orientation matrix:'
      call FeQuestLblMake(id,tpom,il,Veta,'C','N')
      tpom=50.
      nEdwOrMatFirst=0
      do i=1,3
        tpom=50.
        il=il+1
        write(Cislo,'(i1,a2,'' row'')') i,nty(i)
        if(i.eq.1) then
          xpom=tpom+FeTxLengthUnder(Cislo)+10.
          dpom=xqd-xpom-50.
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Cislo,'L',dpom,EdwYd,0)
        xp(1:3)=ubp(i,1:3)
        write(Veta,'(i1,a2,'' row   '',3f10.6)') i,nty(i), xp(1:3)
        tpom=170.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        if(i.eq.1) nLblMatFirst=LblLastMade
        if(i.eq.1) nEdwOrMatFirst=EdwLastMade
      enddo
      il=il+1
      Veta='%Fix matrix'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwFix=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,.true.)
      il=il+1
      Veta='From keyboard'
      xpom=5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
      nCrwKey=CrwLastMade
      il=il+1
      Veta='From file'
      xpom=5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
      nCrwFile=CrwLastMade
      tpom=50.+FeTxLengthUnder(Veta)
      Veta='F%ile:'
      xpom=tpom+FeTxLengthUnder(Veta)+11.
      dpom=200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileUB=EdwLastMade
      tpom=xpom+dpom+15.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtBrowseUB=ButtonLastMade
      tpom=tpom+dpom+15.
      Veta='%Load UB'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtLoadUB=ButtonLastMade
      il=il+1
      tpom=14.+FeTxLengthUnder(Veta)
      Veta='Load .jpr file:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileJPR=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNameJPR)
      tpom=xpom+dpom+15.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtBrowseJPR=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+15.
      Veta='%Load .jpr'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtLoadJPR=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowseUB)
     1  then
        backup=FileNameUB
        FileNameUB=EdwStringQuest(nEdwFileUB)
        call FeFileManager('Select file with orientation matrix',
     1                     FileNameUB,'*.cif_od',0,.true.,ichp)
        if(ichp.eq.0) then
          i=len(trim(FileNameUB))
          if((FileNameUB(i-6:i).eq.'.cif_od').or.(FileNameUB(i-3:i)
     1      .eq.'.cif').or.(FileNameUB(i-3:i).eq.'.sum')) then
            call FeQuestStringEdwOpen(nEdwFileUB,FileNameUB)
          else
            Veta='Unknown file type'
            call FeInfoOut(-1.,-1.,Veta,'L')
            FileNameUB=backup
          end if
        end if
        go to 1500

      else if(CheckType.eq.EventButton.and.CheckNumber.eq.
     1  nButtBrowseJPR) then
        backup=FileNameJPR
        FileNameJPR=EdwStringQuest(nEdwFileJPR)
        call FeFileManager('Select file with movie information',
     1                     FileNameJPR,'*.jpr',0,.true.,ichp)
        if(ichp.eq.0) then
          i=len(trim(FileNameJPR))
          if(FileNameJPR(i-3:i).eq.'.jpr') then
            call FeQuestStringEdwOpen(nEdwFileJPR,FileNameJPR)
          else
            Veta='This is not .jpr file'
            call FeInfoOut(-1.,-1.,Veta,'L')
            FileNameJPR=backup
          end if
        end if
        go to 1500

      else if(CheckType.eq.EventButton.and.CheckNumber.eq.
     1  nButtLoadUB) then
        FileNameUB=EdwStringQuest(nEdwFileUB)
        call VeLoadUB(FileNameUB,ubp)
          nEdw=nEdwOrMatFirst
          do i=1,3
            xp(1:3)=ubp(i,1:3)
            call FeQuestRealAEdwOpen(nEdw,xp,3,.false.,.false.)
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo

          nLbl=nLblMatFirst
          do i=1,3
            write(Veta,'(i1,a2,'' row   '',3f10.6)') i,nty(i),ubp(i,1:3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          end do
        go to 1500

      else if(CheckType.eq.EventButton.and.CheckNumber.eq.
     1  nButtLoadJPR) then
        backup=FileNameJPR
        FileNameJPR=EdwStringQuest(nEdwFileJPR)
        i=len(trim(FileNameJPR))
        if(FileNameJPR(i-3:i).ne.'.jpr') then
          Veta='This is not .jpr file'
          call FeInfoOut(-1.,-1.,Veta,'L')
          FileNameJPR=backup
          call FeQuestStringEdwOpen(nEdwFileJPR,FileNameJPR)
          go to 1500
        end if
        call VeLoadJPR(FileNameJPR,nFrames,InvFile)
        if (InvFile.ne.0) then
          FileNameJPR=backup
          call FeQuestStringEdwOpen(nEdwFileJPR,FileNameJPR)
          go to 1500
        end if
        if (nFrames.ne.0) then
          tpom=20.
          il=9
          if (nilFirstJPR.eq.0) then
            nilFirstJPR=il
            write(Veta,'(''Loaded .jpr file : '',a200)') FileNameJPR
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
            nLblJPRFirst=LblLastMade
            il=il+1
            write(Veta,'(''includes '',i5,'' frames'')') nFrames
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')

          else
            write(Veta,'(''Loaded .jpr file : '',a200)') FileNameJPR
            call FeQuestLblChange(nLblJPRFirst,Veta)
            write(Veta,'(''includes '',i5,'' frames'')') nFrames
            call FeQuestLblChange(nLblJPRFirst+1,Veta)
          endif
        end if
        go to 1500

      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFix) then
        if(CrwLogicQuest(nCrwFix)) then
          call FeQuestCrwClose(nCrwKey)
          call FeQuestCrwClose(nCrwFile)
          call FeQuestEdwClose(nEdwFileUB)
          call FeQuestButtonClose(nButtBrowseUB)
          call FeQuestButtonClose(nButtLoadUB)
          nEdw=nEdwOrMatFirst
          do i=1,3
            call FeQuestRealAFromEdw(nEdw,xp)
            ubp(i,1:3)=xp(1:3)
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          end do
          nLbl=nLblMatFirst
          do i=1,3
            write(Veta,'(i1,a2,'' row   '',3f10.6)') i,nty(i),ubp(i,1:3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          end do
        else
          call FeQuestCrwOpen(nCrwKey,.true.)
          if(CrwLogicQuest(nCrwKey)) then
            nEdw=nEdwOrMatFirst
            do i=1,3
              xp(1:3)=ubp(i,1:3)
              call FeQuestRealAEdwOpen(nEdw,xp,3,.false.,.false.)
              nEdw=nEdw+1
            enddo
            call FeQuestCrwOpen(nCrwFile,.false.)
          end if
        end if
        go to 1500

      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwKey) then
        if(CrwLogicQuest(nCrwKey)) then
          nEdw=nEdwOrMatFirst
          do i=1,3
            xp(1:3)=ubp(i,1:3)
            call FeQuestRealAEdwOpen(nEdw,xp,3,.false.,.false.)
            nEdw=nEdw+1
          enddo
          call FeQuestEdwClose(nEdwFileUB)
          call FeQuestButtonClose(nButtBrowseUB)
          call FeQuestButtonClose(nButtLoadUB)
        end if
        go to 1500

      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwFile) then
        if(CrwLogicQuest(nCrwFile)) then
          call FeQuestStringEdwOpen(nEdwFileUB,FileNameUB)
          call FeQuestButtonOpen(nButtBrowseUB,ButtonOff)
          call FeQuestButtonOpen(nButtLoadUB,ButtonOff)
          nEdw=nEdwOrMatFirst
          do i=1,3
            call FeQuestRealAFromEdw(nEdw,xp)
            ubp(i,1:3)=xp(1:3)
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          end do
          nLbl=nLblMatFirst
          do i=1,3
            write(Veta,'(i1,a2,'' row   '',3f10.6)') i,nty(i),ubp(i,1:3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          end do
        end if
        go to 1500

      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif

      if(ich.eq.0) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        if(nFrames.eq.0) then
          Veta='.jpr file was not loaded'
          call FeInfoOut(-1.,-1.,Veta,'L')
          go to 1500
        end if
        if(CrwLogicQuest(nCrwkey)) then
          nEdw=nEdwOrMatFirst
          do i=1,3
            call FeQuestRealAFromEdw(nEdw,xp)
            ubp(i,1:3)=xp(1:3)
            nEdw=nEdw+1
          enddo
        else
        end if
        TextInfo(1)='Loaded UB matrix:'
        write(Veta,'(i1,a2,'' row   '',3f8.5)') 1,nty(1),ubp(1,1:3)
        TextInfo(2)=Veta
        write(Veta,'(i1,a2,'' row   '',3f8.5)') 2,nty(2),ubp(2,1:3)
        TextInfo(3)=Veta
        write(Veta,'(i1,a2,'' row   '',3f8.5)') 3,nty(3),ubp(3,1:3)
        TextInfo(4)=Veta
        TextInfo(5)='Loaded .jpr file'
        TextInfo(6)=FileNameJPR
        write(Veta,'(''includes '',i5,'' frames'')') nFrames
        TextInfo(7)=Veta
        NInfo=7
        if(.not.FeYesNoHeader(-1.,-1.,
     1     'Do you want to continue with these information?',0)) then
          go to 1500
        else
          call LoadAngles(FileNameJPR,nFrames,AngleFine)
          if (AngleFine) then
            write(Veta,*)'All angular information found'
            NInfo=0
            call FeInfoOut(-1.,-1.,Veta,'L')
            continue
          else
            write(Veta,*)'Some angular information found were missing'
            NInfo=0
            call FeInfoOut(-1.,-1.,Veta,'L')
            goto 1500
          end if
          call CheckJPG(FileNameJPR,nFrames,JPGfine)
          if (JPGFine) then
            write(Veta,*)'All files found'
            NInfo=0
            call FeInfoOut(-1.,-1.,Veta,'L')
            continue
          else
            write(Veta,*)'Some files were missing'
            NInfo=0
            call FeInfoOut(-1.,-1.,Veta,'L')
            goto 1500
          end if
          call ChangeJPG(FileNameJPR,nFrames)
          call LoadMovie(FileNameJPR,nFrames)
          go to 9998
        endif
      end if
      if(ich.ne.0) then
        call FeQuestRemove(id)
        goto 9999
      endif

9998  continue
      call FeQuestRemove(id)
      call HraniSGrafikou(nFrames)
      call system("del.bat")
      call system("del del.bat")
9999  return
      end


      subroutine HraniSGrafikou(nF)
      use transport
      use Basic_mod
      use DRIndex_mod
      use Refine_mod
      parameter (NButt=5)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3),xq(3),xo(3),RMat(3,3),PMat(3,3),xuo(4),yuo(4),
     1          CellDirOld(3),CellParOld(6),QPerO(3),QBasO(3),
     2          CellDirPrev(3,3),CellParPrev(3),CellParSave(6)
      double precision Slozky8(4)
      real :: XDir(3) = (/0.,0.,1./)
      character*256 Veta,BmpPrech(:)
      character*15 :: ButtLabels(NButt) =
     1               (/'%Quit           ',
     2                 '%Print          ',
     3                 '%Save           ',
     4                 ' ----->         ',
     5                 ' <-----         '/)
      character*6 :: IndexLblABS(5) =
     1               (/'omega ','theta ','kappa ','phi   ','#frame'/)
      character*8 FileIndexExt,Nazev
      integer CrwStateQuest,ButtonStateQuest,AktFrame,nF
      logical MameData,MouseActive,CrwLogicQuest,Zacina,PocitejXDir,
     1        FeYesNoHeader,FeYesNo,Nebrat,EqRV,ExistFile,EqIgCase,
     2        JeToTabBin
      logical :: First = (.true.), LatticeOnOld = (.false.)
      equivalence (IdNumbers( 1),IdQuit),
     2            (IdNumbers( 2),IdPrint),
     3            (IdNumbers( 3),IdSave)
      allocatable BmpPrech
      if (allocated(BmpPrech).eqv..false.) allocate(BmpPrech(nF))
      BmpPrech=' '
      NDimIn=NDim(KPhase)
      DiffAxeIn  =DiffAxe
      DiffAngleIn=DiffAngle
      KRefBlock=1
      KPhase=1
      if(.not.allocated(rm6)) call AllocateSymm(1,1,1,1)
      NSymm(KPhase)=1
      NLattVec(KPhase)=1
      Grupa(KPhase)='P1'
      Lattice(KPhase)='P'
      call SetRealArrayTo(QuRefBlock(1,1,KRefBlock),9,0.)
      if(First) then
        First=.false.
        GrIndexColor=Yellow
        GrIndexRad=0.
      endif
      call DRGrIndexSetBasic
      call FeBottomInfo('#prazdno#')
      Id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      CheckKeyboard=.true.
      call FeMakeGrWin(0.,120.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,130.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=100.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      do i=1,NButt                                 ! vytvareni tlacitek
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,ButtLabels(i))
        if(i.eq.IdQuit) then
          nButtQuit=ButtonLastMade
          j=ButtonOff
        else if(i.eq.IdPrint) then
          nButtPrint=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdSave) then
          nButtSave=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.4) then
          nButtVpravo=ButtonLastMade
          j=ButtonOff
        else if(i.eq.5) then
          nButtVlevo=ButtonLastMade
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+dpom-6.
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,White)
      ypom=ypom-dpom
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,
     1                          'Shape recognition')
      nButtShape=ButtonLastMade
      j=ButtonOff
      call FeQuestButtonOpen(ButtonLastMade,j)
      ypom=ypom-dpom
      xpom=15.
      ypom=YMinGrWin-23.
      LocatorType=1
      dpom=FeTxLength('XXXX.XX')+15.
      ypom=YMinGrWin-25.
      xpom=xpom-dpom+15.
      do i=1,5                 !vytvareni spodni listy
        xpom=xpom+dpom+50.
        if(i.le.3) then
          call FeQuestAbsCrwMake(id,xpom,ypom,xpom-33.,YBottomText-7.,
     1                           ' ','L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwCell=CrwLastMade
        endif
        call FeQuestAbsLblMake(id,xpom-3.,YBottomText-2.,
     1                         IndexLblABS(i),'R','N')
        if(i.eq.1) nLblCell=LblLastMade
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      AktFrame=1
      call ABSGrIndexShowAng(AktFrame)
      call FeDeferOutput

!     nacitacni bmpcek pak z toho udelej subroutine
      do ij=1,nF
        do i=1,iYmax
          do j=1,iXmax
            GrIndexColor=RedABS(j,i,ij)+256*
     1        GreenABS(j,i,ij)+65536*BlueABS(j,i,ij)
            call FePoint(real(j)+200.,real(iYmax-i)+200.,GrIndexColor)
          end do
        end do
        BmpPrech(ij)(1:3)='bmp'
        write(Veta,'(i1)')ij
        BmpPrech(ij)(4:4)=Veta(1:1)
        BmpPrech(ij)(5:9)='.bmp'
        call FeSaveImage(200.,real(iXmax)+200.,200.,real(iYmax)+200.,
     1                   BmpPrech(ij))
      end do
      call FeLoadImage(200.,real(iXmax)+200.,200.,real(iYmax)+200.,
     1                 BmpPrech(AktFrame),2)
      FileIndex=' '
      MameData=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
      MouseActive=.false.
      CheckType=EventButton
1500  call FeQuestEvent(id,ich)
1510  if ((CheckType.eq.EventKey.and.CheckNumber.eq.JePageUp).or.
     1  (CheckType.eq.EventButton.and.CheckNumber.eq.nButtVpravo)) then
        if (AktFrame.eq.nF) then
          AktFrame=1
        else
          AktFrame=AktFrame+1
        end if

        call ABSGrIndexShowAng(AktFrame)
        call FeLoadImage(200.,real(iXmax)+200.,200.,real(iYmax)+200.,
     1                 BmpPrech(AktFrame),2)
        call MkTransMat(TransMat,AktFrame)
        call NasobMatMat(InvUBp,TransMat,TMatFin)
        call InvertMat(TMatFin,RotMat)
!        write(6,*)AktFrame
!        write(6,'(3f10.4)')RotMat
        call MorRotujKrystal
        call AbsMorPrumety

        go to 1500

      else if ((CheckType.eq.EventKey.and.CheckNumber.eq.JePageDown)
     1  .or.(CheckType.eq.EventButton.and.CheckNumber.eq.nButtVlevo))
     2  then
        if (AktFrame.eq.1) then
          AktFrame=nF
        else
          AktFrame=AktFrame-1
        end if

        call ABSGrIndexShowAng(AktFrame)
        call FeLoadImage(200.,real(iXmax)+200.,200.,real(iYmax)+200.,
     1                 BmpPrech(AktFrame),2)
        call MkTransMat(TransMat,AktFrame)
        call NasobMatMat(InvUBp,TransMat,TMatFin)
        call InvertMat(TMatFin,RotMat)
        call MorRotujKrystal
        call AbsMorPrumety

        go to 1500

      else if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtShape)
     1  then
        call VeShapeDeterminator(nF)
!        call MorfCryst(0)
        call AbsMorfCryst(0)
        call MkTransMat(TransMat,AktFrame)
        call NasobMatMat(InvUBp,TransMat,TMatFin)
        call InvertMat(TMatFin,RotMat)
        call MorRotujKrystal
        call AbsMorPrumety
        go to 1500

      else if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1   .or.(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape)) then
        call FeQuestRemove(id)
        go to 9000

      end if
      go to 1500


9000  if(allocated(Slozky)) deallocate(Slozky,ProjX,ProjMX,ProjY,
     1                                 ProjMY,Int,FlagS,FlagI,FlagU,
     2                                 ha,iha)
      if(FileIndex.ne.' ') then
        if(Index2d)
     1    call CopyVek(CellParSave,CellRefBlock(1,KRefBlock),6)
        call DRGrIndexIOSmr(1,FileIndex(:idel(FileIndex))//'.smr')
      endif
      DiffAxe  =DiffAxeIn
      DiffAngle=DiffAngleIn
      NDim(KPhase)=NDimIn
      NDimI(KPhase)=NDimIn-3
      NDimQ(KPhase)=NDimIn**2
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      return
100   format(f7.3)
101   format(f7.2)
102   format('q1=',f8.4,'   q2=',f8.4,'    q3=',f8.4)
104   format(4e21.11e4,i11,i6)
      end

      subroutine VeShapeDeterminator(nF)
      use transport
      use vypocetni
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer nF,FramesUsed,R,B,G
      real    UBf(3,3)
      character*256 Veta
      do i=1,3
        do j=1,3
          UBf(i,j)=ubp(j,i)
        end do
      end do
      call InvertMat(UBf,InvUBp)
      iMiller=0
      ich=0
      id=NextQuestId()
      xqd=400.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Shape determination',
     1                   0,LightGray,0,0)
      il=1
      Veta='How many frames do you want to use'
      tpom=10.
      xpom=tpom+FeTxLengthUnder(Veta)+11.
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNFrames=EdwLastMade
      write(Veta,*)nF
      call FeQuestStringEdwOpen(EdwLastMade,Veta)
      tpom=xpom+dpom+15.

1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntAFromEdw(nEdw,n)
        if (n.gt.nF) then
          FramesUsed=nF
        else if (nF.le.10) then
          FramesUsed=nF
        else
          FramesUsed=n
        end if
        do i=1,FramesUsed
          call mkTransMat(TransMat,i)
          call VeBackground(i)
          call VePixFind(i)
          call VeBorderFind(i)
          call VeCalcMiller
        end do
        call vycisti
        do i=1,iMiller
          WRITE(40,*)'miller'
          WRITE(40,*)i
          write(40,'(3(1x,f10.4),f11.6)')Miller(:,i),D(i)
          DFace(1:3,i,KRefBlock)=Miller(1:3,i)
          DFace(4,i,KRefBlock)=D(i)
        end do
        NFaces(KRefBlock)=iMiller
      endif
      if(ich.ne.0) then

      endif

      call FeQuestRemove(id)
      end


      subroutine AbsMorfCryst(Klic)
      use transport
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension DefVect(3),ViewDir(3),PomVek(6),xp(6)
      character Char2
      character*80 t80
      dimension DFaceIn(4,mxface)
      integer CisBut,EventMode,ButtonStateQuest,SbwItemPointerQuest
      logical Recip,FacesNotOK,ctipl,FeYesNo,eqrv,CrwLogicQuest,
     1        Continuously
      parameter (strotuhel=1.)
      if(Klic.ne.0) then
        xp=0.
        do i=1,NFaces(KRefBlock)
          call CopyVek(DFace(1,i,KRefBlock),xp,3)
          call MultM(xp,TRMP,PomVek,NDim(KPhase),NDim(KPhase),1)
          call CopyVek(PomVek,DFaceIn(1,i),3)
          DFaceIn(4,i)=DFace(4,i,KRefBlock)
        enddo
      else
        call CopyVek(DFace(1,1,KRefBlock),DFaceIn,4*NFaces(KRefBlock))
      endif
      NFacesIn=NFaces(KRefBlock)
1030  do i=1,NFaces(KRefBlock)
        call CopyVek(DFace(1,i,KRefBlock),pomvek,3)
        call MorTransVec(.true.,pomvek)
        call CopyVek(pomvek,krov(1,i),3)
        krov(4,i)=DFace(4,i,KRefBlock)
      enddo
      call AbsMorFillArray(FacesNotOK,ctipl)
      if(FacesNotOK) then
        if(ctipl) then
          go to 9999
        else
          go to 1030
        endif
      endif
      call UnitMat(RotMat,3)
      HLFaceNr=0
      Recip=.false.
      scale=0.3
      ichk=0
      do  i=1,NFaces(KRefBlock)
        call CopyVek(DFace(1,i,KRefBlock),krov(1,i),4)
        call MorTransVec(.true.,krov(1,i))
      enddo
      call UnitMat(Axis,3)
      do i=1,3
        call MorTransVec(.false.,Axis(1,i))
      enddo
      TakeMouseMove=.true.
      EventMode=0
      ActFace=0
      Continuously=.false.
      TakeMouseMove=.false.
      AllowChangeMouse=.true.
9999  return
      end


      subroutine MorRotujKrystal
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      do  i=1,PocetBodu
        call multm(RotMat,bod(1,i),rbod(1,i),3,3,1)
      enddo
      do i=1,NFaces(KRefBlock)
        call multm(RotMat,krov(1,i),rkrov(1,i),3,3,1)
      enddo
      do i=1,3
        call multm(RotMat,Axis(1,i),RAxis(1,i),3,3,1)
      enddo
      return
      end



      subroutine AbsMorPrumety
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer Color
      real koef
      logical CrwLogicQuest
      do i=1,NFaces(KRefBlock)
        j=0
1000    j=j+1
        if(rovHr1(i,j).ne.0) then
          if(bodHr(rovHr1(i,j),rovHr2(i,j)).lt.3) then
             bodHr(rovHr1(i,j),rovHr2(i,j))=
     1       bodHr(rovHr1(i,j),rovHr2(i,j))+2
          endif
          go to 1000
        endif
      enddo
      do i=1,NFaces(KRefBlock)
        if(rkrov(3,i).lt.-.0001) cycle
        j=0
1020    j=j+1
        if(rovHr1(i,j).ne.0) then
          if(bodHr(rovHr1(i,j),rovHr2(i,j)).gt.2) then
            bodHr(rovHr1(i,j),rovHr2(i,j))=
     1        bodHr(rovHr1(i,j),rovHr2(i,j))-2
          endif
          go to 1020
        endif
      enddo
      rmax=0.
      do i=1,PocetBodu
        rmax=max(VecOrtLen(Rbod(1,i),3),rmax)
      enddo
      koef=(XLenAcWin*.5-10.)/rmax
1200  if(scale*koef*rmax.gt.XLenAcWin*.5) then
        scale=scale-0.005
        go to 1200
      endif
      pom=scale*koef
      do i=1,PocetBodu
        BodRX(i)=RBod(1,i)*pom+XCenAcWin
        BodRY(i)=RBod(2,i)*pom+YCenAcWin
      enddo
!      call FeClearGrWin
      do i=1, PocetBodu-1
        do j=i+1,PocetBodu
          xu(1)=BodRX(i)
          xu(2)=BodRX(j)
          yu(1)=BodRY(i)
          yu(2)=BodRY(j)
          k=bodHr(i,j)
          if(k.le.2) then
            call FeLineType(NormalLine)
          else
            call FeLineType(DashedLine)
          endif
          if(k.eq.1) then
            Color=White
          else if(k.eq.2) then
            Color=Green
          else if(k.eq.3) then
            Color=LightGray
          else if(k.eq.4) then
            Color=Green
          else
            cycle
          endif
          call FePolyLine(2,xu,yu,Color)
        enddo
      enddo
      if(CrwLogicQuest(nCrwShowAxes)) then
        do i=1,3
          call MorShowVect(RAxis(1,i),SmbABC(i),Yellow)
        enddo
      endif
      call FeLineType(NormalLine)
      return
      end


      subroutine AbsMorFillArray(FacesNotOK,ctipl)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension rmp(3,3),rmi(3,3),xp(3),ps(3),NotOKList(15),
     1          plist(100,100)
      logical chyba,Novy,eqrv,FacesNotOK,ctipl,plist,CrwLogicQuest
      integer PocHr,NotOKList
      character*50 ErrSt,ErrMsg,pomchar
500   do i=1,100
        do j=1,100
          BodRov(i,j)=.false.
          plist(i,j)=.false.
        enddo
      enddo
      call SetIntArrayTo(BodHr,10000,0)
      PocetBodu=0
      FacesNotOK=.false.
      ctipl=.true.
      do i=1,NFaces(KRefBlock)-2
        ps(1)=krov(4,i)
        do m=1,3
          rmp(1,m)=krov(m,i)
        enddo
        do j=i+1,NFaces(KRefBlock)-1
          ps(2)=krov(4,j)
          do m=1,3
            rmp(2,m)=krov(m,j)
          enddo
          do k=j+1,NFaces(KRefBlock)
            do m=1,3
              rmp(3,m)=krov(m,k)
            enddo
            call matinv(rmp,rmi,dets,3)
            if(abs(dets).gt..00001) then
              ps(3)=krov(4,k)
              call multm(rmi,ps,xp,3,3,1)
              chyba=.false.
              do n=1,NFaces(KRefBlock)
                skals=ScalMul(krov(1,n),xp)
                if(abs(skals).gt..00001) then
                  t=krov(4,n)/skals
                else
                  t=0.
                endif
                if(t.lt.0.99999.and.t.gt..00001) then
                  chyba=.true.
                  go to 1210
                endif
              enddo
1210          if(.not.chyba) then
                Novy=.true.
                do l=1,PocetBodu
                  if(eqrv(xp,bod(1,l),3,.001*sqrt(ScalMul(xp,xp))))
     1             then
                    Novy=.false.
                    bodrov(l,i)=.true.
                    bodrov(l,j)=.true.
                    bodrov(l,k)=.true.
                    plist(i,l)=.true.
                    plist(j,l)=.true.
                    plist(k,l)=.true.
                  endif
                enddo
                if(Novy) then
                  PocetBodu=PocetBodu+1
                  call CopyVek(xp,bod(1,PocetBodu),3)
                  bodrov(PocetBodu,i)=.true.
                  bodrov(PocetBodu,j)=.true.
                  bodrov(PocetBodu,k)=.true.
                  plist(i,PocetBodu)=.true.
                  plist(j,PocetBodu)=.true.
                  plist(k,PocetBodu)=.true.
                endif
              endif
            endif
          enddo
        enddo
      enddo
      NotOKNr=0
      do i=1,NFaces(KRefBlock)
        k=0
        do j=1,PocetBodu
          if(plist(i,j)) k=k+1
        enddo
        if(k.lt.3) then
          NotOKNr=NotOKNr+1
          NotOkList(NotOKNr)=i
          FacesNotOk=.true.
        endif
      enddo
      if(FacesNotOK) then
        ctipl=.false.
        do iw=1,NotOKNr
          if(NotOKList(iw).lt.NFaces(KRefBlock)) then
            do i=NotOkList(iw)+1,NFaces(KRefBlock)
              call CopyVek(dface(1,i,KRefBlock),
     1                     dface(1,i-1,KRefBlock),4)
            enddo
            do i=iw,NotOkNr
              NotOKList(i)=NotOkList(i)-1
            enddo
          endif
          NFaces(KRefBlock)=NFaces(KRefBlock)-1
          nCrw=nCrw+1
          nEdw=nEdw+1
        enddo
      else
        PocetHran=0
        do i=1,PocetBodu-1
          do 2400j=i+1,PocetBodu
            n=0
            BodHr(i,j)=0
            do k=1,NFaces(KRefBlock)
              if(BodRov(i,k).and.BodRov(j,k)) then
                n=n+1
                if(n.gt.1) then
                  BodHr(i,j)=1
                  PocetHran=PocetHran+1
                  go to 2400
                endif
              endif
            enddo
2400      continue
        enddo
        do i=1,NFaces(KRefBlock)
          PocHr=0
          do j=1,PocetBodu-1
            if(BodRov(j,i)) then
              do k=j+1,PocetBodu
                if(BodRov(k,i).and.BodHr(j,k).gt.0) then
                  PocHr=PocHr+1
                  RovHr1(i,PocHr)=j
                  RovHr2(i,PocHr)=k
                endif
              enddo
            endif
          enddo
          RovHr1(i,PocHr+1)=0
          RovHr2(i,PocHr+1)=0
        enddo
        call MorUzavreny(FacesNotOK,ctipl)
      endif
      return
      end


      subroutine VeCalcMiller
      use transport
      use vypocetni

      I=0
      do Y=1,iYmax
        do X=1,iXmax
          if (tag(x,y)/=0) then
           I=I+1
           call random_number(xx)
           Xpi(I)=x+((xx-0.5))
           Yp(I)=iYmax-y+((xx-0.5))
          end if
        end do
      enddo
      Ip=I+1
      poca=.false.
      Tp=0
      osa='ne'

      OPEN(72,'koef.txt')
      OPEN(53,'x_1.txt')
      OPEN(54,'y_1.txt')
      open(40,'log1.txt')
!      open(43,'log2.txt')

      it=0
      iMin=1
      Ip=Ip-1
0510  ii=iMin
      it=it+1
      X1p(it)=Xpi(ii)
      Y1p(it)=Yp(ii)
      Tp(iMin)=1
      xmindist=100
      do ij=1,Ip-1
        if (Tp(ij)/=1) then
         xdist=(((Xpi(ii)-Xpi(ij))**2)+((Yp(ii)-Yp(ij))**2))**0.5
         if (xdist<xmindist) then
          xmindist=xdist
          iMin=ij
         end if
        end if
      end do
      if (it==Ip) then
        GOTO 0520
       else
        GOTO 0510
      end if
0520  do ij=1,Ip-1
       xpi(ij)=x1p(ij)
       yp(ij)=y1p(ij)
      end do




0650  Is=1
      Q=1
      Lp(Is)=1
      Mp(Is)=10
      osa='ne'

0700  Jp(is)=Lp(is)
      Kp(is)=Mp(is)
      IF (Mp(is)>Ip-2) then
       GOTO 0900
      end if
       xAveSou=0
       do i=Jp(is),Kp(is)
        xAveSou=xAveSou+Yp(I)
       end do
       xAveSou=xAveSou/(Kp(is)-Jp(is)+1)
      if ((abs(xAveSou-Yp(Kp(is)))<1.5) .AND.
     1   (abs(xAveSou-Yp(Jp(is)))<1.5).AND.((xAveSou>(kYmaxY-3))
     2   .or.(xAveSou<(kYminY+3))))  then
       osa(is)=' x'
       sol(is,1)=xAveSou
       sol(is,2)=0
       Rp(is)=1
       Mp=Mp+1
       GOTO 0700
      end if
       xAveSou=0
       do i=Jp(is),Kp(is)
        xAveSou=xAveSou+Xpi(I)
       end do
       xAveSou=xAveSou/(Kp(is)-Jp(is)+1)
      if ((abs(xAveSou-Yp(Kp(is)))<1.5) .AND.
     1   (abs(xAveSou-Yp(Jp(is)))<1.5).AND.((xAveSou>(kXmaxX-2))
     2   .or.(xAveSou<(kXminX+2))))    then
       osa(is)=' y'
       sol(is,1)=xAveSou
       sol(is,2)=0
       Rp(is)=1
       Mp=Mp+1
       GOTO 0700
      end if
      if (osa(is)=='ne') then
       call ctverce(Xpi,Yp,Jp,Kp,Rp,sol,is,ns)
       if (Rp(is)>0.97) then
         Mp(is)=Mp(is)+1
         GOTO 0700
        else
         is=is+1
         Lp(is)=Mp(is-1)+1
         if ((Ip-2)-Mp(is-1)<20) then
           Mp(is)=Lp(is)+((Ip-2)-Mp(is-1))
         else
           Mp(is)=Lp(is)+10
          end if
        GOTO 0700
       end if
      end if
      is=is+1
      Lp(is)=Mp(is-1)+1
      if ((Ip-2)-Mp(is-1)<20) then
        Mp(is)=Lp(is)+((Ip-2)-Mp(is-1))
       else
        Mp(is)=Lp(is)+10
       end if
      GOTO 0700

0900  change=.false.
      call spojovani(Xpi,Yp,Rp,change)
      call rohyd(Xpi,Yp,Jp,Kp,Rp,sol,is,change,osa)
      call rohyh(Xpi,Yp,Jp,Kp,Rp,sol,is,change,osa)
      WRITE(72,*)
      WRITE(72,*)'pred spojovanim'
      write(72,*)
      do Q=1,is
       WRITE(72,'('' I='',i5)') Q
       WRITE(72,'('' Osa='',a3)') osa(Q)
       WRITE(72,'('' Lp,Mp:'',2i5)') Jp(Q), Kp(Q)
       WRITE(72,'('' Sol:'',2f12.3)') sol(Q,1), sol(Q,2)
       WRITE(72,'('' R2:'',f8.3)') Rp(Q)
      end do

      call spojovani(Xpi,Yp,Rp,change)
      if (change .eqv. .true.) goto 0900

      if (poca .eqv. .FALSE.) then
       call pocatek(xpi,yp,jp,kp,rp,is,poca,ip)
       do q=1,ip-1
!         WRITE(53,*) Xp(q)
!         WRITE(54,*) Yp(q)

       end do
       GOTO 0650
      end if

      WRITE(72,*)'konecne hodnoty'
      write(72,*)
      do iQ=1,is
       WRITE(72,'('' I='',i5)') iQ
       WRITE(72,'('' Osa='',a3)') osa(iQ)
       WRITE(72,'('' Lp,Mp:'',2i5)') Jp(iQ), Kp(iQ)
       WRITE(72,'('' Sol:'',2f12.3)') sol(iQ,1), sol(iQ,2)
       WRITE(72,'('' R2:'',f8.3)') Rp(iQ)

       do ij= Jp(iq),Kp(iq)
        if (osa(q)/=' y') then
         yy=iYmax-NINT(sol(iq,1)+sol(iq,2)*Xpi(ij))
         xx=Nint(Xpi(ij))
         T(xx,yy)='R'
        else
         xx=NINT(sol(iq,1)+sol(iq,2)*Yp(ij))
         yy=iYmax-Nint(Yp(ij))
         T(xx,yy)='R'
        end if
       end do
      end do

      iXstr=382
      iYstr=274
      do q=1,is
        if ((sol(q,1).ne.0).or.(sol(q,2).ne.0)) then
          goto 1111
         else
          goto 1112
        end if
1111    iMiller=iMiller+1
        vek(1,q)=sol(q,2)
        vek(2,q)=0.0
        vek(3,q)=-1.0
        if (sol(q,2)/=0) then
         nvek(1,q)=-1.0/sol(q,2)
         nvek(2,q)=0.0
         nvek(3,q)=-1.0
         Cnvek(q)=iYstr+(1.0/sol(q,2)*iXstr)
        else
         nvek(1,q)=-1.0
         nvek(2,q)=0.0
         nvek(3,q)=0.0
         Cnvek(q)=iXstr
        end if
        if (sol(q,2)/=0) then
         xpr=(Cnvek(q)-sol(q,1))/(vek(1,q)-nvek(1,q))
         ypr=sol(q,2)*xpr+sol(q,1)
        else
         xpr=iXstr
         ypr=sol(q,1)
        end if
        do ij=1,3
         vek(ij,q)=ABS(vek(ij,q))
        end do
        if (xpr>iXstr) vek(1,q)=-1.0*vek(1,q)
        if (ypr<iYstr) vek(3,q)=-1.0*vek(3,q)
        dist=(SQRT((iYstr-ypr)**2+(iXstr-xpr)**2))*0.001645
        D(iMiller)=dist
        call NormVek(vek(:,q))
!        call NasobVekMat(TransMat,vek(:,q),tvek(:,q))
        call NasobMatMat(InvUBp,TransMat,TMatFin)
        call NasobVekMat(TMatFin,vek(:,q),Miller(:,iMiller))

!        write(6,'(3f10.4)')TMatFin
!        WRITE(6,*)'miller'
!        WRITE(6,*)iMiller
!        WRITE(6,'(3(1x,f10.4),f11.6)')Miller(:,iMiller),D(iMiller)

1112    continue
      end do

      T(kXminX,iYmax-kXminY)='1'
      T(kYmaxX,iYmax-kYmaxY)='2'
      T(kXmaxX,iYmax-kXmaxY)='3'
      T(kYminX,iYmax-kYminY)='4'
      T(iXstr,iYstr)='S'

!      do Y=1,iYmax
!        WRITE(43,*)' '
!        do X=1,iXmax
!          WRITE(43,ADVANCE='no',FMT='(a1)') T(X,Y)
!        end do
!      enddo

      close(40)
!      close(43)
      close(53)
      close(54)
      close(72)

!      write(6,*)'dojelo'
      end

      subroutine vycisti
      use vypocetni
      real :: PrechPol(3,ns), PrechD(ns)
      logical :: cond(:)
      allocatable cond

      if (allocated(cond).eqv..false.) ALLOCATE(cond(iMiller))
      cond=.true.
      do i=1,(iMiller-1)
       do j=(i+1),iMiller
        if (cond(i) .eqv. .false.) exit
        if ((ABS(Miller(1,i)-Miller(1,j))<0.2).and.
     1   (ABS(Miller(2,i)-Miller(2,j))<0.2).and.
     2   (ABS(Miller(3,i)-Miller(3,j))<0.2).and.
     3   (ABS(D(i)-D(j))<0.1)) then
         cond(j)=.false.
        end if
       end do
      end do

      j=0
      do i=1,iMiller
       if (cond(i).eqv..true.) then
        j=j+1
        PrechPol(:,j)=Miller(:,i)
        PrechD(j)=D(i)
       end if
      end do

      do i=1,j
       Miller(:,i)=PrechPol(:,i)
       D(i)=PrechD(i)
      end do
      iMiller=j
      deallocate(cond)
      end


      subroutine pocatek(x,y,jp,kp,r,is,ch,ip)
      parameter (n=2)
      parameter (na=10000)
      parameter (ns=100)
      dimension sol(ns,n),X(na),Y(na),R(ns),xsol(ns,n),xR(ns)
      DIMENSION Jp(ns),Kp(ns),iJp(ns),iKp(ns),rx(na),ry(na)
      LOGICAL ch

      WRITE(40,*)
      WRITE(40,*)
      WRITE(40,*)
      WRITE(40,*)'novy pocatek'
      WRITE(40,*)
      WRITE(40,*)
      WRITE(40,*)

      r2max=0.0
      imax=0
      do I=2,is
       if (R(I)+R(I-1)>r2max) then
        r2max=R(I)+R(I-1)
        imax=I
       end if
      end do
      I=0
      J=Jp(imax)
      do
       I=I+1
       if (I>=ip) exit
       Rx(I)=x(J)
       Ry(I)=y(j)
       if (J==Ip-1) then
        J=0
       end if
       J=J+1
      end do
      do I=1,Ip-1
       X(I)=Rx(I)
       Y(I)=Ry(I)
      end do
      ch=.true.
      end

      subroutine spojovani(Xi,Yi,R,ch) !spojovani primek, ktere by mely byt spojeny
      USE vypocetni
!      parameter (n=10000)
!      parameter (ns=1000)
      Real      Xi(n),Yi(n),R(ns),xsol(ns,2),xR(ns)
      integer   iJp(ns),iKp(ns)
      LOGICAL ch, concave

      icorr=1
      Xdiff=Xi(2)-Xi(1)
      if ((Xi(1)<kYmaxX).and.(Yi(1)>kXminY).and.(xdiff<0)) icorr=-1
      if ((Xi(1)>kYmaxX).and.(Yi(1)>kXmaxY).and.(xdiff<0)) icorr=-1
      if ((Xi(1)>kYminX).and.(Yi(1)<kXmaxY).and.(xdiff>0)) icorr=-1
      if ((Xi(1)<kYminX).and.(Yi(1)<kXminY).and.(xdiff>0)) icorr=-1

4100  I=0
4200  I=I+1
      concave=.false.
      if (I>=is) goto 4600
      rs=R(I)+R(I+1)
      iJp(I)=Jp(I)
      iKp(I)=Kp(I+1)
      call ctverce(Xi,Yi,iJp,iKp,xR,xsol,I,ns)
      xrs=2*xR(I)
      Ydiff=(Yi(iKp(I))-Yi(iJp(I)))*icorr
      Xdiff=(Xi(iKp(I))-Xi(iJp(I)))*icorr
      if ((Xdiff<0).AND.(Ydiff>0).AND.
     1 ((Xi(iKp(I))<kYminX).AND.(Xi(iJp(I))<kYminX).and.
     2 (Yi(iKp(I))<kXminY).and.(Yi(iJp(I))<kXminY))) then
       if (icorr==1) then
         if (ABS(sol(I,2))>ABS(sol(I+1,2))) then
          concave=.true.
         end if
        else
         if (ABS(sol(I,2))<ABS(sol(I+1,2))) then
          concave=.true.
         end if
       endif
      end if
      if ((Xdiff>0).AND.(Ydiff>0).and.
     1 ((Xi(iKp(I))<kYmaxX).AND.(Xi(iJp(I))<kYmaxX).and.
     2 (Yi(iKp(I))>kXminY).and.(Yi(iJp(I))>kXminY))) then
       if (icorr==1) then
         if (ABS(sol(I,2))<ABS(sol(I+1,2))) then
          concave=.true.
         end if
        else
         if (ABS(sol(I,2))>ABS(sol(I+1,2))) then
          concave=.true.
         end if
       endif
      end if
      if ((Xdiff>0).AND.(Ydiff<0).and.
     1 ((Xi(iKp(I))>kYmaxX).AND.(Xi(iJp(I))>kYmaxX).and.
     2 (Yi(iKp(I))>kXmaxY).and.(Yi(iJp(I))>kXmaxY))) then
       if (icorr==1) then
         if (ABS(sol(I,2))>ABS(sol(I+1,2))) then
          concave=.true.
         end if
        else
         if (ABS(sol(I,2))<ABS(sol(I+1,2))) then
          concave=.true.
         end if
       endif
      end if
      if ((Xdiff<0).AND.(Ydiff<0).and.
     1 ((Xi(iKp(I))>kYminX).AND.(Xi(iJp(I))>kYminX).and.
     2 (Yi(iKp(I))<kXmaxY).and.(Yi(iJp(I))<kXmaxY))) then
       if (icorr==1) then
         if (ABS(sol(I,2))<ABS(sol(I+1,2))) then
          concave=.true.
         end if
        else
         if (ABS(sol(I,2))>ABS(sol(I+1,2))) then
          concave=.true.
         end if
       endif
      end if

      WRITE(40,*)
      WRITE(40,*) 'I=',I,'Jp',Jp(I),'Kp',Kp(I),'R',R(I)
      WRITE(40,*) 'I+1=',I+1,'Jp',Jp(I+1),'Kp',Kp(I+1),'R',R(I+1)
      WRITE(40,*) 'rozdil',ABS(sol(I,2)-sol(I+1,2))
      WRITE(40,*) 'xdiff',Xdiff,'ydiff',Ydiff
      WRITE(40,*) 'x1', Xi(iKp(I)),'x2',Xi(iJp(I))
      WRITE(40,*) 'y1', Yi(iKp(I)),'y2',Yi(iJp(I))
      WRITE(40,*) concave, icorr
      WRITE(40,*) 'xrs=',xrs,'rs=',rs
      WRITE(40,*)


      if (((xrs>rs).AND.(osa(I)=='ne').AND.(osa(I+1)=='ne'))
     1 .or. (concave .eqv. .true.) ) then

        WRITE(40,*)'spojeno'
        WRITE(40,'(20(''-''))')
        ch=.true.
        Jp(I)=iJp(I)
        Kp(I)=iKp(I)
        R(I)=xr(I)
        sol(I,1)=xsol(I,1)
        sol(I,2)=xsol(I,2)
        do ij= I+1,is
         Jp(ij)=Jp(ij+1)
         Kp(ij)=Kp(ij+1)
         R(ij)=R(ij+1)
         sol(ij,1)=sol(ij+1,1)
         sol(ij,2)=sol(ij+1,2)
         osa(ij)=osa(ij+1)
        end do
        is=is-1
        goto 4100
       else
        WRITE(40,*)'nespojeno'
        WRITE(40,'(20(''-''))')
        goto 4200
      end if
      goto 4200
4600  return
      end

      subroutine rohyh(X,Y,Jp,Kp,R,sol,is,ch,osa)     !hledani rohu smerem vzhuru
      parameter (n=2)
      parameter (na=10000)
      parameter (ns=100)
      dimension sol(ns,n),X(na),Y(na),R(ns),xsol(ns,n),xR(ns)
      DIMENSION Jp(ns),Kp(ns),iJp(ns),iKp(ns)
      CHARACTER*2 osa(ns)
      LOGICAL ch
      I=1
3100  I=I+1
      if (I>is) goto 3600
3200  if (osa(I)=='ne'.and.osa(I-1)=='ne') then
       rs=R(I)+R(I-1)
       iJp(I)=Jp(I)+1
       iKp(I)=Kp(I)
       iJp(I-1)=Jp(I-1)
       iKp(I-1)=Kp(I-1)+1
       call ctverce(X,Y,iJp,iKp,xR,xsol,I,ns)
       call ctverce(X,Y,iJp,iKp,xR,xsol,I-1,ns)
       xrs=xR(I)+xR(I-1)
       if (xrs>rs) then
         Jp(I)=iJp(I)
         Kp(I)=iKp(I)
         Jp(I-1)=iJp(I-1)
         Kp(I-1)=iKp(I-1)
         R(I)=xR(I)
         R(I-1)=xR(I-1)
         sol(I,1)=xsol(I,1)
         sol(I,2)=xsol(I,2)
         sol(I-1,1)=xsol(I-1,1)
         sol(I-1,2)=xsol(I-1,2)
         ch=.true.
         goto 3200
        else
         goto 3100
        end if
      end if
      goto 3100
3600  end

      subroutine rohyd(X,Y,Jp,Kp,R,sol,is,ch,osa)     !hledani rohu smerem dolu
      parameter (n=2)
      parameter (na=10000)
      parameter (ns=100)
      dimension sol(ns,n),X(na),Y(na),R(ns),xsol(ns,n),xR(ns)
      DIMENSION Jp(ns),Kp(ns),iJp(ns),iKp(ns)
      CHARACTER*2 osa(ns)
      LOGICAL ch
      I=1
2100  I=I+1
      if (I>is) goto 2600
2200  if (osa(I)=='ne'.and.osa(I-1)=='ne') then
       rs=R(I)+R(I-1)
       iJp(I)=Jp(I)-1
       iKp(I)=Kp(I)
       iJp(I-1)=Jp(I-1)
       iKp(I-1)=Kp(I-1)-1
!       WRITE(*,*)'jede rohyd',I,is
       call ctverce(X,Y,iJp,iKp,xR,xsol,I,ns)
!       WRITE(*,*)'jede rohyd1',I,is,iJp(i),iKp(i)
       call ctverce(X,Y,iJp,iKp,xR,xsol,I-1,ns)
!       WRITE(*,*)'jede rohyd2',i,is,iJp(I-1),iKp(I-1)
       xrs=xR(I)+xR(I-1)
       if (xrs>rs) then
         Jp(I)=iJp(I)
         Kp(I)=iKp(I)
         Jp(I-1)=iJp(I-1)
         Kp(I-1)=iKp(I-1)
         R(I)=xR(I)
         R(I-1)=xR(I-1)
         sol(I,1)=xsol(I,1)
         sol(I,2)=xsol(I,2)
         sol(I-1,1)=xsol(I-1,1)
         sol(I-1,2)=xsol(I-1,2)
         ch=.true.
         goto 2200
        else
         goto 2100
        end if
      endif
      if (osa(I)==' x') then
       WRITE(40,*) 'i',I,Jp(I),Kp(I),ABS(Y(Jp(I))-Y(Kp(I-1))),
     1  Y(Jp(I)),Y(Kp(I-1)),sol(I,1)
       if ((ABS(sol(I,1)-Y(Kp(I-1)))<1.5).and.
     1  (ABS(Jp(I-1)-Kp(I-1))>2)) then
!       if (ABS(sol(I,1)-Y(Kp(I-1)))<1.5) then
        Jp(I)=Kp(I-1)
        Kp(I-1)=Kp(I-1)-1
!        WRITE(*,*)'jede osa x'
        call ctverce(X,Y,Jp,Kp,R,sol,I-1,ns)
!        WRITE(*,*)'po ose x'
        goto 2200
       end if
      end if
      if (osa(I)==' y') then
       WRITE(40,*) 'i',I,Jp(I),Kp(i),ABS(X(Jp(I))-X(Kp(I-1))),
     1  X(Jp(I)),X(Kp(I)),sol(I,1)
       if (ABS(sol(I,1)-X(Kp(I-1)))<1.5) then
        Jp(I)=Kp(I-1)
        Kp(I-1)=Kp(I-1)-1
        call ctverce(X,Y,Jp,Kp,R,sol,I-1,ns)
        goto 2200
       end if
      end if

      goto 2100
2600  end

      subroutine ctverce(X,Y,Jp,Kp,R,sol,is,ns)          !vypocet linearni regrese
      parameter (n=2)
      parameter (na=10000)
!      parameter (ns=100)
      dimension am((n*(n+1))/2),der(n),ps(n),sol(ns,n),X(na),Y(na),R(ns)
      DIMENSION Jp(ns),Kp(ns)
      am=0.
      ps=0.
      m=0
      ave=0.0
      sstot=0.0
      sserr=0.0
      do im= Jp(is), Kp(is)
       der(1)=1.
       der(2)=x(im)
       Yo=Y(im)
       ave=ave+y(im)
       k=0
       do i=1,n
         ps(i)=ps(i)+y(im)*der(i)
         do j=1,i
           k=k+1
           am(k)=am(k)+der(i)*der(j)
         enddo
       enddo
      enddo
      ave=ave/(Kp(is)-Jp(is)+1)
      ij=0
      do i=1,n
        ij=ij+i
        der(i)=sqrt(am(ij))
      enddo
      call znorm(am,der,n)
      call smi(am,n,ising)
      if(ising.gt.0) then
        write(40,'('' Matrix is singular'')')
        go to 9999
      endif
      call znorm(am,der,n)
      sol(is,2)=1.0
      call VeNasob(am,ps,sol,n,is)
      do im=Jp(is),Kp(is)
       sstot=sstot+((Y(im)-ave)**2)
       sserr=sserr+(Y(im)-(X(im)*sol(is,2)+sol(is,1)))**2
      end do
      R(is)=1-sserr/sstot
      if (R(is)<0.9) R(is)=0.9
      goto 9997
9999  stop
9997  end



      subroutine VeBorderFind(rank)
      use transport
      use vypocetni
      integer rank
      real    Rave,Bave,Gave

      do y=2,iYmax-1
        do x=2,iXmax-1

          if (tag(x,y)==1) then
            jkl=0
            icond=0
            Jxmin=x
            Jxmax=x+1
            Jymin=y
            J=y-1

            do
             J=J+1
             I=Jxmin
             if (tag(I,J)/=1) then
              do
               I=I+1
               if (tag(I,J)==1) then
                 Jxmin=I
                 exit
               else if (I>Jxmax) then
                 Jkl=1
                 exit
               endif
              enddo
             end if
             if (jkl==1) exit

             do   !hledani zacatku
              if (tag(I-1,J)==1) then
               I=I-1
              else
               exit
              end if
             enddo
             Jxmin=I

             do   !hledani konce
              iS=iS+1
              tag(I,J)=2
!              WRITE(40,*) iXmax/2, iYmax/2, I, J
              if ((I==iXmax/2).AND.(J==iYmax/2)) then
                icond=1
              end if
              if (tag(I+1,J)==1) then
               I=I+1
              else if ((tag(I+1,J)/=1).AND.(I+1<jxmax).AND.
     1                (tag(I+1,J-1)==2)) then
               I=I+1
               iS=iS-1
              else
               exit
              end if
             enddo
             Jxmax=I
            enddo



            if (icond/=1) then
!              WRITE(40,*) y, x, icond, jxmax,'mazani'
              do Ik=2,iYmax-3
               do Jk=2,iXmax-3
                if (tag(Jk,Ik)==2) then
                 tag(Jk,Ik)=0
                 T(Jk,Ik)=' '
                end if
               end do
              end do
             else
!              WRITE(40,*) y, x, jxmax,'mazani'
              do I=2,iYmax-1
               do J=2,iXmax-1
                if ((tag(J,I)==2).and.(I<(iYmax-jYmin))) then
                 tag(J,I)=3
                end if
               end do
              end do
            end if
          end if
        end do
      end do
      Rave=0.0
      Bave=0.0
      Gave=0.0
      I=0

      do Y=2,iYmax/2
        do X=2,iXmax-1
          if (tag(x,y)==3) then
           Rave=Rave+RedABS(x,y,rank)
           Bave=Bave+BlueABS(x,y,rank)
           Gave=Gave+GreenABS(x,y,rank)
           I=I+1
          end if
        end do
      end do

      Rave=Rave/I
      Bave=Bave/I
      Gave=Gave/I



      do Y=12,iYmax-11
        do X=12,iXmax-11
          xchange=((RedABS(X,Y,rank)-Rave)**2
     1            +(BlueABS(X,Y,rank)-Bave)**2
     2            +(GreenABS(X,Y,rank)-Gave)**2)**0.5
          if ((xchange<100).AND.(tag(x,y)==3)) then
            tag(x,y)=4
          end if
        enddo
      enddo

      do Y=2,iYmax-1
        do X=2,iXmax-1
          if (tag(x,y)/=4) then
            tag(x,y)=0
            T(x,y)=' '
          else
            tag(x,y)=1
            T(x,y)='#'
          end if
        end do
      end do

      do y=2,iYmax-1
        do x=2,iXmax-1
          if (tag(x,y)==1) then
            jkl=0
            icond=0
            Jxmin=x
            Jxmax=x+1
            J=y-1
            do
             J=J+1
             I=Jxmin
             if (tag(I,J)/=1) then
              do
               I=I+1
               if (tag(I,J)==1) then
                 Jxmin=I
                 exit
               else if (I>Jxmax) then
                 Jkl=1
                 exit
               endif
              enddo
             end if
             if (jkl==1) exit
             do   !hledani zacatku
              if (tag(I-1,J)==1) then
               I=I-1
              else
               exit
              end if
             enddo
             Jxmin=I
             do   !hledani konce
              iS=iS+1
              tag(I,J)=2
!              WRITE(40,*) iXmax/2, iYmax/2, I, J
              if ((I==iXmax/2).AND.(J==iYmax/2)) then
                icond=1
              end if
              if (tag(I+1,J)==1) then
               I=I+1
              else if ((tag(I+1,J)/=1).AND.(I+1<jxmax).AND.
     1                (tag(I+1,J-1)==2)) then
               I=I+1
               iS=iS-1
              else
               exit
              end if
             enddo
             Jxmax=I
            enddo

            if (icond/=1) then
!              WRITE(40,*) y, x, icond, jxmax,'mazani'
              do Ik=2,iYmax-3
               do Jk=2,iXmax-3
                if (tag(Jk,Ik)==2) then
                 tag(Jk,Ik)=0
                 T(Jk,Ik)=' '
                end if
               end do
              end do
             else
!              WRITE(40,*) y, x, jxmax,'mazani'
              jYmin=y
              jYmax=J
              do I=2,iYmax-1
               do J=2,iXmax-1
                if (tag(J,I)==2) then
                 tag(J,I)=3
                end if
               end do
              end do
            end if
          end if
        end do
      end do

      kXminX=ixmax-1
      kXmaxX=0
      kYminY=0
      kYmaxY=iYmax
      do y=2,iYmax-1
        do x=2,iXmax-1
          if (tag(x,y)==3) then
           if ((tag(x-1,y)/=0).AND.(tag(x,y-1)/=0).and.
     1      (tag(x+1,y)/=0).AND.(tag(x,y+1)/=0)) then
            tag(x,y)=1
            T(x,y)=' '
           else
            T(x,y)='#'
           end if
           if (x<kXminX) then
            kXminX=x
            kXminY=iYmax-y
           end if
           if (x>kXmaxX) then
            kXmaxX=x
            kXmaxY=iYmax-y
           end if
           if (y>kYminY) then
            kYminX=x
            kYminY=y
           end if
           if (y<kYmaxY) then
            kYmaxX=x
            kYmaxY=y
           end if
          end if
        end do
      end do

      kYmaxY=iYmax-kYmaxY
      kYminY=iYmax-kYminY

      do y=2,iYmax-1
        do x=2,iXmax-1
          if (tag(x,y)==1) then
           tag(x,y)=0
          end if
        end do
      end do

!      WRITE(6,*)
!      WRITE(6,*)
!      WRITE(6,*) 'Xmin',kXminX,kXminY,'Xmax',kXmaxX,kXmaxY
!      WRITE(6,*) 'Ymin',kYminX,kYminY,'Ymax',kYmaxX,kYmaxY
!      WRITE(6,*)
!      WRITE(6,*)

      call WriteT
      end

      subroutine VePixFind(rank)
      use transport
      integer rank,R,B,G,Y,X,Rave,Bave,Gave
      real chmax,xchange
      logical ColChange
      if (allocated(tag).eqv..true.) deallocate(tag)
      if (allocated(T).eqv..true.) deallocate(T)
      allocate(tag(iXmax,iYmax),T(iXmax,iYmax))
      tag=0
      T=' '
      I=0
      do Y=12,42
        do X=12,42
         R=R+RedABS(x,y,rank)
         B=B+BlueABS(x,y,rank)
         G=G+GreenABS(x,y,rank)
         I=I+1
        enddo
      enddo
      R=R/I
      B=B/I
      G=G/I


      do Y=12,iYmax-11
        do X=12,iXmax-11
          Rave=R+INT(RrozX*X)+INT(RrozY*Y)
          Bave=B+INT(BrozX*X)+INT(BrozY*Y)
          Gave=G+INT(GrozX*X)+INT(GrozY*Y)
          ColChange=.false.
          chmax=0
          xchange=((RedABS(X,Y,rank)-Rave)**2
     1           +(BlueABS(X,Y,rank)-Bave)**2
     2           +(GreenABS(X,Y,rank)-Gave)**2)**0.5
          if (xchange>20) then
            chmax=xchange
            ColChange=.true.
            tag(x,y)=1
          end if
          if (ColChange .eqv. .true.) then
            if (chmax>50) then
              T(X,Y)='#'
            else if (chmax>30) then
              T(X,Y)='*'
            else if (chmax>20) then
              T(X,Y)='.'
            else
              T(X,Y)=' '
            endif
          endif
        end do
      end do
!      call WriteT

      end

      subroutine WriteT
      use transport
      integer Y,X

      if (allocated(T).eqv..false.) go to 9999

      open(41,'output.txt')
      do Y=1,iYmax
        WRITE(41,*)' '
        do X=1,iXmax
          WRITE(41,ADVANCE='no',FMT='(a1)') T(X,Y)
        end do
      enddo
      close(41)

9999  end


      subroutine VeBackground(rank)
      use transport
      real rX,rY
      integer rank,R(4),B(4),G(4)

      do I=1,1               !potlaceni sumu
        do Y=2,iYmax-1
          do X=2,iXmax-1
            RedABS(x,y,rank)=(RedABS(x,y,rank)+RedABS(x-1,y,rank)
     1                       +RedABS(x+1,y,rank)+RedABS(x,y-1,rank)
     2                       +RedABS(x,y+1,rank))/5
            BlueABS(x,y,rank)=(BlueABS(x,y,rank)+BlueABS(x-1,y,rank)
     1                       +BlueABS(x+1,y,rank)+BlueABS(x,y-1,rank)
     2                       +BlueABS(x,y+1,rank))/5
            GreenABS(x,y,rank)=(GreenABS(x,y,rank)
     1                   +GreenABS(x-1,y,rank)+GreenABS(x+1,y,rank)
     2                   +GreenABS(x,y-1,rank)+GreenABS(x,y+1,rank))/5
          end do
        end do
      end do

      do Y=12,42
        do X=12,42
          R(1)=R(1)+RedABS(x,y,rank)
          B(1)=B(1)+BlueABS(x,y,rank)
          G(1)=G(1)+GreenABS(x,y,rank)
          I=I+1
        end do
      end do
      R(1)=R(1)/I
      B(1)=B(1)/I
      G(1)=G(1)/I
!      WRITE(6,*)R(1),B(1),G(1)
      I=0
      do Y=12,42
        do X=iXmax-42,iXmax-11
          R(2)=R(2)+RedABS(x,y,rank)
          B(2)=B(2)+BlueABS(x,y,rank)
          G(2)=G(2)+GreenABS(x,y,rank)
          I=I+1
        end do
      end do
      R(2)=R(2)/I
      B(2)=B(2)/I
      G(2)=G(2)/I
!      WRITE(6,*)R(2),B(2),G(2)
      I=0
      do Y=iYmax-42,iYmax-11
        do X=12,42
          R(3)=R(3)+RedABS(x,y,rank)
          B(3)=B(3)+BlueABS(x,y,rank)
          G(3)=G(3)+GreenABS(x,y,rank)
          I=I+1
        end do
      end do
      R(3)=R(3)/I
      B(3)=B(3)/I
      G(3)=G(3)/I
!      WRITE(6,*)R(3),B(3),G(3)
      I=0
      do Y=iYmax-42,iYmax-11
        do X=iXmax-42,iXmax-11
          R(4)=R(4)+RedABS(x,y,rank)
          B(4)=B(4)+BlueABS(x,y,rank)
          G(4)=G(4)+GreenABS(x,y,rank)
          I=I+1
        enddo
      enddo
      R(4)=R(4)/I
      B(4)=B(4)/I
      G(4)=G(4)/I
!      WRITE(6,*)R(4),B(4),G(4)
      I=0

      rX=iXmax-22
      RrozX=(R(2)-R(1))/rX
      BrozX=(B(2)-B(1))/rX
      GrozX=(G(2)-G(1))/rX
      rY=iYmax-22
      RrozY=(R(3)-R(1))/rY
      BrozY=(B(3)-B(1))/rY
      GrozY=(G(3)-G(1))/rY
!      write(6,*)rX,rY

      end

      subroutine znorm(a,d,n)                         !normalizace matice v linearizovanem tvaru
      dimension a(*),d(n)
      k=0
      do j=1,n
        dj=d(j)
        do i=1,j
          k=k+1
          a(k)=a(k)*dj*d(i)
        enddo
      enddo
      return
      end

      subroutine VeNasob(a,p,x,n,is)                    !nasobeni matic v linearizovanem tvaru
      integer, intent(in) :: n,is
      real, dimension(*), intent(in) :: a
      real, dimension(n), intent(in) :: p
      real, dimension(1000,n), intent(out) :: x
      ijp=0
      do i=1,n
        xi=0.0
        ij=ijp
        id=1
        do j=1,n
          ij=ij+id
          xi=xi+a(ij)*p(j)
          if(j.ge.i) id=j
        enddo
        x(is,i)=xi
        ijp=ijp+i
      enddo
      return
      end


      subroutine mkTransMat(Rout,rank)                !vytvoreni transofrmacni matice z uhlu natoceni krystalu
      USE transport
      REAL,INTENT(out) :: Rout(3,3)
      INTEGER,INTENT(in) :: rank
      REAL :: Ro(3,3),Rk(3,3),Rp(3,3),R2(3,3),R3(3,3),corr(3,3),pi,
     1        omeSup,kapSup,phiSup
      pi=3.1415926535
      omeSup=omeABS(rank)/180.0*pi
      kapSup=kapABS(rank)/180.0*pi
      phiSup=phiABS(rank)/180.0*pi

      call Rot3(Ro,omeSup)
      call Rot3(Rk,kapSup)
      call Rot3(Rp,phiSup)
      call NasobMatMat(Ro,Rk,R2)
      call NasobMatMat(R2,Rp,R3)

!      write(6,*)omeSup,kapSup,phiSup
      corr=0
      corr(1,1)=1.0
      corr(2,2)=-1.0
      corr(3,3)=1.0
      call NasobMatMat(R3,corr,Rout)

!      write(6,*)'TransMat generation'
!      WRITE(6,*)'Ro'
!      WRITE(6,'(3(" ",f13.7))')Ro
!      WRITE(6,*)'Rk'
!      WRITE(6,'(3(" ",f13.7))')Rk
!      WRITE(6,*)'Rp'
!      WRITE(6,'(3(" ",f13.7))')Rp
!      WRITE(6,*)'Rout'
!      WRITE(6,'(3(" ",f13.7))')Rout

      end

      subroutine NasobMatMat(RinL,RinP,Rout)          !nasobeni matice 3x3 matici 3x3
      REAL,INTENT(in) :: RinL(3,3), RinP(3,3)
      REAL,INTENT(out) :: Rout(3,3)

      Rout=0
      do i=1,3
       do j=1,3
        do k=1,3
         Rout(j,i)=Rout(j,i)+RinL(k,i)*RinP(j,k)
        end do
       end do
      end do

      end

      subroutine NormVek(Vek)                         !normalizace vektoru
      REAL :: vek(3,1),norm

      norm=SQRT(vek(1,1)**2+vek(2,1)**2+vek(3,1)**2)
      do i=1,3
        vek(i,1)=vek(i,1)/norm
      end do

      end

      subroutine NasobVekMat(Rin,VekIn,VekOut)        !nasobeni vektoru 3x1 matici 3x3
      REAL,INTENT(in) :: Rin(3,3),VekIn(3,1)
      REAL,INTENT(out) :: VekOut(3,1)

      VekOut=0
      do i=1,3
       do j=1,3
        VekOut(i,1)=VekOUt(i,1)+(Rin(j,i)*VekIn(j,1))
       end do
      end do

      end

      subroutine Rot3(Rout,angle)                     !rotace kolem osy z
      REAL,INTENT(out) :: Rout(3,3)
      REAL,INTENT(in) :: angle

      Rout=0
      Rout(3,3)=1
      Rout(1,1)=COS(angle)
      Rout(2,1)=-1.0*SIN(angle)
      Rout(1,2)=SIN(angle)
      Rout(2,2)=COS(angle)

      end


      subroutine Rotation(Rout,angle,key)                     !rotace kolem osy z
      REAL,INTENT(out) :: Rout(3,3)
      REAL,INTENT(in) :: angle
      character,intent(in) :: key

      if (key.eq.'z') then
        Rout=0
        Rout(3,3)=1
        Rout(1,1)=COS(angle)
        Rout(2,1)=-1.0*SIN(angle)
        Rout(1,2)=SIN(angle)
        Rout(2,2)=COS(angle)
      else if (key.eq.'y') then
        Rout=0
        Rout(2,2)=1
        Rout(1,1)=COS(angle)
        Rout(3,1)=-1.0*SIN(angle)
        Rout(1,3)=SIN(angle)
        Rout(3,3)=COS(angle)
      else if (key.eq.'x') then
        Rout=0
        Rout(1,1)=1
        Rout(2,2)=COS(angle)
        Rout(3,2)=-1.0*SIN(angle)
        Rout(2,3)=SIN(angle)
        Rout(3,3)=COS(angle)
      end if

      end



      subroutine InvertMat(R,invR)                    !inverze matice 3x3
      REAL,INTENT(in) :: R(3,3)
      REAL,INTENT(out) :: invR(3,3)
      REAL :: det, Rt(3,3), Radj(3,3)

      det=(R(1,1)*R(2,2)*R(3,3)+R(1,2)*R(2,3)*R(3,1)
     1 +R(1,3)*R(2,1)*R(3,2)-R(3,1)*R(2,2)*R(1,3)
     2 -R(3,2)*R(2,3)*R(1,1)-R(3,3)*R(2,1)*R(1,2))

      do i=1,3
       do j=1,3
        Rt(j,i)=R(i,j)
       end do
      end do

      Radj(1,1)=Rt(2,2)*Rt(3,3)-Rt(3,2)*Rt(2,3)
      Radj(2,1)=-1.0*(Rt(1,2)*Rt(3,3)-Rt(3,2)*Rt(1,3))
      Radj(3,1)=Rt(1,2)*Rt(2,3)-Rt(2,2)*Rt(1,3)
      Radj(1,2)=-1.0*(Rt(2,1)*Rt(3,3)-Rt(3,1)*Rt(2,3))
      Radj(2,2)=Rt(1,1)*Rt(3,3)-Rt(3,1)*Rt(1,3)
      Radj(3,2)=-1.0*(Rt(1,1)*Rt(2,3)-Rt(2,1)*Rt(1,3))
      Radj(1,3)=Rt(2,1)*Rt(3,2)-Rt(3,1)*Rt(2,2)
      Radj(2,3)=-1.0*(Rt(1,1)*Rt(3,2)-Rt(3,1)*Rt(1,2))
      Radj(3,3)=Rt(1,1)*Rt(2,2)-Rt(2,1)*Rt(1,2)

      call NasobMatCis(1/det,Radj,invR)

      end

      subroutine NasobMatCis(X,Rin,Rout)              !nasobemi matice 3x3 cislem
      REAL,INTENT(in) :: X,Rin(3,3)
      REAL,INTENT(out) :: Rout(3,3)

      do i=1,3
       do j=1,3
        Rout(j,i)=X*Rin(j,i)
       end do
      end do

      end


      subroutine ABSGrIndexShowAng(nF)
      use DRIndex_mod
      use transport
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer nF
      character*6 for
      ik=5
      do i=1,ik
        for='(f8.3)'
        if (i==1) then
          write(Cislo,for) omeABS(nF)
        else if (i==2) then
          write(Cislo,for) theABS(nF)
        else if (i==3) then
          write(Cislo,for) kapABS(nF)
        else if (i==4) then
          write(Cislo,for) phiABS(nF)
        else
          write(Cislo,'(i4)') nF
        end if
        call FeWInfWrite(i,Cislo)
      enddo
100   format(f7.3)
101   format(f7.2)
      return
      end



      subroutine LoadMovie(FileName,nF)
      use transport
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,FileName!,BmpNameABS
      character*3 FrameNumber
      integer nF,Del,dil
      if (allocated(BmpNameABS).eqv..false.) allocate(BmpNameABS(nF))
      do i=1,nF
        if (i<10) then
          write(FrameNumber,'(i1)')i
          dil=0
        else if(i<100) then
          write(FrameNumber,'(i2)')i
          dil=1
        else
          write(FrameNumber,'(i3)')i
          dil=2
        end if
        Del=len(trim(FileName))
        BmpNameABS(i)(1:Del-4)=FileName(1:Del-4)
        BmpNameABS(i)(Del-3:Del-3)='1'
        BmpNameABS(i)(Del-2:Del-2+dil)=FrameNumber(1:1+dil)
        BmpNameABS(i)(Del-1+dil:Del+2+dil)='.bmp'  !docasne reseni nez se prijde na to proc to odmita nacitat bmp ktere vytvori imagemagick
        if(allocated(iBitmapABS)) Deallocate(iBitmapABS)
        call FeLoadBMPFileABS(BmpNameABS(i),iXmax,iYmax)
        if (allocated(BlueABS).eqv..false.)
     1     allocate(BlueABS(iXmax,iYmax,nF))
        if (allocated(GreenABS).eqv..false.)
     1     allocate(GreenABS(iXmax,iYmax,nF))
        if (allocated(RedABS).eqv..false.)
     1     allocate(RedABS(iXmax,iYmax,nF))

        do ii=1,iYmax
          do ij=5,iXmax*5,5
            BlueABS(iBitmapAbs(ij-1,ii),iBitmapAbs(ij,ii),i)=
     1        iBitmapABS(ij-4,ii)
            GreenABS(iBitmapAbs(ij-1,ii),iBitmapAbs(ij,ii),i)=
     1        iBitmapABS(ij-3,ii)
            RedABS(iBitmapAbs(ij-1,ii),iBitmapAbs(ij,ii),i)=
     1        iBitmapABS(ij-2,ii)
!            if (ij>=10) exit
          end do
!          if (ii>2)exit
        end do
!        write(6,*)BlueABS(1,1,i), GreenABS(1,1,i), RedABS(1,1,i)
!        write(6,*)BlueABS(1,2,i), GreenABS(1,2,i), RedABS(1,2,i)

!        exit
      end do

      end

      subroutine ChangeJPG(FileName,nF)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,FileName,JpgName,BmpNameABS
      character*3 FrameNumber
      integer nF,Del,dil
      open(101,'get.bat')
      open(102,'del.bat')
      do i=1,nF
        if (i<10) then
          write(FrameNumber,'(i1)')i
          dil=0
        else if(i<100) then
          write(FrameNumber,'(i2)')i
          dil=1
        else
          write(FrameNumber,'(i3)')i
          dil=2
        end if
        Del=len(trim(FileName))
        JpgName(1:Del-4)=FileName(1:Del-4)
        JpgName(Del-3:Del-3+dil)=FrameNumber(1:1+dil)
        JpgName(Del-2+dil:Del+1+dil)='.jpg'
        BmpNameABS(1:Del-4)=FileName(1:Del-4)
        BmpNameABS(Del-3:Del-3+dil)=FrameNumber(1:1+dil)
        BmpNameABS(Del-2+dil:Del+1+dil)='.bmp'

        write(101,*)"convert " // JpgName(1:Del+1+dil) // " "
     1     //BmpNameABS(1:Del+1+dil)
        write(102,*)"del " //BmpNameABS(1:Del+1+dil)

      end do
      write(102,*)"del get.bat"
      close(101)
      close(102)
      call system("get.bat")
      end

      subroutine CheckJPG(FileName,nF,fine)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,FileName,Veta2
      character*3 FrameNumber
      integer nF,Del,dil
      logical fine,ExistFile
      fine=.true.
      do i=1,nF
        if (i<10) then
          write(FrameNumber,'(i1)')i
          dil=0
        else if(i<100) then
          write(FrameNumber,'(i2)')i
          dil=1
        else
          write(FrameNumber,'(i3)')i
          dil=2
        end if
        Del=len(trim(FileName))
        Veta2(1:Del-4)=FileName(1:Del-4)
        Veta2(Del-3:Del-3+dil)=FrameNumber(1:1+dil)
        Veta2(Del-2+dil:Del+1+dil)='.jpg'
        if(ExistFile(Veta2(1:Del+1+dil))) then
          continue
        else
          write(Veta,*)'File ',Veta2(1:Del+1+dil),' not found'
          NInfo=0
          call FeInfoOut(-1.,-1.,Veta,'L')
          fine=.false.
          exit
        endif
      end do

      end

      subroutine LoadAngles(FileName,nF,fine)
      use transport
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,FileName
      integer nF,Rd
      logical fine
      fine=.true.
      open(101,FileName)
      allocate(omeABS(1),theABS(1),kapABS(1),phiABS(1))
      read(101,'(/)')
      do i=1,nF
        read(101,fmt='(7x,4(4x,f10.5))',iostat=Rd)omeABS(i),theABS(i)
     1     ,kapABS(i),phiABS(i)
        if (Rd.ne.0) then
          fine=.false.
          exit
        end if
      end do

      close(101)
      end

      subroutine VeLoadUB(FileName,LoadedUB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,FileName,Radka
      integer InvFile,FoundData
      real LoadedUB(3,3),backup(3,3)
      FoundData=0
      backup=LoadedUB
      open(101,FileName,status='old',iostat=InvFile)
      if (InvFile.ne.0) then
        Veta='Invalid file'
        call FeInfoOut(-1.,-1.,Veta,'L')
      else
        do while(.true.)
          read(101,'(80a)',end=9998)Radka
          if(Radka(1:1).eq.'_') then
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_11') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(1,1)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_12') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(1,2)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_13') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(1,3)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_21') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(2,1)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_22') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(2,2)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_23') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(2,3)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_31') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(3,1)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_32') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(3,2)
              FoundData=FoundData+1
            endif
            if(Radka(1:27).eq.'_diffrn_orient_matrix_UB_33') then
              write(Veta,*)Radka(28:len(trim(Radka)))
              read(Veta,*)LoadedUB(3,3)
              FoundData=FoundData+1
            endif
          endif
        end do
      end if
9998  if (FoundData.ne.9) then
        Veta='The file is missing UB data'
        call FeInfoOut(-1.,-1.,Veta,'L')
        LoadedUB=backup
      end if
      close(101)
9999  return
      end

      subroutine VeLoadJPR(FileName,nFrames,InvFile)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,FileName
      integer nFrames,InvFile
      open(101,FileName,status='old',iostat=InvFile)
      if (InvFile.ne.0) then
        Veta='Invalid file'
        call FeInfoOut(-1.,-1.,Veta,'L')
      else
        read(101,'(40x,i6,/)')nFrames
      end if
      close(101)
9999  return
      end

      subroutine FeLoadBMPFileABS(FileName,Y,Z)
      use transport
      character*(*) FileName
      character*256 t256
      character*80  Veta
      CHARACTER*15  Cislo
      character*1   PaletteColors(:,:),Bitmap(:,:)
      character*2   Label
      character*1   Radka(:),String(256)
      CHARACTER*5   formI15
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,
     1        BinStToInt,
     2        FileSize,DataOffSet,HeaderSize,DataSize,Width,Height,
     3        Planes,Colors,WiYdthBytes,Y,Z
      logical EqIgCase
      allocatable PaletteColors,Bitmap,Radka
      DLL_import DllLoadBMPFile,DllSelectObject
      equivalence (t256,Label,String)
      formI15='(i15)'
      ich=0
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      if(ln.le.0) go to 9000
      n=FeReadBinaryFile(ln,String,14)
      k=1
      if(.not.EqIgCase(Label,'BM')) go to 9100
      k=k+2
      FileSize=BinStToInt(t256,k,4)
      k=k+4
      DataOffSet=BinStToInt(t256,k,4)
      n=FeReadBinaryFile(ln,String,40)
      k=1
      HeaderSize=BinStToInt(t256,k,4)
      if(HeaderSize.ne.40) go to 9200
      Width=BinStToInt(t256,k,4)
      Height=BinStToInt(t256,k,4)
      Planes=BinStToInt(t256,k,2)
      if(Planes.ne.1) go to 9300
      Colors=BinStToInt(t256,k,2)
      if(Colors.ne.1.and.Colors.ne.4.and.Colors.ne.8.and.
     1   Colors.ne.24.and.Colors.ne.32) go to 9400
      ii=BinStToInt(t256,k,4)
      if(ii.ne.0) go to 9500
      DataSize=BinStToInt(t256,k,4)
      k=k+8
      NPaletteColors=BinStToInt(t256,k,4)
      if(NPaletteColors.ne.2**Colors.and.
     1   (NPaletteColors.ne.0.and.Colors.eq.24)) go to 9600
      if(NPaletteColors.gt.0) then
        allocate(PaletteColors(4,NPaletteColors))
        do i=1,NPaletteColors
          n=FeReadBinaryFile(ln,PaletteColors(1,i),4)
        enddo
      endif
      if(Height.lt.0) go to 9700
      if(Colors.eq.32) then
        idl=4*Width
        kk=4
        WidthBytes=4*Width
      else if(Colors.eq.24) then
        idl=3*Width
        kk=3
        WidthBytes=3*Width
      else
        idl=Width*2**Colors
        idl=(idl-1)/256+1
        WidthBytes=4*Width
      endif
       if(NPaletteColors.gt.0) then
        WidthBytes=3*Width
      else
        WidthBytes=4*Width
      endif
      i=mod(idl,4)
      if(i.ne.0) idl=idl+4-i
      i=mod(WidthBytes,4)
      if(i.ne.0) WidthBytes=WidthBytes+4-i
      allocate(Radka(idl))
      ALLOCATE(Bitmap(WidthBytes,Height),iBitmapABS(Width*5,Height))
!      call SetStringArrayTo(Bitmap,WidthBytes*Height,char(0))
      Bitmap=char(0)
      do i=Height,1,-1
        n=FeReadBinaryFile(ln,Radka,idl)
        jjj=((i-1)*Widthbytes)-1
        if(NPaletteColors.gt.0) then
          j=0
          jj=0
          do l=1,WidthBytes
            k=1
            m=BinStToInt(Radka(l),k,1)+1
            do k=1,3
              jj=jj+1
              Bitmap(jj,i)=PaletteColors(k,m)
            enddo
            jj=jj+1
            Bitmap(jj,i)=char(0)
          enddo
        else
          l=0
          jj=0
          do j=1,Width
            do k=1,4
              jj=jj+1
              if(k.le.kk) then
                l=l+1
                Bitmap(jj,i)=Radka(l)
!                iBitmap(jj,i)=ICHAR(radka(l))
              else
                Bitmap(jj,i)=char(0)
!                iBitmap(jj,i)=jjj/4
              endif
            enddo
          enddo
          l=0
          jj=0
          do j=1,Width
            do k=1,4
              jj=jj+1
              jjj=jjj+1
              if(k.le.kk) then
                l=l+1
!                Bitmap(jj,i)=Radka(l)
                iBitmapABS(jj,i)=ICHAR(radka(l))
              else
!                Bitmap(jj,i)=char(0)
                iBitmapABS(jj,i)=MOD(jjj/4,Width)+1
                jj=jj+1
                iBitmapABS(jj,i)=(jjj/4-MOD(jjj/4,width))/width+1
              endif
            enddo
          enddo
        endif
      enddo
      Y=width
      Z=height
!      WRITE(41,'(4a1)') Bitmap
!      WRITE(6,'(5i4)') iBitmapABS(1:5,1)

!      call DllLoadBMPFile(carg(hDCComp),carg(hDC),carg(offset(Bitmap)),
!     1                    carg(WidthBytes),carg(Width),carg(Height),
!     2                    carg(ix1),carg(iy1))
      go to 9999
9000  t256='The file could not be opened.'
      go to 9900
9100  t256='Unknown label bitmap : "'//Label//'"'
      go to 9900
9200  write(Cislo,FormI15) HeaderSize
      call Zhusti(Cislo)
      t256='The header size of '//Cislo(:idel(Cislo))//
     1     ' bytes not implemeted in the program.'
      go to 9900
9300  write(Cislo,FormI15) Planes
      call Zhusti(Cislo)
      t256='The number of '//Cislo(:idel(Cislo))//
     1     ' color planes not implemeted in the program.'
      go to 9900
9400  write(Cislo,FormI15) Colors
      call Zhusti(Cislo)
      t256='The number of '//Cislo(:idel(Cislo))//
     1     ' colors in bytes not implemeted in the program.'
      go to 9900
9500  t256='Reading of compressed BMP files is n''t implemeted in the'//
     1     ' program.'
      go to 9900
9600  write(t256,FormI15) NPaletteColors
      call Zhusti(t256)
      write(Cislo,FormI15) Colors
      call Zhusti(Cislo)
      t256='The number of '//t256(:idel(t256))//
     1     ' palette colors doesn''t correspond to the number of '//
     2     Cislo(:idel(Cislo))//' colors in bytes.'
      go to 9900
9700  t256='Reading of BMP files arranged "bottom-up" isn''t '//
     1     'implemeted in the program.'
      go to 9900
9800  t256='Display has neither 32 nor 24 bitspixel.'
      go to 9900
9900  Veta='during import of the bitmap "'//FileName(:idel(FileName))//
     1     '"'
      ich=1
      write(6,*)t256
9999  if(ln.ne.0) i=FeCloseBinaryFile(ln)
      if(allocated(PaletteColors)) deallocate(PaletteColors)
      if(allocated(Radka)) deallocate(Radka,Bitmap)
      return
      end


      module prejmenovani
      character*8 AtomLabel(1000),AtomLabelNew(1000)
      logical EscPress
      end module prejmenovani

      subroutine DruhaAbsCorr
      use prejmenovani
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,FileNameCif,Backup,EdwStringQuest
      character*80  Radka,Label
      logical       Loop,Cleny
      integer       Clenu,Counter,LabelSite,Atom
      EscPress=.false.
      FileNameCif=''
      ich=0
      id=NextQuestId()
      xqd=400.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Dodatecne precislovani',
     1                   0,LightGray,0,0)
      il=1
      tpom=xqd*.5
      Veta='Which .cif file do you want to renumber?'
      call FeQuestLblMake(id,tpom,il,Veta,'C','N')
      il=il+1
      Veta='F%ile:'
      tpom=10.+FeTxLengthUnder(Veta)
      xpom=tpom+FeTxLengthUnder(Veta)+11.
      dpom=200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileCif=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNameCif)
      tpom=xpom+dpom+15.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtBrowseCif=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)

1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowseCif)
     1  then
        backup=FileNameCif
        FileNameCif=EdwStringQuest(nEdwFileCif)
        call FeFileManager('Select .cif file for renumbering',
     1                     FileNameCif,'*.cif',0,.true.,ichp)
        if(ichp.eq.0) then
          i=len(trim(FileNameCif))
          if(FileNameCif(i-3:i).eq.'.cif') then
            call FeQuestStringEdwOpen(nEdwFileCif,FileNameCif)
          else
            Veta='Unknown file type'
            call FeInfoOut(-1.,-1.,Veta,'L')
            FileNameCif=backup
          end if
        end if
        go to 1500

      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)

        FileNameCif=EdwStringQuest(nEdwFileCif)
        open(101,FileNameCif,status='old',iostat=InvFile)
        if (InvFile.ne.0) then
          Veta='Invalid file'
          call FeInfoOut(-1.,-1.,Veta,'L')
          go to 1500
        else
          Loop=.false.
          Cleny=.false.
          Counter=0
          Clenu=0
          AtomLabel=' '
          Atom=0
          do while(.true.)
            read(101,'(80a)',end=2000) Radka
            if (Radka(1:5).eq.'loop_') then
              Loop=.true.
              Cleny=.true.
            end if
            if ((Loop.eqv..true.).and.(Radka(1:11).eq.' _atom_site')
     1         .and.(Radka(13:17).ne.'aniso')) then
              Counter=Counter+1
              Cleny=.true.
            else
              Cleny=.false.
            end if
            if ((Loop.eqv..true.).and.(Radka(1:17).eq.
     1       ' _atom_site_label')) then
              LabelSite=Counter
            end if
            if ((Loop.eqv..true.).and.(Cleny.eqv..false.).and.
     1         ((Radka(1:1).eq.'_').or.(Radka.eq.' '))) then
              Loop=.false.
              if (Clenu.eq.0) Clenu=Counter
              Counter=0
            end if
            if ((Loop.eqv..true.).and.(Cleny.eqv..false.).and.
     1         (Counter.ne.0)) then
              call NajdiSlovo(Radka,LabelSite,Label)
              Atom=Atom+1
              AtomLabel(Atom)=Label(1:len(trim(Label)))
            end if
          end do
        end if
2000    close(101)
      endif
      if(ich.ne.0) then
        call FeQuestRemove(id)
        go to 9999
      endif

      call FeQuestRemove(id)
      call VeRenameManually(ich,Atom)
      if (EscPress.eqv..true.) go to 9999
      call VeVypisCif(ich,Atom,FileNameCif)

9999  return
      end

      subroutine VeVypisCif(ich,nAtom,OrigCifName)
      use prejmenovani
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest,FileNameCifOut,Veta,OrigCifName
      character*80 t80,Radka,Word,RadkaSl,backup
      integer nAtom,idel,WordStart,WordEnd,SlStart,SlEnd
      logical ExistFile,FeYesNoHeader,Prepis

      id=NextQuestId()
      i=idel(OrigCifName)
      FileNameCifOut=' '
      FileNameCifOut(1:i-4)=OrigCifName(1:i-4)
      FileNameCifOut(i-3:i+6)='_renum.cif'
      xqd=400.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Output specification',
     1                   0,LightGray,0,0)
      il=1
      tpom=xqd*.5
      Veta='Which .cif file do you want to create?'
      call FeQuestLblMake(id,tpom,il,Veta,'C','N')
      il=il+1
      Veta='F%ile:'
      tpom=10.+FeTxLengthUnder(Veta)
      xpom=tpom+FeTxLengthUnder(Veta)+11.
      dpom=300.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileCifOut=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNameCifOut)
      tpom=xpom+dpom+15.

1500  call FeQuestEvent(id,ich)

      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      end if
      if(ich.eq.0) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        FileNameCifOut=EdwStringQuest(nEdwFileCifOut)
        open(101,OrigCifName,status='old',err=1550)
        if (ExistFile(FileNameCifOut)) then
          NInfo=0
          if(.not.FeYesNoHeader(-1.,-1.,
     1      'Specified file exists,do you want to overwrite it?',
     2      0)) then
            go to 1500
          else
            open(102,FileNameCifOut,status='replace',err=1550)
            go to 1600
          end if
        else
          open(102,FileNameCifOut,status='new',err=1550)
          go to 1600
        end if
1550    write(Veta,*)'Something went terribly wrong...'
        NInfo=0
        call FeInfoOut(-1.,-1.,Veta,'L')
        go to 9998
1600    Prepis=.false.
        do while(.true.)
          read(101,'(80a)',end=2000) Radka
          if (Radka(1:11).eq.' _atom_site') Prepis=.true.
          if (Radka(1:15).eq.' _refln_index_h') Prepis=.false.
          if (Prepis.eqv..true.) then
            RadkaSl=' '
            Word=' '
            WordStart=0
            WordEnd=0
            SlStart=0
            SlEnd=0
            do i=1,80
              backup=' '
              if (Radka(i:i).ne.' ') then
                if ((WordStart.eq.0).or.(Radka(i-1:i-1).eq.' ')) then
                  WordStart=i
                else
                  continue
                end if
              end if
              if (Radka(i:i).eq.' ') then
                if (WordStart.le.WordEnd) then
                  continue
                else
                  WordEnd=i-1
                end if
              end if
              backup=word(1:idel(Word))
              Word=Radka(WordStart:WordEnd)
              do j=1,nAtom
                if (Word(1:idel(Word)).eq.
     1             (AtomLabel(j)(1:idel(AtomLabel(j))))) then
                  k=idel(AtomLabelNew(j))
                  write(Word,'(a)')AtomLabelNew(j)(1:k)
                end if
              end do
              if ((backup(1:idel(backup)).ne.Word(1:idel(Word)))
     1           .and.(WordStart.ne.0).and.(WordEnd.ne.80)) then
                if (SlStart.eq.SlEnd) SlEnd=SlEnd+1
                SlStart=SlEnd
                SlEnd=SlStart+Idel(Word)
                RadkaSl(SlStart:SlEnd)=Word(1:idel(Word))
              endif
            end do
            write(102,'(a)') RadkaSl(1:idel(RadkaSl))
          else
            write(102,'(a)') Radka(1:idel(Radka))
          end if
        end do
      end if
2000  if(ich.ne.0) then

      endif
9998  call FeQuestRemove(id)
      close(101)
      close(102)
9999  return


      end

      subroutine NajdiSlovo(Line,Order,Word)
      character*80 Line,Word
      integer Order,Found,WordStart,WordEnd
      Found=0
      WordStart=0
      WordEnd=80
      do i=1,80
        if (Line(i:i).ne.' ') then
          if ((WordStart.eq.0).or.(Line(i-1:i-1).eq.' ')) then
            WordStart=i
          else
            continue
          end if
        end if
        if (Line(i:i).eq.' ') then
          if (WordStart.eq.0) then
            continue
          else
            Found=Found+1
            WordEnd=i-1
          end if
        end if
        if (Found.eq.Order) exit
      end do
      Word=Line(WordStart:WordEnd)
      end

      subroutine VeRenameManually(ich,nAtom)
      use prejmenovani
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      integer IAt(:),nAtom
      logical EqIgCase
      allocatable IAt
      allocate(IAt(nAtom))
      ich=0
      na=0
      do i=1,nAtom
        na=na+1
        AtomLabelNew(na)=AtomLabel(i)
        IAt(na)=i
      enddo
      id=NextQuestId()
      xqd=550.
      if(na.gt.45) then
        il=16
      else
        il=15
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Edit individual atom names',
     1                   0,LightGray,0,0)
      tpom=5.
      xpom=80.
      dpom=80.
      il=0
      do i=1,min(na,45)
        il=il+1
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        call FeQuestEdwMake(id,xpom-5.,il,xpom,il,' ','R',dpom,EdwYd,
     1                      1)
        if(mod(i,15).eq.0) then
          call FeQuestSvisliceFromToMake(id,xpom+dpom+15.,1,15,0)
          il=0
          tpom=tpom+180.
          xpom=xpom+180.
        endif
        if(i.eq.1) then
          nLblFr=LblLastMade
          nEdwFr=EdwLastMade
        endif
      enddo
      if(na.gt.0) then
        il=-165
        tpom=15.
        dpom=60.
        Cislo='Previous'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtPrevious=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        tpom=xqd-dpom-15.
        Cislo='Next'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtPrevious=0
        nButtNext=0
      endif
      NFrom=1
      NTo=min(na,45)
1400  nLbl=nLblFr
      nEdw=nEdwFr
      do i=NFrom,NTo
        write(Cislo,'(''#'',i5)') i
        call Zhusti(Cislo)
        call FeQuestLblChange(nLbl,Cislo)
        Cislo=AtomLabel(IAt(i))
        Cislo=Cislo(:idel(Cislo))//'=>'
        call FeQuestEdwLabelChange(nEdw,Cislo)
        call FeQuestStringEdwOpen(nEdw,AtomLabelNew(i))
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      do i=NTo+1,NFrom+44
        call FeQuestLblOff(nLbl)
        call FeQuestEdwClose(nEdw)
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      if(nButtPrevious.gt.0) then
        if(NFrom.le.1) then
          call FeQuestButtonDisable(nButtPrevious)
        else
          call FeQuestButtonOff(nButtPrevious)
        endif
        if(NTo.ge.na) then
          call FeQuestButtonDisable(nButtNext)
        else
          call FeQuestButtonOff(nButtNext)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtPrevious) then
          NFrom=max(NFrom-45,1)
          NTo=min(NFrom+44,na)
        else
          NFrom=max(NFrom+45,1)
          NTo=min(NFrom+44,na)
        endif
        go to 1400
      else if(CheckType.eq.EventEdw) then
        Cislo=EdwStringQuest(CheckNumber)
        call AtCheck(Cislo,ichp,i)
        if(ichp.eq.1) then
          call FeChybne(-1.,-1.,'unacceptable symbol in atom string '//
     1                  'try again.',' ',SeriousError)
          go to 1500
        endif
        AtomLabelNew(nFrom+CheckNumber-nEdwFr)=Cislo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        j=0
        do i=1,nAtom
          j=j+1
          ia=i
          call CrlAtomNamesSave(AtomLabel(ia),AtomLabelNew(j),1)
        enddo
        do i=1,nAtom
          do j=i+1,nAtom
            if(EqIgCase(AtomLabelNew(i),AtomLabelNew(j))) then
              call VECorrectAtomNames(ich,nAtom)
              go to 9000
            end if
          enddo
        enddo
      else
        EscPress=.true.
      endif
9000  call FeQuestRemove(id)
      if(allocated(IAt)) deallocate(IAt)
      return
      end

      subroutine VeCorrectAtomNames(ich,nAtom)
      use prejmenovani
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 t80
      character*8 At
      character*2 nty
      integer nAtom
      logical EqIgCase
      namax=nAtom
      xqd=500.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,11,'Atom names '//
     1                   ' are not unique, please make corrections:',1,
     2                   LightGray,0,0)
      iap=1
      iak=min(nAtom,30)
      dpom=FeTxLength('XXXXXXXXX')+10.
      if(namax.gt.30) then
        xpom=xqd-dpom-15.
        il=1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
        nButtNext=ButtonLastMade
        il=10
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
        nButtPrevious=ButtonLastMade
      endif
      tpom=45.
      xpom=50.
      il=0
      dpom=dpom+2.*EdwMarginSize
      do i=1,30
        write(t80,100) i,nty(i)
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'R',dpom,EdwYd,1)
        if(i.eq.1) nEdwAtomFirst=EdwLastMade
        if(il.eq.10) then
          il=0
          xpom=xpom+55.+dpom
          tpom=tpom+55.+dpom
        endif
      enddo
      il=11
1220  nEdw=nEdwAtomFirst
      do i=iap,iap+29
        if(i.le.NAtInd) then
          j=i
        else
          j=NAtMolFr(1,1)-1+i-NAtInd
        endif
        if(i.le.namax) then
          call FeQuestStringEdwOpen(nEdw,AtomLabelNew(j))
        else
          call FeQuestEdwClose(nEdw)
        endif
        nEdw=nEdw+1
      enddo
1250  if(namax.gt.30) then
        if(iak.lt.namax) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(iap.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      endif
      do j=iap,iak
        if(j.le.nAtom) then
          k=j
        else
          k=j-nAtom
        endif
        At=AtomLabelNew(k)
        write(t80,100) j,nty(j)
        do i=1,j-1
          if(i.le.nAtom) then
            l=i
          else
            l=i-nAtom
          endif
          if(EqIgCase(At,AtomLabelNew(l))) then
            do k=1,idel(t80)
              if(t80(k:k).ne.' ') then
                t80='**'//t80(k:idel(t80))
                go to 1305
              endif
            enddo
            go to 1305
          endif
        enddo
1305    call FeQuestEdwLabelChange(j+nEdwAtomFirst-iap,t80)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do j=1,namax
          if(j.le.nAtom) then
            k=j
          else
            k=j-nAtom
          endif
          At=AtomLabelNew(k)
          do i=1,j-1
            if(i.le.nAtom) then
              l=i
            else
              l=i-nAtom
            endif
            if(EqIgCase(At,AtomLabelNew(l))) then
              call FeChybne(-1.,-1.,'problem was not yet fully solved.',
     1                      ' ',SeriousError)
              go to 1500
            endif
          enddo
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventEdw) then
        i=CheckNumber-nEdwAtomFirst+iap
        if(i.le.nAtom) then
          k=i
        else
          k=i-nAtom
        endif
        At=EdwStringQuest(CheckNumber)
        call UprAt(At)
        call FeQuestStringEdwOpen(CheckNumber,At)
        if(EqIgCase(At,AtomLabelNew(k))) then
          go to 1500
        else
          AtomLabelNew(k)=At
          go to 1250
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     2         CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          iap=iap+30
        else
          iap=iap-30
        endif
        iap=max(iap,1)
        iap=min(iap,namax)
        iak=min(iap+29,namax)
        go to 1220
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.eq.0) then
        continue
      else
        EscPress=.true.
      endif
      return
100   format(i5,a2)
      end
