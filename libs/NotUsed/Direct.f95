      subroutine DirectMethods
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call DMPrelim
      if(ErrFlag.ne.0) go to 9999
      call DMWilson
      call DMECalc
      if(ErrFlag.ne.0) go to 9999
      if(allocated(fo)) deallocate(fo,rho,ids,ew,ipr,hkl)
      call DMFirst
      if(ErrFlag.ne.0) go to 9999
      call CloseListing
      LstOpened=.false.
      call FeShowListing(-1.,YBottomMessage,'Statistics program',
     1                   fln(:ifln)//'.dir',0)
9999  if(allocated(fo)) deallocate(fo,rho,ids,ew,ipr,hkl)
      if(allocated(ex)) deallocate(ex,ix1,ix2,NSig,NPsi)
      if(allocated(Ph1)) deallocate(Ph1,Ph2,EEE)
      if(allocated(ir1)) deallocate(ir1,loc)
      return
      end
      subroutine DMPrelim
      use Basic_mod
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      integer hklp,CrlCentroSymm
      logical eqiv,CentroSymm
      line=mxline
      call DMDefault
      open(lst,file=fln(1:ifln)//'.dir')
      LstOpened=.true.
      page=0
      uloha='Program for the direct methods'
      call DMNacti
      call iom50(0,1,fln(:ifln)//'.m50')
      fn=0.
      SigN2=0.
      SigP2=0.
      SigQ3=0.
      NHeavy=0
      do i=1,NAtFormula(KPhase)
        if(AtNum(i,KPhase).gt.1.)  fn=fn+AtMult(i,KPhase)
        if(AtNum(i,KPhase).ge.10.) NHeavy=1
        pom=AtNum(i,KPhase)**2*AtMult(i,KPhase)*
     1      float(NUnits(NPhase))/float(NLattVec(KPhase))
        SigN2=SigN2+pom
        SigQ3=SigQ3+pom*AtNum(i,KPhase)
      enddo
      fn=fn*float(NUnits(NPhase))/
     1      float(NLattVec(KPhase)*NSymmN(KPhase))
      if(NumB.le.0) then
        NumB=nint(4.*fn+100.)
        if(NSymmN(KPhase).le.2) then
          NumB=NumB+100
        else if(NSymmN(KPhase).le.4) then
          NumB=NumB+50
        endif
        NumB=NumB+(NumB+5)/10
      endif
      if(NDet.gt.NumB) NDet=NumB
      if(NDet.lt.0) NDet=(10*NumB+5)/11
      if(NZero.lt.0) NZero=NDet/2
      call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
      n=2*NRef90(KDatBlock)
      call FeFlowChartOpen(-1.,-1.,max0(nint(float(n)*.005),10),n,
     1                     'Reading of the reflection file',' ',' ')
      no=0
      call SetIntArrayTo(mxh,6,0)
2000  read(91,format91,end=2150,err=9000)(ih(i),i=1,NDim(KPhase)),f
      if(ih(1).gt.900) go to 2150
      call FeFlowChartEvent(no,ie)
      do i=4,NDim(KPhase)
       if(ih(i).ne.0) go to 2000
      enddo
      do i=1,NSymmN(KPhase)
        call MultmIRI(ih,rm6(1,i,1,KPhase),ihp,1,NDim(KPhase),
     1                NDim(KPhase))
        do j=1,NDim(KPhase)
          mxh(j)=max0(mxh(j),iabs(ihp(j)))
        enddo
      enddo
      go to 2000
2150  call CloseIfOpened(91)
      call OpenDatBlockM90(91,KDatBlock,fln(:ifln)//'.m90')
      mxd(6)=1
      do i=5,1,-1
        mxd(i)=mxd(i+1)*(2*mxh(i+1)+1)
        if(imax/mxd(i).lt.2*mxh(i)+1) go to 8000
      enddo
      if(allocated(fo)) deallocate(fo,rho,ids,ew,ipr,hkl)
      allocate(fo(no),rho(no),ids(no),ew(no),ipr(no),hkl(no))
      nref=0
      RhoMinActual=1.
      RhoMaxActual=0.
      CentroSymm=CrlCentroSymm().gt.0
      CrSystemPom=mod(CrSystem(KPhase),10)
      if(CentroSymm) then
        jk=1
      else
        jk=2
      endif
2200  read(91,format91,end=2500,err=9000)(ih(i),i=1,NDim(KPhase)),f
      if(ih(1).gt.900) go to 2500
      call FeFlowChartEvent(no,ie)
      do i=4,NDim(KPhase)
        if(ih(i).ne.0) go to 2200
      enddo
      call FromIndSinthl(ih,h,sinthl,rhop,1,0)
      if(rhop.lt.RhoMin.or.rhop.gt.RhoMax) go to 2200
      RhoMinActual=min(RhoMinActual,rhop)
      RhoMaxActual=max(RhoMaxActual,rhop)
      if(f.gt.0.) then
        f=sqrt(f)
      else
        f=0.
      endif
      hklp=0
      ieps=0
      mult=0
      mkang=0
      do i=1,NSymmN(KPhase)
        call MultmIRI(ih,rm6(1,i,1,KPhase),ihp,1,NDim(KPhase),
     1                NDim(KPhase))
        t=0.
        do j=1,3
          t=t+float(ih(j))*s6(j,i,1,KPhase)
        enddo
        t=t-ifix(t)
        if(t.lt.0.) t=t+1.
        do j=1,jk
          call PackHKL(l,ihp,mxh,mxd,3)
          hklp=max0(hklp,l)
          if(eqiv(ih,ihp,3)) then
            mult=mult+1
            if(j.eq.1) then
              ieps=ieps+1
            else
              mkang=nint(12.*t)
              if(mkang.eq.0) mkang=12
            endif
          endif
          call IntVectorToOpposite(ihp,ihp,NDim(KPhase))
        enddo
      enddo
      nref=nref+1
      hkl(nref)=hklp
      fo(nref)=f
      rho(nref)=rhop
      if(CrSystemPom.eq. CrSystemCubic.or.
     1   CrSystemPom.eq.-CrSystemTrigonal) then
        igp=3*mod(iabs(hg(1)+hg(2)+hg(3)),2)
        if(mod(iabs(ih(1)-ih(3)),3).eq.0) igp=igp+1
        if(mod(iabs(ih(2)-ih(3)),3).eq.0.or.
     1     mod(iabs(ih(1)-ih(2)),3).eq.0) igp=igp+1
      else
        hg(3)=mod(iabs(ih(3)),2)
        if(CrSystemPom.lt.5) then
          hg(1)=mod(iabs(ih(1)),2)
          hg(2)=mod(iabs(ih(2)),2)
          if(CrSystem(KPhase).le.CrSystemTetragonal) then
            igp=hg(1)+2*hg(2)+4*hg(3)
          else
            igp=hg(1)+2*hg(2)+3*hg(3)
          endif
        else
          igp=3*hg(3)
          if(mod(iabs(ih(1)),3).eq.0) igp=igp+1
          if(mod(iabs(ih(2)),3).eq.0.or.
     1       mod(iabs(ih(1)+ih(2)),3).eq.0) igp=igp+1
        endif
      endif
      if(CrlCentroSymm().ne.0) mkang=12
      ids(NRef)=1024*(NSymmN(KPhase)/mult)+128*(ieps-1)+8*mkang+igp
      go to 2200
2500  call indexx(nref,hkl,ipr)
      kk=0
2510  kp=kk+1
      if(kp.le.NRef) then
        i=ipr(kp)
        hklp=hkl(i)
        FoSum=Fo(i)
        NSum=1
        kk=kp
2520    kk=kk+1
        if(kk.le.NRef) then
          j=ipr(kk)
          if(hkl(j).eq.hklp) then
            FoSum=FoSum+Fo(j)
            NSum=NSum+1
            ids(j)=-1
            go to 2520
          else
            kk=kk-1
            Fo(i)=FoSum/float(NSum)
            go to 2510
          endif
        endif
      endif
      do 2550i=1,NRef
        if(ids(i).lt.0) go to 2550
        k=ids(i)
        mult=k/1024
        k=mod(k,1024)
        ieps=k/128+1
        k=mod(k,128)
        mkang=k/8
        igp=mod(k,8)
        ew(i)=0.
        call SetFormF(sqrt(rho(i)))
        do j=1,NAtFormula(KPhase)
          ew(i)=ew(i)+AtMult(j,KPhase)*fx(j)**2
        enddo
        ew(i)=ew(i)*float(NUnits(KPhase))
2550  continue
      go to 9999
8000  call FeChybne(-1.,YBottomMessage,
     1              'diffraction indices are too large to be'//
     1              ' packed in one word.',' ',SeriousError)
      go to 9999
9000  call FeReadError(91)
      ErrFlag=1
9999  call FeFlowChartRemove
      return
100   format(a80)
      end
      subroutine DMDefault
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      RhoMin=0.
      RhoMax=.49
      NumB=-1
      NZero=-1
      NDet=-1
      ListE=0
      KMin=60
      return
      end
      subroutine DMNacti
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      character*8 nazev,id(30)
      character*80 radka
      character*85 ven
      dimension idn(30)
      data id/'nref','nzero','ndeter',7*'nic',
     1        'snlmn','snlmx',8*'nic',
     2        10*'nic'/
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call Najdi('direct',i)
      if(i.ne.1) go to 9999
      do i=1,30
        idn(i)=0
      enddo
      k=80
      call newln(1)
      write(lst,'('' The following lines were read as a control data :
     1      '')')
500   if(k.eq.80) then
501     read(m50,100) radka
        call mala(radka)
        ven=' =>'
        j=idel(radka)
        do i=1,j
          ven(i+3:i+3)=radka(i:i)
        enddo
        ven(j+4:j+5)='<='
        call newln(1)
        write(lst,'(a85)') ven
        if(radka(1:1).eq.'*') go to 501
        call vykric(radka)
        if(idel(radka).le.0) go to 501
        k=0
      endif
      call kus(radka,k,nazev)
      if(nazev.eq.'end') go to 9999
      i=islovo(nazev,id,29)
      if(i.eq.0) then
        call FeChybne(-1.,-1.,'unknown keyword : '//nazev,' ',Warning)
      else if(i.lt.0) then
        call FeChybne(-1.,-1.,'keyword : '//nazev//' isn''t unique',' ',
     1                Warning)
      else if(idn(i).gt.0) then
        call FeChybne(-1.,-1.,'duplicate occurence of "'//
     1                nazev(1:idel(nazev))//
     2                '"','remember that the first occurence will be '//
     3                'accepted',Warning)
        call kus(radka,k,nazev)
      else
        go to 511
      endif
      go to 500
511   if(idn(i).ge.0) idn(i)=idn(i)+1
      if(i.gt.20) go to 500
      call kus(radka,k,cislo)
      j=0
      if(i.gt.10) j=1
      call posun(cislo,j)
      if(j.eq.0) go to 2000
      read(cislo,101) j
      go to (1000,1010,1020),i
1000  numb=j
      go to 500
1010  nzero=j
      go to 500
1020  ndet=j
      go to 500
2000  read(cislo,102) pom
      go to (2010,2020), i-10
2010  RhoMin=pom**2
      go to 500
2020  RhoMax=pom**2
      go to 500
9999  call CloseIfOpened(m50)
100   format(a80)
101   format(i15)
102   format(f15.5)
      end
      subroutine DMWilson
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      character*80 Veta,SvFile
      dimension sw(70),sr(70),si(70),sn(70),ra(70),xp(50),yp(50),zp(50)
      equivalence (sw(2),pa)
      SvFile='jdrt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile)
      nb=2*ifix(3.25725*alog(0.05*float(NRef)))+4
      rr=float(nb-3)/(RhoMaxActual-RhoMinActual)
      do i=1,nb+1
        sw(i)=0.
        sr(i)=0.
        si(i)=0.
        sn(i)=0.
        ra(i)=0.
      enddo
      do 1040i=1,NRef
        if(ids(i).lt.0) go to 1040
        n=max(nint(rr*(rho(i)-RhoMinActual)),0)+4
        tmul=ids(i)/1024
        eps=mod(ids(i),1024)/128+1
        sn(n)=sn(n)+tmul
        sw(n)=sw(n)+tmul*ew(i)
        sr(n)=sr(n)+tmul*rho(i)
        si(n)=si(n)+tmul*fo(i)**2/eps
1040  continue
      smr=0.
      smw=0.
      smww=0.
      smrr=0.
      smrw=0.
      yomn= 99999.
      yomx=-99999.
      xomn= 0.
      xomx=-99999.
      do 1080i=4,nb
        ra(i-1)=float(i-3)/rr+RhoMinActual
        sn(i-1)=sn(i-1)+2.*sn(i)+sn(i+1)
        if(sn(i-1).lt..5) go to 1080
        sr(i-1)=(sr(i-1)+2.*sr(i)+sr(i+1))/sn(i-1)
        xomn=amin1(xomn,sr(i-1))
        xomx=amax1(xomx,sr(i-1))
        si(i-1)=(si(i-1)+2.*si(i)+si(i+1))/sn(i-1)
        sw(i-1)=alog(si(i-1)*sn(i-1)/(sw(i-1)+2.*sw(i)+sw(i+1)))
        yomn=amin1(yomn,sw(i-1))
        yomx=amax1(yomx,sw(i-1))
        smr=smr+sr(i-1)
        smw=smw+sw(i-1)
        smrr=smrr+sr(i-1)**2
        smww=smww+sw(i-1)**2
        smrw=smrw+sw(i-1)*sr(i-1)
1080  continue
      ra(nb)=RhoMaxActual
      nb=nb-1
      call TitulekVRamecku('Wilson plot')
      call newln(3)
      write(lst,'(2(10x,''range of'',15x,''mean'',21x)/
     1            2(8x,''sinth/lam**2    number   rho     mean I  '',
     2            ''Wilson   '')/)')
      do i=3,nb,4
        k=min0(i+2,nb)
        call newln(1)
        write(lst,'(2(i4,f8.3,'' -'',f6.3,i9,f8.3,f10.1,f8.3,3x))')
     1             (j-2,ra(j-2),ra(j+1),nint(sn(j)),sr(j),si(j),sw(j),
     2              j=i,k,2)
      enddo
      bn=nb-2
      pa=(smr*smrw-smrr*smw)/(smr**2-smrr*bn)
      sc(1,KDatBlock)=1./sqrt(exp(-pa))
      pb=(smr*smw-bn*smrw)/(smr**2-smrr*bn)
      OverAllB(KDatBlock)=-.5*pb
      call newln(2)
      write(lst,'(/''Scale factor : '',f10.6,20x,''Overall '',
     1             ''temperature factor : '',f10.6)')
     2            sc(1,KDatBlock),OverAllB(KDatBlock)
      sr(1)=-sr(3)
      sw(1)=pb*sr(1)+pa
      rr=sr(nb)/50.
      j=1
      q=rr
      q=0.
      do i=1,50
1110    if(sr(j+2).lt.q.and.j.lt.nb-4) then
          j=j+1
          go to 1110
        endif
        w=Finter4(sw(j),sr(j),q)
        xp(i)=q
        yp(i)=w
        zp(i)=pb*q+pa
        q=q+rr
      enddo
      call UnitMat(F2O,3)
      call FeMakeAcWin(50.,20.,30.,30.)
      xomn=xp(1)
      xomx=xp(50)
      pom=(yomx-yomn)*.2
      yomn=yomn-pom
      yomx=yomx+pom
      reconfig=.true.
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      write(Cislo,'(f10.6)') sc(1,KDatBlock)
      call Zhusti(Cislo)
      Veta='Wilson plot -> scale: '//Cislo(:idel(Cislo))
      write(Cislo,'(f10.6)') OverAllB(KDatBlock)
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//', overall temperature parameter: '//
     1     Cislo(:idel(Cislo))
      call FeOutSt(0,XCenAcWin,(YMaxAcWin+YMaxGrWin)*.5,Veta,'C',White)
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'rho')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'ln(I)')
      call FeXYPlot(xp,yp,50,NormalLine,NormalPlotMode,White)
      call FeXYPlot(xp,zp,50,NormalLine,NormalPlotMode,Green)
      call FeBottomInfo('To continue press any key or mouse button')
1500  call FeEvent(0)
      if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1   (EventType.eq.EventMouse.and.
     2    (EventNumber.eq.JeLeftDown.or.EventNumber.eq.JeRightDown)))
     3  then
      else
        go to 1500
      endif
      call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile,
     1                   0)
      return
      end
      subroutine DMECalc
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      dimension zz(18),cpa(18),cpc(18),cph(18),zt(18,5),
     1          stl(10),rhr(10),shr(10),vst(10,5),ast(5),anu(25),
     2          scal(9),ang(9),ava(9),avc(9),avh(9),ee(9)
      integer, allocatable :: iha(:,:)
      character*128 Veta
      character*11  ELabels(9)
      character*2 itext(10,2)
      data ava/0.886,1.0,1.329,2.0,3.323,6.0,0.736,1.0,2.0/
      data avc/0.798,1.0,1.596,3.0,6.383,15.0,0.968,2.0,8.0/
      data avh/0.718,1.0,1.916,4.5,12.26,37.5,1.145,3.5,26.0/
      data cph/0.368,0.463,0.526,0.574,0.612,0.643,0.670,0.694,0.733,
     1         0.765,0.791,0.813,0.848,0.875,0.896,0.913,0.926,0.938/
      data itext/'  ',' 0','kl','  ',' h','0l','  ',' h','k0','  ',
     1           'h,','k,','2k','-h',' h','hl','h,','k,','-h','-k'/
      data ELabels/'  mod(E)','   E**2','   E**3','   E**4','   E**5',
     1             '   E**6','mod(E**2-1)','(E**2-1)**2','(E**2-1)**3'/
      if(allocated(ex)) deallocate(ex,ix1,ix2,NSig,NPsi)
      NRLim=NumB+NZero+200
      allocate(ex(NRLim),ix1(NRLim),ix2(NRLim),NSig(NRLim),NPsi(NRLim))
      nc=0
      iz=0
      en=0.
      er=10.
      call SetRealArrayTo(Ang ,9,0.)
      call SetRealArrayTo(Scal,9,0.)
      do 1040i=1,NRef
        if(ids(i).lt.0) go to 1040
        tmul=ids(i)/1024
        eps=mod(ids(i),1024)/128+1
        ig=mod(ids(i),8)+1
        fo(i)=(fo(i)/sc(1,KDatBlock))**2/(eps*ew(i)*
     1         exp(-2.*OverAllB(KDatBlock)*rho(i)))
        scal(ig)=scal(ig)+fo(i)*tmul
        ang(ig)=ang(ig)+tmul
1040  continue
      do i=1,8
        scal(9)=scal(9)+scal(i)
        ang(9)=ang(9)+ang(i)
        if(ang(i).gt..5) scal(i)=scal(i)/ang(i)
      enddo
      scal(9)=scal(9)/ang(9)
      call SetRealArrayTo(rhr,10,0.)
      call SetRealArrayTo(shr,10,0.)
      call SetRealArrayTo(vst,50,0.)
      call SetRealArrayTo(ast,5,0.)
      call SetRealArrayTo(zt,90,0.)
      call SetRealArrayTo(anu,25,0.)
      do 1250i=1,NRef
        if(ids(i).lt.0) go to 1250
        tmul=ids(i)/1024
        eps=mod(ids(i),1024)/128+1
        mkang=mod(ids(i),128)/8
        ig=mod(ids(i),8)+1
        esq=fo(i)/scal(ig)
        e=sqrt(esq)
        ee(1)=e
        if(e.gt.en) go to 1100
        e=e+.2*rho(i)
        if(e.gt.er) go to 1110
1100    nc=nc+1
        ix1(nc)=hkl(i)
        ix2(nc)=ids(i)
        ex(nc)=e
        if(nc+iz.ge.NRLim) then
          call HeapRII(NRLim,ex,ix1,ix2,-1)
          nc=NumB
          iz=NZero
          en=ex(NumB)
          if(NZero.gt.0) er=ex(NRLim-NZero+1)
        endif
1110    n=nint(9.*sqrt(rho(i)/RhoMax))+1
        shr(n)=shr(n)+tmul
        rhr(n)=rhr(n)+esq*tmul
        nzr=int(10.0*esq)+1
        if(nzr.gt. 8) nzr=5+(nzr-1)/2
        if(nzr.gt.12) nzr=7+(nzr-1)/2
        do  j=2,6
          ee(j)=ee(j-1)*ee(1)
        enddo
        ee(7)=esq-1.0
        ee(8)=ee(7)*ee(7)
        ee(9)=ee(8)*ee(7)
        ee(7)=abs(ee(7))
        call UnpackHKL(hkl(i),ih,mxh,mxd,3)
        ind=0
        if(CrSystemPom.eq.CrSystemTriclinic.or.
     1     CrSystemPom.eq.CrSystemMonoclinic.or.
     2     CrSystemPom.eq.CrSystemOrthorhombic) then
          if(ih(1).eq.0) ind=ind+3
          if(ih(2).eq.0) ind=ind+4
          if(ih(3).eq.0) ind=ind+5
        else if(CrSystemPom.eq.CrSystemTetragonal) then
          if(ih(1).eq.0.or.ih(2).eq.0)   ind=ind+3
          if(iabs(ih(1)).eq.iabs(ih(2))) ind=ind+4
          if(ih(3).eq.0)                 ind=ind+5
        else if(CrSystemPom.eq.CrSystemTrigonal.or.
     1          CrSystemPom.eq.CrSystemHexagonal) then
          if(ih(1).eq.0.or.
     1       ih(2).eq.0.or.
     2       ih(1)+ih(2).eq.0) ind=ind+3
          if(ih(1).eq.ih(2).or.
     1       ih(1)+2*ih(2).eq.0.or.
     2       2*ih(1)+ih(2).eq.0) ind=ind+4
          if(ih(3).eq.0) ind=ind+5
        else if(CrSystemPom.eq.-CrSystemTrigonal) then
          if(ih(1).eq.ih(2).or.
     1       ih(1).eq.ih(3).or.
     2       ih(2).eq.ih(3)) ind=ind+3
          if(ih(3).eq.2*ih(2)-ih(1).or.
     1       ih(2).eq.2*ih(1)-ih(3).or.
     2       ih(1).eq.2*ih(3)-ih(2)) ind=ind+4
          if(ih(1)+ih(2)+ih(3).eq.0) ind=ind+5
        else
          if(ih(1).eq.0.or.ih(2).eq.0.or.ih(3).eq.0) ind=ind+3
          if(iabs(ih(1)).eq.iabs(ih(2)).or.
     1       iabs(ih(1)).eq.iabs(ih(3)).or.
     2       iabs(ih(2)).eq.iabs(ih(3))) ind=ind+4
          if(iabs(ih(3)).eq.iabs(ih(1)+ih(2)).or.
     1       iabs(ih(2)).eq.iabs(ih(1)+ih(3)).or.
     1       iabs(ih(1)).eq.iabs(ih(2)+ih(3))) ind=ind+5
        endif
        if(ind.lt.3) ind=2
        if(ind.gt.5) ind=0
        do j=1,9
          vst(j,1)=vst(j,1)+tmul*ee(j)
          if(ind.gt.0) vst(j,ind)=vst(j,ind)+tmul*ee(j)
        enddo
        ast(1)=ast(1)+tmul
        if(ind.gt.0) ast(ind)=ast(ind)+tmul
        if(nzr.le.18) zt(nzr,1)=zt(nzr,1)+tmul
        if(ind.gt.0.and.nzr.le.18) zt(nzr,ind)=zt(nzr,ind)+tmul
        net=10.0*ee(1)
        if(net.gt.25) net=25
        if(net.gt.0) anu(net)=anu(net)+1.
1250  continue
1300  if(iz.ne.0) nc=NRLim
      call HeapRII(nc,ex,ix1,ix2,-1)
      NRUsed=nc
      call NewPg(0)
      call TitulekVRamecku('E statistics')
      call TitulekPodtrzeny('E**2 according to appropriate index '//
     1                      'group - before rescaling','=')
      call NewLn(1)
      write(lst,FormA)
      nn=8
      if(iabs(CrSystemPom).ge.4) nn=6
      i=3
      if(iabs(CrSystemPom).ge.5) i=i+3
      call NewLn(i)
      if(iabs(CrSystemPom).le.CrSystemOrthorhombic) then
        Veta='                    all'//
     1       '        eee        oee        eoe        ooe'//
     2       '        eeo        oeo        eoo        ooo'
        go to 1500
      else if(CrSystemPom.eq.CrSystemTetragonal) then
        Veta='                    all'//
     1       '        eee     eoe,oee       ooe'//
     2       '        eeo     eoo,oeo       ooo'
        go to 1500
      else if(CrSystemPom.eq.CrSystemTrigonal.or.
     1        CrSystemPom.eq.CrSystemHexagonal) then
        Veta='Index groups divided on: 1) mod(h-k,3)    '//
     1       '2) mod(h,3)     3) mod(l,3)'
      else if(CrSystemPom.eq. CrSystemCubic.or.
     1        CrSystemPom.eq.-CrSystemTrigonal) then
        Veta='Index groups divided on: 1) mod(h+k+l,3)    '//
     1       '2) mod(h-l,3)    3) mod(h+k+l,2)'
      endif
      write(lst,FormA) Veta(:idel(Veta))
      write(lst,'(25x,''e - zero remainder     o - non-zero '',
     1                ''remainder''/)')
      Veta='                    all'//
     1     '        eoe     ooe,oee       eee'//
     2     '        eoo     ooo,oeo       eeo'
1500  write(lst,FormA) Veta(:idel(Veta))
      write(lst,'(''E**2'',8x,9f11.3)') scal(9),(scal(i),i=1,nn)
      write(lst,'(''Number'',6x,9i11)')
     1      nint(ang(9)),(nint(ang(i)),i=1,nn)
      do i=1,10
        stl(i)=float(i)*sqrt(RhoMax)/10.
        if(shr(i).gt..5) rhr(i)=rhr(i)/shr(i)
        do j=1,5
          if(ast(j).gt.0.5) vst(i,j)=vst(i,j)/ast(j)
        enddo
      enddo
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('Distribution of E**2 with sin(theta)/'//
     1                      'lambda - after rescaling','=')
      call NewLn(1)
      write(lst,FormA)
      call NewLn(3)
      write(lst,'(''sinth/lam'',10f10.4)') stl
      write(lst,'(''E**2     '',10f10.4)') rhr
      write(lst,'(''Number   '',10i10  )')(nint(shr(i)),i=1,10)
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('Averaged values','=')
      call NewLn(1)
      write(lst,FormA)
      call NewLn(14)
      if(iabs(CrSystemPom).gt.3) itext(6,1)=itext(6,2)
      jj=1
      if(CrSystemPom.eq.CrSystemCubic.or.
     1   CrSystemPom.eq.-CrSystemTrigonal) jj=jj+1
      write(lst,'(41x,''Experimental'',39x,''Theoretical''//
     1            ''  Value'',12x,''All data'',7x,''hkl'',6x,4a2,6x,
     2            2a2,6x,4a2,6x,''Acentric     Centric  Hypercentric''
     3            /)')(itext(i,jj),i=1,10)
      write(lst,'(a11,3x,5f12.3,2X,3f12.3)')
     1  (ELabels(i),(vst(i,j),j=1,5),ava(i),avc(i),avh(i),i=1,9)
      write(lst,'(''  Weight      '',5i12)')(nint(ast(i)),i=1,5)
      do i=1,18
        do j=1,5
          if(ast(j).gt..5) zt(i,j)=zt(i,j)/ast(j)
          if(i.ne.1) zt(i,j)=zt(i,j)+zt(i-1,j)
        enddo
        zz(i)=.1*float(i)
        if(i.gt. 8) zz(i)=.2*float(i)-.8
        if(i.gt.12) zz(i)=.4*float(i)-3.2
        cpa(i)=1.-exp(-zz(i))
        t=1./(1.+.47047*sqrt(.5*zz(i)))
        cpc(i)=1.-((.74786*t-.09588)*t+.34802)*t*exp(-.5*zz(i))
      enddo
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('N(z) cumulative probability distribution',
     1                      '=')
      call NewLn(1)
      write(lst,FormA)
      call NewLn(2)
      write(lst,'(''    Z  '',12x,''All data'',7x,''hkl'',6x,4a2,6x,
     1            2a2,6x,4a2,6x,''Acentric     Centric  Hypercentric''
     2            /)')(itext(i,jj),i=1,10)
      do i=1,18
        call NewLn(1)
        write(lst,'(f6.1,8x,5f12.3,2x,3f12.3)')
     1     zz(i),(zt(i,j),j=1,5),cpa(i),cpc(i),cph(i)

      enddo
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('Distribution of E - number of E''s '//
     1                      'greater than 0.7','=')
      call NewLn(1)
      write(lst,FormA)
      do i=2,25
        j=26-i
        anu(j)=anu(j)+anu(j+1)
      enddo
      call NewLn(2)
      write(lst,'(''E     '',19f6.1)')(.1*float(i),i=7,25)
      write(lst,'(''Number'',19i6  )')(nint(anu(i)),i=7,25)
      if(nc.lt.NumB) NumB=nc
      if(nc.lt.NZero) NZero=nc
      if(ListE.eq.0) ListE=NumB
      if(allocated(iha)) deallocate(iha)
      allocate(iha(3,nc))
      do i=1,nc
        call UnpackHKL(ix1(i),iha(1,i),mxh,mxd,3)
      enddo
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('Large E''s used for phase determination',
     1                      '=')
      call NewLn(1)
      write(lst,FormA)
      call NewLn(1)
      write(lst,'(5(''  Code   h   k   l    E  ''))')
      ik=0
      do i=1,(ListE-1)/5+1
        ip=ik+1
        ik=min(ik+5,ListE)
        call NewLn(1)
        write(lst,'(5(i6,3i4,f7.3))')(ii,(iha(j,ii),j=1,3),ex(ii),
     1                                ii=ip,ik)
      enddo
      call NewLn(1)
      write(lst,FormA)
      call TitulekPodtrzeny('Small E''s used in psi(zero)','=')
      call NewLn(1)
      write(lst,FormA)
      call NewLn(1)
      write(lst,'(5(''  Code   h   k   l    E  ''))')
      ik=nc-NZero
      do i=1,(ListE-1)/5+1
        ip=ik+1
        ik=min(ik+5,nc)
        call NewLn(1)
        write(lst,'(5(i6,3i4,f7.3))')(ii,(iha(j,ii),j=1,3),ex(ii),
     1                                ii=ip,ik)
      enddo
      do i=1,numb
        eps=mod(ix2(i),1024)/128+1
        ex(i)=sqrt(eps)*ex(i)
      enddo
      if(allocated(iha)) deallocate(iha)
      return
      end
      subroutine PackHKL(hkl,ih,mxh,mxd,n)
      include 'fepc.cmn'
      dimension ih(n),mxh(n),mxd(n)
      integer hkl
      hkl=0
      do k=1,n
        hkl=hkl+(ih(k)+mxh(k))*mxd(k)
      enddo
      return
      end
      subroutine UnpackHKL(hkl,ih,mxh,mxd,n)
      include 'fepc.cmn'
      dimension ih(n),mxh(n),mxd(n)
      integer hkl
      do i=1,n
        if(i.eq.1) then
          ih(i)=hkl/mxd(i)-mxh(i)
        else
          ih(i)=mod(hkl,mxd(i-1))/mxd(i)-mxh(i)
        endif
      enddo
      return
      end
      subroutine DMFirst
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      call DMGenerEqHKL
      NTripletsMax=1000
      NTriplets=0
      if(allocated(Ph1)) deallocate(Ph1,Ph2,EEE)
      allocate(Ph1(NTripletsMax),Ph2(NTripletsMax),EEE(NTripletsMax))
      if(NZero.gt.0) then
        ik=NRUsed-NZero
        call DMSigma2(3,250,ix1(ik),ix2(ik),NZero,NPsi)
      endif
      call DMSigma2(1,kmin,ix1,ix2,NumB,NSig)
      return
      end
      subroutine DMGenerEqHKL
      use Basic_mod
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      integer, allocatable :: ir1o(:),loco(:)
      integer CrlCentroSymm
      Nip=1
      Nior=0
      NiorMax=NumB*NSymmN(KPhase)
      if(CrlCentroSymm().gt.0) NiorMax=NiorMax/2
      if(allocated(ir1)) deallocate(ir1,loc)
      allocate(ir1(NiorMax),loc(NiOrMax))
      do i=1,NumB
        call UnpackHKL(ix1(i),ih,mxh,mxd,3)
        do 1800j=1,NSymmN(KPhase)
          call MultMIRI(ih,RM6(1,j,1,KPhase),ihp,1,NDim(KPhase),
     1                  NDim(KPhase))
          pom=0.
          do k=1,3
            pom=pom-float(ihp(k))*s6(k,j,1,KPhase)
          enddo
          if( ihp(1).gt.0.or.
     1       (ihp(1).eq.0.and.ihp(2).gt.0).or.
     2       (ihp(1).eq.0.and.ihp(2).eq.0.and.ihp(3).gt.0)) then
            isgn= 1.
          else
            isgn=-1.
            call IntVectorToOpposite(ihp,ihp,NDim(KPhase))
            pom=-pom
          endif
          pom=pom*24.
          call PackHKL(kpom,ihp,mxh,mxd,3)
          ipom=mod(nint(pom)*isgn+2400,24)
          do k=Nip,Nior
            if(kpom.eq.ir1(k)) then
              if(loc(k).lt.0) loc(k)=(24*i+ipom)*isgn
              go to 1800
            endif
          enddo
          Nior=Nior+1
          if(Nior.gt.NiorMax) then
            allocate(ir1o(NiorMax),loco(NiorMax))
            call CopyVekI(ir1,ir1o,NiorMax)
            call CopyVekI(loc,loco,NiorMax)
            NiorMaxO=NiorMax
            deallocate(ir1,loc)
            NiorMax=NiorMax+2000
            allocate(ir1(NiorMax),loc(NiorMax))
            call CopyVekI(ir1o,ir1,NiorMaxO)
            call CopyVekI(loco,loc,NiorMaxO)
            deallocate(ir1o,loco)
          endif
          ir1(Nior)=kpom
          loc(Nior)=(24*i+ipom)*isgn
1800    continue
        Nip=Nior+1
      enddo
      call HeapII(Nior,ir1,loc,1)
      return
      end
      subroutine DMSigma2(Jump,KMinP,ih1,ih2,NERef,NSigAct)
      use Basic_mod
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      dimension iha(3,3),STable(30),EEEO(:),NSigAct(*),ih1(*),ih2(*)
      integer Ph1O(:),Ph2O(:)
      logical EqIV0,UzHoMa
      allocatable Ph1O,Ph2O,EEEO
      do i=1,30
        STable(i)=SIN(15.*ToRad*float(i-1))
      enddo
      EE3min=KMinP
      if(Jump.eq.1) then
        Sigma=200.*SigQ3/(SigN2*sqrt(SigN2))
      else if(Jump.eq.3) then
        Sigma=100.
        SigZ=2.*SigQ3/(SigN2*SQRT(SigN2))
      endif
      call SetIntArrayTo(ih,3,0)
      call PackHKL(Nula,ih,mxh,mxd,3)
      nan=0
      do i=1,NERef
        NTripletsStart=NTriplets+1
        nn=0
        eval=ex(i)/float(mod(ih2(i),1024)/128+1)
        Index=ih1(i)+Nula
        ih10=ih1(i)-Nula
        isg=-1
        i1=Nior
        i2=Nior
        ir12=ir1(i2)-Nula
1500    i1=i1+isg
        if(isg.lt.0) then
          if(i1.le.0) then
            isg=1
            go to 1500
          endif
        else
          if(i1.ge.i2) go to 1800
        endif
        ir11=ir1(i1)-Nula
1540    ipom=ir12+isg*ir11-ih10
        if(ipom.lt.0) then
          go to 1500
        else if(ipom.eq.0) then
          go to 1700
        endif
        i2=i2-1
        if(isg.gt.0.and.i1.ge.i2) then
          go to 1800
        else
          ir12=ir1(i2)-Nula
          go to 1540
        endif
1700    call UnpackHKL(ih1(i),iha(1,1),mxh,mxd,3)
        call IntVectorToOpposite(iha(1,1),iha(1,1),3)
        call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
        if(isg.lt.0) call IntVectorToOpposite(iha(1,2),iha(1,2),3)
        call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
        call SetIntArrayTo(ih,3,0)
        do j=1,3
          do k=1,3
            ih(j)=ih(j)+iha(j,k)
          enddo
        enddo
        if(EqIV0(ih,3)) then
          j=iabs(loc(i1))/24
          k=iabs(loc(i2))/24
          if(j.eq.k.or.((i.le.j.or.i.le.k).and.Jump.ne.3)) go to 1500
          EE3=anint(Sigma*eval*ex(j)*ex(k))
          if(EE3.lt.EE3min) go to 1500
          iph1=iSign(j,loc(i1))*isg
          iph2=iSign(k,loc(i2))
          ifz=0
          if(isg.lt.0.and.loc(i1).gt.0) then
            ifz=2*(mod(iabs(ih2(j)),128)/8)
            if(ifz.gt.0) iph1=iabs(iph1)
          endif
          ifz=isg*mod(iabs(loc(i1)),24)+mod(iabs(loc(i2)),24)+ifz
          ifaze=mod(ifz+240,24)
          if(k.gt.j) then
            l=iph1
            iph1=iph2
            iph2=l
          endif
          UzHoMa=.false.
          do l=NTripletsStart,NTriplets
            if(iabs(iph1).eq.iabs(Ph1(l)).and.
     1         iabs(iph2).eq.iabs(Ph2(l))) then
              if(iph1.eq.Ph1(l).and.iph2.eq.Ph2(l)) then
                UzHoMa=.true.
                go to 1260
              else
                go to 1500
              endif
            endif
          enddo
          nn=nn+1
          NTriplets=NTriplets+1
          l=NTriplets
          if(NTriplets.gt.NTripletsMax) then
            allocate(Ph1O(NTripletsMax),Ph2O(NTripletsMax),
     1               EEEO(NTripletsMax))
            call CopyVekI(Ph1,Ph1O,NTripletsMax)
            call CopyVekI(Ph2,Ph2O,NTripletsMax)
            call CopyVek (EEE,EEEO,NTripletsMax)
            deallocate(Ph1,Ph2,EEE)
            NTripletsMaxO=NTripletsMax
            NTripletsMax =NTripletsMax+1000
            allocate(Ph1(NTripletsMax),Ph2(NTripletsMax),
     1               EEE(NTripletsMax))
            call CopyVekI(Ph1O,Ph1,NTripletsMaxO)
            call CopyVekI(Ph2O,Ph2,NTripletsMaxO)
            call CopyVek (EEEO,EEE,NTripletsMaxO)
            deallocate(Ph1O,Ph2O,EEEO)
          endif
          if(ifaze.ne.0.and.ifaze.ne.12) nan=nan+1
1260      if(UzHoMa) then
            E3=nint(100.*EEE(l))
            ifz=nint(100.*(100.*EEE(l)-E3))
            sr=EE3*STable(ifaze+7)+E3*STable(ifz+7)
            si=EE3*STable(ifaze+1)+E3*STable(ifz+1)
            ifaze=nint((atan2(si,sr)/ToRad+360.)/15.)
            EE3=anint(sqrt(sr**2+si**2))
          else
            Ph1(l)=iph1
            Ph2(l)=iph2
          endif
          EEE(l)=.01*(EE3+.01*float(ifaze))
        endif
        go to 1500
1800    NSigAct(i)=nn
      enddo
      if(Jump.lt.3.and.4*nan/NTriplets.gt.0) NTriplets=-NTriplets
      return
      end
      subroutine DMSigma2Test1(Klic)
      use Basic_mod
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      dimension iha(3,3),ihx(3,2,1000)
      logical EqIV0,EqIV
      if(Klic.eq.1) then
        Sigma=200.*SigQ3/(SigN2*sqrt(SigN2))
      else if(Klic.eq.3) then
        Sigma=100.
      endif
      call SetIntArrayTo(ih,3,0)
      call PackHKL(Nula,ih,mxh,mxd,3)
      do i=1,NumB
        nn=0
        Index=ix1(i)+Nula
        ix10=ix1(i)-Nula
        do i1=1,Nior
          ir11=ir1(i1)-Nula
          do 1100i2=1,Nior
            ir12=ir1(i2)-Nula
            if(-ix10+ir11+ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10-ir11+ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,2),iha(1,2),3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10+ir11-ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,3),iha(1,3),3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10-ir11-ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,2),iha(1,2),3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,3),iha(1,3),3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            go to 1100
1050        do k=1,nn
              if((EqIV(iha(1,2),ihx(1,1,k),3).and.
     1            EqIV(iha(1,3),ihx(1,2,k),3)).or.
     2           (EqIV(iha(1,3),ihx(1,1,k),3).and.
     3            EqIV(iha(1,2),ihx(1,2,k),3))) then
                go to 1100
              endif
            enddo
            nn=nn+1
            call CopyVekI(iha(1,2),ihx(1,1,nn),3)
            call CopyVekI(iha(1,3),ihx(1,2,nn),3)
1100      continue
        enddo
        nno=nn
        nn=0

        isg=-1
        i1=Nior
        i2=Nior
1520    i1=i1-1
        if(i1.le.0) go to 1600
1540    if(ir1(i2)-ir1(i1)-ix10) 1520,1700,1580
1580    i2=i2-1
        go to 1540
1600    isg=1
1620    i1=i1+1
        if(i1.ge.i2) go to 1800
1640    if(ir1(i2)+ir1(i1)-ix10-2*Nula) 1620,1700,1680
1680    i2=i2-1
        if(i1-i2) 1640,1800,1800
1700    call UnpackHKL(ix1(i),iha(1,1),mxh,mxd,3)
        call IntVectorToOpposite(iha(1,1),iha(1,1),3)
        call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
        if(isg.lt.0) call IntVectorToOpposite(iha(1,2),iha(1,2),3)
        call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
        call SetIntArrayTo(ih,3,0)
        do j=1,3
          do k=1,3
            ih(j)=ih(j)+iha(j,k)
          enddo
        enddo
        if(EqIV0(ih,3)) then
          do k=1,nn
           if((EqIV(iha(1,2),ihx(1,1,k),3).and.
     1         EqIV(iha(1,3),ihx(1,2,k),3)).or.
     2        (EqIV(iha(1,3),ihx(1,1,k),3).and.
     3         EqIV(iha(1,2),ihx(1,2,k),3))) then
             go to 1320
           endif
         enddo
         nn=nn+1
         call CopyVekI(iha(1,2),ihx(1,1,nn),3)
         call CopyVekI(iha(1,3),ihx(1,2,nn),3)
        endif
1320    IF(ISG) 1520,1800,1620
1800    continue
        write(Cislo,'(2i5)') nno,nn
        call FeWinMessage(Cislo,' ')
      enddo
      return
      end
      subroutine DMSigma2Test2(Klic)
      use Basic_mod
      use Direct_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'direct.cmn'
      dimension iha(3,3),ihx(3,2,1000)
      logical EqIV0,EqIV
      if(Klic.eq.1) then
        Sigma=200.*SigQ3/(SigN2*sqrt(SigN2))
      else if(Klic.eq.3) then
        Sigma=100.
      endif
      call SetIntArrayTo(ih,3,0)
      call PackHKL(Nula,ih,mxh,mxd,3)
      do i=1,NumB
        nn=0
        Index=ix1(i)+Nula
        ix10=ix1(i)-Nula
        do i1=1,Nior
          ir11=ir1(i1)-Nula
          do 1100i2=1,Nior
            ir12=ir1(i2)-Nula
            if(-ix10+ir11+ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10-ir11+ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,2),iha(1,2),3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10+ir11-ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,3),iha(1,3),3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            if(-ix10-ir11-ir12.eq.0) then
              call UnpackHKL(ix1(i) ,iha(1,1),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,1),iha(1,1),3)
              call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,2),iha(1,2),3)
              call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
              call IntVectorToOpposite(iha(1,3),iha(1,3),3)
              call SetIntArrayTo(ih,3,0)
              do j=1,3
                do k=1,3
                  ih(j)=ih(j)+iha(j,k)
                enddo
              enddo
              if(EqIV0(ih,3)) then
                go to 1050
              else
                go to 1100
              endif
            endif
            go to 1100
1050        do k=1,nn
              if((EqIV(iha(1,2),ihx(1,1,k),3).and.
     1            EqIV(iha(1,3),ihx(1,2,k),3)).or.
     2           (EqIV(iha(1,3),ihx(1,1,k),3).and.
     3            EqIV(iha(1,2),ihx(1,2,k),3))) then
                go to 1100
              endif
            enddo
            nn=nn+1
            call CopyVekI(iha(1,2),ihx(1,1,nn),3)
            call CopyVekI(iha(1,3),ihx(1,2,nn),3)
1100      continue
        enddo
        nno=nn
        nn=0
        isg=-1
        i1=Nior
        i2=Nior
        ir12=ir1(i2)-Nula
1500    i1=i1+isg
        if(isg.lt.0) then
          if(i1.le.0) then
            isg=1
            go to 1500
          endif
        else
          if(i1.ge.i2) go to 1800
        endif
        ir11=ir1(i1)-Nula
1540    ipom=ir12+isg*ir11-ix10
        if(ipom.lt.0) then
          go to 1500
        else if(ipom.eq.0) then
          go to 1700
        endif
        i2=i2-1
        if(isg.gt.0.and.i1.ge.i2) then
          go to 1800
        else
          ir12=ir1(i2)-Nula
          go to 1540
        endif
1700    call UnpackHKL(ix1(i),iha(1,1),mxh,mxd,3)
        call IntVectorToOpposite(iha(1,1),iha(1,1),3)
        call UnpackHKL(ir1(i1),iha(1,2),mxh,mxd,3)
        if(isg.lt.0) call IntVectorToOpposite(iha(1,2),iha(1,2),3)
        call UnpackHKL(ir1(i2),iha(1,3),mxh,mxd,3)
        call SetIntArrayTo(ih,3,0)
        do j=1,3
          do k=1,3
            ih(j)=ih(j)+iha(j,k)
          enddo
        enddo
        if(EqIV0(ih,3)) then
          do k=1,nn
            if((EqIV(iha(1,2),ihx(1,1,k),3).and.
     1          EqIV(iha(1,3),ihx(1,2,k),3)).or.
     2         (EqIV(iha(1,3),ihx(1,1,k),3).and.
     3          EqIV(iha(1,2),ihx(1,2,k),3))) go to 1500
          enddo
          nn=nn+1
          call CopyVekI(iha(1,2),ihx(1,1,nn),3)
          call CopyVekI(iha(1,3),ihx(1,2,nn),3)
        endif
        go to 1500
1800    write(Cislo,'(2i5)') nno,nn
        call FeWinMessage(Cislo,' ')
      enddo
      return
      end
      subroutine TablePatt
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension MPatt(:,:,:,:),TPatt(:,:,:),Mult(:,:),mpom(3,3),
     1          fpom(3,3)
      character*132 ven
      character*80 t80
      logical EqRV,EqIV
      allocatable MPatt,TPatt,Mult
      open(lst,file=fln(1:ifln)//'.pat')
      n=NSymmN(KPhase)*NLattVec(KPhase)
      allocate(MPatt(3,3,n,n),TPatt(3,n,n),Mult(n,n))
      do i=1,NSymmN(KPhase)
        do j=1,NSymmN(KPhase)
          do k=1,3
            do l=1,3
              ind=k+(l-1)*NDim(KPhase)
              MPatt(k,l,i,j)=nint(rm6(ind,j,1,KPhase)-
     1                            rm6(ind,i,1,KPhase))
            enddo
            TPatt(k,i,j)=s6(k,j,1,KPhase)-s6(k,i,1,KPhase)
          enddo
          call od0do1(TPatt(1,i,j),TPatt(1,i,j),3)
        enddo
      enddo
      j2=0
      do jp=1,(NSymmN(KPhase)-1)/4+1
        j1=j2+1
        j2=min0(j2+4,NSymmN(KPhase))
        kk=25
        ven='                        |'
        do j=j1,j2
          do k=1,3
            do l=1,3
              ind=k+(l-1)*NDim(KPhase)
              mpom(k,l)=nint(rm6(ind,j,1,KPhase))
            enddo
          enddo
          call SimpleSymmCode(mpom,s6(1,j,1,1),t80)
          ven=ven(1:kk)//t80(1:24)//'|'
          kk=kk+25
        enddo
        write(lst,101)(ven(k:k),k=1,kk)
        write(lst,102)
        do i=1,NSymmN(KPhase)
          kk=25
          do k=1,3
            do l=1,3
              ind=k+(l-1)*NDim(KPhase)
              mpom(k,l)=nint(rm6(ind,i,1,KPhase))
            enddo
          enddo
          call SimpleSymmCode(mpom,s6(1,i,1,KPhase),t80)
          ven=t80(1:24)//'|'
          do j=j1,j2
            call SimpleSymmCode(MPatt(1,1,i,j),TPatt(1,i,j),t80)
            ven=ven(1:kk)//t80(1:24)//'|'
            kk=kk+25
          enddo
          write(lst,101)(ven(k:k),k=1,kk)
        enddo
        write(lst,102)
        write(lst,FormA)
        write(lst,FormA)
      enddo
      do j=1,NSymmN(KPhase)
        mult(1,j)=1
        do i=2,NSymmN(KPhase)
          mult(i,j)=1
          do k=1,3
            do l=1,3
              ind=k+(l-1)*NDim(KPhase)
              fpom(k,l)=rm6(ind,i,1,KPhase)
              mpom(k,l)=nint(fpom(k,l))
            enddo
          enddo
          call multmi(mpom,MPatt(1,1,1,j),MPatt(1,1,i,j),3,3,3)
          call multm(fpom,TPatt(1,1,j),TPatt(1,i,j),3,3,1)
          call od0do1(TPatt(1,i,j),TPatt(1,i,j),3)
        enddo
      enddo
      do ivt=2,NLattVec(KPhase)
        k=NSymmN(KPhase)*(ivt-1)
        do j=1,NSymmN(KPhase)
          do i=1,NSymmN(KPhase)
            call CopyVekI(MPatt(1,1,i,j),
     1                    MPatt(1,1,i+k,j),9)
            call AddVek(TPatt(1,i,j),vt6(1,ivt,1,KPhase),
     1                  TPatt(1,i+k,j),3)
            call od0do1(TPatt(1,i+k,j),TPatt(1,i+k,j),3)
            mult(i+k,j)=1
          enddo
        enddo
      enddo
      nt=0
      do j=1,NSymmN(KPhase)
        n=0
        do i=1,NSymmN(KPhase)*NLattVec(KPhase)
          if(mult(i,j).eq.0) cycle
          do jj=j,NSymmN(KPhase)
            if(jj.eq.j) then
              ip=i+1
            else
              ip=1
            endif
            do ii=ip,NSymmN(KPhase)*NLattVec(KPhase)
              if(mult(ii,jj).eq.0) cycle
              if(.not.eqiv(MPatt(1,1,i,j),MPatt(1,1,ii,jj),9))
     1          cycle
              if(.not.eqrv(TPatt(1,i,j),TPatt(1,ii,jj),3,.0001))
     1          cycle
              mult(i,j)=mult(i,j)+mult(ii,jj)
              mult(ii,jj)=0
            enddo
          enddo
          n=n+1
          if(n.eq.1) then
            nt=nt+1
            write(lst,FormA)
            write(lst,'(''Type '',a1,10x,''mutiplicity:'',i3)')
     1        char(ichar('A')-1+nt),mult(i,j)
            write(lst,FormA)
          endif
          call SimpleSymmCode(MPatt(1,1,i,j),TPatt(1,i,j),t80)
          write(lst,FormA) t80(:idel(t80))
        enddo
      enddo
      close(lst)
      deallocate(MPatt,TPatt,Mult)
      return
101   format(132a1)
102   format(125('-'))
      end
      subroutine SimpleSymmCode(m,x,t80)
      dimension m(3,3),x(3)
      character*(*) t80
      character*10 is
      character*1 smbc(3)
      data smbc/'x','y','z'/
      do 2000i=1,3
        is=' '
        do 1000k=1,8
          p=x(i)*float(k)
          l=nint(p)
          if(abs(p-float(l)).lt..00001) go to 1100
1000    continue
        is(1:4)='??/?'
        go to 1200
1100    if(l.ne.0) then
          write(is(1:4),'(i2,''/'',i1)') l,k
          if(k.eq.1) is(3:4)='  '
        endif
1200    kk=idel(is)
        do 1400j=1,3
          l=m(i,j)
          k=iabs(l)
          if(l.ne.0) then
            if(l.gt.0) then
              if(kk.ne.0) then
                kk=kk+1
                is(kk:kk)='+'
              endif
            else
              kk=kk+1
              is(kk:kk)='-'
            endif
            if(k.ne.1) then
              kk=kk+1
              write(is(kk:kk),'(i1)') k
            endif
            kk=kk+1
            is(kk:kk)=smbc(j)
          endif
1400    continue
        if(kk.eq.0) then
          is='0'
          kk=1
        endif
        if(i.eq.1) then
          t80=is
        else
          t80=t80(1:idel(t80))//','//is(1:kk)
        endif
2000  continue
      call zhusti(t80)
      return
      end

