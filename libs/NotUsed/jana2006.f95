      program Jana2006
      use Atoms_mod
      use Basic_mod
      use RepAnal_mod
      use DatRed_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'main.cmn'
      include 'powder.cmn'
      include 'editm9.cmn'
      integer FeChdir,FeMenu,UseTabsIn
      dimension MFile(9)
      logical FeYesNo,ExistFile,StructureExists,CrwLogicQuest,
     1        FeYesNoHeader,StructureOpened,Change,EqIgCase,RefineEnd,
     2        GetStructureLocked,Ukoncit,Focus,ParentStructureO
      character*256 Veta1,Veta2,t256,flno,HistoryFileTmp,CurrentDirO,
     1              FlnOld
      character*8   Ext
      MasterName='Jana2006'
      Language=0
      VersionString='               Version : 27/01/2016'
      i=FeEtime()
      LogLn=0
      call SetFePCConstants
      call EM9SetBasic
      call getfln
      call OpenWorkSpace
      call SetBasicConstants
      call FeGetCurrentDir
      if(VasekDebug.ne.0)
     1  write(44,'(''CURRENT DIR : '',a)') CurrentDir(:idel(CurrentDir))
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call TestDataFiles(Ukoncit)
      if(Ukoncit) go to 9000
      close(44)
      if(VasekDebug.ne.0) VasekDebug=0
      if(ifln.eq.0.and..not.BatchMode) then
        if(ExistFile(HistoryFile)) then
          lni=NextLogicNumber()
          call OpenFile(lni,HistoryFile,'formatted','unknown')
800       read(lni,FormA,end=900) Veta1
          read(lni,FormA,end=900) Veta2
          i=FeChdir(Veta1)
          fln=Veta2
          ifln=idel(fln)
          if(StructureOpened(fln)) then
            ifln=0
            go to 800
          endif
900       call CloseIfOpened(lni)
          if(ifln.eq.0.and.fln.ne.' ') ifln=idel(fln)
          call FeBottomInfo(' ')
          call FeGetCurrentDir
        endif
      endif
      if(ifln.ne.0) then
        if(StructureOpened(fln)) then
          NInfo=2
          TextInfo(1)='The structure is either opened by another '//
     1                 'user/task'
          TextInfo(2)='or not correctly closed due to a previous '//
     1                'crash.'
          if(.not.FeYesNoHeader(-1.,-1.,
     1                          'Do you want to continue anyhow?',0))
     2      then
            fln=' '
            ifln=0
          endif
        endif
      endif
1000  call FeEvent(1)
      if(EventType.ne.0) go to 1000
      call FeZacatek
      PreviousM40='jm40'
      call CreateTmpFile(PreviousM40,i,0)
      PreviousM41='jm41'
      call CreateTmpFile(PreviousM41,i,0)
      PreviousM42='jm42'
      call CreateTmpFile(PreviousM42,i,0)
      PreviousM50='jm50'
      call CreateTmpFile(PreviousM50,i,0)
      call DeletePomFiles
      if(ifln.gt.0) call ChangeUSDFile(fln,'opened','*')
      CurrentDirO=' '
      OKForBasicFiles=0
      if(BatchMode) then
        call FeSetFixFont
        i=nint(XLenGrWin/FixFontWidthInPixels)
        j=nint(YLenGrWin/FixFontHeightInPixels)
        call FeLstMake(XMinGrWin,YMaxGrWin,i,j-1,1,' ')
      endif
      call FortFilesClean
      if(HKLUpdate) call EM9HKLUpdate
1100  call FeExceptionInfo
      ShowInfoOnScreen=.true.
      SilentRun=.false.
      AniNaListing=.false.
      ParentM90=' '
      StructureLocked=GetStructureLocked(fln)
      if(VasekTest.gt.0) call FortFilesCheck
      if(ifln.gt.0) call UpdateSummary
      if(StructureLocked) then
        if(CurrentDir.ne.CurrentDirO.or.fln.ne.flno) then
          NInfo=3
          TextInfo(1)='The selected structure is locked. All changes '//
     1                'of the basic'
          TextInfo(2)='structure files will be ignored. If necessary '//
     1                'you can unlock'
          TextInfo(3)='the structure by "File->Structure->Unlock".'
        else if(OKForBasicFiles.eq.0) then
          NInfo=1
          TextInfo(1)='The structure has been locked.'
        endif
        if(NInfo.gt.0) call FeInfoOut(-1.,-1.,'WARNING','L')
        OKForBasicFiles=-1
      else
        OKForBasicFiles= 0
      endif
      CurrentDirO=CurrentDir
      flno=fln
      if(BatchMode) go to 1125
      if(CurrentDir.ne.' '.and.CurrentDir.ne.TmpDir.and.
     1   fln.ne.' '.and.(fln(1:1).ne.'#'.or.fln(ifln:ifln).ne.'#')) then
        HistoryFileTmp='jhst'
        call CreateTmpFile(HistoryFileTmp,i,0)
        lno=NextLogicNumber()
        call OpenFile(lno,HistoryFileTmp,'formatted','unknown')
        write(lno,FormA) CurrentDir(:idel(CurrentDir))
        write(lno,FormA) fln(:ifln)
        if(ExistFile(HistoryFile)) then
          n=1
          lni=NextLogicNumber()
          call OpenFile(lni,HistoryFile,'formatted','unknown')
1110      read(lni,FormA,end=1120) Veta1
          read(lni,FormA,end=1120) Veta2
          if(OpSystem.le.0) then
            if(EqIgCase(Veta1,CurrentDir).and.EqIgCase(Veta2,fln))
     1        go to 1110
          else
            if(Veta1.eq.CurrentDir.and.Veta2.eq.fln) go to 1110
          endif
          n=n+1
          write(lno,FormA) Veta1(:idel(Veta1))
          write(lno,FormA) Veta2(:idel(Veta2))
          if(n.lt.mxhst) go to 1110
1120      call CloseIfOpened(lni)
        endif
        call CloseIfOpened(lno)
        HistoryFileOld='jhso'
        call CreateTmpFile(HistoryFileOld,i,0)
        call CopyFile(HistoryFile,HistoryFileOld)
        call CopyFile(HistoryFileTmp,HistoryFile)
        call DeleteFile(HistoryFileTmp)
      endif
1125  if(KPhaseBasic.ne.KPhase) then
        if(VasekTest.eq.1)
     1    call FeChybne(-1.,-1.,'KPhase<>KPhaseBasic',' ',SeriousError)
        KPhase=KPhaseBasic
      endif
      if(BatchMode) then
        call BatchCoDal(i,j,k)
      else
        call CoDal(i,j,k)
      endif
1130  RunningProgram=' '
      if(i.eq.IdFile) then
        if(j.eq.IdFileDOS) then
          call FeShell
        else if(j.eq.IdFileExport) then
          if(k.eq.IdFileExportShelx) then
            if(ExistM50) then
              Veta1=' '
              call Trm4050(5,Veta1,' ',ich)
              if(ich.ne.0) go to 1100
            else
              Veta1=fln
            endif
            if(isPowder) then
              if(kcommenMax.gt.0) then
                call iom50(0,0,fln(:ifln)//'.m50')
                call ComSym(0,0,ich)
              endif
              call PwdLeBail(1,.true.)
              call FeFlowChartRemove
              call DRExport(-1,Veta1)
            else
              if(ExistM90.or.ExistM95) call DRExport(-1,Veta1)
            endif
          else if(k.eq.IdFileExportJana2000) then
            call CrlExportJana2000
          else if(k.eq.IdFileExportXD) then
            call CrlExportXD
          else if(k.eq.IdFileExportHRTEM) then
            call Refine(1,RefineEnd)
            call DeleteFile(fln(:ifln)//'.ref')
            call DeleteFile(fln(:ifln)//'.s40')
            call GraphicOutput(-8,fln,ich)
          else if(k.eq.IdFileExportDMol3) then
            call CrlExportDMol3
          endif
        else if(j.eq.IdFileCIF) then
          if(k.eq.IdFileCIFMake) then
            if(ParentStructure) then
              call SSG2QMag(Fln)
              Veta1=fln(:ifln)//'.cif'
              MagneticType(KPhase)=0
              call CIFMakeBasicTemplate(Veta1,0)
            else
              call CIFMakeTemplate(Veta1,ich)
              if(ich.ne.0) go to 1100
            endif
            call CIFUpdate(Veta1,ParentStructure,0)
            if(ParentStructure) then
              call QMag2SSG(Fln,0)
              MagneticType(KPhase)=1
            endif
          else if(k.eq.IdFileCIFUpdate) then
            if(ParentStructure) then
              call SSG2QMag(Fln)
              Veta1=fln(:ifln)//'.cif'
              MagneticType(KPhase)=0
            else
              Veta1=' '
            endif
            call CIFUpdate(Veta1,ParentStructure,0)
            if(ParentStructure) then
              call QMag2SSG(Fln,0)
              MagneticType(KPhase)=1
            endif
          else if(k.eq.IdFileCIFEdit) then
            Veta1=' '
            call CIFEdit(Veta1)
          endif
        else if(j.eq.IdFileStruct) then
1200      Veta1='Select name of the structure'
          Focus=.false.
          if(k.eq.IdFileStructNew) then
            Veta1='New : '//Veta1(:idel(Veta1))
            Veta2=fln
            Change=.true.
            call DeletePomFiles
          else if(k.eq.IdFileStructOpen) then
            Veta1='Open : '//Veta1(:idel(Veta1))
            Veta2=' '
            Change=.true.
            call DeletePomFiles
          else if(k.eq.IdFileStructHistory) then
            call FeHistory(Veta2,Focus)
            if(ErrFlag.eq.0) then
              go to 1300
            else
              i=FeChDir(CurrentDirO)
              go to 1100
            endif
          else if(k.eq.IdFileStructSaveAs) then
            Veta1='Save as : '//Veta1(:idel(Veta1))
            Veta2=' '
            Change=.false.
          else if(k.eq.IdFileStructCopyIn) then
            Veta1='Copy in : '//Veta1(:idel(Veta1))
            Veta2=' '
            Change=.false.
          else if(k.eq.IdFileStructClose) then
            Veta2=' '
            call DeletePomFiles
            go to 1300
          else if(k.eq.IdFileStructLock) then
            if(StructureLocked) then
              Veta2='unlocked'
            else
              Veta2='locked'
            endif
            call ChangeUSDFile(fln,'*',Veta2)
            go to 1100
          endif
1210      call FeFileManager(Veta1,Veta2,' ',1,.not.Change,ich)
          if(ich.ne.0) then
            if(ifln.gt.0) call ChangeUSDFile(fln,'opened','*')
            go to 1100
          endif
          if(k.eq.IdFileStructSaveAs.or.k.eq.IdFileStructCopyIn) then
            if(k.eq.IdFileStructSaveAs) then
              idv2=idel(Veta2)
              if(StructureExists(Veta2)) then
                call ExtractFileName(Veta2,Veta1)
                if(.not.FeYesNo(-1.,-1.,'The structure "'//
     1             Veta2(:idv2)//'" already exists, rewrite it?',0))
     2             go to 1100
                call ChangeUSDFile(Veta2,'closed','*')
              endif
              do ii=1,9
                if(.not.EqIgCase(ExtMFile(ii),'.inflip')) then
                  if(ExistMFile(ii))
     1              call CopyFile(fln(:ifln)//
     2                        ExtMFile(ii)(:idel(ExtMFile(ii))),
     3                        Veta2(:idv2)//
     4                        ExtMFile(ii)(:idel(ExtMFile(ii))))
                endif
              enddo
              if(FeYesNo(-1.,-1.,'Do you want to continue with the new '
     1                 //'structure?',0)) then
                call ExtractDirectory(Veta2,Veta1)
                call ExtractFileName(Veta2,Veta2)
                call DeletePomFiles
                i=FeChdir(Veta1)
                call FeGetCurrentDir
                go to 1300
              endif
            else
              if(StructureExists(Veta2)) then
                if(ExistFile(Veta2(:idel(Veta2))//'.m50')) then
                  ln=NextLogicNumber()
                  call OpenFile(ln,Veta2(:idel(Veta2))//'.m50',
     1                          'formatted','old')
                  read(ln,FormA) t256
                  call CloseIfOpened(ln)
                  kk=0
                  call kus(t256,kk,Cislo)
                  if(.not.EqIgCase(Cislo,'version')) then
                    call FeFillTextInfo('copyin.txt',0)
                    call FeInfoOut(-1.,-1.,'INFORMATION','L')
                    go to 1100
                  endif
                endif
                NInfo=0
                idv2=idel(Veta2)
                do ii=1,8
                  if(.not.EqIgCase(ExtMFile(ii),'.inflip')) then
                    NInfo=NInfo+1
                    idl=idel(ExtMFile(ii))
                    TextInfo(NInfo)=ExtMFile(ii)(2:idl)//Tabulator
                    t256=Veta2(:idv2)//ExtMFile(ii)
                    n=0
                    if(ExistFile(t256)) then
                      Cislo='exists'
                      n=n+10
                    else
                      Cislo='empty'
                    endif
                    TextInfo(NInfo)=
     1                TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                Cislo(:idel(Cislo))//Tabulator
                    if(ExistMFile(ii)) then
                      Cislo='exists'
                      n=n+1
                    else
                      Cislo='empty'
                    endif
                    TextInfo(NInfo)=
     1                TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                Cislo(:idel(Cislo))//Tabulator
                    if(n.eq.1) then
                      Cislo='Delete it?'
                    else
                      Cislo='Overwrite it?'
                    endif
                    TextInfo(NInfo)=
     1                TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                Cislo(:idel(Cislo))
                    MFile(NInfo)=ii
                    if(n.eq.0) then
                      NInfo=NInfo-1
                    else if(n.eq.1.or.n.eq.10) then
                      MFile(NInfo)=MFile(NInfo)+10*n
                    endif
                  endif
                enddo
                UseTabsIn=UseTabs
                UseTabs=NextTabs()
                call FeTabsAdd( 60.,UseTabs,IdRightTab,' ')
                call FeTabsAdd(120.,UseTabs,IdRightTab,' ')
                call FeTabsAdd(180.,UseTabs,IdRightTab,' ')
                t256='File'//Tabulator//'Source'//Tabulator//'Target'
                id=NextQuestId()
                xqd=max(FeTxLengthUnder(TextInfo(1)),
     1                  FeTxLengthUnder(t256))+25.+CrwXd
                il=Ninfo+2
                call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,
     1                             0)
                tpom=5.
                xpom=tpom+FeTxLengthUnder(TextInfo(1))+15.
                il=1
                call FeQuestLblMake(id,tpom,il,t256,'L','N')
                call FeQuestLinkaMake(id,-10*il-4)
                do ii=1,NInfo
                  il=il+1
                  call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                                TextInfo(ii),'L',CrwXd,CrwYd,0,0)
                  if(ii.eq.1) nCrwFirst=CrwLastMade
                  call FeQuestCrwOpen(CrwLastMade,
     1              mod(MFile(ii),10).le.4.and.MFile(ii)/10.ne.1)
                enddo
                il=il+1
                t256='%All files'
                wpom=FeTxLengthUnder(t256)+20.
                xpom=(xqd-wpom)*.5
                call FeQuestButtonMake(id,xpom,il,wpom,ButYd,t256)
                nButtAll=ButtonLastMade
                call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1250            call FeQuestEvent(id,ich)
                if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAll)
     1            then
                  nCrw=nCrwFirst
                  do i=1,NInfo
                    call FeQuestCrwOn(nCrw)
                    nCrw=nCrw+1
                  enddo
                  call FeQuestButtonOff(nButtAll)
                  go to 1250
                else if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 1250
                endif
                if(ich.eq.0) then
                  nCrw=nCrwFirst
                  idp=idel(Veta2)
                  do ii=1,NInfo
                    if(CrwLogicQuest(nCrw)) then
                      Ext=ExtMFile(mod(MFile(ii),10))
                      if(MFile(ii)/10.eq.1) then
                        call DeleteFile(fln(:ifln)//Ext)
                      else
                        call CopyFile(Veta2(:idp)//Ext,fln(:ifln)//Ext)
                      endif
                    endif
                    nCrw=nCrw+1
                  enddo
                endif
                call FeQuestRemove(id)
                call FeTabsReset(UseTabs)
                UseTabs=UseTabsIn
              else
                call ExtractFileName(Veta2,Veta1)
                call FeChybne(-1.,-1.,'The structure "'//
     1                        Veta1(:idel(Veta1))//'" doesn''t exist.',
     2                        ' ',SeriousError)
              endif
            endif
            go to 1100
          endif
1300      if(StructureOpened(Veta2).and..not.Focus) then
            NInfo=2
            TextInfo(1)='The structure is either opened by another '//
     1                   'user/task'
            TextInfo(2)='or not correctly closed due to a previous '//
     1                  'crash.'
            if(.not.FeYesNoHeader(-1.,-1.,
     1                            'Do you want to continue anyhow?',0))
     2        then
              i=FeChDir(CurrentDirO)
              call FeGetCurrentDir
              go to 1100
            endif
          endif
          fln=Veta2
          ifln=idel(fln)
          call FeBottomInfo(' ')
          call DeletePomFiles
          call FortFilesClean
          if(k.eq.IdFileStructNew) then
            if(StructureExists(fln)) then
              if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1         '" already exists, continue anyhow?',0)) go to 1320
              do ii=1,8
                if(ExistMFile(ii)) then
                  call DeleteFile(fln(:ifln)//
     1                      ExtMFile(ii)(:idel(ExtMFile(ii))))
                endif
              enddo
              Format95(3:3)='8'
              if(ExistFile(fln(:ifln)//'.ubm').and.
     1           ExistFile(fln(:ifln)//'.mat'))
     2          call DeleteFile(fln(:ifln)//'.mat')
              if(ExistFile(fln(:ifln)//'.prf'))
     1          call DeleteFile(fln(:ifln)//'.prf')
            endif
            ExistMFile=.false.
            ExistPowder=.false.
            ExistSingle=.false.
            call SetBasicM40(.false.)
            call SetBasicM50
            call EM9ImportWizard(0)
          else if(k.eq.IdFileStructOpen) then
            if(.not.StructureExists(fln)) then
              ExistMFile=.false.
              ExistPowder=.false.
              ExistSingle=.false.
              call SetBasicM40(.false.)
              call SetBasicM50
              Grupa(KPhase)='P1'
              NGrupa(KPhase)=1
              CrSystem(KPhase)=1
              Lattice(KPhase)='P'
              NSymm(KPhase)=1
              NSymmN(KPhase)=1
              NDim(KPhase)=3
              NDimQ(KPhase)=9
              NDimI(KPhase)=0
              NComp(KPhase)=1
              NDatBlock=1
              call AllocateSymm(1,1,1,1)
              call UnitMat(rm6(1,1,1,KPhase),3)
              call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
              call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1                      symmc(1,1,1,KPhase),0)
              call iom50(1,0,fln(:ifln)//'.m50')
              ExistM50=.true.
              call iom40(1,0,fln(:ifln)//'.m40')
              ExistM40=.true.
              call EditM50(ich)
            endif
          endif
1320      KPhase=1
          KPhaseBasic=1
          KDatBlock=1
          if(Focus) then
            k=IdFileStructOpen
            go to 1200
          else
            if(ifln.gt.0) call ChangeUSDFile(fln,'opened','*')
            go to 1100
          endif
        else if(j.eq.IdFileImportModel) then
          if(k.eq.IdFileImportModelShelx) then
            call ImportModel(ImportShelx)
          else if(k.eq.IdFileImportModelCif) then
            call ImportModel(ImportCIF)
          else if(k.eq.IdFileImportModelAmplimodes) then
            call ImportAmplimodes(ich)
          else if(k.eq.IdFileImportModelWien) then
            call ImportWien(ich)
          else if(k.eq.IdFileImportModelOxana) then
            call ImportOxana(ich)
          endif
        else if(j.eq.IdFileRefl) then
          if(k.eq.IdFileReflModify) then
            call RewriteTitle('Import/modify M95')
            call EM9ImportWizard(1)
          else if(k.eq.IdFileReflSGTest) then
            call DRSGTest(1,Change)
            if(Change) then
              if(ExistM50.and.StatusM50.le.100.) then
                if(ExistSingle) then
                  if(FeYesNo(-1.,-1.,'Do you want to create '//
     1                       'refinement reflection file (m90)?',1))
     2              then
                    call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,
     1                                     .true.)
                    call EM9CreateM90(0,ich)
                  endif
                endif
              else
                if(FeYesNo(-1.,-1.,'Do you want to create or '//
     1                     'complete basic data file (m50)?',1))
     2            call Editm50(ich)
              endif
            endif
          else if(k.eq.IdFileReflCreate) then
            call RewriteTitle('Create M90')
            call EM9CreateM90(1,ich)
          else if(k.eq.IdFileReflManualCull) then
            call EM9ManualCull
          endif
        else if(j.eq.IdFileExit) then
          if(FeYesNo(-1.,-1.,'Do you really want to quit Jana2006?',0))
     1      then
            go to 9000
          else
            go to 1100
          endif
        endif
      else if(i.eq.IdEdit) then
        if(j.lt.IdEditSavedPoints) then
1500      if(j.eq.IdEditFile) then
            Veta1=' '
            call FeFileManager('Select file to be edited',Veta1,'*.*',0,
     1                         .true.,ich)
            if(ich.gt.0.or.Veta1.eq.' ') then
              go to 1100
            else if(ich.lt.0) then
              call FeChybne(-1.,-1.,'permission denied, you cannot '//
     1                     'edit the selected file, view mode '//
     2                     'activated.',' ',SeriousError)
            endif
          else if(j.eq.IdEditM40) then
            Veta1='.m40'
          else if(j.eq.IdEditM41) then
            Veta1='.m41'
          else if(j.eq.IdEditM42) then
            Veta1='.m42'
          else if(j.eq.IdEditM50) then
            Veta1='.m50'
          else if(j.eq.IdEditM90) then
            Veta1='.m90'
          else if(j.eq.IdEditM95) then
            Veta1='.m95'
          else if(j.eq.IdEditInFlip) then
            Veta1='.inflip'
          endif
          if(j.ne.IdEditFile) Veta1=fln(:ifln)//Veta1(:idel(Veta1))
          call FeEdit(Veta1)
          if(j.eq.IdEditFile) go to 1500
        else if(j.eq.IdEditSavedPoints) then
          call EditSavedPoints
        else
          if(j.eq.IdEditRef) then
            Veta1=fln(:ifln)//'.ref'
          else if(j.eq.IdEditFour) then
            Veta1=fln(:ifln)//'.fou'
          else if(j.eq.IdEditDist) then
            Veta1=fln(:ifln)//'.dis'
          else if(j.eq.IdEditRefRep) then
            Veta1=fln(:ifln)//'.rre'
          else if(j.eq.IdEditLFlip) then
            Veta1=fln(:ifln)//'.sflog'
          else if(j.eq.IdEditCP) then
            Veta1=fln(:ifln)//'.cp'
          else if(j.eq.IdEditInb) then
            Veta1=fln(:ifln)//'.inb'
          else if(j.eq.IdEditDirect) then
            Veta1=fln(:ifln)//'.dir'
          endif
          if(BuildInViewer) then
            call FeListView(Veta1,0)
          else
            call FeEdit(Veta1)
          endif
        endif
      else if(i.eq.IdRun) then
        call NewPg(1)
        if(j.eq.IdRunEditM50) then
          call RewriteTitle('Editm50')
          call Editm50(ich)
        else if(j.eq.IdRunRefine) then
          if((NAtCalc.gt.0.and.StatusM50.gt.0).or.StatusM50.ge.100) then
            Veta1=' '
            n=0
            do i=1,NPhase
              if(NAtFormula(i).le.0) then
                n=i
                exit
              endif
            enddo
            if(n.gt.0) then
              Veta1='The chemical formula'
              if(NPhase.gt.1) then
                write(Cislo,FormI15) n
                call Zhusti(Cislo)
                Veta1=Veta1(:idel(Veta1))//' for phase#'//
     1                Cislo(:idel(Cislo))
              endif
              Veta1=Veta1(:idel(Veta1))//' hasn''t been defined.'
            endif
            go to 8100
          endif
          call RewriteTitle('Refine')
          call Refine(0,RefineEnd)
        else if(j.eq.IdRunFourier) then
          call RewriteTitle('Fourier')
          call Fourier
        else if(j.eq.IdRunContour) then
          call RewriteTitle('Contour')
          call Contour
        else if(j.eq.IdRunDist) then
          call RewriteTitle('Dist')
          call Dist
        else if(j.eq.IdRunGrapht) then
          call RewriteTitle('Grapht')
          call Grapht
        else if(j.eq.IdRunSetCmd) then
          call SetCommands(k)
          if(StartProgram) then
            if(k.eq.1) then
              j=IdRunFourier
            else if(k.eq.2) then
              j=IdRunRefine
            else if(k.eq.3) then
              j=IdRunDist
            endif
            if(k.ge.1.and.k.le.3) then
              k=0
              go to 1130
            endif
          endif
        else if(j.eq.IdRunDirect) then
          call DirectMethods
        else if(j.eq.IdRunRandomModulations) then
          call CrlRandomModulations
        else if(j.eq.IdRunRandomMagnetic) then
          call CrlRandomMagnetic
        else if(j.eq.IdRunSolution) then
          call RunSolution
        endif
      else if(i.eq.IdWizard) then
        if(j.eq.IdWizardleBail) then
          if(ParentStructure) then
            FlnOld=Fln
            iflnOld=ifln
            Fln=Fln(:ifln)//'_tmp'
            iFln=idel(Fln)
            i=0
1600        if(StructureExists(Fln)) then
              i=i+1
              write(Cislo,'(i5)') i
              call Zhusti(Cislo)
              Fln=Fln(:iFln)//'_'//Cislo(:idel(Cislo))
              go to 1600
            endif
            iFln=idel(Fln)
            ParentStructureO=ParentStructure
            call DeleteFile(fln(:ifln)//'.m70')
            if(NDimI(KPhase).gt.0) then
              do i=1,NSymm(KPhase)
                call SetRealArrayTo(s6(1,i,1,KPhase),NDim(KPhase),0.)
              enddo
            endif
            KQMagOld=KQMag(KPhase)
            KQMag(KPhase)=0
            ParentStructure=.false.
            call iom50(1,0,fln(:ifln)//'.m50')
            call SetBasicM40(.true.)
            call iom40(1,0,fln(:ifln)//'.m40')
            call CopyFile(FlnOld(:iFlnOld)//'.m90',Fln(:ifln)//'.m90')
            call CopyFile(FlnOld(:iFlnOld)//'.m95',Fln(:ifln)//'.m95')
            call CopyFile(fln(:ifln)//'.m40',PreviousM40)
            call CopyFile(fln(:ifln)//'.m41',PreviousM41)
            call CopyFile(fln(:ifln)//'.m50',PreviousM50)
          else
            ParentStructureO=.false.
          endif
          call CrlLeBailMaster(ich)
          if(ParentStructureO) then
            if(ich.eq.0) then
              call iom50(0,0,flnOld(:iFlnOld)//'.m50')
              if(KQMagOld.gt.0) call CopyVek(QuPwd(1,1,KPhase),QMag,3)
              call CopyVek(CellPwd(1,KPhase),CellPar(1,1,KPhase),6)
              call iom50(1,0,flnOld(:iFlnOld)//'.m50')
              call iom41(1,0,flnOld(:iFlnOld)//'.m41')
              call MoveFile(Fln(:ifln)//'.m90',FlnOld(:iFlnOld)//'.m90')
              call MoveFile(Fln(:ifln)//'.m95',FlnOld(:iFlnOld)//'.m95')
              call MoveFile(fln(:ifln)//'.m70',flnOld(:iflnOld)//'.m70')
            endif
            call DeletePomFiles
            call DeleteAllFiles(Fln(:ifln)//'*')
            call iom50(0,0,flnOld(:iflnOld)//'.m50')
            call iom40(0,0,flnOld(:iflnOld)//'.m40')
            call iom90(0,flnOld(:iflnOld)//'.m90')
            call iom95(0,flnOld(:iflnOld)//'.m95')
            fln=FlnOld
            ifln=iflnOld
            go to 1100
          endif
        else if(j.eq.IdWizardRietveld) then
          call CrlRietveldMaster(ich)
        endif
      else if(i.eq.IdParam) then
        call EM40EditParameters(j,k)
      else if(i.eq.IdTools) then
        if(j.eq.IdToolsRecover) then
          call CrlRecoverParameterFiles
        else if(j.eq.IdToolsTrans) then
          if(k.eq.IdToolsTransCell) then
            call DRCellTrans
          else if(k.eq.IdToolsTransShift) then
            call OriginShift
          else if(k.eq.IdToolsTransSubGr) then
            call GoToSubGr
          else if(k.eq.IdToolsTransEnantiomorph) then
            call GoToEnantiomorph
          else if(k.eq.IdToolsTransModVec) then
            call DRQVecTrans
          else if(k.eq.IdToolsTransGoToSup) then
            call TrSuper(2,' ')
          else if(k.eq.IdToolsTransGoTo3d) then
            call GoTo3d(' ')
          else if(k.eq.IdToolsTransToX) then
            call TransToXCentr
          else if(k.eq.IdToolsTransFromAmplimodes) then
            call FromAmplimodes(' ')
          endif
        else if(j.eq.IdToolsSpecial) then
          if(k.eq.IdToolsSpecialTLS) then
            call TransTLS
          else if(k.eq.IdToolsSpecialTables) then
            call MakeTables
          else if(k.eq.IdToolsSpecial2dDisp) then
            call GraphMapa
          else if(k.eq.IdToolsSpecialPowSim) then
            call PwdSimulation
          else if(k.eq.IdToolsSpecialSimStr) then
            call CrlMakeSimStr
          else if(k.eq.IdToolsSpecialCorrSt) then
            call CrlCreateCorrectedStructure
          else if(k.eq.IdToolsSpecialMatCalc) then
            call MatrixCalculator(ich)
          else if(k.eq.IdToolsSpecialRepAnal) then
!            NInfo=3
!            TextInfo(1)='Do you want to continue with:'
!            TextInfo(2)='Representation analysis'
!            TextInfo(3)='Group-subgroup transformation'
!            if(FeSelectOnePossibility(-1.,-1.,1,1).eq.1) then
              call UseIrreps
!            else
!              call CrlMagGoToSubgroup
!            endif
          endif
        else if(j.eq.IdToolsPowder) then
          KDatBlockIn=KDatBlock
          if(.not.isPowder) then
            do KDatB=1,NDatBlock
              if(iabs(DataType(KDatB)).eq.2) then
                KDatBlock=KDatB
                isPowder=.true.
                exit
              endif
            enddo
          endif
          if(k.eq.IdToolsPowderPlot) then
            call PwdPrf(0)
          else if(k.eq.IdToolsPowderGnuPlot) then
            call PwdGnuplotPrf
          else if(k.eq.IdToolsPowderLeBail) then
            if(kcommenMax.gt.0) then
              call iom50(0,0,fln(:ifln)//'.m50')
              call ComSym(0,0,ich)
            endif
            call PwdLeBail(0,.true.)
            call FeFlowChartRemove
            if(ErrFlag.eq.0) then
              if(FeYesNo(-1.,-1.,
     1                   'Do you want to start "Profile viewer"?',0))
     2          call PwdPrf(0)
            endif
          else if(k.eq.IdToolsPowderSaveBkg) then
            call PwdSaveBackground
          else if(k.eq.IdToolsPowderExportIRF) then
            call PwdMakeIRFFile
          else if(k.eq.IdToolsPowderWHAnalysis) then
            call PwdWilliamsonHall
          endif
          KDatBlock=KDatBlockIn
          isPowder=iabs(DataType(KDatBlock)).eq.2
        else if(j.eq.IdToolsPhases) then
          if(k.eq.IdToolsPhasesNew) then
            call CreateNewPhase
          else if(k.eq.IdToolsPhasesDelete) then
            call DeletePhase
          else if(k.eq.IdToolsPhasesSwitch) then
            i=FeMenu(-1.,-1.,PhaseName,1,NPhase,1,0)
            if(i.gt.0) then
              KPhase=i
              KPhaseBasic=i
            endif
          endif
        else if(j.eq.IdToolsTplg) then
          if(k.eq.IdToolsTplgCP) then
            call TPCriticalPoints
          else if(k.eq.IdToolsTplgBasint) then
            call TPIntegration
          endif
        else if(j.eq.IdToolsGraph) then
          if(k.eq.IdToolsGraphViewer) then
            call CrlPlotStructure
          else if(k.eq.IdToolsGraphOutput) then
            if(NDimI(KPhase).eq.0) then
              call TrM4050(0,' ',' ',ich)
            else
              call GraphicOutputProlog
              call GraphicOutput(0,' ',ich)
            endif
          else if(k.eq.IdToolsGraphFoFcPlots) then
            call CrlFoFcPlots
          else if(k.eq.IdToolsGraphIndex) then
            call DRGrIndex
          else if(k.eq.IdToolsGraphCrShape) then
            call MorfCryst(1)
          endif
        else if(j.eq.IdToolsRecipW) then
          call DRRecSpaceViewer
        else if(j.eq.IdToolsMEM) then
          if(k.eq.IdToolsMEMBay) then
            Veta1=' '
7000        call MEMExport(Veta1)
            if(Veta1.ne.' ') go to 7000
          else if(k.eq.IdToolsMEMPrima) then
            call PrimaExport(0)
          else if(k.eq.IdToolsMEMDysnomia) then
            call PrimaExport(1)
          endif
        else if(j.eq.IdToolsPref) then
          call FePreferences(IGrOn)
          if(IGrOn.ne.1) then
            call OpenWorkSpace
            call FeMakeGrWin(0.,0.,YBottomMargin,0.)
            CurrentDir=CurrentDirO
            j=FeChdir(CurrentDir(:idel(CurrentDir)))
          endif
        else if(j.eq.IdToolsPrograms) then
          call FeSetPrograms
        else if(j.eq.IdToolsInternet) then
          if(k.eq.IdToolsInternetJana) then
            Veta1='http://jana.fzu.cz'
          else if(k.eq.IdToolsInternetBilbao) then
            Veta1='http://www.cryst.ehu.es/'
          else if(k.eq.IdToolsInternetStokes) then
            Veta1='http://stokes.byu.edu/isodistort.html'
          endif
          call FeShellExecute(Veta1)
        else if(j.eq.IdToolsAbout) then
          call AboutJana
        else if(j.eq.IdToolsAbs) then
          if(k.eq.IdToolsAbsPrvni) then
            call PrvniAbsCorr
          else if(k.eq.IdToolsAbsDruha) then
            call DruhaAbsCorr
          endif
        else if(j.eq.IdToolsICDD) then
          call ICDD
        else if(j.eq.IdToolsNase) then
          if(k.eq.IdToolsNaseKuma) then
            call RewriteTitle('KUMA')
            call KUMA
          else if(k.eq.IdToolsNaseHonza) then
            call RewriteTitle('Honza')
            call Honza
          else if(k.eq.IdToolsNaseTestDiff) then
            call DRTestDiffractometer
          else if(k.eq.IdToolsNaseIndexM95) then
            call DRKumaIndex(0)
          else if(k.eq.IdToolsNaseBruker) then
            call DRBruker
          else if(k.eq.IdToolsNaseNonius) then
            call DRNonius
          else if(k.eq.IdToolsNaseIndex) then
            call DRKumaIndex(4)
          else if(k.eq.IdToolsNaseAdHoc) then
            call CrlMakeDCRED
!            call JeffPhases
!            call OdJeffa
!            call Nasobky
!            call Preoprav
!            call NajdiTenzor
!            call PrevodHKL
!            call UdelejTabulku
!            call DPopul
!            call NajdiTenzorCrysalis
!            call NajdiTenzor
!            call TestOrtho2
!            call TestMatic
!            call NechciSatelity2
!            call PrevodReprezentaci
!            call PrevodNotarius
!            call XDRFac
!            call XDFF
!             call UdelejMatice
!            call TestFindSmb
!            call CislujCIF
!            call RFacTest
!            call TestPolynomials
!            call CrlSigCountSigStat
!            call TLSTest
!            call CrlIoSigIoPlots
!            call NactiXYZ
!            call PrevodHK6
!            call Posunt0
!            call PrepisHKL
!            call PrevodM90
!            call TestRef
!            call UpravStokes
!            call TestJacobi
!            call SimulacePatterson
!            call Karep
!            call UseIrreps
!            call TestGroupList
!            call MakeIdeal
          else if(k.eq.IdToolsNaseUpdateCifFile) then
            call UpDateCifFile
          else if(k.eq.IdToolsNaseTranslateCIF) then
            call TranslateCIF
          else if(k.eq.IdToolsNaseChangeOrderM90) then
            call ChangeOrderM90
          else if(k.eq.IdToolsNaseNormProbPlot) then
            call CrlNormalProbabilityPlot
          else if(k.eq.IdToolsSymmDiffTable) then
            call TablePatt
          endif
        endif
      endif
      call CloseAllFiles
      call FeDeferOutput
      go to 1100
8000  call FeChybne(-1.,-1.,'the M50 file doesn''t exist.',' ',
     1              SeriousError)
      go to 1100
8100  call FeChybne(-1.,-1.,'the M50 file doesn''t contain full '//
     1              'information to run this program.',Veta1,
     2              SeriousError)
      go to 1100
9000  call DeletePomFiles
      call FeTmpFilesDelete
      call FeGrQuit
      stop
100   format(i10)
      end
      subroutine CoDal(Item,WhatToDo1,WhatToDo2)
      use Atoms_mod
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'main.cmn'
      common/subrol/ SubState,xsub,ysub,xdsub,ydsub
      dimension ToolBarX(7)
      character*1  Znak
      character*6  ToolBarZ
      character*8  ext(20)
      character*12 MenuToolBar(6)
      character*20 RolZ(6),SubZ(20,6)
      character*50 RolM(20,6),SubM(20,20,6),t50,p50
      character*80 Veta
      integer WhatToDo1,WhatToDo1P,WhatToDo2,SubState,UseTabsIn,RolN(6),
     1        SubN(20,6),StateToolBarButton(6),Time,FeGetSystemTime,
     2        TimeNew,CrlCentroSymm,CtrlAction(13)
      logical HuraNaTo,RoletaOn,ExistFile,RolA(20,6),SubA(20,20,6),
     1        FeTestIn,NatahniRoletu,MysZaToMuze,EqIgProcenta,Change,
     2        PwdCheckTOFInD,EqRV0,StructureExists,ExistFaces
      real KartPhaseXMin(:),KartPhaseXMax(:),KartPhaseYMin,KartPhaseYMax
      allocatable KartPhaseXMin,KartPhaseXMax
      data MenuToolBar/'%File','%Edit/View','%Run','%Wizards',
     1                 '%Parameters','%Tools'/,StateToolBarButton/6*0/
      data RoletaOn/.false./
      data (RolM(j,1),j=1,20)/'Start s%hell',
     1                        '%Export structure to',
     2                        '%CIF utilities',
     3                        '%Structure',
     4                        '%Import model from',
     5                        '%Reflection file',
     6                        'E%xit',
     7                        13*' '/
      data SubM/2400*' '/
      data (RolM(j,2),j=1,20)/'Editing of f%ile',
     1                        'Editing of m%40 file',
     2                        'Editing of m4%1 file',
     3                        'Editing of m4%2 file',
     4                        'Editing of m%50 file',
     5                        'Editing of m9%0 file',
     6                        'Editing of m%95 file',
     7                        'Editing of inf%lip',
     8                        'Editing of saved p%oints',
     9                        'View of %Refine',
     a                        'View of %Fourier',
     1                        'View of %Dist',
     2                        'View of Dire%ct',
     3                        'View of R%eflection report',
     4                        'View of %Superflip output',
     5                        'View of C%P report',
     6                        'View of In%b report',3*' '/
      data (RolM(j,3),j=1,20)/7*' ','S%olution',12*' '/
      data (RolM(j,4),j=1,20)/'le %Bail wizard','%Rietveld wizard',
     1                        18*' '/
      data (RolM(j,5),j=1,20)/'%Options','%Scale','%Twin fractions',
     1                        '%Extinction','%f'',f"','%Powder',
     2                        '%Atoms','%Molecules',
     3                        'Ma%gnetic moments in polar coordinates',
     4                        'Ele%ctron diffraction',10*' '/
      data (RolM(j,6),j=1,20)/'R%ecover files',
     1                        '%Transformations',
     2                        '%Special tools',
     3                        'Po%wder',
     4                        'P%hases',
     5                        'T%opological analysis',
     6                        '%Graphic',
     7                        '%Reciprocal space viewer',
     8                        'Files for %MEM',
     9                        'Pre%ferences',
     a                        '%Programs',
     1                        '%Connect through internet to',
     2                        '%About Jana2006',7*' '/
      data ext/' ','.m40','.m41','.m42','.m50','.m90','.m95','.inflip',
     1         ' ','.ref','.fou','.dis','.dir','.rre','.sflog','.cp',
     2           '.inb',3*' '/
      AllowResizing=.true.
      call TestUzavreni
      call JanaReset(Change)
      allocate(KartPhaseXMin(NPhase),KartPhaseXMax(NPhase))
      if(Change) go to 9000
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(120.,UseTabs,IdRightTab,' ')
      RolM( 1,IdRun)='%EditM50           '//Tabulator//'CtrlE'
      CtrlAction(1)=IconEditM50
      RolM( 2,IdRun)='Edit %atoms        '//Tabulator//'CtrlA'
      CtrlAction(2)=IconEditAtoms
      RolM( 3,IdRun)='Edit %profile      '//Tabulator//'CtrlP'
      CtrlAction(3)=IconEditProfile
      RolM( 4,IdRun)='Structure sol%ution'//Tabulator//'CtrlU'
      CtrlAction(4)=IconStructureSolution
      RolM( 5,IdRun)='%Fourier           '//Tabulator//'CtrlF'
      CtrlAction(5)=IconFourier
      RolM( 6,IdRun)='%Contour           '//Tabulator//'CtrlC'
      CtrlAction(6)=IconContour
      RolM( 7,IdRun)='%Refine            '//Tabulator//'CtrlR'
      CtrlAction(7)=IconRefine
      RolM( 8,IdRun)='%Dist              '//Tabulator//'CtrlD'
      CtrlAction(8)=IconDist
      RolM( 9,IdRun)='%Matrix calculator '//Tabulator//'CtrlM'
      CtrlAction(9)=IconMatrixCalculator
      RolM(10,IdRun)='Pl%ot structure    '//Tabulator//'CtrlO'
      CtrlAction(10)=IconPlotStructure
      RolM(11,IdRun)='Profile vie%wer    '//Tabulator//'CtrlW'
      CtrlAction(11)=IconProfileViewer
      RolM(12,IdRun)='%Grapht            '//Tabulator//'CtrlG'
      CtrlAction(12)=IconGraphT
      RolM(13,IdRun)='%SetCommands       '//Tabulator//'CtrlS'
      CtrlAction(13)=IconSetCommands
      RolM(IdRunDirect,IdRun)='%Intensity statistics'
      RolM(IdRunRandomModulations,IdRun)=
     1  'Ra%ndom search for modulation amplitudes'
      RolM(IdRunRandomMagnetic,IdRun)=
     1  'Random searc%h for magnetic moments'
      i=IdToolsAbout
      if(ICDDKey.eq.150548) then
        i=i+1
        IdToolsICDD=i
        RolM(IdToolsICDD,IdTools)='%ICDD tester'
      else
        IdToolsICDD=0
      endif
      if(VasekAbs.gt.0) then
        i=i+1
        IdToolsAbs=i
        RolM(IdToolsAbs,IdTools)='Absor%ption'
        SubM(1,IdToolsAbs,IdTools)='%Prvni AbsCorr'
        SubM(2,IdToolsAbs,IdTools)='%Druha AbsCorr'
      else
        IdToolsAbs=0
      endif
      i=i+1
      IdToolsNase=i
      if(VasekTest.ne.0) then
        RolM(IdToolsNase,IdTools)='O%ur specialities'
        SubM(IdToolsNaseKuma,IdToolsNase,IdTools)=
     1    '%KUMA procedures'
        SubM(IdToolsNaseBruker,IdToolsNase,IdTools)=
     1    '%Bruker procedures'
        SubM(IdToolsNaseNonius,IdToolsNase,IdTools)=
     1    '%Nonius procedures'
        SubM(IdToolsNaseIndex,IdToolsNase,IdTools)=
     1    '%Indexing'
        SubM(IdToolsNaseHonza,IdToolsNase,IdTools)=
     1    'CIF for Jan %Fabry'
        SubM(IdToolsNaseIndexM95,IdToolsNase,IdTools)=
     1    '%Index M9 file'
        SubM(IdToolsNaseTestDiff,IdToolsNase,IdTools)=
     1    '%Test of diffractometer'
        SubM(IdToolsNaseAdHoc,IdToolsNase,IdTools)=
     1    '%Ad-hoc'
        SubM(IdToolsNaseUpDateCifFile,IdToolsNase,IdTools)=
     1    '%Up date of CIF dictionary'
        SubM(IdToolsNaseTranslateCif,IdToolsNase,IdTools)=
     1    'T%ranslate CIF to Jana code and vise versa'
        SubM(IdToolsNaseChangeOrderM90,IdToolsNase,IdTools)=
     1    'Order M%90 by sin(th)/lambda'
        SubM(IdToolsNaseNormProbPlot,IdToolsNase,IdTools)=
     1    'Normal probability plot'
        SubM(IdToolsSymmDiffTable,IdToolsNase,IdTools)=
     1    'Make table of symmetry differences'
      endif
1100  if(isPowder) then
        RolM(IdParamTwin,IdParam)='P%hase fractions'
      else
        RolM(IdParamTwin,IdParam)='%Twin fractions'
      endif
      SubM(IdFileExportShelx,IdFileExport,IdFile)=
     1  '%SHELX'
      SubM(IdFileExportJana2000,IdFileExport,IdFile)=
     1  '%Jana2000'
      SubM(IdFileExportXD,IdFileExport,IdFile)=
     1  '%XD - only relection file'
      SubM(IdFileExportHRTEM,IdFileExport,IdFile)=
     1  '%HRTEM'
      SubM(IdFileExportDMol3,IdFileExport,IdFile)=
     1  '%DMol3'
      SubM(IdFileStructNew   ,IdFileStruct,IdFile)=
     1  '%New'
      SubM(IdFileStructOpen   ,IdFileStruct,IdFile)=
     1  '%Open'
      SubM(IdFileStructHistory,IdFileStruct,IdFile)=
     1  '%History'
      SubM(IdFileStructSaveAs ,IdFileStruct,IdFile)=
     1  '%Save as'
      SubM(IdFileStructCopyIn ,IdFileStruct,IdFile)=
     1  '%Copy in'
      SubM(IdFileStructClose  ,IdFileStruct,IdFile)=
     1  'Clos%e'
      if(StructureLocked) then
        SubM(IdFileStructLock   ,IdFileStruct,IdFile)=
     1    'Un%lock'
      else
        SubM(IdFileStructLock   ,IdFileStruct,IdFile)=
     1    '%Lock'
      endif
      SubM(IdFileImportModelShelx,IdFileImportModel,IdFile)=
     1  '%SHELX'
      SubM(IdFileImportModelCif,IdFileImportModel,IdFile)=
     1  '%CIF'
      SubM(IdFileImportModelAmplimodes,IdFileImportModel,IdFile)=
     1  '%Amplimodes'
      if(VasekTest.ne.0) then
        SubM(IdFileImportModelWien,IdFileImportModel,IdFile)=
     1   '%Wien'
        SubM(IdFileImportModelOxana,IdFileImportModel,IdFile)=
     1   '%Oxana'
      endif
      SubM(IdFileCIFMake   ,IdFileCIF,IdFile)=
     1    '%Make CIF file'
      SubM(IdFileCIFUpdate ,IdFileCIF,IdFile)=
     1    '%Update CIF file'
      if(VasekTest.ne.0)
     1  SubM(IdFileCIFEdit   ,IdFileCIF,IdFile)=
     2    '%Edit CIF file'
      SubM(IdFileReflModify   ,IdFileRefl,IdFile)=
     1  '%Import/modify the reflection file'
      SubM(IdFileReflSGTest  ,IdFileRefl,IdFile)=
     1  '%Make space group test'
      SubM(IdFileReflCreate   ,IdFileRefl,IdFile)=
     1  '%Create refinement reflection file'
      SubM(IdFileReflManualCull,IdFileRefl,IdFile)=
     1  '%Handle culling manually'
      SubM(IdToolsTransCell   ,IdToolsTrans,IdTools)=
     1  '%Cell transformation'
      SubM(IdToolsTransModVec ,IdToolsTrans,IdTools)=
     1  'Change %modulation vector'
      SubM(IdToolsTransShift  ,IdToolsTrans,IdTools)=
     1  '%Origin shift'
      SubM(IdToolsTransEnantiomorph,IdToolsTrans,IdTools)=
     1  'Change %enantiomorph'
      SubM(IdToolsTransSubGr  ,IdToolsTrans,IdTools)=
     1  '%Go to subgroup structure'
      SubM(IdToolsTransGoToSup,IdToolsTrans,IdTools)=
     1  'Go to %supercell structure'
      SubM(IdToolsTransGoTo3d ,IdToolsTrans,IdTools)=
     1  'Go to average %3d structure'
      SubM(IdToolsTransToX    ,IdToolsTrans,IdTools)=
     1  'Transform to the X-centered cell'
      SubM(IdToolsTransFromAmplimodes,IdToolsTrans,IdTools)=
     1  'Transform from amplimodes'
      SubM(IdToolsSpecialTLS   ,IdToolsSpecial,IdTools)=
     1  'Transform %TLS'
      SubM(IdToolsSpecialTables,IdToolsSpecial,IdTools)=
     1  'T%ables for publication'
      SubM(IdToolsSpecial2dDisp,IdToolsSpecial,IdTools)=
     1  '%2d displacement map'
      SubM(IdToolsSpecialPowSim,IdToolsSpecial,IdTools)=
     1    'Simulation of %powder structure'
      SubM(IdToolsSpecialSimStr,IdToolsSpecial,IdTools)=
     1    'Simulation of %single crystal data'
      SubM(IdToolsSpecialCorrSt,IdToolsSpecial,IdTools)=
     1    '%Creation of "corrected" structure'
      SubM(IdToolsSpecialMatCalc,IdToolsSpecial,IdTools)=
     1    '%Matrix calculator'
      SubM(IdToolsSpecialRepAnal,IdToolsSpecial,IdTools)=
     1    '%Representation analysis for magnetic structures'
      SubM(IdToolsPhasesNew   ,IdToolsPhases,IdTools)=
     1  '%New phase'
      SubM(IdToolsPhasesDelete,IdToolsPhases,IdTools)=
     1  '%Delete phase'
      SubM(IdToolsPhasesSwitch,IdToolsPhases,IdTools)=
     1  '%Switch phase'
      SubM(IdToolsPowderPlot,IdToolsPowder,IdTools)=
     1  '%Profile viewer'
      SubM(IdToolsPowderGnuPlot,IdToolsPowder,IdTools)=
     1  '%Make eps/png profiles'
      SubM(IdToolsPowderLeBail,IdToolsPowder,IdTools)=
     1  'Make Le%Bail'
      SubM(IdToolsPowderSaveBkg,IdToolsPowder,IdTools)=
     1  '%Save background'
      SubM(IdToolsPowderExportIRF,IdToolsPowder,IdTools)=
     1  'Export %IRF file'
      SubM(IdToolsPowderWHAnalysis,IdToolsPowder,IdTools)=
     1  'Make %Williamson-Hall analysis'
      SubM(IdToolsGraphViewer,IdToolsGraph,IdTools)=
     1  '%Run viewer'
      SubM(IdToolsGraphOutput,IdToolsGraph,IdTools)=
     1  '%Create graphic output file'
      SubM(IdToolsGraphFoFcPlots,IdToolsGraph,IdTools)=
     1    '%Draw Fcalc/Fobs plot'
      SubM(IdToolsPhasesDelete,IdToolsPhases,IdTools)=
     1  '%Delete phase'
      SubM(IdToolsInternetJana,IdToolsInternet,IdTools)=
     1  '%Jana home page'
      SubM(IdToolsInternetBilbao,IdToolsInternet,IdTools)=
     1  '%Bilbao Crystallographic Server'
      SubM(IdToolsInternetStokes,IdToolsInternet,IdTools)=
     1  '%Stokes, Campbell & Hatch ISODISTORT'
      SubM(IdToolsMEMBay,IdToolsMEM,IdTools)=
     1  '%BayMEM'
      SubM(IdToolsMEMPrima,IdToolsMEM,IdTools)=
     1  '%Prima'
      SubM(IdToolsMEMDysnomia,IdToolsMEM,IdTools)=
     1  '%Dysnomia'
      SubM(IdToolsTplgCP,IdToolsTplg,IdTools)='%Find critical points'
      SubM(IdToolsTplgBasint,IdToolsTplg,IdTools)='%Basin integration'
      SubM(IdParamAtomsNew,IdParamAtoms,IdParam)='%New'
      SubM(IdParamAtomsEdit,IdParamAtoms,IdParam)='%Edit'
      SubM(IdParamMolecNew,IdParamMolec,IdParam)='%New molecule'
      SubM(IdParamMolecPosNew,IdParamMolec,IdParam)='New %position'
      SubM(IdParamMolecTrans,IdParamMolec,IdParam)='%Transform'
      SubM(IdParamMolecExpand,IdParamMolec,IdParam)='E%xpand'
      SubM(IdParamMolecEdit,IdParamMolec,IdParam)='%Edit'
      SubM(IdToolsGraphIndex,IdToolsGraph,IdTools)=
     1    '%Indexing'
      SubM(IdToolsGraphCrShape,IdToolsGraph,IdTools)=
     1    '%Crystal faces'
      ExistFaces=.false.
      if(ExistM95) then
        do i=1,NRefBlock
          if(NFaces(i).gt.0) then
            ExistFaces=.true.
            exit
          endif
        enddo
      endif
      KPhaseOld=-1
      if(OpSystem.le.0) then
        DelayForSubMenu=.15
      else
        DelayForSubMenu=.05
      endif
1150  HuraNaTo=.false.
      call SetIcons(RolM(1,3),CtrlAction)
      do 1400i=1,6
        l=0
        do 1300j=1,20
          call SetLogicalArrayTo(SubA(1,j,i),20,.false.)
          if(RolM(j,i).eq.' ') then
            RolN(i)=l
            go to 1400
          endif
          l=l+1
          RolA(j,i)=.false.
          if(i.eq.IdFile) then
            if((j.eq.IdFileDOS).or.
     1         (j.eq.IdFileExport.and.
     2           (ExistM50.or.ExistM90.or.ExistM95)).or.
     3         (j.eq.IdFileStruct).or.
     4         (j.eq.IdFileRefl.and..not.StructureLocked.and.fln.ne.' ')
     5           .or.
     6         (j.eq.IdFileCIF.and.
     7          (fln.ne.' '.and.(NAtCalc.gt.0.or.
     8           MaxNAtFormula.gt.0))).or.
     9         (j.eq.IdFileImportModel.and..not.StructureLocked.and.
     a          StructureExists(fln)).or.
     1         (j.eq.IdFileExit)) then
              go to 1270
            else
              go to 1275
            endif
          else if(i.eq.IdEdit) then
            if(j.eq.IdEditFile) then
              go to 1270
            else
              if(ext(j).ne.' ') then
                if(.not.ExistFile(fln(:ifln)//ext(j)).or.
     1             (j.le.6.and.StructureLocked)) go to 1275
              endif
            endif
          else if(i.eq.IdRun) then
            if(j.eq.IdRunDirect) then
              if(.not.ExistM90.or.ParentStructure) go to 1275
            else if(j.eq.IdRunRandomModulations) then
              if(.not.ExistM90.or.ParentStructure.or.NDimI(KPhase).le.0)
     1          go to 1275
            else if(j.eq.IdRunRandomMagnetic) then
              if(.not.ExistM90.or.ParentStructure.or.
     1           MagneticType(KPhase).le.0.or.MagParMax.le.0)
     2          go to 1275
            endif
          else if(i.eq.IdWizard) then
            if(.not.isPowder) go to 1275
            if(j.eq.IdWizardRietveld.and.ParentStructure) go to 1275
          else if(i.eq.IdParam) then
            if((j.eq.IdParamTwin.and.(NTwin.le.1.and.NPhase.le.1)).or.
     1         (j.eq.IdParamPowder.and..not.ExistPowder).or.
     2         (j.eq.IdParamExt.and..not.ExistSingle).or.
     3         (j.eq.IdParamMagPolar.and.MagneticType(KPhase).le.0).or.
     4         (j.eq.IdParamElDiff.and.
     5          Radiation(1).ne.ElectronRadiation)) go to 1275
          else if(i.eq.IdTools) then
            if(j.eq.IdToolsRecover) then
              if(.not.ExistFile(fln(:ifln)//'.s40').or.
     1           StructureLocked.or.ParentStructure) go to 1275
            else if(j.eq.IdToolsPowder) then
              if(.not.ExistPowder) go to 1275
            else if(j.eq.IdToolsPhases) then
              if(StructureLocked.or.ParentStructure) go to 1275
            else if(j.eq.IdToolsTplg) then
              if(.not.ChargeDensities) go to 1275
            else if(j.eq.IdToolsRecipW) then
              if(isPowder.or.(.not.ExistM95.and..not.
     1           ExistFile(fln(:ifln)//'.m80'))) go to 1275
            endif
          endif
1270      RolA(j,i)=.true.
1275      n=0
          do 1280k=1,20
            if(SubM(k,j,i).eq.' ') then
              SubN(j,i)=n
              go to 1300
            endif
            n=n+1
            if(.not.RolA(j,i)) go to 1280
            if(i.eq.IdFile) then
              if(j.eq.IdFileExport) then
                if((k.eq.IdFileExportShelx.and.NDim(KPhase).gt.3).or.
     1             (k.eq.IdFileExportHRTEM.and..not.ExistM50))
     2          go to 1280
              else if(j.eq.IdFileStruct) then
                if((k.eq.IdFileStructHistory.and.
     1              .not.ExistFile(HistoryFile)).or.
     2             (k.eq.IdFileStructSaveAs.and..not.ExistM50.and.
     3                                     .not.ExistM95).or.
     4             (k.eq.IdFileStructCopyIn.and.
     5              (StructureLocked.or.fln.eq.' ')).or.
     6             (k.eq.IdFileStructClose.and.fln.eq.' ').or.
     7             (k.eq.IdFileStructLock.and.fln.eq.' '))
     8          go to 1280
              else if(j.eq.IdFileImportModel) then
               if(StructureLocked.or..not.StructureExists(fln))
     1           go to 1280
              else if(j.eq.IdFileRefl) then
                if(StructureLocked) go to 1280
                if((k.eq.IdFileReflModify.and..not.ExistM95.and.
     1              .not.ExistM50).or.
     2             (k.eq.IdFileReflSGTest.and.
     3              ((     isPowder.and..not.ExistM90).or.
     4               (.not.isPowder.and..not.ExistM95))).or.
     5             ((k.eq.IdFileReflCreate.or.k.eq.IdFileReflManualCull)
     6               .and.(.not.ExistM95.or..not.ExistM50)).or.
     7             (k.eq.IdFileReflManualCull.and..not.ExistSingle))
     8               go to 1280
              endif
            else if(i.eq.IdParam) then
              if(j.eq.IdParamAtoms) then
                if((k.eq.IdParamAtomsEdit.and.(NAtCalc.le.0.or.
     1              NAtFormula(KPhase).le.0)).or.
     2             (k.eq.IdParamAtomsNew.and.
     3              (NAtFormula(KPhase).le.0.or.StructureLocked)))
     4            go to 1280
              else if(j.eq.IdParamMolec) then
                if((k.ne.IdParamMolecEdit.and.StructureLocked).or.
     1             (k.ne.IdParamMolecNew.and.NMolecLenAll(KPhase).le.0))
     2            go to 1280
              endif
            else if(i.eq.IdTools) then
              if(j.eq.IdToolsTrans) then
                if(.not.ExistM50.or.
     1             (k.eq.IdToolsTransGoToSup.and.
     2              kcommen(KPhase).le.0).or.
     3             ((k.eq.IdToolsTransModVec.or.
     4               k.eq.IdToolsTransGoTo3d).and.NDim(KPhase).le.3).or.
     5              (k.eq.IdToolsTransGoTo3d.and.NComp(KPhase).gt.1).or.
     6              (k.eq.IdToolsTransFromAmplimodes.and.
     7               NAtXYZMode.le.0).or.
     8             ((k.eq.IdToolsTransModVec.or.
     9               k.eq.IdToolsTransCell).and.StructureLocked).or.
     a             (k.eq.IdToolsTransEnantiomorph.and.
     1              CrlCentroSymm().gt.0)) go to 1280
                if(k.eq.IdToolsTransToX) then
                  if(NDimI(KPhase).ne.1) then
                    go to 1280
                  else
                    if(EqRV0(quir(1,1,KPhase),3,.0001)) go to 1280
                  endif
                else if(k.eq.IdToolsTransSubGr) then
                  if(NSymm(KPhase)*NLattVec(KPhase).le.1) go to 1280
                endif
              else if(j.eq.IdToolsSpecial) then
                if(k.eq.IdToolsSpecialTLS) then
                  do m=1,NMolec
                    if(ktls(m).gt.0) go to 1278
                  enddo
                  go to 1280
                else if(k.eq.IdToolsSpecial2dDisp) then
                  if(NDim(KPhase).ne.4) go to 1280
                else if(k.eq.IdToolsSpecialSimStr) then
                  if(.not.ExistM40.or..not.ExistM50.or.NAtCalc.le.0)
     1              go to 1280
                else if(k.eq.IdToolsSpecialRepAnal) then
                  if(MagneticType(KPhase).le.0.or.
     1               Radiation(KDatBlock).ne.NeutronRadiation)
     2              go to 1280
                else if(k.eq.IdToolsSpecialCorrSt) then
                  if(.not.ExistFile(fln(:ifln)//'.m83').or.
     1               .not.ExistFile(fln(:ifln)//'.m90').or.
     2               NPhase.gt.1.or.NRefBlock.gt.1.or.isPowder)
     3              go to 1280
                endif
              else if(j.eq.IdToolsPowder) then
                if(.not.ExistPowder) go to 1280
                if(k.eq.IdToolsPowderGnuPlot) then
                  if(.not.ExistFile(fln(:ifln)//'.prf').or.
     1               CallGnuplot.eq.' '.or.CallGS.eq.' ') go to 1280
                else if(k.eq.IdToolsPowderExportIRF) then
                  do KDatB=1,NDatBlock
                    if(DataType(KDatB).ne.-2.or.
     1                 Radiation(KDatB).ne.NeutronRadiation) go to 1280
                  enddo
                endif
                if(.not.ExistFile(fln(:ifln)//'.prf').and.
     1             k.eq.IdToolsPowderWHAnalysis) go to 1280
              else if(j.eq.IdToolsGraph) then
                if(k.eq.IdToolsGraphViewer) then
                  if(OpSystem.eq.1.or.CallGraphic.eq.' '.or.
     1               NAtCalc.le.0) go to 1280
                else if(k.eq.IdToolsGraphFoFcPlots) then
                  if(.not.ExistFile(fln(:ifln)//'.m83')) go to 1280
                else if(k.eq.IdToolsGraphCrShape) then
                  if(.not.ExistFaces) go to 1280
                endif
              else if(j.eq.IdToolsPhases) then
                if(((k.eq.IdToolsPhasesDelete.or.
     1              k.eq.IdToolsPhasesSwitch).and.NPhase.le.1).or.
     2             StructureLocked) go to 1280
              else if(j.eq.IdToolsNase) then
                if(k.eq.IdToolsNaseChangeOrderM90.and.
     1             (.not.ExistM90.or..not.ExistSingle)) go to 1280
              endif
            endif
1278        SubA(k,j,i)=.true.
1280      continue
          SubN(j,i)=20
1300    continue
        RolN(i)=20
1400  continue
      do i=1,13
        RolA(i,3)=LIcon(CtrlAction(i))
      enddo
1500  xpom=XMaxBasWin-300.
      ypom=YMaxBasWin-100.
      do i=1,NIconUsed
        j=IconOrder(i)
        call FeIconOpen(i,xpom,ypom,IconXLength/EnlargeFactor,
     1                  IconYLength/EnlargeFactor,
     2                  IconLabelAct(j),IconFileAct(j))
        if(.not.LIcon(j)) call FeIconDisable(i)
        if(i.eq.NIconUsed-1) then
          xpom= 50.
          ypom=100.
        else if(mod(i,3).eq.0) then
          ypom=ypom-100.
          xpom=XMaxBasWin-300.
        else
          xpom=xpom+100.
        endif
      enddo
      RolZ(3)=' '
      do i=1,13
        j=index(RolM(i,3),'%')
        if(j.ne.0) RolZ(3)(i:i)=RolM(i,3)(j+1:j+1)
      enddo
      call mala(RolZ(3))
      xpom=15.
      KartPhaseYMin=YMinGrWin
      KartPhaseYMax=YMinGrWin+20.
      do i=1,NPhase
        KartPhaseXMin(i)=xpom
        xp=FeTxLength(PhaseName(i))+15.
        xpom=xpom+xp
        KartPhaseXMax(i)=xpom-1.
      enddo
      yd=20.
      call FeFillRectangle(XMinBasWin,XMaxBasWin,YMaxBasWin,
     1                     YMaxBasWin-yd,4,0,0,LightGray)
      yp=YMaxBasWin-yd
      ypd=YMaxBasWin
      xp=XMinBasWin+10.
      ypom=(yp+ypd)*.5
      xpom=xp
      do i=1,6
        ToolBarX(i)=xpom
        xpom=xpom+10.
        call FeWrMenuItem(xpom,ypom,-1.,MenuToolBar(i),
     1                    ToolBarZ(i:i),
     1                    Black,0)
        xpom=xpom+FeTxLengthUnder(MenuToolBar(i))+10.
      enddo
      ToolBarX(7)=xpom
      if(NDatBlock.gt.1) then
        xpom=XMinBasWin+15.
        ypom=YMaxBasWin-50.
        do i=1,NDatBlock
          CrwExGr(i)=1
          CrwType(i)=CrwTypeRadio
          call FeCrwMake(i,0,xpom,ypom,CrwgXd,CrwgYd)
          call FeCrwOpen(i,i.eq.KDatBlock)
          Veta=DatBlockName(i)
          call FeOutSt(0,xpom+CrwgXd+10.,ypom+5.,MenuDatBlock(i),'L',
     1                 White)
          ypom=ypom-CrwgYd-10.
        enddo
      endif
3000  DoubleClickAllowed=.true.
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      call FeMouseShape(0)
      Item=0
      WhatToDo1=0
      WhatToDo1P=0
      WhatToDo2=0
      ItemNew=0
      NatahniRoletu=.false.
      MysZaToMuze=.false.
3100  VolaToCoDal=.true.
3105  if(KPhase.ne.KPhaseOld.and.NPhase.gt.1) then
        call FeDeferOutput
        call FeClearGrWin
        do i=1,NPhase
          if(i.ne.KPhase) then
            call FeDrawSwitch(KartPhaseXMin(i),KartPhaseXMax(i),
     1                        KartPhaseYMin,KartPhaseYMax,2)
            call FeOutSt(0,
     1        KartPhaseXMin(i)+.5*(KartPhaseXMax(i)-KartPhaseXMin(i)),
     2        KartPhaseYMin+.5*(KartPhaseYMax-KartPhaseYMin),
     3        PhaseName(i),'C',Gray)
          endif
        enddo
        call FeDrawSwitch(KartPhaseXMin(KPhase)-3.,
     1                    KartPhaseXMax(KPhase)+3.,
     2                    KartPhaseYMin,KartPhaseYMax+3.,2)
        call FeOutSt(0,KartPhaseXMin(KPhase)+
     1               .5*(KartPhaseXMax(KPhase)-KartPhaseXMin(KPhase)),
     2               KartPhaseYMin+.5*(KartPhaseYMax-KartPhaseYMin)+
     3               2.,PhaseName(KPhase),'C',Black)
        KPhaseOld=KPhase
        go to 1150
      endif
3110  call FeEvent(0)
3120  VolaToCoDal=.false.
      if(EventType.eq.EventIcon.and..not.RoletaOn) then
        call FeIconOn(EventNumber)
        if(WhatToDo1P.ne.EventNumber.and.WhatToDo1P.gt.0)
     1    call FeIconOff(WhatToDo1P)
        WhatToDo1P=EventNumber
        ItemNew=0
        go to 4010
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1  then
        WhatToDo2=0
        if(FeTestIn()) then
          if(EventType.eq.EventIcon) then
            j=IconOrder(EventNumber)
            Item=IconAction(4,j)
            WhatToDo1=IconAction(5,j)
            WhatToDo2=IconAction(6,j)
          endif
        endif
        if(WhatToDo2.ne.0) then
          HuraNaTo=.true.
        else
          go to 3100
        endif
      else if(EventType.eq.EventMouse.and.(EventNumber.eq.JeLeftDown.or.
     1        EventNumber.eq.JeLeftUp.or.EventNumber.eq.JeMove)) then
        if(EventNumber.eq.JeMove.and.
     1    SubState.ne.0.and.WhatToDo2.eq.0) then
          Time=FeGetSystemTime()
3130      call FeEvent(1)
          if(EventType.eq.0.or.
     1      (EventType.eq.EventMouse.and.EventNumber.eq.JeMove)) then
            TimeNew=FeGetSystemTime()
            if(float(TimeNew-Time)/1000..lt.DelayForSubMenu)
     1        then
              call Roleta(xp,yp,20,
     1                    i,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                    RolN(Item),
     3                    j,SubM(1,1,Item),SubZ(1,Item),SubA(1,1,Item),
     4                    SubN(1,Item),'check')
              if(i.eq.WhatToDo1) Time=TimeNew
              go to 3130
            else
              EventType=EventMouse
              EventNumber=JeMove
            endif
          else
            go to 3120
          endif
        endif
        if(EventNumber.eq.JeLeftDown.and.WhatToDo1P.ne.0) then
          call FeIconOff(WhatToDo1P)
          WhatToDo1P=0
        endif
        if(EventNumber.eq.JeLeftDown.or.EventNumber.eq.JeLeftUp)
     1    ItemNew=0
        if(Ypos.ge.yp.and.Ypos.le.yp+yd.and.Xpos.ge.ToolBarX(1).and.
     1     Xpos.le.ToolBarX(7)) then
          do ItemNew=1,6
            if(Xpos.le.ToolBarX(ItemNew+1)) then
              MysZaToMuze=.true.
              if(WhatToDo1P.ne.0) then
                call FeIconOff(WhatToDo1P)
                WhatToDo1P=0
              endif
              if(EventNumber.eq.JeLeftUp.and.RoletaOn) then
                NatahniRoletu=nLeftDown.eq.0
                nLeftDown=nLeftDown+1
              else if(EventNumber.eq.JeLeftDown) then
                NatahniRoletu=.true.
                if(.not.RoletaOn) nLeftDown=0
              else if(EventNumber.eq.JeMove) then
                if(ItemNew.eq.Item) then
                  go to 3100
                else
                  if(Item.ne.0) then
                    if(StateToolBarButton(Item).eq.1) then
                      go to 4000
                    else if(StateToolBarButton(Item).eq.-1) then
                      xp1=ToolBarX(Item)
                      xp2=ToolBarX(Item+1)
                      call FeToolBarButton(xp1,xp2,yp,ypd,
     1                                     MenuToolBar(Item),
     2                                     StateToolBarButton(Item),0)
                    endif
                  endif
                  xp1=ToolBarX(ItemNew)
                  xp2=ToolBarX(ItemNew+1)
                  call FeToolBarButton(xp1,xp2,yp,ypd,
     1                                 MenuToolBar(ItemNew),
     2                                 StateToolBarButton(ItemNew),-1)
                  Item=ItemNew
                  go to 3100
                endif
              endif
              go to 4000
            endif
          enddo
          ItemNew=0
        else if(Item.ne.0) then
          if(RoletaOn) then
            call Roleta(xp,yp,20,
     1                  WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                  RolN(Item),
     3                  WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                  SubA(1,1,Item), SubN(1,Item),
     5                  'test')
            HuraNaTo=ItemNew.eq.0.and.WhatToDo1.ne.0
            if(HuraNaTo) then
              HuraNaTo=RolA(WhatToDo1,Item)
              if(HuraNaTo) then
                if(SubN(WhatToDo1,Item).gt.0) then
                   HuraNaTo=WhatToDo2.gt.0
                   if(HuraNaTo) HuraNaTo=SubA(WhatToDo2,WhatToDo1,Item)
                endif
              endif
            endif
            if(.not.HuraNaTo.and.(EventNumber.eq.JeLeftUp.or.
     1                            EventNumber.eq.JeLeftDown).and.
     2                            WhatToDo1.gt.0.and.
     3                            WhatToDo2.ge.0) then
              ItemNew=Item
              DoubleClickCount=0
              go to 3100
            endif
            if(Item.eq.3.and.HuraNaTo.and.WhatToDo1.le.13) then
              i=CtrlAction(WhatToDo1)
              Item=IconAction(1,i)
              WhatToDo1=IconAction(2,i)
              WhatToDo2=IconAction(3,i)
            else if(Item.eq.3.and.HuraNaTo.and.WhatToDo1.ge.IdRunDirect)
     1        then
              Item=IdRun
              WhatToDo1=WhatToDo1
              WhatToDo2=0
            endif
          else
            if(MysZaToMuze) then
              ItemNew=0
              NatahniRoletu=.false.
            else
              go to 3100
            endif
          endif
        else
3300      if(EventNumber.eq.JeLeftDown.and.
     1       YPos.ge.KartPhaseYMin.and.YPos.le.KartPhaseYMax.and.
     2       XPos.ge.KartPhaseXMin(1).and.XPos.le.KartPhaseXMax(NPhase))
     3      then
            do KPhase=1,NPhase
              if(XPos.lt.KartPhaseXMax(KPhase)) go to 3340
            enddo
            KPhase=NPhase
          endif
3340      KPhaseBasic=KPhase
          go to 3100
        endif
      else if(EventType.eq.EventKey.and.(EventNumber.eq.JeUp.or.
     1                                   EventNumber.eq.JeDown)) then
        if(Item.ne.0) then
          if(WhatToDo2.eq.0) then
            if(.not.RoletaOn) then
              if(EventNumber.eq.JeUp) then
                WhatToDo1=RolN(Item)
              else
                WhatToDo1=1
              endif
              ItemNew=Item
              NatahniRoletu=.true.
              go to 4000
            endif
            if(EventNumber.eq.JeUp) then
              WhatToDo1=mod(WhatToDo1-2+RolN(Item),
     1                      RolN(Item))+1
            else
              WhatToDo1=mod(WhatToDo1,RolN(Item))+1
            endif
          else
            if(EventNumber.eq.JeUp) then
              WhatToDo2=mod(WhatToDo2-2+SubN(WhatToDo1,Item),
     1                      SubN(WhatToDo1,Item))+1
            else
              WhatToDo2=mod(WhatToDo2,SubN(WhatToDo1,Item))+1
            endif
          endif
          call Roleta(ToolBarX(Item),yp-2.,20,
     1                WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                RolN(Item),
     3                WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                SubA(1,1,Item), SubN(1,Item),
     5                'switch')
        else if(WhatToDo1P.ne.0) then
          if(EventNumber.eq.JeUp.and.WhatToDo1P.ne.1) then
            i=WhatToDo1P-1
          else if(EventNumber.eq.JeDown.and.WhatToDo1P.ne.8) then
            i=WhatToDo1P+1
          else
            i=0
          endif
          if(i.ne.0) then
            call FeIconOff(WhatToDo1P)
            WhatToDo1P=i
            call FeIconOn(WhatToDo1P)
            go to 3100
          endif
        endif
      else if(EventType.eq.EventMouse.and.EventNumber.eq.JeDoubleClick)
     1  then
        if(WhatToDo1P.gt.0) then
          if(xpos.ge.IconXmin(WhatToDo1P).and.
     1       xpos.le.IconXmax(WhatToDo1P).and.
     2       ypos.ge.IconYmin(WhatToDo1P).and.
     3       ypos.le.IconYmax(WhatToDo1P)) then
            call FeWait(.1)
            go to 3500
          else
            go to 3100
          endif
        else
          go to 3100
        endif
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeReturn) then
        if(Item.ne.0.and.WhatToDo1.ne.0) then
          if(RolA(WhatToDo1,Item)) then
            HuraNaTo=.true.
            ItemNew=0
          endif
        else
          if(WhatToDo1P.ne.0) go to 3500
        endif
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
        if(ItemNew.ne.0) then
          if(StateToolBarButton(ItemNew).eq.1) then
            NatahniRoletu=.false.
          else
            ItemNew=0
          endif
        endif
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeLeft) then
        if(Item.ne.0) then
          if(SubState.ne.0.and.WhatToDo2.ne.0) then
            WhatToDo2=0
            call Roleta(ToolBarX(Item),yp-2.,20,
     1                  WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                  RolN(Item),
     3                  WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                  SubA(1,1,Item), SubN(1,Item),
     5                  'switch')
          else
            ItemNew=mod(Item+3,5)+1
            WhatToDo1=1
          endif
        endif
      else if(EventType.eq.EventKey.and.EventNumber.eq.JeRight) then
        if(Item.gt.0) then
          if(SubState.ne.0.and.WhatToDo2.eq.0) then
            WhatToDo2=1
            call Roleta(ToolBarX(Item),yp-2.,20,
     1                  WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                  RolN(Item),
     3                  WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                  SubA(1,1,Item), SubN(1,Item),
     5                  'switch')
          else
            ItemNew=mod(Item,5)+1
            WhatToDo1=1
          endif
        endif
      else if(EventType.eq.EventASCII) then
        if(Item.ne.0) then
          Znak=char(EventNumber)
          call mala(Znak)
          if(SubState.eq.0) then
            i=index(RolZ(Item),Znak)
            if(i.ne.0) then
              if(SubN(i,Item).le.0) then
                if(RolA(i,item)) then
                  WhatToDo1=i
                  HuraNaTo=.true.
                  ItemNew=0
                endif
              else
                WhatToDo1=i
                WhatToDo2=1
                call Roleta(ToolBarX(Item),yp-2.,20,
     1                      WhatToDo1,RolM(1,Item),RolZ(Item),
     2                      RolA(1,Item),RolN(Item),
     3                      WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                      SubA(1,1,Item), SubN(1,Item),
     5                      'switch')
              endif
            endif
          else
            i=index(SubZ(WhatToDo1,Item),Znak)
            if(i.ne.0) then
              if(SubA(i,WhatToDo1,Item)) then
                WhatToDo2=i
                HuraNaTo=.true.
                ItemNew=0
              endif
            endif
          endif
          if(Item.eq.3.and.HuraNaTo.and.WhatToDo1.le.13) then
            i=CtrlAction(WhatToDo1)
            Item=IconAction(1,i)
            WhatToDo1=IconAction(2,i)
            WhatToDo2=IconAction(3,i)
          endif
        else
          go to 3100
        endif
      else if(EventType.eq.EventAlt) then
        MysZaToMuze=.false.
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(ToolBarZ,Znak)
        if(i.ne.0) ItemNew=i
        WhatToDo1=1
        NatahniRoletu=.true.
      else if(EventType.eq.EventCtrl) then
        Znak=char(EventNumber)
        call mala(Znak)
        i=index(RolZ(3),Znak)
        if(i.gt.0) then
          j=CtrlAction(i)
          Item=IconAction(1,j)
          WhatToDo1=IconAction(2,j)
          WhatToDo2=IconAction(3,j)
          HuraNaTo=.true.
        else
          go to 3100
        endif
      else if(EventType.eq.EventCrw) then
        if(Item.ne.0)
     1    call Roleta(xp1,yp-2.,20,
     2                WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     3                RolN(Item),
     4                WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     5                SubA(1,1,Item), SubN(1,Item),
     6                'remove')
        RoletaOn=.false.
        if(EventNumber.ge.1.and.EventNumber.le.NDatBlock) then
          KDatBlockOld=KDatBlock
          do i=1,NDatBlock
            if(CrwLogic(i)) then
              if(EventNumber.ne.i) call FeCrwOff(i)
            else
              if(EventNumber.eq.i) call FeCrwOn(i)
            endif
          enddo
          KDatBlock=EventNumber
        endif
        if(KDatBlock.ne.KDatBlockOld) then
          if(.not.ExistM90) then
            if(ExistM41) then
              DataType(KDatBlock)=2
            else
              DataType(KDatBlock)=1
            endif
          endif
          isPowder=iabs(DataType(KDatBlock)).eq.2
          call PwdSetTOF(KDatBlock)
          go to 1100
        else
          go to 3100
        endif
      else if(EventType.eq.EventResize) then
        do i=1,NIconUsed
          call FeIconRemove(i)
        enddo
        call FeTabsReset(UseTabs)
        call FeMakeGrWin(0.,0.,YBottomMargin,0.)
        call FeBottomInfo(' ')
        go to 1500
      else
        go to 3100
      endif
      go to 4000
3500  i=IconOrder(WhatToDo1P)
      Item=IconAction(1,i)
      WhatToDo1=IconAction(2,i)
      WhatToDo2=IconAction(3,i)
      HuraNaTo=.true.
4000  DoubleClickCount=0
4010  if(Item.ne.0.and.(ItemNew.ne.Item.or..not.NatahniRoletu)) then
        xp1=ToolBarX(Item)
        xp2=ToolBarX(Item+1)
        if(RoletaOn) then
          i=WhatToDo1
          j=WhatToDo2
          call Roleta(xp1,yp-2.,20,
     1                WhatToDo1,RolM(1,Item),RolZ(Item),RolA(1,Item),
     2                RolN(Item),
     3                WhatToDo2,SubM(1,1,Item),SubZ(1,Item),
     4                SubA(1,1,Item), SubN(1,Item),
     5                'remove')
          if(HuraNaTo) then
            WhatToDo1=i
            WhatToDo2=j
          endif
          RoletaOn=.false.
        endif
        call FeToolBarButton(xp1,xp2,yp,ypd,MenuToolBar(Item),
     1                       StateToolBarButton(Item),0)
      endif
      if(HuraNaTo) go to 5000
      if(ItemNew.ne.0) then
        xp1=ToolBarX(ItemNew)
        xp2=ToolBarX(ItemNew+1)
        if(NatahniRoletu) then
          if(.not.RoletaOn.or.ItemNew.ne.Item) then
            call Roleta(xp1,yp-2.,20,
     1                  WhatToDo1,RolM(1,ItemNew),RolZ(ItemNew),
     2                  RolA(1,ItemNew),RolN(ItemNew),WhatToDo2,
     3                  SubM(1,1,ItemNew),SubZ(1,ItemNew),
     4                  SubA(1,1,ItemNew),SubN(1,ItemNew),'create')
            RoletaOn=.true.
          endif
          k=1
        else
          RoletaOn=.false.
          k=-1
        endif
        call FeToolBarButton(xp1,xp2,yp,ypd,MenuToolBar(ItemNew),
     1                       StateToolBarButton(ItemNew),k)
      endif
      Item=ItemNew
      if(Item.le.0) WhatToDo1=0
      go to 3100
5000  call JanaReset(Change)
      if(Change) go to 5100
      call FeDeferOutput
      if(WrongM40.or.WrongM41.or.WrongM50) then
        if(Item.ne.IdFile.and.Item.ne.IdEdit.and.
     1     (Item.ne.IdRun.or.WhatToDo1.ne.IdRunEditM50).and.
     2     (Item.ne.IdTools.or.
     3      (WhatToDo1.ne.IdToolsRecover.and.
     4       WhatToDo1.ne.IdToolsAbout.and.
     5       WhatToDo1.ne.IdToolsPref))) then
          NInfo=3
          if(WrongM40) then
            Veta='M40'
          else if(WrongM41) then
            Veta='M41'
          else
            Veta='M50'
          endif
          TextInfo(1)='Due to the previous error which has occured '//
     1                'during reading '//Veta(1:3)
          TextInfo(2)='only "File", "Edit" or  "Tools->Recover" '//
     1                'commands can be used'
          TextInfo(3)='to correct error(s) or change the structure.'
          call FeInfoOut(-1.,-1.,'WARNING','L')
          do i=1,8
            call FeIconOff(i)
          enddo
          HuraNaTo=.false.
          go to 3000
        endif
      endif
5100  do i=1,NIconUsed
        call FeIconRemove(i)
      enddo
      if(NDatBlock.gt.1) then
        do i=1,NDatBlock
          call FeCrwRemove(i)
        enddo
      endif
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call FeReleaseOutput
      call FeDeferOutput
      TakeMouseMove=.false.
      DoubleClickAllowed=.false.
      AllowChangeMouse=.true.
      go to 8000
6000  read(BatchLN,FormA,err=6500,end=6500) Veta
      i=index(Veta,'->')
      if(i.ne.0) then
        do Item=1,6
          if(EqIgProcenta(Veta(:i-1),MenuToolBar(Item))) go to 6020
        enddo
      else
        go to 6000
      endif
6020  Veta=Veta(i+2:)
      i=index(Veta,'->')
      if(i.gt.0) then
        t50=Veta(:i-1)
        Veta=Veta(i+2:)
      else
        t50=Veta
        Veta=' '
      endif
      do 6100WhatToDo1=1,20
        if(Item.eq.IdRun) then
          k=0
          call Kus(RolM(WhatToDo1,Item),k,p50)
        else
          p50=RolM(WhatToDo1,Item)
        endif
        if(p50.eq.' ') go to 6100
        if(EqIgProcenta(t50,p50)) go to 6200
6100  continue
      go to 6000
6200  WhatToDo2=0
      if(Veta.ne.' ') then
        t50=Veta
        do 6300WhatToDo2=1,20
          p50=SubM(WhatToDo2,WhatToDo1,Item)
          if(p50.eq.' ') go to 6300
          if(EqIgProcenta(p50,t50)) go to 8000
6300    continue
      else
        go to 8000
      endif
      go to 6000
6500  call DeletePomFiles
      stop
8000  if(.not.ChargeDensities.and.lasmaxm.gt.0) then
        call FeChybne(-1.,-1.,'the inconsistency of M40 and M50.',
     1                'M40 for charge densities, but M50 not.',
     2                SeriousError)
        if(Item.ge.3) go to 1150
      endif
      go to 9999
9000  Item=0
      WhatToDo1=0
      WhatToDo2=0
9999  call FeTabsReset(UseTabs)
      UseTabsIn=UseTabs
      deallocate(KartPhaseXMin,KartPhaseXMax)
      AllowResizing=.false.
      return
      end
      subroutine SetIcons(Label,LabelOrder)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'main.cmn'
      character*(*) Label(*)
      integer LabelOrder(*)
      logical ExistFile
      i=IconEditM50
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunEditM50
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=ExistM50
      i=IconDist
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunDist
      IconAction(3,i)=0
      IconAction(4,i)=IdRun
      IconAction(5,i)=IdRunSetCmd
      IconAction(6,i)=3
      LIcon(i)=NAtCalc.gt.0
      i=IconFourier
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunFourier
      IconAction(3,i)=0
      IconAction(4,i)=IdRun
      IconAction(5,i)=IdRunSetCmd
      IconAction(6,i)=1
      LIcon(i)=(NAtCalc.gt.0.or.ExistFile(fln(:ifln)//'.m80').or.
     1          ExistM90).and..not.ParentStructure
      i=IconContour
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunContour
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=(NAtCalc.gt.0.or.ExistFile(fln(:ifln)//'.m80').or.
     1          ExistFile(fln(:ifln)//'.m81').or.ExistM90).and.
     2          .not.ParentStructure
      i=IconRefine
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunRefine
      IconAction(3,i)=0
      IconAction(4,i)=IdRun
      IconAction(5,i)=IdRunSetCmd
      IconAction(6,i)=2
      LIcon(i)=(NAtCalc.gt.0.or.ExistM90).and..not.ParentStructure
      i=IconGrapht
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunGrapht
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=NDimI(KPhase).gt.0.and.NAtCalc.gt.0
      i=IconPlotStructure
      IconAction(1,i)=IdTools
      IconAction(2,i)=IdToolsGraph
      IconAction(3,i)=IdToolsGraphViewer
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=OpSystem.ne.1.and.CallGraphic.ne.' '.and.NAtCalc.gt.0
      i=IconEditAtoms
      IconAction(1,i)=IdParam
      IconAction(2,i)=IdParamAtoms
      IconAction(3,i)=IdParamAtomsEdit
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=NAtCalc.gt.0.and.NAtFormula(KPhase).gt.0
      i=IconSetCommands
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunSetCmd
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=ExistM50
      i=IconProfileViewer
      IconAction(1,i)=IdTools
      IconAction(2,i)=IdToolsPowder
      IconAction(3,i)=IdToolsPowderPlot
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=ExistPowder
      i=IconEditProfile
      IconAction(1,i)=IdParam
      IconAction(2,i)=IdParamPowder
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=ExistPowder
      i=IconStructureSolution
      IconAction(1,i)=IdRun
      IconAction(2,i)=IdRunSolution
      IconAction(3,i)=0
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=ExistM90.and..not.ParentStructure
      i=IconMatrixCalculator
      IconAction(1,i)=IdTools
      IconAction(2,i)=IdToolsSpecial
      IconAction(3,i)=IdToolsSpecialMatCalc
      IconAction(4,i)=0
      IconAction(5,i)=0
      IconAction(6,i)=0
      LIcon(i)=.true.
      IconOrder( 1)=IconEditM50
      IconOrder( 2)=IconEditAtoms
      IconOrder( 3)=IconEditProfile
      IconOrder( 4)=IconStructureSolution
      IconOrder( 5)=IconFourier
      IconOrder( 6)=IconContour
      IconOrder( 7)=IconRefine
      IconOrder( 8)=IconDist
      IconOrder( 9)=IconMatrixCalculator
      IconOrder(10)=IconPlotStructure
      IconOrder(11)=IconProfileViewer
      IconOrder(12)=IconGrapht
      IconOrder(13)=IconSetCommands
      NIconUsed=13
      do i=1,NIconUsed
        j=LabelOrder(i)
        k=index(Label(i),Tabulator)
        if(k.le.0) k=idel(Label(i))
        IconLabelAct(j)=Label(i)(:k-1)
        l=0
        IconFileAct(j)=' '
        do k=1,idel(IconLabelAct(j))
          if(IconLabelAct(j)(k:k).eq.'%'.or.
     1       IconLabelAct(j)(k:k).eq.' ') cycle
          l=l+1
          IconFileAct(j)(l:l)=IconLabelAct(j)(k:k)
        enddo
      enddo
      return
      end
      subroutine TestDataFiles(Ukoncit)
      parameter (NFiles=19)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 NameOfFile,t256
      character*80 DataFiles(NFiles),Radka
      integer FeFileSize
      logical ExistFile,FeYesNoHeader,FeYesNo,Ukoncit,Opravit
      data DataFiles
     1              /'a2ps%lstnewpg.ps',
     2               'a2ps%lstprolo.ps',
     3               'bondval%bvparm.cif',
     4               'bondval%typical_distances.dat',
     5               'cif%cif.dat',
     6               'cif%cif_specific_basic.dat',
     7               'formfac%absor.dat',
     8               'formfac%anom.dat',
     9               'formfac%atoms.dat',
     a               'formfac%ions.dat',
     1               'formfac%eleccoef.dat',
     2               'formfac%electab.dat',
     3               'formfac%wavefj.dat',
     4               'formfac%wavefn.dat',
     5               'formfac%wavefc.dat',
     6               'symmdat%spgroup.dat',
     7               'symmdat%spgroup_alias.dat',
     8               'symmdat%spgroup_list.dat',
     9               'symmdat%pgroup.dat'/
      Ukoncit=.false.
      Opravit=.false.
      do i=1,NFiles
        j=index(DataFiles(i),'%')
        if(j.gt.0) DataFiles(i)(j:j)=DirectoryDelimitor
        if(OpSystem.le.0) then
          NameOfFile=HomeDir(:idel(HomeDir))//DataFiles(i)
        else
          NameOfFile=HomeDir(:idel(HomeDir))//DataFiles(i)
        endif
1000    if(ExistFile(NameOfFile)) then
          ln=NextLogicNumber()
          open(ln,file=NameOfFile)
          read(ln,FormA) Radka
          if(Opravit) then
            if(Radka(1:3).ne.'###') rewind ln
            lno=NextLogicNumber()
            call OpenFile(lno,'ven.l90','formatted','unknown')
            if(ErrFlag.ne.0) go to 1070
            t256='### '//VersionString(16:idel(VersionString))//
     1           ' ### - forced out by user !!!'
1010        write(lno,FormA) t256(:idel(t256))
            read(ln,FormA,end=1050) t256
            go to 1010
1050        call CloseIfOpened(ln)
            call CloseIfOpened(lno)
            call MoveFile('ven.l90',NameOfFile)
            cycle
1070        call CloseIfOpened(ln)
            call CloseIfOpened(lno)
            Ukoncit=.true.
            go to 9999
          else
            call CloseIfOpened(ln)
          endif
          j=index(Radka(4:),'#')+5
          if(Radka(:j).ne.'### '//VersionString(16:idel(VersionString))
     1                 //' ###') then
            TextInfo(1)='The data file "'//
     1                  DataFiles(i)(:idel(DataFiles(i)))//
     2                  '" does not have'
            TextInfo(2)='proper version string.'
            NInfo=2
            Ukoncit=.not.FeYesNoHeader(-1.,-1.,'Do you want to '//
     1                                 'continue anyhow?',0)
            if(Ukoncit) then
              go to 9999
            else
              if(VasekTest.ne.0) then
                Opravit=FeYesNo(-1.,-1.,'Do you want to skip this '//
     1                          'warning from now on?',0)
                if(Opravit) then
                  go to 1000
                else
                  exit
                endif
              else
                exit
              endif
            endif
          endif
        else
          call FeChybne(-1.,-1.,'the data file "'//
     1      DataFiles(i)(:idel(DataFiles(i)))//'" does not exist.',' ',
     2      FatalError)
          Ukoncit=.true.
          go to 9999
        endif
      enddo
      RecLenFacForm=4
      RecLenFacUnform=4
      ln=NextLogicNumber()
      Radka='jtst'
      call CreateTmpFile(Radka,i,0)
      call FeTmpFilesAdd(Radka)
      lrec=4
      open(ln,file=Radka,form='formatted',status='unknown',
     1     access='direct',recl=lrec*RecLenFacForm,err=5000)
      write(ln,'(a16)',rec=1) 'abcdefghabcdefgh'
      call CloseIfOpened(ln)
      RecLenFacForm=nint(float(lrec*RecLenFacForm)/
     1                   float(FeFileSize(Radka))*float(RecLenFacForm))
      open(ln,file=Radka,form='unformatted',status='unknown',
     1     access='direct',recl=lrec*RecLenFacUnform,err=5000)
      write(ln,rec=1)(i,i=1,4)
      call CloseIfOpened(ln)
      RecLenFacUnform=nint(float(lrec*RecLenFacUnform)/
     1                float(FeFileSize(Radka))*float(RecLenFacUnform))
5000  call DeleteFile(Radka)
      call FeTmpFilesClear(Radka)
9999  return
      end
      subroutine AboutJana
      include 'fepc.cmn'
      include 'basic.cmn'
      call FeFillTextInfo('aboutjana.txt',0)
      TextInfo(1)=VersionString
      do i=1,NInfo
        call DeleteFirstSpaces(TextInfo(i))
      enddo
      call FeInfoOut(-1.,-1.,'Jana2006','C')
      return
      end
      subroutine FeSbwActionSpecial(id)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      DegMin=XPwd(SbwColumnFrom(id))
      DegMax=XPwd(SbwColumnTo  (id))
      call PwdPrfKresli(-1,0)
      call FeWInfRemove(1)
      if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove) then
        xpom=(SbwXMinStrelka(id)+SbwXMaxStrelka(id))*.5-25.
        write(Cislo,'(f8.2)')(DegMin+DegMax)*.5
        call Zhusti(Cislo)
        ypom=(SbwYMinStrelka(id)+SbwYMaxStrelka(id))*.5-25.
        call FeWInfMake(1,LastQuest,xpom,ypom,50.,14.)
        call FeWInfWrite(1,Cislo)
      endif
      return
      end
      subroutine BatchCoDal(Item,WhatToDo1,WhatToDo2)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'main.cmn'
      include 'powder.cmn'
      integer WhatToDo1,WhatToDo2,FeChdir
      character*256 Veta,Kus1,Kus2,Kus3,Directory,Veta1,Veta2
      character*80  Nazev
      character*12  At,parp
      logical Change,EqIgCase,ExistFile
      if(ifln.gt.0) then
        call TestUzavreni
        call JanaReset(Change)
        if(Change) go to 9900
      endif
      if(LogLn.eq.0) then
        LogLn=NextLogicNumber()
        call OpenFile(LogLn,BatchDir(:idel(BatchDir))//
     1                'Jana2006-Batch.log','formatted','unknown')
      endif
1100  read(BatchLN,FormA,end=9100,err=9200) Veta
      if(Veta(1:1).eq.'.') then
        Veta=Veta(2:)
        Kus1=' '
        Kus2=' '
        Kus3=' '
        i=LocateSubstring(Veta,'->',.false.,.true.)
        if(i.gt.0) then
          Kus1=Veta(:i-1)
          Veta=Veta(i+2:)
          i=LocateSubstring(Veta,'->',.false.,.true.)
          if(i.gt.0) then
            Kus2=Veta(:i-1)
            Kus3=Veta(i+2:)
          else
            Kus2=Veta
          endif
        else
          Kus1=Veta
        endif
      else
        go to 1100
      endif
      if(EqIgCase(Kus1,'run')) then
        Item=IdRun
        WhatToDo2=0
        if(EqIgCase(Kus2,'refine')) then
          WhatToDo1=IdRunRefine
          go to 9999
        else if(EqIgCase(Kus2,'fourier')) then
          WhatToDo1=IdRunFourier
          go to 9999
        else if(EqIgCase(Kus2,'dist')) then
          WhatToDo1=IdRunDist
          go to 9999
        endif
      else if(EqIgCase(Kus1,'Edit')) then
        if(EqIgCase(Kus2,'Parameters')) then
          if(isPowder) then
            Veta='Edit of powder and structural parameters'
          else
            Veta='Edit of structural parameters'
          endif
          call BatchHlavicka(Veta,'=')
          write(LogLn,FormA) Veta(:idel(Veta))
1500      read(BatchLN,FormA,end=9100,err=9200) Veta
          if(Veta(1:1).eq.'.') then
            backspace BatchLN
            call iom40(1,0,fln(:ifln)//'.m40')
            call iom40(0,0,fln(:ifln)//'.m40')
            go to 1100
          else if(Veta(1:1).eq.'#') then
            go to 1500
          endif
          call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
          k=0
1600      if(k.ge.len(Veta)) go to 1500
          call kus(Veta,k,Nazev)
          call kus(Veta,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,100,err=1640) Value
          i=index(Nazev,'[')
          j=index(Nazev,']')
          if(i.gt.0) then
            if(j.eq.0.or.i.ge.j.or.j.ne.idel(t80)) go to 1650
            At=Nazev(i+1:j-1)
            call uprat(At)
            parp=Nazev(1:i-1)
            k=ktatmol(At)
            if(k.ne.0) then
              m=PocDer(k)
              if(m.le.0) go to 1610
              n=Ktera(parp)
              if(n.le.0) go to 1620
              if(k.gt.0) then
                call ChangeAtCompress(n,n,k,0,ich)
              else if(k.lt.0) then
                call ChangeMolCompress(n,n,-k,0,ich)
              endif
              kder=m+n
            else
              k=ktat(PhaseName,NPhase,at)
              if(k.le.0) go to 1630
              kder=kterasc(parp)
              if(kder.le.0) go to 1610
              kder=kder+(k-1)*NParProfPwd
            endif
          else
            parp=Nazev
            kder=KteraSc(Nazev)
            if(kder.le.0) go to 1610
          endif
          call kdoco(kder,At,parp, 1,pom,sp)
          call kdoco(kder,At,parp,-1,Value,sp)
          go to 1600
1610      Veta='atom "'//at(:idel(at))//'" doesn''t exist.'
          go to 1690
1620      Veta='unknown or inapplicable parameter "'//
     1         parp(:idel(parp))//'"'
          go to 1690
1630      Veta='phase "'//at(:idel(at))//'" doesn''t exist.'
          go to 1690
1640      Veta='wrong numerical string "'//Cislo(:idel(Cislo))//'"'
          go to 1690
1650      Veta='Syntax error for the parameter: "'//
     1         Nazev(:idel(Nazev))//' '//Cislo(:idel(Cislo))//'"'
1690      call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
          go to 1600
        else if(EqIgCase(Kus2,'PwdKeys')) then
          call BatchHlavicka(Veta,'=')
          KDatB=1
          KPh=1
1700      read(BatchLN,FormA,end=9100,err=9200) Veta
          if(Veta(1:1).eq.'.') then
            backspace BatchLN
            call iom40(1,0,fln(:ifln)//'.m40')
            call iom40(0,0,fln(:ifln)//'.m40')
            go to 1100
          else if(Veta(1:1).eq.'#') then
            go to 1700
          endif
          k=0
          call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
1800      if(k.ge.len(Veta)) go to 1700
          call kus(Veta,k,Nazev)
          j=islovo(Nazev,idM41,36)
          call kus(Veta,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,100,err=1805) Value
          IValue=nint(Value)
          kp=k+1
          if(j.eq.IdM41Phase) then
            KPh=IValue
          else if(j.eq.IdM41DatBlock) then
            KDatB=IValue
          else if(i.eq.IdM41KBackg) then
            KBackg(KDatB)=IValue
          else if(j.eq.IdM41NBackg) then
            NBackg(KDatB)=IValue
          else if(j.eq.IdM41KUseInvX) then
            KUseInvX(KDatB)=IValue
          else if(j.eq.IdM41KManBackg) then
            KManBackg(KDatB)=IValue
          else if(j.eq.IdM41KWleBail) then
            KWleBail(KDatB)=IValue
          else if(j.eq.IdM41KAbsor) then
            KAbsor(KDatB)=max(IValue,0)
          else if(j.eq.IdM41Mir) then
            MirPwd(KDatB)=Value
          else if(j.eq.IdM41SkipFrTo) then
            NSkipPwd(KDatB)=NSkipPwd(KDatB)+1
            n=NSkipPwd(KDatB)
            if(n.ge.MxSkipPwd) go to 1700
            SkipPwdFr(n,KDatB)=Value
            call kus(Veta,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=1805) SkipPwdTo(n,KDatB)
            if(Veta(1:1).eq.'!') then
              SkipPwdFr(n,KDatB)=-SkipPwdFr(n,KDatB)-200.
              SkipPwdTo(n,KDatB)=-SkipPwdTo(n,KDatB)-200.
            endif
            go to 1700
          else if(j.eq.IdM41KProfPwd) then
            KProfPwd(KPh,KDatB)=IValue
          else if(j.eq.IdM41KAsym) then
            KAsym(KDatB)=IValue
          else if(j.eq.IdM41KStrain) then
            KStrain(KPh,KDatB)=IValue
          else if(j.eq.IdM41KPref) then
            KPref(KPh,KDatB)=IValue
          else if(j.eq.IdM41SPref) then
            SPref(KPh,KDatB)=Value
          else if(j.eq.IdM41PCutOff) then
            PCutOff(KPh,KDatB)=Value
          else if(j.eq.IdM41DirPref) then
            DirPref(1,KPh,KDatB)=Value
            call StToReal(Veta,k,DirPref(2,KPh,KDatB),2,.false.,ich)
            if(ich.ne.0) then
              Veta=Veta(kp:)
              go to 1810
            endif
          else if(j.eq.IdM41DirBroad) then
            DirBroad(1,KPh,KDatB)=Value
            call StToReal(Veta,k,DirBroad(2,KPh,KDatB),2,.false.,ich)
            if(ich.ne.0) then
              Veta=Veta(kp:)
              go to 1810
            endif
          else if(j.eq.IdM41MMax) then
            MMaxPwd(1,KPh,KDatB)=IValue
            if(NDimI(KPh).gt.1) then
              call StToInt(Veta,k,MMaxPwd(2,KPh,KDatB),NDimI(KPh)-1,
     1                     .false.,ich)
              if(ich.ne.0) then
                Veta=Veta(kp:)
                go to 1810
              endif
            endif
          else if(j.eq.IdM41SatFrMod) then
            if(NAtIndFrAll(KPh).gt.0) SatFrMod(KPh,KDatB)=IValue.eq.1
          else if(j.eq.IdM41KRefLam) then
            KRefLam(KDatB)=IValue
          else if(j.eq.IdM41SkipFrdl) then
            SkipFriedel(KPh,KDatB)=IValue.eq.1
          else if(j.eq.IdM41KRough) then
            KRough(KDatB)=IValue
          else if(j.eq.IdM41RadPrim) then
            RadPrim(KDatB)=Value
          else if(j.eq.IdM41RadSec) then
            RadSec(KDatB)=Value
          else if(j.eq.IdM41KUseRSW) then
            KUseRSW(KDatB)=IValue
          else if(j.eq.IdM41KUseFDSA.or.j.eq.IdM41KUseDS) then
            KUseDS(KDatB)=IValue
          else if(j.eq.IdM41KUsePSoll) then
            KUsePSoll(KDatB)=IValue
          else if(j.eq.IdM41KUseSSoll) then
            KUseSSoll(KDatB)=IValue
          else if(j.eq.IdM41CutOffMn) then
            CutOffMinPwd=Value
            UseCutOffPwd=.true.
          else if(j.eq.IdM41CutOffMx) then
            CutOffMaxPwd=Value
            UseCutOffPwd=.true.
          else if(j.eq.IdM41UseTOFAbs) then
            KUseTOFAbs(KDatB)=IValue
          else if(j.eq.IdM41KIllum) then
            KIllum(KDatB)=IValue
          else if(j.eq.IdM41KFocusBB) then
            KFocusBB(KDatB)=IValue
          endif
          go to 1800
1805      Veta=Cislo
1810      Veta='wrong numerical string "'//Veta(:idel(Veta))//'"'
          go to 1890
1890      call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
          go to 1800
        else if(EqIgCase(Kus2,'Radiation')) then
          Veta='Radiation'
          call BatchHlavicka(Veta,'=')
1900      read(BatchLN,FormA,end=9100,err=9200) Veta
          if(Veta(1:1).eq.'.') then
            backspace BatchLN
            call iom50(1,0,fln(:ifln)//'.m50')
            call iom50(0,0,fln(:ifln)//'.m50')
            go to 1100
          else if(Veta(1:1).eq.'#') then
            go to 1900
          endif
          call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
          k=0
2000      if(k.ge.len(Veta)) go to 1900
          call kus(Veta,k,Nazev)
          j=islovo(Nazev,idM50,70)
          if(j.lt.IdM50Lambda.or.j.gt.IdM50PerfMon) go to 2000
          call kus(Veta,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,100,err=2005) Value
          IValue=nint(Value)
          if(j.eq.IdM50Lambda) then
            LamAve(1)=Value
            LamA1(1)=Value
            LamA2(1)=Value
          else if(j.eq.IdM50RadType) then
            Radiation(1)=IValue
          else if(j.eq.IdM50LpFactor) then
            LPFactor(1)=IValue
          else if(j.eq.IdM50NAlpha) then
            NAlfaP=IValue
          else if(j.eq.IdM50MonAngle) then
            AngleMon(1)=Value
          else if(j.eq.IdM50Kalpha1) then
            LamA1(1)=Value
          else if(j.eq.IdM50Kalpha2) then
            LamA2(1)=Value
          else if(j.eq.IdM50Kalphar) then
            LamRat(1)=Value
          else if(j.eq.IdM50PerfMon) then
            FractPerfMon(1)=Value
          endif
          go to 2000
2005      Veta=Cislo
2010      Veta='wrong numerical string "'//Veta(:idel(Veta))//'"'
          go to 2090
2090      call FeLstWriteLine(Veta,-1)
          write(LogLn,FormA) Veta(:idel(Veta))
          go to 2000
        endif
      else if(EqIgCase(Kus1,'File')) then
        if(EqIgCase(Kus2,'Structure')) then
          if(EqIgCase(Kus3,'Open')) then
            read(BatchLn,FormA) Veta
            call ExtractDirectory(Veta,Veta1)
            call ExtractFileName(Veta,Veta2)
            if(fln.ne.' ') call DeletePomFiles
            i=FeChdir(Veta1)
            fln=Veta2
            ifln=idel(fln)
            call FeBottomInfo(' ')
            call DeletePomFiles
            call FortFilesClean
            if(ifln.gt.0) then
              do i=1,9
                ExistMFile(i)=ExistFile(fln(:ifln)//ExtMFile(i))
              enddo
            else
              ExistMFile=.false.
            endif
!            SetMetAllowed=.true.
            if(Existm50) call iom50(0,0,fln(:ifln)//'.m50')
            if(Existm40) call iom40(0,0,fln(:ifln)//'.m40')
            if(Existm90) call iom90(0,fln(:ifln)//'.m90')
            if(Existm95) call iom95(0,fln(:ifln)//'.m95')
            go to 1100
          endif
        endif
C ICDD block begin
      else if(EqIgCase(Kus1,'ICDD')) then
        call ICDDBatchRun(Kus2,Kus3,ich)
        if(ich.eq.0) then
          go to 1100
        else if(ich.eq.1) then
          go to 9100
        else if(ich.eq.2) then
          go to 9200
        else if(ich.eq.3) then
          go to 1100
        endif
C ICDD block end
      else if(EqIgCase(Kus1,'end')) then
        Veta='End of batch file has been reached.'
        write(LogLn,FormA) Veta(:idel(Veta))
        if(.not.EqIgCase(Kus2,'close')) then
          call CloseIfOpened(LogLn)
          NInfo=0
          WaitTime=5000
          call FeInfoOut(-1.,-1.,'End of batch file has been reached.',
     1                   'L')
        endif
        call DeletePomFiles
        call FeTmpFilesDelete
        call FeGrQuit
        LogLn=0
        stop
      else if(EqIgCase(Kus1,'import')) then
        if(EqIgCase(Kus2,'CIF')) then
          read(BatchLN,FormA,end=9100,err=9200) Veta
          call DeleteFirstSpaces(Veta)
          call ExtractDirectory(Veta,Directory)
          call ExtractFileName(Veta,Kus1)
          call GetPureFileName(Kus1,fln)
          ifln=idel(fln)
          i=FeChDir(Directory)
          call FeGetCurrentDir
          call BatchReadCIF(Kus1,ich)
          if(ich.ne.0) then
            Kus1='CIF import colapsed'
            Kus2=' '
            go to 9500
          endif
        endif
        go to 1100
      else if(EqIgCase(Kus1,'Tools')) then
        if(EqIgCase(Kus2,'Graphic')) then
          if(EqIgCase(Kus3,'Create graphic output file')) then
            call GraphicOutputProlog
            Kus3=fln(:ifln)//'_tmp'
            call GraphicOutput(-7,Kus3,ich)
          endif
        endif
      endif
      go to 1100
9100  Kus1='end of file read from the batch file.'
      Kus2=' '
      go to 9500
9200  Kus1='reading error occured on the batch file.'
      Kus2=' '
      go to 9500
9500  call FeChybne(-1.,-1.,Kus1,Kus2,FatalError)
9900  Item=0
      WhatToDo1=0
      WhatToDo2=0
      call DeletePomFiles
9999  return
100   format(f15.5)
      end
      include 'datablockfepc.cmn'
      include 'datablockjana.cmn'
