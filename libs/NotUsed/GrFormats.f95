      subroutine FeSaveBMPFile(FileName,ix1,ix2,iy1,iy2,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) FileName
      character*256 t256
      character*80  Veta
      character*2   Label
      character*1   Bitmap(:),String(256)
      integer FeOpenBinaryFile,FeCloseBinaryFile,FeWriteBinaryFile,
     1        Width,Height,WidthBytes
      allocatable Bitmap
      equivalence (t256,Label),(t256,String)
      Width=iabs(ix2-ix1)+1
      Height=iabs(iy2-iy1)+1
      ix1p=min(ix1,ix2)
      iy2p=min(iy1,iy2)
      WidthBytes=3*Width
      i=mod(WidthBytes,4)
      if(i.ne.0) WidthBytes=WidthBytes+4-i
      lno=FeOpenBinaryFile(FileName(:idel(FileName)))
      if(lno.le.0) go to 9000
      t256=' '
      Label='BM'
      k=3
      call IntToBinSt(WidthBytes*Height+54,4,t256,k)
      i=0
      call IntToBinSt(i,2,t256,k)
      call IntToBinSt(i,2,t256,k)
      i=54
      call IntToBinSt(i,4,t256,k)
      n=FeWriteBinaryFile(lno,String,14)
      t256=' '
      i=40
      k=1
      call IntToBinSt(i,4,t256,k)
      call IntToBinSt(Width,4,t256,k)
      call IntToBinSt(Height,4,t256,k)
      i=1
      call IntToBinSt(i,2,t256,k)
      call IntToBinSt(24,2,t256,k)
      i=0
      call IntToBinSt(i,4,t256,k)
      call IntToBinSt(WidthBytes*Height,4,t256,k)
      i=0
      call IntToBinSt(i,4,t256,k)
      call IntToBinSt(i,4,t256,k)
      call IntToBinSt(i,4,t256,k)
      call IntToBinSt(i,4,t256,k)
      n=FeWriteBinaryFile(lno,String,40)
      allocate(Bitmap(WidthBytes))
      call SetStringArrayTo(Bitmap,WidthBytes,char(0))
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      do iy=1,Height
        i1=0
        do ix=1,Width
          i=GetPixel(carg(hDCComp),carg(ix+ix1p-1),carg(Height-iy+iy2p))
          Bitmap(i1+3)=char(mod(i,256))
          i=i/256
          Bitmap(i1+2)=char(mod(i,256))
          i=i/256
          Bitmap(i1+1)=char(mod(i,256))
          i1=i1+3
        enddo
        n=FeWriteBinaryFile(lno,Bitmap,WidthBytes)
      enddo
      go to 9999
9000  t256='The file could not be opened.'
      go to 9900
9900  Veta='during export of the bitmap to "'//
     1     FileName(:idel(FileName))//'"'
      call FeChybne(-1.,-1.,Veta,t256,SeriousError)
      ich=1
9999  if(allocated(Bitmap)) deallocate(Bitmap)
      if(lno.gt.0) then
        i=FeCloseBinaryFile(lno)
        i=LocateSubstring(FileName,'\',.false.,.false.)
        if(i.gt.0) Veta=FileName(i+1:)
        call FeResetReadOnly(Veta)
      endif
      return
      end
      subroutine FeLoadBMPFile(FileName,ix1,ix2,iy1,iy2,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) FileName
      character*256 t256
      character*80  Veta
      character*1   PaletteColors(:,:),Bitmap(:,:)
      character*2   Label
      character*1   Radka(:),String(256)
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,
     1        BinStToInt,hBitmap,hDCp,
     2        FileSize,DataOffSet,HeaderSize,DataSize,Width,Height,
     3        bPlanes,Colors,WidthBytes
      logical EqIgCase
      allocatable PaletteColors,Bitmap,Radka
      equivalence (t256,Label,String)
      ich=0
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      if(ln.le.0) go to 9000
      n=FeReadBinaryFile(ln,String,14)
      k=1
      if(.not.EqIgCase(Label,'BM')) go to 9100
      k=k+2
      FileSize=BinStToInt(t256,k,4)
      k=k+4
      DataOffSet=BinStToInt(t256,k,4)
      n=FeReadBinaryFile(ln,String,40)
      k=1
      HeaderSize=BinStToInt(t256,k,4)
      if(HeaderSize.ne.40) go to 9200
      Width=BinStToInt(t256,k,4)
      Height=BinStToInt(t256,k,4)
      bPlanes=BinStToInt(t256,k,2)
      if(bPlanes.ne.1) go to 9300
      Colors=BinStToInt(t256,k,2)
      if(Colors.ne.1.and.Colors.ne.4.and.Colors.ne.8.and.
     1   Colors.ne.24.and.Colors.ne.32) go to 9400
      ii=BinStToInt(t256,k,4)
      if(ii.ne.0) go to 9500
      DataSize=BinStToInt(t256,k,4)
      k=k+8
      NPaletteColors=BinStToInt(t256,k,4)
      if(NPaletteColors.ne.2**Colors.and.
     1   (NPaletteColors.ne.0.and.Colors.eq.24)) go to 9600
      if(NPaletteColors.gt.0) then
        allocate(PaletteColors(4,NPaletteColors))
        do i=1,NPaletteColors
          n=FeReadBinaryFile(ln,PaletteColors(1,i),4)
        enddo
      endif
      if(Height.lt.0) go to 9700
      if(Colors.eq.32) then
        idl=4*Width
        kk=4
      else if(Colors.eq.24) then
        idl=3*Width
        kk=3
      else
        idl=Width*2**Colors
        idl=(idl-1)/256+1
      endif
      if(BitsPixelNew.eq.32) then
        WidthBytes=4*Width
      else if(BitsPixelNew.eq.24) then
        WidthBytes=3*Width
      else if(BitsPixelNew.eq.16) then
        WidthBytes=2*Width
      else
        go to 9800
      endif
      i=mod(idl,4)
      if(i.ne.0) then
        idl=idl+4-i
        idf=4-i
      endif
      allocate(Radka(idl),Bitmap(WidthBytes,Height))
      call SetStringArrayTo(Bitmap,WidthBytes*Height,char(0))
      call SetStringArrayTo(Radka,idl,char(0))
      do i=Height,1,-1
        n=FeReadBinaryFile(ln,Radka,idl)
        if(NPaletteColors.gt.0) then
          j=0
          jj=0
          do l=1,WidthBytes
            k=1
            m=BinStToInt(Radka(l),k,1)+1
            do k=1,3
              jj=jj+1
              Bitmap(jj,i)=PaletteColors(k,m)
            enddo
            jj=jj+1
            Bitmap(jj,i)=char(0)
          enddo
        else if(BitsPixelNew.eq.16) then
          l=0
          jj=0
          do j=1,Width
            l=l+1
            IBlue=nint(31.*float(ichar(Radka(l)))/255.)
            l=l+1
            IGreen=nint(31.*float(ichar(Radka(l)))/255.)
            l=l+1
            IRed=nint(31.*float(ichar(Radka(l)))/255.)
            if(kk.eq.4) l=l+1
            ii=IBlue+IGreen*32+IRed*1024
            jj=jj+1
            Bitmap(jj,i)=char(mod(ii,128))
            jj=jj+1
            Bitmap(jj,i)=char(ii/128)
          enddo
        else
          l=0
          jj=0
          do j=1,Width
            do k=1,4
              jj=jj+1
              if(k.le.kk) then
                l=l+1
                Bitmap(jj,i)=Radka(l)
              else
                Bitmap(jj,i)=char(0)
              endif
            enddo
          enddo
        endif
      enddo
      hBitmap=CreateBitmap(carg(Width),carg(Height),carg(1),
     1                     carg(BitsPixelNew),carg(offset(BitMap)))
      hDCp=CreateCompatibleDC(carg(hDC))
      call SelectObject(carg(hDCp),carg(hBitmap))
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      call BitBlt(carg(hDCComp),carg(ix1),carg(iy2),carg(Width),
     1            carg(Height),carg(hDCp),carg(0),carg(0),carg(SRCCOPY))
      call DeleteDC(carg(hDCp))
      call DeleteObject(carg(hBitmap))
      go to 9999
9000  t256='The file could not be opened.'
      go to 9900
9100  t256='Unknown label bitmap : "'//Label//'"'
      go to 9900
9200  write(Cislo,FormI15) HeaderSize
      call Zhusti(Cislo)
      t256='The header size of '//Cislo(:idel(Cislo))//
     1     ' bytes not implemeted in the program.'
      go to 9900
9300  write(Cislo,FormI15) bPlanes
      call Zhusti(Cislo)
      t256='The number of '//Cislo(:idel(Cislo))//
     1     ' color planes not implemeted in the program.'
      go to 9900
9400  write(Cislo,FormI15) Colors
      call Zhusti(Cislo)
      t256='The number of '//Cislo(:idel(Cislo))//
     1     ' colors in bytes not implemeted in the program.'
      go to 9900
9500  t256='Reading of compressed BMP files is n''t implemeted in the'//
     1     ' program.'
      go to 9900
9600  write(t256,FormI15) NPaletteColors
      call Zhusti(t256)
      write(Cislo,FormI15) Colors
      call Zhusti(Cislo)
      t256='The number of '//t256(:idel(t256))//
     1     ' palette colors doesn''t correspond to the number of '//
     2     Cislo(:idel(Cislo))//' colors in bytes.'
      go to 9900
9700  t256='Reading of BMP files arranged "bottom-up" isn''t '//
     1     'implemeted in the program.'
      go to 9900
9800  t256='Display has neither 32 nor 24 bitspixel.'
      go to 9900
9900  Veta='during import of the bitmap "'//FileName(:idel(FileName))//
     1     '"'
      call FeChybne(-1.,-1.,Veta,t256,SeriousError)
      ich=1
9999  if(ln.ne.0) i=FeCloseBinaryFile(ln)
      if(allocated(PaletteColors)) deallocate(PaletteColors)
      if(allocated(Radka)) deallocate(Radka,Bitmap)
      return
      end
      subroutine FeSavePCXFile(FileName,ix1,ix2,iy1,iy2,ich)
      use Jana_windows
      include 'fepc.cmn'
      character*(*) FileName
      character*256 t256
      character*80  Veta
      character*1   Bitmap(:,:),BitmapC(:),String(256)
      integer FeOpenBinaryFile,FeCloseBinaryFile,FeWriteBinaryFile,
     1        Width,Height
      allocatable Bitmap,BitmapC
      equivalence (t256,String)
      Width=iabs(ix2-ix1)+1
      Height=iabs(iy2-iy1)+1
      ix1p=min(ix1,ix2)
      iy2p=min(iy1,iy2)
      Veta=FileName
      lno=FeOpenBinaryFile(Veta(:idel(Veta)))
      if(lno.le.0) go to 9000
      call SetStringArrayTo(String,256,char(0))
      k=1
      i=10
      call IntToBinSt(i,1,t256,k)
      i=5
      call IntToBinSt(i,1,t256,k)
      i=1
      call IntToBinSt(i,1,t256,k)
      i=8
      call IntToBinSt(i,1,t256,k)
      i=0
      call IntToBinSt(i,2,t256,k)
      call IntToBinSt(i,2,t256,k)
      call IntToBinSt(Width-1,2,t256,k)
      call IntToBinSt(Height-1,2,t256,k)
      i=75
      call IntToBinSt(i,2,t256,k)
      call IntToBinSt(i,2,t256,k)
      k=k+49
      i=3
      call IntToBinSt(i,1,t256,k)
      i=Width
      call IntToBinSt(i,2,t256,k)
      i=1
      call IntToBinSt(i,2,t256,k)
      n=FeWriteBinaryFile(lno,String,128)
      allocate(Bitmap(Width,3),BitmapC(5*Width))
      call SelectObject(carg(hDCComp),carg(hDCBitmap))
      do iy=1,Height
        call SetStringArrayTo(Bitmap,3*Width,char(0))
        do ix=1,Width
          i=GetPixel(carg(hDCComp),carg(ix+ix1p-1),carg(iy+iy2p-1))
          Bitmap(ix,1)=char(mod(i,256))
          i=i/256
          Bitmap(ix,2)=char(mod(i,256))
          i=i/256
          Bitmap(ix,3)=char(mod(i,256))
        enddo
        do i=1,3
          call CompressRLE(Bitmap(1,i),Width,BitmapC,m)
          n=FeWriteBinaryFile(lno,BitmapC,m)
        enddo
      enddo
      go to 9999
9000  t256='The file could not be opened.'
      go to 9900
9900  Veta='during export of the bitmap to "'//
     1     FileName(:idel(FileName))//'"'
      call FeChybne(-1.,-1.,Veta,t256,SeriousError)
      ich=1
9999  if(allocated(Bitmap)) deallocate(Bitmap,BitmapC)
      if(lno.gt.0) then
        i=FeCloseBinaryFile(lno)
        i=LocateSubstring(FileName,'\',.false.,.false.)
        if(i.gt.0) Veta=FileName(i+1:)
        call FeResetReadOnly(Veta)
      endif
      return
      end
      subroutine CompressRLE(Bitmap,n,BitmapC,m)
      character*1 Bitmap(*),BitmapC(*)
      logical Konec
      i=0
      m=0
      Konec=.false.
1100  i=i+1
      if(i.gt.n) go to 5000
      LastByte=ichar(Bitmap(i))
      j=i
      nn=1
1200  j=j+1
       if(j.gt.n) then
        Konec=.true.
        go to 2000
      endif
      NewByte=ichar(Bitmap(j))
      if(NewByte.eq.LastByte) then
        nn=nn+1
        i=i+1
        if(nn.lt.63) go to 1200
      endif
2000  if(nn.lt.2.and.LastByte.lt.192) then
        m=m+1
        BitmapC(m)=char(LastByte)
      else
        m=m+1
        BitmapC(m)=char(192+nn)
        m=m+1
        BitmapC(m)=char(LastByte)
      endif
      if(.not.Konec) go to 1100
5000  return
      end

