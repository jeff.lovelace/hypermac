      function MEMCheckDivision(Pix)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (eps=0.00001)
      integer Pix(6),cv,os,i,ii
      logical   MEMCheckDivision
      real RPix(6),dummy
      MEMCheckDivision=.true.
      do 100 i=1,NDim(KPhase)
        RPix(i)=float(Pix(i))
100   continue
      do 500 cv=1,NLattVec(KPhase)
        do 600 i=1,NDim(KPhase)
          dummy=RPix(i)*vt6(i,cv,1,KPhase)
          if (abs(dummy-int(dummy)).ge.eps) then
            MEMCheckDivision=.false.
            return
          endif
600     continue
500   continue
      do 1100 os=1,NSymmN(KPhase)
        do 1200 i=1,NDim(KPhase)
          do 1300 ii=1,NDim(KPhase)
            dummy=RPix(ii)*rm6(ii+(i-1)*NDim(KPhase),os,1,KPhase)/
     1            RPix(i)
            if (abs(dummy-int(dummy)).ge.eps) then
              MEMCheckDivision=.false.
              return
            endif
1300      continue
          dummy=RPix(i)*s6(i,os,1,KPhase)
          if (abs(dummy-int(dummy)).ge.eps) then
            MEMCheckDivision=.false.
            return
          endif
1200    continue
1100  continue
      return
      end
