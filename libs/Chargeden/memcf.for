      subroutine MEMCF
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical CrwLogicQuest
      integer EdwStateQuest
      character*256 EdwStringQuest
      save nEdwDelta,nEdwWeak,nCrwIncCF,nCrwAutoRS,nEdwAutoRS,
     1     nCrwSymRecover,nCrwRelDelta,nCrwAbsDelta,nCrwAutoDelta,
     2     nCrwDefineDelta,nEdwBiso,nCrwSymNo,nCrwSymAverage
      real xqd
c!    MEMCFMake
      entry MEMCFMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,130.,il,'Include CF','L',CrwXd,CrwYd,
     1                      0,0)
      nCrwIncCF=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Delta','L','N')
      call FeQuestLblOn(LblLastMade)
      call FeQuestCrwMake(id,40.,il,130.,il,'automatic:','L',CrwgXd,
     1     CrwgYd,0,12)
      nCrwAutoDelta=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,40.,il,130.,il,'defined:','L',CrwgXd,
     1     CrwgYd,0,12)
      nCrwDefineDelta=CrwLastMade
      call FeQuestEdwMake(id,23.,il,150.,il,' ','L',50.,EdwYd,0)
      nEdwDelta=EdwLastMade
      call FeQuestCrwMake(id,220.,il,270.,il,'absolute','L',CrwgXd,
     1                       CrwgYd,0,10)
      nCrwAbsDelta=CrwLastMade
      call FeQuestCrwMake(id,290.,il,340.,il,'fraction','L',CrwgXd,
     1                       CrwgYd,0,10)
      nCrwRelDelta=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,130.,il,'%Weak reflections','L',50.,
     1                    EdwYd,0)
      nEdwWeak=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,130.,il,'%Biso','L',50.,EdwYd,0)
      nEdwBiso=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Automatic random seed','L',
     1                    CrwXd,CrwYd,1,0)
      nCrwAutoRS=CrwLastMade
      call FeQuestEdwMake(id,170.,il,250.,il,'Random seed:','L',50.,
     1                    EdwYd,0)
      nEdwAutoRS=EdwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Symmetry:','L','N')
      call FeQuestLblOn(LblLastMade)
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Do not search','L',CrwgXd,
     1                    CrwgYd,0,2)
      nCrwSymNo=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Shift origin','L',CrwgXd,
     1                    CrwgYd,0,2)
      nCrwSymRecover=CrwLastMade

      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Average over symmetry',
     1                    'L',CrwgXd,CrwgYd,0,2)
      nCrwSymAverage=CrwLastMade

c!    MEMCFOpen
      entry MEMCFOpen
      call FeQuestCrwOpen(nCrwIncCF,IncludeCF)
      call FeQuestCrwOpen(nCrwAutodelta,AutoDelta.eqv..true.)
      call FeQuestCrwOpen(nCrwDefineDelta,AutoDelta.eqv..false.)
      call FeQuestRealEdwOpen(nEdwDelta,MEMCFdelta,.false.,.false.)
      call FeQuestCrwOpen(nCrwAbsDelta,.not.MEMCFRelDelta)
      call FeQuestCrwOpen(nCrwRelDelta,MEMCFRelDelta)
      call FeQuestRealEdwOpen(nEdwWeak,MEMCFWeak,.false.,.false.)
      call FeQuestRealEdwOpen(nEdwBiso,Biso,.false.,.false.)
      call FeQuestCrwOpen(nCrwAutoRS,AutoRS)
      if(.not.AutoRS) then
        call FeQuestIntEdwOpen(nEdwAutoRS,MEMCFRS,.false.)
      else
        call FeQuestEdwClose(nEdwAutoRS)
      endif
      call FeQuestCrwOpen(nCrwSymNo,CFSymCode.eq.NoSym)
      call FeQuestCrwOpen(nCrwSymRecover,CFSymCode.eq.RecoverSym)
      call FeQuestCrwOpen(nCrwSymAverage,CFSymCode.eq.AverageSym)
      goto 9999

c!    MEMCFUpdate
      entry MEMCFUpdate
      if(CrwLogicQuest(nCrwIncCF)) then
        IncludeCF=.true.
      else
        IncludeCF=.false.
      endif
      if (CrwLogicQuest(nCrwAutoDelta)) then
        AutoDelta=.true.
      else
        AutoDelta=.false.
      endif
      if(EdwStringQuest(nEdwDelta).ne.' ') then
        call FeQuestRealFromEdw(nEdwDelta,MEMCFDelta)
      else
        MEMCFDelta=-1.0e30
      endif
      if (CrwLogicQuest(nCrwAbsDelta)) then
        MEMCFRelDelta=.false.
      else
        MEMCFRelDelta=.true.
      endif
      if(EdwStringQuest(nEdwWeak).ne.' ') then
        call FeQuestRealFromEdw(nEdwWeak,MEMCFWeak)
      else
        MEMCFWeak=-1.0e30
      endif
      if(EdwStringQuest(nEdwBiso).ne.' ') then
        call FeQuestRealFromEdw(nEdwBiso,Biso)
      else
        Biso=-1.0e30
      endif
      if (CrwLogicQuest(nCrwAutoRS)) then
        AutoRS=.true.
      else
        AutoRS=.false.
        call FeQuestIntFromEdw(nEdwAutoRS,MEMCFRS)
      endif
      if (CrwLogicQuest(nCrwSymRecover)) then
        CFSymCode=RecoverSym
      elseif (CrwLogicQuest(nCrwSymAverage)) then
        CFSymCode=AverageSym
      else
        CFSymCode=NoSym
      endif
      goto 9999
c!    MEMCFCheck
      entry MEMCFCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAutoRS) then
        if(.not.CrwLogicQuest(nCrwAutoRS)) then
          if(EdwStateQuest(nEdwAutoRS).ne.EdwOpened) then
            call FeQuestIntEdwOpen(nEdwAutoRS,MEMCFRS,.false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwAutoRS
        else
          call FeQuestIntFromEdw(nEdwAutoRS,MEMCFRS)
          call FeQuestEdwClose(nEdwAutoRS)
        endif
      endif
      goto 9999
c!    MEMCFComplete
      entry MEMCFComplete(okcheck)
      okcheck=0
      if (.not.IncludeCF) goto 9999
      if (MEMCFDelta.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Fill the value of delta',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwdelta
        goto 9999
      endif
      if (MEMCFDelta.le.0.0) then
        call FeChybne(-1.,-1.,'Delta must be a positive number',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDelta
        goto 9999
      endif
      if (MEMCFWeak.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Fill the proportion',
     1                        'of the weak reflections',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwWeak
        goto 9999
      endif
      if (MEMCFWeak.lt.0.0.or.MEMCFWeak.ge.1.) then
        call FeChybne(-1.,-1.,'Proportion of the weak reflections',
     1                        'must be a number between 0 and 1.',
     2                        SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwWeak
        goto 9999
      endif
      if (Biso.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Please give Biso',' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwBiso
        goto 9999
      endif
      if (.not.AutoRS.and.MEMCFRS.lt.0) then
        call FeChybne(-1.,-1.,'Random seed must be a positive integer',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwAutoRS
        goto 9999
      endif
9999  continue
      return
      end
