      subroutine MEMEDMA
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical CrwLogicQuest
      character*256 EdwStringQuest
      integer nEdwtstart(3),nEdwtend(3),nEdwtstep(3)
      save nCrwIncEDMA,nEdwInEDMA,nEdwOutEDMA,nButtSelAt,nCrwMaxAll,
     1     nCrwMaxAtoms,nCrwPosRel,nCrwPosAbs,nEdwTol,nCrwWrm40,
     2     nEdwwrm40,nCrwScAngst,nCrwScFract,nCrwproj,nEdwaddb,
     3     nEdwtstart,nEdwtend,nEdwtstep,nEdwChlimlist,nEdwChlimint,
     4     nEdwPlim,nCrwCharge,nLblPos,nLblt,nCrwfullcell,nCrwAbsChlim,
     5     nCrwRelChlim,nEdwtotatoms
c!    MEMEDMAMake
      entry MEMEDMAMake(id)
      xqd=QuestXMax(id)-QuestXMin(id)
      il=1
      call FeQuestCrwMake(id,5.,il,100.,il,'Include EDMA','L',
     1     CrwXd,CrwYd,0,0)
      nCrwIncEDMA=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'%Density file','L',xqd-105.,
     1                    EdwYd,0)
      nEdwInEDMA=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'%Output filebase','L',
     1                    xqd-105.,EdwYd,0)
      nEdwOutEDMA=EdwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Maxima:','L','N')
      call FeQuestCrwMake(id,60.,il,90.,il,'atoms','L',CrwgXd,
     1                    CrwgYd,1,7)
      nCrwMaxAtoms=CrwLastMade
      call FeQuestCrwMake(id,120.,il,135.,il,'all','L',CrwgXd,
     1                    CrwgYd,1,7)
      nCrwMaxAll=CrwLastMade
      il=il+1
      call FeQuestButtonMake(id,50.,il,90.,ButYd,'%Select atoms')
      nButtSelAt=ButtonLastMade
      il=il-1
      call FeQuestEdwMake(id,170.,il,230.,il,'%Tolerance','L',50.,
     1                    EdwYd,0)
      nEdwTol=EdwLastMade
      il=il+1
      ilpos=il
      call FeQuestLblMake(id,170.,il,'Position:','L','N')
      nLblPos=LblLastMade
      call FeQuestCrwMake(id,230.,il,280.,il,'absolute','L',CrwgXd,
     1                    CrwgYd,0,8)
      nCrwPosAbs=CrwLastMade
      call FeQuestCrwMake(id,310.,il,360.,il,'relative','L',CrwgXd,
     1                    CrwgYd,0,8)
      nCrwPosRel=CrwLastMade
      il=il-1
      call FeQuestCrwMake(id,170.,il,230.,il,'Create m40','L',
     1     CrwXd,CrwYd,1,0)
      nCrwWrm40=CrwLastMade
      call FeQuestEdwMake(id,250.,il,260.,il,'','L',xqd-265.,
     1                    EdwYd,0)
      nEdwwrm40=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,260.,il,'Number of atoms','L',
     1                    xqd-265.,EdwYd,0)
      nEdwtotatoms=EdwLastMade
      call FeQuestCrwMake(id,5.,il,120.,il,'List maxima in full cell',
     1                    'L',CrwXd,CrwYd,0,0)
      nCrwFullCell=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Scale:','L','N')
      call FeQuestCrwMake(id,50.,il,80.,il,'fract.','L',CrwgXd,
     1                    CrwgYd,0,9)
      nCrwScFract=CrwLastMade
      call FeQuestCrwMake(id,100.,il,120.,il,'A','L',CrwgXd,
     1                    CrwgYd,0,9)
      nCrwScAngst=CrwLastMade
      call FeQuestEdwMake(id,150.,il,190.,il,'Density limit','L',50.,
     1                    EdwYd,0)
      nEdwPlim=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,100.,il,'Integrate charge','L',CrwXd,
     1                    CrwYd,1,0)
      nCrwCharge=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'Charge limit','L',50.,EdwYd,
     1                    0)
      nEdwChlimlist=EdwLastMade
      call FeQuestCrwMake(id,180.,il,225.,il,'absolute','L',CrwgXd,
     1                       CrwgYd,0,12)
      nCrwAbsChlim=CrwLastMade
      call FeQuestCrwMake(id,250.,il,290.,il,'relative','L',CrwgXd,
     1                       CrwgYd,0,12)
      nCrwRelChlim=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'Fract. for c.o.c.','L',50.,
     1                    EdwYd,0)
      nEdwChlimint=EdwLastMade
      if (NDim(KPhase).eq.3) goto 9999
      il=il+1
      call FeQuestCrwMake(id,5.,il,80.,il,'Projection','L',CrwXd,CrwYd,
     1                    1,0)
      nCrwProj=CrwLastMade
      call FeQuestLblMake(id,150.,il,'t-sections:','L','N')
      call FeQuestLblOff(LblLastMade)
      nLblt=LblLastMade
      do 1000 i=1,NDimI(KPhase)
        call FeQuestEdwMake(id,210.,il,240.,il,'start','L',
     1       40.,EdwYd,0)
        nEdwtstart(i)=EdwLastMade
        call FeQuestEdwMake(id,290.,il,320.,il,'end','L',
     1       40.,EdwYd,0)
        nEdwtend(i)=EdwLastMade
        call FeQuestEdwMake(id,370.,il,400.,il,'step','L',
     1       40.,EdwYd,0)
        nEdwtstep(i)=EdwLastMade
        il=il+1
1000  continue
      call FeQuestEdwMake(id,5.,il,80.,il,'Add border','L',50.,EdwYd,0)
      nEdwaddb=EdwLastMade
      goto 9999
c!   MEMEDMAOpen
      entry MEMEDMAOpen
      call FeQuestCrwOpen(nCrwIncEDMA,IncludeEDMA)
      call FeQuestStringEdwOpen(nEdwInEDMA,
     1     MEMInEDMA(:idel(MEMInEDMA)))
      call FeQuestStringEdwOpen(nEdwOutEDMA,
     1     MEMOutEDMA(:idel(MEMOutEDMA)))
      call FeQuestCrwOpen(nCrwMaxAtoms,EDmaxima.eq.atms)
      call FeQuestCrwOpen(nCrwMaxAll,EDmaxima.eq.all)
      if(EDmaxima.eq.atms) then
        call FeQuestCrwClose(nCrwWrm40)
        call FeQuestCrwClose(nCrwFullCell)
        call FeQuestEdwClose(nEdwwrm40)
        call FeQuestLblOn(nLblPos)
        call FeQuestEdwClose(nEdwtotatoms)
        call FeQuestButtonOpen(nButtSelAt,ButtonOff)
        call FeQuestRealEdwOpen(nEdwTol,EDtolerance,.false.,.false.)
        call FeQuestCrwOpen(nCrwPosAbs,EDposition.eq.absol)
        call FeQuestCrwOpen(nCrwPosRel,EDposition.eq.relat)
      elseif(EDmaxima.eq.all) then
        call FeQuestButtonClose(nButtSelAt)
        call FeQuestEdwClose(nEdwTol)
        call FeQuestCrwClose(nCrwPosAbs)
        call FeQuestCrwClose(nCrwPosRel)
        call FeQuestLblOff(nLblPos)
        call FeQuestCrwOpen(nCrwWrm40,EDwrm40)
        call FeQuestCrwOpen(nCrwfullcell,EDfullcell)
        if (EDwrm40) then
          call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
          call FeQuestIntEdwOpen(nEdwtotatoms,totatoms,.false.)
        endif
      endif
      call FeQuestCrwOpen(nCrwScFract,EDscale.eq.fract)
      call FeQuestCrwOpen(nCrwScAngst,EDscale.eq.angst)
      call FeQuestCrwOpen(nCrwCharge,EDcharge)
      call FeQuestRealEdwOpen(nEdwplim,EDplim,.false.,.false.)
      if(EDcharge) then
        call FeQuestRealEdwOpen(nEdwchlimlist,
     1                          EDchlimlist,.false.,.false.)
        call FeQuestCrwOpen(nCrwAbsChlim,AbsChlim)
        call FeQuestCrwOpen(nCrwRelChlim,.not.AbsChlim)
        call FeQuestRealEdwOpen(nEdwchlimint,
     1                          EDchlimint,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwchlimlist)
        call FeQuestCrwClose(nCrwAbsChlim)
        call FeQuestCrwClose(nCrwRelChlim)
        call FeQuestEdwClose(nEdwchlimint)
      endif
      if (NDim(KPhase).eq.3) goto 9999
      call FeQuestCrwOpen(nCrwProj,EDprojection)
      do 1100 i=1,NDimI(KPhase)
        if(.not.EDprojection) then
          call FeQuestRealEdwOpen(nEdwtstart(i),
     1                        tstart(i),.false.,.false.)
          call FeQuestRealEdwOpen(nEdwtend(i),
     1                        tend(i),.false.,.false.)
          call FeQuestRealEdwOpen(nEdwtstep(i),
     1                        tstep(i),.false.,.false.)
        else
          call FeQuestEdwClose(nEdwtstart(i))
          call FeQuestEdwClose(nEdwtend(i))
          call FeQuestEdwClose(nEdwtstep(i))
        endif
1100  continue
      if (.not.EDprojection) then
        call FeQuestRealEdwOpen(nEdwaddb,EDaddb,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwaddb)
      end if
      goto 9999

c!    MEMEDMAUpdate
      entry MEMEDMAUpdate
      if(CrwLogicQuest(nCrwIncEDMA)) then
        IncludeEDMA=.true.
      else
        IncludeEDMA=.false.
      endif
      MEMInEDMA=EdwStringQuest(nEdwInEDMA)
      MEMOutEDMA=EdwStringQuest(nEdwOutEDMA)
      if(CrwLogicQuest(nCrwMaxAll)) then
        EDmaxima=all
        EDfullcell=CrwLogicQuest(nCrwfullcell)
        if (CrwLogicQuest(nCrwWrm40)) then
          EDwrm40=.true.
          EDm40file=EdwStringQuest(nEdwWrm40)
        endif
      elseif(CrwLogicQuest(nCrwMaxAtoms)) then
        EDmaxima=atms
        call FeQuestRealFromEdw(nEdwTol,EDtolerance)
        if(CrwLogicQuest(nCrwPosAbs)) then
          EDposition=absol
        elseif(CrwLogicQuest(nCrwPosrel))then
          EDposition=relat
        endif
      endif
      call FeQuestRealFromEdw(nEdwPlim,EDplim)
      if(CrwLogicQuest(nCrwCharge)) then
        EDcharge=.true.
        call FeQuestRealFromEdw(nEdwChlimlist,EDchlimlist)
        call FeQuestRealFromEdw(nEdwChlimint,EDchlimint)
        AbsChlim=CrwLogicQuest(nCrwAbsChlim)
      else
        EDcharge=.false.
      endif
      if (CrwLogicQuest(nCrwScFract)) then
        EDscale=fract
      else
        EDscale=angst
      endif
      if(NDim(KPhase).gt.3) then
        if (CrwLogicQuest(nCrwproj)) then
          EDprojection=.true.
        else
          EDprojection=.false.
          call FeQuestRealFromEdw(nEdwAddb,EDaddb)
          do 1900 i=1,NDimI(KPhase)
            call FeQuestRealFromEdw(nEdwtstart(i),tstart(i))
            call FeQuestRealFromEdw(nEdwtend(i),tend(i))
            call FeQuestRealFromEdw(nEdwtstep(i),tstep(i))
1900      continue
        endif
      endif
      goto 9999
c!    MEMEDMACheck
      entry MEMEDMACheck(id)
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelAt) then
        call FeQuestButtonOn(nButtSelAt)
        call iom40(0,0,fln(:ifln)//'.m40')
        if(allocated(AtBerEDMA)) deallocate(AtBerEDMA)
        allocate(AtBerEDMA(NAtInd))
        call SetLogicalArrayTo(AtBerEDMA,NAtInd,.true.)
        call SelAtoms('Select atoms from atomic part',Atom(1),
     1                AtBerEDMA(1),isf(1),NAtInd,ich)
        call FeQuestButtonOff(nButtSelAt)
      endif
      if (CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwMaxAtoms.or.
     1    CheckNumber.eq.nCrwMaxAll)) then
        if(CrwLogicQuest(nCrwMaxAtoms)) then
          EDmaxima=atms
          if (CrwLogicQuest(nCrwWrm40)) then
            EDwrm40=.true.
            EDm40file=EdwStringQuest(nEdwWrm40)
            CALL FeQuestIntFromEdw(nEdwtotatoms,TotAtoms)
          else
            EDwrm40=.false.
          endif
          call FeQuestCrwClose(nCrwWrm40)
          call FeQuestEdwClose(nEdwWrm40)
          call FeQuestEdwClose(nEdwtotatoms)
          EDfullcell=CrwLogicQuest(nCrwFullcell)
          call FeQuestCrwClose(nCrwfullcell)
          call FeQuestButtonOpen(nButtSelAt,ButtonOff)
          call FeQuestRealEdwOpen(nEdwTol,EDtolerance,.false.,.false.)
          call FeQuestLblOn(nLblPos)
          call FeQuestCrwOpen(nCrwPosAbs,EDposition.eq.absol)
          call FeQuestCrwOpen(nCrwPosRel,EDposition.eq.relat)
        elseif(CrwLogicQuest(nCrwMaxAll)) then
          EDmaxima=all
          call FeQuestRealFromEdw(nEdwTol,EDtolerance)
          if(CrwLogicQuest(nCrwPosAbs)) then
            EDposition=absol
          elseif(CrwLogicQuest(nCrwPosrel))then
            EDposition=relat
          endif
          call FeQuestButtonClose(nButtSelAt)
          call FeQuestEdwClose(nEdwTol)
          call FeQuestLblOff(nLblPos)
          call FeQuestCrwClose(nCrwPosAbs)
          call FeQuestCrwClose(nCrwPosRel)
          call FeQuestCrwOpen(nCrwWrm40,EDwrm40)
          call FeQuestCrwOpen(nCrwfullcell,EDfullcell)
          if (EDwrm40) then
            call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
            call FeQuestIntEdwOpen(nEdwtotatoms,totatoms,.false.)
          endif
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwWrm40) then
        if(CrwLogicQuest(nCrwWrm40)) then
          call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
          call FeQuestIntEdwOpen(nEdwtotatoms,Totatoms,.false.)
        else
          EDm40file=EdwStringQuest(nEdwWrm40)
          call FeQuestEdwClose(nEdwwrm40)
          CALL FeQuestIntFromEdw(nEdwTotatoms,TotAtoms)
          call FeQuestEdwClose(nEdwTotAtoms)
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwProj) then
        if(CrwLogicQuest(nCrwproj)) then
          call FeQuestLblOff(nLblt)
          do 2000 i=1,NDimI(KPhase)
            call FeQuestRealFromEdw(nEdwtstart(i),tstart(i))
            call FeQuestEdwClose(nEdwtstart(i))
            call FeQuestRealFromEdw(nEdwtend(i),tend(i))
            call FeQuestEdwClose(nEdwtend(i))
            call FeQuestRealFromEdw(nEdwtstep(i),tstep(i))
            call FeQuestEdwClose(nEdwtstep(i))
2000      continue
          call FeQuestRealFromEdw(nEdwaddb,EDaddb)
          call FeQuestEdwClose(nEdwaddb)
        else
          call FeQuestLblOn(nLblt)
          do 2010 i=1,NDimI(KPhase)
            call FeQuestRealEdwOpen(nEdwtstart(i),tstart(i),.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwtend(i),tend(i),.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwtstep(i),tstep(i),.false.,
     1           .false.)
2010      continue
          call FeQuestRealEdwOpen(nEdwaddb,EDaddb,.false.,.false.)
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCharge) then
        if(CrwLogicQuest(nCrwcharge)) then
          call FeQuestRealEdwOpen(nEdwchlimlist,
     1                        EDchlimlist,.false.,.false.)
          call FeQuestCrwOpen(nCrwAbsChlim,AbsChlim)
          call FeQuestCrwOpen(nCrwRelChlim,.not.AbsChlim)
          call FeQuestRealEdwOpen(nEdwchlimint,
     1                        EDchlimint,.false.,.false.)
        else
          call FeQuestRealFromEdw(nEdwchlimlist,EDchlimlist)
          call FeQuestEdwClose(nEdwchlimlist)
          AbsChlim=CrwLogicQuest(nCrwAbsChlim)
          call FeQuestCrwClose(nCrwAbsChlim)
          call FeQuestCrwClose(nCrwRelChlim)
          call FeQuestRealFromEdw(nEdwchlimint,EDchlimint)
          call FeQuestEdwClose(nEdwchlimint)
        endif
      endif
      goto 9999
c!    MEMEDMAComplete
      entry MEMEDMAComplete(okcheck)
      okcheck=0
      if (.not.IncludeEDMA) goto 9999
      if (idel(MEMInEDMA).eq.0) then
        call FeChybne(-1.,-1.,'The name of the density file',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwInEDMA
        okcheck=1
        goto 9999
      endif
      if (idel(MEMOutEDMA).eq.0) then
        call FeChybne(-1.,-1.,'The name of the output filebase',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwOutEDMA
        okcheck=1
        goto 9999
      endif
      if (EDwrm40.and.(idel(EDm40file).eq.0)) then
        call FeChybne(-1.,-1.,'The name of the m40 file',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwWrm40
        okcheck=1
        goto 9999
      endif
9999  continue
      return
      end
