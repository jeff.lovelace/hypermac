      subroutine MEMPresets(klic)
      use Basic_mod
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      parameter (eps=0.00001)
      real  ddiv,mindif,sh,minnel,maxnel
      integer n2max,n3max,n2b,n3b,cv,os,symfac(6),mxsymfac(6),klic
      MEMTitle=fln(1:ifln)
      if (klic.eq.1) then
        MEMName=fln(1:ifln)//'.inflip'
        MEMOutFile=fln(1:ifln)//'.m81 '//fln(1:ifln)//'.m80'
      else
        MEMName=fln(1:ifln)//'.BayMEM'
        MEMOutFile=fln(1:ifln)//'.m81'
      endif
      MEMOutPrior=fln(1:ifln)//'_prior.m81'
      k=0
      call kus(MEMOutFile,k,MEMInEDMA)
      MEMOutEDMA=fln(:ifln)
c!    Find the factors in the pixel division given by the symmetry
      do 400 cv=1,NLattVec(KPhase)
        do 500 os=1,NSymmN(KPhase)
          do 700 i=1,NDim(KPhase)
            sh=vt6(i,cv,1,KPhase)+s6(i,os,1,KPhase)
            symfac(i)=0
            mxsymfac(i)=0
800         symfac(i)=symfac(i)+1
            dummy=sh*float(symfac(i))
            if (abs(dummy-nint(dummy)).ge.eps) goto 800
            if (symfac(i).gt.mxsymfac(i)) mxsymfac(i)=symfac(i)
700       continue
500     continue
400   continue
      do 3100 i=1,3
        if (klic.lt.2) then
          ddiv=CellPar(i,1,KPhase)/.2
        elseif(klic.eq.2) then
          ddiv=CellPar(i,1,KPhase)/.08
        endif
        mindif=ddiv+2.
        n2max=int(log(ddiv)/log(2.))+1
        n3max=int(log(ddiv)/log(3.))+1
        do 3110 j2=0,n2max
          do 3120 j3=0,n3max
            dummy=float(2**j2*3**j3*mxsymfac(i))
            if (abs(dummy-ddiv).lt.mindif) then
              mindif=abs(dummy-ddiv)
              n2b=j2
              n3b=j3
            endif
3120      continue
3110    continue
        MEMDivision(i)=2**n2b*3**n3b*mxsymfac(i)
3100  continue
      do 3200 i=4,NDim(KPhase)
        MEMDivision(i)=16
3200  continue
      IncludeRefs=.true.
      RefsTypeCode=Refsobs
      if (klic.eq.0.or.klic.eq.2)then
        PerformCode=PerformMEM
        RefsSourceCode=Refsm80
        IncludeMEM=.true.
        IncludeCF=.false.
        IncludeEDMA=.false.
      elseif(klic.eq.1)then
        PerformCode=PerformCF
        IncludeRefs=.true.
        RefsSourceCode=Refsm90
        IncludeMEM=.false.
        IncludeCF=.true.
        IncludeEDMA=.true.
      endif
      if (klic.ne.2) then
        IncludePrior=.false.
      else
        IncludePrior=.true.
      endif

      MEMAlgCode=1
      MEMConOrder=2
      MEMConWeight=0


c!    Charge Flipping
      MEMCFDelta=1.
      MEMCFRelDelta=.false.
      AutoDelta=.true.
      MEMCFWeak=0.
      Biso=0.
      AutoRS=.true.
      MEMCFRS=1000
      CFSymCode=AverageSym
      MEMAim=1.
      MEMLambda=0.
      MEMRate=1.
      if(allocated(AtBer)) deallocate(AtBer)
      allocate(AtBer(NAtInd))
      call SetLogicalArrayTo(AtBer,NAtInd,.true.)
      if(klic.eq.2)then
        PriorCode=FromFile
      else
        PriorCode=Flat
      endif
      TwoChannel=.false.
      PriorFile=fln(1:ifln)//'_prior.m81'
      PriorFmt='jana'
      PriorSF=.false.
      PDCSinThMin=0.0
      PDCSinThMax=1.5
      PDCSigma=0.01
      PDCMaxSat=0
c!    for EDMA
      EDmaxima=all
      EDm40file=fln(1:ifln)//'.m40'
      EDtolerance=0.15
      EDposition=absol
      EDcharge=.true.
      if(klic.eq.1) then
        EDwrm40=.true.
        if(NAtFormula(KPhase).gt.0) then
          minnel=100
          maxnel=0
          do 3500i=1,NAtFormula(KPhase)
            minnel=min(minnel,AtNum(i,KPhase))
            maxnel=max(maxnel,AtNum(i,KPhase))
3500      continue
          EDchlimlist=0.3*minnel/maxnel
        else
          EDchlimlist=0.03
        endif
        AbsChlim=.false.
      else
        EDwrm40=.false.
        EDchlimlist=0.0
        AbsChlim=.true.
      endif
      EDchlimint=0.25
      EDplim=0.3
c      EDplim=1.5
      EDscale=fract
      EDprojection=.true.
      EDaddb=0.2
      EDfullcell=.false.
      do 4000 i=1,NDimI(KPhase)
        tstart(i)=0.0
        tstep(i)=0.1
        tend(i)=1.0
4000  continue
      if (klic.gt.0) then
        call FeDeferOutput
        call FePrepniListek(KartIdMEM-1)
        call MEMMEMOpen
        call FePrepniListek(KartIdCF-1)
        call MEMCFOpen
        call FePrepniListek(KartIdPrior-1)
        call MEMPriorOpen
        call FePrepniListek(KartIdEDMA-1)
        call MEMEDMAOpen
        call FePrepniListek(KartIdGeneral-1)
        call MEMGeneralOpen
        call FeReleaseOutput
      endif
9999  continue
      return
      end
