      subroutine MEMExport(KeyText)
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'memexport.cmn'
      character*80 t80
      character*256 KeyText
      logical CrwLogicQuest
      real xqd
      logical ExistFile
      if(NPhase.gt.1) then
        KPhaseIn=KPhase
        il=NPhase
        id=NextQuestId()
        xqd=200.
        t80='Select the phase for MEM export:'
        call FeQuestCreate(id,-1.,-1.,xqd,il,t80,0,LightGray,0,0)
        xpom=5.
        tpom=xpom+CrwgXd+15.
        do il=1,NPhase
          call FeQuestCrwMake(id,tpom,il,xpom,il,PhaseName(il),'L',
     1                        CrwXd,CrwYd,0,1)
          if(il.eq.1) nCrwPhase=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,il.eq.KPhase)
        enddo
1500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          do il=1,NPhase
            if(CrwLogicQuest(il)) then
              KPhase=il
              exit
            endif
          enddo
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      endif
      StringKey=KeyText
      if (StringKey.eq.' ') then
        call MEMPresets(0)
      else
        MEMName=StringKey
        StringKey=' '
      endif
      if (ExistFile(MEMName)) call NactiMEM
      xqd=500.
      linecnt=max(12,10+NDim(KPhase)/3)
      call FeKartCreate(-1.,-1.,xqd,linecnt,'Create MEM file',0,0)
      call FeCreateListek('General',1)
      KartIdGeneral=KartLastId
      call MEMGeneralMake(KartIdGeneral)
      call MEMGeneralOpen
      call FeCreateListek('MEM',1)
      KartIdMEM=KartLastId
      call MEMMEMMake(KartIdMEM)
      call MEMMEMOpen
      call FeCreateListek('Charge flip',1)
      KartIdCF=KartLastId
      call MEMCFMake(KartIdCF)
      call MEMCFOpen
      call FeCreateListek('Prior',1)
      KartIdPrior=KartLastId
      call MEMPriorMake(KartIdPrior)
      call MEMPriorOpen
      call FeCreateListek('EDMA',1)
      KartIdEDMA=KartLastId
      call MEMEDMAMake(KartIdEDMA)
      call MEMEDMAOpen
      call FeCompleteKart(1)
2500  ErrFlag=0
3000  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralUpdate
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMUpdate
        else if(KartId.eq.KartIdCF) then
          call MEMCFUpdate
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorUpdate
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMAUpdate
        endif
        call MEMGeneralComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdGeneral-1)
          goto 3000
        endif
        call MEMMEMComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdMEM-1)
          goto 3000
        endif
        call MEMCFComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdCF-1)
          goto 3000
        endif
        call MEMPriorComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdPrior-1)
          goto 3000
        endif
        call MEMEDMAComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdEDMA-1)
          goto 3000
        endif
        call WriteMEMFile
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralUpdate
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMUpdate
        else if(KartId.eq.KartIdCF) then
          call MEMCFUpdate
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorUpdate
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMAUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralCheck(ich)
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMCheck
        else if(KartId.eq.KartIdCF) then
          call MEMCFCheck
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorCheck
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMACheck(KartIdEdma)
        endif
        if (ich.eq.0) go to 3000
      end if
      call FeDestroyKart
      KeyText=StringKey
      if(allocated(AtBer)) deallocate(AtBer)
      if(allocated(AtBerEDMA)) deallocate(AtBerEDMA)
      if(NPhase.gt.1) KPhase=KPhaseIn
9999  return
      end
