      subroutine NactiMEM
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      parameter (nmemkwd=33)
      integer tit,voxel,perform,
     1           outputfile,fbegin,twoch,
     2           algorithm,conorder,conweight,
     3           initialdensity,initialfile,prsf,
     4           delta,weakratio,randomseed,
     5           searchsymmetry,outputprior,inputfile,
     6           outputbase,maxima,tolerance,plimit,
     7           chlimit,chlimlist,scale,coc,
     8           projection,writem40,tlist,addborder,
     9           position,fullcell,ibiso
      parameter (tit=1,voxel=2,perform=3,
     1           outputfile=4,fbegin=5,twoch=6,
     2           algorithm=7,conorder=8,conweight=9,
     3           initialdensity=10,initialfile=11,prsf=12,
     4           delta=13,weakratio=14,randomseed=15,
     5           searchsymmetry=16,outputprior=17,inputfile=18,
     6           outputbase=19,maxima=20,tolerance=21,plimit=22,
     7           chlimit=23,chlimlist=24,scale=25,coc=26,
     8           projection=27,writem40=28,tlist=29,addborder=30,
     9           position=31,fullcell=32,ibiso=33)
      integer bmf,NextLogicNumber,k
      real rpom(1)
      character*132 buffer,memkwdval(nmemkwd)
      character*18 memkwd(nmemkwd)
      integer memkwdln(nmemkwd)
      data memkwd /'title','voxel','perform','outputfile','fbegin',
     1             '2channel','algorithm','conorder','conweight',
     2             'initialdensity','initialfile','priorsf','delta',
     3             'weakratio','randomseed','searchsymmetry',
     4             'outputprior','inputfile','outputbase','maxima',
     5             'tolerance','plimit','chlimit','chlimlist','scale',
     6             'centerofcharge','projection','writem40','tlist',
     7             'addborder','position','fullcell','Biso'/
      data memkwdln/5,5,7,10,6,8,9,8,9,14,11,7,5,9,10,14,11,9,10,6,9,
     1              6,7,9,5,14,10,8,5,9,8,8,4/
      IncludeMEM=.false.
      IncludeCF=.false.
      IncludePrior=.false.
      do 900i=1,nmemkwd
        memkwdval(i)='not found'
900   continue
      bmf=NextLogicNumber()
      call OpenFile(bmf,MEMName,'formatted','unknown')
1000  read(bmf,FormA,end=8000) buffer
      do 2000 i=1,nmemkwd
      if(buffer(:memkwdln(i)).eq.memkwd(i)(:memkwdln(i))) then
        memkwdval(i)(1:)=buffer(memkwdln(i)+2:)
        if (idel(memkwdval(i)).eq.0) then
          if (i.eq.tlist) then
            do 1050 j=1,NDimI(KPhase)
              read(bmf,*) tstart(j),tend(j),tstep(j)
1050        continue
          endif
          goto 1000
        endif
1100    if (memkwdval(i)(1:1).eq.' ') then
          memkwdval(i)(1:)=memkwdval(i)(2:)
          goto 1100
        else
          goto 2000
        endif
      endif
2000  continue
      goto 1000
8000  continue
      close(bmf)
      if(memkwdval(tit).ne.'not found') then
        read(memkwdval(tit),FormA) MEMTitle
      endif
      if(memkwdval(voxel).ne.'not found') then
        read(memkwdval(voxel),*) (MEMDivision(i),i=1,NDim(KPhase))
      endif
      if(memkwdval(perform).ne.'not found') then
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'CF')
     1    PerformCode=1
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'MEM')
     1    PerformCode=2
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'CF+MEM')
     1    PerformCode=3
      endif
      if(memkwdval(outputfile).ne.'not found') then
        read(memkwdval(outputfile),FormA) MEMOutFile
      endif
      if(memkwdval(outputprior).ne.'not found') then
        IncludePrior=.true.
        read(memkwdval(outputprior),FormA) MEMOutPrior
      endif
      if(memkwdval(fbegin).ne.'not found') then
        IncludeRefs=.true.
      endif
      if(memkwdval(twoch).ne.'not found') then
        IncludeMEM=.true.
        if(memkwdval(twoch)(:idel(memkwdval(twoch))).eq.'yes')
     1    TwoChannel=.true.
        else
          TwoChannel=.false.
      endif
      if(memkwdval(algorithm).ne.'not found') then
        IncludeMEM=.true.
        if (memkwdval(algorithm)(:3).eq.'S-S')then
          MEMAlgCode=SaSa
          memkwdval(algorithm)(1:)=memkwdval(algorithm)(5:)
          if (idel(memkwdval(algorithm)).ne.0) then
            if (memkwdval(algorithm)(1:4).eq.'AUTO') then
              MEMLambda=0.0
              read(memkwdval(algorithm)(6:),*) MEMAim
            else
              read(memkwdval(algorithm),*) MEMLambda,MEMAim
            endif
          endif
        elseif(memkwdval(algorithm)(:6).eq.'MEMSys')then
          MEMAlgCode=MEMSys
          if (idel(memkwdval(algorithm)(7:)).ne.0)
     1      read(memkwdval(algorithm)(7:),*) i,i,MEMAim,MEMRate,dm
        endif
      endif
      if(memkwdval(conorder).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(conorder),*) MEMConOrder
      endif
      if(memkwdval(conweight).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(conweight)(2:),*) MEMConWeight
      endif
      if(memkwdval(initialdensity).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(initialdensity),FormA) PriorFmt
        if (PriorFmt(:idel(PriorFmt)).eq.'flat')then
          PriorCode=Flat
          PriorFmt='jana'
        else
          PriorCode=FromFile
        endif
      endif
      if(memkwdval(initialfile).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(initialfile),FormA) PriorFile
      endif
      if(memkwdval(prsf).ne.'not found') then
        IncludeMEM=.true.
        PriorSF=.true.
        if (NDim(KPhase).gt.3) then
          read(memkwdval(prsf),*)
     1         PDCSinThMin,PDCSinThMax,PDCSigma,PDCMaxSat
        else
          read(memkwdval(prsf),*)
     1         PDCSinThMin,PDCSinThMax,PDCSigma
        endif
      endif
      if(memkwdval(delta).ne.'not found') then
        memkwdval(delta)=memkwdval(delta)//'   '
        if (memkwdval(delta)(1:4).eq.'AUTO') then
          AutoDelta=.true.
        else
          AutoDelta=.false.
          read(memkwdval(delta),*) MEMCFDelta,Buffer
          if(Buffer.eq.'absolute') then
            MEMCFRelDelta=.false.
          elseif (Buffer.eq.'fraction') then
            MEMCFRelDelta=.true.
          endif
        endif
        IncludeCF=.true.
      endif
      if(memkwdval(weakratio).ne.'not found') then
        IncludeCF=.true.
        read(memkwdval(weakratio),*) MEMCFWeak
      endif
      if(memkwdval(weakratio).ne.'not found') then
        IncludeCF=.true.
        read(memkwdval(weakratio),*) Biso
      endif
      if(memkwdval(randomseed).ne.'not found') then
        IncludeCF=.true.
        if (memkwdval(randomseed)(1:4).eq.'AUTO') then
          AutoRS=.true.
        else
          AutoRS=.false.
          read(memkwdval(randomseed),*) MEMCFRS
        endif
      endif
      if(memkwdval(searchsymmetry).ne.'not found') then
        IncludeCF=.true.
        if(memkwdval(searchsymmetry)(1:5).eq.'shift') then
          CFSymCode=RecoverSym
        elseif(memkwdval(searchsymmetry)(1:7).eq.'average') then
          CFSymCode=AverageSym
        else
          CFSymCode=NoSym
        endif
      endif
c! EDMA
      if(memkwdval(inputfile).ne.'not found') then
        IncludeEDMA=.true.
        read(memkwdval(inputfile),FormA) MEMInEDMA
      endif
      if(memkwdval(outputbase).ne.'not found') then
        IncludeEDMA=.true.
        read(memkwdval(outputbase),FormA) MEMOutEDMA
      endif
      if(memkwdval(maxima).ne.'not found') then
        IncludeEDMA=.true.
        if(memkwdval(maxima).eq.'atoms') then
          EDmaxima=atms
        elseif(memkwdval(maxima).eq.'all') then
          EDmaxima=all
        endif
      endif
      if(memkwdval(scale).ne.'not found') then
        if(memkwdval(scale).eq.'angstrom') then
          EDscale=angst
        elseif(memkwdval(scale).eq.'fractional') then
          EDscale=fract
        endif
      endif
      if(memkwdval(writem40).ne.'not found') then
        IncludeEDMA=.true.
        EDm40file=memkwdval(writem40)(:idel(memkwdval(writem40)))
      endif
      if(memkwdval(tolerance).ne.'not found') then
        read(memkwdval(tolerance),*) EDtolerance
      endif
      if(memkwdval(position).ne.'not found') then
        if(memkwdval(position).eq.'absolute') then
          EDposition=absol
        elseif(memkwdval(position).eq.'relative') then
          EDposition=relat
        endif
      endif
      if(memkwdval(coc).ne.'not found') then
        if(memkwdval(coc).eq.'yes') then
          EDcharge=.true.
        else
          EDcharge=.false.
        endif
      endif
      if(memkwdval(fullcell).ne.'not found') then
        if(memkwdval(fullcell).eq.'yes') then
          EDfullcell=.true.
        else
          EDfullcell=.false.
        endif
      endif
      if(memkwdval(plimit).ne.'not found') then
        read(memkwdval(plimit),*) EDplim
      endif
      if(memkwdval(chlimit).ne.'not found') then
        read(memkwdval(chlimit),*) EDchlimint
      endif
      if(memkwdval(chlimlist).ne.'not found') then
        IncludeEDMA=.true.
        k=0
        call StToReal(memkwdval(chlimlist),k,rpom,1,.false.,ich)
        EDchlimlist=rpom(1)
        if (k.lt.len(memkwdval(chlimlist)))then
          call Kus(memkwdval(chlimlist),k,Buffer)
          if(Buffer.eq.'absolute') then
            AbsChlim=.true.
          elseif (Buffer.eq.'relative') then
            AbsChlim=.false.
          endif
        endif
      endif
      if(memkwdval(addborder).ne.'not found') then
        read(memkwdval(addborder),*) EDaddb
      endif
      if(memkwdval(projection).ne.'not found') then
        if(memkwdval(projection).eq.'yes') then
          EDprojection=.true.
        else
          EDprojection=.false.
        endif
      endif
      if(memkwdval(coc).ne.'not found') then
        if(memkwdval(coc).eq.'yes') then
          EDcharge=.true.
        else
          EDcharge=.false.
        endif
      endif
9999  continue
      return
      end
