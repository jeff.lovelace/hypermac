      subroutine MEMGeneral
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest,ExistFile,FeYesNo,MEMCheckDivision
      integer vyskoc
      save nEdwTitle,nEdwName,nEdwDivision,nEdwOutFile,
     1     nCrwPerformCF,nCrwPerformMEM,nCrwPerformCFMEM,
     2     nCrwIncludeRefs,nCrwRefsm80,nCrwRefsm90,nCrwRefsobs,
     3     nCrwRefscalc,nButtStructSol,nButtMEMwithPR
      character*256 EdwStringQuest,t256
      real xqd

      entry MEMGeneralMake(id)
      xqd=QuestXMax(id)-QuestXMin(id)
      il=1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Title','L',xqd-85.,EdwYd,0)
      nEdwTitle=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%File name','L',xqd-85.,
     1                    EdwYd,1)
      nEdwName=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Pixel Division','L',
     1                    xqd-85.,EdwYd,0)
      nEdwDivision=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Output file','L',
     1                    xqd-85.,EdwYd,0)
      nEdwOutFile=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,5.,il,'Perform:','L','B')
      call FeQuestLblOn(LblLastMade)
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'Charge flipping:','L',
     1                    CrwgXd,CrwgYd,0,1)
      nCrwPerformCF=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'MEM:','L',CrwgXd,
     1                    CrwgYd,0,1)
      nCrwPerformMEM=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'Charge flipping + MEM:','L',
     1                    CrwgXd,CrwgYd,0,1)
      nCrwPerformCFMEM=CrwLastMade

      il=il-3
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+150.,il,
     1                    'Include structure factors:','L',CrwXd,CrwYd,
     2                    1,0)
      nCrwIncludeRefs=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+100.,il,'Phased (m80)',
     1                    'L',CrwgXd,CrwgYd,1,5)
      nCrwRefsm80=CrwLastMade
      call FeQuestCrwMake(id,xqd*.5+155.,il,xqd*.5+180.,il,'Obs','L',
     1                    CrwgXd,CrwgYd,0,6)
      nCrwRefsobs=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+100.,il,'Unphased (m90)',
     1                     'L',CrwgXd,CrwgYd,1,5)
      nCrwRefsm90=CrwLastMade
      call FeQuestCrwMake(id,xqd*.5+155.,il,xqd*.5+180.,il,'Calc'
     1                    ,'L',CrwgXd,CrwgYd,0,6)
      nCrwRefscalc=CrwLastMade
      il=il+2
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,5.,il,'Presets:','L','B')
      call FeQuestLblOn(LblLastMade)
      call FeQuestButtonMake(id,80.,il,100.,ButYd,'%Structure solution')
      nButtStructSol=ButtonLastMade
      call FeQuestButtonMake(id,200.,il,100.,ButYd,'%MEM with prior')
      nButtMEMwithPR=ButtonLastMade
      goto 9999

c!    MEMGeneralOpen
      entry MEMGeneralOpen
      call FeQuestStringEdwOpen(nEdwTitle,MEMTitle(1:idel(MEMTitle)))
      call FeQuestStringEdwOpen(nEdwName,MEMName(1:idel(MEMName)))
      call FeQuestIntAEdwOpen(nEdwDivision,MEMDivision,NDim(KPhase),
     1                          .false.)
      call FeQuestStringEdwOpen(nEdwOutFile,MEMOutFile)
      call FeQuestCrwOpen(nCrwPerformCF,PerformCode.eq.PerformCF)
      call FeQuestCrwOpen(nCrwPerformMEM,PerformCode.eq.PerformMEM)
      call FeQuestCrwOpen(nCrwIncludeRefs,IncludeRefs)
      call FeQuestCrwOpen(nCrwPerformCFMEM,PerformCode.eq.PerformCFMEM)
      if (Includerefs) then
        call FeQuestCrwOpen(nCrwRefsm80,RefsSourceCode.eq.Refsm80)
        call FeQuestCrwOpen(nCrwRefsm90,RefsSourceCode.eq.Refsm90)
        if (RefsSourceCode.eq.Refsm80) then
          call FeQuestCrwOpen(nCrwRefsobs,RefsTypeCode.eq.Refsobs)
          call FeQuestCrwOpen(nCrwRefscalc,RefsTypeCode.eq.Refscalc)
        else
          call FeQuestCrwClose(nCrwRefsobs)
          call FeQuestCrwClose(nCrwRefscalc)
        endif
      else
        call FeQuestCrwClose(nCrwRefsm80)
        call FeQuestCrwClose(nCrwRefsm90)
        call FeQuestCrwClose(nCrwRefsobs)
        call FeQuestCrwClose(nCrwRefscalc)
      endif
      call FeQuestButtonOpen(nButtStructSol,ButtonOff)
      if(isPowder) call FeQuestButtonDisable(nButtStructSol)
      call FeQuestButtonOpen(nButtMEMwithPR,ButtonOff)
      goto 9999

c!    MEMGeneralUpdate
      entry MEMGeneralUpdate
      MEMTitle=EdwStringQuest(nEdwTitle)
      MEMName=EdwStringQuest(nEdwName)
      MEMOutFile=EdwStringQuest(nEdwOutFile)
      IncludeRefs=CrwLogicQuest(nCrwIncludeRefs)
      if(EdwStringQuest(nEdwDivision).ne.' ') then
        call FeQuestIntAFromEdw(nEdwDivision,MEMDivision)
      else
        MEMDivision(1)=-111112
      endif
      do 1000 i=nCrwPerformCF,nCrwPerformCF+2
        if (CrwLogicQuest(i)) PerformCode=i-nCrwPerformCF+1
1000  continue
      IncludeRefs=CrwLogic(nCrwIncludeRefs)
      if (CrwLogicQuest(nCrwRefsm80)) then
        if (CrwLogicQuest(nCrwRefsObs))then
          RefsTypeCode=RefsObs
        else
          RefsTypeCode=RefsCalc
        endif
        RefsSourceCode=Refsm80
      else
        RefsSourceCode=Refsm90
      endif
      goto 9999
c!    MEMGeneralCheck
      entry MEMGeneralCheck(vyskoc)
      vyskoc=0
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtStructSol)
     1    then
        call FeQuestButtonOn(nButtStructSol)
        call MEMPresets(1)
        call FeQuestButtonOff(nButtStructSol)
      elseif(CheckType.eq.EventButton.and.
     1       EventNumber.eq.nButtMEMwithPR) then
        call FeQuestButtonOn(nButtMEMwithPR)
        call MEMPresets(2)
        call FeQuestButtonOff(nButtMEMwithPR)
      endif
      if (CheckType.eq.EventCrw.and.Checknumber.eq.nCrwIncludeRefs)then
        if(CrwLogic(nCrwIncludeRefs))then
          call FeQuestCrwOpen(nCrwRefsm80,RefsSourceCode.lt.Refsm90)
          call FeQuestCrwOpen(nCrwRefsm90,RefsSourceCode.eq.Refsm90)
          if (CrwLogic(nCrwRefsm80)) then
            call FeQuestCrwOpen(nCrwRefsobs,RefsTypeCode.eq.Refsobs)
            call FeQuestCrwOpen(nCrwRefscalc,RefsTypeCode.eq.Refscalc)
          endif
        else
          if (CrwLogicQuest(nCrwRefsm80))then
            if (CrwLogicQuest(nCrwRefsObs))then
              RefsTypeCode=RefsObs
            else
              RefsTypeCode=RefsCalc
            endif
            RefsSourceCode=Refsm80
          else
            RefsSourceCode=Refsm90
          endif
          call FeQuestCrwClose(nCrwRefsm80)
          call FeQuestCrwClose(nCrwRefsm90)
          call FeQuestCrwClose(nCrwRefsobs)
          call FeQuestCrwClose(nCrwRefscalc)
        endif
      endif
      if (CheckType.eq.EventCrw.and.(Checknumber.eq.nCrwRefsm80.or.
     1    CheckNumber.eq.nCrwRefsm90)) then
        if (CrwLogicQuest(nCrwRefsm80)) then
          call FeQuestCrwOpen(nCrwRefsObs,RefsTypeCode.eq.RefsObs)
          call FeQuestCrwOpen(nCrwRefsCalc,RefsTypeCode.eq.RefsCalc)
          RefsSourceCode=Refsm80
        else
          if (CrwLogicQuest(nCrwRefsObs))then
            RefsTypeCode=RefsObs
          else
            RefsTypeCode=RefsCalc
          endif
          call FeQuestCrwClose(nCrwRefsObs)
          call FeQuestCrwClose(nCrwRefsCalc)
          RefsSourceCode=Refsm90
        endif
      endif
      if (CheckType.eq.EventEdw.and.Checknumber.eq.nEdwName)then
        t256=EdwStringQuest(nEdwName)
        if (t256.ne.MEMName) then
          MEMName=t256
          if (ExistFile(MEMName)) then
            if(FeYesNo(-1.,-1.,'The MEM input file exists. '//
     1             'Read the file?',1)) then
              StringKey=MEMName
              vyskoc=1
            endif
          endif
        endif
      endif
      goto 9999
c!    MEMGeneralComplete
      entry MEMGeneralComplete(okcheck)
      okcheck=0
      if (idel(MEMName).eq.0) then
        call FeChybne(-1.,-1.,'The filename must not be empty!',
     1                        ' ',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwName
        okcheck=1
        goto 9999
      endif
      if (idel(MEMOutFile).eq.0) then
        call FeChybne(-1.,-1.,'The name of the output file',
     1                        'must not be empty!',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwOutFile
        goto 9999
      endif
      if (MEMDivision(1).le.-111112) then
        call FeChybne(-1.,-1.,'Define the division',
     1                        'of the grid.',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDivision
        goto 9999
      endif
      do 9900 i=1,NDim(KPhase)
        if (MEMDivision(i).le.0) then
          call FeChybne(-1.,-1.,'The number of pixels',
     1                          'must be positive.',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwDivision
          goto 9999
        endif
9900  continue
      if (MEMCheckDivision(MEMDivision).eqv..false.) then
        call FeChybne(-1.,-1.,'The pixel division is inconsistent',
     1                        'with the symmetry.',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDivision
        goto 9999
      endif
9999  continue
      return
      end
