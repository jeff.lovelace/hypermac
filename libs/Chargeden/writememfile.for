      subroutine WriteMEMFile
      use Basic_mod
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'memexport.cmn'
      character*16 VoxFmt
      character*30 symmcp(:,:)
      character*80 t80
      integer CrlCentroSymm
      allocatable symmcp
      if(allocated(symmcp)) deallocate(symmcp)
      allocate(symmcp(NDim(KPhase),NSymmN(KPhase)))
      VoxFmt='(''voxel '', i4)'
      write(VoxFmt(11:11),'(i1)') NDim(KPhase)
      open(MEMFileId,file=MEMName)
c!    General Information
      write(MEMFileId,'(2a)') 'title ',
     1      MEMTitle(1:idel(MEMTitle))
      if(PerformCode.eq.PerformMEM)then
        t80='perform MEM'
      elseif(PerformCode.eq.PerformCFMEM)then
        t80='perform CF+MEM'
      else if(SuperflipAlgorithm.eq.1) then
        t80='perform CF'
      else if(SuperflipAlgorithm.eq.2) then
        t80='perform LDE'
      else if(SuperflipAlgorithm.eq.3) then
        t80='perform AAR'
      endif
      write(MEMFileId,FormA) t80(1:idel(t80))
      if(SuperflipStartModel.eq.2)
     1  write(MEMFileId,FormA) 'modelfile superposition'
      write(MEMFileId,'(''outputfile '',a)')
     1        MEMOutfile(1:idel(MEMOutFile))
      write(MEMFileId,'(''outputformat jana'')')
      write(MEMFileId,'(a,i3)') 'dimension',NDim(KPhase)
      if(PerformCode.ne.PerformCF)
     1  write(MEMFileId,VoxFmt) (MEMDivision(i),i=1,NDim(KPhase))
      write(MEMFileId,'(a,3f10.4,3f8.2)')'cell ',
     1                (CellPar(i,1,KPhase),i=1,6)
      if (NDim(KPhase).gt.3) then
        write(MEMFileId,'(''qvectors'')')
        do 1000 i=1,NDimI(KPhase)
          write(MEMFileId,'(3f14.9)') (qu(j,i,1,KPhase),j=1,3)
1000    continue
        write(MEMFileId,'(''endqvectors'')')
      endif
      write(MEMFileId,'(2a)') 'spacegroup ',Grupa(KPhase)
        if(CrlCentroSymm().gt.0) then
          write(MEMFileId,'(''centro yes'')')
        else
          write(MEMFileId,'(''centro no'')')
        endif
      write(MEMFileId,'(''centers'')')
        do 1200j=1,NLattVec(KPhase)
          write(MEMFileId,'(6f10.6)')
     1      (vt6(k,j,1,KPhase),k=1,NDim(KPhase))
1200    continue
      write(MEMFileId,'(''endcenters'')')
      write(MEMFileId,'(''symmetry'')')
        n=0
        do 1400j=1,NSymmN(KPhase)
          call codesymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                  symmcp(1,j),1)
          do 1300k=1,NDim(KPhase)
            n=max(n,idel(symmcp(k,j))+1)
1300      continue
1400    continue
        if(n.lt.5) n=5
        do 1600j=1,NSymmN(KPhase)
          t80=' '
          k=1
          do 1500l=1,NDim(KPhase)
            t80(k+n-idel(symmcp(l,j)):)=symmcp(l,j)
            k=k+n
1500      continue
          write(MEMFileId,FormA)t80(1:Idel(t80))
1600    continue
      write(MEMFileId,'(''endsymmetry'')')
c!    MEM-specific
      if (includeMEM) then
        write(MEMFileId,*)
        write(MEMFileId,'(''# MEM-specific keywords'')')
        if (MEMAlgCode.eq.SaSa) then
          if (MEMlambda.eq.0.0.and.MEMAim.eq.1.0)then
            write(MEMFileId,'(''algorithm S-S'')')
          elseif(MEMLambda.eq.0.0)then
            write(MEMFileId,'(''algorithm S-S AUTO '',f6.3)') MEMAim
          else
            write(MEMFileId,'(''algorithm S-S '',2f6.3)')
     1            MEMLambda,MEMAim
          endif
        elseif (MEMAlgCode.eq.MEMSys)then
          if (MEMRate.eq.1.0.and.MEMAim.eq.1.0)then
            write(MEMFileId,'(''algorithm MEMSys'')')
          else
           write(MEMFileId,
     1     '(''algorithm MEMSys 4 1'',2f7.3,'' 0.05'')') MEMAim,MEMRate
          endif
        endif
        if (MEMConOrder.ne.2) write(MEMFileId,'(''conorder '',i1)')
     1                              MEMConOrder
        if (MEMConWeight.ne.0) write(MEMFileId,'(''conweight H'',i1)')
     1                              MEMConWeight
        if (PriorCode.eq.Flat) then
          write(MEMFileId,'(''initialdensity flat'')')
        elseif (PriorCode.eq.FromFile) then
          write(MEMFileId,'(''initialdensity '',a)')
     1          PriorFmt(1:idel(PriorFmt))
          write(MEMFileId,'(''initialfile '',a)')
     1          PriorFile(1:idel(PriorFile))
          if (TwoChannel) write(MEMFileId,'(''2channel yes'')')
          if (PriorSF) then
            if(NDim(KPhase).gt.3)then
              write(MEMFileId,'(''priorsf '',3f6.3,i2)')
     1              PDCSinThMin,PDCSinThMax,PDCSigma,PDCMaxSat
            else
              write(MEMFileId,'(''priorsf '',3f7.3)')
     1              PDCSinThMin,PDCSinThMax,PDCSigma
            endif
          endif
        endif
        write(MEMFileId,'(''# End of MEM-specific keywords'')')
      endif
c!    Charge flipping
      if (includeCF) then
        if(NAtFormula(KPhase).gt.0) then
          t80='composition'
          do i=1,NAtFormula(KPhase)
            write(Cislo,FormI15)
     1        nint(AtMult(i,KPhase)*float(NUnits(KPhase)))
            call Zhusti(Cislo)
            t80=t80(:idel(t80))//' '//
     1          AtType(i,KPhase)(:idel(AtType(i,KPhase)))//
     2          Cislo(:idel(Cislo))
          enddo
          write(MEMFileId,FormA) t80(:idel(t80))
        endif
        write(MEMFileId,*)
        write(MEMFileId,'(''# Keywords for charge flipping'')')
        if(SuperflipLocalNorm)
     1    write(MEMFileId,FormA) 'normalize local'
        if(SuperflipKeepTerminal)
     1    write(MEMFileId,FormA) 'terminal yes keep'
        if(SuperflipRepeatMode.eq.-1) then
          write(MEMFileId,FormA) 'repeatmode nosuccess'
        else if(SuperflipRepeatMode.eq.1) then
          write(Cislo,FormI15) SuperflipRepeatMax
          call Zhusti(Cislo)
          write(MEMFileId,FormA) 'repeatmode '//Cislo(:idel(Cislo))
        endif
        if(SuperflipRepeatMode.ne.0)
     1    write(MEMFileId,FormA) 'bestdensities 1 symmetry'
        write(MEMFileId,FormA) 'polish yes'
        write(Cislo,FormI15) SuperflipMaxCycles
        call zhusti(Cislo)
        write(MEMFileId,'(''maxcycles '',a)') Cislo(:idel(Cislo))
        if(AutoDelta) then
          if(SuperflipDelta.le.0.) then
            write(MEMFileId,'(''delta AUTO'')')
          else
            write(Cislo,'(f8.4)') SuperFlipDelta
            call ZdrcniCisla(Cislo,1)
            write(MEMFileId,'(''delta '',a,'' sigma'')')
     1        Cislo(:idel(Cislo))
          endif
        else
          if (MEMCFRelDelta) then
            t80=' fraction'
          else
            t80=' absolute'
          endif
          write(MEMFileId,'(''delta '',f6.3,a)') MEMCFDelta,
     1         t80(:idel(t80))
        endif
        write(MEMFileId,'(''weakratio '',f5.3)') MEMCFWeak
        write(MEMFileId,'(''Biso '',f7.3)') Biso
        if(AutoRS) then
          write(MEMFileID,'(''randomseed AUTO'')')
        else
          write(t80,'(''randomseed '',i15)') MEMCFRS
          call ZdrcniCisla(t80,2)
          write(MEMFileID,FormA) t80(:idel(t80))
        endif
        if (CFSymCode.eq.NoSym)then
          t80='searchsymmetry no'
        elseif (CFSymCode.eq.RecoverSym)then
          t80='searchsymmetry shift'
        elseif (CFSymCode.eq.AverageSym)then
          t80='searchsymmetry average'
        endif
        write(MEMFileId,FormA) t80(1:idel(t80))
        t80='derivesymmetry yes'
        write(MEMFileId,FormA) t80(1:idel(t80))
        if(Radiation(KDatBlock).eq.ElectronRadiation)
     1    write(MEMFileId,FormA) 'missing float 0.5'
        write(MEMFileId,'(''# End of keywords for charge flipping'')')
      endif
c!    Prior
      if (IncludePrior) then
        call iom40(0,0,fln(:ifln)//'.m40')
        write(MEMFileId,*)
        write(MEMFileId,'(''# Prior-specific keywords'')')
        write(MEMFileId,'(''outputprior '',a)')
     1        MEMOutPrior(:idel(MEMOutPrior))
        write(MEMFileId,'(''prioratoms'')')
        do 3000i=1,NAtInd
          if (Atber(i))
     1      write(MEMFileId,'(a8,a2,2X,4f10.6,2x,6f10.6)') atom(i),
     2                    AtType(isf(i),KPhase),ai(i),(x(j,i),j=1,3),
     3                    (beta(j,i)/urcp(j,1,KPhase),j=1,6)
3000    continue
        write(MEMFileId,'(''endprioratoms'')')
        write(MEMFileId,'(''# End of Prior-specific keywords'')')
      endif
c!    EDMA
      if (IncludeEDMA) then
        write(MEMFileId,*)
        write(MEMFileId,'(''# EDMA-specific keywords'')')
        write(MEMFileId,'(''inputfile '',a)')
     1        MEMInEDMA(:idel(MEMInEDMA))
        write(MEMFileId,'(''outputbase '',a)')
     1        MEMOutEDMA(:idel(MEMOutEDMA))
        if (EDwrm40) then
          write(MEMFileId,'(''m40forjana yes'')')
          write(MEMFileId,'(''writem40 '',a)')
     1               EDm40file(1:idel(EDm40file))
        endif
        if (EDmaxima.eq.atms) then
          write(MEMFileId,'(''maxima atoms'')')
          write(MEMFileId,'(''edmaatoms'')')
          do 3100i=1,NAtInd
            if (AtberEDMA(i))
     1        write(MEMFileId,'(a8,2X,3f10.6)') atom(i),(x(j,i),j=1,3)
3100      continue
          write(MEMFileId,'(''endedmaatoms'')')
          if (EDposition.eq.absol) then
            write(MEMFileId,'(''position absolute'')')
          elseif (EDposition.eq.relat) then
            write(MEMFileId,'(''position relative'')')
          endif
          write(MEMFileId,'(''tolerance '',f6.3)') EDtolerance
        elseif (EDmaxima.eq.all) then
          write(MEMFileId,'(''maxima all'')')
        endif
        if (EDfullcell) then
          write(MEMFileId,'(''fullcell yes'')')
        else
          write(MEMFileId,'(''fullcell no'')')
        endif
        if (EDscale.eq.fract) then
          write(MEMFileId,'(''scale fractional'')')
        elseif (EDmaxima.eq.angst) then
          write(MEMFileId,'(''scale angstrom'')')
        endif
c        write(MEMFileId,'(''plimit '',f9.4)') EDplim
        write(MEMFileId,'(''plimit '',f9.4,'' sigma'')') EDplim
        if(EDAtoms.gt.0) then
          write(Cislo,FormI15) EDAtoms
          call Zhusti(Cislo)
        else
          Cislo='composition'
        endif
        t80='numberofatoms '//Cislo(:idel(Cislo))
        write(MEMFileId,FormA) t80(:idel(t80))
        if (EDcharge) then
          write(MEMFileId,'(''centerofcharge yes'')')
          write(MEMFileId,'(''chlimit '',f9.4)') EDchlimint
          if (AbsChlim) then
            write(MEMFileId,'(''chlimlist '',f9.4,'' absolute'')')
     1                        EDchlimlist
          else
            write(MEMFileId,'(''chlimlist '',f9.4,'' relative'')')
     1                        EDchlimlist
          endif
        elseif (EDmaxima.eq.all) then
          write(MEMFileId,'(''centerofcharge no'')')
        endif
        if (NDim(KPhase).gt.3) then
          if (EDprojection) then
            write(MEMFileId,'(''projection yes'')')
          else
            write(MEMFileId,'(''addborder '',f7.4)') EDaddb
            write(MEMFileId,'(''tlist'')')
            do3200 i=1,NDimI(KPhase)
              write(MEMFileId,'(3f6.2)') tstart(i),tend(i),tstep(i)
3200        continue
            write(MEMFileId,'(''endtlist'')')
          endif
        endif

        write(MEMFileId,'(''# End of EDMA-specific keywords'')')
      endif


c!    Reflection list
      if (IncludeRefs) then
        write(MEMFileId,*)
        call ExportRefl2MEM
      endif
      close(MEMFileId)
9999  continue
      if(allocated(symmcp)) deallocate(symmcp)
      return
      end
