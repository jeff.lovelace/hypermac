      subroutine ExportRefl2MEM
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'memexport.cmn'
      integer ihkl(6),ph
      real Fo,Fc,A00,B00,sig,dm,Iobs,sigIobs
      character*256 t256
      character*80 t80
      character*22 formatm80,formatBM,formatm90
      logical ExistFile,EqIgCase
      data formatm80/'( i4,13e12.5)'/
      data formatBM/'( i4,3f14.7)'/
      data formatm90/'( i4,2f9.1,3i4)'/
      write(formatm80(2:2),'(i1)') NDim(KPhase)+1
      write(formatm90(2:2),'(i1)') NDim(KPhase)
      write(formatBM(2:2),'(i1)') NDim(KPhase)
      if (RefsSourceCode.eq.Refsm80)then
        t80=fln(:ifln)//'.m80'
        if (ExistFile(t80)) then
          call OpenFile(80,t80,'formatted','unknown')
        else
          t80='The file '//t80(1:idel(t80))//' doesn''t exist.'
          call FeChybne(-1.,-1.,t80,
     2         'Run Refine to create the file.',SeriousError)
          goto 9999
        endif
        F000=0.
        do i=1,NAtCalc
          if(kswa(i).eq.KPhase) then
            j=isf(i)
            if(j.gt.NAtFormula(KPhase)) cycle
            pom=ai(i)*CellVol(1,KPhase)/CellVol(iswa(i),KPhase)
            if(NDimI(KPhase).gt.0) then
              pom=pom*a0(i)
              if(KFA(2,i).eq.1.and.KModA(2,i).ne.0)
     1          pom=pom*uy(2,KModA(2,i),i)
            endif
            if(Radiation(KDatBlock).eq.NeutronRadiation) then
              fpom=ffn(j,KPhase)
            else
              fpom=AtNum(j,KPhase)
            endif
            F000=F000+fpom*pom
          endif
        enddo
        F000=F000*float(NSymm(KPhase))*Centr(KPhase)
        write(MEMFileId,'(''electrons '',f12.4)') F000
        write(MEMFileId,'(''fbegin'')')
1000    read(80,formatm80,end=3000)(ihkl(i),i=1,NDim(KPhase)),Ph,Fo,dm,
     1                             Fc,A00,B00,(dm,i=1,6),sig,dm
        if(ph.ne.KPhase) go to 1000
        write(MEMFileId,formatBM)(0,i=1,NDim(KPhase)),Fo,0.0,0.0005*Fo
1500    read(80,formatm80,end=3000)(ihkl(i),i=1,NDim(KPhase)),ph,Fo,dm,
     1                              Fc,A00,B00,(dm,i=1,6),sig,dm
        if (ihkl(1).eq.999) goto 3000
        if(ph.ne.KPhase) go to 1500
        if (RefsTypeCode.eq.RefsObs)then
          if (abs(Fc).gt.0.00001)then
            A00=A00*Fo/Fc
            B00=B00*Fo/Fc
          else
            A00=0.0
            B00=0.0
          endif
        endif
        write(MEMFileId,formatBM) (ihkl(i),i=1,NDim(KPhase)),A00,B00,sig
2000    goto 1500
3000    continue
        write(MEMFileId,'(''endf'')')
        close(80)
      elseif(RefsSourceCode.eq.Refsm90)then
        if(isPowder) then
          t80=fln(:ifln)//'.rfl'
        else
          t80=fln(:ifln)//'.m90'
        endif
        if(ExistFile(t80)) then
          if(isPowder) then
            open(91,file=t80)
          else
            call OpenDatBlockM90(91,1,t80)
          endif
        else
          t80='The file '//t80(:idel(t80))//' doesn''t exist.'
          call FeChybne(-1.,-1.,t80,' ',SeriousError)
          goto 9999
        endif
        write(MEMFileId,'(''electrons '',f12.4)') 0.
        if(isPowder) then
          write(MEMFileId,'(''dataitemwidths 4 15 15'')')
          write(MEMFileId,'(''dataformat intensity fwhm'')')
        else
          write(MEMFileId,'(''dataitemwidths 4 14 14'')')
        endif
        write(MEMFileId,'(''fbegin'')')
        if(isPowder) then
          write(FormRfl(2:2),'(i1)') NDim(KPhase)
3500      read(91,FormRfl,end=5000)(ihkl(i),i=1,NDim(KPhase)),Iobs,pom
          write(MEMFileId,FormRfl)(ihkl(i),i=1,NDim(KPhase)),Iobs,pom
          go to 3500
        else
          write(MEMFileId,formatBM)(0,i=1,NDim(KPhase)),0.0,0.0,0.1
4000      read(91,FormA256,end=5000) t256
          if(t256.eq.' ') go to 4000
          k=0
          call kus(t256,k,Cislo)
          if(EqIgCase(Cislo,'data')) go to 5000
          read(t256,format91)(ihkl(i),i=1,NDim(KPhase)),Iobs,sigIobs,i,
     1                        i,itw
          if(ihkl(1).eq.999) goto 5000
          if(itw.gt.1.or.itw.le.0) go to 4000
          if(Iobs.gt.0.) then
            Fo=sqrt(Iobs)
          else
            Fo=0.
          endif
          if(Iobs.le..01*SigIobs) then
            Sig=sqrt(SigIobs)/(.2)
          else
            Sig=.5/Fo*SigIobs
          endif
          Sig=sqrt(Sig**2+(blkoef*Fo)**2)
          if (ihkl(1).lt.999)
     1      write(MEMFileId,formatBM)(ihkl(i),i=1,NDim(KPhase)),Fo,0.,
     2                               sig
          goto 4000
        endif
5000    write(MEMFileId,'(''endf'')')
        close(91)
      endif
9999  continue
      return
      end
