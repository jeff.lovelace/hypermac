      subroutine MEMMEM
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest
      integer EdwStateQuest
      character*256 EdwStringQuest
      save nCrwIncMEM,nCrwSasa,nCrwMEMSys,nEdwConOrder,
     1     nEdwConWeight,nEdwAim,nEdwLambda,nEdwRate,nCrwFlat,
     2     nCrwFromFile,nCrw2ch,nEdwPriorFile,nEdwPriorFmt,
     3     nCrwPriorSF,nEdwSinThMin,nEdwSinThMax,nEdwMaxSat,
     4     nEdwPDCSigma
      real xqd
c!    MEMMEMMake
      entry MEMMEMMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,140.,il,'Include MEM','L',CrwXd,
     1                    CrwYd,0,0)
      nCrwIncMEM=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Algorithm:','L','N')
      call FeQuestLblOn(LblLastMade)
      call FeQuestCrwMake(id,60.,il,140.,il,'Sakata-Sato','L',CrwgXd,
     1                    CrwgYd,1,3)
      nCrwSaSa=CrwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Aim','L',
     1                    30.,EdwYd,0)
      nEdwAim=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,60.,il,140.,il,'MEMSys','L',CrwgXd,CrwgYd,
     1                      1,3)
      nCrwMEMSys=CrwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Lambda','L',
     1                    50.,EdwYd,0)
      nEdwLambda=EdwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Rate','L',
     1                    50.,EdwYd,0)
      nEdwRate=EdwLastMade
      il=il+1
      call FeQuestEudMake(id,5.,il,140.,il,'%Constraint order','L',
     1                    50.,EdwYd,0)
      nEdwConOrder=EdwLastMade
      call FeQuestEudMake(id,xqd*.5+10.,il,xqd*.5+120.,il,
     1                    '%Static weighting','L',50.,EdwYd,0)
      nEdwConWeight=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Flat prior','L',CrwgXd,
     1                    CrwgYd,1,4)
      nCrwFlat=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Non-uniform prior','L',
     1                    CrwgXd,CrwgYd,1,4)
      nCrwFromFile=CrwLastMade
c!    only if non-flat prior
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Two channel','L',CrwXd,
     1                    CrwYd,0,0)
      nCrw2ch=CrwLastMade
      il=il-2
      call FeQuestEdwMake(id,170.,il,250.,il,'Prior file','L',70.,EdwYd,
     1                    0)
      nEdwPriorFile=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,'Prior format','L',70.,
     1                    EdwYd,0)
      nEdwPriorFmt=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,170.,il,250.,il,'PDC','L',CrwXd,CrwYd,1,0)
      nCrwPriorSF=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,
     1                    'Sin(th)/l from','L',50.,EdwYd,0)
      nEdwSinThMin=EdwLastMade
      call FeQuestEdwMake(id,310.,il,330.,il,'to','L',50.,EdwYd,0)
      nEdwSinThMax=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,'Sigma','L',50.,EdwYd,0)
      nEdwPDCSigma=EdwLastMade
      if(NDim(KPhase).gt.3) then
        il=il+1
        call FeQuestEudMake(id,170.,il,250.,il,'Max. sat. index','L',
     1                      50.,EdwYd,0)
        nEdwMaxSat=EdwLastMade
      endif
      goto 9999

c!    MEMMEMOpen
      entry MEMMEMOpen
      call FeQuestCrwOpen(nCrwIncMEM,IncludeMEM)
      call FeQuestCrwOpen(nCrwSaSa,MEMAlgCode.eq.SaSa)
      call FeQuestRealEdwOpen(nEdwAim,MEMAim,.false.,.false.)
      call FeQuestCrwOpen(nCrwMEMSys,MEMAlgCode.eq.MEMSys)
      if (MEMAlgCode.eq.SaSa)then
        call FeQuestEdwClose(nEdwRate)
        call FeQuestRealEdwOpen(nEdwLambda,MEMLambda,.false.,.false.)
      elseif (MEMAlgCode.eq.MEMSys) then
        call FeQuestEdwClose(nEdwLambda)
        call FeQuestRealEdwOpen(nEdwRate,MEMRate,.false.,.false.)
      endif
      call FeQuestIntEdwOpen(nEdwConOrder,MEMConOrder,.false.)
      call FeQuestEudOpen(nEdwConOrder,2,8,2,0.,0.,0.)
      call FeQuestIntEdwOpen(nEdwConWeight,MEMConWeight,.false.)
      call FeQuestEudOpen(nEdwConWeight,0,8,1,0.,0.,0.)
      call FeQuestCrwOpen(nCrwFlat,PriorCode.eq.Flat)
      call FeQuestCrwOpen(nCrwFromFile,PriorCode.eq.FromFile)
      if(PriorCode.eq.FromFile) then
        call FeQuestCrwOpen(nCrw2ch,Twochannel)
        call FeQuestStringEdwOpen(nEdwPriorFile,PriorFile)
        call FeQuestStringEdwOpen(nEdwPriorFmt,PriorFmt)
        call FeQuestCrwOpen(nCrwPriorSF,PriorSF)
        if (PriorSF) then
          call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1         .false.)
          call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1         .false.)
          call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1         .false.)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
            call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
          endif
        else
          call FeQuestEdwClose(nEdwSinThMin)
          call FeQuestEdwClose(nEdwSinThMax)
          call FeQuestEdwClose(nEdwPDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestEdwClose(nEdwMaxSat)
          endif
        endif
      else
        call FeQuestCrwClose(nCrw2ch)
        call FeQuestEdwClose(nEdwPriorFile)
        call FeQuestEdwClose(nEdwPriorFmt)
        call FeQuestCrwClose(nCrwPriorSF)
        call FeQuestEdwClose(nEdwSinThMin)
        call FeQuestEdwClose(nEdwSinThMax)
        call FeQuestEdwClose(nEdwPDCSigma)
        if(NDim(KPhase).gt.3)then
          call FeQuestEdwClose(nEdwMaxSat)
        endif
      endif
      goto 9999

c!    MEMMEMUpdate
      entry MEMMEMUpdate
      if(CrwLogicQuest(nCrwIncMEM)) then
        IncludeMEM=.true.
      else
        IncludeMEM=.false.
      endif
      call FeQuestIntFromEdw(nEdwConOrder,MEMConOrder)
      call FeQuestIntFromEdw(nEdwConWeight,MEMConWeight)
      do 1000 i=nCrwSaSa,nCrwSaSa+1
        if (CrwLogicQuest(i)) MEMAlgCode=i-nCrwSaSa+1
1000  continue
      call FeQuestRealFromEdw(nEdwAim,MEMAim)
      if(EdwStateQuest(nEdwLambda).eq.EdwOpened) then
        call FeQuestRealFromEdw(nEdwLambda,MEMLambda)
      endif
      if(EdwStateQuest(nEdwRate).eq.EdwOpened) then
        call FeQuestRealFromEdw(nEdwRate,MEMRate)
      endif
      do 1100 i=nCrwFlat,nCrwFlat+1
        if (CrwLogicQuest(i)) PriorCode=i-nCrwFlat+1
1100  continue
      if (PriorCode.eq.FromFile)then
        TwoChannel=CrwLogicQuest(nCrw2ch)
        PriorFile=EdwStringQuest(nEdwPriorFile)
        PriorFmt=EdwStringQuest(nEdwPriorFmt)
        PriorSF=CrwLogicQuest(nCrwPriorSF)
        if(PriorSF)then
          call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
          call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
          call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
          endif
        endif
      endif
      goto 9999
c!    MEMMEMCheck
      entry MEMMEMCheck
      if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwSaSa.or.
     1   CheckNumber.eq.nCrwMEMSys)) then
        if(CrwLogicQuest(nCrwSaSa)) then
          if(EdwStateQuest(nEdwRate).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwRate,MEMRate)
            call FeQuestEdwClose(nEdwRate)
          endif
          if(EdwStateQuest(nEdwLambda).ne.EdwOpened) then
            call FeQuestrealEdwOpen(nEdwLambda,MEMLambda,.false.,
     1           .false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwLambda
        else
          if(EdwStateQuest(nEdwLambda).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwLambda,MEMLambda)
            call FeQuestEdwClose(nEdwLambda)
          endif
          if(EdwStateQuest(nEdwRate).ne.EdwOpened) then
            call FeQuestrealEdwOpen(nEdwRate,MEMRate,.false.,.false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwRate
        endif
      elseif(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwFlat.or.
     1   CheckNumber.eq.nCrwFromFile)) then
        if(CrwLogicQuest(nCrwFlat)) then
          TwoChannel=CrwLogicQuest(nCrw2ch)
          call FeQuestCrwClose(nCrw2ch)
          PriorFile=EdwStringQuest(nEdwPriorFile)
          call FeQuestEdwClose(nEdwPriorFile)
          PriorFmt=EdwStringQuest(nEdwPriorFmt)
          call FeQuestEdwClose(nEdwPriorFmt)
          PriorSF=CrwLogicQuest(nCrwPriorSF)
          call FeQuestCrwClose(nCrwPriorSF)
          if(PriorSF)then
            call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
            call FeQuestEdwClose(nEdwSinThMin)
            call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
            call FeQuestEdwClose(nEdwSinThMax)
            call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
            call FeQuestEdwClose(nEdwPDCSigma)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
              call FeQuestEdwClose(nEdwMaxSat)
            endif
          endif
        elseif (CrwLogicQuest(nCrwFromFile)) then
          if(EdwStateQuest(nEdwPriorFile).eq.EdwClosed) then
          call FeQuestCrwOpen(nCrw2ch,Twochannel)
          call FeQuestStringEdwOpen(nEdwPriorFile,PriorFile)
          call FeQuestStringEdwOpen(nEdwPriorFmt,PriorFmt)
          call FeQuestCrwOpen(nCrwPriorSF,PriorSF)
          if(CrwLogicQuest(nCrwPriorSF))then
            call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1          .false.)
            call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1          .false.)
            call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1          .false.)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
              call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
            endif
          endif
          EventType=EventEdw
          EventNumber=nEdwPriorFile
          endif
        endif
      elseif(CheckType.eq.EventCrw.and.
     1       CheckNumber.eq.nCrwPriorSF) then
        if (CrwLogicQuest(nCrwPriorSF)) then
          if(EdwStateQuest(nEdwSinThMin).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1           .false.)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
              call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
            endif
          endif
          EventType=EventEdw
          EventNumber=nEdwSinThMin
        else
          call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
          call FeQuestEdwClose(nEdwSinThMin)
          call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
          call FeQuestEdwClose(nEdwSinThMax)
          call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
          call FeQuestEdwClose(nEdwPDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
            call FeQuestEdwClose(nEdwMaxSat)
          endif
        endif
      else
        call FeInfoOut(-1.,-1.,'Kontrolni bod Questu nebyl osetren','L')
      endif
      goto 9999
c!    MEMMEMComplete
      entry MEMMEMComplete(okcheck)
      okcheck=0
      if (.not.IncludeMEM) goto 9999
      if (MEMAim.le.0.) then
        call FeChybne(-1.,-1.,'Aim must be a positive number.',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwAim
        goto 9999
      endif
      if (MEMRate.le.0.) then
        call FeChybne(-1.,-1.,'Rate must be a positive number.',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwRate
        goto 9999
      endif
      if (PriorCode.eq.FromFile)then
        if (PriorFile.eq.' ')then
        call FeChybne(-1.,-1.,'The name of the file with prior',
     1                        'must not be empty!',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPriorFile
          goto 9999
        endif
        if (PriorFmt(:idel(PriorFmt)).ne.'jana'.and.
     1      PriorFmt(:idel(PriorFmt)).ne.'BMascii'.and.
     2      PriorFmt(:idel(PriorFmt)).ne.'BMbinary')then
        call FeChybne(-1.,-1.,'The format of the prior must be',
     1                'jana, BMascii or BMbinary!',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPriorFmt
          goto 9999
        endif

        if (PriorSF.and.PDCSigma.le.0.) then
          call FeChybne(-1.,-1.,'Aim must be a positive number.',
     1                  ' ',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPDCSigma
          goto 9999
        endif
      endif
9999  continue
      return
      end
