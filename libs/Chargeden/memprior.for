      subroutine MEMPrior
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest
      character*256 EdwStringQuest
      save nCrwIncPrior,nEdwOutPrior,nButtSelAt
c!    MEMPriorMake
      entry MEMPriorMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,75.,il,'Include Prior','L',
     1     CrwXd,CrwYd,0,0)
      nCrwIncPrior=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,75.,il,'%Output file','L',xqd-80.,
     1                    EdwYd,1)
      nEdwOutPrior=EdwLastMade
      il=il+1
      call FeQuestButtonMake(id,5.,il,70.,ButYd,'%Select atoms')
      nButtSelAt=ButtonLastMade
      goto 9999

c!    MEMPriorOpen
      entry MEMPriorOpen
      call FeQuestCrwOpen(nCrwIncPrior,IncludePrior)
      call FeQuestStringEdwOpen(nEdwOutPrior,
     1     MEMOutPrior(1:idel(MEMOutPrior)))
      call FeQuestButtonOpen(nButtSelAt,ButtonOff)
      goto 9999

c!    MEMPRiorUpdate
      entry MEMPriorUpdate
      if(CrwLogicQuest(nCrwIncPrior)) then
        IncludePrior=.true.
      else
        IncludePrior=.false.
      endif
      MEMOutPrior=EdwStringQuest(nEdwOutPrior)
      goto 9999
c!    MEMPriorCheck
      entry MEMPriorCheck
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelAt) then
        if(allocated(AtBer)) deallocate(AtBer)
        allocate(AtBer(NAtInd))
        call SetLogicalArrayTo(AtBer,NAtInd,.true.)
        call iom40(0,0,fln(:ifln)//'.m40')
        call SelAtoms('Select atoms from atomic part',Atom(1),
     1                AtBer(1),isf(1),NAtInd,ich)
      endif
      goto 9999
      entry MEMPriorComplete(okcheck)
      okcheck=0
      if (.not.IncludePrior) goto 9999
      if (idel(MEMOutPrior).eq.0) then
        call FeChybne(-1.,-1.,'The filename must not be empty!',
     1                        ' ',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwOutPrior
        okcheck=1
        goto 9999
      endif
9999  continue
      return
      end
