      function PwdTwoTh2D(TwoTh,Lambda)
      include 'fepc.cmn'
      include 'basic.cmn'
      real Lambda
      PwdTwoTh2D=.5*Lambda/sin(.5*TwoTh*ToRad)
      return
      end
