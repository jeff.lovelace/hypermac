      function PwdTriangleFunction(x,xmax)
      include 'fepc.cmn'
      if(xmax.le.0.) then
        PwdTriangleFunction=1.
      else if(x.ge.-xmax.and.x.le.xmax) then
        PwdTriangleFunction=1.-abs(x)/xmax
      else
        PwdTriangleFunction=0.
      endif
      return
      end
