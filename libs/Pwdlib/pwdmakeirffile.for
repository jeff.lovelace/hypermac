      subroutine PwdMakeIRFFile
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*256 Veta
      logical ExistFile,FeYesNoHeader
      ln=0
      Veta=' '
1100  call FeFileManager('Choose out output "irf file',Veta,'*.irf',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      if(Veta.eq.' ') then
        call FeChybne(-1.,YBottomMessage,'The string is empty, try '//
     1                'again.',' ',SeriousError)
        go to 1100
      else if(ExistFile(Veta)) then
        call FeCutName(Veta,TextInfo(1),len(TextInfo(1))-35,
     1                 CutTextFromLeft)
        NInfo=1
        TextInfo(1)='The file "'//TextInfo(1)(:idel(TextInfo(1)))//
     1              '" already exists'
        if(.not.FeYesNoHeader(-1.,YBottomMessage,
     1                        'Do you want to rewrite it?',1))
     2    go to 1100
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','unknown')
      if(KUseTOFJason(KDatBlock).eq.1) then
        Veta='! To be used with function NPROF=13 in FullProf  (Res=5)'
      else
        Veta='! To be used with function NPROF=9 in FullProf  (Res=5)'
      endif
      write(ln,FormA) Veta(:idel(Veta))
      do i=1,NDatBlock
        KDatBlock=i
        call PwdM92Nacti
        write(Veta,'(''! '',52(''-''),'' Bank'')')
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        Veta(idel(Veta)+2:)=Cislo
        write(ln,FormA) Veta(:idel(Veta))
        Veta='!       Tof-min(us)    step      Tof-max(us)'
        write(ln,FormA) Veta(:idel(Veta))
        tmin=XPwd(1)
        tmax=XPwd(Npnts)
        tstep=(XPwd(2)-XPwd(1))*1000.
        tmin=tmin*1000.
        tmax=tmax*1000.
        write(Veta,'(3f12.4)') tmin,tstep,tmax
        write(ln,FormA) 'TOFRG'//Veta(:idel(Veta))
        Veta='!          Zero    Dtt1'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,100)(ShiftPwd(j,i),j=4,5)
        write(ln,FormA) 'ZD2TOF'//Veta(:idel(Veta))
        Veta='!          Zerot   Dtt1t         Dtt2t    x-cross '//
     1       'Width'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,100)(ShiftPwd(j,i),j=1,3),
     1                  ShiftPwd(7,i),ShiftPwd(6,i)
        write(ln,FormA) 'ZD2TOT'//Veta(:idel(Veta))
        Veta='!     TOF-TWOTH of the bank'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(f10.2)') TOFTTh(i)
        write(ln,FormA) 'TWOTH'//Veta(:idel(Veta))
        Veta='!           Sig-2       Sig-1       Sig-0'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(3f12.3)')(GaussPwd(4-j,1,i),j=1,3)
        write(ln,FormA) 'SIGMA'//Veta(:idel(Veta))
        Veta='!           Gam-2       Gam-1       Gam-0'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(3f12.3)')(LorentzPwd(4-j,1,i),j=1,3)
        write(ln,FormA) 'GAMMA'//Veta(:idel(Veta))
        Veta='!         alph0       beta0       alph1       beta1'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(4f12.6)') AsymPwd(1,i),AsymPwd(3,i),AsymPwd(2,i),
     1                         AsymPwd(4,i)
        write(ln,FormA) 'ALFBE'//Veta(:idel(Veta))
        Veta='!         alph0t      beta0t      alph1t      beta1t'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(4f12.6)') AsymPwd(5,i),AsymPwd(7,i),AsymPwd(6,i),
     1                         AsymPwd(8,i)
        write(ln,FormA) 'ALFBT'//Veta(:idel(Veta))
        write(ln,'(''END'')')
      enddo
9999  call CloseIfOpened(ln)
      return
100   format(f10.2,f13.4,f11.4,2f8.4)
      end
