      logical function PwdCheckTOFInD(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      PwdCheckTOFInD=abs(ShiftPwd(1,KDatBlock)).lt..0001.and.
     1               abs(ShiftPwd(3,KDatBlock)).lt..0001.and.
     2               abs(ShiftPwd(2,KDatBlock)-1.).lt..0001
      return
      end
