      subroutine PwdSetManualBackg(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 Veta,t80
      integer, allocatable :: FP(:)
      logical :: Skip = (.true.),FeYesNo,CrwLogicQuest
      real :: SigLev = (1.0)
      real, allocatable :: XP(:),YP(:),YS(:)
      AskIfQuit=.false.
      NManBackgOld=NManBackg(KDatBlock)
      if(NManBackg(KDatBlock).gt.0) then
        Veta='Manual background already exist, do you want to rewrite'//
     1       ' it?'
        if(.not.FeYesNo(-1.,-1.,Veta,0)) then
          ich=0
          go to 9900
        endif
      else
        NManBackg(KDatBlock)=30
      endif
1100  id=NextQuestId()
      xqd=350.
      il=3
      Veta='Options for generating background profile'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      dpom=60.
      tpom=5.
      Veta='%Number of manual background points:'
      xpom=tpom+FeTxLengthUnder(Veta)+50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNumberPoints=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NManBackg(KDatBlock),.false.)
      il=il+1
      Veta='%Significance intensity level for peaks regions:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSigLevel=EdwLastMade
      spom=xpom+dpom+5.
      call FeQuestLblMake(id,spom,il,'*sig(I)','L','N')
      call FeQuestRealEdwOpen(EdwLastMade,SigLev,.false.,.false.)
      il=il+1
      Veta='%Avoid from considerations skipped regions:'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwSkip=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,Skip)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNumberPoints,NManBackg(KDatBlock))
        call FeQuestRealFromEdw(nEdwSigLevel,SigLev)
        Skip=CrwLogicQuest(nCrwSkip)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9900
      n=0
      do i=1,NPnts
        if(YfPwd(i).ne.0) n=n+1
      enddo
      if(n.lt.5*NManBackg(KDatBlock)) then
        write(Cislo,FormI15) n
        call Zhusti(Cislo)
        write(Veta,FormI15) NManBackg(KDatBlock)
        call Zhusti(Veta)
        Veta='the number of '//Cislo(:idel(Cislo))//' profile points '//
     1       ' too small to determine'
        write(Cislo,FormI15) NManBackg(KDatBlock)
        call Zhusti(Cislo)
        t80=Cislo(:idel(Cislo))//
     1      ' points  manual background, try again.'
        call FeChybne(-1.,-1.,Veta,t80,SeriousError)
        go to 1100
      endif
      if(.not.allocated(XManBackg)) then
        m=NManBackg(KDatBlock)
        allocate(XManBackg(m,NDatBlock),YManBackg(m,NDatBlock))
      else
        call ReallocManBackg(NManBackg(KDatBlock))
      endif
      fm=float(n)/float(NManBackg(KDatBlock)-1)
      do i=1,NPnts
        if(YfPwd(i).eq.1.or..not.Skip) then
          XManBackg(1,KDatBlock)=XPwd(i)
          exit
        endif
      enddo
      k=0
      j=1
      do i=1,NPnts
        if(YfPwd(i).eq.0.and.Skip) cycle
        k=k+1
        if(k.ge.nint(fm*float(j))) then
          j=j+1
          if(j.gt.NManBackg(KDatBlock)-1) cycle
          XManBackg(j,KDatBlock)=XPwd(i)
        endif
      enddo
      do i=NPnts,1,-1
        if(YfPwd(i).eq.1.or..not.Skip) then
          XManBackg(NManBackg(KDatBlock),KDatBlock)=XPwd(i)
          exit
        endif
      enddo
      m=2*nint(fm)+5
      if(allocated(XP)) deallocate(XP,YP,YS,FP)
      allocate(XP(m),YP(m),YS(m),FP(m))
      do i=1,NManBackg(KDatBlock)
        XPMin=XManBackg(max(i-1,1),KDatBlock)
        XPMax=XManBackg(min(i+1,NManBackg(KDatBlock)),KDatBlock)
        m=0
        do j=1,NPnts
          if(YfPwd(j).eq.0.and.Skip) cycle
          if(XPwd(j).gt.XPMax) exit
          if(XPwd(j).ge.XPMin) then
            m=m+1
            XP(m)=XPwd(j)-XManBackg(i,KDatBlock)
            YP(m)=YoPwd(j)
            YS(m)=YsPwd(j)
            if(YS(m).le.0.) YS(m)=sqrt(abs(YP(m)))
            FP(m)=1
          endif
        enddo
        Kolikrat=0
2000    Kolikrat=Kolikrat+1
        n=0
        SumX=0.
        SumY=0.
        SumXY=0.
        SumX2=0.
        do j=1,m
          if(FP(j).eq.0) cycle
          n=n+1
          SumX=SumX+XP(j)
          SumY=SumY+YP(j)
          SumXY=SumXY+XP(j)*YP(j)
          SumX2=SumX2+XP(j)**2
        enddo
        det=0.
        fn=n
        ave=SumX/fn
        nn=0
        do j=1,m
          if(FP(j).eq.0) cycle
          nn=nn+1
          det=det+(XP(j)-ave)**2
        enddo
        det=det*fn
        det=fn*SumX2-SumX**2
        AA=(fn*SumXY-SumX*SumY)/det
        BB=(SumX2*SumY-SumX*SumXY)/det
        SumY=0.
        NOut=0
        NAll=0
        do j=1,m
          if(FP(j).eq.0) cycle
          NAll=NAll+1
          Y=AA*XP(j)+BB
          if(YP(j)-Y.gt.SigLev*YS(j)) then
            FP(j)=0
            NOut=NOut+1
          endif
        enddo
        if(NOut.gt.0.and.NAll-NOut.gt.3) go to 2000
        YManBackg(i,KDatBlock)=BB
      enddo
      Sc(2,KDatBlock)=1.
      go to 9999
9900  NManBackg(KDatBlock)=NManBackgOld
9999  AskIfQuit=.true.
      if(allocated(XP)) deallocate(XP,YP,YS,FP)
      return
      end
