      subroutine PwdPrfBraggInfo(KPhClick)
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension px(2),py(2),hh(3)
      character*6 ch6
      character*8 ch8
      integer Color,UseTabsIn,CrlSubSystemNumber
      logical CrlItIsSatellite
      if(.not.KresliBragg.or.NBragg.le.0) go to 9999
      th=PwdPrfX2Th(xpos)
      plim=abs(PwdPrfX2Th(6.)-PwdPrfX2Th(0.))
      NInfo=1
      ipocet=0
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      do 1200i=1,NBragg
        if(abs(BraggArr(i)-th).gt.plim) then
          if(BraggArr(i).gt.th) go to 1300
          go to 1200
        endif
        KPhase=KlicArr(i)/100+1
        if(KPhase.ne.KPhClick) go to 1200
        k=mod(KlicArr(i),100)
        ipocet=ipocet+1
        if(ipocet.eq.1) then
          pom=15.
          TextInfo(1)=' '
          do j=1,NDim(KPhase)
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                  Tabulator//indices(j)
            call FeTabsAdd(pom,UseTabs,IdLeftTab,' ')
            pom=pom+15.
          enddo
          pom=pom+25.
          do j=1,3
            if(j.eq.1) then
              if(DrawInD) then
                Cislo='d'
              else
                Cislo='2th'
              endif
            else if(j.eq.2) then
              Cislo='F**2'
            else if(j.eq.3) then
              Cislo='FWHM'
            endif
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  Cislo(:idel(Cislo))
            call FeTabsAdd(pom,UseTabs,IdCenterTab,' ')
            pom=pom+60.
          enddo
          pom=pom-30.
          if(kAlpha2.gt.0) then
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  'Doublet'
            call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
            pom=pom+50.
          endif
          if(NPhase.gt.1) then
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  'Phase'
            call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
          endif
        endif
        Zdvih=PruhBragg*float(KPhase-1)
        if(k.eq.1) then
          py(1)=y2bragg+Zdvih-VyskaBragg*.5
        else
          py(1)=y1bragg+Zdvih
        endif
        py(2)=y2bragg+Zdvih
        px(1)=PwdPrfTh2X(BraggArr(i))
        px(2)=px(1)
        if(px(1).lt.XminPlC.or.px(1).gt.XmaxPlC) go to 9999
        call FePolyLine(2,px,py,Cyan)
        if(NInfo.eq.19) go to 1200
        NInfo=NInfo+1
        pos=BraggArr(i)+ShiftArr(i)
        posp=.5*torad*pos
        if(kType.ne.0) then
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          NRefPwd=1
          NLamPwd=1
          if(.not.allocated(ihPwdArr)) then
            n=1
            m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
            l=NAlfa(KDatBlock)
            allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1               iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2               pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3               tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4               sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5               dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6               coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7               cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8               Beta12a(l,n),IBroadHKLa(n))
            if(.not.isTOF.and..not.isED.and.
     1         KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2        allocate(AxDivProfA(-25:25,l,n),
     3                 DerAxDivProfA(-25:25,7,l,n),FDSProf(-25:25,l,n))
          endif
          call SetProfFun(indArr(1,i),hh,posp)
        else
          fwhm=0.
        endif
        KPhase=KlicArr(i)/100+1
        k=mod(KlicArr(i),100)
        if(k.eq.2) then
          ch6=' '
        else if(k.eq.0) then
          ch6='alpha1'
        else
          ch6='alpha2'
        endif
        if(NPhase.gt.1) then
          ch8=PhaseName(KPhase)
        else
          ch8=' '
        endif
        TextInfo(NInfo)=' '
        do j=1,NDim(KPhase)
          write(Cislo,FormI15) indArr(j,i)
          call Zhusti(Cislo)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Tabulator//Cislo(:idel(Cislo))
        enddo
        do j=1,3
          if(j.eq.1) then
            pom=pos
          else if(j.eq.2) then
            pom=ypeakArr(i)
          else if(j.eq.3) then
            pom=fwhm/torad
          endif
          write(Cislo,'(f15.4)') pom
          call Zhusti(Cislo)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Tabulator//Cislo(:idel(Cislo))
        enddo
        if(ch6.ne.' ')
     1    TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                    Tabulator//ch6(:idel(ch6))
        if(ch8.ne.' ')
     1    TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                    Tabulator//ch8(:idel(ch8))
1200  continue
1300  if(NInfo.eq.19.and.ipocet.gt.18) then
        write(TextInfo(NInfo),'(''... and another '',i5,
     1    '' reflections'')') ipocet-17
        call FeDelTwoSpaces(TextInfo(NInfo))
      endif
      call FeKillInfoWin
      if(NInfo.gt.1) then
        pom=y2bragg+2.+PruhBragg*float(KPhClick-1)
        call FeInfoWin(-1.,pom)
      else
        go to 9999
      endif
      do 2000i=1,NBragg
        if(abs(BraggArr(i)-th).gt.plim) then
          if(BraggArr(i).gt.th) go to 9999
          go to 2000
        endif
        KPhase=KlicArr(i)/100+1
        k=mod(KlicArr(i),100)
        Zdvih=PruhBragg*float(KPhase-1)
        if(k.eq.1) then
          py(1)=y2bragg+Zdvih-VyskaBragg*.5
        else
          py(1)=y1bragg+Zdvih
        endif
        py(2)=y2bragg+Zdvih
        px(1)=PwdPrfTh2X(BraggArr(i))
        px(2)=px(1)
        if(CrlItIsSatellite(indArr(1,i))) then
          Color=ColorSat
        else
          j=CrlSubSystemNumber(indArr(1,i))
          if(j.le.0) then
            Color=Red
          else if(j.eq.1) then
            Color=White
          else
            Color=Yellow
          endif
        endif
        call FePolyLine(2,px,py,Color)
2000  continue
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(i1)
      end
