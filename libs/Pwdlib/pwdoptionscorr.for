      subroutine PwdOptionsCorr
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      dimension BackgPwdDef(:)
      character*80 Veta
      integer CrwStateQuest
      logical CrwLogicQuest
      external NToString
      allocatable BackgPwdDef
      save nEdwTerms,nEdwShift,
     1     nCrwShift,nCrwTypeBackg,nCrwUseInvX,nCrwManBackg,nCrwJason,
     2     nButtEditBackg,nButtManBackgImport,nButtSkip,NShift
      save /PwdOptionsC/
      entry PwdOptionsCorrMake(id)
      il=1
      call FeQuestLblMake(id,5.,il,'Background','L','B')
      xpom=5.
      tpom=xpom+CrwgXd+10.
      pom=0.
      do i=1,4
        il=il+1
        if(i.eq.1) then
          Veta='%Legendre polynomials'
        else if(i.eq.2) then
          Veta='%Chebyshev polynomials'
        else if(i.eq.3) then
          Veta='C%os-ortho background'
        else if(i.eq.4) then
          Veta='Co%s-GSAS background'
        endif
        pom=max(pom,FeTxLengthUnder(Veta))
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwTypeBackg=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KBackg(KDatBlock).eq.i)
      enddo
      tpom=tpom+pom+40.
      Veta='N%umber of terms'
      xpom=tpom+FeTxLengthUnder(Veta)+CrwXd+10.
      dpom=40.
      il=2
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwTerms=EdwLastMade
      call FeQuestIntEdwOpen(nEdwTerms,NBackg(KDatBlock),.false.)
      call FeQuestEudOpen(nEdwTerms,0,36,1,0.,0.,0.)
      il=il+1
      Veta='Use 1/%x term'
      xpom=tpom+CrwXd+5.
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseInvX=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KUseInvX(KDatBlock).eq.1)
      il=il+1
      Veta='%Edit background'
      xpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButtEditBackg=ButtonLastMade
      if(NBackg(KDatBlock).gt.0)
     1  call FeQuestButtonOpen(nButtEditBackg,ButtonOff)
      if(ExistM90) then
        il=il+2
        Veta='%Import manual background'
        xpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,5.,il,xpom,ButYd,Veta)
        nButtManBackgImport=ButtonLastMade
        call FeQuestButtonOpen(nButtManBackgImport,ButtonOff)
        call PwdM92Nacti
        Veta='Use %manual background'
        tpom=xcq+5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwManBackg=CrwLastMade
        if(NManBackg(KDatBlock).gt.0)
     1    call FeQuestCrwOpen(nCrwManBackg,KManBackg(KDatBlock).ne.0)
      else
        il=il+1
        nButtManBackgImport=0
        nCrwManBackg=0
      endif
      il=il+1
      Veta='%Define excluded regions'
      xpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,5.,il,xpom,ButYd,Veta)
      nButtSkip=ButtonLastMade
      call FeQuestButtonOpen(nButtSkip,ButtonOff)
      nCrwJason=0
      if(.not.TOFInD) then
        il=il+1
        if(isTOF) then
          Veta='TOF-instrument parameters'
        else
          Veta='Shift parameters'
        endif
        call FeQuestLblMake(id,5.,il,Veta,'L','B')
        if(isTOF) then
          call FeQuestLblMake(id,5.,il,Veta,'L','B')
          if(KUseTOFJason(KDatBlock).le.0) then
            NShift=3
          else
            NShift=7
          endif
          xpom=5.
          il=il+1
          tpom=xpom+CrwXd+10.
          Veta='Use formula developed by Jason %Hodges'
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwJason=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,KUseTOFJason(KDatBlock).eq.1)
        else
          if(isED) then
            NShift=1
          else
            NShift=3
          endif
        endif
        il=il+1
        j=IShiftPwd+IZdvihRec
        ip=0
        do i=1,7
          ip=ip+1
          Veta=' '
          if(DataType(KdatBlock).eq.2) then
            if(i.le.NShift) Veta=lShiftPwd(i)
          else
            if(KUseTOFJason(KDatBlock).le.0) then
              if(i.le.NShift) Veta=lTOF1Pwd(i)
            else
              if(i.le.NShift) Veta=lTOF2Pwd(i)
            endif
          endif
          if(i.eq.4.or.i.eq.6) then
            ip=1
            il=il+1
          endif
          call FeMakeParEdwCrw(id,tpoma(ip),xpoma(ip),il,Veta,nEdw,nCrw)
          if(i.le.NShift)
     1      call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     2                           ShiftPwd(i,KDatBlock),kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwShift=nEdw
            nCrwShift=nCrw
          endif
          j=j+1
        enddo
      else
        nEdwShift=0
        nCrwShift=0
      endif
      go to 9999
      entry PwdOptionsCorrCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseInvX) then
        if(CrwLogicQuest(nCrwUseInvX)) then
          k=IBackgPwd+(KDatBlock-1)*NParRecPwd+NBackg(KDatBlock)-1
          kiPwd(KDatBlock)=0
          do i=NBackg(KDatBlock),1,-1
            BackgPwd(i+1,KDatBlock)=BackgPwd(i,KDatBlock)
            KiPwd(k+1)=kiPwd(k)
            k=k-1
          enddo
          kiPwd(k+1)=0
          NBackg(KDatBlock)=NBackg(KDatBlock)+1
          BackgPwd(1,KDatBlock)=0.
          KUseInvX(KDatBlock)=1
        else
          k=IBackgPwd+(KDatBlock-1)*NParRecPwd+1
          do i=2,NBackg(KDatBlock)
            BackgPwd(i-1,KDatBlock)=BackgPwd(i,KDatBlock)
            KiPwd(k-1)=kiPwd(k)
            k=k+1
          enddo
          kiPwd(k-1)=0
          NBackg(KDatBlock)=NBackg(KDatBlock)-1
          KUseInvX(KDatBlock)=0
        endif
        call FeQuestIntEdwOpen(nEdwTerms,NBackg(KDatBlock),.false.)
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwJason) then
        NShiftOld=NShift
        if(CrwLogicQuest(nCrwJason)) then
          NShift=7
          KUseTOFJason(KDatBlock)=1
        else
          NShift=3
          KUseTOFJason(KDatBlock)=0
        endif
        call FeUpdateParamAndKeys(nEdwShift,nCrwShift,
     1    ShiftPwd(1,KDatBlock),kiPwd(IShiftPwd+IZdvihRec),
     2    NShiftOld)
        nEdw=nEdwShift
        nCrw=nCrwShift
        do i=1,max(NShift,NShiftOld)
          if(i.le.NShift) then
            if(KUseTOFJason(KDatBlock).le.0) then
              Veta=lTOF1Pwd(i)
            else
              Veta=lTOF2Pwd(i)
            endif
            call FeOpenParEdwCrw(nEdw,nCrw,Veta,
     1                           ShiftPwd(i,KDatBlock),kiPwd(j),
     2                           .false.)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtEditBackg) then
        allocate(BackgPwdDef(NBackg(KDatBlock)))
        call SetRealArrayTo(BackgPwdDef,NBackg(KDatBlock),0.)
        call FeEditParameterField(BackgPwd(1,KDatBlock),
     1      BackgPwdS(1,KDatBlock),BackgPwdDef,
     2      kiPwd(IBackgPwd+(KDatBlock-1)*NParRecPwd),
     3      NBackg(KDatBlock),lBackgPwd,xqdp,'Edit background',
     4      tpoma,xpoma,NToString)
        deallocate(BackgPwdDef)
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtManBackgImport) then
        call PwdImportManBackg
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSkip)
     1  then
        call PwdSkipFrTo
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTerms) then
        call FeQuestIntFromEdw(nEdwTerms,NBackgNew)
        i1=min(NBackg(KDatBlock),NBackgNew)+1
        i2=max(NBackg(KDatBlock),NBackgNew)
        j=IBackgPwd+i1-1
        do i=i1,i2
          kiPwd(j)=0
          BackgPwd(i,KDatBlock)=0.
          j=j+1
        enddo
        NBackg(KDatBlock)=NBackgNew
        if(NBackg(KDatBlock).gt.0) then
          call FeQuestButtonOpen(nButtEditBackg,ButtonOff)
        else
          call FeQuestButtonClose(nButtEditBackg)
        endif
        go to 9999
      endif
2500  if(NManBackg(KDatBlock).gt.0) then
        if(CrwStateQuest(nCrwManBackg).eq.CrwClosed) then
          KManBackg(KDatBlock)=1
          call FeQuestCrwOpen(nCrwManBackg,.true.)
        endif
      else
        call FeQuestCrwClose(nCrwManBackg)
      endif
      go to 9999
      entry PwdOptionsCorrUpdate
      nCrw=nCrwTypeBackg
      do i=1,4
        if(CrwLogicQuest(nCrw)) then
          KBackg(KDatBlock)=i
          exit
        endif
        nCrw=nCrw+1
      enddo
      call FeUpdateParamAndKeys(nEdwShift,nCrwShift,
     1  ShiftPwd(1,KDatBlock),kiPwd(IShiftPwd+IZdvihRec),NShift)
      if(CrwLogicQuest(nCrwManBackg)) then
        KManBackg(KDatBlock)=1
      else
        KManBackg(KDatBlock)=0
      endif
9999  return
      end
