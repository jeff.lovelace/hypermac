      subroutine PwdPrf(KtF)
      use Powder_mod
      use Refine_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      logical Mpom,odEdw,drzi,skoc,StartAgain,menilFile,odSkipToBragg,
     1        FileDiff,Prekresli,FeYesNo,lpom,PwdCheckTOFInD,EqRV,
     2        CrwLogicQuest
      integer Color,TimeQuestEvent,tnew,tline,FeGetSystemTime,CoDal,
     1        FeMenu,TimeDrzi,BraggCenterOld,RolMenuSelectedQuest
      dimension xtr(3),ytr(3),ihh(6),xsel(5),ysel(5),hh(3),
     1          XManBackgOld(:),YManBackgOld(:)
      character*20 :: LabPrf(19) =
     1  (/'%Quit       ','%Print      ','S%ave       ','X-          ',
     2    'X+          ','X %exactly  ','Y-          ','Y+          ',
     3    'P-          ','P+          ','%Info       ','%Fit Y      ',
     4    'Sh%rink     ','Defa%ult    ','P%nts OFF   ','%Zero ON    ',
     5    'Details ON  ','Draw in d''%s','%Options    '/)
      character*30 Men(4)
      character*32 TitlePrf,TitleM92
      character*80 ch80,Veta
      character*256 EdwStringQuest
      external PwdPrfNic
      allocatable XManBackgOld,YManBackgOld
      data Men/'%Escape','%Make zoom','%Add to exluded regions',
     1         '%Remove from excluded regions'/
      data TitlePrf/'Powder profile based on prf file'/,
     1     TitleM92/'Powder profile based on m92 file'/
      DrawInD=.false.
      NLabIn=3
      BraggCenter=0.
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      StartAgain=.false.
1100  lpom=DrawInD
      DrawInD=.false.
      if(ExistM90) then
        call PwdM92Nacti
      else
        PrfPointsFormat='(f10.3,3e15.6,f10.3,i5,99e15.6)'
      endif
      KDatBlockIn=KDatBlock
      KDatBlockNew=0
      ktFile=ktf
      call PwdPrfNacti(ktFile)
      if(ErrFlag.ne.0) go to 9999
      allocate(CalcPArr(npnts),CalcPPArr(npnts),BkgArr(npnts),
     1         DifArr(npnts))
      call PwdSetTOF(KDatBlock)
      n=100
      allocate(PeakXPos(n),PeakInt(n),PeakFWHM(n),PeakBckg(3,n),
     1         PeakEta(n),PeakXPosI(n),PeakPor(n))
      DrawInD=lpom
      if(DrawInD) then
        call PwdX2D(XPwd,NPnts)
        call PwdX2D(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
        if(.not.isTOF) then
          j=NPnts
          do i=1,NPnts/2
            pom=XPwd(j)
            XPwd(j)=XPwd(i)
            XPwd(i)=pom
            pom=YoPwd(j)
            YoPwd(j)=YoPwd(i)
            YoPwd(i)=pom
            pom=YcPwd(j)
            YcPwd(j)=YcPwd(i)
            YcPwd(i)=pom
            k=YfPwd(j)
            YfPwd(j)=YfPwd(i)
            YfPwd(i)=k
            j=j-1
          enddo
        endif
        do i=1,NBragg
          if(isTOF) then
            BraggArr(i)=PwdTOF2D(BraggArr(i))
          else
            if(KRefLam(KDatBlock).eq.1) then
              BraggArr(i)=PwdTwoTh2D(BraggArr(i),LamPwd(1,KDatBlock))
            else
              BraggArr(i)=PwdTwoTh2D(BraggArr(i),LamAve(KDatBlock))
            endif
          endif
        enddo
        if(.not.isTOF) then
          j=NBragg
          do i=1,NBragg/2
            pom=BraggArr(j)
            BraggArr(j)=BraggArr(i)
            BraggArr(i)=pom
            pom=MultArr(j)
            MultArr(j)=MultArr(i)
            MultArr(i)=pom
            pom=ShiftArr(j)
            ShiftArr(j)=ShiftArr(i)
            ShiftArr(i)=pom
            pom=ypeakArr(j)
            ypeakArr(j)=ypeakArr(i)
            ypeakArr(i)=pom
            ipom=KlicArr(j)
            KlicArr(j)=KlicArr(i)
            KlicArr(i)=ipom
            do k=1,NDim(KPhase)
              ipom=indArr(k,j)
              indArr(k,j)=indArr(k,i)
              indArr(k,i)=ipom
            enddo
            j=j-1
          enddo
        endif
      endif
      KPhase=KPhaseBasic
      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      call CopyFile(fln(:ifln)//'.m50',PreviousM50)
      call CopyFile(fln(:ifln)//'.m90',fln(:ifln)//'.z90')
      NManBackgOld=NManBackg(KDatBlock)
      if(NManBackgOld.gt.0) then
        allocate(XManBackgOld(NManBackgOld),YManBackgOld(NManBackgOld))
        call CopyVek(XManBackg(1,KDatBlock),XManBackgOld,NManBackgOld)
        call CopyVek(YManBackg(1,KDatBlock),YManBackgOld,NManBackgOld)
      endif
      CalcDer=.false.
      NlamPwd=0
      DegMin=-1.
      DegMax=-1.
      IntMax=-1.
      menilFile=.false.
      smallKrok=4.
      HardCopy=0
      KresliZero=.false.
      KresliDetails=.false.
      KresliObsPoints=.true.
      KresliManBackg=.false.
      whichLine=3
      call PwdPrfOptions(ich,.true.,menilFile,ktFile,istt)
      call FeFlush
1500  id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      CheckMouse=.true.
      call FeMakeGrWin(5.,120.,60.,30.)
      call FeMakeAcWin(5.,0.,0.,10.)
      call FeBottomInfo('#prazdno#')
      if(NDatBlock.gt.1) then
        dpom=0.
        do i=1,NDatBlock
          if(DataType(i).eq.1) then
            MenuDatBlockUse(i)=0
          else
            MenuDatBlockUse(i)=1
          endif
          dpom=max(dpom,FeTxLength(MenuDatBlock(i)))
        enddo
        dpom=dpom+EdwYd+2.*EdwMarginSize
        tpom=300.
        ypom=10.
        Veta='S%witch datblock'
        xpom=tpom+FeTxLength(Veta)+2.
        call FeQuestAbsRolMenuMake(id,tpom,ypom+6.,xpom,ypom,Veta,'L',
     1                             dpom,EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,MenuDatBlock,
     1    MenuDatBlockUse,NDatBlock,KDatBlock)
      endif
      call UnitMat(F2O,3)
      call FeSetTransXo2X(1.,40.,22.,0.,.false.)
      YMaxPlW=YMaxAcWin-6.
      XMaxPlW=XMaxAcWin-3.
      YMaxPlC=YMaxPlW-2.
      XMaxPlC=XMaxPlW-2.
      xpom=12.
      ButtWidth1=XMaxBasWin-XMaxAcWin-2.*xpom
      xpom=XMaxAcWin+xpom
      xleft=xpom
      xright=xpom+ButtWidth1
      ButtWidth2=ButtWidth1/2.-6.
      ButtWidth=ButtWidth1
      il=2
      do i=1,19
        call FeQuestButtonMake(id,xpom,il,ButtWidth,ButYd,
     1                            LabPrf(i))
        k=ButtonOff
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          ButtWidth=ButtWidth2
        else if(i.eq.4) then
          nButtXMinus=ButtonLastMade
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.5) then
          nButtXPlus=ButtonLastMade
          ButtWidth=ButtWidth1
          xpom=xleft
        else if(i.eq.6) then
          nButtXExactly=ButtonLastMade
          ButtWidth=ButtWidth2
        else if(i.eq.7) then
          nButtYMinus=ButtonLastMade
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.8) then
          nButtYPlus=ButtonLastMade
          xpom=xleft
        else if(i.eq.9) then
          nButtPminus=ButtonLastMade
          if(NBragg.le.0) k=ButtonDisabled
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.10) then
          if(NBragg.le.0) k=ButtonDisabled
          nButtPPlus=ButtonLastMade
          ButtWidth=ButtWidth1
          xpom=xleft
        else if(i.eq.11) then
          nButtInfo=ButtonLastMade
        else if(i.eq.12) then
          nButtFitY=ButtonLastMade
        else if(i.eq.13) then
          nButtFit=ButtonLastMade
        else if(i.eq.14) then
          nButtDefault=ButtonLastMade
        else if(i.eq.15) then
          nButtPoints=ButtonLastMade
        else if(i.eq.16) then
          nButtZero=ButtonLastMade
        else if(i.eq.17) then
          nButtDetails=ButtonLastMade
        else if(i.eq.18) then
          nButtDrawInD=ButtonLastMade
          if(isTOF.and.TOFInD) then
            call FeQuestButtonClose(ButtonLastMade)
            il=il-1
            cycle
          else
            if(DrawInD) then
              if(isTOF) then
                Veta=LabPrf(18)(:idel(LabPrf(18))-4)//'t''s'
              else
                Veta=LabPrf(18)(:idel(LabPrf(18))-4)//'2th''s'
              endif
              call FeQuestButtonLabelChange(ButtonLastMade,Veta)
            endif
          endif
        else if(i.eq.19) then
          nButtOpt=ButtonLastMade
        endif
        il=il+1
        call FeQuestButtonOpen(ButtonLastMade,k)
      enddo
      if(DrawInD) then
        Veta='Go to d'
      else
        if(isTOF) then
          Veta='Go to t'
        else
          Veta='Go to 2th'
        endif
      endif
      if(NBragg.gt.0) Veta=Veta(:idel(Veta))//'/hkl'
      il=il+1
      tpom=(xleft+xright)*.5
      call FeQuestEdwMake(id,tpom,-10*il+8,xleft,il,Veta,'C',
     1                    xright-xleft,EdwYd,1)
      nEdwGoTo=EdwLastMade
      call FeQuestStringEdwOpen(nEdwGoTo,' ')
      il=il+2
      Veta='Manual background:'
      call FeQuestLblMake(id,tpom,il,Veta,'C','B')
      Veta='Create new'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,ButtWidth,ButYd,Veta)
        if(NManBackg(KDatBlock).le.0) then
          k=ButtonDisabled
        else
          k=ButtonOff
        endif
        if(i.eq.1) then
          nButtBackgNew=ButtonLastMade
          Veta='Back%groud ON'
          k=ButtonOff
        else if(i.eq.2) then
          nButtBackgOn=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,k)
      enddo
      DegMin=XPwd(1)
      DegMax=XPwd(Npnts)
      pom=(YMaxBasWin+YMaxGrWin)*.5
      call FeQuestAbsLblMake(id,XCenGrWin,pom,' ','C','N')
      call FeQuestLblOff(LblLastMade)
      nLblTitle=LblLastMade
      call PwdPrfKresli(-1,1)
      if(ErrFlag.ne.0) go to 9999
      delkaKroku=XLenAcWin/(nen-nst)
      npulo=0
      xpom=XMinGrWin+2.
      ypom=YMinGrWin
      call FeQuestAbsSbwMake(id,xpom,ypom,XLenGrWin-4.,
     1                       YLenGrWin,1,CutTextNone,SbwHorizontal)
      nSbw=SbwLastMade
      call FeQuestSbwPureOpen(SbwLastMade,nst,nen,Npnts,0,0,0)
      xcenpl=XCenAcWin
      ycenpl=YCenAcWin
      AskIfQuit=.true.
2000  call FeQuestEvent(id,ich)
      if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit).or.
     1   (CheckType.eq.EventGlobal.and.CheckNumber.eq.GlobalClose))
     2  then
        StartAgain=.false.
        go to 6000
      endif
      TimeQuestEvent=FeGetSystemTime()
      odEdw=.false.
      odSkipToBragg=.false.
      HardCopy=0
      ErrFlag=0
      drzi=.false.
      iButOff=0
      if(CheckType.eq.0) go to 6000
      if(CheckType.eq.EventMouse) then
        if(CheckNumber.eq.JeLeftDown) then
          if(YPos.gt.y1bragg.and.
     1       YPos.lt.y2bragg+PruhBragg*float(NPhase-1)) then
            k=ifix((YPos-y1Bragg)/PruhBragg)+1
            call PwdPrfBraggInfo(k)
          else
            call FeKillInfoWin
            if(KresliManBackg) then
              XPom=PwdPrfX2Th(XPos)
              call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1                    XPom,i)
              XPosMin=PwdPrfTh2X(XManBackg(i,KDatBlock))
              YPosMin=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                            Sc(2,KDatBlock))
              D1=sqrt((XPos-XPosMin)**2+(YPos-YPosMin)**2)
              if(i.lt.NManBackg(KDatBlock)) then
                XPosMax=PwdPrfTh2X(XManBackg(i+1,KDatBlock))
                YPosMax=PwdPrfInt2Y(YManBackg(i+1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                D2=sqrt((XPos-XPosMax)**2+(YPos-YPosMax)**2)
              else
                XPosMax=XPosMin
                YPosMax=YPosMin
                D2=999999.
              endif
              D=sqrt((XPosMax-XPosMin)**2+(YPosMax-YPosMin)**2)
              if(D1.gt.10..and.D2.gt.10.) then
                if(D1+D2.gt.D+5.) go to 2170
                call ReallocManBackg(NManBackg(KDatBlock)+1)
                do j=NManBackg(KDatBlock),i+1,-1
                  XManBackg(j+1,KDatBlock)=XManBackg(j,KDatBlock)
                  YManBackg(j+1,KDatBlock)=YManBackg(j,KDatBlock)
                enddo
                NManBackg(KDatBlock)=NManBackg(KDatBlock)+1
                XManBackg(i+1,KDatBlock)=PwdPrfX2Th(XPos)
                YManBackg(i+1,KDatBlock)=PwdPrfY2Int(YPos)/
     1                                   Sc(2,KDatBlock)
                i=i+1
                XPosMin=XPos
                YPosMin=YPos
              else
                if(D2.lt.D1) then
                  XPosMin=XPosMax
                  YPosMin=YPosMax
                  D1=D2
                  i=i+1
                endif
              endif
              if(XManBackg(i,KDatBlock).ge.DegMin.and.
     1           XManBackg(i,KDatBlock).le.DegMax) then
                if(i.le.1) then
                  xsel1=PwdPrfTh2X(XManBackg(i,KDatBlock))
                  ysel1=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                              Sc(2,KDatBlock))
                else
                  xsel1=PwdPrfTh2X(XManBackg(i-1,KDatBlock))
                  ysel1=PwdPrfInt2Y(YManBackg(i-1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                endif
                if(i.ge.NManBackg(KDatBlock)) then
                  xsel3=PwdPrfTh2X(XManBackg(i,KDatBlock))
                  ysel3=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                              Sc(2,KDatBlock))
                else
                  xsel3=PwdPrfTh2X(XManBackg(i+1,KDatBlock))
                  ysel3=PwdPrfInt2Y(YManBackg(i+1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                endif
                if(xsel1.lt.XMinPlC) then
                  call PwdPrfPrusX(xsel1,ysel1,
     1                             XPosMin,YPosMin,
     2                             xsel(1),ysel(1),XMinPlC)
                else
                  xsel(1)=xsel1
                  ysel(1)=ysel1
                endif
                if(xsel3.gt.XMaxPlC) then
                  call PwdPrfPrusX(xsel3,ysel3,
     1                             XPosMin,YPosMin,
     2                             xsel(3),ysel(3),XMaxPlC)
                else
                  xsel(3)=xsel3
                  ysel(3)=ysel3
                endif
                call FeReleaseOutput
                call FeMoveMouseTo(XPosMin,YPosMin)
                call FePlotMode('E')
                TakeMouseMove=.true.
                XPosOld=XPosMin
                YPosOld=YPosMin
                call FeCircle(XPosOld,YPosOld,4.,Green)
                EventType=0
                EventNumber=0
2100            call FeEvent(1)
                if(EventType.eq.0) go to 2100
                if(EventType.eq.EventMouse) then
                  if(EventNumber.eq.JeMove) then
                    if((XPos.le.xsel(1).or.XPos.ge.xsel(3)).and.
     1                 (i.ne.1.and.i.ne.NManBackg(KDatBlock))) then
                      do j=i+1,NManBackg(KDatBlock)
                        XManBackg(j-1,KDatBlock)=XManBackg(j,KDatBlock)
                        YManBackg(j-1,KDatBlock)=YManBackg(j,KDatBlock)
                      enddo
                      NManBackg(KDatBlock)=NManBackg(KDatBlock)-1
                      call FePlotMode('N')
                      Prekresli=.true.
                      EventType=0
                      EventNumber=0
                      go to 4800
                    endif
                    call FeCircle(XPosOld,YPosOld,4.,Green)
                    xsel(2)=XPosOld
                    ysel(2)=YPosOld
                    call FePolyline(3,xsel,ysel,Green)
                    call FeCircle(XPos,YPos,4.,Green)
                    xsel(2)=XPos
                    ysel(2)=YPos
                    if(xsel1.lt.XMinPlC) then
                      call PwdPrfPrusX(xsel1,ysel1,
     1                                 xsel(2),ysel(2),
     2                                 xsel(1),ysel(1),XMinPlC)
                    else
                      xsel(1)=xsel1
                      ysel(1)=ysel1
                    endif
                    if(xsel3.gt.XMaxPlC) then
                      call PwdPrfPrusX(xsel3,ysel3,
     1                                 xsel(2),ysel(2),
     2                                 xsel(3),ysel(3),XMaxPlC)
                    else
                      xsel(3)=xsel3
                      ysel(3)=ysel3
                    endif
                    call FePolyline(3,xsel,ysel,Green)
                    XPosOld=XPos
                    YPosOld=YPos
                    go to 2100
                  else if(EventNumber.eq.JeLeftUp) then
                    call FePlotMode('N')
                    XManBackg(i,KDatBlock)=PwdPrfX2Th(XPos)
                    YManBackg(i,KDatBlock)=PwdPrfY2Int(YPos)/
     1                                     Sc(2,KDatBlock)
                    Prekresli=.true.
                    EventType=0
                    EventNumber=0
                    go to 4800
                  endif
                endif
              endif
            endif
2170        if(XPos.ge.XMinAcWin.and.XPos.le.XMaxAcWin.and.
     1         YPos.ge.yminplC.and.YPos.le.ymaxplC) then
              xsel(1)=XPos
              ysel(1)=yminplC
              xsel(2)=XPos
              ysel(2)=YPos
              xsel(3)=XPos
              ysel(3)=YPos
              xsel(4)=XPos
              ysel(4)=yminplC
              xsel(5)=xsel(1)
              ysel(5)=ysel(1)
              call FePlotMode('E')
              ip=2
              call FePolyLine(2,xsel,ysel,Green)
              TakeMouseMove=.true.
              Prekresli=.false.
              if(DeferredOutput) call FeReleaseOutput
2200          call FeEvent(1)
              if(EventType.eq.0) go to 2200
2250          if(EventType.eq.EventMouse) then
                if(EventNumber.eq.JeMove) then
                  if(YPos.ge.yminAcWin.and.YPos.le.ymaxAcWin.and.
     1               XPos.ge.xminAcWin.and.XPos.le.xmaxAcWin) then
                    call FePolyLine(ip,xsel,ysel,Green)
                    ysel(2)=YPos
                    xsel(3)=XPos
                    ysel(3)=YPos
                    xsel(4)=XPos
                    if(xsel(2).ne.xsel(3)) then
                      ip=5
                    else
                      ip=2
                    endif
                    call FePolyLine(ip,xsel,ysel,Green)
                  endif
                  go to 2200
                else if(EventNumber.eq.JeLeftUp) then
                  TakeMouseMove=.false.
                  DegMinP=PwdPrfX2Th(min(xsel(1),xsel(3)))
                  DegMaxP=PwdPrfX2Th(max(xsel(1),xsel(3)))
                  call locate(XPwd(nst),nen-nst+1,DegMinP,i)
                  call locate(XPwd(nst),nen-nst+1,DegMaxP,j)
                  if(j.le.i) then
                    go to 2270
                  else
                    go to 2200
                  endif
                else if(EventNumber.eq.JeLeftDown) then
                  DegMinP=PwdPrfX2Th(min(xsel(1),xsel(3)))
                  DegMaxP=PwdPrfX2Th(max(xsel(1),xsel(3)))
                  call locate(XPwd(nst),nen-nst+1,DegMinP,i)
                  call locate(XPwd(nst),nen-nst+1,DegMaxP,j)
                  if(i+10.ge.j) then
                    go to 2270
                  else
                    DegMin=DegMinP
                    DegMax=DegMaxP
                  endif
                  IntMax=PwdPrfY2Int(ysel(3))
                  Prekresli=.true.
                  call FeDeferOutput
                else if(EventNumber.eq.JeRightDown) then
                  call FePlotMode('N')
                  CoDal=FeMenu(-1.,-1.,men,1,4,1,0)
                  call FePlotMode('E')
                  if(CoDal.eq.3.or.CoDal.eq.4) then
                    PomMin=PwdPrfX2Th(min(xsel(1),xsel(3)))
                    PomMax=PwdPrfX2Th(max(xsel(1),xsel(3)))
                    if(DrawInD) then
                      if(isTOF) then
                        PomMaxP=PwdD2TOF(PomMax)
                        PomMinP=PwdD2TOF(PomMin)
                        DegMinP=PwdD2TOF(DegMin)
                        DegMaxP=PwdD2TOF(DegMax)
                      else
                        if(KRefLam(KDatBlock).eq.1) then
                          PomMaxP=PwdD2TwoTh(PomMin,LamPwd(1,KDatBlock))
                          PomMinP=PwdD2TwoTh(PomMax,LamPwd(1,KDatBlock))
                          DegMaxP=PwdD2TwoTh(DegMin,LamPwd(1,KDatBlock))
                          DegMinP=PwdD2TwoTh(DegMax,LamPwd(1,KDatBlock))
                        else
                          PomMaxP=PwdD2TwoTh(PomMin,LamAve(KDatBlock))
                          PomMinP=PwdD2TwoTh(PomMax,LamAve(KDatBlock))
                          DegMaxP=PwdD2TwoTh(DegMin,LamAve(KDatBlock))
                          DegMinP=PwdD2TwoTh(DegMax,LamAve(KDatBlock))
                        endif
                      endif
                    else
                      DegMinP=DegMin
                      DegMaxP=DegMax
                      PomMinP=PomMin
                      PomMaxP=PomMax
                    endif
                  endif
                  if(CoDal.eq.2) then
                    EventType=EventMouse
                    EventNumber=JeLeftDown
                    go to 2250
                  else if(CoDal.eq.3) then
                    NSkipPwd(KDatBlock)=NSkipPwd(KDatBlock)+1
                    n=NSkipPwd(KDatBlock)
                    SkipPwdFr(n,KDatBlock)=max(PomMinP,DegMinP)
                    SkipPwdTo(n,KDatBlock)=min(PomMaxP,DegMaxP)
                    call PwdOrderSkipRegions
                    call iom40(1,0,fln(:ifln)//'.m40')
                    Prekresli=.true.
                  else if(CoDal.eq.4) then
                    call PwdRemoveSkipRegion(max(PomMinP,DegMinP),
     1                                       min(PomMaxP,DegMaxP))
                    call iom40(1,0,fln(:ifln)//'.m40')
                    Prekresli=.true.
                  endif
                  go to 2270
                else
                  go to 2200
                endif
              else if(EventType.eq.EventASCII) then
                if(EventNumber.ne.JeEscape) go to 2200
              endif
2270          call FePolyLine(ip,xsel,ysel,Green)
              call FePlotMode('N')
              if(Prekresli) then
                EventType=0
                EventNumber=0
                go to 4800
              endif
            endif
          endif
          EventType=0
          EventNumber=0
          go to 2000
        endif
      endif
2300  if(CheckType.eq.EventButton) then
        call FeKillInfoWin
        if(CheckNumber.eq.nButtXPlus) then
          delkaKroku=delkaKroku*expandX
          drzi=.true.
        else if(CheckNumber.eq.nButtXMinus) then
          delkaKroku=delkaKroku/shrinkX
          drzi=.true.
        else if(CheckNumber.eq.nButtYPlus) then
          IntMax=IntMax/expandY
          drzi=.true.
        else if(CheckNumber.eq.nButtYMinus) then
          IntMax=IntMax*shrinkY
          drzi=.true.
        else
          drzi=.false.
          call FeReleaseOutput
        endif
        if(drzi) go to 4700
        iButOff=CheckNumber
        if(CheckNumber.eq.nButtDrawInD) then
          StartAgain=.true.
          DrawInD=.not.DrawInD
          go to 6000
        else if(CheckNumber.eq.nButtSave.or.
     1          CheckNumber.eq.nButtPrint) then
          if(CheckNumber.eq.nButtSave) then
            ipom=6
            call FeSavePicture('plot',ipom,1)
            if(HardCopy.eq.0) go to 2000
          else
            call FePrintPicture(ich)
            if(ich.ne.0) go to 2000
          endif
          call FeHardCopy(HardCopy,'open')
          if(InvertWhiteBlack.or.
     1       (HardCopy.ne.HardCopyBMP.and.
     2        HardCopy.ne.HardCopyPCX)) then
            if(InvertWhiteBlack) then
              do i=1,8
                if(ColorPole(i).eq.White) then
                  ColorPole(i)=Black
                else if(ColorPole(i).eq.Black) then
                  ColorPole(i)=White
                endif
              enddo
            endif
            call FeFillRectangle(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                           YMaxGrWin,4,0,0,Black)
            call PwdPrfKresli(0,0)
            if(InvertWhiteBlack) then
              do i=1,8
                if(ColorPole(i).eq.White) then
                  ColorPole(i)=Black
                else if(ColorPole(i).eq.Black) then
                  ColorPole(i)=White
                endif
              enddo
            endif
          endif
          call FeHardCopy(HardCopy,'close')
          if(CheckNumber.eq.nButtPrint) then
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
          endif
          HardCopy=0
          go to 4800
        else if(CheckNumber.eq.nButtPPlus.or.CheckNumber.eq.nButtPMinus)
     1    then
          if(CheckNumber.eq.nButtPPlus) then
            izn= 1
          else
            izn=-1
          endif
          BraggCenterOld=BraggCenter
          if(BraggCenter.le.0) then
            pul=.5*(DegMax+DegMin)
            call locate(BraggArr,NBragg,pul,mst)
            if(izn.lt.0) then
              mst=min(mst+1,NBragg)
            else
              mst=max(mst-1,1)
            endif
          else
            mst=BraggCenter
          endif
          m=mst
2400      m=m+izn
          if(m.lt.1.or.m.gt.NBragg) go to 2000
          if(abs(BraggArr(m)-BraggArr(mst)).lt..001) go to 2400
          BraggCenter=m
          pul=.5*(DegMax-DegMin)
          if(BraggArr(m)-pul.le.XPwd(1)) then
            DegMin=XPwd(1)
            DegMax=min(XPwd(Npnts),DegMin+2.*pul)
          else if(BraggArr(m)+pul.ge.XPwd(Npnts)) then
            DegMax=XPwd(Npnts)
            DegMin=max(XPwd(1),DegMax-2.*pul)
          else
            DegMin=BraggArr(m)-pul
            DegMax=BraggArr(m)+pul
          endif
          th=BraggArr(m)
          odSkipToBragg=.true.
          go to 4800
        else if(CheckNumber.eq.nButtDetails) then
          if(kType.eq.0.or.NBragg.le.0.or.KtFile.ne.0) then
            call FeMsgOut(-1.,-1.,'The component function cannot be '//
     1        'calculated for this plot')
            go to 4900
          endif
          KresliDetails=.not.KresliDetails
          if(KresliDetails) then
            Veta=LabPrf(17)(:idel(LabPrf(17))-1)//'FF'
          else
            Veta=LabPrf(17)
          endif
          call FeQuestButtonLabelChange(nButtDetails,Veta)
          go to  4800
        else if(CheckNumber.eq.nButtInfo) then
          if(OpSystem.le.0) then
            call FePlotMode('E')
            Color=Yellow
          else
            Color=BlackXOR
          endif
          call FeReleaseOutput
          ZarazkaL=PwdPrfTh2X(XPwd(nst))
          ZarazkaR=PwdPrfTh2X(XPwd(nen))
          xtr(1)=xcenpl
          xtr(2)=xtr(1)
          ytr(1)=YminPlW
          ytr(2)=YmaxPlW
          call FePolyLine(2,xtr,ytr,Color)
          call FeMoveMouseTo(xcenpl,ycenpl)
          itxt=0
          Mpom=TakeMouseMove
          TakeMouseMove=.true.
          tline=-1
2500      call FeEvent(1)
          tnew=FeGetSystemTime()
          skoc=.false.
          if(itxt.eq.0.and.tline.ge.0.and.tnew-tline.ge.1000) then
            skoc=.true.
            go to 2540
          endif
          if(EventType.eq.0) go to 2500
2540      if(skoc.or.abs(XPos-xtr(1)).gt..5) then
            if(skoc) go to 2550
            if(XPos.gt.ZarazkaR) then
              xkam=ZarazkaR
            else if(XPos.lt.ZarazkaL) then
              xkam=ZarazkaL
            else
              xkam=XPos
            endif
            if(xtr(1).ne.XPos) then
              call FePolyLine(2,xtr,ytr,Color)
              xtr(1)=xkam
              xtr(2)=xkam
              tline=FeGetSystemTime()
              th=PwdPrfX2Th(xkam)
              if(itxt.eq.1) then
                call PwdPrfInfoObnova
                itxt=0
              endif
              call FePolyLine(2,xtr,ytr,Color)
              go to 2600
            endif
2550        call locate(XPwd(nst),nen-nst+1,th,j)
            skoc=.false.
            tline=tnew
            if(j.ne.0) then
              j=j+nst-1
              pom=XPwd(j+1)-XPwd(j)
              wright=(th-XPwd(j))/pom
              wleft=(XPwd(j+1)-th)/pom
              HDif=wleft*DifArr(j)+wright*DifArr(j+1)
              HObs=wleft*abs(YoPwd(j))+wright*abs(YoPwd(j+1))
              HCalc=wleft*YcPwd(j)+wright*YcPwd(j+1)
              HTh=wleft*XPwd(j)+wright*XPwd(j+1)
              if(KresliBragg) then
                call locate(BraggArr,NBragg,th,m)
                m=min(m,NBragg)
                m=max(m,1)
              else
                go to 2570
              endif
              HBragg=0.
              do i=1,6
                ihh(i)=0
              enddo
              if(m.ne.0) then
                plim=abs(PwdPrfX2Th(6.)-PwdPrfX2Th(0.))
                if(abs(th-BraggArr(m)).le.abs(th-BraggArr(m+1)))
     1            then
                  if(abs(BraggArr(m)-th).le.plim)
     1              HBragg=BraggArr(m)+ShiftArr(m)
                  j=m
                else
                  if(abs(BraggArr(m+1)-th).le.plim)
     1              HBragg=BraggArr(m+1)+ShiftArr(m+1)
                  j=m+1
                endif
              endif
2570          call PwdPrfInfo(HTh,HCalc,HObs,HDif,HBragg,xkam)
              itxt=1
            endif
          endif
2600      if(EventType.eq.EventKey.and.
     1       (EventNumber.eq.JeRight.or.EventNumber.eq.JeLeft)) then
            if(EventNumber.eq.JeRight) then
              XPos=XPos+1.1
            else
              XPos=XPos-1.1
            endif
            call FeMoveMouseTo(XPos,YPos)
            go to 2500
          endif
          if((EventType.ne.EventMouse.or.EventNumber.ne.JeLeftDown).and.
     1       (EventType.ne.EventKey.or.EventNumber.ne.JeEscape))
     2        go to 2500
          call FePolyLine(2,xtr,ytr,Color)
          if(itxt.eq.1) call PwdPrfInfoObnova
          if(OpSystem.le.0) call FePlotMode('N')
          TakeMouseMove=Mpom
          go to 4900
        else if(CheckNumber.eq.nButtFit) then
          DegMin=XPwd(1)
          DegMax=XPwd(Npnts)
          IntMax=0.
          go to 4800
        else if(CheckNumber.eq.nButtXExactly) then
          idp=NextQuestId()
          xqdp=170.
          il=2
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,' ',0,LightGray,0,0)
          il=1
          tpom=5.
          ch80='Nex X(m%in):'
          dpom=80.
          xpom=tpom+FeTxLengthUnder(ch80)+10.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,DegMin,.false.,.false.)
          nEdwXMin=EdwLastMade
          il=il+1
          ch80='Nex X(m%ax):'
          call FeQuestEdwMake(idp,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,DegMax,.false.,.false.)
          nEdwXMax=EdwLastMade
2610      call FeQuestEvent(idp,ichp)
          if(CheckType.eq.EventGlobal) then
            go to 2610
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2610
          endif
          if(ichp.eq.0) then
            call FeQuestRealFromEdw(nEdwXMin,DegMinNew)
            call FeQuestRealFromEdw(nEdwXMax,DegMaxNew)
            if(DegMaxNew.gt.DegMinNew) then
              DegMin=max(DegMinNew,XPwd(1    ))
              DegMax=min(DegMaxNew,XPwd(Npnts))
              IntMax=0.
            else
              ichp=1
            endif
          endif
          call FeQuestRemove(idp)
          if(ichp.eq.0) go to 4800
        else if(CheckNumber.eq.nButtDefault) then
          call locate(XPwd,Npnts,.5*(DegMax+DegMin),ipul)
          idp=nint(XLenAcWin/stepIni)
          nst=ipul-idp/2
          nen=nst+idp
          if(nen.gt.Npnts) then
            nst=Npnts-idp
            nen=Npnts
          else if(nst.lt.1) then
            nst=1
            nen=idp
          endif
          DegMin=XPwd(nst)
          DegMax=XPwd(nen)
          IntMax=0.
          go to 4800
        else if(CheckNumber.eq.nButtPoints)then
          KresliObsPoints=.not.KresliObsPoints
          if(KresliObsPoints)then
            Veta=LabPrf(15)
          else
            Veta=LabPrf(15)(:idel(LabPrf(15))-2)//'N'
          endif
          call FeQuestButtonLabelChange(nButtPoints,Veta)
          go to 4800
        else if(CheckNumber.eq.nButtZero) then
          KresliZero=.not.KresliZero
          if(KresliZero) then
            Veta=LabPrf(16)(:idel(LabPrf(16))-1)//'FF'
          else
            Veta=LabPrf(16)
          endif
          call FeQuestButtonLabelChange(nButtZero,Veta)
          go to 4800
        else if(CheckNumber.eq.nButtFitY) then
          CalcMax=-1.
          ObsMax=-1.
          do i=1,Npnts
            if(XPwd(i).lt.DegMin) cycle
            if(XPwd(i).gt.DegMax) exit
            CalcMax=max(CalcMax,YcPwd(i))
            if(KresliObsPoints.or.KresliObsLine)
     1        ObsMax=max(ObsMax,abs(YoPwd(i)))
          enddo
          IntMax=max(CalcMax,ObsMax)
          if(KresliObsPoints) then
            pom=4.*IntMax/(ymaxplW-yminplW)
            if(CalcMax-ObsMax.lt.pom)
     1        IntMax=IntMax+float(nint(pom))
          endif
          go to  4800
        else if(CheckNumber.eq.nButtOpt) then
          call PwdPrfOptions(ich,.false.,menilFile,ktFile,istt)
          if(menilFile) then
            call FeQuestRemove(id)
            go to 1500
          endif
          if(ich.eq.0) then
            CheckNumber=nButtOpt
            go to 4800
          else
            go to 2000
          endif
        else if(CheckNumber.eq.nButtBackgNew) then
          call PwdSetManualBackg(ichp)
          if(ichp.eq.0) then
            if(DrawInD) then
              call PwdD2X(XPwd,NPnts)
              call PwdD2X(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
            endif
            call PwdUpdateReflFiles
            Veta='Back%groud OFF'
            call FeQuestButtonLabelChange(nButtBackgOn,Veta)
            call FeQuestButtonOff(nButtBackgOn)
            KresliManBackg=.true.
            if(DrawInD) then
              call PwdX2D(XPwd,NPnts)
              call PwdX2D(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
            endif
            go to 4800
          endif
          go to 4900
        else if(CheckNumber.eq.nButtBackgOn) then
          KresliManBackg=.not.KresliManBackg
          if(KresliManBackg) then
            Veta='Back%groud OFF'
          else
            Veta='Back%groud ON'
          endif
          call FeQuestButtonLabelChange(nButtBackgOn,Veta)
!          if(DrawInD) call PwdX2D(XManBackg(1,KDatBlock),
!     1                            NManBackg(KDatBlock))
          go to 4800
        else
          go to 4900
        endif
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwGoTo) then
        ch80=EdwStringQuest(nEdwGoTo)
        call FeQuestStringEdwOpen(nEdwGoTo,' ')
        idp=idel(ch80)
        if(idp.le.0) go to 2000
        k=0
        call StToInt(ch80,k,ihh,NDim(KPhase),.false.,ich)
        if(ich.eq.0) then
          call FromIndSinthl(ihh,hh,sinthl,sinthlq,1,0)
          if(DrawInD) then
            th=.5/sinthl
          else
            if(isTOF) then
              th=PwdD2TOF(.5/sinthl)
            else
              if(KRefLam(KDatBlock).eq.1) then
                pom=min(1.,sinthl*LamPwd(1,KDatBlock))
              else
                pom=min(1.,sinthl*LamAvePwd(KDatBlock))
              endif
              th=2.*asin(pom)/torad
            endif
          endif
          zerosh=0.
          call locate(BraggArr,NBragg,th,m)
          if(m.gt.0) then
            zerosh=ShiftArr(m)
            if(m.lt.NBragg) then
              if(abs(th-BraggArr(m)).gt.abs(th-BraggArr(m+1)))
     1          zerosh=ShiftArr(m+1)
            endif
          endif
          th=th-zerosh
        else if(ich.eq.1.or.ich.eq.3) then
          k=0
          call StToReal(ch80,k,th,1,.false.,ich)
        endif
        if(ich.ne.0) then
          call FeChybne(-1.,-1.,
     1      'string doesn''t contain proper number of items',
     2      'or it contains illegal character.',SeriousError)
          go to 2000
        endif
        if(th.lt.XPwd(1).or.th.gt.XPwd(Npnts)) then
          write(ch80,'(''Angle '',f9.3,'' is outside the limits <'',
     1      f9.3,'','',f9.3,''>'')')th,XPwd(1),XPwd(Npnts)
          call FeDelTwoSpaces(ch80)
          call FeMsgOut(-1.,-1.,ch80)
          go to 2000
        endif
        call locate(XPwd(1),Npnts,th,ipul)
        if(ipul.eq.0) go to 2000
        odEdw=.true.
        pul=.5*(DegMax-DegMin)
        if(XPwd(ipul)-pul.le.XPwd(1)) then
          DegMin=XPwd(1)
          DegMax=min(XPwd(Npnts),DegMin+2.*pul)
        else if(XPwd(ipul)+pul.ge.XPwd(Npnts)) then
          DegMax=XPwd(Npnts)
          DegMin=max(XPwd(1),DegMax-2.*pul)
        else
          DegMin=XPwd(ipul)-pul
          DegMax=XPwd(ipul)+pul
        endif
        go to 4800
      endif
      if(CheckType.eq.EventRolMenu) then
        KDatBlockNew=RolMenuSelectedQuest(NRolMenuDatBlock)
        if(KDatBlockNew.ne.KDatBlock) then
          StartAgain=.true.
          go to 6000
        endif
      endif
      if(CheckType.eq.EventKey) then
        if(CheckNumber.eq.JeEscape) call FeKillInfoWin
      endif
      go to 2000
4700  if(CheckNumber.eq.nButtXPlus.or.CheckNumber.eq.nButtXMinus)
     1  then
        npul=XLenAcWin/(2.*DelkaKroku)
        if(npul.le.0) npul=1
        if(CheckNumber.eq.nButtXMinus.and.npul.le.npulo) npul=npulo+1
        ipul=nst+(nen-nst)/2
        nst=max(1,ipul-npul)
        nen=min(Npnts,ipul+npul)
        DegMin=XPwd(nst)
        DegMax=XPwd(nen)
        DelkaKroku=XLenAcWin/(2.*float(nen-nst+1))
        npulo=npul
      endif
4800  if(CheckType.ne.EventButton.or.
     1   (CheckNumber.ne.nButtPPlus.and.CheckNumber.ne.nButtPMinus))
     2  then
        BraggCenter=0
      endif
      call FeKillInfoWin
      call PwdPrfKresli(0,1)
      if(.not.KresliDetails)
     1  call FeQuestButtonLabelChange(nButtDetails,LabPrf(17))
      if(ZPrf) then
        call FeQuestLblChange(nLblTitle,TitlePrf)
      else
        call FeQuestLblChange(nLblTitle,TitleM92)
      endif
      call FeQuestSbwPureOpen(SbwLastMade,nst,nen,Npnts,0,0,0)
      if(drzi) then
        call FeReleaseOutput
        call FeDeferOutput
        if(nen-nst.lt.4.or.nen-nst.gt.Npnts-2) then
          drzi=.false.
          call FeReleaseOutput
        endif
      endif
      delkaKroku=XLenAcWin/float(nen-nst)
4900  if(odEdw.or.odSkipToBragg) then
        strana=8.
4910    xc=PwdPrfTh2X(th)
        yc=YMinAcWin+10.
        vyska=anint(strana*sin(60.*torad))
        xtr(1)=xc
        ytr(1)=yc-vyska*0.5
        xtr(2)=xc+strana*0.5
        ytr(2)=yc+vyska*0.5
        xtr(3)=xc-strana*0.5
        ytr(3)=ytr(2)
        if(xtr(2).le.XMaxAcWin.and.xtr(3).ge.XMinAcWin) then
          phi=180.
          call RotatePoints(xtr,ytr,3,xc,yc,phi)
          call FePolygon(xtr,ytr,3,4,0,0,Red)
        else
          if(BraggCenter.ne.BraggCenterOld) then
            BraggCenter=BraggCenterOld
            go to 4910
          endif
        endif
      else
        if(CheckNumber.eq.nButtXPlus.or.CheckNumber.eq.nButtXMinus.or.
     1     CheckNumber.eq.nButtYPlus.or.CheckNumber.eq.nButtYMinus)
     2    then
          TakeMouseMove=.true.
          TimeDrzi=FeGetSystemTime()
5000      if(EventTypeLost.ne.0) then
            EventType=EventTypeLost
            EventNumber=EventNumberLost
            EventTypeLost=0
            EventNumberLost=0
          else
            call FeEvent(1)
          endif
          TakeMouseMove=.false.
          if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
            drzi=.false.
            call FeReleaseOutput
          else if(FeGetSystemTime()-TimeQuestEvent.lt.500) then
            go to 5000
          else
            if(FeGetSystemTime()-TimeDrzi.lt.20) go to 5000
          endif
        endif
      endif
      if(drzi) then
        go to 2300
      else
        if(iButOff.gt.0) call FeQuestButtonOff(iButOff)
        go to 2000
      endif
6000  call FeKillInfoWin
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call FeQuestRemove(id)
      TakeMouseMove=.false.
      AskIfQuit=.false.
      Veta=' '
      if(NManBackg(KDatBlock).ne.NManBackgOld) then
        if(NManBackgOld.eq.0) then
          Veta='A manual background has been created:'
        else if(NManBackg(KDatBlock).eq.0) then
          Veta='The manual background has been deleted:'
        else
          Veta='The number of points for the manual background has '//
     1         'been modified:'
        endif
      else if(allocated(XManBackgOld)) then
        if(.not.EqRV(XManBackgOld,XManBackg(1,KDatBlock),
     1               NManBackgOld,.001).or.
     2     .not.EqRV(YManBackgOld,YManBackg(1,KDatBlock),
     3               NManBackgOld,.1))
     4    Veta='Points defining the manual background have been '//
     5         'modified:'
      endif
      if(Veta.ne.' ') then
        ich=0
        id=NextQuestId()
        xqd=400.
        il=5
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,0)
        il=1
        xpom=5.
        tpom=xpom+CrwgXd+10.
        Veta='%Accept the new manual background'
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) then
            nCrwAccept=CrwLastMade
            Veta='%Discard the changes'
          else if(i.eq.2) then
            nCrwDiscard=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          il=il+4
        enddo
        xpom=xpom+30.
        tpom=tpom+30.
        il=2
        Veta='%Supress using of polynomial terms'
        do i=1,3
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        0,2)
          if(i.eq.1) then
            nCrwSupress=CrwLastMade
            Veta='%Reset polynomial coefficients to zeros'
          else if(i.eq.2) then
            nCrwReset=CrwLastMade
            Veta='%Keep polynomial coefficients unchanged'
          else
            nCrwKeep=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(NBackg(KDatBlock).le.0) call FeQuestCrwDisable(CrwLastMade)
          il=il+1
        enddo
6500    call FeQuestEvent(id,ich)
        if((CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAccept).or.
     1     (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDiscard)) then
          if(CrwLogicQuest(nCrwAccept).and.NBackg(KDatBlock).gt.0) then
            call FeQuestCrwOpen(nCrwReset,.true.)
            call FeQuestCrwOpen(nCrwSupress,.false.)
            call FeQuestCrwOpen(nCrwKeep,.false.)
          else
            call FeQuestCrwDisable(nCrwReset)
            call FeQuestCrwDisable(nCrwSupress)
            call FeQuestCrwDisable(nCrwKeep)
          endif
          go to 6500
        else if(CheckType.eq.EventGlobal) then
          go to 6500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 6500
        endif
        if(CrwLogicQuest(nCrwAccept)) then
          if(DrawInD) then
            call PwdD2X(XPwd,NPnts)
            call PwdD2X(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
          endif
          call PwdUpdateReflFiles
          KManBackg(KDatBlock)=1
          if(CrwLogicQuest(nCrwSupress)) then
            NBackg(KDatBlock)=0
          else if(CrwLogicQuest(nCrwReset)) then
            call SetRealArrayTo(BackgPwd(1,KDatBlock),NBackg(KDatBlock),
     1                          0.)
          endif
          Sc(2,KDatBlock)=1.
          call iom40(1,0,fln(:ifln)//'.m40')
          call iom41(1,0,fln(:ifln)//'.m41')
        else
          call CopyFile(PreviousM40,fln(:ifln)//'.m40')
          call CopyFile(PreviousM41,fln(:ifln)//'.m41')
          call MoveFile(fln(:ifln)//'.z90',fln(:ifln)//'.m90')
        endif
        call FeQuestRemove(id)
      else
        if(FileDiff(fln(:ifln)//'.m41',PreviousM41)) then
          Veta='The set of excluded regions has been modified.'//
     1         ' Do want to accept the changes?'
          if(.not.FeYesNo(-1.,-1.,Veta,0))
     1      call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        endif
      endif
9999  if(KDatBlockNew.ne.0) KDatBlock=KDatBlockNew
      close(75)
      if(allocated(CalcPArr))
     1  deallocate(CalcPArr,CalcPPArr,BkgArr,DifArr)
      if(allocated(BraggArr))
     1  deallocate(BraggArr,ShiftArr,MultArr,ypeakArr,KlicArr,indArr)
      if(allocated(PeakXPos))
     1  deallocate(PeakXPos,PeakInt,PeakFWHM,PeakBckg,PeakEta,PeakXPosI,
     2             PeakPor)
      if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YcPwd,YbPwd,YsPwd,
     1                                YfPwd,YiPwd,YcPwdInd)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      if(allocated(XManBackgOld)) deallocate(XManBackgOld,YManBackgOld)
      if(StartAgain) go to 1100
      KDatBlock=KDatBlockIn
      KPhase=KPhaseBasic
      DrawInD=.false.
      return
      end
