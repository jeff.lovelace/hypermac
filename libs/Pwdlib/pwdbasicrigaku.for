      subroutine PwdBasicRigaku(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      character*80  t80
      logical EqIgCase
      integer, allocatable :: ipor(:)
      integer Flags
      real, allocatable :: XPom(:),XPomOld(:),Int(:),IntOld(:),SInt(:),
     1                     SIntOld(:)
      real xp(3),ISum
      ich=0
      ib=0
      NPoints=0
      if(allocated(XPom)) deallocate(XPom,Int,SInt)
      NPointsMax=1000
      allocate(XPom(NPointsMax),Int(NPointsMax),SInt(NPointsMax))
      XPom=0.
      Int=0.
1100  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1150
      k=0
      call Kus(Veta,k,t80)
      if(EqIgCase(Veta,'*RAS_DATA_START')) then
        RigakuPwdType=RigakuPwdTypeRas
      else if(EqIgCase(Veta,'*RAS_HEADER_START')) then
        RigakuPwdType=RigakuPwdTypeRas
        go to 1150
      else if(EqIgCase(t80,'*TYPE')) then
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'=')) go to 1100
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'Raw')) go to 1100
        RigakuPwdType=RigakuPwdTypeRaw
      else
        go to 1100
      endif
      nl=0
1150  Speed=1.
      Flags=0
1200  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1200
      k=0
      call Kus(Veta,k,t80)
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        if(EqIgCase(t80,'*RAS_DATA_END')) then
          go to 1500
        else if(EqIgCase(t80,'*RAS_HEADER_END')) then
          ib=ib+1
          go to 1300
        else if(EqIgCase(t80,'*MEAS_SCAN_START')) then
          i=1
        else if(EqIgCase(t80,'*MEAS_SCAN_STEP')) then
          i=2
        else if(EqIgCase(t80,'*HW_XG_WAVE_LENGTH_ALPHA1')) then
          i=3
        else if(EqIgCase(t80,'*HW_XG_WAVE_LENGTH_ALPHA2')) then
          i=4
        else if(EqIgCase(t80,'*MEAS_SCAN_SPEED')) then
          i=5
        else if(EqIgCase(t80,'*RAS_TEMPERATURE_START')) then
1220      read(71,FormA,end=9000) Veta
          k=0
          call kus(Veta,k,t80)
          if(.not.EqIgCase(t80,'*RAS_TEMPERATURE_END')) go to 1220
          go to 1200
        else if(Veta(1:1).eq.'*') then
          go to 1200
        else
          go to 9900
        endif
        call Kus(Veta,k,t80)
        do j=1,idel(t80)
          if(t80(j:j).eq.'"') t80(j:j)=' '
        enddo
      else if(RigakuPwdType.eq.RigakuPwdTypeRaw) then
        if(EqIgCase(t80,'*EOF')) then
          go to 1500
        else if(EqIgCase(t80,'*BEGIN')) then
          go to 1200
        else if(EqIgCase(t80,'*START')) then
          i=1
        else if(EqIgCase(t80,'*STEP')) then
          i=2
        else if(EqIgCase(t80,'*WAVE_LENGTH1')) then
          i=3
        else if(EqIgCase(t80,'*WAVE_LENGTH2')) then
          i=4
        else if(EqIgCase(t80,'*SPEED')) then
          i=5
        else if(Veta(1:1).eq.'*') then
          go to 1200
        else
          if(Flags.eq.11111.or.(ib.gt.0.and.Flags.eq.10011)) then
            backspace 71
            if(RigakuPwdType.eq.RigakuPwdTypeRaw)
     1        Speed=60.*degs/Speed
            nn=0
            ib=ib+1
            go to 1400
          else
            go to 9900
          endif
        endif
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'=')) go to 1200
        call Kus(Veta,k,t80)
      endif
      read(t80,*) pom
      if(i.eq.1) then
        deg0=pom
        Flags=Flags+1
      else if(i.eq.2) then
        degs=pom
        if(ib.eq.0) degs0=pom
        Flags=Flags+10
      else if(i.eq.3) then
        LamA1RefBlock(NRefBlock)=pom
        nl=nl+1
        Flags=Flags+100
      else if(i.eq.4) then
        LamA2RefBlock(NRefBlock)=pom
        nl=nl+10
        Flags=Flags+1000
      else if(i.eq.5) then
        Speed=pom
        Flags=Flags+10000
      endif
      go to 1200
1300  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1300
      k=0
      call Kus(Veta,k,t80)
      if(.not.EqIgCase(t80,'*RAS_INT_START')) go to 1300
1400  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1400
      k=0
      call Kus(Veta,k,t80)
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        if(EqIgCase(t80,'*RAS_INT_END')) go to 1150
      else
        if(EqIgCase(t80,'*END')) go to 1150
        do j=1,idel(Veta)
          if(Veta(j:j).eq.',') Veta(j:j)=' '
        enddo
      endif
      k=0
1450  if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        call StToReal(Veta,k,xp,3,.false.,ich)
      else
        call StToReal(Veta,k,xp(2),1,.false.,ich)
        nn=nn+1
        xp(1)=deg0+float(nn-1)*degs
        xp(3)=1.
      endif
      if(NPoints.ge.NPointsMax) then
        if(allocated(XPomOld)) deallocate(XPomOld,IntOld,SIntOld)
        allocate(XPomOld(NPointsMax),IntOld(NPointsMax),
     1           SIntOld(NPointsMax))
        XPomOld=XPom
        IntOld=Int
        SIntOld=SInt
        deallocate(XPom,Int,SInt)
        NPointsMax=2*NPointsMax
        allocate(XPom(NPointsMax),Int(NPointsMax),SInt(NPointsMax))
        XPom=0.
        Int=0.
        XPom(1:NPoints)=XPomOld(1:NPoints)
        Int(1:NPoints)=IntOld(1:NPoints)
        SInt(1:NPoints)=SIntOld(1:NPoints)
        if(allocated(XPomOld)) deallocate(XPomOld,IntOld,SIntOld)
      endif
      NPoints=NPoints+1
      XPom(NPoints)=xp(1)
!      XPom(NPoints)=xp(1)+degs*.5
      pom=Speed*degs0/degs
      Int(NPoints)=xp(2)*pom
      SInt(NPoints)=max(sqrt(xp(2))*pom,1.)
      if(abs(xp(3)-1.).gt..001) then
        write(Cislo,'(f10.5)') xp(3)
        call FeWinMessage('Uz je to tady',Cislo)
      endif
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        go to 1400
      else
        if(k.lt.len(Veta)) then
          go to 1450
        else
          go to 1400
        endif
      endif
1500  call CloseIfOpened(71)
      if(NPoints.gt.0) then
        Veta=SourceFileRefBlock(KRefBlock)
     1       (:idel(SourceFileRefBlock(KRefBlock)))//'_xy'
        call OpenFile(71,Veta,'formatted','unknown')
        allocate(ipor(NPoints))
        call RIndexx(NPoints,XPom,ipor)
        do ii=1,NPoints
          i=ipor(ii)
          if(Int(i).lt.0.) cycle
          XSum=XPom(i)
          wt=1./SInt(i)**2
          SSum=(wt*SInt(i))**2
          ISum=wt*Int(i)
          WSum=wt
          do jj=ii+1,NPoints
            j=ipor(jj)
            if(XPom(j)-XSum.gt..001) exit
            if(Int(j).lt.0.) cycle
            if(abs(XPom(j)-XSum).lt..000001) then
              wt=1./SInt(j)**2
              ISum=ISum+wt*Int(j)
              SSum=SSum+(wt*SInt(j))**2
              WSum=WSum+wt
              Int(j)=-999.
            endif
          enddo
          Int(i)=ISum/WSum
          SInt(i)=sqrt(SSum)/WSum
        enddo
        do ii=1,NPoints
          i=ipor(ii)
          if(Int(i).ge.0.) write(71,'(3f15.6)') XPom(i),Int(i),SInt(i)
        enddo
        deallocate(ipor)
        rewind 71
        go to 9900
      else
        ich=1
      endif
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      ich=1
      go to 9999
9900  if(nl.ge.10) then
        NAlfaRefBlock(NRefBlock)=2
      else
        NAlfaRefBlock(NRefBlock)=1
      endif
9999  return
      end
