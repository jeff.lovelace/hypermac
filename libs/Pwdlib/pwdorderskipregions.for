      subroutine PwdOrderSkipRegions
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension ifr(MxSkipPwd),ipor(MxSkipPwd),SkipPwdFrNew(MxSkipPwd),
     1          SkipPwdToNew(MxSkipPwd)
      call PwdSetTOF(KDatBlock)
      Order=min(10**OrderSkipFrTo(KDatBlock),10000)
      n=0
      do i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).lt.-900.) cycle
        n=n+1
        SkipPwdFr(n,KDatBlock)=SkipPwdFr(i,KDatBlock)
        SkipPwdTo(n,KDatBlock)=SkipPwdTo(i,KDatBlock)
      enddo
      if(NSkipPwd(KDatBlock).le.1) go to 1500
      do i=1,NSkipPwd(KDatBlock)
        SkipPwdFrNew(i)=SkipPwdFr(i,KDatBlock)
        SkipPwdToNew(i)=SkipPwdTo(i,KDatBlock)
        ifr(i)=Order*SkipPwdFr(i,KDatBlock)
      enddo
      call indexx(NSkipPwd(KDatBlock),ifr,ipor)
      n=0
      do i=1,NSkipPwd(KDatBlock)
        j=ipor(i)
        pomFr=SkipPwdFrNew(j)
        pomTo=SkipPwdToNew(j)
        if(n.gt.0) then
          if(SkipPwdTo(n,KDatBlock).ge.pomFr) then
            pomTo=max(pomTo,SkipPwdTo(n,KDatBlock))
            go to 1150
          endif
        endif
        n=n+1
        SkipPwdFr(n,KDatBlock)=pomFr
1150    SkipPwdTo(n,KDatBlock)=pomTo
      enddo
      if(DrawInD) then
        if(isTOF) then
          XPwd3=PwdD2TOF(XPwd(3))
          XPwd1=PwdD2TOF(XPwd(1))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwd3=PwdD2TwoTh(XPwd(NPnts-2),LamPwd(1,KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(NPnts),LamPwd(1,KDatBlock))
          else
            XPwd3=PwdD2TwoTh(XPwd(NPnts-2),LamAve(KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(NPnts),LamAve(KDatBlock))
          endif
        endif
      else
        XPwd3=XPwd(3)
        XPwd1=XPwd(1)
      endif
      if(SkipPwdFr(1,KDatBlock).lt.XPwd3)
     1  SkipPwdFr(1,KDatBlock)=XPwd1*.999
      if(DrawInD) then
        if(isTOF) then
          XPwd3=PwdD2TOF(XPwd(NPnts-2))
          XPwd1=PwdD2TOF(XPwd(NPnts))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwd3=PwdD2TwoTh(XPwd(3),LamPwd(1,KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(1),LamPwd(1,KDatBlock))
          else
            XPwd3=PwdD2TwoTh(XPwd(3),LamAve(KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(1),LamAve(KDatBlock))
          endif
        endif
      else
        XPwd3=XPwd(NPnts-2)
        XPwd1=XPwd(NPnts)
      endif
      if(SkipPwdTo(n,KDatBlock).gt.XPwd3)
     1  SkipPwdTo(n,KDatBlock)=XPwd1*1.001
1500  NSkipPwd(KDatBlock)=n
      do 2200j=1,Npnts
        if(DrawInD) then
          if(isTOF) then
            XPwdj=PwdD2TOF(XPwd(j))
          else
            if(KRefLam(KDatBlock).eq.1) then
              XPwdj=PwdD2TwoTh(XPwd(j),LamPwd(1,KDatBlock))
            else
              XPwdj=PwdD2TwoTh(XPwd(j),LamAve(KDatBlock))
            endif
          endif
        else
          XPwdj=XPwd(j)
        endif
        do i=1,NskipPwd(KDatBlock)
          if(XPwdj.ge.SkipPwdFr(i,KDatBlock).and.
     1       XPwdj.le.SkipPwdTo(i,KDatBlock)) then
            YfPwd(j)=0
            go to 2200
          endif
        enddo
        if(UseCutoffPwd) then
          if(DrawInD) then
            dpom=XPwd(j)
          else
            if(isTOF) then
              if(TOFInD) then
                dpom=XPwd(j)
              else
                dpom=PwdTOF2D(XPwd(j))
              endif
            else
              dpom=PwdTwoTh2D(XPwd(j),LamA1(KDatBlock))
            endif
          endif
          if(dpom.lt.CutoffMinPwd.or.dpom.gt.CutoffMaxPwd) then
            YfPwd(j)=0
            go to 2200
          endif
        endif
        YfPwd(j)=1
2200  continue
      return
      end
