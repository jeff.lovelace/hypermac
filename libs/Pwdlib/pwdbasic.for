      subroutine PwdBasic(ich)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ip(12),xp(12),nn(7)
      character*256 Radka
      character*80  Veta,t80
      integer :: NLast = 0, nprcf(MxDatBlock)
      integer, allocatable :: Nex(:),Res(:),Npr(:),Nscp(:),Nba(:)
      logical EqIgCase,TOFParNacteny,TOFParNebude
      real LamIn
      equivalence (ip,xp)
      ich=0
      ln=0
      NTimeMaps=0
      NPointsTimeMapsMax=0
      TOFParNebude=.false.
      ISISGSS=.false.
      LamIn=-333.
      NMax=0
      BankPtyp=0
      BankPrcf=0.
      if(DifCode(KRefBlock).ne.IdDataGSAS.and.
     1   DifCode(KRefBlock).ne.IdTOFGSAS.and.
     2   DifCode(KRefBlock).ne.IdDataUXD.and.
     3   DifCode(KRefBlock).ne.IdEDInel.and.
     4   DifCode(KRefBlock).ne.IdDataPSI.and.
     5   DifCode(KRefBlock).ne.IdDataXRDML.and.
     6   DifCode(KRefBlock).ne.IdDataPwdRigaku.and.
     7   DifCode(KRefBlock).ne.IdDataPwdStoe.and.
     8   DifCode(KRefBlock).ne.IdDataG41.and.
     9   DifCode(KRefBlock).ne.IdDataRiet7) then
1010    read(71,FormA,err=9000,end=9000) Radka
        k=0
        kk=0
1020    call Kus(Radka,k,t80)
        if(t80(1:1).eq.'!') then
          if(kk.eq.0) then
            go to 1010
          else
            backspace 71
            go to 1030
          endif
        endif
        call Posun(t80,1)
        read(t80,*,err=1010) pom
        kk=k
        if(k.lt.len(t80)) go to 1020
        backspace 71
      endif
1030  if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdTOFGSAS.or.
     2   DifCode(KRefBlock).eq.IdData11BM) then
        TOFParNacteny=.false.
1100    read(71,FormA,err=9000,end=1200) Radka
        if(Radka.eq.' ') go to 1100
        if(Radka(1:1).eq.'#') then
          if(DifCode(KRefBlock).eq.IdData11BM) then
            k=1
            call kus(Radka,k,Veta)
            if(EqIgCase(Veta,'Wavelength:')) then
              call StToReal(Radka,k,xp,1,.false.,ich)
              if(ich.eq.0) LamIn=xp(1)
            endif
          endif
          go to 1100
        endif
        call mala(Radka)
        i=index(Radka,'instrument parameter file:')
        if(i.gt.0) then
          if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1       DifCode(KRefBlock).eq.IdData11BM) go to 1100
          i=index(Radka,':')
          Radka=Radka(i+1:)
          k=0
          call kus(Radka,k,InsParFileRefBlock(KRefBlock))
          i=idel(InsParFileRefBlock(KRefBlock))
1120      if(InsParFileRefBlock(KRefBlock)(i:i).ne.'/'.and.
     1       InsParFileRefBlock(KRefBlock)(i:i).ne.ObrLom) then
            i=i-1
            if(i.ne.0) go to 1120
          endif
          InsParFileRefBlock(KRefBlock)=
     1      InsParFileRefBlock(KRefBlock)(i+1:)
        else if(index(Radka,'time_map').eq.1) then
          if(DifCode(KRefBlock).eq.IdDataGSAS) go to 1100
          k=0
          call Kus(Radka,k,Veta)
          call StToInt(Radka,k,ip,3,.false.,ich)
          if(ich.ne.0) go to 9000
          NTimeMaps=max(NTimeMaps,ip(1))
          NPointsTimeMapsMax=max(NPointsTimeMapsMax,ip(2))
        endif
        go to 1100
1200    if(DifCode(KRefBlock).eq.IdTOFGSAS) then
          call PwdBasicDefInsFile(1,ich)
          if(ich.ne.0) go to 1300
          ln=NextLogicNumber()
          call OpenFile(ln,InsParFileRefBlock(KRefBlock),'formatted',
     1                  'old')
          if(ErrFlag.ne.0) go to 1200
1210      read(ln,FormA,err=9200,end=1220) Radka
          call mala(Radka)
          i=index(Radka,' icons')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+5
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            BankDifc(n)=xp(1)
            BankDifa(n)=xp(2)
            BankZero(n)=xp(3)
            BankJason(n)=0
            BankCE(n)=0.
            BankZeroE(n)=0.
            BankTCross(n)=0.
            BankWCross(n)=0.
            call SetRealArrayTo(BankSigma(1,n),3,-3333.)
            call SetRealArrayTo(BankGamma(1,n),3,-3333.)
            call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
            go to 1210
          endif
          i=index(Radka,'bnkpar')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToReal(Radka,k,xp,2,.false.,ich)
            if(ich.ne.0) go to 9200
            BankTTh(n)=xp(2)
            go to 1210
          endif
          i=index(Radka,'i ityp')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToInt(Radka,k,ip,1,.false.,ich)
            BankITyp(n)=ip(1)
            NCoff=1
          endif
          i=index(Radka,'icoff')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankICoff(NCoff,n),4)
            NCoff=NCoff+4
            go to 1210
          endif
          do j=1,10
            write(Cislo,'(''prcf'',i2)') j
            i=index(Radka,Cislo(:idel(Cislo)))
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              NMax=max(NMax,n)
              k=i+6
              m=min(4,nprcf(n)-4*(j-1))
              call StToReal(Radka,k,BankPrcf(4*(j-1)+1,n),m,.false.,ich)
              go to 1210
            endif
          enddo
          i=index(Radka,'prcf')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            read(Radka(i+6:),'(2i5)') BankPtyp(n),nprcf(n)
            read(Radka(i+16:),*) pom
            go to 1210
          endif
          go to 1210
1220      call CloseIfOpened(ln)
          do i=1,NMax
            if(BankPtyp(i).eq.3) then
              BankAlfBe(1,i)=0.
              BankAlfBe(2,i)=BankPrcf(2,i)
              BankAlfBe(3,i)=BankPrcf(1,i)
              BankAlfBe(4,i)=BankPrcf(3,i)
              BankSigma(1,i)=BankPrcf(4,i)
              BankSigma(2,i)=BankPrcf(5,i)
              BankSigma(3,i)=BankPrcf(6,i)
              BankGamma(1,i)=BankPrcf(7,i)
              BankGamma(2,i)=BankPrcf(8,i)
              BankGamma(3,i)=BankPrcf(9,i)
            endif
          enddo
        endif
1300    if(allocated(TMapTOF)) deallocate(TMapTOF,ClckWdtTOF,
     1                                    NPointsTimeMaps)
        if(NTimeMaps.gt.0) then
          NPointsTimeMapsMax=(NPointsTimeMapsMax-1)/3
          allocate(TMapTOF(3,NPointsTimeMapsMax+1,NTimeMaps),
     1             ClckWdtTOF(NTimeMaps),NPointsTimeMaps(NTimeMaps))
          rewind 71
1320      read(71,FormA,err=9000,end=1400) Radka
          if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1320
          call mala(Radka)
          if(index(Radka,'time_map').eq.1) then
            k=0
            call Kus(Radka,k,Veta)
            call StToInt(Radka,k,ip,3,.false.,ich)
            if(ich.ne.0) go to 9000
            l=ip(1)
            n=(ip(2)-1)/3+1
            NPointsTimeMaps(l)=n
            m=ip(3)
            call Kus(Radka,k,Veta)
            call StToReal(Radka,k,xp,1,.false.,ich)
            if(ich.ne.0) go to 9000
            ClckWdtTOF(l)=nint(xp(1))
            read(71,'(10i8)',err=9000,end=9000)
     1        ((TMapTOF(i,j,l),i=1,3),j=1,n-1),TMapTOF(2,n,l)
          endif
          go to 1320
        endif
1400    rewind 71
        NBanks=0
1420    read(71,FormA,err=9000,end=1430) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1420
        if(index(Radka,'bank').eq.1) NBanks=NBanks+1
        go to 1420
1430    rewind 71
1440    read(71,FormA,err=9000,end=9000) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1     index(Radka,'bank').ne.1) go to 1440
        backspace(71)
        if(DifCode(KRefBlock).eq.IdTOFGSAS) then
          RadiationRefBlock(KRefBlock)=NeutronRadiation
        else if(DifCode(KRefBlock).eq.IdData11BM) then
          RadiationRefBlock(KRefBlock)=XRayRadiation
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        endif
        if(DifCode(KRefBlock).eq.IdData11BM.and.LamIn.gt.0.) then
          LamAveRefBlock(KRefBlock)=LamIn
          LamA1RefBlock(KRefBlock)=LamIn
          LamA2RefBlock(KRefBlock)=LamIn
        endif
      else if(DifCode(KRefBlock).eq.IdTOFISIST.or.
     1        DifCode(KRefBlock).eq.IdTOFFree.or.
     2        DifCode(KRefBlock).eq.IdTOFISISFullProf) then
1500    if(KRefBlock.le.1) then
          InsParFileRefBlock(KRefBlock)=' '
        else
          InsParFileRefBlock(KRefBlock)=InsParFileRefBlock(KRefBlock-1)
        endif
        call PwdBasicDefInsFile(2,ich)
        if(ich.eq.0) then
          ln=NextLogicNumber()
          call OpenFile(ln,InsParFileRefBlock(KRefBlock),'formatted',
     1                  'old')
          if(ErrFlag.ne.0) go to 1500
          call GetExtentionOfFileName(InsParFileRefBlock(KRefBlock),
     1                                Veta)
          if(EqIgCase(Veta,'pcr')) then
            do i=1,2
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            k=0
            call Kus(Radka,k,Cislo)
            NPatt=1
            if(EqIgCase(Cislo,'npatt')) then
              call StToInt(Radka,k,ip,1,.false.,ich)
              if(ich.ne.0) go to 9999
              NPatt=ip(1)
              do i=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
            endif
            allocate(Npr(NPatt),Res(NPatt),Nex(NPatt),Nscp(NPatt),
     1               Nba(NPatt))
            k=0
            call StToInt(Radka,k,ip,6,.false.,ich)
            if(ich.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              k=0
              call StToInt(Radka,k,ip,12,.false.,ich)
              if(ich.ne.0) go to 9000
              if(ip(1).ne.-1) then
                Veta='data are not TOF type'
                go to 1595
              endif
              if(ip(11).ne.1) then
                Veta='data are in TOF microseconds'
                go to 1595
              endif
              Npr(i)=ip(2)
              if(Npr(i).eq.9) then
                BankJason(i)=0
              else if(Npr(i).eq.10) then
                BankJason(i)=1
              else
                Veta='data are not TOF type'
                go to 1595
              endif
              Nba(i)=ip(3)
              Nex(i)=ip(4)
              Nscp(i)=ip(5)
              Res(i)=ip(9)
            enddo
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              if(Res(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              endif
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              do j=1,Nex(i)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
            enddo
            do i=1,NPatt
              do j=1,Nscp(i)
                call FeWinMessage('For Nsc<>0 not implemented',' ')
              enddo
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call SetRealArrayTo(BankSigma(1,i),3,-3333.)
              call SetRealArrayTo(BankGamma(1,i),3,-3333.)
              call SetRealArrayTo(BankAlfbe(1,i),4,-3333.)
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
              k=0
              call StToReal(Radka,k,xp,7,.false.,ich)
              if(ich.ne.0) go to 9999
              BankZero(i)=xp(1)
              BankDifc(i)=xp(3)
              BankDifa(i)=xp(5)
              BankTTh(i)=xp(7)
              if(BankJason(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                k=0
                call StToReal(Radka,k,xp,5,.false.,ich)
                if(ich.ne.0) go to 9999
                BankZeroE(i)=BankZero(i)
                BankCE(i)=BankDifc(i)
                BankZero(i)=xp(1)
                BankDifc(i)=xp(2)
                BankDifa(i)=xp(3)
                BankTCross(i)=xp(4)
                BankWCross(i)=xp(5)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                if(Nba(i).eq.-3) then
                  n=4
                else if(Nba(i).eq.-2) then
                  n=1
                else if(Nba(i).eq.-1) then
                  n=6
                else if(Nba(i).eq.0) then
                  n=2
                else if(Nba(i).eq.1) then
                  n=2
                else
                  n=0
                  cycle
                endif
                do j=1,n
                  call GetNextPCR(ln,Radka,iend,ierr)
                  if(iend.ne.0) go to 1590
                  if(ierr.ne.0) go to 9000
                enddo
              endif
            enddo
1520        read(ln,FormA,end=1590,err=9000) Radka
            if(LocateSubstring(Radka,'Profile Parameters',.false.,
     1                         .true.).le.0) go to 1520
            do i=1,NPatt
              do j=1,3
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,BankSigma(1,i),3,.false.,ich)
              if(ich.ne.0) go to 9999
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,BankGamma(1,i),3,.false.,ich)
              if(ich.ne.0) go to 9999
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,xp,6,.false.,ich)
              do j=1,4
                BankAlfbe(j,i)=xp(j+2)
              enddo
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
              if(BankJason(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                k=0
                call StToReal(Radka,k,BankAlfbt(1,i),4,.false.,ich)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              endif
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            call CloseIfOpened(ln)
            deallocate(Npr,Res,Nex,Nscp,Nba)
            n=NPatt
            go to 1640
1590        Veta='end of file reached before expected'
1595        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            deallocate(Npr,Res,Nex,Nscp,Nba)
            go to 9999
           else
            call SetRealArrayTo(BankSigma(1,1),3,-3333.)
            call SetRealArrayTo(BankGamma(1,1),3,-3333.)
            call SetRealArrayTo(BankAlfbe(1,1),4,-3333.)
          endif
          nn=0
1610      read(ln,FormA,end=1620,err=9000) Radka
          call Mala(Radka)
          k=0
          call Kus(Radka,k,Cislo)
          if(EqIgCase(Cislo,'d2tof')) then
            nn(1)=nn(1)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0.and.ich.le.2) go to 9200
            do i=1,3
              if(i.le.ich-2.or.ich.eq.0) then
                pom=xp(i)
              else
                pom=0.
              endif
              if(i.eq.1) then
                BankDifc(nn(1))=pom
              else if(i.eq.2) then
                BankDifa(nn(1))=pom
              else
                BankZero(nn(1))=pom
              endif
            enddo
          else if(EqIgCase(Cislo,'zd2tof')) then
            nn(1)=nn(1)+1
            call StToReal(Radka,k,xp,2,.false.,ich)
            if(ich.ne.0) go to 9200
            BankZero(nn(1))=xp(1)
            BankDifc(nn(1))=xp(2)
          else if(EqIgCase(Cislo,'d2tot').or.
     1            EqIgCase(Cislo,'zd2tot')) then
            nn(2)=nn(2)+1
            if(EqIgCase(Cislo,'d2tot')) then
              n=4
            else
              n=5
            endif
            call StToReal(Radka,k,xp,n,.false.,ich)
            BankJason(nn(2))=1
            if(n.eq.5) then
              BankZeroE(nn(2))=xp(1)
            else
              BankZeroE(nn(2))=0.
            endif
            i=n-3
            BankCE(nn(2))=xp(i)
            BankDifa(nn(2))=xp(i+1)
            BankTCross(nn(2))=xp(i+2)
            BankWCross(nn(2))=xp(i+3)
          else if(EqIgCase(Cislo,'zd2tot')) then
            nn(2)=nn(2)+1
            call StToReal(Radka,k,xp,6,.false.,ich)
            BankJason(nn(2))=1
            BankJason(nn(2))=1
            BankCE(nn(2))=xp(1)
            BankDifa(nn(2))=xp(2)
            BankTCross(nn(2))=xp(3)
            BankWCross(nn(2))=xp(4)
            BankZeroE(nn(2))=0.
          else if(EqIgCase(Cislo,'twoth')) then
            nn(3)=nn(3)+1
            call StToReal(Radka,k,xp,1,.false.,ich)
            if(ich.ne.0) go to 9200
            BankTTh(nn(3))=xp(1)
          else if(EqIgCase(Cislo,'sigma')) then
            nn(4)=nn(4)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            pom=xp(3)
            xp(3)=xp(1)
            xp(1)=pom
            call CopyVek(xp,BankSigma(1,nn(4)),3)
          else if(EqIgCase(Cislo,'gamma')) then
            nn(5)=nn(5)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            pom=xp(3)
            xp(3)=xp(1)
            xp(1)=pom
            call CopyVek(xp,BankGamma(1,nn(5)),3)
          else if(EqIgCase(Cislo,'alfbe')) then
            nn(6)=nn(6)+1
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankAlfbe(1,nn(6)),4)
          else if(EqIgCase(Cislo,'alfbt')) then
            nn(7)=nn(7)+1
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankAlfbt(1,nn(7)),4)
          endif
          go to 1610
1620      n=nn(1)
          if(DifCode(KRefBlock).eq.IdTOFISIST.and.nn(1).le.0) then
            rewind ln
1630        read(ln,FormA,err=9200,end=1640) Radka
            call mala(Radka)
            i=index(Radka,' icons')
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              k=i+5
              call StToReal(Radka,k,xp,3,.false.,ich)
              if(ich.ne.0) go to 1640
              BankDifc(n)=xp(1)
              BankDifa(n)=xp(2)
              BankZero(n)=xp(3)
              BankJason(n)=0
              BankCE(n)=0.
              BankZeroE(n)=0.
              BankTCross(n)=0.
              BankWCross(n)=0.
              call SetRealArrayTo(BankSigma(1,n),3,-3333.)
              call SetRealArrayTo(BankGamma(1,n),3,-3333.)
              call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
              go to 1630
            endif
            i=index(Radka,'bnkpar')
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              k=i+6
              call StToReal(Radka,k,xp,2,.false.,ich)
              if(ich.ne.0) go to 1640
              BankTTh(n)=xp(2)
              go to 1630
            endif
            go to 1630
          else
            call CloseIfOpened(ln)
            if(n.gt.0) then
              do i=1,n
                if(BankJason(i).gt.0) then
                  pom=BankCE(i)
                  BankCE(i)=BankDifc(i)
                  BankDifc(i)=pom
                  pom=BankZeroE(i)
                  BankZeroE(i)=BankZero(i)
                  BankZero(i)=pom
                endif
              enddo
            endif
          endif
        else
          n=1
          ich=0
          BankJason(n)=0
          BankDifc(n)=0.
          BankDifa(n)=0.
          BankZero(n)=0.
          BankCE(n)=0.
          BankZeroE(n)=0.
          BankTTh(n)=0.
          BankTCross(n)=0.
          BankWCross(n)=0.
          call SetRealArrayTo(BankSigma(1,n),3,-3333.)
          call SetRealArrayTo(BankGamma(1,n),3,-3333.)
          call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
        endif
1640    if(DifCode(KRefBlock).eq.IdTOFISIST) then
          NBanks=0
          rewind 71
1650      read(71,FormA,err=9000,end=1660) Radka
          call mala(Radka)
          if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1650
          if(index(Radka,'bank').eq.1) NBanks=NBanks+1
          go to 1650
1660      if(NBanks.gt.0) then
            rewind 71
1670        read(71,FormA,err=9000,end=9000) Radka
            call mala(Radka)
            if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1         index(Radka,'bank').ne.1) go to 1670
            backspace(71)
            ISISGSS=.true.
          else
            rewind 71
1680        read(71,FormA,err=9000,end=1690) Radka
            call mala(Radka)
            if(Radka.eq.' ') go to 1680
            k=0
            call Kus(Radka,k,Cislo)
            if(Cislo.eq.'#s') NBanks=NBanks+1
            go to 1680
1690        rewind 71
            if(NBanks.le.0) go to 9000
          endif
        else
          if(n.gt.1) then
            WizardMode=.false.
            ich=0
            id=NextQuestId()
            xqd=250.
            il=6
            call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
            il=-7
            tpom=5.
            Veta='Please select TOF parameters which correspond'
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
            Veta='to the data block just imported:'
            il=il-6
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
            il=il-10
            Veta=' '
            dpom=50.
            call FeQuestEudMake(id,tpom,il,tpom,il,' ','L',dpom,EdwYd,1)
            nEdwN=EdwLastMade
            nsel=NLast+1
            if(nsel.gt.n) nsel=1
            call FeQuestIntEdwOpen(EdwLastMade,nsel,.false.)
            call FeQuestEudOpen(EdwLastMade,1,n,1,dpom,dpom,dpom)
            tpom=tpom+dpom+120.
            Veta='Detector angle:'
            if(BankJason(1).gt.0) then
              n=8
            else
              n=4
            endif
            do i=1,n
              call FeQuestLblMake(id,tpom,il,Veta,'R','N')
              call FeQuestLblMake(id,tpom+5.,il,' ','L','N')
              if(i.eq.1) then
                nLblFirst=LblLastMade
              else if(i.eq.n) then
                exit
              endif
              if(BankJason(1).le.0) then
                Veta=lTOF1Pwd(i)
              else
                Veta=lTOF2Pwd(i)
              endif
              Veta=Veta(:idel(Veta))//':'
              il=il-6
            enddo
1755        nLbl=nLblFirst
            do i=1,n
              if(i.eq.1) then
                pom=BankTTh(nsel)
              else if(i.eq.2) then
                pom=BankZero(nsel)
              else if(i.eq.3) then
                pom=BankDifc(nsel)
              else if(i.eq.4) then
                pom=BankDifa(nsel)
              else if(i.eq.5) then
                pom=BankZeroE(nsel)
              else if(i.eq.6) then
                pom=BankCE(nsel)
              else if(i.eq.7) then
                pom=BankWCross(nsel)
              else if(i.eq.8) then
                pom=BankTCross(nsel)
              endif
              write(Veta,101) pom
              call ZdrcniCisla(Veta,1)
              call FeQuestLblChange(nLbl,Veta)
              nLbl=nLbl+2
            enddo
1760        call FeQuestEvent(id,ich)
            if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwN) then
              call FeQuestIntFromEdw(nEdwN,nsel)
              go to 1755
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 1760
            endif
            call FeQuestRemove(id)
            if(nsel.ne.1) then
              BankTTh (1)=BankTTh (nsel)
              BankDifc(1)=BankDifc(nsel)
              BankDifa(1)=BankDifa(nsel)
              BankZero(1)=BankZero(nsel)
              if(BankJason(1).gt.0) then
                BankZeroE(1)=BankZeroE(nsel)
                BankCE(1)=BankCE(nsel)
                BankWCross(1)=BankWCross(nsel)
                BankTCross(1)=BankTCross(nsel)
              endif
              call CopyVek(BankSigma(1,nsel),BankSigma(1,1),3)
              call CopyVek(BankGamma(1,nsel),BankGamma(1,1),3)
              call CopyVek(BankAlfbe(1,nsel),BankAlfbe(1,1),4)
              call CopyVek(BankAlfbt(1,nsel),BankAlfbt(1,1),4)
            endif
            WizardMode=.true.
            NLast=nsel
          endif
          NBanks=1
        endif
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdTOFISISD) then
        NBanks=0
        rewind 71
1800    read(71,FormA,err=9000,end=1820) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1800
        k=0
        call Kus(Radka,k,Cislo)
        if(Cislo.eq.'#s') NBanks=NBanks+1
        go to 1800
1820    rewind 71
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        NBanks=1
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdEDInel) then
1950    read(71,FormA,err=9000,end=1960) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1950
        k=0
        n=0
        call kus(Radka,k,Veta)
        if(EqIgCase(Veta,'CAL_OFFSET:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDOffsetRefBlock(KRefBlock)=xp(1)*1000.
          if(mod(n,10).eq.0) n=n+1
        else if(EqIgCase(Veta,'CAL_SLOPE:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDSlopeRefBlock(KRefBlock)=xp(1)*1000.
         if(mod(n/10,10).eq.0) n=n+10
        else if(EqIgCase(Veta,'TWO_THETA:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDTThRefBlock(KRefBlock)=xp(1)
          EDEnergy2DRefBlock(KRefBlock)=
     1      .5*TokEV/sin(EDTThRefBlock(KRefBlock)*.5*ToRad)
          if(mod(n/100,100).eq.0) n=n+100
        else if(EqIgCase(Veta,'DATA:')) then
          go to 1960
        endif
        go to 1950
1960    RadiationRefBlock(KRefBlock)=XRayRadiation
!      else if(DifCode(KRefBlock).eq.IdTOFFree) then
!        NBanks=1
!        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdDataFreeOnlyI) then
2100    read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 2100
        deg0=xp(1)
        degs=xp(2)
        PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      else if(DifCode(KRefBlock).eq.IdDataD1AD2B) then
        NImpPwd=2
        read(71,'(16x,f8.3)',err=9000) degs
        read(71,'(f8.3)',err=9000) deg0
        read(71,'(2f8.0)',err=9000) RMoni,RMoniOld
        if(RMoniOld.lt.1.) then
          CNormD1AD2B=1.
        else
          CNormD1AD2B=RMoni/RMoniOld
        endif
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataD1BD20) then
        NImpPwd=2
        read(71,FormA,err=9000)
        read(71,FormA,err=9000)
        read(71,FormA,err=9000)
        read(71,'(f13.0,f9.0,1X,10(f8.3,1X))') pom,pom,deg0,pom,pom,
     1                                         pom,pom,pom,degs
        read(71,FormA,err=9000)
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataSaclay.or.
     1        DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     2        DifCode(KRefBlock).eq.IdDataRiet7) then
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        degs=xp(2)
        deg0=xp(1)
        if(DifCode(KRefBlock).eq.IdDataRiet7) then
          read(71,FormA,err=9000,end=9000) Radka
          k=0
          call kus(Radka,k,t80)
          kp=k
2140      if(Radka(kp:kp).eq.' ') then
            kp=kp-1
            go to 2140
          endif
          call kus(Radka,k,t80)
2150      if(Radka(k:k).eq.' ') then
            k=k-1
            go to 2150
          endif
          backspace 71
          if(k.eq.8) then
            NImpPwd=2
          else if(k.eq.16) then
            if(kp.eq.8.and.index(Radka,'.').le.0) then
              NImpPwd=1
            else
              NImpPwd=-2
            endif
          endif
        else
          NImpPwd=2
        endif
        if(DifCode(KRefBlock).eq.IdDataSaclay.or.
     1     DifCode(KRefBlock).eq.IdDataD1AD2BOld) then
          RadiationRefBlock(KRefBlock)=NeutronRadiation
          PolarizationRefBlock(KRefBLock)=PolarizedLinear
        else
          if(NImpPwd.eq.2) then
            RadiationRefBlock(KRefBlock)=NeutronRadiation
            PolarizationRefBlock(KRefBLock)=PolarizedLinear
          else if(NImpPwd.eq.-2) then
            PolarizationRefBlock(KRefBLock)=PolarizedLinear
          endif
        endif
      else if(DifCode(KRefBlock).eq.IdDataPSI) then
        read(71,FormA,err=9000,end=9000) Radka
        read(71,FormA,err=9000,end=9000) Radka
        k=LocateSubstring(Radka,'lambda=',.false.,.true.)
        if(k.gt.0) then
          k=k+7
          i=0
2170      if(Radka(k:k).eq.' ') then
            k=k+1
            go to 2170
          else if(index(Cifry,Radka(k:k)).gt.0) then
            i=i+1
            Cislo(i:i)=Radka(k:k)
            k=k+1
            go to 2170
          endif
          read(Cislo,'(f15.0)',err=2175) pom
          LamAveRefBlock(KRefBlock)=pom
          LamA1RefBlock(KRefBlock)=pom
          LamA2RefBlock(KRefBlock)=pom
        endif
2175    NImpPwd=1
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,3,.false.,ich)
        if(ich.ne.0) then
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9000
          xp(3)=360.
        endif
        deg0=xp(1)
        degs=xp(2)
        NPntsMax=nint((xp(3)-deg0)/degs)+1
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBLock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataCPI) then
        NImpPwd=1
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        deg0=xp(1)
        read(71,FormA,err=9000,end=9000) Radka
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        degs=xp(1)
2200    read(71,FormA,err=9000,end=9000) Radka
        if(.not.EqIgCase(Radka,'scandata')) go to 2200
        PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      else if(DifCode(KRefBlock).eq.IdDataUXD) then
        n=0
2210    read(71,FormA,err=9000,end=9000) Radka
        call velka(Radka)
        if(index(Radka,'_FILEVERSION').ne.0) then
          j=0
        else if(index(Radka,'_STEPSIZE').ne.0) then
          j=1
          go to 2220
        else if(index(Radka,'_2THETACOUNTS').ne.0.or.
     1          index(Radka,'_2THETACPS').ne.0) then
          UXDVersion=-UXDVersion
          go to 2210
        else if(index(Radka,'_2THETA').ne.0) then
          j=10
          go to 2220
        else
          if(Radka(1:1).eq.';'.or.Radka(1:1).eq.'_') then
            go to 2210
          else
            if(mod(n,10).lt.1.or.n/10.lt.0) then
              if(n.eq.0) then
                Radka='neither scan step nor starting 2th were found.'
              else if(n/10.lt.0) then
                Radka='starting 2th wasn''t found.'
              else
                Radka='scan step wasn''t found.'
              endif
              call FeChybne(-1.,-1.,Radka,' ',SeriousError)
              call FeTxOutEnd
              ErrFlag=1
              go to 9900
            endif
            backspace 71
            go to 9900
          endif
        endif
2220    i=index(Radka,'=')
        if(i.gt.0) then
          Cislo=Radka(i+1:)
          if(j.eq.0) then
            i=idel(Cislo)+1
            Cislo(i:i)='.'
          endif
          read(Cislo,100,err=2210) pom
          if(j.eq.0) then
            UXDVersion=nint(pom)
          else if(j.eq.1) then
            degs=pom
          else if(j.eq.10) then
            deg0=pom
          endif
          n=n+j
        endif
        go to 2210
      else if(DifCode(KRefBlock).eq.IdDataXRDML) then
        call  PwdBasicXRDML(ich)
        deg0=0.
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdRigaku) then
        call  PwdBasicRigaku(ich)
        deg0=0.
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdHuber) then
        call  PwdBasicHuber(ich)
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        call  PwdBasicStoe(ich)
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataG41) then
        call  PwdBasicG41(ich)
        if(ich.ne.0) go to 9999
      else
        deg0=0.
      endif
      go to 9900
9000  call FeReadError(71)
      ich=1
      go to 9999
9200  call FeReadError(ln)
      call CloseIfOpened(ln)
9900  deg=deg0
9999  return
100   format(f15.0)
101   format(f15.4)
102   format(i2)
      end
