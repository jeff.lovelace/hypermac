      subroutine PwdImport(ln,Konec,nalloc,ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ip(4),xp(4)
      integer FeReadBinaryFile,StoeDataLength
      character*512 Radka
      character*1 RadkaP(512)
      character*2   Ch2
      character*4   Ch4
      integer*4 I4
      integer*2 I2
      logical EqIgCase
      equivalence (Ch2,I2),(Ch4,I4,R4)
      equivalence (ip,xp),(Radka,RadkaP)
      xp(3)=0.
      Konec=0
      ich=0
1100  if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        StoeDataLength=FeReadBinaryFile(LnStoe,RadkaP,512)
        if(StoeDataLength.le.0) go to 5000
      else
        read(ln,FormA,err=9100,end=5000) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.
     1     (Radka(1:1).eq.'#'.and.
     2      DifCode(KRefBlock).ne.IdTOFISISD.and.
     3      (DifCode(KRefBlock).ne.IdTOFISIST.or.ISISGSS)))
     4    go to 1100
      endif
      if(DifCode(KRefBlock).eq.IdDataD1AD2B) then
        k=0
        call kus(Radka,k,Cislo)
        if(Cislo(1:1).eq.'-') go to 1100
      endif
      if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdData11BM.or.
     2   DifCode(KRefBlock).eq.IdTOFGSAS) then
        if(index(Radka,'bank').eq.1.or.
     1     index(Radka,'time_map').eq.1) go to 5000
        i=0
1120    i=i+1
        if(Radka(i:i).eq.' ') go to 1120
        if(index(Cifry(1:10),Radka(i:i)).le.0) go to 1100
        if(TypeBank.eq.'esd'.or.TypeBank.eq.'std'.or.
     1     TypeBank.eq.'alt'.or.TypeBank.eq.'fxye'.or.
     2     TypeBank.eq.' ') then
          if(LRALF) then
            n=idel(Radka)/20
          else if(TypeBank.eq.'fxye') then
            n=1
          else
            n=idel(Radka)/8
            if(TypeBank.eq.'esd') n=n/2
          endif
          if(NPnts+n.gt.nalloc) call PwdImportReallocate(nalloc)
          if(.not.IncSpectFileOpened)
     1      call SetRealArrayTo(YiPwd(NPnts+1),n,0.)
          if(LRALF) then
            if(ln.eq.71) then
              read(Radka,FormatBank)(XPwd(i),YoPwd(i),YsPwd(i),
     1                               i=NPnts+1,NPnts+n)
            else
              read(Radka,FormatBank)(XPwd(i),YiPwd(i),YsPwd(i),
     1                               i=NPnts+1,NPnts+n)
              call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
            endif
            do i=NPnts+1,NPnts+n
              XPwd(i)=XPwd(i)/32000.
            enddo
          else
            if(TypeBank.eq.'esd') then
              if(ln.eq.71) then
                read(Radka,FormatBank)(YoPwd(i),YsPwd(i),
     1                                 i=NPnts+1,NPnts+n)
              else
                read(Radka,FormatBank)(YiPwd(i),YsPwd(i),
     1                                 i=NPnts+1,NPnts+n)
                call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
              endif
              call SetRealArrayTo(XPwd(NPnts+1),n,0.)
            else if(TypeBank.eq.'std'.or.TypeBank.eq.' ') then
              if(ln.eq.71) then
                read(Radka,FormatBank)(ii,YoPwd(i),
     1                                 i=NPnts+1,NPnts+n)
              else
                read(Radka,FormatBank)(ii,YiPwd(i),
     1                                 i=NPnts+1,NPnts+n)
                call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
              endif
              call SetRealArrayTo(XPwd(NPnts+1),n,0.)
              call SetRealArrayTo(YsPwd(NPnts+1),n,0.)
            else if(TypeBank.eq.'fxye') then
              k=0
              call StToReal(Radka,k,xp,3,.false.,ich)
              if(ln.eq.71) then
                XPwd(NPnts+n)=xp(1)*.001
                YoPwd(NPnts+n)=xp(2)
                YsPwd(NPnts+n)=xp(3)
              else
                XPwd(NPnts+n)=xp(1)*.001
                YiPwd(NPnts+n)=xp(2)
                YsPwd(NPnts+n)=xp(3)
              endif
            endif
          endif
          if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1       DifCode(KRefBlock).eq.IdData11BM) then
            do i=NPnts+1,NPnts+n
              XPwd(i)=deg0+float(i-1)*degs
            enddo
          else
            if(.not.LRALF.and.allocated(NPointsTimeMaps).and.
     1         ln.eq.71) then
              jp=1
              do i=NPnts+1,NPnts+n
                do j=jp,NPointsTimeMaps(NTimeMap)-1
                  if(i.lt.TMapTOF(1,j,NTimeMap)) go to 1230
                enddo
                j=NPointsTimeMaps(NTimeMap)
1230            jp=j-1
                XPwd(i)=float(TMapTOF(2,jp,NTimeMap)+
     1            (i-TMapTOF(1,jp,NTimeMap))*TMapTOF(3,jp,NTimeMap))*
     2            float(ClckWdtTOF(NTimeMap))/1000000.
!                pom=10000./(PwdIncIntFun(XPwd(i),KRefBlock)*
!     1                      float(TMapTOF(3,jp,NTimeMap)))
!                if(YiPwd(i).gt.0.) then
!                  pom=100./(YiPwd(i)*float(TMapTOF(3,jp,NTimeMap)))
!                else
!                  pom=0.
!                endif
                pom=1./TMapTOF(3,jp,NTimeMap)
                YoPwd(i)=pom*YoPwd(i)
                YsPwd(i)=pom*YsPwd(i)
                YiPwd(i)=pom*YiPwd(i)
              enddo
            endif
          endif
          NPnts=NPnts+n
        endif
        go to 9999
      else if(DifCode(KRefBlock).eq.IdDataMAC.or.
     1   DifCode(KRefBlock).eq.IdDataAPS.or.
     2   DifCode(KRefBlock).eq.IdDataFree.or.
     3   DifCode(KRefBlock).eq.IdDataXRDML.or.
     4   DifCode(KRefBlock).eq.IdDataPwdRigaku.or.
     5   DifCode(KRefBlock).eq.IdDataM92.or.
     6   DifCode(KRefBlock).eq.IdTOFISISD.or.
     7   DifCode(KRefBlock).eq.IdTOFISIST.or.
     8   DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     9   DifCode(KRefBlock).eq.IdTOFFree) then
        if(DifCode(KRefBlock).eq.IdDataM92) then
          k=0
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9100
          if(xp(1).gt.900.) go to 5000
          call StToReal(Radka,k,xp(2),2,.false.,ich)
          if(ich.ne.0) go to 9100
        else if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1          (DifCode(KRefBlock).eq.IdTOFISIST.and..not.ISISGSS))
     2    then
          if(Radka(1:1).eq.'#') go to 5000
          k=0
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9100
        else if(DifCode(KRefBlock).eq.IdTOFISIST) then
          if(index(Radka,'bank').eq.1) go to 5000
          k=0
          if(JenDve) then
            n=2
            xp(3)=0.
          else
            n=3
          endif
          call StToReal(Radka,k,xp,n,.false.,ich)
          if(ich.ne.0) go to 9100
          xp(1)=xp(1)*.001
        else if(DifCode(KRefBlock).eq.IdTOFFree.or.
     1          DifCode(KRefBlock).eq.IdTOFISISFullProf) then
          if(EqIgCase(Radka,'END')) go to 5000
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9100
          if(k.lt.len(Radka)) call StToReal(Radka,k,xp(3),1,.false.,ich)
          if(ich.ne.0) go to 9100
          xp(1)=xp(1)*.001
        else
          k=0
          call GetRealLengthPoint(Radka,k,i,j)
          iorderA=max(iorderA,i)
          ipointA=max(ipointA,j)
          call GetRealLengthPoint(Radka,k,i,j)
          iorderI=max(iorderI,i)
          ipointI=max(ipointI,j)
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9100
          if(k.lt.len(Radka)) then
            kk=k
            call GetRealLengthPoint(Radka,k,i,j)
            iorderI=max(iorderI,i)
            ipointI=max(ipointI,j)
            k=kk
            call StToReal(Radka,k,xp(3),1,.false.,ich)
          endif
          if(ich.ne.0) go to 9100
        endif
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        if(DifCode(KRefBlock).eq.IdDataAPS) XPwd(Npnts)=XPwd(Npnts)*.01
        YoPwd(Npnts)=xp(2)
        YiPwd(Npnts)=0.
        YsPwd(Npnts)=xp(3)
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        if(index(Radka,'end').eq.1) go to 5000
        k=0
        call StToReal(Radka,k,xp,4,.false.,ich)
        if(ich.ne.0) go to 9100
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)*.001
        pom=10000./xp(4)
        YoPwd(Npnts)=xp(2)*pom
        YiPwd(Npnts)=0.
        YsPwd(Npnts)=sqrt(xp(2)/xp(3))*pom
      else if(DifCode(KRefBlock).eq.IdDataFreeOnlyI.or.
     1        DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     2        DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     3        DifCode(KRefBlock).eq.IdDataD1BD20.or.
     4        DifCode(KRefBlock).eq.IdDataSaclay.or.
     5        DifCode(KRefBlock).eq.IdDataPSI.or.
     6        DifCode(KRefBlock).eq.IdDataCPI.or.
     7        DifCode(KRefBlock).eq.IdDataRiet7.or.
     8        DifCode(KRefBlock).eq.IdDataUXD.or.
     9        DifCode(KRefBlock).eq.IdDataPwdHuber.or.
     a        DifCode(KRefBlock).eq.IdDataG41) then
        k=0
        if(DifCode(KRefBlock).eq.IdDataUXD.and.UXDVersion.lt.0)
     1    call Kus(Radka,k,Cislo)
2200    if(DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     1     DifCode(KRefBlock).eq.IdDataD1AD2BOld) then
          read(Radka(k+1:),'(i2,f6.0)',err=9100) i,xp(2)
          if(DifCode(KRefBlock).eq.IdDataD1AD2B.and.
     1       CNormD1AD2B.ne.1.) xp(2)=CNormD1AD2B*xp(2)
          xp(3)=max(i,1)
          k=k+8
        else if(DifCode(KRefBlock).eq.IdDataD1BD20) then
          read(Radka(k+1:),'(i2,f8.0)',err=9100) i,xp(2)
          xp(3)=max(i,1)
          k=k+10
        else if(DifCode(KRefBlock).eq.IdDataSaclay) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
        else if(DifCode(KRefBlock).eq.IdDataRiet7) then
          if(NImpPwd.eq.1) then
            read(Radka(k+1:),'(f8.0)',err=9100) xp(2)
            k=k+8
          else if(NImpPwd.eq.2) then
            read(Radka(k+1:),'(f2.0,f6.0)',err=9100)(xp(4-i),i=1,2)
            k=k+8
          else if(NImpPwd.eq.-2) then
            read(Radka(k+1:),'(f6.0,f10.0)',err=9100)(xp(4-i),i=1,2)
            k=k+16
          endif
        else if(DifCode(KRefBlock).eq.IdDataG41) then
          read(Radka(k+1:),'(f8.1)',err=9100) xp(2)
          if(IVariG41.le.0) xp(3)=sqrt(CNormG41*xp(2))
          k=k+8
        else if(DifCode(KRefBlock).eq.IdDataCPI.or.
     1          DifCode(KRefBlock).eq.IdDataUXD.or.
     2          DifCode(KRefBlock).eq.IdDataFreeOnlyI.or.
     3          DifCode(KRefBlock).eq.IdDataPwdHuber) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
        else if(MaxLength.ne.8) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
          if(ich.ne.0) go to 9100
        else
          read(Radka(k+1:),'(5f8.0)')(xp(i),i=2,NImpPwd+1)
          k=k+MaxLength*NImpPwd
          if(Radka(k+1:).eq.' ') k=256
        endif
        xp(1)=deg
        if(NImpPwd.gt.1.or.NImpPwd.eq.-2) then
          if(DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     1       DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     2       DifCode(KRefBlock).eq.IdDataSaclay) then
            xp(3)=sqrt(xp(2)/xp(3))
          else
            xp(3)=0.
          endif
        endif
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        if(DifCode(KRefBlock).eq.IdDataPSI.or.
     1     (DifCode(KRefBlock).eq.IdDataG41.and.IVariG41.gt.0)) then
          if(PwdInputInt) then
            YoPwd(Npnts)=xp(2)
            YiPwd(Npnts)=0.
            YsPwd(Npnts)=0.
          else
            YsPwd(Npnts)=xp(2)
          endif
          if(Npnts.ge.NPntsMax) then
            if(PwdInputInt) then
              PwdInputInt=.false.
              NPnts=0
            else
              go to 5000
            endif
          endif
        else
          YoPwd(Npnts)=xp(2)
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=xp(3)
        endif
        deg=deg0+float(Npnts)*degs
        if(k.lt.idel(Radka)) go to 2200
      else if(DifCode(KRefBlock).eq.IdEDInel) then
        k=0
3100    call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9100
        NInel=NInel+1
        if(xp(1).gt.0..or.Npnts.gt.0) then
          if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
          Npnts=Npnts+1
          XPwd(Npnts)=(EDOffsetRefBlock(KRefBlock)+
     1                 EDSlopeRefBlock(KRefBlock)*NInel)*.001
          YoPwd(Npnts)=xp(1)
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=0.
        endif
        if(k.lt.len(Radka)) go to 3100
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        k=1
        do j=1,StoeDataLength/StoeDataType
          if(Npnts.ge.NPntsMax) go to 5000
          if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
          Npnts=Npnts+1
          if(StoeDataType.eq.2) then
            ch2=Radka(k:k+1)
            YoPwd(Npnts)=i2
          else
            ch4=Radka(k:k+3)
            YoPwd(Npnts)=i4
          endif
          XPwd(Npnts)=deg0+(Npnts-1)*degs
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=0.
          k=k+StoeDataType
        enddo
      endif
      go to 9999
5000  Konec=1
      go to 9999
9100  call FeReadError(ln)
      ich=1
9999  return
      end
