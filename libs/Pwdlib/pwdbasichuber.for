      subroutine PwdBasicHuber(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      character*80  t80
      real xp(2)
      integer ip(1)
      logical EqIgCase
      ich=0
      NImpPwd=1
      n=0
1100  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1100
      n=n+1
      if(n.gt.20) then
        HuberPwdType=HuberPwdTypeMdi
        go to 1500
      endif
      if(LocateSubstring(Veta,'/*<beginc>',.false.,.true.).gt.0) then
1200    if(LocateSubstring(Veta,'<endc>*/',.false.,.true.).gt.0) then
          HuberPwdType=HuberPwdTypeGDF
          go to 1300
        else
          read(71,FormA,end=9000) Veta
          go to 1200
        endif
      else
        go to 1100
      endif
1300  read(71,*,end=9000) Deg0
      read(71,*,end=9000) DegF
      read(71,*,end=9000) DegS
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      do i=1,3
        read(71,FormA,end=9000) Veta
      enddo
      go to 9999
      entry PwdBasicHuberMDI(ich)
1500  rewind 71
      do i=1,2
        read(71,FormA,end=9000) Veta
      enddo
      k=0
      call StToReal(Veta,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 9100
      Deg0=xp(1)
      DegS=xp(2)
      call Kus(Veta,k,Cislo)
      call StToReal(Veta,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 9100
      LamA1RefBlock(KRefBlock)=xp(1)
      DegF=xp(2)
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      go to 9999
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  call CloseIfOpened(71)
      ich=1
9999  return
      end
