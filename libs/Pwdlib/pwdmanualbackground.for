      function PwdManualBackground(tth)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(NManBackg(KDatBlock).le.1) then
        PwdManualBackground=0.
        go to 9999
      endif
      do ik=2,NManBackg(KDatBlock)
        if(tth.lt.XManBackg(ik,KDatBlock)) go to 2000
      enddo
      ik=NManBackg(KDatBlock)
2000  ip=ik-1
      PwdManualBackground=YManBackg(ip,KDatBlock)+
     1  (YManBackg(ik,KDatBlock)-YManBackg(ip,KDatBlock))/
     2  (XManBackg(ik,KDatBlock)-XManBackg(ip,KDatBlock))*
     3  (tth-XManBackg(ip,KDatBlock))
9999  return
      end
