      subroutine PwdOptions(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*256 Veta
      character*80  t80
      integer :: KartIdZpet=1
      logical Zpet,PwdCheckTOFInD,EqIgCase,Wild,EqWild
      external PwdOptionsRefresh
      save /PwdOptionsC/
      ln=0
      if(allocated(PhaseGr)) deallocate(PhaseGr)
      allocate(PhaseGr(NPhase))
      PhaseGr=0
      if(NPhase.gt.1) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.m50','formatted','unknown')
        if(ErrFlag.ne.0) go to 2000
1100    read(ln,FormA,end=2000) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'refine')) go to 1100
        NGr=0
1200    read(ln,FormA,end=2000) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'restric')) then
          call kus(Veta,k,t80)
          if(k.ge.len(Veta)) go to 1200
          call kus(Veta,k,Cislo)
          if(.not.EqIgCase(Cislo,'profile')) go to 1200
          NGr=NGr+1
          n=0
1300      Wild=index(t80,'*').gt.0.or.index(t80,'?').gt.0
          do KPh=1,NPhase
            if(PhaseGr(KPh).ne.0) cycle
            if(Wild) then
              if(EqWild(PhaseName(KPh),t80,.false.)) then
                PhaseGr(KPh)=NGr
                n=n+1
              endif
            else
              if(EqIgCase(PhaseName(KPh),t80)) then
                PhaseGr(KPh)=NGr
                n=n+1
                exit
              endif
            endif
          enddo
          if(k.lt.len(Veta)) then
            call kus(Veta,k,t80)
            go to 1300
          endif
1400      if(n.le.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.NGr) PhaseGr(KPh)=0
            enddo
            NGr=NGr-1
          endif
          go to 1200
        else if(EqIgCase(Cislo,'end')) then
          go to 2000
        else
          go to 1200
        endif
      endif
2000  if(ln.gt.0) call CloseIfOpened(ln)
      MenuPhaseUse=1
      KeyMenuPhaseUse=0
      do i=1,NPhase
        n=0
        do j=1,NPhase
          if(PhaseGr(j).eq.i) then
            if(n.gt.0) MenuPhaseUse(j)=0
            n=n+1
          endif
        enddo
      enddo
      KDatBlockIn=KDatBlock
      KPhaseIn=KPhase
      KartIdZpet=1
      if(NDatBlock.gt.1) then
        KDatB=0
        do i=1,NDatBlock
          if(DataType(i).eq.1) then
            MenuDatBlockUse(i)=0
          else
            if(KDatB.le.0) KDatB=i
            MenuDatBlockUse(i)=1
          endif
        enddo
        if(MenuDatBlockUse(KDatBlock).eq.0) KDatBlock=KDatB
      endif
2100  do KRefB=1,NRefBlock
        if(RefDatCorrespond(KRefB).eq.KDatBlock) then
          KRefBlock=KRefB
          exit
        endif
      enddo
      call PwdSetTOF(KDatBlock)
      if(kcommenMax.ne.0) call comsym(0,0,ich)
      LastAsymProfile=KAsym(KDatBlock)
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        do i=1,7
          if(i.eq.2.or.i.eq.6.or.i.eq.7)
     1      AsymPwd(i,KDatBlock)=AsymPwd(i,KDatBlock)/ToRad
        enddo
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
          GaussPwd(1,KPhase,KDatBlock)=1000.
          GaussPwd(3,KPhase,KDatBlock)=.01
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
          LorentzPwd(1,KPhase,KDatBlock)=1000.
          LorentzPwd(3,KPhase,KDatBlock)=.01
        endif
      endif
      call PwdGetLamFiles
      IZdvihRec=(KDatBlock-1)*NParRecPwd
      IZdvihCell=(KPhase-1)*NParCellProfPwd
      IZdvihProf=IZdvihCell+(KDatBlock-1)*NParProfPwd
      xqd=600.
      Veta='Powder options'
      if(NPhase.gt.1) Veta=Veta(:idel(Veta))//' for phase : '//
     1                     PhaseName(KPhase)
      if(NDatBlock.gt.1) then
        if(NPhase.gt.1) then
          Veta=Veta(:idel(Veta))//' and datblock : '//
     1         MenuDatBlock(KDatBlock)
        else
          Veta=Veta(:idel(Veta))//' for datblock : '//
     1         MenuDatBlock(KDatBlock)
        endif
      endif
      il=16
      call FeKartCreate(-1.,-1.,xqd,il,Veta,0,OKForBasicFiles)
      n=0
      nRolMenuDatBlock=0
      nRolMenuPhase=0
      if(NPhase.gt.1) n=n+1
      if(NDatBlock.gt.1) n=n+1
      if(n.gt.0) then
        ypom=35.
        if(NDatBlock.gt.1) then
          dpom=0.
          do i=1,NDatBlock
            dpom=max(dpom,FeTxLength(MenuDatBlock(i)))
          enddo
          if(MenuDatBlockUse(KDatBlock).eq.0) KDatBlock=KDatB
          dpom=dpom+2.*EdwMarginSize+EdwYd
          Veta=' '
          xpom=xqd*.5-dpom*.5
          if(n.gt.1) xpom=xpom-dpom*.5-5.
          tpom=xpom
          call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                               Veta,'L',dpom,EdwYd,1)
          nRolMenuDatBlock=RolMenuLastMade+RolMenuFr-1
          call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1      MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
        endif
        if(NPhase.gt.1) then
          dpom=0.
          do i=1,NPhase
            dpom=max(dpom,FeTxLength(PhaseName(i)))
          enddo
          dpom=dpom+2.*EdwMarginSize+EdwYd
          Veta=' '
          xpom=xqd*.5-dpom*.5
          if(n.gt.1) xpom=xpom+dpom*.5+5.
          tpom=xpom
          call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                               Veta,'L',dpom,EdwYd,1)
          nRolMenuPhase=RolMenuLastMade+RolMenuFr-1
          call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                            KPhase)
        endif
      endif
      xcq=xqd*.5-KartSidePruh
      xqdp=xqd-2.*KartSidePruh
      pom=(xqdp-3.*75.-10.)/3.
      pom=(xqdp-80.-pom)/2.
      xpoma(3)=xqdp-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeCreateListek('Cell',0)
      KartIdCell=KartLastId
      call PwdOptionsCellMake(KartIdCell)
      call FeCreateListek('Radiation',0)
      KartIdRadiation=KartLastId
      call PwdOptionsRadiationMake(KartIdRadiation)
      call FeCreateListek('Profile',0)
      KartIdProf=KartLastId
      call PwdOptionsProfMake(KartIdProf)
      call FeCreateListek('Asymmetry/Diffractometer',0)
      KartIdAsym=KartLastId
      call PwdOptionsAsymMake(KartIdAsym)
      Veta='Sample'
      if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown)
     1  Veta=Veta(:idel(Veta))//'/Experiment'
      call FeCreateListek(Veta,0)
      KartIdSample=KartLastId
      call PwdOptionsSampleMake(KartIdSample)
      call FeCreateListek('Corrections',0)
      KartIdCorr=KartLastId
      call PwdOptionsCorrMake(KartIdCorr)
      call FeCreateListek('Various',0)
      KartIdVarious=KartLastId
      call PwdOptionsVariousMake(KartIdVarious)
      call FeCompleteKart(KartIdZpet)
6000  Zpet=.false.
      if(nRolMenuDatBlock.gt.0) RolMenuAlways(nRolMenuDatBlock)=.true.
      if(nRolMenuPhase   .gt.0) RolMenuAlways(nRolMenuPhase)   =.true.
      call FeQuestEventWithKartUpdate(KartId,ich,PwdOptionsRefresh)
      if(nRolMenuDatBlock.gt.0) RolMenuAlways(nRolMenuDatBlock)=.false.
      if(nRolMenuPhase   .gt.0) RolMenuAlways(nRolMenuPhase)   =.false.
      if(CheckType.eq.EventRolMenu.and.
     1   CheckNumberAbs.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        KPhaseNew=0
        if(KDatBlockNew.ne.KDatBlock.and.KDatBlockNew.ne.0) then
          Zpet=.true.
          KartIdZpet=KartId-KartIdCell+1
          go to 8000
        else
          go to 6000
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumberAbs.eq.nRolMenuPhase) then
        KDatBlockNew=0
        KPhaseNew=RolMenuSelected(nRolMenuPhase)
        if(KPhaseNew.ne.KPhase.and.KPhaseNew.ne.0) then
          Zpet=.true.
          KartIdZpet=KartId-KartIdCell+1
          go to 8000
        else
          go to 6000
        endif
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdCell) then
          call PwdOptionsCellCheck
        else if(KartId.eq.KartIdRadiation) then
          call PwdOptionsRadiationCheck
        else if(KartId.eq.KartIdProf) then
          call PwdOptionsProfCheck
        else if(KartId.eq.KartIdAsym) then
          call PwdOptionsAsymCheck
        else if(KartId.eq.KartIdSample) then
          call PwdOptionsSampleCheck
        else if(KartId.eq.KartIdCorr) then
          call PwdOptionsCorrCheck
        else if(KartId.eq.KartIdVarious) then
          call PwdOptionsVariousCheck
        else
        endif
        go to 6000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
        go to 6000
      endif
8000  if(kcommenMax.ne.0) call iom50(0,0,fln(:ifln)//'.m50')
!     Obnova SSG uz zde, aby se neprepisovaly vlnove delky
      if(ich.eq.0) then
        call FeDeferOutput
        do i=KartIdCell,KartIdVarious
          call FePrepniListek(i-KartFirstId+1)
          call PwdOptionsRefresh(0)
        enddo
        call FePrepniListek(KartIdCell-KartFirstId+1)
        call PwdOptionsCellUpdate
        call FePrepniListek(KartIdRadiation-KartFirstId+1)
        call PwdOptionsRadiationUpdate
        call FePrepniListek(KartIdProf-KartFirstId+1)
        call PwdOptionsProfUpdate
        call FePrepniListek(KartIdAsym-KartFirstId+1)
        call PwdOptionsAsymUpdate
        call FePrepniListek(KartIdSample-KartFirstId+1)
        call PwdOptionsSampleUpdate
        call FePrepniListek(KartIdCorr-KartFirstId+1)
        call PwdOptionsCorrUpdate
        call FePrepniListek(KartIdVarious-KartFirstId+1)
        call PwdOptionsVariousUpdate
      endif
      call FeDestroyKart
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        do i=1,7
          if(i.eq.2.or.i.eq.6.or.i.eq.7)
     1      AsymPwd(i,KDatBlock)=AsymPwd(i,KDatBlock)*ToRad
        enddo
      endif
      if(Zpet) then
        if(KPhaseNew.gt.0) then
          KPhase=KPhaseNew
        else
          KDatBlock=KDatBlockNew
        endif
        go to 2100
      endif
9999  KDatBlock=KDatBlockIn
      KPhase=KPhaseIn
      if(allocated(PhaseGr)) deallocate(PhaseGr)
      return
      end
