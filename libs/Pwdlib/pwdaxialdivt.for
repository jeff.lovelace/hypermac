      subroutine PwdAxialDivT(Th2,XLen,SLen,RLen,XRad,SollI,SollD,
     1                        NBeta,N,DelSmooth,EpsStep,Wx,Wy,Wz)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Wy(*),Wx(*),Wz(*),Wh(1000),Wk(1000)
      real I2P,I2M,I2P0,I2M0
      SecTh2=1./cos(Th2*ToRad)
      TanTh2=tan(Th2*ToRad)
      Beta1=(SLen-XLen)*.5/XRad
      Beta2=(SLen+XLen)*.5/XRad
      if(SollI.gt.0) then
        pom=min(SollI*.5,Beta2)
      else
        pom=Beta2
      endif
      BetaStep=pom/float(NBeta)
      FK=2.*XRad**2*TanTh2
      Factor=abs(BetaStep)/(2.*Beta2)
      EpsFrom=-float(N/2)*EpsStep
      Beta=0.
      call SetRealArrayTo(Wy,N,0.)
      call SetRealArrayTo(Wz,N,0.)
      do IBeta=0,iabs(NBeta)
        if(SollI.gt.0.) then
          SI=PwdTriangleFunction(Beta,SollI*.5)
          if(SI.le.0.) go to 1900
        else
          SI=1.
        endif
        if(IBeta.eq.0) then
          FMult=Factor*SI
        else
          FMult=2.*Factor*SI
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.Beta1) then
          Z0P=XLen*.5+Beta*XRad*(1.+SecTh2)
        else if(Beta.ge.Beta1.and.Beta.le.Beta2) then
          Z0P=SLen*.5+Beta*XRad*SecTh2
        else
          go to 1900
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.-Beta1) then
          Z0M=-SLen*.5+Beta*XRad*SecTh2
        else if(Beta.ge.-Beta1.and.Beta.le.Beta2) then
          Z0M=-XLen*.5+Beta*XRad*(1.+SecTh2)
        else
          go to 1900
        endif
        Eps0=Beta**2*.5*tanTh2
        Eps1P=Eps0-(RLen*.5-Z0P)**2/FK
        Eps2P=Eps0-(RLen*.5-Z0M)**2/FK
        Eps1M=Eps0-(RLen*.5+Z0M)**2/FK
        Eps2M=Eps0-(RLen*.5+Z0P)**2/FK
        EpsDeg=EpsFrom
        Gamma0=Beta*SecTh2
        do i=1,N
          Eps=EpsDeg*ToRad
          if((th2-90.).le.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2T(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                            Z0P,Z0M,I2P,I2M,I2P0,I2M0,DelSmooth)
            else
              call PwdAxialDivI2T(Eps,Eps0,Eps1M,Eps2M,Eps1P,Eps2P,Rlen,
     1                            -Z0M,-Z0P,I2M,I2P,I2M0,I2P0,DelSmooth)
            endif
          else if((th2-90.).gt.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2T(-Eps,-Eps0,-Eps1M,-Eps2M,-Eps1P,
     1                            -Eps2P,RLen,-Z0M,-Z0P,I2M,I2P,I2M0,
     2                            I2P0,DelSmooth)
            else
              call PwdAxialDivI2T(-Eps,-Eps0,-Eps1P,-Eps2P,-Eps1M,
     1                            -Eps2M,RLen,Z0P,Z0M,I2P,I2M,I2P0,I2M0,
     2                            DelSmooth)
            endif
          endif
          if(SollD.gt.0.) then
            GammaD=sqrt(2.*abs(tanTh2*(Eps0-Eps)))
            GammaP=Gamma0+GammaD
            GammaM=Gamma0-GammaD
            FP=PwdTriangleFunction(GammaP,SollD*.5)
            FM=PwdTriangleFunction(GammaM,SollD*.5)
          else
            FP=1.
            FM=1.
          endif
          Wy(i)=Wy(i)+(I2P*FP+I2M*FM)*FMult
          Wz(i)=Wz(i)+(I2P0*FP+I2M0*FM)*FMult
          Wh(i)=(I2P*FP+I2M*FM)*FMult
          Wk(i)=(I2P0*FP+I2M0*FM)*FMult
          EpsDeg=EpsDeg+EpsStep
        enddo
1900    Beta=Beta+BetaStep
        pom=0.
        do i=1,N
          pom=max(pom,Wh(i))
        enddo
        if(pom.gt.0.) then
          do i=1,N
            Wh(i)=Wh(i)/pom
            Wk(i)=Wk(i)/pom
          enddo
        endif
        xomn=Wx(1)
        xomx=Wx(N)
        yomn=0.
        yomx=1.
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
        call FeXYPlot(Wx,Wk,N,NormalLine,NormalPlotMode,Green)
        call FeXYPlot(Wx,Wh,N,NormalLine,NormalPlotMode,Red)
        call FeReleaseOutput
        call FeDeferOutput
        call FeWait(.2)
      enddo
      WySum=0.
      do i=1,N
        WySum=WySum+Wy(i)
      enddo
      pom=1./WySum
      do i=1,N
        Wy(i)=Wy(i)*pom
        Wz(i)=Wz(i)*pom
      enddo
      return
      end
