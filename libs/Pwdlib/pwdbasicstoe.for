      subroutine PwdBasicStoe(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*512 Veta
      character*2   Ch2
      character*4   Ch4
      character*1 VetaP(512)
      integer FeOpenBinaryFile,FeReadBinaryFile,I4
      integer*2 I2
      equivalence (Ch2,I2),(Ch4,I4,R4),(Veta,VetaP)
      ich=0
      call CloseIfOpened(71)
      LnStoe=FeOpenBinaryFile(SourceFileRefBlock(KRefBlock)
     1                 (:idel(SourceFileRefBlock(KRefBlock))))
      if(LnStoe.le.0) go to 9999
      i=FeReadBinaryFile(LnStoe,VetaP,512)
      if(i.le.0) go to 9000
      do i=1,304
        if(ichar(Veta(i:i)).eq.0) Veta(i:i)=' '
      enddo
      RadiationRefBlock(KRefBlock)=XRayRadiation
      PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      NAlfaRefBlock(KRefBlock)=2
      k=305
      do i=1,9
        ch2=Veta(k:k+1)
        k=k+2
        if(i.eq.3) then
        else if(i.eq.4) then
          KLamRefBlock(KRefBlock)=8-i2
        endif
      enddo
      do i=1,3
        ch4=Veta(k:k+3)
        if(i.eq.1) then
          LamA1RefBlock(KRefBlock)=R4
        else if(i.eq.2) then
          LamA2RefBlock(KRefBlock)=R4
          LamRatRefBlock(KRefBlock)=.5
          LamAveRefBlock(KRefBlock)=(LamA1RefBlock(KRefBlock)+
     1                               LamRatRefBlock(KRefBlock)*
     2                               LamA1RefBlock(KRefBlock))/
     3                               (1.+LamRatRefBlock(KRefBlock))
        else if(i.eq.3) then
          pom=LamAveRefBlock(KRefBlock)*.5*R4
          if(abs(pom).lt..9999) AngleMon(KDatBlock)=asin(pom)/ToRad
        endif
        k=k+4
      enddo
      do i=1,4
        ch2=Veta(k:k+1)
        k=k+2
      enddo
      do i=1,4
        ch4=Veta(k:k+3)
        k=k+4
      enddo
      do i=1,13
        ch2=Veta(k:k+1)
        k=k+2
      enddo
      do i=1,9
        ch4=Veta(k:k+3)
        k=k+4
      enddo
      n=0
1100  n=n+1
      if(n.gt.10) stop
      i=FeReadBinaryFile(LnStoe,VetaP,512)
      do i=1,32
        if(ichar(Veta(i:i)).eq.0) Veta(i:i)=' '
      enddo
      if(Veta(3:3).ne.'-'.or.Veta(7:7).ne.'-'.or.
     1   Veta(13:13).ne.':') go to 1100
      k=33
      do i=1,4
        ch2=Veta(k:k+1)
        k=k+2
        if(i.eq.4) then
          if(i2.le.0) then
            StoeDataType=4
          else
            StoeDataType=2
          endif
        endif
      enddo
      do i=1,18
        ch4=Veta(k:k+3)
        if(i.eq.2) then
          Deg0=R4
        else if(i.eq.4) then
          DegF=R4
        else if(i.eq.6) then
          DegS=R4
        endif
        k=k+4
      enddo
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      go to 9999
9000  ich=1
9999  return
      end
