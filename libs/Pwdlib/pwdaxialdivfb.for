      function PwdAxialDivFB(Eps0,EpsMinus,EpsPlus)
      include 'fepc.cmn'
      include 'basic.cmn'
      pom1=max(Eps0-EpsMinus,0.)
      pom2=max(Eps0-EpsPlus,0.)
      pom1=sqrt(pom1)
      pom2=sqrt(pom2)
      if(pom1+pom2.gt.0.) then
        PwdAxialDivFB=2./(pom1+pom2)
      else
        PwdAxialDivFB=0.
      endif
      return
      end
