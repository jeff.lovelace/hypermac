      subroutine PwdPrfInfo(th,hcalc,hobs,hdif,hbragg,xkam)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      logical   obnova
      real xlow(5),xhigh(5),ylow(5),yhigh(5)
      character*1 zn
      character*80 ch80(5)
      character*50 jmena(5)
      save jmena,xlow,xhigh,ylow,yhigh
      fhp=PropFontWidthInPixels*.3
      ymx=max(PwdPrfInt2Y(hcalc),PwdPrfInt2Y(hobs))+12.
      if(ymx.gt.ymaxplW-1.) ymx=ymaxplW-1.
      obnova=.false.
      go to 200
      entry PwdPrfInfoObnova
      obnova=.true.
200   if(OpSystem.le.0) call FePlotMode('N')
      do 600k=1,3
        if(k.gt.1.and.obnova) go to 600
        do 500i=1,5
          if(obnova) then
            if(xlow(i).eq.-999.) go to 500
            call FeLoadImage(xlow(i),xhigh(i),ylow(i),yhigh(i),
     1                       jmena(i),0)
            go to 500
          else
            if((hbragg.eq.0.and.i.eq.5).or.
     1        (.not.KresliDifLine.and.i.eq.4).or.
     2        (kType.eq.0.and.i.eq.2)) then
              xlow(i)=-999.
              go to 500
            endif
          endif
          if(k.eq.1) then
            if(i.eq.1) write(ch80(i),'(f10.3)') th
            if(i.eq.2) write(ch80(i),'(f10.1,''(c)'')') hcalc
            if(i.eq.3) write(ch80(i),'(f10.1,''(o)'')') hobs
            if(i.eq.4) write(ch80(i),'(f10.1,''(o-c)'')') hdif
            if(i.eq.5) write(ch80(i),'(f20.3)') hbragg
            call zhusti(ch80(i))
            if(OpSystem.le.0) then
              jmena(i)='jinf'
              call CreateTmpFile(jmena(i),j,1)
            else
              write(zn,'(i1)') i
              jmena(i)='jinf'//zn
            endif
            xl=FeTxLength(ch80(i))
            if(i.eq.1) yp=yminplW+3.
            if(i.eq.2) yp=ymx-36.
            if(i.eq.3) yp=ymx-21.
            if(i.eq.4) yp=ZeroDif
            if(i.eq.5) yp=ybragg
            xlow(i)=xkam+2.
            xhigh(i)=xkam+xl+4.
            ylow(i)=yp-fhp-9.
            yhigh(i)=yp+fhp+9.
            if(xhigh(i).gt.XMaxBasWin) then
              pom=xhigh(i)-xlow(i)+21.
              xlow(i)=xlow(i)-pom
              xhigh(i)=xhigh(i)-pom
            endif
            call FeSaveImage(xlow(i),xhigh(i),ylow(i),yhigh(i),jmena(i))
          else if(k.eq.2) then
            call FeFillRectangle(xlow(i),xhigh(i),ylow(i),yhigh(i),
     1                           4,0,0,Black)
          else
            xp=xlow(i)+1.
            yp=ylow(i)+.5*(yhigh(i)-ylow(i))
            call FeOutSt(0,xp,yp,ch80(i),'L',Yellow)
          endif
500     continue
600   continue
1000  if(OpSystem.le.0) call FePlotMode('E')
      return
      end
