      function PwdD2TwoTh(D,Lambda)
      include 'fepc.cmn'
      include 'basic.cmn'
      real Lambda
      PwdD2TwoTh=2.*asin(Lambda*.5/D)/ToRad
      return
      end
