      subroutine PwdSetBackground
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension derp(*)
      save pom1,pom2,XPwdi1,XPwdi2
      do i=1,npnts
        if(YfPwd(i).eq.1) exit
      enddo
      i1=i
      do i=npnts,1,-1
        if(YfPwd(i).eq.1) exit
      enddo
      i2=i
      call PwdSetTOF(KDatBlock)
      if(DrawInD) then
        if(isTOF) then
          XPwdi1=PwdTOF2D(XPwd(i1))
          XPwdi2=PwdTOF2D(XPwd(i2))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwdi1=PwdD2TwoTh(XPwd(i2),LamPwd(1,KDatBlock))
            XPwdi2=PwdD2TwoTh(XPwd(i1),LamPwd(1,KDatBlock))
          else
            XPwdi1=PwdD2TwoTh(XPwd(i2),LamAve(KDatBlock))
            XPwdi2=PwdD2TwoTh(XPwd(i1),LamAve(KDatBlock))
          endif
        endif
      else
        XPwdi1=XPwd(i1)
        XPwdi2=XPwd(i2)
      endif
      if(KBackg(KDatBlock).le.2) then
        pom1=2./(XPwdi2-XPwdi1)
        pom2=-.5*(XPwdi2+XPwdi1)
      else if(KBackg(KDatBlock).eq.3) then
        pom1=pi/(XPwdi2-XPwdi1)
        pom2=-XPwdi1
      endif
      go to 9999
      entry PwdGetBackground(XPwdi,Bkg,Derp,DerSc)
      if(KBackg(KDatBlock).ne.4) then
        bgi=pom1*(XPwdi+pom2)
      else
        if(isTOF) then
          bgi=XPwdi/XPwdi2*Pi
        else
          bgi=XPwdi*ToRad
        endif
      endif
      if(KUseInvX(KDatBlock).eq.1) then
        pom=1./(XPwdi*ToRad)
        Bkg=BackgPwd(1,KDatBlock)*pom
        derp(1)=pom
        ip=2
      else
        Bkg=0.
        ip=1
      endif
      if(KBackg(KDatBlock).eq.1) then
        Bkg=Bkg+FLegen(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                 NBackg(KDatBlock))
      else if(KBackg(KDatBlock).eq.2) then
        Bkg=Bkg+Chebev(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                 NBackg(KDatBlock))
      else if(KBackg(KDatBlock).eq.3.or.KBackg(KDatBlock).eq.4) then
        Bkg=Bkg+CosBackg(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                   NBackg(KDatBlock))
      else
        Bkg=0.
      endif
      if(KManBackg(KDatBlock).gt.0) then
        Bkg=Bkg+PwdManualBackground(XPwdi)*Sc(2,KDatBlock)
        DerSc=PwdManualBackground(XPwdi)
      else
        DerSc=0.
      endif
9999  return
      end
