      subroutine PwdBasicXRDML(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(1)
      character*256 Veta
      character*80 t80
      character*1  t1
      integer CR,Stav
      real, allocatable :: Int(:),IntOld(:)
      logical EqIgCase,SkipScan
      data CR,LF/13,10/
      if(allocated(Int)) deallocate(Int)
      ich=0
      Nacetl=0
      Stav=0
      NPointsAll=0
      XStart=-9999.
      XEnd  =-9999.
      call CloseIfOpened(71)
      call FeFileBufferOpen(SourceFileRefBlock(KRefBlock),OpSystem)
1100  n=1
      i=0
      Veta=' '
1200  call FeFileBufferGetOneCharacter(t1,ich)
      if(ich.ne.0) then
        ich=0
        go to 5000
      endif
1250  if(t1.eq.'<') then
        i=1
        n=1
        Veta=t1
        if(Stav/10.eq.11) then
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          if(ich.eq.0) then
            if(Stav.eq.111) then
              LamA1RefBlock(NRefBlock)=xp(1)
              Nacetl=Nacetl+1
            else if(Stav.eq.112) then
              LamA2RefBlock(NRefBlock)=xp(1)
              Nacetl=Nacetl+1
            else if(Stav.eq.113) then
              LamRatRefBlock(NRefBlock)=max(xp(1),0.)
              Nacetl=Nacetl+1
            endif
          endif
          Stav=11
        else if(Stav/10.eq.1211) then
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          if(ich.eq.0) then
            if(Stav.eq.12111) then
              XStartInd=xp(1)
            else if(Stav.eq.12112) then
              XEndInd=xp(1)
            endif
          endif
          Stav=1211
        endif
      else if(t1.eq.'>'.and.i.eq.1) then
        if(n.lt.len(Veta)) n=n+1
        Veta(n:n)=t1
        if(Stav.eq.0) then
          if(LocateSubstring(Veta,'xrdMeasurement ',.false.,.true.)
     1     .eq.2) then
            Stav=1
          endif
        else if(Stav.eq.1) then
          if(LocateSubstring(Veta,'usedWavelength',.false.,.true.).eq.2)
     1      then
            Stav=11
          else if(LocateSubstring(Veta,'scan',.false.,.true.).eq.2)
     1      then
            k=LocateSubstring(Veta,'status=',.false.,.true.)
            t80=Veta(k+idel('status='):)
            k1=index(t80,'"')+1
            k2=index(t80(k1+1:),'"')
            k2=k2+k1-1
            SkipScan=EqIgCase(t80(k1:k2),'Aborted')
            Stav=12
          else if(LocateSubstring(Veta,'/xrdMeasurement',.false.,.true.)
     1            .eq.2) then
              Stav=Stav/10
              go to 1100
          else
            go to 1100
          endif
        else if(Stav.eq.11) then
          if(LocateSubstring(Veta,'kAlpha1',.false.,.true.).eq.2) then
            Stav=111
            Cislo=' '
          else if(LocateSubstring(Veta,'kAlpha2',.false.,.true.).eq.2)
     1      then
            Stav=112
            Cislo=' '
          else if(LocateSubstring(Veta,'ratioKAlpha2KAlpha1',
     1                            .false.,.true.).eq.2) then
            Stav=113
            Cislo=' '
          else if(LocateSubstring(Veta,'/usedWavelength',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
            write(Cislo,FormI15) Stav
          endif
          go to 1100
        else if(Stav.eq.12) then
          if(LocateSubstring(Veta,'dataPoints',.false.,.true.).eq.2)
     1      then
            Stav=121
          else if(LocateSubstring(Veta,'/scan',.false.,.true.).eq.2)
     1      then
            Stav=Stav/10
          endif
          go to 1100
        else if(Stav.eq.121) then
          if(LocateSubstring(Veta,'positions axis="2Theta" unit="deg"',
     1       .false.,.true.).eq.2) then
            Stav=1211
          else if(LocateSubstring(Veta,'intensities',.false.,.true.)
     1      .eq.2) then
            if(.not.allocated(Int)) then
              NPointsMax=100
              allocate(Int(NPointsMax))
              Int=0.
            endif
            NPoints=0
2000        Cislo=' '
2100        call FeFileBufferGetOneCharacter(t1,ich)
            if(t1.eq.' '.or.t1.eq.'<') then
              if(idel(Cislo).le.0) then
                go to 2000
              else
                if(index(Cislo,'.').le.0) Cislo=Cislo(:idel(Cislo))//'.'
                k=0
                call StToReal(Cislo,k,xp,1,.false.,ich)
                if(ich.eq.0.and..not.SkipScan) then
                  if(NPoints.ge.NPointsMax) then
                    allocate(IntOld(NPoints))
                    call CopyVek(Int,IntOld,NPoints)
                    deallocate(Int)
                    NPointsMax=NPointsMax+100
                    allocate(Int(NPointsMax))
                    Int=0.
                    call CopyVek(IntOld,Int,NPoints)
                    deallocate(IntOld)
                  endif
                  NPoints=NPoints+1
                  Int(NPoints)=Int(NPoints)+xp(1)
                endif
              endif
              if(t1.eq.'<') then
                if(.not.SkipScan) then
                  NPointsAll=max(NPoints,NPointsAll)
                  XStart=max(XStart,XStartInd)
                  XEnd=max(XEnd,XEndInd)
                endif
                go to 1250
              else
                go to 2000
              endif
            else
              Cislo=Cislo(:idel(Cislo))//t1
              go to 2100
            endif
          else if(LocateSubstring(Veta,'/dataPoints',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
          endif
          go to 1100
        else if(Stav.eq.1211) then
          if(LocateSubstring(Veta,'startPosition',.false.,.true.).eq.2)
     1      then
            Stav=12111
            Cislo=' '
          else if(LocateSubstring(Veta,'endPosition',.false.,.true.)
     1            .eq.2) then
            Stav=12112
            Cislo=' '
          else if(LocateSubstring(Veta,'/positions',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
          endif
          go to 1100
        endif
      else if(Stav/10.eq.11.or.Stav/10.eq.1211) then
        idl=idel(Cislo)
        if(idl.ge.len(Cislo)) idl=idl-1
        Cislo=Cislo(:idel(Cislo))//t1
      else if(ichar(t1).eq.CR.or.ichar(t1).eq.LF.or.i.eq.0) then
        go to 1200
      else
        if(n.lt.len(Veta)) n=n+1
        Veta(n:n)=t1
      endif
      go to 1200
5000  if(Nacetl.eq.3) then
        if(LamRatRefBlock(NRefBlock).le.0) then
          LamA2RefBlock(NRefBlock)=LamA1RefBlock(NRefBlock)
          LamAveRefBlock(NRefBlock)=LamA1RefBlock(NRefBlock)
          KLamRefBlock(NRefBlock)=
     1      LocateInArray(LamA1RefBlock(NRefBlock),LamA1D,7,.0001)
          NAlfaRefBlock(NRefBlock)=1
        else
          LamAveRefBlock(NRefBlock)=(LamA1RefBlock(NRefBlock)+
     1                               LamRatRefBlock(NRefBlock)*
     2                               LamA1RefBlock(NRefBlock))/
     3                              (1.+LamRatRefBlock(NRefBlock))
          KLamRefBlock(NRefBlock)=
     1      LocateInArray(LamAveRefBlock(NRefBlock),LamAveD,7,.0001)
          NAlfaRefBlock(NRefBlock)=2
        endif
      else
        ich=1
        go to 9999
      endif
      if(NPointsAll.gt.0) then
        Veta=SourceFileRefBlock(KRefBlock)
     1       (:idel(SourceFileRefBlock(KRefBlock)))//'_xy'
        call OpenFile(71,Veta,'formatted','unknown')
        Step=anint(100000.*(XEnd-XStart)/float(NPointsAll-1))/100000.
        do i=1,NPointsAll
          write(71,'(2f15.6)') XStart+float(i-1)*Step,Int(i)
        enddo
        rewind 71
      else
        ich=1
      endif
9999  if(allocated(Int)) deallocate(Int)
      return
      end
