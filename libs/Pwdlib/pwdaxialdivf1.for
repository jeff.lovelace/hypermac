      function PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if(Eps0-EpsA.gt.0..and.Eps0-EpsB.gt.0.) then
        PwdAxialDivF1=PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                (sqrt(Eps0-EpsB)-sqrt(Eps0-EpsA))
      else if(Eps0-EpsB.gt.0.) then
        PwdAxialDivF1=PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                sqrt(Eps0-EpsB)
      else
        PwdAxialDivF1=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF1=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF1
      else
        PwdAxialDivF1=0.
      endif
9999  return
      end
