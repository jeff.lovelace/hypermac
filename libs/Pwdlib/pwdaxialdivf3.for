      function PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if((Eps0-EpsA).gt.0.) then
        PwdAxialDivF3=1.+PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                   sqrt(Eps0-EpsA)
      else
        PwdAxialDivF3=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF3=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF3
      else
        PwdAxialDivF3=0.
      endif
9999  return
      end
