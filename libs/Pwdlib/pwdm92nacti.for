      subroutine PwdM92Nacti
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension xp(4),ipoint(4),length(4)
      character*80 Veta
      logical   MamCislo,EqIgCase
      equivalence (xp,ipoint)
      ln=NextLogicNumber()
      call OpenDatBlockM90(ln,KDatBlock,fln(:ifln)//'.m90')
      nn=0
      n=4
1000  read(ln,FormA,err=9000,end=1200) Veta
      if(Veta.eq.' ') go to 1000
      if(EqIgCase(Veta,'manual_background')) then
        n=2
        go to 1200
      endif
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'data')) go to 1200
      nn=nn+1
      if(nn.eq.1) then
        k=0
        MamCislo=.false.
        call SetIntArrayTo(ipoint,4,-1)
        ip=0
        do i=1,idel(Veta)+1
          if(index(Cifry(1:11),Veta(i:i)).gt.0) then
            if(.not.MamCislo) then
              MamCislo=.true.
              k=k+1
              if(k.gt.4) go to 1105
            endif
            if(Veta(i:i).eq.'.') ipoint(k)=i-ip
          else if(Veta(i:i).eq.' '.or.Veta(i:i).eq.'-') then
            if(MamCislo) then
              length(k)=i-1-ip
              if(ipoint(k).lt.0) then
                ipoint(k)=0
              else
                ipoint(k)=length(k)-ipoint(k)
              endif
              ip=i-1
              MamCislo=.false.
            endif
          endif
        enddo
        if(ipoint(4).lt.0) then
          ipoint(4)=ipoint(3)
          length(4)=length(3)
        endif
1105    format92='(f__.__,f__.__,f__.__,f__.__)'
        FormatSkipFrTo(KDatBlock)='(f__.__,1x,f__.__)'
        PrfPointsFormat='(f__.__,3e15.6,f__.__,i5,99e15.6)'
        k=3
        do i=1,4
          write(format92(k:k+1),100) length(i)
          k=k+3
          write(format92(k:k+1),100) ipoint(i)
          k=k+4
        enddo
        OrderSkipFrTo(KDatBlock)=ipoint(1)
        call zhusti(format92)
        write(FormatSkipFrTo(KDatBlock)(3:4),100) length(1)
        write(FormatSkipFrTo(KDatBlock)(6:7),100) ipoint(1)
        write(FormatSkipFrTo(KDatBlock)(13:14),100) length(1)
        write(FormatSkipFrTo(KDatBlock)(16:17),100) ipoint(1)
        call zhusti(FormatSkipFrTo(KDatBlock))
        write(PrfPointsFormat(3:4),100) length(1)
        write(PrfPointsFormat(6:7),100) ipoint(1)
        write(PrfPointsFormat(17:18),100) length(1)
        write(PrfPointsFormat(20:21),100) ipoint(1)
        call zhusti(PrfPointsFormat)
      endif
      go to 1000
1200  call CloseIfOpened(ln)
      call OpenDatBlockM90(ln,KDatBlock,fln(:ifln)//'.m90')
      if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
      allocate(XPwd(nn),YoPwd(nn),YcPwd(nn),YbPwd(nn),YsPwd(nn),
     1         YfPwd(nn),YiPwd(nn),YcPwdInd(nn,NPhase))
      NPnts=0
      NManBackg(KDatBlock)=0
      if(UseIncSpect(KDatBlock).eq.1) then
        n=4
      else
        n=3
      endif
      if(allocated(XManBackg)) deallocate(XManBackg,YManBackg)
1500  read(ln,FormA,err=9000,end=2000) Veta
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'data')) go to 2000
      if(EqIgCase(Veta,'manual_background')) then
        n=2
        if(.not.allocated(XManBackg))
     1     allocate(XManBackg(100,NDatBlock),
     2              YManBackg(100,NDatBlock))
        go to 1500
      endif
      k=0
      if(Veta.eq.' ') go to 1500
      k=0
      call StToReal(Veta,k,xp,n,.false.,ich)
      if(xp(1).le.0.) go to 1500
      if(ich.ne.0) go to 9000
      if(n.ge.3) then
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        YoPwd(Npnts)=xp(2)
        if(xp(3).le.0.) then
          YsPwd(Npnts)=sqrt(abs(xp(2)))
        else
          YsPwd(Npnts)=xp(3)
        endif
        if(n.eq.4) then
          YiPwd(Npnts)=xp(4)
        else
          YiPwd(Npnts)=0.
        endif
        go to 1500
      else
        nn=NManBackg(KDatBlock)+1
        call ReallocManBackg(nn)
        NManBackg(KDatBlock)=nn
        XManBackg(NManBackg(KDatBlock),KDatBlock)=xp(1)
        YManBackg(NManBackg(KDatBlock),KDatBlock)=xp(2)
      endif
      go to 1500
2000  call PwdMakeSkipFlags
      DegStepPwd=(XPwd(NPnts)-XPwd(1))/float(Npnts-1)
      RadStepPwd=DegStepPwd*ToRad
      IncSpectNorm=.false.
      call PwdIncSpectNorm(0)
      go to 9999
9000  call FeReadError(ln)
9900  ErrFlag=1
9999  call CloseIfOpened(ln)
      return
100   format(i2)
      end
