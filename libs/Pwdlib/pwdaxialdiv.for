      subroutine PwdAxialDiv(Th2,XLen,SLen,RLen,XRad,SollI,UseSollI,
     1                       SollD,UseSollD,
     2                       NBeta,N,DelSmooth,EpsStep,Wy)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical UseSollI,UseSollD
      dimension Wy(*)
      real I2P,I2M
      SecTh2=1./cos(Th2*ToRad)
      TanTh2=tan(Th2*ToRad)
      Beta1=(SLen-XLen)*.5/XRad
      Beta2=(SLen+XLen)*.5/XRad
      if(UseSollI) then
        pom=min(SollI*.5,Beta2)
      else
        pom=Beta2
      endif
      BetaStep=pom/float(NBeta)
      FK=2.*XRad**2*TanTh2
      Factor=abs(BetaStep)/(2.*Beta2)
      EpsFrom=-float(N/2)*EpsStep
      Beta=0.
      call SetRealArrayTo(Wy,N,0.)
      do IBeta=0,iabs(NBeta)
        if(UseSollI) then
          SI=PwdTriangleFunction(Beta,SollI*.5)
          if(SI.le.0.) go to 1900
        else
          SI=1.
        endif
        if(IBeta.eq.0) then
          FMult=Factor*SI
        else
          FMult=2.*Factor*SI
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.Beta1) then
          Z0P=XLen*.5+Beta*XRad*(1.+SecTh2)
        else if(Beta.ge.Beta1.and.Beta.le.Beta2) then
          Z0P=SLen*.5+Beta*XRad*SecTh2
        else
          go to 1900
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.-Beta1) then
          Z0M=-SLen*.5+Beta*XRad*SecTh2
        else if(Beta.ge.-Beta1.and.Beta.le.Beta2) then
          Z0M=-XLen*.5+Beta*XRad*(1.+SecTh2)
        else
          go to 1900
        endif
        Eps0=Beta**2*.5*tanTh2
        Eps1P=Eps0-(RLen*.5-Z0P)**2/FK
        Eps2P=Eps0-(RLen*.5-Z0M)**2/FK
        Eps1M=Eps0-(RLen*.5+Z0M)**2/FK
        Eps2M=Eps0-(RLen*.5+Z0P)**2/FK
        EpsDeg=EpsFrom
        Gamma0=Beta*SecTh2
        do i=1,N
          Eps=EpsDeg*ToRad
          if((th2-90.).le.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                           Z0P,Z0M,I2P,I2M,DelSmooth)
            else
              call PwdAxialDivI2(Eps,Eps0,Eps1M,Eps2M,Eps1P,Eps2P,Rlen,
     1                           -Z0M,-Z0P,I2M,I2P,DelSmooth)
            endif
          else if((th2-90.).gt.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2(-Eps,-Eps0,-Eps1M,-Eps2M,-Eps1P,-Eps2P,
     1                           RLen,-Z0M,-Z0P,I2M,I2P,DelSmooth)
            else
              call PwdAxialDivI2(-Eps,-Eps0,-Eps1P,-Eps2P,-Eps1M,-Eps2M,
     1                           RLen,Z0P,Z0M,I2P,I2M,DelSmooth)
            endif
          endif
          if(UseSollD) then
            GammaD=sqrt(2.*abs(tanTh2*(Eps0-Eps)))
            GammaP=Gamma0+GammaD
            GammaM=Gamma0-GammaD
            FP=PwdTriangleFunction(GammaP,SollD*.5)
            FM=PwdTriangleFunction(GammaM,SollD*.5)
          else
            FP=1.
            FM=1.
          endif
          Wy(i)=Wy(i)+(I2P*FP+I2M*FM)*FMult
          EpsDeg=EpsDeg+EpsStep
        enddo
1900    Beta=Beta+BetaStep
      enddo
      WySum=0.
      do i=1,N
        WySum=WySum+Wy(i)
      enddo
      if(WySum.gt.0.) then
        pom=1./WySum
        do i=1,N
          Wy(i)=Wy(i)*pom
        enddo
      endif
      return
      end
