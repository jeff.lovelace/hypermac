      subroutine PwdPrfSum(n1,n2,nb,zacni,skonci)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      logical zacni,skonci
      dimension derp(36),hh(3)
      if(zacni) then
        call PwdSetBackground
        do i=n1,n2
          if(DrawInD) then
            if(isTOF) then
              XPwdi=PwdTOF2D(XPwd(i))
            else
              if(KRefLam(KDatBlock).eq.1) then
                XPwdi=PwdD2TwoTh(XPwd(i),LamPwd(1,KDatBlock))
              else
                XPwdi=PwdD2TwoTh(XPwd(i),LamAve(KDatBlock))
              endif
            endif
          else
            XPwdi=XPwd(i)
          endif
          call PwdGetBackground(XPwdi,CalcPPArr(i),derp,DerSc)
          BkgArr(i)=CalcPPArr(i)
        enddo
      else if(skonci) then
        call CopyVek(CalcPPArr(n1),CalcPArr(n1),n2-n1+1)
        go to 3000
      endif
      if(DrawInD) then
        if(isTOF) then
          pom=PwdTOF2D(BraggArr(nb))
        else
          if(KRefLam(KDatBlock).eq.1) then
            pom=PwdD2TwoTh(BraggArr(nb),LamPwd(1,KDatBlock))
          else
            pom=PwdD2TwoTh(BraggArr(nb),LamAve(KDatBlock))
          endif
        endif
      else
        pom=BraggArr(nb)
      endif
      th=(pom+ShiftArr(nb))*torad*0.5
      KPh=KlicArr(nb)/100+1
      KPhase=KPh
      call FromIndSinthl(indArr(1,nb),hh,sinthl,sinthlq,1,0)
      call SetProfFun(indArr(1,nb),hh,th)
      do i=n1,n2
        if(DrawInD) then
          if(isTOF) then
            XPwdi=PwdTOF2D(XPwd(i))
          else
            if(KRefLam(KDatBlock).eq.1) then
              XPwdi=PwdD2TwoTh(XPwd(i),LamPwd(1,KDatBlock))
            else
              XPwdi=PwdD2TwoTh(XPwd(i),LamAve(KDatBlock))
            endif
          endif
        else
          XPwdi=XPwd(i)
        endif
        CalcPArr(i)=BkgArr(i)
        if(isTOF) then
          pom=XPwdi
          if(KUseTOFAbs(KDatBlock).gt.0) then
            Absorption=PwdTOFAbsorption(XPwdi,TOFAbsPwd(KDatBlock),
     1                                  DAbsorption)
          else
             Absorption=1.
          endif
        else
          pom=XPwdi*ToRad
          Absorption=1./PwdAbsorption(pom)
          call PwdRougness(pom,Roughness,derp)
          Absorption=Absorption*Roughness
        endif
        if(pom.ge.rdeg(1).and.pom.le.rdeg(2)) then
          prispevek=ypeakArr(nb)*prfl(pom-peak)*Absorption
          CalcPArr(i)=CalcPArr(i)+prispevek
          CalcPPArr(i)=CalcPPArr(i)+prispevek
        endif
      enddo
3000  return
      end
