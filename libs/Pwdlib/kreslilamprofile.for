      subroutine KresliLamProfile
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension Wy(-500:500,10),Wx(-500:500)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      DelLam=.00001
      StredLam=1.542
      xomn= 100.
      xomx=-100.
      yomn= 0.
      yomx=-100.
      k=NLamSpect(KDatBlock)
      do i=1,NLamSpect(KDatBlock)
        SpectRel=LamSpectRel(i,KDatBlock)
        poml=LamSpectLen(i,KDatBlock)
        SpectLife=LamSpectLife(i,KDatBlock)*.001
        do j=-500,500
          wx(j)=StredLam+float(j)*DelLam
          xomn=min(xomn,wx(j))
          xomx=max(xomx,wx(j))
            pom=1.+(2.*(StredLam-poml+float(j)*DelLam)/SpectLife)**2
            pom=2.*LamSpectRel(i,KDatBlock)/(pi*pom*SpectLife)
            wy(j,i)=pom
          yomx=max(yomx,wy(j,i))
        enddo
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      do i=1,k
        if(i.eq.1) then
          j=Blue
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Khaki
        else if(i.eq.4) then
          j=Red
        else if(i.eq.5) then
          j=Magenta
        endif
        if(i.lt.k+1) then
          call FeXYPlot(Wx(-500),Wy(-500,i),1001,NormalLine,
     1                  NormalPlotMode,j)
        else
        endif
      enddo
      call FeReleaseOutput
      call FeDeferOutput
1046  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 1046
1050  continue
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
