      subroutine PwdMakeSkipFlags
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      call PwdSetTOF(KDatBlock)
      call PwdOrderSkipRegions
      do 2200j=1,Npnts
        do i=1,NskipPwd(KDatBlock)
          if(XPwd(j).ge.SkipPwdFr(i,KDatBlock).and.
     1       XPwd(j).le.SkipPwdTo(i,KDatBlock)) then
            YfPwd(j)=0
            go to 2200
          endif
        enddo
        if(UseCutoffPwd) then
          if(isTOF) then
            if(TOFInD) then
              dpom=XPwd(j)
            else
              dpom=PwdTOF2D(XPwd(j))
            endif
          else
            dpom=PwdTwoTh2D(XPwd(j),LamA1(KDatBlock))
          endif
          if(dpom.lt.CutoffMinPwd.or.dpom.gt.CutoffMaxPwd) then
            YfPwd(j)=0
            go to 2200
          endif
        endif
        YfPwd(j)=1
2200  continue
      return
      end
