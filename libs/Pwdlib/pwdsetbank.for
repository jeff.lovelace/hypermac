      subroutine PwdSetBank(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension xp(4),ip(4)
      character*256 Radka
      character*80  Veta
      logical EqIgCase
      equivalence (ip,xp)
      ich=0
      LRALF=.false.
      LSLOG=.false.
      if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdData11BM.or.
     2   DifCode(KRefBlock).eq.IdTOFGSAS.or.
     3   (DifCode(KRefBlock).eq.IdTOFISIST.and.ISISGSS)) then
        if(DifCode(KRefBlock).eq.IdDataGSAS) then
          if(NBankActual.le.1) then
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
          else
            PolarizationRefBlock(KRefBlock)=
     1        PolarizationRefBlock(KRefBlock-1)
          endif
        else if(DifCode(KRefBlock).eq.IdData11BM) then
          RadiationRefBlock(KRefBlock)=XRayRadiation
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        else
          RadiationRefBlock(KRefBlock)=NeutronRadiation
        endif
        rewind 71
        n=0
1100    read(71,FormA,err=9100,end=9100) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1     index(Radka,'bank').ne.1) go to 1100
        n=n+1
        if(n.lt.NBankActual) go to 1100
        if(IncSpectFileOpened) then
          rewind 72
          n=0
1150      read(72,FormA,err=9100,end=9100) Veta
          call mala(Veta)
          if(Veta.eq.' '.or.Veta(1:1).eq.'#'.or.
     1       index(Veta,'bank').ne.1) go to 1150
          n=n+1
          if(n.lt.NBankActual) go to 1150
          k=5
          do i=1,6
            call kus(Veta,k,TypeBankInc)
          enddo
          if(TypeBankInc.eq.'std'.or.TypeBankInc.eq.' ') then
            FormatBankInc='(10(i2,f6.0))'
          else if(TypeBankInc.eq.'esd') then
            FormatBankInc='(10f8.0)'
          else if(TypeBankInc.eq.'alt') then
            FormatBankInc='(4(f8.0,f7.0,f5.0))'
          endif
        endif
        k=5
        call StToInt(Radka,k,ip,3,.false.,ich)
        if(ich.ne.0) go to 9100
        if(DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1     DifCode(KRefBlock).eq.IdTOFISIST) then
          j=ip(1)
          if(TOFDifcRefBlock(KRefBlock).lt.-3000.) then
            TOFDifcRefBlock(KRefBlock)=BankDifc(j)
            TOFJasonRefBlock(KRefBlock)=BankJason(j)
          endif
          if(TOFDifaRefBlock(KRefBlock).lt.-3000.)
     1       TOFDifaRefBlock(KRefBlock)=BankDifa(j)
          if(TOFZeroRefBlock(KRefBlock).lt.-3000.)
     1      TOFZeroRefBlock(KRefBlock)=BankZero(j)
          if(TOFTThRefBlock(KRefBlock).lt.-3000.)
     1      TOFTThRefBlock(KRefBlock)=BankTTh(j)
          if(TOFCERefBlock(KRefBlock).lt.-3000.)
     1      TOFCERefBlock(KRefBlock)=BankCE(j)
          if(TOFZeroERefBlock(KRefBlock).lt.-3000.)
     1      TOFZeroERefBlock(KRefBlock)=BankZeroE(j)
          if(TOFTCrossRefBlock(KRefBlock).lt.-3000.)
     1      TOFTCrossRefBlock(KRefBlock)=BankTCross(j)
          if(TOFWCrossRefBlock(KRefBlock).lt.-3000.)
     1      TOFWCrossRefBlock(KRefBlock)=BankWCross(j)
          if(TOFSigmaRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankSigma(1,j),TOFSigmaRefBlock(1,KRefBlock),3)
          if(TOFGammaRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankGamma(1,j),TOFGammaRefBlock(1,KRefBlock),3)
          if(TOFAlfbeRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankAlfBe(1,j),TOFAlfBeRefBlock(1,KRefBlock),4)
          if(TOFAlfbtRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankAlfBt(1,j),TOFAlfBtRefBlock(1,KRefBlock),4)
        endif
        call kus(Radka,k,Veta)
        if(EqIgCase(Veta,'time_map')) then
          call StToInt(Radka,k,ip,1,.false.,ich)
          NTimeMap=ip(1)
        else if(EqIgCase(Veta,'const').or.EqIgCase(Veta,'cons')) then
          kp=k
          call StToReal(Radka,k,xp,4,.false.,ich)
          if(ich.ne.0) then
            k=kp
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) then
              k=kp
              call StToReal(Radka,k,xp,2,.false.,ich)
              if(ich.ne.0) go to 9999
              JenDve=.true.
            endif
          endif
          deg0=xp(1)*.01
          degs=xp(2)*.01
        else if(EqIgCase(Veta,'ralf')) then
          call StToReal(Radka,k,TOFRALF,4,.false.,ich)
          do i=1,4
            TOFRALF(i)=TOFRALF(i)*32.
          enddo
          LRALF=.true.
        else if(EqIgCase(Veta,'slog')) then
          LSLOG=.true.
          call StToReal(Radka,k,SLogPwd,4,.false.,ich)
c         parametry zatim nepouzity
        else
          call Velka(Veta)
          call FeChybne(-1.,-1.,'BINTYP='//Veta(:idel(Veta))//
     1                  ' not yet implemented.',' ',SeriousError)
          go to 9900
        endif
        call kus(Radka,k,TypeBank)
        if(TypeBank.eq.'std'.or.TypeBank.eq.' ') then
          FormatBank='(10(i2,f6.0))'
        else if(TypeBank.eq.'esd') then
          FormatBank='(10f8.0)'
        else if(TypeBank.eq.'alt') then
          FormatBank='(4(f8.0,f7.0,f5.0))'
        else if(TypeBank.eq.'fxy') then
          LRALF=.false.
          FormatBank='*'
          JenDve=.true.
        else if(TypeBank.eq.'fxye') then
          LRALF=.false.
          FormatBank='*'
          JenDve=.false.
        else
          call FeChybne(-1.,-1.,'TYPE='//TypeBank(:idel(TypeBank))//
     1                  ' not yet implemented.',' ',SeriousError)
          go to 9900
        endif
      else if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1        (DifCode(KRefBlock).eq.IdTOFISIST.and..not.ISISGSS)) then
        rewind 71
        n=0
1200    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1200
        k=0
        call Kus(Radka,k,Cislo)
        if(Cislo.eq.'#s') then
          n=n+1
          if(n.ge.NBankActual) go to 1220
        endif
        go to 1200
1220    read(71,FormA,err=9100,end=9100) Radka
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1220
        backspace 71
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        if(DifCode(KRefBlock).eq.IdTOFISISD) then
          TOFJasonRefBlock(KRefBlock)=0
          TOFDifcRefBlock(KRefBlock)=1.
          TOFDifaRefBlock(KRefBlock)=0.
          TOFZeroRefBlock(KRefBlock)=0.
          TOFTThRefBlock(KRefBlock)=90.
          TOFCERefBlock(KRefBlock)=0.
          TOFZeroERefBlock(KRefBlock)=0.
          TOFTCrossRefBlock(KRefBlock)=0.
          TOFWCrossRefBlock(KRefBlock)=0.
        else
          TOFJasonRefBlock(KRefBlock)=BankJason(n)
          TOFDifcRefBlock(KRefBlock)=BankDifc(n)
          TOFDifaRefBlock(KRefBlock)=BankDifa(n)
          TOFZeroRefBlock(KRefBlock)=BankZero(n)
          TOFTThRefBlock(KRefBlock)=BankTTh(n)
          TOFCERefBlock(KRefBlock)=BankCE(n)
          TOFZeroERefBlock(KRefBlock)=BankZeroE(n)
          TOFTCrossRefBlock(KRefBlock)=BankTCross(n)
          TOFWCrossRefBlock(KRefBlock)=BankWCross(n)
        endif
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        rewind 71
1300    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1300
        k=0
        call Kus(Radka,k,Veta)
        if(EqIgCase(Veta,'x')) then
          call Kus(Radka,k,Veta)
          if(index(Veta,'d=').gt.0) then
            i=index(Veta,'/')
            if(i.eq.idel(Veta)) then
              call Kus(Radka,k,Veta)
            else
              Veta=Veta(i+1:)
            endif
            k=0
            call StToReal(Veta,k,TOFDifcRefBlock(KRefBlock),1,.false.,
     1                    ich)
            if(ich.ne.0) go to 9100
            TOFDifaRefBlock(KRefBlock)=0.
            TOFZeroRefBlock(KRefBlock)=0.
            TOFTThRefBlock(KRefBlock)=-3333.
            go to 1320
          endif
        endif
        go to 1300
1320    rewind 71
1330    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1330
        if(index(Radka,'waves').ne.0.and.index(Radka,'tof').ne.0) then
1340      read(71,FormA,err=9100,end=9100) Radka
          if(EqIgCase(Radka,'begin')) go to 9999
          go to 1340
        endif
        go to 1330
      else if(DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     1        DifCode(KRefBlock).eq.IdTOFFree) then
        if(TOFDifcRefBlock(KRefBlock).lt.-3000.) then
          TOFDifcRefBlock(KRefBlock)=BankDifc(1)
          TOFJasonRefBlock(KRefBlock)=BankJason(1)
        endif
        if(TOFDifaRefBlock(KRefBlock).lt.-3000.)
     1     TOFDifaRefBlock(KRefBlock)=BankDifa(1)
        if(TOFZeroRefBlock(KRefBlock).lt.-3000.)
     1    TOFZeroRefBlock(KRefBlock)=BankZero(1)
        if(TOFTThRefBlock(KRefBlock).lt.-3000.)
     1    TOFTThRefBlock(KRefBlock)=BankTTh(1)
        if(TOFCERefBlock(KRefBlock).lt.-3000.)
     1    TOFCERefBlock(KRefBlock)=BankCE(1)
        if(TOFZeroERefBlock(KRefBlock).lt.-3000.)
     1    TOFZeroERefBlock(KRefBlock)=BankZeroE(1)
        if(TOFTCrossRefBlock(KRefBlock).lt.-3000.)
     1    TOFTCrossRefBlock(KRefBlock)=BankTCross(1)
        if(TOFWCrossRefBlock(KRefBlock).lt.-3000.)
     1    TOFWCrossRefBlock(KRefBlock)=BankWCross(1)
        if(TOFSigmaRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankSigma(1,1),TOFSigmaRefBlock(1,KRefBlock),3)
        if(TOFGammaRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankGamma(1,1),TOFGammaRefBlock(1,KRefBlock),3)
        if(TOFAlfbeRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankAlfBe(1,1),TOFAlfBeRefBlock(1,KRefBlock),4)
        if(TOFAlfbtRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankAlfBt(1,1),TOFAlfBtRefBlock(1,KRefBlock),4)
      endif
      go to 9999
9100  call FeReadError(71)
9900  ich=1
9999  return
      end
