      subroutine PwdPrfFun(Type,dt,fwhm,eta,f,dfdt,dfdg,dfde)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Type
      dtq=dt**2
      if(Type.eq.2.or.Type.eq.3) then
        bunbo=(.5*fwhm)**2+dtq
        tl=fwhm/(pi2*bunbo)
        dfdgl=tl/fwhm-.5*tl/bunbo*fwhm
        dfdtl=-2.*tl/bunbo*dt
      else
        tl=0.
        dfdtl=0.
      endif
      if(Type.eq.1.or.Type.eq.3) then
        sigp=fwhm**2/5.545177
        ex=-2.77259*dtq/fwhm**2
        tg=.9394373*ExpJana(ex,ich)/fwhm
        if(dt.ne.0.) then
          dfdtg=2.*tg*ex/dt
        else
          dfdtg=0.
        endif
        dfdgg=-tg/fwhm-2.*tg*ex/fwhm
      else
        ex=0.
        tg=0.
      endif
      if(Type.eq.1) then
        f=tg
        dfde=0.
        dfdt=dfdtg
        dfdg=dfdgg
      else if(Type.eq.2) then
        f=tl
        dfde=0.
        dfdt=dfdtl
        dfdg=dfdgl
      else if(Type.eq.3) then
        f=eta*tl+(1.-eta)*tg
        dfde=tl-tg
        dfdt=eta*dfdtl+(1.-eta)*dfdtg
        dfdg=eta*dfdgl+(1.-eta)*dfdgg
      endif
      return
      end
