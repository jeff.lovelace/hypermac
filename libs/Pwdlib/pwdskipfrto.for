      subroutine PwdSkipFrTo
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/SkipFrToQuest/ nEdwFirst,ThFrTo(2),Prazdno
      dimension xp(2)
      character*80 Veta,FileName
      character*10 Label
      integer WhatHappened
      logical Prazdno,Enabled,EqIgCase
      external PwdSkipFrToRead,PwdSkipFrToWrite,FeVoid
      save /SkipFrToQuest/
      FileName=fln(:ifln)//'_skipfrto.tmp'
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).gt.-180.) then
          write(Veta,FormatSkipFrTo(KDatBlock))
     1      SkipPwdFr(i,KDatBlock),SkipPwdTo(i,KDatBlock)
          Label='skipfrto'
        else
          write(Veta,FormatSkipFrTo(KDatBlock))
     1      -SkipPwdFr(i,KDatBlock)-200.,-SkipPwdTo(i,KDatBlock)-200.
          Label='!skipfrto'
        endif
        k=0
        call WriteLabeledRecord(ln,Label,Veta,k)
      enddo
      close(ln)
      xqd=400.
      il=1
      call RepeatCommandsProlog(id,FileName,xqd,il,ilp,0)
      il=ilp+1
      tpom=5.
      Veta='fro%m'
      xpom=FeTxLengthUnder(Veta)+5.
      do i=1,2
        call FeQuestEdwMake(id,tpom,il,tpom+xpom,il,Veta,'L',60.,EdwYd,
     1                      0)
        tpom=tpom+100.
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
          Veta='%to'
        endif
      enddo
      call FeQuestButtonLabelChange(nButtRepeatEdit,'%Edit')
1300  call SetRealArrayTo(ThFrTo,2,0.)
      Prazdno=.true.
1350  nEdw=nEdwFirst
      do i=1,2
        call FeQuestRealEdwOpen(nEdw,ThFrTo(i),Prazdno,.false.)
        nEdw=nEdw+1
      enddo
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  PwdSkipFrToRead,PwdSkipFrToWrite,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventCrw) then
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.eq.0) then
        n=0
        call OpenFile(ln,FileName,'formatted','unknown')
3000    read(ln,FormA,end=3100) Veta
        k=0
        call kus(Veta,k,Label)
        if(EqIgCase(Label,'skipfrto')) then
          n=n+1
          Enabled=.true.
        else if(EqIgCase(Label,'!skipfrto')) then
          n=n+1
          Enabled=.false.
        else
          go to 3000
        endif
        call StToReal(Veta,k,xp,2,.false.,ich)
        if(ich.eq.0) then
          SkipPwdFr(n,KDatBlock)=xp(1)
          SkipPwdTo(n,KDatBlock)=xp(2)
          if(.not.Enabled) then
            SkipPwdFr(n,KDatBlock)=-SkipPwdFr(n,KDatBlock)-200.
            SkipPwdTo(n,KDatBlock)=-SkipPwdTo(n,KDatBlock)-200.
          endif
        else
          n=n-1
        endif
        go to 3000
3100    NSkipPwd(KDatBlock)=n
        call CloseIfOpened(ln)
        call DeleteFile(FileName)
      endif
9999  return
      end
