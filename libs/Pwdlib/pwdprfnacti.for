      subroutine PwdPrfNacti(ktFile)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer ihh(6)
      integer, allocatable :: intpole(:),intorder(:)
      logical ExistFile,EqIgCase
      character*80 ch80
      character*120 radka
      if(ktFile.eq.0) then
        ZPrf=ExistFile(fln(:idel(fln))//'.prf')
      else
        ZPrf=.false.
      endif
      ln=0
1000  if(ZPrf) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:idel(fln))//'.prf','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        KdeJe=0
1010    if(NDatBlock.gt.1) then
1020      read(ln,FormA,end=1030,err=9000) ch80
          k=0
          call Kus(ch80,k,Cislo)
          if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
            call Kus(ch80,k,Cislo)
            if(EqIgCase(Cislo,'begin')) then
              if(KdeJe.eq.0) then
                go to 1050
              else
                go to 1085
              endif
            endif
          endif
          go to 1020
1030      call CloseIfOpened(ln)
          ZPrf=.false.
          go to 1000
        endif
        if(KdeJe.eq.1) go to 1085
1050    read(ln,'(2i5)',end=9000,err=9000) kType,kAlpha2
        NBragg=1
        Npnts=0
1060    read(ln,FormA,end=9000,err=9000) radka
        if(NBragg.le.1) then
          call mala(Radka)
          i=index(Radka,'e')
          if(i.lt.63+(maxNDim-3)*4) then
            call FeChybne(-1.,-1.,'you are trying to use incorrect or'//
     1        ' Jana2000 "prf" file.','Please re-run the refinement '//
     2        'or le Bail decomposition.',SeriousError)
            go to 9900
          endif
        endif
        read(Radka,PrfFormat,err=9000)(ihh(i),i=1,maxNDim)
        if(ihh(1).gt.900) go to 1070
        NBragg=NBragg+1+kAlpha2
        go to 1060
1070    NBragg=NBragg+kAlpha2
        if(allocated(BraggArr))
     1    deallocate(BraggArr,ShiftArr,MultArr,ypeakArr,KlicArr,indArr)
        if(allocated(intpole)) deallocate(intpole,intorder)
        allocate(BraggArr(NBragg),ShiftArr(NBragg),MultArr(NBragg),
     1           ypeakArr(NBragg),KlicArr(NBragg),indArr(6,NBragg),
     2           intpole(NBragg),intorder(NBragg))
        nn=0
1080    read(ln,FormA,end=9000,err=9000) radka
        k=0
        call kus(radka,k,Cislo)
        call Posun(Cislo,1)
        read(Cislo,'(f15.0)') pom
        if(pom.gt.990.) go to 1082
        if(nn.eq.0.and.
     1     LocateSubstring(Radka,'e',.false.,.true.).le.0)
     2    PrfPointsFormat='(f10.3,3f10.1,f10.3,i5,99f10.1)'
        read(Radka,PrfPointsFormat,err=9000) pom
        nn=nn+1
        if(pom.lt.900.) go to 1080
1082    if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
        allocate(XPwd(nn),YoPwd(nn),YcPwd(nn),YbPwd(nn),YsPwd(nn),
     1           YfPwd(nn),YiPwd(nn),YcPwdInd(nn,NPhase))
        rewind ln
        KdeJe=1
        go to 1010
1085    read(ln,FormA,end=9000,err=9000) radka
        NBragg=1
1100    read(ln,FormA,end=9000,err=9000) radka
        read(Radka,PrfFormat,err=9000)(ihh(i),i=1,maxNDim),
     1        MultArr(NBragg),KPh,(BraggArr(i),ShiftArr(i),pom,
     2        ypeakArr(i),i=NBragg,NBragg+kAlpha2)
        if(ihh(1).gt.900) go to 1300
        do i=NBragg,NBragg+kAlpha2
          intpole(i)=nint(BraggArr(i)*10000.)
          call CopyVekI(ihh,indArr(1,i),maxNDim)
          BraggArr(i)=BraggArr(i)-ShiftArr(i)
        enddo
        k=(KPh-1)*100
        if(kAlpha2.gt.0) then
          KlicArr(NBragg)=k
          KlicArr(NBragg+1)=k+1
          MultArr(NBragg+1)=MultArr(NBragg)
        else
          KlicArr(NBragg)=k+2
        endif
        NBragg=NBragg+1+kAlpha2
        go to 1100
1300    NBragg=NBragg-1
        call indexx(NBragg,intpole,intorder)
        do 1500i=1,NBragg
          if(i.eq.intorder(i)) go to 1500
          io=intorder(i)
          pom=BraggArr(i)
          BraggArr(i)=BraggArr(io)
          BraggArr(io)=pom
          pom=MultArr(i)
          MultArr(i)=MultArr(io)
          MultArr(io)=pom
          pom=ShiftArr(i)
          ShiftArr(i)=ShiftArr(io)
          ShiftArr(io)=pom
          pom=ypeakArr(i)
          ypeakArr(i)=ypeakArr(io)
          ypeakArr(io)=pom
          ipom=KlicArr(i)
          KlicArr(i)=KlicArr(io)
          KlicArr(io)=ipom
          do j=1,NDim(KPhase)
            ipom=indArr(j,i)
            indArr(j,i)=indArr(j,io)
            indArr(j,io)=ipom
          enddo
          do j=i+1,NBragg
            if(intorder(j).eq.i) intorder(j)=io
          enddo
1500    continue
2000    read(ln,'(a)',end=8000) radka
        k=0
        call kus(radka,k,Cislo)
        call Posun(Cislo,1)
        read(Cislo,'(f15.0)') pom
        if(pom.gt.990.) go to 8000
        read(radka,PrfPointsFormat,err=9000,end=9000) XPwdP,YoPwdP,
     1    YcPwdP,YsPwdP,pom,i,YiPwdP
        Npnts=Npnts+1
        XPwd(Npnts)=XPwdP
        YoPwd(Npnts)=YoPwdP
        YsPwd(Npnts)=YsPwdP
        YcPwd(Npnts)=YcPwdP
        YiPwd(Npnts)=YiPwdP
        if(kType.eq.0) YcPwd(Npnts)=abs(YoPwd(Npnts))
        YcPwd(Npnts)=max(0.,YcPwd(Npnts))
        YfPwd(NPnts)=1
        go to 2000
      else
        kType=0
        NBragg=0
        call PwdM92Nacti
        if(ErrFlag.ne.0) go to 9999
        do i=1,Npnts
          YcPwd(i)=abs(YoPwd(i))
        enddo
        go to 8100
      endif
8000  call PwdMakeSkipFlags
8100  if(Npnts.le.0) then
        call FeChybne(-1.,-1.,
     1    'The file doesn''t contain profile data.',' ',SeriousError)
        ErrFlag=0
      endif
      go to 9990
9000  call FeReadError(ln)
9900  ErrFlag=1
9990  if(ln.gt.0) call CloseIfOpened(ln)
9999  if(allocated(intpole)) deallocate(intpole,intorder)
      return
      end
