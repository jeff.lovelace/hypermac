      subroutine PwdSaveBackground
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      dimension derp(36)
      character*256 FileName
      logical   FeYesNoHeader,ExistFile
      FileName=fln(:ifln)//'.bac'
      ln=0
1000  call FeFileManager('Select file where the background is to be '//
     1                   'saved',FileName,'*.bac',0,.true.,ich)
      if(ich.ne.0) go to 9999
      if(ExistFile(FileName)) then
        call FeCutName(FileName,TextInfo(1),len(TextInfo(1))-35,
     1                 CutTextFromLeft)
        NInfo=1
        TextInfo(1)='The file "'//TextInfo(1)(:idel(TextInfo(1)))//
     1              '" already exists'
        if(.not.FeYesNoHeader(-1.,-1.,'Do you want to rewrite it?',1))
     1    go to 1000
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call PwdM92Nacti
      if(ErrFlag.ne.0) go to 9999
      call PwdSetBackground
      do i=1,Npnts
        call PwdGetBackground(XPwd(i),Bkg,derp,DerSc)
        write(ln,Format92) XPwd(i),Bkg
      enddo
9999  call CloseIfOpened(ln)
      return
      end
