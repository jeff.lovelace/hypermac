      subroutine PwdX2D(X,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension X(n)
      do i=1,n
        if(isTOF) then
          X(i)=PwdTOF2D(X(i))
        else
          if(KRefLam(KDatBlock).eq.1) then
            X(i)=PwdTwoTh2D(X(i),LamPwd(1,KDatBlock))
          else
            X(i)=PwdTwoTh2D(X(i),LamAve(KDatBlock))
          endif
        endif
      enddo
      return
      end
