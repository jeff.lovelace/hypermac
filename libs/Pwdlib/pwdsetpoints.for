      subroutine PwdSetPoints(Show,StartTimeP)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ypeakNew(:),hh(3)
      integer StartTimeP,FeGetSystemTime
      real Inorm(:),LpCorrection,LeBailPrfArrPom(:),LpArg
      logical BylUvnitr,Points,Show
      allocatable LeBailPrfArrPom,ypeakNew,INorm
      save scp,nalloc
      Points=.true.
      do i=1,npnts
        if(YfPwd(i).eq.0) then
          YbPwd(i)=0.
          YcPwd(i)=YoPwd(i)
          YcPwdInd(i,1:NPhase)=0.
          YcPwdInd(i,1)=YoPwd(i)
        else
          YcPwd(i)=YbPwd(i)
          YcPwdInd(i,1:NPhase)=0.
        endif
      enddo
      if(.not.LeBailFast) then
        nalloc=50000
        allocate(LeBailPrfArr(nalloc))
        TextInfo(1)='Setup of profile points - '//
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
        TextInfo(3)=' '
      endif
      go to 500
      entry PwdSetIobs
      Points=.false.
500   allocate(ypeakNew(LeBailNBragg),Inorm(LeBailNBragg))
      n=1
      if(isTOF) then
        jfr=npnts
        jto=1
        jst=-1
      else
        jfr=1
        jto=npnts
        jst=1
      endif
      nn=0
      ip=0
      do 1000i=1,LeBailNBragg
        peak=BraggArr(i)
        ypeak=ypeakArr(i)
        BylUvnitr=.false.
        if(.not.Points) then
          ypeakNew(i)=0.
          Inorm(i)=0.
        endif
        KPhase=KPhArr(i)
        if(LeBailFast) then
          rdeg1=RDeg1Arr(i)
          rdeg2=RDeg2Arr(i)
          CutCoef1=1.+1./PCutOff(KPhase,KDatBlock)
          CutCoef2=1.-1./PCutOff(KPhase,KDatBlock)
        else
          nn=nn+1
          write(TextInfo(2),'(''Bragg peak#'',i5)') i
          if(Show.and.ip.ne.0) then
            if(mod(nn,100).eq.0) call FeMsgShow(3,-1.,300.,.false.)
          else
            if(.not.Show) Show=FeGetSystemTime()-StartTimeP.gt.5000
            if(Show) call FeMsgShow(3,-1.,300.,.true.)
            ip=1
          endif
          if(isTOF.or.isED) then
            th=peak
          else
            th=peak*.5
          endif
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          NRefPwd=(i-1)/NAlfa(KDatBlock)+1
          NLamPwd=mod(i-1,NAlfa(KDatBlock))+1
          call SetProfFun(indArr(1,i),hh,th)
          ShiftArr(i)=shift
          FWHMArr(i)=FWHM
          rdeg1=rdeg(1)
          rdeg2=rdeg(2)
          RDeg1Arr(i)=rdeg1
          RDeg2Arr(i)=rdeg2
          CutCoef1=1.+1./PCutOff(KPhase,KDatBlock)
          CutCoef2=1.-1./PCutOff(KPhase,KDatBlock)
          CutCoef1Arr(i)=CutCoef1
          CutCoef2Arr(i)=CutCoef2
          call SetPref(indArr(1,i),pref)
          if(isTOF) then
            LpArg=PwdTOF2D(th)
          else if(isED) then
            LpArg=1.
          else
            LpArg=th
          endif
          pref=pref*MultArr(i)/LpCorrection(LpArg)
        endif
        rdegc1=(rdeg1*CutCoef1+rdeg2*CutCoef2)*.5
        rdegc2=(rdeg2*CutCoef1+rdeg1*CutCoef2)*.5
        do j=jfr,jto,jst
          if(YfPwd(j).eq.0) cycle
          if(IsTOF.or.isED) then
            pom=XPwd(j)
          else
            pom=XPwd(j)*ToRad
          endif
          if(isTOF) then
            if(pom.gt.rdeg2) then
              jfr=j
              cycle
            else if(pom.lt.rdeg1) then
              go to 900
            endif
          else
            if(pom.lt.rdeg1) then
              jfr=j
              cycle
            else if(pom.gt.rdeg2) then
              go to 900
            endif
          endif
          if(.not.BylUvnitr) then
            if(isTOF) then
              if(pom.lt.rdegc1) then
                ypeakNew(i)=0.
                go to 1000
              endif
            else
              if(pom.gt.rdegc2) then
                ypeakNew(i)=0.
                go to 1000
              endif
            endif
            BylUvnitr=pom.gt.rdegc1.and.pom.lt.rdegc2
          endif
          if(LeBailFast) then
            pprofile=LeBailPrfArr(n)
            n=n+1
          else
            if(isTOF) then
              if(KUseTOFAbs(KDatBlock).gt.0) then
                Absorption=PwdTOFAbsorption(pom,TOFAbsPwd(KDatBlock),
     1                                      DAbsorption)
              else
                Absorption=1.
              endif
            else if(isED) then
              Absorption=1.
            else
              Absorption=1./PwdAbsorption(pom)
            endif
            pprofile=prfl(pom-peak)*pref*Absorption
            LeBailPrfArr(n)=pprofile
            if(n.ge.nalloc) then
              allocate(LeBailPrfArrPom(nalloc))
              call CopyVek(LeBailPrfArr,LeBailPrfArrPom,nalloc)
              nallocp=nalloc
              nalloc =nalloc+50000
              deallocate(LeBailPrfArr)
              allocate(LeBailPrfArr(nalloc))
              call CopyVek(LeBailPrfArrPom,LeBailPrfArr,nallocp)
              deallocate(LeBailPrfArrPom)
            endif
            n=n+1
          endif
          if(Points) then
            YcPwd(j)=YcPwd(j)+ypeak*pprofile
            YcPwdInd(j,KPhase)=YcPwdInd(j,KPhase)+ypeak*pprofile
          else
            yical=YcPwd(j)-YbPwd(j)
            yiobs=YoPwd(j)-YbPwd(j)
            if(yical.lt.1.e-15.or.yiobs.lt.1.e-15) then
              zlomek=0.
            else
              zlomek=ypeak*yiobs/yical
            endif
            if(KWleBail(KDatBlock).eq.1) then
              wt=pprofile**2
            else
              wt=pprofile
            endif
            ypeakNew(i)=ypeakNew(i)+wt*zlomek
            Inorm(i)=Inorm(i)+wt
          endif
        enddo
900     if(isTOF) then
          if((pom.gt.rdegc1.or..not.BylUvnitr).and..not.points) then
            ypeakNew(i)=0.
            INorm(i)=0.
          endif
        else
          if((pom.lt.rdegc1.or..not.BylUvnitr).and..not.points) then
            ypeakNew(i)=0.
            INorm(i)=0.
          endif
        endif
1000  continue
      if(Points) then
        RFNom=0.
        RFDen=0.
        do j=1,npnts
          if(YfPwd(j).eq.0) cycle
          YoPwdj=YoPwd(j)
          YsPwdj=YsPwd(j)
          if(YsPwdj.le.0.) then
            if(YoPwdj.gt.0.) then
              YsPwdj=sqrt(YoPwdj)
            else
              cycle
            endif
          endif
          wt=1./YsPwdj**2
          yical=max(YcPwd(j)-YbPwd(j),0.)
          yiobs=max(YoPwd(j)-YbPwd(j),0.)
          RFNom=RFNom+wt*yiobs*yical
          RFDen=RFDen+wt*yical**2
        enddo
        if(RFDen.ne.0.) then
          scp=RFNom/RFDen
        else
          scp=1.
        endif
        RFNom=0.
        RFDen=0.
        RFNomObs=0.
        RFDenObs=0.
        do j=1,npnts
          if(YfPwd(j).eq.0) cycle
          YcPwd(j)=(YcPwd(j)-YbPwd(j))*scp+YbPwd(j)
          RFNom=RFNom+abs(YcPwd(j)-YoPwd(j))
          RFDen=RFDen+YoPwd(j)
          if(YoPwd(j)-YbPwd(j).gt.3.*YsPwd(j)) then
            RFNomObs=RFNomObs+abs(YcPwd(j)-YoPwd(j))
            RFDenObs=RFDenObs+YoPwd(j)
          endif
        enddo
        LeBailRF=RFNom/RFDen*100.
        if(RFDenObs.ne.0.) then
          LeBailRFObs=RFNomObs/RFDenObs*100.
        else
          LeBailRFObs=99.99
        endif
        do i=1,LeBailNBragg
          ypeakArr(i)=ypeakArr(i)*scp
        enddo
      else
        LeBailLastRef=0
        do i=1,LeBailNBragg
          if(Inorm(i).gt.0.) then
            ypeakArr(i)=ypeakNew(i)/Inorm(i)
            LeBailLastRef=i
          else
            ypeakArr(i)=0.
          endif
        enddo
        if(NPhase.gt.1.and.KKLeBail(KDatBlock).eq.1) then
          do KPh=1,NPhase
            if((NAtIndLenAll(KPh).le.0.and.NAtPosLenAll(KPh).le.0).or.
     1         KPhaseSolve.eq.KPh) cycle
            Suma1=0.
            Suma2=0.
            do i=1,LeBailNBragg,NAlfa(KDatBlock)
              if(KPhArr(i).ne.KPh) cycle
              Suma1=Suma1+ypeakArr(i)
              Suma2=Suma2+ICalcArr(i)
            enddo
            Suma1=Suma1/Suma2
            do i=1,LeBailNBragg,NAlfa(KDatBlock)
              if(KPhArr(i).ne.KPh) cycle
              ypeakArr(i)=ICalcArr(i)*Suma1
            enddo
          enddo
        endif
        if(NAlfa(KDatBlock).gt.1) then
          do i=1,LeBailNBragg,Nalfa(KDatBlock)
            ypeakArr(i)=(ypeakArr(i)+ypeakArr(i+1))/
     1                  (1.+LamRat(KDatBlock))
            ypeakArr(i+1)=ypeakArr(i)*LamRat(KDatBlock)
          enddo
        endif
      endif
      if(allocated(ypeakNew)) deallocate(ypeakNew,Inorm)
9999  return
      end
