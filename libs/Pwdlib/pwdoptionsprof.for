      subroutine PwdOptionsProf
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*21 :: StBrDirection = '%Broadening direction'
      dimension StPwdDef(:),StPwdOld(35)
      integer EdwStateQuest,CrwStateQuest
      external NToStrainString,NToStrainString4
      allocatable StPwdDef
      save nEdwCut,nEdwXe,nEdwYe,nEdwZeta,StPwdOld,nCrwXe,nCrwYe,
     1     nCrwZeta,nCrwTypeFirst,nCrwTypeLast,NLorentzW,NGaussW,
     2     KProfPwdNew
      save /PwdOptionsC/
      entry PwdOptionsProfMake(id)
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        if(NDimI(KPhase).eq.1) then
          n=35
        else
          n=15+NDimI(KPhase)
        endif
        call CopyVek(StPwd(1,KPhase,KDatBlock),StPwdOld,n)
      else
        call SetRealArrayTo(StPwdOld,35,.01)
      endif
      il=1
      xpom=5.
      call FeQuestLblMake(id,xpom,il,'Peak-shape function','L','B')
      if(.not.isED) then
        tpom=xpom+CrwgXd+10.
        do i=1,4
          il=il+1
          if(i.eq.1) then
            Veta='%Gaussian'
          else if(i.eq.2) then
            Veta='%Lorentzian'
          else if(i.eq.3) then
            Veta='Pseudo-%Voigt'
          else if(i.eq.4) then
            Veta='%Modified Lorentzian'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) nCrwTypeFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,
     1                        i.eq.KProfPwd(KPhase,KDatBlock))
        enddo
        nCrwTypeLast=CrwLastMade
      else
        il=il+3
        tpom=35.
        call FeQuestLblMake(id,tpom,il,'Gaussian =>','L','N')
        KProfPwd(KPhase,KDatBlock)=1
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        KProfPwd(KPh,KDatBlock)=1
          enddo
        endif
        nCrwTypeFirst=0
        nCrwTypeLast=0
      endif
      KProfPwdNew=KProfPwd(KPhase,KDatBlock)
      il=2
      Veta='%Cutoff'
      tpom=xpoma(2)-FeTxLengthUnder(Veta)-10.
      dpom=40.
      call FeQuestEdwMake(id,tpom,il,xpoma(2),il,Veta,'L',dpom,EdwYd,0)
      nEdwCut=EdwLastMade
      call FeQuestRealEdwOpen(nEdwCut,PCutOff(KPhase,KDatBlock),.false.,
     1                        .false.)
      tpom=xpoma(2)+dpom+5.
      call FeQuestLblMake(id,tpom,il,'*FWHM','L','N')
      il=il+1
      if(isTOF.or.isED) then
        NGaussW=3
      else
        NGaussW=4
      endif
      j=IGaussPwd+IZdvihProf
      do i=1,NGaussW
        Veta=' '
        call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                    GaussPwd(i,KPhase,KDatBlock),kiPwd(j),.false.)
        if(isTOF) then
          Veta=lGaussPwdTOF(i)(:idel(lGaussPwdTOF(i)))
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          Veta=lGaussPwdF(i)
        else
          Veta=lGaussPwd(i)(:idel(lGaussPwd(i)))
        endif
        call FeQuestEdwLabelChange(nEdw,Veta)
        if(i.eq.1) then
          nEdwGauss=nEdw
          nCrwGauss=nCrw
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     (i.eq.2.and.(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.
     3     KPartBroad(KPhase,KDatBlock).le.0).and..not.isTOF).or.
     4     (i.eq.4.and.(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.
     5      KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).and.
     6      .not.isTOF)) then
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
        endif
        j=j+1
        il=il+1
      enddo
      j=ILorentzPwd+IZdvihProf
      il=3
      if(isTOF) then
        NLorentzW=5
        nCrwAniPBroad=0
      else
        NLorentzW=4
      endif
      do i=1,NLorentzW
        Veta=' '
        call FeMakeParEdwCrw(id,tpoma(3),xpoma(3),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                       LorentzPwd(i,KPhase,KDatBlock),kiPwd(j),
     2                       .false.)
        if(isTOF) then
          Veta=lLorentzPwdTOF(i)(:idel(lLorentzPwdTOF(i)))
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.i.lt.5)
     1    then
          Veta=lLorentzPwdF(i)
        else
          Veta=lLorentzPwd(i)(:idel(lLorentzPwd(i)))
        endif
        call FeQuestEdwLabelChange(nEdw,Veta)
        if(i.eq.1) then
          nEdwLorentz=nEdw
          nCrwLorentz=nCrw
        endif
        if(i.eq.2) then
          nEdwXe=nEdw
          nCrwXe=nCrw
        else if(i.eq.4) then
          nEdwYe=nEdw
          nCrwYe=nCrw
        endif
        if(isTOF) then
          if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1       (KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial.and.i.gt.3))
     2      then
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        else
          if((i.eq.2.and.KPartBroad(KPhase,KDatBlock).le.0).or.
     1       (i.eq.4.and.
     2        KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).or.
     3        KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        endif
        j=j+1
        il=il+1
      enddo
      if(isTOF) then
        ilp=il-2
      else
        ilp=il
      endif
      Veta=lZetaPwd
      call FeMakeParEdwCrw(id,tpoma(3),xpoma(3),ilp,Veta,nEdw,nCrw)
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1   KProfPwdNew.eq.IdPwdProfVoigt)
     2  call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     3                       ZetaPwd(KPhase,KDatBlock),
     3                       kiPwd(IZetaPwd+IZdvihProf),.false.)
      il=il+1
      nEdwZeta=nEdw
      nCrwZeta=nCrw
      tpom=5.
      if(.not.isTOF) then
        xpom=FeTxLength('Anisotropic particle broadening')+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      'Anisotropic particle broadening','L',CrwXd,
     2                      CrwYd,1,0)
        nCrwAniPBroad=CrwLastMade
        call FeQuestCrwOpen(nCrwAniPBroad,
     1                      KPartBroad(KPhase,KDatBlock).gt.0)
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss)
     1    call FeQuestCrwDisable(nCrwAniPBroad)
        il=il+1
      endif
      call FeQuestLblMake(id,tpom,il,
     1                    'Anisotropic strain broadening','L','B')
      nLblStrain=LblLastMade
      if(isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1  call FeQuestLblOff(nLblStrain)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='%None'
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.and.
     1   KStrain(KPhase,KDatBlock).eq.1) then
        KStrain(KPhase,KDatBlock)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        KStrain(KPhase,KDatBlock)=0
          enddo
        endif
      endif
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) then
          nCrwStrain=CrwLastMade
          Veta='%Axial method'
        else if(i.eq.2) then
          Veta='%Tensor method'
        endif
        call FeQuestCrwOpen(CrwLastMade,
     1                      i-1.eq.KStrain(KPhase,KDatBlock))
        if((isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental).or.
     1     (KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.and.i.eq.2))
     2    call FeQuestCrwDisable(CrwLastMade)
      enddo
      tpom=tpom+FeTxLengthUnder(Veta)+20.
      Veta='%Edit tensor parameters'
      xpom=FeTxLength(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButEditTensor=ButtonLastMade
      call FeQuestButtonOpen(nButEditTensor,ButtonOff)
      if(KStrain(KPhase,KDatBlock).ne.IdPwdStrainTensor)
     1  call FeQuestButtonDisable(nButEditTensor)
      if(isTOF) then
        il=il-3
      else
        il=il-4
      endif
      dpom=50.
      xpom=xpoma(3)
      tpom=xpom-FeTxLengthUnder(StBrDirection)-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,StBrDirection,'L',
     1                    dpom,EdwYd,0)
      nEdwDirBroad=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwDirBroad,
     1  DirBroad(1,KPhase,KDatBlock),3,.false.,.false.)
      if((KPartBroad(KPhase,KDatBlock).le.0.and.
     1    KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).or.isTOF)
     2   call FeQuestEdwDisable(nEdwDirBroad)
      go to 9999
      entry PwdOptionsProfCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwTypeFirst.and.CheckNumber.le.nCrwTypeLast)
     2     then
        KProfPwdNew=CheckNumber-nCrwTypeFirst+1
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAniPBroad)
     1  then
        if(KPartBroad(KPhase,KDatBlock).le.0) then
          KPartBroad(KPhase,KDatBlock)=1
        else
          KPartBroad(KPhase,KDatBlock)=0
        endif
        j=ILorentzPwd+IZdvihProf+1
        LorentzPwd(2,KPhase,KDatBlock)=0.
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        LorentzPwd(2,KPh,KDatBlock)=0.
          enddo
        endif
        kiPwd(j)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
          enddo
        endif
        if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+1
          GaussPwd(2,KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) GaussPwd(2,KPh,KDatBlock)=0.
            enddo
          endif
          kiPwd(j)=0
        endif
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwStrain.and.
     1        CheckNumber.le.nCrwStrain+2) then
        KStrain(KPhase,KDatBlock)=CheckNumber-nCrwStrain
        j=ILorentzPwd+IZdvihProf+3
        LorentzPwd(4,KPhase,KDatBlock)=0.
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        LorentzPwd(4,KPh,KDatBlock)=0.
          enddo
        endif
        kiPwd(j)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
          enddo
        endif
        if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+3
          GaussPwd(4,KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) GaussPwd(4,KPh,KDatBlock)=0.
            enddo
          endif
          kiPwd(j)=0
        endif
        j=IZetaPwd+IZdvihProf
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
          kiPwd(j)=0
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0)
     2          kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
            enddo
          endif
          ZetaPwd(KPhase,KDatBlock)=.5
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) ZetaPwd(KPh,KDatBlock)=.5
            enddo
          endif
        else
          kiPwd(j)=0
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0)
     2          kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
            enddo
          endif
          ZetaPwd(KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) ZetaPwd(KPh,KDatBlock)=0.
            enddo
          endif
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButEditTensor) then
        if(NDimI(KPhase).eq.1) then
          n=35
        else
          n=15+NDimI(KPhase)
        endif
        call CopyVek(StPwdOld,StPwd(1,KPhase,KDatBlock),n)
        allocate(StPwdDef(35))
        StPwdDef=.1
        call SetRealArrayTo(StPwdDef,35,.01)
        if(NDimI(KPhase).eq.1) then
          call FeEditParameterField(StPwd (1,KPhase,KDatBlock),
     1                              StPwdS(1,KPhase,KDatBlock),StPwdDef,
     2                              kiPwd(IStPwd+IZdvihProf),n,lStPwd,
     3                              xqdp,'Edit strain parameters',tpoma,
     4                              xpoma,NToStrainString4)
        else
          call FeEditParameterField(StPwd(1,KPhase,KDatBlock),
     1                              StPwdS(1,KPhase,KDatBlock),StPwdDef,
     2                              kiPwd(IStPwd+IZdvihProf),n,lStPwd,
     3                              xqdp,'Edit strain parameters',tpoma,
     4                              xpoma,NToStrainString)
        endif
        call CopyVek(StPwd(1,KPhase,KDatBlock),StPwdOld,n)
        deallocate(StPwdDef)
        go to 9999
      endif
2500  j=ILorentzPwd+IZdvihProf
      nEdw=nEdwLorentz
      nCrw=nCrwLorentz
      if(KProfPwdNew.eq.IdPwdProfLorentz.or.
     1   KProfPwdNew.eq.IdPwdProfModLorentz.or.
     2   KProfPwdNew.eq.IdPwdProfVoigt) then
        if(EdwStateQuest(nEdw).ne.EdwOpened) then
          do i=1,NLorentzW
            if(isTOF.or.mod(i,2).ne.0) then
              call FeQuestRealEdwOpen(nEdw,
     1          LorentzPwd(i,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
        endif
        if(nCrwAniPBroad.gt.0) then
          if(CrwStateQuest(nCrwAniPBroad).eq.CrwDisabled)
     1      call FeQuestCrwOpen(nCrwAniPBroad,.false.)
        endif
        if(CrwStateQuest(nCrwStrain+1).eq.CrwDisabled)
     1    call FeQuestCrwOpen(nCrwStrain+1,.false.)
        j=ILorentzPwd+IZdvihProf
        if(.not.isTOF) then
          j=j+1
          if(KPartBroad(KPhase,KDatBlock).gt.0) then
            if(EdwStateQuest(nEdwXe).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdwXe,
     1          LorentzPwd(2,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrwXe,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdwXe)
            call FeQuestCrwDisable(nCrwXe)
          endif
        endif
        if(KStrain(KPhase,KDatBlock).ne.IdPwdStrainTensor.or.
     1     KProfPwdNew.ne.IdPwdProfVoigt) then
          if(EdwStateQuest(nEdwZeta).eq.EdwOpened) then
            call FeQuestEdwDisable(nEdwZeta)
            call FeQuestCrwDisable(nCrwZeta)
          endif
        endif
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
          if(isTOF) then
            nEdw=nEdwLorentz+3
            nCrw=nCrwLorentz+3
            j=ILorentzPwd+IZdvihProf
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              j=j+3
              do i=4,5
                call FeQuestRealEdwOpen(nEdw,
     1            LorentzPwd(i,KPhase,KDatBlock),.false.,.false.)
                call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
                j=j+1
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
            endif
          else
            j=j+2
            if(EdwStateQuest(nEdwYe).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdwYe,
     1          LorentzPwd(4,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrwYe,kiPwd(j).ne.0)
            endif
          endif
        else
          if(isTOF) then
            nEdw=nEdwLorentz+3
            nCrw=nCrwLorentz+3
            do i=4,5
              call FeQuestEdwDisable(nEdw)
              call FeQuestCrwDisable(nCrw)
              nEdw=nEdw+1
              nCrw=nCrw+1
            enddo
          else
            j=j+2
            call FeQuestEdwDisable(nEdwYe)
            call FeQuestCrwDisable(nCrwYe)
          endif
        endif
        j=j+1
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1     KProfPwdNew.eq.IdPwdProfVoigt) then
          if(EdwStateQuest(nEdwZeta).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwZeta,
     1        ZetaPwd(KPhase,KDatBlock),.false.,.false.)
            call FeQuestCrwOpen(nCrwZeta,kiPwd(j).ne.0)
          endif
        endif
        if((.not.isTOF.and.KPartBroad(KPhase,KDatBlock).gt.0).or.
     1     KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
          if(EdwStateQuest(nEdwDirBroad).eq.EdwDisabled) then
            call FeQuestRealAEdwOpen(nEdwDirBroad,
     1        DirBroad(1,KPhase,KDatBlock),3,.false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwDirBroad)
        endif
      else
        nEdw=nEdwLorentz
        nCrw=nCrwLorentz
        do i=1,NLorentzW
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        if(CrwStateQuest(nCrwAniPBroad).ne.CrwDisabled) then
          call FeQuestCrwDisable(nCrwAniPBroad)
          call FeQuestEdwDisable(nEdwDirBroad)
          KPartBroad(KPhase,KDatBlock)=0
          call FeQuestCrwDisable(nCrwStrain+1)
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
            call FeQuestCrwOn(nCrwStrain)
            KStrain(KPhase,KDatBlock)=IdPwdStrainNone
            if(NPhase.gt.1) then
              do KPh=1,NPhase
                if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1             PhaseGr(KPhase).gt.0)
     2            KStrain(KPh,KDatBlock)=IdPwdStrainNone
              enddo
            endif
          endif
        endif
        if(EdwStateQuest(nEdwZeta).eq.EdwOpened) then
          call FeQuestEdwDisable(nEdwZeta)
          call FeQuestCrwDisable(nCrwZeta)
        endif
      endif
      j=IGaussPwd+IZdvihProf
      nEdw=nEdwGauss
      nCrw=nCrwGauss
      if(KProfPwdNew.eq.IdPwdProfGauss.or.
     1   KProfPwdNew.eq.IdPwdProfVoigt) then
        if(EdwStateQuest(nEdw).ne.EdwOpened) then
          do i=1,NGaussW
            if(isTOF.or.mod(i,2).ne.0) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(i,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
        endif
        if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+1
          nEdw=nEdwGauss+1
          nCrw=nCrwGauss+1
          if(KPartBroad(KPhase,KDatBlock).gt.0) then
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(2,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
          j=j+2
          nEdw=nEdwGauss+3
          nCrw=nCrwGauss+3
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(4,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        endif
      else
        do i=1,NGaussW
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
      endif
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        call FeQuestButtonOpen(nButEditTensor,ButtonOff)
      else
        call FeQuestButtonDisable(nButEditTensor)
      endif
      KProfPwd(KPhase,KDatBlock)=KProfPwdNew
      if(NPhase.gt.1) then
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      KProfPwd(KPh,KDatBlock)=KProfPwdNew
        enddo
      endif
      go to 9999
      entry PwdOptionsProfUpdate
      call FeQuestRealFromEdw(nEdwCut,PCutOff(KPhase,KDatBlock))
      if(NPhase.gt.1) then
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      PCutOff(KPh,KDatBlock)=PCutOff(KPhase,KDatBlock)
        enddo
      endif
      call FeUpdateParamAndKeys(nEdwGauss,nCrwGauss,
     1  GaussPwd(1,KPhase,KDatBlock),kiPwd(IGaussPwd+IZdvihProf),
     2  NGaussW)
      if(NPhase.gt.1) then
        j=IGaussPwd+IZdvihProf
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      then
            GaussPwd(1:NGaussW,KPh,KDatBlock)=
     1        GaussPwd(1:NGaussW,KPhase,KDatBlock)
            jj=j+(KPh-KPhase)*NParCellProfPwd
            kiPwd(jj:jj+NGaussW)=kiPwd(j:j+NGaussW)
          endif
        enddo
      endif
      call FeUpdateParamAndKeys(nEdwLorentz,nCrwLorentz,
     1  LorentzPwd(1,KPhase,KDatBlock),kiPwd(ILorentzPwd+IZdvihProf),5)
      if(NPhase.gt.1) then
        j=ILorentzPwd+IZdvihProf
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      then
            LorentzPwd(1:5,KPh,KDatBlock)=
     1        LorentzPwd(1:5,KPhase,KDatBlock)
            jj=j+(KPh-KPhase)*NParCellProfPwd
            kiPwd(jj:jj+5)=kiPwd(j:j+5)
          endif
        enddo
      endif
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1   KProfPwdNew.eq.IdPwdProfVoigt)
     2  call FeUpdateParamAndKeys(nEdwZeta,nCrwZeta,
     3    ZetaPwd(KPhase,KDatBlock),kiPwd(IZetaPwd+IZdvihProf),1)
      if(KPartBroad(KPhase,KDatBlock).gt.0.or.
     1   KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial)
     2  call FeQuestRealAFromEdw(nEdwDirBroad,
     3                           DirBroad(1,KPhase,KDatBlock))
9999  return
      end
