      subroutine KresliGrafNew
      parameter (mxpar=20)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension XArr(361),YArr(361)
      character*80 t80
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,YBottomMargin,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Return')
      nButtReturn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-dpom
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Define')
      nButtDefine=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      xomn=0.
      xomx=360.
      yomn=-.1
      yomx=1.1
      Pref1=1.5
      Pref2=0.
2000  pom=0.
      dpom=1.
      yomx=0.
      do i=1,361
        XArr(i)=pom
        csq=cos(Pom*ToRad)**2
        qq=csq*Pref1**2+(1.-csq)/Pref1
        YArr(i)=Pref2+(1.-Pref2)/qq**1.5
        yomx=max(yomx,YArr(i))
        pom=pom+dpom
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Angle')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Pref')
      call FeXYPlot(XArr,YArr,361,NormalLine,NormalPlotMode,Red)
3000  call FeQuestEvent(id,icont,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtReturn) then
          go to 5000
        else if(CheckNumber.eq.nButtDefine) then
          call FeQuestButtonOff(CheckNumber)
          idp=NextQuestId()
          xqd=150.
          il=2
          call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define',0,LightGray,
     1                       0,0)
          tpom=5.
          t80='Pref%1'
          xpom=tpom+FeTxLength(t80)+5.
          dpom=30.
          il=1
          call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,Pref1,.false.,.false.)
          nEdwPref1=EdwLastMade
          il=il+1
          t80='Pref%2'
          call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,Pref2,.false.,.false.)
          nEdwPref2=EdwLastMade
3500      call FeQuestEvent(idp,icont,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          if(ich.eq.0) then
            call FeQuestRealFromEdw(nEdwPref1,Pref1)
            call FeQuestRealFromEdw(nEdwPref2,Pref2)
          endif
          call FeQuestRemove(idp)
          EventType=0
          go to 2000
        endif
      endif
      go to 3000
5000  call FeQuestRemove(id)
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call UnitMat(F2O,3)
      return
      end
