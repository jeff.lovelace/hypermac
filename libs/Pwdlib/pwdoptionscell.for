      subroutine PwdOptionsCell
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      integer CrlCentroSymm,EdwStateQuest
      logical CrwLogicQuest
      save nEdwCell,nEdwMMax,nCrwCell,nCrwSatFrMod,nCrwSkipFriedel
      save /PwdOptionsC/
      entry PwdOptionsCellMake(id)
      j=ICellPwd+IZdvihCell
      ilp=2
      tpom=5.
      call FeQuestLblMake(id,tpom,ilp-1,'Cell parameters','L','B')
      do i=1,6+NDimI(KPhase)*3
        if(i.le.6) then
          Veta=lCell(i)
          pom=CellPwd(i,KPhase)
        else
          if(i.eq.7) then
            ilp=3
            Veta='Modulation vector'
            if(NDimI(KPhase).gt.1) Veta=Veta(:idel(Veta))//'s'
            call FeQuestLblMake(id,5.,il+1,Veta,'L','B')
          endif
          n=mod(i-7,3)+1
          m=(i-7)/3+1
          pom=QuPwd(n,m,KPhase)
          write(Veta,'(''q'',2i1)') n,m
          if(NDim(KPhase).eq.4) Veta(3:3)=' '
        endif
        im=mod(i-1,3)+1
        il=(i-1)/3+ilp
        tpom=max(xpoma(im)-5.-FeTxLength(Veta),tpoma(im))
        call FeMakeParEdwCrw(id,tpom,xpoma(im),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),i.gt.6)
        if(i.eq.1) then
          nEdwCell=nEdw
          nCrwCell=nCrw
        endif
        j=j+1
      enddo
      if(NDimI(KPhase).gt.0) then
        il=il+1
        Veta='Maximal satellite ind'
        i=idel(Veta)
        if(NDim(KPhase).eq.4) then
          Veta=Veta(:i)//'ex'
        else
          Veta=Veta(:i)//'ices'
        endif
        call FeQuestLblMake(id,5.,il,Veta,'L','B')
        if(NAtCalc.gt.0) then
          il=il+1
          Veta='Use only satellites corresponding to existing '//
     1         'modulation waves'
          xpom=5.
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwSatFrMod=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,SatFrMod(KPhase,KDatBlock))
        else
          nCrwSatFrMod=0
          SatFrMod(KPhase,KDatBlock)=.false.
        endif
        il=il+1
        Veta='%m(max)'
        cpom=FeTxLengthUnder(Veta)+10.
        tpom=5.
        dpom=40.
        xpom=tpom+cpom
        do i=1,NDimI(KPhase)
          if(i.eq.2) then
            Veta(2:2)='p'
          else if(i.eq.3) then
            Veta(2:2)='n'
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',
     1                        dpom,EdwYd,0)
          if(i.eq.1) nEdwMMax=EdwLastMade
          tpom=xpom+dpom+20.
          xpom=tpom+cpom
          if(.not.SatFrMod(KPhase,KDatBlock))
     1      call FeQuestIntEdwOpen(EdwLastMade,
     2                             MMaxPwd(i,KPhase,KDatBlock),.false.)
        enddo
      endif
      if(CrlCentroSymm().le.0) then
        il=il+1
        Veta='Skip Friedel pairs'
        xpom=5.
        tpom=xpom+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSkipFriedel=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SkipFriedel(KPhase,KDatBlock))
      else
        nCrwSkipFriedel=0
        SkipFriedel(KPhase,KDatBlock)=.false.
      endif
      go to 9999
      entry PwdOptionsCellCheck
      if(NDimI(KPhase).gt.0) then
        if(nCrwSatFrMod.gt.0) then
          nEdw=nEdwMMax
          if(CrwLogicQuest(nCrwSatFrMod)) then
            do i=1,NDimI(KPhase)
              call FeQuestIntFromEdw(nEdw,MMaxPwd(i,KPhase,KDatBlock))
              call FeQuestEdwDisable(nEdw)
              nEdw=nEdw+1
            enddo
          else
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              do i=1,NDimI(KPhase)
                call FeQuestIntEdwOpen(nEdw,MMaxPwd(i,KPhase,KDatBlock),
     1                                 .false.)
                nEdw=nEdw+1
              enddo
            endif
          endif
        endif
      endif
      go to 9999
      entry PwdOptionsCellUpdate
      if(nCrwSatFrMod.gt.0)
     1  SatFrMod(KPhase,KDatBlock)=CrwLogicQuest(nCrwSatFrMod)
      if(nCrwSkipFriedel.gt.0)
     1  SkipFriedel(KPhase,KDatBlock)=CrwLogicQuest(nCrwSkipFriedel)
      j=ICellPwd+IZdvihCell
      call FeUpdateParamAndKeys(nEdwCell,nCrwCell,CellPwd(1,KPhase),
     1                          kiPwd(j),6)
      call FeUpdateParamAndKeys(nEdwCell+6,nCrwCell+6,
     1                          QuPwd(1,1,KPhase),kiPwd(j+6),
     2                          3*NDimI(KPhase))
      if(NDimI(KPhase).gt.0) then
        nEdw=nEdwMMax
        do i=1,NDimI(KPhase)
          call FeQuestIntFromEdw(nEdw,MMaxPwd(i,KPhase,KDatBlock))
          nEdw=nEdw+1
        enddo
      endif
      go to 9999
9999  return
      end
