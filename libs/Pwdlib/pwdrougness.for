      subroutine PwdRougness(th2,Roughness,DerRoughness)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension DerRoughness(2)
      real Numer
      if(KRough(KDatBlock).eq.IdPwdRoughNone) then
        RoughNess=1.
        call SetRealArrayTo(DerRoughness,2,0.)
      else
        rsnth=1./sin(th2*.5)
        rsnthq=rsnth**2
      endif
      if(KRough(KDatBlock).eq.IdPwdRoughPitchke) then
        Numer=(1.-RoughPwd(1,KDatBlock)*
     1        (rsnth-RoughPwd(2,KDatBlock)*rsnthq))
        Denom=(1.-RoughPwd(1,KDatBlock)+
     1         RoughPwd(1,KDatBlock)*RoughPwd(2,KDatBlock))
        RoughNess=Numer/Denom
        DenomQ=Denom**2
        DNumer=-rsnth+RoughPwd(2,KDatBlock)*rsnthq
        DDenom=-1.+RoughPwd(2,KDatBlock)
        DerRoughNess(1)=(DNumer*Denom-Numer*DDenom)/DenomQ
        DNumer=RoughPwd(1,KDatBlock)*rsnthq
        DDenom=RoughPwd(1,KDatBlock)
        DerRoughNess(2)=(DNumer*Denom-Numer*DDenom)/DenomQ
      else if(KRough(KDatBlock).eq.IdPwdRoughSuortti) then
        expn=exp(-RoughPwd(2,KDatBlock)*rsnth)
        expd=exp(-RoughPwd(2,KDatBlock))
        Numer=(RoughPwd(1,KDatBlock)+(1.-RoughPwd(1,KDatBlock))*expn)
        Denom=(RoughPwd(1,KDatBlock)+(1.-RoughPwd(1,KDatBlock))*expd)
        RoughNess=Numer/Denom
        DenomQ=Denom**2
        DNumer=1.-expn
        DDenom=1.-expd
        DerRoughNess(1)=(DNumer*Denom-Numer*DDenom)/DenomQ
        DNumer=(-1.+RoughPwd(1,KDatBlock))*expn*rsnth
        DDenom=(-1.+RoughPwd(1,KDatBlock))*expd
        DerRoughNess(2)=(DNumer*Denom-Numer*DDenom)/DenomQ
      endif
      return
      end
