      subroutine PwdPutRecordToM95(ln)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      call PwdIncSpectNorm(1)
      do i=1,Npnts
        if(UseIncSpect(KDatBlock).eq.1) then
          write(ln,Format92) XPwd(i),YoPwd(i),YsPwd(i),YiPwd(i)
        else
          write(ln,Format92) XPwd(i),YoPwd(i),YsPwd(i)
        endif
      enddo
      NLines95(KRefBlock)=Npnts
      NRef95(KRefBlock)=Npnts
      KDatB=RefDatCorrespond(KRefBlock)
      if(KDatB.gt.0) then
        if(NManBackg(KDatB).gt.0) then
          write(ln,FormA) 'manual_background'
          do i=1,NManBackg(KDatB)
            write(ln,Format92) XManBackg(i,KDatB),
     1                         YManBackg(i,KDatB)
          enddo
        endif
      endif
      call PwdIncSpectNorm(0)
      return
      end
