      subroutine PwdD2X(X,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension X(n)
      do i=1,n
        if(isTOF) then
          X(i)=PwdD2TOF(X(i))
        else
          if(KRefLam(KDatBlock).eq.1) then
            X(i)=PwdD2TwoTh(X(i),LamPwd(1,KDatBlock))
          else
            X(i)=PwdD2TwoTh(X(i),LamAve(KDatBlock))
          endif
        endif
      enddo
      return
      end
