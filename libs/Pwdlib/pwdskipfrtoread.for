      subroutine PwdSkipFrToRead(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/SkipFrToQuest/ nEdwFirst,ThFrTo(2),Prazdno
      character*80 t80
      character*(*) Command
      logical   Prazdno
      save /SkipFrToQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'skipfrto'.and.t80.ne.'!skipfrto') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        kk=0
        call StToReal(t80,kk,ThFrTo(i),1,.false.,ich)
        if(ich.ne.0) go to 8040
      enddo
      Prazdno=.false.
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
