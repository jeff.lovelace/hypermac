      subroutine PwdImportManBackg
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      character*80 Veta
      character*12 men(2)
      character*4  mene(2)
      data men/'%GSAS','Fr%ee format'/
      data mene/'.gbg','.dat'/
      character*256 EdwStringQuest
      character*80 FileNameIn
      character*5  t5
      dimension xp(2)
      integer FileInType
      logical NameFromBrowser,ExistFile
      lni=0
      ntypein=2
      NameFromBrowser=.false.
      FileNameIn=' '
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,ntypein+1,'Specify input '//
     1                   'background file',1,LightGray,0,0)
      FileInType=1
      tpom=5.
      xpom=70.
      do i=1,ntypein
        call FeQuestCrwMake(id,tpom,i+1,xpom,i+1,men(i),'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) ncrwTypeFr=CrwLastMade
        if(i.eq.ntypein) ncrwTypeTo=CrwLastMade
        call FeQuestCrwOpen(i,i.eq.FileInType)
      enddo
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,1,dpom,ButYd,Veta)
      nButtonBrowse=ButtonLastMade
      FileNameIn=fln(:ifln)//mene(1)
      Veta='File %name'
      dpom=xpom-20.
      xpom=tpom+FeTxLengthUnder(Veta)+10
      dpom=dpom-xpom
      call FeQuestEdwMake(id,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
      nEdwFileIn=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFileIn,FileNameIn)
      call FeQuestButtonOpen(nButtonBrowse,ButtonOff)
1015  if(.not.NameFromBrowser) FileNameIn=fln(:ifln)//mene(FileInType)
      call FeQuestStringEdwOpen(nEdwFileIn,FileNameIn)
1020  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        FileNameIn=EdwStringQuest(nEdwFileIn)
        if(.not.ExistFile(FileNameIn)) then
          call FeChybne(-1.,-1.,'the file "'//
     1                  FileNameIn(:idel(FileNameIn))//'" doesn''t'//
     2                  ' exist','try again.',SeriousError)
          go to 1020
        endif
        QuestCheck(id)=0
        go to 1020
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.ncrwTypeFr.and.
     1                                  CheckNumber.le.ncrwTypeTo) then
        FileInType=CheckNumber
        go to 1015
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtonBrowse)
     1  then
        t5='*'//mene(FileInType)
        call FeFileManager('Select reflection file',FileNameIn,t5,0,
     1                     .true.,ich)
        if(ich.le.0) then
          NameFromBrowser=FileNameIn.ne.' '
          go to 1015
        else
          FileNameIn=EdwStringQuest(nEdwFileIn)
          go to 1020
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1020
      endif
      if(ich.eq.0) FileNameIn=EdwStringQuest(nEdwFileIn)
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call iom95(0,fln(:ifln)//'.m95')
      call PwdM92Nacti
      lni=NextLogicNumber()
      call OpenFile(lni,FileNameIn,'formatted','old')
      if(ErrFlag.ne.0) go to 8000
      if(.not.allocated(XManBackg))
     1   allocate(XManBackg(100,NDatBlock),YManBackg(100,NDatBlock))
      NManBackg(KDatBlock)=0
1000  read(lni,FormA,err=9100,end=2000) Veta
      if(Veta.eq.' ') go to 1000
      k=0
      if(FileInType.eq.1) then
        call kus(Veta,k,Cislo)
      else
        call DeleteFirstSpaces(Veta)
        if(index(Cifry,Veta(1:1)).le.0) go to 1000
      endif
      call StToReal(Veta,k,xp,2,.false.,ich)
      n=NManBackg(KDatBlock)+1
      call ReallocManBackg(n)
      NManBackg(KDatBlock)=n
      XManBackg(NManBackg(KDatBlock),KDatBlock)=xp(1)
      YManBackg(NManBackg(KDatBlock),KDatBlock)=xp(2)
      go to 1000
2000  write(Cislo,'(''.l'',i2)') KRefBlock
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
      call DeleteFile(RefBlockFileName)
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      call PwdPutRecordToM95(95)
      ModifiedRefBlock(KRefBlock)=.true.
      call CloseIfOpened(95)
      call CompleteM95(0)
      call EM9CreateM90Powder(ich)
      call CompleteM90
      go to 9999
8000  ErrFlag=0
      go to 9999
9100  call FeReadError(lni)
9999  call CloseIfOpened(lni)
      return
103   format(i6)
      end
