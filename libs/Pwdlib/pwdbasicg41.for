      subroutine PwdBasicG41(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      real xp(6)
      ich=0
      do i=1,4
        read(71,FormA,err=9100,end=9100) Veta
      enddo
      k=0
      call StToReal(Veta,k,xp,6,.false.,ich)
      if(ich.ne.0) go to 9100
      NPntsMax=nint(xp(1))
      TempRefBlock(KRefBlock)=xp(2)
      IVariG41=nint(xp(4))
      if(xp(5).gt.1..and.xp(6).gt.1.) then
        CNormG41=xp(5)/xp(6)
      else
        CNormG41=1.
      endif
      read(71,FormA,err=9100,end=9100) Veta
      k=0
      call StToReal(Veta,k,xp,3,.false.,ich)
      deg0=xp(1)
      degs=xp(2)
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      LamAveRefBlock(KRefBlock)=2.423
      LamA1RefBlock(KRefBlock)=2.423
      LamA2RefBlock(KRefBlock)=2.423
      go to 9999
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  call CloseIfOpened(71)
      ich=1
9999  return
      end
