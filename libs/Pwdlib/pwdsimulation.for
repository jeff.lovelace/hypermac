      subroutine PwdSimulation
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'main.cmn'
      character*256 :: FileName = ' ',Veta,EdwStringQuest
      integer FeChdir
      logical StructureExists,FeYesNo,RefineEnd
      id=NextQuestId()
      xqd=400.
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Simulated structure',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Name of the simulated structure:'
      xpomp=tpom+FeTxLengthUnder(Veta)+5.
      dpomp=70.
      dpom=xqd-xpomp-dpomp-15.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      xpomb=xpomp+dpom+10.
      Veta='%Browse'
      call FeQuestButtonMake(id,xpomb,il,dpomp,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        FileName=EdwStringQuest(nEdwName)
        call FeFileManager('Define name of the simulated structure',
     1                     FileName,' ',1,.false.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwName,FileName)
        call FeQuestButtonOff(nButtBrowse)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        FileName=EdwStringQuest(nEdwName)
        if(FileName.eq.' ') then
          Veta='the name of simulated structure hasn''t been '//
     1         'defined, try again.'
          call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        else if(StructureExists(FileName)) then
          call ExtractFileName(FileName,Veta)
          if(FeYesNo(-1.,YBottomMessage,'The structure "'//
     1               Veta(:idel(Veta))//
     2               '" already exists, rewrite it?',0)) go to 1800
        else
          go to 1800
        endif
        EventType=EventEdw
        EventNumber=nEdwName
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 1500
      endif
1800  call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call DeletePomFiles
      call ExtractDirectory(FileName,Veta)
      i=FeChdir(Veta)
      call FeGetCurrentDir
      call ExtractFileName(FileName,fln)
      ifln=idel(fln)
      call FeBottomInfo(' ')
      call DeletePomFiles
      isPowder=.true.
      ExistPowder=.true.
      if(ExistM90) then
        LamAve(1)=LamAve(KDatBlock)
        LamAvePwd(1)=LamAve(KDatBlock)
        LamA1(1)=LamA1(KDatBlock)
        LamA2(1)=LamA2(KDatBlock)
        LamPwd(1,1)=LamA1(KDatBlock)
        LamPwd(2,1)=LamA2(KDatBlock)
        NAlfa(1)=NAlfa(KDatBlock)
        LamRat(1)=LamRat(KDatBlock)
        LpFactor(1)=LpFactor(KDatBlock)
        AngleMon(1)=AngleMon(KDatBlock)
        AlphaGMon(1)=AlphaGMon(KDatBlock)
        BetaGMon(1)=BetaGMon(KDatBlock)
        FractPerfMon(1)=FractPerfMon(KDatBlock)
        Radiation(1)=Radiation(KDatBlock)
      else
        LamAve(1)=LamAveD(5)
        LamAvePwd(1)=LamAveD(5)
        LamA1(1)=LamA1D(5)
        LamA2(1)=LamA2D(5)
        LamPwd(1,1)=LamA1D(5)
        LamPwd(2,1)=LamA2D(5)
        NAlfa(1)=1
        LamRat(1)=LamRatD(5)
        LpFactor(1)=PolarizedMonoPar
        AngleMon(1)=CrlMonAngle(MonCell(1,3),MonH(1,3),LamAve(1))
        FractPerfMon(1)=.5
        Radiation(1)=XRayRadiation
      endif
      call PwdSetTOF(KDatBlock)
      LorentzToCrySize(1)=10.*LamAve(1)/ToRad
      GaussToCrySize(1)=sqrt8ln2*LorentzToCrySize(1)
      ExistM90=.false.
      call DeleteFile(fln(:ifln)//'.m90')
      ExistM95=.false.
      call DeleteFile(fln(:ifln)//'.m95')
      NDatBlock=1
      KDatBlock=1
      DataType(1)=2
      call iom50(1,0,fln(:ifln)//'.m50')
      call SetBasicM41(0)
      call iom40(1,0,fln(:ifln)//'.m40')
      call RefOpenCommands
      NacetlInt(nCmdisimul)=1
      call RefRewriteCommands(1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call FeFillTextInfo('pwdsimul.txt',0)
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      call EM40EditParameters(IdParamPowder,0)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      call Refine(0,RefineEnd)
      if(ErrFlag.ne.0) go to 9999
      call FeDeferOutput
9999  return
      end
