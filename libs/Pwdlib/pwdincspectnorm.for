      subroutine PwdIncSpectNorm(Klic)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      if(UseIncSpect(KDatBlock).ne.1.or.
     1   (Klic.eq.0.eqv.IncSpectNorm)) go to 9999
      do i=1,Npnts
        if(Klic.eq.0) then
          if(YiPwd(i).gt.0.) then
            pom=1./YiPwd(i)
          else
            pom=0.
          endif
        else
          pom=YiPwd(i)
        endif
        YoPwd(i)=YoPwd(i)*pom
        YsPwd(i)=YsPwd(i)*pom
      enddo
      IncSpectNorm=Klic.eq.0
9999  return
      end
