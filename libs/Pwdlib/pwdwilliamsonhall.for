      subroutine PwdWilliamsonHall
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer :: ih(6),RolMenuSelectedQuest,KProfPwdUse=3,
     1           DrawFunction = 1
      logical :: EqIgCase,CrwLogicQuest,UseCalibration=.false.
      real h3(3),xo(3),xpp(3),LorentzPom(4),GaussPom(4)
      real, allocatable :: xx(:),yy(:)
      character*256 :: Veta,EdwStringQuest,FileNormal=' '
      character*12 :: Jmena(4) = (/'%Quit   ',
     1                             '%Save   ',
     2                             '%Print  ',
     3                             '%Options'/)
      double precision suma
      KPhaseIn=KPhase
      KPhaseAct=KPhase
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,
     1                      -1,-1)
      XStripWidth=100.
      call FeMakeGrWin(2.,XStripWidth,YBottomMargin,2.)
      call FeFillRectangle(XMaxBasWin-XStripWidth,XmaxBasWin,YMinGrWin,
     1                     YMaxGrWin,4,0,0,LightGray)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(120.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      dpom=ButYd+8.
      ypom=YMaxGrWin-dpom
      do i=1,4
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Jmena(i))
        j=ButtonOff
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtSave=ButtonLastMade
        else if(i.eq.3) then
          nButtPrint=ButtonLastMade
        else if(i.eq.4) then
          nButtOptions=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
1100  call iom41(0,0,fln(:ifln)//'.m41')
      idp=NextQuestId()
      xqd=450.
      Veta='Options'
      if(NPhase.le.1) then
        il=10
      else
        il=12
      endif
      call FeQuestCreate(idp,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='%Use calibration data'
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,UseCalibration)
      nCrwCalibration=CrwLastMade
      il=il+1
      Veta='%Calibration file:'
      bpom=50.
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-bpom-xpom-20.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFile=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNormal)
      xpom=xpom+dpom+10.
      Veta='%Browse'
      call FeQuestButtonMake(idp,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaMake(idp,il)
      xpom=5.
      tpom=xpom+CrwgXd+5.
      Veta='Li%near curve - Lorentzian dominating case'
      do i=1,3
        il=il+1
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwUseLinear=CrwLastMade
          Veta='%Quadratic curve - Gaussian dominating case'
        else if(i.eq.2) then
          nCrwQuadratic=CrwLastMade
          Veta='%Mixing parameter'
        else if(i.eq.3) then
          nCrwMixParameter=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.DrawFunction)
      enddo
      call FeReleaseOutput
      il=il+1
      call FeQuestLinkaMake(idp,il)
      Veta='Draw %Gaussian component'
      do i=1,3
        il=il+1
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                      2)
        if(i.eq.1) then
          nCrwGauss=CrwLastMade
          Veta='Draw %Lorentzian component'
        else if(i.eq.2) then
          nCrwLorentzian=CrwLastMade
          Veta='Draw pseudo-%Voigt'
        else if(i.eq.3) then
          nCrwVoigt=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.KProfPwdUse)
      enddo
      if(NPhase.gt.1) then
        il=il+1
        call FeQuestLinkaMake(idp,il)
        dpom=0.
        do i=1,NPhase
          dpom=max(dpom,FeTxLength(PhaseName(i)))
        enddo
        dpom=dpom+2.*EdwMarginSize+EdwYd
        Veta='%Select phase:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        il=il+1
        call FeQuestRolMenuMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          1)
        nRolMenuPhase=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                          KPhaseAct)
      else
        nRolMenuPhase=0
      endif
1130  if(UseCalibration) then
        call FeQuestStringEdwOpen(nEdwFile,FileNormal)
        call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      else
        call FeQuestEdwDisable(nEdwFile)
        call FeQuestButtonDisable(nButtBrowse)
      endif
1140  if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt) then
        nCrw=nCrwGauss
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.KProfPwdUse)
          nCrw=nCrw+1
        enddo
!        if(CrwLogicQuest(nCrwMixParameter)) then
!          nCrw=nCrwUseLinear
!          DrawFunction=1
!          do i=1,3
!            call FeQuestCrwOpen(nCrw,i.eq.DrawFunction)
!            nCrw=nCrw+1
!          enddo
!        endif
      else
        nCrw=nCrwGauss
        do i=1,3
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        nCrw=nCrwUseLinear
        DrawFunction=1
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.DrawFunction)
          nCrw=nCrw+1
        enddo
        call FeQuestCrwDisable(nCrwMixParameter)
      endif
1150  call FeQuestEvent(idp,ichp)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        Veta=EdwStringQuest(nEdwFile)
        call FeFileManager('Define the calibration structure',
     1                     Veta,' ',1,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwFile,Veta)
        go to 1150
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCalibration)
     1  then
        UseCalibration=CrwLogicQuest(nCrwCalibration)
        go to 1130
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuPhase) then
        KPhaseAct=RolMenuSelectedQuest(nRolMenuPhase)
        go to 1140
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1150
      endif
      if(ichp.eq.0) then
        if(UseCalibration) then
          FileNormal=EdwStringQuest(nEdwFile)
        else
          FileNormal=' '
        endif
        UseCalibration=FileNormal.ne.' '
        nCrw=nCrwUseLinear
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            DrawFunction=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt) then
          nCrw=nCrwGauss
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.1) then
                KProfPwdUse=IdPwdProfGauss
              else if(i.eq.2) then
                KProfPwdUse=IdPwdProfLorentz
              else
                KProfPwdUse=IdPwdProfVoigt
              endif
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
      endif
      call FeQuestRemove(idp)
      NPh=NPhase
      KDatB=KDatBlock
      if(UseCalibration) then
        call iom50(0,0,FileNormal(:idel(FileNormal))//'.m50')
        call iom41(0,0,FileNormal(:idel(FileNormal))//'.m41')
        KStrain(NPh+1,KDatB)=IdPwdStrainNone
        KProfPwd(NPh+1,KDatB)=KProfPwd(1,1)
        do i=1,4
          LorentzPom(i)=LorentzPwd(i,1,1)
        enddo
        do i=1,4
          GaussPom(i)=GaussPwd(i,1,1)
        enddo
        KProfPom=KProfPwd(1,1)
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      if(UseCalibration) then
        do i=1,4
          LorentzPwd(i,NPh+1,KDatB)=LorentzPom(i)
        enddo
        do i=1,4
          GaussPwd(i,NPh+1,KDatB)=GaussPom(i)
        enddo
        KProfPwd(NPh+1,KDatB)=KProfPom
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:idel(fln))//'.prf','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      KdeJe=0
1210  if(NDatBlock.gt.1) then
1220    read(ln,FormA,end=1230,err=9000) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'begin')) then
            if(KdeJe.eq.0) then
              go to 1250
            else
              go to 1280
            endif
          endif
        endif
        go to 1220
1230    Veta='Datablock "'//
     1        DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))//
     2       '" has not been found, run le Bail or Rietveld refinement '
     3     //'and try once more'
        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        go to 9999
      endif
1250  read(ln,'(2i5)',end=9000,err=9000) kType,kAlpha2
      if(KdeJe.gt.0) go to 1280
      MxRefPwd=0
1260  read(ln,PrfFormat,err=9000) ih(1:maxNDim)
      if(ih(1).gt.900) go to 1270
      MxRefPwd=MxRefPwd+1
      go to 1260
1270  m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
      l=NAlfa(KDatBlock)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      if(allocated(xx)) deallocate(xx,yy)
      allocate(ihPwdArr(maxNDim,MxRefPwd),FCalcPwdArr(MxRefPwd),
     1         MultPwdArr(MxRefPwd),iqPwdArr(MxRefPwd),
     2         KPhPwdArr(MxRefPwd),ypeaka(l,MxRefPwd),peaka(l,MxRefPwd),
     3         pcota(l,MxRefPwd),rdega(2,l,MxRefPwd),shifta(l,MxRefPwd),
     4         fnorma(l,MxRefPwd),tntsima(l,MxRefPwd),
     5         ntsima(l,MxRefPwd),sqsga(l,MxRefPwd),fwhma(l,MxRefPwd),
     6         sigpa(l,MxRefPwd),etaPwda(l,MxRefPwd),
     7         dedffga(l,MxRefPwd),dedffla(l,MxRefPwd),
     8         dfwdga(l,MxRefPwd),dfwdla(l,MxRefPwd),
     9         coef(m,l,MxRefPwd),coefp(m,l,MxRefPwd),
     a         coefq(2,l,MxRefPwd),Prof0(MxRefPwd),cotg2tha(l,MxRefPwd),
     1         cotgtha(l,MxRefPwd),cos2tha(l,MxRefPwd),
     2         cos2thqa(l,MxRefPwd),Alpha12a(l,MxRefPwd),
     3         Beta12a(l,MxRefPwd),xx(MxRefPwd),yy(MxRefPwd),
     4         IBroadHKLa(MxRefPwd))
      rewind ln
      KdeJe=1
      go to 1210
1280  n=0
1300  read(ln,PrfFormat,err=9000,end=1400) ih(1:maxNDim),pom,KPh
      if(ih(1).gt.900) go to 1400
      n=n+1
      ihPwdArr(1:maxNDim,n)=ih(1:maxNDim)
      KPhPwdArr(n)=KPh
      go to 1300
1400  call CloseIfOpened(ln)
      shiftPwd(1:3,KDatBlock)=0.
      KAsym(KDatBlock)=IdPwdAsymNone
      if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt)
     1  KProfPwd(KPhaseAct,KDatBlock)=KProfPwdUse
      xxm=0.
      yymn= 9999.
      yymx=-9999.
      n=0
      NLamPwd=1
      do i=1,MxRefPwd
        ih(1:3)=ihPwdArr(1:3,i)
        KPhase=KPhPwdArr(i)
        if(KPhase.ne.KPhaseAct) cycle
        n=n+1
        call FromIndSinthl(ih,h3,sinthl,sinthlq,1,0)
        th=asin(sinthl*LamPwd(1,KDatBlock))
        call SetProfFun(ih,h3,th)
        dth=(rdeg(2)-rdeg(1))/100.
        ProfMax=0.
        do j=-100,100
           tha=float(j)*dth
           pom=prfl(tha)
           ProfMax=max(ProfMax,pom)
        enddo
        if(DrawFunction.eq.1) then
          xx(n)=sin(th)
          yy(n)=cos(th)/ProfMax*100.
        else if(DrawFunction.eq.2) then
          xx(n)=sin(th)**2
          yy(n)=(cos(th)/ProfMax*100.)**2
        else
          xx(n)=sin(th)
          yy(n)=etaPwd
        endif
        if(UseCalibration) then
          KPhase=NPhase+1
          call SetProfFun(ih,h3,th)
          ProfMax=0.
          do j=-100,100
             tha=float(j)*dth
             pom=prfl(tha)
             ProfMax=max(ProfMax,pom)
          enddo
          if(DrawFunction.eq.1) then
            yy(n)=yy(n)-cos(th)/ProfMax*100.
          else if(DrawFunction.eq.2) then
            yy(n)=yy(n)-(cos(th)/ProfMax*100.)**2
          endif
        endif
        xxm=max(xxm,xx(n))
        yymx=max(yymx,yy(n))
        yymn=min(yymn,yy(n))
      enddo
      KPhase=KPhaseAct
      HardCopy=0
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      yomn=min(yymn*1.1,0.)
      yomx=yymx*1.1
      xomn=0.
      xomx=xxm*1.1
      call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeMakeAcFrame
      if(DrawFunction.eq.1.or.DrawFunction.eq.3) then
        Veta='sin(th)'
      else
        Veta='sin(th)^2'
      endif
      if(HardCopy.eq.HardCopyNum) write(85,FormA) '# x axis: '//
     1                                            Veta(:idel(Veta))
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
      if(DrawFunction.eq.1) then
        Veta='beta*cos(th)*100'
      else if(DrawFunction.eq.2) then
        Veta='(beta*cos(th)*100)^2'
      else
        Veta='eta'
      endif
      if(HardCopy.eq.HardCopyNum) write(85,FormA) '# y axis: '//
     1                                            Veta(:idel(Veta))
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,Veta)
      xpp(3)=0.
      n=0
      do i=1,MxRefPwd
        ih(1:3)=ihPwdArr(1:3,i)
        KPhase=KPhPwdArr(i)
        if(KPhase.ne.KPhaseAct) cycle
        n=n+1
        xpp(1)=xx(n)
        xpp(2)=yy(n)
        call FeXf2X(xpp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,White)
        if(HardCopy.eq.HardCopyNum) write(85,'(2e15.6)') xpp(1:2)
      enddo
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtOptions) then
          go to 1100
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      go to 9999
9000  call FeReadError(ln)
9999  call CloseIfOpened(ln)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,xx,yy,
     6             IBroadHKLa)
      KPhase=KPhaseIn
      call iom41(0,0,fln(:ifln)//'.m41')
      return
      end
