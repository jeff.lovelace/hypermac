      subroutine PwdLeBail(Klic,Info)
      use Basic_mod
      use Atoms_mod
      use Powder_mod
      use Refine_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension hh(3),ih(6),IHAr(6,*),MltAr(*),YPeakP(2)
      character*80 RefFileName,t80
      character*75 TextInfoSave(3)
      integer FlgAr(*),KPhAr(*),FeGetSystemTime,StartTimeP
      integer UseTabsIn
      logical CalcDerSave,SatFrModOnly(MxPhases),Generovat,Info,Show
      real LpCorrection,LeBailRFp,LpArg
      save FScal
      Generovat=.true.
      go to 1100
      entry PwdLeBailFromHKL(Klic,Info,IHAr,FlgAr,KPhAr,MltAr,NRef)
      Generovat=.false.
1100  UseTabsIn=UseTabs
      CalcDerSave=CalcDer
      CalcDer=.false.
      ln=0
      RefFileName=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
      NInfoSave=NInfo
      write(FormRfl(2:2),'(i1)') NDim(KPhase)
      do i=1,3
        TextInfoSave(i)=TextInfo(i)
      enddo
      call PwdSetTOF(KDatBlock)
      if(isTOF.or.isED) then
        FScal=1.
      else
        FScal=1./ToRad
      endif
      if(klic.le.2) then
        if(Generovat) then
          if(Klic.eq.0) then
            if(isTOF) then
              SinTOFThUse=sin(.5*TOFTTh(KDatBlock)*ToRad)
            else if(.not.isED) then
              LPTypeCorrUse=LpFactor(KDatBlock)
              FractPerfMonUse=FractPerfMon(KDatBlock)
              if(LPTypeCorrUse.eq.PolarizedGuinier) then
                snm=sin(AlphaGMon(KDatBlock)*ToRad)
                BetaGMonUse=BetaGMon(KDatBlock)*ToRad
              else
                snm=sin(AngleMon(KDatBlock)*ToRad)
              endif
              Cos2ThMon =1.-2.*snm**2
              Cos2ThMonQ=Cos2ThMon**2
            endif
          endif
          if(Klic.le.1) call PwdM92Nacti
          call OpenFile(91,RefFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          NLamPwd=0
          n=npnts
          if(isTOF) then
            do i=1,npnts
              if(YfPwd(i).eq.1) go to 1150
            enddo
            i=1
1150        if(i.gt.1) i=i-1
            stolmx=.5/PwdTOF2D(XPwd(i))
            XPwdMin(KDatBlock)=XPwd(i)
            do i=npnts,1,-1
              if(YfPwd(i).eq.1) then
                XPwdMax(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          else if(isED) then
            do i=NPnts,1,-1
              if(YfPwd(i).eq.1) go to 1250
            enddo
            i=NPnts
1250        stolmx=.5*XPwd(i)/EDEnergy2D(KDatBlock)
            XPwdMax(KDatBlock)=XPwd(i)
            XPwdMin(KDatBlock)=0.
            do i=1,npnts
              if(YfPwd(i).eq.1) then
                if(i.gt.1) XPwdMin(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          else
            do i=1,3
              ih(i)=i
              ih(i+3)=0
            enddo
            RadLast=XPwd(npnts)*ToRad
            pom=RadLast*.5
            do KPh=1,NPhase
              KPhase=KPh
              call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
              NRefPwd=1
              NLamPwd=1
              if(.not.allocated(ihPwdArr)) then
                n=1
                m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
                l=NAlfa(KDatBlock)
                allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),
     1                   MultPwdArr(n),iqPwdArr(n),KPhPwdArr(n),
     2                   ypeaka(l,n),peaka(l,n),pcota(l,n),rdega(2,l,n),
     3                   shifta(l,n),fnorma(l,n),tntsima(l,n),
     4                   ntsima(l,n),sqsga(l,n),fwhma(l,n),sigpa(l,n),
     5                   etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     6                   dfwdga(l,n),dfwdla(l,n),coef(m,l,n),
     7                   coefp(m,l,n),coefq(2,l,n),Prof0(n),
     8                   cotg2tha(l,n),cotgtha(l,n),cos2tha(l,n),
     9                   cos2thqa(l,n),Alpha12a(l,n),Beta12a(l,n),
     a                   IBroadHKLa(n),stat=i)
                if(i.ne.0) then
                    call FeAllocErr('Error occur in "PwdLeBail" '//
     1                              'point#1.',i)
                  go to 9999
                endif
                if(.not.isTOF.and..not.isED.and.
     1             KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
                  allocate(AxDivProfA(-25:25,l,n),
     1                     DerAxDivProfA(-25:25,7,l,n),
     2                     FDSProf(-25:25,l,n),stat=i)
                  if(i.ne.0) then
                    call FeAllocErr('Error occur in "PwdLeBail" '//
     1                              'point#2.',i)
                    go to 9999
                  endif
                endif
              endif
1400          call SetProfFun(ih,hh,pom)
              if(pom/ToRad.lt.89.98) then
                if(RadLast.ge.rdeg(1)) then
                  pom=pom+.1*ToRad
                  go to 1400
                endif
              endif
            enddo
            pom=min(pom+1.*ToRad,89.99*ToRad)
            stolmx=sin(pom)/LamPwd(1,KDatBlock)
            XPwdMax(KDatBlock)=2.*pom*FScal
            if(NAlfa(KDatBlock).gt.1)
     1        stolmx=max(stolmx,
     2                   stolmx*
     3                   LamPwd(1,KDatBlock)/LamPwd(2,KDatBlock))
            do i=npnts,1,-1
              if(YfPwd(i).eq.1) then
                if(i.lt.npnts) XPwdMax(KDatBlock)=XPwd(i)+.2
                exit
              endif
            enddo
            XPwdMin(KDatBlock)=0.
            do i=1,npnts
              if(YfPwd(i).eq.1) then
                if(i.gt.1) XPwdMin(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          endif
          if(NAtCalc.gt.0.and.DoLeBail.eq.0.or.isimul.gt.0) then
            do i=1,NPhase
              SatFrModOnly(i)=SatFrMod(i,KDatBlock)
            enddo
          else
            call SetLogicalArrayTo(SatFrModOnly,NPhase,.false.)
          endif
          call GenerRefPwd('Generation of reflections',stolmx,
     1                     MMaxPwd(1,1,KDatBlock),
     2                     SatFrModOnly,SkipFriedel,0)
          if(Console.or.BatchMode) then
            call FeFlowChartRemove
            call FeLstWriteLine(' ',-1)
          endif
          if(ErrFlag.ne.0) go to 9999
          n=0
          rewind 91
1500      n=n+1
          read(91,format91pow,end=1550)(ih(i),i=1,6)
          if(ih(1).gt.900) go to 1550
          go to 1500
1550      NRef91(KDatBlock)=n-1
        else
          n=NRef
        endif
        if(NAlfa(KDatBlock).gt.1) n=2*n
        call FeMouseShape(3)
      else
        RefFileName=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
        call OpenFile(91,RefFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        n=(NRef91(KDatBlock)+1)*NAlfa(KDatBlock)
      endif
      if((NAtCalc.gt.0.and.DoLeBail.eq.0.and.klic.ge.2).or.isimul.gt.0)
     1  go to 9999
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa,
     6             stat=i)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf,stat=i)
      NLamPwd=0
      LeBailFast=.false.
      n=n+1
      m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
      l=NAlfa(KDatBlock)
      allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1         iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2         pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3         tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4         sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5         dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6         coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7         cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8         Beta12a(l,n),IBroadHKLa(n),stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PwdLeBail" point#3.',i)
        go to 9999
      endif
      if(.not.isTOF.and..not.isED.and.
     1   KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        allocate(AxDivProfA(-25:25,l,n),DerAxDivProfA(-25:25,7,l,n),
     1           FDSProf(-25:25,l,n),stat=i)
        if(i.ne.0) then
          call FeAllocErr('Error occur in "PwdLeBail" point#4.',i)
          go to 9999
        endif
      endif
      allocate(BraggArr(n),ShiftArr(n),ypeakArr(n),indArr(6,n),
     1         MultArr(n),KPhArr(n),RDeg1Arr(n),RDeg2Arr(n),ICalcArr(n),
     2         CutCoef1Arr(n),CutCoef2Arr(n),FWHMArr(n),stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PwdLeBail" point#5.',i)
        go to 9999
      endif
      if(Generovat) rewind 91
      call PwdMakeSkipFlags
      LeBailNBragg=0
      NArr=0
2100  LeBailNBragg=LeBailNBragg+1
2150  if(Generovat) then
        read(91,format91pow,end=2200)(indArr(i,LeBailNBragg),i=1,6),
     1                                ICalcArr(LeBailNBragg),pom,i,i,KPh
        if(indArr(1,LeBailNBragg).gt.900) go to 2200
      else
        NArr=NArr+1
        if(NArr.gt.NRef) go to 2200
        call CopyVekI(IHAr(1,NArr),indArr(1,LeBailNBragg),6)
        if(FlgAr(NArr).ne.1.or.IHAr(1,NArr).gt.900) go to 2150
        KPh=KPhAr(NArr)
        pom=MltAr(NArr)
      endif
      KPhase=KPh
      call FromIndSinthl(indArr(1,LeBailNBragg),hh,sinthl,sinthlq,1,0)
      MultArr(LeBailNBragg)=pom
      KPhArr(LeBailNBragg)=KPh
      if(isTOF) then
        BraggArr(LeBailNBragg)=PwdD2TOF(.5/sinthl)
      else if(isED) then
        BraggArr(LeBailNBragg)=EDEnergy2D(KDatBlock)*2.*sinthl
      else
        poms=sinthl*LamPwd(1,KDatBlock)
        if(poms.lt..9999) then
          BraggArr(LeBailNBragg)=2.*asin(poms)
        else
          LeBailNBragg=LeBailNBragg-1
          go to 2100
        endif
      endif
      if(NAlfa(KDatBlock).gt.1.and..not.isTOF.and..not.isED) then
        call CopyVekI(indArr(1,LeBailNBragg),indArr(1,LeBailNBragg+1),
     1                NDim(KPh))
        LeBailNBragg=LeBailNBragg+1
        MultArr(LeBailNBragg)=pom
        KPhArr(LeBailNBragg)=KPh
        ICalcArr(LeBailNBragg)=ICalcArr(LeBailNBragg-1)
        poms=sinthl*LamPwd(2,KDatBlock)
        if(poms.lt..9999) then
          BraggArr(LeBailNBragg)=2.*asin(poms)
        else
          LeBailNBragg=LeBailNBragg-2
          go to 2100
        endif
      endif
      go to 2100
2200  LeBailNBragg=LeBailNBragg-1
      call CloseIfOpened(91)
      call FeReleaseOutput
      call FeDeferOutput
      call PwdGetAllBackground
      do i=1,LeBailNBragg
        if(KKLeBail(KDatBlock).eq.1.and.NPhase.gt.1.and.KPhArr(i).gt.1
     1     .and.(NAtIndLenAll(KPhArr(i)).gt.0.or.
     2           NAtPosLenAll(KPhArr(i)).gt.0)) then
          pom=ICalcArr(i)
        else
          pom=10.
        endif
        ypeakArr(i)=pom
        if(NAlfa(KDatBlock).eq.2.and.mod(i,2).eq.0)
     1    ypeakArr(i)=pom*LamRat(KDatBlock)
      enddo
      Ninfo=3
      Show=Info.and.Klic.le.1
      StartTimeP=FeGetSystemTime()
      call PwdSetPoints(Show,StartTimeP)
      if(ErrFlag.ne.0) go to 9999
      if(Info) then
        if(NDatBlock.gt.1) then
          TextInfo(1)='LeBail decomposition - '//
     1      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
        else
          TextInfo(1)='      LeBail decomposition'
        endif
      endif
      if(Show) then
        ip=0
      else
        ip=2
      endif
      LeBailFast=.true.
      do i=1,500
        LeBailRFp=LeBailRF
        call PwdSetIobs
        call PwdSetPoints(Show,StartTimeP)
        pom=LeBailRFp/LeBailRF
        if(Info.and..not.Show) then
          Show=FeGetSystemTime()-StartTimeP.gt.5000
          ip=i
        endif
        if(Show) then
          if(i.ge.ip) then
            write(TextInfo(2),101) i,LeBailRF
            write(TextInfo(3),102) pom
            call FeMsgShow(3,-1.,300.,i.eq.ip)
          endif
        endif
        if(pom.lt.1.001) go to 2300
      enddo
      i=500
2300  if(Show) then
        if(Klic.le.1) call FeWait(1.)
        if(i.ge.ip) call FeMsgClear(3)
      endif
      pom=0.
      do i=1,LeBailNBragg
        pom=max(pom,ypeakArr(i))
      enddo
      scPwd(KDatBlock)=sqrt(pom)
      if(NAtAll.le.0) then
        sc(1,KDatBlock)=scPwd(KDatBlock)
        call iom40only(1,0,fln(:ifln)//'.m40')
        call SetMag(0)
      endif
      if((NAtCalc.gt.0.and.DoLeBail.eq.0.and.klic.ge.2).or.isimul.gt.0)
     1  go to 9999
      if(Klic.le.1) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.prf','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        if(NDatBlock.gt.1)
     1    write(Ln,104)
     2      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     3      'begin'
        if(isTOF) then
          if(TOFInD) then
            i=2
          else
            i=1
          endif
        else if(isED) then
          i=3
        else
          i=0
        endif
        write(Ln,'(20i5)') 2,NAlfa(KDatBlock)-1,i,NPhase,
     1                     (NDim(i),i=1,NPhase)
        do j=1,LeBailNBragg,NAlfa(KDatBlock)
          BraggPom=BraggArr(j)*FScal
          if(BraggPom.lt.XPwdMin(KDatBlock).or.
     1       BraggPom.gt.XPwdMax(KDatBlock)) cycle
          call SetPref(indArr(1,j),pref)
          pom=MultArr(j)*pref
          do k=1,NAlfa(KDatBlock)
            jk=j+k-1
            if(isTOF) then
              LpArg=PwdTOF2D(BraggArr(jk))
            else if(isED) then
              LpArg=1.
            else
              LpArg=BraggArr(jk)*.5
            endif
            YPeakP(k)=ypeakArr(jk)*pom/LpCorrection(LpArg)
          enddo
          write(ln,PrfFormat)(indArr(i,j),i=1,maxNDim),MultArr(j),
     1      KPhArr(j),
     2      (BraggArr(k)*FScal,ShiftArr(k)*FScal,FWHMArr(k)*FScal,
     3       ypeakp(k-j+1),k=j,j+NAlfa(KDatBlock)-1)
        enddo
        write(ln,'('' 999'')')
        do i=1,npnts
          if(isTOF.or.isED) then
            pom=0.
          else
            th=XPwd(i)*ToRad
            csth=cos(th*.5)
            sin2th=sin(th)
            shift=(shiftPwd(1,KDatBlock)+shiftPwd(2,KDatBlock)*csth+
     1             shiftPwd(3,KDatBlock)*sin2th)*.01
            pom=th/ToRad+shift
          endif
          write(Ln,PrfPointsFormat)
     1      XPwd(i),YoPwd(i),YcPwd(i),YsPwd(i),pom,YfPwd(i),YiPwd(i),
     2      (YcPwdInd(i,j),j=1,NPhase),YbPwd(i)
        enddo
        write(ln,'(''999.'')')
        if(NDatBlock.gt.1) write(Ln,104)
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
        call CloseIfOpened(ln)
      endif
      if(scPwd(KDatBlock).gt..000001) then
        ln=NextLogicNumber()
        call OpenFile(91,RefFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        call OpenFile(ln,fln(:ifln)//'.m91','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
!        nn=LeBailLastRef
!        do i=LeBailLastRef,1,-1
!          if(ypeakArr(i).gt.0.) exit
!          nn=i
!        enddo
        nn=LeBailNBragg
        n=0
        do i=1,nn,NAlfa(KDatBlock)
          BraggPom=BraggArr(i)*FScal
          if(BraggPom.lt.XPwdMin(KDatBlock).or.
     1       BraggPom.gt.XPwdMax(KDatBlock)) cycle
          pom=ypeakArr(i)/(scPwd(KDatBlock)*.01)**2
          n=n+1
          write(91,format91pow)(indArr(j,i),j=1,6),pom,MultArr(i),1,0,
     1                          KPhArr(i)
          write(ln,format91pow)(indArr(j,i),j=1,6),pom,1.,1,0,KPhArr(i)
        enddo
        write(91,'('' 999'')')
        write(ln,'('' 999'')')
        call CloseIfOpened(ln)
        call CloseIfOpened(91)
        NRef91(KDatBlock)=n+1
      else
        call FeChybne(-1.,YBottomMessage,'Le Bail decomposition '//
     1                'wasn''t successful.',
     2                'check background and Bragg peak positions.',
     3                SeriousError)
        ErrFlag=1
        go to 9999
      endif
      if(klic.le.1) then
        if(klic.eq.1) then
          KPhase=KPhaseBasic
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'.rfl','formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          ik=LeBailLastRef
          if(NAlfa(KDatBlock).eq.2) ik=ik-1
          do i=ik,1,-NAlfa(KDatBlock)
            if(ypeakArr(i)/(scPwd(KDatBlock)*.01)**2.gt..0001) then
              nn=i
              exit
            endif
          enddo
          do 3000i=1,nn,NAlfa(KDatBlock)
            if(KPhArr(i).ne.KPhase) go to 3000
            peak=BraggArr(i)
            call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
            if(isTOF.or.isED) then
              th=peak
            else
              th=peak*.5
              peak=peak/torad
              do j=1,NSkipPwd(KDatBlock)
                if(peak.ge.SkipPwdFr(j,KDatBlock).and.
     1             peak.le.SkipPwdTo(j,KDatBlock)) go to 3000
              enddo
            endif
            NRefPwd=(i-1)/NAlfa(KDatBlock)+1
            NLamPwd=mod(i-1,NAlfa(KDatBlock))+1
            call SetProfFun(indArr(1,i),hh,th)
            pom=ypeakArr(i)/(scPwd(KDatBlock)*.01)**2
            if(isTOF.or.isED) then
              pomw=fwhm
            else
              pomw=fwhm/torad
            endif
            write(ln,FormRfl)(indArr(j,i),j=1,NDim(KPhase)),pom,pomw
3000      continue
        endif
        close(91,status='delete')
      else
        rewind 91
      endif
      go to 9999
9999  call CloseIfOpened(ln)
      CalcDer=CalcDerSave
      KPhase=KPhaseBasic
      if(allocated(BraggArr))
     1  deallocate(BraggArr,ShiftArr,ypeakArr,indArr,MultArr,KPhArr,
     2             RDeg1Arr,RDeg2Arr,ICalcArr,CutCoef1Arr,CutCoef2Arr,
     3             FWHMArr,stat=i)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa,
     6             stat=i)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf,stat=i)
      if(allocated(LeBailPrfArr)) deallocate(LeBailPrfArr,stat=i)
      do i=1,3
        TextInfo(i)=TextInfoSave(i)
      enddo
      NInfo=NInfoSave
      UseTabs=UseTabsIn
      return
101   format('Step#',i3,'     Rp(i)     =',2f8.3)
102   format(8x,        ' Rp(i)/Rp(i-1) =',f8.4)
103   format('%struct ',80a1)
104   format(a,1x,a)
      end
