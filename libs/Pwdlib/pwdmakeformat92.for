      subroutine PwdMakeFormat92
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer Exponent10
      ipointA=0
      iorderA=0
      ipointI=0
      iorderI=0
      do i=Npnts,1,-1
        Deg=XPwd(i)
        iorderA=max(iorderA,Exponent10(Deg))
        j=ipointA
3030    if(iorderA+j.gt.6) go to 3040
        pom=Deg*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3030
        endif
3040    ipointA=max(ipointA,j)
        Fobs=YoPwd(i)
        if(Fobs.le.0.) cycle
        iorderI=max(Exponent10(Fobs),iorderI)
        j=ipointI
3050    if(iorderI+j.gt.6) go to 3060
        pom=Fobs*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3050
        endif
3060    ipointI=max(ipointI,j)
        Ys=YsPwd(i)
        if(Ys.le.0.) go to 3100
        iorderI=max(Exponent10(Ys),iorderI)
        j=ipointI
        if(iorderI+j.gt.6) go to 3080
        pom=Ys*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3060
        endif
3080    ipointI=max(ipointI,j)
3100    Ys=YiPwd(i)
        if(Ys.le.0.) cycle
        iorderI=max(Exponent10(Ys),iorderI)
        j=ipointI
3120    if(iorderI+j.gt.6) go to 3180
        pom=Ys*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3120
        endif
3180    ipointI=max(ipointI,j)
      enddo
!      if(iorderA+ipointA.gt.6) ipointA=6-iorderA
!      if(iorderI+ipointI.gt.6) ipointI=6-iorderI
5000   write(Format92,'(''(f'',i2,''.'',i1,'',3f'',i2,''.'',i1,'')'')')
     1  ipointA+iorderA+3,ipointA,ipointI+iorderI+3,ipointI
      return
      end
