      subroutine PwdUpdateReflFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          KRefBlock=i
          go to 1100
        endif
      enddo
      go to 9999
1100  write(Cislo,'(''.l'',i2)') KRefBlock
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
      call DeleteFile(RefBlockFileName)
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      call PwdPutRecordToM95(95)
      ModifiedRefBlock(KRefBlock)=.true.
      call CloseIfOpened(95)
      call CompleteM95(0)
      call EM9CreateM90Powder(ich)
      call CompleteM90
9999  return
      end
