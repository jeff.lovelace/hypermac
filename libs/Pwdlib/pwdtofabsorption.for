      function PwdTOFAbsorption(XPwdi,AbsTOFPwd,DerAbsTOFPwd)
      pom=PwdTOF2D(XPwdi)
      PwdTOFAbsorption=expJana(-AbsTOFPwd*pom,ich)
      if(ich.eq.0) then
        DerAbsTOFPwd=-PwdTOFAbsorption*pom
      else
        PwdTOFAbsorption=1.
        DerAbsTOFPwd=0.
      endif
      return
      end
