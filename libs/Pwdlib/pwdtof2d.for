      function PwdTOF2D(TOF)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      ff(x,y)=PwdD2TOF(x)*1000.-y
      if(TOFInD) then
        PwdTOF2D=TOF
      else
        TOFp=TOF*1000.
        if(KUseTOFJason(KDatBlock).le.0) then
          pom=4.*ShiftPwd(3,KDatBlock)/ShiftPwd(2,KDatBlock)**2*
     1          (TOFp-ShiftPwd(1,KDatBlock))
          if(abs(pom).gt.1.e-3) then
            PwdTOF2D=ShiftPwd(2,KDatBlock)/(2.*ShiftPwd(3,KDatBlock))*
     1               (sqrt(1.+pom)-1.)
          else
            PwdTOF2D=1./ShiftPwd(2,KDatBlock)*
     1               (TOFp-ShiftPwd(1,KDatBlock))
          endif
        else
          dd=(TOFp-ShiftPwd(4,KDatBlock))/ShiftPwd(5,KDatBlock)
          x1=dd
          f1=ff(x1,TOFp)
          x2=x1
          zn=1.
          n=0
1100      x2=x2+zn*.001
          f2=ff(x2,TOFp)
          f12=f1*f2
          n=n+1
          if(f12.gt.0.) then
            if(n.gt.1.and.zn.gt.0.) then
              if(f12.gt.f12old) then
                x2=x1
                zn=-1.
                n=0
              endif
            endif
            f12old=f12
            go to 1100
          endif
          n=0
1200      xp=(f2*x1-f1*x2)/(f2-f1)
          n=n+1
          fp=ff(xp,TOFp)
          if(fp*f1.gt.0.) then
            if(abs(x1-xp).gt..00001) then
              f1=fp
              x1=xp
            else
              PwdTOF2D=(x1+xp)*.5
              go to 9999
            endif
          else if(fp*f1.lt.0.) then
            if(abs(x2-xp).gt..00001) then
              f2=fp
              x2=xp
            else
              PwdTOF2D=(x2+xp)*.5
              go to 9999
            endif
          else
            PwdTOF2D=xp
            go to 9999
          endif
          go to 1200
        endif
      endif
9999  return
      end
