      subroutine PwdPrfCalc
      use Powder_mod
      parameter (mxpar=20)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension WCen(5),WRel(5),WGam(5),Th2Cen(5),Th2Gam(5),
     1          WLam(-500:500),Wx(-500:500),WInt(-500:500),
     1          der(mxpar),ps(mxpar),ki(mxpar),
     2          dc(mxpar),am((mxpar*(mxpar+1))/2)
      character*80 SvFile
      logical RefineOn
      data WCen/1.534753,1.540596,1.541058,1.544410,1.544721/
      data WRel/1.59,57.91,7.62,24.17,8.71/
      data WGam/3.69,0.44,0.60,0.52,0.62/
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Refine')
      nButtRefine=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-dpom
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      pom=0.
      do j=1,5
        if(WRel(j).gt.pom) then
          jr=j
          pom=WRel(j)
        endif
      enddo
      nps=5
      nam=(nps*(nps+1))/2
      call SetIntArrayTo(ki,5,1)
      delta=.8
      do 4000ii=1,NBraggPeaks
        i=PeakPor(ii)
        RefineOn=.false.
        pom=0.
        Theta=PeakXPos(i)*.5*ToRad
        do j=1,5
          pom=pom+WCen(j)*WRel(j)*.01
        enddo
        xomn=PeakXPos(i)-delta
        xomx=PeakXPos(i)+delta
        yomn=0.
        yomx=0.
        ysum=0.
        jp=0
        do j=1,NPnts
          XPwdj=XPwd(j)
          if(XPwdj.ge.xomn.and.XPwdj.le.xomx) then
            if(YoPwd(j).gt.yomx) then
              yomx=YoPwd(j)
              jm=j
            endif
            if(jp.eq.0) jp=j
            jk=j
            ysum=ysum+YoPwd(j)
          endif
        enddo
        nn=jk-jp+1
        if(mod(nn,2).ne.1) then
          jk=jk+1
          nn=nn+1
          ysum=ysum+YoPwd(jk)
        endif
        deltap=2.*delta/float(nn-1)
        if(PeakInt(i).lt.0.) then
          PeakBckg(1,i)=(YoPwd(jp)+YoPwd(jk))*.5
          PeakInt(i)=ysum*deltap-PeakBckg(1,i)*deltap*float(nn)
          PeakFWHM(i)=0.1
          PeakEta(i)=0.5
        endif
        xomn=XPwd(jp)
        xomx=XPwd(jk)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        damp=0.1
2000    if(RefineOn) then
          ij=0
          do i=1,nps
            ij=ij+i
            der(i)=1./sqrt(am(ij))
          enddo
          call znorm(am,der,nps)
          call smi(am,nps,ising)
          call znorm(am,der,nps)
          call nasob(am,ps,dc,nps)
          PeakInt(i)=PeakInt(i)+dc(1)*damp
          PeakBckg(1,i)=PeakBckg(1,i)+dc(2)*damp
          PeakXPos(i)=PeakXPos(i)-dc(3)*damp
          PeakFWHM(i)=PeakFWHM(i)+dc(4)*damp
          PeakEta(i)=PeakEta(i)+dc(5)*damp
        endif
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
        call FeXYPlot(XPwd(jp),YoPwd(jp),nn,NormalLine,NormalPlotMode,
     1                white)
        call SetRealArrayTo(ps,nps,0.)
        call SetRealArrayTo(am,nam,0.)
        Theta=PeakXPos(i)*.5*ToRad
        do j=1,5
          Th2Cen(j)=2.*asin(sin(Theta)*WCen(j)/WCen(jr))/ToRad
          Th2Gam(j)=.002*WGam(j)/WCen(jr)*tan(Theta)/ToRad
        enddo
        jj=jp
        do j=-nn/2,nn/2
          XPwdj=XPwd(jj)
          Wx(j)=XPwdj
          pom=0.
          do k=1,5
            call PwdPrfFun(2,XPwdj-Th2Cen(k),Th2Gam(k),1.,fk5,fk1,
     1                     fk2,fk3)
            pom=pom+WRel(k)*.01*fk5
          enddo
          WLam(j)=pom
          jj=jj+1
        enddo
        do j=-nn/2,nn/2
          XPwdj=Wx(j)
          pom=0.
          pomdt=0.
          pomde=0.
          pomdg=0.
          pomdi=0.
          do k=-nn/2,nn/2
            if(j-k.ge.-nn/2.and.j-k.le.nn/2) then
              XPwdk=Wx(k)-PeakXPos(i)
              call PwdPrfFun(3,XPwdk,PeakFWHM(i),PeakEta(i),Voigt,
     1                       VoigtDT,VoigtDG,VoigtDE)
              pom=pom+Voigt*WLam(j-k)
              pomdt=pomdt+VoigtDT*WLam(j-k)
              pomde=pomde+VoigtDE*WLam(j-k)
              pomdg=pomdg+VoigtDG*WLam(j-k)
            endif
          enddo
          pom=pom*deltap
          pomdt=pomdt*deltap
          pomde=pomde*deltap
          pomdg=pomdg*deltap
          WInt(j)=PeakInt(i)*pom+PeakBckg(1,i)
          der(1)=pom
          der(2)=1.
          der(3)=PeakInt(i)*pomdt
          der(4)=PeakInt(i)*pomdg
          der(5)=PeakInt(i)*pomde
          dy=YoPwd(jp+j+nn/2)-WInt(j)
          wt=1.
          m=0
          do k=1,nps
            wtderk=wt*der(k)
            ps(k)=ps(k)+wtderk*dy
            do l=1,k
              m=m+1
              am(m)=am(m)+wtderk*der(l)
            enddo
          enddo
        enddo
        call FeXYPlot(Wx(-nn/2),WInt(-nn/2),nn,NormalLine,
     1                NormalPlotMode,Green)
        if(RefineOn) then
          nref=nref+1
          if(mod(nref,100).eq.0) then
            RefineOn=.false.
          else
            call FeReleaseOutput
            call FeDeferOutput
            go to 2000
          endif
        endif
3000    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton) then
          if(CheckNumber.eq.nButtRefine) then
            nref=0
            RefineOn=.true.
            go to 2000
          else if(CheckNumber.eq.nButtNext) then
            go to 4000
          endif
        endif
        go to 3000
4000  continue
      call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
