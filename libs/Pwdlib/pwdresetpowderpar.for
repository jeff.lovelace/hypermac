      subroutine PwdResetPowderPar
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*8 MenuPhase(MxPhases+1)
      integer RolMenuSelectedQuest
      if(NPhase.le.1) go to 9999
      do i=1,NPhase
        MenuPhase(i)=PhaseName(i)
      enddo
      MenuPhase(NPhase+1)='Default'
      dpom=20.
      do i=1,NPhase+1
        dpom=max(dpom,FeTxLengthUnder(MenuPhase(i)))
      enddo
      dpom=dpom+EdwYd+10.
      xpom=5.
      pom=xpom+dpom
      id=NextQuestId()
      xqd=2.*pom+50.
      il=2
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      call FeQuestRolMenuMake(id,xpom,il-1,xpom,il,'Source','L',dpom,
     1                         EdwYd,0)
      nRolMenuSource=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,MenuPhase,NPhase+1,1)
      bpom=15.
      xpom=(xqd-bpom)*.5
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,'=>')
      nButtAction=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd-dpom-5.
      call FeQuestRolMenuMake(id,xpom,il-1,xpom,il,'Target','L',dpom,
     1                        EdwYd,0)
      nRolMenuTarget=EdwLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,MenuPhase,NPhase,2)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAction)
     1  then
        is=RolMenuSelectedQuest(nRolMenuSource)
        it=RolMenuSelectedQuest(nRolMenuTarget)
        if(is.ne.it) call PwdCopyPowderPar(is,it)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.eq.0) call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
9999  return
      end
