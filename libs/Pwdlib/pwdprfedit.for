      subroutine PwdPrfEdit
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(DrawInD) then
        TextInfo(1)='      d        Int        B0    FWHM     Eta'
      else
        TextInfo(1)='     2th       Int        B0    FWHM     Eta'
      endif
      do ii=1,NBraggPeaks
        i=PeakPor(ii)
        write(TextInfo(ii+1),'(f8.3,2f10.1,2f8.4)')
     1    PeakXPos(i),PeakInt(i),PeakBckg(1,i),PeakFWHM(i),PeakEta(i)
      enddo
      NInfo=NBraggPeaks+1
      call FeInfoOut(-1.,-1.,'PEAKS','L')
      return
      end
