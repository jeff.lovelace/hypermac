      subroutine PwdSetLamProfile(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension isel(2)
      iuz=0
      poml=0.
      do j=1,NAlfa(KDatB)
        pom=0.
        do i=1,NLamSpect(KDatB)
          if(i.ne.iuz.and.LamSpectRel(i,KDatB).gt.pom) then
            k=i
            pom=LamSpectRel(i,KDatB)
          endif
          if(j.eq.1)
     1      poml=poml+LamSpectRel(i,KDatB)*LamSpectLen(i,KDatB)
        enddo
        iuz=k
        isel(j)=k
        LamPwd(j,KDatB)=LamSpectLen(k,KDatB)
      enddo
      LamAvePwd(KDatB)=poml
      DelLam=.00001
      do n=1,NAlfa(KDatB)
        call SetRealArrayTo(LamProfilePwd(-5000,n,KDatB),10001,0.)
        poml1=LamSpectLen(isel(n),KDatB)
        if(NAlfa(KDatB).gt.1) then
          poml2=LamSpectLen(isel(3-n),KDatB)
        else
          poml2=333333.
        endif
        StredLam=poml1
        do 1040i=1,NLamSpect(KDatB)
          SpectRel=LamSpectRel(i,KDatB)
          poml=LamSpectLen(i,KDatB)
          SpectLife=LamSpectLife(i,KDatB)*.001
          if(NAlfa(KDatB).gt.1) then
            if(abs(poml-poml1).gt.abs(poml-poml2)) go to 1040
          endif
          poml=StredLam-poml
          do j=-5000,5000
            pom=1.+(2.*(poml+float(j)*DelLam)/SpectLife)**2
            pom=2.*LamSpectRel(i,KDatB)/(pi*pom*SpectLife)
            LamProfilePwd(j,n,KDatB)=LamProfilePwd(j,n,KDatB)+pom
          enddo
1040    continue
        Suma=0.
        do j=-5000,5000
          Suma=Suma+LamProfilePwd(j,n,KDatB)*.000001
        enddo
        if(n.eq.1) then
          LamRatPwd(KDatB)=1./Suma
        else
          LamRatPwd(KDatB)=Suma*LamRatPwd(KDatB)
        endif
      enddo
      return
      end
