      subroutine PwdWriteRefLst(lstp,ihr,Yoqp,Ycqp,Ysqp,Ykp,Ywp,Yss,
     1                          abas,bbas,afp,bfp,
     2                          affreep,bffreep,afstp,bfstp,affreestp,
     3                          bffreestp,nout,KPh,sinthlp,extkorp,
     4                          extkorpm)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*1 nspec,kspec,t1
      dimension ihr(*)
      if(CalcDer.and.lstp.le.0) go to 9999
      Fcalc=sqrt(Ycqp)
      if(Ywp.gt.0.and.Ykp.gt.1.) then
        Fobs=Yoqp/Ywp
        pom1=Ysqp/Ywp-Fobs**2
        yss=sqrt(Yss)/Ywp
      else
        Fobs=0.
        pom1=0.
        yss=Fobs+1.
      endif
      if(pom1.gt.0.) then
        ys=sqrt(pom1)
      else
        ys=Fobs+1.
      endif
      if(SigMethod.eq.1) then
        ys=yss
      else if(SigMethod.eq.3) then
        ys=max(ys,yss)
      else if(SigMethod.eq.4) then
        ys=min(ys,yss)
      endif
      if(Fobs.gt..01) then
        Fobs=sqrt(Fobs)
        sigyo=max(.5*ys/Fobs,.0001)
        sigyo=sqrt(sigyo**2+(blkoef*Fobs)**2)
      else
        Fobs  =0.
        if(Fcalc.gt..0001) then
          sigyo=Fcalc
        else
          sigyo=1.
        endif
      endif
      dyp=Fobs-Fcalc
      wdy=min(dyp/sigyo,9999.)
      wdy=max(wdy,-9999.)
      if(abs(wdy).gt.vyh) then
        kspec='#'
      else
        kspec=' '
      endif
      if(Fobs.lt.2.*slevel*sigyo) then
        nspec='*'
      else
        nspec=' '
      endif
      if(.not.CalcDer) then
        if(lstp.gt.0) then
          ycf=sqrt(afp**2+bfp**2)
        else
          ycf=Fcalc
        endif
        if(NAtCalc.le.0.or.DoLeBail.ne.0) then
          yof=ycf
        else
          if(Fcalc.ne.0.) then
            yof=Fobs/Fcalc*ycf
          else
            yof=Fobs
          endif
        endif
        write(80,format80)(ihr(m),m=1,maxNDim),KPh,yof,yof,ycf,afp,bfp,
     1                     affreep,bffreep,afstp,bfstp,affreestp,
     2                     bffreestp,sigyo,sigyo
        if(NSpec.eq.'*') then
          t1='<'
        else
          t1='o'
        endif
        write(83,format83p)(ihr(m),m=1,maxNDim),ycf**2,yof**2,
     1                      2.*yof*sigyo,t1,.5/sinthlp,KPh,
     2                      sqrt(abas**2+bbas**2),abas,bbas
      endif
      if(lstp.le.0) go to 9999
      nout=nout+1
      if(ExistMagnetic) then
        write(lstp,format3p)(ihr(m),m=1,maxNDim),Fobs,Fcalc,abas,bbas,
     1    dyp,sigyo,1./sigyo**2,wdy,nout,nspec,kspec,sinthlp,extkorp,
     2    extkorpm,KPh,KPh,KPh
      else
        write(lstp,format3p)(ihr(m),m=1,maxNDim),Fobs,Fcalc,abas,bbas,
     1    dyp,sigyo,1./sigyo**2,wdy,nout,nspec,kspec,sinthlp,extkorp,
     2    KPh,KPh,KPh
      endif
9999  return
      end
