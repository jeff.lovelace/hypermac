      subroutine PwdResetVoigt
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      do KDatB=1,NDatBlock
        if(DataType(KDatB).eq.-2.or.
     1    KAsym(KDatB).eq.IdPwdAsymFundamental) cycle
        do KPh=1,NPhase
         if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1      KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
           Znak=0.
           do i=1,3,2
             if(LorentzPwd(i,KPh,KDatB).ne.0.)
     1         Znak=sign(1.,LorentzPwd(i,KPh,KDatB))
           enddo
           do i=1,5
             LorentzPwd(i,KPh,KDatB)=Znak*LorentzPwd(i,KPh,KDatB)
           enddo
         endif
         if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1      KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
           Znak=0.
           do i=1,4
             if(GaussPwd(i,KPh,KDatB).ne.0.)
     1         Znak=sign(1.,GaussPwd(i,KPh,KDatB))
           enddo
           do i=1,4
             GaussPwd(i,KPh,KDatB)=Znak*GaussPwd(i,KPh,KDatB)
           enddo
         endif
        enddo
      enddo
      return
      end
