      subroutine PwdOptionsRadiation
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*22 :: LabelRatio = '%I(#2)/I(#1)'
      character*14 :: LabelWaveLength1 = 'Wave length'
      character*11 :: LabelWaveLength = 'Wave length #1'
      integer RadiationOld,EdwStateQuest,FeMenu
      logical lpom,CrwLogicQuest
      save nEdwMonAngle,nEdwPerfMon,nEdwWL,nEdwWL1,nEdwWL2,nEdwRat,
     1     nEdwGAlpha,nEdwGBeta,
     2     nCrwPolarizationFirst,nCrwPolarizationLast,nCrwRadFirst,
     3     nCrwRadLast,nCrwDouble,nButtSetMonAngle,nButtInfoParallel,
     4     nButtInfoPerpendicular,nButtTube,nLblPol,nLblMono,
     5     RadiationOld,LPFactorOld
      save /PwdOptionsC/
      entry PwdOptionsRadiationMake(id)
      if(isTOF.or.isED) then
        nLblPol=0
        nCrwPolarizationFirst=0
        nCrwPolarizationLast=0
        nLblMono=0
        nEdwMonAngle=0
        nButtSetMonAngle=0
        nEdwPerfMon=0
        nButtInfoPerpendicular=0
        nButtInfoParallel=0
        Veta='Radiaton parameters can be modified only by reimporting'
        call FeQuestLblMake(id,xqdp*.5,8,Veta,'C','B')
        go to 9999
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        ichk=1
        igrp=1
        il=1
        do i=1,3
          if(i.eq.1) then
            Veta='%X-rays'
            lpom=Radiation(KDatBlock).eq.XRayRadiation
          else if(i.eq.2) then
            Veta='Ne%utrons'
            lpom=Radiation(KDatBlock).eq.NeutronRadiation
          else if(i.eq.3) then
            Veta='%Electrons'
            lpom=Radiation(KDatBlock).eq.ElectronRadiation
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,ichk,igrp)
          if(i.eq.1) then
            nCrwRadFirst=CrwLastMade
            ilb=il
          else
            xpomb=tpom+FeTxLength(Veta)+10.
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          il=il+1
        enddo
        nCrwRadLast=CrwLastMade
        Veta='X%-ray tube'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
        nButtTube=ButtonLastMade
        tpom=5.
        Veta='Kalpha1/Kalpha2 dou%blet'
        xpom=tpom+CrwXd+10.
        ichk=1
        igrp=0
        call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,
     1                      ichk,igrp)
        nCrwDouble=CrwLastMade
        il=il+1
        xpom=tpom+FeTxLength(LabelWaveLength1)+10.
        dpom=100.
        Veta=LabelWaveLength
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL=EdwLastMade
        Veta=LabelWaveLength1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL1=EdwLastMade
        il=il+1
        i=idel(Veta)
        Veta(i:i)='2'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL2=EdwLastMade
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                      EdwYd,0)
        nEdwRat=EdwLastMade
        il=1
        xpom=xpom+dpom+45.
        tpom=xpom+CrwgXd+10.
        Veta='Polarization correction:'
        call FeQuestLblMake(id,xpom+30.,il,Veta,'L','B')
        nLblPol=LblLastMade
        ichk=1
        igrp=2
        do i=1,5
          il=il+1
          if(i.eq.1) then
            Veta='Circul%ar polarization'
            j=PolarizedCircular
          else if(i.eq.2) then
            Veta='%Perpendicular setting'
           j=PolarizedMonoPer
          else if(i.eq.3) then
            Veta='Pa%rallel setting'
            j=PolarizedMonoPar
          else if(i.eq.4) then
            Veta='Guinier %camera'
            j=PolarizedGuinier
          else
            Veta='%Linearly polarized beam'
            j=PolarizedLinear
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          call FeQuestCrwOpen(CrwLastMade,LPFactor(KDatBlock).eq.j)
          if(i.eq.1) then
            nCrwPolarizationFirst=CrwLastMade
          else if(i.eq.2.or.i.eq.3) then
            if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
            Veta='Info'
            dpomb=FeTxLengthUnder(Veta)+10.
            call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
            if(i.eq.2) then
              nButtInfoPerpendicular=ButtonLastMade
            else
              nButtInfoParallel=ButtonLastMade
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          endif
        enddo
        nCrwPolarizationLast=CrwLastMade
        il=il+1
        Veta='Monochromator parameters:'
        call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
        call FeQuestLblOff(LblLastMade)
        nLblMono=LblLastMade
        il=il+1
        Veta='Per%fectness'
        tpom=xpom
        xpom=tpom+FeTxLengthUnder(Veta)+80.
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwPerfMon=EdwLastMade
        il=il+1
        Veta='Glancing an%gle'
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwMonAngle=EdwLastMade
        Veta='%Set glancing angle'
        dpomb=FeTxLengthUnder(Veta)+5.
        call FeQuestButtonMake(id,xpom+dpom+15.,il,
     1                         dpomb,ButYd,Veta)
        nButtSetMonAngle=ButtonLastMade
        Veta='Alp%ha(Guinier)'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGAlpha=EdwLastMade
        il=il+1
        Veta='Beta(Gui%nier)'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGBeta=EdwLastMade
        LPFactorOld=-1
        RadiationOld=-1
        go to 2500
      endif
      entry PwdOptionsRadiationCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwRadFirst.and.CheckNumber.le.nCrwRadLast)
     2  then
        if(CrwLogicQuest(nCrwRadFirst)) then
          Radiation (KDatBlock)=XRayRadiation
        else if(CrwLogicQuest(nCrwRadFirst+1)) then
          Radiation(KDatBlock)=NeutronRadiation
        else
          Radiation(KDatBlock)=ElectronRadiation
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              LPFactor(KDatBlock)=PolarizedCircular
            else if(i.eq.2) then
              LPFactor(KDatBlock)=PolarizedMonoPer
            else if(i.eq.3) then
              LPFactor(KDatBlock)=PolarizedMonoPar
            else if(i.eq.4) then
              LPFactor(KDatBlock)=PolarizedGuinier
            else
              LPFactor(KDatBlock)=PolarizedLinear
            endif
            exit
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        if(CrwLogicQuest(nCrwDouble)) then
          NAlfa(KDatBlock)=2
        else
          NAlfa(KDatBlock)=1
        endif
        RadiationOld=-1
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          LamA1(KDatBlock)=LamA1D(k)
          LamA2(KDatBlock)=LamA2D(k)
          LamAve(KDatBlock)=LamAveD(k)
          LamRat(KDatBlock)=LamRatD(k)
          if(CrwLogicQuest(nCrwDouble)) then
            call FeQuestRealEdwOpen(nEdwWL1,LamA1(KDatBlock),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwWL2,LamA2(KDatBlock),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwRat,LamRat(KDatBlock),.false.,
     1                              .false.)
          else
            call FeQuestRealEdwOpen(nEdwWL,LamA1(KDatBlock),.false.,
     1                              .false.)
          endif
        endif
c        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(NAlfa(KDatBlock).gt.1) then
          call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
          call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
          call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
          LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1               LamA2(KDatBlock)*LamRat(KDatBlock))/
     2               (1.+LamRat(KDatBlock))
        else
          call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
          LamAve(KDatBlock)=LamA1(KDatBlock)
        endif
        AngleMonPom=AngleMon(KDatBlock)
        call CrlCalcMonAngle(AngleMonPom,LamAve(KDatBlock),ichp)
        if(ichp.ne.0) go to 9999
        AngleMon(KDatBlock)=AngleMonPom
        call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(KDatBlock),
     1                          .false.,.false.)
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwWL) then
        call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
        LamAve(KDatBlock)=LamA1(KDatBlock)
      else if(CheckType.eq.EventEdw.and.(CheckNumber.eq.nEdwWL1.or.
     1        CheckNumber.eq.nEdwWL2)) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
        call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
        LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1                     LamRat(KDatBlock)*LamA2(KDatBlock))/
     2                     (1.+LamRat(KDatBlock))
      endif
2500  if(Radiation(KDatBlock).ne.RadiationOld) then
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          call FeQuestButtonOff(nButtTube)
          call FeQuestCrwOpen(nCrwDouble,NAlfa(KDatBlock).gt.1)
          if(NAlfa(KDatBlock).gt.1) then
            if(EdwStateQuest(nEdwWL1).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL)
              call FeQuestRealEdwOpen(nEdwWL1,LamA1(KDatBlock),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2(KDatBlock),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRat(KDatBlock),.false.,
     1                                .false.)
            endif
          else
            if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL1)
              call FeQuestEdwClose(nEdwWL2)
              call FeQuestEdwClose(nEdwRat)
              call FeQuestRealEdwOpen(nEdwWL,LamA1(KDatBlock),.false.,
     1                                .false.)
            endif
          endif
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
            call FeQuestEdwClose(nEdwWL1)
            call FeQuestEdwClose(nEdwWL2)
            call FeQuestEdwClose(nEdwRat)
            call FeQuestRealEdwOpen(nEdwWL,LamAve(KDatBlock),.false.,
     1                              .false.)
          endif
        endif
      endif
      if(Radiation(KDatBlock).ne.RadiationOld.or.
     1   LPFactor(KDatBlock).ne.LPFactorOld) then
        if(Radiation(KDatBlock).eq.NeutronRadiation.or.
     1     Radiation(KDatBlock).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            LPFactor(KDatBlock)=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(LPFactor(KDatBlock).eq.PolarizedMonoPer.or.
     1     LPFactor(KDatBlock).eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(KDatBlock),
     1                              .false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(KDatBlock),
     1                              .false.,.false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(LPFactor(KDatBlock).eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(KDatBlock),
     1                              .false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,AlphaGMon(KDatBlock),
     1                              .false.,.false.)
            call FeQuestRealEdwOpen(nEdwGBeta,BetaGMon(KDatBlock),
     1                              .false.,.false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
      RadiationOld=Radiation(KDatBlock)
      LPFactorOld=LPFactor(KDatBlock)
c      go to 9999
      entry PwdOptionsRadiationUpdate
      if(Radiation(KDatBlock).eq.NeutronRadiation) NAlfa(KDatBlock)=1
      if(NAlfa(KDatBlock).gt.1) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
        call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
        LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1                     LamRat(KDatBlock)*LamA2(KDatBlock))/
     2                     (1.+LamRat(KDatBlock))
      else
        call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
        LamAve(KDatBlock)=LamA1(KDatBlock)
      endif
      klam(KDatBlock)=LocateInArray(LamAve(KDatBlock),LamAveD,7,.0001)
      if(klam(KDatBlock).le.0)
     1  klam(KDatBlock)=LocateInArray(LamA1(KDatBlock),LamA1D,7,.0001)
      LorentzToCrySize(KDatBlock)=10.*LamAve(KDatBlock)/ToRad
      GaussToCrySize(KDatBlock)=sqrt8ln2*LorentzToCrySize(KDatBlock)
      if(LpFactor(KDatBlock).ne.PolarizedCircular.and.
     1   LpFactor(KDatBlock).ne.PolarizedLinear) then
        call FeQuestRealFromEdw(nEdwPerfMon,FractPerfMon(KDatBlock))
        if(LpFactor(KDatBlock).eq.PolarizedGuinier) then
          call FeQuestRealFromEdw(nEdwGAlpha,AlphaGMon(KDatBlock))
          call FeQuestRealFromEdw(nEdwGBeta,BetaGMon(KDatBlock))
        else
          call FeQuestRealFromEdw(nEdwMonAngle,AngleMon(KDatBlock))
        endif
      endif
      LamAvePwd(KDatBlock)=LamAve(KDatBlock)
      LamPwd(1,KDatBlock)=LamA1(KDatBlock)
      LamPwd(2,KDatBlock)=LamA2(KDatBlock)
9999  return
      end
