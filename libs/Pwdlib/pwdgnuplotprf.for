      subroutine PwdGnuplotPrf
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer th2min,th2max,Ymin,Ymax
      character*256 Veta
      character*80  t80
      call PwdPrfNacti(0)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_patt.tmp','formatted','unknown')
      th2min=99999
      th2max=0
      Ymin=0
      Ymax=0
      do i=1,Npnts
        th2min=min(th2min,nint(XPwd(i)))
        th2max=max(th2max,nint(XPwd(i)))
!        Ymin=min(Ymin,nint(YoPwd(i)),nint(YcPwd(i)))
        Ymax=max(Ymax,nint(YoPwd(i)),nint(YcPwd(i)))
        write(ln,*) XPwd(i),YoPwd(i),YcPwd(i),YoPwd(i)-YcPwd(i)
      enddo
      call CloseIfOpened(ln)
      ich=0
      id=NextQuestId()
      xqd=350.
      il=3
      Veta='Define parameters for plot window'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='Procedure designed by Joachim Breternitz'
      call FeQuestLblMake(id,xqd*.5,-10*il+3,Veta,'C','N')
      t80='2th'
      dpom=100.
      do i=1,2
        il=il+1
        tpom=5.
        xpom=tpom+50.
        do j=1,2
          if(j.eq.1) then
            Veta=t80(:idel(t80))//'(min)'
            if(i.eq.1) then
              ipom=th2min
            else
              ipom=Ymin
            endif
          else if(j.eq.2) then
            Veta=t80(:idel(t80))//'(max)'
            if(i.eq.1) then
              ipom=th2max
            else
              ipom=Ymax
            endif
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          call FeQuestIntEdwOpen(EdwLastMade,ipom,.false.)
          if(i.eq.1.and.j.eq.1) nEdwFirst=EdwLastMade
          tpom=tpom+xqd*.5
          xpom=tpom+50.
        enddo
        t80='I'
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=1,2
          do j=1,2
            call FeQuestIntFromEdw(nEdw,ipom)
            if(i.eq.1) then
              if(j.eq.1) then
                th2min=ipom
              else
                th2max=ipom
              endif
            else
              if(j.eq.1) then
                Ymin=ipom
              else
                Ymax=ipom
              endif
            endif
            nEdw=nEdw+1
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        call OpenFile(ln,Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp',
     1                'formatted','unknown')
        do j=1,NBragg,KAlpha2+1
          if(KlicArr(j)/100+1.eq.i)
     1      write(ln,*) BraggArr(j),i*2,' 0.75',indArr(1:3,j)
        enddo
        call CloseIfOpened(ln)
      enddo
      call OpenFile(ln,fln(:ifln)//'.gnu','formatted','unknown')
      write(ln,FormA) 'unset key'
      write(Veta,100) 'x',th2min,th2max
      call Zhusti(Veta)
      write(ln,FormA) 'set '//Veta(:idel(Veta))
      write(Veta,100) 'y',Ymin,Ymax
      call Zhusti(Veta)
      write(ln,FormA) 'set '//Veta(:idel(Veta))
      write(ln,FormA) 'set y2range [1:100]'
      write(ln,FormA) 'set mxtics 5'
      write(ln,FormA) 'set xtics 10'
      write(ln,FormA) 'set xtics out nomirror'
      write(ln,FormA) 'set ytics out nomirror'
      write(ln,FormA) 'set mytics 5'
      write(ln,FormA) 'set xlabel ''Scattering Angle (2{/Symbol q})'''
      write(ln,FormA) 'set ylabel ''Intensity'''
      write(ln,FormA) 'set terminal postscript enhanced color solid '//
     1                '''Arial'' landscape'
      write(ln,FormA) 'set output "'//fln(:ifln)//'_plot.eps"'
      write(ln,FormA) 'set encoding iso_8859_15'
      write(ln,FormA) 'set bars small'
      Veta=fln(:ifln)//'_patt.tmp'
      write(ln,FormA,advance='NO')
     1  'plot "'//Veta(:idel(Veta))//
     2  '" using 1:2 with points lc 000 ps 0.5,'
      write(ln,FormA,advance='NO')
     1  ' "'//Veta(:idel(Veta))//
     2  '" using 1:3 with lines lt 1 lw 2,'
      write(ln,FormA,advance='NO')
     1  ' "'//Veta(:idel(Veta))//
     2  '" using 1:4 with lines lc 025 lw 2,'
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        write(ln,'(a,'' using 1:2:3 axes x1y2'',a)',advance='NO')
     1    ' "'//Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp"',
     2    ' with errorbars ls -1,'
      enddo
      write(ln,FormA)
      write(ln,FormA) 'exit'
      call CloseIfOpened(ln)
      call FeSystem(CallGnuplot(:idel(CallGnuplot))//' '//
     1              fln(:ifln)//'.gnu')
      Veta=' -dSAFER -dBATCH -dNOPAUSE -dFIXEDMEDIA -dEPSCrop '//
     1     '-sDEVICE=png16m -r600x600 -sOutputFile='//
     2     '"'//fln(:ifln)//'_plot.png" "'//fln(:ifln)//'_plot.eps"'
     3
      call FeSystem(CallGS(:idel(CallGS))//Veta(:idel(Veta)))
      NInfo=3
      TextInfo(1)='The powder profile files:'
      TextInfo(2)='"'//fln(:ifln)//'_plot.eps" and "'//fln(:ifln)//
     1            '_plot.png"'
      TextInfo(3)='were created.'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
9999  call DeleteFile(fln(:ifln)//'_patt.tmp')
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        call DeleteFile(Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp')
      enddo
      call DeleteFile(fln(:ifln)//'.gnu')
      return
100   format(a1,'range [',i15,':',i15,']')
      end
