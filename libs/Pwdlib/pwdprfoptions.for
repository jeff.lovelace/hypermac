      subroutine PwdPrfOptions(ich,JenomNastav,menilFile,ktFile,istt)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension nCrwKresliLecos(7),nEdwA(5)
      integer EdwStateQuest
      logical JenomNastav,nechT,CrwLogicQuest,ExistFile,menilFile,lpom
      logical :: UzTadyByl = .false.
      character*80 ch80
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      menilFile=.false.
      nechT=.false.
      nButtDefaults=0
      if(JenomNastav) then
        ZPrf=ExistFile(fln(:ifln)//'.prf')
        go to 2100
      endif
      xqd=500.
      il=12
      if(maxNDimI.gt.0) il=il+1
      if(maxNComp.gt.1) il=il+2
      if(NPhase.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Options',0,LightGray,0,0)
      il=1
      offset=5.
      ch80='Input file:'
      call FeQuestLblMake(id,offset,il,ch80,'L','N')
      xpom=offset+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80=fln(:idel(fln))//'.m90'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,1)
      nCrwm90=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80=fln(:idel(fln))//'.prf'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,1)
      nCrwPrf=CrwLastMade
      if(.not.ExistFile(fln(:ifln)//'.prf')) then
        call FeQuestLblDisable(LblLastMade)
        call FeQuestCrwDisable(nCrwm90)
        call FeQuestCrwDisable(nCrwPrf)
      endif
      il=il+1
      xpom=offset
      tpom=xpom+CrwYd+5.
      ch80='Show title'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,0)
      nCrwShowTitle=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80='Centered'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,0,2)
      nCrwCent=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80='Right aligned'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,0,2)
      nCrwRight=CrwLastMade
      il=il+1
      tpom=offset
      ch80='Title:'
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      dpom=250.
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwTitle=EdwLastMade
      call FeQuestStringEdwOpen(nEdwTitle,plotTitle)
      xpom=offset
      tpom=xpom+CrwYd+5.
      ilp=il
      ch80='Plot labels'
      ichk=0
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,
     1                      ichk,0)
        if(i.eq.1) then
          nCrwLab=CrwLastMade
          ch80='Plot max. count'
        else if(i.eq.2) then
          nCrwCounts=CrwLastMade
          ch80='Plot Bragg positions'
          il=il+2
          ichk=1
          dpom=FeTxLength(ch80)
        else if(i.eq.3) then
          nCrwBragg=CrwLastMade
        endif
      enddo
      il=ilp
      xpom=tpom+dpom+25.
      tpom=xpom+CrwYd+5.
      ch80='Plot %observed line with color:'
      xpomp=tpom+FeTxLengthUnder(ch80)+60.
      dpom=xqd-xpomp-5.
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,
     1                      1,0)
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,' ','L',dpom,EdwYd,
     1                          1)
        if(i.eq.1) then
          nCrwObsLine=CrwLastMade
          nRolMenuColorObs=RolMenuLastMade
          ch80='Plot %calculated line with color:'
        else if(i.eq.2) then
          nCrwCalcLine=CrwLastMade
          nRolMenuColorCalc=RolMenuLastMade
          ch80='Plot %difference curve with color:'
        else if(i.eq.3) then
          nCrwDifLine=CrwLastMade
          nRolMenuColorDif=RolMenuLastMade
        endif
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=xpom
      if(maxNDimI.le.0) then
        ch80='%Reflections with color:'
      else
        if(maxNComp.le.1) then
          ch80='%Main reflections with color:'
        else
          ch80='Main reflections of subsystem #%1 with color:'
        endif
      endif
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,1)
      nRolMenuColorMain1=RolMenuLastMade
      if(maxNComp.gt.1) then
        il=il+1
        ch80='Main reflection of subsystem #2 with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorMain2=RolMenuLastMade
        il=il+1
        ch80='Co%mmon reflections with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorMain12=RolMenuLastMade
      else
        nRolMenuColorMain2=0
        nRolMenuColorMain12=0
      endif
      if(maxNDimI.gt.0) then
        il=il+1
        ch80='Satellites with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorSat=RolMenuLastMade
      else
        nRolMenuColorSat=0
      endif
      if(NPhase.gt.1) then
        il=il+1
        ch80='Bragg labels as phase names'
        xpom=offset
        tpom=xpom+CrwYd+5.
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,0,
     1                        3)
          if(i.eq.1) then
            nCrwUsePhaseNames=CrwLastMade
            ch80='Bragg labels in form (a), (b), ...'
            xpom=tpom+FeTxLengthUnder(ch80)+10.
            tpom=xpom+CrwYd+5.
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.BraggLabel.eq.0)
        enddo
      else
        nCrwUsePhaseNames=0
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      ch80='%Use linear scale for intensity'
      xpom=offset
      tpom=xpom+CrwYd+5.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,0,4)
        if(i.eq.1) then
          nCrwUseLinearScale=CrwLastMade
          ch80='Use square root scale for intensity'
          xpom=tpom+FeTxLengthUnder(ch80)+10.
          tpom=xpom+CrwYd+5.
        endif
      enddo
      il=il+1
      xpom=offset
      tpom=xpom+CrwYd+5.
      ch80='Shift ori%gin of the y axis'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,0)
      nCrwYOriginShift=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,YOriginShift.gt.0.)
      tpom=tpom+FeTxLengthUnder(ch80)+4.
      ch80='to =>'
      call FeQuestLblMake(id,tpom,il,ch80,'L','N')
      nLblTo=LblLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+3.
      dpom=50.
      tpom=xpom+dpom+5.
      ch80=' * min(I(obs)),I(calc)) - factor from the interval <0,1>'
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,1)
      nEdwYOriginShift=EdwLastMade
      il=il+1
      ch80='%Set defaults'
      dpom=FeTxLength(ch80)+50.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,ch80)
      nButtDefaults=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call FeQuestCrwOpen(nCrwShowTitle,KresliTitle)
      call FeQuestCrwOpen(nCrwCent,CenteredTitle)
      call FeQuestCrwOpen(nCrwRight,.not.CenteredTitle)
1000  nCrw=nCrwLab
      do i=1,4
        if(i.eq.1) then
          lpom=KresliLab
        else if(i.eq.2) then
          lpom=KresliMaxCounts
        else if(i.eq.3) then
          lpom=KresliBragg
        else if(i.eq.4) then
          lpom=KresliDifLine
        endif
        call FeQuestCrwOpen(nCrw,lpom)
        nCrw=nCrw+1
      enddo
      nCrw=nCrwObsLine
      nRolMenu=nRolMenuColorObs
      lpom=KresliObsLine
      j=ColorObs
      do i=1,3
        call FeQuestCrwOpen(nCrw,lpom)
        k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenu,ColorNames,PocetBarev,k)
        if(i.eq.1) then
          lpom=KresliCalcLine
          j=ColorCalc
        else if(i.eq.2) then
          lpom=KresliDifLine
          j=ColorDif
        endif
        nCrw=nCrw+1
        nRolMenu=nRolMenu+1
      enddo
      k=max(LocateInIntArray(ColorMain1,ColorNumbers,PocetBarev),1)
      call FeQuestRolMenuOpen(nRolMenuColorMain1,ColorNames,PocetBarev,
     1                        k)
      if(nRolMenuColorMain2.gt.0) then
        k=max(LocateInIntArray(ColorMain2,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain2,ColorNames,
     1                          PocetBarev,k)
        k=max(LocateInIntArray(ColorMain12,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain12,ColorNames,
     1                          PocetBarev,k)
      endif
      if(nRolMenuColorSat.gt.0) then
        k=max(LocateInIntArray(ColorSat,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorSat,ColorNames,PocetBarev,
     1                          k)
      endif
      if(kType.eq.0) then
        call FeQuestCrwDisable(nCrwDifLine)
        call FeQuestRolMenuDisable(nRolMenuColorDif)
        call FeQuestCrwDisable(nCrwCalcLine)
        call FeQuestRolMenuDisable(nRolMenuColorCalc)
      endif
      if(NBragg.le.0) call FeQuestCrwDisable(nCrwBragg)
      call FeReleaseOutPut
      if(ExistFile(fln(:ifln)//'.prf')) then
        call FeQuestCrwOpen(nCrwm90,.not.ZPrf)
        call FeQuestCrwOpen(nCrwPrf,ZPrf)
      endif
      call FeReleaseOutPut
      nCrw=nCrwUseLinearScale
      do i=1,2
        call FeQuestCrwOpen(nCrw,i.eq.2.eqv.SqrtScale)
        nCrw=nCrw+1
      enddo
1500  nCrw=nCrwObsLine
      nRolMenu=nRolMenuColorObs
      do i=1,3
        if(CrwLogicQuest(nCrw)) then
          if(i.eq.1) then
            j=ColorObs
          else if(i.eq.2) then
            j=ColorCalc
          else if(i.eq.3) then
            j=ColorDif
          endif
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenu,ColorNames,PocetBarev,k)
        else
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenu))
          if(i.eq.1) then
            ColorObs=j
          else if(i.eq.2) then
            ColorCalc=j
          else
            ColorDif=j
          endif
          call FeQuestRolMenuDisable(nRolMenu)
        endif
        nCrw=nCrw+1
        nRolMenu=nRolMenu+1
      enddo
      if(KresliTitle) then
        if(EdwStateQuest(nEdwTitle).ne.EdwOpened) then
          call FeQuestStringEdwOpen(nEdwTitle,plotTitle)
          call FeQuestCrwOpen(nCrwCent,CenteredTitle)
          call FeQuestCrwOpen(nCrwRight,.not.CenteredTitle)
        endif
      else
        if(EdwStateQuest(nEdwTitle).ne.EdwDisabled) then
          CenteredTitle=CrwLogicQuest(nCrwCent)
          call FeQuestEdwDisable(nEdwTitle)
          call FeQuestCrwDisable(nCrwCent)
          call FeQuestCrwDisable(nCrwRight)
        endif
      endif
      if(KresliBragg) then
        j=ColorMain1
        k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain1,ColorNames,
     1                          PocetBarev,k)
        if(nRolMenuColorMain2.gt.0) then
          j=ColorMain2
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorMain2,ColorNames,
     1                            PocetBarev,k)
          j=ColorMain12
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorMain12,ColorNames,
     1                            PocetBarev,k)
        endif
        if(nRolMenuColorSat.gt.0) then
          j=ColorSat
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorSat,ColorNames,
     1                            PocetBarev,k)
        endif
        if(NPhase.gt.1) then
          nCrw=nCrwUsePhaseNames
          do i=1,2
            call FeQuestCrwOpen(nCrw,i.eq.1.eqv.BraggLabel.eq.0)
            nCrw=nCrw+1
          enddo
        endif
      else
        j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain1))
        call FeQuestRolMenuDisable(nRolMenuColorMain1)
        if(nRolMenuColorMain2.gt.0) then
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain1))
          call FeQuestRolMenuDisable(nRolMenuColorMain1)
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain12))
          call FeQuestRolMenuDisable(nRolMenuColorMain12)
        endif
        if(nRolMenuColorSat.gt.0) then
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorSat))
          call FeQuestRolMenuDisable(nRolMenuColorSat)
        endif
        if(NPhase.gt.1) then
          nCrw=nCrwUsePhaseNames
          do i=1,2
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
        endif
      endif
      if(CrwLogicQuest(nCrwYOriginShift)) then
        call FeQuestLblOn(nLblTo)
        if(YOriginShift.le.0.) YOriginShift=1.
        call FeQuestRealEdwOpen(nEdwYOriginShift,YOriginShift,.false.,
     1                          .false.)
      else
        call FeQuestLblOff(nLblTo)
        if(EdwStateQuest(nEdwYOriginShift).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwYOriginShift,YOriginShift)
          call FeQuestEdwClose(nEdwYOriginShift)
        endif
      endif
2000  call FeQuestEvent(id,ich)
      nechT=.false.
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwm90.or.CheckNumber.eq.nCrwPrf)) then
        ZPrf=CrwLogicQuest(nCrwPrf)
        if(CrwLogicQuest(nCrwm90))then
          ktFileNew=1
        else
          ktFileNew=0
        endif
        if(ktFileNew.eq.ktFile) go to 2000
        ktFile=ktFileNew
        menilFile=.true.
        call PwdPrfNacti(ktFile)
        CheckType=EventButton
        CheckNumber=nButtDefaults
      endif
2100  if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefaults)
     1   .or.JenomNastav) then
        KresliObsLine=kType.eq.0.or.kType.eq.2
        KresliCalcLine=kType.ne.0
        KresliBragg=NBragg.gt.0
        KresliDifLine=kType.ne.0
        if(.not.UzTadyByl.or..not.JenomNastav) then
          ColorObs=White
          ColorCalc=White
          ColorDif=White
          ColorCalc=Yellow
          ColorMain1=White
          ColorMain2=Yellow
          ColorMain12=Red
          ColorSat=Green
          KresliLab=.true.
          KresliMaxCounts=.true.
          KresliTitle=.false.
          CenteredTitle=.false.
        endif
        if(.not.nechT) plotTitle=StructureName
        shrinkX=1.1
        shrinkY=1.1
        expandX=1.1
        expandY=1.1
        stepIni=1.
        distLabel=6.
        SqrtScale=.false.
        YOriginShift=0.
        if(JenomNastav) then
          go to 9900
        else
          go to 1000
        endif
      endif
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwObsLine.or.CheckNumber.eq.nCrwCalcLine.or.
     2    CheckNumber.eq.nCrwDifLine)) then
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwShowTitle)
     1  then
        KresliTitle=CrwLogicQuest(nCrwShowTitle)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwBragg) then
        KresliBragg=CrwLogicQuest(nCrwBragg)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwYOriginShift)
     1  then
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwYOriginShift)
     1  then
        call FeQuestRealFromEdw(nEdwYOriginShift,YOriginShift)
        YOriginShift=min(YOriginShift,1.)
        YOriginShift=max(YOriginShift,0.)
        call FeQuestRealEdwOpen(nEdwYOriginShift,YOriginShift,.false.,
     1                          .false.)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorObs) then
        ColorObs=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorDif) then
        ColorDif=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorCalc) then
        ColorCalc=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain1) then
        ColorMain1=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain2) then
        ColorMain2=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain12) then
        ColorMain12=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorSat) then
        ColorSat=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventGlobal) then
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0) then
        if(.not.CrwLogicQuest(nCrwYOriginShift)) YOriginShift=0.
        SqrtScale=.not.CrwLogicQuest(nCrwUseLinearScale)
        KresliTitle=CrwLogicQuest(nCrwShowTitle)
        if(KresliTitle) then
          plotTitle=EdwStringQuest(nEdwTitle)
          CenteredTitle=CrwLogicQuest(nCrwCent)
        endif
        nCrw=nCrwLab
        do i=1,6
          lpom=CrwLogicQuest(nCrw)
          if(i.eq.1) then
            KresliLab=lpom
          else if(i.eq.2) then
            KresliMaxCounts=lpom
          else if(i.eq.3) then
            KresliBragg=lpom
          else if(i.eq.4) then
            KresliObsLine=lpom
          else if(i.eq.5) then
            KresliCalcLine=lpom
          else if(i.eq.6) then
            KresliDifLine=lpom
          endif
          nCrw=nCrw+1
        enddo
        if(ExistFile(fln(:ifln)//'.prf'))
     1    ZPrf=CrwLogicQuest(nCrwPrf)
        if(nCrwUsePhaseNames.gt.0) then
          if(CrwLogicQuest(nCrwUsePhaseNames)) then
            BraggLabel=0
          else
            BraggLabel=1
          endif
        endif
      endif
9000  call FeQuestRemove(id)
9900  ColorSkip=Gray
      if(ColorObs.eq.ColorSkip.or.ColorCalc.eq.ColorSkip) then
        ColorSkip=Yellow
        if(ColorObs.eq.ColorSkip.or.ColorCalc.eq.ColorSkip)
     1    ColorSkip=Blue
      endif
      UzTadyByl=.true.
9999  return
      end
