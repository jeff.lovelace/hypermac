      function PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if((Eps0-EpsA).gt.0.) then
        PwdAxialDivF2=-1.+PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                    sqrt(Eps0-EpsA)
      else
        PwdAxialDivF2=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF2=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF2
      else
        PwdAxialDivF2=0.
      endif
9999  return
      end
