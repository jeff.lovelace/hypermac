      subroutine PwdSetFundamentalParameters
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      RadPrim(KDatBlock)=173.
      RadSec(KDatBlock)=173.
      AsymPwd(1,KDatBlock)=.2
      if(PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS) then
        AsymPwd(2,KDatBlock)=10.
        KUseDS(KDatBlock)=2
      else
        AsymPwd(2,KDatBlock)=1.
        KUseDS(KDatBlock)=1
      endif
      AsymPwd(3,KDatBlock)=12.
      AsymPwd(4,KDatBlock)=15.
      AsymPwd(5,KDatBlock)=12.
      AsymPwd(6,KDatBlock)=5.1
      AsymPwd(7,KDatBlock)=5.1
      KUseRSW(KDatBlock)=1
      KUsePSoll(KDatBlock)=1
      KUseSSoll(KDatBlock)=1
      return
      end
