      subroutine PwdBasicDefInsFile(Klic,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 EdwStringQuest,Veta
      logical ExistFile
      WizardMode=.false.
      id=NextQuestId()
      xqd=400.
      if(Klic.eq.1) then
        il=6
      else
        il=3
      endif
      Veta='Define instrument parameter file:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
      il=1
      Veta='%Browse'
      dpomb=FeTxLengthUnder(Veta)+10.
      xpomb=xqd-dpomb-5.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtBrowsePar=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=5.
      dpom=xpomb-xpom-10.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwFilePar=EdwLastMade
      if(InsParFileRefBlock(KRefBlock).eq.' ') then
        do i=KRefBlock-1,1,-1
          if(InsParFileRefBlock(i).ne.' ') then
            InsParFileRefBlock(KRefBlock)=InsParFileRefBlock(i)
            exit
          endif
        enddo
      endif
      call FeQuestStringEdwOpen(EdwLastMade,
     1                          InsParFileRefBlock(KRefBlock))
      if(Klic.eq.1) then
        call FeFillTextInfo('pwdbasictof1.txt',0)
      else
        call FeFillTextInfo('pwdbasictof2.txt',0)
      endif
      tpom=5.
      ilp=il
      il=-10*(il+1)+2
      do i=1,NInfo
        call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
        il=il-6
      enddo
      if(Klic.eq.1) then
        il=ilp+3
        Veta='Define incident spectral file:'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        il=il+1
        Veta='Bro%wse'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
        nButtBrowseSpec=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        nEdwFileSpec=EdwLastMade
        if(IncSpectFileRefBlock(KRefBlock).eq.' ') then
          do i=KRefBlock-1,1,-1
            if(IncSpectFileRefBlock(i).ne.' ') then
              IncSpectFileRefBlock(KRefBlock)=IncSpectFileRefBlock(i)
              exit
            endif
          enddo
        endif
        call FeQuestStringEdwOpen(EdwLastMade,
     1                            IncSpectFileRefBlock(KRefBlock))
        call FeFillTextInfo('pwdbasictof3.txt',0)
        il=-10*(il+1)+2
        do i=1,NInfo
          call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
          il=il-6
        enddo
      else
        nButtBrowseSpec=0
        nEdwFileSpec=0
      endif
1150  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK)
     1  then
        InsParFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFilePar)
        if(nEdwFileSpec.gt.0)
     1    IncSpectFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileSpec)
        if(.not.ExistFile(InsParFileRefBlock(KRefBlock)).and.
     1     InsParFileRefBlock(KRefBlock).ne.' ') then
          call FeChybne(-1.,-1.,'the file "'//
     1      InsParFileRefBlock(KRefBlock)
     2        (:idel(InsParFileRefBlock(KRefBlock)))//
     3      '" does not exist.',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFilePar
          go to 1150
        endif
        if(.not.ExistFile(IncSpectFileRefBlock(KRefBlock)).and.
     1     IncSpectFileRefBlock(KRefBlock).ne.' ') then
          call FeChybne(-1.,-1.,'the file "'//
     1      IncSpectFileRefBlock(KRefBlock)
     2        (:idel(IncSpectFileRefBlock(KRefBlock)))//
     3      '" does not exist.',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFilePar
          go to 1150
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBrowsePar) then
        Veta=EdwStringQuest(nEdwFilePar)
        call FeFileManager('Browse for instrument parameter file',
     1                      Veta,'*.irf *.prm',0,.true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwFilePar,Veta)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBrowseSpec) then
        Veta=EdwStringQuest(nEdwFileSpec)
        call FeFileManager('Browse for instrument parameter file',
     1                      Veta,'*.gsa *.gda',0,.true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwFileSpec,Veta)
        go to 1150
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1150
      endif
      call FeQuestRemove(id)
      WizardMode=.true.
      if(ich.ne.0) then
        InsParFileRefBlock(KRefBlock)=' '
        IncSpectFileRefBlock(KRefBlock)=' '
      endif
      return
      end
