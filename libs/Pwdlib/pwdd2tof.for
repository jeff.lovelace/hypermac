      function PwdD2TOF(dd)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(TOFInD) then
        PwdD2TOF=dd
      else
        if(KUseTOFJason(KDatBlock).le.0) then
          PwdD2TOF=(ShiftPwd(2,KDatBlock)*dd+
     1              ShiftPwd(3,KDatBlock)*dd**2+
     1              ShiftPwd(1,KDatBlock))*.001
        else
          fn=.5*erfc(ShiftPwd(6,KDatBlock)*
     1       (ShiftPwd(7,KDatBlock)-1./dd))
          TE=ShiftPwd(4,KDatBlock)+ShiftPwd(5,KDatBlock)*dd
          TT=ShiftPwd(1,KDatBlock)+ShiftPwd(2,KDatBlock)*dd
     1      -ShiftPwd(3,KDatBlock)/dd
          PwdD2TOF=(fn*TE+(1.-fn)*TT)*.001
        endif
      endif
      return
      end
