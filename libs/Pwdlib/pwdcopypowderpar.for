      subroutine PwdCopyPowderPar(is,it)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      KPhase=it
      if(is.le.0) then
        KStrain(it,KDatBlock)=0
        KAsym(KDatBlock)=0
        NAsym(KDatBlock)=0
        KPref(it,KDatBlock)=0
        KProfPwd(it,KDatBlock)=1
        PCutOff(it,KDatBlock)=8.
        SPref(it,KDatBlock)=2.
        call CopyVek(CellPar(1,1,it),CellPwd(1,it),6)
        call CopyVek(qu(1,1,1,it),QuPwd(1,1,it),3*NDimI(KPhase))
        call SetRealArrayTo(GaussPwd(1,it,KDatBlock),4,0.)
        if(DataType(KDatBlock).eq.-2) then
          GaussPwd(2,it,KDatBlock)=1.
        else
          GaussPwd(3,it,KDatBlock)=5.
        endif
      else
        call CopyVek(GaussPwd(1,is,KDatBlock),
     1               GaussPwd(1,it,KDatBlock),4)
        call CopyVek(LorentzPwd(1,is,KDatBlock),
     1               LorentzPwd(1,it,KDatBlock),5)
        PCutOff(it,KDatBlock)=PCutOff(is,KDatBlock)
      endif
      return
      end
