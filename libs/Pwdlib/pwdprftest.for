      subroutine PwdPrfTest
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Wx(-5000:5000),Wint(-5000:5000),WDiv(-5000:5000)
      character*80 SvFile
      character*80 t80
      integer Color
      logical CrwLogicQuest
      Tiskne=.false.
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Continue')
      nButtContinue=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Print')
      nButtPrint=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      xomnp=-.2
      xomxp= .2
      n=1000
      deltap=(xomxp-xomnp)/float(n)
      xomn=xomnp
      xomx=xomxp
      nn=n
      Th2=120.
      Rad=215.
      DelR=12.
      DelX=12.
      DelS=25.
      DeltaD=-1.
      DeltaI=-1.
      ic=0
1500  idp=NextQuestId()
      xqd=220.
      il=5
      call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define new values',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      t80='%2Theta'
      xpom=tpom+30.
      dpom=30.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdw2Th=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2,.false.,.false.)
      il=il+1
      t80='%X-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelX=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelX,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      il=1
      t80='R%adius'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Rad,.false.,.false.)
      il=il+1
      t80='%S-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelS=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelS,.false.,.false.)
      il=2
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%R-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelR=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelR,.false.,.false.)
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+3.
      t80='Use %primary-beam    Soller slit'
      call FeQuestCrwMake(idp,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,1,0)
      nCrwSollerI=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,DeltaI.gt.0.)
      il=il+1
      t80='Use %diffracted-beam Soller slit'
      call FeQuestCrwMake(idp,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,1,0)
      nCrwSollerD=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,DeltaD.gt.0.)
      il=il-1
      tpom=tpom+FeTxLengthUnder(t80)+2.
      t80='=>'
      xpom=tpom+FeTxLengthUnder(t80)+2.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwSollerI=EdwLastMade
      il=il+1
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwSollerD=EdwLastMade
1600  if(CrwLogicQuest(nCrwSollerI)) then
        if(DeltaI.lt.0.) DeltaI=1.
        call FeQuestRealEdwOpen(nEdwSollerI,DeltaI,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwSollerI)
      endif
      if(CrwLogicQuest(nCrwSollerD)) then
        if(DeltaD.lt.0.) DeltaD=1.
        call FeQuestRealEdwOpen(nEdwSollerD,DeltaD,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwSollerD)
      endif
      call FeQuestEvent(idp,ich)
      if(CheckType.eq.EventCrw) then
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdw2Th,Th2)
        call FeQuestRealFromEdw(nEdwRadius,Rad)
        call FeQuestRealFromEdw(nEdwDelX,DelX)
        call FeQuestRealFromEdw(nEdwDelS,DelS)
        call FeQuestRealFromEdw(nEdwDelR,DelR)
        if(CrwLogicQuest(nCrwSollerI)) then
          call FeQuestRealFromEdw(nEdwSollerI,DeltaI)
        else
          DeltaI=-1.
        endif
        if(CrwLogicQuest(nCrwSollerD)) then
          call FeQuestRealFromEdw(nEdwSollerD,DeltaD)
        else
          DeltaD=-1.
        endif
      endif
      call FeQuestRemove(idp)
      if(ich.ne.0) go to 5500
      do i=-n/2,n/2
        Wx(i)=Th2+float(i)*deltap
      enddo
      call PwdAxialDiv(Th2,DelX,DelS,DelR,Rad,DeltaI*ToRad,.true.,
     1                 DeltaD*ToRad,.true.,
     2                 100,n+1,10.,deltap,Wdiv(-n/2))
      xomn=xomnp+th2
      xomx=xomxp+th2
      WIntMax=0.
      yomx=1.
      ic=ic+1
      if(mod(ic,3).eq.0) then
        Color=Yellow
      else if(mod(ic,3).eq.1) then
        Color=Red
      else if(mod(ic,3).eq.2) then
        Color=Green
      endif
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      if(ic.eq.1) call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      call FeXYPlot(Wx(-nn/2),Wint(-nn/2),nn,NormalLine,
     1              NormalPlotMode,Color)
      call FeReleaseOutput
      call FeDeferOutput
5300  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue.or.CheckNumber.eq.nButtPrint)
     1    then
          if(CheckNumber.eq.nButtPrint) then
            call FePrintPicture(ich)
            if(ich.ne.0) go to 5300
          endif
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 1500
        endif
      endif
      go to 5300
5400  xomn=-0.3
      xomx= 0.02
      yomn=-20.
      yomx= 20.
      call FeMakeAcFrame
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      nn=0
      Posun= -4.
      DelkaS=18.
      xpom=xomn
      dpom=(xomx-xomn)/float(n)
      do i=-n/2,n/2
        nn=nn+1
        Wx(i)=xpom
        WInt(i)= 50.*sqrt(-xpom)+Posun+DelkaS*.5
        WDiv(i)=-50.*sqrt(-xpom)+Posun+DelkaS*.5
        xpom=xpom+dpom
        if(xpom.gt.0.) then
          if(xpom.lt.dpom*.5) then
            xpom=0.
          else
            go to 5420
          endif
        endif
      enddo
5420  Color=White
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeXYPlot(Wx(-n/2),Wint(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        call FeXYPlot(Wx(-n/2),Wdiv(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        jk=20
        jk=1
        do j=1,jk
        nn=0
        xpom=xomn
        dpom=(xomx-xomn)/float(n)
        do i=-n/2,n/2
          nn=nn+1
          Wx(i)=xpom
          WInt(i)= 50.*sqrt(-xpom)+Posun-DelkaS*.5
          WDiv(i)=-50.*sqrt(-xpom)+Posun-DelkaS*.5
          xpom=xpom+dpom
          if(xpom.gt.0.) then
            if(xpom.lt.dpom*.5) then
              xpom=0.
            else
              go to 5440
            endif
          endif
        enddo
5440    Color=White
        call FeXYPlot(Wx(-n/2),Wint(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        call FeXYPlot(Wx(-n/2),Wdiv(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        enddo
        Wx(1)=xomn
        Wx(2)=xomx
        Wint(1)= 8.
        Wint(2)= 8.
        call FeXYPlot(Wx(1),Wint(1),2,DashedLine,NormalPlotMode,White)
        Wint(1)=-8.
        Wint(2)=-8.
        call FeXYPlot(Wx(1),Wint(1),2,DashedLine,NormalPlotMode,White)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
5450  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue) then
          go to 5500
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.ne.0) go to 5450
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 5500
        endif
      endif
      go to 5450
5500  call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
