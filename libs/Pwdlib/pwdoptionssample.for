      subroutine PwdOptionsSample
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*24 :: StAverage = 'Average over equivalents'
      character*5  :: StMir = 'mi*%r'
      logical CrwLogicQuest
      integer CrwStateQuest
      save nEdwPrefP,nEdwDirPref,nEdwTOFAbs,nEdwMir,nEdwRoughP,
     2     nCrwPrefP,nCrwTOFAbs,nCrwRoughP,nCrwKPrefFirst,nCrwKPrefLast,
     3     nCrwKRoughFirst,nCrwKRoughLast,nCrwPrefAve,nCrwKAbsorFirst,
     4     nCrwKAbsorLast,nCrwIllumFirst,nCrwIllumLast,nCrwFocus,
     5     nLblRough,nLblIllum,nLblTOFAbs,
     6     KPrefOld,KRoughOld,KAbsorNew
      save /PwdOptionsC/
      entry PwdOptionsSampleMake(id)
      ICrwGroup=0
      il=1
      call FeQuestLblMake(id,5.,il,'Preferred orientation','L','B')
      KPrefOld=KPref(KPhase,KDatBlock)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      ICrwGroup=ICrwGroup+1
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='%None'
        else if(i.eq.2) then
          Veta='%March-Dollase'
        else
          Veta='%Sasa-Uda'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      ICrwGroup)
        if(i.eq.1) nCrwKPrefFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.KPref(KPhase,KDatBlock))
      enddo
      nCrwKPrefLast=CrwLastMade
      j=IPrefPwd+IZdvihProf
      il=2
      Veta=lPrefPwd
      do i=1,2
        call NToString(i,Veta(5:))
        call FeMakeParEdwCrw(id,tpoma(i+1),xpoma(i+1),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                     PrefPwd(i,KPhase,KDatBlock),kiPwd(j),.false.)
        if(i.eq.1) then
          nEdwPrefP=nEdw
          nCrwPrefP=nCrw
        endif
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefNone) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        j=j+1
      enddo
      ilPref=il
      il=il+1
      xpref=tpoma(2)+FeTxLength(StAverage)+5.
      Veta='Direction'
      call FeQuestEdwMake(id,tpoma(2),il,xpref,il,Veta,'L',40.,EdwYd,0)
      nEdwDirPref=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,tpoma(2),il,xpref,il,StAverage,'L',CrwXd,
     1                    CrwYd,0,0)
      nCrwPrefAve=CrwLastMade
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        call FeQuestRealAEdwOpen(nEdwDirPref,
     1    DirPref(1,KPhase,KDatBlock),3,.false.,.false.)
        call FeQuestCrwOpen(nCrwPrefAve,SPref(KPhase,KDatBlock).ne.0)
      endif
      il=il+1
      call FeQuestLblMake(id,5.,il,'Absorption correction','L','B')
      if(isTOF) then
        il=il+1
        Veta='Use pseu%do-absorption correction'
        xpomp=5.
        tpom=xpomp+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpomp,il,Veta,'L',CrwXd,CrwgYd,
     1                      1,0)
        nCrwTOFAbsP=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KUseTOFAbs(KDatBlock).ne.0)
        tpom=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblTOFAbs=LblLastMade
        xpomp=tpom+FeTxLengthUnder(Veta)+10.
        call FeMakeParEdwCrw(id,tpom,xpomp,il,' ',nEdwTOFAbs,nCrwTOFAbs)
        if(CrwLogicQuest(nCrwTOFAbsP)) then
          j=ITOFAbsPwd+IZdvihRec
          call FeOpenParEdwCrw(nEdwTOFAbs,nCrwTOFAbs,'#keep#',
     1                         TOFAbsPwd(KDatBlock),kiPwd(j),.false.)
        else
          call FeQuestLblOff(nLblTOFAbs)
        endif
        nCrwKAbsorFirst=0
        nCrwKAbsorLast=0
        nEdwMir=0
        il=il+3
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        ICrwGroup=ICrwGroup+1
        do i=0,3
          if((PwdMethod(KDatBlock).eq.IdPwdMethodDS.and.
     1        (i.ne.IdPwdAbsorbNone.and.i.ne.IdPwdAbsorbCylinder)).or.
     2       ((PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS.or.
     3         PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS).and.
     4        (i.eq.IdPwdAbsorbCylinder.or.
     5         i.eq.IdPwdAbsorbTransmission))) cycle
          il=il+1
          if(i.eq.0) then
            Veta='N%one'
          else if(i.eq.1) then
            Veta='C%ylindrical sample'
          else if(i.eq.2) then
            Veta='Symmetrical tr%ansmission'
          else if(i.eq.3) then
            Veta='Symmetrical re%flection'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.0) nCrwKAbsorFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAbsor(KDatBlock))
        enddo
        nCrwKAbsorLast=CrwLastMade
        KAbsorNew=KAbsor(KDatBlock)
        tpom=tpom+FeTxLengthUnder(Veta)+20.
        if(KAbsor(KDatBlock).eq.1) then
          StMir(4:5)='%r'
        else
          StMir(4:5)='%t'
        endif
        xpom=tpom+FeTxLengthUnder(StMir)+20.
        call FeQuestEdwMake(id,tpom,il,xpom,il,StMir,'L',80.,EdwYd,1)
        nEdwMir=EdwLastMade
        if(KAbsor(KDatBlock).ne.IdPwdAbsorbNone) then
          call FeQuestRealEdwOpen(nEdwMir,MirPwd(KDatBlock),.false.,
     1                           .false.)
        else
          MirPwd(KDatBlock)=1.
        endif
        il=il+1
        nCrwTOFAbsP=0
        nCrwTOFAbs =0
        nEdwTOFAbs =0
        nLblTOFAbs= 0
      endif
      if(PwdMethod(KDatBlock).ne.IdPwdMethodDS) then
        call FeQuestLblMake(id,5.,il,
     1                      'Roughness for Bragg-Brentano geometry','L',
     2                      'B')
        nLblRough=LblLastMade
        if(KAbsor(KDatBlock).eq.1) call FeQuestLblOff(nLblRough)
        xpom=5.
        tpom=xpom+CrwgXd+10.
        KRoughOld=KRough(KDatBlock)
        ICrwGroup=ICrwGroup+1
        pom=0.
        do i=1,3
          il=il+1
          if(i.eq.1) then
            Veta='Non%e'
          else if(i.eq.2) then
            Veta='%Pitschke, Hermann & Matter'
          else
            Veta='S%uorti'
          endif
          pom=max(pom,FeTxLengthUnder(Veta))
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.1) nCrwKRoughFirst=CrwLastMade
          if(KAbsor(KDatBlock).ne.1)
     1      call FeQuestCrwOpen(CrwLastMade,i-1.eq.KRough(KDatBlock))
        enddo
        ilp=il
        nCrwKRoughLast=CrwLastMade
        j=IRoughPwd+IZdvihRec
        il=il-3
        Veta=lRoughPwd
        do i=1,2
          il=il+1
          call NToString(i,Veta(6:))
          call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Veta,nEdw,nCrw)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                         RoughPwd(i,KDatBlock),kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwRoughP=nEdw
            nCrwRoughP=nCrw
          endif
          if(KRough(KDatBlock).eq.IdPwdRoughNone) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          j=j+1
        enddo
      else
        nCrwKRoughFirst=0
        nCrwKRoughLast=0
        nEdwRoughP=0
        nCrwRoughP=0
      endif
      if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.and..not.isTOF.and.
     1   .not.isED) then
        il=ilp+1
        Veta='Illumination factor:'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','B')
        nLblIllum=LblLastMade
        if(KAbsor(KDatBlock).eq.1) call FeQuestLblOff(nLblIllum)
        ICrwGroup=ICrwGroup+1
        call FeBoldFont
        xpom=tpom+FeTxLength(Veta)+15.
        call FeNormalFont
        tpom=xpom+CrwgXd+10.
        do i=1,3
          if(i.eq.1) then
            Veta='%1 - none'
          else if(i.eq.2) then
            Veta='%cos(th)'
          else if(i.eq.3) then
            Veta='s%in(th)'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.1) nCrwIllumFirst=CrwLastMade
          if(KAbsor(KDatBlock).eq.0.or.
     1       (KAbsor(KDatBlock).eq.2.and.(i.eq.2.or.i.eq.1)).or.
     2       (KAbsor(KDatBlock).eq.3.and.(i.eq.3.or.i.eq.1)))
     3      call FeQuestCrwOpen(CrwLastMade,KIllum(KDatBlock).eq.i-1)
          xpom=xpom+80.
          tpom=tpom+80.
        enddo
        nCrwIllumLast=CrwLastMade
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+10.
        Veta='Focusing (Bra%gg-Bentano geometry)'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      0)
        nCrwFocus=CrwLastMade
        if(KAbsor(KDatBlock).ne.1)
     1    call FeQuestCrwOpen(CrwLastMade,KFocusBB(KDatBlock).ne.0)
      else
        nCrwIllumFirst=0
        nCrwFocus=0
      endif
      go to 9999
      entry PwdOptionsSampleCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwKPrefFirst.and.
     1   CheckNumber.le.nCrwKPrefLast) then
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefNone) then
          do j=1,2
            DirPref(j,KPhase,KDatBlock)=0.
          enddo
          DirPref(3,KPhase,KDatBlock)=1.
          PrefPwd(2,KPhase,KDatBlock)=0.
        endif
        KPref(KPhase,KDatBlock)=CheckNumber-nCrwKPrefFirst
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefMarchDollase) then
          PrefPwd(1,KPhase,KDatBlock)=1.1
        else
          PrefPwd(1,KPhase,KDatBlock)=0.
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwKRoughFirst.and.
     2        CheckNumber.le.nCrwKRoughLast) then
        call SetRealArrayTo(RoughPwd(1,KDatBlock),2,0.1)
        KRough(KDatBlock)=CheckNumber-nCrwKRoughFirst
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwKAbsorFirst.and.
     2        CheckNumber.le.nCrwKAbsorLast) then
        KAbsorNew=CheckNumber-nCrwKAbsorFirst
        if((PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS.or.
     1    PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)) then
          if(KAbsorNew.gt.0) KAbsorNew=KAbsorNew+2
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMir) then
        call FeQuestRealFromEdw(nEdwMir,MirPwd(KDatBlock))
        go to 9999
      endif
2500  nEdw=nEdwPrefP
      nCrw=nCrwPrefP
      j=IPrefPwd+IZdvihProf
      do i=1,2
        if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
          if(KPref(KPhase,KDatBlock).ne.KPrefOld) then
            call FeQuestRealEdwOpen(nEdw,PrefPwd(i,KPhase,KDatBlock),
     1                              .false.,.false.)
            call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
          endif
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
        j=j+1
      enddo
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        if(KPref(KPhase,KDatBlock).ne.KPrefOld) then
          call FeQuestRealAEdwOpen(nEdwDirPref,
     1      DirPref(1,KPhase,KDatBlock),3,.false.,.false.)
          call FeQuestCrwOpen(nCrwPrefAve,.true.)
        endif
      else
        call FeQuestEdwClose(nEdwDirPref)
        call FeQuestCrwClose(nCrwPrefAve)
      endif
      KPrefOld=KPref(KPhase,KDatBlock)
      if(isTOF) then
        j=ITOFAbsPwd+IZdvihRec
        if(CrwLogicQuest(nCrwTOFAbsP)) then
          KUseTOFAbs(KDatBlock)=1
          call FeQuestLblOn(nLblTOFAbs)
          call FeOpenParEdwCrw(nEdwTOFAbs,nCrwTOFAbs,'#keep#',
     1                         TOFAbsPwd(KDatBlock),kiPwd(j),.false.)
        else
          call FeUpdateParamAndKeys(nEdwTOFAbs,nCrwTOFAbs,
     1                              TOFAbsPwd(KDatBlock),kiPwd(j),1)
          call FeQuestLblOff(nLblTOFAbs)
          call FeQuestEdwClose(nEdwTOFAbs)
          call FeQuestCrwClose(nCrwTOFAbs)
          KUseTOFAbs(KDatBlock)=0
        endif
      else
        if(KAbsorNew.ne.KAbsor(KDatBlock)) then
          if(KAbsorNew.eq.IdPwdAbsorbNone) then
            call FeQuestEdwClose(nEdwMir)
          else
            if(KAbsorNew.eq.IdPwdAbsorbCylinder) then
              StMir(5:5)='r'
            else
              StMir(5:5)='t'
            endif
            call FeQuestEdwLabelChange(nEdwMir,StMir)
            call FeQuestRealEdwOpen(nEdwMir,MirPwd(KDatBlock),.false.,
     1                              .false.)
          endif
          if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown) then
            if(KAbsorNew.eq.1) then
              if(nLblRough.gt.0) then
                call FeQuestLblOff(nLblRough)
                nCrw=nCrwKRoughFirst
                do i=1,3
                  call FeQuestCrwClose(nCrw)
                  nCrw=nCrw+1
                enddo
              endif
              if(nLblIllum.gt.0) then
                call FeQuestLblOff(nLblIllum)
                nCrw=nCrwIllumFirst
                do i=1,3
                  call FeQuestCrwClose(nCrw)
                  nCrw=nCrw+1
                enddo
              endif
              if(nCrwFocus.gt.0) call FeQuestCrwClose(nCrwFocus)
            else
              if(nLblRough.gt.0) then
                if(CrwStateQuest(nCrwKRoughFirst).eq.CrwClosed) then
                  call FeQuestLblOn(nLblRough)
                  nCrw=nCrwKRoughFirst
                  do i=1,3
                    call FeQuestCrwOpen(nCrw,i-1.eq.KRough(KDatBlock))
                    nCrw=nCrw+1
                  enddo
                endif
              endif
              if(nLblIllum.gt.0) then
                if(CrwStateQuest(nCrwIllumFirst).eq.CrwClosed) then
                  call FeQuestLblOn(nLblIllum)
                  nCrw=nCrwIllumFirst
                  do i=1,3
                    call FeQuestCrwOpen(nCrw,
     1                                  KIllum(KDatBlock).eq.i-1)
                    nCrw=nCrw+1
                  enddo
                endif
              endif
              if(nCrwFocus.gt.0) then
                if(CrwStateQuest(nCrwFocus).eq.CrwClosed)
     1            call FeQuestCrwOpen(nCrwFocus,
     2                                KFocusBB(KDatBlock).ne.0)
              endif
            endif
            if(KAbsorNew.eq.2) then
              if(nCrwIllumLast.gt.0)
     1          call FeQuestCrwClose(nCrwIllumLast)
            else if(KAbsorNew.ne.1) then
              if(nCrwIllumLast.gt.0) then
                if(CrwStateQuest(nCrwIllumLast).eq.CrwClosed) then
                  call FeQuestCrwOpen(nCrwIllumLast,.false.)
                  call FeQuestCrwOpen(nCrwIllumFirst,.true.)
                endif
              endif
            endif
            if(KAbsorNew.eq.3) then
              if(nCrwIllumLast-1.gt.0)
     1          call FeQuestCrwClose(nCrwIllumLast-1)
            else if(KAbsorNew.ne.1) then
              if(nCrwIllumLast-1.gt.0) then
                if(CrwStateQuest(nCrwIllumLast-1).eq.CrwClosed) then
                  call FeQuestCrwOpen(nCrwIllumLast-1,.false.)
                  call FeQuestCrwOpen(nCrwIllumFirst,.true.)
                endif
              endif
            endif
          endif
        endif
        KAbsor(KDatBlock)=KAbsorNew
      endif
      if(nCrwKRoughFirst.ne.0) then
        nEdw=nEdwRoughP
        nCrw=nCrwRoughP
        j=IRoughPwd+IZdvihRec
        do i=1,2
          if(KRough(KDatBlock).gt.0) then
            if(KRough(KDatBlock).ne.KRoughOld) then
              call FeQuestRealEdwOpen(nEdw,RoughPwd(i,KDatBlock),
     1                                .false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
        KRoughOld=KRough(KDatBlock)
      endif
      entry PwdOptionsSampleUpdate
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        call FeUpdateParamAndKeys(nEdwPrefP,nCrwPrefP,
     1    PrefPwd(1,KPhase,KDatBlock),kiPwd(IPrefPwd+IZdvihProf),2)
        if(CrwLogicQuest(nCrwPrefAve)) then
          SPref(KPhase,KDatBlock)=1
        else
          SPref(KPhase,KDatBlock)=0
        endif
        call FeQuestRealAFromEdw(nEdwDirPref,
     1                           DirPref(1,KPhase,KDatBlock))
      endif
      if(nCrwKRoughFirst.gt.0)
     1  call FeUpdateParamAndKeys(nEdwRoughP,nCrwRoughP,
     2                            RoughPwd(1,KDatBlock),
     3                            kiPwd(IRoughPwd+IZdvihRec),2)
      if(nCrwIllumFirst.gt.0) then
        nCrw=nCrwIllumFirst
        if(CrwStateQuest(nCrw).ne.CrwClosed) then
          do i=0,2
            if(CrwLogicQuest(nCrw)) then
              KIllum(KDatBlock)=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        else
          KIllum(KDatBlock)=0
        endif
        if(CrwLogicQuest(nCrwFocus)) then
          KFocusBB(KDatBlock)=1
        else
          KFocusBB(KDatBlock)=0
        endif
      endif
      if(isTOF) then
        call FeUpdateParamAndKeys(nEdwTOFAbs,nCrwTOFAbs,
     1                            TOFAbsPwd(KDatBlock),
     2                            kiPwd(ITOFAbsPwd+IZdvihRec),1)
      endif
      if(nCrwKRoughFirst.gt.0) then
        if(KRough(KDatBlock).gt.0) then
          call FeUpdateParamAndKeys(nEdwRoughP,nCrwRoughP,
     1                              RoughPwd(1,KDatBlock),
     2                              kiPwd(IRoughPwd+IZdvihRec),2)
        endif
      endif
9999  return
      end
