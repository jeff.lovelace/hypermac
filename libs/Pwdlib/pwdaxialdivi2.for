      subroutine PwdAxialDivI2(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                         Z0P,Z0M,I2P,I2M,DelSmooth)
      include 'fepc.cmn'
      include 'basic.cmn'
      real I2P,I2M
      integer Case
      character*80 Veta
      Case=0
      I2P=0.
      I2M=0.
      if(RLen.ge.Z0P-Z0M) then
        if(Z0M.ge.-RLen*.5.and.Z0P.le.RLen*.5) then
          EpsA=Eps1P
          EpsB=Eps2P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=1
        else if(Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Siroka sterbina-podivnost-2',Veta)
        endif
      else
        if(Z0M.le.-RLen*.5.and.Z0P.ge.RLen*.5) then
          EpsA=Eps1M
          EpsB=Eps2P
          EpsC=Eps1P
          EpsD=Eps2M
          Case=1
        else if(Z0M.gt.-RLen*.5.and.Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5)
     1    then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Uzka sterbina-podivnost-2',Veta)
        endif
      endif
      EpsPlus =Eps+DelSmooth
      EpsMinus=Eps-DelSmooth
      if(Case.eq.1) then
        if(Eps.gt.EpsA.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          endif
        else if(Eps.ge.EpsB-DelSmooth.and.Eps.le.EpsA) then
          if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          else if(EpsMinus.ge.EpsB) then
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsB,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.gt.EpsC.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.2) then
        if(Eps.ge.EpsA-DelSmooth.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsA,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.gt.EpsB.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.gt.EpsC.and.Eps.le.EpsB) then
          if(EpsPlus.gt.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsMinus.le.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.3) then
        if(Eps.gt.EpsB.and.Eps.le.EpsA+DelSmooth) then
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.gt.EpsC.and.Eps.le.EpsB) then
          if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      endif
      return
      end
