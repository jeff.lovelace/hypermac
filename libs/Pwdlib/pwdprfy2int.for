      function PwdPrfY2Int(y)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(SqrtScale) then
        PwdPrfY2Int=((y-YminPlC)/sky+shy)**2
      else
        PwdPrfY2Int=(y-YminPlC)/sky+shy
      endif
      return
      end
