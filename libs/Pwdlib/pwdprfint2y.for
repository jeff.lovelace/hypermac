      function PwdPrfInt2Y(int)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      real int
      if(SqrtScale) then
        pom=sqrt(int)-shy
      else
        pom=int-shy
      endif
      PwdPrfInt2Y=YminPlC+pom*sky
      return
      end
