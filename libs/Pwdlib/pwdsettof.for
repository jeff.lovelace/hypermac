      subroutine PwdSetTOF(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical PwdCheckTOFInD
      if(DataType(KDatB).eq.-2) then
        if(Radiation(KDatB).eq.NeutronRadiation) then
          isTOF=.true.
          TOFInD=PwdCheckTOFInD(KDatB)
        else
          isED=.true.
        endif
      else
        isTOF=.false.
        TOFInD=.false.
        isED=.false.
      endif
      return
      end
