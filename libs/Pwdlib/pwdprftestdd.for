      subroutine PwdPrfTestDD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Wx(1000),Wy(1000),Wz(1000)
      character*80 SvFile
      character*80 t80
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Continue')
      nButtContinue=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Print')
      nButtPrint=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      Th2=120.
      Th2From=119.7
      Th2To=120.3
      Rad=215.
      DelR=12.
      DelX=12.
      DelS=25.
      DelEps=.01
      NBeta=20
      DelSmooth=.001
      ic=0
1500  idp=NextQuestId()
      xqd=220.
      il=5
      call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define new values',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      t80='%2Theta'
      xpom=tpom+30.
      dpom=30.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,1)
      nEdwTh2=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2,.false.,.false.)
      xpom=xpom+xqd/3.
      tpom=tpom+xqd/3.
      t80='%From'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwTh2From=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2From,.false.,.false.)
      xpom=xpom+xqd/3.
      tpom=tpom+xqd/3.
      t80='%To'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwTh2To=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2To,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='%X-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelX=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelX,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%S-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelS=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelS,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%R-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelR=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelR,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='R%adius'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Rad,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='N%Beta'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNBeta=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NBeta,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='Del%Eps'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelEps=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelEps,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='Sm%ooth'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwOkenko=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelSmooth,.false.,.false.)
1600  call FeQuestEvent(idp,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTh2) then
        call FeQuestRealFromEdw(nEdwTh2From,Th2From)
        call FeQuestRealFromEdw(nEdwTh2To,Th2To)
        pom=Th2
        call FeQuestRealFromEdw(nEdwTh2,Th2)
        pom=Th2-pom
        Th2From=Th2From+pom
        Th2To  =Th2To  +pom
        call FeQuestRealEdwOpen(nEdwTh2From,Th2From,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwTh2To,Th2To,.false.,.false.)
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwTh2,Th2)
        call FeQuestRealFromEdw(nEdwTh2From,Th2From)
        call FeQuestRealFromEdw(nEdwTh2To,Th2To)
        call FeQuestRealFromEdw(nEdwRadius,Rad)
        call FeQuestRealFromEdw(nEdwDelX,DelX)
        call FeQuestRealFromEdw(nEdwDelS,DelS)
        call FeQuestRealFromEdw(nEdwDelR,DelR)
        call FeQuestRealFromEdw(nEdwDelEps,DelEps)
        call FeQuestRealFromEdw(nEdwOkenko,DelSmooth)
        call FeQuestIntFromEdw(nEdwNBeta,NBeta)
      endif
      call FeQuestRemove(idp)
      if(ich.ne.0) go to 5400
      xomn=Th2From
      xomx=Th2To
      Wx(1)=Th2From
      N=nint((xomx-xomn)/DelEps)+1
      do i=2,N
        Wx(i)=Wx(i-1)+DelEps
      enddo
      call PwdAxialDivT(Th2,DelX,DelS,DelR,Rad,-1.,-1.,
     1                  NBeta,N,DelSmooth*ToRad,DelEps,Wx,Wy,Wz)
      yomn=0.
      yomx=0.
      do i=1,N
        yomx=max(yomx,Wy(i))
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      call FeXYPlot(Wx,Wy,N,NormalLine,NormalPlotMode,Green)
      call FeXYPlot(Wx,Wz,N,NormalLine,NormalPlotMode,Red)
      call FeReleaseOutput
      call FeDeferOutput
5300  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue.or.CheckNumber.eq.nButtPrint)
     1    then
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 1500
        endif
      endif
      go to 5300
5400  call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
