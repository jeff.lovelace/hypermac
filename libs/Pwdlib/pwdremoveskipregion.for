      subroutine PwdRemoveSkipRegion(DegFr,DegTo)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      n=NSkipPwd(KDatBlock)
      do 1000i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).gt.DegTo) then
          go to 2000
        else if(SkipPwdTo(i,KDatBlock).lt.DegFr) then
          go to 1000
        else if(SkipPwdFr(i,KDatBlock).ge.DegFr) then
          if(SkipPwdTo(i,KDatBlock).gt.DegTo) then
            SkipPwdFr(i,KDatBlock)=DegTo
            go to 2000
          else
            SkipPwdFr(i,KDatBlock)=-999.
          endif
        else if(SkipPwdFr(i,KDatBlock).lt.DegFr) then
          if(SkipPwdTo(i,KDatBlock).gt.DegTo) then
            n=n+1
            SkipPwdFr(n,KDatBlock)=DegTo
            SkipPwdTo(n,KDatBlock)=SkipPwdTo(i,KDatBlock)
            SkipPwdTo(i,KDatBlock)=DegFr
            go to 2000
          else
            SkipPwdTo(i,KDatBlock)=DegFr
          endif
        endif
1000  continue
2000  NSkipPwd(KDatBlock)=n
      call PwdOrderSkipRegions
      return
      end
