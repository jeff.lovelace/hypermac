      function PwdIncIntFun(xx,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension pa(12)
      data nnn/0/
      if(BankITyp(n).le.2) then
        PwdIncIntFun=BankICoff(1,n)
        m=2
        xxp=1.
        do i=1,5
          xxp=xxp*xx
          if(BankITyp(n).eq.2.and.i.eq.1) then
            PwdIncIntFun=PwdIncIntFun+BankICoff(m,n)/xx**5*
     1                                exp(-BankICoff(m+1,n)/xx**2)
          else
            PwdIncIntFun=PwdIncIntFun+BankICoff(m,n)*
     1                                exp(-BankICoff(m+1,n)*xxp)
          endif
          m=m+2
        enddo
      else if(BankITyp(n).eq.3.or.BankITyp(n).eq.5) then
        if(BankITyp(n).eq.3) then
          xxp=2./xx-1.
        else
          xxp=xx/10.
        endif
        PwdIncIntFun=Chebev(BankICoff(m,n),pa,xxp,12)
      else if(BankITyp(n).eq.4) then
        xxp=2./xx-1.
        if(xxp.lt.-1.) xxp=-1.
        if(xxp.gt. 1.) xxp= 1.
        PwdIncIntFun=BankICoff(1,n)+BankICoff(2,n)/xx**5*
     1                              exp(-BankICoff(3,n)/xx**2)+
     2                              Chebev(BankICoff(4,n),pa,xxp,9)
      else
        PwdIncIntFun=1.
      endif
      return
      end
