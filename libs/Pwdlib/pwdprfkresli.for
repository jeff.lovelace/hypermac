      subroutine PwdPrfKresli(Klic,Psani)
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension XPwdP(:),XPwdG(:),XPwdA(:),seg1x(100),seg2x(100),
     1          seg1y(100),kkFlag(3),seg2y(100),hh(3)
      integer Color,whLine,SegCol(100),Exponent10,Psani,
     1        CrlSubSystemNumber
      logical EqualStep,nahradZacatek,nahradKonec,KrObs,FeYesNoLong,
     1        KrDif,CrlItIsSatellite
      logical :: DetailyAno=.true.
      character*10 ch10
      character*20 ch20,FormatInt
      character*80 Veta
      real IntNaDil
      save EqualStep,YpMin,YpMax,YObsMax,YDifMin,YDifMax,
     1     YCalcMax,KrObs,n1,n2,n1b,n2b
      allocatable XPwdP,XPwdA,XPwdG
      allocate(XPwdP(npnts),XPwdG(NManBackg(KDatBlock)),
     1         XPwdA(max(npnts,NManBackg(KDatBlock))))
      if(Psani.eq.1) DetailyAno=.true.
      call FeDeferOutput
      if(.not.ZPrf) then
        KrDif=.false.
      else
        KrDif=KresliDifLine
      endif
      WLabXL=0.
      xpom=FeTxLength('XXXXX')+5.
      if(KresliBragg) then
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          WLabXL=max(WLabXL,FeTxLength(Cislo)-xpom)
        enddo
      endif
      if(KrDif) then
        if(SqrtScale) then
          WLabXL=max(WLabXL,FeTxLength('sqrt(Iobs)-'))
        else
          WLabXL=max(WLabXL,FeTxLength('delta(I)'))
        endif
      endif
      if(SqrtScale) then
        WLabXL=max(WLabXL,FeTxLength('sqrt(Icalc)'))
      else
        WLabXL=max(WLabXL,FeTxLength('I'))
      endif
      WLabXL=WLabXL+5.
      if(KresliLab) then
        WLabX=WLabXL+xpom
        WLabY=30.
      endif
      YMinPlW=YMinAcWin+WLabY
      XMinPlW=XMinAcWin+WLabX
      XMaxTx=XMinAcWin+WLabXL
      YMinPlC=YMinPlW+6.
      XMinPlC=XMinPlW+2.
      if(klic.eq.-1) then
        EqualStep=.true.
        YpMin=YoPwd(1)
        YpMax=0.
        YObsMax=0.
        YCalcMax=0.
        DelMin=999999.
        YdMax1=0.
        YdMax2=0.
        do i=1,Npnts
          XPwdi=XPwd(i)
          if(i.lt.Npnts) then
            pom=XPwd(i+1)-XPwdi
            if(pom.lt.-.0001) then
              if(Psani.gt.0) then
                Veta='See point #'
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                Veta='See point #'//Cislo(:idel(Cislo))//':'
                write(Veta(idel(Veta)+1:),Format92) XPwd(i),YoPwd(i),
     1                                    YsPwd(i)
                call FeChybne(-1.,-1.,'Profile steps are not '//
     1                        'ascending.',Veta,SeriousError)
              endif
              go to 9000
            endif
            EqualStep=EqualStep.and.abs(pom-DegStepPwd).lt..0001
          endif
          YpMax=max(YcPwd(i),YoPwd(i),YpMax)
          YpMin=min(YoPwd(i),YpMin)
          YObsMax=max(abs(YoPwd(i)),YObsMax)
          YCalcMax=max(abs(YcPwd(i)),YCalcMax)
          if(YfPwd(i).eq.1) then
            YpMin=min(YcPwd(i),YpMin)
            if(SqrtScale) then
              DifArr(i)=sqrt(YoPwd(i))-sqrt(YcPwd(i))
            else
              DifArr(i)=YoPwd(i)-YcPwd(i)
            endif
            YdMax1=max(YdMax1,abs(DifArr(i)))
            YdMax2=max(YdMax2,abs(YoPwd(i)-YcPwd(i)))
          else
            DifArr(i)=0.
          endif
        enddo
        YpMax=max(YpMax,YObsMax)
        if(SqrtScale) then
          do i=1,Npnts
            DifArr(i)=YdMax2/YdMax1*DifArr(i)
          enddo
        endif
        if(YOriginShift.gt.0.) then
          YpMin=YpMin*YOriginShift
        else
          YpMin=0.
        endif
      endif
      if(EqualStep) then
        if(DegMin.gt.0.) then
          n1=nint((max(DegMin,XPwd(1))-XPwd(1))/DegStepPwd)+1
        else
          n1=1
        endif
        if(DegMax.gt.0.) then
          n2=nint((min(DegMax,XPwd(Npnts))-XPwd(1))/
     1      DegStepPwd)+1
        else
          n2=Npnts
        endif
      else
        if(DegMax.lt.0.) DegMax=XPwd(Npnts)
        if(DegMin.lt.0.) DegMin=XPwd(1)
        call locate(XPwd,Npnts,DegMin,n1)
        call locate(XPwd,Npnts,DegMax,n2)
      endif
      n1=max(1,n1)
      n2=min(n2,Npnts)
      if(n2.eq.0) n2=Npnts
      if(n2.le.n1) then
        ErrFlag=1
        go to 9000
      endif
      DegMin=XPwd(n1)
      DegMax=XPwd(n2)
      if(KresliManBackg) then
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMin,n1g)
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMax,n2g)
        if(XManBackg(n1g,KDatBlock).lt.DegMin) n1g=n1g+1
        n1g=max(1,n1g)
        if(XManBackg(n2g,KDatBlock).gt.DegMax) n2g=n2g-1
        n2g=min(n2g,NManBackg(KDatBlock))
      endif
      nst=n1
      nen=n2
      if(NBragg.gt.0) then
        call locate(BraggArr,NBragg,XPwd(n1),n1b)
        call locate(BraggArr,NBragg,XPwd(n2),n2b)
        n1b=max(n1b,1)
        n2b=min(n2b,NBragg)
        n2b=max(n2b,0)
900     if(n1b.lt.NBragg.and.BraggArr(n1b).lt.XPwd(n1)) then
          n1b=n1b+1
          go to 900
        endif
1000    if(n2b.gt.0.and.BraggArr(n2b).gt.XPwd(n2)) then
          n2b=n2b-1
          go to 1000
        endif
        if(n1b.eq.0) n2b=-1
      else
        n1b=1
        n2b=0
      endif
      if(IntMax.le.0.) then
        if(KresliCalcLine.and.(KresliObsLine.or.KresliObsPoints)) then
           IntMax=YpMax
        else
          if(KresliCalcLine) then
            IntMax=YCalcMax
          else
            IntMax=YObsMax
          endif
        endif
      else
        if(IntMax.gt.YpMax) IntMax=YpMax
        if(IntMax.lt.YpMin+1.) IntMax=YpMin+1.
      endif
      Celkem=IntMax-YpMin
      if(Celkem.le.0.) then
        IntMax=100.
        YpMin=0.
        Celkem=100.
      endif
      if(KrDif) then
        YDifMin=0.
        YDifMax=0.
        do i=n1,n2
          YDifMin=min(YDifMin,DifArr(i))
          YDifMax=max(YDifMax,DifArr(i))
        enddo
        YDifMin=abs(min(YDifMin,0.))
        YDifMax=max(0.,YDifMax)
        Celkem=Celkem+YDifMin+YDifMax
      endif
      Vyska=YMaxPlC-YMinPlC
      VyskaMezera1=30.
      VyskaMezera2=8.
      VyskaMezera3=5.
      if(NPhase.le.3) then
        VyskaBragg=16.
      else if(NPhase.le.8) then
        VyskaBragg=12.
      else
        VyskaBragg=8.
      endif
      PruhBragg=VyskaBragg+3.
      Vyska=Vyska-VyskaMezera1
      if(KrDif) then
        Vyska=Vyska-VyskaMezera3
        if(KresliBragg) Vyska=Vyska-VyskaMezera2
        if(Celkem.gt.0.) then
          VyskaDif=Vyska*(YDifMin+YDifMax)/Celkem
        else
          VyskaDif=10.
        endif
      endif
      if(KresliBragg) then
        y1bragg=YMinPlW+VyskaMezera1
        if(KrDif) y1bragg=y1bragg+VyskaDif+VyskaMezera2
        y2bragg=y1bragg+VyskaBragg
      endif
      YMinPlC=YMinPlC+VyskaMezera1
      if(KrDif) YMinPlC=YMinPlC+VyskaDif
      if(KresliBragg) YMinPlC=YMinPlC+PruhBragg*float(NPhase)
      if(KrDif.or.KresliBragg) YMinPlC=YMinPlC+VyskaMezera3
      if(KrDif.and.KresliBragg) YMinPlC=YMinPlC+VyskaMezera2
      if(YMinPlC.le.YMinPlW.or.YMinPlC.ge.YMaxPlC) go to 9000
      if(SqrtScale) then
        shy=sqrt(YpMin)
      else
        shy=YpMin
      endif
      skydif=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      if(SqrtScale) then
        sky=(YMaxPlC-YMinPlC)/(sqrt(IntMax)-sqrt(YpMin))
      else
        sky=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      endif
      shx=XPwd(n1)
      skx=(XMaxPlC-XMinPlC)/(XPwd(n2)-XPwd(n1))
      do i=max(1,n1-1),n2
        XPwdP(i)=PwdPrfTh2X(XPwd(i))
      enddo
      if(KresliManBackg) then
        do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
          XPwdG(i)=PwdPrfTh2X(XManBackg(i,KDatBlock))
        enddo
      endif
      if(HardCopy.eq.0) then
        if(Klic.ne.-2) then
          call FeClearGrWin
        else
          call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,4,0,0,
     1                         Black)
        endif
      endif
      call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,0,0,0,White)
2000  do i=max(1,n1-1),n2
        CalcPArr(i)=PwdPrfInt2Y(YcPwd(i))
        if(KrDif) DifArr(i)=(DifArr(i)+YDifMin)*skydif+YMinPlW+
     1                       VyskaMezera1
      enddo
      if(KresliObsPoints.and.(klic.ne.-2.or.
     1   (.not.KresliCalcLine.and..not.KresliObsLine))) then
        KrObs=.true.
        dzn=min(DegStepPwd*skx-2.,2.)
        dznh=dzn/sqrt(2.)
      else
        KrObs=.false.
      endif
      if(klic.eq.-2) go to 2340
      ZeroCalc=PwdPrfInt2Y(YpMin)
!      ZeroCalc=PwdPrfInt2Y(0.)
      if(KrDif) ZeroDif=YDifMin*skydif+YMinPlW+VyskaMezera1
      if(KresliZero) then
        Color=White
      else
        Color=Black
      endif
      call FeLineType(DashedLine)
      seg1x(1)=XMinPlC
      seg1x(2)=XMaxPlC
      seg1y(1)=ZeroCalc
      seg1y(2)=ZeroCalc
      call FePolyLine(2,seg1x,seg1y,Color)
      if(KrDif) then
        seg1y(1)=ZeroDif
        seg1y(2)=ZeroDif
        call FePolyLine(2,seg1x,seg1y,Color)
      endif
      call FeLineType(NormalLine)
      if(KresliLab) then
        if(SqrtScale) then
          DiffI=sqrt(IntMax)-sqrt(YpMin)
        else
          DiffI=IntMax-YpMin
        endif
        Step=10.**(Exponent10(DiffI)-1)
        Rad=Step*10.
        nn=anint(DiffI/Step)
        if(nn.lt.100) then
          Step=Step*.5
          Rad=Step*10.
          nn=nn*2
        endif
        if(Rad.gt..1) then
          FormatInt='(f20.1)'
        else if(Rad.gt..01) then
          FormatInt='(f20.2)'
        else if(Rad.gt..001) then
          FormatInt='(f20.3)'
        else
          FormatInt='(e20.5)'
        endif
        YpPom=0.
        pisy =-1000.
        pisyd= 1000.
        do i=1,nn
          pomy=PwdPrfInt2Y(YpPom)
          nseg=nseg+1
          seg1y(nseg)=pomy
          seg2y(nseg)=pomy
          SegCol(nseg)=White
          if(mod(i,10).eq.1) then
            seg1x(nseg)=XMinPlW-5.
            seg2x(nseg)=XMinPlW
            if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
              if(Rad.lt.1.) then
                write(ch20,FormatInt) YpPom
              else
                write(ch20,'(i20)') nint(YpPom)
              endif
              call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
              pisy=pomy
              pisyd=pisy
            endif
            go to 2150
          else if(mod(i,10).eq.6) then
            YIntLab=YpPom
          endif
          seg1x(nseg)=XMinPlW-2.
          seg2x(nseg)=XMinPlW
2150      if(nseg.ge.100) then
            call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
            nseg=0
          endif
          YpPom=YpPom+Step
        enddo
2210    if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(SqrtScale) then
          ch20='sqrt(IRel)'
        else
          if(isTOF) then
            ch20='TOF'
          else
            ch20='Intensity'
          endif
        endif
        call FeOutSt(0,XMinPlW-9.,PwdPrfInt2Y(YIntLab),ch20,'R',
     1               White)
        if(KrDif) then
          dify=abs(PwdPrfInt2Y(Step)-PwdPrfInt2Y(0.))
          seg1x(1)=XMinPlW-5.
          seg1x(2)=XMinPlW
          seg1x(3)=XMinPlW-2.
          seg1x(4)=XMinPlW
          i1=nint((ZeroDif-YMinPlW-VyskaMezera1)/dify)
          i2=nint(((YDifMax+YDifMin)*skydif+YMinPlW+VyskaMezera1-
     1            ZeroDif)/dify)
          pisy=-1000.
          do i=-i1-1,i2+1
            pomy=ZeroDif+i*dify
            if(pomy.le.YMinPlW.or.pomy.ge.y1bragg) cycle
            seg1y(1)=pomy
            seg1y(2)=pomy
            if(mod(i,10).eq.0) then
              call FePolyLine(2,seg1x,seg1y,White)
              if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
                pom=float(i)*Step
                if(Rad.lt.1.) then
                  write(ch20,FormatInt) pom
                else
                  write(ch20,'(i20)') nint(pom)
                endif
                call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
                pisy=pomy
              endif
            else
              call FePolyLine(2,seg1x(3),seg1y,White)
            endif
          enddo
          call FeOutSt(0,XMinPlW-9.,ZeroDif,'0.0','R',White)
        endif
        DegNaDil=(XMaxPlW-XMinPlW)*0.2*(DegMax-DegMin)/
     1    (XMaxPlW-XMinPlW-1.)
        if(DegNaDil.gt.10.) then
          step=10.
          idiv=10
        else if(DegNaDil.gt.5.) then
          step=5.
          idiv=5
        else if(DegNaDil.gt.2.) then
          step=2.
          idiv=10
        else if(DegNaDil.gt.1.) then
          step=1.
          idiv=10
        else if(DegNaDil.gt.0.5) then
          step=0.5
          idiv=5
        else if(DegNaDil.gt.0.2) then
          step=0.2
          idiv=10
        else if(DegNaDil.gt.0.1) then
          step=0.1
          idiv=10
        else if(DegNaDil.gt.0.05) then
          step=0.05
          idiv=5
        else
          step=0.01
          idiv=10
        endif
        ch10='(f6.1)'
        write(ch10(5:5),'(i1)') max(-Exponent10(DegNaDil),1)
        idrobne=1
        if(PwdPrfTh2X(DegMin+step/idiv)-PwdPrfTh2X(DegMin).lt.2.) then
          if(idiv.eq.20) then
            idiv=10
            if(PwdPrfTh2X(DegMin+step/idiv)-
     1         PwdPrfTh2X(DegMin).lt.2.) idrobne=0
          else
            idrobne=0
          endif
        endif
        if(idrobne.eq.0) idiv=1.
        deg=DegMin*float(idiv)/step
        deg=float(nint(deg)+1)*(step/float(idiv))
        pisx=-1000.
        nseg=0
        pomx1=0.
        pomx2=0.
2300    pomx=PwdPrfTh2X(deg)
        nseg=nseg+1
        seg1x(nseg)=pomx
        seg2x(nseg)=pomx
        SegCol(nseg)=White
        pom=deg/step
        if(abs(pom-float(nint(pom))).le.0.01) then
          deg=step*float(nint(pom))
          write(ch20,ch10) deg
          call zhusti(ch20)
          pom=FeTxLength(ch20)*.5
          if(pomx-pom.ge.pisx+distLabel) then
            pisx=pomx+pom
            pomx1=pomx2
            pomx2=pomx
            if(pisx.lt.XMaxPlW)
     1        call FeOutSt(0,pomx,
     2          YMinPlW-2.-1.-PropFontHeightInPixels,ch20,'C',White)
          endif
          seg1y(nseg)=YMinPlW-5.
          seg2y(nseg)=YMinPlW
        else
          if(idrobne.eq.1) then
            seg1y(nseg)=YMinPlW-2.
            seg2y(nseg)=YMinPlW
          endif
        endif
        if(nseg.ge.100) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        deg=deg+step/float(idiv)
        if(deg.le.DegMax) go to 2300
        if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(pomx1.gt.0..or.pomx2.gt.0.) then
          pomx=(pomx1+pomx2)*.5
          if(DrawInD) then
            ch20='d'
          else
            if(isTOF) then
              ch20='t'
            else if(isED) then
              ch20='E'
            else
              ch20='2th'
            endif
          endif
          call FeOutSt(0,pomx,YMinPlW-2.-1.-PropFontHeightInPixels,ch20,
     1                 'C',White)
        endif
      endif
      if(KresliTitle.and.plotTitle.ne.' ') then
        if(CenteredTitle) then
          call FeOutSt(0,XMinPlW+0.5*(XMaxPlW-XMinPlW),YMaxPlW-8.,
     1                 plotTitle,'C',White)
        else
          pom=FeTxLengthUnder(plotTitle)
          call FeOutSt(0,XMaxPlW-pom-2.,YMaxPlW-8.,plotTitle,'L',White)
        endif
      endif
      if(KresliBragg) then
        xpom=XMinPlW-5.
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          ypom=y2bragg+PruhBragg*(float(i-1)-.5)+1.
          call FeOutSt(0,xpom,ypom,Cislo,'R',White)
        enddo
      endif
2340  ist=0
      nseg=0
      if(KresliDetails.and.DetailyAno.and.klic.ne.-2) then
        NRefPwd=1
        NLamPwd=1
        iist=0
        iien=0
        if(DrawInD) then
          if(isTOF) then
            DegMinP=PwdTOF2D(DegMin)
            DegMaxP=PwdTOF2D(DegMax)
          else
            if(KRefLam(KDatBlock).eq.1) then
              DegMinP=PwdD2TwoTh(DegMax,LamPwd(1,KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamPwd(1,KDatBlock))
            else
              DegMinP=PwdD2TwoTh(DegMax,LamAve(KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamAve(KDatBlock))
            endif
          endif
        else
          DegMinP=DegMin
          DegMaxP=DegMax
        endif
        if(.not.allocated(ihPwdArr)) then
          n=1
          m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
          l=NAlfa(KDatBlock)
          allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1             iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2             pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3             tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4             sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5             dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6             coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7             cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8             Beta12a(l,n),IBroadHKLa(n))
          if(.not.isTOF.and..not.isED.and.
     1       KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2      allocate(AxDivProfA(-25:25,l,n),DerAxDivProfA(-25:25,7,l,n),
     3               FDSProf(-25:25,l,n))
        endif
        iin=0
        do i=1,NBragg
          if(DrawInD) then
            if(isTOF) then
              pom=PwdTOF2D(BraggArr(i))
            else
              if(KRefLam(KDatBlock).eq.1) then
                pom=PwdD2TwoTh(BraggArr(i),LamPwd(1,KDatBlock))
              else
                pom=PwdD2TwoTh(BraggArr(i),LamAve(KDatBlock))
              endif
            endif
          else
            pom=BraggArr(i)
          endif
          th=(pom+ShiftArr(i))*ToRad*0.5
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          call SetProfFun(indArr(1,i),hh,th)
          if(rdeg(1)/torad.le.DegMaxP.and.rdeg(2)/torad.ge.DegMinP) then
            if(iist.eq.0) iist=i
            iien=i
            iin=iin+1
          endif
        enddo
2360    if(iist.gt.0) then
          iist=iist-1
          iien=iien+1
          iin=iin+2
        else
          iist=1
          iien=1
          iin=1
        endif
        if(iin.gt.100) then
          if(Psani.eq.1) then
            TextInfo(1)='You are about to plot component functions for'
            write(Cislo,'(i4)') iin
            call Zhusti(Cislo)
            TextInfo(2)=Cislo(:idel(Cislo))//
     1                  ' Bragg peaks. Do you want to continue?'
            NInfo=2
            DetailyAno=FeYesNoLong(-1.,-1.,'Warning',0)
          endif
          if(.not.DetailyAno) then
            iist=1
            iien=1
            KresliDetails=.false.
          endif
        endif
      else
        iist=1
        iien=1
      endif
      call SetIntArrayTo(kkFlag,3,0)
      kkFlag(1)=1
      if(KresliObsLine.and.(Klic.ne.-2.or..not.KresliCalcLine))
     1  kkFlag(2)=1
      if(KresliManBackg) kkFlag(3)=1
      if(KresliCalcLine) then
        whLine=whichLine
      else
        whLine=2
      endif
      if(KrObs.or.KresliObsLine.or.KresliCalcLine) then
        k=0
        do j=n1,n2
2430      if(YfPwd(j).eq.0) then
            if(k.eq.0) then
              k=k+1
              seg1x(k)=XPwdP(j)
              seg1y(k)=YMinPlC
            endif
            k=k+1
            seg1x(k)=XPwdP(j)
            if(KrObs.or.KresliObsLine) then
              seg1y(k)=PwdPrfInt2Y(abs(YoPwd(j)))
            else
              seg1y(k)=PwdPrfInt2Y(abs(YcPwd(j)))
            endif
            seg1y(k)=min(seg1y(k),YMaxPlC)
            if(k.lt.98) cycle
          endif
          if(k.gt.0) then
            k=k+1
            seg1x(k)=seg1x(k-1)
            seg1y(k)=YMinPlC
            k=k+1
            seg1x(k)=seg1x(1)
            seg1y(k)=YMinPlC
            call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
            k=0
            if(YfPwd(j).eq.0) go to 2430
          endif
        enddo
        if(k.gt.1) then
          k=k+1
          seg1x(k)=seg1x(k-1)
          seg1y(k)=YMinPlC
          k=k+1
          seg1x(k)=seg1x(1)
          seg1y(k)=YMinPlC
          call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
        endif
      endif
      do ii=iist,iien
        if(ii.gt.iist) then
          call PwdPrfSum(n1,n2,ii,ii.eq.iist+1,ii.eq.iien)
          do i=n1,n2
            CalcPArr(i)=PwdPrfInt2Y(CalcPArr(i))
          enddo
        else
          Color=ColorCalc
        endif
        if(ii.eq.iien.and.iien.gt.1) then
          if(whLine.eq.3) then
            do i=n1,n2
              if(abs(PwdPrfInt2Y(YcPwd(i))-CalcPArr(i)).gt.1.)
     1          go to 2920
            enddo
            cycle
          else if(whLine.eq.2) then
            Color=ColorCalc
          else
            call FeLineType(NormalLine)
            cycle
          endif
        endif
2920    if(ii.eq.iist) then
          call FeLineType(NormalLine)
          kken=3
        else
          call FeLineType(DenseDashedLine)
          kken=1
        endif
        do kk=1,kken
          if(kkFlag(kk).eq.0) cycle
          if(kk.eq.1) then
            if(ii.eq.iist.and.iien.gt.iist.and.whLine.eq.2) cycle
            do i=n1,n2
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.2) then
            Color=ColorObs
            do i=n1,n2
              CalcPArr(i)=PwdPrfInt2Y(abs(YoPwd(i)))
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.3) then
            Color=Magenta
            do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
              CalcPArr(i)=PwdPrfInt2Y(abs(YManBackg(i,KDatBlock))*
     1                                Sc(2,KDatBlock))
              XPwdA(i)=XPwdG(i)
            enddo
            n1p=n1g
            n2p=n2g
          endif
          iskip=0
          do i=n1p,n2p
            if(ii.eq.iist.and.kk.eq.1.and..not.KresliCalcLine)
     1        go to 2990
            if(CalcPArr(i).le.YMaxPlC.and.ist.eq.0) ist=i
            if(CalcPArr(i).gt.YMaxPlC.or.i.eq.n2p) then
              if(ist.eq.0) go to 2990
              if(i-ist.ge.1) then
                nahradZacatek=.false.
                nahradKonec=.false.
                if(ist.gt.n1p) then
                  if(CalcPArr(ist-1).gt.YMaxPlC) then
                    nahradZacatek=.true.
                    call PwdPrfPrusY(XPwdA(ist),CalcPArr(ist),
     1                XPwdA(ist-1),CalcPArr(ist-1),pomx1,pomy1,
     2                YMaxPlC)
                    ist=ist-1
                  endif
                endif
                if(CalcPArr(i).gt.YMaxPlC) then
                  nahradKonec=.true.
                  call PwdPrfPrusY(XPwdA(i-1),CalcPArr(i-1),
     1              XPwdA(i),CalcPArr(i),pomx,pomy,YMaxPlC)
                endif
                call FePolyLine(i-ist+1,XPwdA(ist),CalcPArr(ist),
     1            Color)
                if(nahradKonec) then
                  XPwdA(i)=pomx
                  CalcPArr(i)=pomy
                endif
                if(nahradZacatek) then
                  XPwdA(ist)=pomx1
                  CalcPArr(ist)=pomy1
                endif
                ist=0
              endif
            endif
2990        if(kk.le.2) then
              if(KrObs.and.ii.eq.iist) then
                pomx=XPwdA(i)
                pomy=PwdPrfInt2Y(abs(YoPwd(i)))
                if(pomy+dzn.le.YMaxPlC) then
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy-dznh
                  seg2y(nseg)=pomy+dznh
                  SegCol(nseg)=ColorObs
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy+dznh
                  seg2y(nseg)=pomy-dznh
                  SegCol(nseg)=ColorObs
                endif
                if(nseg.ge.50.or.(i.eq.n2p.and.nseg.gt.0)) then
                  call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,
     1                                SegCol)
                  nseg=0
                endif
              endif
            else
              pomx=XPwdA(i)
              pomy=CalcPArr(i)
              call FeCircle(pomx,pomy,4.,Magenta)
            endif
          enddo
          if(kk.eq.3.and.n2p.gt.0.and.n1p.lt.NManBackg(KDatBlock)) then
            if(n1p.gt.n2p) then
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(1),seg1y(1),XMinPlC)
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(2),seg1y(2),XMaxPlC)
              call FePolyLine(2,seg1x,seg1y,Magenta)
            else
              if(n1p.gt.1) then
                call PwdPrfPrusX(XPwdA(n1p-1),CalcPArr(n1p-1),
     1                           XPwdA(n1p  ),CalcPArr(n1p  ),
     2                           seg1x(1),seg1y(1),XMinPlC)
                seg1x(2)=XPwdA(n1p)
                seg1y(2)=CalcPArr(n1p)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
              if(n2p.lt.NManBackg(KDatBlock)) then
                seg1x(1)=XPwdA(n2p)
                seg1y(1)=CalcPArr(n2p)
                call PwdPrfPrusX(XPwdA(n2p)  ,CalcPArr(n2p)  ,
     1                           XPwdA(n2p+1),CalcPArr(n2p+1),
     2                           seg1x(2),seg1y(2),XMaxPlC)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
            endif
          endif
        enddo
        Color=ColorCalc
      enddo
      call FeLineType(NormalLine)
      if(KrDif) then
        call FePolyLine(n2-n1+1,XPwdP(n1),DifArr(n1),ColorDif)
        do i=max(1,n1-1),n2
          if(KrDif) DifArr(i)=(DifArr(i)-YMinPlW-VyskaMezera1)/skydif-
     1                        YDifMin
        enddo
      endif
      if(KresliBragg.and.klic.ne.-2) then
        do i=n1b,n2b
          seg1x(1)=PwdPrfTh2X(BraggArr(i))
          seg1x(2)=seg1x(1)
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          k=mod(KlicArr(i),100)
          if(CrlItIsSatellite(indArr(1,i))) then
            Color=ColorSat
          else
            j=CrlSubSystemNumber(indArr(1,i))
            if(j.le.0) then
              Color=ColorMain12
            else if(j.eq.1) then
              Color=ColorMain1
            else
              Color=ColorMain2
            endif
          endif
          Zdvih=PruhBragg*float(KPh-1)
          if(k.eq.1) then
            seg1y(1)=y2bragg+Zdvih-VyskaBragg*.5
          else
            seg1y(1)=y1bragg+Zdvih
          endif
          seg1y(2)=y2bragg+Zdvih
          call FePolyLine(2,seg1x,seg1y,Color)
        enddo
        ybragg=y1bragg+0.5*(y2bragg-y1bragg)
      else
        ybragg=0.
      endif
      go to 9999
9000  ErrFlag=0
9999  if(allocated(XPwdP)) deallocate(XPwdP,XPwdG,XPwdA)
      return
      end
