      subroutine PwdOptionsAsym
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*24 :: AsymFundamentalLabel(10) =
     1                (/'Primary radius [mm]   ',
     2                  'Secondary radius [mm] ',
     3                  'RS width [mm]         ',
     4                  'FDS angle [deg]       ',
     5                  'VDS angle [mm]        ',
     6                  'Source length [mm]    ',
     7                  'Sample length [mm]    ',
     8                  'RS length [mm]        ',
     9                  'Primary soller [deg]  ',
     a                  'Secondary soller [deg]'/),
     a                 AsymTOFLabel(3) =
     1                 (/'%None                   ',
     2                   '%Simple rise/decay      ',
     3                   '%Jason Hodges rise/decay'/)
      character*28 :: AsymMethodLabel(6) =
     1                (/'%No correction              ',
     2                  'Si%mpson correction         ',
     3                  '%Berar-Baldinozzi correction',
     4                  'correction by di%vergence   ',
     5                  '%fundamental approach       ',
     6                  '%Debye-Scherer integration  '/)
      character*42 :: AsymDebyeIntLabel(6) =
     1                (/'Relative bandwidth of monochromator     ',
     2                  'Pixel size in microns                   ',
     3                  'Beam height in microns                  ',
     4                  'Capillary diameter in microns           ',
     5                  'Incident beam divergence in milliradians',
     6                  'Radius of sample-to-image arc in mm     '/)
      character*9 :: AsymLabel(2) =
     1               (/'1/tg(th) ',
     2                 '1/tg(2th)'/)
      logical Otevirat,lpom,CrwLogicQuest
      integer CrwStateQuest,EdwStateQuest
      real :: AsymPwdSave(6),
     1        AsymPwdDef (6)=(/.140,42.2,50.,300.,.0714,300./)
      external NToString
      save nCrwFundRefFr,nCrwSSoll,nCrwFundSelFr,nCrwKUseHS,
     1     nCrwFundSelTo,nCrwUseRSW,nCrwUseFDS,nCrwUseVDS,nCrwRSW,
     2     nCrwDS,nCrwPSoll,nCrwFDS,nCrwVDS,nCrwUsePSoll,nCrwUseSSoll,
     4     nEdwFundFr,nEdwRSW,nEdwDS,nEdwPSoll,nEdwSSoll,nEdwNAsym,
     5     nEdwFDS,nEdwVDS,
     6     nLblFundFr,nLblFundTo,nLblFirst,nLinkaFundFr,nLinkaFundTo,
     7     nEdwDebyeIntFr,nEdwDebyeIntTo,AsymPwdSave
      save /PwdOptionsC/
      entry PwdOptionsAsymMake(id)
      il=1
      ilal=il
      do i=1,2
        call FeQuestLblMake(id,xpoma(i+1)+20.,ilal,AsymLabel(i),'C','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblFirst=LblLastMade
      enddo
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymBerar) then
        nLbl=nLblFirst
        do i=1,2
          call FeQuestLblOn(nLbl)
          nLbl=nLbl+1
        enddo
      endif
      il=il+1
      ilp=il
      NAsymNew=NAsym(KDatBlock)
      KAsymNew=KAsym(KDatBlock)
      nLblFundFr=0
      nLinkaFundFr=0
      nCrwFundRefFr=0
      nEdwDebyeIntFr=0
      nEdwFundFr=0
      nEdwRSW=0
      nCrwRSW=0
      nEdwDS=0
      nCrwDS=0
      nEdwPSoll=0
      nCrwPSoll=0
      nEdwSSoll=0
      nCrwSSoll=0
      nCrwFundSelFr=0
      nCrwFundSelTo=0
      nCrwUseRSW=0
      nCrwUseFDS=0
      nCrwUseVDS=0
      nEdwFDS=0
      nCrwFDS=0
      nEdwVDS=0
      nCrwVDS=0
      nEdwNAsym=0
      xpomc=0.
      if(isTOF) then
        ik=3
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,ik
          call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                        AsymTOFLabel(i),'L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwAsymFr=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAsym(KDatBlock)+1)
          il=il+1
        enddo
        nCrwAsymTo=CrwLastMade
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        AsymOrg=0.
        KiAsymOrg=0
        AsymOrg(1,1)=.05
        AsymOrg(3,1)=.05
        AsymOrg(1,2)=2.
        AsymOrg(3,2)=2.
        AsymOrg(5,2)=50.
        AsymOrg(7,2)=50.
        do i=1,8
          if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
            if(i.le.4) then
              Veta=lAsymTOF1Pwd(i)
            else
              Veta=' '
            endif
          else
            Veta=lAsymTOF2Pwd(i)
          endif
          call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,nCrw)
          if(i.eq.1) then
            nEdwAsymP=nEdw
            nCrwAsymP=nCrw
          endif
          if(KAsym(KDatBlock).ne.IdPwdAsymTOFNone.and.
     1      (KAsym(KDatBlock).ne.IdPwdAsymTOF1.or.i.le.4)) then
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                           AsymPwd(i,KDatBlock),kiPwd(j),.false.)
            if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
              AsymOrg(i,1)=AsymPwd(i,KDatBlock)
              KiAsymOrg(i,1)=kiPwd(j)
            else
              AsymOrg(i,2)=AsymPwd(i,KDatBlock)
              KiAsymOrg(i,2)=kiPwd(j)
            endif
          endif
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
          j=j+1
        enddo
        if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          NAsymOld=4
        else
          NAsymOld=8
        endif
      else
        xpomc=5.
        tpom=xpomc+CrwgXd+10.
        do i=1,6
          call FeQuestCrwMake(id,tpom,il,xpomc,il,
     1                        AsymMethodLabel(i),'L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwAsymFr=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAsym(KDatBlock)+1)
          il=il+1
          if((i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodDS).or.
     1       (i.eq.6.and.PwdMethod(KDatBlock).ne.IdPwdMethodDS))
     2      call FeQuestCrwDisable(CrwLastMade)
        enddo
        nCrwAsymTo=CrwLastMade
        il=3
        xpom=288.
        tpom=xpom+CrwgXd+10.
        Veta='%Use H/L and S/L'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      0)
        nCrwKUseHS=CrwLastMade
        il=2
        xpom=0.
        do i=1,10
          xpom=max(xpom,FeTxLengthUnder(AsymFundamentalLabel(i)))
        enddo
        tpom=tpoma(2)+5.
        xpom=tpom+xpom+30.
        dpom=50.
        xpomu=xpom-CrwgXd-15.
        xpomr=xpom+dpom+3.
        j=IAsymPwd+IZdvihRec
        do i=1,10
          Otevirat=(i.ne.4.and.i.ne.5).or.
     1      PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2      (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3      (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
          call FeQuestLblMake(id,tpom,il,
     1                        AsymFundamentalLabel(i),'L','N')
          if(nLblFundFr.le.0) nLblFundFr=LblLastMade
          nLblFundTo=LblLastMade
          if(KAsym(KDatBlock).ne.IdPwdAsymFundamental.or..not.Otevirat)
     1      call FeQuestLblOff(LblLastMade)
          if(i.le.2) then
            call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
            if(nEdwFundFr.le.0) nEdwFundFr=EdwLastMade
          else
            call FeMakeParEdwCrw(id,xpom,xpom,il,' ',nEdw,nCrw)
            if(nCrwFundRefFr.le.0) nCrwFundRefFr=nCrw
            if(i.eq.3) then
              nEdwRSW=nEdw
              nCrwRSW=nCrw
            else if(i.eq.4) then
              nEdwFDS=nEdw
              nCrwFDS=nCrw
            else if(i.eq.5) then
              nEdwVDS=nEdw
              nCrwVDS=nCrw
            else if(i.eq.9) then
              nEdwPSoll=nEdw
              nCrwPSoll=nCrw
            else if(i.eq.10) then
              nEdwSSoll=nEdw
              nCrwSSoll=nCrw
            endif
          endif
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
            if(i.eq.1) then
              pom=RadPrim(KDatBlock)
            else if(i.eq.2) then
              pom=RadSec(KDatBlock)
            else if(i.eq.3.or.i.eq.4) then
              pom=AsymPwd(i-2,KDatBlock)
            else
              pom=AsymPwd(i-3,KDatBlock)
            endif
          endif
          if(i.le.2) then
            if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
          else
            if(Otevirat) then
              if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),
     2                              .false.)
            else
              il=il-1
            endif
            if(i.ne.5) j=j+1
          endif
          if(i.eq.2.or.i.eq.5) then
            il=il+1
            call FeQuestLinkaFromToMake(id,tpom,xqdp-5.,il)
            if(nLinkaFundFr.le.0) nLinkaFundFr=LinkaLastMade
            nLinkaFundTo=LinkaLastMade
            if(KAsym(KDatBlock).ne.IdPwdAsymFundamental)
     1        call FeQuestLinkaOff(LinkaLastMade)
          endif
          il=il+1
        enddo
        il=5
        do i=1,5
          Otevirat=(i.ne.2.and.i.ne.3).or.
     1      PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2      (i.eq.2.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3      (i.eq.3.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
          if(i.eq.1) then
            lpom=KUseRSW(KDatBlock).eq.1
          else if(i.eq.2) then
            lpom=KUseDS(KDatBlock).eq.1
            il=il+1
          else if(i.eq.3) then
            lpom=KUseDS(KDatBlock).eq.2
            il=il+1
          else if(i.eq.4) then
            lpom=KUsePSoll(KDatBlock).eq.1
            il=il+5
          else if(i.eq.5) then
            lpom=KUseSSoll(KDatBlock).eq.1
            il=il+1
          endif
          call FeQuestCrwMake(id,xpomu,il,xpomu,il,' ','L',CrwXd,CrwYd,
     1                        1,0)
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.Otevirat)
     1      call FeQuestCrwOpen(CrwLastMade,lpom)
          if(.not.Otevirat) il=il-1
          if(i.eq.1) then
            nCrwFundSelFr=CrwLastMade
            nCrwUseRSW=CrwLastMade
            nEdw=nEdwRSW
            nCrw=nCrwRSW
          else if(i.eq.2) then
            nCrwUseFDS=CrwLastMade
            nEdw=nEdwFDS
            nCrw=nCrwFDS
          else if(i.eq.3) then
            nCrwUseVDS=CrwLastMade
            nEdw=nEdwVDS
            nCrw=nCrwVDS
          else if(i.eq.4) then
            nCrwUsePSoll=CrwLastMade
            nEdw=nEdwPSoll
            nCrw=nCrwPSoll
          else if(i.eq.5) then
            nCrwUseSSoll=CrwLastMade
            nEdw=nEdwSSoll
            nCrw=nCrwSSoll
          endif
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and..not.lpom)
     1      then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
        enddo
        nCrwFundSelTo=CrwLastMade
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        do i=1,12
          if(KAsym(KDatBlock).eq.IdPwdAsymDivergence.and.i.le.2)
     1      then
            if(KUseHS(KDatBlock).ge.1) then
              ii=0
            else
              ii=2
            endif
            Veta=LAsymPwdD(i+ii)
          else
            Veta=LAsymPwd
            call NToString(i,Veta(5:))
          endif
          call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,nCrw)
          if(i.eq.1) then
            nEdwAsymP=nEdw
            nCrwAsymP=nCrw
          endif
          if(i.le.NAsym(KDatBlock).and.
     1       KAsym(KDatBlock).ne.IdPwdAsymFundamental.and.
     2       KAsym(KDatBlock).ne.IdPwdAsymDebyeInt.and.
     3       KAsym(KDatBlock).ne.IdPwdAsymNone) then
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1        AsymPwd(i,KDatBlock),kiPwd(j),.false.)
          endif
          j=j+1
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
        enddo
        Veta='N%umber of terms'
        xpom=FeTxLengthUnder(Veta)+12.+CrwXd
        tpom=5.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNAsym=EdwLastMade
        if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
          call FeQuestIntEdwOpen(nEdwNAsym,NAsym(KDatBlock),.false.)
          call FeQuestEudOpen(nEdwNAsym,2,12,2,pom,pom,pom)
        endif
        do i=1,5
          xpom=max(xpom,FeTxLengthUnder(AsymDebyeIntLabel(i)))
        enddo
        il=1
        tpom=250.
        xpom=tpom+xpom+10.
        dpom=70.
        do i=1,6
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,AsymDebyeIntLabel(i),
     1                        'L',dpom,EdwYd,0)
          if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
            call FeQuestRealEdwOpen(EdwLastMade,AsymPwd(i,KDatBlock),
     1                              .false.,.false.)
            AsymPwdSave(i)=AsymPwd(i,KDatBlock)
          else
            AsymPwdSave(i)=AsymPwdDef(i)
          endif
          if(nEdwDebyeIntFr.le.0) nEdwDebyeIntFr=EdwLastMade
        enddo
        nEdwDebyeIntTo=EdwLastMade
      endif
      if(KAsym(KDatBlock).eq.IdPwdAsymDivergence)
     1  call FeQuestCrwOpen(nCrwKUseHS,KUseHS(KDatBlock).ge.1)
      go to 9999
      entry PwdOptionsAsymCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwAsymFr.and.
     1   CheckNumber.le.nCrwAsymTo) then
        KAsymNew=CheckNumber-nCrwAsymFr
        if(isTOF) then
          if(KAsymNew.eq.IdPwdAsymTOFNone) then
            NAsymNew=0
          else if(KAsymNew.eq.IdPwdAsymTOF1) then
            NAsymNew=4
          else if(KAsymNew.eq.IdPwdAsymTOF2) then
            NAsymNew=8
          endif
        else
          if(KAsymNew.eq.IdPwdAsymNone) then
            NAsymNew=0
          else if(KAsymNew.eq.IdPwdAsymSimpson) then
            NAsymNew=1
          else if(KAsymNew.eq.IdPwdAsymBerar) then
            NAsymNew=4
          else if(KAsymNew.eq.IdPwdAsymDivergence) then
            NAsymNew=2
          else if(KAsymNew.eq.IdPwdAsymFundamental) then
            NAsymNew=7
          else if(KAsymNew.eq.IdPwdAsymDebyeInt) then
            NAsymNew=6
          endif
        endif
        NAsymOld=NAsym(KDatBlock)
        NAsym(KDatBlock)=0
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwKUseHS) then
        call FeQuestRealFromEdw(nEdwAsymP,pom1)
        call FeQuestRealFromEdw(nEdwAsymP+1,pom2)
        if(CrwLogicQuest(CheckNumber)) then
          KUseHS(KDatBlock)=1
          AsymPwd(1,KDatBlock)=(pom1+pom2)*.5
          AsymPwd(2,KDatBlock)=(pom1-pom2)*.5
          i=0
        else
          KUseHS(KDatBlock)=0
          AsymPwd(1,KDatBlock)=pom1+pom2
          AsymPwd(2,KDatBlock)=pom1-pom2
          i=2
        endif
        call FeQuestEdwLabelChange(nEdwAsymP,LAsymPwdD(i+1))
        call FeQuestRealEdwOpen(nEdwAsymP,AsymPwd(1,KDatBlock),
     1                          .false.,.false.)
        call FeQuestEdwLabelChange(nEdwAsymP+1,LAsymPwdD(i+2))
        call FeQuestRealEdwOpen(nEdwAsymP+1,AsymPwd(2,KDatBlock),
     1                          .false.,.false.)
      else if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwUseRSW.or.
     1                                   CheckNumber.eq.nCrwUseFDS.or.
     2                                   CheckNumber.eq.nCrwUseVDS.or.
     3                                   CheckNumber.eq.nCrwUsePSoll.or.
     4                                   CheckNumber.eq.nCrwUseSSoll))
     5  then
        if(CheckNumber.eq.nCrwUseRSW) then
          nEdw=nEdwRSW
          nCrw=nCrwRSW
          i=1
        else if(CheckNumber.eq.nCrwUseFDS) then
          nEdw=nEdwFDS
          nCrw=nCrwFDS
          i=2
          if(CrwLogicQuest(CheckNumber).and.
     1       CrwStateQuest(nCrwUseVDS).ne.CrwClosed) then
            call FeQuestCrwOff(nCrwUseVDS)
            call FeQuestEdwClose(nEdwVDS)
            call FeQuestCrwClose(nCrwVDS)
            AsymPwd(i,KDatBlock)=1.
          endif
        else if(CheckNumber.eq.nCrwUseVDS) then
          nEdw=nEdwVDS
          nCrw=nCrwVDS
          i=2
          if(CrwLogicQuest(CheckNumber).and.
     1       CrwStateQuest(nCrwUseFDS).ne.CrwClosed) then
            call FeQuestCrwOff(nCrwUseFDS)
            call FeQuestEdwClose(nEdwFDS)
            call FeQuestCrwClose(nCrwFDS)
            AsymPwd(i,KDatBlock)=10.
          endif
        else if(CheckNumber.eq.nCrwUsePSoll) then
          nEdw=nEdwPSoll
          nCrw=nCrwPSoll
          i=6
        else if(CheckNumber.eq.nCrwUseSSoll) then
          nEdw=nEdwSSoll
          nCrw=nCrwSSoll
          i=7
        endif
        j=IAsymPwd+IZdvihRec+i-1
        if(CrwLogicQuest(CheckNumber)) then
          call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),.false.,
     1                            .false.)
          call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
          EventType=EventEdw
          EventNumber=nEdw
        else
          if(CrwLogicQuest(nCrw)) then
            kipwd(j)=1
          else
            kipwd(j)=0
          endif
          call FeQuestRealFromEdw(nEdw,AsymPwd(i,KDatBlock))
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        go to 9999
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNAsym) then
        call FeQuestIntFromEdw(nEdwNAsym,NAsymNew)
      endif
      go to 2500
      entry PwdOptionsAsymRefresh(KAsymNewIn)
      KAsymNew=KAsymNewIn
      if(isTOF) then
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          NAsymNew=0
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymTOF2) then
          NAsymNew=8
        endif
      else
        if(KAsymNew.eq.IdPwdAsymNone) then
          NAsymNew=0
        else if(KAsymNew.eq.IdPwdAsymSimpson) then
          NAsymNew=1
        else if(KAsymNew.eq.IdPwdAsymBerar) then
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymDivergence) then
          NAsymNew=2
        else if(KAsymNew.eq.IdPwdAsymFundamental) then
          NAsymNew=7
        else if(KAsymNew.eq.IdPwdAsymDebyeInt) then
          NAsymNew=6
        endif
      endif
2500  if(isTOF) then
        if(KAsym(KDatBlock).eq.IdPwdAsymTOFNone) then
          n=0
        else if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          n=1
        else
          n=2
        endif
        if(n.gt.0)
     1    call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     2                              AsymOrg(1,n),kiAsymOrg(1,n),
     3                              NAsymOld)
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          n=0
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          n=1
        else
          n=2
        endif
        nEdw=nEdwAsymP
        nCrw=nCrwAsymP
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          do i=1,8
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          j=IAsymPwd+IZdvihRec
          do i=1,8
            if(i.le.4) then
              call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1          AsymOrg(i,n),kiAsymOrg(i,n),.false.)
              call FeQuestEdwLabelChange(nEdw,lAsymTOF1Pwd(i))
            else
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymTOF2) then
          j=IAsymPwd+IZdvihRec
          do i=1,8
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1        AsymOrg(i,n),kiAsymOrg(i,n),.false.)
            call FeQuestEdwLabelChange(nEdw,lAsymTOF2Pwd(i))
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
          NAsymNew=8
        endif
      else
        if(KAsymNew.ne.IdPwdAsymFundamental.and.
     1     KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          do i=nLblFundFr,nLblFundTo
            call FeQuestLblOff(i)
          enddo
          do i=nLinkaFundFr,nLinkaFundTo
            call FeQuestLinkaOff(i)
          enddo
          do i=nCrwFundSelFr,nCrwFundSelTo
            call FeQuestCrwClose(i)
          enddo
          nEdw=nEdwFundFr
          nCrw=nCrwFundRefFr
          do i=1,10
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
            if(i.gt.2) then
              call FeQuestCrwClose(nCrw)
              nCrw=nCrw+1
            endif
          enddo
        endif
        if(KAsymNew.ne.IdPwdAsymDebyeInt.and.
     1     KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
          nEdw=nEdwDebyeIntFr
          do i=1,6
            if(EdwStateQuest(nEdw).eq.EdwOpened)
     1        call FeQuestRealFromEdw(nEdw,AsymPwdSave(i))
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
        endif
        if(KAsymNew.ne.IdPwdAsymBerar) then
          nLbl=nLblFirst
          do i=1,2
            call FeQuestLblOff(nLbl)
            nLbl=nLbl+1
          enddo
          if(EdwStateQuest(nEdwNAsym).ne.EdwClosed) then
            call FeQuestEdwClose(nEdwNAsym)
          endif
        else if(KAsymNew.eq.IdPwdAsymBerar) then
          if(EdwStateQuest(nEdwNAsym).ne.EdwOpened) then
            nLbl=nLblFirst
            do i=1,2
              call FeQuestLblOn(nLbl)
              nLbl=nLbl+1
            enddo
            call FeQuestIntEdwOpen(nEdwNAsym,NAsymNew,.false.)
            call FeQuestEudOpen(nEdwNAsym,2,12,2,pom,pom,pom)
          endif
        endif
        if(KAsymNew.ne.IdPwdAsymDivergence.and.
     1     CrwStateQuest(nCrwKUseHS).ne.CrwClosed)
     2     call FeQuestCrwClose(nCrwKUseHS)
        nEdw=nEdwAsymP
        nCrw=nCrwAsymP
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        do i=1,12
          if((KAsymNew.eq.IdPwdAsymDivergence.or.
     1        KAsym(KDatBlock).eq.IdPwdAsymDivergence).and.
     2        i.le.2.and.KAsymNew.ne.KAsym(KDatBlock)) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            if(KAsymNew.eq.IdPwdAsymDivergence) then
              if(i.eq.1) then
                AsymPwd(i,KDatBlock)= 0.01
                KUseHS(KDatBlock)=0
              else if(i.eq.2) then
                 AsymPwd(i,KDatBlock)=-0.005
              endif
              Veta=LAsymPwdD(i+2)
            else
              AsymPwd(i,KDatBlock)=0.01
              Veta=lAsymPwd
              call NToString(i,Veta(5:))
            endif
            call FeQuestEdwLabelChange(nEdw,Veta)
            call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),.false.,
     1                              .false.)
            call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
          endif
          if(i.gt.NAsymNew.or.KAsymNew.eq.IdPwdAsymFundamental.or.
     1       KAsymNew.eq.IdPwdAsymDebyeInt) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          else if(i.le.NAsymNew.and.KAsymNew.ne.IdPwdAsymDivergence)
     1      then
            if(i.gt.NAsym(KDatBlock)) then
              AsymPwd(i,KDatBlock)=0.001
              call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),
     1                                .false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
        enddo
        if(KAsymNew.eq.IdPwdAsymFundamental.and.
     1     KAsym(KDatBlock).ne.IdPwdAsymFundamental) then
          call PwdSetFundamentalParameters
          i=0
          do nLbl=nLblFundFr,nLblFundTo
            i=i+1
            Otevirat=(i.ne.4.and.i.ne.5).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(Otevirat) call FeQuestLblOn(nLbl)
          enddo
          do i=nLinkaFundFr,nLinkaFundTo
            call FeQuestLinkaOn(i)
          enddo
          j=IAsymPwd+IZdvihRec
          nEdw=nEdwFundFr
          nCrw=nCrwFundRefFr
          do i=1,10
            Otevirat=(i.ne.4.and.i.ne.5).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(i.eq.1) then
              pom=RadPrim(KDatBlock)
            else if(i.eq.2) then
              pom=RadSec(KDatBlock)
            else if(i.eq.3.or.i.eq.4) then
              pom=AsymPwd(i-2,KDatBlock)
            else
              pom=AsymPwd(i-3,KDatBlock)
            endif
            if((i.eq.4.and.KUseDS(KDatBlock).ne.1).or.
     1         (i.eq.5.and.KUseDS(KDatBlock).ne.2)) then
              nCrw=nCrw+1
              nEdw=nEdw+1
              cycle
            endif
            if(i.le.2) then
              if(Otevirat)
     1          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            else
              if(Otevirat)
     1          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),
     2                               .false.)
              nCrw=nCrw+1
            endif
            nEdw=nEdw+1
          enddo
          i=0
          do nCrwP=nCrwFundSelFr,nCrwFundSelTo
            i=i+1
            Otevirat=(i.ne.2.and.i.ne.3).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.2.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.3.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(i.eq.1) then
              lpom=KUseRSW(KDatBlock).eq.1
            else if(i.eq.2) then
              lpom=KUseDS(KDatBlock).eq.1
            else if(i.eq.3) then
              lpom=KUseDS(KDatBlock).eq.2
            else if(i.eq.4) then
              lpom=KUsePSoll(KDatBlock).eq.1
            else if(i.eq.5) then
              lpom=KUseSSoll(KDatBlock).eq.1
            endif
            if(Otevirat) call FeQuestCrwOpen(nCrwP,lpom)
            if(i.eq.1) then
              nEdw=nEdwRSW
              nCrw=nCrwRSW
            else if(i.eq.2) then
              nEdw=nEdwFDS
              nCrw=nCrwFDS
            else if(i.eq.3) then
              nEdw=nEdwVDS
              nCrw=nCrwVDS
            else if(i.eq.4) then
              nEdw=nEdwPSoll
              nCrw=nCrwPSoll
            else if(i.eq.5) then
              nEdw=nEdwSSoll
              nCrw=nCrwSSoll
            endif
            if(.not.lpom) then
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
          enddo
        else if(KAsymNew.eq.IdPwdAsymDebyeInt.and.
     1          KAsym(KDatBlock).ne.IdPwdAsymDebyeInt) then
          nEdw=nEdwDebyeIntFr
          do i=1,6
            call FeQuestRealEdwOpen(nEdw,AsymPwdSave(i),.false.,.false.)
            nEdw=nEdw+1
          enddo
        endif
      endif
      if(KAsymNew.eq.IdPwdAsymDivergence.and.
     1   CrwStateQuest(nCrwKUseHS).eq.CrwClosed)
     2  call FeQuestCrwOpen(nCrwKUseHS,KUseHS(KDatBlock).ge.1)
      KAsym(KDatBlock)=KAsymNew
      NAsym(KDatBlock)=NAsymNew
      go to 9999
      entry PwdOptionsAsymUpdate
      if(KAsym(KDatBlock).ne.IdPwdAsymFundamental.and.
     1   KAsym(KDatBlock).ne.IdPwdAsymDebyeInt.and.
     2   KAsym(KDatBlock).ne.IdPwdAsymNone) then
        call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     1    AsymPwd(1,KDatBlock),kiPwd(IAsymPwd+IZdvihRec),
     2    NAsym(KDatBlock))
      else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        call FeQuestRealFromEdw(nEdwFundFr  ,RadPrim(KDatBlock))
        call FeQuestRealFromEdw(nEdwFundFr+1,RadSec (KDatBlock))
        nEdw=nEdwFundFr+2
        nCrw=nCrwFundRefFr
        j=IAsymPwd+IZdvihRec
        k=1
        do i=3,10
          if(EdwStateQuest(nEdw).ne.EdwClosed) then
            call FeUpdateParamAndKeys(nEdw,nCrw,AsymPwd(k,KDatBlock),
     1                                kiPwd(j),1)
          endif
          if(i.ne.4) then
            j=j+1
            k=k+1
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        nCrw=nCrwFundSelFr
        KUseRSW(KDatBlock)=0
        KUseDS(KDatBlock)=0
        KUsePSoll(KDatBlock)=0
        KUseSSoll(KDatBlock)=0
        nCrw=nCrwFundSelFr
        do i=1,nCrwFundSelTo-nCrwFundSelFr+1
          if(CrwStateQuest(nCrw).ne.CrwClosed) then
            lpom=CrwLogicQuest(nCrw)
            if(i.eq.1) then
              if(lpom) KUseRSW(KDatBlock)=1
            else if(i.eq.2) then
              if(lpom) KUseDS(KDatBlock)=1
            else if(i.eq.3) then
              if(lpom) KUseDS(KDatBlock)=2
            else if(i.eq.4) then
              if(lpom) KUsePSoll(KDatBlock)=1
            else if(i.eq.5) then
              if(lpom) KUseSSoll(KDatBlock)=1
            endif
          endif
          nCrw=nCrw+1
        enddo
      else if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
        nEdw=nEdwDebyeIntFr
        j=IAsymPwd+IZdvihRec
        do i=1,6
          call FeQuestRealFromEdw(nEdw,AsymPwd(i,KDatBlock))
          nEdw=nEdw+1
          kiPwd(j)=0
        enddo
      endif
9999  return
      end
