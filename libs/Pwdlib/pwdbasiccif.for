      subroutine PwdBasicCIF(ln,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ifc(3),ra(8)
      character*256 Radka,Veta
      integer Possibilities(3)
      logical EqIgCase,AlreadyAllocated
      save ifc,NData,mm,AlreadyAllocated
      data Possibilities/114,130,147/
      if(allocated(CifKey)) then
        AlreadyAllocated=.true.
      else
        AlreadyAllocated=.false.
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(ln.eq.0) then
        mm=0
      else
        rewind ln
      endif
1000  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 9000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=9000) Radka
      endif
      k=0
      call kus(Radka,k,Veta)
      call mala(Veta)
      if(Veta.eq.'loop_') then
        NData=0
        call SetIntArrayTo(ifc,3,0)
1100    if(ln.eq.0) then
          mm=mm+1
          if(mm.gt.nCIFUsed) go to 9000
          Radka=CIFArray(mm)
        else
          read(ln,FormA,end=9000) Radka
        endif
        k=0
        call kus(Radka,k,Veta)
        if(Veta.eq.' ') go to 1100
        i=1
1110    if(Veta(i:i).eq.' ') then
          i=i+1
          go to 1110
        endif
        Veta=Veta(i:)
        call mala(Veta)
        if(Veta(1:1).ne.'_') go to 1200
        NData=NData+1
        do i=1,3
          if(EqIgCase(Veta,CifKey(Possibilities(i),20))) ifc(i)=NData
        enddo
        go to 1100
1200    n=0
        do i=1,3
          if(ifc(i).ne.0) n=n+1
        enddo
        if(n.le.0) then
          go to 1000
        else if(n.lt.3) then
          go to 9100
        endif
        if(NData.gt.0) then
          if(ln.eq.0) then
            mm=mm-1
          else
            backspace ln
          endif
          go to 9999
        endif
      endif
      go to 1000
      entry PwdReadCIF(ln,Klic,ik)
      ik=0
2000  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 3000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=3000) Radka
      endif
      if(Radka.eq.' ') go to 2000
      k=0
      call kus(Radka,k,Veta)
      if(Veta(1:1).eq.'_'.or.index(Veta,'data_').eq.1.or.
     1                       index(Veta,'loop_').eq.1) then
        ik=1
      else if(Veta(1:1).eq.'#') then
        go to 2000
      else
        k=0
        call StToReal(Radka,k,ra,NData,.false.,ich)
        if(ich.ne.0) go to 9900
        ih(1)=nint(ra(ifc(1))*1000000.)
        ri=ra(ifc(2))
        rs=ra(ifc(3))
      endif
      go to 9999
3000  ik=1
      go to 9999
9000  Veta='before the end of CIF file is reached.'
      go to 9900
9100  Veta='or some of items h, k, l, I, sig(I) is missing.'
9900  if(Klic.eq.0) then
        Radka='CIF file does not contain any reflection block'
        call FeChybne(-1.,-1.,Radka,Veta,SeriousError)
      endif
      ErrFlag=1
9999  if(allocated(CifKey).and..not.AlreadyAllocated)
     1   deallocate(CifKey,CifKeyFlag)
      return
      end
