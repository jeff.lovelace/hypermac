      subroutine PwdGetLamFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80  Veta
      character*258 ListDir,ListFile,CurrentDirO
      integer FeChDir
      CurrentDirO=CurrentDir
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'lam'//ObrLom
      else
        Veta=HomeDir(:idel(HomeDir))//'lam/'
      endif
      i=FeChdir(Veta)
      call FeGetCurrentDir()
      ListFile='jfile'
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListDir)
      call FeTmpFilesAdd(ListFile)
      i=KLam(KDatBlock)
      if(i.gt.0) then
        Veta=LamTypeD(i)(:idel(LamTypeD(i)))//'*.lam'
      else
        Veta='*.lam'
      endif
      call FeDir(ListDir,ListFile,Veta,0)
      call DeleteFile(ListDir)
      call FeTmpFilesClear(ListDir)
      ln=NextLogicNumber()
      open(ln,file=ListFile)
      nLamFiles=0
1120  read(ln,FormA,end=1130) Veta
      if(nLamFiles.lt.20) then
        nLamFiles=nLamFiles+1
        LamNames(nLamFiles)=Veta
        go to 1120
      endif
1130  close(ln)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListFile)
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      return
      end
