      subroutine PwdOptionsRefresh(KartIdOld)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      dimension GaussPwdOld(4),GaussPwdNew(4)
      integer CrwStateQuest,EdwStateQuest
!      integer :: KPhaseOld=1
      logical lpom,CrwLogicQuest
      save KLamOld,/PwdOptionsC/
!      if(NPhase.gt.1) then
!        nRolMenu=nRolMenuPhase-RolMenuFr+1
!        if(KartId.eq.KartIdProf) then
!          KPhaseOld=KPhase
!          KPhase=1
!          if(KeyMenuPhaseUse.eq.0) then
!            call FeQuestRolMenuClose(nRolMenu)
!            call FeQuestRolMenuWithEnableOpen(nRolMenu,PhaseName,
!     1                                       MenuPhaseUse,NPhase,1)
!          endif
!          KeyMenuPhaseUse=1
!        else
!          if(KeyMenuPhaseUse.ne.0) then
!            KPhase=KPhaseOld
!            call FeQuestRolMenuClose(nRolMenu)
!            call FeQuestRolMenuOpen(nRolMenu,PhaseName,NPhase,KPhase)
!          endif
!          KeyMenuPhaseUse=0
!        endif
!      endif
      if(KartId.eq.KartIdVarious) then
        if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          klam(KDatBlock)=LocateInArray(LamAve(KDatBlock),LamAveD,7,
     1                                 .0001)
          if(klam(KDatBlock).le.0)
     1      klam(KDatBlock)=LocateInArray(LamA1(KDatBlock),LamA1D,7,
     2                                    .0001)
          if(CrwStateQuest(nCrwUseLamFile).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwRefLam)
            nEdw=nEdwLam
            nCrw=nCrwLam
            do i=1,NAlfa(KDatBlock)
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
              nEdw=nEdw+1
              nCrw=nCrw+1
            enddo
            call FeQuestCrwOpen(nCrwUseLamFile,.false.)
            KLamOld=-1
          else
            if(CrwLogicQuest(nCrwUseLamFile)) then
              if(KLamOld.ne.KLam(KDatBlock)) then
                if(KLam(KDatBlock).gt.0) then
                  call PwdGetLamFiles
                  if(nLamFiles.gt.0) then
                    call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                                      nLamFiles,1)
                  else
                    call FeQuestCrwDisable(nCrwUseLamFile)
                    call FeQuestRolMenuClose(nRolMenuLamFile)
                  endif
                  KLamOld=KLam(KDatBlock)
                else
                  call FeQuestRolMenuClose(nRolMenuLamFile)
                  KLamOld=-1
                endif
              endif
            else if(CrwStateQuest(nCrwUseLamFile).eq.CrwDisabled) then
              call FeQuestCrwOpen(nCrwUseLamFile,.false.)
            else
              KLamOld=-1
            endif
          endif
          if(klam(KDatBlock).le.0)
     1      call FeQuestCrwDisable(nCrwUseLamFile)
        else if(nCrwRefLam.ne.0) then
          if(CrwStateQuest(nCrwRefLam).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwUseLamFile)
            call FeQuestRolMenuClose(nRolMenuLamFile)
            call FeQuestCrwOpen(nCrwRefLam,KRefLam(KDatBlock).eq.1)
            if(KRefLam(KDatBlock).eq.1) then
              nEdw=nEdwLam
              nCrw=nCrwLam
              IZdvihRec=(KDatBlock-1)*NParRecPwd
              j=ILamPwd+IZdvihRec
              do i=1,NAlfa(KDatBlock)
                call FeQuestRealEdwOpen(nEdw,LamPwd(i,KDatBlock),
     1                                  .false.,.false.)
                call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
                j=j+1
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
      else if(KartId.eq.KartIdProf) then
        if(.not.isTOF) then
          if(KAsym(KDatBlock).ne.LastAsymProfile) then
            if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwLorentz
              nCrw=nCrwLorentz
              do i=1,5
                if(i.ne.5)
     1            call FeQuestEdwLabelChange(nEdw,lLorentzPwdF(i))
                if(mod(i,2).eq.0.or.i.eq.5) then
                  call FeQuestEdwDisable(nEdw)
                  call FeQuestCrwDisable(nCrw)
                else
                  if(lpom) then
                    call FeQuestRealFromEdw(nEdw,pom)
                  else
                    pom=LorentzPwd(i,KPhase,KDatBlock)
                  endif
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=LorentzToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=max(pom*LorentzToStrain,.01)
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  LorentzPwd(i,KPhase,KDatBlock)=pom
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,lGaussPwdF(i))
                if(lpom) then
                  call FeQuestRealFromEdw(nEdw,GaussPwdOld(i))
                else
                  GaussPwdOld(i)=GaussPwd(i,KPhase,KDatBlock)
                endif
                if(mod(i,2).eq.0) then
c                  call FeQuestEdwClose(nEdw)
c                  call FeQuestCrwClose(nCrw)
                  call FeQuestEdwDisable(nEdw)
                  call FeQuestCrwDisable(nCrw)
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              GaussPwdNew(1)=sqrt(max(GaussPwdOld(4)+GaussPwdOld(3),0.))
              GaussPwdNew(2)=0.
              GaussPwdNew(3)=sqrt(max(GaussPwdOld(1)-
     1                                GaussPwdOld(3),.01))
              GaussPwdNew(4)=0.
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                pom=GaussPwdNew(i)
                if(mod(i,2).ne.0) then
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=GaussToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=pom*GaussToStrain
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  GaussPwd(i,KPhase,KDatBlock)=pom
                endif
                GaussPwd(i,KPhase,KDatBlock)=pom
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              call FeQuestLblOff(nLblStrain)
              call FeQuestEdwDisable(nEdwDirBroad)
              call FeQuestCrwDisable(nCrwAniPBroad)
              call FeQuestButtonDisable(nButEditTensor)
              nCrw=nCrwStrain
              do i=1,3
                call FeQuestCrwDisable(nCrw)
                nCrw=nCrw+1
              enddo
            else if(LastAsymProfile.eq.IdPwdAsymFundamental) then
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwLorentz
              nCrw=nCrwLorentz
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,
     1            lLorentzPwd(i)(:idel(lLorentzPwd(i))))
                if(mod(i,2).eq.1) then
                  if(lpom) then
                    call FeQuestRealFromEdw(nEdw,pom)
                  else
                    pom=LorentzPwd(i,KPhase,KDatBlock)
                  endif
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=LorentzToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=max(pom/LorentzToStrain,0.)
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  LorentzPwd(i,KPhase,KDatBlock)=pom
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,
     1            lGaussPwd(i)(:idel(lGaussPwd(i))))
                if(lpom.and.EdwStateQuest(nEdw).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdw,pom)
                else
                  pom=GaussPwd(i,KPhase,KDatBlock)
                endif
                pom=abs(pom)
                if(i.eq.1) then
                  if(pom.le.0.) pom=100.
                  pom=min(GaussToCrySize(KDatBlock)/pom,10.)
                else if(i.eq.3) then
                  pom=min(pom/GaussToStrain,10.)
                endif
                GaussPwdOld(i)=pom**2
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              GaussPwdNew(1)=GaussPwdOld(3)
              GaussPwdNew(2)=0.
              GaussPwdNew(3)=0.
              GaussPwdNew(4)=GaussPwdOld(1)
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                if(lpom) then
                  if(EdwStateQuest(nEdw).ne.EdwOpened)
     1              call FeQuestCrwOpen(nCrw,.false.)
                  call FeQuestRealEdwOpen(nEdw,GaussPwdNew(i),.false.,
     1                                    .false.)
                endif
                GaussPwd(i,KPhase,KDatBlock)=GaussPwdNew(i)
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              call FeQuestLblOn(nLblStrain)
              call FeQuestCrwOpen(nCrwAniPBroad,.false.)
              nCrw=nCrwStrain
              do i=1,3
                call FeQuestCrwOpen(nCrw,i.eq.1)
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        LastAsymProfile=KAsym(KDatBlock)
      else if(KartId.eq.KartIdAsym) then
        if(isTOF) then
          if(KUseTOFJason(KDatBlock).le.0) then
            call FeQuestCrwDisable(nCrwAsymFr+2)
            if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
              call FeQuestCrwOn(nCrwAsymFr+1)
              call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     1                                  AsymOrg(1,2),kiAsymOrg(1,2),8)
              nEdw=nEdwAsymP
              nCrw=nCrwAsymP
              do i=1,8
                if(i.le.4) then
                  call FeQuestEdwLabelChange(nEdw,lAsymTOF1Pwd(i))
                  call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1              AsymOrg(i,1),kiAsymOrg(i,1),.false.)
                else
                  call FeQuestEdwClose(nEdw)
                  call FeQuestCrwClose(nCrw)
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              KAsym(KPhase)=IdPwdAsymTOF1
              NAsymOld=4
            endif
          else
            if(CrwStateQuest(nCrwAsymFr+2).ne.CrwOn.and.
     1         CrwStateQuest(nCrwAsymFr+2).ne.CrwOff)
     2        call FeQuestCrwOff(nCrwAsymFr+2)
          endif
        else
          KAsymNew=KAsym(KDatBlock)
          if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
            if(KAsym(KDatBlock).ne.IdPwdAsymNone.and.
     1         KAsym(KDatBlock).ne.IdPwdAsymDebyeInt)
     2        KAsymNew=IdPwdAsymNone
            j=0
            do nCrw=nCrwAsymFr,nCrwAsymTo
              if(nCrw.ne.nCrwAsymFr.and.nCrw.ne.nCrwAsymTo) then
                call FeQuestCrwDisable(nCrw)
              else
                if(j.eq.KAsymNew) then
                  call FeQuestCrwOn(nCrw)
                else
                  call FeQuestCrwOff(nCrw)
                endif
              endif
              j=j+1
            enddo
          else
            if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt)
     1        KAsymNew=IdPwdAsymNone
            j=0
            do nCrw=nCrwAsymFr,nCrwAsymTo
              if(nCrw.eq.nCrwAsymTo) then
                call FeQuestCrwDisable(nCrw)
              else
                if(j.eq.KAsymNew) then
                  call FeQuestCrwOn(nCrw)
                else
                  call FeQuestCrwOff(nCrw)
                endif
              endif
              j=j+1
            enddo
          endif
          if(KAsym(KDatBlock).ne.KAsymNew)
     1      call PwdOptionsAsymRefresh(KAsymNew)
        endif
      endif
      return
      end
