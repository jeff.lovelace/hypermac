      subroutine PwdOptionsVarious
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      integer RolMenuStateQuest,EdwStateQuest,RolMenuSelectedQuest
      logical :: DefInD = .true.,CrwLogicQuest
      save nEdwCutoffMin,nEdwCutoffMax,
     1     nCrwWleBail,nCrwKleBail,
     2     nCrwUseCutoff,nCrwInD,nCrwInSinThL
      save /PwdOptionsC/
      entry PwdOptionsVariousMake(id)
      xpom=5.
      tpom=xpom+CrwXd+3.
      il=1
      Veta='%Apply weights in leBail decomposition'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwWleBail=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KWleBail(KDatBlock).eq.1)
      il=il+1
      Veta='%Use structure of known phases in leBail decomposition'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwKleBail=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KKleBail(KDatBlock).eq.1)
      if(.not.isTOF) then
        il=il+1
        Veta='Allow refinement of the %wavelength'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwRefLam=CrwLastMade
        if(KAsym(KDatBlock).ne.IdPwdAsymFundamental)
     1    call FeQuestCrwOpen(CrwLastMade,KRefLam(KDatBlock).eq.1)
        il=il+1
        Veta=lLamPwd
        idl=idel(Veta)+1
        j=ILamPwd+IZdvihRec
        do i=1,NAlfa(KDatBlock)
          write(Veta(idl:),'(i1)') i
          call FeMakeParEdwCrw(id,tpoma(i),xpoma(i),il,Veta,nEdw,nCrw)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',LamPwd(1,KDatBlock),
     1                         kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwLam=nEdw
            nCrwLam=nCrw
          endif
          if(KRefLam(KDatBlock).ne.1.or.
     1       (.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental))
     2      then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          j=j+1
        enddo
        if(nLamFiles.gt.0) then
          il=il-1
          Veta='Use predefined %radiation profile'
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwUseLamFile=CrwLastMade
          il=il+1
          Veta='%File with radiation profile'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          dpom=120.
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,0)
          nRolMenuLamFile=RolMenuLastMade
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1      then
            call FeQuestCrwOpen(CrwLastMade,KUseLamFile(KDatBlock).eq.1)
            if(KUseLamFile(KDatBlock).eq.1) then
              i=LocateInStringArray(LamNames,nLamFiles,
     1                              LamFile(KDatBlock),.true.)
              i=max(i,1)
              call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                                nLamFiles,i)
            endif
          endif
        else
          nCrwUseLamFile=0
          nRolMenuLamFile=0
        endif
        xpom=5.
        tpom=xpom+CrwXd+3.
      else
        nCrwRefLam=0
        nEdwLam=0
        nCrwLam=0
        nCrwUseLamFile=0
        nRolMenuLamFile=0
      endif
      il=il+1
      Veta='Use global %cutoff'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseCutoff=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseCutOffPwd)
      Veta='Defined in %d'
      xpom=xpom+10.
      tpom=tpom+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        if(i.eq.1) then
          nCrwInD=CrwLastMade
          Veta='Defined in %sin(th)/lam'
        else
          nCrwInSinThL=CrwLastMade
        endif
        if(UseCutOffPwd)
     1    call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.DefInD)
      enddo
      Veta='Cutoff(m%in)'
      tpom=150.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      il=il-2
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwCutoffMin=EdwLastMade
          Veta='Cutoff(ma%x)'
          if(DefInD) then
            pom=CutOffMinPwd
          else
            if(CutOffMaxPwd.eq.0.) then
              pom=50.
            else
              pom=.5/CutOffMaxPwd
            endif
          endif
        else
          nEdwCutoffMax=EdwLastMade
          if(DefInD) then
            pom=CutOffMaxPwd
          else
            if(CutOffMinPwd.eq.0.) then
              pom=50.
            else
              pom=.5/CutOffMinPwd
            endif
          endif
        endif
        if(UseCutOffPwd)
     1    call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      enddo
      go to 9999
      entry PwdOptionsVariousCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRefLam) then
        if(CrwLogicQuest(nCrwRefLam)) then
          KRefLam(KDatBlock)=1
        else
          KRefLam(KDatBlock)=0
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseLamFile)
     1  then
        if(CrwLogicQuest(nCrwUseLamFile)) then
          KUseLamFile(KDatBlock)=1
          call PwdGetLamFiles
        else
          KUseLamFile(KDatBlock)=0
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwInD.or.
     1        CheckNumber.eq.nCrwInSinThL) then
        call FeQuestRealFromEdw(nEdwCutoffMin,pom)
        if(pom.eq.0.) then
          CutOffMaxPwd=50.
        else
          CutOffMaxPwd=.5/pom
        endif
        call FeQuestRealFromEdw(nEdwCutoffMax,pom)
        if(pom.eq.0.) then
          CutOffMinPwd=50.
        else
          CutOffMinPwd=.5/pom
        endif
        call FeQuestRealEdwOpen(nEdwCutoffMin,CutOffMinPwd,.false.,
     1                          .false.)
        call FeQuestRealEdwOpen(nEdwCutoffMax,CutOffMaxPwd,.false.,
     1                          .false.)
        DefInD=CrwLogicQuest(nCrwInD)
      endif
2500  if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        if(KUseLamFile(KDatBlock).eq.1) then
          i=LocateInStringArray(LamNames,nLamFiles,
     1                          LamFile(KDatBlock),.true.)
          i=max(i,1)
          call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                            nLamFiles,i)
        else
          if(RolMenuStateQuest(nRolMenuLamFile).eq.RolMenuOpened) then
            i=RolMenuSelectedQuest(nRolMenuLamFile)
            LamFile(KDatBlock)=LamNames(i)
          endif
          call FeQuestRolMenuClose(nRolMenuLamFile)
        endif
      else
        nEdw=nEdwLam
        nCrw=nCrwLam
        j=ILamPwd+IZdvihRec
        do i=1,NAlfa(KDatBlock)
          if(KRefLam(KDatBlock).eq.1) then
            call FeQuestRealEdwOpen(nEdw,LamPwd(i,KDatBlock),.false.,
     1                              .false.)
            call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
      endif
      if(CrwLogicQuest(nCrwUseCutoff)) then
        if(EdwStateQuest(nEdwCutoffMin).ne.EdwOpened) then
          call FeQuestCrwOpen(nCrwInD,DefInD)
          call FeQuestCrwOpen(nCrwInSinThL,.not.DefInD)
          call FeQuestRealEdwOpen(nEdwCutoffMin,CutOffMinPwd,.false.,
     1                            .false.)
          call FeQuestRealEdwOpen(nEdwCutoffMax,CutOffMaxPwd,.false.,
     1                            .false.)
        endif
      else
        call FeQuestCrwClose(nCrwInD)
        call FeQuestCrwClose(nCrwInSinThL)
        call FeQuestEdwClose(nEdwCutoffMin)
        call FeQuestEdwClose(nEdwCutoffMax)
      endif
      go to 9999
      entry PwdOptionsVariousUpdate
      if(KRefLam(KDatBlock).eq.1) then
        j=ILamPwd+IZdvihRec
        call FeUpdateParamAndKeys(nEdwLam,nCrwLam,LamPwd(1,KDatBlock),
     1                            kiPwd(j),2)
      endif
      if(CrwLogicQuest(nCrwWleBail)) then
        KWleBail(KDatBlock)=1
      else
        KWleBail(KDatBlock)=0
      endif
      if(CrwLogicQuest(nCrwKleBail)) then
        KKleBail(KDatBlock)=1
      else
        KKleBail(KDatBlock)=0
      endif
      if(KUseLamFile(KDatBlock).eq.1) then
        i=RolMenuSelectedQuest(nRolMenuLamFile)
        LamFile(KDatBlock)=LamNames(i)
      endif
      UseCutOffPwd=CrwLogicQuest(nCrwUseCutoff)
      if(UseCutOffPwd) then
        call FeQuestRealFromEdw(nEdwCutoffMin,pom)
        if(DefInD) then
          CutOffMinPwd=pom
        else
          if(pom.eq.0.) then
            CutOffMaxPwd=0.
          else
            CutOffMaxPwd=.5/pom
          endif
        endif
        call FeQuestRealFromEdw(nEdwCutoffMax,pom)
        if(DefInD) then
          CutOffMaxPwd=pom
        else
          if(pom.eq.0.) then
            CutOffMinPwd=0.
          else
            CutOffMinPwd=.5/pom
          endif
        endif
      endif
9999  return
      end
