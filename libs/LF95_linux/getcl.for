C
C
      subroutine getcl(cl_args)
      character*(*), intent(out) :: cl_args
      integer :: str_length
      integer :: exit_status
      call get_command(cl_args, str_length, exit_status)
      exit_status = scan(cl_args, " ")
      cl_args=cl_args(exit_status+1:)
      cl_args=adjustl(cl_args)
      end
