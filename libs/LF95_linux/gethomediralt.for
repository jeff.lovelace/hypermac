C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
      subroutine GetHomeDirAlt(strHomeDir,iStatus)
      USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT
      character*(*), intent(out) :: strHomeDir
      integer , intent(out) :: iStatus
      integer :: iIndex
      logical :: lExists
      CHARACTER(len=512) :: strBuffer
      call getcwd(strBuffer)

      strHomeDir=trim(strBuffer)//'/'
      call get_command(strBuffer)
      iIndex = index(strBuffer,'a.out')
      if (strBuffer(1:1).EQ.'.') then
        strHomeDir=trim(strHomeDir)//trim(strBuffer(:iIndex-1))
      else
        strHomeDir=trim(strBuffer(:iIndex-1))
      end if
      iIndex = index(strHomeDir,'bin/',.TRUE.)
      if (iIndex.NE.0) then
        strHomeDir=strHomeDir(:iIndex-1)
      end if
      strBuffer=trim(strHomeDir)//"SYMMDAT/pgroup.dat"
      INQUIRE(FILE=trim(strBuffer), EXIST=lExists)
      if (.NOT.lExists) then
        WRITE(ERROR_UNIT,*) "Could not get the HomeDir."//
     +                 " Please set JANA2006_DIR variable"//
     +                 " and try again."
        call exit(-1)
      end if
      end
