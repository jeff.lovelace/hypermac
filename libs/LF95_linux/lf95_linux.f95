C     subroutine replaces the Leahy Fortran supplied getcl
C
      subroutine getcl(cl_args)
      character*(*), intent(out) :: cl_args
      integer :: str_length
      integer :: exit_status
      call get_command(cl_args, str_length, exit_status) 
      exit_status = scan(cl_args, " ")
      cl_args=cl_args(exit_status+1:)
      cl_args=adjustl(cl_args)
      end

C     subroutine replaces the timer function
C
      subroutine timer(seconds)
      integer, intent(out) :: seconds
      real :: elapsed_time
      call cpu_time(elapsed_time)
      seconds = nint(elapsed_time*100)
      end

C     function returns the Jana Home Directory where
C     configuration files can found
C
      function GetHomeDir()
      USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT
      character(len=512) :: NewHomeDir,GetHomeDir
      integer :: iLenght,iStatus
      CALL GET_ENVIRONMENT_VARIABLE('JANA2006_DIR', NewHomeDir, iLength
     +, iStatus)
      if (iStatus.EQ.1) then ! Try Alternate Approach
        call GetHomeDirAlt(NewHomeDir,iLength)
        GetHomeDir=trim(NewHomeDir)
        return
      else if (iStatus.EQ.2) then ! Try Alternate Approach
        call GetHomeDirAlt(NewHomeDir,iLenght)
        GetHomeDir=trim(NewHomeDir)
        return
      else if (iStatus.EQ.0) then ! Append Delimiter and Return
        GetHomeDir=trim(NewHomeDir)//'/'
        return
      else if (iStatus.EQ.-1) then ! Exit with error
      WRITE(ERROR_UNIT,*) "In GetHomeDir() NewHomeDir variable did"//
     +                 " have enough storage to store the path."//
     +                 " Increase it's size.  Recompile and"//
     +                 " try again."
      call exit(-1)
      end if
      GetHomeDir=Trim('')
      return
      end

C     Find the HomeDir using an alternate approach
C     by trying to determine where the executable is
C
      subroutine GetHomeDirAlt(strHomeDir,iStatus)
      USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT
      character*(*), intent(out) :: strHomeDir
      integer , intent(out) :: iStatus
      integer :: iIndex
      logical :: lExists
      CHARACTER(len=512) :: strBuffer
      call getcwd(strBuffer)
      
      strHomeDir=trim(strBuffer)//'/'
      call get_command(strBuffer)
      iIndex = index(strBuffer,'a.out')
      if (strBuffer(1:1).EQ.'.') then
        strHomeDir=trim(strHomeDir)//trim(strBuffer(:iIndex-1))
      else
        strHomeDir=trim(strBuffer(:iIndex-1))
      end if
      iIndex = index(strHomeDir,'bin/',.TRUE.)
      if (iIndex.NE.0) then
        strHomeDir=strHomeDir(:iIndex-1)
      end if
      strBuffer=trim(strHomeDir)//"SYMMDAT/pgroup.dat"
      INQUIRE(FILE=trim(strBuffer), EXIST=lExists)
      if (.NOT.lExists) then
        WRITE(ERROR_UNIT,*) "Could not get the HomeDir."//
     +                 " Please set JANA2006_DIR variable"//
     +                 " and try again."
        call exit(-1)
      end if
      end
