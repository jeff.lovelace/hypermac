C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
C
      function GetHomeDir()
      USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT
      character(len=512) :: NewHomeDir,GetHomeDir
      integer :: iLenght,iStatus
      CALL GET_ENVIRONMENT_VARIABLE('JANA2006_DIR', NewHomeDir, iLength
     +, iStatus)
      if (iStatus.EQ.1) then ! Try Alternate Approach
        call GetHomeDirAlt(NewHomeDir,iLength)
        GetHomeDir=trim(NewHomeDir)
        return
      else if (iStatus.EQ.2) then ! Try Alternate Approach
        call GetHomeDirAlt(NewHomeDir,iLenght)
        GetHomeDir=trim(NewHomeDir)
        return
      else if (iStatus.EQ.0) then ! Append Delimiter and Return
        GetHomeDir=trim(NewHomeDir)//'/'
        return
      else if (iStatus.EQ.-1) then ! Exit with error
      WRITE(ERROR_UNIT,*) "In GetHomeDir() NewHomeDir variable did"//
     +                 " have enough storage to store the path."//
     +                 " Increase it's size.  Recompile and"//
     +                 " try again."
      call exit(-1)
      end if
      GetHomeDir=Trim('')
      return
      end
