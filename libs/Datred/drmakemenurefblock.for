      subroutine DRMakeMenuRefBlock
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      if(NRefBlock.gt.1) then
        do i=1,NRefBlock
          MenuRefBlock(i)=RefBlockName(i)
          if(DifCode(i).gt.200) then
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      '->Powder TOF/'
          else if(DifCode(i).gt.100) then
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      '->Powder CW/'
          else if(DifCode(i).gt.0) then
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      '->Single crystal/'
          else
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      '->I(hkl) imported/'
          endif
          if(RadiationRefBlock(i).eq.XRayRadiation) then
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      'X-rays'
          else if(RadiationRefBlock(i).eq.NeutronRadiation) then
            MenuRefBlock(i)=MenuRefBlock(i)(:idel(MenuRefBlock(i)))//
     1                      'Neutrons'
          else
            j=idel(MenuRefBlock(i))
            MenuRefBlock(i)(j:j)=' '
          endif
        enddo
      endif
      return
      end
