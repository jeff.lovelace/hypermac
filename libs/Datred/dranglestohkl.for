      subroutine DRAnglesToHKL(Angle,h)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension Angle(4),h(3),c(3)
      call DRHKLToCoor(Angle,c)
      call Multm(ubi,c,h,3,3,1)
      return
      end
