      subroutine DRIntUpDate(OMFromFile,TrXYZ,nEdwFile,nEwdFirstOM,
     1                       nCrwSenseFirst,nEdwWaveLength)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension UBPom(9),TrXYZ(3,3)
      character*80  t80
      character*256 EdwStringQuest
      logical CrwLogicQuest,OMFromFile
      call FeQuestRealFromEdw(nEdwWaveLength,LamAveRefBlock(KRefBlock))
      if(OMFromFile) then
        t80=EdwStringQuest(nEdwFile)
        ln=NextLogicNumber()
        call OpenFile(ln,t80,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        read(ln,*,err=9000)((ub(i,j,1),j=1,3),i=1,3)
        call TrMat(ub,UBPom,3,3)
        close(ln)
      else
        nEdw=nEwdFirstOM
        j=1
        do i=1,3
          call FeQuestRealAFromEdw(nEdw,UBPom(j))
          nEdw=nEdw+1
          j=j+3
        enddo
      endif
      call TrMat(UBPom,ub,3,3)
      call DRCellFromUB
      call TrMat(UBPom,ub(1,1,2),3,3)
      call MatInv(TrXYZ,UBPom,pom,3)
      call Multm(UBPom,ub(1,1,2),ub,3,3,3)
      call MatInv(ub,ubi,pom,3)
      nCrw=nCrwSenseFirst
      do i=1,4
        if(CrwLogicQuest(nCrw)) then
          SenseOfAngle(i)=-1.
        else
          SenseOfAngle(i)= 1.
        endif
        nCrw=nCrw+1
      enddo
      go to 9999
9000  call FeReadError(ln)
      ErrFlag=1
9999  return
      end
