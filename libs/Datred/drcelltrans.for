      subroutine DRCellTrans
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm40.cmn'
      include 'powder.cmn'
      dimension TrMPA(36,20),CellA(6,20),TrMPW(36),CellW(6),xp(9),t(36),
     1          ti(36),trm6(36),trm6i(36),rmp(36),h(6),
     2          dif(3),trm3(3,3),trm3i(3,3),StPom(15),TrMatPom(*),
     3          QuOut(*),ish(3,3),QuPom(3,3),rsh(3,3),rsg(9),nt(6),
     4          it(6),xpp(6),vt6p(:,:),TrMatUsed(3,3)
      character*256 Veta,flnOld,t256
      real :: x2(1,3)=reshape((/.1,-2.,3./),shape(x2))
      character*2   nty,t2
      integer FeChdir,CellTrans,QVecTrans,FeMenu,UseTabsIn,GetDenom
      logical EqRV,ExistFile,EqRV0,CrwLogicQuest,EqIgCase,FeYesNo,
     1        DefinedTrans,ChciJenMatici,MatRealEqUnitMat
      equivalence (t256,flnOld),(ish,rsh,trm3),(QuPom,xp),(t2,t256),
     1            (CellTrans,IdNumbers(1)),(QVecTrans,IdNumbers(2))
      allocatable vt6p
      Klic=CellTrans
      ChciJenMatici=.false.
      go to 1000
      entry DRCellTrMat(TrMatPom,ichv)
      ichv=0
      Klic=CellTrans
      ChciJenMatici=.true.
      go to 1000
      entry DRQVecTrans
      Klic=QVecTrans
      ChciJenMatici=.false.
      go to 1000
      entry DRQVecTrMat(TrMatPom,QuOut,ichv)
      ichv=0
      Klic=QVecTrans
      ChciJenMatici=.true.
1000  IComp=1
      if(ChciJenMatici) go to 1050
      if(NComp(KPhase).gt.1) then
        id=NextQuestId()
        xqd=220.
        il=NComp(KPhase)
        call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,NComp(KPhase)
          write(Veta,'(''transformation apply to'',i2,a2,
     1                 '' subsystem'')') i,nty(i)
          call FeQuestCrwMake(id,tpom,i,xpom,i,Veta,'L',CrwgXd,CrwgYd,
     1                        0,1)
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(i.eq.1) nCrwFirst=CrwLastMade
        enddo
        call FeQuestEvent(id,ich)
        if(ich.eq.0) then
          nCrw=nCrwFirst
          do i=1,NComp(KPhase)
            if(CrwLogicQuest(nCrw)) then
              IComp=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      endif
      if((ExistM95.and.IComp.eq.1).or..not.ChciJenMatici) then
        call iom95(0,fln(:ifln)//'.m95')
      else
        call UnitMat(TrMP,NDim(KPhase))
      endif
1050  if(ChciJenMatici) then
        call CopyMat(TrMatPom,TrMPA(1,1),NDim(KPhase))
      else
        call UnitMat(TrMPA(1,1),NDim(KPhase))
      endif
      do i=1,ubound(SpecMatrixName,1)
1100    j=index(SpecMatrixName(i),'#')
        if(j.gt.0) then
          SpecMatrixName(i)(j:j)=Tabulator
          go to 1100
        endif
      enddo
      call UnitMat(TrMatUsed,3)
      id=NextQuestId()
      xqd=400.
      if(Klic.eq.CellTrans) then
        call CopyVek(CellPar(1,IComp,KPhase),CellA(1,1),6)
        n=1
        nmx=1
        il=11
        call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
        tpom=xqd*.5
        xpom=5.
        il=1
        Veta='Transformed cell parameters - Volume'
        call FeQuestLblMake(id,tpom,il,Veta,'C','B')
        il=il+1
        call DRCellInfo(CellA,Veta)
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        nLblCell=LblLastMade
        wpom=50.
        xpom=xqd-60.
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,'%Back')
        nButtBack=ButtonLastMade
        il=il+1
        Veta='Cumulative matrix'
        call FeQuestLblMake(id,tpom,il,Veta,'C','B')
        do i=1,3
          il=il+1
          call FeQuestLblMake(id,tpom,il,' ','C','N')
          if(i.eq.1) nLblMatrix=LblLastMade
        enddo
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,'%Forward')
        nButtForward=ButtonLastMade
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Transform:'
        call FeQuestLblMake(id,tpom,il,Veta,'C','B')
        il=il+1
        ils=il
        wpom=150.
        xpom=30.
        Veta='to the %reduced cell'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtReduce=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='to a %double cell'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtDouble=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='matri%x calculator'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtMatCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xqd-wpom-30.
        il=ils
        Veta='to those from data %collection'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtOriginal=ButtonLastMade
        if(ChciJenMatici) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        il=il+1
        Veta='by a %matrix'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtMatrix=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='by a %special matrix'
        call FeQuestButtonMake(id,xpom,il,wpom,ButYd,Veta)
        nButtMatrixSpecial=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1200    call DRCellInfo(CellA(1,n),Veta)
        call FeQuestLblChange(nLblCell,Veta)
        nLbl=nLblMatrix
        do i=1,3
          k=NDim(KPhase)*(i-1)
          Veta=SmbABC(i)//'''='
          do j=1,3
            k=k+1
            pom=TrMPA(k,n)
            write(Cislo,'(f7.3,''*'',a1)') pom,SmbABC(j)
            if(j.ne.1.and.pom.ge.0.) then
              if(Cislo(2:2).eq.' ') then
                m=2
              else
                m=1
              endif
              Cislo(m:m)='+'
            endif
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
          enddo
          call FeQuestLblChange(nLbl,Veta)
          nLbl=nLbl+1
        enddo
        nmx=max(n,nmx)
        if(n.gt.1) then
          call FeQuestButtonOpen(nButtBack,ButtonOff)
        else
          call FeQuestButtonOpen(nButtBack,ButtonDisabled)
        endif
        if(n.lt.nmx) then
          call FeQuestButtonOpen(nButtForward,ButtonOff)
        else
          call FeQuestButtonOpen(nButtForward,ButtonDisabled)
        endif
1300    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtBack.or.CheckNumber.eq.nButtForward))
     2    then
          if(CheckNumber.eq.nButtBack) then
            n=max(n-1,1)
          else
            n=min(n+1,nmx)
          endif
          go to 1200
        else if(CheckType.eq.EventButton) then
          call CopyVek(CellA(1,n),CellW,6)
          call CopyMat(TrMPA(1,n),TrMPW,NDim(KPhase))
          if(CheckNumber.eq.nButtReduce) then
            call DRCellReduction(CellW,TrMPW)
          else if(CheckNumber.eq.nButtMatrix) then
            call CopyMat(TrMatUsed,t,3)
            call FeReadRealMat(-1.,60.,'Transformation matrix',SmbABC,
     1                         IdAddPrime,SmbABC,t,ti,3,CheckSingYes,
     2                         CheckPosDefYes,ich)
            if(ich.eq.0) then
              call CopyMat(t,TrMatUsed,3)
              call DRMatTrCell(t,CellW,TrMPW)
            endif
          else if(CheckNumber.eq.nButtMatrixSpecial) then
            UseTabsIn=UseTabs
            UseTabs=NextTabs()
            call FeTabsAdd( 30.,UseTabs,IdRightTab,' ')
            call FeTabsAdd( 90.,UseTabs,IdRightTab,' ')
            call FeTabsAdd(110.,UseTabs,IdRightTab,' ')
            call FeTabsAdd(180.,UseTabs,IdRightTab,' ')
            i=FeMenu(-1.,-1.,SpecMatrixName,1,ubound(SpecMatrixName,1),
     1               1,0)
            call FeTabsReset(UseTabs)
            UseTabs=UseTabsIn
            if(i.gt.0) then
              call DRMatTrCell(SpecMatrix(1,i),CellW,TrMPW)
            else
              go to 1300
            endif
          else if(CheckNumber.eq.nButtDouble) then
            call DRDoubleCell(TrMPW,CellW,IZmena)
          else if(CheckNumber.eq.nButtOriginal) then
            call CopyMat(TrMPI,TrMPW,NDim(KPhase))
            call CopyVek(CellRefBlock(1,0),CellW,6)
          else if(CheckNumber.eq.nButtMatCalc) then
            NRow=3
            NCol=3
            call MatBlock3(TrMPA(1,n),t,NDim(KPhase))
            call TrMat(t,ti,3,3)
            call MatrixCalculatorInOut(ti,NRow,NCol,ichp)
            if(ichp.eq.0) then
              call MatFromBlock3(ti,t,NDim(KPhase))
              call TrMat(t,ti,NDim(KPhase),NDim(KPhase))
              call MatInv(ti,t,pom,NDim(KPhase))
              if(abs(pom).gt..0001) then
                call multm(t,TrMPA(1,n),ti,NDim(KPhase),NDim(KPhase),
     1                     NDim(KPhase))
                if(MatRealEqUnitMat(ti,NDim(KPhase),.0001)) go to 1300
                call MatBlock3(ti,t,NDim(KPhase))
                call MatInv(t,ti,pom,3)
                call DRMatTrCell(ti,CellW,TrMPW)
              else
                go to 1300
              endif
            else
              go to 1300
            endif
          endif
          if(EqRV(TrMPW,TrMPA(1,n),NDimQ(KPhase),.0001)) then
            go to 1300
          else
            if(EqRV(CellA(1,n),CellW,6,.001)) then
              call matinv(TrMPW,ti,pom,NDim(KPhase))
              if(abs(abs(pom)-1.).lt..0001) then
                call multm(ti,TrMPA(1,n),t,NDim(KPhase),NDim(KPhase),
     1                     NDim(KPhase))
                call UnitMat(ti,NDim(KPhase))
                if(EqRV(t,ti,NDimQ(KPhase),.0001)) go to 1300
              endif
            endif
            n=n+1
            if(n.gt.nmx) go to 1400
            if(.not.EqRV(TrMPW,TrMPA(1,n),NDimQ(KPhase),.0001)) then
              if(EqRV(CellA(1,n),CellW,6,.001)) then
                call matinv(TrMPW,ti,pom,NDim(KPhase))
                if(abs(abs(pom)-1.).lt..0001) then
                  call multm(ti,TrMPA(1,n),t,NDim(KPhase),NDim(KPhase),
     1                       NDim(KPhase))
                  call UnitMat(ti,NDim(KPhase))
                  if(EqRV(t,ti,NDimQ(KPhase),.0001)) go to 1300
                endif
              endif
            endif
1400        call CopyVek(CellW,CellA(1,n),6)
            call CopyMat(TrMPW,TrMPA(1,n),NDim(KPhase))
            go to 1200
          endif
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1300
        endif
        call FeQuestRemove(id)
        if(ich.eq.0) then
          if(ChciJenMatici) then
            call CopyMat(TrMPA(1,n),TrMatPom,NDim(KPhase))
            go to 9999
          endif
        else
          if(ChciJenMatici) ichv=ich
          go to 9999
        endif
        if(eqrv(CellA(1,1),CellA(1,n),6,.001).and.
     1     eqrv(TrMPA(1,1),TrMPA(1,n),NDimQ(KPhase),.001)) go to 9999
      else
        yqd=110.+32.*float(NDimI(KPhase))
        call FeQuestAbsCreate(id,-1.,-1.,xqd,yqd,' ',0,LightGray,0,0)
        xpom=5.
        do j=1,2
          ypom=yqd-12.
          if(j.eq.1) then
            Veta='Original modulation vector'
            tpom=5.
            t2=' :'
          else
            tpom=xqd*.5+5.
            Veta='Transformed modulation vector'
            t2=''':'
          endif
          if(NDimI(KPhase).gt.1) Veta=Veta(:idel(Veta))//'s'
          call FeQuestAbsLblMake(id,tpom,ypom,Veta,'L','B')
          ypom=ypom-8.
          do i=1,NDimI(KPhase)
            ypom=ypom-12.
            write(Veta,100) i,t2,(Qu(k,i,IComp,KPhase),k=1,3)
            call CopyVek(Qu(1,i,IComp,KPhase),QuPom(1,i),3)
            call FeQuestAbsLblMake(id,tpom,ypom,Veta,'L','N')
            if(i.eq.1.and.j.eq.2) nLblQVec=LblLastMade
          enddo
        enddo
        ypom=ypom-12.
        call FeQuestAbsLinkaMake(id,ypom)
        ypom=ypom-24.
        Veta='^'
        ypom=ypom-4.
        dpom=ButYd
        xpom=(xqd-dpom)*.5
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Veta)
        nButtApply=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call SetIntArrayTo(ish,9,0)
        call UnitMat(rsg,NDimI(KPhase))
        k=0
        dpom1=50.
        dpom2=25.
        tpomp=dpom1+
     1        float(NDimI(KPhase))*(dpom2+FeTxLength('*q1')+
     2                              FeTxLengthSpace('X+X'))+
     3        FeTxLength('q1''=XX')
        tpomp=(xqd-tpomp)*.5
        do i=1,NDimI(KPhase)
          ypom=ypom-20.
          write(Veta,'(''q'',i1,''''''='')') i
          tpom=tpomp
          xpom=tpom+FeTxLength(Veta(:idel(Veta))//'XX')
          ypome=ypom-8.
          call FeQuestAbsEdwMake(id,tpom,ypom,xpom,ypome,Veta,'L',
     1                           dpom1,EdwYd,0)
          call FeQuestIntAEdwOpen(EdwLastMade,ish(1,i),3,.false.)
          if(i.eq.1) nEdwFirst=EdwLastMade
          tpom=xpom+dpom1+FeTxLength('X')
          do j=1,NDimI(KPhase)
            k=k+1
            Veta='+'
            xpom=tpom+FeTxLength(Veta)+FeTxLength('X')
            call FeQuestAbsEdwMake(id,tpom,ypom,xpom,ypome,Veta,'L',
     1                             dpom2,EdwYd,0)
            call FeQuestRealEdwOpen(EdwLastMade,rsg(k),.false.,.false.)
            tpom=xpom+dpom2+FeTxLength('X')
            write(Veta,'(''*q'',i1)') j
            call FeQuestAbsLblMake(id,tpom,ypom,Veta,'L','N')
            tpom=tpom+FeTxLengthUnder(Veta)+1.
          enddo
        enddo
1600    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtApply) then
          nEdw=nEdwFirst-1
          k=0
          do i=1,NDimI(KPhase)
            nEdw=nEdw+1
            call FeQuestIntAFromEdw(nEdw,ish(1,i))
            do j=1,3
              rsh(j,i)=ish(j,i)
            enddo
            do j=1,NDimI(KPhase)
              k=k+1
              nEdw=nEdw+1
              call FeQuestRealFromEdw(nEdw,rsg(k))
            enddo
          enddo
          l=0
          call CopyVek(rsh,QuPom,3*NDimI(KPhase))
          nLbl=nLblQVec
          t2=''':'
          do i=1,NDimI(KPhase)
            do j=1,NDimI(KPhase)
              l=l+1
              pom=rsg(l)
              do k=1,3
                QuPom(k,i)=QuPom(k,i)+pom*Qu(k,j,IComp,KPhase)
              enddo
            enddo
            write(Veta,100) i,t2,(QuPom(k,i),k=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
          go to 1600
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) then
          if(ChciJenMatici) ichv=ich
          go to 9999
        endif
        n=1
        call UnitMat(t,NDim(KPhase))
        do i=1,NDimI(KPhase)
          do j=1,3
            t(i+3+(j-1)*NDim(KPhase))=rsh(j,i)
          enddo
        enddo
        l=0
        do j=4,NDim(KPhase)
          k=(j-1)*NDim(KPhase)+3
          do i=4,NDim(KPhase)
            k=k+1
            l=l+1
            t(k)=rsg(l)
          enddo
        enddo
        call MatInv(t,TrMPA(1,1),pom,NDim(KPhase))
        if(ChciJenMatici) then
          call MultM(TRMPA(1,1),TrMatPom,TRMPA(1,2),
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          call CopyMat(TRMPA(1,2),TrMatPom,NDim(KPhase))
          call CopyVek(QuPom(1,1),QuOut,3)
          go to 9999
        endif
      endif
      DefinedTrans=.false.
      flnOld=fln
      iflnOld=ifln
      call CrlDefineNewFLN(Konec)
      if(Konec.eq.1) go to 9999
      go to 2000
      entry DRCellDefinedTrans(TrMatPom)
      flnOld=fln
      iflnOld=ifln
      DefinedTrans=.true.
      IComp=1
      n=1
      call MatBlock3(TrMatPom,xp,NDim(KPhase))
      call CopyVek(CellPar(1,1,KPhase),CellA(1,1),6)
      call UnitMat(t,NDim(KPhase))
      call DRMatTrCell(xp,CellA(1,1),t)
      call CopyMat(TrMatPom,TrMPA(1,1),NDim(KPhase))
      Klic=CellTrans
2000  do i=1,5
        if(i.eq.1) then
          Cislo='.m40'
        else if(i.eq.2) then
          if(.not.isPowder) cycle
          Cislo='.m41'
        else if(i.eq.3) then
          Cislo='.m50'
        else if(i.eq.4) then
          Cislo='.m90'
        else if(i.eq.5) then
          Cislo='.m95'
        endif
        Veta=flnOld(:iflnOld)//Cislo(:idel(Cislo))
        if(ExistFile(Veta)) call CopyFile(Veta,'#tmp#'//
     1                                    Cislo(:idel(Cislo)))
      enddo
      if(IComp.eq.1.and.ExistM95) then
        if(.not.EqIgCase(fln,flnOld))
     1    call CopyFile(flnOld(:iflnOld)//'.m95',fln(:ifln)//'.m95')
        if(KPhase.eq.1) then
          call Multm(TrMP,TrMPA(1,n),t,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          call CopyMat(t,TrMP,NDim(KPhase))
          call CompleteM95(0)
        endif
      endif
      NTrans=1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      call CopyMat(TrMPA(1,n),trm6,NDim(KPhase))
      call matinv(trm6,trm6i,pom,NDim(KPhase))
      call CopyMat(TrM6i,TransMat(1,1,IComp),NDim(KPhase))
      call MatBlock3(trm6,trm3,NDim(KPhase))
      call matinv(trm3,trm3i,VolRatio,3)
      NUnits(KPhase)=nint(NUnits(KPhase)*VolRatio)
      if(IComp.eq.1) then
        if(Klic.eq.CellTrans) then
          call CopyVek(CellA(1,n),CellPar(1,1,KPhase),6)
          call DRCell2MetTens(CellPar(1,1,KPhase),MetTens(1,1,KPhase))
          call TrMat(trm3,xp,3,3)
          do j=1,9
            MetTensS(j,1,KPhase)=sqrt(MetTensS(j,1,KPhase))
          enddo
          call MultMQ(xp,MetTensS(1,1,KPhase),t,3,3,3)
          call MultMQ(t,trm3,MetTensS(1,1,KPhase),3,3,3)
          m=1

          do j=1,3
            CellParSU(j,1,KPhase)=MetTensS(m,1,KPhase)*.5/
     1                            CellPar(j,1,KPhase)
            m=m+4
          enddo
          do j=1,2
            do k=j+1,3
              m=k+(j-1)*3
              l=6-j-k
              pom=MetTens(m,1,KPhase)
              if(abs(pom).gt..0001) then
                pom=(MetTensS(m,1,KPhase)**2-
     1               (pom/CellPar(j,1,KPhase)*CellParSU(j,1,KPhase))**2-
     2               (pom/CellPar(k,1,KPhase)*CellParSU(k,1,KPhase))**2)
     3              /(pom*tan(CellPar(l+3,1,KPhase)*ToRad)*ToRad)**2
                if(pom.gt.0.) then
                  CellParSU(l+3,1,KPhase)=sqrt(pom)
                else
                  CellParSU(l+3,1,KPhase)=0.
                endif
              else
                CellParSU(l+3,1,KPhase)=0.
              endif
            enddo
          enddo
          do j=1,9
            MetTensS(j,1,KPhase)=MetTensS(j,1,KPhase)**2
          enddo
          if(ExistPowder) call CopyVek(CellA(1,n),CellPwd(1,KPhase),6)
          do i=1,NDimI(KPhase)
            call CopyVek(Quir(1,i,KPhase),xp,3)
            call multm(xp,trm3,Quir(1,i,KPhase),1,3,3)
            k=NDim(KPhase)
            do j=1,3
              Quir(j,i,KPhase)=Quir(j,i,KPhase)-trm6(k)
              k=k+NDim(KPhase)
            enddo
            call CopyVek(Qui(1,i,KPhase),xp,3)
            call multm(xp,trm3,Qui(1,i,KPhase),1,3,3)
            call AddVek(Qui(1,i,KPhase),QuiR(1,i,KPhase),
     1                  Qu(1,i,1,KPhase),3)
            if(ExistPowder)
     1        call CopyVek(Qu(1,i,1,KPhase),QuPwd(1,i,KPhase),3)
          enddo
        else if(Klic.eq.QVecTrans) then
          call CopyVek(QuPom,Qu(1,1,1,KPhase),3*NDimI(KPhase))
          if(ExistPowder) call CopyVek(QuPom,QuPwd(1,1,KPhase),6)
        endif
        do i=1,NSymm(KPhase)
          call multm(trm6i,rm6(1,i,1,KPhase),rmp,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call multm(rmp,trm6,rm6(1,i,1,KPhase),NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call MatBlock3(rm6(1,i,1,KPhase),rm(1,i,1,KPhase),
     1                   NDim(KPhase))
          call multm(trm6i,s6(1,i,1,KPhase),h,NDim(KPhase),NDim(KPhase),
     1               1)
          call CopyVek(h,s6(1,i,1,KPhase),NDim(KPhase))
          call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
          do j=1,NDimQ(KPhase)
            pom=rm6(j,i,1,KPhase)
            if(abs(anint(pom)-pom).gt..0001) then
              fln=flnOld
              ifln=iflnOld
              go to 9000
            endif
          enddo
        enddo
        do i=1,NSymmL(KPhase)
          call multm(trm6i,rm6l(1,i,1,KPhase),rmp,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call multm(rmp,trm6,rm6l(1,i,1,KPhase),NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call multm(trm6i,s6l(1,i,1,KPhase),h,NDim(KPhase),
     1               NDim(KPhase),1)
          call CopyVek(h,s6l(1,i,1,KPhase),NDim(KPhase))
          call od0do1(s6l(1,i,1,KPhase),s6l(1,i,1,KPhase),NDim(KPhase))
        enddo
        nvtt=0
        nvto=NLattVec(KPhase)
        n=1
        do i=1,NDimQ(KPhase)
          n=max(GetDenom(trm6i(i),10),n)
        enddo
        if(n.gt.1) then
          nn=(2*n+1)**NDim(KPhase)
          call SetIntArrayTo(nt,NDim(KPhase),2*n+1)
        else
          nn=1
        endif
        allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6(1,i,1,KPhase),vt6p(1,i),NDim(KPhase))
        enddo
        nvtt=1
        call SetRealArrayTo(vt6(1,nvtt,1,KPhase),NDim(KPhase),0.)
        do j=1,NLattVec(KPhase)
          do 2210k=1,nn
            if(nn.eq.1) then
              call CopyVek(vt6p(1,j),xp,NDim(KPhase))
            else
              call RecUnpack(k,it,nt,NDim(KPhase))
              do l=1,NDim(KPhase)
                xp(l)=float(it(l)-n-1)+vt6p(l,j)
              enddo
            endif
            call multm(trm6i,xp,xpp,NDim(KPhase),NDim(KPhase),1)
            do l=1,NDim(KPhase)
              call ToFract(xpp(l),Cislo,15)
              if(index(Cislo,'?').le.0) xpp(l)=fract(Cislo,ich)
            enddo
            call od0do1(xpp,xp,NDim(KPhase))
            do l=1,nvtt
              if(eqrv(xp,vt6(1,l,1,KPhase),NDim(KPhase),.001))
     1          go to 2210
            enddo
            nvtt=nvtt+1
            call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),nvtt,
     1                       NComp(KPhase),NPhase,0)
            call CopyVek(xp,vt6(1,nvtt,1,KPhase),NDim(KPhase))
            NLattVec(KPhase)=nvtt
2210      continue
        enddo
        do i=1,NTwin
          if(KPhase.eq.1) then
            if(KPhaseTwin(i).eq.KPhase) then
              call multm(trm3i,rtw(1,i),rmp,3,3,3)
              call multm(rmp,trm3,rtw(1,i),3,3,3)
            else
              call multm(trm3i,rtw(1,i),rmp,3,3,3)
              call CopyMat(rmp,rtw(1,i),3)
            endif
          else
            if(KPhaseTwin(i).eq.KPhase) then
              call multm(rtw(1,i),trm3,rmp,3,3,3)
              call CopyMat(rmp,rtw(1,i),3)
            endif
          endif
        enddo
        deallocate(vt6p)
        call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),1000,
     1                   NComp(KPhase),NPhase,0)
        call EM50CompleteCentr(0,vt6,ubound(vt6,1),nvtt,1000,ich)
        NLattVec(KPhase)=nvtt
        call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),nvtt,
     1                   NComp(KPhase),NPhase,0)
        if(KPhase.eq.1) then
          RedSc=float(nvto)/float(nvtt)
          if(abs(VolRatio*RedSc-1.).lt..001) then
!            RedSc=1.
            VolRatio=1.
          endif
        else
          RedSc=1.
        endif
        do j=1,NDatBlock
          if(iabs(DataType(j)).eq.2) then
            ik=1
          else
            ik=mxscu
          endif
          do i=1,ik
            if(iabs(DataType(j)).eq.2) then
              sc(i,j)=RedSc**2*sc(i,j)
            else
              sc(i,j)=RedSc*sc(i,j)
            endif
          enddo
        enddo
        if(KCommen(KPhase).ne.0) then
          if(ICommen(KPhase).eq.0) then
            do i=1,3
              xp(i)=NCommen(i,1,KPhase)
            enddo
            call multm(trm3i,xp,dif,3,3,1)
            do i=1,3
              NCommen(i,1,KPhase)=max(nint(abs(dif(i))),1)
            enddo
          else
            call multm(trm3i,RCommen(1,1,KPhase),rmp,3,3,3)
            call CopyMat(rmp,RCommen(1,1,KPhase),3)
          endif
        endif
        do i=2,NComp(KPhase)
          call MultM(ZV(1,i,KPhase),trm6,rmp,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          call CopyMat(rmp,ZV(1,i,KPhase),NDim(KPhase))
          call MatInv(ZV(1,i,KPhase),ZVI(1,i,KPhase),pom,NDim(KPhase))
        enddo
      else
        call MultM(trm6i,ZV(1,IComp,KPhase),rmp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call CopyMat(rmp,ZV(1,IComp,KPhase),NDim(KPhase))
        call MatInv(ZV(1,IComp,KPhase),ZVI(1,IComp,KPhase),pom,
     1              NDim(KPhase))
      endif
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call ResetIgnoreW
      call ResetIgnoreE
      if(.not.EqIgCase(fln,flnOld))
     1  call CopyFile(flnOld(:iflnOld)//'.l51',fln(:ifln)//'.l51')
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call SetRealArrayTo(TransVec(1,1,IComp),NDim(KPhase),0.)
      TransZM(1)=0
      call EM40SetTr(IComp)
      call EM40TransAtFromTo(1,NAtInd,ich)
      if(ich.ne.0) go to 9999
      if(NMolecLenAll(KPhase).gt.0) then
        call EM40TransAtFromTo(NAtMolFrAll(KPhase),NAtMolToAll(KPhase),
     1                         ich)
        if(ich.ne.0) go to 9999
      endif
      do i=NMolecFr(IComp,KPhase),NMolecTo(IComp,KPhase)
        call CopyVek(xm(1,i),xp,3)
        call multm(trm3i,xp,xm(1,i),3,3,1)
        StRefPoint(i)=' '
        isw=iswmol(i)
        do j=1,mam(i)
          ji=j+(i-1)*mxp
          if(VolRatio.lt..99) aimol(ji)=aimol(ji)*VolRatio
          call EM40TrMol(ji,ji,isw,1,1)
        enddo
      enddo
      if(VolRatio.lt..99) then
        do i=1,NAtInd
          if(kswa(i).eq.KPhase) ai(i)=ai(i)*VolRatio
        enddo
        if(allocated(IAtActive))
     1    deallocate(IAtActive,LAtActive,NameAtActive)
        n=NAtInd+NAtMol
        allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
        NAtActive=0
        do i=1,NAtInd
          if(kswa(i).eq.KPhase) then
            NAtActive=NAtActive+1
            LAtActive(NAtActive)=.true.
            IAtActive(NAtActive)=i
          endif
        enddo
        if(NMolec.gt.0) then
          iak=NAtMolFrAll(KPhase)-1
          do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
            iap=iak+1
            iak=iap+iam(im)/NPoint(im)-1
            do ia=iap,iak
              NAtActive=NAtActive+1
              LAtActive(NAtActive)=.true.
              IAtActive(NAtActive)=ia
            enddo
          enddo
        endif
        call EM40MergeAtomsRun(.1,ich)
        if(allocated(IAtActive))
     1    deallocate(IAtActive,LAtActive,NameAtActive)
        do i=1,mxscu
          sc(i,1)=sc(i,1)/VolRatio
        enddo
      else if(VolRatio.gt.1.01) then
        do i=1,NComp(KPhase)
          if(IComp.eq.1) then
            fn=0.
            do j=1,NLattVec(KPhase)
              if(EqRV0(vt6(1,j,i,KPhase),NDim(KPhase),.001)) fn=fn+1.
            enddo
          else
            if(i.eq.IComp) then
              fn=1./VolRatio
            else
              fn=1.
            endif
          endif
          do j=1,NAtInd
            if(kswa(j).eq.KPhase.and.iswa(j).eq.i) ai(j)=ai(j)/fn
          enddo
          do j=1,NMolec
            if(kswmol(j).eq.KPhase.and.iswmol(j).eq.i) then
              imp=(j-1)*mxp
              do k=1,mam(j)
                imp=imp+1
                aimol(imp)=ai(imp)/fn
              enddo
            endif
          enddo
        enddo
      endif
      if(isPowder) then
        if(KStrain(KPhase,1).eq.IdPwdStrainTensor) then
          if(NDimI(KPhase).ne.1) then
            call multm(TransC4(1,1,1),StPwd(1,KPhase,1),StPom,15,15,1)
            call CopyVek(StPom,StPwd(1,KPhase,1),15)
          else
            if(VasekTest.ne.0)
     1        call FeWinMessage('Transformace Strain-tenzoru pro 4d '//
     2                          'nechodi',' ')
          endif
        endif
      endif
      call iom40(1,0,fln(:ifln)//'.m40')
3000  if(ExistPowder) then
        Veta=fln(:ifln)//'.prf'
        if(ExistFile(Veta)) call DeleteFile(Veta)
      endif
      if(ExistM95) then
        if(IComp.eq.1) then
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          SilentRun=DefinedTrans
          call EM9CreateM90(0,ich)
          SilentRun=.false.
        else
          if(.not.EqIgCase(fln,flnOld)) then
            if(ExistFile(flnOld(:iflnOld)//'.m90'))
     1        call CopyFile(flnOld(:iflnOld)//'.m90',fln(:ifln)//'.m90')
            call CopyFile(flnOld(:iflnOld)//'.m95',fln(:ifln)//'.m95')
          endif
        endif
      endif
      if(.not.EqIgCase(fln,flnOld).and..not.DefinedTrans) then
        Veta=fln
        fln=flnOld
        ifln=iflnOld
        if(FeYesNo(-1.,-1.,'Do you want to continue with the new '//
     1                   'structure?',0)) then
          call DeletePomFiles
          call ExtractDirectory(Veta,t256)
          i=FeChdir(t256)
          call ExtractFileName(Veta,fln)
          ifln=idel(fln)
          call FeGetCurrentDir
          call FeBottomInfo(' ')
          call DeletePomFiles
          call ChangeUSDFile(fln,'opened','*')
        else
          call iom50(0,0,fln(:ifln)//'.m50')
          if(ExistM90) call iom90(0,fln(:ifln)//'.m90')
          if(ExistM95) call iom95(0,fln(:ifln)//'.m95')
        endif
      endif
      go to 9999
9000  call FeChybne(-1.,-1.,'transformation leads to non-integer '//
     1              'symmetry matrices.','it will not be performed.',
     2              SeriousError)
      do i=1,5
        if(i.eq.1) then
          Cislo='.m40'
        else if(i.eq.2) then
          if(.not.isPowder) cycle
          Cislo='.m41'
        else if(i.eq.3) then
          Cislo='.m50'
        else if(i.eq.4) then
          Cislo='.m90'
        else if(i.eq.5) then
          Cislo='.m95'
        endif
        Veta='#tmp#'//Cislo(:idel(Cislo))
        if(ExistFile(Veta))
     1    call MoveFile(Veta,fln(:ifln)//Cislo(:idel(Cislo)))
      enddo
      call iom50(0,0,fln(:ifln)//'.m50')
9999  call CloseIfOpened(91)
      call CloseIfOpened(92)
      if(allocated(TransMat)) call DeallocateTrans
      call DeleteFile(fln(:ifln)//'.l91')
      do i=1,5
        if(i.eq.1) then
          Cislo='.m40'
        else if(i.eq.2) then
          if(.not.isPowder) cycle
          Cislo='.m41'
        else if(i.eq.3) then
          Cislo='.m50'
        else if(i.eq.4) then
          Cislo='.m90'
        else if(i.eq.5) then
          Cislo='.m95'
        endif
        Veta='#tmp#'//Cislo(:idel(Cislo))
        if(ExistFile(Veta)) call DeleteFile(Veta)
      enddo
      return
100   format('q',i1,a2,3f8.4)
      end
