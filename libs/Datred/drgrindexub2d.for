      subroutine DRGrIndexUB2d(ich)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      common/DRGrIndexC/ nEdwCell,nEdwHKL,nButtCheck
      dimension x1(3),x2(3),x3(3),xp(3),G(3,3),Gi(3,3),
     1          GSave(3,3),GSaveI(3,3),
     1          h1(3),h2(3),h3(3),hp(3),hno(3),
     2          RMat(3,3),QMat(3,3),QMatI(3,3),UBIn(3,3),
     3          xp1(3),xp2(3),xp3(3)
      integer HMax(3),LblStateQuest
      character*256 EdwStringQuest
      character*80  Veta
      external DRGrIndexUB2dCheck,FeVoid
      logical EqIV0,Konec
      real CellParIn(6)
      save /DRGrIndexC/
      CellParIn=CellRefBlock(1:6,KRefBlock)
      do i=1,3
        do j=1,3
          UBIn(i,j)=UB(i,j,KRefBlock)
        enddo
      enddo
      ich=0
      id=NextQuestId()
      xqd=450.
      il=10
      Veta='Define cell parameters and 2d axes'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Cell parameters:'
      xpom=tpom+FeTxLengthUnder(Veta)+80.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      call FeQuestRealAEdwOpen(EdwLastMade,CellFor2d,6,
     1                         CellFor2d(1).le.0.,.false.)
      nEdwCell=EdwLastMade
      Veta='(h,k,l) for 2d axial direction #'
      idl=idel(Veta)
      dpom=80.
      do i=1,2
        il=il+1
        write(Veta(idl+1:),'(i1,'':'')') i
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        call FeQuestIntAEdwOpen(EdwLastMade,IH2d(1,i),3,
     1                          EqIV0(IH2d(1,i),3))
        if(i.eq.1) nEdwHKL=EdwLastMade
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      xpom=5.
      Veta='C%alculate'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtCheck=ButtonLastMade
      il=-il*10-10
      tpom=5.
      Veta='2d cell as measured:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-7
      Veta='2d cell as follows from cell paramaters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblTxCell2d=LblLastMade
      il=il+7
      tpom=tpom+FeTxLength(Veta)+5.
      write(Veta,100) Cell2d
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-7
      Veta=' '
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblCell2d=LblLastMade
      il=il-7
      Veta='Orientation matrix:'
      call FeQuestLblMake(id,tpom,il,Veta,'C','N')
      call FeQuestLblOff(LblLastMade)
      nLblTxUB=LblLastMade
      tpom=(xqd-200.)*.5
      do i=1,3
        il=il-7
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        if(i.eq.1) nLblUB=LblLastMade
      enddo
      il=il-7
      tpom=5.
      Veta='Final cell parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblTxCell3d=LblLastMade
      tpom=tpom+FeTxLength(Veta)+10.
      call FeQuestLblMake(id,tpom,il,' ','L','N')
      nLblCell3d=LblLastMade
1400  if(CellFor2d(i).gt.0.) then
        do i=1,3
          GSave(i,i)=CellFor2d(i)**2
        enddo
        GSave(1,2)=CellFor2d(1)*CellFor2d(2)*cos(CellFor2d(6)*ToRad)
        GSave(2,1)=GSave(1,2)
        GSave(1,3)=CellFor2d(1)*CellFor2d(3)*cos(CellFor2d(5)*ToRad)
        GSave(3,1)=GSave(1,3)
        GSave(2,3)=CellFor2d(2)*CellFor2d(3)*cos(CellFor2d(4)*ToRad)
        GSave(3,2)=GSave(2,3)
      endif
1450  if(CellFor2d(i).gt.0..and..not.EqIV0(IH2d(1,1),3).and.
     1                          .not.EqIV0(IH2d(1,2),3)) then
        do i=1,3
          h1(i)=IH2d(i,1)
          h2(i)=IH2d(i,2)
        enddo
        call MatInv(GSave,GSavei,Volp,3)
        Volp=sqrt(Volp)
        call MultM(GSavei,h1,hp,3,3,1)
        D1=1./sqrt(scalmul(h1,hp))
        call MultM(GSavei,h2,hp,3,3,1)
        D2=1./sqrt(scalmul(h2,hp))
        CosC=-scalmul(h1,hp)*D1*D2
      endif
1500  call FeQuestEventWithCheck(id,ich,DRGrIndexUB2dCheck,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCheck) then
        write(Veta,100) D1,D2,acos(CosC)/ToRad
        if(LblStateQuest(nLblTxCell2d).eq.LblOff)
     1    call FeQuestLblOn(nLblTxCell2d)
        call FeQuestLblChange(nLblCell2d,Veta)
        Konec=.false.
        ich=0
        go to 2000
1550    Veta='cell parameters not specified or incorrect.'
        go to 1600
1560    Veta='Axial dirrection #'
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//
     1       ' not defined.'
        go to 1600
1570    Veta='reflection are paralell.'
1600    call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCell) then
        if(EdwStringQuest(nEdwCell).ne.' ') then
          call FeQuestRealAFromEdw(nEdwCell,CellFor2d)
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwHKL.or.
     2         CheckNumber.eq.nEdwHKL+1)) then
        if(EdwStringQuest(CheckNumber).ne.' ') then
          i=CheckNumber-nEdwHKL+1
          call FeQuestIntAFromEdw(CheckNumber,IH2d(1,i))
          go to 1450
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      Konec=.true.
2000  if(ich.eq.0) then
        if(EdwStringQuest(nEdwCell).ne.' ') then
          call FeQuestRealAFromEdw(nEdwCell,CellFor2d)
        else
          ich=1
        endif
        nEdw=nEdwHKL
        do i=1,2
          if(EdwStringQuest(nEdw).ne.' ') then
            call FeQuestIntAFromEdw(nEdw,IH2d(1,i))
          else
            ich=1
          endif
          nEdw=nEdw+1
        enddo
        RMat=UBIn(1:3,1:3)
        call VecMul(RMat(1,1),RMat(1,2),RMat(1,3))
        RMat3=sqrt(ScalMul(RMat(1,3),RMat(1,3)))
        x1=RMat(1:3,1)
        x2=RMat(1:3,2)
        x3=RMat(1:3,3)/RMat3
        h1=IH2d(1:3,1)
        h2=IH2d(1:3,2)

c        G =GSave
c        Gi=GSavei


c        call VecMul(h1,h2,hp)
c        hp=hp/Volp
c        call MultM(G,hp,h3,3,3,1)
c        h3d=sqrt(ScalMul(hp,h3))

c        QMat(1:3,1)=h1
c        QMat(1:3,2)=h2
c        QMat(1:3,3)=h3*RMat3/h3d
c        call MatInv(QMat,QMatI,pom,3)
c        call MultM(RMat,QMatI,UB(1,1,KRefBlock),3,3,3)
c        Write(6,'('' UB-mat-1'')')
c        write(6,'(3f10.6)')((UB(i,j,KRefBlock),j=1,3),i=1,3)
c        call DRCellFromUB
c        write(6,'(6f10.4)') CellRefBlock(1:6,KRefBlock)
c        call MultM(UB(1,1,KRefBlock),h1,xp1,3,3,1)
c        call MultM(UB(1,1,KRefBlock),h2,xp2,3,3,1)
c        D1=1./sqrt(scalmul(xp1,xp1))
c        D2=1./sqrt(scalmul(xp2,xp2))
c        U=scalmul(xp1,xp2)*D1*D2
c        write(6,'(3f10.4)') D1,D2,acos(-U)/ToRad

c        QMat(1:3,1)=h1
c        QMat(1:3,2)=h2
c        QMat(1:3,3)=h3
c        call MatInv(QMat,QMatI,pom,3)
c        call MultM(RMat,QMatI,UB(1,1,KRefBlock),3,3,3)
c        Write(6,'('' UB-mat-2'')')
c        write(6,'(3f10.6)')((UB(i,j,KRefBlock),j=1,3),i=1,3)
c        call DRCellFromUB
c        write(6,'(6f10.4)') CellRefBlock(1:6,KRefBlock)
c        call MultM(UB(1,1,KRefBlock),h1,xp1,3,3,1)
c        call MultM(UB(1,1,KRefBlock),h2,xp2,3,3,1)
c        D1=1./sqrt(scalmul(xp1,xp1))
c        D2=1./sqrt(scalmul(xp2,xp2))
c        U=scalmul(xp1,xp2)*D1*D2
c        write(6,'(3f10.4)') D1,D2,acos(-U)/ToRad
c
c
        SinE=sin(Cell2d(3)*ToRad)
        SinC=sqrt(1.-CosC**2)
        scp=Cell2d(1)*Cell2d(2)*SinE/(D1*D2*SinC)

        G =GSave /scp
        Gi=GSavei*scp


c
c        call VecMul(h1,h2,hp)
c        hp=hp/Volp
c        call MultM(G,hp,h3,3,3,1)
c        h3d=sqrt(ScalMul(hp,h3))
c
c        QMat(1:3,1)=h1
c        QMat(1:3,2)=h2
c        QMat(1:3,3)=h3*RMat3/h3d
c        call MatInv(QMat,QMatI,pom,3)
c        call MultM(RMat,QMatI,UB(1,1,KRefBlock),3,3,3)
c        Write(6,'('' UB-mat-3'')')
c        write(6,'(3f10.6)')((UB(i,j,KRefBlock),j=1,3),i=1,3)
c        call DRCellFromUB
c        write(6,'(6f10.4)') CellRefBlock(1:6,KRefBlock)
c        call MultM(UB(1,1,KRefBlock),h1,xp1,3,3,1)
c        call MultM(UB(1,1,KRefBlock),h2,xp2,3,3,1)
c        D1=1./sqrt(scalmul(xp1,xp1))
c        D2=1./sqrt(scalmul(xp2,xp2))
c        U=scalmul(xp1,xp2)*D1*D2
c        write(6,'(3f10.4)') D1,D2,acos(-U)/ToRad
c
c        QMat(1:3,1)=h1
c        QMat(1:3,2)=h2
c        QMat(1:3,3)=h3
c        call MatInv(QMat,QMatI,pom,3)
c        call MultM(RMat,QMatI,UB(1,1,KRefBlock),3,3,3)
c        Write(6,'('' UB-mat-4'')')
c        write(6,'(3f10.6)')((UB(i,j,KRefBlock),j=1,3),i=1,3)
c        call DRCellFromUB
c        write(6,'(6f10.4)') CellRefBlock(1:6,KRefBlock)
c        call MultM(UB(1,1,KRefBlock),h1,xp1,3,3,1)
c        call MultM(UB(1,1,KRefBlock),h2,xp2,3,3,1)
c        D1=1./sqrt(scalmul(xp1,xp1))
c        D2=1./sqrt(scalmul(xp2,xp2))
c        U=scalmul(xp1,xp2)*D1*D2
c        write(6,'(3f10.4)') D1,D2,acos(-U)/ToRad

        CosMax=0.
        HMax=-9999
        call VecMul(h1,h2,hp)
        hp=hp/Volp
        call MultM(G,hp,h3,3,3,1)
        h3d=sqrt(ScalMul(hp,h3))
        hno=h3/h3d
        do i1=0,5
          ih(1)=i1
          do i2=-5,5
            if(i1.eq.0.and.i2.lt.0) cycle
            ih(2)=i2
            do i3=-5,5
              if(i1.eq.0.and.i2.eq.0.and.i3.le.0) cycle
              ih(3)=i3
              call MinMultMaxFract(ih,3,MinMult,MaxFract)
              if(MaxFract.gt.1) cycle
              h3=ih(1:3)
              call MultM(Gi,h3,hp,3,3,1)
              h3d=sqrt(ScalMul(hp,h3))
              hp=hp/h3d
              CosP=scalmul(hno,hp)
              if(CosP.gt.CosMax) then
                HMax=ih(1:3)
                CosMax=CosP
              else if(-CosP.gt.CosMax) then
                HMax=ih(1:3)
                CosMax=-CosP
              endif
            enddo
          enddo
        enddo
        h3=HMax
        call MultM(Gi,h1,hp,3,3,1)
        h1h3=ScalMul(hp,h3)
        call MultM(Gi,h2,hp,3,3,1)
        h2h3=ScalMul(hp,h3)
        call MultM(Gi,hno,hp,3,3,1)
        h3h3=ScalMul(hp,h3)
        QMat(1,1:3)=x1
        QMat(2,1:3)=x2
        QMat(3,1:3)=x3
        call MatInv(QMat,QMatI,pom,3)
        do i=1,3
          x3(i)=QMatI(i,1)*h1h3+QMatI(i,2)*h2h3+QMatI(i,3)*h3h3
        enddo
        QMat(1:3,1)=h1
        QMat(1:3,2)=h2
        QMat(1:3,3)=h3
        call MatInv(QMat,QMatI,pom,3)
        RMat(1:3,1)=x1
        RMat(1:3,2)=x2
        RMat(1:3,3)=x3
        call MultM(RMat,QMatI,UB(1,1,KRefBlock),3,3,3)
        call DRCellFromUB
        call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),CellVolume,3)
        do i=1,3
          do j=1,3
            CellDir(j,i)=UBI(i,j,KRefBlock)/CellRefBlock(i,KRefBlock)
          enddo
        enddo
        if(.not.Konec) then
          if(LblStateQuest(nLblTxUB).eq.LblOff)
     1      call FeQuestLblOn(nLblTxUB)
          nLbl=nLblUB
          do i=1,3
            write(Veta,'(3f10.6)')(UB(i,j,KRefBlock),j=1,3)
            call FeQuestLblChange(nLbl,Veta)
            nLbl=nLbl+1
          enddo
          if(LblStateQuest(nLblTxCell3d).eq.LblOff)
     1      call FeQuestLblOn(nLblTxCell3d)
          write(Veta,'(3f10.3,3f10.2)') CellRefBlock(1:6,KRefBlock)
          call FeQuestLblChange(nLblCell3d,Veta)
          go to 1500
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        CellRefBlock(1:6,KRefBlock)=CellParIn
        do i=1,3
          do j=1,3
            UB(i,j,KRefBlock)=UBIn(i,j)
          enddo
        enddo
        call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),CellVolume,3)
        do i=1,3
          do j=1,3
            CellDir(j,i)=UBI(i,j,KRefBlock)/CellFor2d(i)
          enddo
        enddo
      endif
9999  return
100   format(2f10.4,f10.3)
      end
