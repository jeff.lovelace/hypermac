      subroutine DRCell2MetTens(Cell,gg)
      include 'fepc.cmn'
      dimension gg(3,3),Cell(6)
      do i=1,3
        gg(i,i)=Cell(i)**2
      enddo
      gg(1,2)=Cell(1)*Cell(2)*cos(ToRad*Cell(6))
      gg(1,3)=Cell(1)*Cell(3)*cos(ToRad*Cell(5))
      gg(2,3)=Cell(2)*Cell(3)*cos(ToRad*Cell(4))
      gg(2,1)=gg(1,2)
      gg(3,1)=gg(1,3)
      gg(3,2)=gg(2,3)
      return
      end
