      subroutine GoToRef(nr,nrmin,nrmax,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,110.,1,
     1                   'Next reflection to be drawn',0,LightGray,0,0)
      call FeQuestEudMake(id,0.,0,40.,1,' ','C',30.,EdwYd,0)
      nEdw=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nr,.false.)
      call FeQuestEudOpen(EdwLastMade,nrmin,nrmax,1,pom,pom,pom)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) call FeQuestIntFromEdw(nEdw,nr)
      call FeQuestRemove(id)
      return
      end
