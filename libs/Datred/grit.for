      subroutine grit
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension gu(3,3),sum(mxface),temp(3),xg(128,3),wg(128,3),xuz(3),
     1          xcz(3),abound(3,3),dist(3),bbound(2,2),di(2),xyz(3),
     2          cbound(3,3),dbound(2,2)
      logical inside
      xomi= 100.
      yomi= 100.
      zomi= 100.
      xoma=-100.
      yoma=-100.
      zoma=-100.
      call multm(ub(1,1,KRefBlock),MetTens(1,1,KPhase),gu,3,3,3)
      do i=1,NFacesR
        call multm(MetTensI(1,1,KPhase),DfaceR(1,i),temp,3,3,1)
        sum(i)=sqrt(scalmul(dfaceR(1,i),temp))
        dface4R(i)=dfaceR(4,i)*sum(i)*.1
      enddo
      do i=1,3
        call gauleg(0.,1.,xg(1,i),wg(1,i),GaussRefBlock(i,KRefBlock))
      enddo
      npoints=0
      volg=0.
      xmin= 999999.
      xmax=-999999.
      do k=1,NFacesR-2
        do i=1,3
          abound(1,i)=dfaceR(i,k)
        enddo
        do l=k+1,NFacesR-1
          do i=1,3
            abound(2,i)=dfaceR(i,l)
          enddo
          do 2000m=l+1,NFacesR
            do i=1,3
              abound(3,i)=dfaceR(i,m)
            enddo
            call matinv(abound,cbound,det,3)
            if(det.eq.0.) go to 2000
            dist(1)=dface4R(k)
            dist(2)=dface4R(l)
            dist(3)=dface4R(m)
            call multm(cbound,dist,xyz,3,3,1)
            if(.not.inside(xyz,sum)) go to 2000
            call multm(gu,xyz,xuz,3,3,1)
            do i=1,3
              xcz(i)=xyz(i)*CellRefBlock(i,0)
            enddo
            xomi=min(xcz(1),xomi)
            xoma=max(xcz(1),xoma)
            yomi=min(xcz(2),yomi)
            yoma=max(xcz(2),yoma)
            zomi=min(xcz(3),zomi)
            zoma=max(xcz(3),zoma)
            xx=xyz(1)
            xmin=min(xmin,xx)
            xmax=max(xmax,xx)
2000      continue
        enddo
      enddo
      nia=GaussRefBlock(1,KRefBlock)
      do npaa=1,nia
        if(nia.gt.1) then
          xyz(1)=xmin+(xmax-xmin)*xg(npaa,1)
          r1=wg(npaa,1)*(xmax-xmin)
        endif
        ymin= 999999.
        ymax=-999999.
        do k=1,NFacesR-1
          bbound(1,1)=dfaceR(2,k)
          bbound(1,2)=dfaceR(3,k)
          di(1)=dface4R(k)-dfaceR(1,k)*xyz(1)
          do 3000l=k+1,NFacesR
            bbound(2,1)=dfaceR(2,l)
            bbound(2,2)=dfaceR(3,l)
            call matinv(bbound,dbound,det,2)
            if(det.eq.0.) go to 3000
            di(2)=dface4R(l)-dfaceR(1,l)*xyz(1)
            xyz(2)=dbound(1,1)*di(1)+dbound(1,2)*di(2)
            xyz(3)=dbound(2,1)*di(1)+dbound(2,2)*di(2)
            if(inside(xyz,sum)) then
              xx=xyz(2)
              ymin=min(ymin,xx)
              ymax=max(ymax,xx)
            endif
3000      continue
        enddo
        nib=GaussRefBlock(2,KRefBlock)
        do npb=1,nib
          xyz(2)=ymin+(ymax-ymin)*xg(npb,2)
          r2=wg(npb,2)*r1*(ymax-ymin)
          zmin= 999999.
          zmax=-999999.
          do 4000i=1,NFacesR
            if(dfaceR(3,i).eq.0.) go to 4000
            xyz(3)=(dface4R(i)-xyz(1)*dfaceR(1,i)
     1                        -xyz(2)*dfaceR(2,i))/dfaceR(3,i)
            if(inside(xyz,sum)) then
              xx=xyz(3)
              zmin=min(zmin,xx)
              zmax=max(zmax,xx)
            endif
4000      continue
          nic=GaussRefBlock(3,KRefBlock)
          deltaz=zmax-zmin
          do npc=1,nic
            npoints=npoints+1
            gridp(1,npoints)=xyz(1)
            gridp(2,npoints)=xyz(2)
            gridp(3,npoints)=zmin+deltaz*xg(npc,3)
            gridp(4,npoints)=wg(npc,3)*r2*deltaz
            volg=volg+gridp(4,npoints)
          enddo
        enddo
      enddo
      return
      end
