      subroutine DRAbsCorr(JenDopredu)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Veta,FormulaOld,Label
      character*256 EdwStringQuest
      dimension AtAbsCoeffOld(20)
      integer CrwStateQuest,EdwStateQuest,Exponent10
      logical FeYesNoHeader,CrwLogicQuest,DefFormula,JenDopredu,
     1        ChangeFaces,ChangeAbsCoeff,ChangeSemiEmpir,ExistFile,lpom
      real CellParOld(6),dcp(3),dcpt(3)
      CellParOld(1:6)=CellPar(1:6,1,KPhase)
      call DRSetCell(0)
      if(SilentRun) then
        CorrAbsNew=CorrAbs(KRefBlock)
        FormulaOld=Formula(KPhase)
        go to 4100
      endif
      id=NextQuestId()
      Veta='Define parameters for absorption and scaling procedure'
      call FeQuestTitleMake(id,Veta)
      QuestCheck(id)=1
      if(CorrAbs(KRefBlock).lt.0) then
        CorrAbsNew=IdCorrAbsGauss
      else
        CorrAbsNew=CorrAbs(KRefBlock)
        if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
     1    RadiusRefBlockOld=RadiusRefBlock(KRefBlock)
      endif
      ChangeAbsCoeff=.false.
      ChangeFaces=.false.
      RadiusRefBlockOld=RadiusRefBlock(KRefBlock)
      FormulaOld=Formula(KPhase)
      if(Formula(KPhase).eq.' ') FFType(KPhase)=-62
      call SetRealArrayTo(AtAbsCoeffOld,20,-1.)
      call CopyVek(AtAbsCoeff(1,KPhase),AtAbsCoeffOld,
     1             NAtFormula(KPhase))
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+10.
      xmx=0.
      do i=1,4
        if(i.le.3) then
          igr=1
        else
          igr=0
        endif
        il=il+1
        if(i.eq.1) then
          Veta='%None or done before importing'
        else if(i.eq.2) then
          Veta='Correction for %spherical sample'
        else if(i.eq.3) then
          Veta='%Gaussian integration method'
        else if(i.eq.4) then
          Veta='E%mpirical correction and/or frame scaling'
        endif
        xmx=max(xmx,tpom+FeTxLengthUnder(Veta)+20.)
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      igr)
        if(i.le.3) then
          lpom=i.eq.mod(CorrAbsNew,10)+1
        else
          lpom=CorrAbsNew/10.eq.1
        endif
        call FeQuestCrwOpen(CrwLastMade,lpom)
        if(i.eq.1) then
          nCrwRemove=CrwLastMade
        else if(i.eq.2) then
          nCrwSphere=CrwLastMade
        else if(i.eq.3) then
          nCrwGauss=CrwLastMade
        else if(i.eq.4) then
          nCrwSemiEmpir=CrwLastMade
        endif
      enddo
      if(ExistFile(RefBlockFileName)) then
        call OpenFile(95,RefBlockFileName,'formatted','old')
      else
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
      endif
      if(ErrFlag.eq.0) then
        call DRGetReflectionFromM95(95,iend,ich)
        call DRSetCell(0)
        MameDirCos=.true.
        do j=1,2
          do i=1,3
            dcp(i)=dircos(i,j)*rcp(i,1,KPhaseDR)
          enddo
          call multm(MetTens(1,1,KPhaseDR),dcp,dcpt,3,3,1)
          pom=sqrt(scalmul(dcp,dcpt))
          if(abs(abs(pom)-1.).gt..01) then
            if(abs(pom).gt.0.)
     1        call FeChybne(-1.,-1.,'the direction cosines do not '//
     2                      'fulfill the normaliation condition.',
     3                      'Absorption correction by the Gaussian'//
     4                      ' integration cannot be performed.',
     5                      SeriousError)
            MameDirCos=.false.
            exit
          endif
        enddo
      endif
      call CloseIfOpened(95)
      if(.not.MameDirCos) call FeQuestCrwDisable(nCrwGauss)
      il=il-3
      xpom=xmx
      dpom=100.
      tpom=xpom+dpom+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='radius of the sp%here [mm]'
        else if(i.eq.2) then
          Veta='%integration grid'
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwRadSphere=EdwLastMade
        else if(i.eq.2) then
          nEdwGrid=EdwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      Veta='Edit %faces'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtEditFaces=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ilp=-il*10-8
      write(Cislo,FormI15) NFaces(KRefBlock)
      call Zhusti(Cislo)
      Label='Crystal shape enclosed by '//Cislo(:idel(Cislo))//' faces'
      call FeQuestLblMake(id,xpom,ilp,Label,'L','N')
      nLblFaces=LblLastMade
      il=il+2
      call FeQuestLinkaMake(id,il)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      AmiRefBlockOld=AmiRefBlock(KRefBlock)
      DefFormula=AmiRefBlock(KRefBlock).le.0.
      if(DefFormula) then
        if(Formula(KPhase).ne.' ') then
          call DRCalcDenAbs(wmol,dx,ich)
          AmiUsedOld=AmiUsed
        else
          AmiUsedOld=-1.
        endif
      else
        AmiUsedOld=AmiRefBlock(KRefBlock)
      endif
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='D%efine absorption coefficient'
          jk=1
          ichk=0
        else if(i.eq.2) then
          Veta='%Define formula'
          jk=2
          ichk=1
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      2)
        call FeQuestCrwOpen(CrwLastMade,DefFormula.eqv.i.eq.2)
        tpome=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        xpome=tpome+FeTxLengthUnder(Veta)+10.
        ilp=il
        do j=1,jk
          tpo=tpome
          xpo=xpome
          if(j.eq.1) then
            if(i.eq.1) then
              dpom=100.
            else
              dpom=250.
              xdal=xpo+dpom
            endif
          else
            Veta='Number of formula %units'
            tpo=xdal-dpom
            xpo=tpo+FeTxLengthUnder(Veta)+10.
            dpom=xdal-xpo
            xdal=xdal+20.
          endif
          call FeQuestEdwMake(id,tpo,ilp,xpo,ilp,Veta,'L',dpom,
     1                        EdwYd,ichk)
          if(j.eq.1) then
            if(i.eq.1) then
              nEdwAmi=EdwLastMade
            else
              nEdwFormula=EdwLastMade
            endif
          else
            nEdwZ=EdwLastMade
          endif
          ilp=ilp+1
        enddo
        if(i.eq.1) then
          nCrwDefAmi=CrwLastMade
        else if(i.eq.2) then
          nCrwDefFormula=CrwLastMade
        endif
      enddo
      Veta='C%alculate density'
      xpom=xdal
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      il=il+1
      Veta='Cr%oss sections'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtCross=ButtonLastMade
      call DRCalcDenAbs(wmol,dx,ich)
1400  if(mod(CorrAbsNew,10).gt.0) then
        if(CrwStateQuest(nCrwDefFormula).eq.CrwDisabled) then
          call FeQuestCrwOpen(nCrwDefFormula,DefFormula)
          call FeQuestCrwOpen(nCrwDefAmi,.not.DefFormula)
        endif
        if(DefFormula) then
          call FeQuestEdwDisable(nEdwAmi)
          if(EdwStateQuest(nEdwFormula).ne.EdwOpened) then
            call FeQuestStringEdwOpen(nEdwFormula,Formula(KPhase))
            call FeQuestIntEdwOpen(nEdwZ,NUnits(KPhase),.false.)
            call FeQuestButtonOff(nButtCalculateDensity)
            call FeQuestButtonOff(nButtCross)
          endif
        else
          call FeQuestEdwDisable(nEdwFormula)
          call FeQuestEdwDisable(nEdwZ)
          call FeQuestButtonDisable(nButtCalculateDensity)
          call FeQuestButtonDisable(nButtCross)
          if(EdwStateQuest(nEdwAmi).ne.EdwOpened)
     1      call FeQuestRealEdwOpen(nEdwAmi,AmiRefBlock(KRefBlock),
     2                              AmiRefBlock(KRefBlock).le.0.,
     3                              .false.)
        endif
      else
        call FeQuestEdwDisable(nEdwAmi)
        call FeQuestEdwDisable(nEdwFormula)
        call FeQuestEdwDisable(nEdwZ)
        call FeQuestCrwDisable(nCrwDefFormula)
        call FeQuestCrwDisable(nCrwDefAmi)
        call FeQuestButtonDisable(nButtCalculateDensity)
        call FeQuestButtonDisable(nButtCross)
      endif
      if(mod(CorrAbsNew,10).ne.IdCorrAbsSphere)
     1  call FeQuestEdwDisable(nEdwRadSphere)
      if(mod(CorrAbsNew,10).ne.IdCorrAbsGauss) then
        call FeQuestEdwDisable(nEdwGrid)
        call FeQuestButtonDisable(nButtEditFaces)
        call FeQuestLblOff(nLblFaces)
      endif
      if((mod(CorrAbsNew,10).eq.IdCorrAbsNone).or.
     1    .not.MameDircos) then
        Veta='Fra%me scaling'
      else
        Veta='E%mpirical correction and/or frame scaling'
      endif
      if(MameDirCos.or.NRuns(KRefBlock).gt.0) then
        if(CrwStateQuest(nCrwSemiEmpir).eq.CrwDisabled.or.
     1     CrwStateQuest(nCrwSemiEmpir).eq.CrwClosed)
     2    call FeQuestCrwOff(nCrwSemiEmpir)
        call FeQuestCrwLabelChange(nCrwSemiEmpir,Veta)
      else
        if(CrwStateQuest(nCrwSemiEmpir).ne.CrwDisabled)
     1    call FeQuestCrwDisable(nCrwSemiEmpir)
      endif
      if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
        if(EdwStateQuest(nEdwRadSphere).ne.EdwOpened)
     1     call FeQuestRealEdwOpen(nEdwRadSphere,
     2       RadiusRefBlock(KRefBlock),.false.,.false.)
      else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
        if(EdwStateQuest(nEdwGrid).ne.EdwOpened) then
          call FeQuestIntAEdwOpen(nEdwGrid,GaussRefBlock(1,KRefBlock),3,
     1                            .false.)
          call FeQuestButtonOff(nButtEditFaces)
          call FeQuestLblOn(nLblFaces)
        endif
      endif
      if(JenDopredu) call FeQuestButtonDisable(ButtonEsc)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        if(mod(CorrAbsNew,10).ne.0) then
          if(DefFormula) then
            if(Formula(KPhase).eq.' ') then
              Veta='the formula has not yet been defined'
              nEdw=nEdwFormula
              go to 1510
            endif
            AmiRefBlock(KRefBlock)=-1.
          else
            call FeQuestRealFromEdw(nEdwAmi,AmiRefBlock(KRefBlock))
            if(AmiRefBlock(KRefBlock).le.0.) then
              Veta='absorption coefficient not defined'
              nEdw=nEdwAmi
              go to 1510
            endif
          endif
        endif
        if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
          call FeQuestRealFromEdw(nEdwRadSphere,
     1                            RadiusRefBlock(KRefBlock))
          if(RadiusRefBlock(KRefBlock).le.0.) then
            Veta='the radius has not been defined'
            nEdw=nEdwRadSphere
            go to 1510
          endif
        else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
          if(NFaces(KRefBlock).le.3) then
            Veta='the number of faces <4'
            nEdw=nEdwGrid
            go to 1510
          else
            call FeQuestIntAFromEdw(nEdwGrid,GaussRefBlock(1,KRefBlock))
            i=GaussRefBlock(1,KRefBlock)*GaussRefBlock(2,KRefBlock)*
     1        GaussRefBlock(3,KRefBlock)
            if(i.le.0) then
              Veta='number of grid points is too small'
              go to 1505
            else
              allocate(gridp(4,i))
            endif
          endif
        endif
        QuestCheck(id)=0
        go to 1500
1505    nEdw=nEdwGrid
1510    call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.ge.nCrwRemove.and.CheckNumber.le.nCrwSemiEmpir))
     2  then
        nCrw=nCrwRemove
        do CorrAbsNew=0,2
          if(CrwLogicQuest(nCrw)) go to 1520
          nCrw=nCrw+1
        enddo
        CorrAbsNew=IdCorrAbsGauss
1520    if(CrwLogicQuest(nCrwSemiEmpir)) CorrAbsNew=CorrAbsNew+10
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwDefFormula.or.
     2         CheckNumber.eq.nCrwDefAmi)) then
        DefFormula=CrwLogicQuest(nCrwDefFormula)
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFormula) then
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        call ReallocFormF(20,NPhase,NRefBlock)
        call PitFor(0,ich)
        if(ich.eq.0) then
          if(.not.ExistM50.or.mod(StatusM50,100).ge.10) then
            FFType(KPhase)=-62
            call SetStringArrayTo(AtTypeMag(1,KPhase),
     1                            NAtFormula(KPhase),' ')
            call SetStringArrayTo(AtTypeMagJ(1,KPhase),
     1                            NAtFormula(KPhase),' ')
            call EM50ReadAllFormFactors
            call EM50FormFactorsSet
          endif
          go to 1500
        endif
        EventType=EventEdw
        EventNumber=nEdwFormula
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwZ) then
        call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtEditFaces) then
        if(DefFormula) then
          call DRCalcDenAbs(wmol,dx,ich)
          if(ich.ne.0) AmiUsed=-1.
        else
          call FeQuestRealFromEdw(nEdwAmi,AmiRefBlock(KRefBlock))
          AmiUsed=AmiRefBlock(KRefBlock)
        endif
        write(Veta,'(f10.3)') AmiUsed
        call DRReadFaces(ChangeFaces,ich)
        if(ich.eq.0) then
          write(Cislo,FormI15) NFaces(KRefBlock)
          call Zhusti(Cislo)
          Label(27:)=Cislo(:idel(Cislo))//' faces'
          call FeQuestLblChange(nLblFaces,Label)
        endif
        EventType=EventEdw
        EventNumber=nEdwGrid
        go to 1500
      else if(CheckType.eq.EventButton) then
        call DRCalcDenAbs(wmol,dx,ich)
        if(CheckNumber.eq.nButtCalculateDensity) then
          write(TextInfo(1),'(''Molecular weight = '',f8.2)') wmol
          write(TextInfo(2),'(''Calculated density ='',f7.3,
     1                        '' g.cm**(-3)'')') dx
          TextInfo(3)='Absorption coefficient mi'
          if(RadiationRefBlock(KRefBlock).eq.XRayRadiation) then
            if(KLamRefBlock(KRefBlock).gt.0) then
              write(Veta,'(''('',a2,''-Kalfa) ='',f9.3,'' mm**-1'')')
     1          LamTypeD(KLamRefBlock(KRefBlock)),AmiUsed
            else
              write(Veta,'('' ='',f9.3,'' mm**-1'')') AmiUsed
            endif
          else if(RadiationRefBlock(KRefBlock).eq.NeutronRadiation) then
            write(Veta,'('' ='',f9.6,'' mm**-1'')') AmiUsed
          endif
          TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//
     1                Veta(:idel(Veta))
          Ninfo=3
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
        else if(CheckNumber.eq.nButtCross) then
          xdq=500.
          WizardMode=.false.
          idp=NextQuestId()
          ild=(NAtFormula(KPhase)-1)/2+3
          call FeQuestCreate(idp,-1.,-1.,xdq,ild,'Define atomic '//
     1                       'cross sections',0,LightGray,0,0)
          tpom=5.
          xpom=tpom+FeTxLength('XX')+10.
          dpom=100.
          spom=xpom+dpom+30.
          il=1
          do i=1,NAtFormula(KPhase)
            if(il.eq.1) then
              call FeQuestLblMake(idp,xpom,il,'User defined','L','N')
              call FeQuestLblMake(idp,spom,il,'Tabulated','L','N')
            endif
            il=il+1
            call FeQuestEdwMake(idp,tpom,il,xpom,il,AtType(i,1),'L',
     1                          dpom,EdwYd,0)
            if(i.eq.1) nEdwFirst=EdwLastMade
            call FeQuestRealEdwOpen(EdwLastMade,AtAbsCoeff(i,KPhase),
     1                              .false.,.false.)
            j=5-Exponent10(AtAbsCoeffTab(i))
            j=min(13,j)
            j=max(0,j)
            write(RealFormat(6:7),'(i2)') j
            write(Cislo,RealFormat) AtAbsCoeffTab(i)
            call ZdrcniCisla(Cislo,1)
            call FeQuestLblMake(idp,spom,il,Cislo,'L','N')
            if(il.eq.(NAtFormula(KPhase)-1)/2+2) then
              il=1
              xpom=xpom+xdq*.5
              tpom=tpom+xdq*.5
              spom=spom+xdq*.5
            endif
          enddo
          il=ild
          xpom=5.
          call FeQuestLblMake(idp,xpom,il,
     1                        'Reset to tabulated values->','L','N')
          Veta='%Individual'
          dpom=FeTxLengthUnder(Veta)+20.
          xpom=xdq*.5-dpom-10.
          call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
          nButtIndividual=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          Veta='%All'
          xpom=xdq*.5+10.
          call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
          nButtAdd=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1550      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventButton) then
            if(CheckNumber.eq.nButtIndividual) then
              i1=EdwActive-EdwFr+1
              i2=i1
            else
              i1=1
              i2=NAtFormula(KPhase)
            endif
            do i=i1,i2
              AtAbsCoeff(i,KPhase)=AtAbsCoeffTab(i)
              nEdw=nEdwFirst+i-1
              call FeQuestRealEdwOpen(nEdw,AtAbsCoeff(i,KPhase),.false.,
     1                                .false.)
            enddo
            EventType=EventEdw
            EventNumber=nEdwFirst+i1-1
            go to 1550
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1550
          endif
          if(ich.eq.0) then
            nEdw=nEdwFirst
            AtAbsCoeffOwn(KPhase)=.false.
            do i=1,NAtFormula(KPhase)
              call FeQuestRealFromEdw(nEdw,AtAbsCoeff(i,KPhase))
              if(AtAbsCoeff(i,KPhase).gt.0.) then
                if(abs((AtAbsCoeff(i,KPhase)-AtAbsCoeffOld(i))/
     1                  AtAbsCoeff(i,KPhase)).gt..00001)
     2            ChangeAbsCoeff=.true.
              else
                if(abs(AtAbsCoeff(i,KPhase)-AtAbsCoeffOld(i)).gt.
     1             abs(AtAbsCoeff(i,KPhase)+AtAbsCoeffOld(i)))
     2            ChangeAbsCoeff=.true.
              endif
              if(AtAbsCoeff(i,KPhase).gt.0.) then
                if(abs((AtAbsCoeff(i,KPhase)-AtAbsCoeffTab(i))/
     1                  AtAbsCoeff(i,KPhase)).gt..00001)
     2            AtAbsCoeffOwn(KPhase)=.true.
              else
                if(abs(AtAbsCoeff(i,KPhase)-AtAbsCoeffTab(i)).gt.
     1             abs(AtAbsCoeff(i,KPhase)+AtAbsCoeffTab(i)))
     2            AtAbsCoeffOwn(KPhase)=.true.
              endif
              nEdw=nEdw+1
            enddo
          endif
          call FeQuestRemove(idp)
        endif
        WizardMode=.true.
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(.not.ChangeAbsCoeff)
     1    call CopyVek(AtAbsCoeffOld,AtAbsCoeff(1,KPhase),
     2                 NAtFormula(KPhase))
        if(DefFormula) then
          call DRCalcDenAbs(wmol,dx,ich)
          if(ich.ne.0) then
            call FeChybne(-1.,-1.,'in calculation of the absorption '//
     1                    'coefficient.','Check the chemical formula.',
     1                    SeriousError)
            go to 9900
          endif
        else
          call FeQuestRealFromEdw(nEdwAmi,AmiRefBlock(KRefBlock))
          AmiUsed=AmiRefBlock(KRefBlock)
        endif
      endif
      if(ich.ne.0) then
        if(ich.gt.0) then
          ErrFlag=-1
        else
          ErrFlag= 1
        endif
        go to 9999
      else if(CorrAbsNew/10.eq.1) then
        call DRReflForAbsCorrSetting(1,ChangeSemiEmpir,ich)
        if(ich.ne.0) then
          if(CorrAbs(KRefBlock)/10.ne.1) then
            CorrAbsNew=mod(CorrAbsNew,10)
            RunScN(KRefBlock)=0
            SpHEven(KRefBlock)=0
            SpHOdd(KRefBlock)=0
            SpHN(KRefBlock)=0
          endif
        endif
      endif
      if(CorrAbsNew.eq.CorrAbs(KRefBlock)) then
        if(CorrAbsNew/10.eq.1.and.ChangeSemiEmpir) go to 4100
        if(AmiUsedOld.ne.AmiUsed) go to 4100
        if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
          if(ChangeFaces) go to 4100
        else if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
          if(abs(RadiusRefBlockOld-RadiusRefBlock(KRefBlock)).gt..001)
     1      go to 4100
        else if(CorrAbsNew.eq.0) then
          go to 9999
        endif
        NInfo=1
        TextInfo(1)='No change detected which could the affect '//
     1              'previous made correction'
        if(.not.FeYesNoHeader(-1.,-1.,'Do you want to redo the '//
     1                        'correction data anyhow?',0)) go to 9999
      endif
4100  if(CorrAbsNew.eq.0) then
        UseExpCorr(KRefBlock)=1
      else
        UseExpCorr(KRefBlock)=0
      endif
      call DRAbsCorrRun(ich)
      if(ich.ne.0) go to 9900
      CorrAbs(KRefBlock)=CorrAbsNew
      ModifiedRefBlock(KRefBlock)=.true.
      if(abmin.le.0.or.abmax.le.0.) then
        abmin=1.
        abmax=1.
      endif
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.eq.0.and.CorrAbsNew.ne.0) then
        LnSum=NextLogicNumber()
        call OpenFile(LnSum,fln(:ifln)//'_Absorption.l70','formatted',
     1                'unknown')
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,'(''# Absorption'')')
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,FormA)
        if(CorrAbsNew.eq.0) then
          Veta='No'
        else if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
          Veta='Sphere'
        else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
          Veta='Gaussian'
        else
          Veta='None'
        endif
        write(LnSum,FormCIF) CifKey(4,11),Veta(:idel(Veta))
        if(CorrAbsNew.eq.0) then
          write(LnSum,FormCIF) CifKey(5,11),'?'
        else
          write(LnSum,FormCIF) CifKey(5,11)
          Veta=' ''Jana2006'''
          write(LnSum,FormA) Veta(:idel(Veta))
        endif
        write(Cislo,'(f10.3)') AmiUsed
        call ZdrcniCisla(Cislo,1)
        write(LnSum,FormCIF) CifKey(1,11),Cislo(:idel(Cislo))
        do i=1,2
          if(i.eq.1) then
            pom=abmin
          else
            pom=abmax
          endif
          write(Cislo,'(f9.4)') pom
          call ZdrcniCisla(Cislo,1)
          write(LnSum,FormCIF) CifKey(4-i,11),Cislo(:idel(Cislo))
        enddo
        if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
          write(lnSum,'(''loop_'')')
          do i=25,28
            write(LnSum,FormA) ' '//CifKey(i,11)(:idel(CifKey(i,11)))
          enddo
          do i=1,NFaces(KRefBlock)
            write(Veta,'(3f10.3,f10.6)') DFace(1:4,i,KRefBlock)
            call ZdrcniCisla(Veta,4)
            write(LnSum,FormA) Veta(:idel(Veta))
          enddo
        endif
        call CloseIfOpened(LnSum)
      endif
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      if(ExistM50) then
        CellPar(1:6,1,KPhase)=CellParOld(1:6)
        if(FormulaOld.ne.Formula(KPhase)) then
          call PitFor(0,ich)
          do i=1,NAtFormula(KPhase)
            AtTypeFull(i,KPhase)=AtType(i,KPhase)
          enddo
          FFType(KPhase)=-62
          call EM50ReadAllFormFactors
          call EM50FormFactorsSet
        endif
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      go to 9999
9900  ErrFlag=1
9999  if(allocated(gridp)) deallocate(gridp)
      call iom50(0,0,fln(:ifln)//'.m50')
      return
      end
