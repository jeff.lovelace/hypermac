      subroutine DRCorrLPDecay(JenDopredu)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ris(10000,10),xia(10000,10),yp(10000),nst(10),
     1          ri0(10),rid(10),risa(10),rismx(10),rismn(10),jp(10)
      character*80 t80
      character*8  SvFile
      logical eqiv,ExistFile,Vystraha,CrwLogicQuest,JenDopredu
      real LPCorrection
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      LPTypeCorrUse=PolarizationRefBlock(KRefBlock)
      FractPerfMonUse=FractPerfMonRefBlock(KRefBlock)
      if(LPTypeCorrUse.eq.PolarizedGuinier) then
        snm=sin(AlphaGMonRefBlock(KRefBlock)*ToRad)
        BetaGMonUse=BetaGMonRefBlock(KRefBlock)*ToRad
      else
        snm=sin(AngleMonRefBlock(KRefBlock)*ToRad)
      endif
      Cos2ThMon =1.-2.*snm**2
      Cos2ThMonQ=Cos2ThMon**2
      Vystraha=.false.
      call SetIntArrayTo(nst,10,0)
      call SetRealArrayTo(risa,10,0.)
      call SetRealArrayTo(rismx,10,0.)
      call SetRealArrayTo(rismn,10,9999999.)
      id=NextQuestId()
      t80='LP and decay correction'
      call FeQuestTitleMake(id,t80)
      xpom=5.
      tpom=xpom+CrwgXd+3.
      t80='%Linear decay'
      il=0
      do i=1,4
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,CrwgYd,0,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.max(CorrLP(KRefBlock),1))
        if(i.eq.1) then
          nCrwFirst=CrwLastMade
          t80='%Exponential decay'
        else if(i.eq.2) then
          t80='%Step-like decay'
        else if(i.eq.3) then
          t80='%No decay'
        endif
      enddo
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      if(JenDopredu) call FeQuestButtonDisable(ButtonEsc)
1050  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1050
      endif
      if(ich.eq.0) then
        nCrw=nCrwFirst
        do i=1,4
          if(CrwLogicQuest(nCrw)) then
            korst=i
            go to 1070
          endif
          nCrw=nCrw+1
        enddo
      else
        if(ich.gt.0) then
          ErrFlag=-1
          go to 9999
        else
          go to 9900
        endif
      endif
1070  if(KorSt.eq.CorrLp(KRefBlock)) then
        if(KorSt.eq.1) go to 9999
      endif
      if(ExistFile(RefBlockFileName)) then
        call OpenFile(95,RefBlockFileName,'formatted','old')
      else
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
      endif
      if(ErrFlag.ne.0) go to 9999
      nmod=max(nint(float(nref95(KRefBlock))*.005),10)
      ism=0
      call FeFlowChartOpen(-1.,-1.,nmod,nref95(KRefBlock),
     1                     'Reading reflections from M95',' ',' ')
      iz=0
1100  call DRGetReflectionFromM95(95,iend,ich)
      if(ich.ne.0) go to 9100
      if(iend.ne.0) go to 2000
      call FeFlowChartEvent(iz,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) go to 9999
      endif
      if(iflg(1).lt.0) go to 1100
      if(no.lt.0) then
        do is=1,ism
          if(eqiv(ih,ihs(1,is),NDim(KPhase))) go to 1300
        enddo
        if(ism.ge.10) then
          if(.not.Vystraha) then
            call FeChybne(-1.,-1.,'the maximum number of standard '//
     1                    'reflections exceeded',
     2                    'only first ten will be used.',Warning)
            Vystraha=.true.
          endif
          go to 1100
        endif
        ism=ism+1
        is=ism
        do i=1,NDim(KPhase)
          ihs(i,is)=ih(i)
        enddo
1300    n=nst(is)+1
        ris(n,is)=ri
        xia(n,is)=expos
        nst(is)=n
        risa(is)=risa(is)+ri
        rismx(is)=max(rismx(is),ri)
        rismn(is)=min(rismn(is),ri)
      endif
      go to 1100
2000  call FeFlowChartRemove
      call CloseIfOpened(95)
      n=0
      do i=1,ism
        if(nst(i).gt.1) then
          n=n+1
          if(i.gt.n) then
            do j=1,NDim(KPhase)
              ihs(j,n)=ihs(j,i)
            enddo
            nst(n)=nst(i)
            do j=1,nst(n)
              ris(j,n)=ris(j,i)
              xia(j,n)=xia(j,i)
            enddo
            risa(n)=risa(i)
            rismx(n)=rismx(i)
            rismn(n)=rismn(i)
          endif
        endif
      enddo
      ism=n
      if(ism.le.0.and.korst.ne.4) then
        call FeChybne(-1.,-1.,'the number of standard reflections '//
     1                'isn''t sufficient.',
     2                'no decay correction will be applied.',Warning)
        korst=4
      endif
      call FeMakeAcWin(50.,20.,30.,30.)
      do i=1,ism
        n=nst(i)
        risa(i)=risa(i)/float(n)
        t80='('
        k=2
        do j=1,NDim(KPhase)
          write(t80(k:k+2),'(i3)') ihs(j,i)
          k=k+2
          if(j.ne.NDim(KPhase)) then
            t80=t80(1:k)//','
          else
            t80=t80(1:k)//')'
          endif
          k=k+2
        enddo
        call zhusti(t80)
        call UnitMat(F2O,3)
        d=(xia(n,i)-xia(1,i))/float(n)
        xomn=xia(1,i)-d
        xomx=xia(n,i)+d
        sumxx=0.
        sumxy=0.
        sumx=0.
        sumy=0.
        yomx=0.
        yomn=1.e+20
        do j=1,n
          x=xia(j,i)
          if(korst.eq.2) then
            y=log(ris(j,i))
          else
            y=ris(j,i)
          endif
          yomx=max(yomx,ris(j,i))
          yomn=min(yomn,ris(j,i))
          sumxx=sumxx+x**2
          sumxy=sumxy+x*y
          sumx=sumx+x
          sumy=sumy+y
        enddo
        det=sumxx*float(n)-sumx**2
        ri0(i)=(sumxx*sumy-sumx*sumxy)/det
        rid(i)=(sumxy*float(n)-sumx*sumy)/det
        if(korst.eq.2) then
          ri0(i)=exp(ri0(i))
        else
          rid(i)=rid(i)/ri0(i)
        endif
        do j=1,n
          x=xia(j,i)
          if(korst.eq.2) then
            yp(j)=ri0(i)*exp(x*rid(i))
          else
            yp(j)=ri0(i)*(1+x*rid(i))
          endif
        enddo
        yomn=yomn*.8
        yomx=yomx*1.2
        reconfig=.true.
2200    if(reconfig) then
          call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
          reconfig=.false.
        endif
        call FeClearGrWin
        call FeBottomInfo(' ')
        t80='Standard reflection : '//t80(:idel(t80))
        call FeOutSt(0,XCenAcWin,(YMaxAcWin+YMaxGrWin)*.5,t80,'C',White)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'t')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'I')
        t80='I(ave)='
        write(Cislo,'(i15)') nint(risa(i))
        call Zhusti(Cislo)
        t80=t80(:idel(t80))//Cislo(:idel(Cislo))//', I(min)='
        write(Cislo,'(i15)') nint(rismn(i))
        call Zhusti(Cislo)
        t80=t80(:idel(t80))//Cislo(:idel(Cislo))//', I(max)='
        write(Cislo,'(i15)') nint(rismx(i))
        call Zhusti(Cislo)
        t80=t80(:idel(t80))//Cislo(:idel(Cislo))
        call FeOutSt(0,XCenAcWin,YMaxGrWin-40.,t80,'C',White)
        if(korst.lt.3) then
          if(korst.eq.1) then
            write(t80,'(''I='',i8,''*(1 '',f10.6,''*t)'')')
     1            nint(ri0(i)),rid(i)
            if(rid(i).ge.0.) t80(14:14)='+'
          else
            write(t80,'(''I='',i8,''*exp('',f10.6,''*t)'')')
     1            nint(ri0(i)),rid(i)
          endif
          call zhusti(t80)
          call FeOutSt(0,XCenAcWin,YMaxGrWin-60.,t80,'C',White)
          call FeXYPlot(xia(1,i),yp,n,NormalLine,NormalPlotMode,Red)
        endif
        call FeXYPlot(xia(1,i),ris(1,i),n,NormalLine,NormalPlotMode,
     1                White)
        call FeBottomInfo('To continue press any key or mouse button')
2250    call FeEvent(0)
        if(EventType.eq.EventKey.or.EventType.eq.EventASCII.or.
     1     (EventType.eq.EventMouse.and.
     2      (EventNumber.eq.JeLeftDown.or.EventNumber.eq.JeRightDown)))
     3    then
        else
          go to 2250
        endif
      enddo
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      call FeFlowChartOpen(-1.,-1.,nmod,nref95(KRefBlock),
     1                     'Decay and LP correction',' ',' ')
      if(ExistFile(RefBlockFileName)) then
        call OpenFile(95,RefBlockFileName,'formatted','old')
      else
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l97','formatted','unknown')
      iz=0
      do i=1,ism
        jp(i)=1
      enddo
      rip0=10000.*float(ism)
      ripa=0.
      ripmx=0.
      ripn=0.
      ripmn=1e33
3000  call DRGetReflectionFromM95(95,iend,ich)
      if(ich.ne.0) go to 9100
      if(iend.ne.0) go to 4000
      call FeFlowChartEvent(iz,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) go to 9999
      endif
      if(iflg(1).lt.0) go to 3000
      if(no.gt.0) then
        if(korst.ne.4) then
          rip=0.
          do i=1,ism
            if(korst.eq.1) then
              pom=ri0(i)*(1.+rid(i)*expos)
              rip=rip+pom
            else if(korst.eq.2) then
              pom=ri0(i)*exp(rid(i)*expos)
              rip=rip+pom
            else if(korst.eq.3) then
              do j=jp(i),nst(i)
                if(xia(j,i).gt.expos) go to 3060
              enddo
              j=nst(i)
3060          np=j-1
              pom=ris(np,i)+(ris(np+1,i)-ris(np,i))*(expos-xia(np,i))/
     1                                        (xia(np+1,i)-xia(np,i))
              rip=rip+pom
              jp(i)=j
            endif
          enddo
          ripa=ripa+rip
          ripn=ripn+1.
          ripmn=min(ripmn,rip)
          ripmx=max(ripmx,rip)
          corrf(1)=rip0/rip
        else
          corrf(1)=1.
        endif
        corrf(1)=corrf(1)*LPCorrection(theta*ToRad)
      else
        corrf(1)=1.
      endif
      call DRPutReflectionToM95(ln,nl)
      go to 3000
4000  call CloseIfOpened(95)
      call CloseIfOpened(ln)
      ModifiedRefBlock(KRefBlock)=.true.
      call MoveFile(fln(:ifln)//'.l97',RefBlockFileName)
      call FeFlowChartRemove
      if(ripn.gt.0.) then
        ripa=ripa/ripn
        ripn=max(ripmx-ripa,ripa-ripmn)/ripa*100.
      else
        ism=0
        ripa=0.
        ripn=0.
      endif
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.eq.0) then
        LnSum=NextLogicNumber()
        call OpenFile(LnSum,fln(:ifln)//'_StandardRef.l70','formatted',
     1                'unknown')
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,'(''# StandardRef'')')
        write(LnSum,'(''#'',71(''=''))')
        write(LnSum,FormA)
        write(Cislo,FormI15) ism
        call Zhusti(Cislo)
        write(LnSum,FormCIF) CifKey(125,10),Cislo(:idel(Cislo))
        write(Cislo,'(f6.2)') ripn
        call ZdrcniCisla(Cislo,1)
        write(LnSum,FormCIF) CifKey(122,10),Cislo(:idel(Cislo))
        if(ism.ne.0) then
          write(LnSum,'(''loop_'')')
          m=119
          do i=1,NDim(KPhase)
            write(LnSum,FormCIF) ' '//CifKey(m,10)
            if(i.eq.3) m=235
            m=m+1
          enddo
          do j=1,ism
            write(LnSum,'(2x,6i4)')(ihs(i,j),i=1,NDim(KPhase))
          enddo
        endif
        call CloseIfOpened(LnSum)
      endif
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
4200  CorrLp(KRefBlock)=KorSt
      call EM9ImportSavePar
      go to 9999
9100  call FeReadError(95)
9900  ErrFlag=1
9999  call DeleteFile(fln(:ifln)//'.l97')
      call CloseIfOpened(95)
      call CloseIfOpened(89)
      call CopyFile(fln(:ifln)//'_datred.tmp',
     1              fln(:ifln)//'_datred.txt')
      return
      end
