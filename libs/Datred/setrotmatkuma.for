      subroutine SetRotMatKuma(Angle,Alpha,Axis,Rot)
      include 'fepc.cmn'
      dimension Rot(3,3),AM(3,3),BM(3,3)
      integer Axis
      if(Axis.eq.1) then
        call SetRotMatAboutAxis(Angle,-3,Rot)
      else
        call SetRotMatAboutAxis( Alpha,-2,Rot)
        call SetRotMatAboutAxis( Angle,-3,AM)
        call MultM(Rot,AM,BM,3,3,3)
        call SetRotMatAboutAxis(-Alpha,-2,AM)
        call MultM(BM,AM,Rot,3,3,3)
      endif
      return
      end
