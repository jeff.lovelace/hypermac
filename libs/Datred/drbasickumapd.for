      subroutine DRBasicKumaPD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3)
      character*80 Radka
      NDim95(KRefBlock)=3
      Profil=.true.
      doma=-9999.
      domb=-9999.
      domc=-9999.
      n=0
      j=0
1000  if(n.ge.131) go to 2000
      read(71,FormA80,end=9000,err=9100) Radka
      call Mala(Radka)
      if(Radka(1:1).eq.'p') then
        k=1
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'wave') go to 1000
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'length') go to 1000
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'wl') go to 1000
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 9000
        LamAveRefBlock(KRefBlock)=(2.*xp(1)+xp(2))/3.
        n=n+1
      else if(Radka(1:1).eq.'u') then
        k=1
        call StToReal(Radka,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        j=j+1
        n=n+10
        do i=1,3
          ub(j,i,KRefBlock)=xp(i)
        enddo
      else if(Radka(1:1).eq.'j') then
        k=1
        n=n+100
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'attenuation') go to 1000
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'factor') go to 1000
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        AttFac=xp(1)
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'alpha') go to 1000
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        AlphaKuma=xp(1)
        call Kus(Radka,k,Cislo)
        if(Cislo.ne.'beta') go to 1000
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        BetaKuma=xp(1)
      endif
      go to 1000
2000  pom=1./LamAveRefBlock(KRefBlock)
      do i=1,3
        do j=1,3
          ub(j,i,KRefBlock)=ub(j,i,KRefBlock)*pom
        enddo
      enddo
      call DRCellFromUB
      call SetRealArrayTo(SenseOfAngle,4,-1.)
      SenseOfAngle(2)=1.
      PocitatDirCos=.true.
      DirCosFromPsi=.false.
      go to 9999
9000  call FeChybne(-1.,-1.,'KUMA - basic information missing.',' ',
     1              SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  ErrFlag=1
9999  rewind 71
      return
      end
