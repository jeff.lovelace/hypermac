      subroutine DRReadFaces(Change,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension DFaceOld(4,mxface),rmp(9),rmpi(9)
      character*256 EdwStringQuest,FileName
      character*80 t80,p80
      character*2 nty
      logical eqrv,TextFile,WizardModeOld,Change
      data FileName/' '/
      WizardModeOld=WizardMode
      WizardMode=.false.
      ich=0
      NFacesOld=NFaces(KRefBlock)
      call CopyVek(DFace(1,1,KRefBlock),DFaceOld,4*NFaces(KRefBlock))
      il=15
      if(CallXShape.ne.' ') il=il+1
      xqd=380.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Face specification',0,
     1                   LightGray,0,0)
      dpom=50.
      xpom=xqd-dpom-10.
      il=12
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      il=0
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
      nButtPrevious=ButtonLastMade
      il=1
      tpom=5.
      xpom1= 80.
      xpom2=250.
      dpom1=140.
      dpom2=100.
      call FeQuestLblMake(id,xpom1+10.,il,'Indices','L','N')
      call FeQuestLblMake(id,xpom2+10.,il,'d [mm]','L','N')
      do i=1,10
        il=il+1
        write(t80,100) i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom1,il,t80,'L',dpom1,EdwYd,1)
        if(i.eq.1) nEdwFirst=EdwLastMade
        call FeQuestEdwMake(id,tpom,il,xpom2,il,' ','L',dpom2,EdwYd,1)
      enddo
      il=il+2
      t80='Add %opposite'
      dpom=FeTxLengthUnder(t80)+10.
      p80='%Delete face'
      tpom=(FeTxLengthUnder(p80)+10.)*.5
      xpom=xqd*.5-dpom-tpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtOpposite=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5-tpom
      call FeQuestButtonMake(id,xpom,il,2*tpom,ButYd,p80)
      nButtDelete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      t80='Add %clone'
      xpom=xqd*.5+tpom+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtClone=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      t80='%Transformation'
      dpom=FeTxLengthUnder('XXXXXXXXXXXXXXXXXX')+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtTransform=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      t80='%Revert to original'
      xpom=xqd*.5+5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtOriginal=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      t80='Read from %file'
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtReadFile=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      t80='%Show the shape'
      xpom=xqd*.5+5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtShow=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(CallXShape.ne.' ') then
        il=il+1
        t80='Run %X-shape'
        xpom=xqd*.5-dpom*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
        nButtRunXShape=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtRunXShape=0
      endif
      ik=0
1200  iz=ik+1
      ik=min(ik+10,mxface)
1205  nEdw=nEdwFirst
      do i=iz,ik
        call FeQuestRealAEdwOpen(nEdw,DFace(1,i,KRefBlock),3,
     1                           i.gt.NFaces(KRefBlock),.false.)
        nEdw=nEdw+1
        call FeQuestRealEdwOpen(nEdw,DFace(4,i,KRefBlock),
     1                          i.gt.NFaces(KRefBlock),.false.)
        nEdw=nEdw+1
      enddo
      nEdwLast=nEdw-1
1250  if(iz.gt.1) then
        call FeQuestButtonOff(nButtPrevious)
      else
        call FeQuestButtonDisable(nButtPrevious)
      endif
      if(NFaces(KRefBlock).ge.ik.and.mxface.gt.ik) then
        call FeQuestButtonOff(nButtNext)
      else
        call FeQuestButtonDisable(nButtNext)
      endif
1300  call FeQuestEvent(id,ich)
1310  if(CheckType.eq.EventEdw.and.CheckNumber.le.nEdwLast) then
        nEdw=CheckNumber
        im=mod(nEdw,2)
        il=(nEdw-1)/2+iz
        if(EdwStringQuest(nEdw).eq.' ') then
          if(il.le.NFaces(KRefBlock)) then
            if(im.eq.1) then
              do i=il+1,NFaces(KRefBlock)
                call CopyVek(DFace(1,i,KRefBlock),
     1                       DFace(1,i-1,KRefBlock),4)
              enddo
              NFaces(KRefBlock)=NFaces(KRefBlock)-1
            else
              call FeChybne(-1.,-1.,'to remove the face you have to'//
     1                      ' delete thier indices.',' ',SeriousError)
              EventType=EventEdw
              EventNumber=nEdw-1
            endif
            go to 1205
          else
            go to 1300
          endif
        else
          call FeQuestRealAFromEdw(nEdw,DFace(4-3*im,il,KRefBlock))
          if(il.gt.NFaces(KRefBlock)) then
            NFaces(KRefBlock)=NFaces(KRefBlock)+1
            if(il.gt.NFaces(KRefBlock))
     1        call FeQuestRealAEdwOpen(nEdw,DFace(4-3*im,il,KRefBlock),
     2                                 3,.true.,.false.)
            if(NFaces(KRefBlock).ge.ik) go to 1250
          endif
        endif
        go to 1300
      endif
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtOpposite.or.CheckNumber.eq.nButtClone)
     1    then
          il=min((EdwLastActive-1)/2+iz,NFaces(KRefBlock))
          NFaces(KRefBlock)=NFaces(KRefBlock)+1
          call CopyVek(DFace(1,il,KRefBlock),
     1                 DFace(1,NFaces(KRefBlock),KRefBlock),4)
          if(CheckNumber.eq.nButtOpposite) then
            do i=1,3
              DFace(i,NFaces(KRefBlock),KRefBlock)=
     1                      -DFace(i,NFaces(KRefBlock),KRefBlock)
            enddo
          endif
          if(ik.ge.NFaces(KRefBlock)) go to 1550
          j=((NFaces(KRefBlock)-1)/10+1)*10-ik
          go to 1500
        else if(CheckNumber.eq.nButtDelete) then
          il=(EdwLastActive-1)/2+iz
          do i=il+1,NFaces(KRefBlock)
            call CopyVek(DFace(1,i,KRefBlock),DFace(1,i-1,KRefBlock),4)
          enddo
          NFaces(KRefBlock)=NFaces(KRefBlock)-1
          EventType=EventEdw
          EventNumber=EdwLastActive
        else if(CheckNumber.eq.nButtOriginal) then
          NFaces(KRefBlock)=NFacesR
          call CopyVek(DFaceR,DFace(1,1,KRefBlock),4*NFaces(KRefBlock))
          call FeQuestButtonOff(nButtOriginal)
          ik=0
          go to 1200
        else if(CheckNumber.eq.nButtReadFile) then
          idp=NextQuestId()
          xqdp=300.
          call FeQuestCreate(idp,-1.,-1.,xqdp,1,
     1                       'Read faces from file',0,LightGray,0,0)
          t80='%File:'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(t80)+10.
          p80='%Browse'
          dpomb=FeTxLengthUnder(t80)+20.
          dpom=xqdp-xpom-dpomb-15.
          call FeQuestEdwMake(idp,tpom,1,xpom,1,t80,'L',dpom,EdwYd,0)
          nEdwName=EdwLastMade
          call FeQuestStringEdwOpen(nEdwName,FileName)
          xpom=xpom+dpom+10.
          call FeQuestButtonMake(idp,xpom,1,dpomb,ButYd,p80)
          nButtBrowse=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1400      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1      then
            call FeFileManager('Select file with bounding planes',
     1                         FileName,'*',0,.true.,ich)
            if(ich.le.0.and.idel(FileName).gt.0)
     1        call FeQuestStringEdwOpen(nEdwName,FileName)
            go to 1400
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1400
          endif
          if(ich.eq.0) FileName=EdwStringQuest(nEdwName)
          call FeQuestRemove(idp)
          if(ich.ne.0.or.idel(FileName).le.0) go to 1300
          call CheckEOLOnFile(FileName,2)
          if(ErrFlag.ne.0) go to 1300
          ln=NextLogicNumber()
          call OpenFile(ln,FileName,'formatted','old')
          if(ErrFlag.ne.0) go to 1300
          Labeled=0
          TextFile=.false.
1420      read(ln,FormA80,end=1425) t80
          p80=t80
          call velka(p80)
          call mala(t80)
          if(p80.ne.t80) TextFile=.true.
          k=0
          call kus(t80,k,Cislo)
          if(Cislo.eq.'shape') then
            call kus(t80,k,Cislo)
            Labeled=1
          else
            Labeled=0
          endif
          if(Cislo.eq.'face') then
            Labeled=Labeled+1
            go to 1425
          else
            go to 1420
          endif
1425      NFaces(KRefBlock)=0
          rewind ln
          if(Labeled.eq.0.and.TextFile) then
            call FeChybne(-1.,-1.,'the file doesn''t contain any face.',
     1                    ' ',Warning)
            go to 1450
          endif
1430      read(ln,FormA80,end=1440) t80
          if(idel(t80).le.0) go to 1430
          call mala(t80)
          k=0
          if(Labeled.ne.0) then
            call kus(t80,k,Cislo)
            if(Labeled.eq.2) then
              if(Cislo.ne.'shape') go to 1430
              call kus(t80,k,Cislo)
            endif
            if(Cislo.ne.'face') go to 1430
          endif
          if(NFaces(KRefBlock).ge.mxface) then
            write(Cislo,'(i5)') mxface
            call zhusti(Cislo)
            t80='number of faces exceeds the limit'
            p80='Only first '//Cislo(:idel(Cislo))//' faces read in'
            call FeChybne(-1.,-1.,t80,p80,Warning)
            go to 1440
          endif
          NFaces(KRefBlock)=NFaces(KRefBlock)+1
          call StToReal(t80,k,DFace(1,NFaces(KRefBlock),KRefBlock),4,
     1                  .false.,ich)
          if(ich.ne.0) then
            go to 1450
          else
            do i=1,3
              DFace(i,NFaces(KRefBlock),KRefBlock)=
     1          anint(DFace(i,NFaces(KRefBlock),KRefBlock)*1000.)/1000.
            enddo
            DFace(4,NFaces(KRefBlock),KRefBlock)=
     1        anint(DFace(4,NFaces(KRefBlock),KRefBlock)*10000.)/10000.
            go to 1430
          endif
1440      call CloseIfOpened(ln)
          ik=0
          go to 1200
1450      call FeReadError(ln)
          NFaces(KRefBlock)=0
          go to 1300
        else if(CheckNumber.eq.nButtTransform) then
          call UnitMat(rmp,3)
          call FeReadRealMat(-1.,-1.,'Transformation matrix',Indices,
     1                       IdAddPrime,Indices,rmp,rmpi,3,CheckSingYes,
     2                       CheckPosDefNo,ich)
          if(ich.eq.0) then
            do i=1,NFaces(KRefBlock)
              call CopyVek(DFace(1,i,KRefBlock),rmpi,3)
              call Multm(rmpi,rmp,DFace(1,i,KRefBlock),1,3,3)
            enddo
          endif
          go to 1550
        else if(CheckNumber.eq.nButtShow) then
          call MorfCryst(0)
        else if(CheckNumber.eq.nButtRunXShape) then
          call RunXShape
        else if(CheckNumber.eq.nButtPrevious.or.
     1          CheckNumber.eq.nButtNext) then
          if(CheckNumber.eq.nButtNext) then
            j= 10
          else
            j=-10
          endif
          call FeMouseShape(3)
          go to 1500
        endif
        go to 1205
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1300
      endif
      go to 1600
1500  nEdw=nEdwFirst
      do i=iz,ik
        write(t80,100) i+j,nty(i+j)
        call FeQuestEdwLabelChange(nEdw,t80)
        nEdw=nEdw+2
      enddo
      iz=iz+j
      ik=ik+j
1550  EventType=EventEdw
      EventNumber=max(2*min(NFaces(KRefBlock)-iz+1,10)-1,1)
      go to 1205
1600  call FeQuestRemove(id)
      if(ich.ne.0) then
        NFaces(KRefBlock)=NFacesOld
        call CopyVek(DFaceOld,DFace(1,1,KRefBlock),4*NFaces(KRefBlock))
      else
        if(NFaces(KRefBlock).ne.NFacesOld) then
          Change=.true.
        else
          do i=1,NFaces(KRefBlock)
            if(.not.eqrv(DFace(1,i,KRefBlock),DFaceOld(1,i),3,.5).or.
     1         abs(DFace(4,i,KRefBlock)-DFaceOld(4,i)).gt..0001) then
              Change=.true.
              go to 2000
            endif
          enddo
        endif
      endif
2000  WizardMode=WizardModeOld
9999  return
100   format(i2,a2,' face')
      end
