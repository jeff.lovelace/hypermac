      subroutine DRBruker
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeMenu,WhatToDo
      character*30 Men(3)
      data men/'Indexation procedure',
     1         'Cell refinement',
     2         'Find reflection in *.mul'/
1100  WhatToDo=FeMenu(-1.,-1.,men,1,3,1,0)
      if(WhatToDo.eq.1) then
        call DRKumaIndex(2)
      else if(WhatToDo.eq.2) then
        call DRKumaRefine(2)
      else if(WhatToDo.eq.3) then
        call DRBrukerMul
      endif
      if(WhatToDo.gt.0) go to 1100
      return
      end
