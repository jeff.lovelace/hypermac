      subroutine DROsnovaGusMumme
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xx(3),xp(3),xo(3),XAxis(3,2),X1(3),X2(3),XVen(2,0:4),
     1          TrPom(2,2)
      integer Colour,FeRGBCompress
      xx(3)=0.
      do i=0,2
        call SetRealArrayTo(xx,2,0.)
        if(i.ne.0) xx(i)=1.
        call FeXf2X(xx,xo)
        if(i.eq.0) then
          do j=1,2
            do k=1,3
              XAxis(k,j)=-xo(k)
            enddo
          enddo
        else
          call AddVek(XAxis(1,i),xo,XAxis(1,i),3)
        endif
      enddo
c      go to 444
      do i=1,2
        j=3-i
        do izn=1,-1,-2
          n=0
1200      call SetRealArrayTo(xx,2,0.)
          xx(i)=n*izn
          call FeXf2X(xx,xo)
          iuz=0
          if(XAxis(1,j).ne.0.) then
            pom=(XMinAcWin-xo(1))/XAxis(1,j)*XAxis(2,j)+xo(2)
            if(pom.ge.YMinAcWin.and.pom.le.YMaxAcWin) then
              iuz=iuz+1
              XVen(1,iuz)=XMinAcWin
              XVen(2,iuz)=pom
            endif
            pom=(XMaxAcWin-xo(1))/XAxis(1,j)*XAxis(2,j)+xo(2)
            if(pom.ge.YMinAcWin.and.pom.le.YMaxAcWin) then
              iuz=iuz+1
              XVen(1,iuz)=XMaxAcWin
              XVen(2,iuz)=pom
            endif
          endif
          if(XAxis(2,j).ne.0..and.iuz.lt.2) then
            pom=(YMinAcWin-xo(2))/XAxis(2,j)*XAxis(1,j)+xo(1)
            if(pom.ge.XMinAcWin.and.pom.le.XMaxAcWin) then
              iuz=iuz+1
              XVen(1,iuz)=pom
              XVen(2,iuz)=YMinAcWin
            endif
            if(iuz.lt.2) then
              pom=(YMaxAcWin-xo(2))/XAxis(2,j)*XAxis(1,j)+xo(1)
              if(pom.ge.XMinAcWin.and.pom.le.XMaxAcWin) then
                iuz=iuz+1
                XVen(1,iuz)=pom
                XVen(2,iuz)=YMaxAcWin
              endif
            endif
          endif
          if(iuz.ge.2) then
            xu(1)=XVen(1,1)
            xu(2)=XVen(1,2)
            yu(1)=XVen(2,1)
            yu(2)=XVen(2,2)
c            if(n.eq.0) then
c              Colour=FeRGBCompress(222,222,222)
              Colour=FeRGBCompress(100,100,100)
c              Colour=White
c            else
c              Colour=FeRGBCompress( 55, 55, 55)
c            endif
            call FePolyline(2,xu,yu,Colour)
            n=n+1
            go to 1200
          endif
        enddo
      enddo
444   xx(1)=0.
      xx(2)=0.
      xx(3)=0.
      call FeXf2X(xx,xo)
      XVen(1,0)=xo(1)
      XVen(2,0)=xo(2)
      xx(1)=0.
      xx(2)=0.
      xx(3)=0.
      call FeXf2X(xx,xo)
      XVen(1,1)=xo(1)
      XVen(2,1)=xo(2)
      xx(1)= 2.
      xx(2)=-2.
      xx(3)= 0.
      call FeXf2X(xx,xo)
      XVen(1,2)=xo(1)
      XVen(2,2)=xo(2)
      xx(1)= 3.
      xx(2)=-1.
      xx(3)= 0.
      call FeXf2X(xx,xo)
      XVen(1,3)=xo(1)
      XVen(2,3)=xo(2)
      xx(1)=1.
      xx(2)=1.
      xx(3)=0.
      call FeXf2X(xx,xo)
      XVen(1,4)=xo(1)
      XVen(2,4)=xo(2)
      angle=-60.
      do i=1,3
        angle=angle+120.
c        if(i.ne.3) cycle
        if(i.eq.1) then
c          Colour=Yellow
          Colour=Magenta
        else if(i.eq.2) then
          Colour=Red
        else
          Colour=Green
        endif
c        Colour=Yellow
c        if(i.ne.3) then
c          angle=angle+120.
c          cycle
c        endif
        cosp=cos(angle*ToRad)
        sinp=sin(angle*ToRad)
        TrPom(1,1)= cosp
        TrPom(1,2)=-sinp
        TrPom(2,1)= sinp
        TrPom(2,2)= cosp
        i1=0
        do k=1,4
          i1=mod(i1,4)+1
          i2=mod(i1,4)+1
          do j=1,2
            xx(j)=XVen(j,i1)-XVen(j,0)
            xo(j)=XVen(j,0)
          enddo
          call CultM(TrPom,xx,xo,2,2,1)
          xu(1)=xo(1)
          yu(1)=xo(2)
          do j=1,2
            xx(j)=XVen(j,i2)-XVen(j,0)
            xo(j)=XVen(j,0)
          enddo
          call CultM(TrPom,xx,xo,2,2,1)
          xu(2)=xo(1)
          yu(2)=xo(2)
          call FePolyline(2,xu,yu,Colour)
        enddo
        X1(1)= 2.
        X1(2)=-2.
        X2(1)= 1.
        X2(2)= 1.
        xx(3)=0.
        do ix=-10,10
          do iy=-10,10
            xx(1)=float(ix)*X1(1)+float(iy)*X2(1)
            xx(2)=float(ix)*X1(2)+float(iy)*X2(2)
            call FeXf2X(xx,xp)
            do j=1,2
              xp(j)=xp(j)-XVen(j,0)
              xo(j)=XVen(j,0)
            enddo
            call CultM(TrPom,xp,xo,2,2,1)
            if(xo(1)-2..lt.XMinAcWin.or.
     1         xo(1)+2..gt.XMaxAcWin.or.
     2         xo(2)-2..lt.YMinAcWin.or.
     3         xo(2)+2..gt.YMaxAcWin) cycle
            call FeCircle(xo(1),xo(2),3.,Colour)
          enddo
        enddo
      enddo
      return
      end
