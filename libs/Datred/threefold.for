      subroutine ThreeFold
      include 'fepc.cmn'
      include 'basic.cmn'
      character*12 :: Jmena(3) = (/'%Quit ','%Save ','%Print'/)
      dimension xx(3),xxg(3),yy(3),yyg(3),zz(3),zzg(3)
      Tiskne=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,
     1                      -1,-1)
      XStripWidth=100.
      call FeMakeGrWin(2.,XStripWidth,YBottomMargin,2.)
      call FeFillRectangle(XMaxBasWin-XStripWidth,XmaxBasWin,YMinGrWin,
     1                     YMaxGrWin,4,0,0,LightGray)
      call FeMakeAcWin(0.,0.,0.,0.)
      call FeBottomInfo('#prazdno#')
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      dpom=ButYd+8.
      ypom=YMaxGrWin-dpom
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Jmena(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtSave=ButtonLastMade
        else if(i.eq.3) then
          nButtPrint=ButtonLastMade
        endif
        ypom=ypom-dpom
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      xx=0.
      yy=0.
      zz=0.
      xx(1)=1.
      yy(2)=1.
      zz(3)=1.
      CellPar(1,1,1)=1.
      CellPar(2,1,1)=1.
      CellPar(3,1,1)=1.
      CellPar(4,1,1)=90.
      CellPar(5,1,1)=90.
      CellPar(6,1,1)=120.
      call SetMet(0)
      call Multm(MetTensI(1,1,KPhase),xx,xxg,3,3,1)
      call Multm(MetTensI(1,1,KPhase),yy,yyg,3,3,1)
      call Multm(MetTensI(1,1,KPhase),zz,zzg,3,3,1)
      call MatInv(SimTr,SimTrI,pom,3)
      call SetRealArrayTo(F2O,9,0.)
      aa=sqrt(ScalMul(xx,xxg))
      bb=sqrt(ScalMul(yy,yyg))
      cc=sqrt(ScalMul(zz,zzg))
      csa=ScalMul(yy,zzg)/(bb*cc)
      csb=ScalMul(xx,zzg)/(aa*cc)
      csg=ScalMul(xx,yyg)/(aa*bb)
      snb=sqrt(1.-csb**2)
      sng=sqrt(1.-csg**2)
      pom=(csa-csb*csg)/sng
      F2O(1)=aa
      F2O(4)=bb*csg
      F2O(5)=bb*sng
      F2O(7)=cc*csb
      F2O(8)=cc*pom
      F2O(9)=cc*snb*sqrt(1.-(pom/snb)**2)
      call matinv(F2O,O2F,pom,3)
      call FeSetTransXo2X(-5.,5.,-5.,5.,.true.)
2500  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call DROsnovaThreeFold
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
3000  call FeQuestEvent(id,ich)
      if((CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape).or.
     1   (CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)) then
        go to 9900
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPrint) then
        call FePrintPicture(ich)
        if(ich.eq.0) then
          go to 2500
        else
          HardCopy=0
          go to 3000
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSave) then
        call FeSavePicture('figure',6,1)
        if(HardCopy.gt.0) then
          go to 2500
        else
          HardCopy=0
          go to 3000
        endif
      endif
9900  call FeQuestRemove(id)
      call iom50(0,0,fln(:ifln)//'.m50')
9999  return
      end
