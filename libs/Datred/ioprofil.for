      subroutine IOProfil(nref,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      logical eqiv,Change
      rewind 95
      do i=1,nref
        call DRGetReflectionFromM95(95,iend,ich)
        if(iend.ne.0.or.ich.ne.0) go to 9999
      enddo
      do i=1,NNew
        if(NRefNew(i).eq.NRef) then
          call CopyVekI(IProfNew(1,i),IProf(1,2),NProf)
          go to 1060
        endif
      enddo
1060  if(klic.eq.0) then
        hh1=ih(1)
        hh2=ih(2)
        hh3=ih(3)
        sinthl=sqrt(hh1**2*prcp(1,1,KPhase)+hh2**2*prcp(2,1,KPhase)+
     1              hh3**2*prcp(3,1,KPhase)+
     2              2.*(hh1*hh2*prcp(4,1,KPhase)+
     3                  hh1*hh3*prcp(5,1,KPhase)+
     4                  hh2*hh3*prcp(6,1,KPhase)))
        thi=asin(sinthl*LamAveRefBlock(KRefBlock))/ToRad
        do i=1,nprof
          py(i,1)=IProf(i,1)
          py(i,2)=IProf(i,2)
        enddo
      else
        Change=.false.
        do i=1,nprof
          j=IProf(i,2)
          IProf(i,2)=nint(py(i,2))
          Change=Change.or.j.ne.IProf(i,2)
        enddo
        if(eqiv(IProf(1,1),IProf(1,2),NProf)) then
          if(KProf.eq.2) KProf=1
        else
          if(KProf.eq.1) KProf=2
        endif
2200    if(Change) then
          do i=1,NNew
            if(NRefNew(i).eq.NRef) go to 2250
          enddo
          NNew=NNew+1
          i=NNew
2250      call CopyVekI(IProf(1,2),IProfNew(1,i),NProf)
          NRefNew(i)=NRef
        endif
      endif
9999  return
      end
