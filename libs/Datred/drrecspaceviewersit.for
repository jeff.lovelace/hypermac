      subroutine DRRecSpaceViewerSit
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 :: Form85='(.i5,5f15.4,3i4)'
      dimension xx(3),xxt(3),xp(3),xo(3)
      integer FeRGBCompress
      logical EqIV0
      write(Form85(2:2),'(i1)') NDim(KPhase)
      SimNDr=0
      do i=1,SimIz
        ntw=0
        do itw=1,NTwin
          if(HCondAr(itw,i).gt.0) then
            ntw=itw
            exit
          endif
        enddo
        if(ntw.eq.0) cycle
        call IndUnpack(HCondAr(ntw,i),ih,HCondLn,HCondMx,NDim(KPhase))
        if(NDimI(KPhase).gt.0) then
          if(.not.SimDrawSat.and.
     1       .not.EqIV0(ih(4),NDimI(KPhase))) cycle
        endif
        do j=1,3
          xx(j)=ih(j)
          if(.not.SimProjectSat) then
            do k=1,NDimI(KPhase)
              xx(j)=xx(j)+float(ih(k+3))*qu(j,k,1,KPhase)
            enddo
          endif
        enddo
        call multm(xx,SimTrI,xp,1,3,3)
        if(abs(xp(3)-SimLayer).gt.SimDepth+.0001) cycle
        SimNDr=SimNDr+1
        if(SimNDr.gt.NDrAlloc) call ReallocDrPoints(1000)
        call CopyVekI(ih,HDr(1,SimNDr),NDim(KPhase))
        HDrTw(SimNDr)=ntw
        PorDr(SimNDr)=0
        ColDr(SimNDr)=0
        XDr(SimNDr)=0.
        IXDr(SimNDr)=0
        YDr(SimNDr)=0.
        RadDr(SimNDr)=0.
        RIDr(SimNDr)=riar(i)
        RSDr(SimNDr)=rsar(i)
      enddo
      xp(1)=0.
      xp(2)=0.
      xp(3)=SimLayer
      call FeXf2X(xp,xo)
      SimDrLayer=xo(3)
      RadDrMax=0.
      pomp=0.
      do i=1,SimNDr
        do j=1,3
          xx(j)=HDr(j,i)
          do k=1,NDimI(KPhase)
            xx(j)=xx(j)+float(HDr(k+3,i))*qu(j,k,1,KPhase)
          enddo
        enddo
        if(HDrTw(i).ne.1) then
          if(.not.SimDrawTwins) cycle
          xxt(1:3)=xx(1:3)
          call MultM(xxt,RTwI(1,HDrTw(i)),xx,1,3,3)
        endif
        call multm(xx,SimTrI,xp,1,3,3)
        call FeXf2X(xp,xo)
        if(SimType.ne.SimDiffFSig) then
          ri=sqrt(abs(RIDr(i)))
        else
          ri=RIDr(i)*.001
        endif
        XDr(i)=xo(1)
        IXDr(i)=nint(xo(1)*1000.)
        YDr(i)=xo(2)
        if(RIDr(i).gt.3.*RSDr(i).or.SimType.eq.SimDiffFSig) then
          pom=min(100.*abs(ri)/(SimValMax*SimValRed),1.)
          if(SimUseScaledRad) then
            RadDr(i)=SimRadMax*pom
          else
            RadDr(i)=SimRadMax
          endif
          RadDr(i)=max(RadDr(i),SimRadMin)
          RadDrMax=max(RadDrMax,RadDr(i))
          if(.not.SimUseShading) pom=1.
          if(SimType.eq.SimDiffFSig) then
            if(ri.gt.0.) then
              j=SimColorPlus
            else
              j=SimColorMinus
            endif
          else
            if(NTwin.gt.1.and.SimDrawTwins) then
              j=SimColorTwin(HDrTw(i))
            else
              if(NDimI(KPhase).gt.0) then
                ic=0
                do k=1,NComp(KPhase)
                  call MultMIRI(HDr(1,i),zvi(1,k,KPhase),ih,1,
     1                          NDim(KPhase),NDim(KPhase))
                  if(EqIV0(ih(4),NDimI(KPhase))) then
                    ic=k
                    exit
                  endif
                enddo
                if(ic.eq.0) then
                  j=SimColorSat
                else
                  j=SimColorMain(ic)
                endif
              else
                j=SimColorMain(1)
              endif
            endif
          endif
          call FeRGBUncompress(j,IRed,IGreen,IBlue)
          ColDr(i)=FeRGBCompress(nint(float(IRed)*pom),
     1                           nint(float(IGreen)*pom),
     2                           nint(float(IBlue)*pom))
        else
          if(NTwin.gt.1.and.SimDrawTwins) then
            j=SimColorTwin(HDrTw(i))
            call FeRGBUncompress(j,IRed,IGreen,IBlue)
            ColDr(i)=FeRGBCompress(nint(float(IRed)),
     1                             nint(float(IGreen)),
     2                             nint(float(IBlue)))
          else
            ColDr(i)=SimColorUnobs
          endif
          if(SimDrawUnobs) then
            if(SimUseScaledRad) then
              RadDr(i)=SimRadMin
            else
              RadDr(i)=SimRadMax
            endif
          else
            RadDr(i)=0.
          endif
        endif
      enddo
      call indexx(SimNDr,IXDr,PorDr)
      n=0
      do i=1,SimNDr
        if(XDr(i)-RadDr(i).lt.XMinAcWin.or.
     1     XDr(i)+RadDr(i).gt.XMaxAcWin.or.
     2     YDr(i)-RadDr(i).lt.YMinAcWin.or.
     3     YDr(i)+RadDr(i).gt.YMaxAcWin.or.RadDr(i).eq.0.) cycle
        if(HardCopy.eq.HardCopyNum) then
          call FeRGBUncompress(ColDr(i),IRed,IGreen,IBlue)
          write(85,Form85) HDr(1:NDim(KPhase),i),RIDr(i),RSDr(i),
     1      XDr(i),YDr(i),RadDr(i),IRed,IGreen,IBlue
        endif
        call FeCircle(XDr(i),YDr(i),RadDr(i),ColDr(i))
      enddo
9999  return
      end
