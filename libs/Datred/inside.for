      logical function inside(xyz,sum)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xyz(3),sum(*),xp(3)
      inside=.true.
      if(xyz(1).eq.0..and.xyz(2).eq.0..and.xyz(3).eq.0.) go to 9999
      call multm(MetTens(1,1,KPhase),xyz,xp,3,3,1)
      r=sqrt(scalmul(xyz,xp))
      do 1200j=1,NFacesR
        chck=scalmul(dfaceR(1,j),xyz)
        cfi=chck/(sum(j)*r)
        if(cfi.le.0.) go to 1200
!        if(dface4(j,KRefBlock)-chck+0.000001.lt.0.) then
        if(dface4R(j)-chck+0.000001.lt.0.) then
          inside=.false.
          go to 9999
        endif
1200  continue
9999  return
      end
