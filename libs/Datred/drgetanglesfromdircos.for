      subroutine DRGetAnglesFromDircos
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension rot(3,3),tt(3,3)
      do i=1,3
        sod(i)=dircos(i,1)*DRRcp(i)
        sd(i) =dircos(i,2)*DRRcp(i)
      enddo
      snp=sin(2.*uhly(4)*torad*SenseOfAngle(4))
      csp=sqrt(1.-snp**2)
      call multm(sod,UBi(1,1,KRefBlock),tt(1,1),1,3,3)
      call multm(sd ,UBi(1,1,KRefBlock),tt(1,2),1,3,3)
      call VecOrtNorm(tt(1,1),3)
      call VecOrtNorm(tt(1,2),3)
      rot(1,1)=-tt(1,1)
      rot(1,2)=-tt(2,1)
      rot(1,3)=-tt(3,1)
      rot(2,1)=(-tt(1,2)+csp*tt(1,1))/snp
      rot(2,2)=(-tt(2,2)+csp*tt(2,1))/snp
      rot(2,3)=(-tt(3,2)+csp*tt(3,1))/snp
      rot(3,1)=rot(1,2)*rot(2,3)-rot(1,3)*rot(2,2)
      rot(3,2)=rot(1,3)*rot(2,1)-rot(1,1)*rot(2,3)
      rot(3,3)=rot(1,1)*rot(2,2)-rot(1,2)*rot(2,1)
      call EM40GetAngles(rot,0,uhly)
      pom=uhly(1)
      uhly(1)=uhly(3)
      uhly(3)=pom
      do i=1,3
        uhly(i)=uhly(i)*SenseOfAngle(i)
1100    if(uhly(i).gt.180.) then
          uhly(i)=uhly(i)-360.
          go to 1100
        endif
1200    if(uhly(i).lt.-180.) then
          uhly(i)=uhly(i)+360.
          go to 1200
        endif
      enddo
      if(abs(uhly(3)).gt.90.) then
        if(uhly(1).gt.0) then
          uhly(1)=uhly(1)-180.
        else
          uhly(1)=uhly(1)+180.
        endif
        uhly(3)=uhly(3)-sign(1.,uhly(3))*180.
        uhly(2)=-uhly(2)
      endif
      return
      end
