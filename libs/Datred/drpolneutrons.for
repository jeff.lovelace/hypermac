      subroutine DRPolNeutrons
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Radka
      character*80 Veta
      logical EqIgCase
      real xp(3)
      FormatRefBlock(KRefBlock)='(*)'
      IStav=0
1000  read(71,FormA,end=9000) Radka
      if(Radka(1:1).ne.'#') then
        backspace 71
        go to 9999
      endif
      k=1
      call kus(Radka,k,Veta)
      if(EqIgCase(Veta,'Wavelength')) then
        call StToReal(Radka,k,LamAveRefBlock(KRefBlock),1,.false.,ich)
        if(ich.ne.0) go to 9100
        IStav=IStav+1
      else if(EqIgCase(Veta,'Orientation')) then
        do i=1,3
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9100
          UB(i,1:3,KRefBlock)=xp(1:3)
        enddo
        IStav=IStav+10
      else if(EqIgCase(Veta,'Polarization')) then
        MagPolFlagRefBlock(KRefBlock)=1
        call StToReal(Radka,k,MagFieldDirRefBlock(1,KRefBlock),3,
     1                .false.,ich)
        if(ich.ne.0) go to 9100
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 9100
        MagPolPlusRefBlock(KRefBlock)=xp(1)
        MagPolMinusRefBlock(KRefBlock)=xp(2)
        IStav=IStav+100
      endif
      go to 1000
9000  call FeChybne(-1.,-1.,'Header of the file is not complete',
     1              'File probably corrupted.',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  ErrFlag=1
9999  if(ErrFlag.eq.0.and.IStav.eq.111) then
        call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),pom,3)
        call DRCellFromUB
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
        AngleMonRefBlock(KRefBlock)=0.
        AlphaGMonRefBlock(KRefBlock)=0.
        BetaGMonRefBlock(KRefBlock)=0.
        PocitatDirCos=.false.
        DirCosFromPsi=.false.
        FormatRefBlock(KRefBlock)='(3i4,f10.5,f9.5)'
        write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim95(KRefBlock)
      endif
      return
      end
