      subroutine DRBasicSXD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 p80,t80
      dimension h(6),ip(4)
      logical EqRV,EqRVM
      m=0
      p80='phi, chi, omega'
      idl=idel(p80)
      QuMagUsed=.false.
      NQuSXD=0
      PocitatDirCos=.false.
1700  read(71,FormA80,err=9000,end=9000) t80
      call mala(t80)
      i=index(t80,p80(:idl))
      if(i.gt.0) then
        k=i+idl
        call StToReal(t80,k,AnglesSXD,3,.false.,ich)
        if(ich.ne.0) go to 1700
        SenseOfAngle(1)=1.
        SenseOfAngle(2)=1.
        SenseOfAngle(3)=1.
        m=m+1
      else if(index(t80,'lattice parameters').gt.0) then
        if(.not.PocitatDirCos) then
          k=0
          do i=1,3
            call kus(t80,k,p80)
          enddo
          call StToReal(t80,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
          if(ich.ne.0) go to 1700
        endif
      else if(index(t80,'ub and abc matrix').gt.0) then
        do i=1,3
          if(i.eq.1) then
            j=2
          else if(i.eq.2) then
            j=1
          else
            j=3
          endif
          read(71,FormA80,err=9000,end=9000) t80
          k=1
          call StToReal(t80,k,h,6,.false.,ich)
          if(ich.ne.0) go to 1700
          if(i.ne.3) call RealVectorToOpposite(h(4),h(4),3)
          call CopyVek(h(4),ubi(1,j,KRefBlock),3)
        enddo
        call MatInv(ubi(1,1,KRefBlock),ub(1,1,KRefBlock),pom,3)
        call DRCellFromUB
        PocitatDirCos=.true.
        m=m+10
      else if(t80(1:1).eq.'(') then
        i=index(t80,'4f10.4')
        if(i.gt.0) t80(i:i)='9'
        FormatRefBlock(KRefBlock)=t80
      else if(index(t80,'magnetic propagation vectors').gt.0.or.
     1        index(t80,'structural propagation vectors').gt.0) then
        QuMagUsed=index(t80,'magnetic propagation vectors').gt.0
        read(71,FormA80,err=9000,end=9000) t80
        k=0
        call StToInt(t80,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9000
        NQuSXD=ip(1)
        if(NQuSXD.gt.3) go to 9100
        do i=1,NQuSXD
          read(71,FormA80,err=9000,end=9000) t80
          k=0
          call StToInt(t80,k,ip,1,.false.,ich)
          j=ip(1)
          call StToReal(t80,k,QuSXD(1,j),3,.false.,ich)
          if(ich.ne.0) go to 9000
        enddo
      else if(index(t80,'linear combination table').gt.0) then
        read(71,FormA80,err=9000,end=9000) t80
        k=0
        call StToInt(t80,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9000
        NCombSXD=ip(1)
        if(NCombSXD.le.3) then
          do i=1,NCombSXD
            read(71,FormA80,err=9000,end=9000) t80
            k=0
            call StToInt(t80,k,ip,NQuSXD+1,.false.,ich)
            if(ich.ne.0) go to 9000
            call CopyVekI(ip(2),CombSXD(1,ip(1)),NQuSXD)
          enddo
        endif
      else if(index(t80,'h   k   l').gt.0) then
        go to 2000
      else if(index(t80,'h    k    l').gt.0) then
        go to 2000
      else if(index(t80,'longitude').gt.0) then
        go to 2000
      endif
      go to 1700
2000  RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      DirCosFromPsi=.false.
      LamAveRefBlock(KRefBlock)=-1.
      if(.not.QuMagUsed) then
        NCombSXD=NQuSXD
        call SetIntArrayTo(CombSXD,9,0)
        do i=1,NQuSXD
          CombSXD(i,i)=1
        enddo
      endif
      n=0
      do 3000i=1,NQuSXD
        do j=1,n
          do k=1,5
            do l=1,3
              h(l)=QuRefBlock(l,j,KRefBlock)*float(k)
            enddo
            if(EqRV (h,QuSXD(1,i),3,.0001).or.
     1         EqRVM(h,QuSXD(1,i),3,.0001)) go to 3000
            do l=1,3
              h(l)=QuRefBlock(l,j,KRefBlock)/float(k)
            enddo
            if(EqRV (h,QuSXD(1,i),3,.0001).or.
     1         EqRVM(h,QuSXD(1,i),3,.0001)) go to 3000
          enddo
        enddo
        n=n+1
        call CopyVek(QuSXD(1,i),QuRefBlock(1,j,KRefBlock),3)
3000  continue
      NDim95(KRefBlock)=n+3
      if(NDim95(KRefBlock).gt.3) then
        if(QuMagUsed) then
          NDimSXD=4
        else
          NDimSXD=NQuSXD+3
        endif
      else
        NDimSXD=3
      endif
      go to 9999
9000  call FeReadError(71)
      go to 9900
9100  t80='the number of propagation vectors >3.'
      go to 9800
9200  t80='the number of Q combinations >3.'
      go to 9800
9800  call FeChybne(-1.,-1.,t80,' ',SeriousError)
9900  ErrFlag=1
9999  return
      end
