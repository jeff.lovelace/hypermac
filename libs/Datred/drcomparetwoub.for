      subroutine DRCompareTwoUB
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension CellUB(6,2),QuUB(3,2),UBAct(3,3,2),TransMat(36,2),
     1          nLblCell(2),nLblModVec(2),nLblTitleUB(2),nLblUB(2),
     2          nLblTitleCell(2),nLblTitleModVec(2),nLblTitleTrans(2),
     3          nLblTrans(2),tw(3,3),twi(3,3),xp(3),nButtTrans(2),
     4          UBInic(3,3,2),QuInic(3,2),AMat(36),BMat(36),TrPom(3,3),
     5          nButtQuTrans(2),nButtInic(2),QuPom(3),nLblTitleQToQ(2),
     6          nLblQToQ(2)
      character*256 t256
      character*80 Veta
      integer UseTabsIn,UseTabsCell,UseTabsMat1,UseTabsMat2,UseTabsMat3
      logical EqRV0,UzJiMame(2),MatRealEqUnitMat,EqIgCase
      NDimIn=NDim(KPhase)
      NDim(KPhase)=3
      KRefBlockIn=KRefBlock
      KRefBlock=1
      UseTabsIn=UseTabs
      call FeTabsReset(UseTabs)
      t256=' '
      ich=0
      id=NextQuestId()
      xqd=650.
      il=18
      Veta='Compare two orientation matrices'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,-1)
      xpom=10.
      Veta='Read in orientation matrix #%1'
      idl=idel(Veta)
      dpom=FeTxLengthUnder(Veta)+20.
      tpom=xpom+dpom+10.
      il=1
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) then
          Veta(idl:idl)='2'
          nButtReadUB1=ButtonLastMade
          nLblFileUB1=LblLastMade
        else
          nButtReadUB2=ButtonLastMade
          nLblFileUB2=LblLastMade
        endif
        il=il+1
      enddo
      call FeQuestLinkaMake(id,il)
      tpom=5.
      tpom1=tpom+100.
      tpom2=tpom1+80.
      tpom3=tpom1+50.
      tpom4=tpom1+250.
      tpom5=tpom1+330.
      UseTabsCell=NextTabs()
      xpom=15.
      do i=1,6
        call FeTabsAdd(xpom,UseTabsCell,IdRightTab,' ')
        xpom=xpom+50.
      enddo
      il=-il*10
      UseTabsMat1=NextTabs()
      xpom=15.
      do i=1,3
        call FeTabsAdd(xpom,UseTabsMat1,IdCharTab,'.')
        xpom=xpom+55.
      enddo
      UseTabsMat2=NextTabs()
      xpom=15.
      do i=1,4
        call FeTabsAdd(xpom,UseTabsMat2,IdCharTab,'.')
        xpom=xpom+40.
      enddo
      UseTabsMat3=NextTabs()
      xpom=15.
      do i=1,7
        if(i.eq.4) then
          call FeTabsAdd(xpom,UseTabsMat3,IdRightTab,' ')
        else
          call FeTabsAdd(xpom,UseTabsMat3,IdCharTab,'.')
        endif
        xpom=xpom+40.
      enddo
      do i=1,2
        il=il-10
        UseTabs=UseTabsCell
        Veta='Data set#'
        write(Cislo,'(i1)') i
        Veta=Veta(:idel(Veta))//Cislo(1:1)
        call FeQuestLblMake(id,tpom,il,Veta,'L','B')
        call FeQuestLblMake(id,tpom1,il,'Cell parameters:','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblTitleCell(i)=LblLastMade
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblCell(i)=LblLastMade
        il=il-6
        call FeQuestLblMake(id,tpom1,il,'Modulation vector:','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblTitleModVec(i)=LblLastMade
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblModVec(i)=LblLastMade
        ilp=il
        UseTabs=UseTabsMat1
        do j=1,3
          il=il-6
          if(j.eq.1) then
            call FeQuestLblMake(id,tpom1,il,'UB matrix:','L','N')
            call FeQuestLblOff(LblLastMade)
            nLblTitleUB(i)=LblLastMade
          endif
          call FeQuestLblMake(id,tpom3,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          if(j.eq.1) nLblUB(i)=LblLastMade
        enddo
        UseTabs=UseTabsMat2
        il=ilp
        do j=1,4
          il=il-6
          if(j.eq.1) then
            call FeQuestLblMake(id,tpom4,il,'Transformation:','L','N')
            call FeQuestLblOff(LblLastMade)
            nLblTitleTrans(i)=LblLastMade
          endif
          call FeQuestLblMake(id,tpom5,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          if(j.eq.1) nLblTrans(i)=LblLastMade
        enddo
      enddo
      il=il-10
      call FeQuestLinkaMake(id,il)
      il=il-10
      tpom1=5.
      tpom2=tpom1+100.
      tpom3=xqd*.5+5.
      tpom4=tpom3+120.
      UseTabs=UseTabsMat1
      do j=1,3
        if(j.eq.1) then
          call FeQuestLblMake(id,tpom1,il,'Twin matrix:','L','B')
          call FeQuestLblOff(LblLastMade)
          nLblTitleTw=LblLastMade
          call FeQuestLblMake(id,tpom3,il,'Inverse twin matrix:','L',
     1                        'B')
          call FeQuestLblOff(LblLastMade)
          nLblTitleTwi=LblLastMade
        endif
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(j.eq.1) nLblTw=LblLastMade
        call FeQuestLblMake(id,tpom4,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(j.eq.1) nLblTwi=LblLastMade
        il=il-6
      enddo
      il=il-4
      UseTabs=UseTabsMat3
      Veta='Modulation vector 1=>2:'
      call FeBoldFont
      tpom2=tpom1+FeTxLength(Veta)+10.
      call FeNormalFont
      do i=1,2
        call FeQuestLblMake(id,tpom1,il,Veta,'L','B')
        call FeQuestLblOff(LblLastMade)
        nLblTitleQToQ(i)=LblLastMade
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblQToQ(i)=LblLastMade
        il=il-6
        Veta='Modulation vector 2=>1:'
      enddo
      il=il-10
      ilp=il
      Veta='Transfrom cell #1'
      idl=idel(Veta)
      dpom=FeTxLength(Veta)+90.
      xpom=.5*xqd-1.5*dpom-20.
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
        if(i.eq.1) Veta(idl:idl)='2'
        nButtTrans(i)=ButtonLastMade
        il=il-10
      enddo
      il=ilp
      xpom=xpom+dpom+20.
      Veta='Transfrom modulation vector #1'
      idl=idel(Veta)
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
        if(i.eq.1) Veta(idl:idl)='2'
        nButtQuTrans(i)=ButtonLastMade
        il=il-10
      enddo
      il=ilp
      xpom=xpom+dpom+20.
      Veta='Back to original #1'
      idl=idel(Veta)
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
        if(i.eq.1) Veta(idl:idl)='2'
        nButtInic(i)=ButtonLastMade
        il=il-10
      enddo
      call SetLogicalArrayTo(UzJiMame,2,.false.)
      go to 1500
1400  call CopyVek(UBAct(1,1,i),ub(1,1,KRefBlock),9)
      call DRCellFromUB
      call CopyVek(CellRefBlock(1,KRefBlock),CellUB(1,i),6)
      call FeQuestLblOn(nLblTitleCell(i))
      write(Veta,100)(Tabulator,CellUB(j,i),j=1,6)
      call Zhusti(Veta)
      call FeQuestLblChange(nLblCell(i),Veta)
      if(.not.EqRV0(QuUB(1,i),3,.00001)) then
        call FeQuestLblOn(nLblTitleModVec(i))
        write(Veta,101)(Tabulator,QuUB(j,i),j=1,3)
        call Zhusti(Veta)
        call FeQuestLblChange(nLblModVec(i),Veta)
      else
        call SetRealArrayTo(QuUB(1,i),3,0.)
      endif
      call FeQuestLblOn(nLblTitleUB(i))
      nLbl=nLblUB(i)
      do j=1,3
        write(Veta,102)(Tabulator,ub(j,k,KRefBlock),k=1,3)
        call Zhusti(Veta)
        call FeQuestLblChange(nLbl,Veta)
        nLbl=nLbl+1
      enddo
      call FeQuestLblOn(nLblTitleTrans(i))
      nLbl=nLblTrans(i)
      do j=1,NDim(KPhase)
        write(Veta,103)
     1    (Tabulator,TransMat(j+(k-1)*NDim(KPhase),i),k=1,NDim(KPhase))
        call Zhusti(Veta)
        call FeQuestLblChange(nLbl,Veta)
        nLbl=nLbl+1
      enddo
      UzJiMame(i)=.true.
      call FeQuestButtonOff(nButtTrans(i))
      if(NDimI(KPhase).eq.1) call FeQuestButtonOff(nButtQuTrans(i))
      if(MatRealEqUnitMat(TransMat(1,i),NDim(KPhase),.001)) then
        call FeQuestButtonDisable(nButtInic(i))
      else
        call FeQuestButtonOff(nButtInic(i))
      endif
      if(UzJiMame(1).and.UzJiMame(2)) then
        call MatInv(UBAct(1,1,2),Tw,pom,3)
        call MultM(Tw,UBAct(1,1,1),Twi,3,3,3)
        call TrMat(Twi,Tw,3,3)
        call MatInv(Tw,Twi,pom,3)
        call FeQuestLblOn(nLblTitleTw)
        nLbl=nLblTw
        do j=1,3
          write(Veta,102)(Tabulator,Tw(j,k),k=1,3)
          call Zhusti(Veta)
          call FeQuestLblChange(nLbl,Veta)
          nLbl=nLbl+1
          write(Veta,102)(Tabulator,Twi(j,k),k=1,3)
          call Zhusti(Veta)
          call FeQuestLblChange(nLbl,Veta)
          nLbl=nLbl+1
        enddo
        call FeQuestLblOn(nLblTitleTwi)
        if(NDimI(KPhase).gt.0) then
          do i=1,2
            if(i.eq.1) then
              call MultM(QuUB(1,i),Tw,xp,1,3,3)
            else
              call MultM(QuUB(1,i),TwI,xp,1,3,3)
            endif
            write(Veta,104)(Tabulator,QuUB(j,i),j=1,3),Tabulator,
     1                     (Tabulator,xp(j),j=1,3)
            call Zhusti(Veta)
            call FeQuestLblOn(nLblTitleQToQ(i))
            call FeQuestLblChange(nLblQToQ(i),Veta)
          enddo
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtReadUB1.or.CheckNumber.eq.nButtReadUB2))
     2  then
        i=CheckNumber-nButtReadUB1+1
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        Veta='Define input "sum" or "mat" file#'//Cislo(1:1)
        call FeFileManager(Veta,t256,'*.sum *.mat',0,.true.,ich)
        if(ich.eq.0) then
          call ExtractFileName(t256,Veta)
          Veta='=> '//Veta(:idel(Veta))
          call FeQuestLblChange(nLblFileUB1+i-1,Veta)
          call GetExtentionOfFileName(t256,Cislo)
          call OpenFile(70,t256,'formatted','unknown')
          if(EqIgCase(Cislo,'sum')) then
            call DRBasicKumaCCDSeparately
            call CloseIfOpened(70)
          else
            do j=1,3
              read(70,*)(ub(j,k,KRefBlock),k=1,3)
            enddo
            read(70,*,end=1520,err=1520)
     1        (QuRefBlock(j,1,KRefBlock),j=1,3)
            go to 1540
1520        call SetRealArrayTo(QuRefBlock(1,1,KRefBlock),3,-333.)
          endif
1540      call CopyVek(ub(1,1,KRefBlock),UBAct(1,1,i),9)
          call CopyVek(ub(1,1,KRefBlock),UBInic(1,1,i),9)
          if(QuRefBlock(1,1,KRefBlock).gt.-300.) then
            call CopyVek(QuRefBlock(1,1,KRefBlock),QuUB(1,i),3)
            NDim(KPhase)=max(NDim(KPhase),4)
          else
            call SetRealArrayTo(QuUB(1,i),3,0.)
            NDim(KPhase)=max(NDim(KPhase),3)
          endif
          call CopyVek(QuUB(1,i),QuInic(1,i),3)
          NDimI(KPhase)=NDim(KPhase)-3
          NDimQ(KPhase)=NDim(KPhase)**2
          call UnitMat(TransMat(1,i),NDim(KPhase))
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtTrans(1).or.
     2         CheckNumber.eq.nButtTrans(2))) then
        i=CheckNumber-nButtTrans(1)+1
        call CopyVek(CellUB(1,i),CellPar(1,1,KPhase),6)
        call DRCellTrMat(TransMat(1,i),ichp)
        if(ichp.eq.0) then
          call MatBlock3(TransMat(1,i),TrPom,NDim(KPhase))
          call MultM(QuInic(1,i),TrPom,QuUB(1,i),1,3,3)
          call TrMat(TransMat(1,i),AMat,NDim(KPhase),NDim(KPhase))
          call MatInv(AMat,BMat,pom,NDim(KPhase))
          call MatBlock3(BMat,TrPom,NDim(KPhase))
          call MultM(UBInic(1,1,i),TrPom,UBAct(1,1,i),3,3,3)
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtQuTrans(1).or.
     2         CheckNumber.eq.nButtQuTrans(2))) then
        i=CheckNumber-nButtQuTrans(1)+1
        call CopyVek(QuUB(1,i),Qu(1,1,1,KPhase),3)
        call DRQVecTrMat(TransMat(1,i),QuPom,ichp)
        if(ichp.eq.0) then
          call CopyVek(QuPom,QuUB(1,i),3)
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInic(1).or.
     2         CheckNumber.eq.nButtInic(2))) then
        i=CheckNumber-nButtInic(1)+1
        call CopyMat(UBInic(1,1,i),UBAct(1,1,i),3)
        if(NDimI(KPhase).gt.0) call CopyVek(QuInic(1,i),QuUB(1,i),3)
        call UnitMat(TransMat(1,i),NDim(KPhase))
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      KRefBlock=KRefBlockIn
      call FeTabsReset(UseTabsCell)
      call FeTabsReset(UseTabsMat1)
      call FeTabsReset(UseTabsMat2)
      call FeTabsReset(UseTabsMat3)
      UseTabs=UseTabsIn
      NDim(KPhase)=NDimIn
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      return
100   format(3(a1,f8.3),3(a1,f8.2))
101   format(3(a1,f8.4))
102   format(3(a1,f10.6))
103   format(6(a1, f7.3))
104   format(3(a1,f8.4),a1,'=>',3(a1,f8.4))
      end
