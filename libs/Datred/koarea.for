      subroutine KOArea(KO,KOv,KappaIn,OmIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer OmIn,Om,x
      Kappa=KappaIn
      Om=OmIn
      if(kappa.gt.0) then
        kappa=-kappa
        om=-om
        is=-1
      else
        is= 1
      endif
      KO=0
      if(kappa.ge.ColimKuma(1)) then
        if(om.gt.RangeOKuma(1)) then
          KOv= 7
        else if(om.lt.-RangeOKuma(1)) then
          KOv=-7
        else
          KOv= 0
        endif
      else
        x=nint(-SlopeKuma*float(kappa)+float(om))
        if(x.le.ColimKuma(4)) then
          KO=-1
          if(kappa.lt.-RangeOKuma(3)) then
            if(om.lt.-RangeOKuma(1)) then
              KOv=-3
            else
              KOv=-4
            endif
          else
            if(om.lt.-RangeOKuma(1)) then
              KOv=-7
            else
              KOv=-1
            endif
          endif
        else
          if(x.gt.ColimKuma(5)) then
            KO=-2
            if(kappa.lt.-RangeOKuma(3)) then
              if(om.gt.RangeOKuma(1)) then
                KOv=-6
              else
                KOv=-5
              endif
            else
              if(om.gt.RangeOKuma(1)) then
                KOv= 7
              else
                KOv=-2
              endif
            endif
          else
            KO=-9
            KOv=15
          endif
        endif
      endif
      KO =is*KO
      KOv=is*KOv
      return
      end
