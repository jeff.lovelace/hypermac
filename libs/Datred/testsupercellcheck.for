      subroutine TestSuperCellCheck
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      common/TestSuperCell/ CellFound(6,10),TrFound(36,10),nSbw,XShow,
     1                      YShow,CrSystemFound(10),NSupCellShown
      real TrPom(9)
      integer CrSystemFound,SbwItemPointerQuest,SbwActiveIn
      save /TestSuperCell/
      NSupCellSel=SbwItemPointerQuest(nSbw)
      if(NSupCellShown.eq.NSupCellSel) go to 9999
      SbwActiveIn=SbwActive
      call CrlMatEqHide
      if(NSupCellSel.le.NSupCell) then
        call MatBlock3(TrFound(1,NSupCellSel),TrPom,NDim(KPhase))
      else
        call UnitMat(TrPom,3)
      endif
      call CrlMatEqShow(XShow,YShow,3,TrPom,0,SmbABCCapital,SmbABC,3)
      NSupCellShown=NSupCellSel
      SbwActive=SbwActiveIn
9999  return
      end
