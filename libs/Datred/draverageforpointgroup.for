      subroutine DRAverageForPointGroup(CrSystemNameP,PointGroup,QuIn,
     1                                  ToOrtho,FromOrtho,QuIrr,QuRac,
     2                                  ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      character*(*) PointGroup,CrSystemNameP
      character*31 t31
      real, allocatable :: RMP(:,:)
      dimension TrPom(9),ToOrtho(9),FromOrtho(9),QuIn(3,3),QuPom(3,3),
     1          pp(3),mpp(3),IIrr(3),pq(3),INul(3),QuRac(3,3),rmp6(36),
     2          QuIrr(3,3),QuRacPom(3,3),RPom(3,3),RPomI(3,3),RPomC(3,3)
      real MetTens6NewI(36),gpp(9),gppi(9),QuNew(3,3)
      logical EqIgCase,EqRV
      write(t31,100) NRefReadObs,NRefRead
      call Zhusti(t31)
      call FeTxOutCont('Averaging for Laue group: '//
     1                 PointGroup(:idel(PointGroup))//' - '//
     2                 CrSystemNameP(:idel(CrSystemNameP)))
      TextInfo(1)='Averages made from '//t31(:idel(t31))//
     1            ' reflections'
      TextInfo(NInfo)=CrSystemNameP(1:25)//'  '//
     1                PointGroup(:idel(PointGroup))
      Klic=0
      go to 1100
      entry DRGenPointGroup(CrSystemNameP,PointGroup,QuIn,ToOrtho,
     1                      FromOrtho,QuIrr,QuRac,ich)
      Klic=1
1100  allocate(RMP(9,48))
      izmd=0
      ich=0
      do i=1,nSmbPg
        if(EqIgCase(PointGroup,SmbPgI(i))) then
          if(SmbPGN(i).gt.32) then
            ich=1
            go to 2000
          else
            go to 1150
          endif
        endif
      enddo
      ich=1
      go to 2000
1150  if(NDimI(KPhase).eq.1) then
        if(CrSystem(KPhase).eq.CrSystemHexagonal.or.
     1     CrSystem(KPhase).eq.CrSystemTrigonal) then
          Fact=3.
        else
          Fact=2.
        endif
        NIrr=0
        NNul=0
        QuRacPom=0.
        do i=1,3
          pom=abs(Fact*QuIn(i,1))
          if(abs(anint(pom)-pom).gt..01) then
            IIrr(i)=1
            NIrr=NIrr+1
          else
            QuRacPom(i,1)=anint(Fact*QuIn(i,1))/anint(Fact)
            IIrr(i)=0
          endif
          if(pom.gt..01) then
            INul(i)=0
          else
            NNul=NNul+1
            INul(i)=1
          endif
        enddo
        if(CrSystem(KPhase).eq.CrSystemTriclinic) then
          call SetIntArrayTo(IIrr,3,1)
        else if(CrSystem(KPhase).eq.CrSystemMonoclinic) then
          if(NIrr.eq.0) then
            if(abs(QuIn(3,1)).gt.0.) then
              IIrr(3)=1
            else
              IIrr(1)=1
              IIrr(2)=1
            endif
          else if(NIrr.eq.1) then
            if(IIrr(3).ne.1) then
              IIrr(1)=1
              IIrr(2)=1
            endif
          else if(NIrr.eq.2) then
            ich=1
            if(IIrr(3).ne.0) go to 2000
          else if(NIrr.eq.3) then
            ich=1
            go to 2000
          endif
        else if(CrSystem(KPhase).eq.CrSystemOrthorhombic) then
          if(NIrr.eq.0) then
            if(NNul.eq.3) then
              ich=1
              go to 2000
            else
              do i=1,3
                if(INul(i).eq.0) then
                  IIrr(i)=1
                  NIrr=1
                  go to 1200
                endif
              enddo
            endif
          else if(NIrr.gt.1) then
            ich=1
            go to 2000
          endif
        else
          if(NIrr.eq.0) then
            IIrr(3)=1
            NIrr=1
          else if(NIrr.eq.1) then
            if(IIrr(3).eq.0) then
              ich=1
              go to 2000
            endif
          else
            ich=1
            go to 2000
          endif
        endif
1200    do i=1,3
          if(IIrr(i).eq.1) then
            if(QuIn(i,1).ge.0.) then
              zn= 1.
            else
              zn=-1.
            endif
            QuPom(i,1)=QuIn(i,1)+zn*sqrt(.01*abs(QuIn(i,1)))
            QuIrr(i,1)=QuPom(i,1)
            QuRac(i,1)=0.
            izmd=1
          else
            QuIrr(i,1)=0.
            if(INul(i).eq.1) then
              QuRac(i,1)=0.
              QuPom(i,1)=0.
            else
              QuRac(i,1)=QuRacPom(i,1)
              QuPom(i,1)=QuRacPom(i,1)
            endif
          endif
        enddo
      else if(NDimI(KPhase).gt.1) then
        call CopyVek(QuIn,QuPom,3*NDimI(KPhase))
        call CopyVek(QuIn,QuIrr,3*NDimI(KPhase))
      endif
      call GenPg(PointGroup,RMP,NSymm(KPhase),ich)
      k1=0
      k2=0
      n1=0
      n2=0
      if(NDimI(KPhase).gt.1) then
        k1=-1
        k2= 1
      endif
      if(NDimI(KPhase).gt.2) then
        n1=-1
        n2= 1
      endif
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),MaxNLattVec,
     1                 NComp(KPhase),NPhase,1)
      call DefaultISwSymm(0)
      call SetRealArrayTo(ZMag(1,1,KPhase),NSymm(KPhase),1.)
      do is=1,NSymm(KPhase)
        call Multm(FromOrtho,RMP(1,is),TrPom,3,3,3)
        call Multm(TrPom,ToOrtho,RMP(1,is),3,3,3)
        call CopyMat(RMP(1,is),rm(1,is,1,KPhase),3)
        ZMag(is,1,KPhase)=1.
        call CrlMakeRMag(is,1)
        call SetRealArrayTo(rm6(1,is,1,KPhase),NDimQ(KPhase),0.)
        call SetRealArrayTo(s6(1,is,1,KPhase),NDim(KPhase),0.)
        k=0
        do i=1,3
          do j=1,3
            k=k+1
            rm6(j+(i-1)*NDim(KPhase),is,1,KPhase)=RMP(k,is)
          enddo
        enddo
        if(NDimI(KPhase).le.0) go to 1600
        do 1500i=4,NDim(KPhase)
          do j=1,3
            pom=0.
            do k=1,3
              pom=pom+QuPom(k,i-3)*rm6(k+(j-1)*NDim(KPhase),is,1,KPhase)
            enddo
            pp(j)=pom
          enddo
          pqsm=99999.
          do n=n1,n2
            do k=k1,k2
              do 1450j=-1,1
                do l=1,3
                  pq(l)=float(j)*QuPom(l,1)-pp(l)
                  if(NDim(KPhase).gt.4) pq(l)=pq(l)+float(k)*QuPom(l,2)
                  if(NDim(KPhase).gt.5) pq(l)=pq(l)+float(n)*QuPom(l,3)
                  mpp(l)=nint(pq(l))
                  if(abs(pq(l)-float(mpp(l))).gt.2.*DiffModVec) then
                    go to 1450
                  endif
                enddo
                pqs=abs(pq(1))+abs(pq(2))+abs(pq(3))
                if(pqs.lt.pqsm) then
                  do l=1,3
                    rm6(i+NDim(KPhase)*(l-1),is,1,KPhase)=-mpp(l)
                  enddo
                  pqsm=pqs
                  rm6(i+NDim(KPhase)*3,is,1,KPhase)=j
                  if(NDim(KPhase).gt.4)
     1              rm6(i+NDim(KPhase)*4,is,1,KPhase)=k
                  if(NDim(KPhase).gt.5)
     1              rm6(i+NDim(KPhase)*5,is,1,KPhase)=n
                endif
1450          continue
            enddo
          enddo
          if(pqsm.gt.90000.) then
            ich=1
            go to 2000
          endif
1500    continue
1600    do i=1,NDimQ(KPhase)
          rm6(i,is,1,KPhase)=anint(rm6(i,is,1,KPhase))
        enddo
        call MatBlock3(rm6(1,is,1,KPhase),rm(1,is,1,KPhase),
     1                 NDim(KPhase))
        call CodeSymm(rm6(1,is,1,KPhase),s6(1,is,1,KPhase),
     1                symmc(1,is,1,KPhase),0)
      enddo
      if(NDimI(KPhase).gt.0) then
        n=NSymm(KPhase)
        call SetIgnoreWTo(.true.)
        call SetIgnoreETo(.true.)
        call CompleteSymm(0,ich)
        call ResetIgnoreW
        call ResetIgnoreE
        if(ich.ne.0.or.n.ne.NSymm(KPhase)) go to 9900
      endif
2000  if(NDimI(KPhase).eq.1.and.izmd.eq.1) then
        do i=1,3
          if(IIrr(i).eq.1) then
            if(QuIn(i,1).ge.0.) then
              zn=-1.
            else
              zn= 1.
            endif
            QuIrr(i,1)=QuIrr(i,1)+zn*sqrt(.01*abs(QuIn(i,1)))
          endif
        enddo
      else
        call GetQiQr(QuPom,QuIrr,QuRac,NDimI(KPhase),1)
        do i=1,NDimI(KPhase)
          do j=1,3
            QuRac(j,i)=GetFract(QuRac(j,i),DiffModVec,9)
          enddo
        enddo
      endif
      if(ich.ne.0) go to 9900
      if(Klic.eq.1) go to 9999
      NSymmN(KPhase)=NSymm(KPhase)
      if(.not.isPowder) then
        call CrlCompTwinSymmetry(IdSymmFullTr,rtw,NTwin,ich)
        if(TakeSGTwin) then
          NTwinPom=NTwin
          do j=2,NTwin
            call MatInv(rtw(1,j),RPomI,det,3)
            do 2200i=2,NSymm(KPhase)
              call MultM(RPomI,rm(1,i,1,KPhase),RPom,3,3,3)
              call MultM(RPom,rtw(1,j),RPomC,3,3,3)
              do k=1,NSymm(KPhase)
                if(EqRV(RPomC,rm(1,k,1,KPhase),9,.001)) go to 2200
              enddo
              go to 2250
2200        continue
          enddo
          NTwin=1
        endif
2250    call DRAverage(AveFromSGTest,1,ich)
        if(TakeSGTwin) NTwin=NTwinPom
        NAveObs=0
        do i=1,NAve
          if(RIAveAr(i).gt.EM9ObsLim(KDatBlock)*RSAveAr(i))
     1      NAveObs=NAveObs+1
        enddo
        if(rden(1).gt.0.) RIntAll=rnum(1)/rden(1)*100.
        if(roden(1).gt.0.) then
          RIntObs=ronum(1)/roden(1)*100.
          write(Cislo,'(f6.2,''/'',f6.2)') RIntObs,RIntAll
          call Zhusti(Cislo)
          write(t31,100) NAveObs,NAve
          call Zhusti(t31)
        else
          RIntObs=0.
          if(rden(1).gt.0.) then
            write(Cislo,'(''-----/'',f6.2)') RIntAll
          else
            write(Cislo,'(''-----/-----'')')
          endif
          call Zhusti(Cislo)
          t31=Cislo
        endif
        i=index(Cislo,'/')
        TextInfo(NInfo)(41-i:)=Cislo(:idel(Cislo))
        i=index(t31,'/')
        TextInfo(NInfo)(53-i:)=t31(:idel(t31))
        if(roden(1).gt.0.) then
          write(Cislo,'(f10.3)') float(NRefRead)/float(NAve)
          call ZdrcniCisla(Cislo,1)
        else
          Cislo='0'
        endif
        i=index(Cislo,'.')
        TextInfo(NInfo)(64-i:)=Cislo(:idel(Cislo))
      endif
      go to 9999
9900  if(Klic.eq.0)
     1  TextInfo(NInfo)=TextInfo(NInfo)(:33)//
     2                  'inconsistent with modulation vector'
      ich=1
9999  deallocate(RMP)
      return
100   format(i15,'/',i15)
      end
