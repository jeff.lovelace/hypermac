      subroutine DRKumaIndexLimits(KumaInput,sinthlMin,sinthlMax,
     1                             RelIMin,RelIMax,Indexace,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80  Veta
      logical Indexace,CrwLogicQuest
      id=NextQuestId()
      xqd=400.
      if(KumaInput.eq.0.or.KumaInput.eq.3) then
        il=5
      else
        il=2
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1  'Define subset of reflections to be used resp. exported',0,
     2  LightGray,0,0)
      il=1
      Veta='Mi%n(sin(th)/lam)'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=70.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSinthlMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,sinthlMin,.false.,.false.)
      Veta='Ma%x(sin(th)/lam)'
      tpom=tpom+200.
      xpom=xpom+200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSinthlMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,sinthlMax,.false.,.false.)
      il=il+1
      if(KumaInput.ne.1.and.KumaInput.ne.2) then
        Veta='M%in(I/sig(I))'
      else
        Veta='Im%in'
      endif
      tpom=tpom-200.
      xpom=xpom-200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwIMin=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,RelIMin,.false.,.false.)
      if(KumaInput.ne.1.and.KumaInput.ne.2) then
        Veta='M%ax(I/sig(I))'
      else
        Veta='Im%ax'
      endif
      tpom=tpom+200.
      xpom=xpom+200.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwIMax=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,RelIMax,.false.,.false.)
      if(KumaInput.eq.0.or.KumaInput.eq.3) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,2
          il=il+1
          if(i.eq.1) then
            Veta='Make in%dexing'
          else
            Veta='%Export reflection to KUMA tab file'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        0,1)
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(i.eq.1) nCrwIndex=CrwLastMade
        enddo
      endif
1140  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1140
      endif
      if(ich.eq.0) then
        if(KumaInput.eq.0.or.KumaInput.eq.3) then
          Indexace=CrwLogicQuest(nCrwIndex)
        else
          Indexace=.true.
        endif
        call FeQuestRealFromEdw(nEdwSinthlMin,sinthlMin)
        call FeQuestRealFromEdw(nEdwSinthlMax,sinthlMax)
        call FeQuestRealFromEdw(nEdwIMin,RelIMin)
        call FeQuestRealFromEdw(nEdwIMax,RelIMax)
      endif
      call FeQuestRemove(id)
      return
      end
