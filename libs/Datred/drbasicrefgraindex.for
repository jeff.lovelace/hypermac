      subroutine DRBasicRefGraindex
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Radka
      dimension ip(12),rp(12)
      save NRefAll,NRefOK,NRefReadLocal,iflg1p
      equivalence (ip,rp)
      call DRBasicRefGraindexRewind
      NRefAll=0
      NRefOK=0
      NRefReadLocal=0
      go to 9999
      entry DRReadRefGraindex(ik)
      ik=0
      if(NRefAll.le.0) then
        read(71,FormA,end=3000) Radka
        call ChangeCommasForPoints(Radka)
        k=0
        call StToInt(Radka,k,iflg1p,1,.false.,ich)
        if(ich.ne.0) go to 9200
        do i=1,4
          read(71,FormA,end=3000) Radka
        enddo
        call ChangeCommasForPoints(Radka)
        k=0
        call StToInt(Radka,k,ip,2,.false.,ich)
        NRefAll=ip(1)
        NRefOK=ip(2)
        NRefReadLocal=0
      endif
      read(71,FormA,end=3000) Radka
      call ChangeCommasForPoints(Radka)
      NRefReadLocal=NRefReadLocal+1
      if(NRefReadLocal.le.NRefOK) then
        k=0
        call StToInt(Radka,k,ih,NDim95(KRefBlock),.false.,ich)
        if(ich.ne.0) go to 9200
        call StToReal(Radka,k,rp,12,.false.,ich)
        if(ich.ne.0) go to 9200
        ri=rp(12)
        rs=sqrt(ri)
        iflg(1)=iflg1p
      endif
      if(NRefReadLocal.ge.NRefOK) then
        do i=NRefOK+1,NRefAll
          read(71,FormA,end=3000) Radka
        enddo
1220    read(71,FormA,end=3000) Radka
        if(Radka.eq.' ') go to 1220
        backspace 71
        NRefAll=0
        NRefOK=0
        NRefReadLocal=0
      endif
      go to 9999
3000  ik=1
      go to 9999
9200  call FeChybne(-1.,-1.,Radka,'Reading error.',SeriousError)
      ErrFlag=1
9999  return
      end
