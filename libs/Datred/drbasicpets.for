      subroutine DRBasicPets
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      call CloseIfOpened(71)
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)='(3i4,2f10.2)'
      RadiationRefBlock(KRefBlock)=ElectronRadiation
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      call OpenFile(70,SourceFileRefBlock(KRefBlock),'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call DRReadBasicCIF(2)
      write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim95(KRefBlock)
      call CloseIfOpened(70)
      call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted','old')
      call DRBasicRefCIF(71,Klic)
9999  call CloseIfOpened(70)
      return
      end
