      subroutine MorfCryst(Klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension DefVect(3),ViewDir(3),PomVek(6),xp(6)
      character Char2
      character*80 t80
      dimension DFaceIn(4,mxface)
      integer CisBut,EventMode,ButtonStateQuest,SbwItemPointerQuest
      logical Recip,FacesNotOK,ctipl,FeYesNo,eqrv,CrwLogicQuest,
     1        Continuously
      parameter (strotuhel=1.)
      if(Klic.ne.0) then
        xp=0.
        do i=1,NFaces(KRefBlock)
          call CopyVek(DFace(1,i,KRefBlock),xp,3)
          call MultM(xp,TRMP,PomVek,NDim(KPhase),NDim(KPhase),1)
          call CopyVek(PomVek,DFaceIn(1,i),3)
          DFaceIn(4,i)=DFace(4,i,KRefBlock)
        enddo
      else
        call CopyVek(DFace(1,1,KRefBlock),DFaceIn,4*NFaces(KRefBlock))
      endif
      NFacesIn=NFaces(KRefBlock)
1030  do i=1,NFaces(KRefBlock)
        call CopyVek(DFace(1,i,KRefBlock),pomvek,3)
        call MorTransVec(.true.,pomvek)
        call CopyVek(pomvek,krov(1,i),3)
        krov(4,i)=DFace(4,i,KRefBlock)
      enddo
      call MorFillArray(FacesNotOK,ctipl)
      if(FacesNotOK) then
        if(ctipl) then
          go to 9999
        else
          go to 1030
        endif
      endif
      call UnitMat(RotMat,3)
      HLFaceNr=0
      Recip=.false.
      scale=1.
      DefVect(1)=0.
      DefVect(2)=0.
      DefVect(3)=1.
      call CopyVek(DefVect,ViewDir,3)
      id=NextQuestId()
      ichk=0
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      RozmerPlochy=YLenBasWin-80.
      xdPruh=(XLenBasWin-RozmerPlochy)*.5
      ydPruh=YLenBasWin-RozmerPlochy
      call FeMakeGrWin(xdPruh,xdPruh,ydPruh-10.,10.)
      call FeMakeAcWin(0.,0.,0.,0.)
      call FeBottomInfo('#prazdno#')
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      xCenPravyPruh=XMaxAcWin+xdPruh*.5
      ypom=YMaxAcWin-25.
      call FeQuestAbsLblMake(id,xCenPravyPruh,ypom,'Change','C','B')
      ichk=ichk+1
      xpom=XMaxAcWin+20.
      tpom=xpom+CrwgXd+10.
      ypom=ypom-5.
      do i=1,2
        ypom=ypom-15.
        if(i.eq.1) then
          t80='in steps'
        else
          t80='continuously'
        endif
        call FeQuestAbsCrwMake(id,tpom,ypom+.5*CrwgYd,xpom,ypom,t80,'L',
     1                         CrwgXd,CrwgYd,0,ichk)
        if(i.eq.1) then
          nCrwInSteps=CrwLastMade
        else
          nCrwContinuously=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      nCrwSum1=nCrwInSteps+nCrwContinuously
      ypom=ypom-30.
      call FeQuestAbsLblMake(id,xCenPravyPruh,ypom,'Around','C','B')
      ichk=ichk+1
      ypom=ypom-5.
      do i=1,2
        ypom=ypom-15.
        if(i.eq.1) then
          t80='screen axes'
        else
          t80='crystal axes'
        endif
        call FeQuestAbsCrwMake(id,tpom,ypom+.5*CrwgYd,xpom,ypom,t80,'L',
     1                         CrwgXd,CrwgYd,0,ichk)
        if(i.eq.1) then
          nCrwScreenAxes=CrwLastMade
        else
          nCrwCrystalAxes=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
      enddo
      nCrwSum2=nCrwScreenAxes+nCrwCrystalAxes
      ypom=ypom-30.
      call FeQuestAbsLblMake(id,xCenPravyPruh,ypom,'Rotate','C','B')
      xpom1=xCenPravyPruh-15.-ButYd
      xpom2=xCenPravyPruh+15.
      ypom=ypom-5.
      do i=1,3
        ypom=ypom-25.
        call FeQuestAbsButtonMake(id,xpom1,ypom,ButYd+3.,ButYd,'-')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) nButtRotateFirst=ButtonLastMade
        call FeQuestAbsButtonMake(id,xpom2,ypom,ButYd+3.,ButYd,'+')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestAbsLblMake(id,xCenPravyPruh,ypom+.5*ButYd,
     1                         SmbABC(i),'C','N')
        if(i.eq.1) nLblRotFirst=LblLastMade
        if(i.eq.3) nButtRotateLast=ButtonLastMade
      enddo
      xCenLevyPruh=xdPruh*.5
      ypom=YMaxAcWin-25.
      do i=1,3
        ypom=ypom-25.
        if(i.eq.1) then
          t80='%View along'
        else if(i.eq.2) then
          t80='%Highlight face'
        else if(i.eq.3) then
          t80='%Default scale'
          ypom=ypom-60.
        endif
        dpom=FeTxLengthUnder(t80)+5.
        xpom=xCenLevyPruh-dpom*.5
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,t80)
        if(i.eq.1) then
          nButtViewAlong=ButtonLastMade
        else if(i.eq.2) then
          nButtHighLight=ButtonLastMade
        else if(i.eq.3) then
          nButtDefaultScale=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      ypom=ypom+60.
      call FeQuestAbsLblMake(id,xCenLevyPruh,ypom,'Scale','C','B')
      ypom=ypom-30.
      xpom=xCenLevyPruh-11.-ButYd
      call FeQuestAbsButtonMake(id,xpom,ypom,ButYd+3.,ButYd,'-')
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtScaleMinus=ButtonLastMade
      xpom=xCenLevyPruh+11.
      call FeQuestAbsButtonMake(id,xpom,ypom,ButYd+3.,ButYd,'+')
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtScalePlus=ButtonLastMade
      t80='Sho%w axes'
      xpom=40.
      tpom=xpom+CrwXd+10.
      ypom=ypom-70.
      call FeQuestAbsCrwMake(id,tpom,ypom+.5*CrwYd,xpom,ypom,t80,'L',
     1                       CrwXd,CrwYd,0,0)
      nCrwShowAxes=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,.true.)
      ypom=YMinAcWin-28.
      dpom=FeTxLength('XXXXXXXXXXXX')+2.*EdwMarginSize
      xpom=XCenBasWin-dpom*.5
      call FeWinfMake(1,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      ypom=ypom-27.
      t80='%Reset'
      dpom=FeTxLengthUnder(t80)+10.
      xpom=XCenBasWin-dpom-5.
      do i=1,2
        if(i.eq.2) then
          t80='%Exit'
          xpom=XCenBasWin+5.
        endif
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,t80)
        if(i.eq.1) then
          nButtReset=ButtonLastMade
        else
          nButtExit=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      do  i=1,NFaces(KRefBlock)
        call CopyVek(DFace(1,i,KRefBlock),krov(1,i),4)
        call MorTransVec(.true.,krov(1,i))
      enddo
      call UnitMat(Axis,3)
      do i=1,3
        call MorTransVec(.false.,Axis(1,i))
      enddo
      call MorNatochkl(DefVect)
      call MorPrumety
      TakeMouseMove=.true.
      EventMode=0
      ActFace=0
      Continuously=.false.
2200  EventMode=0
      if(Continuously) then
        do i=nButtRotateFirst,nButtRotateLast
          if(ButtonStateQuest(i).eq.ButtonOn) then
            EventMode=1
            go to 2202
          endif
        enddo
      endif
2202  call FeMouseShape(0)
      AllowChangeMouse=.false.
      call FeReleaseOutput
      call FeEvent(EventMode)
      call FeDeferOutput
      if(EventType.eq.EventMouse) then
        if(EventNumber.eq.JeMove) then
          call MorShowFaceInd
        else if(EventNumber.eq.JeLeftDown.and.ActFace.ne.0) then
          if(HlFaceNr.ne.0) then
            i=0
2205        i=i+1
            BodHr(RovHr1(HLFaceNr,i),RovHr2(HLFaceNr,i))=1
            if(rovhr1(hlfacenr,i+1).gt.0) goto 2205
            HlFaceNr=0
          endif
        else if(EventNumber.eq.JeLeftUp.and.CrwLogicQuest(nCrwInSteps))
     1    then
          call MorVymackniButtony(nButtRotateFirst,nButtRotateLast,0)
          Continuously=.false.
        endif
        go to 2200
      endif
      if(EventType.eq.EventButton) then
        if(EventNumber.ge.nButtRotateFirst.and.
     1     EventNumber.le.nButtRotateLast) CisBut=EventNumber
      endif
      if(EventType.eq.EventCrw) then
        if(EventNumber.eq.nCrwInSteps.or.
     1     EventNumber.eq.nCrwContinuously) then
          call FeQuestCrwOn(EventNumber)
          call FeQuestCrwOff(nCrwSum1-EventNumber)
          Continuously=CrwLogicQuest(nCrwContinuously)
          CisBut=0
          call MorVymackniButtony(nButtRotateFirst,nButtRotateLast,0)
          go to 2200
        else if(EventNumber.eq.nCrwScreenAxes.or.
     1          EventNumber.eq.nCrwCrystalAxes) then
          call FeQuestCrwOn(EventNumber)
          call FeQuestCrwOff(nCrwSum2-EventNumber)
          nLbl=nLblRotFirst
          do i=1,3
            ypom=ypom-12.
            if(EventNumber.eq.nCrwScreenAxes) then
              Char2=SmbX(i)
            else
              Char2=Smbabc(i)
            endif
            call FeQuestLblChange(nLbl,Char2)
            nLbl=nLbl+1
          enddo
        else if(EventNumber.eq.nCrwShowAxes) then
          if(CrwLogicQuest(nCrwShowAxes)) then
            call FeQuestCrwOff(nCrwShowAxes)
          else
            call FeQuestCrwOn(nCrwShowAxes)
          endif
          call MorPrumety
        endif
      endif
      if(EventType.eq.EventButton.or.Continuously) then
        if(Continuously.and.EventNumber.le.nButtRotateLast)
     1    EventNumber=CisBut
        if(EventNumber.ge.nButtRotateFirst.and.
     1     EventNumber.le.nButtRotateLast) then
          if(Continuously) then
            call MorVymackniButtony(nButtRotateFirst,nButtRotateLast,
     1                              EventNumber)
            call MorShowFaceInd
          else
            call FeQuestButtonOn(EventNumber)
          endif
          i=(EventNumber-nButtRotateFirst)/2+1
          j=mod(EventNumber-nButtRotateFirst,2)
          if(j.eq.0) then
            pom=-1.
          else
            pom= 1.
          endif
          if(CrwLogicQuest(nCrwScreenAxes)) then
            call SetRealArrayTo(SmerRot,3,0.)
            SmerRot(i)=1.
          else
            call CopyVek(RAxis(1,i),SmerRot,3)
          endif
          call MorDelejRotmat(Pom*StRotUhel,SmerRot)
          call MorRotujKrystal
          call MorPrumety
          Continuously=.true.
          go to 2200
        endif
        if(EventType.eq.EventButton) call FeQuestButtonOn(EventNumber)
        if(EventNumber.eq.nButtReset) then
          if(Continuously) then
            call MorVymackniButtony(nButtRotateFirst,nButtRotateLast,0)
            CisBut=0
          endif
          call MorNatochkl(DefVect)
          call MorPrumety
          scale=1.
        else if(EventNumber.eq.nButtViewAlong) then
          idp=NextQuestId()
          call FeQuestCreate(idp,-2.,-1.,200.,3,
     1                       'View along direction',0,LightGray,0,0)
          call FeQuestEdwMake(idp,10.,1,10.,1,' ','L',180.,EdwYd,1)
          nEdwViewDir=EdwLastMade
          if(HlFaceNr.ne.0) call CopyVek(DFace(1,HlFaceNr,KRefBlock),
     1                                   ViewDir,3)
          call FeQuestRealAEdwOpen(EdwLastMade,ViewDir,3,.false.,
     1                             .false.)
          call FeQuestCrwMake(idp,25.,2,10.,2,'direct space','L',CrwgXd,
     1                        CrwgYd,1,1)
          call FeQuestCrwOpen(CrwLastMade,.not.Recip)
          call FeQuestCrwMake(idp,25.,3,10.,3,'reciprocal space','L',
     1                        CrwgXd,CrwgYd,1,1)
          nCrwRecip=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,Recip)
2270      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventEdw) then
            call FeQuestRealAFromEdw(nEdwViewDir,ViewDir)
            if(VecOrtLen(ViewDir,3).lt..001) then
              call FeChybne(-2.,-1.,'null vector, try again.',' ',
     1                      SeriousError)
            endif
            go to 2270
          else if(CheckType.eq.EventCrw) then
            Recip=CrwLogicQuest(nCrwRecip)
            go to 2270
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2270
          endif
          call FeQuestRemove(idp)
          call FeWInfWrite(1,' ')
          call FeMouseShape(0)
          if(ich.eq.0) then
            call CopyVek(ViewDir,PomVek,3)
            call MorTransVec(Recip,PomVek)
            call MorNatochkl(PomVek)
            call MorPrumety
            call MorVymackniButtony(nButtRotateFirst,nButtRotateLast,0)
          endif
        else if(EventNumber.eq.nButtScaleMinus.or.
     1          EventNumber.eq.nButtScalePlus) then
          if(EventNumber.eq.nButtScaleMinus) then
            xpom1=-.01
          else
            xpom1= .01
          endif
          scale=scale+xpom1
          call MorPrumety
        else if(EventNumber.eq.nButtDefaultScale) then
          scale=1.
          call MorPrumety
        else if(EventNumber.eq.nButtExit) then
          if(Klic.eq.0) then
            go to 3000
          else
            go to 9900
          endif
        else if(EventNumber.eq.nButtHighLight) then
          if(HLFaceNr.eq.0) then
            if(ActFace.eq.0) then
              idp=NextQuestId()
              il=5
              xqd=400.
              call FeQuestCreate(idp,-1.,-1.,xqd,il,'Select face',0,
     1                           LightGray,0,0)
              ln=NextLogicNumber()
              call OpenFile(ln,fln(:ifln)//'_faces.tmp','formatted',
     1                      'unknown')
              if(ErrFlag.ne.0) go to 2550
              do i=1,NFaces(KRefBlock)
                write(t80,'(3f5.0)')(DFace(k,i,KRefBlock),k=1,3)
                call ZdrcniCisla(t80,3)
                write(ln,FormA) t80(:idel(t80))
              enddo
              call CloseIfOpened(ln)
              xpom=10.
              dpom=xqd-20.-SbwPruhXd
              call FeQuestSbwMake(idp,xpom,il,dpom,il,5,CutTextFromLeft,
     1                            SbwHorizontal)
              nSbw=SbwLastMade
              call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_faces.tmp')
2370          call FeQuestEvent(idp,ich)
              if(CheckType.ne.0) then
                call NebylOsetren
                go to 2370
              endif
              if(ich.eq.0) HLFaceNr=SbwItemPointerQuest(nSbw)
              call FeQuestRemove(idp)
              if(ich.ne.0) then
                call FeQuestButtonOff(nButtHighLight)
                go to 2200
              endif
              call DeleteFile(fln(:ifln)//'_faces.tmp')
              call FeWInfWrite(1,' ')
            else
              HlFaceNr=ActFace
              ich=0
            endif
2550        i=0
2600        i=i+1
            BodHr(RovHr1(HLFaceNr,i),RovHr2(HLFaceNr,i))=2
            if(rovhr1(HLfaceNr,i+1).gt.0) goto 2600
            write(t80,'(3f5.0)')(DFace(i,HLFaceNr,KRefBlock),i=1,3)
            call ZdrcniCisla(t80,3)
            call FeQuestButtonLabelChange(nButtHighLight,'Clear '//
     1                                    t80(:idel(t80)))
            call MorPrumety
          else
            call FeQuestButtonLabelChange(nButtHighLight,
     1                                    '%Highlight face')
            i=0
2690        i=i+1
            bodhr(rovhr1(hlfacenr,i),rovhr2(hlfacenr,i))=1
            if(rovhr1(hlfacenr,i+1).gt.0) goto 2690
            HlFaceNr=0
            call MorPrumety
          endif
        endif
        if(EventType.eq.EventButton) call FeQuestButtonOff(EventNumber)
      endif
      go to 2200
3000  if(NFaces(KRefBlock).eq.NFacesIn) then
        do i=1,NFaces(KRefBlock)
          if(.not.eqrv(DFace(1,i,KRefBlock),DFaceIn(1,i),3,.001).or.
     1       abs(DFace(4,i,KRefBlock)-DFaceIn(4,i)).gt..0001) go to 3200
        enddo
        go to 9900
      endif
3200  if(.not.FeYesNo(-1.,-1.,'Do you want to save faces?',0)) then
        NFaces(KRefBlock)=NFacesIn
        call CopyVek(DFaceIn,DFace(1,1,KRefBlock),4*NFacesIn)
      endif
9900  call FeWInfRemove(1)
      call FeQuestRemove(id)
      TakeMouseMove=.false.
      AllowChangeMouse=.true.
9999  return
      end
