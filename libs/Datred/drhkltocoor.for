      subroutine DRHKLToCoor(Angle,c)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension Angle(4),c(3),cosarr(4),sinarr(4)
      equivalence (CosFi   ,cosarr(1)),(SinFi   ,sinarr(1)),
     1            (CosChi  ,cosarr(2)),(SinChi  ,sinarr(2)),
     2            (CosOmega,cosarr(3)),(SinOmega,sinarr(3)),
     3            (CosTheta,cosarr(4)),(SinTheta,sinarr(4))
      do i=1,4
        pom=Angle(i)*ToRad*SenseOfAngle(i)
        if(i.eq.3) pom=pom-Angle(4)*ToRad*SenseOfAngle(4)
        cosarr(i)=cos(pom)
        sinarr(i)=sin(pom)
      enddo
      D=2.*SinTheta/LamAveRefBlock(KRefBlock)
      c(1)=-(CosOmega*CosChi*SinFi+SinOmega*CosFi)*D
      c(2)=-(CosOmega*CosChi*CosFi-SinOmega*SinFi)*D
      c(3)=  CosOmega*SinChi                      *D
      return
      end
