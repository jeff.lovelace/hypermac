      subroutine DRBasicRefXD
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Radka
      dimension iar(1)
      read(71,FormA,end=3000) Radka
      call Mala(Radka)
      k=0
      call Kus(Radka,k,Cislo)
      call Kus(Radka,k,Cislo)
      XDCteI=Cislo.eq.'f^2'.or.Cislo.eq.'f2'
      XDNdat=6
      call Kus(Radka,k,Cislo)
      if(Cislo.eq.'ndat') then
        call StToInt(Radka,k,iar,1,.false.,ich)
        if(ich.ne.0) go to 9200
        XDNdat=iar(1)
      endif
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)='(*)'
      RadiationRefBlock(KRefBlock)=XRayRadiation
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      go to 9999
3000  ik=1
      go to 9999
9200  call FeChybne(-1.,-1.,Radka,'Reading error.',SeriousError)
      ErrFlag=1
9999  return
      end
