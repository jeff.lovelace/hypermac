      subroutine MorShowFaceInd
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*50 t50
      integer inface(100)
      do i=1,100
        inface(i)=0
      enddo
      do i=1,NFaces(KRefBlock)
        if(rkrov(3,i).lt.0.0001) cycle
        j=0
1100    j=j+1
        x1=bodrx(rovhr1(i,j))
        y1=bodry(rovhr1(i,j))
        x2=bodrx(rovhr2(i,j))
        y2=bodry(rovhr2(i,j))
        if((ypos.gt.min(y1,y2)).and.(ypos.lt.max(y1,y2))) then
          xp=(x1*y2-x2*y1+ypos*(x2-x1))/(y2-y1)
          if(xp.gt.xpos) inface(i)=inface(i)+1
          if(xp.lt.xpos) inface(i)=inface(i)-1
          if(inface(i).eq.0) go to 1200
        endif
        if(rovhr1(i,j+1).ne.0) go to 1100
      enddo
      i=0
1200  if(i.ne.ActFace) then
        if(i.ne.0) then
          write(t50,'(3i4)')(nint(dface(j,i,KRefBlock)),j=1,3)
          call ZdrcniCisla(t50,3)
        else
          t50=' '
        endif
        call FeWInfWrite(1,t50)
        if(DeferredOutput) then
          call FeReleaseOutput
          call FeDeferOutput
        endif
        ActFace=i
      endif
      return
      end
