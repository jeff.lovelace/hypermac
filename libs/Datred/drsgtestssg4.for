      subroutine DRSGTestSSG4(Key,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension NObsA(20),h(6),nx(4),ni(4),hp(6),ih1(6),QuSymm(3,3),
     1          QuPom(3,3)
      character*256 FileName
      character*80 Veta,SSG4Text(20)
      character*60 Grp
      character*4  GrX4
      logical EqIgCase,SystExtRef,CrwLogicQuest,Observed
      save SSG4Text,nCrwFirst,ntx,ntxopt,nButtFirst,nButtLast
      NAtCalcIn=NAtCalc
      NAtCalc=0
      if(EqIgCase(GrupaSel,GrupaSelOld).or.Key.eq.1) go to 2500
      idl=index(GrupaSel,')')
      if( iPGSel.le.4.or.
     1   (iPGSel.ge. 9.and.iPgSel.le.10).or.
     2   (iPGSel.ge.16.and.iPgSel.le.17).or.
     3   (iPGSel.ge.21.and.iPgSel.le.22)) then
        nn=1
      else if( iPGSel.eq. 5.or.
     1         iPGSel.eq.11.or.
     2         iPGSel.eq.23.or.Grupa(KPhase)(1:1).eq.'R') then
        nn=2
      else if((iPGSel.ge. 6.and.iPgSel.le. 8).or.
     1        (iPGSel.ge.12.and.iPgSel.le.14).or.
     2        (iPGSel.ge.18.and.iPgSel.le.20).or.
     3        (iPGSel.ge.24.and.iPgSel.le.26)) then
        nn=3
      else
        nn=4
      endif
      if(iPGSel.le.15) then
        m=3
      else
        m=5
      endif
      call SetIntArrayTo(nx,nn,m)
      n=m**nn
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      ntx=1
      ntxopt=0
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.sgt4','formatted','unknown')
      SumaAllMin=99999999.
      do 2000i=1,n
        call RecUnPack(i,ni,nx,nn)
        GrX4=' '
        do j=1,nn
          k=ni(j)
          GrX4(j:j)=SmbSymmT(k)
        enddo
        Grupa(KPhase)=GrupaSel(:idl)//GrX4(:nn)
        call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
        if(ich.ne.0) go to 2000
        call FindSmbSgOrgShiftNo(Grp,.false.,1)
        if(.not.EqIgCase(Grp,Grupa(KPhase))) go to 2000
        NObs=0
        NAll=0
        SumaAll=0.
        SumaObs=0.
        call DRWriteUsedSymm(ln,ntx)
        call SetRealArrayTo(h ,6,0.)
        call SetRealArrayTo(hp,6,0.)
        do 1500j=1,NRefRead
          do ntw=1,NTwin
            if(HCondAr(ntw,i).lt.0) then
              if(ntw.eq.1) then
                go to 1500
              else
                cycle
              endif
            endif
            call IndUnPack(HCondAr(ntw,j),ihp,HCondLn,HCondMx,
     1                     NDim(KPhase))
            call indtr(ihp,CellTrSel6,ih,NDim(KPhase))
            if(ntw.eq.1) then
              do k=4,NDim(KPhase)
                if(ih(k).ne.0) go to 1450
              enddo
              go to 1500
1450          call CopyVekI(ih,ih1,NDim(KPhase))
            endif
            if(.not.SystExtRef(ih,.false.,1)) go to 1500
          enddo
          RelI=max(riar(j)/rsar(j),0.)
          Observed=RelI.gt.3.
          NAll=NAll+1
          SumaAll=SumaAll+RelI
          if(Observed) then
            NObs=NObs+1
            SumaObs=SumaObs+RelI
            write(ln,FormExtRefl)
     1        ntx,(ih1(k),k=1,NDim(KPhase)),riar(j),rsar(j)
          endif
1500    continue
        if(NAll.gt.0) then
          SumaAll=SumaAll/float(NAll)
        else
          SumaAll=0.
        endif
        if(NObs.gt.0) then
          SumaObs=SumaObs/float(NObs)
        else
          SumaObs=0.
        endif
        NObsA(ntx)=NObs
        ntx=ntx+1
        SSG4Text(ntx)=Grupa(KPhase)
        write(Veta,100) nobs,nall
        call Zhusti(Veta)
        SSG4Text(ntx)=SSG4Text(ntx)(:idel(SSG4Text(ntx)))//
     1                Tabulator//Veta(:idel(Veta))
        write(Veta,101) SumaObs,SumaAll
        call Zhusti(Veta)
        SSG4Text(ntx)=SSG4Text(ntx)(:idel(SSG4Text(ntx)))//
     1                Tabulator//Veta(:idel(Veta))
        if(SumaAll.lt.SumaAllMin) then
          ntxopt=ntx
          SumaAllMin=SumaAll
        endif
2000  continue
      call CloseIfOpened(ln)
      if(ntxopt.le.0) ntxopt=ntx
      call ResetIgnoreW
      call ResetIgnoreE
      if(ntx.le.0) then
        call FeUnforeseenError(' ')
        ich=1
        go to 9999
      endif
      GrupaSelOld=GrupaSel
2500  id=NextQuestId()
      call FeQuestTitleMake(id,'Select superspace group')
      call FeTabsReset(7)
      call FeTabsReset(8)
      SSG4Text(1)='Superspace group'//
     1            Tabulator//'obs/all'//
     2            Tabulator//'ave(I/sig(I))'
      pom=120.
      call FeTabsAdd(pom,7,IdRightTab,' ')
      pom=pom+FeTxLength('obs')
      call FeTabsAdd(pom,8,IdCharTab,'/')
      pom=200.
      call FeTabsAdd(pom,7,IdRightTab,' ')
      pom=pom+FeTxLength('ave(I')
      call FeTabsAdd(pom,8,IdCharTab,'/')
      UseTabs=7
      il=1
      xpom=5.
      tpom=xpom+5.+CrwgXd
      tpomb=WizardLength-100.
      dpomb=85.
      call FeQuestLblMake(id,tpom,il,SSG4Text(1),'L','N')
      UseTabs=8
      do i=2,ntx
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,SSG4Text(i),'L',
     1                      CrwgXd,CrwgYd,0,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.ntxopt)
        call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'Details')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.2) then
          nCrwFirst=CrwLastMade
          nButtFirst=ButtonLastMade
        endif
      enddo
      nButtLast=ButtonLastMade
3500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   CheckNumber.ge.nButtFirst.and.CheckNumber.le.nButtLast) then
        n=CheckNumber-nButtFirst+1
        FileName=fln(:ifln)//'_tmp.sgt4'
        call OpenFile(lst,FileName,'formatted','unknown')
        LstOpened=.true.
        k=0
        call Kus(SSG4Text(n+1),k,Veta)
        Uloha='List of strongest satellites contradicting '//
     1        'the superspace group '//Veta(:idel(Veta))
        call NewPg(1)
        if(NDimI(KPhase).eq.1) then
          call AddVek(QuIrrSel,QuRacSel,QuPom,3)
        else
          call CopyVek(QuIrrSel,QuPom,3*NDimI(KPhase))
        endif
        call DRWriteUsedPar(CellParSel,QuPom,CellTrSel6)
        lni=NextLogicNumber()
        call OpenFile(lni,fln(:ifln)//'.sgt4','formatted','unknown')
3510    read(lni,FormA,end=3515) Veta
        if(LocateSubstring(Veta,'symm',.false.,.true.).le.0) go to 3510
        read(Veta,'(i4)') j
        if(j.lt.n) then
          go to 3510
        else if(j.eq.n) then
          call NewLn(1)
          write(lst,FormA) Veta(14:idel(Veta))
          go to 3510
        endif
3515    rewind lni
        Veta=Uloha
        if(NObsA(n).le.0) Veta=Veta(:idel(Veta))//' is empty'
        call TitulekVRamecku(Veta)
        if(NObsA(n).gt.0) then
          NExt(1)=0
          NExtObs(1)=0
          NExt500(1)=0
          RIExtMax(1)=0.
          SumRelI(1)=0.
3520      read(lni,FormA,end=3530) Veta
          if(LocateSubstring(Veta,'symm',.false.,.true.).gt.0)
     1      go to 3520
          read(Veta,FormExtRefl) j,(ih(i),i=1,NDim(KPhase)),
     1                           fpom,spom
          if(j.eq.n)
     1      call EM9NejdouPotvory(1,ih,hp,fpom,spom,NDim(KPhase),
     2                            .false.,3.)
          go to 3520
3530      call DRDejSpravnePotvory
          call CloseIfOpened(lni)
        endif
        call CloseListing
        call FeListView(FileName,0)
        call DeleteFile(FileName)
        go to 3500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3500
      endif
      if(ich.ge.0) then
        nCrw=nCrwFirst
        do i=2,ntx
          if(CrwLogicQuest(nCrw)) then
            ntxopt=i
            go to 3560
          endif
          nCrw=NCrw+1
        enddo
        ich=1
        go to 9999
3560    k=0
        if(ich.eq.0) call kus(SSG4Text(i),k,GrupaSel)
      endif
9999  NAtCalc=NAtCalcIn
      return
100   format(i15,'/'i15)
101   format(f10.3,'/',f10.3)
      end
