      subroutine DRCell2OrtTr(Cell,OrtTr)
      include 'fepc.cmn'
      dimension OrtTr(9),Cell(6)
      csa=cos(Cell(4)*ToRad)
      csb=cos(Cell(5)*ToRad)
      csg=cos(Cell(6)*ToRad)
      snb=sqrt(1.-csb**2)
      sng=sqrt(1.-csg**2)
      pom=(csa-csb*csg)/sng
      OrtTr(1)=Cell(1)
      OrtTr(2)=0.
      OrtTr(3)=0.
      OrtTr(4)=Cell(2)*csg
      OrtTr(5)=Cell(2)*sng
      OrtTr(6)=0.
      OrtTr(7)=Cell(3)*csb
      OrtTr(8)=Cell(3)*pom
      OrtTr(9)=Cell(3)*snb*sqrt(1.-(pom/snb)**2)
      return
      end
