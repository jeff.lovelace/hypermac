      subroutine DRAbsSemiEmpirSet(ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 :: Veta,t80
      real, allocatable :: Iobs(:),SIobs(:),flma(:,:),AbsCorrArr(:),
     1                     RMP(:),PS(:),ScFr(:),der(:),Diag(:),
     2                     ParOld(:),ParDiff(:),KdeJe(:),TBarA(:)
      real :: Iave,Jhj,xp(3),xr(3),ylm1(81),ylm2(81),Alm(81),flm(81),
     1        ab(19),hh(3),DirCosP(3,2)
      integer ihr(6),FlagMax,h
      integer, allocatable :: Flag(:),iha(:,:)
      logical EqIV,EQIV0,Poprve,Change,Konec,FeYesNo
      logical :: CorrWeight=.false.,RefineDelMi=.false.
      DelMi(KRefBlock)=0.
      call DRSetCell(0)
1050  ln=0
      if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
        rmi=AmiUsed*RadiusRefBlock(KRefBlock)
        pom=rmi*10.+1.
        n=pom
        pom=pom-float(n)
        if(n.gt.100) then
          call FeChybne(-1.,-1.,'too high mi*r for spherical sample',
     1                  'data will not be corrected.',SeriousError)
          go to 9999
        endif
        do j=1,19
          ab(j)=abskou(j,n)+pom*(abskou(j,n+1)-abskou(j,n))
        enddo
      else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
        if(NFaces(KRefBlock).le.3) then
          call FeChybne(-1.,-1.,'number of faces is not sufficient '//
     1                  'to make the correction.',' ',SeriousError)
          go to 9999
        endif
        NFacesR=NFaces(KRefBlock)
        do k=1,NFacesR
          if(UseTrRefBlock(KRefBlock)) then
            call MultM(DFace(1,k,KRefBlock),TrRefBlock(1,KRefBlock),
     1                 DFaceR(1,k),1,3,3)
          else
            call CopyVek(DFace(1,k,KRefBlock),DFaceR(1,k),3)
          endif
          DFaceR(4,k)=DFace(4,k,KRefBlock)
        enddo
        call grit
      endif
      Veta=fln(:ifln)//'.eqv'
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.gt.0) then
        ErrFlag=0
        go to 9900
      endif
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        n=0
        do i=1,NRuns(KRefBlock)
          n=n+RunNFrames(i,KRefBlock)
        enddo
        call DRSetRunScGrN(ich)
      else
        RunScN(KRefBlock)=0
      endif
      NSpHarm=0
      if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
        do l=1,8
          if(mod(l,2).eq.1) then
            if(l.gt.SpHOdd(KRefBlock)) cycle
          else
            if(l.gt.SpHEven(KRefBlock)) cycle
          endif
          do k=-l,l
            NSpHarm=NSpHarm+1
          enddo
        enddo
      endif
      FlagMax=0
      NRef=0
      ihr=-999999
      do kk=1,2
        mm=0
1100    read(ln,FormA,end=1300) Veta
        NRef=NRef+1
        if(kk.eq.2) then
          read(Veta,FormatEQV) iha(1:maxNDim,NRef),
     1      Iobs(NRef),SIobs(NRef),i,((DirCosP(i,j),j=1,2),i=1,3),pom
          if(SEType.eq.SEScales.or.SEType.eq.SEAll) KdeJe(NRef)=pom
          if(EqIV0(iha(1,NRef),NDim(KPhase))) then
            NRef=NRef-1
            go to 1100
          endif
          ih=0
          ih(1:maxNDim)=iha(1:maxNDim,NRef)
          do i=1,3
            sod(i)= dircosP(i,1)*rcp(i,1,KPhase)
            sd(i) = dircosP(i,2)*rcp(i,1,KPhase)
          enddo
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
            call MultM(TrToOrtho(1,1,1),sod,xp,3,3,1)
            call MultM(TrToOrtho(1,1,1),sd ,xr,3,3,1)
            call SpherHarm(xp,ylm1,8,3)
            call SpherHarm(xr,ylm2,8,3)
            j=0
            i=1
            do l=1,8
              i=l**2
              if(mod(l,2).eq.1) then
                if(l.gt.SpHOdd(KRefBlock)) cycle
              else
                if(l.gt.SpHEven(KRefBlock)) cycle
              endif
              do k=-l,l
                i=i+1
                j=j+1
                flma(j,NRef)=.5*(ylm1(i)+ylm2(i))
              enddo
            enddo
          endif
          sod=-sod
          if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
            call FromIndSinthl(iha(1,NRef),hh,sinthl,sinthlq,1,0)
            ThP=asin(sinthl*LamAveRefBlock(KRefBlock))/ToRad
            d1=.2*ThP+1.
            j=d1
            d1=d1-float(j)
            Corr0=ab(j)+d1*(ab(j+1)-ab(j))
          else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
            call DRLength2
            Corr0=corrf(2)
            TBarA(NRef)=TBar*10.
          else
            Corr0=1.
          endif
          Iobs(NRef)=Iobs(NRef)*Corr0
          SIobs(NRef)=SIobs(NRef)*Corr0
          AbsCorrArr(NRef)=Corr0
          do i=1,NSymm(KPhase)
            call MultMIRI(iha(1,NRef),RM6(1,i,1,KPhase),ih,1,
     1                    NDim(KPhase),NDim(KPhase))
            if(EqIV(ih,ihr,NDim(KPhase))) go to 1200
          enddo
          if(mm.eq.1) then
            if(SEType.eq.SESpHarm.or.SEType.eq.SEAll)
     1        flma(1:NSpHarm,NRef-1)=flma(1:NSpHarm,NRef)
            Iobs(NRef-1)=Iobs(NRef)
            SIobs(NRef-1)=SIobs(NRef)
            AbsCorrArr(NRef-1)=AbsCorrArr(NRef)
            TBarA(NRef-1)=TBarA(NRef)
            iha(1:maxNDim,NRef-1)=iha(1:maxNDim,NRef)
            NRef=NRef-1
            FlagMax=FlagMax-1
          endif
          mm=0
          FlagMax=FlagMax+1
          ihr(1:maxNDim)=iha(1:maxNDim,NRef)
1200      Flag(NRef)=FlagMax
          mm=mm+1
        endif
        go to 1100
1300    if(kk.eq.1) then
          if(allocated(Iobs)) deallocate(Iobs,SIobs,Flag,AbsCorrArr,iha,
     1                                   TBarA)
          if(allocated(flma)) deallocate(flma)
          if(allocated(KdeJe)) deallocate(KdeJe)
          allocate(Iobs(NRef),SIobs(NRef),Flag(NRef),AbsCorrArr(NRef),
     1             iha(6,NRef),TBarA(NRef))
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll)
     1      allocate(flma(NSpHarm,NRef))
          if(SEType.eq.SEScales.or.SEType.eq.SEAll)
     1      allocate(KdeJe(NRef))
          rewind ln
          NRef=0
        endif
      enddo
      call CloseIfOpened(ln)
      n=RunScN(KRefBlock)+NSpHarm+1
      if(allocated(RMP)) deallocate(RMP,PS,der,Diag,ParOld,ParDiff)
      allocate(RMP((n*(n+1))/2),PS(n),der(n),Diag(n),ParOld(n),
     1         ParDiff(n))
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        if(allocated(ScFr)) deallocate(ScFr)
        allocate(ScFr(RunScN(KRefBlock)))
        ScFr=1.
      endif
      if(SEType.eq.SESpHarm) then
        NRefPar=NSpHarm
      else if(SEType.eq.SEScales) then
        NRefPar=RunScN(KRefBlock)
      else
        NRefPar=n-1
      endif
      if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss.and.RefineDelMi)
     1  NRefPar=NRefPar+1
      Alm=0.
      Alm(1)=1.
      ICykl=0
      Rold=1.
      NDump=0
      IDump=0
      Poprve=.true.
      Konec=.false.
2000  nn=0
      wsiq =0.
      wsisq=0.
      si =0.
      sis=0.
      if(SEType.eq.SESpHarm) then
        NRefPar=NSpHarm
      else if(SEType.eq.SEScales) then
        NRefPar=RunScN(KRefBlock)
      else
        NRefPar=n-1
      endif
      if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss.and.RefineDelMi)
     1  NRefPar=NRefPar+1
      NUni=0
      NAll=0
      CorrMin=9999999.
      CorrMax=-9999999.
      do h=1,FlagMax
        ip=0
2100    nn=nn+1
        if(ip.eq.0) ip=nn
        if(Flag(nn).eq.h) then
          ik=nn
          if(nn.lt.NRef) go to 2100
        else
          nn=nn-1
        endif
        NUni=NUni+1
2150    m=0
        sav =0.
        savs=0.
        swav =0.
        swavs=0.
        do i=ip,ik
          NAll=NAll+1
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
            Corr1=Alm(1)+VecOrtScal(Alm(2),Flma(1,i),NSpHarm)
          else
            Corr1=1.
          endif
          if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
            ikde=RunScGrN(ifix(KdeJe(i)),KRefBlock)
            if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
              pom=ScFr(ikde)
            else
              c1=(KdeJe(i)-float(RunScGrM0(ikde,KRefBlock)))/
     1            float(RunScGrM(ikde,KRefBlock)-1)
              pom=(1.-c1)*ScFr(ikde)+c1*ScFr(ikde+1)
            endif
            Corr2=pom
          else
            Corr2=1.
          endif
          pom=AbsCorrArr(i)*Corr1
          if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
            Corr0=exp(DelMi(KRefBlock)*TBarA(i))
            pom=pom*Corr0
          else
            Corr0=1.
          endif
          CorrMin=min(pom,CorrMin)
          CorrMax=max(pom,CorrMax)
          m=m+1
          if(CorrWeight) then
            wt=1./(Corr0*Corr1*Corr2*SIobs(i))**2
          else
            wt=1./(SIobs(i))**2
          endif
          swav =swav +wt*Iobs(i)*Corr0*Corr1*Corr2
          swavs=swavs+wt
          sav =sav +Iobs(i)*Corr0*Corr1*Corr2
          savs=savs+1.
        enddo
        Iwave=swav/swavs
        Iave=sav/savs
        do i=ip,ik
          if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
            Corr0=exp(DelMi(KRefBlock)*TBarA(i))
          else
            Corr0=1.
          endif
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
            Corr1=Alm(1)+VecOrtScal(Alm(2),Flma(1,i),NSpHarm)
          else
            Corr1=1.
          endif
          if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
            ikde=RunScGrN(ifix(KdeJe(i)),KRefBlock)
            if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
              pom=ScFr(ikde)
            else
              c1=(KdeJe(i)-float(RunScGrM0(ikde,KRefBlock)))/
     1            float(RunScGrM(ikde,KRefBlock)-1)
              pom=(1.-c1)*ScFr(ikde)+c1*ScFr(ikde+1)
            endif
            Corr2=pom
          else
            Corr2=1.
          endif
          if(CorrWeight) then
            wt=1./(Corr1*Corr2*SIobs(i))**2
          else
            wt=1./(SIobs(i))**2
          endif
          wsiq=wsiq+wt*(Iobs(i)*Corr0*Corr1*Corr2-Iwave)**2
          wsisq=wsisq+wt*(Iobs(i)*Corr0*Corr1*Corr2)**2
          si=si+abs(Iobs(i)*Corr0*Corr1*Corr2-Iave)
          sis=sis+abs(Iobs(i)*Corr0*Corr1*Corr2)
        enddo
      enddo
      wa=wsisq/wsiq
      RNew=si/sis
      Poprve=.false.
      NInfo=2
      write(TextInfo(1),'(''R(int):'',f7.3,'' Rw(int):'',f7.3)')
     2  100.*RNew,100.*sqrt(wsiq/wsisq)
      write(TextInfo(2),'(''T(min):'',f7.4,'', T(max):'',f7.4)')
     1  1./CorrMax,1./CorrMin
      call FeMsgShow(1,-1.,300.,ICykl.eq.0)
      if(CorrMin.lt.0.) then
        call FeWait(2.)
        call FeChybne(-1.,-1.,'the process cannot continue due to a '//
     1                'severe numerical problem.',
     2                'Modify parametrs and try again.',SeriousError)
        call FeMsgClear(1)
        go to 9900
      endif
      if(100.*abs(RNew-ROld).lt..001.or.ICykl.ge.NCyklSemiEmpir.or.
     1   Konec) then
        call FeWait(2.)
        call FeMsgClear(1)
        go to 3000
      endif
      if(SEType.eq.SESpHarm) then
        Veta='coefficients of spherical harmonics'
      else if(SEType.eq.SEScales) then
        Veta='frame scales'
      else
        Veta='frame scales and coefficients of spherical harmonics'
      endif
      write(t80,'(i10,''/'',i10)') ICykl,NCyklSemiEmpir
      call Zhusti(t80)
      call FeFlowChartOpen(-1.,120.,max(nint(NAll*.005),10),2*NAll,
     1                     'Refinement of '//Veta(:idel(Veta))//' - '//
     2                     t80(:idel(t80)),'%End','%Parameters')
      Rold=Rnew
      RMP=0.
      PS=0.
      nn=0
      NUsed=0
      do h=1,FlagMax
        ip=0
        sw=0.
2200    nn=nn+1
        if(ip.eq.0) ip=nn
        if(Flag(nn).eq.h) then
          ik=nn
          if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
            Corr0=exp(DelMi(KRefBlock)*TBarA(nn))
          else
            Corr0=1.
          endif
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
            Corr1=Alm(1)+VecOrtScal(Alm(2),Flma(1,nn),NSpHarm)
          else
            Corr1=1.
          endif
          if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
            ikde=RunScGrN(ifix(KdeJe(nn)),KRefBlock)
            if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
              pom=ScFr(ikde)
            else
              c1=(KdeJe(nn)-float(RunScGrM0(ikde,KRefBlock)))/
     1            float(RunScGrM(ikde,KRefBlock)-1)
              pom=(1.-c1)*ScFr(ikde)+c1*ScFr(ikde+1)
            endif
            Corr2=pom
          else
            Corr2=1.
          endif
          if(CorrWeight) then
            sw=sw+1./(SIobs(nn)*Corr0*Corr1*Corr2)**2
          else
            sw=sw+1./SIobs(nn)**2
          endif
          if(nn.lt.NRef) go to 2200
        else
          nn=nn-1
          do i=ip,ik
            call FeFlowChartEvent(NUsed,ie)
            if(ie.ne.0) then
              if(ie.eq.1) then
                Konec=FeYesNo(-1.,250.,
     1            'Do you really want to end the refinement?',0)
              else if(ie.eq.3) then
                if(FeYesNo(-1.,250.,'Do you really want to cancel the'//
     1                   ' refinement?',0)) then
                  call FeMsgClear(1)
                  go to 9900
                endif
              else if(ie.eq.2) then
                call DRAbsSemiEmpirOptions(ICykl)
              endif
            endif
            der=0.
            if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
              Corr0=exp(DelMi(KRefBlock)*TBarA(i))
            else
              Corr0=1.
            endif
            if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
              Corr1=Alm(1)+VecOrtScal(Alm(2),Flma(1,i),NSpHarm)
            else
              Corr1=1.
            endif
            if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
              ikde=RunScGrN(ifix(KdeJe(i)),KRefBlock)
              if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                pom=ScFr(ikde)
              else
                c1=(KdeJe(i)-float(RunScGrM0(ikde,KRefBlock)))/
     1              float(RunScGrM(ikde,KRefBlock)-1)
                pom=(1.-c1)*ScFr(ikde)+c1*ScFr(ikde+1)
              endif
              Corr2=pom
            else
              Corr2=1.
            endif
            FI=-Iobs(i)*Corr0*Corr1*Corr2
            l=0
            if(CorrWeight) then
              wt=1./(Corr0*Corr1*Corr2*SIobs(i))**2
            else
              wt=1./(SIobs(i))**2
            endif
            if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
              do k=1,NSpHarm
                l=l+1
                der(l)=der(l)+Flma(k,i)*Iobs(i)*Corr0*Corr2
              enddo
            endif
            if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
              l=l+ikde
              if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                der(l)=der(l)+Iobs(i)*Corr0*Corr1
              else
                der(l)=der(l)+(1.-c1)*Iobs(i)*Corr0*Corr1
                der(l+1)=der(l+1)+c1*Iobs(i)*Corr0*Corr1
              endif
            endif
            if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss.and.RefineDelMi)
     1        der(NRefPar)=der(NRefPar)+
     2                     Iobs(i)*Corr0*Corr1*Corr2*TBarA(i)
            do j=ip,ik
              if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
                Corr0=exp(DelMi(KRefBlock)*TBarA(j))
              else
                Corr0=1.
              endif
              if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
                Corr1=Alm(1)+VecOrtScal(Alm(2),Flma(1,j),NSpHarm)
              else
                Corr1=1.
              endif
              if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
                jkde=RunScGrN(ifix(KdeJe(j)),KRefBlock)
                if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                  pom=ScFr(jkde)
                else
                  c1=(KdeJe(j)-float(RunScGrM0(jkde,KRefBlock)))/
     1                float(RunScGrM(ikde,KRefBlock)-1)
                  pom=(1.-c1)*ScFr(jkde)+c1*ScFr(jkde+1)
                endif
                Corr2=pom
              else
                Corr2=1.
              endif
              if(CorrWeight) then
                wtj=1./(Corr0*Corr1*Corr2*SIobs(j))**2
              else
                wtj=1./SIobs(j)**2
              endif
!              pom=wtj/sw*Iobs(j)*Corr0
              pom=wtj/sw*Iobs(j)
              l=0
              if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
                do k=1,NSpHarm
                  l=l+1
                  der(l)=der(l)-pom*Corr0*Corr2*Flma(k,j)
                enddo
              endif
              if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
                l=l+jkde
                der(l)=der(l)-pom*Corr0*Corr1
              endif
              if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss.and.RefineDelMi)
     1          der(NRefPar)=der(NRefPar)-
     2                       pom*Corr0*Corr1*Corr2*TBarA(j)
              FI=FI+pom*Corr0*Corr1*Corr2
            enddo
            m=0
            do ii=1,NRefPar
              derii=der(ii)
              if(derii.eq.0.) then
                m=m+ii
                cycle
              endif
              PS(ii)=PS(ii)+wt*FI*derii
              do jj=1,ii
                m=m+1
                derjj=der(jj)
                if(derjj.ne.0.) RMP(m)=RMP(m)+wt*derii*derjj
              enddo
            enddo
          enddo
          if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
            do i=ip,ik
              call FeFlowChartEvent(NUsed,ie)
              if(ie.ne.0) then
                if(ie.eq.1) then
                  Konec=FeYesNo(-1.,250.,
     1              'Do you really want to end the refinement?',0)
                else if(ie.eq.3) then
                  if(FeYesNo(-1.,250.,'Do you really want to cancel '//
     1                       'the refinement?',0)) then
                    call FeMsgClear(1)
                    go to 9900
                  endif
                else if(ie.eq.2) then
                  call DRAbsSemiEmpirOptions(ICykl)
                endif
              endif
              FI=0.
              der=0.
              do j=2,NSpHarm+1
                FI=FI-Alm(j)*Flma(j-1,i)
                der(j-1)=Flma(j-1,i)
              enddo
              m=0
              do ii=1,NRefPar
                PS(ii)=PS(ii)+wa*FI*der(ii)
                do jj=1,ii
                  m=m+1
                  RMP(m)=RMP(m)+wa*der(ii)*der(jj)
                enddo
              enddo
            enddo
          endif
        endif
2211    continue
      enddo
      l=NSpHarm
      wt=1./(.01)**2
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        FI=RunScN(KRefBlock)
        fn=0.
        ScSum=0.
        do i=1,RunScN(KRefBlock)
          if(ScBrat(i,KRefBlock)) then
            FI=FI-ScFr(i)
            ScSum=ScSum+ScFr(i)
            fn=fn+1.
          endif
        enddo
        FI=fn-ScSum
        der=0.
        do ii=1,RunScN(KRefBlock)
          if(ScBrat(ii,KRefBlock)) der(l+ii)=1.
        enddo
        m=0
        do ii=1,NRefPar
          PS(ii)=PS(ii)+wt*FI*der(ii)
          do jj=1,ii
            m=m+1
            RMP(m)=RMP(m)+wt*der(ii)*der(jj)
          enddo
        enddo
        ij=0
        do i=1,NRefPar
          ij=ij+i
          if(i.gt.l.and.i.le.n) then
            if(.not.ScBrat(i-l,KRefBlock)) RMP(ij)=1.
          endif
        enddo
      endif
      ij=0
      do i=1,NRefPar
        ij=ij+i
        if(LSMethodSemiEmpir.eq.2) RMP(ij)=RMP(ij)*(1.+MarqLamSemiEmpir)
        Diag(i)=1./sqrt(RMP(ij))
      enddo
      call znorm(RMP,Diag,NRefPar)
      call smi(RMP,NRefPar,ising)
      if(ising.ne.0) then
        write(Cislo,'(2i6)') ising,ising-NSpHarm
        call ZdrcniCisla(Cislo,2)
        call FeChybne(-1.,-1.,'the process cannot continue due to a '//
     1                'singularity problem #'//Cislo,
     2                'Modify parametrs and try again.',SeriousError)
        call FeMsgClear(1)
        go to 9900
      endif
      call znorm(RMP,Diag,NRefPar)
      call nasob(RMP,PS,ParDiff,NRefPar)
      l=0
      if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
        do i=1,NSpHarm
          l=l+1
          ParOld(l)=Alm(i+1)
        enddo
      endif
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        do i=1,RunScN(KRefBlock)
          l=l+1
          ParOld(l)=ScFr(i)
        enddo
      endif
      l=0
      if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
        do i=1,NSpHarm
          l=l+1
          Alm(i+1)=ParOld(l)+ParDiff(l)*TlumSemiEmpir
        enddo
      endif
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        do i=1,RunScN(KRefBlock)
          l=l+1
          ScFr(i)=ParOld(l)+ParDiff(l)*TlumSemiEmpir
        enddo
      endif
      if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
        s=0.
        do i=2,NSpHarm+1
          s=s+Alm(i)
        enddo
        s=-s/float(NSpHarm)
        do i=2,NSpHarm+1
          Alm(i)=Alm(i)+s
        enddo
      endif
      if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        ScSum=0.
        fn=0.
        do i=1,RunScN(KRefBlock)
          if(ScBrat(i,KRefBlock)) then
            FI=FI-ScFr(i)
            ScSum=ScSum+ScFr(i)
            fn=fn+1.
          endif
        enddo
        pom=(fn-ScSum)/fn
        do i=1,RunScN(KRefBlock)
          if(ScBrat(i,KRefBlock))
     1      ScFr(i)=ScFr(i)+pom
        enddo
      endif
      if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss.and.RefineDelMi) then
        pom=DelMi(KRefBlock)+ParDiff(NRefPar)*TlumSemiEmpir
        if(AmiUsed+pom.gt.AmiUsed/3.and.
     1     AmiUsed+pom.lt.AmiUsed*3.) then
           DelMi(KRefBlock)=pom
        else
           DelMi(KRefBlock)=0.
        endif
      endif
      ICykl=ICykl+1
      call FeFlowChartRemove
      go to 2000
3000  if(SEType.eq.SEScales.or.SEType.eq.SEAll) then
        if(allocated(ScFrame)) deallocate(ScFrame)
        allocate(ScFrame(RunScN(KRefBlock),MxRefBlock))
        ScFrame=1.
        do i=1,RunScN(KRefBlock)
          ScFrame(i,KRefBlock)=ScFr(i)
        enddo
      endif
      if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
        if(allocated(SpHAlm)) deallocate(SpHAlm)
        allocate(SpHAlm(NSpHarm+1,MxRefBlock))
        do i=1,NSpHarm+1
          SpHAlm(i,KRefBlock)=Alm(i)
        enddo
        SpHN(KRefBlock)=NSpHarm+1
      endif
      if(allocated(Iobs)) deallocate(Iobs,SIobs,Flag,AbsCorrArr,iha,
     1                               TBarA)
      if(allocated(flma)) deallocate(flma)
      if(allocated(KdeJe)) deallocate(KdeJe)
      if(allocated(RMP)) deallocate(RMP,PS,der,Diag,ParOld,ParDiff)
      if(allocated(ScFr)) deallocate(ScFr)
      if(ExistM50) then
        Veta=Formula(KPhase)
        NUnitsSave=NUnits(KPhase)
        call iom50(0,0,fln(:ifln)//'.m50')
        Formula(KPhase)=Veta
        NUnits(KPhase)=NUnitsSave
      endif
!      call DeleteFile(fln(:ifln)//'.eqv')
      go to 9999
9900  ich=1
9999  call FeFlowChartRemove
      return
      end
