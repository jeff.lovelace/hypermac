      subroutine DRBasicIPStoe
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 EdwStringQuest
      character*128 Radka,Veta,FileName(2)
      character*80  t80
      dimension xp(7),ia(1)
      logical Skrtat,ExistFile,CrwLogicQuest,Znovu,EqIgCase,WizardModeIn
      integer Vyskyt,TypeFile
      data Last/0/
      WizardModeIn=WizardMode
      WizardMode=.false.
      Znovu=.false.
      WorkString=SourceFileRefBlock(KRefBlock)
      call GetPureFileName(SourceFileRefBlock(KRefBlock),WorkString)
      if(EqIgCase(WorkString(:idel(WorkString)),'.hklf5'))
     1  HKLF5RefBlock(KRefBlock)=1
1150  FileName(1)=WorkString(:idel(WorkString))//'.sum'
      FileName(2)=WorkString(:idel(WorkString))//'.crs'
      id=NextQuestId()
      xqd=400.
      il=4
      Veta='Define the file containing basic information'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='%The basic information will be supplied later manually'
      do i=1,3
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
        if(i.eq.1) then
          Veta='Use the "%sum" file'
          nCrwManual=CrwLastMade
        else if(i.eq.2) then
          Veta='Use the "%crs" file'
          nCrwSumFile=CrwLastMade
        else
          nCrwCrsFile=CrwLastMade
        endif
        il=il+1
      enddo
      Veta='%File name:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      t80='%Browse'
      dpomb=FeTxLengthUnder(t80)+20.
      dpom=xqd-xpom-dpomb-15.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,t80)
      nButtBrowse=ButtonLastMade
      TypeFile=1
      call FeQuestStringEdwOpen(nEdwFileName,FileName(TypeFile))
1300  if(TypeFile.ne.0)
     1  FileName(TypeFile)=EdwStringQuest(nEdwFileName)
      if(CrwLogicQuest(nCrwManual)) then
        call FeQuestEdwDisable(nEdwFileName)
        call FeQuestButtonDisable(nButtBrowse)
        TypeFile=0
      else
        if(CrwLogicQuest(nCrwSumFile)) then
          TypeFile=1
        else
          TypeFile=2
        endif
        call FeQuestStringEdwOpen(nEdwFileName,FileName(TypeFile))
        call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      endif
1350  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1300
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        if(CrwLogicQuest(nCrwSumFile)) then
          Cislo='sum'
        else
          Cislo='crs'
        endif
        call FeFileManager('Select corresponding "'//
     1                     Cislo(:idel(Cislo))//'" file',
     2                     FileName(TypeFile),
     3                     '*.'//Cislo(:idel(Cislo)),0,.true.,ich)
        if(ich.le.0.and.idel(FileName).gt.0)
     1    call FeQuestStringEdwOpen(nEdwFileName,FileName)
          go to 1350
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1350
      endif
      if(ich.eq.0) then
        if(CrwLogicQuest(nCrwManual)) then
          TypeFile=0
        else if(CrwLogicQuest(nCrwSumFile)) then
          TypeFile=1
        else
          TypeFile=2
        endif
        if(TypeFile.ne.0) then
          FileName(TypeFile)=EdwStringQuest(nEdwFileName)
          if(.not.ExistFile(FileName(TypeFile))) then
            Veta='the file "'//
     1           FileName(TypeFile)(:idel(FileName(TypeFile)))//
     1           '" doesn''t exist.'
            call FeChybne(-1.,YBottomMessage,Veta(:idel(Veta))//
     1                    ', try again.',' ',SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1350
          endif
        else
          FileName=' '
        endif
      endif
      call FeQuestRemove(id)
      WizardMode=.true.
      if(ich.ne.0) go to 9900
      if(TypeFile.eq.0) go to 6000
      call CheckEOLOnFile(FileName(TypeFile),2)
      if(ErrFlag.ne.0) go to 6000
      call OpenFile(70,FileName(TypeFile),'formatted','old')
      if(ErrFlag.ne.0) go to 6000
      if(TypeFile.eq.1) then
        icr=0
        Vyskyt=0
        MameCell=0
        MameSU=0
1500    read(70,FormA,end=2000) Radka
        if(Radka.eq.' ') go to 1500
        if(LocateSubstring(Radka,'cell refinement',.false.,.true.).gt.0
     1       .or.
     2     LocateSubstring(Radka,'cell transformation',.false.,.true.)
     3       .gt.0) then
          icr=icr+1
        else if(LocateSubstring(Radka,'cell/modulation vectors '//
     1                          'refinement',.false.,.true.).gt.0) then
          icr=icr+1
        endif
        if(LocateSubstring(Radka,'orienting matrix',.false.,.true.).gt.
     1     0) then
          k=0
          do i=1,3
            call kus(radka,k,cislo)
          enddo
          if(LocateSubstring(Radka,'new',.false.,.true.).gt.0)
     1      call kus(radka,k,Cislo)
          do i=1,3
            call StToReal(Radka,k,xp,3,.false.,ich)
            do j=1,3
              ub(i,j,KRefBlock)=xp(j)
            enddo
            if(i.ne.3) then
1540          read(70,FormA,end=2000) radka
              if(Radka.eq.' ') go to 1540
            endif
            k=0
          enddo
          if(icr.le.1) then
            Vyskyt=Vyskyt+1
          else
            icr=0
          endif
          go to 1500
        else if(LocateSubstring(Radka,'Cell :',.false.,.true.).gt.0)
     1    then
          if(LocateSubstring(Radka,'Initial',.false.,.true.).gt.0.or.
     1       LocateSubstring(Radka,'Final',.false.,.true.).gt.0.or.
     2       LocateSubstring(Radka,'refinement',.false.,.true.).gt.0)
     3      go to 1500
          k=0
          call kus(radka,k,cislo)
          call kus(radka,k,cislo)
          call StToReal(Radka,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
          if(ich.eq.0) MameCell=MameCell+1
          go to 1500
        else if(LocateSubstring(Radka,'Esds',.false.,.true.).gt.0) then
          if(LocateSubstring(Radka,'Initial',.false.,.true.).gt.0.or.
     1       LocateSubstring(Radka,'Final',.false.,.true.).gt.0.or.
     2       LocateSubstring(Radka,'refinement',.false.,.true.).gt.0)
     3      go to 1500
          k=0
          call kus(radka,k,cislo)
          call kus(radka,k,cislo)
          call StToReal(Radka,k,CellRefBlockSU(1,KRefBlock),6,.false.,
     1                  ich)
          if(ich.eq.0) MameSU=MameSU+1
          go to 1500
        else if(LocateSubstring(Radka,'wavelength',.false.,.true.).gt.0)
     1    then
          k=0
          do i=1,3
            call kus(radka,k,cislo)
          enddo
          call StToReal(Radka,k,xp,1,.false.,ich)
          LamA1RefBlock(KRefBlock)=xp(1)
          LamA2RefBlock(KRefBlock)=xp(1)
          LamAveRefBlock(KRefBlock)=xp(1)
          go to 1500
        else if(LocateSubstring(Radka,'modulation vectors',.false.,
     1          .true.).gt.0) then
          n=0
1720      read(70,FormA,end=2000) Radka
          if(Radka.eq.' ') go to 1720
          k=0
          call kus(Radka,k,Cislo)
          if(.not.EqIgCase(Cislo(1:1),'q')) then
            backspace 70
            go to 1500
          endif
          Skrtat=.false.
          do 1760i=1,idel(Radka)
            if(Radka(i:i).eq.'(') then
              Skrtat=.true.
            else if(Radka(i:i).eq.')') then
              Skrtat=.false.
            else if(.not.Skrtat) then
              go to 1760
            endif
          Radka(i:i)=' '
1760      continue
          n=n+1
          NDim(KPhaseDR)=n+3
          NDimI(KPhaseDR)=n
          NDimQ(KPhaseDR)=(n+3)**2
          maxNDim=max(maxNDim,NDim(KPhaseDR))
          call StToReal(Radka,k,QuRefBlock(1,n,KRefBlock),3,.false.,ich)
          go to 1720
        else if(LocateSubstring(Radka,'Twin law for',.false.,.true.)
     1         .gt.0.and.HKLF5RefBlock(KRefBlock).eq.1) then
          i=LocateSubstring(Radka,'hkl(1)',.false.,.true.)
          if(i.le.0) go to 1500
          j=LocateSubstring(Radka(i+1:),'hkl',.false.,.true.)+i
          if(j.le.i) go to 1500
          k=j
          call Kus(Radka,k,Cislo)
          i=LocateSubstring(Cislo,'(',.false.,.true.)
          j=LocateSubstring(Cislo,')',.false.,.true.)
          if(i.ge.j) go to 1500
          Cislo=Cislo(i+1:j-1)
          k=0
          call StToInt(Cislo,k,ia,1,.false.,ich)
          if(ich.ne.0) go to 1500
          itw=ia(1)
          NTwin=max(itw,NTwin)
          m=1
          do i=1,3
            if(i.eq.1) then
              k=LocateSubstring(Radka,':',.false.,.true.)
            else
              read(70,FormA,end=2000) Radka
              k=0
            endif
            call StToReal(Radka,k,xp,3,.false.,ich)
            call CopyVek(xp,rtw(m,itw),3)
            m=m+3
          enddo
        endif
        go to 1500
2000    if(Vyskyt.gt.1) then
          write(Cislo,FormI15) Vyskyt
          call Zhusti(Cislo)
          Radka='Your sum file consists of '//Cislo(:idel(Cislo))//
     1          ' cell data blocks.'
          call FeChybne(-1.,-1.,Radka,'DATRED will use the last run.',
     1                  Warning)
        else if(Vyskyt.le.0) then
          Znovu=.true.
          go to 1150
        endif
        if(MameCell.le.0) call DRCellFromUB
      else if(TypeFile.eq.2) then
        MameCell=0
        NFaces(KRefBlock)=0
2500    read(70,FormA,end=3000) Radka
        if(Radka.eq.' ') go to 2500
        k=0
        call Kus(Radka,k,Cislo)
        if(EqIgCase(Cislo,'CELL')) then
          call StToReal(Radka,k,xp,7,.false.,ich)
          if(ich.ne.0) go to 3200
          LamA1RefBlock(KRefBlock)=xp(1)
          LamA2RefBlock(KRefBlock)=xp(1)
          LamAveRefBlock(KRefBlock)=xp(1)
          CellRefBlock(1:6,KRefBlock)=xp(2:7)
          MameCell=1
        else if(EqIgCase(Cislo,'ZERR')) then
          call Kus(Radka,k,Cislo)
          call StToReal(Radka,k,CellRefBlockSU(1,KRefBlock),6,.false.,
     1                  ich)
          if(ich.ne.0) go to 3200
        else if(EqIgCase(Cislo,'OMAX')) then
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 3200
          do j=1,3
            ub(1,j,KRefBlock)=xp(j)
          enddo
        else if(EqIgCase(Cislo,'OMAY')) then
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 3200
          do j=1,3
            ub(2,j,KRefBlock)=xp(j)
          enddo
        else if(EqIgCase(Cislo,'OMAZ')) then
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 3200
          do j=1,3
            ub(3,j,KRefBlock)=xp(j)
          enddo
        else if(EqIgCase(Cislo,'FACE')) then
          NFaces(KRefBlock)=NFaces(KRefBlock)+1
          call StToReal(Radka,k,DFace(1,NFaces(KRefBlock),KRefBlock),4,
     1                  .false.,ich)
          if(ich.ne.0) go to 3200
        endif
        go to 2500
3000    if(MameCell.le.0.or.ich.ne.0) then
          Radka='The "crs" file does not contains basic information'
          call FeChybne(-1.,YBottomMessage,Radka,'try again.',' ',
     1                  SeriousError)
          Znovu=.true.
          call CloseIfOpened(70)
          go to 1150
        else
          go to 6000
        endif
3200    call FeReadError(70)
      endif
      call DRBasicSHELXINoAbsCorr
      entry DRBasicNoniusCCD
      WizardModeIn=WizardMode
6000  FormatRefBlock(KRefBlock)='(.i4,2f8.2,i4,6f8.5,14x,f8.2)'
      call DRTestNDim(NDim95(KRefBlock))
      write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim95(KRefBlock)
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      call CloseIfOpened(70)
      if(LamAveRefBlock(KRefBlock).le.0.) then
        LamAveRefBlock(KRefBlock)=LamAveD(6)
        LamA1RefBlock(KRefBlock) =LamA1D(6)
        LamA2RefBlock(KRefBlock) =LamA2D(6)
        LamRatRefBlock(KRefBlock)=LamRat(6)
      endif
      call DRBasicSHELXINoAbsCorr
      go to 9999
9900  ErrFlag=1
9999  WizardMode=WizardModeIn
      return
      end
