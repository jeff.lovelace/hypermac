      subroutine DRCellFromUB
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ubt(9)
      call trmat(ub(1,1,KRefBlock),ubt,3,3)
      call multm(ubt,ub(1,1,KRefBlock),DRMetTensI,3,3,3)
      call matinv(DRMetTensI,DRMetTens,pom,3)
      do i=1,3
        CellRefBlock(i,KRefBlock)=sqrt(DRMetTens(i,i))
      enddo
      CellRefBlock(4,KRefBlock)=acos(DRMetTens(2,3)/
     1  (CellRefBlock(2,KRefBlock)*CellRefBlock(3,KRefBlock)))/ToRad
      CellRefBlock(5,KRefBlock)=acos(DRMetTens(1,3)/
     1  (CellRefBlock(1,KRefBlock)*CellRefBlock(3,KRefBlock)))/ToRad
      CellRefBlock(6,KRefBlock)=acos(DRMetTens(1,2)/
     1  (CellRefBlock(1,KRefBlock)*CellRefBlock(2,KRefBlock)))/ToRad
      return
      end
