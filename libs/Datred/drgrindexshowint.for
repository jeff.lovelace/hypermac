      subroutine DRGrIndexShowInt(XMinP,XMaxP,YMinP,YMaxP,ProjXI,FInt,
     1                            nf)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real FInt(*)
      integer ProjXI(*)
      call FeFillRectangle(XMinP-2.,XMaxP+2.,YMinP-2.,YMaxP+2.,4,0,0,
     1                     Blue)
      call FeFillRectangle(XMinP,XMaxP,YMinP,YMaxP,4,0,0,White)
      do i=1,nf+1
        xu(1)=ProjXI(i)/EnlargeFactor
        xu(2)=xu(1)
        yu(1)=YMinP
        yu(2)=YMinP+60.*sqrt(sqrt(FInt(i)))
        call FePolyLine(2,xu,yu,Black)
      enddo
      return
      end
