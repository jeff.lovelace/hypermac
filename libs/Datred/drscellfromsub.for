      subroutine DRSCellFromSUB(U,SU,SCell,Vol,SVol,G,SG)
      include 'fepc.cmn'
      dimension U(3,3),SU(3,3),SCell(6),AM(3,3),BM(3,3),CM(3,3),G(3,3),
     1          SG(3,3)
      call TrMat(U,AM,3,3)
      call multm(AM,U,G,3,3,3)
      do i=1,3
        do j=1,3
          AM(i,j)=U(i,j)**2
          BM(i,j)=SU(i,j)**2
        enddo
      enddo
      call TrMat(AM,CM,3,3)
      call multm(CM,BM,SG,3,3,3)
      call TrMat(BM,CM,3,3)
      call cultm(CM,AM,SG,3,3,3)
      do i=1,3
        do j=1,3
          SG(i,j)=sqrt(SG(i,j))
          if(i.eq.j) SG(i,i)=sqrt(2.)*SG(i,i)
        enddo
      enddo
      AR=sqrt(G(1,1))
      BR=sqrt(G(2,2))
      CR=sqrt(G(3,3))
      SA=SG(1,1)/(2.*AR)
      SB=SG(2,2)/(2.*BR)
      SC=SG(3,3)/(2.*CR)
      CosAlfaR=G(2,3)/(BR*CR)
      if(abs(G(2,3)).gt.0.) then
        pom=SG(2,3)/G(2,3)
      else
        pom=0.
      endif
      SCosAlfaR=abs(CosAlfaR)*sqrt(pom**2+(SB/BR)**2+(SC/CR)**2)
      SinAlfaR=sqrt(1.-CosAlfaR**2)
      CosBetaR=G(1,3)/(AR*CR)
      if(abs(G(1,3)).gt.0.) then
        pom=SG(1,3)/G(1,3)
      else
        pom=0.
      endif
      SCosBetaR=abs(CosBetaR)*sqrt(pom**2+(SA/AR)**2+(SC/CR)**2)
      SinBetaR=sqrt(1.-CosBetaR**2)
      CosGamaR=G(1,2)/(AR*BR)
      if(abs(G(1,2)).gt.0.) then
        pom=SG(1,2)/G(1,2)
      else
        pom=0.
      endif
      SCosGamaR=abs(CosGamaR)*sqrt(pom**2+(SA/AR)**2+(SB/BR)**2)
      SinGamaR=sqrt(1.-CosGamaR**2)
      CosinyAR=CosBetaR*CosGamaR-CosAlfaR
      CosinyBR=CosAlfaR*CosGamaR-CosBetaR
      CosinyGR=CosAlfaR*CosBetaR-CosGamaR
      PomQ=1.-CosAlfaR**2-CosBetaR**2-CosGamaR**2
     1      +2.*CosAlfaR*CosBetaR*CosGamaR
      SPomQ=2.*(CosAlfaR*SCosAlfaR+CosBetaR*SCosBetaR+
     1          CosGamaR*SCosGamaR+
     2          SCosAlfaR* CosBetaR* CosGamaR+
     3           CosAlfaR*SCosBetaR* CosGamaR+
     4           CosAlfaR* CosBetaR*SCosGamaR)
      Pom=sqrt(PomQ)
      SPom=.5/Pom*SPomQ
      VolR=AR*BR*CR*Pom
      SVolR=SA*BR*CR*Pom+AR*SB*CR*Pom+AR*BR*SC*Pom+AR*BR*CR*SPom
      Vol=1./VolR
      SVol=1./VolR**2*SVolR
      A=SinAlfaR/(Pom*AR)
      B=SinBetaR/(Pom*BR)
      C=SinGamaR/(Pom*CR)
      SCell(1)=A*sqrt((SA/AR)**2+(CosinyBR*SCosBetaR/PomQ)**2
     1                          +(CosinyGR*SCosGamaR/PomQ)**2
     2                          +(CosinyBR*CosinyGR*SCosAlfaR/
     3                           (PomQ*SinAlfaR)**2)**2)
      SCell(2)=B*sqrt((SB/BR)**2+(CosinyAR*SCosAlfaR/PomQ)**2
     1                          +(CosinyGR*SCosGamaR/PomQ)**2
     2                          +(CosinyAR*CosinyGR*SCosBetaR/
     3                           (PomQ*SinBetaR**2))**2)
      SCell(3)=C*sqrt((SC/CR)**2+(CosinyAR*SCosAlfaR/PomQ)**2
     1                          +(CosinyBR*SCosBetaR/PomQ)**2
     2                          +(CosinyAR*CosinyBR*SCosGamaR/
     3                           (PomQ*SinGamaR**2))**2)
      CosAlfa=CosinyAR/(SinBetaR*SinGamaR)
      CosBeta=CosinyBR/(SinAlfaR*SinGamaR)
      CosGama=CosinyGR/(SinAlfaR*SinBetaR)
      SCosAlfa=sqrt((CosinyGR*SCosBetaR/SinBetaR)**2+
     1              (CosinyBR*SCosGamaR/SinGamaR)**2+
     2              SCosAlfaR**2)
      SCosBeta=sqrt((CosinyGR*SCosAlfaR/SinAlfaR)**2+
     1              (CosinyAR*SCosGamaR/SinGamaR)**2+
     2              SCosBetaR**2)
      SCosGama=sqrt((CosinyBR*SCosAlfaR/SinAlfaR)**2+
     1              (CosinyAR*SCosBetaR/SinBetaR)**2+
     2              SCosGamaR**2)
      SCell(4)=SCosAlfa/(ToRad*sqrt(1-CosAlfa**2))
      SCell(5)=SCosBeta/(ToRad*sqrt(1-CosBeta**2))
      SCell(6)=SCosGama/(ToRad*sqrt(1-CosGama**2))
      return
      end
