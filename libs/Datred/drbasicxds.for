      subroutine DRBasicXDS
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Radka
      logical EqIgCase
      IStav=0
1000  read(71,FormA80,end=9000) Radka
      if(EqIgCase(radka(1:14),'!END_OF_HEADER')) go to 9999
      k=0
      call kus(Radka,k,Cislo)
      if(EqIgCase(Radka(1:18),'!X-RAY_WAVELENGTH=')) then
        call StToReal(Radka,k,LamAveRefBlock(KRefBlock),1,.false.,ich)
      else if(EqIgCase(Radka(1:21),'!UNIT_CELL_CONSTANTS=')) then
        call StToReal(Radka,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
        if(ich.ne.0) go to 9100
      else if(EqIgCase(Radka(1:18),'!UNIT_CELL_A-AXIS=')) then
        IStav=IStav+1
        call StToReal(Radka,k,UB(1,1,KRefBlock),3,.false.,ich)
        if(ich.ne.0) go to 9100
      else if(EqIgCase(Radka(1:18),'!UNIT_CELL_B-AXIS=')) then
        IStav=IStav+10
        call StToReal(Radka,k,UB(1,2,KRefBlock),3,.false.,ich)
        if(ich.ne.0) go to 9100
      else if(EqIgCase(Radka(1:18),'!UNIT_CELL_C-AXIS=')) then
        IStav=IStav+100
        call StToReal(Radka,k,UB(1,3,KRefBlock),3,.false.,ich)
        if(ich.ne.0) go to 9100
      endif
      go to 1000
9000  call FeChybne(-1.,-1.,'XDS - the keyword END_OF_HEADER not '//
     1              'found.','File probably corrupted.',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  ErrFlag=1
9999  if(ErrFlag.eq.0.and.IStav.eq.111) then
        call TrMat(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),3,3)
        call MatInv(UBI(1,1,KRefBlock),UB(1,1,KRefBlock),pom,3)
        PocitatDirCos=.true.
        DirCosFromPsi=.true.
      else
        PocitatDirCos=.false.
        DirCosFromPsi=.false.
      endif
      return
      end
