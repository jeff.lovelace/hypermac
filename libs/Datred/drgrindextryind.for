      subroutine DRGrIndexTryInd
      use Basic_mod
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3),hp(3),ZlomekZIndexu(3),IU(4)
      logical EqIV,EqIV0
      IndexedSel=0
      IndexedAll=0
      IndexedAllSat=0
      IndexedSelSat=0
      nBasVec=3
      do i=1,3
        do j=1,4
          if(i.eq.j.or.j.eq.4) then
            BasVec(j,i)=1
          else
            BasVec(j,i)=0
          endif
        enddo
        BasVecFlag(i)=.true.
        MBasVec(i)=-1
      enddo
      if(NDimI(KPhase).gt.0)
     1  call CopyVek(QuRefBlock(1,1,KRefBlock),Qu(1,1,1,KPhase),
     2               3*NDimI(KPhase))
      GrIndexNSel=0
      do 1200i=1,GrIndexNAll
        if(FlagU(i).le.0) cycle
        call multm(UBI(1,1,KRefBlock),Slozky(1,i),ha(1,i),3,3,1)
        call ChngInd(ha(1,i),iha(1,i),1,hp,MMin,MMax,1,CheckExtRefNo)
        call multm(UB(1,1,KRefBlock),hp,xp,3,3,1)
        if(VecOrtLen(xp,3).lt.DiffMax) then
          FlagI(i)=1
          IndexedAll=IndexedAll+1
          if(FlagS(i).ne.0) IndexedSel=IndexedSel+1
          if(NDimI(KPhase).gt.0) then
            if(.not.EqIV0(iha(4,i),NDimI(KPhase))) then
              IndexedAllSat=IndexedAllSat+1
              if(FlagS(i).ne.0) IndexedSelSat=IndexedSelSat+1
            endif
          endif
        else
          FlagI(i)=0
        endif
        if(FlagS(i).eq.0) go to 1200
        GrIndexNSel=GrIndexNSel+1
        MinMult=0
        do k=1,5
          fk=k
          do j=1,3
            hp(j)=anint(ha(j,i)*fk)/fk-ha(j,i)
          enddo
          call multm(UB(1,1,KRefBlock),hp,xp,3,3,1)
          if(VecOrtLen(xp,3).lt.DiffMax) then
            MinMult=k
            exit
          endif
        enddo
        if(MinMult.le.0) go to 1200
        do j=1,3
          ZlomekZIndexu(j)=anint(ha(j,i)*fk)
        enddo
        do j=1,MinMult-1
          do k=1,3
            IU(k)=ZlomekZIndexu(k)*j
          enddo
          IU(4)=MinMult
          do k=1,3
1140        if(IU(k).ge.MinMult) then
              IU(k)=IU(k)-MinMult
              go to 1140
            endif
1160        if(IU(k).lt.0) then
              IU(k)=IU(k)+MinMult
              go to 1160
            endif
          enddo
          call MinMultMaxFract(IU,4,k,MaxFract)
          do k=1,4
            IU(k)=IU(k)/MaxFract
          enddo
          call MinMultMaxFract(IU,3,k,MaxFract)
          do k=1,3
            IU(k)=IU(k)/MaxFract
          enddo
          do k=1,nBasVec
            if(eqiv(IU,BasVec(1,k),4)) then
              MBasVec(k)=MBasVec(k)-1
              go to 1200
            endif
          enddo
          if(nBasVec.lt.22) then
            nBasVec=nBasVec+1
            call CopyVekI(IU,BasVec(1,nBasVec),4)
            MBasVec(nBasVec)=-1
          endif
        enddo
1200  continue
      return
      end
