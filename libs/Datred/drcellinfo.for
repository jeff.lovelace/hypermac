      subroutine DRCellInfo(CellA,Veta)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension CellA(6)
      character*(*) Veta
      csa=cos(Cella(4)*ToRad)
      csb=cos(Cella(5)*ToRad)
      csg=cos(Cella(6)*ToRad)
      Vol=Cella(1)*Cella(2)*Cella(3)*
     1  sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      write(Veta,'(3f8.4,3f8.3,f10.1)')(CellA(i),i=1,6),Vol
      return
      end
