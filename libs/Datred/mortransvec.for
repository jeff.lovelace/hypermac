      subroutine MorTransVec(Recip,Vektor)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical Recip
      dimension Vektor(3),pomv(3)
      if(Recip) then
        call multm(MetTensI(1,1,KPhase),Vektor,pomv,3,3,1)
      else
        call CopyVek(Vektor,pomv,3)
      endif
      call multm(TrToOrtho(1,1,KPhase),pomv,Vektor,3,3,1)
      call VecOrtNorm(Vektor,3)
      return
      end
