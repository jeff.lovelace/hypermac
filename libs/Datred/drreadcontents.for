      subroutine DRReadContents(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*72 formulao
      Formulao=Formula(KPhase)
      ich=0
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,160.,2,'Cell contents',0,
     1                   LightGray,0,0)
      call FeQuestEdwMake(id,5.,1,60.,1,'%Formula','L',95.,EdwYd,1)
      nEdwFormula=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,Formula(KPhase))
      call FeQuestEdwMake(id,5.,2,60.,2,'Formula %units','L',20.,EdwYd,
     1                    0)
      nEdwZ=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NUnits(KPhase),.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFormula) then
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        call PitFor(0,ich)
        if(ich.eq.0) go to 1500
        EventType=EventEdw
        EventNumber=nEdwFormula
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
      call FeQuestRemove(id)
      if(ich.ne.0) Formula(KPhase)=Formulao
9900  return
      end
