      subroutine MorUzavreny(FacesNotOK,ctipl)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      logical FacesNotOK,ctipl
      character*50 t50
      if(NFaces(KRefBlock)+PocetBodu.ne.PocetHran+2) then
        write(t50,'(''Number of faces : '',i3,'', points : '',i3,
     1              '', edges'',i3 )') NFaces(KRefBlock),PocetBodu,
     2                                 PocetHran
        call FeChybne(-1.,-1.,'wrong crystal shape.',t50,SeriousError)
        FacesNotOK=.true.
        ctipl=.true.
      endif
      return
      end
