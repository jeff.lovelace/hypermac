      subroutine DRBrukerMul
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 FileIn,t256,CurrentDirO,ActualDir
      character*80 flno,Label
      dimension ih(3),ihp(3),iho(3)
      integer FeChdir
      logical EqIV
      data FileIn,ActualDir/2*' '/,ih/3*0/
      CurrentDirO=CurrentDir
      flno=fln
      if(ActualDir.ne.' ') then
        i=FeChdir(ActualDir)
        call FeGetCurrentDir
      endif
      call FeFileManager('Select the *.mul file',FileIn,'*.mul',0,
     1                   .false.,ich)
      if(ich.ne.0) go to 9999
      ActualDir=CurrentDir
      ln=NextLogicNumber()
      call OpenFile(ln,FileIn,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      IUnderScore=index(FileIn,'_')
      if(IUnderScore.le.0) IUnderScore=index(FileIn,'.')
      if(IUnderScore.le.0) IUnderScore=idel(FileIn)
      call OpenFile(lst,'mul.lst','formatted','unknown')
      id=NextQuestId()
      xqd=200.
      yqd=30.+9.*6.
      call FeQuestAbsCreate(id,-1.,-1.,xqd,yqd,' ',0,LightGray,0,-1)
      tpom=5.
      t256='%Select indices'
      xpom=tpom+FeTxLengthUnder(t256)+3.
      dpom=50.
      ypom=yqd-8.
      call FeQuestAbsEdwMake(id,tpom,ypom,xpom,ypom-3.,t256,'L',dpom,
     1                       EdwYd,0)
      nEdwInd=EdwLastMade
      call FeQuestIntAEdwOpen(EdwLastMade,ih,3,.true.)
      call FeQuestAbsLblMake(id,xpom,ypom,' ','L','N')
      call FeQuestLblOff(LblLastMade)
      nLblInd=LblLastMade
      t256='%Find'
      dpom=40.
      xpom=xpom+dpom+40.
      call FeQuestAbsButtonMake(id,xpom,ypom-3.,dpom,ButYd,t256)
      nButtFind=ButtonLastMade
      call FeQuestButtonOpen(nButtFind,ButtonOff)
      ypom=ypom-8.
      call FeQuestAbsLinkaMake(id,ypom)
      ypomp=ypom
      do i=1,6
        ypom=ypom-8.
        TextInfo(i)=' '
        call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(i),'L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblInfoFirst=LblLastMade
      enddo
      ypom=ypom-10.
      xpom=5.
1500  call FeQuestEvent(id,ich)
1550  if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtFind) then
        call FeQuestIntAFromEdw(nEdwInd,ih)
        call FeQuestEdwClose(nEdwInd)
        write(Cislo,'(3i4)') ih
        Label='Indices to search for: '//Cislo(:idel(Cislo))
        ypom=yqd-8.
        call FeQuestLblChange(nLblInd,Label)
        iho(1)=999
        rewind ln
2000    read(ln,FormA,end=2200) t256
        if(t256(1:1).eq.'!') go to 2000
        read(t256,100) ihp,ri,rs,IBatch,xo,yo,zo,xp,yp,zp,swing,Ang1,
     1                 irt,Chi,Ang2,IComp
        if(.not.eqiv(ih,ihp,3)) then
          if(IComp.lt.0) then
            call CopyVekI(ihp,iho,3)
            rio=ri
            rso=rs
            ICompO=IComp
          else
            iho(1)=999
          endif
          go to 2000
        endif
        if(IComp.lt.0) then
          read(ln,100,end=2200) iho,rio,rso,i,(pom,i=1,8),i,pom,pom,
     1                          ICompO
        endif
        if(irt.eq.2) then
          Omega=Ang1
          Phi=Ang2
          Cislo='omega'
        else
          Omega=Ang2
          Phi=Ang1
          Cislo='phi'
        endif
        write(TextInfo(1),'(''Reflection:'',3i4,2f10.2,1x,a,i3)')
     1    ihp,ri,rs,Cislo(:idel(Cislo)),IComp
        if(iho(1).lt.900) then
          write(t256,'(3i4,2f10.2,1x,a,i3)')
     1      iho,rio,rso,Cislo(:idel(Cislo)),ICompO
          iho(1)=999
        else
          t256=' --- --- ---'
        endif
        write(TextInfo(2),'(''Ovelaped  :'',a)') t256(:idel(t256))
        write(TextInfo(3),'(''Angles    :'',4f9.3)')
     1    swing,Omega,Phi,Chi
        write(TextInfo(4),'(''Observed  :'',3f9.3,i4)') xo,yo,zo,ibatch
        write(TextInfo(5),'(''Predicted :'',3f9.3)') xp,yp,zp
        write(t256,'(''_'',i2,''_'',i4,''.sfrm'')') IBatch,nint(zo)
        do i=1,idel(t256)
          if(t256(i:i).eq.' ') t256(i:i)='0'
        enddo
        t256=FileIn(:IUnderScore-1)//t256(:idel(t256))
        write(TextInfo(6),'(''File      :'',a)') t256(:idel(t256))
        nLbl=nLblInfoFirst
        do i=1,6
          call FeQuestLblChange(nLbl,TextInfo(i))
          nLbl=nLbl+1
        enddo
        call FeQuestButtonOff(nButtFind)
        call FeQuestButtonLabelChange(nButtFind,'%Find next')
2150    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtFind) then
          go to 2000
        else if(CheckNumber.ne.0) then
          go to 2150
        endif
        go to 2300
2200    call FeQuestButtonOff(nButtFind)
        call FeQuestButtonLabelChange(nButtFind,'%Find')
        call FeQuestLblChange(nLbl,' ')
        call FeQuestIntAEdwOpen(nEdwInd,ih,3,.true.)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
2300  call FeQuestRemove(id)
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      fln=flno
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lst)
      return
100   format(3i4,2f8.0,i4,51x,2(2f7.2,f8.2),18x,2f7.2,i2,74x,2f8.3,i4)
      end
