      subroutine DRGrIndexFindCell(ich)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta
      integer, allocatable :: Por(:),FlagUOld(:),FlagIOld(:),ReflUsed(:)
      real, allocatable :: SlozkyP(:,:)
      logical, allocatable :: AlreadyIndexed(:,:)
      integer ik(2),SkipFlag(20)
      real TrPom(36),TrPomI(36),Cell(6),U(3,3),Ui(3,3),T(9),Tt(9),
     1     Ti(3,3),TQ(9)
      logical FeYesNo
      if(allocated(Por)) deallocate(Por,FlagUOld,FlagIOld)
      allocate(Por(GrIndexNAll),FlagUOld(GrIndexNAll),
     1                          FlagIOld(GrIndexNAll))
      ich=0
      NDimIn=NDim(KPhase)
      NDim(KPhase)=3
      NTriplIn=NTripl
      NRefForIndexIn=NRefForIndex
      NRefForTriplIn=NRefForTripl
      VolAMinIn=VolAMin
      VolMaxIn=VolMax
      DiffMaxIn=DiffMax
      do i=1,GrIndexNAll
        Int(i)=-Int(i)
      enddo
      call rindexx(GrIndexNAll,Int,Por)
      do i=1,GrIndexNAll
        Int(i)=-Int(i)
        FlagUOld(i)=FlagU(i)
        FlagIOld(i)=FlagI(i)
      enddo
      call DRGrIndexFindCellOpt(VolMax,VolAMin,DiffMax,NRefForIndex,
     1                          NRefForTripl,ich)
      if(ich.ne.0) go to 9999
      FlagU=0
      do i=1,min(NRefForIndex,GrIndexNAll)
        ii=Por(i)
        FlagU(ii)=max(FlagU(ii),1)
      enddo
      if(allocated(SlozkyP)) deallocate(SlozkyP,ReflUsed)
      allocate(SlozkyP(4,NRefForTripl),ReflUsed(NRefForTripl))
      n=0
      do 1100i=1,GrIndexNAll
        ip=Por(i)
        if(FlagS(ip).le.0) cycle
        AA=VecOrtLen(Slozky(1,ip),3)
        do jp=1,n
          BB=VecOrtLen(SlozkyP(1,jp),3)
          CosGama=min(VecOrtScal(Slozky(1,ip),SlozkyP(1,jp),3)/(AA*BB),
     1                1.)
          if(abs(CosGama).gt..99) then
            if(SlozkyP(4,jp).gt.Slozky(4,ip)) then
              SlozkyP(1:4,jp)=Slozky(1:4,ip)
            endif
            go to 1100
          endif
        enddo
        n=n+1
        SlozkyP(1:4,n)=Slozky(1:4,ip)
        ReflUsed(n)=ip
        if(n.ge.NRefForTripl) go to 1200
1100  continue
      NRefForTripl=n
1200  VMax=0.
      nn=(NRefForTripl*(NRefForTripl-1)*(NRefForTripl-2))/6
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nn)*.005),10),nn,
     1                     'Search for optimal triplets of '//
     2                     'diffraction vectors','End',' ')
      iz=0
      NTripl=0
      if(Allocated(AlreadyIndexed)) deallocate(AlreadyIndexed)
      allocate(AlreadyIndexed(NRefForTripl,20))
      do ii=3,NRefForTripl
        AA=VecOrtLen(SlozkyP(1,ii),3)
        do jj=1,ii-1
          BB=VecOrtLen(SlozkyP(1,jj),3)
          CosGama=VecOrtScal(SlozkyP(1,ii),SlozkyP(1,jj),3)/(AA*BB)
          do 2000kk=1,jj-1
            do i=1,NTripl
              if(AlreadyIndexed(ii,i).and.
     1           AlreadyIndexed(jj,i).and.
     1           AlreadyIndexed(kk,i)) then
                   call FeFlowChartEvent(iz,is)
                   go to 2000
              endif
            enddo
            call FeFlowChartEvent(iz,is)
            if(is.ne.0) then
              if(is.eq.1) then
                if(FeYesNo(-1.,120.,'Do you really want to end '//
     1                     'the search?',0)) go to 2100
              else if(is.eq.3) then
                if(FeYesNo(-1.,120.,'Do you really want to cancel '//
     1                     'the search?',0)) then
                  call FeFlowChartRemove
                  go to 9900
                endif
              endif
            endif
            CC=VecOrtLen(SlozkyP(1,kk),3)
            CosAlfa=VecOrtScal(SlozkyP(1,jj),SlozkyP(1,kk),3)/(BB*CC)
            CosBeta=VecOrtScal(SlozkyP(1,ii),SlozkyP(1,kk),3)/(AA*CC)
            VolA=1.-CosAlfa**2-CosBeta**2-CosGama**2
     1            +2.*CosAlfa*CosBeta*CosGama
            VolA=sqrt(max(VolA,0.))
            if(VolA.lt.VolAMin) go to 2000
            VolP=1./(VolA*AA*BB*CC)
            if(VolP.gt.VolMax) cycle
            call CopyVek(SlozkyP(1,ii),UB(1,1,KRefBlock),3)
            call CopyVek(SlozkyP(1,jj),UB(1,2,KRefBlock),3)
            call CopyVek(SlozkyP(1,kk),UB(1,3,KRefBlock),3)
            call Matinv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
            Vol=1./Vol
            if(Vol.lt.0.) then
              do i=1,3
                UB(i,3,KRefBlock)=-UB(i,3,KRefBlock)
              enddo
              Vol=-Vol
            endif
            call DRCellFromUB
            call UnitMat(T,3)
            call DRCellReduction(CellRefBlock(1,KRefBlock),T)
            call TrMat(T,Tt,3,3)
            call MatInv(Tt,T,det,3)
            call CopyMat(UB(1,1,KRefBlock),TrPom,3)
            call MultM(TrPom,T,UB(1,1,KRefBlock),3,3,3)
            call DRCellFromUB
            call Matinv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
            Vol=1./Vol
            do 1500i=1,NTripl
              call MultM(UBITripl(1,1,i),UB(1,1,KRefBlock),TrPom,3,3,3)
              do j=1,9
                if(abs(TrPom(j)-anint(TrPom(j))).gt..01) go to 1500
              enddo
              go to 2000
1500        continue
            call DRGrIndexTryInd
            call DRGrIndexFindCellRefine(0,Vol,ich)
            if(ich.ne.0) go to 2000
            if(NTripl.lt.20) then
              NTripl=NTripl+1
              MTripl=NTripl
            else
              if(Vol.lt.VTripl(MTripl))
     1          go to 2000
            endif
            AlreadyIndexed(1:NRefForTripl,MTripl)=.false.
            do i=1,GrIndexNAll
              if(FlagI(i).eq.0) cycle
              do j=1,NRefForTripl
                if(ReflUsed(j).eq.i) then
                  AlreadyIndexed(j,MTripl)=.true.
                endif
              enddo
            enddo
            call CopyMat(UB (1,1,KRefBlock),UBTripl (1,1,MTripl),3)
            call CopyMat(UBI(1,1,KRefBlock),UBITripl(1,1,MTripl),3)
            call DRCellFromUB
            call CopyVek(CellRefBlock(1:6,KRefBlock),
     1                   CellTripl(1,MTripl),6)
            NIndSelTripl(MTripl)=IndexedSel
            VTripl(MTripl)=Vol
            do i=1,NTripl
              VTripl(i)=-VTripl(i)
            enddo
            call RIndexx(NTripl,VTripl,PorTripl)
            do i=1,NTripl
              VTripl(i)=-VTripl(i)
            enddo

            MTripl=PorTripl(NTripl)
2000      continue
        enddo
      enddo
2100  call FeFlowChartRemove
      do ITripl=1,NTripl
        MTripl=PorTripl(ITripl)
        call CopyMat(UBTripl (1,1,MTripl),UB (1,1,KRefBlock),3)
        call CopyMat(UBITripl(1,1,MTripl),UBI(1,1,KRefBlock),3)
        Vol=VTripl(MTripl)
        call DRGrIndexTryInd
2200    if(NBasVec.le.3) go to 2500
        call indexx(nBasVec-3,MBasVec(4),OBasVec(4))
        do i=4,nBasVec
          OBasVec(i)=OBasVec(i)+3
        enddo
        m=-MBasVec(OBasVec(4))
        if(m.lt.nint(float(IndexedAll)*.05)) go to 2500
        do i=4,4
          ii=OBasVec(i)
          Veta='('
          kk=2
          do j=1,3
            ik(1)=BasVec(j,ii)
            ik(2)=BasVec(4,ii)
            call MinMultMaxFract(ik,2,k,MaxFract)
            do k=1,2
              ik(k)=ik(k)/MaxFract
            enddo
            if(ik(1).eq.0) then
              Veta(kk:kk+1)='0,'
            else if(ik(1).eq.1.and.ik(2).eq.1) then
              Veta(kk:kk+1)='1,'
            else
              write(Veta(kk:),'(i3,''/'',i3,'','')') ik
            endif
            call Zhusti(Veta)
            kk=idel(Veta)
            if(j.eq.3) then
              Veta(kk:kk)=')'
            else
              kk=kk+1
            endif
          enddo
        enddo
        BasVecFlag(1:3)=.true.
        BasVecFlag(4:nBasVec)=.false.
        ii=OBasVec(4)
        BasVecFlag(ii)=.true.
        IVolMax=0
        do i=1,nBasVec-2
          if(.not.BasVecFlag(i)) cycle
          do n=1,3
            Ti(n,1)=float(BasVec(n,i))/float(BasVec(4,i))
          enddo
          do j=i+1,nBasVec-1
            if(.not.BasVecFlag(j)) cycle
            do n=1,3
              Ti(n,2)=float(BasVec(n,j))/float(BasVec(4,j))
            enddo
            do k=j+1,nBasVec
              if(.not.BasVecFlag(k)) cycle
              do n=1,3
                Ti(n,3)=float(BasVec(n,k))/float(BasVec(4,k))
              enddo
              call MatInv(Ti,Tt,VolP,3)
              if(abs(VolP).lt..0001) cycle
              IVol=nint(1./VolP)
              if(IVol.lt.0) then
                IVol=-IVol
                call RealVectorToOpposite(Ti(1,3),Ti(1,3),3)
              endif
              if(IVol.ge.IVolMax) then
                call CopyMat(Ti,T,3)
                IVolMax=IVol
              endif
            enddo
          enddo
        enddo
        call MatInv(T,Tt,Det,3)
        if(Vol/Det.gt.VolMax) go to 2500
        call CopyMat(UB(1,1,KRefBlock),TrPom,3)
        call MultM(TrPom,T,UB(1,1,KRefBlock),3,3,3)
        call DRCellFromUB
        call UnitMat(T,3)
        call DRCellReduction(CellRefBlock(1,KRefBlock),T)
        call TrMat(T,Tt,3,3)
        call MatInv(Tt,T,det,3)
        call CopyMat(UB(1,1,KRefBlock),TrPom,3)
        call MultM(TrPom,T,UB(1,1,KRefBlock),3,3,3)
        call DRCellFromUB
        call Matinv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
        Vol=1./Vol
        call DRGrIndexTryInd
        call DRGrIndexFindCellRefine(1,Vol,ich)
        if(ich.eq.0.and.Vol.lt.VolMax) go to 2200
2500    call CopyMat(UB (1,1,KRefBlock),UBTripl (1,1,MTripl),3)
        call CopyMat(UBI(1,1,KRefBlock),UBITripl(1,1,MTripl),3)
        call DRCellFromUB
        call CopyVek(CellRefBlock(1,KRefBlock),CellTripl(1,MTripl),6)
        NIndSelTripl(MTripl)=IndexedSel
        VTripl(MTripl)=Vol
      enddo
      if(Ntripl.le.0) then
          TextInfo(1)='Sorry, no reasonable cell has been found.'
          TextInfo(2)='Please modify the cell searching parameters '//
     1                'and try again.'
          Ninfo=2
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 9900
      endif
      do i=1,NTripl
        VTripl(i)=-VTripl(i)
      enddo
      call RIndexx(NTripl,VTripl,PorTripl)
      do i=1,NTripl
        VTripl(i)=-VTripl(i)
      enddo
      SkipFlag=0
      do i=1,NTripl-1
        ii=PorTripl(i)
        if(SkipFlag(i).ne.0) cycle
        do 2600j=i+1,NTripl
          jj=PorTripl(j)
          if(SkipFlag(i).ne.0) cycle
          call MultM(UBITripl(1,1,jj),UBTripl(1,1,ii),TrPom,3,3,3)
          do k=1,9
            if(abs(TrPom(k)-anint(TrPom(k))).gt..01) go to 2600
          enddo
          SkipFlag(jj)=1
2600    continue
      enddo
      n=0
      do i=1,NTripl
        if(SkipFlag(i).ne.0) cycle
        n=n+1
        call CopyMat(UBTripl (1,1,i),UBTripl (1,1,n),3)
        call CopyMat(UBITripl(1,1,i),UBITripl(1,1,n),3)
        call CopyVek(CellTripl(1,i),CellTripl(1,n),6)
        NIndSelTripl(n)=NIndSelTripl(i)
        VTripl(n)=VTripl(i)
      enddo
      NTripl=n
      do i=1,NTripl
        VTripl(i)=-float(NIndSelTripl(i))/VTripl(i)
      enddo
      call RIndexx(NTripl,VTripl,PorTripl)
      do i=1,NTripl
        VTripl(i)=-float(NIndSelTripl(i))/VTripl(i)
      enddo
      call DRGrIndexFindCellSelect(ich)
      if(ich.ne.0) go to 9999
      if(FileIndex.ne.' ')
     1  call DRGrIndexIOSmr(1,FileIndex(:idel(FileIndex))//'.smr')
      call CopyMat(UBTripl(1,1,MTripl),UB(1,1,KRefBlock),3)
      call CopyMat(UBITripl(1,1,MTripl),UBI(1,1,KRefBlock),3)
      call CopyVek(CellTripl(1,MTripl),CellRefBlock(1,KRefBlock),6)
      do i=1,3
        do j=1,3
          CellDir(j,i)=UBI(i,j,KRefBlock)/CellRefBlock(i,KRefBlock)
        enddo
      enddo
      ich=0
      go to 9999
9900  ich=1
9999  if(ich.ne.0) then
        NRefForIndex=NRefForIndexIn
        NRefForTripl=NRefForTriplIn
        VolAMin=VolAMinIn
        VolMax=VolMaxIn
        DiffMax=DiffMaxIn
      endif
      do i=1,GrIndexNAll
        FlagU(i)=FlagUOld(i)
        FlagI(i)=FlagIOld(i)
      enddo
      if(allocated(Por)) deallocate(Por,FlagUOld,FlagIOld)
      if(allocated(SlozkyP)) deallocate(SlozkyP,ReflUsed)
      NDim(KPhase)=NDimIn
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      return
      end
