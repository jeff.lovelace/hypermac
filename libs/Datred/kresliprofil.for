      subroutine KresliProfil(Text,xomn,xomx,yomn,yomx,Message)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'profil.cmn'
      integer Color
      character*(*) Text,Message
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeOutSt(0,XCenAcWin,(YMaxAcWin+YMaxGrWin)*.5,Text,'C',
     1               White)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Omega')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'I')
        call FeXYPlot(px,py(1,2),nprof,NormalLine,NormalPlotMode,Green)
        call FeXYPlot(px,py,nprof,NormalLine,NormalPlotMode,White)
        xu(1)=XMinAcWin
        yu(1)=FeYo2Y(trshld)
        xu(2)=XMaxAcWin
        yu(2)=yu(1)
        call FeLineType(DashedLine)
        if(yu(1).le.YMaxAcWin) call FePolyline(2,xu,yu,Red)
        yu(1)=YMinAcWin
        xu(1)=FeXo2X(xteziste)
        yu(2)=YMaxAcWin
        xu(2)=xu(1)
        call FePolyline(2,xu,yu,Red)
        call FeLineType(NormalLine)
        do i=1,2
          call bpb(rip(i),rsp(i),i,NBckg)
          write(StBPB(i),101) nint(rip(i)),nint(rsp(i))
          call Zhusti(StBPB(i))
          call FeRewriteString(0,xbpb(i),ybpb,StBPBo(i),StBPB(i),'C',
     1                         LightGray,Black)
          StBPBo(i)=StBPB(i)
        enddo
        call FeOutSt(0,xmsg,ybpb,Message,'C',Black)
        do i=1,nprof
          pxu(i)=FeXo2X(px(i))
          pyu(i,1)=FeYo2Y(py(i,1))
          pyu(i,2)=FeYo2Y(py(i,2))
          call FeCircle(pxu(i),pyu(i,2),1.2,Green)
          call FeCircle(pxu(i),pyu(i,1),1.2,White)
        enddo
        ypom=YMinAcWin-1.
        yu(1)=ypom
        yu(2)=ypom-3.
        yu(3)=ypom-3.
        do 2400i=1,2
          if(OmZnak(i).lt.px(1).or.OmZnak(i).gt.px(nprof)) go to 2400
          xpom=FeXo2X(OmZnak(i))
          xu(1)=xpom
          xu(2)=xpom-3.
          xu(3)=xpom+3.
          if(i.eq.2) then
            Color=Red
          else if(i.eq.4) then
            Color=Green
          else
            Color=Yellow
          endif
          call FePolygon(xu,yu,3,4,0,0,Color)
2400    continue
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
9000  return
101   format(i10,'(',i10,')')
      end
