      subroutine KOTArea(KO,KOv,A)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer A(4)
      if(A(2).lt.LoLimKuma(2)) then
        KO=-8
      else if(A(2).gt.UpLimKuma(2)) then
        KO=-7
      else if(A(3).lt.LoLimKuma(3)) then
        KO=-4
      else if(A(3).gt.UpLimKuma(3)) then
        KO=-3
      else if(A(1).lt.LoLimKuma(1)) then
        KO=-6
      else if(A(1).gt.UpLimKuma(1)) then
        KO=-5
      else
        call KOArea(KO,KOv,A(3),A(1))
        if(abs(KO).gt.2) KOv=15
      endif
      return
      end
