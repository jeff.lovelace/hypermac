      subroutine DRGrIndexRefine(ich)
      use Basic_mod
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension hp(6)
      character*256 Veta
      character*80 TextInfo1
      character*1 ZnakI,ZnakS
      real UBStore(3,3,10),T(3,3),Ti(3,3),CellSUStore(6,10),
     1     QuStore(3,3,10),QuSStore(3,3,10),T6(36),xp(3),Tt(3,3),
     2     AM(120),AMI(120),PS(18),Ugen(18),SUgen(18),Uq(9),SUq(9),
     3     Uqp(9),SUqp(9),UBSU(3,3),DRMetTensSU(3,3),UBIn(3,3),TQ(3,3),
     4     cpp(6),rcpp(6),der(15),CellD(15),ddrdgi(9),ddrdrcp(6),
     5     gi(3,3),VolStore(10),CellParRestR(6)
      integer, allocatable :: FlagIOld(:)
      integer SbwLnQuest,UseTabsIn,UseTabsM,UseTabsC,SbwItemSelQuest,
     1        SbwItemPointerQuest,UseTabsPom,FeMenu,ik(2),CrSystemPom,
     2        EdwStateQuest
      integer :: FixedCell=9
      logical eqiv,CrwLogicQuest,lpom
      if(index2d) then
        nd=2
        ndo=2
        ndi=0
        nt=8
      else
        nd=NDim(KPhase)
        ndo=3
        ndi=NDimI(KPhase)
        nt=9
      endif
      call CopyMat(UB(1,1,KRefBlock),UBIn,3)
      call DRCellFromUB
      call Matinv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),CellVolume,3)
      if(CellVolume.lt.0.) then
        call RealMatrixToOpposite(UB(1,1,KRefBlock),UB(1,1,KRefBlock),
     1                            3)
        call RealMatrixToOpposite(UBI(1,1,KRefBlock),UBI(1,1,KRefBlock),
     1                            3)
        CellVolume=-CellVolume
      endif
      CellVolume=1./CellVolume
      UseTabsIn=UseTabs
      xqd =650.
      xqdp=xqd*.5
      ild=21
      if(ndi.gt.0) then
        ild=ild+1
        nt=nt+2
      endif
      Veta='Refine cell parameters'
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,ild,Veta,0,LightGray,0,0)
      il=1
      xpom=5.
      do i=1,6
        ZnakI='C'
        if(i.eq.1) then
          Veta='No.'
          ZnakI='R'
          xpom=xpom+FixFontWidthInPixels*6./EnlargeFactor
        else if(i.le.4) then
          Veta=Indices(i-1)
          if(i.eq.2) then
            xpom=xpom+FixFontWidthInPixels*5./EnlargeFactor
          else
            xpom=xpom+FixFontWidthInPixels*8./EnlargeFactor
          endif
        else if(i.eq.5) then
          Veta='Indexed'
          xpom=xpom+FixFontWidthInPixels*8./EnlargeFactor
        else if(i.eq.6) then
          Veta='Selected'
          xpom=xpom+FixFontWidthInPixels*7./EnlargeFactor
        endif
        dx=0.
        do j=1,2
          call FeQuestLblMake(id,xpom+dx,il,Veta,ZnakI,'N')
          dx=xqd*.5-5.
        enddo
      enddo
      dpom=110.
      space=12.
      fnb=5.
      xpom=xqdp-fnb*dpom*.5-(fnb-1.)*space*.5
      xpomp=xpom
      il=ild-2
      Veta='Red%uce cell'
      do i=1,nint(fnb)
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(Index2d) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.1) then
          nButtReduce=ButtonLastMade
          Veta='Go to %supercell'
        else if(i.eq.2) then
          nButtSuper=ButtonLastMade
          Veta='Go to %doublecells'
        else if(i.eq.3) then
          nButtDouble=ButtonLastMade
          Veta='Transform by %matrix'
        else if(i.eq.4) then
          Veta='%By a special matrix'
          nButtMatrix=ButtonLastMade
        else if(i.eq.5) then
          nButtMatrixSpecial=ButtonLastMade
        endif
        xpom=xpom+dpom+space
      enddo
      il=il+1
      Veta='D%efine symmetry'
      fnb=5.
      xpom=xqdp-fnb*dpom*.5-(fnb-1.)*space*.5
      do i=1,nint(fnb)
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(Index2d.and.i.le.2) then
          j=ButtonDisabled
        else
          j=ButtonOff
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.1) then
          nButtDefSymm=ButtonLastMade
          Veta='S%ymmetrical supecell'
        else if(i.eq.2) then
          nButtSymmSuper=ButtonLastMade
          Veta='%Refine UB+cell'
        else if(i.eq.3) then
          nButtRefineUB=ButtonLastMade
          Veta='Refine %cell from d'
        else if(i.eq.4) then
          nButtRefineCell=ButtonLastMade
          Veta='Sho%w listing'
        else if(i.eq.5) then
          nButtListing=ButtonLastMade
        endif
        xpom=xpom+dpom+space
      enddo
      il=il+1
      Veta='Step bac%k'
      fnb=3.
      xpom=xqdp-fnb*dpom*.5-(fnb-1.)*space*.5
      do i=1,nint(fnb)
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtStepBack=ButtonLastMade
          Veta='Step %forward'
        else if(i.eq.2) then
          nButtStepForward=ButtonLastMade
          Veta='%Options'
        else if(i.eq.3) then
          nButtOptions=ButtonLastMade
        endif
        xpom=xpom+dpom+space
      enddo
      call SetStringArrayTo(TextInfo,8,' ')
      xpom=5.
      dpom=xqd-10.
      ypom=260.
      il=12
      call FeQuestSbwMake(id,xpom,il,dpom,11,2,
     1                    CutTextFromLeft,SbwHorizontal)
      nSbw=SbwLastMade
      il=-10*il-7
      UseTabsM=NextTabs()
      if(ndo.eq.2) then
        pom=100.
      else
        pom=30.
      endif
      do i=1,ndo
        call FeTabsAdd(pom,UseTabsM,IdCharTab,'.')
        pom=pom+60.
      enddo
      if(ndo.eq.2) then
        pom=330.
      else
        pom=300.
      endif
      do i=1,ndo
        call FeTabsAdd(pom,UseTabsM,IdCharTab,'.')
        pom=pom+60.
      enddo
      UseTabsC=NextTabs()
      pom=120.
      do i=1,9
        call FeTabsAdd(pom,UseTabsC,IdCharTab,'.')
        pom=pom+50.
      enddo
      do i=1,nt
        il=il-5
        if(i.ge.ndo+1.and.i.le.ndo+3) UseTabs=UseTabsM
        if(i.ge.ndo+5.and.i.le.nd+9) UseTabs=UseTabsC
        if(i.eq.1) then
          xpom=xqdp
          ZnakI='C'
        else if(i.eq.2) then
          if(ndo.eq.2) then
            xpom=105.
          else
            xpom=65.
          endif
          ZnakI='L'
        else if(i.eq.3) then
          xpom=340.
          il=il+5
          ZnakI='L'
        else if(i.eq.ndo+4) then
          if(ndo.eq.2) then
            xpom=150.
            ZnakI='L'
          else
            xpom=200.
            ZnakI='L'
          endif
        else
          xpom=5.
          ZnakI='L'
        endif
        call FeQuestLblMake(id,xpom,il,TextInfo(i),ZnakI,'N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1.or.i.eq.6.or.i.eq.nt) then
          if(i.eq.1) nLblRefFirst=LblLastMade
          il=il-5
          call FeQuestLinkaMake(id,il)
        endif
      enddo
      NRefOut=20
      nmax=min(NRefOut,GrIndexNAll)
      m=1
      NStore=0
      NActual=0
1100  if(NStore.lt.10) then
        NStore=NStore+1
      else
        do i=2,NStore
          call CopyMat(UBStore(1,1,i),UBStore(1,1,i-1),3)
          call CopyVek(CellSUStore(1,i),
     1                 CellSUStore(1,i-1),6)
          do j=1,ndi
            call CopyVek( QuStore(1,j,i), QuStore(1,j,i-1),3)
            call CopyVek(QuSStore(1,j,i),QuSStore(1,j,i-1),3)
          enddo
        enddo
      endif
      call CopyMat(UB(1,1,KRefBlock),UBStore(1,1,NStore),3)
      call CopyVek(CellRefBlockSU(1,KDatBlock),CellSUStore(1,NStore),6)
      VolStore(NStore)=CellVolume
      do j=1,ndi
        call CopyVek(QuRefBlock(1,j,KDatBlock), QuStore(1,j,NStore),3)
        call CopyVek(QuRefBlockSU(1,j,KRefBlock),QuSStore(1,j,NStore),3)
      enddo
      NActual=NStore
1150  n1=nmax*(m-1)+1
      n2=min(GrIndexNAll,n1+nmax-1)
      nLbl=nLblRefFirst
      do il=1,9
        TextInfo(il)=' '
        call FeQuestLblChange(nLbl,TextInfo(il))
        nLbl=nLbl+1
      enddo
      kk=1
      call CloseIfOpened(SbwLnQuest(nSbw))
      ln=NextLogicNumber()
      call OpenFile(ln,FileIndex(:idel(FileIndex))//'_lstref.tmp',
     1              'formatted','unknown')
      call DRGrIndexTryInd
      do i=1,GrIndexNAll
        if(FlagU(i).le.0) cycle
        if(FlagS(i).ne.0) then
          ZnakS='S'
        else
          ZnakS=' '
        endif
        call multm(UBI(1,1,KRefBlock),Slozky(1,i),ha(1,i),3,3,1)
        if(FlagI(i).ne.0) then
          ZnakI='I'
        else
          ZnakI=' '
        endif
        write(ln,'(i6,3f8.3,2(4x,a1,2x))') i,(ha(j,i),j=1,3),ZnakI,ZnakS
      enddo
      call CloseIfOpened(ln)
      call FeQuestSbwTextOpen(nSbw,20,FileIndex(:idel(FileIndex))//
     1                        '_lstref.tmp')
      if(IndexedAll.le.3) then
        call FeQuestButtonDisable(nButtRefineUB)
        call FeQuestButtonDisable(nButtRefineCell)
      else
        call FeQuestButtonOff(nButtRefineUB)
        call FeQuestButtonOff(nButtRefineCell)
      endif
      if(NStore.gt.0) then
        if(NActual.gt.1) then
          call FeQuestButtonOff(nButtStepBack)
        else
          call FeQuestButtonDisable(nButtStepBack)
        endif
        if(NActual.lt.NStore) then
          call FeQuestButtonOff(nButtStepForward)
        else
          call FeQuestButtonDisable(nButtStepForward)
        endif
      else
        call FeQuestButtonDisable(nButtStepBack)
        call FeQuestButtonDisable(nButtStepForward)
      endif
      if(.not.Index2d) then
        if(nBasVec.le.3) then
          call FeQuestButtonDisable(nButtSuper)
        else
          call FeQuestButtonOff(nButtSuper)
        endif
      endif
      write(Veta,103) IndexedAll,GrIndexNAll
      call Zhusti(Veta)
      write(Cislo,'(f5.1)') float(IndexedAll)/float(GrIndexNAll)*100.
      call ZdrcniCisla(Cislo,1)
      il=1
      TextInfo(il)='Indexed all: '//Veta(:idel(Veta))//
     1             ' ~ '//Cislo(:idel(Cislo))//'%'
      if(ndi.gt.0) then
        write(Cislo,FormI15) IndexedAllSat
        call Zhusti(Cislo)
        TextInfo(il)(idel(TextInfo(il))+3:)=Cislo(:idel(Cislo))//
     1                                      ' satellites'
      endif
      write(Veta,103) IndexedSel,GrIndexNSel
      call Zhusti(Veta)
      write(Cislo,'(f5.1)') float(IndexedSel)/float(GrIndexNSel)*100.
      call ZdrcniCisla(Cislo,1)
      TextInfo(il)(idel(TextInfo(il))+10:)='Indexed filtered: '//
     1  Veta(:idel(Veta))//' ~ '//Cislo(:idel(Cislo))//'%'
      if(ndi.gt.0) then
        write(Cislo,FormI15) IndexedSelSat
        call Zhusti(Cislo)
        TextInfo(il)(idel(TextInfo(il))+3:)=Cislo(:idel(Cislo))//
     1                                      ' satellites'
      endif
      TextInfo1=TextInfo(il)
      il=il+1
      TextInfo(il)='Orientation matrix'
      il=il+1
      TextInfo(il)='Metric tensor'
      do i=1,ndo
        il=il+1
        write(TextInfo(il),'(3(a1,f10.6))')
     1    (Tabulator,UB(i,j,1),j=1,ndo)
        write(TextInfo(il)(34:),'(3(a1,f10.3))')
     1    (Tabulator,DRMetTens(i,j),j=1,ndo)
        call Zhusti(TextInfo(il))
      enddo
      il=il+1
      if(ndo.eq.2) then
        write(TextInfo(il),'(''Area :'',f10.2)') CellVolume
      else
        if(AllowScalCell) then
          do i=1,3
            pom=pom*CellRefBlock(i,KDatBlock)/CellParRest(i)
          enddo
          pom=pom**0.333333
          write(Veta,'(f10.2,''/'',f10.4)') CellVolume,pom
          call Zhusti(Veta)
          write(TextInfo(il),'(''Volume/Scale : '',a)')
     1      Veta(:idel(Veta))
        else
          write(TextInfo(il),'(15x,''Volume :'',f10.2)') CellVolume
        endif
      endif
      do i=1,2
        il=il+1
        if(i.eq.1) then
          if(ndo.eq.2) then
            write(TextInfo(il),100) 'Cell parameters:',
     1                    (Tabulator,CellRefBlock(j,KDatBlock),j=1,2),
     2                     Tabulator,CellRefBlock(6,KDatBlock)
          else
            write(TextInfo(il),100) 'Cell parameters:',
     1                    (Tabulator,CellRefBlock(j,KDatBlock),j=1,6)
          endif
        else
          if(ndo.eq.2) then
            write(TextInfo(il),100) 's.u.:',
     1                    (Tabulator,CellRefBlockSU(j,KDatBlock),j=1,2),
     2                     Tabulator,CellRefBlockSU(6,KDatBlock)
          else
            write(TextInfo(il),100) 's.u.:',
     1                    (Tabulator,CellRefBlockSU(j,KDatBlock),j=1,6)
          endif
        endif
        call Zhusti(TextInfo(il)(17:))
      enddo
      if(ndi.gt.0) then
        do i=1,2
          il=il+1
          if(i.eq.1) then
            write(TextInfo(il),101) 'Modulation vector:',
     1        ((Tabulator,QuRefBlock(j,k,KRefBlock),j=1,3),
     2          k=1,ndi)
          else
            write(TextInfo(il),101) 's.u.:',
     1        ((Tabulator,QuRefBlockSU(j,k,KRefBlock),j=1,3),
     2         k=1,ndi)
          endif
          call Zhusti(TextInfo(il)(17:))
        enddo
      endif
      nLbl=nLblRefFirst
      do il=1,nt
        call FeQuestLblChange(nLbl,TextInfo(il))
        nLbl=nLbl+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtStepBack.or.
     2    CheckNumber.eq.nButtStepForward)) then
        if(CheckNumber.eq.nButtStepBack) then
          NActual=NActual-1
        else
          NActual=NActual+1
        endif
        call CopyMat(UBStore(1,1,NActual),UB(1,1,KRefBlock),3)
        call CopyVek(CellSUStore(1,NActual),
     1               CellRefBlockSU(1,KDatBlock),6)
        do j=1,ndi
          call CopyVek(QuStore(1,j,NActual),QuRefBlock(1,j,KDatBlock),3)
          call CopyVek(QuSStore(1,j,NActual),
     1                 QuRefBlockSU(1,j,KRefBlock),3)
        enddo
        go to 1150
      endif
1550  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtOptions) then
          CrSystemPom=CrSystemRest
          idp=NextQuestId()
          Veta='Options:'
          xqdp=500.
          il=13
          if(ndi.gt.0) il=il+ndi
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
          il=1
          tpom=5.
          Veta='%Maximal d* difference in Ang**-1 for indexing:'
          xpom=tpom+FeTxLengthUnder(Veta)+120.
          dpom=50.
          do i=1,3
            call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) then
              pom=DiffMax
              Veta='Maximal deviation in %cell lengths in [A] for '//
     1             'symmetric supercell search:'
              nEdwDelta=EdwLastMade
            else if(i.eq.2) then
              pom=DiffAxe
              Veta='Maximal deviation in cell %angles in degs for '//
     1             'symmetric supercell search:'
              nEdwDiffAxe=EdwLastMade
            else
              pom=DiffAngle
              nEdwDiffAngle=EdwLastMade
            endif
            call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
            il=il+1
          enddo
          if(ndi.gt.0) then
            tpom1=5.
            dpom=50.
            do i=1,ndi
              Veta=Indices(i+3)//'(min):'
              if(i.eq.1) xpom1=tpom1+FeTxLength(Veta)+5.
              call FeQuestEudMake(idp,tpom1,il,xpom1,il,Veta,'L',dpom,
     1                            EdwYd,0)
              call FeQuestIntEdwOpen(EdwLastMade,MMin(i),.false.)
              call FeQuestEudOpen(EdwLastMade,-4,0,1,0.,0.,0.)
              Veta=Indices(i+3)//'(max):'
              if(i.eq.1) then
                nEdwMM=EdwLastMade
                tpom2=xpom1+dpom+50.
                xpom2=tpom2+FeTxLength(Veta)+5.
              endif
              call FeQuestEudMake(idp,tpom2,il,xpom2,il,Veta,'L',dpom,
     1                            EdwYd,0)
              call FeQuestIntEdwOpen(EdwLastMade,MMax(i),.false.)
              call FeQuestEudOpen(EdwLastMade,0,4,1,0.,0.,0.)
              il=il+1
            enddo
          else
            nEdwMM=0
          endif
          Veta='Restrictions used in the refinement of cell parameters:'
          call FeQuestLblMake(idp,xqdp/2.,il,Veta,'C','B')
          xpom=30.
          tpom=xpom+CrwXd+10.
          il=il+1
          il=-10*il
          j=0
          do i=1,10
            if(i.ge.2.and.i.le.4) then
              j=2
              Veta=CrSystemName(j)(:idel(CrSystemName(j)))//
     1             ' - setting "'//char(ichar('a')+i-2)//'"'
              k=j+10*(i-1)
            else if(i.lt.10) then
              j=j+1
              k=j
              if(i.eq.7) then
                Veta='Rhombic'
              else
                Veta=CrSystemName(j)
              endif
            else
              k=FixedCell
              Veta='%Fixed cell parameters'
            endif
            call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',
     1                          CrwXd,CrwYd,1,1)
            call FeQuestCrwOpen(CrwLastMade,CrSystemRest.eq.k)
            if(i.eq.1) nCrwSystemFirst=CrwLastMade
            if(i.ne.10) il=il-8
          enddo
          nCrwSystemLast=CrwLastMade
          tpom=tpom+FeTxLength(Veta)+5.
          Veta='=>'
          xpom=tpom+FeTxLength(Veta)+5.
          dpom=xqdp-xpom-5.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                         0)
          nEdwCellPar=EdwLastMade
          il=il-10
          tpom=xpom+CrwXd+10.
          Veta='Allow lattice %scaling'
          call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        0,0)
          nCrwScalCell=CrwLastMade
          call FeQuestCrwOpen(nCrwScalCell,AllowScalCell)
1600      if(CrwLogicQuest(nCrwSystemLast)) then
            call FeQuestRealAEdwOpen(nEdwCellPar,CellParRest,6,
     1                               CellParRest(1).lt..01,.false.)
            call FeQuestCrwOpen(nCrwScalCell,AllowScalCell)
          else
            if(EdwStateQuest(nEdwCellPar).eq.EdwOpened)
     1        call FeQuestRealAFromEdw(nEdwCellPar,CellParRest)
            call FeQuestEdwDisable(nEdwCellPar)
            AllowScalCell=CrwLogicQuest(nCrwScalCell)
            call FeQuestCrwDisable(nCrwScalCell)
          endif
1650      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.ge.nCrwSystemFirst.and.
     2        CheckNumber.le.nCrwSystemLast)) then
            go to 1600
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1650
          endif
          if(ich.eq.0) then
            call FeQuestRealFromEdw(nEdwDelta,DiffMax)
            call FeQuestRealFromEdw(nEdwDiffAxe,DiffAxe)
            call FeQuestRealFromEdw(nEdwDiffAngle,DiffDiffAngle)
            if(nEdwMM.gt.0) then
              nEdw=nEdwMM
              do i=1,NDimI(KPhase)
                call FeQuestIntAFromEdw(nEdw,MMin(i))
                nEdw=nEdw+1
                call FeQuestIntAFromEdw(nEdw,MMax(i))
                nEdw=nEdw+1
              enddo
            endif
            nCrw=nCrwSystemFirst
            j=0
            do i=1,10
              if(i.ge.2.and.i.le.4) then
                j=2
                k=j+10*(i-1)
              else if(i.ne.10) then
                j=j+1
                k=j
              else
                k=FixedCell
              endif
              if(CrwLogicQuest(nCrw)) then
                CrSystemRest=k
                exit
              endif
              nCrw=nCrw+1
            enddo
            if(CrSystemRest.eq.FixedCell) then
              call FeQuestRealAFromEdw(nEdwCellPar,CellParRest)
              CellParRestR=CellParRest
              do i=4,6
                CellParRestR(i)=cos(CellParRestR(i)*ToRad)
              enddo
              call Recip(CellParRestR,CellParRestR,pom)
              do i=4,6
                CellParRestR(i)=acos(CellParRestR(i))/ToRad
              enddo
              AllowScalCell=CrwLogicQuest(nCrwScalCell)
            endif
          endif
          call FeQuestRemove(idp)
          if(IndexedAll.gt.3) then
            CheckNumber=nButtRefineUB
            call FeQuestButtonOff(nButtOptions)
            go to 1550
          else
            go to 1150
          endif
        else if(CheckNumber.eq.nButtReduce) then
          call UnitMat(T6,nd)
          call DRCellReduction(CellRefBlock(1,KRefBlock),T6)
          call MatBlock3(T6,T,nd)
          call CopyMat(T,TQ,3)
          call TrMat(T,Tt,3,3)
          call MatInv(Tt,T,det,3)
        else if(CheckNumber.eq.nButtMatrix) then
          call UnitMat(T,3)
          call FeReadRealMat(-1.,-1.,'Transformation matrix',SmbABC,
     1                       IdAddPrime,SmbABC,T,Ti,3,CheckSingYes,
     2                       CheckPosDefYes,ich)
          if(ich.ne.0) go to 1500
          call CopyMat(T,TQ,3)
          call TrMat(Ti,T,3,3)
        else if(CheckNumber.eq.nButtMatrixSpecial) then
          UseTabsPom=UseTabs
          UseTabs=NextTabs()
          call FeTabsAdd( 30.,UseTabs,IdRightTab,' ')
          call FeTabsAdd( 90.,UseTabs,IdRightTab,' ')
          call FeTabsAdd(110.,UseTabs,IdRightTab,' ')
          call FeTabsAdd(180.,UseTabs,IdRightTab,' ')
          i=FeMenu(-1.,-1.,SpecMatrixName,1,ubound(SpecMatrixName,1),
     1             1,0)
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsPom
          if(i.le.0) go to 1500
          call CopyMat(SpecMatrix(1,i),T,3)
          call CopyMat(T,TQ,3)
          call MatInv(T,Ti,det,3)
          call TrMat(Ti,T,3,3)
        else if(CheckNumber.eq.nButtDouble) then
          call UnitMat(T,3)
          call DRDoubleCell(T,CellRefBlock(1,KRefBlock),k)
          call CopyMat(T,TQ,3)
          call MatInv(T,Ti,det,3)
          call TrMat(Ti,T,3,3)
        else if(CheckNumber.eq.nButtSuper) then
          idp=NextQuestId()
          xqd=400.
          dpom=140.
          xqdp=xqd*.5
          il=13
          Veta='Select lattice points to define a new supercell:'
          call FeQuestCreate(idp,-1.,-1.,xqd,13,Veta,0,LightGray,0,0)
          il=1
          dpom=xqd-10.-SbwPruhXd
          UseTabsPom=UseTabs
          UseTabs=NextTabs()
          do j=1,2
            if(j.eq.1) then
              Veta='Vector'
              xpom=5.
            else if(j.eq.2) then
              Veta='Number of occurences'
              xpom=xpom+65.
            endif
            dx=0.
            do i=1,2
              call FeQuestLblMake(idp,xpom+dx,il,Veta,'L','N')
              if(j.eq.2)
     1          call FeTabsAdd(xpom+dx+40.,UseTabs,IdLeftTab,' ')
              dx=dpom*.5
            enddo
          enddo
          xpom=5.
          il=12
          call FeQuestSbwMake(idp,xpom,il,dpom,il-1,2,
     1                        CutTextFromLeft,SbwVertical)
          nSbw=SbwLastMade
          il=il+1
          dpom=60.
          xpom=xqdp-dpom-5.
          call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,
     1                           '%Refresh')
          nButtRefresh=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          xpom=xpom+dpom+10.
          call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,
     1                           'Select %all')
          nButtAll=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1700      ln=NextLogicNumber()
          call OpenFile(ln,FileIndex(:idel(FileIndex))//'_basvec.tmp',
     1                  'formatted','unknown')
          if(ErrFlag.ne.0) go to 1800
          call indexx(nBasVec-3,MBasVec(4),OBasVec(4))
          do i=4,nBasVec
            OBasVec(i)=OBasVec(i)+3
          enddo
          do i=4,nBasVec
            ii=OBasVec(i)
            Veta='('
            kk=2
            do j=1,3
              ik(1)=BasVec(j,ii)
              ik(2)=BasVec(4,ii)
              call MinMultMaxFract(ik,2,k,MaxFract)
              do k=1,2
                ik(k)=ik(k)/MaxFract
              enddo
              if(ik(1).eq.0) then
                Veta(kk:kk+1)='0,'
              else if(ik(1).eq.1.and.ik(2).eq.1) then
                Veta(kk:kk+1)='1,'
              else
                write(Veta(kk:),'(i3,''/'',i3,'','')') ik
              endif
              call Zhusti(Veta)
              kk=idel(Veta)
              if(j.eq.3) then
                Veta(kk:kk)=')'
              else
                kk=kk+1
              endif
            enddo
            write(Cislo,FormI15) -MBasVec(ii)
            call Zhusti(Cislo)
            Veta=Veta(:idel(Veta))//Tabulator//
     1           Cislo(:idel(Cislo))
            write(ln,FormA) Veta(:idel(Veta))
          enddo
          call CloseIfOpened(ln)
          call FeQuestSbwSelectOpen(nSbw,FileIndex(:idel(FileIndex))//
     1                              '_basvec.tmp')
1750      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventButton) then
            if(CheckNumber.eq.nButtRefresh) then
              j=0
            else if(CheckNumber.eq.nButtAll) then
              j=1
            endif
            do i=1,nBasVec-3
              call FeQuestSetSbwItemSel(i,nSbw,j)
            enddo
            call CloseIfOpened(SbwLnQuest(nSbw))
            go to 1700
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1750
          endif
          if(ich.eq.0) then
            n=0
            call SetLogicalArrayTo(BasVecFlag,nBasVec,.false.)
            call SetLogicalArrayTo(BasVecFlag,3,.true.)
            do i=1,nBasVec-3
              if(SbwItemSelQuest(i,nSbw).eq.1) then
                ii=OBasVec(i+3)
                BasVecFlag(ii)=.true.
                n=n+1
              endif
            enddo
            if(n.le.0) then
              i=OBasVec(SbwItemPointerQuest(nSbw)+3)
              BasVecFlag(i)=.true.
            endif
            IVolMax=0
            do i=1,nBasVec-2
              if(.not.BasVecFlag(i)) cycle
              do n=1,3
                Ti(n,1)=float(BasVec(n,i))/float(BasVec(4,i))
              enddo
              do j=i+1,nBasVec-1
                if(.not.BasVecFlag(j)) cycle
                do n=1,3
                  Ti(n,2)=float(BasVec(n,j))/float(BasVec(4,j))
                enddo
                do k=j+1,nBasVec
                  if(.not.BasVecFlag(k)) cycle
                  do n=1,3
                    Ti(n,3)=float(BasVec(n,k))/float(BasVec(4,k))
                  enddo
                  call MatInv(Ti,Tt,Vol,3)
                  if(abs(Vol).lt..0001) cycle
                  IVol=nint(1./Vol)
                  if(IVol.lt.0) then
                    IVol=-IVol
                    call RealVectorToOpposite(Ti(1,3),Ti(1,3),3)
                  endif
                  if(IVol.ge.IVolMax) then
                    call CopyMat(Ti,T,3)
                    IVolMax=IVol
                  endif
                enddo
              enddo
            enddo
            call TrMat(T,Ti,3,3)
            call MatInv(Ti,TQ,det,3)
          endif
1800      call FeQuestRemove(idp)
          call DeleteFile(FileIndex(:idel(FileIndex))//'_basvec.tmp')
          call FeTabsReset(UseTabs)
          UseTabs=UseTabsPom
          if(ich.ne.0) go to 1500
        else if(CheckNumber.eq.nButtDefSymm) then
          call FindSmbSg(Grupa(KPhase),.true.,1)
          call EM50DefSpaceGroup(1,ich)
          if(ich.ne.0) go to 1500
          call UnitMat(T,3)
          call UnitMat(TQ,3)
        else if(CheckNumber.eq.nButtSymmSuper) then
          call UnitMat(T6,nd)
          i=0
          call DRSGTestSuperCell(CellRefBlock(1,KDatBlock),T6,i,0,ich)
          if(ich.ne.0) go to 1500
          call MatBlock3(T6,T,nd)
          call CopyMat(T,TQ,3)
          call TrMat(T,Tt,3,3)
          call MatInv(Tt,T,det,3)
        else if(CheckNumber.eq.nButtRefineUB.or.
     1          CheckNumber.eq.nButtRefineCell) then
          if(IndexedAll.le.3) go to 1500
          mm=0
          allocate(FlagIOld(GrIndexNAll))
2000      call SetRealArrayTo(AM,NDimQ(KPhase),0.)
          call SetRealArrayTo(PS,3*nd,0.)
          do i=1,GrIndexNAll
            if(FlagI(i).le.0.or.FlagU(i).le.0) cycle
            pom=0.
            do j=1,nd
              hp(j)=iha(j,i)
              pom=pom+abs(hp(j))
            enddo
            if(pom.lt..99) cycle
            if(index2d.and.abs(hp(3)).gt..5) cycle
            m=0
            do j=1,nd
              do k=1,nd
                m=m+1
                AM(m)=AM(m)+hp(j)*hp(k)
              enddo
            enddo
            m=0
            do k=1,nd
              do j=1,3
                m=m+1
                PS(m)=PS(m)+Slozky(j,i)*hp(k)
              enddo
            enddo
          enddo
          call matinv(AM,AMI,det,nd)
          call Multm(PS,AMI,Ugen,3,nd,nd)
          do j=1,3
            Suma=0.
            do i=1,GrIndexNAll
              if(FlagI(i).le.0.or.FlagU(i).le.0) cycle
              pom=0.
              do k=1,nd
                hp(k)=iha(k,i)
                pom=pom+abs(hp(j))
              enddo
              if(pom.lt..99) cycle
              if(index2d.and.abs(hp(3)).gt..5) cycle
              pp=Slozky(j,i)
              do k=1,nd
                pp=pp-Ugen(j+(k-1)*3)*hp(k)
              enddo
              Suma=Suma+pp**2
            enddo
            Suma=sqrt(Suma/float(IndexedAll-nd))
            do k=1,nd
              SUgen(j+(k-1)*3)=Suma*sqrt(AMI(k+(k-1)*nd))
              if(k.le.3) then
                UB  (j,k,1)= Ugen(j+(k-1)*3)
                UBSU(j,k)  =SUgen(j+(k-1)*3)
              else
                Uq(j+(k-4)*3)= Ugen(j+(k-1)*3)
               SUq(j+(k-4)*3)=SUgen(j+(k-1)*3)
              endif
            enddo
          enddo
          call CopyVekI(FlagI,FlagIOld,GrIndexNAll)
2500      if(CrSystemRest.gt.1.and.ndi.le.0) then
            nn=9
            do ic=1,10
              AR=sqrt(UB(1,1,1)**2+UB(2,1,1)**2+UB(3,1,1)**2)
              BR=sqrt(UB(1,2,1)**2+UB(2,2,1)**2+UB(3,2,1)**2)
              CR=sqrt(UB(1,3,1)**2+UB(2,3,1)**2+UB(3,3,1)**2)
              GaR=acos((UB(1,1,1)*UB(1,2,1)+
     1                  UB(2,1,1)*UB(2,2,1)+
     2                  UB(3,1,1)*UB(3,2,1))/(AR*BR))/ToRad
              BeR=acos((UB(1,1,1)*UB(1,3,1)+
     1                  UB(2,1,1)*UB(2,3,1)+
     2                  UB(3,1,1)*UB(3,3,1))/(AR*CR))/ToRad
              AlR=acos((UB(1,2,1)*UB(1,3,1)+
     1                  UB(2,2,1)*UB(2,3,1)+
     2                  UB(3,2,1)*UB(3,3,1))/(BR*CR))/ToRad
              AM=0.
              PS=0.
              Suma=0.
              wt=1000.
              do i=1,GrIndexNAll
               if(FlagI(i).le.0.or.FlagU(i).le.0) cycle
                pom=0.
                do j=1,nd
                  hp(j)=iha(j,i)
                  pom=pom+abs(hp(j))
                enddo
                if(pom.lt..99) cycle
                if(index2d.and.abs(hp(3)).gt..5) cycle
                n=0
                do k=1,3
                  der(1:nn)=0.
                  do j=1,3
                    n=n+1
                    der(n)=iha(j,i)
                  enddo
                  dy=Slozky(k,i)-UB(k,1,1)*float(iha(1,i))
     1                          -UB(k,2,1)*float(iha(2,i))
     2                          -UB(k,3,1)*float(iha(3,i))
                  Suma=Suma+wt*dy**2
                  m=0
                  do j=1,nn
                    do l=1,j
                      m=m+1
                      AM(m)=AM(m)+wt*der(j)*der(l)
                    enddo
                    PS(j)=PS(j)+wt*der(j)*dy
                  enddo
                enddo
              enddo
              Suma=sqrt(Suma/float(3*IndexedAll-9))
              wt=(Suma/.0000001)**2
              CrSystemPom=mod(CrSystemRest,10)
              Mono=CrSystemRest/10
              if(CrSystemPom.eq.CrSystemTetragonal.or.
     1           CrSystemPom.eq.CrSystemTrigonal.or.
     2           CrSystemPom.eq.CrSystemHexagonal.or.
     3           CrSystemPom.eq.CrSystemCubic.or.
     4           (CrSystemPom.eq.FixedCell.and..not.AllowScalCell)) then
                der(1:nn)=0.
                der(2)=-UB(1,2,1)/BR
                der(5)=-UB(2,2,1)/BR
                der(8)=-UB(3,2,1)/BR
                if(CrSystemRest.eq.FixedCell) then
                  dy=(BR-CellParRestR(2))
                else
                  dy=(BR-AR)
                  der(1)= UB(1,1,1)/AR
                  der(4)= UB(2,1,1)/AR
                  der(7)= UB(3,1,1)/AR
                endif
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              if(CrSystemPom.eq.CrSystemCubic.or.
     1           CrSystemPom.eq.CrSystemTrigonal.or.
     2           (CrSystemPom.eq.FixedCell.and..not.AllowScalCell)) then
                der(1:nn)=0.
                der(1)=-UB(1,1,1)/AR
                der(4)=-UB(2,1,1)/AR
                der(7)=-UB(3,1,1)/AR
                if(CrSystemPom.eq.FixedCell) then
                  dy=(AR-CellParRestR(1))
                else
                  dy=(AR-CR)
                  der(2)=UB(1,3,1)/CR
                  der(5)=UB(2,3,1)/CR
                  der(8)=UB(3,3,1)/CR
                endif
              endif
              wt=(Suma/.0001)**2
              if((CrSystemPom.eq.CrSystemMonoclinic.and.
     1            (Mono.eq.2.or.Mono.eq.3)).or.
     2           CrSystemPom.eq.CrSystemOrthorhombic.or.
     3           CrSystemPom.eq.CrSystemTetragonal.or.
     4           CrSystemPom.eq.CrSystemHexagonal.or.
     5           CrSystemPom.eq.CrSystemCubic.or.
     6           CrSystemPom.eq.FixedCell) then
                if(CrSystemPom.eq.FixedCell) then
                  pom=CellParRestR(4)
                else
                  pom=90.
                endif
                dy=(AlR-pom)
                FNom=UB(1,2,1)*UB(1,3,1)+UB(2,2,1)*UB(2,3,1)+
     1               UB(3,2,1)*UB(3,3,1)
                pom=FNom/(BR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(2)=pom/(CR*BR**3)*(2.*UB(1,2,1)*FNom-
     1                                 BR**2*UB(1,3,1))
                der(5)=pom/(CR*BR**3)*(2.*UB(2,2,1)*FNom-
     1                                 BR**2*UB(2,3,1))
                der(8)=pom/(CR*BR**3)*(2.*UB(3,2,1)*FNom-
     1                                 BR**2*UB(3,3,1))
                der(3)=pom/(BR*CR**3)*(2.*UB(1,3,1)*FNom-
     1                                 CR**2*UB(1,2,1))
                der(6)=pom/(BR*CR**3)*(2.*UB(2,3,1)*FNom-
     1                                 CR**2*UB(2,2,1))
                der(9)=pom/(BR*CR**3)*(2.*UB(3,3,1)*FNom-
     1                                 CR**2*UB(3,2,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              if((CrSystemPom.eq.CrSystemMonoclinic.and.
     1            (Mono.eq.1.or.Mono.eq.3)).or.
     2           CrSystemPom.eq.CrSystemOrthorhombic.or.
     3           CrSystemPom.eq.CrSystemTetragonal.or.
     4           CrSystemPom.eq.CrSystemHexagonal.or.
     5           CrSystemPom.eq.CrSystemCubic.or.
     6           CrSystemPom.eq.FixedCell) then
                if(CrSystemPom.eq.FixedCell) then
                  pom=CellParRestR(5)
                else
                  pom=90.
                endif
                dy=(BeR-pom)
                FNom=UB(1,1,1)*UB(1,3,1)+UB(2,1,1)*UB(2,3,1)+
     1               UB(3,1,1)*UB(3,3,1)
                pom=FNom/(AR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(1)=pom/(CR*AR**3)*(2.*UB(1,1,1)*FNom-
     1                                 AR**2*UB(1,3,1))
                der(4)=pom/(CR*AR**3)*(2.*UB(2,1,1)*FNom-
     1                                 AR**2*UB(2,3,1))
                der(7)=pom/(CR*AR**3)*(2.*UB(3,1,1)*FNom-
     1                                 AR**2*UB(3,3,1))
                der(3)=pom/(AR*CR**3)*(2.*UB(1,3,1)*FNom-
     1                                 CR**2*UB(1,1,1))
                der(6)=pom/(AR*CR**3)*(2.*UB(2,3,1)*FNom-
     1                                 CR**2*UB(2,1,1))
                der(9)=pom/(AR*CR**3)*(2.*UB(3,3,1)*FNom-
     1                                 CR**2*UB(3,1,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              if((CrSystemPom.eq.CrSystemMonoclinic.and.
     1            (Mono.eq.1.or.Mono.eq.2)).or.
     2           CrSystemPom.eq.CrSystemOrthorhombic.or.
     3           CrSystemPom.eq.CrSystemTetragonal.or.
     4           CrSystemPom.eq.CrSystemHexagonal.or.
     5           CrSystemPom.eq.CrSystemCubic.or.
     6           CrSystemPom.eq.FixedCell) then
                if(CrSystemPom.eq.FixedCell) then
                  pom=CellParRestR(6)
                else if(CrSystemPom.eq.CrSystemHexagonal) then
                  pom=120.
                else
                  pom=90.
                endif
                dy=(GaR-pom)
                FNom=UB(1,1,1)*UB(1,2,1)+UB(2,1,1)*UB(2,2,1)+
     1               UB(3,1,1)*UB(3,2,1)
                pom=FNom/(AR*BR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(1)=pom/(BR*AR**3)*(2.*UB(1,1,1)*FNom-
     1                                 AR**2*UB(1,2,1))
                der(4)=pom/(BR*AR**3)*(2.*UB(2,1,1)*FNom-
     1                                 AR**2*UB(2,2,1))
                der(7)=pom/(BR*AR**3)*(2.*UB(3,1,1)*FNom-
     1                                 AR**2*UB(3,2,1))
                der(2)=pom/(AR*BR**3)*(2.*UB(1,2,1)*FNom-
     1                                 BR**2*UB(1,1,1))
                der(5)=pom/(AR*BR**3)*(2.*UB(2,2,1)*FNom-
     1                                 BR**2*UB(2,1,1))
                der(8)=pom/(AR*BR**3)*(2.*UB(3,2,1)*FNom-
     1                                 BR**2*UB(3,1,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              if(CrSystemPom.eq.CrSystemTrigonal) then
                dy=(AlR-BeR)
                FNom=UB(1,2,1)*UB(1,3,1)+UB(2,2,1)*UB(2,3,1)+
     1               UB(3,2,1)*UB(3,3,1)
                pom=FNom/(BR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(2)=pom/(CR*BR**3)*(2.*UB(1,2,1)*FNom-
     1                                 BR**2*UB(1,3,1))
                der(5)=pom/(CR*BR**3)*(2.*UB(2,2,1)*FNom-
     1                                 BR**2*UB(2,3,1))
                der(8)=pom/(CR*BR**3)*(2.*UB(3,2,1)*FNom-
     1                                 BR**2*UB(3,3,1))
                der(3)=pom/(BR*CR**3)*(2.*UB(1,3,1)*FNom-
     1                                 CR**2*UB(1,2,1))
                der(6)=pom/(BR*CR**3)*(2.*UB(2,3,1)*FNom-
     1                                 CR**2*UB(2,2,1))
                der(9)=pom/(BR*CR**3)*(2.*UB(3,3,1)*FNom-
     1                                 CR**2*UB(3,2,1))
                FNom=UB(1,1,1)*UB(1,3,1)+UB(2,1,1)*UB(2,3,1)+
     1               UB(3,1,1)*UB(3,3,1)
                pom=FNom/(AR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1)=-pom/(CR*AR**3)*(2.*UB(1,1,1)*FNom-
     1                                  AR**2*UB(1,3,1))
                der(4)=-pom/(CR*AR**3)*(2.*UB(2,1,1)*FNom-
     1                                  AR**2*UB(2,3,1))
                der(7)=-pom/(CR*AR**3)*(2.*UB(3,1,1)*FNom-
     1                                  AR**2*UB(3,3,1))
                der(3)=der(3)-
     1                 pom/(AR*CR**3)*(2.*UB(1,3,1)*FNom-
     2                                 CR**2*UB(1,1,1))
                der(6)=der(6)-
     1                 pom/(AR*CR**3)*(2.*UB(2,3,1)*FNom-
     2                                 CR**2*UB(2,1,1))
                der(9)=der(9)-
     1                 pom/(AR*CR**3)*(2.*UB(3,3,1)*FNom-
     2                                 CR**2*UB(3,1,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
                dy=(BeR-GaR)
                FNom=UB(1,1,1)*UB(1,3,1)+UB(2,1,1)*UB(2,3,1)+
     1               UB(3,1,1)*UB(3,3,1)
                pom=FNom/(AR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(1)=pom/(CR*AR**3)*(2.*UB(1,1,1)*FNom-
     1                                 AR**2*UB(1,3,1))
                der(4)=pom/(CR*AR**3)*(2.*UB(2,1,1)*FNom-
     1                                 AR**2*UB(2,3,1))
                der(7)=pom/(CR*AR**3)*(2.*UB(3,1,1)*FNom-
     1                                 AR**2*UB(3,3,1))
                der(3)=pom/(AR*CR**3)*(2.*UB(1,3,1)*FNom-
     1                                 CR**2*UB(1,1,1))
                der(6)=pom/(AR*CR**3)*(2.*UB(2,3,1)*FNom-
     1                                 CR**2*UB(2,1,1))
                der(9)=pom/(AR*CR**3)*(2.*UB(3,3,1)*FNom-
     1                                 CR**2*UB(3,1,1))
                FNom=UB(1,1,1)*UB(1,2,1)+UB(2,1,1)*UB(2,2,1)+
     1               UB(3,1,1)*UB(3,2,1)
                pom=FNom/(AR*BR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1)=der(1)-
     1                 pom/(BR*AR**3)*(2.*UB(1,1,1)*FNom-
     2                                 AR**2*UB(1,2,1))
                der(4)=der(4)-
     1                 pom/(BR*AR**3)*(2.*UB(2,1,1)*FNom-
     2                                 AR**2*UB(2,2,1))
                der(7)=der(7)-
     1                 pom/(BR*AR**3)*(2.*UB(3,1,1)*FNom-
     2                                 AR**2*UB(3,2,1))
                der(2)=-pom/(AR*BR**3)*(2.*UB(1,2,1)*FNom-
     1                                  BR**2*UB(1,1,1))
                der(5)=-pom/(AR*BR**3)*(2.*UB(2,2,1)*FNom-
     1                                  BR**2*UB(2,1,1))
                der(8)=-pom/(AR*BR**3)*(2.*UB(3,2,1)*FNom-
     1                                  BR**2*UB(3,1,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
                dy=(AlR-GaR)
                FNom=UB(1,2,1)*UB(1,3,1)+UB(2,2,1)*UB(2,3,1)+
     1               UB(3,2,1)*UB(3,3,1)
                pom=FNom/(BR*CR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1:nn)=0.
                der(2)=pom/(CR*BR**3)*(2.*UB(1,2,1)*FNom-
     1                                 BR**2*UB(1,3,1))
                der(5)=pom/(CR*BR**3)*(2.*UB(2,2,1)*FNom-
     1                                 BR**2*UB(2,3,1))
                der(8)=pom/(CR*BR**3)*(2.*UB(3,2,1)*FNom-
     1                                 BR**2*UB(3,3,1))
                der(3)=pom/(BR*CR**3)*(2.*UB(1,3,1)*FNom-
     1                                 CR**2*UB(1,2,1))
                der(6)=pom/(BR*CR**3)*(2.*UB(2,3,1)*FNom-
     1                                 CR**2*UB(2,2,1))
                der(9)=pom/(BR*CR**3)*(2.*UB(3,3,1)*FNom-
     1                                 CR**2*UB(3,2,1))
                FNom=UB(1,1,1)*UB(1,2,1)+UB(2,1,1)*UB(2,2,1)+
     1               UB(3,1,1)*UB(3,2,1)
                pom=FNom/(AR*BR)
                pom=-1./(ToRad*sqrt(1.-pom))
                der(1)=-pom/(BR*AR**3)*(2.*UB(1,1,1)*FNom-
     2                                  AR**2*UB(1,2,1))
                der(4)=-pom/(BR*AR**3)*(2.*UB(2,1,1)*FNom-
     2                                  AR**2*UB(2,2,1))
                der(7)=-pom/(BR*AR**3)*(2.*UB(3,1,1)*FNom-
     2                                  AR**2*UB(3,2,1))
                der(2)=der(2)-
     1                 pom/(AR*BR**3)*(2.*UB(1,2,1)*FNom-
     2                                 BR**2*UB(1,1,1))
                der(5)=der(5)-
     1                 pom/(AR*BR**3)*(2.*UB(2,2,1)*FNom-
     2                                 BR**2*UB(2,1,1))
                der(8)=der(8)-
     1                 pom/(AR*BR**3)*(2.*UB(3,2,1)*FNom-
     2                                 BR**2*UB(3,1,1))
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              wt=(Suma/.0000001)**2
              if((CrSystemPom.eq.FixedCell.and.AllowScalCell)) then
                Ratio=CellParRestR(2)/CellParRestR(1)
                dy=BR-AR*Ratio
                der(1:nn)=0.
                der(2)=-UB(1,2,1)/BR
                der(5)=-UB(2,2,1)/BR
                der(8)=-UB(3,2,1)/BR
                der(1)= UB(1,1,1)/AR*Ratio
                der(4)= UB(2,1,1)/AR*Ratio
                der(7)= UB(3,1,1)/AR*Ratio
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
                Ratio=CellParRestR(1)/CellParRestR(3)
                dy=AR-CR*Ratio
                der(1:nn)=0.
                der(1)=-UB(1,1,1)/AR
                der(4)=-UB(2,1,1)/AR
                der(7)=-UB(3,1,1)/AR
                der(2)= UB(1,3,1)/CR*Ratio
                der(5)= UB(2,3,1)/CR*Ratio
                der(8)= UB(3,3,1)/CR*Ratio
                m=0
                do j=1,nn
                  do l=1,j
                    m=m+1
                    AM(m)=AM(m)+wt*der(j)*der(l)
                  enddo
                  PS(j)=PS(j)+wt*der(j)*dy
                enddo
              endif
              call smi(AM,nn,ising)
              if(ising.ne.0) then
                Veta='the refinement failed - singularity.'
                call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
                deallocate(FlagIOld)
                go to 1500
              endif
              UGen=0.
              call nasob(AM,PS,UGen,nn)
              m=0
              ij=0
              do j=1,3
                do i=1,3
                  m=m+1
                  ij=ij+m
                  UB(j,i,1)=UB(j,i,1)+UGen(m)
                  UBSU(j,i)=Suma*sqrt(AM(ij))
                enddo
              enddo
            enddo
          endif
          call DRSCellFromSUB(UB(1,1,KRefBlock),UBSU,
     1                        CellRefBlockSU(1,KRefBlock),Vol,
     2                        SVol,DRMetTens,DRMetTensSU)
          call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
          CellVolume=1./Vol
          call multm (UBI(1,1,KRefBlock),Uq,Uqp,3,3,ndi)
          call multmq(UBI(1,1,KRefBlock),SUq,SUqp,3,3,ndi)
          do i=1,3
            do j=1,ndi
              QuRefBlock  (i,j,KRefBlock)= Uqp(i+(j-1)*3)
              QuRefBlockSU(i,j,KRefBlock)=SUqp(i+(j-1)*3)
            enddo
          enddo
          mm=mm+1
          call DRGrIndexTryInd
          if(.not.EqIV(FlagI,FlagIOld,GrIndexNAll).and.mm.lt.50) then
            call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
            if(CrSystemRest.gt.1.and.ndi.le.0) then
              call CopyVekI(FlagI,FlagIOld,GrIndexNAll)
              go to 2500
            else
              go to 2000
            endif
          else
            if(CheckNumber.eq.nButtRefineCell) then
              if(Index2d) then
                nn=3
              else
                nn=6+3*ndi
              endif
              n=0
              Suma=0.
2100          cpp(1)=CellRefBlock(1,KRefBlock)
              cpp(2)=CellRefBlock(2,KRefBlock)
              cpp(3)=CellRefBlock(3,KRefBlock)
              cpp(4)=cos(CellRefBlock(4,KRefBlock)*ToRad)
              cpp(5)=cos(CellRefBlock(5,KRefBlock)*ToRad)
              cpp(6)=cos(CellRefBlock(6,KRefBlock)*ToRad)
              sinap=sqrt(1.-cpp(4)**2)
              sinbp=sqrt(1.-cpp(5)**2)
              singp=sqrt(1.-cpp(6)**2)
              call Recip(cpp,rcpp,V)
              VolP=V/(cpp(1)*cpp(2)*cpp(3))
              sinarp=sqrt(1.-rcpp(4)**2)
              sinbrp=sqrt(1.-rcpp(5)**2)
              singrp=sqrt(1.-rcpp(6)**2)
              cosarp=rcpp(4)
              cosbrp=rcpp(5)
              cosgrp=rcpp(6)
              cotarp=cosarp/sinarp
              cotbrp=cosbrp/sinbrp
              cotgrp=cosgrp/singrp
              do j=1,3
                gi(j,j)=rcpp(j)**2
              enddo
              gi(1,2)=rcpp(1)*rcpp(2)*rcpp(6)
              gi(1,3)=rcpp(1)*rcpp(3)*rcpp(5)
              gi(2,3)=rcpp(2)*rcpp(3)*rcpp(4)
              gi(2,1)=gi(1,2)
              gi(3,1)=gi(1,3)
              gi(3,2)=gi(2,3)
              n=n+1
              AM=0.
              PS=0.
              Suma=0.
              do i=1,GrIndexNAll
                if(FlagI(i).le.0.or.FlagU(i).le.0) cycle
                pom=0.
                do j=1,nd
                  hp(j)=iha(j,i)
                  if(j.gt.3) cycle
                  do k=1,ndi
                    hp(j)=hp(j)+
     1                    QuRefBlock(j,k,KDatBlock)*float(iha(k+3,i))
                  enddo
                  pom=pom+abs(hp(j))
                enddo
                if(pom.lt..99) cycle
                dr=0.
                do j=1,3
                  do k=1,3
                    dr=dr+gi(j,k)*hp(j)*hp(k)
                  enddo
                enddo
                dr=sqrt(dr)
                ddr=.5/dr
                dy=Slozky(4,i)-dr
                Suma=Suma+dy**2
                do j=1,6
                  call indext(j,k,l)
                  ddrdgi(j)=ddr*hp(k)*hp(l)
                  if(k.ne.l) ddrdgi(j)=2.*ddrdgi(j)
                enddo
                do j=1,3
                  ddrdrcp(j)=2.*ddrdgi(j)*rcpp(j)
                  do k=1,3
                    if(j.le.2) then
                      l=k
                    else
                      l=k-1
                    endif
                    if(k.eq.4-j) cycle
                    ddrdrcp(j)=ddrdrcp(j)+
     1                         ddrdgi(k+3)*rcpp(7-k)*rcpp(l)
                  enddo
                enddo
                do j=4,6
                  call indext(j,k,l)
                  ddrdrcp(10-j)=ddrdgi(j)*rcpp(k)*rcpp(l)
                enddo
                do j=1,nd
                  der(j)=-ddrdrcp(j)*rcpp(j)/cpp(j)
                enddo
                if(Index2d) then
                  der(3)=((ddrdrcp(3)/cpp(3)*cotarp*cotbrp+
     1                     ddrdrcp(1)/cpp(1)*cotgrp/sinbrp+
     2                     ddrdrcp(2)/cpp(2)*cotgrp/sinarp)/VolP+
     3                     ddrdrcp(6)*singp/(sinap*sinbp)+
     4                     ddrdrcp(4)*cosbrp*sinap/(singp*sinbp)+
     5                     ddrdrcp(5)*cosarp*sinbp/(singp*sinap))*torad
                else
                  der(4)=((ddrdrcp(1)/cpp(1)*cotbrp*cotgrp+
     1                     ddrdrcp(2)/cpp(2)*cotarp/singrp+
     2                     ddrdrcp(3)/cpp(3)*cotarp/sinbrp)/VolP+
     3                     ddrdrcp(4)*sinap/(sinbp*singp)+
     4                     ddrdrcp(5)*cosgrp*sinbp/(sinap*singp)+
     5                     ddrdrcp(6)*cosbrp*singp/(sinap*sinbp))*torad
                  der(5)=((ddrdrcp(2)/cpp(2)*cotgrp*cotarp+
     1                     ddrdrcp(3)/cpp(3)*cotbrp/sinarp+
     2                     ddrdrcp(1)/cpp(1)*cotbrp/singrp)/VolP+
     3                     ddrdrcp(5)*sinbp/(sinap*singp)+
     4                     ddrdrcp(4)*cosgrp*sinap/(sinbp*singp)+
     5                     ddrdrcp(6)*cosarp*singp/(sinbp*sinap))*torad
                  der(6)=((ddrdrcp(3)/cpp(3)*cotarp*cotbrp+
     1                     ddrdrcp(1)/cpp(1)*cotgrp/sinbrp+
     2                     ddrdrcp(2)/cpp(2)*cotgrp/sinarp)/VolP+
     3                     ddrdrcp(6)*singp/(sinap*sinbp)+
     4                     ddrdrcp(4)*cosbrp*sinap/(singp*sinbp)+
     5                     ddrdrcp(5)*cosarp*sinbp/(singp*sinap))*torad
                endif
                m=6
                do j=1,ndi
                  do k=1,3
                    m=m+1
                    k1=mod(k  ,3)+1
                    k2=mod(k+1,3)+1
                    der(m)=2.*ddr*(gi(k,k )*hp(k )*hp(j+3)+
     1                             gi(k,k1)*hp(k1)*hp(j+3)+
     2                             gi(k,k2)*hp(k2)*hp(j+3))
                  enddo
                enddo
                m=0
                do j=1,nn
                  do k=1,j
                    m=m+1
                    AM(m)=AM(m)+der(j)*der(k)
                  enddo
                  PS(j)=PS(j)+der(j)*dy
                enddo
              enddo
              call smi(AM,nn,ising)
              if(ising.ne.0) then
                Veta='the refinement failed - singularity'
                call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
                go to 1500
              endif
              CellD=0.
              call nasob(AM,PS,CellD,nn)
              Suma=sqrt(Suma/float(IndexedAll-6-3*ndi))
              pom=0.
              k=0
              if(Index2d) then
                do i=1,2
                  k=k+i
                  CellRefBlock(i,KRefBlock)=CellRefBlock(i,KRefBlock)+
     1                                      CellD(i)
                  CellRefBlockSU(i,KRefBlock)=Suma*sqrt(AM(k))
                  pom=pom+abs(CellD(i))
                enddo
                CellRefBlock(6,KRefBlock)=CellRefBlock(6,KRefBlock)+
     1                                    CellD(3)
                pom=pom+abs(CellD(3))*.1
              else
                do i=1,6
                  k=k+i
                  CellRefBlock(i,KRefBlock)=CellRefBlock(i,KRefBlock)+
     1                                      CellD(i)
                  CellRefBlockSU(i,KRefBlock)=Suma*sqrt(AM(k))
                  if(i.le.3) then
                    pom=pom+abs(CellD(i))
                  else
                    pom=pom+abs(CellD(i))*.1
                  endif
                enddo
              endif
              m=6
              kk=6
              do i=1,ndi
                do j=1,3
                  kk=kk+1
                  k=k+kk
                  m=m+1
                  QuRefBlock(j,i,KDatBlock)=
     1              QuRefBlock(j,i,KDatBlock)+CellD(m)
                  QuRefBlockSU(j,i,KDatBlock)=Suma*sqrt(AM(k))
                  pom=pom+abs(CellD(i))
                enddo
              enddo
              if(n.lt.100.and.pom.gt..00001) go to 2100
              cosap=cos(CellRefBlock(4,KRefBlock)*ToRad)
              cosbp=cos(CellRefBlock(5,KRefBlock)*ToRad)
              cosgp=cos(CellRefBlock(6,KRefBlock)*ToRad)
              CellVolume=CellRefBlock(1,KRefBlock)*
     1                   CellRefBlock(2,KRefBlock)*
     2                   CellRefBlock(3,KRefBlock)*
     3                   sqrt(1.-cosap**2-cosbp**2-cosgp**2+
     4                        2.*cosap*cosbp*cosgp)
              do i=1,3
                DRMetTens(i,i)=CellRefBlock(i,KRefBlock)**2
              enddo
              DRMetTens(1,2)=CellRefBlock(1,KRefBlock)*
     1                       CellRefBlock(2,KRefBlock)*cosgp
              DRMetTens(1,3)=CellRefBlock(1,KRefBlock)*
     1                       CellRefBlock(3,KRefBlock)*cosbp
              DRMetTens(2,3)=CellRefBlock(2,KRefBlock)*
     1                       CellRefBlock(3,KRefBlock)*cosap
              DRMetTens(2,1)=DRMetTens(1,2)
              DRMetTens(3,1)=DRMetTens(1,3)
              DRMetTens(3,2)=DRMetTens(2,3)
            else
              call DRCellFromUB
              if(CrSystemRest.gt.1.and.ndi.le.0) then
                if(CrSystemPom.eq.CrSystemMonoclinic) then
                  if(Mono.eq.1) then
                    CellRefBlock  (5,KRefBlock)=90.
                    CellRefBlockSU(5,KRefBlock)=0.
                    CellRefBlock  (6,KRefBlock)=90.
                    CellRefBlockSU(6,KRefBlock)=0.
                  else if(Mono.eq.2) then
                    CellRefBlock  (4,KRefBlock)=90.
                    CellRefBlockSU(4,KRefBlock)=0.
                    CellRefBlock  (6,KRefBlock)=90.
                    CellRefBlockSU(6,KRefBlock)=0.
                  else if(Mono.eq.3) then
                    CellRefBlock  (4,KRefBlock)=90.
                    CellRefBlockSU(4,KRefBlock)=0.
                    CellRefBlock  (5,KRefBlock)=90.
                    CellRefBlockSU(5,KRefBlock)=0.
                  endif
                else if(CrSystemPom.eq.CrSystemOrthorhombic.or.
     1                  CrSystemPom.eq.CrSystemTetragonal.or.
     2                  CrSystemPom.eq.CrSystemCubic) then
                  CellRefBlock  (4,KRefBlock)=90.
                  CellRefBlockSU(4,KRefBlock)=0.
                  CellRefBlock  (5,KRefBlock)=90.
                  CellRefBlockSU(5,KRefBlock)=0.
                  CellRefBlock  (6,KRefBlock)=90.
                  CellRefBlockSU(6,KRefBlock)=0.
                else if(CrSystemPom.eq.CrSystemHexagonal) then
                  CellRefBlock  (4,KRefBlock)=90.
                  CellRefBlockSU(4,KRefBlock)=0.
                  CellRefBlock  (5,KRefBlock)=90.
                  CellRefBlockSU(5,KRefBlock)=0.
                  CellRefBlock  (6,KRefBlock)=120.
                  CellRefBlockSU(6,KRefBlock)=0.
                else if(CrSystemPom.eq.CrSystemTrigonal) then
                  pom=0.
                  poms=0.
                  do i=1,3
                    pom=pom+CellRefBlock(i+3,KRefBlock)
                    poms=poms+CellRefBlockSU(i+3,KRefBlock)
                  enddo
                  pom=pom/3.
                  poms=poms/3.
                  do i=1,3
                    CellRefBlock  (i+3,KRefBlock)=pom
                    CellRefBlockSU(i+3,KRefBlock)=poms
                  enddo
                endif
                if(CrSystemPom.eq.CrSystemTetragonal.or.
     1             CrSystemPom.eq.CrSystemHexagonal.or.
     2             CrSystemPom.eq.CrSystemTrigonal.or.
     3             CrSystemPom.eq.CrSystemCubic) then
                  if(CrSystemPom.eq.CrSystemTrigonal.or.
     1               CrSystemPom.eq.CrSystemCubic) then
                    i2=3
                  else
                    i2=2
                  endif
                  pom=0.
                  poms=0.
                  do i=1,i2
                    pom=pom+CellRefBlock(i,KRefBlock)
                    poms=poms+CellRefBlockSU(i,KRefBlock)
                  enddo
                  pom=pom/float(i2)
                  poms=poms/float(i2)
                  do i=1,i2
                    CellRefBlock  (i,KRefBlock)=pom
                    CellRefBlockSU(i,KRefBlock)=poms
                  enddo
                endif
                if(CrSystemPom.eq.FixedCell) then
                  if(AllowScalCell) then
                    pom=0.
                    poms=0.
                    pomp=0.
                    do i=1,3
                      pom=pom+CellRefBlock(i,KRefBlock)/CellParRest(i)
                      poms=poms+CellRefBlockSU(i,KRefBlock)
                      pomp=pomp+CellParRest(i)
                    enddo
                    pom=pom/3.
                    do i=1,3
                      CellRefBlock  (i,KRefBlock)=CellParRest(i)*pom
                      CellRefBlockSU(i,KRefBlock)=
     1                  poms*CellParRest(i)/pomp
                    enddo
                  else
                    do i=1,3
                      CellRefBlock(i,KRefBlock)=CellParRest(i)
                      CellRefBlockSU(i,KRefBlock)=0.
                    enddo
                  endif
                  do i=4,6
                    CellRefBlock(i,KRefBlock)=CellParRest(i)
                    CellRefBlockSU(i,KRefBlock)=0.
                  enddo
                endif
              endif
            endif
            deallocate(FlagIOld)
            go to 1100
          endif
        else if(CheckNumber.eq.nButtListing) then
          call OpenFile(lst,FileIndex(:idel(FileIndex))//'.index',
     1                  'formatted','unknown')
          LstOpened=.true.
          Uloha='Cell refinement and indexing report: '//
     1           FileIndex(:idel(FileIndex))
          call NewPg(1)
          call NewLn(5)
          write(lst,'(10x,''Orientation matrix :'',
     1                45x,''Metric tensor :''/)')
          do i=1,3
            write(lst,'(5x,3f10.6,30x,3f10.4)')
     1        (UB(i,j,KRefBlock),j=1,3),(DRMetTens(i,j),j=1,3)
          enddo
          call NewLn(3)
          write(lst,FormA)
          write(lst,'(''Cell parameters :'',3f10.4,3f10.3)')
     1      (CellRefBlock  (i,KRefBlock),i=1,6)
          write(lst,'(''s.u.            :'',3f10.4,3f10.3)')
     1      (CellRefBlockSU(i,KRefBlock),i=1,6)
          if(ndi.gt.0) then
            call NewLn(3*ndi)
            do i=1,ndi
              write(lst,FormA)
              write(lst,'(''Modulation vector#'',i1,'' :'',3f8.4)')
     1          i,(QuRefBlock(j,i,KRefBlock),j=1,3)
              write(lst,'(''s.u.                :'',3f8.4)')
     1          (QuRefBlockSU(j,i,KRefBlock),j=1,3)
            enddo
          endif
          call NewLn(3)
          write(lst,FormA)
          write(lst,FormA) '     '//TextInfo1(:idel(TextInfo1))
          write(lst,FormA)
          call NewLn(3)
          write(lst,FormA)
          Veta='    No.       Real indices'
          i=2+nd*2
          Veta(idel(Veta)+i:)='Indices'
          i=-2+nd*2
          Veta(idel(Veta)+i:)='Intensity'
          write(lst,'(2a65)') Veta,Veta
          write(lst,FormA)
          do i=1,GrIndexNAll
            if(FlagU(i).le.0) cycle
            if(mod(i,2).eq.1) then
              idl=1
            else
              idl=66
            endif
            write(Veta(idl:),'(i6,3f8.3,2(6x,a1,3x))') i,(ha(j,i),j=1,3)
            if(FlagI(i).le.0) then
              write(Veta(idel(Veta)+1:),'(6a4)')
     1          ('   -',j=1,nd)
            else
              write(Veta(idel(Veta)+1:),'(6i4)')
     1          (iha(j,i),j=1,nd)
            endif
            write(Veta(idel(Veta)+1:),'(i10)') nint(Int(i))
            if(idl.gt.1.or.i.eq.GrIndexNAll) then
              call NewLn(1)
              write(lst,FormA) Veta(:idel(Veta))
            endif
          enddo
          call CloseListing
          call FeListView(FileIndex(:idel(FileIndex))//'.index',0)
          go to 1500
        endif
        call MultM(UBStore(1,1,NActual),T,UB(1,1,KRefBlock),3,3,3)
        do i=1,ndi
          call MultM(QuStore(1,i,NActual),TQ,xp,1,3,3)
          call CopyVek(xp,QuRefBlock(1,i,KPhase),3)
        enddo
        call DRCellFromUB
        call Matinv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),CellVolume,3)
        if(CellVolume.lt.0.) then
          call RealMatrixToOpposite(UB(1,1,KRefBlock),UB(1,1,KRefBlock),
     1                              3)
          call RealMatrixToOpposite(UBI(1,1,KRefBlock),
     1                              UBI(1,1,KRefBlock),3)
          CellVolume=-CellVolume
        endif
        CellVolume=1./CellVolume
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),pom,3)
        do i=1,3
          do j=1,3
            CellDir(j,i)=UBI(i,j,KRefBlock)/CellRefBlock(i,KRefBlock)
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) call CopyMat(UBIn,UB(1,1,KRefBlock),3)
      call FeTabsReset(UseTabsM)
      call FeTabsReset(UseTabsC)
      UseTabs=UseTabsIn
      return
100   format(a,3(a1,f9.4),3(a1,f8.3))
101   format(a,9(a1,f9.4))
103   format(i6,'/',i6)
      end
