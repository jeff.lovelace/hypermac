      subroutine DRReflForAbsCorrSetting(Klic,Change,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer NMen(11),CrSystemFromCell,CrSystemReduced,CrSystemPure,
     1        CrSystemAxis,RolMenuSelectedQuest,EdwStateQuest,
     2        RunScGrOld,SpHEvenOld,SpHOddOld,RunScGrP,SpHEvenP,SpHOddP,
     3        RunScNOld,SpHNOld,ScFrMethodOld
      logical DefinePointGroup,EqIgCase,CrwLogicQuest,FeYesNoHeader,
     1        lpom,Change,WizardModeOld,RunOpt,EqRV,ExistFile
      character*256 Veta
      character*80  t80,FormulaSave
      character*22 :: CrSystemNameP(11) = (/'Triclinic             ',
     1                                      'Monoclinic-setting "a"',
     2                                      'Monoclinic-setting "b"',
     3                                      'Monoclinic-setting "c"',
     4                                      'Orthorhombic          ',
     5                                      'Tetragonal-setting "a"',
     6                                      'Tetragonal-setting "b"',
     7                                      'Tetragonal-setting "c"',
     8                                      'Trigonal              ',
     9                                      'Hexagonal             ',
     a                                      'Cubic                 '/)
      character*8  MenPG(8),PGSel,PGAveOld,PGPom
      real CellPom(6),ToOrtho(9),FromOrtho(9),TrPom(9),TrPomI(9),
     1     TrPom6(36),TrPom6I(36),MatPom(36),QuIn(3,3),QuIrr(3,3),
     2     QuRac(3,3),CutMaxAveOld,CutMinAveOld,AlmOld(81)
      real, allocatable :: ScFrameOld(:)
      write(FormatEQV(2:2),'(i1)') maxNDim
      DefinePointGroup=.false.
      PGSel=' '
      PGAveOld=PGAve(KRefBlock)
      WizardModeOld=WizardMode
      WizardMode=.false.
1000  id=NextQuestId()
      il=13
      if(Klic.eq.1) then
        il=il+5
        CutMaxAveOld=CutMaxAve(KRefBlock)
        CutMinAveOld=CutMinAve(KRefBlock)
        FLimCullAveOld=FLimCullAve(KRefBlock)
        NRefMaxAveOld=NRefMaxAve(KRefBlock)
        RunScGrOld=RunScGr(KRefBlock)
        SpHEvenOld=SpHEven(KRefBlock)
        SpHOddOld=SpHOdd(KRefBlock)
        RunScNOld=RunScN(KRefBlock)
        SpHNOld=SpHN(KRefBlock)
        ScFrMethodOld=ScFrMethod(KRefBlock)
        if(RunScNOld.gt.0) then
          if(allocated(ScFrameOld)) deallocate(ScFrameOld)
          allocate(ScFrameOld(RunScNOld))
          ScFrameOld(1:RunScNOld)=ScFrame(1:RunScNOld,KRefBlock)
        endif
        SpHNOld=SpHN(KRefBlock)
        if(SpHNOld.gt.1)
     1    AlmOld(1:SpHNOld)=SpHAlm(1:SpHNOld,KRefBlock)
        Change=.false.
      else
        call DeleteFile(fln(:ifln)//'.eqv')
        call DeleteFile(fln(:ifln)//'.crs')
        Change=.true.
      endif
      if(ExistM50.and.NSymmN(KPhase).gt.0) il=il+2
      xqd=540.
      if(Klic.eq.0) then
        Veta='Define selection criteria for X-shape'
      else if(Klic.eq.1) then
        Veta='Define parameters for frame scale and/or empirical '//
     1       'absorption correction'
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=0
      xpom=5.
      tpom=xpom+CrwXd+10.
      t80='%Apply cutoff for weak reflections'
      Veta='=> Reflections with I%<'
      tpome=tpom+FeTxLengthUnder(t80)+10.
      xpome=tpome+FeTxLengthUnder(Veta)+30.
      dpome=60.
      if(Klic.eq.1) then
        pom=CutMinAve(KRefBlock)
        if(pom.lt.0.) pom=3.
        lpom=CutMinAve(KRefBlock).gt.0.
      else
        pom=CutMinXShape
        if(pom.lt.0.) then
          CutMinXShape=20.
          pom=20.
        endif
        lpom=CutMinXShape.gt.0.
      endif
      do i=1,4
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(CrwLastMade,lpom)
        call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpome,EdwYd,0)
        if(i.le.3) then
          call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
          if(i.eq.1) then
            t80='*sig(I) will be skipped'
          else if(i.eq.2) then
            t80='% of I(max) will be skipped'
          else if(i.eq.3) then
            t80='*sig(I(ave)) will be culled'
          endif
          call FeQuestLblMake(id,xpome+dpome+5.,il,t80,'L','N')
        else
          call FeQuestIntEdwOpen(EdwLastMade,npom,.false.)
        endif
        if(.not.lpom) then
          call FeQuestEdwDisable(EdwLastMade)
          if(i.le.3) call FeQuestLblDisable(LblLastMade)
        endif
        if(i.eq.1) then
          t80='Appl%y cutoff for strong reflections'
          Veta='=> Reflections with I%>'
          if(Klic.eq.1) then
            pom=CutMaxAve(KRefBlock)
            if(pom.lt.0.) pom=90.
            lpom=CutMaxAve(KRefBlock).gt.0.
          else
            pom=CutMaxXShape
            if(pom.lt.0.) then
              pom=80.
              CutMaxXShape=80
            endif
            lpom=CutMaxXShape.gt.0.
          endif
          nCrwCutMinAve=CrwLastMade
          nEdwCutMinAve=EdwLastMade
          nLblCutMinAve=LblLastMade
        else if(i.eq.2) then
          t80='A%pply culling'
          Veta='=> Reflections |I-I(ave)|>'
          if(Klic.eq.1) then
            pom=FLimCullAve(KRefBlock)
            if(pom.lt.0.) pom=20.
          else
            pom=FLimCullXShape
            if(pom.lt.0.) pom=20.
            lpom=CutMaxXShape.gt.0.
          endif
          nCrwCutMaxAve=CrwLastMade
          nEdwCutMaxAve=EdwLastMade
          nLblCutMaxAve=LblLastMade
          lpom=FLimCullAve(KRefBlock).gt.0.
        else if(i.eq.3) then
          t80='Limit maximal %number of unique reflections'
          tpome=tpome+50.
          if(Klic.eq.1) then
            npom=NRefMaxAve(KRefBlock)
            if(NRefMaxAve(KRefBlock).le.0) npom=1000
            lpom=NRefMaxAve(KRefBlock).gt.0
          else
            npom=NRefMaxXShape
            if(npom.le.0) then
              NRefMaxXShape=200
              npom=200
            endif
            lpom=NRefMaxXShape.gt.0
          endif
          Veta='=>'
          nCrwCullAve=CrwLastMade
          nEdwCullAve=EdwLastMade
          nLblCullAve=LblLastMade
        else
          t80='Limit maximal %number of unique reflections'
          nCrwNRefMaxAve=CrwLastMade
          nEdwNRefMaxAve=EdwLastMade
        endif
      enddo
      if(Klic.eq.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        tpom=5.
        Veta='Create file for %X-shape'
        dpom=FeTxLengthUnder(Veta)+20.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtXShape=ButtonLastMade
        tpom=tpom+dpom+20.
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        nLblXShape=LblLastMade
        il=il+1
        Veta='Note that X-Shape will cut the reflection list when the'//
     1       ' total number exceeds 3000 (2000 in older versions)'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      else
        nButtXShape=0
        nLblXShape=0
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      if(Klic.eq.1) then
        xpom=5.
        tpom=xpom+CrwXd+10.
        Veta='%Scale frames'
        tpome=tpom+220.
        xpome=tpome+60.
        dpome=30.
        do i=1,2
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          if(i.eq.1) then
            nCrwScales=CrwLastMade
            tpomp=tpom+FeTxLengthUnder(Veta)+15.
            Veta='Set'
            call FeQuestLblMake(id,tpomp,il,Veta,'L','N')
            nLblFrameGr=LblLastMade
            xpomp=tpomp+FeTxLengthUnder(Veta)+10
            tpomp=xpomp+dpome+20.
            Veta='frames=%1 scale'
            call FeQuestEudMake(id,tpomp,il,xpomp,il,Veta,'L',dpome,
     1                          EdwYd,1)
            nEdwFrameGr=EdwLastMade
            call FeQuestIntEdwOpen(EdwLastMade,RunScGr(KRefBlock),
     1                             .false.)
            n=0
            do j=1,NRuns(KRefBlock)
              n=n+RunNFrames(j,KRefBlock)
            enddo
            call FeQuestEudOpen(EdwLastMade,1,n,1,0.,0.,0.)
            if(.not.lpom) then
              call FeQuestLblDisable(nLblFrameGr)
              call FeQuestEdwDisable(EdwLastMade)
            endif
            tpomp=tpomp+FeTxLengthUnder(Veta)+15.
            Veta='Run coverage %test'
            dpomp=FeTxLengthUnder(Veta)+20.
            call FeQuestButtonMake(id,tpomp,il,dpomp,ButYd,Veta)
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            if(.not.lpom) call FeQuestButtonDisable(ButtonLastMade)
            nButtCovTest=ButtonLastMade
            tpomp=tpomp+dpomp+15.
            Veta='Dra%w scale graph'
            dpomp=FeTxLengthUnder(Veta)+20.
            call FeQuestButtonMake(id,tpomp,il,dpomp,ButYd,Veta)
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            if(.not.lpom) call FeQuestButtonDisable(ButtonLastMade)
            nButtScaleGraph=ButtonLastMade
            xpomp=100.
            tpomp=xpomp+CrwXd+10.
            il=il+1
            Veta='Use step-li%ke correction'
            do ii=1,2
              call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwgXd,
     1                            CrwgYd,1,1)
              call FeQuestCrwOpen(CrwLastMade,
     1                            ii-1.eq.ScFrMethod(KRefBlock))
              if(ii.eq.1) then
                Veta='Use linear appro%ximation '
                nCrwStepLike=CrwLastMade
              else
                nCrwLinear=CrwLastMade
              endif
              pom=FeTxLength(Veta)+30.
              xpomp=xpomp+pom
              tpomp=tpomp+pom
            enddo
            Veta='Optimize coefficients of sherical %harmonics'
            PomLen=FeTxLengthUnder(Veta)
            lpom=RunScGr(KRefBlock).gt.0
            CrwLastMade=nCrwScales
          else
            nCrwSpHarm=CrwLastMade
            Veta='Nu%mber of even harmonics:'
            xpome=tpome+FeTxLengthUnder(Veta)+10.
            dpome=25.
            call FeQuestEudMake(id,tpome,il,xpome,il,Veta,'L',dpome,
     1                          EdwYd,0)
            nEdwEvenHarm=EdwLastMade
            j=SpHEven(KRefBlock)
            if(j.le.0) j=8
            call FeQuestIntEdwOpen(EdwLastMade,j,.false.)
            call FeQuestEudOpen(EdwLastMade,2,8,2,0.,0.,0.)
            il=il+1
            Veta='Us%e odd harmonics'
            tpom=tpom+PomLen-FeTxLengthUnder(Veta)
            xpom=tpom-CrwXd-10.
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                          1,0)
            call FeQuestCrwOpen(CrwLastMade,SpHOdd(KRefBlock).gt.0)
            nCrwUseOddHarm=CrwLastMade
            Veta='Num%ber of odd harmonics:'
            call FeQuestEudMake(id,tpome,il,xpome,il,Veta,'L',dpome,
     1                          EdwYd,0)
            nEdwOddHarm=EdwLastMade
            call FeQuestIntEdwOpen(EdwLastMade,SpHOdd(KRefBlock),
     1                             .false.)
            call FeQuestEudOpen(EdwLastMade,1,7,2,0.,0.,0.)
            if(SpHOdd(KRefBlock).le.0)
     1        call FeQuestEdwDisable(EdwLastMade)
            CrwLastMade=nCrwSpHarm
            lpom=SpHEven(KRefBlock).gt.0
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if((i.eq.1.and.NRuns(KRefBlock).le.0).or.
     1       (i.eq.2.and..not.MameDirCos))
     2      call FeQuestCrwDisable(CrwLastMade)
          il=il+1
          call FeQuestLinkaMake(id,il)
        enddo
      else
        nCrwScales=0
        nCrwSpHarm=0
        nEdwEvenHarm=0
        nCrwUseOddHarm=0
        nEdwOddHarm=0
        nLblFrameGr=0
        nEdwFrameGr=0
        nButtCovTest=0
        nButtScaleGraph=0
        nCrwStepLike=0
        nCrwLinear=0
      endif
      if(ExistM50.and.NSymmN(KPhase).gt.0) then
        xpom=5.
        tpom=xpom+CrwgXd+3.
        do i=1,2
          il=il+1
          if(i.eq.1) then
            call GetPointGroupFromSpaceGroup(PGPom,ii,jj)
            Veta='%Use the point group from m50: '//PGPom(:idel(PGPom))
          else if(i.eq.2) then
            Veta='%Define point group'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,1,2)
          if(i.eq.1) then
            nCrwSymmM50=CrwLastMade
          else
            nCrwSymmDef=CrwLastMade
          endif
          DefinePointGroup=.not.EqIgCase(PGAve(KRefBlock),PGPom).and.
     1                     PGAve(KRefBlock).ne.' '
          call FeQuestCrwOpen(CrwLastMade,i.eq.2.eqv.DefinePointGroup)
          if(DefinePointGroup) then
            IDefPG=1
          else
            IDefPG=0
          endif
        enddo
      else
        DefinePointGroup=.true.
        IDefPG=0
        il=il+1
        nCrwSymmM50=0
        nCrwSymmDef=0
      endif
      ill=il
      il=il+1
      tpom=5.
      Veta='C%rystal system:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=0.
      do i=1,11
        dpom=max(dpom,FeTxLength(CrSystemNameP(i)))
      enddo
      dpom=dpom+EdwYd+2.*EdwMarginSize
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuCrSystem=RolMenuLastMade
      il=il+1
      Veta='P%oint group:'
      dpom=70.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuPG=RolMenuLastMade
      il=ill
      call FeQuestLblMake(id,xqd*.75,ill,'Tolerances for crystal '//
     1                    'system recognition:','C','B')
      call FeQuestLblOff(LblLastMade)
      nLblTol=LblLastMade
      Veta='Maximal deviation for %cell lengths in [A]'
      tpom=xqd*.5
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      if(NDimI(KPhase).gt.0) then
        ik=3
      else
        ik=2
        nEwdDiffModVec=0
      endif
      do i=1,ik
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        if(i.eq.1) then
          pom=DiffAxe
          Veta='Maximal deviation for cell an%gles in deg'
          nEwdDiffAxe=EdwLastMade
        else if(i.eq.2) then
          pom=DiffAngle
          Veta='Maximal deviation for modulation %vector'
          nEwdDiffAngle=EdwLastMade
        else
          pom=DiffModVec
          nEwdDiffModVec=EdwLastMade
        endif
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      if(Klic.eq.1) then
        tpom=5.
        Veta='Set re%finement options'
        dpom=FeTxLengthUnder(Veta)+20.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtOptions=ButtonLastMade
        il=il+1
        Veta='Run optimali%zation'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtOptimize=ButtonLastMade
        tpom=tpom+dpom+20.
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        nLblRFac=LblLastMade
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Return to previous parameters'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=xqd*.5-dpom-10.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtPrevious=ButtonLastMade
        Veta='Accept optimized parameters'
        tpom=tpom+dpom+10.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtAccept=ButtonLastMade
        nButtOmitXShape=0
        nButtRunXShape=0
      else
        Veta='O%mit X-shape procedure'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=xqd*.5-dpom-10.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtOmitXShape=ButtonLastMade
        Veta='%Start X-shape program'
        tpom=tpom+dpom+10.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtRunXShape=ButtonLastMade
        nButtOptimize=0
        nButtOptions=0
        nLblRFac=0
        nButtPrevious=0
        nButtAccept=0
      endif
1400  if(DefinePointGroup) then
        call FeQuestLblOn(nLblTol)
        call FeQuestRealEdwOpen(nEwdDiffAxe,DiffAxe,.false.,.false.)
        call FeQuestRealEdwOpen(nEwdDiffAngle,DiffAngle,.false.,
     1                          .false.)
        if(nEwdDiffModVec.gt.0)
     1    call FeQuestRealEdwOpen(nEwdDiffModVec,DiffModVec,.false.,
     2                           .false.)
      else
        call FeQuestLblOff(nLblTol)
        call FeQuestEdwClose(nEwdDiffAxe)
        call FeQuestEdwClose(nEwdDiffAngle)
        if(nEwdDiffModVec.gt.0)
     1    call FeQuestEdwClose(nEwdDiffModVec)
        call FeQuestRolMenuClose(nRolMenuCrSystem)
        call FeQuestRolMenuClose(nRolMenuPG)
        PGSel=' '
      endif
      CrSystemSel=0
1420  if(DefinePointGroup) then
        call FeQuestRealFromEdw(nEwdDiffAxe,DiffAxe)
        call FeQuestRealFromEdw(nEwdDiffAngle,DiffAngle)
        if(nEwdDiffModVec.gt.0)
     1    call FeQuestRealFromEdw(nEwdDiffModVec,DiffModVec)
        CrSystemReduced=CrSystemFromCell(CellRefBlock(1,0),DiffAxe,
     1                                   DiffAngle)
        CrSystemPure=mod(iabs(CrSystemReduced),10)
        CrSystemAxis=iabs(CrSystemReduced)/10
        if(CrSystemAxis.le.0) CrSystemAxis=3
        call SetIntArrayTo(NMen,11,0)
        NMen(1)=1
        if(CrSystemPure.eq.CrSystemCubic) then
          call SetIntArrayTo(NMen(IdMonoclinicA),7,1)
          NMen(IdCubic-4)=1
        else if(CrSystemPure.eq.CrSystemHexagonal) then
          NMen(IdMonoclinicC)=1
          NMen(IdTrigonal)=1
          NMen(IdHexagonal-4)=1
        else if(CrSystemPure.eq.CrSystemTrigonal) then
          NMen(IdMonoclinicC)=1
          NMen(IdTrigonal)=1
        else if(CrSystemPure.eq.CrSystemTetragonal) then
          call SetIntArrayTo(NMen(IdMonoclinicA),4,1)
          NMen(CrSystemAxis+IdOrthorhombic)=1
        else if(CrSystemPure.eq.CrSystemOrthorhombic) then
          call SetIntArrayTo(NMen(IdMonoclinicA),4,1)
        else if(CrSystemPure.eq.CrSystemMonoclinic) then
          NMen(CrSystemAxis+IdTriclinic)=1
        endif
        do i=1,11
          if(NMen(i).ne.0) CrSystemSel=i
        enddo
      endif
1430  if(DefinePointGroup) then
        if(CrSystemSel.ge.IdCubic-4) then
          call CopyStringArray(SmbPGI(36),MenPG,5)
          NMenPG=5
        else if(CrSystemSel.ge.IdHexagonal-4) then
          call CopyStringArray(SmbPGI(28),MenPG,7)
          NMenPG=7
        else if(CrSystemSel.ge.IdTrigonal) then
          call CopyStringArray(SmbPGI(17),MenPG,11)
          NMenPG=8
        else if(CrSystemSel.ge.IdTetragonalA) then
          call CopyStringArray(SmbPGI(9),MenPG,8)
          NMenPG=8
        else if(CrSystemSel.ge.IdOrthorhombic) then
          MenPG(1)=SmbPGI(6)
          MenPG(2)='2mm'
          MenPG(3)='m2m'
          MenPG(4)='mm2'
          MenPG(5)=SmbPGI(8)
          NMenPG=5
        else if(CrSystemSel.ge.IdMonoclinicA) then
          call CopyStringArray(SmbPGI(3),MenPG,3)
          NMenPG=3
        else if(CrSystemSel.ge.IdTriclinic) then
          call CopyStringArray(SmbPGI,MenPG,2)
          NMenPG=2
        endif
        PGSel=MenPG(NMenPG)
        IMenPG=NMenPG
        if(IDefPG.eq.1) then
          do i=1,NMenPG
            if(EqIgCase(MenPG(i),PGAveOld)) then
              PGSel=PGAveOld
              IMenPG=i
              exit
            endif
          enddo
          IDefPG=0
        endif
        call FeQuestRolMenuWithEnableOpen(nRolMenuCrSystem,
     1                    CrSystemNameP,NMen,11,CrSystemSel)
      endif
1440  if(DefinePointGroup)
     1  call FeQuestRolMenuOpen(nRolMenuPG,MenPG,NMenPG,IMenPG)
      if(Klic.eq.0) go to 1500
1450  if(CrwLogicQuest(nCrwScales)) then
        if(EdwStateQuest(nEdwFrameGr).ne.EdwOpened) then
          if(RunScGr(KRefBlock).le.0) then
            RunScGr(KRefBlock)=4
1460        call DRSetRunScGrN(ich)
            if(ich.ne.0) then
              RunScGr(KRefBlock)=RunScGr(KRefBlock)+1
              go to 1460
            endif
            if(allocated(ScFrame)) deallocate(ScFrame)
            allocate(ScFrame(RunScN(KRefBlock),MxRefBlock))
            ScFrame(1:RunScN(KRefBlock),KRefBlock)=1.
          endif
          call FeQuestIntEdwOpen(nEdwFrameGr,RunScGr(KRefBlock),
     1                           .false.)
          call FeQuestLblOn(nLblFrameGr)
          call FeQuestButtonOff(nButtCovTest)
          call FeQuestButtonOff(nButtScaleGraph)
          nCrw=nCrwStepLike
          do i=1,2
            call FeQuestCrwOpen(nCrw,i-1.eq.ScFrMethod(KRefBlock))
            nCrw=nCrw+1
          enddo
        endif
      else
        if(EdwStateQuest(nEdwFrameGr).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwFrameGr,RunScGr(KRefBlock))
          call FeQuestEdwDisable(nEdwFrameGr)
          call FeQuestLblDisable(nLblFrameGr)
          call FeQuestButtonDisable(nButtCovTest)
          call FeQuestButtonDisable(nButtScaleGraph)
          nCrw=nCrwStepLike
          do i=1,2
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
        endif
      endif
1470  if(CrwLogicQuest(nCrwSpHarm)) then
        if(EdwStateQuest(nEdwEvenHarm).ne.EdwOpened) then
          j=SpHEven(KRefBlock)
          if(j.le.0) j=8
          call FeQuestIntEdwOpen(nEdwEvenHarm,j,.false.)
          call FeQuestCrwOpen(nCrwUseOddHarm,SpHOdd(KRefBlock).gt.0)
          if(EdwStateQuest(nEdwOddHarm).ne.EdwOpened.and.
     1       SpHOdd(KRefBlock).gt.0) then
            call FeQuestIntEdwOpen(nEdwOddHarm,SpHOdd(KRefBlock),
     1                             .false.)
          else
            call FeQuestEdwDisable(nEdwOddHarm)
          endif
        endif
      else
        if(EdwStateQuest(nEdwEvenHarm).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwEvenHarm,SpHEven(KRefBlock))
          call FeQuestEdwDisable(nEdwEvenHarm)
          if(CrwLogicQuest(nCrwUseOddHarm)) then
            call FeQuestIntFromEdw(nEdwOddHarm,SpHOdd(KRefBlock))
            call FeQuestEdwDisable(nEdwOddHarm)
          else
            SpHOdd(KRefBlock)=0
          endif
          call FeQuestCrwDisable(nCrwUseOddHarm)
        endif
      endif
1500  if(Klic.eq.1) then
        if(Change) then
          call FeQuestButtonOff(nButtAccept)
        else
          call FeQuestButtonDisable(nButtAccept)
        endif
      else
        if(ExistFile(fln(:ifln)//'.eqv')) then
          call FeQuestButtonOff(nButtRunXShape)
        else
          call FeQuestButtonDisable(nButtRunXShape)
        endif
      endif
      call FeQuestEvent(id,ich)
      RunOpt=.false.
      if(Klic.eq.0) then
        CovTest=0
      else
        CovTest=2
      endif
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCutMinAve) then
        if(CrwLogicQuest(CheckNumber).and.
     1     EdwStateQuest(nEdwCutMinAve).ne.EdwOpened) then
          pom=CutMinAve(KRefBlock)
          if(pom.lt.0.) pom=3.
          call FeQuestRealEdwOpen(nEdwCutMinAve,pom,.false.,.false.)
          call FeQuestLblOn(nLblCutMinAve)
        else if(.not.CrwLogicQuest(CheckNumber).and.
     1          EdwStateQuest(nEdwCutMinAve).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwCutMinAve,CutMinAve(KRefBlock))
          call FeQuestEdwDisable(nEdwCutMinAve)
          call FeQuestLblDisable(nLblCutMinAve)
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCutMaxAve)
     1  then
        if(CrwLogicQuest(CheckNumber).and.
     1     EdwStateQuest(nEdwCutMaxAve).ne.EdwOpened) then
          pom=CutMaxAve(KRefBlock)
          if(pom.lt.0.) pom=90.
          call FeQuestRealEdwOpen(nEdwCutMaxAve,pom,.false.,.false.)
          call FeQuestLblOn(nLblCutMaxAve)
        else if(.not.CrwLogicQuest(CheckNumber).and.
     1          EdwStateQuest(nEdwCutMaxAve).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwCutMaxAve,CutMaxAve(KRefBlock))
          call FeQuestEdwDisable(nEdwCutMaxAve)
          call FeQuestLblDisable(nLblCutMaxAve)
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCullAve)
     1  then
        if(CrwLogicQuest(CheckNumber).and.
     1     EdwStateQuest(nEdwCullAve).ne.EdwOpened) then
          pom=FLimCullAve(KRefBlock)
          if(pom.lt.0.) pom=20.
          call FeQuestRealEdwOpen(nEdwCullAve,pom,.false.,.false.)
          call FeQuestLblOn(nLblCullAve)
        else if(.not.CrwLogicQuest(CheckNumber).and.
     1          EdwStateQuest(nEdwCullAve).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwCullAve,FLimCullAve(KRefBlock))
          call FeQuestEdwDisable(nEdwCullAve)
          call FeQuestLblDisable(nLblCullAve)
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwNRefMaxAve) then
        if(CrwLogicQuest(CheckNumber).and.
     1     EdwStateQuest(nEdwNRefMaxAve).ne.EdwOpened) then
          j=NRefMaxAve(KRefBlock)
          if(j.lt.0) j=500
          call FeQuestIntEdwOpen(nEdwNRefMaxAve,j,.false.)
          call FeQuestIntFromEdw(nEdwNRefMaxAve,
     1                           NRefMaxAve(KRefBlock))
        else if(.not.CrwLogicQuest(CheckNumber).and.
     1          EdwStateQuest(nEdwNRefMaxAve).eq.EdwOpened) then
          call FeQuestEdwDisable(nEdwNRefMaxAve)
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwScales) then
        go to 1450
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSpHarm)
     1  then
        go to 1470
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseOddHarm)
     1  then
        if(CrwLogicQuest(nCrwUseOddHarm)) then
          if(EdwStateQuest(nEdwOddHarm).ne.EdwOpened) then
            SpHOdd(KRefBlock)=1
            call FeQuestIntEdwOpen(nEdwOddHarm,SpHOdd(KRefBlock),
     1                             .false.)
          endif
        else
          if(EdwStateQuest(nEdwOddHarm).eq.EdwOpened) then
            SpHOdd(KRefBlock)=0
            call FeQuestEdwDisable(nEdwOddHarm)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwSymmM50.or.CheckNumber.eq.nCrwSymmDef))
     2  then
        DefinePointGroup=CrwLogicQuest(nCrwSymmDef)
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwStepLike.or.
     2         CheckNumber.eq.nCrwLinear)) then
        if(CheckNumber.eq.nCrwStepLike) then
          ScFrMethod(KRefBlock)=ScFrMethodStepLike
        else
          ScFrMethod(KRefBlock)=ScFrMethodInterpol
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuCrSystem) then
        i=RolMenuSelectedQuest(nRolMenuCrSystem)
        if(CrSystemSel.ne.i) then
          CrSystemSel=i
          go to 1430
        else
          go to 1500
        endif
      else if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuPG)
     1  then
        IMenPG=RolMenuSelectedQuest(nRolMenuPG)
        if(.not.EqIgCase(PGSel,MenPG(IMenPG))) then
          PGSel=MenPG(IMenPG)
          go to 1440
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEwdDiffAxe) then
        call FeQuestRealFromEdw(nEwdDiffAxe,pom)
        if(DiffAxe.ne.pom) then
          DiffAxe=pom
          go to 1420
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwFrameGr) then
        i=RunScGr(KRefBlock)
        call FeQuestIntFromEdw(nEdwFrameGr,RunScGr(KRefBlock))
        call DRSetRunScGrN(ich)
        if(ich.ne.0) then
          write(Cislo,'(i5)') MxFrames
          call Zhusti(Cislo)
          call FeChybne(-1.,YBottomMessage,'The number of scale '//
     1                  'factors would exceed the limit '//
     2                  Cislo(:idel(Cislo))//'.',
     3                  'Please enlarge the number of frames in one '//
     4                  'scaling group.',SeriousError)
          RunScGr(KRefBlock)=i
          call FeQuestIntEdwOpen(nEdwFrameGr,RunScGr(KRefBlock),.false.)
        endif
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEwdDiffAngle)
     1  then
        call FeQuestRealFromEdw(nEwdDiffAngle,pom)
        if(DiffAngle.ne.pom) then
          DiffAngle=pom
          go to 1420
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEwdDiffModVec)
     1  then
        call FeQuestRealFromEdw(nEwdDiffModVec,pom)
        if(DiffModVec.ne.pom) then
          DiffModVec=pom
          go to 1420
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCovTest)
     1  then
        CovTest=1
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtScaleGraph) then
        call DRGrFrScales
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOptimize)
     1  then
        RunOpt=.true.
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOptions)
     1  then
        call DRAbsSemiEmpirOptions(-1)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPrevious)
     1  then
        CutMaxAve(KRefBlock)=CutMaxAveOld
        CutMinAve(KRefBlock)=CutMinAveOld
        NRefMaxAve(KRefBlock)=NRefMaxAveOld
        RunScGr(KRefBlock)=RunScGrOld
        SpHEven(KRefBlock)=SpHEvenOld
        SpHOdd(KRefBlock)=SpHOddOld
        RunScN(KRefBlock)=RunScNOld
        SpHN(KRefBlock)=SpHNOld
        PGAve(KRefBlock)=PGAveOld
        ScFrMethod(KRefBlock)=ScFrMethodOld
        if(RunScNOld.gt.0)
     1    ScFrame(1:RunScNOld,KRefBlock)=ScFrameOld(1:RunScNOld)
        if(SpHNOld.gt.1)
     1    SpHAlm(1:SpHNOld,KRefBlock)=AlmOld(1:SpHNOld)
        if(RunScNOld.gt.0) then
          if(SpHNOld.gt.1) then
            SEType=SEAll
          else
            SEType=SEScales
          endif
        else
          if(SpHNOld.gt.1) then
            SEType=SESpHarm
          endif
        endif
        Change=.false.
        ich=1
        nButtOmitXShape=0
        nButtRunXShape=0
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtOmitXShape) then
        ich=1
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAccept.or.
     2         CheckNumber.eq.nButtRunXShape)) then
        ich=0
        go to 9000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtXShape)
     1  then
        RunOpt=.true.
        ich=0
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.ne.0) then
        call FeQuestRemove(id)
        go to 9900
      endif
      if(Klic.eq.1) then
        if(CrwLogicQuest(nCrwScales)) then
          if(CrwLogicQuest(nCrwSpHarm)) then
            SEType=SEAll
          else
            SEType=SEScales
          endif
          call FeQuestIntFromEdw(nEdwFrameGr,RunScGr(KRefBlock))
        else
          if(CrwLogicQuest(nCrwSpHarm)) then
            SEType=SESpHarm
            CovTest=0
          else
            NInfo=1
            TextInfo(1)='You did not select any action.'
            if(FeYesNoHeader(-1.,-1.,'Do you really want to skip '//
     1                       'this correction?',0)) then
              go to 9900
            else
              go to 1500
            endif
          endif
          RunScGr(KRefBlock)=0
        endif
        if(CrwLogicQuest(nCrwSpHarm)) then
          call FeQuestIntFromEdw(nEdwEvenHarm,SpHEven(KRefBlock))
          if(CrwLogicQuest(nCrwUseOddHarm)) then
            call FeQuestIntFromEdw(nEdwOddHarm,SpHOdd(KRefBlock))
          else
            SpHOdd(KRefBlock)=0
          endif
        else
          SpHEven(KRefBlock)=0
          SpHOdd(KRefBlock)=0
        endif
      endif
      if(CrwLogicQuest(nCrwCutMinAve)) then
        call FeQuestRealFromEdw(nEdwCutMinAve,pom)
      else
        pom=-333.
      endif
      if(Klic.eq.1) then
        CutMinAve(KRefBlock)=pom
      else
        CutMinXShape=pom
      endif
      if(CrwLogicQuest(nCrwCutMaxAve)) then
        call FeQuestRealFromEdw(nEdwCutMaxAve,pom)
      else
        pom=-333.
      endif
      if(Klic.eq.1) then
        CutMaxAve(KRefBlock)=pom
      else
        CutMaxXShape=pom
      endif
      if(CrwLogicQuest(nCrwCullAve)) then
        call FeQuestRealFromEdw(nEdwCullAve,pom)
      else
        pom=-333.
      endif
      if(Klic.eq.1) then
        FLimCullAve(KRefBlock)=pom
      else
        FLimCullXShape=pom
      endif
      if(CrwLogicQuest(nCrwNRefMaxAve)) then
        call FeQuestIntFromEdw(nEdwNRefMaxAve,npom)
      else
        npom=-333
      endif
      if(Klic.eq.1) then
        NRefMaxAve(KRefBlock)=npom
      else
        NRefMaxXShape=npom
      endif
      if(ExistM50) then
        FormulaSave=Formula(KPhase)
        NUnitsSave=NUnits(KPhase)
        call iom50(0,0,fln(:ifln)//'.m50')
        Formula(KPhase)=FormulaSave
        NUnits(KPhase)=NUnitsSave
      endif
      call CrlSetTrFractToOrtho(CellRefBlock(1,0),ToOrtho)
      if(DefinePointGroup) then
        PGAve(KRefBlock)=PGSel
        call CopyVek(CellRefBlock(1,0),CellPom,6)
        i=123
        CrSystem(KPhase)=CrSystemTriclinic
        if(CrSystemSel.eq.IdMonoclinicA) then
          CellPom(1)=CellRefBlock(2,0)
          CellPom(2)=CellRefBlock(3,0)
          CellPom(3)=CellRefBlock(1,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=CellRefBlock(4,0)
          i=231
          CrSystem(KPhase)=CrSystemMonoclinic
        else if(CrSystemSel.eq.IdMonoclinicB) then
          CellPom(1)=CellRefBlock(3,0)
          CellPom(2)=CellRefBlock(1,0)
          CellPom(3)=CellRefBlock(2,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=CellRefBlock(5,0)
          i=312
          CrSystem(KPhase)=CrSystemMonoclinic
        else if(CrSystemSel.eq.IdMonoclinicC) then
          CellPom(4)=90.
          CellPom(5)=90.
          CrSystem(KPhase)=CrSystemMonoclinic
        else if(CrSystemSel.eq.IdOrthorhombic) then
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=90.
          if(EqIgCase(PGSel,'2mm')) then
            CellPom(1)=CellRefBlock(2,0)
            CellPom(2)=CellRefBlock(3,0)
            CellPom(3)=CellRefBlock(1,0)
            PGSel='mm2'
            i=231
          else if(EqIgCase(PGSel,'m2m')) then
            CellPom(1)=CellRefBlock(3,0)
            CellPom(2)=CellRefBlock(1,0)
            CellPom(3)=CellRefBlock(2,0)
            PGSel='mm2'
            i=312
          endif
          CrSystem(KPhase)=CrSystemOrthorhombic
        else if(CrSystemSel.eq.IdTetragonalA) then
          pom=(CellRefBlock(2,0)+CellRefBlock(3,0))*.5
          CellPom(1)=pom
          CellPom(2)=pom
          CellPom(3)=CellRefBlock(1,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=90.
          i=231
          CrSystem(KPhase)=CrSystemTetragonal
        else if(CrSystemSel.eq.IdTetragonalB) then
          pom=(CellRefBlock(1,0)+CellRefBlock(3,0))*.5
          CellPom(1)=pom
          CellPom(2)=pom
          CellPom(3)=CellRefBlock(2,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=90.
          i=312
          CrSystem(KPhase)=CrSystemTetragonal
        else if(CrSystemSel.eq.IdTetragonalC) then
          pom=(CellRefBlock(1,0)+CellRefBlock(2,0))*.5
          CellPom(1)=pom
          CellPom(2)=pom
          CellPom(3)=CellRefBlock(3,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=90.
          CrSystem(KPhase)=CrSystemTetragonal
        else if(CrSystemSel.eq.IdTrigonal.or.
     1          CrSystemSel.eq.IdHexagonal-4) then
          pom=(CellRefBlock(1,0)+CellRefBlock(2,0))*.5
          CellPom(1)=pom
          CellPom(2)=pom
          CellPom(3)=CellRefBlock(3,0)
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=120.
          if(CrSystemSel.eq.IdTrigonal) then
            CrSystem(KPhase)=CrSystemTrigonal
          else
            CrSystem(KPhase)=CrSystemHexagonal
          endif
        else if(CrSystemSel.eq.IdCubic-4) then
          pom=(CellRefBlock(1,0)+CellRefBlock(2,0)+CellRefBlock(3,0))/3.
          CellPom(1)=pom
          CellPom(2)=pom
          CellPom(3)=pom
          CellPom(4)=90.
          CellPom(5)=90.
          CellPom(6)=90.
          CrSystem(KPhase)=CrSystemCubic
        endif
        call SetPermutMat(TrPomI,3,i,ich)
        call MatInv(TrPomI,TrPom,pom,3)
        call MatFromBlock3(TrPomI,TrPom6I,NDim(KPhase))
        call MatInv(TrPom6I,TrPom6,pom,NDim(KPhase))
        call CrlSetTrFractToOrtho(CellPom,ToOrtho)
        call MatInv(ToOrtho,FromOrtho,pom,3)
        do i=1,NDimI(KPhase)
          call MultM(quRefBlock(1,i,0),TrPom,QuIn(1,i),1,3,3)
        enddo
        call DRGenPointGroup(CrSystemNameP(1),PGSel,QuIn,ToOrtho,
     1                       FromOrtho,QuIrr,QuRac,ich)
        NSymmN(KPhase)=NSymm(KPhase)
        do i=1,NSymmN(KPhase)
          call SetRealArrayTo(s6(1,i,1,KPhase),NDim(KPhase),0.)
        enddo
        if(ich.ne.0) then
          call FeChybne(-1.,YBottomMessage,'point group "'//
     1                  PGSel(:idel(PGSel))//
     2                  '" could not be interpreted',' ',SeriousError)
          go to 1500
        endif
      else
        call GetPointGroupFromSpaceGroup(PGAve(KRefBlock),i,j)
        call CopyMat(TrMP,TrPom6I,NDim(KPhase))
        call MatInv(TrPom6I,TrPom6,pom,NDim(KPhase))
      endif
      do i=1,NSymmN(KPhase)
        call multm(TrPom6,rm6(1,i,1,KPhase),MatPom,
     1             NDim(KPhase),NDim(KPhase),NDim(KPhase))
        call multm(MatPom,TrPom6I,rm6(1,i,1,KPhase),
     1             NDim(KPhase),NDim(KPhase),NDim(KPhase))
      enddo
      if(Klic.eq.0) then
        CovTest=0
        i=AveFromXShape
      else
        Change=CutMaxAveOld.ne.CutMaxAve(KRefBlock).or.
     1         CutMinAveOld.ne.CutMinAve(KRefBlock).or.
     2         FLimCullAveOld.ne.FLimCullAve(KRefBlock).or.
     3         NRefMaxAveOld.ne.NRefMaxAve(KRefBlock).or.
     4         RunScGrOld.ne.RunScGr(KRefBlock).or.
     5         SpHEvenOld.ne.SpHEven(KRefBlock).or.
     6         SpHOddOld.ne.SpHOdd(KRefBlock).or.
     7         .not.EqIgCase(PGAve(KRefBlock),PGAveOld).or.
     8         CorrAbsNew.ne.CorrAbs(KRefBlock).or.
     9         ScFrMethodOld.ne.ScFrMethod(KRefBlock)
        i=AveForSemiEmpir
      endif
      if(CovTest.eq.1.or.RunOpt) then
        UseExpCorr(KRefBlock)=0
        call DRSetCell(0)
        if(Klic.eq.1) then
          FLimCull(KDatBlock)=FLimCullAve(KDatBlock)
        else
          FLimCull(KDatBlock)=FLimCullXShape
        endif
        SigIMethod(KDatBlock)=max(SigIMethod(KDatBlock),1)
        call DRAverage(i,1,ich)
        if(ich.lt.0.or.CovTest.eq.1) go to 1500
        if(RunOpt.and.Klic.eq.1) then
          call DRAbsSemiEmpirSet(ich)
          if(RunOpt) then
            if(ich.eq.0) then
              call FeQuestLblChange(nLblRFac,
     1                              TextInfo(1)(:idel(TextInfo(1)))//
     2                              ', '//TextInfo(2))
              if(.not.Change.and.
     1           (SEType.eq.SEScales.or.SEType.eq.SEAll)) then
                Change=.not.EqRV(ScFrameOld,ScFrame(1,KRefBlock),
     1                           RunScNOld,.0001)
              endif
              if(.not.Change.and.
     1           (SEType.eq.SESpHarm.or.SEType.eq.SEAll)) then
                Change=.not.EqRV(AlmOld,SpHAlm(1,KRefBlock),SpHNOld,
     1                           .0001)
              endif
            else
              call FeQuestLblChange(nLblRFac,' ')
            endif
            go to 1500
          endif
        else if(Klic.eq.0) then
          write(Veta,'(''Number of unique reflections:'',i7,
     1                 ''   Total number of reflections:'',i7)')
     2      NUniXShape,NAllXShape
            call FeQuestLblChange(nLblXShape,Veta)
          go to 1500
        endif
      endif
9000  if(ich.gt.0) then
        go to 9900
      else if(ich.lt.0.or.CovTest.eq.1.or.RunOpt) then
        go to 1500
      else
        call FeQuestRemove(id)
        if(Klic.eq.1) then
          if(.not.Change.and.
     1       (SEType.eq.SEScales.or.SEType.eq.SEAll)) then
             Change=.not.EqRV(ScFrameOld,ScFrame(1,KRefBlock),
     1                        RunScNOld,.0001)
          endif
          if(.not.Change.and.
     1       (SEType.eq.SESpHarm.or.SEType.eq.SEAll)) then
            Change=.not.EqRV(AlmOld,SpHAlm(1,KRefBlock),SpHNOld,.0001)
          endif
        endif
        go to 9999
      endif
9900  ich=1
9999  WizardMode=WizardModeOld
      if(allocated(ScFrameOld)) deallocate(ScFrameOld)
      if(ExistM50) then
        FormulaSave=Formula(KPhase)
        NUnitsSave=NUnits(KPhase)
        call iom50(0,0,fln(:ifln)//'.m50')
        Formula(KPhase)=FormulaSave
        NUnits(KPhase)=NUnitsSave
      endif
      return
      end
