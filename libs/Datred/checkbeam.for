      subroutine CheckBeam(ad,ac,A,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer ad,ac,aov,A(4)
      i=A(1)-A(2)+OnePi(1)
      if(iabs(i).gt.OnePi(1)) i=i-isign(2*OnePi(1),i)
      call KOArea(ad,aov,A(3),i)
      if(abs(ad).gt.2) then
        ich=4
      else
        i=A(1)+OnePi(1)
        if(iabs(i).gt.OnePi(1)) i=i-isign(2*OnePi(1),i)
        call KOArea(ac,aov,A(3),i)
        if(iabs(ac).gt.2) ich=17
      endif
      return
      end
