      subroutine DRBasicMonstrum
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3),ip(3)
      character*80 t80,FileInMat
      character*2  nty
      logical FileReadIn,ExistFile,EqIgCase,CrwLogicQuest,
     1        FeYesNoHeader
      equivalence (xp,ip)
      NDim95(KRefBlock)=3
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      nstd=0
      call SetIntArrayTo(ihs,60,0)
      KappaGeom=.false.
      WorkString=SourceFileRefBlock(KRefBlock)
      ii=index(WorkString,'.')-1
      if(ii.le.0) ii=idel(WorkString)
      if(DifCode(KRefBlock).eq.IdD9ILL)
     1  RadiationRefBlock(KRefBlock)=NeutronRadiation
      if(DifCode(KRefBlock).eq.IdD9ILL.and.
     1   .not.ExistFile(WorkString(:ii)//'.mat')) then
        FileInMat=WorkString(:ii)//'.ubm'
        if(ExistFile(FileInMat)) then
          call CheckEOLOnFile(FileInMat,2)
          call OpenFile(70,FileInMat,'formatted','unknown')
          if(ErrFlag.ne.0) go to 1070
1000      read(70,FormA80,end=1015) t80
          if(t80.eq.' ') go to 1000
          call Mala(t80)
          i=index(t80,'wavelength')
          if(i.le.0) then
            i=index(t80,'lambda')
            if(i.le.0) go to 1000
          endif
          k=0
          do j=1,3
            call kus(t80,k,Cislo)
          enddo
          call posun(Cislo,1)
          read(Cislo,'(f15.0)') LamAveRefBlock(KRefBlock)
1015      rewind 70
1020      read(70,FormA80,end=1070) t80
          call Mala(t80)
          i=index(t80,'final orientation [ub] matrix')
          if(i.le.0) go to 1020
          do i=1,3
1030        read(70,FormA80,end=1070) t80
            if(t80.eq.' ') go to 1030
            k=0
            call StToReal(t80,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 1070
            do j=1,3
              ub(i,j,KRefBlock)=xp(j)
            enddo
          enddo
          call CloseIfOpened(70)
          FileReadIn=.true.
          go to 1300
        endif
1070    call CloseIfOpened(70)
      endif
      FileInMat=WorkString(:ii)//'.mat'
      if(ExistFile(FileInMat)) then
        FileReadIn=.true.
        call CheckEOLOnFile(FileInMat,2)
        if(ErrFlag.ne.0) go to 1200
        call OpenFile(70,FileInMat,'formatted','old')
        if(ErrFlag.ne.0) go to 1200
        do i=1,3
          read(70,FormA80,end=1200) t80
          k=0
          call StToReal(t80,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 1200
          do j=1,3
            ub(i,j,KRefBlock)=xp(j)
          enddo
        enddo
        read(70,FormA80,end=1200) t80
        k=0
        call StToReal(t80,k,LamAveRefBlock(KRefBlock),1,.false.,ich)
        if(ich.ne.0) go to 1200
        call StToInt(t80,k,ip,1,.false.,ich)
        if(ich.eq.0) then
          KappaGeom=ip(1).ne.0
        else
          go to 1140
        endif
        call StToInt(t80,k,ip,1,.false.,ich)
        if(ich.eq.0)    iskipHasy=ip(1)
1140    read(70,FormA80,err=1300,end=1300) t80
        call mala(t80)
        if(EqIgCase(t80,'end')) go to 1300
        if(nstd.ge.10) go to 1300
        nstd=nstd+1
        k=0
        call StToInt(t80,k,ihs(1,nstd),3,.false.,ich)
        if(ich.ne.0) go to 1200
        go to 1140
      endif
1200  FileReadIn=.false.
      call SetRealArrayTo(ub,9,0.)
      LamAveRefBlock(KRefBlock)=1.
      iskipHasy=0
      KappaGeom=.false.
1300  call CloseIfOpened(70)
      WizardMode=.false.
      id=NextQuestId()
      xqd=450.
      il=10
      t80='Define basic data for the data collection'
      call FeQuestCreate(id,-1.,-1.,xqd,il,t80,0,LightGray,0,0)
      il=1
      tpom=5.
      t80='Orientation matrix:'
      call FeQuestLblMake(id,tpom,il,t80,'L','N')
      tpom=tpom+FeTxLength(t80)+10.
      do i=1,3
        write(Cislo,'(i1,a2,'' row'')') i,nty(i)
        if(i.eq.1) then
          xpom=tpom+FeTxLengthUnder(Cislo)+10.
          dpom=xqd-xpom-5.
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Cislo,'L',dpom,EdwYd,0)
        do j=1,3
          xp(j)=ub(i,j,KRefBlock)
        enddo
        if(i.eq.1) then
          t80(:20)=' '
          nEdwOrMatFirst=EdwLastMade
        endif
        call FeQuestRealAEdwOpen(EdwLastMade,xp,3,.not.FileReadIn,
     1                           .false.)
        il=il+1
      enddo
      t80='%Wave length:'
      dpom=80.
      tpom=xpom-FeTxLengthUnder(t80)-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwWaveLength=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,LamAveRefBlock(KRefBlock),
     1                        .not.FileReadIn,.false.)
      if(DifCode(KRefBlock).eq.IdHasyLabF1) then
        il=il+1
        t80='%Kappa geometry used'
        xpom=tpom+FeTxLengthUnder(t80)+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,0,0)
        nCrwKappa=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KappaGeom)
        tpom=xpom+CrwXd+15.
      else
        nCrwKappa=0
        tpom=5.
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      t80='%Number of standard reflections'
      xpom=tpom+FeTxLengthUnder(t80)+10.
      dpom=40.
      call FeQuestEudMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,1)
      nEdwNstd=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,nstd,.not.FileReadIn)
      call FeQuestEudOpen(EdwLastMade,0,10,1,0.,0.,0.)
      tpom=5.
      t80='#11'
      xpom=tpom+FeTxLength(t80)+3.
      spom=xqd/3.
      dpom=spom-xpom-5.
      do i=1,10
        if(mod(i,3).eq.1) il=il+1
        write(t80,'(''#'',i2)') i
        call Zhusti(t80)
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwStd=EdwLastMade
        if(mod(i,3).eq.0) then
          tpom=tpom-2.*spom
          xpom=xpom-2.*spom
        else
          tpom=tpom+spom
          xpom=xpom+spom
        endif
        call FeQuestStringEdwOpen(EdwLastMade,' ')
      enddo
1450  nEdw=nEdwStd
      do i=1,10
        if(i.le.nstd) then
          k=0
          do j=1,3
            k=k+iabs(ihs(j,i))
          enddo
          call FeQuestIntAEdwOpen(nEdw,ihs(1,i),3,k.eq.0)
        else
          call FeQuestEdwClose(nEdw)
        endif
        nEdw=nEdw+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNstd) then
        call FeQuestIntFromEdw(nEdwNstd,nstd)
        go to 1450
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwOrMatFirst
        do i=1,3
          call FeQuestRealAFromEdw(nEdw,xp)
          do j=1,3
            ub(i,j,KRefBlock)=xp(j)
          enddo
          nEdw=nEdw+1
        enddo
        NInfo=1
        call DRCellFromUB
        call FeQuestRealFromEdw(nEdwWaveLength,
     1                          LamAveRefBlock(KRefBlock))
        if(nCrwKappa.gt.0) KappaGeom=CrwLogicQuest(nCrwKappa)
        call FeQuestIntFromEdw(nEdwNstd,nstd)
        nEdw=nEdwStd
        do i=1,nstd
          nhs(i)=0
          call FeQuestIntAFromEdw(nEdw,ihs(1,i))
          nEdw=nEdw+1
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9900
      WizardMode=.true.
      FileInMat=WorkString(:ii)//'.mat'
      call OpenFile(70,FileInMat,'formatted','unknown')
      write(70,'(3f10.6)')((ub(i,j,KRefBlock),j=1,3),i=1,3)
      if(KappaGeom) then
        i=1
      else
        i=0
      endif
      write(70,'(f9.5,2i2)') LamAveRefBlock(KRefBlock),i,iskipHasy
      do i=1,nstd
        write(70,'(6i4)')(ihs(j,i),j=1,3)
      enddo
      call CloseIfOpened(70)
      PocitatDirCos=.true.
      DirCosFromPsi=.false.
      call SetRealArrayTo(SenseOfAngle,4,-1.)
      SenseOfAngle(2)=1.
      if(DifCode(KRefBlock).eq.IdD9ILL) then
        zn=-1.
      else
        zn= 1.
      endif
      do j=1,3
        pom=ub(1,j,KRefBlock)
        ub(1,j,KRefBlock)=zn*ub(2,j,KRefBlock)
        ub(2,j,KRefBlock)=pom
      enddo
      do j=1,3
        ub(3,j,KRefBlock)=-ub(3,j,KRefBlock)
      enddo
      if(KappaGeom) then
        csalfa =cos(60.*ToRad)
        csalfaq=csalfa**2
        snalfa=sin(60.*ToRad)
        snalfaq=snalfa**2
      endif
      go to 9999
9000  call FeChybne(-1.,-1.,'during reading of the file '//fln(:ifln)//
     1              '.mat',' ',SeriousError)
9900  ErrFlag=1
      go to 9999
9999  return
      end
