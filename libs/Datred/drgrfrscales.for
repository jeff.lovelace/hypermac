      subroutine DRGrFrScales
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real xpp(3),xo(3),xp(2),yp(2),xsel(5),ysel(5)
      character*256 Veta
      character*17 :: Labels(3) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            '/)
      n2=0
      do i=1,NRuns(NRefBlock)
        n1=n2+1
        n2=n2+RunNFrames(i,KRefBlock)
      enddo
      Tiskne=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(80.,40.,30.,40.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          wpom=wpom/2.-5.
          xsave=xpom
          ysave=ypom
        endif
        if(i.le.1) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      XMin=1.
      XMax=RunCCDMax(KRefBlock)
      YMin=0.
      YMax=2.
      do i=1,RunScN(KRefBlock)
        YMin=min(YMin,ScFrame(i,KRefBlock))
        YMax=max(YMax,ScFrame(i,KRefBlock))
      enddo
      call FeQuestButtonOff(nButtPrint)
      call FeQuestButtonOff(nButtSave)
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      call UnitMat(F2O,3)
      call UnitMat(O2F,3)
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        xomn=XMin
        xomx=XMax
        yomn=YMin
        yomx=YMax
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        Veta='Scale/Frame group'
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1                 (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2                 pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3                 (XCornAcWin(1,2)-XCornAcWin(1,3)),Veta,'C',White)
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Frame group')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Scale')
        xpp(3)=0.
        n=1
        m=1
        do i=1,NRuns(NRefBlock)
          do j=1,RunNFrames(i,KRefBlock)
            n=n+1
            if(RunScGrN(n,KRefBlock).gt.m) then
              xpp(1)=n-1
              xpp(2)=ScFrame(m,KRefBlock)
              m=m+1
            else
              cycle
            endif
            call FeXf2X(xpp,xo)
            call FeCircleOpen(xo(1),xo(2),3.,White)
          enddo
        enddo
      endif
      j=1
      do i=1,NRuns(NRefBlock)-1
        j=j+RunNFrames(i,KRefBlock)
        xpp(1)=j
        xpp(2)=YMin
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(2)=YMax
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      enddo
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
!      AllowChangeMouse=.false.
      CheckMouse=.true.
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        endif
      else if(CheckType.eq.EventMouse) then
        if(CheckNumber.eq.JeLeftDown) then
          xsel(1)=XPos
          ysel(1)=YPos
          do i=1,5
            xsel(i)=XPos
            ysel(i)=YPos
          enddo
          xo(1)=FeX2Xo(XPos)
          xo(2)=FeY2Yo(YPOs)
          xo(3)=0.
          call multm(O2F,xo,xp,3,3,1)
          XMinP=xp(1)
          YMinP=xp(2)
          call FePlotMode('E')
          ip=2
          call FePolyLine(2,xsel,ysel,Green)
          TakeMouseMove=.true.
          if(DeferredOutput) call FeReleaseOutput
2600      call FeEvent(1)
          if(EventType.eq.0) go to 2600
2650      if(EventType.eq.EventMouse) then
            if(EventNumber.eq.JeMove) then
              call FePolyLine(ip,xsel,ysel,Green)
              ysel(2)=YPos
              xsel(3)=XPos
              ysel(3)=YPos
              xsel(4)=XPos
              if(xsel(2).ne.xsel(3)) then
                ip=5
              else
                ip=2
              endif
              call FePolyLine(ip,xsel,ysel,Green)
              go to 2600
            else if(EventNumber.eq.JeLeftUp) then
              TakeMouseMove=.false.
              xo(1)=FeX2Xo(xsel(3))
              xo(2)=FeY2Yo(ysel(3))
              xo(3)=0.
              call multm(O2F,xo,xp,3,3,1)
              XMin=min(XMinP,xp(1))
              YMin=min(YMinP,xp(2))
              XMax=max(XMinP,xp(1))
              YMax=max(YMinP,xp(2))
              call FePolyLine(ip,xsel,ysel,Green)
              call FePlotMode('N')
              DMin=999999.
              do mm=1,1
                if(mm.eq.1) then
                  Kolik=0
                else
                  Mame=0
                endif
                n=1
                m=1
                do i=1,NRuns(NRefBlock)
                  do j=1,RunNFrames(i,KRefBlock)
                    n=n+1
                    if(RunScGrN(n,KRefBlock).gt.m) then
                      xpp(1)=n-1
                      xpp(2)=ScFrame(m,KRefBlock)
                      m=m+1
                    else
                      cycle
                    endif
                    if(xpp(1).ge.XMin.and.xpp(1).le.XMax.and.
     1                 xpp(2).ge.YMin.and.xpp(2).le.YMax) then
                      write(6,'('' Run/Frame:'',3i7,3f10.1,3f10.4)')
     1                  i,j,m,XMin,xpp(1),XMax,YMin,xpp(2),YMax
                    endif
                  enddo
                enddo
              enddo
            endif
          endif
        endif
        go to 2500
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
9999  return
100   format(3f14.6)
      end
