      subroutine KUMA
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension h(3),x(3),hh(3),Rot(3,3),Rko(3,3),ccc(3),DirNozzle(3),
     1          c(3),cc(3),uk(4),iuk(4),DirFi(3),
     2          hA (:,:),PsiA (:),IUkA (:,:),DiffThA (:),DiffAngA (:),
     3          hAp(:,:),PsiAp(:),IUkAp(:,:),DiffThAp(:),DiffAngAp(:),
     4          ipor(:)
      character*80 FileName,Veta
      character*30 Men(6)
      integer FeMenu
      logical ExistFile,Append
      allocatable ipor,hA,PsiA,IUkA,DiffThA,DiffAngA,hAp,PsiAp,IUkAp,
     1            DiffThAp,DiffAngAp
      data men/'Generuj-psi=0','Generuj-psi optimal','Generuj-pro N2',
     1         'Indexation procedure','Cell refinement',
     2         'Find twin matrix'/
      data DirNozzle/.5,.0,.8660254/,DirFi/0.,0.,1./
      call KUMAInit
      ln=0
      lno=0
200   klic=FeMenu(-1.,-1.,men,1,6,1,0)
      if(Klic.le.0) go to 9999
      if(Klic.lt.4) then
        FileName='km4.hkl'
300     call FeReadFileName(FileName,'Define output hkl file','*.hkl',
     1                      ich)
        if(ich.ne.0) go to 9999
        if(ExistFile(FileName)) then
          id=NextQuestId()
          xqd=300.
          il=1
          call FeQuestCreate(id,-1.,-1.,xqd,il,'The file "'//
     1                       FileName(:idel(FileName))//
     2                       '" already exists.',0,LightGray,-1,-1)

          Veta='%Append'
          dpom=FeTxLength(Veta)+20.
          xpom=xqd*.5-dpom*1.5-10.
          do i=1,3
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
            if(i.eq.1) then
              Veta='%Rewrite'
              nButtAppend=ButtonLastMade
            else if(i.eq.2) then
              nButtRewrite=ButtonLastMade
              Veta='%Cancel'
            endif
            xpom=xpom+dpom+10.
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          enddo
500       call FeQuestEvent(id,ich)
          if(CheckType.eq.EventButton) then
            if(CheckNumber.eq.nButtAppend) then
              Append=.true.
            else if(CheckNumber.eq.nButtRewrite) then
              Append=.false.
            else
              call FeQuestRemove(id)
              go to 300
            endif
            call FeQuestRemove(id)
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 500
          endif
        else
          Append=.false.
        endif
        lno=NextLogicNumber()
        call OpenFile(lno,FileName,'formatted','unknown')
        if(Append) then
          Veta=fln(:ifln)//'.l99'
          call CopyFile(FileName,Veta)
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','unknown')
600       read(ln,FormA80,end=610) Veta
          write(lno,FormA1)(Veta(i:i),i=1,idel(Veta))
          go to 600
610       close(ln,status='delete')
        endif
        ln=0
        call SetIntArrayTo(ih,6,0)
      else
        ln=0
        lno=0
      endif
      if(klic.eq.1) then
        if(ExistM50) then
          call iom50(0,0,fln(:ifln)//'.m50')
          call ComSym(0,0,ich)
        else
          call FeChybne(-1.,-1.,'m50 not present.',' ',SeriousError)
          go to 9999
        endif
        UseEFormat91=.false.
        if(UseEFormat91) then
          Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
        else
          Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
        endif
        write(Format91(2:2),'(i1)') NDim(KPhase)
        call GenerQuest(3,lno)
        if(ErrFlag.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.gen','formatted','old')
        if(ErrFlag.ne.0) go to 9999
        nn=0
1000    read(ln,Format91,err=2000,end=2000)(ih(i),i=1,NDim(KPhase))
        if(ih(1).gt.900) go to 2000
        nn=nn+1
c        do i=1,3
c          h(i)=ih(i)
c          do j=1,NDimI(KPhase)
c            h(i)=h(i)+qu(i,j,1,KPhase)*float(ih(j+3))
c          enddo
c        enddo
        write(lno,101)(ih(i),i=1,NDim(KPhase))
c        write(lno,100) h
        go to 1000
2000    write(TextInfo(1),'(i5,'' reflections were generated'')') nn
        Ninfo=1
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
      else if(klic.eq.2.or.klic.eq.3) then
        if(ExistM95) then
          call iom95(0,fln(:ifln)//'.m95')
          call iom50(0,0,fln(:ifln)//'.m50')
        else
          call FeChybne(-1.,-1.,'m94 not present.',' ',SeriousError)
          go to 9999
        endif
        call GenerQuest(3,lno)
        if(ErrFlag.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.gen','formatted','old')
        if(ErrFlag.ne.0) go to 9999
        nn=0
        NAlloc=10000
        allocate(hA(3,NAlloc),PsiA(NAlloc),IUkA(NAlloc,4),
     1           DiffThA(NAlloc),DiffAngA(NAlloc))
5000    read(ln,Format91,err=5600,end=5600)(ih(i),i=1,NDim(KPhase))
        if(ih(1).gt.900) go to 5600
        do i=1,3
          h(i)=ih(i)
          do j=1,NDimI(KPhase)
            h(i)=h(i)+qu(i,j,1,KPhase)*float(ih(j+3))
          enddo
        enddo
        call Multm(ub,h ,c ,3,3,1)
        call AnglesFromHKL(h,0.,uk,Rko,ich)
        nmin=9999
        if(Klic.eq.2) then
          if(ih(4).ne.0.or.ih(5).ne.0) then
            fm=0.
          else
            fm=1.
          endif
          pom=0.
          do i=1,3
            hh(i)=float(ih(i))+qu(i,1,1,KPhase)*fm
            pom=pom+abs(hh(i))
          enddo
          if(pom.le..001) hh(1)=1.
          call Multm(ub,hh,cc,3,3,1)
          do i=1,3
            ccc(i)=cc(i)-c(i)
          enddo
          DiffAng=VecOrtScal(c,ccc,3)/(VecOrtLen(c,3)*VecOrtLen(ccc,3))
          if(abs(DiffAng).lt.1.) then
            DiffAng=acos(DiffAng)/ToRad
          else if(DiffAng.gt.0.) then
            DiffAng=0.
          else
            DiffAng=180.
          endif
          call FromIndSinthl(ihp,h ,sinthl1,pom,1,1)
          call FromIndSinthl(ihp,hh,sinthl2,pom,1,1)
          DiffTh=(asin(sinthl2*LamAveRefBlock(KRefBlock))-
     1            asin(sinthl1*LamAveRefBlock(KRefBlock)))/ToRad
          call SetRotKuma(uk(1)-uk(2),uk(3),uk(4),Rot)
          call MultM(Rot,cc,x,3,3,1)
          Psi0=-AngKuma(x(3),x(1))
          nkrat=9
        else
          Psi0=0.
          nkrat=36
          AngleWithNozzleMax =-99999.
          AngleWithNozzleMaxA=-99999.
        endif
        do 5350i=1,4
          if(i.eq.1.or.i.eq.3) then
            PsiD= 5.*ToRad
          else
            PsiD=-5.*ToRad
          endif
          if(i.le.2) then
            Psi=Psi0
          else
            Psi=Psi0+Pi
            if(Psi.gt.pi) Psi=Psi-Pi2
          endif
          n=0
5300      call AnglesFromRotation(uk,Rko,Psi,ich)
          if(ich.eq.0) call CheckProblems(uk,iuk,ich)
          if(Klic.eq.3.and.ich.eq.0) then
            call SetRotKuma(uk(1)-uk(2),uk(3),uk(4),Rot)
            call MultM(Rot,DirFi,x,3,3,1)
            pom=VecOrtScal(x,DirNozzle,3)
            pom=min(pom, .999999)
            pom=max(pom,-.999999)
            AngleWithNozzle=acos(pom)/ToRad
            AngleWithNozzleA=abs(AngleWithNozzle)
            if(AngleWithNozzleA.gt.70.) then
              AngleWithNozzleMaxA=AngleWithNozzleA
              AngleWithNozzleMax=AngleWithNozzle
              PsiOpt=Psi
              go to 5360
            else
              if(AngleWithNozzleA.gt.AngleWithNozzleMaxA) then
                AngleWithNozzleMaxA=AngleWithNozzleA
                AngleWithNozzleMax=AngleWithNozzle
                PsiOpt=Psi
              endif
              ich=1
            endif
          endif
          if(ich.ne.0) then
            if(n.ge.nkrat) go to 5350
            n=n+1
            Psi=Psi+PsiD
            go to 5300
          endif
          if(n.lt.nmin) then
            nmin=n
            PsiOpt=Psi
          endif
          if(nmin.eq.0) go to 5360
5350    continue
        if(Klic.eq.2) then
         if(nmin.ge.nkrat) then
            write(TextInfo(1),'(6i4)')(ih(i),i=1,NDim(KPhase))
            call ZdrcniCisla(TextInfo(1),NDim(KPhase))
            TextInfo(1)='Reflection : '//TextInfo(1)(:idel(TextInfo(1)))
     1                //' cannot be optimized'
            Ninfo=1
            call FeInfoOut(-1.,-1.,'WARNING','L')
            nn=nn-1
            go to 5000
          endif
        endif
5360    nn=nn+1
        if(nn.gt.NAlloc) then
          allocate(hAp(3,NAlloc),PsiAp(NAlloc),IUkAp(NAlloc,4),
     1             DiffThAp(NAlloc),DiffAngAp(NAlloc))
          call CopyVek (hA,hAp,3*NAlloc)
          call CopyVekI(IUkA,IUkAp,4*NAlloc)
          call CopyVek (PsiA,PsiAp,NAlloc)
          call CopyVek (DiffThA,DiffThAp,NAlloc)
          call CopyVek (DiffAngA,DiffAngAp,NAlloc)
          NAllocp=NAlloc
          NAlloc=NAlloc+10000
          deallocate(hA,PsiA,IUkA,DiffThA,DiffAngA)
          allocate(hA(3,NAlloc),PsiA(NAlloc),IUkA(NAlloc,4),
     1             DiffThA(NAlloc),DiffAngA(NAlloc))
          call CopyVek (hAp,hA,3*NAllocp)
          call CopyVekI(IUkAp,IUkA,4*NAllocp)
          call CopyVek (PsiAp,PsiA,NAllocp)
          call CopyVek (DiffThAp,DiffThA,NAllocp)
          call CopyVek (DiffAngAp,DiffAngA,NAllocp)
          deallocate(hAp,PsiAp,IUkAp,DiffThAp,DiffAngAp)
        endif
        PsiA(nn)=PsiOpt
        DiffThA(nn)=DiffTh
        DiffAngA(nn)=DiffAng
        call CopyVek(h,hA(1,nn),3)
        if(Klic.eq.2) then
          do i=1,4
            IUkA(nn,i)=IUk(i)
          enddo
        else
          IUkA(nn,2)=nint(AngleWithNozzleMax*10000.)
        endif
        go to 5000
5600    allocate(ipor(nn))
        call indexx(nn,IUkA(1,2),ipor)
        if(Klic.eq.2) then
          j=ipor(1)
          ipr=1
5700      do i=1,4
            IUk(i)=IUkA(j,i)
          enddo
          write(lno,100)(hA(k,j),k=1,3),PsiA(j)/ToRad,DiffThA(j),
     1                   DiffAngA(j)
          mind=99999999
          j=0
          ipor(ipr)=0
          do 5800i=1,nn
            k=ipor(i)
            if(k.le.0) go to 5800
            id=0
            do m=1,4
              id=max(iabs(IUkA(k,m)-IUk(m)),id)
              if(id.gt.mind) go to 5800
            enddo
            mind=id
            j=k
            ipr=i
5800      continue
          if(j.gt.0) go to 5700
        else
          do i=1,nn
            if(Klic.eq.3) then
              j=i
            else
              j=ipor(i)
            endif
            if(iabs(IUkA(j,2)).ge.700000)
     1        write(lno,100)(hA(k,j),k=1,3),PsiA(j)/ToRad,
     2                       float(IUkA(j,2))/10000.
          enddo
          do i=1,nn
            if(Klic.eq.3) then
              j=i
            else
              j=ipor(i)
            endif
            if(iabs(IUkA(j,2)).lt.700000)
     1        write(lno,100)(hA(k,j),k=1,3),PsiA(j)/ToRad,
     2                       float(IUkA(j,2))/10000.
          enddo
        endif
        if(allocated(ipor)) deallocate(ipor)
        write(TextInfo(1),'(i5,'' reflections were generated'')') nn
        Ninfo=1
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        if(Allocated(hA)) deallocate(hA,PsiA,IUkA,DiffThA,DiffAngA)
      else if(Klic.eq.4) then
        call DRKUMAIndex(1)
        go to 200
      else if(Klic.eq.5) then
        call DRKUMARefine(1)
        go to 200
      else if(Klic.eq.6) then
        call DRCompareTwoUB
        go to 200
      endif
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      return
100   format(6f10.4,4i10)
101   format(6i4)
      end
