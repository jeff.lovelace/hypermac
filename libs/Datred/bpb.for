      subroutine BPB(ri,rs,k,nb)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'profil.cmn'
      B=0.
      P=0.
      do i=1,nprof
        if(i.le.nb.or.i.ge.nprof-nb+1) then
          B=B+py(i,k)
        else
          P=P+py(i,k)
        endif
      enddo
      r=float((nprof-2*nb))/float((2*nb))
      ri=(P-B*r)*rych
      rs=sqrt(P+B*r**2)*rych
      return
      end
