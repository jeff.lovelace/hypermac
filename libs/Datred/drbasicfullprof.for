      subroutine DRBasicFullProf
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3),ip(3)
      character*256 Radka
      logical EqRV,EqRVM
      equivalence (xp,ip)
      QuMagUsed=.false.
      read(71,FormA,err=9000,end=9000) Radka
      read(71,FormA,err=9000,end=9000) FormatRefBlock(KRefBlock)
      read(71,FormA,err=9000,end=9000) Radka
      k=0
      call SetRealArrayTo(xp,3,0.)
      call StToReal(Radka,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 9999
      LamA1RefBlock(KRefBlock)=xp(1)
      LamA2RefBlock(KRefBlock)=xp(1)
      LamAveRefBlock(KRefBlock)=xp(1)
      if(ich.ne.0) go to 9000
      FSqFullProf=1-nint(xp(2))
      TwinFullProf=nint(xp(3))
      read(71,FormA,err=9000,end=9000) Radka
      i=LocateSubstring(Radka,'!',.false.,.true.)
      Radka(i:)=' '
      if(idel(Radka).gt.10) then
        backspace 71
        NDim95(KRefBlock)=3
        NDimSXD=3
        go to 2000
      endif
      QuMagUsed=.true.
      k=0
      call StToInt(Radka,k,ip,1,.false.,ich)
      if(ich.ne.0) go to 9000
      NQuSXD=ip(1)
      if(NQuSXD.gt.3) go to 9100
      do i=1,NQuSXD
        read(71,FormA80,err=9000,end=9000) Radka
        k=0
        call StToInt(Radka,k,ip,1,.false.,ich)
        j=ip(1)
        call StToReal(Radka,k,QuSXD(1,j),3,.false.,ich)
        if(ich.ne.0) go to 9000
      enddo
2000  RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      n=0
      do 3000i=1,NQuSXD
        do j=1,n
          do k=1,5
            do l=1,3
              xp(l)=QuRefBlock(l,j,KRefBlock)*float(k)
            enddo
            if(EqRV (xp,QuSXD(1,i),3,.0001).or.
     1         EqRVM(xp,QuSXD(1,i),3,.0001)) go to 3000
            do l=1,3
              xp(l)=QuRefBlock(l,j,KRefBlock)/float(k)
            enddo
            if(EqRV (xp,QuSXD(1,i),3,.0001).or.
     1         EqRVM(xp,QuSXD(1,i),3,.0001)) go to 3000
          enddo
        enddo
        n=n+1
        call CopyVek(QuSXD(1,i),QuRefBlock(1,j,KRefBlock),3)
3000  continue
      NDim95(KRefBlock)=n+3
      if(NDim95(KRefBlock).gt.3) then
        if(QuMagUsed) then
          NDimSXD=4
        else
          NDimSXD=NQuSXD+3
        endif
      else
        NDimSXD=3
      endif
      go to 9999
9000  call FeReadError(71)
      go to 9900
9100  Radka='the number of propagation vectors >3.'
      go to 9800
9200  Radka='the number of Q combinations >3.'
      go to 9800
9800  call FeChybne(-1.,-1.,Radka,' ',SeriousError)
9900  ErrFlag=1
9999  return
      end
