      subroutine SiemensUnCompress(String,k,ic)
      include 'fepc.cmn'
      character*(*) String
      character*42 low
      character*52 high
      data low/'0123456789`~!@#$%^&*()-_=+||[{]};:''",<.>/?'/
      data high/'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'/
      low(28:28)=char(92)
      ic=0
1000  i=index(high,String(k:k))-1
      if(i.ge.0) then
        ic=ic*52+i
        k=k+1
        go to 1000
      endif
      i=index(low,String(k:k))
      ic=ic*42+i-1
      k=k+1
      return
      end
