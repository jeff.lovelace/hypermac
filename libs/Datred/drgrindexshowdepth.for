      subroutine DRGrIndexShowDepth(XMinP,XMaxP,YMinP,YMaxP)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call FeFillRectangle(XMinP,XMaxP,YMinP,YMaxP,4,0,0,White)
      do i=1,ProjNX
        xu(1)=ProjX(i)/EnlargeFactor
        xu(2)=xu(1)
        yu(1)=YMinP
        yu(2)=YMinP+50.*float(ProjMX(i))/float(ProjNMaxX)
        call FePolyLine(2,xu,yu,Black)
      enddo
      return
      end
