      subroutine DRAbsSemiEmpirOptions(ICykl)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer EdwStateQuest
      logical CrwLogicQuest
      character*256 Veta
      id=NextQuestId()
      il=4
      xqd=250.
      call FeQuestCreate(id,-1.,230.,xqd,il,'Modify refinement '//
     1                   'options:',0,LightGray,0,0)
      il=1
      tpom=5.
      dpom=60.
      xpom=100.
      Veta='%Damping factor'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwTlum=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,TlumSemiEmpir,.false.,.false.)
      il=il+1
      xpomp=5.
      tpomp=xpomp+CrwXd+10.
      Veta='%Use Marquart method'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwMarq=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,LSMethodSemiEmpir.eq.2)
      il=il+1
      Veta='%Fudge factor'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFudge=EdwLastMade
      il=il+1
      Veta='%Number of cycles'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNCykl=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NCyklSemiEmpir,.false.)
      if(ICykl.ne.NCyklSemiEmpir) then
        ic=9999
      else
        ic=NCyklSemiEmpir
      endif
      call FeQuestEudOpen(EdwLastMade,min(ICykl+1,NCyklSemiEmpir),ic,1,
     1                    pom,pom,pom)
1400  if(CrwLogicQuest(nCrwMarq)) then
        call FeQuestRealEdwOpen(nEdwFudge,MarqLamSemiEmpir,.false.,
     1                          .false.)
      else
        if(EdwStateQuest(nEdwFudge).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwFudge,MarqLamSemiEmpir)
          call FeQuestEdwDisable(nEdwFudge)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMarq) then
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwTlum,TlumSemiEmpir)
        TlumSemiEmpir=max(TlumSemiEmpir,0.)
        pom=min(TlumSemiEmpir,1.)
        call FeQuestIntFromEdw(nEdwNCykl,NCyklSemiEmpir)
        if(CrwLogicQuest(nCrwMarq)) then
          LSMethodSemiEmpir=2
          call FeQuestRealFromEdw(nEdwFudge,MarqLamSemiEmpir)
        else
          LSMethodSemiEmpir=1
        endif
      endif
      call FeQuestRemove(id)
      return
      end
