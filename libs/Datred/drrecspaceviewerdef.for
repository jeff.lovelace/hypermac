      subroutine DRRecSpaceViewerDef(idal,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xx(3),xxg(3),yy(3),yyg(3),zz(3),zzg(3),r(3)
      character*80 Veta,t80
      integer EdwStateQuest,XDir(3),YDir(3),ZDir(3),zk,zl,z(3),zmez(3),
     1        SimDirSelOld,SimDirOld(3),SimTypeOld
      logical ExistFile,CrwLogicQuest,SimAveOld,SimCompleteOld,EqIV
      SimDirSelOld=SimDirSel
      call CopyVekI(SimDir,SimDirOld,3)
      SimTypeOld=SimType
      SimAveOld=SimAve
      SimCompleteOld=SimComplete
      idal=0
      ich=0
      il=9
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      xpom=5.
      il=1
      call FeQuestLblMake(id,xpom,il,'Viewing direction:','L','B')
      tpom=xpom+CrwXd+10.
      do i=1,4
        il=il+1
        if(i.le.3) then
          Veta='%'//SmbABC(i)
        else
          Veta='%General:'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.SimDirSel)
        if(i.eq.1) then
          nCrwDirFirst=CrwLastMade
        else if(i.eq.4) then
          xpome=tpom+FeTxLengthUnder(Veta)+5.
          dpom=50.
          call FeQuestEdwMake(id,tpom,il,xpome,il,' ','L',dpom,EdwYd,0)
          nEdwDir=EdwLastMade
          nCrwDirLast=CrwLastMade
        endif
      enddo
      il=1
      xpom=xpom+xqd*.5
      call FeQuestLblMake(id,xpom,il,'Value:','L','B')
      tpom=xpom+CrwXd+10.
      do i=1,4
        il=il+1
        if(i.eq.1) then
          Veta='%Iobs'
        else if(i.eq.2) then
          Veta='I%obs from refine'
          nCrwIobs=CrwLastMade
        else if(i.eq.3) then
          Veta='Ica%lc from refine'
          nCrwIobsRef=CrwLastMade
        else if(i.eq.4) then
          Veta='(Fcalc-Fobs)/%sig(Fobs)'
          nCrwIcalcRef=CrwLastMade
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.SimType)
        if(.not.ExistM95.and.i.eq.1) then
          call FeQuestCrwDisable(CrwLastMade)
          if(SimType.eq.1) SimType=2
        endif
        if(.not.ExistFile(fln(:ifln)//'.m83').and.i.ge.2)
     1    call FeQuestCrwDisable(CrwLastMade)
      enddo
      nCrwDiffFSig=CrwLastMade
      il=il+1
      if(nCrwIobs.eq.0) il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=5.
      tpom=xpom+CrwXd+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='apply symmetry averaging'
        else if(i.eq.2) then
          Veta='complete the diffraction pattern by the symmetry'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        if(i.eq.1) then
          call FeQuestCrwOpen(CrwLastMade,SimAve)
          nCrwAve=CrwLastMade
        else if(i.eq.2) then
          nCrwComplete=CrwLastMade
        endif
      enddo
1400  if(SimDirSel.eq.4) then
        if(EdwStateQuest(nEdwDir).ne.EdwOpened)
     1    call FeQuestIntAEdwOpen(nEdwDir,SimDir,3,.false.)
      else
        call FeQuestEdwClose(nEdwDir)
      endif
      if(nCrwAve.ne.0) then
        if(SimAve) then
          if(CrwState(nCrwComplete).ne.CrwOff.and.
     1       CrwState(nCrwComplete).ne.CrwOn)
     2       call FeQuestCrwOpen(nCrwComplete,.true.)
        else
          call FeQuestCrwClose(nCrwComplete)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.ge.nCrwDirFirst.and.CheckNumber.le.nCrwDirLast))
     2  then
        nCrw=nCrwDirFirst
        do i=1,4
          if(CrwLogicQuest(nCrw)) then
            SimDirSel=i
            go to 1400
          endif
          nCrw=nCrw+1
        enddo
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwAve) then
        SimAve=CrwLogicQuest(nCrwAve)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(SimAve) SimComplete=CrwLogicQuest(nCrwComplete)
        if(nCrwIobs.ne.0) then
          if(CrwLogicQuest(nCrwIobs)) then
            SimType=SimIobs
          endif
        endif
        if(CrwLogicQuest(nCrwIobsRef)) then
          SimType=SimIobsRef
        else if(CrwLogicQuest(nCrwIcalcRef)) then
          SimType=SimIcalcRef
        else if(CrwLogicQuest(nCrwDiffFSig)) then
          SimType=SimDiffFSig
        endif
        call SetIntArrayTo(ZDir,3,0)
        call SetIntArrayTo(XDir,3,0)
        call SetIntArrayTo(YDir,3,0)
        if(SimDirSel.le.3) then
          ZDir(SimDirSel)=1
        else
          call FeQuestIntAFromEdw(nEdwDir,SimDir)
          call CopyVekI(SimDir,ZDir,3)
        endif
        call MinMultMaxFract(ZDir,3,MinMult,MaxFract)
        NNul=0
        INul=0
        do i=1,3
          ZDir(i)=ZDir(i)/MaxFract
          if(ZDir(i).eq.0) then
            NNul=NNul+1
            INul=i
          else
            INeNul=i
          endif
        enddo
        if(NNul.eq.3) then
          Veta='view direction cannot be the null vector'
          t80='Try again'
          go to 9900
        else if(NNul.eq.2) then
          XDir(mod(INeNul,3)+1)=1.
          YDir(mod(INeNul+1,3)+1)=1.
        else if(NNul.eq.1) then
          XDir(INul)=1.
          i=mod(INul  ,3)+1
          j=mod(INul+1,3)+1
          YDir(i)= ZDir(j)
          YDir(j)=-ZDir(i)
          if(abs(YDir(i)).le.abs(YDir(j))) then
            k=i
            l=j
            zn=1.
          else
            k=i
            l=j
            zn=-1.
          endif
          r0=zn/float(YDir(k))
          r(1)=zn*float(YDir(l))/float(YDir(k))
          IMin=999999
          do i=-YDir(k),YDir(k)
            pom=r0+r(1)*float(i)
            ipom=nint(pom)
            if(abs(float(ipom)-pom).lt..001) then
              j=abs(ipom)+iabs(i)
              if(j.lt.IMin) then
                IMin=j
                zl=ipom
                zk=i
              endif
            endif
          enddo
          if(IMin.le.999999) then
            ZDir(k)=zk
            ZDir(l)=zl
          else
            go to 9000
          endif
        else
          im=2*max(iabs(ZDir(1)),iabs(ZDir(2)),iabs(ZDir(3)))
          XDirMin=9999.
          YDirMin=9999.
          call SetIntArrayTo(XDir,3,9999)
          call SetIntArrayTo(YDir,3,9999)
          do i=0,im
            ih(1)=i
            xx(1)=i
            if(i.eq.0) then
              jp=0
            else
              jp=-im
            endif
            do j=jp,im
              ih(2)=j
              xx(2)=j
              if(i.eq.0) then
                kp=0
              else
                kp=-im
              endif
              do k=kp,im
                ih(3)=k
                xx(3)=k
                ip=0
                do l=1,3
                  ip=ip+ZDir(l)*ih(l)
                enddo
                if(ip.ne.0) cycle
                call Multm(MetTensI(1,1,KPhase),xx,xxg,3,3,1)
                pom=sqrt(scalmul(xx,xxg))
                if(pom.lt..00001) cycle
                if(pom.lt.XDirMin) then
                  YDirMin=XDirMin
                  call CopyVekI(XDir,YDir,3)
                  XDirMin=pom
                  call CopyVekI(ih,XDir,3)
                else if(pom.lt.YDirMin) then
                  YDirMin=pom
                  call CopyVekI(ih,YDir,3)
                endif
              enddo
            enddo
          enddo
          if(XDirMin.gt.9000..or.YDirMin.gt.9000.) go to 9000
          z(1)= XDir(2)*YDir(3)-XDir(3)*YDir(2)
          z(2)=-XDir(1)*YDir(3)+XDir(3)*YDir(1)
          z(3)= XDir(1)*YDir(2)-XDir(2)*YDir(1)
          izmn=999999
          iz=0
          do i=1,3
            if(z(i).ne.0.and.iabs(z(i)).lt.iabs(izmn)) then
              iz=i
              izmn=z(i)
            endif
          enddo
          do i=1,3
            if(i.eq.iz.or.z(i).eq.0) then
              zmez(i)=0
            else
              zmez(i)=iabs(izmn)
            endif
            r(i)=-float(z(i))/float(izmn)
          enddo
          if(izmn.gt.90000) go to 9000
          r0=1./float(izmn)
          ZDirMin=9999.
          do i=-zmez(1),zmez(1)
            ih(1)=i
            xx(1)=i
            do j=-zmez(2),zmez(2)
              ih(2)=j
              xx(2)=j
              do k=-zmez(3),zmez(3)
                ih(3)=k
                xx(3)=k
                pom=r0+scalmul(xx,r)
                ipom=nint(pom)
                if(abs(float(ipom)-pom).lt..0001) then
                  xx(iz)=ipom
                  call Multm(MetTensI(1,1,KPhase),xx,xxg,3,3,1)
                  pom=sqrt(scalmul(xx,xxg))
                  xx(iz)=0.
                  if(pom.lt.ZDirMin) then
                    ZDirMin=pom
                    call CopyVekI(ih,ZDir,3)
                    ZDir(iz)=ipom
                  endif
                endif
              enddo
            enddo
          enddo
          if(ZDirMin.gt.9000.) go to 9000
        endif
        do i=1,3
          SimTr(1,i)=XDir(i)
          xx(i)=XDir(i)
          SimTr(2,i)=YDir(i)
          yy(i)=YDir(i)
          SimTr(3,i)=ZDir(i)
          zz(i)=ZDir(i)
        enddo
        call Multm(MetTensI(1,1,KPhase),xx,xxg,3,3,1)
        call Multm(MetTensI(1,1,KPhase),yy,yyg,3,3,1)
        call Multm(MetTensI(1,1,KPhase),zz,zzg,3,3,1)
        call MatInv(SimTr,SimTrI,pom,3)
        call SetRealArrayTo(F2O,9,0.)
        aa=sqrt(ScalMul(xx,xxg))
        bb=sqrt(ScalMul(yy,yyg))
        cc=sqrt(ScalMul(zz,zzg))
        csa=ScalMul(yy,zzg)/(bb*cc)
        csb=ScalMul(xx,zzg)/(aa*cc)
        csg=ScalMul(xx,yyg)/(aa*bb)
        snb=sqrt(1.-csb**2)
        sng=sqrt(1.-csg**2)
        pom=(csa-csb*csg)/sng
        F2O(1)=aa
        F2O(4)=bb*csg
        F2O(5)=bb*sng
        F2O(7)=cc*csb
        F2O(8)=cc*pom
        F2O(9)=cc*snb*sqrt(1.-(pom/snb)**2)
        call matinv(F2O,O2F,pom,3)
        if(SimTypeOld.ne.SimType.or.(SimAveOld.neqv.SimAve).or.
     1     (SimCompleteOld.neqv.SimComplete)) then
          idal=2
        else if(SimDirSelOld.ne.SimDirSel.or.
     1          .not.EqIV(SimDir,SimDirOld,3)) then
          idal=1
        endif
      endif
      go to 9999
9000  call MakeHKLString(SimDir,3,Veta)
      Veta='the selected direction '//Veta(:idel(Veta))//
     1     ' cannot properly handled'
      t80='This is a bug, please contact authors'
9900  call FeChybne(-1.,YBottomMessage,Veta,t80,SeriousError)
      call FeQuestButtonOff(ButtonOK-ButtonFr+1)
      go to 1500
9999  call FeQuestRemove(id)
      call FeMouseShape(0)
      SimDirSelOld=SimDirSel
      return
      end
