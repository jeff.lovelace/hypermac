      subroutine AnglesFromRotation(ANew,X,Psi,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension x(3,3),R(3,3),Re(3,3),ANew(4)
      ich=0
      eta=Psi+psim
      call CopyVek(Porig,ANew,4)
      call RotationalMatrix(X,ANew(3),ANew(1))
      if(eta.eq.0.) then
        ANew(1)=ANew(1)+ANew(2)
        go to 9999
      endif
      s=sin(eta)
      c=cos(eta)
      call StoreMatrix(Re,eta,4)
      call MultM(Re,X,R,3,3,3)
      c=(R(3,3)-ca*cab)/(sa*sab)
      if(Abs(c).gt.1.) then
        ich=2
      else
        s=sign(1.,ANew(3))
        s=s*Sqrt(1.-c*c)
        ANew(3)=AngKuma(s,c)
        a=(1.+ca*cab)*c+sa*sab
        b=(ca+cab)*s
        wy=a**2+b**2
        v1=R(1,1)+R(2,2)
        v2=R(1,2)-R(2,1)
        sum=AngKuma((a*v2-b*v1)/wy,(a*v1+b*v2)/wy)
        a=sa*cab*c-ca*sab
        b=sa*s
        wy=a**2+b**2
        v1=R(3,1)
        v2=R(3,2)
        dif=AngKuma((a*v2-b*v1)/wy,(a*v1+b*v2)/wy)
        ANew(1)=sum-dif
        ANew(4)=ANew(4)+dif
1000    if(ANew(4).gt.pi) then
          ANew(4)=ANew(4)-pi2
          go to 1000
        endif
1100    if(ANew(4).lt.-pi) then
          ANew(4)=ANew(4)+pi2
          go to 1100
        endif
        call RotationalMatrix(X,ANew(3),ANew(1))
        ANew(1)=ANew(1)+ANew(2)
      endif
9999  return
      end
