      function GetMaxd(facenr)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer facenr
      dmin=999999999.
      do i=1,PocetBodu
        dmin=min(dmin,abs(scalmul(krov(1,facenr),bod(1,i))+
     1           krov(4,facenr)))
      enddo
      getmaxd=krov(4,facenr)*0.996-dmin
      return
      end
