      subroutine DRSGTestCentr(ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'powder.cmn'
      dimension NCenObs(728),NCenAll(728),BasCenVec(6,728),SCenAll(728),
     1          SCenObs(728),h(6,20),hp(6),nx(6),ni(6),NSpec(8),
     2          VT6X(:,:),NSelX(:),QuPom(3,3),BasCenVecSpec(3,3,8),
     3          NBasCenVecSpec(8),NCenObsSpec(8),NCenAllSpec(8),
     4          SCenObsSpec(8),SCenAllSpec(8)
      character*256 Radka,FileName
      character*80  CenText(10),Veta
      character*1   t1
      integer SbwLnQuest,SbwItemSelQuest,UseTabsL,UseTabsI,UseTabsIn,
     1        CenAcceptable(9),CentrMultOld
      integer :: TakeSGTwinUsed=-1
      logical CrwLogicQuest,BratVT6X(:),EqRV,Observed,XCentr,UseOld,
     1        LeBailFirst
      data nlim/1000/
      data NBasCenVecSpec/0,1,1,1,1,3,2,2/
      data BasCenVecSpec/9*.0,
     2     .0,.5,.5,6*.0,
     3     .5,.0,.5,6*.0,
     4     .5,.5,.0,6*.0,
     5     .5,.5,.5,6*.0,
     6     .333333,.666667,.666667,.666667,.333333,.333333,3*.0,
     7     .666667,.333333,.666667,.333333,.666667,.333333,3*.0,
     8     .0,.5,.5,.5,.0,.5,.5,.5,.0/
      save CenText,ilMax,nCrwFirst,nCrwSel,nVTX,VT6X,BratVT6X,NSelX,
     1     CenAcceptable,icMax,nButtFirst,nButtLast,CentrMultOld,
     2     NAtCalcIn
      allocatable VT6X,NSelX,BratVT6X
      NAtCalcIn=NAtCalc
      NAtCalc=0
      UseOld=EqRV(CellParSel(1),CellParSelOld(1),3,DiffAxe  ).and.
     1       EqRV(CellParSel(4),CellParSelOld(4),3,DiffAngle).and.
     2       EqRV(CellTrSel6,CellTrSel6Old,NDimQ(KPhase),.001).and.
     3       NCellSel.eq.NCellSelOld.and.
     4       abs(SumaObsLimCentr-SumaObsLimCentrOld).lt..001.and.
     5       LaueGroupSel.eq.LaueGroupUsedInCentr.and.
     6       CentrMultOld.eq.CentrMult.and.
     7       (TakeSGTwin.eqv.TakeSGTwinUsed.eq.1).and.
     8       TakeSGTwinUsed.ge.0
      if(UseOld) go to 1500
      call CopyVek(CellParSel,CellParSelOld,6)
      call CopyVek(CellTrSel6,CellTrSel6Old,NDimQ(KPhase))
      if(TakeSGTwin) then
        TakeSGTwinUsed=1
      else
        TakeSGTwinUsed=0
      endif
      if(isPowder) then
        XCentr=.false.
        NSymm(KPhase)=2
        call RealMatrixToOpposite(rm6(1,1,1,KPhase),
     1                            rm6(1,2,1,KPhase),NDim(KPhase))
        call SetRealArrayTo(s6(1,2,1,KPhase),NDim(KPhase),0.)
      else
        nn=CentrMult
        n=nn**NDim(KPhase)-1
        CentrStep=1./float(nn)
        call SetIntArrayTo(nx,NDim(KPhase),nn)
        call SetIntArrayTo(NSpec,8,0)
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.centr','formatted','unknown')
        do 1100i=1,n
          call RecUnPack(i+1,ni,nx,NDim(KPhase))
          do j=1,NDim(KPhase)
            BasCenVec(j,i)=CentrStep*float(ni(j)-1)
          enddo
          do j=4,NDim(KPhase)
            if(ni(j).gt.1) go to 1100
          enddo
          NNul=0
          KdeNul=0
          do j=1,3
            if(ni(j).eq.1) then
              NNul=NNul+1
              KdeNul=j
            endif
          enddo
          if(nn.eq.2) then
            if(NNul.eq.1) then
              NSpec(KdeNul+1)=i
            else if(NNul.eq.0) then
              NSpec(5)=i
            endif
          else
            if(NNul.eq.0) then
              if(ni(1).eq.2.and.ni(2).eq.3.and.ni(3).eq.3) then
                NSpec(6)=i+NSpec(6)
              else if(ni(1).eq.3.and.ni(2).eq.2.and.ni(3).eq.2) then
                NSpec(6)=i*1000+NSpec(6)
              else if(ni(1).eq.3.and.ni(2).eq.2.and.ni(3).eq.3) then
                NSpec(7)=i+NSpec(7)
              else if(ni(1).eq.2.and.ni(2).eq.3.and.ni(3).eq.2) then
                NSpec(7)=i*1000+NSpec(7)
              endif
            endif
          endif
1100    continue
        call SetIntArrayTo(NCenObs,n,0)
        call SetIntArrayTo(NCenAll,n,0)
        call SetRealArrayTo(SCenObs,n,0.)
        call SetRealArrayTo(SCenAll,n,0.)
        call SetIntArrayTo(NCenObsSpec,8,0)
        call SetIntArrayTo(NCenAllSpec,8,0)
        call SetRealArrayTo(SCenObsSpec,8,0.)
        call SetRealArrayTo(SCenAllSpec,8,0.)
        call SetRealArrayTo(h,6,0.)
        do i=1,NRefRead
          do ntw=1,NTwin
            if(HCondAr(ntw,i).lt.0) then
              h(1,ntw)=999
              cycle
            endif
            call IndUnPack(HCondAr(ntw,i),ih,HCondLn,HCondMx,
     1                     NDim(KPhase))
            do k=1,NDim(KPhase)
              hp(k)=ih(k)
            enddo
            call multm(hp,CellTrSel6,h(1,ntw),1,NDim(KPhase),
     1                 NDim(KPhase))
          enddo
          if(rsar(i).gt.0.) then
            RelI=max(riar(i)/rsar(i),0.)
          else
            RelI=0.
          endif
          Observed=RelI.gt.3.
          do 1200j=1,n
            do ntw=1,NTwin
              if(h(1,ntw).gt.900) cycle
              pom=VecOrtScal(h(1,ntw),BasCenVec(1,j),NDim(KPhase))
              if(abs(anint(pom)-pom).le..0001) then
                go to 1200
              endif
            enddo
            SCenAll(j)=SCenAll(j)+RelI
            NCenAll(j)=NCenAll(j)+1
            if(Observed) then
              NCenObs(j)=NCenObs(j)+1
              SCenObs(j)=SCenObs(j)+RelI
            endif
1200      continue
          do 1300j=2,8
            do 1250ntw=1,NTwin
              if(h(1,ntw).gt.900) cycle
              do k=1,NBasCenVecSpec(j)
                pom=VecOrtScal(h(1,ntw),BasCenVecSpec(1,k,j),3)
                if(abs(anint(pom)-pom).gt..0001) go to 1250
              enddo
              go to 1300
1250        continue
            NCenAllSpec(j)=NCenAllSpec(j)+1
            SCenAllSpec(j)=SCenAllSpec(j)+RelI
            if(Observed) then
              NCenObsSpec(j)=NCenObsSpec(j)+1
              SCenObsSpec(j)=SCenObsSpec(j)+RelI
              write(ln,FormExtRefl) j,(nint(h(k,1)),k=1,NDim(KPhase)),
     1                              riar(i),rsar(i)
            endif
1300      continue
        enddo
        do i=1,n
          if(NCenAll(i).gt.0) then
            SCenAll(i)=SCenAll(i)/float(NCenAll(i))
          else
            SCenAll(i)=0.
          endif
          if(NCenObs(i).gt.0) then
            SCenObs(i)=SCenObs(i)/float(NCenObs(i))
          else
            SCenObs(i)=0.
          endif
        enddo
        do i=2,8
          if(NCenAllSpec(i).gt.0) then
            SCenAllSpec(i)=SCenAllSpec(i)/float(NCenAllSpec(i))
          else
            SCenAllSpec(i)=0.
          endif
          if(NCenObsSpec(i).gt.0) then
            SCenObsSpec(i)=SCenObsSpec(i)/float(NCenObsSpec(i))
          else
            SCenObsSpec(i)=0.
          endif
        enddo
        FLim=SumaObsLimCentr
        if(.not.allocated(VT6X))
     1    allocate(VT6X(NDim(KPhase),nlim),BratVT6X(nlim),NSelX(nlim))
        iziz=0
1420    call SetRealArrayTo(VT6X(1,1),NDim(KPhase),0.)
        call SetLogicalArrayTo(BratVT6X,nlim,.true.)
        nVTX=1
        XCentr=.false.
        iziz=iziz+1
        do 1480i=1,n
          if(SCenObs(i).lt.Flim) then
            nVTX=nVTX+1
            call CopyVek(BasCenVec(1,i),VT6X(1,nVTX),NDim(KPhase))
            NSelX(nVTX)=i
            if(.not.XCentr) then
              if(nn.eq.2) then
                do j=2,5
                  if(NSpec(j).eq.i) go to 1480
                enddo
                XCentr=.true.
              else
                do j=6,7
                  if(mod(NSpec(j),1000).eq.i.or.NSpec(j)/1000.eq.i)
     1              go to 1480
                enddo
                XCentr=.true.
              endif
            endif
          endif
1480    continue
        if(XCentr) then
          nVTXOld=nVTX
          call EM50CompleteCentr(1,VT6X,ubound(VT6X,1),nVTX,nlim,ich)
          if(nVTX.ne.nVTXOld.or.ich.ne.0) then
            if(FLim.gt.0.) then
              Flim=Flim-.1
              go to 1420
            else
              XCentr=.false.
            endif
          endif
        endif
        call CloseIfOpened(ln)
      endif
      NCellSelOld=NCellSel
      SumaObsLimCentrOld=SumaObsLimCentr
      call SetIntArrayTo(CenAcceptable,9,1)
      CenAcceptable(6)=0
      CenAcceptable(7)=0
      if(NCellSel.eq.IdTriclinic) then
        CenAcceptable(6)=1
        CenAcceptable(7)=1
      else if(NCellSel.eq.IdTetragonalA.or.
     1        NCellSel.eq.IdTetragonalB.or.
     2        NCellSel.eq.IdTetragonalC) then
        CenAcceptable(2)=0
        CenAcceptable(3)=0
      else if(NCellSel.eq.IdTrigonal.or.
     1        NCellSel.eq.IdRhombicAmBmC.or.
     2        NCellSel.eq.IdRhombicmABmC.or.
     3        NCellSel.eq.IdRhombicmAmBC.or.
     4        NCellSel.eq.IdRhombicABC) then
        call SetIntArrayTo(CenAcceptable(2),7,0)
        CenAcceptable(6)=1
        CenAcceptable(7)=1
      else if(NCellSel.eq.IdHexagonal) then
        call SetIntArrayTo(CenAcceptable(2),7,0)
      else if(NCellSel.eq.IdCubic) then
        call SetIntArrayTo(CenAcceptable(2),3,0)
      endif
1500  ich=0
      id=NextQuestId()
      call FeQuestTitleMake(id,'Select cell centering')
      ilm=7
      if(XCentr) ilm=ilm+2
      xpom=5.
      tpom=xpom+5.+CrwgXd
      tpomb=WizardLength-100.
      dpomb=85.
      pom=50.
      il=1
      call FeQuestLblMake(id,pom,il,'Centering','L','N')
      if(isPowder) then
        pom=pom+87.
        Veta='Rp(obs)/Rp(all)'
      else
        pom=pom+105.
        Veta='obs/all'
      endif
      call FeQuestLblMake(id,pom,il,Veta,'L','N')
      if(isPowder) then
        pom=pom+100.
        Veta='N(Extinct)/N(Gener)'
      else
        pom=pom+92.
        Veta='ave(I/sig(I))'
      endif
      call FeQuestLblMake(id,pom,il,Veta,'L','N')
      call FeTabsReset(4)
      pom=50.
      call FeTabsAdd(pom,4,IdCenterTab,' ')
      pom=150.
      call FeTabsAdd(pom,4,IdCharTab,'/')
      pom=pom+100.
      if(isPowder) then
        t1='.'
      else
        t1='/'
      endif
      call FeTabsAdd(pom,4,IdCharTab,t1)
      UseTabs=4
      if(UseOld) then
        do il=2,ilMax
          if(.not.isPowder) then
            call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'Details')
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          endif
          if(CenAcceptable(il-1).eq.0) then
            call FeQuestLblMake(id,5.,il,'n.a.','L','N')
            call FeQuestLblMake(id,tpom,il,CenText(il),'L','N')
          else
            call FeQuestCrwMake(id,tpom,il,xpom,il,CenText(il),'L',
     1                          CrwgXd,CrwgYd,0,1)
            call FeQuestCrwOpen(CrwLastMade,CrwLastMade.eq.nCrwSel)
          endif
        enddo
        il=ilMax
      else
        ic=0
        UseTabs=4
        LeBailFirst=.true.
        LaueGroupUsedInCentr=LaueGroupSel
        if(XCentr) then
          icMax=9
        else
          icMax=8
        endif
        nCrwOpt=1
        SumaObsOpt=0.
        call FeFillTextInfo('spacegrouptest0.txt',0)
        ilp=-(WizardLines-1)*10
        tpomp=WizardLength*.5
        do i=1,NInfo
          call FeQuestLblMake(id,tpomp,ilp,TextInfo(i),'C','B')
          if(i.eq.1) nLblW=LblLastMade
          ilp=ilp-5
          if(i.eq.2) ilp=ilp-3
        enddo
        do 2000i=1,icMax
          il=il+1
          Cislo=' '
          if(i.eq.IdCentrRObv) then
            Cislo='-obverse'
            ic=6
          else if(i.eq.IdCentrRRev) then
            Cislo='-reverse'
            ic=6
          else if(i.ge.IdCentrF) then
            ic=i-1
          else
            ic=i
          endif
          CenText(il)=Tabulator//smbc(ic:ic)//Cislo(:idel(Cislo))
          if(isPowder) then
            call EM50GenVecCentr(ic,LeBailFirst,ich)
            if(LeBailFirst) then
              call PwdLeBail(0,.false.)
              ln=NextLogicNumber()
              Veta=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
              call OpenFile(ln,Veta,'formatted','old')
              if(ErrFlag.ne.0) then
                ich=1
                go to 9999
              endif
              if(allocated(ihar)) deallocate(ihar,FlgAr,KPhAr,MltAr)
              NRef=NRef91(KDatBlock)
              allocate(ihar(6,NRef),flgar(NRef),KPhAr(NRef),MltAr(NRef))
              do j=1,NRef
                read(ln,format91pow,end=1920,err=1920)(ihar(k,j),k=1,6),
     1                                             pom,pom,k,k,KPhAr(j)
                MltAr(j)=pom
                FlgAr(j)=1
              enddo
              call CloseIfOpened(ln)
              LeBailFirst=.false.
              NVse1=NRef-1
              NExt1=0
              go to 1950
1920          ich=1
              call CloseIfOpened(ln)
              go to 9999
            else
              NVse1=0
              NExt1=0
              do 1940j=1,NRef
                if(ihar(1,j).gt.900) then
                  FlgAr(j)=1
                  cycle
                endif
                do k=1,NDim(KPhase)
                  hp(k)=ihar(k,j)
                enddo
                NVse1=NVse1+1
                do k=2,NLattVec(KPhase)
                  pom=VecOrtScal(hp,VT6(1,k,1,KPhase),NDim(KPhase))
                  if(abs(anint(pom)-pom).gt..0001) then
                    FlgAr(j)=0
                    NExt1=NExt1+1
                    go to 1940
                  endif
                enddo
                FlgAr(j)=1
1940          continue
              call PwdLeBailFromHKL(0,.false.,IHAr,FlgAr,KPhAr,MltAr,
     1                              NRef)
            endif
1950        write(Radka,101) LeBailRFObs,LeBailRF
            call Zhusti(Radka)
            write(Cislo,'(f10.4)') float(NExt1)/float(NVse1)
            call Zhusti(Cislo)
            CenText(il)=CenText(il)(:idel(CenText(il)))//Tabulator//
     1                  Radka(:idel(Radka))//Tabulator//
     2                  Cislo(:idel(Cislo))
          else
            if(i.eq.IdCentrP) then
              nall=0
              nobs=0
              SumaObs=0.
              SumaAll=0.
            else if(i.eq.IdCentrX) then
              ilx=il
              nobs=0
              nall=0
              SumaObs=0.
              SumaAll=0.
            else
              nobs=NCenObsSpec(i)
              nall=NCenAllSpec(i)
              SumaObs=SCenObsSpec(i)
              SumaAll=SCenAllSpec(i)
            endif
            write(Radka,100) nobs,nall
            call Zhusti(Radka)
            CenText(il)=CenText(il)(:idel(CenText(il)))//Tabulator//
     1                  Radka(:idel(Radka))
            write(Radka,101) SumaObs,SumaAll
            call Zhusti(Radka)
            CenText(il)=CenText(il)(:idel(CenText(il)))//Tabulator//
     1                  Radka(:idel(Radka))
          endif
          if(.not.isPowder) then
            call FeQuestButtonMake(id,tpomb,il,dpomb,ButYd,'Details')
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          endif
          if(CenAcceptable(il-1).eq.0) then
            call FeQuestLblMake(id,5.,il,'n.a.','L','N')
            call FeQuestLblMake(id,tpom,il,CenText(il),'L','N')
          else
            call FeQuestCrwMake(id,tpom,il,xpom,il,CenText(il),'L',
     1                          CrwgXd,CrwgYd,0,1)
            if(i.eq.1) then
              nCrwFirst=CrwLastMade
              nButtFirst=ButtonLastMade
            endif
            if(i.eq.IdCentrX) nCrwX=CrwLastMade
            call FeQuestCrwOpen(CrwLastMade,.false.)
            if(isPowder) then
              nCrwOpt=1
            else
              if(SumaObs.lt.SumaObsLimCentr.and.SumaObs.ge.SumaObsOpt)
     1          then
                SumaObsOpt=SumaObs
                nCrwOpt=CrwLastMade
              endif
            endif
          endif
          call FeReleaseOutput
          call FeDeferOutput
2000    continue
        ilMax=il
        nButtLast=ButtonLastMade
        call FeQuestCrwOn(nCrwOpt)
        nLbl=nLblW
        do i=1,NInfo
          call FeQuestLblOff(nLbl)
          nLbl=nLbl+1
        enddo
        call FeMouseShape(0)
      endif
      if(XCentr) then
        il=il+1
        Radka='%Show/modify X centering'
        dpom=FeTxLengthUnder(Radka)+10.
        xpom=(WizardLength-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Radka)
        nButtXCentr=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtXCentr=0
      endif
      call FeFillTextInfo('spacegrouptest1.txt',0)
      il=-(il+2)*10
      tpom=5.
      do i=1,NInfo
        call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
        il=il-5
        if(i.eq.2) il=il-3
      enddo
2100  if(XCentr) then
        NCenObsX=0
        NCenAllX=0
        SCenObsX=0.
        SCenAllX=0.
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.centrx','formatted','unknown')
        do 2140i=1,NRefRead
          do ntw=1,NTwin
            if(HCondAr(ntw,i).lt.0) then
              h(1,ntw)=999
              cycle
            endif
            call IndUnPack(HCondAr(ntw,i),ih,HCondLn,HCondMx,
     1                     NDim(KPhase))
            do k=1,NDim(KPhase)
              hp(k)=ih(k)
            enddo
            call multm(hp,CellTrSel6,h(1,ntw),1,NDim(KPhase),
     1                 NDim(KPhase))
          enddo
          do 2120ntw=1,NTwin
            if(h(1,ntw).gt.900) cycle
            do k=2,nVTX
              if(BratVT6X(k)) then
                pom=VecOrtScal(h(1,ntw),VT6X(1,k),NDim(KPhase))
                if(abs(anint(pom)-pom).gt..0001) go to 2120
              endif
            enddo
            go to 2140
2120      continue
          RelI=max(riar(i)/rsar(i),0.)
          Observed=RelI.gt.3.
          SCenAllX=SCenAllX+RelI
          NCenAllX=NCenAllX+1
          if(Observed) then
            NCenObsX=NCenObsX+1
            SCenObsX=SCenObsX+RelI
            write(ln,FormExtRefl) 9,(nint(h(k,1)),k=1,NDim(KPhase)),
     1                            riar(i),rsar(i)
          endif
2140    continue
        call CloseIfOpened(ln)
        if(NCenAllX.gt.0) then
          SCenAllX=SCenAllX/float(NCenAllX)
        else
          SCenAllX=0.
        endif
        if(NCenObsX.gt.0) then
          SCenObsX=SCenObsX/float(NCenObsX)
        else
          SCenObsX=0.
        endif
        write(Radka,100) NCenObsX,NCenAllX
        call Zhusti(Radka)
        i=LocateSubstring(CenText(ilx),Tabulator,.false.,.true.)
        i=LocateSubstring(CenText(ilx)(i+1:),Tabulator,.false.,.true.)+i
        CenText(ilx)=CenText(ilx)(:i-1)//Tabulator//Radka(:idel(Radka))
        write(Radka,101) SCenObsX,SCenAllX
        call Zhusti(Radka)
        CenText(ilx)=CenText(ilx)(:idel(CenText(ilx)))//Tabulator//
     1               Radka(:idel(Radka))
        call FeQuestCrwLabelChange(nCrwX,CenText(ilx))
      endif
2200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtXCentr) then
        UseTabsIn=UseTabs
        WizardMode=.false.
        idp=NextQuestId()
        Veta=Tabulator//'Vector'//Tabulator//'#reflections'//
     1       Tabulator//'ave(I/sig(I))'
        UseTabsL=NextTabs()
        pom=15.
        call FeTabsAdd(pom,UseTabsL,IdRightTab,' ')
        pom=FeTxLength('(')+float(NDim(KPhase)-1)*FeTxLength('X/X,')+
     1      FeTxLength('X/X)|')
        UseTabsI=NextTabs()
        call FeTabsAdd(pom,UseTabsI,IdRightTab,' ')
        call FeTabsAdd(pom+15.,UseTabsL,IdRightTab,' ')
        Radka='XXXXXXX/XXXXXXX'
        call FeTabsAdd(pom+.5*FeTxLength(Radka),UseTabsI,IdCharTab,'/')
        pom=pom+FeTxLength(Radka)+FeTxLength('|')
        call FeTabsAdd(pom,UseTabsI,IdRightTab,' ')
        call FeTabsAdd(pom+15.,UseTabsL,IdRightTab,' ')
        call FeTabsAdd(pom+.5*FeTxLength(Radka),UseTabsI,IdCharTab,'/')
        xqdp=pom+FeTxLength(Radka)+2.*EdwMarginSize+SbwPruhXd+10.
        ilk=9
        call FeQuestCreate(idp,-1.,-1.,xqdp,ilk,'Select individual'//
     1                     ' centring vectors',0,LightGray,-1,0)
        il=1
        xpom=5.
        UseTabs=UseTabsL
        call FeQuestLblMake(idp,xpom,il,Veta,'L','N')
        il=8
        dpom=xqdp-10.-SbwPruhXd
        UseTabs=UseTabsI
        call FeQuestSbwMake(idp,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                      SbwVertical)
        nSbw=SbwLastMade
        il=il+1
        dpom=100.
        xpom=xqdp*.5-dpom-10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,'%Refresh')
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xqdp*.5+10.
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,'%Select all')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_xcentr.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 2800
        do i=1,nVTX
          Radka='('
          do j=1,NDim(KPhase)
            call ToFract(vt6X(j,i),Cislo,15)
            Radka=Radka(:idel(Radka))//Cislo(:idel(Cislo))
            if(j.lt.NDim(KPhase)) then
              Cislo=','
            else
              Cislo=')'
            endif
            Radka=Radka(:idel(Radka))//Cislo(:idel(Cislo))
          enddo
          Radka=Radka(:idel(Radka))//Tabulator//'|'
          if(i.ne.1) then
            l=NSelX(i)
            nobs=NCenObs(l)
            nall=NCenAll(l)
          else
            l=0
            nobs=0
            nall=0
          endif
          write(Veta,100) nobs,nall
          call Zhusti(Veta)
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))//
     1          Tabulator//'|'
          if(l.eq.0) then
            SumaObs=0.
            SumaAll=0.
          else
            SumaObs=SCenObs(l)
            SumaAll=SCenAll(l)
          endif
          write(Veta,101) SumaObs,SumaAll
          call Zhusti(Veta)
          ii=index(Veta,'/')+1
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))
          write(ln,FormA) Radka(:idel(Radka))
        enddo
        call CloseIfOpened(ln)
2600    do i=1,nVTX
          if(BratVT6X(i)) then
            j=1
          else
            j=0
          endif
          call FeQuestSetSbwItemSel(i,nSbw,j)
        enddo
        call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_xcentr.tmp')
2700    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton) then
          call SetLogicalArrayTo(BratVT6X(2),nVTX-1,
     1                           CheckNumber.ne.nButtRefresh)
          call CloseIfOpened(SbwLnQuest(nSbw))
          go to 2600
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2700
        endif
        if(ich.eq.0) then
          do i=1,nVTX
            BratVT6X(i)=SbwItemSelQuest(i,nSbw).eq.1
          enddo
        endif
2800    call FeQuestRemove(idp)
        call FeTabsReset(UseTabsL)
        call FeTabsReset(UseTabsI)
        UseTabs=UseTabsIn
        call DeleteFile(fln(:ifln)//'_xcentr.tmp')
        call FeQuestButtonOff(nButtXCentr)
        WizardMode=.true.
        go to 2100
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.ge.nButtFirst.and.CheckNumber.le.nButtLast)
     2  then
        n=CheckNumber-nButtFirst+1
        lni=NextLogicNumber()
        if(n.eq.9) then
          Cislo='.centrx'
          NCenObsP=NCenObsX
        else
          Cislo='.centr'
          NCenObsP=NCenObsSpec(n)
        endif
        call OpenFile(lni,fln(:ifln)//Cislo(:idel(Cislo)),'formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 2200
        FileName=fln(:ifln)//'_tmp.centr'
        call OpenFile(lst,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 2200
        LstOpened=.true.
        Cislo=' '
        if(n.eq.IdCentrRObv) then
          Cislo='-obverse'
          ic=6
        else if(n.eq.IdCentrRRev) then
          Cislo='-reverse'
          ic=6
        else if(n.ge.IdCentrF) then
          ic=n-1
        else
          ic=n
        endif
        Uloha='List of strongest reflections contradicting '//
     1        smbc(ic:ic)//Cislo(:idel(Cislo))//' centering'
        call NewPg(1)
        if(NDimI(KPhase).eq.1) then
          call AddVek(QuIrrSel,QuRacSel,QuPom,3)
        else
          call CopyVek(QuIrrSel,QuPom,3*NDimI(KPhase))
        endif
        call DRWriteUsedPar(CellParSel,QuPom,CellTrSel6)
        if(NCenObsP.gt.0) then
          call TitulekVRamecku(Uloha)
          NExt(1)=0
          NExtObs(1)=0
          NExt500(1)=0
          RIExtMax(1)=0.
          SumRelI(1)=0.
2820      read(lni,FormExtRefl,end=2830) j,(ih(i),i=1,NDim(KPhase)),
     1                                   fpom,spom
          if(j.eq.n)
     1      call EM9NejdouPotvory(1,ih,hp,fpom,spom,NDim(KPhase),
     2                            .false.,3.)
          go to 2820
2830      call DRDejSpravnePotvory
        endif
        call CloseListing
        call FeListView(FileName,0)
2880    call CloseIfOpened(lni)
        call DeleteFile(FileName)
        go to 2200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2200
      endif
      if(ich.ge.0) then
        nCrw=nCrwFirst
        ic=0
        do i=1,icMax
          if(CenAcceptable(i).eq.0) cycle
          if(CrwLogicQuest(nCrw)) then
            CentrSel=i
            nCrwSel=nCrw
            go to 3100
          endif
          nCrw=nCrw+1
        enddo
      endif
3100  CentrMultOld=CentrMult
      if(CentrSel.eq.IdCentrX) then
        NLattVec(KPhase)=0
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),nlim,NComp(KPhase),
     1                   NPhase,1)
        do i=1,nVTX
          if(BratVT6X(i)) then
            NLattVec(KPhase)=NLattVec(KPhase)+1
            if(NLattVec(KPhase).gt.nlim) then
              write(Cislo,'(i3)') nlim
              call zhusti(Cislo)
              call FeChybne(-1.,-1.,'maximum number of '//
     1                      Cislo(:idel(Cislo))//
     2                      ' centring vectors exceeded.',' ',
     3                      SeriousError)
              ich=1
              go to 9900
            endif
            call CopyVek(VT6X(1,i),VT6(1,NLattVec(KPhase),1,KPhase),
     1                   NDim(KPhase))
          endif
        enddo
        call EM50CompleteCentr(0,VT6,ubound(VT6,1),NLattVec(KPhase),
     1                         nlim,ichp)
        if(ichp.ne.0) go to 9900
      endif
      go to 9999
      entry DRSGTestCentrClean
9900  if(allocated(VT6X)) deallocate(VT6X,BratVT6X,NSelX)
9999  NAtCalc=NAtCalcIn
      return
100   format(i15,'/'i15)
101   format(f10.3,'/',f10.3)
      end
