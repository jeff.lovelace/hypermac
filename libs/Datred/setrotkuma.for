      subroutine SetRotKuma(Om,Kappa,Phi,Rot)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension Rot(3,3),AM(3,3),BM(3,3)
      real Kappa
      call SetRotMatKuma(Om/ToRad,BetaKuma,1,Rot)
      call SetRotMatKuma(Kappa/ToRad,AlphaKuma,2,AM)
      call MultM(Rot,AM,BM,3,3,3)
      call SetRotMatKuma(Phi/ToRad,BetaKuma,2,AM)
      call MultM(BM,AM,Rot,3,3,3)
      return
      end
