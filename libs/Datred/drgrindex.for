      subroutine DRGrIndex
      use Basic_mod
      use DRIndex_mod
      use Refine_mod
      use Datred_mod
      parameter (NButt=16)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer :: nButtMinus(3),nButtPlus(3),nLblRot(3),RotateAxes=1,
     1           CrwStateQuest,ButtonStateQuest,nlam=6,RotateType=1,
     2           EdwStateQuest,GrIndexSuperOld(3)=(/0,0,0/)
      integer, allocatable :: ProjXI(:)
      real :: xp(5),xq(3),xo(3),h(3),RMat(3,3),PMat(3,3),xuo(4),yuo(4),
     1        CellDirOld(3),CellParOld(6),QPerO(3),QBasO(3),
     2        CellDirPrev(3,3),CellParPrev(3),CellParSave(6),
     3        TrToOrthoIPom(9),XDir(3) = (/0.,0.,1./),AngleStep=5.,
     4        LamGeneral=1.,IntMin,IntMax,IntDelta
      real, allocatable :: FInt(:)
      double precision Slozky8(4),UB8(3,3)
      character*256 Veta
      character*20 :: ButtLabels(NButt) =
     1               (/'%Quit             ',
     2                 '%Print            ',
     3                 '%Save             ',
     4                 '%New plot         ',
     5                 '%View direction   ',
     6                 '%2d section ON    ',
     7                 'Fill s%upercell   ',
     8                 '%All to cell ON   ',
     9                 '%Lattice ON       ',
     a                 'Fin%d cell        ',
     1                 'Sele%ct cell      ',
     2                 '%Refine cell      ',
     3                 'R%eset cell       ',
     4                 'Save inde%xed     ',
     5                 'Clean ref. %filter',
     6                 '%Options          '/)
      character*8 FileIndexExt,Nazev
      logical :: MameData,MouseActive,CrwLogicQuest,Zacina,PocitejXDir,
     1           FeYesNoHeader,FeYesNo,Nebrat,EqRV,ExistFile,EqIgCase,
     2           JeToTabBin,First=.true.,LatticeOnOld=.false.,EqIV,
     3           Poprve=.false.,Select,LevyInt,PravyInt
      equivalence (IdNumbers( 1),IdQuit),
     2            (IdNumbers( 2),IdPrint),
     3            (IdNumbers( 3),IdSave),
     4            (IdNumbers( 4),IdNew),
     5            (IdNumbers( 5),IdViewDir),
     6            (IdNumbers( 6),Id2dSection),
     7            (IdNumbers( 7),IdSupercell),
     8            (IdNumbers( 8),Id2Cell),
     9            (IdNumbers( 9),IdLattice),
     a            (IdNumbers(10),IdFindCell),
     1            (IdNumbers(11),IdSelectCell),
     2            (IdNumbers(12),IdRefine),
     3            (IdNumbers(13),IdReset),
     4            (IdNumbers(14),IdIndex),
     5            (IdNumbers(15),IdResetSel),
     6            (IdNumbers(16),IdOptions)
      NDimIn=NDim(KPhase)
      DiffAxeIn  =DiffAxe
      DiffAngleIn=DiffAngle
      KRefBlock=1
      KPhase=1
      if(.not.allocated(rm6)) call AllocateSymm(1,1,1,1)
      NSymm(KPhase)=1
      NLattVec(KPhase)=1
      Grupa(KPhase)='P1'
      Lattice(KPhase)='P'
      call SetRealArrayTo(QuRefBlock(1,1,KRefBlock),9,0.)
      if(First) then
        First=.false.
        GrIndexColor=Yellow
        GrIndexRad=0.
      endif
      call DRGrIndexSetBasic
      call FeBottomInfo('#prazdno#')
      Id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,120.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,130.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+5.
      ypom=pom-dpom-2.
      wpom=100.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      do i=1,NButt
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,ButtLabels(i))
        if(i.eq.IdQuit) then
          nButtQuit=ButtonLastMade
          j=ButtonOff
        else if(i.eq.IdPrint) then
          nButtPrint=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdSave) then
          nButtSave=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdNew) then
          nButtNew=ButtonLastMade
          j=ButtonOff
        else if(i.eq.Id2dSection) then
          nButt2dSection=ButtonLastMade
          j=ButtonOff
          ypom=ypom+dpom
        else if(i.eq.IdSupercell) then
          nButtSupercell=ButtonLastMade
          j=-1111
        else if(i.eq.IdViewDir) then
          nButtProjection=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.Id2Cell) then
          nButt2Cell=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdLattice) then
          nButtLattice=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdFindCell) then
          nButtFindCell=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdSelectCell) then
          nButtSelectCell=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdRefine) then
          nButtRefine=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdReset) then
          nButtReset=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdIndex) then
          nButtIndex=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdResetSel) then
          nButtResetSel=ButtonLastMade
          j=ButtonDisabled
        else if(i.eq.IdOptions) then
          nButtOptions=ButtonLastMade
          j=ButtonOff
        else if(i.eq.IdOptions) then
          nButtOptions=ButtonLastMade
          j=ButtonOff
        endif
        if(j.gt.0) call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.IdSave.or.i.eq.IdLattice.or.i.eq.IdOptions) then
          ypom=ypom-5.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          ypom=ypom-1.
        endif
        if(i.eq.IdOptions) then
          ypomp=ypom
          ypom=ButYd+5.
        else
          ypom=ypom-dpom
        endif
      enddo
      nButtBasTo=ButtonLastMade
!      ypom=ypom+dpom-6.
!      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,White)
      xpom=15.
      ypom=YMinGrWin-23.
      LocatorType=1
      Select=.false.
      do i=1,6
        if(i.eq.1) then
          Veta='$$GenRot'
        else if(i.eq.2) then
          Veta='$$AxRot'
        else if(i.eq.3) then
          Veta='$$MeasureD'
        else if(i.eq.4) then
          Veta='$$Limits'
        else if(i.eq.5) then
          Veta='$$Zoom'
        else if(i.eq.6) then
          xpom=xpom-3.*(CrwsXd+10.)
          Veta='$$Localize'
        endif
        call FeQuestAbsCrwMake(id,0.,ypom,xpom,ypom,Veta,'C',CrwsXd,
     1                         CrwsYd,1,1)
        if(i.ne.6) then
          call FeQuestCrwOpen(CrwLastMade,i.eq.LocatorType)
          call FeQuestCrwLock(CrwLastMade)
        endif
        if(i.eq.1) then
          nCrwRotGeneral=CrwLastMade
        else if(i.eq.2) then
          nCrwRotInPlane=CrwLastMade
        else if(i.eq.3) then
          nCrwSpan=CrwLastMade
        else if(i.eq.4) then
          nCrwSelect=CrwLastMade
        else if(i.eq.5) then
          nCrwZoom=CrwLastMade
        else if(i.eq.6) then
          nCrwLocalize=CrwLastMade
        endif
        if(i.eq.5) xpomp=xpom
        xpom=xpom+CrwsXd+10.
      enddo
      xpom=(XMaxBasWin+XMaxGrWin)*.5
      ypom=ypomp-10.
      call FeQuestAbsLblMake(id,xpom,ypom,'Rotate','C','B')
      ypom=ypom-5.
      xpom=XMaxGrWin+10.
      tpom=xpom+CrwgXd+10.
      Veta='Picture axes'
      do i=1,4
        ypom=ypom-18.
        call FeQuestAbsCrwMake(id,tpom,ypom+7.,xpom,ypom,Veta,'L',
     1                         CrwgXd,CrwgYd,1,2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.RotateAxes)
        if(i.eq.1) then
          Veta='Laboratory axes'
          nCrwRotateAxes=CrwLastMade
        else if(i.eq.2) then
          Veta='Direct axes'
        else if(i.eq.3) then
          Veta='Reciprocal axes'
        endif
      enddo
      ypom=ypom+3.
      xpom=(XMaxBasWin+XMaxGrWin)*.5
      wpom=FeTxLength('X')+10.
      do i=1,3
        ypom=ypom-21.
        call FeQuestAbsButtonMake(id,xpom-wpom-15.,ypom-ButYd*.5,wpom,
     1                            ButYd,'-')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtMinus(i)=ButtonLastMade
        if(RotateAxes.eq.1) then
          Veta='p'//Smbx(i)
        else if(RotateAxes.eq.2) then
          Veta=Smbx(i)
        else if(RotateAxes.eq.3) then
          Veta=SmbABC(i)
        else if(RotateAxes.eq.4) then
          Veta=SmbABC(i)//'*'
        endif
        call FeQuestAbsLblMake(id,xpom,ypom,Veta,'C','N')
        nLblRot(i)=LblLastMade
        call FeQuestAbsButtonMake(id,xpom+15.,ypom-ButYd*.5,wpom,ButYd,
     1                            '+')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtPlus(i)=ButtonLastMade
      enddo
      wpom=100.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      Veta='Ro%tate in steps'
      ypom=ypom-30.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Veta)
      nButtRotateType=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      dpom=50.
      ypom=ypom-10.
      xpom=(XMaxBasWin+XMaxGrWin)*.5
      call FeQuestAbsEdwMake(id,xpom,ypom,xpom-dpom*.5,ypom-25.,
     1                       'Angle step','C',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,AngleStep,.false.,.false.)
      nEdwAngleStep=EdwLastMade
      xpom=xpomp+10.
      call FeQuestAbsSvisliceFromToMake(id,xpom+15.,YMinBasWin,
     1                                  YMinGrWin-2.,0)
      dpom=FeTxLength('XXXX.XX')+15.
      ypom=YMinGrWin-27.
      xpom=xpom-dpom+3.
      do i=1,6
        xpom=xpom+dpom+45.
        if(i.le.3) then
          call FeQuestAbsCrwMake(id,xpom,ypom,xpom-25.,YBottomText-7.,
     1                           ' ','L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwCell=CrwLastMade
        endif
        call FeQuestAbsLblMake(id,xpom-3.,YBottomText-2.,
     1                         IndexWinfLabel(i),'R','N')
        if(i.eq.1) nLblCell=LblLastMade
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      call DRGrIndexShowCell
      FileIndex=' '
      MameData=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
      MouseActive=.false.
      Poprve=.true.
1400  if(MameData) then
        if(Index2d) then
          NCellMax=2
        else
          NCellMax=3
        endif
        NCell=0
        do i=1,NCellMax
          if(CellRefBlock(i,KRefBlock).gt.0.) NCell=NCell+1
        enddo
        if(CrwStateQuest(nCrwRotGeneral).ne.CrwOff.and.
     1     CrwStateQuest(nCrwRotGeneral).ne.CrwOn) then
          nCrw=nCrwRotGeneral
          do i=1,5
            if(CrwStateQuest(nCrw).ne.CrwDisabled)
     1        call FeQuestCrwOpen(nCrw,i.eq.LocatorType)
            nCrw=nCrw+1
          enddo
        endif
        call FeQuestButtonOff(nButtSave)
        call FeQuestButtonOff(nButtPrint)
        call FeQuestButtonOff(nButtReset)
        call FeQuestButtonOff(nButtFindCell)
        if(NTripl.gt.0) then
          call FeQuestButtonOff(nButtSelectCell)
        else
          call FeQuestButtonDisable(nButtSelectCell)
        endif
        call FeQuestButtonDisable(nButtResetSel)
        if(Index2d) then
          call FeQuestButtonDisable(nButtProjection)
        else
          call FeQuestButtonOff(nButtProjection)
          do i=1,GrIndexNAll
            if(FlagU(i).le.0) then
              call FeQuestButtonOff(nButtResetSel)
              exit
            endif
          enddo
        endif
        nCrw=nCrwRotateAxes
        do i=1,4
          call FeQuestCrwOpen(nCrw,i.eq.RotateAxes)
          if(NCell.lt.3.and.i.gt.2) call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        do i=1,3
          call FeQuestButtonOpen(nButtMinus(i),ButtonOff)
          call FeQuestLblOn(nLblRot(i))
          call FeQuestButtonOpen(nButtPlus(i),ButtonOff)
        enddo
        call FeQuestButtonOpen(nButtRotateType,ButtonOff)
        call FeQuestRealEdwOpen(nEdwAngleStep,AngleStep,.false.,.false.)
      else
        call FeQuestButtonDisable(nButtSave)
        call FeQuestButtonDisable(nButtPrint)
        call FeQuestButtonDisable(nButtResetSel)
        call FeQuestButtonDisable(nButtProjection)
        call FeQuestButtonDisable(nButtFindCell)
        call FeQuestButtonDisable(nButtSelectCell)
        nCrw=nCrwRotateAxes
        do i=1,4
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        do i=1,3
          call FeQuestButtonDisable(nButtMinus(i))
          call FeQuestLblDisable(nLblRot(i))
          call FeQuestButtonDisable(nButtPlus(i))
        enddo
        call FeQuestButtonDisable(nButtRotateType)
        call FeQuestEdwDisable(nEdwAngleStep)
        if(Poprve) then
          CheckType=EventButton
          CheckNumber=nButtNew
          Poprve=.false.
          call FeWait(.1)
          go to 1510
        endif
      endif
      GrCellDMax=0.
      do i1=0,GrIndexSuper(1)
        xp(1)=float(i1)-float(GrIndexSuper(1))*.5
        do i2=0,GrIndexSuper(2)
          xp(2)=float(i2)-float(GrIndexSuper(2))*.5
          do i3=0,GrIndexSuper(3)
            xp(3)=float(i3)-float(GrIndexSuper(3))*.5
            call MultM(UB(1,1,KRefBlock),xp,xq,3,3,1)
            GrCellDMax=max(GrCellDMax,VecOrtLen(xq,3))
          enddo
        enddo
      enddo
      if(CellProjection)
     1  call FeSetTransXo2X(-GrCellDMax,GrCellDMax,
     2                      -GrCellDMax,GrCellDMax,.true.)
      GrIndexSuperOld=GrIndexSuper
      call DRGrIndexPlot
1430  if(Index2d) then
        NCellMax=2
      else
        NCellMax=3
      endif
      if(LocatorType.eq.3) then
        nCrw=nCrwCell
        do i=1,NCellMax
          n=nCrw+CrwFr-1
          if(nCrw.eq.nCrwCellNew) then
            call FeCrwOn(n)
            CrwLogic(n)=.true.
          else
            call FeCrwOff(n)
            CrwLogic(n)=.false.
          endif
          nCrw=nCrw+1
        enddo
        ICell=nCrwCellNew-nCrwCell+1
        ICellOld=ICell
      endif
      NCell=0
      do i=1,NCellMax
        if(CellRefBlock(i,KRefBlock).gt.0.) NCell=NCell+1
      enddo
      if(NCell.eq.NCellMax) then
        if(NCellOld.ne.NCellMax) then
          call FeQuestButtonOff(nButtLattice)
          call FeQuestButtonOff(nButtRefine)
          call FeQuestButtonOff(nButtIndex)
          if(.not.Index2d)
     1      call FeQuestButtonOff(nButt2Cell)
          NCellOld=NCell
        endif
        if((Index2d.and.
     1      .not.EqRV(CellDirI,CellDirPrev,9,.001).or.
     2      .not.EqRV(CellRefBlock(1,KRefBlock),CellParPrev,3,.001)).or.
     3     (.not.Index2d.and.
     4      .not.EqRV(CellDirI,CellDirPrev,9,.001).or.
     5      .not.EqRV(CellRefBlock(1,KRefBlock),CellParPrev,3,.001)).or.
     6     .not.EqIV(GrIndexSuper,GrIndexSuperOld,3)) then
          if(Index2d) then
            CellDirI(3,1)=0.
            CellDirI(3,2)=0.
            CellDirI(3,3)=1.
            call MatInv(CellDirI,CellDir,pom,3)
            CellRefBlock(3,KRefBlock)=1.
          else
            call MatInv(CellDir,CellDirI,pom,3)
          endif
          do i=1,3
            do j=1,3
              UBI(i,j,KRefBlock)=CellDir(j,i)*CellRefBlock(i,KRefBlock)
            enddo
          enddo
          call MatInv(UBI(1,1,KRefBlock),UB(1,1,KRefBlock),pom,3)
          if(Index2d) then
            call CopyVek(CellDirI,CellDirPrev,9)
          else
            call CopyVek(CellDir ,CellDirPrev,9)
          endif
          call CopyVek(CellRefBlock(1,KRefBlock),CellParPrev,3)
        endif
      else
        IndexCoorSystem=min(IndexCoorSystem,1)
      endif
1500  HardCopy=0
      call FeQuestEvent(id,ich)
      if(Select.and.CheckType.ne.EventMouse) then
        call FeFillRectangle(XMinP-2.,XMaxP+2.,YMinP-2.,YMaxP+2.,4,
     1                       0,0,Black)
        LevyInt=.false.
        PravyInt=.false.
        Select=.false.
        call FeQuestCrwOff(nCrwSelect)
        if(CheckType.ne.EventCrw.or.
     1     (CheckNumber.lt.nCrwRotGeneral.or.
     2      CheckNumber.gt.nCrwLocalize)) then
          if(LocatorTypeOld.eq.2) then
            nCrw=nCrwRotInPlane
          else if(LocatorTypeOld.eq.3) then
            nCrw=nCrwSpan
          else if(LocatorTypeOld.eq.5) then
            nCrw=nCrwZoom
          else if(LocatorTypeOld.eq.6) then
            nCrw=nCrwLocalize
          else
            nCrw=nCrwRotGeneral
            LocatorTypeOld=1
          endif
          LocatorType=LocatorTypeOld
          call FeQuestCrwOn(nCrw)
        endif
      endif
1510  if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit).or.
     1   (CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape)) then
        call FeQuestRemove(id)
        go to 9000
      endif
1515  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtSave)
     1    then
          if(CheckNumber.eq.nButtPrint) then
            call FePrintPicture(ich)
          else
            call FeSavePicture('index',6,1)
            if(HardCopy.le.0) then
              HardCopy=0
              call FeQuestButtonOff(CheckNumber)
              go to 1500
            endif
          endif
          call FeHardCopy(HardCopy,'open')
          if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                            HardCopy.ne.HardCopyPCX))
     2      call DRGrIndexPlot
          call FeHardCopy(HardCopy,'close')
          if(CheckNumber.eq.nButtPrint) then
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
          endif
          HardCopy=0
          go to 1500
        else if(CheckNumber.eq.nButtNew) then
          NUB=0
          if(FileIndex.ne.' ')
     1      call DRGrIndexIOSmr(1,FileIndex(:idel(FileIndex))//'.smr')
          call FeFileManager('Select peak table file',FileIndex,
     1                       '*.txt *.xyz *.tab *.tabbin *.p4p '//
     2                       '*.clust *.cor *.drx *.topi *.m95',
     3                       0,.true.,ich)
          if(ich.ne.0) go to 1500
          call GetExtentionOfFileName(FileIndex,FileIndexExt)
          FileIndexExt='.'//FileIndexExt(:idel(FileIndexExt))
          ln=NextLogicNumber()
          if(EqIgCase(FileIndexExt,'.tabbin')) then
            open(ln,file=FileIndex,form='unformatted',access='direct',
     1           recl=8,err=1500)
            JeToTabBin=.true.
          else if(EqIgCase(FileIndexExt,'.m95')) then
            KRefBlock=1
            KPhaseDR=1
            call iom95(0,fln(:ifln)//'.m95')
            call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
            call DRSetCell(0)
            call MatInv(TrToOrtho(1,1,KPhaseDR),TrToOrthoIPom,pom,3)
          else
            call OpenFile(ln,FileIndex,'formatted','old')
            if(ErrFlag.ne.0) go to 1500
            JeToTabBin=.false.
          endif
          call DRGrIndexSetBasic
          ICellOld=-1
          ICell=-1
          NCellOld=0
          NCell=0
          IDir=0
          IDirOld=0
          call SetRealArrayTo(CellRefBlock(1,KRefBlock),6,-1.)
          call DRGrIndexShowCell
          nCrw=nCrwCell
          do i=1,3
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          LocatorType=1
          call FeQuestCrwClose(nCrwRotGeneral)
          nCrw=nCrwCell
          Veta=FileIndex
          call GetPureFileName(Veta,FileIndex)
          GrIndexNAll=0
          NSlozky=3
          if(EqIgCase(FileIndexExt,'.tab').or.
     1       EqIgCase(FileIndexExt,'.tabbin')) then
            NSlozky=4
            idp=NextQuestId()
            xqd=200.
            il=3
            Veta='Define wavelength:'
            call FeQuestCreate(idp,-1.,-1.,xqd,il,Veta,0,LightGray,-1,0)
            il=0
            xpom=5.
            tpom=xpom+CrwgXd+5.
            Veta='Mo KAlpha'
            do i=1,3
              il=il+1
              call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                            CrwgYd,1,1)
              if(i.eq.1) then
                Veta='Cu KAlpha'
                nCrwMo=CrwLastMade
              else if(i.eq.2) then
                Veta='General'
                nCrwCu=CrwLastMade
              else if(i.eq.3) then
                nCrwGeneral=CrwLastMade
              endif
              call FeQuestCrwOpen(CrwLastMade,(i.eq.1.and.nlam.eq.6).or.
     1                                        (i.eq.2.and.nlam.eq.5).or.
     2                                        (i.eq.3.and.nlam.eq.0))
              if(i.eq.3) then
                tpome=tpom+FeTxLength(Veta)+5.
                Veta='=>'
                xpome=tpome+FeTxLength(Veta)+5.
                call FeQuestEdwMake(idp,tpome,il,xpome,il,Veta,'L',50.,
     1                              EdwYd,0)
                nEdwGeneral=EdwLastMade
              endif
            enddo
1520        nCrw=nCrwMo
            do i=1,3
              if(CrwLogicQuest(nCrw)) then
                if(nCrw.eq.nCrwGeneral) then
                  if(EdwStateQuest(nEdwGeneral).ne.EdwOpened)
     1              call FeQuestRealEdwOpen(nEdwGeneral,LamGeneral,
     2                                      .false.,.false.)
                else
                  if(EdwStateQuest(nEdwGeneral).eq.EdwOpened) then
                    call FeQuestRealFromEdw(nEdwGeneral,LamGeneral)
                    call FeQuestEdwClose(nEdwGeneral)
                  endif
                endif
                exit
              endif
              nCrw=nCrw+1
            enddo
1525        call FeQuestEvent(idp,ichp)
            if(CheckType.eq.EventCrw) then
              go to 1520
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 1520
            endif
            if(CrwLogicQuest(nCrwMo)) then
              nlam=6
            else if(CrwLogicQuest(nCrwCu)) then
              nlam=5
            else
              call FeQuestRealFromEdw(nEdwGeneral,LamGeneral)
              nlam=0
            endif
            call FeQuestRemove(idp)
            if(JeToTabBin) then
              read(ln,rec=1) GrIndexNAll
              iz=39
            else
              read(ln,*) GrIndexNAll
            endif
            if(GrIndexNAll.le.0) then
              call FeChybne(-1.,-1.,'The input file doesn''t contain '//
     1                      'any reflection.','Define a new file.',
     2                      SeriousError)
              CheckType=EventButton
              CheckNumber=nButtNew
              go to 1515
            endif
          else if(EqIgCase(FileIndexExt,'.p4p')) then
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            lno=NextLogicNumber()
            call OpenFile(lno,Veta,'formatted','unknown')
1530        read(ln,FormA,end=1535) Veta
            if(Veta.eq.' ') go to 1530
            k=0
            call kus(Veta,k,Nazev)
            i=0
            if(EqIgCase(Nazev,'ref05').or.EqIgCase(Nazev,'ref1k')) then
              GrIndexNAll=GrIndexNAll+1
              call kus(Veta,k,Nazev)
              call StToReal(Veta,k,xp,3,.false.,ich)
              call StToReal(Veta,k,xp,3,.false.,ich)
              call StToReal(Veta,k,xp,3,.false.,ich)
              call StToReal(Veta,k,xp,2,.false.,ich)
              Intenzita=nint(xp(1))
              call StToReal(Veta,k,xp,3,.false.,ich)
              write(lno,'(3f10.4,i15)') xp(1:3),Intenzita
            else if(EqIgCase(Nazev,'ort1')) then
               i=1
            else if(EqIgCase(Nazev,'ort2')) then
               i=2
            else if(EqIgCase(Nazev,'ort3')) then
               i=3
            endif
            if(i.ne.0) then
              do j=1,3
                call StToReal(Veta,k,UB(i,j,KRefBlock),1,.false.,ich)
                if(ich.ne.0) go to 1530
c                if(i.le.2) UB(i,j,KRefBlock)=-UB(i,j,KRefBlock)
              enddo
              NUB=NUB+i
            endif
            go to 1530
1535        call CloseIfOpened(lno)
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            call OpenFile(ln,Veta,'formatted','unknown')
          else if(EqIgCase(FileIndexExt,'.drx')) then
            n=0
1550        read(ln,FormA,end=1555) Veta
            if(Veta(1:1).eq.'!'.or.idel(Veta).lt.36) then
              n=n+1
              go to 1550
            endif
            GrIndexNAll=GrIndexNAll+1
            go to 1550
1555        rewind ln
            do i=1,n
              read(ln,FormA)
            enddo
          else if(EqIgCase(FileIndexExt,'.m95')) then
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            lno=NextLogicNumber()
            call OpenFile(lno,Veta,'formatted','unknown')
1560        call DRGetReflectionFromM95(95,iend,ich)
            if(ich.ne.0) go to 1565
            if(iend.ne.0) go to 1565
            if(ri.lt.3.*rs) go to 1560
            pom1=corrf(1)*corrf(2)
            if(RunScN(KRefBlock).gt.0) then
              ikde=min((RunCCD-1)/RunScGr(KRefBlock)+1,
     1                 RunScN(KRefBlock))
              pom1=pom1*ScFrame(ikde,KRefBlock)
            endif
            ri=ri*pom1
            h=0.
            do i=1,3
              h(i)=ih(i)
              do j=1,maxNDim-3
                h(i)=h(i)+quRefBlock(i,j,0)*ih(j+3)
              enddo
            enddo
            call MultM(h,TrToOrthoIPom,xp,1,3,3)
            write(lno,'(3f10.4,i15)') xp(1:3),nint(ri)
            GrIndexNAll=GrIndexNAll+1
            go to 1560
1565        call CloseIfOpened(lno)
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            call OpenFile(ln,Veta,'formatted','unknown')
          else if(EqIgCase(FileIndexExt,'.topi')) then
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            lno=NextLogicNumber()
            call OpenFile(lno,Veta,'formatted','unknown')
1570        read(ln,FormA,end=1575) Veta
            if(Veta.eq.' ') go to 1570
            k=0
            call StToReal(Veta,k,xp,5,.false.,ich)
            if(ich.ne.0) go to 1570
            write(lno,'(3f10.4,i15)')(xp(i)/pi2,i=3,5),nint(xp(1))
            GrIndexNAll=GrIndexNAll+1
            go to 1570
1575        call CloseIfOpened(lno)
            Veta=FileIndex(:idel(FileIndex))//'_tmp.xyz'
            call OpenFile(ln,Veta,'formatted','unknown')
          else
1580        read(ln,FormA,end=1585) Veta
            GrIndexNAll=GrIndexNAll+1
            go to 1580
1585        rewind ln
          endif
          if(allocated(Slozky)) deallocate(Slozky,ProjX,ProjMX,ProjY,
     1                                     ProjMY,Int,FlagS,FlagI,FlagU,
     2                                     ha,iha)
          allocate(Slozky(4,GrIndexNAll),
     1             ProjX(GrIndexNAll),ProjMX(GrIndexNAll),
     2             ProjY(GrIndexNAll),ProjMY(GrIndexNAll),
     3             Int(GrIndexNAll),FlagS(GrIndexNAll),
     4             FlagI(GrIndexNAll),FlagU(GrIndexNAll),
     5             ha(6,GrIndexNAll),iha(6,GrIndexNAll))
          call SetIntArrayTo(FlagI,GrIndexNAll,0)
          call SetIntArrayTo(iha,6*GrIndexNAll,0)
          GrIndexDMax=0.
          GrIndexIMax=0.
          GrIndexSMax=0.
          do i=1,GrIndexNAll
            if(JeToTabBin) then
              do j=1,4
                iz=iz+1
                read(ln,rec=iz) Slozky8(j)
              enddo
              iz=iz+1
              read(ln,rec=iz) IntPom
              pom=IntPom
              do j=1,4
                Slozky(j,i)=Slozky8(j)
              enddo
              iz=iz+16
            else
              read(ln,*)(Slozky(j,i),j=1,NSlozky),pom
            endif
            if(NSlozky.eq.4) then
              if(nlam.ne.0) then
                poml=LamAveD(nlam)
              else
                poml=LamGeneral
              endif
              do j=1,4
                Slozky(j,i)=Slozky(j,i)/poml
              enddo
            endif
            Slozky(4,i)=VecOrtLen(Slozky(1,i),3)
            GrIndexDMax=max(GrIndexDMax,Slozky(4,i))
            GrIndexIMax=max(GrIndexIMax,pom)
            GrIndexSMax=max(GrIndexSMax,.5*Slozky(4,i))
            Int(i)=pom
            FlagS(i)=1
            FlagU(i)=1
          enddo
          GrIndexSMax=anint(GrIndexSMax*1000.+.5)/1000.
          call UnitMat(F2O,3)
          call UnitMat(O2F,3)
          call FeSetTransXo2X(-GrIndexDMax,GrIndexDMax,-GrIndexDMax,
     1                         GrIndexDMax,.true.)
          Zoom=1.
          ZoomActual=1.
          CellProjection=.false.
          MameData=.true.
          Index2d=.false.
          call SetRealArrayTo(CellDirPrev,9,0.)
          call SetRealArrayTo(CellParPrev,3,0.)
          NDim(KPhase)=3
          NDimI(KPhase)=0
          NDimQ(KPhase)=9
          nLbl=nLblCell
          do i=1,6
            call FeQuestLblChange(nLbl,IndexWinfLabel(i))
            nLbl=nLbl+1
          enddo
          if(ExistFile(FileIndex(:idel(FileIndex))//'.smr')) then
            call DRGrIndexIOSmr(0,FileIndex(:idel(FileIndex))//'.smr')
            call DRGrIndexShowCell
            call CloseIfOpened(ln)
            NCell=0
            do i=1,3
              if(CellRefBlock(i,KRefBlock).gt.0.) NCell=NCell+1
            enddo
            call DRGrIndexTryInd
            go to 1400
          else
            call SetRealArrayTo(CellRefBlockSU(1,KRefBlock),6, 0.)
            call SetRealArrayTo(QuRefBlock  (1,1,KDatBlock),9, 0.)
            call SetRealArrayTo(QuRefBlockSU(1,1,KDatBlock),9, 0.)
            pom=0.
            if(EqIgCase(FileIndexExt,'.tab')) then
              do i=1,3
                read(ln,104)(UB(i,j,KRefBlock),j=1,3)
                read(ln,104)
                do j=1,3
                  UB(i,j,KRefBlock)=UB(i,j,KRefBlock)/LamAveD(NLam)
                  pom=pom+abs(UB(i,j,KRefBlock))
                enddo
              enddo
            else if(EqIgCase(FileIndexExt,'.tabbin')) then
              if(nlam.ne.0) then
                poml=LamAveD(nlam)
              else
                poml=LamGeneral
              endif
              write(Veta,'(f10.6,i5)') LamGeneral,nlam
              call FeWinMessage(Veta,' ')
              do i=1,3
                do j=1,3
                  iz=iz+1
                  read(ln,rec=iz) UB8(i,j)
                  UB(i,j,KRefBlock)=UB8(i,j)/poml
                  pom=pom+abs(UB(i,j,KRefBlock))
                enddo
              enddo
            else if(EqIgCase(FileIndexExt,'.p4p')) then
              if(NUB.eq.6) pom=1.
            else if(EqIgCase(FileIndexExt,'.drx')) then
              call CloseIfOpened(ln)
              call GetPureFileName(FileIndex,Veta)
              Veta=Veta(:idel(Veta))//'.rmat'
              if(ExistFile(Veta)) then
                call OpenFile(ln,Veta,'formatted','old')
                n=0
1590            read(ln,FormA,end=1595) Veta
                if(Veta(1:1).eq.'#') go to 1590
                k=0
                call Kus(Veta,k,Nazev)
                if(EqIgCase(Nazev,'RMAT')) then
                  do i=1,3
                    n=n+1
                    read(ln,FormA,end=1595) Veta
                    read(Veta,*)(UB(i,j,KRefBlock),j=1,3)
                  enddo
                endif
1595            if(n.ne.3) then
                  pom=0.
                else
                  pom=1.
                endif
              endif
            endif
            if(pom.gt.0.) then
              call DRCellFromUB
              call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),pom,3)
              do i=1,3
                do j=1,3
                  CellDir(j,i)=UBI(i,j,KRefBlock)/
     1                         CellRefBlock(i,KRefBlock)
                enddo
              enddo
              call MatInv(CellDir,CellDirI,pom,3)
              call DRGrIndexShowCell
            endif
            XDirection(1)=1.
            XDirection(2)=0.
            XDirection(3)=0.
            ViewDirection(1)=0.
            ViewDirection(2)=0.
            ViewDirection(3)=1.
            call CloseIfOpened(ln)
            go to 5000
          endif
        else if(CheckNumber.eq.nButt2dSection) then
          j=Id2dSection
          if(Index2d) then
            Veta=ButtLabels(j)(:idel(ButtLabels(j))-1)//'N'
          else
            Veta=ButtLabels(j)(:idel(ButtLabels(j))-1)//'FF'
          endif
          Index2d=.not.Index2d
          call FeQuestButtonLabelChange(nButt2dSection,Veta)
          if(Index2d) then
            call CopyVek(CellRefBlock(1,KRefBlock),CellParSave,6)
            call SetRealArrayTo(CellRefBlock(1,KRefBlock),6,-1.)
            call SetRealArrayTo(Cell2d,3,-1.)
            ICell=1
            xp=0.
            n=0
            pmin=99999.
            do i=1,GrIndexNAll
              if(FlagU(i).le.0) cycle
              if(Slozky(4,i).gt..01.and.Slozky(4,i).lt.pmin) then
                pmin=Slozky(4,i)
                ip=i
              endif
              do j=i+1,GrIndexNAll
                if(FlagU(j).le.0) cycle
                call VecMul(Slozky(1,i),Slozky(1,j),xo)
                pom=0.
                do k=1,3
                  pom=pom+xo(k)**2
                enddo
                if(pom/(Slozky(4,i)*Slozky(4,j)).lt..01) cycle
                call VecOrtNorm(xo,3)
                pom=0.
                do k=1,3
                  if(abs(xo(k)).gt.pom) then
                    pom=abs(xo(k))
                    km=k
                  endif
                enddo
                n=n+1
                do k=1,3
                  xp(k)=xp(k)+xo(k)/xo(km)
                enddo
              enddo
            enddo
            do i=1,3
              ViewDirection(i)=xp(i)/float(n)
            enddo
            do i=1,3
              XDirection(i)=Slozky(i,ip)/Slozky(4,ip)
            enddo
            call FeQuestButtonDisable(nButtProjection)
            call FeQuestButtonDisable(nButt2Cell)
            call FeQuestButtonDisable(nButtResetSel)
            call FeQuestCrwDisable(nCrwRotGeneral)
            call FeQuestCrwDisable(nCrwSelect)
            LocatorType=2
            nLbl=nLblCell
            nCrw=nCrwCell
            do i=1,6
              call FeQuestCrwClose(nCrw)
              if(i.le.3) then
                if(i.eq.1) then
                  Veta='d1'
                else if(i.eq.2) then
                  Veta='d2'
                else if(i.eq.3) then
                  Veta='angle'
                endif
                call FeQuestLblChange(nLbl,Veta)
                call FeWInfWrite(i,' ')
              else
                call FeQuestLblOff(nLbl)
                call FeWInfClose(i)
              endif
              nLbl=nLbl+1
              nCrw=nCrw+1
            enddo
          else
            call CopyVek(CellRefBlock(1,KRefBlock),Cell2d,2)
            Cell2d(3)=CellRefBlock(6,KRefBlock)
            call CopyVek(CellParSave,CellRefBlock(1,KRefBlock),6)
            if(NCell.eq.NCellMax) call DRGrIndexUB2d(ichp)
            call FeQuestButtonOff(nButtProjection)
            call FeQuestButtonOff(nButt2Cell)
            call FeQuestButtonDisable(nButtResetSel)
            do i=1,GrIndexNAll
              if(FlagU(i).le.0) then
                call FeQuestButtonOff(nButtResetSel)
                exit
              endif
            enddo
            call FeQuestCrwOff(nCrwRotGeneral)
            call FeQuestCrwOff(nCrwSelect)
            LocatorType=1
            nLbl=nLblCell
            nCrw=nCrwCell
            do i=1,6
              call FeQuestCrwClose(nCrw)
              call FeQuestLblChange(nLbl,IndexWinfLabel(i))
              call FeWInfWrite(i,' ')
              nLbl=nLbl+1
              nCrw=nCrw+1
            enddo
            NCellMax=3
            call DRGrIndexShowCell
          endif
          go to 5000
        else if(CheckNumber.eq.nButtProjection) then
          idp=NextQuestId()
          xqd=350.
          il=6
          call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
          il=1
          tpom=5.
          Veta='View along the axis:'
          call FeQuestLblMake(idp,tpom,il,Veta,'L','B')
          xpom=5.
          tpom=xpom+CrwgXd+5.
          n=0
          m=0
          nEdwOld=0
          do i=1,12
            if(i.le.3) then
              Veta=SmbABC(i)
            else if(i.eq.4) then
              Veta='General [u,v,w]'
            else if(i.le.7) then
              Veta=SmbABC(i-4)//'*'
            else if(i.eq.8) then
              Veta='General (h,k,l)'
            else if(i.le.11) then
              Veta=SmbX(i-8)
            else if(i.eq.12) then
              Veta='General (x,y,z)'
            endif
            if(i.eq.5.or.i.eq.9) then
              il=il-4
              tpom=tpom+120.
              xpom=xpom+120.
            endif
            il=il+1
            call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            KOpen=1
            if(i.le.3) then
              if(i.eq.1) nCrwDirectionFirst=CrwLastMade
              if(CellRefBlock(i,KRefBlock).lt.0) then
                KOpen=-1
              else
                n=n+1
              endif
            else if(i.le.8) then
              if(n.lt.3) KOpen=-1
            endif
            if(KOpen.lt.0) then
              call FeQuestCrwOpen(CrwLastMade,.false.)
              call FeQuestCrwDisable(CrwLastMade)
            else
              if(m.eq.0) then
                if(IDirOld.le.0) then
                  IDir=i
                else
                  IDir=IDirOld
                endif
              endif
              call FeQuestCrwOpen(CrwLastMade,(m.eq.0.and.IDirOld.le.0)
     1                                     .or.i.eq.IDirOld)
              m=m+1
            endif
          enddo
          il=il+1
          tpom=5.
          xpom=tpom+15.
          dpom=60.
          do i=1,3
            call FeQuestEdwMake(idp,tpom,il,xpom,il,' ','L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) nEdwDirectionFirst=EdwLastMade
            tpom=tpom+120.
            xpom=xpom+120.
          enddo
1600      j=mod(IDir-1,4)+1
          k=(IDir-1)/4+1
          nEdw=nEdwDirectionFirst
          if(j.eq.4.and.k.ne.0) then
            do i=1,3
              if(k.eq.1) then
                Veta=char(ichar('u')+i-1)
              else if(k.eq.2) then
                Veta=indices(i)
              else
                Veta=SmbX(i)
              endif
              call FeQuestEdwLabelChange(nEdw,Veta)
              call FeQuestRealEdwOpen(nEdw,XDir(i),.false.,.false.)
              nEdw=nEdw+1
            enddo
          else
            do i=1,3
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          endif
1650      call FeQuestEvent(idp,ichp)
          if(CheckType.eq.EventCrw) then
            IDir=CheckNumber-nCrwDirectionFirst+1
            go to 1600
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 1650
          endif
          if(ichp.eq.0) then
            nCrw=nCrwDirectionFirst
            do i=1,12
              if(CrwLogicQuest(nCrw)) then
                IDir=i
                exit
              endif
              nCrw=nCrw+1
            enddo
            k=mod(IDir-1,4)+1
            if(k.eq.4) then
              nEdw=nEdwDirectionFirst
              do i=1,3
                call FeQuestRealFromEdw(nEdw,XDir(i))
                nEdw=nEdw+1
              enddo
            endif
            call SetRealArrayTo(ViewDirection,3,0.)
            call SetRealArrayTo(XDirection,3,0.)
            PocitejXDir=.true.
            kk=mod(k,3)+1
            if(IDir.le.3) then
              call CopyVek(CellDir(1,k),ViewDirection,3)
              if(CellRefBlock(kk,KRefBlock).gt.0.) then
                call CopyVek(CellDir(1,kk),XDirection,3)
                PocitejXDir=.false.
              else
                kk=mod(kk,3)+1
                if(CellRefBlock(kk,KRefBlock).gt.0.) then
                  call CopyVek(CellDir(1,kk),XDirection,3)
                  PocitejXDir=.false.
                endif
              endif
            else if(IDir.eq.4) then
              do i=1,3
                do j=1,3
                  ViewDirection(j)=ViewDirection(j)+
     1                             XDir(i)*CellDir(j,i)
                enddo
              enddo
            else if(IDir.le.7) then
              do i=1,3
                ViewDirection(i)=CellDirI(k,i)
              enddo
              if(NCell.ge.3) then
                do i=1,3
                  XDirection(i)=CellDirI(kk,i)
                enddo
                PocitejXDir=.false.
              endif
            else if(IDir.eq.8) then
              do i=1,3
                do j=1,3
                  ViewDirection(j)=ViewDirection(j)+
     1                             XDir(i)*CellDirI(i,j)
                enddo
              enddo
            else if(IDir.le.11) then
              ViewDirection(k)=1.
              XDirection(kk)=1.
              PocitejXDir=.false.
            else if(IDir.eq.12) then
              call CopyVek(XDir,ViewDirection,3)
            endif
            if(PocitejXDir) then
              pom=9999.
              do i=1,3
                poma=abs(ViewDirection(i))
                if(poma.lt.pom) then
                  pom=poma
                  ip=i
                endif
              enddo
              call SetRealArrayTo(XDirection,3,0.)
              XDirection(ip)=1.
            endif
            IDirOld=IDir
          endif
          call FeQuestRemove(idp)
          if(ichp.eq.0) then
            go to 5000
          else
            go to 1500
          endif
        else if(CheckNumber.eq.IdSupercell) then
          idp=NextQuestId()
          xqd=180.
          il=3
          call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define reciprocal '//
     1                       'supercell',0,LightGray,0,0)
          il=0
          tpom=30.
          Veta='%a* from 0 to'
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          dpom=40.
          do i=1,3
            Veta(2:2)=SmbABC(i)
            il=il+1
            call FeQuestEudMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1) nEdwFirst=EdwLastMade
            call FeQuestIntEdwOpen(EdwLastMade,GrIndexSuper(i),.false.)
            call FeQuestEudOpen(EdwLastMade,1,6,1,0.,0.,0.)
          enddo
1670      call FeQuestEvent(idp,ichp)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 1670
          endif
          if(ichp.eq.0) then
            nEdw=nEdwFirst
            do i=1,3
              call FeQuestIntFromEdw(nEdw,GrIndexSuper(i))
              nEdw=nEdw+1
            enddo
          endif
          call FeQuestRemove(idp)
          if(ichp.eq.0) then
            go to 1400
          else
            go to 1500
          endif
        else if(CheckNumber.eq.nButt2Cell) then
          i=Id2Cell
          if(CellProjection) then
            CellProjection=.false.
            if(LatticeOn.neqv.LatticeOnOld) then
              j=IdLattice
              if(LatticeOnOld) then
                 Veta=ButtLabels(j)(:idel(ButtLabels(j))-1)//'FF'
              else
                 Veta=ButtLabels(j)(:idel(ButtLabels(j))-1)//'N'
              endif
              call FeQuestButtonLabelChange(nButtLattice,Veta)
              LatticeOn=LatticeOnOld
            endif
            Veta=ButtLabels(i)(:idel(ButtLabels(i))-1)//'N'
            call FeSetTransXo2X(-GrIndexDMax,GrIndexDMax,-GrIndexDMax,
     1                           GrIndexDMax,.true.)
            call FeQuestCrwClose(nCrwLocalize)
            call FeQuestCrwOff(nCrwSpan)
            call FeQuestCrwOff(nCrwZoom)
            if(LocatorType.eq.6) then
              LocatorType=2
              call FeQuestCrwOn(nCrwRotInPlane)
            endif
            nLbl=nLblCell
            do i=1,6
              if(i.le.3) then
                call FeQuestLblChange(nLbl,IndexWinfLabel(i))
              else
                call FeQuestLblOn(nLbl)
              endif
              nLbl=nLbl+1
            enddo
            call DRGrIndexShowCell
            Cislo='cell'
            call FeQuestButtonClose(IdSupercell)
            call FeQuestButtonOpen(Id2dSection,ButtonOff)
          else
            LatticeOnOld=LatticeOn
            if(.not.LatticeOn) then
              LatticeOn=.true.
              j=IdLattice
              Veta=ButtLabels(j)(:idel(ButtLabels(j))-1)//'FF'
              call FeQuestButtonLabelChange(nButtLattice,Veta)
            endif
            CellProjection=.true.
            Veta=ButtLabels(i)(:idel(ButtLabels(i))-1)//'FF'
            call FeSetTransXo2X(-GrCellDMax,GrCellDMax,
     1                          -GrCellDMax,GrCellDMax,.true.)
            if(CrwLogicQuest(nCrwSpan)) then
              LocatorType=1
              call FeQuestCrwOn(nCrwRotGeneral)
            endif
            call FeQuestCrwClose(nCrwSpan)
            call FeQuestCrwOff(nCrwLocalize)
            call FeQuestCrwLock(nCrwZoom)
            nCrw=nCrwCell
            do i=1,3
              call FeQuestCrwClose(nCrw)
              nCrw=nCrw+1
            enddo
            nLbl=nLblCell
            do i=1,6
              if(i.le.3) then
                write(Cislo,'(''q'',i1,''='')') i
                call FeQuestLblChange(nLbl,Cislo)
                call FeWInfWrite(i,' ')
              else
                call FeQuestLblOff(nLbl)
                call FeWInfClose(i)
              endif
              nLbl=nLbl+1
            enddo
            Cislo='q-vector(s)'
            call FeQuestButtonClose(Id2dSection)
            call FeQuestButtonOpen(IdSupercell,ButtonOff)
          endif
          call FeQuestButtonLabelChange(CheckNumber,Veta)
          Veta=ButtLabels(IdReset)(:7)//Cislo(:idel(Cislo))
          call FeQuestButtonLabelChange(nButtReset,Veta)
          go to 1400
        else if(CheckNumber.eq.nButtLattice) then
          i=IdLattice
          if(LatticeOn) then
            LatticeOn=.false.
            Veta=ButtLabels(i)(:idel(ButtLabels(i))-1)//'N'
          else
            LatticeOn=.true.
            Veta=ButtLabels(i)(:idel(ButtLabels(i))-1)//'FF'
          endif
          call FeQuestButtonLabelChange(CheckNumber,Veta)
          go to 1400
        else if(CheckNumber.eq.nButtRefine) then
          call DRGrIndexRefine(ichp)
          if(ichp.eq.0.and..not.CellProjection) call DRGrIndexShowCell
          go to 1400
        else if(CheckNumber.eq.nButtFindCell.or.
     1          CheckNumber.eq.nButtSelectCell) then
          if(CheckNumber.eq.nButtFindCell) then
            call DRGrIndexFindCell(ichp)
          else
            call DRGrIndexFindCellSelect(ichp)
          endif
          if(ichp.eq.0) then
            LatticeOn=.true.
            call CopyMat(UBTripl(1,1,MTripl),UB(1,1,KRefBlock),3)
            call CopyMat(UBITripl(1,1,MTripl),UBI(1,1,KRefBlock),3)
            call CopyVek(CellTripl(1,MTripl),CellRefBlock(1,KRefBlock),
     1                   6)
            do i=1,3
              do j=1,3
                CellDir(j,i)=UBI(i,j,KRefBlock)/
     1                       CellRefBlock(i,KRefBlock)
              enddo
            enddo
            call MatInv(CellDir,CellDirI,pom,3)
            do i=1,3
              XDirection(i)=CellDirI(1,i)
              ViewDirection(i)=CellDirI(3,i)
            enddo
            IDir=7
            if(.not.CellProjection) call DRGrIndexShowCell
            go to 5000
          else
            go to 1500
          endif
        else if(CheckNumber.eq.nButtReset) then
          if(CellProjection) then
            call SetRealArrayTo(QuRefBlock(1,1,KRefBlock),
     1                          3*NDimI(KPhase),0.)
            NDim (KPhase)=3
            NDimI(KPhase)=0
            NDimQ(KPhase)=9
          else
            call SetRealArrayTo(CellRefBlock(1,KRefBlock),6,-1.)
            call DRGrIndexShowCell
            ICell=0
          endif
          go to 1400
        else if(CheckNumber.eq.nButtIndex) then
          Veta=FileIndex(:idel(FileIndex))//'.hkl'
          call FeFileManager('Select output file:',Veta,'*.hkl',0,
     1                       .true.,ich)
          if(ich.ne.0) go to 1500
          if(ExistFile(Veta)) then
            if(.not.FeYesNo(-1.,-1.,'The file "'//Veta(:idel(Veta))//
     1                      '" already exists, rewrite it?',0))
     2        go to 1500
          endif
          call DRGrIndexTryInd
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','unknown')
          Veta='(3i4,2f8.1)'
          write(Veta(2:2),'(i1)') NDim(KPhase)
          do i=1,GrIndexNAll
            if(FlagI(i).gt.0.and.FlagU(i).gt.0)
     1        write(ln,Veta)(iha(j,i),j=1,NDim(KPhase)),Int(i),
     2                       sqrt(abs(Int(i)))
          enddo
          call CloseIfOpened(ln)
          call DRGrIndexIOSmr(1,FileIndex(:idel(FileIndex))//'.smr')
          go to 1500
        else if(CheckNumber.eq.nButtResetSel) then
          FlagU=1
          call FeDeferOutput
          call DRGrIndexPlot
          call FeQuestButtonDisable(nButtResetSel)
          go to 1500
        else if(CheckNumber.eq.nButtOptions) then
          call DRGrIndexOptions
          go to 1500
        else if(CheckNumber.eq.nButtRotateType) then
          if(RotateType.eq.1) then
            Veta='Ro%tate continuously'
            call FeQuestRealFromEdw(nEdwAngleStep,AngleStep)
            call FeQuestEdwDisable(nEdwAngleStep)
          else
            Veta='Ro%tate in steps'
            call FeQuestRealEdwOpen(nEdwAngleStep,AngleStep,.false.,
     1                              .false.)
          endif
          call FeQuestButtonLabelChange(nButtRotateType,Veta)
          RotateType=3-RotateType
          go to 1500
        endif
        irotp=0
        do i=1,3
          if(CheckNumber.eq.nButtPlus(i)) then
            irotp=i
            exit
          else if(CheckNumber.eq.nButtMinus(i)) then
            irotp=-i
            exit
          endif
        enddo
        if(.not.MameData.or.irotp.eq.0) go to 1500
        iarotp=iabs(irotp)
        if(RotateAxes.eq.1.or.RotateAxes.eq.2) then
          xp=0.
          xp(iarotp)=1.
        else if(RotateAxes.eq.3) then
          xp(1:3)=CellDir(1:3,iarotp)
        else if(RotateAxes.eq.4) then
          xp(1:3)=CellDirI(iarotp,1:3)
        endif
        if(RotateAxes.eq.1) then
          xo(1:3)=xp(1:3)
        else
          call MultM(ViewMatrix,xp,xo,3,3,1)
        endif
        if(RotateType.eq.1) then
          call FeQuestRealFromEdw(nEdwAngleStep,AngleStep)
          Angle=AngleStep*sign(1.,float(irotp))*ToRad
          call SetGenRot(Angle,xo,1,RMat)
          call Multm(RMat,ViewMatrix,PMat,3,3,3)
          call CopyMat(PMat,ViewMatrix,3)
          call DRGrIndexPlot
          go to 1500
        else
1700      call FeReleaseOutput
          call FeEvent(1)
          call FeDeferOutput
          if(EventType.ne.EventMouse.and.EventType.ne.0.and.
     1       (EventType.ne.EventSystem.or.EventNumber.ne.JeMimoOkno))
     2      then
            CheckType=EventType
            CheckNumber=EventNumber
            if(CheckType.eq.EventButton) then
              if(irotp.gt.0) then
                nButtP=nButtPlus(iarotp)
              else
                nButtP=nButtMinus(iarotp)
              endif
              call FeQuestButtonOff(nButtP)
              call FeQuestButtonDeactivate(nButtP)
              call FeQuestButtonOn(CheckNumber)
              call FeQuestButtonActivate(CheckNumber)
              EventType=0
              EventNumber=0
            else if(CheckType.eq.EventCrw) then
              if(CheckNumber.ge.nCrwRotateAxes.and.
     1           CheckNumber.le.nCrwRotateAxes+3) then
                RotateAxes=CheckNumber-nCrwRotateAxes+1
                do i=1,3
                  if(RotateAxes.eq.1) then
                    Veta='p'//Smbx(i)
                  else if(RotateAxes.eq.2) then
                    Veta=Smbx(i)
                  else if(RotateAxes.eq.3) then
                    Veta=SmbABC(i)
                  else if(RotateAxes.eq.4) then
                    Veta=SmbABC(i)//'*'
                  endif
                  call FeQuestLblChange(nLblRot(i),Veta)
                enddo
                nCrw=nCrwRotateAxes
                do i=1,4
                  call FeQuestCrwOpen(nCrw,i.eq.RotateAxes)
                  nCrw=nCrw+1
                enddo
                if(RotateAxes.eq.1.or.RotateAxes.eq.2) then
                  xp=0.
                  xp(iarotp)=1.
                else if(RotateAxes.eq.3) then
                  xp(1:3)=CellDir(1:3,iarotp)
                else if(RotateAxes.eq.4) then
                  xp(1:3)=CellDirI(iarotp,1:3)
                endif
                if(RotateAxes.eq.1) then
                  xo(1:3)=xp(1:3)
                else
                  call MultM(ViewMatrix,xp,xo,3,3,1)
                endif
                go to 1750
              endif
              call FeQuestCrwOn(CheckNumber)
              call FeQuestCrwActivate(CheckNumber)
              EventType=0
              EventNumber=0
            else if(CheckType.eq.EventKey.and.
     1              CheckNumber.eq.JeEscape) then
              CheckNumber=JeHome
              go to 1500
            endif
            go to 1510
          endif
1750      Angle=.5*sign(1.,float(irotp))*ToRad
          call SetGenRot(Angle,xo,1,RMat)
          call Multm(RMat,ViewMatrix,PMat,3,3,3)
          call CopyMat(PMat,ViewMatrix,3)
          call DRGrIndexPlot
          go to 1700
        endif
        go to 1500
      else if(CheckType.eq.EventMouse) then
        if(.not.MameData) go to 1500
        if(CheckNumber.eq.JeLeftDown) then
          if(Select) then
            if(YPos.lt.YIntMin.or.YPos.gt.YIntMax) go to 1500
          else
            if(XPos.lt.XMinAcWin.or.XPos.gt.XMaxAcWin.or.
     1         YPos.lt.YMinAcWin.or.YPos.gt.YMaxAcWin) go to 1500
          endif
          XPrev=XPos
          YPrev=YPos
          ZoomActual=Zoom
          MouseActive=.true.
          if(LocatorType.eq.3) then
            call FePlotMode('E')
            no=0
            call MatInv(ViewMatrix,ViewMatrixI,pom,3)
            call DRGrIndexGetQPer
            call CopyVek(CellDir(1,ICell),CellDirOld,3)
            call CopyVek(CellRefBlock(1,KRefBlock),CellParOld,6)
            go to 1500
          else if(LocatorType.eq.4) then
            LevyInt=XPos.gt.XIntMin-2..and.
     1              XPos.lt.XIntMin+2.
            PravyInt=XPos.gt.XIntMax-2..and.
     1               XPos.lt.XIntMax+2.
            go to 1500
          else if(LocatorType.eq.6) then
            if(xf2.gt.xf1) then
              call FeMouseShape(0)
              idp=NextQuestId()
              xqd=500.
              ild=4
              if(NDimI(KPhase).gt.0.and.NDimI(KPhase).lt.3) then
                ild=ild+NDimI(KPhase)+1
              else if(NDimI(KPhase).ge.3) then
                ild=ild+3
              endif
              Veta='Specify the depth:'
              call FeQuestCreate(idp,-1.,-1.,xqd,ild,Veta,0,LightGray,0,
     1                           0)
              XMinP=QuestXMin(idp)+5.
              XMaxP=QuestXMax(idp)-5.
              YMinP=QuestYMin(idp)+50.+float(ild-4)*QuestLineWidth
              YMaxP=YMinP+50.
              call MultM(UB(1,1,KRefBlock),QBas,QBasO,3,3,1)
              call MultM(UB(1,1,KRefBlock),QPer,QPerO,3,3,1)
              ProjNX=0
              ProjNMaxX=1
              nn=0
              xfs=0.
              do 1800i=1,GrIndexNAll
                if(FlagU(i).le.0) go to 1800
                if(GrIndexUseSLim.and.
     1             (Slozky(4,i).lt.GrIndexSMin*2..or.
     2              Slozky(4,i).gt.GrIndexSMax*2.)) then
                  go to 1800
                endif
                if(GrIndexUseILim.and.
     1             (Int(i).lt.GrIndexIMin.or.
     2              Int(i).gt.GrIndexIMax)) then
                  go to 1800
                endif
                call MultM(UBI(1,1,KRefBlock),Slozky(1,i),xp,3,3,1)
                call od0don(xp,xp,GrIndexSuper,3)
                call MultM(UB(1,1,KRefBlock),xp,xq,3,3,1)
                Sum1=0.
                Sum2=0.
                Sum3=0.
                do j=1,3
                  Sum1=Sum1+xq(j)*QPerO(j)
                  Sum2=Sum2+QPerO(j)*QPerO(j)
                  Sum3=Sum3+QBasO(j)*QPerO(j)
                enddo
                xf=(Sum1-Sum3)/Sum2
                D=0.
                do j=1,3
                  D=D+(xq(j)-xf*QPerO(j)-QBasO(j))**2
                enddo
                if(sqrt(D).gt.DiffMax.or.xf.lt.xf1.or.xf.gt.xf2) cycle
                nn=nn+1
                xfs=xfs+xf
                xpom=XMinP+(xf-xf1)/(xf2-xf1)*(XMaxP-XMinP)
                do j=1,ProjNX
                  pom=xpom*EnlargeFactor-ProjX(j)
                  if(pom.le..5.and.
     1               pom.gt.-.5) then
                    ProjMX(j)=ProjMX(j)+1
                    ProjNMaxX=max(ProjNMaxX,ProjMX(j))
                    go to 1800
                  endif
                enddo
                ProjNX=ProjNX+1
                ProjMX(ProjNX)=1
                ProjX(ProjNX)=anint(xpom*EnlargeFactor)
!                ProjX(ProjNX)=anint(xpom)
1800          continue
              call DRGrIndexShowDepth(XMinP,XMaxP,YMinP,YMaxP)
              xf=xfs/float(nn)
              xpom=XMinP+(xf-xf1)/(xf2-xf1)*(XMaxP-XMinP)
              xu(1)=xpom
              xu(2)=xpom
              yu(1)=YMinP
              yu(2)=YMinP+50.
              call FePolyLine(2,xu,yu,Red)
              TakeMouseMove=.true.
              CheckMouse=.true.
              MouseActive=.false.
              write(Veta,102)(xf*QPer(j)+QBas(j),j=1,3)
              il=1
              tpom=xqd*.5
              call FeQuestLblMake(idp,tpom,il,Veta,'C','N')
              nLblQ=LblLastMade
              if(NDimI(KPhase).gt.0) then
                il=4
                xpom=5.
                tpom=xpom+CrwgXd+15.
                Veta='Replace modulation vector #%'
                idl=idel(Veta)
                do i=1,NDimI(KPhase)
                  il=il+1
                  write(Cislo,'(i1)') i
                  Veta=Veta(:idl)//Cislo(1:1)
                  write(Veta(idl+10:),102)
     1              (QuRefBlock(j,i,KRefBlock),j=1,3)
                  call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',
     1                                CrwgXd,CrwgYd,0,1)
                  if(i.eq.1) nCrwModVec=CrwLastMade
                  call FeQuestCrwOpen(CrwLastMade,
     1              i.eq.NDimI(KPhase).and.NDimI(KPhase).eq.3)
                enddo
                if(NDimI(KPhase).lt.3) then
                  il=il+1
                  Veta='Add it as a new modulation vector'
                  call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',
     1                                CrwgXd,CrwgYd,0,1)
                  call FeQuestCrwOpen(CrwLastMade,.true.)
                endif
              endif
1850          call FeQuestEvent(idp,ichp)
              if(CheckType.eq.EventMouse) then
                if(CheckNumber.eq.JeLeftDown) then
                  if(XPos.ge.XMinP.and.XPos.le.XMaxP.and.
     1               YPos.ge.YMinP.and.YPos.le.YMaxP) then
                    XPrev=XPos
                    YPrev=YPos
                    call DRGrIndexShowDepth(XMinP,XMaxP,YMinP,YMaxP)
                    MouseActive=.true.
                    xu(1)=XPos
                    xu(2)=XPos
                    yu(1)=YMinP
                    yu(2)=YMinP+50.
                    call FePlotMode('E')
                    call FePolyLine(2,xu,yu,Cyan)
                  endif
                else if(CheckNumber.eq.JeLeftUp) then
                  if(MouseActive) then
                    call FePlotMode('N')
                    call DRGrIndexShowDepth(XMinP,XMaxP,YMinP,YMaxP)
                    xf=xf1+(XPrev-XMinP)/(XMaxP-XMinP)*(xf2-xf1)
                    xu(1)=XPrev
                    xu(2)=XPrev
                    yu(1)=YMinP
                    yu(2)=YMinP+50.
                    call FePolyLine(2,xu,yu,Red)
                    MouseActive=.false.
                  endif
                else if(CheckNumber.eq.JeMove) then
                  if(MouseActive.and.
     1               XPos.ge.XMinP.and.XPos.le.XMaxP.and.
     1               YPos.ge.YMinP.and.YPos.le.YMaxP) then
                    if(Xpos.ne.XPrev) then
                      call FePolyLine(2,xu,yu,Cyan)
                      xu(1)=XPos
                      xu(2)=XPos
                      call FePolyLine(2,xu,yu,Cyan)
                      XPrev=XPos
                      YPrev=YPos
                      xf=xf1+(XPrev-XMinP)/(XMaxP-XMinP)*(xf2-xf1)
                      write(Veta,102)(xf*QPer(j)+QBas(j),j=1,3)
                      call FePlotMode('N')
                      call FeQuestLblChange(nLblQ,Veta)
                      call FePlotMode('E')
                    endif
                  endif
                endif
                go to 1850
              else if(CheckType.ne.0) then
                call NebylOsetren
                go to 1850
              endif
              if(ichp.eq.0) then
                if(NDimI(KPhase).gt.0) then
                  nCrw=nCrwModVec
                  kam=0
                  do i=1,NDimI(KPhase)
                    if(CrwLogicQuest(nCrw)) then
                      kam=i
                      exit
                    endif
                    nCrw=nCrw+1
                  enddo
                else
                  kam=0
                endif
                if(kam.eq.0) then
                  NDim(KPhase)=NDim(KPhase)+1
                  NDimI(KPhase)=NDimI(KPhase)+1
                  NDimQ(KPhase)=NDim(KPhase)**2
                  kam=NDimI(KPhase)
                endif
                do i=1,3
                  QuRefBlock(i,kam,KRefBlock)=xf*QPer(i)+QBas(i)
                enddo
              endif
              call FeQuestRemove(idp)
              MouseActive=.false.
              call FeMouseShape(1)
              do i=1,3
                call FeWInfWrite(i,' ')
              enddo
              if(ichp.ne.0) then
                go to 1500
              else
                go to 1400
              endif
            endif
          endif
        else if(CheckNumber.eq.JeMove) then
          if(MouseActive) then
            if(LocatorType.le.2) then
              xp(1)=XPos-XPrev
              xp(2)=YPos-YPrev
              xp(3)=0.
              xo(1)=XPos
              xo(2)=YPos
              xo(3)=0.
              Angle=sqrt(VecOrtScal(xp,xp,3))*ToRad*.2
              if(LocatorType.eq.1) then
                call VecOrtNorm(xp,3)
                pom=xp(1)
                xp(1)=-xp(2)
                xp(2)= pom
              else if(LocatorType.eq.2) then
                call FeX2Xf(xo,xq)
                call VecMul(xq,xp,xo)
                if(xo(3).lt.0.) Angle=-Angle
                xp(1)=0.
                xp(2)=0.
                xp(3)=1.
              endif
              call SetGenRot(Angle,xp,1,RMat)
              call Multm(RMat,ViewMatrix,PMat,3,3,3)
              call CopyMat(PMat,ViewMatrix,3)
              XPrev=XPos
              YPrev=YPos
            else if(LocatorType.eq.3) then
              if(no.ge.2) then
                call FePolyLine(2,xuo,yuo,Green)
                if(Index2d) then
                  pom=sqrt((xuo(2)-xuo(1))**2+(yuo(2)-yuo(1))**2)
                  xo(1)=(xuo(2)-xuo(1))/pom
                  xo(2)=(yuo(2)-yuo(1))/pom
                  xu(1)=xuo(2)
                  yu(1)=yuo(2)
                  xu(2)=xuo(2)-7.*xo(1)+4.*xo(2)
                  yu(2)=yuo(2)-7.*xo(2)-4.*xo(1)
                  call FePolyLine(2,xu,yu,Green)
                  xu(2)=xuo(2)-7.*xo(1)-4.*xo(2)
                  yu(2)=yuo(2)-7.*xo(2)+4.*xo(1)
                  call FePolyLine(2,xu,yu,Green)
                endif
              endif
              xp(1)=0.
              xp(2)=0.
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(1)=anint(xo(1))
              yu(1)=anint(xo(2))
              xu(2)=XPos
              yu(2)=YPos
              xuo(1)=xu(1)
              yuo(1)=yu(1)
              xuo(2)=xu(2)
              yuo(2)=yu(2)
              call FePolyLine(2,xuo,yuo,Green)
              if(Index2d) then
                pom=sqrt((xuo(2)-xuo(1))**2+(yuo(2)-yuo(1))**2)
                xo(1)=(xuo(2)-xuo(1))/pom
                xo(2)=(yuo(2)-yuo(1))/pom
                xu(1)=xuo(2)
                yu(1)=yuo(2)
                xu(2)=xuo(2)-7.*xo(1)+4.*xo(2)
                yu(2)=yuo(2)-7.*xo(2)-4.*xo(1)
                call FePolyLine(2,xu,yu,Green)
                xu(2)=xuo(2)-7.*xo(1)-4.*xo(2)
                yu(2)=yuo(2)-7.*xo(2)+4.*xo(1)
                call FePolyLine(2,xu,yu,Green)
                no=2
              else
                if(no.ge.4) call FePolyLine(2,xuo(3),yuo(3),Red)
                pomx=xu(1)-xu(2)
                pomy=yu(1)-yu(2)
                if(pomy.ne.0.) then
                  pom=-pomx/pomy
                  n=2
                  y1=(XCornAcWin(1,1)-xu(2))*pom+yu(2)
                  if(y1.ge.XCornAcWin(2,1).and.y1.le.XCornAcWin(2,2))
     1              then
                    n=n+1
                    xu(n)=XCornAcWin(1,1)
                    yu(n)=y1
                  endif
                  y2=(XCornAcWin(1,4)-xu(2))*pom+yu(2)
                  if(y2.ge.XCornAcWin(2,1).and.y2.le.XCornAcWin(2,2))
     1              then
                    n=n+1
                    xu(n)=XCornAcWin(1,4)
                    yu(n)=y2
                    if(n.eq.4) go to 1900
                  endif
                  x1=(XCornAcWin(2,1)-yu(2))/pom+xu(2)
                  if(x1.ge.XCornAcWin(1,1).and.x1.le.XCornAcWin(1,4))
     1              then
                    n=n+1
                    xu(n)=x1
                    yu(n)=XCornAcWin(2,1)
                    if(n.eq.4) go to 1900
                  endif
                  x2=(XCornAcWin(2,2)-yu(2))/pom+xu(2)
                  if(x2.ge.XCornAcWin(1,1).and.x2.le.XCornAcWin(1,4))
     1              then
                    n=n+1
                    xu(n)=x2
                    yu(n)=XCornAcWin(2,2)
                  endif
                else
                  n=4
                  xu(3)=xu(2)
                  yu(3)=XCornAcWin(2,1)
                  xu(4)=xu(2)
                  yu(4)=XCornAcWin(2,2)
                endif
1900            if(n.ge.4) then
                  call FePolyLine(2,xu(3),yu(3),Red)
                  xuo(3)=xu(3)
                  yuo(3)=yu(3)
                  xuo(4)=xu(4)
                  yuo(4)=yu(4)
                endif
                no=n
              endif
              xo(1)=XPos
              xo(2)=YPos
              xo(3)=0.
              call FeX2Xf(xo,xq)
              CellLength =VecOrtLen(xq,3)
              CellLengthR=1./CellLength
              call Multm(ViewMatrixI,xq,xo,3,3,1)
              if(Index2d) then
                do i=1,3
                  CellDirI(ICell,i)=xo(i)*CellLengthR
                enddo
              else
                do i=1,3
                  CellDir (i,ICell)=xo(i)*CellLengthR
                enddo
                CellLength=CellLengthR
              endif
              write(Cislo,100) CellLengthR
              call Zhusti(Cislo)
              call FePlotMode('N')
              call FeWInfWrite(ICell,Cislo(:idel(Cislo)))
              call FePlotMode('E')
              Zacina=.false.
              do i=1,NCellMax
                if(CellRefBlock(i,KRefBlock).gt.0..and.i.ne.ICell)
     1            then
                  if(Index2d) then
                    pom=0.
                    do j=1,3
                      pom=pom-CellDirI(i,j)*CellDirI(ICell,j)
                    enddo
                  else
                    pom=VecOrtScal(CellDir(1,i),CellDir(1,ICell),3)
                  endif
                  if(Index2d) then
                    k=6
                    ko=3
                  else
                    k=9-i-ICell
                    ko=k
                  endif
                  if(abs(pom).lt.1.) then
                    CellRefBlock(k,KRefBlock)=acos(pom)/ToRad
                  else if(pom.ge.1.) then
                    CellRefBlock(k,KRefBlock)=0.
                  else
                    CellRefBlock(k,KRefBlock)=180.
                  endif
                  write(Cislo,101) CellRefBlock(k,KRefBlock)
                  call Zhusti(Cislo)
                  call FePlotMode('N')
                  call FeWInfWrite(ko,Cislo(:idel(Cislo)))
                  call FePlotMode('E')
                endif
              enddo
              go to 1500
            else if(LocatorType.eq.4) then
              call FePlotMode('E')
              yu(1)=YIntMin
              yu(2)=YIntMax
              if(LevyInt.and.XPos.lt.XIntMax-4..and.
     1                       XIntMin.ge.XMinP) then
                xu(1)=XIntMin
                xu(2)=xu(1)
                call FePolyLine(2,xu,yu,Red)
                xu(1)=min(XPos,XIntMax-4.)
                xu(1)=max(XPos,XMinP)
                xu(2)=xu(1)
                XIntMin=xu(1)
                call FePolyLine(2,xu,yu,Red)
              else if(PravyInt.and.XPos.gt.XIntMin+4.
     1                        .and.XIntMax.le.XMaxP) then
                xu(1)=XIntMax
                xu(2)=xu(1)
                call FePolyLine(2,xu,yu,Red)
                xu(1)=max(XPos,XIntMin+4.)
                xu(1)=min(XPos,XMaxP)
                xu(2)=xu(1)
                XIntMax=xu(1)
                call FePolyLine(2,xu,yu,Red)
              endif
              GrIndexUseILim=.true.
              do i=1,nf+1
                if(float(ProjXI(i))/EnlargeFactor.gt.XIntMin) then
                  GrIndexIMin=FInt(max(i-1,1))*IntMax
                  exit
                endif
              enddo
              do i=nf+1,1,-1
                if(float(ProjXI(i))/EnlargeFactor.lt.XIntMax) then
                  GrIndexIMax=FInt(min(i+1,nf+1))*IntMax
                  exit
                endif
              enddo
              call FePlotMode('N')
              call DRGrIndexPlot
              call DRGrIndexShowInt(XMinP,XMaxP,YMinP,YMaxP,ProjXI,FInt,
     1                              nf)
              xu(1)=XIntMin
              xu(2)=xu(1)
              XIntMin=xu(1)
              yu(1)=YIntMin
              yu(2)=YIntMax
              call FePolyLine(2,xu,yu,Red)
              xu(1)=XIntMax
              xu(2)=xu(1)
              XIntMax=xu(1)
              call FePolyLine(2,xu,yu,Red)
              go to 1500
            else if(LocatorType.eq.5) then
              pom=1.-(YPos-YPrev)*.005
              if(pom.gt.0.) ZoomActual=min(Zoom/pom,10.)
              pom=GrIndexDMax/ZoomActual
              call FeSetTransXo2X(-pom,pom,-pom,pom,.true.)
            endif
            call DRGrIndexPlot
            go to 1500
          else if(LocatorType.eq.6) then
            xq(1)=XPos
            xq(2)=YPos
            xq(3)=0.
            call FeX2Xf(xq,xp)
            call MultM(ViewMatrixI,xp,xq,3,3,1)
            call MultM(UBI(1,1,KRefBlock),xq,QBas,3,3,1)
            do i=1,3
              QBas(i)=QBas(i)+.5*GrIndexSuper(i)
            enddo
            xf1=-9999.
            xf2= 9999.
            do i=1,3
              if(abs(QPer(i)).gt..001) then
                pom=1./QPer(i)
                xf1p=-QBas(i)*pom
                if(QPer(i).gt.0.) then
                  xf2p=xf1p+pom*GrIndexSuper(i)
                else
                  xf2p=xf1p
                  xf1p=xf1p+pom*GrIndexSuper(i)
                endif
                xf1=max(xf1p,xf1)
                xf2=min(xf2p,xf2)
              else
                if(QBas(i).lt.0..or.QBas(i).gt.GrIndexSuper(i)) then
                  xf1= 1.
                  xf2=-1.
                  exit
                endif
              endif
            enddo
            if(xf2.gt.xf1) then
              call FeMouseShape(1)
              do i=1,3
                write(Cislo,'(f15.3)') QBas(i)
                call Zhusti(Cislo)
                call FeWInfWrite(i,Cislo(:idel(Cislo)))
              enddo
            else
              call FeMouseShape(0)
              do i=1,3
                call FeWInfWrite(i,' ')
              enddo
            endif
          endif
        else if(CheckNumber.eq.JeLeftUp) then
          if(.not.MouseActive) go to 1500
          MouseActive=.false.
          if(LocatorType.eq.3) then
            call FePlotMode('N')
            if(Zacina) go to 1500
            Nebrat=.false.
            do i=1,NCellMax
              if(CellRefBlock(i,KRefBlock).gt.0..and.i.ne.ICell) then
                if(Index2d) then
                  k=6
                else
                  k=9-i-ICell
                endif
                if(CellRefBlock(k,KRefBlock).lt.15.) then
                  NInfo=1
                  if(Index2d) then
                    TextInfo(1)='Angle seems to be too small.'
                  else
                    TextInfo(1)='Angle "'//LCell(k)(:idel(LCell(k)))//
     1                          '" seems to be too small.'
                  endif
                  if(.not.FeYesNoHeader(-1.,-1.,
     1                                  'Do you want to accept it '//
     2                                  'anyhow?',0)) then
                    CellRefBlock(ICell,KRefBlock)=-1.
                    Nebrat=.true.
                  endif
                endif
                if(Nebrat) CellRefBlock(i,KRefBlock)=-1.
              endif
            enddo
            if(Nebrat) then
              NCell=NCell-1
              NCellOld=NCellOld-1
              call FeQuestButtonDisable(nButtLattice)
              call FeQuestButtonDisable(nButtRefine)
              call FeQuestButtonDisable(nButtIndex)
              call FeQuestButtonDisable(nButt2Cell)
              ichp=1
              idp=0
              go to 2100
            endif
            idp=NextQuestId()
            if(Index2d) then
              Veta='Define the reflection order:'
              xqd=200.
            else
              Veta='Define the order of interplanar distance:'
              xqd=260.
            endif
            il=1
            call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
            tpom=5.
            xpom=tpom+FeTxLength(Veta)+5.
            dpom=35.
            call FeQuestEudMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            nEdwOrder=EdwLastMade
            call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
            call FeQuestEudOpen(EdwLastMade,1,200,1,0.,0.,0.)
2000        call FeQuestEvent(idp,ichp)
            if(CheckType.ne.0) then
              call NebylOsetren
              go to 2000
            endif
2100        if(ichp.eq.0) then
              call FeQuestIntFromEdw(nEdwOrder,NOrder)
              if(Index2d) then
                CellRefBlock(ICell,KRefBlock)=float(NOrder)/CellLength
              else
                CellRefBlock(ICell,KRefBlock)=CellLength*float(NOrder)
              endif
              write(Cislo,100) CellRefBlock(ICell,KRefBlock)
              call Zhusti(Cislo)
              call FeWInfWrite(ICell,Cislo(:idel(Cislo)))
              ICellOld=-1
            else
              call CopyVek(CellDirOld,CellDir(1,ICell),3)
              call CopyVek(CellParOld,CellRefBlock(1,KRefBlock),6)
              call DRGrIndexShowCell
            endif
            if(idp.ne.0) call FeQuestRemove(idp)
            Zacina=.true.
            call FeDeferOutput
            call DRGrIndexPlot
            go to 3000
          else if(LocatorType.eq.4) then
            call FePlotMode('N')
            go to 1500
          else if(LocatorType.eq.5) then
            Zoom=ZoomActual
          endif
        endif
      else if(CheckType.eq.EventCrw) then
        if(CheckNumber.ge.nCrwRotGeneral.and.
     1     CheckNumber.le.nCrwLocalize) then
          LocatorType=CheckNumber-nCrwRotGeneral+1
          call FeMouseShape(0)
          if(CheckNumber.eq.nCrwRotGeneral.or.
     1       CheckNumber.eq.nCrwRotInPlane) then
            if(EdwStateQuest(nEdwAngleStep).ne.EdwOpened) then
              nCrw=nCrwRotateAxes
              do i=1,4
                call FeQuestCrwOpen(nCrw,i.eq.RotateAxes)
                nCrw=nCrw+1
              enddo
              do i=1,3
                call FeQuestButtonOpen(nButtMinus(i),ButtonOff)
                call FeQuestLblOn(nLblRot(i))
                call FeQuestButtonOpen(nButtPlus(i),ButtonOff)
              enddo
              call FeQuestButtonOpen(nButtRotateType,ButtonOff)
              call FeQuestRealEdwOpen(nEdwAngleStep,AngleStep,.false.,
     1                                .false.)
            endif
          else
            if(EdwStateQuest(nEdwAngleStep).eq.EdwOpened) then
              nCrw=nCrwRotateAxes
              do i=1,4
                call FeQuestCrwDisable(nCrw)
                nCrw=nCrw+1
              enddo
              do i=1,3
                call FeQuestButtonDisable(nButtMinus(i))
                call FeQuestLblDisable(nLblRot(i))
                call FeQuestButtonDisable(nButtPlus(i))
              enddo
              call FeQuestButtonDisable(nButtRotateType)
              call FeQuestEdwDisable(nEdwAngleStep)
            endif
          endif
          if(LocatorType.ne.4.and.Select) then
            call FeFillRectangle(xf1,xf2,XCornAcWin(2,1)-80.,
     1                           XCornAcWin(2,1)-20.,4,0,0,Black)
            Select=.false.
            if(allocated(ipor)) deallocate(ipor,FInt,ProjXI)
          endif
          if(CrwLogicQuest(nCrwSpan)) then
            ICellOld=0
            Zacina=.true.
            if(NCell.lt.3) then
              go to 3000
            else
              ICell=max(ICell,1)
              go to 3100
            endif
          else if(CrwLogicQuest(nCrwSelect)) then
            LocatorTypeOld=LocatorType
            Select=.true.
            XMinP=XMinGrWin+10.
            XMaxP=XCornAcWin(1,1)-10.
            YMinP=XCornAcWin(2,1)-80.
            YMaxP=XCornAcWin(2,1)-20.
            nf=nint((XMaxP-XMinP)*EnlargeFactor)
            if(allocated(ipor)) deallocate(ipor,FInt,ProjXI)
            allocate(ipor(GrIndexNAll),FInt(nf+1),ProjXI(nf+1))
            call rindexx(GrIndexNAll,int,ipor)
            IntMin=int(ipor(1))
            IntMax=int(ipor(GrIndexNAll))
            IntDelta=(IntMax-IntMin)/float(nf)
            FInt(1)=0.
            pom=0.
            pomd=float(GrIndexNAll)/float(nf)
            ProjXI(1)=nint(XMinP*EnlargeFactor)
            do i=2,nf+1
              pom=pom+pomd
              ProjXI(i)=nint((float(i-1)*(XMaxP-XMinP)/float(nf)+XMinP)*
     1                  EnlargeFactor)
              FInt(i)=int(ipor(nint(pom)))/IntMax
            enddo
            call DRGrIndexShowInt(XMinP,XMaxP,YMinP,YMaxP,ProjXI,FInt,
     1                            nf)
            if(GrIndexUseILim) then
              Diff1=99999.
              Diff2=99999.
              do i=1,nf+1
                pom=abs(GrIndexIMin-FInt(i)*IntMax)
                if(pom.lt.Diff1) then
                  xup1=anint(ProjXI(i)/EnlargeFactor)
                  Diff1=pom
                endif
                pom=abs(GrIndexIMax-FInt(i)*IntMax)
                if(pom.lt.Diff2) then
                  xup2=anint(ProjXI(i)/EnlargeFactor)
                  Diff2=pom
                endif
              enddo
            else
              xup1=anint(XMinP)
              xup2=anint(float(ProjXI(nf+1))/EnlargeFactor)
            endif
            xu(1)=xup1
            xu(2)=xup1
            XIntMin=xu(1)
            yu(1)=YMinP
            yu(2)=YMinP+60.
            YIntMin=yu(1)
            YIntMax=yu(2)
            call FePolyLine(2,xu,yu,Red)
            xu(1)=xup2
            xu(2)=xup2
            XIntMax=xup2
            call FePolyLine(2,xu,yu,Red)
          else if(CrwLogicQuest(nCrwLocalize)) then
            call MatInv(ViewMatrix,ViewMatrixI,pom,3)
            call DRGrIndexGetQPer
          endif
          if(.not.CellProjection) then
            nCrw=nCrwCell
            do i=1,3
              call FeQuestCrwClose(nCrw)
              nCrw=nCrw+1
            enddo
          endif
          go to 1500
        else if(CheckNumber.ge.nCrwCell.and.CheckNumber.le.nCrwCell+2)
     1    then
          nCrw=nCrwCell
          NCrwCellNew=0
          do i=1,NCellMax
            if(CrwLogicQuest(nCrw)) then
              ICell=i
              nCrwCellNew=nCrw
              exit
            endif
            nCrw=nCrw+1
          enddo
          if(ICell.ne.ICellOld) go to 1430
        else if(CheckNumber.ge.nCrwRotateAxes.and.
     1          CheckNumber.le.nCrwRotateAxes+3) then
          RotateAxes=CheckNumber-nCrwRotateAxes+1
          do i=1,3
            if(RotateAxes.eq.1) then
              Veta='p'//Smbx(i)
            else if(RotateAxes.eq.2) then
              Veta=Smbx(i)
            else if(RotateAxes.eq.3) then
              Veta=SmbABC(i)
            else if(RotateAxes.eq.4) then
              Veta=SmbABC(i)//'*'
            endif
            call FeQuestLblChange(nLblRot(i),Veta)
          enddo
        endif
        go to 1500
      endif
      go to 1500
3000  if(NCell.lt.NCellMax) then
        ICell=1
        do i=1,NCellMax
          if(CellRefBlock(i,KRefBlock).lt.0.) then
            ICell=i
            exit
          endif
        enddo
      else
        ICell=mod(ICell,NCellMax)+1
      endif
3100  nCrwCellNew=ICell+nCrwCell-1
      go to 1430
5000  call CopyVek(ViewDirection,ViewMatrixI(1,3),3)
      call VecOrtNorm(ViewMatrixI(1,3),3)
      call CopyVek(XDirection,ViewMatrixI(1,1),3)
      call VecOrtNorm(ViewMatrixI(1,1),3)
      pom=-VecOrtScal(ViewMatrixI(1,1),ViewMatrixI(1,3),3)
      do i=1,3
        ViewMatrixI(i,1)=ViewMatrixI(i,1)+pom*ViewMatrixI(i,3)
      enddo
      call VecOrtNorm(ViewMatrixI(1,1),3)
      call VecMul(ViewMatrixI(1,3),ViewMatrixI(1,1),ViewMatrixI(1,2))
      call MatInv(ViewMatrixI,ViewMatrix,pom,3)
      call DRGrIndexGetQPer
      go to 1400
9000  if(allocated(Slozky)) deallocate(Slozky,ProjX,ProjMX,ProjY,
     1                                 ProjMY,Int,FlagS,FlagI,FlagU,
     2                                 ha,iha)
      if(allocated(ipor)) deallocate(ipor,FInt,ProjXI)
      if(FileIndex.ne.' ') then
        if(Index2d)
     1    call CopyVek(CellParSave,CellRefBlock(1,KRefBlock),6)
        call DRGrIndexIOSmr(1,FileIndex(:idel(FileIndex))//'.smr')
      endif
      DiffAxe  =DiffAxeIn
      DiffAngle=DiffAngleIn
      NDim(KPhase)=NDimIn
      NDimI(KPhase)=NDimIn-3
      NDimQ(KPhase)=NDimIn**2
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      return
100   format(f7.3)
101   format(f7.2)
102   format('q1=',f8.4,'   q2=',f8.4,'    q3=',f8.4)
104   format(4e21.11e4,i11,i6)
      end
