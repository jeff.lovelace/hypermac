      subroutine DRGrIndexGetQPer
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3),xq(3)
      call SetRealArrayTo(xq,3,0.)
      call FeX2Xf(xq,xp)
      call MultM(ViewMatrixI,xp,xq,3,3,1)
      call MultM(UBI(1,1,KRefBlock),xq,QPer,3,3,1)
      call SetRealArrayTo(xq,3,0.)
      xq(3)=1.
      call FeX2Xf(xq,xp)
      call MultM(ViewMatrixI,xp,xq,3,3,1)
      call MultM(UBI(1,1,KRefBlock),xq,xp,3,3,1)
      pom=0.
      do i=1,3
        QPer(i)=xp(i)-QPer(i)
        pom=max(pom,abs(QPer(i)))
      enddo
      if(pom.ne.0.) then
        do i=1,3
          QPer(i)=QPer(i)/pom
        enddo
      endif
      return
      end
