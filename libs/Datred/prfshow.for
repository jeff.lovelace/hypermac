      subroutine PrfShow
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      character*8 jmena(11)
      data jmena/'%Quit','%Print','%Save','%Next','%Back','%Go to',
     1           '%Indices','Poin%ts','%Cut','%Reset','%Options'/
      StBPBo(1)=' '
      StBPBo(2)=' '
      NNew=0
      IntFromProfile=.true.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,40.,YBottomMargin,0.)
      call FeBottomInfo('#prazdno#')
      call FeMakeAcWin(25.,10.,15.,14.)
      xbpb(1)=XMinAcWin
      xbpb(2)=XMaxAcWin
      xmsg=XCenAcWin
      ybpb=YBottomText
      call UnitMat(F2O,3)
      call OpenFile(95,fln(:ifln)//'.m95','formatted','old')
      if(ErrFlag.ne.0) go to 9500
      xpom=XMaxGrWin+4.
      dpom=XMaxBasWin-XMaxGrWin-8.
      ypom=YMaxGrWin-30.
      do i=1,11
        call FeQuestAbsButtonMake(id,xpom,ypom,dpom,ButYd,Jmena(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.3.or.i.eq.7.or.i.eq.10) then
          ypom=ypom-15.
        else
          ypom=ypom-10.
        endif
      enddo
      call FeClearGrWin
      call PrfOptions
      nref=0
      ie=4
2000  ich=0
      nrefo=nref
      if(ie.eq.1) then
        call FeQuestButtonOff(1)
        call FeQuestRemove(id)
        go to 9000
      else if(ie.eq.4) then
        if(nref.ge.nref95(KRefBlock)) then
          ie=-1
          go to 3000
        endif
        nref=nref+1
      else if(ie.eq.5) then
        if(nref.le.1) then
          ie=-1
          go to 3000
        endif
        nref=nref-1
      else if(ie.eq.6) then
        call GoToRef(nref,1,nref95(KRefBlock),ich)
      else if(ie.eq.7) then
        call GoToIndices(nref,ich)
      else if(ie.eq.11) then
        call PrfOptions
      endif
      if(ich.ne.0) then
        call FeQuestButtonOff(ie)
        nref=nrefo
        ie=4
      endif
      if(nref.ge.nref95(KRefBlock)) then
        call FeQuestButtonDisable(4)
      else
        call FeQuestButtonOff(4)
      endif
      if(nref.le.1) then
        call FeQuestButtonDisable(5)
      else
        call FeQuestButtonOff(5)
      endif
3000  call EditProfile(nref,ie)
      go to 2000
9000  pom=0.
      do 9100i=1,2
        if(StBPBo(i).eq.' ') go to 9100
        call FeRewriteString(0,xbpb(i),ybpb,StBPBo(i),' ','C',LightGray,
     1                       Black)
9100  continue
      ich=0
      go to 9999
9500  call FeChybne(-1.,-1.,'the file '//'"'//fln(:ifln)//'.m95'//'"',
     1              'cannot be open.',SeriousError)
      ich=1
9999  call CloseIfOpened(95)
      if(ich.eq.0.and.NNew.gt.0) call DRUpdateM95
      return
      end
