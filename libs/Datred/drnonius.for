      subroutine DRNonius
      include 'fepc.cmn'
      include 'basic.cmn'
      integer FeMenu,WhatToDo
      character*30 Men(2)
      data men/'Indexation procedure',
     1         'Cell refinement'/
1100  WhatToDo=FeMenu(-1.,-1.,men,1,2,1,0)
      if(WhatToDo.eq.1) then
        call DRKumaIndex(3)
      else if(WhatToDo.eq.2) then
        call DRKumaRefine(3)
      endif
      if(WhatToDo.gt.0) go to 1100
      return
      end
