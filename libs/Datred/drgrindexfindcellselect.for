      subroutine DRGrIndexFindCellSelect(ich)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer UseTabsIn,UseTabsCell,UseTabsMat1,UseTabsMat2,UseTabsSel,
     1        SbwLnQuest,SbwItemPointerQuest,nLblCell(2),nLblUB(2),
     2        PorAct(2),nLblFoundCell(2),nLblDet(2),nEdwCell(2),
     3        nButtCell(2),nLblTrans(2),NIndSelAct(2),nLblIndSel(2)
      real CellAct(6,2),UBAct(3,3,2),tw(3,3),twi(3,3),VolAct(2)
      character*256 t256
      character*80 Veta
      UseTabsIn=UseTabs
      call FeTabsReset(UseTabs)
      t256=' '
      ich=0
      id=NextQuestId()
      xqd=500.
      il=16
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,-1)
      tpom=5.
      tpom1=xqd*.5-100.
      tpom2=tpom+FeTxLength('Cell parameters:')+10.
      tpom3=tpom+FeTxLength('Orientation matrix:')+10.
      UseTabsCell=NextTabs()
      xpom=5.
      do i=1,6
        call FeTabsAdd(xpom,UseTabsCell,IdRightTab,' ')
        xpom=xpom+40.
        if(i.eq.3) xpom=xpom+10.
      enddo
      xpom=xpom+10.
      call FeTabsAdd(xpom,UseTabsCell,IdRightTab,' ')
      xpom=xpom+FeTxLength('Volume:')+10.
      call FeTabsAdd(xpom,UseTabsCell,IdRightTab,' ')
      UseTabsMat1=NextTabs()
      xpom=15.
      do i=1,3
        call FeTabsAdd(xpom,UseTabsMat1,IdCharTab,'.')
        xpom=xpom+55.
      enddo
      UseTabsMat2=NextTabs()
      xpom=15.
      do i=1,4
        call FeTabsAdd(xpom,UseTabsMat2,IdCharTab,'.')
        xpom=xpom+40.
      enddo
      UseTabsSel=NextTabs()
      xpom=10.
      call FeTabsAdd(xpom,UseTabsSel,IdLeftTab,' ')
      xpom=xpom+55.
      call FeTabsAdd(xpom,UseTabsSel,IdLeftTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabsSel,IdCharTab,'/')
      xpom=xpom+30.
      do i=1,3
        xpom=xpom+45.
        call FeTabsAdd(xpom,UseTabsSel,IdLeftTab,' ')
      enddo
      do i=1,3
        xpom=xpom+50.
        call FeTabsAdd(xpom,UseTabsSel,IdLeftTab,' ')
      enddo
      il=0
      do i=1,2
        il=il-10
        Veta='Cell#'
        xpom=tpom1+FeTxLength(Veta)+5.
        dpom=25.
        call FeQuestEudMake(id,tpom1,il,xpom,il-1,Veta,'L',dpom,EdwYd,1)
        nEdwCell(i)=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,i,.false.)
        call FeQuestEudOpen(EdwLastMade,1,NTripl,1,0.,0.,0.)
        Veta='From the list'
        tpomp=xpom+dpom+25.
        dpom=FeTxLength(Veta)+10.
        call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
        nButtCell(i)=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        UseTabs=UseTabsCell
        il=il-10
        call FeQuestLblMake(id,tpom,il,'Cell parameters:','L','N')
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        nLblCell(i)=LblLastMade
        il=il-6
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        nLblIndSel(i)=LblLastMade
        UseTabs=UseTabsMat1
        il=il-6
        call FeQuestLblMake(id,tpom,il,'Orientation matrix:','L','N')
        do j=1,3
          call FeQuestLblMake(id,tpom3,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          if(j.eq.1) nLblUB(i)=LblLastMade
          il=il-6
        enddo
        UseTabs=UseTabsMat2
        il=il-4
        call FeQuestLinkaMake(id,il)
      enddo
      il=il-10
      tpom2=5.
      tpom1=tpom2+45.
      tpom4=xqd*.5+5.
      tpom3=tpom4+45.
      call FeQuestLblMake(id,tpom1,il,' ','L','N')
      nLblTrans(1)=LblLastMade
      call FeQuestLblMake(id,tpom3,il,' ','L','N')
      nLblTrans(2)=LblLastMade
      UseTabs=UseTabsMat1
      il=il-6
      do j=1,3
        call FeQuestLblMake(id,tpom2,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(j.eq.1) nLblTw=LblLastMade
        call FeQuestLblMake(id,tpom4,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(j.eq.1) nLblTwi=LblLastMade
        il=il-6
      enddo
      Veta='Determinant:'
      call FeQuestLblMake(id,tpom2+40.,il,Veta,'L','N')
      nLblDet(1)=LblLastMade
      call FeQuestLblMake(id,tpom4+40.,il,Veta,'L','N')
      nLblDet(2)=LblLastMade
      IAct=0
      do i=1,2
        if(NTripl.eq.1.and.i.eq.2) then
          ii=1
        else
          ii=i
        endif
        PorAct(i)=ii
        j=PorTripl(ii)
        call CopyVek(UBTripl(1,1,j),UBAct(1,1,i),9)
        VolAct(i)=VTripl(j)
        CellAct(1:6,i)=CellTripl(1:6,j)
        NIndSelAct(i)=NIndSelTripl(j)
      enddo
      il=il-15.
      dpom=150.
      write(Cislo,FormI15) PorAct(1)
      call Zhusti(Cislo)
      Veta='%Continue with cell#'//Cislo(:idel(Cislo))
      xpom=xqd*.5-dpom*1.5-10.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='%Don''t use any of found cells'
          nButtCell1=ButtonLastMade
        else if(i.eq.2) then
          write(Cislo,FormI15) PorAct(2)
          call Zhusti(Cislo)
          Veta='Co%ntinue with cell#'//Cislo(:idel(Cislo))
          nButtQuit=ButtonLastMade
        else if(i.eq.3) then
          nButtCell2=ButtonLastMade
        endif
        xpom=xpom+dpom+10.
      enddo
1400  if(IAct.le.0) then
        ip=1
        ik=2
      else
        ip=IAct
        ik=IAct
      endif
      do i=ip,ik
        write(Veta,100)(Tabulator,CellAct(j,i),j=1,6),Tabulator//
     1                  'Volume:',Tabulator,VolAct(i)
        call Zhusti(Veta)
        call FeQuestLblChange(nLblCell(i),Veta)
        write(Veta,103) NIndSelAct(i),GrIndexNSel
        call Zhusti(Veta)
        write(Cislo,'(f5.1)')
     1    float(NIndSelAct(i))/float(GrIndexNSel)*100.
        call ZdrcniCisla(Cislo,1)
        Veta='Indexed: '//Veta(:idel(Veta))//' ~ '//
     1       Cislo(:idel(Cislo))//'%'
        call FeQuestLblChange(nLblIndSel(i),Veta)
        nLbl=nLblUB(i)
        do j=1,3
          write(Veta,102)(Tabulator,UBAct(j,k,i),k=1,3)
          call Zhusti(Veta)
          call FeQuestLblChange(nLbl,Veta)
          nLbl=nLbl+1
        enddo
      enddo
      call MatInv(UBAct(1,1,2),Tw,pom,3)
      call MultM(Tw,UBAct(1,1,1),Twi,3,3,3)
      call TrMat(Twi,Tw,3,3)
      call MatInv(Tw,Twi,pom,3)
      Veta='Determinant:'
      write(Cislo,'(f15.3)') pom
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//' '//Cislo
      call FeQuestLblChange(nLblDet(1),Veta)
      Veta='Determinant:'
      write(Cislo,'(f15.3)') 1./pom
      call Zhusti(Cislo)
      Veta=Veta(:idel(Veta))//' '//Cislo
      call FeQuestLblChange(nLblDet(2),Veta)
      nLbl=nLblTw
      do i=1,2
        Veta='Cell#'
        write(Cislo,FormI15) PorAct(i)
        call Zhusti(Cislo)
        Veta='Cell#'//Cislo(:idel(Cislo))//' -> Cell#'
        write(Cislo,FormI15) PorAct(3-i)
        call Zhusti(Cislo)
        Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        call FeQuestLblChange(nLblTrans(i),Veta)
      enddo
      do j=1,3
        write(Veta,102)(Tabulator,Tw(j,k),k=1,3)
        call Zhusti(Veta)
        call FeQuestLblChange(nLbl,Veta)
        nLbl=nLbl+1
        write(Veta,102)(Tabulator,Twi(j,k),k=1,3)
        call Zhusti(Veta)
        call FeQuestLblChange(nLbl,Veta)
        nLbl=nLbl+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtCell(1).or.CheckNumber.eq.nButtCell(2)))
     2  then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_found_cells.tmp','formatted',
     1                'unknown')
        do i=1,NTripl
          j=PorTripl(i)
          write(Veta,'(a1,i5,a1,f10.2)') Tabulator,i,Tabulator,VTripl(j)
          call Zhusti(Veta)
          write(t256,103) NIndSelTripl(j),GrIndexNSel
          call Zhusti(t256)
          Veta=Veta(:idel(Veta))//Tabulator//t256(:idel(t256))
          write(t256,'(3(a1,f8.3),3(a1,f8.2))')
     1     (Tabulator,CellTripl(k,j),k=1,6)
          Veta=Veta(:idel(Veta))//t256(:idel(t256))
          write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        idp=NextQuestId()
        xqdp=530.
        if(CheckNumber.eq.nButtCell(1)) then
          IAct=1
        else
          IAct=2
        endif
        il=max(NTripl-2,5)
        Veta='Select cell from the list:'
        call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
        il=1
        tpom=20.
        call FeQuestLblMake(idp,tpom,il,'No.','L','N')
        tpom=52.
        call FeQuestLblMake(idp,tpom,il,'Volume','L','N')
        tpom=120.
        call FeQuestLblMake(idp,tpom,il,'Indexed','L','N')
        tpom=280.
        call FeQuestLblMake(idp,tpom,il,'Cell parameters','L','N')
        xpom=15.
        dpom=xqdp-30.-SbwPruhXd
        il=max(NTripl-3,4)
        call FeQuestSbwMake(idp,xpom,il+1,dpom,il,1,CutTextFromLeft,
     1                      SbwVertical)
        call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
        il=il+1
        nSbw=SbwLastMade
        UseTabs=UseTabsSel
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_found_cells.tmp')
2500    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventSbwDoubleClick) then
          ich=0
          go to 2800
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
2800    if(ich.eq.0) then
          j=SbwItemPointerQuest(nSbw)
          i=PorTripl(j)
          call CopyVek(UBTripl(1,1,i),UBAct(1,1,IAct),9)
          VolAct(IAct)=VTripl(i)
          PorAct(IAct)=j
          CellAct(1:6,IAct)=CellTripl(1:6,i)
          NIndSelAct(IAct)=NIndSelTripl(i)
        endif
        call FeQuestRemove(idp)
        call DeleteFile(fln(:ifln)//'_found_cells.tmp')
        if(ich.eq.0) then
          call FeQuestIntEdwOpen(nEdwCell(IAct),j,.false.)
          write(Cislo,FormI15) j
          call Zhusti(Cislo)
          if(IAct.eq.1) then
            Veta='%Continue with cell#'//Cislo(:idel(Cislo))
            call FeQuestButtonLabelChange(nButtCell1,Veta)
          else
            Veta='Co%ntinue with cell#'//Cislo(:idel(Cislo))
            call FeQuestButtonLabelChange(nButtCell2,Veta)
          endif
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwCell(1).or.
     2         CheckNumber.eq.nEdwCell(2))) then
        if(CheckNumber.eq.nEdwCell(1)) then
          IAct=1
        else
          IAct=2
        endif
        call FeQuestIntFromEdw(CheckNumber,j)
        i=PorTripl(j)
        CellAct(1:6,IAct)=CellTripl(1:6,i)
        call CopyVek(UBTripl(1,1,i),UBAct(1,1,IAct),9)
        VolAct(IAct)=VTripl(i)
        PorAct(IAct)=j
        NIndSelAct(IAct)=NIndSelTripl(i)
        write(Cislo,FormI15) j
        call Zhusti(Cislo)
        if(IAct.eq.1) then
          Veta='%Continue with cell#'//Cislo(:idel(Cislo))
          call FeQuestButtonLabelChange(nButtCell1,Veta)
        else
          Veta='Co%ntinue with cell#'//Cislo(:idel(Cislo))
          call FeQuestButtonLabelChange(nButtCell2,Veta)
        endif
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1  then
        ich=1
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCell1)
     1  then
        ich=0
        MTripl=PorTripl(PorAct(1))
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCell2)
     1  then
        ich=0
        MTripl=PorTripl(PorAct(2))
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      call FeTabsReset(UseTabsCell)
      call FeTabsReset(UseTabsMat1)
      call FeTabsReset(UseTabsMat2)
      call FeTabsReset(UseTabsSel)
      UseTabs=UseTabsIn
      return
100   format(3(a1,f8.3),3(a1,f8.2),a,a1,f10.2)
101   format(3(a1,f8.4))
102   format(3(a1,f10.6))
103   format(i6,'/',i6)
104   format(3(a1,f8.4),a1,'=>',3(a1,f8.4))
      end
