      function AngKuma(s,c)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      ss=s
      cc=c
      if(abs(s*StepKuma(1)).lt..5) then
        ss=0.
        cc=sign(1.,cc)
      endif
      if(cc*StepKuma(1).gt..5) then
        AngKuma=atan(ss/cc)
      else if(cc*StepKuma(1).lt.-.5) then
        AngKuma=atan(ss/cc)+Pi*sign(1.,ss)
      else
        AngKuma=Pi*.5*sign(1.,ss)
      endif
      return
      end
