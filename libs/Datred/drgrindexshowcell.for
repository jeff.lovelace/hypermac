      subroutine DRGrIndexShowCell
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      if(NCellMax.eq.2) then
        ik=3
      else
        ik=6
      endif
      do i=1,ik
        if(CellRefBlock(i,KRefBlock).gt.0) then
          if(i.le.NCellMax) then
            write(Cislo,100) CellRefBlock(i,KRefBlock)
          else
            if(ik.eq.3) then
              k=6
            else
              k=i
            endif
            write(Cislo,101) CellRefBlock(k,KRefBlock)
          endif
          call Zhusti(Cislo)
        else
          Cislo=' '
        endif
        call FeWInfWrite(i,Cislo)
      enddo
100   format(f7.3)
101   format(f7.2)
      return
      end
