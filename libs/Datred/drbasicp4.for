      subroutine DRBasicP4
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension idn(10),rmp(9)
      character*8  nazev,id(10)
      character*256 t256
      character*80 t80
      logical UzJiMa
      integer ifr(20)
      data id/'cell','ort1','ort2','ort3','source','qvec','latvec',
     1        'face','chem','cellsd'/,idn/5*0,3*-1,2*0/
      data rmp/1.,0.,1.,1.,0.,-1.,0.,1.,0./
      PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
      call SetRealArrayTo(SenseOfAngle,4,1.)
      SenseOfAngle(1)=-1.
      PocitatDirCos=.true.
      DirCosFromPsi=.false.
      KdoToVola=0
      go to 900
      entry DRBasicBrukerCCD
      ln=0
      NDim95P=3
      KdoToVola=1
      WorkString=SourceFileRefBlock(KRefBlock)
      call OpenFile(72,WorkString(:idel(WorkString)-4)//'.p4p',
     1              'formatted','old')
      if(ErrFlag.ne.0) go to 8500
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
900   ln=71+KdoToVola
      UzJiMa=.false.
      rewind ln
      NLattVec(1)=0
      NFaces(KRefBlock)=0
!      if(DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
!        NRuns(KRefBlock)=1
!        RunMeasureTime(1,KRefBlock)=1.
!      else
        NRuns(KRefBlock)=0
!      endif
      NDim95P=3
1000  read(ln,FormA80,end=8000) t80
      k=0
      call kus(t80,k,nazev)
      call mala(Nazev)
      if(nazev.eq.'data') go to 8000
      i=islovo(nazev,id,10)
      if(i.le.0) go to 1000
      idn(i)=1
      if(i.eq.1) then
        call StToReal(t80,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
        if(ich.ne.0) go to 8100
        UzJiMa=.true.
      else if(i.eq.10) then
        call StToReal(t80,k,CellRefBlockSU(1,KRefBlock),6,.false.,ich)
        if(ich.ne.0) go to 8100
      else if(i.gt.1.and.i.le.4) then
        do j=1,3
          call StToReal(t80,k,ub(i-1,j,KRefBlock),1,.false.,ich)
          if(ich.ne.0) go to 8100
          if(i.le.3) ub(i-1,j,KRefBlock)=-ub(i-1,j,KRefBlock)
        enddo
      else if(i.eq.5) then
        call kus(t80,k,Cislo)
        call StToReal(t80,k,LamAveRefBlock(KRefBlock),1,.false.,ich)
        if(ich.ne.0) go to 8100
      else if(i.eq.6) then
        NDim95P=NDim95P+1
        call StToReal(t80,k,QuRefBlock(1,NDim95P-3,KRefBlock),3,
     1                .false.,ich)
        if(ich.ne.0) go to 8100
      else if(i.eq.7) then
        if(.not.allocated(rm)) then
          n=NDim(KPhaseDR)
          NDim(KPhaseDR)=NDim95P
          call AllocateSymm(1,1,1,1)
          NDim(KPhaseDR)=n
        endif
        NLattP=NLattVec(1)+1
        call ReallocSymm(NDim95P,NSymm(KPhaseDR),NLattP,NComp(KPhaseDR),
     1                   NPhase,0)
        NLattVec(1)=NLattP
        call StToReal(t80,k,vt6(1,NLattVec(1),1,1),NDim95P,.false.,ich)
        if(ich.ne.0) go to 8100
      else if(i.eq.8) then
        NFaces(KRefBlock)=NFaces(KRefBlock)+1
        call StToReal(t80,k,DFace(1,NFaces(KRefBlock),KRefBlock),4,
     1                .false.,ich)
        if(ich.ne.0) go to 8100
      else if(i.eq.9) then
        FormulaRefBlock=' '
        m=0
        do i=k+1,idel(t80)
          if(t80(i:i).eq.'?') then
            FormulaRefBlock=' '
            exit
          endif
          j=ichar(t80(i:i))
          if(j.ge.ichar('A').and.j.le.ichar('Z')) then
            if(k.ne.0) then
              m=m+1
              FormulaRefBlock(m:m)=' '
            endif
          endif
          m=m+1
          FormulaRefBlock(m:m)=t80(i:i)
        enddo
      endif
      go to 1000
8000  do i=1,5
        if(idn(i).eq.0) go to 8050
      enddo
      Profil=KdoToVola.eq.0
      if(.not.UzJiMa) call DRCellFromUB
      go to 8888
8050  if(KdoToVola.eq.0) then
        Cislo='p4o'
      else
        Cislo='p4p'
      endif
      call FeChybne(-1.,-1.,'the "'//Cislo(:idel(Cislo))//
     1              '" file doesn''t contain basic information.',' ',
     2              Warning)
      go to 8888
8100  call FeChybne(-1.,-1.,'the syntactic error in the following '//
     1              'line:',t80,SeriousError)
8500  ErrFlag=1
8888  if(KdoToVola.ne.0) call CloseIfOpened(ln)
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)='(3i4,2f8.2,i4,6f8.5)'
      if(NDim95P.gt.3) then
        NDim95(KRefBlock)=6
        FormatRefBlock(KRefBlock)(2:2)='6'
      endif
      if(DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
        rewind 71
        NRuns(KRefBlock)=0
        ikde1=(NDim95(KRefBlock)+1)*4+65
        ikde2=(NDim95(KRefBlock)+1)*4+82
        RunNFrames=0
        RunMeasureTime=1.
9000    read(71,FormA,end=9020) t256
        if(t256.eq.' ') go to 9000
        read(t256,FormatRefBlock(KRefBlock))
     1    (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it
        read(t256(ikde1:),'(i3)') i
        if(i.ne.0) go to 9000
        NRuns(KRefBlock)=max(NRuns(KRefBlock),it)
        read(t256(ikde2:),'(f8.2)') pom
        RunNFrames(it,KRefBlock)=
     1    max(RunNFrames(it,KRefBlock),ifix(pom)+1)
        go to 9000
9020    rewind(71)
      endif
      return
101   format(f15.0)
102   format(e15.0)
      end
