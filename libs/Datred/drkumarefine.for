      subroutine DRKUMARefine(KumaInput)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension in(2),SlozkyP(4),UBFromFile(3,3),U(3,3),Ui(3,3),hp(6),
     1          c(3),xp(3),AM(225),PS(18),Ugen(18),AMInv(225),SuGen(18),
     2          SU(3,3),G(3,3),SG(3,3),SCellRefBlock(6),Uq(9),SUq(9),
     3          Uqp(9),SUqp(9),squ(3,3),dtdgi(6),dtdrcp(6),dtdh3(3),
     4          der(15),sol(15),QuSave(3,3),MMin(3),MMax(3)
      character*256 FileIn,FileLst,FileMat,t256
      character*100 Radka
      character*8 Nazev
      character*1 znc
      integer FlagKP,EdwStateQuest,KumaInput,UseTabsIn
      logical FeYesNoHeader,lpom,EqIgCase,Indexace
      dimension Slozky(:,:),Int(:),iha(:,:),BraggTh(:),
     1          SlozkyO(:,:),IntO(:)
      integer FlagI(:)
      allocatable Slozky,Int,iha,BraggTh,FlagI,SlozkyO,IntO
      equivalence (u,ub),(ui,ubi)
      data RelIMin,RelIMax/0.,1000000./,sinthlMin,sinthlMax/0.,1./,
     1     DiffMax/.1/,MMaxP/1/,QuSave/9*0./
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      pom=50.
      do i=1,9
        call FeTabsAdd(pom*float(i),UseTabs,IdLeftTab,' ')
      enddo
      KRefBlock=1
      MMaxP=1
      FileIn=' '
      ln=0
      lno=0
      if(KumaInput.eq.1) then
        Radka='*.ta*'
      else if(KumaInput.eq.2) then
        Radka='*.p4p'
      else if(KumaInput.eq.3) then
        Radka='*.rmat'
      else
        go to 9999
      endif
      call FeFileManager('Select peak table file',FileIn,Radka,0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,FileIn,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      FileLst=FileIn
      i=idel(FileLst)
1020  if(FileLst(i:i).ne.'.') then
        i=i-1
        if(i.gt.1) then
          go to 1020
        else
          go to 1030
        endif
      endif
      FileLst(i+1:)=' '
1030  FileLst=FileLst(:i)//'lst'
      FileMat=FileLst(:i)//'mat'
      call OpenFile(lno,FileLst,'formatted','unknown')
      RelIMin=0.
      RelIMax=1000000.
      call DRKumaIndexLimits(KumaInput,sinthlMin,sinthlMax,RelIMin,
     1                       RelIMax,Indexace,ich)
      if(ich.ne.0) go to 9999
      if(KumaInput.ne.1) then
        mxi=1000
        allocate(Slozky(4,mxi),Int(mxi))
      endif
1040  if(KumaInput.eq.1) then
        read(ln,FormA80) Radka
        k=0
        call StToInt(Radka,k,in,1,.false.,ich)
        if(ich.ne.0) go to 9999
        mxi=in(1)
        allocate(Slozky(4,mxi),Int(mxi),iha(6,mxi),BraggTh(mxi),
     1           FlagI(mxi))
        NRef=0
        do 1060i=1,mxi
          read(ln,104) SlozkyP,IntP,FlagKP
          pom=IntP
          if(pom.lt.RelIMin.or.pom.gt.RelIMax) go to 1060
          do j=1,4
            SlozkyP(j)=SlozkyP(j)/LamAveD(6)
          enddo
          pom=.5*VecOrtLen(SlozkyP,3)
          if(pom.lt.sinthlMin.or.pom.gt.sinthlMax) go to 1060
          NRef=NRef+1
          call CopyVek(SlozkyP,Slozky(1,NRef),4)
          Int(NRef)=IntP
1060    continue
      else if(KumaInput.eq.2) then
        NRef=0
1080    read(ln,FormA,end=2100) t256
        call mala(t256)
        k=0
        call kus(t256,k,Nazev)
        if(Nazev(1:3).eq.'ort') then
          read(Nazev(4:4),'(i1)') i
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          do j=1,3
            UBFromFile(i,j)=xp(j)
          enddo
        else if(Nazev.eq.'ref05'.or.Nazev.eq.'ref1k') then
          NRef=NRef+1
          if(Nref.gt.mxi) then
            allocate(SlozkyO(4,mxi),IntO(mxi))
            call CopyVek (Slozky,SlozkyO,4*mxi)
            call CopyVekI(Int,IntO,mxi)
            mxio=mxi
            mxi=mxi+1000
            deallocate(Slozky,Int)
            allocate(Slozky(4,mxi),Int(mxi))
            call CopyVek (SlozkyO,Slozky,4*mxio)
            call CopyVekI(IntO,Int,mxio)
            deallocate(SlozkyO,IntO)
          endif
          call kus(t256,k,Nazev)
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9999
          int(NRef)=nint(xp(1))
          call StToReal(t256,k,Slozky(1,NRef),3,.false.,ich)
          Slozky(4,NRef)=0.
          if(ich.ne.0) go to 9999
        endif
        go to 1080
      else if(KumaInput.eq.3) then
1110    read(ln,FormA,end=9999) t256
        k=0
        call kus(t256,k,Nazev)
        if(EqIgCase(Nazev,'rmat')) then
          do i=1,3
            read(ln,FormA,end=9999) t256
            k=0
            call StToReal(t256,k,xp,3,.false.,ich)
            do j=1,3
              UBFromFile(i,j)=xp(j)
            enddo
          enddo
        else
          go to 1110
        endif
        call CloseIfOpened(ln)
        FileIn='cell.drx'
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        NRef=0
        read(ln,FormA) t256
1120    read(ln,FormA,end=2100) t256
        if(t256.eq.' ') go to 1120
        NRef=NRef+1
        if(Nref.gt.mxi) then
          allocate(SlozkyO(4,mxi),IntO(mxi))
          call CopyVek (Slozky,SlozkyO,4*mxi)
          call CopyVekI(Int,IntO,mxi)
          mxio=mxi
          mxi=mxi+1000
          deallocate(Slozky,Int)
          allocate(Slozky(4,mxi),Int(mxi))
          call CopyVek (SlozkyO,Slozky,4*mxio)
          call CopyVekI(IntO,Int,mxio)
          deallocate(SlozkyO,IntO)
        endif
        k=0
        call StToReal(t256,k,Slozky(1,NRef),3,.false.,ich)
        if(ich.ne.0) go to 9999
        Slozky(4,NRef)=0.
        call StToReal(t256,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9999
        Int(NRef)=nint(xp(1))
        go to 1120
      endif
2100  if(KumaInput.eq.1) then
        pom=0.
        do i=1,3
          read(ln,104)(UBFromFile(i,j),j=1,3)
          read(ln,104)
          do j=1,3
            UBFromFile(i,j)=UBFromFile(i,j)/LamAveD(6)
            pom=pom+abs(UBFromFile(i,j))
          enddo
          if(pom.lt..0001) then
            call FeChybne(-1.,-1.,'Orientation matrix is singular.',' ',
     1                    SeriousError)
            go to 9999
          endif
        enddo
      else
        allocate(iha(NDim(KPhase),mxi),BraggTh(mxi),FlagI(mxi))
        call SetIntArrayTo(FlagI,NRef,0)
      endif
      call CloseIfOpened(ln)
      call CopyVek(UBFromFile,U,9)
      n=0
      id=NextQuestId()
      xqd=350.
      il=6
      Radka='Define modulation vectors'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Radka,0,LightGray,-1,0)
      il=1
      tpom=5.
      Radka='%Number of modulation vectors'
      xpom=tpom+FeTxLengthUnder(Radka)+10.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,1)
      nEdwNDim=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,0,.false.)
      call FeQuestEudOpen(EdwLastMade,0,3,1,0.,0.,0.)
      xpom=tpom+FeTxLength('XXXX')+3.
      dpom=xqd-xpom-5.
      do i=1,3
        il=il+1
        write(Radka,'(''q('',i1,'')'')') i
        call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwFirstQ=EdwLastMade
      enddo
      il=il+1
      tpom=5.
      dpom=70.
      Radka='%Maximal satellite index'
      xpom=tpom+FeTxLength(Radka)+3.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,0)
      nEdwMMax=EdwLastMade
      il=il+1
      Radka='%Delta in degs'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,EdwYd,0)
      nEdwDelta=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DiffMax,.false.,.false.)
2190  nEdw=nEdwFirstQ
      do i=1,3
        lpom=.false.
        do j=1,3
          if(abs(QuSave(j,i)).gt..0001) go to 2194
        enddo
        lpom=.true.
2194    if(i.le.n) then
          if(EdwStateQuest(nEdw).ne.EdwOpened)
     1      call FeQuestRealAEdwOpen(nEdw,QuSave(1,i),3,lpom,.false.)
        else
          call FeQuestEdwClose(nEdw)
        endif
        nEdw=nEdw+1
      enddo
      if(n.gt.0) then
        if(EdwStateQuest(nEdwMMax).ne.EdwOpened)
     1    call FeQuestIntEdwOpen(nEdwMMax,MMaxP,.false.)
      else
        call FeQuestEdwClose(nEdwMMax)
      endif
2200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNDim) then
        call FeQuestIntFromEdw(nEdwNDim,n)
        go to 2190
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2200
      endif
      nEdw=nEdwFirstQ
      do i=1,n
        if(i.le.n) then
          call FeQuestRealAFromEdw(nEdw,QuSave(1,i))
        else
          call SetRealArrayTo(QuSave(1,i),3,0.)
        endif
        nEdw=nEdw+1
      enddo
      call CopyVek(QuSave,Qu(1,1,1,KPhase),9)
      call FeQuestRealFromEdw(nEdwDelta,DiffMax)
      call FeQuestIntFromEdw(nEdwMMax,MMaxP)
      call FeQuestRemove(id)
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      NLattVec(KPhase)=1
      NDim(KPhase)=n+3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      call Matinv(U,Ui,Vol,3)
2230  rewind lno
      Indexed=0
      IndexedSat=0
      il=NDim(KPhase)*4+30
      MMax=0
      do i=1,NDimI(KPhase)
        MMax(i)=MMaxP
      enddo
      MMin=-MMax
      do i=1,NRef
        call Multm(Ui,Slozky(1,i),hp,3,3,1)
        BraggTh(i)=asin(.5*VecOrtLen(Slozky(1,i),3)*LamAveD(6))
        call ChngInd(hp,iha(1,i),1,c,MMin,MMax,1,CheckExtRefNo)
        call multm(U,c,xp,3,3,1)
        znc=' '
        if(asin(.5*VecOrtLen(xp,3)*LamAveD(6))/ToRad.lt.DiffMax) then
          FlagI(i)=1
          Indexed=Indexed+1
          if(iha(4,i).ne.0.or.iha(5,i).ne.0) IndexedSat=IndexedSat+1
          do k=2,NLattVec(KPhase)
            pom=0.
            do j=1,NDim(KPhase)
              pom=pom+float(iha(j,i))*vt6(j,k,1,KPhase)
            enddo
            if(abs(anint(pom)-pom).gt..1) then
              znc='*'
              go to 2236
            endif
          enddo
2236      write(Radka,'(3f8.3,'' => '',6i4)')(hp(j),j=1,3),
     1                 (iha(j,i),j=1,NDim(KPhase))
        else
          FlagI(i)=0
          write(Radka,'(3f8.3,'' => not indexed !!!'')')(hp(j),j=1,3)
        endif
        write(Radka(il:),'(i10)') Int(i)
        write(lno,FormA1)(Radka(j:j),j=1,idel(Radka)),' ',(znc,j=1,3)
      enddo
      call SetRealArrayTo(am,NDim(KPhase)*NDim(KPhase),0.)
      call SetRealArrayTo(ps,3*NDim(KPhase),0.)
      do 2300i=1,NRef
        if(FlagI(i).eq.0) go to 2300
        do j=1,NDim(KPhase)
          hp(j)=iha(j,i)
        enddo
        m=0
        do j=1,NDim(KPhase)
          do k=1,NDim(KPhase)
            m=m+1
            AM(m)=AM(m)+hp(j)*hp(k)
          enddo
        enddo
        m=0
        do k=1,NDim(KPhase)
          do j=1,3
            m=m+1
            PS(m)=PS(m)+Slozky(j,i)*hp(k)
          enddo
        enddo
2300  continue
      call matinv(AM,AMinv,det,NDim(KPhase))
      call Multm(PS,AMinv,Ugen,3,NDim(KPhase),NDim(KPhase))
      do j=1,3
        pom=0.
        do 2380i=1,NRef
          if(FlagI(i).eq.0) go to 2380
          do k=1,NDim(KPhase)
            hp(k)=iha(k,i)
          enddo
          pp=Slozky(j,i)
          do k=1,NDim(KPhase)
            pp=pp-Ugen(j+(k-1)*3)*hp(k)
          enddo
          pom=pom+pp**2
2380    continue
        pom=sqrt(pom/float(Indexed-NDim(KPhase)))
        do k=1,NDim(KPhase)
          SUgen(j+(k-1)*3)=pom*sqrt(AMinv(k+(k-1)*NDim(KPhase)))
          if(k.le.3) then
            U(j,k)=Ugen(j+(k-1)*3)
            SU(j,k)=SUgen(j+(k-1)*3)
          else
            Uq(j+(k-4)*3)=Ugen(j+(k-1)*3)
            SUq(j+(k-4)*3)=SUgen(j+(k-1)*3)
          endif
        enddo
      enddo
      call Matinv(U,Ui,Vol,3)
      call multm (Ui,Uq,Uqp,3,3,NDimI(KPhase))
      call multmq(Ui,SUq,SUqp,3,3,NDimI(KPhase))
      do i=1,3
        if(NDimI(KPhase).gt.0) then
           quSave(i,1)= Uqp(i)
          squ(i,1)    =SUqp(i)
        endif
        if(NDimI(KPhase).gt.1) then
           quSave(i,2)= Uqp(i+3)
          squ(i,2)    =SUqp(i+3)
        endif
      enddo
      call DRCellFromUB
      call DRSCellFromSUB(U,SU,SCellRefBlock,Vol,SVol,G,SG)
      write(lno,'(3f10.6)')((U(i,j),j=1,3),i=1,3)
      open(ln,file=FileMat)
      write(ln,'(3f10.6)')((U(i,j),j=1,3),i=1,3)
      write(ln,'(3f10.6)')((QuSave(j,i),j=1,3),i=1,1)
      close(ln)
      write(lno,FormA1)
      write(lno,'(''Calculated from refined orientation matrix'')')
      write(lno,'(''=========================================='')')
      write(lno,FormA1)
      if(NDim(KPhase).gt.3) then
        NInfo=8
      else
        NInfo=5
      endif
      TextInfo(1)=Tabulator//' '//Tabulator//' '//Tabulator//' '//
     1            Tabulator//'Cell parameters'//Tabulator//' '//
     2            Tabulator//' '//Tabulator//'Volume'
      write(Radka,100) 'Cell parameters:',(CellRefBlock(j,1),j=1,6),Vol
      TextInfo(2)=' '
      TextInfo(3)=' '
      do i=1,3
        write(Cislo,103) Tabulator,CellRefBlock(i,1)
        call Zhusti(Cislo)
        TextInfo(2)=TextInfo(2)(:idel(TextInfo(2)))//Cislo(:idel(Cislo))
        write(Cislo,103) Tabulator,SCellRefBlock(i)
        call Zhusti(Cislo)
        TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//Cislo(:idel(Cislo))
      enddo
      do i=4,6
        write(Cislo,105) Tabulator,CellRefBlock(i,1)
        call Zhusti(Cislo)
        TextInfo(2)=TextInfo(2)(:idel(TextInfo(2)))//Cislo(:idel(Cislo))
        write(Cislo,105) Tabulator,SCellRefBlock(i)
        call Zhusti(Cislo)
        TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//Cislo(:idel(Cislo))
      enddo
      write(Cislo,106) Tabulator,Vol
      call Zhusti(Cislo)
      TextInfo(2)=TextInfo(2)(:idel(TextInfo(2)))//Cislo(:idel(Cislo))
      write(Cislo,106) Tabulator,SVol
      TextInfo(3)=TextInfo(3)(:idel(TextInfo(3)))//Cislo(:idel(Cislo))
      call Zhusti(Cislo)
      write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
      write(Radka,100) 's.u.:           ',(SCellRefBlock(j),j=1,6),SVol
      n=3
      if(NDim(KPhase).gt.3) then
        write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
        write(lno,FormA1)
        Radka=' '
        j=12
        TextInfo(4)=Tabulator//' '//Tabulator
        do i=1,NDimI(KPhase)
          write(Cislo,'(''q('',i1,''):'')') i
          Radka(j:)=Cislo
          j=j+29
          TextInfo(4)=TextInfo(4)(:idel(TextInfo(4)))//
     1                Cislo(:idel(Cislo))//
     2                Tabulator//' '//Tabulator//' '//Tabulator
        enddo
        write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
        k=1
        TextInfo(5)=' '
        do i=1,NDimI(KPhase)
          write(Radka(k:),'(3f8.5)')(QuSave(j,i),j=1,3)
          k=k+29
          do j=1,3
            write(Cislo,107) Tabulator,QuSave(j,i)
            call Zhusti(Cislo)
            TextInfo(5)=TextInfo(5)(:idel(TextInfo(5)))//
     1                  Cislo(:idel(Cislo))
          enddo
        enddo
        write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
        k=1
        TextInfo(6)=' '
        do i=1,NDimI(KPhase)
          write(Radka(k:),'(3f8.5)')(squ(j,i),j=1,3)
          k=k+29
          do j=1,3
            write(Cislo,107) Tabulator,squ(j,i)
            call Zhusti(Cislo)
            TextInfo(6)=TextInfo(6)(:idel(TextInfo(6)))//
     1                  Cislo(:idel(Cislo))
          enddo
        enddo
        write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
        n=6
      endif
      n=n+1
      write(TextInfo(n),'(i5,'' reflections of all '',i5,
     1                    '' were used'')') Indexed,NRef
      n=n+1
      write(TextInfo(n),'(i5,'' satellites were used'')')
     1      IndexedSat
      Radka='Do you want repeat the refine process?'
      if(FeYesNoHeader(-1.,-1.,Radka,1)) go to 2230
      go to 5000
      write(lno,FormA1)
      write(lno,FormA1)
      write(lno,FormA1)
      write(lno,FormA1)
3000  NComp(KPhase)=1
      call CopyVek(CellRefBlock(1,1),CellPar,6)
      ncykl=0
3100  call SetRealArrayTo(am,225,0.)
      call SetRealArrayTo(ps,15,0.)
      npar=6+NDimI(KPhase)*3
      SetMetAllowed=.true.
      call SetMet(0)
      wdy=0
      do 3400n=1,NRef
        if(FlagI(n).eq.0) go to 3400
        call FromIndSinthl(iha(1,n),hp,sinthl,sinthlq,1,0)
        BraggThCalc=asin(sinthl*LamAveD(6))
        tth=tan(BraggThCalc)
        pom=tth/sinthlq*.25
        do i=1,6
          call indext(i,k,l)
          dtdgi(i)=pom*hp(k)*hp(l)
        enddo
        do i=1,3
          dtdrcp(i)=dtdgi(i)*rcp(i,1,KPhase)
          do 3210j=1,3
            if(i.le.2) then
              k=j
            else
              k=j-1
            endif
            if(j.eq.4-i) go to 3210
            dtdrcp(i)=dtdrcp(i)+dtdgi(j+3)*rcp(7-j,1,KPhase)*
     1                                        rcp(k,1,KPhase)
3210      continue
          if(NDim(KPhase).gt.3) then
            call multm(MetTensI(1,1,KPhase),hp,dtdh3,3,3,1)
            do j=1,3
              dtdh3(j)=dtdh3(j)*pom
            enddo
          endif
        enddo
        do i=4,6
          call indext(i,k,l)
          dtdrcp(10-i)=dtdgi(i)*rcp(k,1,KPhase)*rcp(l,1,KPhase)
        enddo
        j=1
        der(j)=-dtdrcp(1)*sina(KPhase)/
     1         (Vcos(KPhase)*CellPar(1,1,KPhase)**2)
        j=j+1
        der(j)=-dtdrcp(2)*sinb(KPhase)/
     1         (Vcos(KPhase)*CellPar(2,1,KPhase)**2)
        j=j+1
        der(j)=-dtdrcp(3)*sing(KPhase)/
     1         (Vcos(KPhase)*CellPar(3,1,KPhase)**2)
        j=j+1
        der(j)=
     1    ((dtdrcp(1)/CellPar(1,1,KPhase)*cotgbr(KPhase)*cotggr(KPhase)+
     2      dtdrcp(2)/CellPar(2,1,KPhase)*cotgar(KPhase)/singr(KPhase)+
     3      dtdrcp(3)/CellPar(3,1,KPhase)*cotgar(KPhase)/sinbr(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(4)*sina(KPhase)/(sinb(KPhase)*sing(KPhase))+
     6      dtdrcp(5)*cosgr(KPhase)*sinb(KPhase)/
     7      (sina(KPhase)*sing(KPhase))+
     8      dtdrcp(6)*cosbr(KPhase)*sing(KPhase)/
     9      (sina(KPhase)*sinb(KPhase)))*torad
        j=j+1
        der(j)=
     1    ((dtdrcp(2)/CellPar(2,1,KPhase)*cotggr(KPhase)*cotgar(KPhase)+
     2      dtdrcp(3)/CellPar(3,1,KPhase)*cotgbr(KPhase)/sinar(KPhase)+
     3      dtdrcp(1)/CellPar(1,1,KPhase)*cotgbr(KPhase)/singr(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(5)*sinb(KPhase)/(sina(KPhase)*sing(KPhase))+
     6      dtdrcp(4)*cosgr(KPhase)*sina(KPhase)/
     7      (sinb(KPhase)*sing(KPhase))+
     8      dtdrcp(6)*cosar(KPhase)*sing(KPhase)/
     9      (sinb(KPhase)*sina(KPhase)))*torad
        j=j+1
        der(j)=
     1    ((dtdrcp(3)/CellPar(3,1,KPhase)*cotgar(KPhase)*cotgbr(KPhase)+
     2      dtdrcp(1)/CellPar(1,1,KPhase)*cotggr(KPhase)/sinbr(KPhase)+
     3      dtdrcp(2)/CellPar(2,1,KPhase)*cotggr(KPhase)/sinar(KPhase))/
     4      Vcos(KPhase)+
     5      dtdrcp(6)*sing(KPhase)/(sina(KPhase)*sinb(KPhase))+
     6      dtdrcp(4)*cosbr(KPhase)*sina(KPhase)/
     7      (sing(KPhase)*sinb(KPhase))+
     8      dtdrcp(5)*cosar(KPhase)*sinb(KPhase)/
     9      (sing(KPhase)*sina(KPhase)))*torad
        if(NDim(KPhase).gt.3) then
          do k=1,NDimI(KPhase)
            do i=1,3
              j=j+1
              der(j)=float(iha(k+3,n))*dtdh3(i)
            enddo
          enddo
        endif
        dy=(BraggTh(n)-BraggThCalc)
        wdy=wdy+dy**2
        m=0
        do j=1,npar
          do k=1,npar
            m=m+1
            AM(m)=AM(m)+der(j)*der(k)
          enddo
          PS(j)=PS(j)+der(j)*dy
        enddo
3400  continue
      call matinv(AM,AMinv,det,npar)
      call Multm(AMinv,PS,Sol,npar,npar,1)
      wdy=sqrt(wdy/float(Indexed-npar))
      pom=0.
      ChngRel=-0.1
      do i=1,npar
        pom=pom+abs(Sol(i))
        esd=wdy*sqrt(AMinv(i+(i-1)*npar))
        if(i.le.6) then
          CellPar(i,1,KPhase)=CellPar(i,1,KPhase)+Sol(i)
          SCellRefBlock(i)=esd
        else if(i.le.9) then
          quSave(i-6,1)=quSave(i-6,1)+Sol(i)
          squ(i-6,1)=esd
        else if(i.le.12) then
          quSave(i-9,2)=quSave(i-9,2)+Sol(i)
          squ(i-9,2)=esd
        else if(i.le.15) then
          quSave(i-12,3)=quSave(i-12,3)+Sol(i)
          squ(i-12,3)=esd
        endif
        ChngRel=max(abs(Sol(i)/esd),ChngRel)
      enddo
      ncykl=ncykl+1
      if(ncykl.lt.10.and.ChngRel.gt..01) go to 3100
      CosAlfa=cos(CellPar(4,1,KPhase)*ToRad)
      CosBeta=cos(CellPar(5,1,KPhase)*ToRad)
      CosGama=cos(CellPar(6,1,KPhase)*ToRad)
      SCosAlfa=sin(CellPar(4,1,KPhase)*ToRad)*ToRad*SCellRefBlock(4)
      SCosBeta=sin(CellPar(5,1,KPhase)*ToRad)*ToRad*SCellRefBlock(5)
      SCosGama=sin(CellPar(6,1,KPhase)*ToRad)*ToRad*SCellRefBlock(6)
      PomQ=1.-CosAlfa**2-CosBeta**2-CosGama**2+
     1        2.*CosAlfa*CosBeta*CosGama
      SPomQ=2.*(CosAlfa*SCosAlfa+CosBeta*SCosBeta+
     1          CosGama*SCosGama+
     2          SCosAlfa* CosBeta* CosGama+
     3           CosAlfa*SCosBeta* CosGama+
     4           CosAlfa* CosBeta*SCosGama)
      Pom=sqrt(PomQ)
      SPom=.5/Pom*SPomQ
      AA=CellPar(1,1,KPhase)
      BB=CellPar(2,1,KPhase)
      CC=CellPar(3,1,KPhase)
      SAA=SCellRefBlock(1)
      SBB=SCellRefBlock(2)
      SCC=SCellRefBlock(3)
      Vol=AA*BB*CC*Pom
      SVol=SAA*BB*CC*Pom+AA*SBB*CC*Pom+AA*BB*SCC*Pom+AA*BB*CC*SPom
      write(lno,'(''Calculated from refined theta positions'')')
      write(lno,'(''======================================='')')
      write(lno,FormA1)
      write(lno,100) 'Cell parameters:',(CellPar(j,1,KPhase),j=1,6),Vol
      write(lno,100) 's.u.:           ',(SCellRefBlock(j),j=1,6),SVol
      write(lno,FormA1)
      Radka=' '
      j=12
      do i=1,NDimI(KPhase)
        write(Radka(j:),'(''q('',i1,''):'')') i
        j=j+29
      enddo
      write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
      j=1
      do i=1,NDimI(KPhase)
        write(Radka(j:),'(3f8.5)')(quSave(j,i),j=1,3)
        j=j+29
      enddo
      write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
      j=1
      do i=1,NDimI(KPhase)
        write(Radka(j:),'(3f8.5)')(squ(j,i),j=1,3)
        j=j+29
      enddo
      write(lno,FormA1)(Radka(i:i),i=1,idel(Radka))
      pom=(CellPar(1,1,KPhase)+CellPar(2,1,KPhase))*.5
      spom=(SCellRefBlock(1)+SCellRefBlock(2))*.5
      pomq=(quSave(1,1)+quSave(2,1)+
     1      quSave(1,2)-quSave(2,2))*.25
      spomq=(squ(1,1)+squ(2,1)+squ(1,2)+squ(2,2))*.25
      write(lno,FormA1)
      write(lno,'(''a:      '',2f8.4)') pom,spom
      write(lno,'(''c:      '',2f8.4)') CellPar(3,1,KPhase),
     1                                  SCellRefBlock(3)
      write(lno,'(''Vol:    '',2f8.2)') Vol,Svol
      write(lno,'(''q(ave): '',2f8.4)') pomq,spomq
      write(lno,FormA1)
      write(lno,FormA1)
      write(lno,FormA1)
5000  write(lno,'(i5,'' reflections of all '',i5,'' were used'')')
     1      Indexed,NRef
      write(lno,'(i5,'' satellites were used'')') IndexedSat
      close(lno)
      call FeEdit(FileLst)
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      if(allocated(Slozky)) deallocate(Slozky,Int)
      if(allocated(iha)) deallocate(iha,BraggTh,FlagI)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(a16,3f9.4,3f8.3,f10.2)
101   format(a16,6f8.4)
102   format(a16,6f8.2)
103   format(a1,f14.4)
104   format(4e21.11e4,i11,i6)
105   format(a1,f14.3)
106   format(a1,f14.2)
107   format(a1,f8.5)
      end
