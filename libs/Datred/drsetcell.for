      subroutine DRSetCell(KRefB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      do i=1,3
        CellPar(i,1,KPhaseDR)=CellRefBlock(i,KRefB)
        CellPar(i+3,1,KPhaseDR)=CellRefBlock(i+3,KRefB)
        rcp(i,1,KPhaseDR)=CellRefBlock(i,KRefB)
        rcp(i+3,1,KPhaseDR)=cos(CellRefBlock(i+3,KRefB)*ToRad)
      enddo
      call recip(rcp(1,1,KPhaseDR),rcp(1,1,KPhaseDR),pom)
      do i=1,3
        prcp(i,1,KPhaseDR)=.25*rcp(i,1,KPhaseDR)**2
      enddo
      prcp(4,1,KPhaseDR)=.25*rcp(1,1,KPhase)*rcp(2,1,KPhaseDR)*
     1                                       rcp(6,1,KPhaseDR)
      prcp(5,1,KPhaseDR)=.25*rcp(1,1,KPhase)*rcp(3,1,KPhaseDR)*
     1                                       rcp(5,1,KPhaseDR)
      prcp(6,1,KPhaseDR)=.25*rcp(2,1,KPhase)*rcp(3,1,KPhaseDR)*
     1                                       rcp(4,1,KPhaseDR)
      csa=cos(CellRefBlock(4,KRefB)*ToRad)
      csb=cos(CellRefBlock(5,KRefB)*ToRad)
      csg=cos(CellRefBlock(6,KRefB)*ToRad)
      snb=sqrt(1.-csb**2)
      sng=sqrt(1.-csg**2)
      pom=(csa-csb*csg)/sng
      TrToOrtho(1,1,KPhaseDR)=CellRefBlock(1,KRefB)
      TrToOrtho(2,1,KPhaseDR)=0.
      TrToOrtho(3,1,KPhaseDR)=0.
      TrToOrtho(4,1,KPhaseDR)=CellRefBlock(2,KRefB)*csg
      TrToOrtho(5,1,KPhaseDR)=CellRefBlock(2,KRefB)*sng
      TrToOrtho(6,1,KPhaseDR)=0.
      TrToOrtho(7,1,KPhaseDR)=CellRefBlock(3,KRefB)*csb
      TrToOrtho(8,1,KPhaseDR)=CellRefBlock(3,KRefB)*pom
      TrToOrtho(9,1,KPhaseDR)=CellRefBlock(3,KRefB)*snb*
     1                      sqrt(1.-(pom/snb)**2)
      MetTens(1,1,KPhaseDR)=CellRefBlock(1,KRefB)**2
      MetTens(5,1,KPhaseDR)=CellRefBlock(2,KRefB)**2
      MetTens(9,1,KPhaseDR)=CellRefBlock(3,KRefB)**2
      MetTens(2,1,KPhaseDR)=CellRefBlock(1,KRefB)*
     1                      CellRefBlock(2,KRefB)*csg
      MetTens(4,1,KPhaseDR)=MetTens(2,1,KPhase)
      MetTens(3,1,KPhaseDR)=CellRefBlock(1,KRefB)*
     1                      CellRefBlock(3,KRefB)*csb
      MetTens(7,1,KPhaseDR)=MetTens(3,1,KPhase)
      MetTens(6,1,KPhaseDR)=CellRefBlock(2,KRefB)*
     1                      CellRefBlock(3,KRefB)*csa
      MetTens(8,1,KPhaseDR)=MetTens(6,1,KPhase)
      call matinv(MetTens(1,1,KPhase),MetTensI(1,1,KPhase),pom,3)
      CellVol(1,KPhaseDR)=TrToOrtho(1,1,KPhaseDR)*
     1                    TrToOrtho(5,1,KPhaseDR)*
     2                    TrToOrtho(9,1,KPhaseDR)
      return
      end
