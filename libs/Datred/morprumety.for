      subroutine MorPrumety
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer Color
      real koef
      logical CrwLogicQuest
      do i=1,NFaces(KRefBlock)
        j=0
1000    j=j+1
        if(rovHr1(i,j).ne.0) then
          if(bodHr(rovHr1(i,j),rovHr2(i,j)).lt.3) then
             bodHr(rovHr1(i,j),rovHr2(i,j))=
     1       bodHr(rovHr1(i,j),rovHr2(i,j))+2
          endif
          go to 1000
        endif
      enddo
      do i=1,NFaces(KRefBlock)
        if(rkrov(3,i).lt.-.0001) cycle
        j=0
1020    j=j+1
        if(rovHr1(i,j).ne.0) then
          if(bodHr(rovHr1(i,j),rovHr2(i,j)).gt.2) then
            bodHr(rovHr1(i,j),rovHr2(i,j))=
     1        bodHr(rovHr1(i,j),rovHr2(i,j))-2
          endif
          go to 1020
        endif
      enddo
      rmax=0.
      do i=1,PocetBodu
        rmax=max(VecOrtLen(Rbod(1,i),3),rmax)
      enddo
      koef=(XLenAcWin*.5-10.)/rmax
1200  if(scale*koef*rmax.gt.XLenAcWin*.5) then
        scale=scale-0.005
        go to 1200
      endif
      pom=scale*koef
      do i=1,PocetBodu
        BodRX(i)=RBod(1,i)*pom+XCenAcWin
        BodRY(i)=RBod(2,i)*pom+YCenAcWin
      enddo
      call FeClearGrWin
      do i=1, PocetBodu-1
        do j=i+1,PocetBodu
          xu(1)=BodRX(i)
          xu(2)=BodRX(j)
          yu(1)=BodRY(i)
          yu(2)=BodRY(j)
          k=bodHr(i,j)
          if(k.le.2) then
            call FeLineType(NormalLine)
          else
            call FeLineType(DashedLine)
          endif
          if(k.eq.1) then
            Color=White
          else if(k.eq.2) then
            Color=Green
          else if(k.eq.3) then
            Color=LightGray
          else if(k.eq.4) then
            Color=Green
          else
            cycle
          endif
          call FePolyLine(2,xu,yu,Color)
        enddo
      enddo
      if(CrwLogicQuest(nCrwShowAxes)) then
        do i=1,3
          call MorShowVect(RAxis(1,i),SmbABC(i),Yellow)
        enddo
      endif
      call FeLineType(NormalLine)
      return
      end
