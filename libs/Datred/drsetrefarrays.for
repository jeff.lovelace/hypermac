      subroutine DRSetRefArrays(Klic)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension h(6),hp(6),difi(3),dif(3),ihq(6),MMin(3),MMax(3)
      logical ExistFile
      data difi/3*.01/
      MaxRefBlockAr=0
      NRefBlockAr=0
      if(Klic.eq.0) then
        NRefBFrom=KRefBlock
        NRefBTo=KRefBlock
      else
        NRefBFrom=1
        NRefBTo=NRefBlock
      endif
      rim=0.
      rsm=0.
      call SetIntArrayTo(HCondMx,6,0)
      if(allocated(riar)) deallocate(riar,rsar,ICullAr,RefBlockAr,
     1                               RunCCDAr)
      NRefAlloc=10000
      allocate(riar(NRefAlloc),rsar(NRefAlloc),ICullAr(NRefAlloc),
     1         RefBlockAr(NRefAlloc),RunCCDAr(NRefAlloc))
      call SetRealArrayTo(hp,6,0.)
      do mm=1,2
        NRefRead=0
        if(mm.eq.1) NRefReadObs=0
        do IRefBlock=NRefBFrom,NRefBTo
          if(Klic.gt.0) then
            if(RefDatCorrespond(IRefBlock).ne.KDatBlock) cycle
          else if(Klic.lt.0) then
            if(.not.UseInSGTest(IRefBlock)) cycle
          endif
          if(ExistFile(RefBlockFileName)) then
            call OpenFile(95,RefBlockFileName,'formatted','old')
          else
            call OpenRefBlockM95(95,IRefBlock,fln(:ifln)//'.m95')
          endif
          if(ErrFlag.ne.0) go to 9999
          if(mm.eq.1) NRefBlockAr=NRefBlockAr+1
2100      call DRGetReflectionFromM95(95,iend,ich)
          if(iend.ne.0.or.ich.ne.0) go to 2120
!          if(no.lt.0.or.iflg(1).lt.0.or.iflg(2).ne.1) go to 2100
          if(no.lt.0.or.iflg(2).lt.0) go to 2100
          itwi=iflg(2)
          MaxRefBlockAr=max(MaxRefBlockAr,IRefBlock)
          NRefRead=NRefRead+1
          if(NRefRead.gt.NRefAlloc.and.mm.eq.1)
     1       call ReallocSGTest(10000)
          call indtr(ih,TrMP,ihp,NDim(KPhase))
          if(ihp(1).gt.900) then
            NRefRead=NRefRead-1
            go to 2100
          endif
          MMaxP=0
          do i=4,NDim(KPhase)
            MMaxP=max(iabs(ihp(i)),MMaxP)
          enddo
          MMax=0
          do i=1,NDimI(KPhase)
            MMax(i)=MMaxP
          enddo
          MMin=-MMax
          do i=1,3
            h(i)=ihp(i)
            do l=1,NDimI(KPhase)
              h(i)=h(i)+quOrg(i,l)*float(ihp(l+3))
            enddo
          enddo
!
!
!
!          h(1:NDim(KPhase))=ih(1:NDim(KPhase))
!          call MultM(h,TrMP,hp,1,NDim(KPhase),NDim(KPhase))
!!          call indtr(ih,TrMP,ihp,NDim(KPhase))
!!          if(ihp(1).gt.900) then
!!            NRefRead=NRefRead-1
!!            go to 2100
!!          endif
!          MMaxP=0
!          do i=4,NDim(KPhase)
!            MMaxP=max(nint(abs(hp(i))),MMaxP)
!          enddo
!          MMax=0
!          do i=1,NDimI(KPhase)
!            MMax(i)=MMaxP
!          enddo
!          MMin=-MMax
!          do i=1,3
!            h(i)=hp(i)
!            do l=1,NDimI(KPhase)
!              h(i)=h(i)+quOrg(i,l)*hp(l+3)
!            enddo
!          enddo
          if(itwi.ne.1) then
            hp(1:3)=h(1:3)
            call MultM(hp,rtwi(1,itwi),h,1,3,3)
          endif
          if(Klic.le.0) then
            do 2110ntw=1,NTwin
              call multm(h,rtw(1,ntw),hp,1,3,3)
              call CopyVek(difi,dif,3)
              call ChngInd(hp,ih,1,dif,MMin,MMax,1,CheckExtRefNo)
              do i=1,3
                if(abs(dif(i)).gt.difi(i)) then
                  if(mm.eq.2) HCondAr(ntw,NRefRead)=-1
                  go to 2110
                endif
              enddo
              if(mm.eq.1) then
                do j=1,NSymm(KPhase)
                  call MultmIRI(ih,rm6(1,j,1,KPhase),ihq,1,NDim(KPhase),
     1                          NDim(KPhase))
                  do i=1,NDim(KPhase)
                    HCondMx(i)=max(iabs(ihq(i)),HCondMx(i))
                  enddo
                enddo
              else
                HCondAr(ntw,NRefRead)=IndPack(ih,HCondLn,HCondMx,
     1                                        NDim(KPhase))
              endif
2110        continue
          else
            call IndFromIndReal(h,mmaxp,difi,ih,itw,iswp,-1.,
     1                          CheckExtRefYes)
            if(iswp.le.0) then
              NRefRead=NRefRead-1
              go to 2100
            endif
            if(mm.eq.1) then
              do j=1,NSymm(KPhase)
                call MultmIRI(ih,rm6(1,j,1,KPhase),ihq,1,NDim(KPhase),
     1                        NDim(KPhase))
                do i=1,NDim(KPhase)
                  HCondMx(i)=max(iabs(ihq(i)),HCondMx(i))
                enddo
              enddo
            else
              call IndFromIndReal(h,mmaxp,difi,ih,itw,iswp,-1.,
     1                            CheckExtRefYes)
              if(iswp.le.0) then
                NRefRead=NRefRead-1
                go to 2100
              endif
              HCondAr(1,NRefRead)=IndPack(ih,HCondLn,HCondMx,
     1                                    NDim(KPhase))
            endif
          endif
          if(mm.eq.1) then
            pom1=corrf(1)*corrf(2)
            if(RunScN(KRefBlock).gt.0) then
              ikde=RunScGrN(ifix(expos),KRefBlock)
              if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                pom=ScFrame(ikde,KRefBlock)
              else
                c1=(expos-float(RunScGrM0(ikde,KRefBlock)))/
     1              float(RunScGrM(ikde,KRefBlock)-1)
                pom=(1.-c1)*ScFrame(ikde,KRefBlock)+
     1              c1*ScFrame(ikde+1,KRefBlock)
              endif
              pom1=pom1*pom
            endif
            riar(NRefRead)=anint(ri*pom1*100.)*.01
            rsar(NRefRead)=anint(rs*pom1*100.)*.01
            if(riar(NRefRead).gt.EM9ObsLim(KDatBlock)*rsar(NRefRead))
     1        NRefReadObs=NRefReadObs+1
            RefBlockAr(NRefRead)=IRefBlock+no*1000
            rim=max(riar(NRefRead),rim)
            rsm=max(rsar(NRefRead),rsm)
            ICullAr(NRefRead)=iflg(3)
            RunCCDAr(NRefRead)=expos
          endif
          go to 2100
2120      call CloseIfOpened(95)
        enddo
        if(mm.eq.1) then
          j=1
          do i=1,NDim(KPhase)
            HCondLn(i)=2*HCondMx(i)+1
            if(imax/j.lt.HCondLn(i)) then
              call FeChybne(-1.,-1.,'Zle je zle!!!',' ',SeriousError)
              go to 9999
            endif
            j=j*HCondLn(i)
          enddo
          if(allocated(HCondAr)) deallocate(HCondAr)
          allocate(HCondAr(NTwin,NRefAlloc))
        endif
      enddo
      ScSgTest=1.
      if(rim.gt.999999.9) then
2250    if(rim.gt.999999.9) then
          ScSgTest=ScSgTest*.1
          rim=rim*.1
          rsm=rsm*.1
          go to 2250
        endif
2280    if(rsm.gt.9999.9) then
          ScSgTest=ScSgTest*.1
          rsm=rsm*.1
          go to 2280
        endif
      endif
      if(ScSgTest.ne.1.) then
        do i=1,NRefRead
          riar(i)=riar(i)*ScSgTest
          rsar(i)=rsar(i)*ScSgTest
        enddo
      endif
      call FeMouseShape(0)
9999  return
      end
