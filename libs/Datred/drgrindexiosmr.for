      subroutine DRGrIndexIOSmr(Klic,FileName)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(3)
      integer ip(4)
      character*(*) FileName
      character*12 :: Id(11) = (/'ubmatrix  ',
     1                           'ubcrysalis',
     2                           'viewmatrix',
     3                           'celldir   ',
     4                           'cell      ',
     5                           'cellsu    ',
     6                           'qvec      ',
     7                           'qvecsu    ',
     8                           'cellvol   ',
     9                           'cellfor2d ',
     a                           'findcells '/),Nazev
      character*256 Veta
      equivalence (IdNumbers( 1),IdUBMatrix),
     1            (IdNumbers( 2),IdUBCrysalis),
     2            (IdNumbers( 3),IdViewMatrix),
     3            (IdNumbers( 4),IdCellDir),
     4            (IdNumbers( 5),IdCell),
     5            (IdNumbers( 6),IdCellSU),
     6            (IdNumbers( 7),IdQVec),
     7            (IdNumbers( 8),IdQVecSU),
     8            (IdNumbers( 9),IdCellVol),
     9            (IdNumbers(10),IdCellFor2d),
     a            (IdNumbers(11),IdFindCells)
      if(Klic.eq.0) then
        call UnitMat(UB(1,1,KRefBlock),3)
        call UnitMat(ViewMatrix,3)
        call SetRealArrayTo(CellRefBlock  (1,KRefBlock),6,-1.)
        call SetRealArrayTo(CellRefBlockSU(1,KRefBlock),6, 0.)
        call SetRealArrayTo(CellFor2d,6,-1.)
        call SetRealArrayTo(QuRefBlock  (1,1,KDatBlock),9, 0.)
        call SetRealArrayTo(QuRefBlockSU(1,1,KDatBlock),9, 0.)
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(Klic.eq.0) then
        nq=0
        nqs=0
        CellVolume=-9999.
1050    k=256
1100    if(k.ge.256) then
          read(ln,FormA,end=9999) Veta
          k=0
        endif
        call kus(Veta,k,Nazev)
        i=islovo(Nazev,id,11)
        if(i.eq.IdUBMatrix) then
          do i=1,3
            read(ln,FormA,end=9999) Veta
            k=0
            call StToReal(Veta,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9000
            do j=1,3
              UB(i,j,KRefBlock)=xp(j)
            enddo
          enddo
        else if(i.eq.IdViewMatrix) then
          do i=1,3
            read(ln,FormA,end=9999) Veta
            k=0
            call StToReal(Veta,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9000
            do j=1,3
              ViewMatrix(i,j)=xp(j)
            enddo
          enddo
        else if(i.eq.IdCellDir) then
          do i=1,3
            read(ln,FormA,end=9999) Veta
            k=0
            call StToReal(Veta,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9000
            do j=1,3
              CellDir(i,j)=xp(j)
            enddo
          enddo
        else if(i.eq.IdCell) then
          call StToReal(Veta,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdCellSU) then
          call StToReal(Veta,k,CellRefBlockSU(1,KRefBlock),6,.false.,
     1                  ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdQVec) then
          if(nq.lt.3) nq=nq+1
          call StToReal(Veta,k,QuRefBlock(1,nq,KRefBlock),3,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdQVecSU) then
          if(nqs.lt.3) nqs=nqs+1
          call StToReal(Veta,k,QuRefBlockSU(1,nqs,KRefBlock),3,.false.,
     1                  ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdCellVol) then
          call StToReal(Veta,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          CellVolume=xp(1)
        else if(i.eq.IdCellFor2d) then
          call StToReal(Veta,k,CellFor2d,6,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdFindCells) then
          call StToInt(Veta,k,ip,4,.false.,ich)
          if(ich.ne.0) go to 9000
          NTripl=ip(1)
          NRefForTripl=ip(2)
          NRefForIndex=ip(3)
          GrIndexNSel=ip(4)
          call StToReal(Veta,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9000
          VolMax=xp(1)
          VolAMin=xp(2)
          DiffMax=xp(3)
          do ITripl=1,NTripl
            PorTripl(ITripl)=ITripl
            read(ln,FormA,end=9999) Veta
            k=0
            call StToint(Veta,k,NIndSelTripl(i),1,.false.,ich)
            if(ich.ne.0) go to 1050
            do i=1,3
              read(ln,FormA,end=9999) Veta
              k=0
              call StToReal(Veta,k,xp,3,.false.,ich)
              if(ich.ne.0) go to 1050
              do j=1,3
                UBTripl(i,j,ITripl)=xp(j)
              enddo
            enddo
            call MatInv(UBTripl(1,1,ITripl),UBITripl(1,1,ITripl),pom,3)
            VTripl(ITripl)=1./pom
            read(ln,FormA,end=9999) Veta
            k=0
            call StToReal(Veta,k,CellTripl(1,ITripl),6,.false.,ich)
            if(ich.ne.0) go to 1050
          enddo
        endif
        go to 1050
      else
        write(ln,FormA) Id(IdUBMatrix)(:idel(Id(IdUBMatrix)))
        write(ln,100)((UB(i,j,KRefBlock),j=1,3),i=1,3)
        write(ln,FormA) Id(IdUBCrysalis)(:idel(Id(IdUBCrysalis)))
        write(ln,100)((UB(i,j,KRefBlock)*LamAveRefBlock(KRefBlock),
     1                 j=1,3),i=1,3)
        write(ln,FormA) Id(IdViewMatrix)(:idel(Id(IdViewMatrix)))
        write(ln,100)((ViewMatrix(i,j),j=1,3),i=1,3)
        write(ln,FormA) Id(IdCellDir)(:idel(Id(IdCellDir)))
        write(ln,100)((CellDir(i,j),j=1,3),i=1,3)
        write(Veta,101)(CellRefBlock(i,KRefBlock),i=1,6)
        k=0
        call WriteLabeledRecord(ln,Id(IdCell),Veta,k)
        write(Veta,'(f15.2)') CellVolume
        k=0
        call WriteLabeledRecord(ln,Id(IdCellVol),Veta,k)
        write(Veta,101)(CellRefBlockSU(i,KRefBlock),i=1,6)
        k=0
        call WriteLabeledRecord(ln,Id(IdCellSU),Veta,k)
        do i=1,NDimI(KPhase)
          write(Veta,101)(QuRefBlock(j,i,KRefBlock),j=1,3)
          k=0
          call WriteLabeledRecord(ln,Id(IdQVec),Veta,k)
          write(Veta,101)(QuRefBlockSU(j,i,KRefBlock),j=1,3)
          k=0
          call WriteLabeledRecord(ln,Id(IdQVecSU),Veta,k)
        enddo
        write(Veta,101) CellFor2d
        k=0
        call WriteLabeledRecord(ln,Id(IdCellFor2d),Veta,k)
        write(Veta,'(4i15,f15.3,2f15.6)') NTripl,NRefForTripl,
     1    NRefForIndex,GrIndexNSel,VolMax,VolAMin,DiffMax
        call ZdrcniCisla(Veta,7)
        k=0
        call WriteLabeledRecord(ln,Id(IdFindCells),Veta,k)
        do k=1,NTripl
          ITripl=PorTripl(k)
          write(ln,'(i5)') NIndSelTripl(ITripl)
          write(ln,100)((UBTripl(i,j,ITripl),j=1,3),i=1,3)
          write(ln,101)(CellTripl(i,ITripl),i=1,6)
        enddo
      endif
      go to 9999
9000  call FeChybne(-1.,-1.,'syntax problem during reading smr.',
     1              Veta,SeriousError)
      ErrFlag=1
9999  call CloseIfOpened(ln)
      if(ErrFlag.eq.0.and.Klic.eq.0) then
        NDimI(KPhase)=nq
        NDim(KPhase)=3+nq
        NDimQ(KPhase)=NDim(KPhase)**2
        call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),pom,3)
        if(CellVolume.lt.0.) CellVolume=1./pom
        call MatInv(CellDir,CellDirI,pom,3)
        call MatInv(ViewMatrix,ViewMatrixI,pom,3)
        call DRGrIndexGetQPer
      endif
      return
100   format(3f10.6)
101   format(3f10.4,3f10.3)
      end
