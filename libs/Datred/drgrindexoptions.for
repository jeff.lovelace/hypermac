      subroutine DRGrIndexOptions
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Veta,TmpFile
      integer RolMenuSelectedQuest,ColorIn
      logical CrwLogicQuest,UseSLimIn,UseILimIn
      real IMinIn,IMaxIn
      SMinIn=GrIndexSMin
      SMaxIn=GrIndexSMax
      IMinIn=GrIndexIMin
      IMaxIn=GrIndexIMax
      ColorIn=GrIndexColor
      UseSLimIn=GrIndexUseSLim
      UseILimIn=GrIndexUseILim
      IndexCoorSystemIn=IndexCoorSystem
      ich=0
      id=NextQuestId()
      xqd=300.
      il=15
      if(NDimI(KPhase).gt.0) il=il+1+NDimI(KPhase)
      call FeQuestCreate(id,XMaxBasWin-xqd,YMinBasWin,xqd,il,' ',0,
     1                   LightGray,-1,-1)
      il=1
      tpom=5.
      dpom=50.
      Veta='%Radius'
      xpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRad=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,GrIndexRad,.false.,.false.)
      il=il+1
      Veta='Co%lor'
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom+30.,
     1                        EdwYd,0)
      nRolMenuColor=RolMenuLastMade
      do j=2,8
        if(GrIndexColor.eq.ColorNumbers(j)) then
          k=j-1
          go to 1210
        endif
      enddo
      k=5
1210  call FeQuestRolMenuOpen(RolMenuLastMade,ColorNames(2),7,k)
      il=il+1
      xpomp=5.
      tpomp=xpomp+CrwXd+10.
      Veta='Skip indexed relections'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwSkipIndexed=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,GrIndexSkipIndexed)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Apply %intensity limits'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseIlim=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,GrIndexUseILim)
      il=il+1
      Veta='Minimal:'
      tpom=15.
      dpom=80.
      do j=1,2
        do i=1,2
          ilp=il+(i-1)*3
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          call FeQuestEdwMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,
     1                        0)
          if(j.eq.1) then
            if(i.eq.1) then
              nEdwIMin=EdwLastMade
              pom=GrIndexIMin
            else
              nEdwSMin=EdwLastMade
              pom=GrIndexSMin
            endif
          else
            if(i.eq.1) then
              nEdwIMax=EdwLastMade
              pom=GrIndexIMax
            else
              nEdwSMax=EdwLastMade
              pom=GrIndexSMax
            endif
          endif
          call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        enddo
        Veta='Maximal:'
        tpom=tpom+xqd*.5
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Apply %sin(theta)/lambda limits'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseSLim=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,GrIndexUseSLim)
      il=il+1
      tpom=5.
      xpom=120.
      dpomc=100.
      wpom=50.
      dpom=15.
      xpom=xqd*.5-wpom*1.5-dpom
      ypom=12.
      Veta='%OK'
      do i=1,3
        if(i.eq.2) then
          Veta='%Cancel'
          nButtOK=ButtonLastMade
        else if(i.eq.3) then
          Veta='%Apply'
          nButtCancel=ButtonLastMade
        endif
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+wpom
      enddo
      nButtApply=ButtonLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Display coordinate system'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwDisplayCoorSyst=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,IndexCoorSystem.gt.0)
      Veta='La%boratory system'
      xpom=25.
      tpom=xpom+CrwgXd+5.
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwCoorSystFirst=CrwLastMade
          Veta='Reci%procal coordinate system'
        else if(i.eq.2) then
          Veta='%Direct coordinate system'
        endif
      enddo
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        tpom1=5.
        dpom=50.
        do i=1,NDimI(KPhase)
          il=il+1
          Veta=Indices(i+3)//'(min):'
          if(i.eq.1) xpom1=tpom1+FeTxLength(Veta)+5.
          call FeQuestEudMake(id,tpom1,il,xpom1,il,Veta,'L',dpom,EdwYd,
     1                        0)
          call FeQuestIntEdwOpen(EdwLastMade,MMin(i),.false.)
          call FeQuestEudOpen(EdwLastMade,-4,0,1,0.,0.,0.)
          Veta=Indices(i+3)//'(max):'
          if(i.eq.1) then
            nEdwMM=EdwLastMade
            tpom2=xpom1+dpom+50.
            xpom2=tpom2+FeTxLength(Veta)+5.
          endif
          call FeQuestEudMake(id,tpom2,il,xpom2,il,Veta,'L',dpom,EdwYd,
     1                        0)
          call FeQuestIntEdwOpen(EdwLastMade,MMax(i),.false.)
          call FeQuestEudOpen(EdwLastMade,0,4,1,0.,0.,0.)
        enddo
      else
        nEdwMM=0
      endif
1450  if(GrIndexUseILim) then
        call FeQuestRealEdwOpen(nEdwIMin,GrIndexIMin,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwIMax,GrIndexIMax,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwIMin)
        call FeQuestEdwClose(nEdwIMax)
      endif
      if(GrIndexUseSLim) then
        call FeQuestRealEdwOpen(nEdwSMin,GrIndexSMin,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwSMax,GrIndexSMax,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwSMin)
        call FeQuestEdwClose(nEdwSMax)
      endif
      nCrw=nCrwCoorSystFirst
      do i=1,3
        if(IndexCoorSystem.gt.0) then
          call FeQuestCrwOpen(nCrw,IndexCoorSystem.eq.i)
          if(NCell.lt.3.and.i.gt.1) call FeQuestCrwDisable(nCrw)
        else
          call FeQuestCrwClose(nCrw)
        endif
        nCrw=nCrw+1
      enddo
1500  call FeQuestEvent(id,ich)
      GrIndexColor=ColorNumbers(RolMenuSelectedQuest(nRolMenuColor)+1)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtApply.or.CheckNumber.eq.nButtOK)) then
        call FeQuestRealFromEdw(nEdwRad,GrIndexRad)
        GrIndexSkipIndexed=CrwLogicQuest(nCrwSkipIndexed)
        if(GrIndexUseILim) then
          call FeQuestRealFromEdw(nEdwIMin,GrIndexIMin)
          call FeQuestRealFromEdw(nEdwIMax,GrIndexIMax)
        endif
        if(GrIndexUseSLim) then
          call FeQuestRealFromEdw(nEdwSMin,GrIndexSMin)
          call FeQuestRealFromEdw(nEdwSMax,GrIndexSMax)
        endif
        if(CrwLogicQuest(nCrwDisplayCoorSyst)) then
          nCrw=nCrwCoorSystFirst
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              IndexCoorSystem=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
        if(nEdwMM.gt.0) then
          nEdw=nEdwMM
          do i=1,NDimI(KPhase)
            call FeQuestIntAFromEdw(nEdw,MMin(i))
            nEdw=nEdw+1
            call FeQuestIntAFromEdw(nEdw,MMax(i))
            nEdw=nEdw+1
          enddo
        endif
        TmpFile='jtmp'
        if(OpSystem.le.0) then
          call CreateTmpFile(TmpFile,i,1)
        else
          call CreateTmpFileName(TmpFile,id)
        endif
        call FeSaveImage(QuestXMin(id)-FrameWidth,
     1                   QuestXMax(id)+FrameWidth,
     2                   QuestYMin(id)-FrameWidth,
     3                   QuestYMax(id)+FrameWidth,TmpFile)
        call FeClearGrWin
        call DRGrIndexPlot
        call FeLoadImage(QuestXMin(id)-FrameWidth,
     1                   QuestXMax(id)+FrameWidth,
     2                   QuestYMin(id)-FrameWidth,
     3                   QuestYMax(id)+FrameWidth,TmpFile,0)
        if(CheckNumber.eq.nButtOK) then
          call FeQuestRemove(id)
          go to 9999
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCancel)
     1  then
        if(UseSLimIn.neqv.GrIndexUseSLim.or.
     1     UseILimIn.neqv.GrIndexUseILim.or.
     2     SMinIn.ne.GrIndexSMin.or.SMaxIn.ne.GrIndexSMax.or.
     3     IMinIn.ne.GrIndexIMin.or.IMaxIn.ne.GrIndexIMax.or.
     4     ColorIn.ne.GrIndexColor) then
          GrIndexSMin=SMinIn
          GrIndexSMax=SMaxIn
          GrIndexIMin=IMinIn
          GrIndexIMax=IMaxIn
          GrIndexColor=ColorIn
          GrIndexUseSLim=UseSLimIn
          GrIndexUseILim=UseILimIn
          IndexCoorSystem=IndexCoorSystemIn
        endif
        call FeQuestRemove(id)
        go to 9999
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseILim) then
        GrIndexUseILim=CrwLogicQuest(CheckNumber)
        go to 1450
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseSLim) then
        GrIndexUseSLim=CrwLogicQuest(CheckNumber)
        go to 1450
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwDisplayCoorSyst) then
        if(CrwLogicQuest(CheckNumber)) then
          IndexCoorSystem=1
        else
          IndexCoorSystem=0
        endif
        go to 1450
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
9999  call FeClearGrWin
      call DRGrIndexPlot
      return
      end
