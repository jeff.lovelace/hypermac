      subroutine DRGrIndexFindCellOpt(VolMax,VolAMin,DiffMax,
     1                                NRefForIndex,NRefForTripl,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      id=NextQuestId()
      xqd=300.
      il=5
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Define parameters for '//
     1                   'indexation',0,LightGray,0,0)
      Veta='%Number of strongest reflections to index:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+35.
      dpom=60.
      do il=1,5
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(il.eq.1) then
          ipom=NRefForIndex
          Veta='N%umber of reflections for triplet search:'
        else if(il.eq.2) then
          ipom=NRefForTripl
          Veta='AngVolume(%min):'
        else if(il.eq.3) then
          pom=VolAMin
          Veta='Volume(ma%x)'
        else if(il.eq.4) then
          Veta='M%aximal d* difference in Ang**-1 for indexing:'
          pom=VolMax
        else if(il.eq.5) then
          pom=DiffMax
        endif
        if(il.le.2) then
          call FeQuestIntEdwOpen(EdwLastMade,ipom,.false.)
        else
          call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        endif
        if(il.eq.1) then
          nEdwRefForIndex=EdwLastMade
        else if(il.eq.2) then
          nEdwRefForTripl=EdwLastMade
        else if(il.eq.3) then
          nEdwMinAngVol=EdwLastMade
        else if(il.eq.4) then
          nEdwMaxVol=EdwLastMade
        else if(il.eq.5) then
          nEdwDiffMax=EdwLastMade
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwRefForIndex,NRefForIndex)
        call FeQuestIntFromEdw(nEdwRefForTripl,NRefForTripl)
        call FeQuestRealFromEdw(nEdwMinAngVol,VolAMin)
        call FeQuestRealFromEdw(nEdwMaxVol,VolMax)
        call FeQuestRealFromEdw(nEdwDiffMax,DiffMax)
      endif
      call FeQuestRemove(id)
      return
      end
