      subroutine DRAbsCorrRun(ich)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real ab(19),flm(81),ylm1(81),ylm2(81),xp(3),xr(3)
      character*80 t80
      integer Exponent10
      logical ExistFile
      ich=0
      if(CorrAbsNew.eq.0.and.CorrAbs(KRefBlock).eq.0) go to 9999
      if(mod(CorrAbsNew,10).eq.0) then
        t80=' - none'
      else if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
        Volg=1.333333*pi*RadiusRefBlock(KRefBlock)**3*1.e-3/
     1       CellVol(1,KPhase)
        rmi=AmiUsed*RadiusRefBlock(KRefBlock)
        pom=rmi*10.+1.
        n=pom
        pom=pom-float(n)
        if(n.gt.100) then
          call FeChybne(-1.,-1.,'too high mi*r for spherical sample',
     1                  'data will not be corrected.',SeriousError)
          go to 9999
        endif
        do j=1,19
          ab(j)=abskou(j,n)+pom*(abskou(j,n+1)-abskou(j,n))
        enddo
        t80=' - spherical sample'
      else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
        if(NFaces(KRefBlock).le.3) then
          call FeChybne(-1.,-1.,'number of faces is not sufficient '//
     1                  'to make the correction.',' ',SeriousError)
          go to 9999
        endif
        t80=' - general shape'
      endif
      if(CorrAbsNew/10.eq.1) then
        t80=t80(:idel(t80))//'+empirical/frame scaling'
      endif
      t80='Absorption correction'//t80
      if(ExistFile(RefBlockFileName)) then
        call OpenFile(95,RefBlockFileName,'formatted','old')
      else
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
      endif
      if(ErrFlag.ne.0) go to 9999
      call OpenFile(96,fln(:ifln)//'.l97','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call FeFlowChartOpen(-1.,-1.,
     1  max(nint(float(nref95(KRefBlock))*.005),10),nref95(KRefBlock),
     2  t80,' ',' ')
      if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
        NFacesR=NFaces(KRefBlock)
        do k=1,NFacesR
          if(UseTrRefBlock(KRefBlock)) then
            call MultM(DFace(1,k,KRefBlock),TrRefBlock(1,KRefBlock),
     1                 DFaceR(1,k),1,3,3)
          else
            call CopyVek(DFace(1,k,KRefBlock),DFaceR(1,k),3)
          endif
          DFaceR(4,k)=DFace(4,k,KRefBlock)
        enddo
        call grit
      endif
      abmax=-1.
      abmin=1.e20
      iz=0
3000  call DRGetReflectionFromM95(95,iend,ich)
      if(ich.ne.0) go to 9000
      if(iend.ne.0) go to 4000
      call FeFlowChartEvent(iz,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) go to 9999
      endif
      if(CorrAbsNew.ne.0) then
        sinchi=sin(chi*ToRad)
        sinom=sin((omega-theta)*ToRad)
      else
        Corrf(2)=1.
      endif
      do i=1,3
        sod(i)=dircos(i,1)*rcp(i,1,KPhase)
        sd(i) =dircos(i,2)*rcp(i,1,KPhase)
      enddo
      if(mod(CorrAbsNew,10).eq.IdCorrAbsSphere) then
        d1=.2*theta+1.
        j=d1
        d1=d1-float(j)
        corrf(2)=ab(j)+d1*(ab(j+1)-ab(j))
      else if(mod(CorrAbsNew,10).eq.IdCorrAbsGauss) then
        call DRLength2
      endif
      if(CorrAbsNew/10.eq.1) then
        sod=-sod
        call MultM(TrToOrtho(1,1,1),sod,xp,3,3,1)
        call MultM(TrToOrtho(1,1,1),sd ,xr,3,3,1)
        call SpherHarm(xp,ylm1,8,3)
        call SpherHarm(xr,ylm2,8,3)
        j=0
        i=1
        do l=1,8
          i=l**2
          if(mod(l,2).eq.1) then
            if(l.gt.SpHOdd(KRefBlock)) cycle
          else
            if(l.gt.SpHEven(KRefBlock)) cycle
          endif
          do k=-l,l
            i=i+1
            j=j+1
            flm(j)=.5*(ylm1(i)+ylm2(i))
          enddo
        enddo
        if(SEType.eq.SESpHarm.or.SEType.eq.SEAll) then
          Corr2=SpHAlm(1,KRefBlock)+
     1          VecOrtScal(SpHAlm(2,KRefBlock),flm,SpHN(KRefBlock)-1)
        else
          Corr2=1.
        endif
        Corrf(2)=Corrf(2)*Corr2
      endif
      abmin=min(abmin,1./Corrf(2))
      abmax=max(abmax,1./Corrf(2))
3500  call DRPutReflectionToM95(96,nl)
      go to 3000
4000  call CloseIfOpened(95)
      call CloseIfOpened(96)
      call MoveFile(fln(:ifln)//'.l97',RefBlockFileName)
      call FeFlowChartRemove
      write(TextInfo(1),'(''  T(min) : '',f7.4,''  T(max) : '',f7.4)')
     1      abmin,abmax
      if(CorrAbsNew.eq.0) then
        NInfo=1
      else
        Ninfo=2
        pom=volg*1.e3*CellVol(1,KPhase)
        i=max(6-Exponent10(pom),0)
        write(t80,'(''(f12.'',i1,'')'')') min(i,8)
        write(TextInfo(2),t80) pom
        TextInfo(2)='   Crystal volume :'//TextInfo(2)(:10)//' mm**3'
      endif
      call FeInfoOut(-1.,-1.,'Transmission extremes','L')
      go to 9999
9000  call FeReadError(95)
9999  call DeleteFile(fln(:ifln)//'.l97')
      call CloseIfOpened(95)
      call CloseIfOpened(89)
      return
      end
