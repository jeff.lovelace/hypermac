      subroutine DRUpdateM95
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      ich=0
      call OpenFile(95,fln(:ifln)//'.m95','formatted','old')
      if(ErrFlag.ne.0) then
        ich=1
        go to 2000
      endif
      call OpenFile(96,fln(:ifln)//'.l95','formatted','unknown')
      NRef=0
1000  call DRGetReflectionFromM95(95,iend,ich)
      if(ich.ne.0.or.iend.ne.0) go to 2000
      NRef=NRef+1
      do i=1,NNew
        if(NRefNew(i).eq.NRef) then
          call CopyVekI(IProfNew(1,i),IProf(1,2),NProf)
          go to 1200
        endif
      enddo
1200  call DRPutReflectionToM95(96,nl)
      go to 1000
2000  call CloseIfOpened(95)
      call CloseIfOpened(96)
      if(ich.ne.0) then
        call DeleteFile(fln(:ifln)//'.l95')
      else
        call MoveFile(fln(:ifln)//'.l95',fln(:ifln)//'.m95')
      endif
      return
      end
