      subroutine DRGetReflectionFromM95(ln,iend,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      character*256 t256
      character*20  FormProf
      logical EqIgCase,eqiv
      data FormProf/'(''  prof1'',15i6)'/
      iend=0
      ich=0
      read(ln,FormA,end=2000,err=3000) t256
      k=0
      call kus(t256,k,Cislo)
      if(EqIgCase(Cislo(1:4),'data')) go to 2000
      backspace ln
      read(ln,format95,end=2000,err=3000)
     1  no,(ih(i),i=1,maxNDim),uhly,ri,rs,expos,iflg,corrf,tbar,
     2  dircos,DRLam,RunCCD,CorrAbsAppl,CorrScaleAppl
      if(CorrAbsAppl.le.0.) CorrAbsAppl=1.
      if(CorrScaleAppl.le.0.) CorrScaleAppl=1.
      if(UseExpCorr(KRefBlock).eq.0) then
        pom=CorrAbsAppl*CorrScaleAppl
        if(pom.gt.0.) then
          ri=ri/pom
          rs=rs/pom
        endif
      endif
      do i=1,2
        if(iflg(i).eq.0) iflg(i)=1
      enddo
      read(ln,FormA,end=1500,err=3000) t256
      k=0
      call kus(t256,k,Cislo)
      if(EqIgCase(Cislo(1:5),'prof0')) then
        read(t256,100) tmn,tmx,omn,omx,rych,NProf,NBckg,KProf
        if(KProf.le.0) then
          KProf=NBckg
          NBckg=0
        endif
        do k=1,KProf
          read(ln,101)(IProf(i,k),i=1,NProf)
        enddo
        if(KProf.eq.1) call CopyVekI(IProf(1,1),IProf(1,2),NProf)
      else
        go to 1500
      endif
      go to 9999
1500  backspace ln
      go to 9999
      entry DRPutReflectionToM95(ln,nl)
      if(NProf.gt.0) then
        if(eqiv(IProf(1,1),IProf(1,2),NProf)) then
          KProf=1
        else
          KProf=2
        endif
        if(IntFromProfile) then
          do i=1,NProf
            py(i,KProf)=IProf(i,KProf)
          enddo
          call BPB(ri,rs,KProf,NBckg)
        endif
      endif
      if(CorrAbsAppl.le.0.) CorrAbsAppl=1.
      if(CorrScaleAppl.le.0.) CorrScaleAppl=1.
      if(UseExpCorr(KRefBlock).eq.0) then
        pom=CorrAbsAppl*CorrScaleAppl
        if(pom.gt.0.) then
          ri=ri*pom
          rs=rs*pom
        endif
      endif
      write(ln,format95) no,(ih(i),i=1,maxNDim),uhly,ri,rs,expos,
     1                   iflg,corrf,tbar,dircos,DRLam,RunCCD,
     2                   CorrAbsAppl,CorrScaleAppl
      nl=2
      if(KProf.gt.0) then
        write(ln,102) tmn,tmx,omn,omx,rych,NProf,NBckg,KProf
        nl=nl+1
        do k=1,KProf
          write(FormProf(9:9),'(i1)') k
          write(ln,FormProf)(IProf(i,k),i=1,NProf)
          nl=nl+(NProf-1)/15+1
        enddo
      endif
      go to 9999
2000  iend=1
      go to 9999
3000  ich=1
9999  return
100   format(8x,5f7.2,3i5)
101   format(8x,15i6)
102   format('  prof0',5f7.2,3i5)
      return
      end
