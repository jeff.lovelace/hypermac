      subroutine AngleInSteps(A,AS)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension A(4)
      integer AS(4)
      if(abs(A(1)).gt.pi) A(1)=A(1)-sign(pi2,A(1))
      if(A(4).lt.0..or.A(4).ge.pi2) A(4)=A(4)-sign(pi2,A(4))
      do i=1,3
        AS(i)=nint(A(i)*StepKuma(i))
      enddo
      AS(4)=InPhi(A(4)/ToRad)
      return
      end
