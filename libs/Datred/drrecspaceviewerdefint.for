      subroutine DRRecSpaceViewerDefInt
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension MainCol(3)
      character*80 Veta,TmpFile
      character*40 HKLString(2),LblNetDirString(2)
      integer RolMenuSelectedQuest,SatCol,PlusCol,RolMenuStateQuest,
     1        UnobsCol,TwinCol(3),CrwStateQuest,ihh(6),nRolMenuTwin,
     2        nLblNetDir(2),nEdwNet(2,2),EdwStateQuest
      logical CrwLogicQuest
      real h(3),hh(3)
      SimDepthOld=SimDepth
      ich=0
      id=NextQuestId()
      xqd=330.
      if(SimType.eq.SimDiffFSig) then
        il=10
      else
        if(NDimI(KPhase).gt.0) then
          il=12+NComp(KPhase)
        else
          il=10
        endif
      endif
      if(NTwin.gt.1) then
        il=il+1
        if(NDimI(KPhase).le.0) then
          il=il+min(NTwin,3)-2
        endif
      else
        SimDrawTwins=.false.
      endif
      call FeQuestCreate(id,XMaxBasWin-xqd,YMinBasWin,xqd,il,' ',0,
     1                   LightGray,-1,-1)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+10.
      Veta='Use scaled %radia'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwScale=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SimUseScaledRad)
      xpom=tpom+FeTxLengthUnder(Veta)+18.
      dpom=50.
      tpom=xpom+dpom+10.
      Veta='Ma%x.radius'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRad=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,SimRadMax,.false.,.false.)
      il=il+1
      Veta='S%ensitivity limit in %'
      tpom=5.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSensitivity=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,SimValRed,.false.,.false.)
      call FeQuestEudOpen(EdwLastMade,0,0,0,0.,100.,5.)
      xpom=5.
      tpom=xpom+CrwXd+10.
      il=il+1
      Veta='Use color s%hading'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwShade=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SimUseShading)
      if(NDimI(KPhase).gt.0.and.SimType.ne.SimDiffFSig) then
        il=il+1
        Veta='Draw sa%tellites'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwDrawSat=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SimDrawSat)
        il=il+1
        Veta='Pro%ject associated sattelites to the plane of main '//
     1       'reflections'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwProjectSat=CrwLastMade
      else
        nCrwDrawSat=0
        nCrwProjectSat=0
      endif
      if(SimType.ne.SimDiffFSig) then
        il=il+1
        Veta='Draw %unobserved'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwDrawUnobs=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SimDrawUnobs)
      else
        nCrwDrawUnobs=0
      endif
      il=il+1
      Veta='Use %default net'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwDefaultNet=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SimUseDefaultNet)
      do j=1,2
        do i=1,3
          ihh(i)=Nint(SimTr(j,i))
        enddo
        call MakeHKLString(ihh,3,HKLString(j))
      enddo
      xpomp=xpom+20.
      dpome=50.
      do i=1,2
        il=il+1
        LblNetDirString(i)=SmbX(i)//'-net-direction'
        Veta=LblNetDirString(i)(:idel(LblNetDirString(i)))//' ='
        call FeQuestLblMake(id,xpomp,il,Veta,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLblNetDir(i)=LblLastMade
        if(i.eq.1) then
          xpome1=xpomp+FeTxLength(Veta)+5.
          tpome1=xpome1+dpome+5.
        endif
        Veta='*'//HKLString(1)(:idel(HKLString(1)))//' +'
        call FeQuestEdwMake(id,tpome1,il,xpome1,il,Veta,'L',dpome,EdwYd,
     1                      0)
        nEdwNet(1,i)=EdwLastMade
        if(i.eq.1) then
          xpome2=tpome1+FeTxLength(Veta)+5.
          tpome2=xpome2+dpome+5.
        endif
        Veta='*'//HKLString(2)(:idel(HKLString(2)))
        call FeQuestEdwMake(id,tpome2,il,xpome2,il,Veta,'L',dpome,EdwYd,
     1                      0)
        nEdwNet(2,i)=EdwLastMade
      enddo
      if(NTwin.gt.1) then
        il=il+1
        Veta='Draw all t%wins'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwDrawTwins=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SimDrawTwins)
      else
        nCrwDrawTwins=0
      endif
      tpom=5.
      xpom=120.
      dpomc=100.
      nRolMenuMain=0
      nRolMenuPlus=0
      nRolMenuMinus=0
      nRolMenuSat=0
      nRolMenuUnobs=0
      if(SimType.ne.SimDiffFSig) then
        ik=NComp(KPhase)+2
        ip=NComp(KPhase)
      else
        ik=3
        ip=1
      endif
      ilp=il
      do i=1,ik
        if(i.le.ip) then
          if(SimType.ne.SimDiffFSig) then
            if(NDimI(KPhase).gt.0) then
              if(NComp(KPhase).eq.1) then
                Veta='%Main:'
              else
                write(Veta,'(''Main subsystem#%'',i1,'':'')') i
              endif
            else
              Veta='O%bserved:'
            endif
            icol=SimColorMain(i)
          else
            Veta='%Positive:'
            icol=SimColorPlus
          endif
        else if(i.eq.ip+1) then
          if(SimType.ne.SimDiffFSig) then
            if(NDimI(KPhase).gt.0) then
              Veta='%Satellites:'
              icol=SimColorSat
            else
              nRolMenuSat=0
              cycle
            endif
          else
            Veta='%Negative:'
            icol=SimColorMinus
            nRolMenuSat=0
          endif
        else
          if(SimType.ne.SimDiffFSig) then
            Veta='U%nobserved:'
            icol=SimColorUnobs
          else
            nRolMenuUnobs=0
            cycle
          endif
        endif
        do j=2,8
          if(icol.eq.ColorNumbers(j)) then
            k=j-1
            go to 1210
          endif
        enddo
        k=1
1210    il=il+1
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpomc,EdwYd,
     1                          0)
        if(i.le.ip) then
          if(SimType.ne.SimDiffFSig) then
            if(i.eq.1) nRolMenuMain=RolMenuLastMade
            nRolMenuPlus=0
            MainCol(i)=k
          else
            nRolMenuPlus=RolMenuLastMade
            nRolMenuMain=0
            PlusCol=k
          endif
          call FeQuestRolMenuOpen(RolMenuLastMade,ColorNames(2),11,k)
        else if(i.eq.ip+1) then
          if(SimType.ne.SimDiffFSig) then
            nRolMenuSat=RolMenuLastMade
            nRolMenuMinus=0
            SatCol=k
          else
            nRolMenuMinus=RolMenuLastMade
            nRolMenuSat=0
            call FeQuestRolMenuOpen(RolMenuLastMade,ColorNames(2),11,k)
            MinusCol=k
          endif
        else
          nRolMenuUnobs=RolMenuLastMade
          UnobsCol=k
        endif
      enddo
      nRolMenuTwin=0
      il=ilp
      if(NTwin.gt.1) then
        do i=1,3
          write(Veta,'(''Twin#%'',i1)') i
          il=il+1
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpomc,
     1                            EdwYd,0)
          if(i.eq.1) nRolMenuTwin=RolMenuLastMade
          do j=2,8
            if(SimColorTwin(i).eq.ColorNumbers(j)) then
              TwinCol(i)=j-1
              go to 1220
            endif
          enddo
          TwinCol(i)=1
1220    enddo
      endif
      wpom=50.
      dpom=15.
      xpom=xqd*.5-wpom*1.5-dpom
      ypom=12.
      Veta='%OK'
      do i=1,3
        if(i.eq.2) then
          Veta='%Cancel'
          nButtOK=ButtonLastMade
        else if(i.eq.3) then
          Veta='%Apply'
          nButtCancel=ButtonLastMade
        endif
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+wpom
      enddo
      nButtApply=ButtonLastMade
1400  if(nCrwDrawSat.gt.0) then
        if(CrwLogicQuest(nCrwDrawSat)) then
          if(CrwStateQuest(nCrwProjectSat).ne.CrwOn.and.
     1       CrwStateQuest(nCrwProjectSat).ne.CrwOff)
     2      call FeQuestCrwOpen(nCrwProjectSat,SimProjectSat)
        else
          call FeQuestCrwDisable(nCrwProjectSat,SimProject)
        endif
      endif
      if(SimDrawTwins) then
        if(nRolMenuMain.gt.0) then
          nRolMenu=nRolMenuMain
          do i=1,NComp(KPhase)
            call FeQuestRolMenuClose(nRolMenu)
            nRolMenu=nRolMenu+1
          enddo
        endif
        if(nRolMenuSat.gt.0) call FeQuestRolMenuClose(nRolMenuSat)
        if(nRolMenuUnobs.gt.0) call FeQuestRolMenuClose(nRolMenuUnobs)
        if(nRolMenuPlus.gt.0) call FeQuestRolMenuClose(nRolMenuPlus)
        if(nRolMenuMinus.gt.0) call FeQuestRolMenuClose(nRolMenuMinus)
        nRolMenu=nRolMenuTwin
        do i=1,NTwin
          call FeQuestRolMenuOpen(nRolMenu,ColorNames(2),11,TwinCol(i))
          nRolMenu=nRolMenu+1
        enddo
      else
        nRolMenu=nRolMenuTwin
        do i=1,NTwin
          call FeQuestRolMenuClose(nRolMenu)
          nRolMenu=nRolMenu+1
        enddo
        if(nCrwDrawSat.gt.0) then
          if(CrwLogicQuest(nCrwDrawSat)) then
            if(RolMenuStateQuest(nRolMenuSat).ne.RolMenuOpened)
     1        call FeQuestRolMenuOpen(nRolMenuSat,ColorNames(2),11,
     2                                SatCol)
          else
            call FeQuestRolMenuDisable(nRolMenuSat)
            call FeQuestCrwDisable(nCrwProjectSat,SimProject)
          endif
        endif
        if(nCrwDrawUnobs.gt.0) then
          if(CrwLogicQuest(nCrwDrawUnobs)) then
            if(RolMenuStateQuest(nRolMenuUnobs).ne.RolMenuOpened)
     1        call FeQuestRolMenuOpen(nRolMenuUnobs,ColorNames(2),11,
     2                                UnobsCol)
          else
            call FeQuestRolMenuClose(nRolMenuUnobs)
          endif
        endif
        if(nRolMenuMain.gt.0) then
          nRolMenu=nRolMenuMain
          do i=1,NComp(KPhase)
            call FeQuestRolMenuClose(nRolMenu)
            if(RolMenuStateQuest(nRolMenu).ne.RolMenuOpened)
     1        call FeQuestRolMenuOpen(nRolMenu,ColorNames(2),11,
     2                                MainCol(i))
            nRolMenu=nRolMenu+1
          enddo
        endif
      endif
1450  do i=1,2
        if(SimUseDefaultNet) then
          if(EdwStateQuest(nEdwNet(1,i)).eq.EdwOpened) then
            do j=1,2
              call FeQuestRealFromEdw(nEdwNet(j,i),SimNet(j,i))
              call FeQuestEdwClose(nEdwNet(j,i))
            enddo
          endif
          Veta=LblNetDirString(i)(:idel(LblNetDirString(i)))//': '//
     1         HKLString(i)
          call FeQuestLblChange(nLblNetDir(i),Veta)
        else
          Veta=LblNetDirString(i)(:idel(LblNetDirString(i)))//' ='
          call FeQuestLblChange(nLblNetDir(i),Veta)
          do j=1,2
            call FeQuestRealEdwOpen(nEdwNet(j,i),SimNet(j,i),.false.,
     1                              .false.)
          enddo
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(NDimI(KPhase).gt.0.and.SimType.ne.SimDiffFSig) then
        if(RolMenuStateQuest(nRolMenuSat).eq.RolMenuOpened.and.
     1     CrwLogicQuest(nCrwDrawSat))
     2    SatCol=RolMenuSelectedQuest(nRolMenuSat)
      endif
      if(nCrwDrawUnobs.gt.0) then
        if(RolMenuStateQuest(nRolMenuUnobs).eq.RolMenuOpened.and.
     1     CrwLogicQuest(nCrwDrawUnobs))
     2    UnobsCol=RolMenuSelectedQuest(nRolMenuUnobs)
      endif
      if(nRolMenuMain.gt.0) then
        nRolMenu=nRolMenuMain
        if(RolMenuStateQuest(nRolMenu).eq.RolMenuOpened) then
          do i=1,NComp(KPhase)
            MainCol(i)=RolMenuSelectedQuest(nRolMenu)
            nRolMenu=nRolMenu+1
          enddo
        endif
      endif
      if(nRolMenuPlus.gt.0) then
        if(RolMenuStateQuest(nRolMenuPlus).eq.RolMenuOpened) then
          PlusCol=RolMenuSelectedQuest(nRolMenuPlus)
          MinusCol=RolMenuSelectedQuest(nRolMenuMinus)
        endif
      endif
      if(nRolMenuTwin.gt.0) then
        nRolMenu=nRolMenuTwin
        if(RolMenuStateQuest(nRolMenu).eq.RolMenuOpened) then
          do i=1,NTwin
            TwinCol(i)=RolMenuSelectedQuest(nRolMenu)
            nRolMenu=nRolMenu+1
          enddo
        endif
      endif
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtApply.or.CheckNumber.eq.nButtOK)) then
        if(nCrwDrawSat.gt.0) SimDrawSat=CrwLogicQuest(nCrwDrawSat)
        if(nCrwProjectSat.gt.0) then
          SimProjectSat=CrwLogicQuest(nCrwProjectSat)
          SimDepthOld=SimDepth
          SimDepth=0.
        else
          SimProjectSat=.false.
          SimDepth=SimDepthOld
        endif
        if(.not.SimUseDefaultNet) then
          do i=1,2
            do j=1,2
              call FeQuestRealFromEdw(nEdwNet(j,i),SimNet(j,i))
            enddo
          enddo
        endif
        if(nCrwDrawTwins.gt.0) SimDrawTwins=CrwLogicQuest(nCrwDrawTwins)
        if(SimDrawTwins) then
          do i=1,NTwin
            SimColorTwin(i)=ColorNumbers(TwinCol(i)+1)
          enddo
        else
          if(nRolMenuPlus.gt.0) then
            SimColorPlus=ColorNumbers(PlusCol+1)
            SimColorMinus=ColorNumbers(MinusCol+1)
          else
            if(nRolMenuMain.gt.0) then
              do i=1,NComp(KPhase)
                SimColorMain(i)=ColorNumbers(MainCol(i)+1)
              enddo
            endif
            if(nRolMenuSat.gt.0) SimColorSat=ColorNumbers(SatCol+1)
          endif
        endif
        if(NDimI(KPhase).gt.0) then
          SimLayer=SimMainLayer
          if(.not.SimProjectSat) then
            ihh=0
            call CopyVekI(SimSatLayer,ihh(4),NDimI(KPhase))
            do j=1,3
              h(j)=ihh(j)
              do k=1,NDimI(KPhase)
                h(j)=h(j)+float(ihh(k+3))*qu(j,k,1,KPhase)
              enddo
            enddo
            call multm(h,SimTrI,hh,1,3,3)
            SimLayer=SimLayer+hh(3)
          endif
        endif
        if(nCrwDrawUnobs.gt.0) then
          SimColorUnobs=ColorNumbers(UnobsCol+1)
          SimDrawUnobs=CrwLogicQuest(nCrwDrawUnobs)
        endif
        SimUseShading=CrwLogicQuest(nCrwShade)
        SimUseScaledRad=CrwLogicQuest(nCrwScale)
        call FeQuestRealFromEdw(nEdwRad,SimRadMax)
        call FeQuestRealFromEdw(nEdwSensitivity,SimValRed)
        TmpFile='jtmp'
        if(OpSystem.le.0) then
          call CreateTmpFile(TmpFile,i,1)
        else
          call CreateTmpFileName(TmpFile,id)
        endif
        call FeSaveImage(QuestXMin(id)-FrameWidth,
     1                   QuestXMax(id)+FrameWidth,
     2                   QuestYMin(id)-FrameWidth,
     3                   QuestYMax(id)+FrameWidth,TmpFile)
        call FeClearGrWin
        call DRRecSpaceViewerOsnova
        call DRRecSpaceViewerSit
        call FeLoadImage(QuestXMin(id)-FrameWidth,
     1                   QuestXMax(id)+FrameWidth,
     2                   QuestYMin(id)-FrameWidth,
     3                   QuestYMax(id)+FrameWidth,TmpFile,0)
        if(CheckNumber.eq.nButtOK) then
          call FeQuestRemove(id)
          go to 9999
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCancel)
     1  then
        call FeQuestRemove(id)
        go to 9999
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwDrawSat.or.
     2         CheckNumber.eq.nCrwDrawUnobs)) then
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawTwins)
     1  then
        SimDrawTwins=CrwLogicQuest(nCrwDrawTwins)
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDefaultNet)
     1  then
        SimUseDefaultNet=CrwLogicQuest(nCrwDefaultNet)
        go to 1450
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
9999  return
      end
