      subroutine DRLength2
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension gg(50),hh(50),CosIn(50),CosOut(50)
      dimension nin(26),nout(26)
      double precision suma1,suma2
      character*80 Veta
      mo=0
      lo=0
      suma1=0.
      suma2=0.
      do k=1,NFacesR
        CosInP =scalmul(sod,DFaceR(1,k))
        CosOutP=scalmul(sd, DFaceR(1,k))
        if(CosInP.lt.0.) then
          mo=mo+1
          CosIn(mo)=1./CosInP
          nin(mo)=k
        endif
        if(CosOutP.gt.0) then
          lo=lo+1
          CosOut(lo)=1./CosOutP
          nout(lo)=k
        endif
      enddo
      do l=1,NPoints
        do k=1,mo
          gg(k)=-(DFace4R(nin(k))
     1            -scalmul(gridp(1,l),DFaceR(1,nin (k))))*CosIn(k)
        enddo
        do k=1,lo
          hh(k)= (DFace4R(nout(k))
     1            -scalmul(gridp(1,l),DFaceR(1,nout(k))))*CosOut(k)
        enddo
        gk=gg(1)
        do k=2,mo
          gk=min(gk,gg(k))
        enddo
        gk=max(0.,gk)
        hk=hh(1)
        do k=2,lo
          hk=min(hk,hh(k))
        enddo
        hk=max(0.,hk)+gk
        ab=exp(-hk*AMiUsed*10.)*GridP(4,l)
        suma1=suma1+ab*hk
        suma2=suma2+ab
      enddo
      tbar=suma1/suma2
      corrf(2)=volg/suma2
      return
      end
