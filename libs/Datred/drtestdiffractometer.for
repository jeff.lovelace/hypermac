      subroutine DRTestDiffractometer
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(4),TrXYZ(3,3),ior(3),h(3),xs(4)
      character*256 EdwStringQuest
      character*80 Veta,t80
      character*5  UhelLabel(4)
      character*2 nty
      integer EdwStateQuest
      logical OMFromFile,CrwLogicQuest,FeYesNo
      data ior/1,3,2/,UhelLabel/'Phi','Chi','Omega','Theta'/,
     1     xs/4*0./
      OMFromFile=.true.
      id=NextQuestId()
      xqd=260.
      call FeQuestCreate(id,-1.,-1.,xqd,11,'Interactive import',
     1                   0,LightGray,-1,0)
      il=0
      nCrwGroup=1
      Veta='Orientation matrix : from a %file'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,nCrwGroup)
        if(i.eq.1) then
          nCrwOMFromFile=CrwLastMade
        else
          nCrwOMExplicitly=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.OMFromFile)
        Veta='                     %explicitly'
      enddo
      il=il-1
      tpom=xpom+CrwgXd+5.
      tpoms=tpom
      Veta='=>'
      xpom=tpom+FeTxLengthUnder(Veta)+3.
      t80='%Browse'
      dpomb=FeTxLengthUnder(t80)+5.
      dpom=xqd-xpom-dpomb-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFile=EdwLastMade
      xpom=xpom+dpom+5.
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,t80)
      nButtBrowse=ButtonLastMade
      xpom=tpoms+FeTxLengthUnder('XXXXXXXXXX')+5.
      dpom=xqd-xpom-5.
      il=il+1
      do i=1,3
        write(Veta,'(''=> %'',i1,a2,'' row'')') i,nty(i)
        if(i.gt.1) Veta(1:2)=' '
        call FeQuestEdwMake(id,tpoms,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwFirstOM=EdwLastMade
        il=il+1
      enddo
      xpom=0.
      ilp=il
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='Direction towards primary beam'
        else if(i.eq.2) then
          Veta='Direction perpendicular to diffraction plane'
        else if(i.eq.3) then
          Veta='Coplementary direction'
        endif
        call FeQuestLblMake(id,5.,il,Veta,'L','N')
        xpom=max(xpom,FeTxLengthUnder(Veta))
      enddo
      xpom=xpom+15.
      il=ilp
      dpom=CrwgXd+5.
      call UnitMat(TrXYZ,3)
      do i=1,4
        if(i.eq.1) then
          Veta='-'
          k=0
          xdp=CrwXd
          ydp=CrwYd
        else
          Veta=smbx(i-1)
          nCrwGroup=nCrwGroup+1
          k=nCrwGroup
          xdp=CrwgXd
          ydp=CrwgYd
        endif
        call FeQuestLblMake(id,xpom+.5*xdp,ilp,Veta,'C','N')
        do il=ilp+1,ilp+3
          call FeQuestCrwMake(id,tpom,il,xpom,il,' ','L',xdp,ydp,
     1                        1,k)
          if(il.eq.ilp+1) then
            if(i.eq.1) then
              nCrwSignFirst=CrwLastMade
            else if(i.eq.2) then
              nCrwXYZFirst=CrwLastMade
            endif
          endif
        enddo
        xpom=xpom+dpom
      enddo
      il=ilp+4
      Veta='Indicate angles having opposite sense :'
      call FeQuestLblMake(id,5.,il,Veta,'L','N')
      tpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom
      do i=1,4
        xpom=tpom+FeTxLengthUnder(UhelLabel(i))+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,UhelLabel(i),'L',CrwXd,
     1                      CrwXd,0,0)
        call FeQuestCrwOpen(CrwLastMade,.false.)
        if(i.eq.1) nCrwSenseFirst=CrwLastMade
        tpom=tpom+30.
      enddo
      il=il+1
      Veta='%Wave length'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=40.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwWaveLength=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,LamAveRefBlock(KRefBlock),
     1                        .false.,.false.)
      il=il+1
      Veta='(hkl)->Euler'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xqd*.5-dpom-5.,il,dpom,ButYd,Veta)
      nButtHKLToEuler=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='Euler->(hkl)'
      call FeQuestButtonMake(id,xqd*.5+5.,il,dpom,ButYd,Veta)
      nButtEulerToHKL=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  nEdw=nEdwFirstOM
      if(OMFromFile) then
        if(EdwStateQuest(nEdwFile).ne.EdwOpened) then
          do i=1,3
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
          call FeQuestStringEdwOpen(nEdwFile,' ')
          call FeQuestButtonOpen(nButtBrowse,ButtonOff)
        endif
      else
        if(EdwStateQuest(nEdwFile).eq.EdwOpened) then
          call FeQuestEdwClose(nEdwFile)
          call FeQuestButtonClose(nButtBrowse)
          do i=1,3
            do j=1,3
              xp(j)=ub(i,j,1)
            enddo
            call FeQuestRealAEdwOpen(nEdw,xp,3,.false.,.false.)
            nEdw=nEdw+1
          enddo
        endif
      endif
1600  nCrw=nCrwXYZFirst
      do i=1,3
        k=0
        do j=1,3
          if(k.eq.0) then
            call FeQuestCrwOpen(nCrw,abs(TrXYZ(ior(j),i)).gt..01)
            if(CrwLogicQuest(nCrw)) k=1
          else
            call FeQuestCrwClose(nCrw)
          endif
          nCrw=nCrw+1
        enddo
      enddo
1700  nCrw=nCrwSignFirst
      do 1750i=1,3
        do j=1,3
          pom=TrXYZ(ior(i),j)
          if(abs(pom).gt..01) go to 1745
        enddo
        go to 1750
1745    call FeQuestCrwOpen(nCrw,pom.lt.-.01)
        nCrw=nCrw+1
1750  continue
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        if(CheckNumber.eq.nCrwOMFromFile) then
          OMFromFile=.true.
        else if(CheckNumber.eq.nCrwOMExplicitly) then
          OMFromFile=.false.
        else if(CheckNumber.ge.nCrwXYZFirst.and.
     1          CheckNumber.le.nCrwXYZFirst+8) then
          k=CheckNumber-nCrwXYZFirst+1
          j=(k-1)/3+1
          i=ior(mod(k-1,3)+1)
          do k=1,3
            if(abs(TrXYZ(k,j)).gt..01) go to 2110
          enddo
2110      do l=1,3
            if(abs(TrXYZ(i,l)).gt..01) go to 2130
          enddo
2130      TrXYZ(k,j)=0.
          TrXYZ(i,j)=1.
          TrXYZ(i,l)=0.
          TrXYZ(k,l)=1.
          go to 1600
        else if(CheckNumber.ge.nCrwSignFirst.and.
     1          CheckNumber.le.nCrwSignFirst+2) then
          j=ior(mod(CheckNumber-nCrwSignFirst,3)+1)
          do i=1,3
            TrXYZ(j,i)=-TrXYZ(j,i)
          enddo
        endif
        go to 1500
      else if(CheckType.eq.EventButton) then
        ib=CheckNumber
        if(ib.eq.nButtHKLToEuler.or.ib.eq.nButtEulerToHKL) then
          call DRIntUpDate(OMFromFile,TrXYZ,nEdwFile,nEdwFirstOM,
     1                     nCrwSenseFirst,nEdwWaveLength)
          if(ErrFlag.ne.0) then
            ErrFlag=0
            go to 2000
          endif
          Veta=ButtonText(ib+ButtonFr-1)
          idn=NextQuestId()
          xqdp=180.
          call FeQuestCreate(idn,-1.,-1.,xqdp,10,Veta,0,LightGray,0,
     1                       -1)
          il=1
          Veta='Orientation matrix'
          do i=1,3
            il=il+1
            write(Veta,'(3f10.6)')(ub(i,j,1),j=1,3)
            call FeQuestLblMake(idn,xqdp*.5,il,Veta,'C','N')
          enddo
          il=il+1
          Veta='Cell parameters'
          call FeQuestLblMake(idn,xqdp*.5,il,Veta,'C','N')
          il=il+1
          write(t80,'(3f9.3,3f9.2)')(CellRefBlock(i,0),i=1,6)
          call FeQuestLblMake(idn,xqdp*.5,il,t80,'C','N')
          il=il+1
          tpom=5.
          if(ib.eq.nButtHKLToEuler) then
            Veta='%Indices'
            ik=4
          else
            Veta='%Angles'
            ik=3
          endif
          xpom=tpom+FeTxLengthUnder(Veta)+5.
          dpom=xqdp-xpom-5.
          call FeQuestEdwMake(idn,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          nEdwIndices=EdwLastMade
          pom=0.
          do i=1,7-ik
            pom=pom+abs(xs(i))
          enddo
          if(pom.gt..001) then
            if(ib.eq.nButtHKLToEuler) then
              if(abs(xs(4)).gt..001) pom=0.
            else
              if(abs(xs(4)).lt..001) pom=0.
            endif
          endif
          call FeQuestRealAEdwOpen(EdwLastMade,xs,7-ik,pom.lt..001,
     1                             .false.)
          il=il+1
          call FeQuestButtonMake(idn,(xqdp-ButYd)*.5,il,ButYd,ButYd,'#')
          nButtCalc=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          il=il+1
          tpom=5.
          do i=1,ik
            Veta=' '
            if(ib.eq.nButtHKLToEuler) then
              Veta=Veta(:10-idel(UhelLabel(i)))//UhelLabel(i)
            else
              Veta(10:10)=indices(i)
            endif
            call FeQuestLblMake(idn,tpom,il,Veta,'L','N')
            tpom=tpom+FeTxLengthUnder(Veta)
          enddo
          il=il+1
          call FeQuestLblMake(idn,5.,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          nLblPos=LblLastMade
          t80=' '
3030      call FeQuestEvent(idn,ich)
          if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtCalc).or.
     1       (CheckType.eq.EventKey.and.CheckNumber.eq.JeReturn)) then
            call FeQuestRealAFromEdw(nEdwIndices,xs)
            if(ib.eq.nButtHKLToEuler) then
              call CopyVek(xs,h,3)
              xs(4)=0.
              call DRHKLToAngles(h,xp)
              write(t80,100) xp
            else
              call DRAnglesToHKL(xs,h)
              write(t80,100) h
            endif
            call FeQuestLblChange(nLblPos,t80)
            go to 3030
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3030
          endif
          call FeQuestRemove(idn)
        else if(ib.eq.nButtBrowse) then
          t80=EdwStringQuest(nEdwFile)
          call FeFileManager('Select file with orientation matrix',t80,
     1                       '*.*',0,.true.,ich)
          if(ich.eq.0) call FeQuestStringEdwOpen(nEdwFile,t80)
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(.not.FeYesNo(-1.,-1.,'Do you really want to close the testing',
     1   0)) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      call FeQuestRemove(id)
      return
100   format(4f10.3)
      end
