      subroutine DRWriteUsedSymm(lnt,ntst)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 t80
      integer CrlCentroSymm
      t80='Centrosymmetric super-space group: '//
     1    Grupa(KPhase)(:idel(Grupa(KPhase)))
      if(NDim(KPhase).eq.3) t80(18:)=t80(24:)
      if(CrlCentroSymm().le.0) t80='Non-c'//t80(2:)
      write(lnt,100) ntst,t80(:idel(t80))
      write(lnt,100) ntst
      write(lnt,100) ntst,'List of centring vectors:'
      write(lnt,100) ntst
      do j=1,NLattVec(KPhase)
        write(t80,'(6f10.6)')(vt6(k,j,1,KPhase),k=1,NDim(KPhase))
        write(lnt,100) ntst,t80(:idel(t80))
      enddo
      write(lnt,100) ntst
      write(lnt,100) ntst,'Symmetry operators:'
      write(lnt,100) ntst
      n=0
      do j=1,NSymm(KPhase)
        call CodeSymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                symmc(1,j,1,KPhase),0)
        do k=1,NDim(KPhase)
          kk=idel(symmc(k,j,1,KPhase))+1
          if(symmc(k,j,1,KPhase)(1:1).ne.'-') kk=kk+1
          n=max(n,kk)
        enddo
      enddo
      if(n.lt.5) n=5
      do j=1,NSymm(KPhase)
        t80=' '
        k=5
        do l=1,NDim(KPhase)
          if(symmc(l,j,1,KPhase)(1:1).eq.'-') then
            kk=k
          else
            kk=k+1
          endif
          t80(kk:)=symmc(l,j,1,KPhase)
          k=k+n
        enddo
        write(lnt,100) ntst,t80(:idel(t80))
      enddo
      return
100   format(i4,' symm',4x,a)
      end
