      subroutine DRSGTestLaue(LookForSuperCell,Key,ich)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension CellParArr(6,15),CellTrArr(36,15),TrPom(3,3),i90(3),
     1          ToOrtho(9),FromOrtho(9),QuPom(3,3),TrSymmFull(36),
     2          TrSymmFullI(36),QuIrrArr(3,3,15),QuRacArr(3,3,15),
     3          TrTwin(36),SPom(:,:),TrPomI(3,3),TwPom(3,3),
     4          TrMPStart(36),TrPom6(36)
      character*256 Radka,FileName
      character*80  :: p80,LaueStringO(:),AveragedFrom=' '
      character*25  :: CrSystemNameP(15) = (/
     1                   'Triclinic                ',
     2                   'Monoclinic-setting "a"   ',
     3                   'Monoclinic-setting "b"   ',
     4                   'Monoclinic-setting "c"   ',
     5                   'Orthorhombic             ',
     6                   'Tetragonal-setting "a"   ',
     7                   'Tetragonal-setting "b"   ',
     8                   'Tetragonal-setting "c"   ',
     9                   'Trigonal                 ',
     a                   'Rhombic-setting "a,-b,-c"',
     1                   'Rhombic-setting "-a,b,-c"',
     2                   'Rhombic-setting "-a,-b,c"',
     3                   'Rhombic-setting "a,b,c"  ',
     4                   'Hexagonal                ',
     5                   'Cubic                    '/)
      integer CrSystemFromCell,CrSystemReduced,CrSystemPure,CrSystemAxis
     1       ,CrSystemRezerva,SbwItemPointerQuest,RLaueStringO(:),
     2        Order,OrderLaue,OrderRInt,SbwLnQuest
      logical LookForSuperCell,EqRV,UseOld,EqIgCase
      allocatable LaueStringO,RLaueStringO,SPom
      equivalence (IdNumbers(1),OrderLaue),
     1            (IdNumbers(2),OrderRInt)
      if(Key.eq.1) then
        UseOld=.true.
        go to 1600
      endif
      call CopyMat(TrMP,TrMPStart,NDim(KPhase))
1300  call CopyVek(CellParOrg,CellParArr,6)
      call UnitMat(CellTrArr,NDim(KPhase))
      call CopyMat(TrMPStart,TrMP,NDim(KPhase))
      call CopyMat(TrMPStart,TrMPOrg,NDim(KPhase))
      if(LookForSuperCell) then
        i=0
        call DRSGTestSuperCell(CellParArr,CellTrArr,i,0,ich)
        if(ich.ne.0) go to 9999
        if(NSupCellSel.gt.NSupCell) then
          call CopyVek(CellParOrg,CellParArr,6)
          call UnitMat(CellTrArr,NDim(KPhase))
        else
          call MultM(TrMP,CellTrArr,TrMpOrg,NDim(KPhase),NDim(KPhase),
     1                                                  NDim(KPhase))
          call CopyMat(TrMpOrg,TrMP,NDim(KPhase))
        endif
        call UnitMat(CellTrArr,NDim(KPhase))
        KeyP=0
        if(NRefBlock.gt.1) then
          if(allocated(UseInSGTest)) KeyP=-1
        endif
        call DRSetRefArrays(KeyP)
      endif
      CrSystemReduced=CrSystemFromCell(CellParArr,DiffAxe,DiffAngle)
      if(CrSystemReduced.eq.1) then
        call DRCellReduction(CellParArr,CellTrArr)
        CrSystemReduced=CrSystemFromCell(CellParArr,DiffAxe,DiffAngle)
      endif
      UseOld=EqRV(CellParArr(1,1),CellParArr1Old(1),3,DiffAxe  ).and.
     1       EqRV(CellParArr(4,1),CellParArr1Old(4),3,DiffAngle).and.
     2       abs(DiffAxe  -DiffAxeOld  ).lt..0001.and.
     3       abs(DiffAngle-DiffAngleOld).lt..001.and.
     4       abs(DiffModVec-DiffModVecOld).lt..00001
      if(UseOld) then
        if((TakeSGTwinOld.ne.-1.and.TakeSGTwin.eqv.TakeSGTwinOld.eq.1)
     1     .and.(TakeOldTwinOld.ne.-1.and.
     2           TakeOldTwin.eqv.TakeOldTwinOld.eq.1)) then
          go to 1600
        endif
      endif
      if(TakeSGTwin) then
        TakeSGTwinOld=1
      else
        TakeSGTwinOld=0
      endif
      if(TakeOldTwin) then
        TakeOldTwinOld=1
      else
        TakeOldTwinOld=0
      endif
      call CopyVek(CellParArr,CellParArr1Old,6)
      call SetRealArrayTo(CellParArr(1,2),84,-1.)
      CrSystemReduced=CrSystemFromCell(CellParArr,DiffAxe,DiffAngle)
      CrSystemPure=mod(iabs(CrSystemReduced),10)
      CrSystemAxis=iabs(CrSystemReduced)/10
      if(CrSystemPure.eq.CrSystemCubic) then
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdCubic),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdCubic),NDim(KPhase))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdTetragonalC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               celltrarr(1,IdTetragonalC),NDim(KPhase))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdTetragonalB),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdTetragonalB),NDim(KPhase))
        call SetPermutMat(TrPom,3,231,ich)
        call DRMatTrCell(TrPom,CellParArr(1,IdTetragonalB),
     1                   CellTrArr(1,IdTetragonalB))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdTetragonalA),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdTetragonalA),NDim(KPhase))
        call SetPermutMat(TrPom,3,312,ich)
        call DRMatTrCell(TrPom,CellParArr(1,IdTetragonalA),
     1                   CellTrArr(1,IdTetragonalA))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdRhombicAmBmC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdRhombicAmBmC),NDim(KPhase))
        TrPom(1,3)= 1.
        TrPom(2,3)=-1.
        TrPom(3,3)=-1.
        TrPom(1,1)= 1.
        TrPom(2,1)= 1.
        TrPom(3,1)= 0.
        TrPom(1,2)= 0.
        TrPom(2,2)=-1.
        TrPom(3,2)= 1.
        call DRMatTrCell(TrPom,CellParArr(1,IdRhombicAmBmC),
     1                   CellTrArr(1,IdRhombicAmBmC))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdRhombicmABmC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdRhombicmABmC),NDim(KPhase))
        TrPom(1,3)=-1.
        TrPom(2,3)= 1.
        TrPom(3,3)=-1.
        TrPom(1,1)=-1.
        TrPom(2,1)=-1.
        TrPom(3,1)= 0.
        TrPom(1,2)= 0.
        TrPom(2,2)= 1.
        TrPom(3,2)= 1.
        call DRMatTrCell(TrPom,CellParArr(1,IdRhombicmABmC),
     1                   CellTrArr(1,IdRhombicmABmC))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdRhombicmAmBC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdRhombicmAmBC),NDim(KPhase))
        TrPom(1,3)=-1.
        TrPom(2,3)=-1.
        TrPom(3,3)= 1.
        TrPom(1,1)=-1.
        TrPom(2,1)= 1.
        TrPom(3,1)= 0.
        TrPom(1,2)= 0.
        TrPom(2,2)=-1.
        TrPom(3,2)=-1.
        call DRMatTrCell(TrPom,CellParArr(1,IdRhombicmAmBC),
     1                   CellTrArr(1,IdRhombicmAmBC))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdRhombicABC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdRhombicABC),NDim(KPhase))
        TrPom(1,1)= 1.
        TrPom(2,1)=-1.
        TrPom(3,1)= 0.
        TrPom(1,2)= 0.
        TrPom(2,2)= 1.
        TrPom(3,2)=-1.
        TrPom(1,3)= 1.
        TrPom(2,3)= 1.
        TrPom(3,3)= 1.
        call DRMatTrCell(TrPom,CellParArr(1,IdRhombicABC),
     1                   CellTrArr(1,IdRhombicABC))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdOrthorhombic),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdOrthorhombic),NDim(KPhase))
        go to 1350
      else if(CrSystemPure.eq.CrSystemHexagonal) then
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdHexagonal),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdHexagonal),NDim(KPhase))
        if(CrSystemAxis.eq.3.or.CrSystemAxis.eq.0) then
          i=123
        else if(CrSystemAxis.eq.1) then
          i=312
        else if(CrSystemAxis.eq.2) then
          i=231
        endif
        call SetPermutMat(TrPom,3,i,ich)
        call DRMatTrCell(TrPom,CellParArr(1,IdHexagonal),
     1                   CellTrArr(1,IdHexagonal))
        if(CrSystemReduced.lt.0) then
          call UnitMat(TrPom,3)
          TrPom(2,2)=-1.
          TrPom(3,3)=-1.
          call DRMatTrCell(TrPom,CellParArr(1,IdHexagonal),
     1                     CellTrArr(1,IdHexagonal))
        endif
        call CopyVek(CellParArr(1,IdHexagonal),
     1               CellParArr(1,IdTrigonal ),6)
        call CopyMat(CellTrArr(1,IdHexagonal),
     1               CellTrArr(1,IdTrigonal ),NDim(KPhase))
        call CopyVek(CellParArr(1,IdHexagonal),
     1               CellParArr(1,IdMonoclinicC),6)
        call CopyMat(CellTrArr(1,IdHexagonal),
     1               CellTrArr(1,IdMonoclinicC),NDim(KPhase))
      else if(CrSystemPure.eq.CrSystemTrigonal) then
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdTrigonal),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdTrigonal ),NDim(KPhase))
        if(CrSystemReduced.eq.-CrSystemTrigonal) then
          call UnitMat(TrPom,3)
          TrPom(2,1)=-1.
          TrPom(3,2)=-1.
          TrPom(1,3)= 1.
          TrPom(2,3)= 1.
          call DRMatTrCell(TrPom,CellParArr(1,IdTrigonal),
     1                     CellTrArr(1,IdTrigonal))
        endif
        call CopyVek(CellParArr(1,IdTrigonal),
     1               CellParArr(1,IdMonoclinicC),6)
        call CopyMat(CellTrArr(1,IdTrigonal),
     1               CellTrArr(1,IdMonoclinicC),NDim(KPhase))
      else if(CrSystemPure.eq.CrSystemTetragonal) then
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdTetragonalC),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdTetraGonalC),NDim(KPhase))
        if(CrSystemAxis.eq.3.or.CrSystemAxis.eq.0) then
          i=123
        else if(CrSystemAxis.eq.1) then
          i=312
        else if(CrSystemAxis.eq.2) then
          i=231
        endif
        call SetPermutMat(TrPom,3,i,ich)
        call DRMatTrCell(TrPom,CellParArr(1,IdTetragonalC),
     1                   CellTrArr(1,IdTetragonalC))
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdOrthorhombic),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdOrthorhombic),NDim(KPhase))
        go to 1350
      else if(CrSystemPure.eq.CrSystemOrthorhombic) then
        call CopyVek(CellParArr(1,IdTriclinic),
     1               CellParArr(1,IdOrthoRhombic),6)
        call CopyMat(CellTrArr(1,IdTriclinic),
     1               CellTrArr(1,IdOrthoRhombic),NDim(KPhase))
        go to 1350
      else if(CrSystemPure.eq.CrSystemMonoclinic) then
        if(CrSystemAxis.eq.3.or.CrSystemAxis.eq.0) then
          i=123
        else if(CrSystemAxis.eq.1) then
          i=312
        else if(CrSystemAxis.eq.2) then
          i=231
        endif
        j=IdMonoclinicB
        call CopyVek(CellParArr(1,IdTriclinic),CellParArr(1,j),6)
        call CopyMat(CellTrArr(1,IdTriclinic),CellTrArr(1,j),
     1               NDim(KPhase))
        call SetPermutMat(TrPom,3,i,ich)
        call DRMatTrCell(TrPom,CellParArr(1,j),CellTrArr(1,j))
      endif
      go to 1400
1350  call CopyVek(CellParArr(1,IdTriclinic),
     1             CellParArr(1,IdMonoclinicC),6)
      call CopyMat(CellTrArr(1,IdTriclinic),
     1             CellTrArr(1,IdMonoclinicC),NDim(KPhase))
      call CopyVek(CellParArr(1,IdTriclinic),
     1             CellParArr(1,IdMonoclinicB),6)
      call CopyMat(CellTrArr(1,IdTriclinic),
     1             CellTrArr(1,IdMonoclinicB),NDim(KPhase))
      call SetPermutMat(TrPom,3,231,ich)
      call DRMatTrCell(TrPom,CellParArr(1,IdMonoclinicB),
     1                 CellTrArr(1,IdMonoclinicB))
      call CopyVek(CellParArr(1,IdTriclinic),
     1             CellParArr(1,IdMonoclinicA),6)
      call CopyMat(CellTrArr(1,IdTriclinic),
     1             CellTrArr(1,IdMonoclinicA),NDim(KPhase))
      call SetPermutMat(TrPom,3,312,ich)
      call DRMatTrCell(TrPom,CellParArr(1,IdMonoclinicA),
     1                 CellTrArr(1,IdMonoclinicA))
1400  call CopyVek(CellParArr,CellParArrOld,66)
      if(.not.isPowder) then
        call OpenFile(lst,fln(:ifln)//'.laue','formatted','unknown')
        LstOpened=.true.
      endif
      RIntAllLowest=9999999.
      RIntAllLimit=10.
      NOptimalLaue=0
      NInfo=1
      if(allocated(LaueString)) deallocate(LaueString,RLaueString)
      allocate(LaueString(20),RLaueString(20))
      nLaueString=20
      iLaueString=0
      if(TakeSGTwin) then
        IR1=1
      else
        IR1=2
        if(allocated(SPom)) deallocate(SPom)
        allocate(SPom(3,NTwin))
        call SetRealArrayTo(SPom,3*NTwin,0.)
        call CrlStoreSymmetryExplicite(IdSymmFull  ,rtw,SPom,SPom,NTwin,
     1                                 1,3)
        call CrlStoreSymmetryExplicite(IdSymmFullTr,rtw,SPom,SPom,NTwin,
     1                                 1,3)
      endif
      do 1500IRepeat=IR1,2
        if(IRepeat.eq.2)
     1    call FeTxOut(-1.,-1.,'Averaging for Laue group: 6/mmm - '//
     2                 CrSystemNameP(10)(:idel(CrSystemNameP(10))))
        mLaueGroup=1
        if(IRepeat.eq.1) then
          i1=15
          i2=1
          i3=-1
        else
          i1=1
          i2=15
          i3=1
        endif
        do i=i1,i2,i3
          if(i.eq.IdRhombicAmBmC.or.
     1       i.eq.IdRhombicmABmC.or.
     2       i.eq.IdRhombicmAmBC.or.
     3       i.eq.IdRhombicABC) then
            mLaueGroup=3
          else
            mLaueGroup=1
          endif
          if(i.eq.IdTriclinic) then
            LaueGroup=0
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemTriclinic
            Monoclinic(KPhase)=0
          else if(i.eq.IdMonoclinicA.or.
     1            i.eq.IdMonoclinicB.or.
     2            i.eq.IdMonoclinicC) then
            LaueGroup=1
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemMonoclinic
            Monoclinic(KPhase)=3
          else if(i.eq.IdOrthorhombic) then
            LaueGroup=2
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemOrthorhombic
            Monoclinic(KPhase)=0
          else if(i.eq.IdTetragonalA.or.
     1            i.eq.IdTetragonalB.or.
     2            i.eq.IdTetragonalC) then
            LaueGroup=3
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemTetragonal
          else if(i.eq.IdTrigonal) then
            LaueGroup=5
            nLaueGroup=3
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdRhombicAmBmC.or.
     1            i.eq.IdRhombicmABmC.or.
     2            i.eq.IdRhombicmAmBC.or.
     3            i.eq.IdRhombicABC) then
            LaueGroup=5
            nLaueGroup=4
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdHexagonal) then
            LaueGroup=9
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdCubic) then
            LaueGroup=11
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemCubic
          endif
          if(CellParArr(1,i).gt.0.) then
            call CopyMat(CellTrArr(1,i),TrMP,NDim(KPhase))
            call MatInv(TrMP,TrMPI,pom,NDim(KPhase))
            if(i.eq.IdTriclinic) then
              call SetIntArrayTo(i90,3,0)
            else
              call SetIntArrayTo(i90,3,1)
              if(i.eq.IdMonoclinicA.or.i.eq.IdMonoclinicB.or.
     1           i.eq.IdMonoclinicC.or.i.eq.IdHexagonal.or.
     2           i.eq.IdTrigonal) i90(3)=0
            endif
            do j=1,3
              if(i90(j).eq.1) CellParArr(j+3,i)=90.
            enddo
            if(i.eq.IdTetragonalA.or.i.eq.IdTetragonalB.or.
     1         i.eq.IdTetragonalC.or.i.eq.IdHexagonal.or.
     2         i.eq.IdTrigonal) then
              pom=(CellParArr(1,i)+CellParArr(2,i))*.5
              CellParArr(1,i)=pom
              CellParArr(2,i)=pom
            else if(i.eq.IdCubic) then
              pom=(CellParArr(1,i)+CellParArr(2,i)+CellParArr(3,i))/3.
              CellParArr(1,i)=pom
              CellParArr(2,i)=pom
              CellParArr(3,i)=pom
            endif
            if(i.eq.IdHexagonal.or.i.eq.IdTrigonal.or.
     1         i.eq.IdRhombicAmBmC.or.i.eq.IdRhombicmABmC.or.
     2         i.eq.IdRhombicmAmBC.or.i.eq.IdRhombicABC)
     3        CellParArr(6,i)=120.
            call CrlSetTrFractToOrtho(CellParArr(1,i),ToOrtho)
            call matinv(ToOrtho,FromOrtho,pom,3)
            if(IRepeat.eq.1) then
              j1=nLaueGroup
              j2=1
              j3=-MLaueGroup
            else
              j1=1
              j2=nLaueGroup
              j3=MLaueGroup
            endif
            do j=j1,j2,j3
              call MatBlock3(CellTrArr(1,i),TrPom,NDim(KPhase))
              if(NDimI(KPhase).gt.0) then
                do k=1,NDimI(KPhase)
                  call Multm(QuOrg(1,k),TrPom,QuPom(1,k),1,3,3)
                enddo
              endif
              k=LaueGroupPointer(LaueGroup+j)
              CrSystemRezerva=CrSystem(KPhase)
              Radka=CrSystemNameP(i)
              if(IRepeat.eq.1) then
                call DRGenPointGroup(Radka,SmbPGI(k),QuPom,ToOrtho,
     1                               FromOrtho,QuIrrArr(1,1,i),
     2                               QuRacArr(1,1,i),ich)
                call CrlStoreSymmetry(IdSymmFull)
                call CrlStoreSymmetry(IdSymmFullTr)
                call CopyMat(TrMP ,TrSymmFull ,NDim(KPhase))
                call CopyMat(TrMPI,TrSymmFullI,NDim(KPhase))
                go to 1500
              else
                NInfo=NInfo+1
                if((CrSystemPure.eq.CrSystemMonoclinic.and.
     1              i.eq.IdMonoclinicB).or.
     2             (CrSystemPure.eq.CrSystemTetragonal.and.
     3              i.eq.IdTetragonalC)) then
                  kk=index(Radka,'-')
                  Radka(kk:)=' '
                endif
                if(.not.isPowder) then
                  StructureName=' '
                  write(lst,'(''Report#'',i3)') NInfo-1
                  Uloha='Averaging for the point group: '//
     1                  SmbPGI(k)(:idel(SmbPGI(k)))
                  call NewPg(1)
                  call MultM(TrMPOrg,CellTrArr(1,i),TrPom6,
     1                       NDim(KPhase),NDim(KPhase),NDim(KPhase))
                  call DRWriteUsedPar(CellParArr(1,i),QuPom,TrPom6)
                  call TitulekVRamecku(Uloha)
                endif
                if(TakeSGTwin) then
                  call MultM(TrMP,TrSymmFullI,TrTwin,NDim(KPhase),
     1                       NDim(KPhase),NDim(KPhase))
                  call CrlTransSymmetry(IdSymmFull,TrTwin,IdSymmFullTr)
                else
                  call CrlTransSymmetry(IdSymmFull,TrPom,IdSymmFullTr)
                  call SetRealArrayTo(SPom,3*NTwin,0.)
                  call CrlRestoreSymmetryExplicite(IdSymmFullTr,rtw,
     1                                             SPom,SPom,NTwin,m,m)
                endif
                call DRAverageForPointGroup(Radka,SmbPGI(k),QuPom,
     1            ToOrtho,FromOrtho,QuIrrArr(1,1,i),QuRacArr(1,1,i),ich)
                p80=TextInfo(NInfo)(:26)
                kk=26
                if(index(TextInfo(NInfo),'inconsistent').le.0) then
1450              call kus(TextInfo(NInfo),kk,Cislo)
                  p80=p80(:idel(p80))//Tabulator//'|'//Tabulator//Cislo
                  if(kk.lt.len(TextInfo(NInfo))) go to 1450
                  p80=p80(:idel(p80))//Tabulator//'|'
                else
                  call kus(TextInfo(NInfo),kk,Cislo)
                  p80=p80(:idel(p80))//Tabulator//'|'//Tabulator//Cislo
                  p80=p80(:idel(p80))//Tabulator//'|'//Tabulator//
     1                TextInfo(NInfo)(kk+1:)
                endif
                if(iLaueString.ge.NLaueString) then
                  allocate(LaueStringO(NLaueString),
     1                    RLaueStringO(NLaueString))
                  do l=1,iLaueString
                     LaueStringO(l)= LaueString(l)
                    RLaueStringO(l)=RLaueString(l)
                  enddo
                  deallocate(LaueString,RLaueString)
                  nLaueString=2*nLaueString
                  allocate( LaueString(nLaueString),
     1                     RLaueString(nLaueString))
                  do l=1,iLaueString
                     LaueString(l)= LaueStringO(l)
                    RLaueString(l)=RLaueStringO(l)
                  enddo
                  deallocate(LaueStringO,RLaueStringO)
                endif
                iLaueString=iLaueString+1
                 LaueString(iLaueString)=p80
                RLaueString(iLaueString)=nint(RIntAll*1000.)
                if(RIntAll.le.RIntAllLowest) then
                  RIntAllLowest=RIntAll
                  nSelLaue=iLaueString
                endif
                if(RIntAll.lt.RIntAllLimit) NOptimalLaue=iLaueString
              endif
              CrSystem(KPhase)=CrSystemRezerva
            enddo
          endif
        enddo
1500  continue
      call FeTxOutEnd
      if(NOptimalLaue.gt.0) nSelLaue=NOptimalLaue
      if(allocated(SPom)) deallocate(SPom)
      Order=OrderLaue
      AveragedFrom=TextInfo(1)
1600  il=1
      call FeTabsReset(1)
      pom=125.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      pom=pom+15.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      pom=pom+45.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      pom=pom+40.
      call FeTabsAdd(pom,1,IdCharTab,'/')
      pom=pom+40.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      pom=pom+45.
      call FeTabsAdd(pom,1,IdCharTab,'/')
      pom=pom+55.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      pom=pom+35.
      call FeTabsAdd(pom,1,IdCharTab,'.')
      pom=pom+45.
      call FeTabsAdd(pom,1,IdRightTab,' ')
      UseTabs=1
      id=NextQuestId()
      call FeQuestTitleMake(id,'Select Laue symmetry')
      xpom=5.
      if(.not.isPowder) then
        p80='Cystal system'
        tpom=10.
        call FeQuestLblMake(id,tpom,il,p80,'L','N')
        tpom=tpom+FeTxLength(p80)+65.
        p80='Point group'
        call FeQuestLblMake(id,tpom,il,p80,'L','N')
        tpom=tpom+FeTxLength(p80)+15.
        p80='Rint(obs/all)'
        call FeQuestLblMake(id,tpom,il,p80,'L','N')
        tpom=tpom+FeTxLength(p80)+20.
        p80='#averaged'
        call FeQuestLblMake(id,tpom,il,p80,'L','N')
        tpom=tpom+FeTxLength(p80)+45.
        p80='Redundancy'
        call FeQuestLblMake(id,tpom,il,p80,'L','N')
      endif
      xpom=5.
      dpom=WizardLength-10.-SbwPruhXd
      il=12
      if(isPowder) il=il+3
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      if(allocated(OLaueString)) deallocate(OLaueString)
      allocate(OLaueString(iLaueString))
      if(isPowder) then
        do i=1,iLaueString
          OLaueString(i)=i
        enddo
      else
        call indexx(iLaueString,RLaueString,OLaueString)
      endif
      if(.not.isPowder) then
        il=il+1
        xpom=10.
        tpom=xpom+5.+CrwgXd
        p80='ordered by %Laue symmetry'
        call FeQuestCrwMake(id,tpom,il,xpom,il,p80,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,Order.eq.OrderLaue)
        nCrwOrderLaue=CrwLastMade
        dpom=85.
        call FeQuestButtonMake(id,(WizardLength-dpom)*.5,il,dpom,
     1                         ButYd,'%Details')
        nButtDetails=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        p80='ordered by %Rint'
        call FeQuestCrwMake(id,tpom,il,xpom,il,p80,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,Order.eq.OrderRInt)
        nCrwOrderRInt=CrwLastMade
        il=il+1
        tpom=5.
        call FeQuestLblMake(id,tpom,il,AveragedFrom,'L','N')
      endif
      call CloseListing
      DiffAxeOld  =DiffAxe
      DiffAngleOld=DiffAngle
      DiffModVecOld=DiffModVec
1650  ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_Laue.tmp','formatted','unknown')
      k=0
      do i=1,iLaueString
        if(Order.eq.OrderLaue) then
          j=i
        else if(Order.eq.OrderRInt) then
          j=OLaueString(i)
        endif
        if(j.eq.nSelLaue) k=i
        write(ln,FormA) LaueString(j)(:idel(LaueString(j)))
      enddo
      k=max(k,1)
      call CloseIfOpened(ln)
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_Laue.tmp')
      call FeQuestSbwItemOff(nSbw,SbwItemPointerQuest(nSbw))
      call FeQuestSbwItemOn(nSbw,k)
1700  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtDetails) then
        NSelLaue=SbwItemPointerQuest(nSbw)
        if(Order.eq.OrderRInt) NSelLaue=OLaueString(NSelLaue)
        lni=NextLogicNumber()
        call OpenFile(lni,fln(:ifln)//'.laue','formatted','unknown')
        if(ErrFlag.ne.0) go to 1700
        FileName=fln(:ifln)//'_tmp.laue'
        lno=NextLogicNumber()
        call OpenFile(lno,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1700
1720    read(lni,FormA,end=1780) Radka
        k=0
        call Kus(Radka,k,Cislo)
        if(.not.EqIgCase(Cislo,'Report#')) go to 1720
        call Kus(Radka,k,Cislo)
        read(Cislo,FormI15) i
        if(i.lt.nSelLaue) then
          go to 1720
        else if(i.gt.nSelLaue) then
          go to 1780
        endif
1740    read(lni,FormA,end=1760) Radka
        k=0
        call Kus(Radka,k,Cislo)
        if(.not.EqIgCase(Cislo,'Report#')) then
          write(lno,FormA) Radka(:idel(Radka))
          go to 1740
        endif
1760    call CloseIfOpened(lno)
        call FeListView(FileName,0)
1780    call CloseIfOpened(lni)
        call CloseIfOpened(lno)
        call DeleteFile(FileName)
        go to 1700
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwOrderLaue) then
        Order=OrderLaue
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 1650
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwOrderRInt) then
        Order=OrderRInt
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 1650
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1700
      endif
      if(ich.ge.0) then
        NSelLaue=SbwItemPointerQuest(nSbw)
        if(Order.eq.OrderRInt) NSelLaue=OLaueString(NSelLaue)
        k=1
        do i=1,15
          if(i.eq.IdRhombicAmBmC.or.
     1       i.eq.IdRhombicmABmC.or.
     2       i.eq.IdRhombicmAmBC.or.
     3       i.eq.IdRhombicABC) then
            mLaueGroup=3
          else
            mLaueGroup=1
          endif
          if(i.eq.IdTriclinic) then
            LaueGroup=0
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemTriclinic
            Monoclinic(KPhase)=0
          else if(i.eq.IdMonoclinicA.or.
     1            i.eq.IdMonoclinicB.or.
     2            i.eq.IdMonoclinicC) then
            LaueGroup=1
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemMonoclinic
            Monoclinic(KPhase)=3
          else if(i.eq.IdOrthorhombic) then
            LaueGroup=2
            nLaueGroup=1
            CrSystem(KPhase)=CrSystemOrthorhombic
            Monoclinic(KPhase)=0
          else if(i.eq.IdTetragonalA.or.
     1            i.eq.IdTetragonalB.or.
     2            i.eq.IdTetragonalC) then
            LaueGroup=3
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemTetragonal
          else if(i.eq.IdTrigonal) then
            LaueGroup=5
            nLaueGroup=3
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdRhombicAmBmC.or.
     1            i.eq.IdRhombicmABmC.or.
     2            i.eq.IdRhombicmAmBC.or.
     3            i.eq.IdRhombicABC) then
            LaueGroup=5
            nLaueGroup=4
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdHexagonal) then
            LaueGroup=9
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemHexagonal
          else if(i.eq.IdCubic) then
            LaueGroup=11
            nLaueGroup=2
            CrSystem(KPhase)=CrSystemCubic
          endif
          if(CellParArr(1,i).gt.0.) then
            do j=1,NLaueGroup,MLaueGroup
              if(k.eq.nSelLaue) then
                call CopyVek(CellParArr(1,i),CellParSel,6)
                call CopyMat(CellTrArr(1,i),CellTrSel6,NDim(KPhase))
                NCellSel=i
                call CopyMat(CellTrArr(1,i),TrMP,NDim(KPhase))
                call MatInv(TrMP,TrMPI,pom,NDim(KPhase))
                call MatBlock3(CellTrArr(1,i),TrPom,NDim(KPhase))
                if(NDimI(KPhase).gt.0) then
                  call MatBlock3(CellTrArr(1,i),TrPom,NDim(KPhase))
                  do k=1,NDimI(KPhase)
                    call Multm(QuOrg(1,k),TrPom,QuPom(1,k),1,3,3)
                  enddo
                endif
                LaueGroupSel=LaueGroup+j
                k=LaueGroupPointer(LaueGroupSel)
                call CrlSetTrFractToOrtho(CellParArr(1,i),ToOrtho)
                call matinv(ToOrtho,FromOrtho,pom,3)
                call DRGenPointGroup(CrSystemNameP(i),SmbPGI(k),QuPom,
     1                               ToOrtho,FromOrtho,QuIrrSel,
     2                               QuRacSel,ichp)
                if(TakeSGTwin) then
                  call MultM(TrMP,TrSymmFullI,TrTwin,NDim(KPhase),
     1                       NDim(KPhase),NDim(KPhase))
                  call CrlTransSymmetry(IdSymmFull,TrTwin,IdSymmFullTr)
!                  call CrlCompTwinSymmetry(IdSymmFullTr,rtw,NTwin,ichp)
                  call CrlCompTwinSymmetry(IdSymmFull,rtw,NTwin,ichp)
                else
                  if(allocated(SPom)) deallocate(SPom)
                  allocate(SPom(3,NTwin))
                  call SetRealArrayTo(SPom,3*NTwin,0.)
                  call CrlTransSymmetry(IdSymmFull,TrPom,IdSymmFullTr)
!                  call CrlRestoreSymmetryExplicite(IdSymmFullTr,rtw,
!     1                                             SPom,SPom,NTwin,m,m)
                  call CrlRestoreSymmetryExplicite(IdSymmFull,rtw,
     1                                             SPom,SPom,NTwin,m,m)
                  if(allocated(SPom)) deallocate(SPom)
                endif
                go to 1900
              endif
              k=k+1
            enddo
          endif
        enddo
      endif
1900  if(CrSystem(KPhase).eq.CrSystemMonoclinic) then
        Monoclinic(KPhase)=2
        call SetPermutMat(TrPom,3,312,ichp)
        call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,CellTrSel6)
      endif
      if(ich.ne.0) then
        if(ich.gt.0.and.LookForSuperCell.and.NSupCell.gt.0) go to 1300
        go to 9999
      else
        if(TakeSGTwin) then
          if(NTwin.gt.1) then
            do i=2,NTwin
              call matinv(rtw(1,i),rtwi(1,i),pom,3)
            enddo
            KeyP=0
            if(NRefBlock.gt.1) then
              if(allocated(UseInSGTest)) KeyP=-1
            endif
            call CopyMat(TrMpOrg,TrMP,NDim(KPhase))
            call DRSetRefArrays(KeyP)
          endif
        endif
      endif
9999  return
      end
