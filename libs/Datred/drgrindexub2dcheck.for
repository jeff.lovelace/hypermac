      subroutine DRGrIndexUB2dCheck
      common/DRGrIndexC/ nEdwCell,nEdwHKL,nButtCheck
      save /DRGrIndexC/
      character*256 EdwStringQuest
      if(EdwStringQuest(nEdwCell) .ne.' '.and.
     1   EdwStringQuest(nEdwHKL)  .ne.' '.and.
     2   EdwStringQuest(nEdwHKL+1).ne.' ') then
        call FeQuestButtonOff(nButtCheck)
      else
        call FeQuestButtonDisable(nButtCheck)
      endif
      return
      end
