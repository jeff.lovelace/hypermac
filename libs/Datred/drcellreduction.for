      subroutine DRCellReduction(Cell,Trm)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Cell(6),trm(*),t1(3,3),t2(3,3),tp(3,3),tt(36),
     1          pommat(3,3),Qi(3,3),Qr(3),xp(3),tt6(36)
      logical hotovo
      real ksi
      data t1/0,-1,0,-1,0,0,0,0,-1/,t2/-1,0,0,0,0,-1,0,-1,0/
      Key=0
      go to 500
      entry DRCellQReduction(Cell,Qi,Qr,trm)
      Key=1
500   aa=Cell(1)**2
      bb=Cell(2)**2
      cc=Cell(3)**2
      ksi=2.*Cell(2)*Cell(3)*cos(ToRad*Cell(4))
      eta=2.*Cell(1)*Cell(3)*cos(ToRad*Cell(5))
      zeta=2.*Cell(1)*Cell(2)*cos(ToRad*Cell(6))
      call UnitMat(tt,3)
      m=0
      mold=-1
      n=0
1000  hotovo=.true.
      n=n+1
      if(mold.ne.m) then
        n=0
      else
        n=n+1
        if(n.gt.20) go to 1500
      endif
      mold=m
      if(aa.gt.bb.or.(aa.eq.bb.and.abs(ksi).gt.abs(eta))) then
        m=1
        pom=aa
        aa=bb
        bb=pom
        pom=eta
        eta=ksi
        ksi=pom
        call multm(tt,t1,tp,3,3,3)
        call CopyMat(tp,tt,3)
      endif
      if(bb.gt.cc.or.(bb.eq.cc.and.abs(eta).gt.abs(zeta))) then
        m=2
        pom=bb
        bb=cc
        cc=pom
        pom=eta
        eta=zeta
        zeta=pom
        call multm(tt,t2,tp,3,3,3)
        call CopyMat(tp,tt,3)
        hotovo=.false.
      endif
      if(hotovo) then
        m=3
        call UnitMat(tp,3)
        if(ksi*eta*zeta.gt.0.) then
          tp(1,1)=sign(1.,ksi)
          tp(2,2)=sign(1.,eta)
          tp(3,3)=sign(1.,zeta)
          ksi=abs(ksi)
          eta=abs(eta)
          zeta=abs(zeta)
        else
          i=0
          if(ksi.eq.0.) then
            i=1
          else
            tp(1,1)=-sign(1.,ksi)
          endif
          if(eta.eq.0.) then
            i=2
          else
            tp(2,2)=-sign(1.,eta)
          endif
          if(zeta.eq.0.) then
            i=3
          else
            tp(3,3)=-sign(1.,zeta)
          endif
          if(i.ne.0.and.tp(1,1)*tp(2,2)*tp(3,3).lt.0..and.i.ne.0)
     1       tp(i,i)=-1.
          ksi=-abs(ksi)
          eta=-abs(eta)
          zeta=-abs(zeta)
        endif
        call multm(tt,tp,pommat,3,3,3)
        call CopyMat(pommat,tt,3)
      endif
      if(hotovo.and.(abs(ksi).gt.bb.or.(ksi.eq.bb.and.2.*eta.lt.zeta).
     1                              or.(ksi.eq.-bb.and.zeta.lt.0.)))
     2   then
        m=4
        call UnitMat(tp,3)
        pom1=-sign(1.,ksi)
        tp(2,3)=pom1
        cc=cc+bb+pom1*ksi
        eta=eta+zeta*pom1
        ksi=ksi+2.*bb*pom1
        hotovo=.false.
        call multm(tt,tp,pommat,3,3,3)
        call CopyMat(pommat,tt,3)
      endif
      if(hotovo.and.(abs(eta).gt.aa.or.(eta.eq.aa.and.2.*ksi.lt.zeta).
     1                              or.(eta.eq.-aa.and.zeta.lt.0.)))
     2   then
        m=5
        call UnitMat(tp,3)
        pom1=-sign(1.,eta)
        tp(1,3)=pom1
        cc=cc+aa+pom1*eta
        ksi=ksi+zeta*pom1
        eta=eta+2.*aa*pom1
        hotovo=.false.
        call multm(tt,tp,pommat,3,3,3)
        call CopyMat(pommat,tt,3)
      endif
      if(hotovo.and.(abs(zeta).gt.aa.or.(zeta.eq.aa.and.2.*ksi.lt.eta)
     1                              .or.(zeta.eq.-aa.and.eta.lt.0.)))
     2   then
        m=6
        call UnitMat(tp,3)
        pom1=-sign(1.,zeta)
        tp(1,2)=pom1
        bb=bb+aa+pom1*zeta
        ksi=ksi+eta*pom1
        zeta=zeta+2.*aa*pom1
        hotovo=.false.
        call multm(tt,tp,pommat,3,3,3)
        call CopyMat(pommat,tt,3)
      endif
      if(hotovo) then
        m=7
        pom1=ksi+eta+zeta+aa+bb
        if(pom1.lt.0..or.(pom1.eq.0..and.2.*(aa+eta)+zeta.gt.0.)) then
          call UnitMat(tp,3)
          tp(1,3)=1.
          tp(2,3)=1.
          cc=cc+pom1
          ksi=2.*bb+ksi+zeta
          eta=2.*aa+eta+zeta
          hotovo=.false.
          call multm(tt,tp,pommat,3,3,3)
          call CopyMat(pommat,tt,3)
        endif
      endif
      if(.not.hotovo) go to 1000
1500  if(Key.ne.0) then
        do i=1,NDimI(KPhase)
          call Multm(Qi(1,i),tt,xp,1,3,3)
          call CopyVek(xp,Qi(1,i),3)
        enddo
        if(NDimI(KPhase).eq.1) then
          call Multm(Qr,tt,xp,1,3,3)
          call CopyVek(xp,Qr,3)
        endif
      endif
      call MatFromBlock3(tt,tt6,NDim(KPhase))
      call Multm(TrM,tt6,tt,NDim(KPhase),NDim(KPhase),NDim(KPhase))
      call CopyMat(tt,TrM,NDim(KPhase))
      Cell(1)=sqrt(aa)
      Cell(2)=sqrt(bb)
      Cell(3)=sqrt(cc)
      Cell(4)=acos(.5*ksi/(Cell(2)*Cell(3)))/ToRad
      Cell(5)=acos(.5*eta/(Cell(1)*Cell(3)))/ToRad
      Cell(6)=acos(.5*zeta/(Cell(1)*Cell(2)))/ToRad
9999  return
      end
