      subroutine DRBasicSCDLANL
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      LamAveRefBlock(KRefBlock)=-1.
      FormatRefBlock(KRefBlock)='(3i4,2f8.2,i4,f8.4,f7.4)'
      return
      end
