      subroutine CheckProblems(uk,iuk,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension uk(4),iuk(4),ScW(2)
      integer FromAngle(4),ToAngle(4),a1,ao,ad1,ad2,a2,ac1,ac2
      call AngleInSteps(uk,iuk)
      if(width3.gt.1.) then
        sth=sin(uk(2))*width3
        if(sth.lt.1.) then
          ith=nint(asin(sth)*StepKuma(1))
          iuk(1)=iuk(1)-iuk(2)/2+ith
          iuk(2)=2*ith
        endif
      endif
      ScW(1)=(width1+width2*tan(uk(2)))*DegKuma(1)
      if(abs(uk(2)).gt.TransTheta.and.FractKuma.gt.0.) then
        ScW(2)=ScW(1)*FractKuma
      else
        ScW(2)=0
      endif
      FromAngle(3)=iuk(3)
      FromAngle(4)=iuk(4)
      if(ModeKuma.gt.0) then
        m=StepsKuma-1
        fm=float(m)
        w1=ScW(1)/fm
        FromAngle(1)=iuk(1)-nint(.5*w1*fm)
        w2=ScW(2)/fm
        FromAngle(2)=iuk(2)-nint(.5*w2*fm)
        if(ModeKuma.eq.2) then
          FromAngle(1)=FromAngle(1)+nint(.5*w1)
          FromAngle(2)=FromAngle(2)+nint(.5*w2)
        endif
        call CopyVekI(FromAngle,ToAngle,4)
        ToAngle(1)=FromAngle(1)+nint(w1)*m
        ToAngle(2)=FromAngle(2)+nint(w2)*m
      else
        FromAngle(1)=iuk(1)-ScW(1)*.5
        FromAngle(2)=iuk(2)-ScW(2)*.5
        call CopyVekI(FromAngle,ToAngle,4)
        ToAngle(1)=FromAngle(1)+ScW(1)
        ToAngle(2)=FromAngle(2)+ScW(2)
      endif
      call KOTArea(a1,ao,FromAngle)
      call KOTArea(a2,ao,  ToAngle)
      ich=0
      if(iabs(a1).le.2.and.iabs(a2).le.2.and.a1.eq.a2) then
        call CheckBeam(ad1,ac1,FromAngle,ich)
        call CheckBeam(ad2,ac2,  ToAngle,ich)
        if(ich.eq.0) then
          if(ad1.ne.ad2) then
            ich=4
          else if(ac1.ne.ac2) then
            ich=17
          endif
        endif
      else if(iabs(a1).eq.9.or.iabs(a2).eq.9) then
        ich=3
      else
        ich=5
      endif
      return
      end
