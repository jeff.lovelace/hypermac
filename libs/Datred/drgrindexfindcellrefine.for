      subroutine DRGrIndexFindCellRefine(Klic,Vol,ich)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real AM(9),AMI(9),PS(9),hp(6),TrPom(9),TrPomI(9)
      integer, allocatable :: FlagIOld(:)
      logical EqIV
      if(allocated(FlagIOld)) deallocate(FlagIOld)
      allocate(FlagIOld(GrIndexNAll))
      ich=0
1600  if(IndexedAll.le.3) go to 9900
      mm=0
      do 1700i=1,NTripl
        call MultM(UBITripl(1,1,i),UB(1,1,KRefBlock),TrPom,3,3,3)
        do j=1,9
          if(abs(TrPom(j)-anint(TrPom(j))).gt..01) go to 1700
        enddo
        if(Klic.eq.0) then
          ich=2
          go to 9999
        endif
1700  continue
      call SetRealArrayTo(AM,9,0.)
      call SetRealArrayTo(PS,9,0.)
      do i=1,GrIndexNAll
        if(FlagI(i).le.0.or.FlagU(i).le.0) cycle
        pom=0.
        do j=1,3
          hp(j)=iha(j,i)
          pom=pom+abs(hp(j))
        enddo
        if(pom.lt..99) cycle
        m=0
        do j=1,3
          do k=1,3
            m=m+1
            AM(m)=AM(m)+hp(j)*hp(k)
          enddo
        enddo
        m=0
        do k=1,3
          do j=1,3
            m=m+1
            PS(m)=PS(m)+Slozky(j,i)*hp(k)
          enddo
        enddo
      enddo
      call matinv(AM,AMI,det,3)
      call Multm(PS,AMI,UB(1,1,KRefBlock),3,3,3)
      call CopyVekI(FlagI,FlagIOld,GrIndexNAll)
      call MatInv(UB(1,1,KRefBlock),UBI(1,1,KRefBlock),Vol,3)
      if(Vol.eq.0.) go to 9900
      Vol=1./Vol
      mm=mm+1
      call DRGrIndexTryInd
      if(.not.EqIV(FlagI,FlagIOld,GrIndexNAll).and.mm.lt.50)
     1  go to 1600
      go to 9999
9900  ich=1
9999  if(allocated(FlagIOld)) deallocate(FlagIOld)
      return
      end
