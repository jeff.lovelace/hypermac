      subroutine DRRecSpaceViewerOsnova
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xx(3),xo(3),xp(3),XAxis(3,2),XVen(2,2)
      integer Colour,FeRGBCompress
      character*80 Veta
      if(SimDrawTwins.and.SimUseDefaultNet) then
        NDrTw=NTwin
      else
        NDrTw=1
      endif
      do itw=NDrTw,1,-1
        xx(3)=SimLayer
        do i=0,2
          call SetRealArrayTo(xx,2,0.)
          if(SimUseDefaultNet) then
            if(i.ne.0) then
              xx(i)=1.
            endif
            if(itw.ne.1) then
              xp=xx
              call MultM(xp,SimTr,xx,1,3,3)
              call MultM(xx,RtwI(1,itw),xp,1,3,3)
              call MultM(xp,SimTrI,xx,1,3,3)
            endif
          else
            if(i.gt.0) xx(1:2)=SimNet(1:2,i)
          endif
          call FeXf2X(xx,xo)
          if(i.eq.0) then
            do j=1,3
              do k=1,3
                XAxis(k,j)=-xo(k)
              enddo
            enddo
          else
            call AddVek(XAxis(1,i),xo,XAxis(1,i),3)
          endif
        enddo
        do i=1,2
          j=3-i
          do izn=1,-1,-2
            n=0
1200        call SetRealArrayTo(xx,2,0.)
            if(SimUseDefaultNet) then
              xx(i)=n*izn
              if(itw.ne.1) then
                xp=xx
                call MultM(xp,SimTr,xx,1,3,3)
                call MultM(xx,RtwI(1,itw),xp,1,3,3)
                call MultM(xp,SimTrI,xx,1,3,3)
              endif
            else
              xx(1:2)=float(n*izn)*SimNet(1:2,i)
            endif
            call FeXf2X(xx,xo)
            iuz=0
            if(XAxis(1,j).ne.0.) then
              pom=(XMinAcWin-xo(1))/XAxis(1,j)*XAxis(2,j)+xo(2)
              if(pom.ge.YMinAcWin.and.pom.le.YMaxAcWin) then
                iuz=iuz+1
                XVen(1,iuz)=XMinAcWin
                XVen(2,iuz)=pom
              endif
              pom=(XMaxAcWin-xo(1))/XAxis(1,j)*XAxis(2,j)+xo(2)
              if(pom.ge.YMinAcWin.and.pom.le.YMaxAcWin) then
                iuz=iuz+1
                XVen(1,iuz)=XMaxAcWin
                XVen(2,iuz)=pom
              endif
            endif
            if(XAxis(2,j).ne.0..and.iuz.lt.2) then
              pom=(YMinAcWin-xo(2))/XAxis(2,j)*XAxis(1,j)+xo(1)
              if(pom.ge.XMinAcWin.and.pom.le.XMaxAcWin) then
                iuz=iuz+1
                XVen(1,iuz)=pom
                XVen(2,iuz)=YMinAcWin
              endif
              if(iuz.lt.2) then
                pom=(YMaxAcWin-xo(2))/XAxis(2,j)*XAxis(1,j)+xo(1)
                if(pom.ge.XMinAcWin.and.pom.le.XMaxAcWin) then
                  iuz=iuz+1
                  XVen(1,iuz)=pom
                  XVen(2,iuz)=YMaxAcWin
                endif
              endif
            endif
            if(iuz.ge.2) then
              xu(1)=XVen(1,1)
              xu(2)=XVen(1,2)
              yu(1)=XVen(2,1)
              yu(2)=XVen(2,2)
              if(Ntwin.gt.1.and.SimDrawTwins.and.SimUseDefaultNet) then
                Colour=SimColorTwin(itw)
              else
                Colour=White
              endif
              if(n.eq.0) then
                nn=3
              else
                nn=1
              endif
              do ii=1,nn
                if(i.eq.1) then
                  xu(1)=XVen(1,1)+(float(ii-2))/EnlargeFactor
                  xu(2)=XVen(1,2)+(float(ii-2))/EnlargeFactor
                  yu(1)=XVen(2,1)
                  yu(2)=XVen(2,2)
                else
                  xu(1)=XVen(1,1)
                  xu(2)=XVen(1,2)
                  yu(1)=XVen(2,1)+(float(ii-2))/EnlargeFactor
                  yu(2)=XVen(2,2)+(float(ii-2))/EnlargeFactor
                endif
                call FePolyline(2,xu,yu,Colour)
              enddo
              n=n+1
              go to 1200
            endif
          enddo
        enddo
      enddo
      if(HardCopy.eq.0) then
        call FeFillRectangle(XMinBasVec,XMaxBasVec,YMinBasVec,
     1                       YMaxBasVec,4,0,0,LightGray)
        aa=sqrt(VecOrtScal(XAxis(1,1),XAxis(1,1),2))
        bb=sqrt(VecOrtScal(XAxis(1,2),XAxis(1,2),2))
        ab=VecOrtScal(XAxis(1,1),XAxis(1,2),2)
        angle=acos(ab/(aa*bb))
        pom=max(-bb*cos(angle),0.)
        xl=aa+pom
        yl=bb*cos(angle-pi/2.)
        ratio=(XMaxBasVec-XMinBasVec-10.)/max(xl,yl)
        xl=xl*ratio
        yl=yl*ratio
        xu(1)=(XMaxBasVec+XMinBasVec-xl)*.5+pom*ratio
        yu(1)=YMaxBasVec-yl
        xu(2)=XAxis(1,1)*ratio+xu(1)
        yu(2)=XAxis(2,1)*ratio+yu(1)
        call FePolyline(2,xu,yu,Black)
        if(SimUseDefaultNet) then
          do i=1,3
            ih(i)=nint(SimTr(1,i))
          enddo
        else
          do i=1,3
            ih(i)=nint(SimTr(1,i)*SimNet(1,1)+SimTr(2,i)*SimNet(2,1))
          enddo
        endif
        call MakeHKLString(ih,3,Veta)
        xpom=xu(2)-FeTxLength(Veta)
        ypom=yu(2)-6.
        call FeQuestAbsLblMake(idSim,xpom,ypom,Veta,'L','N')
        xu(2)=XAxis(1,2)*ratio+xu(1)
        yu(2)=XAxis(2,2)*ratio+yu(1)
        call FePolyline(2,xu,yu,Black)
        if(SimUseDefaultNet) then
          do i=1,3
            ih(i)=nint(SimTr(2,i))
          enddo
        else
          do i=1,3
            ih(i)=nint(SimTr(1,i)*SimNet(1,2)+SimTr(2,i)*SimNet(2,2))
          enddo
        endif
        call MakeHKLString(ih,3,Veta)
        xpom=xu(2)+2.
        ypom=yu(2)-6.
        call FeQuestAbsLblMake(idSim,xpom,ypom,Veta,'L','N')
        LblTo=LblTo-2
      endif
      if(Ntwin.gt.1.and.SimDrawTwins.and..not.SimUseDefaultNet) then
        do itw=1,NTwin
          call SetRealArrayTo(xp,2,0.)
          xp(3)=SimLayer
          call MultM(xp,SimTr,xx,1,3,3)
          call MultM(xx,RtwI(1,itw),xp,1,3,3)
          call MultM(xp,SimTrI,xx,1,3,3)
          call FeXf2X(xx,xo)
          xu(1)=xo(1)
          yu(1)=xo(2)
          xu(5)=xo(1)
          yu(5)=xo(2)
          xp(1)=1.
          xp(2)=0.
          xp(3)=SimLayer
          call MultM(xp,SimTr,xx,1,3,3)
          call MultM(xx,RtwI(1,itw),xp,1,3,3)
          call MultM(xp,SimTrI,xx,1,3,3)
          call FeXf2X(xx,xo)
          xu(2)=xo(1)
          yu(2)=xo(2)
          xp(1)=1.
          xp(2)=1.
          xp(3)=SimLayer
          call MultM(xp,SimTr,xx,1,3,3)
          call MultM(xx,RtwI(1,itw),xp,1,3,3)
          call MultM(xp,SimTrI,xx,1,3,3)
          call FeXf2X(xx,xo)
          xu(3)=xo(1)
          yu(3)=xo(2)
          xp(1)=0.
          xp(2)=1.
          xp(3)=SimLayer
          call MultM(xp,SimTr,xx,1,3,3)
          call MultM(xx,RtwI(1,itw),xp,1,3,3)
          call MultM(xp,SimTrI,xx,1,3,3)
          call FeXf2X(xx,xo)
          xu(4)=xo(1)
          yu(4)=xo(2)
          call FePolyline(5,xu,yu,SimColorTwin(itw))
        enddo
      endif
      return
      end
