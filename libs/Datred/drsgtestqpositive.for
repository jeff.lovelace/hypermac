      subroutine DRSGTestQPositive(TrPom)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension TrPom6(36),TrPom6I(36),TrPom(3,3)
      call UnitMat(TrPom6,NDim(KPhase))
      if(NDimI(KPhase).eq.1) then
        do i=1,3
          if(QuIrrSel(i,1).lt.-.00001) then
            TrPom6(16)=-1.
            call RealVectorToOpposite(QuIrrSel(1,1),
     1                                QuIrrSel(1,1),3)
            call RealVectorToOpposite(QuRacSel,QuRacSel,3)
            go to 1015
          else if(QuIrrSel(i,1).gt..00001) then
            go to 1015
          endif
        enddo
1015    do i=1,3
          if(QuRacSel(i,1).lt.-.00001) then
            QuRacSel(i,1)=1.+QuRacSel(i,1)
            TrPom6(4*i)=1.
          endif
        enddo
        call Multm(CellTrSel6,TrPom6,TrPom6I,NDim(KPhase),NDim(KPhase),
     1             NDim(KPhase))
        call CopyMat(TrPom6I,CellTrSel6,NDim(KPhase))
      endif
      do l=1,3
        do k=1,3
          m=k+(l-1)*NDim(KPhase)
          TrPom6(m)=TrPom(k,l)
        enddo
      enddo
      call MatInv(TrPom6,TrPom6I,pom,NDim(KPhase))
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6(1,i,1,KPhase),TrPom6,NDim(KPhase))
        call Multm(TrPom6I,TrPom6,vt6(1,i,1,KPhase),NDim(KPhase),
     1             NDim(KPhase),1)
        call od0do1(vt6(1,i,1,KPhase),vt6(1,i,1,KPhase),NDim(KPhase))
      enddo
      return
      end
