      subroutine DRReadRefTrIntInd(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension h(*),hf(6),hh(6)
      Klic=0
      do i=1,maxNDim
        hf(i)=ih(i)
        if(i.gt.3) MMaxRefBlock(KRefBlock)=
     1               max(MMaxRefBlock(KRefBlock),iabs(ih(i)))
      enddo
      go to 2000
      entry DRReadRefTrRealInd(h,ich)
      Klic=1
      call SetRealArrayTo(hf,6,0.)
      call CopyVek(h,hf,NDim95(KRefBlock))
2000  ich=0
      if(UseTrRefBlock(KRefBlock)) then
        call MultM(hf,TrRefBlock(1,KRefBlock),hh,1,maxNDim,maxNDim)
        if(IntTrRefBlock(KRefBlock)) then
          do i=1,maxNDim
            ih(i)=nint(hh(i))
          enddo
          iflg(2)=ITwRead(KRefBlock)
          KPhase=KPhaseTwin(iflg(2))
          go to 9999
        else
          if(Klic.eq.0) then
            do i=4,NDim(KPhaseDR)
              MMaxRefBlock(KRefBlock)=
     1                 max(MMaxRefBlock(KRefBlock),nint(abs(hh(i))))
            enddo
          endif
        endif
      else
        call CopyVek(hf,hh,NDim(KPhase))
      endif
      isw=1
      call MultM(hh,TrMP,hf,1,maxNDim,maxNDim)
      do i=1,maxNDim
        ihp(i)=anint(hf(i))
      enddo
      do i=1,3
        HReadReal(i)=hf(i)
        do j=1,maxNDim-3
          HReadReal(i)=HReadReal(i)+quRefBlock(i,j,0)*hf(j+3)
        enddo
      enddo
      if(HKLF5RefBlock(KRefBlock).eq.0) then
        if(ITwRead(KRefBlock).ne.1) then
          call CopyVek(HReadReal,hh,3)
          call Multm(hh,rtwi(1,ITwRead(KRefBlock)),HReadReal,1,3,3)
        endif
        call IndFromIndReal(HReadReal,MMaxRefBlock(KRefBlock),
     1                      DiffSatRefBlock(1,KRefBlock),ihp,iflg(2),
     2                      isw,-1.,CheckExtRefNo)
        if(isw.gt.0) KPhase=KPhaseTwin(iflg(2))
      endif
      if(isw.le.0) then
        ich=1
      else
        call multmIRI(ihp,TrMPI,ih,1,NDim(KPhase),NDim(KPhase))
      endif
9999  return
      end
