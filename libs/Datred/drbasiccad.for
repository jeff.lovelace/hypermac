      subroutine DRBasicCad
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      dimension dfa(2)
      character*80 veta
      logical FeYesNo
      NDim95(KRefBlock)=3
      call DRGetCADRecord(veta,31,1,*9000,*9100)
      read(veta,100,err=9100)((ub(i,j,KRefBlock),j=1,3),i=1,2)
      call DRGetCADRecord(veta,32,1,*9000,*9100)
      read(veta,100,err=9100)(ub(3,j,KRefBlock),j=1,3),dfa,attfac
      call DRCellFromUB
      call DRGetCADRecord(veta,26,1,*9000,*9100)
      read(veta,'(35x,2f10.7)',err=9100) snalfaq,csalfa
      snalfa=sqrt(snalfaq)
      csalfaq=csalfa**2
      call DRGetCADRecord(veta,22,1,*9000,*9100)
      read(veta,'(16x,2f5.2,10x,i2,6x,f6.3,6x,i2)',err=9100)
     1  doma,domb,i,pom,CAD4ProfFlag
      ThOmRatio=float(i)*.333333
      Profil=CAD4ProfFlag.ne.0
      if(CAD4ProfFlag.eq.1) then
        CAD4ProfFormat='(4(i5,2i4))'
      else if(CAD4ProfFlag.eq.4) then
        CAD4ProfFormat='(2x,12i5)'
      else if(CAD4ProfFlag.eq.6) then
        CAD4ProfFormat='(2x,12z5)'
      else
        Profil=.false.
        CAD4ProfFormat=' '
      endif
      if(Profil) then
        BerBPB=.not.FeYesNo(-1.,-1.,'Do you want to use I and '//
     1                      'sig(I) from profile analysis?',0)
      else
        BerBPB=.true.
      endif
1200  call DRGetCADRecord(veta,23,1,*9000,*9100)
      read(veta,'(4x,i2)',err=9100) NonEqCheck
      call SetRealArrayTo(SenseOfAngle,4,-1.)
      PocitatDirCos=.true.
      DirCosFromPsi=.false.
      go to 9999
9000  call FeChybne(-1.,-1.,'CAD4 - basic information missing.',' ',
     1              SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  ErrFlag=1
9999  return
100   format(4x,2(2x,3f9.6))
      end
