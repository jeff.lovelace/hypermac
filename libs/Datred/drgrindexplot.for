      subroutine DRGrIndexPlot
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real :: xe(3,8),xo(3),xp(3),xq(3),xd(3),xoe(3,8),xon(3,6)
      character*2 t2
      integer :: Color,IndexCoorSystemOld=-999,nn(3)=(/2,2,2/),nd(3),
     1           ion(6)
      logical InsideXYPlot
      call FeDeferOutput
      call FeClearGrWin
      call FeMakeAcFrame
      call FeFillRectangle(XCornAcWin(1,1),XCornAcWin(1,3),
     1                     XCornAcWin(2,1)-80.,XCornAcWin(2,1)-20.,
     2                     4,0,0,White)
      call FeFillRectangle(XCornAcWin(1,1)-80.,XCornAcWin(1,1)-20.,
     1                     XCornAcWin(2,1),XCornAcWin(2,2),
     2                     4,0,0,White)
      ProjNX=0
      ProjNMaxX=1
      ProjNY=0
      ProjNMaxY=1
      if(LatticeOn) then
        if(CellProjection) then
          do i=1,8
            call RecUnpack(i,nd,nn,3)
            do k=1,3
              xe(k,i)=(nd(k)-1)*GrIndexSuper(k)
            enddo
            do k=1,3
              xp(k)=xe(k,i)-float(GrIndexSuper(k))*.5
            enddo
            call MultM(UB(1,1,KRefBlock),xp,xo,3,3,1)
            call MultM(ViewMatrix,xo,xoe(1,i),3,3,1)
          enddo
          n=0
          j=0
          do i=1,2
            do k=1,3
              xp(k)=xoe(k,2+j)-xoe(k,1+j)
              xq(k)=xoe(k,3+j)-xoe(k,1+j)
            enddo
            n=n+1
            ion(n)=j+1
            call VecMul(xp,xq,xon(1,n))
            call VecOrtNorm(xon(1,n),3)
            if(i.eq.2) call RealVectorToOpposite(xon(1,n),xon(1,n),3)
            j=j+4
          enddo
          j=0
          do i=1,2
            do k=1,3
              xp(k)=xoe(k,3+j)-xoe(k,1+j)
              xq(k)=xoe(k,5+j)-xoe(k,1+j)
            enddo
            n=n+1
            ion(n)=j+1
            call VecMul(xp,xq,xon(1,n))
            call VecOrtNorm(xon(1,n),3)
            if(i.eq.2) call RealVectorToOpposite(xon(1,n),xon(1,n),3)
            j=j+1
          enddo
          j=0
          do i=1,2
            do k=1,3
              xp(k)=xoe(k,5+j)-xoe(k,1+j)
              xq(k)=xoe(k,2+j)-xoe(k,1+j)
            enddo
            n=n+1
            ion(n)=j+1
            call VecMul(xp,xq,xon(1,n))
            call VecOrtNorm(xon(1,n),3)
            if(i.eq.2) call RealVectorToOpposite(xon(1,n),xon(1,n),3)
            j=j+2
          enddo
          do i1=1,3
            i2=mod(i1,3)+1
            i3=mod(i1+1,3)+1
            xp(i1)=-float(GrIndexSuper(i1))*.5
            xq(i1)=GrIndexSuper(i1)-float(GrIndexSuper(i1))*.5
            do j=0,GrIndexSuper(i2)
              xp(i2)=float(j)-float(GrIndexSuper(i2))*.5
              xq(i2)=xp(i2)
              do k=0,GrIndexSuper(i3)
                xp(i3)=float(k)-float(GrIndexSuper(i3))*.5
                xq(i3)=xp(i3)
                call MultM(UB(1,1,KRefBlock),xp,xo,3,3,1)
                call MultM(ViewMatrix,xo,xd,3,3,1)
                call FeXf2X(xd,xo)
                xu(1)=xo(1)
                yu(1)=xo(2)
                call MultM(UB(1,1,KRefBlock),xq,xo,3,3,1)
                call MultM(ViewMatrix,xo,xd,3,3,1)
                call FeXf2X(xd,xo)
                xu(2)=xo(1)
                yu(2)=xo(2)
                call FePolyLine(2,xu,yu,White)
              enddo
            enddo
          enddo
          if(NDimI(KPhase).gt.0) then
            xp(1:3)=0.
            xp(3)=DiffMax
            call MultM(UB(1,1,KRefBlock),xp,xq,3,3,1)
            call FeXf2X(xq,xo)
            xp(1:3)=0.
            call FeXf2X(xp,xq)
            xo=xo-xq
            Rad=sqrt(xo(1)**2+xo(2)**2+xo(3)**2)*4.
            if(NDimI(KPhase).gt.1) then
              i2p= MMin(2)
              i2k= MMax(2)
            else
              i2p=0
              i2k=0
            endif
            if(NDimI(KPhase).gt.2) then
              i3p= MMin(3)
              i3k= MMax(3)
            else
              i3p=0
              i3k=0
            endif
            do i1=MMin(1),MMax(1)
              do i2=i2p,i2k
                do i3=i2p,i2k
                  xp(1:3)=0.
                  do j=1,3
                    xp(j)=float(i1)*QuRefBlock(j,1,KRefBlock)+
     1                    float(i2)*QuRefBlock(j,2,KRefBlock)+
     2                    float(i3)*QuRefBlock(j,3,KRefBlock)
                  enddo
                  call od0do1(xp,xp,3)
                  do j1=-GrIndexSuper(1),GrIndexSuper(1)
                    xd(1)=j1
                    do j2=-GrIndexSuper(2),GrIndexSuper(2)
                      xd(2)=j2
                      do 1150j3=-GrIndexSuper(3),GrIndexSuper(3)
                        xd(3)=j3
                        call AddVek(xp,xd,xq,3)
                        do j=1,3
                          xq(j)=xq(j)-float(GrIndexSuper(j))*.5
                        enddo
                        call MultM(UB(1,1,KRefBlock),xq,xo,3,3,1)
                        call MultM(ViewMatrix,xo,xq,3,3,1)
                        do j=1,6
                          do k=1,3
                            xo(k)=xq(k)-xoe(k,ion(j))
                          enddo
                          if(ScalMul(xo,xon(1,j)).lt.-DiffMax)
     1                      go to 1150
                        enddo
                        call FeXf2X(xq,xo)
                        call FeCircleOpen(xo(1),xo(2),Rad,Green)
1150                  continue
                    enddo
                  enddo
                enddo
              enddo
            enddo
          endif
        else
          ionn=0
          pomx=0.
          do i=1,3
            call SetRealArrayTo(xp,3,0.)
            xp(i)=1.
            call MultM(UB(1,1,KRefBlock),xp,xo,3,3,1)
            call MultM(ViewMatrix,xo,xq,3,3,1)
            call VecOrtNorm(xq,3)
            pom=abs(xq(3))
            if(pom.gt.pomx) then
              ionn=i
              pomx=pom
            endif
          enddo
          call SetRealArrayTo(xp,3,0.)
          Color=Green
          do io1=1,3
            if(io1.eq.ionn) cycle
            io2=6-io1-ionn
            do iz=-1,1,2
              if(iz.eq.-1) then
                i=0
              else
                i=1
              endif
1200          xp(io1)=i
              xp(io2)=1
              call MultM(UB(1,1,KRefBlock),xp,xo,3,3,1)
              call MultM(ViewMatrix,xo,xq,3,3,1)
              call FeXf2X(xq,xo)
              xu(1)=xo(1)
              yu(1)=xo(2)
              xp(io2)=-1.
              call Multm(UB(1,1,KRefBlock),xp,xo,3,3,1)
              call MultM(ViewMatrix,xo,xq,3,3,1)
              call FeXf2X(xq,xo)
              xu(2)=xo(1)
              yu(2)=xo(2)
              call FeExpandLineToActiveWindow(xu(1),yu(1),xu(2),yu(2),
     1                                      xu(1),yu(1),xu(2),yu(2),ich)
              if(ich.eq.0) then
                call FePolyLine(2,xu,yu,Color)
                i=i+iz
                go to 1200
              endif
            enddo
            Color=Red
          enddo
        endif
      endif
      do 2000i=1,GrIndexNAll
        if(FlagU(i).le.0) go to 2000
        if(GrIndexSkipIndexed.and.FlagI(i).gt.0) then
          FlagS(i)=0
          go to 2000
        endif
        if(GrIndexUseSLim.and.
     1     (Slozky(4,i).lt.GrIndexSMin*2..or.
     2      Slozky(4,i).gt.GrIndexSMax*2.)) then
          FlagS(i)=0
          go to 2000
        endif
        if(GrIndexUseILim.and.
     1     (Int(i).lt.GrIndexIMin.or.
     2      Int(i).gt.GrIndexIMax)) then
          FlagS(i)=0
          go to 2000
        endif
        FlagS(i)=1
        if(CellProjection) then
          call MultM(UBI(1,1,KRefBlock),Slozky(1,i),xp,3,3,1)
          call od0don(xp,xp,GrIndexSuper,3)
          do j=1,3
            xp(j)=xp(j)-float(GrIndexSuper(j))*.5
          enddo
          call MultM(UB(1,1,KRefBlock),xp,xq,3,3,1)
          call Multm(ViewMatrix,xq,xp,3,3,1)
        else
          call Multm(ViewMatrix,Slozky(1,i),xp,3,3,1)
        endif
        call FeXf2X(xp,xo)
        if(InsideXYPlot(xo(1),xo(2))) then
          if(GrIndexRad.lt.1.) then
            call FePoint(xo(1),xo(2),GrIndexColor)
          else
            call FeCircle(xo(1),xo(2),GrIndexRad,GrIndexColor)
          endif
          do j=1,ProjNX
            pom=xo(1)*EnlargeFactor-ProjX(j)
            if(pom.le..5.and.pom.gt.-.5) then
              ProjMX(j)=ProjMX(j)+1
              ProjNMaxX=max(ProjNMaxX,ProjMX(j))
              go to 1500
            endif
          enddo
          ProjNX=ProjNX+1
          ProjMX(ProjNX)=1
          ProjX(ProjNX)=anint(xo(1)*EnlargeFactor)
1500      do j=1,ProjNY
            pom=xo(2)*EnlargeFactor-ProjY(j)
            if(pom.le..5.and.pom.gt.-.5) then
              ProjMY(j)=ProjMY(j)+1
              ProjNMaxY=max(ProjNMaxY,ProjMY(j))
              go to 2000
            endif
          enddo
          ProjNY=ProjNY+1
          ProjMY(ProjNY)=1
          ProjY(ProjNY)=anint(xo(2)*EnlargeFactor)
        endif
2000  continue
      do i=1,ProjNX
        xu(1)=ProjX(i)/EnlargeFactor
        xu(2)=ProjX(i)/EnlargeFactor
        yu(1)=XCornAcWin(2,1)-20.
        yu(2)=yu(1)-60.*float(ProjMX(i))/float(ProjNMaxX)
        call FePolyLine(2,xu,yu,Black)
      enddo
      do i=1,ProjNY
        yu(1)=ProjY(i)/EnlargeFactor
        yu(2)=ProjY(i)/EnlargeFactor
        xu(1)=XCornAcWin(1,1)-20.
        xu(2)=xu(1)-60.*float(ProjMY(i))/float(ProjNMaxY)
        call FePolyLine(2,xu,yu,Black)
      enddo
      if(IndexCoorSystemOld.gt.0.and.
     1   IndexCoorSystemOld.ne.IndexCoorSystem) then
        call FePlotMode('E')
        xu1=(XCornAcWin(1,3)+XMaxGrWin)*.5
        yu1=(XCornAcWin(2,3)+XCornAcWin(2,4))*.5
        do i=1,3
          if(IndexCoorSystem.eq.1) then
            call SetRealArrayTo(xp,3,0.)
            xp(i)=1.
            t2=SmbX(i)
          else if(IndexCoorSystem.eq.2) then
            do j=1,3
              xp(j)=CellDirI(i,j)
            enddo
            t2=SmbABC(i)//'*'
          else if(IndexCoorSystem.eq.3) then
            call CopyVek(CellDir(1,i),xp,3)
            t2=SmbABC(i)
          endif
          call MultM(ViewMatrix,xp,xo,3,3,1)
          pom=sqrt(xo(1)**2+xo(2)**2+xo(3)**2)
          xu(1)=xu1
          yu(1)=yu1
          xu2=xu1+50.*xo(1)/pom
          yu2=yu1+50.*xo(2)/pom
          xu(2)=xu2
          yu(2)=yu2
          call FePolyLine(2,xu,yu,Magenta)
          pom=sqrt(xo(1)**2+xo(2)**2)
          if(pom.gt.0.) then
            do j=1,3
              xo(j)=xo(j)/pom
            enddo
            dx=10.*xo(1)
            dy=10.*xo(2)
          else
            dx=10.
            dy=10.
          endif
          xu(1)=xu2
          yu(1)=yu2
          xu(2)=xu2-7.*xo(1)+4.*xo(2)
          yu(2)=yu2-7.*xo(2)-4.*xo(1)
          xu(3)=xu2-4.*xo(1)
          yu(3)=yu2-4.*xo(2)
          xu(4)=xu2-7.*xo(1)-4.*xo(2)
          yu(4)=yu2-7.*xo(2)+4.*xo(1)
          call FePolygon(xu,yu,4,4,0,0,Magenta)
          if(xo(1).lt.0.) dx=dx-FeTxLength(SmbX(i))
          call FeOutSt(0,xu2+dx,yu2+dy,t2,'L',Magenta)
        enddo
        call FePlotMode('N')
      endif

      if(IndexCoorSystem.gt.0) then
        xu1=(XCornAcWin(1,3)+XMaxGrWin)*.5
        yu1=(XCornAcWin(2,3)+XCornAcWin(2,4))*.5
        do i=1,3
          if(IndexCoorSystem.eq.1) then
            call SetRealArrayTo(xp,3,0.)
            xp(i)=1.
            t2=SmbX(i)
          else if(IndexCoorSystem.eq.2) then
            do j=1,3
              xp(j)=CellDirI(i,j)
            enddo
            t2=SmbABC(i)//'*'
          else if(IndexCoorSystem.eq.3) then
            call CopyVek(CellDir(1,i),xp,3)
            t2=SmbABC(i)
          endif
          call MultM(ViewMatrix,xp,xo,3,3,1)
          pom=sqrt(xo(1)**2+xo(2)**2+xo(3)**2)
          xu(1)=xu1
          yu(1)=yu1
          xu2=xu1+50.*xo(1)/pom
          yu2=yu1+50.*xo(2)/pom
          xu(2)=xu2
          yu(2)=yu2
          call FePolyLine(2,xu,yu,Magenta)
          pom=sqrt(xo(1)**2+xo(2)**2)
          if(pom.gt.0.) then
            do j=1,3
              xo(j)=xo(j)/pom
            enddo
            dx=10.*xo(1)
            dy=10.*xo(2)
          else
            dx=10.
            dy=10.
          endif
          xu(1)=xu2
          yu(1)=yu2
          xu(2)=xu2-7.*xo(1)+4.*xo(2)
          yu(2)=yu2-7.*xo(2)-4.*xo(1)
          xu(3)=xu2-4.*xo(1)
          yu(3)=yu2-4.*xo(2)
          xu(4)=xu2-7.*xo(1)-4.*xo(2)
          yu(4)=yu2-7.*xo(2)+4.*xo(1)
          call FePolygon(xu,yu,4,4,0,0,Magenta)
          if(xo(1).lt.0.) dx=dx-FeTxLength(SmbX(i))
          call FeOutSt(0,xu2+dx,yu2+dy,t2,'L',Magenta)
        enddo
      endif
      IndexCoorSystemOld=IndexCoorSystem
      return
      end
