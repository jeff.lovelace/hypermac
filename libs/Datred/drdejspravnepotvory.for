      subroutine DRDejSpravnePotvory
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      character*80 t80,p80,ta80(2)
      equivalence (t80,ta80(1)),(p80,ta80(2))
      p80=' '
      if(NExt500(1).ne.500) call Indexx(NExt500(1),ria(1,1),
     1                                  OrderExtRef(1,1))
      write(t80,EM9Form1)(indices(i),i=1,NDim(KPhase))
      t80(idel(t80)+1:)='       I      sig(I)  I/sig(I)'
      idlp=idel(t80)
      call newln(1)
      write(lst,FormA1)(t80(i:i),i=1,idlp),(' ',i=1,10),
     1                 (t80(i:i),i=1,idlp)
      m=min(2*(mxline-Line),NExt500(1))
      ks=0
2000  k=ks
      if(mod(m,2).eq.1) m=m+1
      do i=1,m
        ip=mod(i-1,2)+1
        ks=ks+1
        if(ks.gt.NExt500(1)) go to 2300
        if(ip.eq.1) then
          k=k+1
          kk=OrderExtRef(k,1)
        else
          kk=OrderExtRef(k+m/2,1)
        endif
        write(ta80(ip),EM9Form2)(IHExt(j,kk,1),j=1,NDim(KPhase))
        pom1=-float(ria(kk,1))*.0001
        pom2= float(rsa(kk,1))*.01
        idl=idel(ta80(ip))+1
        write(ta80(ip)(idl:),100) pom1*pom2,pom2,pom1
2300    if(ip.eq.2) then
          call newln(1)
          write(lst,FormA1)(ta80(1)(j:j),j=1,idlp),(' ',j=1,10),
     1                     (ta80(2)(j:j),j=1,idlp)
                 ta80(2)=' '
        endif
      enddo
      if(ks.lt.NExt500(1)) then
        m=min(2*(mxline-3),NExt500(1)-ks)
        go to 2000
      endif
      return
100   format(1x,3f9.1)
      end
