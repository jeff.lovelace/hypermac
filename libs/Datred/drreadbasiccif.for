      subroutine DRReadBasicCIF(Klic)
      use Basic_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      parameter (MxLoop=10000)
      dimension pa(:),spa(:),ia(:)
      character*256 Veta,Radka,t256
      character*80  StLbl(:),sta(:)
      integer TypeCIFOD
      logical MameCell,EqIgCase
      allocatable pa,spa,ia,StLbl,sta
      nCIFArray=1000
      allocate(CifKey(400,40),CifKeyFlag(400,40),CIFArray(nCIFArray))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.ne.0) go to 9999
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta)
      allocate(pa(MxLoop),spa(MxLoop),ia(MxLoop),StLbl(MxLoop),
     1         sta(MxLoop))
      n=0
      NLoop=0
1100  read(70,FormA,end=1150) Veta
      n=n+1
      if(n.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
      CIFArray(n)=Veta
      call SkrtniPrvniMezery(Veta)
      if(EqIgCase(Veta,'loop_')) then
        NLoop=n
      else if(NLoop.gt.0.and.EqIgCase(Veta,CifKey(13,16))) then
1110    read(70,FormA,end=1140) Veta
        call SkrtniPrvniMezery(Veta)
        if(Veta(1:1).eq.'_') go to 1110
1120    read(70,FormA,end=1140) Veta
        call SkrtniPrvniMezery(Veta)
        if(Veta(1:1).ne.'_'.and..not.EqIgCase(Veta,'loop_')) go to 1120
        backspace(70)
        n=NLoop-1
        go to 1100
1140    n=NLoop-1
        go to 1150
      else if(NLoop.gt.0.and.Veta(1:1).ne.'_') then
        NLoop=0
      endif
      go to 1100
1150  nCIFUsed=n
      MameCell=.false.
      n=10
      m=51
      call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
        go to 9900
      else if(ich.eq.-1) then
        pom=LamAveD(6)
      endif
      LamA1RefBlock(KRefBlock)=pom
      LamA2RefBlock(KRefBlock)=pom
      LamAveRefBlock(KRefBlock)=pom
      do m=1,9
        call CIFGetReal(CifKey(m+23,n),pom,spom,Veta,ich)
        if(ich.ne.0) then
          if(ich.eq.-1) then
            Veta='Orientation matrix is missing or incomplete.'
            go to 9100
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          endif
        endif
        i=(m-1)/3+1
        j=mod(m-1,3)+1
        if(Klic.eq.0) then
          ub(i,j,KRefBlock)=pom
        else if(Klic.eq.1) then
          ub(j,i,KRefBlock)=pom
        else if(Klic.eq.2) then
          ub(i,j,KRefBlock)=pom
        endif
      enddo
      n=5
      if(Klic.eq.0) then
        m=157
      else
        m=5
      endif
      do i=1,6
        call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
        if(ich.ne.0) then
          if(ich.eq.-1) then
            Veta='Cell parameters are missing or incomplete.'
            go to 9100
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          endif
        endif
        CellRefBlock  (i,KRefBlock)=pom
        CellRefBlockSU(i,KRefBlock)=spom
        MameCell=.true.
        m=m+1
        if(i.eq.3.and.Klic.ne.0) m=m-7
      enddo
      do i=1,3
        Veta=CifKey(146+i,5)
        call CIFGetReal(Veta,quRefBlock(i,1,KRefBlock),pom,t256,ich)
        if(ich.le.0) then
          if(ich.ge.-1) then
            cycle
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
        call CIFGetLoop(CifKey(150,5),StLbl,MxLoop,Veta,ia,Sta,pa,
     1                  spa,1,n,t256,ich)
        if(ich.le.0) then
          if(ich.eq.-1) then
            cycle
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
        jj=0
        do j=1,n
          if(StLbl(j).eq.'?') cycle
          jj=jj+1
          StLbl(jj)=StLbl(j)
          pa(jj)=pa(j)
        enddo
        n=jj
        do j=1,n
          k=0
          call kus(StLbl(j),k,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15) k
          quRefBlock(i,k,KRefBlock)=pa(k)
        enddo
        if(Klic.eq.2.and.i.eq.1) NDim95(KRefBlock)=NDim95(KRefBlock)+n
      enddo
      if(Klic.eq.0) then
        m=1
        n=25
1170    call CIFGetInt(CifKey(m,n),i,Veta,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
          go to 9900
        else if(ich.eq.0) then
          NTwin=i
          if(NPhase.eq.1) then
            do i=1,NTwin
              sctw(i,1)=1./float(NTwin)
            enddo
          endif
          if(i.gt.1) HKLF5RefBlock(KRefBlock)=1
        else if(n.eq.25) then
          m=163
          n=5
          go to 1170
        endif
        if(NTwin.gt.1) then
          ishift=7
          m=6
          n=23
1180      do i=1,9
            Veta=CifKey(ishift+i,n)
            call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,Veta,ia,Sta,pa,
     1                      spa,1,nn,t256,ich)
            if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
              go to 9900
            else if(ich.ne.0.and.n.eq.23) then
              m=164
              ishift=164
              n=5
              go to 1180
            endif
            do j=1,nn
              k=0
              call kus(StLbl(j),k,Cislo)
              call posun(Cislo,0)
              read(Cislo,FormI15) k
              rtw(i,k)=pa(k)
            enddo
          enddo
          m=7
          n=23
          Veta=CifKey(m,n)
          call CIFGetLoop(CifKey(6,23),StLbl,MxLoop,Veta,ia,Sta,pa,
     1                    spa,1,nn,t256,ich)
          if(ich.eq.0) then
            do j=1,nn
              k=0
              call kus(StLbl(j),k,Cislo)
              call posun(Cislo,0)
              read(Cislo,FormI15) k
              sctw(k,1)=pa(k)
            enddo
          endif
        endif
        m=17
        n=10
        i=1
1200    k=0
        call Kus(CIFArray(i),k,Veta)
        if(.not.EqIgCase(Veta,CifKey(m,n))) then
          i=i+1
          if(i.lt.nCIFUsed) then
            go to 1200
          else
            go to 1300
          endif
        endif
        ip=i
        TypeCIFOD=0
        Konec=0
1210    i=i+1
        k=0
        call Kus(CIFArray(i),k,Cislo)
        if(EqIgCase(Cislo,'#__')) then
          TypeCIFOD=1
          i=i-1
          go to 1215
        else if(EqIgCase(Cislo,'List')) then
          call Kus(CIFArray(i),k,Cislo)
          if(EqIgCase(Cislo,'of')) then
            call Kus(CIFArray(i),k,Cislo)
            if(EqIgCase(Cislo,'runs')) then
              TypeCIFOD=2
              go to 1230
            endif
          endif
        else if(EqIgCase(Cislo,';')) then
          if(Konec.gt.0) go to 1300
          Konec=Konec+1
        endif
        go to 1210
1215    m=2
        NRuns(KRefBlock)=0
1220    i=i+1
        if(CIFArray(i).eq.' '.or.CIFArray(i)(1:1).eq.'#'.or.
     1     LocateSubstring(CIFArray(i),'_',.false.,.true.).gt.1)
     2    go to 1220
        k=0
        call Kus(CIFArray(i),k,Cislo)
        if(Cislo.eq.';') then
          go to 1300
        endif
        m=mod(m,2)+1
        if(m.eq.1) NRuns(KRefBlock)=NRuns(KRefBlock)+1
        n=NRuns(KRefBlock)
        k=0
        do j=1,4
         call Kus(CIFArray(i),k,Cislo)
        enddo
        if(m.eq.1) then
          call Kus(CIFArray(i),k,Cislo)
          call StToReal(CIFArray(i),k,RunMeasureTime(n,KRefBlock),1,
     1                  .false.,ich)
        else
          call StToInt(CIFArray(i),k,RunNFrames(n,KRefBlock),1,.false.,
     1                 ich)
        endif
        go to 1220
1230    n=0
1240    i=i+1
        k=0
        call Kus(CIFArray(i),k,Cislo)
        if(EqIgCase(Cislo,';')) then
          NRuns(KRefBlock)=n
          go to 1300
        else if(CIFArray(i)(1:5).ne.'-----') then
          go to 1240
        endif
1250    i=i+1
        if(CIFArray(i).eq.' ') go to 1250
        k=0
        call Kus(CIFArray(i),k,Cislo)
        if(EqIgCase(Cislo,';')) then
          NRuns(KRefBlock)=n
          go to 1300
        endif
        n=n+1
        read(CIFArray(i),101,err=1270,end=1270)
     1    j,(pom,k=1,3),RunMeasureTime(n,KRefBlock),(pom,k=1,3),
     2    RunNFrames(n,KRefBlock)
        go to 1250
1270    n=n-1
        go to 1250
      endif
1300  n=11
      m=24
      do i=1,4
        m=m+1
        if(i.le.3) then
          k=-1
        else
          k=1
        endif
        call CIFGetLoop(' ',StLbl,MxLoop,CifKey(m,n),ia,Sta,pa,spa,k,
     1                  NFaces(KRefBlock),Veta,ich)
        if(ich.ne.0) then
          if(ich.eq.-1) then
            NFaces(KRefBlock)=0
            go to 2000
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          endif
        endif
        do j=1,NFaces(KRefBlock)
          if(k.lt.0) then
            DFace(i,j,KRefBlock)=ia(j)
          else
            DFace(4,j,KRefBlock)=pa(j)
          endif
        enddo
      enddo
2000  m=3
      n=10
      call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
        go to 9900
      else if(ich.eq.0) then
        TempRefBlock(KRefBlock)=pom
      endif
      if(NPhase.lt.2) then
        m=23
        n=6
        call CIFGetSt(CifKey(m,n),sta,mm,Radka,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
          go to 9900
        else if(ich.eq.0) then
          Formula(1)=sta(1)
        endif
      endif
      LnSum=NextLogicNumber()
      call OpenFile(LnSum,fln(:ifln)//'_DatColl.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# DatColl'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      n=5
      do m=10,13
        if(m.eq.10) then
          call CIFGetInt(CifKey(m,n),i,Veta,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          write(Veta,FormI15) i
        else
          call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          call RoundESD(Veta,pom,spom,2,0,0)
        endif
        call Zhusti(Veta)
        write(LnSum,FormCIF) CifKey(m,n),Veta(:idel(Veta))
      enddo
      n=11
      do i=1,5
        if(i.eq.1) then
          m=8
        else if(i.eq.2) then
          m=13
        else
          m=i+14
          call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          call RoundESD(Veta,pom,spom,3,0,0)
          go to 2100
        endif
        call CIFGetSt(CifKey(m,n),sta,mm,Radka,ich)
        Veta=sta(1)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
          go to 9900
        else if(ich.ne.0) then
          cycle
        endif
        if(mm.eq.1) then
          Veta=''''//Veta(:idel(Veta))//''''
          go to 2100
        else
          write(LnSum,FormCIF) CifKey(m,n)
          write(LnSum,100)
          write(LnSum,FormA)(sta(j)(:idel(sta(j))),j=1,mm)
          write(LnSum,100)
          cycle
        endif
2100    call Zhusti(Veta)
        write(LnSum,FormCIF) CifKey(m,n),Veta(:idel(Veta))
      enddo
      n=10
      do i=1,11
        if(i.eq.1) then
          m=45
        else if(i.eq.2) then
          m=20
        else if(i.eq.3) then
          m=11
        else if(i.eq.4) then
          m=12
        else if(i.eq.5) then
          m=22
        else if(i.eq.6) then
          m=21
        else if(i.eq.7) then
          m=110
        else if(i.eq.8) then
          m=109
        else if(i.eq.9) then
          m=113
        else if(i.eq.10) then
          m=117
        else if(i.eq.11) then
          m=111
        endif
        if(i.le.8) then
          call CIFGetSt(CifKey(m,n),sta,mm,Radka,ich)
          Veta=sta(1)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          if(m.ne.12) then
            if(mm.eq.1) then
              Veta=''''//Veta(:idel(Veta))//''''
            else
              write(LnSum,FormCIF) CifKey(m,n)
              write(LnSum,100)
              write(LnSum,FormA)(sta(j)(:idel(sta(j))),j=1,mm)
              write(LnSum,100)
              cycle
            endif
          endif
        else
          call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          call RoundESD(Veta,pom,spom,3,0,0)
        endif
        write(LnSum,FormCIF) CifKey(m,n),Veta(:idel(Veta))
      enddo
      n=8
      do m=1,3
        call CIFGetSt(CifKey(m,n),sta,mm,Radka,ich)
        Veta=sta(1)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
          go to 9900
        else if(ich.ne.0) then
          cycle
        endif
        if(mm.eq.1) then
          Veta=' '''//Veta(:idel(Veta))//''''
          write(LnSum,FormCIF) CifKey(m,n)
          write(LnSum,FormA) Veta(:idel(Veta))
        else
          write(LnSum,FormCIF) CifKey(m,n)
          write(LnSum,100)
          write(LnSum,FormA)(sta(j)(:idel(sta(j))),j=1,mm)
          write(LnSum,100)
        endif
      enddo
      call CloseIfOpened(LnSum)
      call OpenFile(LnSum,fln(:ifln)//'_Absorption.l70','formatted',
     1              'unknown')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,'(''# Absorption'')')
      write(LnSum,'(''#'',71(''=''))')
      write(LnSum,FormA)
      n=11
      do m=1,5
        if(m.ge.4) then
          call CIFGetSt(CifKey(m,n),sta,mm,Radka,ich)
          Veta=sta(1)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          if(mm.eq.1) then
            Veta=''''//Veta(:idel(Veta))//''''
          else
            write(LnSum,FormCIF) CifKey(m,n)
            write(LnSum,100)
            write(LnSum,FormA)(sta(j)(:idel(sta(j))),j=1,mm)
            write(LnSum,100)
            cycle
          endif
        else
          call CIFGetReal(CifKey(m,n),pom,spom,Veta,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            cycle
          endif
          call RoundESD(Veta,pom,spom,3,0,0)
        endif
        call Zhusti(Veta)
        write(LnSum,FormCIF) CifKey(m,n),Veta(:idel(Veta))
      enddo
      if(Klic.eq.2) then
        call ReallocateED(KRefBlock)
        call SetBasicM42(KRefBlock,-1)
        call CopyMat(ub(1,1,KRefBlock),OrMatEDZone(1,1,KRefBlock),3)
        m=174
        n=10
        call CIFGetReal(CifKey(m,n),OmEDZone(KRefBlock),spom,Veta,ich)
        do i=1,6
          call CIFGetLoop(CifKey(167,10),StLbl,MxLoop,CifKey(167+i,10),
     1                    ia,Sta,pa,spa,1,nn,t256,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 9900
          else if(ich.ne.0) then
            NEDZone(KRefBlock)=0
            go to 8000
          endif
          if(i.eq.1) call ReallocateEDZones(max(nn-NMaxEDZone,0))
          do j=1,nn
            if(i.eq.1) call SetBasicM42(-KRefBlock,j)
            k=0
            call StToInt(StLbl(j),k,ia,1,.false.,ich)
            if(ich.ne.0) go to 9100
            if(i.le.3) then
              HEDZone(i,ia(1),KRefBlock)=pa(j)
            else if(i.eq.4) then
              PrAngEDZone(ia(1),KRefBlock)=pa(j)
            else if(i.eq.5) then
              AlphaEDZone(ia(1),KRefBlock)=pa(j)
            else if(i.eq.6) then
              BetaEDZone(ia(1),KRefBlock)=pa(j)
            endif
          enddo
          NEDZone(KRefBlock)=nn
        enddo
        NRefBlockED=max(NRefBlockED,KRefBlock)
      endif
8000  call CloseIfOpened(LnSum)
      if(.not.MameCell) call DRCellFromUB
      go to 9999
9100  call FeChybne(-1.,-1.,'during reading of the CIF file.',Veta,
     1              SeriousError)
      go to 9999
9900  ErrFlag=1
      go to 9999
9999  if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag,CIFArray)
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta)
      return
100   format(';')
101   format(i3,4x,f10.2,2f7.2,f8.2,6x,f8.2,2f7.2,i5)
      end
