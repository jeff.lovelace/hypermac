      subroutine RotationalMatrix(R,Kap,Om)
      include 'fepc.cmn'
      include 'basic.cmn'
      real Kap
      dimension Rk(3,3),Ro(3,3),R(3,3)
      call StoreMatrix(Ro,om,1)
      call StoreMatrix(Rk,kap,2)
      call MultM(Ro,Rk,R,3,3,3)
      return
      end
