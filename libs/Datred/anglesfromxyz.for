      subroutine AnglesFromXYZ(x,Psi,A,Rko,ich)
      include 'fepc.cmn'
      dimension Rko(3,3),x(3),A(4)
      call AnglesXYZ(x,A,ich)
      if(Psi.ne.0.) then
        A(1)=A(1)-A(2)
        call AnglesFromRotation(A,Rko,Psi,ich)
      endif
      call RotationalMatrix(Rko,A(3),A(1)-A(2))
      return
      end
