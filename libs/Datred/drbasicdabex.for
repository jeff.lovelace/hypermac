      subroutine DRBasicDABEX
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta
      logical ExistFile
      NDim95(KRefBlock)=3
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
1200  read(71,FormA) Veta
      idl=idel(Veta)
      if(idel(Veta).lt.35) go to 1200
      rewind 71
      k=0
      call Kus(Veta,k,Cislo)
      if(index(Cislo,'.').gt.0) then
        RealIndices=.true.
        if(idl.gt.60) then
          PocitatDirCos=.true.
          FormatRefBlock(KRefBlock)='(3f8.3,2f8.3,f3.0,7f8.4)'
        else
          FormatRefBlock(KRefBlock)='(5f8.3,f5.0,e12.4)'
        endif
      else
        if(idl.gt.60) then
          PocitatDirCos=.true.
          FormatRefBlock(KRefBlock)='(3i5,2f8.3,f3.0,7f8.4)'
        else
          FormatRefBlock(KRefBlock)='(3i5,2f8.3,f5.0,e12.4)'
        endif
      endif
      Veta='dab.dat'
1100  call FeFileManager('Select summary "cif" file',Veta,'*.dat',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      if(.not.ExistFile(Veta)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                Veta(:idel(Veta))//
     2                '" does not exist, try again.',' ',SeriousError)
        go to 1100
      endif
      call OpenFile(70,Veta,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,3
        read(70,FormA,end=9999) Veta
      enddo
      k=0
      call StToReal(Veta,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
      if(ich.ne.0) go to 9999
      do i=1,3
        read(70,FormA,end=9999) Veta
        k=0
        do j=1,3
          call StToReal(Veta,k,ub(i,j,KRefBlock),1,.false.,ich)
          if(ich.ne.0) go to 9999
        enddo
      enddo
      read(70,FormA,end=9999) Veta
      k=0
      call StToReal(Veta,k,LamAveRefBlock(KRefBlock),1,.false.,ich)
9999  call CloseIfOpened(70)
      return
      end
