      subroutine KumaToEuler(uk,u)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension uk(4),u(4),Rot(3,3),AM(3,3),BM(3,3)
      u(4)=uk(2)*.5
      call SetRotMatKuma(uk(1),BetaKuma,1,Rot)
      call SetRotMatKuma(uk(3),AlphaKuma,2,AM)
      call MultM(Rot,AM,BM,3,3,3)
      call SetRotMatKuma(uk(4),BetaKuma,2,AM)
      call MultM(BM,AM,Rot,3,3,3)
      if((abs(uk(3)).eq.0..or.abs(uk(3)).eq.pi).and.BetaKuma.eq.0.) then
        u(1)=uk(4)
        u(2)=0.
        u(3)=uk(1)
      else
        pom=atan2(sqrt(Rot(3,1)**2+Rot(3,2)**2),Rot(3,3))
        rsinc=1./sin(pom)
        u(1)=atan2(-Rot(3,1)*rsinc, Rot(3,2)*rsinc)/ToRad
        u(2)=pom/ToRad
        u(3)=atan2(-Rot(1,3)*rsinc,-Rot(2,3)*rsinc)/ToRad
        call uprav(u(3))
        if(abs(u(3)).gt.90.) then
          u(3)=u(3)+180.
          u(2)=-u(2)
          u(1)=u(1)+180.
          call uprav(u(3))
        endif
        call uprav(u(1))
        call uprav(u(2))
      endif
      return
      end
