      subroutine DRBasicSENJU
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 FileName
      logical ExistFile
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      DirCosFromPsi=.false.
      LamAveRefBlock(KRefBlock)=-1.
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)='(1x,i6,3i4,2f7.2,f8.2,2f7.2,f8.4,'//
     1                           'f9.2,2f10.4,i5,i7,i4,3f9.4,2i5)'
      NRunSENJU=0
      NRunSENJUMax=0
      ln=0
1100  ip=LocateSubstring(SourceFileRefBlock(KRefBlock),'.',
     1                   .false.,.false.)-1
      if(ip.le.1) ip=idel(SourceFileRefBlock(KRefBlock))
      FileName=SourceFileRefBlock(KRefBlock)(:ip)//'.matrix'
      if(.not.ExistFile(FileName)) go to 9900
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      do i=1,3
        read(ln,*,err=9100)(ub(i,j,KRefBlock),j=1,3)
      enddo
      call DRCellFromUB
      read(ln,*,err=9100)(CellRefBlock(i,KRefBlock),i=1,6)
      read(ln,*,err=9100)(CellRefBlockSU(i,KRefBlock),i=1,6)
      go to 9999
9100  call FeReadError(ln)
      ErrFlag=1
      go to 9999
9900  CellRefBlock(1,KRefBlock)=-1.
      call SetRealArrayTo(ub(1,1,KRefBlock),9,-333.)
9999  call CloseIfOpened(ln)
      return
      end
