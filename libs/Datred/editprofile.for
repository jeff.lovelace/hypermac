      subroutine EditProfile(nref,ie)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      dimension iprf(100),h(3),c(3),ipor(:)
      character*128 Veta,Message
      integer ButtonStateQuest
      allocatable ipor
      if(ie.lt.0) go to 2500
      call IOProfil(nref,0)
      Message=' '
      do i=1,nprof
        iprf(i)=nint(py(i,1))
      enddo
      allocate(ipor(nprof))
      call indexx(nprof,iprf,ipor)
      ntrshld=nint(nprof*RatioBS)
      trshld=0.
      do i=1,ntrshld
        trshld=trshld+py(ipor(i),1)
      enddo
      trshld=trshld/float(ntrshld)
      j=0
      do i=1,nprof
        if(py(i,1)-trshld.gt.strshld*sqrt(py(i,1))) then
          if(j.eq.0) then
            j=1
            k=1
          else if(j.eq.2) then
            j=3
            go to 2030
          else
            k=k+1
          endif
        else
          if(j.eq.1) j=2
        endif
      enddo
2030  if(j.eq.3.and.CheckSplit) then
        Message='Split!!!'
        go to 2090
      endif
      if(k.gt.nint(float(nprof)*RatioPS).and.CheckBroad) then
        Message='Broad!!!'
        go to 2090
      endif
2090  continue
      odf=(omx-omn)/float(nprof)
      omp=omn
      omk=omx
      do i=1,3
        h(i)=ih(i)
      enddo
      call multm(ub(1,1,KRefBlock),h,c,3,3,1)
      pom=c(1)**2+c(2)**2
      chip=atan2(c(3),sqrt(pom))/ToRad
      fip=atan2(-c(1),c(2))/ToRad
      pom=sqrt(pom+c(3)**2)
      thp=asin(.5*LamAveRefBlock(KRefBlock)*pom)/ToRad
      OmZnak(1)=thp+asin(-sin((fi-fip)*ToRad)*cos(chip*ToRad))/ToRad
     1          -omega+theta
      s1=0.
      s2=0.
      do i=1,nprof
        px(i)=omp+odf*float(i-1)
        s1=s1+py(i,1)*px(i)
        s2=s2+py(i,1)
      enddo
      xteziste=s1/s2
      if(abs(xteziste-OmZnak(1)).gt.DevLim.and.Message.eq.' '.and.
     1   CheckDeviated) Message='Deviated!!!'
      if(JenSpatne.and.(ie.eq.4.or.ie.eq.5).and.Message.eq.' ')
     1  go to 9999
      px(nprof)=omk
      write(Veta,'(6i4)')(ih(i),i=1,NDim(KPhase))
      call ZdrcniCisla(Veta,NDim(KPhase))
      do i=1,idel(Veta)
        if(Veta(i:i).eq.' ') Veta(i:i)=','
      enddo
      write(Cislo,'(''Ref.#'',i6)') nref
      call Zhusti(Cislo)
      Veta=Cislo(:idel(Cislo))//' ('//Veta(:idel(Veta))//')'
      yomn=0.
      yomx=0.
      do j=1,2
        do i=1,nprof
          yomx=amax1(yomx,py(i,j))
        enddo
      enddo
      xomn=omp
      xomx=omk
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call KresliProfil(Veta,xomn,xomx,yomn,yomx,Message)
      if(ButtonStateQuest(ie).ne.ButtonDisabled)
     1  call FeQuestButtonOff(ie)
      HardCopy=0
2500  call FeEvent(0)
      if(EventType.ne.1) go to 2500
      ie=EventNumber
      call FeQuestButtonOn(ie)
      if(ie.eq.2.or.ie.eq.3) then
        if(ie.eq.2) then
          call FePrintPicture(ich)
          if(ich.ne.0) HardCopy=0
        else
          call FeSavePicture('profile',6,1)
          if(HardCopy.lt.0) HardCopy=0
        endif
        if((HardCopy.ge.1.and.HardCopy.le.5).or.HardCopy.lt.0)
     1     call KresliProfil(Veta,xomn,xomx,yomn,yomx,Message)
        call FeQuestButtonOff(ie)
        go to 2500
      else if(ie.eq.8) then
        ipom=0
        call FeMouseShape(0)
        AllowChangeMouse=.false.
        TakeMouseMove=.true.
3000    call FeEvent(0)
        if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown) then
          if(ipom.ne.0) then
            call FePolyLine(m,xu(i1),yu(i1),Green)
            do i=i1,i2
              call FeCircle(xu(i),yu(i),1.2,Green)
            enddo
            if(ipom.ne.1) yu(1)=pyu(ipom-1,1)
            yu(2)=pyu(ipom,1)
            if(ipom.ne.nprof) yu(3)=pyu(ipom+1,1)
            call FePolyLine(m,xu(i1),yu(i1),White)
            do i=i1,i2
              call FeCircle(xu(i),yu(i),1.2,White)
            enddo
            ipom=0
          else
            do ipom=1,nprof
              if(sqrt((pxu(ipom)-xpos)**2+(pyu(ipom,2)-ypos)**2).le.1.)
     1          then
                if(ipom.gt.1.and.ipom.lt.nprof) then
                  i1=1
                  i2=3
                  m=3
                else if(ipom.eq.1) then
                  i1=2
                  i2=3
                  m=2
                else if(ipom.eq.nprof) then
                  i1=1
                  i2=2
                  m=2
                endif
                if(ipom.ne.1) then
                  xu(1)=pxu(ipom-1)
                  yu(1)=pyu(ipom-1,2)
                endif
                xu(2)=pxu(ipom)
                yu(2)=pyu(ipom,2)
                if(ipom.ne.nprof) then
                  xu(3)=pxu(ipom+1)
                  yu(3)=pyu(ipom+1,2)
                endif
                call FeCircle(xu(2),yu(2),1.2,Black)
                call FePolyLine(m,xu(i1),yu(i1),Black)
                if(ipom.ne.1) yu(1)=pyu(ipom-1,1)
                yu(2)=pyu(ipom,1)
                if(ipom.ne.nprof) yu(3)=pyu(ipom+1,1)
                call FeCircle(xu(2),yu(2),1.2,White)
                call FePolyLine(m,xu(i1),yu(i1),White)
                if(ipom.ne.1) yu(1)=pyu(ipom-1,2)
                yu(2)=pyu(ipom,2)
                if(ipom.ne.nprof) yu(3)=pyu(ipom+1,2)
                call FePlotMode('E')
                call FeCircle(xu(2),yu(2),1.2,Green)
                call FePolyLine(m,xu(i1),yu(i1),Green)
                call FePlotMode('N')
                go to 3000
              endif
            enddo
            ipom=0
          endif
        else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove.and.
     1          ipom.ne.0) then
          call FePlotMode('E')
          call FeCircle(xu(2),yu(2),1.2,Green)
          call FePolyLine(m,xu(i1),yu(i1),Green)
          if(abs(xu(2)-xpos).gt.1.) call FEMoveMouseTo(xu(2),ypos)
          yu(2)=ypos
          pyu(ipom,2)=yu(2)
          py(ipom,2)=FeY2Yo(yu(2))
          call FeCircle(xu(2),yu(2),1.2,Green)
          call FePolyLine(m,xu(i1),yu(i1),Green)
          call FePlotMode('N')
        else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1    then
          go to 4500
        endif
        go to 3000
      else if(ie.eq.9) then
        ipom=0
        call FeMouseShape(0)
        AllowChangeMouse=.false.
        TakeMouseMove=.true.
4000    call FeEvent(0)
        if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftDown) then
          if(ipom.lt.2) then
            do i=1,nprof
              if(sqrt((pxu(i)-xpos)**2+(pyu(i,2)-ypos)**2).le.1.) then
                ipom=ipom+1
                if(ipom.eq.1) then
                  i1=i
                else
                  i2=i
                endif
                xu(ipom)=pxu(i)
                yu(ipom)=pyu(i,2)
                if(ipom.eq.2) call FePlotMode('E')
                call FeCircle(xu(ipom),yu(ipom),1.2,Green)
                if(ipom.eq.2) then
                  call FePolyLine(2,xu,yu,Green)
                  call FePlotMode('N')
                endif
              endif
            enddo
          else
            call FeCircle(xu(1),yu(1),1.2,Green)
            call FeCircle(xu(2),yu(2),1.2,Green)
            call FePolyLine(2,xu,yu,Green)
            sklon=(yu(2)-yu(1))/(xu(2)-xu(1))
            yu1=yu(1)
            xu1=xu(1)
            if(i2.gt.i1) then
              j=1
            else
              j=-1
            endif
            do i=i1,i2,j
              pyu(i,2)=yu1+sklon*(pxu(i)-xu1)
              call FeCircle(pxu(i),pyu(i,2),1.2,Green)
              py(i,2)=FeY2Yo(pyu(i,2))
            enddo
            ipom=0
          endif
        else if(EventType.eq.EventMouse.and.EventNumber.eq.JeMove.and.
     1          ipom.eq.2) then
          call FePlotMode('E')
          call FeCircle(xu(2),yu(2),1.2,Green)
          call FePolyLine(2,xu,yu,Green)
          if(abs(xu(2)-xpos).gt.1.) call FEMoveMouseTo(xu(2),ypos)
          yu(2)=ypos
          call FeCircle(xu(2),yu(2),1.2,Green)
          call FePolyLine(2,xu,yu,Green)
          call FePlotMode('N')
        else if(EventType.eq.EventMouse.and.EventNumber.eq.JeRightDown)
     1    then
          go to 4500
        endif
        go to 4000
      else if(ie.eq.10) then
        call CopyVek(py(1,1),py(1,2),nprof)
        call KresliProfil(Veta,xomn,xomx,yomn,yomx,Message)
        call IOProfil(nref,1)
      else if(ie.eq.3) then
        call KresliProfil(Veta,xomn,xomx,yomn,yomx,Message)
      else
        EventType=EventButton
        EventNumber=ie
        call FeQuestButtonOn(ie)
        go to 9000
      endif
      go to 5000
4500  TakeMouseMove=.false.
      call bpb(rip(2),rsp(2),2,nbckg)
      write(StBPB(2),101) nint(rip(2)),nint(rsp(2))
      call Zhusti(StBPB(2))
      call FeRewriteString(0,xbpb(2),ybpb,StBPBo(2),StBPB(2),'C',
     1                     LightGray,Black)
      StBPBo(2)=StBPB(2)
      call IOProfil(nref,1)
5000  call FeQuestButtonOff(ie)
      go to 2500
9000  call FeOutSt(0,xmsg,ybpb,Message,'C',LightGray)
9999  if(allocated(ipor)) deallocate(ipor)
      return
101   format(i10,'(',i10,')')
      end
