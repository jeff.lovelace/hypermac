      subroutine DRBasicKoala
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Veta
      logical ExistFile
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      LamAveRefBlock(KRefBlock)=-1.
      FormatRefBlock(KRefBlock)='(3i4,2f8.2,4x,f8.3)'
      Veta='Laue4_jana.out'
      if(ExistFile(Veta)) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','old')
        do i=1,2
          read(ln,FormA,err=1035,end=1035)
        enddo
        read(ln,FormA,err=1035,end=1035) Veta
        k=0
        call StToReal(Veta,k,CellRefBlock(1,KRefBlock),6,.false.,ich)
1035    call CloseIfOpened(ln)
      endif
      return
      end
