      subroutine DRSGTest(Key,Change)
      use Basic_mod
      use Atoms_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      dimension xp(6),trm6(36),trm6i(36),trm3(3,3),trm3t(3,3),
     1          trm3i(3,3),rmp(36),hp(6),rm6Org(:,:),s6Org(:,:),
     2          vt6Org(:,:),VT6X(:,:),QuSymm(3,3),FLimCullO(MxDatBlock),
     3          RTwIn(:,:)
      character*80 Veta,t80
      character*60 GrupaOrg
      character*10 Clen,SpaceSt
      integer GrupaEq,CrSystemFromCell,CrwStateQuest
      logical EqRv,FeYesNoHeader,Change,CrwLogicQuest,PwdCheckTOFInD,
     1        LookForSuperCell,ExistFile,lpom,EqRV0,EqIgCase,
     2        SkipFriedelIn,TestCentr
      real MetTensP(9)
      allocatable rm6Org,s6Org,vt6Org,VT6X,RTwIn
      TestCentr=.false.
      Veta=' '
      do i=1,NDatBlock
        FLimCullO(i)=FLimCull(i)
        FLimCull(i)=0
      enddo
      if(isPowder) then
        if(.not.ExistM90) Veta='Reflection file M90 does not exist and'
        SkipFriedelIn=SkipFriedel(KPhase,KDatBlock)
        SkipFriedel(KPhase,KDatBlock)=.true.
      else
        if(.not.ExistM95) Veta='Repository file M95 does not exist and'
        if(.not.ExistM90) call SetBasicM90(KDatBlock)
      endif
      if(Veta.ne.' ') then
        call FeChybne(-1.,-1.,Veta,
     1                'therefore the test cannot be made.',
     2                Warning)
        go to 9999
      endif
      if(NAtCalc.gt.0) then
        call FeFillTextInfo('drsgtest1.txt',0)
        if(.not.FeYesNoHeader(-1.,-1.,'Do you really want to continue?',
     1                        0)) go to 9999
      endif
      if(NRefBlock.gt.1) then
         TextInfo(1)='Select refblock to be used for the space '//
     1               'group test'
         call CopyStringArray(MenuRefBlock,TextInfo(2),NRefBlock)
         NInfo=NRefBlock+1
         allocate(UseInSGTest(NRefBlock))
         call FeSelectFromOffer(-1.,-1.,1,UseInSGTest,ich)
         if(ich.ne.0) go to 9999
         n=0
         do i=1,NRefBlock
           if(UseInSGTest(i)) n=n+1
         enddo
         if(n.le.0) go to 9999
      endif
      NTwinIn=NTwin
      allocate(RTwIn(3,NTwin))
      call CopyVek(RTw,RTwIn,3*NTwin)
      NRefRead=0
      IdSymmFull=0
      IdSymmFullTr=0
      Change=.false.
      if(ExistM95) call iom95(0,fln(:ifln)//'.m95')
      FormExtRefl='(4i4,2f9.1)'
      write(FormExtRefl(2:2),'(i1)') NDim(KPhase)+1
      if(mod(Key,10).gt.0) then
        isPowder=iabs(DataType(KDatBlock)).eq.2
        call PwdSetTOF(KDatBlock)
      endif
1050  DiffAxeOld  =-1.
      DiffAngleOld=-1.
      DiffModVecOld=-1.
      SumaObsLimCentrOld=-1.
      SumaObsLimExtinctOld=-1.
      NCellSelOld=-1
      CentrSelOld=-1
      LaueGroupSelOld=-1
      LaueGroupUsedInCentr=-1
      CentrSelOld=-1
      GrupaSelOld=' '
      TakeSGTwinOld=-1
      TakeOldTwinOld=-1
      if(CrSystemFromCell(CellPar(1,1,KPhase),DiffAxe,DiffAngle).eq.
     1                            CrSystemHexagonal) then
        CentrMult=3
      else
        CentrMult=2
      endif
      allocate(rm6Org(NDimQ(KPhase),NSymm(KPhase)),
     1         s6Org(NDim(KPhase),NSymm(KPhase)),vt6Org(NDim(KPhase),
     2         NLattVec(KPhase)))
      call SetRealArrayTo(CellParArr1Old,6,-1.)
      call SetRealArrayTo(CellParArrOld,66,-1.)
      call SetRealArrayTo(CellParSelOld,6,-1.)
      call SetRealArrayTo(CellTrSel6Old,NDimQ(KPhase),-1.)
      call SetRealArrayTo(CellParSelSGOld,6,-1.)
      call SetRealArrayTo(CellTrSel6SGOld,NDimQ(KPhase),-1.)
      call CopyVek(CellPar(1,1,KPhase),CellParOrg,6)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      call CopyMat(TrMP,TrMpOrg,NDim(KPhase))
      GrupaOrg=Grupa(KPhase)
      nsOrg=NSymm(KPhase)
      nsnOrg=NSymmN(KPhase)
      do i=1,NSymm(KPhase)
        call CopyMat(rm6(1,i,1,KPhase),rm6Org(1,i),NDim(KPhase))
        call CopyVek(s6(1,i,1,KPhase),s6Org(1,i),NDim(KPhase))
      enddo
      nvtOrg=NLattVec(KPhase)
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6(1,i,1,KPhase),vt6Org(1,i),NDim(KPhase))
      enddo
      WizardId=NextQuestId()
      WizardMode=.true.
      WizardLength=500.
      WizardLines=15
      call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                   'xxx',0,LightGray,0,0)
      LookForSuperCell=.true.
      igr=0
      if(Key.ge.10) then
        call FeQuestButtonDisable(ButtonEsc)
        call FeFillTextInfo('drsgtest2.txt',0)
        call FeWizardTextInfo('INFORMATION',0,1,1,ich)
        if(ich.ne.0) go to 9900
      endif
      TakeSGTwin=.false.
1100  call CopyMat(TrMpOrg,TrMP,NDim(KPhase))
      id=NextQuestId()
      call FeQuestTitleMake(Id,'Tolerances for crystal system '//
     1                         'recognition:')
      ich=0
      il=1
      tpom=5.
      Veta='Original cell parameters:'
      write(Veta(idel(Veta)+1:),'(3f7.3,3f7.2)')(CellPar(j,1,KPhase),
     1                                         j=1,6)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta='Maximal deviation for %cell lengths in [A]'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      if(NDimI(KPhase).gt.0) then
        ik=3
      else
        ik=2
      endif
      do i=1,ik
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          pom=DiffAxe
          Veta='Maximal deviation for cell %angles in deg'
          nEwdDiffAxe=EdwLastMade
        else if(i.eq.2) then
          pom=DiffAngle
          Veta='Maximal deviation for %modulation vector'
          nEwdDiffAngle=EdwLastMade
        else
          pom=DiffModVec
          nEwdDiffModVec=EdwLastMade
        endif
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      enddo
      TakeOldTwin=.false.
      if(isPowder) then
        nEwdLimCentr=0
        nEwdLimExtinct=0
        nCrwLookForSuperCell=0
        nCrwTakeTwin=0
        nCrwOldTwin=0
        xpom=20.
        tpom=xpom+CrwXd+5.
      else
        il=il+1
        tpom=WizardLength*.5
        call FeQuestLblMake(id,tpom,il,'Tolerances for space group '//
     1                      'recognition:','C','B')
        tpom=5.
        Veta='Maximal ave(I/sig(I)) for cen%tering'
        xpom=tpom+FeTxLengthUnder(Veta)+15.
        do i=1,2
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) then
            pom=SumaObsLimCentr
            Veta='Maximal ave(I/sig(I)) for %extinctions'
            nEwdLimCentr=EdwLastMade
          else
            pom=SumaObsLimExtinct
            nEwdLimExtinct=EdwLastMade
          endif
          call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        enddo
        xpom=20.
        tpom=xpom+CrwXd+5.
        Veta='Search for %higher symmetrical supercell '//
     1       '(recommended)'
        ichk=0
        do i=1,3
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                        ichk,0)
          if(i.eq.1) then
            nCrwLookForSuperCell=CrwLastMade
            lpom=LookForSuperCell
            Veta='Introduce t%win laws in case of subgroups'
            ichk=1
          else if(i.eq.2) then
            nCrwTakeTwin=CrwLastMade
            lpom=TakeSGTwin
            Veta='Use %old twin matrices in testing'
          else if(i.eq.3) then
            lpom=TakeOldTwin
            nCrwOldTwin=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          if(NTwinIn.le.1.and.i.eq.3)
     1      call FeQuestCrwDisable(CrwLastMade)
        enddo
      endif
      il=il+1
      call FeQuestLblMake(id,WizardLength*.5,il,
     1                    'Check non-standard centering:','C','B')
      igr=igr+1
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='look for centering vectors composed from 0 and 1/%2'
        else
          nCrwCentrHalf=CrwLastMade
          Veta='look for centering vectors composed from 0, 1/%3 and '//
     1         '2/3'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      0,igr)
        call FeQuestCrwOpen(CrwLastMade,CentrMult.eq.i+1)
      enddo
      call FeQuestButtonDisable(ButtonEsc)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwTakeTwin.or.
     2    CheckNumber.eq.nCrwOldTwin)) then
        if(CrwStateQuest(nCrwOldTwin).eq.CrwDisabled) go to 1500
        if(CheckNumber.eq.nCrwTakeTwin.and.CrwLogicQuest(nCrwTakeTwin))
     1    then
          call FeQuestCrwOff(nCrwOldTwin)
        else if(CheckNumber.eq.nCrwOldTwin.and.
     1          CrwLogicQuest(nCrwOldTwin)) then
          call FeQuestCrwOff(nCrwTakeTwin)
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEwdDiffAxe  ,DiffAxe  )
        call FeQuestRealFromEdw(nEwdDiffAngle,DiffAngle)
        if(NDimI(KPhase).gt.0)
     1    call FeQuestRealFromEdw(nEwdDiffModVec,DiffModVec)
        if(.not.isPowder) then
          LookForSuperCell=CrwLogicQuest(nCrwLookForSuperCell)
          TakeSGTwin=CrwLogicQuest(nCrwTakeTwin)
          TakeOldTwin=CrwLogicQuest(nCrwOldTwin)
          if(TakeSGTwin) then
            NTwin=1
          else
            if(TakeSGTwinOld.eq.1) call iom50(0,0,fln(:ifln)//'.m50')
          endif
          if(TakeOldTwin) then
            if(TakeOldTwinOld.eq.1) call iom50(0,0,fln(:ifln)//'.m50')
          else
            NTwin=1
          endif
          call FeQuestRealFromEdw(nEwdLimExtinct,SumaObsLimExtinct)
          call FeQuestRealFromEdw(nEwdLimCentr  ,SumaObsLimCentr  )
        else
          LookForSuperCell=.false.
        endif
        if(CrwLogicQuest(nCrwCentrHalf)) then
          CentrMult=2
        else
          CentrMult=3
        endif
      endif
      if(ich.ne.0) then
        if(ich.gt.0) then
          ErrFlag=-1
        else
          ErrFlag= 1
        endif
        go to 9900
      endif
      call FeMouseShape(3)
      if(.not.isPowder) then
        KeyP=0
        if(NRefBlock.gt.1) then
          if(allocated(UseInSGTest)) KeyP=-1
        endif
        call DRSetRefArrays(KeyP)
        if(ErrFlag.ne.0) go to 9900
      endif
      KeyPom=0
2300  call DRSGTestLaue(LookForSuperCell,KeyPom,ich)
      if(ich.ne.0) then
        if(ich.eq.-1) then
          go to 5000
        else
          go to 1100
        endif
      endif
2500  call DRSGTestCentr(ich)
      TestCentr=.true.
      if(ich.ne.0) then
        if(ich.eq.-1) then
          go to 5000
        else
          KeyPom=1
          go to 2300
        endif
      endif
      KeyPom=0
2700  call DRSGTestSGroup(KeyPom,Change,ich)
      if(ich.ne.0) then
        if(ich.eq.-1) then
          go to 5000
        else
          go to 2500
        endif
      endif
      KeyPom=0
2900  if(NDimI(KPhase).eq.1.and..not.isPowder) then
        call DRSGTestSSG4(KeyPom,ich)
        if(ich.ne.0) then
          if(ich.eq.-1) then
            go to 5000
          else
            KeyPom=1
            go to 2700
          endif
        endif
      endif
      Change=.true.
      xpom=5.
      tpom=xpom+CrwgXd+3.
      id=NextQuestid()
      if(NDim(KPhase).gt.3) then
        SpaceSt='superspace'
      else
        SpaceSt='space'
      endif
      call FeQuestTitleMake(id,'Final step of the '//
     1                      SpaceSt(:idel(SpaceSt))//' group test')
      t80='accept the '//SpaceSt(:idel(SpaceSt))//' group'
      call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
      ypom=12.+QuestLineWidth*float(WizardLines)
      GrupaEq=0
      do i=1,3
        if(i.eq.1) then
          Veta=t80(:idel(t80))//' in the standard setting:'
        else if(i.eq.2) then
          Veta=t80(:idel(t80))//' transformed into the original cell:'
        else if(i.eq.3) then
          Veta='discard the changes'
        endif
        call FeQuestAbsCrwMake(id,tpom,ypom+5.,xpom,ypom,Veta,'L',
     1                         CrwgXd,CrwgYd,0,1)
        ypom=ypom-16.
        xpoml=20.
        if(i.eq.1) then
          if(EqIgCase(GrupaOrg,GrupaSel)) GrupaEq=i
          nCrwStandard=CrwLastMade
          Veta=SpaceSt(:idel(SpaceSt))//' group: '//
     1         GrupaSel(:idel(GrupaSel))
          call Velka(Veta(1:1))
          call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
          ypom=ypom-16.
          write(Veta,'(3f9.4,3f9.3)') CellParSel
          call ZdrcniCisla(Veta,6)
          Veta='Cell parameters: '//Veta(:idel(Veta))
          call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
          ypom=ypom-16.
          do j=1,NDimI(KPhase)
            if(NDimI(KPhase).eq.1) then
              write(Veta,'(''Modulation vector: '',3f8.3)')
     1                          (QuIrrSel(k,j)+QuRacSel(k,j),k=1,3)
            else
              write(Veta,'(''Modulation vector #'',i1,'':'',3f8.3)')
     1          j,(QuIrrSel(k,j)+QuRacSel(k,j),k=1,3)
            endif
            call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
            ypom=ypom-16.
          enddo
          call FeTabsReset(0)
          UseTabs=0
          do j=1,3
            if(j.eq.1) then
              Veta='Transformation matrix:'
              idlv=idel(Veta)+1
              pomt=FeTxLengthSpace(Veta(:idel(Veta))//' ')
              call FeTabsAdd(pomt,0,IdRightTab,' ')
            else
              Veta=' '
            endif
            Veta=Veta(:idlv)//Tabulator//SmbABC(j)//'''='
            if(j.eq.1) pomt=FeTxLength(Veta(:idel(Veta)))
            m=(j-1)*NDim(KPhase)
            do k=1,3
              m=m+1
              pom=CellTrSel6(m)
              if(j.eq.1) then
                if(k.eq.1) pomc=FeTxLength('XXXXXXX*X')
                pomt=pomt+pomc
                call FeTabsAdd(pomt,0,IdLeftTab,' ')
              endif
              write(Clen,'(f7.3,''*'',a1)') pom,SmbABC(k)
              if(pom.ge.0..and.k.ne.1) then
                l=1
3032            if(Clen(l:l).eq.' ') then
                  l=l+1
                  go to 3032
                endif
                l=l-1
                if(l.gt.0) Clen(l:l)='+'
              endif
              Veta=Veta(:idel(Veta))//Tabulator//Clen(:idel(Clen))
            enddo
            call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
            ypom=ypom-16.
          enddo
          ypom=ypom-8.
          call multm(TrMPOrg,CellTrSel6,TrMP,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          NGrupa(KPhase)=iGSel
          CrSystem(KPhase)=mod(CrSystemSel,10)
          Monoclinic(KPhase)=CrSystemSel/10
          Grupa(KPhase)=GrupaSel
          call CopyVek(CellParSel,CellPar(1,1,KPhase),6)
          if(NDimI(KPhase).gt.0) then
            call CopyVek(QuIrrSel,Qu(1,1,1,KPhase),NDimI(KPhase)*3)
            call CopyVek(QuRacSel,quir(1,1,KPhase),NDimI(KPhase)*3)
            call AddVek(Qu(1,1,1,KPhase),QuRacSel,
     1                  Qu(1,1,1,KPhase),NDimI(KPhase)*3)
          endif
          if(NDimI(KPhase).gt.1)
     1      s6(4:NDim(KPhase),1:NSymm(KPhase),1,KPhase)=0.
          call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
          call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
          call EM50MakeStandardOrder
          call CopyMat(CellTrSel6,trm6I,NDim(KPhase))
          call matinv(trm6I,trm6,VolRatio,NDim(KPhase))
          call MatBlock3(trm6,trm3,NDim(KPhase))
          call matinv(trm3,trm3i,VolRatio,3)
          if(GrupaSel(1:1).eq.'X') then
            allocate(VT6X(NDim(KPhase),NLattVec(KPhase)))
            do j=1,NLattVec(KPhase)
              call CopyVek(vt6(1,j,1,KPhase),VT6X(1,j),NDim(KPhase))
            enddo
            nVTX=NLattVec(KPhase)
          endif
        else if(i.eq.2) then
          nCrwOriginal=CrwLastMade
          MetTensP(1)=CellParSel(1)**2
          MetTensP(5)=CellParSel(2)**2
          MetTensP(9)=CellParSel(3)**2
          MetTensP(2)=CellParSel(1)*CellParSel(2)*
     1                cos(ToRad*CellParSel(6))
          MetTensP(4)=MetTensP(2)
          MetTensP(3)=CellParSel(1)*CellParSel(3)*
     1                cos(ToRad*CellParSel(5))
          MetTensP(7)=MetTensP(3)
          MetTensP(6)=CellParSel(2)*CellParSel(3)*
     1                cos(ToRad*CellParSel(4))
          MetTensP(8)=MetTensP(6)
          call TrMat(trm3,trm3t,3,3)
          call MultM(trm3t,MetTensP,rmp,3,3,3)
          call MultM(rmp,trm3,MetTensP,3,3,3)
          k=1
          do j=1,3
            CellPar(j,1,KPhase)=sqrt(MetTensP(k))
            k=k+4
          enddo
          pom=min(MetTensP(2)/
     1        (CellPar(1,1,KPhase)*CellPar(2,1,KPhase)),1.)
          pom=max(pom,-1.)
          if(abs(pom).lt..000001) pom=0.
          CellPar(6,1,KPhase)=acos(pom)/ToRad
          pom=min(MetTensP(3)/
     1        (CellPar(1,1,KPhase)*CellPar(3,1,KPhase)),1.)
          pom=max(pom,-1.)
          if(abs(pom).lt..000001) pom=0.
          CellPar(5,1,KPhase)=acos(pom)/ToRad
          pom=min(MetTensP(6)/
     1        (CellPar(2,1,KPhase)*CellPar(3,1,KPhase)),1.)
          pom=max(pom,-1.)
          if(abs(pom).lt..000001) pom=0.
          CellPar(4,1,KPhase)=acos(pom)/ToRad
          do j=1,NDimI(KPhase)
            if(NDimI(KPhase).gt.1) then
              call Multm(Qu(1,j,1,KPhase),trm3,rmp,1,3,3)
              call CopyVek(rmp,Qu(1,j,1,KPhase),3)
            else
              call Multm(QuIrrSel(1,j),trm3,rmp,1,3,3)
              call CopyVek(rmp,Qu(1,j,1,KPhase),3)
              call Multm(QuRacSel(1,j),trm3,rmp,1,3,3)
              if(Trm6(16).lt.0.) then
                call RealVectorToOpposite(Qu(1,j,1,KPhase),
     1                                    Qu(1,j,1,KPhase),3)
                call RealVectorToOpposite(rmp,rmp,3)
              endif
              do k=1,3
                if(rmp(k).lt.-.00001) then
                  rmp(k)=1.+rmp(k)
                  Trm6(k*4)=-sign(1.,Trm6(16))
                else
                  Trm6(k*4)=0.
                endif
              enddo
              call AddVek(Qu(1,j,1,KPhase),rmp,Qu(1,j,1,KPhase),3)
            endif
          enddo
          call matinv(trm6,trm6I,VolRatio,NDim(KPhase))
          VolRatio=abs(VolRatio)
          MaJi=0
          do 3100j=1,NSymm(KPhase)
            call multm(trm6i,rm6(1,j,1,KPhase),rmp,NDim(KPhase),
     1                 NDim(KPhase),NDim(KPhase))
            call multm(rmp,trm6,rm6(1,j,1,KPhase),NDim(KPhase),
     1                 NDim(KPhase),NDim(KPhase))
            call multm(trm6i,s6(1,j,1,KPhase),rmp,NDim(KPhase),
     1                 NDim(KPhase),1)
            call od0do1(rmp,s6(1,j,1,KPhase),NDim(KPhase))
            call MatBlock3(rm6(1,j,1,KPhase),rm(1,j,1,KPhase),
     1                     NDim(KPhase))
            if(MaJi.le.0) then
              do k=1,3
                m=(k-1)*NDim(KPhase)
                do l=1,3
                  m=m+1
                  if(abs(CellTrSel6(m)-rm6(m,j,1,KPhase)).gt..001)
     1              go to 3100
                enddo
              enddo
              MaJi=j
            endif
3100      enddo
          if(MaJi.gt.0) then
            NDefault=2
          else
            NDefault=1
          endif
          nvtt=0
          do 3140j=1,NLattVec(KPhase)
            call multm(trm6i,vt6(1,j,1,KPhase),rmp,NDim(KPhase),
     1                 NDim(KPhase),1)
            call od0do1(rmp,xp,NDim(KPhase))
            do k=1,nvtt
              if(eqrv(xp,vt6(1,k,1,KPhase),NDim(KPhase),.001))
     1           go to 3140
            enddo
            nvtt=nvtt+1
            call CopyVek(xp,vt6(1,nvtt,1,KPhase),NDim(KPhase))
3140      continue
          if(VolRatio.gt.1.5) then
            ik=nint(VolRatio)-1
            do i3=-ik,ik
              xp(3)=i3
              do i2=-ik,ik
                xp(2)=i2
                do 3160i1=-ik,ik
                  xp(1)=i1
                  call multm(trm3i,xp,rmp,3,3,1)
                  call od0do1(rmp,hp,3)
                  do j=1,nvtt
                    if(eqrv(hp,vt6(1,j,1,KPhase),3,.001)) go to 3160
                  enddo
                  nvtt=nvtt+1
                  call ReallocSymm(NDim(KPhase),NSymm(KPhase),nvtt,
     1                             NComp(KPhase),NPhase,0)
                  call CopyVek(hp,vt6(1,nvtt,1,KPhase),3)
                  if(NDimI(KPhase).gt.0)
     1              call SetRealArrayTo(vt6(4,nvtt,1,KPhase),
     2                                  NDimI(KPhase),0.)
3160            continue
              enddo
            enddo
          endif
          call EM50CompleteCentr(0,vt6,ubound(vt6,1),nvtt,1000,ich)
          NLattVec(KPhase)=nvtt
          call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
          if(.not.EqRV0(ShSg(1,KPhase),NDim(KPhase),.0001)) then
            call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
            call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
            call EM50MakeStandardOrder
          endif
          call CopyMat(TrMPOrg,TrMP,NDim(KPhase))
          if(EqIgCase(GrupaOrg,Grupa(KPhase))) GrupaEq=i
          Veta=SpaceSt(:idel(SpaceSt))//' group: '//
     1         Grupa(KPhase)(:idel(Grupa(KPhase)))
          call Velka(Veta(1:1))
          call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
          ypom=ypom-16.
          write(Veta,'(3f9.4,3f9.3)')(CellPar(j,1,KPhase),j=1,6)
          call ZdrcniCisla(Veta,6)
          Veta='Cell parameters: '//Veta(:idel(Veta))
          call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
          ypom=ypom-16.
          do j=1,NDimI(KPhase)
            if(NDimI(KPhase).eq.1) then
              write(Veta,'(''Modulation vector: '',3f8.3)')
     1                          (Qu(k,j,1,KPhase),k=1,3)
            else
              write(Veta,'(''Modulation vector #'',i1,'':'',3f8.3)')
     1          j,(Qu(k,j,1,KPhase),k=1,3)
            endif
            call FeQuestAbsLblMake(id,xpoml,ypom,Veta,'L','N')
            ypom=ypom-16.
          enddo
          ypom=ypom-8.
        else if(i.eq.3) then
          nCrwDiscard=CrwLastMade
        endif
      enddo
      nCrw=nCrwStandard
      do i=1,3
        call FeQuestCrwOpen(nCrw,i.eq.NDefault)
        nCrw=nCrw+1
      enddo
      if(isPowder) then
        nCrwAcceptTwin=0
      else
        Veta='%Accept twinning matrices induced by the space group '//
     1       'test'
        ypom=ypom-10.
        xpom=5.
        tpom=xpom+CrwXd+5.
        call FeQuestabsCrwMake(id,tpom,ypom+5.,xpom,ypom,Veta,'L',CrwXd,
     1                         CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,.true.)
        if(NTwin.le.1.or..not.TakeSGTwin)
     1    call FeQuestCrwDisable(CrwLastMade)
        nCrwAcceptTwin=CrwLastMade
      endif
      if(NRefBlock.gt.1.and.GrupaEq.eq.1.and.abs(VolRatio-1.).lt..001)
     1  then
        call FeFillTextInfo('drsgtest3.txt',0)
        i=index(TextInfo(1),'#$%')
        if(i.gt.0)
     1    TextInfo(1)=TextInfo(1)(1:i-1)//SpaceSt(:idel(SpaceSt))//
     2                TextInfo(1)(i+3:)
        i=index(TextInfo(4),'#$%')
        write(Cislo,'(''#'',i5)') KDatBlock
        call Zhusti(Cislo)
        if(i.gt.0)
     1    TextInfo(4)=TextInfo(4)(1:i-1)//Cislo(:idel(Cislo))//
     2                TextInfo(4)(i+3:)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call FeQuestCrwOff(nCrwStandard)
        call FeQuestCrwOn(nCrwDiscard)
      endif
4100  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 4100
      endif
      if(ich.ne.0) then
        if(ich.eq.-1) then
          Change=.false.
          go to 5000
        else
          KeyPom=1
          call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
          if(NDim(KPhase).eq.4) then
            go to 2900
          else
            go to 2700
          endif
        endif
      else
        MaxNSymm=max(MaxNSymm,NSymm(KPhase))
        MaxNLattVec=max(MaxNLattVec,NLattVec(KPhase))
        if(.not.isPowder) then
          if(CrwLogicQuest(nCrwAcceptTwin)) then
            pom=1./float(NTwin)
            j=mxscu
4200        if(sc(j,1).le.0.) then
              j=j-1
              if(j.gt.1) go to 4200
            endif
            mxscutw=max(j+NTwin-1,6)
            mxscu=mxscutw-NTwin+1
            do i=1,NTwin
              sctw(i,KDatBlock)=pom
            enddo
            if(NAtAll.gt.0) then
              if(allocated(isa)) deallocate(isa)
              allocate(isa(MaxNSymm,NAtAll))
              do i=1,NAtAll
                do j=1,MaxNSymm
                  isa(j,i)=j
                enddo
              enddo
            endif
            call iom40(1,0,fln(:ifln)//'.m40')
            call DeleteFile(PreviousM40)
          else
            NTwin=NTwinIn
            call CopyVek(RTwIn,RTw,3*NTwin)
          endif
        endif
        if(CrwLogicQuest(nCrwStandard)) then
          if(GrupaSel(1:1).eq.'X') then
            do j=1,nVTX
              call CopyVek(VT6X(1,j),vt6(1,j,1,KPhase),NDim(KPhase))
            enddo
            NLattVec(KPhase)=nVTX
          endif
          call multm(TrMPOrg,CellTrSel6,TrMP,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          NGrupa(KPhase)=iGSel
          CrSystem(KPhase)=mod(CrSystemSel,10)
          Monoclinic(KPhase)=CrSystemSel/10
          Grupa(KPhase)=GrupaSel
          call CopyVek(CellParSel,CellPar(1,1,KPhase),6)
          if(NDimI(KPhase).gt.0) then
            call CopyVek(QuIrrSel,Qu(1,1,1,KPhase),NDimI(KPhase)*3)
            call CopyVek(QuRacSel,quir(1,1,KPhase),NDimI(KPhase)*3)
            call AddVek(Qu(1,1,1,KPhase),QuRacSel,Qu(1,1,1,KPhase),
     1                  NDimI(KPhase)*3)
          endif
          call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
          call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
          call EM50MakeStandardOrder
          do i=2,NTwin
            call multm(rtw(1,i),trm3i,rmp,3,3,3)
            call multm(trm3,rmp,rtw(1,i),3,3,3)
            call matinv(rtw(1,i),rtwi(1,i),pom,3)
          enddo
        else
          RepeatSGTest=.false.
          if(CrwLogicQuest(nCrwDiscard)) then
            Change=.false.
            go to 5000
          endif
        endif
      endif
      call iom50(1,0,fln(:ifln)//'.m50')
      call DeleteFile(PreviousM50)
      if(isPowder) then
        call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
        call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),3*NDimI(KPhase))
        call iom41(1,0,fln(:ifln)//'.m41')
        call DeleteFile(PreviousM41)
      endif
      call DeleteAllFiles(fln(:ifln)//'.l??')
      call CompleteM95(0)
      Veta=fln(:ifln)//'.inflip'
      if(ExistFile(Veta)) call DeleteFile(Veta)
      go to 9900
5000  call CopyVek(CellParOrg,CellPar(1,1,KPhase),6)
      call CopyVek(QuOrg,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      call CopyMat(TrMpOrg,TrMP,NDim(KPhase))
      Grupa(KPhase)=GrupaOrg
      NSymm(KPhase)=nsOrg
      NSymmN(KPhase)=nsnOrg
      do i=1,NSymm(KPhase)
        call CopyMat(rm6Org(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
        call CopyVek(s6Org(1,i),s6(1,i,1,KPhase),NDim(KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
      enddo
      NLattVec(KPhase)=nvtOrg
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6Org(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
      enddo
      NTwin=NTwinIn
9900  call FeQuestRemove(WizardId)
9999  call CloseIfOpened(95)
      call DeleteFile(fln(:ifln)//'_spgroups.tmp')
      WizardMode=.false.
      if(isPowder) then
        if(allocated(ihar)) deallocate(ihar,FlgAr,KPhAr,MltAr)
        SkipFriedel(KPhase,KDatBlock)=SkipFriedelIn
      else
        if(allocated(HCondAr)) deallocate(HCondAr,riar,rsar,ICullAr,
     1                                    RefBlockAr,RunCCDAr)
      endif
      if(allocated(rm6Org)) deallocate(rm6Org,s6Org,vt6Org)
      if(allocated(VT6X)) deallocate(VT6X)
      if(allocated(HCondAveAr)) then
        deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     1             Ave2Org)
        NAve=0
        NAveRed=0
        NAveMax=0
        NAveRedMax=0
        NAveRed=0
      endif
      if(TestCentr) call DRSGTestCentrClean
      call FeTabsReset(-1)
      if(RepeatSGTest) then
        call FeFillTextInfo('drsgtest4.txt',0)
        if(FeYesNoHeader(-1.,-1.,'Do you want repeat the test?',1)) then
          NRefRead=0
          go to 1050
        endif
      endif
      call DeleteFile(fln(:ifln)//'.laue')
      call DeleteFile(fln(:ifln)//'.centr')
      call DeleteFile(fln(:ifln)//'.centrx')
      call DeleteFile(fln(:ifln)//'.sgt')
      call DeleteFile(fln(:ifln)//'.sgt4')
      if(allocated(UseInSGTest)) deallocate(UseInSGTest)
      if(allocated(LaueString)) deallocate(LaueString,RLaueString)
      if(allocated(OLaueString)) deallocate(OLaueString)
      if(allocated(RTwIn)) deallocate(RTwIn)
      do i=1,NDatBlock
        FLimCull(i)=FLimCullO(i)
      enddo
      if(Key.ne.1) call CrlCleanSymmetry
      return
100   format(i15,'/',i15)
101   format(f10.3)
104   format(5i5,l5,3i5,3f8.3,f10.5)
      end
