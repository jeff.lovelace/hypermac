      subroutine DRMatTrCell(t,Cell,TrM)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension t(3,3),tt(36),Cell(6),TrM(*),g(3,3),gp(3,3),
     1          Qi(3,3),Qr(3,3),xp(36)
      go to 2000
      entry DRMatTrCellQ(t,Cell,Qi,Qr,TrM)
      do i=1,NDimI(KPhase)
        call Multm(Qi(1,i),t,xp,1,3,3)
        call CopyVek(xp,Qi(1,i),3)
        call Multm(Qr(1,i),t,xp,1,3,3)
        call CopyVek(xp,Qr(1,i),3)
      enddo
2000  call DRCell2MetTens(Cell,g)
      call TrMat(t,tt,3,3)
      call Multm(tt,g,gp,3,3,3)
      call Multm(gp,t,g,3,3,3)
      call DRMetTens2Cell(g,Cell)
      call MatFromBlock3(t,tt,NDim(KPhase))
      call Multm(TrM,tt,xp,NDim(KPhase),NDim(KPhase),NDim(KPhase))
      call CopyMat(xp,TrM,NDim(KPhase))
      return
      end
