      subroutine DRBasicJanaM90
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta
      logical ExistFile,EqIgCase
      close(71)
      ExistM90=.true.
      call iom90(0,SourceFileRefBlock(KRefBlock))
      ExistM90=.false.
      LamAveRefBlock(KRefBlock)=LamAve(1)
      LamA1RefBlock(KRefBlock)=LamA1(1)
      LamA2RefBlock(KRefBlock)=LamA2(1)
      PocitatUhly=LamAveRefBlock(KRefBlock).gt.0.
      NAlfaRefBlock(KRefBlock)=NAlfa(1)
      LamRatRefBlock(KRefBlock)=LamRat(1)
      PolarizationRefBlock(KRefBlock)=LpFactor(1)
      AngleMonRefBlock(KRefBlock)=AngleMon(1)
      AlphaGMonRefBlock(KRefBlock)=AlphaGMon(1)
      BetaGMonRefBlock(KRefBlock)=BetaGMon(1)
      FractPerfMonRefBlock(KRefBlock)=FractPerfMon(1)
      RadiationRefBlock(KRefBlock)=Radiation(1)
      TempRefBlock(KRefBlock)=DatCollTemp(1)
      HKLF5RefBlock(KRefBlock)=HKLF5(1)
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      call OpenFile(71,SourceFileRefBlock(KRefBlock),'formatted','old')
      if(ErrFlag.ne.0) go to 9999
1100  read(71,FormA,end=9999) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'Data')) then
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'Block1')) then
          go to 1200
        else
          go to 1100
        endif
      else
        go to 1100
      endif
1200  FormatRefBlock(KRefBlock)=Format91
      write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim95(KRefBlock)
9999  return
      end
