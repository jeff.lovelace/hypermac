      subroutine DRWriteUsedPar(CellParActual,QuActual,CellTrActual6)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*25 t25
      dimension CellParActual(*),CellTrActual6(*),QuActual(3,*)
      call NewLn(9+3*NDimI(KPhase))
      write(lst,FormA)
      t25='Original cell parameters:'
      write(lst,100) t25,(CellParOrg(i),i=1,6)
      if(NDimI(KPhase).gt.0) then
        t25='Original q vector'
        if(NDimI(KPhase).eq.1) then
          t25=t25(:idel(t25))//':'
        else
          t25=t25(:idel(t25))//'(s):'
        endif
        do j=1,NDimI(KPhase)
          write(lst,101) t25,(QuOrg(i,j),i=1,3)
          t25=' '
        enddo
      endif
      write(lst,FormA)
      t25='Transformation matrix:'
      do j=1,NDim(KPhase)
        write(lst,101) t25,
     1    (CellTrActual6(j+(i-1)*NDim(KPhase)),i=1,NDim(KPhase))
        t25=' '
      enddo
      write(lst,FormA)
      t25='Actual   cell parameters:'
      write(lst,100) t25,(CellParActual(i),i=1,6)
      if(NDimI(KPhase).gt.0) then
        t25='Actual   q vector'
        if(NDimI(KPhase).eq.1) then
          t25=t25(:idel(t25))//':'
        else
          t25=t25(:idel(t25))//'(s):'
        endif
        do j=1,NDimI(KPhase)
          write(lst,101) t25,(QuActual(i,j),i=1,3)
          t25=' '
        enddo
      endif
      write(lst,FormA)
      return
100   format(a25,3f9.3,3f9.2)
101   format(a25,6f9.3)
      end
