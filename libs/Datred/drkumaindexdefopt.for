      subroutine DRKumaIndexDefOpt(Method,VolMin,VolMax,VolAMin,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Radka
      logical CrwLogicQuest
      id=NextQuestId()
      xqd=300.
      il=3
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Define cell searching '//
     1                   'parameters',0,LightGray,0,0)
      xpom=5.
      tpom=20.
      do il=1,2
        if(il.eq.1) then
          Radka='prefer %larger volume'
        else if(il.eq.2) then
          Radka='prefer %angles closer 90 deg'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Radka,'L',CrwgXd,CrwgYd,
     1                      1,1)
        call FeQuestCrwOpen(CrwLastMade,Method.eq.il)
        if(il.eq.1) then
          nCrwVolume=CrwLastMade
        else if(il.eq.2) then
          nCrwAngles=CrwLastMade
        endif
      enddo
      xpom=tpom+FeTxLengthUnder(Radka)+15.
      dpom=40.
      tpom=xpom+dpom+5.
      do il=1,3
        if(il.eq.1) then
          Radka='AngVolume(%min)'
          pom=VolAMin
        else if(il.eq.2) then
          Radka='Volume(mi%n)'
          pom=VolMin
        else if(il.eq.3) then
          Radka='Volume(ma%x)'
          pom=VolMax
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Radka,'L',dpom,
     1                      EdwYd,0)
        if(il.eq.3.or.il.eq.Method)
     1    call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        if(il.eq.1) then
          nEdwMinAngVol=EdwLastMade
        else if(il.eq.2) then
          nEdwMinVol=EdwLastMade
        else if(il.eq.3) then
          nEdwMaxVol=EdwLastMade
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        if(CheckNumber.eq.nCrwVolume) then
          call FeQuestEdwClose(nEdwMinVol)
          call FeQuestRealEdwOpen(nEdwMinAngVol,VolAMin,.false.,.false.)
        else if(CheckNumber.eq.nCrwAngles) then
          call FeQuestEdwClose(nEdwMinAngVol)
          call FeQuestRealEdwOpen(nEdwMinVol,VolMin,.false.,.false.)
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(CrwLogicQuest(nCrwVolume)) then
          Method=1
          call FeQuestRealFromEdw(nEdwMinAngVol,VolAMin)
        else
          Method=2
          call FeQuestRealFromEdw(nEdwMinVol,VolMin)
        endif
        call FeQuestRealFromEdw(nEdwMaxVol,VolMax)
      endif
      call FeQuestRemove(id)
      return
      end
