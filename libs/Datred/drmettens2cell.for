      subroutine DRMetTens2Cell(gg,Cell)
      include 'fepc.cmn'
      dimension gg(3,3),Cell(6)
      do i=1,3
        Cell(i)=sqrt(gg(i,i))
      enddo
      Cell(4)=acos(gg(2,3)/(Cell(2)*Cell(3)))/ToRad
      Cell(5)=acos(gg(1,3)/(Cell(1)*Cell(3)))/ToRad
      Cell(6)=acos(gg(1,2)/(Cell(1)*Cell(2)))/ToRad
      return
      end
