      subroutine DRKumaIndex(KumaInput)
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 FileIn,StoreFile,t256
      character*100 Radka
      character*80  Veta
      character*45  StoreLabel(10)
      character*8   Nazev
      character*1   ZnakI,ZnakS
      dimension U(3,3),Ui(3,3),hh(3),T(3,3),Tt(3,3),Ti(3,3),IU(4),ik(2),
     1          UStore(3,3),hp(3),xp(3),AM(3,3),AMinv(3,3),PS(3,3),
     2          SCellRefBlock(6),SU(3,3),G(3,3),isel(2),UBFromFile(3,3),
     3          SG(3,3),SlozkyP(4)
      integer ZlomekZIndexu(3),FeMenu,FlagKP,UseTabsIn,
     1        TakeUBFromFile,in(1),SbwLnQuest,SbwItemSelQuest,UseTabsM,
     2        UseTabsC
      logical eqiv,MameMatici,EqIgCase,FeYesNoHeader,ExistFile,FeYesNo,
     1        StepBackPossible
      equivalence (u,ub),(ui,ubi),(AM,SU,T),(AMinv,Ti),(PS,Tt),
     1            (G,DRMetTens),(Radka,Veta)
      data IntMin,IntMax/0,999999/,Method,VolMin/2,10./
      data RelIMin,RelIMax/0.,1000000./,sinthlMin,sinthlMax/0.,1./
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      FileIn=' '
      nStore=0
      MatrixFromFileOK=1
      call SetRealArrayTo(SG,9,0.)
      call SetRealArrayTo(SU,9,0.)
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      KRefBlock=1
      ln=NextLogicNumber()
c      call DRKumaIndexLimits(KumaInput,sinthlMin,sinthlMax,RelIMin,
c     1                       RelIMax,Indexace,ich)
c      if(ich.ne.0) go to 9999
      if(KumaInput.ne.1.and.KumaInput.ne.4) then
        mxi=1000
        allocate(Slozky(4,mxi),Int(mxi))
      endif
      if(KumaInput.eq.1) then
        call FeFileManager('Select peak table file',FileIn,'*.ta*',0,
     1                     .true.,ich)
        if(ich.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        read(ln,FormA80) Radka
        k=0
        call StToInt(Radka,k,in,1,.false.,ich)
        if(ich.ne.0) go to 9999
        mxi=in(1)
        allocate(Slozky(4,mxi),Int(mxi),ha(3,mxi),Uhel(mxi,mxi),
     1           Euler(4,mxi),Delka(mxi),FlagS(mxi),FlagI(mxi),
     2           FlagK(mxi))
        NRef=0
        do i=1,mxi
          read(ln,104) SlozkyP,IntP,FlagKP
          pom=IntP
          if(pom.lt.RelIMin.or.pom.gt.RelIMax) continue
          do j=1,4
            SlozkyP(j)=SlozkyP(j)/LamAveD(6)
          enddo
          pom=.5*VecOrtLen(SlozkyP,3)
          if(pom.lt.sinthlMin.or.pom.gt.sinthlMax) continue
          NRef=NRef+1
          call CopyVek(SlozkyP,Slozky(1,NRef),4)
          Int(NRef)=IntP
          FlagS(NRef)=1
          FlagI(NRef)=0
          FlagK(NRef)=FlagKP
        enddo
        pom=0.
        do i=1,3
          read(ln,104)(UBFromFile(i,j),j=1,3)
          read(ln,104)
          do j=1,3
            UBFromFile(i,j)=UBFromFile(i,j)/LamAveD(6)
            pom=pom+abs(UBFromFile(i,j))
          enddo
          if(pom.lt..0001) then
            MatrixFromFileOK=0
            go to 1280
          endif
        enddo
      else if(KumaInput.eq.2) then
        call FeFileManager('Select p4p file',FileIn,'*.p4p',0,.true.,
     1                     ich)
        if(ich.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        NRef=0
1080    read(ln,FormA,end=1280) t256
        call mala(t256)
        k=0
        call kus(t256,k,Nazev)
        if(Nazev(1:3).eq.'ort') then
          read(Nazev(4:4),'(i1)') i
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          do j=1,3
            UBFromFile(i,j)=xp(j)
          enddo
        else if(Nazev.eq.'ref05'.or.Nazev.eq.'ref1k') then
          NRef=NRef+1
1100      if(Nref.gt.mxi) then
            allocate(SlozkyO(4,mxi),IntO(mxi))
            call CopyVek (Slozky,SlozkyO,4*mxi)
            call CopyVekI(Int,IntO,mxi)
            mxio=mxi
            mxi=mxi+1000
            deallocate(Slozky,Int)
            allocate(Slozky(4,mxi),Int(mxi))
            call CopyVek (SlozkyO,Slozky,4*mxio)
            call CopyVekI(IntO,Int,mxio)
            deallocate(SlozkyO,IntO)
          endif
          call kus(t256,k,Nazev)
          call StToReal(t256,k,hh,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9999
          call StToReal(t256,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9999
          if(xp(1).lt.RelIMin.or.xp(1).gt.RelIMax) then
            NRef=NRef-1
            go to 1080
          endif
          int(NRef)=nint(xp(1))
          call StToReal(t256,k,Slozky(1,NRef),3,.false.,ich)
          if(ich.ne.0) go to 9999
          AA=VecOrtLen(Slozky(1,NRef),3)
          Slozky(4,NRef)=0.
          if(AA.le.0.) NRef=NRef-1
        endif
        go to 1080
      else if(KumaInput.eq.3) then
        ln=NextLogicNumber()
        call FeFileManager('Select peak table file',FileIn,'*.rmat',0,
     1                   .true.,ich)
        if(ich.ne.0) go to 9999
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
1110    read(ln,FormA,end=9999) t256
        k=0
        call kus(t256,k,Nazev)
        if(EqIgCase(Nazev,'rmat')) then
          do i=1,3
            read(ln,FormA,end=9999) t256
            k=0
            call StToReal(t256,k,xp,3,.false.,ich)
            do j=1,3
              UBFromFile(i,j)=xp(j)
            enddo
          enddo
        else
          go to 1110
        endif
        call CloseIfOpened(ln)
        call ExtractDirectory(FileIn,t256)
        FileIn=t256(:idel(t256))//ObrLom//'cell.drx'
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        NRef=0
        read(ln,FormA) t256
1120    read(ln,FormA,end=1280) t256
        if(t256.eq.' ') go to 1120
        NRef=NRef+1
        if(Nref.gt.mxi) then
          allocate(SlozkyO(4,mxi),IntO(mxi))
          call CopyVek (Slozky,SlozkyO,4*mxi)
          call CopyVekI(Int,IntO,mxi)
          mxio=mxi
          mxi=mxi+1000
          deallocate(Slozky,Int)
          allocate(Slozky(4,mxi),Int(mxi))
          call CopyVek (SlozkyO,Slozky,4*mxio)
          call CopyVekI(IntO,Int,mxio)
          deallocate(SlozkyO,IntO)
        endif
        k=0
        call StToReal(t256,k,Slozky(1,NRef),3,.false.,ich)
        if(ich.ne.0) go to 9999
        call StToReal(t256,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9999
        Int(NRef)=nint(xp(1))
        go to 1120
      else if(KumaInput.eq.4) then
        call FeFileManager('Select peak table file',FileIn,'*.txt',0,
     1                     .true.,ich)
        if(ich.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,FileIn,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        mxi=0
1150    read(ln,FormA80,end=1155) Radka
        mxi=mxi+1
        go to 1150
1155    allocate(Slozky(4,mxi),Int(mxi))
        rewind ln
        NRef=0
        do i=1,mxi
          read(ln,*)(SlozkyP(j),j=1,3),pom
          IntP=pom
          if(pom.lt.RelIMin.or.pom.gt.RelIMax) cycle
          pom=.5*VecOrtLen(SlozkyP,3)
          if(pom.lt.sinthlMin.or.pom.gt.sinthlMax) cycle
          SlozkyP(4)=pom
          NRef=NRef+1
          call CopyVek(SlozkyP,Slozky(1,NRef),4)
          Int(NRef)=IntP
        enddo
      endif
1280  call CloseIfOpened(ln)
      if(KumaInput.ne.1) then
        allocate(ha(3,mxi),Uhel(mxi,mxi),Euler(4,mxi),Delka(mxi),
     1           FlagS(mxi),FlagI(mxi),FlagK(mxi))
        call SetIntArrayTo(FlagS,NRef,1)
        call SetIntArrayTo(FlagI,NRef,0)
        call SetIntArrayTo(FlagK,NRef,0)
      endif
      StoreFile='jsvf'
      call CreateTmpFile(StoreFile,i,0)
      call FeTmpFilesAdd(StoreFile)
      do i=1,NRef
        AA=VecOrtLen(Slozky(1,i),3)
        Delka(i)=AA
        Euler(1,i)=asin(.5*AA*LamAveD(6))/ToRad
        Euler(2,i)=2.*Euler(1,i)
        Euler(3,i)=atan2(-Slozky(3,i),
     1                   sqrt(Slozky(1,i)**2+Slozky(2,i)**2))/ToRad
        if(Slozky(1,i).eq.0.and.Slozky(2,i).eq.0.) then
          Euler(4,i)=90.
        else
          Euler(4,i)=atan2(-Slozky(1,i),Slozky(2,i))/ToRad
        endif
        if(Euler(4,i).lt.0.) Euler(4,i)=360.+Euler(4,i)
        Uhel(i,i)=0.
        do j=1,i-1
          pom=VecOrtScal(Slozky(1,i),Slozky(1,j),3)/
     1        (AA*VecOrtLen(Slozky(1,j),3))
          Uhel(i,j)=pom
          Uhel(j,i)=pom
        enddo
      enddo
      MameMatici=.false.
1300  id=NextQuestId()
      call FeTabsReset(UseTabs)
      pom=60.
      do i=1,4
        call FeTabsAdd(pom,UseTabs,IdCharTab,'.')
        pom=pom+40.
      enddo
      pom=220.
      do i=1,4
        call FeTabsAdd(pom,UseTabs,IdCharTab,'.')
        pom=pom+60.
      enddo
      pom=pom+30.
      call FeTabsAdd(pom,UseTabs,IdLeftTab,' ')
      il=15
      xqd =550.
      xqdp=xqd*.5
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Select peaks '//
     1                   'for indexation',0,LightGray,0,0)
      il=1
      call FeQuestLblMake(id,105.,il,'Indices','L','N')
      call FeQuestLblMake(id,310.,il,'Angles','L','N')
      call FeQuestLblMake(id,460.,il,'Intensity','L','N')
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=13
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      il=il+1
      dpom=60.
      xpom=xqdp-2.5*dpom-30.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Indexed')
      nButtIndexed=ButtonLastMade
      if(MameMatici) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,i)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Refresh')
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Select %all')
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Filter')
      nButtFilter=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Re%verse')
      nButtReverse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      dpom=100.
      xpom=xqdp-1.5*dpom-15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Optimal triplet')
      nButtOptimal=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%UB as read in')
      nButtUBFromFile=ButtonLastMade
      if(MatrixFromFileOK.gt.0) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,i)
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'I%mport UB')
      nButtUBImport=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1400  ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_refind.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NRef
        write(Radka,'(''#'',i5)') i
        k=7
        do j=1,3
          if(MameMatici) then
            write(Radka(k:),'(a1,f8.3)') Tabulator,ha(j,i)
          else
            Radka(k:)=Tabulator//'-----'
          endif
          k=k+9
        enddo
        call Zhusti(Radka)
        k=idel(Radka)+1
        if(FlagI(i).eq.1) then
          Radka(k:)=Tabulator//'X'
        else
          Radka(k:)=Tabulator//' '
        endif
        k=k+2
        do j=1,4
          write(Radka(k:),'(a1,f8.3)') Tabulator,Euler(j,i)
          k=k+9
        enddo
        write(Radka(k:),'(a1,i9)') Tabulator,nint(Int(i))
        write(ln,FormA) Radka(:idel(Radka))
        if(mod(FlagS(i),10).eq.1) then
          j=0
          if(FlagS(i)/10.gt.0) j=1
        else
          j=-1
        endif
        call FeQuestSetSbwItemSel(i,nSbw,j)
      enddo
      call CloseIfOpened(ln)
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_refind.tmp')
      TakeUBFromFile=0
1600  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtRefresh) then
          i=0
          call SetIntArrayTo(FlagS,NRef,0)
        else if(CheckNumber.eq.nButtAll) then
          do i=1,NRef
            FlagS(i)=(FlagS(i)/10)*10+1
          enddo
        else if(CheckNumber.eq.nButtReverse) then
          do i=1,NRef
            if(FlagS(i).eq.0) then
              FlagS(i)=1
            else
              FlagS(i)=0
            endif
          enddo
        else if(CheckNumber.eq.nButtIndexed) then
          do i=1,NRef
            if(FlagI(i).eq.0) FlagS(i)=0
          enddo
        else if(CheckNumber.eq.nButtFilter) then
          idp=NextQuestId()
          call FeQuestCreate(idp,-1.,-1.,170.,2,' ',0,LightGray,0,0)
          il=1
          tpom=5.
          xpom=60.
          dpom=90.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,'Int(min)','L',dpom,
     1                        EdwYd,0)
          nEdwIntMin=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,IntMin,.false.)
          il=il+1
          call FeQuestEdwMake(idp,tpom,il,xpom,il,'Int(max)','L',dpom,
     1                        EdwYd,0)
          nEdwIntMax=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,IntMax,.false.)
2000      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2000
          endif
          if(ich.eq.0) then
            call FeQuestIntFromEdw(nEdwIntMin,IntMin)
            call FeQuestIntFromEdw(nEdwIntMax,IntMax)
            do i=1,NRef
              if(Int(i).lt.IntMin.or.Int(i).gt.IntMax) FlagS(i)=0
            enddo
          endif
          call FeQuestRemove(idp)
        else if(CheckNumber.eq.nButtOptimal) then
          call DRKumaIndexDefOpt(Method,VolMin,VolMax,VolAMin,ich)
          if(ich.ne.0) go to 1600
          n=0
          do i=1,NRef-2
            if(flagS(i).eq.0) cycle
            do j=i+1,NRef-1
              if(flagS(j).eq.0) cycle
              do k=j+1,NRef
                if(flagS(k).eq.0) cycle
                n=n+1
              enddo
            enddo
          enddo
c          n=0
c          do i=1,NRef
c            if(flagS(i).eq.0) n=n+1
c          enddo
c          n=((n-2)*(n-1)*n)/6
          call FeFlowChartOpen(-1.,-1.,max(n/10,1),n,
     1                         'Searching for optimal triplet.',' ',' ')

          VMax=0.
          iz=0
          do i=1,NRef-2
            if(flagS(i).eq.0) cycle
            do j=i+1,NRef-1
              if(flagS(j).eq.0) cycle
              CosGama=Uhel(i,j)
              do k=j+1,NRef
                if(flagS(k).eq.0) cycle
                call FeFlowChartEvent(iz,is)
                if(is.ne.0) then
                  call FeBudeBreak
                  if(ErrFlag.ne.0) go to 2050
                endif
                CosAlfa=Uhel(i,k)
                CosBeta=Uhel(j,k)
                VolA=1.-CosAlfa**2-CosBeta**2-CosGama**2
     1                +2.*CosAlfa*CosBeta*CosGama
                Vol=VolA*Delka(i)*Delka(j)*Delka(k)
                if(Vol.lt.1./VolMax) cycle
                Vol=1./Vol
                if(Method.eq.1) then
                  if(VolA.lt.VolAMin) cycle
                else
                  if(Vol.lt.VolMin) cycle
                  Vol=VolA
                endif
                if(Vol.gt.VMax) then
                  iv1=i
                  iv2=j
                  iv3=k
                  VMax=Vol
                endif
               enddo
            enddo
          enddo
2050      if(VMax.gt.0.) then
            do i=1,NRef
              FlagS(i)=mod(FlagS(i),10)
            enddo
            FlagS(iv1)=FlagS(iv1)+10
            FlagS(iv2)=FlagS(iv2)+10
            FlagS(iv3)=FlagS(iv3)+10
          endif
          call FeFlowChartRemove
        else if(CheckNumber.eq.nButtUBFromFile) then
          TakeUBFromFile=1
          call CopyMat(UBFromFile,U,3)
          EventType=EventButton
          EventNumber=ButtonOK
          go to 1600
        else if(CheckNumber.eq.nButtUBImport) then
          TakeUBFromFile=2
          call FeFileManager('Select peak table file for UB import',
     1                       FileIn,'*.ta*',0,.true.,ich)
          if(ich.eq.0) then
            call OpenFile(ln,FileIn,'formatted','old')
            if(ErrFlag.ne.0) go to 1600
            read(ln,FormA80) Radka
            k=0
            call StToInt(Radka,k,in,1,.false.,ich)
            if(ich.ne.0) go to 9999
            NRefP=in(1)
            do i=1,NrefP
              read(ln,104) pom
            enddo
            do i=1,3
              read(ln,104)(U(i,j),j=1,3)
              read(ln,104)
              do j=1,3
                U(i,j)=U(i,j)/LamAveD(6)
              enddo
            enddo
            call CloseIfOpened(ln)
            EventType=EventButton
            EventNumber=ButtonOK
            go to 1600
          endif
        endif
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0) then
        do i=1,NRef
          j=SbwItemSelQuest(i,nSbw)
          if(j.eq.1) then
            FlagS(i)=11
          else if(j.eq.0) then
            FlagS(i)=1
          else
            FlagS(i)=0
          endif
        enddo
        NRefForTriplets=0
        if(TakeUBFromFile.eq.0) then
          do i=1,NRef
            if(FlagS(i).gt.10) NRefForTriplets=NRefForTriplets+1
          enddo
          if(NRefForTriplets.lt.3) then
            NInfo=1
            TextInfo(1)='Warning - no triplet defined!'
            if(FeYesNoHeader(-1.,-1.,'Do you want to finish?',0)) then
              ich=1
              go to 2350
            else
              call FeQuestButtonOff(ButtonOk-ButtonFr+1)
              call CloseIfOpened(SbwLnQuest(nSbw))
              go to 1400
            endif
          endif
        endif
        NRefSel=0
        do i=1,NRef
          if(FlagS(i).ne.0) NRefSel=NRefSel+1
        enddo
        if(NRefSel.lt.3) then
          NInfo=1
          TextInfo(1)='Warning - too low number of selected reflection!'
          if(FeYesNoHeader(-1.,-1.,'Do you want to finish?',0)) then
            ich=1
            go to 2350
          else
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            go to 1400
          endif
        endif
      endif
2350  call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_refind.tmp')
      if(ich.ne.0) go to 9999
      NRefOut=20
      nmax=min(NRefOut,NRef)
      do 5000iv1=1,NRef-2
        if(FlagS(iv1).lt.10.and.TakeUBFromFile.eq.0) go to 5000
        do 4900iv2=iv1+1,NRef-1
          if(FlagS(iv2).lt.10.and.TakeUBFromFile.eq.0) go to 4900
          do 4800iv3=iv2+1,NRef
            if(FlagS(iv3).lt.10.and.TakeUBFromFile.eq.0) go to 4800
            call SetRealArrayTo(SCellRefBlock,6,0.)
            if(TakeUBFromFile.ne.0) then
              if(TakeUBFromFile.eq.1) call CopyVek(UBFromFile,U,9)
              Radka='Taken from file'
            else
              call CopyVek(Slozky(1,iv1),U(1,1),3)
              call CopyVek(Slozky(1,iv2),U(1,2),3)
              call CopyVek(Slozky(1,iv3),U(1,3),3)
              write(Radka,'(3i4)') iv1,iv2,iv3
              Radka='Triplet : '//Radka(:idel(Radka))
            endif
            call Matinv(U,Ui,Vol,3)
            if(abs(Vol).lt..0001) then
              call FeMsgOut(-1.,-1.,Radka(:idel(Radka))//' - skipped')
              go to 4800
            endif
            id=NextQuestId()
            xqd =550.
            xqdp=xqd*.5
            il=20
            StepBackPossible=.false.
            call FeQuestCreate(id,-1.,-1.,xqd,il,Radka,0,LightGray,-1,
     1                         -1)
            dpom=70.
            fnb=6.
            space=12.
            xpom=xqdp-fnb*dpom*.5-(fnb-1.)*space*.5
            il=18
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Red%uce')
            nButtReduce=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Supercell')
            nButtSuper=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Doublecell')
            nButtDouble=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Matrix')
            nButtMatrix=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Step ba%ck')
            nButtStepBack=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Re%fine')
            nButtRefine=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            il=il+1
            fnb=5.
            xpom=xqdp-fnb*dpom*.5-(fnb-1.)*space*.5
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Options')
            nButtOptions=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'S%tore')
            nButtStore=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Restore')
            nButtRestore=ButtonLastMade
            if(nStore.gt.0) then
              j=ButtonOff
            else
              j=ButtonDisabled
            endif
            call FeQuestButtonOpen(ButtonLastMade,j)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'S%ave')
            nButtSave=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            xpom=xpom+dpom+space
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Com%pare')
            nButtCompare=ButtonLastMade
            if(nStore.gt.1) then
              j=ButtonOff
            else
              j=ButtonDisabled
            endif
            call FeQuestButtonOpen(ButtonLastMade,j)
            il=il+1
            xpom=xqdp-.5*dpom
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Quit')
            nButtQuit=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            pom=dpom
            dpom=60.
            xpom=xqdp-dpom-pom*.5-10.
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,
     1                             'Ne%xt triplet')
            nButtNextTriplet=ButtonLastMade
            if(NRefForTriplets.gt.3) then
              j=ButtonOff
            else
              j=ButtonDisabled
            endif
            call FeQuestButtonOpen(ButtonLastMade,j)
            xpom=xpom+dpom+pom+20.
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Ne%w select')
            nButtNewSelect=ButtonLastMade
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            m=1
            call SetStringArrayTo(TextInfo,8,' ')
            xpom=5.
            dpom=xqd-10.
            ypom=260.
            call FeQuestAbsSbwMake(id,xpom,ypom,dpom,220.,2,
     1                             CutTextFromLeft,SbwHorizontal)
            nSbw=SbwLastMade
            ypom=ypom-20.
            UseTabsM=NextTabs()
            pom=30.
            do i=1,3
              call FeTabsAdd(pom,UseTabsM,IdCharTab,'.')
              pom=pom+60.
            enddo
            pom=300.
            do i=1,3
              call FeTabsAdd(pom,UseTabsM,IdCharTab,'.')
              pom=pom+60.
            enddo
            UseTabsC=NextTabs()
            pom=110.
            do i=1,6
              call FeTabsAdd(pom,UseTabsC,IdCharTab,'.')
              pom=pom+50.
            enddo
            do il=1,9
              ypom=ypom-15.
              if(il.ge.4.and.il.le.6) UseTabs=UseTabsM
              if(il.ge.8.and.il.le.9) UseTabs=UseTabsC
              if(il.eq.1) then
                xpom=xqdp
                ZnakI='C'
              else if(il.eq.2) then
                xpom=60.
                ZnakI='L'
              else if(il.eq.3) then
                xpom=350.
                ypom=ypom+15.
                ZnakI='L'
              else if(il.eq.7) then
                xpom=xqdp
                ZnakI='C'
              else
                xpom=5.
                ZnakI='L'
              endif
              call FeQuestAbsLblMake(id,xpom,ypom,TextInfo(il),ZnakI,
     1                               'N')
              call FeQuestLblOff(LblLastMade)
              if(il.eq.1.or.il.eq.6) then
                if(il.eq.1) nLblRefFirst=LblLastMade
                ypom=ypom-10.
                call FeQuestAbsLinkaMake(id,ypom)
              endif
            enddo
2500        nBasVec=3
            do i=1,3
              do j=1,4
                if(i.eq.j.or.j.eq.4) then
                  BasVec(j,i)=1
                else
                  BasVec(j,i)=0
                endif
              enddo
              BasVecFlag(i)=.true.
              MBasVec(i)=0
            enddo
            call Matinv(U,Ui,Vol,3)
            if(Vol.lt.0.) then
              do i=1,3
                do j=1,3
                  U (i,j)=-U (i,j)
                  Ui(i,j)=-Ui(i,j)
                enddo
              enddo
              Vol=-Vol
            endif
            call DRCellFromUB
            n1=nmax*(m-1)+1
            n2=min(NRef,n1+nmax-1)
            nLbl=nLblRefFirst
            do il=1,9
              TextInfo(il)=' '
              call FeQuestLblChange(nLbl,TextInfo(il))
              nLbl=nLbl+1
            enddo
            kk=1
            IndexedAll=0
            IndexedSel=0
            NRefSel=0
            call CloseIfOpened(SbwLnQuest(nSbw))
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'_lstref.tmp','formatted',
     1                    'unknown')
            do 2580i=1,NRef
              if(FlagS(i).ne.0) then
                ZnakS='S'
                NRefSel=NRefSel+1
              else
                ZnakS=' '
              endif
              call multm(Ui,Slozky(1,i),ha(1,i),3,3,1)
              do j=1,3
                hp(j)=anint(ha(j,i))-ha(j,i)
              enddo
              call multm(U,hp,xp,3,3,1)
              if(asin(.5*VecOrtLen(xp,3)*LamAveD(6))/ToRad.lt.DiffMax)
     1            then
                FlagI(i)=1
                ZnakI='X'
                IndexedAll=IndexedAll+1
                if(FlagS(i).ne.0) IndexedSel=IndexedSel+1
              else
                FlagI(i)=0
                ZnakI=' '
              endif
              write(ln,'(''#'',i5,3f8.3,2(1x,a1))') i,(ha(j,i),j=1,3),
     1                                              ZnakI,ZnakS
              if(FlagS(i).eq.0) go to 2580
              do k=1,10
                fk=k
                do j=1,3
                  hp(j)=anint(ha(j,i)*fk)/fk-ha(j,i)
                enddo
                call multm(U,hp,xp,3,3,1)
                if(asin(.5*VecOrtLen(xp,3)*LamAveD(6))/ToRad.lt.DiffMax)
     1            go to 2540
              enddo
              cycle
2540          MinMult=k
              do j=1,3
                ZlomekZIndexu(j)=nint(ha(j,i)*fk)
              enddo
              do j=1,MinMult-1
                do k=1,3
                  IU(k)=ZlomekZIndexu(k)*j
                enddo
                IU(4)=MinMult
                do k=1,3
2556              if(IU(k).ge.MinMult) then
                    IU(k)=IU(k)-MinMult
                    go to 2556
                  endif
2558              if(IU(k).lt.0) then
                    IU(k)=IU(k)+MinMult
                    go to 2558
                  endif
                enddo
                call MinMultMaxFract(IU,4,k,MaxFract)
                do k=1,4
                  IU(k)=IU(k)/MaxFract
                enddo
                call MinMultMaxFract(IU,3,k,MaxFract)
                do k=1,3
                  IU(k)=IU(k)/MaxFract
                enddo
                do k=1,nBasVec
                  if(eqiv(IU,BasVec(1,k),4)) then
                    MBasVec(k)=MBasVec(k)+1
                    go to 2580
                  endif
                enddo
                if(nBasVec.lt.22) then
                  nBasVec=nBasVec+1
                  call CopyVekI(IU,BasVec(1,nBasVec),4)
                  MBasVec(nBasVec)=0
                endif
              enddo
2580        continue
            call CloseIfOpened(ln)
            call FeQuestSbwTextOpen(nSbw,20,fln(:ifln)//'_lstref.tmp')
            if(IndexedAll.le.3) then
              call FeQuestButtonDisable(nButtRefine)
            else
              call FeQuestButtonOff(nButtRefine)
            endif
            if(StepBackPossible) then
              call FeQuestButtonOff(nButtStepBack)
            else
              call FeQuestButtonDisable(nButtStepBack)
            endif
            if(nStore.gt.1) then
              call FeQuestButtonOff(nButtCompare)
            else
              call FeQuestButtonDisable(nButtCompare)
            endif
            if(nBasVec.le.3.and.BasVec(4,1).eq.1.and.BasVec(4,2).eq.1.
     1                     .and.BasVec(4,3).eq.1) then
              call FeQuestButtonDisable(nButtSuper)
            else
              call FeQuestButtonOff(nButtSuper)
            endif
            write(Radka,103) IndexedAll,NRef
            call Zhusti(Radka)
            write(Cislo,'(f5.1)') float(IndexedAll)/float(NRef)*100.
            call ZdrcniCisla(Cislo,1)
            il=1
            TextInfo(il)(5:)='Indexed :   all '//Radka(:idel(Radka))//
     1                       ' ~ '//Cislo(:idel(Cislo))//'%'
            write(Radka,103) IndexedSel,NRefSel
            call Zhusti(Radka)
            write(Cislo,'(f5.1)') float(IndexedSel)/float(NRefSel)*100.
            call ZdrcniCisla(Cislo,1)
            TextInfo(il)(idel(TextInfo(il))+3:)='selected '//
     1        Radka(:idel(Radka))//' ~ '//Cislo(:idel(Cislo))//'%'
            il=il+1
            TextInfo(il)='Orientation matrix'
            il=il+1
            TextInfo(il)='Metric tensor'
            do i=1,3
              il=il+1
              write(TextInfo(il),'(3(a1,f10.6))')
     1          (Tabulator,U(i,j),j=1,3)
              write(TextInfo(il)(34:),'(3(a1,f10.3))')
     1          (Tabulator,G(i,j),j=1,3)
              call Zhusti(TextInfo(il))
            enddo
            il=il+1
            write(TextInfo(il),'(''Volume :'',f10.2)') 1./Vol
            do i=1,2
              il=il+1
              if(i.eq.1) then
                write(TextInfo(il),100) 'Cell parameters:',
     1                           (Tabulator,CellRefBlock(j,1),j=1,6)
              else
                write(TextInfo(il),100) 'e.s.d.:         ',
     1                            (Tabulator,SCellRefBlock(j),j=1,6)
              endif
              call Zhusti(TextInfo(il)(17:))
              k=k+3
            enddo
            nLbl=nLblRefFirst
            do il=1,9
              call FeQuestLblChange(nLbl,TextInfo(il))
              nLbl=nLbl+1
            enddo
3000        call FeQuestEvent(id,ich)
            if(CheckType.eq.EventButton.and.
     1         CheckNumber.eq.nButtStepBack) then
              call CopyMat(UStore,U,3)
              call FeQuestButtonOff(nButtStepBack)
              StepBackPossible=.false.
              go to 2500
            else
              call CopyMat(U,UStore,3)
            endif
            if((CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape).or.
     1         (CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit))
     2        then
              if(FeYesNo(-1.,-1.,'Do you really want to quit indexing'//
     1                   ' program?',0)) then
                call FeQuestRemove(id)
                go to 5100
              else
                go to 3000
              endif
            else if(CheckType.eq.EventButton) then
              if(CheckNumber.eq.nButtNextTriplet) then
                go to 4790
              else if(CheckNumber.eq.nButtOptions) then
                idp=NextQuestId()
                call FeQuestCreate(idp,-1.,-1.,200.,1,
     1                             'Define maximal difference',0,
     2                             LightGray,0,0)
                il=1
                tpom=5.
                Veta='%Delta in degs:'
                xpom=tpom+FeTxLengthUnder(Veta)+10.
                dpom=60.
                call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,
     1                              EdwYd,0)
                nEdwDelta=EdwLastMade
                call FeQuestRealEdwOpen(EdwLastMade,DiffMax,.false.,
     1                                  .false.)
3100            call FeQuestEvent(idp,ich)
                if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 3100
                endif
                if(ich.eq.0) then
                  call FeQuestRealFromEdw(nEdwDelta,DiffMax)
                endif
                call FeQuestRemove(idp)
                go to 2500
              else if(CheckNumber.eq.nButtStore) then
                if(nStore.ge.10) then
                  call FeChybne(-1.,-1.,'number of stored matrices '//
     1                          'limited to 10.',' ',SeriousError)
                  go to 2500
                else if(nStore.eq.0) then
                  call FeQuestButtonOff(nButtReStore)
                endif
                nStore=nStore+1
                write(Radka,103) IndexedAll,NRef
                write(Cislo,'(f5.1)') float(IndexedAll)/float(NRef)*100.
                write(StoreLabel(nStore),'(''Vol= '',f6.1)') 1./Vol
                StoreLabel(nStore)=
     1            StoreLabel(nStore)(:idel(StoreLabel(nStore)))//
     2            ' indexed '//Radka(:idel(Radka))//' ~ '//
     3            Cislo(:idel(Cislo))//ObrLom//'%'
                ln=NextLogicNumber()
                call OpenFile(ln,StoreFile,'formatted','unknown')
                if(ErrFlag.ne.0) go to 2500
3200            read(ln,FormA80,end=3205) Veta
                go to 3200
3205            backspace ln
                write(ln,'(''begin'')')
                write(ln,101) NRef,IndexedAll,NRefSel,IndexedSel,1./Vol
                do i=1,NRef
                  write(ln,102)(Slozky(j,i),j=1,3),FlagS(i),FlagI(i)
                enddo
                do i=1,3
                  write(ln,102)(U(i,j),j=1,3)
                enddo
                write(ln,'(''end'')')
                call CloseIfOpened(ln)
                go to 2500
              else if(CheckNumber.eq.nButtRestore) then
                ln=NextLogicNumber()
                call OpenFile(ln,StoreFile,'formatted','old')
                if(ErrFlag.ne.0) go to 2500
3305            i=FeMenu(-1.,-1.,StoreLabel,1,nStore,1,0)
                if(i.ge.1.and.i.le.nStore) then
                  rewind ln
                  n=0
3310              read(ln,FormA80,end=3390) Veta
                  if(EqIgCase(Veta,'begin')) then
                    n=n+1
                    if(n.ge.i) then
                      read(ln,101,err=3390,end=3390)
     1                  NRef,IndexedAll,NRefSel,IndexedSel,dpom
                      do i=1,NRef
                        read(ln,102,err=3390,end=3390)
     1                    (Slozky(j,i),j=1,3),FlagS(i),FlagI(i)
                      enddo
                      do i=1,3
                        read(ln,102,err=3390,end=3390)(U(i,j),j=1,3)
                      enddo
                      go to 3390
                    endif
                  endif
                  go to 3310
                endif
3390            call CloseIfOpened(ln)
                go to 2500
              else if(CheckNumber.eq.nButtCompare) then
                call SetStringArrayTo(TextInfo,3,' ')
                idp=NextQuestId()
                Radka='Relationship between stored lattices'
                StringL=PropFontWidthInPixels*42.
                xqd =2.*StringL+15.
                xqdp=xqd*.5
                yqd=(NStore+4)*7.+33.
                call FeQuestAbsCreate(idp,-1.,-1.,xqd,yqd,Radka,0,
     1                                LightGray,-1,0)
                ypom=yqd-9.
                xpom=xpom+StringL*.5
                Radka='Lattice #1'
                call FeQuestAbsLblMake(idp,xpom,ypom,Radka,'C','N')
                xpom=xpom+StringL+5.
                Radka='Lattice #2'
                call FeQuestAbsLblMake(idp,xpom,ypom,Radka,'C','N')
                xpom1=5.
                xpom2=xpom1+StringL+5.
                tpom1=xpom1+CrwgXd+3.
                tpom2=xpom2+CrwgXd+3.
                ypom=ypom-9.
                do i=1,NStore
                  call FeQuestAbsCrwMake(idp,tpom1,ypom,xpom1,ypom-2.,
     1                                   StoreLabel(i),'L',CrwXd,CrwYd,
     2                                   1,1)
                  call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                  if(i.eq.1) nCrwFirst=CrwLastMade
                  call FeQuestAbsCrwMake(idp,tpom2,ypom,xpom2,ypom-2.,
     1                                   StoreLabel(i),'L',CrwXd,CrwYd,
     2                                   1,2)
                  call FeQuestCrwOpen(CrwLastMade,i.eq.1)
                  ypom=ypom-7.
                enddo
                ypom=ypom-2.
                call SetIntArrayTo(isel,2,1)
                Radka='Twinning matrix #1 to #2'
                call FeQuestAbsLblMake(idp,xqdp,ypom,Radka,'C','N')
                xpom=xqdp-12.*PropFontWidthInPixels
                do il=1,3
                  ypom=ypom-7.
                  call FeQuestAbsLblMake(idp,xpom,ypom,' ','L','N')
                  call FeQuestLblOff(LblLastMade)
                  if(il.eq.1) nLblMatFirst=LblLastMade
                enddo
3450            call OpenFile(ln,StoreFile,'formatted','old')
                do 3480ii=1,2
                  rewind ln
                  k=0
3460              read(ln,FormA80,end=3480) Veta
                  if(EqIgCase(Veta,'begin')) then
                    k=k+1
                    if(k.ge.isel(ii)) then
                      read(ln,101,err=3480,end=3480)
     1                       NRef,IndexedAll,NRefSel,IndexedSel,dpom
                      do i=1,NRef
                        read(ln,102,err=3480,end=3480)
     1                       (Slozky(j,i),j=1,3),FlagS(i),FlagI(i)
                      enddo
                      do i=1,3
                        read(ln,102,err=3480,end=3480)(Tt(i,j),j=1,3)
                      enddo
                      if(ii.eq.1) then
                        call CopyVek(Tt,U,9)
                      else
                        call matinv(Tt,Ui,pom,3)
                      endif
                      go to 3480
                    endif
                  endif
                  go to 3460
3480            continue
                call CloseIfOpened(ln)
                call multm(Ui,U,Tt,3,3,3)
                call TrMat(Tt,T,3,3)
                nLbl=nLblMatFirst
                do il=1,3
                  write(TextInfo(il),'(3f8.4)')(T(il,i),i=1,3)
                  call FeQuestLblChange(nLbl,TextInfo(il))
                  nLbl=nLbl+1
                enddo
3490            call FeQuestEvent(idp,ich)
                if(CheckType.eq.EventCrw) then
                  i=CheckNumber-nCrwFirst+1
                  ii=mod(i-1,2)+1
                  i=(i-1)/2+1
                  isel(ii)=i
                  go to 3450
                else if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 3490
                endif
                call FeQuestRemove(idp)
                go to 2500
              else if(CheckNumber.eq.nButtSave) then
                call FeFileManager('Select output peak table file',
     1                             FileIn,'*.ta*',0,.true.,ich)
                if(ich.eq.0) then
                  if(ExistFile(FileIn)) then
                    NInfo=1
                    TextInfo(1)='The file "'//FileIn(:idel(FileIn))//
     1                          '" already exists'
                    if(.not.FeYesNoHeader(-1.,-1.,
     1                 'Do you want to rewrite it?',0)) go to 2500
                  endif
                  ln=NextLogicNumber()
                  call OpenFile(ln,FileIn,'formatted','unknown')
                  if(ErrFlag.ne.0) go to 3550
                  write(Cislo,FormI15) NRef
                  call Zhusti(Cislo)
                  write(ln,FormA1)(Cislo(i:i),i=1,idel(Cislo))
                  write(ln,104)((Slozky(j,i)*LamAveD(6),j=1,4),Int(i),
     1                          FlagK(i),i=1,NRef)
                  do i=1,3
                    write(ln,104)( U(i,j)*LamAveD(6),j=1,3)
                    write(ln,104)(SU(i,j)*LamAveD(6),j=1,3)
                  enddo
                  do i=1,3
                    write(ln,104)( G(i,j)*LamAveD(6)**2,j=1,3)
                    write(ln,104)(SG(i,j)*LamAveD(6)**2,j=1,3)
                  enddo
                  write(ln,'(2i10)') 0,1
3550              call CloseIfOpened(ln)
                endif
                go to 2500
              else if(CheckNumber.eq.nButtNewSelect) then
                call FeQuestRemove(id)
                MameMatici=.true.
                go to 1300
              else if(CheckNumber.eq.nButtReduce) then
                call UnitMat(T,3)
                call DRCellReduction(CellRefBlock(1,1),T)
                call TrMat(T,Tt,3,3)
                call matinv(Tt,T,det,3)
              else if(CheckNumber.eq.nButtMatrix) then
                call UnitMat(T,3)
                call FeReadRealMat(-1.,-1.,'Transformation matrix',
     1            SmbABC,IdAddPrime,SmbABC,t,ti,3,CheckSingYes,
     2            CheckPosDefYes,ich)
                if(ich.ne.0) go to 2500
                call TrMat(Ti,T,3,3)
              else if(CheckNumber.eq.nButtDouble) then
                call UnitMat(T,3)
                call DRDoubleCell(T,CellRefBlock(1,1),k)
                call matinv(T,Ti,det,3)
                call TrMat(Ti,T,3,3)
              else if(CheckNumber.eq.nButtSuper) then
                idp=NextQuestId()
                xqd=400.
                dpom=140.
                xqdp=xqd*.5
                il=12
                Radka='Select a new base vectors'
                call FeQuestCreate(idp,-1.,-1.,xqd,12,Radka,0,
     1                             LightGray,0,0)
                xpom=5.
                dpom=xqd-10.-SbwPruhXd
                il=11
                call FeQuestSbwMake(idp,xpom,il,dpom,il,2,
     1                              CutTextFromLeft,SbwVertical)
                nSbw=SbwLastMade
                il=il+1
                dpom=60.
                xpom=xqdp-dpom-5.
                call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,
     1                                 '%Refresh')
                nButtRefresh=ButtonLastMade
                call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
                xpom=xpom+dpom+10.
                call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,
     1                                 'Select %all')
                nButtAll=ButtonLastMade
                call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
3600            ln=NextLogicNumber()
                call OpenFile(ln,fln(:ifln)//'_basvec.tmp','formatted',
     1                        'unknown')
                if(ErrFlag.ne.0) go to 3990
                do i=4,nBasVec
                  Radka='('
                  kk=2
                  do j=1,3
                    ik(1)=BasVec(j,i)
                    ik(2)=BasVec(4,i)
                    call MinMultMaxFract(ik,2,k,MaxFract)
                    do k=1,2
                      ik(k)=ik(k)/MaxFract
                    enddo
                    if(ik(1).eq.0) then
                      Radka(kk:kk+1)='0,'
                    else if(ik(1).eq.1.and.ik(2).eq.1) then
                      Radka(kk:kk+1)='1,'
                    else
                      write(Radka(kk:),'(i3,''/'',i3,'','')') ik
                    endif
                    call Zhusti(Radka)
                    kk=idel(Radka)
                    if(j.eq.3) then
                      Radka(kk:kk)=')'
                    else
                      kk=kk+1
                    endif
                    write(Cislo,FormI15) MBasVec(i)
                    call Zhusti(Cislo)
                    Radka=Radka(:idel(Radka))//' ----- '//
     1                    Cislo(:idel(Cislo))
                  enddo
                  write(ln,FormA) Radka(:idel(Radka))
                enddo
                call CloseIfOpened(ln)
                call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//
     1                                    '_basvec.tmp')
3700            call FeQuestEvent(idp,ich)
                if(CheckType.eq.EventButton) then
                  if(CheckNumber.eq.nButtRefresh) then
                    j=0
                  else if(CheckNumber.eq.nButtAll) then
                    j=1
                  endif
                  do i=1,nBasVec-3
                    call FeQuestSetSbwItemSel(i,nSbw,j)
                  enddo
                  call CloseIfOpened(SbwLnQuest(nSbw))
                  go to 3600
                else if(CheckType.ne.0) then
                  call NebylOsetren
                  go to 3700
                endif
                if(ich.eq.0) then
                  do i=1,nBasVec-3
                    BasVecFlag(i+3)=SbwItemSelQuest(i,nSbw).eq.1
                  enddo
                  IVolMax=0
                  do i=1,nBasVec-2
                    if(.not.BasVecFlag(i)) cycle
                    do n=1,3
                      Ti(n,1)=float(BasVec(n,i))/float(BasVec(4,i))
                    enddo
                    do j=i+1,nBasVec-1
                      if(.not.BasVecFlag(j)) cycle
                      do n=1,3
                        Ti(n,2)=float(BasVec(n,j))/float(BasVec(4,j))
                      enddo
                      do k=j+1,nBasVec
                        if(.not.BasVecFlag(k)) cycle
                        do n=1,3
                          Ti(n,3)=float(BasVec(n,k))/float(BasVec(4,k))
                        enddo
                        call matinv(Ti,Tt,Vol,3)
                        if(Vol.gt..0001) then
                          IVol=nint(1./Vol)
                          if(IVol.ge.IVolMax) then
                            call multm(UStore,Ti,U,3,3,3)
                            call DRCellFromUB
                            CosAlfa=cos(CellRefBlock(4,1)*ToRad)
                            CosBeta=cos(CellRefBlock(5,1)*ToRad)
                            CosGama=cos(CellRefBlock(6,1)*ToRad)
                            Ang=sqrt(1.-CosAlfa**2-CosBeta**2-CosGama**2
     1                               +2.*CosAlfa*CosBeta*CosGama)
                            if(IVol.gt.IVolMax) AngMax=0.
                            if(Ang.gt.AngMax) then
                              call CopyMat(Ti,T,3)
                              IVolMax=IVol
                              AngMax=Ang
                            endif
                          endif
                        endif
                      enddo
                    enddo
                  enddo
                endif
3990            call FeQuestRemove(idp)
                call DeleteFile(fln(:ifln)//'_basvec.tmp')
                if(ich.ne.0) go to 2500
              else if(CheckNumber.eq.nButtRefine) then
                call SetRealArrayTo(am,9,0.)
                call SetRealArrayTo(ps,9,0.)
                do 4060i=1,NRef
                  if(FlagI(i).eq.0) go to 4060
                  do j=1,3
                    hp(j)=anint(ha(j,i))
                  enddo
                  do j=1,3
                    do k=j,3
                      AM(j,k)=AM(j,k)+hp(j)*hp(k)
                    enddo
                  enddo
                  do j=1,3
                    do k=1,3
                      PS(j,k)=PS(j,k)+Slozky(j,i)*hp(k)
                    enddo
                  enddo
4060            continue
                am(2,1)=am(1,2)
                am(3,1)=am(1,3)
                am(3,2)=am(2,3)
                call matinv(AM,AMinv,det,3)
                call Multm(PS,AMinv,U,3,3,3)
                if(IndexedAll.le.3) go to 2500
                do j=1,3
                  pom=0.
                  do 4080i=1,NRef
                    if(FlagI(i).eq.0) go to 4080
                    do k=1,3
                      hp(k)=anint(ha(k,i))
                    enddo
                    pp=Slozky(j,i)
                    do k=1,3
                      pp=pp-U(j,k)*hp(k)
                    enddo
                    pom=pom+pp**2
4080              continue
                  pom=sqrt(pom/float(IndexedAll-3))
                  do k=1,3
                    SU(j,k)=pom*sqrt(AMinv(k,k))
                  enddo
                enddo
                call DRSCellFromSUB(U,SU,SCellRefBlock,Vol,SVol,G,SG)
                go to 2500
              endif
              StepBackPossible=.true.
              call multm(UStore,T,U,3,3,3)
              go to 2500
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 3000
            endif
4790        call FeQuestRemove(id)
            if(TakeUBFromFile.ne.0) go to 5100
4800      continue
4900    continue
5000  continue
      NInfo=2
      TextInfo(1)='The last triplet has been used. Now you can either'//
     1            ' make'
      TextInfo(2)='another selection or quit the indexation program.'
      if(FeYesNoHeader(-1.,-1.,'Do you want to try another selection?',
     1                 0)) then
        MameMatici=.true.
        go to 1300
      endif
5100  if(NStore.le.1) go to 9999
9999  call CloseIfOpened(ln)
      call DeleteFile(PreviousM50)
      if(allocated(Slozky))
     1  deallocate(Slozky,Int)
      if(allocated(ha))
     1  deallocate(ha,Uhel,Euler,Delka,FlagS,FlagI,FlagK)
      call FeTabsReset(UseTabs)
      call FeTabsReset(UseTabsM)
      call FeTabsReset(UseTabsC)
      UseTabs=UseTabsIn
      return
100   format(a16,3(a1,f9.4),3(a1,f8.3))
101   format(4i5,f10.1)
102   format(3f10.6,2i5)
103   format(i5,'/',i5)
104   format(4e21.11e4,i11,i6)
105   format(5f10.6)
      end
