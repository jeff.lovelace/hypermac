      subroutine MorShowVect(vect,popis,barva)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension vect(3)
      character*(*) popis
      integer barva
      call FeLineType(DashedLine)
      xu(1)=XCenAcWin
      yu(1)=YCenAcWin
      berle=.5*(XLenAcWin*.5-7.)/VecOrtLen(vect,3)
      xu(2)=vect(1)*berle+XCenAcWin
      yu(2)=vect(2)*berle+YCenAcWin
      call FePolyLine(2,xu,yu,barva)
      soupopx=vect(1)*berle*1.05+XCenAcWin
      soupopy=vect(2)*berle*1.05+YCenAcWin
      if(popis.ne.' ') call FeOutSt(0,soupopx,soupopy,popis,'C',barva)
      call FeLineType(NormalLine)
      return
      end
