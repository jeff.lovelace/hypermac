      subroutine SiemensCompress(ic,String)
      include 'fepc.cmn'
      character*(*) String
      character*42 low
      character*52 high
      data low/'0123456789`~!@#$%^&*()-_=+|\[{]};:''",<.>/?'/
      data high/'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'/
      ip=iabs(ic)
      i=mod(ip,42)+1
      String=low(i:i)
      ip=(ip-i+1)/42
1000  if(ip.le.0) return
      i=mod(ip,52)+1
      String=high(i:i)//String
      ip=(ip-i+1)/52
      go to 1000
      end
