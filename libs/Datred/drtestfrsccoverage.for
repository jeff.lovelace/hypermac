      subroutine DRTestFrScCoverage(n,m)
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer ihr(6),FlagMax,h
      integer, allocatable :: Flag(:),CovA(:,:),iha(:,:)
      logical EqIV0,EqIV
      character*256 :: Veta
      real, allocatable :: KdeJe(:)
      Veta=fln(:ifln)//'.eqv'
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call DRSetRunScGrN(ich)
      if(ich.ne.0) then
        ErrFlag=ich
        go to 9999
      endif
      if(allocated(CovA)) deallocate(CovA)
      allocate(CovA(RunScN(KRefBlock),RunScN(KRefBlock)))
      if(allocated(ScBrat)) deallocate(ScBrat)
      allocate(ScBrat(RunScN(KRefBlock),KRefBlock))
      ScBrat=.false.
      FlagMax=0
      NRef=0
      ihr=-999999
      do kk=1,2
        m=0
1100    read(ln,FormA,end=1300) Veta
        NRef=NRef+1
        if(kk.eq.2) then
          read(Veta,FormatEQV) iha(1:maxNDim,NRef),
     1      pom,pom,i,((pom,j=1,2),i=1,3),KdeJe(NRef)
          ikde=RunScGrN(ifix(KdeJe(NRef)),KRefBlock)
          ScBrat(ikde,KRefBlock)=.true.
          if(ScFrMethod(KRefBlock).eq.ScFrMethodInterpol)
     1      ScBrat(ikde+1,KRefBlock)=.true.
          Veta=' '
          if(EqIV0(iha(1,NRef),4)) then
            NRef=NRef-1
            go to 1100
          endif
          if(kk.eq.2) then
            ih=0
            ih(1:maxNDim)=iha(1:maxNDim,NRef)
          endif
          do i=1,NSymm(KPhase)
            call MultMIRI(iha(1,NRef),RM6(1,i,1,KPhase),ih,1,
     1                    NDim(KPhase),NDim(KPhase))
            if(EqIV(ih,ihr,maxNDim)) go to 1200
          enddo
          if(m.eq.1) then
            iha(1:maxNDim,NRef-1)=iha(1:maxNDim,NRef)
            NRef=NRef-1
            FlagMax=FlagMax-1
          endif
          m=0
          FlagMax=FlagMax+1
          ihr(1:maxNDim)=iha(1:maxNDim,NRef)
1200      Flag(NRef)=FlagMax
          m=m+1
        endif
        go to 1100
1300    if(kk.eq.1) then
          allocate(Flag(NRef),KdeJe(NRef),iha(6,NRef))
          rewind ln
          NRef=0
        endif
      enddo
      CovA=0
      nn=0
      do h=1,FlagMax
        ip=0
1400    nn=nn+1
        if(ip.eq.0) ip=nn
        if(Flag(nn).eq.h) then
          ik=nn
          if(nn.lt.NRef) go to 1400
        else
          nn=nn-1
          if(ik-ip+1.le.1) cycle
          do i=ip,ik
            ikde=RunScGrN(ifix(KdeJe(i)),KRefBlock)
            do j=ip,ik
              jkde=RunScGrN(ifix(KdeJe(j)),KRefBlock)
              CovA(ikde,jkde)=CovA(ikde,jkde)+1
              CovA(jkde,ikde)=CovA(jkde,ikde)+1
              if(ScFrMethod(KRefBlock).eq.ScFrMethodInterpol) then
                CovA(ikde+1,jkde)=CovA(ikde+1,jkde)+1
                CovA(jkde+1,ikde)=CovA(jkde+1,ikde)+1
                CovA(ikde,jkde+1)=CovA(ikde,jkde+1)+1
                CovA(jkde,ikde+1)=CovA(jkde,ikde+1)+1
                CovA(ikde+1,jkde+1)=CovA(ikde+1,jkde+1)+1
                CovA(jkde+1,ikde+1)=CovA(jkde+1,ikde+1)+1
              endif
            enddo
          enddo
        endif
      enddo
      do i=1,RunScN(KRefBlock)
        do j=1,RunScN(KRefBlock)
          if(CovA(i,j).gt.0) then
            do k=1,RunScN(KRefBlock)
              if(CovA(j,k).gt.0.and.CovA(i,k).eq.0) then
                CovA(i,k)=1
                CovA(k,i)=1
              endif
            enddo
          endif
        enddo
      enddo
      do i=1,RunScN(KRefBlock)
        do j=1,RunScN(KRefBlock)
          if(.not.ScBrat(i,KRefBlock).or..not.ScBrat(j,KRefBlock)) then
            CovA(i,j)=1
            CovA(j,i)=1
          endif
        enddo
      enddo
      m=0
      n=0
      do i=1,RunScN(KRefBlock)
        do j=1,RunScN(KRefBlock)
          m=m+1
          if(CovA(i,j).le.0) then
            n=n+1
          endif
        enddo
      enddo
9999  call CloseIfOpened(ln)
      if(allocated(CovA)) deallocate(CovA)
      if(allocated(Flag)) deallocate(Flag,KdeJe,iha)
      return
      end
