      subroutine SetRotMatAboutAxis(fi,Axis,R)
      include 'fepc.cmn'
      dimension R(3,3)
      integer Axis
      pom=fi*ToRad
      if(Axis.lt.0) pom=-pom
      sinf=sin(pom)
      cosf=cos(pom)
      call UnitMat(R,3)
      i=iabs(Axis)
      if(i.eq.1) then
        j=3
      else if(i.eq.2) then
        j=1
      else if(i.eq.3) then
        j=2
      endif
      i=6-j-i
      R(i,i)= cosf
      R(j,j)= cosf
      R(i,j)=-sinf
      R(j,i)= sinf
      return
      end
