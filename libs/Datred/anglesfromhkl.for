      subroutine AnglesFromHKL(h,psi,A,Rko,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),Rko(3,3),A(4),x(3)
      call multm(ub,h,x,3,3,1)
      call AnglesFromXYZ(x,psi,A,Rko,ich)
      return
      end
