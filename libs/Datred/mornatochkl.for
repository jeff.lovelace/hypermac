      subroutine MorNatochkl(Vect)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension Vect(3),PomMat(3,3)
      call CopyVek(Vect,PomMat(1,3),3)
      call VecOrtNorm(PomMat(1,3),3)
      if(abs(PomMat(2,3)).lt..99) then
        pom=1.
        do i=1,3
          if(i.eq.2) then
            PomMat(i,1)=0.
          else
            j=4-i
            PomMat(i,1)=pom*PomMat(j,3)
            pom=-1.
          endif
        enddo
      else
        PomMat(1,1)=1.
        PomMat(2,1)=0.
        PomMat(3,1)=0.
      endif
      call VecOrtNorm(PomMat(1,1),3)
      call VecMul(PomMat(1,3),PomMat(1,1),PomMat(1,2))
      call TrMat(PomMat,RotMat,3,3)
      call MorRotujKrystal
      return
      end
