      subroutine DRBasicRigakuCCD
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta
      logical ExistFile
      Veta='CrystalClear.cif'
1100  call FeFileManager('Select summary "cif" file',Veta,'*.cif',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      if(.not.ExistFile(Veta)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                Veta(:idel(Veta))//
     2                '" does not exist, try again.',' ',SeriousError)
        go to 1100
      endif
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)='(3i4,2f8.2,i4,6f8.5)'
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      call OpenFile(70,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      call DRReadBasicCIF(1)
9999  call CloseIfOpened(70)
      return
      end
