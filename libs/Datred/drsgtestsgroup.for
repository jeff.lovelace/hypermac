      subroutine DRSGTestSGroup(Key,Change,ich)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'powder.cmn'
      dimension TrPom(3,3),CellPom(6),NSumaAll(100),IPorSG(100),ih1(6),
     1          Ktera(100),hp(3),CellTrSel6Save(36),QuSymm(3,3),
     2          CellParSelSave(6),QuIrrSelSave(9),QuPom(3,3),
     3          QuRacSelSave(9)
      character*256 Radka,FileName
      character*80  SGText(100),Veta,Grp,TextOut,GrpList(:),GrpListP(:)
      character*40  itxt
      character*20  GrupaS
      character*8   GrupaR
      character*3   abc
      character*1   CentrChar,t1
      integer CrSystemFromCell,SbwItemPointerQuest
      logical SystExtRef,EqIgCase,Change,FeYesNoHeader,EqRV,EqIV0,
     1        UseOld,First,LastLetter1,Observed,ranover,LeBailFirst
      real MPom(36)
      allocatable GrpList,GrpListP
      save SGText,NSG,IPorSG,nSGSel,CentrChar,CellTrSel6Save,
     1     CellParSelSave,QuIrrSelSave,QuRacSelSave
      data abc/'abc'/
      ranover=.false.
      NAtCalcIn=NAtCalc
      NAtCalc=0
      if(Key.eq.1) then
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_spgroups.tmp','formatted',
     1                'unknown')
        UseOld=.true.
        call CopyMat(CellTrSel6Save,CellTrSel6,NDim(KPhase))
        call CopyVek(CellParSelSave,CellParSel,6)
        call CopyVek(QuIrrSelSave,QuIrrSel,3*NDimI(KPhase))
        call CopyVek(QuRacSelSave,QuRacSel,3*NDimI(KPhase))
        go to 3415
      endif
      Monoclinic(KPhase)=0
      call UnitMat(TrPom,3)
      RepeatSGTest=.false.
      if(NCellSel.eq.IdTriclinic) then
        CrSystem(KPhase)=CrSystemTriclinic
        if(CentrSel.eq.IdCentrP) go to 1900
        if(CentrSel.eq.IdCentrA) then
          TrPom(2,2)=.5
          TrPom(3,2)=.5
        else if(CentrSel.eq.IdCentrB) then
          TrPom(1,1)=.5
          TrPom(3,1)=.5
        else if(CentrSel.eq.IdCentrC) then
          TrPom(1,1)=.5
          TrPom(2,1)=.5
        else if(CentrSel.eq.IdCentrI) then
          TrPom(1,1)=.5
          TrPom(2,1)=.5
          TrPom(3,1)=.5
        else if(CentrSel.eq.IdCentrF) then
          TrPom(1,1)=.5
          TrPom(2,1)=.5
          TrPom(2,2)=.5
          TrPom(3,2)=.5
        else if(CentrSel.eq.IdCentrRObv) then
          TrPom(1,1)= .666667
          TrPom(2,1)= .333333
          TrPom(3,1)= .333333
          TrPom(1,2)=-.333333
          TrPom(2,2)= .333333
          TrPom(3,2)= .333333
          TrPom(1,3)=-.333333
          TrPom(2,3)=-.666667
          TrPom(3,3)= .333333
        else if(CentrSel.eq.IdCentrRRev) then
          TrPom(1,1)=-.666667
          TrPom(2,1)=-.333333
          TrPom(3,1)= .333333
          TrPom(1,2)= .333333
          TrPom(2,2)=-.333333
          TrPom(3,2)= .333333
          TrPom(1,3)= .333333
          TrPom(2,3)= .666667
        else if(CentrSel.eq.IdCentrX) then
          call DRXCell2PCell(TrPom,ichp)
        endif
        call MatInv(TrPom,QuRacSelSave,ddd,3)
        CentrSel=IdCentrP
        call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,CellTrSel6)
        call DRCellQReduction(CellParSel,QuIrrSel,QuRacSel,CellTrSel6)
        RepeatSGTest=.true.
      else if(NCellSel.eq.IdMonoclinicA.or.NCellSel.eq.IdMonoclinicB.or.
     1        NCellSel.eq.IdMonoclinicC) then
        MonoClinic(KPhase)=2
        CrSystem(KPhase)=CrSystemMonoclinic
        if(CentrSel.eq.IdCentrB) then
          call UnitMat(TrPom,3)
          TrPom(1,1)=0.5
          TrPom(3,1)=0.5
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          CentrSel=IdCentrP
          call DRCellQReduction(CellParSel,QuIrrSel,QuRacSel,CellTrSel6)
          i=iabs(CrSystemFromCell(CellParSel,DiffAxe,DiffAngle))/10
          if(i.eq.0.or.i.eq.3) then
            call SetPermutMat(TrPom,3,312,ich)
            call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                        CellTrSel6)
          else if(i.eq.1) then
            call SetPermutMat(TrPom,3,231,ich)
            call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                        CellTrSel6)
          endif
          RepeatSGTest=.true.
        else if(CentrSel.eq.IdCentrA) then
          call SetPermutMat(TrPom,3,321,ich)
          TrPom(2,2)=-1.
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          CentrSel=IdCentrC
        else if(CentrSel.eq.IdCentrI) then
          TrPom(1,1)=1.
          TrPom(3,1)=1.
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          CentrSel=IdCentrC
        else if(CentrSel.eq.IdCentrF) then
          call UnitMat(TrPom,3)
          TrPom(1,3)=0.5
          TrPom(3,3)=0.5
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          CentrSel=IdCentrC
          RepeatSGTest=.true.
        endif
        go to 1800
      else if(NCellSel.eq.IdOrthorhombic) then
        CrSystem(KPhase)=CrSystemOrthorhombic
      else if(NCellSel.eq.IdTetragonalA.or.NCellSel.eq.IdTetragonalB.or.
     1        NCellSel.eq.IdTetragonalC) then
        if(CentrSel.eq.IdCentrI) then
          go to 1900
        else if(CentrSel.eq.IdCentrF.or.CentrSel.eq.IdCentrC) then
          call UnitMat(TrPom,3)
          TrPom(1,1)= .5
          TrPom(2,1)=-.5
          TrPom(1,2)= .5
          TrPom(2,2)= .5
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          if(CentrSel.eq.IdCentrF) then
            CentrSel=IdCentrI
          else
            CentrSel=IdCentrP
          endif
          RepeatSGTest=.true.
        else if(CentrSel.ne.IdCentrX) then
          CentrSel=IdCentrP
        endif
      else if(NCellSel.eq.IdTrigonal.or.
     1        NCellSel.eq.IdRhombicAmBmC.or.
     2        NCellSel.eq.IdRhombicmABmC.or.
     3        NCellSel.eq.IdRhombicmAmBC.or.
     4        NCellSel.eq.IdRhombicABC) then
        if(CentrSel.eq.IdCentrRObv) then
          go to 1900
        else if(CentrSel.eq.IdCentrRRev) then
          call UnitMat(TrPom,3)
          TrPom(1,1)=-1.
          TrPom(2,2)=-1.
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
          CentrSel=IdCentrRObv
          Change=.true.
        endif
      else if(NCellSel.eq.IdHexagonal) then
        CrSystem(KPhase)=CrSystemHexagonal
      else if(NCellSel.eq.IdCubic) then
        CrSystem(KPhase)=CrSystemCubic
      endif
      go to 1900
1800  call UnitMat(TrPom,3)
      AngleBest=9999.
      do j=-3,3
        TrPom(3,1)=j
        if(CentrSel.eq.IdCentrC.and.mod(iabs(j),2).eq.1) cycle
        do i=-3,3
          if(i*j.ne.0) cycle
          TrPom(1,3)=i
          call CopyVek(CellParSel,CellPom,6)
          call UnitMat(MPom,NDim(KPhase))
          call DRMatTrCell(TrPom,CellPom,MPom)
          pom=abs(CellPom(5)-90.)
          if(pom.lt.AngleBest) then
            ibest=i
            jbest=j
            AngleBest=pom
          endif
        enddo
      enddo
      TrPom(3,1)=jbest
      TrPom(1,3)=ibest
      call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,CellTrSel6)
1900  if(NCellSel.eq.IdMonoclinicA.or.NCellSel.eq.IdMonoclinicB.or.
     1   NCellSel.eq.IdMonoclinicC) then
        if(CellParSel(5).lt.90.) then
          call UnitMat(TrPom,3)
          TrPom(2,2)=-1.
          TrPom(3,3)=-1.
          call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                      CellTrSel6)
        endif
      endif
      ic=CentrSel
      if(ic.gt.IdCentrRRev) ic=ic-1
      CentrChar=smbc(ic:ic)
      call DRSGTestQPositive(TrPom)
      if(NDimI(KPhase).gt.0.or.isPowder) then
        call CopyVek(CellParSel,CellPar(1,1,KPhase),6)
        call CopyVek(QuIrrSel,Qu(1,1,1,KPhase),NDimI(KPhase)*3)
        if(NDimI(KPhase).eq.1) then
          call CrlGetSmbQ4dFromQ(QuIrrSel,QuRacSel,SmbQVSel,i)
          call AddVek(Qu(1,1,1,KPhase),QuRacSel,Qu(1,1,1,KPhase),3)
        endif
        call SetMet(0)
      endif
      if(LaueGroupSel.gt.1) then
        IPgFirst=LaueGroupPointer(LaueGroupSel-1)+1
        IPgFirst=SmbPGN(IPgFirst)
      else
        IPgFirst=1
      endif
      IPgLast=LaueGroupPointer(LaueGroupSel)
      i=idel(SmbPGI(IPgLast))
      LastLetter1=SmbPGI(IPgLast)(i:i).eq.'1'
      IPgLast=SmbPGN(IPgLast)
      UseOld=EqRV(CellParSel(1),CellParSelSGOld(1),3,DiffAxe  ).and.
     1       EqRV(CellParSel(4),CellParSelSGOld(4),3,DiffAngle).and.
     2       EqRV(CellTrSel6,CellTrSel6SGOld,NDimQ(KPhase),.001).and.
     3       (LaueGroupSel.eq.LaueGroupSelOld).and.
     4       (CentrSel.eq.CentrSelOld).and.
     5       abs(SumaObsLimExtinct-SumaObsLimExtinctOld).lt..001
      if(UseOld) then
        lno=NextLogicNumber()
        call OpenFile(lno,fln(:ifln)//'_spgroups.tmp','formatted',
     1                'unknown')
        go to 3415
      else
        GrupaSelOld=' '
      endif
      lni=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1      'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_spgroups.tmp','formatted',
     1              'unknown')
      read(lni,FormA) Radka
      if(Radka(1:1).ne.'#') rewind lni
      First=.true.
      m=0
      NSG=0
      if(isPowder) then
        SumaObsLim=100.
        SumaObsLimCenter=100.
        SumaObsLimExtinct=100.
        LeBailFirst=.true.
      endif
2000  read(lni,FormSG,end=2010) ig,ipg,idl,GrupaR,itxt,GrupaS
      if(ipg.lt.IPgFirst.or.ipg.gt.IPgLast) go to 2000
      if(ig.eq.39.or.ig.eq.41.or.ig.eq.64.or.ig.eq.67.or.ig.eq.68) then
        Cislo=GrupaR
        call mala(Cislo)
        if(index(Cislo,'e').le.0) go to 2000
      endif
      j=idel(GrupaR)
      if(ipg.ge.18.and.ipg.le.20.and.GrupaR(1:1).ne.'R') then
        j=idel(GrupaR)
        if((     LastLetter1.and.GrupaR(j:j).ne.'1').or.
     1     (.not.LastLetter1.and.GrupaR(j:j).eq.'1')) go to 2000
      endif
      if(First) then
        n=0
        First=.false.
      endif
      n=n+1
      if(ipg.ge.3.and.ipg.le.5.and.mod(n,3).ne.2) go to 2000
      if(EqIgCase(CentrChar,GrupaR(1:1)).or.
     1   (CentrChar.eq.'X'.and.EqIgCase(GrupaR(1:1),'P'))) then
        GrupaR(1:1)=CentrChar
        m=m+1
        write(lno,FormSG) ig,ipg,idl,GrupaR,itxt,GrupaS
      endif
      go to 2000
2010  close(lni)
      call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
!      if(.not.isPowder) then
      SumaObsLim=SumaObsLimExtinct
      SumaObsMin=99999.
!      endif
      m=min(m+2,30)
2100  rewind lno
      TextOut='Space group tested: ............'
      call FeTxOut(-1.,-1.,TextOut)
      if(allocated(GrpList)) deallocate(GrpList)
      nalloc=100
      allocate(GrpList(nalloc))
      nGrpList=0
      ntst=0
      lnt=NextLogicNumber()
      call OpenFile(lnt,fln(:ifln)//'.sgt','formatted','unknown')
2200  read(lno,FormSG,end=3400) ig,ipg,idl,Grupa(KPhase),itxt,GrupaS
      idlr=idel(Grupa(KPhase))
      if(NDimI(KPhase).eq.1) then
        Grupa(KPhase)=Grupa(KPhase)(:idel(Grupa(KPhase)))//
     1                SmbQVSel(:idel(SmbQVSel))
        call DRGenSymFor4D(ipg,ich)
        if(ich.ne.0) go to 2200
      endif
      call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
      NSymmN(KPhase)=NSymm(KPhase)
      if(CentrChar.eq.'X') then
        call FindSmbSgOrgShiftNo(Grp,ChangeOrderNo,1)
        idl=index(Grp,'(')
        if(idl.le.0) then
          idl=idel(Grp)
        else
          idl=idl-1
        endif
        if(.not.EqIgCase(Grupa(KPhase)(:idlr),Grp(:idl))) then
          ich=-1
          go to 2230
        endif
        nGrpList=nGrpList+1
        if(nGrpList.gt.nalloc) then
          allocate(GrpListP(nalloc))
          do i=1,nalloc
            GrpListP(i)=GrpList(i)
          enddo
          nallocp=nalloc
          nalloc=nalloc+100
          deallocate(GrpList)
          allocate(GrpList(nalloc))
          do i=1,nallocp
            GrpList(i)=GrpListP(i)
          enddo
          deallocate(GrpListP)
        endif
        GrpList(nGrpList)=Grp(:idl)
        Grupa(KPhase)=Grp
      endif
2230  GrupaR=Grupa(KPhase)
      if(NDimI(KPhase).eq.1) then
        i=index(GrupaR,'(')
        if(i.gt.0) GrupaR(i:)=' '
      endif
      if(ich.eq.0) then
        Radka=GrupaR(:idel(GrupaR))//Tabulator//'|'
        TextOut(21:)=GrupaR
        call FeTxOutCont(TextOut)
        if(isPowder) then
          if(LeBailFirst) then
            ntst=ntst+1
            call PwdLeBail(0,.false.)
            ln=NextLogicNumber()
            Veta=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
            call OpenFile(ln,Veta,'formatted','old')
            if(ErrFlag.ne.0) then
              ich=1
              go to 9000
            endif
            if(allocated(ihar)) deallocate(ihar,FlgAr,KPhAr,MltAr)
            NRef=NRef91(KDatBlock)
            allocate(ihar(6,NRef),flgar(NRef),KPhAr(NRef),
     1               MltAr(NRef))
            do j=1,NRef
              read(ln,format91pow,end=2320,err=2320)(ihar(k,j),k=1,6),
     1                                           pom,pom,k,k,KPhAr(j)
              MltAr(j)=pom
              FlgAr(j)=1
            enddo
            call CloseIfOpened(ln)
            LeBailFirst=.false.
            NVse1=NRef-1
            NExt1=0
            go to 2400
2320        ich=1
            call CloseIfOpened(ln)
            go to 9000
          else
            NVse1=0
            NExt1=0
            do i=1,NRef
              if(ihar(1,i).gt.900) then
                FlgAr(i)=1
                cycle
              endif
              if(.not.EqIV0(ihar(4,i),NDimI(KPhase))) cycle
              NVse1=NVse1+1
              if(SystExtRef(ihar(1,i),.false.,1)) then
                FlgAr(i)=0
                NExt1=NExt1+1
              else
                FlgAr(i)=1
              endif
            enddo
            call PwdLeBailFromHKL(0,.false.,IHAr,FlgAr,KPhAr,MltAr,
     1                            NRef)
          endif
2400      write(Veta,101) LeBailRFObs,LeBailRF
          call Zhusti(Veta)
          pom=float(NExt1)/float(NVse1)
          write(Cislo,'(f10.4)') pom
          call Zhusti(Cislo)
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))//
     1                               Tabulator//'|'//
     2                               Tabulator//Cislo(:idel(Cislo))//
     3                               Tabulator//'|'
          SumaObs=LeBailRFObs
          SumaAll=LeBailRF
          SumaFOM=SumaAll*(1.-.5*pom)
          write(Cislo,'(f10.4)') SumaFOM
          call Zhusti(Cislo)
          Radka=Radka(:idel(Radka))//Tabulator//Cislo(:idel(Cislo))//
     1                               Tabulator//'|'
        else
          ntst=ntst+1
          NAll=0
          NObs=0
          SumaAll=0.
          SumaObs=0.
          call DRWriteUsedSymm(lnt,ntst)
          do 2500i=1,NRefRead
            do ntw=1,NTwin
              if(HCondAr(ntw,i).lt.0) then
                if(ntw.eq.1) then
                  go to 2500
                else
                  cycle
                endif
              endif
              call IndUnPack(HCondAr(ntw,i),ihp,HCondLn,HCondMx,
     1                       NDim(KPhase))
              call indtr(ihp,CellTrSel6,ih,NDim(KPhase))
              if(ntw.eq.1) then
                do k=4,NDim(KPhase)
                  if(ih(k).ne.0) go to 2500
                enddo
                call CopyVekI(ih,ih1,NDim(KPhase))
              endif
              if(.not.SystExtRef(ih,.false.,1)) go to 2500
            enddo
            if(rsar(i).gt.0.) then
              RelI=max(riar(i)/rsar(i),0.)
            else
              RelI=0.
            endif
            Observed=RelI.gt.3.
            NAll=NAll+1
            SumaAll=SumaAll+RelI
            if(Observed) then
              NObs=NObs+1
              SumaObs=SumaObs+RelI
              write(lnt,FormExtRefl)
     1          ntst,(ih1(k),k=1,NDim(KPhase)),riar(i),rsar(i)
            endif
2500      continue
          if(NAll.gt.0) then
            SumaAll=SumaAll/float(NAll)
          else
            SumaAll=0.
          endif
          if(NObs.gt.0) then
            SumaObs=SumaObs/float(NObs)
          else
            SumaObs=0.
          endif
          write(Veta,100) NObs,NAll
          call Zhusti(Veta)
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))//
     1                               Tabulator//'|'
          write(Veta,101) SumaObs,SumaAll
          call Zhusti(Veta)
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))//
     1                               Tabulator//'|'
          if(NObs.le.5) then
            NObsP=0
          else
            NObsP=NObs
          endif
          SumaFOM=(SumaObs**3*float(NObsP)+SumaAll**3*float(NAll-NObsP))
     1           /float(NAll**2+1)+1./float(NAll+1)
          write(Veta,'(f10.5)') SumaFOM
          Radka=Radka(:idel(Radka))//Tabulator//Veta(:idel(Veta))//
     1                               Tabulator//'|'
          SumaObsMin=min(SumaObsMin,SumaObs)
        endif
        if(SumaObs.lt.SumaObsLim) then
          if(NSG.lt.100) then
            NSG=NSG+1
            i=NSG
          else
            i=IPorSG(100)
          endif
          NSumaAll(i)=nint(SumaFOM*100000.)
          SGText(i)=Radka
          Ktera(i)=ntst
          if(NSG.ge.100) call indexx(100,NSumaAll,IPorSG)
        endif
      endif
      go to 2200
3400  call ResetIgnoreW
      call ResetIgnoreE
      call CloseIfOpened(lnt)
      call FeTxOutEnd
      if(NSG.le.0) then
        if(SumaObsMin.lt.90000.) then
          NInfo=1
          TextInfo(1)='No space group achieved better figure of merit.'
          if(FeYesNoHeader(-1.,-1.,'Do you want to use a higher limit?',
     1                     0)) then
            SumaObsLim=max(anint(SumaObsMin*1.05),1.)
            go to 2100
          else
            ich=1
            go to 9000
          endif
        else
          if(ntst.le.0) then
            Cislo='groups'
            if(NDimI(KPhase).eq.1) Cislo='super'//Cislo(:idel(Cislo))
            call FeChybne(-1.,-1.,'All '//Cislo(:idel(Cislo))//
     1                    ' contradict to selected centering ',
     2                    'and Laue group. Try another choice.',
     3                    Warning)
            ich=1
          else if(isPowder) then
            call FeChybne(-1.,-1.,'the procedure failed due to bad '//
     1        'profile fit.',' ',Warning)
            ich=-1
          else
            call FeUnforeseenError(' ')
            ich=-1
          endif
          go to 9000
        endif
      endif
      if(NSG.lt.100) call indexx(NSG,NSumaAll,IPorSG)
      call CopyVek(CellParSel,CellParSelSGOld,6)
      call CopyVek(CellTrSel6,CellTrSel6SGOld,NDimQ(KPhase))
      LaueGroupSelOld=LaueGroupSel
      CentrSelOld=CentrSel
      SumaObsLimExtinctOld=SumaObsLimExtinct
3415  call CopyMat(CellTrSel6,CellTrSel6Save,NDim(KPhase))
      call CopyVek(CellParSel,CellParSelSave,6)
      call CopyVek(QuIrrSel,QuIrrSelSave,3*NDimI(KPhase))
      call CopyVek(QuRacSel,QuRacSelSave,3*NDimI(KPhase))
      id=NextQuestId()
      call FeQuestTitleMake(id,'Select space group')
      il=1
      xpom=5.
      call FeQuestLblMake(id,xpom,il,'Space group','L','N')
      if(isPowder) then
        xsh=90.
        Veta='Rp(obs)/Rp(all)'
      else
        xsh=110.
        Veta='obs/all'
      endif
      xpom=xpom+xsh
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      if(isPowder) then
        xsh=90.
        Veta='N(Extinct)/N(Gener)'
      else
        xsh=80.
        Veta='ave(I/sig(I))'
      endif
      xpom=xpom+xsh
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      xsh=100.
      if(isPowder) xsh=xsh+28.
      Veta='FOM'
      xpom=xpom+xsh
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      call FeTabsReset(6)
      pom=76.
      call FeTabsAdd(pom,6,IdRightTab,' ')
      pom=pom+44.
      call FeTabsAdd(pom,6,IdCharTab,'/')
      pom=pom+44.
      call FeTabsAdd(pom,6,IdRightTab,' ')
      if(isPowder) then
        t1='.'
      else
        t1='/'
      endif
      pom=pom+44.
      call FeTabsAdd(pom,6,IdCharTab,t1)
      pom=pom+48.
      if(isPowder) pom=pom+20.
      call FeTabsAdd(pom,6,IdRightTab,' ')
      pom=pom+24.
      if(isPowder) pom=pom+5.
      call FeTabsAdd(pom,6,IdCharTab,'.')
      pom=pom+50.
      call FeTabsAdd(pom,6,IdRightTab,' ')
      if(UseOld) go to 3445
      i=1
3420  if(i.ge.NSG) then
        nSGSel=1
        go to 3445
      endif
      n=0
      ip=i
      NSuma=NSumaAll(IPorSG(i))
3430  i=i+1
      n=n+1
      j=IPorSG(i)
3435  if(NSumaAll(j).eq.NSuma) then
        if(i.lt.NSG) go to 3430
      endif
      if(n.gt.1) then
        call IntVectorToOpposite(IPorSG(ip),IPorSG(ip),n)
        call HeapI(n,IPorSG(ip),1)
        call IntVectorToOpposite(IPorSG(ip),IPorSG(ip),n)
      endif
      go to 3420
3445  ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_spgroup.tmp','formatted','unknown')
      do i=1,NSG
        j=IPorSG(i)
        write(ln,FormA) SGText(j)(:idel(SGText(j)))
      enddo
      call CloseIfOpened(ln)
      dpom=WizardLength-10.-SbwPruhXd
      if(isPowder) then
        il=16
      else
        il=15
      endif
      UseTabs=6
      xpom=5.
      call FeQuestSbwMake(id,xpom,il-1,dpom,il-2,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbw=SbwLastMade
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_spgroup.tmp')
      call FeQuestSbwItemOff(nSbw,SbwItemPointerQuest(nSbw))
      call FeQuestSbwItemOn(nSbw,NSupCellSel)
      call FeQuestSbwItemOff(nSbw,SbwItemPointerQuest(nSbw))
      call FeQuestSbwItemOn(nSbw,nSGSel)
      dpom=85.
      tpom=(WizardLength-dpom)*.5
      if(.not.isPowder) then
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,'Details')
        nButtDetails=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      endif
3500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDetails) then
        nSGSel=SbwItemPointerQuest(nSbw)
        i=IPorSG(nSGSel)
        n=Ktera(i)
        k=0
        call kus(SGText(i),k,GrupaSel)
        FileName=fln(:ifln)//'_tmp.sgt'
        call OpenFile(lst,FileName,'formatted','unknown')
        LstOpened=.true.
        Uloha='List of strongest reflections contradicting '//
     1        'the space group '//GrupaSel(:idel(GrupaSel))
        call NewPg(1)
        if(NDimI(KPhase).eq.1) then
          call AddVek(QuIrrSel,QuRacSel,QuPom,3)
        else
          call CopyVek(QuIrrSel,QuPom,3*NDimI(KPhase))
        endif
        call DRWriteUsedPar(CellParSel,QuPom,CellTrSel6)
        lni=NextLogicNumber()
        call OpenFile(lni,fln(:ifln)//'.sgt','formatted','unknown')
3510    read(lni,FormA,end=3515) Veta
        if(LocateSubstring(Veta,'symm',.false.,.true.).le.0) go to 3510
        read(Veta,'(i4)') j
        if(j.lt.n) then
          go to 3510
        else if(j.eq.n) then
          call NewLn(1)
          write(lst,FormA) Veta(14:idel(Veta))
          go to 3510
        endif
3515    rewind lni
        Veta=Uloha
        if(NSumaAll(i).le.0) Veta=Veta(:idel(Veta))//' is empty'
        call TitulekVRamecku(Veta)
        if(NSumaAll(i).gt.0) then
          NExt(1)=0
          NExtObs(1)=0
          NExt500(1)=0
          RIExtMax(1)=0.
          SumRelI(1)=0.
3520      read(lni,FormA,end=3530) Veta
          if(LocateSubstring(Veta,'symm',.false.,.true.).gt.0)
     1      go to 3520
          read(Veta,FormExtRefl) j,(ih(i),i=1,NDim(KPhase)),
     1                           fpom,spom
          if(j.eq.n)
     1      call EM9NejdouPotvory(1,ih,hp,fpom,spom,NDim(KPhase),
     2                            .false.,3.)
          go to 3520
3530      call DRDejSpravnePotvory
        else
          call TitulekVRamecku(Uloha(:idel(Uloha))//' is empty')
        endif
        call CloseListing
        call FeListView(FileName,0)
        call DeleteFile(FileName)
        call CloseIfOpened(lni)
        go to 3500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3500
      endif
      if(ich.ge.0) then
        nSGSel=SbwItemPointerQuest(nSbw)
        k=0
        call kus(SGText(IPorSG(nSGSel)),k,GrupaSel)
        if(.not.isPowder) then
          rewind lno
3570      read(lno,FormSG,end=3700) ig,ipg,idl,GrupaR,itxt,GrupaS
          if(.not.EqIgCase(GrupaR,GrupaSel)) go to 3570
          idl=index(GrupaS,'#')
          if(idl.le.0) go to 3700
          call SetRealArrayTo(TrPom,9,0.)
          i=0
          j=idl
          im=idel(GrupaS)
3580      izn=1
          i=i+1
          if(i.gt.3) then
            if(j.eq.im) then
              rewind lno
              go to 3590
            else
              go to 3700
            endif
          endif
3585      j=j+1
          if(j.gt.im) go to 3700
          k=index(abc,GrupaS(j:j))
          if(k.ge.1) then
            TrPom(i,k)=izn
            go to 3580
          else if(GrupaS(j:j).eq.'-') then
            izn=-1.
          else if(GrupaS(j:j).ne.',') then
            go to 3700
          endif
          go to 3585
3590      lni=NextLogicNumber()
          if(OpSystem.le.0) then
            Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1          'spgroup.dat'
          else
            Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
          endif
          call OpenFile(lni,Veta,'formatted','old')
          read(lni,FormA) Radka
          if(Radka(1:1).ne.'#') rewind lni
          if(GrupaSel(1:1).eq.'X') GrupaSel(1:1)='P'
3600      read(lni,FormSG) igp,ipg,idl,GrupaR,itxt,GrupaS
          if(ig.eq.39.or.ig.eq.41.or.ig.eq.64.or.ig.eq.67.or.ig.eq.68)
     1      then
            Cislo=GrupaR
            call mala(Cislo)
            if(index(Cislo,'e').le.0) go to 3600
          endif
          if(igp.ne.ig) go to 3600
          close(lni)
          if(.not.EqIgCase(GrupaR,GrupaSel)) then
            call DRMatTrCellQ(TrPom,CellParSel,QuIrrSel,QuRacSel,
     1                        CellTrSel6)
            GrupaSel=GrupaR
            call DRSGTestQPositive(TrPom)
          endif
          if(CentrChar.eq.'X') GrupaSel(1:1)='X'
        endif
      else
        go to 9000
      endif
3700  call CopyVek(QuIrrSel,Qu(1,1,1,KPhase),NDimI(KPhase)*3)
      if(NDimI(KPhase).eq.1) then
        call CrlGetSmbQ4dFromQ(QuIrrSel,QuRacSel,SmbQVSel,i)
        call AddVek(Qu(1,1,1,KPhase),QuRacSel,Qu(1,1,1,KPhase),3)
        GrupaSel=GrupaSel(:idel(GrupaSel))//SmbQVSel(:idel(SmbQVSel))
      endif
      iGSel=ig
      iPGSel=ipg
      CrSystemSel=CrSystem(KPhase)
      if(Monoclinic(KPhase).ne.0)
     1  CrSystemSel=Monoclinic(KPhase)*10+mod(CrSystemSel,10)
      if(ich.ne.0) then
        call CopyVek(CellParSelOld,CellParSel,6)
        call CopyVek(CellTrSel6Old,CellTrSel6,NDimQ(KPhase))
      endif
9000  call CloseIfOpened(lno)
      if(allocated(GrpList)) deallocate(GrpList)
      NAtCalc=NAtCalcIn
      return
100   format(i15,'/'i15)
101   format(f10.3,'/',f10.3)
      end
