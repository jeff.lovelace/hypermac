      subroutine Extracts2HKL
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(6)
      character*256 :: Veta,FileNameIn=' ',FileNameOut,EdwStringQuest,
     1                 CurrentDirO
      character*80 t80
      integer FeChdir
      logical EqIgCase
      call FeGetCurrentDir
      CurrentDirO=CurrentDir
      id=NextQuestId()
      Veta='Specify the input "Extracts" file'
      il=1
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      bpom=50.
      xpom=5.
      tpom=5.
      dpom=xqd-bpom-20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNameIn)
      Veta='%Browse'
      xpom=tpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        FileNameIn=EdwStringQuest(nEdwName)
        call FeFileManager('Select input file:',FileNameIn,'*',0,
     1                     .true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,FileNameIn)
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      if(ich.eq.0) then
        FileNameIn=EdwStringQuest(nEdwName)
        call ExtractDirectory(FileNameIn,Veta)
        i=FeChdir(Veta)
        call ExtractFileName(FileNameIn,Veta)
        FileNameIn=Veta
        call GetPureFileName(FileNameIn,FileNameOut)
        FileNameOut=FileNameOut(:idel(FileNameOut))//'.hkl'
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      lni=NextLogicNumber()
      call OpenFile(lni,FileNameIn,'formatted','unknown')
      lno=NextLogicNumber()
      call OpenFile(lno,FileNameOut,'formatted','unknown')
1500  read(lni,FormA,end=3000) Veta
      if(Veta(1:1).eq.';') go to 1500
      do i=1,idel(Veta)
        if(Veta(i:i).eq.Tabulator) Veta(i:i)=' '
      enddo
      if(Veta.eq.' ') go to 1500
      k=0
      do i=1,3
        call kus(Veta,k,t80)
        if(EqIgCase(t80,'nan')) go to 1500
        call Posun(t80,0)
        read(t80,*,err=1500) ih(i)
      enddo
      call kus(Veta,k,t80)
      if(EqIgCase(t80,'nan')) go to 1500
      call Posun(t80,1)
      read(t80,*,err=1500) FInt
      call kus(Veta,k,t80)
      if(EqIgCase(t80,'nan')) go to 1500
      call Posun(t80,1)
      read(t80,*,err=1500) SFInt
      write(lno,FormatSHELX)(ih(i),i=1,3),FInt*.01,SFInt*.01
      go to 1500
3000  close(lno)
      close(lni)
      i=FeChdir(CurrentDirO)
9999  return
      end
