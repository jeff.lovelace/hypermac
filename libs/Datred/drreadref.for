      subroutine DRReadRef(ich)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      include 'profil.cmn'
      dimension uhlyk(4),c(3),h(6),ia(4),xp(3),fdir(3),frec(3),qdir(3),
     1          qrec(3),fqdir(3),fqrec(3),rot(3,3),rt(3,3),tt(3,3)
!     2          RunMax(1111),RunMin(1111)
      integer HRead(6)
      character*256 t256,t256opt,Radka
      character*6   priznaky
      character*3   Flag
      logical eqiv,PrijdeVen,EqIV0,JesteNe,EqIgCase
      real Longitude,Latitude
      data attenhw/12.74/,JesteNe/.true./
      data ScanSpeed,BckgFactor/2*-9999./
      ich=0
      Konec=0
      call SetIntArrayTo(ih,6,0)
      call SetRealArrayTo(h,3,0.)
      DRLam=0.
      RunCCD=1
      CorrAbsAppl=1.
      CorrScaleAppl=1.
      KPhaseIn=KPhase
1000  if(DifCode(KRefBlock).eq.IdCAD4) then
        call DRGetCADRecord(Radka,1,0,*9920,*9930)
        i=index(Radka,'.')
        if(i.eq.0.or.i.ge.27) then
          read(Radka,'(4x,i6,3i5,1x,a6,f7.2,f4.0,f6.0,f7.0,f6.0)',
     1      err=9930) no,(ih(i),i=1,3),priznaky,pom,rych,b1,p,b2
          call CopyVekI(ih,HRead,NDim95(KRefBlock))
        else
          read(Radka,'(3x,i5,3f6.2)',err=9930) no,h
          read(Radka(27:),'(a6,f7.2,f4.0,f6.0,f7.0,f6.0)',err=9930)
     1      priznaky,pom,rych,b1,p,b2
          do i=1,3
            ih(i)=nint(h(i))
          enddo
        endif
        call DRGetCADRecord(Radka,2,0,*9920,*9930)
        read(Radka,'(10x,f8.3,3f9.3,7x,f7.0)',err=9930) uhlyk,expos
        if(expos0.lt.0.) expos0=expos
        expos=expos-expos0
        if(priznaky(1:1).eq.'I') then
          no=-no
        else
          no= no
        endif
        if(priznaky(2:2).eq.'C'.or.priznaky(2:2).eq.'X') go to 1000
        if(priznaky(4:4).eq.'T') go to 1000
        rych=1./rych
        if(rych.lt.0.) rych=attfac*rych
        snkp=sin(uhlyk(4)*ToRad/2.)
        cskp=cos(uhlyk(4)*ToRad/2.)
        chi=2.*atan2(snalfa*snkp,sqrt(csalfaq+snalfaq*cskp**2))/ToRad
        call uprav(chi)
        theta=uhlyk(1)
        call uprav(theta)
        delta=atan2(csalfa*snkp,cskp)/ToRad
        fi=uhlyk(2)+delta
        call uprav(fi)
        omega=uhlyk(3)+delta
        call uprav(omega)
        if(maxNDim.gt.3) then
          eps=omega-theta
          sinc=sin(chi*ToRad)
          cosc=cos(chi*ToRad)
          sine=sin(eps*ToRad)
          cose=cos(eps*ToRad)
          chib=atan2(cose*sinc,sign(sqrt((sinc*sine)**2+cosc**2),cose))
          coscb=cos(chib)
          sincb=sin(chib)
          if(coscb.ne.0.) then
            fib=fi*ToRad-atan2(-sine/coscb,cose*cosc/coscb)
          else
            fib=fi*ToRad
          endif
          cosfb=cos(fib)
          sinfb=sin(fib)
          pom=2.*sin(theta*ToRad)/LamAveRefBlock(KRefBlock)
          c(1)=-sinfb*coscb*pom
          c(2)= cosfb*coscb*pom
          c(3)= sincb*pom
          call MultM(ubi(1,1,KRefBlock),c,h,3,3,1)
          call DRReadRefTrRealInd(h,ich)
          if(ich.ne.0) go to 9900
        else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        endif
        if(Profil) then
          j=1
          do i=3,10
            if(CAD4ProfFlag.eq.1) then
              call DRGetCADRecord(Radka,i,0,*1190,*9930)
              kp=11
            else
              read(71,FormA) Radka
              kp=1
            endif
            do k=kp,idel(Radka)
              if(Radka(k:k).eq.'*') Radka(k:k)='9'
            enddo
            read(Radka(kp:),CAD4ProfFormat,err=9930)
     1        (iprof(k,1),k=j,j+11)
            j=j+12
          enddo
          if(NonEqCheck.eq.0) then
            nprof=96
            nbckg=16
          else
            nprof=48
            nbckg=8
            j=1
            do i=1,48
              iprof(i,1)=iprof(j,1)+iprof(j+1,1)
              j=j+2
            enddo
          endif
          KProf=1
        endif
1190    call DRGetCADRecord(Radka,19,0,*1200,*9930)
        go to 1210
1200    Radka=' '
        Konec=1
1210    if(Radka.eq.' '.or.BerBPB) then
          rych=abs(rych)
          ri=(p-2.*(b1+b2))*rych
          rs=sqrt((p+4.*(b1+b2)))*rych
          if(rs.le.0.) rs=3.
        else
          read(Radka,'(29x,f8.1,f6.1)',err=9930) ri,rs
          if(rych.lt.0.) then
            ri=ri*attfac
            rs=rs*attfac
          endif
          if(rs.le.0.) rs=3.
        endif
        expnew=expos/3600.+addtime
        if(expnew.lt.expold) then
          expos=expold+1./3600.
          addtime=expos-expnew
        else
          expos=expnew
        endif
        expold=expos
        if(Profil) then
          tmn=theta
          tmx=theta
          dt=(doma+domb*tan(theta*ToRad))*.75
          omn=theta-dt
          omx=theta+dt
        endif
      else if(DifCode(KRefBlock).eq.IdSiemensP4) then
        read(71,FormA,end=9920,err=9930) t256
        if(t256(1:1).ne.' ') go to 1000
        read(t256,'(i7,3i4,4f8.2,f5.2,f8.2,i6,i12,i6,g15.1,g8.1,f8.2)'
     1      ,end=9920,err=9930)
     2      no,(ih(i),i=1,NDim95(KRefBlock)),theta,omega,fi,chi,delta,
     3      rych,i,i,i,ri,rs,expos
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        if(maxNDim.gt.3) then
          do i=1,3
            h(i)=ih(i)
          enddo
          call DRReadRefTrRealInd(h,ich)
          if(ich.ne.0) go to 9900
        else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        endif
        theta=theta/2.
        delta=delta*.5
        tmn=theta-delta
        tmx=theta+delta
        omn=omega-delta
        omx=omega+delta
        if(no.eq.0) go to 1000
        nprof=0
1510    read(71,FormA,end=1512,err=9930) t256
        go to 1515
1512    t256=' '
1515    if(t256(1:1).eq.' ') then
          nbckg=nprof/6
          backspace 71
        else
          idl=idel(t256)
          k=1
1520      nprof=nprof+1
          call SiemensUncompress(t256,k,iprof(nprof,1))
          py(nprof,1)=iprof(nprof,1)
          if(k.le.idl) go to 1520
          go to 1510
        endif
      else if(DifCode(KRefBlock).eq.IdIPDSStoe.or.
     1        DifCode(KRefBlock).eq.IdNoniusCCD.or.
     2        DifCode(KRefBlock).eq.IdBrukerCCD.or.
     3        DifCode(KRefBlock).eq.IdBrukerCCDRaw.or.
     4        DifCode(KRefBlock).eq.IdRigakuCCD.or.
     5        DifCode(KRefBlock).eq.IdImportSHELXI.or.
     6        DifCode(KRefBlock).eq.IdSHELXINoAbsCorr) then
        ntw=0
        itw=999999
1700    read(71,FormA,end=9920,err=9930) t256
        if(t256.eq.' ') go to 1700
        read(t256,FormatRefBlock(KRefBlock),err=9930)
     1    (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2    ((dircos(i,j),j=1,2),i=1,3)
        if(EqIV0(ih,NDim95(KRefBlock))) go to 9910
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        call RealVectorToOpposite(dircos(1,1),dircos(1,1),3)
        if(UseTrRefBlock(KRefBlock)) then
          do i=1,2
            call MultM(TrDirCos(1,KRefBlock),DirCos(1,i),xp,3,3,1)
            call CopyVek(xp,DirCos(1,i),3)
          enddo
        endif
        if(HKLF5RefBlock(KRefBlock).eq.0) then
          if(DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
            ikde1=(NDim95(KRefBlock)+1)*4+65
            read(t256(ikde1:),'(i3)') i
            if(i.ne.0) go to 1700
            ikde2=(NDim95(KRefBlock)+1)*4+82
            OffSet=0.
            do i=1,it-1
              OffSet=Offset+RunNFrames(i,KRefBlock)
            enddo
            read(t256(ikde2:),'(f8.2)') pom
            if(pom.le.0.) then
              NRuns(KRefBlock)=0
              go to 1710
            endif
            expos=pom+OffSet
            RunCCD=nint(expos)
            RunCCDMax(KRefBlock)=max(RunCCDMax(KRefBlock),RunCCD)
          else
            if(NRuns(KRefBlock).gt.0) then
              ikde=(NDim95(KRefBlock)+1)*4+79
              if(DifCode(KRefBlock).eq.IdBrukerCCDRaw) i=i+3
              read(t256(ikde:),'(f8.2)') pom
              if(pom.le.0.) then
                NRuns(KRefBlock)=0
                go to 1710
              endif
              expos=pom
              RunCCD=nint(pom)
              RunCCDMax(KRefBlock)=max(RunCCDMax(KRefBlock),RunCCD)
              if(it.gt.NRuns(KRefBlock)) then
                RunNFrames(NRuns(KRefBlock),KRefBlock)=
     1            (RunNFrames(NRuns(KRefBlock),KRefBlock)+
     2             RunCCDMax(KRefBlock))/2
                NRuns(KRefBlock)=NRuns(KRefBlock)+1
                RunMeasureTime(NRuns(KRefBlock),KRefBlock)=1.
                if(it.ne.NRuns(KRefBlock))
     1            call FeChybne(-1.,-1.,'it<>NRuns',' ',SeriousError)
              else
                RunNFrames(NRuns(KRefBlock),KRefBlock)=
     1            RunCCDMax(KRefBlock)
              endif
            endif
          endif
1710      call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        else if(HKLF5RefBlock(KRefBlock).eq.1) then
          iflg(2)=it
          KPhase=KPhaseTwin(iabs(it))
          if(UseTrRefBlock(KRefBlock)) then
            call MultMIRI(ih,TrRefBlock(1,KRefBlock),ihp,1,maxNDim,
     1                    maxNDim)
            call CopyVekI(ihp,ih,maxNDim)
          endif
        else if(HKLF5RefBlock(KRefBlock).eq.2) then
          if(it.lt.0) then
            ntw=ntw+1
            if(iabs(it).lt.itw) then
              itw=iabs(it)
              t256opt=t256
            endif
            go to 1700
          else
            if(ntw.le.0.or.it.lt.itw) then
              iflg(2)=it
            else
              read(t256opt,FormatRefBlock(KRefBlock),err=9930)
     1          (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2          ((dircos(i,j),j=1,2),i=1,3)
              iflg(2)=itw
            endif
            KPhase=KPhaseTwin(iabs(iflg(2)))
          endif
        endif
        noa=noa+1
        no=noa
        if(NRuns(KRefBlock).le.0) expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdD9ILL) then
        read(71,FormA,end=9920,err=9930) t256
        k=0
        call kus(t256,k,Cislo)
        call posun(Cislo,0)
        read(Cislo,FormI15,err=9930) no
        call StToReal(t256,k,h,NDim95(KRefBlock),.false.,ich)
        if(ich.eq.1) go to 9930
        do i=1,NDim95(KRefBlock)
          if(h(i).gt.900.) go to 9920
          ih(i)=nint(h(i))
        enddo
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        noa=no
        expos=float(noa)*.01
        do i=1,nstd
          if(eqiv(HRead,ihs(1,i),maxNDim)) then
            if(nhs(i).gt.0) no=-no
            nhs(i)=nhs(i)+1
          endif
        enddo
        if(maxNDim.gt.3) then
          call DRReadRefTrRealInd(h,ich)
          if(ich.ne.0) go to 9900
        else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        endif
        call StToReal(t256,k,h,2,.false.,ich)
        if(ich.ne.0) go to 9930
        ri=h(1)
        rs=h(2)
        call StToReal(t256,k,uhlyk,4,.false.,ich)
        uhly(1)=uhlyk(4)
        uhly(2)=uhlyk(3)
        uhly(3)=uhlyk(2)
        uhly(4)=uhlyk(1)
        if(ich.ne.0) go to 9930
      else if(DifCode(KRefBlock).eq.IdPolNeutrons) then
        read(71,*,end=9920,err=9930) ih(1:NDim95(KRefBlock)),ri,rs
        noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdHasyLabF1.or.
     1        DifCode(KRefBlock).eq.IdHasyLabHuber) then
        read(71,FormA,end=9920,err=9930) t256
        i=1
        if(t256.eq.' ') go to 1000
4010    if(t256(i:i).eq.' ') then
          i=i+1
          go to 4010
        else
          if(index(cifry,t256(i:i)).le.0) go to 1000
        endif
        if(DifCode(KRefBlock).eq.IdHasyLabHuber) then
          k=0
          call StToInt(t256,k,ih,NDim95(KRefBlock),.false.,ich)
          if(ich.ne.0) go to 9930
          call CopyVekI(ih,HRead,NDim95(KRefBlock))
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        else
          k=0
          call StToReal(t256,k,h,3,.false.,ich)
          if(ich.eq.1) go to 9930
          do i=1,3
            if(h(i).gt.900.) go to 9920
            if(maxNDim.le.3) ih(i)=nint(h(i))
          enddo
          call CopyVekI(ih,HRead,NDim95(KRefBlock))
          if(maxNDim.gt.3) then
            call DRReadRefTrRealInd(h,ich)
            if(ich.ne.0) go to 9900
          else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
          endif
        endif
        do i=1,iskipHasy
          read(71,FormA)
        enddo
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        do i=1,nstd
          if(eqiv(HRead,ihs(1,i),NDim95(KRefBlock))) then
            if(nhs(i).gt.0) no=-no
            nhs(i)=nhs(i)+1
          endif
        enddo
        call kus(t256,k,Cislo)
            call posun(Cislo,1)
        read(Cislo,100,err=9930) ri
        call kus(t256,k,Cislo)
        call posun(Cislo,1)
        read(Cislo,100,err=9930) rs
        do i=1,2
          call kus(t256,k,Cislo)
          if(k.ge.128) go to 1000
        enddo
        call StToReal(t256,k,uhlyk,4,.false.,ich)
        theta=uhlyk(1)*.5
        if(KappaGeom) then
          snkp=sin(uhlyk(3)*ToRad/2.)
          cskp=cos(uhlyk(3)*ToRad/2.)
          chi=2.*atan2(snalfa*snkp,sqrt(csalfaq+snalfaq*cskp**2))/ToRad
          delta=atan2(csalfa*snkp,cskp)/ToRad
          fi=-uhlyk(4)-delta
          omega=uhlyk(2)+delta
        else
          omega=uhlyk(2)
          fi=uhlyk(4)
          chi=uhlyk(3)
        endif
      else if(DifCode(KRefBlock).eq.IdKumaCCD) then
        call SetIntArrayTo(ih(4),0,3)
        read(71,FormA,end=9920,err=9930) t256
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930) ih(1:3)
        if(EqIV0(ih,3)) then
          k=0
          do i=1,4
            call kus(t256,k,Cislo)
          enddo
          if(k.ge.len(t256)) go to 9920
!          if(index(Cislo,'.').gt.0) go to 9920
        endif
        if(HKLF5RefBlock(KRefBlock).gt.0) then
          if(TypeHKLKuma.eq.0) then
            read(t256,FormatRefBlock(KRefBlock),end=9920,err=4100)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2       ((dircos(i,j),j=1,2),i=1,3),ix,iy,flp,IRStart,IREnd,IRMax,
     3       CorrAbsAppl,CorrScaleAppl
            go to 4150
4100        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2       ((dircos(i,j),j=1,2),i=1,3),ix,iy,flp,IRStart,IREnd,IRMax
            CorrAbsAppl=0.
            CorrScaleAppl=0.
4150        if(NRuns(KRefBlock).gt.0) then
              ir=IRMax/65536+1
              if=mod(IRMax,65536)+1
              RunCCD=if
              do i=1,ir-1
                RunCCD=RunCCD+RunNFrames(i,KRefBlock)
              enddo
            endif
          else
            read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,pom,schwpsi
          endif
          if(it.lt.0.and.HKLF5RefBlock(KRefBlock).eq.2) go to 1000
          iflg(2)=it
          KPhase=KPhaseTwin(iabs(it))
        else
          if(TypeHKLKuma.eq.0) then
            read(t256,FormatRefBlock(KRefBlock),end=9920,err=4200)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2       ((dircos(i,j),j=1,2),i=1,3),ix,iy,flp,IRStart,IREnd,IRMax,
     3       CorrAbsAppl,CorrScaleAppl
            go to 4250
4200        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2       ((dircos(i,j),j=1,2),i=1,3),ix,iy,flp,IRStart,IREnd,IRMax
            CorrAbsAppl=0.
            CorrScaleAppl=0.
4250        if(NRuns(KRefBlock).gt.0) then
              ir=IRMax/65536+1
              if=mod(IRMax,65536)+1
              RunCCD=if
              do i=1,ir-1
                RunCCD=RunCCD+RunNFrames(i,KRefBlock)
              enddo
              expos=RunCCD
            endif
          else
            read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1       (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,pom,schwpsi
          endif
        endif
        if(EqIV0(ih,NDim95(KRefBlock))) go to 9920
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        call DRReadRefTrIntInd(ich)
        if(ich.ne.0) go to 9900
        if(UseTrRefBlock(KRefBlock)) then
          do i=1,2
            call MultM(TrDirCos(1,KRefBlock),DirCos(1,i),xp,3,3,1)
            call CopyVek(xp,DirCos(1,i),3)
          enddo
        endif
        noa=noa+1
        no=noa
        if(NRuns(KRefBlock).le.0) expos=float(noa)*.01
        if(TypeHKLKuma.eq.0) then
          pom=0.
          do i=2,3
            do j=1,2
              pom=pom+abs(DirCos(i,j))
            enddo
          enddo
          if(pom.gt..00001) then
            call RealVectorToOpposite(dircos(1,1),dircos(1,1),3)
            PocitatDirCos=.false.
            call FromIndSinthl(ih,HReadReal,sinthl,sinthlq,1,0)
            pomo=sinthl*LamAveRefBlock(KRefBlock)
            if(pomo.gt..99999) go to 9800
            omega=asin(sinthl*LamAveRefBlock(KRefBlock))/ToRad
            theta=omega
            call DRGetAnglesFromDircos
          else
            PocitatUhly=.true.
          endif
        else
          call CopyVek(HReadReal,h,3)
        endif
      else if(DifCode(KRefBlock).eq.IdKumaPD) then
        n=0
5000    if(n.ge.111) go to 5500
        read(71,FormA,end=9920,err=9930) Radka
        call mala(Radka)
        k=1
5100    if(Radka(1:1).eq.'p') then
          call kus(Radka,k,Cislo)
          if(Cislo.eq.'scan') then
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'parameters') go to 5000
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'sc') go to 5000
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'s') go to 5000
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) ScanSpeed
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'sc') go to 5000
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'w') go to 5000
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) doma
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) domb
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) domc
          else if(Cislo.eq.'mo') then
            call kus(Radka,k,Cislo)
            if(Cislo.ne.'b') go to 5000
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) BckgFactor
          endif
        else if(Radka(1:1).eq.'#'.or.Radka(1:1).eq.'r') then
          if(ScanSpeed.lt.0..or.doma.lt.0..or.domb.lt.0..or.domc.lt.0.
     1                      .or.BckgFactor.lt.0.) then
            call FeChybne(-1.,-1.,'KUMA - basic information missing.',
     1                    ' ',SeriousError)
            go to 9940
          endif
          n=1
          nprof=0
          call kus(Radka,k,Cislo)
          call posun(Cislo,0)
          read(Cislo,FormI15,err=9930) no
          call StToReal(Radka,k,h,3,.false.,ich)
          if(ich.ne.0) go to 9930
          PrijdeVen=.false.
          if(maxNDim.le.3) then
            do i=1,3
              pom=anint(h(i))
              if(abs(pom-h(i)).gt..001) then
                PrijdeVen=.true.
                go to 5130
              endif
              ih(i)=nint(pom)
            enddo
            call CopyVekI(ih,HRead,NDim95(KRefBlock))
            call DRReadRefTrIntInd(ich)
            if(ich.ne.0) go to 9900
          else
            call DRReadRefTrRealInd(h,ich)
            if(ich.ne.0) then
              PrijdeVen=.true.
              go to 5130
            endif
          endif
5130      call kus(Radka,k,Cislo)
          call kus(Radka,k,Flag)
          if(Radka(1:1).eq.'#') then
            call StToReal(Radka,k,c,3,.false.,ich)
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9930) rych
            rych=rych*ScanSpeed*60.
            if(ich.ne.0) go to 9930
            B1=c(1)
            B2=c(3)
            P=c(2)
          else
            call StToReal(Radka,k,c,2,.false.,ich)
            if(ich.ne.0) go to 9930
            ri=c(2)
            rs=sqrt(c(2))
            rych=ScanSpeed*60.
          endif
        else if(Radka(1:1).eq.'z'.and.n.eq.1) then
          n=n+10
          call StToReal(Radka,k,uhlyk,4,.false.,ich)
          if(ich.ne.0) go to 9930
        else if(Radka(1:1).eq.'s') then
          n=n+100
5150      k=1
5200      call kus(Radka,k,Cislo)
          call posun(Cislo,0)
          Nprof=Nprof+1
          read(Cislo,FormI15) Iprof(NProf,1)
          if(k.ge.80) then
            read(71,FormA,end=9920,err=9930) Radka
            call mala(Radka)
            if(Radka(1:1).eq.'s') then
              go to 5150
            else
              NBckg=nint(.5*float(Nprof)*BckgFactor/(BckgFactor+1.))
              if(flag.eq.'ft'.or.flag.eq.'aft') rych=rych*AttFac
              go to 5000
            endif
          endif
          go to 5200
        else if(Radka(1:1).eq.'y') then
          k=1
          call StToInt(Radka,k,ia,3,.false.,ich)
          call StToInt(Radka,k,ia,4,.false.,ich)
          ScanTime=-float(ia(1))-float(ia(2))/60.
     1             -float(100*ia(3)+ia(4))/360000.
          call StToInt(Radka,k,ia,4,.false.,ich)
          ScanTime=ScanTime+float(ia(1))+float(ia(2))/60.
     1                     +float(100*ia(3)+ia(4))/360000.
          if(ScanTime.lt.0.) ScanTime=ScanTime+24.
        endif
        go to 5000
5500    call KumaToEuler(uhlyk,uhly)
        pom=theta*ToRad
        dt=(doma+domb*tan(pom))*.5
        pomo=sin(pom)*domc
        if(pomo.gt..99999) go to 9800
        pom=asin(pomo)/ToRad
        tmn=pom-dt
        tmx=pom+dt
        omn=pom-dt
        omx=pom+dt
        expos=expold+ScanTime*.5
        expold=expos+ScanTime*.5
        if(PrijdeVen) go to 9900
      else if(DifCode(KRefBlock).eq.IdXDS) then
5600    read(71,FormA,end=9920,err=9930) t256
        if(EqIgCase(t256(1:12),'!END_OF_DATA')) go to 9920
        if(t256(1:1).eq.'!') go to 5600
        k=0
        call StToReal(t256,k,h,3,.false.,ich)
        if(ich.ne.0) go to 9930
        call kus(t256,k,Cislo)
        call posun(Cislo,1)
        read(Cislo,100,err=9930) ri
        call kus(t256,k,Cislo)
        call posun(Cislo,1)
        read(Cislo,100,err=9930) rs
        do i=1,3
          ih(i)=nint(h(i))
        enddo
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        if(maxNDim.gt.3) then
          do i=1,3
            h(i)=ih(i)
          enddo
          call DRReadRefTrRealInd(h,ich)
          if(ich.ne.0) go to 9900
        else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        do i=1,7
          call kus(t256,k,Cislo)
        enddo
        read(Cislo,100,err=9930) schwpsi
      else if(DifCode(KRefBlock).eq.IdImportDatRed) then
        call FeWinMessage('Not yet implemented',' ')
        go to 9930
      else if(DifCode(KRefBlock).eq.IdSXD) then
        read(71,FormA,end=9920,err=9930) t256
        call mala(t256)
        if(t256(1:1).eq.'#'.or.t256(1:1).eq.'('.or.
     1    index(t256,'unobserved').gt.0) go to 1000
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1    (ih(i),i=1,NDimSXD),ri,rs,iflg(1),DRLam,pom,
     2    corrf(1),tbar,pom,Longitude,Latitude
        if(NDim95(KDatBlock).gt.0) then
          call SetRealArrayTo(h,NDim95(KRefBlock),0.)
          do i=1,3
            h(i)=ih(i)
          enddo
          if(QuMagUsed) then
            if(ih(4).gt.0) call AddVek(h,QuSXD(1,ih(4)),h,3)
          else
            do i=1,3
              do j=1,NCombSXD
                do k=1,NQuSXD
                  h(i)=h(i)+QuSXD(i,k)*float(CombSXD(k,j)*ih(j+3))
                enddo
              enddo
            enddo
            call CopyVekI(ih,HRead,NDim95(KRefBlock))
          endif
          call DRReadRefTrRealInd(h,ich)
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        pom=corrf(1)
        ri=ri*pom
        rs=rs*pom
        corrf(1)=1.
        corrf(2)=1.
        call CopyVek(AnglesSXD,uhly,3)
      else if(DifCode(KRefBlock).eq.IdTopaz) then
        read(71,FormA,end=9920,err=9930) t256
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1    (ih(i),i=1,NDim95(KRefBlock)),ri,rs,iflg(1),DRLam,tbar,
     2    ((dircos(i,j),j=1,2),i=1,3),i1,i2,re1,i3,re2,re3,re4,re5
        if(EqIV0(ih,maxNDim)) go to 9910
        call CopyVekI(ih,HRead,maxNDim)
        call RealVectorToOpposite(dircos(1,1),dircos(1,1),3)
        if(UseTrRefBlock(KRefBlock)) then
          do i=1,2
            call MultM(TrDirCos(1,KRefBlock),DirCos(1,i),xp,3,3,1)
            call CopyVek(xp,DirCos(1,i),3)
          enddo
        endif
        call DRReadRefTrIntInd(ich)
        if(ich.ne.0) go to 9900
        noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdSENJU) then
        read(71,FormA,end=9920,err=9930) t256
        read(t256,'(i1)',end=9920,err=9930) i
        if(i.ne.3) go to 1000
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1    i,(ih(i),i=1,NDim95(KRefBlock)),(pom,i=1,5),DRLam,pom,ri,rs,i,
     2    NRun
        do i=1,NRunSENJU
          if(RunSenju(i).eq.NRun) then
            iflg(1)=i
            go to 5700
          endif
        enddo
        call ReallocateRunSENJU(1)
        NRunSENJU=NRunSENJU+1
        iflg(1)=NRunSENJU
        RunSENJU(NRunSENJU)=NRun
5700    noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdVivaldi.or.
     1        DifCode(KRefBlock).eq.IdKoala) then
        read(71,FormA,end=9920,err=9930) t256
        if(t256.eq.' ') go to 1000
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1    (ih(i),i=1,NDim95(KRefBlock)),ri,rs,DRLam
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        rs=2.*rs*ri
        ri=ri**2
        corrf(1)=1.
        corrf(2)=1.
      else if(DifCode(KRefBlock).eq.IdSCDLANL) then
        read(71,FormA,end=9920,err=9930) t256
        if(t256.eq.' ') go to 1000
        read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1    (ih(i),i=1,NDim95(KRefBlock)),ri,rs,iflg(1),DRLam,tbar
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        rs=2.*rs*ri
        ri=ri**2
        corrf(1)=1.
        corrf(2)=1.
      else if(DifCode(KRefBlock).eq.IdImportFullProf) then
        read(71,FormA,end=9920,err=9930) t256
        if(LocateSubstring(t256,'!',.false.,.true.).gt.0) go to 1000
        if(TwinFullProf.gt.0) then
          read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1      (ih(i),i=1,NDimSXD),ri,rs,itwp
          if(itwp.ne.1) go to 1000
        else
          read(t256,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1      (ih(i),i=1,NDimSXD),ri,rs
        endif
        if(FSqFullProf.eq.0) then
          rs=2.*ri*rs
          ri=ri**2
        endif
        if(NDim95(KDatBlock).gt.0) then
          call SetRealArrayTo(h,NDim95(KRefBlock),0.)
          do i=1,3
            h(i)=ih(i)
          enddo
          if(ih(4).gt.0) call AddVek(h,QuSXD(1,ih(4)),h,3)
          call DRReadRefTrRealInd(h,ich)
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        corrf(1)=1.
        corrf(2)=1.
      else if(DifCode(KRefBlock).eq.IdImportSHELXF.or.
     1        DifCode(KRefBlock).eq.IdImportHKLF5.or.
     2        DifCode(KRefBlock).eq.IdImportIPDS.or.
     3        DifCode(KRefBlock).eq.IdImportCIF.or.
     4        DifCode(KRefBlock).eq.IdImportGraindex.or.
     5        DifCode(KRefBlock).eq.IdImportCCDBruker.or.
     6        DifCode(KRefBlock).eq.IdImportGeneralF.or.
     7        DifCode(KRefBlock).eq.IdImportGeneralI.or.
     8        DifCode(KRefBlock).eq.IdPets) then
        if(FreeFormat) then
          read(71,FormA,end=9920,err=9930) t256
          do i=1,idel(t256)
            if(ichar(t256(i:i)).eq.9) t256(i:i)=' '
          enddo
          if(idel(t256).eq.0) go to 1000
          k=0
          RealIndices=.false.
          do i=1,maxNDim
            call Kus(t256,k,Cislo)
            if(index(Cislo,'.').gt.0) then
              RealIndices=.true.
              exit
            endif
          enddo
          if(RealIndices) then
            read(t256,*,end=9920,err=9930)(h(i),i=1,3),ri,rs
            call DRReadRefTrRealInd(h,ich)
            go to 6450
          else
            read(t256,*,end=9920,err=9930)
     1        (ih(i),i=1,NDim95(KRefBlock)),ri,rs
          endif
        else if(DifCode(KRefBlock).eq.IdImportCIF.or.
     1          DifCode(KRefBlock).eq.IdPets) then
          call DRReadRefCIF(71,0,ik)
          RunCCD=iflg(1)
          NRuns(KRefBlock)=1
          RunMeasureTime(1,KRefBlock)=1.
          RunNFrames(1,KRefBlock)=max(RunNFrames(1,KRefBlock),RunCCD)
          iflg(1)=1
          if(ik.ne.0) go to 9920
        else if(DifCode(KRefBlock).eq.IdImportGraindex) then
          call DRReadRefGraindex(ik)
          if(ik.ne.0) go to 9920
        else if(DifCode(KRefBlock).eq.IdImportHKLF5) then
          ntw=0
          itw=999999
6400      read(71,FormA,end=9920,err=9930) t256
          if(t256.eq.' ') go to 6400
          read(t256,FormatRefBlock(KRefBlock),err=9930)
     1      (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it,
     2      ((dircos(i,j),j=1,2),i=1,3)
          if(EqIV0(ih,NDim95(KRefBlock))) go to 9910
          if(UseTrRefBlock(KRefBlock)) then
            call MultMIRI(ih,TrRefBlock(1,KRefBlock),ihp,1,maxNDim,
     1                    maxNDim)
            call CopyVekI(ihp,ih,maxNDim)
          endif
          if(HKLF5RefBlock(KRefBlock).eq.1) then
            iflg(2)=it
            KPhase=KPhaseTwin(iabs(it))
          else if(HKLF5RefBlock(KRefBlock).eq.2) then
            if(it.lt.0) then
              ntw=ntw+1
              if(iabs(it).lt.itw) then
                itw=iabs(it)
                t256opt=t256
              endif
              go to 6400
            else
              if(ntw.le.0.or.it.lt.itw) then
                iflg(2)=it
                KPhase=KPhaseTwin(iabs(it))
              else
                read(t256opt,FormatRefBlock(KRefBlock),err=9930)
     1            (ih(i),i=1,NDim95(KRefBlock)),ri,rs,it
                iflg(2)=itw
                KPhase=KPhaseTwin(iabs(itw))
              endif
            endif
          endif
        else
          if(nread.le.1) then
            i1=LocateSubstring(FormatRefBlock(KRefBlock),'f',.false.,
     1                         .true.)
            i2=LocateSubstring(FormatRefBlock(KRefBlock),'i',.false.,
     1                         .true.)
            RealIndices=i1.lt.i2.or.i2.le.0
          endif
          if(RealIndices) then
            read(71,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1        h(1:3),ri,rs
            call DRReadRefTrRealInd(h,ich)
            if(ich.ne.0) go to 9900
          else
            read(71,FormatRefBlock(KRefBlock),end=9920,err=9930)
     1        (ih(i),i=1,NDim95(KRefBlock)),ri,rs
          endif
        endif
        call CopyVekI(ih,HRead,maxNDim)
        if(EqIV0(ih,maxNDim)) go to 9910
        if(DifCode(KRefBlock).ne.IdImportHKLF5)
     1    call DRReadRefTrIntInd(ich)
6450    if(ich.ne.0) go to 9900
        if(DifCode(KRefBlock).eq.IdImportSHELXF.or.
     1     DifCode(KRefBlock).eq.IdImportSHELXI.or.
     2     DifCode(KRefBlock).eq.IdImportGeneralF.or.
     3     DifCode(KRefBlock).eq.IdImportGeneralI.or.
     4     DifCode(KRefBlock).eq.IdImportHKLF5.or.
     5     DifCode(KRefBlock).eq.IdImportCCDBruker) then
!          if(DifCode(KRefBlock).ne.IdImportGeneralF.and.
!     1       DifCode(KRefBlock).ne.IdImportGeneralI) then
!            ri=10.*ri
!            rs=10.*rs
!          endif
          if(DifCode(KRefBlock).eq.IdImportSHELXF.or.
     1       DifCode(KRefBlock).eq.IdImportGeneralF) then
            if(ri.gt.rs) then
              rs=2.*rs*ri
            else
              rs=2.*rs**2
            endif
            ri=ri**2
          endif
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdImportXD) then
        read(71,FormA,end=9920,err=9930) Radka
        k=0
        call StToInt(Radka,k,ia,4,.false.,ich)
        if(ich.ne.0) go to 9900
        call CopyVekI(ia,ih,3)
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 9900
        if(XDCteI) then
          ri=100.*xp(1)
          rs=100.*xp(2)
        else
          ri=100.*xp(1)**2
          rs=100.*2.*xp(1)*xp(2)
        endif
        if(XDNdat.gt.6) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9900
          tbar=xp(1)
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdImportJanaM90) then
        read(71,FormatRefBlock(KRefBlock),err=9930,end=9920)
     1   ih(1:NDim95(KRefBlock)),ri,rs,iflg(1),i,iflg(2),tbar,DRLam,
     2    ((dircos(i,j),i=1,3),j=1,2)
        if(UseTrRefBlock(KRefBlock)) then
          do i=1,2
            call MultM(TrDirCos(1,KRefBlock),DirCos(1,i),xp,3,3,1)
            call CopyVek(xp,DirCos(1,i),3)
          enddo
        endif
        call DRReadRefTrIntInd(ich)
        if(ich.ne.0) go to 9900
        noa=noa+1
        no=noa
        expos=float(noa)*.01
      else if(DifCode(KRefBlock).eq.IdImportDABEX) then
6500    read(71,FormA,end=9920) Radka
        if(idel(Radka).lt.35) go to 6500
        if(PocitatDirCos) then
          if(RealIndices) then
            read(Radka,FormatRefBlock(KRefBlock),err=9930) h(1:3),ri,rs,
     1        pom,((dircos(i,j),i=1,3),j=1,2),tbar
            ih(1:3)=nint(h(1:3))
          else
            read(Radka,FormatRefBlock(KRefBlock),err=9930) ih(1:3),ri,
     1        rs,pom,((dircos(i,j),i=1,3),j=1,2),tbar
          endif
        else
          if(RealIndices) then
            read(Radka,FormatRefBlock(KRefBlock),err=9930) h(1:3),ri,rs,
     1        pom,tbar
            ih(1:3)=nint(h(1:3))
          else
            read(Radka,FormatRefBlock(KRefBlock),err=9930) ih(1:3),ri,
     1        rs,pom,tbar
          endif
        endif
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        rs=2.*ri*rs
        ri=ri**2
      else if(DifCode(KRefBlock).eq.Id6T2LBB) then
6600    read(71,FormA,end=9920,err=9930) Radka
        do i=1,idel(Radka)
          if(Radka(i:i).eq.Tabulator) Radka(i:i)=' '
        enddo
        if(Radka.eq.' '.or.
     1     LocateSubstring(Radka,'f',.false.,.true.).gt.0) go to 6600
        k=0
        call Kus(Radka,k,Cislo)
        call StToReal(Radka,k,h,NDim95(KRefBlock),.false.,ich)
        if(ich.ne.0) go to 9930
        do i=1,NDim95(KRefBlock)
          ih(i)=nint(h(i))
        enddo
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 9930
        ri=xp(1)
        rs=xp(2)
        noa=noa+1
        no=noa
        expos=float(noa)*.01
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        if(maxNDim.gt.3) then
          do i=1,3
            h(i)=ih(i)
          enddo
          call DRReadRefTrRealInd(h,ich)
          if(ich.ne.0) go to 9900
        else
          call DRReadRefTrIntInd(ich)
          if(ich.ne.0) go to 9900
        endif
      endif
      if(PocitatDirCos) then
        if(DirCosFromPsi) then
          call FromIndSinthl(ih,HReadReal,sinthl,sinthlq,1,0)
          pomo=sinthl*LamAveRefBlock(KRefBlock)
          if(pomo.gt..99999) go to 9800
          theta=asin(sinthl*LamAveRefBlock(KRefBlock))/ToRad
          do i=1,3
            do j=i,3
              if(abs(HRead(i)-HRead(j)).gt..0001) then
                do ii=1,3
                  jj=mod(ii,3)+1
                  kk=mod(ii+1,3)+1
                  qdir(ii)=HRead(jj)-HRead(kk)
                enddo
                go to 7000
              endif
            enddo
          enddo
          qdir(1)= HRead(1)
          qdir(2)=-HRead(1)
          qdir(3)=0.
7000      frec(1:3)=HRead(1:3)
          call multm(MetTensI(1,1,KPhase),frec,fdir,3,3,1)
          call VecNor(frec,fdir)
          call multm(MetTens(1,1,KPhase),qdir,qrec,3,3,1)
          pom=sqrt(scalmul(qrec,qdir))
          call VecNor(qrec,qdir)
          call VecMul(fdir,qdir,fqrec)
          call multm(MetTensI(1,1,KPhase),fqrec,fqdir,3,3,1)
          call VecNor(fqrec,fqdir)
          cspsi=cos(schwpsi*torad)
          snpsi=sin(schwpsi*torad)
          csth=cos(theta*torad)
          snth=sin(theta*torad)
          do i=1,3
            sod(i)=(-qdir(i)*snpsi-fqdir(i)*cspsi)*csth-fdir(i)*snth
            sd(i) =(-qdir(i)*snpsi-fqdir(i)*cspsi)*csth+fdir(i)*snth
          enddo
        else if(DifCode(KRefBlock).eq.IdSXD) then
          call UnitMat(Rot,3)
          do i=3,1,-1
            if(i.eq.1.or.i.eq.3) then
              j=3
            else
              j=1
            endif
            call SetRotMatAboutAxis(AnglesSXD(i)*SenseOfAngle(i),j,Rt)
            call multm(Rot,Rt,tt,3,3,3)
            call CopyMat(tt,Rot,3)
          enddo
          Longitude=(180.+Longitude)*ToRad
          Latitude=Latitude*ToRad
          csa=cos(Longitude)
          sna=sin(Longitude)
          csb=cos(Latitude)
          snb=sin(Latitude)
          tt(1,1)=-1.
          tt(2,1)= 0.
          tt(3,1)= 0.
          tt(1,2)=csa*csb
          tt(2,2)=sna*csb
          tt(3,2)=snb
          call multm(Rot,ub(1,1,KRefBlock),rt,3,3,3)
          call multm(tt(1,1),rt,sod,1,3,3)
          call multm(tt(1,2),rt,sd ,1,3,3)
        else if(DifCode(KRefBlock).eq.IdImportDABEX) then
          sod(1:3)=dircos(1:3,1)*rcp(1:3,1,KPhase)
          sd (1:3)=dircos(1:3,2)*rcp(1:3,1,KPhase)
        else
          call UnitMat(Rot,3)
          do i=3,1,-1
            if(i.eq.1.or.i.eq.3) then
              j=3
            else
              j=1
            endif
            call SetRotMatAboutAxis(uhly(i)*SenseOfAngle(i),j,Rt)
            call multm(Rot,Rt,tt,3,3,3)
            call CopyMat(tt,Rot,3)
          enddo
          tt(1,1)=-1.
          tt(2,1)= 0.
          tt(3,1)= 0.
          pom=2.*theta*torad*SenseOfAngle(4)
          tt(1,2)=-cos(pom)
          tt(2,2)=-sin(pom)
          tt(3,2)=0.
          call multm(Rot,ub(1,1,KRefBlock),rt,3,3,3)
          call multm(tt(1,1),rt,sod,1,3,3)
          call multm(tt(1,2),rt,sd ,1,3,3)
        endif
        do i=1,3
          dircos(i,1)=sod(i)/rcp(i,1,KPhase)
          dircos(i,2)=sd(i)/rcp(i,1,KPhase)
        enddo
        if(DirCosFromPsi) call DRGetAnglesFromDircos
      else
        if(PocitatUhly) then
          fi=0.
          chi=0.
          call FromIndSinthl(ih,HReadReal,sinthl,sinthlq,1,0)
          pomo=sinthl*LamAveRefBlock(KRefBlock)
          if(pomo.gt..99999) then
            go to 9800
          endif
          omega=asin(sinthl*LamAveRefBlock(KRefBlock))/ToRad
          theta=omega
        endif
      endif
      if(Profil) KProf=1
      call uprav(fi)
      call uprav(chi)
      call uprav(omega)
      call uprav(theta)
      if(Konec.ne.0) go to 9920
      go to 9999
9800  if(RealIndices) then
        write(Radka,'(6f10.4)')(h(i),i=1,NDim95(KRefBlock))
        call ZdrcniCisla(Radka,NDim95(KRefBlock))
      else
        write(Radka,'(6i4)')(ih(i),i=1,NDim95(KRefBlock))
        call CopyVekI(ih,HRead,NDim95(KRefBlock))
        call ZdrcniCisla(Radka,NDim95(KRefBlock))
      endif
      j=idel(Radka)
      do i=1,j
        if(Radka(i:i).eq.' ') Radka(i:i)=','
      enddo
      Radka='the reflection ('//Radka(:j)//') has unrealistic indices.'
      call FeChybne(-1.,-1.,Radka,' ',WarningWithESC)
      if(ErrFlag.ne.0) then
        go to 9940
      else
        go to 9950
      endif
9900  if(IntFromProfile) then
        do i=1,NProf
          py(i,1)=IProf(i,1)
        enddo
        call BPB(ri,rs,1,NBckg)
      endif
      call EM9NejdouPotvory(1,HRead,h,ri,rs,NDim95(KRefBlock),
     1                      RealIndices,3.)
      ich=5
      go to 9999
9910  ich=1
      go to 9999
9920  ich=2
      if((DifCode(KRefBlock).eq.IdIPDSStoe.or.
     1    DifCode(KRefBlock).eq.IdNoniusCCD.or.
     2    DifCode(KRefBlock).eq.IdBrukerCCD.or.
     3    DifCode(KRefBlock).eq.IdRigakuCCD.or.
     4    DifCode(KRefBlock).eq.IdImportSHELXI.or.
     5    DifCode(KRefBlock).eq.IdSHELXINoAbsCorr).and.
     6    NRuns(KRefBlock).gt.0) then
        RunNFrames(NRuns(KRefBlock),KRefBlock)=
     1    RunNFrames(NRuns(KRefBlock),KRefBlock)+1
        RunCCDMax(KRefBlock)=RunCCDMax(KRefBlock)+1
        n=RunNFrames(1,KRefBlock)
        do i=2,NRuns(KRefBlock)
          m=RunNFrames(i,KRefBlock)
          RunNFrames(i,KRefBlock)=RunNFrames(i,KRefBlock)-n
          n=m
        enddo
      endif
      go to 9999
9930  ich=3
      go to 9999
9940  ich=4
      go to 9999
9950  ich=5
9999  RunCCDMax(KRefBlock)=max(RunCCDMax(KRefBlock),RunCCD)
      KPhase=KPhaseIn
      return
100   format(f15.0)
      end
