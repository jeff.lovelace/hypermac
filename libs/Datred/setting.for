      subroutine Setting(x,A,Rko,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension x(3),A(4),Rko(3,3)
      z1=x(3)*cb-x(1)*sb
      if(abs(z1).ge.1.) z1=sign(1.,z1)
      som=z1*cab/sa
      com=Sqrt(1.-som**2)
      tom=som/com
      A(1)=AngKuma(som,com)
      xx=tom**2*sc2
      if(xx.gt.1.) then
        A(3)=-2.*AngKuma(com,sc1*som)
        ska=sin(A(3))
        cka=cos(A(3))
        sph=0.
        cph=1.
      else if(abs(A(1))*stepKuma(1).lt.0.5) then
        som=0.
        com=1.
        A(1)=som
        ska=som
        cka=1.
        A(3)=AngKuma(ska,cka)
        xx=1.
      else
        A(3)=2.0*atan((sqrt(1.-xx)-1.)/(tom*sc1))
        ska=sin(A(3))
        cka=cos(A(3))
        xx=(com*cka-ca*som*ska)
      endif
      x1=x(3)*sb+x(1)*cb
      if(Abs(x1).gt.Abs(xx)) then
        sph=sign(1.,-x1*xx)
        cph=0.
      else if(Abs(x(2)).gt.Abs(xx)) then
        cph=sign(1.,x(2)*xx)
        sph=0.
      else
        sph=-x1/xx
        cph=x(2)/xx
      endif
      A(4)=AngKuma(sph,cph)
      call CopyVek(A,Porig,4)
      psim=rPsi(A(1),A(3),A(4))
      Psi=0.
      call AnglesFromRotation(A,Rko,Psi,ich)
      A(1)=A(1)-A(2)
      return
      end
