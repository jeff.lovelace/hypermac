      subroutine DRBasicSHELXINoAbsCorr
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Radka
      FormatRefBlock(KRefBlock)='(.i4,2f8.4,i4,6f8.5,2f7.2,f8.2)'
      write(FormatRefBlock(KRefBlock)(2:2),101) NDim95(KRefBlock)
      rewind 71
      RunMax=0.
      NRuns(KRefBlock)=0
1100  read(71,FormatRefBlock(KRefBlock),err=9999,end=3000)
     1  ih(1:maxNDim),(pom,i=1,2),i,(pom,i=1,8),RunPom
      RunMax=max(RunPom,RunMax)
      go to 1100
3000  rewind 71
      NRuns(KRefBlock)=1
      RunNFrames(1,KRefBlock)=nint(RunMax)
      RunMeasureTime(1,KRefBlock)=1.
      RunCCDMax(KRefBlock)=RunNFrames(1,KRefBlock)
9999  return
101   format(i1)
      end
