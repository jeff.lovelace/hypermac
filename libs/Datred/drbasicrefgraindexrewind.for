      subroutine DRBasicRefGraindexRewind
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Radka
      rewind 71
1100  read(71,FormA,end=9000) Radka
      call ChangeCommasForPoints(Radka)
      i1=0
      do i=1,idel(Radka)
        if(Radka(i:i).ne.' ') then
          i2=index(Cifry(1:13),Radka(i:i))
          if(i2.le.0) go to 1100
          i1=max(i1,index(Cifry(1:10),Radka(i:i)))
        endif
      enddo
      if(i1.eq.0) go to 1100
      backspace 71
      go to 9999
9000  Radka='the end of the input file is reached.'
9900  call FeChybne(-1.,-1.,Radka,' ',SeriousError)
      ErrFlag=1
9999  return
      end
