      subroutine StoreMatrix(R,eta,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension R(3,3)
      s=sin(eta)
      c=cos(eta)
      call UnitMat(R,3)
      if(n.eq.1) then
        R(1,1)= c
        R(1,2)= s
        R(2,1)=-s
        R(2,2)= c
      else if(n.eq.2) then
        R(1,1)= ca*cab*c+sa*sab
        R(1,2)= ca*s
        R(1,3)= ca*sab*c-sa*cab
        R(2,1)=-cab*s
        R(2,2)= c
        R(2,3)=-sab*s
        R(3,1)= sa*cab*c-ca*sab
        R(3,2)= sa*s
        R(3,3)= sa*sab*c+ca*cab
      else if(n.eq.3) then
        R(1,1)= cb*c
        R(1,2)= s
        R(1,3)= sb*c
        R(2,1)=-cb*s
        R(2,2)= c
        R(2,3)=-sb*s
        R(3,1)=-sb
        R(3,3)=cb
      else if(n.eq.4) then
        R(1,1)= c
        R(1,3)=-s
        R(3,1)= s
        R(3,3)= c
      else if(n.eq.5) then
        R(2,2)= c
        R(2,3)= s
        R(3,2)=-s
        R(3,3)= c
      endif
      return
      end
