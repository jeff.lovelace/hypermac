      subroutine DRCalcDenAbs(wmol,dx,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real LamAveOld
      wmol=0.
      AmiUsed=0.
      ich=0
      RadiationOld=Radiation(1)
      Radiation(1)=RadiationRefBlock(KRefBlock)
      LamAveOld=LamAve(1)
      LamAve(1)=LamAveRefBlock(KRefBlock)
      do i=1,NAtFormula(KPhase)
        call RealFromAtomFile(AtType(i,KPhase),'atweight',pomw,0,
     1                        ich)
        if(ich.ne.0) go to 9900
        call CrlReadAbsCoeff(0,AtType(i,KPhase),AtAbsCoeffTab(i),ich)
        if(ich.ne.0) go to 9900
        if(.not.AtAbsCoeffOwn(KPhase))
     1    AtAbsCoeff(i,KPhase)=AtAbsCoeffTab(i)
        wmol=wmol+pomw              * AtMult(i,KPhase)
        AmiUsed=AmiUsed+AtAbsCoeff(i,KPhase)*AtMult(i,KPhase)*.1
      enddo
      dx=float(NUnits(KPhase))*wmol*1.e24/
     1   (CellVol(1,KPhase)*AvogadroNumber)
      AmiUsed=AmiUsed*float(NUnits(KPhase))/CellVol(1,KPhase)
      go to 9999
9900  ich=1
9999  Radiation(1)=RadiationOld
      LamAve(1)=LamAveOld
      return
      end
