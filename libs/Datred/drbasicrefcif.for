      subroutine DRBasicRefCIF(ln,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ihc(6),ifc(2),ra(10)
      character*256 Radka,Veta
      character*256, allocatable :: StRef(:)
      character*80 StPom(20)
      logical CteInt,EqIgCase,AlreadyAllocated,Poprve
      integer :: CIFRefl(13)=(/13,14,15,32,33,34, 7, 8,10,11,17,18,40/),
     1           CIFTwin(13)=(/23,24,25,26,27,28, 0, 0,20,21, 0, 0, 0/),
     2           CIFPom(13),CteTwin,ia(2)
      integer, allocatable :: nopa(:),itwp(:)
      save ihc,ifc,NData,CteInt,mm,CteTwin,CIFPom,iff,ift,idd,idt
      save AlreadyAllocated
      if(allocated(CifKey)) then
        AlreadyAllocated=.true.
      else
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
        AlreadyAllocated=.false.
      endif
      Poprve=.true.
1000  if(ln.eq.0) then
        mm=0
      else
        rewind ln
      endif
      NData=0
      if(Poprve) then
        icat=16
        nCIFPom=13
        CIFPom(1:nCIFPom)=CIFRefl(1:nCIFPom)
      else
        icat=23
        nCIFPom=13
        CIFPom(1:nCIFPom)=CIFTwin(1:nCIFPom)
      endif
1050  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 9000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=9000) Radka
      endif
      k=0
      call kus(Radka,k,Veta)
      call mala(Veta)
      if(Veta.eq.'loop_') then
        j=0
        call SetIntArrayTo(ihc,6,0)
        call SetIntArrayTo(ifc,2,0)
        iff=0
        ift=0
        idt=0
        idd=0
        CteTwin=0
1100    if(ln.eq.0) then
          mm=mm+1
          if(mm.gt.nCIFUsed) go to 9000
          Radka=CIFArray(mm)
        else
          read(ln,FormA,end=9000) Radka
        endif
        if(Radka.eq.' ') go to 1100
        k=0
        call kus(Radka,k,Veta)
        i=1
1110    if(Veta(i:i).eq.' ') then
          i=i+1
          go to 1110
        endif
        Veta=Veta(i:)
        if(Veta(1:1).ne.'_') go to 1200
        j=j+1
        do i=1,nCIFPom
          k=CIFPom(i)
          if(k.le.0) cycle
          if(EqIgCase(Veta,CifKey(k,icat))) then
            if(i.le.6) then
              ihc(i)=j
            else if(i.le.8) then
              CteInt=.false.
              ifc(i-6)=j
            else if(i.le.10) then
              CteInt=.true.
              ifc(i-8)=j
            else if(i.le.12) then
              CteInt=.true.
              ifc(i-10)=j
            else if(i.le.13) then
              iff=j
            else
              cycle
            endif
            go to 1100
          endif
        enddo
        if(Poprve) then
          if(EqIgCase(Veta,CifKey(56,22))) then
            CteTwin=1
            ift=j
          endif
        else
          if(EqIgCase(Veta,CifKey(34,icat))) then
            idt=j
            CteTwin=2
          else if(EqIgCase(Veta,CifKey(17,icat))) then
            idd=j
            CteTwin=2
          endif
        endif
        go to 1100
1200    n=0
        do i=1,6
          if(ihc(i).ne.0) n=n+1
        enddo
        if(n.le.0) then
          go to 1050
        else if(n.gt.0.and.n.lt.3) then
          go to 9100
        endif
        NData=j
1400    if(index(Radka,'?').gt.0) then
          go to 9000
        else if(Radka.eq.' ') then
          if(ln.eq.0) then
            mm=mm+1
            if(mm.gt.nCIFUsed) go to 9000
            Radka=CIFArray(mm)
          else
            read(ln,FormA,end=9000) Radka
          endif
          go to 1400
        endif
        NDim95(KRefBlock)=n
        n=0
        do i=1,2
          if(ifc(i).ne.0) n=n+1
        enddo
        if(n.gt.0.and.n.lt.2) go to 9100
        if(NData.gt.0) then
          if(ln.eq.0) then
            mm=mm-1
          else
            backspace ln
          endif
          go to 1450
        endif
      endif
      go to 1050
1450  if(CteTwin.eq.2) then
        HKLF5RefBlock(KRefBlock)=1
        allocate(nopa(NTwin),itwp(NTwin),StRef(NTwin))
        if(ln.eq.0) then
          mmStart=mm
        else
          lno=NextLogicNumber()
          call OpenFile(lno,fln(:ifln)//'.l99','formatted','unknown')
        endif
        nop=0
        n=0
1500    if(ln.eq.0) then
          mm=mm+1
          if(mm.gt.nCIFUsed) go to 1700
          Radka=CIFArray(mm)
        else
          read(ln,FormA,end=1700) Radka
        endif
        if(Radka.eq.' ') go to 1500
        k=0
        call StToInt(Radka,k,ia,2,.false.,ich)
        if(nop.le.0) then
          nop=ia(1)
          n=1
          if(ln.eq.0) mm1=mm
        else if(ia(1).eq.nop) then
          n=n+1
        else if(ia(1).ne.nop) then
          if(ln.eq.0) then
            mm=mm-1
          else
            backspace ln
          endif
          itwp(n)=-itwp(n)
          do i=1,n
            if(ln.eq.0) then
              j=mm1+i-1
              write(CIFArray(j),'(2i10,a)') nopa(i),itwp(i),
     1                                      StRef(i)(:idel(StRef(i)))
            else
              write(lno,'(2i10,a)') nopa(i),itwp(i),
     1                              StRef(i)(:idel(StRef(i)))
            endif
          enddo
          nop=0
          n=0
          go to 1500
        endif
        nopa(n)=nop
        itwp(n)=-ia(2)
        kk=k
        do i=k-1,1,-1
          if(Radka(i:i).eq.' ') then
            kk=i
          else
            exit
          endif
        enddo
        StRef(n)=Radka(kk:)
        go to 1500
1700    if(n.gt.0) then
          itwp(n)=-itwp(n)
          do i=1,n
            if(ln.eq.0) then
              j=mm1+i-1
              write(CIFArray(j),'(2i10,a)') nopa(i),itwp(i),
     1                                      StRef(i)(:idel(StRef(i)))
            else
              write(lno,'(2i10,a)') nopa(i),itwp(i),
     1                              StRef(i)(:idel(StRef(i)))
            endif
          enddo
        endif
        if(ln.eq.0) then
          mm=mmStart
        else
          close(lno)
          close(ln)
          call OpenFile(ln,fln(:ifln)//'.l99','formatted','unknown')
        endif
        deallocate(nopa,itwp,StRef)
      endif
      go to 9999
      entry DRReadRefCIF(ln,Klic,ik)
      ik=0
2000  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 3000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=3000) Radka
      endif
      if(Radka.eq.' ') go to 2000
      k=0
      call kus(Radka,k,Veta)
      if(Veta(1:1).eq.'_'.or.index(Veta,'data_').eq.1..or.
     1   EqIgCase(Veta,'loop_')) then
        ik=1
      else if(Veta(1:1).eq.'#') then
        go to 2000
      else
        k=0
        n=0
2100    n=n+1
        call kus(Radka,k,StPom(n))
        if(k.lt.len(Radka)) go to 2100
        if(n.ne.NData) go to 9200
        do i=1,6
          j=ihc(i)
          if(j.le.0) cycle
          k=0
          call StToReal(StPom(j),k,ra,1,.false.,ich)
          ih(i)=nint(ra(1))
        enddo
        do i=1,2
          k=0
          call StToReal(StPom(ifc(i)),k,ra,1,.false.,ich)
          if(i.eq.1) then
            ri=ra(1)
          else
            rs=ra(1)
          endif
        enddo
        if(.not.CteInt) then
          if(ri.gt.rs) then
            rs=2.*rs*ri
          else
            rs=2.*rs**2
          endif
          ri=ri**2
        endif
        if(iff.gt.0) then
          k=0
          call StToReal(StPom(iff),k,ra,1,.false.,ich)
          iflg(1)=nint(ra(1))
        endif
        if(CteTwin.eq.1) then
          if(ift.gt.0) then
            k=0
            call StToReal(StPom(ift),k,ra,1,.false.,ich)
            iflg(2)=nint(ra(1))
          endif
        else if(CteTwin.eq.2) then
          if(idd.gt.0) then
            k=0
            call StToReal(StPom(idd),k,ra,1,.false.,ich)
!            RunCCD=nint(ra(1))
          endif
          if(idt.gt.0) then
            k=0
            call StToReal(StPom(idt),k,ra,1,.false.,ich)
            iflg(2)=nint(ra(1))
          endif
        else
          iflg(2)=1
        endif
      endif
      go to 9999
3000  ik=1
      go to 9999
9000  if(Poprve) then
        Poprve=.false.
        go to 1000
      endif
      Radka='CIF file does not contain any reflection block'
      Veta='before the end of CIF file is reached.'
      go to 9900
9100  Radka='or some of items h, k, l, I, sig(I) is missing.'
      Veta=' '
      go to 9900
9200  Veta='Reading error.'
9900  if(Klic.eq.0) call FeChybne(-1.,-1.,Radka,Veta,SeriousError)
      ErrFlag=1
9999  if(allocated(CifKey).and..not.AlreadyAllocated)
     1  deallocate(CifKey,CifKeyFlag)
      return
      end
