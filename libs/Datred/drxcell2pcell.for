      subroutine DRXCell2PCell(Tr,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Tr(3,3),TrI(3,3),TrP(3,3),VTPom(6)
      logical OK(:)
      allocatable OK
      allocate(OK(NLattVec(KPhase)))
      ich=0
      call SetLogicalArrayTo(OK,NLattVec(KPhase),.false.)
      OK(1)=.true.
      i=1
      call UnitMat(Tr,3)
1100  if(.not.OK(i)) then
        DetOpt=10000.
        mopt=0
        do m=1,3
          call CopyMat(Tr,TrP,3)
          do j=1,3
            TrP(j,m)=vt6(j,i,1,KPhase)
          enddo
          call MatInv(TrP,TrI,Det,3)
          if(Det.gt..0001.and.Det.lt.DetOpt) then
            DetOpt=Det
            mopt=m
          endif
        enddo
        if(mopt.gt.0) then
          do j=1,3
            Tr(j,mopt)=vt6(j,i,1,KPhase)
          enddo
          call MatInv(Tr,TrI,Det,3)
        else
          ich=1
          call UnitMat(Tr,3)
          go to 9999
        endif
        do 1700j=i,NLattVec(KPhase)
          call MultM(TrI,vt6(1,j,1,KPhase),VTPom,3,3,1)
          do k=1,3
            if(abs(anint(VTPom(k))-VTPom(k)).gt..0001) go to 1700
          enddo
          OK(j)=.true.
1700    continue
      endif
      i=i+1
      if(i.le.NLattVec(KPhase)) go to 1100
9999  deallocate(OK)
      return
      end
