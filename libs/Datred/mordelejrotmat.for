      subroutine MorDelejRotMat(uh,s)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension protmat(3,3),protmat2(3,3),s(3)
      real jmcs
      csuh=cos(uh*ToRad)
      snuh=sin(uh*ToRad)
      jmcs=1.-csuh
      protmat(1,1)=csuh+jmcs*s(1)**2
      protmat(1,2)=-snuh*s(3)+jmcs*s(1)*s(2)
      protmat(1,3)=snuh*s(2)+jmcs*s(1)*s(3)
      protmat(2,1)=snuh*s(3)+jmcs*s(2)*s(1)
      protmat(2,2)=csuh+jmcs*s(2)**2
      protmat(2,3)=-snuh*s(1)+jmcs*s(2)*s(3)
      protmat(3,1)=-snuh*s(2)+jmcs*s(3)*s(1)
      protmat(3,2)=snuh*s(1)+jmcs*s(3)*s(2)
      protmat(3,3)=csuh+jmcs*s(3)**2
      call multm(protmat,rotmat,protmat2,3,3,3)
      do i=1,3
        do  j=1,3
          rotmat(i,j)=protmat2(i,j)
        enddo
      enddo
      return
      end
