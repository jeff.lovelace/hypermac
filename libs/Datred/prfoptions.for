      subroutine PrfOptions
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'profil.cmn'
      logical CrwLogicQuest,LTrshld
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,180.,8,'Profile drawing option',0,
     1                   LightGray,0,0)
      il=1
      call FeQuestCrwMake(id,15.,il,5.,il,'check reflections for '//
     1                    '%splitting','L',CrwXd,CrwYd,1,0)
      nCrwSplit=CrwLastMade
      call FeQuestCrwOpen(nCrwSplit,CheckSplit)
      il=il+1
      call FeQuestCrwMake(id,15.,il,5.,il,'check reflections for '//
     1                    '%broadering','L',CrwXd,CrwYd,1,0)
      nCrwBroad=CrwLastMade
      call FeQuestCrwOpen(nCrwBroad,CheckBroad)
      il=il+1
      xEdw=15.
      tEdw=xEdw+40.
      call FeQuestEdwMake(id,tEdw,il,xEdw,il,'back%ground/scan length '
     1                  //'ratio','L',35.,EdwYd,0)
      nEdwRatioBS=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,tEdw,il,xEdw,il,'%peak/scan length ratio',
     1                    'L',35.,EdwYd,0)
      nEdwRatioPS=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,tEdw,il,xEdw,il,'s%ignificant level for '//
     1                    'peak','L',35.,EdwYd,0)
      nEdwSTrshld=EdwLastMade
      if(CheckBroad) call FeQuestRealEdwOpen(nEdwRatioPS,RatioPS,
     1                                       .false.,.true.)
      if(CheckSplit.or.CheckBroad) then
        call FeQuestRealEdwOpen(nEdwRatioBS,RatioBS,.false.,.true.)
        call FeQuestRealEdwOpen(nEdwSTrshld,strshld,.false.,.false.)
        LTrshld=.true.
      else
        LTrshld=.false.
      endif
      il=il+1
      call FeQuestCrwMake(id,15.,il,5.,il,'check reflections for '//
     1                    '%displacement','L',CrwXd,CrwYd,1,0)
      nCrwDeviated=CrwLastMade
      call FeQuestCrwOpen(nCrwDeviated,CheckDeviated)
      il=il+1
      call FeQuestEdwMake(id,tEdw,il,xEdw,il,'maximal %omega difference'
     1                   ,'L',35.,EdwYd,0)
      nEdwDeviated=EdwLastMade
      if(CheckDeviated) call FeQuestRealEdwOpen(nEdwDeviated,DevLim,
     1                                          .false.,.false.)
      il=il+1
      call FeQuestCrwMake(id,15.,il,5.,il,'Next/Back concerns only '//
     1                    '"s%uspect" reflections','L',CrwXd,CrwYd,0,0)
      nCrwSpatne=CrwLastMade
      call FeQuestCrwOpen(nCrwSpatne,JenSpatne)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.EventNumber.eq.nCrwDeviated) then
        if(CrwLogicQuest(nCrwDeviated)) then
          call FeQuestRealEdwOpen(nEdwDeviated,DevLim,.false.,.false.)
          EventType=EventEdw
          EventNumber=nEdwDeviated
        else
          call FeQuestEdwClose(nEdwDeviated)
        endif
        go to 1500
      else if(CheckType.eq.EventCrw) then
        if(CrwLogicQuest(nCrwBroad).and.EventNumber.eq.nCrwBroad) then
          call FeQuestRealEdwOpen(nEdwRatioPS,RatioPS,.false.,.true.)
        else
          call FeQuestEdwClose(nEdwRatioPS)
        endif
        if(CrwLogicQuest(nCrwSplit).or.CrwLogicQuest(nCrwBroad)) then
          if(.not.LTrshld) then
            call FeQuestRealEdwOpen(nEdwRatioBS,RatioBS,.false.,.true.)
            call FeQuestRealEdwOpen(nEdwSTrshld,strshld,.false.,.false.)
            EventType=EventEdw
            EventNumber=nEdwRatioBS
            LTrshld=.true.
          endif
          EventType=EventEdw
          EventNumber=nEdwRatioBS
          go to 1500
        else
          if(LTrshld) then
            call FeQuestEdwClose(nEdwRatioBS)
            call FeQuestEdwClose(nEdwSTrshld)
            LTrshld=.false.
          endif
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        CheckSplit=CrwLogicQuest(nCrwSplit)
        CheckBroad=CrwLogicQuest(nCrwBroad)
        CheckDeviated=CrwLogicQuest(nCrwDeviated)
        JenSpatne=CrwLogicQuest(nCrwSpatne)
        if(LTrshld) then
          call FeQuestRealFromEdw(nEdwRatioBS,RatioBS)
          if(CheckBroad) call FeQuestRealFromEdw(nEdwRatioPS,RatioPS)
          call FeQuestRealFromEdw(nEdwSTrshld,strshld)
        endif
        if(CheckDeviated) call FeQuestRealFromEdw(nEdwDeviated,DevLim)
      endif
      call FeQuestRemove(id)
      return
      end
