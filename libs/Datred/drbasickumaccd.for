      subroutine DRBasicKumaCCD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(6)
      character*256 Veta,Radka,FileName,EdwStringQuest
      character*40 :: SumString(7) =
     1             (/'U - MATRIX:                         ',
     2               'UB - MATRIX:                        ',
     3               'WAVELENGTH (ANG):                   ',
     4               'MONOCHROMATOR                       ',
     5               'Incommensurate integration: q =     ',
     6               'Incommensurate crystal sorting      ',
     7               'Incommensurate/quasi-crystal sorting'/)
      integer Vyskyt,TypeFile
      logical WizardModeIn,ExistFile,UzJiMa
      UzJiMa=.false.
      WizardModeIn=WizardMode
      FormatRefBlock(KRefBlock)=
     1  '(.i4,2f8.4,i4,6f8.5,2i5,f7.4,3z7,2f11.5)'
      Klic=0
      call DRTestNDim(NDim95(KRefBlock))
      write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim95(KRefBlock)
      read(71,FormA) Radka
      rewind 71
c      idl=idel(Radka)-(NDim95(KRefBlock)+1)*4-16
c      if(idl.gt.0.and.idl.ne.48)
c     1  FormatRefBlock(KRefBlock)=FormatRefBlock(KRefBlock)(:14)//
c     2                            '6f8.5)'
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      idl=idel(Radka)
      i=4*NDim95(KRefBlock)+20
      if(idl.le.i) then
        TypeHKLKuma=0
      else if(idl.le.i+20) then
        TypeHKLKuma=1
        FormatRefBlock(KRefBlock)(15:)='2f10.4)'
        PocitatDirCos=.true.
        DirCosFromPsi=.true.
      else
        TypeHKLKuma=0
      endif
      rewind 71
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      call GetPureFileName(SourceFileRefBlock(KRefBlock),FileName)
      TypeFile=2
      FileName=FileName(:idel(FileName))//'.cif_od'
      if(SilentRun) go to 1400
1150  WizardMode=.false.
      id=NextQuestId()
      xqd=400.
      il=3
      Veta='Define basic input file:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+5.
      Veta='input from "%sum" file'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        if(i.eq.1) then
          Veta='input from "%cif_od" file'
          nCrwSum=CrwLastMade
        else
          nCrwCif=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
      enddo
      il=il+1
      Veta='%File name:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      Radka='%Browse'
      dpomb=FeTxLengthUnder(Radka)+20.
      dpom=xqd-xpom-dpomb-15.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      xpom=xpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,Radka)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        FileName=EdwStringQuest(nEdwFileName)
        if(.not.ExistFile(FileName)) then
          Veta='file "'//FileName(:idel(FileName))//
     1         '" doesn''t exist, try again.'
          call FeChybne(-1.,250.,Veta,' ',SeriousError)
          go to 1200
        endif
        QuestCheck(id)=0
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwSum.or.CheckNumber.eq.nCrwCif)) then
        Veta=EdwStringQuest(nEdwFileName)
        call GetPureFileName(Veta,Radka)
        i=LocateSubstring(Veta,'_red',.false.,.true.)
        if(i.gt.0.and.i.eq.idel(Radka)-3) Radka(i:)=' '
        if(CheckNumber.eq.nCrwSum) then
          TypeFile=1

          Radka=Radka(:idel(Radka))//'_red.sum'
        else
          TypeFile=2
          Radka=Radka(:idel(Radka))//'.cif_od'
        endif
        call FeQuestStringEdwOpen(nEdwFileName,Radka)
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        call FeFileManager('Select file with orientation matrix',
     1                     FileName,'*.sum',0,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwFileName,FileName)
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      if(ich.ne.0) then
        ErrFlag=1
        go to 9999
      endif
1400  call CheckEOLOnFile(FileName,2)
      if(ErrFlag.ne.0) go to 9999
      call OpenFile(70,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      go to 1450
      entry DRBasicKumaCCDSeparately
      Klic=1
      TypeFile=1
1450  Vyskyt=0
      if(TypeFile.eq.1) then
1500    read(70,FormA,end=7000) radka
        if(Radka.eq.' ') go to 1500
        if(LocateSubstring(Radka,SumString(1)(:idel(SumString(1))),
     1                     .false.,.true.).gt.0.or.
     2     LocateSubstring(Radka,SumString(2)(:idel(SumString(2))),
     3                     .false.,.true.).gt.0) then
          do i=1,3
1510        read(70,FormA,end=9000) radka
            if(Radka.eq.' ') go to 1510
            k=1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9000
            do j=1,3
              ub(i,j,KRefBlock)=xp(j)
            enddo
          enddo
          m=0
          do i=1,2
1520        read(70,FormA,end=9000) radka
            if(Radka.eq.' ') go to 1520
            do j=1,idel(Radka)
              if(Radka(j:j).eq.'('.or.Radka(j:j).eq.')') Radka(j:j)=' '
            enddo
            k=0
            call StToReal(Radka,k,xp,6,.false.,ich)
            if(ich.ne.0) then
              call SetRealArrayTo(CellRefBlock  (1,KRefBlock),6,0.)
              call SetRealArrayTo(CellRefBlockSU(1,KRefBlock),6,0.)
              go to 1530
            endif
            l=0
            do j=1,3
              m=m+1
              l=l+1
              CellRefBlock(m,KRefBlock)=xp(l)
              l=l+1
              CellRefBlockSU(m,KRefBlock)=xp(l)
            enddo
          enddo
          UzJiMa=.true.
1530      Vyskyt=Vyskyt+1
          go to 1500
        else if(LocateSubstring(Radka,SumString(3)(:idel(SumString(3))),
     1                          .false.,.true.).eq.1.and.
     2          nref95(KRefBlock).le.0) then
          LamA1RefBlock(KRefBlock)=0.
          LamA2RefBlock(KRefBlock)=0.
          LamAveRefBlock(KRefBlock)=0.
          k=idel(SumString(3))+1
          kt=0
1620      call kus(radka,k,cislo)
          if(cislo.eq.'A1') then
            kt=1
          else if(cislo.eq.'A2') then
            kt=2
          else
            if(kt.ne.0) call posun(cislo,1)
            if(kt.eq.1) read(cislo,100,err=9000)
     1                    LamA1RefBlock(KRefBlock)
            if(kt.eq.2) read(cislo,100,err=9000)
     1                    LamA2RefBlock(KRefBlock)
            kt=0
          endif
          if(k.lt.len(radka)) go to 1620
          if(LamA1RefBlock(KRefBlock).le.0.)then
            TextInfo(1) = 'Information about wavelength '//
     1        'not found.'
            TextInfo(2) = 'Cannot calculate cell parameters'
            NInfo = 2
            call FeInfoOut(-1.,-1.,'Warning','L')
          else
            if(LamA2RefBlock(KRefBlock).le.0.) then
              LamAveRefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
              k=LocateInArray(LamAveRefBlock(NRefBlock),LamAveD,7,.0001)
              if(k.gt.0) then
                LamAveRefBlock(KRefBlock)=LamAveD(k)
                LamA1RefBlock(KRefBlock)=LamA1D(k)
                LamA2RefBlock(KRefBlock)=LamA2D(k)
                KLamRefBlock(NRefBlock)=k
              else
                LamA2RefBlock(KRefBlock)=LamA1RefBlock(KRefBlock)
              endif
            else
              LamAveRefBlock(KRefBlock)=(2.*LamA1RefBlock(KRefBlock)+
     1                                      LamA2RefBlock(KRefBlock))/3.
            endif
          endif
          go to 1500
        else if(LocateSubstring(Radka,SumString(4)(:idel(SumString(4))),
     1                          .false.,.true.).eq.1.and.
     2          nref95(KRefBlock).le.0) then
          k=idel(SumString(4))+1
          if(Radka(k:k).ne.' ') go to 1500
          ipis=0
1640      call kus(radka,k,cislo)
          if(cislo.eq.'(DEG)') then
            ipis=1
          else
            if(ipis.ne.1) go to 1640
            call posun(cislo,1)
            read(cislo,100,err=9000) AngleMonRefBlock(KRefBlock)
          endif
          if(k.lt.len(radka)) go to 1640
        else if(LocateSubstring(Radka,SumString(5)(:idel(SumString(5))),
     1                          .false.,.true.).eq.1) then
          k=idel(SumString(5))+1
          do i=1,3
            call kus(Radka,k,Cislo)
            call posun(Cislo,1)
            read(Cislo,100,err=9000) QuRefBlock(i,1,KRefBlock)
            call kus(Radka,k,Cislo)
            call kus(Radka,k,Cislo)
          enddo
        else if(LocateSubstring(Radka,SumString(6)(:idel(SumString(6))),
     1                          .false.,.true.).gt.0.or.
     2          LocateSubstring(Radka,SumString(7)(:idel(SumString(7))),
     3                          .false.,.true.).gt.0) then
          i=LocateSubstring(Radka,'q1:',.false.,.true.)+3
          j=LocateSubstring(Radka(i:),',',.false.,.true.)+i-2
          k=0
          call StToReal(Radka(i:j),k,xp,3,.false.,ich)
          if(ich.eq.0) call CopyVek(xp,QuRefBlock(1,1,KRefBlock),3)
        endif
        go to 1500
      else
        call DRReadBasicCIF(0)
        if(ErrFlag.ne.0) go to 9999
      endif
7000  if(Vyskyt.gt.1.and.Klic.eq.0) then
        write(Cislo,FormI15) Vyskyt
        call Zhusti(Cislo)
        Radka='Your sum file consists of '//Cislo(:idel(Cislo))//
     1        ' appended data reduction runs.'
        call FeChybne(-1.,-1.,Radka,'DATRED will use the last run.',
     1                Warning)
      endif
      do i=1,3
        do j=1,3
          if(LamA1RefBlock(KRefBlock).gt.0.)
     1      ub(i,j,KRefBlock)=ub(i,j,KRefBlock)/
     2                        LamA1RefBlock(KRefBlock)
        enddo
      enddo
      if(TypeFile.eq.1.and..not.UzJiMa) call DRCellFromUB
      go to 9999
9000  call FeReadError(70)
      go to 9900
9900  ErrFlag=1
      go to 9999
9999  call CloseIfOpened(70)
      return
100   format(f15.0)
      end
