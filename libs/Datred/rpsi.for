      function RPsi(oo,ok,op)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      f11=cos(oo)*cos(ok)*sb*cb*ca**2*(cos(op)-1)
      f=-sin(op)*cos(oo)*sin(ok)*sb
      h=sin(oo)*sin(ok)*sb*cb*(1-cos(op))
      f12=ca*(cos(oo)*sa*(cos(ok)-1)*(cb**2+cos(op)*sb**2)+h+f)
      f13=cos(oo)*sb*cb*sa**2*(cos(op)-1)
      f14=sin(oo)*sin(ok)*sa*(cb**2+cos(op)*sb**2)
      f15=sin(op)*sin(oo)*cos(ok)*sb
      f21=ca**2*(cb**2+cos(op)*sb**2)
      f22=sb*cb*sa*ca*(cos(op)-1)*(cos(ok)-1)
      f23=sa*(cos(ok)*sa*(cb**2+cos(op)*sb**2)-sin(op)*sin(ok)*sb)
      f1=f11+f12+f13-f14-f15
      f2=f21+f22+f23
      RPsi=AngKuma(f1,f2)
      return
      end
