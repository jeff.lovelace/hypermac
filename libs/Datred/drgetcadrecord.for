      subroutine DRGetCADRecord(Veta,Ktera,Klic,*,*)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Veta,VetaOld
      data VetaOld/' '/
      if(Klic.eq.1) then
        rewind 71
        VetaOld=' '
      endif
1000  if(VetaOld.eq.' ') then
        read(71,FormA80,err=3000,end=2000) Veta
        if(ichar(Veta(1:1)).lt.32) Veta=Veta(2:)
        if(Veta.eq.' ') go to 1000
        if(idel(Veta).le.3) go to 2000
        if(Veta(1:1).eq.'$') go to 2000
        if(jentri) Veta=' '//Veta
      else
        Veta=VetaOld
        VetaOld=' '
      endif
      k=0
      call kus(Veta(:4),k,Cislo)
      call posun(Cislo,0)
      read(Cislo,FormI15,err=3000) k
      if(k.eq.Ktera) go to 1500
      if(Ktera.eq.19.and.k.eq.1) then
        VetaOld=Veta
        Veta=' '
      else
        go to 1000
      endif
1500  return
2000  return 1
3000  return 2
      end
