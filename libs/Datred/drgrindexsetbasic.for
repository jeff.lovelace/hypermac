      subroutine DRGrIndexSetBasic
      use DRIndex_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      Zoom=1.
      GrIndexUseSLim=.false.
      GrIndexUseILim=.false.
      GrIndexIMin=0.
      GrIndexSMin=0.
      GrIndexIMax=100.
      GrIndexSMax=1.
      LatticeOn=.false.
      call SetRealArrayTo(CellRefBlock(1,KRefBlock),6,-1.)
      GrIndexSkipIndexed=.false.
      Index2d=.false.
      NCellMax=3
      IH2d=0
      return
      end
