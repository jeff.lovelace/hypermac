      function InPhi(xin)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      x=xin
      if(x.gt.360.) x=x-360.
      if(x.lt.  0.) x=x+360.
      n=x*0.1+1.
      InPhi=nint((PCorrKuma(n+1)-(float(n)-0.1*x)*
     1            (PCorrKuma(n+1)-PCorrKuma(n)))*DegKuma(4))
      return
      end
