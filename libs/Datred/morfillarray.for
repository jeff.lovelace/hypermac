      subroutine MorFillArray(FacesNotOK,ctipl)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension rmp(3,3),rmi(3,3),xp(3),ps(3),NotOKList(15),
     1          plist(100,100)
      logical chyba,Novy,eqrv,FacesNotOK,ctipl,plist,CrwLogicQuest
      integer PocHr,NotOKList
      character*50 ErrSt,ErrMsg,pomchar
500   do i=1,100
        do j=1,100
          BodRov(i,j)=.false.
          plist(i,j)=.false.
        enddo
      enddo
      call SetIntArrayTo(BodHr,10000,0)
      PocetBodu=0
      FacesNotOK=.false.
      ctipl=.true.
      do i=1,NFaces(KRefBlock)-2
        ps(1)=krov(4,i)
        do m=1,3
          rmp(1,m)=krov(m,i)
        enddo
        do j=i+1,NFaces(KRefBlock)-1
          ps(2)=krov(4,j)
          do m=1,3
            rmp(2,m)=krov(m,j)
          enddo
          do k=j+1,NFaces(KRefBlock)
            do m=1,3
              rmp(3,m)=krov(m,k)
            enddo
            call matinv(rmp,rmi,dets,3)
            if(abs(dets).gt..00001) then
              ps(3)=krov(4,k)
              call multm(rmi,ps,xp,3,3,1)
              chyba=.false.
              do n=1,NFaces(KRefBlock)
                skals=ScalMul(krov(1,n),xp)
                if(abs(skals).gt..00001) then
                  t=krov(4,n)/skals
                else
                  t=0.
                endif
                if(t.lt.0.99999.and.t.gt..00001) then
                  chyba=.true.
                  go to 1210
                endif
              enddo
1210          if(.not.chyba) then
                Novy=.true.
                do l=1,PocetBodu
                  if(eqrv(xp,bod(1,l),3,.001*sqrt(ScalMul(xp,xp))))
     1             then
                    Novy=.false.
                    bodrov(l,i)=.true.
                    bodrov(l,j)=.true.
                    bodrov(l,k)=.true.
                    plist(i,l)=.true.
                    plist(j,l)=.true.
                    plist(k,l)=.true.
                  endif
                enddo
                if(Novy) then
                  PocetBodu=PocetBodu+1
                  call CopyVek(xp,bod(1,PocetBodu),3)
                  bodrov(PocetBodu,i)=.true.
                  bodrov(PocetBodu,j)=.true.
                  bodrov(PocetBodu,k)=.true.
                  plist(i,PocetBodu)=.true.
                  plist(j,PocetBodu)=.true.
                  plist(k,PocetBodu)=.true.
                endif
              endif
            endif
          enddo
        enddo
      enddo
      NotOKNr=0
      do i=1,NFaces(KRefBlock)
        k=0
        do j=1,PocetBodu
          if(plist(i,j)) k=k+1
        enddo
        if(k.lt.3) then
          NotOKNr=NotOKNr+1
          NotOkList(NotOKNr)=i
          FacesNotOk=.true.
        endif
      enddo
      if(FacesNotOK) then
        ErrSt='Too large d for the following face'
        if(notoknr.gt.1) then
          ErrSt=ErrSt(:31)//'s'
          ErrMsg='Please change their d''s or delete them'
        else
          ErrMsg='Please change its d or delete it'
        endif
        id=NextQuestId()
        xqd=300.
        call FeQuestCreate(id,-1.,-1.,xqd,NotOKNr+2,ErrSt,0,LightGray,
     1                     1,0)
        il=1
        call FequestLblMake(id,xqd*.5,il,ErrMsg,'C','B')
        il=il+1
        tpom=45.
        call FeQuestLblMake(id,tpom,il,'Indices','C','N')
        tpom=120.
        call FeQuestLblMake(id,tpom,il,'Delete face','C','N')
        tpom=235.
        call FeQuestLblMake(id,tpom,il,'Changed d','C','N')
        tpom=25.
        xpom=110.
        dpom=80.
        xpome=200.
        do iw=1,NotOKNr
          il=il+1
          write(pomchar,'(3f5.0)')(dface(i,notoklist(iw),KRefBlock),
     1                             i=1,3)
          call ZdrcniCisla(pomchar,3)
          PomChar='['//PomChar(:idel(PomChar))//']'
          do i=1,idel(PomChar)
            if(PomChar(i:i).eq.' ') PomChar(i:i)=','
          enddo
          call FequestLblMake(id,tpom,il,pomchar,'L','N')
          call FeQuestCrwMake(id,xpom,il,xpom,il,' ','L',CrwXd,CrwYd,1,
     1                        0)
          if(iw.eq.1) nCrwFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,.false.)
          call FeQuestEdwMake(id,xpome,il,xpome,il,' ','L',dpom,EdwYd,0)
          if(iw.eq.1) nEdwFirst=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,getmaxd(notoklist(iw)),
     1                            .false.,.false.)
        enddo
2240    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw) then
          i=CheckNumber-nCrwFirst+1
          nEdw=nEdwFirst+CheckNumber-nCrwFirst
          if(CrwLogicQuest(CheckNumber)) then
            call FeQuestEdwClose(nEdw)
          else
            call FeQuestRealEdwOpen(nEdw,getmaxd(notoklist(i)),
     1                              .false.,.false.)
          endif
          go to 2240
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2240
        endif
2260    if(ich.eq.0) then
          ctipl=.false.
          nCrw=nCrwFirst
          nEdw=nEdwFirst
          do iw=1,NotOKNr
            if(CrwLogicQuest(nCrw)) then
              if(NotOKList(nCrw).lt.NFaces(KRefBlock)) then
                do i=NotOkList(nCrw)+1,NFaces(KRefBlock)
                  call CopyVek(dface(1,i,KRefBlock),
     1                         dface(1,i-1,KRefBlock),4)
                enddo
                do i=iw,NotOkNr
                  NotOKList(i)=NotOkList(i)-1
                enddo
              endif
              NFaces(KRefBlock)=NFaces(KRefBlock)-1
            else
              call FeQuestRealFromEdw(nEdw,
     1                                dface(4,NotOKList(iw),KRefBlock))
            endif
            nCrw=nCrw+1
            nEdw=nEdw+1
          enddo
        endif
        call FeQuestRemove(id)
      else
        PocetHran=0
        do i=1,PocetBodu-1
          do 2400j=i+1,PocetBodu
            n=0
            BodHr(i,j)=0
            do k=1,NFaces(KRefBlock)
              if(BodRov(i,k).and.BodRov(j,k)) then
                n=n+1
                if(n.gt.1) then
                  BodHr(i,j)=1
                  PocetHran=PocetHran+1
                  go to 2400
                endif
              endif
            enddo
2400      continue
        enddo
        do i=1,NFaces(KRefBlock)
          PocHr=0
          do j=1,PocetBodu-1
            if(BodRov(j,i)) then
              do k=j+1,PocetBodu
                if(BodRov(k,i).and.BodHr(j,k).gt.0) then
                  PocHr=PocHr+1
                  RovHr1(i,PocHr)=j
                  RovHr2(i,PocHr)=k
                endif
              enddo
            endif
          enddo
          RovHr1(i,PocHr+1)=0
          RovHr2(i,PocHr+1)=0
        enddo
        call MorUzavreny(FacesNotOK,ctipl)
      endif
      return
      end
