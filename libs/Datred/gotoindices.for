      subroutine GoToIndices(nref,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'profil.cmn'
      dimension ihr(6)
      logical eqiv
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,YBottomMessage,1,
     1                   'Indices of the reflection to be drawn',1,
     2                   LightGray,0,0)
      call FeQuestEdwMake(id,0.,0,30.,1,' ','C',60.,EdwYd,0)
      nEdw=EdwLastMade
      call FeQuestIntAEdwOpen(EdwLastMade,ih,NDim(KPhase),.true.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.EventNumber.eq.ButtonOk) then
        call FeQuestIntAFromEdw(nEdw,ihr)
        rewind 95
        do i=1,nref95(KRefBlock)
          call DRGetReflectionFromM95(95,iend,ich)
          if(iend.ne.0) go to 2100
          if(eqiv(ih,ihr,NDim(KPhase))) then
            nref=i
            QuestCheck(id)=0
            go to 1500
          endif
        enddo
2100    call FeChybne(-1.,-1.,'Reflecion not found, try again.',' ',
     1                SeriousError)
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      return
      end
