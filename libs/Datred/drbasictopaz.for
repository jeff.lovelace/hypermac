      subroutine DRBasicTopaz
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      AngleMonRefBlock(KRefBlock)=0.
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      DirCosFromPsi=.false.
      LamAveRefBlock(KRefBlock)=-1.
      NDim95(KRefBlock)=3
      FormatRefBlock(KRefBlock)=
     1  '(3i4,2f8.2,i4,2f8.5,6f9.5,2i6,f7.4,i4,f9.5,f8.4,2f7.2)'
      PocitatDirCos=.false.
      DirCosFromPsi=.false.
      return
      end
