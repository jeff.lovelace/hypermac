      subroutine DRSetRunScGrN(ich)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*80 Veta
      ich=0
      n2=0
      n=0
      m2=0
      m=0
      if(allocated(RunScGrN)) deallocate(RunScGrN)
      allocate(RunScGrN(RunCCDMax(KRefBlock),NRefBlock))
1100  m=m+1
      if(m.gt.NRuns(KRefBlock)) go to 2000
      m1=m2+1
      m2=m2+RunNFrames(m,KRefBlock)
      nn=max(float(RunNFrames(m,KRefBlock))/
     1       float(RunScGr(KRefBlock)),1.)
      nn=float(RunNFrames(m,KRefBlock))/float(nn)
1200  n=n+1
      n1=n2+1
      n2=n2+nn
      if(n1.gt.m2) then
        n=n-1
        n2=n2-nn
        go to 1100
      else if(n2.le.m2) then
        if(n.gt.MxFrames) then
          ich=1
          go to 9999
        endif
        RunScGrN(n1:n2,KRefBlock)=n
        RunScGrM(n,KRefBlock)=n2-n1+1
        if(n2.eq.m2.and.ScFrMethod(KRefBlock).eq.1) then
          n=n+1
          RunScGrM(n,KRefBlock)=0
        endif
        go to 1200
      else if(n2.gt.m2) then
        n=n-1
        RunScGrN(n1:m2,KRefBlock)=n
        RunScGrM(n,KRefBlock)=RunScGrM(n,KRefBlock)+m2-n1+1
        n2=m2
        if(ScFrMethod(KRefBlock).eq.1) then
          n=n+1
          RunScGrM(n,KRefBlock)=0
        endif
        go to 1100
      endif
2000  RunScN(KRefBlock)=n
      RunScGrM0(1,KRefBlock)=1
      do i=2,n
        RunScGrM0(i,KRefBlock)=RunScGrM0(i-1,KRefBlock)+
     1                         RunScGrM(i-1,KRefBlock)
      enddo
9999  return
      end
