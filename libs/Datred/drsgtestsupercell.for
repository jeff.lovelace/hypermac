      subroutine DRSGTestSuperCell(CellStart,CellTrStart,CrSystemStart,
     1                             Klic,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      common/TestSuperCell/ CellFound(6,10),TrFound(36,10),nSbw,XShow,
     1                      YShow,CrSystemFound(10),NSupCellShown
      dimension CellPom(6),TrPom(3,3),CellStart(6),CellTrStart(36),
     1          Tr(36),TrPomP(3,3)
      character*80 Veta
      integer CrSystemStart,CrSystemPom,CrSystemAct,CrSystemFromCell,
     1        SbwLnQuest,SbwItemPointerQuest,UseTabsIn,CrSystemMax,
     2        CrSystemFound
      logical UseOld
      external TestSuperCellCheck,FeVoid
      save /TestSuperCell/
      UseOld=abs(DiffAxe  -DiffAxeOld  ).lt..0001.and.
     1       abs(DiffAngle-DiffAngleOld).lt..001
      if(UseOld) go to 3050
      call SetRealArrayTo(TrPom,9,0.)
      if(CrSystemStart.le.0)
     1  CrSystemStart=mod(CrSystemFromCell(CellStart,DiffAxe,DiffAngle),
     2                    10)
      CrSystemMax=CrSystemStart
      csa=cos(CellStart(4)*ToRad)
      csb=cos(CellStart(5)*ToRad)
      csg=cos(CellStart(6)*ToRad)
      VolStart=CellStart(1)*CellStart(2)*CellStart(3)*
     1         sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      NSupCell=0
      call FeMouseShape(3)
      do i=2,9
        CrSystemAct=CrSystemMax
        if(i.eq.8) then
          jk=4
        else if(i.eq.6) then
          jk=3
        else if(i.eq.4.or.i.eq.9) then
          jk=2
        else
          jk=1
        endif
        do j=1,jk
          if(j.eq.1) then
            IPom1=i
            IPom2=1
            IPom3=1
          else if(j.eq.2) then
            if(i.eq.4) then
              IPom1=1
              IPom2=2
              IPom3=2
            else if(i.eq.6) then
              IPom1=1
              IPom2=2
              IPom3=3
            else if(i.eq.8) then
              IPom1=1
              IPom2=2
              IPom3=4
            else if(i.eq.9) then
              IPom1=1
              IPom2=3
              IPom3=3
            endif
          else if(j.eq.3) then
            if(i.eq.6) then
              IPom1=1
              IPom2=3
              IPom3=2
            else if(i.eq.8) then
              IPom1=1
              IPom2=4
              IPom3=2
            endif
          else if(j.eq.4) then
            IPom1=2
            IPom2=2
            IPom3=2
          endif
          do k=1,3
            do l=1,3
              if(l.eq.k) then
                TrPom(l,l)=IPom1
              else if(l.eq.mod(k,3)+1) then
                TrPom(l,l)=IPom2
              else if(l.eq.mod(k+1,3)+1) then
                TrPom(l,l)=IPom3
              endif
            enddo
            i21p=0
            i21k=nint(TrPom(2,2))-1
            i31p=0
            i31k=nint(TrPom(3,3))-1
            i32p=0
            i32k=nint(TrPom(3,3))-1
            do i21=i21p,i21k
              TrPom(2,1)=i21
              do i31=i31p,i31k
                TrPom(3,1)=i31
                do 1200i32=i32p,i32k
                  TrPom(3,2)=i32
                  call CopyVek(CellStart,CellPom,6)
                  call UnitMat(Tr,NDim(KPhase))
                  call DRMatTrCell(TrPom,CellPom,Tr)
                  call DRCellReduction(CellPom,Tr)
                  kk=CrSystemFromCell(CellPom,DiffAxe,DiffAngle)
                  io=iabs(kk)/10
                  if(io.ne.0.and.kk.ne.CrSystemMonoclinic) then
                    if(io.eq.1) then
                      m=231
                    else if(io.eq.2) then
                      m=312
                    endif
                    call SetPermutMat(TrPomP,3,231,ich)
                    call DRMatTrCell(TrPomP,CellPom,Tr)
                    kk=mod(iabs(kk),10)*isign(1,kk)
                  endif
                  if(kk.lt.0) then
                    call UnitMat(TrPomP,3)
                    TrPomP(2,2)=-1.
                    TrPomP(3,3)=-1.
                    call DRMatTrCell(TrPomP,CellPom,Tr)
                    kk=-kk
                  endif
                  CrSystemPom=kk
                  if(CrSystemPom.gt.CrSystemAct.and.NSupCell.le.10)
     1              then
                    if(NSupCell.lt.10) then
                      NSupCell=NSupCell+1
                    else
                      if(CrSystemPom.le.CrSystemFound(NSupCell))
     1                  go to 1200
                    endif
                    call CopyVek(CellPom,CellFound(1,NSupCell),6)
                    call CopyMat(Tr,TrFound(1,NSupCell),NDim(KPhase))
                    CrSystemFound(NSupCell)=CrSystemPom
                    CrSystemMax=max(CrSystemMax,CrSystemPom)
                  endif
1200            continue
              enddo
            enddo
            if(IPom1.eq.IPom2.and.IPom1.eq.IPom3) exit
          enddo
        enddo
      enddo
      call FeMouseShape(0)
      NSupCellSel=NSupCell+1
3050  if(NSupCell.gt.0) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        xpom=10.
        do i=1,6
          call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
          if(i.le.3) then
            xpom=xpom+45.
          else
            xpom=xpom+40.
          endif
        enddo
        xpom=250.
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+1.5*PropFontWidthInPixels
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+70.
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+1.5*PropFontWidthInPixels
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        id=NextQuestId()
        Veta='Select supercell'
        if(WizardMode) then
          call FeQuestTitleMake(id,Veta)
          xqd=WizardLength
        else
          il=15
          xqd=500.
          call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        endif
        il=1
        Veta='Cell'
        xpom=150.
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        Veta='n*Volume'
        xpom=280.
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        Veta='System'
        xpom=380.
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_cells.tmp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do i=1,NSupCell
          il=il+1
          csa=cos(CellFound(4,i)*ToRad)
          csb=cos(CellFound(5,i)*ToRad)
          csg=cos(CellFound(6,i)*ToRad)
          Vol=CellFound(1,i)*CellFound(2,i)*CellFound(3,i)*
     1        sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
          write(Cislo,'(i2,''*'',f10.2)') nint(Vol/VolStart),VolStart
          call Zhusti(Cislo)
          k=mod(CrSystemFound(i),10)
          write(Veta,'(3(a1,f7.3),3(a1,f7.2),2(a1,''|'',a1,a))')
     1      (Tabulator,CellFound(j,i),j=1,6),Tabulator,Tabulator,
     2       Cislo(:idel(Cislo)),Tabulator,Tabulator,CrSystemName(k)
          write(ln,FormA) Veta(:idel(Veta))
        enddo
        if(Klic.eq.0) then
          Veta='continue with the basic cell'
        else
          Veta='continue without introducing reticular twinning'
        endif
        write(ln,FormA) Tabulator//Tabulator//Tabulator//Tabulator//
     1                  Veta(:idel(Veta))
        call CloseIfOpened(ln)
        il=11
        xpom=5.
        dpom=xqd-10.-SbwPruhXd
        call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                      SbwVertical)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_cells.tmp')
        call FeQuestSbwItemOff(nSbw,SbwItemPointerQuest(nSbw))
        call FeQuestSbwItemOn(nSbw,NSupCellSel)
        YShow=QuestYPosition(id,il+4)+QuestYMin(id)+5.
        if(Klic.eq.0) then
          XShow=QuestXMin(id)+80.
        else
          XShow=QuestXMin(id)+150.
        endif
        call CrlMatEqReset
        NSupCellShown=-1
        if(Klic.eq.0) then
          il=il+2
          Veta='%Save it by matrix calculator'
          dpom=FeTxLengthUnder(Veta)+10.
          xpom=xqd-dpom-50.
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          nButtCalculator=ButtonLastMade
        else
          nButtCalculator=0
        endif
3500    call FeQuestEventWithCheck(id,ich,TestSuperCellCheck,FeVoid)
        if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCalculator)
     1    then
          if(NSupCellSel.le.NSupCell) then
            call TrMat(TrFound(1,NSupCellSel),TrPom,3,3)
          else
            call UnitMat(TrPom,3)
          endif
          call MatrixCalculatorInOut(TrPom,3,3,ichp)
          go to 3500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 3500
        endif
        if(ich.eq.0) then
          NSupCellSel=SbwItemPointerQuest(nSbw)
          if(NSupCellSel.le.NSupCell) then
            call CopyVek(CellFound(1,NSupCellSel),CellStart,6)
            call Multm(TrFound(1,NSupCellSel),CellTrStart,Tr,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call CopyMat(Tr,CellTrStart,NDim(KPhase))
            CrSystemStart=CrSystemFound(NSupCellSel)
          endif
        endif
        call CloseIfOpened(SbwLnQuest(nSbw))
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
        call CrlMatEqHide
        if(.not.WizardMode) call FeQuestRemove(id)
      else
        NInfo=1
        TextInfo(1)='No supercell having a higher cell symmetry has '//
     1              'been found.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
      endif
9999  call DeleteFile(fln(:ifln)//'_cells.tmp')
      return
      end
