      subroutine DRHKLToAngles(H,Angle)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension h(3),c(3),Angle(4)
      call multm(ub,h,c,3,3,1)
      tmp=sqrt(c(1)**2+c(2)**2+c(3)**2)
      Angle(2)=atan2(c(3),sqrt(c(1)**2+c(2)**2))/ToRad*SenseOfAngle(2)
      if(c(1).ne.0..or.c(2).ne.0.) then
        Angle(1)=atan2(-c(1),-c(2))/ToRad*SenseOfAngle(1)
      else
        Angle(1)=0.
      endif
      Angle(3)=asin(.5*LamAveRefBlock(KRefBlock)*tmp)/ToRad*
     1         SenseOfAngle(3)
      Angle(4)=Angle(3)
      if(Angle(3).lt.0.) then
        if(Angle(1).gt.0.) then
          Angle(1)=Angle(1)-180.
        else
          Angle(1)=Angle(1)+180.
        endif
        do i=2,4
          Angle(i)=-Angle(i)
        enddo
      endif
      return
      end
