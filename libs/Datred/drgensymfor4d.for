      subroutine DRGenSymFor4D(ipg,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension nx(4),ni(4),QuSymm(3,3)
      character*4 GrX4
      idl=index(Grupa(KPhase),')')
      if( ipg.le.4.or.
     1   (ipg.ge. 9.and.ipg.le.10).or.
     2   (ipg.ge.16.and.ipg.le.17).or.
     3   (ipg.ge.21.and.ipg.le.22)) then
        nn=1
      else if( ipg.eq. 5.or.
     1         ipg.eq.11.or.
     2         ipg.eq.23.or.Grupa(KPhase)(1:1).eq.'R') then
        nn=2
      else if((ipg.ge. 6.and.ipg.le. 8).or.
     1        (ipg.ge.12.and.ipg.le.14).or.
     2        (ipg.ge.18.and.ipg.le.20).or.
     3        (ipg.ge.24.and.ipg.le.26)) then
        nn=3
      else
        nn=4
      endif
      if(ipg.le.15) then
        m=3
      else
        m=5
      endif
      call SetIntArrayTo(nx,nn,m)
      n=m**nn
      do i=1,n
        call RecUnPack(i,ni,nx,nn)
        GrX4=' '
        do j=1,nn
          k=ni(j)
          GrX4(j:j)=SmbSymmT(k)
        enddo
        Grupa(KPhase)=Grupa(KPhase)(:idl)//GrX4(:nn)
        call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QuSymm,ich)
        if(ich.eq.0) go to 9999
      enddo
9999  return
      end
