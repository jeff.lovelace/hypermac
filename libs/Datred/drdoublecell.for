      subroutine DRDoubleCell(TrMPA,Cell,itr)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension t(36),td(3,3,8),Cellp(6),TrMPa(*),Cell(6)
      character*80 Veta,menc(0:7)
      integer FeMenu
      data td/1.,0.,0.,0.,1.,0.,0.,0.,1.,
     1        2.,0.,0.,0.,1.,0.,0.,0.,1.,
     2        1.,0.,0.,0.,2.,0.,0.,0.,1.,
     3        1.,1.,0.,0.,2.,0.,0.,0.,1.,
     4        1.,0.,0.,0.,1.,0.,0.,0.,2.,
     5        1.,0.,1.,0.,1.,0.,0.,0.,2.,
     6        1.,0.,0.,0.,1.,1.,0.,0.,2.,
     7        1.,0.,1.,0.,1.,1.,0.,0.,2./
      do i=0,10
        if(NTabs(i).eq.0) then
          UseTabs=i
          go to 1200
        endif
      enddo
      UseTabs=0
1200  xpom=FeTxLength('XXXXX.XXX')
      do i=1,6
        call FeTabsAdd(xpom*float(i),UseTabs,IdLeftTab,' ')
      enddo
      do i=1,8
        call CopyVek(Cell,CellP,6)
        call UnitMat(t,NDim(KPhase))
        call DRMatTrCell(td(1,1,i),CellP,t)
        call DRCellReduction(CellP,t)
        write(Veta,'(3f9.4,3f9.2)') Cellp
        k=0
        do j=1,6
          call kus(Veta,k,Cislo)
          if(j.eq.1) then
            MenC(i-1)=Tabulator//Cislo(:idel(Cislo))
          else
            MenC(i-1)=MenC(i-1)(:idel(MenC(i-1)))//Tabulator//
     1                Cislo(:idel(Cislo))
          endif
        enddo
      enddo
      menc(0)=' '
      menc(0)(20:)='return without doubling of cell parameters'
      itr=FeMenu(-1.,-1.,menc,0,7,0,0)
      if(itr.gt.0) then
        call CopyVek(Cell,CellP,6)
        call DRMatTrCell(td(1,1,itr+1),Cell,TrMPA)
        call DRCellReduction(Cell,TrMPA)
      endif
      call FeTabsReset(UseTabs)
      return
      end
