      subroutine DRExport(Key,flnp)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*(*) flnp
      character*80 OutFile,InFile,Text,OutFormat,InFormat
      logical FeYesNo,FeYesNoHeader,ExistFile,OnlyMain,First,FromM90
      equivalence (OutFile,OurFormat)
      if(flnp.eq.' ') then
        OutFile=fln(:ifln)//'.hkl'
      else
        OutFile=flnp(:idel(flnp))//'.hkl'
      endif
      FromM90=.false.
1000  if(Key.le.0) then
        Infile=fln(:ifln)//'.m95'
        Text='Exporting reflections for SHELX'
        if(Key.eq.0) then
          call FeFileManager('Select output SHELX reflection file',
     1                       OutFile,'*.hkl',0,.true.,ich)
          if(ich.ne.0) go to 9999
          if(NDim(KPhase).gt.3) then
            OnlyMain=FeYesNo(-1.,-1.,'Do you want to export only main '
     1                     //'reflections?',1)
          else
            OnlyMain=.true.
          endif
          call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
        else
          OnlyMain=.false.
          if(ExistM95.and..not.isPowder) then
            FromM90=.false.
          else
            FromM90=.true.
          endif
          if(FromM90) then
            if(isPowder) then
              Infile=fln(:ifln)//'.rfl'
              call OpenFile(95,InFile,'formatted','old')
              NDimP=NDim(KPhase)
              InFormat=FormRfl
              nrefp=0
              n=0
1100          read(95,InFormat,end=1110,err=1110)(ih(i),i=1,NDimP),
     1                                            ri,pom
              if(ri.ne.0.) then
                n0=0
              else
                n0=n0+1
              endif
              nrefp=nrefp+1
              go to 1100
1110          nrefp0=nrefp-n0
              rewind 95
            else
              Infile=fln(:ifln)//'.m90'
              call iom90(0,Infile)
              call iom50(0,0,fln(:ifln)//'.m90')
              nrefp=NRef90(KDatBlock)
              call OpenDatBlockM90(95,KDatBlock,fln(:ifln)//'.m90')
              NDimP=NDim(KPhase)
              InFormat=format91
            endif
            if(ErrFlag.ne.0) go to 9999
          else
            call iom95(0,Infile)
            KRefBlock=0
          endif
        endif
      endif
      if(ExistFile(OutFile).and.Key.ge.0) then
        NInfo=1
        TextInfo(1)='File "'//OutFile(:idel(OutFile))//
     1              '" already exists'
        if(.not.FeYesNoHeader(-1.,-1.,'Do you want to rewrite it?',0))
     1    then
          if(Key.eq.0) then
            go to 1000
          else
            go to 9999
          endif
        endif
      endif
      call OpenFile(90,OutFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      OutFormat=FormatShelx
      NDimO=NDim(KPhase)
      if(OnlyMain) then
        OutFormat(2:2)='3'
        NDimO=3
      endif
      scf=.1
      iq=1
1500  iz=0
      First=.true.
      sdircos=0.
1600  if(.not.FromM90) then
1650    KRefBlock=KRefBlock+1
        if(KRefBlock.gt.NRefBlock) go to 3100
        if(RefDatCorrespond(KRefBlock).ne.KDatBlock) go to 1650
        call OpenRefBlockM95(95,KRefBlock,fln(:ifln)//'.m95')
        nrefp=nref95(KRefBlock)
      endif
      nn=0
      call FeFlowChartOpen(-1.,-1.,max(nint(float(nrefp)*.005),10),
     1                     nrefp,Text,' ',' ')
2000  if(FromM90) then
        read(95,InFormat,end=3000,err=3000)(ih(i),i=1,NDimP),ri,pom
        if(ih(1).gt.900) go to 3000
        if(nn.ge.nrefp0) go to 3000
        if(ri.gt.0.) then
          rs=sqrt(ri)*scf
        else
          rs=1.
        endif
        ri=ri*scf
      else
        call DRGetReflectionFromM95(95,iend,ich)
        if(ich.ne.0) go to 9000
        if(iend.ne.0) go to 3000
        if(no.gt.0.and.(Key.ne.1.or.ri.gt.10.*rs)) then
          if(iflg(1).lt.0) go to 2000
          call indtr(ih,trmp,ihp,NDim(KPhase))
          if(ihp(1).gt.900) go to 2000
          call CopyVekI(ihp,ih,NDim(KPhase))
          if(Key.le.0) then
            pom=corrf(1)*corrf(2)*scf
            if(RunScN(KRefBlock).gt.0) then
              ikde=RunScGrN(RunCCD,KRefBlock)
              if(ScFrMethod(KRefBlock).eq.ScFrMethodStepLike) then
                pomp=ScFrame(ikde,KRefBlock)
              else
                c1=(expos-float(RunScGrM0(ikde,KRefBlock)))/
     1              float(RunScGrM(ikde,KRefBlock)-1)
                pomp=(1.-c1)*ScFrame(ikde,KRefBlock)+
     1               c1*ScFrame(ikde+1,KRefBlock)
              endif
              pom=pom*pomp
            endif
          else
            pom=scf
          endif
          ri=ri*pom
          rs=rs*pom
        else
          go to 2000
        endif
        if(First) then
          First=.false.
          sdircos=0.
          do i=1,3
            sdircos=sdircos+abs(dircos(i,1))
          enddo
        endif
      endif
      if(ri.gt.99999.99) then
        rewind 95
        rewind 90
        call FeFlowChartRemove
        scf=scf*.1
        KRefBlock=KRefBlock-1
        call CloseIfOpened(95)
        go to 1500
      endif
      call FeFlowChartEvent(iz,is)
      if(is.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) then
          close(90,status='delete')
          go to 9999
        endif
      endif
      if(OnlyMain) then
        do i=4,NDim(KPhase)
          if(ih(i).ne.0) go to 2000
        enddo
      endif
      if(sdircos.gt..001) then
        write(90,OutFormat)(ih(i),i=1,NDimO),ri,rs,iq,
     1                     (-dircos(i,1),dircos(i,2),i=1,3)
      else
        write(90,OutFormat)(ih(i),i=1,NDimO),ri,rs,iq
      endif
      nn=nn+1
      go to 2000
3000  if(.not.FromM90) then
        call CloseIfOpened(95)
        go to 1600
      endif
3100  write(90,OutFormat)(0,i=1,NDimO),0.,0.,0
      call FeFlowChartRemove
      if(Key.eq.1) then
        write(TextInfo(1),'(''"psi" file contains'',i5,'' reflections''
     1                      )') nn
        Ninfo=1
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
      endif
      go to 9999
9000  call FeFlowChartRemove
      call FeReadError(95)
9999  call CloseIfOpened(90)
      call CloseIfOpened(95)
      return
      end
