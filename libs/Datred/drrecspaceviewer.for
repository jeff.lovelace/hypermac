      subroutine DRRecSpaceViewer
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension h(3),hh(3),ihq(6)
      character*256 Veta
      character*60  format83a
      character*12  Jmena(4)
      real, allocatable :: riarp(:),rsarp(:),riarn(:),rsarn(:),riaro(:),
     1                     rsaro(:)
      integer, allocatable :: RefBlockArP(:),ICullArP(:),RunCCDArP(:),
     1                        HCondArP(:,:),
     2                        RefBlockArN(:),ICullArN(:),RunCCDArN(:),
     3                        HCondArN(:,:),
     4                        RefBlockArO(:),ICullArO(:),RunCCDArO(:),
     5                        HCondArO(:,:)
      integer Exponent10,SimMMax(3),SimIzMax
      logical EqIv
      data jmena/'%Quit','P%rint','%Save','Ne%w'/,SimLayerOld/999999./
      Tiskne=.false.
!      KPhaseIn=KPhase
!      KPhase=1
      call DRRecSpaceViewerDef(idal,ich)
      if(ich.ne.0) go to 9999
      idSim=NextQuestId()
      call FeQuestAbsCreate(idSim,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,
     1                      -1,-1)
      XStripWidth=100.
      call FeMakeGrWin(2.,XStripWidth,YBottomMargin,2.)
      call FeFillRectangle(XMaxBasWin-XStripWidth,XmaxBasWin,YMinGrWin,
     1                     YMaxGrWin,4,0,0,LightGray)
      call FeMakeAcWin(0.,0.,0.,0.)
      call FeBottomInfo('#prazdno#')
      xpom=50.
      ypom=YMinGrWin-10.
      Veta='Indices'
      call FeQuestAbsLblMake(idSim,xpom,YBottomText,Veta,'L','N')
      nLblInd=LblLastMade
      call FeQuestLblOff(LblLastMade)
      xpom=xpom+FeTxLength(Veta)+3.
      ypom=YMaxBasWin-20.
      dpom=3.*FeTxLength('xxxx.xxx')+2.*EdwMarginSize
      call FeWinfMake(1,0,xpom,YMinGrWin-25.,dpom,
     1                1.2*PropFontHeightInPixels)
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      dpom=ButYd+8.
      ypom=YMaxGrWin-dpom
      do i=1,4
        call FeQuestAbsButtonMake(idSim,xpom,ypom,wpom,ButYd,Jmena(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
        endif
        ypom=ypom-dpom
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      tpom=(XMaxBasWin+XMaxGrWin)*.5
      ypomn=ypom-dpom
      call FeQuestAbsEudMake(idSim,tpom,ypom+6.,xpom,ypomn,'La%yer:',
     1                       'C',wpom-10.,EdwYd,1)
      nEdwLayer=EdwLastMade
      call FeQuestIntEdwOpen(nEdwLayer,SimMainLayer,.false.)
      call FeQuestEudOpen(nEdwLayer,-5,5,1,0.,0.,0.)
      if(NDimI(KPhase).gt.0) then
        ypom =ypomn-dpom
        ypomn=ypom -dpom
        Veta='%Depth'
        call FeQuestAbsEdwMake(idSim,tpom,ypom+6.,xpom,ypomn,Veta,
     1                         'C',wpom-10.,EdwYd,1)
        nEdwDepth=EdwLastMade
        call FeQuestRealEdwOpen(nEdwDepth,SimDepth,.false.,.false.)
        do i=1,NDimI(KPhase)
          ypom =ypomn-dpom
          ypomn=ypom -dpom
          Veta='%'//indices(i+3)//' sublayer'
          call FeQuestAbsEudMake(idSim,tpom,ypom+6.,xpom,ypomn,Veta,
     1                           'C',wpom-10.,EdwYd,1)
          if(i.eq.1) nEdwSatLayer=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,SimSatLayer(i),.false.)
          call FeQuestEudOpen(EdwLastMade,-5,5,1,0.,0.,0.)
        enddo
      else
        nEdwDepth=0
        nEdwSatLayer=0
      endif
      ypom =ypomn-dpom
      ypomn=ypom -dpom
      call FeQuestAbsEudMake(idSim,tpom,ypom+6.,xpom,ypomn,
     1                       'S%cope in %:','C',wpom-10.,EdwYd,1)
      nEdwScope=EdwLastMade
      call FeQuestRealEdwOpen(nEdwScope,SimScope,.false.,.false.)
      call FeQuestEudOpen(nEdwScope,0,0,0,10.,100.,10.)
      ypom =ypomn-dpom-10.
      Veta='F%ull scope'
      call FeQuestAbsButtonMake(idSim,xpom,ypom,wpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtFullScope=ButtonLastMade
      ypom =ypom-dpom
      Veta='%Options'
      call FeQuestAbsButtonMake(idSim,xpom,ypom,wpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtInt=ButtonLastMade
      ypom=ypom-dpom
      xpom=(XMaxBasWin+XMaxGrWin)*.5
      Veta='Base vectors'
      call FeQuestAbsLblMake(idSim,xpom,ypom,Veta,'C','N')
      nLblBase=LblLastMade
      XMinBasVec=XMaxBasWin-XStripWidth+1.
      XMaxBasVec=XMaxBasWin
      YMaxBasVec=ypom-15.
      YMinBasVec=YMaxBasVec-XStripWidth
      call CopyMat(TrMP,TrMPOrg,NDim(KPhase))
1200  if(SimType.eq.SimIobs) then
        call iom95(0,fln(:ifln)//'.m95')
        NRefRead=0
        if(allocated(HCondAr)) deallocate(HCondAr)
        call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
        call DRSetRefArrays(0)
        SimIz=NRefRead
      else
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
        if(ErrFlag.ne.0) go to 9900
        call UnitMat(TrMP,NDim(KPhase))
        call SetIntArrayTo(HCondMx,6,0)
        SimIz=0
        if(allocated(RIAr)) deallocate(RIAr,RSAr,ICullAr,RefBlockAr,
     1                                 RunCCDAr)
        NRefAlloc=10000
        allocate(RIAr(NRefAlloc),RSAr(NRefAlloc),ICullAr(NRefAlloc),
     1           RefBlockAr(NRefAlloc),RunCCDAr(NRefAlloc))
        read(ln,FormA) Veta
        i=LocateSubstring(Veta,'e',.false.,.true.)
        if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
          format83a=format83e
        else
          format83a=format83
        endif
        rewind ln
1300    SimIz=SimIz+1
        if(SimIz.gt.NRefAlloc) call ReallocSGTest(10000)
        read(ln,Format83a,end=1320,err=9100)
     1    (ih(i),i=1,NDim(KPhase)),pom,RIAr(SimIz),RSAr(SimIz),
     2    Veta(1:1),itw,wdy
        RunCCDAr(SimIz)=1
        if(ih(1).gt.900) go to 1320
        if(SimType.eq.SimIcalcRef) then
          RIAr(SimIz)=pom
          if(RSAr(SimIz).le.0.) RSAr(SimIz)=sqrt(abs(RIAr(SimIz)))
        else if(SimType.eq.SimDiffFSig) then
          RIAr(SimIz)=wdy*1000.
        endif
        ICullAr(SimIz)=0
        RefBlockAr(SimIz)=KRefBlock
        do j=1,NSymm(KPhase)
          call MultmIRI(ih,rm6(1,j,1,KPhase),ihq,1,NDim(KPhase),
     1                  NDim(KPhase))
          do i=1,NDim(KPhase)
            HCondMx(i)=max(iabs(ihq(i)),HCondMx(i))
          enddo
        enddo
        go to 1300
1320    SimIz=SimIz-1
        NRefRead=SimIz
        if(allocated(HCondAr)) deallocate(HCondAr)
        allocate(HCondAr(NTwin,NRefAlloc))
        HCondAr=0
        j=1
        do i=1,NDim(KPhase)
          HCondLn(i)=2*HCondMx(i)+1
          if(imax/j.lt.HCondLn(i)) then
            call FeChybne(-1.,-1.,'Zle je zle!!!',' ',SeriousError)
            call CloseIfOpened(ln)
            go to 9999
          endif
          j=j*HCondLn(i)
        enddo
        rewind ln
        SimIz=0
1350    SimIz=SimIz+1
1360    read(ln,Format83a,end=1370,err=9100)(ih(i),i=1,NDim(KPhase)),
     1    pom
        if(ih(1).gt.900) go to 1370
        HCondAr(1,SimIz)=IndPack(ih,HCondLn,HCondMx,NDim(KPhase))
        go to 1350
1370    call CloseIfOpened(ln)
      endif
      SimValMax=0.
      SimSinThLMax=0.
      do i=1,NRefRead
        ntw=0
        do itw=1,NTwin
          if(HCondAr(itw,i).gt.0) then
            ntw=itw
            exit
          endif
        enddo
        if(ntw.eq.0) cycle
        call IndUnpack(HCondAr(ntw,i),ih,HCondLn,HCondMx,NDim(KPhase))
        call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
        SimSinThLMax=max(SimSinThLMax,sinthl)
        SimValMax=max(SimValMax,abs(RIAr(i)))
      enddo
      i=Exponent10(SimValMax)-6
      if(i.gt.0) then
        SimScale=1./float(10**i)
      else
        SimScale=1.
      endif
      if(SimType.ne.SimDiffFSig) then
        SimValMax=sqrt(SimValMax)
      else
        SimValMax=SimValMax*.001
      endif
      SimMainLayer=0
      SimLayer=0
      SimSatLayer=0
      nzero=0
      if(allocated(HDr))
     1  deallocate(HDr,HDrTw,RIDr,RSDr,XDr,YDr,RadDr,ColDr,PorDr,IXDr)
      NDrAlloc=1000
      allocate(HDr(NDim(NPhase),NDrAlloc),HDrTw(NDrAlloc),
     1         RIDr(NDrAlloc),RSDr(NDrAlloc),
     2         XDr(NDrAlloc),YDr(NDrAlloc),RadDr(NDrAlloc),
     3         ColDr(NDrAlloc),PorDr(NDrAlloc),IXDr(NDrAlloc))
      if(SimAve) then
        call UnitMat(TrMP,NDim(KPhase))
        if(allocated(HCondArN)) deallocate(HCondArN,riarn,rsarn,
     1                                     ICullArN,RefBlockArN,
     2                                     RunCCDArN)
        allocate(HCondArN(NTwin,NRefRead),riarN(NRefRead),
     1           rsarN(NRefRead),ICullArN(NRefRead),
     2           RefBlockArN(NRefRead),RunCCDArN(NRefRead))
        SimIzMax=NRefRead
        if(NTwin.gt.0) then
          if(allocated(HCondArP)) deallocate(HCondArP,riarp,rsarp,
     1                                       ICullArP,RefBlockArP,
     2                                       RunCCDArP)
          allocate(HCondArP(NTwin,NRefRead),riarp(NRefRead),
     1             rsarp(NRefRead),ICullArP(NRefRead),
     2             RefBlockArP(NRefRead),RunCCDArP(NRefRead))
          HCondArP(1:NTwin,1:NRefRead)=HCondAr(1:NTwin,1:NRefRead)
          riarp(1:NRefRead)=riar(1:NRefRead)
          rsarp(1:NRefRead)=rsar(1:NRefRead)
          ICullArP(1:NRefRead)=ICullAr(1:NRefRead)
          RefBlockArP(1:NRefRead)=RefBlockAr(1:NRefRead)
          RunCCDArP(1:NRefRead)=RunCCDAr(1:NRefRead)
        endif
        SimIz=0
        do itw=1,NTwin
          call DRAverage(AveFromRSViewer,itw,ich)
          call OpenFile(91,fln(:ifln)//'.l91','formatted','unknown')
          if(ErrFlag.eq.0) then
1400        read(91,Format83e,end=1500)(ih(i),i=1,NDim(KPhase)),ri,ri,rs
            ri=ri/SimScale
            rs=rs/SimScale
            if(SimComplete) then
              izp=SimIz+1
              do 1450i=1,NSymmN(KPhase)
                call IndTr(ih,rm6(1,i,1,KPhase),ihp,NDim(KPhase))
                do iz=izp,SimIz
                  call IndUnpack(HCondArN(1,iz),ihq,HCondLn,HCondMx,
     1                           NDim(KPhase))
                  if(EqIv(ihp,ihq,NDim(KPhase))) go to 1450
                enddo
                if(SimIz.ge.SimIzMax) then
                  allocate(HCondArO(NTwin,SimIz),riaro(SimIz),
     1                     rsaro(SimIz),ICullArO(SimIz),
     2                     RefBlockArO(SimIz),RunCCDArO(SimIz))
                  HCondArO(1:NTwin,1:SimIz)=HCondArN(1:NTwin,1:SimIz)
                  riarO(1:SimIz)=riarN(1:SimIz)
                  rsarO(1:SimIz)=rsarN(1:SimIz)
                  ICullArO(1:SimIz)=ICullArN(1:SimIz)
                  RefBlockArO(1:SimIz)=RefBlockArN(1:SimIz)
                  RunCCDArO(1:SimIz)=RunCCDArN(1:SimIz)
                  deallocate(HCondArN,riarN,rsarN,ICullArN,
     1                       RefBlockArN,RunCCDArN)
                  n=SimIzMax+10000
                  allocate(HCondArN(NTwin,n),riarN(n),
     1                     rsarN(n),ICullArN(n),
     2                     RefBlockArN(n),RunCCDArN(n))
                  HCondArN(1:NTwin,1:SimIz)=HCondArO(1:NTwin,1:SimIz)
                  riarN(1:SimIz)=riarO(1:SimIz)
                  rsarN(1:SimIz)=rsarO(1:SimIz)
                  ICullArN(1:SimIz)=ICullArO(1:SimIz)
                  RefBlockArN(1:SimIz)=RefBlockArO(1:SimIz)
                  RunCCDArN(1:SimIz)=RunCCDArO(1:SimIz)
                  deallocate(HCondArO,riarO,rsarO,ICullArO,
     1                       RefBlockArO,RunCCDArO)
                  SimIzMax=n
                endif
                SimIz=SimIz+1
                call SetIntArrayTo(HCondArN(1,SimIz),NTwin,-1)
                HCondArN(itw,SimIz)=
     1            IndPack(ihp,HCondLn,HCondMx,NDim(KPhase))
                RIArN(SimIz)=ri
                RSArN(SimIz)=rs
                ICullArN(SimIz)=0
                RefBlockArN(SimIz)=0
                RunCCDArN(SimIz)=1
1450          continue
            else
              if(SimIz.ge.SimIzMax) then
                allocate(HCondArO(NTwin,SimIz),riaro(SimIz),
     1                   rsaro(SimIz),ICullArO(SimIz),
     2                   RefBlockArO(SimIz),RunCCDArO(SimIz))
                HCondArO(1:NTwin,1:SimIz)=HCondArN(1:NTwin,1:SimIz)
                riarO(1:SimIz)=riarN(1:SimIz)
                rsarO(1:SimIz)=rsarN(1:SimIz)
                ICullArO(1:SimIz)=ICullArN(1:SimIz)
                RefBlockArO(1:SimIz)=RefBlockArN(1:SimIz)
                RunCCDArO(1:SimIz)=RunCCDArN(1:SimIz)
                deallocate(HCondArN,riarN,rsarN,ICullArN,
     1                     RefBlockArN,RunCCDArN)
                n=SimIzMax+10000
                allocate(HCondArN(NTwin,n),riarN(n),
     1                   rsarN(n),ICullArN(n),
     2                   RefBlockArN(n),RunCCDArN(n))
                HCondArN(1:NTwin,1:SimIz)=HCondArO(1:NTwin,1:SimIz)
                riarN(1:SimIz)=riarO(1:SimIz)
                rsarN(1:SimIz)=rsarO(1:SimIz)
                ICullArN(1:SimIz)=ICullArO(1:SimIz)
                RefBlockArN(1:SimIz)=RefBlockArO(1:SimIz)
                RunCCDArN(1:SimIz)=RunCCDArO(1:SimIz)
                deallocate(HCondArO,riarO,rsarO,ICullArO,
     1                     RefBlockArO,RunCCDArO)
                SimIzMax=n
              endif
              SimIz=SimIz+1
              call SetIntArrayTo(HCondArN(1,SimIz),NTwin,-1)
              HCondArN(itw,SimIz)=IndPack(ih,HCondLn,HCondMx,
     1                                    NDim(KPhase))
              RIArN(SimIz)=ri
              RSArN(SimIz)=rs
              ICullArN(SimIz)=0
              RefBlockArN(SimIz)=0
            endif
            go to 1400
          else
            ErrFlag=0
          endif
1500      call CloseIfOpened(91)
          call DeleteFile(fln(:ifln)//'.l91')
!   ????
          if(NTwin.gt.0) then
            HCondAr(1:NTwin,1:NRefRead)=HCondArP(1:NTwin,1:NRefRead)
            riar(1:NRefRead)=riarp(1:NRefRead)
            rsar(1:NRefRead)=rsarp(1:NRefRead)
            ICullAr(1:NRefRead)=ICullArP(1:NRefRead)
            RefBlockAr(1:NRefRead)=RefBlockArP(1:NRefRead)
            RunCCDAr(1:NRefRead)=RunCCDArP(1:NRefRead)
          endif
        enddo
        if(allocated(HCondArP)) deallocate(HCondArP,riarp,rsarp,
     1                                     ICullArP,RefBlockArP,
     2                                     RunCCDArP)
        if(allocated(HCondAr)) deallocate(HCondAr,riar,rsar,
     1                                     ICullAr,RefBlockAr,
     2                                     RunCCDAr)
        allocate(HCondAr(NTwin,SimIz),riar(SimIz),rsar(SimIz),
     1           ICullAr(SimIz),RefBlockAr(SimIz),RunCCDAr(SimIz))
        HCondAr(1:NTwin,1:SimIz)=HCondArN(1:NTwin,1:SimIz)
        riar(1:SimIz)=riarN(1:SimIz)
        rsar(1:SimIz)=rsarN(1:SimIz)
        ICullAr(1:SimIz)=ICullArN(1:SimIz)
        RefBlockAr(1:SimIz)=RefBlockArN(1:SimIz)
        RunCCDAr(1:SimIz)=RunCCDArN(1:SimIz)
        deallocate(HCondArN,riarN,rsarN,ICullArN,
     1             RefBlockArN,RunCCDArN)
        NRefRead=SimIz
      else

      endif
      call FeQuestLblOn(nLblInd)
2000  call FeQuestIntEdwOpen(nEdwLayer,SimMainLayer,.false.)
      if(NDimI(KPhase).gt.0.and..not.SimProjectSat) then
        nEdw=nEdwSatLayer
        do i=1,NDimI(KPhase)
          call FeQuestIntEdwOpen(nEdw,SimSatLayer(i),.false.)
          nEdw=nEdw+1
        enddo
      endif
      ih=0
      do i=1,3
        ih(i)=Nint(SimTr(1,i))
      enddo
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      p1=SimScope*.01*SimSinThLMax/sinthl
      do i=1,3
        ih(i)=Nint(SimTr(2,i))
      enddo
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      p2=SimScope*.01*SimSinThLMax/sinthl
      do i=1,3
        ih(i)=Nint(SimTr(3,i))
      enddo
      call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
      p3=SimSinThLMax/sinthl
      call FeSetTransXo2X(-p1,p1,-p2,p2,.true.)
      call FeQuestEudOpen(nEdwLayer,-nint(p3),nint(p3),1,0.,0.,0.)
      call FeQuestRealEdwOpen(nEdwScope,SimScope,.false.,.false.)
      call FeQuestEudOpen(nEdwScope,0,0,0,10.,100.,10.)
      if(NDimI(KPhase).gt.0) then
        SimMMax=0
        do i=1,SimIz
          call IndUnpack(HCondAr(1,i),ih,HCondLn,HCondMx,NDim(KPhase))
          do j=4,NDim(KPhase)
            SimMMax(j-3)=max(SimMMax(j-3),ih(j))
          enddo
        enddo
        if(SimProjectSat) then
          ii=NDimI(KPhase)
          nEdw=nEdwSatLayer
          do i=1,NDimI(KPhase)
            call FeQuestIntFromEdw(nEdw,SimSatLayer(i))
            call FeQuestEdwDisable(nEdw)
            nEdw=nEdw+1
          enddo
        else
          call FeQuestRealEdwOpen(nEdwDepth,SimDepth,.false.,.false.)
          nEdw=nEdwSatLayer
          ii=0
          do i=1,NDimI(KPhase)
            do j=1,3
              h(j)=qu(j,i,1,KPhase)
            enddo
            call multm(h,SimTrI,hh,1,3,3)
            if(abs(hh(3)).gt..01) then
              call FeQuestEudOpen(nEdw,-SimMMax(i),SimMMax(i),1,0.,0.,
     1                            0.)
            else
              ii=ii+1
              call FeQuestIntFromEdw(nEdw,SimSatLayer(i))
              call FeQuestEdwDisable(nEdw)
            endif
            nEdw=nEdw+1
          enddo
        endif
        if(ii.eq.NDimI(KPhase)) then
          call FeQuestRealFromEdw(nEdwDepth,SimDepth)
          call FeQuestEdwDisable(nEdwDepth)
        endif
      endif
      HardCopy=0
      nrep=0
2500  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call DRRecSpaceViewerOsnova
        call DRRecSpaceViewerSit
        SimLayerOld=SimLayer
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
      if(nrep.gt.0) then
        nrep=0
        go to 2500
      endif
      TakeMouseMove=.true.
      CheckMouse=.true.
      if(XPos.ge.XMinAcWin.and.XPos.le.XMaxAcWin.and.
     1   YPos.ge.YMinAcWin.and.YPos.le.YMaxAcWin) then
        h(1)=Xpos
        h(2)=Ypos
        h(3)=SimDrLayer
        call FeX2Xf(h,hh)
        call multm(hh,SimTr,h,1,3,3)
        write(Veta,'(3f8.3)') h
      else
        Veta=' '
      endif
      call FeWInfWrite(1,Veta)
3000  call FeQuestEvent(idSim,ich)
      if(CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape.or.
     1   CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit) then
        go to 9900
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPrint) then
        call FePrintPicture(ich)
        if(ich.eq.0) then
          go to 2500
        else
          HardCopy=0
          go to 3000
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSave) then
        call FeSavePicture('figure',7,1)
        if(HardCopy.gt.0) then
          go to 2500
        else
          HardCopy=0
          go to 3000
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew) then
        call DRRecSpaceViewerDef(idal,ich)
        if(ich.ne.0.or.idal.eq.0) then
          go to 3000
        else if(idal.eq.1) then
          SimLayer=0
          SimMainLayer=0
          SimSatLayer=0
          go to 2000
        else if(idal.eq.2) then
          SimLayer=0
          SimMainLayer=0
          SimSatLayer=0
          go to 1200
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtFullScope) then
        SimScope=100.
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInt) then
        call DRRecSpaceViewerDefInt
        go to 2000
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwLayer.or.
     2         (CheckNumber.ge.nEdwSatLayer.and.
     3          CheckNumber.le.nEdwSatLayer+NDimI(KPhase)-1))) then
        call FeQuestIntFromEdw(nEdwLayer,SimMainLayer)
        SimLayer=SimMainLayer
        if(NDimI(KPhase).gt.0.and..not.SimProjectSat) then
          nEdw=nEdwSatLayer
          do i=1,NDimI(KPhase)
            call FeQuestIntFromEdw(nEdw,SimSatLayer(i))
            nEdw=nEdw+1
          enddo
          ih=0
          call CopyVekI(SimSatLayer,ih(4),NDimI(KPhase))
          do j=1,3
            h(j)=ih(j)
            do k=1,NDimI(KPhase)
              h(j)=h(j)+float(ih(k+3))*qu(j,k,1,KPhase)
            enddo
          enddo
          call multm(h,SimTrI,hh,1,3,3)
          SimLayer=SimLayer+hh(3)
        endif
        if(SimLayer.eq.SimLayerOld) then
          go to 3000
        else
          go to 2000
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwDepth) then
        call FeQuestRealFromEdw(nEdwDepth,SimDepth)
        go to 2000
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwScope) then
        call FeQuestRealFromEdw(nEdwScope,SimScope)
        go to 2000
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeMove) then
        if(XPos.ge.XMinAcWin.and.XPos.le.XMaxAcWin.and.
     1     YPos.ge.YMinAcWin.and.YPos.le.YMaxAcWin) then
          h(1)=Xpos
          h(2)=Ypos
          h(3)=SimDrLayer
          call FeX2Xf(h,hh)
          call multm(hh,SimTr,h,1,3,3)
          write(Veta,'(3f8.3)') h
        else
          Veta=' '
        endif
        call FeWInfWrite(1,Veta)
        go to 3000
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeLeftDown)
     1  then
        if(XPos.ge.XMinAcWin.and.XPos.le.XMaxAcWin.and.
     1     YPos.ge.YMinAcWin.and.YPos.le.YMaxAcWin) then
          i1=1
          i2=SimNDr
          i3=0
          i4=0
3100      i=(i1+i2)/2
          if(i2-i1.le.3) go to 3110
          if(XPos.lt.XDr(PorDr(i))-RadDrMax) then
            i2=i
            go to 3100
          else if(XPos.gt.XDr(PorDr(i))-RadDrMax) then
            i1=i
            go to 3100
          endif
3110      i3=1
          i4=i2
3120      i=(i3+i4)/2
          if(i4-i3.le.3) go to 3150
          if(XPos.lt.XDr(PorDr(i))+RadDrMax) then
            i4=i
            go to 3120
          else if(XPos.gt.XDr(PorDr(i))+RadDrMax) then
            i3=i
            go to 3120
          endif
3150      i1=min(i1,i3)
          i2=max(i2,i4)
          do i=i1,i2
            j=PorDr(i)
            if(sqrt((Xpos-XDr(j))**2+(Ypos-YDr(j))**2).lt.RadDr(j)+1.)
     1        then
              XPosO=XPos
              YPosO=YPos
              call MakeHKLString(HDr(1,j),NDim(KPhase),Veta)
              TextInfo(1)='Indices:   '//Veta(:idel(Veta))
              if(SimType.eq.SimDiffFSig) then
                write(Veta,'(f10.3)') RIDr(j)*.001
                call Zhusti(Veta)
                TextInfo(2)='(Fcalc-Fobs)/sig(Fobs): '//
     1                      Veta(:idel(Veta))
              else
                call RoundESD(Veta,RIDr(j),RSDr(j),1,0,0)
                TextInfo(2)='Intensity: '//Veta(:idel(Veta))
              endif
              NInfo=2
              call FeInfoOut(-1.,-1.,'INFORMATION','L')
              call FeMoveMouseTo(XPosO,YPosO)
              go to 3000
            endif
          enddo
        endif
      endif
      go to 3000
9000  call FeFlowChartRemove
      go to 9900
9100  call FeReadError(ln)
      call FeFlowChartRemove
9900  call FeQuestRemove(idSim)
      call FeWinfRemove(1)
9999  if(allocated(HCondAr)) deallocate(HCondAr,RIAr,RSAr,ICullAr,
     1                                  RefBlockAr,RunCCDAr)
      if(allocated(HDr))
     1  deallocate(HDr,HDrTw,RIDr,RSDr,XDr,YDr,RadDr,ColDr,PorDr,IXDr)
      call CopyMat(TrMPOrg,TrMP,NDim(KPhase))
!      KPhase=KPhaseIn
      return
      end
