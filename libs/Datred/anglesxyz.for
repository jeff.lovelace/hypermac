      subroutine AnglesXYZ(x,A,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension x(3),A(4),Rko(3,3)
      real le
      le=VecOrtLen(x,3)
      if(le.lt.1.0e-7) then
        x(1)=0.
        x(2)=1.
        x(3)=0.
        A(2)=0.
      else
        x(1)=x(1)/le
        x(2)=x(2)/le
        x(3)=x(3)/le
        le=.5*le*LamAveRefBlock(KRefBlock)
        A(2)=asin(le)
        call Setting(x,A,Rko,ich)
        A(1)=A(1)+A(2)
      endif
      return
      end
