      subroutine GenerRef(Text,sntolmx,MMaxi,OnlyLaue,klic)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      integer FlagIn(:),ZRefine
      logical SystExtRef,eqiv,SatFrModOnly(:),SkipFriedel(:),
     1        SatFrModOnlyIn(*),SkipFriedelIn(*),OnlyLaue,ExistFile,
     2        ReflFulfilsCond,lpom
      character*(*) Text
      character*80 Veta,FormatOut
      dimension ihmina(6,3),ihmaxa(6,3),ihmaxc(6),ihdc(6),kwp(3),
     1          ihmaxf(6),ihdf(6),ihf(6),dif(3),mmaxi(3,*),hh(3),hhh(3),
     2          ihp(6),ihh(6),ihc(6),io(6),MMin(3),MMax(3),lng(:),
     3          iha(:),is(:),imlt(:),ihap(:),isp(:),imltp(:),ipor(:),
     4          ihn(:,:),fmn(:),snn(:),FCalcN(:),PomMat(36),sctwo(:),
     5          MMinAll(3),MMaxAll(3)
      allocatable iha,is,imlt,ihap,isp,imltp,ipor,ihn,fmn,snn,FCalcN,
     1            SatFrModOnly,SkipFriedel,sctwo,lng,FlagIn
      data dif/3*.001/
      allocate(SatFrModOnly(NPhase),SkipFriedel(NPhase),FlagIn(NPhase),
     1         lng(NPhase))
      call SetLogicalArrayTo(SatFrModOnly,NPhase,.false.)
      call SetLogicalArrayTo(SkipFriedel,NPhase,OnlyLaue)
      ZRefine=0
      go to 500
      entry GenerRefPwd(Text,sntolmx,mmaxi,SatFrModOnlyIn,SkipFriedelIn,
     1                  klic)
      allocate(SatFrModOnly(NPhase),SkipFriedel(NPhase),FlagIn(NPhase),
     1         lng(NPhase))
      ZRefine=1
      do i=1,NPhase
        SatFrModOnly(i)=SatFrModOnlyIn(i)
        SkipFriedel(i)=SkipFriedelIn(i).or.DoLeBail.gt.0
      enddo
      if(NAtCalc.gt.0) then
        if(allocated(AtDisable)) then
          ZRefine=2
        else
          Veta=fln(:ifln)//'.ref'
          if(ExistFile(Veta))
     1      call MoveFile(Veta,Veta(:idel(Veta))//'_tmp')
          call RefPrelim(0)
        endif
        iq=1
      endif
500   if(klic.eq.2) then
        FormatOut='(3i4,f11.6,i4)'
        NDimOut=NDim(KPhase)
        write(FormatOut(2:2),'(i1)') NDimOut
      else
        if(NPhase.le.1) ln=91
        if(isPowder) then
          FormatOut=format91pow
          NDimOut=6
        else
          FormatOut=format91
          NDimOut=MaxNDim
        endif
      endif
      nalloc=10000
      allocate(iha(nalloc),is(nalloc),imlt(nalloc),sctwo(NPhase))
      if(ZRefine.ne.0.and.MaxNDimI.gt.0.and.NAtCalc.gt.0) then
        if(OrthoOrd.gt.0) call trortho(0)
        if(metoda.ne.0) call calcm2(i,i,isw,-1)
      endif
      MMax=0
      do KPh=1,NPhase
        KPhase=KPh
        if(NAtCalc.gt.0.and.ZRefine.ne.0) then
          sco=sc(1,KDatBlock)
          sc(1,KDatBlock)=100.*CellVol(1,KPhase)/CellVol(1,1)
          call CopyVek(sctw(1,KDatBlock),sctwo,NPhase)
          call SetRealArrayTo(sctw(1,KDatBlock),NPhase,0.)
          sctw(KPhase,KDatBlock)=1.
        endif
        if(SatFrModOnly(KPh).and.NDim(KPh).gt.3) then
          call SetIntArrayTo(MMax,3,0)
          do j=1,MaxUsedKw(KPh)
            do i=1,NDimI(KPh)
              MMax(i)=max(MMax(i),iabs(kw(i,j,KPh)))
            enddo
          enddo
        else
          call CopyVekI(MMaxi(1,KPh),MMax,NDimI(KPh))
        endif
        MMin=-MMax
        if(KCommen(KPh).gt.0) then
          if(.not.SwitchedToComm) call ComSym(0,0,ich)
          call SuperSGToSuperCellSG(ich)
          if(ich.ne.0) then
            ErrFlag=1
            go to 9000
          endif
          call UnitMat(PomMat,NDim(KPh))
          call DRMatTrCell(RCommen(1,1,KPh),CellPar(1,1,KPh),PomMat)
          NDim(KPh)=3
          NDimI(KPh)=NDim(KPh)-3
          NDimQ(KPh)=NDim(KPh)**2
          NComp(KPh)=1
          call SetMet(0)
        endif
        if(Klic.ne.2.and.NPhase.gt.1) then
          lng(KPh)=NextLogicNumber()
          Cislo=' '
          write(Cislo(1:4),'(''.l'',i2)') 70+KPh
          call OpenFile(lng(KPh),fln(:ifln)//Cislo(:idel(Cislo)),
     1                  'formatted','unknown')
          ln=lng(KPh)
        endif
        do i=1,6
          io(i)=i
        enddo
        io(1)=3
        io(2)=1
        io(3)=2
        call SetIntArrayTo(ihc,6,0)
        if(klic.eq.1) then
          if(mod(CrSystem(KPh),10).eq.2) then
            io(1)=Monoclinic(KPh)
            io(2)=mod(io(1),3)+1
            io(3)=6-io(1)-io(2)
          endif
        endif
        do i=1,6
          if(i.le.NDim(KPh)) then
            ihmaxc(i)=-999999
            ihdc(i)=0
          else
            ihmaxc(i)=0
            ihdc(i)=1
          endif
        enddo
        ng=0
        do isw=1,NComp(KPh)
          ig=1
          do i=1,3
            pom=2.*sntolmx*CellPar(i,isw,KPh)
            do j=1,NDimI(KPh)
              pom=pom+abs(float(MMax(j))*qu(i,j,isw,KPh))
            enddo
            ihmina(i,isw)=-nint(pom)
            ihmaxa(i,isw)= nint(pom)
            j=2*ihmaxa(i,isw)+1
            ig=ig*j
          enddo
          MMaxAll=0
          do i=4,6
            if(i.le.NDim(KPh)) then
              ihmina(i,isw)= MMin(i-3)
              ihmaxa(i,isw)= MMax(i-3)
              MMaxAll(i-3)=max(MMaxAll(i-3),MMax(i-3))
              j=2*MMax(i-3)+1
            else
              ihmina(i,isw)=0
              ihmaxa(i,isw)=0
              j=1
            endif
            ig=ig*j
          enddo
          call IndTr(ihmaxa(1,isw),zv(1,isw,KPh),ihc,NDim(KPh))
          do j=1,NDim(KPh)
            ihmaxc(j)=max(iabs(ihc(j)),ihmaxc(j))
          enddo
          ng=ng+ig
        enddo
        call SetIntArrayTo(ihp,3,0)
        do im=ihmina(4,1),ihmaxa(4,1)
          ihp(4)=im
          do in=ihmina(5,1),ihmaxa(5,1)
            ihp(5)=in
            do ip=ihmina(6,1),ihmaxa(6,1)
              ihp(6)=ip
              do i=1,NSymm(KPh)
                call indtr(ihp,rm6(1,i,1,KPh),ihh,NDim(KPh))
                do j=4,NDim(KPh)
                  ihmaxc(j)=max(iabs(ihh(j)),ihmaxc(j))
                enddo
              enddo
            enddo
          enddo
        enddo
        do i=1,NDim(KPh)
          ihdc(i)=2*ihmaxc(i)+1
        enddo
        do i=1,NDim(KPh)
          j=io(i)
          ihmaxf(i)=ihmaxc(j)
          ihdf(i)=ihdc(j)
        enddo
        NRef90p=0
        nmod=max(nint(float(ng)*.005),10)
        if(ZRefine.ne.0.and.NDatBlock.gt.1) then
          call FeFlowChartRemove
          write(Cislo,FormI15) KDatBlock
          call Zhusti(Cislo)
          Veta=Text(:idel(Text))//' for DatBlock#'//Cislo(:idel(Cislo))
        else
          Veta=Text
        endif
        if(NPhase.gt.1) then
          if(ZRefine.ne.0.and.NDatBlock.gt.1) then
            Veta=Veta(:idel(Veta))//' and'
          else
            Veta=Veta(:idel(Veta))//' for'
          endif
          Veta=Veta(:idel(Veta))//' phase: '//
     1      PhaseName(KPh)(:idel(PhaseName(KPh)))
        endif
        call FeFlowChartOpen(-1.,-1.,nmod,ng,Veta,' ',' ')
        ig=0
        if(SkipFriedel(KPh)) then
          ncsp=2
        else
          ncsp=1
        endif
        do isw=1,NComp(KPh)
          do ih=ihmina(1,isw),ihmaxa(1,isw)
            ihp(1)=ih
            do ik=ihmina(2,isw),ihmaxa(2,isw)
              ihp(2)=ik
              do il=ihmina(3,isw),ihmaxa(3,isw)
                ihp(3)=il
                do im=ihmina(4,isw),ihmaxa(4,isw)
                  ihp(4)=im
                  do in=ihmina(5,isw),ihmaxa(5,isw)
                    ihp(5)=in
                    do 3500ip=ihmina(6,isw),ihmaxa(6,isw)
                      ihp(6)=ip
                      call FeFlowChartEvent(ig,ie)
                      if(ie.ne.0) then
                        ErrFlag=1
                        if(ZRefine.eq.0.or.NDatBlock.le.1) then
                          call FeFlowChartRemove
                          go to 9000
                        endif
                      endif
                      call FromIndSinthl(ihp,hh,sinthl,sinthlq,isw,0)
                      if(sinthl.gt.sntolmx.or.sinthl.lt..00001)
     1                  go to 3500
                      call IndTr(ihp,zv(1,isw,KPh),ihc,NDim(KPh))
                      if(isw.gt.1) then
                        do i=4,NDim(KPh)
                          if(ihc(i).lt.ihmina(i,1).or.
     1                       ihc(i).gt.ihmaxa(i,1)) then
                            go to 3020
                          endif
                        enddo
                        go to 3500
                      endif
3020                  if(SystExtRef(ihc,.false.,1)) go to 3500
                      if(ZRefine.ne.0) then
                        do i=1,nskrt
                          lpom=ReflFulfilsCond(ihc,ihm(1,i),ihma(1,i),
     1                           ihsn(1,i),imdn(i),ieqn(i),
     2                           ihsv(1,i),imdv(i),ieqv(i))
                          if(lpom.eqv.DontUse(i)) go to 3500
                        enddo
                      endif
                      if(SatFrModOnly(KPh).and.NDim(KPh).gt.3) then
                        call IndTr(ihc,zvi(1,isw,KPh),ihh,NDim(KPh))
                        j=0
                        do i=4,NDim(KPh)
                          j=j+iabs(ihh(i))
                        enddo
                        if(j.eq.0) go to 3150
                        do j=1,MaxUsedKw(KPh)
                          call CopyVekI(kw(1,j,KPh),kwp,NDimI(KPh))
                          do n=1,2
                            if(n.eq.2)
     1                        call IntVectorToOpposite(kwp,kwp,
     2                                                 NDimI(KPh))
                            if(eqiv(ihh(4),kwp,NDimI(KPh))) go to 3150
                          enddo
                        enddo
                        go to 3500
                      endif
3150                  mx=0
                      nn=0
                      do i=1,NSymm(KPh)
                        do 3200ic=1,3-2*ncsp,-2
                          if(ic.eq.1) then
                            call indtr(ihc,rm6(1,i,1,KPh),ihh,
     1                                 NDim(KPh))
                          else
                            do j=1,NDim(KPh)
                              ihh(j)=-ihh(j)
                            enddo
                          endif
                          if(ZRefine.ne.0) then
                            do j=1,nskrt
                              lpom=ReflFulfilsCond(ihc,ihm(1,j),
     1                             ihma(1,j),ihsn(1,j),imdn(j),ieqn(j),
     2                                       ihsv(1,j),imdv(j),ieqv(j))
                              if(lpom.eqv.DontUse(j)) go to 3200
                            enddo
                          endif
                          do j=1,NDim(KPh)
                            ihf(j)=ihh(io(j))
                          enddo
                          m=IndPack(ihf,ihdf,ihmaxf,NDim(KPh))
                          if(i.eq.1.and.ic.eq.1) then
                            mx=m
                            nn=1
                          else if(m.eq.mx) then
                            nn=nn+1
                          else if(m.gt.mx) then
                            go to 3500
                          endif
3200                    continue
                      enddo
                      NRef90p=NRef90p+1
                      if(NRef90p.gt.nalloc) then
                        allocate(ihap(nalloc),isp(nalloc),imltp(nalloc))
                        call CopyVekI(iha,ihap,nalloc)
                        call CopyVekI(is,isp,nalloc)
                        call CopyVekI(imlt,imltp,nalloc)
                        nallocp=nalloc
                        nalloc=nalloc+10000
                        deallocate(iha,is,imlt)
                        allocate(iha(nalloc),is(nalloc),imlt(nalloc))
                        call CopyVekI(ihap,iha,nallocp)
                        call CopyVekI(isp,is,nallocp)
                        call CopyVekI(imltp,imlt,nallocp)
                        deallocate(ihap,isp,imltp)
                      endif
                      iha(NRef90p)=mx
                      is(NRef90p)=sinthl*1000000.
                      imlt(NRef90p)=NSymm(KPh)*ncsp/nn
3500                continue
                  enddo
                enddo
              enddo
            enddo
          enddo
        enddo
5100    call FeFlowChartEvent(ng,ie)
        allocate(ipor(NRef90p))
        if(klic.eq.0.or.klic.eq.2) then
          call indexx(NRef90p,is,ipor)
        else
          call indexx(NRef90p,iha,ipor)
        endif
        ndp=NDim(KPh)
        n=NRef90p
        if(KCommen(KPh).gt.0) then
          call iom50(0,0,fln(:ifln)//'.m50')
          KPhase=KPh
          call ComSym(0,0,ich)
          if(ich.ne.0) go to 9000
          NRef90p=0
          MMaxAll=0
          do i=1,NDimI(KPh)
            MMaxAll(i)=max(MMaxAll(i),MMax(i))
          enddo
        endif
        scp=1.
        MMinAll=-MMaxAll
5200    do 5500i=1,n
          j=ipor(i)
          call IndUnpack(iha(j),ihh,ihdf,ihmaxf,ndp)
          do k=1,ndp
            ihc(io(k))=ihh(k)
          enddo
          itw=1
          if(KCommen(KPh).gt.0) then
            do k=1,3
              hhh(k)=float(ihc(k))
            enddo
            call MultM(hhh,RCommenI(1,1,KPhase),hh,1,3,3)
            call ChngInd(hh,ihc,1,dif,MMinAll,MMaxAll,1,CheckExtRefYes)
            do k=1,3
              if(dif(k).gt..01) go to 5500
            enddo
            if(SatFrModOnly(KPh).and.NDim(KPh).gt.3) then
              m=0
              do k=4,NDim(KPh)
                m=m+iabs(ihc(k))
              enddo
              if(m.ne.0) then
                do k=1,MaxUsedKw(KPh)
                  call CopyVekI(kw(1,k,KPh),kwp,NDimI(KPh))
                  do m=1,2
                    if(m.eq.2)
     1                call IntVectorToOpposite(kwp,kwp,NDimI(KPh))
                    if(eqiv(ihc(4),kwp,NDimI(KPh))) go to 5490
                  enddo
                enddo
                go to 5500
              endif
            endif
5490        NRef90p=NRef90p+1
          endif
          if(klic.eq.2) then
            write(91,FormatOut)
     1        (ihc(k),k=1,NDim(KPh)),500000./float(is(j)),imlt(j)
          else
            if(ZRefine.ne.0.and.NAtCalc.gt.0.and.
     1         (NAtIndLenAll(KPhase).gt.0.or.NAtPosLenAll(KPhase).gt.0))
     2        then
              do k=1,NDimOut
                ihref(k,1)=ihc(k)
                IHRead(k)=ihc(k)
              enddo
              call FromIndSinthl(ihref(1,1),hh,sinthl,sinthlq,1,0)
              call RefLoopBefore(1,0,ich)
              call calc(1)
              if(ErrFlag.ne.0) go to 9000
            else
              FCalc=1.
            endif
            pom=FCalc**2*scp
            if(pom.gt.9999999.) then
              scp=scp*.1
              rewind ln
              go to 5200
            endif
            write(ln,FormatOut)(ihc(k),k=1,NDimOut),pom,
     1            float(imlt(j)),1,0,KPh,float(is(j))/1000000.
          endif
5500    continue
        if(klic.ne.2) then
          write(ln,'('' 999'')')
          rewind ln
          if(Klic.eq.1) NRef90(KDatBlock)=NRef90p
        endif
        if(ZRefine.eq.0.or.NDatBlock.le.1) call FeFlowChartRemove
        deallocate(ipor)
        if(NAtCalc.gt.0.and.ZRefine.ne.0) then
          sc(1,KDatBlock)=sco
          call CopyVek(sctwo,sctw(1,KDatBlock),NPhase)
        endif
      enddo
      if(NAtCalc.gt.0.and.ZRefine.eq.1) then
        call CloseIfOpened(lst)
        Veta=fln(:ifln)//'.ref'
        if(ExistFile(Veta(:idel(Veta))//'_tmp'))
     1    call MoveFile(Veta(:idel(Veta))//'_tmp',Veta)
        call RefDeallocate(.true.,1)
      endif
      if(ZRefine.ne.0.and.MaxNDimI.gt.0.and.NAtCalc.gt.0) then
        if(OrthoOrd.gt.0) call trortho(1)
        call iom40only(0,0,fln(:ifln)//'.m40')
        if(MagneticType(KPhase).ne.0) then
          call SetMag(-1)
          call SetMag(0)
        endif
c        if(metoda.ne.0) call calcm2(i,i,isw,-1)
      endif
      if(NPhase.gt.1) then
        allocate(ihn(6,NPhase),FCalcN(NPhase),fmn(NPhase),snn(NPhase))
        call SetIntArrayTo(FlagIn,NPhase,0)
        do i=1,NPhase
          ln=lng(i)
          read(ln,FormatOut,err=7050,end=7050)(ihn(j,i),j=1,NDimOut),
     1      FCalcN(i),fmn(i),j,j,j,snn(i)
          if(ihn(1,i).gt.900) go to 7050
          FlagIn(i)=1
          cycle
7050      FlagIn(i)=2
        enddo
7200    snmin=999999.
        do 7220i=1,NPhase
          if(FlagIn(i).ne.1) go to 7220
          if(snn(i).lt.snmin) then
            k=i
            snmin=snn(k)
          endif
7220    continue
        if(snmin.gt.900000.) go to 7500
        write(91,FormatOut)(ihn(j,k),j=1,NDimOut),FCalcN(k),fmn(k),1,0,
     1                      k,snmin
        ln=lng(k)
        read(ln,FormatOut,err=7250,end=7250)(ihn(j,k),j=1,NDimOut),
     1                                       FCalcN(k),fmn(k),j,j,j,
     2                                       snn(k)
        if(ihn(1,k).gt.900) go to 7250
        go to 7200
7250    FlagIn(k)=2
        go to 7200
7500    write(91,'('' 999'')')
        deallocate(ihn,FCalcN,fmn,snn)
      endif
      rewind 91
9000  if(NPhase.gt.1) then
        do i=1,NPhase
          close(lng(i),status='delete')
        enddo
      endif
      KPhase=KPhaseBasic
      if(Allocated(iha)) deallocate(iha,is,imlt,sctwo)
      if(allocated(SatFrModOnly))
     1  deallocate(SatFrModOnly,SkipFriedel,FlagIn,lng)
      return
      end
