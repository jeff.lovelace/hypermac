      subroutine SetAtMagMode
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(3)
      do i=1,NAtMagMode
        ia=IAtMagMode(1,i)
        do j=1,NMAtMagMode(i)
          kt=KtAt(LMagAMode,NMagAMode,LAtMagMode(j,i))
          KAtMagMode(j,i)=kt
          fn=float(MMagAMode(kt)/3)
          pom=0.
          do k=1,MMagAMode(kt),3
            call MultM(MetTens(1,1,KPhase),MagAMode(k,kt),xp,3,3,1)
            pom=pom+scalmul(MagAMode(k,kt),xp)
          enddo
          FAtMagMode(j,i)=sqrt(fn/pom)
        enddo
      enddo
      return
      end
