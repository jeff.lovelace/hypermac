      subroutine GetShSgOld(rx,px,n,nx,vt,nvt,ShSg)
      include 'fepc.cmn'
      dimension rx(2*n,n),px(2*n),ShSg(n),pxi(6),ShSgP(6),vt(6,nvt),
     1          ShSgV(6)
      if(nx.le.0) go to 9999
      if(nx.gt.0) then
        i1m=2
      else
        i1m=0
      endif
      if(nx.gt.1) then
        i2m=2
      else
        i2m=0
      endif
      if(nx.gt.2) then
        i3m=2
      else
        i3m=0
      endif
      if(nx.gt.3) then
        i4m=2
      else
        i4m=0
      endif
      if(nx.gt.4) then
        i5m=2
      else
        i5m=0
      endif
      if(nx.gt.5) then
        i6m=2
      else
        i6m=0
      endif
      difc=0.
      nposc=0
      do j=1,n
        difc=difc+abs(ShSg(j))
        if(ShSg(j).ge.0.) nposc=nposc+1
      enddo
      do i1=-i1m,i1m
        if(n.gt.0) pxi(1)=px(1)+float(i1)
        do i2=-i2m,i2m
          if(n.gt.1) pxi(2)=px(2)+float(i2)
          do i3=-i3m,i3m
            if(n.gt.2) pxi(3)=px(3)+float(i3)
            do i4=-i4m,i4m
              if(n.gt.3) pxi(4)=px(4)+float(i4)
              do i5=-i5m,i5m
                if(n.gt.4) pxi(5)=px(5)+float(i5)
                do i6=-i6m,i6m
                  if(n.gt.5) pxi(6)=px(6)+float(i6)
                  call SetRealArrayTo(ShSgP,n,0.)
                  do 1200i=nx,1,-1
                    do j=1,n
                      if(abs(rx(i,j)).gt..0001) then
                        ShSgP(j)=pxi(i)
                        do k=j+1,n
                          ShSgP(j)=ShSgP(j)-rx(i,k)*ShSgP(k)
                        enddo
                        ShSgP(j)=ShSgP(j)/rx(i,j)
                        go to 1200
                      endif
                    enddo
1200              continue
                  do 2000ivt=1,nvt
                    call AddVek(ShSgP,vt(1,ivt),ShSgV,n)
                    call od0do1(ShSgV,ShSgV,n)
                    difp=0.
                    npos=0
                    do j=1,n
                      if(ShSgV(j).gt..5) ShSgV(j)=ShSgV(j)-1.
                      pom=ShSgV(j)
                      difp=difp+abs(pom)
                      if(difp.gt.difc) go to 2000
                      if(ShSgV(j).ge.0.) npos=npos+1
                    enddo
                    if((difp.eq.difc.and.npos.gt.nposc).or.
     1                  difp.lt.difc) then
                      difc=difp
                      call CopyVek(ShSgV,ShSg,n)
                      nposc=npos
                    endif
2000              continue
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
4000  continue
      if(difc.gt.900000.) call SetRealArrayTo(ShSg,n,0.)
9999  return
      end
