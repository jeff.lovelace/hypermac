      subroutine MakeTablesOrtWaves(ln,Nazev,OrSel,Orm,nd,kmod)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer OrSel(*)
      dimension Orm(*)
      character*(*) Nazev
      character*256 t256
      t256=Nazev//';'
      call Zhusti(t256)
      idl=idel(t256)
      do kwp=1,kmod
        if(kwp.gt.1) t256=' '
        write(t256(idl:),100) 'o',kwp
        call zhusti(t256(idl:))
        j=idel(t256)
        if(t256(j:j).eq.',') t256(j:j)=' '
        l=0
        do m=1,mxw21
          if(orsel(m).eq.1) then
            l=l+1
            write(Cislo,'(f15.3)') orm(kwp+1+nd*(l-1))
            t256=t256(:idel(t256))//';'//Cislo(:idel(Cislo))
          else
            t256=t256(:idel(t256))//';-'
          endif
          if(l.ge.kwp+1) go to 1200
        enddo
1200    call zhusti(t256(9:))
        write(ln,FormA) t256(:idel(t256))
      enddo
      return
100   format(';',a1,3(',',i2))
      end
