      subroutine GoToEnantiomorph
      use Basic_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 flnNew,Veta
      real xp(6)
      logical EqIgCase,ExistFile,StandardSetting,EqRV0,EqRVM
      Veta=fln
      call CrlDefineNewfln(Konec)
      if(Konec.eq.1) go to 9999
      flnNew=fln
      iflnNew=ifln
      fln=Veta
      ifln=idel(fln)
      StandardSetting=EqRV0(ShSg(1,KPhase),3,.001)
      if(.not.EqIgCase(fln,flnNew)) then
        do i=1,6
          if(i.eq.1) then
            Cislo='.m40'
          else if(i.eq.2) then
            if(.not.isPowder) cycle
            Cislo='.m41'
          else if(i.eq.3) then
            Cislo='.m50'
          else if(i.eq.4) then
            Cislo='.m90'
          else if(i.eq.5) then
            Cislo='.m95'
          else if(i.eq.6) then
            Cislo='.l51'
          endif
          Veta=fln(:ifln)//Cislo(:idel(Cislo))
          if(ExistFile(Veta)) call CopyFile(Veta,flnNew(:iflnNew)//
     1                                      Cislo(:idel(Cislo)))
        enddo
      endif
      do i=1,NSymm(KPhase)
        call RealVectorToOpposite(s6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                            NDim(KPhase))
        call NormCentr(s6(1,i,1,KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
      enddo
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      xp=1.
      if(StandardSetting.and..not.EqRV0(ShSg(1,KPhase),3,.001)) then
        do i=1,NSymm(KPhase)
          call CopyVek(s6(1,i,1,KPhase),s6o,NDim(KPhase))
          call multm(rm6(1,i,1,KPhase),ShSg(1,KPhase),xp,NDim(KPhase),
     1               NDim(KPhase),1)
          do j=1,NDim(KPhase)
            s6(j,i,1,KPhase)=s6(j,i,1,KPhase)+xp(j)-ShSg(j,KPhase)
          enddo
          call NormCentr(s6(1,i,1,KPhase))
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                  symmc(1,i,1,KPhase),0)
        enddo
        do i=1,3
          xp(i)=1.-ShSg(i,KPhase)
        enddo
        call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      endif
      if(NTwin.gt.1) then
        do KDatB=1,NDatBlock
          do i=1,NTwin-1
            do j=i+1,NTwin
              if(EqRVM(rtw(1,i),rtw(1,j),9,.001)) then
                pom=sctw(j,KDatB)
                sctw(j,KDatB)=sctw(i,KDatB)
                sctw(i,KDatB)=pom
                pom=sctws(j,KDatB)
                sctws(j,KDatB)=sctws(i,KDatB)
                sctws(i,KDatB)=pom
                exit
              endif
            enddo
          enddo
        enddo
      endif
      call iom50(1,0,flnNew(:idel(flnNew))//'.m50')
      call iom50(0,0,flnNew(:idel(flnNew))//'.m50')
      NTrans=1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      call UnitMat(TransMat(1,1,1),NDim(KPhase))
      call RealMatrixToOpposite(TransMat(1,1,1),TransMat(1,1,1),
     1                          NDim(KPhase))
      call CopyVek(xp,TransVec(1,1,1),NDim(KPhase))
      TransZM(1)=1.
      call EM40SetTr(0)
      call EM40TransAll(ich)
      call iom40(1,0,flnNew(:iflnNew)//'.m40')
      if(.not.EqIgCase(fln,flnNew))
     1  call ContinueWithNewStructure(flnNew)
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      if(allocated(TransMat)) call DeallocateTrans
9999  return
      end
