      subroutine CrSystemFromOper()
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ior(3)
      character*6 mirror
      character*3 iosy,t3
      character*2 t2m
      data mirror/'mabcnd'/
      iosy='000'
      i3=0
      i4=0
      i6=0
      jold=0
      nosy=0
      CrSystem(KPhase)=0
      do i=1,NSymm(KPhase)
        call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,t3,t2m,ior,
     1             nn,pom)
        j=KeyOr(ior)
        if(j.ge.1.and.j.le.3.and.(t3(1:1).eq.'2'.or.
     1                            index(mirror,t3(1:1)).gt.0)) then
          iosy(j:j)='1'
          nosy=j
        endif
        if((index(t3(1:1),'4').ne.0.or.index(t3,'-4').ne.0).and.
     1     i4.eq.0) then
          if(CrSystem(KPhase).ne.CrSystemCubic)
     1      CrSystem(KPhase)=CrSystemTetragonal
        endif
        if(index(t3(1:1),'3').ne.0.and.i3.eq.0.and.i6.eq.0) then
          if(j.ge.1.and.j.le.3) then
            if(CrSystem(KPhase).ne.CrSystemCubic)
     1        CrSystem(KPhase)=CrSystemTrigonal
            if(i3.eq.0) i3=i
          else if(j.ge.6.or.j.le.9) then
            if(CrSystem(KPhase).eq.-CrSystemTrigonal.and.j.ne.jold) then
              CrSystem(KPhase)=CrSystemCubic
            else
              CrSystem(KPhase)=-CrSystemTrigonal
              jold=j
            endif
            if(i3.eq.0.and.j.eq.6) i3=i
          endif
        endif
        if((index(t3,'6').ne.0.or.index(t3,'-6').ne.0).and.
     1     i6.eq.0) then
          if(CrSystem(KPhase).ne.CrSystemCubic)
     1      CrSystem(KPhase)=CrSystemHexagonal
          i6=i
        endif
      enddo
      if(CrSystem(KPhase).eq.0) then
        if(iosy.eq.'000') then
          CrSystem(KPhase)=CrSystemTriclinic
        else if(iosy.eq.'111') then
          CrSystem(KPhase)=CrSystemOrthorhombic
        else
          CrSystem(KPhase)=10*nosy+CrSystemMonoclinic
        endif
      endif
      return
      end
