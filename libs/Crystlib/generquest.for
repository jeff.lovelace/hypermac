      subroutine GenerQuest(Klic,lng)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension mmin(3),mmax(3),ih(6),h(3)
      character*256 EdwStringQuest
      character*80 Veta,FileName,TmpFile
      character*17 Label
      character*12 Nazev
      character*12 :: IdGener(7) =
     1              (/'GenerType ','LimType   ','Limits    ',
     2                'mmin      ','mmax      ','ApplyCentr',
     3                'ApplyExt  '/)
      integer GenerType,CrwStateQuest
      logical first,CrwLogicQuest,ApplyCentr,ApplyExt,SystExtRef,
     1        InclZeroLayer
      data first/.true./,mmin,mmax/6*0/,LimType,GenerType/1,4/
      data ApplyCentr,ApplyExt/2*.true./
      data Label/'Defined by index:'/
      save thmn,thmx
      if(Klic.gt.0) then
        if(abs(klic).eq.1) then
          Veta='.l91'
        else
          Veta='.gen'
        endif
        FileName=fln(:ifln)//Veta(1:4)
      else
        GenerType=1
        ApplyCentr=.false.
        ApplyExt=.false.
        DCREDFile=fln(:ifln)//'.dcredhkl'
        if(NDimI(KPhase).gt.0)
     1    DCREDFile=DCREDFile(:idel(DCREDFile))//'m'
        if(NDimI(KPhase).gt.1)
     1    DCREDFile=DCREDFile(:idel(DCREDFile))//'n'
        if(NDimI(KPhase).gt.2)
     1    DCREDFile=DCREDFile(:idel(DCREDFile))//'o'
      endif
      if(BatchMode) then
        LimType=1
        GenerType=4
        ApplyCentr=.true.
        ApplyExt=.true.
        thmn=0.
        thmx=30.
1100    read(BatchLN,FormA,end=9100,err=9200) Veta
        if(Veta(1:1).eq.'.') then
          backspace BatchLN
          go to 1500
        else if(Veta(1:1).eq.'#') then
          go to 1100
        endif
        k=0
1200    if(k.ge.len(Veta)) go to 1100
        call kus(Veta,k,Nazev)
        j=islovo(Nazev,IdGener,7)
        call kus(Veta,k,Cislo)
        call Posun(Cislo,1)
        read(Cislo,'(f15.5)',err=9200) Value
        IValue=nint(Value)
        if(j.eq.1) then
          GenerType=IValue
        else if(j.eq.2) then
          LimType=IValue
        else if(j.eq.3) then
          thmn=Value
          call kus(Veta,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,'(f15.5)',err=9200) thmx
        else if(j.eq.4) then
          mmin(1)=IValue
          do i=2,NDimI(KPhase)
            call kus(Veta,k,Cislo)
            call Posun(Cislo,0)
            read(Cislo,FormI15,err=9200) mmin(i)
          enddo
        else if(j.eq.5) then
          mmax(1)=IValue
          do i=2,NDimI(KPhase)
            call kus(Veta,k,Cislo)
            call Posun(Cislo,0)
            read(Cislo,FormI15,err=9200) mmax(i)
          enddo
        else if(j.eq.6) then
          ApplyCentr=IValue.eq.1
        else if(j.eq.7) then
          ApplyExt=IValue.eq.1
        endif
        go to 1200
1500    snthlmn=sin(thmn*torad)/LamAve(KDatBlock)
        snthlmx=sin(thmx*torad)/LamAve(KDatBlock)
      else
        if(first) then
          thmn=0.
          thmx=LamAve(KDatBlock)/LamAveD(6)*.5
          if(thmx.gt..999) then
            thmx=89.99
          else
            thmx=anint(asin(thmx)/ToRad*100.)/100.
          endif
          first=.false.
        endif
        id=NextQuestId()
        xqd =400.
        xqdp=xqd*.5
        if(Klic.ge.0) then
          il=9
        else
          il=5
        endif
        if(NDim(KPhase).gt.3) il=il+NDim(KPhase)-2
        call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
        il=0
        if(Klic.le.0) then
          il=il+1
          tpom=5.
          Veta='%Output DCRED file:'
          xpomp=tpom+FeTxLengthUnder(Veta)+5.
          dpomb=70.
          dpom=xqd-xpomp-dpomb-15.
          call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
          nEdwName=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,DCREDFile)
          xpomb=xpomp+dpom+10.
          Veta='%Browse'
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
          nButtBrowse=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          il=il+1
          call FeQuestLinkaMake(id,il)
        else
          nEdwName=0
          nButtBrowse=0
        endif
        il=il+1
        ill=il
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,3
          if(i.eq.1) then
            Veta='%theta'
          else if(i.eq.2) then
            Veta='%d'
          else
            Veta='%sin(theta)/lambda'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) then
            nCrwTheta=CrwLastMade
          else if(i.eq.2) then
            nCrwD=CrwLastMade
          else
            nCrwSinthl=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.LimType)
          il=il+1
        enddo
        ilk=il-1
        tpom=tpom+FeTxLength(Veta)+15.
        tpoms=tpom
        il=ill
        Veta='From'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=100.
        do i=1,2
          if(i.eq.2) Veta='To'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) nEdwLim=EdwLastMade
          il=il+1
        enddo
        il=ilk
        if(Klic.ge.0) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          xpom=5.
          tpom=xpom+CrwgXd+10.
          do i=1,4
            il=il+1
            if(i.eq.1) then
              Veta='%Full sphere'
              tpoml=tpom+FeTxLengthUnder(Veta)+40.
            else if(i.eq.2) then
              Veta='%Half sphere'
            else if(i.eq.3) then
              Veta='%Point group'
            else
              Veta='Sp%ace group'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,2)
            if(i.eq.1) then
              nCrwFull=CrwLastMade
            else if(i.eq.2) then
              nCrwHalf=CrwLastMade
            else if(i.eq.3) then
              nCrwPointGr=CrwLastMade
            else
              nCrwSpaceGr=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.GenerType)
          enddo
          il=4
          il=il+1
          ill=il
          xpom=tpoml+FeTxLength(Label)+20.
          tpom=xpom+CrwgXd+10.
          xpomp=xpom
          do i=1,3
            call FeQuestCrwMake(id,tpom,il,xpom,il,Indices(i),'L',
     1                          CrwgXd,CrwgYd,0,3)
            if(i.eq.1) nCrwH=CrwLastMade
            xpom=xpom+40.
            tpom=tpom+40.
          enddo
          il=il+1
          xpom=xpomp
          tpom=xpom+CrwgXd+10.
          Veta='P%ositive'
          do i=1,2
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,0,4)
            if(i.eq.1) then
              pom=FeTxLengthUnder(Veta)+CrwgXd+20.
              nCrwPositive=CrwLastMade
              Veta='%Negative'
              xpom=xpom+pom
              tpom=tpom+pom
            else
              nCrwNegative=CrwLastMade
            endif
          enddo
          il=il+1
          xpom=xpomp
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,'Include zero layer',
     1                        'L',CrwXd,CrwYd,0,0)
          nCrwInclZeroLayer=CrwLastMade
          il=il+1
          xpom=tpoml
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,'apply systematic '//
     1                        'extinctions','L',CrwXd,CrwYd,1,0)
          nCrwApplyExt=CrwLastMade
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,'apply cell centring',
     1                        'L',CrwXd,CrwYd,0,0)
          nCrwApplyCentr=CrwLastMade
        else
          nCrwFull=0
          nCrwHalf=0
          nCrwPointGr=0
          nCrwSpaceGr=0
          nCrwPositive=0
          nCrwNegative=0
          nCrwInclZeroLayer=0
          nCrwApplyExt=0
          nCrwApplyCentr=0
        endif
        if(NDim(KPhase).le.3) go to 2100
        il=il+1
        call FeQuestLinkaMake(id,il)
        dpom=40.
        do i=1,NDimI(KPhase)
          il=il+1
          Veta='Satellite index '//Indices(i+3)//':'
          call FeQuestLblMake(id,5.,il,Veta,'L','N')
          if(i.eq.1) then
            tpom1=FeTxLength(Veta)+40.
            tpom2=tpom1+dpom+60.
          endif
          Veta='from'
          if(i.eq.1) then
            xpom1=tpom1+FeTxLength(Veta)+5.
            xpom2=tpom2+FeTxLength(Veta)+5.
          endif
          call FeQuestEudMake(id,tpom1,il,xpom1,il,Veta,'L',dpom,EdwYd,
     1                        1)
          call FeQuestIntEdwOpen(EdwLastMade,mmin(i),.false.)
          call FeQuestEudOpen(EdwLastMade,0,mmax(i),1,0.,0.,0.)
          if(i.eq.1) nEdwMFrom=EdwLastMade
          Veta='to'
          call FeQuestEudMake(id,tpom2,il,xpom2,il,Veta,'L',dpom,EdwYd,
     1                        1)
          call FeQuestIntEdwOpen(EdwLastMade,mmax(i),.false.)
          call FeQuestEudOpen(EdwLastMade,mmin(i),10,1,0.,0.,0.)
        enddo
2100    nEdw=nEdwLim
        do i=1,2
          if(LimType.eq.2) then
            j=3-i
          else
            j=i
          endif
          if(j.eq.1) then
            pom=thmn
          else
            pom=thmx
          endif
          if(LimType.eq.2) then
            if(pom.le.0.) then
              pom=1000.
            else
              pom=.5*LamAve(KDatBlock)/sin(pom*torad)
            endif
          else if(LimType.eq.3) then
            pom=sin(pom*torad)/LamAve(KDatBlock)
          endif
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
          LimTypeOld=LimType
        enddo
        if(Klic.ge.0) then
          call FeQuestLblMake(id,tpoml,ill,Label,'L','N')
          nLbl=LblLastMade
          call FeQuestLblOff(LblLastMade)
        endif
2200    if(Klic.lt.0) go to 2500
        if(GenerType.eq.2) then
          call FeQuestLblOn(nLbl)
          nCrw=nCrwH
          do i=1,3
            call FeQuestCrwOpen(nCrw,i.eq.1)
            nCrw=nCrw+1
          enddo
          nCrw=nCrwPositive
          do i=1,2
            call FeQuestCrwOpen(nCrw,i.eq.1)
            nCrw=nCrw+1
          enddo
          call FeQuestCrwOpen(nCrwInclZeroLayer,.true.)
        else
          call FeQuestLblOff(nLbl)
          nCrw=nCrwh
          do i=1,3
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
          nCrw=nCrwPositive
          do i=1,2
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestCrwDisable(nCrwInclZeroLayer)
        endif
        if(GenerType.le.3) then
          call FeQuestCrwOpen(nCrwApplyExt,ApplyExt)
          if(NLattVec(KPhase).gt.1)
     1      call FeQuestCrwOpen(nCrwApplyCentr,ApplyCentr)
        else
          call FeQuestCrwDisable(nCrwApplyCentr)
          call FeQuestCrwDisable(nCrwApplyExt)
        endif
2500    call FeQuestEvent(id,ich)
        nEdw=nEdwLim
        do i=1,2
          j=i
          call FeQuestRealFromEdw(nEdw,pom)
          if(LimTypeOld.eq.2) then
            j=3-i
            if(pom.gt.0.) then
              if(pom.gt.100.) then
                pom=0.
              else
                pom=.5*LamAve(KDatBlock)/pom
              endif
            else
              pom=2.
            endif
          else if(LimTypeOld.eq.3) then
            pom=LamAve(KDatBlock)*pom
          endif
          if(LimTypeOld.ne.1) then
            if(pom.ge.1.) then
              pom=89.99
            else
              pom=anint(asin(pom)/torad*100.)/100.
            endif
          endif
          if(j.eq.1) then
            thmn=pom
          else
            thmx=pom
          endif
          nEDw=nEdw+1
        enddo
        if(CheckType.eq.EventCrw) then
          if(CheckNumber.ge.nCrwTheta.and.
     1       CheckNumber.le.nCrwSinthl) then
            LimType=CheckNumber-nCrwTheta+1
            go to 2100
          else if(CheckNumber.ge.nCrwFull.and.
     1            CheckNumber.le.nCrwSpaceGr) then
            GenerType=CheckNumber-nCrwFull+1
            go to 2200
          else if(CheckNumber.eq.nCrwApplyExt) then
            if(CrwLogicQuest(nCrwApplyExt)) then
              call FeQuestCrwDisable(nCrwApplyCentr)
            else
              if(NLattVec(KPhase).gt.1)
     1          call FeQuestCrwOpen(nCrwApplyCentr,.true.)
            endif
            go to 2500
          endif
        else if(CheckType.eq.EventEdw) then
          if(CheckNumber.ge.nEdwMFrom.and.
     1       CheckNumber.le.nEdwMFrom+NDimI(KPhase)*2-1) then
            i=CheckNumber-nEdwMFrom
            n=i/2+1
            if(mod(i,2).eq.0) then
              call FeQuestIntFromEdw(CheckNumber,j)
              call FeQuestEudOpen(CheckNumber+1,j,10,1,0.,0.,0.)
            else
              call FeQuestIntFromEdw(CheckNumber,j)
              call FeQuestEudOpen(CheckNumber-1,0,j,1,0.,0.,0.)
            endif
            go to 2500
          endif
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        if(ich.eq.0) then
          if(Klic.lt.0) then
            DCREDFile=EdwStringQuest(nEdwName)
            FileName=DCREDFile
          endif
          call FeQuestRealFromEdw(nEdwLim,pmin)
          call FeQuestRealFromEdw(nEdwLim+1,pmax)
          if(LimType.eq.1) then
            thmn=min(pmin,89.99)
            thmx=min(pmax,89.99)
            snthlmn=sin(thmn*torad)/LamAve(KDatBlock)
            snthlmx=sin(thmx*torad)/LamAve(KDatBlock)
          else if(LimType.eq.2) then
            if(pmin.gt.0.) then
              if(pmin.gt.100.) then
                snthlmx=0.
              else
                snthlmx=.5/pmin
              endif
            else
              snthlmx=2.
            endif
            if(pmax.gt.0.) then
              if(pmax.gt.100.) then
                snthlmn=0.
              else
                snthlmn=.5/pmax
              endif
            else
              snthlmn=2.
            endif
          else
            snthlmn=pmin
            snthlmx=pmax
          endif
          if(LimType.ne.1) then
            pom=snthlmn*LamAve(KDatBlock)
            if(pom.ge.1.) then
              thmn=89.99
            else
              thmn=anint(asin(pom)/torad*100.)/100.
            endif
            pom=snthlmx*LamAve(KDatBlock)
            if(pom.ge.1.) then
              thmx=89.99
            else
              thmx=anint(asin(pom)/torad*100.)/100.
            endif
            snthlmn=sin(thmn*torad)/LamAve(KDatBlock)
            snthlmx=sin(thmx*torad)/LamAve(KDatBlock)
          endif
          if(NDim(KPhase).gt.3) then
            nEdw=nEdwMFrom
            do i=1,NDimI(KPhase)
              call FeQuestIntFromEdw(nEdw,mmin(i))
              nEdw=nEdw+1
              call FeQuestIntFromEdw(nEdw,mmax(i))
              nEdw=nEdw+1
            enddo
          endif
          if(Klic.ge.0) then
            if(GenerType.eq.2) then
              nCrw=nCrwH
              do i=1,3
                if(CrwLogicQuest(nCrw)) then
                  IndexPositive=i
                  go to 3110
                endif
                nCrw=nCrw+1
              enddo
3110          if(CrwLogicQuest(nCrwPositive)) then
                zn= 1.
              else
                zn=-1.
              endif
              InclZeroLayer=CrwLogicQuest(nCrwInclZeroLayer)
            endif
            ApplyCentr=.true.
            ApplyExt  =.true.
            if(GenerType.ne.4) then
              if(CrwStateQuest(nCrwApplyCentr).eq.CrwOn.or.
     1           CrwStateQuest(nCrwApplyCentr).eq.CrwOff)
     2          ApplyCentr=CrwLogicQuest(nCrwApplyCentr)
              ApplyExt=CrwLogicQuest(nCrwApplyExt)
            endif
            if(GenerType.eq.3.and.ApplyCentr.and.ApplyExt) GenerType=4
          endif
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9900
      endif
      call OpenFile(91,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      if(GenerType.eq.1.or.GenerType.eq.2) then
        call SetIntArrayTo(NSymm,NPhase,1)
      else if(GenerType.eq.3) then
        do KPh=1,NPhase
          do i=1,NComp(KPh)
            call SetRealArrayTo(s6(1,1,i,KPh),6*NSymm(KPhase),0.)
          enddo
        enddo
      endif
      if(GenerType.le.3) then
        if(.not.ApplyCentr) call SetIntArrayTo(NLattVec,NPhase,1)
      endif
      if(lng.gt.0) then
        if(StructureName.eq.' ') then
        Veta='????'
      else
        Veta=StructureName
      endif
      write(lng,'(''# Name : '',a)') Veta(:idel(Veta))
      write(lng,'(''# Cell parameters : '',3f10.4,3f10.3)')
     1           (CellPar(i,1,KPhase),i=1,6)
      write(lng,'(''# theta runs from '',f8.3,'' to '',f8.3)') thmn,
     1                                                         thmx
      do i=1,NDimI(KPhase)
          write(lng,'(''# satellite index "'',a1,''" runs from'',i3,
     1                '' to'',i3)') indices(i+3),mmin(i),mmax(i)
        enddo
        if(GenerType.eq.1) then
          Veta='in full sphere'
        else if(GenerType.eq.2) then
          Veta='in half sphere'
        else if(GenerType.eq.3) then
          Veta='according to point group'
        else if(GenerType.eq.4) then
          Veta='according to space group'
        endif
        write(lng,'(''# reflection generated '',a)') Veta(:idel(Veta))
        if(GenerType.eq.2) then
          Veta='# index "'//indices(IndexPositive)//'" will be kept'
          if(zn.gt.0.) then
            if(InclZeroLayer) then
              Cislo='non-negative'
            else
              Cislo='positive'
            endif
          else
            if(InclZeroLayer) then
              Cislo='non-positive'
            else
              Cislo='negative'
            endif
          endif
          Veta=Veta(:idel(Veta)+1)//Cislo(:idel(Cislo))
          write(lng,FormA) Veta(:idel(Veta))
        endif
        if(GenerType.ne.4) then
          if(NLattVec(KPhase).gt.1) then
            Veta='# The cell centering will'
            if(ApplyCentr) then
              Veta=Veta(:idel(Veta))//' be applied'
            else
              Veta=Veta(:idel(Veta))//' not be applied'
            endif
            write(lng,FormA) Veta(:idel(Veta))
          endif
          Veta='# Systematically absent reflection were'
          if(ApplyExt) then
            Veta=Veta(:idel(Veta))//' skipped'
          else
            Veta=Veta(:idel(Veta))//' generated'
          endif
          write(lng,FormA) Veta(:idel(Veta))
        endif
      endif
      call GenerRef('Generation of reflections',snthlmx,mmax,.false.,
     1              abs(Klic))
      if(ErrFlag.ne.0) go to 9900
      if(snthlmn.gt..0001) go to 4000
      do i=1,NDimI(KPhase)
        if(mmin(i).gt.0) go to 4000
      enddo
      if(GenerType.eq.2) go to 4000
      if((GenerType.eq.1.or.GenerType.eq.3).and.ApplyExt) go to 4000
      go to 5500
4000  close(91)
      call iom50(0,0,fln(:ifln)//'.m50')
      call comsym(0,0,ich)
      TmpFile='jgen'
      call CreateTmpFile(TmpFile,i,0)
      call FeTmpFilesAdd(TmpFile)
      call CopyFile(FileName,TmpFile)
      call OpenFile(91,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      call OpenFile(92,TmpFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
4100  read(92,FormA,end=5000) Veta
      read(Veta,'(6i4)')(ih(i),i=1,NDim(KPhase))
      if(ih(1).gt.900) go to 5000
      do i=4,NDim(KPhase)
        if(abs(ih(i)).lt.mmin(i-3)) go to 4100
      enddo
      if(ApplyExt) then
        if(SystExtRef(ih,.false.,1)) go to 4100
      endif
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      if(sinthl.lt.snthlmn) go to 4100
      if(GenerType.eq.2) then
        if(zn*h(IndexPositive).gt.-.0001) then
          if(abs(h(IndexPositive)).lt..0001.and..not.InclZeroLayer)
     1      go to 4100
        else
          go to 4100
        endif
      endif
      write(91,FormA) Veta(:idel(Veta))
      go to 4100
5000  call DeleteFile(TmpFile)
      call FeTmpFilesClear(TmpFile)
      rewind(91)
5500  if(lng.gt.0) then
        nn=0
6000    read(91,FormA,end=6100) Veta
        read(Veta,'(6i4)')(ih(i),i=1,NDim(KPhase))
        if(ih(1).gt.900) go to 6100
        nn=nn+1
        go to 6000
6100    write(lng,'(''# number of generated reflections :'',i6)') nn
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call comsym(0,0,ich)
      go to 9900
9100  TmpFile='end of file read from the batch file.'
      Veta=' '
      go to 9500
9200  TmpFile='reading error occured on the batch file.'
9500  call FeChybne(-1.,-1.,TmpFile,Veta,SeriousError)
9900  if(ich.ne.0) ErrFlag=1
      call CloseifOpened(91)
      call CloseifOpened(92)
      go to 9999
      entry GenerQuestReset
      First=.true.
9999  return
      end
