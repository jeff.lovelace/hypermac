      subroutine CrlMakeSimStr
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      dimension h(3),iho(:,:),ihg(:,:)
      character*256 t256,p256,Veta,EdwStringQuest,FileName,flnOld
      character*60 format83a
      character*1  t1
      integer FeChdir,RolMenuSelectedQuest,RadiationP,CrwStateQuest,
     1        EdwStateQuest,ihitw(6,18)
      logical StructureExists,FeYesNo,eqiv,FromM90,CrwLogicQuest,
     1        EqIgCase,UseBckg,isPowderOld,ExistFile,RefineEnd
      real Iobs,Icalc,IcalcMax,LamAveP,LamA1P,LamA2P,LamRatP
      data FromM90,UseBckg/2*.false./,FileName/' '/
      allocatable iho,ihg
      data SigLev,BckgLevConst,BckgLevLorentz,thd/3*1.,15./
      flnOld=fln
      ln=0
      lno=0
      ich=0
      id=NextQuestId()
      xqd=400.
      il=6
      if(ExistM90.and.DataType(KDatBlock).eq.1) il=il+3
      if(NPhase.gt.1) il=il+1
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Simulated structure',1,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Name of the simulated structure:'
      xpomp=tpom+FeTxLengthUnder(Veta)+5.
      dpomb=70.
      dpom=xqd-xpomp-dpomb-15.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      xpomb=xpomp+dpom+10.
      Veta='%Browse'
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(ExistM90.and.DataType(KDatBlock).eq.1) then
        xpom=5.
        tpom=xpom+CrwgXd+5.
        do i=1,2
          il=il+1
          if(i.eq.1) then
            Veta='%Use indices and sig(I) from M90'
          else
            Veta='%Generate indices within a specified region'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                              CrwgYd,1,1)
          if(i.eq.1) then
            nCrwM90=CrwLastMade
          else
            nCrwGener=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.FromM90)
        enddo
        il=il+1
        call FeQuestLinkaMake(id,il)
        nLinka=LinkaLastMade
      else
        nCrwM90=0
        nCrwGener=0
        nLinka=0
      endif
      tpom=5.
      il=il+1
      Veta='N%oise level:'
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwSigLev=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,SigLev,.false.,.false.)
      il=il+1
      Veta='%Model background for calculation of sig(I):'
      xpom=tpom+CrwXd+5.
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwBckg=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseBckg)
      il=il+1
      Veta='%Constatnt backgroud level:'
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwBckgLevConst=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,BckgLevConst,.false.,.false.)
      Veta='%Draw background'
      xpomb=xpomp+dpom+25.
      dpomb=FeTxLengthUnder(Veta)+25.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtonDrawBackg=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='%Lozentzian backgroud level:'
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwBckgLevLorentz=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,BckgLevLorentz,.false.,
     1                        .false.)
      il=il+1
      Veta='Lorentzian backgroud %halfwidth:'
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwThHalfWidth=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,thd,.false.,.false.)
      if(NPhase.gt.1) then
        il=il+1
        tpom=5.
        Veta='Select %phase to be simulated:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=100.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                          EdwYd,1)
        nRolMenuPhase=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,1)
      else
        nRolMenuPhase=0
      endif
      nph=1
1400  if(nCrwM90.ne.0) then
        if(CrwLogicQuest(nCrwM90)) then
          UseBckg=CrwLogicQuest(nCrwBckg)
          call FeQuestCrwDisable(nCrwBckg)
        else
          call FeQuestCrwOpen(nCrwBckg,UseBckg)
        endif
      endif
1450  if((CrwStateQuest(nCrwBckg).eq.CrwOn.or.
     1    CrwStateQuest(nCrwBckg).eq.CrwDisabled).and.
     2   EdwStateQuest(nCrwBckg).ne.EdwOpened) then
        call FeQuestRealEdwOpen(nEdwBckgLevConst,BckgLevConst,.false.,
     1                          .false.)
        call FeQuestRealEdwOpen(nEdwBckgLevLorentz,BckgLevLorentz,
     1                          .false.,.false.)
        call FeQuestRealEdwOpen(nEdwThHalfWidth,thd,.false.,.false.)
        call FeQuestButtonOpen(nButtonDrawBackg,ButtonOff)
      endif
      if((CrwStateQuest(nCrwBckg).eq.CrwOff.or.
     1    CrwStateQuest(nCrwBckg).eq.CrwDisabled).and.
     1   EdwStateQuest(nCrwBckg).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwBckgLevConst,BckgLevConst)
          call FeQuestRealFromEdw(nEdwBckgLevLorentz,BckgLevLorentz)
          call FeQuestRealFromEdw(nEdwThHalfWidth,thd)
          call FeQuestEdwDisable(nEdwBckgLevConst)
          call FeQuestEdwDisable(nEdwBckgLevLorentz)
          call FeQuestEdwDisable(nEdwThHalfWidth)
          call FeQuestButtonDisable(nButtonDrawBackg)
      endif
1500  call FeQuestEvent(id,ich)
      FileName=EdwStringQuest(nEdwName)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        call ExtractFileName(FileName,Veta)
        if(FileName.eq.' ') then
          Veta='the name of simulated structure hasn''t been '//
     1         'defined, try again.'
          call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        else if(EqIgCase(FileName,FlnOld)) then
          Veta='the name of simulated structure must be different '//
     1         'from the parent structure, try again.'
          call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        else if(StructureExists(FileName)) then
          if(FeYesNo(-1.,YBottomMessage,'The structure "'//
     1               Veta(:idel(Veta))//
     2               '" already exists, rewrite it?',0))
     3      QuestCheck(id)=0
        else
          QuestCheck(id)=0
        endif
        EventType=EventEdw
        EventNumber=nEdwName
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        FileName=EdwStringQuest(nEdwName)
        call FeFileManager('Define name of the simulated structure',
     1                     FileName,' ',1,.false.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwName,FileName)
        call FeQuestButtonOff(nButtBrowse)
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtonDrawBackg) then
        call FeQuestRealFromEdw(nEdwBckgLevConst,BckgLevConst)
        call FeQuestRealFromEdw(nEdwBckgLevLorentz,BckgLevLorentz)
        call FeQuestRealFromEdw(nEdwThHalfWidth,thd)
        call CrlDrawBackg(BckgLevConst,BckgLevLorentz,thd)
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.eq.nCrwM90.or.CheckNumber.eq.nCrwGener)) then
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwBckg) then
        go to 1450
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuPhase) then
        nph=RolMenuSelectedQuest(nRolMenuPhase)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwSigLev,SigLev)
        call FeQuestRealFromEdw(nEdwBckgLevConst,BckgLevConst)
        call FeQuestRealFromEdw(nEdwBckgLevLorentz,BckgLevLorentz)
        call FeQuestRealFromEdw(nEdwThHalfWidth,thd)
        if(ExistM90.and.DataType(KDatBlock).eq.1) then
          FromM90=CrwLogicQuest(nCrwM90)
        else
          FromM90=.false.
        endif
        UseBckg=CrwLogicQuest(nCrwBckg)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9900
      if(ExistM90) then
        call iom90(0,fln(:ifln)//'.m90')
      else
        call iom50(0,0,fln(:ifln)//'.m50')
        if(MagneticType(KPhase).gt.0) then
          RadiationP=NeutronRadiation
          LamAveP=.5
          LamA1P=.5
          LamA2P=.5
        else
          RadiationP=XRayRadiation
        endif
        Radiation(KDatBlock)=RadiationP
      endif
      if(KRefLam(KDatBlock).ne.0) then
        LamA1P=LamPwd(1,KDatBlock)
        LamA2P=LamPwd(1,KDatBlock)
        LamAveP=LamPwd(1,KDatBlock)
      else
        if(LamAve(KDatBlock).lt.0.) then
          LamAveP=LamAveD(6)
          LamA1P=LamA1D(6)
          LamA2P=LamA2D(6)
        else
          LamAveP=LamAve(KDatBlock)
          LamA1P=LamA1(KDatBlock)
          LamA2P=LamA2(KDatBlock)
        endif
      endif
      NAlfaP=NAlfa(KDatBlock)
      LamRatP=LamRat(KDatBlock)
      LpFactorP=LpFactor(KDatBlock)
      AngleMonP=AngleMon(KDatBlock)
      RadiationP=Radiation(KDatBlock)
      call CopyFile(fln(:ifln)//'.m40',FileName(:idel(FileName))//
     1              '.m40')
      call CopyFile(fln(:ifln)//'.m50',FileName(:idel(FileName))//
     1              '.m50')
      call DeleteFile(FileName(:idel(FileName))//'.m90')
      call DeletePomFiles
      call ExtractDirectory(FileName,p256)
      i=FeChdir(p256)
      call FeGetCurrentDir
      fln=Veta
      ifln=idel(fln)
      call FeBottomInfo(' ')
      call DeletePomFiles
      do i=1,NDatBlock
        if(i.ne.KDatBlock) NRef90(i)=0
      enddo
      call iom50(0,0,fln(:ifln)//'.m50')
      call UpdateDatBlocksM40
      NDatBlockIn=NDatBlock
      NDatBlock=1
      call iom40(1,0,fln(:ifln)//'.m40')
      if(ExistM41) then
        call DeleteFile(fln(:ifln)//'.m41')
        ExistM41=.false.
      endif
      KDatBlock=1
      DataType(1)=1
      LamAve(1)=LamAveP
      LamA1(1)=LamA1P
      LamA2(1)=LamA2P
      NAlfa(1)=NAlfaP
      LamRat(1)=LamRatP
      LpFactor(1)=LpFactorP
      AngleMon(1)=AngleMonP
      Radiation(1)=RadiationP
      ExistM95=.false.
      if(ExistM90) then
        p256=flnOld(:idel(flnOld))//'.m90'
        if(DataType(KDatBlock).eq.1) then
          if(NDatBlockIn.gt.1) then
            call iom90(1,p256)
            lni=NextLogicNumber()
            call OpenFile(lni,p256,'formatted','unknown')
            if(ErrFlag.ne.0) go to 9900
            lno=NextLogicNumber()
            call OpenFile(lno,fln(:ifln)//'.m90','formatted','unknown')
1900        read(lni,FormA,end=1920) t256
            write(lno,FormA) t256(:idel(t256))
            if(.not.EqIgCase(t256,'end')) go to 1900
1920        close(lni,status='delete')
            lni=NextLogicNumber()
            call OpenDatBlockM90(lni,KDatBlock,p256)
            if(ErrFlag.ne.0) go to 9900
            write(lno,'(''Data Block#1'')')
1940        read(lni,FormA,end=1960) t256
            if(t256.eq.' ') go to 1940
            k=0
            call kus(t256,k,Cislo)
            if(EqIgCase(Cislo,'data')) go to 1960
            write(lno,FormA) t256(:idel(t256))
            go to 1940
1960        close(lni)
            close(lno)
          else
            call CopyFile(p256,fln(:ifln)//'.m90')
          endif
        else
          ExistM90=.false.
        endif
      endif
      if(ExistM90) then
        call iom90(0,fln(:ifln)//'.m90')
        if(.not.FromM90) call DeleteFile(fln(:ifln)//'.m90')
      else
        ExistSingle=.false.
      endif
      isPowderOld=isPowder
      isPowder=.false.
      ExistPowder=.false.
      NDatBlock=1
      KDatBlock=1
      DataType(1)=1
      call PwdSetTOF(1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      isPowder=isPowderOld
      j=1
      do i=1,NPhase
        if(i.ne.nph) then
          call DeletePhaseBatch(j)
        else
          j=j+1
        endif
      enddo
      isPowder=.false.
      KPhase=1
      call RefOpenCommands
      NacetlInt(nCmdncykl)=0
      NacetlInt(nCmdCallFour)=0
      NacetlInt(nCmdkim)=-2
      if(FromM90) then
        k=0
      else
        k=1
        if(.not.ExistM90.or.isPowderOld) then
          sc(1,1)=1.
          call SetRealArrayTo(ec,12*MxDatBlock,0.)
          ExtTensor(1)=0
          call SetIntArrayTo(KiS(Mxsc+1,1),13,0)
        endif
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      NacetlInt(nCmdisimul)=k
      NacetlInt(nCmdDoLeBail)=0
      call RefRewriteCommands(1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      UseEFormat91=.false.
      Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
      write(Format91(2:2),'(i1)') NDim(KPhase)
      ShowInfoOnScreen=.false.
      call GenerQuestReset
      call Refine(0,RefineEnd)
      if(ErrFlag.eq.0) then
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        call CopyFile(fln(:ifln)//'.m50',PreviousM50)
        call CopyFile(fln(:ifln)//'.m40',PreviousM40)
        if(ExistPowder) call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      else if(ErrFlag.ne.0) then
        go to 9900
      endif
      if(FromM90) then
        lni=NextLogicNumber()
        call OpenDatBlockM90(lni,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 9900
        nalloc=1000
        allocate(ihg(NDim(KPhase),nalloc))
        NFromM90=0
2220    read(lni,FormA,end=2250) t256
        if(t256.eq.' ') go to 2220
        k=0
        call kus(t256,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 2250
        read(t256,format91,end=2250)(ih(i),i=1,NDim(KPhase))
        NFromM90=NFromM90+1
        if(NFromM90.gt.nalloc) then
          allocate(iho(NDim(KPhase),nalloc))
          call CopyVekI(ihg,iho,nalloc*NDim(KPhase))
          nalloco=nalloc
          deallocate(ihg)
          nalloc=nalloc+1000
          allocate(ihg(NDim(KPhase),nalloc))
          call CopyVekI(iho,ihg,nalloco*NDim(KPhase))
          deallocate(iho)
        endif
        call CopyVekI(ih,ihg(1,NFromM90),NDim(KPhase))
        go to 2220
2250    call CloseIfOpened(lni)
      endif
      NDatBlock=1
      call SetBasicM90(1)
      DataType(1)=1
      LamAve(1)=LamAveP
      LamA1(1)=LamA1P
      LamA2(1)=LamA2P
      NAlfa(1)=NAlfaP
      LamRat(1)=LamRatP
      LpFactor(1)=LpFactorP
      AngleMon(1)=AngleMonP
      Radiation(1)=RadiationP
      NRef90(1)=0
      call iom40(1,0,fln(:ifln)//'.m40')
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
      if(ErrFlag.ne.0) go to 9900
      IcalcAve=0.
      m=0
      if(.not.FromM90) then
        IcalcMax=0.
        IcalcAve=0.
        read(ln,FormA) t256
        i=LocateSubstring(t256,'e',.false.,.true.)
        if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
          format83a=format83e
        else
          format83a=format83
        endif
        rewind ln
2500    read(ln,format83a,end=2600,err=9000)
     1                     (ih(i),i=1,NDim(KPhase)),Icalc,Iobs,SigIobs,
     2                     t1,itw,pom,pom,pom,pom,pom
        Iobs=Iobs/pom
        Icalc=Icalc/pom
        IcalcAve=IcalcAve+Icalc
        m=m+1
        IcalcMax=max(Icalc,IcalcMax)
        go to 2500
2600    if(IcalcMax.gt.0..and..not.ExistM90) then
          sc(1,1)=sqrt(100000./IcalcMax)
          call iom40(1,0,fln(:ifln)//'.m40')
          scp=sc(1,1)**2
        else
          scp=1.
        endif
        rewind ln
        IcalcAve=IcalcAve/float(m)
        if(UseBckg) then
          BckgConst  =IcalcAve*BckgLevConst  *scp
          BckgLorentz=IcalcAve*BckgLevLorentz*scp
        endif
      endif
      t256=flnOld(:idel(flnOld))//'.m95'
      if(ExistFile(t256)) then
        ExistM95=.true.
        call iom95(0,t256)
        ExistM95=.false.
        InsParFileRefBlock(KRefBlock)=' '
        IncSpectFileRefBlock(KRefBlock)=' '
      else
        KRefBlock=1
        NRefBlock=1
        call SetBasicM95(KRefBlock)
      endif
      LamAveRefBlock(KRefBlock)=LamAveP
      RadiationRefBlock(KRefBlock)=RadiationP
      if(RadiationP.eq.NeutronRadiation) then
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
      else
        PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
        AngleMonRefBlock(KRefBlock)=
     1    CrlMonAngle(MonCell(1,3),MonH(1,3),LamAveRefBlock(KRefBlock))
      endif
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      TempRefBlock(KRefBlock)=293
      NDim95(KRefBlock)=NDim(KPhase)
      CorrLp(KRefBlock)=-1
      CorrAbs(KRefBlock)=-1
      call UnitMat(TrMP,NDim(KPhase))
      DifCode(KRefBlock)=IdImportSimul
      SourceFileRefBlock(KRefBlock)='?The strucure simulated from "'//
     1                               flnOld(:idel(flnOld))//'"'
      SourceFileDateRefBlock(KRefBlock)='?'
      SourceFileTimeRefBlock(KRefBlock)='?'
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,0),3)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,KRefBlock),3)
      enddo
      CellReadIn(KRefBlock)=.true.
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      call SetRealArrayTo(uhly,4,0.)
      call SetRealArrayTo(dircos,6,0.)
      call SetRealArrayTo(corrf,2,1.)
      call SetIntArrayTo(iflg,2,1)
      iflg(3)=0
      tbar=0.
      KProf=0
      NProf=0
      DRlam=0.
      RefBlockFileName=fln(:ifln)//'.l01'
      t256='     0 reflections already processed'
      write(t256(1:6),100) 0
      call FeTxOut(-1.,-1.,t256)
      lno=NextLogicNumber()
      call OpenFile(lno,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      if(FromM90) IFromM90=1
      NRef95(KRefBlock)=0
      NLines95(KRefBlock)=0
      read(ln,FormA) p256
      i=LocateSubstring(p256,'e',.false.,.true.)
      if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
        format83a=format83e
      else
        format83a=format83
      endif
      rewind ln
3000  read(ln,format83a,end=4000,err=9000)
     1                 (ih(i),i=1,NDim(KPhase)),Icalc,Iobs,SigIobs,t1,
     2                 itwr,pom,pom,pom,pom,pom
      Iobs=Iobs/pom
      Icalc=Icalc/pom
      SigIobs=SigIobs/pom
      call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
      pom=sinthl*LamAve(KDatBlock)
      if(pom.lt..999) then
        th=asin(pom)/ToRad
      else
        th=90.
      endif
      if(FromM90) then
        SigIobsP=SigIobs
        do i=IFromM90,NFromM90
          if(eqiv(ih,ihg(1,i),NDim(KPhase))) then
            IFromM90=i+1
            go to 3300
          endif
        enddo
        go to 3000
      else
        Icalc=Icalc*scp
        SigIobsP=sqrt(Icalc)
        if(UseBckg) then
          SigIobs=sqrt(SigIobsP**2+BckgLorentz/(1.+(th/thd)**2)+
     1                BckgConst)
        else
          SigIobs=SigIobsP
        endif
      endif
3300  Delta=SigIobsP*gasdev()*SigLev
      ri=Icalc+Delta
      rs=SigIobs
      NRef95(KRefBlock)=NRef95(KRefBlock)+1
      if(mod(NRef95(KRefBlock),50).eq.0) then
        write(t256(1:6),100) NRef95(KRefBlock)
        call FeTxOutCont(t256)
      endif
      expos=float(NRef95(KRefBlock))*.1
      no=NRef95(KRefBlock)
      iflg(2)=itwr
      call DRPutReflectionToM95(lno,n)
      NLines95(KRefBlock)=NLines95(KRefBlock)+n
      go to 3000
4000  call CloseIfOpened(lno)
      write(t256(1:6),100) NRef95(KRefBlock)
      call FeTxOutCont(t256)
      call FeTxOutEnd
      call iom95(1,fln(:ifln)//'.m95')
      call CompleteM95(0)
      ExistM95=.true.
      ExistM90=.false.
      call iom50(0,0,fln(:ifln)//'.m50')
      call EM9CreateM90(0,ich)
      if(KPhase.eq.1.and.abs(EM9ScToM90(KDatBlock)-1.).gt..01) then
        sc(1,1)=sc(1,1)*sqrt(EM9ScToM90(KDatBlock))
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      call RefOpenCommands
      NacetlInt(nCmdisimul)=0
      call RefRewriteCommands(1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      call CloseIfOpened(90)
      if(ExistM41) call DeleteFile(fln(:ifln)//'.m41')
      if(VasekTest.ne.0.and.ExistFile(fln(:ifln)//'.m90'))
     1  call CrlIoSigIoPlots
      go to 9999
9000  call FeReadError(ln)
9900  fln=flnold
      ifln=idel(fln)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      if(allocated(ihg)) deallocate(ihg)
      if(allocated(iho)) deallocate(iho)
      return
100   format(i6)
      end
