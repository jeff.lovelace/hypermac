      subroutine SetFormF(sinthl)
      use Basic_mod
      use EDZones_mod
      use fscattf_module
      include 'fepc.cmn'
      include 'basic.cmn'
      logical EqIgCase,FirstTypePlus,FirstTypeMinus
      double precision sthl,u,E0,freal,fimag
      sinthlq=sinthl**2
      call SetRealArrayTo(fx,NAtFormula(KPhase),0.)
      FirstTypePlus=.true.
      FirstTypeMinus=.true.
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        do i=1,NAtFormula(KPhase)
          if(FFType(KPhase).gt.0) then
            pt=sinthl/.05+1.
            ipt=ifix(pt)-1
            do k=1,4
              pom=1.
              do l=1,4
                if(k.ne.l) pom=pom*(pt-float(ipt+l))/float(k-l)
              enddo
              l=k+ipt
              if(l.gt.0) then
                if(l.le.FFType(KPhase)) then
                  ffp=FFBasic(l,i,KPhase)
                else
                  ffp=FFBasic(FFType(KPhase),i,KPhase)
                endif
              else
                ffp=FFBasic(1,i,KPhase)
              endif
              fx(i)=fx(i)+pom*ffp
            enddo
          else if(FFType(KPhase).eq.-9) then
            fx(i)=FFBasic(9,i,KPhase)
            do k=1,7,2
              arg=-sinthlq*FFBasic(k+1,i,KPhase)
              if(arg.lt.-60.) cycle
              fx(i)=fx(i)+FFBasic(k,i,KPhase)*exp(arg)
            enddo
          else
            if(FirstTypeMinus) then
              do 2100k=1,2
                do j=1,56
                  if(k.eq.1) then
                    ffxj=ffx(j)
                  else
                    ffxj=ffxh(j)
                  endif
                  if(sinthl.lt.ffxj) then
                    if(k.eq.1) then
                      jpn=min(j,52)
                      xpn=(sinthl-ffx(jpn))/(ffx(jpn-1)-ffx(jpn))
                    else
                      jph=min(j,52)
                      xph=(sinthl-ffxh(jph))/(ffxh(jph-1)-ffxh(jph))
                    endif
                    go to 2100
                  endif
                enddo
2100          continue
              FirstTypeMinus=.false.
            endif
            if(EqIgCase(AtType(i,KPhase),'H').or.
     1         EqIgCase(AtType(i,KPhase),'D')) then
              jp=jph
              xp=xph
            else
              jp=jpn
              xp=xpn
            endif
            pom=ffa(4,jp,i,KPhase)
            do k=3,1,-1
              pom=pom*xp+ffa(k,jp,i,KPhase)
            enddo
            fx(i)=pom
          endif
        enddo
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        call CopyVek(ffn(1,KPhase),fx,NAtFormula(KPhase))
        do i=1,NAtFormula(KPhase)
          if(AtTypeMag(i,KPhase).ne.' ') then
            fm(i)=FFMag(7,i,KPhase)
            do k=1,5,2
              arg=-sinthlq*FFMag(k+1,i,KPhase)
              if(arg.lt.-60.) cycle
              fm(i)=fm(i)+FFMag(k,i,KPhase)*exp(arg)
            enddo
            if(TypeFFMag(i,KPhase).ne.0) fm(i)=fm(i)*sinthlq
          else
            fm(i)=0.
          endif
        enddo
      else if(Radiation(KDatBlock).eq.ElectronRadiation) then
        if(UseWKS.and.CalcDyn) then
          E0=E0ED
          u=UisoED
          sthl=sinthl
          do i=1,NAtFormula(KPhase)
            call WKS(nint(AtNum(i,KPhase)),sthl,u,E0,AbsFlagED,freal,
     1               fimag)
            fx(i)=freal
            FFra(i,KPhase,KDatBlock)=0.
            FFia(i,KPhase,KDatBlock)=fimag*LamAve(KDatBlock)*GammaED
          enddo
        else
          do i=1,NAtFormula(KPhase)
            if(FirstTypeMinus) then
              do j=1,56
                ffxj=ffx(j)
                if(sinthl.lt.ffxj) then
                  jp=min(j,52)
                  xp=(sinthl-ffx(jp))/(ffx(jp-1)-ffx(jp))
                  exit
                endif
              enddo
            endif
            FirstTypeMinus=.false.
            pom=ffae(4,jp,i,KPhase)
            do k=3,1,-1
              pom=pom*xp+ffae(k,jp,i,KPhase)
            enddo
            fx(i)=pom
            FFra(i,KPhase,KDatBlock)=0.
            FFia(i,KPhase,KDatBlock)=0.
          enddo
        endif
      endif
      return
      end
