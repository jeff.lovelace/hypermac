      subroutine DeletePhase
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer PhaseDel(MxPhases),KiTwin(MxPhases)
      logical FeYesNo,CrwLogicQuest
      character*80 Veta
      logical Prvni
      call SetIntArrayTo(PhaseDel,NPhase,1)
      if(.not.isPowder) PhaseDel(1)=0
      id=NextQuestId()
      xqd=250.
      il=NPhase
      Veta='Select the phase to be deleted:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Prvni=.true.
      do i=1,NPhase
        il=il+1
        if(i.eq.1.and..not.isPowder) then
          Veta='The reference "'//
     1         PhaseName(i)(:idel(PhaseName(i)))//'" cannot be deleted'
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        else
          call FeQuestCrwMake(id,tpom,il,xpom,il,PhaseName(i),'L',CrwXd,
     1                        CrwYd,0,1)
          call FeQuestCrwOpen(CrwLastMade,Prvni)
          if(Prvni) nCrwFirst=CrwLastMade
          Prvni=.false.
        endif
      enddo
      call FeQuestEvent(id,ich)
      if(ich.eq.0) then
        nCrw=nCrwFirst
        do i=1,NPhase
          if(i.ge.2.or.isPowder) then
            if(CrwLogicQuest(nCrw)) then
              KPhDel=i
              call FeQuestRemove(id)
              go to 1030
            endif
            nCrw=nCrw+1
          endif
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
1030  if(FeYesNo(-1.,-1.,'Do you really want to delete phase "'//
     1           PhaseName(KPhDel)(:idel(PhaseName(KPhDel)))//'"?',0))
     2  then
        go to 1100
      else
        go to 9999
      endif
      entry DeletePhaseBatch(KPhDelIn)
      KPhDel=KPhDelIn
1100  if(KPhDel.lt.NPhase) then
        call AtSun(NAtIndToAll(KPhDel)+1,NAtInd,NAtIndFrAll(KPhDel))
        do KPh=KPhDel+1,NPhase
          do isw=1,NComp(KPh)
            NAtIndFr(isw,KPh-1)=NAtIndFr(isw,KPh)
            NAtIndTo(isw,KPh-1)=NAtIndTo(isw,KPh)
            NAtIndLen(isw,KPh-1)=NAtIndLen(isw,KPh)
          enddo
          NAtIndFrAll(KPh-1)=NAtIndFrAll(KPh)
          NAtIndToAll(KPh-1)=NAtIndToAll(KPh)
          NAtIndLenAll(KPh-1)=NAtIndLenAll(KPh)
        enddo
        if(NMolecFrAll(KPhDel).gt.0) then
          j=NMolecFrAll(KPhDel)-1
          do i=NMolecToAll(KPhDel)+1,NMolec
            j=j+1
            iam(j)=iam(i)
            mam(j)=mam(i)
          enddo
          call AtSun(NAtMolToAll(KPhDel)+1,NAtAll,NAtMolFrAll(KPhDel))
          do KPh=KPhDel+1,NPhase
            do isw=1,NComp(KPh)
              NAtMolFr(isw,KPh-1)=NAtMolFr(isw,KPh)
              NAtMolTo(isw,KPh-1)=NAtMolTo(isw,KPh)
              NAtMolLen(isw,KPh-1)=NAtMolLen(isw,KPh)
            enddo
            NAtMolFrAll(KPh-1)=NAtMolFrAll(KPh)
            NAtMolToAll(KPh-1)=NAtMolToAll(KPh)
            NAtMolLenAll(KPh-1)=NAtMolLenAll(KPh)
          enddo
          call MolSun(NMolecToAll(KPhDel)+1,NMolec,NMolecFrAll(KPhDel))
          do KPh=KPhDel+1,NPhase
            do isw=1,NComp(KPh)
              NMolecFr(isw,KPh-1)=NMolecFr(isw,KPh)
              NMolecTo(isw,KPh-1)=NMolecTo(isw,KPh)
              NMolecLen(isw,KPh-1)=NMolecLen(isw,KPh)
            enddo
            NMolecFrAll(KPh-1)=NMolecFrAll(KPh)
            NMolecToAll(KPh-1)=NMolecToAll(KPh)
            NMolecLenAll(KPh-1)=NMolecLenAll(KPh)
          enddo
        endif
        call EM40UpdateAtomLimits
        IZdvihP=(KPhDel-1)*NParCellProfPwd
        do KPh=KPhDel,NPhase-1
          PhaseName(KPh)=PhaseName(KPh+1)
          call CopyVek(CellPar(1,1,KPh+1),CellPar(1,1,KPh),6)
          call CopyVek(CellParSU(1,1,KPh+1),CellParSU(1,1,KPh),6)
          do i=1,NLattVec(KPh+1)
            call CopyVek(vt6(1,i,1,KPh+1),vt6(1,i,1,KPh),NDim(KPh+1))
          enddo
          NLattVec(KPh)=NLattVec(KPh+1)
          do i=1,NSymm(KPh+1)
            do isw=1,NComp(KPh+1)
              do j=1,NDim(KPh+1)
                symmc(j,i,isw,KPh)=symmc(j,i,isw,KPh+1)
              enddo
              call CopyMat(rm6(1,i,isw,KPh+1),rm6(1,i,isw,KPh),
     1                     NDim(KPh+1))
              call CopyMat(rm(1,i,isw,KPh+1),rm(1,i,isw,KPh),3)
              call CopyVek(s6(1,i,isw,KPh+1),s6(1,i,isw,KPh),
     1                     NDim(KPh+1))
              ISwSymm(i,isw,KPh)=ISwSymm(i,isw,KPh+1)
            enddo
          enddo
          NSymm(KPh)=NSymm(KPh+1)
          Grupa(KPh)=Grupa(KPh+1)
          Lattice(KPh)=Lattice(KPh+1)
          ngrupa(KPh)=ngrupa(KPh+1)
          CrSystem(KPh)=CrSystem(KPh+1)
          call CopyVek(ShSg(1,KPh+1),ShSg(1,KPh),NDim(KPh+1))
          do i=1,NAtFormula(KPh+1)
            AtTypeFull(i,KPh)=AtTypeFull(i,KPh+1)
            AtType(i,KPh)=AtType(i,KPh+1)
            AtTypeMag(i,KPh)=AtTypeMag(i,KPh+1)
            AtTypeMagJ(i,KPh)=AtTypeMagJ(i,KPh+1)
            AtWeight(i,KPh)=AtWeight(i,KPh+1)
            AtRadius(i,KPh)=AtRadius(i,KPh+1)
            AtColor(i,KPh)=AtColor(i,KPh+1)
            if(Radiation(KDatBlock).eq.NeutronRadiation) then
              ffn(i,KPh)=ffn(i,KPh+1)
              ffni(i,KPh)=ffni(i,KPh+1)
            else if(Radiation(KDatBlock).eq.XRayRadiation) then
              do KDatB=1,NDatBlock
                ffra(i,KPh,KDatB)=ffra(i,KPh+1,KDatB)
                ffia(i,KPh,KDatB)=ffia(i,KPh+1,KDatB)
              enddo
              n=iabs(FFType(KPh))
              call CopyVek(FFBasic(1,i,KPh+1),FFBasic(1,i,KPh),n)
              if(ChargeDensities) then
                call CopyVekI(PopCore(1,i,KPh+1),PopCore(1,i,KPh),21)
                call CopyVekI(PopVal(1,i,KPh+1),PopVal(1,i,KPh),21)
                if(PopVal(1,i,KPh).eq.-2) then
                  HNSlater(i,KPh)=HNSlater(i,KPh+1)
                  HZSlater(i,KPh)=HZSlater(i,KPh+1)
                endif
                call CopyVekI(NSlater(1,i,KPh+1),NSlater(1,i,KPh),8)
                call CopyVek(ZSlater(1,i,KPh+1),ZSlater(1,i,KPh),8)
                call CopyVek(FFCore(1,i,KPh+1),FFCore(1,i,KPh),n)
                call CopyVek(FFVal (1,i,KPh+1),FFVal (1,i,KPh),n)
              endif
            endif
          enddo
          NAtFormula(KPh)=NAtFormula(KPh+1)
          Formula(KPh)=Formula(KPh+1)
          NUnits(KPh)=NUnits(KPh+1)
          if(NDim(KPh+1).gt.3) then
            n=3*NDimI(KPh+1)
            call CopyVek(qu(1,1,1,KPh+1),qu(1,1,1,KPh),n)
            call CopyVek(quPwd(1,1,KPh+1),quPwd(1,1,KPh),n)
            call CopyVek(quir(1,1,KPh+1),quir(1,1,KPh),n)
            call CopyVek(zv(1,1,KPh+1),zv(1,1,KPh),
     1                   NDimQ(KPh)*NComp(KPh))
            if(KCommen(KPh+1).ne.0) then
              call CopyVekI(NCommen(1,1,KPh+1),NCommen(1,1,KPh),3)
              call CopyVek (RCommen(1,1,KPh+1),RCommen(1,1,KPh),3)
              call CopyVek(trez(1,1,KPh+1),trez(1,1,KPh),NDimI(KPh))
              KCommen(KPh)=KCommen(KPh+1)
              ICommen(KPh)=ICommen(KPh+1)
            endif
            do KDatB=1,NDatBlock
              call CopyVekI(MMaxPwd(1,KPh+1,KDatB),MMaxPwd(1,KPh,KDatB),
     1                      NDimI(KPh))
            enddo
          endif
          do KDatB=1,NDatBlock
            call CopyVek(DirPref (1,KPh+1,KDatB),
     1                   DirPref (1,KPh,KDatB),3)
            call CopyVek(DirBroad(1,KPh+1,KDatB),
     1                   DirBroad(1,KPh,KDatB),3)
            SatFrMod(KPh,KDatB)=SatFrMod(KPh+1,KDatB)
            SkipFriedel(KPh,KDatB)=SkipFriedel(KPh+1,KDatB)
            KProfPwd(KPh,KDatB)=KProfPwd(KPh+1,KDatB)
            KStrain(KPh,KDatB)=KStrain(KPh+1,KDatB)
            KPref(KPh,KDatB)=KPref(KPh+1,KDatB)
            SPref(KPh,KDatB)=SPref(KPh+1,KDatB)
            PCutOff(KPh,KDatB)=PCutOff(KPh+1,KDatB)
          enddo
          call CopyVek(CellPwd(1,KPh+1),CellPwd(1,KPh),6)
          j=ICellPwd+IZdvihP
          call CopyVekI(KiPwd(j+NParCellProfPwd),KiPwd(j),6)
          IZdvih=IZdvihP+NParCellPwd
          do KDatB=1,NDatBlock
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              call CopyVek(GaussPwd(1,KPh+1,KDatB),
     1                     GaussPwd(1,KPh,KDatB),4)
              j=IGaussPwd+IZdvih
              call CopyVekI(KiPwd(j+NParProfPwd),KiPwd(j),4)
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor.and.
     1           KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
                i=5
              else
                i=4
              endif
              call CopyVek(LorentzPwd(1,KPh+1,KDatB),
     1                     LorentzPwd(1,KPh,KDatB),i)
              j=ILorentzPwd+IZdvih
              call CopyVekI(KiPwd(j+NParProfPwd),KiPwd(j),i)
              ZetaPwd(KPh,KDatB)=ZetaPwd(KPh+1,KDatB)
            endif
            if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
              if(NDimI(KPh).eq.1) then
                 n=35
              else
                 n=15+NDimI(KPh)
              endif
              call CopyVek(StPwd(1,KPh+1,KDatB),
     1                     StPwd(1,KPh+1,KDatB),n)
              j=IStPwd+IZdvih
              call CopyVekI(KiPwd(j+NParProfPwd),KiPwd(j),n)
            endif
            if(KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              call CopyVek(PrefPwd(1,KPh+1,KDatB),
     1                     PrefPwd(1,KPh,KDatB),2)
              j=IPrefPwd+IZdvih
              call CopyVekI(KiPwd(j+NParProfPwd),KiPwd(j),2)
            endif
            IZdvih=IZdvih+NParProfPwd
          enddo
          IZdvihP=IZdvihP+NParCellProfPwd
          NDim(KPh)=NDim(KPh+1)
          NDimI(KPh)=NDimI(KPh+1)
          NDimQ(KPh)=NDimQ(KPh+1)
        enddo
      endif
      if(isPowder) then
        j=1
        kip=mxscu
        do i=1,NPhase
          if(i.ne.KPhDel) then
            sctw(j,KDatBlock)=sctw(i,KDatBlock)
            if(i.ne.1.and.j.ne.1) kiTwin(j-1)=KiS(kip,KDatBlock)
            j=j+1
          endif
          kip=kip+1
        enddo
        call SetIntArrayTo(KiS(mxscu+1,KDatBlock),NPhase-1,0)
        call CopyVekI(kiTwin,KiS(mxscu+2,KDatBlock),NPhase-2)
      else
        kip=mxscu+1
        do i=2,ITwPh
          if(KPhaseTwin(i).eq.KPhDel) then
            sctw(i,KDatBlock)=0.
            KiS(kip,KDatBlock)=0
            KPhaseTwin(i)=0
          else if(KPhaseTwin(i).gt.KPhDel) then
            KPhaseTwin(i)=KPhaseTwin(i)-1
          endif
          kip=kip+1
        enddo
        if(FeYesNo(-1.,-1.,'Do you want to delete inactive twinning '//
     1             'matrices?',1)) then
          j=1
          kip=mxscu
          do i=1,NTwin
            if(KPhaseTwin(i).ne.0) then
              call CopyMat(rtw(1,i),rtw(1,j),3)
              KPhaseTwin(j)=KPhaseTwin(i)
              sctw(j,KDatBlock)=sctw(i,KDatBlock)
              if(i.ne.1) kiTwin(j-1)=KiS(kip,KDatBlock)
              j=j+1
            endif
            kip=kip+1
          enddo
          NTwinNew=j-1
          call SetIntArrayTo(KiS(mxscu+1,KDatBlock),NTwin-1,0)
          call CopyVekI(kiTwin,KiS(mxscu+1+NTwin-NTwinNew,KDatBlock),
     1                  NTwinNew-1)
          NTwin=NTwinNew
        else
          do i=1,NTwin
            if(KPhaseTwin(i).eq.0) KPhaseTwin(i)=1
          enddo
        endif
      endif
      NPhase=NPhase-1
      if(KPhaseBasic.eq.KPhDel) KPhaseBasic=1
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom40(1,0,fln(:ifln)//'.m40')
9999  return
      end
