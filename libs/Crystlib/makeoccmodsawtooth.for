      subroutine MakeOccModSawTooth(occ,x40,tzero,nt,dt,u1,u2,kf,Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension occ(*),tzero(3),nt(3),dt(3),x4i(3),x4(3),nd(3),x40(3),
     1          Gamma(9),d4(3)
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      if(kf.eq.0) then
        call SetRealArrayTo(occ,ntt,1.)
        go to 9999
      endif
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      delta=u2*.5
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        x4p=x4(1)-u1
        ix4p=x4p
        if(x4p.lt.0.) ix4p=ix4p-1
        x4p=x4p-float(ix4p)
        if(x4p.gt..5) x4p=x4p-1.
        if(x4p.ge.-delta.and.x4p.le.delta) then
          occ(i)=1.
        else
          if(kf.eq.2) then
            x4p=x4p-.5
            if(x4p.lt.-.5) x4p=x4p+1.
            if(x4p.ge.-.5+delta.and.x4p.le..5-delta) then
              occ(i)=1.
            else
              occ(i)=0.
            endif
          else
            occ(i)=0.
          endif
        endif
      enddo
9999  return
      end
