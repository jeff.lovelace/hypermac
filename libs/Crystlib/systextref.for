      logical function SystExtRef(ih,SkipCentr,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical eqiv,SkipCentr
      dimension ih(*),ihp(6),sp(6)
      SystExtRef=.false.
      if(MagneticType(KPhase).gt.0) then
        do ic=1,NLattVec(KPhase)
          a=0.
          do j=1,NDim(KPhase)
             a=a+vt6(j,ic,isw,KPhase)*float(ih(j))
          enddo
          if(abs(a-anint(a)).gt..01) go to 9000
        enddo
      else
        if(SkipCentr) then
          ip=2
        else
          ip=1
        endif
        do ic=1,NLattVec(KPhase)
          do i=ip,NSymm(KPhase)
            call indtr(ih,rm6(1,i,isw,KPhase),ihp,NDim(KPhase))
            if(eqiv(ih,ihp,NDim(KPhase))) then
              call AddVek(vt6(1,ic,isw,KPhase),s6(1,i,isw,KPhase),sp,
     1                    NDim(KPhase))
              a=0.
              do j=1,NDim(KPhase)
                a=a+sp(j)*float(ih(j))
              enddo
              if(abs(a-anint(a)).gt..01) go to 9000
            endif
          enddo
        enddo
      endif
      go to 9999
9000  SystExtRef=.true.
9999  return
      end
