      subroutine SetBasicM42(KRefB,KZone)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      if(KRefB.eq.0.and.KZone.eq.0) then
        EDCommands=' '
        EDThreads=2
        EDTiltcorr=1
        EDGeometryIEDT=0
        CalcDyn=.false.
        RescaleToFCalc=.true.
        UseWKS=.true.
        AbsFlagED=1
      endif
      if(KRefB.eq.0) then
        i1=1
        i2=NRefBlock
      else
        i1=iabs(KRefB)
        i2=iabs(KRefB)
      endif
      if(KZone.lt.0) then
        j1=1
        j2=0
      else if(KZone.eq.0) then
        j1=1
        j2=NMaxEDZone
      else
        j1=KZone
        j2=KZone
      endif
      do i=i1,i2
        if(KRefB.ge.0) then
          call UnitMat(OrMatEDZone(1,1,i),3)
          NEdZone(i)=0
          OmEDZone(i)=0.
          GMaxEDZone(i)=2.
          SGMaxMEDZone(i)=.01
          SGMaxREDZone(i)=.1
          CSGMaxREDZone(i)=.4
          EDIntSteps(i)=128
          EDThreads(i)=2
          EDTiltcorr(i)=1
          EDGeometryIEDT(i)=0
        endif
        do j=j1,j2
          call SetIntArrayTo(KiED(1,j,i),EDNPar,0)
          call SetRealArrayTo(HEDZone (1,j,i),3,1.)
          RFacEDZone(j,i)=0.
          PhiEDZone (j,i)=0.
          PhiEDZoneS(j,i)=0.
          ThetaEDZone (j,i)=0.
          ThetaEDZoneS(j,i)=0.
          ThickEDZone (j,i)=400.
          ThickEDZoneS(j,i)=0.
          XNormEDZone (j,i)=0.
          XNormEDZoneS(j,i)=0.
          YNormEDZone (j,i)=0.
          YNormEDZoneS(j,i)=0.
          AlphaEDZone(j,i)=0.
          BetaEDZone(j,i)=0.
          PrAngEDZone(j,i)=0.
          ScEDZone(j,i) =1.
          ScEDZoneS(j,i)=0.
          NThickEDZone(j,i)=0
          UseEDZone(j,i)=.true.
        enddo
      enddo
9999  return
      end
