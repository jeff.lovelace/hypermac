      subroutine Trm4050(Klic,flnp,Titulek,ich)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) flnp,Titulek
      character*256 FileName
      character*80 radka,t80
      character*15 isp(6)
      character*13 men(8)
      character*8  AtomNew(:),AtomOld(:)
      character*5  filtr(8)
      character*1  centry,syst
      dimension isfold(:),sp(6)
      integer FeMenu,CrlCentroSymm
      logical carka,FeYesNo,ExistFile,StructureExists,EqRV
      logical Nenacitat
      dimension rmp(9),ipp(1),xp(3)
      data men/'%XTAL format','C%Vis format','%CrystalMaker',
     1         '%MOLVIEW','%SHELX','%Jana','C%IF','%HRTEM'/
      data filtr/'*.xti','*.zt','*.cm','.m','*.ins',' ','*.cif',
     1           '*.cel'/
      allocatable AtomNew,AtomOld,isfold
      ich=0
      inquire(m40,opened=Nenacitat)
      Nenacitat=.not.Nenacitat
      if(Klic.eq.0) then
        nfor=FeMenu(-1.,-1.,men,1,8,1,0)
      else
        nfor=iabs(Klic)
      endif
      if(nfor.lt.1.or.nfor.gt.8) go to 9000
      if(flnp.eq.' ') then
        FileName=' '
        if(ifln.gt.0.and.nfor.ne.6) FileName=fln(:ifln)//filtr(nfor)(2:)
        if(nfor.ne.6) then
          i=0
        else
          i=1
        endif
        call FeFileManager('Select the output file name',FileName,
     1                     Filtr(nfor),i,.true.,ich)
        flnp=FileName
        do i=idel(FileName),1,-1
          if(FileName(i:i).eq.'.') then
            flnp=FileName(:i-1)
            go to 1010
          endif
        enddo
      else
        FileName=flnp
      endif
1010  if(ich.gt.0.or.FileName.eq.' ') go to 9000
      if(nfor.eq.6) then
        if(StructureExists(flnp).and..not.BatchMode) then
          t80='The strucure "'//flnp(:idel(flnp))//
     1        '" already exists, overwrite it?'
          if(.not.FeYesNo(-1.,-1.,t80,0)) go to 9000
        endif
      else
        if(ExistFile(FileName).and..not.BatchMode) then
          if(.not.FeYesNo(-1.,-1.,'The file "'//
     1       FileName(:idel(FileName))//
     2       '" already exists, overwrite it?',0)) go to 9000
        endif
        if(nfor.ne.7) then
          call OpenFile(55,FileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
        endif
      endif
      if(Nenacitat) then
        allocate(AtomOld(NAtCalc),AtomNew(NAtCalc),isfold(NAtCalc))
        call CopyVekI(isf,isfold,NAtCalc)
        do i=1,NAtCalc
          AtomOld(i)=Atom(i)
        enddo
      else
        rewind m40
        k=0
1050    read(m40,FormA) radka
        kk=0
        call StToInt(radka,kk,ipp,1,.false.,ich)
        NAtCalc=ipp(1)
        read(m40,104) sc(1,1)
        do i=1,3
          read(m40,104)
        enddo
        if(k.ne.0) go to 1200
        allocate(AtomOld(NAtCalc),AtomNew(NAtCalc),isfold(NAtCalc))
        do i=1,NAtCalc
          read(m40,103) AtomOld(i),isfold(i)
          read(m40,104)
        enddo
        rewind m40
        k=1
        go to 1050
      endif
1200  if(nfor.lt.3) then
        if(CrlCentroSymm().gt.0) then
          centry='c'
        else
          centry='n'
        endif
      endif
      if(nfor.eq.1) then
        write(55,'(''master yes'')')
        write(55,'(''startx'')')
        write(radka,102)(CellPar(i,1,KPhase),i=1,6)
        k=0
        call WriteLabeledRecord(55,'cell',Radka,k)
        write(55,'(''latice   '',a1,3x,a1)') centry,lattice(KPhase)(1:1)
        do i=1,NSymmN(KPhase)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),isp,0)
          t80=isp(1)//isp(2)//isp(3)
          carka=.true.
          do l=1,idel(t80)
            if(t80(l:l).eq.' ') then
              if(.not.carka) then
                t80(l:l)=','
                carka=.true.
              endif
            else
              carka=.false.
            endif
          enddo
          call zhusti(t80)
          t80='symtry '//t80(:idel(t80))
          write(55,FormA) t80(:idel(t80))
        enddo
        NUnits(KPhase)=8
        write(55,'(''celmol '',i2)') NUnits(KPhase)
        do i=1,NAtFormula(KPhase)
          write(55,'(''celcon '',a2,i6)') AtType(i,KPhase),
     1                   nint(AtMult(i,KPhase)*NUnits(KPhase))
        enddo
        write(55,'(''addatm'')')
        do 1600i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 1600
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          call SetRealArrayTo(xp,3,0.)
          k=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,ii).ne.0) then
              xp(1)=ax(KModA(1,ii),ii)
            else
              xp(1)=uy(1,KModA(2,ii),ii)
            endif
          endif
          call SpecPos(x(1,ii),xp,k,1,.01,n)
          pom=1./float(n)
          write(55,'(''atom '',a5,3f9.5,'' $1'',f6.3,3f9.5,'' $1'',
     1               f6.3)') atom(ii)(1:5),(x(j,ii),j=1,3),ai(ii)/pom,
     2                       (sx(j,ii),j=1,3),sai(ii)/pom
          if(itf(ii).eq.1) then
            write(55,'(''u     '',a8,2f10.6)') atom(ii)(1:5),
     1        beta(1,i)/episq,sbeta(1,i)/episq
          else
            write(55,'(''uij   '',a8,6f10.6)') atom(ii)(1:5),
     1           (beta(j,ii)/urcp(j,1,KPhase),j=1,6)
            write(55,'(''suij  '',a8,6f10.6)') atom(ii)(1:5),
     1           (sbeta(j,ii)/urcp(j,1,KPhase),j=1,6)
          endif
1600    continue
        write(55,'(''atable''/''finish'')')
      else if(nfor.eq.2) then
        if(ngrupa(KPhase).eq.0) then
          grupa(KPhase)(1:11)='NonStandard'
          syst(1:1)='?'
        endif
        if(ngrupa(KPhase).ge.  1.and.ngrupa(KPhase).le.  2) syst='1'
        if(ngrupa(KPhase).ge.  3.and.ngrupa(KPhase).le. 15) syst='2'
        if(ngrupa(KPhase).ge. 16.and.ngrupa(KPhase).le. 74) syst='3'
        if(ngrupa(KPhase).ge. 75.and.ngrupa(KPhase).le.142) syst='4'
        if(ngrupa(KPhase).ge.143.and.ngrupa(KPhase).le.167) syst='5'
        if(ngrupa(KPhase).ge.168.and.ngrupa(KPhase).le.194) syst='6'
        if(ngrupa(KPhase).ge.195.and.ngrupa(KPhase).le.230) syst='7'
        write(55,'(''BEGIN'')')
        write(55,'(''TITLE '',80a1)')(fln(i:i),i=1,ifln)
        write(radka,102)(CellPar(i,1,KPhase),i=1,6)
        k=0
        call WriteLabeledRecord(55,'cell',Radka,k)
        write(55,'(''SGROUP '',a11,i5,'' ? '',a1,1x,a1,1x,a1)')
     1        grupa(KPhase),ngrupa(KPhase),centry,syst,
     2        lattice(KPhase)(1:1)
        write(55,'(''SMAT  {'')')
        do i=1,NSymmN(KPhase)
          write(55,'(3(3i3,f10.6))')
     1      ((nint(rm6(k+(j-1)*3,i,1,KPhase)),j=1,3),
     1       s6(k,i,1,KPhase),k=1,3)
        enddo
        write(55,'(''}'')')
        write(55,'(''ATOMS {'')')
        do 2100i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 2100
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          write(55,'(a2,'' '''''',a8,'''''''',3f10.6,'' 1.000 2.000'')')
     1          AtType(isf(ii),KPhase),atom(ii),(x(j,ii),j=1,3)
2100    continue
        write(55,'(''}'')')
        write(55,'(''END'')')
      else if(nfor.eq.3) then
        write(55,'(''TITLE '',20a1)')(fln(i:i),i=1,ifln)
        write(radka,102)(CellPar(i,1,KPhase),i=1,6)
        k=0
        call WriteLabeledRecord(55,'cell',Radka,k)
        do i=1,NSymmN(KPhase)
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),isp,0)
          t80='SYMM '//isp(1)(:idel(isp(1)))//' '//
     1                 isp(2)(:idel(isp(2)))//' '//
     2                 isp(3)(:idel(isp(3)))
          write(55,FormA) t80(:idel(t80))
        enddo
        write(55,'(''LATC '',a1)') lattice(KPhase)(1:1)
        write(55,'(''VIEW 0 0 1'')')
        write(55,'(''XYZR 0.0 1.0 0.0 1.0 0.0 1.0'')')
        write(55,'(''END'')')
        do 3500i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 3500
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          write(55,'(a2,3x,a8,3x,3f10.5)')
     1      AtType(isf(ii),KPhase),atom(ii),(x(j,ii),j=1,3)
3500    continue
        close(55)
      else if(nfor.eq.4) then
        write(radka,102)(CellPar(i,1,KPhase),i=1,6)
        k=0
        call WriteLabeledRecord(55,'Cell',Radka,k)
        write(55,'(''Localize''/''Showcell''/''Nobond'')')
        do i=1,NAtFormula(KPhase)
          write(55,'(''Define  '',A2,''  1.5  0.0  Red'')')
     1      AtType(i,KPhase)
        enddo
        do 4100i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 4100
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          write(55,'(''Atom  '',i4,3x,a8,3f10.5)') i,atom(ii),
     1              (x(j,ii),j=1,3)
4100    continue
        write(55,'(''Spacegroup '',i3,''   = '',a11)') ngrupa(KPhase),
     1                                                 grupa(KPhase)
        write(55,'(''=Duplicate 2 2 2''/''=Translate -1 -1 -1'')')
      else if(nfor.eq.5) then
        write(55,'(''TITL '',20a1)')(fln(i:i),i=1,ifln)
        write(radka,'(f9.6,3f10.4,3f10.3)') LamAve(1),
     1                                      (CellPar(i,1,KPhase),i=1,6)
        k=0
        call WriteLabeledRecord(55,'CELL',Radka,k)
        latt=index(smbc,lattice(KPhase)(1:1))
        if(latt.eq.1) then
          kk=1
        else if(latt.ge.2.and.latt.le.4) then
          kk=latt+3
        else
          kk=latt-3
        endif
        if(CrlCentroSymm().le.0) kk=-kk
        write(radka,'(i2)') kk
        k=0
        call WriteLabeledRecord(55,'LATT',Radka,k)
        do 5100i=2,NSymmN(KPhase)
          if(kk.gt.0) then
            call RealMatrixToOpposite(rm6(1,i,1,KPhase),rmp,
     1                                NDim(KPhase))
            do j=1,i-1
              if(EqRV(rmp,rm6(1,j,1,KPhase),NDimQ(KPhase),.001)) then
                do 5050k=1,NLattVec(KPhase)
                  call AddVek(s6(1,i,1,KPhase),vt6(1,k,1,KPhase),sp,
     1                        NDim(KPhase))
                  do l=1,NDim(KPhase)
                    pom=sp(l)-s6(l,j,1,KPhase)
                    if(abs(anint(pom)-pom).gt..001) go to 5050
                  enddo
                  go to 5100
5050            continue
              endif
            enddo
          endif
          call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),isp,0)
          t80=isp(1)//isp(2)//isp(3)
          carka=.true.
          do l=1,idel(t80)
            if(t80(l:l).eq.' ') then
              if(.not.carka) then
                t80(l:l)=','
                carka=.true.
              endif
            else
              carka=.false.
            endif
          enddo
          call zhusti(t80)
          t80='SYMM '//t80
          write(55,FormA) t80(:idel(t80))
5100    continue
        if(NAtFormula(KPhase).gt.0) then
          radka='SFAC'
          k=6
          i=0
5150      i=i+1
          radka=radka(:k)//AtType(i,KPhase)
          if(i.lt.NAtFormula(KPhase)) then
            k=idel(radka)+1
            go to 5150
          endif
          write(55,FormA) radka(:idel(radka))
          write(radka,'(15i5)')
     1      (nint(AtMult(i,KPhase)*float(NUnits(KPhase))),
     2                                i=1,NAtFormula(KPhase))
        endif
        k=0
        call WriteLabeledRecord(55,'UNIT',Radka,k)
        write(radka,'(f9.6)') sc(1,1)
        k=0
        call WriteLabeledRecord(55,'FVAR',Radka,k)
        call TestAtomNames(AtomOld,AtomNew,isfold,NAtCalc,iflag)
        if(iflag.gt.0) then
          write(55,'(''REM ****************************************'')')
          write(55,'(''REM **   JANA2006 vers. SHELX atom names  **'')')
          write(55,'(''REM ****************************************'')')
          ik=0
          do j=1,(NAtCalc-1)/4+1
            ip=ik+1
            ik=min(ik+4,NAtCalc)
            t80='REM '
            k=5
            do i=ip,ik
              t80(k:)=atomold(i)(:idel(atomold(i)))//'='//
     1                atomnew(i)(:min(idel(atomnew(i)),4))
              k=k+16
            enddo
            write(55,FormA) t80(:idel(t80))
          enddo
        else if(iflag.lt.0) then
          close(55,status='delete')
          go to 9999
        endif
        do 5200i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 5200
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          if(itf(ii).eq.1) then
            write(55,100) AtomNew(i),isfold(i),(x(j,ii),j=1,3),
     1                    ai(ii)+10.,beta(1,ii)/episq
          else
            write(55,100) AtomNew(i),isfold(i),(x(j,ii),j=1,3),
     1                    ai(ii)+10.,(beta(j,ii)/urcp(j,1,KPhase),j=1,2)
            write(55,101) beta(3,ii)/urcp(3,1,KPhase),
     1                   (beta(j,ii)/urcp(j,1,KPhase),j=6,4,-1)
          endif
5200    continue
        write(55,'(''HKLF 4'')')
        write(55,'(''END'')')
      else if(nfor.eq.6) then
        i=idel(FileName)
        call MoveFile('#tmp#.m40',FileName(:i)//'.m40')
        call MoveFile('#tmp#.m50',FileName(:i)//'.m50')
        go to 9999
      else if(nfor.eq.7) then
        if(Nenacitat) then
          i=0
        else
          i=1
        endif
        call CIFMakeBasicTemplate(FileName,i)
        call CIFUpdate(FileName,.true.,i)
      else
        write(55,FormA) Titulek(:idel(Titulek))
        if(CellPar(1,1,KPhase).gt.CellPar(2,1,KPhase)) then
          i1=1
          i2=2
        else
          i1=2
          i2=1
        endif
        write(55,'(1x,i2,6f8.4)') 10,
     1    CellPar(i1,1,KPhase)*.1,CellPar(i2,1,KPhase)*.1,
     2    CellPar(3,1,KPhase)*.1,CellPar(3+i1,1,KPhase),
     3    CellPar(3+i2,1,KPhase),CellPar(6,1,KPhase)
        do 6000i=1,NAtCalc
          if(Nenacitat) then
            if(kswa(i).ne.KPhase) go to 6000
            ii=i
          else
            ii=1
            read(m40,103) Atom(1),isf(1),itf(1),ai(1),(x(j,1),j=1,3)
            read(m40,104) (beta(j,1),j=1,6)
          endif
          if(itf(ii).gt.1) then
            call boueq(beta(1,ii),sbeta(1,ii),1,pom,spom,iswa(ii))
          else
            pom=beta(1,ii)
          endif
          j=isf(ii)
          write(55,'(1x,a2,8f8.4)') AtType(j,KPhase),x(i1,ii),x(i2,ii),
     1                              x(3,ii),ai(ii),pom/episq
6000    continue
      endif
      call CloseIfOpened(m40)
      call CloseIfOpened(55)
      go to 9999
9000  ich=1
9999  if(allocated(AtomOld)) deallocate(AtomOld,AtomNew,isfold)
      return
100   format(a4,i4,f10.5,f10.5,f10.5,f10.5,f10.5,f10.5,' =')
101   format(f14.5,f10.5,f10.5,f10.5)
102   format(3f10.4,3f10.3)
103   format(a8,2i3,4x,4f9.6)
104   format(6f9.6)
      end
