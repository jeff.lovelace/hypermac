      subroutine CrlDefineNewFLN(Konec)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest
      character*80  t80
      logical CrwLogicQuest,StructureExists,FeYesNo
      ich=0
      id=NextQuestId()
      xqd=400.
      il=4
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
      xpom=5.
      tpom=xpom+CrwXd+10.
      Veta='%discard changes'
      il=0
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          Veta='%rewrite the old structure'
          nCrwDiscard=CrwLastMade
        else if(i.eq.2) then
          Veta='%create a new one'
        else
          nCrwNew=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
      enddo
      il=il+1
      tpom=5.
      Veta='%Structure name'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      Cislo='%Browse'
      dpom=xqd-xpom-FeTxLengthUnder(Cislo)-25.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      nEdwName=EdwLastMade
      xpom=xpom+dpom+10.
      dpom=FeTxLengthUnder(Cislo)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Cislo)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtBrowse=ButtonLastMade
1200  if(CrwLogicQuest(nCrwNew)) then
        call FeQuestStringEdwOpen(nEdwName,' ')
        call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      else
        call FeQuestEdwDisable(nEdwName)
        call FeQuestButtonDisable(nButtBrowse)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        Veta=EdwStringQuest(nEdwName)
        call FeFileManager('Define name of the new structure',Veta,' ',
     1                     1,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,Veta)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      Konec=0
      if(CrwLogicQuest(nCrwDiscard)) then
        Konec=1
      else if(CrwLogicQuest(nCrwNew)) then
        Veta=EdwStringQuest(nEdwName)
        if(Veta.eq.' ') then
          call FeChybne(-1.,-1.,'the name of structure cannot be an '//
     1                  'empty string, try again.',' ',SeriousError)
          go to 1500
        else if(StructureExists(Veta)) then
          call ExtractFileName(Veta,t80)
          if(.not.FeYesNo(-1.,-1.,'The structure "'//t80(:idel(t80))//
     1                    '" already exists, rewrite it?',0)) then
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
        endif
        fln=Veta
        ifln=idel(fln)
      endif
      call FeQuestRemove(id)
      return
      end
