      subroutine RunGeminography
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      character*8 PointGroup
      character*1 :: uvw(3)=(/'u','v','w'/)
      integer :: CodePg,CodeCentr,TwType=1,nEdwHKL(3,2),NMax=20,
     1           HLim(3,2)=reshape((/0,0,1,0,0,1/),shape(HLim))
      logical EqIgCase,CrwLogicQuest
      real :: omLL=0.,omUL=6.,Tolerance=.5
      id=NextQuestId()
      il=7
      xqd=400.
      Veta='Geminography parameters:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      tpom=5.
      dpom=50.
      do j=1,2
        if(j.eq.1) then
          Veta='h(min)/u(min):'
          xpom=tpom+FeTxLength(Veta)+15.
        else
          Veta='h(max)/u(max):'
          xpom=xpom+180.
          tpom=tpom+180.
        endif
        il=1
        do i=1,3
          write(Veta(1:1),FormA1) Indices(i)
          write(Veta(8:8),FormA1) uvw(i)
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          call FeQuestIntEdwOpen(EdwLastMade,HLim(i,j),.false.)
          if(j.eq.1) then
            ii=-5
            jj=HLim(i,j+1)
          else
            ii=HLim(i,j-1)
            jj=5
          endif
          call FeQuestEudOpen(EdwLastMade,ii,jj,1,0.,0.,0.)
          nEdwHKL(i,j)=EdwLastMade
          il=il+1
        enddo
      enddo
      Veta='n(max)'
      xpom=xpom-180.
      tpom=tpom-180.
      ilp=il
      do i=1,4
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwNMax=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,NMax,.false.)
          Veta='om(LL) [deg]'
          il=il+1
          cycle
        else if(i.eq.2) then
          nEdwOmLL=EdwLastMade
          pom=OmLL
          Veta='om(UL) [deg]'
        else if(i.eq.3) then
          nEdwOmUL=EdwLastMade
          pom=OmUL
          Veta='Tolerance on pseudo-symmetry [Ang]'
          xpom=tpom+FeTxLength(Veta)+15.
        else if(i.eq.4) then
          nEdwTolerance=EdwLastMade
          pom=Tolerance
        endif
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        il=il+1
      enddo
      il=-10*ilp-7
      Veta='Reflection twin'
      xpom=200.
      tpom=xpom+CrwgXd+10.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwReflection=CrwLastMade
          Veta='Rotation twin'
        else
          nCrwRotation=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.TwType)
        il=il-7
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwHKL(1,1).and.
     1   CheckNumber.le.nEdwHKL(3,2)) then
        call FeQuestIntFromEdw(CheckNumber,HLim(i,j))
        n=CheckNumber-nEdwHKL(1,1)
        i=mod(n,3)+1
        j=n/3+1
        nEdw1=CheckNumber+EdwFr-1
        if(j.eq.1) then
          nEdw2=nEdw1+3
          ii=HLim(i,j)
          jj=EdwInt(3,nEdw2)
        else
          nEdw2=nEdw1-3
          ii=EdwInt(2,nEdw2)
          jj=HLim(i,j)
        endif
        call FeQuestEudOpen(nEdwHKL(i,3-j),ii,jj,1,0.,0.,0.)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNMax,NMax)
        call FeQuestRealFromEdw(nEdwOmLL,OmLL)
        call FeQuestRealFromEdw(nEdwOmUL,OmUL)
        call FeQuestRealFromEdw(nEdwTolerance,Tolerance)
        if(CrwLogicQuest(nCrwReflection)) then
          TwType=0
        else
          TwType=1
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,'input.in','formatted','unknown')
      write(ln,FormA) 'Title'
      call GetPointGroupFromSpaceGroup(PointGroup,NPg,NPgPor)
      CodePG=1
      if(NPgPor.le.2) then
        CodePG=NPgPor
      else if(NPgPor.eq.3) then
        if(Monoclinic(KPhase).eq.1) then
          CodePG=5
        else if(Monoclinic(KPhase).eq.3) then
          CodePG=4
        else
          CodePG=3
        endif
      else if(NPgPor.eq.4) then
        if(Monoclinic(KPhase).eq.1) then
          CodePG=8
        else if(Monoclinic(KPhase).eq.3) then
          CodePG=7
        else
          CodePG=6
        endif
      else if(NPgPor.eq.5) then
        if(Monoclinic(KPhase).eq.1) then
          CodePG=11
        else if(Monoclinic(KPhase).eq.3) then
          CodePG=10
        else
          CodePG=9
        endif
      else if(NPgPor.eq.6) then
        CodePG=12
      else if(NPgPor.eq.7) then
        if(EqIgCase(PointGroup,'mm2')) then
          CodePG=12
        else if(EqIgCase(PointGroup,'m2m')) then
          CodePG=13
        else if(EqIgCase(PointGroup,'2mm')) then
          CodePG=14
        endif
      else if(NPgPor.le.17) then
        CodePG=NPgPor+8
      else if(NPgPor.le.18) then
        CodePG=27
      else if(NPgPor.le.19) then
        CodePG=32
      else if(NPgPor.le.20) then
        CodePG=36
      else if(NPgPor.le.21) then
        CodePG=40
      else if(NPgPor.le.22) then
        CodePG=31
      else if(NPgPor.le.23) then
        CodePG=35
      else if(NPgPor.le.24) then
        CodePG=39
      else if(NPgPor.le.25) then
        CodePG=29
      else if(NPgPor.le.26) then
        CodePG=33
      else if(NPgPor.le.27) then
        CodePG=37
      else if(NPgPor.le.40) then
        CodePG=NPgPor+13
      endif
      CodeCentr=1
      if(EqIgCase(Lattice(KPhase),'I')) then
        CodeCentr=2
      else if(EqIgCase(Lattice(KPhase),'R')) then
        CodeCentr=3
      else if(EqIgCase(Lattice(KPhase),'F')) then
        CodeCentr=4
      else if(EqIgCase(Lattice(KPhase),'A')) then
        CodeCentr=5
      else if(EqIgCase(Lattice(KPhase),'B')) then
        CodeCentr=6
      else if(EqIgCase(Lattice(KPhase),'C')) then
        CodeCentr=7
      endif
      write(Veta,'(3i5,3f10.4,3f10.3,7i5,3f10.4)')
     1 CodePG,CodeCentr,TwType,CellPar(1:6,1,KPhase),
     2 ((HLim(i,j),j=1,2),i=1,3),NMax,OmLL,OmUL,Tolerance
      call ZdrcniCisla(Veta,19)
      write(ln,FormA) Veta(:idel(Veta))
      call CloseIfOpened(ln)
      Veta=CallGeminography(:idel(CallGeminography))//
     1     ' input.in'
      call FeSystemCommand(Veta,0)
      call FeWaitInfo('end of the graphic visualization.')
      call FeEdit('OUTPUT.OUT')
9999  return
      end
