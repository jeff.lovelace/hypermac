      subroutine CompleteSymm(Klic,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      real rmp(36),sp(6),sp1(6),sp2(6)
      integer, allocatable :: is(:),iso(:)
      logical Eqrv,Pridal
      nalloc=1000
      allocate(is(nalloc))
      ich=0
      nss=NSymm(KPhase)
      do i=1,NSymm(KPhase)
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      call SetIntArrayTo(is,nalloc,0)
1500  i=0
2000  if(i.ge.NSymm(KPhase)) then
        call CrlOrderMagSymmetry
        go to 9000
      endif
      i=i+1
      j=is(i)
2100  if(j.ge.NSymm(KPhase)) go to 2000
      j=j+1
      is(i)=j
      call multm(rm6(1,i,1,KPhase),rm6(1,j,1,KPhase),rmp,NDim(KPhase),
     1                                      NDim(KPhase),NDim(KPhase))
      ZMagP=ZMag(i,1,KPhase)*ZMag(j,1,KPhase)
      call multm(rm6(1,i,1,KPhase),s6(1,j,1,KPhase),sp,NDim(KPhase),
     1                                               NDim(KPhase),1)
      call AddVek(sp,s6(1,i,1,KPhase),sp,NDim(KPhase))
      call od0do1(sp,sp,NDim(KPhase))
      Pridal=.false.
      do k=1,NSymm(KPhase)
        if(EqRV(rmp,rm6(1,k,1,KPhase),NDimQ(KPhase),.001)) then
          call CopyVek(sp,sp1,NDim(KPhase))
          call CopyVek(s6(1,k,1,KPhase),sp2,NDim(KPhase))
          call NormCentr(sp1)
          call NormCentr(sp2)
          if(Eqrv(sp1,sp2,NDim(KPhase),.0001)) then
            if(MagneticType(KPhase).ne.0) then
              if(abs(ZMag(k,1,KPhase)-ZMagP).lt..001) then
                go to 6000
              else
                go to 7000
              endif
            else
              go to 6000
            endif
          else
            if(k.eq.1.and.Klic.eq.0) then
              if(MagneticType(KPhase).eq.0) then
                go to 7000
              else
                if(abs(ZMag(k,1,KPhase)-ZMagP).lt..001) go to 7000
              endif
            endif
          endif
        endif
      enddo
      Pridal=.true.
      if(NSymm(KPhase).ge.3333) go to 8000
      NSymm(KPhase)=NSymm(KPhase)+1
      NSymmN(KPhase)=NSymmN(KPhase)+1
      if(NSymm(KPhase).gt.nalloc) then
        if(NSymm(KPhase).lt.999) then
          allocate(iso(nalloc))
          call CopyVekI(is,iso,nalloc)
          nalloco=nalloc
          nalloc=nalloc+100
          deallocate(is)
          allocate(is(nalloc))
          call CopyVekI(iso,is,nalloco)
          deallocate(iso)
        else
          call FeChybne(-1.,-1.,'The number of generated symmetry '//
     1                  'operators exceeds any reasonable limit.',
     2                  'Please check them for consistency.',
     3                  SeriousError)
          go to 8000
        endif
      endif
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,1)
      call SetRealArrayTo(rm6(1,NSymm(KPhase),1,KPhase),maxNDim**2,0.)
      call SetRealArrayTo(s6 (1,NSymm(KPhase),1,KPhase),maxNDim,0.)
      call SetStringArrayTo(symmc(1,NSymm(KPhase),1,KPhase),maxNDim,' ')
      call CopyMat(rmp,rm6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
      call CopyVek(sp,s6(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
      call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1               rm(1,NSymm(KPhase),1,KPhase),
     2               NDim(KPhase))
      call MatInv(rm(1,NSymm(KPhase),1,KPhase),rmp,det,3)
      ZMag(NSymm(KPhase),1,KPhase)=ZMagP
      call CrlMakeRMag(NSymm(KPhase),1)
      call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1              s6(1,NSymm(KPhase),1,KPhase),
     2              symmc(1,NSymm(KPhase),1,KPhase),0)
      ISwSymm(NSymm(KPhase),1,KPhase)=1
      BratSymm(NSymm(KPhase),KPhase)=.true.
6000  if(Pridal) then
        go to 1500
      else
        go to 2100
      endif
7000  write(Veta,'(6f8.3)')(sp(k),k=1,NDim(KPhase))
      call FeChybne(-1.,-1.,'inconsistent non-primitive translation '//
     1              'found:',Veta,SeriousError)
      go to 8000
7100  call FeChybne(-1.,-1.,'inconsistent time inversion flags '//
     1              'generated.',' ',SeriousError)
8000  NSymm(KPhase)=nss
      ich=1
9000  deallocate(is)
      return
      end
