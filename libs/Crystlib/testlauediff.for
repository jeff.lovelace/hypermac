      subroutine TestLaueDiff
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension X(5000),Y(5000,5),O(5000,5)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      xomn= 9999.
      xomx=-9999.
      yomn= 9999.
      yomx=-9999.
      dx=pi/100000.
      RN=100.
      ymez=10.
      do i=1,5000
        X(i)=dx*float(i-1)
        Y(i,1)=RN*tan(X(i))
        Y(i,2)=tan(RN*X(i))
        if(X(i).eq.0.) then
          Y(i,3)=ymez
        else
          Y(i,3)=ymez*(sin(RN*X(i))/(RN*sin(X(i))))**2
        endif
        xomn=min(xomn,X(i))
        xomx=max(xomx,X(i))
        if(Y(i,1).lt.0..or.Y(i,1).gt.ymez) then
          O(i,1)=0.
        else
          O(i,1)=1.
        endif
        if(Y(i,2).lt.0..or.Y(i,2).gt.ymez) then
          O(i,2)=0.
        else
          O(i,2)=1.
        endif
        O(i,3)=1.
      enddo
      yomx= ymez
      yomn= 0.
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'X')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Y')
      do i=1,3
        if(i.eq.1) then
          j=Red
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Yellow
        else if(i.eq.4) then
          j=Khaki
        else if(i.eq.5) then
          j=Blue
        endif
        call GrtDrawPerPartes(X,Y(1,i),O(1,i),
     1                        5000,NormalLine,NormalPlotMode,j)
      enddo
      call FeReleaseOutput
      call FeDeferOutput
6000  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 6000
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
