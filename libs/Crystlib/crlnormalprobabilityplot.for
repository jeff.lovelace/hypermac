      subroutine CrlNormalProbabilityPlot
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(3),xnorm(:),FobsA(:),FcalcA(:),
     1          sigFA(:),wdyA(:),am(3),ps(2)
      integer ipor(:),iwdy(:)
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(6) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Change %b        ',
     5                'Change %c        ',
     6                '%Optimal         '/)
      allocatable ipor,iwdy,xnorm,FobsA,FcalcA,sigFA,wdyA
      Tiskne=.false.
      ln=0
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA) Veta
      i=LocateSubstring(Veta,'e',.false.,.true.)
      if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
        format83a=format83e
      else
        format83a=format83
      endif
      rewind ln
      n=0
1100  read(ln,format83a,end=1200)
      n=n+1
      go to 1100
1200  allocate(ipor(n),iwdy(n),xnorm(n),FobsA(n),FcalcA(n),sigFA(n),
     1         wdyA(n))
      rewind ln
      n=0
1300  read(ln,format83a,end=1400)(j,i=1,maxNDim),(pom,i=1,3),
     1  Cislo,itw,wdy,Fobs,Fcalc,sigF
      n=n+1
      wdyA(n)=wdy
      FobsA(n)=Fobs
      FcalcA(n)=Fcalc
      sigFA(n)=sigF
      go to 1300
1400  call CloseIfOpened(ln)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
c        else if(i.eq.4) then
c          nButtDPlot=ButtonLastMade
        else if(i.eq.4) then
          nButtChangeB=ButtonLastMade
        else if(i.eq.5) then
          nButtChangeC=ButtonLastMade
        else if(i.eq.6) then
          nButtOptimal=ButtonLastMade
        endif
        j=ButtonOff
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.6) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      KPlot=0
      wta=0
      wtb=1.
      wtc=0.
2000  FMax=0.
      do i=1,n
        if(KPlot.eq.0) then
          pom=wdyA(i)
        else
          wt=sqrt(wta+wtb*sigFA(i)**2+wtc*FobsA(i)**2)
          pom=(FobsA(i)-FcalcA(i))/wt
        endif
        iwdy(i)=nint(pom*1000.)
        FMax=max(abs(pom),FMax)
      enddo
      call indexx(n,iwdy,ipor)
      XMax=0.
      do i=1,n
        j=ipor(i)
        xnorm(j)=erfi(float(-n+2*i-1)/float(n))
        XMax=max(XMax,xnorm(j))
      enddo
      HardCopy=0
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        rewind ln
        yomn=-FMax*1.1
        yomx= FMax*1.1
        xomn=-XMax*1.1
        xomx= XMax*1.1
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Normal')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'wdy')
        xpp(3)=0.
        do i=1,n
          j=ipor(i)
          xpp(1)=xnorm(j)
          xpp(2)=float(iwdy(j))*.001
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
        enddo
        if(FMax.gt.XMax) then
          xpp(1)=xomx
          xpp(2)=xomx
        else
          xpp(1)=yomx
          xpp(2)=yomx
        endif
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=-xpp(1)
        xpp(2)=-xpp(2)
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckNumber.eq.nButtQuit) then
        go to 8000
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtChangeB)
     1  then
        wtb=wtb*1.1
        KPlot=1
        go to 2000
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtChangeC)
     1  then
        wtc=wtc+.0001
        KPlot=1
        go to 2000
      else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtOptimal)
     1  then
        wtb=sqrt(FMax/3.)
        do iii=1,50
3000    call SetRealArrayTo(am,3,0.)
        call SetRealArrayTo(ps,2,0.)
        Suma=0.
c        if(wtc.le.0.) wtc=.01
        do i=1,n
          wt=sqrt(wta+wtb*sigFA(i)**2+wtc*FobsA(i)**2)
          delta=(FobsA(i)-FcalcA(i))/wt
          pom=-delta/wt**3
          xo(1)=.5*pom*sigFA(i)**2
          xo(2)=.5*pom*FobsA(i)**2
          m=0
          do j=1,2
            do k=1,j
              m=m+1
              am(m)=am(m)+xo(j)*xo(k)
            enddo
            ps(j)=ps(j)-xo(j)*(delta-xnorm(i))
          enddo
          Suma=Suma+(delta-xnorm(i))**2
        enddo
        ps(1)=0.
c        write(Veta,'(e15.5)') Suma
c        call FeWinMessage(Veta,' ')
        call smi(am,2,ising)
        if(ising.le.0) then
          call nasob(am,ps,xo,2)
c          wtb=sqrt(abs(wtb**2+xo(1)))
c          wtc=sqrt(abs(wtc**2+xo(2)))
          wtb=wtb+xo(1)
          wtc=wtc+xo(2)
c          pause
c          write(Veta,'(''Oprava:'',4f10.6)') xo(1),xo(2),wtb,wtc
c          call FeWinMessage(Veta,' ')
c          go to 3000
        else
          call FeWinMessage('Singular',' ')
        endif
        enddo
        KPlot=1
        go to 2000
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  if(allocated(ipor)) deallocate(ipor,iwdy,xnorm,FobsA,FcalcA,sigFA,
     1                               wdyA)
      return
      end
