      subroutine CrlRandomModulations
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,t256
      integer :: NCyklMax=50,EdwStateQuest,KModMx(3)
      real :: RFac(5),RFacOpt(5),AmpMax(3)=(/.1,.01,.001/)
      logical :: ExistFile,RefineEnd,CrwLogicQuest,UseKMod(3),
     1           UseStartValues=.false.
      ShowInfoOnScreen=.false.
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      call Random_Seed()
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.b40')
      KModMx=0
      do ia=1,NAtAll
        do i=1,3
          KModMx(i)=max(KModMx(i),KModA(i,ia))
        enddo
      enddo
      id=NextQuestId()
      xqd=550.
      il=10
      Veta='Random search for modulation amplitudes'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='o%ccupational modulation'
      t256='=> maximal amplitude'
      tpome=tpom+FeTxLengthUnder(Veta)+5.
      xpome=tpome+FeTxLengthUnder(t256)+5.
      dpom=70.
      tpoml=xpome+dpom+5.
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(CrwLastMade,.true.)
        call FeQuestEdwMake(id,tpome,il,xpome,il,t256,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,AmpMax(i),.false.,.false.)
        if(i.eq.1) then
          nCrwOcc=CrwLastMade
          nEdwOcc=EdwLastMade
          Veta='%positional parameters'
        else if(i.eq.2) then
          nCrwX=CrwLastMade
          nEdwX=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems','L','N')
          nLblX=LblLastMade
          Veta='A%DP parameters'
        else if(i.eq.3) then
          nCrwADP=CrwLastMade
          nEdwADP=EdwLastMade
          nLblADP=LblLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems^2','L','N')
        endif
        if(KModMx(i).le.0) then
          UseKMod(i)=.false.
          call FeQuestCrwDisable(CrwLastMade)
          call FeQuestEdwDisable(EdwLastMade)
          call FeQuestLblDisable(LblLastMade)
        else
          UseKMod(i)=.true.
        endif
      enddo
      il=il+1
      Veta='%Start with original parameters'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseStartValues=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseStartValues)
      il=il+1
      tpom=5.
      Veta='Maximal number of hits:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxHits=EdwLastMade
      call FeQuestIntEdwOpen(nEdwMaxHits,NCyklMax,.false.)
      tpome=xpom+dpom+20.
      Veta='%Edit refinement commands'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpome,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefineCommands=ButtonLastMade
      tpome=tpome+dpom+20.
      Veta='%Run optimatization'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpome,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRun=ButtonLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Trial#0'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblLast=LblLastMade
      tpomp=100.
      call FeQuestLblMake(id,tpomp,il,' ','L','N')
      nLblRLast=LblLastMade
      Veta='Best fit hit#0'
      il=il+1
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblBest=LblLastMade
      call FeQuestLblMake(id,tpomp,il,' ','L','N')
      nLblRBest=LblLastMade
      Veta='Original'
      il=il+1
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblOriginal=LblLastMade
      call FeQuestLblOff(LblLastMade)
      call FeQuestLblMake(id,tpomp,il,' ','L','N')
      nLblROriginal=LblLastMade
      call FeQuestLblOff(LblLastMade)
      il=il+1
      Veta='%Omit solution'
      dpom=FeTxLengthUnder(Veta)+30.
      xpom=xqd*.5-dpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtOmit=ButtonLastMade
      Veta='%Accept solution'
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAccept=ButtonLastMade
1400  if(ExistFile(fln(:ifln)//'.b40')) then
        call FeQuestButtonOff(nButtAccept)
      else
        call FeQuestButtonDisable(nButtAccept)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAccept) then
        call CopyFile(fln(:ifln)//'.b40',fln(:ifln)//'.m40')
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOmit)
     1  then
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtRefineCommands) then
        k=2
        NeverStartProgram=.true.
        call SetCommands(k)
        NeverStartProgram=.false.
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwOcc) then
        UseKMod(1)=CrwLogicQuest(nCrwOcc)
        if(UseKMod(1)) then
          if(EdwStateQuest(nEdwOcc).ne.EdwOpened)
     1      call FeQuestRealEdwOpen(nEdwOcc,AmpMax(1),.false.,.false.)
        else
          if(EdwStateQuest(nEdwOcc).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwOcc,AmpMax(1))
            call FeQuestEdwDisable(nEdwOcc)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwX) then
        UseKMod(2)=CrwLogicQuest(nCrwX)
        if(UseKMod(2)) then
          if(EdwStateQuest(nEdwX).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwX,AmpMax(2),.false.,.false.)
            call FeQuestLblOn(nLblX)
          endif
        else
          if(EdwStateQuest(nEdwX).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwX,AmpMax(2))
            call FeQuestEdwDisable(nEdwX)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwADP) then
        UseKMod(3)=CrwLogicQuest(nCrwADP)
        if(UseKMod(3)) then
          if(EdwStateQuest(nEdwADP).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwADP,AmpMax(3),.false.,.false.)
            call FeQuestLblOn(nLblADP)
          endif
        else
          if(EdwStateQuest(nEdwADP).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwADP,AmpMax(3))
            call FeQuestEdwDisable(nEdwADP)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwUseStartValues) then
        UseStartValues=CrwLogicQuest(nCrwUseStartValues)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRun) then
        if(UseKMod(1)) call FeQuestRealFromEdw(nEdwOcc,AmpMax(1))
        if(UseKMod(2)) call FeQuestRealFromEdw(nEdwX,AmpMax(2))
        if(UseKMod(3)) call FeQuestRealFromEdw(nEdwADP,AmpMax(3))
        call FeQuestIntFromEdw(nEdwMaxHits,NCyklMax)
        if(UseStartValues) then
          NCykl=-1
          call FeQuestLblOn(nLblOriginal)
          call FeQuestLblOn(nLblROriginal)
        else
          NCykl=0
        endif
        RFacOpt=9999.
2100    NCykl=NCykl+1
        if(NCykl.gt.NCyklMax) go to 1400
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
        if(NCykl.eq.0) go to 2200
        poms=0.
        call EM40SwitchBetaU(0,0)
        do ia=1,NAtAll
          isw=iswa(ia)
          ksw=kswa(ia)
          if(UseKMod(1)) then
            do i=1,KModA(1,ia)
              call Random_Number(pom)
              pom=2.*pom-1.
              if(UseStartValues) poms=ax(i,ia)
              ax(i,ia)=poms+AmpMax(1)*pom
              call Random_Number(pom)
              pom=2.*pom-1.
              if(UseStartValues) poms=ay(i,ia)
              ay(i,ia)=poms+AmpMax(1)*pom
            enddo
          endif
          if(UseKMod(2)) then
            do i=1,KModA(2,ia)
              do j=1,3
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=ux(j,i,ia)
                ux(j,i,ia)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=uy(j,i,ia)
                uy(j,i,ia)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
              enddo
            enddo
          endif
          if(UseKMod(3)) then
            do i=1,KModA(3,ia)
              do j=1,6
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=bx(j,i,ia)
                bx(j,i,ia)=poms+AmpMax(3)*pom
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=by(j,i,ia)
                by(j,i,ia)=poms+AmpMax(3)*pom
              enddo
            enddo
          endif
        enddo
        call EM40SwitchBetaU(1,0)
        do im=1,NMolec
          do k=1,mam(i)
            ji=k+mxp*(im-1)
            if(UseKMod(1)) then
              do i=1,KModM(1,ji)
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=axm(i,ji)
                axm(i,ji)=poms+AmpMax(1)*pom
                call Random_Number(pom)
                pom=2.*pom-1.
                if(UseStartValues) poms=aym(i,ji)
                aym(i,ji)=poms+AmpMax(1)*pom
              enddo
            endif
            if(UseKMod(2)) then
              do i=1,KModM(2,ji)
                do j=1,3
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=utx(j,i,ji)
                  utx(j,i,ji)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=uty(j,i,ji)
                  uty(j,i,ji)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
                  call Random_Number(pom)
                  pom=(2.*pom-1.)*.1
                  if(UseStartValues) poms=urx(j,i,ji)
                  urx(j,i,ji)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
                  call Random_Number(pom)
                  pom=(2.*pom-1.)*.1
                  if(UseStartValues) poms=ury(j,i,ji)
                  pom=(2.*pom-1.)*.1
                  ury(j,i,ji)=poms+AmpMax(2)*pom/CellPar(j,isw,ksw)
                enddo
              enddo
            endif
            if(UseKMod(3)) then
              do i=1,KModM(3,ji)
                do j=1,6
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=ttx(j,i,ji)
                  ttx(j,i,ji)=poms+AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=tty(j,i,ji)
                  tty(j,i,ji)=poms+AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=tlx(j,i,ji)
                  tlx(j,i,ji)=poms+AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=tly(j,i,ji)
                  tly(j,i,ji)=poms+AmpMax(3)*pom
                enddo
                do j=1,9
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=tsx(j,i,ji)
                  tsx(j,i,ji)=poms+AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  if(UseStartValues) poms=tsy(j,i,ji)
                  tsy(j,i,ji)=poms+AmpMax(3)*pom
                enddo
              enddo
            endif
          enddo
        enddo
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
        if(KCommen(KPhase).gt.0) call iom50(0,0,fln(:ifln)//'.m50')
2200    call Refine(0,RefineEnd)
        if(RefineEnd) go to 1400
        if(ErrFlag.eq.0) then
          call UpdateSummary
        else
          ErrFlag=0
        endif
2300    Veta=fln(:ifln)//'.m70'
        if(ExistFile(Veta)) then
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','unknown')
          if(ErrFlag.ne.0) go to 2100
2320      read(ln,FormA,end=2340) Veta
          m=12
          n=15
          do i=1,5
            j=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                        .false.,.true.)
            if(j.gt.0) then
              k=j+idel(CIFKey(m,n))
              call StToReal(Veta,k,RFac(i),1,.false.,ichp)
              if(ichp.ne.0) RFac(i)=.9999
              if(i.ne.1) RFac(i)=RFac(i)*100.
              go to 2320
            endif
            if(i.eq.1) then
              m=23
            else if(i.eq.2) then
              m=38
            else if(i.eq.3) then
              m=22
            else if(i.eq.4) then
              m=40
            endif
          enddo
          go to 2320
2340      call CloseIfOpened(ln)
        endif
        if(NCykl.gt.0) then
          write(Veta,'(''Trial#'',i5)') NCykl
          call Zhusti(Veta)
          call FeQuestLblChange(nLblLast,Veta)
        endif
        if(RFac(1).gt.90.) then
          write(Veta,103)
        else
          write(Veta,102)(RFac(i),i=1,5)
        endif
        if(NCykl.gt.0) then
          call FeQuestLblChange(nLblRLast,Veta)
        else
          call FeQuestLblChange(nLblROriginal,Veta)
        endif
        if(RFac(1).lt.RFacOpt(1)) then
          RFacOpt=RFac
          call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.b40')
          NCyklBest=NCykl
          write(Veta,'(i5)') NCykl
          call Zhusti(Veta)
          Veta='Best fit hit#'//Veta(:idel(Veta))
          call FeQuestLblChange(nLblBest,Veta)
          write(Veta,102)(RFacOpt(i),i=1,5)
          call FeQuestLblChange(nLblRBest,Veta)
        endif
        go to 2100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call iom40(0,0,fln(:ifln)//'.m40')
9999  if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      call FeQuestRemove(id)
      return
102   format('GOF=',f6.2,5x,'R(obs)=',f6.2,5x,'Rw(obs)=',f6.2,5x,
     1       'R(all)=',f6.2,5x,'Rw(all)=',f6.2)
103   format('GOF=   ---',5x,'R(obs)=   ---',5x,'Rw(obs)=   ---',5x,
     1       'R(all)=   ---',5x,'Rw(all)=   ---')
      end
