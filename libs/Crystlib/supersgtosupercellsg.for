      subroutine SuperSGToSuperCellSG(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      logical eqrv
      real, allocatable :: vt6p(:,:)
      dimension xs(6),rmp(36),xp(3),xpp(3),GammaInt(9),fc(3)
      do i=1,NSymm(KPhase)
        call GetGammaInt(RM6(1,i,1,KPhase),GammaInt)
        call normtr(s6(1,i,1,KPhase),1,1)
        call qbyx(s6(1,i,1,KPhase),xp,1)
        call CopyVek(s6(4,i,1,KPhase),xpp,NDimI(KPhase))
        call Cultm(GammaInt,trez(1,1,KPhase),xpp,NDimI(KPhase),
     1             NDimI(KPhase),1)
        do j=1,NDimI(KPhase)
          xp(j)=xp(j)+trez(j,1,KPhase)-xpp(j)
        enddo
        call CopyVek(s6(1,i,1,KPhase),xs,3)
        call TrSymmPartInSuperCell(xp,RCommenI(1,1,KPhase),xs,ich)
        if(ich.ne.0) then
          ich=i+10
          go to 9000
        endif
        call MatBlock3(rm6(1,i,1,KPhase),rmp,NDim(KPhase))
        call MultM(RCommenI(1,1,KPhase),rm(1,i,1,KPhase),rmp,3,3,3)
        call MultM(rmp,RCommen(1,1,KPhase),rm6(1,i,1,KPhase),3,3,3)
        call CopyVek(xs,s6(1,i,1,KPhase),3)
      enddo
      n=NLattVec(KPhase)
      nvt=0
      do 2200i=1,NLattVec(KPhase)
        call qbyx(vt6(1,i,1,KPhase),xp,1)
        do j=1,NDimI(KPhase)
          xp(j)=xp(j)-vt6(j+3,i,1,KPhase)
        enddo
        call CopyVek(vt6(1,i,1,KPhase),xs,3)
        call TrSymmPartInSuperCell(xp,RCommenI(1,1,KPhase),xs,ich)
        if(ich.ne.0) then
          ich=1000+i
          go to 9000
        endif
        do j=1,nvt
          if(eqrv(xs,vt6(1,j,1,KPhase),3,.0001)) go to 2200
        enddo
        nvt=nvt+1
        call CopyVek(xs,vt6(1,nvt,1,KPhase),3)
2200  continue
      ic=0
      NLattVec(KPhase)=nvt
      do ic3=0,NCommen(3,1,KPhase)-1
        fc(3)=ic3
        do ic2=0,NCommen(2,1,KPhase)-1
          fc(2)=ic2
          do 2400ic1=0,NCommen(1,1,KPhase)-1
            fc(1)=ic1
            call qbyx(fc,xs,1)
            do i=1,NDimI(KPhase)
              if(abs(anint(xs(i))-xs(i)).gt..0001) go to 2400
            enddo
            call MultM(RCommenI(1,1,KPhase),fc,xs,3,3,1)
            call od0do1(xs,xs,3)
            do i=1,NLattVec(KPhase)
              if(eqrv(xs,vt6(1,i,1,KPhase),3,.0001)) go to 2400
            enddo
            NLattVec(KPhase)=NLattVec(KPhase)+1
            call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),
     1        NLattVec(KPhase),NComp(KPhase),NPhase,0)
            call SetRealArrayTo(vt6(1,NLattVec(KPhase),1,KPhase),
     1                          NDim(KPhase),0.)
            call CopyVek(xs,vt6(1,NLattVec(KPhase),1,KPhase),3)
2400      continue
        enddo
      enddo
      allocate(vt6p(NDim(KPhase),1000))
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6(1,i,1,KPhase),vt6p(1,i),NDim(KPhase))
      enddo
      call EM50CompleteCentr(0,vt6p,ubound(vt6p,1),NLattVec(KPhase),
     1                       1000,ich)
      call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6p(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
      enddo
      deallocate(vt6p)
      ScCentrGC=float(n)/float(NLattVec(KPhase))
      go to 9999
9000  if(ich.lt.10) then
        Veta='inversion center'
      else if(ich.lt.1000) then
        write(Veta,100) ich-10
        call zhusti(Veta)
        Veta='symmetry operator #'//Veta(:idel(Veta))
      else
        write(Veta,100) ich-1000
        call zhusti(Veta)
        Veta='centering vector #'//Veta(:idel(Veta))
      endif
      call FeChybne(-1.,-1.,'unforeseen error during transformation in',
     1              Veta(:idel(Veta))//', please contact authors.',
     2              SeriousError)
      ich=1
9999  return
100   format(i5)
      end
