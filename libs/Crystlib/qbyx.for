      subroutine qbyx(x,y,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*),y(*)
      if(NDim(KPhase).gt.3) then
        do i=1,NDimI(KPhase)
          y(i)=scalmul(qu(1,i,isw,KPhase),x)
        enddo
      else
        call SetRealArrayTo(y,3,0.)
      endif
      return
      end
