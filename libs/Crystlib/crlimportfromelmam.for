      subroutine CrlImportFromELMAM
      use Atoms_mod
      use Basic_mod
      use ELMAM_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,t256
      character*80  AtFill(5),AtP,AtPA(1)
      character*8 DBLabelP,DBLocSystP,DBLocSystAtTypeP(2),DBNeighbors
      character*2 AtTypeCentral,AtTypeNeigh,AtTypeNeighUsed
      integer ip(1),UseTabsIn,SbwLnQuest,ButtonStateQuest,AtFillN(5),
     1        AtNeighF(4),nButtAtLocSyst(2)
      logical EqIgCase,FeYesNo,ExistFile
      real AtDist(4),xp(7)
      external CrlImportFromELMAMCheck,FeVoid
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(50.,UseTabs,IdRightTab,' ')
      ln=NextLogicNumber()
      if(PathToELMAM.eq.' ') then
        call FeChybne(-1.,-1.,'the ELMAM2 data file isn''t defined.',
     1                'Use Tools->Programs to specify the file.',
     2                 SeriousError)
        go to 9999
      else
        Veta=PathToELMAM(:idel(PathToELMAM))
        if(.not.ExistFile(Veta)) then
          call FeChybne(-1.,-1.,'the ELMAM2 data file doesn''t exist.',
     1                  'Use Tools->Programs to specify this file.',
     2                  SeriousError)
          go to 9999
        endif
      endif
      if(.not.ChargeDensities) then
        NInfo=3
        TextInfo(1)='The charge denisity option has not been yet '//
     1               'activated.'
        TextInfo(2)='In the next step the program will start the '//
     1              'EditM50 procedure'
        TextInfo(3)='where you can activave this option.'
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call EditM50(ich)
        if(.not.ChargeDensities) then
          call FeChybne(-1.,-1.,'Sorry, you were not successful in '//
     1                  'activating the charge density option. ',
     2                  ' ',SeriousError)
          go to 9999
        endif
      endif
      call OpenFile(ln,PathToELMAM,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      NAtActive=NAtIndToAll(KPhase)-NAtIndFrAll(KPhase)+1
      NAtOffset=NAtIndFrAll(KPhase)-1
      NAtSel=1
      iswELMAM=1
      ich=0
      nDBMax=0
      iDBOld=-1
      id=NextQuestId()
      xqd=600.
      ild=14
      Veta='Import multipole parameters from ELMAM'
      call FeQuestCreate(id,-1.,-1.,xqd,ild,Veta,0,LightGray,0,0)
      il=1
      Veta='Central atom:'
      tpom=(xqd-FeTxLengthUnder(Veta))*.5-20.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+10.
      call FeQuestLblMake(id,tpom,il,' ','L','B')
      nLblCentral=LblLastMade
      Veta='%Skip+previous'
      xpom=35.
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtSkipPrevious=ButtonLastMade
      Veta='S%kip+Next'
      xpom=xqd-dpom-35.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtSkipNext=ButtonLastMade
      Veta='%Previous'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd-dpom)*.5-150.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtPrevious=ButtonLastMade
      Veta='Ne%xt'
      xpom=(xqd-dpom)*.5+150.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtNext=ButtonLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+2
      tpom=5.
      Veta='Number of nei%ghbors:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=40.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwNeighbors=EdwLastMade
      xpom=xpom+dpom+25.
      Veta='%Neighbor atoms from list'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtNeighList=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='Neighbor atom#'
      il=-10*il
      idl=idel(Veta)
      tpom=xpom+dpom+20.
      ilp=il
      do i=1,4
        write(Veta(idl+1:),'(i1,'':'')') i
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblL=LblLastMade
        il=il-7
      enddo
      tpom=tpom+FeTxLengthUnder(Veta)+10.
      il=ilp
      do i=1,4
        call FeQuestLblMake(id,tpom,il,' ','L','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblA=LblLastMade
        il=il-7
      enddo
      il=ilp
      tpom=tpom+85.
      call FeQuestLblMake(id,xqd-5.,il+8,'Distance to central atom','R',
     1                    'N')
      do i=1,4
        call FeQuestLblMake(id,tpom,il,' ','L','B')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblD=LblLastMade
        il=il-7
      enddo
      il=il-2
      call FeQuestLinkaMake(id,il)
      il=il-10
      ils=14
      dpom=xqd*.5
      xpom=5.
      call FeQuestSbwMake(id,xpom,ils,dpom,7,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwSelect=SbwLastMade
      Veta='Sho%w details'
      dpomb=FeTxLengthUnder(Veta)+20.
      xpom=xqd-(dpom+xpom+dpomb)*.5
      call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtInfo=ButtonLastMade
      il=il-16
      tpomp=xqd*.5+35.
      Veta='Local coordinale system :'
      call FeQuestLblMake(id,tpomp,il,Veta,'L','N')
      tpom=tpomp+FeTxLengthUnder(Veta)+10.
      call FeQuestLblMake(id,tpom,il,' ','L','B')
      nLblLocSystem=LblLastMade
      Veta='Atom#'
      tpom=tpomp+FeTxLength(Veta)+15.
      t256='<-- Define'
      dpom=FeTxLength(t256)+10.
      xpom=tpom+FeTxLength(Veta)+60.
      do i=1,2
        il=il-10
        write(Cislo,'(i1)') i
        call FeQuestLblMake(id,tpomp,il,Veta(:idel(Veta))//Cislo(1:1)//
     1                      ':','L','N')
        call FeQuestLblMake(id,tpom,il,' ','L','B')
        call FeQuestButtonMake(id,xpom,il,dpomb,ButYd,t256)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nLblAtLocSyst(i)=LblLastMade
        nButtAtLocSyst(i)=ButtonLastMade
      enddo
1400  call FeQuestLblChange(nLblCentral,Atom(NAtSel+NAtOffset))
      AtTypeCentral=AtType(isf(NAtSel+NAtOffset),KPhase)
      if(NAtSel.le.1) then
        call FeQuestButtonDisable(nButtPrevious)
        call FeQuestButtonDisable(nButtSkipPrevious)
      else
        call FeQuestButtonOff(nButtPrevious)
        call FeQuestButtonOff(nButtSkipPrevious)
      endif
      if(NAtSel.ge.NAtActive) then
        call FeQuestButtonDisable(nButtNext)
        call FeQuestButtonDisable(nButtSkipNext)
      else
        call FeQuestButtonOff(nButtNext)
        call FeQuestButtonOff(nButtSkipNext)
      endif
      call DistFromAtom(Atom(NAtSel+NAtOffset),3.,iswELMAM)
      NNeigh=0
      fnn=0.
      nn=0
      DstMin=10.
      DstMax=0.
      DstAve=0.
      AtTypeNeighUsed=' '
      AtNeigh=' '
      AtDist=0.
      AtFill=' '
      AtLocSyst=' '
      nAtFill=0
      do i=1,NDist
        k=ipord(i)
        ian=ktat(Atom(NAtOffset+1),NAtActive,ADist(k))
        if(ian.le.0) then
          cycle
        else
          isfi=isf(ian)
          AtTypeNeigh=AtType(isfi,KPhase)
        endif
        AtP=ADist(k)
        j=idel(SymCodeJanaDist(k))
        if(j.gt.0) AtP=AtP(:idel(AtP))//'#'//SymCodeJanaDist(k)(:j)
        if(nAtFill.lt.5) then
          nAtFill=nAtFill+1
          AtFill(nAtFill)=AtP
          AtFillN(nAtFill)=ian
        endif
        yy=0.
        if(EqIgCase(AtTypeNeigh,'H')) then
          if(DDist(k).lt.1.2) then
            yy=1.
            go to 1422
          else
            cycle
          endif
        endif
        if(EqIgCase(AtTypeCentral,'C')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.53-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.442-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'B')) then
            yy=exp((1.7-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.390-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'F')) then
            yy=exp((1.32-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'S')) then
            yy=exp((1.770-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Cl')) then
            yy=exp((1.76-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Br')) then
            yy=exp((1.9-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Fe')) then
            yy=exp((1.689-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Co')) then
            yy=exp((1.634-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Cu')) then
            yy=exp((1.446-DDist(k))/.37)
            yy=exp((2.2-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'P')) then
            yy=exp((1.89-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'Si')) then
            yy=exp((1.883-DDist(k))/.37)
          else
            cycle
          endif
        else if(EqIgCase(AtTypeCentral,'N')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.442-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.361-DDist(k))/.37)
          else
            cycle
          endif
        else if(EqIgCase(AtTypeCentral,'O')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.390-DDist(k))/.37)
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.361-DDist(k))/.37)
          else
            cycle
          endif
        else if(EqIgCase(AtTypeCentral,'H')) then
          if(nn.le.0) then
            yy=1.
          else
            yy=0.
          endif
        endif
        if(yy.lt..2) then
          cycle
        endif
1422    AtTypeNeighUsed=AtTypeNeigh
        if(nn.gt.3) cycle
        fnn=fnn+yy
        nn=nn+1
        AtNeigh(nn)=AtP
        AtNeighN(nn)=ian
        AtDist(nn)=DDist(k)
        DstMin=min(DstMin,DDist(k))
        DstMax=max(DstMax,DDist(k))
        DstAve=DstAve+DDist(k)
      enddo
      NNeigh=nn
1430  nLbl=nLblL
      do i=1,4
        if(i.le.NNeigh) then
          call FeQuestLblOn(nLbl)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
      enddo
      nLbl=nLblA
      do i=1,4
        if(i.le.NNeigh) then
          call FeQuestLblChange(nLbl,AtNeigh(i))
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
      enddo
      nLbl=nLblD
      do i=1,4
        if(i.le.NNeigh) then
          write(Veta,'(f8.3)') AtDist(i)
          call Zhusti(Veta)
          call FeQuestLblChange(nLbl,Veta)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
      enddo
      call FeQuestIntEdwOpen(nEdwNeighbors,NNeigh,.false.)
      call FeQuestEudOpen(nEdwNeighbors,1,4,1,0.,0.,0.)
      call DistFromAtom(Atom(NAtSel+NAtOffset),3.,iswELMAM)
      NAtSelOld=NAtSel
      do kk=1,2
        rewind ln
        nDB=0
1450    read(ln,FormA,end=1480) Veta
        if(Veta(1:1).eq.'!') go to 1450
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'ATOM')) then
          call Kus(Veta,k,DBLabelP)
          call Kus(Veta,k,AtP)
          if(.not.EqIgCase(AtP,AtTypeCentral)) go to 1450
          call Kus(Veta,k,DBLocSystP)
          do i=1,2
            call Kus(Veta,k,DBLocSystAtTypeP(i))
          enddo
          call Kus(Veta,k,Cislo)
          call Kus(Veta,k,DBNeighbors)
          AtNeighF=0
          do i=1,idel(DBNeighbors)
            do j=1,NNeigh
              if(AtNeighF(j).ne.0) cycle
              if(EqIgCase(DBNeighbors(i:i),
     1           AtType(isf(AtNeighN(j)),KPhase))) then
                AtNeighF(j)=1
                go to 1455
              endif
            enddo
            go to 1450
1455      enddo
          do j=1,NNeigh
            if(AtNeighF(j).le.0) go to 1450
          enddo
          if(NNeigh.gt.1) then
            do i=1,2
              AtNeighF=0
              do j=1,NNeigh
                if(AtNeighF(j).gt.0) cycle
                if(EqIgCase(DBLocSystAtTypeP(i),
     1             AtType(isf(AtNeighN(j)),KPhase))) then
                  AtNeighF(j)=1
                  go to 1457
                endif
              enddo
              go to 1450
1457        enddo
          endif
1460      read(ln,FormA,end=1480) Veta
          if(Veta(1:1).eq.'!') go to 1460
          k=0
          call Kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'NBOND')) then
            call StToInt(Veta,k,ip,1,.false.,ich)
            if(ich.ne.0) go to 1450
            if(ip(1).eq.NNeigh) then
              nDB=nDB+1
              if(kk.eq.2) then
                DBLabel(nDB)=DBLabelP
                DBLocSyst(nDB)=DBLocSystP
                DBPopAS(1:16,nDB)=0.
                DBLAsMax(nDB)=0
                DBWild(nDB)=' '
                DBText(nDB)=' '
              endif
              ni=0
1470          read(ln,FormA,end=1475) Veta
              if(Veta(1:1).eq.'!') go to 1470
              k=0
              call Kus(Veta,k,Cislo)
              if(EqIgCase(Cislo,'KMD')) then
                ni=ni+10000
                call StToReal(Veta,k,xp,7,.false.,ich)
                if(ich.ne.0) then
                  nDB=nDB-1
                  go to 1450
                endif
                if(kk.eq.2) then
                  DBKappa(1:2,nDB)=xp(1:2)
                  DBPopVal(nDB)=xp(3)
                  DBPopAs(1:4,nDB)=xp(4:7)
                  DBLAsMax(nDB)=max(DBLAsMax(nDB),3)
                  DBLocSystAtType(1:2,nDB)=DBLocSystAtTypeP(1:2)
                endif
              else if(EqIgCase(Cislo,'QUA')) then
                ni=ni+1000
                call StToReal(Veta,k,xp,5,.false.,ich)
                if(ich.ne.0) then
                  nDB=nDB-1
                  go to 1450
                endif
                if(kk.eq.2) then
                  DBPopAs(5:9,nDB)=xp(1:5)
                  DBLAsMax(nDB)=max(DBLAsMax(nDB),4)
                endif
              else if(EqIgCase(Cislo,'OCT')) then
                ni=ni+100
                call StToReal(Veta,k,xp,7,.false.,ich)
                if(ich.ne.0) then
                  nDB=nDB-1
                  go to 1450
                endif
                if(kk.eq.2) then
                  DBPopAs(10:16,nDB)=xp(1:7)
                  DBLAsMax(nDB)=max(DBLAsMax(nDB),5)
                endif
              else if(EqIgCase(Cislo,'WILDCARD')) then
                ni=ni+10
                if(kk.eq.2) then
                  DBWild(nDB)=Veta(k+1:)
                  call SkrtniPrvniMezery(DBWild(nDB))
                endif
              else if(EqIgCase(Cislo,'TEXT')) then
                ni=ni+1
                if(kk.eq.2) then
                  DBText(nDB)=Veta(k+1:)
                  call SkrtniPrvniMezery(DBText(nDB))
                endif
              else if(EqIgCase(Cislo,'ATOM')) then
                if(ni.lt.10000) then
                  nDB=nDB-1
                endif
                backspace ln
                go to 1450
              endif
              go to 1470
            endif
            go to 1450
          else
            go to 1460
          endif
        else
          go to 1450
        endif
1475    nDB=nDB-1
        go to 1450
1480    if(kk.eq.1) then
          n=nDB+1
          if(n.gt.nDBMax) then
            if(allocated(DBLabel))
     1        deallocate(DBLabel,DBText,DBWild,DBLocSyst,DBPopVal,
     2                   DBKappa,DBLAsMax,DBPopAs,DBLocSystAtType)
            allocate(DBLabel(n),DBText(n),DBWild(n),
     1               DBLocSyst(n),DBPopVal(n),DBKappa(2,n),
     2               DBLAsMax(n),DBPopAs(16,n),DBLocSystAtType(2,n))
            nDBMax=n
          endif
        else
          go to 1490
        endif
        cycle
      enddo
1490  call CloseIfOpened(SbwLnQuest(nSbwSelect))
      lnDB=NextLogicNumber()
      t256=fln(:ifln)//'_ELMAM.tmp'
      call DeleteFile(t256)
      call OpenFile(lnDB,t256,'formatted','unknown')
      do i=1,nDB
        write(lnDB,'(a,a1,a)') DBLabel(i)(:idel(DBLabel(i))),Tabulator,
     1                         DBText(i)(:idel(DBText(i)))
      enddo
      call CloseIfOpened(lnDB)
      call FeQuestSbwMenuOpen(nSbwSelect,t256)
      if(nDB.gt.0) then
        iDB=1
        call FeQuestLblChange(nLblLocSystem,DBLocSyst(iDB))
      else
        iDB=0
        call FeQuestLblOff(nLblLocSystem)
      endif
      if(nDB.gt.0) then
        if(ButtonStateQuest(nButtInfo).ne.ButtonOff)
     1    call FeQuestButtonOff(nButtInfo)
      else
        call FeQuestButtonDisable(nButtInfo)
      endif
      call CrlImportFromELMAMCheck
      NAtSelOld=NAtSel
1500  call FeQuestEventWithCheck(id,ich,CrlImportFromELMAMCheck,FeVoid)
      do i=1,2
        call FeQuestLblChange(nLblAtLocSyst(i),AtLocSyst(i))
      enddo
      if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtNext.or.CheckNumber.eq.nButtSkipNext))
     2  then
        NAtSel=NAtSel+1
        iDBOld=-1
        if(CheckNumber.eq.nButtNext) then
          Klic=0
          go to 2500
        else
          go to 1400
        endif
      else if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtPrevious.or.
     2    CheckNumber.eq.nButtSkipPrevious)) then
        NAtSel=NAtSel-1
        iDBOld=-1
        if(CheckNumber.eq.nButtPrevious) then
          Klic=0
          go to 2500
        else
          go to 1400
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtNeighList) then
        call SelNeighborAtoms('Select the neighbor atoms',3.,
     1                        Atom(NAtOffset+NAtSel),AtFill,5,nAtFill,
     2                        iswELMAM,ich)
        if(ich.eq.0) then
          NNeigh=min(nAtFill,4)
          call DistFromAtom(Atom(NAtSel+NAtOffset),3.,iswELMAM)
          do i=1,NNeigh
            AtNeigh(i)=AtFill(i)
            do k=1,NDist
               AtP=ADist(k)
               ian=ktat(Atom(NAtOffset+1),NAtActive,ADist(k))
               j=idel(SymCodeJanaDist(k))
               if(j.gt.0) AtP=AtP(:idel(AtP))//'#'//
     1                        SymCodeJanaDist(k)(:j)
              if(EqIgCase(AtNeigh(i),AtP)) then
                AtDist(i)=DDist(k)
                AtNeighN(nn)=ian
                exit
              endif
            enddo
          enddo
          go to 1430
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInfo)
     1  then
        rewind ln
        lni=NextLogicNumber()
        t256=fln(:ifln)//'_info.tmp'
        call OpenFile(lni,t256,'formatted','unknown')
1700    read(ln,FormA,end=1750) Veta
        if(Veta(1:1).eq.'!') go to 1700
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'ATOM')) then
          call Kus(Veta,k,DBLabelP)
          if(EqIgCase(DBLabelP,DBLabel(iDB))) then
            write(lni,FormA) Veta(:idel(Veta))
1720        read(ln,FormA,end=1750) Veta
            if(Veta(1:1).eq.'!'.or.Veta.eq.' ') go to 1720
            k=0
            call Kus(Veta,k,Cislo)
            if(.not.EqIgCase(Cislo,'ATOM')) then
              write(lni,FormA) Veta(:idel(Veta))
              go to 1720
            endif
          else
            go to 1700
          endif
          call CloseIfOpened(lni)
          call FeEdit(t256)
          call DeleteFile(t256)
        else
          go to 1700
        endif
1750    go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNeighbors)
     1  then
        call FeQuestIntFromEdw(nEdwNeighbors,NNeighNew)
        if(NNeighNew.gt.NNeigh) then
          call DistFromAtom(Atom(NAtSel+NAtOffset),3.,iswELMAM)
          do i=1,NNeighNew
            AtNeigh(i)=AtFill(i)
            do k=1,NDist
               AtP=ADist(k)
               j=idel(SymCodeJanaDist(k))
               if(j.gt.0) AtP=AtP(:idel(AtP))//'#'//
     1                        SymCodeJanaDist(k)(:j)
              if(EqIgCase(AtNeigh(i),AtP)) then
                AtDist(i)=DDist(k)
                exit
              endif
            enddo
          enddo
        endif
        if(NNeighNew.ne.NNeigh) then
          NNeigh=NNeighNew
          go to 1430
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAtLocSyst(1).or.
     2         CheckNumber.eq.nButtAtLocSyst(2))) then
        Veta='Select atom#1 for local coordinate system'
        if(CheckNumber.eq.nButtAtLocSyst(1)) then
          i=1
          NAtPom=NAtSel
        else
          i=2
          if(NNeigh.le.1) then
            NAtPom=AtNeighN(1)
            Veta(13:13)='2'
          else
            NAtPom=NAtSel
          endif
        endif
        call SelNeighborAtoms(Veta,3.,Atom(NAtOffset+NAtPom),AtPA,1,
     1                        nAtPA,iswELMAM,ich)
        if(ich.ne.0.or.nAtPA.ne.1) go to 1500
        AtLocSyst(i)=AtPA(1)
        do i=1,2
          call FeQuestLblChange(nLblAtLocSyst(i),AtLocSyst(i))
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        Klic=2
      else
        go to 3000
      endif
2500  ia=NAtSelOld+NAtOffset
      if(nDB.le.0) then
        LAsMax(ia)=0
        go to 2700
      endif
      if(AtLocSyst(1).eq.' '.or.AtLocSyst(2).eq.' ') then
        Veta='Local system is not properly defined.'
        call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        if(Klic.eq.2) call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        NAtSel=NAtSelOld
        go to 1500
      endif
      ia=NAtSelOld+NAtOffset
      if(EqIgCase(DBLocSyst(iDB),'XY')) then
        LocAtSystAx(ia)='xy'
        LocAtSystSt(1:2,ia)=AtLocSyst(1:2)
      else if(EqIgCase(DBLocSyst(iDB),'ZX')) then
        LocAtSystAx(ia)='zx'
        LocAtSystSt(1:2,ia)=AtLocSyst(1:2)
      else if(EqIgCase(DBLocSyst(iDB),'bXY').or.
     1        EqIgCase(DBLocSyst(iDB),'bZX')) then
        if(EqIgCase(DBLocSyst(iDB),'bXY')) then
          LocAtSystAx(ia)='xy'
        else
          LocAtSystAx(ia)='zx'
        endif
        LocAtSystSt(1,ia)=AtLocSyst(1)(:idel(AtLocSyst(1)))//' ^ '//
     1                    AtLocSyst(2)(:idel(AtLocSyst(2)))
        LocAtSystSt(2,ia)=AtLocSyst(1)(:idel(AtLocSyst(1)))//' - '//
     1                    AtLocSyst(2)(:idel(AtLocSyst(2)))
      else
        call FeChybne(-1.,YBottomMessage,'Jeste neni',' ',SeriousError)
        if(Klic.eq.2) call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        NAtSel=NAtSelOld
        go to 1500
      endif
      LAsMax(ia)=DBLasMax(iDB)
      PopAS(1:16,ia)=DBPopAS(1:16,iDB)
      SmbPGAt(ia)='1'
      popc(ia)=0.
      do i=1,28
        popc(ia)=popc(ia)+PopCore(i,isf(ia),KPhase)
      enddo
      popv(ia)=DBPopVal(iDB)
      Kapa1(ia)=DBKappa(1,iDB)
      Kapa2(ia)=DBKappa(2,iDB)
      LocAtSense(ia)='1'
2700  if(Klic.eq.0) then
        go to 1400
      else if(Klic.eq.1) then
        go to 1400
      endif
3000  if(ich.eq.0) then
        if(FeYesNo(-1.,-1.,'Do you really want to close the form?',0))
     1      then
          do j=1,2
          SumPopv=0.
          SumPopc=0.
          SumProt=0.
          do i=NAtOffset+1,NAtOffset+NAtActive
            if(LAsMax(i).le.0) cycle
            popvp=popv(i)*ai(i)
            SumPopv=SumPopv+popvp+PopAS(1,i)*ai(i)
            SumPopc=SumPopc+popc(i)*ai(i)
            SumProt=SumProt+AtNum(isf(i),KPhase)*ai(i)
          enddo
          pom=(SumProt-SumPopc)/SumPopv
          do i=NAtOffset+1,NAtOffset+NAtActive
            popv(i)=popv(i)*pom
            PopAS(1,i)=PopAS(1,i)*pom
          enddo
          enddo
          call iom40(1,0,fln(:ifln)//'.m40')
        else
          call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1500
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then

      endif
9999  close(ln)
      if(allocated(DBLabel)) deallocate(DBLabel,DBText,DBWild,DBLocSyst,
     1                                  DBPopVal,DBKappa,DBLAsMax,
     2                                  DBPopAs,DBLocSystAtType)
      call FeTabsReset(UseTabs)
      UseTabsIn=UseTabs
      return
      end
