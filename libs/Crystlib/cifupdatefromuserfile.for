      subroutine CIFUpdateFromUserFile(UserFile)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) UserFile
      character*80 CifKeyToBeFound
      character*256 Veta
      character*80  t80
      logical EqIgCase,Konec,ExistFile
      ln=NextLogicNumber()
      if(UserFile.eq.' '.or..not.ExistFile(UserFile)) go to 9999
      call OpenFile(ln,UserFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      Konec=.false.
      n=0
      nline=0
      nloop=0
1100  read(Ln,FormA,end=9999) Veta
      nline=nline+1
1150  k=0
      call Kus(Veta,k,t80)
      if(EqIgCase(t80,'loop_')) then
        nloop=nline
      else if(t80(1:1).eq.'_') then
        CifKeyToBeFound=t80
        go to 1200
      else if(t80(1:1).eq.'#') then
        nloop=0
      endif
      go to 1100
1200  if(nloop.gt.0) then
        m=-1
        do i=1,nline-nloop+1
          backspace Ln
        enddo
      else
        m=1
        n=n+1
        if(n.gt.nCIFArrayUpdate)
     1     call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
        CIFArrayUpdate(n)=Veta
      endif
      nspace=0
1500  read(Ln,FormA,end=1900) Veta
      nline=nline+1
      if(Veta.eq.' ') then
        nspace=nspace+1
        go to 1600
      endif
      k=0
      call Kus(Veta,k,t80)
      if(m.le.0) then
        if(m.lt.0) then
          if(t80(1:1).ne.'_'.and..not.EqIgCase(t80,'loop_')) m=m+2
        else
          if(t80(1:1).eq.'_') m=m+1
        endif
      else
        if(t80(1:1).eq.'_'.or.EqIgCase(t80,'loop_')) then
          go to 2000
        else if(t80(1:1).eq.'#') then
          nspace=nspace+1
          go to 1600
        endif
      endif
      nspace=0
1600  n=n+1
      if(n.gt.nCIFArrayUpdate)
     1   call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
      CIFArrayUpdate(n)=Veta
      go to 1500
1900  Konec=.true.
2000  n=n-nspace
      if(n.gt.0) then
        call CIFUpdateRecord(CifKeyToBeFound,' ',n)
        n=0
        nloop=0
        if(.not.Konec) go to 1150
      endif
9999  call CloseIfOpened(ln)
      return
      end
