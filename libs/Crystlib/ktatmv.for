      function KtAtMv(at,n,klic)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*12 mol
      logical Prvne,wild,eqwild
      save wild,iap,imp,ipp
      data Prvne/.true./
      if(Prvne) then
        wild=index(at,'*').gt.0.or.index(at,'?').gt.0
        if(wild) then
          Prvne=.false.
          iap=1
          imp=1
          ipp=1
        endif
      endif
      if(wild) then
        do i=iap,n
          if(.not.eqwild(Atom(i),at,.false.)) cycle
          KtAtMv=i
          iap=i+1
          go to 9999
        enddo
        if(iap.lt.NAtMolFr(1,1)) iap=NAtMolFr(1,1)
        do i=iap,NAtAll
         if(.not.eqwild(atom(i),at,.false.)) cycle
          KtAtMv=i
          iap=i+1
          go to 9999
        enddo
        if(klic.eq.0) then
          do i=imp,NMolec
            do j=ipp,mam(i)
              ji=j+mxp*(i-1)
              write(mol,'(a8,''#'',i2)') MolName(i),j
              call zhusti(mol)
              if(.not.eqwild(mol,at,.false.)) cycle
              KtAtMv=-ji
              ipp=ipp+1
              if(ipp.gt.mam(i)) then
                ipp=1
                imp=imp+1
              endif
              go to 9999
            enddo
          enddo
        endif
        KtAtMv=0
      else
        if(Prvne) then
          nn=NAtInd
          NAtInd=n
          KtAtMv=KtAtMol(at)
          NAtInd=nn
          Prvne=.false.
        else
          KtAtMv=0
        endif
      endif
      if(KtAtMv.eq.0) Prvne=.true.
9999  return
      end
