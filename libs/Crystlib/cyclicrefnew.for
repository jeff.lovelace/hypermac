      subroutine CyclicRefNew
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 CyclicRefMasterPureFile,FlnOld,Veta,CurrentDirO,
     1              t256,Directory,EdwStringQuest,ListFiles
      character*12 :: UnitsArr(3)=(/'K    ',
     1                              'MPa  ',
     2                              'min  '/),
     3                Variable(3)=(/'Temperature',
     4                              'Pressure   ',
     5                              'Time       '/)
      integer FeChDir,SbwLnQuest,TypeDef,ButtonStateQuest,
     1        SbwItemPointerQuest,SbwItemSelQuest,SbwItemFromQuest
      logical :: ExistFile,EqIgCase,FeYesNo,CopyFiles,CrwLogicQuest,
     1           OnlyOneDirection,UpDownOrder=.true.
      real, allocatable  :: ParArr(:)
      real xp(1)
      UseTabsIn=UseTabs
      FlnOld=Fln
      CurrentDirO=CurrentDir
      call FeFileManager('Select the master file for cyclic refinement',
     1                    CyclicRefMasterFile,'*.cref',0,.false.,ich)
      if(ich.ne.0) go to 9900
      call GetExtentionOfFileName(CyclicRefMasterFile,Veta)
      if(.not.EqIgCase(Veta,'cref'))
     1  CyclicRefMasterFile=
     2    CyclicRefMasterFile(:idel(CyclicRefMasterFile))//'.cref'
      call GetPureFileName(CyclicRefMasterFile,CyclicRefMasterPureFile)
      if(ExistFile(CyclicRefMasterFile)) then
        idl=idel(CyclicRefMasterFile)
        if(.not.FeYesNo(-1.,-1.,'The master file for cyclic '//
     1                  'refinement "'//CyclicRefMasterFile(:idl)//
     2                  '" already exists, rewrite it?',0)) go to 9900
      endif
      call DeleteFile(CyclicRefMasterFile)
      idl=idel(CyclicRefMasterPureFile)
      ListFiles=CyclicRefMasterPureFile(:idl)//'_list.tmp'
      call DeleteFile(ListFiles)
      CyclicRefType=1
      id=NextQuestId()
      il=17
      xqd=550.
      UseTabs=NextTabs()
      call FeTabsAdd(0.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(xqd-180.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(xqd-145.,UseTabs,IdCharTab,'.')
      call FeTabsAdd(xqd-100.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(xqd-70.,UseTabs,IdRightTab,' ')
      Veta='Create the master file and define/import data files'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
      il=1
      Veta='File'
      tpom=95.
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      Veta=Variable(1)
      tpom=xqd-127.
      call FeQuestLblMake(id,tpom,il,Veta,'C','B')
      nLblVariable=LblLastMade
      Veta='Direction'
      tpom=xqd-55.
      call FeQuestLblMake(id,tpom,il,Veta,'C','B')
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=11
      ild=10
      call FeQuestSbwMake(id,xpom,il,dpom,ild,1,CutTextFromRight,
     1                    SbwVertical)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      SbwRightClickAllowed=.false.
      nSbwFiles=SbwLastMade
      il=-il*10-9
      Veta='%Define/copy data files'
      dpom=FeTxLengthUnder(Veta)+60.
      xpom=xqd*.5-dpom-10.
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta=Variable(CyclicRefType)
          call Mala(Veta)
          Veta='Define i%ndividual '//Veta(:idel(Veta))//'s'
          nButtImport=ButtonLastMade
        else if(i.eq.2) then
          nButtParam=ButtonLastMade
        endif
        xpom=xpom+dpom+20.
      enddo
      il=il-10
      Veta='%Select all'
      dpom=FeTxLengthUnder(Veta)+60.
      xpom=xqd*.5-dpom*1.5-20.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='%Revert the direction'
          nButtAll=ButtonLastMade
        else if(i.eq.2) then
          Veta='R%efesh'
          nButtRevert=ButtonLastMade
        else if(i.eq.3) then
          nButtRefresh=ButtonLastMade
        endif
        xpom=xpom+dpom+20.
      enddo
      il=il-10
      xpom=xqd*.5-dpom*.5
      Veta='Up->Down %order'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtUpDown=ButtonLastMade
      il=il-8
      call FeQuestLinkaMake(id,il)
      il=il-8
      Veta='Variable parameter:'
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      call FeBoldFont
      xpom=tpom+FeTxLength(Veta)+10.
      call FeNormalFont
      tpom=xpom+CrwgXd+10.
      Veta='%temperature'
      do i=1,3
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwTemp=CrwLastMade
          Veta='%pressure'
        else if(i.eq.2) then
          nCrwPress=CrwLastMade
          tpome=tpom+FeTxLengthUnder(Veta)+50.
          dpome=100.
          Veta='%Units:'
          xpome=tpome+FeTxLengthUnder(Veta)+10.
          call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpome,EdwYd,
     1                        0)
          call FeQuestStringEdwOpen(EdwLastMade,UnitsArr(1))
          nEdwUnits=EdwLastMade
          Veta='ti%me'
        else if(i.eq.3) then
          nCrwTime=CrwLastMade
        endif
        il=il-8
      enddo
      NCyclicRefFile=0
1400  lns=NextLogicNumber()
      idl=idel(CyclicRefMasterPureFile)
      call CloseIfOpened(SbwLnQuest(nSbwFiles))
      call OpenFile(lns,ListFiles,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      if(NCyclicRefFile.gt.0.and.allocated(CyclicRefFile)) then
        do i=1,NCyclicRefFile
          if(CyclicRefParam(i).lt.-90000.) then
            do j=1,NCyclicRefFile
              CyclicRefOrder(j)=j
            enddo
            OnlyOneDirection=.true.
            go to 1450
          endif
        enddo
        if(allocated(ParArr)) deallocate(ParArr)
        allocate(ParArr(NCyclicRefFile))
        ParMax=CyclicRefParam(1)
        do i=1,NCyclicRefFile
          ParMax=max(ParMax,CyclicRefParam(i))
        enddo
        n1=0
        n2=0
        do i=1,NCyclicRefFile
          if(CyclicRefUpDown(i).gt.0) then
            n1=n1+1
            ParArr(i)=CyclicRefParam(i)
!            if(UpDownOrder) then
!              ParArr(i)=CyclicRefParam(i)
!            else
!              ParArr(i)=2.*ParMax+(ParMax+CyclicRefParam(i))
!            endif
          else
            n2=n2+1
            if(UpDownOrder) then
              ParArr(i)=2.*ParMax+(ParMax-CyclicRefParam(i))
            else
              ParArr(i)=-CyclicRefParam(i)
            endif
          endif
        enddo
        OnlyOneDirection=.not.(n1.gt.0.and.n2.gt.0)
        call RIndexx(NCyclicRefFile,ParArr,CyclicRefOrder)
        if(allocated(ParArr)) deallocate(ParArr)
1450    CyclicRefStart=0
        CyclicRefStart(CyclicRefOrder(1))=1
        do i=1,NCyclicRefFile
          j=CyclicRefOrder(i)
          if(CyclicRefParam(j).lt.-90000.) then
            Cislo='???'
          else
            write(Cislo,'(f15.3)') CyclicRefParam(j)
            call ZdrcniCisla(Cislo,1)
          endif
          if(CyclicRefUpDown(j).gt.0) then
            t256='up'
          else
            t256='down'
          endif
          write(lns,'(5(a1,a))') Tabulator,
     1      CyclicRefFile(j)(:idel(CyclicRefFile(j))),Tabulator,'|',
     2      Tabulator,Cislo(:idel(Cislo)),Tabulator,'|',Tabulator,
     3      t256(:idel(t256))
        enddo
        call FeQuestButtonOff(nButtParam)
        call FeQuestButtonOff(nButtRevert)
        call FeQuestButtonOff(nButtRefresh)
        call FeQuestButtonOff(nButtAll)
        if(OnlyOneDirection) then
          call FeQuestButtonDisable(nButtUpDown)
        else
          if(ButtonStateQuest(nButtUpDown).eq.ButtonDisabled) then
            call FeQuestButtonOff(nButtUpDown)
            UpDownOrder=.true.
          endif
        endif
      else
        call FeQuestButtonDisable(nButtParam)
        call FeQuestButtonDisable(nButtRevert)
        call FeQuestButtonDisable(nButtRefresh)
        call FeQuestButtonDisable(nButtAll)
        call FeQuestButtonDisable(nButtUpDown)
      endif
      call CloseIfOpened(lns)
      ItemSelOld=SbwItemPointerQuest(nSbwFiles)
      ItemFromOld=SbwItemFromQuest(nSbwFiles)
      call FeQuestSbwSelectOpen(nSbwFiles,ListFiles)
      call FeQuestSbwShow(nSbwFiles,ItemFromOld)
      call FeQuestSbwItemOff(nSbwFiles,SbwItemPointerQuest(nSbwFiles))
      call FeQuestSbwItemOn(nSbwFiles,ItemSelOld)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        do i=1,NCyclicRefFile
          if(CyclicRefParam(i).lt.-90000.) then
            Cislo=Variable(CyclicRefType)
            Veta=Cislo(:idel(Cislo))//'s for some data blocks not yet'//
     1           ' defined.'
            call FeChybne(-1.,YBottomMessage,Veta,
     1                    'Please complete them.',SeriousError)
            go to 1500
          endif
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtImport)
     1  then
        Veta='Select/import data files'
        t256='Import_files.tmp'
        call FeSelectFiles(Veta,t256,'*',ich)
        ln=NextLogicNumber()
        call OpenFile(ln,t256,'formatted','unknown')
        read(ln,FormA,end=1660) Veta
        if(index(Veta,'\').gt.0) then
          call ExtractDirectory(Veta,Directory)
          CopyFiles=.not.EqIgCase(Directory,CurrentDir)
        else
          CopyFiles=.false.
        endif
        rewind ln
        NCyclicRefFile=0
1600    read(ln,FormA,end=1620) Veta
        NCyclicRefFile=NCyclicRefFile+1
        call ExtractFileName(Veta,t256)
        if(CopyFiles) call CopyFile(Veta,t256)
        call GetPureFileName(t256,Veta)
        idl=idel(Veta)
        call DeleteAllFiles(Veta(:idl)//'.m*')
        call DeleteAllFiles(Veta(:idl)//'.s*')
        call DeleteFile(Veta(:idl)//'.prf')
        call DeleteFile(Veta(:idl)//'.ref')
        go to 1600
1620    if(NCyclicRefFile.le.0) go to 1660
        rewind ln
        if(allocated(CyclicRefFile))
     1    deallocate(CyclicRefFile,CyclicRefStart,CyclicRefUse,
     1               CyclicRefUpDown,CyclicRefOrder,CyclicRefParam)
        allocate(CyclicRefFile(NCyclicRefFile),
     1           CyclicRefStart(NCyclicRefFile),
     2           CyclicRefUse(NCyclicRefFile),
     3           CyclicRefUpDown(NCyclicRefFile),
     4           CyclicRefOrder(NCyclicRefFile),
     5           CyclicRefParam(NCyclicRefFile))
        i=0
1640    read(ln,FormA,end=1660) Veta
        i=i+1
        call ExtractFileName(Veta,t256)
        CyclicRefStart(i)=0
        CyclicRefUpDown(i)=1
        CyclicRefUse(i)=1
        CyclicRefParam(i)=-99999.
        CyclicRefFile(i)=t256
        go to 1640
1660    close(ln,status='delete')
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtUpDown)
     1  then
        UpDownOrder=.not.UpDownOrder
        if(UpDownOrder) then
          Veta='Up->Down %order'
        else
          Veta='Down->Up %order'
        endif
        call FeQuestButtonLabelChange(nButtUpDown,Veta)
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtParam)
     1  then
        idp=NextQuestId()
        xqdp=450.
        Veta=Variable(CyclicRefType)
        call Mala(Veta)
        Veta='Define '//Veta(:idel(Veta))//' for imported files'
        call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
        il=-10
        Veta='%Derive them from file names'
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,3
          call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,1,1)
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(i.eq.1) then
            nCrwFromFile=CrwLastMade
            Veta='Define them %explicitly in string by ten items'
          else if(i.eq.2) then
            Veta='Define them indi%vidually'
            nCrwFromString=CrwLastMade
          else
            nCrwIndividually=CrwLastMade
          endif
          il=il-8
        enddo
        TypeDef=1
        call FeQuestLinkaMake(idp,il)
        il=il-8
        ilp=il
        tpom=5.
        Veta='%File name:'
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=xqdp-xpom-5.
        call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwFileName=EdwLastMade
        Veta=Variable(CyclicRefType)
        call Mala(Veta)
        Veta='Rewrite the "'//Veta(:idel(Veta))//'" substring by the '//
     1       'question symbol.'
        il=il-8
        tpom=xpom
        call FeQuestLblMake(idp,tpom,il,Veta,'L','N')
        call FeQuestLblOff(LblLastMade)
        nLblFromFile=LblLastMade
        il=ilp
        call FeQuestLblMake(idp,xpom+dpom*.5,il,
     1                      Variable(CyclicRefType),'C','B')
        call FeQuestLblOff(LblLastMade)
        il=il-8
        nLbl10=LblLastMade
        n10=(NCyclicRefFile-1)/10+1
        ik=0
        tpom=5.
        do i=1,min(10,n10)
          ip=ik+1
          ik=min(ik+10,NCyclicRefFile)
          write(Veta,'(''#'',i5,''-#'',i5)') ip,ik
          call Zhusti(Veta)
          if(i.eq.1) then
            xpom=tpom+FeTxLength(Veta)+30.
            dpom=xqdp-xpom-5.
          endif
          call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          if(i.eq.1) nEdw10First=EdwLastMade
          il=il-10
        enddo
        nEdw10Last=EdwLastMade
        il=ilp
        tpom=xqdp-150.
        il=il-8
        tpom=5.
        xpom=tpom+FeTxLength(CyclicRefFile(1))+20.
        dpom=100.
        call FeQuestLblMake(idp,xpom+dpom*.5,il+8,
     1                      Variable(CyclicRefType),'C','B')
        call FeQuestLblOff(LblLastMade)
        nLbl1=LblLastMade
        do i=1,min(10,NCyclicRefFile)
          call FeQuestEdwMake(idp,tpom,il,xpom,il,CyclicRefFile(i),'L',
     1                        dpom,EdwYd,0)
          if(i.eq.1) nEdw1First=EdwLastMade
          il=il-10
        enddo
        i10p=1
        i10k=min(10,n10)
        nEdw1Last=EdwLastMade
        il=il-3
        i1p=1
        i1k=min(10,NCyclicRefFile)
        dpom=FeTxLengthUnder(Veta)+20.
        xpom=xqdp*.5-dpom-8.
        Veta='%Previous'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtPrevious=ButtonLastMade
        xpom=xpom+dpom+16.
        Veta='%Next'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1700    if(TypeDef.eq.1) then
          call FeQuestStringEdwOpen(nEdwFileName,CyclicRefFile(1))
          call FeQuestLblOn(nLblFromFile)
          call FeQuestButtonClose(nButtPrevious)
          call FeQuestButtonClose(nButtNext)
        else
          if(ButtonStateQuest(nButtPrevious).ne.ButtonOff.and.
     1       ButtonStateQuest(nButtPrevious).ne.ButtonDisabled) then
            call FeQuestButtonOff(nButtPrevious)
            call FeQuestButtonOff(nButtNext)
          endif
        endif
        if(TypeDef.eq.2) then
          call FeQuestLblOn(nLbl10)
          nEdw=nEdw10First-1
          jk=(i10p-1)*10
          do i=i10p,i10k
            jp=jk+1
            jk=min(jk+10,NCyclicRefFile)
            nEdw=nEdw+1
            write(Veta,'(''#'',i5,''-#'',i5)') jp,jk
            call Zhusti(Veta)
            call FeQuestEdwLabelChange(nEdw,Veta)
            if(jp.le.jk) then
              Veta=' '
              do j=jp,jk
                if(CyclicRefParam(j).gt.-90000.) then
                  write(Cislo,'(f15.3)') CyclicRefParam(j)
                  call ZdrcniCisla(Cislo,1)
                else
                  Cislo='???'
                endif
                Veta=Veta(:idel(Veta))//' '//Cislo
              enddo
              call FeQuestStringEdwOpen(nEdw,Veta)
            else
              call FeQuestEdwClose(nEdw)
            endif
          enddo
          if(i10p.le.1) then
            call FeQuestButtonDisable(nButtPrevious)
          else
            call FeQuestButtonOff(nButtPrevious)
          endif
          if(i10k.ge.n10) then
            call FeQuestButtonDisable(nButtNext)
          else
            call FeQuestButtonOff(nButtNext)
          endif
        else if(TypeDef.eq.3) then
          call FeQuestLblOn(nLbl1)
          nEdw=nEdw1First
          do i=i1p,i1k
            if(CyclicRefParam(i).gt.-90000.) then
              write(Cislo,'(f15.3)') CyclicRefParam(i)
              call ZdrcniCisla(Cislo,1)
            else
              Cislo='???'
            endif
            call FeQuestEdwLabelChange(nEdw,CyclicRefFile(i))
            call FeQuestStringEdwOpen(nEdw,Cislo)
            nEdw=nEdw+1
          enddo
          do i=i1k+1,i1p+9
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
          if(i1p.le.1) then
            call FeQuestButtonDisable(nButtPrevious)
          else
            call FeQuestButtonOff(nButtPrevious)
          endif
          if(i1k.ge.NCyclicRefFile) then
            call FeQuestButtonDisable(nButtNext)
          else
            call FeQuestButtonOff(nButtNext)
          endif
        endif
1750    call FeQuestEvent(idp,ichp)
        if(CheckType.eq.EventCrw) then
          if(CheckNumber.ge.nCrwFromFile.and.
     1       CheckNumber.le.nCrwIndividually) then
            if(TypeDef.eq.1) then
              call FeQuestEdwClose(nEdwFileName)
              call FeQuestLblOff(nLblFromFile)
            else if(TypeDef.eq.2) then
              nEdw=nEdw10First
              do i=1,n10
                call FeQuestEdwClose(nEdw)
                nEdw=nEdw+1
              enddo
            else if(TypeDef.eq.3) then
              call FeQuestLblOff(nLbl1)
              nEdw=nEdw1First
              do i=1,min(10,NCyclicRefFile)
                call FeQuestEdwClose(nEdw)
                nEdw=nEdw+1
              enddo
            endif
          endif
          TypeDef=CheckNumber-nCrwFromFile+1
          go to 1700
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtPrevious.or.
     2           CheckNumber.eq.nButtNext)) then
          if(TypeDef.eq.2) then
            nEdw=nEdw10First-1
            jk=(i10p-1)*10
            do i=i10p,i10k
              nEdw=nEdw+1
              jp=jk+1
              jk=min(jk+10,NCyclicRefFile)
              Veta=EdwStringQuest(nEdw)
              k=0
              do j=jp,jk
                call Kus(Veta,k,Cislo)
                if(Cislo(1:1).eq.'?') then
                  CyclicRefParam(j)=-99999.
                else
                  read(Cislo,*,err=1760) CyclicRefParam(j)
                  cycle
1760              CyclicRefParam(j)=-99999.
                endif
              enddo
            enddo
            if(CheckNumber.eq.nButtPrevious) then
              i10p=max(i1p-10,1)
            else
              i10p=min(i10p+10,NCyclicRefFile)
            endif
            i10k=min(i10p+9,NCyclicRefFile)
            go to 1700
          else if(TypeDef.eq.3) then
            nEdw=nEdw1First-1
            do i=i1p,i1k
              nEdw=nEdw+1
              Cislo=EdwStringQuest(nEdw)
              if(Cislo(1:1).eq.'?') then
                CyclicRefParam(i)=-99999.
              else
                read(Cislo,*,err=1770) CyclicRefParam(i)
                cycle
              endif
1770          CyclicRefParam(i)=-99999.
            enddo
            if(CheckNumber.eq.nButtPrevious) then
              i1p=max(i1p-10,1)
            else
              i1p=min(i1p+10,NCyclicRefFile)
            endif
            i1k=min(i1p+9,NCyclicRefFile)
          endif
          go to 1700
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1750
        endif
        if(ichp.eq.0) then
          if(TypeDef.eq.1) then
            Veta=EdwStringQuest(nEdwFileName)
            ip=0
            ik=0
            do i=1,idel(Veta)
              if(Veta(i:i).eq.'?') then
                if(ip.le.0) ip=i
                ik=i
              else
                if(ip.gt.0.and.ik.gt.0) exit
              endif
            enddo
            do i=1,NCyclicRefFile
              read(CyclicRefFile(i)(ip:ik),*) CyclicRefParam(i)
            enddo
          else if(TypeDef.eq.2) then
            nEdw=nEdw10First-1
            jk=(i10p-1)*10
            do i=i10p,i10k
              nEdw=nEdw+1
              jp=jk+1
              jk=min(jk+10,NCyclicRefFile)
              Veta=EdwStringQuest(nEdw)
              k=0
              do j=jp,jk
                if(k.ge.idel(Veta)) then
                  CyclicRefParam(j)=-99999.
                  cycle
                endif
                call Kus(Veta,k,Cislo)
                if(Cislo(1:1).eq.'?') then
                  CyclicRefParam(j)=-99999.
                else
                  read(Cislo,*,err=1810) CyclicRefParam(j)
                  cycle
1810              CyclicRefParam(j)=-99999.
                endif
              enddo
            enddo
          else if(TypeDef.eq.3) then
            nEdw=nEdw1First-1
            do i=i1p,i1k
              nEdw=nEdw+1
              Cislo=EdwStringQuest(nEdw)
              if(Cislo(1:1).eq.'?') then
                CyclicRefParam(i)=-99999.
              else
                read(Cislo,*,err=1820) CyclicRefParam(i)
                cycle
              endif
1820          CyclicRefParam(i)=-99999.
            enddo
          endif
        endif
        call FeQuestRemove(idp)
        if(ichp.ne.0) then

        endif
        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        if(CheckNumber.eq.nButtAll) then
          j=1
        else
          j=0
        endif
        lni=SbwLnQuest(nSbwFiles)
        i=0
        rewind lni
1900    read(lni,FormA,end=1920) Veta
        i=i+1
        call FeQuestSetSbwItemSel(i,nSbwFiles,j)
        go to 1900
1920    call CloseIfOpened(SbwLnQuest(nSbwFiles))
        ItemSelOld=SbwItemPointerQuest(nSbwFiles)
        ItemFromOld=SbwItemFromQuest(nSbwFiles)
        call FeQuestSbwSelectOpen(nSbwFiles,ListFiles)
        call FeQuestSbwShow(nSbwFiles,ItemFromOld)
        call FeQuestSbwItemOff(nSbwFiles,SbwItemPointerQuest(nSbwFiles))
        call FeQuestSbwItemOn(nSbwFiles,ItemSelOld)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRevert)
     1  then
        do i=1,NCyclicRefFile
          if(SbwItemSelQuest(i,nSbwFiles).eq.1) then
            j=CyclicRefOrder(i)
            CyclicRefUpDown(j)=-CyclicRefUpDown(j)
            call FeQuestSetSbwItemSel(i,nSbwFiles,0)
          endif
        enddo
        go to 1400
      else if(CheckType.eq.EventCrw) then
        UnitsArr(CyclicRefType)=EdwStringQuest(nEdwUnits)
        CyclicRefType=CheckNumber-nCrwTemp+1
        Veta=Variable(CyclicRefType)
        call FeQuestStringEdwOpen(nEdwUnits,UnitsArr(CyclicRefType))
        call FeQuestLblChange(nLblVariable,Veta)
        call Mala(Veta)
        Veta='Define i%ndividual '//Veta(:idel(Veta))//'s'
        call FeQuestButtonLabelChange(nButtParam,Veta)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0.and.NCyclicRefFile.gt.0) then
        CyclicRefTypeUnits=EdwStringQuest(nEdwUnits)
        call IOCyclicRef(1)
      endif
      call FeQuestRemove(id)
      call DeleteFile(ListFiles)
      if(ich.ne.0.or.NCyclicRefFile.le.0) go to 9900
      if(FeYesNo(-1.,-1.,'Do you want to continue with cyclic '//
     1           'refinemnt right now?',1)) then
        call CloseIfOpened(ln)
        call FeTabsReset(UseTabs)
        UseTabs=UseTabsIn
        call CyclicRef(1)
        go to 9999
      endif
9900  i=FeChDir(CurrentDirO)
      call FeGetCurrentDir
      call CloseIfOpened(ln)
      Fln=FlnOld
      iFln=idel(Fln)
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
9999  return
      end
