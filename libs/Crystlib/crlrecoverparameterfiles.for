      subroutine CrlRecoverParameterFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 Veta
      logical FeYesNo
      Veta='Do you really want to recover the parameter file'
      j=idel(Veta)
      if(isPowder.or.ExistElectronData) then
        Veta=Veta(:j)//'s?'
      else
        Veta=Veta(:j)//'?'
      endif
      if(FeYesNo(-1.,-1.,Veta,0)) then
        call CopyFile(fln(:ifln)//'.s40',fln(:ifln)//'.m40')
        if(ExistPowder) then
          call CopyFile(fln(:ifln)//'.s41',fln(:ifln)//'.m41')
          call iom40(0,0,fln(:ifln)//'.m40')
          do KPh=1,NPhase
            call CopyVek(CellPwd(1,KPh),CellPar(1,1,KPh),6)
            call SetRealArrayTo(CellPwds(1,KPh),6,0.)
            call CopyVek(QuPwd(1,1,KPh),Qu(1,1,1,KPh),3*NDimI(KPh))
          enddo
          call iom50(1,0,fln(:ifln)//'.m50')
        else if(ExistElectronData) then
          call CopyFile(fln(:ifln)//'.s42',fln(:ifln)//'.m42')
        endif
      endif
      return
      end
