      subroutine EqStringToModEq(String,Variables,n,LeftSide,RightSide,
     1                           Modulo,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String,Variables
      character*80 Radka
      integer LeftSide(n),RightSide,Modulo,ich
      logical EqIgCase
      ich=0
      ieq=index(String,'=')
      if(ieq.le.0) go to 9000
      Radka=String(:ieq-1)
      Cislo=' '
      LeftSide=0
      do m=1,idel(Radka)
        j=LocateSubstring(Variables,Radka(m:m),.false.,.true.)
        if(j.gt.0) then
          if(Cislo.eq.' '.or.Cislo.eq.'+') then
            Cislo='1'
          else if(Cislo.eq.'-') then
            Cislo='-1'
          endif
          call Posun(Cislo,0)
          read(Cislo,FormI15,err=9000) l
          LeftSide(j)=LeftSide(j)+l
          Cislo=' '
        else if(Radka(m:m).eq.'*') then
          cycle
        else
          j=index(Cifry,Radka(m:m))
          if(j.eq.11) go to 9000
          Cislo=Cislo(:idel(Cislo))//Radka(m:m)
        endif
      enddo
      Radka=String(ieq+1:)
      Modulo=0
      RightSide=0
      do m=1,idel(Radka)
        if(EqIgCase(Radka(m:m),'n')) then
          Type=1
        else if((Radka(m:m).eq.'+'.or.Radka(m:m).eq.'-').and.
     1          Cislo.ne.' ') then
          Type=2
          RightSide=RightSide+l
        else if(Radka(m:m).eq.'*') then
          cycle
        else
          j=index(Cifry,Radka(m:m))
          if(j.eq.11) go to 9000
          Cislo=Cislo(:idel(Cislo))//Radka(m:m)
          if(m.eq.idel(Radka)) then
            Type=2
          else
            cycle
          endif
        endif
        if(Cislo.eq.' '.or.Cislo.eq.'+') then
          Cislo='1'
        else if(Cislo.eq.'-') then
          Cislo='-1'
        endif
        call Posun(Cislo,0)
        read(Cislo,FormI15,err=9000) l
        if(Type.eq.1) then
          Modulo=l
        else if(Type.eq.2) then
          RightSide=RightSide+l
        endif
        Cislo=' '
      enddo
      go to 9999
9000  ich=1
9999  return
      end
