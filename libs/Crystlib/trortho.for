      subroutine trortho(klic)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodap(7)
      character*80 t80
      if(SwitchedToHarm.eqv.Klic.eq.0) then
        if(VasekTest.ne.0) then
          if(Klic.eq.0) then
            t80='do harmonickych'
          else
            t80='do ortho'
          endif
          t80='chce prepinat '//t80(:idel(t80))//
     1        ', ale ono je jiz prepnuto'
          call FeChybne(-1.,-1.,t80,' ',Warning)
        endif
        go to 9999
      endif
      KPhaseIn=KPhase
      do ia=1,NAtCalc
        if(TypeModFun(ia).ne.1) cycle
        isw=iswa(ia)
        KPhase=kswa(ia)
        itfi=itf(ia)
        if(klic.eq.0) then
          call SetIntArrayTo(kmodap,7,0)
          kmodap(1)=KModA(1,ia)
          do n=2,itfi+1
            if(KModA(n,ia).le.0) cycle
            kmodap(n)=(OrthoSel(2*KModA(n,ia)+1,ia)+1)/2
          enddo
          call ReallocateAtomParams(NAtAll,itfmax,IFrMax,kmodap,
     1                              MagParMax)
          call SetIntArrayTo(kmodap,7,0)
          kmodap(1)=KModA(1,ia)
          do n=2,itfi+1
            kmodap(n)=KModA(n,ia)
            nrank=TRank(n-1)
            if(n.eq.2) then
              call trzor(x(1,ia),ux(1,1,ia),uy(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sux(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(suy(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            else if(n.eq.3) then
              call trzor(beta(1,ia),bx(1,1,ia),by(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sbx(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(sby(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            else if(n.eq.4) then
              call trzor(c3(1,ia),c3x(1,1,ia),c3y(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sc3x(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(sc3y(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            else if(n.eq.5) then
              call trzor(c4(1,ia),c4x(1,1,ia),c4y(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sc4x(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(sc4y(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            else if(n.eq.6) then
              call trzor(c5(1,ia),c5x(1,1,ia),c5y(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sc5x(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(sc5y(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            else
              call trzor(c6(1,ia),c6x(1,1,ia),c6y(1,1,ia),nrank,
     1                   KFA(n,ia),kmodap(n),kmodao(n,ia),
     2                   OrthoMat(1,ia),OrthoSel(1,ia),OrthoOrd)
              nn=kmodap(n)-kmodao(n,ia)
              if(nn.gt.0) then
                call SetRealArrayTo(sc6x(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
                call SetRealArrayTo(sc6y(1,kmodao(n,ia)+1,ia),nn*NRank,
     1                              0.)
              endif
            endif
          enddo
          if(ia.le.NAtCalc.or.ia.ge.NAtMolFr(1,1))
     1      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),kmodap,
     2                     MagPar(ia),.true.)
          do n=2,itfi+1
            KModA(n,ia)=kmodap(n)
          enddo
          if(kmol(ia).eq.0) call qbyx(x(1,ia),qcnt(1,ia),isw)
        else
          do n=2,itfi+1
            nrank=TRank(n-1)
            kmod=kmodao(n,ia)
            if(kmod.le.0) kmod=KModA(n,ia)
            if(n.eq.2) then
              call trdoor(x(1,ia),ux(1,1,ia),uy(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.3) then
              call trdoor(beta(1,ia),bx(1,1,ia),by(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.4) then
              call trdoor(c3(1,ia),c3x(1,1,ia),c3y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.5) then
              call trdoor(c4(1,ia),c4x(1,1,ia),c4y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.6) then
              call trdoor(c5(1,ia),c5x(1,1,ia),c5y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else
              call trdoor(c6(1,ia),c6x(1,1,ia),c6y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            endif
          enddo
          do n=itfi+2,7
            kmodao(n,ia)=0
          enddo
        endif
      enddo
      SwitchedToHarm=Klic.eq.0
      KPhase=KPhaseIn
9999  return
      end
