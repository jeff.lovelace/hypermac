      subroutine TestTOFLorentz
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 SvFile
      dimension XTOF(-500:500)
      real ITOF(-500:500,10)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,40.,YBottomMargin,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      call UnitMat(F2O,3)
      xomn= 9999.
      xomx=-9999.
      yomn= 0.
      yomx=-100.
      be=.01
      al=.05
      ga=10.
      do i=-500,500
        XTOF(i)=float(i)
        ITOF(i,1)=ConvTOFWithLorentz(XTOF(i),al,be,ga,dx,da,db,dg)
        ITOF(i,2)=(ConvTOFWithLorentz(XTOF(i)+.1,al,be,ga,dap,dbp,dxp,
     1                                dgp)-ITOF(i,1))*10.
        ITOF(i,1)=dx
        xomn=min(xomn,XTOF(i))
        xomx=max(xomx,XTOF(i))
        yomn=min(yomn,ITOF(i,1),ITOF(i,2))
        yomx=max(yomx,ITOF(i,1),ITOF(i,2))
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'TOF')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      do i=1,2
        if(i.eq.1) then
          j=Red
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Magenta
        else if(i.eq.4) then
          j=Khaki
        else if(i.eq.5) then
          j=Blue
        endif
        call FeXYPlot(XTOF(-500),ITOF(-500,i),1001,NormalLine,
     1                  NormalPlotMode,j)
      enddo
      call FeReleaseOutput
      call FeDeferOutput
6000  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 6000
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
