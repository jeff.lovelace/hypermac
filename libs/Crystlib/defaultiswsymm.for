      subroutine DefaultISwSymm(isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(isw.eq.0) then
        ip=1
        ik=NComp(KPhase)
      else
        ip=isw
        ik=isw
      endif
      do i=ip,ik
        call SetIntArrayTo(ISwSymm(1,i,KPhase),NSymm(KPhase),i)
      enddo
      return
      end
