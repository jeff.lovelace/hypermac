      subroutine OriginShift
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension xp(6),TransVecPom(6),TransVecIn(*)
      integer EdwStateQuest
      character*256 EdwStringQuest
      character*80  Veta
      character*8   Atp
      logical UsePoint,CrwLogicQuest
      call SetRealArrayTo(TransVecPom,NDim(KPhase),0.)
      Atp=' '
      UsePoint=.true.
      il=2
      id=NextQuestId()
      Veta='Move the origin to:'
      dpom=60.
      xqd=80.+float(NDim(KPhase))*(dpom+25.)
      call FeQuestCreate(id,-1.,-1.,xqd,2,Veta,0,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwXd+5.
      tpome=80.
      Veta='%Point:'
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwPoint=CrwLastMade
          tpp=tpome
          xpp=tpome+5.
          do j=1,NDim(KPhase)
            if(NDimI(KPhase).gt.0) then
              Veta=smbx6(j)
            else
              Veta=smbx(j)
            endif
            call FeQuestEdwMake(id,tpp,il,xpp,il,Veta,'R',dpom,EdwYd,0)
            xpp=xpp+dpom+25.
            tpp=tpp+dpom+25.
            if(j.eq.1) nEdwPoint=EdwLastMade
          enddo
          Veta='%Atom:'
        else
          call FeQuestEdwMake(id,tpome,il,tpome+5.,il,' ','R',2*dpom,
     1                        EdwYd,0)
          nEdwAtom=EdwLastMade
        endif
      enddo
1400  if(UsePoint) then
        if(EdwStateQuest(nEdwAtom).eq.EdwOpened) then
          Atp=EdwStringQuest(nEdwAtom)
          call FeQuestEdwClose(nEdwAtom)
        endif
        nEdw=nEdwPoint
        do i=1,NDim(KPhase)
          call FeQuestRealEdwOpen(nEdw,TransVecPom(i),.false.,.true.)
          nEdw=nEdw+1
        enddo
      else
        if(EdwStateQuest(nEdwPoint).eq.EdwOpened) then
          nEdw=nEdwPoint
          do i=1,NDim(KPhase)
            call FeQuestRealFromEdw(nEdw,TransVecPom(i))
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
        endif
        call FeQuestStringEdwOpen(nEdwAtom,Atp)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwPoint.or.CheckNumber.eq.nCrwPoint+1)) then
        UsePoint=CrwLogicQuest(nCrwPoint)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(UsePoint) then
          nEdw=nEdwPoint
          do i=1,NDim(KPhase)
            call FeQuestRealFromEdw(nEdw,TransVecPom(i))
            TransVecPom(i)=-TransVecPom(i)
            nEdw=nEdw+1
          enddo
        else
          Atp=EdwStringQuest(nEdwAtom)
          i=ktat(Atom(NAtIndFrAll(KPhase)),NAtIndLenAll(KPhase),Atp)
          if(i.le.0) then
            Veta='Atom "'//Atp(:idel(Atp))//'" doen''t exist - try '//
     1           'again.'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
          call SetRealArrayTo(TransVecPom,NDim(KPhase),0.)
          call RealVectorToOpposite(x(1,i),TransVecPom,3)
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      go to 2000
      entry OriginDefinedShift(TransVecIn)
      call CopyVek(TransVecIn,TransVecPom,NDim(KPhase))
2000  ntrans=1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      call UnitMat(TransMat,NDim(KPhase))
      call CopyVek(TransVecPom,TransVec,NDim(KPhase))
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      call RefOpenCommands
      call EM40SetTr(0)
      call EM40TransAll(ich)
      call iom40only(1,0,fln(:ifln)//'.m40')
      do i=1,NSymm(KPhase)
        call multm(rm6(1,i,1,KPhase),TransVec,xp,NDim(KPhase),
     1             NDim(KPhase),1)
        do j=1,NDim(KPhase)
          s6(j,i,1,KPhase)=s6(j,i,1,KPhase)-xp(j)+TransVec(j,1,1)
        enddo
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
      enddo
      if(KCommen(KPhase).gt.0)
     1  call AddVek(TRez(1,1,KPhase),TransVecPom(4),TRez(1,1,KPhase),
     2              NDimI(KPhase))
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call RefRewriteCommands(1)
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
9999  if(allocated(TransMat)) call DeallocateTrans
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      return
      end
