      subroutine SelAtomsFromPhase(Text,AtomP,Brat,kf,isfp,n,ich)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kf(n),isfp(n),isfVyber(:)
      character*(*) Text,AtomP(n)
      character*8   AtomVyber(:)
      logical Brat(*),BratVyber(:)
      allocatable AtomVyber,BratVyber,isfVyber
      allocate(AtomVyber(NAtAll),BratVyber(NAtAll),isfVyber(NAtAll))
      nv=0
      do i=1,n
        if(kf(i).eq.KPhase) then
          nv=nv+1
          AtomVyber(nv)=AtomP(i)
          BratVyber(nv)=Brat(i)
          isfVyber(nv)=isfp(i)
        endif
      enddo
      call SelAtoms(Text,AtomVyber,BratVyber,isfVyber,nv,ich)
      nv=0
      do i=1,n
        if(kf(i).eq.KPhase) then
          nv=nv+1
          Brat(i)=BratVyber(nv)
        else
          Brat(i)=.false.
        endif
      enddo
      deallocate(AtomVyber,BratVyber,isfVyber)
      return
      end
