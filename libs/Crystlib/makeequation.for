      subroutine MakeEquation(i,Veta)
      use Basic_mod
      character*(*) Veta
      character*80  s80
      Veta=lpa(i)
      if(lat(i).eq.' ') then
        Veta=Veta(:idel(Veta))//'='
      else
        Veta=Veta(:idel(Veta))//'['//lat(i)(:idel(lat(i)))//']='
      endif
      if(pab(i).ne.0..or.npa(i).eq.0) then
        call Real2String(pab(i),s80,4)
        call zhusti(s80)
        call RealToFreeForm(s80)
        if(idel(s80).gt.0) Veta=Veta(:idel(Veta))//s80
      endif
      do j=1,npa(i)
        pom=pko(j,i)
        call Real2String(pom,s80,4)
        call RealToFreeForm(s80)
        k=idel(Veta)
        if(Veta(k:k).ne.'='.and.pom.ge.0.) s80(1:1)='+'
        call RealToFreeForm(s80)
        call zhusti(s80)
        if(s80.eq.'0') then
          pko(j,i)=0.
          pom=0.
        endif
        if(s80.eq.'+1') then
          Veta=Veta(:k)//'+'
        else if(s80.eq.'-1') then
          Veta=Veta(:k)//'-'
        else if(s80.ne.'1') then
          Veta=Veta(:k)//s80(:idel(s80))//'*'
        endif
        k=idel(Veta)
        Veta=Veta(:k)//ppa(j,i)(:idel(ppa(j,i)))
        kp=k+1
        if(pat(j,i).ne.' ')
     1    Veta=Veta(:idel(Veta))//'['//pat(j,i)(:idel(pat(j,i)))//']'
      enddo
      return
      end
