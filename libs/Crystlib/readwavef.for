      subroutine ReadWaveF(Atom,iat,Orbit,pop,ffp,nffp,Klic,AtWaveFile,
     1                     ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real z(7,40),coef(7,40),pop(7),ff(10),ffp(*),orb0(240),orb1(240),
     1     orb2(240),DSto(0:2),zp(40),coefp(40),xp(3)
      integer ip(2),n(7,40),np(40),nsto(7)
      character*(*) AtWaveFile
      character*256 t256,STOString,UseWaveFile
      character*2 Atom,AtomIn
      character*1 Orbit,OrbitIn
      logical EqIgCase,UseXDBnk,ExistFile
      ich=0
      ln=NextLogicNumber()
      if(EqIgCase(AtWaveFile,'FF-table')) go to 9999
      if(Klic.eq.0) then
        ds=.62831853
      else
        ds=.0333333
      endif
      if(EqIgCase(AtWaveFile,'STO-table')) then
        j=0
        do i=1,7
          if(EqIgCase(OrbitName(i),Orbit)) then
            j=i
            exit
          endif
        enddo
        nao=0
        nsto=0
        do i=1,7
          nn=NCoefSTOA(i,j,iat,KPhase)
          if(nn.le.0) cycle
          nao=nao+1
          nsto(nao)=nn
          do k=1,nn
            n(nao,k)=NSTOA(i,j,k,iat,KPhase)
            z(nao,k)=ZSTOA(i,j,k,iat,KPhase)
            coef(nao,k)=CSTOA(i,j,k,iat,KPhase)
          enddo
        enddo
      else
        UseWaveFile=NameOfWaveFile
        if(AtWaveFile.ne.' '.and..not.EqIgCase(AtWaveFile,'Default'))
     1    then
          do i=1,3
            if(EqIgCase(AtWaveFile,MenuWF(i))) then
              UseWaveFile=MenuWF(i)
            endif
          enddo
        endif
        t256=HomeDir(:idel(HomeDir))
        if(OpSystem.le.0) then
          t256=t256(:idel(t256))//'formfac'
        else
          t256=t256(:idel(t256))//'formfac'
        endif
        UseXDBnk=
     1    LocateSubstring(NameOfWaveFile,'.bnk',.false.,.true.).gt.0
        if(UseXDBnk) then
          t256=XDBnkDir(:idel(XDBnkDir))//DirectoryDelimitor//
     1         UseWaveFile(:idel(UseWaveFile))
        else
          t256=t256(:idel(t256))//DirectoryDelimitor//
     1         UseWaveFile(:idel(UseWaveFile))
        endif
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.ne.0) go to 9000
        if(UseXDBnk) then
          nao=0
1100      read(ln,FormA,end=1400) t256
          if(.not.EqIgCase(':'//Atom(:idel(Atom)),t256(1:3))) go to 1100
          read(ln,FormA,end=1400) t256
          read(ln,FormA,end=1400) t256
          read(ln,FormA,end=1400) STOString
          k=0
1150      call kus(STOString,k,Cislo)
          if(.not.EqIgCase(Cislo,'STO')) go to 1400
1200      call kus(STOString,k,Cislo)
          OrbitIn=Cislo(2:2)
          call StToInt(STOString,k,ip,2,.false.,ich)
          if(ich.ne.0) go to 1400
          nstop=ip(2)
          i=0
1250      if(mod(i,4).eq.0) then
            read(ln,FormA,end=1400) t256
            kk=0
          endif
          i=i+1
          call StToReal(t256,kk,xp,3,.false.,ich)
          np(i)=nint(xp(3))
          coefp(i)=xp(1)
          zp(i)=xp(2)
          if(i.lt.nstop) go to 1250
          if(EqIgCase(Orbit,OrbitIn)) then
            nao=nao+1
            nsto(nao)=nstop
            do ii=1,nstop
              n(nao,ii)=np(ii)
              z(nao,ii)=zp(ii)
              coef(nao,ii)=coefp(ii)
            enddo
          endif
          if(k.lt.len(STOString)) go to 1200
        else
          read(ln,FormA) t256
          if(t256(1:1).ne.'#') rewind ln
1300      read(ln,FormA,end=9100,err=9100) t256
          if(.not.EqIgCase(Atom,t256(1:2))) go to 1300
          read(t256,FormA,end=9100,err=9100) AtomIn
          nao=0
1350      read(ln,FormA,end=1400) t256
          if(t256.eq.' ') go to 1350
          if(t256(1:1).eq.' ') then
            read(t256,'(i2,a1,i3)',end=1400,err=1400) j,OrbitIn,nstop
            read(ln,'(4(f12.8,f12.4,i2))')(coefp(i),zp(i),np(i),
     1                                     i=1,nstop)
            if(EqIgCase(OrbitIn,Orbit)) then
              nao=nao+1
              nsto(nao)=nstop
              do i=1,nstop
                n(nao,i)=np(i)
                z(nao,i)=zp(i)
                coef(nao,i)=coefp(i)
              enddo
            endif
            go to 1350
          endif
        endif
      endif
1400  if(nao.le.0) go to 9000
      do j=1,nao
        do i=1,nsto(j)
          z2=2.*z(j,i)/BohrRad
          rn=z2
          do k=1,2*n(j,i)
            rn=rn*z2/float(k)
          enddo
          rn=sqrt(rn)
          coef(j,i)=coef(j,i)*rn
        enddo
      enddo
      if(Klic.eq.0) then
        do 1500iao=1,nao
          if(pop(iao).eq.0.) go to 1500
          do ib1=1,nsto(iao)
            z1=z(iao,ib1)/BohrRad
            n1=n(iao,ib1)
            do ib2=1,ib1
              zz=z1+z(iao,ib2)/BohrRad
              nn=n1+n(iao,ib2)
              if(ib1.eq.ib2) then
                cn=1.
              else
                cn=2.
              endif
              s=-ds
              do i=1,nffp
                s=s+ds
                call TRANSO(nn,1,zz,s,ff)
                cao=cn*coef(iao,ib1)*coef(iao,ib2)*pop(iao)
                ffp(i)=ff(1)*cao+ffp(i)
              enddo
            enddo
          enddo
1500    continue
      else if(Klic.eq.1) then
        do 2500iao=1,nao
          call SetRealArrayTo(orb0,240,0.)
          if(pop(iao).eq.0.) go to 2500
          do ib1=1,nsto(iao)
            z1=z(iao,ib1)/BohrRad
            n1=n(iao,ib1)-1
            cao=coef(iao,ib1)
            s=0.
            do i=1,240
              orb0(i)=orb0(i)+cao*SlaterDensity(s,z1,n1)
              s=s+ds
            enddo
          enddo
          do i=1,240
            ffp(i)=ffp(i)+pop(iao)*orb0(i)**2
          enddo
2500    continue
      else
        do 3500iao=1,nao
          call SetRealArrayTo(orb0,240,0.)
          call SetRealArrayTo(orb1,240,0.)
          call SetRealArrayTo(orb2,240,0.)
          if(pop(iao).eq.0.) go to 3500
          do ib1=1,nsto(iao)
            z1=z(iao,ib1)/BohrRad
            n1=n(iao,ib1)-1
            cao=coef(iao,ib1)
            s=0.
            do i=1,240
              call SlaterDensityDer(s,z1,n1,DSto)
              orb0(i)=orb0(i)+cao*DSto(0)
              orb1(i)=orb1(i)+cao*DSto(1)
              orb2(i)=orb2(i)+cao*DSto(2)
              s=s+ds
            enddo
          enddo
          pom=2.*pop(iao)
          do i=1,240
            ffp(i)=ffp(i)+pom*orb0(i)*orb1(i)
          enddo
          do i=241,480
            j=i-240
            ffp(i)=ffp(i)+pom*(orb1(j)**2+orb2(j)*orb0(j))
          enddo
3500    continue
      endif
      go to 9999
9000  ich=1
      ErrFlag=0
      go to 9999
9100  ich=2
9999  call CloseIfOpened(ln)
      return
      end
