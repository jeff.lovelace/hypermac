      subroutine FromCifShelxJana
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*256 Veta
      character*80  t80
      integer ih(3)
      logical EqIgCase,EqIV0,RefineEnd
      Veta=' '
      call FeFileManager('Define input CIF file',Veta,'*.cif',0,
     1                   .false.,ich)
      if(ich.ne.0.or.Veta.eq.' ') go to 9999
      lni=NextLogicNumber()
      call OpenFile(lni,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      nHKL=0
      nRES=0
      nn=0
      lno=NextLogicNumber()
1100  read(lni,FormA,end=9000) Veta
      if(EqIgCase(Veta,'_shelx_res_file')) then
        nn=nn+1
        nRES=nRES+1
        write(Cislo,'(i5)') nRES
        call Zhusti(Cislo)
        Veta='zkri-'//Cislo(:idel(Cislo))//'.res'
        call OpenFile(lno,Veta,'formatted','unknown')
1200    read(lni,FormA,end=9000) Veta
        k=0
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'TITL')) go to 1200
        backspace lni
1300    read(lni,FormA,end=9000) Veta
        k=0
        call kus(Veta,k,t80)
        if(t80(1:1).eq.';') go to 1400
        write(lno,FormA) Veta(:idel(Veta))
        go to 1300
1400    call CloseIfOpened(lno)
      endif
      if(EqIgCase(Veta,'_shelx_hkl_file')) then
        nn=nn+1000
        nHKL=nHKL+1
        write(Cislo,'(i5)') nHKL
        call Zhusti(Cislo)
        Veta='zkri-'//Cislo(:idel(Cislo))//'.hkl'
        call OpenFile(lno,Veta,'formatted','unknown')
2200    read(lni,FormA,end=9000) Veta
        if(Veta(1:1).eq.';'.or.Veta.eq.' ') go to 2200
2300    read(lni,FormA,end=9000) Veta
        if(Veta.eq.' ') go to 2300
        k=0
        call kus(Veta,k,t80)
        if(t80(1:1).eq.';') go to 2400
        read(Veta,'(3i4)',err=2400) ih
        if(EqIV0(ih,3)) go to 2400
        write(lno,FormA) Veta(:idel(Veta))
        go to 2300
2400    call CloseIfOpened(lno)
      endif
      if(nn.eq.1001) then
        write(Cislo,'(i5)') nRES
        call Zhusti(Cislo)
        Fln='zkri-'//Cislo(:idel(Cislo))
        iFln=idel(Fln)
        call DeleteFile(Fln(:iFln)//'.m40')
        call DeleteFile(Fln(:iFln)//'.m50')
        call DeleteFile(Fln(:iFln)//'.m90')
        call DeleteFile(Fln(:iFln)//'.m95')
        ExistMFile=.false.
        Veta=Fln(:iFln)//'.res'
        SilentRun=.true.
        call ReadSpecifiedSHELX(Veta,ich)
        SilentRun=.false.
        call RefOpenCommands
        lnc=NextLogicNumber()
        open(lnc,file=fln(:ifln)//'_fixed.tmp')
        lnn=NextLogicNumber()
        open(lnn,file=fln(:ifln)//'_fixed_new.tmp')
3200    read(lnc,FormA,end=3220) Veta
        write(lnn,FormA) Veta(:idel(Veta))
        go to 3200
3220    call CloseIfOpened(lnc)
        Veta='fixed all *'
        write(lnn,FormA) Veta(:idel(Veta))
        close(lnn)
        call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                fln(:ifln)//'_fixed.tmp')
        NacetlInt(nCmdncykl)=1
        NacetlReal(nCmdtlum)=1.
        call RefRewriteCommands(1)
        IgnoreW=.true.
        IgnoreE=.true.
        call RefDeallocate(.false.,0)
        call Refine(0,RefineEnd)
        if(ErrFlag.eq.0) then
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CopyFile(fln(:ifln)//'.m50',PreviousM50)
          call CopyFile(fln(:ifln)//'.m40',PreviousM40)
        endif
        call DeleteAllFiles('*.l*')
        nn=0
      endif
      go to 1100
9000  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
9999  return
      end
