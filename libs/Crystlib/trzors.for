      subroutine trzors(x,ux,uy,sx,sux,suy,n,kfx,kmodx,kmodxo,orm,orsel,
     1                 ord)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(n),ux(n,*),uy(n,*),sx(n),sux(n,*),suy(n,*),
     1          orm(*),orx(:),orxo(:),sorx(:),sorxo(:)
      integer orsel(*),ord,OrSelLast
      allocatable orx,orxo,sorx,sorxo
      allocate(orx(ord),orxo(ord),sorx(ord),sorxo(ord))
      kmodxo=kmodx
      if(kfx.eq.0) then
        jk=2*kmodx+1
      else
        jk=2*kmodx-1
      endif
      if(jk.le.1) go to 9999
      call SetRealArrayTo( orxo,ord,0.)
      call SetRealArrayTo(sorxo,ord,0.)
      do i=1,n
         orxo(1)= x(i)
        sorxo(1)=sx(i)
        m=1
        do j=1,kmodx
          m=m+1
           orxo(m)= ux(i,j)
          sorxo(m)=sux(i,j)
          m=m+1
           orxo(m)= uy(i,j)
          sorxo(m)=suy(i,j)
        enddo
        call MultM ( orxo,orm, orx,1,ord,ord)
        call MultMQ(sorxo,orm,sorx,1,ord,ord)
        x(i)=orx(1)
        m=1
        OrSelLast=0
        do j=2,jk
          m=m+1
          do k=OrSelLast+1,OrSel(j)
            if(k.eq.OrSel(j)) then
               pom= orx(m)
              spom=sorx(m)
            else
               pom=0.
              spom=0.
            endif
            kk=(k+1)/2
            if(mod(k,2).eq.1) then
               ux(i,kk)= pom
              sux(i,kk)=spom
               uy(i,kk)=0.
              suy(i,kk)=0.
            else
              uy(i,kk)=pom
            endif
          enddo
          OrSelLast=OrSel(j)
        enddo
        if(kfx.ne.0) then
          kk=kk+1
          ux(i,kk)=orx(jk+1)
          uy(i,kk)=orx(jk+2)
        endif
      enddo
      kmodx=(OrSelLast+1)/2
      if(kfx.ne.0) kmodx=kmodx+1
9999  deallocate(orx,orxo,sorx,sorxo)
      return
      end
