      subroutine MakeBetaModPol(bd,x40,tzero,nt,dt,bx,by,kmod,x4s,delta,
     1                          Gamma,Type)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension bd(6,*),bx(6,*),by(6,*),x40(3),tzero(3),nt(3),dt(3),
     1          x4i(3),x4(3),nd(3),Gamma(9),d4(3),FPolA(2*mxw+1)
      integer Type
      if(InvMag(KPhase).gt.0) then
        kmg=2
      else
        kmg=1
      endif
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        call SetRealArrayTo(bd(1,i),6,0.)
        kk=0
        km=0
        do k=1,kmod
          if(mod(k,kmg).gt.0) cycle
          if(k.eq.kmg) then
            pom=x4(1)-x4s
            if(kmg.gt.1) pom=pom+x4(1)
            j=pom
            if(pom.lt.0.) j=j-1
            pom=pom-float(j)
            if(pom.gt..5) pom=pom-1.
            pom=pom/delta
            call GetFPol(pom,FPolA,2*kmod+1,Type)
          endif
          kk=kk+2
          km=km+kmg
          do l=1,6
            bd(l,i)=bd(l,i)+bx(l,k)*FPolA(kk)+by(l,k)*FPolA(kk+1)
          enddo
        enddo
      enddo
      return
      end
