      subroutine CrlMatEqShow(xm,ym,ild,RMat,Decimal,LeftVar,RightVar,
     1                        nd)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RMat(*),XSvis(6)
      character*80 Veta
      character*10 FormR
      character*(*) LeftVar(*),RightVar(*)
      integer :: id=0,UseTabsIn,Decimal
      logical WizardModeIn
      save UseTabsIn,UseTabsP,LastActiveQuestSave
      XPosOld=XPos
      YPosOld=YPos
      WizardModeIn=WizardMode
      WizardMode=.false.
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      UseTabsP=UseTabs
      dpom=40.
      xdl=0.
      xdr=0.
      do i=1,nd
        xdl=max(xdl,FeTxLength(LeftVar(i))+20.)
        xdr=max(xdl,FeTxLength(RightVar(i))+20.)
      enddo
      if(Decimal.le.0) then
        xdc=FeTxLength('-0')+20.
      else
        Cislo='-0.'
        do i=1,Decimal
          Cislo(idel(Cislo)+1:)='0'
        enddo
        xdc=FeTxLength(Cislo)+20.
        write(FormR,'(''(f15.'',i2,'')'')') Decimal
      endif
      XSvis(1)=10.
      XSvis(2)=XSvis(1)+xdl
      call FeTabsAdd(XSvis(2)-10.,UseTabs,IdLeftTab,' ')
      xpom=FeTxLength('=')+20.
      XSvis(3)=XSvis(2)+xpom
      xpom=XSvis(3)-10.
      do i=1,nd
        xpom=xpom+xdc
        call FeTabsAdd(xpom,UseTabs,IdLeftTab,' ')
      enddo
      XSvis(4)=XSvis(3)+float(nd)*xdc
      xpom=FeTxLength('*')+20.
      XSvis(5)=XSvis(4)+xpom
      XSvis(6)=XSvis(5)+xdr
      call FeTabsAdd(XSvis(6)-10.,UseTabs,IdLeftTab,' ')
      xqd=XSvis(6)+10.
      id=NextQuestId()
      call FeQuestCreate(id,xm,ym,xqd,ild,' ',0,0,-1,-1)
      k=0
      il=1
      do i=1,nd
        Veta=Tabulator//LeftVar(i)
        do j=1,nd
          k=k+1
          if(Decimal.le.0) then
            call ToFract(RMat(k),Cislo,15)
          else
            write(Cislo,FormR) RMat(k)
          endif
          Veta=Veta(:idel(Veta))//Tabulator//Cislo(:idel(Cislo))
        enddo
        Veta=Veta(:idel(Veta))//Tabulator//RightVar(i)
        call FeQuestLblMake(id,0.,il,Veta,'L','N')
        il=il+1
      enddo
      il=1
      call FeQuestSvisliceFromToMake(id,xsvis(1),il,il+nd-1,1)
      call FeQuestSvisliceFromToMake(id,xsvis(2),il,il+nd-1,1)
      nSvis1=SvisliceLastMade
      call FeQuestSvisliceFromToMake(id,xsvis(3),il,il+nd-1,1)
      nSvis2=SvisliceLastMade
      xpom=(SvisliceXPosQuest(nSvis1)+SvisliceXPosQuest(nSvis2))*.5
      ypom=(SvisliceYMinQuest(nSvis1)+SvisliceYMaxQuest(nSvis1))*.5
      call FeQuestAbsLblMake(id,xpom,ypom,'=','C','N')
      call FeQuestSvisliceFromToMake(id,xsvis(4),il,il+nd-1,1)
      nSvis1=SvisliceLastMade
      call FeQuestSvisliceFromToMake(id,xsvis(5),il,il+nd-1,1)
      nSvis2=SvisliceLastMade
      xpom=(SvisliceXPosQuest(nSvis1)+SvisliceXPosQuest(nSvis2))*.5
      ypom=(SvisliceYMinQuest(nSvis1)+SvisliceYMaxQuest(nSvis1))*.5
      call FeQuestAbsLblMake(id,xpom,ypom,'*','C','N')
      call FeQuestSvisliceFromToMake(id,xsvis(6),il,il+nd-1,1)
      call FeFrToRefresh(QuestCalledFrom(id))
      LastActiveQuest=QuestCalledFrom(id)
      LastActiveQuestSave=LastActiveQuest
      go to 9000
      entry CrlMatEqHide
      if(id.eq.0) go to 9999
      XPosOld=XPos
      YPosOld=YPos
      LastActiveQuest=id
      UseTabs=UseTabsP
      WizardModeIn=WizardMode
      WizardMode=.false.
      call FeFrToRefresh(id)
      call FeQuestActiveObjOn
      call FeQuestRemove(id)
      LastActiveQuest=LastActiveQuestSave
      call FeTabsReset(UseTabs)
9000  UseTabs=UseTabsIn
      WizardMode=WizardModeIn
      call FeMoveMouseTo(XPosOld,YPosOld)
      go to 9999
      entry CrlMatEqReset
      id=0
9999  return
      end
