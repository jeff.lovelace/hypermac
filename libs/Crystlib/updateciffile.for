      subroutine UpDateCifFile
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 CifKeyLabel(:),Radka,Veta
      integer CifKeyLabelLength(:)
      logical loop,EqIgCase,FeYesNo,Change
      allocatable CifKeyLabel,CifKeyLabelLength
      allocate(CifKey(400,40),CifKeyLabel(40),CifKeyFlag(400,40),
     1         CifKeyLabelLength(40))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.ne.0) go to 9999
      NTrida=0
      Change=.false.
      do i=1,40
        Veta=' '
        do 1400j=1,400
          if(CifKey(j,i).eq.' ') go to 1400
          if(Veta.eq.' ') then
            Veta=CifKey(j,i)
            go to 1400
          endif
          id=0
          do k=1,idel(Veta)
            if(CifKey(j,i)(k:k).eq.Veta(k:k)) id=id+1
          enddo
          CifKeyLabelLength(i)=id
          Veta=Veta(:id)
1400    continue
        if(Veta.ne.' ') NTrida=i
        CifKeyLabel(i)=Veta
      enddo
      call FeFileManager('Define input file',Radka,'*.dic',0,
     1                   .false.,ich)
      if(ich.ne.0.or.Radka.eq.' ') go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,Radka,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
2000  read(ln,FormA,end=5000) Radka
      k=0
      call kus(Radka,k,Veta)
2100  if(EqIgCase(Veta,'loop_')) then
        call kus(Radka,k,Veta)
        loop=.true.
      else
        loop=.false.
      endif
      if(.not.EqIgCase(Veta,'_name')) go to 2000
      call kus(Radka,k,Veta)
2200  id=idel(Veta)-1
      if(Veta(id:id).eq.']') go to 2000
      Veta=Veta(2:id)
      do 2300i=1,NTrida
        j=CifKeyLabelLength(i)
        if(j.le.0) go to 2300
        if(index(Veta,CifKeyLabel(i)(:j)).eq.1)
     1    go to 2500
2300  continue
      NTrida=NTrida+1
      i=index(Veta(2:),'_')+1
      CifKeyLabel(NTrida)=Veta(:i)
      CifKeyLabelLength(NTrida)=idel(CifKeyLabel(NTrida))
      i=NTrida
      NInfo=1
      write(TextInfo(1),FormA)
     1  CifKeyLabel(NTrida)(:CifKeyLabelLength(NTrida))
      call FeInfoOut(-1.,-1.,'NEW CLASS','L')
2500  kam=999
      do 2600j=1,400
        if(CifKey(j,i).eq.' ') then
          if(kam.gt.900) kam=j
          go to 2600
        endif
        if(EqIgCase(Veta,CifKey(j,i))) go to 3000
2600  continue
      Change=.true.
      call FeChybne(-1.,-1.,Veta,'NEW KEYWORD',WarningWithESC)
      CifKey(kam,i)=Veta
3000  if(loop) then
        read(ln,FormA,end=5000) Radka
        k=0
        call kus(Radka,k,Veta)
        if(Veta(1:1).eq.'_'.or.Veta(1:1).eq.'loop_') then
          go to 2100
        else
          go to 2200
        endif
      endif
      go to 2000
5000  call CloseIfOpened(ln)
      if(.not.Change) then
        call FeMsgOut(-1.,-1.,'There is no new item')
        go to 9999
      endif
      call OpenFile(ln,'cif_new.dat','formatted','unknown')
      Radka='### '//VersionString(12:idel(VersionString))//' ###'
      write(ln,FormA) Radka(:idel(Radka))
      do i=1,NTrida
        do 5100j=1,400
          if(CifKey(j,i).eq.' ') go to 5100
          write(ln,'(2i5,2x,80a1)')
     1      j,i,(CifKey(j,i)(k:k),k=1,idel(CifKey(j,i)))
5100    continue
      enddo
      call CloseIfOpened(ln)
      if(FeYesNo(-1.,-1.,'Do you want to rewrite old files?',1)) then
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'cif'//ObrLom//'cif.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'cif/cif.dat'
        endif
        call MoveFile('cif_new.dat',Veta)
      endif
9999  deallocate(CifKey,CifKeyLabel,CifKeyFlag,CifKeyLabelLength)
      return
      end
