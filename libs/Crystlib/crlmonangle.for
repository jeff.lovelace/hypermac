      function CrlMonAngle(cp,ih,Lambda)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension cp(6),cpp(6),rcpp(6)
      integer ih(3)
      real Lambda
      do i=1,3
        cpp(i)=cp(i)
      enddo
      do i=4,6
        cpp(i)=cos(cp(i)*ToRad)
      enddo
      call recip(cpp,rcpp,pom)
      pom=0.
      do i=1,3
        pom=pom+(float(ih(i))*rcpp(i))**2
      enddo
      do i=1,3
        do j=i+1,3
          k=9-i-j
          pom=pom+2.*float(ih(i))*rcpp(i)*
     1               float(ih(j))*rcpp(j)*rcpp(k)
        enddo
      enddo
      pom=sqrt(pom)*.5*Lambda
      if(abs(pom).lt..999) then
        CrlMonAngle=asin(pom)/ToRad
      else
        CrlMonAngle=15.
      endif
      return
      end
