      function LocateAtom(AtomArray,n,AtomString,icrim)
      character*(*) AtomArray(*),AtomString
      logical EqIgCase
      LocateAtom=0
      icrim=index(AtomString,'#')
      if(icrim.le.0) then
        i=idel(AtomString)
      else
        i=icrim-1
      endif
      do j=1,n
        if(EqIgCase(AtomArray(j),AtomString(:i))) then
          LocateAtom=j
          exit
        endif
      enddo
      return
      end
