      subroutine iost(ln,a,ki,n,klic,tisk)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(*),ki(*)
      character*15 format1
      character*25 format2
      character*80 FormatOut
      integer tisk,Exponent10
      data format1/'( f9.6,  x, i1)'/
      data format2/'( f9.6,  x,''=>'', i1,''<='')'/
      ErrFlag=0
      format1(2:2)='6'
      format1(12:12)='6'
      format1(8:9)=' 6'
      format2(2:2)='6'
      format2(17:17)='6'
      format2(8:9)=' 3'
      nc=n/6
      nd=n-6*nc
      nc=nc+1
      i2=0
      if(klic.eq.0) then
        do ic=1,nc
          if(ic.ne.nc) then
            i1=i2+1
            i2=i2+6
          else
            if(nd.eq.0) cycle
            i1=i2+1
            i2=i2+nd
            write(format1(2:2),100) nd
            write(format1(12:12),100) nd
            write(format2(2:2),100) nd
            write(format2(17:17),100) nd
            write(format1(8:9),101) 6+(6-nd)*9
            write(format2(8:9),101) 3+(6-nd)*9
          endif
          read(ln,format1,err=9000,end=9000)(a(i),i=i1,i2),
     1      (ki(i),i=i1,i2)
          if(tisk.eq.1) then
            call newln(1)
            write(lst,format2)(a(i),i=i1,i2),(max(ki(i),0),i=i1,i2)
          endif
        enddo
      else
        do ic=1,nc
          FormatOut='('
          k=2
          if(ic.ne.nc) then
            i1=i2+1
            i2=i2+6
            nn=6
          else
            if(nd.eq.0) cycle
            i1=i2+1
            i2=i2+nd
            nn=nd
          endif
          do i=i1,i2
            if(abs(a(i)).gt.0.) then
              j=max(6-Exponent10(a(i)),0)
              j=min(j,6)
            else
              j=6
            endif
            write(FormatOut(k:),'(''f9.'',i1,'','')') j
            k=k+5
          enddo
          write(FormatOut(k:),'(i2,''x,'')') 60-9*nn
          k=k+4
          FormatOut(k:)='6i1)'
          write(ln,FormatOut)(a(i),i=i1,i2),(max(ki(i),0),i=i1,i2)
        enddo
      endif
      go to 9999
9000  ErrFlag=1
9999  return
100   format(i1)
101   format(i2)
      end
