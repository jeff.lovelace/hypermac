      subroutine CrlImportFromELMAMCheck
      use Atoms_mod
      use Basic_mod
      use ELMAM_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer SbwItemPointerQuest
      character*80 AtP
      logical EqIgCase
      if(nDB.gt.0) then
        iDB=SbwItemPointerQuest(nSbwSelect)
        if(iDB.ne.iDBOld) then
          call FeQuestLblChange(nLblLocSystem,DBLocSyst(iDB))
          AtLocSyst=' '
          k=0
          do j=1,2
            do i=1,NNeigh
              m=AtNeighN(i)
              if(EqIgCase(AtType(isf(m+NAtOffset),KPhase),
     1                    DBLocSystAtType(j,iDB))) then
                if(j.eq.2.and.EqIgCase(AtLocSyst(1),AtNeigh(i))) cycle
                k=k+1
                AtLocSyst(j)=AtNeigh(i)
                exit
              endif
            enddo
          enddo
          if(k.eq.1) then
            call DistFromAtom(Atom(NAtOffset+AtNeighN(1)),3.,iswELMAM)
            do i=1,NDist
              k=ipord(i)
              ian=ktat(Atom(NAtOffset+1),NAtActive,ADist(k))
              if(ian.le.0) cycle
              if(.not.EqIgCase(AtType(isf(NAtOffset+ian),KPhase),
     1                         DBLocSystAtType(2,iDB))) cycle
              AtP=ADist(k)
              j=idel(SymCodeJanaDist(k))
              if(j.gt.0) AtP=AtP(:idel(AtP))//'#'//
     1                       SymCodeJanaDist(k)(:j)
              if(EqIgCase(AtP,Atom(NAtOffset+NAtSel)).or.
     1           EqIgCase(AtP,AtLocSyst(1))) then
                cycle
              else
                AtLocSyst(2)=AtP
                exit
              endif
            enddo
          endif
          iDBOld=iDB
        endif
        do i=1,2
         call FeQuestLblChange(nLblAtLocSyst(i),AtLocSyst(i))
        enddo
      else
        iDBOld=0
      endif
      return
      end
