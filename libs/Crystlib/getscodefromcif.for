      subroutine GetScodeFromCIF(CIFCode,isw,SymmCode)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(3),it(3)
      character*(*) CIFCode,SymmCode
      character*80 Veta
      if(CIFCode.eq.'.') then
        SymmCode=' '
        go to 9999
      endif
      i=index(CIFCode,'_')
      read(CIFCode(:i-1),*,err=9000) isymm
      icenter=(isymm-1)/NSymm(KPhase)+1
      isymm=mod(isymm-1,NSymm(KPhase))+1
      k=1
      do j=i+1,i+3
        read(CIFCode(j:j),*) it(k)
        it(k)=it(k)-5
        k=k+1
      enddo
      do i=1,3
        xp(i)=vt6(i,icenter,isw,KPhase)+s6(i,icenter,isw,KPhase)+
     1        float(it(i))
      enddo
      call SCode(isymm,icenter,xp,it,isw,SymmCode,Veta)
      go to 9999
9000  SymmCode='???'
9999  return
      end
