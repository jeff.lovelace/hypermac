      subroutine NactiCommon(ln,izpet)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*85 ven
      character*80 NactiVetaInt,t80
      character*10  nazev
      logical first,EqIgCase
      data first/.true./
      if(first) then
        if(LstOpened) then
          call newln(1)
          write(lst,'(''The following lines were read as a control '',
     1                ''data : '')')
        endif
        first=.false.
      endif
      if(izpet.ge.0) then
        PosledniPozice=80
      else
        PosledniPozice=min(PosledniPozice,80)
      endif
1000  if(PosledniPozice.eq.80) then
1010    read(ln,FormA,err=9100,end=9200) NactiVetaInt
        t80=NactiVetaInt
        call DeleteFirstSpaces(t80)
        k=0
        call kus(t80,k,Cislo)
        if(LstOpened.and..not.EqIgCase(Cislo,'end')) then
          ven='=>'
          Ven(3:)=t80
          ven(idel(Ven)+1:)='<='
          call newln(1)
          write(lst,FormA) ven
        endif
        if(NactiVetaInt.eq.' ') go to 1010
        ip=1
1015    if(NactiVetaInt(ip:ip).eq.' ') then
          ip=ip+1
          go to 1015
        endif
        if(NactiVetaInt(ip:ip).eq.'*'.or.NactiVetaInt(ip:ip).eq.'#')
     1    go to 1010
        if(NactiVetaInt(ip:ip).eq.'!') then
          if(PreskocVykricnik) then
1020        read(ln,FormA,err=9100,end=9200) NactiVetaInt
            call Zhusti(NactiVetaInt)
            if(NactiVetaInt(1:1).eq.'&') go to 1020
            backspace ln
            go to 1010
          else
            NactiVetaInt=NactiVetaInt(ip+1:)
            NacetlVykricnik=.true.
          endif
        else
          NactiVetaInt=NactiVetaInt(ip:)
          NacetlVykricnik=.false.
        endif
        call vykric(NactiVetaInt)
        if(idel(NactiVetaInt).le.0) go to 1010
        PosledniPozice=0
      endif
      call kus(NactiVetaInt,PosledniPozice,Nazev)
      if(EqIgCase(Nazev,'end')) then
        izpet=0
        first=.true.
        return
      endif
      i=islovo(Nazev,NactiKeywords,NactiKeys)
      if(i.eq.0) then
        call FeChybne(-1.,-1.,'unknown keyword : '//nazev,' ',
     1                WarningWithESC)
      else if(i.lt.0) then
        call FeChybne(-1.,-1.,'keyword : '//nazev//' isn''t unique',' ',
     1                WarningWithESC)
      else if(NactiRepeat(i).gt.0) then
        call FeChybne(-1.,-1.,'duplicate occurrence of "'//
     1                nazev(:idel(nazev))//
     2                '"','remember that the first occurrence will be '
     3              //'accepted',WarningWithESC)
        call kus(NactiVetaInt,PosledniPozice,cislo)
      else
        go to 2000
      endif
      if(ErrFlag.ne.0) then
        go to 9999
      else
        go to 1000
      endif
2000  if(NactiRepeat(i).ge.0) NactiRepeat(i)=NactiRepeat(i)+1
      if(i.le.NactiInt+NactiReal) then
        call kus(NactiVetaInt,PosledniPozice,cislo)
        if(i.le.NactiInt) then
          call posun(cislo,0)
          read(cislo,FormI15,err=9000) NacetlInt(i)
        else
          call posun(cislo,1)
          read(cislo,'(f15.0)',err=9000) NacetlReal(i)
        endif
      else
        NactiVeta=NactiVetaInt
3000    read(ln,FormA,err=9100,end=9200) t80
        if(t80.eq.' ') go to 3000
        j=1
3030    if(t80(j:j).eq.' ') then
          j=j+1
          go to 3030
        endif
        k=idel(NactiVeta)
        if(t80(j:j).eq.'&'.or.NactiVeta(k:k).eq.'+'.or.
     1     NactiVeta(k:k).eq.'-') then
          if(NactiVeta(k:k).eq.'+'.and.NactiVeta(j:j).eq.'+') then
            j=j+1
          else if(NactiVeta(k:k).eq.'-'.and.NactiVeta(j:j).eq.'+')
     1      then
            j=j+1
          else if(NactiVeta(k:k).eq.'+'.and.NactiVeta(j:j).eq.'-')
     1      then
            k=k-1
          else if(NactiVeta(k:k).eq.'-'.and.NactiVeta(j:j).eq.'-')
     1      then
            t80(j:j)='+'
            k=k-1
          else if(t80(j:j).eq.'&') then
            t80(j:j)=' '
            j=j+1
          endif
          NactiVeta=NactiVeta(:k)//t80(j:idel(t80))
          go to 3000
        else
          backspace ln
        endif
        izpet=i
        go to 9999
      endif
      go to 1000
9000  call ChybaNacteni(i)
      go to 1000
9100  call FeChybne(-1.,-1.,'occurred during reading of keywords.',' ',
     1              Warning)
      izpet=0
      go to 9999
9200  call FeChybne(-1.,-1.,'EOF reached during reading of keywords.',
     1              ' ',Warning)
      izpet=0
9999  return
      end
