      subroutine IOSavedPoints(Klic,PureFileName)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) PureFileName
      character*80 Veta
      integer Pise,ln(2)
      logical Podtrzeno
      call SetIntArrayTo(ln,2,0)
      ln(2)=0
      if(.not.ExistM40) go to 9999
      if(ParentStructure.and.NDim(KPhase).gt.0) then
        Veta=PureFileName(:idel(PureFileName))//'.m44'
      else
        Veta=PureFileName(:idel(PureFileName))//'.m40'
      endif
      call OpenFile(m40,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      if(Klic.eq.0) then
1100    read(m40,FormA,end=1200) Veta
        if(Veta(1:10).ne.'----------'.or.
     1     LocateSubstring(Veta,'Saved points',.false.,.true.).le.0)
     2       go to 1100
        read(m40,100) n
        if(n.gt.0) then
          call ReallocateSavedPoints(n)
          do i=1,n
            read(m40,101,err=1200,end=1200)
     1        StSavedPoint(i),(XSavedPoint(j,i),j=1,3)
          enddo
          NSavedPoints=n
        endif
        go to 9999
1200    NSavedPoints=0
        go to 9999
      else
        ln(1)=NextLogicNumber()
        call OpenFile(ln(1),PureFileName(:idel(PureFileName))//
     1                '_m40.tmp','formatted','unknown')
        ln(2)=NextLogicNumber()
        call OpenFile(ln(2),PureFileName(:idel(PureFileName))//
     1                '_peaks.tmp','formatted',
     1                'unknown')
        if(ErrFlag.ne.0) go to 9999
        Pise=1
2100    read(m40,FormA,end=2300) Veta
        if(Veta(1:10).ne.'----------') go to 2200
        if(LocateSubstring(Veta,'Saved points',.false.,.true.).gt.0)
     1    then
          Pise=0
        else if(LocateSubstring(Veta,'Fourier maxima',.false.,.true.)
     1          .gt.0.or.
     2          LocateSubstring(Veta,'Fourier minima',.false.,.true.)
     3          .gt.0) then
          Pise=2
        endif
2200    if(Pise.ne.0) then
          write(ln(Pise),FormA) Veta(:idel(Veta))
        endif
        go to 2100
2300    write(ln(1),102)
        write(ln(1),100) NSavedPoints
        do i=1,NSavedPoints
          write(ln(1),101)
     1      StSavedPoint(i),(XSavedPoint(j,i),j=1,3)
        enddo
        Podtrzeno=.false.
        rewind ln(2)
2400    read(ln(2),FormA,end=2500) Veta
        write(ln(1),FormA) Veta(:idel(Veta))
        Podtrzeno=Veta(1:10).eq.'----------'
        go to 2400
2500    if(.not.Podtrzeno) write(ln(1),'(72(''-''))')
        do i=1,2
          call CloseIfOpened(ln(i))
        enddo
        call CloseIfOpened(m40)
        if(ParentStructure.and.NDim(KPhase).gt.0) then
          Veta=PureFileName(:idel(PureFileName))//'.m44'
        else
          Veta=PureFileName(:idel(PureFileName))//'.m40'
        endif
        call MoveFile(PureFileName(:idel(PureFileName))//'_m40.tmp',
     1                Veta)
        call DeleteFile(PureFileName(:idel(PureFileName))//'_peaks.tmp')
      endif
9999  call CloseIfOpened(m40)
      do i=1,2
        call CloseIfOpened(ln(i))
      enddo
      return
100   format(i5)
101   format(a8,19x,3f9.6)
102   format(26('-'),3x,'Saved points',3x,28('-'))
      end
