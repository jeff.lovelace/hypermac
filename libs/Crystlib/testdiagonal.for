      logical function TestDiagonal(rmp,n)
      include 'fepc.cmn'
      dimension rmp(n,n)
      TestDiagonal=.true.
      do i=1,3
        do j=1,3
          if(i.ne.j.and.abs(rmp(i,j)).gt..0001) then
            TestDiagonal=.false.
            go to 9999
          endif
        enddo
      enddo
9999  return
      end
