      subroutine CIFSetMod(ux,uy,sux,suy,psin,pcos,ssin,scos,pmod,pfaz,
     1                     smod,sfaz,it)
      include 'fepc.cmn'
      if(it.eq.0) then
        ux= psin
        uy= pcos
        sux= ssin
        suy= scos
      else
        psin=sin(pfaz)
        pcos=cos(pfaz)
        ux=-pmod*psin
        uy= pmod*pcos
        psin=psin**2
        pcos=pcos**2
        sfaz=sfaz**2
        smod=smod**2
        sux=sqrt(smod*psin+sfaz*pmod*pcos)
        suy=sqrt(smod*pcos+sfaz*pmod*psin)
      endif
      return
      end
