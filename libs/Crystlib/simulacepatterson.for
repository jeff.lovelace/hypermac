      subroutine SimulacePatterson
      parameter (MxPoints=1000)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension X(MxPoints),Y(MxPoints,5),xp(3),xd(3),yp(3),
     1          O(MxPoints)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      xomn=0.
      xomx=1.
c      yomn=-.1
      yomn=-1.
      yomx=1.
      dx=1./float(MxPoints)
      xp(1)=.10
      xp(2)=.45
      xp(3)=.90
      xd(1)=.02
      xd(2)=.02
      xd(3)=.02
      yp(1)=1.
      yp(2)=0.5
      yp(3)=0.2
      do i=1,MxPoints
        X(i)=dx*float(i-1)
        pom=0.
        do j=1,3
          pom=pom+yp(j)*ExpJana(-((X(i)-xp(j))/xd(j))**2,ich)
        enddo
        Y(i,1)=pom
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'X')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Y')
c      do i=1,1
c        if(i.eq.1) then
c          j=Red
c        else if(i.eq.2) then
c          j=Green
c        else if(i.eq.3) then
c          j=Yellow
c        else if(i.eq.4) then
c          j=Khaki
c        else if(i.eq.5) then
c          j=Blue
c        endif
c        call FeXYPlot(X(1),Y(1,1),MxPoints,NormalLine,NormalPlotMode,j)
c      enddo
      pom=0.
      do i=1,MxPoints
        do j=1,MxPoints
          if(j+i.le.MxPoints) then
            Y(j+i,2)=Y(j,1)
          else
            Y(j+i-MxPoints,2)=Y(j,1)
          endif
        enddo
        Y(i,3)=0.
        do j=1,MxPoints
          Y(i,3)=Y(i,3)+Y(j,1)*Y(j,2)
        enddo
        pom=max(pom,Y(i,3))
      enddo
      do i=1,MxPoints
        O(i)=0.
        Y(i,3)=Y(i,3)/pom
        Y(i,3)=Y(i,3)-1.
      enddo
      do i=1,MxPoints
        O(i)=1.
        do j=1,MxPoints
          if(j+i.le.MxPoints) then
            Y(j+i,2)=Y(j,1)
          else
            Y(j+i-MxPoints,2)=Y(j,1)
          endif
        enddo
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'X')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Y')
        call FeXYPlot(X,Y(1,1),MxPoints,NormalLine,NormalPlotMode,White)
        call FeXYPlot(X,Y(1,2),MxPoints,NormalLine,NormalPlotMode,Green)
        call GrtDrawPerPartes(X,Y(1,3),O,
     1                        MxPoints,NormalLine,NormalPlotMode,Yellow)
        call FeReleaseOutput
        call FeDeferOutput
        if(Y(i,3)+1..gt..01) then
          pom=.2
        else
          pom=.01
        endif
        call FeWait(pom)
      enddo
6000  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 6000
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
