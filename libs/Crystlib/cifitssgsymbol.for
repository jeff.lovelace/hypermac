      subroutine CIFITSSGSymbol(Symbol,ich)
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension qsymm(3)
      character*(*) Symbol
      if(index(Symbol,'?').gt.0) then
        ich=-1
        go to 9999
      endif
      j=0
      do i=1,idel(Symbol)
        if(Symbol(i:i).ne.' '.and.Symbol(i:i).ne.','.and.
     1     Symbol(i:i).ne.'\') then
          j=j+1
          Symbol(j:j)=Symbol(i:i)
        endif
      enddo
      Symbol(j+1:)=' '
      NNul=0
      KdeNul=0
      KdeNenul=0
      do i=4,6
        Cislo=lcell(i)
        k=LocateSubstring(Symbol,Cislo(:idel(Cislo)),.false.,.true.)
        if(k.gt.0) then
          KdeNenul=i-3
          m=idel(Cislo)
          Symbol(k:k+m-1)=' '
          Symbol(k:k)=Cislo(1:1)
          call zhusti(Symbol)
        else
          NNul=NNul+1
          KdeNul=i-3
        endif
      enddo
      do i=4,6
        Cislo=lcell(i)
        k=LocateSubstring(Symbol,Cislo(:idel(Cislo)),.false.,.true.)
        if(k.gt.0) then
          KdeNenul=i-3
          m=idel(Cislo)
          Symbol(k:k+m-1)=' '
          Symbol(k:k)=Cislo(1:1)
          call zhusti(Symbol)
        else
          NNul=NNul+1
              KdeNul=i-3
        endif
      enddo
1100  k=LocateSubstring(Symbol,'0.5',.false.,.true.)
      if(k.gt.0) then
        Symbol(k:k+2)='1/2'
        go to 1100
      endif
      Grupa(KPhase)=Symbol
      kp=index(Symbol,'(')+1
      kk=index(Symbol,')')-1
      NNul=0
      KdeNul=0
      KdeNenul=0
      do i=1,3
        Cislo=lcell(i+3)(1:1)
        k=LocateSubstring(Symbol(kp:kk),Cislo(:idel(Cislo)),.false.,
     1                    .true.)
        if(k.gt.0) then
          KdeNenul=i
        else
          NNul=NNul+1
          KdeNul=i
        endif
      enddo
      if(NNul.eq.2) then
        Monoclinic(KPhase)=KdeNenul
      else if(NNul.eq.1) then
        Monoclinic(KPhase)=KdeNul
      endif
      if(NPhase.le.1) then
        n=1
        call AllocateSymm(n+1,n+1,1,NPhase)
      endif
      call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
      if(ich.eq.0) call EM50MakeStandardOrder
9999  return
      end
