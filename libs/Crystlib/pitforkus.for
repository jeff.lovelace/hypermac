      subroutine PitForKus(Klic,FormulaP,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FormulaP
      dimension pa(1)
      character*80 Clen
      character*7 AtTypeP
      logical EqIgCase
      ich=0
      idl=idel(FormulaP)
      k=0
1100  kp=k
      call Kus(FormulaP,k,Clen)
      idlcl=idel(Clen)
      n=0
      do i=1,idlcl
        if(index(Cifry(:11),Clen(i:i)).le.0) then
          n=n+1
        else
          exit
        endif
      enddo
      if(n.gt.2) then
        Clen=Clen(:2)
        k=kp+2
      endif
      write(Cislo,'(2i3)') k,n
      idlcl=idel(Clen)
      if=0
      ic=0
      do i=1,idlcl
        if(index(Cifry(:11),Clen(i:i)).gt.0) then
          if(ic.eq.0) ic=i
        else
          if(ic.gt.0) then
            Clen(i:)=' '
            k=kp+i-1
            go to 1300
          endif
          if(if.eq.0) if=i
        endif
      enddo
1300  if(if.ne.0) then
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        if(ic.gt.0) then
          AtTypeP=Clen(if:ic-1)
        else
          AtTypeP=Clen
        endif
        call UprAt(AtTypeP)
        do i=1,98
          if(EqIgCase(AtTypeP,atn(i))) go to 1360
        enddo
        if(EqIgCase(AtTypeP,'D')) then
          isfh(KPhase)=NAtFormula(KPhase)
          go to 1360
        endif
        go to 9200
1360    AtMultP=-1.
        if(EqIgCase(AtTypeP,'H').and.isfh(KPhase).eq.0)
     1    isfh(KPhase)=NAtFormula(KPhase)
        if(Klic.eq.0) then
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          AtMult(NAtFormula(KPhase),KPhase)=AtMultP
          AtType(NAtFormula(KPhase),KPhase)=AtTypeP
          AtTypeFull(NAtFormula(KPhase),KPhase)=AtTypeP
          AtTypeMag(NAtFormula(KPhase),KPhase)=' '
          AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
          call EM50ReadOneFormFactor(NAtFormula(KPhase))
          call EM50OneFormFactorSet(NAtFormula(KPhase))
          j=LocateInStringArray(atn,98,AtType(NAtFormula(KPhase),KPhase)
     1                         ,IgnoreCaseYes)
          if(j.gt.0) then
            AtNum(NAtFormula(KPhase),KPhase)=float(j)
          else
            AtNum(NAtFormula(KPhase),KPhase)=0.
          endif
        endif
      endif
      if(ic.gt.0) then
        if(if.le.0.and.AtMultP.gt.0.) go to 9000
        kk=ic-1
        call StToReal(Clen,kk,pa,1,.false.,ich)
        if(ich.ne.0) go to 9100
        AtMultP=pa(1)
        if(Klic.eq.0) AtMult(NAtFormula(KPhase),KPhase)=AtMultP
      endif
      do i=1,NAtFormula(KPhase)-1
        if(Klic.eq.0) then
          if(EqIgCase(AtType(i,KPhase),AtTypeP)) then
            NAtFormula(KPhase)=NAtFormula(KPhase)-1
            if(AtMult(i,KPhase).lt.0.) AtMult(i,KPhase)=1.
            if(AtMultP.lt.0.) then
              AtMult(i,KPhase)=AtMult(i,KPhase)+1.
            else
              AtMult(i,KPhase)=AtMult(i,KPhase)+AtMultP
            endif
          endif
        endif
      enddo
      if(k.lt.len(FormulaP)) go to 1100
      if(Klic.eq.0) then
        do i=1,NAtFormula(KPhase)
          if(AtMult(i,KPhase).lt.0.) AtMult(i,KPhase)=1.
        enddo
      endif
      go to 9999
9000  call FeChybne(-1.,-1.,'for atom "'//AtTypeP(:idel(AtTypeP))//
     1              '" in the formula "'//
     2              Formula(KPhase)(:idel(Formula(KPhase)))//'"',
     3              'there are two coefficients',SeriousError)
      go to 9900
9100  call FeChybne(-1.,-1.,'incorrect number in chemical formula '
     1            //Formula(KPhase)(:idel(Formula(KPhase)))//'"',
     2              Clen(ic:),SeriousError)
      go to 9900
9200  call FeChybne(-1.,-1.,'atom type "'//
     1              AtTypeP(:idel(AtTypeP))//
     2              '" is not on the list.',' ',SeriousError)
9900  ich=1
      NAtFormula(KPhase)=0
9999  return
      end
