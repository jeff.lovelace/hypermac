      subroutine EditAtMolSplitReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditAtMolSplitC/ nEdwAtMol,nCrwAtoms,nCrwMolecules,
     1                        AtMolString
      character*256 Veta,AtMolString,EdwStringQuest
      character*(*) Command
      logical EqIgCase
      save EditAtMolSplitC
      ich=0
      if(Command.eq.' ') go to 9999
      k=0
      call kus(Command,k,Veta)
      if(EqIgCase(Veta,'atsplit')) then
        i=1
      else
        i=2
      endif
      call FeQuestCrwOpen(nCrwAtoms,i.eq.1)
      call FeQuestCrwOpen(nCrwMolecules,i.ne.1)
      AtMolString=Command(k+1:)
      if(i.eq.1) then
        call TestAtomString(AtMolString,IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,Veta)
      else
        call TestMolString(AtMolString,IdWildNo,IdSymmYes,Veta)
      endif
      if(Veta.ne.' ') go to 8100
      go to 9999
8100  ich=1
      if(Veta.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,Veta,'Edit or delete the relevant '//
     2                'command.',SeriousError)
9999  return
      end
