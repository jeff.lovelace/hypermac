      subroutine EditSavedPoints
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditSavedPointsC/ nEdwLabel,nEdwXYZ,LabelString,xs
      dimension xs(3)
      character*80  Veta
      character*8   LabelString
      integer WhatHappened
      logical Prazdny,FileDiff,FeYesNo
      external EditSavedPointsReadCommand,
     1         EditSavedPointsWriteCommand,FeVoid
      save /EditSavedPointsC/
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_peaks.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NSavedPoints
        write(Veta,'(a8,3f10.6)') StSavedPoint(i),
     1                            (XSavedPoint(j,i),j=1,3)
        call ZdrcniCisla(Veta,4)
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      xqd=500.
      il=1
      call RepeatCommandsPrologWithoutEnable(id,
     1                       fln(:ifln)//'_peaks.tmp',xqd,il,ilp,0)
      il=ilp+1
      Veta='%Label:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLabel=EdwLastMade

      tpom=xpom+dpom+20.
      Veta='F%ractional coordinates:'
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwXYZ=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
1300  LabelString=' '
      call SetRealArrayTo(xs,3,0.)
      Prazdny=.true.
1350  call FeQuestStringEdwOpen(nEdwLabel,LabelString)
      call FeQuestRealAEdwOpen(nEdwXYZ,xs,3,Prazdny,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  EditSavedPointsReadCommand,EditSavedPointsWriteCommand,FeVoid,
     2  FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        Prazdny=.false.
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_peaks.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NSavedPoints=0
2500  read(ln,FormA,end=2600) Veta
      if(NSavedPoints.ge.MaxSavedPoints)
     1  call ReallocateSavedPoints(MaxSavedPoints+10)
      NSavedPoints=NSavedPoints+1
      k=0
      call kus(Veta,k,StSavedPoint(NSavedPoints))
      call StToReal(Veta,k,XSavedPoint(1,NSavedPoints),3,.false.,ich)
      go to 2500
2600  call CloseIfOpened(ln)
      call iom40(1,0,fln(:ifln)//'.m40')
      if(FileDiff(PreviousM40,fln(:ifln)//'.m40')) then
        if(FeYesNo(-1.,-1.,'Do you want to rewrite saved points?',
     1             1)) then
          call DeleteFile(PreviousM40)
          go to 9999
        else
          call CopyFile(PreviousM40,fln(:ifln)//'.m40')
          call DeleteFile(PreviousM40)
        endif
      endif
9999  return
      end
