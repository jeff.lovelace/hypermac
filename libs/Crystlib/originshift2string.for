      subroutine OriginShift2String(Shift,n,String)
      dimension Shift(n)
      character*(*) String
      character*15  t15
      String='('
      do i=1,n
        call ToFract(Shift(i),t15,24)
        String=String(:idel(String))//t15(:idel(t15))
        if(i.eq.n) then
          t15=')'
        else
          t15=','
        endif
        String=String(:idel(String))//t15(:idel(t15))
      enddo
      return
      end
