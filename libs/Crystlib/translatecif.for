      subroutine TranslateCIF
      use Basic_mod
      include 'fepc.cmn'
      dimension ip(2)
      character*256 EdwStringQuest
      character*80  Veta
      logical EqIgCase
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.ne.0) go to 9999
      id=NextQuestId()
      xqd=450.
      il=3
      Veta='Translate CIF word to Jana code or vice versa'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,-1)
      il=1
      Veta='Enter CIR word or Jana code:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdw=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      tpom=(xqd-ButYd)*.5
      call FeQuestButtonMake(id,tpom,il,ButYd,ButYd,'#')
      nButt=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,' ','L','N')
      nLbl=LblLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButt) then
        if(EdwStringQuest(nEdw).eq.' ') then
          Veta=' '
        else
          Veta=EdwStringQuest(nEdw)
          k=0
          call StToInt(Veta,k,ip,2,.false.,ichp)
          if(ichp.eq.0) then
            Veta=CifKey(ip(1),ip(2))
            Veta='CIF code: '//Veta(:idel(Veta))
          else
            do i=1,400
              do j=1,40
                if(EqIgCase(Veta,CifKey(i,j))) go to 2200
              enddo
            enddo
            Veta='String not found'
            go to 2500
2200        write(Veta,'(''Jana code:'',2i4)') i,j
          endif
        endif
2500    call FeQuestLblChange(nLbl,Veta)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
9999  if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      return
      end
