      subroutine MakeBetaMod(bd,x40,tzero,nt,dt,bx,by,kmod,kf,Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension bd(6,*),bx(6,*),by(6,*),x40(3),tzero(3),nt(3),dt(3),
     1          x4i(3),x4(3),nd(3),Gamma(9),d4(3)
      if(kf.ne.0) then
        x4s=by(1,kmod)
        delta=by(2,kmod)
      endif
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        call SetRealArrayTo(bd(1,i),6,0.)
        do k=1,kmod
          arg=0.
          do j=1,NDimI(KPhase)
            arg=arg+x4(j)*float(kw(j,k,KPhase))
          enddo
          arg=pi2*arg
          if(kf.eq.0.or.k.ne.kmod) then
            do l=1,6
              bd(l,i)=bd(l,i)+bx(l,k)*sin(arg)+by(l,k)*cos(arg)
            enddo
          else
            x4p=x4(1)-x4s
            ix4p=x4p
            if(x4p.lt.0.) ix4p=ix4p-1
            x4p=x4p-float(ix4p)
            if(x4p.gt..5) x4p=x4p-1.
            if(x4p.ge.-delta/2..and.x4p.le.delta/2.) then
              znak= 1.
            else
              znak=-1.
            endif
            do l=1,6
              bd(l,i)=bd(l,i)+znak*bx(l,k)
            enddo
          endif
        enddo
      enddo
      return
      end
