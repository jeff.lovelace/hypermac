      subroutine GetSymIndRespectTwin(ihi,iho,itr,kitw,isym,nscp,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ihi(*),iho(*),itr(*),ihp(6),hp(6),hpp(6),hsp(6),hs1(3),
     1          hs2(3),difi(3),ih(6),xp(6)
      logical eqrv
      data difi/3*.01/
      KPhaseIn=KPhase
      KPhase=KPhaseTwin(kitw)
      ich=0
      call indtr(ihi,rm6(1,iabs(isym),1,KPhase),ihp,NDim(KPhase))
      if(isym.lt.0) then
        do i=1,NDim(KPhase)
          ihp(i)=-ihp(i)
        enddo
      endif
      do i=1,NDim(KPhase)
        iho(i)=ihp(itr(i))
      enddo
      if(NTwin.gt.1.and.kitw.gt.0) then
        mmax=0
        do i=4,NDim(KPhase)
          mmax=max(iabs(ihi(i)),mmax)
        enddo
        call IndRealFromInd(ihi,xp,1)
        call MultM(xp,rtwi(1,kitw),hpp,1,3,3)
        call IndRealFromInd(ihp,xp,1)
        call MultM(xp,rtwi(1,kitw),hp,1,3,3)
        do 2000itw=2,NTwin
          KPhase=KPhaseTwin(itw)
          call multm(hp,rtw(1,itw),hs1,1,3,3)
          if(mmax.le.0) then
            do i=1,3
              if(abs(anint(hs1(i))-hs1(i)).gt..01) go to 2000
            enddo
          else
            call IndFromIndReal(hs1,mmax,difi,ih,j,k,-1.,CheckExtRefNo)
            if(k.le.0.or.j.ne.1) go to 2000
          endif
          call multm(hpp,rtw(1,itw),hsp,1,3,3)
          do i=1,NSymm(KPhase)
            call multm(hsp,rm(1,i,1,KPhase),hs2,1,3,3)
c            call multm(hp,rtw(1,itw),hs1,1,3,3)
            if(eqrv(hs1,hs2,3,.01)) go to 2000
            if(nscp.eq.2) then
              do j=1,3
                hs2(j)=-hs2(j)
              enddo
              if(eqrv(hs1,hs2,3,.01)) go to 2000
            endif
          enddo
          ich=1
          go to 9999
2000    continue
      endif
9999  KPhase=KPhaseIn
      return
      end
