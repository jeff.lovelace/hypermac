      subroutine SetMet(tisk)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real zsigp(9),vsig(9),smp(9),qup(3,3),MetTensOld(9),
     1     MetTensIOld(9),ee(3),qq(3),quip(3,3),
     2     QMat(9),QMatS(9),GammaI(9),GammaRI(9)
      character*80 t80,p80
      character*2 nty
      integer tisk
      logical EqRV,MatRealEqUnitMat
      data tpisq/19.7392088/
      if(ParentStructure.and.NQMag(KPhase).eq.1) then
        nd=4
        ndi=1
      else
        nd=NDim(KPhase)
        ndi=NDimI(KPhase)
      endif
      do i=1,NComp(KPhase)
        if(.not.SetMetAllowed) then
          call CopyMat(MetTens (1,i,KPhase),MetTensOld ,3)
          call CopyMat(MetTensI(1,i,KPhase),MetTensIOld,3)
        endif
        if(i.ne.1) then
          call matinv(zv(1,i,KPhase),zvi(1,i,KPhase),pom,nd)
          if(abs(pom).lt.0.) go to 9000
          m=0
          do j=1,3
            do k=1,3
              m=m+1
              pom=zv(k+(j-1)*nd,i,KPhase)
              do l=4,nd
                pom=pom+zv(k+(l-1)*nd,i,KPhase)*
     1                                  qu(j,l-3,1,KPhase)
              enddo
              zsig(m,i,KPhase)=pom
            enddo
          enddo
          if(KCommen(KPhase).gt.0.and.ndi.gt.1)
     1      call MultM(ZSig(1,i,KPhase),RCommen(1,1,KPhase),
     2                 RCommen(1,i,KPhase),3,3,3)
          call matinv(zsig(1,i,KPhase),zsigi(1,i,KPhase),pom,3)
          if(abs(pom).le.0.) go to 9100
          call trmat(zsig(1,i,KPhase),zsigp,3,3)
          call multm(zsig(1,i,KPhase),MetTensI(1,1,KPhase),smp,3,3,3)
          call multm(smp,zsigp,MetTensI(1,i,KPhase),3,3,3)
          call matinv(MetTensI(1,i,KPhase),MetTens(1,i,KPhase),
     1                CellVol(i,KPhase),3)
          if(CellVol(i,KPhase).le.0.) go to 9200
          CellVol(i,KPhase)=1./sqrt(CellVol(i,KPhase))
          m=1
          do j=1,3
            rcp(j,i,KPhase)=sqrt(MetTensI(m,i,KPhase))
            CellPar(j,i,KPhase)=sqrt(MetTens(m,i,KPhase))
            m=m+4
          enddo
          do j=4,6
            call indext(10-j,l,k)
            m=l+(k-1)*3
            rcp(j,i,KPhase)=MetTensI(m,i,KPhase)/
     1                      (rcp(l,i,KPhase)*rcp(k,i,KPhase))
            CellPar(j,i,KPhase)=MetTens(m,i,KPhase)/
     1                         (CellPar(l,i,KPhase)*CellPar(k,i,KPhase))
            if(abs(CellPar(j,i,KPhase)).le..000001)
     1                                   CellPar(j,i,KPhase)=0.
            CellPar(j,i,KPhase)=acos(CellPar(j,i,KPhase))/torad
          enddo
          csa=cos(torad*CellPar(4,i,KPhase))
          csb=cos(torad*CellPar(5,i,KPhase))
          csg=cos(torad*CellPar(6,i,KPhase))
          m=0
          do j=1,3
            do k=4,nd
              m=m+1
              pom=zv(k+(j-1)*nd,i,KPhase)
              do l=4,nd
                pom=pom+zv(k+(l-1)*nd,i,KPhase)*
     1                                  qu(j,l-3,1,KPhase)
              enddo
              vsig(m)=pom
            enddo
          enddo
          call multm(vsig,zsigi(1,i,KPhase),zsigp,ndi,3,3)
          do j=1,ndi
            do k=1,3
              qu(k,j,i,KPhase)=zsigp(j+(k-1)*ndi)
            enddo
          enddo
          if(KCommen(KPhase).gt.0) then
            do j=4,nd
              do k=4,nd
                pom=zv(k+(j-1)*nd,i,KPhase)
                do l=1,3
                  pom=pom-qu(l,j-3,i,KPhase)*
     1                    zv(l+(k-1)*nd,i,KPhase)
                enddo
                zsigp(k-3+(j-4)*ndi)=pom
              enddo
            enddo
            call multm(zsigp,trez(1,1,KPhase),trez(1,i,KPhase),
     1                 ndi,ndi,1)
          endif
        else
          call UnitMat(zv (1,1,KPhase),nd)
          call UnitMat(zvi(1,1,KPhase),nd)
          call UnitMat(zsig (1,1,KPhase),3)
          call UnitMat(zsigi(1,1,KPhase),3)
          call CopyVek(CellPar(1,1,KPhase),rcp(1,1,KPhase),3)
          cosa(KPhase)=cos(torad*CellPar(4,1,KPhase))
          cosb(KPhase)=cos(torad*CellPar(5,1,KPhase))
          cosg(KPhase)=cos(torad*CellPar(6,1,KPhase))
          if(abs(cosa(KPhase)).le..000001) cosa(KPhase)=0.
          if(abs(cosb(KPhase)).le..000001) cosb(KPhase)=0.
          if(abs(cosg(KPhase)).le..000001) cosg(KPhase)=0.
          rcp(4,1,KPhase)=cosa(KPhase)
          rcp(5,1,KPhase)=cosb(KPhase)
          rcp(6,1,KPhase)=cosg(KPhase)
          sina(KPhase)=sqrt(1.-cosa(KPhase)**2)
          sinb(KPhase)=sqrt(1.-cosb(KPhase)**2)
          sing(KPhase)=sqrt(1.-cosg(KPhase)**2)
          csa=cosa(KPhase)
          csb=cosb(KPhase)
          csg=cosg(KPhase)
          cotga(KPhase)=cosa(KPhase)/sina(KPhase)
          cotgb(KPhase)=cosb(KPhase)/sinb(KPhase)
          cotgg(KPhase)=cosg(KPhase)/sing(KPhase)
          call recip(rcp(1,1,KPhase),rcp(1,1,KPhase),CellVol(1,KPhase))
          Vcos(KPhase) =CellVol(1,KPhase)/
     1                  (CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1                                       CellPar(3,1,KPhase))
          Vcosr(KPhase)=1./(CellVol(1,KPhase)*
     1                  rcp(1,1,KPhase)*rcp(2,1,KPhase)*rcp(3,1,KPhase))
          cosar(KPhase)=rcp(4,1,KPhase)
          cosbr(KPhase)=rcp(5,1,KPhase)
          cosgr(KPhase)=rcp(6,1,KPhase)
          sinar(KPhase)=sqrt(1.-cosar(KPhase)**2)
          sinbr(KPhase)=sqrt(1.-cosbr(KPhase)**2)
          singr(KPhase)=sqrt(1.-cosgr(KPhase)**2)
          cotgar(KPhase)=cosar(KPhase)/sinar(KPhase)
          cotgbr(KPhase)=cosbr(KPhase)/sinbr(KPhase)
          cotggr(KPhase)=cosgr(KPhase)/singr(KPhase)
          if(CellVol(1,KPhase).le.0.) go to 9600
          MetTens(1,1,KPhase)=CellPar(1,1,KPhase)**2
          MetTens(5,1,KPhase)=CellPar(2,1,KPhase)**2
          MetTens(9,1,KPhase)=CellPar(3,1,KPhase)**2
          MetTens(2,1,KPhase)=CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1                        cosg(KPhase)
          MetTens(4,1,KPhase)=MetTens(2,1,KPhase)
          MetTens(3,1,KPhase)=CellPar(1,1,KPhase)*CellPar(3,1,KPhase)*
     1                        cosb(KPhase)
          MetTens(7,1,KPhase)=MetTens(3,1,KPhase)
          MetTens(6,1,KPhase)=CellPar(2,1,KPhase)*CellPar(3,1,KPhase)*
     1                        cosa(KPhase)
          MetTens(8,1,KPhase)=MetTens(6,1,KPhase)
          call matinv(MetTens(1,1,KPhase),MetTensI(1,1,KPhase),pom,3)
          if(pom.le.0.) go to 9600
          if(kcommen(KPhase).gt.0) then
            if(ICommen(KPhase).eq.0) then
              do j=1,ndi
                nn=1
                do k=1,3
                  nn=nn*ncommen(k,i,KPhase)
                enddo
                if(nn.eq.1) cycle
                do k=1,3
                  nn=ncommen(k,i,KPhase)
                  if(nn.gt.1)
     1              qu(k,j,i,KPhase)=anint(qu(k,j,i,KPhase)*nn)/
     2                               float(nn)
                enddo
              enddo
            endif
          endif
        endif
        if(KCommen(KPhase).gt.0) call SetCommen(i,ndi)
        do j=1,3
          prcp(j,i,KPhase)=.25*rcp(j,i,KPhase)**2
        enddo
        prcp(4,i,KPhase)=.25*rcp(1,i,KPhase)*rcp(2,i,KPhase)*
     1                                       rcp(6,i,KPhase)
        prcp(5,i,KPhase)=.25*rcp(1,i,KPhase)*rcp(3,i,KPhase)*
     1                                       rcp(5,i,KPhase)
        prcp(6,i,KPhase)=.25*rcp(2,i,KPhase)*rcp(3,i,KPhase)*
     1                                       rcp(4,i,KPhase)
        do j=1,3
          urcp(j,i,KPhase)=tpisq*rcp(j,i,KPhase)**2
        enddo
        urcp(4,i,KPhase)=tpisq*rcp(1,i,KPhase)*rcp(2,i,KPhase)
        urcp(5,i,KPhase)=tpisq*rcp(1,i,KPhase)*rcp(3,i,KPhase)
        urcp(6,i,KPhase)=tpisq*rcp(2,i,KPhase)*rcp(3,i,KPhase)
        if(SetMetAllowed) then
          snb=sqrt(1.-csb**2)
          sng=sqrt(1.-csg**2)
          pom=(csa-csb*csg)/sng
          TrToOrtho(1,i,KPhase)=CellPar(1,i,KPhase)
          TrToOrtho(2,i,KPhase)=0.
          TrToOrtho(3,i,KPhase)=0.
          TrToOrtho(4,i,KPhase)=CellPar(2,i,KPhase)*csg
          TrToOrtho(5,i,KPhase)=CellPar(2,i,KPhase)*sng
          TrToOrtho(6,i,KPhase)=0.
          TrToOrtho(7,i,KPhase)=CellPar(3,i,KPhase)*csb
          TrToOrtho(8,i,KPhase)=CellPar(3,i,KPhase)*pom
          TrToOrtho(9,i,KPhase)=CellPar(3,i,KPhase)*snb*
     1                          sqrt(1.-(pom/snb)**2)
          call matinv(TrToOrtho(1,i,KPhase),TrToOrthoI(1,i,KPhase),pom,
     1                3)
        else
          call CopyMat(MetTensOld ,MetTens (1,i,KPhase),3)
          call CopyMat(MetTensIOld,MetTensI(1,i,KPhase),3)
        endif
        if(i.eq.1) then
          do j=1,3
            do k=j,3
              if(k.eq.j) then
                MetTensS(4*(j-1)+1,i,KPhase)=(2.*CellPar(j,i,KPhase)*
     1                                        CellParSU(j,i,KPhase))**2
              else
                m=k+(j-1)*3
                l=6-j-k
                pom=MetTens(m,i,KPhase)
                if(abs(CellPar(l+3,i,KPhase)-90.).gt..001) then
                  pom=sqrt((pom/CellPar(j,i,KPhase)*
     1                      CellParSU(j,i,KPhase))**2+
     2                     (pom/CellPar(k,i,KPhase)*
     3                      CellParSU(k,i,KPhase))**2+
     4                     (pom*
     5                      tan(CellPar(l+3,i,KPhase)*ToRad)*
     6                      ToRad*CellParSU(l+3,i,KPhase))**2)
                else
                  pom=0.
                endif
                MetTensS(m,i,KPhase)=pom**2
                MetTensS(j+(k-1)*3,i,KPhase)=MetTensS(m,i,KPhase)
              endif
            enddo
          enddo
        else
          call SetRealArrayTo(CellParSU(1,i,KPhase),6,0.)
          call SetRealArrayTo(MetTensS (1,KPhase,i),9,0.)
        endif
      enddo
      k=0
      do i=1,3
        do j=1,NDimI(KPhase)
          k=k+1
          QMat(k)=qu(i,j,1,KPhase)
        enddo
      enddo
      do isw=1,NComp(KPhase)
        do i=1,ndi
          call multm(MetTensI(1,isw,KPhase),qu(1,i,isw,KPhase),
     1               qup(1,i),3,3,1)
          call multm(MetTensI(1,isw,KPhase),qui(1,i,KPhase),
     1               quip(1,i),3,3,1)
        enddo
        do i=1,nd
          k=i+(i-1)*nd
          do j=i,nd
            if(i.le.3.and.j.le.3) then
              MetTens6I(k,isw,KPhase)=MetTensI(i+3*(j-1),isw,KPhase)
            else if(i.le.3.and.j.gt.3) then
              MetTens6I(k,isw,KPhase)=qup(i,j-3)
            else
              pomq=scalmul(qu(1,i-3,isw,KPhase),qup(1,j-3))
              pome=scalmul(qui(1,i-3,KPhase),quip(1,j-3))
!              if(pome.eq.0.) pome=.0001
              MetTens6I(k,isw,KPhase)=pome+pomq
            endif
            if(i.ne.j) then
              l=j+(i-1)*nd
              MetTens6I(l,isw,KPhase)=MetTens6I(k,isw,KPhase)
            endif
            k=k+nd
          enddo
        enddo
        call MatInv(MetTens6I(1,isw,KPhase),MetTens6(1,isw,KPhase),det,
     1              nd)
      enddo
      go to 9999
9000  p80='the W-matrix is singular.'
      go to 9500
9100  p80='the W-matrix is incorrect.'
      go to 9500
9200  p80='metric tensor isn''t positive definite.'
9500  write(t80,100) i,nty(i)
      call FeChybne(-1.,-1.,p80,t80,SeriousError)
      go to 9900
9600  call FeChybne(-1.,-1.,'wrong cell parameters.',' ',SeriousError)
9900  ErrFlag=1
9999  return
100   format(i1,a2,' composite part')
      end
