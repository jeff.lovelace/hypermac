      subroutine CrlSetTrFractToOrtho(Cell,ToOrtho)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Cell(6),ToOrtho(9)
      csa=cos(torad*Cell(4))
      csb=cos(torad*Cell(5))
      csg=cos(torad*Cell(6))
      snb=sqrt(1.-csb**2)
      sng=sqrt(1.-csg**2)
      pom=(csa-csb*csg)/sng
      ToOrtho(1)=Cell(1)
      ToOrtho(2)=0.
      ToOrtho(3)=0.
      ToOrtho(4)=Cell(2)*csg
      ToOrtho(5)=Cell(2)*sng
      ToOrtho(6)=0.
      ToOrtho(7)=Cell(3)*csb
      ToOrtho(8)=Cell(3)*pom
      ToOrtho(9)=Cell(3)*snb*sqrt(1.-(pom/snb)**2)
      return
      end
