      subroutine CrlMakeExtString(RefGroupMat,RefCond,StRefGroup,
     1                            StRefCond)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RefGroupMat(*),RefCond(*)
      character*10 t10
      character*(*) StRefGroup,StRefCond
      StRefGroup='('
      do i=1,NDim(KPhase)
        k=i
        t10=' '
        do j=1,NDim(KPhase)
          pom=RefGroupMat(k)
          if(abs(pom).gt..0001) then
            call ToFract(pom,Cislo,15)
            if(pom.gt..0001.and.idel(t10).gt.0)
     1        t10=t10(:idel(t10))//'+'
            if(Cislo.eq.'1') then
              t10=t10(:idel(t10))//indices(j)
            else if(Cislo.eq.'-1') then
              t10=t10(:idel(t10))//'-'//indices(j)
            else
              t10=t10(:idel(t10))//Cislo(:idel(Cislo))//
     1          indices(j)
            endif
          endif
          k=k+NDim(KPhase)
        enddo
        if(t10.eq.' ') t10='0'
        StRefGroup=StRefGroup(:idel(StRefGroup))//t10(:idel(t10))
        if(i.ne.NDim(KPhase)) then
          StRefGroup=StRefGroup(:idel(StRefGroup))//','
        else
          StRefGroup=StRefGroup(:idel(StRefGroup))//')'
        endif
      enddo
      StRefCond=' '
      do i=1,NDim(KPhase)
        pom=RefCond(i)
        if(abs(pom).gt..0001) then
          call ToFract(pom,Cislo,15)
          if(pom.gt..0001.and.idel(StRefCond).gt.0)
     1        StRefCond=StRefCond(:idel(StRefCond))//'+'
          if(Cislo.eq.'1') then
            StRefCond=StRefCond(:idel(StRefCond))//indices(i)
          else if(Cislo.eq.'-1') then
            StRefCond=StRefCond(:idel(StRefCond))//'-'//indices(i)
          else
            StRefCond=StRefCond(:idel(StRefCond))//
     1                Cislo(:idel(Cislo))//indices(i)
          endif
        endif
      enddo
      if(StRefCond.eq.' ') go to 5000
      write(Cislo,'(''='',i5,''n'')') nint(RefCond(NDim(KPhase)+1))
      call zhusti(Cislo)
      StRefCond=StRefCond(:idel(StRefCond))//Cislo(:idel(Cislo))
      go to 9999
5000  StRefCond='No condition'
9999  return
      end
