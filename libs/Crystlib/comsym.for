      subroutine ComSym(WhichPhase,tisk,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension GammaInt(9),GammaIntInv(9),xp(6),rmp(36),xpp(3),
     1          GammaN(9),rm6p(36),s1(6),s2(6)
      dimension rm6pp(36)
      character*80 Veta
      character*15 SymmSt(:,:)
      integer smflag(:),tisk,WhichPhase,CrlCentroSymm,CrlMagInver
      logical eqrv,ClosedInSuperCell,EqRV0
      allocatable SymmSt,smflag
      ISymmCommen=0
      ich=0
      KPhaseIn=KPhase
      if(KCommenMax.le.0) go to 9999
      if(SwitchedToComm) then
        if(VasekTest.ne.0)
     1    call FeChybne(-1.,-1.,'chce prepinat comm, ale ono je jiz '//
     2                  'prepnuto',' ',Warning)
        go to 9999
      endif
      nc=0
      ns=0
      nvt=0
      do KPh=1,NPhase
        KPhase=KPh
        call CrlStoreSymmetry(ISymmSSG(KPhase))
        if(KCommen(KPh).gt.0) then
          nc=max(NComp(KPh),nc)
          ns=max(NSymm(KPh),ns)
          nvt=max(NLattVec(KPh),nvt)
        endif
      enddo
      if(allocated(rmgc))
     1  deallocate(rmgc,rm6gc,s6gc,KwSymGC,GammaIntGC,rtgc,rc3gc,
     2             rc4gc,rc5gc,rc6gc,iswGC,RMagGC,ZMagGC)
      n=ns*nvt
      allocate(rmgc(9,n,nc,NPhase),rm6gc(36,n,nc,NPhase),
     1         s6gc(6,n,nc,NPhase),
     2         KwSymGC(mxw,n,nc,NPhase),rtgc(36,n,nc,NPhase),
     3         rc3gc(100,n,nc,NPhase),rc4gc(225,n,nc,NPhase),
     4         rc5gc(441,n,nc,NPhase),rc6gc(784,n,nc,NPhase),
     5         GammaIntGC(9,n,nc,NPhase),iswGC(n,nc,NPhase),
     6         RMagGC(9,n,nc,NPhase),ZMagGC(n,nc,NPhase))
      allocate(smflag(n))
      ngcMax=0
      do KPh=1,NPhase
        KPhase=KPh
        if(KCommen(KPh).le.0) then
          ngc(KPh)=0
          cycle
        endif
        nsp=NSymm(KPh)
        do isw=1,NComp(KPh)
          if(NDimI(KPh).eq.1) then
            fn=NCommQ(1,isw,KPh)*NCommQ(2,isw,KPh)*NCommQ(3,isw,KPh)
          else
            fn=NCommen(1,isw,KPh)*NCommen(2,isw,KPh)*
     1         NCommen(3,isw,KPh)
          endif
          if(fn.le.0.) then
            if(NDimI(KPh).eq.1) then
              write(Veta,'(2(i5,''x''),i5)')(NCommQ(i,isw,KPh),i=1,3)
            else
              write(Veta,'(2(i5,''x''),i5)')(NCommen(i,isw,KPh),i=1,3)
            endif
            call Zhusti(Veta)
            call FeChybne(-1.,-1.,'unrealistic supercell: '//
     1                    Veta(:idel(Veta)),' ',SeriousError)
            ich=1
            go to 9999
          endif
          do i=1,NDimI(KPh)
            if(NDimI(KPh).eq.1) fn=NCommQ(i,isw,KPh)
            do j=1,3
              qu(j,i,isw,KPh)=anint(fn*qu(j,i,isw,KPh))/fn
            enddo
          enddo
        enddo
        if(NLattVec(KPh).gt.1) then
          do i=1,NLattVec(KPh)
            call qbyx(vt6(1,i,1,KPh),xp,1)
            do j=1,NDimI(KPh)
              xp(j)=xp(j)-vt6(j+3,i,1,KPh)
            enddo
            if(ClosedInSupercell(xp)) then
              smflag(i)=1
            else
              smflag(i)=0
            endif
          enddo
          nn=1
          do i=1,NLattVec(KPh)
            if(smflag(i).ne.0) cycle
            nn=nn+1
            do j=1,NLattVec(KPh)
              if(smflag(j).ne.1.or.j.eq.i) cycle
              call AddVek(vt6(1,i,1,KPh),vt6(1,j,1,KPh),xp,
     1                    NDim(KPh))
              call normtr(xp,1,0)
              do k=i+1,NLattVec(KPh)
                if(smflag(k).ne.0) cycle
                if(eqrv(xp,vt6(1,k,1,KPh),NDim(KPh),.0001))
     1            smflag(k)=-1
              enddo
            enddo
          enddo
          nss=NSymm(KPh)
          call ReallocSymm(NDim(KPh),nss*nn,NLattVec(KPh),NComp(KPh),
     1                     NPhase,0)
          do i=1,NLattVec(KPh)
            if(smflag(i).ne.0) cycle
            do isw=1,NComp(KPh)
              do k=1,NSymm(KPh)
                call CopySymmOperator(k,k+nss,isw)
                call AddVek(s6(1,k+nss,isw,KPh),vt6(1,i,isw,KPh),
     1                      s6(1,k+nss,isw,KPh),NDim(KPh))
                call normtr(s6(1,k+nss,isw,KPh),1,0)
              enddo
            enddo
            nss=nss+NSymm(KPh)
          enddo
          NSymm(KPh)=nss
          NSymmN(KPh)=nss
          j=0
          do i=1,NLattVec(KPh)
            if(smflag(i).eq.1) then
              j=j+1
              do isw=1,NComp(KPh)
                call CopyVek(vt6(1,i,isw,KPh),vt6(1,j,isw,KPh),
     1                       NDim(KPh))
              enddo
          endif
          enddo
          NLattVec(KPh)=j
        endif
2500    centr(KPh)=NLattVec(KPh)
        do i=1,NSymm(KPh)
          call GetGammaInt(RM6(1,i,1,KPh),GammaInt)
          call normtr(s6(1,i,1,KPh),1,1)
          call qbyx(s6(1,i,1,KPh),xp,1)
          call CopyVek(s6(4,i,1,KPh),xpp,NDimI(KPh))
          call Cultm(GammaInt,trez(1,1,KPh),xpp,NDimI(KPh),NDimI(KPh),
     1               1)
          do j=1,NDimI(KPh)
            xp(j)=xp(j)+trez(j,1,KPh)-xpp(j)
          enddo
          if(ClosedInSupercell(xp)) then
            smflag(i)=1
          else
            smflag(i)=0
          endif
        enddo
        nsr=NSymm(KPh)/nsp
        do is=1,nsp
          do ir=1,nsr
            i=(ir-1)*nsp+is
            if(smflag(i).ne.0) cycle
            do js=1,nsp
              do jr=1,nsr
                j=(jr-1)*nsp+js
                if(smflag(j).ne.1.or.j.eq.i) cycle
                call multm(rm6(1,j,1,KPh),rm6(1,i,1,KPh),rmp,
     1                     NDim(KPh),NDim(KPh),NDim(KPh))
                call CopyVek(s6(1,j,1,KPh),xp,NDim(KPh))
                call cultm(rm6(1,j,1,KPh),s6(1,i,1,KPh),xp,
     1                     NDim(KPh),NDim(KPh),1)
                call normtr(xp,1,1)
                do k=1,NSymm(KPh)
                  ks=mod(k-1,nsp)+1
                  if(ks.lt.is) then
                    cycle
                  else if(ks.eq.is) then
                    kr=(k-1)/nsp+1
                    if(kr.le.ir) cycle
                  endif
                  if(smflag(k).ne.0) cycle
                  if(eqrv(rm6(1,k,1,KPh),rmp,NDimQ(KPh),.0001)) then
                    call CopyVek(s6(1,k,1,KPh),s1,NDim(KPh))
                    call CopyVek(xp,s2,NDim(KPh))
                    call NormCentr(s1)
                    call NormCentr(s2)
                    if(eqrv(s6(1,k,1,KPh),xp,NDim(KPh),.0001))
     1                smflag(k)=-1
                  endif
                enddo
              enddo
            enddo
          enddo
        enddo
        ngc(KPh)=0
        do k=1,NSymm(KPh)
          if(smflag(k).eq.0) ngc(KPh)=ngc(KPh)+1
        enddo
        ngcMax=max(ngcMax,ngc(KPh))
        n=0
        do k=1,NSymm(KPh)
          if(smflag(k).ne.0) cycle
          n=n+1
          do isw=1,NComp(KPh)
            call CopyVek(rm6(1,k,isw,KPh),rm6gc(1,n,isw,KPh),
     1                   NDimQ(KPh))
            call CopyVek(rm (1,k,isw,KPh),rmgc (1,n,isw,KPh),9)
            call GetGammaIntInv(rm6(1,k,isw,KPh),GammaIntInv)
            do j=1,NDimI(KPh)**2
              GammaIntGC(j,n,isw,KPh)=nint(GammaIntInv(j))
            enddo
            call srotb(rmgc(1,n,isw,KPh),rmgc(1,n,isw,KPh),
     1                 rtgc(1,n,isw,KPh))
            call srotc(rmgc(1,n,isw,KPh),3,rc3gc(1,n,isw,KPh))
            call srotc(rmgc(1,n,isw,KPh),4,rc4gc(1,n,isw,KPh))
            call srotc(rmgc(1,n,isw,KPh),5,rc5gc(1,n,isw,KPh))
            call srotc(rmgc(1,n,isw,KPh),6,rc6gc(1,n,isw,KPh))
            call CopyVek(s6(1,k,isw,KPh),s6gc(1,n,isw,KPh),
     1                   NDim(KPh))
            call CopyVek(RMag(1,k,isw,KPh),RMagGC(1,n,isw,KPh),9)
            ZMagGC(n,isw,KPh)=ZMag(k,isw,KPh)
          enddo
        enddo
        j=0
        do i=1,NSymm(KPh)
          if(smflag(i).eq.1) then
            j=j+1
            do isw=1,NComp(KPh)
              call CopyVek(rm6(1,i,isw,KPh),rm6(1,j,isw,KPh),
     1                     NDimQ(KPh))
              call CopyVek(s6(1,i,isw,KPh), s6(1,j,isw,KPh),
     1                     NDim(KPh))
              call MatBlock3(rm6(1,j,isw,KPh),rm(1,j,isw,KPh),
     1                       NDim(KPh))
              ZMag(j,isw,KPh)=ZMag(i,isw,KPh)
              ISwSymm(j,isw,KPh)=ISwSymm(i,isw,KPh)
              call CopyVek(RMag(1,i,isw,KPh),RMag(1,j,isw,KPh),9)
            enddo
          endif
        enddo
        NSymm(KPh)=j
        if(CrlMagInver().gt.0) then
          NSymmN(KPhase)=j/2
        else
          NSymmN(KPhase)=j
        endif
        if(tisk.eq.1.and.n.gt.0.and.
     1     (WhichPhase.eq.0.or.WhichPhase.eq.KPh)) then
          call newln(3)
          write(lst,'(/''Commensurate option leads to the '',
     1                 ''following symmetry :''/)')
          call newln(1)
          if(CrlCentroSymm().gt.0) then
            write(lst,'(''Centrosymmetric super-space group'')')
          else
            write(lst,'(''Non-centrosymmetric super-space group'')')
          endif
          call newln(NLattVec(KPh)+3)
          write(lst,'(/''List of centring vectors :''/)')
          do i=1,NLattVec(KPh)
            write(lst,'(6f10.6)')(vt6(j,i,1,KPh),j=1,NDim(KPh))
          enddo
          call newln(NSymm(KPh)+3)
          write(lst,'(/''Symmetry operators :''/)')
          allocate(SymmSt(NDim(KPh),NSymm(KPh)))
          m=0
          do i=1,NSymm(KPh)
            call CodeSymm(rm6(1,i,1,KPh),s6(1,i,1,KPh),SymmSt(1,i),
     1                    0)
            do j=1,NDim(KPh)
              m=max(m,idel(SymmSt(j,i))+1)
            enddo
          enddo
          if(m.lt.5) m=5
          do i=1,NSymm(KPh)
            Veta=' '
            k=5
            do l=1,NDim(KPhase)
              if(SymmSt(l,i)(1:1).eq.'-') then
                kk=k
              else
                kk=k+1
              endif
              Veta(kk:)=SymmSt(l,i)
              k=k+m
            enddo
            write(lst,FormA) Veta(:idel(Veta))
          enddo
          call newln(n+3)
          write(lst,'(/''The following generators will be applied '',
     1                 ''to all atoms :''/)')
          deallocate(SymmSt)
          allocate(SymmSt(NDim(KPh),n))
          m=0
          do i=1,n
            call CodeSymm(rm6gc(1,i,1,KPh),s6gc(1,i,1,KPh),
     1                    SymmSt(1,i),0)
            do j=1,NDim(KPh)
              m=max(m,idel(SymmSt(j,i))+1)
            enddo
          enddo
          if(m.lt.5) m=5
          do i=1,n
            Veta=' '
            k=5
            do l=1,NDim(KPhase)
              if(SymmSt(l,i)(1:1).eq.'-') then
                kk=k
              else
                kk=k+1
              endif
              Veta(kk:)=SymmSt(l,i)
              k=k+m
            enddo
            write(lst,FormA) Veta(:idel(Veta))
          enddo
          deallocate(SymmSt)
        endif
      enddo
      do KPh=1,NPhase
        do isw=1,NComp(KPh)
          do 5100j=1,ngc(KPh)
            do m=4,NDim(KPh)
              do k=1,3
                GammaN(k+(m-4)*3)=rm6gc(k+(m-1)*NDim(KPh),j,isw,KPh)
              enddo
            enddo
            if(EqRV0(GammaN,3*NDimI(KPh),.0001)) then
              iswGC(j,isw,KPh)=isw
              go to 5100
            else
              call Multm(ZVi(1,isw,KPh),rm6gc(1,j,isw,KPh),rm6p,
     1                   NDim(KPh),NDim(KPh),NDim(KPh))
              iswGC(j,isw,KPh)=0
              do 5050jsw=1,NComp(KPh)
                if(jsw.eq.isw) go to 5050
                call Multm(rm6p,ZV(1,jsw,KPh),rm6pp,
     1                     NDim(KPh),NDim(KPh),NDim(KPh))
                do m=4,NDim(KPh)
                  do k=1,3
                    if(abs(rm6pp(k+(m-1)*NDim(KPh))).gt..0001)
     1                go to 5050
                  enddo
                enddo
                iswGC(j,isw,KPh)=jsw
                call CopyMat(rm6pp,RM6GC(1,j,isw,KPh),NDim(KPh))
                call MatBlock3(rm6pp,rmGC(1,j,isw,KPh),NDim(KPh))
                call GetGammaIntInv(RM6GC(1,j,isw,KPh),GammaIntInv)
                do k=1,NDimI(KPh)**2
                  GammaIntGC(k,j,isw,KPh)=nint(GammaIntInv(k))
                enddo
                call srotb(rmgc(1,j,isw,KPh),rmgc(1,j,isw,KPh),
     1                     rtgc(1,j,isw,KPh))
                call srotc(rmgc(1,j,isw,KPh),3,rc3gc(1,j,isw,KPh))
                call srotc(rmgc(1,j,isw,KPh),4,rc4gc(1,j,isw,KPh))
                call srotc(rmgc(1,j,isw,KPh),5,rc5gc(1,j,isw,KPh))
                call srotc(rmgc(1,j,isw,KPh),6,rc6gc(1,j,isw,KPh))
                call MultM(ZVi(1,isw,KPh),s6gc(1,j,isw,KPh),rm6p,
     1                     NDim(KPh),NDim(KPh),1)
                call MultM(ZV (1,jsw,KPh),rm6p,s6gc(1,j,isw,KPh),
     1                     NDim(KPh),NDim(KPh),1)
                go to 5100
5050          continue
              call FeChybne(-1.,-1.,'the composite description not '//
     1          'complete.','Symmetry induced subsystem not defined.',
     2          SeriousError)
            endif
5100      continue
        enddo
        call CrlStoreSymmetry(ISymmCommen(KPhase))
      enddo
      SwitchedToComm=.true.
      call CrlOrderMagSymmetry
9999  if(allocated(smflag)) deallocate(smflag)
      KPhase=KPhaseIn
      return
      end
