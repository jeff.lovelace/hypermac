      subroutine PrimaExport(Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest
      character*20  GrFull,GrOrigin
      character*10  GrPerm
      character*8   GrShort
      character*6   GrPoint
      character*5   GrNum
      real :: xp(3),Lambda0=0.002
      integer symfac(6),mxsymfac(6),MEMDivision(6),ih(6)
      logical EqIgCase,ExistFile,Poprve,EqIV0
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup_list.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_list.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
1100  read(ln,101,end=1150) GrNum,GrShort,GrFull,GrPerm,GrPoint,
     1                      GrOrigin
      if(EqIgCase(GrShort,Grupa(KPhase))) then
        if(.not.EqIgCase(GrPerm,'(a,b,c)').and.GrPerm.ne.' ') go to 1100
        if(GrOrigin.eq.' ') then
          GrOrigin='0 0 0'
        else
          do i=1,idel(GrOrigin)
            if(GrOrigin(i:i).eq.',') GrOrigin(i:i)=' '
          enddo
        endif
        k=0
        call StToReal(GrOrigin,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 1100
        do i=1,3
          xp(i)=xp(i)+ShSg(i,KPhase)
        enddo
        do i=1,NLattVec(KPhase)
          n=0
          do j=1,3
            pom=vt6(j,i,1,KPhase)-xp(j)
            if(abs(pom-anint(pom)).gt..001) then
              exit
            else
              n=n+1
            endif
          enddo
          if(n.ge.3) exit
        enddo
        if(n.lt.3) go to 1100
        i=index(GrNum,'/')
        if(i.eq.0) then
          NSet=1
        else
          if(GrNum(i+1:i+1).eq.'1') then
            NSet=1
          else
            NSet=2
          endif
          GrNum(i:)=' '
        endif
        read(GrNum,*) NGr
      else
        go to 1100
      endif
1150  call CloseIfOpened(ln)
      if(NGr.le.0) go to 9000
      if(.not.ExistFile(fln(:ifln)//'.m80')) go to 9100
      ich=0
      id=NextQuestId()
      xqd=450.
      il=3
      Veta='Create MEM file for'
      if(Klic.eq.0) then
        Veta=Veta(:idel(Veta))//' PRIMA'
      else
        Veta=Veta(:idel(Veta))//' Dysnomia'
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      tpom=5.
      xpom=80.
      dpom=xqd-xpom-5.
      Veta='%Title:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwTitle=EdwLastMade
      if(Klic.eq.0) then
        Veta=fln(:ifln)//' - for PRIMA'
      else
        Veta=fln(:ifln)//' - for Dysnomia'
      endif
      call FeQuestStringEdwOpen(EdwLastMade,Veta)
      il=il+1
      Veta='%MEM filename:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMEM=EdwLastMade
      Veta=fln(:ifln)//'.mem'
      call FeQuestStringEdwOpen(EdwLastMade,Veta)
      do ntv=1,NLattVec(KPhase)
        do isymm=1,NSymmN(KPhase)
          do i=1,NDim(KPhase)
            sh=vt6(i,ntv,1,KPhase)+s6(i,isymm,1,KPhase)
            symfac(i)=0
            mxsymfac(i)=0
1200        symfac(i)=symfac(i)+1
            pom=sh*float(symfac(i))
            if (abs(pom-nint(pom)).ge..001) goto 1200
            if (symfac(i).gt.mxsymfac(i)) mxsymfac(i)=symfac(i)
          enddo
        enddo
      enddo
      do i=1,3
        ddiv=CellPar(i,1,KPhase)/.08
        mindif=ddiv+2.
        n2max=int(log(ddiv)/log(2.))+1
        n3max=int(log(ddiv)/log(3.))+1
        do j2=0,n2max
          do j3=0,n3max
            dummy=float(2**j2*3**j3*mxsymfac(i))
            if (abs(dummy-ddiv).lt.mindif) then
              mindif=abs(dummy-ddiv)
              n2b=j2
              n3b=j3
            endif
          enddo
        enddo
        MEMDivision(i)=2**n2b*3**n3b*mxsymfac(i)
      enddo
      il=il+1
      Veta='%Pixel division:'
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwPixels=EdwLastMade
      call FeQuestIntAEdwOpen(EdwLastMade,MEMDivision,3,.false.)
!      il=il+1
      Veta='%Initial Lagrangian multiplier:'
      xpom=xqd-dpom-5.
      tpom=xpom-FeTxLengthUnder(Veta)-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwLambda=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Lambda0,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        Veta=EdwStringQuest(nEdwMEM)
        call OpenFile(ln,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        Veta=EdwStringQuest(nEdwTitle)
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,'(6f10.5)') CellPar(1:6,1,KPhase)
        if(Klic.eq.0) then
          write(ln,'(f15.7,4i5)') 0.,-1,0,0,0
        else
          write(ln,'(2f15.7,4i5)') 0.,0.,-1,0,0,0
        endif
        call FeQuestRealFromEdw(nEdwLambda,Lambda0)
        call FeQuestIntAFromEdw(nEdwPixels,MEMDivision)
        write(ln,100) NGr,NSet,MEMDivision(1:3)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call OpenFile(80,fln(:ifln)//'.m80','formatted','unknown')
      Poprve=.true.
      n=0
      call FouNastavM80
3000  read(80,FormA,end=3100) Veta
      if(NDatBlock.gt.1) then
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(1:5),'block')) go to 3100
      endif
      read(Veta,Format80,end=3100,err=8000)(ih(i),i=1,maxNDim),KPh,
     1  fo1,fo2,fc,ac,bc,acfree,bcfree,acst,bcst,acfreest,bcfreest,
     2  sigfo1,sigfo2
      if(KPh.ne.KPhase) go to 3000
      if(EqIV0(ih,maxNDim)) then
        if(Poprve) then
          if(Radiation(KDatBlock).eq.NeutronRadiation) then
            Totale=Fc+bc
            TotaleN=-bc
            bc=0.
            bcfree=0.
            bcst=0.
            bcfreest=0.
          else
            Totale=Fc
          endif
        endif
        go to 3000
      endif
      if(Poprve) then
        n=n+1
      else
        pom=sqrt(ac**2+bc**2)
        write(ln,'(3i5,3f15.7)') ih(1:3),fo1*ac/pom,fo1*bc/pom,sigfo1
      endif
      go to 3000
3100  if(Poprve) then
        if(Radiation(KDatBlock).eq.NeutronRadiation) then
          write(ln,102) Totale,TotaleN,Lambda0
        else
          write(ln,103) Totale,Lambda0
        endif
        write(ln,100) n
        rewind 80
        call FouNastavM80
        Poprve=.false.
        go to 3000
      endif
      go to 9999
8000  call FeReadError(80)
      go to 9999
9000  if(Klic.eq.0) then
        Veta='PRIMA'
      else
        Veta='Dysnomia'
      endif
      Veta=Veta(:idel(Veta))//' works only with standard settings !!!'
      call FeChybne(-1.,-1.,Veta,'Please transfrom first your '//
     1              'structure and try again.',SeriousError)
      go to 9999
9100  Veta='the file m80 which contains reflections with phases is '//
     1     'missing.'
      call FeChybne(-1.,-1.,Veta,'Run the refinement procedure with '//
     1              '0 or more cycles and try again.',SeriousError)
9999  call CloseIfOpened(ln)
      call CloseIfOpened(80)
      return
100   format(5i5)
101   format(a5,1x,a8,1x,a20,1x,a10,1x,a6,1x,a20)
102   format(2f10.3,f10.7)
103   format( f10.3,f10.7)
      end
