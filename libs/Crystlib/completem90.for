      subroutine CompleteM90
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 FileOut,Veta1,Veta2
      logical ExistFile,EqIgCase,UseDatBlock
      NDatB=0
      do i=1,NDatBlock
        if(NRef90(i).gt.0) NDatB=NDatB+1
      enddo
      if(NDatB.le.0) then
        call DeleteFile(fln(:ifln)//'.m90')
        ExistM90=.false.
        NDatBlock=0
        go to 9999
      endif
      FileOut=fln(:ifln)//'.l90'
      call iom90(1,FileOut)
      lno=NextLogicNumber()
      lni=0
      if(ExistM90) then
        call OpenFile(lno,FileOut,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        lni=NextLogicNumber()
        call OpenFile(lni,fln(:ifln)//'.m90','formatted','old')
        if(ErrFlag.ne.0) go to 9999
1050    read(lni,FormA,end=1200) Veta1
        read(lno,FormA,end=1200) Veta2
        if(.not.EqIgCase(Veta1,Veta2)) go to 1200
        if(.not.EqIgCase(Veta1,'end')) go to 1050
      else
        go to 1200
      endif
      do i=1,NDatBlock
        write(Cislo,'(''.l'',i2)') MxRefBlock+i
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        if(ModifyDatBlock(i).or.
     1     ExistFile(fln(:ifln)//Cislo(:idel(Cislo)))) go to 1200
      enddo
      go to 9000
1200  call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      call OpenForAppend(lno,FileOut)
      if(ErrFlag.ne.0) go to 9000
      if(lni.eq.0) lni=NextLogicNumber()
      n=0
      do i=1,NDatBlock
        n=n+NRef90(i)
      enddo
      call FeFlowChartOpen(-1.,-1.,max(nint(float(n)*.005),10),n,
     1                     'Completing reflection file',' ',' ')
      iz=0
      NDatB=0
      do i=1,NDatBlock
        UseDatBlock=NRef90(i).gt.0
        if(UseDatBlock) NDatB=NDatB+1
        write(Cislo,'(''.l'',i2)') MxRefBlock+i
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        DatBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        if(ExistFile(DatBlockFileName)) then
          call OpenFile(lni,DatBlockFileName,'formatted','old')
        else
          if(ExistM90) then
            call OpenDatBlockM90(lni,i,fln(:ifln)//'.m90')
          else
            call FeFlowChartRemove
            go to 9000
          endif
        endif
        if(ErrFlag.ne.0) then
          call FeFlowChartRemove
          go to 9000
        endif
        if(UseDatBlock) write(lno,'(''Data '',a)') DatBlockName(NDatB)
2100    read(lni,FormA,end=2200) Veta1
        k=0
        call kus(Veta1,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 2200
        if(UseDatBlock) then
          write(lno,'(a)') Veta1(:idel(Veta1))
          call FeFlowChartEvent(iz,ie)
        endif
        go to 2100
2200    call CloseIfOpened(lni)
        if(ExistFile(DatBlockFileName))
     1    call DeleteFile(DatBlockFileName)
      enddo
      NDatBlock=NDatB
      call FeFlowChartRemove
5000  call CloseIfOpened(lno)
      call MoveFile(fln(:ifln)//'.l90',fln(:ifln)//'.m90')
      ExistM90=.true.
9000  call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      call DeleteFile(FileOut)
9999  return
      end
