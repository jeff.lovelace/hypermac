      function KoincAtoms(ia,iod,ido,dmez,dist)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension x1(6),y1(6),y2(6),y3(6),y3r(6)
      call SetRealArrayTo(x1,6,0.)
      call CopyVek(x(1,ia),x1,3)
      if(NDimI(kswa(ia)).eq.1.and.KModA(1,ia).gt.0) then
        if(KFA(1,ia).ne.0) then
          call od0do1(ax(KModA(1,ia),ia),x1(4),1)
        else if(KFA(2,ia).ne.0) then
          call od0do1(uy(1,KModA(2,ia),ia),x1(4),1)
        endif
      endif
      do 2000j=iod,ido
        kswj=kswa(j)
        if(kswj.ne.kswa(ia).or.KModA(1,j).ne.KModA(1,ia).or.
     1     KFA(1,j).ne.KFA(1,ia)) go to 2000
        call SetRealArrayTo(y1,6,0.)
        call CopyVek(x(1,j),y1,3)
        NDimP=3
        if(NDimI(kswj).eq.1.and.KModA(1,j).gt.0) then
          if(KFA(1,j).ne.0) then
            call od0do1(ax(KModA(1,j),j),y1(4),1)
            NDimP=4
          else if(KFA(2,j).ne.0) then
            call od0do1(uy(1,KModA(2,j),j),x1(4),1)
            NDimP=4
          endif
        endif
        do 1500jsym=1,NSymm(kswj)
          iswj=ISwSymm(jsym,iswa(j),kswj)
          if(iswj.ne.iswa(ia)) go to 1500
          call multm(rm6(1,jsym,iswj,kswj),y1,y2,NDim(kswj),NDim(kswj),
     1               1)
          call AddVek(y2,s6(1,jsym,iswj,kswj),y2,NDim(kswj))
          do 1400ivt=1,NLattVec(kswj)
            call AddVek(y2,vt6(1,ivt,iswj,kswj),y3,NDim(kswj))
            do m=1,NDimP
              if(m.gt.3) call od0do1(y3(m),y3(m),1)
              y3(m)=x1(m)-y3(m)
              y3(m)=y3(m)-anint(y3(m))
              if(m.gt.3.and.abs(y3(m)).gt..00001.and.
     1                      abs(y3(m)).lt..99999) go to 1400
            enddo
            call multm(MetTens(1,iswj,kswj),y3,y3r,3,3,1)
            dist=sqrt(scalmul(y3,y3r))
            if(dist.le.dmez) then
              KoincAtoms=j
              go to 9999
            endif
1400      continue
1500    continue
2000  continue
      dist=-1.
      KoincAtoms=0
9999  return
      end
