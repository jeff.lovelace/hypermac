      subroutine ImportAmplimodes(ich)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(36),ip(4)
      real, allocatable :: XYZModePom(:),aiOld(:),XOld(:,:),BetaOld(:,:)
      integer, allocatable :: ipor(:),jpor(:),isfOld(:),itfOld(:),
     1                        IAtPom(:)
      character*256 Veta,FileName,EdwStringQuest
      character*80  t80
      character*8   Atp,IrrepLabel
      logical CrwLogicQuest,EqIgCase,EqRV
      ln=0
      FileName=' '
      IFile=-1
      ich=0
      id=NextQuestId()
      xqd=450.
      il=3
      Veta='Import amplimodes:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='from %Crystallographic server in Bilbao by Copy/Paste'//
     1     ' method'
      xpom=5.
      tpom=xpom+CrwgXd+15.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
        if(i.eq.1) then
          nCrwCopyPaste=CrwLastMade
          Veta='from already existing %Fullprof file'
        else
          nCrwFullprof=CrwLastMade
        endif
        il=il+1
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      Veta='File %name:'
      bpom=80.
      tpom=5.
      xpom=FeTxLength(Veta)+10.
      dpom=xqd-xpom-bpom-15.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      Veta='%Browse'
      call FeQuestButtonMake(id,xqd-bpom-5.,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
1400  if(CrwLogicQuest(nCrwCopyPaste)) then
        FileName=EdwStringQuest(nEdwName)
        call FeQuestEdwDisable(nEdwName)
        call FeQuestButtonDisable(nButtBrowse)
      else if(CheckNumber.eq.nCrwFullprof) then
        call FeQuestStringEdwOpen(nEdwName,FileName)
        call FeQuestButtonOff(nButtBrowse)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        call FeFileManager('Define input Fullprof file:',FileName,
     1                     '*.pcr',0,.true.,ich)
        call FeQuestStringEdwOpen(nEdwName,FileName)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        ln=NextLogicNumber()
        if(CrwLogicQuest(nCrwCopyPaste)) then
          IFile=0
          FileName='jpcr'
          call CreateTmpFile(FileName,i,0)
          call FeTmpFilesAdd(FileName)
          call OpenFile(ln,FileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          write(ln,FormA) '! Paste here the amplimode block for '//
     1                    'Fullprof'
          call CloseIfOpened(ln)
          call FeEdit(FileName)
        else
          IFile=1
        endif
        call OpenFile(ln,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      NAtOld=0
      do i=1,NAtInd
        if(kswa(i).eq.KPhase) NAtOld=NAtOld+1
      enddo
      if(NAtOld.gt.0) then
        if(allocated(isfOld)) deallocate(isfOld,itfOld,aiOld,XOld,
     1                                   BetaOld)
        allocate(isfOld(NAtOld),itfOld(NAtOld),aiOld(NAtOld),
     1           XOld(3,NAtOld),BetaOld(6,NAtOld))
        n=0
        do i=1,NAtInd
          if(kswa(i).eq.KPhase) then
            n=n+1
            isfOld(n)=isf(i)
            isf(i)=0
            itfOld(n)=itf(i)
            aiOld(n)=ai(i)
            XOld(1:3,n)=x(1:3,i)
            BetaOld(1:6,n)=beta(1:6,i)
          endif
        enddo
      endif
      if(NAtXYZMode.gt.0) call TrXYZMode(1)
      call EM40CleanAtoms
      NAtXYZMode=0
      NXYZAMode=0
      NAtMagMode=0
      NMagAMode=0
2100  read(ln,FormA,end=9800) Veta
      if(Veta(1:1).ne.'!'.or.Veta.eq.' ') go to 2100
      k=1
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,'Atom')) go to 2100
      read(ln,FormA,end=9800) Veta
      if(Veta(1:1).ne.'!') then
        nl=2
        backspace ln
      else
        k=1
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'beta11')) then
          nl=4
        else
          go to 9800
        endif
      endif
2200  read(ln,FormA,end=9800) Veta
      if(Veta(1:1).eq.'!'.or.Veta.eq.' ') go to 2300
      if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
      kam=NAtIndTo(1,KPhase)+1
      call AtSun(kam,NAtAll,kam+1)
      NAtIndLen(1,KPhase)=NAtIndLen(1,KPhase)+1
      call EM40UpdateAtomLimits
      call SetBasicKeysForAtom(kam)
      if(kam.eq.1) then
        PrvniKiAtomu(kam)=ndoff
      else
        PrvniKiAtomu(kam)=PrvniKiAtomu(kam-1)+DelkaKiAtomu(kam-1)
      endif
      DelkaKiAtomu(kam)=0
      k=0
      call Kus(Veta,k,Atom(kam))
      call Kus(Veta,k,Atp)
      isf(kam)=KtAt(AtType(1,KPhase),NAtFormula(KPhase),Atp)
      if(isf(kam).le.0) go to 9810
      call StToReal(Veta,k,x(1,kam),3,.false.,ich)
      if(ich.ne.0) go to 9820
      call StToReal(Veta,k,xp,2,.false.,ich)
      if(ich.ne.0) go to 9830
      ai(kam)=xp(2)
      call StToInt(Veta,k,ip,4,.false.,ich)
      if(ich.ne.0) go to 9840
      if(ip(3).eq.0) then
        itf(kam)=1
        beta(1,kam)=xp(1)
        do i=1,nl-2
          read(ln,FormA,end=9800) Veta
        enddo
      else if(ip(3).eq.2) then
        itf(kam)=2
        if(nl.ne.4) go to 9850
        do i=1,2
          read(ln,FormA,end=9800) Veta
        enddo
        k=0
        call StToReal(Veta,k,beta(1,kam),6,.false.,ich)
      else
        go to 9850
      endif
      call LocateXWithSymmetry(x(1,kam),XOld,NAtOld,1,.5,ia,isym,ivt,
     1                         xp)
      if(ia.gt.0) then
        ai(kam)=aiOld(ia)
        if(itfOld(ia).gt.1.and.isym.gt.1) then
          itf(kam)=2
          call srotb(rm(1,isym,1,KPhase),rm(1,isym,1,KPhase),xp)
          call MultM(xp,BetaOld(1,ia),Beta(1,kam),6,6,1)
        else
          itf(kam)=1
          beta(1:6,kam)=betaOld(1:6,ia)
        endif
      endif
      read(ln,FormA,end=9800) Veta
      go to 2200
2300  allocate(XYZModePom(3*NAtAll),ipor(3*NAtAll),jpor(3*NAtAll))
      do mm=1,2
        if(mm.eq.2) then
          if(allocated(IAtPom)) deallocate(IAtPom)
          allocate(IAtPom(MXYZAModeMax/3))
          NXYZ=0
        endif
        rewind ln
2310    read(ln,FormA,end=9800) Veta
        if(Veta(1:1).ne.'!'.or.Veta.eq.' ') go to 2310
        k=1
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Nm')) go to 2310
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Atm').and..not.EqIgCase(Cislo,'Atom'))
     1    go to 2310
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Irrep')) go to 2310
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Vx')) go to 2310
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Vy')) go to 2310
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Vz')) go to 2310
        Label=0
2500    read(ln,FormA,end=9800) Veta
        if(Veta(1:1).eq.'!'.or.Veta.eq.' ') then
          Konec=1
          ip(1)=999999
          go to 2510
        else
          Konec=0
        endif
        if(Veta(1:1).eq.'!'.or.Veta.eq.' ') cycle
        k=0
        call StToInt(Veta,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9910
        call Kus(Veta,k,Atp)
        call Kus(Veta,k,Cislo)
        call StToReal(Veta,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 9920
2510    if(ip(1).eq.Label) then
          if(.not.EqIgCase(Cislo,IrrepLabel)) go to 9930
          if(mm.eq.2) then
            iat=KtAt(Atom(NAtIndFr(1,KPhase)),NAtIndLen(1,KPhase),Atp)
            if(iat.le.0) go to 9940
            i=LocateInIntArray(iat,IAtPom,nat)
            if(i.le.0) then
              nat=nat+1
              IAtPom(nat)=iat
            endif
          endif
          n=n+3
        else
          if(Label.ne.0) then
            n=n+2
            if(mm.eq.1) then
              m=1
              do i=1,NXYZAMode
                if(LocateSubstring(LXYZAMode(i),
     1                             IrrepLabel(:idel(IrrepLabel)),
     2                             .false.,.true.).eq.1) m=m+1
                if(n.ne.MXYZAMode(i)) cycle
                if(EqRV(XYZAMode(1,i),XYZModePom,n,.00001)) go to 2550
              enddo
              write(t80,FormI15) m
              call Zhusti(t80)
              call ReallocateXYZAMode(NXYZAMode+1,n)
              NXYZAMode=NXYZAMode+1
              LXYZAMode(NXYZAMode)=IrrepLabel(:idel(IrrepLabel))//'#'//
     1                             t80(:idel(t80))
              MXYZAMode(NXYZAMode)=n
              call CopyVek(XYZModePom,XYZAMode(1,NXYZAMode),n)
            else
              NXYZ=NXYZ+1
              Kam=0
              do m=1,NAtXYZMode
                if(nat.ne.MAtXYZMode(m)) cycle
                do i=1,nat
                  if(IAtPom(i).eq.IAtXYZMode(i,m)) then
                    Kam=m
                    go to 2520
                  endif
                enddo
              enddo
2520          if(Kam.eq.0) then
                call ReallocateAtXYZMode(10)
                NAtXYZMode=NAtXYZMode+1
                Kam=NAtXYZMode
                call CopyVekI(IAtPom(1),IAtXYZMode(1,Kam),nat)
                MAtXYZMode(Kam)=nat
                JAtXYZMode(Kam)=0
              endif
              do m=1,NXYZAMode
                if(n.ne.MXYZAMode(m)) cycle
                if(EqRV(XYZModePom,XYZAMode(1,m),n,.00001)) go to 2530
              enddo
              go to 9950
2530          JAtXYZMode(Kam)=JAtXYZMode(Kam)+1
              NMAtXYZMode(Kam)=JAtXYZMode(Kam)
              LAtXYZMode(JAtXYZMode(Kam),Kam)=LXYZAMode(m)
               AtXYZMode(JAtXYZMode(Kam),Kam)=0.
              SAtXYZMode(JAtXYZMode(Kam),Kam)=0.
              KiAtXYZMode(JAtXYZMode(Kam),Kam)=1
              ipor(NXYZ)=JAtXYZMode(Kam)
              jpor(NXYZ)=Kam
            endif
          endif
2550      if(Konec.eq.1) cycle
          n=1
          IrrepLabel=Cislo
          Label=ip(1)
          if(mm.eq.2) then
            nat=1
            iat=KtAt(Atom(NAtIndFr(1,KPhase)),NAtIndLen(1,KPhase),Atp)
            if(iat.le.0) go to 9940
            IAtPom(1)=iat
          endif
        endif
        call CopyVek(xp,XYZModePom(n),3)
        go to 2500
      enddo
2600  rewind ln
2610  read(ln,FormA,end=2800) Veta
      if(Veta(1:1).ne.'!'.or.Veta.eq.' ') go to 2610
      k=1
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,'Amplitudes')) go to 2610
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,'of')) go to 2610
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,'Symmetry')) go to 2610
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,'Modes')) go to 2610
      n=1
      m=1
      read(ln,FormA,end=2800) Veta
      k=0
      call Kus(Veta,k,Cislo)
      NXYZ=0
      if(.not.EqIgCase(Cislo,'A_MODES')) go to 2630
2620  read(ln,FormA,end=2800) Veta
2630  if(Veta(1:1).eq.'!'.or.Veta.eq.' ') go to 2800
      k=0
      call Kus(Veta,k,Cislo)
      call StToReal(Veta,k,xp,1,.false.,ich)
      if(ich.ne.0) go to 9960
      NXYZ=NXYZ+1
      AtXYZMode(ipor(NXYZ),jpor(NXYZ))=xp(1)
      m=m+1
      if(m.gt.NMAtXYZMode(n)) then
        m=1
        n=n+1
      endif
      go to 2620
2800  call SetAtXYZMode
      do n=1,NXYZAMode
        pom=0.
        do i=1,MXYZAMode(n)
          pom=max(pom,abs(XYZAMode(i,n)))
        enddo
        do i=1,MXYZAMode(n)
          XYZAMode(i,n)=XYZAMode(i,n)/pom
        enddo
      enddo
      if(NAtXYZMode.gt.0) then
        call TrXYZMode(0)
        pom=0.
        fn=0
        do k=1,2
          do i=1,NAtXYZMode
            do j=1,NMAtXYZMode(i)
              if(k.eq.1) then
                pom=pom+abs(AtXYZMode(j,i))
                fn=fn+1.
              else
                AtXYZMode(j,i)=pom
              endif
            enddo
          enddo
          if(pom/fn.gt..001) then
            exit
          else
            pom=.01
          endif
        enddo
      endif
      call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
      go to 9999
9800  t80='syntactic error on the input file.'
      go to 9990
9810  t80='form factor "'//Atp(:idel(Atp))//
     1     '" has not been defined in the formula.'
      go to 9990
9820  t80='during import of coordinates for the atom "'//
     1     Atom(kam)(:idel(Atom(kam)))//'".'
      go to 9990
9830  t80='during import of Biso and occupancy for the atom "'//
     1     Atom(kam)(:idel(Atom(kam)))//'".'
      go to 9990
9840  t80='during import of integer flags for the atom "'//
     1     Atom(kam)(:idel(Atom(kam)))//'".'
      go to 9990
9850  t80='the ADP flag for the atom "'//
     1     Atom(kam)(:idel(Atom(kam)))//
     2     '" is not consistent with the label information.'
      go to 9990
9860  t80='the ADP flag for the atom "'//
     1     Atom(kam)(:idel(Atom(kam)))//
     2     '" is not consistent with the label information.'
      go to 9990
9910  t80='incorrect amplimode label.'
      go to 9990
9920  t80='during reading of amplimode coefficients.'
      go to 9990
9930  t80='inconsistent irrep label.'
      go to 9990
9940  t80='unknown atomic label.'
      go to 9990
9950  t80='unexpected error - could not find atomic orbit.'
      go to 9990
9960  t80='during reading of the amplitude'
      go to 9990
9990  call FeChybne(-1.,-1.,t80,Veta,SeriousError)
      call CopyFile(PreviousM40,fln(:ifln)//'.m40')
      call DeleteFile(PreviousM40)
      call CopyFile(PreviousM50,fln(:ifln)//'.m50')
      call DeleteFile(PreviousM50)
      ich=1
9999  if(IFile.eq.0) then
        call DeleteFile(FileName)
        call FeTmpFilesClear(FileName)
      endif
      if(allocated(XYZModePom)) deallocate(XYZModePom,ipor,jpor)
      if(allocated(IAtPom)) deallocate(IAtPom)
      if(allocated(isfOld)) deallocate(isfOld,itfOld,aiOld,XOld,BetaOld)
      call CloseIfOpened(ln)
      return
      end
