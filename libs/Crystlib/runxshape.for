      subroutine RunXShape
      use Basic_mod
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension DFaceN(4,mxface)
      character*150 Veta
      character*15 isp(6)
      logical Carka,FeYesNo,eqrv,lpom
      if(AmiUsed.le.0.) then
        call FeChybne(-1.,-1.,'absorption coefficient can''t be '//
     1                'determined.',' ',SeriousError)
        go to 9999
      endif
      lpom=.true.
      call DRReflForAbsCorrSetting(0,lpom,ich)
      if(ich.ne.0) go to 9900
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.crs','formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      write(ln,'(''TITL *** file transported from datred ***'')')
      write(Veta,'(f9.6,3f10.4,3f10.2)') LamAveRefBlock(KRefBlock),
     1                                  (CellRefBlock(i,0),i=1,6)
      k=0
      call WriteLabeledRecord(ln,'CELL',Veta,k)
      write(Veta,'(i2)') -1
      k=0
      call WriteLabeledRecord(ln,'LATT',Veta,k)
      do i=1,NSymmN(KPhase)
        call CodeSymm(rm6(1,i,1,1),s6(1,i,1,1),isp,0)
        Veta=isp(1)//isp(2)//isp(3)
        carka=.true.
        do l=1,idel(Veta)
          if(Veta(l:l).eq.' ') then
            if(.not.carka) then
              Veta(l:l)=','
              carka=.true.
            endif
          else
            carka=.false.
          endif
        enddo
        call zhusti(Veta)
        write(ln,'(''SYMM '',80a1)')(Veta(l:l),l=1,idel(Veta))
      enddo
      if(NAtFormula(KPhase).gt.0) then
        Veta='SFAC'
        k=6
        i=0
2150    i=i+1
        Veta=Veta(:k)//AtType(i,KPhase)
        if(i.lt.NAtFormula(KPhase)) then
          k=idel(Veta)+1
          go to 2150
        endif
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(15f10.3)')(AtMult(i,KPhase)*float(NUnits(KPhase)),
     1                          i=1,NAtFormula(KPhase))
        k=0
        call WriteLabeledRecord(ln,'UNIT',Veta,k)
      endif
      write(Veta,'(2f10.2)') 0.5,AMiUsed*10.
      k=0
      call WriteLabeledRecord(ln,'ABSC',Veta,k)
      do i=1,NFaces(KRefBlock)
        write(Veta,'(4f12.5)')(DFace(j,i,KRefBlock),j=1,4)
        k=0
        call WriteLabeledRecord(ln,'FACE',Veta,k)
      enddo
      write(ln,'(''END'')')
      call CloseIfOpened(ln)
      if(ExistM50) call iom50(0,0,fln(:ifln)//'.m50')
      if(ExistM95) then
        NFacesN=NFaces(KRefBlock)
        call CopyVek(DFace(1,1,KRefBlock),DFaceN,4*NFacesN)
        call iom95(0,fln(:ifln)//'.m95')
        NFaces(KRefBlock)=NFacesn
        call CopyVek(DFaceN,DFace(1,1,KRefBlock),4*NFacesN)
      endif
      if(ich.gt.0) go to 9900
      call FeOsVariable('WINDIR',Veta)
      call OpenFile(ln,Veta(:idel(Veta))//'\stoe.ini','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9900
      write(ln,'(''[X-SHAPE]'')')
      call FeGetCurrentDir
      Veta='LastFile='//CurrentDir(:idel(CurrentDir))//fln(:ifln)//
     1      '.crs'
      write(ln,FormA) Veta(:idel(Veta))
      call CloseIfOpened(ln)
      Ninfo=7
      TextInfo(1)='Jana2006 will now start X-shape to allow you '//
     1            'optimize and/or modify your'
      TextInfo(2)='crystal shape. The crystal data were written to '//
     1            'the file "'//fln(:ifln)//'.crs".'
      TextInfo(3)='You should open this file in X-shape and it will '//
     1            'be rewritten when the'
      TextInfo(4)='save command in X-shape command is applied.'
      TextInfo(5)='                          IMPORTANT WARNING !!!'
      TextInfo(6)='X-shape will not ask you before closing if you '//
     1            'want to save made'
      TextInfo(7)='changes. You have to do it youself before closing.'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      call FeSystemCommand(CallXShape,0)
      call FeWaitInfo('end of the graphic visualization.')
      call OpenFile(ln,fln(:ifln)//'.crs','formatted','unknown')
      if(ErrFlag.ne.0) go to 3300
      NFacesn=0
3000  read(ln,FormA80,end=3300) Veta
      if(Veta.eq.'end') go to 3300
      i=LocateSubstring(Veta,'face',.false.,.true.)
      if(i.eq.1) then
        NFacesn=NFacesn+1
        k=5
        call StToReal(Veta,k,dfacen(1,NFacesn),4,.false.,ich)
        if(ich.ne.0) NFacesn=NFacesn-1
      endif
      go to 3000
3300  call CloseIfOpened(ln)
      call DeleteFile(fln(:ifln)//'.crs')
      if(NFacesn.eq.NFaces(KRefBlock)) then
        do i=1,NFaces(KRefBlock)
          if(.not.eqrv(DFace(1,i,KRefBlock),dfacen(1,i),3,.5).or.
     1       abs(DFace(4,i,KRefBlock)-dfacen(4,i)).gt..0001) go to 3320
        enddo
      else
        go to 3320
      endif
      Ninfo=1
      TextInfo(1)='The shape was not changed, using the old one'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      go to 9900
3320  if(.not.FeYesNo(-1.,-1.,'Do you want to use faces from X-shape?',
     1                0)) then
        go to 9900
      else
        NFaces(KRefBlock)=NFacesn
        call CopyVek(DFacen,DFace(1,1,KRefBlock),NFaces(KRefBlock)*4)
      endif
9900  call DeleteFile(fln(:ifln)//'.eqv')
      if(no.gt.1) call DeleteFile(fln(:ifln)//'.psi')
      call DeleteFile(fln(:ifln)//'.crs')
      call DeleteFile(fln(:ifln)//'.crb')
9999  return
      end
