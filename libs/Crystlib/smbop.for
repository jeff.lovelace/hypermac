      subroutine SmbOp(rm6p,s6p,isw,znak,t3,t2m,io,nn,det)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(9),rmi(9),mr(9),s6p(*),rm6p(*),io(3),iop(3),
     1          xp(3),rs(9),rp(9),rpp(9),re(9),xpsa(1),xpma(1),
     2          xpp0(3),xpp(4),xpi(3),xpps(3),x1(3),x2(3),x3(3),
     3          xr(3)
      character*(*) t3,t2m
      logical eqiv,eqrv,EqIV0
      equivalence (xps,xpsa),(xpm,xpma)
      k=0
      stopa=0.
      do i=1,3
        do j=1,3
          k=k+1
          rmp(k)=znak*rm6p(j+(i-1)*NDim(KPhase))
          if(i.eq.j) stopa=stopa+rmp(k)
          mr(k)=nint(rmp(k))
          ijk=1
          if(abs(float(mr(k))-rmp(k)).gt..01) go to 9000
        enddo
      enddo
      call matinv(rmp,rmi,det,3)
      ijk=2
      if(abs(det).lt..001) go to 9000
      nn=0
      call SetRealArrayTo(rs,9,0.)
      call UnitMat(re,3)
      call CopyMat(rmp,rp,3)
1020  nn=nn+1
      call AddVek(rs,rp,rs,9)
      call CopyVek(rp,rpp,9)
      if(.not.eqrv(rp,re,9,.0001)) then
        call multm(rpp,rmp,rp,3,3,3)
        go to 1020
      endif
      do i=1,9
        rs(i)=rs(i)/float(nn)
      enddo
      do i=0,4
        io(1)=i
        if(i.eq.0) then
          jp=0
        else
          jp=-4
        endif
        do j=jp,4
          io(2)=j
          if(i.eq.0.and.j.eq.0) then
            kp=0
          else
            kp=-4
          endif
          do 1060k=kp,4
            l=0
            io(3)=k
            do kk=1,3
              if(io(kk).eq.0.or.iabs(io(kk)).eq.2) l=l+1
            enddo
            if(l.eq.3) go to 1060
            call multmi(mr,io,iop,3,3,1)
            if(det.lt.0.) then
              do kk=1,3
                iop(kk)=-iop(kk)
              enddo
            endif
            if(eqiv(io,iop,3)) go to 1300
1060      continue
        enddo
      enddo
      call SetIntArrayTo(io,3,0)
1300  t3='?'
      ixpm=999999
      nnulm=999999
      knulm=999999
      do i0=1,NLattVec(KPhase)
        call AddVek(s6p,vt6(1,i0,isw,KPhase),xpp0,3)
        do i3=-1,1
          xpp(3)=xpp0(3)+float(i3)
          do i2=-1,1
            xpp(2)=xpp0(2)+float(i2)
            do 1100i1=-1,1
              xpp(1)=xpp0(1)+float(i1)
              call multm(rs,xpp,xpi,3,3,1)
              call od0do1(xpi,xpi,3)
              do i=1,3
                if(abs(xpi(i)).lt..1.and.abs(xpi(i)).gt..01) go to 1100
              enddo
              pom=0.
              nnul=0
              do i=1,3
                axpi=abs(xpi(i))
                pom=pom+axpi
                if(axpi.lt..0001) then
                  nnul=nnul+1
                  knul=i
                endif
              enddo
              ipom=nint(pom*96.)
              if(nn.eq.2.and.ixpm.eq.48.and.ipom.ne.0) cycle
              if(ipom.lt.ixpm.or.nnul.gt.nnulm.or.
     1           (nnul.eq.nnulm.and.nnul.eq.2.and.knul.lt.knulm)) then
                ixpm=ipom
                call CopyVek(xpi,xp,3)
                call CopyVek(xpp,xpps,3)
                i0m=i0
                nnulm=nnul
                knulm=knul
                if(ixpm.eq.0) go to 1350
              endif
1100        continue
          enddo
        enddo
      enddo
1350  xpm=float(ixpm)/96.
      xps=0.
      do i=1,3
        xps=xps+xp(i)*float(iop(i))
      enddo
      RotAngle=0.
      if(.not.EqIV0(io,3)) then
        do i=1,3
          x1(i)=io(i)
        enddo
        call multm(MetTens(1,isw,KPhase),x1,xr,3,3,1)
        x1x1=sqrt(scalmul(x1,xr))
        do i3=0,2
          x2(3)=x1(3)+float(i3)
          do i2=0,2
            x2(2)=x1(2)+float(i2)
            do i1=0,2
              x2(1)=x1(1)+float(i1)
              call multm(MetTens(1,isw,KPhase),x2,xr,3,3,1)
              x2x2=sqrt(scalmul(x2,xr))
              if(x2x2.lt..1) cycle
              x1x2=scalmul(x1,xr)
              if(abs(x1x2/(x1x1*x2x2)).gt..9) cycle
              pom=-x1x2/x1x1**2
              do i=1,3
                x2(i)=pom*x1(i)+x2(i)
              enddo
              call multm(MetTens(1,isw,KPhase),x2,xr,3,3,1)
              x1x2=scalmul(x1,xr)
              call multm(MetTens(1,isw,KPhase),x2,xr,3,3,1)
              x2x2=sqrt(scalmul(x2,xr))
              do i=1,3
                x2(i)=x2(i)/x2x2
              enddo
              call MultM(rmp,x2,x3,3,3,1)
              call multm(MetTens(1,isw,KPhase),x3,xr,3,3,1)
              x3x3=sqrt(scalmul(x3,xr))
              do i=1,3
                x3(i)=x3(i)/x3x3
              enddo
              call multm(MetTens(1,isw,KPhase),x3,xr,3,3,1)
              pom=scalmul(x2,xr)
              if(pom.gt..99) then
                RotAngle=0.
              else if(pom.lt.-.99) then
                RotAngle=180.
              else
                RotAngle=acos(scalmul(x2,xr))/ToRad
              endif
              call VecMul(x2,x3,xr)
              pom=0.
              do i=1,3
                pom=pom+xr(i)*x1(i)
              enddo
              if(pom.lt.0.) RotAngle=-RotAngle
            enddo
          enddo
        enddo
      endif
      if(nn.eq.1) then
        t3='1'
        call SetIntArrayTo(io,3,0)
      else if(nn.eq.2) then
        if(det.gt.0.) then
          t3='2'
          if(abs(xps-.5).lt..01) t3(2:2)='1'
        else
          if(stopa.lt.-2.99) then
            t3='-1'
            call SetIntArrayTo(io,3,0)
          else if(xpm.lt..01) then
            t3='m'
          else
            ionn=0
            ion0=0
            ion1=0
            io0=0
            io1=0
            do i=1,3
              if(io(i).eq.0) then
                ion0=ion0+1
                io0=i
              else
                ionn=ionn+1
                if(iabs(io(i)).eq.1) then
                  ion1=ion1+1
                  if(io1.eq.0) io1=i
                endif
              endif
            enddo
            npul=0
            kpul=0
            nnul=0
            knul=0
            nctvrt=0
            kctvrt=0
            if(ionn.eq.0) then
              ijk=3
              go to 9000
            else if(ionn.eq.1.or.ionn.eq.2) then
              do i=1,3
                pom=abs(xp(i))
                if(abs(pom-.5).lt..01) then
                  npul=npul+1
                  kpul=i
                else if(abs(pom).lt..01) then
                  nnul=nnul+1
                  knul=i
                else if(abs(abs(pom-.5)-.25).lt..01) then
                  nctvrt=nctvrt+1
                  kctvrt=i
                endif
              enddo
            else
              ijk=4
              go to 9000
            endif
            if(nnul.eq.3) then
              t3='m'
            else if(npul.eq.1.and.nnul.eq.2) then
              t3=SmbABC(kpul)
            else if(npul.eq.2.and.nnul.eq.1) then
              t3='n'
            else if(nctvrt.eq.2.and.nnul.eq.1) then
              if(ion1.eq.1) then
                t3='d'
              else if(ion1.eq.2) then
                t3='g1'
              else
                ijk=5
                go to 9000
              endif
            else if(nctvrt.eq.3.and.ion1.eq.2) then
              t3='d'
            else if(nctvrt.eq.2.and.npul.eq.1) then
              if(ion1.eq.2) then
                t3='g2'
              else
                ijk=6
                go to 9000
              endif
            else
              ijk=7
              go to 9000
            endif
          endif
        endif
      else if(nn.eq.4) then
        if(det.gt.0.) then
          ior=KeyOr(io)
          if(abs(xps).gt..01.and.((ior.eq.1.and.mr(8).eq.1).or.
     1       (ior.eq.2.and.mr(3).eq.1).or.(ior.eq.3.and.mr(4).eq.1)))
     2       xps=1.-xps
          if(abs(xps).lt..01) then
            t3='4'
          else if(abs(xps-.25).lt..01) then
            t3='41'
          else if(abs(xps-.5).lt..01) then
            t3='42'
          else if(abs(xps-.75).lt..01) then
            t3='43'
          else
            t3='4?'
          endif
        else
          t3='-4'
        endif
        if(RotAngle.lt.0.) t3=t3(:idel(t3))//'-'
      else if(nn.eq.3) then
        if(det.gt.0.) then
          ior=KeyOr(io)
          if(abs(xps).gt..01) then
            j=0
            k=mod(ior+1,3)+1
            do i=1,3
              j=j+mr(k)
              k=k+3
            enddo
            if(j.ne.0) xps=1.-xps
          endif
          if(abs(xps).lt..01) then
            t3='3'
          else if(abs(xps-.333333).lt..01) then
            t3='31'
          else if(abs(xps-.666667).lt..01) then
            t3='32'
          else
            t3='3d'
          endif
        else
          t3='-3'
        endif
        if(RotAngle.lt.0.) t3=t3(:idel(t3))//'-'
      else if(nn.eq.6) then
        if(det.gt.0.) then
          ior=KeyOr(io)
          if(abs(xps).gt..01) then
            j=0
            k=mod(ior+1,3)+1
            do i=1,3
              j=j+mr(k)
              k=k+3
            enddo
            if(j.eq.0) xps=1.-xps
          endif
          if(abs(xps).lt..01) then
            t3='6'
          else if(abs(xps-.166667).lt..01) then
            t3='61'
          else if(abs(xps-.333333).lt..01) then
            t3='62'
          else if(abs(xps-.5).lt..01) then
            t3='63'
          else if(abs(xps-.666667).lt..01) then
            t3='64'
          else if(abs(xps-.833333).lt..01) then
            t3='65'
          else
            t3='3?'
          endif
        else
          if(abs(stopa).lt..1) then
            t3='-3'
          else
            t3='-6'
          endif
        endif
        if(RotAngle.lt.0) t3=t3(:idel(t3))//'-'
      endif
      if(NDim(KPhase).eq.4) then
         if(znak*rm6p(16).lt.0.) then
           t2m='0'
         else
           call AddVek(s6p,vt6(1,i0m,isw,KPhase),xpp,4)
           xps=xpp(4)-scalmul(quir(1,1,KPhase),xpp)
           call od0do1(xpsa,xpsa,1)
           if(abs(xps).lt..01) then
             t2m='0'
           else if(abs(xps-.5).lt..01) then
             t2m='s'
           else if(abs(xps-.333333).lt..01) then
             t2m='t'
           else if(abs(xps-.666667).lt..01) then
             t2m='-t'
           else if(abs(xps-.25).lt..01) then
             t2m='q'
           else if(abs(xps-.75).lt..01) then
             t2m='-q'
           else if(abs(xps-.1666667).lt..01) then
             t2m='h'
           else if(abs(xps-.8333333).lt..01) then
             t2m='-h'
           endif
         endif
      else
        t2m=' '
      endif
      go to 9999
9000  t3='?'
      t2m='?'
9999  return
      end
