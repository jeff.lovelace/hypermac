      subroutine FindSmbSg(Grp,ChangeOrder,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Grp
      character*256 Veta
      character*60  GrupaO,GrPom,GrPomMag
      character*20  Grp4
      character*8   GrupaI
      character*6   mirror,SmbMagCentr
      character*4   ABC
      character*3   iosy,imirr
      character*3, allocatable :: t3(:)
      character*2, allocatable :: t2m(:)
      character*1, allocatable :: tm(:)
      logical eqrv,ChangeOrder,EqIgCase,PrvniGrp,FindOriginShift,
     1        TestDiagonal,EqRV0
      logical, allocatable :: Diagonal(:)
      integer CrlCentroSymm,CrlMagInver,CrlMagCenter,TauSymm(5)
      integer, allocatable :: io(:,:),ipor(:),jvtp(:)
      dimension kosy(3),s6p(6),ShSgP(6),rmp(36),sp(6),rmps(36),
     1          RefCond(7),RefCondS(7),rx(72),px(12),rxo(72),pxo(12),
     2          kmirr(3),QSymm(3,3),Tau(5)
      real, allocatable :: rm6s(:,:),rms(:,:),s6s(:,:),zmags(:)
      data ABC/'ABCI'/,mirror/'mabcnd'/
      FindOriginShift=.true.
      go to 1000
      entry FindSmbSgOrgShiftNo(Grp,ChangeOrder,isw)
      FindOriginShift=.false.
1000  NDim2=2*NDim(KPhase)
      nss=NSymm(KPhase)
      if(MagneticType(KPhase).gt.0) then
        invm=CrlMagInver()
        invc=CrlMagCenter()
      else
        invm=0
        invc=0
      endif
      if(NDimI(KPhase).eq.0) then
        if(invm.gt.0) then
          nsno=nss/2
        else
          nsno=nss
        endif
        if(invm.gt.0.or.invc.gt.0) then
          nsns=nss/2
        else
          nsns=nss
        endif
      else
        nsns=NSymmN(KPhase)
        nsno=NSymmN(KPhase)
        if(invc.gt.0) then
          nsns=NSymmN(KPhase)/2
          nsno=NSymmN(KPhase)/2
        endif
        call GetQiQr(qu(1,1,isw,KPhase),qui(1,isw,KPhase),
     1               quir(1,isw,KPhase),NDimI(KPhase),isw)
      endif
      nvts=NLattVec(KPhase)
      KCentroSymm=CrlCentroSymm()
      if(KCentroSymm.gt.0) then
        do j=1,NDim(KPhase)
          ShSgP(j)=-s6(j,KCentroSymm,isw,KPhase)*.5
          if(ShSgP(j).lt.-.00001) ShSgP(j)=ShSgP(j)+.5
        enddo
      else
        call SetRealArrayTo(ShSgP,NDim(KPhase),0.)
      endif
      k=0
      allocate(rm6s(NDimQ(KPhase),NSymm(KPhase)),rms(9,NSymm(KPhase)),
     1         s6s(NDim(KPhase),NSymm(KPhase)),zmags(NSymm(KPhase)),
     2         ipor(NSymm(KPhase)))
      do i=1,NSymm(KPhase)
        call CopyMat (rm6(1,i,isw,KPhase),rm6s(1,i),NDim(KPhase))
        call CopyMat (rm(1,i,isw,KPhase),rms(1,i),3)
        call CopyVek(s6(1,i,isw,KPhase),s6s(1,i),NDim(KPhase))
        zmags(i)=zmag(i,isw,KPhase)
      enddo
      do i=1,nsns
        ipor(i)=nsns+1
      enddo
1090  Grp=' '
      Grp4=' '
      SmbMagCentr=' '
      call FindSmbCentr(Grp(1:1),isw)
      Lattice(KPhase)=Grp(1:1)
      iosy='000'
      imirr='000'
      i6=0
      i4=0
      i3=0
      jold=0
      call SetIntArrayTo(kosy,3,0)
      call SetIntArrayTo(kmirr,3,0)
      nosy=0
      nmirr=0
      CrSystem(KPhase)=0
      Monoclinic(KPhase)=0
      n=nsns
      allocate(t3(n),t2m(n),tm(n),io(3,n),Diagonal(n))
      do i=1,nsns
        call CopyVek(s6(1,i,isw,KPhase),s6p,NDim(KPhase))
        ZMagP=ZMag(i,isw,KPhase)
        if(invm.gt.0) then
          if(ZMagP.lt.0.) then
            ZMagP=1.
            s6p(4)=s6p(4)+s6(4,invm,isw,KPhase)
            call od0do1(s6p(4),s6p(4),1)
          endif
        endif
        call SmbOp(rm6(1,i,isw,KPhase),s6p,isw,1.,t3(i),t2m(i),io(1,i),
     1             nn,pom)
        idl=idel(t3(i))
        Diagonal(i)=TestDiagonal(rm6(1,i,isw,KPhase),NDim(KPhase))
        ijk=1
        if(t3(i).eq.'?') go to 8000
        if(MagneticType(KPhase).gt.0) then
          if(ZMagP.gt.0.) then
            tm(i)=' '
          else
            tm(i)=''''
          endif
        endif
        j=KeyOr(io(1,i))
        if(j.ge.1.and.j.le.3) then
          if(t3(i)(1:1).eq.'2') then
            iosy(j:j)='1'
            kosy(j)=i
            nosy=j
          else if(index(mirror,t3(i)(1:1)).gt.0) then
            imirr(j:j)='1'
            kmirr(j)=i
            nmirr=j
          endif
        endif
        if(((t3(i)(1:1).eq. '4'.and.i4.le.0).or.
     1      (t3(i)(1:2).eq.'-4'.and.i4.eq.0)).and.
     2      t3(i)(idl:idl).ne.'-') then
          if(CrSystem(KPhase).ne.CrSystemCubic)
     1      CrSystem(KPhase)=CrSystemTetragonal
          i4=i
          if(t3(i)(1:2).eq.'-4') i4=-i4
        endif
        if((t3(i)(1:1).eq.'3'.or.t3(i).eq.'-3').and.i6.eq.0.and.
     1     t3(i)(idl:idl).ne.'-') then
          if(j.ge.1.and.j.le.3) then
            if(CrSystem(KPhase).ne.CrSystemCubic)
     1        CrSystem(KPhase)=CrSystemTrigonal
            if(i3.eq.0) i3=i
          else if(j.ge.6.or.j.le.9) then
            if(CrSystem(KPhase).eq.-CrSystemTrigonal) then
              if(j.ne.jold) CrSystem(KPhase)=CrSystemCubic
            else if(CrSystem(KPhase).ne.CrSystemCubic) then
              CrSystem(KPhase)=-CrSystemTrigonal
              jold=j
            endif
            if(i3.eq.0.and.j.eq.6) i3=i
          endif
        endif
        if(((t3(i)(1:1).eq. '6'.and.i6.le.0).or.
     1      (t3(i)(1:2).eq.'-6'.and.i6.eq.0)).and.t3(i)(idl:idl).ne.'-')
     2    then
          if(CrSystem(KPhase).ne.CrSystemCubic)
     1      CrSystem(KPhase)=CrSystemHexagonal
          i6=i
          if(t3(i)(1:2).eq.'-6') i6=-i6
        endif
      enddo
      NTau=0
      NOsyMirr=0
      do i=1,3
        if(iosy(i:i).eq.'1'.or.imirr(i:i).eq.'1') NOsyMirr=NOsyMirr+1
      enddo
      if(CrSystem(KPhase).eq.0) then
        if(NOsyMirr.eq.0) then
          if(nsns.gt.2.or.(nsns.eq.2.and.KCentroSymm.le.0)) then
            ijk=2
            go to 8000
          else
            CrSystem(KPhase)=CrSystemTriclinic
          endif
        else if(NOsyMirr.eq.3) then
          CrSystem(KPhase)=CrSystemOrthorhombic
        else
          CrSystem(KPhase)=CrSystemMonoclinic
        endif
      endif
      if(CrSystem(KPhase).eq.CrSystemTriclinic) then
        if(nsns.le.2) then
          if(KCentroSymm.gt.0) then
            Grp=Grp(1:1)//'-1'
            if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(2)
            if(NDimI(KPhase).eq.1) then
              NTau=NTau+1
              Tau(NTau)=s6(4,2,isw,KPhase)
              TauSymm(NTau)=2
              Grp4='0'
            endif
          else
            Grp=Grp(1:1)//'1'
            if(NDimI(KPhase).eq.1) then
              NTau=NTau+1
              Tau(NTau)=s6(4,1,isw,KPhase)
              TauSymm(NTau)=1
              Grp4='0'
            endif
          endif
        else
          ijk=3
          go to 8000
        endif
      else if(CrSystem(KPhase).eq.CrSystemMonoclinic) then
        i=0
        j=0
        if(nosy.gt.0) i=kosy(nosy)
        if(nmirr.gt.0) j=kmirr(nmirr)
        if(KCentroSymm.gt.0) then
          ijk=4
          if(i.eq.0.or.j.eq.0) go to 8000
          Grp=Grp(1:1)//t3(i)(1:idel(t3(i)))
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
          Grp=Grp(:idel(Grp))//'/'//t3(j)(1:idel(t3(j)))
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(j)
          if(NDimI(KPhase).eq.1) then
            Grp4=t2m(i)(:idel(t2m(i)))//t2m(j)(:idel(t2m(j)))
            NTau=NTau+1
            Tau(NTau)=s6(4,i,isw,KPhase)
            TauSymm(NTau)=i
            NTau=NTau+1
            Tau(NTau)=s6(4,j,isw,KPhase)
            TauSymm(NTau)=j
          endif
        else
          ijk=5
          if(i.eq.0.and.j.eq.0) go to 8000
          j=max(i,j)
          Grp=Grp(1:1)//t3(j)(:idel(t3(j)))
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(j)
          if(NDimI(KPhase).eq.1) then
            Grp4=t2m(j)
            NTau=NTau+1
            Tau(NTau)=s6(4,j,isw,KPhase)
            TauSymm(NTau)=j
          endif
        endif
        i=max(nosy,nmirr)
        CrSystem(KPhase)=10*i+CrSystemMonoclinic
        Monoclinic(KPhase)=i
!        call CheckSystem(CellPar(1,isw,KPhase),i,j)
!        ijk=6
!        if(i.ne.0.and.i.ne.Monoclinic(KPhase)) go to 8000
        ijk=7
        if(index(Grp,'d').gt.0) go to 8000
        do i=1,nsns
          ijk=8
          if(.not.Diagonal(i)) go to 8000
        enddo
      else if(CrSystem(KPhase).eq.CrSystemOrthorhombic) then
        n2=0
        NTau=0
        do i=1,3
          j=kmirr(i)
          if(j.le.0) j=kosy(i)
          Grp=Grp(:idel(Grp))//t3(j)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(j)
          if(NDimI(KPhase).eq.1) then
            NTau=NTau+1
            Grp4=Grp4(:idel(Grp4))//t2m(j)(:idel(t2m(j)))
            Tau(NTau)=s6(4,j,isw,KPhase)
            TauSymm(NTau)=j
          endif
        enddo
        j=0
        GrPom=' '
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            GrPom(j:j)=Grp(i:i)
          endif
        enddo
        idl=idel(GrPom)
        if(GrPom.eq.'I222') then
          nx=1
          do i=1,4
            if(rm6(1,i,isw,KPhase).lt.0.) then
              call CopyVek(s6(1,i,isw,KPhase),px(nx),3)
              if((rm6(NDim(KPhase)+2,i,isw,KPhase).gt.0..and.
     1            abs(s6(2,i,isw,KPhase)).gt..1).or.
     2           (rm6(2*NDim(KPhase)+3,i,isw,KPhase).gt.0..and.
     3            abs(s6(3,i,isw,KPhase)).gt..1))
     4          call AddVek(vt6(1,2,isw,KPhase),px(nx),px(nx),3)
              call od0do1(px(nx),px(nx),3)
              if(nx.gt.1) then
                if(abs(px(1)-px(4)).gt..01) then
                  GrPom=Grp
                  j=0
                  Grp=' '
                  do k=1,idel(GrPom)
                    j=j+1
                    if(GrPom(k:k).eq.'2') then
                      Grp(j:j+1)='21'
                      j=j+1
                    else
                      Grp(j:j)=GrPom(k:k)
                    endif
                  enddo
                  go to 2050
                endif
              else
                nx=nx+3
              endif
            endif
          enddo
        else
          call SpGroupAlias(GrPom)
        endif
        k=2
        j=idel(GrPom)
        if(j.eq.4) then
          if(GrPom(1:1).eq.'A') then
            if(GrPom(2:2).eq.'b'.or.GrPom(2:2).eq.'c') GrPom(2:2)='e'
          else if(GrPom(1:1).eq.'B') then
            if(GrPom(3:3).eq.'a'.or.GrPom(3:3).eq.'c') GrPom(3:3)='e'
          else if(GrPom(1:1).eq.'C') then
            if(GrPom(4:4).eq.'a'.or.GrPom(4:4).eq.'b') GrPom(4:4)='e'
          endif
        endif
        j=0
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            Grp(i:i)=GrPom(j:j)
          endif
        enddo
        do i=1,nsns
          ijk=9
          if(.not.Diagonal(i)) go to 8000
        enddo
      else if(CrSystem(KPhase).eq.CrSystemTetragonal) then
        if(EqIgCase(Grp(1:1),'F').or.EqIgCase(Grp(1:1),'A').or.
     1     EqIgCase(Grp(1:1),'B').or.EqIgCase(Grp(1:1),'C')) then
          ijk=10
          go to 8000
        endif
        Grp=Grp(1:1)//t3(iabs(i4))
        if(NDimI(KPhase).eq.1) then
          Grp4=t2m(iabs(i4))
          NTau=NTau+1
          Tau(NTau)=s6(4,iabs(i4),isw,KPhase)
          TauSymm(NTau)=iabs(i4)
        endif
        if(Grp(1:1).eq.'I') then
          if(Grp(2:3).eq.'43') then
            Grp(2:3)='41'
          else if(Grp(2:3).eq.'42') then
            Grp(2:2)='4'
          endif
        endif
        if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(iabs(i4))
        if(KCentroSymm.gt.0) then
          ior=KeyOr(io(1,iabs(i4)))
          do i=1,nsns
            if(index(mirror,t3(i)(1:1)).gt.0.and.KeyOr(io(1,i)).eq.ior)
     1        go to 1650
          enddo
          ijk=11
          go to 8000
1650      if(Grp(1:1).eq.'I'.and.t3(i).eq.'b') t3(i)='a'
          Grp=Grp(:idel(Grp))//'/'//t3(i)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
          if(NDimI(KPhase).eq.1) then
            Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
            NTau=NTau+1
            Tau(NTau)=s6(4,i,isw,KPhase)
            TauSymm(NTau)=i
          endif
        endif
        if(NOsyMirr.eq.3) then
          i=kmirr(1)
          if(i.le.0) i=kosy(1)
          if(i.gt.0) then
            if(Grp(1:1).eq.'I'.and.t3(i).eq.'b') t3(i)='c'
            Grp=Grp(1:idel(Grp))//t3(i)
            if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
            if(NDimI(KPhase).eq.1) then
              Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
              NTau=NTau+1
              Tau(NTau)=s6(4,i,isw,KPhase)
              TauSymm(NTau)=i
            endif
            do i=1,nsns
              if(KCentroSymm.gt.0.and.
     1            index(mirror,t3(i)(1:1)).le.0) cycle
              if(Grp(1:1).eq.'I'.and.t3(i).eq.'c') t3(i)='m'
              if(KeyOr(io(1,i)).eq.4.and.t3(i).ne.'n') go to 2000
            enddo
          endif
          ijk=12
          go to 8000
        endif
      else if(CrSystem(KPhase).eq.CrSystemTrigonal) then
        if(KCentroSymm.gt.0) t3(i3)='-3'
        Grp=Grp(1:1)//t3(i3)
        if(MagneticType(KPhase).gt.0) then
          if(KCentroSymm.gt.0) then
            Grp=Grp(:idel(Grp))//tm(KCentroSymm)
          else
            Grp=Grp(:idel(Grp))//tm(i3)
          endif
        endif
        if(NDimI(KPhase).eq.1) then
          Grp4=t2m(i3)
          NTau=NTau+1
          Tau(NTau)=s6(4,i3,isw,KPhase)
          TauSymm(NTau)=i3
        endif
        if(NOsyMirr.ne.0) then
          i=kmirr(1)
          if(i.le.0) i=kosy(1)
          Grp=Grp(:idel(Grp))//t3(i)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
          if(NDimI(KPhase).eq.1) then
            Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
            NTau=NTau+1
            Tau(NTau)=s6(4,i,isw,KPhase)
            TauSymm(NTau)=i
          endif
        else
          Grp=Grp(:idel(Grp))//'1'
          if(NDimI(KPhase).eq.1) then
            Grp4=Grp4(:idel(Grp4))//'0'
            NTau=NTau+1
            Tau(NTau)=0.
            TauSymm(NTau)=1
          endif
        endif
        if(NDimI(KPhase).eq.1) ip=2
        do i=1,nsns
          if(KCentroSymm.gt.0.and.
     1       index(mirror,t3(i)(1:1)).le.0) cycle
          if(KeyOr(io(1,i)).eq.4) go to 2000
        enddo
        i=idel(Grp)
        if(NOsyMirr.ne.0) then
          if(Grp(1:1).ne.'R') then
            Grp=Grp(1:i)//'1'
            if(NDimI(KPhase).eq.1) then
              Grp4=Grp4(:idel(Grp4))//'0'
              NTau=NTau+1
              Tau(NTau)=0.
              TauSymm(NTau)=1
            endif
          endif
        else
          Grp(i:i)=' '
          if(NDimI(KPhase).eq.1) then
            ip=idel(Grp4)
            Grp4(ip:ip)=' '
          endif
        endif
        if(Grp(2:).eq.'3211') Grp(2:)='321'
        if(Grp(2:).eq.'31211') Grp(2:)='3121'
      else if(CrSystem(KPhase).eq.CrSystemHexagonal) then
        Grp=Grp(1:1)//t3(abs(i6))
        if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(iabs(i6))
        if(NDimI(KPhase).eq.1) then
          Grp4=t2m(iabs(i6))
          NTau=NTau+1
          Tau(NTau)=s6(4,iabs(i6),isw,KPhase)
          TauSymm(NTau)=iabs(i6)
        endif
        if(KCentroSymm.gt.0) then
          ior=KeyOr(io(1,iabs(i6)))
          do i=1,nsns
            if(index(mirror,t3(i)(1:1)).gt.0.and.KeyOr(io(1,i)).eq.ior)
     1        go to 1750
          enddo
          ijk=13
          go to 8000
1750      Grp=Grp(1:idel(Grp))//'/'//t3(i)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
          if(NDimI(KPhase).eq.1) then
            Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
            NTau=NTau+1
            Tau(NTau)=s6(4,i,isw,KPhase)
            TauSymm(NTau)=i
          endif
        endif
        if(NOsyMirr.eq.3) then
          i=kmirr(1)
          if(i.le.0) i=kosy(1)
          Grp=Grp(1:idel(Grp))//t3(i)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
          if(NDimI(KPhase).eq.1) then
            Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
            NTau=NTau+1
            Tau(NTau)=s6(4,i,isw,KPhase)
            TauSymm(NTau)=i
          endif
          do i=1,nsns
            if(KCentroSymm.gt.0.and.
     1         index(mirror,t3(i)(1:1)).le.0) cycle
            if(KeyOr(io(1,i)).eq.4) go to 2000
          enddo
          ijk=14
          go to 8000
        endif
      else if(CrSystem(KPhase).eq.CrSystemCubic) then
        ijk=15
        if(i4.eq.0.and.kosy(1).eq.0) go to 8000
        if(KCentroSymm.gt.0) t3(i3)='-3'
        if(i4.eq.0.or.KCentroSymm.gt.0) then
          j=kmirr(3)
          if(j.le.0) j=kosy(3)
          Grp=Grp(1:1)//t3(j)
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(j)
        else
          if(Grp(1:1).eq.'I') then
            if(t3(iabs(i4)).eq.'43') then
              t3(iabs(i4))='41'
            else if(t3(iabs(i4)).eq.'42') then
              t3(iabs(i4))='4 '
            endif
          endif
          Grp=Grp(1:1)//t3(iabs(i4))
          if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//
     1                                      tm(iabs(i4))
        endif
        if(Grp(1:1).eq.'I'.and.t3(i3).eq.'3d') t3(i3)='3'
        Grp=Grp(:idel(Grp))//t3(i3)
        if(MagneticType(KPhase).gt.0) then
          if(KCentroSymm.gt.0) then
            Grp=Grp(:idel(Grp))//tm(KCentroSymm)
          else
            Grp=Grp(:idel(Grp))//tm(i3)
          endif
        endif
        do i=1,nsns
          if(KCentroSymm.gt.0.and.
     1       index(mirror,t3(i)(1:1)).le.0) cycle
          if(KeyOr(io(1,i)).eq.4) then
            Grp=Grp(:idel(Grp))//t3(i)
            if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
            go to 1825
          endif
        enddo
1825    if((Grp(2:2).eq.'b'.or.Grp(2:2).eq.'c').and.
     1     (Grp(1:1).eq.'F'.or.Grp(1:1).eq.'I')) then
          Grp(2:2)='a'
        endif
!   Grupy cislo 197 a 199 ... I23 a I213
        j=0
        GrPom=' '
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            GrPom(j:j)=Grp(i:i)
          endif
        enddo
        idl=idel(GrPom)
        if(GrPom.eq.'I23') then
          nx=1
          do i=1,nsns
            if(t3(i).ne.'2') cycle
            if(rm6(1,i,isw,KPhase).lt.0.) then
              call CopyVek(s6(1,i,isw,KPhase),px(nx),3)
              if((rm6(NDim(KPhase)+2,i,isw,KPhase).gt.0..and.
     1            abs(s6(2,i,isw,KPhase)).gt..1).or.
     2           (rm6(2*NDim(KPhase)+3,i,isw,KPhase).gt.0..and.
     3            abs(s6(3,i,isw,KPhase)).gt..1))
     4          call AddVek(vt6(1,2,isw,KPhase),px(nx),px(nx),3)
              call od0do1(px(nx),px(nx),3)
              if(nx.gt.1) then
                if(abs(px(1)-px(4)).gt..01) then
                  GrPom=Grp
                  j=0
                  Grp=' '
                  do k=1,idel(GrPom)
                    j=j+1
                    if(GrPom(k:k).eq.'2') then
                      Grp(j:j+1)='21'
                      j=j+1
                    else
                      Grp(j:j)=GrPom(k:k)
                    endif
                  enddo
                  go to 2050
                endif
              else
                nx=nx+3
              endif
            endif
          enddo
        else if(GrPom.eq.'P-43c') then
          GrPom='P-43n'
        else if(GrPom.eq.'Pn-3c') then
          GrPom='Pn-3n'
        else if(GrPom.eq.'Pm-3c') then
          GrPom='Pm-3n'
        else if(GrPom.eq.'F4332') then
          GrPom='F4132'
        endif
        j=0
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            Grp(i:i)=GrPom(j:j)
          endif
        enddo
      else
        ijk=16
        go to 8000
      endif
      go to 2050
2000  Grp=Grp(1:idel(Grp))//t3(i)
      if(MagneticType(KPhase).gt.0) Grp=Grp(:idel(Grp))//tm(i)
      if(NDimI(KPhase).eq.1) then
        Grp4=Grp4(:idel(Grp4))//t2m(i)(:idel(t2m(i)))
        NTau=NTau+1
        Tau(NTau)=s6(4,i,isw,KPhase)
        TauSymm(NTau)=i
      endif
2050  j=0
      GrPom=' '
      do i=1,idel(Grp)
        if(Grp(i:i).ne.'''') then
          j=j+1
          GrPom(j:j)=Grp(i:i)
        endif
      enddo
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
      ngrupa(KPhase)=0
2100  read(ln,'(i3,7x,a8)',err=9000,end=2200) igi,GrupaI
      if(GrPom.eq.GrupaI.or.
     1   (GrPom(1:1).eq.'X'.and.GrPom(2:).eq.GrupaI(2:))) then
        ngrupa(KPhase)=igi
        go to 2200
      else
        go to 2100
      endif
2200  call CloseIfOpened(ln)
      if(MagneticType(KPhase).gt.0) then
        if(invc.gt.0.and.NDimI(KPhase).le.1) then
          SmbMagCentr=' '
          i=invc
          nnul=0
          knul=0
          npul=9999
          kpul=0
          do k=1,NLattVec(KPhase)
            call AddVek(s6(1,i,1,KPhase),vt6(1,k,1,KPhase),sp,
     1                  NDim(KPhase))
            call od0do1(sp,sp,NDim(KPhase))
            nnula=0
            knula=0
            npula=0
            kpula=0
            do j=1,3
              if(abs(sp(j)).le..001) then
                nnula=nnula+1
                knula=j
              else if(abs(sp(j)-.5).le..001) then
                npula=npula+1
                kpula=j
              endif
            enddo
            if(nnula.eq.0.and.npula.eq.0) cycle
            if(npula.lt.npul.or.
     1         (npula.eq.npul.and.npula.eq.1.and.kpula.lt.kpul).or.
     2         (npula.eq.npul.and.npula.eq.2.and.knula.gt.knul)) then
              nnul=nnula
              knul=knula
              npul=npula
              kpul=kpula
            endif
          enddo
          if(npul+nnul.eq.3) then
            if(npul.eq.3) then
              SmbMagCentr='I'
            else if(npul.eq.2) then
              SmbMagCentr=char(ichar('A')+knul-1)
            else if(npul.eq.1) then
              SmbMagCentr=char(ichar('a')+kpul-1)
            endif
            if(SmbMagCentr.eq.'c') then
              if(CrSystem(KPhase).eq.CrSystemTrigonal) then
                SmbMagCentr='I'
              else if(CrSystem(KPhase).eq.CrSystemTriclinic) then
                SmbMagCentr='s'
              endif
            endif
          endif
          if(SmbMagCentr.ne.' ')
     1      SmbMagCentr='['//SmbMagCentr(:idel(SmbMagCentr))//']'
        endif
        if(invm.gt.0.and.NDimI(KPhase).eq.1) then
          Grp=Grp(:idel(Grp))//'.1'''
          if(abs(s6(4,invm,1,KPhase)).lt..001) then
            Grp4=Grp4(:idel(Grp4))//'0'
          else if(abs(abs(s6(4,invm,1,KPhase))-.5).lt..001) then
            Grp4=Grp4(:idel(Grp4))//'s'
          endif
        endif
      endif
      if(NDimI(KPhase).eq.1) then
        call CrlGetSmbQ4dFromSymm(qu(1,1,isw,KPhase),isw,
     1                            Grp(idel(Grp)+1:),iQuIrr)
        Grp=Grp(:idel(Grp))//Grp4(:idel(Grp4))
      endif
      if(Grp(1:1).eq.'X') then
        GrPom=' '
        j=0
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            GrPom(j:j)=Grp(i:i)
          endif
        enddo
        if(NLattVec(KPhase).eq.1) then
          GrPom(1:1)='P'
        else if(NLattVec(KPhase).eq.2.or.NLattVec(KPhase).eq.4) then
          do i=2,NLattVec(KPhase)
            npul=0
            nnul=0
            kde=0
            nsum=0
            do j=1,3
              if(abs(vt6(j,i,1,KPhase)).le..001) then
                nnul=nnul+1
                kde=j
                nsum=nsum+1
              else if(abs(vt6(j,i,1,KPhase)-.5).le..001) then
                npul=npul+1
              endif
            enddo
            if(NLattVec(KPhase).eq.2) then
              if(nnul.le.0) then
                GrPom(1:1)='I'
              else if(nnul.eq.1) then
                GrPom(1:1)=smbABC(kde)
                call Velka(GrPom(1:1))
              endif
            else
              if(kde.ne.1) exit
              nsum=nsum+kde
            endif
            if(i.eq.4.and.nsum.eq.6) GrPom(1:1)='F'
          enddo
        endif
        if(GrPom(1:1).ne.'X') then
          Veta=GrPom
          call SpGroupAlias(GrPom)
          GrPom(1:1)='X'
        endif
        j=0
        do i=1,idel(Grp)
          if(Grp(i:i).ne.'''') then
            j=j+1
            Grp(i:i)=GrPom(j:j)
          endif
        enddo
      endif
      if(isw.ne.1.or..not.FindOriginShift) go to 8100
      PrvniGrp=.true.
      allocate(jvtp(nsns))
3100  GrupaO=Grupa(KPhase)
      Grupa(KPhase)=Grp
      call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
      call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
      call EM50MakeStandardOrder
      call CrlOrderMagSymmetry
      Grupa(KPhase)=GrupaO
      if(ich.ne.0) then
        ijk=17
        go to 8100
      endif
      nd=NDim(KPhase)
!      if(NDimI(KPhase).eq.1.and.PrvniGrp.and.
!     1   .not.EqRV0(QuiR(1,1,KPhase),3,.0001)) nd=3
!      write(Cislo,'(3l5)') NDimI(KPhase).eq.1,PrvniGrp,
!     1      .not.EqRV0(QuiR(1,1,KPhase),3,.0001)
      call SetIntArrayTo(jvtp,nsns,1)
      call SetRealArrayTo(ShSg(1,KPhase),nd,999999.)
      nn=0
3150  nx=0
      call SetRealArrayTo(rx,72,0.)
      call SetRealArrayTo(px,12,0.)
      is=0
      nsnsn=NSymm(KPhase)
      do 3300i=1,nsns
        call CopyVek(rm6s(1,i),rmps,NDimQ(KPhase))
        call ShSgGetReflCond(rm6s(1,i),s6s(1,i),RefCondS,nd)
        is=is+1
        do j=1,nsnsn
          if(.not.eqrv(rm6s(1,i),rm6(1,j,isw,KPhase),NDimQ(KPhase),
     1                 .001).or.
     2       abs(ZMagS(i)-ZMag(j,isw,KPhase)).gt..001) cycle
          call CopyVek (rm6(1,j,isw,KPhase),rmp,NDimQ(KPhase))
          iporp=j
          do jvt=jvtp(is),NLattVec(KPhase)
            call AddVek(s6(1,j,isw,KPhase),
     1                  vt6(1,jvt,isw,KPhase),sp,NDim(KPhase))
            call ShSgGetReflCond(rm6(1,j,isw,KPhase),sp,RefCond,nd)
            if(.not.eqrv(RefCond,RefCondS,nd+1,.0001))
     1        cycle
            jvtp(is)=jvt
            call CopyVek(rx,rxo,72)
            call CopyVek(px,pxo,12)
            do k=1,nd
              sp(k)=-sp(k)+s6s(k,i)
            enddo
            ipor(i)=iporp
            do k=1,nd
              m=nx+k
              mm=k
              px(m)=sp(k)
              do l=1,nd
                if(k.eq.l) then
                  rx(m)=rm6(mm,j,isw,KPhase)-1.
                else
                  rx(m)=rm6(mm,j,isw,KPhase)
                endif
                m=m+2*nd
                mm=mm+NDim(KPhase)
              enddo
            enddo
            nx=nx+nd
            call SolveShSg(rx,px,nd,nx,ich)
            if(ich.ne.0) then
              call CopyVek(rxo,rx,72)
              call CopyVek(pxo,px,12)
            else
              if(i.ge.nsns) then
                call GetShSg(rx,px,nd,nx,
     1                       vt6(1,1,isw,KPhase),ubound(vt6,1),
     2                       NLattVec(KPhase),ShSg(1,KPhase))
                go to 3200
              else
                go to 3300
              endif
            endif
          enddo
          jvtp(is)=1
          do k=is-1,1,-1
            if(jvtp(k).lt.NLattVec(KPhase)) then
              jvtp(k)=jvtp(k)+1
              nn=nn+1
              if(nn.gt.12345) then
                if(VasekTest.ne.0) then
                  call FeChybne(-1.,-1.,'Zle maticko zle!!!',
     1                          'Pocatek ne a ne urcit',
     2                          SeriousError)
                  call SetRealArrayTo(ShSg(1,KPhase),
     1                                NDim(KPhase),0.)
                endif
                go to 8100
              endif
              go to 3150
            else
              jvtp(k)=1
            endif
          enddo
          if(Grp(1:4).eq.'C222'.and.index(Grp,'1').gt.0.and.
     1       PrvniGrp) then
            k=index(Grp,')')
            Grp(k+1:)=' '
            if(k.gt.0) then
              k=k+iQuIrr
              if(Grp(k:k).eq.'s') then
                Grp(k:k)='0'
              else
                Grp(k:k)='s'
              endif
            endif
            PrvniGrp=.false.
            go to 3100
          endif
          go to 3250
3200      if(NDimI(KPhase).eq.1.and.PrvniGrp.and.
     1            .not.EqRV0(QuiR(1,1,KPhase),3,.0001)) then
            Veta=Grp
            QrShift=ScalMul(QuiR(1,1,KPhase),ShSg(1,KPhase))
            if(abs(QrShift).gt..001) then
              k=index(Grp,')')
              Grp(k+1:)=' '
              do ii=1,NTau
                is=TauSymm(ii)
                if(rm6s(16,is).gt.0.) then
                  call MultM(rm6s(1,is),ShSg(1,KPhase),px,NDim(KPhase),
     1                       NDim(KPhase),1)
                  do jj=1,NDim(KPhase)
                    px(jj)=s6s(jj,is)+ShSg(jj,KPhase)-px(jj)
                  enddo
                  call od0do1(px,px,NDim(KPhase))
                  Tau(ii)=px(4)-scalmul(quir(1,1,KPhase),px)
                  call od0do1(Tau(ii),Tau(ii),1)
                  if(abs(Tau(ii)).lt..01) then
                    Cislo='0'
                  else if(abs(Tau(ii)-.5).lt..01) then
                    Cislo='s'
                  else if(abs(Tau(ii)-.333333).lt..01) then
                    Cislo='t'
                  else if(abs(Tau(ii)-.666667).lt..01) then
                    Cislo='-t'
                  else if(abs(Tau(ii)-.25).lt..01) then
                    Cislo='q'
                  else if(abs(Tau(ii)-.75).lt..01) then
                    Cislo='-q'
                  else if(abs(Tau(ii)-.1666667).lt..01) then
                    Cislo='h'
                  else if(abs(Tau(ii)-.8333333).lt..01) then
                    Cislo='-h'
                  endif
                else
                  Cislo='0'
                endif
                Grp=Grp(:idel(Grp))//Cislo(:idel(Cislo))
              enddo
              if(invm.gt.0) then
                if(abs(s6(4,invm,1,KPhase)).lt..001) then
                  Grp=Grp(:idel(Grp))//'0'
                else if(abs(abs(s6(4,invm,1,KPhase))-.5).lt..001) then
                  Grp=Grp(:idel(Grp))//'s'
                endif
              endif
            endif
            PrvniGrp=.false.
            go to 3100
          else
            go to 8100
          endif
3250      if(VasekTest.ne.0) then
            call FeChybne(-1.,-1.,'Zle maticko zle!!!',
     1                    'Udelal co mohl a nic',
     2                    SeriousError)
            call SetRealArrayTo(ShSg(1,KPhase),NDim(KPhase),0.)
          endif
          go to 8100
        enddo
3300  continue
      go to 8100
8000  Grp='???'
      ngrupa(KPhase)=0
8100  if(SmbMagCentr.ne.' '.and.Grp(1:1).ne.'?')
     1  Grp=Grp(1:1)//SmbMagCentr(:idel(SmbMagCentr))//Grp(2:)
      do i=1,nsns
        if(ipor(i).gt.nsns) then
          do j=1,nsns
            ipor(j)=j
          enddo
          exit
        endif
      enddo
      do i=1,nss
        if(ChangeOrder) then
          if(i.le.nsns) then
            j=ipor(i)
          else
            j=i
          endif
        else
          j=i
        endif
        call CopyMat(rm6s(1,i),rm6(1,j,isw,KPhase),NDim(KPhase))
        call CopyVek(s6s(1,i),s6(1,j,isw,KPhase),NDim(KPhase))
        call CodeSymm(rm6(1,j,isw,KPhase),s6(1,j,isw,KPhase),
     1                symmc(1,j,isw,KPhase),0)
        call MatBlock3(rm6(1,j,isw,KPhase),rm(1,j,isw,KPhase),
     1                 NDim(KPhase))
        zmag(j,isw,KPhase)=zmags(i)
        call CrlMakeRMag(j,isw)
      enddo
      NSymm(KPhase)=nss
      NSymmN(KPhase)=nsno
      NLattVec(KPhase)=nvts
      call SetRealArrayTo(ShSg(5,KPhase),2,0.)
      call od0do1(ShSg(1,KPhase),ShSg(1,KPhase),NDim(KPhase))
      do i=1,NDim(KPhase)
        if(ShSg(i,KPhase).gt..5) ShSg(i,KPhase)=ShSg(i,KPhase)-1.
      enddo
      go to 9999
9000  call FeReadError(ln)
      call CloseIfOpened(ln)
9999  if(allocated(t3)) deallocate(t3,t2m,tm,io,Diagonal,rm6s,rms,s6s,
     1                             zmags,ipor)
      if(allocated(jvtp)) deallocate(jvtp)
      return
      end
