      subroutine SelAtoms(Text,Atom,Brat,isf,n,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/SelAtomsC/ nButtInclAtoms,nButtExclAtoms,nEdwAtoms,
     1                  nButtInclTypes,nButtExclTypes,nRolMenuTypes
      dimension isf(n)
      character*(*) Text,Atom(n)
      character*256 EdwStringQuest
      character*80 t80
      character*8  At
      logical Brat(n),EqWild,lpom,SelOne,WizardModeIn
      integer SbwLnQuest,RolMenuSelectedQuest,SbwItemSelQuest,
     1        SbwItemPointerQuest
      external SelAtomsCheck,FeVoid
      save /SelAtomsC/
      SelOne=.false.
      go to 1000
      entry SelOneAtom(Text,Atom,ia,n,ich)
      SelOne=.true.
1000  if(n.le.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      dl=0.
      idl=0
      do i=1,n
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        dl=max(dl,FeTxLength(Atom(i))+10.)
        idl=max(idl,idel(Atom(i)))
      enddo
      call CloseIfOpened(ln)
      xqd=600.
      if(n.gt.50) then
        nrow=11
      else if(n.gt.20) then
        nrow=10
      else if(n.gt.10) then
        nrow=5
      else
        nrow=5
      endif
      if(idl.le.8) then
        ncol=7
      else
        ncol=max(xqd/dl,1.)
      endif
      if(SelOne) then
        il=nrow+1
      else
        il=nrow+3
      endif
      WizardModeIn=WizardMode
      WizardMode=.false.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=nrow
      ild=nrow
      call FeQuestSbwMake(id,xpom,il,dpom,ild,ncol,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      if(SelOne) then
        nButtInclAtoms=0
        nButtExclAtoms=0
        nEdwAtoms=0
        nButtInclTypes=0
        nButtExclTypes=0
        nRolMenuTypes=0
      else
        il=il+2
        dpom=80.
        xpom=xqd*.5-dpom-10.
        t80='Select %all'
        call FeQuestButtonMake(id,xpom,-10*il+3,dpom,ButYd,t80)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xqd*.5+10.
        t80='%Refresh'
        call FeQuestButtonMake(id,xpom,-10*il+3,dpom,ButYd,t80)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        ypom=(QuestYPosition(id,il+1)+QuestYPosition(id,il+2))*.5-10.
        xpom=5.
        dpom=80.
        tpom=xpom+dpom+10.
        call FeQuestEdwMake(id,tpom,-10*il-15,xpom,-10*il-15,'=>','L',
     1                      dpom,EdwYd,0)
        nEdwAtoms=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,' ')
        xpom=tpom+30.
        dpom=100.
        t80='%Include atoms'
        do i=1,2
          il=il+1
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
          if(i.eq.1) then
            nButtInclAtoms=ButtonLastMade
            t80='%Exclude atoms'
          else
            nButtExclAtoms=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
        enddo
        il=il-2
        xpom=xqd*.5+5.
        dpom=80.
        tpom=xpom+dpom+10.
        call FeQuestRolMenuMake(id,tpom,-10*il-15,xpom,-10*il-15,'=>',
     1                          'L',dpom,EdwYd,0)
        nRolMenuTypes=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        xpom=tpom+30.
        dpom=100.
        t80='Include atom %types'
        do i=1,2
          il=il+1
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
          if(i.eq.1) then
            nButtInclTypes=ButtonLastMade
            t80='Exclude atom t%ypes'
          else
            nButtExclTypes=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
        enddo
      endif
1200  if(SelOne) then
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_list.tmp')
      else
        do i=1,n
          if(Brat(i)) then
            j=1
          else
            j=0
          endif
          call FeQuestSetSbwItemSel(i,nSbw,j)
        enddo
        call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_list.tmp')
      endif
1500  call FeQuestEventWithCheck(id,ich,SelAtomsCheck,FeVoid)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(Brat,n,lpom)
        go to 1600
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclAtoms.or.
     2         CheckNumber.eq.nButtExclAtoms)) then
        lpom=CheckNumber.eq.nButtInclAtoms
        t80=EdwStringQuest(nEdwAtoms)
        call mala(t80)
        do i=1,n
          At=Atom(i)
          call mala(At)
          if(EqWild(At,t80,.false.)) Brat(i)=lpom
        enddo
        call FeQuestStringEdwOpen(nEdwAtoms,' ')
        go to 1600
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInclTypes.or.
     2         CheckNumber.eq.nButtExclTypes)) then
        lpom=CheckNumber.eq.nButtInclTypes
        isfp=RolMenuSelectedQuest(nRolMenuTypes)
        do i=1,n
          if(isf(i).eq.isfp) Brat(i)=lpom
        enddo
        call FeQuestRolMenuOpen(nRolMenuTypes,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 2000
1600  call CloseIfOpened(SbwLnQuest(nSbw))
      go to 1200
2000  if(SelOne) then
        ia=SbwItemPointerQuest(nSbw)
      else
!        j=SbwItemPointerQuest(nSbw)
        do i=1,n
          Brat(i)=SbwItemSelQuest(i,nSbw).eq.1
!          if(Brat(i)) j=0
        enddo
!        if(j.ne.0) Brat(j)=.true.
      endif
      call FeQuestRemove(id)
9999  call DeleteFile(fln(:ifln)//'_list.tmp')
      WizardMode=WizardModeIn
      return
      end
