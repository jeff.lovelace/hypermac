      subroutine SCode(n,nctr,x,it,isw,scout,sczout)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(3),it(3)
      character*(*) scout,sczout
      character*15 isven(3),is(:,:,:,:)
      save is
      allocatable is
      do i=1,3
        isven(i)=' '
        do k=1,8
          p=x(i)*float(k)
          l=nint(p)
          if(abs(p-float(l)).lt..00001) go to 1500
        enddo
        isven(i)='???/?'
        go to 1600
1500    if(l.ne.0) then
          if(k.eq.1) then
            write(isven(i),'(i3)') l
          else
            write(isven(i),'(i3,''/'',i1)') l,k
          endif
          call Zhusti(isven(i))
        endif
1600    if(isven(i)(1:1).ne.'-'.and.isven(i).ne.' ')
     1    isven(i)='+'//isven(i)(:idel(isven(i)))
        kk=idel(isven(i))
        if(kk.ne.0) then
          isven(i)=is(i,n,isw,KPhase)(:idel(is(i,n,isw,KPhase)))//
     1             isven(i)(1:kk)
        else
          isven(i)=is(i,n,isw,KPhase)
        endif
        kk=kk+1
        if(isven(i)(kk:kk).eq.' '.and.kk.ne.1) isven(i)(kk:kk)='+'
        call zhusti(isven(i))
      enddo
      scout=isven(1)
      call zhusti(scout)
      do i=2,3
        kk=idel(scout)
        scout=scout(1:kk)//', '//isven(i)(1:idel(isven(i)))
      enddo
      if(n.ne.1) then
        write(sczout,'(''s'',i4)') n
      else
        sczout=' '
      endif
      if(nctr.ne.1) write(sczout(6:10),'(''c'',i4)') nctr
      if(it(1).ne.0.or.it(2).ne.0.or.it(3).ne.0) write(sczout(11:),
     1   '(''t'',i3,2('','',i3))') it
      call zhusti(scout)
      call zhusti(sczout)
      go to 9999
      entry SetScode
      if(allocated(is)) deallocate(is)
      allocate(is(3,2*MaxNSymm,3,NPhase))
      do KPh=1,NPhase
        do l=1,NComp(KPh)
          i=0
          do ii=1,NSymm(KPh)
            i=i+1
            do k=1,3
              is(k,i,l,KPh)=' '
              kk=0
              do 1040j=1,3
                m=nint(rm6(k+NDim(KPh)*(j-1),ii,l,KPh))
                if(m.eq.0) go to 1040
                kk=idel(is(k,i,l,KPh))+1
                if(m.lt.0) is(k,i,l,KPh)(kk:kk)='-'
                kk=kk+1
                if(iabs(m).gt.1) then
                  write(is(k,i,l,KPh)(kk:15),'(i1)') iabs(m)
                  kk=kk+1
                endif
                write(is(k,i,l,KPh)(kk:15),FormA) smbx(j)
1040          continue
              i1=idel(is(k,i,l,KPh))
              do j=2,i1
                if(is(k,i,l,KPh)(j:j).eq.' ') is(k,i,l,KPh)(j:j)='+'
              enddo
            enddo
          enddo
        enddo
      enddo
9999  return
      end
