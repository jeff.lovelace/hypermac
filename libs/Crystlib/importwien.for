      subroutine ImportWien(ich)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      logical ExistFile,EqIgCase
      Veta=' '
1100  call FeFileManager('Select input Wien file',Veta,'*.txt',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9900
      if(.not.ExistFile(Veta)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                Veta(:idel(Veta))//
     2                '" does not exist, try again.',' ',SeriousError)
          go to 1100
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','old')
      nat=0
1200  read(ln,FormA,end=1500) Veta
      if(EqIgCase(Veta(1:4),':POS')) then
        nat=nat+1
        k=LocateSubstring(Veta,'POSITION =',.false.,.true.)
        k=k+10
        call StToReal(Veta,k,x(1,nat),3,.false.,ich)
        x(1,nat)=-x(1,nat)
      endif
      go to 1200
1500  call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
9900  call CloseIfOpened(ln)
      return
      end
