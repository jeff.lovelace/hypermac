      subroutine CrlReadAbsCoeff(Key,At,AtAbsCoef,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*(*) At
      dimension xp(2)
      integer RadiationPom
      real LamAvePom
      ich=0
      AtAbsCoef=0.
      RadiationPom=0
      k=0
      LamAvePom=0.
      if(Key.eq.1) then
        if(ExistM90) then
          call iom90(0,fln(:ifln)//'.m90')
          RadiationPom=Radiation(KDatBlock)
          k=klam(KDatBlock)
          LamAvePom=LamAve(KDatBlock)
        else if(ExistM95) then
          RadiationPom=RadiationRefBlock(KRefBlock)
          k=klamRefBlock(KRefBlock)
          LamAvePom=LamAveRefBlock(KRefBlock)
        else
          RadiationPom=Radiation(1)
          k=klam(1)
          LamAvePom=LamAve(1)
        endif
      else
        RadiationPom=RadiationRefBlock(KRefBlock)
        k=klamRefBlock(KRefBlock)
        LamAvePom=LamAveRefBlock(KRefBlock)
      endif
      if(iabs(DataType(KDatBlock)).eq.2.and.
     1   KRefLam(KDatBlock).ne.0) LamAvePom=LamPwd(1,KDatBlock)
      if(LamAvePom.lt..05) then
        AtAbsCoef=0.
      else
        if(RadiationPom.eq.XRayRadiation) then
          if(k.gt.0) then
            call RealFromAtomFile(At,'Xray_cross_section',AtAbsCoef,k,
     1                          ich)
          else
            AtAbsCoef=EM50ReadAbsor(At,LamAvePom)
          endif
        else if(RadiationPom.eq.NeutronRadiation) then
          call RealAFromAtomFile(At,'Neutron_cross_section',xp,2,ich)
          if(ich.ne.0) go to 9999
          AtAbsCoef=xp(2)*LamAvePom/1.798
        endif
      endif
9999  return
      end
