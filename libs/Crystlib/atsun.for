      subroutine AtSun(iod,ido,jod)
      include 'fepc.cmn'
      include 'basic.cmn'
      if(jod.le.iod) then
        i1=iod
        i2=ido
        ik=1
      else
        i1=ido
        i2=iod
        ik=-1
      endif
      id=jod-iod
      do i=i1,i2,ik
        j=i+id
        call AtCopy(i,j)
      enddo
      return
      end
