      subroutine GoToSubGrGetSubSel
      use Basic_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iar(2)
      character*256 Veta
      character*8   PointGroup,GrupaR
      character*2   t2
      logical EqIgCase
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
      if(NDim(KPhase).ne.4) then
        i=1
        j=idel(Grupa(KPhase))
      else
        i=index(Grupa(KPhase),':')
        if(i.gt.0) then
          i=i+1
          j=index(Grupa(KPhase)(i+1:),':')+i-1
        else
          i=1
          j=index(Grupa(KPhase),'(')-1
        endif
      endif
1500  read(ln,FormSG,end=1550) ig,ipg,idl,GrupaR
      if(EqIgCase(GrupaR,Grupa(KPhase)(i:j))) then
        if(NGrupa(KPhase).ne.ig) then
          if(VasekTest.eq.1) then
            write(Cislo,'(2i4)') NGrupa(KPhase),ig
            Veta='Nesouhlas poradoveho cisla grupy:'//
     1           Cislo(:idel(Cislo))
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            NGrupa(KPhase)=ig
          endif
        endif
        go to 1550
      endif
      go to 1500
1550  call CloseIfOpened(ln)
      call GetPointGroupFromSpaceGroup(PointGroup,i,j)
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'pgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
1700  read(ln,FormA,end=1800) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1   go to 1700
      call Kus(Veta,k,Cislo)
      call StToInt(Veta,k,iar,1,.false.,ich)
      if(ich.ne.0) go to 1800
      m=iar(1)
      do i=1,m
        read(ln,FormA,end=1800) Veta
      enddo
      read(ln,FormA,end=1800) Veta
      k=0
      call StToInt(Veta,k,iar,1,.false.,ich)
      if(ich.ne.0) go to 1800
      NSubGr=iar(1)
      if(NSubGr.le.0) go to 1800
      if(allocated(SelSubGr)) deallocate(SelSubGr,GrpSubGr,SysSubGr,
     1                                   NGrpSubGr,PorGrpSubGr)
      allocate(SelSubGr(NSymm(KPhase),NSubGr),GrpSubGr(NSubGr),
     1         SysSubGr(NSubGr),NGrpSubGr(NSubGr),PorGrpSubGr(NSubGr))
      call SetIntArrayTo(SelSubGr,NSymm(KPhase)*NSubGr,0)
      do i=1,NSubGr
        read(ln,FormA,end=1800) Veta
        k=0
        call StToInt(Veta,k,SelSubGr(1,i),NSymm(KPhase),.false.,ich)
      enddo
      do i=1,NSymm(KPhase)
        call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),
     1             t2,io(1,i),m,det)
        ipri(i)=i
      enddo
1800  call CloseIfOpened(ln)
      return
      end
