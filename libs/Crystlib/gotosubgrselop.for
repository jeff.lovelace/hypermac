      subroutine GoToSubGrSelOp(Klic,IPocatek,nl,TrMatPom,GrpTr,TrShift,
     1                          ich)
      use Basic_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrMatPom(*),TrMatPomI(36),TrShift(*),rmp(36),sp(6)
      character*(*) GrpTr
      character*80 Veta,Grpp,Grp
      character*2  t2m,nty
      character*1  LatticeS
      integer UseTabsS,UseTabsP,SbwItemPointerQuest,SbwItemSelQuest
      logical Konci,CrwLogicQuest,EqRV,IsNormal,BratSymmIn(:)
      allocatable BratSymmIn
      if(allocated(BratSymmIn)) deallocate(BratSymmIn)
      allocate(BratSymmIn(NSymm(KPhase)))
      do i=1,NSymm(KPhase)
        BratSymmIn(i)=BratSymmSub(i)
      enddo
      if(IPocatek.gt.1) go to 2700
      LatticeS=Lattice(KPhase)
      call GoToSubGrGetSubSel
      if(allocated(SelSubGr)) then
        do i=1,NSubGr
          call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.false.)
          jk=NSymm(KPhase)
          do j=1,NSymm(KPhase)
            k=SelSubGr(j,i)
            if(k.gt.0) then
              BratSymmSub(k)=.true.
            else
              jk=j-1
              exit
            endif
          enddo
          call GoToSubGrGetGrSmb(nss,nvts,Grp,GrpSubGr(i),NGrpSubGr(i),
     1                           ncoset,TrMatPom,TrMatPomI,
     2                           TrShift,icrsys,IsNormal,0)
          if(Grupa(KPhase)(1:1).eq.'X') GrpSubGr(i)(1:1)='X'
          if(NDimI(KPhase).eq.1) then
            j=index(GrpSubGr(i),'(')
            if(j.gt.0) GrpSubGr(i)(j:)=' '
          endif
          call TrMat2String(TrMatPom,NDim(KPhase),SysSubGr(i))
          NGrpSubGr(i)=100000*indc+(300-NGrpSubGr(i))*100+i
        enddo
        call indexx(NSubGr,NGrpSubGr,PorGrpSubGr)
      endif
      do i=1,NSymm(KPhase)
        BratSymmSub(i)=BratSymmIn(i)
      enddo
      if(.not.WizardMode) then
        WizardLength=StSymL+SbwPruhXd+150.
        i=6
        if(NLattVec(KPhase).gt.1) i=i+1
        if(NSubGr.gt.1) i=i+1
        WizardLines=min(18,nl+i)
        WizardId=NextQuestId()
        WizardMode=.true.
        WizardTitle=.true.
        call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                     'x',0,LightGray,0,0)
      endif
      xqd=WizardLength
      ilm=WizardLines
      StSymLP=StSymL
      xpom=150.
      if(NDimI(KPhase).gt.0) then
        StSymLP=StSymLP+50.
        xpom=xpom+50.
      endif
      UseTabsS=NextTabs()
      call FeTabsAdd(xpom,UseTabsS,IdRightTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabsS,IdCenterTab,' ')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabsS,IdRightTab,' ')
      xpom=xpom+40.
      call FeTabsAdd(xpom,UseTabsS,IdRightTab,' ')
      UseTabs=UseTabsS
1100  id=NextQuestId()
      Veta='Define subgroup'
      call FeQuestTitleMake(id,Veta)
      call FeQuestButtonDisable(ButtonEsc)
      QuestCheck(id)=1
      il=1
      idm=40
      xsbw=(xqd-StSymLP)*.5
      xpom=8.+xsbw
      Veta='Operator'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      xpom=xpom+187.
      if(NDimI(KPhase).gt.0) xpom=xpom+50.
      Veta='Symbol'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      xpom=xpom+100.
      Veta='Direction'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=ilm-5
      if(NLattVec(KPhase).gt.1) il=il-1
      if(NSubGr.gt.1) il=il-1
      xpom=xsbw
      call FeQuestSbwMake(id,xpom,il,StSymLP,il-1,1,CutTextNone,
     1                    SbwVertical)
      nSbw=SbwLastMade
      if(NLattVec(KPhase).gt.1.and.Klic.le.0) then
        il=il+1
        Veta='%Keep '//Lattice(KPhase)(1:1)//' centring'
        xpom=(xqd-FeTxLengthUnder(Veta)-10.-CrwXd)*.5
        tpom=xpom+10.+CrwXd
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwCenter=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
      else
        nCrwCenter=0
      endif
      if(NSubGr.gt.1) then
        il=il+1
        Veta='Select non-isomorphic %subgroup'
        dpom=FeTxLengthUnder(Veta)+20.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtSelect=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtSelect=0
      endif
      il=il+1
      Veta='C%omplete subgroup'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xqd*.5-dpom-10.,il,dpom,ButYd,Veta)
      nButtComplete=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='%Refresh'
      call FeQuestButtonMake(id,xqd*.5+10.,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Space group'
      xpom=FeTxLength(Veta)
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta='Subgroup'
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      Veta=':'
      tpom=tpom+xpom+5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      if(Grupa(KPhase)(1:1).eq.'?') then
        do i=1,NSymm(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),
     1               t2m,io(1,i),n,det)
        enddo
        call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.true.)
        call GoToSubGrGetGrSmb(nss,nvts,Grp,GrpTr,i,ncoset,
     1                         TrMatPom,TrMatPomI,TrShift,icrsys,
     2                         IsNormal,1)
      else
        GrpTr=Grupa(KPhase)
        call UnitMat(TrMatPom,NDim(KPhase))
        call CopyVek(ShSg(1,KPhase),TrShift,3)
      endif
!      if(NDim(KPhase).ne.4) then
        i=1
        j=idel(GrpTr)
!      else
!        i=index(GrpTr,':')
!        if(i.gt.0) then
!          i=i+1
!          j=index(GrpTr(i+1:),':')+i-1
!        else
!          i=1
!          j=index(GrpTr,'(')-1
!        endif
!      endif
      Veta=GrpTr(i:j)
      tpom=tpom+10.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=' '
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblSubgroup=LblLastMade
      tpom=xqd*.5-100.
      Veta='Axes :'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+5.
      call TrMat2String(TrMatPom,NDim(KPhase),Veta)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=' '
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblAxes=LblLastMade
      Veta='Index :'
      tpom=xqd-FeTxLength(Veta)-25.
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+5.
      Veta=' '
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblIndex=LblLastMade
      Veta='Origin :'
      tpom=tpom-FeTxLength(Veta)-140.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+5.
      call OriginShift2String(TrShift,3,Veta)
      tpom=tpom+5.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=' '
      call FeQuestLblMake(id,tpom,il+1,Veta,'L','N')
      nLblOrigin=LblLastMade
      il=il+2
      tpom=5.
      Veta=' '
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','N')
      nLblNormal=LblLastMade
      Grp=' '
      Grpp=' '
1200  do i=1,NSymm(KPhase)
!        if(NDim(KPhase).gt.3) then
!          do j=1,3
!            Cislo=symmc(j,i,1,KPhase)
!            do k=1,NDim(KPhase)
!              l=index(Cislo,smbx6(k))
!              if(l.ne.0) Cislo(l:l+1)=smbx(k)//' '
!            enddo
!            call zhusti(Cislo)
!            if(j.eq.1) then
!              StSym(i)=Cislo
!            else
!              StSym(i)=StSym(i)(:idel(StSym(i)))//' '//Cislo
!            endif
!          enddo
!        else
          call MakeSymmSt(StSym(i),symmc(1,i,1,KPhase))
!        endif
        if(MagneticType(KPhase).gt.0) then
          if(ZMag(i,1,KPhase).gt.0) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          StSym(i)=StSym(i)(:idel(StSym(i)))//Cislo(:idel(Cislo))
        endif
        call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),t2m,
     1             io(1,i),n,det)
        if(OpSmb(i).eq.'3'.or.OpSmb(i).eq.'4'.or.OpSmb(i).eq.'6')
     1    OpSmb(i)(2:2)='+'
        if(OpSmb(i).eq.'-3'.or.OpSmb(i).eq.'-4'.or.OpSmb(i).eq.'-6')
     1    OpSmb(i)(3:3)='+'
        if(ZMag(i,1,KPhase).lt.0) OpSmb(i)(idel(OpSmb(i))+1:)=''''
        StSym(i)=StSym(i)(:idel(StSym(i)))//Tabulator//'|'//
     1           Tabulator//OpSmb(i)
        write(Veta,'(''('',2(i2,'',''),i2,'')'')')(io(j,i),j=1,3)
        call zhusti(Veta)
        StSym(i)=StSym(i)(:idel(StSym(i)))//Tabulator//'|'//
     1           Tabulator//Veta(:idel(Veta))
        k=100
        ips(i)=0
        do j=1,3
          ips(i)=ips(i)+100000*iabs(io(j,i))+k*(io(j,i)+2)+n
          k=k*10
        enddo
        if(det.lt.0.) ips(i)=ips(i)+1
        k=0
        do j=1,i-1
          if(ips(i).eq.ips(j)/100) k=k+1
        enddo
        ips(i)=ips(i)*100+k
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l52','formatted','unknown')
      call indexx(NSymm(KPhase),ips,ipr)
      do i=1,NSymm(KPhase)
        ipri(ipr(i))=i
        write(ln,FormA) StSym(ipr(i))
      enddo
      call CloseIfOpened(ln)
1400  do i=1,NSymm(KPhase)
        j=ipr(i)
        if(BratSymmSub(j)) then
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbw,j)
      enddo
      UseTabs=UseTabsS
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'.l52')
1500  call FeQuestEvent(id,ich)
      if(Grp.ne.' ') then
        do i=1,NSymm(KPhase)
          if(BratSymmSub(i).neqv.SbwItemSelQuest(ipri(i),nSbw).eq.1)
     1      then
            if(.not.Konci) Grp=' '
            exit
          endif
        enddo
      endif
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        QuestCheck(id)=0
        if(Grp.eq.' ') then
          Konci=.true.
          go to 2000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCenter) then
        if(CrwLogicQuest(nCrwCenter)) then
          k=0
          do i=1,NSymm(KPhase),nvtp
            k=k+1
            call CopySymmOperator(i,k,1)
          enddo
          NSymm(KPhase)=NSymm(KPhase)/nvtp
          NLattVec(KPhase)=nvtp
          if(nButtSelect.gt.0) call FeQuestButtonOff(nButtSelect)
          nvtp=1
        else
          ns=NSymm(KPhase)*NLattVec(KPhase)
          call ReallocSymm(NDim(KPhase),ns,NLattVec(KPhase),
     1                     NComp(KPhase),NPhase,0)
          do i=NSymm(KPhase),1,-1
            k=(i-1)*NLattVec(KPhase)
            do j=1,NLattVec(KPhase)
              k=k+1
              call AddVek(s6(1,i,1,KPhase),vt6(1,j,1,KPhase),
     1                    s6(1,k,1,KPhase),NDim(KPhase))
              call od0do1(s6(1,k,1,KPhase),s6(1,k,1,KPhase),
     1                    NDim(KPhase))
              call CopyMat(rm6(1,i,1,KPhase),rm6(1,k,1,KPhase),
     1                     NDim(KPhase))
              call CopyMat(rm(1,i,1,KPhase),rm(1,k,1,KPhase),3)
              call CopyMat(RMag(1,i,1,KPhase),RMag(1,k,1,KPhase),3)
              call CodeSymm(rm6(1,k,1,KPhase),s6(1,k,1,KPhase),
     1                      symmc(1,k,1,KPhase),0)
              IswSymm(k,1,KPhase)=IswSymm(i,1,KPhase)
              BratSymmSub(k)=BratSymmSub(i)
              ZMag(k,1,KPhase)=ZMag(i,1,KPhase)
            enddo
          enddo
          NSymm(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
          nvtp=NLattVec(KPhase)
          NLattVec(KPhase)=1
          if(nButtSelect.gt.0) call FeQuestButtonDisable(nButtSelect)
        endif
        i=nSbw+SbwFr-1
        call CloseIfOpened(SbwLn(i))
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelect)
     1  then
        WizardMode=.false.
        idp=NextQuestId()
        n=min(12,NSubGr)
        il=n+1
        Veta='Select subroup'
        UseTabsP=NextTabs()
        call FeTabsAdd(70.+EdwMarginSize,UseTabsP,IdCenterTab,' ')
        call FeTabsAdd(90.+EdwMarginSize,UseTabsP,IdRightTab,' ')
        UseTabs=UseTabsP
        xqdp=300.
        call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
        xpom=7.
        il=1
        call FeQuestLblMake(idp,10.,il,'Subgroup','L','N')
        call FeQuestLblMake(idp,130.,il,'Axes','L','N')
        xpom=5.
        call FeQuestSbwMake(idp,xpom,n+1,xqdp-14.-SbwPruhXd,n,1,
     1                      CutTextNone,SbwVertical)
        nSbwSelect=SbwLastMade
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.l53','formatted','unknown')
        do i=1,NSubGr
          j=PorGrpSubGr(i)
          Veta=GrpSubGr(j)(:idel(GrpSubGr(j)))//Tabulator//'|'//
     1         Tabulator//SysSubGr(j)(:idel(SysSubGr(j)))
          write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        call FeQuestSbwMenuOpen(nSbwSelect,fln(:ifln)//'.l53')
1600    call FeQuestEvent(idp,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1600
        endif
        call FeTabsReset(UseTabsP)
        WizardMode=.true.
        if(ich.eq.0) then
          i=PorGrpSubGr(SbwItemPointerQuest(nSbwSelect))
          call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.false.)
          do j=1,NSymm(KPhase)
            k=SelSubGr(j,i)
            if(k.gt.0) then
              BratSymmSub(k)=.true.
            else
              exit
            endif
          enddo
          call FeQuestRemove(idp)
          do i=1,NSymm(KPhase)
            j=ipr(i)
            if(BratSymmSub(j)) then
              j=1
            else
              j=0
            endif
            call FeQuestSetSbwItemSel(i,nSbw,j)
          enddo
          Konci=.false.
          go to 2000
        else
          call FeQuestRemove(idp)
          UseTabs=UseTabsS
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtComplete)
     1  then
        Konci=.false.
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRefresh)
     1  then
        BratSymmSub(1)=.true.
        do i=2,NSymm(KPhase)
          BratSymmSub(i)=.false.
        enddo
        call FeQuestButtonOff(nButtRefresh)
        go to 2300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 2500
2000  do i=1,NSymm(KPhase)
        BratSymmSub(i)=SbwItemSelQuest(ipri(i),nSbw).eq.1
      enddo
      call GoToSubGrGetGrSmb(nss,nvts,Grp,GrpTr,i,ncoset,
     1                       TrMatPom,TrMatPomI,TrShift,icrsys,
     2                       IsNormal,1)
!      if(NDim(KPhase).ne.4.or.GrpTr(1:1).eq.'?') then
        Grpp=GrpTr
!      else
!        Grpp=GrpTr(:index(GrpTr,'(')-1)
!      endif
      call FeQuestLblChange(nLblSubgroup,Grpp)
      call TrMat2String(TrMatPom,NDim(KPhase),Veta)
      call FeQuestLblChange(nLblAxes,Veta)
      write(Veta,'(i3)') indc
      call Zhusti(Veta)
      call FeQuestLblChange(nLblIndex,Veta)
      call OriginShift2String(TrShift,3,Veta)
      call FeQuestLblChange(nLblOrigin,Veta)
      Veta='The selected subgroup'
      if(IsNormal) then
        Veta(idel(Veta)+2:)='is normal'
      else
        Veta(idel(Veta)+2:)='is not normal'
      endif
      call FeQuestLblChange(nLblNormal,Veta)
      if(Konci) go to 1500
2300  call CloseIfOpened(SbwLn(nSbw))
      go to 1400
2500  QuestCheck(id)=0
      if(ich.ne.0) go to 9000
      if(Grp(1:1).eq.'?') then
        Lattice(KPhase)=LatticeS
      else
        Lattice(KPhase)=Grp(1:1)
      endif
2550  if(Klic.le.0) then
        if(indc.eq.1) then
          call FeFillTextInfo('gotosubgr1.txt',0)
          inext=-1
        else
          if(indc.lt.nss) then
            call FeFillTextInfo('gotosubgr2.txt',0)
            i=index(TextInfo(1),'#$%')
            if(i.gt.0) then
              if(NDimI(KPhase).gt.0) then
                Veta='super'
              else
                Veta=' '
              endif
              TextInfo(1)=TextInfo(1)(1:i-1)//Veta(:idel(Veta))//
     1                    TextInfo(1)(i+3:)
            endif
            i=index(TextInfo(2),'#$%')
            write(Veta,'(i2)') indc
            call zhusti(Veta)
            if(i.gt.0)
     1        TextInfo(2)=TextInfo(2)(1:i-1)//Veta(:idel(Veta))//
     2                    TextInfo(2)(i+3:)
            inext=1
          else
            go to 2600
          endif
        endif
        call FeWizardTextInfo('INFORMATION',1,inext,1,ich)
        if(ich.ne.0) then
          if(ich.lt.0) then
            go to 9000
          else
            go to 1100
          endif
        endif
        if(indc.eq.1) then
          ich=1
          go to 9000
        endif
      endif
2600  do i=1,nss
        if(BratSymmSub(i)) then
          ips(i)=1
        else
          ips(i)=0
        endif
      enddo
      n=1
      do i=1,nss
        if(ips(i).ne.0) cycle
        n=n+1
        do 2650j=1,nss
          if(ips(j).eq.1) then
            call MultM(rm6(1,i,1,KPhase),rm6(1,j,1,KPhase),rmp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call CopyVek(s6(1,i,1,KPhase),sp,NDim(KPhase))
            call CultM(rm6(1,i,1,KPhase),s6(1,j,1,KPhase),sp,
     1                 NDim(KPhase),NDim(KPhase),1)
            call od0do1(sp,sp,NDim(KPhase))
            do k=1,nss
              if(ips(k).eq.0.and.
     1           eqrv(rm6(1,k,1,KPhase),rmp,NDimQ(KPhase),.001).and.
     2           eqrv( s6(1,k,1,KPhase), sp,NDim (KPhase),.001)) then
                ips(k)=n
                itrl(n)=k
                go to 2650
              endif
            enddo
          endif
2650    continue
      enddo
      if(indc.ge.nss) go to 9999
2700  nmax=8
      if(IPocatek.le.1) then
        i=2
      else
        i=IPocatek
      endif
2750  if(Klic.le.0) then
        id=NextQuestId()
        write(Veta,'(i2,a2)') i,nty(i)
        call zhusti(Veta)
        Veta='Select one representative of the '//Veta(:idel(Veta))//
     1      ' coset'
        call FeQuestTitleMake(id,Veta)
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.l52','formatted','unknown')
        m=0
        MSel=1
        do j=1,nss
          k=ipr(j)
          if(ips(k).eq.i) then
            m=m+1
            iopc(m)=k
            if(IPocatek.le.1) then
              if(m.eq.1) then
                itrl(i)=iopc(m)
                MSel=m
              endif
            else
              if(itrl(i).eq.iopc(m)) MSel=m
            endif
            write(ln,FormA) StSym(k)
          endif
        enddo
        call CloseIfOpened(ln)
        mm=max(m+1,5)
        ilp=(ilm-mm)/2+1
        if(ilp.ge.1) then
          il=ilp+mm-1
          ild=mm-1
        else
          ilp=1
          il=ilm
          ild=ilm-2
        endif
        xsbw=(xqd-StSymLP)*.5
        xpom=8.+xsbw
        Veta='Operator'
        call FeQuestLblMake(id,xpom,ilp,Veta,'L','N')
        xpom=xpom+187.
        Veta='Symbol'
        call FeQuestLblMake(id,xpom,ilp,Veta,'L','N')
        xpom=xpom+100.
        Veta='Direction'
        call FeQuestLblMake(id,xpom,ilp,Veta,'L','N')
        UseTabs=UseTabsS
        xpom=xsbw
        call FeQuestSbwMake(id,xpom,il,StSymLP,ild,1,CutTextNone,
     1                      SbwVertical)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'.l52')
        call FeQuestSbwItemOff(nSbw,1)
        call FeQuestSbwItemOn(nSbw,MSel)
        tpom=5.
2800    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 2800
        endif
        if(ich.eq.0) then
          itrl(i)=iopc(SbwItemPointerQuest(nSbw))
          if(i.lt.indc) then
            i=i+1
            go to 2750
          endif
          IPocatek=i
        else if(ich.lt.0) then
          go to 9000
        else
          if(i.gt.2) then
            i=i-1
            go to 2750
          else
            go to 1100
          endif
        endif
      else
        do i=1,indc
          do j=1,nss
            k=ipr(j)
            if(ips(k).eq.i) then
              itrl(i)=j
              cycle
            endif
          enddo
        enddo
      endif
      go to 9999
9000  call FeQuestRemove(id)
9999  call FeTabsReset(UseTabsS)
      if(allocated(BratSymmIn)) deallocate(BratSymmIn)
      return
      end
