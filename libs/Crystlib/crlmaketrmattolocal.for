      subroutine CrlMakeTrMatToLocal(XCentr,LocSystName,LocSystAx,
     1              LocSystSt,LocSense,LocSystX,TrMat,TrMatInv,imol,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) LocSystAx,LocSystSt(2),LocSystName
      character*80 t80,ErrSt
      character*1  LocSense
      dimension TrMat(9),TrMatInv(9),xp(3,3),xpi(3,3),px(3),ip(3),
     1          XCentr(3)
      real LocSystX(3,2)
      ich=0
      do j=1,2
        l=0
        ipj=ichar(LocSystAx(j:j))-ichar('x')+1
        if(ipj.lt.1.or.ipj.gt.3) then
          if(LocSystAx.eq.' ') then
            ipj=j
          else
            ErrSt='incorrect axis symbol "'//LocSystAx(j:j)//'"'
            go to 9000
          endif
        endif
        ip(j)=ipj
        if(j.eq.2.and.ip(1).eq.ip(2)) then
          ErrSt='double occurence of axis symbol "'//LocSystAx(1:2)//'"'
          go to 9000
        endif
        call CrlGetXFromAtString(LocSystSt(j),imol,LocSystX(1,j),ErrSt,
     1                           ich)
        if(ich.eq.0) then
          do k=1,3
            px(k)=LocSystX(k,j)-XCentr(k)
          enddo
        else if(ich.lt.0) then
          call CopyVek(LocSystX(1,j),px,3)
          ich=0
        else
          go to 9000
        endif
        if(VecOrtScal(px,px,3).lt..00001) then
          ErrSt='singular definintion'
          go to 9000
        endif
        call multm(TrToOrtho(1,1,KPhase),px,xp(1,ipj),3,3,1)
        call VecOrtNorm(xp(1,ipj),3)
      enddo
      ip(3)=6-ip(1)-ip(2)
      pom=VecOrtScal(xp(1,ip(1)),xp(1,ip(2)),3)
      do k=1,3
        xp(k,ip(2))=xp(k,ip(2))-xp(k,ip(1))*pom
      enddo
      call VecOrtNorm(xp(1,ip(2)),3)
      call VecMul(xp(1,ip(1)),xp(1,ip(2)),xp(1,ip(3)))
      if((ip(1).eq.1.and.ip(2).eq.3).or.
     1   (ip(1).eq.2.and.ip(2).eq.1).or.
     2   (ip(1).eq.3.and.ip(2).eq.2))
     3   call RealVectorToOpposite(xp(1,ip(3)),xp(1,ip(3)),3)
      if(VecOrtScal(xp(1,ip(3)),xp(1,ip(3)),3).lt..00001) then
        ErrSt='singular definintion'
        go to 9000
      endif
      if(LocSense.eq.'-')
     1  call RealVectorToOpposite(xp(1,ip(3)),xp(1,ip(3)),3)
      call MatInv(xp,xpi,pom,3)
      call multm(xpi,TrToOrtho(1,1,KPhase),TrMat,3,3,3)
      call MatInv(TrMat,TrMatInv,pom,3)
      go to 9999
9000  t80='in the definition of local coordinate system for "'//
     1    LocSystName(:idel(LocSystName))//'"'
      call FeChybne(-1.,-1.,t80,ErrSt,SeriousError)
      ich=1
9999  return
108   format(f15.0)
109   format(3f9.6)
      end
