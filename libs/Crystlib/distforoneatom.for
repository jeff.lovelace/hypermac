      subroutine DistForOneAtom(ktery,dmez,isw,klic)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6),x1(3),y1(6),y2(6),u(3),ug(3),idist(20),uxp(6),
     1          uyp(6),uxpp(6),uypp(6),jsh(3),shj(3),shjc(3),uz(3)
      ndist=0
      dmezi=dmez
      call SetRealArrayTo(xp(4),3,0.)
      if(klic.eq.0) then
        call CopyVek(x(1,ktery),xp,3)
      else
        call bunka(x(1,ktery),x1,0)
      endif
      do i=1,NAtCalc
        if(ai(i).le.0..or.iswa(i).ne.isw.or.kswa(i).ne.KPhase) cycle
        if(klic.eq.0) then
          call bunka(x(1,i),x1,0)
        else
          call CopyVek(x(1,i),xp,3)
        endif
        do jsym=1,NSymm(KPhase)
          if(klic.eq.0) then
            if(isa(jsym,ktery).le.0) cycle
          else
            if(isa(jsym,i).le.0) cycle
          endif
          call multm(rm6(1,jsym,isw,KPhase),xp,y1,NDim(KPhase),
     1                                          NDim(KPhase),1)
          do l=1,NDim(KPhase)
            y1(l)=y1(l)+s6(l,jsym,isw,KPhase)
          enddo
          eps=rm6(NDimQ(KPhase),jsym,isw,KPhase)
          do jcenter=1,NLattVec(KPhase)
            call AddVek(y1,vt6(1,jcenter,isw,KPhase),y2,NDim(KPhase))
            call bunka(y2,shj,1)
            do l=1,3
              uz(l)=y2(l)-x1(l)
            enddo
            do jcellx=-1,1
              cellxj=jcellx
              u(1)=uz(1)+cellxj
              jsh(1)=nint(shj(1))+jcellx
              shjc(1)=shj(1)+cellxj+s6(1,jsym,isw,KPhase)+
     1                vt6(1,jcenter,isw,KPhase)
              do jcelly=-1,1
                cellyj=jcelly
                u(2)=uz(2)+cellyj
                jsh(2)=nint(shj(2))+jcelly
                shjc(2)=shj(2)+cellyj+s6(2,jsym,isw,KPhase)+
     1                  vt6(2,jcenter,isw,KPhase)
                do jcellz=-1,1
                  cellzj=jcellz
                  u(3)=uz(3)+cellzj
                  jsh(3)=nint(shj(3))+jcellz
                  shjc(3)=shj(3)+cellzj+s6(3,jsym,isw,KPhase)+
     1                    vt6(3,jcenter,isw,KPhase)
                  call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
                  d=sqrt(scalmul(u,ug))
                  if(d.ge.dmezi) cycle
                  m=d*1000.
                  if(ndist.lt.50) then
                    ndist=ndist+1
                    j1=ndist
                  else
                    j1=ipord(50)
                  endif
                  idist(j1)=m
                  if(ndist.eq.50) call indexx(50,idist,ipord)
                  do j=1,3
                    xdist(j,j1)=u(j)+x1(j)
                  enddo
                  if(NDim(KPhase).gt.3) then
                    fi=pi2*y2(4)
                    do k=1,KModA(2,ktery)
                      fip=fi*float(k)
                      csm=cos(fip)
                      snm=sin(fip)
                      do j=1,3
                        uxp(j)=ux(j,k,ktery)
                        uyp(j)=uy(j,k,ktery)
                      enddo
                      do j=4,NDim(KPhase)
                        uxp(j)=0.
                        uyp(j)=0.
                      enddo
                      call multm(rm6(1,jsym,isw,KPhase),uxp,uxpp,
     1                           NDim(KPhase),NDim(KPhase),1)
                      call multm(rm6(1,jsym,isw,KPhase),uyp,uypp,
     1                           NDim(KPhase),NDim(KPhase),1)
                      do j=1,3
                        uxdist(j,k,j1)= eps*uxpp(j)*csm+uypp(j)*snm
                        uydist(j,k,j1)=-eps*uxpp(j)*snm+uypp(j)*csm
                      enddo
                    enddo
                  endif
                  ddist(j1)=d
                  adist(j1)=atom(i)
                  call scode(jsym,jcenter,shjc,jsh,isw,
     1                       SymCodeDist(j1),SymCodeJanaDist(j1))
                  if(ndist.eq.50) dmezi=idist(ipord(ndist))/1000.
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
      if(ndist.gt.1) then
        if(ndist.lt.50) call indexx(ndist,idist,ipord)
      else
        ipord(1)=1
      endif
      return
      end
