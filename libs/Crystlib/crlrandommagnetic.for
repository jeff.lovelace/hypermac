      subroutine CrlRandomMagnetic
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,t256
      integer :: NCyklMax=50,EdwStateQuest
      real :: RFac(5),RFacOpt(5)
      logical ExistFile,RefineEnd,CrwLogicQuest,UseKMod(3)
      ShowInfoOnScreen=.false.
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      call Random_Seed()
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.b40')
      KMagMx=0
      do ia=1,NAtAll
        KMagMx=max(KMagMx,MagPar(ia))
      enddo
      id=NextQuestId()
      xqd=500.
      il=6
      Veta='Random search for magnetic amplitudes'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=1
      Veta='%Magnetic amplitude:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+30.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMagAmpl=EdwLastMade
      AmpMax=.2
      call FeQuestRealEdwOpen(nEdwMagAmpl,AmpMax,.false.,.false.)
      il=il+1
      Veta='Maximal number of hits:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxHits=EdwLastMade
      call FeQuestIntEdwOpen(nEdwMaxHits,NCyklMax,.false.)
      tpome=xpom+dpom+20.
      Veta='%Edit refinement commands'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpome,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefineCommands=ButtonLastMade
      tpome=tpome+dpom+20.
      Veta='%Run optimatization'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpome,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRun=ButtonLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Trial#0'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblLast=LblLastMade
      tpomp=100.
      call FeQuestLblMake(id,tpomp,il,' ','L','N')
      nLblRLast=LblLastMade
      Veta='Best fit hit#0'
      il=il+1
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblBest=LblLastMade
      call FeQuestLblMake(id,tpomp,il,' ','L','N')
      nLblRBest=LblLastMade
      il=il+1
      Veta='%Omit solution'
      dpom=FeTxLengthUnder(Veta)+30.
      xpom=xqd*.5-dpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtOmit=ButtonLastMade
      Veta='%Accept solution'
      xpom=xpom+dpom+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAccept=ButtonLastMade
1400  if(ExistFile(fln(:ifln)//'.b40')) then
        call FeQuestButtonOff(nButtAccept)
      else
        call FeQuestButtonDisable(nButtAccept)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAccept) then
        call CopyFile(fln(:ifln)//'.b40',fln(:ifln)//'.m40')
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtOmit)
     1  then
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtRefineCommands) then
        k=2
        NeverStartProgram=.true.
        call SetCommands(k)
        NeverStartProgram=.false.
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRun) then
        call FeQuestRealFromEdw(nEdwMagAmpl,AmpMax)
        call FeQuestIntFromEdw(nEdwMaxHits,NCyklMax)
        NCykl=0
        RFacOpt=9999.
2100    NCykl=NCykl+1
        if(NCykl.gt.NCyklMax) go to 1400
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
        do ia=1,NAtAll
          if(MagPar(ia).le.0) cycle
          isw=iswa(ia)
          ksw=kswa(ia)
          do i=1,3
            call Random_Number(pom)
            pom=2.*pom-1.
            sm0(i,ia)=AmpMax*pom/CellPar(i,isw,ksw)
          enddo
          do i=1,MagPar(ia)-1
            do j=1,3
              call Random_Number(pom)
              pom=2.*pom-1.
              smx(j,i,ia)=AmpMax*pom/CellPar(j,isw,ksw)
              call Random_Number(pom)
              pom=2.*pom-1.
              smy(j,i,ia)=AmpMax*pom/CellPar(j,isw,ksw)
            enddo
          enddo
        enddo
        call iom40(1,0,fln(:ifln)//'.m40')
        call iom40(0,0,fln(:ifln)//'.m40')
        call Refine(0,RefineEnd)
        if(RefineEnd) go to 1400
        if(ErrFlag.eq.0) then
          call UpdateSummary
        else
          ErrFlag=0
        endif
2300    Veta=fln(:ifln)//'.m70'
        if(ExistFile(Veta)) then
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','unknown')
          if(ErrFlag.ne.0) go to 2100
2320      read(ln,FormA,end=2340) Veta
          m=12
          n=15
          do i=1,5
            j=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                        .false.,.true.)
            if(j.gt.0) then
              k=j+idel(CIFKey(m,n))
              call StToReal(Veta,k,RFac(i),1,.false.,ichp)
              if(ichp.ne.0) RFac(i)=.9999
              if(i.ne.1) RFac(i)=RFac(i)*100.
              go to 2320
            endif
            if(i.eq.1) then
              m=23
            else if(i.eq.2) then
              m=38
            else if(i.eq.3) then
              m=22
            else if(i.eq.4) then
              m=40
            endif
          enddo
          go to 2320
2340      call CloseIfOpened(ln)
        endif
        write(Veta,'(''Trial#'',i5)') NCykl
        call Zhusti(Veta)
        call FeQuestLblChange(nLblLast,Veta)
        if(RFac(1).gt.90.) then
          write(Veta,103)
        else
          write(Veta,102)(RFac(i),i=1,5)
        endif
        call FeQuestLblChange(nLblRLast,Veta)
        if(RFac(1).lt.RFacOpt(1)) then
          RFacOpt=RFac
          call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.b40')
          NCyklBest=NCykl
          write(Veta,'(i5)') NCykl
          call Zhusti(Veta)
          Veta='Best fit hit#'//Veta(:idel(Veta))
          call FeQuestLblChange(nLblBest,Veta)
          write(Veta,102)(RFacOpt(i),i=1,5)
          call FeQuestLblChange(nLblRBest,Veta)
        endif
        go to 2100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call iom40(0,0,fln(:ifln)//'.m40')
9999  if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      call FeQuestRemove(id)
      return
102   format('GOF=',f6.2,5x,'R(obs)=',f6.2,5x,'Rw(obs)=',f6.2,5x,
     1       'R(all)=',f6.2,5x,'Rw(all)=',f6.2)
103   format('GOF=   ---',5x,'R(obs)=   ---',5x,'Rw(obs)=   ---',5x,
     1       'R(all)=   ---',5x,'Rw(all)=   ---')
      end
