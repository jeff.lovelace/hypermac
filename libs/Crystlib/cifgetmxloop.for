      subroutine CIFGetMxLoop(MxLoop)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256,p256
      logical EqIgCase
      MxLoop=0
      mm=0
1100  call CIFNewRecord(mm,t256,*9999)
      k=0
      call kus(t256,k,p256)
      if(.not.EqIgCase(p256,'loop_')) go to 1100
      m=0
1150  if(k.lt.len(t256)) then
        call kus(t256,k,p256)
        if(p256(1:1).eq.'_') then
          if(LocateSubstring(p256,'_geom' ,.false.,.true.).le.0.and.
     1       LocateSubstring(p256,'_refln',.false.,.true.).le.0.and.
     2       LocateSubstring(p256,'_pd',.false.,.true.).le.0)
     3         go to 1100
          m=m+1
          go to 1150
        endif
      endif
1200  call CIFNewRecord(mm,t256,*9999)
      k=0
      call kus(t256,k,p256)
      if(p256(1:1).eq.'_') then
        if(LocateSubstring(p256,'_geom' ,.false.,.true.).eq.1.or.
     1     LocateSubstring(p256,'_refln',.false.,.true.).eq.1.or.
     2     LocateSubstring(p256,'_pd',.false.,.true.).eq.1)
     3       go to 1100
        m=m+1
        go to 1200
      endif
      n=0
1300  call CIFNewRecord(mm,t256,*9000)
      k=0
      call kus(t256,k,p256)
      if(p256(1:1).eq.'_'.or.EqIgCase(p256,'loop_')) then
        mm=mm-1
        if(m.gt.0) MxLoop=max(MxLoop,nint(float(n)/float(m)))
        go to 1100
      endif
1320  n=n+1
      if(k.lt.len(t256)) then
        call kus(t256,k,p256)
        go to 1320
      else
        go to 1300
      endif
9000  MxLoop=max(MxLoop,nint(float(n)/float(m)))
9999  return
      end
