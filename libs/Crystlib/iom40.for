      subroutine iom40(klic,tisk,FileNameIn)
      use Atoms_mod
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension iw(mxw,MxPhases),kwp(3),nw(MxPhases),px(6),
     1          kmodp(7),kip(6)
      character*(*) FileNameIn
      character*256 FileName,PureFileName
      character*80 tp80,t80
      character*12 :: nazev,id(9) =
     1            (/'maxsc   ',
     2              'ortho   ',
     3              'wave    ',
     4              'polynom ',
     5              'polar   ',
     6              'xyzmode ',
     7              'magmode ',
     8              'atsplit ',
     9              'molsplit'/)
      character*8 NamePolynom(:),NamePolynomOld(:)
      integer :: tisk, NPolynom = 0
      logical block,eqiv,only,EqIgCase,OldFile,EqIV0
      allocatable NamePolynom,NamePolynomOld
      only=.false.
      OldFile=.false.
      FileName=FileNameIn
      go to 1000
      entry iom40old(klic,tisk)
      FileName=fln(:ifln)//'.m40'
      only=.false.
      OldFile=.true.
      go to 1000
      entry iom40only(klic,tisk,FileNameIn)
      OldFile=.false.
      FileName=FileNameIn
      only=.true.
1000  call GetPureFileName(FileName,PureFileName)
      if(NAtXYZMode.gt.0) then
        if(Klic.ge.1) then
          call TrXYZMode(1)
        else
          call TrXYZMode(-1)
        endif
      endif
      if(NAtMagMode.gt.0) then
        if(Klic.ge.1) then
          call TrMagMode(1)
        else
          call TrMagMode(-1)
        endif
      endif
      KPhaseIn=KPhase
      if(klic.le.0) then
        if(allocated(FFrRef))
     1    deallocate(FFrRef,FFiRef,sFFrRef,sFFiRef)
        if(.not.EqIV0(KAnRef,NDatBlock))
     1    allocate( FFrRef(MaxNAtFormula,NPhase,NDatBlock),
     2              FFiRef(MaxNAtFormula,NPhase,NDatBlock),
     3             sFFrRef(MaxNAtFormula,NPhase,NDatBlock),
     4             sFFiRef(MaxNAtFormula,NPhase,NDatBlock))
        itfmax=0
        IFrMax=0
        MagParMax=0
        call SetIntArrayTo(KModAMax,7,0)
        call SetIntArrayTo(KModMMax,3,0)
        MagParMax=0
        call DeallocateAtomParams
        Block=.false.
        call OpenFile(m40,FileName,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
1100    read(m40,FormA,err=1460,end=1460) t80
        kk=0
        call kus(t80,kk,nazev)
        if(.not.Block) then
          if(EqIgCase(Nazev,'commands')) then
            block=.true.
            go to 1100
          else
            rewind m40
            go to 1120
          endif
        endif
        if(.not.EqIgCase(Nazev,'end')) go to 1100
1120    call SetBasicM40(Only)
        nl=0
        NPosMax=0
        do KPh=1,NPhase
          read(m40,104,err=9000,end=9000)
     1      (NAtIndLen(i,KPh),NMolecLen(i,KPh),i=1,NComp(KPh)),
     2       lite(KPh),irot(KPh)
          nl=nl+1
          do i=1,NComp(KPh)
            NAtPosLen(i,KPh)=0
            NAtMolLen(i,KPh)=0
            do j=1,NMolecLen(i,KPh)
              read(m40,104,err=9000,end=9000) NAt,NPos
              NPosMax=max(NPosMax,NPos)
              nl=nl+1
              NAtPosLen(i,KPh)=NAtPosLen(i,KPh)+NAt*NPos
              NAtMolLen(i,KPh)=NAtMolLen(i,KPh)+NAt
            enddo
          enddo
        enddo
        call EM40UpdateMolecLimits
        NAtCalc=0
        NAtCalcBasic=0
        call EM40UpdateAtomLimits
        NAtCalcBasic=NAtCalc
        MxAtAll=NAtAll
        if(allocated(iam)) deallocate(iam,mam)
        if(NMolec.gt.0) allocate(iam(NMolec),mam(NMolec))
        mxm=0
        mxp=0
        do i=1,nl
          backspace m40
        enddo
        n=0
        do KPh=1,NPhase
          read(m40,FormA,err=9000,end=9000)
          do i=1,NComp(KPh)
            do j=1,NMolecLen(i,KPh)
              n=n+1
              read(m40,104,err=9000,end=9000) iam(n),mam(n)
            enddo
          enddo
        enddo
        if(NAtAll.gt.0) then
          n=NAtAll
        else
          n=100
        endif
        call AllocateAtoms(n)
        call AllocateOrtho(50)
        mxscutw=6
        block=.false.
        call SetIntArrayTo(nw,MxPhases,0)
        rewind(m40)
        NPolynom=0
        NPolynomMax=0
        NSplitMolecules=0
        NMolSplit=0
        NAtSplit=0
1200    read(m40,FormA,err=1460,end=1460) t80
1210    kk=0
        call kus(t80,kk,nazev)
        if(.not.block) then
          if(.not.EqIgCase(nazev,'commands')) then
            rewind m40
            go to 1400
          else
            block=.true.
            go to 1200
          endif
        endif
        if(EqIgCase(nazev,'end')) go to 1400
        i=islovo(nazev,id,9)
        if(i.eq.1) then
          call kus(t80,kk,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=1300) j
          mxscutw=min(j,mxsc)
        else if(i.eq.2) then
          call ReallocOrtho(nor+1)
          nor=nor+1
          call kus(t80,kk,OrA(nor))
          if(kk.eq.80) go to 1300
          call uprat(OrA(nor))
          call kus(t80,kk,cislo)
          if(kk.eq.80) go to 1300
          call posun(cislo,1)
          read(cislo,102,err=1300) OrDel(nor)
          if(kk.eq.80) go to 1300
          j=1
          call kus(t80,kk,cislo)
          call posun(cislo,1)
          read(cislo,102,err=1300) OrX40(nor)
          call kus(t80,kk,cislo)
          ik=1
1220      read(m40,FormA,err=1460,end=1460) t80
          kk=0
          call kus(t80,kk,tp80)
          i=islovo(tp80,id,7)
          if(i.gt.0.or.EqIgCase(tp80,'end')) then
            call posun(cislo,1)
            read(cislo,102,err=1240) OrEps(nor)
            if(OrEps(nor).le.0.) go to 1240
            go to 1250
1240        OrEps(nor)=.95
1250        backspace m40
            go to 1200
          else
            if(kk.lt.len(t80)) call kus(t80,kk,Cislo)
            go to 1220
          endif
        else if(i.eq.3) then
          call kus(t80,kk,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=1300) i
          if(NPhase.gt.1) then
            call kus(t80,kk,cislo)
            call posun(cislo,0)
            read(cislo,FormI15,err=1300) j
            if(j.gt.MxPhases) then
              call FeChybne(-1.,-1.,' incorrect phase specification.',
     1                      t80,SeriousError)
              ErrFlag=1
              go to 9999
            endif
          else
            j=1
          endif
          if(NPhase.gt.1) KPhase=j
          if(i.le.NDimI(KPhase)) then
            call FeChybne(-1.,-1.,'the basic modulation vector(s) '//
     1                    'cannot be changed.',' ',SeriousError)
            ErrFlag=1
            go to 9999
          else if(i.gt.mxw) then
            tp80='the maximum number of waves is '
            i=idel(tp80)+1
            write(tp80(i:i+1),'(i2)') mxw
            call FeChybne(-1.,-1.,tp80,t80,SeriousError)
            ErrFlag=1
            go to 9999
          endif
          call StToInt(t80,kk,kw(1,i,j),NDimI(KPhase),.false.,ich)
          if(ich.ne.0) go to 1300
          nw(j)=nw(j)+1
          iw(nw(j),j)=i
        else if(i.eq.4) then
1260      call kus(t80,kk,tp80)
          call UprAt(tp80)
          if(tp80.ne.' ') then
            if(NPolynom.le.NPolynomMax) then
              if(NPolynom.gt.0) then
                allocate(NamePolynomOld(NPolynom))
                do i=1,NPolynom
                  NamePolynomOld(i)=NamePolynom(i)
                enddo
              endif
              if(allocated(NamePolynom)) deallocate(NamePolynom)
              allocate(NamePolynom(NPolynom+10))
              NPolynomMax=NPolynom+10
              if(NPolynom.gt.0) then
                do i=1,NPolynom
                  NamePolynom(i)=NamePolynomOld(i)
                enddo
                deallocate(NamePolynomOld)
              endif
            endif
            NPolynom=NPolynom+1
            NamePolynom(NPolynom)=tp80
            if(kk.lt.len(t80)) go to 1260
          endif
        else if(i.eq.5) then
1270      call kus(t80,kk,tp80)
          call UprAt(tp80)
          if(tp80.ne.' ') then
            NPolar=NPolar+1
            NamePolar(NPolar)=tp80
            if(kk.lt.len(t80)) go to 1270
          endif
        else if(i.eq.6) then
          call Kus(t80,kk,Nazev)
          call StToInt(t80,kk,kwp(1),1,.false.,ich)
          if(ich.ne.0) go to 1300
          call ReallocateXYZAMode(NXYZAMode+1,kwp(1))
          NXYZAMode=NXYZAMode+1
          LXYZAMode(NXYZAMode)=Nazev
          MXYZAMode(NXYZAMode)=kwp(1)
          read(m40,*,end=1300,err=1300)
     1      (XYZAMode(j,NXYZAMode),j=1,MXYZAMode(NXYZAMode))
          pom=0.
          do i=1,MXYZAMode(NXYZAMode)
            pom=max(pom,abs(XYZAMode(i,NXYZAMode)))
          enddo
          do i=1,MXYZAMode(NXYZAMode)
            XYZAMode(i,NXYZAMode)=XYZAMode(i,NXYZAMode)/pom
          enddo
        else if(i.eq.7) then
          call Kus(t80,kk,Nazev)
          call StToInt(t80,kk,kwp(1),1,.false.,ich)
          if(ich.ne.0) go to 1300
          call ReallocateMagAMode(NMagAMode+1,kwp(1))
          NMagAMode=NMagAMode+1
          LMagAMode(NMagAMode)=Nazev
          MMagAMode(NMagAMode)=kwp(1)
          read(m40,*,end=1300,err=1300)
     1      (MagAMode(j,NMagAMode),j=1,MMagAMode(NMagAMode))
          px=0.
          do i=1,MMagAMode(NMagAMode)
            j=mod(i-1,3)+1
            px(j)=max(px(j),abs(MagAMode(i,NMagAMode)))
          enddo
          do i=1,MMagAMode(NMagAMode)
            j=mod(i-1,3)+1
            if(px(j).gt..0001)
     1        MagAMode(i,NMagAMode)=MagAMode(i,NMagAMode)/px(j)
          enddo
        else if(i.eq.8) then
          call ReallocateAtSplit(1,2)
          NAtSplit=NAtSplit+1
          MAtSplit(NAtSplit)=0
1280      if(kk.lt.len(t80)) then
            call ReallocateAtSplit(0,MAtSplit(NAtSplit)+1)
            MAtSplit(NAtSplit)=MAtSplit(NAtSplit)+1
            call kus(t80,kk,AtSplit(MAtSplit(NAtSplit),NAtSplit))
            IAtSplit(MAtSplit(NAtSplit),NAtSplit)=0
            go to 1280
          endif
        else if(i.eq.9) then
          call ReallocateMolSplit(1,2)
          NMolSplit=NMolSplit+1
          MMolSplit(NMolSplit)=0
1290      if(kk.lt.len(t80)) then
            call ReallocateMolSplit(0,MMolSplit(NMolSplit)+1)
            MMolSplit(NMolSplit)=MMolSplit(NMolSplit)+1
            call kus(t80,kk,MolSplit(MMolSplit(NMolSplit),NMolSplit))
            IMolSplit(MMolSplit(NMolSplit),NMolSplit)=0
            go to 1290
          endif
        else
          go to 1300
        endif
        go to 1200
1300    call FeChybne(-1.,-1.,'syntactic error in the following '//
     1                'command on M40:',t80,SeriousError)
        ErrFlag=1
        go to 9999
1400    PrvniKi=ndoff+1
        PrvniKiAtInd=PrvniKi
        do i=1,nl
          read(m40,FormA,err=9000,end=9000) t80
        enddo
1460    if(NAtInd.le.0.and.NMolec.le.0) then
          PosledniKiAtInd=PrvniKi-1
          PrvniKiAtPos=PrvniKi
          PosledniKiAtPos=PosledniKiAtInd
          PrvniKiAtMol=PrvniKi
          PosledniKiAtMol=PosledniKiAtInd
        endif
        if(tisk.eq.1) then
          if(line.gt.40) then
            line=mxline
          else
            call newln(1)
            write(lst,FormA)
          endif
          do KPh=1,NPhase
            KPhase=KPh
            call newln(NComp(KPh)+4)
            t80='M40 file contains'
            if(NPhase.gt.1) t80=t80(:idel(t80)+1)//' - '//
     1        PhaseName(KPh)
            write(lst,FormA) t80(:idel(t80))
            if(NComp(KPh).eq.1) then
              write(lst,'(''Number of atoms : '',i3,'' number of '',
     1                    ''rigid body modulated parts : '',i3)')
     2                    NAtIndLen(1,KPh),NMolecLen(1,KPh)
            else
              do i=1,NComp(KPh)
                write(lst,'(''Number of atoms : '',i3,'' number of '',
     1                      ''rigid body modulated parts : '',i3,
     2                      '' for composite part #'',i1)')
     3                    NAtIndLen(i,KPh),NMolecLen(i,KPh)
              enddo
            endif
            write(lst,FormA)
          enddo
        endif
      else
        call OpenFile(m40,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_peaks.tmp','formatted','unknown')
        if(ErrFlag.ne.0) then
          ln=0
          go to 1700
        endif
        n=0
1600    if(OldFile) then
          if(index(t80,'**********').le.0) go to 1600
        else
          read(m40,FormA,end=1650) t80
          if(t80(1:10).ne.'----------'.or.
     1      (LocateSubstring(t80,'Fourier maxima',.false.,.true.).le.0
     2        .and.
     3       LocateSubstring(t80,'Fourier minima',.false.,.true.).le.0
     4        .and.
     5       LocateSubstring(t80,'Saved points',.false.,.true.).le.0))
     6      go to 1600
        endif
        n=1
1610    write(ln,FormA) t80(:idel(t80))
        read(m40,FormA,end=1650) t80
        go to 1610
1650    rewind m40
        if(n.eq.0) then
          call CloseIfOpened(ln)
          call DeleteFile(fln(:ifln)//'_peaks.tmp')
          ln=0
        else
          rewind ln
        endif
1700    call SetIntArrayTo(nw,MxPhases,0)
        nwall=0
        do k=1,NPhase
          KPhase=k
          if(NDim(KPhase).gt.3) then
            do i=1,mxw
              call SetIntArrayTo(kwp,3,0)
              if(NDim(KPhase).eq.4.or.i.le.NDimI(k))
     1          kwp(mod(i-1,NDimI(k))+1)=(i-1)/NDimI(k)+1
              if(.not.eqiv(kwp,kw(1,i,k),NDimI(k))) then
                nw(k)=nw(k)+1
                nwall=nwall+1
                iw(nw(k),k)=i
              endif
            enddo
          endif
        enddo
        if(mxscutw.gt.6.or.nwall.gt.0.or.
     1     NPolar.gt.0.or.NXYZAMode.gt.0.or.NMagAMode.gt.0.or.
     2     NAtSplit.gt.0.or.NMolSplit.gt.0) then
          write(m40,'(''commands'')')
          if(mxscutw.ne.6) write(m40,'(a,1x,i2)')
     1      id(1)(:idel(id(1))),mxscutw
          if(NPhase.le.1) then
            do i=1,nw(1)
              write(m40,106) id(3)(:idel(id(3))),iw(i,1),
     1          (kw(j,iw(i,1),1),j=1,NDimI(1))
            enddo
          else
            do KPh=1,NPhase
              KPhase=KPh
              do i=1,nw(KPh)
                write(m40,106) id(3)(:idel(id(3))),iw(i,KPh),KPh,
     1            (kw(j,iw(i,KPh),KPh),j=1,NDimI(KPh))
              enddo
            enddo
          endif
          t80=id(5)
          do i=1,NPolar
            idl1=idel(NamePolar(i))
            idl2=idel(t80)+1
            if(idl1+idl2.gt.len(t80)) then
              write(m40,FormA) t80(:idel(t80))
              t80=id(5)
              idl2=idel(t80)+1
            endif
            t80=t80(:idl2)//NamePolar(i)(:idl1)
          enddo
          if(idel(t80).gt.idel(id(5))) write(m40,FormA) t80(:idel(t80))
          do i=1,NXYZAMode
            write(Cislo,FormI15) MXYZAMode(i)
            call Zhusti(Cislo)
            t80=Id(6)(:idel(id(6)))//' '//
     1          LXYZAMode(i)(:idel(LXYZAMode(i)))//' '//
     2          Cislo(:idel(Cislo))
            write(m40,FormA) t80(:idel(t80))
            write(m40,107)(XYZAMode(j,i),j=1,MXYZAMode(i))
          enddo
          do i=1,NMagAMode
            write(Cislo,FormI15) MMagAMode(i)
            call Zhusti(Cislo)
            t80=Id(7)(:idel(id(7)))//' '//
     1          LMagAMode(i)(:idel(LMagAMode(i)))//' '//
     2          Cislo(:idel(Cislo))
            write(m40,FormA) t80(:idel(t80))
            write(m40,107)(MagAMode(j,i),j=1,MMagAMode(i))
          enddo
          do i=1,NAtSplit
            t80=Id(8)(:idel(id(8)))//' '//
     1          AtSplit(1,i)(:idel(AtSplit(1,i)))//' '//
     2          AtSplit(2,i)(:idel(AtSplit(2,i)))
            write(m40,FormA) t80(:idel(t80))
          enddo
          do i=1,NMolSplit
            t80=Id(9)(:idel(id(9)))//' '//
     1          MolSplit(1,i)(:idel(MolSplit(1,i)))//' '//
     2          MolSplit(2,i)(:idel(MolSplit(2,i)))
            write(m40,FormA) t80(:idel(t80))
          enddo
          write(m40,'(''end'')')
        endif
        k=0
        do KPh=1,NPhase
          KPhase=KPh
          write(m40,104)
     1     (NAtIndLen(i,KPh),NMolecLen(i,KPh),i=1,NComp(KPh)),lite(KPh),
     2     irot(KPh)
          do i=1,NComp(KPh)
            do j=NMolecFr(i,KPh),NMolecTo(i,KPh)
               k=k+1
              write(m40,104) iam(k)/KPoint(k),mam(k)
            enddo
          enddo
        enddo
      endif
      mxscu=mxscutw
      do KDatB=1,max(NDatBlock,1)
        itwph=max(NTwin,NPhase)
        mxscu=min(mxscutw-itwph+1,mxscu)
        if(klic.ge.1)
     1    call CopyVek(sctw(2,KDatB),sc(mxscu+1,KDatB),itwph-1)
        call iost(m40,sc(1,KDatB),kis(1,KDatB),mxscutw,klic,0)
        if(ErrFlag.ne.0) go to 9000
        if(iabs(DataType(KDatB)).eq.2) then
          if(KManBackg(KDatB).gt.0.and.sc(2,KDatB).le.0.) sc(2,KDatB)=1.
        endif
        sctw(1,KDatB)=1.
        if(itwph.gt.1) then
          j=mxscu+1
          do i=2,itwph
            sctw(i,KDatB)=sc(j,KDatB)
            sctw(1,KDatB)=sctw(1,KDatB)-sctw(i,KDatB)
            sc(j,KDatB)=0.
            j=j+1
          enddo
        endif
        ip=MxSc+1
        if(MaxMagneticType.le.0) then
          n=2
        else
          n=4
        endif
        if(Klic.ge.1) then
          k=1
          px(k)=OverAllB(KDatB)
          kip(k)=kis(ip,KDatB)
          if(MaxMagneticType.gt.0) then
            do i=1,2
              k=k+1
              ip=ip+1
              kip(k)=kis(ip,KDatB)
              px(k)=ecMag(i,KDatB)
            enddo
          else
            ip=ip+2
          endif
          k=k+1
          ip=ip+1
          kip(k)=kis(ip,KDatB)
          px(k)=ScLam2(KDatB)
        endif
        call iost(m40,px,kip,n,klic,0)
        if(Klic.le.0) then
          k=1
          OverAllB(KDatB)=px(k)
          kis(ip,KDatB)=kip(k)
          if(MaxMagneticType.gt.0) then
            do i=1,2
              k=k+1
              ip=ip+1
              kis(ip,KDatB)=kip(k)
              if(px(k).le.0.) then
                ecMag(i,KDatB)=1.
              else
                ecMag(i,KDatB)=px(k)
              endif
            enddo
          else
            ip=ip+2
          endif
          k=k+1
          ip=ip+1
          kis(ip,KDatB)=kip(k)
          ScLam2(KDatB)=px(k)
        endif
        ip=MxSc+7
        call iost(m40,ec(1,KDatB),kis(ip,KDatB),12,klic,0)
        if(ErrFlag.ne.0) go to 9000
        ip=ip+12
        do KPh=1,NPhase
          if(KAnRef(KDatB).ne.0) then
            call iost(m40,FFrRef(1,KPh,KDatB),kis(ip,KDatB),
     1                NAtFormula(KPh),klic,0)
            if(ErrFlag.ne.0) go to 9000
            call iost(m40,FFiRef(1,KPh,KDatB),
     1                kis(ip+MaxNAtFormula,KDatB),
     2                NAtFormula(KPh),klic,0)
            if(ErrFlag.ne.0) go to 9000
          endif
          ip=ip+MaxNAtFormula
        enddo
      enddo
      j=0
      do KPh=1,NPhase
        KPhase=KPh
        NAtH(KPh)=0
        do isw=1,NComp(KPh)
          call ioat(NAtIndFr(isw,KPh),NAtIndTo(isw,KPh),isw,0,klic,0)
          if(ErrFlag.eq.0) then
            cycle
          else if(ErrFlag.eq.1) then
            go to 9000
          else
          endif
        enddo
      enddo
      if(klic.le.0) then
        PosledniKiAtInd=PrvniKi-1
        PrvniKiAtPos=PrvniKi
        PosledniKiAtPos=PosledniKiAtInd
        PrvniKiAtMol=PrvniKi
        PosledniKiAtMol=PosledniKiAtInd
        PrvniKiMol=PrvniKi
        PosledniKiMol=PosledniKiAtInd
      else
        PrvniKi=PrvniKiAtMol
      endif
      if(NMolec.gt.0.and.Klic.le.0)
     1  call AllocateMolecules(NMolec,NPosMax)
      do KPh=1,NPhase
        KPhase=KPh
        call iomol(klic,tisk)
        if(ErrFlag.ne.0) go to 9000
      enddo
      if(Klic.eq.0) then
        PrvniKiAtXYZMode=1
        PosledniKiAtXYZMode=0
        PrvniKiAtMagMode=1
        PosledniKiAtMagMode=0
      endif
      if(NXYZAMode.gt.0) then
        if(Klic.le.0) then
3100      read(m40,FormA,err=9000,end=3200) t80
          k=0
          call Kus(t80,k,Nazev)
          if(t80(1:10).eq.'----------'.or.EqIgCase(Nazev,'AtMagOrbit'))
     1      then
            backspace m40
            go to 3200
          else if(t80.eq.' ') then
            go to 3200
          endif
          if(EqIgCase(Nazev,'AtOrbit')) then
            call ReallocateAtXYZMode(10)
            NAtXYZMode=NAtXYZMode+1
            JAtXYZMode(NAtXYZMode)=0
            n=0
3120        call Kus(t80,k,Nazev)
            ia=ktat(Atom,NAtCalc,Nazev)
            if(ia.le.0) then
              call FeChybne(-1.,-1.,'atom "'//Nazev(:idel(Nazev))//
     1                      '" used in the atom orbit does not exist',
     2                      ' ',SeriousError)
              ErrFlag=1
              go to 9999
            endif
            n=n+1
            IAtXYZMode(n,NAtXYZMode)=ia
            if(k.lt.len(t80)) go to 3120
            read(m40,FormA,err=9000,end=3500) t80
            k=0
            call Kus(t80,k,Nazev)
            if(EqIgCase(Nazev,'AtOrbit')) then
              go to 3120
            else
              backspace m40
            endif
            MAtXYZMode(NAtXYZMode)=n
            m=0
          else
            m=m+1
            NMAtXYZMode(NAtXYZMode)=m
            read(t80,108,err=9000,end=9000) LAtXYZMode(m,NAtXYZMode),
     1                                       AtXYZMode(m,NAtXYZMode),
     2                                     KiAtXYZMode(m,NAtXYZMode)
            SAtXYZMode(m,NAtXYZMode)=0.
            if(Klic.eq.0) PosledniKiAtXYZMode=PosledniKiAtXYZMode+1
          endif
          go to 3100
3200      if(NAtXYZMode.gt.0) call SetAtXYZMode
        else
          do i=1,NAtXYZMode
            t80='AtOrbit'
            idl=idel(t80)
            do j=1,MAtXYZMode(i)
              ia=IAtXYZMode(j,i)
              idla=idel(Atom(ia))
              if(idl+idla+1.gt.len(t80)) then
                write(m40,FormA) t80(:idl)
                t80='AtOrbit'
                idl=idel(t80)
              endif
              t80=t80(:idl)//' '//Atom(ia)(:idla)
              idl=idl+idla+1
            enddo
            if(idl.gt.8) write(m40,FormA) t80(:idl)
            do j=1,NMAtXYZMode(i)
              write(m40,108) LAtXYZMode(j,i),AtXYZMode(j,i),
     1                       KiAtXYZMode(j,i)
            enddo
          enddo
        endif
        if(NAtXYZMode.gt.0) call TrXYZMode(0)
      endif
      if(NMagAMode.gt.0) then
        if(Klic.le.0) then
3300      read(m40,FormA,err=9000,end=3400) t80
          if(t80(1:10).eq.'----------') then
            backspace m40
            go to 3400
          else if(t80.eq.' ') then
            go to 3400
          endif
          k=0
          call Kus(t80,k,Nazev)
          if(EqIgCase(Nazev,'AtMagOrbit')) then
            call ReallocateAtMagMode(10)
            NAtMagMode=NAtMagMode+1
            JAtMagMode(NAtMagMode)=0
            n=0
3320        call Kus(t80,k,Nazev)
            ia=ktat(Atom,NAtCalc,Nazev)
            if(ia.le.0) then
              call FeChybne(-1.,-1.,'atom "'//Nazev(:idel(Nazev))//
     1                      '" used in the atom orbit does not exist',
     2                      ' ',SeriousError)
              ErrFlag=1
              go to 9999
            endif
            n=n+1
            IAtMagMode(n,NAtMagMode)=ia
            if(k.lt.len(t80)) go to 3320
            read(m40,FormA,err=9000,end=3500) t80
            k=0
            call Kus(t80,k,Nazev)
            if(EqIgCase(Nazev,'AtOrbit')) then
              go to 3320
            else
              backspace m40
            endif
            MAtMagMode(NAtMagMode)=n
            m=0
          else
            m=m+1
            NMAtMagMode(NAtMagMode)=m
            read(t80,108,err=9000,end=9000) LAtMagMode(m,NAtMagMode),
     1                                       AtMagMode(m,NAtMagMode),
     2                                     KiAtMagMode(m,NAtMagMode)
            SAtMagMode(m,NAtMagMode)=0.
            if(Klic.eq.0) PosledniKiAtMagMode=PosledniKiAtMagMode+1
          endif
          go to 3300
3400      if(NAtMagMode.gt.0) call SetAtMagMode
        else
          do i=1,NAtMagMode
            t80='AtMagOrbit'
            idl=idel(t80)
            do j=1,MAtMagMode(i)
              ia=IAtMagMode(j,i)
              idla=idel(Atom(ia))
              if(idl+idla+1.gt.len(t80)) then
                write(m40,FormA) t80(:idl)
                t80='AtOrbit'
                idl=idel(t80)
              endif
              t80=t80(:idl)//' '//Atom(ia)(:idla)
              idl=idl+idla+1
            enddo
            if(idl.gt.8) write(m40,FormA) t80(:idl)
            do j=1,NMAtMagMode(i)
              write(m40,108) LAtMagMode(j,i),AtMagMode(j,i),
     1                       KiAtMagMode(j,i)
            enddo
          enddo
        endif
        if(NAtMagMode.gt.0) call TrMagMode(0)
      endif
      if(.not.OldFile) then
        if(klic.eq.0) then
          read(m40,FormA,err=3500,end=3500) t80
          if(t80(1:10).ne.'----------') then
            backspace m40
          else
            if(LocateSubstring(t80,'s.u. block',.false.,.true.).le.0)
     1        go to 3500
          endif
        else
          write(m40,'(28(''-''),''   s.u. block   '',28(''-''))')
        endif
        do KDatB=1,max(NDatBlock,1)
          if(klic.eq.0) then
            call iosts(m40,ScS(1,KDatB),mxscutw,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            call CopyVek(ScS(mxscu+1,KDatB),ScTwS(2,KDatB),itwph-1)
            sctws(1,KDatB)=0.
            do i=2,itwph
              sctws(1,KDatB)=sctws(1,KDatB)+sctws(i,KDatB)**2
            enddo
            sctws(1,KDatB)=sqrt(sctws(1,KDatB))
            call iosts(m40,px,6,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            k=1
            OverAllBS(KDatB)=px(k)
            if(MaxMagneticType.gt.0) then
              do i=1,2
                k=k+1
                ecsMag(i,KDatB)=px(k)
              enddo
            endif
            k=k+1
            ScLam2S(KDatB)=px(k)
            call iosts(m40,ecs(1,KDatB),12,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            do KPh=1,NPhase
              if(KAnRef(KDatB).ne.0) then
                read(m40,101,err=3500,end=3500)
     1            (sFFrRef(i,KPh,KDatB),i=1,NAtFormula(KPh))
                read(m40,101,err=3500,end=3500)
     1            (sFFiRef(i,KPh,KDatB),i=1,NAtFormula(KPh))
              endif
            enddo
          else
            call CopyVek(ScTwS(2,KDatB),ScS(mxscu+1,KDatB),itwph-1)
            call iosts(m40,ScS(1,KDatB),mxscutw,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            k=1
            px(k)=OverAllBS(KDatB)
            if(MaxMagneticType.gt.0) then
              do i=1,2
                k=k+1
                px(k)=ecsMag(i,KDatB)
              enddo
            endif
            k=k+1
            px(k)=ScLam2S(KDatB)
            call iosts(m40,px,k,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            call iosts(m40,ecs(1,KDatB),12,klic)
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 3500
            endif
            do KPh=1,NPhase
              if(KAnRef(KDatB).ne.0) then
                write(m40,101)(sFFrRef(i,KPh,KDatB),i=1,NAtFormula(KPh))
                write(m40,101)(sFFiRef(i,KPh,KDatB),i=1,NAtFormula(KPh))
              endif
            enddo
          endif
        enddo
      endif
      do KPh=1,NPhase
        KPhase=KPh
        call ioats(NAtIndFrAll(KPh),NAtIndToAll(KPh),klic,*3500)
      enddo
      do KPh=1,NPhase
        KPhase=KPh
        call iomols(klic,*3500)
      enddo
      if(NAtXYZMode.gt.0) then
        if(Klic.le.0) then
          do i=1,NAtXYZMode
            read(m40,FormA,err=3500,end=3500) t80
            read(m40,108,err=3500,end=3500)
     1        (Nazev,SAtXYZMode(j,i),j=1,NMAtXYZMode(i))
          enddo
        else
          do i=1,NAtXYZMode
            t80='AtOrbit'
            do j=1,MAtXYZMode(i)
              t80=t80(:idel(t80))//' '//Atom(IAtXYZMode(j,i))
            enddo
            write(m40,FormA) t80(:idel(t80))
            do j=1,NMAtXYZMode(i)
              write(m40,108) LAtXYZMode(j,i),SAtXYZMode(j,i)
            enddo
          enddo
        endif
      endif
      if(NAtMagMode.gt.0) then
        if(Klic.le.0) then
          do i=1,NAtMagMode
            read(m40,FormA,err=3500,end=3500) t80
            read(m40,108,err=3500,end=3500)
     1        (Nazev,SAtMagMode(j,i),j=1,NMAtMagMode(i))
          enddo
        else
          do i=1,NAtMagMode
            t80='AtMagOrbit'
            do j=1,MAtMagMode(i)
              t80=t80(:idel(t80))//' '//Atom(IAtMagMode(j,i))
            enddo
            write(m40,FormA) t80(:idel(t80))
            do j=1,NMAtMagMode(i)
              write(m40,108) LAtMagMode(j,i),SAtMagMode(j,i)
            enddo
          enddo
        endif
      endif
      go to 5000
3500  if(klic.eq.0) then
        do KDatB=1,max(NDatBlock,1)
          call SetRealArrayTo(ScS(1,KDatB),mxscutw,0.)
          OverAllBS(KDatB)=0
          call SetRealArrayTo(ecs(1,KDatB),12,0.)
          do KPh=1,NPhase
            if(KAnRef(KDatB).ne.0) then
              n=NAtFormula(KPh)
              call SetRealArrayTo(sFFrRef(1,KPh,KDatB),n,0.)
              call SetRealArrayTo(sFFiRef(1,KPh,KDatB),n,0.)
            endif
          enddo
        enddo
        do i=1,NAtAll
          k=0
          if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
          do j=0,max(itf(i),2)
            kmod=KModA(j+1,i)
            n=TRank(j)
            if(j.eq.0) then
              sai(i)=0
              if(kmod.gt.0) then
                sa0(i)=0.
                call SetRealArrayTo(sax(1,i),kmod*n,0.)
                call SetRealArrayTo(say(1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.1) then
              call SetRealArrayTo(sx(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sux(1,1,i),kmod*n,0.)
                call SetRealArrayTo(suy(1,1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.2) then
              call SetRealArrayTo(sbeta(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sbx(1,1,i),kmod*n,0.)
                call SetRealArrayTo(sby(1,1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.3) then
              call SetRealArrayTo(sc3(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sc3x(1,1,i),kmod*n,0.)
                call SetRealArrayTo(sc3y(1,1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.4) then
              call SetRealArrayTo(sc4(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sc4x(1,1,i),kmod*n,0.)
                call SetRealArrayTo(sc4y(1,1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.5) then
              call SetRealArrayTo(sc5(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sc5x(1,1,i),kmod*n,0.)
                call SetRealArrayTo(sc5y(1,1,i),kmod*n,0.)
                k=1
              endif
            else if(j.eq.6) then
              call SetRealArrayTo(sc6(1,i),n,0.)
              if(kmod.gt.0) then
                call SetRealArrayTo(sc6x(1,1,i),kmod*n,0.)
                call SetRealArrayTo(sc6y(1,1,i),kmod*n,0.)
                k=1
              endif
            endif
          enddo
          if(k.ne.0) sphf(i)=0
          do j=1,MagPar(i)
            if(j.eq.1) then
              call SetRealArrayTo(ssm0(1,i),3,0.)
            else
              call SetRealArrayTo(ssmx(1,j-1,i),3,0.)
              call SetRealArrayTo(ssmy(1,j-1,i),3,0.)
            endif
          enddo
          if(lasmax(i).gt.0) then
            spopc(i)=0.
            spopv(i)=0.
            skapa1(i)=0.
            skapa2(i)=0.
            k=(lasmax(i)-1)**2
            call SetRealArrayTo(spopas(1,i),k,0.)
          endif
        enddo
        do i=1,NMolec
          do j=1,mam(i)
            ji=j+mxp*(i-1)
            saimol(ji)=0.
            call SetRealArrayTo(seuler(1,ji),3,0.)
            call SetRealArrayTo(strans(1,ji),3,0.)
            if(KTLS(i).gt.0) then
              call SetRealArrayTo(stt(1,ji),6,0.)
              call SetRealArrayTo(stl(1,ji),6,0.)
              call SetRealArrayTo(sts(1,ji),9,0.)
            endif
            if(KModM(1,ji).gt.0) then
              n=KModM(1,ji)
              sa0m(ji)=0.
              call SetRealArrayTo(saxm(1,ji),n,0.)
              call SetRealArrayTo(saym(1,ji),n,0.)
            endif
            if(KModM(2,ji).gt.0) then
              n=KModM(2,ji)*3
              call SetRealArrayTo(sutx(1,1,ji),n,0.)
              call SetRealArrayTo(suty(1,1,ji),n,0.)
              call SetRealArrayTo(surx(1,1,ji),n,0.)
              call SetRealArrayTo(sury(1,1,ji),n,0.)
            endif
            if(KModM(3,ji).gt.0) then
              n=KModM(2,ji)*6
              call SetRealArrayTo(sttx(1,1,ji),n,0.)
              call SetRealArrayTo(stty(1,1,ji),n,0.)
              call SetRealArrayTo(stlx(1,1,ji),n,0.)
              call SetRealArrayTo(stly(1,1,ji),n,0.)
              n=KModM(2,ji)*9
              call SetRealArrayTo(stsx(1,1,ji),n,0.)
              call SetRealArrayTo(stsy(1,1,ji),n,0.)
            endif
            if(NDimI(KPhase).gt.0) sphfm(ji)=0.
          enddo
        enddo
      endif
cccc  pridat nulovani alokovanych sig parametru
5000  if(NAtMol.gt.0) call SetMol(klic,tisk)
      if(Klic.eq.0) then
        PrvniKiAtXYZMode=PosledniKiMol+PrvniKiAtXYZMode
        PosledniKiAtXYZMode=PosledniKiMol+PosledniKiAtXYZMode
        PrvniKiAtMagMode=PosledniKiAtXYZMode+PrvniKiAtMagMode
        PosledniKiAtMagMode=PosledniKiAtXYZMode+PosledniKiAtMagMode
      endif
      call SpecAt
      if(ErrFlag.ne.0) go to 9999
      SwitchedToHarm=.false.
      if(klic.eq.0) then
        MaxUsedKwAll=0
        MaxUsedItfAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          MaxUsedItf(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh) cycle
            MaxUsedItf(KPh)=max(MaxUsedItf(KPh),itf(i))
            if(NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
          MaxUsedItfAll=max(MaxUsedItfAll,MaxUsedItf(KPh))
        enddo
        do j=1,NAtAll
          if(j.gt.NAtInd.and.j.lt.NAtMolFr(1,1)) cycle
          do i=1,j-1
            if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
            if(EqIgCase(Atom(j),Atom(i))) go to 6100
          enddo
        enddo
        go to 6300
6100    ErrFlag=-1
      else
        if(ln.gt.0) then
6200      read(ln,FormA,end=6250) t80
          write(m40,FormA) t80(:idel(t80))
          go to 6200
6250      call CloseIfOpened(ln)
          call DeleteFile(fln(:ifln)//'_peaks.tmp')
          ln=0
        endif
      endif
6300  CrenelSawtoothExist=.false.
      CrenelExist=.false.
      do i=1,NAtInd
        if(KFA(1,i).ne.0.or.KFA(2,i).ne.0) then
          CrenelSawtoothExist=.true.
          if(KFA(1,i).ne.0) CrenelExist=.true.
        endif
      enddo
      do i=1,NMolec
        ji=(i-1)*mxp
        do j=1,mam(i)
          ji=ji+1
          if(KFM(1,ji).ne.0.or.KFM(2,ji).ne.0) then
            CrenelSawtoothExist=.true.
            if(KFM(1,ji).ne.0) CrenelExist=.true.
          endif
        enddo
      enddo
      if(Klic.eq.0) then
        do 6400k=1,NPolynom
          iat=ktat(Atom,NAtInd,NamePolynom(k))
          if(iat.gt.0) then
            if(KFA(1,iat).gt.0) TypeModFun(iat)=2
          else
            do i=1,NMolec
              t80=MolName(i)
              do j=1,mam(i)
                ji=j+mxp*(i-1)
                write(Cislo,'(''#'',i10)') j
                call Zhusti(Cislo)
                Cislo=t80(:idel(t80))//Cislo(:idel(Cislo))
                if(EqIgCase(NamePolynom(k),Cislo)) then
                  TypeModFunMol(ji)=2
                  go to 6400
                endif
              enddo
            enddo
          endif
6400    continue
        NPolynom=0
        do k=1,nor
          iat=ktat(Atom,NAtInd,ora(k))
          if(iat.gt.0) then
            if(KFA(1,iat).gt.0) then
               TypeModFun(iat)=1
               OrthoX40(iat)=orx40(k)
               OrthoDelta(iat)=ordel(k)
               OrthoEps(iat)=oreps(k)
               cycle
            endif
          endif
        enddo
        nor=0
        OrthoOrd=0
        if(allocated(OrthoSel)) deallocate(OrthoSel)
        do iat=1,NAtAll
          if(TypeModFun(iat).eq.1) then
            OrthoOrd=2*MaxUsedKwAll+1
            if(.not.allocated(OrthoSel))
     1        allocate(OrthoSel(OrthoOrd,NAtAll+1))
            call UpdateOrtho(iat,OrthoX40(iat),OrthoDelta(iat),
     1                       OrthoEps(iat),OrthoSel(1,iat),
     2                       OrthoOrd)
          endif
        enddo
        if(OrthoOrd.gt.0) then
          if(allocated(OrthoMat)) deallocate(OrthoMat,OrthoMatI)
          m=OrthoOrd**2
          allocate(OrthoMat(m,NAtAll+1),OrthoMatI(m,NAtAll+1))
          do iat=1,NAtAll
            if(TypeModFun(iat).eq.1) then
              call MatOr(OrthoX40(iat),OrthoDelta(iat),
     1                   OrthoSel(1,iat),OrthoOrd,OrthoMat(1,iat),
     2                   OrthoMatI(1,iat),iat)
            else
              call SetIntArrayTo(OrthoSel(1,iat),OrthoOrd,1)
              call UnitMat(OrthoMat (1,iat),OrthoOrd)
              call UnitMat(OrthoMatI(1,iat),OrthoOrd)
            endif
          enddo
          call trortho(0)
          MaxUsedKwAll=0
          do KPh=1,NPhase
            KPhase=KPh
            MaxUsedKw(KPh)=0
            MaxUsedItf(KPh)=0
            kmodp=0
            do i=1,NAtCalc
              if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
              do j=1,7
                MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
                kmodp(j)=max(kmodp(j),KModA(j,i))
              enddo
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
            enddo
            MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
          enddo
          call trortho(1)
          call ReallocateAtomParams(NAtAll,itfMax,ifrMax,kmodp,
     1                              MaxUsedKwAll)
        endif
        do k=1,NMolSplit
          do 6500l=1,MMolSplit(k)
            do i=1,NMolec
              t80=MolName(i)
              do j=1,mam(i)
                ji=j+mxp*(i-1)
                write(Cislo,'(''#'',i10)') j
                call Zhusti(Cislo)
                Cislo=t80(:idel(t80))//Cislo(:idel(Cislo))
                if(EqIgCase(MolSplit(l,k),Cislo)) then
                  IMolSplit(l,k)=ji
                  go to 6500
                endif
              enddo
            enddo
6500      enddo
        enddo
        do k=1,NAtSplit
          do l=1,MAtSplit(k)
            IAtSplit(l,k)=ktat(Atom,NAtCalc,AtSplit(l,k))
          enddo
        enddo
      endif
      call SetIntArrayTo(KUsePolar,NAtInd,0)
      do i=1,NPolar
        iat=ktat(Atom,NAtInd,NamePolar(i))
        if(iat.gt.0) KUsePolar(iat)=1
      enddo
      go to 9900
9000  if(NAtInd.le.0.and.NMolec.le.0) then
        ErrFlag=0
        go to 9900
      endif
      if(ErrFlag.ne.2) call FeReadError(m40)
      ErrFlag=1
      go to 9999
9900  if(ExistPowder) then
        call CloseIfOpened(m40)
        i=ErrFlag
        if(.not.only) then
          if(OldFile) then
            call OpenFile(m41,fln(:ifln)//'.m41','formatted','unknown')
            call iom41Old(klic,tisk,fln)
            call CloseIfOpened(m41)
          else
            call iom41(klic,tisk,
     1                 PureFileName(:idel(PureFileName))//'.m41')
          endif
          if(ErrFlag.ne.0) ErrFlag=2
        endif
        if(i.ne.0) ErrFlag=i
      else
        call SetIntArrayTo(kiPwd,MxParPwd,0)
      endif
9999  call CloseIfOpened(m40)
      if(Klic.ne.0) ExistM40=.true.
      if(ErrFlag.eq.0) call IOSavedPoints(Klic,PureFileName)
      if(ErrFlag.ne.0.and.ErrFlag.ne.-1.and.Klic.eq.0) then
        do KPh=1,NPhase
          do i=1,NComp(KPh)
            NAtIndLen(i,KPh)=0
            NMolecLen(i,KPh)=0
            NAtPosLen(i,KPh)=0
            NAtMolLen(i,KPh)=0
          enddo
        enddo
        call EM40UpdateMolecLimits
        NAtCalc=0
        NAtCalcBasic=0
        call EM40UpdateAtomLimits
        NAtCalcBasic=NAtCalc
        MxAtAll=NAtAll
        NMolec=0
      endif
      KPhase=KPhaseIn
      if(allocated(NamePolynom)) deallocate(NamePolynom)
      return
100   format('wave ',5i5)
101   format(6f9.6)
102   format(f15.0)
103   format(72i1)
104   format(11i5)
105   format(f9.6,45x,6x,i1)
106   format(a,1x,5i5)
107   format(6f9.5)
108   format(a8,10x,f9.6,33x,i1)
      end
