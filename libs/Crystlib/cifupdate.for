      subroutine CIFUpdate(FileIn,JenZaklad,Klic)
      use Basic_mod
      use Powder_mod
      use Refine_mod
      use Atoms_mod
      use Molec_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'refine.cmn'
      real csp(3),snp(3),xp(3),qupom(3),RPom(9)
      real, allocatable :: xyzor(:,:),betaor(:,:)
      integer iap(6),CrlMagCenter,CrlMagInver
      integer, allocatable :: ipr(:)
      character*(*) FileIn
      character*256 Veta,EdwStringQuest
      character*80  t80,p80,s80,Format83a,Format83o
      character*40  p40
      character*20  p20,s20
      character*15  IncPwd
      character*8   p8,s8
      character*5   CharQ(3)
      character*2   nty
      character*1 Znak
      integer CrlCentroSymm,CrlNumberOfFriedelPairs,Exponent10
      logical, allocatable :: BratAtom(:)
      logical CrwLogicQuest,JenZaklad,ExistSummary,ExistFile,EqIgCase,
     1        NacitatAtomy,IdenticalAtoms,FeYesNo,EqIV0,CIFAllocated,
     2        MatRealEqMinusUnitMat,Psat,MoreDomains,FullOverlap,PolMag
      data CharQ/'alpha','beta','gamma'/
      norp=0
      CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      KRefBlockIn=KRefBlock
      if(FileIn.eq.' ') then
        FileIn=fln(:ifln)//'.cif'
1050    call FeFileManager('Select input CIF file',FileIn,'*.cif',0,
     1                     .true.,ich)
        if(ich.ne.0.or.FileIn.eq.' ') go to 9999
        if(.not.ExistFile(FileIn)) then
          if(.not.FeYesNo(-1.,-1.,'The file "'//FileIn(:idel(FileIn))//
     1                  '" does not exist, try again?',0)) go to 1050
        endif
      endif
      NacitatAtomy=Klic.eq.1
c      if(.not.NacitatAtomy) then
c        KPhaseIn=KPhase
c        call iom50(0,0,fln(:ifln)//'.m50')
c        call iom40(0,0,fln(:ifln)//'.m40')
c        KPhase=KPhaseIn
c      endif
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn(:idel(FileIn)),'formatted','unknown')
      if(ErrFlag.ne.0) go to 9100
      ExistSummary=ExistFile(fln(:ifln)//'.m70')
      if(ExistSummary) then
        LnSum=NextLogicNumber()
        call OpenFile(LnSum,fln(:ifln)//'.m70','formatted','old')
        if(ErrFlag.ne.0) go to 9100
      else
        LnSum=0
      endif
      if(allocated(CIFArray)) deallocate(CIFArray)
      nCIFArray=1000
      allocate(CIFArray(nCIFArray))
      nCIFArrayUpdate=1000
      allocate(CIFArrayUpdate(nCIFArrayUpdate),BratAtom(NAtAll))
      n=0
1100  read(lni,FormA,end=1200) Veta
      n=n+1
      if(n.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
      CIFArray(n)=Veta
      go to 1100
1200  nCIFUsed=n
      if(.not.JenZaklad) call CIFUpdateFromUserFile(CIFSpecificFile)
      wmol=0.
      ami=0.
      if(NAtFormula(KPhase).gt.0) then
        do i=1,NAtFormula(KPhase)
          if(AtType(i,KPhase).eq.'C') then
            AtType(i,KPhase)='#C'
            go to 1210
          endif
        enddo
        go to 1230
1210    do i=1,NAtFormula(KPhase)
          if(AtType(i,KPhase).eq.'H') then
            AtType(i,KPhase)='#H'
            exit
          endif
        enddo
1230    allocate(ipr(NAtFormula(KPhase)))
        call SortText(NAtFormula(KPhase),AtType(1,KPhase),ipr)
        do i=1,NAtFormula(KPhase)
          if(AtType(i,KPhase).eq.'#C') AtType(i,KPhase)='C'
          if(AtType(i,KPhase).eq.'#H') AtType(i,KPhase)='H'
        enddo
        do i=1,NAtFormula(KPhase)
          j=ipr(i)
          AtMulti=AtMult(j,KPhase)
          if(i.eq.1) then
            t80=''''//AtType(j,KPhase)
          else
            t80=t80(:idel(t80))//' '//AtType(j,KPhase)
          endif
          call RoundESD(Cislo,AtMulti,0.,6,0,1)
          t80=t80(:idel(t80))//Cislo(:idel(Cislo))
          if(.not.isED.and..not.isTOF) then
            call CrlReadAbsCoeff(1,AtType(j,KPhase),pom,ich)
            if(ich.ne.0) cycle
            ami=ami+pom*AtMulti*.1
          endif
          call RealFromAtomFile(AtTypeFull(j,KPhase),'atweight',pomw,0,
     1                          ich)
          if(ich.ne.0) cycle
          wmol=wmol+pomw*AtMulti
        enddo
        t80=t80(:idel(t80))//''''
      else
        t80='?'
      endif
      p1=1.
      do i=1,3
        pom=CellPar(i+3,1,KPhase)*torad
        csp(i)=cos(pom)
        snp(i)=sin(pom)
        p1=p1*csp(i)
      enddo
      p2=(CellVol(1,KPhase)/(CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1                       CellPar(3,1,KPhase)))**2
      SVol=0.
      do i=1,6
        if(i.le.3) then
          SVol=SVol+(CellParSU(i,1,KPhase)/CellPar(i,1,KPhase))**2
        else
          SVol=SVol+((p1/csp(i-3)-csp(i-3))/p2*snp(i-3)*
     1               CellParSU(i,1,KPhase)*torad)**2
        endif
      enddo
      SVol=sqrt(SVol)*CellVol(1,KPhase)
      dx=wmol*float(NUnits(KPhase))*1.e24/
     1   (CellVol(1,KPhase)*AvogadroNumber)
      sdx=dx*SVol/CellVol(1,KPhase)
      ami=ami*float(NUnits(KPhase))/CellVol(1,KPhase)
      m=23
      n=6
      if(idel(t80).le.39) then
        write(CIFArrayUpdate(1),FormCIF)
     1    CifKey(m,n),t80(:idel(t80))
        l=1
      else
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n)
        CIFArrayUpdate(2)=t80
        l=2
      endif
      call CIFUpdateRecord(CifKey(m,n),'CHEMICAL DATA',l)
      if(.not.JenZaklad) then
        m=24
        l=1
        write(Cislo,'(f10.1)') wmol
        call ZdrcniCisla(Cislo,1)
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'CHEMICAL DATA',l)
        m=1
        if(CrlCentroSymm().gt.0) then
          Cislo='.'
        else
          Cislo='?'
        endif
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'CHEMICAL DATA',l)
      endif
      m=1
      n=18
      l=1
      if(CrSystem(KPhase).gt.0) then
        Cislo=CrSystemName(mod(CrSystem(KPhase),10))
        call mala(Cislo)
      else
        Cislo='unknown'
      endif
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      call FindSmbSg(t80,.false.,1)
!      do i=1,idel(t80)
!        if(t80(i:i).eq.'''') t80(i:i)=' '
!      enddo
      call Zhusti(t80)
      i1=index(t80,'[')
      i2=index(t80,']')
      if(i1.gt.0.and.i2.gt.0) t80=t80(:i1-1)//t80(i2+1:)
      if(NDimI(KPhase).eq.1) then
        m=2
        n=21
        j=index(t80,'(')
        do i=1,3
          k=LocateSubstring(t80(j:),CharQ(i)(1:1),.false.,.true.)
          if(k.gt.0) then
            k=k+j-1
            t80=t80(:k-1)//ObrLom//t80(k:idel(t80))
          endif
        enddo
      else
        m=4
        n=18
        ln=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 2070
        read(ln,FormA) Veta
        if(Veta(1:1).ne.'#') rewind ln
        i=0
2062    read(ln,FormSG,err=2070,end=2070) igi,igi,igi,s8,p40,p20,p20,s20
        i=i+1
        if(mod(CrSystem(KPhase),10).eq.CrSystemMonoclinic.and.
     1     Monoclinic(KPhase).ne.mod(i,3)+1) then
          p40='?'
          go to 2062
        endif
        if(t80.eq.s8) then
!          if(.not.MakeCIFForGraphicViewer.and.p20.ne.s20) then
!            ich=0
!            id=NextQuestId()
!            xqd=300.
!            il=2
!            p80='Select Hermann-Mauguin symbol to be used:'
!            call FeQuestCreate(id,-1.,-1.,xqd,il,p80,0,LightGray,-1,0)
!            il=0
!            xpom=5.
!            tpom=xpom+CrwgXd+5.
!            do i=1,2
!              il=il+1
!              if(i.eq.1) then
!                p80=''''//p20(:idel(p20))//''' - %full symbol'
!              else
!                p80=''''//s20(:idel(s20))//''' - %short symbol'
!              endif
!              call FeQuestCrwMake(id,tpom,il,xpom,il,p80,'L',CrwgXd,
!     1                            CrwgYd,0,1)
!              if(i.eq.1) nCrwSymbol=CrwLastMade
!              call FeQuestCrwOpen(CrwLastMade,i.eq.2)
!            enddo
!2065        call FeQuestEvent(id,ich)
!            if(CheckType.ne.0) then
!              call NebylOsetren
!              go to 2065
!            endif
!            if(CrwLogicQuest(nCrwSymbol)) then
!              t80=p20
!            else
!              t80=s20
!            endif
!            call FeQuestRemove(id)
!          else
            t80=s20
!          endif
        else
          p40='?'
          go to 2062
        endif
2070    call CloseIfOpened(ln)
      endif
      if(t80.eq.'?'.or.t80.eq.' ') then
        t80='?'
      else
        t80=''''//t80(:idel(t80))//''''
      endif
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      if(NDimI(KPhase).le.0) then
        i=index(p40,'#')
        if(i.gt.0) p40(i:)=' '
        m=3
        if(p40.ne.'?') then
          if(p40(1:1).eq.'-') then
            i=2
          else
            i=1
          endif
          t80=''''//p40(1:i)
          t80=t80(:idel(t80)+1)//p40(i+1:idel(p40))//''''
          i=0
2080      i=i+1
          if(t80(i:i).eq.';') then
            t80(i:i)=' '
            if(i.lt.idel(t80)) go to 2080
          endif
        else
          if(JenZaklad) then
            t80='?'
            go to 2083
          endif
          il=2
          xqd=400.
          id=NextQuestId()
          call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
          il=1
          tpom=5.
          p80='The Hall''s symbol for the space group "'//
     1        Grupa(KPhase)(:idel(Grupa(KPhase)))//
     2        '" has not been found'
          call FeQuestLblMake(id,tpom,il,p80,'L','N')
          p80='Please complete the symbol:'
          xpom=tpom+FeTxLength(p80)+3.
          dpomp=80.
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,p80,'L',dpomp,EdwYd,0)
          nEdwHall=EdwLastMade
          t80=Grupa(KPhase)(1:1)
          if(CrlCentroSymm().gt.0) t80='-'//t80(1:1)
          call FeQuestStringEdwOpen(EdwLastMade,t80)
2082      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 2082
          endif
          t80=EdwStringQuest(nEdwHall)
          call FeQuestRemove(id)
        endif
2083    write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
        m=2
        n=18
        if(NGrupa(KPhase).gt.0) then
          write(t80,'(i5)') NGrupa(KPhase)
          call Zhusti(t80)
        else
          t80='?'
        endif
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      endif
      CIFArrayUpdate(1)='loop_'
      n=21
      if(NDimI(KPhase).eq.0) then
        if(MagneticType(KPhase).gt.0) then
          i=39
          j=41
        else
          i=8
          j=9
        endif
      else
        if(MagneticType(KPhase).gt.0) then
          i=69
          j=70
        else
          i=6
          j=7
        endif
      endif
!      Veta='_space_group_symop.magn_id'
      Veta=CifKey(i,n)
      CIFArrayUpdate(2)=' '//Veta(:idel(Veta))
      CIFArrayUpdate(3)=' '//CifKey(j,n)(:idel(CifKey(j,n)))
      if(MagneticType(KPhase).gt.0) then
        l=0
        do i=1,NSymmN(KPhase)
          l=l+1
          call CodeSymmCIF(i,1,t80)
          write(Cislo,FormI15) l
          call Zhusti(Cislo)
          if(ZMag(i,1,KPhase).gt.0.) then
            t80=t80(:idel(t80))//',+1'
          else
            t80=t80(:idel(t80))//',-1'
          endif
          CIFArrayUpdate(l+3)=' '//Cislo(1:4)//t80(1:idel(t80))
        enddo
      else
        l=0
        do k=1,NLattVec(KPhase)
          do i=1,NSymm(KPhase)
            l=l+1
            call CodeSymmCIF(i,k,t80)
            write(Cislo,FormI15) l
            call Zhusti(Cislo)
            CIFArrayUpdate(l+3)=' '//Cislo(1:4)//t80(1:idel(t80))
          enddo
        enddo
      endif
      lmx=l+3
      call CIFUpdateRecord(CifKey(j,n),'CRYSTAL DATA',lmx)
      if(MagneticType(KPhase).gt.0) then
        n=21
        CIFArrayUpdate(1)='loop_'
        if(NDimI(KPhase).eq.0) then
          i=46
          j=48
        else
          i=73
          j=74
        endif
        CIFArrayUpdate(2)=' '//CifKey(i,n)(:idel(CifKey(i,n)))
        CIFArrayUpdate(3)=' '//CifKey(j,n)(:idel(CifKey(j,n)))
        l=0
        kk=0
        do k=1,NLattVec(KPhase)
          l=l+1
          call CodeSymmCIF(1,k,t80)
          write(Cislo,FormI15) l
          call Zhusti(Cislo)
          t80=t80(:idel(t80))//',+1'
          CIFArrayUpdate(l+3)=' '//Cislo(1:4)//t80(1:idel(t80))
        enddo
        i=iabs(CrlMagCenter())
        if(i.gt.0) then
          do k=1,NLattVec(KPhase)
            l=l+1
            call CodeSymmCIF(i,k,t80)
            write(Cislo,FormI15) l
            call Zhusti(Cislo)
            if(ZMag(i,1,KPhase).gt.0.) then
              t80=t80(:idel(t80))//',+1'
            else
              t80=t80(:idel(t80))//',-1'
            endif
            CIFArrayUpdate(l+3)=' '//Cislo(1:4)//t80(1:idel(t80))
          enddo
        endif
        i=iabs(CrlMagInver())
        if(i.gt.0) then
          do k=1,NLattVec(KPhase)
            l=l+1
            call CodeSymmCIF(i,k,t80)
            write(Cislo,FormI15) l
            call Zhusti(Cislo)
            if(ZMag(i,1,KPhase).gt.0.) then
              t80=t80(:idel(t80))//',+1'
            else
              t80=t80(:idel(t80))//',-1'
            endif
            CIFArrayUpdate(l+3)=' '//Cislo(1:4)//t80(1:idel(t80))
          enddo
        endif
        lmx=l+3
        call CIFUpdateRecord(CifKey(j,n),'CRYSTAL DATA',lmx)
      endif
      m=5
      n=5
      l=1
      do i=1,6
        call RoundESD(Cislo,CellPar(i,1,KPhase),CellParSU(i,1,KPhase),
     1                4,0,0)
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
        if(i.ne.3) then
          m=m+1
        else
          m=1
        endif
      enddo
      m=16
      call RoundESD(t80,CellVol(1,KPhase),SVol,6,0,0)
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      if(NTwin.gt.1.and..not.isPowder) then
        l=1
        CIFArrayUpdate(l)='loop_'
        n=23
        do m=6,16
          l=l+1
          CIFArrayUpdate(l)=' '//CifKey(m,n)
        enddo
        do i=1,NTwin
          call RoundESD(t80,sctw(i,KDatBlock),sctws(i,KDatBlock),6,0,0)
          write(Cislo,'(i5)') i
          call Zhusti(Cislo)
          l=l+1
          CIFArrayUpdate(l)=Cislo(:idel(Cislo))//' '//t80(:idel(t80))
          do j=1,3
            l=l+1
            write(t80,101)(RTw(j+(k-1)*3,i),k=1,3)
            call ZdrcniCisla(t80,3)
            CIFArrayUpdate(l)=t80
          enddo
        enddo
        m=6
        n=23
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      endif
      if(NDimI(KPhase).gt.0) then
        write(Cislo,FormI15) NDimI(KPhase)
        call ZdrcniCisla(Cislo,1)
        m=21
        n=5
        l=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
        CIFArrayUpdate(1)='loop_'
        m=150
        CIFArrayUpdate(2)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        l=2
        m=146
        do i=1,3
          m=m+1
          l=l+1
          CIFArrayUpdate(l)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        do i=1,NDimI(KPhase)
          l=l+1
          write(CIFArrayUpdate(l),107) i,(qu(j,i,1,KPhase),j=1,3)
        enddo
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
        if(KCommen(KPhase).gt.0) then
          n=22
          l=0
          m=0
          do i=1,3
            do j=1,3
              l=l+1
              m=m+1
              write(t80,109) RCommen(i+(j-1)*3,1,KPhase)
              call ZdrcniCisla(t80,1)
              write(CIFArrayUpdate(l),FormCIF)
     1          CifKey(m,n),t80(:idel(t80))
            enddo
          enddo
          call CIFUpdateRecord(CifKey(1,n),'CRYSTAL DATA',l)
          m=9
          l=0
          do i=1,NDimI(KPhase)
            l=l+1
            m=m+1
            write(t80,109) trez(i,1,KPhase)
            call ZdrcniCisla(t80,1)
            write(CIFArrayUpdate(l),FormCIF)
     1        CifKey(m,n),t80(:idel(t80))
          enddo
          call CIFUpdateRecord(CifKey(10,n),'CRYSTAL DATA',l)
        endif
        if(NComp(KPhase).gt.1) then
          write(Cislo,FormI15) NComp(KPhase)
          call ZdrcniCisla(Cislo,1)
          m=146
          n=5
          l=1
          write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                     Cislo(:idel(Cislo))
          call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
          CIFArrayUpdate(1)='loop_'
          m=24
          CIFArrayUpdate(2)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
          m=23
          CIFArrayUpdate(3)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
          p80=' '//CifKey(25,5)(:idel(CifKey(25,5)))
          idl=index(p80,'W')
          l=3
          do i=1,NDim(KPhase)
            do j=1,NDim(KPhase)
              l=l+1
              write(t80,100) i,j
              call zhusti(t80)
              CIFArrayUpdate(l)=p80(:idl)//t80(:idel(t80))
            enddo
          enddo
          do i=1,NComp(KPhase)
            l=l+1
            write(CIFArrayUpdate(l),'(i3,3x,'''''''',i1,''-'',a2,
     1            '' subsystem'''''')') i,i,nty(i)
            do j=1,NDim(KPhase)
              l=l+1
              write(Veta,'(6f8.4)')(zv(j+(k-1)*NDim(KPhase),i,KPhase),
     1                              k=1,NDim(KPhase))
              call ZdrcniCisla(Veta,NDim(KPhase))
              write(CIFArrayUpdate(l),FormA) '    '//Veta(:idel(Veta))
            enddo
          enddo
          m=24
          call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
        endif
      endif
      m=4
      n=5
      l=1
      write(Cislo,FormI15) NUnits(KPhase)
      call ZdrcniCisla(Cislo,1)
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      if(JenZaklad) go to 2500
      if(ExistSummary) then
        n=5
        do m=10,13
          if(m.eq.11) cycle
          call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
        enddo
        n=11
        call CIFItemFromSummary(CifKey(8,n),'CRYSTAL DATA',ich)
        call CIFItemFromSummary(CifKey(13,n),'CRYSTAL DATA',ich)
        do m=17,19
          call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
        enddo
        do m=1,5
          call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
        enddo
      endif
      m=9
      n=11
      l=1
      call RoundESD(Cislo,dx,0.,4,0,0)
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      ichp=0
      if(ExistSummary) then
        m=14
        call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
        m=1
        call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ichp)
        do i=4,5
          call CIFItemFromSummary(CifKey(i,n),'CRYSTAL DATA',ich)
        enddo
        if(ichp.eq.0) then
          id1=idel(CifKey(4,n))
          id2=idel(CifKey(1,n))
          m=0
          do i=1,nCIFArray
            if(CIFArray(i).eq.' ') cycle
            if(LocateSubstring(CIFArray(i),
     1                         CifKey(4,n)(:id1),
     2                         .false.,.true.).gt.0) then
              k=0
              call Kus(CIFArray(i),k,t80)
              call Kus(CIFArray(i),k,t80)
              p80=' '
              do k=1,idel(t80)
                if(t80(k:k).ne.'''') p80=p80(:idel(p80))//t80(k:k)
              enddo
              if(mod(m,10).le.0) m=m+1
            else if(LocateSubstring(CIFArray(i),
     1                              CifKey(1,n)(:id2),
     2                              .false.,.true.).gt.0) then
              k=0
              call Kus(CIFArray(i),k,t80)
              call StToReal(CIFArray(i),k,xp,1,.false.,ich)
              if(ich.ne.0) cycle
              if(m/10.eq.0) m=m+10
              amip=xp(1)
            endif
            if(m.eq.11) then
              write(t80,'(f10.4)') amip
              if(EqIgCase(p80,'analytical')) then
                if(abs(ami-amip).gt..01) then
                  write(t80,109) ami
                  call ZdrcniCisla(t80,1)
                  write(p80,109) amip
                  call ZdrcniCisla(p80,1)
                  p80='the absortion correction was made with mi='//
     1                 p80(:idel(p80))//', different from mi='//
     2                 t80(:idel(t80))
                  t80='as follows from final composition. '//
     2                'You should rerun the absorption correction.'
                  call FeChybne(-1.,-1.,p80,t80,SeriousError)
                endif
              else
                ichp=1
              endif
              exit
            endif
          enddo
        endif
        do i=1,2
          call CIFItemFromSummary(CifKey(4-i,n),'CRYSTAL DATA',ich)
        enddo
        call CIFItemFromSummary(CifKey(25,n),'CRYSTAL DATA',ich)
      endif
      if(.not.ExistSummary.or.ichp.ne.0) then
        write(Cislo,109) ami
        call ZdrcniCisla(Cislo,1)
        m=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',1)
      endif
      write(Cislo,109) DatCollTemp(KDatBlock)
      call ZdrcniCisla(Cislo,1)
      m=11
      n=5
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      if(ExistSummary)
     1   call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
      m=3
      n=10
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      if(ExistSummary) then
        call CIFItemFromSummary(CifKey(110,n),'CRYSTAL DATA',ich)
        call CIFItemFromSummary(CifKey(113,n),'CRYSTAL DATA',ich)
        call CIFItemFromSummary(CifKey(117,n),'CRYSTAL DATA',ich)
        call CIFItemFromSummary(CifKey(111,n),'CRYSTAL DATA',ich)
      endif
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        t80='X-ray'
        if(KLam(KDatBlock).le.0) then
          p80='?'
        else
          p80='''X-ray tube'''
        endif
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        t80='neutron'
        p80='?'
      else if(Radiation(KDatBlock).eq.ElectronRadiation) then
        t80='electron'
        p80='?'
      endif
      m=48
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      if(.not.isPowder) then
        m=109
        if(ExistSummary) then
          call CIFItemFromSummary(CifKey(m,n),'CRYSTAL DATA',ich)
        else
          ich=0
        endif
        if(ich.ne.0) then
          write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),p80(:idel(p80))
          call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
        endif
        if(KLam(KDatBlock).gt.0) then
          t80=LamTypeD(KLam(KDatBlock))
          t80=''''//t80(:idel(t80))//' K\a'''
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
          t80='''X-ray'''
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
          t80='''neutron'''
        else if(Radiation(KDatBlock).eq.ElectronRadiation) then
          t80='''electron'''
        endif
        m=49
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      endif
      write(t80,'(f10.6)') LamAve(KDatBlock)
      call ZdrcniCisla(t80,1)
      m=51
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      KRefBlock=0
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          if(KRefBlock.gt.0) then
            KRefBlock=-1
          else if(KRefBlock.eq.0) then
            KRefBlock=i
          endif
        endif
      enddo
      t80='?'
      p80='?'
      if(isPowder.or..not.ExistM95.or.KRefBlock.le.0) then
        go to 2242
      else if(DifCode(KRefBlock).eq.IdCAD4.or.
     1        DifCode(KRefBlock).eq.IdSiemensP4.or.
     2        DifCode(KRefBlock).eq.IdIPDSStoe.or.
     3        DifCode(KRefBlock).eq.IdKumaCCD.or.
     4        DifCode(KRefBlock).eq.IdKumaPD.or.
     5        DifCode(KRefBlock).eq.IdNoniusCCD.or.
     6        DifCode(KRefBlock).eq.IdBrukerCCD.or.
     7        DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
        t80='graphite'
        p80='''four-circle diffractometer'''
      endif
2242  l=1
      m=45
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      if(ExistSummary)
     1  call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
      m=18
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),p80(:idel(p80))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      if(isPowder.or..not.ExistM95.or.KRefBlock.le.0) then
        t80='?'
      else if(DifCode(KRefBlock).eq.IdCAD4) then
        t80='''CAD4'''
      else if(DifCode(KRefBlock).eq.IdSiemensP4) then
        t80='''Siemens'''
      else if(DifCode(KRefBlock).eq.IdIPDSStoe) then
        t80='''IPDS Stoe'''
      else if(DifCode(KRefBlock).eq.IdKumaCCD) then
        t80='''Oxford Diffraction CCD'''
      else if(DifCode(KRefBlock).eq.IdKumaPD) then
        t80='''Oxford Diffraction point detector'''
      else if(DifCode(KRefBlock).eq.IdNoniusCCD) then
        t80='''Nonius CCD'''
      else if(DifCode(KRefBlock).eq.IdBrukerCCD.or.
     1        DifCode(KRefBlock).eq.IdBrukerCCDRaw) then
        t80='''Bruker CCD'''
      else
        t80='?'
      endif
      m=20
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      if(ExistSummary) then
        do i=1,4
          if(i.eq.1) then
            m=20
          else if(i.eq.2) then
            m=11
          else if(i.eq.3) then
            m=12
          else
            m=22
          endif
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
        enddo
      endif
2244  if(isPowder) then
        call PwdM92Nacti
        write(Cislo,FormI15) Npnts
        call Zhusti(Cislo)
        m=27
        n=20
        l=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
        call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
        do m=4,5
          if(m.eq.4) then
            pom=XPwd(1)
          else
            pom=XPwd(Npnts)
          endif
          write(Cislo,109) pom
          call Zhusti(Cislo)
          write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                     Cislo(:idel(Cislo))
          call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
        enddo
        pom=DegStepPwd
        do i=3,Npnts
          if(abs(XPwd(i)-XPwd(i-1)-DegStepPwd).gt..0005) then
            pom=-1.
            go to 2250
          endif
        enddo
2250    if(pom.gt.0.) then
          write(IncPwd,109) pom
        else
          IncPwd='?'
        endif
        call ZdrcniCisla(IncPwd,1)
        m=6
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                   IncPwd(:idel(IncPwd))
        call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
      else
        if(KRefBlock.gt.0) then
          if(DifCode(KRefBlock).eq.IdCAD4.or.
     1       DifCode(KRefBlock).eq.IdSiemensP4.or.
     2       DifCode(KRefBlock).eq.IdKumaPD) then
            t80=''''//ObrLom//'q/2'//ObrLom//'q'''
          else if(DifCode(KRefBlock).eq.IdIPDSStoe) then
            t80='''??integration method??'''
          else
            t80='?'
          endif
        else
          t80='?'
        endif
        m=21
        n=10
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'EXPERIMENTAL DATA',l)
        if(ExistSummary) then
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          m=93
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          do i=1,2
            call CIFItemFromSummary(CifKey(98-i,n),'EXPERIMENTAL DATA',
     1                              ich)
          enddo
          m=95
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          m=5
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          m=6
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          m=85
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          m=86
          call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          if(ich.eq.0) then
            NInfo=4
            TextInfo(1)='Jana2006 version for creation of the CIF '//
     1                  'file is newer than'
            TextInfo(2)='the version used during the final data '//
     1                  'processing.'
            TextInfo(3)='Please run "File->Reflection file->Create '//
     1                  'refinement reflection file"'
            TextInfo(4)='and create the CIF file once more.'
            call FeInfoOut(-1.,-1.,'WARNING','L')
          else
            m=130
            call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          endif
          m=88
          do j=1,2*NDim(KPhase)
            call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
            if(j.eq.6) m=140
            if(mod(j,2).eq.1) then
              m=m-1
            else
              m=m+3
            endif
          enddo
          do i=1,5
            if(i.eq.1) then
              m=125
            else if(i.eq.2) then
              m=123
            else if(i.eq.3) then
              m=124
            else if(i.eq.4) then
              m=122
            else if(i.eq.5) then
              m=119
            endif
            call CIFItemFromSummary(CifKey(m,n),'EXPERIMENTAL DATA',ich)
          enddo
        endif
      endif
      if(isPowder) then
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
          t80='''Gaussian'''
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz) then
          t80='''Lorentzian'''
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
          t80='''Modified Lorentzian'''
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          t80='''Pseudo-Voigt'''
        endif
        m=145
        n=20
        l=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
        j=IBackgPwd-1
        nn=0
        do i=NBackg(KDatBlock),1,-1
          if(KiPwd(j+i).gt.0) then
            nn=i
            go to 2323
          endif
        enddo
2323    if(nn.gt.0) then
          write(Cislo,FormI15) nn
          call Zhusti(Cislo)
          if(KBackg(KDatBlock).eq.1) then
            t80='Legendre polynoms'
          else if(KBackg(KDatBlock).eq.2) then
            t80='Chebyshev polynoms'
          else if(KBackg(KDatBlock).eq.3) then
            t80='cos-ortho functions'
          else if(KBackg(KDatBlock).eq.4) then
            t80='cos-GSAS functions'
          else
            t80='?'
          endif
          if(t80.ne.'?') t80=Cislo(:idel(Cislo))//' '//t80(:idel(t80))
        else
          t80='?'
        endif
        if(KManBackg(KDatBlock).gt.0) then
          if(t80.eq.'?') then
            t80='Manual background'
          else
            t80='Manual background combined with '//t80(:idel(t80))
          endif
        endif
        m=139
        if(t80.ne.'?') then
          j=idel(t80)
          t80=''''//t80(:j)//''''
          j=j+2
        else
          j=1
        endif
        if(j.le.47) then
          write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:j)
          l=1
        else
          write(CIFArrayUpdate(1),FormCIF) CifKey(m,n)
          CIFArrayUpdate(2)=t80
          l=2
        endif
        call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefMarchDollase) then
          t80='''March & Dollase'''
        else if(KPref(KPhase,KDatBlock).eq.IdPwdPrefSasaUda) then
          t80='''Sasa & Uda'''
        else
          t80='none'
        endif
        m=141
        l=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
        do m=142,144
          call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        enddo
      else
        n=17
        l=1
        if(ExistSummary) then
          do m=12,10,-2
            call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
          enddo
          m=15
          call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        endif
      endif
      if(ExistSummary) then
        n=15
        if(isPowder) then
          nn=nCIFPwdFit
        else
          nn=nCIFSingleFit
        endif
        do i=1,nn
          if(isPowder) then
            m=CIFPwdFit(i)
          else
            m=CIFSingleFit(i)
          endif
          call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        enddo
      endif
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      if(allocated(nai)) then
        deallocate(nai,naixb,sumai,mai,atvai,nails,RestType)
        nvaiMax=0
      endif
      if(ExtTensor(1).eq.0) then
        k=0
      else
        if(ExtType(1).eq.1) then
          k=7
        else if(ExtType(1).eq.2) then
          k=1
        else
          k=-1
        endif
      endif
      if(k.gt.0) then
        call RoundESD(t80,ec(k,1)*10000.,ecs(k,1)*10000.,6,0,0)
      else if(k.lt.0) then
        call RoundESD(t80,ec(k,1)*10000.,ecs(k,1)*10000.,6,0,0)
        call RoundESD(Cislo,ec(7,1)*10000.,ecs(7,1)*10000.,6,0,0)
        t80=t80(1:idel(t80))//' '//Cislo(1:idel(Cislo))
      else
        t80='?'
      endif
      m=9
      n=15
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
      call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
      if(isfh(KPhase).gt.0.and..not.JenZaklad) then
        nxy=0
        nxn=0
        nby=0
        nbn=0
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or.isf(i).ne.isfh(KPhase)) cycle
          if(EqIV0(KiA(2,i),3)) then
            nxn=nxn+1
          else
            nxy=nxy+1
          endif
          if(KiA(5,i).eq.0) then
            nbn=nbn+1
          else
            nby=nby+1
          endif
        enddo
        if(nxy.gt.0) then
          if(nxn.gt.0) then
            t80='''mixed'''
          else
            t80='''refall'''
          endif
        else
          t80='''constr'''
        endif
        m=16
        n=15
        l=1
        write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),t80(:idel(t80))
        call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
      endif
      if(isPowder.and..not.JenZaklad) then
        n=20
        l=1
        do m=115,117
          if(m.eq.115) then
            do 2433j=1,Npnts
              pom=XPwd(j)
              do k=1,NskipPwd(KDatBlock)
                if(pom.ge.SkipPwdFr(k,KDatBlock).and.
     1             pom.le.SkipPwdTo(k,KDatBlock)) go to 2433
              enddo
              go to 2437
2433        continue
            Cislo='?'
          else if(m.eq.116) then
            do 2435j=Npnts,1,-1
              pom=XPwd(j)
              do k=1,NskipPwd(KDatBlock)
                if(pom.ge.SkipPwdFr(k,KDatBlock).and.
     1             pom.le.SkipPwdTo(k,KDatBlock)) go to 2435
              enddo
              go to 2437
2435        continue
            Cislo='?'
          else
            Cislo=IncPwd
            go to 2438
          endif
          go to 2438
2437      write(Cislo,109) pom
          call ZdrcniCisla(Cislo,1)
2438      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                     Cislo(:idel(Cislo))
          call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
        enddo
        if(NskipPwd(KDatBlock).gt.0) then
          CIFArrayUpdate(1)='loop_'
          m=128
          CIFArrayUpdate(2)=CifKey(m,n)
          l=2
          do i=1,NskipPwd(KDatBlock)
            l=l+1
            write(t80,'(''''''from '',f10.3,'' to '',f10.3,'''''''')')
     1        SkipPwdFr(i,KDatBlock),SkipPwdTo(i,KDatBlock)
            call ZdrcniCisla(t80,4)
            CIFArrayUpdate(l)=' '//t80(:idel(t80))
          enddo
          call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
        endif
      endif
2500  if(Radiation(KDatBlock).eq.XRayRadiation) then
        ktab=3
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        ktab=1
      else if(Radiation(KDatBlock).eq.ElectronRadiation) then
        ktab=0
        mm=21
      endif
      n=3
      do itab=1,ktab
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          do i=1,NAtFormula(KPhase)
            if((itab.eq.1.and.FFType(KPhase).eq. -9).or.
     1         (itab.eq.2.and.FFType(KPhase).eq.-62).or.
     2         (itab.eq.2.and.FFType(KPhase).gt.  0)) then
              ntab=FFType(KPhase)
              go to 2515
            endif
          enddo
          cycle
        endif
2515    CIFArrayUpdate(1)='loop_'
        m=21
        mm=21
        CIFArrayUpdate(2)=' '//CifKey(m,n)
        l=2
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          do m=17,16,-1
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
          enddo
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
          if(MagneticType(KPhase).ne.0) then
            m=56
            n=3
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
            n=3
          endif
          m=18
          l=l+1
          CIFArrayUpdate(l)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        endif
        m=19
        l=l+1
        CIFArrayUpdate(l)=' '//CifKey(m,n)
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          if(ntab.gt.0) then
            m=20
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(m,n)
          else if(ntab.eq.-9) then
            do m=7,10
              l=l+1
              CIFArrayUpdate(l)=' '//CifKey(m,n)
              l=l+1
              CIFArrayUpdate(l)=' '//CifKey(m+4,n)
            enddo
            m=15
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(m,n)
          endif
        endif
        do i=1,NAtFormula(KPhase)
          ii=ipr(i)
          if(Radiation(KDatBlock).eq.XRayRadiation) then
            if(FFType(KPhase).ne.ntab) cycle
            l=l+1
            if(l.gt.nCIFArrayUpdate)
     1        call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
            if(KAnRef(KPhase).le.0) then
              write(CIFArrayUpdate(l),111)
     1          AtType(ii,KPhase),ffra(ii,KPhase,KDatBlock),
     2                            ffia(ii,KPhase,KDatBlock)
            else
              write(CIFArrayUpdate(l),111)
     1          AtType(ii,KPhase),FFrRef(ii,KPhase,KDatBlock),
     2                            FFiRef(ii,KPhase,KDatBlock)
            endif
            if(ntab.gt.0) then
              l=l+1
              if(l.gt.nCIFArrayUpdate)
     1          call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(l)=' ''International Tables Vol C '//
     1                          'tables 4.2.6.8 and 6.1.1.1'''
              l=l+1
              if(l.gt.nCIFArrayUpdate)
     1          call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(l)=';'
              s80=' stol'
              p80='  f  '
              k=6
              pom=0.
              do j=1,FFType(KPhase)
                write(s8,'(f8.2)') pom
                write(p8,'(f8.3)') FFBasic(j,ii,KPhase)
                if(k+8.gt.80) then
                  l=l+1
                  if(l.gt.nCIFArrayUpdate)
     1              call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
                  CIFArrayUpdate(l)=s80
                  l=l+1
                  if(l.gt.nCIFArrayUpdate)
     1              call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
                  CIFArrayUpdate(l)=p80
                  s80=' stol'
                  p80='  f  '
                  k=6
                endif
                s80=s80(:k)//s8
                p80=p80(:k)//p8
                k=k+8
                pom=pom+.05
              enddo
              l=l+1
              if(l.gt.nCIFArrayUpdate)
     1          call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(l)=';'
            else if(ntab.eq.-9) then
              l=l+1
              CIFArrayUpdate(l)=' ''International Tables Vol C '//
     1                          'tables 4.2.6.8 and 6.1.1.4'''
              p80=' '
              k=1
              do j=1,9
                write(Cislo,'(f15.6)') FFBasic(j,ii,KPhase)
                call ZdrcniCisla(Cislo,1)
                ll=idel(Cislo)
                if(k+ll.gt.80) then
                  l=l+1
                  if(l.gt.nCIFArrayUpdate)
     1              call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
                  CIFArrayUpdate(l)=p80
                  p80=' '
                  k=0
                endif
                if(p80.eq.' ') then
                  p80=Cislo
                  k=ll
                else
                  p80=p80(:idel(p80))//' '//Cislo(:idel(Cislo))
                  k=k+ll
                endif
              enddo
              l=l+1
              if(l.gt.nCIFArrayUpdate)
     1           call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(l)=p80
            else
              l=l+1
              if(l.gt.nCIFArrayUpdate)
     1          call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(l)=' ''International Tables Vol C '//
     1                          'tables 4.2.6.8 and 6.1.1.1'''
            endif
          else if(Radiation(KDatBlock).eq.NeutronRadiation) then
            l=l+1
            if(l.gt.nCIFArrayUpdate)
     1        call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
            if(MagneticType(KPhase).gt.0) then
              Cislo=AtTypeMag(i,KPhase)
              if(Cislo.eq.' ') Cislo='.'
              write(CIFArrayUpdate(l),
     1          '(1x,a2,1x,a,1x,f8.3,'' International_Tables_Vol_C'')')
     2          AtType(i,KPhase),Cislo(:idel(Cislo)),ffn(i,KPhase)
            else
              write(CIFArrayUpdate(l),
     1          '(1x,a2,1x,f8.3,'' International_Tables_Vol_C'')')
     2          AtType(i,KPhase),ffn(i,KPhase)
            endif
          endif
        enddo
      enddo
      call CIFUpdateRecord(CifKey(mm,n),'REFINEMENT DATA',l)
      if(ExistSummary) then
        n=8
        do m=1,3
          call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        enddo
        n=24
        m=CIFRestDist(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        m=CIFRestAngle(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        m=CIFRestTorsion(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        m=CIFRestEqDist(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        m=CIFRestEqAngle(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
        m=CIFRestEqTorsion(1)
        call CIFItemFromSummary(CifKey(m,n),'REFINEMENT DATA',ich)
      endif
      if(NacitatAtomy) then
        BratAtom(1)=.true.
      else
        call SetLogicalArrayTo(BratAtom,NAtCalc,.true.)
        xp=0.
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
!          do j=i+1,NAtCalc
!            if(kswa(j).ne.KPhase) cycle
!            if(IdenticalAtoms(i,j,.001)) then
!              ai(i)=ai(i)+ai(j)
!              BratAtom(j)=.false.
!            endif
!          enddo
          kk=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,i).ne.0.and.KModA(1,i).ne.0) then
              xp(1)=ax(KModA(1,i),i)
              kk=1
            else if(KFA(2,i).ne.0.and.KModA(2,i).ne.0) then
              xp(1)=uy(1,KModA(2,i),i)
              kk=1
            endif
          endif
          call SpecPos(x(1,i),xp,kk,iswa(i),.01,nocc)
          pom=nocc
          if(ai(i).le.0.) then
            BratAtom(i)=.false.
          else if(abs(ai(i)*pom-1.).le..0001) then
            sai(i)=0.
          endif
        enddo
      endif
      kp=0
      do i=1,NAtCalc
        if(NacitatAtomy) then
          j=1
          read(m40,105) Atom(j),isf(j),itf(j)
          read(m40,106)
          do k=3,itf(j)
            do l=1,(TRank(k)-1)/6+1
              read(m40,106)
            enddo
          enddo
        else
          if(kswa(i).ne.KPhase) cycle
          j=i
        endif
        if(BratAtom(j)) kp=max(idel(Atom(j)),kp)
      enddo
      kp=kp+4
      if(NacitatAtomy) then
        rewind m40
        do i=1,5
          read(m40,106)
        enddo
      endif
      nmx=NSymmN(KPhase)*NLattVec(KPhase)
      CIFArrayUpdate(1)='loop_'
      ll=1
      n=1
      nn=nCIFAtX
      if(NComp(KPhase).le.1) nn=nn-1
      do i=1,nn
        ll=ll+1
        CIFArrayUpdate(ll)=' '//
     1    CifKey(CIFAtX(i),n)(:idel(CifKey(CIFAtX(i),n)))
        if(i.eq.3) mm=CIFAtX(i)
      enddo
      itfmx=0
      if(NacitatAtomy) then
        call SetRealArrayTo(sx,3,0.)
        call SetRealArrayTo(sbeta,6,0.)
        sai(1)=0.
      else if(NDimI(KPhase).eq.1.and.NMolec.gt.0) then
        do ia=1,NAtCalc
          if(kswa(ia).ne.KPhase.or.kmol(ia).le.0) cycle
          call CrlMakeMolAtShift(ia)
        enddo
      endif
      do i=1,NAtCalc
        if(NacitatAtomy) then
          l=1
          read(m40,105) Atom(l),isf(l),itf(l),ai(l),(x(j,l),j=1,3)
          read(m40,106)(beta(j,l),j=1,6)
          do k=3,itf(l)
            do l=1,(TRank(k)-1)/6+1
              read(m40,106)
            enddo
          enddo
          isw=1
        else
          call DistModifyAtName(i,Atom(i))
          l=i
          isw=iswa(i)
          if(kswa(i).ne.KPhase) cycle
        endif
        if(.not.BratAtom(l)) cycle
        itfmx=max(itfmx,itf(l))
        if(NAtFormula(KPhase).gt.0) then
          Cislo=AtType(isf(l),KPhase)
        else
          Cislo=atom(l)(:2)
          if(index(Cifry,Cislo(2:2)).gt.0) Cislo(2:2)=' '
        endif
        Veta=' '//atom(l)(:kp-4)//' '//Cislo(:idel(Cislo))
        k=idel(Veta)+2
        do j=1,3
          call RoundESD(Veta(k:),x(j,l),sx(j,l),6,1,0)
          k=k+15
        enddo
        if(itf(l).eq.1) then
          Cislo=' Uiso'
        else
          Cislo=' Uani'
        endif
        Veta=Veta(:idel(Veta))//Cislo(:5)
        if(itf(l).gt.1) then
          call boueq(beta(1,l),sbeta(1,l),0,bizo,sbizo,isw)
          m=1
        else
          bizo=beta(1,l)/episq
          sbizo=sbeta(1,l)/episq
        endif
        k=idel(Veta)+2
        call RoundESD(Veta(k:),bizo,sbizo,4,1,0)
        call SetRealArrayTo(xp,3,0.)
        kk=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,l).ne.0.and.KModA(1,l).ne.0) then
            xp(1)=ax(KModA(1,l),l)
            kk=1
          else if(KFA(2,l).ne.0.and.KModA(2,l).ne.0) then
            xp(1)=uy(1,KModA(2,l),l)
            kk=1
          endif
        endif
        call SpecPos(x(1,l),xp,kk,isw,.01,nocc)
        pom=nocc
        k=idel(Veta)+1
        if(nocc.le.0) nocc=1
        write(Veta(k:),'(i4)') nmx/nocc
        k=k+5
        aip=ai(l)
        saip=sai(l)
        if(NDimI(KPhase).gt.0) then
          if(KModA(1,l).gt.0) then
            if(NDimI(KPhase).eq.1.or.KFA(1,l).eq.0) then
              saip=sqrt((a0(l)*saip)**2+(aip*sa0(l))**2)
              aip=aip*a0(l)
            else
              aip=1.
              do j=1,NDimI(KPhase)
                aip=aip*ay(j,l)
              enddo
              saip=0.
              do j=1,NDimI(KPhase)
                if(ay(j,l).ne.0) saip=saip+(aip*say(j,l)/ay(j,l))**2
              enddo
              saip=sqrt(saip)
            endif
          endif
        endif
        call RoundESD(Veta(k:),anint(aip*pom*1.e5)*1.e-5,saip*pom,4,0,0)
        Veta=Veta(:idel(Veta))//' d . . .'
        if(NComp(KPhase).gt.1) then
          write(Cislo,FormI15) iswa(l)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta)+1)//Cislo(:idel(Cislo))
        endif
        call FeDelTwoSpaces(Veta)
        ll=ll+1
        if(ll.gt.nCIFArrayUpdate)
     1    call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
        CIFArrayUpdate(ll)=' '//Veta(:idel(Veta))
      enddo
      call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      if(itfmx.gt.1) then
        if(NacitatAtomy) then
          rewind m40
          do i=1,5
            read(m40,106)
          enddo
          lite(KPhase)=0
          irot(KPhase)=1
        endif
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do i=1,nCIFAtT
          ll=ll+1
          CIFArrayUpdate(ll)=' '//
     1      CifKey(CIFAtT(i),n)(:idel(CifKey(CIFAtT(i),n)))
          if(i.eq.3) mm=CIFAtT(i)
        enddo
        do i=1,NAtCalc
          if(NacitatAtomy) then
            l=1
            read(m40,105) Atom(l),isf(l),itf(l),ai(l),(x(j,l),j=1,3)
            read(m40,106)(beta(j,l),j=1,6)
            do k=3,itf(l)
              do l=1,(TRank(k)-1)/6+1
                read(m40,106)
              enddo
            enddo
            isw=1
          else
            if(kswa(i).ne.KPhase) cycle
            l=i
            isw=iswa(i)
          endif
          if(.not.BratAtom(l).or.itf(l).lt.2) cycle
          Veta=' '//atom(l)(:idel(atom(l)))//' '//AtType(isf(l),KPhase)
          do j=1,6
            pom=1./urcp(j,isw,KPhase)
            call RoundESD(Cislo,beta(j,l)*pom,sbeta(j,l)*pom,6,1,0)
            Veta=Veta(:idel(Veta))//' '//Cislo(:idel(Cislo))
          enddo
          call FeDelTwoSpaces(Veta)
          ll=ll+1
          if(ll.gt.nCIFArrayUpdate)
     1      call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
          CIFArrayUpdate(ll)=Veta(:idel(Veta))
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
        m=81
        ik=8
        do nn=3,itfmx
          CIFArrayUpdate(1)='loop_'
          ll=1
          n=22
          ik=ik+nn+1
          do i=1,ik
            m=m+1
            ll=ll+1
            CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
            if(i.eq.3) mm=m
          enddo
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase) cycle
            isw=iswa(i)
            if(.not.BratAtom(i).or.itf(i).lt.nn) cycle
            Veta=' '//atom(i)(:idel(atom(i)))//' '//
     1           AtType(isf(i),KPhase)
            do j=1,ik-2
              if(nn.eq.3) then
                call RoundESD(Cislo,c3(j,i),sc3(j,i),10,1,0)
              else if(nn.eq.4) then
                call RoundESD(Cislo,c4(j,i),sc4(j,i),15,1,0)
              else if(nn.eq.5) then
                call RoundESD(Cislo,c5(j,i),sc5(j,i),21,1,0)
              else if(nn.eq.6) then
                call RoundESD(Cislo,c6(j,i),sc6(j,i),28,1,0)
              endif
              Veta=Veta(:idel(Veta))//' '//Cislo(:idel(Cislo))
              call FeDelTwoSpaces(Veta)
              if(idel(Veta)+15.gt.80) then
                ll=ll+1
                if(ll.gt.nCIFArrayUpdate)
     1            call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
                CIFArrayUpdate(ll)=Veta(:idel(Veta))
                Veta=' '
              endif
            enddo
            ll=ll+1
            if(ll.gt.nCIFArrayUpdate)
     1        call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
            CIFArrayUpdate(ll)=Veta(:idel(Veta))
          enddo
          call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
        enddo
      endif
      if(MagneticType(KPhase).gt.0.and.MagParMax.gt.0) then
        n=1
        CIFArrayUpdate(1)='loop_'
        CIFArrayUpdate(2)=' '//CifKey(116,n)(:idel(CifKey(116,n)))
!        CIFArrayUpdate(3)=' '//CifKey(117,n)(:idel(CifKey(117,n)))
        mm=116
        m=122
        ll=2
        do i=1,3
          ll=ll+1
          m=m+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        do i=1,NAtCalc
          if(.not.BratAtom(i).or.kswa(i).ne.KPhase.or.MagPar(i).le.0)
     1      cycle
          PolMag=ktat(NamePolar,NPolar,Atom(i)).gt.0
          if(PolMag) then
            call ShpCoor2Fract( sm0(1,i),RPom)
            call ShpCoor2Fract(ssm0(1,i),RPom)
          endif
          Veta=' '//atom(i)
          isw=iswa(i)
          do j=1,3
            call RoundESD(Cislo, sm0(j,i)*CellPar(j,isw,KPhase),
     1                          ssm0(j,i)*CellPar(j,isw,KPhase),
     2                          6,1,0)
            Veta=Veta(:idel(Veta))//' '//Cislo(:idel(Cislo))
            call FeDelTwoSpaces(Veta)
            if(idel(Veta)+15.gt.80) then
              ll=ll+1
              if(ll.gt.nCIFArrayUpdate)
     1          call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
              CIFArrayUpdate(ll)=Veta(:idel(Veta))
              Veta=' '
            endif
          enddo
          ll=ll+1
          if(ll.gt.nCIFArrayUpdate)
     1      call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
          CIFArrayUpdate(ll)=Veta(:idel(Veta))
          call Fract2ShpCoor(sm0(1,i))
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(NDim(KPhase).le.3) go to 5000
      do ia=1,NAtCalc
        if(TypeModFun(ia).eq.1) norp=norp+1
      enddo
      if(norp.gt.0) then
        if(allocated(xyzor)) deallocate(xyzor,betaor)
        allocate(xyzor(3,NAtCalc),betaor(6,NAtCalc))
        do i=1,NAtCalc
          xyzor(1:3,i)=x(1:3,i)
          betaor(1:6,i)=beta(1:6,i)
        enddo
        call trortho(0)
      endif
      nms=0
      nmx=0
      nmb=0
      nmc=0
      nmm=0
      NCrenel=0
      NSawTooth=0
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
        kmodsi=KModA(1,i)
        if(KFA(1,i).ne.0) then
          kmodsi=kmodsi-NDimI(KPhase)
          NCrenel=NCrenel+1
        endif
        kmodxi=KModA(2,i)
        if(KFA(2,i).ne.0) then
          kmodxi=kmodxi-1
          NSawTooth=NSawTooth+1
        endif
        nms=max(kmodsi,nms)
        nmx=max(kmodxi,nmx)
        nmb=max(KModA(3,i),nmb)
        do j=4,7
          nmc=max(nmc,KModA(j,i))
        enddo
        if(MagneticType(KPhase).ne.0) nmm=max(nmm,MagPar(i)-1)
      enddo
      mxwp=max(nms,nmx,nmb,nmc,nmm)
      if(mxwp.le.0.and.NCrenel.le.0.and.NSawTooth.le.0) go to 5000
      ll=1
      n=1
      CIFArrayUpdate(ll)='loop_'
      ll=ll+1
      j=64
      CIFArrayUpdate(ll)=' '//CifKey(j,n)(:idel(CifKey(j,n)))
      mm=j
      if(CIFNewFourierWaves) then
        nn=22
        do j=53,52+NDimI(KPhase)
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(j,nn)(:idel(CifKey(j,nn)))
        enddo
        if(norp.gt.0) then
          ll=ll+1
          write(CIFArrayUpdate(ll),102) 0,0
        endif
        do i=1,mxwp
          ll=ll+1
          write(CIFArrayUpdate(ll),102)
     1      i,(kw(j,i,KPhase),j=1,NDimI(KPhase))
        enddo
      else
        do j=65,67
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(j,n)(:idel(CifKey(j,n)))
          if(j.eq.64) mm=j
        enddo
        do i=1,mxwp
          do k=1,3
            qupom(k)=0.
            do j=1,NDimI(KPhase)
              qupom(k)=qupom(k)+qu(k,j,1,KPhase)*float(kw(j,i,KPhase))
            enddo
          enddo
          ll=ll+1
          write(CIFArrayUpdate(ll),107) i,qupom
        enddo
      endif
      call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      norp=0
      do ia=1,NAtCalc
        if(TypeModFun(ia).ne.1) cycle
        norp=norp+1
        if(norp.eq.1) then
          CIFArrayUpdate(1)='loop_'
          ll=1
          n=22
          mm=49
          do i=49,52
            ll=ll+1
            CIFArrayUpdate(ll)=' '//CifKey(i,n)(:idel(CifKey(i,n)))
          enddo
        endif
        ll=ll+1
        write(CIFArrayUpdate(ll),107) norp,OrthoX40(ia),OrthoDelta(ia),
     1                                OrthoEps(ia)
      enddo
      call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      if(nms.gt.0) then
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=22
        mm=200
        do m=200,201
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.KFA(1,i).ne.0.or.
     1       KModA(1,i).le.0) cycle
          t80=' '//atom(i)
          call RoundESD(Cislo,a0(i),sa0(i),4,1,0)
          t80=' '//atom(i)//' '//Cislo(:idel(Cislo))
          ll=ll+1
          CIFArrayUpdate(ll)=t80
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do i=1,nCIFAtSM
          m=CIFAtSM(i)
          if(i.eq.1) mm=m
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
          t80=' '//atom(i)
          kmodsi=KModA(1,i)
          if(KFA(1,i).ne.0) then
            kmodsi=kmodsi-NDimI(KPhase)
          endif
          do k=1,kmodsi
            write(t80(kp:),'(i3)') k
            call RoundESD(cislo,ay(k,i),say(k,i),4,1,0)
            t80(kp+5:)=Cislo
            call RoundESD(cislo,ax(k,i),sax(k,i),4,1,0)
            t80(kp+25:)=Cislo
            ll=ll+1
            CIFArrayUpdate(ll)=t80
          enddo
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(NCrenel.gt.0) then
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do m=103,105
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
          if(m.eq.103) mm=m
        enddo
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or.KFA(1,i).le.0.or..not.BratAtom(i))
     1       cycle
          k=KModA(1,i)
          t80=' '//atom(i)
          call RoundESD(Cislo,ax(k,i),sax(k,i),4,1,0)
          t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))
          call RoundESD(Cislo,a0(i),sa0(i),4,1,0)
          t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))
          ll=ll+1
          CIFArrayUpdate(ll)=t80
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(nmx.gt.0) then
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do i=1,nCIFAtXM
          m=CIFAtXM(i)
          if(i.eq.1) mm=m
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        nu=0
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.
     1       TypeModFun(i).gt.1) cycle
          t80=' '//atom(i)
          kmodxi=KModA(2,i)
          if(KFA(2,i).ne.0) then
            kmodxi=kmodxi-1
          endif
          if(TypeModFun(i).eq.1.and.KModA(2,i).gt.0) then
            nu=nu+1
            do j=1,3
              write(t80(kp:),103) smbx(j),0
              pom=x(j,i)-xyzor(j,i)
              call RoundESD(Cislo,pom,0.,4,1,0)
              t80(kp+8:)=Cislo
              Cislo='0'
              t80(kp+25:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          endif
          do k=1,kmodxi
            if(k.eq.1.and.TypeModFun(i).eq.0) nu=nu+1
            do j=1,3
              write(t80(kp:),103) smbx(j),k
              call RoundESD(cislo,uy(j,k,i),suy(j,k,i),4,1,0)
              t80(kp+8:)=Cislo
              call RoundESD(cislo,ux(j,k,i),sux(j,k,i),4,1,0)
              t80(kp+25:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          enddo
        enddo
        if(nu.gt.0)
     1    call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
        mm=24
        n=22
        ll=1
        CIFArrayUpdate(1)='loop_'
        do m=24,27
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        Psat=.false.
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.
     1       TypeModFun(i).le.1) cycle
          t80=' '//atom(i)
          do kk=1,2*KModA(2,i)
            Psat=.true.
            k=(kk-1)/2+1
            do j=1,3
              write(t80(kp:),103) smbx(j),kk
              if(mod(kk,2).eq.1) then
                call RoundESD(cislo,ux(j,k,i),sux(j,k,i),4,1,0)
              else
                call RoundESD(cislo,uy(j,k,i),suy(j,k,i),4,1,0)
              endif
              t80(kp+8:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          enddo
        enddo
        if(Psat)
     1    call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(NSawTooth.gt.0) then
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do m=97,102
          ll=ll+1
          CIFArrayUpdate(ll)=CifKey(m,n)
          if(m.eq.97) mm=m
        enddo
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or.KFA(2,i).le.0) cycle
          k=KModA(2,i)
          t80=' '//atom(i)
          do j=1,3
            call RoundESD(Cislo,ux(j,k,i),sux(j,k,i),4,1,0)
            t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))
          enddo
          do j=1,2
            call RoundESD(Cislo,uy(j,k,i),suy(j,k,i),4,1,0)
            t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))
          enddo
          ll=ll+1
          CIFArrayUpdate(ll)=t80
        enddo
        call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(nmb.gt.0) then
        CIFArrayUpdate(1)='loop_'
        ll=1
        n=1
        do i=1,nCIFAtTM
          m=CIFAtTM(i)
          if(i.eq.1) mm=m
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        nu=0
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.
     1       TypeModFun(i).gt.1) cycle
          isw=iswa(i)
          t80=' '//atom(i)
          if(TypeModFun(i).eq.1.and.KModA(3,i).gt.0) then
            nu=nu+1
            do j=1,6
              call indext(j,l,m)
              write(t80(kp:),104) 'U',l,m,0
              pom=(beta(j,i)-betaor(j,i))/urcp(j,isw,KPhase)
              call RoundESD(Cislo,pom,0.,4,1,0)
              t80(kp+8:)=Cislo
              Cislo='0'
              t80(kp+25:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          endif
          do k=1,KModA(3,i)
            if(k.eq.1) nu=nu+1
            do j=1,6
              pom=1./urcp(j,isw,KPhase)
              call indext(j,l,m)
              write(t80(kp:),104) 'U',l,m,k
              call RoundESD(cislo,by(j,k,i)*pom,sby(j,k,i)*pom,6,1,0)
              t80(kp+10:)=Cislo
              call RoundESD(cislo,bx(j,k,i)*pom,sbx(j,k,i)*pom,6,1,0)
              t80(kp+25:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          enddo
        enddo
        if(nu.gt.0)
     1    call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
        mm=31
        n=22
        ll=1
        CIFArrayUpdate(1)='loop_'
        do m=31,34
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
        enddo
        Psat=.false.
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.
     1       TypeModFun(i).ne.2) cycle
          isw=iswa(i)
          t80=' '//atom(i)
          do kk=1,2*KModA(3,i)
            Psat=.true.
            k=(kk-1)/2+1
            do j=1,6
              pom=1./urcp(j,isw,KPhase)
              call indext(j,l,m)
              write(t80(kp:),104) 'U',l,m,kk
              if(mod(kk,2).eq.1) then
                call RoundESD(cislo,bx(j,k,i)*pom,sbx(j,k,i)*pom,6,1,0)
              else
                call RoundESD(cislo,by(j,k,i)*pom,sby(j,k,i)*pom,6,1,0)
              endif
              t80(kp+10:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          enddo
        enddo
        if(Psat)
     1    call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
      if(nmc.gt.0) then
        mp=0
        n=22
        do nn=3,6
          CIFArrayUpdate(1)='loop_'
          ll=1
          Znak=char(ichar('C')+nn-3)
          do i=1,nCIFAtCM
            m=CIFAtCM(i)+mp
            if(i.eq.1) mm=m
            ll=ll+1
            CIFArrayUpdate(ll)=' '//CifKey(m,n)(:idel(CifKey(m,n)))
          enddo
          nu=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
            isw=iswa(i)
            t80=' '//atom(i)
            do k=1,KModA(nn+1,i)
              if(k.eq.1) nu=nu+1
              do j=1,TRank(nn)
                call indexc(j,nn,iap)
                write(t80(kp:),108) Znak,(iap(l),l=1,nn)
                write(t80(kp+7:),110) k
                if(nn.eq.3) then
                  call RoundESD(cislo,c3y(j,k,i),sc3y(j,k,i),6,1,0)
                else if(nn.eq.4) then
                  call RoundESD(cislo,c4y(j,k,i),sc4y(j,k,i),6,1,0)
                else if(nn.eq.5) then
                  call RoundESD(cislo,c5y(j,k,i),sc5y(j,k,i),6,1,0)
                else if(nn.eq.6) then
                  call RoundESD(cislo,c6y(j,k,i),sc6y(j,k,i),6,1,0)
                endif
                t80(kp+14:)=Cislo
                if(nn.eq.3) then
                  call RoundESD(cislo,c3x(j,k,i),sc3x(j,k,i),6,1,0)
                else if(nn.eq.4) then
                  call RoundESD(cislo,c4x(j,k,i),sc4x(j,k,i),6,1,0)
                else if(nn.eq.5) then
                  call RoundESD(cislo,c5x(j,k,i),sc5x(j,k,i),6,1,0)
                else if(nn.eq.6) then
                  call RoundESD(cislo,c6x(j,k,i),sc6x(j,k,i),6,1,0)
                endif
                t80(kp+29:)=Cislo
                ll=ll+1
                CIFArrayUpdate(ll)=t80
              enddo
            enddo
          enddo
          if(nu.gt.0)
     1      call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
          mp=mp+9
        enddo
      endif
      if(MagneticType(KPhase).ne.0.and.nmm.gt.0) then
        n=1
        CIFArrayUpdate(1)='loop_'
        CIFArrayUpdate(2)=' '//CifKey(108,1)(:idel(CifKey(108,n)))
        CIFArrayUpdate(3)=' '//CifKey(109,1)(:idel(CifKey(109,n)))
        CIFArrayUpdate(4)=' '//CifKey(110,1)(:idel(CifKey(110,n)))
        m=111
        mm=111
        ll=4
        do i=1,2
          ll=ll+1
          CIFArrayUpdate(ll)=' '//CifKey(m,1)(:idel(CifKey(m,n)))
          m=m+1
        enddo
        nu=0
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.
     1       MagPar(i).lt.1) cycle
          isw=iswa(i)
          t80=' '//atom(i)
          do k=1,MagPar(i)-1
            if(k.eq.1) nu=nu+1
            do j=1,3
              write(t80(kp:),103) smbx(j),k
              call RoundESD(Cislo, smy(j,k,i)*CellPar(j,isw,KPhase),
     1                            ssmy(j,k,i)*CellPar(j,isw,KPhase),
     2                            6,1,0)
              t80(kp+8:)=Cislo
              call RoundESD(Cislo, smx(j,k,i)*CellPar(j,isw,KPhase),
     1                            ssmx(j,k,i)*CellPar(j,isw,KPhase),
     2                            6,1,0)
              t80(kp+25:)=Cislo
              ll=ll+1
              CIFArrayUpdate(ll)=t80
            enddo
          enddo
        enddo
        if(nu.gt.0)
     1    call CIFUpdateRecord(CifKey(mm,n),'ATOMIC COORDINATES',ll)
      endif
5000  if(JenZaklad) go to 7000
      if(ExistSummary) then
        m=10
        n=12
        call CIFItemFromSummary(CifKey(m,n),'MOLECULAR GEOMETRY',ich)
        m=3
        call CIFItemFromSummary(CifKey(m,n),'MOLECULAR GEOMETRY',ich)
        m=23
        call CIFItemFromSummary(CifKey(m,n),'MOLECULAR GEOMETRY',ich)
        m=34
        call CIFItemFromSummary(CifKey(m,n),'MOLECULAR GEOMETRY',ich)
      endif
      mm=0
      ll=0
      if(isPowder) then
        p80='POWDER PROFILE'
        if(.not.ExistFile(fln(:ifln)//'.prf')) then
          TextInfo(1)='A listing of the powder profile could not be '
          TextInfo(2)='created as *.prf file does not exist.'
          Ninfo=2
          call FeInfoOut(-1.,-1.,'WARNING','L')
          go to 7000
        endif
        CIFArrayUpdate(1)='loop_'
        l=1
        ll=20
        if(isTOF) then
          do i=1,nCIFPwdTOF
            if(i.eq.1) mm=CIFPwdTOF(i)
            l=l+1
            CIFArrayUpdate(l)=' '//
     1        CifKey(CIFPwdTOF(i),ll)(:idel(CifKey(CIFPwdTOF(i),ll)))
          enddo
        else
          do i=1,nCIFPwdProf
            if(i.eq.1) mm=CIFPwdProf(i)
            l=l+1
            CIFArrayUpdate(l)=' '//
     1        CifKey(CIFPwdProf(i),ll)(:idel(CifKey(CIFPwdProf(i),ll)))
          enddo
        endif
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.prf','formatted','old')
        if(ErrFlag.ne.0) go to 7000
        if(NDatBlock.gt.1) then
5020      read(ln,FormA,end=5050,err=5180) s80
          k=0
          call Kus(s80,k,Cislo)
          if(EqIgCase(Cislo,DatBlockName(KDatBlock))) go to 5100
          go to 5020
        endif
5050    n=0
5100    read(ln,'(i4)') i
        if(i.le.900) go to 5100
5150    read(ln,PrfPointsFormat,err=5180,end=5180)
     1                               pom,YoPwdi,YcPwdi,YsPwdi,XPwdi,i
        if(i.le.0) go to 5150
        if(pom.gt.900.) go to 5180
        if(isTOF) XPwdi=PwdTOF2D(pom)
        n=n+1
        l=l+1
        if(l.gt.nCIFArrayUpdate)
     1    call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
        if(isTOF) then
          write(CIFArrayUpdate(l),'(i10,f15.6,3f15.2)') n,XPwdi,YoPwdi,
     1                                                  YcPwdi,YsPwdi
        else
          write(CIFArrayUpdate(l),'(i10,f15.4,3f15.2)') n,XPwdi,YoPwdi,
     1                                                  YcPwdi,YsPwdi
        endif
        go to 5150
5180    Veta=fln(:ifln)//'.m83'
        if(ExistFile(Veta)) then
          call CloseIfOpened(ln)
          if(mm.gt.0) call CIFUpdateRecord(CifKey(mm,ll),p80,l)
          p80='STRUCTURE-FACTOR LIST'
          CIFArrayUpdate(1)='loop_'
          l=1
          ll=16
          do i=13,15
            if(i.eq.13) mm=i
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(i,ll)(:idel(CifKey(i,ll)))
          enddo
          do i=32,31+NDimI(KPhase)
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(i,ll)(:idel(CifKey(i,ll)))
          enddo
          do i=1,4
            l=l+1
            CIFArrayUpdate(l)=' '//
     1        CifKey(CIFSingleRfl(i),ll)
     2          (:idel(CifKey(CIFSingleRfl(i),ll)))
          enddo
          l=l+1
          i=31
          CIFArrayUpdate(l)=' '//CifKey(i,ll)(:idel(CifKey(i,ll)))
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','old')
          if(ErrFlag.ne.0) then
            ErrFlag=0
            go to 6000
          endif
5200      read(ln,FormA,end=6000) Veta
          read(Veta,format83p) ih(1:maxNDim),pom1,pom2,pom3,Cislo(1:1),
     1                        pom4,KPh
          if(KPh.ne.KPhase) go to 5200
          write(Veta,format83p) ih(1:maxNDim),pom1,pom2,pom3,Cislo(1:1),
     1                         pom4
          l=l+1
          if(l.gt.nCIFArrayUpdate)
     1      call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
          CIFArrayUpdate(l)=Veta
          go to 5200
        endif
      else
        p80='STRUCTURE-FACTOR LIST'
        if(.not.ExistFile(fln(:ifln)//'.m83')) then
          TextInfo(1)='The reflection cif could not be created'
          TextInfo(2)='as the F(obs)/F(calc) resp. I(obs)/I(calc)'
          TextInfo(3)='file does not exist.'
          Ninfo=3
            call FeInfoOut(-1.,-1.,'WARNING','L')
          go to 7000
        endif
        if(CrlCentroSymm().le.0) then
          n=CrlNumberOfFriedelPairs()
          write(Cislo,FormI15) n
          call Zhusti(Cislo)
          t80=';  '//Cislo(:idel(Cislo))//' of Friedel pairs used'//
     1        ' in the refinement'
          m=4
          n=15
          l=1
          CIFArrayUpdate(l)=CifKey(m,n)
          l=l+1
          CIFArrayUpdate(l)=t80
          l=l+1
          CIFArrayUpdate(l)=';'
          call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
          do i=1,NTwin
            if(MatRealEqMinusUnitMat(RTw(1,i),3,.0001)) then
              call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
              call RoundESD(t80,sctw(i,KDatBlock),sctws(i,KDatBlock),6,
     1                      1,0)
              l=1
              m=5
              write(CIFArrayUpdate(l),FormCIF)
     1          CifKey(m,n),t80(:idel(t80))
              call CIFUpdateRecord(CifKey(m,n),'REFINEMENT DATA',l)
              exit
            endif
          enddo
        endif
        CIFArrayUpdate(1)='loop_'
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
        if(ErrFlag.ne.0) go to 7000
        read(ln,FormA) Veta
        i=LocateSubstring(Veta,'e',.false.,.true.)
        if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
          Format83a=Format83e
        else
          Format83a=Format83
        endif
        rewind ln
        MoreDomains=.false.
        FullOverlap=.true.
        pom=0.
        IdlZone=0
5300    read(ln,Format83a,end=5310)(ih(i),i=1,NDim(KPhase)),FObsPom,
     1                                FCalcPom,FSigPom,Znak,itw,
     2                                (xxx,i=1,8),t80
        IDLZone=max(idel(t80),IdlZone)
        pom=max(pom,FObsPom,FCalcPom,FSigPom)
        if(iabs(itw).gt.1.and..not.MoreDomains) MoreDomains=.true.
        if(n.le.3.and.(Znak.eq.'<'.or.EqIgCase(Znak,'o'))) n=n+1
        go to 5300
5310    if(MoreDomains) then
          rewind ln
          if(allocated(ipr)) deallocate(ipr)
          allocate(ipr(NTwin))
          m=0
          ipr=0
5320      read(ln,Format83a,end=5330)(ih(i),i=1,NDim(KPhase)),FObsPom,
     1                                FCalcPom,FSigPom,Znak,itw
          iatw=iabs(itw)
          iatw=mod(iatw,100)
          ipr(iatw)=ipr(iatw)+1
          if(itw.gt.0) then
            do i=1,NTwin
              if(ipr(i).le.0) then
                FullOverlap=.false.
                go to 5330
              endif
            enddo
            ipr=0
          endif
          go to 5320
        endif
5330    rewind ln
        if(FullOverlap) MoreDomains=.false.
        ie=Exponent10(pom)
        if(ie.ge.6) then
          je=0
        else
          je=2
          ie=ie+2
        endif
        ie=ie+3
!        if(MoreDomains.or.NTwin.gt.1) then
        if(MoreDomains) then
          ll=23
          Format83o='(i10,i5,'//Format83(2:)
          write(Format83o(15:18),'(i2,''.'',i1)') ie,je
          l=2
          CIFArrayUpdate(l)=' '//CifKey(17,ll)
          l=l+1
          CIFArrayUpdate(l)=' '//CifKey(34,ll)
          do i=23,22+NDim(KPhase)
            if(i.eq.23) mm=i
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(i,ll)
          enddo
          l=l+1
          CIFArrayUpdate(l)=' '//CifKey(18,ll)
          do i=20,22
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(i,ll)
          enddo
        else
          Format83o=Format83
          if(Radiation(KDatBlock).eq.ElectronRadiation.and.CalcDyn) then
            l=1
            ll=17
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(16,17)
            mm=16
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(19,17)
            if(NDatBlock.gt.1) then
              write(t80,'(i10,''%'',i10)') NMaxEDZone,NDatBlock
              call Zhusti(t80)
!              idl=idel(t80)+4
            endif
            do KDatB=1,NDatBlock
              do i=1,NEDZone(KDatB)
                if(.not.UseEDZone(i,KDatB)) cycle
                l=l+1
                if(NDatBlock.gt.1) then
                  write(t80,'(i10,''%'',i10)') i,KDatB
                  call Zhusti(t80)
                  write(CIFArrayUpdate(l),'(a,f10.3)') t80(:idlZone+1),
     1              ScEDZone(i,KDatB)
                else
                  write(CIFArrayUpdate(l),'(i5,f10.3)') i,
     1              ScEDZone(i,KDatBlock)
                endif
              enddo
            enddo
            call CIFUpdateRecord(CifKey(mm,ll),p80,l)
            i=index(Format83o,'x')
            Veta=Format83o
            write(Cislo,'('',1x,a'',i2)') IdlZone+1
            call Zhusti(Cislo)
            Format83o=Format83o(:i-3)//Cislo(:idel(Cislo))//
     1                Format83o(i-2:)
          endif
          l=1
          ll=16
          do i=13,15
            l=l+1
            if(i.eq.13) mm=i
            CIFArrayUpdate(l)=' '//CifKey(i,ll)(:idel(CifKey(i,ll)))
          enddo
          do i=32,31+NDimI(KPhase)
            l=l+1
            CIFArrayUpdate(l)=' '//CifKey(i,ll)(:idel(CifKey(i,ll)))
          enddo
          write(Format83O( 8:11),'(i2,''.'',i1)') ie,je
          if(Radiation(KDatBlock).eq.ElectronRadiation.and.CalcDyn) then
            do i=1,nCIFElectronRfl
              l=l+1
              CIFArrayUpdate(l)=' '//
     1          CifKey(CIFElectronRfl(i),ll)
     2            (:idel(CifKey(CIFElectronRfl(i),ll)))
            enddo
          else
            do i=1,4
              l=l+1
              CIFArrayUpdate(l)=' '//
     1          CifKey(CIFSingleRfl(i),ll)
     2            (:idel(CifKey(CIFSingleRfl(i),ll)))
            enddo
          endif
        endif
        rewind ln
        FCalcMax=0.
        nref=1
5350    read(ln,Format83a,end=6000)(ih(i),i=1,NDim(KPhase)),FObsPom,
     1                              FCalcPom,FSigPom,Znak,itw,
     2                              (pom,i=1,8),t80
        FCalcMax=max(FCalcMax,FCalcPom)
!        if(MoreDomains.or.NTwin.gt.1) then
        if(MoreDomains) then
          itwp=iabs(itw)
        else
          if(itw.lt.0) go to 5350
        endif
        l=l+1
        if(l.gt.nCIFArrayUpdate)
     1    call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
        if(MoreDomains) then
          write(CIFArrayUpdate(l),Format83O) nref,itwp,
     1      ih(1:NDim(KPhase)),FObsPom,FCalcPom,FSigPom,Znak
        else
          if(Radiation(KDatBlock).eq.ElectronRadiation.and.CalcDyn) then
            write(CIFArrayUpdate(l),Format83O)(ih(i),i=1,NDim(KPhase)),
     1                                         FObsPom,FCalcPom,FSigPom,
     2                                         t80(:idel(t80)),Znak
          else
            write(CIFArrayUpdate(l),Format83O)(ih(i),i=1,NDim(KPhase)),
     1                                         FObsPom,FCalcPom,FSigPom,
     2                                         Znak
          endif
        endif
        if(itw.gt.0) nref=nref+1
        go to 5350
      endif
6000  call CloseIfOpened(ln)
      if(mm.gt.0) call CIFUpdateRecord(CifKey(mm,ll),p80,l)
      if(mm.gt.0.and..not.IsPowder) then
        ln=NextLogicNumber()
        j=idel(FileIn)
        do i=j,1,-1
          if(FileIn(i:i).eq.'.') go to 6100
        enddo
        i=j+1
6100    Veta=FileIn(1:i-1)//'.fcf'
        call OpenFile(ln,Veta,'formatted','unknown')
        write(ln,FormA) '#'
        Veta='# h,k,l, Fc-squared, Fo-squared, sigma(Fo-squared) and '//
     1       'status flag'
        write(ln,FormA) Veta(:idel(Veta))
        write(ln,FormA) '#'
        write(ln,FormA) 'data_I'
        Veta='_shelx_title'
        if(StructureName.ne.' ') then
          write(ln,FormCIF)
     1      Veta,''''//StructureName(:idel(StructureName))//''''
        else
          write(ln,FormCIF) Veta,'''   '''
        endif
        Veta='_shelx_refln_list_code'
        write(ln,FormCIF) Veta,'4'
        write(Cislo,'(f10.2)') sqrt(FCalcMax)
        call Zhusti(Cislo)
        Veta='_shelx_F_calc_maximum'
        write(ln,FormCIF) Veta,Cislo(:idel(Cislo))
        i=14
        j=11
        do k=1,nCIFUsed
          if(LocateSubstring(CIFArray(k),CifKey(i,j)(:idel(CifKey(i,j)))
     1       ,.false.,.true.).gt.0) then
            write(ln,FormA) CIFArray(k)(:idel(CIFArray(k)))
            exit
          endif
        enddo
        i=96
        j=10
        do k=1,nCIFUsed
          if(LocateSubstring(CIFArray(k),CifKey(i,j)(:idel(CifKey(i,j)))
     1       ,.false.,.true.).gt.0) then
            kk=0
            call kus(CIFArray(k),kk,t80)
            call kus(CIFArray(k),kk,Cislo)
            if(Cislo.ne.'?') then
              call posun(Cislo,1)
              read(Cislo,'(f15.0)',err=6110) pom
              write(Cislo,'(f15.4)') .5*LamAve(KDatBlock)/
     1                                  sin(pom*ToRad)
              go to 6120
6110          Cislo='?'
            endif
6120        i=1
            j=17
            call Zhusti(Cislo)
            write(ln,FormCIF) CifKey(i,j),Cislo(:idel(Cislo))
            exit
          endif
        enddo
        write(ln,FormA)
        if(NDim(KPhase).eq.3) then
          j=6
          n=18
        else
          j=6
          n=21
        endif
        write(ln,'(''loop_'')')
        Veta=' '//CifKey(j,n)(:idel(CifKey(j,n)))
        write(ln,FormA) Veta(:idel(Veta))
        do k=1,NLattVec(KPhase)
          do i=1,NSymmN(KPhase)
            call CodeSymmCIF(i,k,t80)
            Veta=' '''
            idl=2
            do j=1,idel(t80)
              if(t80(j:j).eq.',') then
                Veta=Veta(:idl)//', '
                idl=idl+2
              else
                Veta=Veta(:idl)//t80(j:j)
                idl=idl+1
              endif
            enddo
            Veta=Veta(:idel(Veta))//''''
            write(ln,FormA) Veta(:idel(Veta))
          enddo
        enddo
        write(ln,FormA)
        m=5
        n=5
        do i=1,6
          call RoundESD(Cislo,CellPar(i,1,KPhase),CellParSU(i,1,KPhase),
     1                  4,0,0)
          write(ln,FormCIF) CifKey(m,n),Cislo(:idel(Cislo))
          if(i.ne.3) then
            m=m+1
          else
            m=1
          endif
        enddo
        write(ln,FormA)
        Cislo='1.000'
        Veta='_shelx_F_squared_multiplier'
        write(ln,FormCIF) Veta,Cislo(:idel(Cislo))
        write(ln,FormA)
        do i=1,l
          write(ln,FormA)
     1      CIFArrayUpdate(i)(:idel(CIFArrayUpdate(i)))
        enddo
        call CloseIfOpened(ln)
      endif
7000  rewind lni
      n=1
7100  write(lni,FormA) CIFArray(n)(:idel(CIFArray(n)))
      n=n+1
      if(n.le.nCIFUsed) go to 7100
9100  call CloseIfOpened(lni)
      call CloseIfOpened(LnSum)
9999  if(.not.CIFAllocated) deallocate(CifKey,CifKeyFlag)
      if(allocated(CIFArray)) deallocate(CIFArray)
      if(allocated(CIFArrayUpdate)) deallocate(CIFArrayUpdate)
      if(allocated(BratAtom)) deallocate(BratAtom)
      if(allocated(ExtTensor))
     1  deallocate(ExtTensor,ExtType,ExtDistr,ExtRadius)
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      if(allocated(ipr)) deallocate(ipr)
      if(allocated(xyzor)) deallocate(xyzor,betaor)
      KRefBlock=KRefBlockIn
      call DeleteFile(PreviousM40)
      if(norp.gt.0) call trortho(1)
      return
100   format(2('_',i2))
101   format(10f12.6)
102   format(8i5)
103   format(a1,2i3)
104   format(a1,2i1,2i3)
105   format(a8,2i3,4x,4f9.6)
106   format(6f9.6)
107   format(i2,3f10.6)
108   format(a1,6i1)
109   format(f8.3)
110   format(2i3)
111   format(1x,a2,1x,2f8.4)
      end
