      real function LPCorrection(Th)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      real LCorrection
      if(isTOF) then
        LPCorrection=1./(SinTOFThUse*Th**4)
      else if(isED) then
        LPCorrection=1.
      else
        sn=sin(Th)
        cs2q=(1.-2.*sn**2)**2
        if(isPowder) then
          if(KFocusBB(KDatBlock).eq.0.or.
     1       LpTypeCorrUse.eq.PolarizedGuinier) then
            LCorrection=sn**2*sqrt(1.-sn**2)
          else
            LCorrection=sn*sqrt(1.-sn**2)
          endif
          if(LpTypeCorrUse.eq.PolarizedGuinier) then
            LCorrection=LCorrection*cos(2.*Th-BetaGMonUse)
          else
            if(KIllum(KDatBlock).eq.1) then
              LCorrection=LCorrection*sqrt(1.-sn**2)
            else if(KIllum(KDatBlock).eq.2) then
              LCorrection=LCorrection*sn
            endif
          endif
        else
          LCorrection=2.*sn*sqrt(1.-sn**2)
        endif
        if(LpTypeCorrUse.eq.PolarizedCircular) then
          t1=(1.+cs2q)*.5
          t2=0.
        else if(LpTypeCorrUse.eq.PolarizedMonoPer) then
          t1=(1.-FractPerfMonUse)*(Cos2ThMonQ+cs2q)/(1.+Cos2ThMonQ)
          t2=    FractPerfMonUse *(Cos2ThMon +cs2q)/(1.+Cos2ThMon )
        else if(LpTypeCorrUse.eq.PolarizedMonoPar.or.
     1          LpTypeCorrUse.eq.PolarizedGuinier) Then
          t1=(1.-FractPerfMonUse)*(1.+cs2q*Cos2ThMonQ)/(1.+Cos2ThMonQ)
          t2=    FractPerfMonUse *(1.+cs2q*Cos2ThMon )/(1.+Cos2ThMon )
        else if(LpTypeCorrUse.eq.PolarizedLinear) then
          t1=1.
          t2=0.
        endif
        LpCorrection=LCorrection/(t1+t2)
      endif
9999  return
      end
