      subroutine iom50(klic,tisk,FileName)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      character*(*) FileName
      character*256 t256,PureFileName,radka
      character*80 t80
      character*12 nazev
      character*8 Tasks(4)
      character*2 nty,At1,At2
      character*1 Orbit
      dimension PomMat(36),npom(28),xpom(28),FFBasicP(121),
     1          FFCoreP(121),FFValP(121),FFraP(MxDatBlock),
     2          FFiaP(MxDatBlock)
      integer tisk,State,RadiationP,ColorOrder,FeRGBCompress
      logical poprve,EqIgCase,ExistFile,eqrv,EqRV0,EqIV0,OldColor,
     1        Prepnuto,CoreValSourceDefined
      real LamAveP,LamA1P,LamA2P,LamRatP
      equivalence (npom,xpom)
      data tasks/'refine','fourier','dist','contour'/
      KPhaseIn=KPhase
      ln=0
      call GetPureFileName(FileName,PureFileName)
      if(klic.eq.0) then
        call SetBasicM50
!   Jestli je to potreba udelat tam kde to je opravdu potreba
!        if(.not.ExistM90) then
!          UseEFormat91=.false.
!          Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
!        endif
        LamAveP=-1.
        LamA1P=-1.
        LamA2P=-1.
        LamRatP=-1.
        RadiationP=-1
        LPFactorP=-1
        NAlfaP=-1
        AngleMonP=-1.
        FractPerfMonP=-1.
        AlphaGMonP=-1.
        BetaGMonP=-1.
        if(allocated(rmgc))
     1    deallocate(rmgc,rm6gc,s6gc,KwSymGC,GammaIntGC,rtgc,rc3gc,
     2               rc4gc,rc5gc,rc6gc,iswGC,RMagGC,ZMagGC)
        if(ExistFile(FileName)) then
          call OpenFile(m50,FileName,'formatted','old')
          if(ErrFlag.ne.0) go to 9999
        else
          go to 9999
        endif
        read(m50,FormA,err=9000,end=9010) Radka
        LastPosition=0
        call kus(Radka,LastPosition,Cislo)
        if(.not.EqIgCase(Cislo,'version')) rewind(m50)
        KPhP=0
        NPhase=0
        Poprve=.true.
        LastPosition=len(Radka)
1100    if(LastPosition.ge.len(Radka)) then
1150      read(m50,FormA,err=9000,end=1500) Radka
          if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1       idel(Radka).le.0.or.Radka(1:2).eq.'--') go to 1150
          if(EqIgCase(Radka,'end')) go to 1500
          LastPosition=0
          call vykric(radka)
        endif
        call kus(Radka,LastPosition,Nazev)
        i=islovo(nazev,IdM50,72)
        if(i.eq.IdM50Phase) then
          KPhP=KPhP+1
          PhaseName(KPhP)=Radka(LastPosition+1:)
          if(PhaseName(KPhP).eq.' ') then
            write(PhaseName(KPhP),'(''Phase#'',i2)') KPhP
            call Zhusti(PhaseName(KPhP))
          endif
          Poprve=.true.
        else if(i.le.0) then
          go to 1100
        endif
        KPh=max(KPhP,1)
        KPhase=KPh
        NPhase=max(NPhase,KPh)
        if(i.eq.IdM50Title) then
          if(LastPosition.lt.len(Radka)) then
            StructureName=Radka(LastPosition+1:)
            LastPosition=len(Radka)
          else
            StructureName=' '
          endif
        else if(i.eq.IdM50RoundMethod) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          RoundMethod=npom(1)
        else if(i.eq.IdM50NDim) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          NDim(KPh)=npom(1)
          NDimI(KPh)=NDim(KPh)-3
          NDimQ(KPh)=NDim(KPh)**2
          MaxNDim=max(MaxNDim,NDim(KPh))
          MaxNDimI=MaxNDim-3
        else if(i.eq.IdM50NComp) then
          call StToInt(Radka,LastPosition,NComp(KPh),1,.false.,ich)
          if(ich.ne.0) go to 9000
          MaxNComp=max(MaxNComp,NComp(KPh))
        else if(i.eq.IdM50Centro) then
          MaxNCSymm=2
        else if(i.eq.IdM50Symmetry) then
          NSymm(KPh)=NSymm(KPh)+1
          MaxNSymm=max(MaxNSymm,NSymm(KPh))
        else if(i.eq.IdM50LocalSymm) then
          NSymmL(KPh)=NSymmL(KPh)+1
          MaxNSymmL=max(MaxNSymmL,NSymmL(KPh))
        else if(i.eq.IdM50ChemFormula) then
          Formula(KPh)=Radka(LastPosition+1:)
          call PitFor(1,ich)
          MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPh))
        else if(i.eq.IdM50LattVec) then
          if(Poprve) NLattVec(KPh)=0
          NLattVec(KPh)=NLattVec(KPh)+1
          MaxNLattVec=max(MaxNLattVec,NLattVec(KPh))
          Poprve=.false.
        else if(i.eq.IdM50Lattice) then
          if(Poprve) then
            call kus(Radka,LastPosition,Cislo)
            call Velka(Cislo)
            j=index(SmbC,Cislo(1:1))
            if(j.eq.1) then
              NLattVec(KPh)=1
            else if(j.le.5) then
              NLattVec(KPh)=2
            else if(j.eq.6) then
              NLattVec(KPh)=3
            else if(j.eq.7) then
              NLattVec(KPh)=4
            endif
            MaxNLattVec=max(MaxNLattVec,NLattVec(KPh))
          endif
        else if(i.eq.IdM50RefOfAnom) then
          call StToInt(Radka,LastPosition,KAnRef,NDatBlock,.false.,
     1                 ich)
        else if(i.eq.IdM50Magnetic) then
          call StToInt(Radka,LastPosition,MagneticType(KPh),1,.false.,
     1                 ich)
          if(ich.ne.0) go to 9000
          MaxMagneticType=max(MaxMagneticType,MagneticType(KPh))
        else if(i.eq.IdM50FormTab) then
          call StToInt(Radka,LastPosition,FFType(KPh),1,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50Lambda) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          LamAveP=xpom(1)
        else if(i.eq.IdM50RadType) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          RadiationP=npom(1)
        else if(i.eq.IdM50LpFactor) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          LPFactorP=npom(1)
        else if(i.eq.IdM50NAlpha) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          NAlfaP=npom(1)
        else if(i.eq.IdM50MonAngle) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          AngleMonP=xpom(1)
        else if(i.eq.IdM50AlphaGMon) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          AlphaGMonP=xpom(1)
        else if(i.eq.IdM50BetaGMon) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          BetaGMonP=xpom(1)
        else if(i.eq.IdM50Kalpha1) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          LamA1P=xpom(1)
        else if(i.eq.IdM50Kalpha2) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          LamA2P=xpom(1)
        else if(i.eq.IdM50Kalphar) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          LamRatP=xpom(1)
        else if(i.eq.IdM50PerfMon) then
          call StToReal(Radka,LastPosition,xpom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          FractPerfMonP=xpom(1)
        endif
        go to 1100
1500    if(NPhase.eq.1) PhaseName(1)=' '
        n=NDim(KPhase)
        NDim(KPhase)=MaxNDim
        MaxNSymm=MaxNSymm*MaxNCSymm
        call AllocateSymm(MaxNSymm,MaxNLattVec,MaxNComp,NPhase)
        NDim(KPhase)=n
        if(allocated(rm6l)) deallocate(rm6l,rml,s6l,ISwSymmL,KwSymL,rtl,
     1                                 rc3l,rc4l,rc5l,rc6l,GammaIntL,
     2                                 LXYZMode,XYZMode,NXYZMode,
     3                                 MXYZMode)
        if(MaxNSymmL.gt.0) then
          allocate(rm6l     (MaxNDim**2,MaxNSymmL  ,MaxNComp,NPhase),
     1             rml      (9         ,MaxNSymmL  ,MaxNComp,NPhase),
     2             s6l      (MaxNDim   ,MaxNSymmL  ,MaxNComp,NPhase),
     3             rtl      (36        ,MaxNSymmL  ,MaxNComp,NPhase),
     4             rc3l     (100       ,MaxNSymmL  ,MaxNComp,NPhase),
     5             rc4l     (225       ,MaxNSymmL  ,MaxNComp,NPhase),
     6             rc5l     (441       ,MaxNSymmL  ,MaxNComp,NPhase),
     7             rc6l     (784       ,MaxNSymmL  ,MaxNComp,NPhase),
     8             GammaIntL(9         ,MaxNSymmL  ,MaxNComp,NPhase),
     9             KwSymL  (mxw       ,MaxNSymmL   ,MaxNComp,NPhase),
     a             ISwSymmL           (MaxNSymmL   ,MaxNComp,NPhase),
     1             LXYZMode(3*(MaxNSymmL+1),NPhase),
     2             XYZMode (3*(MaxNSymmL+1),3*(MaxNSymmL+1),NPhase),
     3             NXYZMode(NPhase),MXYZMode(NPhase))
          call SetIntArrayTo(NXYZMode,NPhase,0)
          call SetIntArrayTo(MXYZMode,NPhase,0)
        endif
        if(NDatBlock.le.0.or.(.not.Existm90.and..not.Existm95)) then
          if(RadiationP.gt.0) Radiation(1)=RadiationP
          if(LamAveP.gt.0.) then
            LamAve(1)=LamAveP
            LamA1(1)=LamAveP
            LamPwd(1,1)=LamAveP
            LamA2(1)=LamAveP
            LamPwd(2,1)=LamAveP
            NAlfaP=1
          endif
          if(LamA1P.gt.0.) then
            LamA1(1)=LamA1P
            LamPwd(1,1)=LamA1P
            NAlfaP=1
          endif
          if(LamA2P.gt.0.) then
            LamA2(1)=LamA2P
            LamPwd(2,1)=LamA2P
            NAlfaP=NAlfaP+1
          endif
          if(LamRatP.gt.0.) LamRat(1)=LamRatP
          if(LPFactorP.ge.0) LPFactor(1)=LPFactorP
          if(NAlfaP.gt.0) NAlfa(1)=NAlfaP
          if(AngleMonP.gt.0.) AngleMon(1)=AngleMonP
          if(AlphaGMonP.gt.0.) AlphaGMon(1)=AlphaGMonP
          if(BetaGMonP.gt.0.) BetaGMon(1)=BetaGMonP
          if(FractPerfMonP.ge.0.) FractPerfMon(1)=FractPerfMonP
          KLam(1)=LocateInArray(LamAveP,LamAveD,7,.0001)
          if(NAlfaP.eq.1.and.klam(1).eq.0)
     1      KLam(1)=LocateInArray(LamAveP,LamA1D,7,.0001)
        endif
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=MaxNAtFormula
        m=121
        i=NDatBlock
        if(n.gt.0) then
          allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1             AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2             AtWeight(n,NPhase),AtRadius(n,NPhase),
     3             AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4             AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5             FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6             FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7             FFra(n,NPhase,i),FFia(n,NPhase,i),
     8             FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9             TypeFFMag(n,NPhase),
     a             FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1             FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
          allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1             PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2             ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3             HNSlater(n,NPhase),HZSlater(n,NPhase),
     4             ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5             NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6             CoreValSource(n,NPhase))
          CoreValSource='Default'
          CoreValSourceDefined=.false.
        endif
        lnd=NextLogicNumber()
        if(OpSystem.le.0) then
          t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                'typical_distances.dat'
        else
          t256=HomeDir(:idel(HomeDir))//'bondval/typical_distances.dat'
        endif
        call OpenFile(lnd,t256,'formatted','old')
        do KPh=1,NPhase
          KPhase=KPh
          call PitFor(0,ich)
          call SetFormula(Formula(KPhase))
          do i=1,NAtFormula(KPh)
            AtTypeFull(i,KPh)=AtType(i,KPh)
            AtTypeMag(i,KPh)=' '
            AtTypeMagJ(i,KPh)=' '
          enddo
          call EM50ReadAllFormFactors
          call EM50FormFactorsSet
          if(n.gt.0) call SetRealArrayTo(TypicalDist(1,1,KPh),n**2,-1.)
          do i=1,NAtFormula(KPh)
            if(EqIgCase(AtType(i,KPh),'D')) then
              AtNum(i,KPh)=1.
            else
              AtNum(i,KPh)=0.
              do j=1,98
                if(EqIgCase(AtType(i,KPh),atn(j))) then
                  AtNum(i,KPh)=float(j)
                  exit
                endif
              enddo
            endif
            do 1620j=i,NAtFormula(KPh)
              rewind lnd
              read(lnd,FormA) t80
              if(t80(1:1).ne.'#') rewind lnd
1610          read(lnd,'(2(a2,2x),f6.3)',end=1620) At1,At2,pom
              if((EqIgCase(AtType(i,KPh),At1).and.
     1            EqIgCase(AtType(j,KPh),At2)).or.
     2           (EqIgCase(AtType(j,KPh),At1).and.
     3            EqIgCase(AtType(i,KPh),At2))) then
                TypicalDist(i,j,KPh)=pom
                TypicalDist(j,i,KPh)=pom
                go to 1620
              else
                go to 1610
              endif
1620        continue
          enddo
        enddo
        call CloseIfOpened(lnd)
        read(m50,FormA,err=9000,end=9010) Radka
        LastPosition=0
        call kus(Radka,LastPosition,Cislo)
        if(.not.EqIgCase(Cislo,'version')) rewind(m50)
        KPhP=0
        Poprve=.true.
        OldColor=.false.
        icomp=1
        isymm=0
        isymml=0
        ilatt=0
        iqi=0
        iqr=0
        nat=0
        ntd=0
        ndtr=0
        ndti=0
        icomm=0
        StatusM50=StatusM50*NPhase
        LastPosition=len(Radka)
2100    if(LastPosition.ge.len(Radka)) then
2110      read(m50,FormA,err=9000,end=3000) Radka
          if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1       idel(Radka).le.0.or.Radka(1:2).eq.'--') go to 2110
          if(EqIgCase(Radka,'end')) go to 3000
          LastPosition=0
          call vykric(radka)
        endif
        call kus(Radka,LastPosition,Nazev)
        i=islovo(nazev,IdM50,72)
        if(i.eq.IdM50Phase) then
          KPhP=KPhP+1
          icomp=1
          isymm=0
          isymml=0
          ilatt=0
          iqi=0
          iqr=0
          nat=0
          ntd=0
          Poprve=.true.
          NQMag(KPhP)=0
        else if(i.le.0) then
          go to 2100
        endif
        KPh=max(KPhP,1)
        KPhase=KPh
        if(i.eq.IdM50Title) then
          LastPosition=len(Radka)
        else if(i.eq.IdM50Cell) then
          call StToReal(Radka,LastPosition,CellPar(1,1,KPh),6,.false.,
     1                  ich)
          if(ich.ne.0) go to 9000
          if(CellPar(1,1,KPh).gt..1) StatusM50=StatusM50-10000
        else if(i.eq.IdM50EsdCell) then
          call StToReal(Radka,LastPosition,CellParSU(1,1,KPh),6,.false.,
     1                  ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50Qi) then
          iqi=iqi+1
          if(iqi.gt.NDimI(KPh)) then
            LastPosition=len(Radka)
            go to 2100
          else
            call StToReal(Radka,LastPosition,qui(1,iqi,KPh),3,.false.,
     1                      ich)
            if(ich.ne.0) go to 9000
          endif
        else if(i.eq.IdM50Qr) then
          iqr=iqr+1
          if(iqr.gt.NDimI(KPh)) then
            LastPosition=len(Radka)
            go to 2100
          else
            call StToReal(Radka,LastPosition,quir(1,iqr,KPh),3,.false.,
     1                    ich)
            if(ich.ne.0) go to 9000
          endif
        else if(i.eq.IdM50QMag) then
          call StToReal(Radka,LastPosition,xpom,3,.false.,ich)
          if(ich.ne.0) go to 9000
          if(EqRV0(xpom,3,.0001)) then
            call SetRealArrayTo(xpom,3,0.)
          else
            NQMag(KPh)=NQMag(KPh)+1
          endif
          QMag(1:3,NQMag(KPh),KPh)=xpom(1:3)
        else if(i.eq.IdM50WMatrix) then
          icomp=icomp+1
          if(icomp.gt.NComp(KPh)) go to 2100
          do j=1,NDimQ(KPh),NDim(KPh)
            read(m50,FormA,err=9000,end=9010) Radka
            LastPosition=0
            call StToReal(Radka,LastPosition,PomMat(j),NDim(KPh),
     1                    .false.,ich)
           if(ich.ne.0) go to 9000
          enddo
          call trmat(PomMat,zv(1,icomp,KPh),NDim(KPh),NDim(KPh))
          LastPosition=len(Radka)
        else if(i.eq.IdM50Commen) then
          icomm=icomm+1
          KCommen(KPh)=1
          if(icomm.eq.1.and.index(Radka(LastPosition+1:),'.').le.0) then
            call StToInt(Radka,LastPosition,NCommen(1,1,KPh),3,.false.,
     1                   ich)
          else
            if(icomm.eq.2.and.ICommen(KPh).eq.0) then
              do i=1,3
                RCommen(i,1,KPh)=NCommen(i,1,KPh)
              enddo
            endif
            j=(icomm-1)*3+1
            call StToReal(Radka,LastPosition,RCommen(j,1,KPh),3,.false.,
     1                    ich)
            ICommen(KPh)=1
          endif
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50Tzero) then
          call StToReal(Radka,LastPosition,trez(1,1,KPh),
     1                  max(NDimI(KPh),1),.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50ChemFormula) then
          if(NAtFormula(KPh).gt.0) StatusM50=StatusM50-10
        else if(i.eq.IdM50Centro) then
          NCSymm(KPh)=2
        else if(i.eq.IdM50Symmetry) then
          isymm=isymm+1
          if(isymm.eq.1) StatusM50=StatusM50-1000
          call ReadSymm(radka(LastPosition:),rm6(1,isymm,1,KPh),
     1                  s6(1,isymm,1,KPh),symmc(1,isymm,1,KPh),
     2                  zmagp,tisk)
          if(ErrFlag.ne.0) go to 9999
          call MatBlock3(rm6(1,isymm,1,KPh),rm(1,isymm,1,KPh),NDim(KPh))
          ISwSymm(isymm,1,KPh)=1
          zmag(isymm,1,KPh)=zmagp
          call CrlMakeRMag(isymm,1)
          LastPosition=len(Radka)
        else if(i.eq.IdM50LocalSymm) then
          isymml=isymml+1
          read(m50,*,err=9000,end=9010)
     1      ((rm6l(i+(j-1)*NDim(KPh),isymml,1,KPh),j=1,NDim(KPh)),
     2                                             i=1,NDim(KPh))
          read(m50,*,err=9000,end=9010)
     1      (s6l(i,isymml,1,KPh),i=1,NDim(KPh))
          LastPosition=len(Radka)
        else if(i.eq.IdM50XYZMode) then
          NXYZMode(KPh)=NXYZMode(KPh)+1
          MXYZMode(KPh)=MXYZMode(KPh)+1
          call kus(radka,LastPosition,LXYZMode(NXYZMode(KPh),KPh))
          read(m50,*,err=9000,end=9010)
     1      (XYZMode(i,NXYZMode(KPh),KPh),i=1,3*(NSymmL(KPh)+1))
        else if(i.eq.IdM50Lattice) then
          call kus(radka,LastPosition,Lattice(KPh))
          call velka(Lattice(KPh))
        else if(i.eq.IdM50LattVec) then
          ilatt=ilatt+1
          call SetRealArrayTo(Vt6(1,ilatt,1,KPh),MaxNDim,0.)
          call StToReal(Radka,LastPosition,Vt6(1,ilatt,1,KPh),
     1                  NDim(KPh),.false.,ich)
          if(ich.ne.0) go to 9000
          LastPosition=len(Radka)
        else if(i.eq.IdM50SpGroup) then
          call kus(radka,LastPosition,grupa(KPh))
          call StToInt(Radka,LastPosition,ngrupa(KPh),1,.false.,ich)
          if(ich.ne.0) go to 9000
          call uprat(grupa(KPh))
          if(LastPosition.lt.len(Radka)) then
            call StToInt(Radka,LastPosition,CrSystem(KPh),1,.false.,ich)
            if(ich.ne.0) go to 9000
            Monoclinic(KPh)=CrSystem(KPh)/10
          endif
        else if(i.eq.IdM50UnitsNumb) then
          call StToInt(Radka,LastPosition,NUnits(KPh),1,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50SgShift) then
          call StToReal(Radka,LastPosition,ShSg(1,KPh),NDim(KPh),
     1                  .false.,ich)
          if(ich.ne.0) go to 9000
          pom=0.
          do j=1,NDim(KPh)
            pom=pom+abs(ShSg(j,KPh))
          enddo
          if(Pom.gt..001) then
            StdSg(KPh)=0
          else
            StdSg(KPh)=1
          endif
        else if(i.eq.IdM50Twin) then
          call StToInt(Radka,LastPosition,npom,1,.false.,ich)
          if(ich.ne.0) go to 9000
          NTwin=npom(1)
          do n=2,NTwin
            do j=1,3
              read(m50,*,err=9000,end=9010)(rtw(j+(k-1)*3,n),
     1                                      k=1,3)
            enddo
            call matinv(rtw(1,n),rtwi(1,n),pom,3)
            if(pom.eq.0.) then
              write(t80,'(i1,a2,1x)') n,nty(n)
              call FeChybne(-1.,-1.,t80(1:4)//'twinning matrix is'//
     1                      ' singular.',' ',SeriousError)
              ErrFlag=1
              go to 9999
            endif
          enddo
          LastPosition=len(Radka)
        else if(i.eq.IdM50PhaseTwin) then
          call StToInt(Radka,LastPosition,KPhaseTwin,NTwin,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(i.eq.IdM50Densities) then
          ChargeDensities=.true.
        else if(i.eq.IdM50WaveFile) then
          call Kus(Radka,LastPosition,NameOfWaveFile)
          if(EqIgCase(NameOfWaveFile,'wavef.dat'))
     1      NameOfWaveFile='wavefn.dat'
        else if(i.eq.IdM50ParentStr) then
          ParentStructure=.true.
        else if(i.eq.IdM50TypDist) then
          ntd=ntd+1
          call ReallocateTypicalDist(ntd)
          call Kus(Radka,LastPosition,AtTypicalDist(1,ntd,KPh))
          i=KtAt(AtType(1,KPh),MaxNAtFormula,AtTypicalDist(1,ntd,KPh))
          call Kus(Radka,LastPosition,AtTypicalDist(2,ntd,KPh))
          j=KtAt(AtType(1,KPh),MaxNAtFormula,AtTypicalDist(2,ntd,KPh))
          call StToReal(Radka,LastPosition,DTypicalDist(ntd,KPh),1,
     1                  .false.,ich)
          if(ich.ne.0) go to 9000
          NTypicalDist(KPh)=ntd
          if(i.gt.0.and.j.gt.0) then
            TypicalDist(i,j,KPh)=DTypicalDist(ntd,KPh)
            TypicalDist(j,i,KPh)=DTypicalDist(ntd,KPh)
          endif
        else if(i.eq.IdM50Atom) then
          if(MaxNAtFormula.gt.0) then
            nat=nat+1
            call Kus(Radka,LastPosition,AtTypeFull(nat,KPh))
            if(.not.EqIgCase(AtType(nat,KPh),AtTypeFull(nat,KPh))) then
              call EM50OneFormFactorSet(nat)
              call GetPureAtType(AtTypeFull(nat,KPh),AtType(nat,KPh))
            endif
            call SetRealArrayTo(FFCore(1,nat,KPh),iabs(FFType(KPh)),0.)
            call SetRealArrayTo(FFVal(1,nat,KPh),iabs(FFType(KPh)),0.)
            call SetRealArrayTo(ZSTOA(1,1,1,nat,NPhase),1960,0.)
            call SetRealArrayTo(CSTOA(1,1,1,nat,NPhase),1960,0.)
            call SetIntArrayTo(NSTOA(1,1,1,nat,NPhase),1960,0)
            call SetIntArrayTo(NCoefSTOA(1,1,nat,NPhase),49,0)
            ndtr=0
            ndti=0
          endif
        else if(i.eq.IdM50AtMag) then
          call Kus(Radka,LastPosition,AtTypeMag(nat,KPh))
        else if(i.eq.IdM50AtMagJ) then
          call Kus(Radka,LastPosition,AtTypeMagJ(nat,KPh))
        else if(i.eq.IdM50FFBasic) then
          if(nat.gt.0.and.(.not.ChargeDensities.or.
     1       .not.CoreValSourceDefined.or.
     2       EqIgCase(CoreValSource(nat,KPh),'FF-table')))
     3      call ReadFFFromM50(FFBasic(1,nat,KPh),iabs(FFType(KPh)))
          go to 2600
        else if(i.eq.IdM50FFCore) then
          if(nat.gt.0) then
            call StToInt(Radka,LastPosition,PopCore(1,nat,KPh),28,
     1                   .false.,ich)
            if(ich.eq.1) go to 9000
            if(EqIgCase(CoreValSource(nat,KPh),'FF-table').or.
     1         .not.CoreValSourceDefined)
     2        call ReadFFFromM50(FFCore(1,nat,KPh),FFType(KPh))
            TypeCore(nat,KPh)=0
          endif
          go to 2600
        else if(i.eq.IdM50FFVal) then
          if(nat.gt.0) then
            call StToInt(Radka,LastPosition,PopVal(1,nat,KPh),1,.false.,
     1                   ich)
            if(ich.eq.1) go to 9000
            call SetIntArrayTo(PopVal(2,nat,KPh),27,0)
            if(PopVal(1,nat,KPh).ge.0) then
              call StToInt(Radka,LastPosition,PopVal(2,nat,KPh),27,
     1                     .false.,ich)
              if(ich.eq.1) go to 9000
              TypeVal(nat,KPh)=0
            else if(PopVal(1,nat,KPh).eq.-1) then
              TypeVal(nat,KPh)=-1
            else if(PopVal(1,nat,KPh).eq.-2) then
              call StToInt(Radka,LastPosition,HNSlater(nat,KPh),1,
     1                     .false.,ich)
              if(ich.eq.1) go to 9000
              call StToReal(Radka,LastPosition,HZSlater(nat,KPh),1,
     1                      .false.,ich)
              if(ich.eq.1) go to 9000
              TypeVal(nat,KPh)=-2
            endif
            if(EqIgCase(CoreValSource(nat,KPh),'FF-table').or.
     1         .not.CoreValSourceDefined) then
              call ReadFFFromM50(FFVal(1,nat,KPh),FFType(KPh))
              fj=0
              do ii=1,27
                fj=fj+PopVal(ii,nat,KPh)
              enddo
              do ii=1,FFType(KPh)
                FFBasic(ii,nat,KPh)=FFCore(ii,nat,KPh)+
     1                              fj*FFVal(ii,nat,KPh)
              enddo
            endif
          endif
          go to 2600
        else if(i.eq.IdM50WFFile) then
          CoreValSourceDefined=.true.
          CoreValSource(nat,KPh)=Radka(LastPosition+1:)
          LastPosition=len(Radka)
        else if(i.eq.IdM50STOFuntion) then
2200      read(m50,FormA,err=9000,end=9010) Radka
          if(Radka.eq.' ') go to 2200
          k=0
          call kus(Radka,k,Cislo)
          i=islovo(Cislo,IdM50,72)
          if(i.gt.0) then
            backspace m50
            LastPosition=len(Radka)
            go to 2100
          endif
          read(Radka,112) j,Orbit,n
          k=0
          do i=1,7
            if(EqIgCase(OrbitName(i),Orbit)) then
              k=i
              exit
            endif
          enddo
          if(k.eq.0) go to 9000
          NCoefSTOA(j,k,nat,KPhase)=n
          read(m50,113,err=9000,end=9010)
     1      (CSTOA(j,k,i,nat,KPh),ZSTOA(j,k,i,nat,KPh),
     2       NSTOA(j,k,i,nat,KPh),i=1,n)
          go to 2200
        else if(i.eq.IdM50NSlater) then
          if(nat.gt.0) then
            call StToInt(Radka,LastPosition,NSlater(1,nat,KPh),8,
     1                   .false.,ich)
            if(ich.eq.1) go to 9000
          endif
          LastPosition=len(Radka)
        else if(i.eq.IdM50ZSlater.or.i.eq.IdM50DzSlater) then
          if(nat.gt.0) then
            call StToReal(Radka,LastPosition,ZSlater(1,nat,KPh),8,
     1                    .false.,ich)
            if(ich.eq.1) go to 9000
          endif
          LastPosition=len(Radka)
        else if(i.eq.IdM50AtRadius) then
          if(nat.gt.0) then
            call StToReal(Radka,LastPosition,AtRadius(nat,KPh),1,
     1                    .false.,ich)
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50Color) then
          if(nat.gt.0) then
            lpos=LastPosition
            call kus(Radka,LastPosition,Cislo)
            if(EqIgCase(Cislo(:idel(Cislo)),'purple')) Cislo='Magenta'
            j=ColorOrder(Cislo)
            if(j.gt.0) then
              AtColor(nat,KPh)=ColorNumbers(j)
              OldColor=.true.
            else
              LastPosition=lpos
              call StToInt(Radka,LastPosition,npom,1,.false.,ich)
              if(ich.ne.0) go to 9000
              j=npom(1)
              l=1000000
              do m=1,3
                npom(m)=j/l
                j=j-npom(m)*l
                l=l/1000
              enddo
              AtColor(nat,KPh)=
     1          FeRGBCompress(npom(1),npom(2),npom(3))
            endif
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50FPrime) then
2400      ndtr=ndtr+1
!          if(ndtr.gt.NDatBlock) go to 2100
          if(Radiation(ndtr).ne.XRayRadiation) go to 2400
          if(nat.gt.0.and.KAnRef(ndtr).ne.1) then
            call StToReal(Radka,LastPosition,ffra(nat,KPh,ndtr),1,
     1                    .false.,ich)
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50FDoublePrime) then
2410      ndti=ndti+1
!          if(ndti.gt.NDatBlock) go to 2100
          if(Radiation(ndti).ne.XRayRadiation) go to 2410
          if(nat.gt.0.and.KAnRef(ndti).ne.1) then
            call StToReal(Radka,LastPosition,ffia(nat,KPh,ndti),1,
     1                    .false.,ich)
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50FNeutron) then
          if(nat.gt.0) then
            call StToReal(Radka,LastPosition,ffn(nat,KPh),1,.false.,ich)
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50FNeutronIm) then
          if(nat.gt.0) then
            call StToReal(Radka,LastPosition,ffni(nat,KPh),1,.false.,
     1                    ich)
            if(ich.eq.1) go to 9000
          endif
        else if(i.eq.IdM50FFMag) then
          if(nat.gt.0) then
            if(LastPosition.lt.len(Radka)) then
              call StToInt(Radka,LastPosition,TypeFFMag(nat,KPh),1,
     1                     .false.,ich)
              if(ich.ne.0) go to 9000
            else
              TypeFFMag(nat,KPh)=0
            endif
            call ReadFFFromM50(FFMag(1,nat,KPh),7)
          endif
        endif
        go to 2100
2600    if(ErrFlag.eq.1) then
          go to 9000
        else if(ErrFlag.eq.2) then
          go to 9010
        endif
        LastPosition=len(Radka)
        go to 2100
3000    itwph=max(NPhase,NTwin)
        ln=NextLogicNumber()
        call OpenFile(ln,PureFileName(:idel(PureFileName))//'.l51',
     1                'formatted','unknown')
        write(ln,FormA1)('*',i=1,79)
        j=0
        State=0
3100    read(m50,FormA,end=3200,err=3200) Radka
        if(j.eq.0.and.Radka(1:4).eq.'****') go to 3100
        LastPosition=0
        call kus(Radka,LastPosition,Cislo)
        do i=1,4
          if(EqIgCase(Cislo,Tasks(i))) State=State+10**(i-1)
        enddo
        write(ln,FormA) Radka(:idel(Radka))
        j=1
        go to 3100
3200    do i=4,1,-1
          if(State.eq.1111) go to 3300
          j=10**(i-1)
          if(State/j.eq.0) then
            if(NPhase.gt.1.and.i.ne.1) then
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))//' '//
     1           PhaseName(1)(:idel(PhaseName(1)))
            else
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))
            endif
            write(ln,'(''end '',a)') Tasks(i)(:idel(Tasks(i)))
          else
            State=State-j
          endif
        enddo
3300    call CloseIfOpened(ln)
        if(StatusM50.lt.10000) then
          Prepnuto=.false.
          do KPh=1,NPhase
            KPhase=KPh
            do i=1,NDimI(KPh)
              call AddVek(qui(1,i,KPh),quir(1,i,KPh),qu(1,i,1,KPh),3)
            enddo
            nc=index(smbc,Lattice(KPh))
            if(nc.gt.0.and.nc.le.7) call EM50GenVecCentr(nc,.false.,ich)
            if(ParentStructure) ZMag=1.
            call CrlExpandCentroSym
            call SetMet(tisk)
            if(ErrFlag.ne.0) go to 9999
            if(StatusM50.lt.1000) then
              if(idel(Lattice(KPh)).gt.1) then
                Lattice(KPh)='X'
                call FindSmbSg(Grupa(KPh),ChangeOrderYes,1)
              endif
              if(lattice(KPh).ne.'X') then
                call CtiLatt(tisk)
                if(ErrFlag.ne.0) go to 9999
              endif
              call CrlOrderMagSymmetry
              if(Grupa(KPh)(1:1).ne.'?')
     1          call FindSmbSg(Grupa(KPh),ChangeOrderYes,1)
              call CorrectForStandardSetting
              call FinSym(tisk)
              call SetScode
            endif
            if(NAtFormula(KPh).gt.0) then
              FFType(KPh)=min(FFType(KPh),121)
              if(FFType(KPh).eq.-62) call SetIntFormFCoef
              if(OldColor) then
                do i=1,NAtFormula(KPh)
                  AtRadius(i,KPh)=AtRadius(i,KPh)*.5
                enddo
              endif
              if(ExistElectronData) call SetIntFormFCoefEl
            endif
            if(CrSystem(KPh).le.0) then
              call CheckSystem(CellPar(1,1,i),Monoclinic(KPh),
     1                         CrSystem(KPh))
              if(CrSystem(KPh).eq.2)
     1          CrSystem(KPh)=CrSystem(KPh)+Monoclinic(KPh)*10
            endif
          enddo
        endif
        do KPh=1,NPhase
          KPhase=KPh
          if(NDimI(KPh).le.0.and.NQMag(KPh).le.0) then
            KCommen(KPh)=0
            call SetRealArrayTo(trez(1,1,KPh),NDimI(KPh),0.)
          endif
          j=0
          nt=iabs(FFType(KPh))
          do i=1,NAtFormula(KPh)
            if(j.eq.0) then
              FFCoreD=0.
              FFValD=0.
              j=1
            endif
            if(ChargeDensities) then
              if(CoreValSourceDefined) then
                call SetCoreVal(i)
                if(ErrFlag.ne.0) go to 9999
              else
                call CopyVek(FFCore(1,i,KPh),FFCoreP,nt)
                call CopyVek(FFVal (1,i,KPh),FFValP,nt)
                call SetCoreVal(i)
                if(ErrFlag.ne.0) go to 9999
                if(.not.EqRV(FFCoreP,FFCore(1,i,KPh),nt,.001).or.
     1             .not.EqRV(FFValP,FFVal (1,i,KPh),nt,.001)) then
                  call CopyVek(FFCoreP,FFCore(1,i,KPh),nt)
                  call CopyVek(FFValP ,FFVal (1,i,KPh),nt)
                  CoreValSource(i,KPh)='FF-table'
                endif
              endif
            endif
            if(idel(AtTypeMag(i,KPh)).gt.0) then
              if(EqIgCase(AtTypeMag(i,KPh)(1:4),'Ce2+'))
     1          AtTypeMag(i,KPh)(3:3)='3'
              idl=idel(AtTypeMag(i,KPh))
              if(LocateSubstring(AtTypeMag(i,KPh),'own',.false.,.true.)
     1           .gt.0) then
                AtTypeMag(i,KPh)=AtType(i,KPh)
                AtTypeMagJ(i,KPh)='own'
              else if(EqIgCase(AtTypeMag(i,KPh)(idl:idl),'m')) then
                AtTypeMag(i,KPh)(idl:idl)=' '
                AtTypeMagJ(i,KPh)='j0j2'
              else if(idel(AtTypeMagJ(i,KPh)).le.0) then
                AtTypeMagJ(i,KPh)='j0'
              endif
              if(.not.EqIgCase(AtTypeMagJ(i,KPh),'own')) then
                if(EqIgCase(AtTypeMagJ(i,KPh),'j0j2')) then
                  t80='Magnetic_formfactor_<j0>+c<j2>'
                  TypeFFMag(i,KPh)=0
                else
                  t80='Magnetic_formfactor_<'//
     1                 AtTypeMagJ(i,KPh)(:idel(AtTypeMagJ(i,KPh)))//'>'
                  if(EqIgCase(AtTypeMagJ(i,KPh),'j0')) then
                    TypeFFMag(i,KPh)=0
                  else
                    TypeFFMag(i,KPh)=1
                  endif
                endif
                call MagFFFromAtomFile(AtType(i,KPh),AtTypeMag(i,KPh),
     1                                 t80,FFMag(1,i,KPh),ich)
              endif
            endif
          enddo
          KCommenMax=max(KCommenMax,KCommen(KPh))
          if(tisk.eq.1) then
            call GetPureFileName(FileName,t80)
            if(ParentStructure) call QMag2SSG(t80,0)
            call inform50
            if(ParentStructure) call SSG2QMag(t80)
          endif
        enddo
        write(format80(2:2),100) maxNDim
        format83 (2:2)=format80(2:2)
        format83p(2:2)=format80(2:2)
        format83e(2:2)=format80(2:2)
        format95 (5:5)=format80(2:2)
        PrfFormat(2:2)=format80(2:2)
        t80=fln(:ifln)//'.m95'
        if(ExistFile(t80)) then
          do i=1,NRefBlock
            if(DifCode(i).gt.100) cycle
            call OpenRefBlockM95(95,i,t80)
            read(95,FormA) t256
            if((t256(7:7).eq.' '.or.t256(7:7).eq.'-').and.
     1         t256(6:6).ne.' ') then
              Format95(3:3)='6'
            else
              Format95(3:3)='8'
            endif
            close(95)
            exit
          enddo
        else
          Format95(3:3)='8'
        endif
      else if(Klic.eq.1) then
        if(ParentStructure) then
          call GetPureFileName(FileName,t80)
          call SSG2QMag(t80)
        endif
        call OpenFile(m50,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        write(m50,'(''Version Jana2006'')')
        t80=IdM50(IdM50Title)(:idel(IdM50(IdM50Title)))//' '//
     1      StructureName
        write(m50,FormA) t80(:idel(t80))
        do KPh=1,NPhase
          KPhase=KPh
          if(NPhase.gt.1) then
            write(m50,FormA1)('-',i=1,60)
            t80=IdM50(IdM50Phase)(:idel(IdM50(IdM50Phase)))//' '//
     1          PhaseName(KPh)(:idel(PhaseName(KPh)))
            write(m50,FormA) t80(:idel(t80))
          endif
          if(CellPar(1,1,KPh).gt..1) then
            if(ExistPowder) then
              write(radka,111)(CellPar(i,1,KPh),i=1,6)
            else
              write(radka,104)(CellPar(i,1,KPh),i=1,6)
            endif
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50Cell),Radka,k)
            if(ExistPowder) then
              write(radka,111)(CellParSU(i,1,KPh),i=1,6)
            else
              write(radka,104)(CellParSU(i,1,KPh),i=1,6)
            endif
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50EsdCell),Radka,k)
          endif
          if(NDimI(KPh).gt.0) then
            write(radka,108) NDim(KPh),IdM50(IdM50Ncomp),NComp(KPh)
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50NDim),Radka,k)
            do i=1,NDimI(KPh)
c              do j=1,3
c                if(abs(qu(j,i,1,KPh)).lt..0001.and.
c     1             abs(quir(j,i,KPh)).gt..0001) quir(j,i,KPh)=0.
c              enddo
              write(radka,107)(qu(j,i,1,KPh)-quir(j,i,KPh),j=1,3)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Qi),Radka,k)
              write(radka,107)(quir(j,i,KPh),j=1,3)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Qr),Radka,k)
            enddo
          endif
          if(NQMag(KPh).gt.0.and.MagneticType(KPh).gt.0) then
            do i=1,NQMag(KPh)
              write(radka,107) QMag(1:3,i,KPh)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Qmag),Radka,k)
            enddo
          endif
          if(NDimI(KPh).gt.0) then
            do i=2,NComp(KPh)
              write(m50,FormA)
     1          IdM50(IdM50WMatrix)(:idel(IdM50(IdM50WMatrix)))
              do k=1,NDim(KPh)
                write(m50,106)(zv(k+(j-1)*NDim(KPh),i,KPh),
     1                         j=1,NDim(KPh))
              enddo
            enddo
          endif
          if(NSymm(KPh).gt.0) then
            radka=Grupa(KPh)
            if(Radka.eq.' ') Radka='???'
            write(radka(idel(radka)+1:),'(2i4)')
     1        ngrupa(KPh),CrSystem(KPh)
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50SpGroup),Radka,k)
            pom=0.
            do j=1,NDim(KPh)
              pom=pom+abs(ShSg(j,KPh))
            enddo
            if(pom.gt..001) then
              StdSg(KPh)=0
            else
              StdSg(KPh)=1
            endif
            if(StdSg(KPh).eq.0) then
              write(radka,107)(shsg(i,KPh),i=1,NDim(KPh))
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50SgShift),Radka,k)
            endif
            Radka=Lattice(KPh)
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50Lattice),Radka,k)
            if(lattice(KPh)(1:1).eq.'X') then
              do i=1,NLattVec(KPh)
                write(radka,107)(vt6(j,i,1,KPh),j=1,NDim(KPh))
                k=0
                call WriteLabeledRecord(m50,IdM50(IdM50LattVec),Radka,k)
              enddo
            endif
            do i=1,NSymm(KPh)
              call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                      symmc(1,i,1,KPhase),0)
              Radka=' '
              do j=1,NDim(KPh)
                if(i.eq.1) then
                  if(NDim(KPh).le.3) then
                    Cislo=smbx(j)
                  else
                    Cislo=smbx6(j)
                  endif
                  if(.not.EqIgCase(symmc(j,i,1,KPh),Cislo)) go to 3400
                endif
                radka(idel(Radka)+2:)=symmc(j,i,1,KPh)
              enddo
              if(MaxMagneticType.gt.0.and..not.ParentStructure) then
                if(zmag(i,1,KPh).gt.0) then
                  Cislo='m'
                else
                  Cislo='-m'
                endif
                radka(idel(Radka)+2:)=Cislo(:idel(Cislo))
              endif
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Symmetry),Radka,k)
            enddo
            go to 3500
3400        call FeChybne(-1.,-1.,'The first space group operator is '//
     1                    'not identity.','Please contact authors.',
     2                    SeriousError)
          endif
3500      if(NSymmL(KPh).gt.0) then
            do n=1,NSymmL(KPh)
              write(Radka,105) n
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50LocalSymm),Radka,k)
              do i=1,NDim(KPh)
                write(m50,104)
     1            (rm6l(i+(j-1)*NDim(KPh),n,1,KPh),j=1,NDim(KPh))
              enddo
              write(m50,104)(s6l(i,n,1,KPh),i=1,NDim(KPh))
            enddo
            if(NXYZMode(KPh).gt.0) then
              do n=1,NXYZMode(KPh)
                k=0
                call WriteLabeledRecord(m50,IdM50(IdM50XYZMode),
     1                                  LXYZMode(n,KPh),k)
                write(m50,102)(XYZMode(i,n,KPh),i=1,MXYZMode(KPhase))
              enddo
            endif
          endif
          write(radka,'(i5)') NUnits(KPh)
          k=0
          call WriteLabeledRecord(m50,IdM50(IdM50Unitsnumb),Radka,k)
          if(Formula(KPh).ne.' ') then
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50ChemFormula),
     1                              Formula(KPh),k)
            if(ExistXRayData.or..not.ExistM90) then
              k=0
              write(Radka,105) FFType(KPh)
              call WriteLabeledRecord(m50,IdM50(IdM50FormTab),Radka,k)
            endif
          endif
          if(MagneticType(KPh).gt.0) then
            write(Radka,105) MagneticType(KPh)
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50Magnetic),Radka,k)
          endif
          nt=iabs(FFType(KPh))
          do i=1,NAtFormula(KPh)
            call uprat(AtTypeFull(i,KPh))
            t256=AtTypeFull(i,KPh)
            if(MagneticType(KPh).gt.0) then
              if(AtTypeMag(i,KPh).ne.' ') then
                t256(idel(t256)+2:)=IdM50(IdM50AtMag)
                t256(idel(t256)+2:)=
     1            AtTypeMag(i,KPh)(:idel(AtTypeMag(i,KPh)))
                t256(idel(t256)+2:)=IdM50(IdM50AtMagJ)
                t256(idel(t256)+2:)=
     1            AtTypeMagJ(i,KPh)(:idel(AtTypeMagJ(i,KPh)))
              endif
            endif
            t256(idel(t256)+2:)=IdM50(IdM50AtRadius)
            write(t256(idel(t256)+2:),103)
     1        AtRadius(i,KPh),IdM50(IdM50Color)
            call FeRGBUncompress(AtColor(i,KPh),npom(1),npom(2),npom(3))
            j=0
            do m=1,3
              j=j*1000+npom(m)
            enddo
            write(t256(idel(t256)+2:),'(i9)') j
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50Atom),t256,k)
            if(ChargeDensities) then
              t256=IdM50(IdM50WFFile)(:idel(IdM50(IdM50WFFile)))//' '//
     1             CoreValSource(i,KPh)(:idel(CoreValSource(i,KPh)))
              write(m50,FormA) t256(:idel(t256))
            endif
            call CopyVek(FFBasic(1,i,KPh),FFBasicP,nt)
            do j=1,NDatBlock
              FFraP(j)=FFra(i,KPh,j)
              FFiaP(j)=FFia(i,KPh,j)
            enddo
            FFnP=FFn(i,KPh)
            FFniP=FFni(i,KPh)
            if(ChargeDensities) then
              j1=IdM50FFBasic
              j2=IdM50FFVal
              call CopyVek(FFCore(1,i,KPh),FFCoreP,nt)
              call CopyVek(FFVal (1,i,KPh),FFValP,nt)
            else
              j1=IdM50FFBasic
              j2=IdM50FFBasic
            endif
            if(ChargeDensities) then
              if(.not.EqRV0(FFCoreP,nt,.001).or.
     1           .not.EqRV0(FFValP,nt,.001)) then
                call SetIgnoreWTo(.true.)
                call SetIgnoreETo(.true.)
              endif
            endif
            call EM50OneFormFactorSet(i)
            if(ChargeDensities) then
              if(.not.EqRV0(FFCoreP,nt,.001).or.
     1           .not.EqRV0(FFValP,nt,.001)) then
                call ResetIgnoreW
                call ResetIgnoreE
                call CopyVek(FFCoreP,FFCore(1,i,KPh),nt)
                call CopyVek(FFValP ,FFVal (1,i,KPh),nt)
              endif
            endif
            if(EqIgCase(CoreValSource(i,KPh),'STO-table')) then
              write(m50,FormA)
     1          IdM50(IdM50STOFuntion)(:idel(IdM50(IdM50STOFuntion)))
              do k=1,7
                do j=1,7
                  n=NCoefSTOA(j,k,i,KPhase)
                  if(n.le.0) cycle
                  write(m50,112) j,OrbitName(k),n
                  write(m50,113)
     1             (CSTOA(j,k,m,i,KPh),ZSTOA(j,k,m,i,KPh),
     2              NSTOA(j,k,m,i,KPh),m=1,n)
                enddo
              enddo
            endif
            m=0
            do j=j1,j2
              if(j.eq.IdM50FFCore) then
                call CopyVekI(PopCore(1,i,KPh),npom,28)
              else if(j.eq.IdM50FFVal) then
                call CopyVekI(PopVal(1,i,KPh),npom,28)
              else
                call SetIntArrayTo(npom,28,0)
              endif
              if(npom(1).ge.0) then
                n=28
3600            if(npom(n).eq.0) then
                  n=n-1
                  if(n.gt.0) go to 3600
                endif
                if(j.gt.j1) then
                  write(Radka,109)(npom(k),k=1,max(n,1))
                else
                  Radka=' '
                endif
              else
                write(Radka,109) npom(1)
                if(npom(1).eq.-2) then
                  n=idel(Radka)+2
                  write(Radka(n:),'(i5,f10.4)') HNSlater(i,KPh),
     1                                          HZSlater(i,KPh)
                endif
              endif
              m=m+1
              if(m.eq.1) then
                if(.not.EqRV(FFBasic(1,i,KPh),FFBasicP,nt,.0001).or.
     1             (ChargeDensities.and.
     2              EqIgCase(CoreValSource(i,KPh),'FF-table'))) then
                  k=0
                  call WriteLabeledRecord(m50,IdM50(j),Radka,k)
                  if(FFType(KPh).eq.-9) then
                    write(m50,'(7f11.6)')(FFBasicP(k),k=1,nt)
                  else
                    write(m50,106)(FFBasicP(k),k=1,nt)
                  endif
                  do k=1,nt
                    FFBasic(k,i,KPh)=FFBasicP(k)
                  enddo
                endif
              else if(m.eq.2) then
                k=0
                call WriteLabeledRecord(m50,IdM50(j),Radka,k)
                if(EqIgCase(CoreValSource(i,KPh),'FF-table'))
     1            write(m50,106)(FFCore(k,i,KPh),k=1,nt)
              else
                k=0
                call WriteLabeledRecord(m50,IdM50(j),Radka,k)
                if(EqIgCase(CoreValSource(i,KPh),'FF-table'))
     1            write(m50,106)(FFVal(k,i,KPh),k=1,nt)
              endif
            enddo
            if(EqIgCase(AtTypeMagJ(i,KPh),'own')) then
              write(Radka,109) TypeFFMag(i,KPh)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50FFmag),Radka,k)
              write(m50,106)(FFMag(j,i,KPh),j=1,7)
            endif
            if(KAnRef(KDatBlock).eq.0) then
              do j=1,NDatBlock
                if(abs(FFraP(j)-FFra(i,KPh,j)).gt..0001.or.
     1             abs(FFiaP(j)-FFia(i,KPh,j)).gt..0001) go to 3700
              enddo
              go to 3750
3700          do j=1,NDatBlock
                if(Radiation(j).ne.XRayRadiation) cycle
                write(Radka,'(f10.5,1x,a12,1x,f10.5)')
     1          FFraP(j),IdM50(IdM50FDoublePrime),FFiaP(j)
                k=0
                call WriteLabeledRecord(m50,IdM50(IdM50FPrime),Radka,k)
                FFra(i,KPh,j)=FFraP(j)
                FFia(i,KPh,j)=FFiaP(j)
              enddo
            endif
3750        if(ChargeDensities) then
              write(radka,105)(NSlater(j,i,KPh),j=1,8)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50NSlater),Radka,k)
              write(radka,106)(ZSlater(j,i,KPh),j=1,8)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50ZSlater),Radka,k)
            endif
            if(abs(FFn (i,KPh)-FFnP ).gt..0001.or.
     1         abs(FFni(i,KPh)-FFniP).gt..0001) then
              write(Radka,'(f10.5,1x,a12,1x,f10.5)')
     1          ffnP,IdM50(IdM50FneutronIm),ffniP
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Fneutron),Radka,k)
              FFn (i,KPh)=FFnP
              FFni(i,KPh)=FFniP
            endif
          enddo
          if(NTypicalDistMax.gt.0) then
            lnd=NextLogicNumber()
            if(OpSystem.le.0) then
              t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1                    'typical_distances.dat'
            else
              t256=HomeDir(:idel(HomeDir))//
     1             'bondval/typical_distances.dat'
            endif
            call OpenFile(lnd,t256,'formatted','old')
            do i=1,NTypicalDist(KPh)
              j=KtAt(AtType(1,KPh),MaxNAtFormula,AtTypicalDist(1,i,KPh))
              if(j.le.0) cycle
              j=KtAt(AtType(1,KPh),MaxNAtFormula,AtTypicalDist(2,i,KPh))
              if(j.le.0) cycle
              rewind lnd
              read(lnd,FormA) t80
              if(t80(1:1).ne.'#') rewind lnd
3810          read(lnd,'(2(a2,2x),f6.3)',end=3820) At1,At2,pom
              if((EqIgCase(AtTypicalDist(1,i,KPh),At1).and.
     1            EqIgCase(AtTypicalDist(2,i,KPh),At2)).or.
     2           (EqIgCase(AtTypicalDist(1,i,KPh),At2).and.
     3            EqIgCase(AtTypicalDist(2,i,KPh),At1))) then
                if(abs(pom-DTypicalDist(i,KPh)).gt..001) then
                  go to 3820
                else
                  cycle
                endif
              else
                go to 3810
              endif
3820          write(Cislo,'(f15.3)') DTypicalDist(i,KPh)
              Radka=AtTypicalDist(1,i,KPh)//' '//
     1              AtTypicalDist(2,i,KPh)//' '//Cislo
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50TypDist),Radka,k)
            enddo
            call CloseIfOpened(lnd)
          endif
          if(KCommen(KPh).ne.0.and.NDimI(KPh).gt.0) then
            if(ICommen(KPh).eq.0) then
              write(Radka,105)(NCommen(i,1,KPh),i=1,3)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50Commen),Radka,k)
            else
              do i=1,3
                jp=(i-1)*3+1
                write(Radka,104)(RCommen(j,1,KPh),j=jp,jp+2)
                k=0
                call WriteLabeledRecord(m50,IdM50(IdM50Commen),Radka,k)
              enddo
            endif
            write(Radka,107)(trez(i,1,KPh),i=1,max(NDimI(KPh),1))
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50TZero),Radka,k)
          else
            KCommen(KPh)=0
            call SetRealArrayTo(trez(1,1,KPh),max(NDimI(KPh),1),0.)
          endif
        enddo
        if(NPhase.gt.1) write(m50,FormA1)('-',i=1,60)
        if(NDatBlock.le.0.or.(.not.Existm90.and..not.Existm95)) then
          write(Radka,'(f11.5,3(1x,a12,1x,i3))') LamAve(1),
     1                          IdM50(IdM50RadType),Radiation(1),
     2                          IdM50(IdM50LPFactor),LPFactor(1)
          k=0
          call WriteLabeledRecord(m50,IdM50(IdM50Lambda),Radka,k)
          if(Radiation(1).ne.NeutronRadiation) then
            if(LPFactor(1).eq.PolarizedMonoPer.or.
     1         LPFactor(1).eq.PolarizedMonoPar) then
              write(Radka,110) AngleMon(1),
     1                         IdM50(IdM50PerfMon),FractPerfMon(1)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50MonAngle),Radka,k)

            else if(LPFactor(1).eq.PolarizedGuinier) then
              write(Radka,110) AlphaGMon(1),
     1                         IdM50(IdM50BetaGMon),BetaGMon(1),
     2                         IdM50(IdM50PerfMon),FractPerfMon(1)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50AlphaGMon),Radka,k)
            endif
            if(NAlfa(1).gt.1) then
              write(Radka,'(i1,3(1x,a12,1x,f10.6))') NAlfa(1),
     1          IdM50(IdM50KAlpha1),LamA1(1),IdM50(IdM50KAlpha2),
     2          LamA2(1),IdM50(IdM50KAlphar),LamRat(1)
              k=0
              call WriteLabeledRecord(m50,IdM50(IdM50NAlpha),Radka,k)
            endif
          endif
        endif
        if(.not.EqIV0(KAnRef,NDatBlock)) then
          write(Radka,105)(KAnRef(i),i=1,NDatBlock)
          call ZdrcniCisla(Radka,NDatBlock)
          k=0
          call WriteLabeledRecord(m50,IdM50(IdM50RefOfAnom),Radka,k)
        endif
        if(NTwin.gt.1) then
          write(radka,105) NTwin
          k=0
          call WriteLabeledRecord(m50,IdM50(IdM50Twin),Radka,k)
          do n=2,NTwin
            do i=1,3
              write(m50,104)(rtw(i+(j-1)*3,n),j=1,3)
            enddo
          enddo
          if(NPhase.gt.1) then
            write(radka,105)(KPhaseTwin(i),i=1,NTwin)
            k=0
            call WriteLabeledRecord(m50,IdM50(IdM50PhaseTwin),Radka,k)
          endif
        endif
        if(ChargeDensities) then
          write(m50,FormA)
     1      IdM50(IdM50Densities)(:idel(IdM50(IdM50Densities)))
          k=0
          call WriteLabeledRecord(m50,IdM50(IdM50WaveFile),
     1                            NameOfWaveFile,k)
        endif
        if(ParentStructure) then
          write(m50,FormA)
     1      IdM50(IdM50ParentStr)(:idel(IdM50(IdM50ParentStr)))
        endif
        k=0
        write(Radka,FormI15) RoundMethod
        call WriteLabeledRecord(m50,IdM50(IdM50RoundMethod),Radka,k)
        write(m50,'(''end'')')
        write(m50,FormA1)('*',i=1,79)
        Radka=PureFileName(:idel(PureFileName))//'.l51'
        ln=NextLogicNumber()
        if(.not.ExistFile(Radka)) then
          call OpenFile(ln,Radka,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          do i=4,1,-1
            if(NPhase.gt.1.and.i.ne.1) then
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))//' '//
     1          PhaseName(1)(:idel(PhaseName(1)))
            else
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))
            endif
            write(ln,'(''end '',a)') Tasks(i)(:idel(Tasks(i)))
          enddo
          rewind ln
        else
          call OpenFile(ln,Radka,'formatted','old')
          if(ErrFlag.ne.0) go to 9999
        endif
        j=0
4000    read(ln,FormA,end=4100,err=4100) Radka
        if(j.eq.0.and.Radka(1:4).eq.'****') go to 4000
        write(m50,FormA) Radka(:idel(Radka))
        j=1
        go to 4000
4100    call CloseIfOpened(ln)
        do i=1,NPhase
          if(CellPar(1,1,i).gt..1) call setmet(0)
        enddo
        ExistM50=.true.
      endif
      if(ParentStructure) then
        call GetPureFileName(FileName,t80)
        call QMag2SSG(t80,0)
      endif
      go to 9999
9000  call FeReadError(m50)
      go to 9900
9010  t80='Unexpected end of file'
      call FeChybne(-1.,-1.,'wrong data on M50 file',t80,SeriousError)
      go to 9900
9020  call FeReadError(ln)
9900  ErrFlag=1
9999  call CloseIfOpened(m50)
      call CloseIfOpened(ln)
      KPhase=KPhaseIn
      SwitchedToComm=.false.
      return
100   format(10i1)
101   format(f15.0)
102   format(6f10.6)
103   format(f8.4,2(1x,a12,1x,f8.4))
104   format(7f11.4)
105   format(18i4)
106   format(8f9.4)
107   format(7f11.6)
108   format(i4,2(1x,a12,i4))
109   format(28i3)
110   format(f11.4,1x,2(a12,f11.2,1x))
111   format(3f11.6,3f11.4)
112   format(i2,a1,i3)
113   format(4(f12.8,f12.4,i2))
      end
