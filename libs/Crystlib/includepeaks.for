      subroutine IncludePeaks
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xn(:,:),isfn(:),kmodap(3)
      character*8 Atomn(:)
      character*80 Veta
      logical BratAtom(:)
      allocatable xn,isfn,Atomn,BratAtom
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 5000
1100  read(m40,FormA,end=5000) Veta
      if(Veta(1:10).ne.'----------'.or.
     1   LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 1100
      read(m40,'(2i5)',end=5000) isw,KPh
      if(KPhase.ne.KPh) go to 5000
      n=0
1120  n=n+1
      read(m40,FormA,end=1130) Veta
      if(Veta(1:10).eq.'----------'.or.Veta.eq.' ') go to 1130
      read(Veta,'(63x,3i3)',end=1130)(kmodap(i),i=1,3)
      k=1
      if(kmodap(1).gt.0) k=kmodap(1)+1
      if(kmodap(2).gt.0) k=k+kmodap(2)
      if(kmodap(3).gt.0) k=k+kmodap(3)*2
      if(k.gt.1) k=k+1
      read(m40,FormA,end=1130)(Veta,j=1,k)
      go to 1120
1130  if(allocated(xn)) deallocate(xn,isfn,Atomn,BratAtom)
      n=n-1
      allocate(xn(3,n),isfn(n),Atomn(n),BratAtom(n))
      rewind m40
1200  read(m40,FormA,end=5000) Veta
      if(Veta(1:10).ne.'----------'.or.
     1   LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 1200
      read(m40,FormA,end=1230) Veta
      i=0
1220  i=i+1
      read(m40,FormA,end=1230) Veta
      if(Veta(1:10).eq.'----------'.or.Veta.eq.' ') go to 1230
      read(Veta,'(a8,19x,3f9.6,12x,3i3)',end=1230)
     1  Atomn(i),(xn(j,i),j=1,3),kmodap
      k=1
      if(kmodap(1).gt.0) k=kmodap(1)+1
      if(kmodap(2).gt.0) k=k+kmodap(2)
      if(kmodap(3).gt.0) k=k+kmodap(3)*2
      if(k.gt.1) k=k+1
      write(Cislo,'(i5)') k
      read(m40,FormA,end=1230)(Veta,j=1,k)
      j=KoincM40(xn(1,i),.5,dist,isw)
      if(ktat(Atom,NAtCalc,AtomN(i)).eq.j) then
        BratAtom(i)=.true.
      else
        BratAtom(i)=j.le.0
      endif
      isfn(i)=0
      go to 1220
1230  call CloseIfOpened(m40)
      call ReallocateAtoms(n)
      call SelAtoms('Select peaks to be added',Atomn,BratAtom,isfn,n,
     1              ich)
      if(ich.ne.0) go to 5000
      isfp=1
      AtNumP=AtNum(1,KPhase)
      do i=2,NAtFormula(KPhase)
        if(AtNum(i,KPhase).lt.AtNumP) then
          isfp=i
          AtNumP=AtNum(i,KPhase)
        endif
      enddo
      Kam=NAtIndTo(isw,KPhase)
      do i=1,n
        if(BratAtom(i)) then
          Kam=Kam+1
          call AtSun(Kam,NAtAll,Kam+1)
          call SetBasicKeysForAtom(Kam)
          isf(Kam)=isfp
          if(Kam.gt.1) then
            PrvniKiAtomu(Kam)=PrvniKiAtomu(Kam-1)+mxda
          else
            PrvniKiAtomu(Kam)=ndoff
          endif
          DelkaKiAtomu(Kam)=10
          Atom(Kam)=Atomn(i)
          ai(Kam)=1.
          kswa(Kam)=KPhase
          iswa(Kam)=isw
          call CopyVek(xn(1,i),x(1,Kam),3)
          beta(1,Kam)=3.
          NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
          call EM40UpdateAtomLimits
        endif
      enddo
      call iom40(1,0,fln(:ifln)//'.m40')
5000  call CloseIfOpened(m40)
      if(allocated(xn)) deallocate(xn,isfn,Atomn,BratAtom)
      return
      end
