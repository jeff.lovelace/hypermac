      subroutine MakeMagModPol(sm,x40,tzero,nt,dt,sm0,smx,smy,kmod,x4s,
     1                         delta,Gamma,Type)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension sm(3,*),sm0(3),smx(3,*),smy(3,*),x40(3),tzero(3),nt(3),
     1          dt(3),x4i(3),x4(3),nd(3),Gamma(9),d4(3),FPolA(2*mxw+1)
      integer Type
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        call SetRealArrayTo(sm(1,i),3,0.)
        kk=0
        do k=1,kmod
         if(k.eq.1) then
           pom=x4(1)-x4s
           j=pom
           if(pom.lt.0.) j=j-1
           pom=pom-float(j)
           if(pom.gt..5) pom=pom-1.
           pom=pom/delta
           call GetFPol(pom,FPolA,2*kmod+1,Type)
         endif
         kk=kk+2
         do l=1,6
            sm(l,i)=sm(l,i)+smx(l,k)*FPolA(kk)+smy(l,k)*FPolA(kk+1)
         enddo
        enddo
      enddo
      return
      end
