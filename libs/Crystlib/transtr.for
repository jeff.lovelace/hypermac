      subroutine transtr(u,tr,tro)
      include 'fepc.cmn'
      dimension u(3),tr(9),tro(9),xp(3),fp(9)
      call multm(tr,u,xp,3,3,1)
      pom=0.
      do m=1,3
        pom=pom+xp(m)**2
      enddo
      pom=1./sqrt(pom)
      do m=1,3
        xp(m)=xp(m)*pom
      enddo
      if(xp(3).gt.-0.99999) then
        fp(3)= xp(1)
        fp(6)= xp(2)
        fp(7)=-xp(1)
        fp(8)=-xp(2)
        fp(9)= xp(3)
        pom=1./(1.+xp(3))
        fp(1)=1.-xp(1)**2*pom
        fp(5)=1.-xp(2)**2*pom
        fp(2)=-xp(1)*xp(2)*pom
        fp(4)=fp(2)
      else
        call UnitMat(fp,3)
        fp(5)=-1.
        fp(9)=-1.
      endif
      call multm(fp,tr,tro,3,3,3)
      return
      end
