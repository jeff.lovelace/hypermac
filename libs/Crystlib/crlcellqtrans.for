      subroutine CrlCellQTrans(TMat,Cell,Qu,nd,ich)
      dimension Cell(*),Qu(*)
      dimension TMat(*),TMatI(36),ZSig(9),ZSigI(9),PomMat(36),VSig(9)
      call MatInv(TMat,TMatI,pom,nd)
      call UnitMat(PomMat,nd)
      if(abs(pom).le.0.) then
        ich=1
        go to 9999
      endif
      if(nd.gt.3) then
        m=0
        do j=1,3
          do k=1,3
            m=m+1
            pom=TMatI(k+(j-1)*nd)
            do l=4,nd
              pom=pom+TMatI(k+(l-1)*nd)*Qu(j+(l-4)*3)
            enddo
            ZSigI(m)=pom
          enddo
        enddo
        call matinv(ZSigI,ZSig,pom,3)
        if(abs(pom).le.0.) then
          ich=1
          go to 9999
        endif
      else
        call CopyMat(TMat ,ZSig ,3)
        call CopyMat(TMatI,ZSigI,3)
      endif
      call DRMatTrCell(ZSig,Cell,PomMat)
      if(nd.gt.3) then
        m=0
        do j=1,3
          do k=4,nd
            m=m+1
            pom=TMatI(k+(j-1)*nd)
            do l=4,nd
              pom=pom+TMatI(k+(l-1)*nd)*Qu(j+(l-4)*3)
            enddo
            VSig(m)=pom
          enddo
        enddo
        call multm(VSig,ZSig,PomMat,nd-3,3,3)
        call trmat(PomMat,Qu,nd-3,3)
      endif
9999  return
      end
