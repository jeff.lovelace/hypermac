      subroutine recip(cp,rcp,vr)
      include 'fepc.cmn'
      dimension cp(6),rcp(6)
      ar=cp(1)
      br=cp(2)
      cr=cp(3)
      cosar=cp(4)
      cosbr=cp(5)
      cosgr=cp(6)
      vr=1.+2.*cosar*cosbr*cosgr-cosar**2-cosbr**2-cosgr**2
      if(vr.le.0) return
      vr=ar*br*cr*sqrt(vr)
      sinar=sqrt(1.-cosar**2)
      sinbr=sqrt(1.-cosbr**2)
      singr=sqrt(1.-cosgr**2)
      a=br*cr*sinar/vr
      b=cr*ar*sinbr/vr
      c=ar*br*singr/vr
      cosa=(cosbr*cosgr-cosar)/(sinbr*singr)
      cosb=(cosgr*cosar-cosbr)/(singr*sinar)
      cosc=(cosar*cosbr-cosgr)/(sinar*sinbr)
      rcp(1)=a
      rcp(2)=b
      rcp(3)=c
      rcp(4)=cosa
      rcp(5)=cosb
      rcp(6)=cosc
      return
      end
