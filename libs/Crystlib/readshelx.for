      subroutine ReadSHELX(Klic,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      dimension h(3),fvar(:),fvarp(:),trp(3,3),ip(3),FFBasicP(9)
      character*(*) FileSHELXIn
      character*256 ShelxFileName
      character*80  t80,FormatShelxIn
      character*4   slovo(80),RESI_string
      character*3   StPh
      character*2   natp
      logical ExistFile,StructureExists,FeYesNo,EqIgCase,
     1        FeYesNoHeader,NovyF,MatRealEqMinusUnitMat
      allocatable fvar,fvarp
      data slovo/'titl','cell','latt','symm','sfac','unit','fvar',
     2           'end ','axis','absc','face','hklf','merg','omit',
     3           'eees','tang','phix','l.s.','afls','bond','list',
     4           'fmap','grid','plan','wght','ilsf','zerr','acta',
     5           'rtab','mpla','sump','nic ','exyz','eadp','exti',
     6           'conn','mole','twin','basf','rem ','damp','dfix',
     7           'afix','part','simu','eqiv','flat','wpdb','temp',
     8           'htab','conf','dang','bind','disp','tref','resi',
     9           'delu','defs','chiv','isor','cgls','bump','swat',
     a           'shel','size','move','more','conf','neut','rigu',
     1           'sadi','#l72','#l73','#l74','#l75','#l76','#l77',
     2           '#l78','#l79','#l80'/
      equivalence (IdTITL,IdNumbers( 1)),(IdCELL,IdNumbers( 2)),
     1            (IdLATT,IdNumbers( 3)),(IdSYMM,IdNumbers( 4)),
     2            (IdSFAC,IdNumbers( 5)),(IdUNIT,IdNumbers( 6)),
     3            (IdFVAR,IdNumbers( 7)),(IdEND ,IdNumbers( 8)),
     4            (IdAXIS,IdNumbers( 9)),(IdABSC,IdNumbers(10)),
     5            (IdFACE,IdNumbers(11)),(IdHKLF,IdNumbers(12)),
     6            (IdMERG,IdNumbers(13)),(IdOMIT,IdNumbers(14)),
     7            (IdEEES,IdNumbers(15)),(IdTANG,IdNumbers(16)),
     8            (IdPHIX,IdNumbers(17)),(IdLS  ,IdNumbers(18)),
     9            (IdAFLS,IdNumbers(19)),(IdBOND,IdNumbers(20)),
     A            (IdLIST,IdNumbers(21)),(IdFMAP,IdNumbers(22)),
     1            (IdGRId,IdNumbers(23)),(IdPLAN,IdNumbers(24)),
     2            (IdWGHT,IdNumbers(25)),(IdILSF,IdNumbers(26)),
     3            (IdZERR,IdNumbers(27)),(IdACTA,IdNumbers(28)),
     4            (IdRTAB,IdNumbers(29)),(IdMPLA,IdNumbers(30)),
     5            (IdSUMP,IdNumbers(31)),(IdNIC ,IdNumbers(32)),
     6            (IdEXYZ,IdNumbers(33)),(IdEADP,IdNumbers(34)),
     7            (IdEXTI,IdNumbers(35)),(IdCONN,IdNumbers(36)),
     8            (IdMOLE,IdNumbers(37)),(IdTWIN,IdNumbers(38)),
     9            (IdBASF,IdNumbers(39)),(IdREM ,IdNumbers(40)),
     A            (IdDAMP,IdNumbers(41)),(IdDFIX,IdNumbers(42)),
     1            (IdAFIX,IdNumbers(43)),(IdPART,IdNumbers(44)),
     2            (IdSIMU,IdNumbers(45)),(IdEQIV,IdNumbers(46)),
     3            (IdFLAT,IdNumbers(47)),(IdWPDB,IdNumbers(48)),
     4            (IdTEMP,IdNumbers(49)),(IdHTAB,IdNumbers(50)),
     5            (IdCONF,IdNumbers(51)),(IdDANG,IdNumbers(52)),
     6            (IdBIND,IdNumbers(53)),(IdDISP,IdNumbers(54)),
     7            (IdTREF,IdNumbers(55)),(IdRESI,IdNumbers(56)),
     8            (IdDELU,IdNumbers(57)),(IdDEFS,IdNumbers(58)),
     9            (IdCHIV,IdNumbers(59)),(IdISOR,IdNumbers(60)),
     A            (IdCGLS,IdNumbers(61)),(IdBUMP,IdNumbers(62)),
     1            (IdSWAT,IdNumbers(63)),(IdSHEL,IdNumbers(64))
      KlicUsed=Klic
      ShelxFileName=fln(:ifln)//'.ins'
      if(.not.ExistFile(ShelxFileName)) ShelxFileName=fln(:ifln)//'.res'
      go to 1100
      entry ReadSpecifiedSHELX(FileSHELXIn,ich)
      ShelxFileName=FileSHELXIn
      KlicUsed=1
1100  if(KlicUsed.le.0) then
        if(StructureExists(fln).and.KlicUsed.ne.-2) then
          if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1                    '" already exists, rewrite it?',0)) go to 9900
        endif
1120    call FeFileManager('Define the input SHELX file',ShelxFileName,
     1                     '*.ins *.res',0,.true.,ich)
        if(ich.ne.0) go to 9900
        if(.not.ExistFile(ShelxFileName)) then
          call FeChybne(-1.,YBottomMessage,'the file "'//
     1                  ShelxFileName(:idel(ShelxFileName))//
     1                  '" does not exist, try again.',' ',SeriousError)
          go to 1120
        endif
        call OpenFile(44,ShelxFileName,'formatted','old')
        if(ErrFlag.ne.0) go to 1120
      else
        call CheckEOLOnFile(ShelxFileName,2)
        if(ErrFlag.ne.0) go to 9900
        call OpenFile(44,ShelxFileName,'formatted','old')
      endif
      i=index(ShelxFileName,'.')
      if(i.gt.0) ShelxFileName=ShelxFileName(:i-1)
      nalloc=10000
      allocate(fvar(nalloc))
      fvar=0.
      FormatShelxIn='(3i4,2f8.0,i4,6f8.5)'
      FormatShelxIn(2:2)='3'
      UseEFormat91=.false.
      Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
      Format95(5:5)='3'
      RESI_string=' '
      if(KPhase.le.1) then
        call SetBasicM40(.true.)
        call SetBasicM50
        NRefBlock=1
        KRefBlock=1
        MaxNSymm=1
        MaxNSymmN=1
        nac=0
        StPh=' '
        call AllocateSymm(96,96,1,1)
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=20
        m=121
        i=1
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4           AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7           FFra(n,NPhase,i),FFia(n,NPhase,i),
     8           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9           TypeFFMag(n,NPhase),
     a           FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1           PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2           ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3           HNSlater(n,NPhase),HZSlater(n,NPhase),
     4           ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5           NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6           CoreValSource(n,NPhase))
        FFCoreD=0.
        FFValD=0.
        CoreValSource='Default'
        MaxNLattVec=1
        MaxNAtFormula=0
        call SetRealArrayTo(TypicalDist(1,1,1),NPhase*n**2,-1.)
      else
        nac=NAtIndToAll(KPhase-1)
        write(StPh,'(''_'',i2)') KPhase
        NDim(KPhase)=3
        call ReallocSymm(NDim(KPhase),96,4,NComp(KPhase),NPhase,0)
        call ReallocFormF(20,NPhase,NDatBlock)
        deallocate(isa)
        MaxNSymm=96
        MaxNLattVec=4
        allocate(isa(MaxNSymm,NAtAll))
        do ia=1,NAtAll
          do i=1,MaxNSymm
            isa(i,ia)=i
          enddo
        enddo
      endif
      HKLF5RefBlock(KRefBlock)=0
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      nfp=0
      nlam=1
      it=0
      nvar=0
      call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
      call UnitMat(rm(1,1,1,KPhase),3)
      ISwSymm(1,1,KPhase)=1
      call SetRealArrayTo(s6(1,1,1,KPhase),6,0.)
      call CodeSymm(rm6(1,1,1,1),s6(1,1,1,1),symmc(1,1,1,1),0)
      call UnitMat(trp,NDim(KPhase))
      call SetRealArrayTo(sc(2,1),5,0.)
1200  read(44,FormA,end=2000) t80
      call mala(t80)
      if(idel(t80).le.0) go to 1200
      do i=1,80
        k=0
        call kus(t80,k,Cislo)
        if(idel(Cislo).gt.4) Cislo=Cislo(1:4)
        call mala(Cislo)
        if(EqIgCase(Cislo,slovo(i))) then
          if(i.eq.IdTITL) then
            if(KPhase.gt.1) go to 1200
            StructureName=t80(k:)
          else if(i.eq.IdCELL) then
            call kus(t80,k,cislo)
            if(KPhase.le.1) then
              call posun(Cislo,1)
              read(cislo,103) LamAve(1)
            endif
            if(LamAve(1).lt..1) Radiation(KRefBlock)=ElectronRadiation
            do j=1,6
              call kus(t80,k,cislo)
              call posun(Cislo,1)
              read(cislo,103) CellPar(j,1,KPhase)
              if(KPhase.gt.1.and.ExistPowder)
     1          CellPwd(j,KPhase)=CellPar(j,1,KPhase)
            enddo
            call setmet(0)
          else if(i.eq.IdLATT) then
            call kus(t80,k,Cislo)
            call posun(Cislo,0)
            read(Cislo,FormI15) Latt
            if(Latt.gt.0) then
              NCSymm(KPhase)=2
            else
              NCSymm(KPhase)=1
              Latt=-Latt
            endif
            if(Latt.eq.1) then
              NLattVec(KPhase)=1
              Latt=1
            else if(Latt.le.2.or.Latt.le.4) then
              NLattVec(KPhase)=Latt
              Latt=Latt+3
            else
              NLattVec(KPhase)=2
              Latt=Latt-3
            endif
            Lattice(KPhase)=Smbc(Latt:Latt)
            call CtiLatt(0)
            Grupa(KPhase)=Lattice(KPhase)
            MaxNLattVec=max(NLattVec(KPhase),MaxNLattVec)
          else if(i.eq.IdSYMM) then
            t80=t80(k:)
            call Zhusti(t80)
            do j=1,idel(t80)
              if(t80(j:j).eq.',') t80(j:j)=' '
            enddo
            if(t80.eq.'x y z') go to 1200
            NSymm(KPhase)=NSymm(KPhase)+1
            MaxNSymm=NSymm(KPhase)
            MaxNSymmN=NSymm(KPhase)
            NSymmN(KPhase)=NSymm(KPhase)
            call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                       NLattVec(KPhase),NComp(KPhase),NPhase,0)
            call SetRealArrayTo(rm6(1,NSymm(KPhase),1,KPhase),
     1                          NDimQ(KPhase),0.)
            ISwSymm(NSymm(KPhase),1,KPhase)=1
            k=0
            do 1430j=1,3
              s6(j,NSymm(KPhase),1,KPhase)=0.
              call kus(t80,k,Cislo)
              do l=1,3
                m=index(Cislo,smbx(l))
                if(m.eq.0) then
                  zn=0
                else
                  Cislo(m:m)=' '
                  if(m.eq.1) then
                    zn= 1.
                  else if(Cislo(m-1:m-1).eq.'+') then
                    zn= 1.
                    Cislo(m-1:m-1)=' '
                  else if(Cislo(m-1:m-1).eq.'-') then
                    zn=-1.
                    Cislo(m-1:m-1)=' '
                  endif
                endif
                jj=j+(l-1)*3
                rm6(jj,NSymm(KPhase),1,KPhase)=zn
                call zhusti(Cislo)
                if(idel(Cislo).le.0) go to 1430
              enddo
              if(idel(Cislo).eq.0) go to 1430
              s6(j,NSymm(KPhase),1,KPhase)=fract(Cislo,ich)
              if(ich.ne.0) then
                call FeChybne(-1.,-1.,'the symmetry was incorrect.',t80,
     1                        SeriousError)
                go to 9900
              endif
1430        continue
            call CopyMat(rm6(1,NSymm(KPhase),1,1),
     1                   rm(1,NSymm(KPhase),1,1),3)
            call CodeSymm(rm6(1,NSymm(KPhase),1,1),
     1                    s6(1,NSymm(KPhase),1,1),
     2                    symmc(1,NSymm(KPhase),1,1),0)
          else if(i.eq.IdSFAC) then
            NovyF=.true.
1440        if(k.ge.80) go to 1200
            call kus(t80,k,Cislo)
            call DalsiShelx(Cislo,t80,k,0)
            if(.not.NovyF) then
              Natp=Cislo
              it=it+1
              if(it.le.9) then
                read(Cislo,103,err=1445) FFBasicP(it)
                go to 1440
              else if(it.lt.14) then
                go to 1440
              else
                NovyF=.true.
                go to 1440
              endif
1445          NovyF=.true.
              Cislo=Natp
            endif
            if(it.eq.9) then
              if(KlicUsed.eq.-2) then
                if(Radiation(KDatBlock).eq.NeutronRadiation) then
                  FFn (NAtFormula(KPhase),KPhase)=FFBasicP(9)
                  FFni(NAtFormula(KPhase),KPhase)=0.
                else
                  FFType(KPhase)=-9
                  call CopyVek(FFBasicP,
     1              FFBasic(1,NAtFormula(KPhase),KPhase),9)

                endif
              else
                FFType(KPhase)=-9
                call CopyVek(FFBasicP,
     1            FFBasic(1,NAtFormula(KPhase),KPhase),9)
              endif
            endif
            if(NovyF) then
              NAtFormula(KPhase)=NAtFormula(KPhase)+1
              MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
              call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
              AtTypeFull(NAtFormula(KPhase),KPhase)=Cislo(1:2)
              AtTypeMag(NAtFormula(KPhase),KPhase)=' '
              AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
              call Uprat(AtTypeFull(NAtFormula(KPhase),KPhase))
              call GetPureAtType(AtTypeFull(NAtFormula(KPhase),KPhase),
     1                           AtType(NAtFormula(KPhase),KPhase))
              call EM50ReadOneFormFactor(NAtFormula(KPhase))
              call EM50OneFormFactorSet(NAtFormula(KPhase))
              j=LocateInStringArray(atn,98,
     1          AtType(NAtFormula(KPhase),KPhase),IgnoreCaseYes)
              if(j.gt.0) then
                AtNum(NAtFormula(KPhase),KPhase)=float(j)
              else
                AtNum(NAtFormula(KPhase),KPhase)=0.
              endif
              NovyF=.false.
              it=0
            endif
            go to 1440
          else if(i.eq.IdUNIT) then
1450        call kus(t80,k,Cislo)
            nfp=nfp+1
            call posun(Cislo,1)
            read(Cislo,103) AtMult(nfp,KPhase)
            if(k.lt.80) go to 1450
          else if(i.eq.IdFVAR) then
1452        call kus(t80,k,cislo)
            call posun(cislo,1)
            read(cislo,103,err=1800) pom
            if(nvar.eq.0) then
              sc(1,1)=pom
              if(sc(1,1).ge.100.) sc(1,1)=sc(1,1)-100.
              sc(1,1)=sc(1,1)*sqrt(10.)
            else
              fvar(nvar)=pom
            endif
            if(nvar.ge.nalloc) then
              allocate(fvarp(nalloc))
              call CopyVeK(fvar,fvarp,nalloc)
              nallocp=nalloc
              nalloc=nalloc+10000
              deallocate(fvar)
              allocate(fvar(nalloc))
              call CopyVeK(fvarp,fvar,nallocp)
              deallocate(fvarp)
            endif
            nvar=nvar+1
            if(k.lt.80) go to 1452
          else if(i.eq.IdEND) then
            go to 2000
          else if(i.eq.IdHKLF) then
            call StToInt(t80,k,ip,1,.false.,ich)
            if(ich.ne.0) go to 1800
            if(ip(1).eq.5) HKLF5RefBlock(KRefBlock)=1
            if(k.lt.80) then
              call StToReal(t80,k,trp,1,.false.,ich)
              if(ich.ne.0) go to 1800
              call StToReal(t80,k,trp,9,.false.,ich)
              if(ich.ne.0) go to 1800
            endif
          else if(i.eq.IdTWIN) then
            if(KPhase.gt.1) go to 1200
            NTwin=2
            m=0
            do j=1,3
              do l=1,3
                m=m+1
                if(k.ge.80) then
                  if(j.eq.l) then
                    rtw(m,2)=-1.
                  else
                    rtw(m,2)= 0.
                  endif
                else
                  call kus(t80,k,Cislo)
                  call posun(Cislo,1)
                  read(Cislo,103) rtw(m,2)
                endif
              enddo
            enddo
          else if(i.eq.IdBASF) then
            if(KPhase.gt.1) go to 1200
            j=1
1460        if(k.lt.len(t80)) then
              j=j+1
              call kus(t80,k,cislo)
              call posun(cislo,1)
              read(cislo,103) sctw(j,1)
              KiS(mxscu+j-1,1)=1
              go to 1460
            endif
          else if(i.eq.IdDAMP) then
            if(KPhase.gt.1) go to 1200
            call kus(t80,k,cislo)
            call posun(cislo,1)
            read(cislo,103) pom
          else if(i.eq.IdRESI) then
1470        call kus(t80,k,Cislo)
            do j=1,idel(Cislo)
              if(index(cifry,Cislo(j:j)).le.0) then
                if(k.lt.len(t80)) then
                  go to 1470
                else
                  go to 1200
                endif
              endif
            enddo
            RESI_string=Cislo
          else if(i.eq.IdDFIX) then
            go to 1500
          else
            go to 1500
          endif
          go to 1200
1500      k=max(idel(t80),1)
          if(t80(k:k).eq.'=') then
            read(44,FormA,end=2000) t80
            go to 1500
          else
            go to 1200
          endif
        endif
      enddo
      if(NCSymm(KPhase).eq.2) then
        do i=1,NSymm(KPhase)
          if(MatRealEqMinusUnitMat(RM(1,i,1,KPhase),NDim(KPhase),.001))
     1      go to 1600
        enddo
        call CrlExpandCentroSym
        MaxNSymm=max(NSymm(KPhase),MaxNSymm)
        MaxNSymmN=max(NSymm(KPhase),MaxNSymmN)
        NSymmN(KPhase)=NSymm(KPhase)
        deallocate(isa)
        n=ubound(isf,1)
        allocate(isa(MaxNSymm,n))
        do i=1,n
          do j=1,MaxNSymm
            isa(j,i)=j
          enddo
        enddo
1600    NCSymm(KPhase)=1
      endif
      call ShelxSeparMinus(t80)
      if(nac.le.0.and.MxAtAll.le.0) then
        call AllocateAtoms(1)
      else
        call ReallocateAtoms(1)
        call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+1)
      endif
c      call EM40UpdateAtomLimits
      nac=nac+1
      k=0
      call kus(t80,k,Cislo)
      call SetBasicKeysForAtom(nac)
      if(index(Cislo,'_').le.0.and.RESI_string.ne.' ')
     1  Cislo=Cislo(:idel(Cislo))//'_'//RESI_string(:idel(RESI_string))
      atom(nac)=Cislo
      if(KPhase.gt.1) Atom(nac)=Atom(nac)(:idel(Atom(nac)))//
     1                          StPh(:idel(StPh))
      kswa(nac)=KPhase
      k=4
      call kus(t80,k,cislo)
      call DalsiShelx(cislo,t80,k,1)
      call posun(cislo,0)
      read(cislo,FormI15,err=1790) isf(nac)
      do i=1,3
        call kus(t80,k,cislo)
        call DalsiShelx(cislo,t80,k,1)
        call posun(cislo,1)
        read(cislo,103,err=1790) x(i,nac)
        call ShelxGetPar(x(i,nac),fvar)
        sx(i,nac)=0.
      enddo
      call SetRealArrayTo(h,3,0.)
      call SpecPos(x(1,nac),h,0,1,.01,i)
      pom=1./float(i)
      call kus(t80,k,cislo)
      call DalsiShelx(cislo,t80,k,1)
      if(Cislo.eq.' ') then
        ai(nac)=pom
      else
        call posun(cislo,1)
        read(cislo,103) ai(nac)
        call ShelxGetPar(ai(nac),fvar)
      endif
      sai(nac)=0.
      if(abs(ai(nac)-pom).lt..00002) ai(nac)=pom
      itf(nac)=2
      call SetRealArrayTo(beta(1,nac),6,0.)
      do i=1,6
        call kus(t80,k,cislo)
        call DalsiShelx(cislo,t80,k,1)
        sbeta(i,nac)=0.
        if(i.eq.1.and.Cislo.eq.' ') then
          beta(i,nac)=0.02
        else
          call posun(cislo,1)
          read(cislo,103,err=1790) beta(i,nac)
          call ShelxGetPar(beta(i,nac),fvar)
          if(i.lt.6.and.k.eq.80) then
            itf(nac)=1
            exit
          endif
        endif
      enddo
      if(itf(nac).eq.2) then
        pom=beta(4,nac)
        beta(4,nac)=beta(6,nac)
        beta(6,nac)=pom
        do i=1,6
           beta(i,nac)=urcp(i,1,KPhase)*beta(i,nac)
          sbeta(i,nac)=0.
        enddo
        call boueq(beta(1,nac),sbeta(1,nac),1,bizo,sbizo,1)
      else
        if(beta(1,nac).lt.-1.) then
          beta(1,nac)=bizo*abs(beta(1,nac))
        else
          beta(1,nac)=beta(1,nac)*episq
          bizo=beta(1,nac)
        endif
        sbeta(1,nac)=0.
      endif
      if(nac.eq.1) then
        PrvniKiAtomu(nac)=ndoff+1
      else
        PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
      endif
      DelkaKiAtomu(nac)=mxda
      call SetIntArrayTo(KiA(1,nac),mxda,0)
      if(KPhase.gt.1) then
        NAtIndLen(1,KPhase)=nac-NAtIndToAll(KPhase-1)
      else
        NAtIndLen(1,KPhase)=nac
      endif
      call EM40UpdateAtomLimits
      go to 1200
1790  call AtSun(NAtMolFr(1,1)+1,NAtAll+1,NAtMolFr(1,1))
      nac=nac-1
1800  if(VasekTest.ne.0) then
        Ninfo=2
        TextInfo(1)='Please check carefully your SHELX "ins" file'//
     1              '  and in case that'
        Textinfo(2)='the relevant line is correct please inform '//
     1              'authors of Jana2000'
        call FeMsgShow(1,-1.,YCenBasWin-1,.true.)
        call FeChybne(-1.,YBottomMessage,'the following line from '//
     1                'wasn''t correctly interpreted.',t80,SeriousError)
        call FeMsgClear(1)
      endif
      go to 1200
2000  if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      if(KPhase.gt.1) then
        NAtIndLen(1,KPhase)=nac-NAtIndToAll(KPhase-1)
      else
        NAtIndLen(1,KPhase)=nac
      endif
      call EM40UpdateAtomLimits
      itfmax=2
      MagParMax=0
      centr(KPhase)=0
      call SetRealArrayTo(ec,12*MxDatBlock,0.)
      OverAllB(1)=0.
      NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)*NCSymm(KPhase)
      nt=9
      do i=1,NAtFormula(KPhase)
        AtMult(i,KPhase)=AtMult(i,KPhase)/float(NUnits(KPhase))
      enddo
      call CloseIfOpened(44)
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call SetFormula(Formula(KPhase))
      call iom50(1,0,fln(:ifln)//'.m50')
      if(KPhase.gt.1.or.KlicUsed.lt.0) then
        IType=2
        go to 9000
      endif
      IType=0
      j=LocateInArray(LamAve(1),LamAveD,7,.0001)
      KLam(1)=j
      if(j.eq.0) then
        LamA1(1)=LamAve(1)
        LamA2(1)=LamAve(1)
        LamRat(1)=0.
        NAlfa(1)=1
      else
        LamA1(1)=LamA1D(j)
        LamA2(1)=LamA2D(j)
        LamRat(1)=LamRatD(j)
        NAlfa(1)=2
      endif
      if(.not.ExistFile(ShelxFileName(:idel(ShelxFileName))//'.hkl'))
     1   then
        TextInfo(1)='SHELX reflection file doesn''t exists.'
        if(ExistM90.and.KlicUsed.eq.0) then
          NInfo=4
          TextInfo(2)='The old existing m90 and m95 will be used '
          TextInfo(3)='with changed m50 and m40. The consistency with '
          TextInfo(4)='reflection files is not guaranteed!'
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          call OpenFile(90,fln(:ifln)//'.m90','formatted','unknown')
          IType=2
          go to 2500
        else if(ExistM95.and.KlicUsed.eq.0) then
          NInfo=4
          TextInfo(2)='The old existing m95 will be used '
          TextInfo(3)='with changed m50 and m40. The consistency '
          TextInfo(4)='with reflection files is not guaranteed!'
          if(FeYesNoHeader(-1.,-1.,'Do you want to create M90 file now?'
     1                     ,1)) then
            IType=1
            go to 9000
          else
            go to 9900
          endif
        else
          if(KlicUsed.eq.0) then
            NInfo=2
            TextInfo(2)='No reflection file present.'
            call FeInfoOut(-1.,-1.,'WARNING','L')
          endif
          NRef90(KDatBlock)=0
          IType=2
          go to 9000
        endif
      endif
      t80=ShelxFileName(:idel(ShelxFileName))//'.hkl'
      call CheckEOLOnFile(t80,0)
      if(ErrFlag.ne.0) go to 9000
      k=HKLF5RefBlock(KRefBlock)
      call SetBasicM95(KRefBlock)
      HKLF5RefBlock(KRefBlock)=k
      SourceFileRefBlock(KRefBlock)=t80
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      call OpenFile(90,t80,'formatted','old')
      if(ErrFlag.ne.0) go to 9000
      DifCode(KRefBlock)=IdImportSHELXI
      call UnitMat(TrMP,3)
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
      CellReadIn(KRefBlock)=.true.
      CorrLp(KRefBlock)=-1
      CorrAbs(KRefBlock)=-1
      LamAveRefBlock(KRefBlock)=LamAve(1)
      NDim95(KRefBlock)=3
      if(LamAve(1).lt..1) then
        RadiationRefBlock(KRefBlock)=ElectronRadiation
      else
        RadiationRefBlock(KRefBlock)=XRayRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
        AngleMonRefBlock(KRefBlock)=CrlMonAngle(MonCell(1,3),MonH(1,3),
     1                                        LamAveRefBlock(KRefBlock))
        AlphaGMonRefBlock(KRefBlock)=0.
        BetaGMonRefBlock(KRefBlock)=0.
      endif
      TempRefBlock(KRefBlock)=293
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      RefBlockFileName=fln(:ifln)//'.l01'
      t80='     0 reflections already transformed'
      write(t80(1:6),100) 0
      call FeTxOut(-1.,-1.,t80)
2500  Format91(2:2)='3'
      Format95(5:5)='3'
      tbar=0.
      call SetRealArrayTo(uhly,4,0.)
      call SetRealArrayTo(dircos,6,0.)
      call SetRealArrayTo(corrf,2,1.)
      iq=1
      itw=1
      KProf=0
      NProf=0
      DRlam=0.
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      if(HKLF5RefBlock(KRefBlock).eq.1) NTwin=1
3000  if(IType.eq.0) then
        read(90,FormatShelxIn,end=4000)(ihp(i),i=1,NDim(KPhase)),ri,rs,k
        if(HKLF5RefBlock(KRefBlock).eq.1) then
          itw=k
          NTwin=max(NTwin,iabs(k))
        endif
        call indtr(ihp,trp,ih,3)
        if(ih(1).gt.900) go to 3000
      else
        read(90,Format91,end=4000)(ih(i),i=1,NDim(KPhase)),ri,rs
      endif
      if(ihp(1).eq.0.and.ihp(2).eq.0.and.ihp(3).eq.0) go to 4000
      NRef95(KDatBlock)=NRef95(KDatBlock)+1
      no=NRef95(KDatBlock)
      if(IType.eq.0) then
        if(mod(NRef95(KDatBlock),50).eq.0) then
          write(t80(1:6),100) NRef95(KDatBlock)
          call FeTxOutCont(t80)
        endif
        ri=ri*10.
        rs=rs*10.
      endif
      if(rs.lt..1) then
        if(ri.gt.0.) then
          rs=max(sqrt(ri),.1)
        else
          ri=.1
          rs=.1
        endif
      endif
      if(IType.eq.0) then
        expos=float(NRef95(KDatBlock))*.1
        iflg(1)=iq
        iflg(2)=itw
        iflg(3)=0
        call DRPutReflectionToM95(95,n)
        NLines95(KRefBlock)=NLines95(KRefBlock)+2
      endif
      go to 3000
4000  if(NTwin.gt.1.and.HKLF5RefBlock(KRefBlock).eq.1) then
        do i=1,NTwin
          call UnitMat(rtw (1,i),3)
          call UnitMat(rtwi(1,i),3)
        enddo
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      if(IType.eq.0) then
        call CloseIfOpened(95)
        write(t80(1:6),100) NRef95(KDatBlock)
        call FeTxOutCont(t80)
        call FeTxOutEnd
        call iom95(1,fln(:ifln)//'.m95')
        call CompleteM95(0)
        ExistM95=.true.
      endif
9000  if(KPhase.le.1) call FinalStepOfImport(IType,'SHELX')
      go to 9999
9100  call FeChybne(-1.,-1.,'syntax error in the input SHELX file',
     1              t80,SeriousError)
9900  ich=1
9999  call CloseIfOpened(90)
      call CloseIfOpened(91)
      call CloseIfOpened(95)
      if(allocated(fvar)) deallocate(fvar)
      return
100   format(i6)
101   format(6f10.6)
103   format(f15.1)
104   format(5i5,l5,3i5,3f8.3,f10.5)
      end
