      subroutine ShSgGetReflCond(rmi,si,vc,nd)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmt(36),rmp(36),rmf(36),vc(7),pxx(7),si(*),rmi(*)
      call SetRealArrayTo(vc,7,0.)
      call TrMat(rmi,rmt,NDim(KPhase),NDim(KPhase))
      k=nd**2+1
      kk=0
      do i=1,NDim(KPhase)
        do j=1,NDim(KPhase)
          kk=kk+1
          if(i.gt.nd.or.j.gt.nd) cycle
          k=k-1
          if(i.ne.j) then
            rmp(k)=rmt(kk)
          else
            rmp(k)=rmt(kk)-1.
          endif
        enddo
      enddo
      call triangl(rmp,vc,nd,nd,nx)
      call SetRealArrayTo(rmf,36,0.)
      call SetRealArrayTo(rmf,nd,-333.)
      do 1060i=1,nd
        k=i
        do j=1,nd
          if(abs(rmp(k)).gt..0001) then
            jj=nd+1-j
            rmf(jj)=0.
            kk=jj+(jj-2)*nd
            k=k+nd
            do l=j+1,nd
              rmf(kk)=-rmp(k)
              k =k +nd
              kk=kk-nd
            enddo
            go to 1060
          endif
          k=k+nd
        enddo
        go to 1065
1060  continue
1065  do i=1,nd
        if(rmf(i).lt.-300.) then
          rmf(i)=0.
          k=i+(i-1)*nd
          rmf(k)=1.
        endif
      enddo
      k=1
      do i=1,nd
        call FractVecToNomDenForm(rmf(k),pom,nd,ich)
      enddo
      call Multm(si,rmf,pxx,1,nd,nd)
      do izn=1,-1,-2
        if(izn.lt.0) then
          call RealVectorToOpposite(pxx,vc,nd)
        else
          call CopyVek(pxx,vc,nd)
        endif
        call od0do1(vc,vc,nd)
        pom=0.
        do i=1,nd
          pom=pom+abs(vc(i))
        enddo
        if(pom.lt..0001) cycle
        zn=0.
        do m=1,nd
         if(vc(m).gt..5001) vc(m)=vc(m)-1.
         if(abs(zn).lt..0001.and.abs(vc(m)).gt..0001)
     1     zn=sign(1.,vc(m))
         vc(m)=vc(m)*zn
        enddo
        call FractVecToNomDenForm(vc,vc(nd+1),nd,ich)
      enddo
      return
      end
