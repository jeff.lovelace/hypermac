      subroutine InvSymmOp(R1,s1,R2,s2,n)
      dimension R1(n,n),R2(n,n),s1(n),s2(n)
      call MatInv(R1,R2,det,n)
      call MultM(R1,s1,s2,n,n,1)
      call RealVectorToOpposite(s2,s2,n)
      return
      end
