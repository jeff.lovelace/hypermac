      subroutine SpecPos(xn,xn4,kfsi,isw,dif,nspec)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xn(3),x(6),x1(6),x2(6),x3(6),x3r(6),ax(6,3),px(6),
     1          smp(3,3),GammaN(9),xn4(*)
      logical EqRV0
      integer CrlMagInver
      nspec=0
      call SetRealArrayTo(ax,18,0.)
      call SetRealArrayTo(px, 6,0.)
      call SetRealArrayTo(x , 6,0.)
      call CopyVek(xn,x,3)
      if(kfsi.gt.0) then
        NDimP=NDim(KPhase)
        call od0do1(xn4,x(4),NDimI(KPhase))
      else
        NDimP=3
      endif
      nx=0
      do jsym=1,NSymm(KPhase)
        call GetGammaN(jsym,isw,KPhase,GammaN)
        if(.not.EqRV0(GammaN,3*NDimI(KPhase),.0001)) cycle
        call multm(rm6(1,jsym,isw,KPhase),x,x1,NDim(KPhase),
     1             NDim(KPhase),1)
        call AddVek(x1,s6(1,jsym,isw,KPhase),x2,NDim(KPhase))
        do 1200ivt=1,NLattVec(KPhase)
          call SetRealArrayTo(px(nx+1),6-nx,0.)
          call AddVek(x2,vt6(1,ivt,isw,KPhase),x3,NDim(KPhase))
          do m=1,NDimP
            if(m.gt.3) call od0do1(x3(m),x3(m),1)
            x3(m)=x(m)-x3(m)
            apom=anint(x3(m))
            x3(m)=x3(m)-apom
            if(m.le.3) then
              px(nx+m)=px(nx+m)-s6(m,jsym,isw,KPhase)
     1                -vt6(m,ivt,isw,KPhase)-apom
            else
              if(abs(x3(m)).gt..00001.and.
     1           abs(x3(m)).lt..99999) go to 1200
            endif
          enddo
          call multm(MetTens(1,isw,KPhase),x3,x3r,3,3,1)
          if(sqrt(scalmul(x3,x3r)).gt.dif) go to 1200
          nspec=nspec+1
          do l=1,3
            do m=1,3
              smp(l,m)=rm6(l+NDim(KPhase)*(m-1),jsym,isw,KPhase)
              if(m.eq.l) then
                ax(nx+l,m)=smp(l,m)-1.
              else
                ax(nx+l,m)=smp(l,m)
              endif
            enddo
          enddo
          call triangl(ax,px,6,3,nx)
1200    continue
      enddo
      do j=1,nx
        do k=1,3
          if(abs(ax(j,k)).gt..0001) then
            l=k
            exit
          endif
        enddo
        pom=px(j)
        do k=l+1,3
          pom=pom-ax(j,k)*xn(k)
        enddo
        xn(l)=pom
      enddo
      if(CrlMagInver().gt.0) nspec=nspec/2
      return
      end
